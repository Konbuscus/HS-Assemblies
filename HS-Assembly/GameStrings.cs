﻿// Decompiled with JetBrains decompiler
// Type: GameStrings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class GameStrings
{
  private static Map<GameStringCategory, GameStringTable> s_tables = new Map<GameStringCategory, GameStringTable>();
  private static readonly char[] LANGUAGE_RULE_ARG_DELIMITERS = new char[1]{ ',' };
  public static Map<TAG_CLASS, string> s_classNames = new Map<TAG_CLASS, string>() { { TAG_CLASS.DEATHKNIGHT, "GLOBAL_CLASS_DEATHKNIGHT" }, { TAG_CLASS.DRUID, "GLOBAL_CLASS_DRUID" }, { TAG_CLASS.HUNTER, "GLOBAL_CLASS_HUNTER" }, { TAG_CLASS.MAGE, "GLOBAL_CLASS_MAGE" }, { TAG_CLASS.PALADIN, "GLOBAL_CLASS_PALADIN" }, { TAG_CLASS.PRIEST, "GLOBAL_CLASS_PRIEST" }, { TAG_CLASS.ROGUE, "GLOBAL_CLASS_ROGUE" }, { TAG_CLASS.SHAMAN, "GLOBAL_CLASS_SHAMAN" }, { TAG_CLASS.WARLOCK, "GLOBAL_CLASS_WARLOCK" }, { TAG_CLASS.WARRIOR, "GLOBAL_CLASS_WARRIOR" }, { TAG_CLASS.NEUTRAL, "GLOBAL_CLASS_NEUTRAL" } };
  public static Map<TAG_RACE, string> s_raceNames = new Map<TAG_RACE, string>() { { TAG_RACE.BLOODELF, "GLOBAL_RACE_BLOODELF" }, { TAG_RACE.DRAENEI, "GLOBAL_RACE_DRAENEI" }, { TAG_RACE.DWARF, "GLOBAL_RACE_DWARF" }, { TAG_RACE.GNOME, "GLOBAL_RACE_GNOME" }, { TAG_RACE.GOBLIN, "GLOBAL_RACE_GOBLIN" }, { TAG_RACE.HUMAN, "GLOBAL_RACE_HUMAN" }, { TAG_RACE.NIGHTELF, "GLOBAL_RACE_NIGHTELF" }, { TAG_RACE.ORC, "GLOBAL_RACE_ORC" }, { TAG_RACE.TAUREN, "GLOBAL_RACE_TAUREN" }, { TAG_RACE.TROLL, "GLOBAL_RACE_TROLL" }, { TAG_RACE.UNDEAD, "GLOBAL_RACE_UNDEAD" }, { TAG_RACE.WORGEN, "GLOBAL_RACE_WORGEN" }, { TAG_RACE.MURLOC, "GLOBAL_RACE_MURLOC" }, { TAG_RACE.DEMON, "GLOBAL_RACE_DEMON" }, { TAG_RACE.SCOURGE, "GLOBAL_RACE_SCOURGE" }, { TAG_RACE.MECHANICAL, "GLOBAL_RACE_MECHANICAL" }, { TAG_RACE.ELEMENTAL, "GLOBAL_RACE_ELEMENTAL" }, { TAG_RACE.OGRE, "GLOBAL_RACE_OGRE" }, { TAG_RACE.PET, "GLOBAL_RACE_PET" }, { TAG_RACE.TOTEM, "GLOBAL_RACE_TOTEM" }, { TAG_RACE.NERUBIAN, "GLOBAL_RACE_NERUBIAN" }, { TAG_RACE.PIRATE, "GLOBAL_RACE_PIRATE" }, { TAG_RACE.DRAGON, "GLOBAL_RACE_DRAGON" } };
  public static Map<TAG_RARITY, string> s_rarityNames = new Map<TAG_RARITY, string>() { { TAG_RARITY.COMMON, "GLOBAL_RARITY_COMMON" }, { TAG_RARITY.EPIC, "GLOBAL_RARITY_EPIC" }, { TAG_RARITY.LEGENDARY, "GLOBAL_RARITY_LEGENDARY" }, { TAG_RARITY.RARE, "GLOBAL_RARITY_RARE" }, { TAG_RARITY.FREE, "GLOBAL_RARITY_FREE" } };
  public static Map<TAG_CARD_SET, string> s_cardSetNames = new Map<TAG_CARD_SET, string>() { { TAG_CARD_SET.CORE, "GLOBAL_CARD_SET_CORE" }, { TAG_CARD_SET.EXPERT1, "GLOBAL_CARD_SET_EXPERT1" }, { TAG_CARD_SET.REWARD, "GLOBAL_CARD_SET_REWARD" }, { TAG_CARD_SET.PROMO, "GLOBAL_CARD_SET_PROMO" }, { TAG_CARD_SET.FP1, "GLOBAL_CARD_SET_NAXX" }, { TAG_CARD_SET.PE1, "GLOBAL_CARD_SET_GVG" }, { TAG_CARD_SET.BRM, "GLOBAL_CARD_SET_BRM" }, { TAG_CARD_SET.TGT, "GLOBAL_CARD_SET_TGT" }, { TAG_CARD_SET.LOE, "GLOBAL_CARD_SET_LOE" }, { TAG_CARD_SET.OG, "GLOBAL_CARD_SET_OG" }, { TAG_CARD_SET.OG_RESERVE, "GLOBAL_CARD_SET_OG_RESERVE" }, { TAG_CARD_SET.SLUSH, "GLOBAL_CARD_SET_DEBUG" }, { TAG_CARD_SET.KARA, "GLOBAL_CARD_SET_KARA" }, { TAG_CARD_SET.KARA_RESERVE, "GLOBAL_CARD_SET_KARA_RESERVE" }, { TAG_CARD_SET.GANGS, "GLOBAL_CARD_SET_GANGS" }, { TAG_CARD_SET.GANGS_RESERVE, "GLOBAL_CARD_SET_GANGS_RESERVE" } };
  public static Map<TAG_CARD_SET, string> s_cardSetNamesShortened = new Map<TAG_CARD_SET, string>() { { TAG_CARD_SET.CORE, "GLOBAL_CARD_SET_CORE" }, { TAG_CARD_SET.EXPERT1, "GLOBAL_CARD_SET_EXPERT1" }, { TAG_CARD_SET.REWARD, "GLOBAL_CARD_SET_REWARD" }, { TAG_CARD_SET.PROMO, "GLOBAL_CARD_SET_PROMO" }, { TAG_CARD_SET.FP1, "GLOBAL_CARD_SET_NAXX" }, { TAG_CARD_SET.PE1, "GLOBAL_CARD_SET_GVG" }, { TAG_CARD_SET.BRM, "GLOBAL_CARD_SET_BRM" }, { TAG_CARD_SET.TGT, "GLOBAL_CARD_SET_TGT_SHORT" }, { TAG_CARD_SET.LOE, "GLOBAL_CARD_SET_LOE_SHORT" }, { TAG_CARD_SET.OG, "GLOBAL_CARD_SET_OG_SHORT" }, { TAG_CARD_SET.OG_RESERVE, "GLOBAL_CARD_SET_OG_RESERVE" }, { TAG_CARD_SET.SLUSH, "GLOBAL_CARD_SET_DEBUG" }, { TAG_CARD_SET.KARA, "GLOBAL_CARD_SET_KARA_SHORT" }, { TAG_CARD_SET.KARA_RESERVE, "GLOBAL_CARD_SET_KARA_RESERVE" }, { TAG_CARD_SET.GANGS, "GLOBAL_CARD_SET_GANGS_SHORT" }, { TAG_CARD_SET.GANGS_RESERVE, "GLOBAL_CARD_SET_GANGS_RESERVE" } };
  public static Map<TAG_CARD_SET, string> s_cardSetNamesInitials = new Map<TAG_CARD_SET, string>() { { TAG_CARD_SET.FP1, "GLOBAL_CARD_SET_NAXX_SEARCHABLE_SHORTHAND_NAMES" }, { TAG_CARD_SET.PE1, "GLOBAL_CARD_SET_GVG_SEARCHABLE_SHORTHAND_NAMES" }, { TAG_CARD_SET.BRM, "GLOBAL_CARD_SET_BRM_SEARCHABLE_SHORTHAND_NAMES" }, { TAG_CARD_SET.TGT, "GLOBAL_CARD_SET_TGT_SEARCHABLE_SHORTHAND_NAMES" }, { TAG_CARD_SET.LOE, "GLOBAL_CARD_SET_LOE_SEARCHABLE_SHORTHAND_NAMES" }, { TAG_CARD_SET.OG, "GLOBAL_CARD_SET_OG_SEARCHABLE_SHORTHAND_NAMES" }, { TAG_CARD_SET.GANGS, "GLOBAL_CARD_SET_GANGS_SEARCHABLE_SHORTHAND_NAMES" } };
  public static Map<TAG_CARDTYPE, string> s_cardTypeNames = new Map<TAG_CARDTYPE, string>() { { TAG_CARDTYPE.HERO, "GLOBAL_CARDTYPE_HERO" }, { TAG_CARDTYPE.MINION, "GLOBAL_CARDTYPE_MINION" }, { TAG_CARDTYPE.SPELL, "GLOBAL_CARDTYPE_SPELL" }, { TAG_CARDTYPE.ENCHANTMENT, "GLOBAL_CARDTYPE_ENCHANTMENT" }, { TAG_CARDTYPE.WEAPON, "GLOBAL_CARDTYPE_WEAPON" }, { TAG_CARDTYPE.ITEM, "GLOBAL_CARDTYPE_ITEM" }, { TAG_CARDTYPE.TOKEN, "GLOBAL_CARDTYPE_TOKEN" }, { TAG_CARDTYPE.HERO_POWER, "GLOBAL_CARDTYPE_HEROPOWER" } };
  public static Map<TAG_MULTI_CLASS_GROUP, string> s_multiClassGroupeNames = new Map<TAG_MULTI_CLASS_GROUP, string>() { { TAG_MULTI_CLASS_GROUP.GRIMY_GOONS, "GLOBAL_KEYWORD_GRIMY_GOONS" }, { TAG_MULTI_CLASS_GROUP.JADE_LOTUS, "GLOBAL_KEYWORD_JADE_LOTUS" }, { TAG_MULTI_CLASS_GROUP.KABAL, "GLOBAL_KEYWORD_KABAL" } };
  public const string s_UnknownName = "UNKNOWN";
  private const string NUMBER_PATTERN = "(?:[0-9]+,)*[0-9]+";

  public static void LoadAll()
  {
    float realtimeSinceStartup1 = Time.realtimeSinceStartup;
    foreach (int num in Enum.GetValues(typeof (GameStringCategory)))
    {
      GameStringCategory cat = (GameStringCategory) num;
      if (cat != GameStringCategory.INVALID)
        GameStrings.LoadCategory(cat);
    }
    float realtimeSinceStartup2 = Time.realtimeSinceStartup;
    Log.Cameron.Print(string.Format("Loading All GameStrings took {0}s)", (object) (float) ((double) realtimeSinceStartup2 - (double) realtimeSinceStartup1)));
  }

  public static void ReloadAll()
  {
    float realtimeSinceStartup1 = Time.realtimeSinceStartup;
    foreach (int num in Enum.GetValues(typeof (GameStringCategory)))
    {
      GameStringCategory gameStringCategory = (GameStringCategory) num;
      if (gameStringCategory != GameStringCategory.INVALID)
      {
        if (GameStrings.s_tables.ContainsKey(gameStringCategory))
          GameStrings.UnloadCategory(gameStringCategory);
        GameStrings.LoadCategory(gameStringCategory);
      }
    }
    float realtimeSinceStartup2 = Time.realtimeSinceStartup;
    Log.Cameron.Print(string.Format("Reloading All GameStrings took {0}s)", (object) (float) ((double) realtimeSinceStartup2 - (double) realtimeSinceStartup1)));
  }

  public static void WillReset()
  {
    GameStrings.ReloadAll();
  }

  public static string GetAssetPath(Locale locale, string fileName)
  {
    return FileUtils.GetAssetPath(string.Format("Strings/{0}/{1}", (object) locale, (object) fileName));
  }

  public static bool HasKey(string key)
  {
    return GameStrings.Find(key) != null;
  }

  public static string Get(string key)
  {
    string str = GameStrings.Find(key);
    if (str == null)
      return key;
    return GameStrings.ParseLanguageRules(str);
  }

  public static string Format(string key, params object[] args)
  {
    string format = GameStrings.Find(key);
    if (format == null)
      return key;
    return GameStrings.ParseLanguageRules(string.Format((IFormatProvider) Localization.GetCultureInfo(), format, args));
  }

  public static string FormatPlurals(string key, GameStrings.PluralNumber[] pluralNumbers, params object[] args)
  {
    string format = GameStrings.Find(key);
    if (format == null)
      return key;
    return GameStrings.ParseLanguageRules(string.Format((IFormatProvider) Localization.GetCultureInfo(), format, args), pluralNumbers);
  }

  public static string ParseLanguageRules(string str)
  {
    str = GameStrings.ParseLanguageRule4(str, (GameStrings.PluralNumber[]) null);
    return str;
  }

  public static string ParseLanguageRules(string str, GameStrings.PluralNumber[] pluralNumbers)
  {
    str = GameStrings.ParseLanguageRule4(str, pluralNumbers);
    return str;
  }

  public static bool HasClassName(TAG_CLASS tag)
  {
    return GameStrings.s_classNames.ContainsKey(tag);
  }

  public static string GetClassName(TAG_CLASS tag)
  {
    string key = (string) null;
    if (GameStrings.s_classNames.TryGetValue(tag, out key))
      return GameStrings.Get(key);
    return "UNKNOWN";
  }

  public static string GetClassNameKey(TAG_CLASS tag)
  {
    string str = (string) null;
    if (GameStrings.s_classNames.TryGetValue(tag, out str))
      return str;
    return (string) null;
  }

  private static KeywordTextDbfRecord GetKeywordTextRecord(GAME_TAG tag)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return GameDbf.KeywordText.GetRecord(new Predicate<KeywordTextDbfRecord>(new GameStrings.\u003CGetKeywordTextRecord\u003Ec__AnonStorey446() { tag = tag }.\u003C\u003Em__2FC));
  }

  public static bool HasKeywordName(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    return keywordTextRecord != null && !string.IsNullOrEmpty(keywordTextRecord.Name);
  }

  public static string GetKeywordName(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    if (keywordTextRecord == null || keywordTextRecord.Name == null)
      return "UNKNOWN";
    return GameStrings.Get(keywordTextRecord.Name);
  }

  public static string GetKeywordNameKey(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    if (keywordTextRecord == null || keywordTextRecord.Name == null)
      return "UNKNOWN";
    return keywordTextRecord.Name;
  }

  public static bool HasKeywordText(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    return keywordTextRecord != null && !string.IsNullOrEmpty(keywordTextRecord.Text);
  }

  public static string GetKeywordText(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    if (keywordTextRecord == null || keywordTextRecord.Text == null)
      return "UNKNOWN";
    return GameStrings.Get(keywordTextRecord.Text);
  }

  public static string GetKeywordTextKey(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    if (keywordTextRecord == null || keywordTextRecord.Text == null)
      return "UNKNOWN";
    return keywordTextRecord.Text;
  }

  public static bool HasRefKeywordText(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    return keywordTextRecord != null && !string.IsNullOrEmpty(keywordTextRecord.RefText);
  }

  public static string GetRefKeywordText(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    if (keywordTextRecord == null || keywordTextRecord.RefText == null)
      return "UNKNOWN";
    return GameStrings.Get(keywordTextRecord.RefText);
  }

  public static string GetRefKeywordTextKey(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    if (keywordTextRecord == null || keywordTextRecord.RefText == null)
      return "UNKNOWN";
    return keywordTextRecord.RefText;
  }

  public static bool HasCollectionKeywordText(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    return keywordTextRecord != null && !string.IsNullOrEmpty(keywordTextRecord.CollectionText);
  }

  public static string GetCollectionKeywordText(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    if (keywordTextRecord == null || keywordTextRecord.CollectionText == null)
      return "UNKNOWN";
    return GameStrings.Get(keywordTextRecord.CollectionText);
  }

  public static string GetCollectionKeywordTextKey(GAME_TAG tag)
  {
    KeywordTextDbfRecord keywordTextRecord = GameStrings.GetKeywordTextRecord(tag);
    if (keywordTextRecord == null || keywordTextRecord.CollectionText == null)
      return "UNKNOWN";
    return keywordTextRecord.CollectionText;
  }

  public static bool HasRarityText(TAG_RARITY tag)
  {
    return GameStrings.s_rarityNames.ContainsKey(tag);
  }

  public static string GetRarityText(TAG_RARITY tag)
  {
    string key = (string) null;
    if (GameStrings.s_rarityNames.TryGetValue(tag, out key))
      return GameStrings.Get(key);
    return "UNKNOWN";
  }

  public static string GetRarityTextKey(TAG_RARITY tag)
  {
    string str = (string) null;
    if (GameStrings.s_rarityNames.TryGetValue(tag, out str))
      return str;
    return (string) null;
  }

  public static bool HasRaceName(TAG_RACE tag)
  {
    return GameStrings.s_raceNames.ContainsKey(tag);
  }

  public static string GetRaceName(TAG_RACE tag)
  {
    string key = (string) null;
    if (GameStrings.s_raceNames.TryGetValue(tag, out key))
      return GameStrings.Get(key);
    return "UNKNOWN";
  }

  public static string GetRaceNameKey(TAG_RACE tag)
  {
    string str = (string) null;
    if (GameStrings.s_raceNames.TryGetValue(tag, out str))
      return str;
    return (string) null;
  }

  public static bool HasCardTypeName(TAG_CARDTYPE tag)
  {
    return GameStrings.s_cardTypeNames.ContainsKey(tag);
  }

  public static string GetCardTypeName(TAG_CARDTYPE tag)
  {
    string key = (string) null;
    if (GameStrings.s_cardTypeNames.TryGetValue(tag, out key))
      return GameStrings.Get(key);
    return "UNKNOWN";
  }

  public static string GetCardTypeNameKey(TAG_CARDTYPE tag)
  {
    string str = (string) null;
    if (GameStrings.s_cardTypeNames.TryGetValue(tag, out str))
      return str;
    return (string) null;
  }

  public static bool HasCardSetName(TAG_CARD_SET tag)
  {
    return GameStrings.s_cardSetNames.ContainsKey(tag);
  }

  public static string GetCardSetName(TAG_CARD_SET tag)
  {
    string key = (string) null;
    if (GameStrings.s_cardSetNames.TryGetValue(tag, out key))
      return GameStrings.Get(key);
    return "UNKNOWN";
  }

  public static string GetCardSetNameKey(TAG_CARD_SET tag)
  {
    string str = (string) null;
    if (GameStrings.s_cardSetNames.TryGetValue(tag, out str))
      return str;
    return (string) null;
  }

  public static bool HasCardSetNameShortened(TAG_CARD_SET tag)
  {
    return GameStrings.s_cardSetNamesShortened.ContainsKey(tag);
  }

  public static string GetCardSetNameShortened(TAG_CARD_SET tag)
  {
    string key = (string) null;
    if (GameStrings.s_cardSetNamesShortened.TryGetValue(tag, out key))
      return GameStrings.Get(key);
    return "UNKNOWN";
  }

  public static string GetCardSetNameKeyShortened(TAG_CARD_SET tag)
  {
    string str = (string) null;
    if (GameStrings.s_cardSetNamesShortened.TryGetValue(tag, out str))
      return str;
    return (string) null;
  }

  public static bool HasCardSetNameInitials(TAG_CARD_SET tag)
  {
    return GameStrings.s_cardSetNamesInitials.ContainsKey(tag);
  }

  public static string GetCardSetNameInitials(TAG_CARD_SET tag)
  {
    string key = (string) null;
    if (GameStrings.s_cardSetNamesInitials.TryGetValue(tag, out key))
      return GameStrings.Get(key);
    return "UNKNOWN";
  }

  public static bool HasMultiClassGroupName(TAG_MULTI_CLASS_GROUP tag)
  {
    return GameStrings.s_multiClassGroupeNames.ContainsKey(tag);
  }

  public static string GetMultiClassGroupName(TAG_MULTI_CLASS_GROUP tag)
  {
    string key = (string) null;
    if (GameStrings.s_multiClassGroupeNames.TryGetValue(tag, out key))
      return GameStrings.Get(key);
    return "UNKNOWN";
  }

  public static string GetRandomTip(TipCategory tipCategory)
  {
    int num = 0;
    List<string> stringList = new List<string>();
    while (true)
    {
      string key1 = string.Format("GLUE_TIP_{0}_{1}", (object) tipCategory, (object) num);
      string str1 = GameStrings.Get(key1);
      if (!str1.Equals(key1))
      {
        if (UniversalInputManager.Get().IsTouchMode())
        {
          string key2 = key1 + "_TOUCH";
          string str2 = GameStrings.Get(key2);
          if (!str2.Equals(key2))
            str1 = str2;
          if ((bool) UniversalInputManager.UsePhoneUI)
          {
            string key3 = key1 + "_PHONE";
            string str3 = GameStrings.Get(key3);
            if (!str3.Equals(key3))
              str1 = str3;
          }
        }
        stringList.Add(str1);
        ++num;
      }
      else
        break;
    }
    if (stringList.Count == 0)
    {
      Debug.LogError((object) string.Format("GameStrings.GetRandomTip() - no tips in category {0}", (object) tipCategory));
      return "UNKNOWN";
    }
    int index = UnityEngine.Random.Range(0, stringList.Count);
    return stringList[index];
  }

  public static string GetTip(TipCategory tipCategory, int progress, TipCategory randomTipCategory = TipCategory.DEFAULT)
  {
    int num = 0;
    List<string> stringList = new List<string>();
    while (true)
    {
      string key1 = string.Format("GLUE_TIP_{0}_{1}", (object) tipCategory, (object) num);
      string str1 = GameStrings.Get(key1);
      if (!str1.Equals(key1))
      {
        if (UniversalInputManager.Get().IsTouchMode())
        {
          string key2 = key1 + "_TOUCH";
          string str2 = GameStrings.Get(key2);
          if (!str2.Equals(key2))
            str1 = str2;
          if ((bool) UniversalInputManager.UsePhoneUI)
          {
            string key3 = key1 + "_PHONE";
            string str3 = GameStrings.Get(key3);
            if (!str3.Equals(key3))
              str1 = str3;
          }
        }
        stringList.Add(str1);
        ++num;
      }
      else
        break;
    }
    if (progress < stringList.Count)
      return stringList[progress];
    return GameStrings.GetRandomTip(randomTipCategory);
  }

  private static bool LoadCategory(GameStringCategory cat)
  {
    if (GameStrings.s_tables.ContainsKey(cat))
    {
      Debug.LogWarning((object) string.Format("GameStrings.LoadCategory() - {0} is already loaded", (object) cat));
      return false;
    }
    GameStringTable table = new GameStringTable();
    if (!table.Load(cat))
    {
      Debug.LogError((object) string.Format("GameStrings.LoadCategory() - {0} failed to load", (object) cat));
      return false;
    }
    if (ApplicationMgr.IsInternal())
      GameStrings.CheckConflicts(table);
    GameStrings.s_tables.Add(cat, table);
    return true;
  }

  private static bool UnloadCategory(GameStringCategory cat)
  {
    if (GameStrings.s_tables.Remove(cat))
      return true;
    Debug.LogWarning((object) string.Format("GameStrings.UnloadCategory() - {0} was never loaded", (object) cat));
    return false;
  }

  private static void CheckConflicts(GameStringTable table)
  {
    Map<string, string>.KeyCollection keys = table.GetAll().Keys;
    GameStringCategory category = table.GetCategory();
    using (Map<GameStringCategory, GameStringTable>.ValueCollection.Enumerator enumerator1 = GameStrings.s_tables.Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        GameStringTable current1 = enumerator1.Current;
        using (Map<string, string>.KeyCollection.Enumerator enumerator2 = keys.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            string current2 = enumerator2.Current;
            if (current1.Get(current2) != null)
              Error.AddDevWarning("GameStrings Error", string.Format("GameStrings.CheckConflicts() - Tag {0} is used in {1} and {2}. All tags must be unique.", (object) current2, (object) category, (object) current1.GetCategory()));
          }
        }
      }
    }
  }

  private static string Find(string key)
  {
    using (Map<GameStringCategory, GameStringTable>.ValueCollection.Enumerator enumerator = GameStrings.s_tables.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string str = enumerator.Current.Get(key);
        if (str != null)
          return str;
      }
    }
    return (string) null;
  }

  private static string[] ParseLanguageRuleArgs(string str, int ruleIndex, out int argStartIndex, out int argEndIndex)
  {
    argStartIndex = -1;
    argEndIndex = -1;
    argStartIndex = str.IndexOf('(', ruleIndex + 2);
    if (argStartIndex < 0)
    {
      Debug.LogWarning((object) string.Format("GameStrings.ParseLanguageRuleArgs() - failed to parse '(' for rule at index {0} in string {1}", (object) ruleIndex, (object) str));
      return (string[]) null;
    }
    argEndIndex = str.IndexOf(')', argStartIndex + 1);
    if (argEndIndex < 0)
    {
      Debug.LogWarning((object) string.Format("GameStrings.ParseLanguageRuleArgs() - failed to parse ')' for rule at index {0} in string {1}", (object) ruleIndex, (object) str));
      return (string[]) null;
    }
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.Append(str, argStartIndex + 1, argEndIndex - argStartIndex - 1);
    string input = stringBuilder.ToString();
    MatchCollection matchCollection = Regex.Matches(input, "(?:[0-9]+,)*[0-9]+");
    if (matchCollection.Count > 0)
    {
      stringBuilder.Remove(0, stringBuilder.Length);
      int startIndex = 0;
      foreach (Match match in matchCollection)
      {
        stringBuilder.Append(input, startIndex, match.Index - startIndex);
        stringBuilder.Append('0', match.Length);
        startIndex = match.Index + match.Length;
      }
      stringBuilder.Append(input, startIndex, input.Length - startIndex);
      input = stringBuilder.ToString();
    }
    string[] strArray = input.Split(GameStrings.LANGUAGE_RULE_ARG_DELIMITERS);
    int num1 = 0;
    for (int index = 0; index < strArray.Length; ++index)
    {
      string str1 = strArray[index];
      if (matchCollection.Count > 0)
      {
        stringBuilder.Remove(0, stringBuilder.Length);
        int startIndex = 0;
        foreach (Match match in matchCollection)
        {
          if (match.Index >= num1 && match.Index < num1 + str1.Length)
          {
            int num2 = match.Index - num1;
            stringBuilder.Append(str1, startIndex, num2 - startIndex);
            stringBuilder.Append(match.Value);
            startIndex = num2 + match.Length;
          }
        }
        stringBuilder.Append(str1, startIndex, str1.Length - startIndex);
        str1 = stringBuilder.ToString();
        num1 += str1.Length + 1;
      }
      string str2 = str1.Trim();
      strArray[index] = str2;
    }
    return strArray;
  }

  private static string ParseLanguageRule4(string str, GameStrings.PluralNumber[] pluralNumbers = null)
  {
    StringBuilder stringBuilder = (StringBuilder) null;
    int? nullable = new int?();
    int startIndex1 = 0;
    int num = 0;
    for (int ruleIndex = str.IndexOf("|4"); ruleIndex >= 0; ruleIndex = str.IndexOf("|4", ruleIndex + 2))
    {
      ++num;
      int argStartIndex;
      int argEndIndex;
      string[] languageRuleArgs = GameStrings.ParseLanguageRuleArgs(str, ruleIndex, out argStartIndex, out argEndIndex);
      if (languageRuleArgs != null)
      {
        int startIndex2 = startIndex1;
        int length = ruleIndex - startIndex1;
        string betweenRulesStr = str.Substring(startIndex2, length);
        GameStrings.PluralNumber pluralNumber = (GameStrings.PluralNumber) null;
        if (pluralNumbers != null)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: reference to a compiler-generated method
          pluralNumber = Array.Find<GameStrings.PluralNumber>(pluralNumbers, new Predicate<GameStrings.PluralNumber>(new GameStrings.\u003CParseLanguageRule4\u003Ec__AnonStorey447()
          {
            pluralArgIndex = num - 1
          }.\u003C\u003Em__2FD));
        }
        if (pluralNumber != null)
        {
          nullable = new int?(pluralNumber.m_number);
        }
        else
        {
          int number;
          if (GameStrings.ParseLanguageRule4Number(languageRuleArgs, betweenRulesStr, out number))
            nullable = new int?(number);
          else if (!nullable.HasValue)
          {
            Debug.LogWarning((object) string.Format("GameStrings.ParseLanguageRule4() - failed to parse a number in substring \"{0}\" (indexes {1}-{2}) for rule {3} in string \"{4}\"", (object) betweenRulesStr, (object) startIndex2, (object) length, (object) num, (object) str));
            continue;
          }
        }
        int pluralIndex = GameStrings.GetPluralIndex(nullable.Value);
        if (pluralIndex >= languageRuleArgs.Length)
        {
          Debug.LogWarning((object) string.Format("GameStrings.ParseLanguageRule4() - not enough arguments for rule {0} in string \"{1}\"", (object) num, (object) str));
        }
        else
        {
          string str1 = languageRuleArgs[pluralIndex];
          if (stringBuilder == null)
            stringBuilder = new StringBuilder();
          stringBuilder.Append(betweenRulesStr);
          stringBuilder.Append(str1);
          startIndex1 = argEndIndex + 1;
        }
        if (pluralNumber != null && pluralNumber.m_useForOnlyThisIndex)
          nullable = new int?();
      }
    }
    if (stringBuilder == null)
      return str;
    stringBuilder.Append(str, startIndex1, str.Length - startIndex1);
    return stringBuilder.ToString();
  }

  private static bool ParseLanguageRule4Number(string[] args, string betweenRulesStr, out int number)
  {
    if (GameStrings.ParseLanguageRule4Number_Foreward(args[0], out number) || GameStrings.ParseLanguageRule4Number_Backward(betweenRulesStr, out number))
      return true;
    number = 0;
    return false;
  }

  private static bool ParseLanguageRule4Number_Foreward(string str, out int number)
  {
    number = 0;
    Match match = Regex.Match(str, "(?:[0-9]+,)*[0-9]+");
    return match.Success && GeneralUtils.TryParseInt(match.Value, out number);
  }

  private static bool ParseLanguageRule4Number_Backward(string str, out int number)
  {
    number = 0;
    MatchCollection matchCollection = Regex.Matches(str, "(?:[0-9]+,)*[0-9]+");
    return matchCollection.Count != 0 && GeneralUtils.TryParseInt(matchCollection[matchCollection.Count - 1].Value, out number);
  }

  private static int GetPluralIndex(int number)
  {
    switch (Localization.GetLocale())
    {
      case Locale.frFR:
      case Locale.koKR:
      case Locale.zhTW:
      case Locale.zhCN:
        return number <= 1 ? 0 : 1;
      case Locale.ruRU:
        switch (number % 100)
        {
          case 11:
          case 12:
          case 13:
          case 14:
            return 2;
          default:
            switch (number % 10)
            {
              case 1:
                return 0;
              case 2:
              case 3:
              case 4:
                return 1;
              default:
                return 2;
            }
        }
      case Locale.plPL:
        if (number == 1)
          return 0;
        if (number == 0)
          return 2;
        switch (number % 100)
        {
          case 11:
          case 12:
          case 13:
          case 14:
            return 2;
          default:
            switch (number % 10)
            {
              case 2:
              case 3:
              case 4:
                return 1;
              default:
                return 2;
            }
        }
      default:
        return number == 1 ? 0 : 1;
    }
  }

  public class PluralNumber
  {
    public int m_index;
    public int m_number;
    public bool m_useForOnlyThisIndex;
  }
}
