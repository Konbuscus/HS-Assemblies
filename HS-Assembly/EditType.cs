﻿// Decompiled with JetBrains decompiler
// Type: EditType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum EditType
{
  DEFAULT,
  TEXT_AREA,
  MATERIAL,
  TEXTURE,
  CARD_TEXTURE,
  SOUND_PREFAB,
  AUDIO_CLIP,
  SPELL,
  CARD_SOUND_SPELL,
  GAME_OBJECT,
  SCENE_OBJECT,
  ACTOR,
}
