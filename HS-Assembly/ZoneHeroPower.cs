﻿// Decompiled with JetBrains decompiler
// Type: ZoneHeroPower
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class ZoneHeroPower : Zone
{
  private void Awake()
  {
  }

  public override string ToString()
  {
    return string.Format("{0} (Hero Power)", (object) base.ToString());
  }

  public override bool CanAcceptTags(int controllerId, TAG_ZONE zoneTag, TAG_CARDTYPE cardType)
  {
    return base.CanAcceptTags(controllerId, zoneTag, cardType) && cardType == TAG_CARDTYPE.HERO_POWER;
  }
}
