﻿// Decompiled with JetBrains decompiler
// Type: CustomEditTypeUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public static class CustomEditTypeUtils
{
  public static string GetExtension(EditType type)
  {
    switch (type)
    {
      case EditType.MATERIAL:
        return "mat";
      case EditType.TEXTURE:
      case EditType.CARD_TEXTURE:
        return "psd";
      default:
        return "prefab";
    }
  }
}
