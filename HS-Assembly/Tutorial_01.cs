﻿// Decompiled with JetBrains decompiler
// Type: Tutorial_01
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Tutorial_01 : TutorialEntity
{
  private bool tooltipsDisabled = true;
  private string textToShowForAttackTip = GameStrings.Get("TUTORIAL01_HELP_02");
  private PlatformDependentValue<float> m_gemScale = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 1.75f, Phone = 1.2f };
  private PlatformDependentValue<Vector3> m_attackTooltipPosition = new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(-2.15f, 0.0f, -0.62f), Phone = new Vector3(-3.5f, 0.0f, -0.62f) };
  private PlatformDependentValue<Vector3> m_healthTooltipPosition = new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(2.05f, 0.0f, -0.62f), Phone = new Vector3(3.25f, 0.0f, -0.62f) };
  private PlatformDependentValue<Vector3> m_heroHealthTooltipPosition = new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(2.4f, 0.3f, -0.8f), Phone = new Vector3(3.5f, 0.3f, 0.6f) };
  private Notification endTurnNotifier;
  private Notification handBounceArrow;
  private Notification handFadeArrow;
  private Notification noFireballPopup;
  private Notification attackWithYourMinion;
  private Notification crushThisGnoll;
  private Notification freeCardsPopup;
  private TooltipPanel attackHelpPanel;
  private TooltipPanel healthHelpPanel;
  private Card mousedOverCard;
  private GameObject costLabel;
  private GameObject attackLabel;
  private GameObject healthLabel;
  private Card firstMurlocCard;
  private Card firstRaptorCard;
  private int numTimesTextSwapStarted;
  private GameObject startingPack;
  private bool packOpened;
  private bool announcerIsFinishedYapping;
  private bool firstAttackFinished;
  private bool m_jainaSpeaking;
  private bool m_isShowingAttackHelpPanel;

  public Tutorial_01()
  {
    MulliganManager.Get().ForceMulliganActive(true);
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_TUTORIAL_01_ANNOUNCER_01");
    this.PreloadSound("VO_TUTORIAL_01_ANNOUNCER_02");
    this.PreloadSound("VO_TUTORIAL_01_ANNOUNCER_03");
    this.PreloadSound("VO_TUTORIAL_01_ANNOUNCER_04");
    this.PreloadSound("VO_TUTORIAL_01_ANNOUNCER_05");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_13_10");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_01_01");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_02_02");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_03_03");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_20_16");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_05_05");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_06_06");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_07_07");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_21_17");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_09_08");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_15_11");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_16_12");
    this.PreloadSound("VO_TUTORIAL_JAINA_02_55_ALT2");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_10_09");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_17_13");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_18_14");
    this.PreloadSound("VO_TUTORIAL_01_JAINA_19_15");
    this.PreloadSound("VO_TUTORIAL_01_HOGGER_01_01");
    this.PreloadSound("VO_TUTORIAL_01_HOGGER_02_02");
    this.PreloadSound("VO_TUTORIAL_01_HOGGER_03_03");
    this.PreloadSound("VO_TUTORIAL_01_HOGGER_04_04");
    this.PreloadSound("VO_TUTORIAL_01_HOGGER_06_06_ALT");
    this.PreloadSound("VO_TUTORIAL_01_HOGGER_08_08_ALT");
    this.PreloadSound("VO_TUTORIAL_01_HOGGER_09_09_ALT");
    this.PreloadSound("VO_TUTORIAL_01_HOGGER_11_11");
  }

  public override void NotifyOfGameOver(TAG_PLAYSTATE gameResult)
  {
    base.NotifyOfGameOver(gameResult);
    if ((Object) this.attackHelpPanel != (Object) null)
    {
      Object.Destroy((Object) this.attackHelpPanel.gameObject);
      this.attackHelpPanel = (TooltipPanel) null;
    }
    if ((Object) this.healthHelpPanel != (Object) null)
    {
      Object.Destroy((Object) this.healthHelpPanel.gameObject);
      this.healthHelpPanel = (TooltipPanel) null;
    }
    this.EnsureCardGemsAreOnTheCorrectLayer();
    if (gameResult == TAG_PLAYSTATE.WON)
    {
      this.SetTutorialProgress(TutorialProgress.HOGGER_COMPLETE);
      this.PlaySound("VO_TUTORIAL_01_HOGGER_11_11", 1f, true, false);
    }
    else if (gameResult == TAG_PLAYSTATE.TIED)
      this.PlaySound("VO_TUTORIAL_01_HOGGER_11_11", 1f, true, false);
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    InputManager.Get().RemovePhoneHandShownListener(new InputManager.PhoneHandShownCallback(this.OnPhoneHandShown));
    InputManager.Get().RemovePhoneHandHiddenListener(new InputManager.PhoneHandHiddenCallback(this.OnPhoneHandHidden));
  }

  private void EnsureCardGemsAreOnTheCorrectLayer()
  {
    List<Card> cardList = new List<Card>();
    cardList.AddRange((IEnumerable<Card>) GameState.Get().GetFriendlySidePlayer().GetBattlefieldZone().GetCards());
    cardList.AddRange((IEnumerable<Card>) GameState.Get().GetOpposingSidePlayer().GetBattlefieldZone().GetCards());
    cardList.Add(GameState.Get().GetFriendlySidePlayer().GetHeroCard());
    cardList.Add(GameState.Get().GetOpposingSidePlayer().GetHeroCard());
    using (List<Card>.Enumerator enumerator = cardList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (!((Object) current == (Object) null) && !((Object) current.GetActor() == (Object) null))
        {
          if ((Object) current.GetActor().GetAttackObject() != (Object) null)
            SceneUtils.SetLayer(current.GetActor().GetAttackObject().gameObject, GameLayer.Default);
          if ((Object) current.GetActor().GetHealthObject() != (Object) null)
            SceneUtils.SetLayer(current.GetActor().GetHealthObject().gameObject, GameLayer.Default);
        }
      }
    }
  }

  public override void NotifyOfCardGrabbed(Entity entity)
  {
    if (this.GetTag(GAME_TAG.TURN) == 2 || entity.GetCardId() == "CS2_025")
      BoardTutorial.Get().EnableHighlight(true);
    this.NukeNumberLabels();
  }

  public override void NotifyOfCardDropped(Entity entity)
  {
    if (this.GetTag(GAME_TAG.TURN) != 2 && !(entity.GetCardId() == "CS2_025"))
      return;
    BoardTutorial.Get().EnableHighlight(false);
  }

  public override bool NotifyOfEndTurnButtonPushed()
  {
    Network.Options optionsPacket = GameState.Get().GetOptionsPacket();
    if (optionsPacket != null && optionsPacket.List != null && optionsPacket.List.Count == 1)
    {
      NotificationManager.Get().DestroyAllArrows();
      return true;
    }
    if ((Object) this.endTurnNotifier != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.endTurnNotifier);
    Vector3 position1 = EndTurnButton.Get().transform.position;
    Vector3 position2 = new Vector3(position1.x - 3f, position1.y, position1.z);
    string key = "TUTORIAL_NO_ENDTURN_ATK";
    if (!GameState.Get().GetFriendlySidePlayer().HasReadyAttackers())
      key = "TUTORIAL_NO_ENDTURN";
    this.endTurnNotifier = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, position2, TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get(key), true);
    NotificationManager.Get().DestroyNotification(this.endTurnNotifier, 2.5f);
    return false;
  }

  public override bool NotifyOfPlayError(PlayErrors.ErrorType error, Entity errorSource)
  {
    return error == PlayErrors.ErrorType.REQ_ATTACK_GREATER_THAN_0 && errorSource.GetCardId() == "TU4a_006";
  }

  public override void NotifyOfTargetModeCancelled()
  {
    if ((Object) this.crushThisGnoll == (Object) null)
      return;
    NotificationManager.Get().DestroyAllPopUps();
    if ((Object) this.firstRaptorCard == (Object) null || !(this.firstRaptorCard.GetZone() is ZonePlay))
      return;
    this.ShowAttackWithYourMinionPopup();
  }

  public override bool NotifyOfBattlefieldCardClicked(Entity clickedEntity, bool wasInTargetMode)
  {
    if (this.GetTag(GAME_TAG.TURN) == 4)
    {
      if (clickedEntity.GetCardId() == "CS2_168")
      {
        if (!wasInTargetMode && !this.firstAttackFinished)
        {
          if ((Object) this.crushThisGnoll != (Object) null)
            NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.crushThisGnoll);
          NotificationManager.Get().DestroyAllPopUps();
          Vector3 position = GameState.Get().GetOpposingSidePlayer().GetBattlefieldZone().GetFirstCard().transform.position;
          this.crushThisGnoll = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(position.x - 3f, position.y, position.z), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("TUTORIAL01_HELP_03"), true);
          this.crushThisGnoll.ShowPopUpArrow(Notification.PopUpArrowDirection.Right);
          ++this.numTimesTextSwapStarted;
          Gameplay.Get().StartCoroutine(this.WaitAndThenHide(this.numTimesTextSwapStarted));
        }
      }
      else if (clickedEntity.GetCardId() == "TU4a_002" && wasInTargetMode)
      {
        if ((Object) this.crushThisGnoll != (Object) null)
          NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.crushThisGnoll);
        NotificationManager.Get().DestroyAllPopUps();
        this.firstAttackFinished = true;
      }
    }
    else if (this.GetTag(GAME_TAG.TURN) == 6 && clickedEntity.GetCardId() == "TU4a_001" && wasInTargetMode)
      NotificationManager.Get().DestroyAllPopUps();
    if (wasInTargetMode && (Object) InputManager.Get().GetHeldCard() != (Object) null && InputManager.Get().GetHeldCard().GetEntity().GetCardId() == "CS2_029")
    {
      if (clickedEntity.IsControlledByLocalUser())
      {
        this.ShowDontFireballYourselfPopup(clickedEntity.GetCard().transform.position);
        return false;
      }
      if (clickedEntity.GetCardId() == "TU4a_003" && this.GetTag(GAME_TAG.TURN) >= 8)
      {
        if ((Object) this.noFireballPopup != (Object) null)
          NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.noFireballPopup);
        Vector3 position = clickedEntity.GetCard().transform.position;
        this.noFireballPopup = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(position.x - 3f, position.y, position.z), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("TUTORIAL01_HELP_08"), true);
        NotificationManager.Get().DestroyNotification(this.noFireballPopup, 3f);
        return false;
      }
    }
    return true;
  }

  [DebuggerHidden]
  private IEnumerator WaitAndThenHide(int numTimesStarted)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CWaitAndThenHide\u003Ec__Iterator204() { numTimesStarted = numTimesStarted, \u003C\u0024\u003EnumTimesStarted = numTimesStarted, \u003C\u003Ef__this = this };
  }

  public override bool NotifyOfCardTooltipDisplayShow(Card card)
  {
    if (GameState.Get().IsGameOver())
      return false;
    Entity entity = card.GetEntity();
    if (entity.IsMinion())
    {
      if ((Object) this.attackHelpPanel == (Object) null)
      {
        this.m_isShowingAttackHelpPanel = true;
        this.ShowAttackTooltip(card);
        Gameplay.Get().StartCoroutine(this.ShowHealthTooltipAfterWait(card));
      }
      return false;
    }
    if (!entity.IsHero())
      return true;
    if ((Object) this.healthHelpPanel == (Object) null)
      this.ShowHealthTooltip(card);
    return false;
  }

  private void ShowAttackTooltip(Card card)
  {
    SceneUtils.SetLayer(card.GetActor().GetAttackObject().gameObject, GameLayer.Tooltip);
    Vector3 position = card.transform.position;
    Vector3 attackTooltipPosition = (Vector3) this.m_attackTooltipPosition;
    Vector3 vector3 = new Vector3(position.x + attackTooltipPosition.x, position.y + attackTooltipPosition.y, position.z + attackTooltipPosition.z);
    this.attackHelpPanel = TooltipPanelManager.Get().CreateKeywordPanel(0);
    this.attackHelpPanel.Reset();
    this.attackHelpPanel.SetScale((float) TooltipPanel.GAMEPLAY_SCALE);
    this.attackHelpPanel.Initialize(GameStrings.Get("GLOBAL_ATTACK"), GameStrings.Get("TUTORIAL01_HELP_12"));
    this.attackHelpPanel.transform.position = vector3;
    RenderUtils.SetAlpha(this.attackHelpPanel.gameObject, 0.0f);
    iTween.FadeTo(this.attackHelpPanel.gameObject, iTween.Hash((object) "alpha", (object) 1, (object) "time", (object) 0.25f));
    card.GetActor().GetAttackObject().Enlarge((float) this.m_gemScale);
  }

  [DebuggerHidden]
  private IEnumerator ShowHealthTooltipAfterWait(Card card)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CShowHealthTooltipAfterWait\u003Ec__Iterator205() { card = card, \u003C\u0024\u003Ecard = card, \u003C\u003Ef__this = this };
  }

  private void ShowHealthTooltip(Card card)
  {
    SceneUtils.SetLayer(card.GetActor().GetHealthObject().gameObject, GameLayer.Tooltip);
    Vector3 position = card.transform.position;
    Vector3 healthTooltipPosition = (Vector3) this.m_healthTooltipPosition;
    if (card.GetEntity().IsHero())
    {
      healthTooltipPosition = (Vector3) this.m_heroHealthTooltipPosition;
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        if (!card.GetEntity().IsControlledByLocalUser())
          healthTooltipPosition.z -= 0.75f;
        else if (Localization.GetLocale() == Locale.ruRU)
          ++healthTooltipPosition.z;
      }
    }
    Vector3 vector3 = new Vector3(position.x + healthTooltipPosition.x, position.y + healthTooltipPosition.y, position.z + healthTooltipPosition.z);
    this.healthHelpPanel = TooltipPanelManager.Get().CreateKeywordPanel(0);
    this.healthHelpPanel.Reset();
    this.healthHelpPanel.SetScale((float) TooltipPanel.GAMEPLAY_SCALE);
    this.healthHelpPanel.Initialize(GameStrings.Get("GLOBAL_HEALTH"), GameStrings.Get("TUTORIAL01_HELP_13"));
    this.healthHelpPanel.transform.position = vector3;
    RenderUtils.SetAlpha(this.healthHelpPanel.gameObject, 0.0f);
    iTween.FadeTo(this.healthHelpPanel.gameObject, iTween.Hash((object) "alpha", (object) 1, (object) "time", (object) 0.25f));
    card.GetActor().GetHealthObject().Enlarge((float) this.m_gemScale);
  }

  public override void NotifyOfCardTooltipDisplayHide(Card card)
  {
    if ((Object) this.attackHelpPanel != (Object) null)
    {
      if ((Object) card != (Object) null)
      {
        GemObject attackObject = card.GetActor().GetAttackObject();
        SceneUtils.SetLayer(attackObject.gameObject, GameLayer.Default);
        attackObject.Shrink();
      }
      Object.Destroy((Object) this.attackHelpPanel.gameObject);
      this.m_isShowingAttackHelpPanel = false;
    }
    if (!((Object) this.healthHelpPanel != (Object) null))
      return;
    if ((Object) card != (Object) null)
    {
      GemObject healthObject = card.GetActor().GetHealthObject();
      SceneUtils.SetLayer(healthObject.gameObject, GameLayer.Default);
      healthObject.Shrink();
    }
    Object.Destroy((Object) this.healthHelpPanel.gameObject);
  }

  private void ManaLabelLoadedCallback(string actorName, GameObject actorObject, object callbackData)
  {
    if (this.m_isShowingAttackHelpPanel)
      return;
    GameObject costTextObject = ((Card) callbackData).GetActor().GetCostTextObject();
    if ((Object) costTextObject == (Object) null)
    {
      Object.Destroy((Object) actorObject);
    }
    else
    {
      this.costLabel = actorObject;
      actorObject.transform.parent = costTextObject.transform;
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.transform.localPosition = new Vector3(-0.017f, 0.3512533f, 0.0f);
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.GetComponent<UberText>().Text = GameStrings.Get("GLOBAL_COST");
    }
  }

  private void AttackLabelLoadedCallback(string actorName, GameObject actorObject, object callbackData)
  {
    if (this.m_isShowingAttackHelpPanel)
      return;
    GameObject attackTextObject = ((Card) callbackData).GetActor().GetAttackTextObject();
    if ((Object) attackTextObject == (Object) null)
    {
      Object.Destroy((Object) actorObject);
    }
    else
    {
      this.attackLabel = actorObject;
      actorObject.transform.parent = attackTextObject.transform;
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.transform.localPosition = new Vector3(-0.2f, -0.3039344f, 0.0f);
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.GetComponent<UberText>().Text = GameStrings.Get("GLOBAL_ATTACK");
    }
  }

  private void HealthLabelLoadedCallback(string actorName, GameObject actorObject, object callbackData)
  {
    if (this.m_isShowingAttackHelpPanel)
      return;
    GameObject healthTextObject = ((Card) callbackData).GetActor().GetHealthTextObject();
    if ((Object) healthTextObject == (Object) null)
    {
      Object.Destroy((Object) actorObject);
    }
    else
    {
      this.healthLabel = actorObject;
      actorObject.transform.parent = healthTextObject.transform;
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.transform.localPosition = new Vector3(0.21f, -0.31f, 0.0f);
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.GetComponent<UberText>().Text = GameStrings.Get("GLOBAL_HEALTH");
    }
  }

  public override void NotifyOfCardMousedOver(Entity mousedOverEntity)
  {
    if (this.ShouldShowArrowOnCardInHand(mousedOverEntity))
      NotificationManager.Get().DestroyAllArrows();
    if (mousedOverEntity.GetZone() != TAG_ZONE.HAND)
      return;
    this.mousedOverCard = mousedOverEntity.GetCard();
    AssetLoader.Get().LoadActor("NumberLabel", new AssetLoader.GameObjectCallback(this.ManaLabelLoadedCallback), (object) this.mousedOverCard, false);
    AssetLoader.Get().LoadActor("NumberLabel", new AssetLoader.GameObjectCallback(this.AttackLabelLoadedCallback), (object) this.mousedOverCard, false);
    AssetLoader.Get().LoadActor("NumberLabel", new AssetLoader.GameObjectCallback(this.HealthLabelLoadedCallback), (object) this.mousedOverCard, false);
  }

  public override void NotifyOfCardMousedOff(Entity mousedOffEntity)
  {
    if (this.ShouldShowArrowOnCardInHand(mousedOffEntity))
      Gameplay.Get().StartCoroutine(this.ShowArrowInSeconds(0.5f));
    this.NukeNumberLabels();
  }

  private void NukeNumberLabels()
  {
    this.mousedOverCard = (Card) null;
    if ((Object) this.costLabel != (Object) null)
      Object.Destroy((Object) this.costLabel);
    if ((Object) this.attackLabel != (Object) null)
      Object.Destroy((Object) this.attackLabel);
    if (!((Object) this.healthLabel != (Object) null))
      return;
    Object.Destroy((Object) this.healthLabel);
  }

  private bool ShouldShowArrowOnCardInHand(Entity entity)
  {
    if (entity.GetZone() != TAG_ZONE.HAND)
      return false;
    switch (this.GetTag(GAME_TAG.TURN))
    {
      case 2:
        return true;
      case 4:
        if (GameState.Get().GetFriendlySidePlayer().GetBattlefieldZone().GetCards().Count == 0)
          return true;
        break;
    }
    return false;
  }

  [DebuggerHidden]
  private IEnumerator ShowArrowInSeconds(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CShowArrowInSeconds\u003Ec__Iterator206() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds, \u003C\u003Ef__this = this };
  }

  private void ShowHandBouncingArrow()
  {
    if ((Object) this.handBounceArrow != (Object) null)
      return;
    List<Card> cards = GameState.Get().GetFriendlySidePlayer().GetHandZone().GetCards();
    if (cards.Count == 0)
      return;
    Card card = cards[0];
    Vector3 position = card.transform.position;
    this.handBounceArrow = NotificationManager.Get().CreateBouncingArrow(UserAttentionBlocker.NONE, !(bool) UniversalInputManager.UsePhoneUI ? new Vector3(position.x, position.y, position.z + 2f) : new Vector3(position.x - 0.08f, position.y + 0.2f, position.z + 1.2f), new Vector3(0.0f, 0.0f, 0.0f));
    this.handBounceArrow.transform.parent = card.transform;
  }

  private void ShowHandFadeArrow()
  {
    List<Card> cards = GameState.Get().GetFriendlySidePlayer().GetHandZone().GetCards();
    if (cards.Count == 0)
      return;
    this.ShowFadeArrow(cards[0], (Card) null);
  }

  private void ShowFadeArrow(Card card, Card target = null)
  {
    if ((Object) this.handFadeArrow != (Object) null)
      return;
    Vector3 position1 = card.transform.position;
    Vector3 rotation = new Vector3(0.0f, 180f, 0.0f);
    Vector3 position2;
    if ((Object) target != (Object) null)
    {
      Vector3 vector3_1 = target.transform.position - position1;
      Vector3 vector3_2 = new Vector3(position1.x, position1.y + 0.47f, position1.z + 0.27f);
      float num = Vector3.Angle(target.transform.position - vector3_2, new Vector3(0.0f, 0.0f, -1f));
      rotation = new Vector3(0.0f, -Mathf.Sign(vector3_1.x) * num, 0.0f);
      position2 = vector3_2 + 0.3f * vector3_1;
    }
    else
      position2 = new Vector3(position1.x, position1.y + 0.047f, position1.z + 0.95f);
    this.handFadeArrow = NotificationManager.Get().CreateFadeArrow(position2, rotation);
    if ((Object) target != (Object) null)
      this.handFadeArrow.transform.localScale = 1.25f * Vector3.one;
    this.handFadeArrow.transform.parent = card.transform;
  }

  private void HideFadeArrow()
  {
    if (!((Object) this.handFadeArrow != (Object) null))
      return;
    NotificationManager.Get().DestroyNotification(this.handFadeArrow, 0.0f);
    this.handFadeArrow = (Notification) null;
  }

  private void OnPhoneHandShown(object userData)
  {
    if ((Object) this.handBounceArrow != (Object) null)
    {
      NotificationManager.Get().DestroyNotification(this.handBounceArrow, 0.0f);
      this.handBounceArrow = (Notification) null;
    }
    this.ShowHandFadeArrow();
  }

  private void OnPhoneHandHidden(object userData)
  {
    this.HideFadeArrow();
    this.ShowHandBouncingArrow();
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator207() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CHandleMissionEventWithTiming\u003Ec__Iterator208() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  private void ShowAttackWithYourMinionPopup()
  {
    if ((Object) this.attackWithYourMinion != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.attackWithYourMinion);
    if (this.firstAttackFinished || (Object) this.firstMurlocCard == (Object) null)
      return;
    this.firstMurlocCard.GetActor().ToggleForceIdle(false);
    this.firstMurlocCard.GetActor().SetActorState(ActorStateType.CARD_PLAYABLE);
    Vector3 position = this.firstMurlocCard.transform.position;
    if (this.firstMurlocCard.GetEntity().IsExhausted() || !(this.firstMurlocCard.GetZone() is ZonePlay))
      return;
    if ((Object) this.firstRaptorCard != (Object) null && this.firstMurlocCard.GetZonePosition() < this.firstRaptorCard.GetZonePosition())
    {
      this.attackWithYourMinion = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(position.x - 3f, position.y, position.z), TutorialEntity.HELP_POPUP_SCALE, this.textToShowForAttackTip, true);
      this.attackWithYourMinion.ShowPopUpArrow(Notification.PopUpArrowDirection.Right);
    }
    else
    {
      this.attackWithYourMinion = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(position.x + 3f, position.y, position.z), TutorialEntity.HELP_POPUP_SCALE, this.textToShowForAttackTip, true);
      this.attackWithYourMinion.ShowPopUpArrow(Notification.PopUpArrowDirection.Left);
    }
    this.ShowFadeArrow(this.firstMurlocCard, GameState.Get().GetOpposingSidePlayer().GetBattlefieldZone().GetFirstCard());
    Gameplay.Get().StartCoroutine(this.SwapHelpTextAndFlashMinion());
  }

  [DebuggerHidden]
  private IEnumerator SwapHelpTextAndFlashMinion()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CSwapHelpTextAndFlashMinion\u003Ec__Iterator209() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator FlashMinionUntilAttackBegins(Card minionToFlash)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CFlashMinionUntilAttackBegins\u003Ec__Iterator20A() { minionToFlash = minionToFlash, \u003C\u0024\u003EminionToFlash = minionToFlash, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator BeginFlashingMinionLoop(Card minionToFlash)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CBeginFlashingMinionLoop\u003Ec__Iterator20B() { minionToFlash = minionToFlash, \u003C\u0024\u003EminionToFlash = minionToFlash, \u003C\u003Ef__this = this };
  }

  private void ShowEndTurnBouncingArrow()
  {
    if (EndTurnButton.Get().IsInWaitingState())
      return;
    Vector3 position = EndTurnButton.Get().transform.position;
    NotificationManager.Get().CreateBouncingArrow(UserAttentionBlocker.NONE, new Vector3(position.x - 2f, position.y, position.z), new Vector3(0.0f, -90f, 0.0f));
  }

  private void ShowDontFireballYourselfPopup(Vector3 origin)
  {
    if ((Object) this.noFireballPopup != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.noFireballPopup);
    this.noFireballPopup = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(origin.x - 3f, origin.y, origin.z), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("TUTORIAL01_HELP_07"), true);
    NotificationManager.Get().DestroyNotification(this.noFireballPopup, 2.5f);
  }

  public override bool ShouldDoAlternateMulliganIntro()
  {
    return true;
  }

  public override bool DoAlternateMulliganIntro()
  {
    AssetLoader.Get().LoadActor("GameOpen_Pack", new AssetLoader.GameObjectCallback(this.PackLoadedCallback), (object) null, false);
    return true;
  }

  private void PackLoadedCallback(string actorName, GameObject actorObject, object callbackData)
  {
    MusicManager.Get().StartPlaylist(MusicPlaylistType.Misc_Tutorial01);
    Card heroCard1 = GameState.Get().GetFriendlySidePlayer().GetHeroCard();
    Card heroCard2 = GameState.Get().GetOpposingSidePlayer().GetHeroCard();
    this.startingPack = actorObject;
    Transform transform1 = SceneUtils.FindChildBySubstring(this.startingPack, "Hero_Dummy").transform;
    heroCard1.transform.parent = transform1;
    heroCard1.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
    heroCard1.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    SceneUtils.SetLayer(heroCard1.GetActor().GetRootObject(), GameLayer.IgnoreFullScreenEffects);
    Transform transform2 = SceneUtils.FindChildBySubstring(this.startingPack, "HeroEnemy_Dummy").transform;
    heroCard2.transform.parent = transform2;
    heroCard2.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
    heroCard2.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    heroCard1.SetDoNotSort(true);
    Transform bone = Board.Get().FindBone("Tutorial1HeroStart");
    actorObject.transform.position = bone.position;
    heroCard1.GetActor().GetHealthObject().Hide();
    heroCard2.GetActor().GetHealthObject().Hide();
    heroCard2.GetActor().Hide();
    heroCard1.GetActor().Hide();
    SceneMgr.Get().NotifySceneLoaded();
    Gameplay.Get().StartCoroutine(this.UpdatePresence());
    Gameplay.Get().StartCoroutine(this.ShowPackOpeningArrow(bone.position));
  }

  [DebuggerHidden]
  private IEnumerator UpdatePresence()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Tutorial_01.\u003CUpdatePresence\u003Ec__Iterator20C presenceCIterator20C = new Tutorial_01.\u003CUpdatePresence\u003Ec__Iterator20C();
    return (IEnumerator) presenceCIterator20C;
  }

  [DebuggerHidden]
  private IEnumerator ShowPackOpeningArrow(Vector3 packSpot)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CShowPackOpeningArrow\u003Ec__Iterator20D() { packSpot = packSpot, \u003C\u0024\u003EpackSpot = packSpot, \u003C\u003Ef__this = this };
  }

  public override void NotifyOfGamePackOpened()
  {
    this.packOpened = true;
    if (!((Object) this.freeCardsPopup != (Object) null))
      return;
    NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.freeCardsPopup);
  }

  public override void NotifyOfCustomIntroFinished()
  {
    Card heroCard1 = GameState.Get().GetFriendlySidePlayer().GetHeroCard();
    Card heroCard2 = GameState.Get().GetOpposingSidePlayer().GetHeroCard();
    heroCard1.SetDoNotSort(false);
    heroCard2.GetActor().TurnOnCollider();
    heroCard1.GetActor().TurnOnCollider();
    heroCard1.transform.parent = (Transform) null;
    heroCard2.transform.parent = (Transform) null;
    SceneUtils.SetLayer(heroCard1.GetActor().GetRootObject(), GameLayer.CardRaycast);
    Gameplay.Get().StartCoroutine(this.ContinueFinishingCustomIntro());
  }

  [DebuggerHidden]
  private IEnumerator ContinueFinishingCustomIntro()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_01.\u003CContinueFinishingCustomIntro\u003Ec__Iterator20E() { \u003C\u003Ef__this = this };
  }

  public override bool IsMouseOverDelayOverriden()
  {
    return true;
  }

  public override bool AreTooltipsDisabled()
  {
    return this.tooltipsDisabled;
  }

  public override bool ShouldShowBigCard()
  {
    return this.GetTag(GAME_TAG.TURN) > 8;
  }

  public override void NotifyOfDefeatCoinAnimation()
  {
    this.PlaySound("VO_TUTORIAL_01_JAINA_13_10", 1f, true, false);
  }

  public override bool ShouldShowHeroTooltips()
  {
    return true;
  }

  public override List<RewardData> GetCustomRewards()
  {
    List<RewardData> rewardDataList = new List<RewardData>();
    CardRewardData cardRewardData = new CardRewardData("CS2_023", TAG_PREMIUM.NORMAL, 2);
    cardRewardData.MarkAsDummyReward();
    rewardDataList.Add((RewardData) cardRewardData);
    return rewardDataList;
  }
}
