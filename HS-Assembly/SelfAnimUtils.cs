﻿// Decompiled with JetBrains decompiler
// Type: SelfAnimUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SelfAnimUtils : MonoBehaviour
{
  public void PrintLog(string message)
  {
    Debug.Log((object) message);
  }

  public void PrintLogWarning(string message)
  {
    Debug.LogWarning((object) message);
  }

  public void PrintLogError(string message)
  {
    Debug.LogError((object) message);
  }

  public void PlayParticles()
  {
    if (!((Object) this.GetComponent<ParticleEmitter>() != (Object) null))
      return;
    this.GetComponent<ParticleEmitter>().emit = true;
  }

  public void StopParticles()
  {
    if (!((Object) this.GetComponent<ParticleEmitter>() != (Object) null))
      return;
    this.GetComponent<ParticleEmitter>().emit = false;
  }

  public void KillParticles()
  {
    if ((Object) this.GetComponent<ParticleEmitter>() == (Object) null)
      return;
    this.GetComponent<ParticleEmitter>().emit = false;
    this.GetComponent<ParticleEmitter>().particles = new Particle[0];
  }

  public void PlayAnimation()
  {
    if (!((Object) this.GetComponent<Animation>() != (Object) null))
      return;
    this.GetComponent<Animation>().Play();
  }

  public void StopAnimation()
  {
    if (!((Object) this.GetComponent<Animation>() != (Object) null))
      return;
    this.GetComponent<Animation>().Stop();
  }

  public void ActivateHierarchy()
  {
    this.gameObject.SetActive(true);
  }

  public void DeactivateHierarchy()
  {
    this.gameObject.SetActive(false);
  }

  public void DestroyHierarchy()
  {
    Object.Destroy((Object) this.gameObject);
  }

  public void FadeIn(float FadeSec)
  {
    iTween.FadeTo(this.gameObject, 1f, FadeSec);
  }

  public void FadeOut(float FadeSec)
  {
    iTween.FadeTo(this.gameObject, 0.0f, FadeSec);
  }

  public void SetAlphaHierarchy(float alpha)
  {
    foreach (Renderer componentsInChild in this.GetComponentsInChildren<Renderer>())
    {
      if (componentsInChild.material.HasProperty("_Color"))
      {
        Color color = componentsInChild.material.color;
        color.a = alpha;
        componentsInChild.material.color = color;
      }
    }
  }

  public void PlayDefaultSound()
  {
    if ((Object) this.GetComponent<AudioSource>() == (Object) null)
      Debug.LogError((object) string.Format("SelfAnimUtils.PlayDefaultSound() - Tried to play the AudioSource on {0} but it has no AudioSource. You need an AudioSource to use this function.", (object) this.gameObject));
    else if ((Object) SoundManager.Get() == (Object) null)
      this.GetComponent<AudioSource>().Play();
    else
      SoundManager.Get().Play(this.GetComponent<AudioSource>(), true);
  }

  public void PlaySound(AudioClip clip)
  {
    if ((Object) clip == (Object) null)
      Debug.LogError((object) string.Format("SelfAnimUtils.PlayDefaultSound() - No clip was given when trying to play the AudioSource on {0}. You need a clip to use this function.", (object) this.gameObject));
    else if ((Object) this.GetComponent<AudioSource>() == (Object) null)
      Debug.LogError((object) string.Format("SelfAnimUtils.PlayDefaultSound() - Tried to play clip {0} on {1} but it has no AudioSource. You need an AudioSource to use this function.", (object) clip, (object) this.gameObject));
    else if ((Object) SoundManager.Get() == (Object) null)
      this.GetComponent<AudioSource>().PlayOneShot(clip);
    else
      SoundManager.Get().PlayOneShot(this.GetComponent<AudioSource>(), clip, 1f, true);
  }

  public void RandomRotationX()
  {
    TransformUtil.SetEulerAngleX(this.gameObject, Random.Range(0.0f, 360f));
  }

  public void RandomRotationY()
  {
    TransformUtil.SetEulerAngleY(this.gameObject, Random.Range(0.0f, 360f));
  }

  public void RandomRotationZ()
  {
    TransformUtil.SetEulerAngleZ(this.gameObject, Random.Range(0.0f, 360f));
  }
}
