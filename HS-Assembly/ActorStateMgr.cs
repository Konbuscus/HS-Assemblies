﻿// Decompiled with JetBrains decompiler
// Type: ActorStateMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorStateMgr : MonoBehaviour
{
  private Map<ActorStateType, List<ActorState>> m_actorStateMap = new Map<ActorStateType, List<ActorState>>();
  private bool m_shown = true;
  public GameObject m_ObjectContainer;
  private ActorStateType m_activeStateType;
  private HighlightState m_HighlightState;

  private void Start()
  {
    this.m_HighlightState = this.FindHightlightObject();
    this.BuildStateMap();
    if (this.m_activeStateType == ActorStateType.NONE)
      this.HideImpl();
    else if (this.m_shown)
      this.ShowImpl();
    else
      this.HideImpl();
  }

  public Map<ActorStateType, List<ActorState>> GetStateMap()
  {
    return this.m_actorStateMap;
  }

  public ActorStateType GetActiveStateType()
  {
    return this.m_activeStateType;
  }

  public List<ActorState> GetActiveStateList()
  {
    List<ActorState> actorStateList = (List<ActorState>) null;
    if (!this.m_actorStateMap.TryGetValue(this.m_activeStateType, out actorStateList))
      return (List<ActorState>) null;
    return actorStateList;
  }

  public float GetMaximumAnimationTimeOfActiveStates()
  {
    if (this.GetActiveStateList() == null)
      return 0.0f;
    float b = 0.0f;
    using (List<ActorState>.Enumerator enumerator = this.GetActiveStateList().GetEnumerator())
    {
      while (enumerator.MoveNext())
        b = Mathf.Max(enumerator.Current.GetAnimationDuration(), b);
    }
    return b;
  }

  public bool ChangeState(ActorStateType stateType)
  {
    return this.ChangeState_NewState(stateType) || this.ChangeState_LegacyState(stateType);
  }

  public bool ChangeState_NewState(ActorStateType stateType)
  {
    if (!(bool) ((UnityEngine.Object) this.m_HighlightState))
      return false;
    ActorStateType activeStateType = this.m_activeStateType;
    this.m_activeStateType = stateType;
    if (activeStateType != stateType)
      return this.m_HighlightState.ChangeState(stateType);
    return true;
  }

  public bool ChangeState_LegacyState(ActorStateType stateType)
  {
    List<ActorState> nextStateList = (List<ActorState>) null;
    this.m_actorStateMap.TryGetValue(stateType, out nextStateList);
    ActorStateType activeStateType = this.m_activeStateType;
    this.m_activeStateType = stateType;
    if (activeStateType != ActorStateType.NONE)
    {
      List<ActorState> actorStateList;
      if (this.m_actorStateMap.TryGetValue(activeStateType, out actorStateList))
      {
        using (List<ActorState>.Enumerator enumerator = actorStateList.GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.Stop(nextStateList);
        }
      }
    }
    else if (stateType != ActorStateType.NONE && (UnityEngine.Object) this.m_ObjectContainer != (UnityEngine.Object) null)
      this.m_ObjectContainer.SetActive(true);
    if (stateType == ActorStateType.NONE)
    {
      if (activeStateType != ActorStateType.NONE && (UnityEngine.Object) this.m_ObjectContainer != (UnityEngine.Object) null)
        this.m_ObjectContainer.SetActive(false);
      return true;
    }
    if (nextStateList == null)
      return false;
    using (List<ActorState>.Enumerator enumerator = nextStateList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Play();
    }
    return true;
  }

  public void ShowStateMgr()
  {
    if (this.m_shown)
      return;
    this.m_shown = true;
    this.ShowImpl();
  }

  public void HideStateMgr()
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    this.HideImpl();
  }

  public void RefreshStateMgr()
  {
    if (!(bool) ((UnityEngine.Object) this.m_HighlightState))
      return;
    this.m_HighlightState.SetDirty();
  }

  private HighlightState FindHightlightObject()
  {
    IEnumerator enumerator = this.transform.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
      {
        HighlightState component = ((Component) enumerator.Current).gameObject.GetComponent<HighlightState>();
        if ((bool) ((UnityEngine.Object) component))
          return component;
      }
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    return (HighlightState) null;
  }

  private void BuildStateMap()
  {
    IEnumerator enumerator = this.transform.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
      {
        ActorState component = ((Component) enumerator.Current).gameObject.GetComponent<ActorState>();
        if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        {
          ActorStateType stateType = component.m_StateType;
          if (stateType != ActorStateType.NONE)
          {
            List<ActorState> actorStateList;
            if (!this.m_actorStateMap.TryGetValue(stateType, out actorStateList))
            {
              actorStateList = new List<ActorState>();
              this.m_actorStateMap.Add(stateType, actorStateList);
            }
            actorStateList.Add(component);
          }
        }
      }
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
  }

  private void ShowImpl()
  {
    if ((bool) ((UnityEngine.Object) this.m_HighlightState))
      this.m_HighlightState.ChangeState(this.m_activeStateType);
    if (this.m_activeStateType != ActorStateType.NONE && (UnityEngine.Object) this.m_ObjectContainer != (UnityEngine.Object) null)
      this.m_ObjectContainer.SetActive(true);
    List<ActorState> activeStateList = this.GetActiveStateList();
    if (activeStateList == null)
      return;
    using (List<ActorState>.Enumerator enumerator = activeStateList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.ShowState();
    }
  }

  private void HideImpl()
  {
    if ((bool) ((UnityEngine.Object) this.m_HighlightState))
      this.m_HighlightState.ChangeState(ActorStateType.NONE);
    List<ActorState> activeStateList = this.GetActiveStateList();
    if (activeStateList != null)
    {
      using (List<ActorState>.Enumerator enumerator = activeStateList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.HideState();
      }
    }
    if (this.m_activeStateType == ActorStateType.NONE || !((UnityEngine.Object) this.m_ObjectContainer != (UnityEngine.Object) null))
      return;
    this.m_ObjectContainer.SetActive(false);
  }
}
