﻿// Decompiled with JetBrains decompiler
// Type: LOE07_MineCart
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class LOE07_MineCart : LOE_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();
  private Notification m_turnCounter;
  private MineCartRushArt m_mineCartArt;

  public override void StartGameplaySoundtracks()
  {
    MusicManager.Get().StartPlaylist(MusicPlaylistType.InGame_LOE_Minecart);
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOE_07_START");
    this.PreloadSound("VO_LOE_07_WIN");
    this.PreloadSound("VO_LOEA07_MINE_ARCHAEDAS");
    this.PreloadSound("VO_LOEA07_MINE_DETONATE");
    this.PreloadSound("VO_LOEA07_MINE_RAMMING");
    this.PreloadSound("VO_LOEA07_MINE_PARROT");
    this.PreloadSound("VO_LOEA07_MINE_BOOMZOOKA");
    this.PreloadSound("VO_LOEA07_MINE_DYNAMITE");
    this.PreloadSound("VO_LOEA07_MINE_BOOM");
    this.PreloadSound("VO_LOEA07_MINE_BARREL_FORWARD");
    this.PreloadSound("VO_LOEA07_MINE_HUNKER_DOWN");
    this.PreloadSound("VO_LOEA07_MINE_SPIKED_DECOY");
    this.PreloadSound("VO_LOEA07_MINE_REPAIRS");
    this.PreloadSound("VO_BRANN_WIN_07_ALT_07");
    this.PreloadSound("VO_LOE_07_RESPONSE");
    this.PreloadSound("Mine_response");
  }

  public override bool ShouldDoAlternateMulliganIntro()
  {
    return true;
  }

  public override bool ShouldHandleCoin()
  {
    return false;
  }

  public override void NotifyOfMulliganInitialized()
  {
    base.NotifyOfMulliganInitialized();
    this.InitVisuals();
  }

  public override void NotifyOfMulliganEnded()
  {
    base.NotifyOfMulliganEnded();
    GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor().GetHealthObject().Hide();
  }

  public override void OnTagChanged(TagDelta change)
  {
    base.OnTagChanged(change);
    if (change.tag != 48 || change.newValue == change.oldValue)
      return;
    this.UpdateVisuals(change.newValue);
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE07_MineCart.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator168() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  protected override void PlayEmoteResponse(EmoteType emoteType, CardSoundSpell emoteSpell)
  {
    if (!MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS.Contains(emoteType))
      return;
    SoundManager.Get().LoadAndPlay("Mine_response", GameState.Get().GetOpposingSidePlayer().GetHero().GetCard().GetActor().gameObject);
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE07_MineCart.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator169() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE07_MineCart.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator16A() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE07_MineCart.\u003CHandleGameOverWithTiming\u003Ec__Iterator16B() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }

  private void InitVisuals()
  {
    int cost = this.GetCost();
    this.InitMineCartArt();
    this.InitTurnCounter(cost);
  }

  private void InitMineCartArt()
  {
    this.m_mineCartArt = AssetLoader.Get().LoadGameObject("MineCartRushArt", true, false).GetComponent<MineCartRushArt>();
  }

  private void InitTurnCounter(int cost)
  {
    this.m_turnCounter = AssetLoader.Get().LoadGameObject("LOE_Turn_Timer", true, false).GetComponent<Notification>();
    PlayMakerFSM component = this.m_turnCounter.GetComponent<PlayMakerFSM>();
    component.FsmVariables.GetFsmBool("RunningMan").Value = false;
    component.FsmVariables.GetFsmBool("MineCart").Value = true;
    component.SendEvent("Birth");
    this.m_turnCounter.transform.parent = GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor().gameObject.transform;
    this.m_turnCounter.transform.localPosition = new Vector3(-1.4f, 0.187f, -0.11f);
    this.m_turnCounter.transform.localScale = Vector3.one * 0.52f;
    this.UpdateTurnCounterText(cost);
  }

  private void UpdateVisuals(int cost)
  {
    this.UpdateMineCartArt();
    this.UpdateTurnCounter(cost);
  }

  private void UpdateMineCartArt()
  {
    this.m_mineCartArt.DoPortraitSwap(GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor());
  }

  private void UpdateTurnCounter(int cost)
  {
    this.m_turnCounter.GetComponent<PlayMakerFSM>().SendEvent("Action");
    this.UpdateTurnCounterText(cost);
  }

  private void UpdateTurnCounterText(int cost)
  {
    this.m_turnCounter.ChangeDialogText(GameStrings.FormatPlurals("MISSION_DEFAULTCOUNTERNAME", new GameStrings.PluralNumber[1]
    {
      new GameStrings.PluralNumber()
      {
        m_index = 0,
        m_number = cost
      }
    }), cost.ToString(), string.Empty, string.Empty);
  }
}
