﻿// Decompiled with JetBrains decompiler
// Type: DeckTemplatePicker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class DeckTemplatePicker : MonoBehaviour
{
  public Vector3 m_bottomPanelHideOffset = new Vector3(0.0f, 0.0f, 25f);
  public float m_bottomPanelSlideInWaitDelay = 0.25f;
  public float m_bottomPanelAnimateTime = 0.25f;
  public float m_packAnimInTime = 0.25f;
  public float m_packAnimOutTime = 0.2f;
  private List<DeckTemplatePickerButton> m_pickerButtons = new List<DeckTemplatePickerButton>();
  private CollectionManager.TemplateDeck m_customDeck = new CollectionManager.TemplateDeck();
  private List<DeckTemplatePicker.OnTemplateDeckChosen> m_templateDeckChosenListeners = new List<DeckTemplatePicker.OnTemplateDeckChosen>();
  private TransformProps m_customDeckInitialPosition = new TransformProps();
  public GameObject m_root;
  public GameObject m_pageHeader;
  public UberText m_pageHeaderText;
  public UIBObjectSpacing m_pickerButtonRoot;
  public DeckTemplatePickerButton m_pickerButtonTpl;
  public DeckTemplatePickerButton m_customDeckButton;
  public UberText m_deckTemplateDescription;
  public UberText m_deckTemplatePhoneName;
  public PlayButton m_chooseButton;
  public GameObject m_bottomPanel;
  public DeckTemplatePhoneTray m_phoneTray;
  public UIBButton m_phoneBackButton;
  public Vector3 m_offscreenPackOffset;
  public Transform m_ghostCardTipBone;
  private TAG_CLASS m_currentSelectedClass;
  private CollectionManager.TemplateDeck m_currentSelectedDeck;
  private Vector3 m_origBottomPanelPos;
  private bool m_showingBottomPanel;
  private bool m_packsShown;

  private void Awake()
  {
    this.m_currentSelectedDeck = this.m_customDeck;
    for (int index = 0; index < 3; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      DeckTemplatePicker.\u003CAwake\u003Ec__AnonStorey39F awakeCAnonStorey39F = new DeckTemplatePicker.\u003CAwake\u003Ec__AnonStorey39F();
      // ISSUE: reference to a compiler-generated field
      awakeCAnonStorey39F.\u003C\u003Ef__this = this;
      // ISSUE: reference to a compiler-generated field
      awakeCAnonStorey39F.idx = index;
      DeckTemplatePickerButton templatePickerButton = (DeckTemplatePickerButton) GameUtils.Instantiate((Component) this.m_pickerButtonTpl, this.m_pickerButtonRoot.gameObject, true);
      Vector3 zero = Vector3.zero;
      if ((bool) UniversalInputManager.UsePhoneUI)
        zero.x = 0.75f;
      this.m_pickerButtonRoot.AddObject((Component) templatePickerButton, zero, true);
      // ISSUE: reference to a compiler-generated method
      templatePickerButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(awakeCAnonStorey39F.\u003C\u003Em__F0));
      templatePickerButton.gameObject.SetActive(true);
      this.m_pickerButtons.Add(templatePickerButton);
    }
    if (this.m_pickerButtons.Count > 0)
      this.m_pickerButtons[this.m_pickerButtons.Count - 1].SetIsStarterDeck(true);
    this.m_pickerButtonRoot.UpdatePositions();
    this.m_pickerButtonTpl.gameObject.SetActive(false);
    if ((UnityEngine.Object) this.m_customDeckButton != (UnityEngine.Object) null)
    {
      this.m_customDeckButton.gameObject.SetActive(true);
      this.m_customDeckButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.SelectCustomDeckButton(false)));
    }
    if ((UnityEngine.Object) this.m_chooseButton != (UnityEngine.Object) null)
    {
      this.m_chooseButton.Disable();
      this.m_chooseButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ChooseRecipeAndFillInCards()));
    }
    if ((UnityEngine.Object) this.m_phoneTray != (UnityEngine.Object) null)
    {
      this.m_phoneTray.m_scrollbar.SaveScroll("start");
      this.m_phoneTray.gameObject.SetActive(false);
    }
    if ((UnityEngine.Object) this.m_bottomPanel != (UnityEngine.Object) null)
      this.m_origBottomPanelPos = this.m_bottomPanel.transform.localPosition;
    if ((UnityEngine.Object) this.m_phoneBackButton != (UnityEngine.Object) null)
      this.m_phoneBackButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OnBackButtonPressed(e)));
    TransformUtil.CopyLocal(this.m_customDeckInitialPosition, (Component) this.m_customDeckButton.transform);
  }

  private void OnBackButtonPressed(UIEvent e)
  {
    Navigation.GoBack();
  }

  [DebuggerHidden]
  private IEnumerator BackOut()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTemplatePicker.\u003CBackOut\u003Ec__Iterator58()
    {
      \u003C\u003Ef__this = this
    };
  }

  public bool OnNavigateBack()
  {
    this.StartCoroutine(this.BackOut());
    return true;
  }

  public void RegisterOnTemplateDeckChosen(DeckTemplatePicker.OnTemplateDeckChosen dlg)
  {
    this.m_templateDeckChosenListeners.Add(dlg);
  }

  public void UnregisterOnTemplateDeckChosen(DeckTemplatePicker.OnTemplateDeckChosen dlg)
  {
    this.m_templateDeckChosenListeners.Remove(dlg);
  }

  public bool IsShowingBottomPanel()
  {
    return this.m_showingBottomPanel;
  }

  public bool IsShowingPacks()
  {
    return this.m_packsShown;
  }

  [DebuggerHidden]
  public IEnumerator Show(bool show)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTemplatePicker.\u003CShow\u003Ec__Iterator59()
    {
      show = show,
      \u003C\u0024\u003Eshow = show,
      \u003C\u003Ef__this = this
    };
  }

  private void SetupTemplateButtons(CollectionManager.TemplateDeck refDeck)
  {
    List<CollectionManager.TemplateDeck> templateDecks = CollectionManager.Get().GetTemplateDecks(this.m_currentSelectedClass);
    for (int index = 0; index < this.m_pickerButtons.Count && index < templateDecks.Count; ++index)
    {
      CollectionManager.TemplateDeck templateDeck = templateDecks[index];
      bool flag = refDeck == templateDeck;
      if (flag)
        this.m_currentSelectedDeck = templateDeck;
      this.m_pickerButtons[index].SetSelected(false);
      if (flag && (UnityEngine.Object) this.m_deckTemplateDescription != (UnityEngine.Object) null)
        this.m_deckTemplateDescription.Text = templateDeck.m_description;
      if (flag && (UnityEngine.Object) this.m_deckTemplatePhoneName != (UnityEngine.Object) null)
        this.m_deckTemplatePhoneName.Text = templateDeck.m_title;
      this.m_pickerButtons[index].transform.localEulerAngles = Vector3.zero;
      this.m_pickerButtons[index].GetComponent<RandomTransform>().Apply();
      this.m_pickerButtons[index].GetComponent<AnimatedLowPolyPack>().Init(0, this.m_pickerButtons[index].transform.localPosition, this.m_pickerButtons[index].transform.localPosition + this.m_offscreenPackOffset, false, false);
      this.m_pickerButtons[index].GetComponent<AnimatedLowPolyPack>().SetFlyingLocalRotations(this.m_pickerButtons[index].transform.localEulerAngles, this.m_pickerButtons[index].transform.localEulerAngles);
    }
    if (!((UnityEngine.Object) this.m_customDeckButton != (UnityEngine.Object) null))
      return;
    this.m_customDeckButton.SetSelected(false);
    this.m_customDeckButton.transform.localEulerAngles = Vector3.zero;
    this.m_customDeckButton.GetComponent<AnimatedLowPolyPack>().Init(0, this.m_customDeckButton.transform.localPosition, this.m_customDeckButton.transform.localPosition + this.m_offscreenPackOffset, false, false);
    this.m_customDeckButton.GetComponent<AnimatedLowPolyPack>().SetFlyingLocalRotations(this.m_customDeckButton.transform.localEulerAngles, this.m_customDeckButton.transform.localEulerAngles);
  }

  [DebuggerHidden]
  public IEnumerator ShowPacks(bool show)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTemplatePicker.\u003CShowPacks\u003Ec__Iterator5A()
    {
      show = show,
      \u003C\u0024\u003Eshow = show,
      \u003C\u003Ef__this = this
    };
  }

  public void ShowBottomPanel(bool show)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTemplatePicker.\u003CShowBottomPanel\u003Ec__AnonStorey3A0 panelCAnonStorey3A0 = new DeckTemplatePicker.\u003CShowBottomPanel\u003Ec__AnonStorey3A0();
    // ISSUE: reference to a compiler-generated field
    panelCAnonStorey3A0.show = show;
    // ISSUE: reference to a compiler-generated field
    panelCAnonStorey3A0.\u003C\u003Ef__this = this;
    if (!((UnityEngine.Object) this.m_bottomPanel != (UnityEngine.Object) null))
      return;
    Vector3 origBottomPanelPos1 = this.m_origBottomPanelPos;
    Vector3 origBottomPanelPos2 = this.m_origBottomPanelPos;
    float num = 0.0f;
    // ISSUE: reference to a compiler-generated field
    if (panelCAnonStorey3A0.show)
    {
      origBottomPanelPos2 += this.m_bottomPanelHideOffset;
      num = this.m_bottomPanelSlideInWaitDelay;
      this.m_showingBottomPanel = true;
    }
    else
    {
      origBottomPanelPos1 += this.m_bottomPanelHideOffset;
      // ISSUE: reference to a compiler-generated method
      ApplicationMgr.Get().ScheduleCallback(this.m_bottomPanelAnimateTime, false, new ApplicationMgr.ScheduledCallback(panelCAnonStorey3A0.\u003C\u003Em__F4), (object) null);
    }
    iTween.Stop(this.m_bottomPanel);
    this.m_bottomPanel.transform.localPosition = origBottomPanelPos2;
    iTween.MoveTo(this.m_bottomPanel, iTween.Hash((object) "position", (object) origBottomPanelPos1, (object) "isLocal", (object) true, (object) "time", (object) this.m_bottomPanelAnimateTime, (object) "delay", (object) num));
  }

  public void OnTrayToggled(bool shown)
  {
    if (shown)
    {
      this.StartCoroutine(this.ShowTutorialPopup());
    }
    else
    {
      CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.CARDS, true, (CollectionManagerDisplay.ViewModeData) null);
      this.StartCoroutine(CollectionManagerDisplay.Get().ShowDeckTemplateTipsIfNeeded());
    }
  }

  [DebuggerHidden]
  private IEnumerator ShowTutorialPopup()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTemplatePicker.\u003CShowTutorialPopup\u003Ec__Iterator5B popupCIterator5B = new DeckTemplatePicker.\u003CShowTutorialPopup\u003Ec__Iterator5B();
    return (IEnumerator) popupCIterator5B;
  }

  public void SetDeckClass(TAG_CLASS deckClass)
  {
    this.m_currentSelectedClass = deckClass;
    List<CollectionManager.TemplateDeck> templateDecks = CollectionManager.Get().GetTemplateDecks(this.m_currentSelectedClass);
    int num1 = templateDecks == null ? 0 : templateDecks.Count;
    Color classColor = CollectionPageManager.s_classColors[deckClass];
    this.m_pageHeaderText.Text = GameStrings.Format("GLUE_DECK_TEMPLATE_CHOOSE_DECK", (object) GameStrings.GetClassName(deckClass));
    CollectionPageDisplay.SetClassFlavorTextures(this.m_pageHeader, CollectionPageDisplay.TagClassToHeaderClass(deckClass));
    for (int index = 0; index < this.m_pickerButtons.Count; ++index)
    {
      DeckTemplatePickerButton pickerButton = this.m_pickerButtons[index];
      bool flag = index < num1;
      pickerButton.gameObject.SetActive(flag);
      if (flag)
      {
        CollectionManager.TemplateDeck templateDeck = templateDecks[index];
        pickerButton.SetTitleText(templateDeck.m_title);
        int count = 0;
        using (Map<string, int>.Enumerator enumerator = templateDeck.m_cardIds.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, int> current = enumerator.Current;
            int standard;
            int golden;
            CollectionManager.Get().GetOwnedCardCount(current.Key, out standard, out golden);
            int num2 = Mathf.Min(standard + golden, current.Value);
            count += num2;
          }
        }
        pickerButton.SetCardCountText(count);
        pickerButton.SetDeckTexture(templateDeck.m_displayTexture);
        pickerButton.m_packRibbon.material.color = classColor;
      }
    }
    if (!((UnityEngine.Object) this.m_customDeckButton != (UnityEngine.Object) null))
      return;
    this.m_customDeckButton.m_deckTexture.material.mainTextureOffset = CollectionPageManager.s_classTextureOffsets[deckClass];
    this.m_customDeckButton.m_packRibbon.material.color = classColor;
  }

  private void SelectButtonWithIndex(int index)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTemplatePicker.\u003CSelectButtonWithIndex\u003Ec__AnonStorey3A1 indexCAnonStorey3A1 = new DeckTemplatePicker.\u003CSelectButtonWithIndex\u003Ec__AnonStorey3A1();
    // ISSUE: reference to a compiler-generated field
    indexCAnonStorey3A1.index = index;
    // ISSUE: reference to a compiler-generated field
    indexCAnonStorey3A1.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated method
    Action confirmAction = new Action(indexCAnonStorey3A1.\u003C\u003Em__F5);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (indexCAnonStorey3A1.index < this.m_pickerButtons.Count && this.m_pickerButtons[indexCAnonStorey3A1.index].GetOwnedCardCount() < 10 && !this.m_pickerButtons[indexCAnonStorey3A1.index].IsStarterDeck())
      this.ShowLowCardsPopup(confirmAction, (Action) null);
    else
      confirmAction();
  }

  [DebuggerHidden]
  public IEnumerator ShowTips()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTemplatePicker.\u003CShowTips\u003Ec__Iterator5C()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void FillDeckWithTemplate(CollectionManager.TemplateDeck tplDeck)
  {
    CollectionDeck editingDeck1 = CollectionDeckTray.Get().GetCardsContent().GetEditingDeck();
    if (editingDeck1 == null)
      return;
    if (tplDeck == null)
    {
      CollectionDeck editedDeck = CollectionManager.Get().GetEditedDeck();
      editingDeck1.CopyFrom(editedDeck);
    }
    else
      editingDeck1.FillFromTemplateDeck(tplDeck);
    CollectionDeckTray.Get().m_cardsContent.UpdateCardList(true, (Actor) null);
    CollectionDeckTray.Get().m_decksContent.UpdateDeckName((string) null);
    if (!((UnityEngine.Object) this.m_phoneTray != (UnityEngine.Object) null))
      return;
    CollectionDeck editingDeck2 = this.m_phoneTray.m_cardsContent.GetEditingDeck();
    if (tplDeck == null)
    {
      CollectionDeck editedDeck = CollectionManager.Get().GetEditedDeck();
      editingDeck2.CopyFrom(editedDeck);
    }
    else
      editingDeck2.FillFromTemplateDeck(tplDeck);
    this.m_phoneTray.m_cardsContent.UpdateCardList(true, (Actor) null);
    SceneUtils.SetLayer((Component) this.m_phoneTray, GameLayer.IgnoreFullScreenEffects);
  }

  private void FillWithCustomDeck()
  {
    this.FillDeckWithTemplate((CollectionManager.TemplateDeck) null);
  }

  private void FireOnTemplateDeckChosenEvent()
  {
    foreach (DeckTemplatePicker.OnTemplateDeckChosen templateDeckChosen in this.m_templateDeckChosenListeners.ToArray())
      templateDeckChosen();
  }

  [DebuggerHidden]
  private IEnumerator HideTrays()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTemplatePicker.\u003CHideTrays\u003Ec__Iterator5D()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ChooseRecipeAndFillInCards()
  {
    CollectionManager.Get().SetShowDeckTemplatePageForClass(this.m_currentSelectedClass, this.m_currentSelectedDeck != this.m_customDeck);
    CollectionDeckTray.Get().GetCardsContent().CommitFakeDeckChanges();
    this.FireOnTemplateDeckChosenEvent();
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    if (this.m_currentSelectedDeck != this.m_customDeck)
    {
      taggedDeck.SourceType = DeckSourceType.DECK_SOURCE_TYPE_TEMPLATE;
      Network.Get().SetDeckTemplateSource(taggedDeck.ID, this.m_currentSelectedDeck.m_id);
    }
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.StartCoroutine(this.EnterDeckPhone());
    if (!CollectionManager.Get().ShouldShowWildToStandardTutorial(true) || taggedDeck.IsWild)
      return;
    CollectionManagerDisplay.Get().ShowStandardInfoTutorial(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS);
  }

  private void ShowLowCardsPopup(Action confirmAction, Action cancelAction = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_COLLECTION_DECK_TEMPLATE_LOW_CARD_WARNING"),
      m_text = GameStrings.Get("GLUE_COLLECTION_DECK_TEMPLATE_LOW_CARD_WARNING_MESSAGE"),
      m_cancelText = GameStrings.Get("GLUE_COLLECTION_DECK_TEMPLATE_LOW_CARD_WARNING_CANCEL"),
      m_confirmText = GameStrings.Get("GLUE_COLLECTION_DECK_TEMPLATE_LOW_CARD_WARNING_CONFIRM"),
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_responseCallback = new AlertPopup.ResponseCallback(new DeckTemplatePicker.\u003CShowLowCardsPopup\u003Ec__AnonStorey3A2()
      {
        confirmAction = confirmAction,
        cancelAction = cancelAction
      }.\u003C\u003Em__F6)
    });
  }

  private void SelectCustomDeckButton(bool preselect = false)
  {
    CollectionDeckTray collectionDeckTray = CollectionDeckTray.Get();
    if ((UnityEngine.Object) collectionDeckTray != (UnityEngine.Object) null && !preselect)
      collectionDeckTray.FlashDeckTemplateHighlight();
    if ((UnityEngine.Object) this.m_chooseButton != (UnityEngine.Object) null)
      this.m_chooseButton.Enable();
    for (int index = 0; index < this.m_pickerButtons.Count; ++index)
      this.m_pickerButtons[index].SetSelected(false);
    if ((UnityEngine.Object) this.m_customDeckButton != (UnityEngine.Object) null)
      this.m_customDeckButton.SetSelected(true);
    if ((UnityEngine.Object) this.m_deckTemplateDescription != (UnityEngine.Object) null)
      this.m_deckTemplateDescription.Text = GameStrings.Get("GLUE_DECK_TEMPLATE_CUSTOM_DECK_DESCRIPTION");
    this.FillWithCustomDeck();
    this.m_currentSelectedDeck = this.m_customDeck;
    if (!(bool) UniversalInputManager.UsePhoneUI || preselect)
      return;
    this.ChooseRecipeAndFillInCards();
  }

  [DebuggerHidden]
  private IEnumerator EnterDeckPhone()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTemplatePicker.\u003CEnterDeckPhone\u003Ec__Iterator5E()
    {
      \u003C\u003Ef__this = this
    };
  }

  public delegate void OnTemplateDeckChosen();
}
