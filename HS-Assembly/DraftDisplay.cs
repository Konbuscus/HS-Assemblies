﻿// Decompiled with JetBrains decompiler
// Type: DraftDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class DraftDisplay : MonoBehaviour
{
  private static readonly Vector3 CHOICE_ACTOR_LOCAL_SCALE = new Vector3(7.2f, 7.2f, 7.2f);
  private static readonly Vector3 HERO_ACTOR_LOCAL_SCALE = new Vector3(8.285825f, 8.285825f, 8.285825f);
  private static readonly Vector3 HERO_LABEL_SCALE = new Vector3(8f, 8f, 8f);
  private static readonly Vector3 HERO_POWER_START_POSITION = new Vector3(0.0f, 0.0f, -0.3410472f);
  private static readonly Vector3 HERO_POWER_POSITION = new Vector3(1.40873f, 0.0f, -0.3410472f);
  private static readonly Vector3 HERO_POWER_SCALE = new Vector3(0.3419997f, 0.3419997f, 0.3419997f);
  private static readonly Vector3 CHOICE_ACTOR_LOCAL_SCALE_PHONE = new Vector3(14.5f, 14.5f, 14.5f);
  private static readonly Vector3 HERO_ACTOR_LOCAL_SCALE_PHONE = new Vector3(15.5f, 15.5f, 15.5f);
  private static readonly Vector3 HERO_LABEL_SCALE_PHONE = new Vector3(15f, 15f, 15f);
  private static readonly Vector3 HERO_POWER_START_POSITION_PHONE = new Vector3(1.6f, 0.3f, -0.15f);
  private static readonly Vector3 HERO_POWER_POSITION_PHONE = new Vector3(1.07f, 0.3f, -0.15f);
  private static readonly Vector3 HERO_POWER_SCALE_PHONE = new Vector3(0.5f, 0.5f, 0.5f);
  private List<DraftDisplay.DraftChoice> m_choices = new List<DraftDisplay.DraftChoice>();
  private FullDef[] m_heroPowerDefs = new FullDef[3];
  private bool m_animationsComplete = true;
  private List<HeroLabel> m_currentLabels = new List<HeroLabel>();
  private CardSoundSpell[] m_heroEmotes = new CardSoundSpell[3];
  private List<Achievement> m_newlyCompletedAchieves = new List<Achievement>();
  public Collider m_pickArea;
  public UberText m_instructionText;
  public UberText m_forgeLabel;
  public DraftManaCurve m_manaCurve;
  public GameObject m_heroLabel;
  public Spell m_DeckCompleteSpell;
  public float m_DeckCardBarFlareUpDelay;
  public PegUIElement m_heroClickCatcher;
  public DraftPhoneDeckTray m_draftDeckTray;
  [CustomEditField(Sections = "Buttons")]
  public UIBButton m_backButton;
  public StandardPegButtonNew m_retireButton;
  public PlayButton m_playButton;
  [CustomEditField(Sections = "Bones")]
  public Transform m_bigHeroBone;
  public Transform m_socketHeroBone;
  [CustomEditField(Sections = "Phone")]
  public GameObject m_PhonePlayButtonTray;
  public Transform m_PhoneBackButtonBone;
  public Transform m_PhoneDeckTrayHiddenBone;
  public GameObject m_Phone3WayButtonRoot;
  public GameObject m_PhoneChooseHero;
  public GameObject m_PhoneLargeViewDeckButton;
  public ArenaPhoneControl m_PhoneDeckControl;
  private static DraftDisplay s_instance;
  private DraftDisplay.DraftMode m_currentMode;
  private NormalButton m_confirmButton;
  private Actor m_heroPower;
  private bool m_netCacheReady;
  private bool m_questsHandled;
  private Actor m_chosenHero;
  private bool m_skipHeroEmotes;
  private bool m_isHeroAnimating;
  private DraftCardVisual m_zoomedHero;
  private bool m_wasDrafting;
  private bool m_firstTimeIntroComplete;

  private void Awake()
  {
    DraftDisplay.s_instance = this;
    AssetLoader.Get().LoadActor("DraftHeroChooseButton", new AssetLoader.GameObjectCallback(this.OnConfirmButtonLoaded), (object) null, false);
    AssetLoader.Get().LoadActor("History_HeroPower", new AssetLoader.GameObjectCallback(this.LoadHeroPowerCallback), (object) null, false);
    if ((bool) UniversalInputManager.UsePhoneUI)
      AssetLoader.Get().LoadGameObject("BackButton_phone", new AssetLoader.GameObjectCallback(this.OnPhoneBackButtonLoaded), (object) null, false);
    DraftManager.Get().RegisterDisplayHandlers();
    SceneMgr.Get().RegisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
    this.m_forgeLabel.Text = GameStrings.Get("GLUE_TOOLTIP_BUTTON_FORGE_HEADLINE");
    this.m_instructionText.Text = string.Empty;
    this.m_pickArea.enabled = false;
    if (!DemoMgr.Get().ArenaIs1WinMode())
      return;
    Options.Get().SetBool(Option.HAS_SEEN_FORGE_PLAY_MODE, false);
    Options.Get().SetBool(Option.HAS_SEEN_FORGE_CARD_CHOICE, false);
    Options.Get().SetBool(Option.HAS_SEEN_FORGE, true);
    Options.Get().SetBool(Option.HAS_SEEN_FORGE_HERO_CHOICE, true);
    Options.Get().SetBool(Option.HAS_SEEN_FORGE_CARD_CHOICE2, false);
  }

  private void OnDestroy()
  {
    DraftDisplay.s_instance = (DraftDisplay) null;
  }

  private void Start()
  {
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    NetCache.Get().RegisterScreenForge(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    this.SetupRetireButton();
    this.m_playButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.PlayButtonPress));
    this.m_manaCurve.GetComponent<PegUIElement>().AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.ManaCurveOver));
    this.m_manaCurve.GetComponent<PegUIElement>().AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.ManaCurveOut));
    this.m_playButton.SetText(GameStrings.Get("GLOBAL_PLAY"));
    this.ShowPhonePlayButton(false);
    if (!(bool) UniversalInputManager.UsePhoneUI)
      this.SetupBackButton();
    Network.FindOutCurrentDraftState();
    MusicManager.Get().StartPlaylist(MusicPlaylistType.UI_Arena);
    this.StartCoroutine(this.NotifySceneLoadedWhenReady());
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_draftDeckTray.gameObject.SetActive(true);
    ArenaTrayDisplay.Get().ShowPlainPaperBackground();
  }

  private void Update()
  {
    Network.Get().ProcessNetwork();
  }

  public static DraftDisplay Get()
  {
    return DraftDisplay.s_instance;
  }

  public void OnOpenRewardsComplete()
  {
    this.ExitDraftScene();
  }

  public void OnApplicationPause(bool pauseStatus)
  {
    if (!GameMgr.Get().IsFindingGame())
      return;
    this.CancelFindGame();
  }

  public void Unload()
  {
    Box.Get().SetToIgnoreFullScreenEffects(false);
    if ((UnityEngine.Object) this.m_confirmButton != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_confirmButton.gameObject);
    if ((UnityEngine.Object) this.m_heroPower != (UnityEngine.Object) null)
      this.m_heroPower.Destroy();
    if ((UnityEngine.Object) this.m_chosenHero != (UnityEngine.Object) null)
      this.m_chosenHero.Destroy();
    DraftManager.Get().UnregisterDisplayHandlers();
    DraftInputManager.Get().Unload();
  }

  public void AcceptNewChoices(List<NetCache.CardDefinition> choices)
  {
    this.DestroyOldChoices();
    this.UpdateInstructionText();
    this.StartCoroutine(this.WaitForAnimsToFinishAndThenDisplayNewChoices(choices));
  }

  [DebuggerHidden]
  private IEnumerator WaitForAnimsToFinishAndThenDisplayNewChoices(List<NetCache.CardDefinition> choices)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DraftDisplay.\u003CWaitForAnimsToFinishAndThenDisplayNewChoices\u003Ec__Iterator82() { choices = choices, \u003C\u0024\u003Echoices = choices, \u003C\u003Ef__this = this };
  }

  public void SetDraftMode(DraftDisplay.DraftMode mode)
  {
    bool flag = this.m_currentMode != mode;
    this.m_currentMode = mode;
    if (!flag)
      return;
    Log.Arena.Print("SetDraftMode - " + (object) this.m_currentMode);
    this.InitializeDraftScreen();
  }

  public DraftDisplay.DraftMode GetDraftMode()
  {
    return this.m_currentMode;
  }

  public void CancelFindGame()
  {
    GameMgr.Get().CancelFindGame();
    this.HandleGameStartupFailure();
  }

  public void OnHeroClicked(int heroChoice)
  {
    SoundManager.Get().LoadAndPlay("tournament_screen_select_hero");
    this.m_isHeroAnimating = true;
    DraftDisplay.DraftChoice choice = this.m_choices[heroChoice - 1];
    this.m_zoomedHero = choice.m_actor.GetCollider().gameObject.GetComponent<DraftCardVisual>();
    choice.m_actor.SetUnlit();
    iTween.MoveTo(choice.m_actor.gameObject, this.m_bigHeroBone.position, 0.25f);
    iTween.ScaleTo(choice.m_actor.gameObject, this.m_bigHeroBone.localScale, 0.25f);
    SoundManager.Get().LoadAndPlay("forge_hero_portrait_plate_rises");
    this.FadeEffectsIn();
    SceneUtils.SetLayer(choice.m_actor.gameObject, GameLayer.IgnoreFullScreenEffects);
    UniversalInputManager.Get().SetGameDialogActive(true);
    this.m_confirmButton.gameObject.SetActive(true);
    this.m_confirmButton.m_button.GetComponent<PlayMakerFSM>().SendEvent("Birth");
    this.m_confirmButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnConfirmButtonClicked));
    this.m_heroClickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCancelButtonClicked));
    this.m_heroClickCatcher.gameObject.SetActive(true);
    choice.m_actor.TurnOffCollider();
    choice.m_actor.SetActorState(ActorStateType.CARD_IDLE);
    bool flag = this.IsHeroEmoteSpellReady(heroChoice - 1);
    this.StartCoroutine(this.WaitForSpellToLoadAndPlay(heroChoice - 1));
    this.ShowHeroPowerBigCard();
    if (!this.CanAutoDraft() || !flag)
      return;
    this.OnConfirmButtonClicked((UIEvent) null);
  }

  private bool IsHeroEmoteSpellReady(int index)
  {
    if (!((UnityEngine.Object) this.m_heroEmotes[index] != (UnityEngine.Object) null))
      return this.m_skipHeroEmotes;
    return true;
  }

  [DebuggerHidden]
  private IEnumerator WaitForSpellToLoadAndPlay(int index)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DraftDisplay.\u003CWaitForSpellToLoadAndPlay\u003Ec__Iterator83() { index = index, \u003C\u0024\u003Eindex = index, \u003C\u003Ef__this = this };
  }

  private void OnConfirmButtonClicked(UIEvent e)
  {
    if (GameUtils.IsAnyTransitionActive())
      return;
    this.EnableBackButton(false);
    this.DoHeroSelectAnimation();
  }

  private void OnCancelButtonClicked(UIEvent e)
  {
    if (this.IsInHeroSelectMode())
      this.DoHeroCancelAnimation();
    else
      Navigation.GoBack();
  }

  private void RemoveListeners()
  {
    this.m_confirmButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnConfirmButtonClicked));
    this.m_confirmButton.m_button.GetComponent<PlayMakerFSM>().SendEvent("Death");
    this.m_confirmButton.gameObject.SetActive(false);
    this.m_heroClickCatcher.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCancelButtonClicked));
    this.m_heroClickCatcher.gameObject.SetActive(false);
  }

  private void FadeEffectsIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.SetBlurBrightness(1f);
    fullScreenFxMgr.SetBlurDesaturation(0.0f);
    fullScreenFxMgr.Vignette(0.8f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.Blur(1f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void FadeEffectsOut()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, new FullScreenFXMgr.EffectListener(this.OnFadeFinished));
    fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void OnFadeFinished()
  {
    if ((UnityEngine.Object) this.m_chosenHero == (UnityEngine.Object) null)
      return;
    SceneUtils.SetLayer(this.m_chosenHero.gameObject, GameLayer.Default);
  }

  public void DoHeroCancelAnimation()
  {
    this.RemoveListeners();
    this.m_heroPower.Hide();
    DraftDisplay.DraftChoice choice = this.m_choices[this.m_zoomedHero.GetChoiceNum() - 1];
    SceneUtils.SetLayer(choice.m_actor.gameObject, GameLayer.Default);
    choice.m_actor.TurnOnCollider();
    this.FadeEffectsOut();
    UniversalInputManager.Get().SetGameDialogActive(false);
    this.m_isHeroAnimating = false;
    this.m_pickArea.enabled = true;
    iTween.MoveTo(this.m_zoomedHero.GetActor().gameObject, this.GetCardPosition(this.m_zoomedHero.GetChoiceNum() - 1, true), 0.25f);
    if ((bool) UniversalInputManager.UsePhoneUI)
      iTween.ScaleTo(choice.m_actor.gameObject, DraftDisplay.HERO_ACTOR_LOCAL_SCALE_PHONE, 0.25f);
    else
      iTween.ScaleTo(choice.m_actor.gameObject, DraftDisplay.HERO_ACTOR_LOCAL_SCALE, 0.25f);
    this.m_pickArea.enabled = false;
    this.m_zoomedHero = (DraftCardVisual) null;
  }

  public bool IsInHeroSelectMode()
  {
    return (UnityEngine.Object) this.m_zoomedHero != (UnityEngine.Object) null;
  }

  private void DoHeroSelectAnimation()
  {
    this.RemoveListeners();
    this.m_heroPower.Hide();
    this.FadeEffectsOut();
    UniversalInputManager.Get().SetGameDialogActive(false);
    this.m_chosenHero = this.m_zoomedHero.GetActor();
    this.m_zoomedHero.SetChosenFlag(true);
    DraftManager.Get().MakeChoice(this.m_zoomedHero.GetChoiceNum());
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_zoomedHero.GetActor().transform.parent = this.m_socketHeroBone;
      iTween.MoveTo(this.m_zoomedHero.GetActor().gameObject, iTween.Hash((object) "position", (object) Vector3.zero, (object) "time", (object) 0.25f, (object) "isLocal", (object) true, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) "PhoneHeroAnimationFinished", (object) "oncompletetarget", (object) this.gameObject));
      iTween.ScaleTo(this.m_zoomedHero.GetActor().gameObject, iTween.Hash((object) "scale", (object) Vector3.one, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeInCubic));
    }
    else
    {
      this.m_zoomedHero.GetActor().ActivateSpellBirthState(SpellType.CONSTRUCT);
      this.m_zoomedHero = (DraftCardVisual) null;
      this.m_isHeroAnimating = false;
    }
    SoundManager.Get().LoadAndPlay("forge_hero_portrait_plate_descend_and_impact");
    if (Options.Get().GetBool(Option.HAS_SEEN_FORGE_CARD_CHOICE, false) || !UserAttentionManager.CanShowAttentionGrabber("DraftDisplay.DoHeroSelectAnimation:" + (object) Option.HAS_SEEN_FORGE_CARD_CHOICE))
      return;
    NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_FORGE_INST2_20"), "VO_INNKEEPER_FORGE_INST2_20", 3f, (Action) null, false);
    Options.Get().SetBool(Option.HAS_SEEN_FORGE_CARD_CHOICE, true);
  }

  private void PhoneHeroAnimationFinished()
  {
    Log.Arena.Print("Phone Hero animation complete");
    this.m_zoomedHero = (DraftCardVisual) null;
    this.m_isHeroAnimating = false;
  }

  public void AddCardToManaCurve(EntityDef entityDef)
  {
    if ((UnityEngine.Object) this.m_manaCurve == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) string.Format("DraftDisplay.AddCardToManaCurve({0}) - m_manaCurve is null", (object) entityDef));
    else
      this.m_manaCurve.AddCardToManaCurve(entityDef);
  }

  public List<DraftCardVisual> GetCardVisuals()
  {
    List<DraftCardVisual> draftCardVisualList = new List<DraftCardVisual>();
    using (List<DraftDisplay.DraftChoice>.Enumerator enumerator = this.m_choices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DraftDisplay.DraftChoice current = enumerator.Current;
        if ((UnityEngine.Object) current.m_actor == (UnityEngine.Object) null)
          return (List<DraftCardVisual>) null;
        DraftCardVisual component = current.m_actor.GetCollider().gameObject.GetComponent<DraftCardVisual>();
        if ((UnityEngine.Object) component == (UnityEngine.Object) null)
          return (List<DraftCardVisual>) null;
        draftCardVisualList.Add(component);
      }
    }
    return draftCardVisualList;
  }

  public void HandleGameStartupFailure()
  {
    this.m_playButton.Enable();
    this.ShowPhonePlayButton(true);
    PresenceMgr.Get().SetPrevStatus();
  }

  public void DoDeckCompleteAnims()
  {
    SoundManager.Get().LoadAndPlay("forge_commit_deck");
    if ((bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_DeckCompleteSpell.Activate();
    if (!((UnityEngine.Object) this.m_draftDeckTray != (UnityEngine.Object) null))
      return;
    this.m_draftDeckTray.GetCardsContent().ShowDeckCompleteEffects();
  }

  [DebuggerHidden]
  private IEnumerator NotifySceneLoadedWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DraftDisplay.\u003CNotifySceneLoadedWhenReady\u003Ec__Iterator84() { \u003C\u003Ef__this = this };
  }

  private void ShowNewQuests()
  {
    if (this.m_questsHandled || this.m_currentMode == DraftDisplay.DraftMode.IN_REWARDS)
      return;
    this.m_questsHandled = true;
    if (AchieveManager.Get().HasQuestsToShow(true))
    {
      WelcomeQuests.Show(UserAttentionBlocker.NONE, false, new WelcomeQuests.DelOnWelcomeQuestsClosed(this.OnNewQuestsShown), false);
    }
    else
    {
      this.OnNewQuestsShown();
      GameToastMgr.Get().UpdateQuestProgressToasts();
    }
  }

  private void OnNewQuestsShown()
  {
    this.m_newlyCompletedAchieves = AchieveManager.Get().GetNewCompletedAchieves();
    this.ShowNextCompletedQuestToast((object) null);
  }

  private void ShowNextCompletedQuestToast(object userData)
  {
    if (this.m_newlyCompletedAchieves.Count == 0)
      return;
    Achievement completedAchieve = this.m_newlyCompletedAchieves[0];
    this.m_newlyCompletedAchieves.RemoveAt(0);
    QuestToast.ShowQuestToast(UserAttentionBlocker.NONE, new QuestToast.DelOnCloseQuestToast(this.ShowNextCompletedQuestToast), true, completedAchieve);
  }

  private void InitializeDraftScreen()
  {
    if (!this.m_firstTimeIntroComplete && !Options.Get().GetBool(Option.HAS_SEEN_FORGE, false) && UserAttentionManager.CanShowAttentionGrabber("DraftDisplay.InitializeDraftScreen:" + (object) Option.HAS_SEEN_FORGE))
    {
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.ARENA_PURCHASE);
      this.m_firstTimeIntroComplete = true;
      this.DoFirstTimeIntro();
    }
    else
    {
      switch (this.m_currentMode)
      {
        case DraftDisplay.DraftMode.NO_ACTIVE_DRAFT:
          PresenceMgr.Get().SetStatus((Enum) PresenceStatus.ARENA_PURCHASE);
          this.ShowPurchaseScreen();
          break;
        case DraftDisplay.DraftMode.DRAFTING:
          if (StoreManager.Get().HasOutstandingPurchaseNotices(ProductType.PRODUCT_TYPE_DRAFT))
            this.ShowPurchaseScreen();
          PresenceMgr.Get().SetStatus((Enum) PresenceStatus.ARENA_FORGE);
          this.ShowCurrentlyDraftingScreen();
          break;
        case DraftDisplay.DraftMode.ACTIVE_DRAFT_DECK:
          PresenceMgr.Get().SetStatus((Enum) PresenceStatus.ARENA_IDLE);
          this.StartCoroutine(this.ShowActiveDraftScreen());
          break;
        case DraftDisplay.DraftMode.IN_REWARDS:
          PresenceMgr.Get().SetStatus((Enum) PresenceStatus.ARENA_REWARD);
          this.ShowDraftRewardsScreen();
          break;
        default:
          UnityEngine.Debug.LogError((object) string.Format("DraftDisplay.InitializeDraftScreen(): don't know how to handle m_currentMode = {0}", (object) this.m_currentMode));
          break;
      }
    }
  }

  private void OnConfirmButtonLoaded(string name, GameObject go, object callbackData)
  {
    this.m_confirmButton = go.GetComponent<NormalButton>();
    this.m_confirmButton.SetText(GameStrings.Get("GLUE_CHOOSE"));
    this.m_confirmButton.gameObject.SetActive(false);
    SceneUtils.SetLayer(go, GameLayer.IgnoreFullScreenEffects);
  }

  private void OnPhoneBackButtonLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Phone Back Button failed to load!");
    }
    else
    {
      this.m_backButton = go.GetComponent<UIBButton>();
      this.m_backButton.transform.parent = this.m_PhoneBackButtonBone;
      this.m_backButton.transform.position = this.m_PhoneBackButtonBone.position;
      this.m_backButton.transform.localScale = this.m_PhoneBackButtonBone.localScale;
      this.m_backButton.transform.rotation = Quaternion.identity;
      SceneUtils.SetLayer(go, GameLayer.Default);
      this.SetupBackButton();
    }
  }

  private void LoadHeroPowerCallback(string actorName, GameObject actorObject, object callbackData)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.LoadHeroPowerCallback() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.LoadHeroPowerCallback() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        component.TurnOffCollider();
        SceneUtils.SetLayer(component.gameObject, GameLayer.IgnoreFullScreenEffects);
        this.m_heroPower = component;
        component.Hide();
      }
    }
  }

  private void ShowHeroPowerBigCard()
  {
    if ((UnityEngine.Object) this.m_heroPower == (UnityEngine.Object) null)
      return;
    SceneUtils.SetLayer(this.m_heroPower.gameObject, GameLayer.IgnoreFullScreenEffects);
    this.m_heroPower.gameObject.transform.parent = this.m_zoomedHero.GetActor().transform;
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_heroPower.gameObject.transform.localPosition = DraftDisplay.HERO_POWER_START_POSITION_PHONE;
      this.m_heroPower.gameObject.transform.localScale = DraftDisplay.HERO_POWER_SCALE_PHONE;
    }
    else
    {
      this.m_heroPower.gameObject.transform.localPosition = DraftDisplay.HERO_POWER_START_POSITION;
      this.m_heroPower.gameObject.transform.localScale = DraftDisplay.HERO_POWER_SCALE;
    }
    this.StartCoroutine(this.ShowHeroPowerWhenDefIsLoaded());
  }

  [DebuggerHidden]
  private IEnumerator ShowHeroPowerWhenDefIsLoaded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DraftDisplay.\u003CShowHeroPowerWhenDefIsLoaded\u003Ec__Iterator85() { \u003C\u003Ef__this = this };
  }

  private void DestroyOldChoices()
  {
    this.m_animationsComplete = false;
    using (List<DraftDisplay.DraftChoice>.Enumerator enumerator1 = this.m_choices.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        DraftDisplay.DraftChoice current = enumerator1.Current;
        Actor actor = current.m_actor;
        if (!((UnityEngine.Object) actor == (UnityEngine.Object) null))
        {
          DraftCardVisual component = actor.GetCollider().gameObject.GetComponent<DraftCardVisual>();
          actor.TurnOffCollider();
          Spell spell = actor.GetSpell(this.GetSpellTypeForRarity(actor.GetEntityDef().GetRarity()));
          if (component.IsChosen())
          {
            if (!actor.GetEntityDef().IsHero())
            {
              this.AddCardToManaCurve(actor.GetEntityDef());
              actor.GetSpell(SpellType.SUMMON_OUT_FORGE).AddFinishedCallback(new Spell.FinishedCallback(this.DestroyChoiceOnSpellFinish), (object) actor);
              actor.ActivateSpellBirthState(SpellType.SUMMON_OUT_FORGE);
              spell.ActivateState(SpellStateType.DEATH);
              SoundManager.Get().LoadAndPlay("forge_select_card_1");
              this.m_draftDeckTray.GetCardsContent().UpdateCardList(current.m_cardID, true, actor);
            }
            else
            {
              using (List<HeroLabel>.Enumerator enumerator2 = this.m_currentLabels.GetEnumerator())
              {
                while (enumerator2.MoveNext())
                  enumerator2.Current.FadeOut();
              }
            }
          }
          else
          {
            SoundManager.Get().LoadAndPlay("unselected_cards_dissipate");
            actor.GetSpell(SpellType.BURN).AddFinishedCallback(new Spell.FinishedCallback(this.DestroyChoiceOnSpellFinish), (object) actor);
            actor.ActivateSpellBirthState(SpellType.BURN);
            if (actor.GetEntityDef().IsHero())
              actor.Hide(true);
            if ((UnityEngine.Object) spell != (UnityEngine.Object) null)
              spell.ActivateState(SpellStateType.DEATH);
          }
        }
      }
    }
    this.StartCoroutine(this.CompleteAnims());
    this.m_choices.Clear();
  }

  [DebuggerHidden]
  private IEnumerator CompleteAnims()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DraftDisplay.\u003CCompleteAnims\u003Ec__Iterator86() { \u003C\u003Ef__this = this };
  }

  private void DestroyChoiceOnSpellFinish(Spell spell, object actorObject)
  {
    this.StartCoroutine(this.DestroyObjectAfterDelay(((Component) actorObject).gameObject));
  }

  [DebuggerHidden]
  private IEnumerator DestroyObjectAfterDelay(GameObject gameObjectToDestroy)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DraftDisplay.\u003CDestroyObjectAfterDelay\u003Ec__Iterator87() { gameObjectToDestroy = gameObjectToDestroy, \u003C\u0024\u003EgameObjectToDestroy = gameObjectToDestroy };
  }

  private void OnFullDefLoaded(string cardID, FullDef def, object userData)
  {
    DraftDisplay.ChoiceCallback choiceCallback = (DraftDisplay.ChoiceCallback) userData;
    choiceCallback.fullDef = def;
    if (def.GetEntityDef().IsHero())
    {
      AssetLoader.Get().LoadActor(ActorNames.GetZoneActor(def.GetEntityDef(), TAG_ZONE.PLAY), new AssetLoader.GameObjectCallback(this.OnActorLoaded), (object) choiceCallback, false);
      AssetLoader.Get().LoadCardPrefab(def.GetEntityDef().GetCardId(), new AssetLoader.GameObjectCallback(this.OnCardDefLoaded), (object) choiceCallback.choiceID, false);
      DefLoader.Get().LoadFullDef(GameUtils.GetHeroPowerCardIdFromHero(def.GetEntityDef().GetCardId()), new DefLoader.LoadDefCallback<FullDef>(this.OnHeroPowerFullDefLoaded), (object) choiceCallback.choiceID);
    }
    else
      AssetLoader.Get().LoadActor(ActorNames.GetHandActor(def.GetEntityDef()), new AssetLoader.GameObjectCallback(this.OnActorLoaded), (object) choiceCallback, false);
  }

  private void OnHeroPowerFullDefLoaded(string cardID, FullDef def, object userData)
  {
    this.m_heroPowerDefs[(int) userData - 1] = def;
  }

  private void UpdateInstructionText()
  {
    if (this.GetDraftMode() == DraftDisplay.DraftMode.DRAFTING)
    {
      if (DraftManager.Get().GetSlot() == 0)
      {
        if (!Options.Get().GetBool(Option.HAS_SEEN_FORGE_HERO_CHOICE, false) && UserAttentionManager.CanShowAttentionGrabber("DraftDisplay.UpdateInstructionText:" + (object) Option.HAS_SEEN_FORGE_HERO_CHOICE))
        {
          NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_FORGE_INST1_19"), "VO_INNKEEPER_FORGE_INST1_19", 3f, (Action) null, false);
          Options.Get().SetBool(Option.HAS_SEEN_FORGE_HERO_CHOICE, true);
        }
      }
      else if (DraftManager.Get().GetSlot() == 2 && !Options.Get().GetBool(Option.HAS_SEEN_FORGE_CARD_CHOICE2, false) && UserAttentionManager.CanShowAttentionGrabber("DraftDisplay.UpdateInstructionText:" + (object) Option.HAS_SEEN_FORGE_CARD_CHOICE2))
      {
        NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_FORGE_INST3_21"), "VO_INNKEEPER_FORGE_INST3_21", 3f, (Action) null, false);
        Options.Get().SetBool(Option.HAS_SEEN_FORGE_CARD_CHOICE2, true);
      }
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        if (DraftManager.Get().GetSlot() == 0)
          this.m_PhoneDeckControl.SetMode(ArenaPhoneControl.ControlMode.ChooseHero);
        else if (DraftManager.Get().GetDraftDeck().GetTotalCardCount() > 0)
          this.m_PhoneDeckControl.SetMode(ArenaPhoneControl.ControlMode.CardCountViewDeck);
        else
          this.m_PhoneDeckControl.SetMode(ArenaPhoneControl.ControlMode.ChooseCard);
      }
      else
        this.m_instructionText.Text = DraftManager.Get().GetSlot() != 0 ? GameStrings.Get("GLUE_DRAFT_INSTRUCTIONS") : GameStrings.Get("GLUE_DRAFT_HERO_INSTRUCTIONS");
    }
    else if (this.GetDraftMode() == DraftDisplay.DraftMode.ACTIVE_DRAFT_DECK)
    {
      if ((bool) UniversalInputManager.UsePhoneUI)
        this.m_PhoneDeckControl.SetMode(ArenaPhoneControl.ControlMode.ViewDeck);
      else
        this.m_instructionText.Text = GameStrings.Get("GLUE_DRAFT_MATCH_PROG");
    }
    else
      this.m_instructionText.Text = string.Empty;
  }

  private void DoFirstTimeIntro()
  {
    Box.Get().SetToIgnoreFullScreenEffects(true);
    if (!(bool) UniversalInputManager.UsePhoneUI)
      this.m_playButton.Disable();
    this.ShowPhonePlayButton(false);
    this.m_retireButton.Disable();
    if ((bool) ((UnityEngine.Object) this.m_manaCurve))
      this.m_manaCurve.ResetBars();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      StoreManager.Get().StartArenaTransaction(new Store.ExitCallback(this.OnStoreBackButtonPressed), (object) null, true);
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_ARENA_1ST_TIME_HEADER"),
      m_text = GameStrings.Get("GLUE_ARENA_1ST_TIME_DESC"),
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK,
      m_responseCallback = new AlertPopup.ResponseCallback(this.OnFirstTimeIntroOkButtonPressed)
    });
    SoundManager.Get().LoadAndPlay("VO_INNKEEPER_ARENA_INTRO2");
  }

  private void OnFirstTimeIntroOkButtonPressed(AlertPopup.Response response, object userData)
  {
    StoreManager.Get().HideStore(StoreType.ARENA_STORE);
    DraftManager.Get().RequestDraftStart();
    Options.Get().SetBool(Option.HAS_SEEN_FORGE, true);
  }

  private void ShowPurchaseScreen()
  {
    Box.Get().SetToIgnoreFullScreenEffects(true);
    if (!(bool) UniversalInputManager.UsePhoneUI)
      this.m_playButton.Disable();
    this.ShowPhonePlayButton(false);
    this.m_retireButton.Disable();
    if ((bool) ((UnityEngine.Object) this.m_manaCurve))
      this.m_manaCurve.ResetBars();
    if (DemoMgr.Get().ArenaIs1WinMode())
      Network.PurchaseViaGold(1, ProductType.PRODUCT_TYPE_DRAFT, 0);
    else
      StoreManager.Get().StartArenaTransaction(new Store.ExitCallback(this.OnStoreBackButtonPressed), (object) null, false);
  }

  private void ShowCurrentlyDraftingScreen()
  {
    this.m_wasDrafting = true;
    ArenaTrayDisplay.Get().ShowPlainPaperBackground();
    StoreManager.Get().HideStore(StoreType.ARENA_STORE);
    this.UpdateInstructionText();
    this.m_retireButton.Disable();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      this.m_playButton.Disable();
    this.ShowPhonePlayButton(false);
    this.LoadAndPositionHeroCard();
  }

  [DebuggerHidden]
  private IEnumerator ShowActiveDraftScreen()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DraftDisplay.\u003CShowActiveDraftScreen\u003Ec__Iterator88() { \u003C\u003Ef__this = this };
  }

  private void ShowDraftRewardsScreen()
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
      this.m_playButton.Disable();
    this.ShowPhonePlayButton(false);
    this.m_retireButton.Disable();
    if (DemoMgr.Get().ArenaIs1WinMode())
    {
      this.StartCoroutine(this.RestartArena());
    }
    else
    {
      if (DraftManager.Get().DeckWasActiveDuringSession())
      {
        if (DraftManager.Get().GetWins() >= DraftManager.Get().GetMaxWins() && !Options.Get().GetBool(Option.HAS_SEEN_FORGE_MAX_WIN, false) && UserAttentionManager.CanShowAttentionGrabber("DraftDisplay.ShowDraftRewardsScreen:" + (object) Option.HAS_SEEN_FORGE_MAX_WIN))
        {
          NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_MAX_ARENA_WINS_04"), "VO_INNKEEPER_MAX_ARENA_WINS_04", 0.0f, (Action) null, false);
          Options.Get().SetBool(Option.HAS_SEEN_FORGE_MAX_WIN, true);
        }
        ArenaTrayDisplay.Get().UpdateTray(false);
        ArenaTrayDisplay.Get().ActivateKey();
        if ((UnityEngine.Object) this.m_PhoneDeckControl != (UnityEngine.Object) null)
          this.m_PhoneDeckControl.SetMode(ArenaPhoneControl.ControlMode.Rewards);
      }
      else
        ArenaTrayDisplay.Get().ShowRewardsOpenAtStart();
      this.LoadAndPositionHeroCard();
    }
  }

  [DebuggerHidden]
  private IEnumerator RestartArena()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DraftDisplay.\u003CRestartArena\u003Ec__Iterator89() { \u003C\u003Ef__this = this };
  }

  private void LastArenaWinsLabelLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    int num = (int) callbackData;
    actorObject.GetComponent<UberText>().Text = "Last Arena: " + (object) num + " Wins";
    actorObject.transform.position = new Vector3(11.40591f, 1.341853f, 29.28797f);
    actorObject.transform.localScale = new Vector3(15f, 15f, 15f);
  }

  private void LoadAndPositionHeroCard()
  {
    if ((UnityEngine.Object) this.m_chosenHero != (UnityEngine.Object) null)
      return;
    CollectionDeck draftDeck = DraftManager.Get().GetDraftDeck();
    if (draftDeck == null)
      Log.Rachelle.Print("bug 8052, null exception");
    else
      GameUtils.LoadAndPositionHeroCard(draftDeck.HeroCardID, draftDeck.HeroPremium, new GameUtils.LoadHeroActorCallback(this.OnHeroActorLoaded));
  }

  private void OnNetCacheReady()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    if (!NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>().Games.Forge)
    {
      if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
        return;
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
      Error.AddWarningLoc("GLOBAL_FEATURE_DISABLED_TITLE", "GLOBAL_FEATURE_DISABLED_MESSAGE_FORGE");
    }
    else
      this.m_netCacheReady = true;
  }

  private void PositionAndShowChoices()
  {
    this.m_pickArea.enabled = true;
    for (int cardChoice = 0; cardChoice < this.m_choices.Count; ++cardChoice)
    {
      DraftDisplay.DraftChoice choice = this.m_choices[cardChoice];
      if ((UnityEngine.Object) choice.m_actor == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("DraftDisplay.PositionAndShowChoices(): WARNING found choice with null actor (cardID = {0}). Skipping...", (object) choice.m_cardID));
      }
      else
      {
        bool isHero = choice.m_actor.GetEntityDef().IsHero();
        choice.m_actor.transform.position = this.GetCardPosition(cardChoice, isHero);
        choice.m_actor.Show();
        choice.m_actor.ActivateSpellBirthState(SpellType.SUMMON_IN_FORGE);
        TAG_RARITY rarity = choice.m_actor.GetEntityDef().GetRarity();
        choice.m_actor.ActivateSpellBirthState(this.GetSpellTypeForRarity(rarity));
        switch (rarity)
        {
          case TAG_RARITY.COMMON:
          case TAG_RARITY.FREE:
            SoundManager.Get().LoadAndPlay("forge_normal_card_appears");
            break;
          case TAG_RARITY.RARE:
          case TAG_RARITY.EPIC:
          case TAG_RARITY.LEGENDARY:
            SoundManager.Get().LoadAndPlay("forge_rarity_card_appears");
            break;
        }
        if (isHero)
        {
          if (cardChoice == 0 && DemoMgr.Get().ArenaIs1WinMode())
            DemoMgr.Get().CreateDemoText(GameStrings.Get("GLUE_BLIZZCON2013_ARENA"), false, true);
          choice.m_actor.GetHealthObject().Hide();
          GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_heroLabel);
          gameObject.transform.position = choice.m_actor.GetMeshRenderer().transform.position;
          HeroLabel component = gameObject.GetComponent<HeroLabel>();
          if ((bool) UniversalInputManager.UsePhoneUI)
          {
            choice.m_actor.transform.localScale = DraftDisplay.HERO_ACTOR_LOCAL_SCALE_PHONE;
            gameObject.transform.localScale = DraftDisplay.HERO_LABEL_SCALE_PHONE;
          }
          else
          {
            choice.m_actor.transform.localScale = DraftDisplay.HERO_ACTOR_LOCAL_SCALE;
            gameObject.transform.localScale = DraftDisplay.HERO_LABEL_SCALE;
          }
          component.UpdateText(choice.m_actor.GetEntityDef().GetName(), GameStrings.GetClassName(choice.m_actor.GetEntityDef().GetClass()).ToUpper());
          this.m_currentLabels.Add(component);
        }
        else if ((bool) UniversalInputManager.UsePhoneUI)
          choice.m_actor.transform.localScale = DraftDisplay.CHOICE_ACTOR_LOCAL_SCALE_PHONE;
        else
          choice.m_actor.transform.localScale = DraftDisplay.CHOICE_ACTOR_LOCAL_SCALE;
      }
    }
    this.EnableBackButton(true);
    this.StartCoroutine(this.RunAutoDraftCheat());
    this.m_pickArea.enabled = false;
  }

  private bool CanAutoDraft()
  {
    return ApplicationMgr.IsInternal() && Vars.Key("Arena.AutoDraft").GetBool(false);
  }

  [DebuggerHidden]
  public IEnumerator RunAutoDraftCheat()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DraftDisplay.\u003CRunAutoDraftCheat\u003Ec__Iterator8A() { \u003C\u003Ef__this = this };
  }

  private Vector3 GetCardPosition(int cardChoice, bool isHero)
  {
    float num1 = this.m_pickArea.bounds.center.x - this.m_pickArea.bounds.extents.x;
    float num2 = this.m_pickArea.bounds.size.x / 3f;
    float num3 = this.m_choices.Count != 2 ? (float) (-(double) num2 / 2.0) : 0.0f;
    float num4 = 0.0f;
    if (isHero)
      num4 = 1f;
    return new Vector3(num1 + (float) (cardChoice + 1) * num2 + num3, this.m_pickArea.transform.position.y, this.m_pickArea.transform.position.z + num4);
  }

  private SpellType GetSpellTypeForRarity(TAG_RARITY rarity)
  {
    switch (rarity)
    {
      case TAG_RARITY.RARE:
        return SpellType.BURST_RARE;
      case TAG_RARITY.EPIC:
        return SpellType.BURST_EPIC;
      case TAG_RARITY.LEGENDARY:
        return SpellType.BURST_LEGENDARY;
      default:
        return SpellType.BURST_COMMON;
    }
  }

  private void OnHeroActorLoaded(Actor actor)
  {
    this.m_chosenHero = actor;
    this.m_chosenHero.transform.parent = this.m_socketHeroBone.transform;
    this.m_chosenHero.transform.localPosition = Vector3.zero;
    this.m_chosenHero.transform.localScale = Vector3.one;
  }

  private void OnActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DraftDisplay.\u003COnActorLoaded\u003Ec__AnonStorey3B0 loadedCAnonStorey3B0 = new DraftDisplay.\u003COnActorLoaded\u003Ec__AnonStorey3B0();
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DraftDisplay.OnActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("DraftDisplay.OnActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        DraftDisplay.ChoiceCallback choiceCallback = (DraftDisplay.ChoiceCallback) callbackData;
        FullDef fullDef = choiceCallback.fullDef;
        // ISSUE: reference to a compiler-generated field
        loadedCAnonStorey3B0.entityDef = fullDef.GetEntityDef();
        CardDef cardDef = fullDef.GetCardDef();
        // ISSUE: reference to a compiler-generated method
        DraftDisplay.DraftChoice draftChoice = this.m_choices.Find(new Predicate<DraftDisplay.DraftChoice>(loadedCAnonStorey3B0.\u003C\u003Em__126));
        if (draftChoice == null)
        {
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          UnityEngine.Debug.LogWarning((object) string.Format("DraftDisplay.OnActorLoaded(): Could not find draft choice {0} (cardID = {1}) in m_choices.", (object) loadedCAnonStorey3B0.entityDef.GetName(), (object) loadedCAnonStorey3B0.entityDef.GetCardId()));
          UnityEngine.Object.Destroy((UnityEngine.Object) component);
        }
        else
        {
          draftChoice.m_actor = component;
          draftChoice.m_actor.SetPremium(draftChoice.m_premium);
          // ISSUE: reference to a compiler-generated field
          draftChoice.m_actor.SetEntityDef(loadedCAnonStorey3B0.entityDef);
          draftChoice.m_actor.SetCardDef(cardDef);
          draftChoice.m_actor.UpdateAllComponents();
          draftChoice.m_actor.gameObject.name = cardDef.name + "_actor";
          draftChoice.m_actor.ContactShadow(true);
          DraftCardVisual draftCardVisual = draftChoice.m_actor.GetCollider().gameObject.AddComponent<DraftCardVisual>();
          draftCardVisual.SetActor(draftChoice.m_actor);
          draftCardVisual.SetChoiceNum(choiceCallback.choiceID);
          if (this.HaveActorsForAllChoices())
            this.PositionAndShowChoices();
          else
            draftChoice.m_actor.Hide();
        }
      }
    }
  }

  private void OnCardDefLoaded(string cardID, GameObject cardObject, object callbackData)
  {
    using (List<EmoteEntryDef>.Enumerator enumerator = cardObject.GetComponent<CardDef>().m_EmoteDefs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EmoteEntryDef current = enumerator.Current;
        if (current.m_emoteType == EmoteType.PICKED)
          AssetLoader.Get().LoadSpell(current.m_emoteSoundSpellPath, new AssetLoader.GameObjectCallback(this.OnStartEmoteLoaded), callbackData, false);
      }
    }
  }

  private void OnStartEmoteLoaded(string name, GameObject go, object callbackData)
  {
    CardSoundSpell cardSoundSpell = (CardSoundSpell) null;
    if ((UnityEngine.Object) go != (UnityEngine.Object) null)
      cardSoundSpell = go.GetComponent<CardSoundSpell>();
    this.m_skipHeroEmotes |= (UnityEngine.Object) cardSoundSpell == (UnityEngine.Object) null;
    if (this.m_skipHeroEmotes)
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else
      this.m_heroEmotes[(int) callbackData - 1] = cardSoundSpell;
  }

  private bool HaveActorsForAllChoices()
  {
    using (List<DraftDisplay.DraftChoice>.Enumerator enumerator = this.m_choices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (!((UnityEngine.Object) enumerator.Current.m_actor != (UnityEngine.Object) null))
          return false;
      }
    }
    return true;
  }

  private void InitManaCurve()
  {
    CollectionDeck draftDeck = DraftManager.Get().GetDraftDeck();
    if (draftDeck == null)
      return;
    using (List<CollectionDeckSlot>.Enumerator enumerator = draftDeck.GetSlots().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        EntityDef entityDef = DefLoader.Get().GetEntityDef(current.CardID);
        for (int index = 0; index < current.Count; ++index)
          this.AddCardToManaCurve(entityDef);
      }
    }
  }

  private void OnStoreBackButtonPressed(bool authorizationBackButtonPressed, object userData)
  {
    this.ExitDraftScene();
  }

  private bool OnNavigateBack()
  {
    if (this.IsInHeroSelectMode())
    {
      this.DoHeroCancelAnimation();
      return false;
    }
    if ((UnityEngine.Object) ArenaTrayDisplay.Get() == (UnityEngine.Object) null)
      return false;
    ArenaTrayDisplay.Get().KeyFXCancel();
    this.ExitDraftScene();
    return true;
  }

  private void BackButtonPress(UIEvent e)
  {
    Navigation.GoBack();
  }

  private void ExitDraftScene()
  {
    GameMgr.Get().CancelFindGame();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      this.m_playButton.Disable();
    this.ShowPhonePlayButton(false);
    StoreManager.Get().HideStore(StoreType.ARENA_STORE);
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
  }

  private void PlayButtonPress(UIEvent e)
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
      this.m_playButton.Disable();
    this.ShowPhonePlayButton(false);
    DraftManager.Get().FindGame();
    PresenceMgr.Get().SetStatus((Enum) PresenceStatus.ARENA_QUEUE);
  }

  private void RetireButtonPress(UIEvent e)
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_FORGE_RETIRE_WARNING_HEADER"),
      m_text = GameStrings.Get("GLUE_FORGE_RETIRE_WARNING_DESC"),
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_responseCallback = new AlertPopup.ResponseCallback(this.OnRetirePopupResponse)
    });
  }

  private void OnRetirePopupResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
      return;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_draftDeckTray.gameObject.GetComponent<SlidingTray>().HideTray();
    DraftManager draftManager = DraftManager.Get();
    this.m_retireButton.Disable();
    Network.RetireDraftDeck(draftManager.GetDraftDeck().ID, draftManager.GetSlot());
  }

  private void ManaCurveOver(UIEvent e)
  {
    this.m_manaCurve.GetComponent<TooltipZone>().ShowTooltip(GameStrings.Get("GLUE_FORGE_MANATIP_HEADER"), GameStrings.Get("GLUE_FORGE_MANATIP_DESC"), (float) (!(bool) UniversalInputManager.UsePhoneUI ? TooltipPanel.FORGE_SCALE : TooltipPanel.BOX_SCALE), true);
  }

  private void ManaCurveOut(UIEvent e)
  {
    this.m_manaCurve.GetComponent<TooltipZone>().HideTooltip();
  }

  private void DeckHeaderOver(UIEvent e)
  {
    this.m_draftDeckTray.GetTooltipZone().ShowTooltip(GameStrings.Get("GLUE_ARENA_DECK_TOOLTIP_HEADER"), GameStrings.Get("GLUE_ARENA_DECK_TOOLTIP"), (float) TooltipPanel.FORGE_SCALE, true);
  }

  private void DeckHeaderOut(UIEvent e)
  {
    this.m_draftDeckTray.GetTooltipZone().HideTooltip();
  }

  private void SetupBackButton()
  {
    if (DemoMgr.Get().CantExitArena())
    {
      this.m_backButton.SetText(string.Empty);
    }
    else
    {
      this.m_backButton.SetText(GameStrings.Get("GLOBAL_BACK"));
      this.m_backButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.BackButtonPress));
    }
  }

  private void EnableBackButton(bool buttonEnabled)
  {
    this.m_backButton.enabled = buttonEnabled;
  }

  private void SetupRetireButton()
  {
    if (DemoMgr.Get().CantExitArena())
    {
      this.m_retireButton.SetText(string.Empty);
    }
    else
    {
      this.m_retireButton.SetText(GameStrings.Get("GLUE_DRAFT_RETIRE_BUTTON"));
      this.m_retireButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.RetireButtonPress));
    }
  }

  private void ShowPhonePlayButton(bool show)
  {
    if ((UnityEngine.Object) this.m_PhonePlayButtonTray == (UnityEngine.Object) null)
      return;
    SlidingTray component = this.m_PhonePlayButtonTray.GetComponent<SlidingTray>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    component.ToggleTraySlider(show, (Transform) null, true);
  }

  private void OnScenePreUnload(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    if (prevMode != SceneMgr.Mode.DRAFT || !this.IsInHeroSelectMode())
      return;
    this.m_zoomedHero.gameObject.SetActive(false);
    this.m_heroPower.gameObject.SetActive(false);
    this.m_confirmButton.gameObject.SetActive(false);
    UniversalInputManager.Get().SetGameDialogActive(false);
  }

  public enum DraftMode
  {
    INVALID,
    NO_ACTIVE_DRAFT,
    DRAFTING,
    ACTIVE_DRAFT_DECK,
    IN_REWARDS,
  }

  private class ChoiceCallback
  {
    public FullDef fullDef;
    public int choiceID;
    public int slot;
  }

  private class DraftChoice
  {
    public string m_cardID = string.Empty;
    public TAG_PREMIUM m_premium;
    public Actor m_actor;
  }
}
