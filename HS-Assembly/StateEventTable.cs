﻿// Decompiled with JetBrains decompiler
// Type: StateEventTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class StateEventTable : MonoBehaviour
{
  [CustomEditField(ListTable = true, Sections = "Event Table")]
  public List<StateEventTable.StateEvent> m_Events = new List<StateEventTable.StateEvent>();
  private Map<string, List<StateEventTable.StateEventTrigger>> m_StateEventStartListeners = new Map<string, List<StateEventTable.StateEventTrigger>>();
  private Map<string, List<StateEventTable.StateEventTrigger>> m_StateEventEndListeners = new Map<string, List<StateEventTable.StateEventTrigger>>();
  private Map<string, List<StateEventTable.StateEventTrigger>> m_StateEventStartOnceListeners = new Map<string, List<StateEventTable.StateEventTrigger>>();
  private Map<string, List<StateEventTable.StateEventTrigger>> m_StateEventEndOnceListeners = new Map<string, List<StateEventTable.StateEventTrigger>>();
  private QueueList<StateEventTable.QueueStateEvent> m_QueuedEvents = new QueueList<StateEventTable.QueueStateEvent>();
  private string m_LastState;

  public void TriggerState(string eventName, bool saveLastState = true, string nameOverride = null)
  {
    StateEventTable.StateEvent stateEvent = this.GetStateEvent(eventName);
    if (stateEvent == null)
    {
      Debug.LogError((object) string.Format("{0} not defined in event table.", (object) eventName), (UnityEngine.Object) this.gameObject);
    }
    else
    {
      this.m_QueuedEvents.Enqueue(new StateEventTable.QueueStateEvent()
      {
        m_StateEvent = stateEvent,
        m_NameOverride = nameOverride,
        m_SaveAsLastState = saveLastState
      });
      if (this.m_QueuedEvents.Count != 1)
        return;
      this.StartNextQueuedState((StateEventTable.QueueStateEvent) null);
    }
  }

  public bool HasState(string eventName)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_Events.Find(new Predicate<StateEventTable.StateEvent>(new StateEventTable.\u003CHasState\u003Ec__AnonStorey35F() { eventName = eventName }.\u003C\u003Em__3D)) != null;
  }

  public void CancelQueuedStates()
  {
    this.m_QueuedEvents.Clear();
  }

  public Spell GetSpellEvent(string eventName)
  {
    StateEventTable.StateEvent stateEvent = this.GetStateEvent(eventName);
    if (stateEvent != null)
      return stateEvent.m_Event;
    return (Spell) null;
  }

  public string GetLastState()
  {
    return this.m_LastState;
  }

  public void AddStateEventStartListener(string eventName, StateEventTable.StateEventTrigger dlg, bool once = false)
  {
    this.AddStateEventListener(!once ? this.m_StateEventStartListeners : this.m_StateEventStartOnceListeners, eventName, dlg);
  }

  public void RemoveStateEventStartListener(string eventName, StateEventTable.StateEventTrigger dlg)
  {
    this.RemoveStateEventListener(this.m_StateEventStartListeners, eventName, dlg);
  }

  public void AddStateEventEndListener(string eventName, StateEventTable.StateEventTrigger dlg, bool once = false)
  {
    this.AddStateEventListener(!once ? this.m_StateEventEndListeners : this.m_StateEventEndOnceListeners, eventName, dlg);
  }

  public void RemoveStateEventEndListener(string eventName, StateEventTable.StateEventTrigger dlg)
  {
    this.RemoveStateEventListener(this.m_StateEventEndListeners, eventName, dlg);
  }

  public PlayMakerFSM GetFSMFromEvent(string evtName)
  {
    Spell spellEvent = this.GetSpellEvent(evtName);
    if ((UnityEngine.Object) spellEvent != (UnityEngine.Object) null)
      return spellEvent.GetComponent<PlayMakerFSM>();
    return (PlayMakerFSM) null;
  }

  public void SetFloatVar(string eventName, string varName, float value)
  {
    PlayMakerFSM fsmFromEvent = this.GetFSMFromEvent(eventName);
    if ((UnityEngine.Object) fsmFromEvent == (UnityEngine.Object) null)
      return;
    fsmFromEvent.FsmVariables.GetFsmFloat(varName).Value = value;
  }

  public void SetIntVar(string eventName, string varName, int value)
  {
    PlayMakerFSM fsmFromEvent = this.GetFSMFromEvent(eventName);
    if ((UnityEngine.Object) fsmFromEvent == (UnityEngine.Object) null)
      return;
    fsmFromEvent.FsmVariables.GetFsmInt(varName).Value = value;
  }

  public void SetBoolVar(string eventName, string varName, bool value)
  {
    PlayMakerFSM fsmFromEvent = this.GetFSMFromEvent(eventName);
    if ((UnityEngine.Object) fsmFromEvent == (UnityEngine.Object) null)
      return;
    fsmFromEvent.FsmVariables.GetFsmBool(varName).Value = value;
  }

  public void SetGameObjectVar(string eventName, string varName, GameObject value)
  {
    PlayMakerFSM fsmFromEvent = this.GetFSMFromEvent(eventName);
    if ((UnityEngine.Object) fsmFromEvent == (UnityEngine.Object) null)
      return;
    fsmFromEvent.FsmVariables.GetFsmGameObject(varName).Value = value;
  }

  public void SetGameObjectVar(string eventName, string varName, Component value)
  {
    PlayMakerFSM fsmFromEvent = this.GetFSMFromEvent(eventName);
    if ((UnityEngine.Object) fsmFromEvent == (UnityEngine.Object) null)
      return;
    fsmFromEvent.FsmVariables.GetFsmGameObject(varName).Value = value.gameObject;
  }

  public void SetVector3Var(string eventName, string varName, Vector3 value)
  {
    PlayMakerFSM fsmFromEvent = this.GetFSMFromEvent(eventName);
    if ((UnityEngine.Object) fsmFromEvent == (UnityEngine.Object) null)
      return;
    fsmFromEvent.FsmVariables.GetFsmVector3(varName).Value = value;
  }

  public void SetVar(string eventName, string varName, object value)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    StateEventTable.\u003CSetVar\u003Ec__AnonStorey360 varCAnonStorey360 = new StateEventTable.\u003CSetVar\u003Ec__AnonStorey360();
    // ISSUE: reference to a compiler-generated field
    varCAnonStorey360.eventName = eventName;
    // ISSUE: reference to a compiler-generated field
    varCAnonStorey360.varName = varName;
    // ISSUE: reference to a compiler-generated field
    varCAnonStorey360.value = value;
    // ISSUE: reference to a compiler-generated field
    varCAnonStorey360.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    if (varCAnonStorey360.value is GameObject)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      this.SetGameObjectVar(varCAnonStorey360.eventName, varCAnonStorey360.varName, (GameObject) varCAnonStorey360.value);
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      if (varCAnonStorey360.value is Component)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        this.SetGameObjectVar(varCAnonStorey360.eventName, varCAnonStorey360.varName, (Component) varCAnonStorey360.value);
      }
      else
      {
        Action action;
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated field
        if (new Map<System.Type, Action>() { { typeof (float), new Action(varCAnonStorey360.\u003C\u003Em__3E) }, { typeof (int), new Action(varCAnonStorey360.\u003C\u003Em__3F) }, { typeof (bool), new Action(varCAnonStorey360.\u003C\u003Em__40) } }.TryGetValue(varCAnonStorey360.value.GetType(), out action))
        {
          action();
        }
        else
        {
          // ISSUE: reference to a compiler-generated field
          Debug.LogError((object) string.Format("Set var type ({0}) not supported.", (object) varCAnonStorey360.value.GetType()));
        }
      }
    }
  }

  protected StateEventTable.StateEvent GetStateEvent(string eventName)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_Events.Find(new Predicate<StateEventTable.StateEvent>(new StateEventTable.\u003CGetStateEvent\u003Ec__AnonStorey361() { eventName = eventName }.\u003C\u003Em__41));
  }

  private void StartNextQueuedState(StateEventTable.QueueStateEvent lastEvt)
  {
    if (this.m_QueuedEvents.Count == 0)
    {
      if (lastEvt == null)
        return;
      this.FireStateEventFinishedEvent(this.m_StateEventEndListeners, lastEvt, false);
      this.FireStateEventFinishedEvent(this.m_StateEventEndOnceListeners, lastEvt, true);
    }
    else
    {
      StateEventTable.QueueStateEvent stateEvt = this.m_QueuedEvents.Peek();
      StateEventTable.StateEvent stateEvent = stateEvt.m_StateEvent;
      if (stateEvt.m_SaveAsLastState)
        this.m_LastState = stateEvt.GetEventName();
      stateEvent.m_Event.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.QueueNextState), (object) stateEvt);
      this.FireStateEventFinishedEvent(this.m_StateEventStartListeners, stateEvt, false);
      this.FireStateEventFinishedEvent(this.m_StateEventStartOnceListeners, stateEvt, true);
      stateEvent.m_Event.Activate();
    }
  }

  private void QueueNextState(Spell spell, SpellStateType prevStateType, object thisStateEvent)
  {
    if (this.m_QueuedEvents.Count == 0)
      return;
    this.m_QueuedEvents.Dequeue();
    this.StartNextQueuedState((StateEventTable.QueueStateEvent) thisStateEvent);
  }

  private void AddStateEventListener(Map<string, List<StateEventTable.StateEventTrigger>> listenerDict, string eventName, StateEventTable.StateEventTrigger dlg)
  {
    List<StateEventTable.StateEventTrigger> stateEventTriggerList;
    if (!listenerDict.TryGetValue(eventName, out stateEventTriggerList))
    {
      stateEventTriggerList = new List<StateEventTable.StateEventTrigger>();
      listenerDict[eventName] = stateEventTriggerList;
    }
    stateEventTriggerList.Add(dlg);
  }

  private void RemoveStateEventListener(Map<string, List<StateEventTable.StateEventTrigger>> listenerDict, string eventName, StateEventTable.StateEventTrigger dlg)
  {
    List<StateEventTable.StateEventTrigger> stateEventTriggerList;
    if (!listenerDict.TryGetValue(eventName, out stateEventTriggerList))
      return;
    stateEventTriggerList.Remove(dlg);
  }

  private void FireStateEventFinishedEvent(Map<string, List<StateEventTable.StateEventTrigger>> listenerDict, StateEventTable.QueueStateEvent stateEvt, bool clear = false)
  {
    List<StateEventTable.StateEventTrigger> stateEventTriggerList;
    if (!listenerDict.TryGetValue(stateEvt.GetEventName(), out stateEventTriggerList))
      return;
    foreach (StateEventTable.StateEventTrigger stateEventTrigger in stateEventTriggerList.ToArray())
      stateEventTrigger(stateEvt.m_StateEvent.m_Event);
    if (!clear)
      return;
    stateEventTriggerList.Clear();
  }

  [Serializable]
  public class StateEvent
  {
    public string m_Name;
    public Spell m_Event;
  }

  protected class QueueStateEvent
  {
    public bool m_SaveAsLastState = true;
    public StateEventTable.StateEvent m_StateEvent;
    public string m_NameOverride;

    public string GetEventName()
    {
      if (string.IsNullOrEmpty(this.m_NameOverride))
        return this.m_StateEvent.m_Name;
      return this.m_NameOverride;
    }
  }

  public delegate void StateEventTrigger(Spell evt);
}
