﻿// Decompiled with JetBrains decompiler
// Type: SpecialEventVisualMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class SpecialEventVisualMgr : MonoBehaviour
{
  public List<SpecialEventVisualDef> m_EventDefs = new List<SpecialEventVisualDef>();
  private static SpecialEventVisualMgr s_instance;

  private void Awake()
  {
    SpecialEventVisualMgr.s_instance = this;
  }

  private void OnDestroy()
  {
    SpecialEventVisualMgr.s_instance = (SpecialEventVisualMgr) null;
  }

  public static SpecialEventVisualMgr Get()
  {
    return SpecialEventVisualMgr.s_instance;
  }

  public bool LoadEvent(SpecialEventType eventType)
  {
    for (int index = 0; index < this.m_EventDefs.Count; ++index)
    {
      SpecialEventVisualDef eventDef = this.m_EventDefs[index];
      if (eventDef.m_EventType == eventType)
      {
        AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(eventDef.m_Prefab), (AssetLoader.GameObjectCallback) null, (object) null, false);
        return true;
      }
    }
    return false;
  }

  public bool UnloadEvent(SpecialEventType eventType)
  {
    for (int index = 0; index < this.m_EventDefs.Count; ++index)
    {
      SpecialEventVisualDef eventDef = this.m_EventDefs[index];
      if (eventDef.m_EventType == eventType)
      {
        GameObject gameObject = GameObject.Find(FileUtils.GameAssetPathToName(eventDef.m_Prefab + "(Clone)"));
        if ((Object) gameObject != (Object) null)
          Object.Destroy((Object) gameObject);
      }
    }
    return false;
  }

  private void OnEventFinished(Spell spell, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    Object.Destroy((Object) spell.gameObject);
  }

  public static SpecialEventType GetActiveEventType()
  {
    if (SpecialEventManager.Get().IsEventActive(SpecialEventType.GVG_PROMOTION, false))
      return SpecialEventType.GVG_PROMOTION;
    return SpecialEventManager.Get().IsEventActive(SpecialEventType.SPECIAL_EVENT_PRE_TAVERN_BRAWL, false) ? SpecialEventType.SPECIAL_EVENT_PRE_TAVERN_BRAWL : SpecialEventType.IGNORE;
  }
}
