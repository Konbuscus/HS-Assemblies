﻿// Decompiled with JetBrains decompiler
// Type: Shadowform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class Shadowform : SuperSpell
{
  public int m_MaterialIndex = 1;
  public float m_FadeInTime = 1f;
  public float m_Desaturate = 0.8f;
  public Color m_Tint = new Color(177f / 256f, 21f / 64f, 103f / 128f, 1f);
  public float m_Contrast = -0.29f;
  public float m_Intensity = 0.85f;
  public float m_FxIntensity = 4f;
  public Material m_ShadowformMaterial;
  private Material m_MaterialInstance;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Shadowform.\u003COnBirth\u003Ec__AnonStorey409 birthCAnonStorey409 = new Shadowform.\u003COnBirth\u003Ec__AnonStorey409();
    base.OnBirth(prevStateType);
    if ((UnityEngine.Object) this.m_ShadowformMaterial == (UnityEngine.Object) null)
      return;
    Actor componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<Actor>((Component) this);
    componentInThisOrParents.SetShadowform(true);
    this.m_MaterialInstance = new Material(this.m_ShadowformMaterial);
    this.m_MaterialInstance.mainTexture = componentInThisOrParents.GetPortraitTexture();
    componentInThisOrParents.SetPortraitMaterial(this.m_MaterialInstance);
    this.OnSpellFinished();
    GameObject portraitMesh = componentInThisOrParents.GetPortraitMesh();
    // ISSUE: reference to a compiler-generated field
    birthCAnonStorey409.mat = portraitMesh.GetComponent<Renderer>().materials[componentInThisOrParents.m_portraitMatIdx];
    // ISSUE: reference to a compiler-generated method
    Hashtable args1 = iTween.Hash((object) "time", (object) this.m_FadeInTime, (object) "from", (object) 0.0f, (object) "to", (object) this.m_Desaturate, (object) "onupdate", (object) new Action<object>(birthCAnonStorey409.\u003C\u003Em__22E), (object) "onupdatetarget", (object) componentInThisOrParents.gameObject);
    iTween.ValueTo(componentInThisOrParents.gameObject, args1);
    // ISSUE: reference to a compiler-generated method
    Hashtable args2 = iTween.Hash((object) "time", (object) this.m_FadeInTime, (object) "from", (object) Color.white, (object) "to", (object) this.m_Tint, (object) "onupdate", (object) new Action<object>(birthCAnonStorey409.\u003C\u003Em__22F), (object) "onupdatetarget", (object) componentInThisOrParents.gameObject);
    iTween.ValueTo(componentInThisOrParents.gameObject, args2);
    // ISSUE: reference to a compiler-generated method
    Hashtable args3 = iTween.Hash((object) "time", (object) this.m_FadeInTime, (object) "from", (object) 0.0f, (object) "to", (object) this.m_Contrast, (object) "onupdate", (object) new Action<object>(birthCAnonStorey409.\u003C\u003Em__230), (object) "onupdatetarget", (object) componentInThisOrParents.gameObject);
    iTween.ValueTo(componentInThisOrParents.gameObject, args3);
    // ISSUE: reference to a compiler-generated method
    Hashtable args4 = iTween.Hash((object) "time", (object) this.m_FadeInTime, (object) "from", (object) 1f, (object) "to", (object) this.m_Intensity, (object) "onupdate", (object) new Action<object>(birthCAnonStorey409.\u003C\u003Em__231), (object) "onupdatetarget", (object) componentInThisOrParents.gameObject);
    iTween.ValueTo(componentInThisOrParents.gameObject, args4);
    // ISSUE: reference to a compiler-generated method
    Hashtable args5 = iTween.Hash((object) "time", (object) this.m_FadeInTime, (object) "from", (object) 0.0f, (object) "to", (object) this.m_FxIntensity, (object) "onupdate", (object) new Action<object>(birthCAnonStorey409.\u003C\u003Em__232), (object) "onupdatetarget", (object) componentInThisOrParents.gameObject);
    iTween.ValueTo(componentInThisOrParents.gameObject, args5);
  }

  protected override void OnDeath(SpellStateType prevStateType)
  {
    base.OnDeath(prevStateType);
    Actor componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<Actor>((Component) this);
    componentInThisOrParents.SetShadowform(false);
    componentInThisOrParents.UpdateAllComponents();
  }
}
