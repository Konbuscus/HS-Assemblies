﻿// Decompiled with JetBrains decompiler
// Type: GameDbfIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameDbfIndex
{
  private Map<string, CardDbfRecord> m_cardsByCardId;
  private Map<int, List<CardTagDbfRecord>> m_cardTagsByCardDbId;
  private List<string> m_allCardIds;
  private List<int> m_allCardDbIds;
  private List<string> m_collectibleCardIds;
  private List<int> m_collectibleCardDbIds;
  private int m_collectibleCardCount;
  private Map<int, List<FixedRewardMapDbfRecord>> m_fixedRewardsByAction;
  private Map<FixedActionType, List<FixedRewardActionDbfRecord>> m_fixedActionRecordsByType;
  private Map<int, List<int>> m_subsetsReferencedByRuleId;
  private Map<int, HashSet<string>> m_subsetCards;
  private Map<int, HashSet<int>> m_rulesByDeckRulesetId;

  public GameDbfIndex()
  {
    this.Initialize();
  }

  public void Initialize()
  {
    this.m_cardsByCardId = new Map<string, CardDbfRecord>();
    this.m_cardTagsByCardDbId = new Map<int, List<CardTagDbfRecord>>();
    this.m_allCardIds = new List<string>();
    this.m_allCardDbIds = new List<int>();
    this.m_collectibleCardIds = new List<string>();
    this.m_collectibleCardDbIds = new List<int>();
    this.m_collectibleCardCount = 0;
    this.m_fixedRewardsByAction = new Map<int, List<FixedRewardMapDbfRecord>>();
    this.m_fixedActionRecordsByType = new Map<FixedActionType, List<FixedRewardActionDbfRecord>>();
    this.m_subsetsReferencedByRuleId = new Map<int, List<int>>();
    this.m_subsetCards = new Map<int, HashSet<string>>();
    this.m_rulesByDeckRulesetId = new Map<int, HashSet<int>>();
  }

  public void OnCardTagAdded(CardTagDbfRecord cardTagRecord)
  {
    int cardId = cardTagRecord.CardId;
    List<CardTagDbfRecord> cardTagDbfRecordList = (List<CardTagDbfRecord>) null;
    if (!this.m_cardTagsByCardDbId.TryGetValue(cardId, out cardTagDbfRecordList))
    {
      cardTagDbfRecordList = new List<CardTagDbfRecord>();
      this.m_cardTagsByCardDbId[cardId] = cardTagDbfRecordList;
    }
    cardTagDbfRecordList.Add(cardTagRecord);
  }

  public void OnCardTagRemoved(List<CardTagDbfRecord> removedRecords)
  {
    using (List<CardTagDbfRecord>.Enumerator enumerator1 = removedRecords.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        CardTagDbfRecord current = enumerator1.Current;
        using (Map<int, List<CardTagDbfRecord>>.ValueCollection.Enumerator enumerator2 = this.m_cardTagsByCardDbId.Values.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            if (enumerator2.Current.Remove(current))
              break;
          }
        }
      }
    }
  }

  public int GetCardTagValue(int cardDbId, GAME_TAG tagId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameDbfIndex.\u003CGetCardTagValue\u003Ec__AnonStorey441 valueCAnonStorey441 = new GameDbfIndex.\u003CGetCardTagValue\u003Ec__AnonStorey441();
    // ISSUE: reference to a compiler-generated field
    valueCAnonStorey441.tagId = tagId;
    List<CardTagDbfRecord> cardTagDbfRecordList = (List<CardTagDbfRecord>) null;
    if (!this.m_cardTagsByCardDbId.TryGetValue(cardDbId, out cardTagDbfRecordList))
      return 0;
    // ISSUE: reference to a compiler-generated method
    CardTagDbfRecord cardTagDbfRecord = cardTagDbfRecordList.Find(new Predicate<CardTagDbfRecord>(valueCAnonStorey441.\u003C\u003Em__2EC));
    if (cardTagDbfRecord == null)
      return 0;
    return cardTagDbfRecord.TagValue;
  }

  public IEnumerable<CardTagDbfRecord> GetCardTagRecords(int cardDbId)
  {
    List<CardTagDbfRecord> cardTagDbfRecordList = (List<CardTagDbfRecord>) null;
    if (this.m_cardTagsByCardDbId.TryGetValue(cardDbId, out cardTagDbfRecordList))
      return (IEnumerable<CardTagDbfRecord>) cardTagDbfRecordList;
    return (IEnumerable<CardTagDbfRecord>) null;
  }

  public void OnCardAdded(CardDbfRecord cardRecord)
  {
    int id = cardRecord.ID;
    bool flag = this.GetCardTagValue(id, GAME_TAG.COLLECTIBLE) == 1;
    string noteMiniGuid = cardRecord.NoteMiniGuid;
    this.m_cardsByCardId[noteMiniGuid] = cardRecord;
    this.m_allCardDbIds.Add(id);
    this.m_allCardIds.Add(noteMiniGuid);
    if (!flag)
      return;
    ++this.m_collectibleCardCount;
    this.m_collectibleCardIds.Add(noteMiniGuid);
    this.m_collectibleCardDbIds.Add(id);
  }

  public void OnCardRemoved(List<CardDbfRecord> removedRecords)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameDbfIndex.\u003COnCardRemoved\u003Ec__AnonStorey442 removedCAnonStorey442 = new GameDbfIndex.\u003COnCardRemoved\u003Ec__AnonStorey442();
    // ISSUE: reference to a compiler-generated field
    removedCAnonStorey442.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    removedCAnonStorey442.removedCardDbIds = new HashSet<int>();
    // ISSUE: reference to a compiler-generated field
    removedCAnonStorey442.removedCardIds = new HashSet<string>();
    using (List<CardDbfRecord>.Enumerator enumerator = removedRecords.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardDbfRecord current = enumerator.Current;
        // ISSUE: reference to a compiler-generated field
        removedCAnonStorey442.removedCardDbIds.Add(current.ID);
        if (current.NoteMiniGuid != null)
        {
          // ISSUE: reference to a compiler-generated field
          removedCAnonStorey442.removedCardIds.Add(current.NoteMiniGuid);
          this.m_cardsByCardId.Remove(current.NoteMiniGuid);
        }
      }
    }
    // ISSUE: reference to a compiler-generated field
    if (removedCAnonStorey442.removedCardDbIds.Count > 0)
    {
      // ISSUE: reference to a compiler-generated method
      this.m_allCardDbIds.RemoveAll(new Predicate<int>(removedCAnonStorey442.\u003C\u003Em__2ED));
      // ISSUE: reference to a compiler-generated method
      this.m_collectibleCardDbIds.RemoveAll(new Predicate<int>(removedCAnonStorey442.\u003C\u003Em__2EE));
    }
    // ISSUE: reference to a compiler-generated field
    if (removedCAnonStorey442.removedCardIds.Count <= 0)
      return;
    // ISSUE: reference to a compiler-generated method
    this.m_allCardIds.RemoveAll(new Predicate<string>(removedCAnonStorey442.\u003C\u003Em__2EF));
    // ISSUE: reference to a compiler-generated method
    this.m_collectibleCardIds.RemoveAll(new Predicate<string>(removedCAnonStorey442.\u003C\u003Em__2F0));
  }

  public void OnFixedRewardMapAdded(FixedRewardMapDbfRecord record)
  {
    int actionId = record.ActionId;
    List<FixedRewardMapDbfRecord> rewardMapDbfRecordList;
    if (!this.m_fixedRewardsByAction.TryGetValue(actionId, out rewardMapDbfRecordList))
    {
      rewardMapDbfRecordList = new List<FixedRewardMapDbfRecord>();
      this.m_fixedRewardsByAction.Add(actionId, rewardMapDbfRecordList);
    }
    rewardMapDbfRecordList.Add(record);
  }

  public void OnFixedRewardMapRemoved(List<FixedRewardMapDbfRecord> removedRecords)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameDbfIndex.\u003COnFixedRewardMapRemoved\u003Ec__AnonStorey443 removedCAnonStorey443 = new GameDbfIndex.\u003COnFixedRewardMapRemoved\u003Ec__AnonStorey443();
    // ISSUE: reference to a compiler-generated field
    removedCAnonStorey443.removedIds = new HashSet<int>(removedRecords.Select<FixedRewardMapDbfRecord, int>((Func<FixedRewardMapDbfRecord, int>) (r => r.ID)));
    using (HashSet<int>.Enumerator enumerator = new HashSet<int>(removedRecords.Select<FixedRewardMapDbfRecord, int>((Func<FixedRewardMapDbfRecord, int>) (r => r.ActionId))).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        List<FixedRewardMapDbfRecord> rewardMapDbfRecordList;
        if (this.m_fixedRewardsByAction.TryGetValue(enumerator.Current, out rewardMapDbfRecordList))
        {
          // ISSUE: reference to a compiler-generated method
          rewardMapDbfRecordList.RemoveAll(new Predicate<FixedRewardMapDbfRecord>(removedCAnonStorey443.\u003C\u003Em__2F3));
        }
      }
    }
  }

  public void OnFixedRewardActionAdded(FixedRewardActionDbfRecord record)
  {
    string type = record.Type;
    FixedActionType key;
    try
    {
      key = EnumUtils.GetEnum<FixedActionType>(type);
    }
    catch
    {
      Debug.LogErrorFormat("Error parsing FixedRewardAction.Type, type did not match a FixedRewardType: {0}", (object) type);
      return;
    }
    List<FixedRewardActionDbfRecord> rewardActionDbfRecordList;
    if (!this.m_fixedActionRecordsByType.TryGetValue(key, out rewardActionDbfRecordList))
    {
      rewardActionDbfRecordList = new List<FixedRewardActionDbfRecord>();
      this.m_fixedActionRecordsByType.Add(key, rewardActionDbfRecordList);
    }
    rewardActionDbfRecordList.Add(record);
  }

  public void OnFixedRewardActionRemoved(List<FixedRewardActionDbfRecord> removedRecords)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameDbfIndex.\u003COnFixedRewardActionRemoved\u003Ec__AnonStorey444 removedCAnonStorey444 = new GameDbfIndex.\u003COnFixedRewardActionRemoved\u003Ec__AnonStorey444();
    // ISSUE: reference to a compiler-generated field
    removedCAnonStorey444.removedIds = new HashSet<int>(removedRecords.Select<FixedRewardActionDbfRecord, int>((Func<FixedRewardActionDbfRecord, int>) (r => r.ID)));
    HashSet<FixedActionType> fixedActionTypeSet;
    try
    {
      fixedActionTypeSet = new HashSet<FixedActionType>(removedRecords.Select<FixedRewardActionDbfRecord, FixedActionType>((Func<FixedRewardActionDbfRecord, FixedActionType>) (r => EnumUtils.GetEnum<FixedActionType>(r.Type))));
    }
    catch
    {
      Debug.LogErrorFormat("Error parsing FixedRewardAction.Type, type did not match a FixedRewardType: {0}", (object) string.Join(", ", removedRecords.Select<FixedRewardActionDbfRecord, string>((Func<FixedRewardActionDbfRecord, string>) (r => r.Type)).ToArray<string>()));
      fixedActionTypeSet = new HashSet<FixedActionType>();
    }
    using (HashSet<FixedActionType>.Enumerator enumerator = fixedActionTypeSet.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        List<FixedRewardActionDbfRecord> rewardActionDbfRecordList;
        if (this.m_fixedActionRecordsByType.TryGetValue(enumerator.Current, out rewardActionDbfRecordList))
        {
          // ISSUE: reference to a compiler-generated method
          rewardActionDbfRecordList.RemoveAll(new Predicate<FixedRewardActionDbfRecord>(removedCAnonStorey444.\u003C\u003Em__2F7));
        }
      }
    }
  }

  public void OnDeckRulesetRuleSubsetAdded(DeckRulesetRuleSubsetDbfRecord record)
  {
    int deckRulesetRuleId = record.DeckRulesetRuleId;
    int subsetId = record.SubsetId;
    List<int> intList;
    if (!this.m_subsetsReferencedByRuleId.TryGetValue(deckRulesetRuleId, out intList))
    {
      intList = new List<int>();
      this.m_subsetsReferencedByRuleId[deckRulesetRuleId] = intList;
    }
    intList.Add(subsetId);
  }

  public void OnDeckRulesetRuleSubsetRemoved(List<DeckRulesetRuleSubsetDbfRecord> removedRecords)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameDbfIndex.\u003COnDeckRulesetRuleSubsetRemoved\u003Ec__AnonStorey445 removedCAnonStorey445 = new GameDbfIndex.\u003COnDeckRulesetRuleSubsetRemoved\u003Ec__AnonStorey445();
    using (List<DeckRulesetRuleSubsetDbfRecord>.Enumerator enumerator = removedRecords.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        removedCAnonStorey445.rec = enumerator.Current;
        List<int> intList;
        // ISSUE: reference to a compiler-generated field
        if (this.m_subsetsReferencedByRuleId.TryGetValue(removedCAnonStorey445.rec.DeckRulesetRuleId, out intList))
        {
          // ISSUE: reference to a compiler-generated method
          intList.RemoveAll(new Predicate<int>(removedCAnonStorey445.\u003C\u003Em__2F8));
        }
      }
    }
  }

  public void OnSubsetCardAdded(SubsetCardDbfRecord record)
  {
    int subsetId = record.SubsetId;
    int cardId = record.CardId;
    CardDbfRecord record1 = GameDbf.Card.GetRecord(cardId);
    if (record1 == null)
      return;
    HashSet<string> stringSet;
    if (!this.m_subsetCards.TryGetValue(subsetId, out stringSet))
    {
      stringSet = new HashSet<string>();
      this.m_subsetCards[subsetId] = stringSet;
    }
    stringSet.Add(record1.NoteMiniGuid);
  }

  public void OnSubsetCardRemoved(List<SubsetCardDbfRecord> removedRecords)
  {
    using (List<SubsetCardDbfRecord>.Enumerator enumerator = removedRecords.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SubsetCardDbfRecord current = enumerator.Current;
        HashSet<string> stringSet;
        if (this.m_subsetCards.TryGetValue(current.SubsetId, out stringSet) && stringSet != null)
        {
          CardDbfRecord record = GameDbf.Card.GetRecord(current.CardId);
          if (record != null && record.NoteMiniGuid != null)
            stringSet.Remove(record.NoteMiniGuid);
        }
      }
    }
  }

  public void OnDeckRulesetRuleAdded(DeckRulesetRuleDbfRecord record)
  {
    HashSet<int> intSet;
    if (!this.m_rulesByDeckRulesetId.TryGetValue(record.DeckRulesetId, out intSet))
    {
      intSet = new HashSet<int>();
      this.m_rulesByDeckRulesetId[record.DeckRulesetId] = intSet;
    }
    intSet.Add(record.ID);
  }

  public void OnDeckRulesetRuleRemoved(List<DeckRulesetRuleDbfRecord> removedRecords)
  {
    using (List<DeckRulesetRuleDbfRecord>.Enumerator enumerator = removedRecords.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckRulesetRuleDbfRecord current = enumerator.Current;
        HashSet<int> intSet;
        if (this.m_rulesByDeckRulesetId.TryGetValue(current.DeckRulesetId, out intSet))
          intSet.Remove(current.ID);
      }
    }
  }

  public CardDbfRecord GetCardRecord(string cardId)
  {
    if (string.IsNullOrEmpty(cardId))
      return (CardDbfRecord) null;
    if (cardId == "PlaceholderCard")
      this.CachePlaceholderRecord();
    CardDbfRecord cardDbfRecord = (CardDbfRecord) null;
    this.m_cardsByCardId.TryGetValue(cardId, out cardDbfRecord);
    return cardDbfRecord;
  }

  private void CachePlaceholderRecord()
  {
    if (this.m_cardsByCardId.ContainsKey("PlaceholderCard"))
      return;
    CardDbfRecord cardDbfRecord = new CardDbfRecord();
    cardDbfRecord.SetID(-1);
    cardDbfRecord.SetNoteMiniGuid("PlaceholderCard");
    DbfLocValue v1 = new DbfLocValue();
    v1.SetString(Locale.enUS, "Placeholder Card");
    cardDbfRecord.SetName(v1);
    DbfLocValue v2 = new DbfLocValue();
    v2.SetString(Locale.enUS, "Battlecry: Someone remembers to publish this card.");
    cardDbfRecord.SetTextInHand(v2);
    Dictionary<GAME_TAG, int> dictionary = new Dictionary<GAME_TAG, int>() { { GAME_TAG.CARD_SET, 7 }, { GAME_TAG.CARDTYPE, 4 }, { GAME_TAG.CLASS, 4 }, { GAME_TAG.RARITY, 4 }, { GAME_TAG.FACTION, 3 }, { GAME_TAG.COST, 9 }, { GAME_TAG.HEALTH, 8 }, { GAME_TAG.ATK, 6 } };
    List<CardTagDbfRecord> cardTagDbfRecordList = new List<CardTagDbfRecord>();
    using (Dictionary<GAME_TAG, int>.Enumerator enumerator = dictionary.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<GAME_TAG, int> current = enumerator.Current;
        CardTagDbfRecord cardTagDbfRecord = new CardTagDbfRecord();
        cardTagDbfRecord.SetCardId(cardDbfRecord.ID);
        cardTagDbfRecord.SetTagId((int) current.Key);
        cardTagDbfRecord.SetTagValue(current.Value);
        cardTagDbfRecordList.Add(cardTagDbfRecord);
      }
    }
    this.m_cardsByCardId.Add("PlaceholderCard", cardDbfRecord);
    this.m_cardTagsByCardDbId.Add(cardDbfRecord.ID, cardTagDbfRecordList);
  }

  public int GetCollectibleCardCount()
  {
    return this.m_collectibleCardCount;
  }

  public List<string> GetAllCardIds()
  {
    return this.m_allCardIds;
  }

  public List<int> GetAllCardDbIds()
  {
    return this.m_allCardDbIds;
  }

  public List<string> GetCollectibleCardIds()
  {
    return this.m_collectibleCardIds;
  }

  public List<int> GetCollectibleCardDbIds()
  {
    return this.m_collectibleCardDbIds;
  }

  public List<FixedRewardMapDbfRecord> GetFixedRewardMapRecordsForAction(int actionId)
  {
    List<FixedRewardMapDbfRecord> rewardMapDbfRecordList = (List<FixedRewardMapDbfRecord>) null;
    if (!this.m_fixedRewardsByAction.TryGetValue(actionId, out rewardMapDbfRecordList))
      rewardMapDbfRecordList = new List<FixedRewardMapDbfRecord>();
    return rewardMapDbfRecordList;
  }

  public List<FixedRewardActionDbfRecord> GetFixedActionRecordsForType(FixedActionType type)
  {
    List<FixedRewardActionDbfRecord> rewardActionDbfRecordList = (List<FixedRewardActionDbfRecord>) null;
    if (!this.m_fixedActionRecordsByType.TryGetValue(type, out rewardActionDbfRecordList))
      rewardActionDbfRecordList = new List<FixedRewardActionDbfRecord>();
    return rewardActionDbfRecordList;
  }

  public List<HashSet<string>> GetSubsetsForRule(int ruleId)
  {
    List<HashSet<string>> stringSetList = new List<HashSet<string>>();
    List<int> intList;
    if (this.m_subsetsReferencedByRuleId.TryGetValue(ruleId, out intList))
    {
      for (int index = 0; index < intList.Count; ++index)
        stringSetList.Add(this.GetSubsetById(intList[index]));
    }
    return stringSetList;
  }

  public DeckRulesetRuleDbfRecord[] GetRulesForDeckRuleset(int deckRulesetId)
  {
    HashSet<int> source;
    if (!this.m_rulesByDeckRulesetId.TryGetValue(deckRulesetId, out source))
      source = new HashSet<int>();
    // ISSUE: object of a compiler-generated type is created
    return source.Select<int, \u003C\u003E__AnonType1<int, DeckRulesetRuleDbfRecord>>((Func<int, \u003C\u003E__AnonType1<int, DeckRulesetRuleDbfRecord>>) (ruleId => new \u003C\u003E__AnonType1<int, DeckRulesetRuleDbfRecord>(ruleId, GameDbf.DeckRulesetRule.GetRecord(ruleId)))).Where<\u003C\u003E__AnonType1<int, DeckRulesetRuleDbfRecord>>((Func<\u003C\u003E__AnonType1<int, DeckRulesetRuleDbfRecord>, bool>) (param0 => param0.ruleDbf != null)).Select<\u003C\u003E__AnonType1<int, DeckRulesetRuleDbfRecord>, DeckRulesetRuleDbfRecord>((Func<\u003C\u003E__AnonType1<int, DeckRulesetRuleDbfRecord>, DeckRulesetRuleDbfRecord>) (param0 => param0.ruleDbf)).ToArray<DeckRulesetRuleDbfRecord>();
  }

  public HashSet<string> GetSubsetById(int id)
  {
    HashSet<string> stringSet = (HashSet<string>) null;
    if (!this.m_subsetCards.TryGetValue(id, out stringSet))
      stringSet = new HashSet<string>();
    return stringSet;
  }
}
