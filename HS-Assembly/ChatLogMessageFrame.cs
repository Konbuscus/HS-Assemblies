﻿// Decompiled with JetBrains decompiler
// Type: ChatLogMessageFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ChatLogMessageFrame : MonoBehaviour
{
  public GameObject m_Background;
  public UberText m_Text;
  private float m_initialPadding;
  private float m_initialBackgroundHeight;
  private float m_initialBackgroundLocalScaleY;

  private void Awake()
  {
    Bounds bounds = this.m_Background.GetComponent<Collider>().bounds;
    Bounds worldSpaceBounds = this.m_Text.GetTextWorldSpaceBounds();
    this.m_initialPadding = bounds.size.y - worldSpaceBounds.size.y;
    this.m_initialBackgroundHeight = bounds.size.y;
    this.m_initialBackgroundLocalScaleY = this.m_Background.transform.localScale.y;
  }

  public string GetMessage()
  {
    return this.m_Text.Text;
  }

  public void SetMessage(string message)
  {
    this.m_Text.Text = message;
    this.UpdateText();
    this.UpdateBackground();
  }

  public Color GetColor()
  {
    return this.m_Text.TextColor;
  }

  public void SetColor(Color color)
  {
    this.m_Text.TextColor = color;
  }

  private void UpdateText()
  {
    this.m_Text.UpdateNow();
  }

  private void UpdateBackground()
  {
    float num = this.m_Text.GetTextWorldSpaceBounds().size.y + this.m_initialPadding;
    float backgroundLocalScaleY = this.m_initialBackgroundLocalScaleY;
    if ((double) num > (double) this.m_initialBackgroundHeight)
      backgroundLocalScaleY *= num / this.m_initialBackgroundHeight;
    TransformUtil.SetLocalScaleY(this.m_Background, backgroundLocalScaleY);
  }
}
