﻿// Decompiled with JetBrains decompiler
// Type: StoreManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using UnityEngine;
using WTCG.BI;

public class StoreManager
{
  public static readonly int DEFAULT_SECONDS_BEFORE_AUTO_CANCEL = 600;
  public static readonly int DEFAULT_SECONDS_BEFORE_AUTO_CANCEL_THIRD_PARTY = 60;
  public static readonly PlatformDependentValue<GeneralStoreMode> s_defaultStoreMode = new PlatformDependentValue<GeneralStoreMode>(PlatformCategory.Screen) { PC = GeneralStoreMode.CARDS, Phone = GeneralStoreMode.NONE };
  private static readonly long MAX_REQUEST_DELAY_TICKS = 6000000000;
  private static readonly long MIN_CONFIG_REQUEST_DELAY_TICKS = 600000000;
  private static readonly long MIN_STATUS_REQUEST_DELAY_TICKS = 450000000;
  private static readonly long EARLY_STATUS_REQUEST_DELAY_TICKS = 50000000;
  private static readonly long CHALLENGE_CANCEL_STATUS_REQUEST_DELAY_TICKS = 10000000;
  private static readonly int UNKNOWN_TRANSACTION_ID = -1;
  private static readonly Map<Currency, Locale> s_currencyToLocaleMap = new Map<Currency, Locale>() { { Currency.USD, Locale.enUS }, { Currency.GBP, Locale.enGB }, { Currency.KRW, Locale.koKR }, { Currency.EUR, Locale.frFR }, { Currency.RUB, Locale.ruRU }, { Currency.ARS, Locale.esMX }, { Currency.CLP, Locale.esMX }, { Currency.MXN, Locale.esMX }, { Currency.BRL, Locale.ptBR }, { Currency.AUD, Locale.enUS }, { Currency.CPT, Locale.zhCN }, { Currency.TPT, Locale.zhTW } };
  private static readonly string DEFAULT_CURRENCY_FORMAT = "{0:C2}";
  private static readonly Map<Currency, string> s_currencySpecialFormats = new Map<Currency, string>() { { Currency.KRW, "{0:C0}" }, { Currency.TPT, "{0:C0}" } };
  private static readonly Map<AdventureDbId, ProductType> s_adventureToProductMap = new Map<AdventureDbId, ProductType>() { { AdventureDbId.NAXXRAMAS, ProductType.PRODUCT_TYPE_NAXX }, { AdventureDbId.BRM, ProductType.PRODUCT_TYPE_BRM }, { AdventureDbId.LOE, ProductType.PRODUCT_TYPE_LOE } };
  private static StoreManager s_instance = (StoreManager) null;
  public static readonly PlatformDependentValue<bool> HAS_THIRD_PARTY_APP_STORE = new PlatformDependentValue<bool>(PlatformCategory.OS) { PC = false, Mac = false, iOS = true, Android = true };
  private readonly PlatformDependentValue<string> s_storePrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "StoreMain", Phone = "StoreMain_phone" };
  private readonly PlatformDependentValue<string> s_storePurchaseAuthPrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "StorePurchaseAuth", Phone = "StorePurchaseAuth_phone" };
  private readonly PlatformDependentValue<string> s_storeSummaryPrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "StoreSummary", Phone = "StoreSummary_phone" };
  private readonly PlatformDependentValue<string> s_storeSendToBAMPrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "StoreSendToBAM", Phone = "StoreSendToBAM_phone" };
  private readonly PlatformDependentValue<string> s_storeDoneWithBAMPrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "StoreDoneWithBAM", Phone = "StoreDoneWithBAM_phone" };
  private readonly PlatformDependentValue<string> s_storeChallengePromptPrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "StoreChallengePrompt", Phone = "StoreChallengePrompt_phone" };
  private readonly PlatformDependentValue<string> s_storeLegalBAMLinksPrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "StoreLegalBAMLinks", Phone = "StoreLegalBAMLinks_phone" };
  private readonly PlatformDependentValue<string> s_arenaStorePrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "ArenaStore", Phone = "ArenaStore_phone" };
  private readonly PlatformDependentValue<string> s_heroicBrawlStorePrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "TavernBrawlStore", Phone = "TavernBrawlStore_phone" };
  private readonly PlatformDependentValue<string> s_adventureStorePrefab = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "AdventureStore", Phone = "AdventureStore_phone" };
  private long m_ticksBeforeAutoCancel = (long) StoreManager.DEFAULT_SECONDS_BEFORE_AUTO_CANCEL * 10000000L;
  private long m_ticksBeforeAutoCancelThirdParty = (long) StoreManager.DEFAULT_SECONDS_BEFORE_AUTO_CANCEL_THIRD_PARTY * 10000000L;
  private long m_configRequestDelayTicks = StoreManager.MIN_CONFIG_REQUEST_DELAY_TICKS;
  private Map<string, Network.Bundle> m_bundles = new Map<string, Network.Bundle>();
  private Map<int, Network.GoldCostBooster> m_goldCostBooster = new Map<int, Network.GoldCostBooster>();
  private HashSet<long> m_transactionIDsConclusivelyHandled = new HashSet<long>();
  private Map<StoreType, Store> m_stores = new Map<StoreType, Store>();
  private List<StoreManager.StatusChangedListener> m_statusChangedListeners = new List<StoreManager.StatusChangedListener>();
  private List<StoreManager.SuccessfulPurchaseAckListener> m_successfulPurchaseAckListeners = new List<StoreManager.SuccessfulPurchaseAckListener>();
  private List<StoreManager.SuccessfulPurchaseListener> m_successfulPurchaseListeners = new List<StoreManager.SuccessfulPurchaseListener>();
  private List<StoreManager.AuthorizationExitListener> m_authExitListeners = new List<StoreManager.AuthorizationExitListener>();
  private List<StoreManager.StoreShownListener> m_storeShownListeners = new List<StoreManager.StoreShownListener>();
  private List<StoreManager.StoreHiddenListener> m_storeHiddenListeners = new List<StoreManager.StoreHiddenListener>();
  private List<StoreManager.StoreAchievesListener> m_storeAchievesListeners = new List<StoreManager.StoreAchievesListener>();
  private long m_statusRequestDelayTicks = StoreManager.MIN_STATUS_REQUEST_DELAY_TICKS;
  private HashSet<long> m_confirmedTransactionIDs = new HashSet<long>();
  private List<NetCache.ProfileNoticePurchase> m_outstandingPurchaseNotices = new List<NetCache.ProfileNoticePurchase>();
  private List<Achievement> m_completedAchieves = new List<Achievement>();
  public const int NO_ITEM_COUNT_REQUIREMENT = 0;
  public const int NO_PRODUCT_DATA_REQUIREMENT = 0;
  private bool m_featuresReady;
  private bool m_initComplete;
  private bool m_battlePayAvailable;
  private bool m_firstNoticesProcessed;
  private bool m_firstMoneyOrGTAPPTransactionSet;
  private long m_lastCancelRequestTime;
  private long m_lastConfigRequestTime;
  private bool m_configLoaded;
  private long? m_goldCostArena;
  private Currency m_currency;
  private StoreType m_currentStoreType;
  private float m_showStoreStart;
  private Network.PurchaseMethod m_challengePurchaseMethod;
  private bool m_openWhenLastEventFired;
  private StoreManager.TransactionStatus m_status;
  private long m_lastStatusRequestTime;
  private StorePurchaseAuth m_storePurchaseAuth;
  private StoreSummary m_storeSummary;
  private StoreSendToBAM m_storeSendToBAM;
  private StoreLegalBAMLinks m_storeLegalBAMLinks;
  private StoreDoneWithBAM m_storeDoneWithBAM;
  private StoreChallengePrompt m_storeChallengePrompt;
  private bool m_waitingToShowStore;
  private StoreManager.ShowStoreData m_showStoreData;
  private MoneyOrGTAPPTransaction m_activeMoneyOrGTAPPTransaction;
  private bool m_licenseAchievesListenerRegistered;
  private string m_requestedThirdPartyProductId;
  private bool m_shouldAutoCancelThirdPartyTransaction;
  private StoreManager.TransactionStatus m_previousStatusBeforeAutoCancel;
  private bool m_outOfSessionThirdPartyTransaction;
  private Coroutine m_handlePurchaseNoticesCoroutine;

  public static BattlePayProvider StoreProvider
  {
    get
    {
      return BattlePayProvider.BP_PROVIDER_BLIZZARD;
    }
  }

  private StoreManager.TransactionStatus Status
  {
    get
    {
      return this.m_status;
    }
    set
    {
      if (this.m_lastCancelRequestTime == 0L && this.m_status == StoreManager.TransactionStatus.UNKNOWN)
        this.m_lastCancelRequestTime = DateTime.Now.Ticks;
      this.m_status = value;
      this.FireStatusChangedEventIfNeeded();
    }
  }

  private bool FirstNoticesProcessed
  {
    get
    {
      return this.m_firstNoticesProcessed;
    }
    set
    {
      this.m_firstNoticesProcessed = value;
      this.FireStatusChangedEventIfNeeded();
    }
  }

  private bool BattlePayAvailable
  {
    get
    {
      return this.m_battlePayAvailable;
    }
    set
    {
      this.m_battlePayAvailable = value;
      this.FireStatusChangedEventIfNeeded();
    }
  }

  private bool FeaturesReady
  {
    get
    {
      return this.m_featuresReady;
    }
    set
    {
      this.m_featuresReady = value;
      this.FireStatusChangedEventIfNeeded();
    }
  }

  private bool ConfigLoaded
  {
    get
    {
      return this.m_configLoaded;
    }
    set
    {
      this.m_configLoaded = value;
      this.FireStatusChangedEventIfNeeded();
    }
  }

  public static StoreManager Get()
  {
    if (StoreManager.s_instance == null)
      StoreManager.s_instance = new StoreManager();
    return StoreManager.s_instance;
  }

  public static void DestroyInstance()
  {
    Store store = StoreManager.s_instance.GetStore(StoreType.GENERAL_STORE);
    if ((UnityEngine.Object) store != (UnityEngine.Object) null)
    {
      store.Hide();
      UnityEngine.Object.DestroyObject((UnityEngine.Object) store);
    }
    if (AchieveManager.Get() != null && StoreManager.s_instance != null)
    {
      AchieveManager.Get().RemoveAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(StoreManager.s_instance.OnAchievesUpdated));
      AchieveManager.Get().RemoveLicenseAddedAchievesUpdatedListener(new AchieveManager.LicenseAddedAchievesUpdatedCallback(StoreManager.s_instance.OnLicenseAddedAchievesUpdated));
    }
    StoreManager.s_instance = (StoreManager) null;
  }

  public void Init()
  {
    NetCache.Get().RegisterFeatures(new NetCache.NetCacheCallback(this.OnNetCacheFeaturesReady));
    if (this.m_initComplete)
      return;
    this.m_lastConfigRequestTime = 0L;
    this.m_lastStatusRequestTime = 0L;
    this.m_configRequestDelayTicks = StoreManager.MIN_CONFIG_REQUEST_DELAY_TICKS;
    this.m_statusRequestDelayTicks = StoreManager.MIN_STATUS_REQUEST_DELAY_TICKS;
    SceneMgr.Get().RegisterSceneUnloadedEvent(new SceneMgr.SceneUnloadedCallback(this.OnSceneUnloaded));
    Network network = Network.Get();
    network.RegisterNetHandler((object) BattlePayStatusResponse.PacketID.ID, new Network.NetHandler(this.OnBattlePayStatusResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) BattlePayConfigResponse.PacketID.ID, new Network.NetHandler(this.OnBattlePayConfigResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.PurchaseMethod.PacketID.ID, new Network.NetHandler(this.OnPurchaseMethod), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.PurchaseResponse.PacketID.ID, new Network.NetHandler(this.OnPurchaseResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) CancelPurchaseResponse.PacketID.ID, new Network.NetHandler(this.OnPurchaseCanceledResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PurchaseWithGoldResponse.PacketID.ID, new Network.NetHandler(this.OnPurchaseViaGoldResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.ThirdPartyPurchaseStatusResponse.PacketID.ID, new Network.NetHandler(this.OnThirdPartyPurchaseStatusResponse), (Network.TimeoutHandler) null);
    NetCache.NetCacheProfileNotices netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileNotices>();
    if (netObject != null)
      this.OnNewNotices(netObject.Notices, false);
    NetCache.Get().RegisterNewNoticesListener(new NetCache.DelNewNoticesListener(this.OnNewNotices));
    NetCache.Get().RegisterGoldBalanceListener(new NetCache.DelGoldBalanceListener(this.OnGoldBalanceChanged));
    this.m_initComplete = true;
    AssetLoader.Get().LoadGameObject((string) this.s_storePrefab, new AssetLoader.GameObjectCallback(this.OnGeneralStoreLoaded), (object) null, false);
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
  }

  private void WillReset()
  {
    ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
    Network network = Network.Get();
    network.RemoveNetHandler((object) BattlePayStatusResponse.PacketID.ID, new Network.NetHandler(this.OnBattlePayStatusResponse));
    network.RemoveNetHandler((object) BattlePayConfigResponse.PacketID.ID, new Network.NetHandler(this.OnBattlePayConfigResponse));
    network.RemoveNetHandler((object) PegasusUtil.PurchaseMethod.PacketID.ID, new Network.NetHandler(this.OnPurchaseMethod));
    network.RemoveNetHandler((object) PegasusUtil.PurchaseResponse.PacketID.ID, new Network.NetHandler(this.OnPurchaseResponse));
    network.RemoveNetHandler((object) CancelPurchaseResponse.PacketID.ID, new Network.NetHandler(this.OnPurchaseCanceledResponse));
    network.RemoveNetHandler((object) PurchaseWithGoldResponse.PacketID.ID, new Network.NetHandler(this.OnPurchaseViaGoldResponse));
    network.RemoveNetHandler((object) PegasusUtil.ThirdPartyPurchaseStatusResponse.PacketID.ID, new Network.NetHandler(this.OnThirdPartyPurchaseStatusResponse));
    StoreMobilePurchase.Reset();
    StoreManager.DestroyInstance();
  }

  public void Heartbeat()
  {
    if (!this.m_initComplete)
      return;
    long ticks = DateTime.Now.Ticks;
    this.RequestStatusIfNeeded(ticks);
    this.RequestConfigIfNeeded(ticks);
    this.AutoCancelPurchaseIfNeeded(ticks);
  }

  public bool IsOpen()
  {
    if (!this.FirstNoticesProcessed || !this.IsStoreFeatureEnabled() || (!this.BattlePayAvailable || !this.ConfigLoaded) || !this.HaveProductsToSell())
      return false;
    return StoreManager.TransactionStatus.UNKNOWN != this.Status;
  }

  public bool IsStoreFeatureEnabled()
  {
    NetCache.NetCacheFeatures netCacheFeatures = this.GetNetCacheFeatures();
    if (netCacheFeatures == null)
      return false;
    return netCacheFeatures.Store.Store;
  }

  public bool IsBattlePayFeatureEnabled()
  {
    NetCache.NetCacheFeatures netCacheFeatures = this.GetNetCacheFeatures();
    if (netCacheFeatures == null || !netCacheFeatures.Store.Store)
      return false;
    return netCacheFeatures.Store.BattlePay;
  }

  public bool IsBuyWithGoldFeatureEnabled()
  {
    NetCache.NetCacheFeatures netCacheFeatures = this.GetNetCacheFeatures();
    if (netCacheFeatures == null || !netCacheFeatures.Store.Store)
      return false;
    return netCacheFeatures.Store.BuyWithGold;
  }

  public bool RegisterStatusChangedListener(StoreManager.StatusChangedCallback callback)
  {
    return this.RegisterStatusChangedListener(callback, (object) null);
  }

  public bool RegisterStatusChangedListener(StoreManager.StatusChangedCallback callback, object userData)
  {
    StoreManager.StatusChangedListener statusChangedListener = new StoreManager.StatusChangedListener();
    statusChangedListener.SetCallback(callback);
    statusChangedListener.SetUserData(userData);
    if (this.m_statusChangedListeners.Contains(statusChangedListener))
      return false;
    this.m_statusChangedListeners.Add(statusChangedListener);
    return true;
  }

  public bool RemoveStatusChangedListener(StoreManager.StatusChangedCallback callback)
  {
    return this.RemoveStatusChangedListener(callback, (object) null);
  }

  public bool RemoveStatusChangedListener(StoreManager.StatusChangedCallback callback, object userData)
  {
    StoreManager.StatusChangedListener statusChangedListener = new StoreManager.StatusChangedListener();
    statusChangedListener.SetCallback(callback);
    statusChangedListener.SetUserData(userData);
    return this.m_statusChangedListeners.Remove(statusChangedListener);
  }

  public bool RegisterSuccessfulPurchaseListener(StoreManager.SuccessfulPurchaseCallback callback)
  {
    return this.RegisterSuccessfulPurchaseListener(callback, (object) null);
  }

  public bool RegisterSuccessfulPurchaseListener(StoreManager.SuccessfulPurchaseCallback callback, object userData)
  {
    StoreManager.SuccessfulPurchaseListener purchaseListener = new StoreManager.SuccessfulPurchaseListener();
    purchaseListener.SetCallback(callback);
    purchaseListener.SetUserData(userData);
    if (this.m_successfulPurchaseListeners.Contains(purchaseListener))
      return false;
    this.m_successfulPurchaseListeners.Add(purchaseListener);
    return true;
  }

  public bool RemoveSuccessfulPurchaseListener(StoreManager.SuccessfulPurchaseCallback callback)
  {
    return this.RemoveSuccessfulPurchaseListener(callback, (object) null);
  }

  public bool RemoveSuccessfulPurchaseListener(StoreManager.SuccessfulPurchaseCallback callback, object userData)
  {
    StoreManager.SuccessfulPurchaseListener purchaseListener = new StoreManager.SuccessfulPurchaseListener();
    purchaseListener.SetCallback(callback);
    purchaseListener.SetUserData(userData);
    return this.m_successfulPurchaseListeners.Remove(purchaseListener);
  }

  public bool RegisterSuccessfulPurchaseAckListener(StoreManager.SuccessfulPurchaseAckCallback callback)
  {
    return this.RegisterSuccessfulPurchaseAckListener(callback, (object) null);
  }

  public bool RegisterSuccessfulPurchaseAckListener(StoreManager.SuccessfulPurchaseAckCallback callback, object userData)
  {
    StoreManager.SuccessfulPurchaseAckListener purchaseAckListener = new StoreManager.SuccessfulPurchaseAckListener();
    purchaseAckListener.SetCallback(callback);
    purchaseAckListener.SetUserData(userData);
    if (this.m_successfulPurchaseAckListeners.Contains(purchaseAckListener))
      return false;
    this.m_successfulPurchaseAckListeners.Add(purchaseAckListener);
    return true;
  }

  public bool RemoveSuccessfulPurchaseAckListener(StoreManager.SuccessfulPurchaseAckCallback callback)
  {
    return this.RemoveSuccessfulPurchaseAckListener(callback, (object) null);
  }

  public bool RemoveSuccessfulPurchaseAckListener(StoreManager.SuccessfulPurchaseAckCallback callback, object userData)
  {
    StoreManager.SuccessfulPurchaseAckListener purchaseAckListener = new StoreManager.SuccessfulPurchaseAckListener();
    purchaseAckListener.SetCallback(callback);
    purchaseAckListener.SetUserData(userData);
    return this.m_successfulPurchaseAckListeners.Remove(purchaseAckListener);
  }

  public bool RegisterAuthorizationExitListener(StoreManager.AuthorizationExitCallback callback)
  {
    return this.RegisterAuthorizationExitListener(callback, (object) null);
  }

  public bool RegisterAuthorizationExitListener(StoreManager.AuthorizationExitCallback callback, object userData)
  {
    StoreManager.AuthorizationExitListener authorizationExitListener = new StoreManager.AuthorizationExitListener();
    authorizationExitListener.SetCallback(callback);
    authorizationExitListener.SetUserData(userData);
    if (this.m_authExitListeners.Contains(authorizationExitListener))
      return false;
    this.m_authExitListeners.Add(authorizationExitListener);
    return true;
  }

  public bool RemoveAuthorizationExitListener(StoreManager.AuthorizationExitCallback callback)
  {
    return this.RemoveAuthorizationExitListener(callback, (object) null);
  }

  public bool RemoveAuthorizationExitListener(StoreManager.AuthorizationExitCallback callback, object userData)
  {
    StoreManager.AuthorizationExitListener authorizationExitListener = new StoreManager.AuthorizationExitListener();
    authorizationExitListener.SetCallback(callback);
    authorizationExitListener.SetUserData(userData);
    return this.m_authExitListeners.Remove(authorizationExitListener);
  }

  public bool RegisterStoreShownListener(StoreManager.StoreShownCallback callback)
  {
    return this.RegisterStoreShownListener(callback, (object) null);
  }

  public bool RegisterStoreShownListener(StoreManager.StoreShownCallback callback, object userData)
  {
    StoreManager.StoreShownListener storeShownListener = new StoreManager.StoreShownListener();
    storeShownListener.SetCallback(callback);
    storeShownListener.SetUserData(userData);
    if (this.m_storeShownListeners.Contains(storeShownListener))
      return false;
    this.m_storeShownListeners.Add(storeShownListener);
    return true;
  }

  public bool RemoveStoreShownListener(StoreManager.StoreShownCallback callback)
  {
    return this.RemoveStoreShownListener(callback, (object) null);
  }

  public bool RemoveStoreShownListener(StoreManager.StoreShownCallback callback, object userData)
  {
    StoreManager.StoreShownListener storeShownListener = new StoreManager.StoreShownListener();
    storeShownListener.SetCallback(callback);
    storeShownListener.SetUserData(userData);
    return this.m_storeShownListeners.Remove(storeShownListener);
  }

  public bool RegisterStoreHiddenListener(StoreManager.StoreHiddenCallback callback)
  {
    return this.RegisterStoreHiddenListener(callback, (object) null);
  }

  public bool RegisterStoreHiddenListener(StoreManager.StoreHiddenCallback callback, object userData)
  {
    StoreManager.StoreHiddenListener storeHiddenListener = new StoreManager.StoreHiddenListener();
    storeHiddenListener.SetCallback(callback);
    storeHiddenListener.SetUserData(userData);
    if (this.m_storeHiddenListeners.Contains(storeHiddenListener))
      return false;
    this.m_storeHiddenListeners.Add(storeHiddenListener);
    return false;
  }

  public bool RemoveStoreHiddenListener(StoreManager.StoreHiddenCallback callback)
  {
    return this.RemoveStoreHiddenListener(callback, (object) null);
  }

  public bool RemoveStoreHiddenListener(StoreManager.StoreHiddenCallback callback, object userData)
  {
    StoreManager.StoreHiddenListener storeHiddenListener = new StoreManager.StoreHiddenListener();
    storeHiddenListener.SetCallback(callback);
    storeHiddenListener.SetUserData(userData);
    return this.m_storeHiddenListeners.Remove(storeHiddenListener);
  }

  public bool RegisterStoreAchievesListener(StoreManager.StoreAchievesCallback callback)
  {
    return this.RegisterStoreAchievesListener(callback, (object) null);
  }

  public bool RegisterStoreAchievesListener(StoreManager.StoreAchievesCallback callback, object userData)
  {
    StoreManager.StoreAchievesListener achievesListener = new StoreManager.StoreAchievesListener();
    achievesListener.SetCallback(callback);
    achievesListener.SetUserData(userData);
    if (this.m_storeAchievesListeners.Contains(achievesListener))
      return false;
    this.m_storeAchievesListeners.Add(achievesListener);
    return false;
  }

  public bool RemoveStoreAchievesListener(StoreManager.StoreAchievesCallback callback)
  {
    return this.RemoveStoreAchievesListener(callback, (object) null);
  }

  public bool RemoveStoreAchievesListener(StoreManager.StoreAchievesCallback callback, object userData)
  {
    StoreManager.StoreAchievesListener achievesListener = new StoreManager.StoreAchievesListener();
    achievesListener.SetCallback(callback);
    achievesListener.SetUserData(userData);
    return this.m_storeAchievesListeners.Remove(achievesListener);
  }

  public bool IsWaitingToShow()
  {
    return this.m_waitingToShowStore;
  }

  public Store GetCurrentStore()
  {
    return this.GetStore(this.m_currentStoreType);
  }

  public Store GetStore(StoreType storeType)
  {
    Store store = (Store) null;
    this.m_stores.TryGetValue(storeType, out store);
    return store;
  }

  public bool IsShown()
  {
    Store currentStore = this.GetCurrentStore();
    if ((UnityEngine.Object) currentStore != (UnityEngine.Object) null)
      return currentStore.IsShown();
    return false;
  }

  public bool IsShownOrWaitingToShow()
  {
    return this.IsWaitingToShow() || this.IsShown();
  }

  public bool GetGoldCostNoGTAPP(NoGTAPPTransactionData noGTAPPTransactionData, out long cost)
  {
    cost = 0L;
    if (noGTAPPTransactionData == null)
      return false;
    long cost1 = 0;
    switch (noGTAPPTransactionData.Product)
    {
      case ProductType.PRODUCT_TYPE_BOOSTER:
        if (!this.GetBoosterGoldCostNoGTAPP(noGTAPPTransactionData.ProductData, out cost1))
          return false;
        break;
      case ProductType.PRODUCT_TYPE_DRAFT:
        if (!this.GetArenaGoldCostNoGTAPP(out cost1))
          return false;
        break;
      default:
        UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.GetGoldPriceNoGTAPP(): don't have a no-GTAPP gold price for product {0} data {1}", (object) noGTAPPTransactionData.Product, (object) noGTAPPTransactionData.ProductData));
        return false;
    }
    cost = cost1 * (long) noGTAPPTransactionData.Quantity;
    return true;
  }

  public Network.Bundle GetBundle(string productID)
  {
    if (this.m_bundles.ContainsKey(productID))
      return this.m_bundles[productID];
    UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.GetBundle(): don't have a bundle for productID '{0}'", (object) productID));
    return (Network.Bundle) null;
  }

  public HashSet<ProductType> GetProductsInItemList(List<Network.BundleItem> items)
  {
    HashSet<ProductType> productTypeSet = new HashSet<ProductType>();
    using (List<Network.BundleItem>.Enumerator enumerator = items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.BundleItem current = enumerator.Current;
        productTypeSet.Add(current.Product);
      }
    }
    return productTypeSet;
  }

  public HashSet<ProductType> GetProductsInBundle(Network.Bundle bundle)
  {
    if (bundle == null)
      return new HashSet<ProductType>();
    return this.GetProductsInItemList(bundle.Items);
  }

  public bool IsProductAlreadyOwned(Network.Bundle bundle)
  {
    if (bundle == null)
      return false;
    using (List<Network.BundleItem>.Enumerator enumerator = bundle.Items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.BundleItem current = enumerator.Current;
        if (this.AlreadyOwnsProduct(current.Product, current.ProductData))
          return true;
      }
    }
    return false;
  }

  public bool IsProductFree(Network.Bundle bundle)
  {
    return false;
  }

  public bool IsProductPrePurchase(Network.Bundle bundle)
  {
    if (bundle == null)
      return false;
    HashSet<ProductType> productsInItemList = this.GetProductsInItemList(bundle.Items);
    return productsInItemList.Contains(ProductType.PRODUCT_TYPE_BRM) && productsInItemList.Contains(ProductType.PRODUCT_TYPE_CARD_BACK) || productsInItemList.Contains(ProductType.PRODUCT_TYPE_WING) && productsInItemList.Contains(ProductType.PRODUCT_TYPE_CARD_BACK) || productsInItemList.Contains(ProductType.PRODUCT_TYPE_BOOSTER) && productsInItemList.Contains(ProductType.PRODUCT_TYPE_CARD_BACK) && (bundle.Items.Find((Predicate<Network.BundleItem>) (obj =>
    {
      if (obj.Product == ProductType.PRODUCT_TYPE_BOOSTER)
        return obj.ProductData == 10;
      return false;
    })) != null || bundle.Items.Find((Predicate<Network.BundleItem>) (obj =>
    {
      if (obj.Product == ProductType.PRODUCT_TYPE_BOOSTER)
        return obj.ProductData == 11;
      return false;
    })) != null);
  }

  public bool IsProductFirstPurchaseBundle(Network.Bundle bundle)
  {
    return bundle != null && this.GetProductsInItemList(bundle.Items).Contains(ProductType.PRODUCT_TYPE_HIDDEN_LICENSE) && bundle.Items.Find((Predicate<Network.BundleItem>) (obj =>
    {
      if (obj.Product == ProductType.PRODUCT_TYPE_HIDDEN_LICENSE)
        return obj.ProductData == 1;
      return false;
    })) != null;
  }

  public List<Network.Bundle> GetAllBundlesForProduct(ProductType product, bool requireRealMoneyOption, int productData = 0, int numItemsRequired = 0)
  {
    List<Network.Bundle> bundleList = new List<Network.Bundle>();
    using (Map<string, Network.Bundle>.ValueCollection.Enumerator enumerator1 = this.m_bundles.Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Network.Bundle current1 = enumerator1.Current;
        if (numItemsRequired == 0 || current1.Items.Count == numItemsRequired)
        {
          bool flag = false;
          using (List<Network.BundleItem>.Enumerator enumerator2 = current1.Items.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Network.BundleItem current2 = enumerator2.Current;
              if (current2.Product == product && (productData == 0 || current2.ProductData == productData))
              {
                flag = true;
                break;
              }
            }
          }
          if (flag && this.IsBundleAvailableNow(current1, requireRealMoneyOption))
            bundleList.Add(current1);
        }
      }
    }
    return bundleList;
  }

  public Network.Bundle GetLowestCostUnownedBundle(ProductType product, bool requireRealMoneyOption, int productData, int numItemsRequired = 0)
  {
    List<Network.Bundle> bundlesForProduct = StoreManager.Get().GetAllBundlesForProduct(product, requireRealMoneyOption, productData, numItemsRequired);
    Network.Bundle bundle = (Network.Bundle) null;
    using (List<Network.Bundle>.Enumerator enumerator = bundlesForProduct.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Bundle current = enumerator.Current;
        if ((numItemsRequired == 0 || current.Items.Count == numItemsRequired) && !this.IsProductAlreadyOwned(current))
        {
          if (bundle == null)
          {
            bundle = current;
          }
          else
          {
            double? cost1 = bundle.Cost;
            int num;
            if (cost1.HasValue)
            {
              double? cost2 = current.Cost;
              if (cost2.HasValue)
              {
                num = cost1.Value <= cost2.Value ? 1 : 0;
                goto label_9;
              }
            }
            num = 0;
label_9:
            if (num == 0)
              bundle = current;
          }
        }
      }
    }
    return bundle;
  }

  public List<Network.Bundle> GetAvailableBundlesForProduct(ProductType productType, bool requireRealMoneyOption, bool includeAlreadyOwnedBundles, out bool productTypeExists, int productData = 0, int numItemsRequired = 0)
  {
    productTypeExists = false;
    bool flag1 = false;
    List<Network.Bundle> bundleList = new List<Network.Bundle>();
    using (Map<string, Network.Bundle>.ValueCollection.Enumerator enumerator1 = this.m_bundles.Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Network.Bundle current1 = enumerator1.Current;
        if ((numItemsRequired == 0 || current1.Items.Count == numItemsRequired) && this.IsBundleAvailableNow(current1, requireRealMoneyOption))
        {
          bool flag2 = false;
          bool flag3 = false;
          using (List<Network.BundleItem>.Enumerator enumerator2 = current1.Items.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Network.BundleItem current2 = enumerator2.Current;
              if (current2.Product == productType)
                productTypeExists = true;
              flag3 = this.AlreadyOwnsProduct(current2.Product, current2.ProductData);
              if (flag3)
              {
                flag1 = true;
                if (!includeAlreadyOwnedBundles)
                  break;
              }
              if (current2.Product == productType && (productData == 0 || current2.ProductData == productData))
              {
                flag2 = true;
                break;
              }
            }
          }
          if (flag2 && (!flag3 || includeAlreadyOwnedBundles))
            bundleList.Add(current1);
        }
      }
    }
    if (bundleList.Count == 0 && !flag1)
      productTypeExists = false;
    return bundleList;
  }

  public bool GetAvailableAdventureBundle(AdventureDbId adventureId, bool requireRealMoneyOption, out Network.Bundle bundle, out bool productExists)
  {
    bundle = (Network.Bundle) null;
    if (StoreManager.GetAdventureProductType(adventureId) == ProductType.PRODUCT_TYPE_UNKNOWN)
    {
      productExists = false;
      return false;
    }
    bool productTypeExists = false;
    List<Network.Bundle> bundlesForProduct;
    switch (adventureId)
    {
      case AdventureDbId.NAXXRAMAS:
        bundlesForProduct = this.GetAvailableBundlesForProduct(ProductType.PRODUCT_TYPE_NAXX, requireRealMoneyOption, false, out productTypeExists, 5, 0);
        break;
      case AdventureDbId.BRM:
        bundlesForProduct = this.GetAvailableBundlesForProduct(ProductType.PRODUCT_TYPE_BRM, requireRealMoneyOption, false, out productTypeExists, 10, 0);
        break;
      case AdventureDbId.LOE:
        bundlesForProduct = this.GetAvailableBundlesForProduct(ProductType.PRODUCT_TYPE_LOE, requireRealMoneyOption, false, out productTypeExists, 14, 0);
        break;
      default:
        int finalAdventureWing = GameUtils.GetFinalAdventureWing((int) adventureId);
        bundlesForProduct = this.GetAvailableBundlesForProduct(ProductType.PRODUCT_TYPE_WING, requireRealMoneyOption, false, out productTypeExists, finalAdventureWing, 0);
        break;
    }
    productExists = productTypeExists;
    if (bundlesForProduct != null)
    {
      using (List<Network.Bundle>.Enumerator enumerator = bundlesForProduct.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Network.Bundle current = enumerator.Current;
          int count = current.Items.Count;
          if (count != 0 && this.IsBundleAvailableNow(current, requireRealMoneyOption))
          {
            if (bundle == null)
              bundle = current;
            else if (bundle.Items.Count <= count)
              bundle = current;
          }
        }
      }
    }
    return bundle != null;
  }

  public bool GetAvailableAdventureBundle(AdventureDbId adventureId, bool requireRealMoneyOption, out Network.Bundle bundle)
  {
    bool productExists;
    return this.GetAvailableAdventureBundle(adventureId, requireRealMoneyOption, out bundle, out productExists);
  }

  public bool CanBuyBoosterWithGold(int boosterDbId)
  {
    BoosterDbfRecord record = GameDbf.Booster.GetRecord(boosterDbId);
    SpecialEventType outVal;
    if (record == null || string.IsNullOrEmpty(record.BuyWithGoldEvent) || !EnumUtils.TryGetEnum<SpecialEventType>(record.BuyWithGoldEvent, out outVal))
      return false;
    if (outVal == SpecialEventType.IGNORE)
      return true;
    return SpecialEventManager.Get().IsEventActive(outVal, false);
  }

  public bool IsBoosterPreorderActive(int boosterDbId, out Network.Bundle preOrderBundle)
  {
    using (List<Network.Bundle>.Enumerator enumerator = this.GetAllBundlesForProduct(ProductType.PRODUCT_TYPE_BOOSTER, GeneralStorePacksContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, boosterDbId, 0).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Bundle current = enumerator.Current;
        if (this.IsProductPrePurchase(current))
        {
          preOrderBundle = current;
          return true;
        }
      }
    }
    preOrderBundle = (Network.Bundle) null;
    return false;
  }

  public bool IsBoosterFirstPurchaseBundle(int boosterDbId, out Network.Bundle firstPurchaseBundle)
  {
    if (boosterDbId != 17)
    {
      firstPurchaseBundle = (Network.Bundle) null;
      return false;
    }
    List<Network.Bundle> bundlesForProduct = this.GetAllBundlesForProduct(ProductType.PRODUCT_TYPE_HIDDEN_LICENSE, GeneralStorePacksContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, 1, 0);
    if (bundlesForProduct.Count <= 0)
    {
      firstPurchaseBundle = (Network.Bundle) null;
      return false;
    }
    firstPurchaseBundle = bundlesForProduct[0];
    return true;
  }

  public bool GetHeroBundleByCardDbId(int heroCardDbId, out Network.Bundle heroBundle)
  {
    bool productTypeExists = false;
    List<Network.Bundle> bundlesForProduct = this.GetAvailableBundlesForProduct(ProductType.PRODUCT_TYPE_HERO, false, true, out productTypeExists, 0, 0);
    heroBundle = (Network.Bundle) null;
    if (!productTypeExists)
      return false;
    using (List<Network.Bundle>.Enumerator enumerator1 = bundlesForProduct.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Network.Bundle current1 = enumerator1.Current;
        using (List<Network.BundleItem>.Enumerator enumerator2 = current1.Items.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Network.BundleItem current2 = enumerator2.Current;
            if (current2.Product == ProductType.PRODUCT_TYPE_HERO && current2.ProductData == heroCardDbId)
            {
              heroBundle = current1;
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  public bool GetHeroBundleByCardMiniGuid(string heroCardId_aka_miniGuid, out Network.Bundle heroBundle)
  {
    return this.GetHeroBundleByCardDbId(GameUtils.TranslateCardIdToDbId(heroCardId_aka_miniGuid), out heroBundle);
  }

  public bool IsChinaCustomer()
  {
    return this.m_currency == Currency.CPT;
  }

  public bool IsKoreanCustomer()
  {
    return this.m_currency == Currency.KRW;
  }

  public bool IsEuropeanCustomer()
  {
    return this.m_currency == Currency.GBP || this.m_currency == Currency.RUB || this.m_currency == Currency.EUR;
  }

  public bool IsNorthAmericanCustomer()
  {
    return this.m_currency == Currency.USD || this.m_currency == Currency.ARS || (this.m_currency == Currency.CLP || this.m_currency == Currency.MXN) || (this.m_currency == Currency.BRL || this.m_currency == Currency.AUD || this.m_currency == Currency.SGD);
  }

  public string GetTaxText()
  {
    Currency currency = this.m_currency;
    switch (currency)
    {
      case Currency.USD:
        return GameStrings.Get("GLUE_STORE_SUMMARY_TAX_DISCLAIMER_USD");
      case Currency.KRW:
        return string.Empty;
      default:
        if (currency != Currency.CPT && currency != Currency.TPT)
          return GameStrings.Get("GLUE_STORE_SUMMARY_TAX_DISCLAIMER");
        goto case Currency.KRW;
    }
  }

  public string FormatCostBundle(Network.Bundle bundle)
  {
    if (!bundle.Cost.HasValue)
      return string.Empty;
    if ((bool) StoreManager.HAS_THIRD_PARTY_APP_STORE && ApplicationMgr.GetAndroidStore() != AndroidStore.BLIZZARD)
    {
      string mobileProductId = string.Empty;
      if (ApplicationMgr.GetAndroidStore() == AndroidStore.GOOGLE)
        mobileProductId = bundle.GooglePlayID;
      else if (ApplicationMgr.GetAndroidStore() == AndroidStore.AMAZON)
        mobileProductId = bundle.AmazonID;
      string localizedProductPrice = StoreMobilePurchase.GetLocalizedProductPrice(mobileProductId);
      if (localizedProductPrice.Length > 0)
        return localizedProductPrice;
    }
    return this.FormatCostText(bundle.Cost.Value);
  }

  public string FormatCostText(double cost)
  {
    return StoreManager.FormatCostText(cost, this.m_currency);
  }

  public static string FormatCostText(double cost, Currency currency)
  {
    if (!StoreManager.s_currencyToLocaleMap.ContainsKey(currency))
      Error.AddFatalLoc("GLOBAL_ERROR_CURRENCY_INVALID");
    Locale currencyToLocale = StoreManager.s_currencyToLocaleMap[currency];
    string defaultCurrencyFormat;
    if (!StoreManager.s_currencySpecialFormats.TryGetValue(currency, out defaultCurrencyFormat))
      defaultCurrencyFormat = StoreManager.DEFAULT_CURRENCY_FORMAT;
    CultureInfo specificCulture = CultureInfo.CreateSpecificCulture(Localization.ConvertLocaleToDotNet(currencyToLocale));
    switch (currencyToLocale)
    {
      case Locale.koKR:
        specificCulture.NumberFormat.CurrencySymbol = "B";
        break;
      case Locale.ruRU:
        specificCulture.NumberFormat.CurrencySymbol = string.Format(" {0}", (object) specificCulture.NumberFormat.CurrencySymbol);
        break;
      case Locale.zhTW:
        specificCulture.NumberFormat.CurrencyGroupSeparator = string.Empty;
        break;
    }
    return string.Format((IFormatProvider) specificCulture, defaultCurrencyFormat, new object[1]{ (object) cost });
  }

  public string GetProductName(List<Network.BundleItem> items)
  {
    string empty = string.Empty;
    return items.Count != 1 ? this.GetMultiItemProductName(items) : this.GetSingleItemProductName(items[0]);
  }

  public int GetWingItemCount(List<Network.BundleItem> items)
  {
    int num = 0;
    using (List<Network.BundleItem>.Enumerator enumerator = items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        switch (enumerator.Current.Product)
        {
          case ProductType.PRODUCT_TYPE_NAXX:
            ++num;
            continue;
          case ProductType.PRODUCT_TYPE_BRM:
            ++num;
            continue;
          case ProductType.PRODUCT_TYPE_LOE:
            ++num;
            continue;
          case ProductType.PRODUCT_TYPE_WING:
            ++num;
            continue;
          default:
            continue;
        }
      }
    }
    return num;
  }

  public string GetProductQuantityText(ProductType product, int productData, int quantity)
  {
    string str = string.Empty;
    switch (product)
    {
      case ProductType.PRODUCT_TYPE_BOOSTER:
        str = GameStrings.Format("GLUE_STORE_QUANTITY_PACK", (object) quantity);
        break;
      case ProductType.PRODUCT_TYPE_DRAFT:
        str = GameStrings.Format("GLUE_STORE_SUMMARY_ITEM_ORDERED", (object) quantity, (object) GameStrings.Get("GLUE_STORE_PRODUCT_NAME_FORGE_TICKET"));
        break;
      default:
        UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.GetProductQuantityText(): don't know how to format quantity for product {0} (data {1})", (object) product, (object) productData));
        break;
    }
    return str;
  }

  public void GetThirdPartyPurchaseStatus(string transactionID)
  {
    Network.GetThirdPartyPurchaseStatus(transactionID);
  }

  public bool DoZeroCostTransactionIfPossible(StoreType storeType, Store.ExitCallback exitCallback, object userData, ProductType product, int productData = 0, int numItems = 0)
  {
    if (this.m_waitingToShowStore)
    {
      Log.Rachelle.Print("StoreManager.DoZeroCostTransactionIfPossible(): already waiting to show store");
      return false;
    }
    bool productTypeExists = false;
    List<Network.Bundle> bundlesForProduct = this.GetAvailableBundlesForProduct(product, false, false, out productTypeExists, productData, numItems);
    Network.Bundle bundle = (Network.Bundle) null;
    using (List<Network.Bundle>.Enumerator enumerator = bundlesForProduct.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Bundle current = enumerator.Current;
        if (this.IsProductFree(current))
        {
          bundle = current;
          break;
        }
      }
    }
    if (bundle == null)
      return false;
    StoreManager.ZeroCostTransactionData costTransactionData = new StoreManager.ZeroCostTransactionData(bundle);
    this.m_currentStoreType = storeType;
    this.m_showStoreData.m_exitCallback = exitCallback;
    this.m_showStoreData.m_exitCallbackUserData = userData;
    this.m_showStoreData.m_isTotallyFake = true;
    this.m_showStoreData.m_storeProduct = product;
    this.m_showStoreData.m_storeProductData = bundle.Items.Count != 1 ? productData : bundle.Items[0].ProductData;
    this.m_showStoreData.m_storeMode = GeneralStoreMode.NONE;
    this.m_showStoreData.m_zeroCostTransactionData = costTransactionData;
    this.ShowStoreWhenLoaded();
    return true;
  }

  public void StartGeneralTransaction()
  {
    this.StartGeneralTransaction((GeneralStoreMode) StoreManager.s_defaultStoreMode);
  }

  public void StartGeneralTransaction(GeneralStoreMode mode)
  {
    if (this.m_waitingToShowStore)
    {
      Log.Rachelle.Print("StoreManager.StartGeneralTransaction(): already waiting to show store");
    }
    else
    {
      this.m_currentStoreType = StoreType.GENERAL_STORE;
      this.m_showStoreData.m_exitCallback = (Store.ExitCallback) null;
      this.m_showStoreData.m_exitCallbackUserData = (object) null;
      this.m_showStoreData.m_isTotallyFake = false;
      this.m_showStoreData.m_storeProduct = ProductType.PRODUCT_TYPE_UNKNOWN;
      this.m_showStoreData.m_storeProductData = 0;
      this.m_showStoreData.m_storeMode = mode;
      this.m_showStoreData.m_zeroCostTransactionData = (StoreManager.ZeroCostTransactionData) null;
      this.ShowStoreWhenLoaded();
    }
  }

  public void StartArenaTransaction(Store.ExitCallback exitCallback, object exitCallbackUserData, bool isTotallyFake)
  {
    if (this.m_waitingToShowStore)
    {
      Log.Rachelle.Print("StoreManager.StartArenaTransaction(): already waiting to show store");
    }
    else
    {
      this.m_currentStoreType = StoreType.ARENA_STORE;
      this.m_showStoreData.m_exitCallback = exitCallback;
      this.m_showStoreData.m_exitCallbackUserData = (object) null;
      this.m_showStoreData.m_isTotallyFake = isTotallyFake;
      this.m_showStoreData.m_storeProduct = ProductType.PRODUCT_TYPE_UNKNOWN;
      this.m_showStoreData.m_storeProductData = 0;
      this.m_showStoreData.m_zeroCostTransactionData = (StoreManager.ZeroCostTransactionData) null;
      this.ShowStoreWhenLoaded();
    }
  }

  public void StartTavernBrawlTransaction(Store.ExitCallback exitCallback, bool isTotallyFake)
  {
    if (this.m_waitingToShowStore)
    {
      Log.Alex.Print("StoreManager.StartTavernBrawlTransaction(): already waiting to show store");
    }
    else
    {
      this.m_currentStoreType = StoreType.TAVERN_BRAWL_STORE;
      this.m_showStoreData.m_exitCallback = exitCallback;
      this.m_showStoreData.m_exitCallbackUserData = (object) null;
      this.m_showStoreData.m_isTotallyFake = isTotallyFake;
      this.m_showStoreData.m_storeProduct = ProductType.PRODUCT_TYPE_UNKNOWN;
      this.m_showStoreData.m_storeProductData = 0;
      this.m_showStoreData.m_zeroCostTransactionData = (StoreManager.ZeroCostTransactionData) null;
      this.ShowStoreWhenLoaded();
    }
  }

  public void StartAdventureTransaction(ProductType product, int productData, Store.ExitCallback exitCallback, object exitCallbackUserData)
  {
    if (this.m_waitingToShowStore)
    {
      Log.Rachelle.Print("StoreManager.StartAdventureTransaction(): already waiting to show store");
    }
    else
    {
      if (!this.CanBuyProduct(product, productData))
        return;
      this.m_currentStoreType = StoreType.ADVENTURE_STORE;
      this.m_showStoreData.m_exitCallback = exitCallback;
      this.m_showStoreData.m_exitCallbackUserData = exitCallbackUserData;
      this.m_showStoreData.m_isTotallyFake = false;
      this.m_showStoreData.m_storeProduct = product;
      this.m_showStoreData.m_storeProductData = productData;
      this.m_showStoreData.m_zeroCostTransactionData = (StoreManager.ZeroCostTransactionData) null;
      this.ShowStoreWhenLoaded();
    }
  }

  public void HideStore(StoreType storeType)
  {
    Store store = this.GetStore(storeType);
    if (!((UnityEngine.Object) store != (UnityEngine.Object) null))
      return;
    store.Hide();
    this.HideAllPurchasePopups();
  }

  public bool TransactionInProgress()
  {
    return this.Status == StoreManager.TransactionStatus.IN_PROGRESS_MONEY || this.Status == StoreManager.TransactionStatus.IN_PROGRESS_GOLD_GTAPP || (this.Status == StoreManager.TransactionStatus.IN_PROGRESS_GOLD_NO_GTAPP || this.Status == StoreManager.TransactionStatus.WAIT_THIRD_PARTY_RECEIPT) || this.Status == StoreManager.TransactionStatus.WAIT_THIRD_PARTY_INIT || StoreManager.TransactionStatus.WAIT_ZERO_COST_LICENSE == this.Status;
  }

  public bool IsPromptShowing()
  {
    if ((UnityEngine.Object) this.m_storePurchaseAuth != (UnityEngine.Object) null && this.m_storePurchaseAuth.IsShown() || (UnityEngine.Object) this.m_storeSummary != (UnityEngine.Object) null && this.m_storeSummary.IsShown() || ((UnityEngine.Object) this.m_storeSendToBAM != (UnityEngine.Object) null && this.m_storeSendToBAM.IsShown() || (UnityEngine.Object) this.m_storeLegalBAMLinks != (UnityEngine.Object) null && this.m_storeLegalBAMLinks.IsShown()) || (UnityEngine.Object) this.m_storeDoneWithBAM != (UnityEngine.Object) null && this.m_storeDoneWithBAM.IsShown())
      return true;
    if ((UnityEngine.Object) this.m_storeChallengePrompt != (UnityEngine.Object) null)
      return this.m_storeChallengePrompt.IsShown();
    return false;
  }

  public bool HasOutstandingPurchaseNotices(ProductType product)
  {
    foreach (NetCache.ProfileNoticePurchase profileNoticePurchase in this.m_outstandingPurchaseNotices.ToArray())
    {
      Network.Bundle bundle = this.GetBundle(profileNoticePurchase.ProductID);
      if (bundle != null)
      {
        using (List<Network.BundleItem>.Enumerator enumerator = bundle.Items.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            if (enumerator.Current.Product == product)
              return true;
          }
        }
      }
    }
    return false;
  }

  public static ProductType GetAdventureProductType(AdventureDbId adventureId)
  {
    ProductType productType = ProductType.PRODUCT_TYPE_UNKNOWN;
    if (StoreManager.s_adventureToProductMap.TryGetValue(adventureId, out productType))
      return productType;
    return GameUtils.IsExpansionAdventure(adventureId) ? ProductType.PRODUCT_TYPE_WING : ProductType.PRODUCT_TYPE_UNKNOWN;
  }

  public bool IsIdActiveTransaction(long id)
  {
    if (this.m_activeMoneyOrGTAPPTransaction == null)
      return false;
    return id == this.m_activeMoneyOrGTAPPTransaction.ID;
  }

  public static bool IsFirstPurchaseBundleBooster(int boosterId)
  {
    return 17 == boosterId;
  }

  public static bool IsFirstPurchaseBundleOwned()
  {
    HiddenLicenseDbfRecord record1 = GameDbf.HiddenLicense.GetRecord(1);
    if (record1 == null)
      return false;
    AccountLicenseDbfRecord record2 = GameDbf.AccountLicense.GetRecord(record1.AccountLicenseId);
    if (record2 == null)
      return false;
    return AccountLicenseMgr.Get().OwnsAccountLicense(record2.LicenseId);
  }

  private void ShowStoreWhenLoaded()
  {
    this.m_showStoreStart = Time.realtimeSinceStartup;
    this.m_waitingToShowStore = true;
    if (!this.IsCurrentStoreLoaded())
      this.Load(this.m_currentStoreType);
    else
      this.ShowStore();
  }

  private void ShowStore()
  {
    if (!this.m_licenseAchievesListenerRegistered)
    {
      AchieveManager.Get().RegisterLicenseAddedAchievesUpdatedListener(new AchieveManager.LicenseAddedAchievesUpdatedCallback(this.OnLicenseAddedAchievesUpdated));
      this.m_licenseAchievesListenerRegistered = true;
    }
    if (this.Status == StoreManager.TransactionStatus.READY && AchieveManager.Get().HasActiveLicenseAddedAchieves())
      this.Status = StoreManager.TransactionStatus.WAIT_ZERO_COST_LICENSE;
    Store currentStore = this.GetCurrentStore();
    bool flag = true;
    switch (this.m_currentStoreType)
    {
      case StoreType.GENERAL_STORE:
        if (this.IsOpen())
        {
          ((GeneralStore) currentStore).SetMode(this.m_showStoreData.m_storeMode);
          break;
        }
        UnityEngine.Debug.LogWarning((object) "StoreManager.ShowStore(): Cannot show general store.. Store is not open");
        if (this.m_showStoreData.m_exitCallback != null)
          this.m_showStoreData.m_exitCallback(false, this.m_showStoreData.m_exitCallbackUserData);
        flag = false;
        break;
      case StoreType.ARENA_STORE:
        currentStore.RegisterExitListener(this.m_showStoreData.m_exitCallback, this.m_showStoreData.m_exitCallbackUserData);
        break;
      case StoreType.ADVENTURE_STORE:
        if (this.IsOpen())
        {
          AdventureStore adventureStore = (AdventureStore) currentStore;
          if ((UnityEngine.Object) adventureStore != (UnityEngine.Object) null)
            adventureStore.SetAdventureProduct(this.m_showStoreData.m_storeProduct, this.m_showStoreData.m_storeProductData);
          if (this.m_showStoreData.m_exitCallback != null)
          {
            currentStore.RegisterExitListener(this.m_showStoreData.m_exitCallback, this.m_showStoreData.m_exitCallbackUserData);
            break;
          }
          break;
        }
        UnityEngine.Debug.LogWarning((object) "StoreManager.ShowStore(): Cannot show adventure store.. Store is not open");
        if (this.m_showStoreData.m_exitCallback != null)
          this.m_showStoreData.m_exitCallback(false, this.m_showStoreData.m_exitCallbackUserData);
        flag = false;
        break;
      case StoreType.TAVERN_BRAWL_STORE:
        currentStore.RegisterExitListener(this.m_showStoreData.m_exitCallback, this.m_showStoreData.m_exitCallbackUserData);
        break;
    }
    if (flag)
      currentStore.Show(new Store.DelOnStoreShown(this.OnStoreShown), this.m_showStoreData.m_isTotallyFake);
    if (this.m_currentStoreType == StoreType.ADVENTURE_STORE && this.IsOpen() && (this.Status == StoreManager.TransactionStatus.READY && this.m_showStoreData.m_zeroCostTransactionData != null))
    {
      this.Status = StoreManager.TransactionStatus.WAIT_ZERO_COST_LICENSE;
      this.ActivateStoreCover();
      this.m_storePurchaseAuth.Show((MoneyOrGTAPPTransaction) null, true, true);
      Network.GetPurchaseMethod(this.m_showStoreData.m_zeroCostTransactionData.Bundle.ProductID, 1, this.m_currency);
    }
    Log.Store.Print("{0} took {1}s to load", new object[2]
    {
      (object) this.m_currentStoreType,
      (object) (float) ((double) Time.realtimeSinceStartup - (double) this.m_showStoreStart)
    });
    this.m_waitingToShowStore = false;
  }

  private void OnStoreShown()
  {
    foreach (StoreManager.StoreShownListener storeShownListener in this.m_storeShownListeners.ToArray())
      storeShownListener.Fire();
    if (this.TransactionInProgress())
    {
      this.ActivateStoreCover();
      bool enableBackButton = this.ShouldEnablePurchaseAuthBackButton(this.m_currentStoreType);
      if (this.Status == StoreManager.TransactionStatus.WAIT_ZERO_COST_LICENSE && this.m_currentStoreType == StoreType.ADVENTURE_STORE)
        enableBackButton = true;
      if (this.Status == StoreManager.TransactionStatus.WAIT_THIRD_PARTY_RECEIPT && this.m_outOfSessionThirdPartyTransaction)
      {
        int storeProvider = (int) StoreManager.StoreProvider;
        BattlePayProvider? provider = this.m_activeMoneyOrGTAPPTransaction.Provider;
        int valueOrDefault = (int) provider.GetValueOrDefault();
        if ((storeProvider != valueOrDefault ? 1 : (!provider.HasValue ? 1 : 0)) != 0)
        {
          this.m_storePurchaseAuth.ShowPurchaseLocked(this.m_activeMoneyOrGTAPPTransaction, enableBackButton, StoreManager.TransactionStatus.WAIT_ZERO_COST_LICENSE == this.Status, (StorePurchaseAuth.PurchaseLockedDialogCallback) (showHelp =>
          {
            if (showHelp)
              Application.OpenURL(NydusLink.GetSupportLink("outstanding-purchase", false));
            this.GetCurrentStore().Close();
          }));
          return;
        }
      }
      this.m_storePurchaseAuth.Show(this.m_activeMoneyOrGTAPPTransaction, enableBackButton, StoreManager.TransactionStatus.WAIT_ZERO_COST_LICENSE == this.Status);
    }
    else
    {
      Log.Store.Print("StoreManager.OnStoreShown() m_outstandingPurchaseNoticesCount={0}.", (object) this.m_outstandingPurchaseNotices.Count);
      if (this.m_outstandingPurchaseNotices.Count > 0)
        this.ActivateStoreCover();
      this.m_handlePurchaseNoticesCoroutine = SceneMgr.Get().StartCoroutine(this.HandleAllOutstandingPurchaseNotices());
    }
  }

  [DebuggerHidden]
  private IEnumerator HandleAllOutstandingPurchaseNotices()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StoreManager.\u003CHandleAllOutstandingPurchaseNotices\u003Ec__Iterator276() { \u003C\u003Ef__this = this };
  }

  private bool ShouldEnablePurchaseAuthBackButton(StoreType storeType)
  {
    switch (this.m_currentStoreType)
    {
      case StoreType.ARENA_STORE:
        return true;
      case StoreType.TAVERN_BRAWL_STORE:
        return true;
      default:
        return false;
    }
  }

  private bool IsCurrentStoreLoaded()
  {
    Store currentStore = this.GetCurrentStore();
    return !((UnityEngine.Object) currentStore == (UnityEngine.Object) null) && currentStore.IsReady() && (!((UnityEngine.Object) this.m_storePurchaseAuth == (UnityEngine.Object) null) && !((UnityEngine.Object) this.m_storeSummary == (UnityEngine.Object) null)) && (!((UnityEngine.Object) this.m_storeSendToBAM == (UnityEngine.Object) null) && !((UnityEngine.Object) this.m_storeLegalBAMLinks == (UnityEngine.Object) null) && (!((UnityEngine.Object) this.m_storeDoneWithBAM == (UnityEngine.Object) null) && !((UnityEngine.Object) this.m_storeChallengePrompt == (UnityEngine.Object) null)));
  }

  private void Load(StoreType storeType)
  {
    if ((UnityEngine.Object) this.GetCurrentStore() != (UnityEngine.Object) null)
      return;
    switch (storeType)
    {
      case StoreType.GENERAL_STORE:
        AssetLoader.Get().LoadGameObject((string) this.s_storePrefab, new AssetLoader.GameObjectCallback(this.OnGeneralStoreLoaded), (object) null, false);
        break;
      case StoreType.ARENA_STORE:
        AssetLoader.Get().LoadGameObject((string) this.s_arenaStorePrefab, new AssetLoader.GameObjectCallback(this.OnArenaStoreLoaded), (object) null, false);
        break;
      case StoreType.ADVENTURE_STORE:
        AssetLoader.Get().LoadGameObject((string) this.s_adventureStorePrefab, new AssetLoader.GameObjectCallback(this.OnAdventureStoreLoaded), (object) null, false);
        break;
      case StoreType.TAVERN_BRAWL_STORE:
        AssetLoader.Get().LoadGameObject((string) this.s_heroicBrawlStorePrefab, new AssetLoader.GameObjectCallback(this.OnHeroicBrawlStoreLoaded), (object) null, false);
        break;
    }
    AssetLoader.Get().LoadGameObject((string) this.s_storePurchaseAuthPrefab, new AssetLoader.GameObjectCallback(this.OnStorePurchaseAuthLoaded), (object) null, false);
    AssetLoader.Get().LoadGameObject((string) this.s_storeSummaryPrefab, new AssetLoader.GameObjectCallback(this.OnStoreSummaryLoaded), (object) null, false);
    AssetLoader.Get().LoadGameObject((string) this.s_storeSendToBAMPrefab, new AssetLoader.GameObjectCallback(this.OnStoreSendToBAMLoaded), (object) null, false);
    AssetLoader.Get().LoadGameObject((string) this.s_storeDoneWithBAMPrefab, new AssetLoader.GameObjectCallback(this.OnStoreDoneWithBAMLoaded), (object) null, false);
    AssetLoader.Get().LoadGameObject((string) this.s_storeChallengePromptPrefab, new AssetLoader.GameObjectCallback(this.OnStoreChallengePromptLoaded), (object) null, false);
    AssetLoader.Get().LoadGameObject((string) this.s_storeLegalBAMLinksPrefab, new AssetLoader.GameObjectCallback(this.OnStoreLegalBAMLinksLoaded), (object) null, false);
  }

  public void UnloadAndFreeMemory()
  {
    using (Map<StoreType, Store>.Enumerator enumerator = this.m_stores.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<StoreType, Store> current = enumerator.Current;
        if ((UnityEngine.Object) current.Value != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) current.Value);
      }
    }
    this.m_stores.Clear();
    if ((UnityEngine.Object) this.m_storePurchaseAuth != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_storePurchaseAuth.gameObject);
      this.m_storePurchaseAuth = (StorePurchaseAuth) null;
    }
    if ((UnityEngine.Object) this.m_storeSummary != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_storeSummary.gameObject);
      this.m_storeSummary = (StoreSummary) null;
    }
    if ((UnityEngine.Object) this.m_storeSendToBAM != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_storeSendToBAM.gameObject);
      this.m_storeSendToBAM = (StoreSendToBAM) null;
    }
    if ((UnityEngine.Object) this.m_storeLegalBAMLinks != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_storeLegalBAMLinks.gameObject);
      this.m_storeLegalBAMLinks = (StoreLegalBAMLinks) null;
    }
    if ((UnityEngine.Object) this.m_storeDoneWithBAM != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_storeDoneWithBAM.gameObject);
      this.m_storeDoneWithBAM = (StoreDoneWithBAM) null;
    }
    if (!((UnityEngine.Object) this.m_storeChallengePrompt != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_storeChallengePrompt.gameObject);
    this.m_storeChallengePrompt = (StoreChallengePrompt) null;
  }

  private void FireStatusChangedEventIfNeeded()
  {
    bool isOpen = this.IsOpen();
    if (this.m_openWhenLastEventFired == isOpen)
      return;
    foreach (StoreManager.StatusChangedListener statusChangedListener in this.m_statusChangedListeners.ToArray())
      statusChangedListener.Fire(isOpen);
    this.m_openWhenLastEventFired = isOpen;
  }

  private NetCache.NetCacheFeatures GetNetCacheFeatures()
  {
    if (!this.FeaturesReady)
      return (NetCache.NetCacheFeatures) null;
    NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
    if (netObject == null)
      this.FeaturesReady = false;
    return netObject;
  }

  private bool AlreadyOwnsProduct(ProductType product, int productData)
  {
    switch (product)
    {
      case ProductType.PRODUCT_TYPE_BOOSTER:
      case ProductType.PRODUCT_TYPE_DRAFT:
      case ProductType.PRODUCT_TYPE_RANDOM_CARD:
      case ProductType.PRODUCT_TYPE_TAVERN_BRAWL_TICKET:
        return false;
      case ProductType.PRODUCT_TYPE_NAXX:
      case ProductType.PRODUCT_TYPE_BRM:
      case ProductType.PRODUCT_TYPE_LOE:
      case ProductType.PRODUCT_TYPE_WING:
        return AdventureProgressMgr.Get().OwnsWing(productData);
      case ProductType.PRODUCT_TYPE_CARD_BACK:
        return CardBackManager.Get().IsCardBackOwned(productData);
      case ProductType.PRODUCT_TYPE_HERO:
        return CollectionManager.Get().IsCardInCollection(GameUtils.TranslateDbIdToCardId(productData), TAG_PREMIUM.NORMAL);
      case ProductType.PRODUCT_TYPE_HIDDEN_LICENSE:
        HiddenLicenseDbfRecord record1 = GameDbf.HiddenLicense.GetRecord(productData);
        if (record1 == null)
          return true;
        AccountLicenseDbfRecord record2 = GameDbf.AccountLicense.GetRecord(record1.AccountLicenseId);
        if (record2 == null)
          return true;
        return AccountLicenseMgr.Get().OwnsAccountLicense(record2.LicenseId);
      default:
        UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.AlreadyOwnsProduct() unknown product {0} productData {1}", (object) product, (object) productData));
        return false;
    }
  }

  private string GetSingleItemProductName(Network.BundleItem item)
  {
    string str = string.Empty;
    switch (item.Product)
    {
      case ProductType.PRODUCT_TYPE_BOOSTER:
        string name = (string) GameDbf.Booster.GetRecord(item.ProductData).Name;
        str = GameStrings.Format("GLUE_STORE_PRODUCT_NAME_PACK", (object) item.Quantity, (object) name);
        break;
      case ProductType.PRODUCT_TYPE_DRAFT:
        str = GameStrings.Get("GLUE_STORE_PRODUCT_NAME_FORGE_TICKET");
        break;
      case ProductType.PRODUCT_TYPE_NAXX:
      case ProductType.PRODUCT_TYPE_BRM:
      case ProductType.PRODUCT_TYPE_LOE:
      case ProductType.PRODUCT_TYPE_WING:
        str = AdventureProgressMgr.Get().GetWingName(item.ProductData);
        break;
      case ProductType.PRODUCT_TYPE_CARD_BACK:
        CardBackDbfRecord record1 = GameDbf.CardBack.GetRecord(item.ProductData);
        if (record1 != null)
        {
          str = (string) record1.Name;
          break;
        }
        break;
      case ProductType.PRODUCT_TYPE_HERO:
        EntityDef entityDef = DefLoader.Get().GetEntityDef(item.ProductData);
        if (entityDef != null)
        {
          str = entityDef.GetName();
          break;
        }
        break;
      case ProductType.PRODUCT_TYPE_TAVERN_BRAWL_TICKET:
        TavernBrawlTicketDbfRecord record2 = GameDbf.TavernBrawlTicket.GetRecord(item.ProductData);
        if (record2 != null)
        {
          str = (string) record2.StoreName;
          break;
        }
        break;
      default:
        UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.GetSingleItemProductName(): don't know how to format name for bundle product {0}", (object) item.Product));
        break;
    }
    return str;
  }

  private string GetMultiItemProductName(List<Network.BundleItem> items)
  {
    HashSet<ProductType> productsInItemList = this.GetProductsInItemList(items);
    if (productsInItemList.Contains(ProductType.PRODUCT_TYPE_NAXX))
      return GameStrings.Format("GLUE_STORE_PRODUCT_NAME_NAXX_WING_BUNDLE", (object) items.Count);
    if (productsInItemList.Contains(ProductType.PRODUCT_TYPE_BRM))
    {
      if (productsInItemList.Contains(ProductType.PRODUCT_TYPE_CARD_BACK))
        return GameStrings.Get("GLUE_STORE_PRODUCT_NAME_BRM_PRESALE_BUNDLE");
      return GameStrings.Format("GLUE_STORE_PRODUCT_NAME_BRM_WING_BUNDLE", (object) items.Count);
    }
    if (productsInItemList.Contains(ProductType.PRODUCT_TYPE_LOE))
      return GameStrings.Format("GLUE_STORE_PRODUCT_NAME_LOE_WING_BUNDLE", (object) items.Count);
    if (productsInItemList.Contains(ProductType.PRODUCT_TYPE_WING))
    {
      string productStringKey = GameUtils.GetAdventureProductStringKey(items[0].ProductData);
      if (productsInItemList.Contains(ProductType.PRODUCT_TYPE_CARD_BACK))
        return GameStrings.Get(string.Format("GLUE_STORE_PRODUCT_NAME_{0}_PRESALE_BUNDLE", (object) productStringKey));
      return GameStrings.Format(string.Format("GLUE_STORE_PRODUCT_NAME_{0}_WING_BUNDLE", (object) productStringKey), (object) items.Count);
    }
    if (productsInItemList.Contains(ProductType.PRODUCT_TYPE_HERO))
    {
      Network.BundleItem bundleItem = items.Find((Predicate<Network.BundleItem>) (obj => obj.Product == ProductType.PRODUCT_TYPE_HERO));
      if (bundleItem != null)
        return this.GetSingleItemProductName(bundleItem);
    }
    else
    {
      if (productsInItemList.Contains(ProductType.PRODUCT_TYPE_HIDDEN_LICENSE) && productsInItemList.Contains(ProductType.PRODUCT_TYPE_RANDOM_CARD))
        return GameStrings.Get("GLUE_STORE_PRODUCT_NAME_FIRST_PURCHASE_BUNDLE");
      if (productsInItemList.Contains(ProductType.PRODUCT_TYPE_BOOSTER) && productsInItemList.Contains(ProductType.PRODUCT_TYPE_CARD_BACK))
      {
        if (items.Find((Predicate<Network.BundleItem>) (obj =>
        {
          if (obj.Product == ProductType.PRODUCT_TYPE_BOOSTER)
            return obj.ProductData == 10;
          return false;
        })) != null)
          return GameStrings.Get("GLUE_STORE_PRODUCT_NAME_TGT_PRESALE_BUNDLE");
        if (items.Find((Predicate<Network.BundleItem>) (obj =>
        {
          if (obj.Product == ProductType.PRODUCT_TYPE_BOOSTER)
            return obj.ProductData == 11;
          return false;
        })) != null)
          return GameStrings.Get("GLUE_STORE_PRODUCT_NAME_OG_PRESALE_BUNDLE");
      }
    }
    string empty = string.Empty;
    using (List<Network.BundleItem>.Enumerator enumerator = items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.BundleItem current = enumerator.Current;
        empty += string.Format("[Product={0},ProductData={1},Quantity={2}],", (object) current.Product, (object) current.ProductData, (object) current.Quantity);
      }
    }
    UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.GetMultiItemProductName(): don't know how to format product name for items '{0}'", (object) empty));
    return string.Empty;
  }

  private bool GetBoosterGoldCostNoGTAPP(int boosterID, out long cost)
  {
    cost = 0L;
    if (!this.m_goldCostBooster.ContainsKey(boosterID) || !this.CanBuyBoosterWithGold(boosterID))
      return false;
    Network.GoldCostBooster goldCostBooster = this.m_goldCostBooster[boosterID];
    if (!goldCostBooster.Cost.HasValue || goldCostBooster.Cost.Value <= 0L)
      return false;
    cost = goldCostBooster.Cost.Value;
    return true;
  }

  private bool GetArenaGoldCostNoGTAPP(out long cost)
  {
    cost = 0L;
    if (!this.m_goldCostArena.HasValue)
      return false;
    cost = this.m_goldCostArena.Value;
    return true;
  }

  private long GetIncreasedRequestDelayTicks(long currentRequestDelayTicks, long minimumDelayTicks)
  {
    if (currentRequestDelayTicks < minimumDelayTicks)
      return minimumDelayTicks;
    return Math.Min(currentRequestDelayTicks * 2L, StoreManager.MAX_REQUEST_DELAY_TICKS);
  }

  private void RequestStatusIfNeeded(long now)
  {
    if (!this.ConfigLoaded || (this.BattlePayAvailable && (this.Status != StoreManager.TransactionStatus.UNKNOWN && this.Status != StoreManager.TransactionStatus.IN_PROGRESS_MONEY && (this.Status != StoreManager.TransactionStatus.IN_PROGRESS_GOLD_GTAPP && this.Status != StoreManager.TransactionStatus.CHALLENGE_SUBMITTED) && StoreManager.TransactionStatus.CHALLENGE_CANCELED != this.Status) || now - this.m_lastStatusRequestTime < this.m_statusRequestDelayTicks))
      return;
    this.m_statusRequestDelayTicks = this.GetIncreasedRequestDelayTicks(this.m_statusRequestDelayTicks, StoreManager.MIN_STATUS_REQUEST_DELAY_TICKS);
    Log.Rachelle.Print(string.Format("StoreManager updated STATUS delay, now waiting {0} seconds between requests", (object) (this.m_statusRequestDelayTicks / 10000000L)));
    this.m_lastStatusRequestTime = now;
    Network.RequestBattlePayStatus();
  }

  private void RequestConfigIfNeeded(long now)
  {
    if (this.ConfigLoaded || now - this.m_lastConfigRequestTime < this.m_configRequestDelayTicks)
      return;
    this.m_configRequestDelayTicks = this.GetIncreasedRequestDelayTicks(this.m_configRequestDelayTicks, StoreManager.MIN_CONFIG_REQUEST_DELAY_TICKS);
    Log.Rachelle.Print(string.Format("StoreManager updated CONFIG delay, now waiting {0} seconds between requests", (object) (this.m_configRequestDelayTicks / 10000000L)));
    this.m_lastConfigRequestTime = now;
    Network.RequestBattlePayConfig();
  }

  private bool AutoCancelPurchaseIfNeeded(long now)
  {
    if ((bool) StoreManager.HAS_THIRD_PARTY_APP_STORE && ApplicationMgr.GetAndroidStore() != AndroidStore.BLIZZARD)
    {
      if (now - this.m_lastCancelRequestTime < this.m_ticksBeforeAutoCancelThirdParty)
        return false;
    }
    else if (now - this.m_lastCancelRequestTime < this.m_ticksBeforeAutoCancel)
      return false;
    return this.AutoCancelPurchaseIfPossible();
  }

  private bool AutoCancelPurchaseIfPossible()
  {
    if (this.m_activeMoneyOrGTAPPTransaction == null || !this.m_activeMoneyOrGTAPPTransaction.Provider.HasValue)
      return false;
    if (this.m_activeMoneyOrGTAPPTransaction.Provider.Value == BattlePayProvider.BP_PROVIDER_BLIZZARD)
    {
      switch (this.Status)
      {
        case StoreManager.TransactionStatus.IN_PROGRESS_MONEY:
        case StoreManager.TransactionStatus.IN_PROGRESS_GOLD_GTAPP:
        case StoreManager.TransactionStatus.WAIT_METHOD_OF_PAYMENT:
        case StoreManager.TransactionStatus.WAIT_CONFIRM:
        case StoreManager.TransactionStatus.WAIT_RISK:
        case StoreManager.TransactionStatus.CHALLENGE_SUBMITTED:
        case StoreManager.TransactionStatus.CHALLENGE_CANCELED:
          Log.Rachelle.Print("StoreManager.AutoCancelPurchaseIfPossible() canceling Blizzard purchase, status={0}", (object) this.Status);
          this.Status = StoreManager.TransactionStatus.AUTO_CANCELING;
          this.m_lastCancelRequestTime = DateTime.Now.Ticks;
          Network.CancelBlizzardPurchase(true, new CancelPurchase.CancelReason?(), (string) null);
          return true;
        default:
          return false;
      }
    }
    else
    {
      if (this.Status != StoreManager.TransactionStatus.WAIT_THIRD_PARTY_RECEIPT || !this.m_shouldAutoCancelThirdPartyTransaction)
        return false;
      Log.Rachelle.Print("StoreManager.AutoCancelPurchaseIfPossible() canceling Third-Party purchase");
      Log.Yim.Print("StoreManager.AutoCancelPurchaseIfPossible() canceling Third-Party purchase");
      this.m_previousStatusBeforeAutoCancel = this.Status;
      this.Status = StoreManager.TransactionStatus.AUTO_CANCELING;
      this.m_lastCancelRequestTime = DateTime.Now.Ticks;
      BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_THIRD_PARTY_PURCHASE_AUTO_CANCEL, 0, this.m_activeMoneyOrGTAPPTransaction != null ? this.m_activeMoneyOrGTAPPTransaction.ID.ToString() : string.Empty);
      Network.CancelThirdPartyPurchase(CancelPurchase.CancelReason.USER_CANCELING_TO_UNBLOCK, (string) null);
      return true;
    }
  }

  private void CancelBlizzardPurchase(CancelPurchase.CancelReason? reason = null, string errorMessage = null)
  {
    Log.Rachelle.Print("StoreManager.CancelBlizzardPurchase() reason=", (object) (!reason.HasValue ? "null" : reason.Value.ToString()));
    this.Status = StoreManager.TransactionStatus.USER_CANCELING;
    this.m_lastCancelRequestTime = DateTime.Now.Ticks;
    Network.CancelBlizzardPurchase(false, reason, errorMessage);
  }

  public void CancelThirdPartyPurchase(CancelPurchase.CancelReason reason)
  {
    Log.Rachelle.Print(string.Format("StoreManager.CancelThirdPartyPurchase(): reason={0}", (object) reason));
    this.Status = StoreManager.TransactionStatus.USER_CANCELING;
    this.m_lastCancelRequestTime = DateTime.Now.Ticks;
    Network.CancelThirdPartyPurchase(reason, (string) null);
  }

  private bool HaveProductsToSell()
  {
    if (this.m_bundles.Count <= 0 && this.m_goldCostBooster.Count <= 0)
      return this.m_goldCostArena.HasValue;
    return true;
  }

  public bool IsBundleRealMoneyOptionAvailableNow(Network.Bundle bundle)
  {
    if (bundle == null || (!bundle.Cost.HasValue || !SpecialEventManager.Get().IsEventActive(bundle.ProductEvent, true)))
      return false;
    return SpecialEventManager.Get().IsEventActive(bundle.RealMoneyProductEvent, true);
  }

  public bool IsBundleGoldOptionAvailableNow(Network.Bundle bundle)
  {
    return bundle != null && (bundle.GoldCost.HasValue && SpecialEventManager.Get().IsEventActive(bundle.ProductEvent, true));
  }

  private bool IsBundleAvailableNow(Network.Bundle bundle, bool requireRealMoneyOption)
  {
    if (bundle == null)
      return false;
    if (this.IsBundleRealMoneyOptionAvailableNow(bundle))
      return true;
    if (requireRealMoneyOption)
      return false;
    return this.IsBundleGoldOptionAvailableNow(bundle);
  }

  private void OnStoreExit(bool authorizationBackButtonPressed, object userData)
  {
    if (this.m_activeMoneyOrGTAPPTransaction != null)
      this.m_activeMoneyOrGTAPPTransaction.ClosedStore = true;
    string challengeID = this.m_storeChallengePrompt.HideChallenge();
    if (challengeID != null)
      this.OnChallengeCancel(challengeID);
    else
      this.AutoCancelPurchaseIfPossible();
    this.DeactivateStoreCover();
    this.HideAllPurchasePopups();
    foreach (StoreManager.StoreHiddenListener storeHiddenListener in this.m_storeHiddenListeners.ToArray())
      storeHiddenListener.Fire();
  }

  private void OnStoreInfo(object userData)
  {
    this.ActivateStoreCover();
    this.m_storeSendToBAM.Show((MoneyOrGTAPPTransaction) null, StoreSendToBAM.BAMReason.PAYMENT_INFO, string.Empty, false);
  }

  public bool CanBuyBundle(Network.Bundle bundleToBuy)
  {
    if (bundleToBuy == null)
    {
      UnityEngine.Debug.LogWarning((object) "Null bundle passed to CanBuyBundle!");
      return false;
    }
    if (AchieveManager.Get() == null || !AchieveManager.Get().IsReady())
      return false;
    if (bundleToBuy.Items.Count < 1)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("Attempting to buy bundle {0}, which does not contain any items!", (object) bundleToBuy.ProductID));
      return false;
    }
    bool productTypeExists = false;
    using (List<Network.Bundle>.Enumerator enumerator = this.GetAvailableBundlesForProduct(bundleToBuy.Items[0].Product, false, false, out productTypeExists, 0, 0).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.ProductID == bundleToBuy.ProductID)
          return true;
      }
    }
    return false;
  }

  private bool CanBuyProduct(ProductType product, int productData)
  {
    if (AchieveManager.Get() == null || !AchieveManager.Get().IsReady())
    {
      UnityEngine.Debug.LogWarning((object) string.Format("Waiting for AchieveManager to check if we already own product {0} of type {1}, cannot purchase!", (object) productData, (object) product));
      return false;
    }
    if (!this.AlreadyOwnsProduct(product, productData))
      return true;
    UnityEngine.Debug.LogWarning((object) string.Format("Already own product {0} of type {1}, cannot purchase again!", (object) productData, (object) product));
    return false;
  }

  private void OnStoreBuyWithMoney(string productID, int quantity, object userData)
  {
    Network.Bundle bundle = this.GetBundle(productID);
    if (!this.CanBuyBundle(bundle))
    {
      UnityEngine.Debug.LogWarning((object) string.Format("Attempting to buy bundle {0}, but it is not available! achieveReady={1}", (object) bundle.ProductID, (object) AchieveManager.Get().IsReady()));
    }
    else
    {
      this.ActivateStoreCover();
      if ((bool) StoreManager.HAS_THIRD_PARTY_APP_STORE && ApplicationMgr.GetAndroidStore() != AndroidStore.BLIZZARD)
      {
        if (this.IsProductFree(bundle))
        {
          UnityEngine.Debug.LogWarning((object) "Attempting to purchase a free bundle!  This should not be possible, and you would be charged by the Third Party Provider price if we allowed it.");
          this.DeactivateStoreCover();
        }
        else
        {
          BattlePayProvider storeProvider = StoreManager.StoreProvider;
          this.SetActiveMoneyOrGTAPPTransaction((long) StoreManager.UNKNOWN_TRANSACTION_ID, productID, new BattlePayProvider?(storeProvider), false, false);
          this.m_storePurchaseAuth.Show(this.m_activeMoneyOrGTAPPTransaction, this.ShouldEnablePurchaseAuthBackButton(this.m_currentStoreType), false);
          this.Status = StoreManager.TransactionStatus.WAIT_THIRD_PARTY_INIT;
          Network.BeginThirdPartyPurchase(storeProvider, productID, quantity);
          Log.Yim.Print("Network.BeginThirdPartyPurchase(" + (object) storeProvider + ", " + productID + ", " + (object) quantity + ")");
        }
      }
      else
      {
        this.SetActiveMoneyOrGTAPPTransaction((long) StoreManager.UNKNOWN_TRANSACTION_ID, productID, new BattlePayProvider?(BattlePayProvider.BP_PROVIDER_BLIZZARD), false, false);
        this.Status = StoreManager.TransactionStatus.WAIT_METHOD_OF_PAYMENT;
        this.m_storePurchaseAuth.Show(this.m_activeMoneyOrGTAPPTransaction, false, false);
        Network.GetPurchaseMethod(productID, quantity, this.m_currency);
      }
    }
  }

  private void OnStoreBuyWithGTAPP(string productID, int quantity, object userData)
  {
    Network.Bundle bundle = this.GetBundle(productID);
    if (!this.CanBuyBundle(bundle))
    {
      UnityEngine.Debug.LogWarning((object) string.Format("Attempting to buy bundle {0}, but it is not available! achieveReady={1}", (object) bundle.ProductID, (object) AchieveManager.Get().IsReady()));
    }
    else
    {
      this.ActivateStoreCover();
      this.SetActiveMoneyOrGTAPPTransaction((long) StoreManager.UNKNOWN_TRANSACTION_ID, productID, new BattlePayProvider?(BattlePayProvider.BP_PROVIDER_BLIZZARD), true, false);
      this.Status = StoreManager.TransactionStatus.WAIT_METHOD_OF_PAYMENT;
      this.m_storePurchaseAuth.Show(this.m_activeMoneyOrGTAPPTransaction, false, false);
      Network.GetPurchaseMethod(productID, quantity, Currency.XSG);
    }
  }

  private void OnStoreBuyWithGoldNoGTAPP(NoGTAPPTransactionData noGTAPPtransactionData, object userData)
  {
    if (noGTAPPtransactionData == null || !this.CanBuyProduct(noGTAPPtransactionData.Product, noGTAPPtransactionData.ProductData))
      return;
    this.ActivateStoreCover();
    this.m_storePurchaseAuth.Show((MoneyOrGTAPPTransaction) null, this.ShouldEnablePurchaseAuthBackButton(this.m_currentStoreType), false);
    this.Status = StoreManager.TransactionStatus.IN_PROGRESS_GOLD_NO_GTAPP;
    Network.PurchaseViaGold(noGTAPPtransactionData.Quantity, noGTAPPtransactionData.Product, noGTAPPtransactionData.ProductData);
  }

  private void OnSummaryConfirm(string productID, int quantity, object userData)
  {
    this.m_storePurchaseAuth.Show(this.m_activeMoneyOrGTAPPTransaction, this.ShouldEnablePurchaseAuthBackButton(this.m_currentStoreType), false);
    if (this.m_challengePurchaseMethod != null)
      this.m_storeChallengePrompt.StartCoroutine(this.m_storeChallengePrompt.Show(this.m_challengePurchaseMethod.ChallengeURL));
    else
      this.ConfirmPurchase();
  }

  private void ConfirmPurchase()
  {
    this.m_lastStatusRequestTime = DateTime.Now.Ticks;
    this.m_statusRequestDelayTicks = StoreManager.EARLY_STATUS_REQUEST_DELAY_TICKS;
    Log.Rachelle.Print(string.Format("StoreManager updating STATUS delay due to summary confirm, now waiting {0} seconds between requests", (object) (this.m_statusRequestDelayTicks / 10000000L)));
    this.Status = !this.m_activeMoneyOrGTAPPTransaction.IsGTAPP ? StoreManager.TransactionStatus.IN_PROGRESS_MONEY : StoreManager.TransactionStatus.IN_PROGRESS_GOLD_GTAPP;
    Network.ConfirmPurchase();
  }

  private void OnSummaryCancel(object userData)
  {
    this.CancelBlizzardPurchase(new CancelPurchase.CancelReason?(), (string) null);
    this.DeactivateStoreCover();
  }

  private void OnThirdPartyPurchaseApproved()
  {
    Log.Yim.Print("OnThirdPartyPurchaseApproved");
    StoreManager.TransactionStatus status = this.Status;
    this.Status = StoreManager.TransactionStatus.WAIT_THIRD_PARTY_RECEIPT;
    if ((bool) StoreManager.HAS_THIRD_PARTY_APP_STORE && ApplicationMgr.GetAndroidStore() != AndroidStore.BLIZZARD)
    {
      if (this.m_activeMoneyOrGTAPPTransaction == null)
        UnityEngine.Debug.LogWarning((object) "StoreManager.OnThirdPartyPurchaseApproved() but m_activeMoneyOrGTAPPTransaction is null");
      else if ((long) StoreManager.UNKNOWN_TRANSACTION_ID == this.m_activeMoneyOrGTAPPTransaction.ID)
        UnityEngine.Debug.LogWarning((object) "StoreManager.OnThirdPartyPurchaseApproved() but m_activeMoneyOrGTAPPTransaction ID is UNKNOWN_TRANSACTION_ID");
      else if (this.m_activeMoneyOrGTAPPTransaction.Provider.HasValue && this.m_activeMoneyOrGTAPPTransaction.Provider.Value != BattlePayProvider.BP_PROVIDER_APPLE && (this.m_activeMoneyOrGTAPPTransaction.Provider.Value != BattlePayProvider.BP_PROVIDER_GOOGLE_PLAY && this.m_activeMoneyOrGTAPPTransaction.Provider.Value != BattlePayProvider.BP_PROVIDER_AMAZON))
        UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.OnThirdPartyPurchaseApproved() active transaction is not an Third Party Provider transaction, so Third Party Provider shouldn't be servicing it (m_activeMoneyOrGTAPPTransaction = {0})", (object) this.m_activeMoneyOrGTAPPTransaction));
      else if (this.GetBundle(this.m_activeMoneyOrGTAPPTransaction.ProductID) == null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.OnThirdPartyPurchaseApproved() but bundle is null (m_activeMoneyOrGTAPPTransaction = {0})", (object) this.m_activeMoneyOrGTAPPTransaction));
      }
      else
      {
        if (this.IsCurrentStoreLoaded())
          this.ActivateStoreCover();
        BattlePayProvider storeProvider = StoreManager.StoreProvider;
        this.SetActiveMoneyOrGTAPPTransaction(this.m_activeMoneyOrGTAPPTransaction.ID, this.m_activeMoneyOrGTAPPTransaction.ProductID, new BattlePayProvider?(storeProvider), false, false);
        if (this.IsCurrentStoreLoaded())
          this.m_storePurchaseAuth.Show(this.m_activeMoneyOrGTAPPTransaction, this.ShouldEnablePurchaseAuthBackButton(this.m_currentStoreType), false);
        if (status == StoreManager.TransactionStatus.WAIT_THIRD_PARTY_INIT)
        {
          string mobileProductId = (string) null;
          BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_THIRD_PARTY_PURCHASE_REQUEST, 0, this.m_activeMoneyOrGTAPPTransaction.ID.ToString() + "|" + mobileProductId);
          StoreMobilePurchase.PurchaseProductById(mobileProductId);
        }
        else
        {
          UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.OnThirdPartyPurchaseApproved() previous Status was {0}, expected {1} (m_activeMoneyOrGTAPPTransaction = {2}", (object) status, (object) StoreManager.TransactionStatus.WAIT_THIRD_PARTY_INIT, (object) this.m_activeMoneyOrGTAPPTransaction));
          Log.Yim.Print("Previous ongoing purchase detected, expecting third party receipt..");
          Log.Yim.Print("m_activeMoneyOrGTAPPTransaction = " + (object) this.m_activeMoneyOrGTAPPTransaction);
          this.m_outOfSessionThirdPartyTransaction = true;
          if (storeProvider != this.m_activeMoneyOrGTAPPTransaction.Provider.Value)
            return;
          this.m_requestedThirdPartyProductId = string.Empty;
          BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_THIRD_PARTY_PURCHASE_RECEIPT_REQUEST, 0, this.m_activeMoneyOrGTAPPTransaction.ID.ToString() + "|" + this.m_requestedThirdPartyProductId);
          StoreMobilePurchase.WaitingOnThirdPartyReceipt(this.m_requestedThirdPartyProductId);
        }
      }
    }
    else
      this.m_outOfSessionThirdPartyTransaction = true;
  }

  private void OnSummaryInfo(object userData)
  {
    this.ActivateStoreCover();
    this.AutoCancelPurchaseIfPossible();
    this.m_storeSendToBAM.Show((MoneyOrGTAPPTransaction) null, StoreSendToBAM.BAMReason.EULA_AND_TOS, string.Empty, false);
  }

  private void OnSummaryPaymentAndTOS(object userData)
  {
    this.AutoCancelPurchaseIfPossible();
    this.m_storeLegalBAMLinks.Show();
  }

  private void OnChallengeComplete(string challengeID, bool isSuccess, CancelPurchase.CancelReason? reason, string internalErrorInfo)
  {
    if (!isSuccess)
    {
      this.OnChallengeCancel_Internal(challengeID, reason, internalErrorInfo);
    }
    else
    {
      this.m_storePurchaseAuth.Show(this.m_activeMoneyOrGTAPPTransaction, this.ShouldEnablePurchaseAuthBackButton(this.m_currentStoreType), false);
      this.m_lastStatusRequestTime = DateTime.Now.Ticks;
      this.m_statusRequestDelayTicks = StoreManager.EARLY_STATUS_REQUEST_DELAY_TICKS;
      Log.Rachelle.Print(string.Format("StoreManager updating STATUS delay due to challenge answer submit, now waiting {0} seconds between requests", (object) (this.m_statusRequestDelayTicks / 10000000L)));
      this.Status = StoreManager.TransactionStatus.CHALLENGE_SUBMITTED;
      this.ConfirmPurchase();
    }
  }

  private void OnChallengeCancel(string challengeID)
  {
    this.OnChallengeCancel_Internal(challengeID, new CancelPurchase.CancelReason?(), (string) null);
  }

  private void OnChallengeCancel_Internal(string challengeID, CancelPurchase.CancelReason? reason, string errorMessage)
  {
    UnityEngine.Debug.LogFormat("Canceling purchase from challengeId={0} reason={1} msg={2}", (object) challengeID, (object) (!reason.HasValue ? "null" : reason.Value.ToString()), (object) errorMessage);
    this.Status = StoreManager.TransactionStatus.CHALLENGE_CANCELED;
    this.m_lastStatusRequestTime = DateTime.Now.Ticks;
    this.m_statusRequestDelayTicks = StoreManager.CHALLENGE_CANCEL_STATUS_REQUEST_DELAY_TICKS;
    this.CancelBlizzardPurchase(reason, errorMessage);
    this.DeactivateStoreCover();
    this.HideAllPurchasePopups();
  }

  private void OnSendToBAMOkay(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, StoreSendToBAM.BAMReason reason)
  {
    if (moneyOrGTAPPTransaction != null)
      this.ConfirmActiveMoneyTransaction(moneyOrGTAPPTransaction.ID);
    if (reason == StoreSendToBAM.BAMReason.PAYMENT_INFO)
      this.OnDoneWithBAM();
    else
      this.m_storeDoneWithBAM.Show();
  }

  private void OnSendToBAMCancel(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction)
  {
    if (moneyOrGTAPPTransaction != null)
      this.ConfirmActiveMoneyTransaction(moneyOrGTAPPTransaction.ID);
    this.DeactivateStoreCover();
  }

  private void OnSendToBAMLegal(StoreLegalBAMLinks.BAMReason reason)
  {
    this.DeactivateStoreCover();
  }

  private void OnSendToBAMLegalCancel()
  {
    this.DeactivateStoreCover();
  }

  private void OnDoneWithBAM()
  {
    this.DeactivateStoreCover();
  }

  private void OnAchievesUpdated(List<Achievement> updatedAchives, List<Achievement> completedAchivees, object userData)
  {
    this.m_completedAchieves = AchieveManager.Get().GetNewCompletedAchieves();
    this.ShowCompletedAchieve();
    StoreManager.StoreAchievesData storeAchievesData = userData as StoreManager.StoreAchievesData;
    foreach (StoreManager.StoreAchievesListener achievesListener in this.m_storeAchievesListeners.ToArray())
      achievesListener.Fire(storeAchievesData.Bundle, storeAchievesData.MethodOfPayment);
  }

  private void OnLicenseAddedAchievesUpdated(List<Achievement> activeLicenseAddedAchieves, object userData)
  {
    if (this.Status != StoreManager.TransactionStatus.WAIT_ZERO_COST_LICENSE || activeLicenseAddedAchieves.Count > 0)
      return;
    Log.Rachelle.Print("StoreManager.OnLicenseAddedAchievesUpdated(): done waiting for licenses!");
    if (this.IsCurrentStoreLoaded())
      this.m_storePurchaseAuth.CompletePurchaseSuccess((MoneyOrGTAPPTransaction) null);
    this.Status = StoreManager.TransactionStatus.READY;
  }

  private void ShowCompletedAchieve()
  {
    bool enabled = this.m_completedAchieves.Count == 0;
    if (this.m_currentStoreType == StoreType.GENERAL_STORE)
    {
      GeneralStore currentStore = (GeneralStore) this.GetCurrentStore();
      if ((UnityEngine.Object) currentStore != (UnityEngine.Object) null)
        currentStore.EnableClickCatcher(enabled);
    }
    if (enabled)
      return;
    Achievement completedAchieve = this.m_completedAchieves[0];
    this.m_completedAchieves.RemoveAt(0);
    QuestToast.ShowQuestToast(UserAttentionBlocker.NONE, (QuestToast.DelOnCloseQuestToast) (userData => this.ShowCompletedAchieve()), true, completedAchieve, false);
  }

  private void OnPurchaseResultAcknowledged(bool success, MoneyOrGTAPPTransaction moneyOrGTAPPTransaction)
  {
    PaymentMethod paymentMethod;
    Network.Bundle bundle;
    if (moneyOrGTAPPTransaction == null)
    {
      paymentMethod = PaymentMethod.GOLD_NO_GTAPP;
      bundle = (Network.Bundle) null;
    }
    else
    {
      if (moneyOrGTAPPTransaction.ID > 0L)
        this.m_transactionIDsConclusivelyHandled.Add(moneyOrGTAPPTransaction.ID);
      paymentMethod = !moneyOrGTAPPTransaction.IsGTAPP ? PaymentMethod.MONEY : PaymentMethod.GOLD_GTAPP;
      bundle = this.GetBundle(moneyOrGTAPPTransaction.ProductID);
    }
    if (paymentMethod != PaymentMethod.GOLD_NO_GTAPP)
      this.ConfirmActiveMoneyTransaction(moneyOrGTAPPTransaction.ID);
    if (success)
    {
      foreach (StoreManager.SuccessfulPurchaseAckListener purchaseAckListener in this.m_successfulPurchaseAckListeners.ToArray())
        purchaseAckListener.Fire(bundle, paymentMethod);
    }
    this.DeactivateStoreCover();
    Store currentStore = this.GetCurrentStore();
    if (this.m_currentStoreType == StoreType.ADVENTURE_STORE)
      currentStore.Close();
    if (this.BattlePayAvailable || this.m_currentStoreType != StoreType.GENERAL_STORE)
      return;
    currentStore.Close();
  }

  private void OnAuthExit()
  {
    foreach (StoreManager.AuthorizationExitListener authorizationExitListener in this.m_authExitListeners.ToArray())
      authorizationExitListener.Fire();
  }

  private void ActivateStoreCover()
  {
    Store currentStore = this.GetCurrentStore();
    if (!((UnityEngine.Object) currentStore != (UnityEngine.Object) null))
      return;
    currentStore.ActivateCover(true);
  }

  private void DeactivateStoreCover()
  {
    Store currentStore = this.GetCurrentStore();
    if (!((UnityEngine.Object) currentStore != (UnityEngine.Object) null))
      return;
    currentStore.ActivateCover(false);
  }

  private void HandlePurchaseSuccess(StoreManager.PurchaseErrorSource? source, MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, string thirdPartyID)
  {
    this.NotifyMobileGamePurchaseResponse(thirdPartyID, true);
    this.Status = StoreManager.TransactionStatus.READY;
    PaymentMethod paymentMethod;
    Network.Bundle bundle;
    if (moneyOrGTAPPTransaction == null)
    {
      paymentMethod = PaymentMethod.GOLD_NO_GTAPP;
      bundle = (Network.Bundle) null;
    }
    else
    {
      paymentMethod = !moneyOrGTAPPTransaction.IsGTAPP ? PaymentMethod.MONEY : PaymentMethod.GOLD_GTAPP;
      bundle = this.GetBundle(moneyOrGTAPPTransaction.ProductID);
    }
    bool flag = false;
    if (bundle != null)
    {
      using (List<Network.BundleItem>.Enumerator enumerator = bundle.Items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          switch (enumerator.Current.Product)
          {
            case ProductType.PRODUCT_TYPE_HERO:
              flag = true;
              continue;
            default:
              continue;
          }
        }
      }
    }
    if (flag)
      NetCache.Get().RefreshNetObject<NetCache.NetCacheFavoriteHeroes>();
    foreach (StoreManager.SuccessfulPurchaseListener purchaseListener in this.m_successfulPurchaseListeners.ToArray())
      purchaseListener.Fire(bundle, paymentMethod);
    if (!this.IsCurrentStoreLoaded())
      return;
    if (source.HasValue && source.Value == StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE)
    {
      this.ActivateStoreCover();
      this.m_storePurchaseAuth.ShowPreviousPurchaseSuccess(moneyOrGTAPPTransaction, this.ShouldEnablePurchaseAuthBackButton(this.m_currentStoreType));
    }
    else
    {
      Store currentStore = this.GetCurrentStore();
      if (paymentMethod == PaymentMethod.GOLD_GTAPP || paymentMethod == PaymentMethod.GOLD_NO_GTAPP)
        currentStore.OnGoldSpent();
      else
        currentStore.OnMoneySpent();
      this.m_storePurchaseAuth.CompletePurchaseSuccess(moneyOrGTAPPTransaction);
    }
  }

  private void HandleFailedRiskError(StoreManager.PurchaseErrorSource source)
  {
    bool flag = StoreManager.TransactionStatus.CHALLENGE_CANCELED == this.Status;
    this.Status = StoreManager.TransactionStatus.READY;
    if (flag)
    {
      Log.Rachelle.Print("HandleFailedRiskError for canceled transaction");
      if (this.m_activeMoneyOrGTAPPTransaction != null)
        this.ConfirmActiveMoneyTransaction(this.m_activeMoneyOrGTAPPTransaction.ID);
      this.DeactivateStoreCover();
    }
    else
    {
      if (!this.IsCurrentStoreLoaded() || !this.GetCurrentStore().IsShown())
        return;
      this.m_storePurchaseAuth.Hide();
      this.ActivateStoreCover();
      this.m_storeSendToBAM.Show(this.m_activeMoneyOrGTAPPTransaction, StoreSendToBAM.BAMReason.NEED_PASSWORD_RESET, string.Empty, source == StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE);
    }
  }

  private void HandleSendToBAMError(StoreManager.PurchaseErrorSource source, StoreSendToBAM.BAMReason reason, string errorCode)
  {
    this.Status = StoreManager.TransactionStatus.READY;
    if (!this.IsCurrentStoreLoaded() || !this.GetCurrentStore().IsShown())
      return;
    this.m_storePurchaseAuth.Hide();
    this.ActivateStoreCover();
    this.m_storeSendToBAM.Show(this.m_activeMoneyOrGTAPPTransaction, reason, errorCode, source == StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE);
  }

  public void NotifyMobileGamePurchaseResponse(string thirdPartyID, bool isSuccess)
  {
    Log.Yim.Print("NotifyMobileGamePurchaseResponse(" + thirdPartyID + ", " + (!isSuccess ? "false" : "true") + ")");
    if (!(bool) StoreManager.HAS_THIRD_PARTY_APP_STORE || ApplicationMgr.GetAndroidStore() == AndroidStore.BLIZZARD || string.IsNullOrEmpty(thirdPartyID))
      return;
    BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_THIRD_PARTY_PURCHASE_RECEIPT_SUBMITTED_RESPONSE, 0, (this.m_activeMoneyOrGTAPPTransaction != null ? this.m_activeMoneyOrGTAPPTransaction.ID.ToString() : string.Empty) + "|" + thirdPartyID + "|" + (!isSuccess ? "false" : "true"));
    StoreMobilePurchase.GamePurchaseStatusResponse(thirdPartyID, isSuccess);
  }

  private void CompletePurchaseFailure(StoreManager.PurchaseErrorSource source, MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, string failDetails, string thirdPartyID, bool removeThirdPartyReceipt, Network.PurchaseErrorInfo.ErrorType error)
  {
    if (removeThirdPartyReceipt)
      this.NotifyMobileGamePurchaseResponse(thirdPartyID, false);
    if (!this.IsCurrentStoreLoaded())
      return;
    if (source == StoreManager.PurchaseErrorSource.FROM_PURCHASE_METHOD_RESPONSE)
    {
      this.ActivateStoreCover();
      this.m_storePurchaseAuth.ShowPurchaseMethodFailure(moneyOrGTAPPTransaction, failDetails, this.ShouldEnablePurchaseAuthBackButton(this.m_currentStoreType), error);
    }
    else if (source == StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE)
    {
      this.ActivateStoreCover();
      this.m_storePurchaseAuth.ShowPreviousPurchaseFailure(moneyOrGTAPPTransaction, failDetails, this.ShouldEnablePurchaseAuthBackButton(this.m_currentStoreType), error);
    }
    else
    {
      if (this.m_storePurchaseAuth.CompletePurchaseFailure(moneyOrGTAPPTransaction, failDetails, error))
        return;
      UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.CompletePurchaseFailure(): purchased failed ({0}) but the store authorization window has been closed.", (object) failDetails));
      this.DeactivateStoreCover();
    }
  }

  private void HandlePurchaseError(StoreManager.PurchaseErrorSource source, Network.PurchaseErrorInfo.ErrorType purchaseErrorType, string purchaseErrorCode, string thirdPartyID, bool isGTAPP)
  {
    if (this.IsConclusiveState(purchaseErrorType) && this.m_activeMoneyOrGTAPPTransaction != null && this.m_transactionIDsConclusivelyHandled.Contains(this.m_activeMoneyOrGTAPPTransaction.ID))
    {
      Log.Rachelle.Print("HandlePurchaseError already handled purchase error for conclusive state on transaction (Transaction: {0}, current purchaseErrorType = {1})", new object[2]
      {
        (object) this.m_activeMoneyOrGTAPPTransaction,
        (object) purchaseErrorType
      });
    }
    else
    {
      Log.Yim.Print(string.Format("HandlePurchaseError source={0} purchaseErrorType={1} purchaseErrorCode={2} thirdPartyID={3}", (object) source, (object) purchaseErrorType, (object) purchaseErrorCode, (object) thirdPartyID));
      string failDetails1 = string.Empty;
      Network.PurchaseErrorInfo.ErrorType errorType = purchaseErrorType;
      switch (errorType + 1)
      {
        case Network.PurchaseErrorInfo.ErrorType.SUCCESS:
          UnityEngine.Debug.LogWarning((object) "StoreManager.HandlePurchaseError: purchase error is UNKNOWN, taking no action on this purchase");
          return;
        case Network.PurchaseErrorInfo.ErrorType.STILL_IN_PROGRESS:
          if (source == StoreManager.PurchaseErrorSource.FROM_PURCHASE_METHOD_RESPONSE)
          {
            UnityEngine.Debug.LogWarning((object) "StoreManager.HandlePurchaseError: received SUCCESS from payment method purchase error.");
            return;
          }
          if (isGTAPP)
            NetCache.Get().RefreshNetObject<NetCache.NetCacheGoldBalance>();
          this.HandlePurchaseSuccess(new StoreManager.PurchaseErrorSource?(source), this.m_activeMoneyOrGTAPPTransaction, thirdPartyID);
          return;
        case Network.PurchaseErrorInfo.ErrorType.INVALID_BNET:
          if (source == StoreManager.PurchaseErrorSource.FROM_PURCHASE_METHOD_RESPONSE)
          {
            UnityEngine.Debug.LogWarning((object) "StoreManager.HandlePurchaseError: received STILL_IN_PROGRESS from payment method purchase error.");
            return;
          }
          if (source == StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE)
            return;
          this.Status = !isGTAPP ? StoreManager.TransactionStatus.IN_PROGRESS_MONEY : StoreManager.TransactionStatus.IN_PROGRESS_GOLD_GTAPP;
          return;
        case Network.PurchaseErrorInfo.ErrorType.SERVICE_NA:
          failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_BNET_ID");
          break;
        case Network.PurchaseErrorInfo.ErrorType.PURCHASE_IN_PROGRESS:
          if (source != StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE)
          {
            if (this.Status != StoreManager.TransactionStatus.UNKNOWN)
              this.BattlePayAvailable = false;
            this.Status = StoreManager.TransactionStatus.UNKNOWN;
          }
          string failDetails2 = GameStrings.Get("GLUE_STORE_FAIL_NO_BATTLEPAY");
          this.CompletePurchaseFailure(source, this.m_activeMoneyOrGTAPPTransaction, failDetails2, thirdPartyID, this.IsSafeToRemoveMobileReceipt(purchaseErrorType), purchaseErrorType);
          return;
        case Network.PurchaseErrorInfo.ErrorType.DATABASE:
          if (source != StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE)
            this.Status = !isGTAPP ? StoreManager.TransactionStatus.IN_PROGRESS_MONEY : StoreManager.TransactionStatus.IN_PROGRESS_GOLD_GTAPP;
          string failDetails3 = GameStrings.Get("GLUE_STORE_FAIL_IN_PROGRESS");
          this.CompletePurchaseFailure(source, this.m_activeMoneyOrGTAPPTransaction, failDetails3, thirdPartyID, this.IsSafeToRemoveMobileReceipt(purchaseErrorType), purchaseErrorType);
          return;
        case Network.PurchaseErrorInfo.ErrorType.INVALID_QUANTITY:
          failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_DATABASE");
          break;
        case Network.PurchaseErrorInfo.ErrorType.DUPLICATE_LICENSE:
          failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_QUANTITY");
          break;
        case Network.PurchaseErrorInfo.ErrorType.REQUEST_NOT_SENT:
          failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_LICENSE");
          break;
        case Network.PurchaseErrorInfo.ErrorType.NO_ACTIVE_BPAY:
          if (source != StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE && this.Status != StoreManager.TransactionStatus.UNKNOWN)
            this.BattlePayAvailable = false;
          failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_NO_BATTLEPAY");
          break;
        case Network.PurchaseErrorInfo.ErrorType.FAILED_RISK:
          failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_NO_ACTIVE_BPAY");
          break;
        case Network.PurchaseErrorInfo.ErrorType.CANCELED:
          this.HandleFailedRiskError(source);
          return;
        case Network.PurchaseErrorInfo.ErrorType.WAIT_MOP:
          if (source == StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE)
            return;
          this.Status = StoreManager.TransactionStatus.READY;
          return;
        case Network.PurchaseErrorInfo.ErrorType.WAIT_CONFIRM:
          Log.Rachelle.Print("StoreManager.HandlePurchaseError: Status is WAIT_MOP.. this probably shouldn't be happening.");
          if (source == StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE)
            return;
          if (this.Status == StoreManager.TransactionStatus.UNKNOWN)
          {
            Log.Rachelle.Print(string.Format("StoreManager.HandlePurchaseError: Status is WAIT_MOP, previous Status was UNKNOWN, source = {0}", (object) source));
            return;
          }
          this.Status = StoreManager.TransactionStatus.WAIT_METHOD_OF_PAYMENT;
          return;
        case Network.PurchaseErrorInfo.ErrorType.WAIT_RISK:
          if (source == StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE || this.Status != StoreManager.TransactionStatus.UNKNOWN)
            return;
          Log.Rachelle.Print(string.Format("StoreManager.HandlePurchaseError: Status is WAIT_CONFIRM, previous Status was UNKNOWN, source = {0}. Going to try to cancel the purchase.", (object) source));
          this.CancelBlizzardPurchase(new CancelPurchase.CancelReason?(), (string) null);
          return;
        case Network.PurchaseErrorInfo.ErrorType.PRODUCT_NA:
          if (source == StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE)
            return;
          Log.Rachelle.Print("StoreManager.HandlePurchaseError: Waiting for client to respond to Risk challenge");
          if (this.Status == StoreManager.TransactionStatus.UNKNOWN)
          {
            Log.Rachelle.Print(string.Format("StoreManager.HandlePurchaseError: Status is WAIT_RISK, previous Status was UNKNOWN, source = {0}", (object) source));
            return;
          }
          if (this.Status == StoreManager.TransactionStatus.CHALLENGE_SUBMITTED || this.Status == StoreManager.TransactionStatus.CHALLENGE_CANCELED)
          {
            Log.Rachelle.Print(string.Format("StoreManager.HandlePurchaseError: Status = {0}; ignoring WAIT_RISK purchase error info", (object) this.Status));
            return;
          }
          this.Status = StoreManager.TransactionStatus.WAIT_RISK;
          return;
        case Network.PurchaseErrorInfo.ErrorType.RISK_TIMEOUT:
          failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_PRODUCT_NA");
          break;
        case Network.PurchaseErrorInfo.ErrorType.PRODUCT_ALREADY_OWNED:
          failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_CHALLENGE_TIMEOUT");
          break;
        case Network.PurchaseErrorInfo.ErrorType.WAIT_THIRD_PARTY_RECEIPT:
          failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_PRODUCT_ALREADY_OWNED");
          break;
        case Network.PurchaseErrorInfo.ErrorType.PRODUCT_EVENT_HAS_ENDED:
          this.OnThirdPartyPurchaseApproved();
          return;
        case Network.PurchaseErrorInfo.ErrorType.PURCHASE_IN_PROGRESS | Network.PurchaseErrorInfo.ErrorType.RISK_TIMEOUT:
          failDetails1 = this.m_activeMoneyOrGTAPPTransaction == null || !this.IsProductPrePurchase(this.GetBundle(this.m_activeMoneyOrGTAPPTransaction.ProductID)) ? GameStrings.Get("GLUE_STORE_PRODUCT_EVENT_HAS_ENDED") : GameStrings.Get("GLUE_STORE_PRE_PURCHASE_HAS_ENDED");
          break;
        default:
          switch (errorType)
          {
            case Network.PurchaseErrorInfo.ErrorType.BP_GENERIC_FAIL:
            case Network.PurchaseErrorInfo.ErrorType.BP_RISK_ERROR:
            case Network.PurchaseErrorInfo.ErrorType.BP_PAYMENT_AUTH:
            case Network.PurchaseErrorInfo.ErrorType.BP_PROVIDER_DENIED:
            case Network.PurchaseErrorInfo.ErrorType.E_BP_GENERIC_FAIL_RETRY_CONTACT_CS_IF_PERSISTS:
              if (!isGTAPP)
              {
                StoreSendToBAM.BAMReason reason = StoreSendToBAM.BAMReason.GENERIC_PAYMENT_FAIL;
                if (purchaseErrorType == Network.PurchaseErrorInfo.ErrorType.E_BP_GENERIC_FAIL_RETRY_CONTACT_CS_IF_PERSISTS)
                  reason = StoreSendToBAM.BAMReason.GENERIC_PURCHASE_FAIL_RETRY_CONTACT_CS_IF_PERSISTS;
                this.HandleSendToBAMError(source, reason, purchaseErrorCode);
                if (!(bool) StoreManager.HAS_THIRD_PARTY_APP_STORE || ApplicationMgr.GetAndroidStore() == AndroidStore.BLIZZARD)
                  return;
                this.CompletePurchaseFailure(source, this.m_activeMoneyOrGTAPPTransaction, failDetails1, thirdPartyID, this.IsSafeToRemoveMobileReceipt(purchaseErrorType), purchaseErrorType);
                return;
              }
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_GOLD_GENERIC");
              break;
            case Network.PurchaseErrorInfo.ErrorType.BP_INVALID_CC_EXPIRY:
              if (!isGTAPP)
              {
                this.HandleSendToBAMError(source, StoreSendToBAM.BAMReason.CREDIT_CARD_EXPIRED, string.Empty);
                return;
              }
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_GOLD_GENERIC");
              break;
            case Network.PurchaseErrorInfo.ErrorType.BP_NO_VALID_PAYMENT:
              if (source == StoreManager.PurchaseErrorSource.FROM_PURCHASE_METHOD_RESPONSE)
              {
                UnityEngine.Debug.LogWarning((object) "StoreManager.HandlePurchaseError: received BP_NO_VALID_PAYMENT from payment method purchase error.");
                break;
              }
              if (!isGTAPP)
              {
                this.HandleSendToBAMError(source, StoreSendToBAM.BAMReason.NO_VALID_PAYMENT_METHOD, string.Empty);
                return;
              }
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_GOLD_GENERIC");
              break;
            case Network.PurchaseErrorInfo.ErrorType.BP_PURCHASE_BAN:
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_PURCHASE_BAN");
              break;
            case Network.PurchaseErrorInfo.ErrorType.BP_SPENDING_LIMIT:
              failDetails1 = isGTAPP ? GameStrings.Get("GLUE_STORE_FAIL_GOLD_GENERIC") : GameStrings.Get("GLUE_STORE_FAIL_SPENDING_LIMIT");
              break;
            case Network.PurchaseErrorInfo.ErrorType.BP_PARENTAL_CONTROL:
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_PARENTAL_CONTROL");
              break;
            case Network.PurchaseErrorInfo.ErrorType.BP_THROTTLED:
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_THROTTLED");
              break;
            case Network.PurchaseErrorInfo.ErrorType.BP_THIRD_PARTY_BAD_RECEIPT:
            case Network.PurchaseErrorInfo.ErrorType.BP_THIRD_PARTY_RECEIPT_USED:
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_THIRD_PARTY_BAD_RECEIPT");
              break;
            case Network.PurchaseErrorInfo.ErrorType.BP_PRODUCT_UNIQUENESS_VIOLATED:
              this.HandleSendToBAMError(source, StoreSendToBAM.BAMReason.PRODUCT_UNIQUENESS_VIOLATED, string.Empty);
              return;
            case Network.PurchaseErrorInfo.ErrorType.BP_REGION_IS_DOWN:
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_REGION_IS_DOWN");
              break;
            case Network.PurchaseErrorInfo.ErrorType.E_BP_CHALLENGE_ID_FAILED_VERIFICATION:
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_CHALLENGE_ID_FAILED_VERIFICATION");
              break;
            default:
              failDetails1 = GameStrings.Get("GLUE_STORE_FAIL_GENERAL");
              break;
          }
      }
      if (source != StoreManager.PurchaseErrorSource.FROM_PREVIOUS_PURCHASE)
      {
        bool flag = StoreManager.TransactionStatus.UNKNOWN == this.Status;
        this.Status = StoreManager.TransactionStatus.READY;
        if (flag && this.IsSafeToRemoveMobileReceipt(purchaseErrorType))
        {
          this.NotifyMobileGamePurchaseResponse(thirdPartyID, false);
          return;
        }
      }
      this.CompletePurchaseFailure(source, this.m_activeMoneyOrGTAPPTransaction, failDetails1, thirdPartyID, this.IsSafeToRemoveMobileReceipt(purchaseErrorType), purchaseErrorType);
    }
  }

  private void SetActiveMoneyOrGTAPPTransaction(long id, string productID, BattlePayProvider? provider, bool isGTAPP, bool tryToResolvePreviousTransactionNotices)
  {
    MoneyOrGTAPPTransaction gtappTransaction = new MoneyOrGTAPPTransaction(id, productID, provider, isGTAPP);
    bool flag = true;
    if (this.m_activeMoneyOrGTAPPTransaction != null)
    {
      if (gtappTransaction.Equals((object) this.m_activeMoneyOrGTAPPTransaction))
        flag = !this.m_activeMoneyOrGTAPPTransaction.Provider.HasValue && provider.HasValue;
      else if ((long) StoreManager.UNKNOWN_TRANSACTION_ID != this.m_activeMoneyOrGTAPPTransaction.ID)
        UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.SetActiveMoneyOrGTAPPTransaction(id={0},productID='{1}',isGTAPP={2},provider={3}) does not match active money or GTAPP transaction '{4}'", (object) id, (object) productID, (object) isGTAPP, (object) (!provider.HasValue ? "UNKNOWN" : provider.Value.ToString()), (object) this.m_activeMoneyOrGTAPPTransaction));
    }
    if (flag)
    {
      Log.Rachelle.Print(string.Format("SetActiveMoneyOrGTAPPTransaction() {0}", (object) gtappTransaction));
      Log.Yim.Print(string.Format("SetActiveMoneyOrGTAPPTransaction() {0}", (object) gtappTransaction));
      this.m_activeMoneyOrGTAPPTransaction = gtappTransaction;
    }
    if (this.m_firstMoneyOrGTAPPTransactionSet)
      return;
    this.m_firstMoneyOrGTAPPTransactionSet = true;
    if (!tryToResolvePreviousTransactionNotices)
      return;
    this.ResolveFirstMoneyOrGTAPPTransactionIfPossible();
  }

  private void ResolveFirstMoneyOrGTAPPTransactionIfPossible()
  {
    if (!this.m_firstMoneyOrGTAPPTransactionSet || !this.FirstNoticesProcessed || (this.m_activeMoneyOrGTAPPTransaction == null || this.m_outstandingPurchaseNotices.Find((Predicate<NetCache.ProfileNoticePurchase>) (obj => obj.OriginData == this.m_activeMoneyOrGTAPPTransaction.ID)) != null))
      return;
    Log.Rachelle.Print(string.Format("StoreManager.ResolveFirstMoneyTransactionIfPossible(): no outstanding notices for transaction {0}; setting m_activeMoneyOrGTAPPTransaction = null", (object) this.m_activeMoneyOrGTAPPTransaction));
    this.m_activeMoneyOrGTAPPTransaction = (MoneyOrGTAPPTransaction) null;
  }

  private void ConfirmActiveMoneyTransaction(long id)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    StoreManager.\u003CConfirmActiveMoneyTransaction\u003Ec__AnonStorey3FB transactionCAnonStorey3Fb = new StoreManager.\u003CConfirmActiveMoneyTransaction\u003Ec__AnonStorey3FB();
    // ISSUE: reference to a compiler-generated field
    transactionCAnonStorey3Fb.id = id;
    // ISSUE: reference to a compiler-generated field
    if (this.m_activeMoneyOrGTAPPTransaction == null || this.m_activeMoneyOrGTAPPTransaction.ID != (long) StoreManager.UNKNOWN_TRANSACTION_ID && this.m_activeMoneyOrGTAPPTransaction.ID != transactionCAnonStorey3Fb.id)
    {
      // ISSUE: reference to a compiler-generated field
      UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.ConfirmActiveMoneyTransaction(id={0}) does not match active money transaction '{1}'", (object) transactionCAnonStorey3Fb.id, (object) this.m_activeMoneyOrGTAPPTransaction));
    }
    // ISSUE: reference to a compiler-generated field
    Log.Rachelle.Print(string.Format("ConfirmActiveMoneyTransaction() {0}", (object) transactionCAnonStorey3Fb.id));
    // ISSUE: reference to a compiler-generated method
    Predicate<NetCache.ProfileNoticePurchase> match = new Predicate<NetCache.ProfileNoticePurchase>(transactionCAnonStorey3Fb.\u003C\u003Em__1F1);
    List<NetCache.ProfileNoticePurchase> all = this.m_outstandingPurchaseNotices.FindAll(match);
    this.m_outstandingPurchaseNotices.RemoveAll(match);
    using (List<NetCache.ProfileNoticePurchase>.Enumerator enumerator = all.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Network.AckNotice(enumerator.Current.NoticeID);
    }
    // ISSUE: reference to a compiler-generated field
    this.m_confirmedTransactionIDs.Add(transactionCAnonStorey3Fb.id);
    this.m_activeMoneyOrGTAPPTransaction = (MoneyOrGTAPPTransaction) null;
  }

  private void OnNewNotices(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList)
  {
    List<long> longList = new List<long>();
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = newNotices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.ProfileNotice current = enumerator.Current;
        if (current.Type == NetCache.ProfileNotice.NoticeType.PURCHASE)
        {
          if (current.Origin == NetCache.ProfileNotice.NoticeOrigin.PURCHASE_CANCELED)
          {
            Log.Rachelle.Print(string.Format("StoreManager.OnNewNotices() ack'ing purchase canceled notice for bpay ID {0}", (object) current.OriginData));
            longList.Add(current.NoticeID);
          }
          else if (this.m_confirmedTransactionIDs.Contains(current.OriginData))
          {
            Log.Rachelle.Print(string.Format("StoreManager.OnNewNotices() ack'ing purchase notice for already confirmed bpay ID {0}", (object) current.OriginData));
            longList.Add(current.NoticeID);
          }
          else
          {
            NetCache.ProfileNoticePurchase profileNoticePurchase = current as NetCache.ProfileNoticePurchase;
            Log.Rachelle.Print(string.Format("StoreManager.OnNewNotices() adding outstanding purchase notice for bpay ID {0}", (object) current.OriginData));
            this.m_outstandingPurchaseNotices.Add(profileNoticePurchase);
          }
        }
      }
    }
    using (List<long>.Enumerator enumerator = longList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Network.AckNotice(enumerator.Current);
    }
    if (this.FirstNoticesProcessed)
      return;
    this.FirstNoticesProcessed = true;
    if (this.Status != StoreManager.TransactionStatus.READY)
      return;
    this.ResolveFirstMoneyOrGTAPPTransactionIfPossible();
  }

  private void RemoveThirdPartyReceipt(string transactionID)
  {
    if (!(bool) StoreManager.HAS_THIRD_PARTY_APP_STORE || ApplicationMgr.GetAndroidStore() == AndroidStore.BLIZZARD || string.IsNullOrEmpty(transactionID))
      return;
    StoreMobilePurchase.FinishTransactionForId(transactionID);
  }

  private void OnGoldBalanceChanged(NetCache.NetCacheGoldBalance balance)
  {
    Store currentStore = this.GetCurrentStore();
    if ((UnityEngine.Object) currentStore == (UnityEngine.Object) null || !currentStore.IsShown())
      return;
    currentStore.OnGoldBalanceChanged(balance);
  }

  private void OnNetCacheFeaturesReady()
  {
    this.FeaturesReady = true;
  }

  private void OnPurchaseCanceledResponse()
  {
    Network.PurchaseCanceledResponse canceledResponse = Network.GetPurchaseCanceledResponse();
    switch (canceledResponse.Result)
    {
      case Network.PurchaseCanceledResponse.CancelResult.SUCCESS:
        Log.Rachelle.Print("StoreManager.OnPurchaseCanceledResponse(): purchase successfully canceled.");
        BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_THIRD_PARTY_PURCHASE_CANCEL_RESPONSE, 0, this.m_activeMoneyOrGTAPPTransaction != null ? this.m_activeMoneyOrGTAPPTransaction.ID.ToString() : string.Empty);
        this.m_lastStatusRequestTime = DateTime.Now.Ticks;
        this.ConfirmActiveMoneyTransaction(canceledResponse.TransactionID);
        this.Status = StoreManager.TransactionStatus.READY;
        this.m_shouldAutoCancelThirdPartyTransaction = false;
        this.m_requestedThirdPartyProductId = (string) null;
        this.m_previousStatusBeforeAutoCancel = StoreManager.TransactionStatus.UNKNOWN;
        this.m_outOfSessionThirdPartyTransaction = false;
        break;
      case Network.PurchaseCanceledResponse.CancelResult.NOT_ALLOWED:
        UnityEngine.Debug.LogWarning((object) "StoreManager.OnPurchaseCanceledResponse(): cancel purchase is not allowed right now.");
        BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_THIRD_PARTY_PURCHASE_CANCEL_RESPONSE, 1, this.m_activeMoneyOrGTAPPTransaction != null ? this.m_activeMoneyOrGTAPPTransaction.ID.ToString() : string.Empty);
        bool isGTAPP = Currency.XSG == canceledResponse.CurrencyType;
        this.SetActiveMoneyOrGTAPPTransaction(canceledResponse.TransactionID, canceledResponse.ProductID, MoneyOrGTAPPTransaction.UNKNOWN_PROVIDER, isGTAPP, true);
        this.Status = !isGTAPP ? StoreManager.TransactionStatus.IN_PROGRESS_MONEY : StoreManager.TransactionStatus.IN_PROGRESS_GOLD_GTAPP;
        if (this.m_previousStatusBeforeAutoCancel == StoreManager.TransactionStatus.UNKNOWN)
          break;
        this.Status = this.m_previousStatusBeforeAutoCancel;
        this.m_previousStatusBeforeAutoCancel = StoreManager.TransactionStatus.UNKNOWN;
        break;
      case Network.PurchaseCanceledResponse.CancelResult.NOTHING_TO_CANCEL:
        BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_THIRD_PARTY_PURCHASE_CANCEL_RESPONSE, 2, this.m_activeMoneyOrGTAPPTransaction != null ? this.m_activeMoneyOrGTAPPTransaction.ID.ToString() : string.Empty);
        this.m_previousStatusBeforeAutoCancel = StoreManager.TransactionStatus.UNKNOWN;
        if (this.m_activeMoneyOrGTAPPTransaction != null && (long) StoreManager.UNKNOWN_TRANSACTION_ID != this.m_activeMoneyOrGTAPPTransaction.ID)
          this.ConfirmActiveMoneyTransaction(this.m_activeMoneyOrGTAPPTransaction.ID);
        this.Status = StoreManager.TransactionStatus.READY;
        break;
    }
  }

  private bool IsConclusiveState(Network.PurchaseErrorInfo.ErrorType errorType)
  {
    Network.PurchaseErrorInfo.ErrorType errorType1 = errorType;
    switch (errorType1)
    {
      case Network.PurchaseErrorInfo.ErrorType.WAIT_MOP:
      case Network.PurchaseErrorInfo.ErrorType.WAIT_CONFIRM:
      case Network.PurchaseErrorInfo.ErrorType.WAIT_RISK:
      case Network.PurchaseErrorInfo.ErrorType.WAIT_THIRD_PARTY_RECEIPT:
label_2:
        return false;
      default:
        switch (errorType1 + 1)
        {
          case Network.PurchaseErrorInfo.ErrorType.SUCCESS:
          case Network.PurchaseErrorInfo.ErrorType.INVALID_BNET:
            goto label_2;
          default:
            return true;
        }
    }
  }

  private bool IsSafeToRemoveMobileReceipt(Network.PurchaseErrorInfo.ErrorType errorType)
  {
    if (!this.IsConclusiveState(errorType))
      return false;
    switch (errorType)
    {
      case Network.PurchaseErrorInfo.ErrorType.BP_THIRD_PARTY_BAD_RECEIPT:
      case Network.PurchaseErrorInfo.ErrorType.BP_THIRD_PARTY_RECEIPT_USED:
      case Network.PurchaseErrorInfo.ErrorType.SUCCESS:
        return true;
      default:
        return false;
    }
  }

  private void OnBattlePayStatusResponse()
  {
    Network.BattlePayStatus payStatusResponse = Network.GetBattlePayStatusResponse();
    this.BattlePayAvailable = payStatusResponse.BattlePayAvailable;
    bool flag;
    switch (payStatusResponse.State)
    {
      case Network.BattlePayStatus.PurchaseState.READY:
        this.Status = StoreManager.TransactionStatus.READY;
        flag = true;
        break;
      case Network.BattlePayStatus.PurchaseState.CHECK_RESULTS:
        bool isGTAPP = Currency.XSG == payStatusResponse.CurrencyType;
        bool tryToResolvePreviousTransactionNotices = this.IsConclusiveState(payStatusResponse.PurchaseError.Error);
        this.SetActiveMoneyOrGTAPPTransaction(payStatusResponse.TransactionID, payStatusResponse.ProductID, payStatusResponse.Provider, isGTAPP, tryToResolvePreviousTransactionNotices);
        this.HandlePurchaseError(StoreManager.PurchaseErrorSource.FROM_STATUS_OR_PURCHASE_RESPONSE, payStatusResponse.PurchaseError.Error, payStatusResponse.PurchaseError.ErrorCode, payStatusResponse.ThirdPartyID, isGTAPP);
        flag = payStatusResponse.PurchaseError.Error != Network.PurchaseErrorInfo.ErrorType.STILL_IN_PROGRESS;
        break;
      case Network.BattlePayStatus.PurchaseState.ERROR:
        UnityEngine.Debug.LogWarning((object) "StoreManager.OnBattlePayStatusResponse(): Error getting status. Check with Rachelle.");
        flag = false;
        break;
      default:
        UnityEngine.Debug.LogError((object) string.Format("StoreManager.OnBattlePayStatusResponse(): unknown state {0}", (object) payStatusResponse.State));
        flag = false;
        break;
    }
    if (!this.BattlePayAvailable || !flag)
      return;
    this.m_statusRequestDelayTicks = StoreManager.MIN_STATUS_REQUEST_DELAY_TICKS;
    Log.Rachelle.Print(string.Format("StoreManager reset STATUS delay, now waiting {0} seconds between requests", (object) (this.m_statusRequestDelayTicks / 10000000L)));
  }

  private void OnBattlePayConfigResponse()
  {
    Network.BattlePayConfig payConfigResponse = Network.GetBattlePayConfigResponse();
    if (!payConfigResponse.Available)
    {
      this.BattlePayAvailable = false;
    }
    else
    {
      this.m_configRequestDelayTicks = StoreManager.MIN_CONFIG_REQUEST_DELAY_TICKS;
      Log.Rachelle.Print(string.Format("StoreManager reset CONFIG delay, now waiting {0} seconds between requests", (object) (this.m_configRequestDelayTicks / 10000000L)));
      this.BattlePayAvailable = true;
      this.m_currency = payConfigResponse.Currency;
      this.m_ticksBeforeAutoCancel = (long) payConfigResponse.SecondsBeforeAutoCancel * 10000000L;
      this.m_ticksBeforeAutoCancelThirdParty = (long) StoreManager.DEFAULT_SECONDS_BEFORE_AUTO_CANCEL_THIRD_PARTY * 10000000L;
      this.m_bundles.Clear();
      if ((bool) StoreManager.HAS_THIRD_PARTY_APP_STORE && ApplicationMgr.GetAndroidStore() != AndroidStore.BLIZZARD)
        StoreMobilePurchase.ClearProductList();
      using (List<Network.Bundle>.Enumerator enumerator = payConfigResponse.Bundles.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Network.Bundle current = enumerator.Current;
          if (current.ExclusiveProviders == null || current.ExclusiveProviders.Count <= 0 || current.ExclusiveProviders.Contains(StoreManager.StoreProvider))
          {
            if ((bool) StoreManager.HAS_THIRD_PARTY_APP_STORE && ApplicationMgr.GetAndroidStore() != AndroidStore.BLIZZARD)
            {
              string empty = string.Empty;
              if (!string.IsNullOrEmpty(empty))
              {
                Log.Yim.Print("bundle.ProductId=" + current.ProductID + " thirdPartyID=" + empty);
                StoreMobilePurchase.AddProductById(empty);
                this.m_bundles.Add(current.ProductID, current);
              }
            }
            else
              this.m_bundles.Add(current.ProductID, current);
          }
        }
      }
      this.m_goldCostBooster.Clear();
      using (List<Network.GoldCostBooster>.Enumerator enumerator = payConfigResponse.GoldCostBoosters.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Network.GoldCostBooster current = enumerator.Current;
          this.m_goldCostBooster.Add(current.ID, current);
        }
      }
      this.m_goldCostArena = payConfigResponse.GoldCostArena;
      if ((bool) StoreManager.HAS_THIRD_PARTY_APP_STORE && ApplicationMgr.GetAndroidStore() != AndroidStore.BLIZZARD)
        StoreMobilePurchase.ValidateAllProducts(MobileCallbackManager.Get().name);
      if ((bool) StoreManager.HAS_THIRD_PARTY_APP_STORE && ApplicationMgr.GetAndroidStore() != AndroidStore.BLIZZARD)
        return;
      this.ConfigLoaded = true;
    }
  }

  private void HandleZeroCostLicensePurchaseMethod(Network.PurchaseMethod method)
  {
    if (method.PurchaseError.Error != Network.PurchaseErrorInfo.ErrorType.STILL_IN_PROGRESS)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.HandleZeroCostLicensePurchaseMethod() FAILED error={0}", (object) method.PurchaseError.Error));
      this.Status = StoreManager.TransactionStatus.READY;
    }
    else
      Log.Rachelle.Print("StoreManager.HandleZeroCostLicensePurchaseMethod succeeded, refreshing achieves");
  }

  private void OnPurchaseMethod()
  {
    Network.PurchaseMethod purchaseMethodResponse = Network.GetPurchaseMethodResponse();
    if (purchaseMethodResponse.IsZeroCostLicense)
    {
      this.HandleZeroCostLicensePurchaseMethod(purchaseMethodResponse);
    }
    else
    {
      this.m_challengePurchaseMethod = string.IsNullOrEmpty(purchaseMethodResponse.ChallengeID) || string.IsNullOrEmpty(purchaseMethodResponse.ChallengeURL) ? (Network.PurchaseMethod) null : purchaseMethodResponse;
      bool isGTAPP = Currency.XSG == purchaseMethodResponse.Currency;
      this.SetActiveMoneyOrGTAPPTransaction(purchaseMethodResponse.TransactionID, purchaseMethodResponse.ProductID, new BattlePayProvider?(BattlePayProvider.BP_PROVIDER_BLIZZARD), isGTAPP, false);
      bool flag = false;
      if (purchaseMethodResponse.PurchaseError != null)
      {
        if (purchaseMethodResponse.PurchaseError.Error == Network.PurchaseErrorInfo.ErrorType.BP_NO_VALID_PAYMENT)
        {
          flag = true;
        }
        else
        {
          this.HandlePurchaseError(StoreManager.PurchaseErrorSource.FROM_PURCHASE_METHOD_RESPONSE, purchaseMethodResponse.PurchaseError.Error, purchaseMethodResponse.PurchaseError.ErrorCode, string.Empty, isGTAPP);
          return;
        }
      }
      this.ActivateStoreCover();
      if (isGTAPP)
      {
        this.OnSummaryConfirm(purchaseMethodResponse.ProductID, purchaseMethodResponse.Quantity, (object) null);
      }
      else
      {
        string paymentMethodName = !flag ? (!purchaseMethodResponse.UseEBalance ? purchaseMethodResponse.WalletName : GameStrings.Get("GLUE_STORE_BNET_BALANCE")) : (string) null;
        Store currentStore = this.GetCurrentStore();
        if ((UnityEngine.Object) currentStore == (UnityEngine.Object) null || !currentStore.IsShown())
        {
          this.AutoCancelPurchaseIfPossible();
        }
        else
        {
          if ((UnityEngine.Object) this.m_storePurchaseAuth != (UnityEngine.Object) null)
            this.m_storePurchaseAuth.Hide();
          this.Status = StoreManager.TransactionStatus.WAIT_CONFIRM;
          this.m_storeSummary.Show(purchaseMethodResponse.ProductID, purchaseMethodResponse.Quantity, paymentMethodName);
        }
      }
    }
  }

  private void OnPurchaseResponse()
  {
    Network.PurchaseResponse purchaseResponse = Network.GetPurchaseResponse();
    bool isGTAPP = Currency.XSG == purchaseResponse.CurrencyType;
    this.SetActiveMoneyOrGTAPPTransaction(purchaseResponse.TransactionID, purchaseResponse.ProductID, MoneyOrGTAPPTransaction.UNKNOWN_PROVIDER, isGTAPP, false);
    this.HandlePurchaseError(StoreManager.PurchaseErrorSource.FROM_STATUS_OR_PURCHASE_RESPONSE, purchaseResponse.PurchaseError.Error, purchaseResponse.PurchaseError.ErrorCode, purchaseResponse.ThirdPartyID, isGTAPP);
  }

  private void OnPurchaseViaGoldResponse()
  {
    Network.PurchaseViaGoldResponse withGoldResponse = Network.GetPurchaseWithGoldResponse();
    string empty = string.Empty;
    string failDetails;
    switch (withGoldResponse.Error)
    {
      case Network.PurchaseViaGoldResponse.ErrorType.SUCCESS:
        NetCache.Get().RefreshNetObject<NetCache.NetCacheGoldBalance>();
        this.HandlePurchaseSuccess(new StoreManager.PurchaseErrorSource?(), (MoneyOrGTAPPTransaction) null, string.Empty);
        return;
      case Network.PurchaseViaGoldResponse.ErrorType.INSUFFICIENT_GOLD:
        failDetails = GameStrings.Get("GLUE_STORE_FAIL_NOT_ENOUGH_GOLD");
        break;
      case Network.PurchaseViaGoldResponse.ErrorType.PRODUCT_NA:
        failDetails = GameStrings.Get("GLUE_STORE_FAIL_PRODUCT_NA");
        break;
      case Network.PurchaseViaGoldResponse.ErrorType.FEATURE_NA:
        failDetails = GameStrings.Get("GLUE_TOOLTIP_BUTTON_DISABLED_DESC");
        break;
      case Network.PurchaseViaGoldResponse.ErrorType.INVALID_QUANTITY:
        failDetails = GameStrings.Get("GLUE_STORE_FAIL_QUANTITY");
        break;
      default:
        failDetails = GameStrings.Get("GLUE_STORE_FAIL_GENERAL");
        break;
    }
    this.Status = StoreManager.TransactionStatus.READY;
    this.m_storePurchaseAuth.CompletePurchaseFailure((MoneyOrGTAPPTransaction) null, failDetails, Network.PurchaseErrorInfo.ErrorType.BP_GENERIC_FAIL);
  }

  private void OnThirdPartyPurchaseStatusResponse()
  {
    Network.ThirdPartyPurchaseStatusResponse purchaseStatusResponse = Network.GetThirdPartyPurchaseStatusResponse();
    UnityEngine.Debug.Log((object) string.Format("StoreManager.OnThirdPartyPurchaseStatusResponse(): ThirdPartyID='{0}', Status={1} -- receipt can be removed from client now", (object) purchaseStatusResponse.ThirdPartyID, (object) purchaseStatusResponse.Status));
    switch (purchaseStatusResponse.Status)
    {
      case Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus.NOT_FOUND:
        StoreMobilePurchase.ThirdPartyPurchaseStatus(purchaseStatusResponse.ThirdPartyID, purchaseStatusResponse.Status);
        break;
      case Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus.SUCCEEDED:
      case Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus.FAILED:
        Log.Rachelle.Print(string.Format("StoreManager.OnThirdPartyPurchaseStatusResponse(): ThirdPartyID='{0}', Status={1} -- receipt can be removed from client now", (object) purchaseStatusResponse.ThirdPartyID, (object) purchaseStatusResponse.Status));
        goto case Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus.NOT_FOUND;
      case Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus.IN_PROGRESS:
        Log.Rachelle.Print(string.Format("StoreManager.OnThirdPartyPurchaseStatusResponse(): ThirdPartyID='{0}' still in progress, leave receipt on client", (object) purchaseStatusResponse.ThirdPartyID));
        goto case Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus.NOT_FOUND;
      default:
        UnityEngine.Debug.LogWarning((object) string.Format("StoreManager.OnThirdPartyPurchaseStatusResponse(): unexpected Status {0} received for third party ID '{1}'", (object) purchaseStatusResponse.Status, (object) purchaseStatusResponse.ThirdPartyID));
        goto case Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus.NOT_FOUND;
    }
  }

  private void OnStoreComponentReady(object userData)
  {
    if (!this.m_waitingToShowStore || !this.IsCurrentStoreLoaded())
      return;
    this.ShowStore();
  }

  private void OnGeneralStoreLoaded(string name, GameObject go, object callbackData)
  {
    GeneralStore generalStore = this.OnStoreLoaded<GeneralStore>(go, StoreType.GENERAL_STORE);
    if (!((UnityEngine.Object) generalStore != (UnityEngine.Object) null))
      return;
    this.SetupLoadedStore((Store) generalStore);
  }

  private void OnArenaStoreLoaded(string name, GameObject go, object callbackData)
  {
    ArenaStore arenaStore = this.OnStoreLoaded<ArenaStore>(go, StoreType.ARENA_STORE);
    if (!((UnityEngine.Object) arenaStore != (UnityEngine.Object) null))
      return;
    this.SetupLoadedStore((Store) arenaStore);
  }

  private void OnHeroicBrawlStoreLoaded(string name, GameObject go, object callbackData)
  {
    TavernBrawlStore tavernBrawlStore = this.OnStoreLoaded<TavernBrawlStore>(go, StoreType.TAVERN_BRAWL_STORE);
    if (!((UnityEngine.Object) tavernBrawlStore != (UnityEngine.Object) null))
      return;
    this.SetupLoadedStore((Store) tavernBrawlStore);
  }

  private void OnAdventureStoreLoaded(string name, GameObject go, object callbackData)
  {
    AdventureStore adventureStore = this.OnStoreLoaded<AdventureStore>(go, StoreType.ADVENTURE_STORE);
    if (!((UnityEngine.Object) adventureStore != (UnityEngine.Object) null))
      return;
    this.SetupLoadedStore((Store) adventureStore);
  }

  private T OnStoreLoaded<T>(GameObject go, StoreType storeType) where T : Store
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("StoreManager.OnStoreLoaded<{0}>(): go is null!", (object) typeof (T)));
      return (T) null;
    }
    T component = go.GetComponent<T>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("StoreManager.OnStoreLoaded<{0}>(): go has no {1} component!", (object) typeof (T), (object) typeof (T)));
      return (T) null;
    }
    this.m_stores[storeType] = (Store) component;
    return component;
  }

  private void SetupLoadedStore(Store store)
  {
    if ((UnityEngine.Object) store == (UnityEngine.Object) null)
      return;
    store.Hide();
    store.RegisterBuyWithMoneyListener(new Store.BuyWithMoneyCallback(this.OnStoreBuyWithMoney));
    store.RegisterBuyWithGoldGTAPPListener(new Store.BuyWithGoldGTAPPCallback(this.OnStoreBuyWithGTAPP));
    store.RegisterBuyWithGoldNoGTAPPListener(new Store.BuyWithGoldNoGTAPPCallback(this.OnStoreBuyWithGoldNoGTAPP));
    store.RegisterExitListener(new Store.ExitCallback(this.OnStoreExit));
    store.RegisterInfoListener(new Store.InfoCallback(this.OnStoreInfo));
    store.RegisterReadyListener(new Store.ReadyCallback(this.OnStoreComponentReady));
    this.OnStoreComponentReady((object) null);
  }

  private void OnStorePurchaseAuthLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "StoreManager.OnStorePurchaseAuthLoaded(): go is null!");
    }
    else
    {
      this.m_storePurchaseAuth = go.GetComponent<StorePurchaseAuth>();
      if ((UnityEngine.Object) this.m_storePurchaseAuth == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "StoreManager.OnStorePurchaseAuthLoaded(): go has no StorePurchaseAuth component");
      }
      else
      {
        this.m_storePurchaseAuth.Hide();
        this.m_storePurchaseAuth.RegisterAckPurchaseResultListener(new StorePurchaseAuth.AckPurchaseResultListener(this.OnPurchaseResultAcknowledged));
        this.m_storePurchaseAuth.RegisterExitListener(new StorePurchaseAuth.ExitListener(this.OnAuthExit));
        this.OnStoreComponentReady((object) null);
      }
    }
  }

  private void OnStoreSummaryLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "StoreManager.OnStoreSummaryLoaded(): go is null!");
    }
    else
    {
      this.m_storeSummary = go.GetComponent<StoreSummary>();
      if ((UnityEngine.Object) this.m_storeSummary == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "StoreManager.OnStoreSummaryLoaded(): go has no StoreSummary component");
      }
      else
      {
        this.m_storeSummary.Hide();
        this.m_storeSummary.RegisterConfirmListener(new StoreSummary.ConfirmCallback(this.OnSummaryConfirm));
        this.m_storeSummary.RegisterCancelListener(new StoreSummary.CancelCallback(this.OnSummaryCancel));
        this.m_storeSummary.RegisterInfoListener(new StoreSummary.InfoCallback(this.OnSummaryInfo));
        this.m_storeSummary.RegisterPaymentAndTOSListener(new StoreSummary.PaymentAndTOSCallback(this.OnSummaryPaymentAndTOS));
        this.OnStoreComponentReady((object) null);
      }
    }
  }

  private void OnStoreSendToBAMLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "StoreManager.OnStoreSendToBAMLoaded(): go is null!");
    }
    else
    {
      this.m_storeSendToBAM = go.GetComponent<StoreSendToBAM>();
      if ((UnityEngine.Object) this.m_storeSendToBAM == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "StoreManager.OnStoreSendToBAMLoaded(): go has no StoreSendToBAM component");
      }
      else
      {
        this.m_storeSendToBAM.Hide();
        this.m_storeSendToBAM.RegisterOkayListener(new StoreSendToBAM.DelOKListener(this.OnSendToBAMOkay));
        this.m_storeSendToBAM.RegisterCancelListener(new StoreSendToBAM.DelCancelListener(this.OnSendToBAMCancel));
        this.OnStoreComponentReady((object) null);
      }
    }
  }

  private void OnStoreLegalBAMLinksLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "StoreManager.OnStoreLegalBAMLinksLoaded(): go is null!");
    }
    else
    {
      this.m_storeLegalBAMLinks = go.GetComponent<StoreLegalBAMLinks>();
      if ((UnityEngine.Object) this.m_storeLegalBAMLinks == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "StoreManager.OnStoreLegalBAMLinksLoaded(): go has no StoreLegalBAMLinks component");
      }
      else
      {
        this.m_storeLegalBAMLinks.Hide();
        this.m_storeLegalBAMLinks.RegisterSendToBAMListener(new StoreLegalBAMLinks.SendToBAMListener(this.OnSendToBAMLegal));
        this.m_storeLegalBAMLinks.RegisterCancelListener(new StoreLegalBAMLinks.CancelListener(this.OnSendToBAMLegalCancel));
        this.OnStoreComponentReady((object) null);
      }
    }
  }

  private void OnStoreDoneWithBAMLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "StoreManager.OnStoreDoneWithBAMLoaded(): go is null!");
    }
    else
    {
      this.m_storeDoneWithBAM = go.GetComponent<StoreDoneWithBAM>();
      if ((UnityEngine.Object) this.m_storeDoneWithBAM == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "StoreManager.OnStoreDoneWithBAMLoaded(): go has no StoreDoneWithBAM component");
      }
      else
      {
        this.m_storeDoneWithBAM.Hide();
        this.m_storeDoneWithBAM.RegisterOkayListener(new StoreDoneWithBAM.ButtonPressedListener(this.OnDoneWithBAM));
        this.OnStoreComponentReady((object) null);
      }
    }
  }

  private void OnStoreChallengePromptLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "StoreManager.OnStoreChallengePromptLoaded(): go is null!");
    }
    else
    {
      this.m_storeChallengePrompt = go.GetComponent<StoreChallengePrompt>();
      if ((UnityEngine.Object) this.m_storeChallengePrompt == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "StoreManager.OnStoreChallengePromptLoaded(): go has no StoreChallengePrompt component");
      }
      else
      {
        this.m_storeChallengePrompt.Hide();
        this.m_storeChallengePrompt.OnChallengeComplete += new StoreChallengePrompt.CompleteListener(this.OnChallengeComplete);
        this.m_storeChallengePrompt.OnCancel += new StoreChallengePrompt.CancelListener(this.OnChallengeCancel);
        this.OnStoreComponentReady((object) null);
      }
    }
  }

  private void OnSceneUnloaded(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    this.UnloadAndFreeMemory();
  }

  private void HideAllPurchasePopups()
  {
    if ((UnityEngine.Object) this.m_storePurchaseAuth != (UnityEngine.Object) null)
      this.m_storePurchaseAuth.Hide();
    if ((UnityEngine.Object) this.m_storeSummary != (UnityEngine.Object) null)
      this.m_storeSummary.Hide();
    if ((UnityEngine.Object) this.m_storeSendToBAM != (UnityEngine.Object) null)
      this.m_storeSendToBAM.Hide();
    if ((UnityEngine.Object) this.m_storeLegalBAMLinks != (UnityEngine.Object) null)
      this.m_storeLegalBAMLinks.Hide();
    if ((UnityEngine.Object) this.m_storeDoneWithBAM != (UnityEngine.Object) null)
      this.m_storeDoneWithBAM.Hide();
    if ((UnityEngine.Object) this.m_storeChallengePrompt != (UnityEngine.Object) null)
      this.m_storeChallengePrompt.Hide();
    if (this.m_handlePurchaseNoticesCoroutine == null)
      return;
    SceneMgr.Get().StopCoroutine(this.m_handlePurchaseNoticesCoroutine);
  }

  private enum PurchaseErrorSource
  {
    FROM_PURCHASE_METHOD_RESPONSE,
    FROM_STATUS_OR_PURCHASE_RESPONSE,
    FROM_PREVIOUS_PURCHASE,
  }

  private enum TransactionStatus
  {
    UNKNOWN,
    IN_PROGRESS_MONEY,
    IN_PROGRESS_GOLD_GTAPP,
    IN_PROGRESS_GOLD_NO_GTAPP,
    READY,
    WAIT_ZERO_COST_LICENSE,
    WAIT_METHOD_OF_PAYMENT,
    WAIT_THIRD_PARTY_INIT,
    WAIT_THIRD_PARTY_RECEIPT,
    WAIT_CONFIRM,
    WAIT_RISK,
    CHALLENGE_SUBMITTED,
    CHALLENGE_CANCELED,
    USER_CANCELING,
    AUTO_CANCELING,
  }

  private class StoreAchievesData
  {
    public Network.Bundle Bundle { get; private set; }

    public PaymentMethod MethodOfPayment { get; private set; }

    public StoreAchievesData(Network.Bundle bundle, PaymentMethod paymentMethod)
    {
      this.Bundle = bundle;
      this.MethodOfPayment = paymentMethod;
    }
  }

  private class ZeroCostTransactionData
  {
    public Network.Bundle Bundle { get; private set; }

    public ZeroCostTransactionData(Network.Bundle bundle)
    {
      this.Bundle = bundle;
    }
  }

  private class StatusChangedListener : EventListener<StoreManager.StatusChangedCallback>
  {
    public void Fire(bool isOpen)
    {
      this.m_callback(isOpen, this.m_userData);
    }
  }

  private class SuccessfulPurchaseAckListener : EventListener<StoreManager.SuccessfulPurchaseAckCallback>
  {
    public void Fire(Network.Bundle bundle, PaymentMethod paymentMethod)
    {
      this.m_callback(bundle, paymentMethod, this.m_userData);
    }
  }

  private class SuccessfulPurchaseListener : EventListener<StoreManager.SuccessfulPurchaseCallback>
  {
    public void Fire(Network.Bundle bundle, PaymentMethod paymentMethod)
    {
      this.m_callback(bundle, paymentMethod, this.m_userData);
    }
  }

  private class AuthorizationExitListener : EventListener<StoreManager.AuthorizationExitCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  private class StoreShownListener : EventListener<StoreManager.StoreShownCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  private class StoreHiddenListener : EventListener<StoreManager.StoreHiddenCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  private class StoreAchievesListener : EventListener<StoreManager.StoreAchievesCallback>
  {
    public void Fire(Network.Bundle bundle, PaymentMethod paymentMethod)
    {
      this.m_callback(bundle, paymentMethod, this.m_userData);
    }
  }

  private struct ShowStoreData
  {
    public bool m_isTotallyFake;
    public Store.ExitCallback m_exitCallback;
    public object m_exitCallbackUserData;
    public ProductType m_storeProduct;
    public int m_storeProductData;
    public GeneralStoreMode m_storeMode;
    public StoreManager.ZeroCostTransactionData m_zeroCostTransactionData;
  }

  public delegate void StatusChangedCallback(bool isOpen, object userData);

  public delegate void SuccessfulPurchaseAckCallback(Network.Bundle bundle, PaymentMethod purchaseMethod, object userData);

  public delegate void SuccessfulPurchaseCallback(Network.Bundle bundle, PaymentMethod purchaseMethod, object userData);

  public delegate void AuthorizationExitCallback(object userData);

  public delegate void StoreShownCallback(object userData);

  public delegate void StoreHiddenCallback(object userData);

  public delegate void StoreAchievesCallback(Network.Bundle bundle, PaymentMethod paymentMethod, object userData);
}
