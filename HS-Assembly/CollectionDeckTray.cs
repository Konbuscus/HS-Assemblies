﻿// Decompiled with JetBrains decompiler
// Type: CollectionDeckTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class CollectionDeckTray : MonoBehaviour
{
  public List<CollectionDeckTray.DeckContentScroll> m_scrollables = new List<CollectionDeckTray.DeckContentScroll>();
  private Map<CollectionDeckTray.DeckContentTypes, DeckTrayContent> m_contents = new Map<CollectionDeckTray.DeckContentTypes, DeckTrayContent>();
  private CollectionDeckTray.DeckContentTypes m_currentContent = CollectionDeckTray.DeckContentTypes.INVALID;
  private CollectionDeckTray.DeckContentTypes m_contentToSet = CollectionDeckTray.DeckContentTypes.INVALID;
  private List<CollectionDeckTray.ModeSwitched> m_modeSwitchedListeners = new List<CollectionDeckTray.ModeSwitched>();
  public UberText m_countLabelText;
  public UberText m_countText;
  public UIBButton m_doneButton;
  public GameObject m_backArrow;
  public UberText m_myDecksLabel;
  public DeckTrayDeckListContent m_decksContent;
  public DeckTrayCardListContent m_cardsContent;
  public DeckTrayCardBackContent m_cardBackContent;
  public DeckTrayHeroSkinContent m_heroSkinContent;
  public DeckBigCard m_deckBigCard;
  public UIBScrollable m_scrollbar;
  public GameObject m_inputBlocker;
  public TooltipZone m_deckHeaderTooltip;
  public GameObject m_topCardPositionBone;
  public Transform m_removeCardTutorialBone;
  public PlayMakerFSM m_deckTemplateChosenGlow;
  private static CollectionDeckTray s_instance;
  private bool m_settingNewMode;
  private bool m_updatingTrayMode;

  private void Awake()
  {
    CollectionDeckTray.s_instance = this;
    if ((UnityEngine.Object) this.gameObject.GetComponent<AudioSource>() == (UnityEngine.Object) null)
      this.gameObject.AddComponent<AudioSource>();
    if ((UnityEngine.Object) this.m_scrollbar != (UnityEngine.Object) null)
    {
      if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL && !(bool) UniversalInputManager.UsePhoneUI)
      {
        Vector3 center = this.m_scrollbar.m_ScrollBounds.center;
        center.z = 3f;
        this.m_scrollbar.m_ScrollBounds.center = center;
        Vector3 size = this.m_scrollbar.m_ScrollBounds.size;
        size.z = 47.67f;
        this.m_scrollbar.m_ScrollBounds.size = size;
        if ((UnityEngine.Object) this.m_cardsContent != (UnityEngine.Object) null && (UnityEngine.Object) this.m_cardsContent.m_deckCompleteHighlight != (UnityEngine.Object) null)
        {
          Vector3 localPosition = this.m_cardsContent.m_deckCompleteHighlight.transform.localPosition;
          localPosition.z = -34.15f;
          this.m_cardsContent.m_deckCompleteHighlight.transform.localPosition = localPosition;
        }
      }
      this.m_scrollbar.Enable(false);
      this.m_scrollbar.AddTouchScrollStartedListener(new UIBScrollable.OnTouchScrollStarted(this.OnTouchScrollStarted));
      this.m_scrollbar.AddTouchScrollEndedListener(new UIBScrollable.OnTouchScrollEnded(this.OnTouchScrollEnded));
    }
    this.m_contents[CollectionDeckTray.DeckContentTypes.Decks] = (DeckTrayContent) this.m_decksContent;
    this.m_contents[CollectionDeckTray.DeckContentTypes.Cards] = (DeckTrayContent) this.m_cardsContent;
    if ((UnityEngine.Object) this.m_heroSkinContent != (UnityEngine.Object) null)
    {
      this.m_contents[CollectionDeckTray.DeckContentTypes.HeroSkin] = (DeckTrayContent) this.m_heroSkinContent;
      this.m_heroSkinContent.RegisterHeroAssignedListener(new DeckTrayHeroSkinContent.HeroAssigned(this.OnHeroAssigned));
    }
    if ((UnityEngine.Object) this.m_cardBackContent != (UnityEngine.Object) null)
      this.m_contents[CollectionDeckTray.DeckContentTypes.CardBack] = (DeckTrayContent) this.m_cardBackContent;
    this.m_cardsContent.RegisterCardTileHeldListener(new DeckTrayCardListContent.CardTileHeld(this.OnCardTileHeld));
    this.m_cardsContent.RegisterCardTilePressListener(new DeckTrayCardListContent.CardTilePress(this.OnCardTilePress));
    this.m_cardsContent.RegisterCardTileTapListener(new DeckTrayCardListContent.CardTileTap(this.OnCardTileTap));
    this.m_cardsContent.RegisterCardTileOverListener(new DeckTrayCardListContent.CardTileOver(this.OnCardTileOver));
    this.m_cardsContent.RegisterCardTileOutListener(new DeckTrayCardListContent.CardTileOut(this.OnCardTileOut));
    this.m_cardsContent.RegisterCardTileReleaseListener(new DeckTrayCardListContent.CardTileRelease(this.OnCardTileRelease));
    this.m_cardsContent.RegisterCardCountUpdated(new DeckTrayCardListContent.CardCountChanged(this.OnCardCountUpdated));
    this.m_decksContent.RegisterDeckCountUpdated(new DeckTrayDeckListContent.DeckCountChanged(this.OnDeckCountUpdated));
    this.m_decksContent.RegisterBusyWithDeck(new DeckTrayDeckListContent.BusyWithDeck(this.OnBusyWithDeck));
    string key = "GLUE_COLLECTION_MY_DECKS";
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
      key = !TavernBrawlManager.Get().IsCurrentSeasonSessionBased ? "GLUE_COLLECTION_DECK" : "GLUE_HEROIC_BRAWL_DECK";
    this.SetMyDecksLabelText(GameStrings.Get(key));
    this.m_doneButton.SetText(GameStrings.Get("GLOBAL_BACK"));
    this.m_doneButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.DoneButtonPress));
    CollectionManager.Get().RegisterTaggedDeckChanged(new CollectionManager.OnTaggedDeckChanged(this.OnTaggedDeckChanged));
    CollectionInputMgr.Get().SetScrollbar(this.m_scrollbar);
    CollectionManagerDisplay.Get().UpdateCurrentPageCardLocks(true);
    CollectionManagerDisplay.Get().RegisterSwitchViewModeListener(new CollectionManagerDisplay.OnSwitchViewMode(this.OnCMViewModeChanged));
    using (List<CollectionDeckTray.DeckContentScroll>.Enumerator enumerator = this.m_scrollables.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SaveStartPosition();
    }
  }

  private void OnDestroy()
  {
    CollectionManager collectionManager = CollectionManager.Get();
    if (collectionManager != null)
    {
      collectionManager.RemoveTaggedDeckChanged(new CollectionManager.OnTaggedDeckChanged(this.OnTaggedDeckChanged));
      collectionManager.DoneEditing();
    }
    CollectionDeckTray.s_instance = (CollectionDeckTray) null;
  }

  private void Start()
  {
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnBackOutOfCollectionScreen));
    SoundManager.Get().Load("panel_slide_off_deck_creation_screen");
  }

  public bool CanPickupCard()
  {
    CollectionDeckTray.DeckContentTypes currentContentType = this.GetCurrentContentType();
    CollectionManagerDisplay.ViewMode viewMode = CollectionManagerDisplay.Get().GetViewMode();
    if (currentContentType == CollectionDeckTray.DeckContentTypes.Cards && viewMode == CollectionManagerDisplay.ViewMode.CARDS || currentContentType == CollectionDeckTray.DeckContentTypes.CardBack && viewMode == CollectionManagerDisplay.ViewMode.CARD_BACKS)
      return true;
    if (currentContentType == CollectionDeckTray.DeckContentTypes.HeroSkin)
      return viewMode == CollectionManagerDisplay.ViewMode.HERO_SKINS;
    return false;
  }

  public static CollectionDeckTray Get()
  {
    return CollectionDeckTray.s_instance;
  }

  public void Initialize()
  {
    this.SetTrayMode(CollectionDeckTray.DeckContentTypes.Decks);
  }

  public void Unload()
  {
    CollectionInputMgr.Get().SetScrollbar((UIBScrollable) null);
  }

  public bool AddCard(EntityDef cardEntityDef, TAG_PREMIUM premium, DeckTrayDeckTileVisual deckTileToRemove, bool playSound, Actor animateActor = null)
  {
    return this.GetCardsContent().AddCard(cardEntityDef, premium, deckTileToRemove, playSound, animateActor);
  }

  public int RemoveClosestInvalidCard(EntityDef entityDef, int sameRemoveCount)
  {
    return this.GetCardsContent().RemoveClosestInvalidCard(entityDef, sameRemoveCount);
  }

  public bool SetCardBack(Actor actor)
  {
    CollectionCardBack component = actor.gameObject.GetComponent<CollectionCardBack>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return false;
    return this.GetCardBackContent().SetNewCardBack(component.GetCardBackId(), actor.gameObject);
  }

  public void FlashDeckTemplateHighlight()
  {
    if (!((UnityEngine.Object) this.m_deckTemplateChosenGlow != (UnityEngine.Object) null))
      return;
    this.m_deckTemplateChosenGlow.SendEvent("Flash");
  }

  public void SetHeroSkin(Actor actor)
  {
    this.GetHeroSkinContent().SetNewHeroSkin(actor);
  }

  public bool HandleDeletedCardDeckUpdate(string cardID)
  {
    if (!this.IsShowingDeckContents())
      return false;
    this.GetCardsContent().UpdateCardList(cardID, true, (Actor) null);
    CollectionManagerDisplay.Get().UpdateCurrentPageCardLocks(true);
    return true;
  }

  public bool RemoveCard(string cardID, TAG_PREMIUM premium, bool valid)
  {
    bool flag = false;
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    if (taggedDeck != null)
    {
      flag = taggedDeck.RemoveCard(cardID, premium, valid, false);
      this.HandleDeletedCardDeckUpdate(cardID);
    }
    return flag;
  }

  public void ShowDeck(CollectionManagerDisplay.ViewMode viewMode, long deckID, bool isNewDeck)
  {
    CollectionManager.Get().StartEditingDeck(CollectionManager.DeckTag.Editing, deckID, (object) isNewDeck);
    if (viewMode == CollectionManagerDisplay.ViewMode.HERO_SKINS && !CollectionManagerDisplay.Get().CanViewHeroSkins() || viewMode == CollectionManagerDisplay.ViewMode.CARD_BACKS && !CollectionManagerDisplay.Get().CanViewCardBacks())
    {
      viewMode = CollectionManagerDisplay.ViewMode.CARDS;
      CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.CARDS, (CollectionManagerDisplay.ViewModeData) null);
    }
    this.SetTrayMode(this.GetContentTypeFromViewMode(viewMode));
    CollectionDeck editedDeck = CollectionManager.Get().GetEditedDeck();
    editedDeck.ReconcileUnownedCards();
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL)
      Navigation.Push(new Navigation.NavigateBackHandler(this.OnBackOutOfDeckContents));
    if (!CollectionManager.Get().ShouldShowWildToStandardTutorial(false) || !editedDeck.IsWild || CollectionManagerDisplay.Get().GetViewMode() == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
      return;
    CollectionManagerDisplay.Get().ShowConvertTutorial(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS);
  }

  public void EnterEditDeckModeForTavernBrawl()
  {
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnBackOutOfDeckContents));
    this.UpdateDoneButtonText();
    this.m_cardsContent.UpdateCardList(true, (Actor) null);
  }

  public void ExitEditDeckModeForTavernBrawl()
  {
    this.UpdateDoneButtonText();
  }

  public void AllowInput(bool allowed)
  {
    this.m_inputBlocker.SetActive(!allowed);
  }

  public bool MouseIsOver()
  {
    return UniversalInputManager.Get().InputIsOver(this.gameObject);
  }

  public bool IsShowingDeckContents()
  {
    return this.GetCurrentContentType() != CollectionDeckTray.DeckContentTypes.Decks;
  }

  public bool IsWaitingToDeleteDeck()
  {
    return this.m_decksContent.IsWaitingToDeleteDeck();
  }

  public void DeleteEditingDeck(bool popNavigation = true)
  {
    if (popNavigation)
      Navigation.Pop();
    this.m_decksContent.DeleteEditingDeck();
    this.SetTrayMode(CollectionDeckTray.DeckContentTypes.Decks);
  }

  public void CancelRenamingDeck()
  {
    this.m_decksContent.CancelRenameEditingDeck();
  }

  public DeckBigCard GetDeckBigCard()
  {
    return this.m_deckBigCard;
  }

  public void ClearCountLabels()
  {
    this.m_countLabelText.Text = string.Empty;
    this.m_countText.Text = string.Empty;
  }

  public DeckTrayDeckTileVisual GetCardTileVisual(string cardID)
  {
    return this.m_cardsContent.GetCardTileVisual(cardID);
  }

  public DeckTrayDeckTileVisual GetCardTileVisual(string cardID, TAG_PREMIUM premType)
  {
    return this.m_cardsContent.GetCardTileVisual(cardID, premType);
  }

  public DeckTrayDeckTileVisual GetCardTileVisual(int index)
  {
    return this.m_cardsContent.GetCardTileVisual(index);
  }

  public DeckTrayDeckTileVisual GetOrAddCardTileVisual(int index, bool affectedByScrollbar = true)
  {
    DeckTrayDeckTileVisual addCardTileVisual = this.m_cardsContent.GetOrAddCardTileVisual(index);
    if ((UnityEngine.Object) addCardTileVisual == (UnityEngine.Object) null)
    {
      addCardTileVisual = this.m_cardsContent.GetOrAddCardTileVisual(index);
      if (affectedByScrollbar)
        this.m_scrollbar.AddVisibleAffectedObject(addCardTileVisual.gameObject, this.m_cardsContent.GetCardVisualExtents(), true, new UIBScrollable.VisibleAffected(CollectionDeckTray.OnDeckTrayTileScrollVisibleAffected));
    }
    return addCardTileVisual;
  }

  public DeckTrayContent GetCurrentContent()
  {
    return this.m_contents[this.m_currentContent];
  }

  public CollectionDeckTray.DeckContentTypes GetCurrentContentType()
  {
    return this.m_currentContent;
  }

  public void SetMyDecksLabelText(string text)
  {
    this.m_myDecksLabel.Text = text;
  }

  public TooltipZone GetTooltipZone()
  {
    return this.m_deckHeaderTooltip;
  }

  public static void OnDeckTrayTileScrollVisibleAffected(GameObject obj, bool visible)
  {
    DeckTrayDeckTileVisual component = obj.GetComponent<DeckTrayDeckTileVisual>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || !component.IsInUse() || visible == component.gameObject.activeSelf)
      return;
    component.gameObject.SetActive(visible);
  }

  public DeckTrayDeckListContent GetDecksContent()
  {
    return this.m_decksContent;
  }

  public DeckTrayCardListContent GetCardsContent()
  {
    return this.m_cardsContent;
  }

  public DeckTrayCardBackContent GetCardBackContent()
  {
    return this.m_cardBackContent;
  }

  public DeckTrayHeroSkinContent GetHeroSkinContent()
  {
    return this.m_heroSkinContent;
  }

  public void SetTrayMode(CollectionDeckTray.DeckContentTypes contentType)
  {
    this.m_contentToSet = contentType;
    if (this.m_settingNewMode || this.m_currentContent == contentType)
      return;
    this.StartCoroutine(this.UpdateTrayMode());
  }

  public void RegisterModeSwitchedListener(CollectionDeckTray.ModeSwitched callback)
  {
    this.m_modeSwitchedListeners.Add(callback);
  }

  public void UnregisterModeSwitchedListener(CollectionDeckTray.ModeSwitched callback)
  {
    this.m_modeSwitchedListeners.Remove(callback);
  }

  public void Exit()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      return;
    this.HideUnseenDeckTrays();
  }

  public void UpdateTileVisuals()
  {
    this.m_cardsContent.UpdateTileVisuals();
  }

  public CollectionDeckBoxVisual GetEditingDeckBox()
  {
    TraySection editingTraySection = this.GetDecksContent().GetEditingTraySection();
    if ((UnityEngine.Object) editingTraySection == (UnityEngine.Object) null)
      return (CollectionDeckBoxVisual) null;
    return editingTraySection.m_deckBox;
  }

  private void DoneButtonPress(UIEvent e)
  {
    Navigation.GoBack();
  }

  public bool OnBackOutOfDeckContents()
  {
    return this.OnBackOutOfDeckContentsImpl(false);
  }

  public bool OnBackOutOfDeckContentsImpl(bool deleteDeck)
  {
    if (this.GetCurrentContentType() != CollectionDeckTray.DeckContentTypes.INVALID && !this.GetCurrentContent().IsModeActive() || !this.IsShowingDeckContents())
      return false;
    Log.DeckTray.Print("backing out of deck contents " + (object) deleteDeck);
    DeckHelper.Get().Hide(true);
    CollectionManagerDisplay.Get().HideConvertTutorial();
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    if (deleteDeck)
      this.m_decksContent.DeleteDeck(taggedDeck.ID);
    DeckRuleset deckRuleset = CollectionManager.Get().GetDeckRuleset();
    CollectionManagerDisplay.Get().m_pageManager.HideNonDeckTemplateTabs(false, false);
    bool flag = true;
    IList<DeckRuleViolation> violations;
    if (deckRuleset != null)
      flag = deckRuleset.IsDeckValid(taggedDeck, out violations);
    else
      violations = (IList<DeckRuleViolation>) new List<DeckRuleViolation>();
    if (!flag && !deleteDeck)
    {
      this.PopupInvalidDeckConfirmation(violations);
    }
    else
    {
      if (!taggedDeck.IsWild && flag && (CollectionManager.Get().ShouldShowWildToStandardTutorial(false) && UserAttentionManager.CanShowAttentionGrabber(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS, "CollectionDeckTray.OnBackOutOfDeckContentsImpl:ShowSetRotationTutorial")))
      {
        Options.Get().SetBool(Option.NEEDS_TO_MAKE_STANDARD_DECK, false);
        Options.Get().SetLong(Option.LAST_CUSTOM_DECK_CHOSEN, taggedDeck.ID);
        Notification popupText = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS, OverlayUI.Get().GetRelativePosition(this.m_doneButton.transform.position, (Camera) null, (Transform) null, 0.0f) + (!(bool) UniversalInputManager.UsePhoneUI ? new Vector3(-30.8f, 0.0f, 17.8f) : new Vector3(-56.5f, 0.0f, 35f)), NotificationManager.NOTIFICATITON_WORLD_SCALE, GameStrings.Get("GLUE_COLLECTION_TUTORIAL16"), false);
        popupText.ShowPopUpArrow(Notification.PopUpArrowDirection.RightDown);
        popupText.PulseReminderEveryXSeconds(3f);
        UserAttentionManager.StopBlocking(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS);
        this.m_doneButton.GetComponentInChildren<HighlightState>().ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
      }
      this.SaveCurrentDeckAndEnterDeckListMode();
    }
    return true;
  }

  private void PopupInvalidDeckConfirmation(IList<DeckRuleViolation> violations)
  {
    HashSet<DeckRule.RuleType> ruleTypeSet = new HashSet<DeckRule.RuleType>()
    {
      DeckRule.RuleType.IS_NOT_ROTATED,
      DeckRule.RuleType.DECK_SIZE,
      DeckRule.RuleType.PLAYER_OWNS_EACH_COPY
    };
    bool flag = false;
    string str = string.Empty;
    for (int index = 0; index < violations.Count; ++index)
    {
      DeckRuleViolation violation = violations[index];
      if (ruleTypeSet.Contains(violation.Rule.Type))
      {
        flag = true;
        str = str + violation.DisplayError + "\n";
      }
    }
    CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.CARDS, (CollectionManagerDisplay.ViewModeData) null);
    AlertPopup.PopupInfo info;
    if (flag)
      info = new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_COLLECTION_DECK_INVALID_POPUP_HEADER"),
        m_text = str + "\n" + GameStrings.Get("GLUE_COLLECTION_DECK_RULE_FINISH_AUTOMATICALLY"),
        m_cancelText = GameStrings.Get("GLUE_COLLECTION_DECK_SAVE_ANYWAY"),
        m_confirmText = GameStrings.Get("GLUE_COLLECTION_DECK_FINISH_FOR_ME"),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
        m_responseCallback = (AlertPopup.ResponseCallback) ((response, userData) =>
        {
          if (response == AlertPopup.Response.CANCEL)
            this.SaveCurrentDeckAndEnterDeckListMode();
          else
            this.StartCoroutine(this.FinishMyDeckPress());
        })
      };
    else
      info = new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_COLLECTION_DECK_INVALID_POPUP_HEADER"),
        m_text = str,
        m_okText = GameStrings.Get("GLOBAL_OKAY"),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_responseCallback = (AlertPopup.ResponseCallback) ((response, userData) => this.SaveCurrentDeckAndEnterDeckListMode())
      };
    DialogManager.Get().ShowPopup(info);
  }

  private bool OnBackOutOfCollectionScreen()
  {
    NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL16"), 0.0f);
    this.m_doneButton.GetComponentInChildren<HighlightState>().ChangeState(ActorStateType.HIGHLIGHT_OFF);
    if (this.GetCurrentContentType() != CollectionDeckTray.DeckContentTypes.INVALID && !this.GetCurrentContent().IsModeActive() || this.IsShowingDeckContents() && SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL)
      return false;
    AnimationUtil.DelayedActivate(this.gameObject, 0.25f, false);
    CollectionManagerDisplay.Get().Exit();
    return true;
  }

  private void SaveCurrentDeckAndEnterDeckListMode()
  {
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    if (taggedDeck != null)
      taggedDeck.SendChanges();
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
    {
      if ((UnityEngine.Object) TavernBrawlDisplay.Get() != (UnityEngine.Object) null)
        TavernBrawlDisplay.Get().BackFromDeckEdit(true);
      this.m_cardsContent.UpdateCardList(true, (Actor) null);
    }
    else
    {
      this.SetTrayMode(CollectionDeckTray.DeckContentTypes.Decks);
      CollectionManager.Get().DoneEditing();
      this.UpdateDoneButtonText();
      if (!((UnityEngine.Object) CollectionManagerDisplay.Get() != (UnityEngine.Object) null))
        return;
      CollectionManagerDisplay.Get().OnDoneEditingDeck();
    }
  }

  [DebuggerHidden]
  private IEnumerator FinishMyDeckPress()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionDeckTray.\u003CFinishMyDeckPress\u003Ec__Iterator35()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void PopulateDeck(IEnumerable<DeckMaker.DeckFill> fillCards)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionDeckTray.\u003CPopulateDeck\u003Ec__AnonStorey381 deckCAnonStorey381 = new CollectionDeckTray.\u003CPopulateDeck\u003Ec__AnonStorey381();
    // ISSUE: reference to a compiler-generated field
    deckCAnonStorey381.fillCards = fillCards;
    // ISSUE: reference to a compiler-generated field
    deckCAnonStorey381.\u003C\u003Ef__this = this;
    AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
    info.m_headerText = GameStrings.Get("GLUE_COLLECTION_DECK_PASTE_POPUP_HEADER");
    info.m_text = GameStrings.Format("GLUE_COLLECTION_DECK_PASTE_POPUP_MESSAGE");
    info.m_showAlertIcon = true;
    info.m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL;
    // ISSUE: reference to a compiler-generated method
    AlertPopup.ResponseCallback responseCallback = new AlertPopup.ResponseCallback(deckCAnonStorey381.\u003C\u003Em__B2);
    info.m_responseCallback = responseCallback;
    DialogManager.Get().ShowPopup(info);
  }

  [DebuggerHidden]
  private IEnumerator AutoAddCardsWithTiming(IEnumerable<DeckMaker.DeckFill> fillCards, DeckRuleset deckRuleset)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionDeckTray.\u003CAutoAddCardsWithTiming\u003Ec__Iterator36()
    {
      deckRuleset = deckRuleset,
      fillCards = fillCards,
      \u003C\u0024\u003EdeckRuleset = deckRuleset,
      \u003C\u0024\u003EfillCards = fillCards,
      \u003C\u003Ef__this = this
    };
  }

  public void UpdateDoneButtonText()
  {
    bool flag1 = !CollectionManager.Get().IsInEditMode() || CollectionManagerDisplay.Get().GetViewMode() == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE;
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
    {
      TavernBrawlDisplay tavernBrawlDisplay = TavernBrawlDisplay.Get();
      flag1 = (UnityEngine.Object) tavernBrawlDisplay != (UnityEngine.Object) null && !tavernBrawlDisplay.IsInDeckEditMode() && !(bool) UniversalInputManager.UsePhoneUI;
    }
    bool flag2 = (UnityEngine.Object) this.m_backArrow != (UnityEngine.Object) null;
    if (flag1)
    {
      this.m_doneButton.SetText(!flag2 ? GameStrings.Get("GLOBAL_BACK") : string.Empty);
      if (!flag2)
        return;
      this.m_backArrow.gameObject.SetActive(true);
    }
    else
    {
      this.m_doneButton.SetText(GameStrings.Get("GLOBAL_DONE"));
      if (!flag2)
        return;
      this.m_backArrow.gameObject.SetActive(false);
    }
  }

  public bool IsUpdatingTrayMode()
  {
    return this.m_updatingTrayMode;
  }

  [DebuggerHidden]
  private IEnumerator UpdateTrayMode()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionDeckTray.\u003CUpdateTrayMode\u003Ec__Iterator37()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void TryEnableScrollbar()
  {
    if ((UnityEngine.Object) this.m_scrollbar == (UnityEngine.Object) null || (UnityEngine.Object) this.GetCurrentContent() == (UnityEngine.Object) null)
      return;
    CollectionDeckTray.DeckContentScroll deckContentScroll = this.m_scrollables.Find((Predicate<CollectionDeckTray.DeckContentScroll>) (type => this.GetCurrentContentType() == type.m_contentType));
    if (deckContentScroll == null || (UnityEngine.Object) deckContentScroll.m_scrollObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "No scrollable object defined.");
    }
    else
    {
      this.m_scrollbar.m_ScrollObject = deckContentScroll.m_scrollObject;
      this.m_scrollbar.ResetScrollStartPosition(deckContentScroll.GetStartPosition());
      if (deckContentScroll.m_saveScrollPosition)
        this.m_scrollbar.SetScrollSnap(deckContentScroll.GetCurrentScroll(), true);
      this.m_scrollbar.EnableIfNeeded();
    }
  }

  public void SaveScrollbarPosition(CollectionDeckTray.DeckContentTypes contentType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    CollectionDeckTray.DeckContentScroll deckContentScroll = this.m_scrollables.Find(new Predicate<CollectionDeckTray.DeckContentScroll>(new CollectionDeckTray.\u003CSaveScrollbarPosition\u003Ec__AnonStorey382()
    {
      contentType = contentType
    }.\u003C\u003Em__B4));
    if (deckContentScroll == null || !deckContentScroll.m_saveScrollPosition)
      return;
    deckContentScroll.SaveCurrentScroll(this.m_scrollbar.GetScroll());
  }

  private void TryDisableScrollbar()
  {
    if ((UnityEngine.Object) this.m_scrollbar == (UnityEngine.Object) null || (UnityEngine.Object) this.m_scrollbar.m_ScrollObject == (UnityEngine.Object) null)
      return;
    this.m_scrollbar.Enable(false);
    this.m_scrollbar.m_ScrollObject = (GameObject) null;
  }

  private void HideUnseenDeckTrays()
  {
    if (this.m_currentContent != CollectionDeckTray.DeckContentTypes.Decks)
      return;
    this.m_decksContent.HideTraySectionsNotInBounds(this.m_scrollbar.m_ScrollBounds.bounds);
  }

  private void OnTouchScrollStarted()
  {
    if (!((UnityEngine.Object) this.m_deckBigCard != (UnityEngine.Object) null))
      return;
    this.m_deckBigCard.ForceHide();
  }

  private void OnTouchScrollEnded()
  {
  }

  private void OnCardTilePress(DeckTrayDeckTileVisual cardTile)
  {
    if (UniversalInputManager.Get().IsTouchMode())
    {
      this.ShowDeckBigCard(cardTile, 0.2f);
    }
    else
    {
      if (!((UnityEngine.Object) CollectionInputMgr.Get() != (UnityEngine.Object) null))
        return;
      this.HideDeckBigCard(cardTile, false);
    }
  }

  private void OnCardTileTap(DeckTrayDeckTileVisual cardTile)
  {
    if (!UniversalInputManager.Get().IsTouchMode() || CollectionManagerDisplay.Get().GetViewMode() == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE || CollectionManager.Get().GetEditedDeck().IsValidSlot(cardTile.GetSlot()))
      return;
    this.m_cardsContent.ShowDeckHelper(cardTile, false, true);
  }

  private void OnCardTileHeld(DeckTrayDeckTileVisual cardTile)
  {
    if (!((UnityEngine.Object) CollectionInputMgr.Get() != (UnityEngine.Object) null) || TavernBrawlDisplay.IsTavernBrawlViewing() || (CollectionManagerDisplay.Get().GetViewMode() == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE || !CollectionInputMgr.Get().GrabCard(cardTile)) || !((UnityEngine.Object) this.m_deckBigCard != (UnityEngine.Object) null))
      return;
    this.HideDeckBigCard(cardTile, true);
  }

  private void OnCardTileOver(DeckTrayDeckTileVisual cardTile)
  {
    if (UniversalInputManager.Get().IsTouchMode() || !((UnityEngine.Object) CollectionInputMgr.Get() == (UnityEngine.Object) null) && CollectionInputMgr.Get().HasHeldCard())
      return;
    this.ShowDeckBigCard(cardTile, 0.0f);
  }

  private void OnCardTileOut(DeckTrayDeckTileVisual cardTile)
  {
    this.HideDeckBigCard(cardTile, false);
  }

  public void OnCardTileRelease(DeckTrayDeckTileVisual cardTile)
  {
    this.RemoveCardTile(cardTile, false);
  }

  public void RemoveCardTile(DeckTrayDeckTileVisual cardTile, bool removeAllCopies = false)
  {
    if (CollectionManagerDisplay.Get().GetViewMode() == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
      return;
    CollectionDeck editedDeck = CollectionManager.Get().GetEditedDeck();
    if (UniversalInputManager.Get().IsTouchMode())
      this.HideDeckBigCard(cardTile, false);
    else if (!editedDeck.IsValidSlot(cardTile.GetSlot()))
    {
      this.m_cardsContent.ShowDeckHelper(cardTile, false, true);
    }
    else
    {
      if ((UnityEngine.Object) CollectionInputMgr.Get() == (UnityEngine.Object) null || TavernBrawlDisplay.IsTavernBrawlViewing())
        return;
      CollectionDeckTileActor actor = cardTile.GetActor();
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(actor.GetSpell(SpellType.SUMMON_IN).gameObject);
      gameObject.transform.position = actor.transform.position + new Vector3(-2f, 0.0f, 0.0f);
      gameObject.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
      gameObject.GetComponent<Spell>().ActivateState(SpellStateType.BIRTH);
      this.StartCoroutine(this.DestroyAfterSeconds(gameObject));
      if ((UnityEngine.Object) CollectionDeckTray.Get() != (UnityEngine.Object) null)
        CollectionDeckTray.Get().RemoveCard(cardTile.GetCardID(), cardTile.GetPremium(), editedDeck.IsValidSlot(cardTile.GetSlot()));
      iTween.MoveTo(gameObject, new Vector3(gameObject.transform.position.x - 10f, gameObject.transform.position.y + 10f, gameObject.transform.position.z), 4f);
      SoundManager.Get().LoadAndPlay("collection_manager_card_remove_from_deck_instant", this.gameObject);
    }
  }

  private void OnCardCountUpdated(int cardCount)
  {
    string str1 = GameStrings.Get("GLUE_DECK_TRAY_CARD_COUNT_LABEL");
    string str2 = GameStrings.Format("GLUE_DECK_TRAY_COUNT", (object) cardCount, (object) CollectionManager.Get().GetDeckSize());
    this.m_countLabelText.Text = str1;
    this.m_countText.Text = str2;
  }

  private void OnDeckCountUpdated(int deckCount)
  {
    string str1;
    string str2;
    if (AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
    {
      str1 = GameStrings.Get("GLUE_DECK_TRAY_DECK_COUNT_LABEL");
      str2 = GameStrings.Format("GLUE_DECK_TRAY_COUNT", (object) deckCount, (object) 18);
    }
    else
    {
      str1 = GameStrings.Get("GLUE_DECK_TRAY_HEROES_UNLOCKED_COUNT_LABEL");
      str2 = GameStrings.Format("GLUE_DECK_TRAY_COUNT", (object) deckCount, (object) 9);
    }
    this.m_countLabelText.Text = str1;
    this.m_countText.Text = str2;
  }

  private void OnBusyWithDeck(bool busy)
  {
    if ((UnityEngine.Object) this.m_inputBlocker == (UnityEngine.Object) null)
      Log.All.PrintError("If this happens, please notify JMac and copy your stack trace to bug 21743!");
    else
      this.m_inputBlocker.SetActive(busy);
  }

  private void OnTaggedDeckChanged(CollectionManager.DeckTag tag, CollectionDeck newDeck, CollectionDeck oldDeck, object callbackData)
  {
    bool isNewDeck = callbackData != null && callbackData is bool && (bool) callbackData;
    using (Map<CollectionDeckTray.DeckContentTypes, DeckTrayContent>.Enumerator enumerator = this.m_contents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.OnTaggedDeckChanged(tag, newDeck, oldDeck, isNewDeck);
    }
  }

  [DebuggerHidden]
  private IEnumerator DestroyAfterSeconds(GameObject go)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionDeckTray.\u003CDestroyAfterSeconds\u003Ec__Iterator38()
    {
      go = go,
      \u003C\u0024\u003Ego = go
    };
  }

  private void ShowDeckBigCard(DeckTrayDeckTileVisual cardTile, float delay = 0)
  {
    CollectionDeckTileActor actor = cardTile.GetActor();
    if ((UnityEngine.Object) this.m_deckBigCard == (UnityEngine.Object) null)
      return;
    CollectionDeck editedDeck = CollectionManager.Get().GetEditedDeck();
    EntityDef entityDef = actor.GetEntityDef();
    CardDef cardDef = DefLoader.Get().GetCardDef(entityDef.GetCardId(), (CardPortraitQuality) null);
    GhostCard.Type ghostTypeFromSlot = GhostCard.GetGhostTypeFromSlot(editedDeck, cardTile.GetSlot());
    this.m_deckBigCard.Show(entityDef, actor.GetPremium(), cardDef, actor.gameObject.transform.position, ghostTypeFromSlot, delay);
    if (UniversalInputManager.Get().IsTouchMode())
      cardTile.SetHighlight(true);
    if (!((UnityEngine.Object) CollectionManagerDisplay.Get().m_deckTemplateCardReplacePopup != (UnityEngine.Object) null))
      return;
    CollectionManagerDisplay.Get().m_deckTemplateCardReplacePopup.Shrink(0.1f);
  }

  private void HideDeckBigCard(DeckTrayDeckTileVisual cardTile, bool force = false)
  {
    CollectionDeckTileActor actor = cardTile.GetActor();
    if (!((UnityEngine.Object) this.m_deckBigCard != (UnityEngine.Object) null))
      return;
    if (force)
      this.m_deckBigCard.ForceHide();
    else
      this.m_deckBigCard.Hide(actor.GetEntityDef(), actor.GetPremium());
    if (UniversalInputManager.Get().IsTouchMode())
      cardTile.SetHighlight(false);
    if (!((UnityEngine.Object) CollectionManagerDisplay.Get().m_deckTemplateCardReplacePopup != (UnityEngine.Object) null))
      return;
    CollectionManagerDisplay.Get().m_deckTemplateCardReplacePopup.Unshrink(0.1f);
  }

  private void OnCMViewModeChanged(CollectionManagerDisplay.ViewMode prevMode, CollectionManagerDisplay.ViewMode mode, CollectionManagerDisplay.ViewModeData userdata, bool triggerResponse)
  {
    CollectionDeckTray.DeckContentTypes typeFromViewMode = this.GetContentTypeFromViewMode(mode);
    this.m_cardsContent.ShowFakeDeck(mode == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE);
    if (!triggerResponse)
      return;
    CollectionDeckTray.Get().m_decksContent.UpdateDeckName((string) null);
    if (this.m_currentContent == CollectionDeckTray.DeckContentTypes.Decks)
      return;
    this.SetTrayMode(typeFromViewMode);
  }

  private CollectionDeckTray.DeckContentTypes GetContentTypeFromViewMode(CollectionManagerDisplay.ViewMode viewMode)
  {
    switch (viewMode)
    {
      case CollectionManagerDisplay.ViewMode.HERO_SKINS:
        return CollectionDeckTray.DeckContentTypes.HeroSkin;
      case CollectionManagerDisplay.ViewMode.CARD_BACKS:
        return CollectionDeckTray.DeckContentTypes.CardBack;
      default:
        return CollectionDeckTray.DeckContentTypes.Cards;
    }
  }

  private void FireModeSwitchedEvent()
  {
    foreach (CollectionDeckTray.ModeSwitched modeSwitched in this.m_modeSwitchedListeners.ToArray())
      modeSwitched();
  }

  private void OnHeroAssigned(string cardID)
  {
    this.m_decksContent.UpdateEditingDeckBoxVisual(cardID);
  }

  public enum DeckContentTypes
  {
    Decks,
    Cards,
    HeroSkin,
    CardBack,
    INVALID,
  }

  [Serializable]
  public class DeckContentScroll
  {
    public CollectionDeckTray.DeckContentTypes m_contentType;
    public GameObject m_scrollObject;
    public bool m_saveScrollPosition;
    private Vector3 m_startPos;
    private float m_currentScroll;

    public void SaveStartPosition()
    {
      if (!((UnityEngine.Object) this.m_scrollObject != (UnityEngine.Object) null))
        return;
      this.m_startPos = this.m_scrollObject.transform.localPosition;
    }

    public Vector3 GetStartPosition()
    {
      return this.m_startPos;
    }

    public Vector3 GetCurrentPosition()
    {
      if ((UnityEngine.Object) this.m_scrollObject != (UnityEngine.Object) null)
        return this.m_scrollObject.transform.localPosition;
      return Vector3.zero;
    }

    public void SaveCurrentScroll(float scroll)
    {
      this.m_currentScroll = scroll;
    }

    public float GetCurrentScroll()
    {
      return this.m_currentScroll;
    }
  }

  public delegate void ModeSwitched();
}
