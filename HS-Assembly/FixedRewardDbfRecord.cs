﻿// Decompiled with JetBrains decompiler
// Type: FixedRewardDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FixedRewardDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private string m_Type;
  [SerializeField]
  private int m_CardId;
  [SerializeField]
  private int m_CardPremium;
  [SerializeField]
  private int m_CardBackId;
  [SerializeField]
  private int m_MetaActionId;
  [SerializeField]
  private ulong m_MetaActionFlags;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("TYPE", "reward type")]
  public string Type
  {
    get
    {
      return this.m_Type;
    }
  }

  [DbfField("CARD_ID", "CARD.ID for reward. 0 means ignore.")]
  public int CardId
  {
    get
    {
      return this.m_CardId;
    }
  }

  [DbfField("CARD_PREMIUM", "card premium for reward. 0 means ignore.")]
  public int CardPremium
  {
    get
    {
      return this.m_CardPremium;
    }
  }

  [DbfField("CARD_BACK_ID", "CARD_BACK.ID for reward. 0 means ignore.")]
  public int CardBackId
  {
    get
    {
      return this.m_CardBackId;
    }
  }

  [DbfField("META_ACTION_ID", "meta-action FIXED_REWARD_ACTION.ID granted for this reward. 0 means ignore.")]
  public int MetaActionId
  {
    get
    {
      return this.m_MetaActionId;
    }
  }

  [DbfField("META_ACTION_FLAGS", "meta-action flags granted for this reward. 0 means ignore. flag values are generally any Power-of-2 starting with 1, then 2, 4, 8, 16, etc. theoretically a FIXED_REWARD can grant multiple flags.")]
  public ulong MetaActionFlags
  {
    get
    {
      return this.m_MetaActionFlags;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    FixedRewardDbfAsset fixedRewardDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (FixedRewardDbfAsset)) as FixedRewardDbfAsset;
    if ((UnityEngine.Object) fixedRewardDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("FixedRewardDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < fixedRewardDbfAsset.Records.Count; ++index)
      fixedRewardDbfAsset.Records[index].StripUnusedLocales();
    records = (object) fixedRewardDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetType(string v)
  {
    this.m_Type = v;
  }

  public void SetCardId(int v)
  {
    this.m_CardId = v;
  }

  public void SetCardPremium(int v)
  {
    this.m_CardPremium = v;
  }

  public void SetCardBackId(int v)
  {
    this.m_CardBackId = v;
  }

  public void SetMetaActionId(int v)
  {
    this.m_MetaActionId = v;
  }

  public void SetMetaActionFlags(ulong v)
  {
    this.m_MetaActionFlags = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardDbfRecord.\u003C\u003Ef__switch\u0024map3E == null)
      {
        // ISSUE: reference to a compiler-generated field
        FixedRewardDbfRecord.\u003C\u003Ef__switch\u0024map3E = new Dictionary<string, int>(8)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TYPE",
            2
          },
          {
            "CARD_ID",
            3
          },
          {
            "CARD_PREMIUM",
            4
          },
          {
            "CARD_BACK_ID",
            5
          },
          {
            "META_ACTION_ID",
            6
          },
          {
            "META_ACTION_FLAGS",
            7
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardDbfRecord.\u003C\u003Ef__switch\u0024map3E.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Type;
          case 3:
            return (object) this.CardId;
          case 4:
            return (object) this.CardPremium;
          case 5:
            return (object) this.CardBackId;
          case 6:
            return (object) this.MetaActionId;
          case 7:
            return (object) this.MetaActionFlags;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (FixedRewardDbfRecord.\u003C\u003Ef__switch\u0024map3F == null)
    {
      // ISSUE: reference to a compiler-generated field
      FixedRewardDbfRecord.\u003C\u003Ef__switch\u0024map3F = new Dictionary<string, int>(8)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "TYPE",
          2
        },
        {
          "CARD_ID",
          3
        },
        {
          "CARD_PREMIUM",
          4
        },
        {
          "CARD_BACK_ID",
          5
        },
        {
          "META_ACTION_ID",
          6
        },
        {
          "META_ACTION_FLAGS",
          7
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!FixedRewardDbfRecord.\u003C\u003Ef__switch\u0024map3F.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetType((string) val);
        break;
      case 3:
        this.SetCardId((int) val);
        break;
      case 4:
        this.SetCardPremium((int) val);
        break;
      case 5:
        this.SetCardBackId((int) val);
        break;
      case 6:
        this.SetMetaActionId((int) val);
        break;
      case 7:
        this.SetMetaActionFlags((ulong) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardDbfRecord.\u003C\u003Ef__switch\u0024map40 == null)
      {
        // ISSUE: reference to a compiler-generated field
        FixedRewardDbfRecord.\u003C\u003Ef__switch\u0024map40 = new Dictionary<string, int>(8)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TYPE",
            2
          },
          {
            "CARD_ID",
            3
          },
          {
            "CARD_PREMIUM",
            4
          },
          {
            "CARD_BACK_ID",
            5
          },
          {
            "META_ACTION_ID",
            6
          },
          {
            "META_ACTION_FLAGS",
            7
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardDbfRecord.\u003C\u003Ef__switch\u0024map40.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (string);
          case 3:
            return typeof (int);
          case 4:
            return typeof (int);
          case 5:
            return typeof (int);
          case 6:
            return typeof (int);
          case 7:
            return typeof (ulong);
        }
      }
    }
    return (System.Type) null;
  }
}
