﻿// Decompiled with JetBrains decompiler
// Type: ActivateToggle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ActivateToggle : MonoBehaviour
{
  public GameObject obj;
  private bool onoff;

  private void Start()
  {
    if (!((Object) this.obj != (Object) null))
      return;
    this.onoff = this.obj.activeSelf;
  }

  public void ToggleActive()
  {
    this.onoff = !this.onoff;
    if (!((Object) this.obj != (Object) null))
      return;
    this.obj.SetActive(this.onoff);
  }

  public void ToggleOn()
  {
    this.onoff = true;
    if (!((Object) this.obj != (Object) null))
      return;
    this.obj.SetActive(this.onoff);
  }

  public void ToggleOff()
  {
    this.onoff = false;
    if (!((Object) this.obj != (Object) null))
      return;
    this.obj.SetActive(this.onoff);
  }
}
