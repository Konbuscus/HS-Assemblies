﻿// Decompiled with JetBrains decompiler
// Type: ChestVisual
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class ChestVisual
{
  public Mesh m_chestMesh;
  public Material m_chestMaterial;
  public GameObject m_glowMesh;
  public Material m_glowMaterial;
  public string chestName;
  public string levelUpAnimation;
  public string chestChangeAnimation;
}
