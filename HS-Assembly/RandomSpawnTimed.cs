﻿// Decompiled with JetBrains decompiler
// Type: RandomSpawnTimed
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RandomSpawnTimed : MonoBehaviour
{
  public float minWaitTime = 5f;
  public float maxWaitTime = 15f;
  public float killX = 10f;
  public float killZ = 10f;
  public GameObject objPrefab;
  private List<GameObject> listOfObjs;

  private void Start()
  {
    this.listOfObjs = new List<GameObject>();
    this.StartCoroutine(this.RespawnLoop());
  }

  [DebuggerHidden]
  private IEnumerator RespawnLoop()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RandomSpawnTimed.\u003CRespawnLoop\u003Ec__Iterator33E() { \u003C\u003Ef__this = this };
  }

  private void Update()
  {
    for (int index = 0; index < this.listOfObjs.Count; ++index)
    {
      if ((double) Mathf.Abs(this.listOfObjs[index].transform.position.x - this.gameObject.transform.position.x) > (double) this.killX || (double) Mathf.Abs(this.listOfObjs[index].transform.position.z - this.gameObject.transform.position.z) > (double) this.killZ)
      {
        GameObject listOfObj = this.listOfObjs[index];
        this.listOfObjs.Remove(this.listOfObjs[index]);
        Object.Destroy((Object) listOfObj);
        --index;
      }
    }
  }
}
