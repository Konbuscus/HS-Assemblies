﻿// Decompiled with JetBrains decompiler
// Type: HighlightState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using HutongGames.PlayMaker;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class HighlightState : MonoBehaviour
{
  private readonly string HIGHLIGHT_SHADER_NAME = "Custom/Selection/Highlight";
  public Vector3 m_HistoryTranslation = new Vector3(0.0f, -0.1f, 0.0f);
  private string m_BirthTransition = "None";
  private string m_SecondBirthTransition = "None";
  private string m_IdleTransition = "None";
  private string m_DeathTransition = "None";
  private const string FSM_BIRTH_STATE = "Birth";
  private const string FSM_IDLE_STATE = "Idle";
  private const string FSM_DEATH_STATE = "Death";
  private const string FSM_BIRTHTRANSITION_STATE = "BirthTransition";
  private const string FSM_IDLETRANSITION_STATE = "IdleTransition";
  private const string FSM_DEATHTRANSITION_STATE = "DeathTransition";
  public GameObject m_RenderPlane;
  public HighlightStateType m_highlightType;
  public Texture2D m_StaticSilouetteTexture;
  public Texture2D m_StaticSilouetteTextureUnique;
  public Texture2D m_MultiClassStaticSilouetteTexture;
  public Texture2D m_MultiClassStaticSilouetteTextureUnique;
  public int m_RenderQueue;
  public List<HighlightRenderState> m_HighlightStates;
  public ActorStateType m_debugState;
  protected ActorStateType m_PreviousState;
  protected ActorStateType m_CurrentState;
  protected PlayMakerFSM m_FSM;
  private string m_sendEvent;
  private bool m_isDirty;
  private bool m_forceRerender;
  private bool m_Hide;
  private bool m_VisibilityState;
  private float m_seed;
  private Material m_Material;

  public ActorStateType CurrentState
  {
    get
    {
      return this.m_CurrentState;
    }
  }

  private void Awake()
  {
    if ((Object) this.m_RenderPlane == (Object) null)
    {
      if (!Application.isEditor)
        UnityEngine.Debug.LogError((object) "m_RenderPlane is null!");
      this.enabled = false;
    }
    else
    {
      this.m_RenderPlane.GetComponent<Renderer>().enabled = false;
      this.m_VisibilityState = false;
      this.m_FSM = this.m_RenderPlane.GetComponent<PlayMakerFSM>();
    }
    if ((Object) this.m_FSM != (Object) null)
      this.m_FSM.enabled = true;
    if (this.m_highlightType == HighlightStateType.NONE)
    {
      Transform parent = this.transform.parent;
      if ((Object) parent != (Object) null)
        this.m_highlightType = !(bool) ((Object) parent.GetComponent<ActorStateMgr>()) ? HighlightStateType.HIGHLIGHT : HighlightStateType.CARD;
    }
    if (this.m_highlightType == HighlightStateType.NONE)
    {
      UnityEngine.Debug.LogError((object) "m_highlightType is not set!");
      this.enabled = false;
    }
    this.Setup();
  }

  private void Update()
  {
    if (this.m_debugState != ActorStateType.NONE)
    {
      this.ChangeState(this.m_debugState);
      this.ForceUpdate();
    }
    if (this.m_Hide)
    {
      if ((Object) this.m_RenderPlane == (Object) null)
        return;
      this.m_RenderPlane.GetComponent<Renderer>().enabled = false;
    }
    else
    {
      if (!this.m_isDirty || (Object) this.m_RenderPlane == (Object) null || !this.m_RenderPlane.GetComponent<Renderer>().enabled)
        return;
      this.UpdateSilouette();
      this.m_isDirty = false;
    }
  }

  private void OnApplicationFocus(bool state)
  {
    this.m_isDirty = true;
    this.m_forceRerender = true;
  }

  protected void OnDestroy()
  {
    if (!(bool) ((Object) this.m_Material))
      return;
    Object.Destroy((Object) this.m_Material);
  }

  private void LateUpdate()
  {
  }

  private void Setup()
  {
    this.m_seed = Random.value;
    this.m_CurrentState = ActorStateType.CARD_IDLE;
    this.m_RenderPlane.GetComponent<Renderer>().enabled = false;
    this.m_VisibilityState = false;
    if ((Object) this.m_Material == (Object) null)
    {
      Shader shader = ShaderUtils.FindShader(this.HIGHLIGHT_SHADER_NAME);
      if (!(bool) ((Object) shader))
      {
        UnityEngine.Debug.LogError((object) ("Failed to load Highlight Shader: " + this.HIGHLIGHT_SHADER_NAME));
        this.enabled = false;
      }
      this.m_Material = new Material(shader);
      this.m_RenderPlane.GetComponent<Renderer>().sharedMaterial = this.m_Material;
    }
    this.m_RenderPlane.GetComponent<Renderer>().sharedMaterial = this.m_Material;
  }

  public void Show()
  {
    this.m_Hide = false;
    if ((Object) this.m_RenderPlane == (Object) null || !this.m_VisibilityState || this.m_RenderPlane.GetComponent<Renderer>().enabled)
      return;
    this.m_RenderPlane.GetComponent<Renderer>().enabled = true;
  }

  public void Hide()
  {
    this.m_Hide = true;
    if ((Object) this.m_RenderPlane == (Object) null)
      return;
    this.m_RenderPlane.GetComponent<Renderer>().enabled = false;
  }

  public void SetDirty()
  {
    this.m_isDirty = true;
  }

  public void ForceUpdate()
  {
    this.m_isDirty = true;
    this.m_forceRerender = true;
  }

  public void ContinuousUpdate(float updateTime)
  {
    this.StartCoroutine(this.ContinuousSilouetteRender(updateTime));
  }

  public bool IsReady()
  {
    return (Object) this.m_Material != (Object) null;
  }

  public bool ChangeState(ActorStateType stateType)
  {
    if (stateType == this.m_CurrentState)
      return true;
    this.m_PreviousState = this.m_CurrentState;
    this.m_CurrentState = stateType;
    if (stateType == ActorStateType.NONE)
    {
      this.m_RenderPlane.GetComponent<Renderer>().enabled = false;
      this.m_VisibilityState = false;
      return true;
    }
    if (stateType == ActorStateType.CARD_IDLE || stateType == ActorStateType.HIGHLIGHT_OFF)
    {
      if ((Object) this.m_FSM == (Object) null)
      {
        this.m_RenderPlane.GetComponent<Renderer>().enabled = false;
        this.m_VisibilityState = false;
        return true;
      }
      this.m_DeathTransition = this.m_PreviousState.ToString();
      this.SendDataToPlaymaker();
      this.SendPlaymakerDeathEvent();
      return true;
    }
    using (List<HighlightRenderState>.Enumerator enumerator = this.m_HighlightStates.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HighlightRenderState current = enumerator.Current;
        if (current.m_StateType == stateType)
        {
          if ((Object) current.m_Material != (Object) null && (Object) this.m_Material != (Object) null)
          {
            this.m_Material.CopyPropertiesFromMaterial(current.m_Material);
            this.m_RenderPlane.GetComponent<Renderer>().sharedMaterial = this.m_Material;
            this.m_RenderPlane.GetComponent<Renderer>().sharedMaterial.SetFloat("_Seed", this.m_seed);
            bool flag = this.RenderSilouette();
            if (stateType == ActorStateType.CARD_HISTORY)
              this.transform.localPosition = this.m_HistoryTranslation;
            else
              this.transform.localPosition = current.m_Offset;
            if ((Object) this.m_FSM == (Object) null)
            {
              if (!this.m_Hide)
                this.m_RenderPlane.GetComponent<Renderer>().enabled = true;
              this.m_VisibilityState = true;
            }
            else
            {
              this.m_BirthTransition = stateType.ToString();
              this.m_SecondBirthTransition = this.m_PreviousState.ToString();
              this.m_IdleTransition = this.m_BirthTransition;
              this.SendDataToPlaymaker();
              this.SendPlaymakerBirthEvent();
            }
            return flag;
          }
          this.m_RenderPlane.GetComponent<Renderer>().enabled = false;
          this.m_VisibilityState = false;
          return true;
        }
      }
    }
    if (this.m_highlightType == HighlightStateType.CARD)
      this.m_CurrentState = ActorStateType.CARD_IDLE;
    else if (this.m_highlightType == HighlightStateType.HIGHLIGHT)
      this.m_CurrentState = ActorStateType.HIGHLIGHT_OFF;
    this.m_DeathTransition = this.m_PreviousState.ToString();
    this.SendDataToPlaymaker();
    this.SendPlaymakerDeathEvent();
    this.m_RenderPlane.GetComponent<Renderer>().enabled = false;
    this.m_VisibilityState = false;
    return false;
  }

  protected void UpdateSilouette()
  {
    this.RenderSilouette();
  }

  private bool RenderSilouette()
  {
    this.m_isDirty = false;
    if ((Object) this.m_StaticSilouetteTexture != (Object) null)
    {
      Texture2D texture2D = this.m_StaticSilouetteTexture;
      Actor componentInParents = SceneUtils.FindComponentInParents<Actor>(this.gameObject);
      if ((Object) componentInParents != (Object) null)
      {
        if (componentInParents.IsElite() && (Object) this.m_StaticSilouetteTextureUnique != (Object) null)
          texture2D = this.m_StaticSilouetteTextureUnique;
        if (componentInParents.IsMultiClass() && (Object) this.m_MultiClassStaticSilouetteTexture != (Object) null)
          texture2D = this.m_MultiClassStaticSilouetteTexture;
        if (componentInParents.IsElite() && componentInParents.IsMultiClass() && (Object) this.m_MultiClassStaticSilouetteTextureUnique != (Object) null)
          texture2D = this.m_MultiClassStaticSilouetteTextureUnique;
      }
      this.m_RenderPlane.GetComponent<Renderer>().sharedMaterial.mainTexture = (Texture) texture2D;
      this.m_RenderPlane.GetComponent<Renderer>().sharedMaterial.renderQueue = 3000 + this.m_RenderQueue;
      this.m_forceRerender = false;
      return true;
    }
    HighlightRender component = this.m_RenderPlane.GetComponent<HighlightRender>();
    if ((Object) component == (Object) null)
    {
      UnityEngine.Debug.LogError((object) "Unable to find HighlightRender component on m_RenderPlane");
      return false;
    }
    if (component.enabled)
    {
      component.CreateSilhouetteTexture(this.m_forceRerender);
      this.m_RenderPlane.GetComponent<Renderer>().sharedMaterial.mainTexture = (Texture) component.SilhouetteTexture;
      this.m_RenderPlane.GetComponent<Renderer>().sharedMaterial.renderQueue = 3000 + this.m_RenderQueue;
    }
    this.m_forceRerender = false;
    return true;
  }

  [DebuggerHidden]
  private IEnumerator ContinuousSilouetteRender(float renderTime)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HighlightState.\u003CContinuousSilouetteRender\u003Ec__Iterator335() { renderTime = renderTime, \u003C\u0024\u003ErenderTime = renderTime, \u003C\u003Ef__this = this };
  }

  private void SendDataToPlaymaker()
  {
    if ((Object) this.m_FSM == (Object) null)
      return;
    FsmMaterial fsmMaterial = this.m_FSM.FsmVariables.GetFsmMaterial("HighlightMaterial");
    if (fsmMaterial != null)
      fsmMaterial.Value = this.m_RenderPlane.GetComponent<Renderer>().sharedMaterial;
    FsmString fsmString1 = this.m_FSM.FsmVariables.GetFsmString("CurrentState");
    if (fsmString1 != null)
      fsmString1.Value = this.m_CurrentState.ToString();
    FsmString fsmString2 = this.m_FSM.FsmVariables.GetFsmString("PreviousState");
    if (fsmString2 == null)
      return;
    fsmString2.Value = this.m_PreviousState.ToString();
  }

  private void SendPlaymakerDeathEvent()
  {
    if ((Object) this.m_FSM == (Object) null)
      return;
    FsmString fsmString = this.m_FSM.FsmVariables.GetFsmString("DeathTransition");
    if (fsmString != null)
      fsmString.Value = this.m_DeathTransition;
    this.m_FSM.SendEvent("Death");
  }

  private void SendPlaymakerBirthEvent()
  {
    if ((Object) this.m_FSM == (Object) null)
      return;
    FsmString fsmString1 = this.m_FSM.FsmVariables.GetFsmString("BirthTransition");
    if (fsmString1 != null)
      fsmString1.Value = this.m_BirthTransition;
    FsmString fsmString2 = this.m_FSM.FsmVariables.GetFsmString("SecondBirthTransition");
    if (fsmString2 != null)
      fsmString2.Value = this.m_SecondBirthTransition;
    FsmString fsmString3 = this.m_FSM.FsmVariables.GetFsmString("IdleTransition");
    if (fsmString3 != null)
      fsmString3.Value = this.m_IdleTransition;
    this.m_FSM.SendEvent("Birth");
  }

  public void OnActionFinished()
  {
  }
}
