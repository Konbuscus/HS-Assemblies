﻿// Decompiled with JetBrains decompiler
// Type: AnimSpeed
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class AnimSpeed : MonoBehaviour
{
  public float animspeed = 1f;

  private void Awake()
  {
    IEnumerator enumerator = this.GetComponent<Animation>().GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
        ((AnimationState) enumerator.Current).speed = this.animspeed;
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
  }
}
