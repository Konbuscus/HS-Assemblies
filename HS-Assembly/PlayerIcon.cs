﻿// Decompiled with JetBrains decompiler
// Type: PlayerIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using UnityEngine;

public class PlayerIcon : PegUIElement
{
  public GameObject m_OfflineIcon;
  public GameObject m_OnlineIcon;
  public PlayerPortrait m_OnlinePortrait;
  private bool m_hidden;
  private BnetPlayer m_player;

  public void Hide()
  {
    this.m_hidden = true;
    this.gameObject.SetActive(false);
  }

  public void Show()
  {
    this.m_hidden = false;
    this.gameObject.SetActive(true);
  }

  public BnetPlayer GetPlayer()
  {
    return this.m_player;
  }

  public bool SetPlayer(BnetPlayer player)
  {
    if (this.m_player == player)
      return false;
    this.m_player = player;
    this.UpdateIcon();
    return true;
  }

  public void UpdateIcon()
  {
    if (this.m_player.IsOnline() && (bgs.FourCC) this.m_player.GetBestProgramId() != (bgs.FourCC) BnetProgramId.PHOENIX)
    {
      if (!this.m_hidden)
        this.gameObject.SetActive(true);
      this.m_OnlinePortrait.SetProgramId(this.m_player.GetBestProgramId());
    }
    else
      this.gameObject.SetActive(false);
  }
}
