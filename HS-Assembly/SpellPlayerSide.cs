﻿// Decompiled with JetBrains decompiler
// Type: SpellPlayerSide
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum SpellPlayerSide
{
  NEUTRAL,
  FRIENDLY,
  OPPONENT,
  SOURCE,
  TARGET,
  BOTH,
}
