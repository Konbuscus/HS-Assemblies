﻿// Decompiled with JetBrains decompiler
// Type: SeasonDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SeasonDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private DbfLocValue m_SeasonStartName;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("NAME", "")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("SEASON_START_NAME", "")]
  public DbfLocValue SeasonStartName
  {
    get
    {
      return this.m_SeasonStartName;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    SeasonDbfAsset seasonDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (SeasonDbfAsset)) as SeasonDbfAsset;
    if ((UnityEngine.Object) seasonDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("SeasonDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < seasonDbfAsset.Records.Count; ++index)
      seasonDbfAsset.Records[index].StripUnusedLocales();
    records = (object) seasonDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Name.StripUnusedLocales();
    this.m_SeasonStartName.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetSeasonStartName(DbfLocValue v)
  {
    this.m_SeasonStartName = v;
    v.SetDebugInfo(this.ID, "SEASON_START_NAME");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (SeasonDbfRecord.\u003C\u003Ef__switch\u0024map65 == null)
      {
        // ISSUE: reference to a compiler-generated field
        SeasonDbfRecord.\u003C\u003Ef__switch\u0024map65 = new Dictionary<string, int>(4)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "NAME",
            2
          },
          {
            "SEASON_START_NAME",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (SeasonDbfRecord.\u003C\u003Ef__switch\u0024map65.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Name;
          case 3:
            return (object) this.SeasonStartName;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (SeasonDbfRecord.\u003C\u003Ef__switch\u0024map66 == null)
    {
      // ISSUE: reference to a compiler-generated field
      SeasonDbfRecord.\u003C\u003Ef__switch\u0024map66 = new Dictionary<string, int>(4)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "NAME",
          2
        },
        {
          "SEASON_START_NAME",
          3
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!SeasonDbfRecord.\u003C\u003Ef__switch\u0024map66.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetName((DbfLocValue) val);
        break;
      case 3:
        this.SetSeasonStartName((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (SeasonDbfRecord.\u003C\u003Ef__switch\u0024map67 == null)
      {
        // ISSUE: reference to a compiler-generated field
        SeasonDbfRecord.\u003C\u003Ef__switch\u0024map67 = new Dictionary<string, int>(4)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "NAME",
            2
          },
          {
            "SEASON_START_NAME",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (SeasonDbfRecord.\u003C\u003Ef__switch\u0024map67.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (DbfLocValue);
          case 3:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
