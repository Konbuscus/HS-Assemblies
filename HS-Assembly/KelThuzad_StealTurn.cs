﻿// Decompiled with JetBrains decompiler
// Type: KelThuzad_StealTurn
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class KelThuzad_StealTurn : Spell
{
  public GameObject m_Lightning;

  protected override void OnAction(SpellStateType prevStateType)
  {
    this.StartCoroutine(this.SpellEffect(prevStateType));
    base.OnAction(prevStateType);
  }

  [DebuggerHidden]
  private IEnumerator SpellEffect(SpellStateType prevStateType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    KelThuzad_StealTurn.\u003CSpellEffect\u003Ec__Iterator2BD effectCIterator2Bd = new KelThuzad_StealTurn.\u003CSpellEffect\u003Ec__Iterator2BD();
    return (IEnumerator) effectCIterator2Bd;
  }
}
