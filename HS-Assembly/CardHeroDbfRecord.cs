﻿// Decompiled with JetBrains decompiler
// Type: CardHeroDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CardHeroDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_CardId;
  [SerializeField]
  private int m_CardBackId;
  [SerializeField]
  private DbfLocValue m_Description;
  [SerializeField]
  private DbfLocValue m_StoreDesc;
  [SerializeField]
  private DbfLocValue m_StoreDescPhone;
  [SerializeField]
  private string m_StoreBannerPrefab;
  [SerializeField]
  private string m_StoreBackgroundTexture;
  [SerializeField]
  private int m_StoreSortOrder;
  [SerializeField]
  private DbfLocValue m_PurchaseCompleteMsg;

  [DbfField("CARD_ID", "the ASSET.CARD.ID")]
  public int CardId
  {
    get
    {
      return this.m_CardId;
    }
  }

  [DbfField("CARD_BACK_ID", "ASSET.CARD_BACK.ID")]
  public int CardBackId
  {
    get
    {
      return this.m_CardBackId;
    }
  }

  [DbfField("DESCRIPTION", "this is the description that shows up when you look at the hero in Collection Manager heroes tab.")]
  public DbfLocValue Description
  {
    get
    {
      return this.m_Description;
    }
  }

  [DbfField("STORE_DESC", "description of the hero for purchase on store")]
  public DbfLocValue StoreDesc
  {
    get
    {
      return this.m_StoreDesc;
    }
  }

  [DbfField("STORE_DESC_PHONE", "description specifically for smaller screen phone UI")]
  public DbfLocValue StoreDescPhone
  {
    get
    {
      return this.m_StoreDescPhone;
    }
  }

  [DbfField("STORE_BANNER_PREFAB", "if non-null, a prefab to be instantiated and attached to the GeneralStoreHeroesContentDisplay at localPosition (0, 0, 0) - used for adding additional flare to the hero purchase experience. (Originally used for Khadgar promotion)")]
  public string StoreBannerPrefab
  {
    get
    {
      return this.m_StoreBannerPrefab;
    }
  }

  [DbfField("STORE_BACKGROUND_TEXTURE", "if null, uses the default purple background border around the hero frame in store. if non-null, specifies an alternative texture to be used. (Originally used for Khadgar promotion)")]
  public string StoreBackgroundTexture
  {
    get
    {
      return this.m_StoreBackgroundTexture;
    }
  }

  [DbfField("STORE_SORT_ORDER", "sort ordering when displayed in the store (ascending, as in 0 is first, then 1, etc)")]
  public int StoreSortOrder
  {
    get
    {
      return this.m_StoreSortOrder;
    }
  }

  [DbfField("PURCHASE_COMPLETE_MSG", "A 'thank you' message displayed to the player after a successful purchase. (Originally used for Khadgar promotion)")]
  public DbfLocValue PurchaseCompleteMsg
  {
    get
    {
      return this.m_PurchaseCompleteMsg;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    CardHeroDbfAsset cardHeroDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (CardHeroDbfAsset)) as CardHeroDbfAsset;
    if ((UnityEngine.Object) cardHeroDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("CardHeroDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < cardHeroDbfAsset.Records.Count; ++index)
      cardHeroDbfAsset.Records[index].StripUnusedLocales();
    records = (object) cardHeroDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Description.StripUnusedLocales();
    this.m_StoreDesc.StripUnusedLocales();
    this.m_StoreDescPhone.StripUnusedLocales();
    this.m_PurchaseCompleteMsg.StripUnusedLocales();
  }

  public void SetCardId(int v)
  {
    this.m_CardId = v;
  }

  public void SetCardBackId(int v)
  {
    this.m_CardBackId = v;
  }

  public void SetDescription(DbfLocValue v)
  {
    this.m_Description = v;
    v.SetDebugInfo(this.ID, "DESCRIPTION");
  }

  public void SetStoreDesc(DbfLocValue v)
  {
    this.m_StoreDesc = v;
    v.SetDebugInfo(this.ID, "STORE_DESC");
  }

  public void SetStoreDescPhone(DbfLocValue v)
  {
    this.m_StoreDescPhone = v;
    v.SetDebugInfo(this.ID, "STORE_DESC_PHONE");
  }

  public void SetStoreBannerPrefab(string v)
  {
    this.m_StoreBannerPrefab = v;
  }

  public void SetStoreBackgroundTexture(string v)
  {
    this.m_StoreBackgroundTexture = v;
  }

  public void SetStoreSortOrder(int v)
  {
    this.m_StoreSortOrder = v;
  }

  public void SetPurchaseCompleteMsg(DbfLocValue v)
  {
    this.m_PurchaseCompleteMsg = v;
    v.SetDebugInfo(this.ID, "PURCHASE_COMPLETE_MSG");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CardHeroDbfRecord.\u003C\u003Ef__switch\u0024map23 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CardHeroDbfRecord.\u003C\u003Ef__switch\u0024map23 = new Dictionary<string, int>(10)
        {
          {
            "ID",
            0
          },
          {
            "CARD_ID",
            1
          },
          {
            "CARD_BACK_ID",
            2
          },
          {
            "DESCRIPTION",
            3
          },
          {
            "STORE_DESC",
            4
          },
          {
            "STORE_DESC_PHONE",
            5
          },
          {
            "STORE_BANNER_PREFAB",
            6
          },
          {
            "STORE_BACKGROUND_TEXTURE",
            7
          },
          {
            "STORE_SORT_ORDER",
            8
          },
          {
            "PURCHASE_COMPLETE_MSG",
            9
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CardHeroDbfRecord.\u003C\u003Ef__switch\u0024map23.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.CardId;
          case 2:
            return (object) this.CardBackId;
          case 3:
            return (object) this.Description;
          case 4:
            return (object) this.StoreDesc;
          case 5:
            return (object) this.StoreDescPhone;
          case 6:
            return (object) this.StoreBannerPrefab;
          case 7:
            return (object) this.StoreBackgroundTexture;
          case 8:
            return (object) this.StoreSortOrder;
          case 9:
            return (object) this.PurchaseCompleteMsg;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (CardHeroDbfRecord.\u003C\u003Ef__switch\u0024map24 == null)
    {
      // ISSUE: reference to a compiler-generated field
      CardHeroDbfRecord.\u003C\u003Ef__switch\u0024map24 = new Dictionary<string, int>(10)
      {
        {
          "ID",
          0
        },
        {
          "CARD_ID",
          1
        },
        {
          "CARD_BACK_ID",
          2
        },
        {
          "DESCRIPTION",
          3
        },
        {
          "STORE_DESC",
          4
        },
        {
          "STORE_DESC_PHONE",
          5
        },
        {
          "STORE_BANNER_PREFAB",
          6
        },
        {
          "STORE_BACKGROUND_TEXTURE",
          7
        },
        {
          "STORE_SORT_ORDER",
          8
        },
        {
          "PURCHASE_COMPLETE_MSG",
          9
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!CardHeroDbfRecord.\u003C\u003Ef__switch\u0024map24.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetCardId((int) val);
        break;
      case 2:
        this.SetCardBackId((int) val);
        break;
      case 3:
        this.SetDescription((DbfLocValue) val);
        break;
      case 4:
        this.SetStoreDesc((DbfLocValue) val);
        break;
      case 5:
        this.SetStoreDescPhone((DbfLocValue) val);
        break;
      case 6:
        this.SetStoreBannerPrefab((string) val);
        break;
      case 7:
        this.SetStoreBackgroundTexture((string) val);
        break;
      case 8:
        this.SetStoreSortOrder((int) val);
        break;
      case 9:
        this.SetPurchaseCompleteMsg((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CardHeroDbfRecord.\u003C\u003Ef__switch\u0024map25 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CardHeroDbfRecord.\u003C\u003Ef__switch\u0024map25 = new Dictionary<string, int>(10)
        {
          {
            "ID",
            0
          },
          {
            "CARD_ID",
            1
          },
          {
            "CARD_BACK_ID",
            2
          },
          {
            "DESCRIPTION",
            3
          },
          {
            "STORE_DESC",
            4
          },
          {
            "STORE_DESC_PHONE",
            5
          },
          {
            "STORE_BANNER_PREFAB",
            6
          },
          {
            "STORE_BACKGROUND_TEXTURE",
            7
          },
          {
            "STORE_SORT_ORDER",
            8
          },
          {
            "PURCHASE_COMPLETE_MSG",
            9
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CardHeroDbfRecord.\u003C\u003Ef__switch\u0024map25.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (int);
          case 3:
            return typeof (DbfLocValue);
          case 4:
            return typeof (DbfLocValue);
          case 5:
            return typeof (DbfLocValue);
          case 6:
            return typeof (string);
          case 7:
            return typeof (string);
          case 8:
            return typeof (int);
          case 9:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
