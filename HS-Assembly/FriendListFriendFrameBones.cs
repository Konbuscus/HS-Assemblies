﻿// Decompiled with JetBrains decompiler
// Type: FriendListFriendFrameBones
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class FriendListFriendFrameBones
{
  public Transform m_PlayerNameText;
  public Transform m_StatusText;
  public Transform m_RightComponent;
  public Transform m_Medal;
}
