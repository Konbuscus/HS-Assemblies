﻿// Decompiled with JetBrains decompiler
// Type: GameToastMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameToastMgr : MonoBehaviour
{
  private List<GameToast> m_toasts = new List<GameToast>();
  private List<GameToastMgr.QuestProgressToastShownListener> m_questProgressToastShownListeners = new List<GameToastMgr.QuestProgressToastShownListener>();
  private const float FADE_IN_TIME = 0.25f;
  private const float FADE_OUT_TIME = 0.5f;
  private const float HOLD_TIME = 4f;
  private const int MAX_DAYS_TO_SHOW_TIME_REMAINING = 5;
  public QuestProgressToast m_questProgressToastPrefab;
  public SeasonTimeRemainingToast m_seasonTimeRemainingToastPrefab;
  private static GameToastMgr s_instance;
  private int m_numToastsShown;

  private void Awake()
  {
    GameToastMgr.s_instance = this;
  }

  private void OnDestroy()
  {
    GameToastMgr.s_instance = (GameToastMgr) null;
  }

  public static GameToastMgr Get()
  {
    return GameToastMgr.s_instance;
  }

  public bool RegisterQuestProgressToastShownListener(GameToastMgr.QuestProgressToastShownCallback callback)
  {
    if (callback == null)
      return false;
    GameToastMgr.QuestProgressToastShownListener toastShownListener = new GameToastMgr.QuestProgressToastShownListener();
    toastShownListener.SetCallback(callback);
    toastShownListener.SetUserData((object) null);
    if (this.m_questProgressToastShownListeners.Contains(toastShownListener))
      return false;
    this.m_questProgressToastShownListeners.Add(toastShownListener);
    return true;
  }

  public bool RemoveQuestProgressToastShownListener(GameToastMgr.QuestProgressToastShownCallback callback)
  {
    if (callback == null)
      return false;
    GameToastMgr.QuestProgressToastShownListener toastShownListener = new GameToastMgr.QuestProgressToastShownListener();
    toastShownListener.SetCallback(callback);
    toastShownListener.SetUserData((object) null);
    return this.m_questProgressToastShownListeners.Remove(toastShownListener);
  }

  private void FireAllQuestProgressListeners(int achieveId, int progress)
  {
    foreach (GameToastMgr.QuestProgressToastShownListener toastShownListener in this.m_questProgressToastShownListeners.ToArray())
      toastShownListener.Fire(achieveId);
  }

  private bool AddToast(UserAttentionBlocker blocker, GameToast toast, string callerName)
  {
    if (!UserAttentionManager.CanShowAttentionGrabber(blocker, "GameToastMgr." + callerName))
      return false;
    toast.transform.parent = BnetBar.Get().m_questProgressToastBone.transform;
    toast.transform.localRotation = Quaternion.Euler(new Vector3(90f, 180f, 0.0f));
    toast.transform.localScale = new Vector3(450f, 1f, 450f);
    toast.transform.localPosition = Vector3.zero;
    this.m_toasts.Add(toast);
    RenderUtils.SetAlpha(toast.gameObject, 0.0f);
    this.UpdateToastPositions();
    Hashtable args = iTween.Hash((object) "amount", (object) 1f, (object) "time", (object) 0.25f, (object) "delay", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) "FadeOutToast", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) toast);
    iTween.FadeTo(toast.gameObject, args);
    return true;
  }

  public bool AreToastsActive()
  {
    return this.m_toasts.Count > 0;
  }

  private void FadeOutToast(GameToast toast)
  {
    Hashtable args = iTween.Hash((object) "amount", (object) 0.0f, (object) "delay", (object) 4f, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) "DeactivateToast", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) toast);
    iTween.FadeTo(toast.gameObject, args);
  }

  private void DeactivateToast(GameToast toast)
  {
    toast.gameObject.SetActive(false);
    this.m_toasts.Remove(toast);
    this.UpdateToastPositions();
  }

  private void UpdateToastPositions()
  {
    int num = 0;
    using (List<GameToast>.Enumerator enumerator = this.m_toasts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameToast current = enumerator.Current;
        if (num > 0)
          TransformUtil.SetPoint(current.gameObject, Anchor.BOTTOM, this.m_toasts[num - 1].gameObject, Anchor.TOP, new Vector3(0.0f, 5f, 0.0f));
        ++num;
      }
    }
  }

  public void UpdateQuestProgressToasts()
  {
    if (!UserAttentionManager.CanShowAttentionGrabber("GameToastMgr.UpdateQuestProgressToasts"))
      return;
    using (List<Achievement>.Enumerator enumerator = AchieveManager.Get().GetNewlyProgressedQuests().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        this.AddQuestProgressToast(current.ID, current.Name, current.Description, current.Progress, current.MaxProgress);
        current.AckCurrentProgressAndRewardNotices(true);
      }
    }
  }

  public void AddQuestProgressToast(int achieveId, string questName, string questDescription, int progress, int maxProgress)
  {
    if (progress == maxProgress)
      return;
    QuestProgressToast questProgressToast = UnityEngine.Object.Instantiate<QuestProgressToast>(this.m_questProgressToastPrefab);
    questProgressToast.UpdateDisplay(questName, questDescription, progress, maxProgress);
    if (!this.AddToast(UserAttentionBlocker.NONE, (GameToast) questProgressToast, "AddQuestProgressToast"))
      return;
    this.FireAllQuestProgressListeners(achieveId, progress);
  }

  public void AddSeasonTimeRemainingToast()
  {
    if (!UserAttentionManager.CanShowAttentionGrabber("GameToastMgr.AddSeasonTimeRemainingToast"))
      return;
    NetCache.NetCacheRewardProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>();
    if (netObject == null)
      return;
    int totalSeconds = (int) (DateTime.FromFileTimeUtc(netObject.SeasonEndDate) - DateTime.UtcNow).TotalSeconds;
    int num1 = totalSeconds >= 86400 ? totalSeconds / 86400 : 1;
    int num2 = Options.Get().GetInt(Option.SEASON_END_THRESHOLD);
    if (num1 == num2)
      return;
    int val = -1;
    switch (num1)
    {
      case 1:
        val = 1;
        break;
      case 2:
        val = 2;
        break;
      case 3:
        val = 3;
        break;
      case 4:
        val = 4;
        break;
      case 5:
        val = 5;
        break;
      case 10:
        val = 10;
        break;
    }
    if (val == -1)
      return;
    Options.Get().SetInt(Option.SEASON_END_THRESHOLD, val);
    if (num1 < 5 && (SceneMgr.Get().GetMode() != SceneMgr.Mode.TOURNAMENT || !Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE)))
      return;
    string str;
    if (totalSeconds < 86400)
      str = TimeUtils.GetElapsedTimeString(totalSeconds, TimeUtils.SPLASHSCREEN_DATETIME_STRINGSET);
    else
      str = GameStrings.Format(TimeUtils.SPLASHSCREEN_DATETIME_STRINGSET.m_days, (object) num1);
    SeasonDbfRecord record = GameDbf.Season.GetRecord(netObject.Season);
    if (record == null)
      return;
    string name = (string) record.Name;
    SeasonTimeRemainingToast timeRemainingToast = UnityEngine.Object.Instantiate<SeasonTimeRemainingToast>(this.m_seasonTimeRemainingToastPrefab);
    timeRemainingToast.UpdateDisplay(name, GameStrings.Format("GLOBAL_REMAINING_DATETIME", (object) str));
    this.AddToast(UserAttentionBlocker.NONE, (GameToast) timeRemainingToast, "AddSeasonTimeRemainingToast");
  }

  public enum SEASON_TOAST_THRESHHOLDS
  {
    NONE = 0,
    ONE = 1,
    TWO = 2,
    THREE = 3,
    FOUR = 4,
    FIVE = 5,
    TEN = 10,
  }

  public enum TOAST_TYPE
  {
    NORMAL,
    NO_COUNT,
  }

  private class QuestProgressToastShownListener : EventListener<GameToastMgr.QuestProgressToastShownCallback>
  {
    public void Fire(int achieveId)
    {
      this.m_callback(achieveId);
    }
  }

  public delegate void QuestProgressToastShownCallback(int achieveId);
}
