﻿// Decompiled with JetBrains decompiler
// Type: CraftingModeButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CraftingModeButton : UIBButton
{
  public Vector3 m_jarJiggleRotation = new Vector3(0.0f, 30f, 0.0f);
  public GameObject m_dustBottle;
  public GameObject m_activeGlow;
  public ParticleSystem m_dustShower;
  public GameObject m_textObject;
  public MeshRenderer m_mainMesh;
  public Material m_enabledMaterial;
  public Material m_disabledMaterial;
  private bool m_isGlowEnabled;
  private bool m_showDustBottle;
  private bool m_isJiggling;

  public void ShowActiveGlow(bool show)
  {
    this.m_isGlowEnabled = show;
    this.m_activeGlow.SetActive(show);
  }

  public void ShowDustBottle(bool show)
  {
    this.m_showDustBottle = show;
    this.m_dustBottle.SetActive(show);
    if (!show)
      return;
    this.StartBottleJiggle();
  }

  private void StartBottleJiggle()
  {
    this.StopCoroutine("Jiggle");
    iTween.Stop(this.m_dustBottle.gameObject);
    this.BottleJiggle();
  }

  private void BottleJiggle()
  {
    this.StartCoroutine(this.Jiggle());
  }

  [DebuggerHidden]
  private IEnumerator Jiggle()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingModeButton.\u003CJiggle\u003Ec__Iterator4E()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void Enable(bool enabled)
  {
    this.SetEnabled(enabled);
    this.m_activeGlow.SetActive(enabled && this.m_isGlowEnabled);
    this.m_textObject.SetActive(enabled);
    this.m_dustShower.gameObject.SetActive(enabled);
    if (enabled)
      this.m_dustBottle.SetActive(this.m_showDustBottle);
    else
      this.m_dustBottle.SetActive(false);
    this.m_mainMesh.sharedMaterial = !enabled ? this.m_disabledMaterial : this.m_enabledMaterial;
  }
}
