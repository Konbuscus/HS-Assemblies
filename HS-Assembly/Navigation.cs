﻿// Decompiled with JetBrains decompiler
// Type: Navigation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Navigation
{
  private static Stack<Navigation.NavigateBackHandler> history = new Stack<Navigation.NavigateBackHandler>();

  public static bool NAVIGATION_DEBUG
  {
    get
    {
      return Vars.Key("Application.Navigation.Debug").GetBool(false);
    }
    set
    {
      VarsInternal.Get().Set("Application.Navigation.Debug", value.ToString());
    }
  }

  public static void Clear()
  {
    Navigation.history.Clear();
    if (!Navigation.NAVIGATION_DEBUG)
      return;
    Navigation.DumpStack();
  }

  public static bool GoBack()
  {
    if (Navigation.history.Count == 0 || !Navigation.CanNavigate())
      return false;
    Navigation.NavigateBackHandler t = Navigation.history.Peek();
    if (!t())
      return false;
    if (Navigation.history.Count > 0 && (MulticastDelegate) t == (MulticastDelegate) Navigation.history.Peek())
      Navigation.history.Pop();
    else if (Navigation.history.Contains(t))
      Log.All.PrintWarning("Navigation tried to remove handler and failed, but the handler exists further down the stack! Perhaps something went wrong, like a new scene added itself to the top of the stack in its Awake? Handler to remove: {0}", (object) t);
    if (Navigation.NAVIGATION_DEBUG)
      Navigation.DumpStack();
    return true;
  }

  public static void Push(Navigation.NavigateBackHandler handler)
  {
    if (handler == null)
      return;
    Navigation.history.Push(handler);
    if (!Navigation.NAVIGATION_DEBUG)
      return;
    Navigation.DumpStack();
  }

  public static void PushUnique(Navigation.NavigateBackHandler handler)
  {
    if (handler == null || Navigation.history.Contains(handler))
      return;
    Navigation.history.Push(handler);
    if (!Navigation.NAVIGATION_DEBUG)
      return;
    Navigation.DumpStack();
  }

  public static void PushIfNotOnTop(Navigation.NavigateBackHandler handler)
  {
    if (handler == null)
      return;
    if (Navigation.history.Count > 0 && (MulticastDelegate) Navigation.history.Peek() == (MulticastDelegate) handler)
    {
      if (!Navigation.NAVIGATION_DEBUG)
        return;
      Debug.LogFormat("Navigation - Did not push {0}, it already exists on the top of the stack!", (object) Navigation.StackEntryToString(handler));
    }
    else
    {
      Navigation.history.Push(handler);
      if (!Navigation.NAVIGATION_DEBUG)
        return;
      Navigation.DumpStack();
    }
  }

  public static void Pop()
  {
    if (Navigation.history.Count == 0 || !Navigation.CanNavigate())
      return;
    Navigation.history.Pop();
    if (!Navigation.NAVIGATION_DEBUG)
      return;
    Navigation.DumpStack();
  }

  public static bool RemoveHandler(Navigation.NavigateBackHandler handler)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Navigation.\u003CRemoveHandler\u003Ec__AnonStorey3C5 handlerCAnonStorey3C5 = new Navigation.\u003CRemoveHandler\u003Ec__AnonStorey3C5();
    // ISSUE: reference to a compiler-generated field
    handlerCAnonStorey3C5.handler = handler;
    if (Navigation.history.Count == 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    bool flag = Navigation.history.Contains(handlerCAnonStorey3C5.handler);
    if (flag)
    {
      // ISSUE: reference to a compiler-generated method
      Navigation.history = new Stack<Navigation.NavigateBackHandler>(Navigation.history.Where<Navigation.NavigateBackHandler>(new Func<Navigation.NavigateBackHandler, bool>(handlerCAnonStorey3C5.\u003C\u003Em__168)).Reverse<Navigation.NavigateBackHandler>());
    }
    if (Navigation.NAVIGATION_DEBUG)
      Navigation.DumpStack();
    return flag;
  }

  public static bool BackStackContainsHandler(Navigation.NavigateBackHandler handler)
  {
    return Navigation.history.Contains(handler);
  }

  public static bool BlockBackingOut()
  {
    return false;
  }

  private static bool CanNavigate()
  {
    if (GameUtils.IsAnyTransitionActive() && (UnityEngine.Object) SplashScreen.Get() != (UnityEngine.Object) null && !SplashScreen.Get().IsWebLoginCanvasActive())
      return false;
    switch (GameMgr.Get().GetFindGameState())
    {
      case FindGameState.CLIENT_STARTED:
      case FindGameState.CLIENT_CANCELED:
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
      case FindGameState.SERVER_GAME_CONNECTING:
      case FindGameState.SERVER_GAME_STARTED:
      case FindGameState.SERVER_GAME_CANCELED:
        return false;
      default:
        return true;
    }
  }

  private static string StackEntryToString(Navigation.NavigateBackHandler entry)
  {
    return string.Format("{0}.{1} Target={2}", (object) entry.Method.DeclaringType, (object) entry.Method.Name, entry == null || entry.Target == null ? (!entry.Method.IsStatic ? (object) "null" : (object) "<static>") : (object) entry.Target.ToString());
  }

  public static void DumpStack()
  {
    Debug.Log((object) string.Format("Navigation Stack Dump (count: {0})\n", (object) Navigation.history.Count));
    int num = 0;
    using (Stack<Navigation.NavigateBackHandler>.Enumerator enumerator = Navigation.history.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Navigation.NavigateBackHandler current = enumerator.Current;
        Debug.Log((object) string.Format("{0}: {1}\n", (object) num, (object) Navigation.StackEntryToString(current)));
        ++num;
      }
    }
  }

  public delegate bool NavigateBackHandler();
}
