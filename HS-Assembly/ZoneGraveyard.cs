﻿// Decompiled with JetBrains decompiler
// Type: ZoneGraveyard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class ZoneGraveyard : Zone
{
  public override bool CanAcceptTags(int controllerId, TAG_ZONE zoneTag, TAG_CARDTYPE cardType)
  {
    return base.CanAcceptTags(controllerId, zoneTag, cardType) && (cardType == TAG_CARDTYPE.MINION || cardType == TAG_CARDTYPE.WEAPON || (cardType == TAG_CARDTYPE.SPELL || cardType == TAG_CARDTYPE.HERO));
  }

  public override void UpdateLayout()
  {
    this.m_updatingLayout = true;
    if (this.IsBlockingLayout())
    {
      this.UpdateLayoutFinished();
    }
    else
    {
      for (int index = 0; index < this.m_cards.Count; ++index)
      {
        Card card = this.m_cards[index];
        if (!card.IsDoNotSort())
        {
          card.HideCard();
          card.EnableTransitioningZones(false);
        }
      }
      this.UpdateLayoutFinished();
    }
  }
}
