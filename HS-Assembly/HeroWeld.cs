﻿// Decompiled with JetBrains decompiler
// Type: HeroWeld
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class HeroWeld : MonoBehaviour
{
  private Light[] m_lights;

  public void DoAnim()
  {
    this.gameObject.SetActive(true);
    this.m_lights = this.gameObject.GetComponentsInChildren<Light>();
    foreach (Behaviour light in this.m_lights)
      light.enabled = true;
    string str = "HeroWeldIn";
    this.gameObject.GetComponent<Animation>().Stop(str);
    this.gameObject.GetComponent<Animation>().Play(str);
    this.StartCoroutine(this.DestroyWhenFinished());
  }

  [DebuggerHidden]
  private IEnumerator DestroyWhenFinished()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroWeld.\u003CDestroyWhenFinished\u003Ec__Iterator334() { \u003C\u003Ef__this = this };
  }
}
