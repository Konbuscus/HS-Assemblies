﻿// Decompiled with JetBrains decompiler
// Type: GeneralStoreHeroesContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[CustomEditClass]
public class GeneralStoreHeroesContent : GeneralStoreContent
{
  public string m_keyArtFadeAnim = "HeroSkinArt_WipeAway";
  public string m_keyArtAppearAnim = "HeroSkinArtGlowIn";
  private int m_currentCardBackPreview = -1;
  private int m_currentDisplay = -1;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_keyArtFadeSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_keyArtAppearSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_previewButtonClick;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_backgroundFlipSound;
  public GameObject m_heroEmptyDisplay;
  public GeneralStoreHeroesContentDisplay m_heroDisplay;
  public MeshRenderer m_renderQuad1;
  public GameObject m_renderToTexture1;
  public MeshRenderer m_renderQuad2;
  public GameObject m_renderToTexture2;
  private GameObject m_currentSelectedHeroBannerFlare;
  private CollectionHeroDef m_currentHeroDef;
  private CardHeroDbfRecord m_currentDbfRecord;
  private GeneralStoreHeroesContentDisplay m_heroDisplay1;
  private GeneralStoreHeroesContentDisplay m_heroDisplay2;

  private void Awake()
  {
    this.m_heroDisplay1 = this.m_heroDisplay;
    this.m_heroDisplay2 = UnityEngine.Object.Instantiate<GeneralStoreHeroesContentDisplay>(this.m_heroDisplay);
    this.m_heroDisplay2.transform.parent = this.m_heroDisplay1.transform.parent;
    this.m_heroDisplay2.transform.localPosition = this.m_heroDisplay1.transform.localPosition;
    this.m_heroDisplay2.transform.localScale = this.m_heroDisplay1.transform.localScale;
    this.m_heroDisplay2.transform.localRotation = this.m_heroDisplay1.transform.localRotation;
    this.m_heroDisplay2.gameObject.SetActive(false);
    this.m_heroDisplay1.SetParent(this);
    this.m_heroDisplay2.SetParent(this);
    this.m_heroDisplay1.SetKeyArtRenderer(this.m_renderQuad1);
    this.m_heroDisplay2.SetKeyArtRenderer(this.m_renderQuad2);
    this.m_renderToTexture1.GetComponent<RenderToTexture>().m_RenderToObject = this.m_heroDisplay1.m_renderArtQuad;
    this.m_renderToTexture2.GetComponent<RenderToTexture>().m_RenderToObject = this.m_heroDisplay2.m_renderArtQuad;
  }

  public override bool AnimateEntranceEnd()
  {
    this.m_parentStore.HideAccentTexture();
    return true;
  }

  public CardHeroDbfRecord GetSelectedHero()
  {
    return this.m_currentDbfRecord;
  }

  public void SelectHero(CardHeroDbfRecord cardHeroDbfRecord, bool animate = true)
  {
    if (cardHeroDbfRecord == this.m_currentDbfRecord)
      return;
    this.m_currentDbfRecord = cardHeroDbfRecord;
    Network.Bundle heroBundle = (Network.Bundle) null;
    StoreManager.Get().GetHeroBundleByCardDbId(cardHeroDbfRecord.CardId, out heroBundle);
    this.SetCurrentMoneyBundle(heroBundle, false);
    if ((UnityEngine.Object) this.m_currentHeroDef != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_currentHeroDef.gameObject);
      this.m_currentHeroDef = (CollectionHeroDef) null;
    }
    this.m_currentCardBackPreview = cardHeroDbfRecord.CardBackId;
    this.m_currentHeroDef = GameUtils.LoadGameObjectWithComponent<CollectionHeroDef>(DefLoader.Get().GetCardDef(this.m_currentDbfRecord.CardId).m_CollectionHeroDefPath);
    bool purchased = !StoreManager.Get().CanBuyBundle(heroBundle);
    this.AnimateAndUpdateDisplays(cardHeroDbfRecord, this.m_currentCardBackPreview, this.m_currentHeroDef, purchased);
    this.PlayHeroMusic();
    this.UpdateHeroDescription(purchased);
  }

  public void PlayCurrentHeroPurchaseEmote()
  {
    GeneralStoreHeroesContentDisplay currentDisplay = this.GetCurrentDisplay();
    if (!((UnityEngine.Object) currentDisplay != (UnityEngine.Object) null))
      return;
    currentDisplay.PlayPurchaseEmote();
  }

  public override void StoreShown(bool isCurrent)
  {
    if (this.m_currentDisplay == -1 || !isCurrent)
      return;
    this.PlayHeroMusic();
    this.ResetHeroPreview();
  }

  public override void PreStoreFlipIn()
  {
    this.ResetHeroPreview();
  }

  public override void PostStoreFlipIn(bool animatedFlipIn)
  {
    this.PlayHeroMusic();
  }

  public override void TryBuyWithMoney(Network.Bundle bundle, GeneralStoreContent.BuyEvent successBuyCB, GeneralStoreContent.BuyEvent failedBuyCB)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStoreHeroesContent.\u003CTryBuyWithMoney\u003Ec__AnonStorey3ED moneyCAnonStorey3Ed = new GeneralStoreHeroesContent.\u003CTryBuyWithMoney\u003Ec__AnonStorey3ED();
    // ISSUE: reference to a compiler-generated field
    moneyCAnonStorey3Ed.failedBuyCB = failedBuyCB;
    // ISSUE: reference to a compiler-generated field
    moneyCAnonStorey3Ed.\u003C\u003Ef__this = this;
    SpecialEventManager specialEventManager = SpecialEventManager.Get();
    if (!specialEventManager.IsEventActive(bundle.ProductEvent, false))
    {
      string key = "GLUE_STORE_PRODUCT_NOT_AVAILABLE_TEXT";
      if (specialEventManager.HasEventEnded(bundle.ProductEvent))
        key = "GLUE_STORE_PRODUCT_NOT_AVAILABLE_TEXT_HAS_ENDED";
      else if (specialEventManager.GetEventLocalStartTime(bundle.ProductEvent).HasValue && !specialEventManager.HasEventStarted(bundle.ProductEvent))
        key = "GLUE_STORE_PRODUCT_NOT_AVAILABLE_TEXT_NOT_YET_STARTED";
      AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
      info.m_headerText = GameStrings.Get("GLUE_STORE_PRODUCT_NOT_AVAILABLE_HEADER");
      info.m_text = GameStrings.Get(key);
      info.m_showAlertIcon = true;
      info.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
      // ISSUE: reference to a compiler-generated method
      info.m_responseCallback = new AlertPopup.ResponseCallback(moneyCAnonStorey3Ed.\u003C\u003Em__1B7);
      this.m_parentStore.ActivateCover(true);
      DialogManager.Get().ShowPopup(info);
    }
    else
    {
      if (successBuyCB == null)
        return;
      successBuyCB();
    }
  }

  protected override void OnRefresh()
  {
    Network.Bundle heroBundle = (Network.Bundle) null;
    if (this.m_currentDbfRecord != null)
      StoreManager.Get().GetHeroBundleByCardDbId(this.m_currentDbfRecord.CardId, out heroBundle);
    bool flag = !StoreManager.Get().CanBuyBundle(heroBundle);
    this.GetCurrentDisplay().ShowPurchasedCheckmark(flag);
    this.SetCurrentMoneyBundle(heroBundle, true);
    this.UpdateHeroDescription(flag);
  }

  public override bool IsPurchaseDisabled()
  {
    return this.m_currentDisplay == -1;
  }

  public override string GetMoneyDisplayOwnedText()
  {
    return GameStrings.Get("GLUE_STORE_HERO_BUTTON_COST_OWNED_TEXT");
  }

  private GameObject GetCurrentDisplayContainer()
  {
    return this.GetCurrentDisplay().gameObject;
  }

  private GameObject GetNextDisplayContainer()
  {
    if ((this.m_currentDisplay + 1) % 2 == 0)
      return this.m_heroDisplay1.gameObject;
    return this.m_heroDisplay2.gameObject;
  }

  private GeneralStoreHeroesContentDisplay GetCurrentDisplay()
  {
    if (this.m_currentDisplay == 0)
      return this.m_heroDisplay1;
    return this.m_heroDisplay2;
  }

  private void AnimateAndUpdateDisplays(CardHeroDbfRecord cardHeroDbfRecord, int cardBackIdx, CollectionHeroDef heroDef, bool purchased)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStoreHeroesContent.\u003CAnimateAndUpdateDisplays\u003Ec__AnonStorey3EE displaysCAnonStorey3Ee = new GeneralStoreHeroesContent.\u003CAnimateAndUpdateDisplays\u003Ec__AnonStorey3EE();
    // ISSUE: reference to a compiler-generated field
    displaysCAnonStorey3Ee.currDisplay = (GameObject) null;
    if (this.m_currentDisplay == -1)
    {
      this.m_currentDisplay = 1;
      // ISSUE: reference to a compiler-generated field
      displaysCAnonStorey3Ee.currDisplay = this.m_heroEmptyDisplay;
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      displaysCAnonStorey3Ee.currDisplay = this.GetCurrentDisplayContainer();
    }
    GameObject displayContainer = this.GetNextDisplayContainer();
    GeneralStoreHeroesContentDisplay currentDisplay1 = this.GetCurrentDisplay();
    this.m_currentDisplay = (this.m_currentDisplay + 1) % 2;
    // ISSUE: reference to a compiler-generated field
    displaysCAnonStorey3Ee.currDisplay.transform.localRotation = Quaternion.identity;
    displayContainer.transform.localEulerAngles = new Vector3(180f, 0.0f, 0.0f);
    displayContainer.SetActive(true);
    // ISSUE: reference to a compiler-generated field
    iTween.StopByName(displaysCAnonStorey3Ee.currDisplay, "ROTATION_TWEEN");
    iTween.StopByName(displayContainer, "ROTATION_TWEEN");
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    iTween.RotateBy(displaysCAnonStorey3Ee.currDisplay, iTween.Hash((object) "amount", (object) new Vector3(0.5f, 0.0f, 0.0f), (object) "time", (object) 0.5f, (object) "name", (object) "ROTATION_TWEEN", (object) "oncomplete", (object) new Action<object>(displaysCAnonStorey3Ee.\u003C\u003Em__1B8)));
    if ((UnityEngine.Object) this.m_currentSelectedHeroBannerFlare != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_currentSelectedHeroBannerFlare);
      this.m_currentSelectedHeroBannerFlare = (GameObject) null;
    }
    if (this.m_currentDbfRecord != null && !string.IsNullOrEmpty(this.m_currentDbfRecord.StoreBannerPrefab))
    {
      this.m_currentSelectedHeroBannerFlare = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(this.m_currentDbfRecord.StoreBannerPrefab), true, false);
      if ((UnityEngine.Object) this.m_currentSelectedHeroBannerFlare != (UnityEngine.Object) null)
      {
        GameUtils.SetParent(this.m_currentSelectedHeroBannerFlare, displayContainer, false);
        this.m_currentSelectedHeroBannerFlare.transform.localPosition = Vector3.zero;
        this.m_currentSelectedHeroBannerFlare.transform.localRotation = Quaternion.identity;
        this.m_currentSelectedHeroBannerFlare.gameObject.SetActive(true);
      }
    }
    iTween.RotateBy(displayContainer, iTween.Hash((object) "amount", (object) new Vector3(0.5f, 0.0f, 0.0f), (object) "time", (object) 0.5f, (object) "name", (object) "ROTATION_TWEEN"));
    if (!string.IsNullOrEmpty(this.m_backgroundFlipSound))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_backgroundFlipSound));
    GeneralStoreHeroesContentDisplay currentDisplay2 = this.GetCurrentDisplay();
    currentDisplay2.UpdateFrame(cardHeroDbfRecord, cardBackIdx, heroDef);
    currentDisplay2.ShowPurchasedCheckmark(purchased);
    currentDisplay2.ResetPreview();
    currentDisplay1.ResetPreview();
  }

  private void ResetHeroPreview()
  {
    this.GetCurrentDisplay().ResetPreview();
  }

  private void PlayHeroMusic()
  {
    if (!((UnityEngine.Object) this.m_currentHeroDef == (UnityEngine.Object) null) && this.m_currentHeroDef.m_heroPlaylist != MusicPlaylistType.Invalid && MusicManager.Get().StartPlaylist(this.m_currentHeroDef.m_heroPlaylist))
      return;
    this.m_parentStore.ResumePreviousMusicPlaylist();
  }

  private void UpdateHeroDescription(bool purchased)
  {
    if (this.m_currentDisplay == -1 || this.m_currentDbfRecord == null)
    {
      this.m_parentStore.SetChooseDescription(GameStrings.Get("GLUE_STORE_CHOOSE_HERO"));
    }
    else
    {
      string warning = !StoreManager.Get().IsKoreanCustomer() ? string.Empty : GameStrings.Get("GLUE_STORE_SUMMARY_KOREAN_AGREEMENT_HERO");
      this.m_parentStore.SetDescription(string.Empty, this.GetHeroDescriptionString(), warning);
    }
    this.m_parentStore.HideAccentTexture();
  }

  private string GetHeroDescriptionString()
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return (string) this.m_currentDbfRecord.StoreDesc;
    return (string) this.m_currentDbfRecord.StoreDescPhone;
  }
}
