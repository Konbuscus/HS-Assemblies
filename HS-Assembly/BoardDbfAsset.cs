﻿// Decompiled with JetBrains decompiler
// Type: BoardDbfAsset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class BoardDbfAsset : ScriptableObject
{
  public List<BoardDbfRecord> Records = new List<BoardDbfRecord>();
}
