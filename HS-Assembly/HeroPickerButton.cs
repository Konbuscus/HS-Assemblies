﻿// Decompiled with JetBrains decompiler
// Type: HeroPickerButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HeroPickerButton : PegUIElement
{
  private static readonly Color BASIC_SET_COLOR_IN_PROGRESS = new Color(0.97f, 0.82f, 0.22f);
  public List<Material> CLASS_MATERIALS = new List<Material>();
  private bool m_isDeckValid = true;
  public GameObject m_heroClassIcon;
  public UberText m_classLabel;
  public UberText m_setProgressLabel;
  public GameObject m_labelGradient;
  public GameObject m_buttonFrame;
  public TAG_CLASS m_heroClass;
  public HeroPickerButtonBones m_bones;
  public GameObject m_invalidDeckX;
  private FullDef m_fullDef;
  private bool m_isSelected;
  private HighlightState m_highlightState;
  private bool m_locked;
  private long m_preconDeckID;
  private HeroPickerButton.UnlockedCallback m_unlockedCallback;
  private TAG_PREMIUM m_premium;
  private float? m_seed;

  public void SetPreconDeckID(long preconDeckID)
  {
    this.m_preconDeckID = preconDeckID;
  }

  public long GetPreconDeckID()
  {
    return this.m_preconDeckID;
  }

  public bool IsDeckValid()
  {
    return this.m_isDeckValid;
  }

  public void SetIsDeckValid(bool isValid)
  {
    if (this.m_isDeckValid == isValid)
      return;
    this.m_isDeckValid = isValid;
    this.m_invalidDeckX.SetActive(!this.m_isDeckValid);
  }

  public void UpdateDisplay(FullDef def, TAG_PREMIUM premium)
  {
    this.m_heroClass = def.GetEntityDef().GetClass();
    this.SetFullDef(def);
    this.SetClassname(GameStrings.GetClassName(this.m_heroClass));
    this.SetClassIcon(this.GetClassIconMaterial(this.m_heroClass));
    this.SetBasicSetProgress(this.m_heroClass);
    this.SetPremium(premium);
  }

  public void SetClassIcon(Material mat)
  {
    this.m_heroClassIcon.GetComponent<Renderer>().material = mat;
    this.m_heroClassIcon.GetComponent<Renderer>().material.renderQueue = 3007;
  }

  public void SetClassname(string s)
  {
    this.m_classLabel.Text = s;
  }

  public void SetFullDef(FullDef def)
  {
    this.m_fullDef = def;
    this.UpdatePortrait();
  }

  public FullDef GetFullDef()
  {
    return this.m_fullDef;
  }

  public void SetSelected(bool isSelected)
  {
    this.m_isSelected = isSelected;
    if (isSelected)
      this.Lower();
    else
      this.Raise();
  }

  public bool IsSelected()
  {
    return this.m_isSelected;
  }

  public void SetBasicSetProgress(TAG_CLASS classTag)
  {
    int basicCardsIown = CollectionManager.Get().GetBasicCardsIOwn(classTag);
    int num = 20;
    if (basicCardsIown == num)
    {
      this.m_classLabel.transform.position = this.m_bones.m_classLabelOneLine.position;
      this.m_labelGradient.transform.parent = this.m_bones.m_gradientOneLine;
      this.m_labelGradient.transform.localPosition = Vector3.zero;
      this.m_labelGradient.transform.localScale = Vector3.one;
      this.m_setProgressLabel.gameObject.SetActive(false);
    }
    else
    {
      this.m_classLabel.transform.position = this.m_bones.m_classLabelTwoLine.position;
      this.m_labelGradient.transform.parent = this.m_bones.m_gradientTwoLine;
      this.m_setProgressLabel.Text = GameStrings.Format(!(bool) UniversalInputManager.UsePhoneUI ? "GLUE_BASIC_SET_PROGRESS" : "GLUE_BASIC_SET_PROGRESS_PHONE", (object) basicCardsIown, (object) num);
      this.m_labelGradient.transform.localPosition = Vector3.zero;
      this.m_labelGradient.transform.localScale = Vector3.one;
      this.m_setProgressLabel.gameObject.SetActive(true);
      this.m_setProgressLabel.TextColor = HeroPickerButton.BASIC_SET_COLOR_IN_PROGRESS;
    }
  }

  public void Lower()
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
      this.Activate(false);
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) new Vector3(this.GetOriginalLocalPosition().x, this.GetOriginalLocalPosition().y + (!this.m_locked ? -0.7f : 0.7f), this.GetOriginalLocalPosition().z), (object) "time", (object) 0.1f, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true));
  }

  public void Raise()
  {
    if (this.m_isSelected)
      return;
    this.Activate(true);
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) new Vector3(this.GetOriginalLocalPosition().x, this.GetOriginalLocalPosition().y, this.GetOriginalLocalPosition().z), (object) "time", (object) 0.1f, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true));
  }

  public void SetHighlightState(ActorStateType stateType)
  {
    if ((Object) this.m_highlightState == (Object) null)
      this.m_highlightState = this.gameObject.transform.parent.GetComponentInChildren<HighlightState>();
    if (!((Object) this.m_highlightState != (Object) null))
      return;
    this.m_highlightState.ChangeState(stateType);
  }

  public void Activate(bool enable)
  {
    this.SetEnabled(enable);
  }

  public void Lock()
  {
    this.transform.parent.transform.localEulerAngles = new Vector3(0.0f, 180f, 180f);
    this.m_locked = true;
  }

  public void SetProgress(int acknowledgedProgress, int currProgress, int maxProgress)
  {
    this.SetProgress(acknowledgedProgress, currProgress, maxProgress, true);
  }

  public void SetProgress(int acknowledgedProgress, int currProgress, int maxProgress, bool shouldAnimate)
  {
    bool flag = acknowledgedProgress == currProgress;
    if (currProgress != maxProgress)
      return;
    this.Unlock(!flag && shouldAnimate);
  }

  public bool IsLocked()
  {
    return this.m_locked;
  }

  public void SetUnlockedCallback(HeroPickerButton.UnlockedCallback unlockedCallback)
  {
    this.m_unlockedCallback = unlockedCallback;
  }

  public TAG_PREMIUM GetPremium()
  {
    return this.m_premium;
  }

  public void SetPremium(TAG_PREMIUM premium)
  {
    this.m_premium = premium;
    this.UpdatePortrait();
  }

  private void Unlock(bool animate)
  {
    this.transform.parent.localEulerAngles = new Vector3(0.0f, 180f, 0.0f);
    this.UnlockAfterAnimate();
  }

  private void UnlockAfterAnimate()
  {
    this.m_locked = false;
    if (this.m_unlockedCallback == null)
      return;
    this.m_unlockedCallback(this);
  }

  private Material GetClassIconMaterial(TAG_CLASS classTag)
  {
    int index = 0;
    switch (classTag)
    {
      case TAG_CLASS.INVALID:
      case TAG_CLASS.NEUTRAL:
        index = 9;
        break;
      case TAG_CLASS.DRUID:
        index = 5;
        break;
      case TAG_CLASS.HUNTER:
        index = 4;
        break;
      case TAG_CLASS.MAGE:
        index = 7;
        break;
      case TAG_CLASS.PALADIN:
        index = 3;
        break;
      case TAG_CLASS.PRIEST:
        index = 8;
        break;
      case TAG_CLASS.ROGUE:
        index = 2;
        break;
      case TAG_CLASS.SHAMAN:
        index = 1;
        break;
      case TAG_CLASS.WARLOCK:
        index = 6;
        break;
      case TAG_CLASS.WARRIOR:
        index = 0;
        break;
    }
    return this.CLASS_MATERIALS[index];
  }

  private void UpdatePortrait()
  {
    if (this.m_fullDef == null)
      return;
    CardDef cardDef = this.m_fullDef.GetCardDef();
    if ((Object) cardDef == (Object) null)
      return;
    Material deckPickerPortrait = cardDef.GetDeckPickerPortrait();
    if ((Object) deckPickerPortrait == (Object) null)
      return;
    DeckPickerHero component = this.GetComponent<DeckPickerHero>();
    Material portraitMaterial = cardDef.GetPremiumPortraitMaterial();
    if (this.m_premium == TAG_PREMIUM.GOLDEN && (Object) portraitMaterial != (Object) null)
    {
      component.m_PortraitMesh.GetComponent<Renderer>().material = portraitMaterial;
      component.m_PortraitMesh.GetComponent<Renderer>().material.mainTextureOffset = deckPickerPortrait.mainTextureOffset;
      component.m_PortraitMesh.GetComponent<Renderer>().material.mainTextureScale = deckPickerPortrait.mainTextureScale;
      component.m_PortraitMesh.GetComponent<Renderer>().material.SetTexture("_ShadowTex", (Texture) null);
      if (!this.m_seed.HasValue)
        this.m_seed = new float?(Random.value);
      if (!component.m_PortraitMesh.GetComponent<Renderer>().material.HasProperty("_Seed"))
        return;
      component.m_PortraitMesh.GetComponent<Renderer>().material.SetFloat("_Seed", this.m_seed.Value);
    }
    else
      component.m_PortraitMesh.GetComponent<Renderer>().sharedMaterial = deckPickerPortrait;
  }

  protected override void OnRelease()
  {
    if (!this.m_isDeckValid)
      return;
    this.Lower();
  }

  public delegate void UnlockedCallback(HeroPickerButton button);
}
