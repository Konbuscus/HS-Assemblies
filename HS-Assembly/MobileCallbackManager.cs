﻿// Decompiled with JetBrains decompiler
// Type: MobileCallbackManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using UnityEngine;

public class MobileCallbackManager : MonoBehaviour
{
  private const string CHINESE_CURRENCY_CODE = "CNY";
  private const string CHINESE_COUNTRY_CODE = "CN";
  private static MobileCallbackManager s_Instance;
  public bool m_wasBreakingNewsShown;

  private void Awake()
  {
    MobileCallbackManager.s_Instance = this;
  }

  private void OnDestroy()
  {
    MobileCallbackManager.s_Instance = (MobileCallbackManager) null;
  }

  public static MobileCallbackManager Get()
  {
    return MobileCallbackManager.s_Instance;
  }

  public void ClearCaches(LowMemorySeverity severity)
  {
    Debug.LogWarning((object) string.Format("Clearing Caches; this will force assets to be reloaded off disk and may be slow! {0}", (object) severity));
    if (StoreManager.Get() != null)
      StoreManager.Get().UnloadAndFreeMemory();
    if (severity != LowMemorySeverity.CRITICAL)
      return;
    if ((UnityEngine.Object) SpellCache.Get() != (UnityEngine.Object) null)
      SpellCache.Get().Clear();
    AssetCache.ClearAllCaches(true, false);
  }

  public void LowMemoryWarning(string msg)
  {
    LowMemorySeverity outVal;
    if (!EnumUtils.TryGetEnum<LowMemorySeverity>(msg, out outVal))
      outVal = LowMemorySeverity.MODERATE;
    Debug.LogWarningFormat("Receiving LowMemoryWarning severity={0}", (object) outVal);
    if (outVal >= LowMemorySeverity.SEVERE)
      this.ClearCaches(outVal);
    ApplicationMgr.Get().UnloadUnusedAssets();
  }

  public static bool IsAndroidDeviceTabletSized()
  {
    return true;
  }

  public static void RegisterPushNotifications()
  {
    Log.Yim.Print("MobileCallbackManager - RegisterPushNotifications()");
    MobileCallbackManager.RegisterForPushNotifications();
  }

  public static void SetPushRegistrationInfo(ulong gameAccountId, constants.BnetRegion bnetRegion, string locale)
  {
    Log.Yim.Print("SetPushRegistrationInfo(" + (object) gameAccountId + ", " + (object) bnetRegion + ", " + locale + ")");
    string bnetRegion1 = NydusLink.RegionToStr(bnetRegion) ?? "US";
    MobileCallbackManager.PushRegistrationInfo(Convert.ToString(gameAccountId), bnetRegion1, locale);
  }

  private static bool IsDevice(string deviceModel)
  {
    return false;
  }

  public static uint GetMemoryUsage()
  {
    return Profiler.GetTotalAllocatedMemory();
  }

  public static bool AreMotionEffectsEnabled()
  {
    return true;
  }

  private static void RegisterForPushNotifications()
  {
  }

  private static void PushRegistrationInfo(string gameAccountIdStr, string bnetRegion, string locale)
  {
  }
}
