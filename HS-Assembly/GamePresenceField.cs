﻿// Decompiled with JetBrains decompiler
// Type: GamePresenceField
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class GamePresenceField
{
  public const uint GAME_ACCOUNT = 2;
  public const uint CAN_BE_INVITED_TO_GAME = 1;
  public const uint DEBUG_STRING = 2;
  public const uint DEPRECATED_ARENA_RECORD = 3;
  public const uint CARDS_OPENED = 4;
  public const uint DRUID_LEVEL = 5;
  public const uint HUNTER_LEVEL = 6;
  public const uint MAGE_LEVEL = 7;
  public const uint PALADIN_LEVEL = 8;
  public const uint PRIEST_LEVEL = 9;
  public const uint ROGUE_LEVEL = 10;
  public const uint SHAMAN_LEVEL = 11;
  public const uint WARLOCK_LEVEL = 12;
  public const uint WARRIOR_LEVEL = 13;
  public const uint GAIN_MEDAL = 14;
  public const uint TUTORIAL_BEATEN = 15;
  public const uint COLLECTION_EVENT = 16;
  public const uint STATUS = 17;
  public const uint RANK = 18;
  public const uint CLIENT_VERSION = 19;
  public const uint CLIENT_ENV = 20;
  public const uint SPECTATOR_INFO = 21;
  public const uint SESSION_RECORD = 22;
}
