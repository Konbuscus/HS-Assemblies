﻿// Decompiled with JetBrains decompiler
// Type: BoxDrawerStateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class BoxDrawerStateInfo
{
  public float m_ClosedMoveSec = 1f;
  public iTween.EaseType m_ClosedMoveEaseType = iTween.EaseType.linear;
  public float m_ClosedBoxOpenedMoveSec = 1f;
  public iTween.EaseType m_ClosedBoxOpenedMoveEaseType = iTween.EaseType.linear;
  public float m_OpenedMoveSec = 1f;
  public iTween.EaseType m_OpenedMoveEaseType = iTween.EaseType.easeOutBounce;
  public GameObject m_ClosedBone;
  public float m_ClosedDelaySec;
  public GameObject m_ClosedBoxOpenedBone;
  public float m_ClosedBoxOpenedDelaySec;
  public GameObject m_OpenedBone;
  public float m_OpenedDelaySec;
}
