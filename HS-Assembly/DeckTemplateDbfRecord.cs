﻿// Decompiled with JetBrains decompiler
// Type: DeckTemplateDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DeckTemplateDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_ClassId;
  [SerializeField]
  private string m_Event;
  [SerializeField]
  private int m_SortOrder;
  [SerializeField]
  private int m_DeckId;
  [SerializeField]
  private string m_DisplayTexture;

  [DbfField("CLASS_ID", "points at CLASS.ID to identify which class this deck is available for")]
  public int ClassId
  {
    get
    {
      return this.m_ClassId;
    }
  }

  [DbfField("EVENT", "the event string, from EVENT_TIMING, that activates these rows.")]
  public string Event
  {
    get
    {
      return this.m_Event;
    }
  }

  [DbfField("SORT_ORDER", "the sorting order for deck templates.")]
  public int SortOrder
  {
    get
    {
      return this.m_SortOrder;
    }
  }

  [DbfField("DECK_ID", "points at DECK.ID to get the name, description, and card-list for the deck")]
  public int DeckId
  {
    get
    {
      return this.m_DeckId;
    }
  }

  [DbfField("DISPLAY_TEXTURE", "display texture to use in the deck template picker display")]
  public string DisplayTexture
  {
    get
    {
      return this.m_DisplayTexture;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    DeckTemplateDbfAsset templateDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (DeckTemplateDbfAsset)) as DeckTemplateDbfAsset;
    if ((UnityEngine.Object) templateDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("DeckTemplateDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < templateDbfAsset.Records.Count; ++index)
      templateDbfAsset.Records[index].StripUnusedLocales();
    records = (object) templateDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetClassId(int v)
  {
    this.m_ClassId = v;
  }

  public void SetEvent(string v)
  {
    this.m_Event = v;
  }

  public void SetSortOrder(int v)
  {
    this.m_SortOrder = v;
  }

  public void SetDeckId(int v)
  {
    this.m_DeckId = v;
  }

  public void SetDisplayTexture(string v)
  {
    this.m_DisplayTexture = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckTemplateDbfRecord.\u003C\u003Ef__switch\u0024map38 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckTemplateDbfRecord.\u003C\u003Ef__switch\u0024map38 = new Dictionary<string, int>(6)
        {
          {
            "ID",
            0
          },
          {
            "CLASS_ID",
            1
          },
          {
            "EVENT",
            2
          },
          {
            "SORT_ORDER",
            3
          },
          {
            "DECK_ID",
            4
          },
          {
            "DISPLAY_TEXTURE",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckTemplateDbfRecord.\u003C\u003Ef__switch\u0024map38.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.ClassId;
          case 2:
            return (object) this.Event;
          case 3:
            return (object) this.SortOrder;
          case 4:
            return (object) this.DeckId;
          case 5:
            return (object) this.DisplayTexture;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (DeckTemplateDbfRecord.\u003C\u003Ef__switch\u0024map39 == null)
    {
      // ISSUE: reference to a compiler-generated field
      DeckTemplateDbfRecord.\u003C\u003Ef__switch\u0024map39 = new Dictionary<string, int>(6)
      {
        {
          "ID",
          0
        },
        {
          "CLASS_ID",
          1
        },
        {
          "EVENT",
          2
        },
        {
          "SORT_ORDER",
          3
        },
        {
          "DECK_ID",
          4
        },
        {
          "DISPLAY_TEXTURE",
          5
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!DeckTemplateDbfRecord.\u003C\u003Ef__switch\u0024map39.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetClassId((int) val);
        break;
      case 2:
        this.SetEvent((string) val);
        break;
      case 3:
        this.SetSortOrder((int) val);
        break;
      case 4:
        this.SetDeckId((int) val);
        break;
      case 5:
        this.SetDisplayTexture((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckTemplateDbfRecord.\u003C\u003Ef__switch\u0024map3A == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckTemplateDbfRecord.\u003C\u003Ef__switch\u0024map3A = new Dictionary<string, int>(6)
        {
          {
            "ID",
            0
          },
          {
            "CLASS_ID",
            1
          },
          {
            "EVENT",
            2
          },
          {
            "SORT_ORDER",
            3
          },
          {
            "DECK_ID",
            4
          },
          {
            "DISPLAY_TEXTURE",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckTemplateDbfRecord.\u003C\u003Ef__switch\u0024map3A.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (string);
          case 3:
            return typeof (int);
          case 4:
            return typeof (int);
          case 5:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
