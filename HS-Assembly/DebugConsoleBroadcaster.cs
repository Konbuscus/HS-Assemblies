﻿// Decompiled with JetBrains decompiler
// Type: DebugConsoleBroadcaster
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Timers;
using UnityEngine;

public class DebugConsoleBroadcaster
{
  private static readonly TimeSpan Interval = new TimeSpan(0, 0, UnityEngine.Random.Range(7, 10));
  private Socket m_Socket;
  private IPEndPoint m_RemoteEndPoint;
  private byte[] m_RequestBytes;
  private Timer m_Timer;
  private bool m_started;

  public void Start(int destinationPort, string broadCastResponse)
  {
    this.m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
    this.m_Socket.EnableBroadcast = true;
    this.m_RequestBytes = new ASCIIEncoding().GetBytes(broadCastResponse);
    this.m_RemoteEndPoint = new IPEndPoint(IPAddress.Broadcast, destinationPort);
    this.m_Timer = new Timer(DebugConsoleBroadcaster.Interval.TotalMilliseconds);
    this.m_Timer.Elapsed += new ElapsedEventHandler(this.OnTimerTick);
    this.m_Timer.Start();
    this.m_started = true;
  }

  public void Stop()
  {
    if (!this.m_started)
      return;
    this.m_Timer.Stop();
    this.m_Socket.Close();
  }

  private void OnTimerTick(object sender, ElapsedEventArgs args)
  {
    this.m_Socket.BeginSendTo(this.m_RequestBytes, 0, this.m_RequestBytes.Length, SocketFlags.None, (EndPoint) this.m_RemoteEndPoint, new AsyncCallback(this.OnSendTo), (object) this);
  }

  private void OnSendTo(IAsyncResult ar)
  {
    try
    {
      this.m_Socket.EndSendTo(ar);
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("error debug broadcast: " + ex.Message));
    }
  }
}
