﻿// Decompiled with JetBrains decompiler
// Type: TutorialLesson2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TutorialLesson2 : MonoBehaviour
{
  public UberText m_cost;
  public UberText m_yourMana;

  private void Awake()
  {
    this.m_cost.SetGameStringText("GLOBAL_TUTORIAL_COST");
    this.m_yourMana.SetGameStringText("GLOBAL_TUTORIAL_YOUR_MANA");
  }
}
