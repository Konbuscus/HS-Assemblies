﻿// Decompiled with JetBrains decompiler
// Type: BoxDoorStateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class BoxDoorStateInfo
{
  public Vector3 m_OpenedRotation = new Vector3(0.0f, 0.0f, 180f);
  public float m_OpenedRotateSec = 0.35f;
  public Vector3 m_ClosedRotation = Vector3.zero;
  public float m_ClosedRotateSec = 0.35f;
  public float m_OpenedDelaySec;
  public iTween.EaseType m_OpenedRotateEaseType;
  public float m_ClosedDelaySec;
  public iTween.EaseType m_ClosedRotateEaseType;
}
