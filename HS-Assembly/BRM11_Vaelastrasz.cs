﻿// Decompiled with JetBrains decompiler
// Type: BRM11_Vaelastrasz
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class BRM11_Vaelastrasz : BRM_MissionEntity
{
  private bool m_cardLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BRMA11_1_RESPONSE_03");
    this.PreloadSound("VO_BRMA11_1_HERO_POWER_05");
    this.PreloadSound("VO_BRMA11_1_CARD_04");
    this.PreloadSound("VO_BRMA11_1_KILL_PLAYER_06");
    this.PreloadSound("VO_BRMA11_1_ALEXSTRAZA_07");
    this.PreloadSound("VO_BRMA11_1_TURN1_02");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA11_1_RESPONSE_03",
            m_stringTag = "VO_BRMA11_1_RESPONSE_03"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM11_Vaelastrasz.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator145() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM11_Vaelastrasz.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator146() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM11_Vaelastrasz.\u003CHandleMissionEventWithTiming\u003Ec__Iterator147() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM11_Vaelastrasz.\u003CHandleGameOverWithTiming\u003Ec__Iterator148() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
