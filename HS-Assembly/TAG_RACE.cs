﻿// Decompiled with JetBrains decompiler
// Type: TAG_RACE
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum TAG_RACE
{
  INVALID,
  BLOODELF,
  DRAENEI,
  DWARF,
  GNOME,
  GOBLIN,
  HUMAN,
  NIGHTELF,
  ORC,
  TAUREN,
  TROLL,
  UNDEAD,
  WORGEN,
  GOBLIN2,
  MURLOC,
  DEMON,
  SCOURGE,
  MECHANICAL,
  ELEMENTAL,
  OGRE,
  PET,
  TOTEM,
  NERUBIAN,
  PIRATE,
  DRAGON,
}
