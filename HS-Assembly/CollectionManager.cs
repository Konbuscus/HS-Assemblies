﻿// Decompiled with JetBrains decompiler
// Type: CollectionManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using PegasusUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WTCG.BI;

public class CollectionManager
{
  private Map<long, CollectionDeck> m_decks = new Map<long, CollectionDeck>();
  private Map<long, CollectionDeck> m_baseDecks = new Map<long, CollectionDeck>();
  private Map<TAG_CLASS, CollectionManager.PreconDeck> m_preconDecks = new Map<TAG_CLASS, CollectionManager.PreconDeck>();
  private Map<CollectionManager.DeckTag, CollectionDeck> m_taggedDecks = new Map<CollectionManager.DeckTag, CollectionDeck>();
  private Map<TAG_CLASS, List<CollectionManager.TemplateDeck>> m_templateDecks = new Map<TAG_CLASS, List<CollectionManager.TemplateDeck>>();
  private Map<int, CollectionManager.TemplateDeck> m_templateDeckMap = new Map<int, CollectionManager.TemplateDeck>();
  private List<TAG_CARD_SET> m_displayableCardSets = new List<TAG_CARD_SET>();
  private List<CollectionManager.DelOnCollectionLoaded> m_collectionLoadedListeners = new List<CollectionManager.DelOnCollectionLoaded>();
  private List<CollectionManager.DelOnCollectionChanged> m_collectionChangedListeners = new List<CollectionManager.DelOnCollectionChanged>();
  private List<CollectionManager.DelOnDeckCreated> m_deckCreatedListeners = new List<CollectionManager.DelOnDeckCreated>();
  private List<CollectionManager.DelOnDeckDeleted> m_deckDeletedListeners = new List<CollectionManager.DelOnDeckDeleted>();
  private List<CollectionManager.DelOnDeckContents> m_deckContentsListeners = new List<CollectionManager.DelOnDeckContents>();
  private List<CollectionManager.DelOnAllDeckContents> m_allDeckContentsListeners = new List<CollectionManager.DelOnAllDeckContents>();
  private List<CollectionManager.DelOnNewCardSeen> m_newCardSeenListeners = new List<CollectionManager.DelOnNewCardSeen>();
  private List<CollectionManager.DelOnCardRewardInserted> m_cardRewardListeners = new List<CollectionManager.DelOnCardRewardInserted>();
  private List<CollectionManager.OnMassDisenchant> m_massDisenchantListeners = new List<CollectionManager.OnMassDisenchant>();
  private List<CollectionManager.OnTaggedDeckChanged> m_taggedDeckChangedListeners = new List<CollectionManager.OnTaggedDeckChanged>();
  private List<CollectibleCard> m_collectibleCards = new List<CollectibleCard>();
  private Map<CollectionManager.CollectibleCardIndex, CollectibleCard> m_collectibleCardIndex = new Map<CollectionManager.CollectibleCardIndex, CollectibleCard>();
  private List<CollectionManager.DefaultCardbackChangedListener> m_defaultCardbackChangedListeners = new List<CollectionManager.DefaultCardbackChangedListener>();
  private List<CollectionManager.FavoriteHeroChangedListener> m_favoriteHeroChangedListeners = new List<CollectionManager.FavoriteHeroChangedListener>();
  private const int NUM_CARDS_GRANTED_POST_TUTORIAL = 96;
  private const int NUM_CARDS_TO_UNLOCK_ADVANCED_CM = 116;
  private const int NUM_EXPERT_CARDS_TO_UNLOCK_CRAFTING = 20;
  public const int NUM_EXPERT_CARDS_TO_UNLOCK_FORGE = 20;
  public const int NUM_BASIC_CARDS_PER_CLASS = 20;
  public const int NUM_CLASS_CARDS_GRANTED_PER_CLASS_UNLOCK = 6;
  public const int MAX_NUM_TEMPLATE_DECKS = 3;
  public const int MAX_DECKS_PER_PLAYER = 18;
  public const int NUM_CLASSES = 9;
  public const int DEFAULT_MAX_COPIES_PER_CARD_NORMAL = 2;
  public const int DEFAULT_MAX_COPIES_PER_CARD_LEGENDARY = 1;
  public const int DEFAULT_MAX_CARDS_PER_DECK = 30;
  private const float PENDING_DECK_CONTENTS_REQUEST_THRESHOLD_SECONDS = 10f;
  private static CollectionManager s_instance;
  private bool m_cardStacksRegistered;
  private bool m_collectionLoaded;
  private Map<long, float> m_pendingRequestDeckContents;
  private float m_collectionLastModifiedTime;
  private bool m_accountHasWildCards;
  private float m_lastSearchForWildCardsTime;
  private bool m_accountEverHadWildCards;
  private bool m_accountHasRotatedItems;
  private bool m_waitingForBoxTransition;
  private bool m_hasVisitedCollection;
  private bool m_editMode;
  private bool m_creatingInnkeeperDeck;
  private DeckRuleset m_deckRuleset;

  private void UpdateCardsWithNetData()
  {
    if (this.m_cardStacksRegistered)
      return;
    using (List<NetCache.CardStack>.Enumerator enumerator = NetCache.Get().GetNetObject<NetCache.NetCacheCollection>().Stacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.CardStack current = enumerator.Current;
        string name = current.Def.Name;
        if (GameUtils.IsCardCollectible(name))
        {
          EntityDef entityDef = DefLoader.Get().GetEntityDef(name);
          this.AddCounts(current, entityDef);
        }
      }
    }
    NetCache.NetCacheCardValues netObject = NetCache.Get().GetNetObject<NetCache.NetCacheCardValues>();
    if (netObject != null && netObject.Values.Count > 0)
    {
      using (Map<NetCache.CardDefinition, NetCache.CardValue>.Enumerator enumerator = netObject.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<NetCache.CardDefinition, NetCache.CardValue> current = enumerator.Current;
          CollectibleCard card = this.GetCard(current.Key.Name, current.Key.Premium);
          if (card != null)
          {
            card.CraftBuyCost = current.Value.Buy;
            card.CraftSellCost = current.Value.Sell;
          }
        }
      }
    }
    this.m_cardStacksRegistered = true;
  }

  private void OnCardSale()
  {
    Network.CardSaleResult cardSaleResult = Network.GetCardSaleResult();
    bool flag;
    switch (cardSaleResult.Action)
    {
      case Network.CardSaleResult.SaleResult.GENERIC_FAILURE:
        CraftingManager.Get().OnCardGenericError(cardSaleResult);
        flag = false;
        break;
      case Network.CardSaleResult.SaleResult.CARD_WAS_SOLD:
        for (int index = 1; index <= cardSaleResult.Count; ++index)
          this.RemoveCollectionCard(cardSaleResult.AssetName, cardSaleResult.Premium, 1);
        CraftingManager.Get().OnCardDisenchanted(cardSaleResult);
        flag = true;
        break;
      case Network.CardSaleResult.SaleResult.CARD_WAS_BOUGHT:
        for (int index = 1; index <= cardSaleResult.Count; ++index)
          this.InsertNewCollectionCard(cardSaleResult.AssetName, cardSaleResult.Premium, DateTime.Now, 1, true);
        CraftingManager.Get().OnCardCreated(cardSaleResult);
        AchieveManager.Get().ValidateAchievesNow();
        flag = true;
        break;
      case Network.CardSaleResult.SaleResult.SOULBOUND:
        CraftingManager.Get().OnCardDisenchantSoulboundError(cardSaleResult);
        flag = false;
        break;
      case Network.CardSaleResult.SaleResult.FAILED_WRONG_SELL_PRICE:
        NetCache.CardValue cardValue1 = CraftingManager.Get().GetCardValue(cardSaleResult.AssetName, cardSaleResult.Premium);
        if (cardValue1 != null)
        {
          cardValue1.Sell = cardSaleResult.UnitSellPrice;
          cardValue1.Nerfed = cardSaleResult.Nerfed;
        }
        flag = false;
        break;
      case Network.CardSaleResult.SaleResult.FAILED_WRONG_BUY_PRICE:
        NetCache.CardValue cardValue2 = CraftingManager.Get().GetCardValue(cardSaleResult.AssetName, cardSaleResult.Premium);
        if (cardValue2 != null)
        {
          cardValue2.Buy = cardSaleResult.UnitBuyPrice;
          cardValue2.Nerfed = cardSaleResult.Nerfed;
        }
        flag = false;
        break;
      case Network.CardSaleResult.SaleResult.FAILED_NO_PERMISSION:
        CraftingManager.Get().OnCardPermissionError(cardSaleResult);
        flag = false;
        break;
      case Network.CardSaleResult.SaleResult.FAILED_EVENT_NOT_ACTIVE:
        CraftingManager.Get().OnCardCraftingEventNotActiveError(cardSaleResult);
        flag = false;
        break;
      case Network.CardSaleResult.SaleResult.COUNT_MISMATCH:
        CraftingManager.Get().OnCardGenericError(cardSaleResult);
        CollectionManager.CollectibleCardIndex key = new CollectionManager.CollectibleCardIndex(cardSaleResult.AssetName, cardSaleResult.Premium);
        CollectibleCard collectibleCard = (CollectibleCard) null;
        if (cardSaleResult.CurrentCollectionCount.HasValue && this.m_collectibleCardIndex.TryGetValue(key, out collectibleCard))
          this.m_collectibleCardIndex[key].OwnedCount = cardSaleResult.CurrentCollectionCount.Value;
        CollectionCardVisual cardVisual = CollectionManagerDisplay.Get().m_pageManager.GetCardVisual(cardSaleResult.AssetName, cardSaleResult.Premium);
        if ((UnityEngine.Object) cardVisual != (UnityEngine.Object) null && cardVisual.IsShown())
          cardVisual.OnDoneCrafting();
        flag = false;
        break;
      default:
        CraftingManager.Get().OnCardUnknownError(cardSaleResult);
        flag = false;
        break;
    }
    string format = string.Format("CollectionManager.OnCardSale {0} for card {1} (asset {2}) premium {3}", (object) cardSaleResult.Action, (object) cardSaleResult.AssetName, (object) cardSaleResult.AssetID, (object) cardSaleResult.Premium);
    if (!flag)
    {
      Debug.LogWarning((object) format);
    }
    else
    {
      Log.Crafting.Print(format);
      this.OnCollectionChanged();
    }
  }

  private void OnMassDisenchantResponse()
  {
    Network.MassDisenchantResponse disenchantResponse = Network.GetMassDisenchantResponse();
    if (disenchantResponse.Amount == 0)
    {
      Debug.LogError((object) "CollectionManager.OnMassDisenchantResponse(): Amount is 0. This means the backend failed to mass disenchant correctly.");
    }
    else
    {
      NetCache.Get().OnArcaneDustBalanceChanged((long) disenchantResponse.Amount);
      foreach (CollectionManager.OnMassDisenchant onMassDisenchant in this.m_massDisenchantListeners.ToArray())
        onMassDisenchant(disenchantResponse.Amount);
      using (List<CollectibleCard>.Enumerator enumerator = this.GetMassDisenchantCards().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CollectibleCard current = enumerator.Current;
          this.RemoveCollectionCard(current.CardId, current.PremiumType, current.DisenchantCount);
        }
      }
      this.OnCollectionChanged();
    }
  }

  private void OnSetFavoriteHeroResponse()
  {
    Network.SetFavoriteHeroResponse favoriteHeroResponse = Network.GetSetFavoriteHeroResponse();
    if (!favoriteHeroResponse.Success)
      return;
    if (favoriteHeroResponse.HeroClass == TAG_CLASS.NEUTRAL || favoriteHeroResponse.Hero == null)
    {
      Debug.LogWarning((object) string.Format("CollectionManager.OnSetFavoriteHeroResponse: setting hero was a success, but message contains invalid class ({0}) and/or hero ({1})", (object) favoriteHeroResponse.HeroClass, (object) favoriteHeroResponse.Hero));
    }
    else
    {
      NetCache.NetCacheFavoriteHeroes netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFavoriteHeroes>();
      if (netObject != null)
      {
        netObject.FavoriteHeroes[favoriteHeroResponse.HeroClass] = favoriteHeroResponse.Hero;
        Log.Rachelle.Print("CollectionManager.OnSetFavoriteHeroResponse: favorite hero for class {0} updated to {1}", new object[2]
        {
          (object) favoriteHeroResponse.HeroClass,
          (object) favoriteHeroResponse.Hero
        });
      }
      this.UpdateFavoriteHero(favoriteHeroResponse.HeroClass, favoriteHeroResponse.Hero.Name, favoriteHeroResponse.Hero.Premium);
    }
  }

  private void NetCache_OnFavoriteHeroesReceived()
  {
    NetCache.NetCacheFavoriteHeroes netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFavoriteHeroes>();
    if (netObject == null || netObject.FavoriteHeroes == null)
      return;
    using (Map<TAG_CLASS, NetCache.CardDefinition>.Enumerator enumerator = netObject.FavoriteHeroes.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<TAG_CLASS, NetCache.CardDefinition> current = enumerator.Current;
        TAG_CLASS key = current.Key;
        NetCache.CardDefinition cardDefinition = current.Value;
        this.UpdateFavoriteHero(key, cardDefinition.Name, cardDefinition.Premium);
      }
    }
  }

  private void UpdateFavoriteHero(TAG_CLASS heroClass, string heroCardId, TAG_PREMIUM premium)
  {
    if (NetCache.Get().IsNetObjectAvailable<NetCache.NetCacheDecks>())
    {
      using (List<NetCache.DeckHeader>.Enumerator enumerator = NetCache.Get().GetNetObject<NetCache.NetCacheDecks>().Decks.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          NetCache.DeckHeader current = enumerator.Current;
          if (!current.HeroOverridden)
          {
            EntityDef entityDef = DefLoader.Get().GetEntityDef(current.Hero);
            if (heroClass == entityDef.GetClass())
            {
              current.Hero = heroCardId;
              current.HeroPremium = premium;
              CollectionDeck deck = this.GetDeck(current.ID);
              if (deck != null)
              {
                deck.HeroCardID = heroCardId;
                deck.HeroPremium = premium;
              }
              CollectionDeck baseDeck = this.GetBaseDeck(current.ID);
              if (baseDeck != null)
              {
                baseDeck.HeroCardID = heroCardId;
                baseDeck.HeroPremium = premium;
              }
            }
          }
        }
      }
    }
    else
      Log.JMac.PrintWarning("Received Favorite Heroes without NetCacheDecks being ready!");
    if (this.m_favoriteHeroChangedListeners.Count <= 0)
      return;
    NetCache.CardDefinition favoriteHero = new NetCache.CardDefinition();
    favoriteHero.Name = heroCardId;
    favoriteHero.Premium = premium;
    foreach (CollectionManager.FavoriteHeroChangedListener heroChangedListener in this.m_favoriteHeroChangedListeners.ToArray())
      heroChangedListener.Fire(heroClass, favoriteHero);
  }

  public void NetCache_OnDecksReceived()
  {
    using (List<NetCache.DeckHeader>.Enumerator enumerator = NetCache.Get().GetNetObject<NetCache.NetCacheDecks>().Decks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.DeckHeader current = enumerator.Current;
        if (current.Type == DeckType.NORMAL_DECK && this.GetDeck(current.ID) == null && DefLoader.Get().GetEntityDef(current.Hero) != null)
          this.AddDeck(current, false);
      }
    }
  }

  private void OnDefaultCardBackSet()
  {
    Network.CardBackResponse cardBackResponse = Network.GetCardBackResponse();
    if (!cardBackResponse.Success)
    {
      Log.Rachelle.Print("SetCardBack FAILED (cardBack = {0})", (object) cardBackResponse.CardBack);
    }
    else
    {
      NetCache.NetCacheCardBacks netObject1 = NetCache.Get().GetNetObject<NetCache.NetCacheCardBacks>();
      NetCache.NetCacheDecks netObject2 = NetCache.Get().GetNetObject<NetCache.NetCacheDecks>();
      netObject1.DefaultCardBack = cardBackResponse.CardBack;
      using (List<NetCache.DeckHeader>.Enumerator enumerator = netObject2.Decks.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          NetCache.DeckHeader current = enumerator.Current;
          if (!current.CardBackOverridden)
          {
            current.CardBack = cardBackResponse.CardBack;
            CollectionDeck deck = this.GetDeck(current.ID);
            if (deck != null)
              deck.CardBackID = current.CardBack;
            CollectionDeck baseDeck = this.GetBaseDeck(current.ID);
            if (baseDeck != null)
              baseDeck.CardBackID = current.CardBack;
          }
        }
      }
      foreach (CollectionManager.DefaultCardbackChangedListener cardbackChangedListener in this.m_defaultCardbackChangedListeners.ToArray())
        cardbackChangedListener.Fire(netObject1.DefaultCardBack);
    }
  }

  private void OnGetDeckContentsResponse()
  {
    GetDeckContentsResponse contentsResponse = Network.GetDeckContentsResponse();
    for (int index = 0; index < contentsResponse.Decks.Count; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      CollectionManager.\u003COnGetDeckContentsResponse\u003Ec__AnonStorey383 responseCAnonStorey383 = new CollectionManager.\u003COnGetDeckContentsResponse\u003Ec__AnonStorey383();
      // ISSUE: reference to a compiler-generated field
      responseCAnonStorey383.netDeck = Network.DeckContents.FromPacket(contentsResponse.Decks[index]);
      if (this.m_pendingRequestDeckContents != null)
      {
        // ISSUE: reference to a compiler-generated field
        this.m_pendingRequestDeckContents.Remove(responseCAnonStorey383.netDeck.Deck);
      }
      CollectionDeck collectionDeck1 = (CollectionDeck) null;
      // ISSUE: reference to a compiler-generated field
      this.m_decks.TryGetValue(responseCAnonStorey383.netDeck.Deck, out collectionDeck1);
      CollectionDeck collectionDeck2 = (CollectionDeck) null;
      // ISSUE: reference to a compiler-generated field
      this.m_baseDecks.TryGetValue(responseCAnonStorey383.netDeck.Deck, out collectionDeck2);
      if (collectionDeck1 == null || collectionDeck2 == null)
      {
        // ISSUE: reference to a compiler-generated method
        if (!this.m_preconDecks.Any<KeyValuePair<TAG_CLASS, CollectionManager.PreconDeck>>(new Func<KeyValuePair<TAG_CLASS, CollectionManager.PreconDeck>, bool>(responseCAnonStorey383.\u003C\u003Em__B5)))
        {
          // ISSUE: reference to a compiler-generated field
          Debug.LogErrorFormat("Got contents for an unknown deck or baseDeck: deckId={0}", (object) responseCAnonStorey383.netDeck.Deck);
        }
      }
      else
      {
        collectionDeck1.ClearSlotContents();
        collectionDeck2.ClearSlotContents();
        // ISSUE: reference to a compiler-generated field
        using (List<Network.CardUserData>.Enumerator enumerator = responseCAnonStorey383.netDeck.Cards.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Network.CardUserData current = enumerator.Current;
            string cardId = GameUtils.TranslateDbIdToCardId(current.DbId);
            if (cardId == null)
            {
              Debug.LogError((object) string.Format("CollectionManager.OnDeck(): Could not find card with asset ID {0} in our card manifest", (object) current.DbId));
            }
            else
            {
              collectionDeck1.AddCard_IgnoreValidity(cardId, current.Premium, current.Count);
              collectionDeck2.AddCard_IgnoreValidity(cardId, current.Premium, current.Count);
            }
          }
        }
        collectionDeck1.MarkNetworkContentsLoaded();
      }
      // ISSUE: reference to a compiler-generated field
      this.FireDeckContentsEvent(responseCAnonStorey383.netDeck.Deck);
    }
    using (SortedDictionary<long, CollectionDeck>.ValueCollection.Enumerator enumerator = this.GetDecks().Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (!enumerator.Current.NetworkContentsLoaded())
          return;
      }
    }
    if (this.m_pendingRequestDeckContents != null)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      foreach (long key in this.m_pendingRequestDeckContents.Where<KeyValuePair<long, float>>(new Func<KeyValuePair<long, float>, bool>(new CollectionManager.\u003COnGetDeckContentsResponse\u003Ec__AnonStorey384()
      {
        now = Time.realtimeSinceStartup
      }.\u003C\u003Em__B6)).Select<KeyValuePair<long, float>, long>((Func<KeyValuePair<long, float>, long>) (kv => kv.Key)).ToArray<long>())
        this.m_pendingRequestDeckContents.Remove(key);
    }
    if (this.m_pendingRequestDeckContents != null && this.m_pendingRequestDeckContents.Count != 0)
      return;
    this.FireAllDeckContentsEvent();
  }

  private void OnDBAction()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003COnDBAction\u003Ec__AnonStorey385 actionCAnonStorey385 = new CollectionManager.\u003COnDBAction\u003Ec__AnonStorey385();
    Network.DBAction deckResponse = Network.GetDeckResponse();
    Log.Rachelle.Print(string.Format("MetaData:{0} DBAction:{1} Result:{2}", (object) deckResponse.MetaData, (object) deckResponse.Action, (object) deckResponse.Result));
    bool flag1 = false;
    bool flag2 = false;
    switch (deckResponse.Action)
    {
      case Network.DBAction.ActionType.CREATE_DECK:
        if (deckResponse.Result != Network.DBAction.ResultType.SUCCESS)
        {
          if ((UnityEngine.Object) CollectionDeckTray.Get() != (UnityEngine.Object) null)
            CollectionDeckTray.Get().GetDecksContent().CreateNewDeckCancelled();
          if (this.m_creatingInnkeeperDeck)
          {
            Log.ReturningPlayer.PrintError("CollectionManager.OnDBAction(): Creating a new Innkeeper Deck failed! Response: {0}", (object) deckResponse);
            this.m_creatingInnkeeperDeck = false;
            break;
          }
          break;
        }
        break;
      case Network.DBAction.ActionType.RENAME_DECK:
        flag1 = true;
        break;
      case Network.DBAction.ActionType.SET_DECK:
        if (this.m_creatingInnkeeperDeck)
        {
          if (deckResponse.Result == Network.DBAction.ResultType.SUCCESS)
          {
            Options.Get().SetBool(Option.SHOW_INNKEEPER_DECK_DIALOGUE, true);
            Log.ReturningPlayer.Print("Innkeeper Deck generated successfully!");
          }
          else
            Log.ReturningPlayer.PrintError("CollectionManager.OnDBAction(): Setting the contents of the new Innkeeper Deck failed! Response: {0}", (object) deckResponse);
          this.m_creatingInnkeeperDeck = false;
        }
        flag2 = true;
        break;
    }
    if (!flag1 && !flag2)
      return;
    // ISSUE: reference to a compiler-generated field
    actionCAnonStorey385.deckID = deckResponse.MetaData;
    // ISSUE: reference to a compiler-generated field
    CollectionDeck deck = this.GetDeck(actionCAnonStorey385.deckID);
    // ISSUE: reference to a compiler-generated field
    CollectionDeck baseDeck = this.GetBaseDeck(actionCAnonStorey385.deckID);
    if (deckResponse.Result == Network.DBAction.ResultType.SUCCESS)
    {
      Log.Rachelle.Print(string.Format("CollectionManager.OnDBAction(): overwriting baseDeck with {0} updated deck ({1}:{2})", !deck.IsTourneyValid ? (object) "INVALID" : (object) "valid", (object) deck.ID, (object) deck.Name));
      baseDeck.CopyFrom(deck);
      // ISSUE: reference to a compiler-generated method
      NetCache.DeckHeader deckHeader = NetCache.Get().GetNetObject<NetCache.NetCacheDecks>().Decks.Find(new Predicate<NetCache.DeckHeader>(actionCAnonStorey385.\u003C\u003Em__B8));
      if (deckHeader != null)
      {
        deckHeader.HeroOverridden = deck.HeroOverridden;
        deckHeader.CardBackOverridden = deck.CardBackOverridden;
        deckHeader.SeasonId = deck.SeasonId;
        deckHeader.NeedsName = deck.NeedsName;
        deckHeader.IsWild = deck.IsWild;
        deckHeader.LastModified = new DateTime?(DateTime.Now);
      }
    }
    else
    {
      Log.Rachelle.Print(string.Format("CollectionManager.OnDBAction(): overwriting deck that failed to update with base deck ({0}:{1})", (object) baseDeck.ID, (object) baseDeck.Name));
      deck.CopyFrom(baseDeck);
    }
    if (flag1)
      deck.OnNameChangeComplete();
    if (!flag2)
      return;
    deck.OnContentChangesComplete();
  }

  private void OnDeckCreated()
  {
    NetCache.DeckHeader createdDeck = Network.GetCreatedDeck();
    Log.Rachelle.Print(string.Format("DeckCreated:{0} ID:{1} Hero:{2}", (object) createdDeck.Name, (object) createdDeck.ID, (object) createdDeck.Hero));
    CollectionDeck deck = this.AddDeck(createdDeck);
    deck.MarkNetworkContentsLoaded();
    using (List<CollectionManager.DelOnDeckCreated>.Enumerator enumerator = this.m_deckCreatedListeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current(createdDeck.ID);
    }
    if (!this.m_creatingInnkeeperDeck)
      return;
    this.FillInnkeeperDeck(deck);
  }

  private void OnDeckDeleted()
  {
    Log.Rachelle.Print("CollectionManager.OnDeckDeleted");
    long deletedDeckId = Network.GetDeletedDeckID();
    Log.Rachelle.Print(string.Format("DeckDeleted:{0}", (object) deletedDeckId));
    this.RemoveDeck(deletedDeckId);
    if ((UnityEngine.Object) CollectionDeckTray.Get() == (UnityEngine.Object) null)
      return;
    foreach (CollectionManager.DelOnDeckDeleted delOnDeckDeleted in this.m_deckDeletedListeners.ToArray())
      delOnDeckDeleted(deletedDeckId);
  }

  private void OnDeckRenamed()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003COnDeckRenamed\u003Ec__AnonStorey386 renamedCAnonStorey386 = new CollectionManager.\u003COnDeckRenamed\u003Ec__AnonStorey386();
    Network.DeckName renamedDeck = Network.GetRenamedDeck();
    Log.Rachelle.Print(string.Format("OnDeckRenamed {0}", (object) renamedDeck.Deck));
    // ISSUE: reference to a compiler-generated field
    renamedCAnonStorey386.id = renamedDeck.Deck;
    string name = renamedDeck.Name;
    // ISSUE: reference to a compiler-generated field
    this.GetBaseDeck(renamedCAnonStorey386.id).Name = name;
    // ISSUE: reference to a compiler-generated field
    CollectionDeck deck = this.GetDeck(renamedCAnonStorey386.id);
    deck.Name = name;
    // ISSUE: reference to a compiler-generated method
    NetCache.DeckHeader deckHeader = NetCache.Get().GetNetObject<NetCache.NetCacheDecks>().Decks.Find(new Predicate<NetCache.DeckHeader>(renamedCAnonStorey386.\u003C\u003Em__B9));
    if (deckHeader != null)
    {
      deckHeader.Name = name;
      deckHeader.LastModified = new DateTime?(DateTime.Now);
    }
    deck.OnNameChangeComplete();
  }

  public static void Init()
  {
    if (CollectionManager.s_instance == null)
    {
      CollectionManager.s_instance = new CollectionManager();
      ApplicationMgr.Get().WillReset += new Action(CollectionManager.s_instance.WillReset);
      NetCache.Get().RegisterUpdatedListener(typeof (NetCache.NetCacheFavoriteHeroes), new Action(CollectionManager.s_instance.NetCache_OnFavoriteHeroesReceived));
    }
    CollectionManager.s_instance.InitImpl();
  }

  public static CollectionManager Get()
  {
    return CollectionManager.s_instance;
  }

  public bool IsFullyLoaded()
  {
    return this.m_collectionLoaded;
  }

  public void RegisterCollectionNetHandlers()
  {
    Network network = Network.Get();
    network.RegisterNetHandler((object) BoughtSoldCard.PacketID.ID, new Network.NetHandler(this.OnCardSale), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.MassDisenchantResponse.PacketID.ID, new Network.NetHandler(this.OnMassDisenchantResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.SetFavoriteHeroResponse.PacketID.ID, new Network.NetHandler(this.OnSetFavoriteHeroResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) SetCardBackResponse.PacketID.ID, new Network.NetHandler(this.OnDefaultCardBackSet), (Network.TimeoutHandler) null);
  }

  public void RemoveCollectionNetHandlers()
  {
    Network network = Network.Get();
    network.RemoveNetHandler((object) BoughtSoldCard.PacketID.ID, new Network.NetHandler(this.OnCardSale));
    network.RemoveNetHandler((object) PegasusUtil.MassDisenchantResponse.PacketID.ID, new Network.NetHandler(this.OnMassDisenchantResponse));
    network.RemoveNetHandler((object) PegasusUtil.SetFavoriteHeroResponse.PacketID.ID, new Network.NetHandler(this.OnSetFavoriteHeroResponse));
    network.RemoveNetHandler((object) SetCardBackResponse.PacketID.ID, new Network.NetHandler(this.OnDefaultCardBackSet));
  }

  public bool HasVisitedCollection()
  {
    return this.m_hasVisitedCollection;
  }

  public void SetHasVisitedCollection(bool enable)
  {
    this.m_hasVisitedCollection = enable;
  }

  public bool IsWaitingForBoxTransition()
  {
    return this.m_waitingForBoxTransition;
  }

  public void NotifyOfBoxTransitionStart()
  {
    Box.Get().AddTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    this.m_waitingForBoxTransition = true;
  }

  public void OnBoxTransitionFinished(object userData)
  {
    Box.Get().RemoveTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    this.m_waitingForBoxTransition = false;
  }

  public void AddCardReward(CardRewardData cardReward, bool markAsNew)
  {
    this.AddCardRewards(new List<CardRewardData>()
    {
      cardReward
    }, markAsNew);
  }

  public void AddCardRewards(List<CardRewardData> cardRewards, bool markAsNew)
  {
    List<string> cardIDs = new List<string>();
    List<TAG_PREMIUM> cardPremiums = new List<TAG_PREMIUM>();
    List<DateTime> insertDates = new List<DateTime>();
    List<int> counts = new List<int>();
    DateTime now = DateTime.Now;
    using (List<CardRewardData>.Enumerator enumerator = cardRewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardRewardData current = enumerator.Current;
        cardIDs.Add(current.CardID);
        cardPremiums.Add(current.Premium);
        insertDates.Add(now);
        counts.Add(current.Count);
        foreach (CollectionManager.DelOnCardRewardInserted cardRewardInserted in this.m_cardRewardListeners.ToArray())
          cardRewardInserted(current.CardID, current.Premium);
      }
    }
    this.InsertNewCollectionCards(cardIDs, cardPremiums, insertDates, counts, !markAsNew);
    AchieveManager.Get().ValidateAchievesNow();
  }

  public float CollectionLastModifiedTime()
  {
    return this.m_collectionLastModifiedTime;
  }

  public int EntityDefSortComparison(EntityDef entityDef1, EntityDef entityDef2)
  {
    int num1 = entityDef1.GetCost() - entityDef2.GetCost();
    if (num1 != 0)
      return num1;
    int num2 = string.Compare(entityDef1.GetName(), entityDef2.GetName(), true);
    if (num2 != 0)
      return num2;
    return this.GetCardTypeSortOrder(entityDef1) - this.GetCardTypeSortOrder(entityDef2);
  }

  public int GetCardTypeSortOrder(EntityDef entityDef)
  {
    switch (entityDef.GetCardType())
    {
      case TAG_CARDTYPE.MINION:
        return 3;
      case TAG_CARDTYPE.SPELL:
        return 2;
      case TAG_CARDTYPE.WEAPON:
        return 1;
      default:
        return 0;
    }
  }

  public List<CollectibleCard> FindCards(string searchString = null, List<CollectibleCardFilter.FilterMask> filterMasks = null, int? manaCost = null, TAG_CARD_SET[] theseCardSets = null, TAG_CLASS[] theseClassTypes = null, TAG_CARDTYPE[] theseCardTypes = null, TAG_RARITY? rarity = null, TAG_RACE? race = null, bool? isHero = null, int? minOwned = null, bool? notSeen = null, bool? isCraftable = null, CollectionManager.CollectibleCardFilterFunc[] priorityFilters = null, DeckRuleset deckRuleset = null, bool returnAfterFirstResult = false)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003CFindCards\u003Ec__AnonStorey387 cardsCAnonStorey387 = new CollectionManager.\u003CFindCards\u003Ec__AnonStorey387();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.minOwned = minOwned;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.filterMasks = filterMasks;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.theseCardSets = theseCardSets;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.theseClassTypes = theseClassTypes;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.theseCardTypes = theseCardTypes;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.rarity = rarity;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.race = race;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.isHero = isHero;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.isCraftable = isCraftable;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.deckRuleset = deckRuleset;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.filterFuncs = new List<CollectionManager.CollectibleCardFilterFunc>();
    if (priorityFilters != null)
    {
      // ISSUE: reference to a compiler-generated field
      cardsCAnonStorey387.filterFuncs.AddRange((IEnumerable<CollectionManager.CollectibleCardFilterFunc>) priorityFilters);
    }
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.minOwned.HasValue)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__BA));
    }
    // ISSUE: reference to a compiler-generated method
    CollectionManager.CollectibleCardFilterFunc collectibleCardFilterFunc = new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__BB);
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.filterMasks != null)
    {
      // ISSUE: reference to a compiler-generated field
      cardsCAnonStorey387.filterFuncs.Add(collectibleCardFilterFunc);
    }
    if (manaCost.HasValue)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      CollectionManager.\u003CFindCards\u003Ec__AnonStorey388 cardsCAnonStorey388 = new CollectionManager.\u003CFindCards\u003Ec__AnonStorey388();
      // ISSUE: reference to a compiler-generated field
      cardsCAnonStorey388.minManaCost = manaCost.Value;
      // ISSUE: reference to a compiler-generated field
      cardsCAnonStorey388.maxManaCost = manaCost.Value;
      // ISSUE: reference to a compiler-generated field
      if (cardsCAnonStorey388.maxManaCost >= 7)
      {
        // ISSUE: reference to a compiler-generated field
        cardsCAnonStorey388.maxManaCost = int.MaxValue;
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey388.\u003C\u003Em__BC));
    }
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.theseCardSets != null && cardsCAnonStorey387.theseCardSets.Length > 0)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__BD));
    }
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.theseClassTypes != null && cardsCAnonStorey387.theseClassTypes.Length > 0)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__BE));
    }
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.theseCardTypes != null && cardsCAnonStorey387.theseCardTypes.Length > 0)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__BF));
    }
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.rarity.HasValue)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__C0));
    }
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.race.HasValue)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__C1));
    }
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.isHero.HasValue)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__C2));
    }
    if (notSeen.HasValue)
    {
      if (notSeen.Value)
      {
        // ISSUE: reference to a compiler-generated field
        cardsCAnonStorey387.filterFuncs.Add((CollectionManager.CollectibleCardFilterFunc) (card => card.SeenCount < card.OwnedCount));
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        cardsCAnonStorey387.filterFuncs.Add((CollectionManager.CollectibleCardFilterFunc) (card => card.SeenCount == card.OwnedCount));
      }
    }
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.isCraftable.HasValue)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__C5));
    }
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey387.filterFuncs.AddRange((IEnumerable<CollectionManager.CollectibleCardFilterFunc>) CollectibleCardFilter.FiltersFromSearchString(searchString));
    // ISSUE: reference to a compiler-generated field
    if (cardsCAnonStorey387.deckRuleset != null)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      cardsCAnonStorey387.filterFuncs.Add(new CollectionManager.CollectibleCardFilterFunc(cardsCAnonStorey387.\u003C\u003Em__C6));
    }
    // ISSUE: reference to a compiler-generated method
    Predicate<CollectibleCard> match = new Predicate<CollectibleCard>(cardsCAnonStorey387.\u003C\u003Em__C7);
    List<CollectibleCard> collectibleCards;
    if (returnAfterFirstResult)
    {
      collectibleCards = new List<CollectibleCard>();
      CollectibleCard collectibleCard = this.m_collectibleCards.Find(match);
      if (collectibleCard != null)
        collectibleCards.Add(collectibleCard);
    }
    else
      collectibleCards = this.m_collectibleCards.FindAll(match);
    string str1 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_EXTRA");
    if (searchString != null)
    {
      if (((IEnumerable<string>) searchString.ToLower().Split(' ')).Contains<string>(str1))
        this.FilterOnlyExtraCards(ref collectibleCards);
    }
    string str2 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_MISSING");
    if (searchString != null)
    {
      if (((IEnumerable<string>) searchString.ToLower().Split(' ')).Contains<string>(str2))
        this.FilterOnlyMissingCards(ref collectibleCards);
    }
    return collectibleCards;
  }

  public List<CollectibleCard> FindOrderedCards(string searchString = null, List<CollectibleCardFilter.FilterMask> filterMasks = null, int? manaCost = null, TAG_CARD_SET[] theseCardSets = null, TAG_CLASS[] theseClassTypes = null, TAG_CARDTYPE[] theseCardTypes = null, TAG_RARITY? rarity = null, TAG_RACE? race = null, bool? isHero = null, int? minOwned = null, bool? notSeen = null, bool? isCraftable = null, CollectionManager.CollectibleCardFilterFunc[] priorityFilters = null, DeckRuleset deckRuleset = null)
  {
    return this.FindCards(searchString, filterMasks, manaCost, theseCardSets, theseClassTypes, theseCardTypes, rarity, race, isHero, minOwned, notSeen, isCraftable, priorityFilters, deckRuleset, false).OrderBy<CollectibleCard, int>((Func<CollectibleCard, int>) (c => c.ManaCost)).ThenBy<CollectibleCard, string>((Func<CollectibleCard, string>) (c => c.Name)).ToList<CollectibleCard>();
  }

  public void FilterOnlyExtraCards(ref List<CollectibleCard> collectibleCards)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003CFilterOnlyExtraCards\u003Ec__AnonStorey389 cardsCAnonStorey389 = new CollectionManager.\u003CFilterOnlyExtraCards\u003Ec__AnonStorey389();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey389.collectibleCardCount = new Map<string, int>();
    using (List<CollectibleCard>.Enumerator enumerator = collectibleCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectibleCard current = enumerator.Current;
        if (current.IsCraftable)
        {
          // ISSUE: reference to a compiler-generated field
          if (!cardsCAnonStorey389.collectibleCardCount.ContainsKey(current.CardId))
          {
            // ISSUE: reference to a compiler-generated field
            cardsCAnonStorey389.collectibleCardCount.Add(current.CardId, current.OwnedCount);
          }
          else
          {
            Map<string, int> collectibleCardCount;
            string cardId;
            // ISSUE: reference to a compiler-generated field
            (collectibleCardCount = cardsCAnonStorey389.collectibleCardCount)[cardId = current.CardId] = collectibleCardCount[cardId] + current.OwnedCount;
          }
        }
      }
    }
    // ISSUE: reference to a compiler-generated method
    collectibleCards.RemoveAll(new Predicate<CollectibleCard>(cardsCAnonStorey389.\u003C\u003Em__CA));
  }

  public void FilterOnlyMissingCards(ref List<CollectibleCard> collectibleCards)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003CFilterOnlyMissingCards\u003Ec__AnonStorey38A cardsCAnonStorey38A = new CollectionManager.\u003CFilterOnlyMissingCards\u003Ec__AnonStorey38A();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey38A.collectibleCardCount = new Map<string, int>();
    using (List<CollectibleCard>.Enumerator enumerator = collectibleCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectibleCard current = enumerator.Current;
        if (current.IsCraftable)
        {
          // ISSUE: reference to a compiler-generated field
          if (!cardsCAnonStorey38A.collectibleCardCount.ContainsKey(current.CardId))
          {
            // ISSUE: reference to a compiler-generated field
            cardsCAnonStorey38A.collectibleCardCount.Add(current.CardId, current.OwnedCount);
          }
          else
          {
            Map<string, int> collectibleCardCount;
            string cardId;
            // ISSUE: reference to a compiler-generated field
            (collectibleCardCount = cardsCAnonStorey38A.collectibleCardCount)[cardId = current.CardId] = collectibleCardCount[cardId] + current.OwnedCount;
          }
        }
      }
    }
    // ISSUE: reference to a compiler-generated method
    collectibleCards.RemoveAll(new Predicate<CollectibleCard>(cardsCAnonStorey38A.\u003C\u003Em__CB));
  }

  public List<CollectibleCard> GetAllCards()
  {
    return this.m_collectibleCards;
  }

  public void RegisterCollectionLoadedListener(CollectionManager.DelOnCollectionLoaded listener)
  {
    if (this.m_collectionLoadedListeners.Contains(listener))
      return;
    this.m_collectionLoadedListeners.Add(listener);
  }

  public bool RemoveCollectionLoadedListener(CollectionManager.DelOnCollectionLoaded listener)
  {
    return this.m_collectionLoadedListeners.Remove(listener);
  }

  public void RegisterCollectionChangedListener(CollectionManager.DelOnCollectionChanged listener)
  {
    if (this.m_collectionChangedListeners.Contains(listener))
      return;
    this.m_collectionChangedListeners.Add(listener);
  }

  public bool RemoveCollectionChangedListener(CollectionManager.DelOnCollectionChanged listener)
  {
    return this.m_collectionChangedListeners.Remove(listener);
  }

  public void RegisterDeckCreatedListener(CollectionManager.DelOnDeckCreated listener)
  {
    if (this.m_deckCreatedListeners.Contains(listener))
      return;
    this.m_deckCreatedListeners.Add(listener);
  }

  public bool RemoveDeckCreatedListener(CollectionManager.DelOnDeckCreated listener)
  {
    return this.m_deckCreatedListeners.Remove(listener);
  }

  public void RegisterDeckDeletedListener(CollectionManager.DelOnDeckDeleted listener)
  {
    if (this.m_deckDeletedListeners.Contains(listener))
      return;
    this.m_deckDeletedListeners.Add(listener);
  }

  public bool RemoveDeckDeletedListener(CollectionManager.DelOnDeckDeleted listener)
  {
    return this.m_deckDeletedListeners.Remove(listener);
  }

  public void RegisterDeckContentsListener(CollectionManager.DelOnDeckContents listener)
  {
    if (this.m_deckContentsListeners.Contains(listener))
      return;
    this.m_deckContentsListeners.Add(listener);
  }

  public bool RemoveDeckContentsListener(CollectionManager.DelOnDeckContents listener)
  {
    return this.m_deckContentsListeners.Remove(listener);
  }

  public void RegisterNewCardSeenListener(CollectionManager.DelOnNewCardSeen listener)
  {
    if (this.m_newCardSeenListeners.Contains(listener))
      return;
    this.m_newCardSeenListeners.Add(listener);
  }

  public bool RemoveNewCardSeenListener(CollectionManager.DelOnNewCardSeen listener)
  {
    return this.m_newCardSeenListeners.Remove(listener);
  }

  public void RegisterCardRewardInsertedListener(CollectionManager.DelOnCardRewardInserted listener)
  {
    if (this.m_cardRewardListeners.Contains(listener))
      return;
    this.m_cardRewardListeners.Add(listener);
  }

  public bool RemoveCardRewardInsertedListener(CollectionManager.DelOnCardRewardInserted listener)
  {
    return this.m_cardRewardListeners.Remove(listener);
  }

  public void RegisterMassDisenchantListener(CollectionManager.OnMassDisenchant listener)
  {
    if (this.m_massDisenchantListeners.Contains(listener))
      return;
    this.m_massDisenchantListeners.Add(listener);
  }

  public void RemoveMassDisenchantListener(CollectionManager.OnMassDisenchant listener)
  {
    this.m_massDisenchantListeners.Remove(listener);
  }

  public void RegisterTaggedDeckChanged(CollectionManager.OnTaggedDeckChanged listener)
  {
    this.m_taggedDeckChangedListeners.Add(listener);
  }

  public void RemoveTaggedDeckChanged(CollectionManager.OnTaggedDeckChanged listener)
  {
    this.m_taggedDeckChangedListeners.Remove(listener);
  }

  public bool RegisterDefaultCardbackChangedListener(CollectionManager.DefaultCardbackChangedCallback callback)
  {
    return this.RegisterDefaultCardbackChangedListener(callback, (object) null);
  }

  public bool RegisterDefaultCardbackChangedListener(CollectionManager.DefaultCardbackChangedCallback callback, object userData)
  {
    CollectionManager.DefaultCardbackChangedListener cardbackChangedListener = new CollectionManager.DefaultCardbackChangedListener();
    cardbackChangedListener.SetCallback(callback);
    cardbackChangedListener.SetUserData(userData);
    if (this.m_defaultCardbackChangedListeners.Contains(cardbackChangedListener))
      return false;
    this.m_defaultCardbackChangedListeners.Add(cardbackChangedListener);
    return true;
  }

  public bool RemoveDefaultCardbackChangedListener(CollectionManager.DefaultCardbackChangedCallback callback)
  {
    return this.RemoveDefaultCardbackChangedListener(callback, (object) null);
  }

  public bool RemoveDefaultCardbackChangedListener(CollectionManager.DefaultCardbackChangedCallback callback, object userData)
  {
    CollectionManager.DefaultCardbackChangedListener cardbackChangedListener = new CollectionManager.DefaultCardbackChangedListener();
    cardbackChangedListener.SetCallback(callback);
    cardbackChangedListener.SetUserData(userData);
    return this.m_defaultCardbackChangedListeners.Remove(cardbackChangedListener);
  }

  public bool RegisterFavoriteHeroChangedListener(CollectionManager.FavoriteHeroChangedCallback callback)
  {
    return this.RegisterFavoriteHeroChangedListener(callback, (object) null);
  }

  public bool RegisterFavoriteHeroChangedListener(CollectionManager.FavoriteHeroChangedCallback callback, object userData)
  {
    CollectionManager.FavoriteHeroChangedListener heroChangedListener = new CollectionManager.FavoriteHeroChangedListener();
    heroChangedListener.SetCallback(callback);
    heroChangedListener.SetUserData(userData);
    if (this.m_favoriteHeroChangedListeners.Contains(heroChangedListener))
      return false;
    this.m_favoriteHeroChangedListeners.Add(heroChangedListener);
    return true;
  }

  public bool RemoveFavoriteHeroChangedListener(CollectionManager.FavoriteHeroChangedCallback callback)
  {
    return this.RemoveFavoriteHeroChangedListener(callback, (object) null);
  }

  public bool RemoveFavoriteHeroChangedListener(CollectionManager.FavoriteHeroChangedCallback callback, object userData)
  {
    CollectionManager.FavoriteHeroChangedListener heroChangedListener = new CollectionManager.FavoriteHeroChangedListener();
    heroChangedListener.SetCallback(callback);
    heroChangedListener.SetUserData(userData);
    return this.m_favoriteHeroChangedListeners.Remove(heroChangedListener);
  }

  public TAG_PREMIUM GetBestCardPremium(string cardID)
  {
    CollectibleCard collectibleCard = (CollectibleCard) null;
    return this.m_collectibleCardIndex.TryGetValue(new CollectionManager.CollectibleCardIndex(cardID, TAG_PREMIUM.GOLDEN), out collectibleCard) && collectibleCard.OwnedCount > 0 ? TAG_PREMIUM.GOLDEN : TAG_PREMIUM.NORMAL;
  }

  public CollectibleCard GetCard(string cardID, TAG_PREMIUM premium)
  {
    CollectibleCard collectibleCard = (CollectibleCard) null;
    this.m_collectibleCardIndex.TryGetValue(new CollectionManager.CollectibleCardIndex(cardID, premium), out collectibleCard);
    return collectibleCard;
  }

  public List<CollectibleCard> GetHeroesIOwn(TAG_CLASS heroClass)
  {
    return new List<CollectibleCard>((IEnumerable<CollectibleCard>) this.FindCards((string) null, (List<CollectibleCardFilter.FilterMask>) null, new int?(), (TAG_CARD_SET[]) null, (TAG_CLASS[]) null, (TAG_CARDTYPE[]) null, new TAG_RARITY?(), new TAG_RACE?(), new bool?(true), new int?(1), new bool?(), new bool?(), (CollectionManager.CollectibleCardFilterFunc[]) null, (DeckRuleset) null, false));
  }

  public List<CollectibleCard> GetBestHeroesIOwn(TAG_CLASS heroClass)
  {
    List<CollectibleCard> cards = this.FindCards((string) null, (List<CollectibleCardFilter.FilterMask>) null, new int?(), (TAG_CARD_SET[]) null, new TAG_CLASS[1]
    {
      heroClass
    }, (TAG_CARDTYPE[]) null, new TAG_RARITY?(), new TAG_RACE?(), new bool?(true), new int?(1), new bool?(), new bool?(), (CollectionManager.CollectibleCardFilterFunc[]) null, (DeckRuleset) null, false);
    IEnumerable<CollectibleCard> collectibleCards1 = cards.Where<CollectibleCard>((Func<CollectibleCard, bool>) (h => h.PremiumType == TAG_PREMIUM.GOLDEN));
    IEnumerable<CollectibleCard> collectibleCards2 = cards.Where<CollectibleCard>((Func<CollectibleCard, bool>) (h => h.PremiumType == TAG_PREMIUM.NORMAL));
    List<CollectibleCard> collectibleCardList = new List<CollectibleCard>();
    foreach (CollectibleCard collectibleCard in collectibleCards1)
      collectibleCardList.Add(collectibleCard);
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003CGetBestHeroesIOwn\u003Ec__AnonStorey38B iownCAnonStorey38B = new CollectionManager.\u003CGetBestHeroesIOwn\u003Ec__AnonStorey38B();
    foreach (CollectibleCard collectibleCard in collectibleCards2)
    {
      // ISSUE: reference to a compiler-generated field
      iownCAnonStorey38B.heroCard = collectibleCard;
      // ISSUE: reference to a compiler-generated method
      if (collectibleCardList.Find(new Predicate<CollectibleCard>(iownCAnonStorey38B.\u003C\u003Em__CE)) == null)
      {
        // ISSUE: reference to a compiler-generated field
        collectibleCardList.Add(iownCAnonStorey38B.heroCard);
      }
    }
    return collectibleCardList;
  }

  public NetCache.CardDefinition GetFavoriteHero(TAG_CLASS heroClass)
  {
    NetCache.NetCacheFavoriteHeroes netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFavoriteHeroes>();
    if (netObject == null)
      return (NetCache.CardDefinition) null;
    if (!netObject.FavoriteHeroes.ContainsKey(heroClass))
      return (NetCache.CardDefinition) null;
    return netObject.FavoriteHeroes[heroClass];
  }

  public int GetBasicCardsIOwn(TAG_CLASS cardClass)
  {
    return NetCache.Get().GetNetObject<NetCache.NetCacheCollection>().BasicCardsUnlockedPerClass[cardClass];
  }

  public bool AllCardsInSetOwned(TAG_CARD_SET? cardSet, TAG_CLASS? cardClass, TAG_RARITY? cardRarity, TAG_RACE? cardRace, TAG_PREMIUM? premium)
  {
    TAG_CARD_SET[] tagCardSetArray1;
    if (cardSet.HasValue)
      tagCardSetArray1 = new TAG_CARD_SET[1]
      {
        cardSet.Value
      };
    else
      tagCardSetArray1 = (TAG_CARD_SET[]) null;
    TAG_CARD_SET[] tagCardSetArray2 = tagCardSetArray1;
    TAG_CLASS[] tagClassArray1;
    if (cardClass.HasValue)
      tagClassArray1 = new TAG_CLASS[1]{ cardClass.Value };
    else
      tagClassArray1 = (TAG_CLASS[]) null;
    TAG_CLASS[] tagClassArray2 = tagClassArray1;
    List<CollectibleCardFilter.FilterMask> filterMasks = new List<CollectibleCardFilter.FilterMask>();
    filterMasks.Add(~CollectibleCardFilter.FilterMask.PREMIUM_ALL | CollectibleCardFilter.FilterMaskFromPremiumType(premium));
    TAG_CARD_SET[] theseCardSets = tagCardSetArray2;
    TAG_CLASS[] theseClassTypes = tagClassArray2;
    TAG_RARITY? rarity = cardRarity;
    List<CollectibleCard> cards = this.FindCards((string) null, filterMasks, new int?(), theseCardSets, theseClassTypes, (TAG_CARDTYPE[]) null, rarity, cardRace, new bool?(), new int?(), new bool?(), new bool?(), (CollectionManager.CollectibleCardFilterFunc[]) null, (DeckRuleset) null, false);
    HashSet<int> intSet = new HashSet<int>();
    using (List<CollectibleCard>.Enumerator enumerator = cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectibleCard current = enumerator.Current;
        if (current.OwnedCount == 0)
        {
          if (premium.HasValue || intSet.Contains(current.CardDbId))
            return false;
          intSet.Add(current.CardDbId);
        }
      }
    }
    return true;
  }

  public List<CollectibleCard> GetOwnedCards()
  {
    return new List<CollectibleCard>((IEnumerable<CollectibleCard>) this.FindCards((string) null, (List<CollectibleCardFilter.FilterMask>) null, new int?(), (TAG_CARD_SET[]) null, (TAG_CLASS[]) null, (TAG_CARDTYPE[]) null, new TAG_RARITY?(), new TAG_RACE?(), new bool?(), new int?(1), new bool?(), new bool?(), (CollectionManager.CollectibleCardFilterFunc[]) null, (DeckRuleset) null, false));
  }

  public int GetOwnedCardCount(string cardId, TAG_PREMIUM premium)
  {
    int standard;
    int golden;
    this.GetOwnedCardCount(cardId, out standard, out golden);
    if (premium == TAG_PREMIUM.GOLDEN)
      return golden;
    return standard;
  }

  public void GetOwnedCardCount(string cardId, out int standard, out int golden)
  {
    standard = 0;
    golden = 0;
    CollectibleCard collectibleCard = (CollectibleCard) null;
    if (this.m_collectibleCardIndex.TryGetValue(new CollectionManager.CollectibleCardIndex(cardId, TAG_PREMIUM.NORMAL), out collectibleCard))
      standard = standard + collectibleCard.OwnedCount;
    if (!this.m_collectibleCardIndex.TryGetValue(new CollectionManager.CollectibleCardIndex(cardId, TAG_PREMIUM.GOLDEN), out collectibleCard))
      return;
    golden = golden + collectibleCard.OwnedCount;
  }

  public List<TAG_CARD_SET> GetDisplayableCardSets()
  {
    return this.m_displayableCardSets;
  }

  public bool IsCardInCollection(string cardID, TAG_PREMIUM premium)
  {
    CollectibleCard collectibleCard = (CollectibleCard) null;
    if (this.m_collectibleCardIndex.TryGetValue(new CollectionManager.CollectibleCardIndex(cardID, premium), out collectibleCard))
      return collectibleCard.OwnedCount > 0;
    return false;
  }

  public int GetNumCopiesInCollection(string cardID, TAG_PREMIUM premium)
  {
    CollectibleCard collectibleCard = (CollectibleCard) null;
    this.m_collectibleCardIndex.TryGetValue(new CollectionManager.CollectibleCardIndex(cardID, premium), out collectibleCard);
    if (collectibleCard != null)
      return collectibleCard.OwnedCount;
    return 0;
  }

  public List<CollectibleCard> GetMassDisenchantCards()
  {
    List<CollectibleCard> collectibleCardList = new List<CollectibleCard>();
    using (List<CollectibleCard>.Enumerator enumerator = this.GetOwnedCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectibleCard current = enumerator.Current;
        if (current.DisenchantCount > 0)
          collectibleCardList.Add(current);
      }
    }
    return collectibleCardList;
  }

  public int GetCardsToDisenchantCount()
  {
    int num = 0;
    using (List<CollectibleCard>.Enumerator enumerator = this.GetMassDisenchantCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectibleCard current = enumerator.Current;
        num += current.DisenchantCount;
      }
    }
    return num;
  }

  public void MarkAllInstancesAsSeen(string cardID, TAG_PREMIUM premium)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003CMarkAllInstancesAsSeen\u003Ec__AnonStorey38C seenCAnonStorey38C = new CollectionManager.\u003CMarkAllInstancesAsSeen\u003Ec__AnonStorey38C();
    NetCache.NetCacheCollection netObject = NetCache.Get().GetNetObject<NetCache.NetCacheCollection>();
    int dbId = GameUtils.TranslateCardIdToDbId(cardID);
    if (dbId == 0)
      return;
    // ISSUE: reference to a compiler-generated field
    seenCAnonStorey38C.card = this.GetCard(cardID, premium);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (seenCAnonStorey38C.card.SeenCount == seenCAnonStorey38C.card.OwnedCount)
      return;
    Network.AckCardSeenBefore(dbId, premium);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    seenCAnonStorey38C.card.SeenCount = seenCAnonStorey38C.card.OwnedCount;
    // ISSUE: reference to a compiler-generated method
    NetCache.CardStack cardStack = netObject.Stacks.Find(new Predicate<NetCache.CardStack>(seenCAnonStorey38C.\u003C\u003Em__CF));
    if (cardStack != null)
      cardStack.NumSeen = cardStack.Count;
    using (List<CollectionManager.DelOnNewCardSeen>.Enumerator enumerator = this.m_newCardSeenListeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current(cardID, premium);
    }
  }

  public void OnBoosterOpened(List<NetCache.BoosterCard> cards)
  {
    if (Options.Get().GetBool(Option.FAKE_PACK_OPENING))
      return;
    List<string> cardIDs = new List<string>();
    List<TAG_PREMIUM> cardPremiums = new List<TAG_PREMIUM>();
    List<DateTime> insertDates = new List<DateTime>();
    List<int> counts = new List<int>();
    using (List<NetCache.BoosterCard>.Enumerator enumerator = cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.BoosterCard current = enumerator.Current;
        cardIDs.Add(current.Def.Name);
        cardPremiums.Add(current.Def.Premium);
        insertDates.Add(new DateTime(current.Date));
        counts.Add(1);
      }
    }
    this.InsertNewCollectionCards(cardIDs, cardPremiums, insertDates, counts, false);
    AchieveManager.Get().ValidateAchievesNow();
    this.OnCollectionChanged();
  }

  public void OnCardRewardOpened(string cardID, TAG_PREMIUM premium, int count)
  {
    this.InsertNewCollectionCard(cardID, premium, DateTime.Now, count, false);
    AchieveManager.Get().ValidateAchievesNow();
    this.OnCollectionChanged();
  }

  public CollectionManager.PreconDeck GetPreconDeck(TAG_CLASS heroClass)
  {
    if (!this.m_preconDecks.ContainsKey(heroClass))
      return (CollectionManager.PreconDeck) null;
    return this.m_preconDecks[heroClass];
  }

  public SortedDictionary<long, CollectionDeck> GetDecks()
  {
    SortedDictionary<long, CollectionDeck> sortedDictionary = new SortedDictionary<long, CollectionDeck>();
    using (Map<long, CollectionDeck>.Enumerator enumerator = this.m_decks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, CollectionDeck> current = enumerator.Current;
        if (current.Value != null && (current.Value.Type != DeckType.TAVERN_BRAWL_DECK || TavernBrawlManager.Get().IsTavernBrawlActive && current.Value.SeasonId == TavernBrawlManager.Get().CurrentMission().seasonId))
          sortedDictionary.Add(current.Key, current.Value);
      }
    }
    return sortedDictionary;
  }

  public List<CollectionDeck> GetDecks(DeckType deckType)
  {
    if (!NetCache.Get().IsNetObjectAvailable<NetCache.NetCacheDecks>())
      Debug.LogWarning((object) "Attempting to get decks from CollectionManager, even though NetCacheDecks is not ready (meaning it's waiting for the decks to be updated)!");
    List<CollectionDeck> collectionDeckList = new List<CollectionDeck>();
    using (Map<long, CollectionDeck>.ValueCollection.Enumerator enumerator = this.m_decks.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeck current = enumerator.Current;
        if (current.Type == deckType)
        {
          if (deckType == DeckType.TAVERN_BRAWL_DECK)
          {
            TavernBrawlMission tavernBrawlMission = !TavernBrawlManager.Get().IsTavernBrawlActive ? (TavernBrawlMission) null : TavernBrawlManager.Get().CurrentMission();
            if (tavernBrawlMission == null || current.SeasonId != tavernBrawlMission.seasonId)
              continue;
          }
          collectionDeckList.Add(current);
        }
      }
    }
    collectionDeckList.Sort((IComparer<CollectionDeck>) new CollectionManager.DeckSort());
    return collectionDeckList;
  }

  public List<CollectionDeck> GetDecksWithClass(TAG_CLASS classType, DeckType deckType)
  {
    List<CollectionDeck> decks = this.GetDecks(deckType);
    List<CollectionDeck> collectionDeckList = new List<CollectionDeck>();
    using (List<CollectionDeck>.Enumerator enumerator = decks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeck current = enumerator.Current;
        if (current.GetClass() == classType)
          collectionDeckList.Add(current);
      }
    }
    return collectionDeckList;
  }

  public CollectionDeck GetDeck(long id)
  {
    CollectionDeck collectionDeck;
    if (!this.m_decks.TryGetValue(id, out collectionDeck))
      return (CollectionDeck) null;
    if (collectionDeck != null && collectionDeck.Type == DeckType.TAVERN_BRAWL_DECK)
    {
      TavernBrawlMission tavernBrawlMission = !TavernBrawlManager.Get().IsTavernBrawlActive ? (TavernBrawlMission) null : TavernBrawlManager.Get().CurrentMission();
      if (tavernBrawlMission == null || collectionDeck.SeasonId != tavernBrawlMission.seasonId)
        return (CollectionDeck) null;
    }
    return collectionDeck;
  }

  public bool AreAllDeckContentsReady()
  {
    return FixedRewardsMgr.Get().IsStartupFinished() && this.m_decks.FirstOrDefault<KeyValuePair<long, CollectionDeck>>((Func<KeyValuePair<long, CollectionDeck>, bool>) (kv =>
    {
      if (!kv.Value.NetworkContentsLoaded())
        return kv.Value.Type != DeckType.TAVERN_BRAWL_DECK;
      return false;
    })).Value == null;
  }

  public bool ShouldAccountSeeStandardWild()
  {
    return AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES) && (this.AccountEverHadWildCards() || this.AccountHasRotatedItems());
  }

  public bool AccountHasRotatedItems()
  {
    if (this.m_accountHasRotatedItems)
      return true;
    NetCache.NetCacheBoosters netObject = NetCache.Get().GetNetObject<NetCache.NetCacheBoosters>();
    if (netObject != null)
    {
      using (List<NetCache.BoosterStack>.Enumerator enumerator = netObject.BoosterStacks.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (GameUtils.IsBoosterRotated((BoosterDbId) enumerator.Current.Id))
          {
            this.m_accountHasRotatedItems = true;
            return true;
          }
        }
      }
    }
    using (HashSet<AdventureDbId>.Enumerator enumerator = GameUtils.GetRotatedAdventures().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (AdventureProgressMgr.Get().OwnsOneOrMoreAdventureWings(enumerator.Current))
        {
          this.m_accountHasRotatedItems = true;
          return true;
        }
      }
    }
    return false;
  }

  public bool AccountEverHadWildCards()
  {
    if (this.m_accountEverHadWildCards)
      return true;
    this.m_accountEverHadWildCards = GameUtils.HasSeenStandardModeTutorial() || this.AccountHasWildCards();
    return this.m_accountEverHadWildCards;
  }

  public bool AccountHasWildCards()
  {
    if (!GameUtils.IsAnythingRotated())
      return false;
    if (this.GetNumberOfWildDecks() > 0)
      return true;
    if ((double) this.m_lastSearchForWildCardsTime > (double) this.m_collectionLastModifiedTime)
      return this.m_accountHasWildCards;
    this.m_accountHasWildCards = this.m_collectibleCards.Any<CollectibleCard>((Func<CollectibleCard, bool>) (c =>
    {
      if (c.OwnedCount > 0)
        return GameUtils.IsCardRotated(c.GetEntityDef());
      return false;
    }));
    this.m_lastSearchForWildCardsTime = Time.realtimeSinceStartup;
    return this.m_accountHasWildCards;
  }

  public int GetNumberOfWildDecks()
  {
    int num = 0;
    using (Map<long, CollectionDeck>.Enumerator enumerator = this.m_decks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.IsWild)
          ++num;
      }
    }
    return num;
  }

  public int GetNumberOfStandardDecks()
  {
    int num = 0;
    using (Map<long, CollectionDeck>.Enumerator enumerator = this.m_decks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (!enumerator.Current.Value.IsWild)
          ++num;
      }
    }
    return num;
  }

  public bool AccountHasValidStandardDeck()
  {
    using (List<CollectionDeck>.Enumerator enumerator = CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeck current = enumerator.Current;
        if (current.IsTourneyValid && !current.IsWild)
          return true;
      }
    }
    return false;
  }

  public bool AccountHasAnyValidDeck()
  {
    using (List<CollectionDeck>.Enumerator enumerator = CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.IsTourneyValid)
          return true;
      }
    }
    return false;
  }

  public CollectionDeck GetTaggedDeck(CollectionManager.DeckTag tag)
  {
    CollectionDeck collectionDeck = (CollectionDeck) null;
    if (this.m_taggedDecks.TryGetValue(tag, out collectionDeck) && collectionDeck != null && collectionDeck.Type == DeckType.TAVERN_BRAWL_DECK)
    {
      TavernBrawlMission tavernBrawlMission = !TavernBrawlManager.Get().IsTavernBrawlActive ? (TavernBrawlMission) null : TavernBrawlManager.Get().CurrentMission();
      if (tavernBrawlMission == null || collectionDeck.SeasonId != tavernBrawlMission.seasonId)
        return (CollectionDeck) null;
    }
    return collectionDeck;
  }

  public CollectionDeck GetEditedDeck()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.DRAFT)
      return this.GetTaggedDeck(CollectionManager.DeckTag.Arena);
    return this.GetTaggedDeck(CollectionManager.DeckTag.Editing);
  }

  public int GetDeckSize()
  {
    if (this.m_deckRuleset == null)
      return 30;
    return this.m_deckRuleset.GetDeckSize();
  }

  public List<CollectionManager.TemplateDeck> GetTemplateDecks(TAG_CLASS classType)
  {
    List<CollectionManager.TemplateDeck> templateDeckList = (List<CollectionManager.TemplateDeck>) null;
    this.m_templateDecks.TryGetValue(classType, out templateDeckList);
    return templateDeckList;
  }

  public bool IsInEditMode()
  {
    return this.m_editMode;
  }

  public CollectionDeck StartEditingDeck(CollectionManager.DeckTag tag, long deckId, object callbackData = null)
  {
    this.m_editMode = true;
    CollectionDeck deck = this.GetDeck(deckId);
    DeckRuleset deckRuleset;
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL)
    {
      deckRuleset = !deck.IsWild ? DeckRuleset.GetStandardRuleset() : DeckRuleset.GetWildRuleset();
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.DECKEDITOR);
    }
    else
      deckRuleset = TavernBrawlManager.Get().GetDeckRuleset();
    this.SetDeckRuleset(deckRuleset);
    CollectionManagerDisplay.Get().OnStartEditingDeck(deck.IsWild);
    return this.SetTaggedDeck(tag, deckId, callbackData);
  }

  public void DoneEditing()
  {
    bool editMode = this.m_editMode;
    this.m_editMode = false;
    if (editMode && (UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null && SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL)
      PresenceMgr.Get().SetPrevStatus();
    this.SetDeckRuleset((DeckRuleset) null);
    this.ClearTaggedDeck(CollectionManager.DeckTag.Editing);
  }

  public DeckRuleset GetDeckRuleset()
  {
    return this.m_deckRuleset;
  }

  public bool IsShowingWildTheming(CollectionDeck deck = null)
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
      return false;
    if (deck == null)
      deck = this.GetEditedDeck();
    if (deck != null)
      return deck.IsWild;
    return (UnityEngine.Object) CollectionManagerDisplay.Get() != (UnityEngine.Object) null && CollectionManagerDisplay.Get().SetFilterTrayInitialized() && CollectionManagerDisplay.Get().m_pageManager.CardSetFilterIncludesWild();
  }

  public void SetDeckRuleset(DeckRuleset deckRuleset)
  {
    this.m_deckRuleset = deckRuleset;
    if (!((UnityEngine.Object) CollectionManagerDisplay.Get() != (UnityEngine.Object) null))
      return;
    CollectionManagerDisplay.Get().m_pageManager.SetDeckRuleset(deckRuleset, false);
  }

  public CollectionDeck SetTaggedDeck(CollectionManager.DeckTag tag, long deckId, object callbackData = null)
  {
    CollectionDeck deck = (CollectionDeck) null;
    this.m_decks.TryGetValue(deckId, out deck);
    this.SetTaggedDeck(tag, deck, callbackData);
    return deck;
  }

  public void SetTaggedDeck(CollectionManager.DeckTag tag, CollectionDeck deck, object callbackData = null)
  {
    CollectionDeck taggedDeck = this.GetTaggedDeck(tag);
    if (deck == taggedDeck)
      return;
    this.m_taggedDecks[tag] = deck;
    foreach (CollectionManager.OnTaggedDeckChanged taggedDeckChanged in this.m_taggedDeckChangedListeners.ToArray())
      taggedDeckChanged(tag, deck, taggedDeck, callbackData);
  }

  public void ClearTaggedDeck(CollectionManager.DeckTag tag)
  {
    this.SetTaggedDeck(tag, (CollectionDeck) null, (object) null);
  }

  public void SendCreateDeck(DeckType deckType, string name, string heroCardID)
  {
    int dbId = GameUtils.TranslateCardIdToDbId(heroCardID);
    if (dbId == 0)
    {
      Debug.LogWarning((object) string.Format("CollectionManager.SendCreateDeck(): Unknown hero cardID {0}", (object) heroCardID));
    }
    else
    {
      bool isWild = Options.Get().GetBool(Option.IN_WILD_MODE);
      if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
        isWild = true;
      TAG_PREMIUM bestCardPremium = this.GetBestCardPremium(heroCardID);
      Network.Get().CreateDeck(deckType, name, dbId, bestCardPremium, isWild, TimeUtils.GetEpochTime(), DeckSourceType.DECK_SOURCE_TYPE_NORMAL);
    }
  }

  public void SendDeleteDeck(long id)
  {
    Network.Get().DeleteDeck(id);
  }

  public bool RequestDeckContentsForDecksWithoutContentsLoaded(CollectionManager.DelOnAllDeckContents callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003CRequestDeckContentsForDecksWithoutContentsLoaded\u003Ec__AnonStorey38D loadedCAnonStorey38D = new CollectionManager.\u003CRequestDeckContentsForDecksWithoutContentsLoaded\u003Ec__AnonStorey38D();
    // ISSUE: reference to a compiler-generated field
    loadedCAnonStorey38D.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    loadedCAnonStorey38D.now = Time.realtimeSinceStartup;
    IEnumerable<KeyValuePair<long, CollectionDeck>> source1 = this.m_decks.Where<KeyValuePair<long, CollectionDeck>>((Func<KeyValuePair<long, CollectionDeck>, bool>) (kv => !kv.Value.NetworkContentsLoaded()));
    if (!TavernBrawlManager.Get().IsTavernBrawlActive)
      source1 = source1.Where<KeyValuePair<long, CollectionDeck>>((Func<KeyValuePair<long, CollectionDeck>, bool>) (kv => kv.Value.Type != DeckType.TAVERN_BRAWL_DECK));
    if (!source1.Any<KeyValuePair<long, CollectionDeck>>())
    {
      if (callback != null)
        callback();
      return false;
    }
    if (callback != null && !this.m_allDeckContentsListeners.Contains(callback))
      this.m_allDeckContentsListeners.Add(callback);
    if (this.m_pendingRequestDeckContents != null)
    {
      // ISSUE: reference to a compiler-generated method
      source1 = source1.Where<KeyValuePair<long, CollectionDeck>>(new Func<KeyValuePair<long, CollectionDeck>, bool>(loadedCAnonStorey38D.\u003C\u003Em__D4));
    }
    IEnumerable<long> source2 = source1.Select<KeyValuePair<long, CollectionDeck>, long>((Func<KeyValuePair<long, CollectionDeck>, long>) (kv => kv.Value.ID));
    if (!source2.Any<long>())
      return true;
    long[] array = source2.ToArray<long>();
    if (this.m_pendingRequestDeckContents == null)
      this.m_pendingRequestDeckContents = new Map<long, float>();
    for (int index = 0; index < array.Length; ++index)
    {
      // ISSUE: reference to a compiler-generated field
      this.m_pendingRequestDeckContents[array[index]] = loadedCAnonStorey38D.now;
    }
    Network.Get().RequestDeckContents(array);
    return true;
  }

  public void RequestDeckContents(long id)
  {
    CollectionDeck deck = this.GetDeck(id);
    if (deck != null && deck.NetworkContentsLoaded())
    {
      this.FireDeckContentsEvent(id);
    }
    else
    {
      float realtimeSinceStartup = Time.realtimeSinceStartup;
      float num;
      if (this.m_pendingRequestDeckContents != null && this.m_pendingRequestDeckContents.TryGetValue(id, out num))
      {
        if ((double) realtimeSinceStartup - (double) num < 10.0)
          return;
        this.m_pendingRequestDeckContents.Remove(id);
      }
      if (this.m_pendingRequestDeckContents == null)
        this.m_pendingRequestDeckContents = new Map<long, float>();
      this.m_pendingRequestDeckContents[id] = realtimeSinceStartup;
      Network.Get().RequestDeckContents(id);
    }
  }

  public CollectionDeck GetBaseDeck(long id)
  {
    CollectionDeck collectionDeck;
    if (this.m_baseDecks.TryGetValue(id, out collectionDeck))
      return collectionDeck;
    return (CollectionDeck) null;
  }

  public string AutoGenerateDeckName(TAG_CLASS classTag)
  {
    string className = GameStrings.GetClassName(classTag);
    int num = 1;
    string name;
    do
    {
      if (num == 1)
        name = GameStrings.Format("GLUE_COLLECTION_CUSTOM_DECKNAME_TEMPLATE", (object) className, (object) string.Empty);
      else
        name = GameStrings.Format("GLUE_COLLECTION_CUSTOM_DECKNAME_TEMPLATE", (object) className, (object) num);
      ++num;
    }
    while (this.IsDeckNameTaken(name));
    return name;
  }

  public string GetVanillaHeroCardIDFromClass(TAG_CLASS heroClass)
  {
    string empty = string.Empty;
    switch (heroClass)
    {
      case TAG_CLASS.DRUID:
        return "HERO_06";
      case TAG_CLASS.HUNTER:
        return "HERO_05";
      case TAG_CLASS.MAGE:
        return "HERO_08";
      case TAG_CLASS.PALADIN:
        return "HERO_04";
      case TAG_CLASS.PRIEST:
        return "HERO_09";
      case TAG_CLASS.ROGUE:
        return "HERO_03";
      case TAG_CLASS.SHAMAN:
        return "HERO_02";
      case TAG_CLASS.WARLOCK:
        return "HERO_07";
      case TAG_CLASS.WARRIOR:
        return "HERO_01";
      default:
        return string.Empty;
    }
  }

  public string GetVanillaHeroCardID(EntityDef HeroSkinEntityDef)
  {
    return CollectionManager.Get().GetVanillaHeroCardIDFromClass(HeroSkinEntityDef.GetClass());
  }

  public bool ShouldShowDeckTemplatePageForClass(TAG_CLASS classType)
  {
    return (Options.Get().GetInt(Option.SKIP_DECK_TEMPLATE_PAGE_FOR_CLASS_FLAGS, 0) & 1 << (int) (classType & (TAG_CLASS) 31)) == 0;
  }

  public void SetShowDeckTemplatePageForClass(TAG_CLASS classType, bool show)
  {
    int num1 = Options.Get().GetInt(Option.SKIP_DECK_TEMPLATE_PAGE_FOR_CLASS_FLAGS, 0);
    int num2 = 1 << (int) (classType & (TAG_CLASS) 31);
    int val = num1 | num2;
    if (show)
      val ^= num2;
    Options.Get().SetInt(Option.SKIP_DECK_TEMPLATE_PAGE_FOR_CLASS_FLAGS, val);
  }

  public bool ShouldShowWildToStandardTutorial(bool checkPrevSceneIsPlayMode = true)
  {
    if (!this.ShouldAccountSeeStandardWild() || SceneMgr.Get().GetMode() != SceneMgr.Mode.COLLECTIONMANAGER || checkPrevSceneIsPlayMode && SceneMgr.Get().GetPrevMode() != SceneMgr.Mode.TOURNAMENT)
      return false;
    return Options.Get().GetBool(Option.NEEDS_TO_MAKE_STANDARD_DECK);
  }

  private void InitImpl()
  {
    using (List<string>.Enumerator enumerator = GameUtils.GetAllCollectibleCardIds().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        EntityDef entityDef = DefLoader.Get().GetEntityDef(current);
        if (entityDef == null)
        {
          Error.AddDevFatal("Failed to find an EntityDef for collectible card {0}", (object) current);
          return;
        }
        this.RegisterCard(entityDef, current, TAG_PREMIUM.NORMAL);
        if (entityDef.GetCardSet() != TAG_CARD_SET.HERO_SKINS)
          this.RegisterCard(entityDef, current, TAG_PREMIUM.GOLDEN);
      }
    }
    Network network = Network.Get();
    network.RegisterNetHandler((object) GetDeckContentsResponse.PacketID.ID, new Network.NetHandler(this.OnGetDeckContentsResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.DBAction.PacketID.ID, new Network.NetHandler(this.OnDBAction), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) DeckCreated.PacketID.ID, new Network.NetHandler(this.OnDeckCreated), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) DeckDeleted.PacketID.ID, new Network.NetHandler(this.OnDeckDeleted), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) DeckRenamed.PacketID.ID, new Network.NetHandler(this.OnDeckRenamed), (Network.TimeoutHandler) null);
    NetCache.Get().RegisterCollectionManager(new NetCache.NetCacheCallback(this.OnNetCacheReady));
  }

  private void WillReset()
  {
    NetCache.Get().RemoveUpdatedListener(typeof (NetCache.NetCacheDecks), new Action(CollectionManager.s_instance.NetCache_OnDecksReceived));
    this.m_decks.Clear();
    this.m_baseDecks.Clear();
    this.m_preconDecks.Clear();
    this.m_taggedDecks.Clear();
    this.m_defaultCardbackChangedListeners.Clear();
    this.m_favoriteHeroChangedListeners.Clear();
    this.m_templateDecks.Clear();
    this.m_templateDeckMap.Clear();
    this.m_collectibleCards = new List<CollectibleCard>();
    this.m_collectibleCardIndex = new Map<CollectionManager.CollectibleCardIndex, CollectibleCard>();
    this.m_collectionLastModifiedTime = 0.0f;
    this.m_cardStacksRegistered = false;
    this.m_lastSearchForWildCardsTime = 0.0f;
    this.m_accountEverHadWildCards = false;
    this.m_accountHasRotatedItems = false;
    if (!this.m_creatingInnkeeperDeck)
      return;
    this.m_creatingInnkeeperDeck = false;
  }

  private void OnCollectionChanged()
  {
    foreach (CollectionManager.DelOnCollectionChanged collectionChanged in this.m_collectionChangedListeners.ToArray())
      collectionChanged();
  }

  private List<string> GetCardIDsInSet(TAG_CARD_SET? cardSet, TAG_CLASS? cardClass, TAG_RARITY? cardRarity, TAG_RACE? cardRace)
  {
    List<string> collectibleCardIds = GameUtils.GetNonHeroCollectibleCardIds();
    List<string> stringList = new List<string>();
    using (List<string>.Enumerator enumerator = collectibleCardIds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        EntityDef entityDef = DefLoader.Get().GetEntityDef(current);
        if ((!cardClass.HasValue || cardClass.Value == entityDef.GetClass()) && (!cardRarity.HasValue || cardRarity.Value == entityDef.GetRarity()) && ((!cardSet.HasValue || cardSet.Value == entityDef.GetCardSet()) && (!cardRace.HasValue || cardRace.Value == entityDef.GetRace())))
          stringList.Add(current);
      }
    }
    return stringList;
  }

  public int NumCardsOwnedInSet(TAG_CARD_SET cardSet)
  {
    List<CollectibleCard> cards = this.FindCards((string) null, (List<CollectibleCardFilter.FilterMask>) null, new int?(), new TAG_CARD_SET[1]
    {
      cardSet
    }, (TAG_CLASS[]) null, (TAG_CARDTYPE[]) null, new TAG_RARITY?(), new TAG_RACE?(), new bool?(), new int?(1), new bool?(), new bool?(), (CollectionManager.CollectibleCardFilterFunc[]) null, (DeckRuleset) null, false);
    int num = 0;
    using (List<CollectibleCard>.Enumerator enumerator = cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectibleCard current = enumerator.Current;
        num += current.OwnedCount;
      }
    }
    return num;
  }

  private CollectibleCard RegisterCard(EntityDef entityDef, string cardID, TAG_PREMIUM premium)
  {
    CollectionManager.CollectibleCardIndex key = new CollectionManager.CollectibleCardIndex(cardID, premium);
    CollectibleCard collectibleCard = (CollectibleCard) null;
    if (!this.m_collectibleCardIndex.TryGetValue(key, out collectibleCard))
    {
      collectibleCard = new CollectibleCard(GameUtils.GetCardRecord(cardID), entityDef, premium);
      this.m_collectibleCards.Add(collectibleCard);
      this.m_collectibleCardIndex.Add(key, collectibleCard);
    }
    return collectibleCard;
  }

  private CollectibleCard AddCounts(NetCache.CardStack netStack, EntityDef entityDef)
  {
    return this.AddCounts(entityDef, netStack.Def.Name, netStack.Def.Premium, new DateTime(netStack.Date), netStack.Count, netStack.NumSeen);
  }

  private CollectibleCard AddCounts(EntityDef entityDef, string cardID, TAG_PREMIUM premium, DateTime insertDate, int count, int numSeen)
  {
    if (entityDef == null)
    {
      Debug.LogError((object) string.Format("CollectionManager.RegisterCardStack(): DefLoader failed to get entity def for {0}", (object) cardID));
      return (CollectibleCard) null;
    }
    this.m_collectionLastModifiedTime = Time.realtimeSinceStartup;
    CollectibleCard collectibleCard = this.RegisterCard(entityDef, cardID, premium);
    collectibleCard.AddCounts(count, numSeen, insertDate);
    return collectibleCard;
  }

  private void AddPreconDeckFromNotice(NetCache.ProfileNoticePreconDeck preconDeckNotice)
  {
    EntityDef entityDef = DefLoader.Get().GetEntityDef(preconDeckNotice.HeroAsset);
    if (entityDef == null)
      return;
    this.AddPreconDeck(entityDef.GetClass(), preconDeckNotice.DeckID);
    NetCache.NetCacheDecks netObject = NetCache.Get().GetNetObject<NetCache.NetCacheDecks>();
    if (netObject == null)
      return;
    NetCache.DeckHeader deckHeader = new NetCache.DeckHeader()
    {
      ID = preconDeckNotice.DeckID,
      Name = "precon",
      Hero = entityDef.GetCardId(),
      HeroPower = GameUtils.GetHeroPowerCardIdFromHero(preconDeckNotice.HeroAsset),
      Type = DeckType.PRECON_DECK,
      SortOrder = preconDeckNotice.DeckID,
      SourceType = DeckSourceType.DECK_SOURCE_TYPE_BASIC_DECK
    };
    netObject.Decks.Add(deckHeader);
    Network.AckNotice(preconDeckNotice.NoticeID);
  }

  private void AddPreconDeck(TAG_CLASS heroClass, long deckID)
  {
    if (this.m_preconDecks.ContainsKey(heroClass))
    {
      Debug.LogWarning((object) string.Format("CollectionManager.AddPreconDeck(): Already have a precon deck for class {0}, cannot add deckID {1}", (object) heroClass, (object) deckID));
    }
    else
    {
      Log.Rachelle.Print(string.Format("CollectionManager.AddPreconDeck() heroClass={0} deckID={1}", (object) heroClass, (object) deckID));
      this.m_preconDecks[heroClass] = new CollectionManager.PreconDeck(deckID);
    }
  }

  private CollectionDeck AddDeck(NetCache.DeckHeader deckHeader)
  {
    return this.AddDeck(deckHeader, true);
  }

  private CollectionDeck AddDeck(NetCache.DeckHeader deckHeader, bool updateNetCache)
  {
    if (deckHeader.Type != DeckType.NORMAL_DECK && deckHeader.Type != DeckType.TAVERN_BRAWL_DECK)
    {
      Debug.LogWarning((object) string.Format("CollectionManager.AddDeck(): deckHeader {0} is not of type NORMAL_DECK or TAVERN_BRAWL_DECK", (object) deckHeader));
      return (CollectionDeck) null;
    }
    ulong num = (ulong) deckHeader.ID;
    if (deckHeader.CreateDate.HasValue)
      num = TimeUtils.DateTimeToUnixTimeStamp(deckHeader.CreateDate.Value);
    CollectionDeck collectionDeck1 = new CollectionDeck()
    {
      ID = deckHeader.ID,
      Type = deckHeader.Type,
      Name = deckHeader.Name,
      HeroCardID = deckHeader.Hero,
      HeroPremium = deckHeader.HeroPremium,
      HeroOverridden = deckHeader.HeroOverridden,
      CardBackID = deckHeader.CardBack,
      CardBackOverridden = deckHeader.CardBackOverridden,
      SeasonId = deckHeader.SeasonId,
      NeedsName = deckHeader.NeedsName,
      SortOrder = deckHeader.SortOrder,
      IsWild = deckHeader.IsWild,
      SourceType = deckHeader.SourceType,
      CreateDate = num,
      Locked = deckHeader.Locked
    };
    if (collectionDeck1.NeedsName && string.IsNullOrEmpty(collectionDeck1.Name))
    {
      collectionDeck1.Name = GameStrings.Format("GLOBAL_BASIC_DECK_NAME", (object) GameStrings.GetClassName(collectionDeck1.GetClass()));
      Log.JMac.Print(string.Format("Set deck name to {0}", (object) collectionDeck1.Name));
    }
    this.m_decks.Add(deckHeader.ID, collectionDeck1);
    CollectionDeck collectionDeck2 = new CollectionDeck()
    {
      ID = deckHeader.ID,
      Type = deckHeader.Type,
      Name = deckHeader.Name,
      HeroCardID = deckHeader.Hero,
      HeroPremium = deckHeader.HeroPremium,
      HeroOverridden = deckHeader.HeroOverridden,
      CardBackID = deckHeader.CardBack,
      CardBackOverridden = deckHeader.CardBackOverridden,
      SeasonId = deckHeader.SeasonId,
      NeedsName = deckHeader.NeedsName,
      SortOrder = deckHeader.SortOrder,
      IsWild = deckHeader.IsWild,
      SourceType = deckHeader.SourceType
    };
    this.m_baseDecks.Add(deckHeader.ID, collectionDeck2);
    if (updateNetCache)
      NetCache.Get().GetNetObject<NetCache.NetCacheDecks>().Decks.Add(deckHeader);
    return collectionDeck1;
  }

  private void RemoveDeck(long id)
  {
    this.m_decks.Remove(id);
    this.m_baseDecks.Remove(id);
    NetCache.NetCacheDecks netObject = NetCache.Get().GetNetObject<NetCache.NetCacheDecks>();
    for (int index = 0; index < netObject.Decks.Count; ++index)
    {
      if (netObject.Decks[index].ID == id)
      {
        netObject.Decks.RemoveAt(index);
        break;
      }
    }
  }

  private bool IsDeckNameTaken(string name)
  {
    using (SortedDictionary<long, CollectionDeck>.ValueCollection.Enumerator enumerator = this.GetDecks().Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Name.Trim().Equals(name, StringComparison.InvariantCultureIgnoreCase))
          return true;
      }
    }
    return false;
  }

  private void FireDeckContentsEvent(long id)
  {
    foreach (CollectionManager.DelOnDeckContents delOnDeckContents in this.m_deckContentsListeners.ToArray())
      delOnDeckContents(id);
  }

  private void FireAllDeckContentsEvent()
  {
    CollectionManager.DelOnAllDeckContents[] array = this.m_allDeckContentsListeners.ToArray();
    this.m_allDeckContentsListeners.Clear();
    foreach (CollectionManager.DelOnAllDeckContents onAllDeckContents in array)
      onAllDeckContents();
  }

  private void OnNetCacheReady()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    Log.Rachelle.Print("CollectionManager.OnNetCacheReady");
    using (List<string>.Enumerator enumerator = GameUtils.GetNonHeroCollectibleCardIds().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TAG_CARD_SET cardSet = DefLoader.Get().GetEntityDef(enumerator.Current).GetCardSet();
        if (!this.m_displayableCardSets.Contains(cardSet))
          this.m_displayableCardSets.Add(cardSet);
      }
    }
    AchieveManager.InitRequests();
    this.UpdateCardsWithNetData();
    using (List<NetCache.DeckHeader>.Enumerator enumerator = NetCache.Get().GetNetObject<NetCache.NetCacheDecks>().Decks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.DeckHeader current = enumerator.Current;
        switch (current.Type)
        {
          case DeckType.NORMAL_DECK:
          case DeckType.TAVERN_BRAWL_DECK:
            this.AddDeck(current, false);
            continue;
          case DeckType.PRECON_DECK:
            this.AddPreconDeck(DefLoader.Get().GetEntityDef(current.Hero).GetClass(), current.ID);
            continue;
          default:
            Debug.LogWarning((object) string.Format("CollectionManager.OnNetCacheReady(): don't know how to handle deck type {0}", (object) current.Type));
            continue;
        }
      }
    }
    NetCache.Get().RegisterUpdatedListener(typeof (NetCache.NetCacheDecks), new Action(CollectionManager.s_instance.NetCache_OnDecksReceived));
    this.UpdateShowAdvancedCMOption();
    foreach (CollectionManager.DelOnCollectionLoaded collectionLoaded in this.m_collectionLoadedListeners.ToArray())
      collectionLoaded();
    NetCache.Get().RegisterNewNoticesListener(new NetCache.DelNewNoticesListener(this.OnNewNotices));
    this.m_collectionLoaded = true;
    this.LoadTemplateDecks();
  }

  public void FixedRewardsStartupComplete()
  {
  }

  private void OnNewNotices(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList)
  {
    List<NetCache.ProfileNotice> all = newNotices.FindAll((Predicate<NetCache.ProfileNotice>) (obj => obj.Type == NetCache.ProfileNotice.NoticeType.PRECON_DECK));
    bool flag = false;
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = all.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        this.AddPreconDeckFromNotice(enumerator.Current as NetCache.ProfileNoticePreconDeck);
        flag = true;
      }
    }
    if (!flag)
      return;
    NetCache.Get().ReloadNetObject<NetCache.NetCacheDecks>();
  }

  private void UpdateShowAdvancedCMOption()
  {
    if (Options.Get().GetBool(Option.SHOW_ADVANCED_COLLECTIONMANAGER, false) || NetCache.Get().GetNetObject<NetCache.NetCacheCollection>().TotalCardsOwned < 116)
      return;
    Options.Get().SetBool(Option.SHOW_ADVANCED_COLLECTIONMANAGER, true);
  }

  private void UpdateDeckHeroArt(string heroCardID)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003CUpdateDeckHeroArt\u003Ec__AnonStorey38E artCAnonStorey38E = new CollectionManager.\u003CUpdateDeckHeroArt\u003Ec__AnonStorey38E();
    // ISSUE: reference to a compiler-generated field
    artCAnonStorey38E.heroCardID = heroCardID;
    // ISSUE: reference to a compiler-generated field
    TAG_PREMIUM bestCardPremium = this.GetBestCardPremium(artCAnonStorey38E.heroCardID);
    // ISSUE: reference to a compiler-generated field
    TAG_CLASS heroClass = DefLoader.Get().GetEntityDef(artCAnonStorey38E.heroCardID).GetClass();
    CollectionManager.PreconDeck preconDeck = this.GetPreconDeck(heroClass);
    if (preconDeck != null)
      this.m_preconDecks[heroClass] = new CollectionManager.PreconDeck(preconDeck.ID);
    // ISSUE: reference to a compiler-generated method
    using (List<CollectionDeck>.Enumerator enumerator = this.m_baseDecks.Values.ToList<CollectionDeck>().FindAll(new Predicate<CollectionDeck>(artCAnonStorey38E.\u003C\u003Em__D7)).GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.HeroPremium = bestCardPremium;
    }
    // ISSUE: reference to a compiler-generated method
    using (List<CollectionDeck>.Enumerator enumerator = this.m_decks.Values.ToList<CollectionDeck>().FindAll(new Predicate<CollectionDeck>(artCAnonStorey38E.\u003C\u003Em__D8)).GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.HeroPremium = bestCardPremium;
    }
  }

  private void InsertNewCollectionCard(string cardID, TAG_PREMIUM premium, DateTime insertDate, int count, bool seenBefore)
  {
    EntityDef entityDef = DefLoader.Get().GetEntityDef(cardID);
    int numSeen = !seenBefore ? 0 : count;
    CollectibleCard collectibleCard = this.AddCounts(entityDef, cardID, premium, insertDate, count, numSeen);
    if (entityDef.IsHero())
    {
      if (premium != TAG_PREMIUM.GOLDEN)
        return;
      this.UpdateDeckHeroArt(cardID);
    }
    else
    {
      this.NotifyNetCacheOfNewCards(new NetCache.CardDefinition()
      {
        Name = cardID,
        Premium = premium
      }, insertDate.Ticks, count, seenBefore);
      AchieveManager.Get().NotifyOfCardGained(entityDef, premium, collectibleCard.OwnedCount);
      this.UpdateShowAdvancedCMOption();
    }
  }

  private void InsertNewCollectionCards(List<string> cardIDs, List<TAG_PREMIUM> cardPremiums, List<DateTime> insertDates, List<int> counts, bool seenBefore)
  {
    List<EntityDef> entityDefs = new List<EntityDef>();
    bool hasGolden = false;
    for (int index = 0; index < cardIDs.Count; ++index)
    {
      string cardId = cardIDs[index];
      TAG_PREMIUM cardPremium = cardPremiums[index];
      DateTime insertDate = insertDates[index];
      int count = counts[index];
      EntityDef entityDef = DefLoader.Get().GetEntityDef(cardId);
      int numSeen = !seenBefore ? 0 : count;
      this.AddCounts(entityDef, cardId, cardPremium, insertDate, count, numSeen);
      hasGolden |= cardPremium == TAG_PREMIUM.GOLDEN;
      if (entityDef.IsHero())
      {
        if (cardPremium == TAG_PREMIUM.GOLDEN)
          this.UpdateDeckHeroArt(cardId);
      }
      else
      {
        this.NotifyNetCacheOfNewCards(new NetCache.CardDefinition()
        {
          Name = cardId,
          Premium = cardPremium
        }, insertDate.Ticks, count, seenBefore);
        entityDefs.Add(entityDef);
        this.UpdateShowAdvancedCMOption();
      }
    }
    AchieveManager.Get().NotifyOfCardsGained(entityDefs, hasGolden);
  }

  private void RemoveCollectionCard(string cardID, TAG_PREMIUM premium, int count)
  {
    CollectibleCard card = this.GetCard(cardID, premium);
    card.RemoveCounts(count);
    this.m_collectionLastModifiedTime = Time.realtimeSinceStartup;
    int ownedCount = card.OwnedCount;
    using (SortedDictionary<long, CollectionDeck>.ValueCollection.Enumerator enumerator = this.GetDecks().Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeck current = enumerator.Current;
        if (!current.Locked)
        {
          int firstMatchingSlot = current.GetCardCountFirstMatchingSlot(cardID, premium);
          if (firstMatchingSlot > ownedCount)
          {
            int num = firstMatchingSlot - ownedCount;
            for (int index = 0; index < num; ++index)
              current.RemoveCard(cardID, premium, true, false);
            if (!CollectionDeckTray.Get().HandleDeletedCardDeckUpdate(cardID))
              current.SendChanges();
          }
        }
      }
    }
    this.NotifyNetCacheOfRemovedCards(new NetCache.CardDefinition()
    {
      Name = cardID,
      Premium = premium
    }, count);
  }

  private void UpdateCardCounts(NetCache.NetCacheCollection netCacheCards, NetCache.CardDefinition cardDef, int count, int newCount)
  {
    netCacheCards.TotalCardsOwned += count;
    if (cardDef.Premium != TAG_PREMIUM.NORMAL)
      return;
    EntityDef entityDef = DefLoader.Get().GetEntityDef(cardDef.Name);
    if (!entityDef.IsBasicCardUnlock())
      return;
    int num = !entityDef.IsElite() ? 2 : 1;
    if (newCount < 0 || newCount > num)
    {
      Debug.LogError((object) ("CollectionManager.UpdateCardCounts: created an illegal stack size of " + (object) newCount + " for card " + (object) entityDef));
      count = 0;
    }
    Map<TAG_CLASS, int> unlockedPerClass;
    TAG_CLASS index;
    (unlockedPerClass = netCacheCards.BasicCardsUnlockedPerClass)[index = entityDef.GetClass()] = unlockedPerClass[index] + count;
  }

  private void NotifyNetCacheOfRemovedCards(NetCache.CardDefinition cardDef, int count)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003CNotifyNetCacheOfRemovedCards\u003Ec__AnonStorey38F cardsCAnonStorey38F = new CollectionManager.\u003CNotifyNetCacheOfRemovedCards\u003Ec__AnonStorey38F();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey38F.cardDef = cardDef;
    NetCache.NetCacheCollection netObject = NetCache.Get().GetNetObject<NetCache.NetCacheCollection>();
    // ISSUE: reference to a compiler-generated method
    NetCache.CardStack cardStack = netObject.Stacks.Find(new Predicate<NetCache.CardStack>(cardsCAnonStorey38F.\u003C\u003Em__D9));
    if (cardStack == null)
    {
      Debug.LogError((object) "CollectionManager.NotifyNetCacheOfRemovedCards() - trying to remove a card from an empty stack!");
    }
    else
    {
      cardStack.Count -= count;
      if (cardStack.Count <= 0)
        netObject.Stacks.Remove(cardStack);
      // ISSUE: reference to a compiler-generated field
      this.UpdateCardCounts(netObject, cardsCAnonStorey38F.cardDef, -count, cardStack.Count);
    }
  }

  private void NotifyNetCacheOfNewCards(NetCache.CardDefinition cardDef, long insertDate, int count, bool seenBefore)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManager.\u003CNotifyNetCacheOfNewCards\u003Ec__AnonStorey390 cardsCAnonStorey390 = new CollectionManager.\u003CNotifyNetCacheOfNewCards\u003Ec__AnonStorey390();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey390.cardDef = cardDef;
    NetCache.NetCacheCollection netObject = NetCache.Get().GetNetObject<NetCache.NetCacheCollection>();
    // ISSUE: reference to a compiler-generated method
    NetCache.CardStack cardStack = netObject.Stacks.Find(new Predicate<NetCache.CardStack>(cardsCAnonStorey390.\u003C\u003Em__DA));
    if (cardStack == null)
    {
      // ISSUE: reference to a compiler-generated field
      cardStack = new NetCache.CardStack()
      {
        Def = cardsCAnonStorey390.cardDef,
        Date = insertDate,
        Count = count,
        NumSeen = !seenBefore ? 0 : count
      };
      netObject.Stacks.Add(cardStack);
    }
    else
    {
      if (insertDate > cardStack.Date)
        cardStack.Date = insertDate;
      cardStack.Count += count;
      if (seenBefore)
        cardStack.NumSeen += count;
    }
    // ISSUE: reference to a compiler-generated field
    this.UpdateCardCounts(netObject, cardsCAnonStorey390.cardDef, count, cardStack.Count);
  }

  private void LoadTemplateDecks()
  {
    float realtimeSinceStartup1 = Time.realtimeSinceStartup;
    using (List<DeckTemplateDbfRecord>.Enumerator enumerator = GameDbf.DeckTemplate.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckTemplateDbfRecord current = enumerator.Current;
        string eventName = current.Event;
        if (string.IsNullOrEmpty(eventName) || !SpecialEventManager.Get().IsEventActive(eventName, false))
        {
          int deckId = current.DeckId;
          if (!this.m_templateDeckMap.ContainsKey(deckId))
          {
            DeckDbfRecord record1 = GameDbf.Deck.GetRecord(deckId);
            if (record1 == null)
            {
              Debug.LogError((object) string.Format("Unable to find deck with ID {0}", (object) deckId));
            }
            else
            {
              Map<string, int> map1 = new Map<string, int>();
              int nextCard;
              for (DeckCardDbfRecord deckCardDbfRecord = GameDbf.DeckCard.GetRecord(record1.TopCardId); deckCardDbfRecord != null; deckCardDbfRecord = nextCard != 0 ? GameDbf.DeckCard.GetRecord(nextCard) : (DeckCardDbfRecord) null)
              {
                int cardId = deckCardDbfRecord.CardId;
                CardDbfRecord record2 = GameDbf.Card.GetRecord(cardId);
                if (record2 != null)
                {
                  string noteMiniGuid = record2.NoteMiniGuid;
                  if (map1.ContainsKey(noteMiniGuid))
                  {
                    Map<string, int> map2;
                    string index;
                    (map2 = map1)[index = noteMiniGuid] = map2[index] + 1;
                  }
                  else
                    map1[noteMiniGuid] = 1;
                }
                else
                  Debug.LogError((object) string.Format("Card ID in deck not found in CARD.XML: {0}", (object) cardId));
                nextCard = deckCardDbfRecord.NextCard;
              }
              TAG_CLASS classId = (TAG_CLASS) current.ClassId;
              List<CollectionManager.TemplateDeck> templateDeckList = (List<CollectionManager.TemplateDeck>) null;
              if (!this.m_templateDecks.TryGetValue(classId, out templateDeckList))
              {
                templateDeckList = new List<CollectionManager.TemplateDeck>();
                this.m_templateDecks.Add(classId, templateDeckList);
              }
              CollectionManager.TemplateDeck templateDeck = new CollectionManager.TemplateDeck()
              {
                m_id = deckId,
                m_class = classId,
                m_sortOrder = current.SortOrder,
                m_cardIds = map1,
                m_title = (string) record1.Name,
                m_description = (string) record1.Description,
                m_displayTexture = FileUtils.GameAssetPathToName(current.DisplayTexture)
              };
              templateDeckList.Add(templateDeck);
              this.m_templateDeckMap.Add(templateDeck.m_id, templateDeck);
            }
          }
        }
      }
    }
    using (Map<TAG_CLASS, List<CollectionManager.TemplateDeck>>.Enumerator enumerator = this.m_templateDecks.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.Sort((Comparison<CollectionManager.TemplateDeck>) ((lhs, rhs) =>
        {
          if (lhs.m_sortOrder != rhs.m_sortOrder)
            return Mathf.Clamp(lhs.m_sortOrder - rhs.m_sortOrder, -1, 1);
          return -Mathf.Clamp(lhs.m_id - rhs.m_id, -1, 1);
        }));
    }
    float realtimeSinceStartup2 = Time.realtimeSinceStartup;
    Log.Cameron.Print("_decktemplate: Time spent loading template decks: " + (object) (float) ((double) realtimeSinceStartup2 - (double) realtimeSinceStartup1));
  }

  public void AutoGenerateNewDeckForPlayer()
  {
    if (!this.IsFullyLoaded())
      Log.ReturningPlayer.PrintWarning("Attempting to auto-generate a new deck before Collection Manager is fully loaded!");
    else if (this.GetDecks(DeckType.NORMAL_DECK).Count >= 18)
    {
      Log.ReturningPlayer.PrintWarning("Cannot auto-generate a new deck for this player - all deck slots are filled!");
      BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_RETURNING_PLAYER_DECK_NOT_CREATED_TOO_MANY_DECKS, string.Format("{0}", (object) ReturningPlayerMgr.Get().AbTestGroup));
    }
    else
    {
      NetCache.NetCacheHeroLevels netObject = NetCache.Get().GetNetObject<NetCache.NetCacheHeroLevels>();
      if (netObject == null)
      {
        Log.ReturningPlayer.PrintWarning("CollectionManager.AutoGenerateNewDeckForPlayer(): NetCacheHeroLevels is null!");
      }
      else
      {
        List<NetCache.HeroLevel> levels = netObject.Levels;
        NetCache.HeroLevel heroLevel1 = (NetCache.HeroLevel) null;
        NetCache.HeroLevel heroLevel2 = (NetCache.HeroLevel) null;
        using (List<NetCache.HeroLevel>.Enumerator enumerator = levels.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            NetCache.HeroLevel current = enumerator.Current;
            if (current.Class == TAG_CLASS.MAGE)
              heroLevel2 = current;
            if (heroLevel1 == null || current.CurrentLevel.XP > heroLevel1.CurrentLevel.XP)
              heroLevel1 = current;
          }
        }
        if (heroLevel1 == null)
        {
          Debug.LogWarning((object) "CollectionManager.AutoGenerateNewDeckForPlayer(): could not find a hero with the highest level!");
        }
        else
        {
          if (heroLevel2 != null && heroLevel2.CurrentLevel.XP == heroLevel1.CurrentLevel.XP)
            heroLevel1 = heroLevel2;
          NetCache.CardDefinition favoriteHero = this.GetFavoriteHero(heroLevel1.Class);
          string str = (string) null;
          TAG_PREMIUM heroPremium = TAG_PREMIUM.NORMAL;
          if (favoriteHero != null)
          {
            str = favoriteHero.Name;
            heroPremium = favoriteHero.Premium;
          }
          if (str == null)
          {
            List<CollectibleCard> bestHeroesIown = this.GetBestHeroesIOwn(heroLevel1.Class);
            if (bestHeroesIown.Count > 0)
            {
              str = bestHeroesIown[0].CardId;
              heroPremium = bestHeroesIown[0].PremiumType;
            }
          }
          if (str == null)
          {
            str = this.GetVanillaHeroCardIDFromClass(heroLevel1.Class);
            heroPremium = this.GetBestCardPremium(str);
          }
          int dbId = GameUtils.TranslateCardIdToDbId(str);
          if (dbId == 0)
          {
            Debug.LogWarning((object) string.Format("CollectionManager.AutoGenerateNewDeckForPlayer(): Unknown hero cardID {0}", (object) str));
          }
          else
          {
            Log.ReturningPlayer.Print("Generating a deck for the player!");
            this.m_creatingInnkeeperDeck = true;
            Network.Get().CreateDeck(DeckType.NORMAL_DECK, GameStrings.Get("GLOBAL_RETURNING_PLAYER_GENERATED_DECK"), dbId, heroPremium, false, -500L, DeckSourceType.DECK_SOURCE_TYPE_INNKEEPER_DECK);
          }
        }
      }
    }
  }

  private void FillInnkeeperDeck(CollectionDeck deck)
  {
    DeckRuleset standardRuleset = DeckRuleset.GetStandardRuleset();
    if (standardRuleset == null)
    {
      Log.All.PrintError("OnInnkeeperDeckCreated: deckRuleset is null!");
    }
    else
    {
      int num = standardRuleset != null ? standardRuleset.GetDeckSize() : int.MaxValue;
      foreach (DeckMaker.DeckFill fillCard in DeckMaker.GetFillCards(deck, standardRuleset))
      {
        if (deck.GetTotalCardCount() < num)
        {
          EntityDef addCard = fillCard.m_addCard;
          string cardId = addCard.GetCardId();
          if (deck.CanAddOwnedCard(cardId, TAG_PREMIUM.GOLDEN))
            deck.AddCard(addCard, TAG_PREMIUM.GOLDEN, false);
          else if (deck.CanAddOwnedCard(cardId, TAG_PREMIUM.NORMAL))
            deck.AddCard(addCard, TAG_PREMIUM.NORMAL, false);
          else
            Log.All.PrintError("OnInnkeeperDeckCreated: Attempting to add FillCard {0} to Innkeeper Deck that is not valid!", (object) addCard);
        }
        else
          break;
      }
      deck.SendChanges();
    }
  }

  public class PreconDeck
  {
    private long m_id;

    public long ID
    {
      get
      {
        return this.m_id;
      }
    }

    public PreconDeck(long id)
    {
      this.m_id = id;
    }
  }

  public enum DeckTag
  {
    Editing,
    Arena,
  }

  public class TemplateDeck
  {
    public Map<string, int> m_cardIds = new Map<string, int>();
    public int m_id;
    public TAG_CLASS m_class;
    public int m_sortOrder;
    public string m_title;
    public string m_description;
    public string m_displayTexture;
  }

  private struct CollectibleCardIndex
  {
    public string CardId;
    public TAG_PREMIUM Premium;

    public CollectibleCardIndex(string cardId, TAG_PREMIUM premium)
    {
      this.CardId = cardId;
      this.Premium = premium;
    }
  }

  private class DefaultCardbackChangedListener : EventListener<CollectionManager.DefaultCardbackChangedCallback>
  {
    public void Fire(int newDefaultCardBackID)
    {
      this.m_callback(newDefaultCardBackID, this.m_userData);
    }
  }

  private class FavoriteHeroChangedListener : EventListener<CollectionManager.FavoriteHeroChangedCallback>
  {
    public void Fire(TAG_CLASS heroClass, NetCache.CardDefinition favoriteHero)
    {
      this.m_callback(heroClass, favoriteHero, this.m_userData);
    }
  }

  public class DeckSort : IComparer<CollectionDeck>
  {
    public const int INNKEEPER_DECK_SORT_ORDER = -500;

    public int Compare(CollectionDeck a, CollectionDeck b)
    {
      if (!a.IsWild && b.IsWild)
        return -1;
      if (a.IsWild && !b.IsWild)
        return 1;
      if (a.SortOrder == b.SortOrder)
        return a.CreateDate.CompareTo(b.CreateDate);
      return a.SortOrder.CompareTo(b.SortOrder);
    }
  }

  public delegate void DefaultCardbackChangedCallback(int newDefaultCardBackID, object userData);

  public delegate void FavoriteHeroChangedCallback(TAG_CLASS heroClass, NetCache.CardDefinition favoriteHero, object userData);

  public delegate bool CollectibleCardFilterFunc(CollectibleCard card);

  public delegate void DelOnCollectionLoaded();

  public delegate void DelOnCollectionChanged();

  public delegate void DelOnDeckCreated(long id);

  public delegate void DelOnDeckDeleted(long id);

  public delegate void DelOnDeckContents(long id);

  public delegate void DelOnAllDeckContents();

  public delegate void DelOnNewCardSeen(string cardID, TAG_PREMIUM premium);

  public delegate void DelOnCardRewardInserted(string cardID, TAG_PREMIUM premium);

  public delegate void DelOnAchievesCompleted(List<Achievement> achievements);

  public delegate void OnMassDisenchant(int amount);

  public delegate void OnTaggedDeckChanged(CollectionManager.DeckTag tag, CollectionDeck newDeck, CollectionDeck oldDeck, object callbackData);
}
