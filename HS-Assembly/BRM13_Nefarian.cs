﻿// Decompiled with JetBrains decompiler
// Type: BRM13_Nefarian
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BRM13_Nefarian : BRM_MissionEntity
{
  private Vector3 ragLinePosition = new Vector3(95f, NotificationManager.DEPTH, 36.8f);
  private bool m_heroPowerLinePlayed;
  private int m_ragLine;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BRMA13_1_RESPONSE_05");
    this.PreloadSound("VO_BRMA13_1_TURN1_PT1_02");
    this.PreloadSound("VO_BRMA13_1_TURN1_PT2_03");
    this.PreloadSound("VO_RAGNAROS_NEF1_71");
    this.PreloadSound("VO_BRMA13_1_HP_PALADIN_07");
    this.PreloadSound("VO_BRMA13_1_HP_PRIEST_08");
    this.PreloadSound("VO_BRMA13_1_HP_WARLOCK_10");
    this.PreloadSound("VO_BRMA13_1_HP_WARRIOR_09");
    this.PreloadSound("VO_BRMA13_1_HP_MAGE_11");
    this.PreloadSound("VO_BRMA13_1_HP_DRUID_14");
    this.PreloadSound("VO_BRMA13_1_HP_SHAMAN_13");
    this.PreloadSound("VO_BRMA13_1_HP_HUNTER_12");
    this.PreloadSound("VO_BRMA13_1_HP_ROGUE_15");
    this.PreloadSound("VO_BRMA13_1_HP_GENERIC_18");
    this.PreloadSound("VO_BRMA13_1_DEATHWING_19");
    this.PreloadSound("VO_NEFARIAN_NEF2_65");
    this.PreloadSound("VO_NEFARIAN_NEF_MISSION_66");
    this.PreloadSound("VO_RAGNAROS_NEF3_72");
    this.PreloadSound("VO_NEFARIAN_HEROIC_BLOCK_77");
    this.PreloadSound("VO_RAGNAROS_NEF4_73");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA13_1_RESPONSE_05",
            m_stringTag = "VO_BRMA13_1_RESPONSE_05"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM13_Nefarian.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator14C() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM13_Nefarian.\u003CHandleMissionEventWithTiming\u003Ec__Iterator14D() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator UnBusyInSeconds(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM13_Nefarian.\u003CUnBusyInSeconds\u003Ec__Iterator14E() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds };
  }
}
