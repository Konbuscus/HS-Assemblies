﻿// Decompiled with JetBrains decompiler
// Type: CardInfoPane
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardInfoPane : MonoBehaviour
{
  public UberText m_artistName;
  public UberText m_rarityLabel;
  public UberText m_flavorText;
  public UberText m_setName;
  public GameObject m_standardTheming;
  public RarityGem m_rarityGem;
  public GameObject m_wildTheming;
  public RarityGem m_wildRarityGem;

  public void UpdateContent()
  {
    EntityDef entityDef;
    TAG_PREMIUM premium;
    if (!CraftingManager.Get().GetShownCardInfo(out entityDef, out premium))
      return;
    TAG_RARITY rarity = entityDef.GetRarity();
    TAG_CARD_SET cardSet = entityDef.GetCardSet();
    this.m_rarityLabel.Text = cardSet != TAG_CARD_SET.CORE ? GameStrings.GetRarityText(rarity) : string.Empty;
    this.AssignRarityColors(rarity, cardSet);
    if (GameUtils.IsCardRotated(entityDef))
    {
      this.m_wildTheming.SetActive(true);
      this.m_standardTheming.SetActive(false);
      this.m_wildRarityGem.SetRarityGem(rarity, cardSet);
    }
    else
    {
      this.m_standardTheming.SetActive(true);
      this.m_wildTheming.SetActive(false);
      this.m_rarityGem.SetRarityGem(rarity, cardSet);
    }
    this.m_setName.Text = GameStrings.GetCardSetName(cardSet);
    this.m_artistName.Text = GameStrings.Format("GLUE_COLLECTION_ARTIST", (object) entityDef.GetArtistName());
    this.m_wildTheming.SetActive(GameUtils.IsCardRotated(entityDef));
    string str = "<color=#000000ff>" + entityDef.GetFlavorText() + "</color>";
    NetCache.CardValue cardValue = CraftingManager.Get().GetCardValue(entityDef.GetCardId(), premium);
    if (cardValue != null && cardValue.Nerfed)
    {
      if (!string.IsNullOrEmpty(str))
        str += "\n\n";
      str += GameStrings.Get("GLUE_COLLECTION_RECENTLY_NERFED");
    }
    this.m_flavorText.Text = str;
  }

  private void AssignRarityColors(TAG_RARITY rarity, TAG_CARD_SET cardSet)
  {
    if (cardSet == TAG_CARD_SET.CORE)
    {
      this.m_rarityLabel.TextColor = new Color(0.53f, 0.52f, 0.51f, 1f);
    }
    else
    {
      switch (rarity)
      {
        case TAG_RARITY.RARE:
          this.m_rarityLabel.TextColor = new Color(0.11f, 0.33f, 0.8f, 1f);
          break;
        case TAG_RARITY.EPIC:
          this.m_rarityLabel.TextColor = new Color(0.77f, 0.03f, 1f, 1f);
          break;
        case TAG_RARITY.LEGENDARY:
          this.m_rarityLabel.TextColor = new Color(1f, 0.56f, 0.0f, 1f);
          break;
        default:
          this.m_rarityLabel.TextColor = Color.white;
          break;
      }
    }
  }
}
