﻿// Decompiled with JetBrains decompiler
// Type: MobileDeviceLocale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MobileDeviceLocale
{
  private static Map<string, Locale> s_languageCodeToLocale = new Map<string, Locale>() { { "en", Locale.enUS }, { "fr", Locale.frFR }, { "de", Locale.deDE }, { "ko", Locale.koKR }, { "ru", Locale.ruRU }, { "it", Locale.itIT }, { "pt", Locale.ptBR }, { "pl", Locale.plPL }, { "ja", Locale.jaJP }, { "th", Locale.thTH }, { "en-AU", Locale.enUS }, { "en-GB", Locale.enUS }, { "fr-CA", Locale.frFR }, { "es-MX", Locale.esMX }, { "zh-Hans", Locale.zhCN }, { "zh-Hant", Locale.zhTW }, { "pt-PT", Locale.ptBR } };
  private static Map<string, int> s_countryCodeToRegionId = new Map<string, int>() { { "AD", 2 }, { "AE", 2 }, { "AG", 1 }, { "AL", 2 }, { "AM", 2 }, { "AO", 2 }, { "AR", 1 }, { "AT", 2 }, { "AU", 1 }, { "AZ", 2 }, { "BA", 2 }, { "BB", 1 }, { "BD", 1 }, { "BE", 2 }, { "BF", 2 }, { "BG", 2 }, { "BH", 2 }, { "BI", 2 }, { "BJ", 2 }, { "BM", 2 }, { "BN", 1 }, { "BO", 1 }, { "BR", 1 }, { "BS", 1 }, { "BT", 1 }, { "BW", 2 }, { "BY", 2 }, { "BZ", 1 }, { "CA", 1 }, { "CD", 2 }, { "CF", 2 }, { "CG", 2 }, { "CH", 2 }, { "CI", 2 }, { "CL", 1 }, { "CM", 2 }, { "CN", 3 }, { "CO", 1 }, { "CR", 1 }, { "CU", 1 }, { "CV", 2 }, { "CY", 2 }, { "CZ", 2 }, { "DE", 2 }, { "DJ", 2 }, { "DK", 2 }, { "DM", 1 }, { "DO", 1 }, { "DZ", 2 }, { "EC", 1 }, { "EE", 2 }, { "EG", 2 }, { "ER", 2 }, { "ES", 2 }, { "ET", 2 }, { "FI", 2 }, { "FJ", 1 }, { "FK", 2 }, { "FO", 2 }, { "FR", 2 }, { "GA", 2 }, { "GB", 2 }, { "GD", 1 }, { "GE", 2 }, { "GL", 2 }, { "GM", 2 }, { "GN", 2 }, { "GQ", 2 }, { "GR", 2 }, { "GS", 2 }, { "GT", 1 }, { "GW", 2 }, { "GY", 1 }, { "HK", 3 }, { "HN", 1 }, { "HR", 2 }, { "HT", 1 }, { "HU", 2 }, { "ID", 1 }, { "IE", 2 }, { "IL", 2 }, { "IM", 2 }, { "IN", 1 }, { "IQ", 2 }, { "IR", 2 }, { "IS", 2 }, { "IT", 2 }, { "JM", 1 }, { "JO", 2 }, { "JP", 3 }, { "KE", 2 }, { "KG", 2 }, { "KH", 2 }, { "KI", 1 }, { "KM", 2 }, { "KP", 1 }, { "KR", 3 }, { "KW", 2 }, { "KY", 2 }, { "KZ", 2 }, { "LA", 1 }, { "LB", 2 }, { "LC", 1 }, { "LI", 2 }, { "LK", 1 }, { "LR", 2 }, { "LS", 2 }, { "LT", 2 }, { "LU", 2 }, { "LV", 2 }, { "LY", 2 }, { "MA", 2 }, { "MC", 2 }, { "MD", 2 }, { "ME", 2 }, { "MG", 2 }, { "MK", 2 }, { "ML", 2 }, { "MM", 1 }, { "MN", 2 }, { "MO", 3 }, { "MR", 2 }, { "MT", 2 }, { "MU", 2 }, { "MV", 2 }, { "MW", 2 }, { "MX", 1 }, { "MY", 1 }, { "MZ", 2 }, { "NA", 2 }, { "NC", 2 }, { "NE", 2 }, { "NG", 2 }, { "NI", 1 }, { "NL", 2 }, { "NO", 2 }, { "NP", 1 }, { "NR", 1 }, { "NZ", 1 }, { "OM", 2 }, { "PA", 1 }, { "PE", 1 }, { "PF", 1 }, { "PG", 1 }, { "PH", 1 }, { "PK", 2 }, { "PL", 2 }, { "PT", 2 }, { "PY", 1 }, { "QA", 2 }, { "RO", 2 }, { "RS", 2 }, { "RU", 2 }, { "RW", 2 }, { "SA", 2 }, { "SB", 1 }, { "SC", 2 }, { "SD", 2 }, { "SE", 2 }, { "SG", 1 }, { "SH", 2 }, { "SI", 2 }, { "SK", 2 }, { "SL", 2 }, { "SN", 2 }, { "SO", 2 }, { "SR", 2 }, { "ST", 2 }, { "SV", 1 }, { "SY", 2 }, { "SZ", 2 }, { "TD", 2 }, { "TG", 2 }, { "TH", 1 }, { "TJ", 2 }, { "TL", 1 }, { "TM", 2 }, { "TN", 2 }, { "TO", 1 }, { "TR", 2 }, { "TT", 1 }, { "TV", 1 }, { "TW", 3 }, { "TZ", 2 }, { "UA", 2 }, { "UG", 2 }, { "US", 1 }, { "UY", 1 }, { "UZ", 2 }, { "VA", 2 }, { "VC", 1 }, { "VE", 1 }, { "VN", 1 }, { "VU", 1 }, { "WS", 1 }, { "YE", 2 }, { "YU", 2 }, { "ZA", 2 }, { "ZM", 2 }, { "ZW", 2 } };
  private static int s_defaultRegionId = 1;
  private static Map<constants.BnetRegion, MobileDeviceLocale.ConnectionData> s_regionIdToProdIP = new Map<constants.BnetRegion, MobileDeviceLocale.ConnectionData>() { { constants.BnetRegion.REGION_UNKNOWN, new MobileDeviceLocale.ConnectionData() { address = "us.actual.battle.net", port = 1119, version = "product" } }, { constants.BnetRegion.REGION_US, new MobileDeviceLocale.ConnectionData() { address = "us.actual.battle.net", port = 1119, version = "product" } }, { constants.BnetRegion.REGION_EU, new MobileDeviceLocale.ConnectionData() { address = "eu.actual.battle.net", port = 1119, version = "product" } }, { constants.BnetRegion.REGION_KR, new MobileDeviceLocale.ConnectionData() { address = "kr.actual.battle.net", port = 1119, version = "product" } }, { constants.BnetRegion.REGION_TW, new MobileDeviceLocale.ConnectionData() { address = "kr.actual.battle.net", port = 1119, version = "product" } }, { constants.BnetRegion.REGION_CN, new MobileDeviceLocale.ConnectionData() { address = "cn.actual.battle.net", port = 1119, version = "product" } }, { constants.BnetRegion.REGION_PTR_LOC, new MobileDeviceLocale.ConnectionData() { address = "beta.actual.battle.net", port = 1119, version = "LOC" } } };
  private static MobileDeviceLocale.ConnectionData s_defaultProdIP = new MobileDeviceLocale.ConnectionData() { address = "us.actual.battle.net", port = 1119, version = "product" };
  public static Map<constants.BnetRegion, MobileDeviceLocale.ConnectionData> s_regionIdToDevIP = new Map<constants.BnetRegion, MobileDeviceLocale.ConnectionData>() { { constants.BnetRegion.REGION_US, new MobileDeviceLocale.ConnectionData() { name = "dev25-qaus", address = "st1-bn-front01.battle.net", port = 1119, version = "dev25-qaus", tutorialPort = 3725, gameServerAddress = "10.63.160.51" } }, { constants.BnetRegion.REGION_CN, new MobileDeviceLocale.ConnectionData() { name = "dev25-qacn", address = "st5-bn-front01.battle.net", port = 1119, version = "dev25-qacn", tutorialPort = 3725, gameServerAddress = "10.63.160.65" } }, { constants.BnetRegion.REGION_EU, new MobileDeviceLocale.ConnectionData() { name = "dev25-qaeu", address = "st2-bn-front01.battle.net", port = 1119, version = "dev25-qaeu", tutorialPort = 3725, gameServerAddress = "10.63.160.59" } }, { constants.BnetRegion.REGION_KR, new MobileDeviceLocale.ConnectionData() { name = "dev25-qakr", address = "st3-bn-front01.battle.net", port = 1119, version = "dev25-qakr", tutorialPort = 3725, gameServerAddress = "10.63.160.60" } }, { (constants.BnetRegion) 75, new MobileDeviceLocale.ConnectionData() { name = "dev25-dev1", address = "dev25.bgs.battle.net", port = 1119, version = "dev25-dev1", tutorialPort = 3725, gameServerAddress = "10.63.160.71" } }, { (constants.BnetRegion) 76, new MobileDeviceLocale.ConnectionData() { name = "dev25-dev2", address = "dev25.bgs.battle.net", port = 1119, version = "dev25-dev2", tutorialPort = 3725, gameServerAddress = "10.63.160.72" } }, { (constants.BnetRegion) 77, new MobileDeviceLocale.ConnectionData() { name = "dev25-dev3", address = "dev25.bgs.battle.net", port = 1119, version = "dev25-dev3", tutorialPort = 3725, gameServerAddress = "10.63.160.73" } }, { (constants.BnetRegion) 78, new MobileDeviceLocale.ConnectionData() { name = "dev25-dev4", address = "dev25.bgs.battle.net", port = 1119, version = "dev25-dev4", tutorialPort = 3725, gameServerAddress = "10.63.160.52" } }, { (constants.BnetRegion) 79, new MobileDeviceLocale.ConnectionData() { name = "dev25-dev5", address = "dev25.bgs.battle.net", port = 1119, version = "dev25-dev5", tutorialPort = 3725, gameServerAddress = "10.63.160.53" } }, { (constants.BnetRegion) 80, new MobileDeviceLocale.ConnectionData() { name = "dev25-dev6", address = "dev25.bgs.battle.net", port = 1119, version = "dev25-dev6", tutorialPort = 3725, gameServerAddress = "10.63.160.54" } }, { (constants.BnetRegion) 81, new MobileDeviceLocale.ConnectionData() { name = "dev25-qa1", address = "st1-bn-front01.battle.net", port = 1119, version = "dev25-qa1", tutorialPort = 3725, gameServerAddress = "10.63.160.62" } }, { (constants.BnetRegion) 82, new MobileDeviceLocale.ConnectionData() { name = "dev25-qa2", address = "st1-bn-front01.battle.net", port = 1119, version = "dev25-qa2", tutorialPort = 3725, gameServerAddress = "10.63.160.66" } }, { (constants.BnetRegion) 83, new MobileDeviceLocale.ConnectionData() { name = "dev25-qa3", address = "st1-bn-front01.battle.net", port = 1119, version = "dev25-qa3", tutorialPort = 3725, gameServerAddress = "10.63.160.67" } }, { (constants.BnetRegion) 84, new MobileDeviceLocale.ConnectionData() { name = "dev25-qa4", address = "st1-bn-front01.battle.net", port = 1119, version = "dev25-qa4", tutorialPort = 3725, gameServerAddress = "10.63.160.68" } }, { (constants.BnetRegion) 85, new MobileDeviceLocale.ConnectionData() { name = "dev25-qa5", address = "st1-bn-front01.battle.net", port = 1119, version = "dev25-qa5", tutorialPort = 3725, gameServerAddress = "10.63.160.69" } }, { (constants.BnetRegion) 86, new MobileDeviceLocale.ConnectionData() { name = "dev25-qa6", address = "st1-bn-front01.battle.net", port = 1119, version = "dev25-qa6", tutorialPort = 3725, gameServerAddress = "10.63.160.70" } } };
  private static constants.BnetRegion s_defaultDevRegion = constants.BnetRegion.REGION_US;
  public const string ptrServerVersion = "BETA";

  public static constants.BnetRegion FindDevRegionByServerVersion(string version)
  {
    using (Map<constants.BnetRegion, MobileDeviceLocale.ConnectionData>.KeyCollection.Enumerator enumerator = MobileDeviceLocale.s_regionIdToDevIP.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        constants.BnetRegion current = enumerator.Current;
        if (version == MobileDeviceLocale.s_regionIdToDevIP[current].version)
          return current;
      }
    }
    return constants.BnetRegion.REGION_UNINITIALIZED;
  }

  public static constants.BnetRegion GetCurrentRegionId()
  {
    int defaultRegionId = Options.Get().GetInt(Option.PREFERRED_REGION);
    if (defaultRegionId < 0)
    {
      if (MobileDeviceLocale.UseClientConfigForEnv())
      {
        constants.BnetRegion regionByServerVersion = MobileDeviceLocale.FindDevRegionByServerVersion(Vars.Key("Aurora.Version.String").GetStr(string.Empty));
        Log.JMac.Print("Battle.net region from client.config version: " + (object) regionByServerVersion);
        if (regionByServerVersion != constants.BnetRegion.REGION_UNINITIALIZED)
          return regionByServerVersion;
      }
      try
      {
        if (!MobileDeviceLocale.s_countryCodeToRegionId.TryGetValue(MobileDeviceLocale.GetCountryCode(), out defaultRegionId))
          defaultRegionId = MobileDeviceLocale.s_defaultRegionId;
      }
      catch (Exception ex)
      {
      }
    }
    return (constants.BnetRegion) defaultRegionId;
  }

  public static List<string> GetRegionCodesForCurrentRegionId()
  {
    List<string> stringList = new List<string>();
    int currentRegionId = (int) MobileDeviceLocale.GetCurrentRegionId();
    using (Map<string, int>.Enumerator enumerator = MobileDeviceLocale.s_countryCodeToRegionId.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        if (current.Value == currentRegionId)
          stringList.Add(current.Key);
      }
    }
    return stringList;
  }

  public static MobileDeviceLocale.ConnectionData GetConnectionDataFromRegionId(constants.BnetRegion region, bool isDev)
  {
    MobileDeviceLocale.ConnectionData defaultProdIp;
    if (isDev)
    {
      if (!MobileDeviceLocale.s_regionIdToDevIP.TryGetValue(region, out defaultProdIp) && !MobileDeviceLocale.s_regionIdToDevIP.TryGetValue(MobileDeviceLocale.s_defaultDevRegion, out defaultProdIp))
        Debug.LogError((object) "Invalid region set for s_defaultDevRegion!  This should never happen!!!");
    }
    else if (!MobileDeviceLocale.s_regionIdToProdIP.TryGetValue(region, out defaultProdIp))
      defaultProdIp = MobileDeviceLocale.s_defaultProdIP;
    return defaultProdIp;
  }

  public static Locale GetBestGuessForLocale()
  {
    Locale locale = Locale.enUS;
    bool flag = false;
    string key1 = MobileDeviceLocale.GetLanguageCode();
    try
    {
      flag = MobileDeviceLocale.s_languageCodeToLocale.TryGetValue(key1, out locale);
    }
    catch (Exception ex)
    {
    }
    if (!flag)
    {
      key1 = key1.Substring(0, 2);
      try
      {
        flag = MobileDeviceLocale.s_languageCodeToLocale.TryGetValue(key1, out locale);
      }
      catch (Exception ex)
      {
      }
    }
    if (!flag)
    {
      int num1 = 1;
      string countryCode = MobileDeviceLocale.GetCountryCode();
      try
      {
        MobileDeviceLocale.s_countryCodeToRegionId.TryGetValue(countryCode, out num1);
      }
      catch (Exception ex)
      {
      }
      string key2 = key1;
      if (key2 != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (MobileDeviceLocale.\u003C\u003Ef__switch\u0024mapC9 == null)
        {
          // ISSUE: reference to a compiler-generated field
          MobileDeviceLocale.\u003C\u003Ef__switch\u0024mapC9 = new Dictionary<string, int>(2)
          {
            {
              "es",
              0
            },
            {
              "zh",
              1
            }
          };
        }
        int num2;
        // ISSUE: reference to a compiler-generated field
        if (MobileDeviceLocale.\u003C\u003Ef__switch\u0024mapC9.TryGetValue(key2, out num2))
        {
          if (num2 != 0)
          {
            if (num2 == 1)
            {
              locale = !(countryCode == "CN") ? Locale.zhTW : Locale.zhCN;
              goto label_20;
            }
          }
          else
          {
            locale = num1 != 1 ? Locale.esES : Locale.esMX;
            goto label_20;
          }
        }
      }
      locale = Locale.enUS;
    }
label_20:
    return locale;
  }

  public static bool UseClientConfigForEnv()
  {
    bool flag1 = Vars.Key("Aurora.Env.Override").GetInt(0) != 0;
    if (Vars.Key("Aurora.Env.DisableOverrideOnDevices").GetInt(0) != 0)
      flag1 = false;
    string str = Vars.Key("Aurora.Env").GetStr(string.Empty);
    bool flag2 = (str == null ? 1 : (str == string.Empty ? 1 : 0)) == 0;
    if (flag1)
      return flag2;
    return false;
  }

  public static string GetCountryCode()
  {
    return MobileDeviceLocale.GetLocaleCountryCode();
  }

  public static string GetLanguageCode()
  {
    return MobileDeviceLocale.GetLocaleLanguageCode();
  }

  private static string GetLocaleCountryCode()
  {
    return string.Empty;
  }

  private static string GetLocaleLanguageCode()
  {
    return string.Empty;
  }

  public struct ConnectionData
  {
    public string address;
    public int port;
    public string version;
    public string name;
    public int tutorialPort;
    public string gameServerAddress;
  }
}
