﻿// Decompiled with JetBrains decompiler
// Type: Locale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum Locale
{
  UNKNOWN = -1,
  enUS = 0,
  enGB = 1,
  frFR = 2,
  deDE = 3,
  koKR = 4,
  esES = 5,
  esMX = 6,
  ruRU = 7,
  zhTW = 8,
  zhCN = 9,
  itIT = 10,
  ptBR = 11,
  plPL = 12,
  jaJP = 14,
  thTH = 15,
}
