﻿// Decompiled with JetBrains decompiler
// Type: CardTypeBanner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardTypeBanner : MonoBehaviour
{
  private readonly Color MINION_COLOR = new Color(0.1529412f, 0.1254902f, 0.03529412f);
  private readonly Color SPELL_COLOR = new Color(0.8745098f, 0.7882353f, 0.5254902f);
  private readonly Color WEAPON_COLOR = new Color(0.8745098f, 0.7882353f, 0.5254902f);
  public GameObject m_root;
  public UberText m_text;
  public GameObject m_spellBanner;
  public GameObject m_minionBanner;
  public GameObject m_weaponBanner;
  private static CardTypeBanner s_instance;
  private Actor m_actor;

  private void Awake()
  {
    CardTypeBanner.s_instance = this;
  }

  private void OnDestroy()
  {
    CardTypeBanner.s_instance = (CardTypeBanner) null;
  }

  private void Update()
  {
    if (!((Object) this.m_actor != (Object) null))
      return;
    this.UpdatePosition();
  }

  public static CardTypeBanner Get()
  {
    return CardTypeBanner.s_instance;
  }

  public bool IsShown()
  {
    return (bool) ((Object) this.m_actor);
  }

  public void Show(Actor a)
  {
    this.m_actor = a;
    this.ShowImpl();
  }

  public void Hide()
  {
    this.m_actor = (Actor) null;
    this.HideImpl();
  }

  public void Hide(Actor actor)
  {
    if (!((Object) this.m_actor == (Object) actor))
      return;
    this.Hide();
  }

  public CardDef GetCardDef()
  {
    if ((Object) this.m_actor != (Object) null)
      return this.m_actor.GetCardDef();
    return (CardDef) null;
  }

  private void ShowImpl()
  {
    this.m_root.gameObject.SetActive(true);
    TAG_CARDTYPE cardType = this.m_actor.GetEntity().GetCardType();
    this.m_text.gameObject.SetActive(true);
    this.m_text.Text = GameStrings.GetCardTypeName(cardType);
    switch (cardType)
    {
      case TAG_CARDTYPE.MINION:
        this.m_text.TextColor = this.MINION_COLOR;
        this.m_minionBanner.SetActive(true);
        break;
      case TAG_CARDTYPE.SPELL:
        this.m_text.TextColor = this.SPELL_COLOR;
        this.m_spellBanner.SetActive(true);
        break;
      case TAG_CARDTYPE.WEAPON:
        this.m_text.TextColor = this.WEAPON_COLOR;
        this.m_weaponBanner.SetActive(true);
        break;
    }
    this.UpdatePosition();
  }

  private void HideImpl()
  {
    this.m_root.gameObject.SetActive(false);
  }

  private void UpdatePosition()
  {
    this.m_root.transform.position = this.m_actor.GetCardTypeBannerAnchor().transform.position;
  }
}
