﻿// Decompiled with JetBrains decompiler
// Type: DbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public abstract class DbfRecord
{
  [SerializeField]
  private int m_ID;

  [DbfField("ID", "")]
  public int ID
  {
    get
    {
      return this.m_ID;
    }
  }

  public void SetID(int id)
  {
    this.m_ID = id;
  }

  public DbfFieldAttribute GetDbfFieldAttribute(string propertyName)
  {
    PropertyInfo property = this.GetType().GetProperty(propertyName);
    if (property != null)
    {
      object[] customAttributes = property.GetCustomAttributes(typeof (DbfFieldAttribute), true);
      if (customAttributes.Length > 0)
        return (DbfFieldAttribute) customAttributes[0];
    }
    return (DbfFieldAttribute) null;
  }

  public abstract object GetVar(string varName);

  public abstract void SetVar(string varName, object value);

  public abstract System.Type GetVarType(string varName);

  public abstract bool LoadRecordsFromAsset<T>(string assetPath, out List<T> records);

  public abstract bool SaveRecordsToAsset<T>(string assetPath, List<T> records) where T : DbfRecord, new();

  public abstract void StripUnusedLocales();
}
