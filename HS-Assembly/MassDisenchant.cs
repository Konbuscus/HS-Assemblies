﻿// Decompiled with JetBrains decompiler
// Type: MassDisenchant
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class MassDisenchant : MonoBehaviour
{
  private bool m_useSingle = true;
  private List<GameObject> m_cleanupObjects = new List<GameObject>();
  public GameObject m_root;
  public GameObject m_disenchantContainer;
  public MassDisenchantFX m_FX;
  public MassDisenchantSound m_sound;
  public UberText m_headlineText;
  public UberText m_detailsHeadlineText;
  public UberText m_detailsText;
  public UberText m_totalAmountText;
  public NormalButton m_disenchantButton;
  public UberText m_singleSubHeadlineText;
  public UberText m_doubleSubHeadlineText;
  public GameObject m_singleRoot;
  public GameObject m_doubleRoot;
  public List<DisenchantBar> m_singleDisenchantBars;
  public List<DisenchantBar> m_doubleDisenchantBars;
  public UIBButton m_infoButton;
  public Material m_rarityBarNormalMaterial;
  public Material m_rarityBarGoldMaterial;
  public Mesh m_rarityBarNormalMesh;
  public Mesh m_rarityBarGoldMesh;
  private int m_totalAmount;
  private int m_totalCardsToDisenchant;
  private Vector3 m_origTotalScale;
  private Vector3 m_origDustScale;
  private int m_highestGlowBalls;
  private static MassDisenchant s_Instance;

  private void Awake()
  {
    MassDisenchant.s_Instance = this;
    this.m_headlineText.Text = GameStrings.Get("GLUE_MASS_DISENCHANT_HEADLINE");
    this.m_detailsHeadlineText.Text = GameStrings.Get("GLUE_MASS_DISENCHANT_DETAILS_HEADLINE");
    this.m_disenchantButton.SetText(GameStrings.Get("GLUE_MASS_DISENCHANT_BUTTON_TEXT"));
    if ((UnityEngine.Object) this.m_detailsText != (UnityEngine.Object) null)
      this.m_detailsText.Text = GameStrings.Get("GLUE_MASS_DISENCHANT_DETAILS");
    if ((UnityEngine.Object) this.m_singleSubHeadlineText != (UnityEngine.Object) null)
      this.m_singleSubHeadlineText.Text = GameStrings.Get("GLUE_MASS_DISENCHANT_SUB_HEADLINE_TEXT");
    if ((UnityEngine.Object) this.m_doubleSubHeadlineText != (UnityEngine.Object) null)
      this.m_doubleSubHeadlineText.Text = GameStrings.Get("GLUE_MASS_DISENCHANT_SUB_HEADLINE_TEXT");
    this.m_disenchantButton.SetUserOverYOffset(-0.04409015f);
    using (List<DisenchantBar>.Enumerator enumerator = this.m_singleDisenchantBars.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Init();
    }
    using (List<DisenchantBar>.Enumerator enumerator = this.m_doubleDisenchantBars.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Init();
    }
    CollectionManager.Get().RegisterMassDisenchantListener(new CollectionManager.OnMassDisenchant(this.OnMassDisenchant));
  }

  private void Start()
  {
    this.m_disenchantButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDisenchantButtonPressed));
    this.m_disenchantButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnDisenchantButtonOver));
    this.m_disenchantButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnDisenchantButtonOut));
    if (!((UnityEngine.Object) this.m_infoButton != (UnityEngine.Object) null))
      return;
    this.m_infoButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnInfoButtonPressed));
  }

  public static MassDisenchant Get()
  {
    return MassDisenchant.s_Instance;
  }

  public void Show()
  {
    this.m_root.SetActive(true);
  }

  public void Hide()
  {
    this.m_root.SetActive(false);
  }

  private void OnDestroy()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_cleanupObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    CollectionManager.Get().RemoveMassDisenchantListener(new CollectionManager.OnMassDisenchant(this.OnMassDisenchant));
  }

  public int GetTotalAmount()
  {
    return this.m_totalAmount;
  }

  public void UpdateContents(List<CollectibleCard> disenchantCards)
  {
    this.m_useSingle = disenchantCards.Where<CollectibleCard>((Func<CollectibleCard, bool>) (c => c.PremiumType == TAG_PREMIUM.GOLDEN)).ToList<CollectibleCard>().Count == 0;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_useSingle = true;
    List<DisenchantBar> disenchantBarList = !this.m_useSingle ? this.m_doubleDisenchantBars : this.m_singleDisenchantBars;
    using (List<DisenchantBar>.Enumerator enumerator = disenchantBarList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Reset();
    }
    this.m_totalAmount = 0;
    this.m_totalCardsToDisenchant = 0;
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MassDisenchant.\u003CUpdateContents\u003Ec__AnonStorey3A7 contentsCAnonStorey3A7 = new MassDisenchant.\u003CUpdateContents\u003Ec__AnonStorey3A7();
    using (List<CollectibleCard>.Enumerator enumerator = disenchantCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        contentsCAnonStorey3A7.card = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        MassDisenchant.\u003CUpdateContents\u003Ec__AnonStorey3A8 contentsCAnonStorey3A8 = new MassDisenchant.\u003CUpdateContents\u003Ec__AnonStorey3A8();
        // ISSUE: reference to a compiler-generated field
        contentsCAnonStorey3A8.\u003C\u003Ef__ref\u0024935 = contentsCAnonStorey3A7;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        NetCache.CardValue cardValue = CraftingManager.Get().GetCardValue(contentsCAnonStorey3A7.card.CardId, contentsCAnonStorey3A7.card.PremiumType);
        if (cardValue != null)
        {
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          contentsCAnonStorey3A8.entityDef = DefLoader.Get().GetEntityDef(contentsCAnonStorey3A7.card.CardId);
          // ISSUE: reference to a compiler-generated field
          int sellAmount = cardValue.Sell * contentsCAnonStorey3A7.card.DisenchantCount;
          // ISSUE: reference to a compiler-generated method
          DisenchantBar disenchantBar = disenchantBarList.Find(new Predicate<DisenchantBar>(contentsCAnonStorey3A8.\u003C\u003Em__103));
          if (disenchantBar == null)
          {
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            UnityEngine.Debug.LogWarning((object) string.Format("MassDisenchant.UpdateContents(): Could not find {0} bar to modify for card {1} (premium {2}, disenchant count {3})", (object) (!this.m_useSingle ? "double" : "single"), (object) contentsCAnonStorey3A8.entityDef, (object) contentsCAnonStorey3A7.card.PremiumType, (object) contentsCAnonStorey3A7.card.DisenchantCount));
          }
          else
          {
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            disenchantBar.AddCards(contentsCAnonStorey3A7.card.DisenchantCount, sellAmount, contentsCAnonStorey3A7.card.PremiumType);
            // ISSUE: reference to a compiler-generated field
            this.m_totalCardsToDisenchant += contentsCAnonStorey3A7.card.DisenchantCount;
            this.m_totalAmount += sellAmount;
          }
        }
      }
    }
    if (this.m_totalAmount > 0)
    {
      this.m_singleRoot.SetActive(this.m_useSingle);
      if ((UnityEngine.Object) this.m_doubleRoot != (UnityEngine.Object) null)
        this.m_doubleRoot.SetActive(!this.m_useSingle);
      this.m_disenchantButton.SetEnabled(true);
    }
    using (List<DisenchantBar>.Enumerator enumerator = disenchantBarList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateVisuals(this.m_totalCardsToDisenchant);
    }
    this.m_totalAmountText.Text = GameStrings.Format("GLUE_MASS_DISENCHANT_TOTAL_AMOUNT", (object) this.m_totalAmount);
  }

  [DebuggerHidden]
  public IEnumerator StartHighlight()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MassDisenchant.\u003CStartHighlight\u003Ec__Iterator5F()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void OnMassDisenchant(int amount)
  {
    int maxGlowBalls;
    switch (GraphicsManager.Get().RenderQualityLevel)
    {
      case GraphicsQuality.Low:
        maxGlowBalls = 3;
        break;
      case GraphicsQuality.Medium:
        maxGlowBalls = 6;
        break;
      default:
        maxGlowBalls = 10;
        break;
    }
    this.BlockUI(true);
    this.StartCoroutine(this.DoDisenchantAnims(maxGlowBalls, amount));
  }

  private void BlockUI(bool block = true)
  {
    this.m_FX.m_blockInteraction.SetActive(block);
  }

  private void OnDisenchantButtonOver(UIEvent e)
  {
    if (CollectionManagerDisplay.Get().m_pageManager.IsShowingMassDisenchant())
    {
      this.m_FX.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
      SoundManager.Get().LoadAndPlay("Hub_Mouseover");
    }
    else
      this.m_FX.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }

  private void OnDisenchantButtonOut(UIEvent e)
  {
    if (CollectionManagerDisplay.Get().m_pageManager.IsShowingMassDisenchant())
      this.m_FX.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    else
      this.m_FX.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }

  private void OnDisenchantButtonPressed(UIEvent e)
  {
    Options.Get().SetBool(Option.HAS_DISENCHANTED, true);
    this.m_disenchantButton.SetEnabled(false);
    this.m_FX.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
    Network.MassDisenchant();
  }

  private void OnInfoButtonPressed(UIEvent e)
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_MASS_DISENCHANT_BUTTON_TEXT"),
      m_text = string.Format("{0}\n\n{1}", (object) GameStrings.Get("GLUE_MASS_DISENCHANT_DETAILS_HEADLINE"), (object) GameStrings.Get("GLUE_MASS_DISENCHANT_DETAILS")),
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  private void Unbloomify(List<GameObject> glows, float newVal)
  {
    using (List<GameObject>.Enumerator enumerator = glows.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.GetComponent<RenderToTexture>().m_BloomIntensity = newVal;
    }
  }

  private void UncolorTotal(float newVal)
  {
    this.m_totalAmountText.TextColor = Color.Lerp(Color.white, new Color(0.7f, 0.85f, 1f, 1f), newVal);
  }

  private void SetGemSaturation(List<DisenchantBar> disenchantBars, float saturation, bool onlyActive = false, bool onlyInactive = false)
  {
    using (List<DisenchantBar>.Enumerator enumerator = disenchantBars.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DisenchantBar current = enumerator.Current;
        int numCards = current.GetNumCards();
        if (onlyActive && numCards != 0 || onlyInactive && numCards == 0 || !onlyInactive && !onlyActive)
          current.m_rarityGem.GetComponent<Renderer>().material.SetColor("_Fade", new Color(saturation, saturation, saturation, 1f));
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator DoDisenchantAnims(int maxGlowBalls, int disenchantTotal)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MassDisenchant.\u003CDoDisenchantAnims\u003Ec__Iterator60()
    {
      disenchantTotal = disenchantTotal,
      maxGlowBalls = maxGlowBalls,
      \u003C\u0024\u003EdisenchantTotal = disenchantTotal,
      \u003C\u0024\u003EmaxGlowBalls = maxGlowBalls,
      \u003C\u003Ef__this = this
    };
  }

  private void SetDustBalance(float bal)
  {
    int num = (int) bal;
    if ((bool) UniversalInputManager.UsePhoneUI)
      ArcaneDustAmount.Get().m_dustCount.Text = num.ToString();
    else
      BnetBar.Get().m_currencyFrame.m_amount.Text = num.ToString();
  }

  private float DrainBarAndDust(DisenchantBar bar, int drainRun, float duration, float rate)
  {
    float numCards = (float) bar.GetNumCards();
    float num1 = numCards - (float) ((double) (drainRun + 1) * (double) numCards / ((double) duration / (double) rate));
    if ((double) num1 < 0.0)
      num1 = 0.0f;
    float amountDust = (float) bar.GetAmountDust();
    float num2 = amountDust - (float) ((double) (drainRun + 1) * (double) amountDust / ((double) duration / (double) rate));
    if ((double) num2 < 0.0)
      num2 = 0.0f;
    bar.m_numCardsText.Text = Convert.ToInt32(num1).ToString();
    bar.m_amountBar.GetComponent<Renderer>().material.SetFloat("_Percent", num1 / (float) this.m_totalCardsToDisenchant);
    bar.m_amountText.Text = Convert.ToInt32(num2).ToString();
    return num2;
  }

  private Vector3 GetRanBoxPt(GameObject box)
  {
    Vector3 localScale = box.transform.localScale;
    return box.transform.position + new Vector3(UnityEngine.Random.Range((float) (-(double) localScale.x / 2.0), localScale.x / 2f), UnityEngine.Random.Range((float) (-(double) localScale.y / 2.0), localScale.y / 2f), UnityEngine.Random.Range((float) (-(double) localScale.z / 2.0), localScale.z / 2f));
  }

  [DebuggerHidden]
  private IEnumerator LaunchGlowball(DisenchantBar bar, RarityFX rareFX, int glowBallNum, int totalGlowBalls, int m_highestGlowBalls)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MassDisenchant.\u003CLaunchGlowball\u003Ec__Iterator61()
    {
      glowBallNum = glowBallNum,
      m_highestGlowBalls = m_highestGlowBalls,
      totalGlowBalls = totalGlowBalls,
      rareFX = rareFX,
      bar = bar,
      \u003C\u0024\u003EglowBallNum = glowBallNum,
      \u003C\u0024\u003Em_highestGlowBalls = m_highestGlowBalls,
      \u003C\u0024\u003EtotalGlowBalls = totalGlowBalls,
      \u003C\u0024\u003ErareFX = rareFX,
      \u003C\u0024\u003Ebar = bar,
      \u003C\u003Ef__this = this
    };
  }

  private RarityFX GetRarityFX(DisenchantBar bar)
  {
    RarityFX rarityFx = new RarityFX();
    switch (bar.m_rarity)
    {
      case TAG_RARITY.RARE:
        rarityFx.burstFX = this.m_FX.m_burstFX_Rare;
        rarityFx.explodeFX = !(bool) UniversalInputManager.UsePhoneUI ? BnetBar.Get().m_currencyFrame.m_explodeFX_Rare : ArcaneDustAmount.Get().m_explodeFX_Rare;
        rarityFx.glowBallMat = this.m_FX.m_glowBallMat_Rare;
        rarityFx.glowTrailMat = this.m_FX.m_glowTrailMat_Rare;
        break;
      case TAG_RARITY.EPIC:
        rarityFx.burstFX = this.m_FX.m_burstFX_Epic;
        rarityFx.explodeFX = !(bool) UniversalInputManager.UsePhoneUI ? BnetBar.Get().m_currencyFrame.m_explodeFX_Epic : ArcaneDustAmount.Get().m_explodeFX_Epic;
        rarityFx.glowBallMat = this.m_FX.m_glowBallMat_Epic;
        rarityFx.glowTrailMat = this.m_FX.m_glowTrailMat_Epic;
        break;
      case TAG_RARITY.LEGENDARY:
        rarityFx.burstFX = this.m_FX.m_burstFX_Legendary;
        rarityFx.explodeFX = !(bool) UniversalInputManager.UsePhoneUI ? BnetBar.Get().m_currencyFrame.m_explodeFX_Legendary : ArcaneDustAmount.Get().m_explodeFX_Legendary;
        rarityFx.glowBallMat = this.m_FX.m_glowBallMat_Legendary;
        rarityFx.glowTrailMat = this.m_FX.m_glowTrailMat_Legendary;
        break;
      default:
        rarityFx.burstFX = this.m_FX.m_burstFX_Common;
        rarityFx.explodeFX = !(bool) UniversalInputManager.UsePhoneUI ? BnetBar.Get().m_currencyFrame.m_explodeFX_Legendary : ArcaneDustAmount.Get().m_explodeFX_Legendary;
        rarityFx.glowBallMat = this.m_FX.m_glowBallMat_Common;
        rarityFx.glowTrailMat = this.m_FX.m_glowTrailMat_Common;
        break;
    }
    return rarityFx;
  }

  private RaritySound GetRaritySound(DisenchantBar bar)
  {
    RaritySound raritySound = new RaritySound();
    switch (bar.m_rarity)
    {
      case TAG_RARITY.RARE:
        raritySound.m_drainSound = this.m_sound.m_rare.m_drainSound;
        raritySound.m_jarSound = this.m_sound.m_rare.m_jarSound;
        raritySound.m_missileSound = this.m_sound.m_rare.m_missileSound;
        break;
      case TAG_RARITY.EPIC:
        raritySound.m_drainSound = this.m_sound.m_epic.m_drainSound;
        raritySound.m_jarSound = this.m_sound.m_epic.m_jarSound;
        raritySound.m_missileSound = this.m_sound.m_epic.m_missileSound;
        break;
      case TAG_RARITY.LEGENDARY:
        raritySound.m_drainSound = this.m_sound.m_legendary.m_drainSound;
        raritySound.m_jarSound = this.m_sound.m_legendary.m_jarSound;
        raritySound.m_missileSound = this.m_sound.m_legendary.m_missileSound;
        break;
      default:
        raritySound.m_drainSound = this.m_sound.m_common.m_drainSound;
        raritySound.m_jarSound = this.m_sound.m_common.m_jarSound;
        raritySound.m_missileSound = this.m_sound.m_common.m_missileSound;
        break;
    }
    return raritySound;
  }
}
