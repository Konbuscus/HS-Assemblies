﻿// Decompiled with JetBrains decompiler
// Type: ChessAttackAnimation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class ChessAttackAnimation : Spell
{
  public float m_ImpactEffectDelay = 0.3f;
  public float m_SpellFinishDelay = 0.15f;
  public GameObject m_ChessShockwaveRed;
  public GameObject m_ChessShockwaveBlue;
  public GameObject m_ChessTrailRed;
  public GameObject m_ChessTrailBlue;
  public GameObject m_ChessImpactRed;
  public GameObject m_ChessImpactBlue;
  public GameObject m_ChessSettleDust;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_ShowAttackSoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_ShowImpactSoundPrefab;

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    this.StartCoroutine(this.AttackAnimation());
  }

  [DebuggerHidden]
  private IEnumerator AttackAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ChessAttackAnimation.\u003CAttackAnimation\u003Ec__Iterator2AE() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator PlayImpactEffects()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ChessAttackAnimation.\u003CPlayImpactEffects\u003Ec__Iterator2AF() { \u003C\u003Ef__this = this };
  }

  private void ShakeMinion(GameObject target, Vector3 targetOrgPos, Vector3 targetOrgRot)
  {
    iTween.MoveTo(target, iTween.Hash((object) "position", (object) new Vector3(targetOrgPos.x, targetOrgPos.y + 0.15f, targetOrgPos.z), (object) "time", (object) 0.05f, (object) "islocal", (object) true));
    iTween.RotateTo(target, iTween.Hash((object) "rotation", (object) new Vector3(Random.Range(-15f, 15f), Random.Range(-15f, 15f), Random.Range(-15f, 15f)), (object) "time", (object) 0.08f, (object) "islocal", (object) true));
    iTween.RotateTo(target, iTween.Hash((object) "rotation", (object) new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f)), (object) "time", (object) 0.08f, (object) "islocal", (object) true, (object) "delay", (object) 0.08f));
    iTween.RotateTo(target, iTween.Hash((object) "rotation", (object) new Vector3(Random.Range(-5f, 5f), Random.Range(-5f, 5f), Random.Range(-5f, 5f)), (object) "time", (object) 0.08f, (object) "islocal", (object) true, (object) "delay", (object) 0.16f));
    iTween.MoveTo(target, iTween.Hash((object) "position", (object) targetOrgPos, (object) "time", (object) 0.08f, (object) "islocal", (object) true, (object) "delay", (object) 0.24f));
    iTween.RotateTo(target, iTween.Hash((object) "rotation", (object) targetOrgRot, (object) "time", (object) 0.08f, (object) "islocal", (object) true, (object) "delay", (object) 0.24f));
  }

  [DebuggerHidden]
  private IEnumerator DoSpellFinished()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ChessAttackAnimation.\u003CDoSpellFinished\u003Ec__Iterator2B0() { \u003C\u003Ef__this = this };
  }
}
