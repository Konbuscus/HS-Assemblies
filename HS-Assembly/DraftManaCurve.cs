﻿// Decompiled with JetBrains decompiler
// Type: DraftManaCurve
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DraftManaCurve : MonoBehaviour
{
  private const int MAX_CARDS = 10;
  private const float SIZE_PER_CARD = 0.1f;
  public List<ManaCostBar> m_bars;
  private List<int> m_manaCosts;

  private void Awake()
  {
    this.ResetBars();
  }

  public void UpdateBars()
  {
    int num = 0;
    using (List<int>.Enumerator enumerator = this.m_manaCosts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        if (current > num)
          num = current;
      }
    }
    if (num < 10)
      num = 10;
    for (int index = 0; index < this.m_bars.Count; ++index)
    {
      this.m_bars[index].m_maxValue = (float) num;
      this.m_bars[index].AnimateBar((float) this.m_manaCosts[index]);
    }
  }

  public void AddCardOfCost(int cost)
  {
    if (this.m_manaCosts == null)
      return;
    cost = Mathf.Clamp(cost, 0, 7);
    List<int> manaCosts;
    int index;
    (manaCosts = this.m_manaCosts)[index = cost] = manaCosts[index] + 1;
    this.UpdateBars();
  }

  public void ResetBars()
  {
    this.m_manaCosts = new List<int>();
    for (int index = 0; index < this.m_bars.Count; ++index)
      this.m_manaCosts.Add(0);
    this.UpdateBars();
  }

  public void AddCardToManaCurve(EntityDef entityDef)
  {
    if (entityDef == null)
      Debug.LogWarning((object) "DraftManaCurve.AddCardToManaCurve() - entityDef is null");
    else
      this.AddCardOfCost(entityDef.GetCost());
  }
}
