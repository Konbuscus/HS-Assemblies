﻿// Decompiled with JetBrains decompiler
// Type: GameplayErrorManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GameplayErrorManager : MonoBehaviour
{
  private static GameplayErrorManager s_instance;
  private static GameplayErrorCloud s_messageInstance;
  private GUIStyle m_errorDisplayStyle;
  private string m_message;
  private float m_displaySecsLeft;
  public GameplayErrorCloud m_errorMessagePrefab;

  private void Awake()
  {
    GameplayErrorManager.s_instance = this;
    GameplayErrorManager.s_messageInstance = Object.Instantiate<GameplayErrorCloud>(this.m_errorMessagePrefab);
  }

  private void OnDestroy()
  {
    GameplayErrorManager.s_instance = (GameplayErrorManager) null;
  }

  private void Start()
  {
    this.m_message = string.Empty;
    this.m_errorDisplayStyle = new GUIStyle();
    this.m_errorDisplayStyle.fontSize = 24;
    this.m_errorDisplayStyle.fontStyle = FontStyle.Bold;
    this.m_errorDisplayStyle.alignment = TextAnchor.UpperCenter;
  }

  public static GameplayErrorManager Get()
  {
    return GameplayErrorManager.s_instance;
  }

  public void DisplayMessage(string message)
  {
    this.m_message = message;
    this.m_displaySecsLeft = (float) message.Length * 0.1f;
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      GameplayErrorManager.s_messageInstance.transform.localPosition = new Vector3(-7.9f, 9f, -4.43f);
      GameplayErrorManager.s_messageInstance.gameObject.GetComponentInChildren<UberText>().gameObject.transform.localPosition = new Vector3(2.49f, 0.0f, -2.13f);
    }
    else
      GameplayErrorManager.s_messageInstance.transform.localPosition = new Vector3(-7.9f, 9.98f, -5.17f);
    GameplayErrorManager.s_messageInstance.ShowMessage(this.m_message, this.m_displaySecsLeft);
    SoundManager.Get().LoadAndPlay("UI_no_can_do");
  }
}
