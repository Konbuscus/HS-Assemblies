﻿// Decompiled with JetBrains decompiler
// Type: SoundDuckedCategoryDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class SoundDuckedCategoryDef
{
  public float m_Volume = 0.2f;
  public float m_BeginSec = 0.7f;
  public iTween.EaseType m_BeginEaseType = iTween.EaseType.linear;
  public float m_RestoreSec = 0.7f;
  public iTween.EaseType m_RestoreEaseType = iTween.EaseType.linear;
  public SoundCategory m_Category;
}
