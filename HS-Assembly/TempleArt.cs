﻿// Decompiled with JetBrains decompiler
// Type: TempleArt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TempleArt : MonoBehaviour
{
  public float m_portraitSwapDelay = 0.5f;
  public List<Texture2D> m_portraits;
  public Spell m_portraitSwapSpell;

  public void DoPortraitSwap(Actor actor, int turn)
  {
    this.StartCoroutine(this.DoPortraitSwapWithTiming(actor, turn));
  }

  [DebuggerHidden]
  private IEnumerator DoPortraitSwapWithTiming(Actor actor, int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TempleArt.\u003CDoPortraitSwapWithTiming\u003Ec__Iterator2A()
    {
      actor = actor,
      turn = turn,
      \u003C\u0024\u003Eactor = actor,
      \u003C\u0024\u003Eturn = turn,
      \u003C\u003Ef__this = this
    };
  }

  private Texture2D GetArtForTurn(int turn)
  {
    return this.m_portraits[turn];
  }
}
