﻿// Decompiled with JetBrains decompiler
// Type: AspectRatioDependentPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AspectRatioDependentPosition : MonoBehaviour
{
  public Vector3 m_minLocalPosition;

  private void Awake()
  {
    this.transform.localPosition = TransformUtil.GetAspectRatioDependentPosition(this.m_minLocalPosition, this.transform.localPosition);
  }
}
