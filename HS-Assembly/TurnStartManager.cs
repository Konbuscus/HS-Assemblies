﻿// Decompiled with JetBrains decompiler
// Type: TurnStartManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TurnStartManager : MonoBehaviour
{
  private List<Card> m_cardsToDraw = new List<Card>();
  private List<TurnStartManager.CardChange> m_exhaustedChangesToHandle = new List<TurnStartManager.CardChange>();
  public TurnStartIndicator m_turnStartPrefab;
  private static TurnStartManager s_instance;
  private TurnStartIndicator m_turnStartInstance;
  private bool m_listeningForTurnEvents;
  private int m_manaCrystalsGained;
  private int m_manaCrystalsFilled;
  private SpellController m_spellController;
  private bool m_blockingInput;
  private bool m_twoScoopsDisplayed;

  private void Awake()
  {
    TurnStartManager.s_instance = this;
    this.m_turnStartInstance = UnityEngine.Object.Instantiate<TurnStartIndicator>(this.m_turnStartPrefab);
    this.m_turnStartInstance.transform.parent = this.transform;
    GameState.Get().RegisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
  }

  private void OnDestroy()
  {
    TurnStartManager.s_instance = (TurnStartManager) null;
  }

  public static TurnStartManager Get()
  {
    return TurnStartManager.s_instance;
  }

  public bool IsListeningForTurnEvents()
  {
    return this.m_listeningForTurnEvents;
  }

  public void BeginListeningForTurnEvents()
  {
    this.m_cardsToDraw.Clear();
    this.m_exhaustedChangesToHandle.Clear();
    this.m_manaCrystalsGained = 0;
    this.m_manaCrystalsFilled = 0;
    this.m_twoScoopsDisplayed = false;
    this.m_listeningForTurnEvents = true;
    this.m_blockingInput = true;
  }

  public void NotifyOfManaCrystalGained(int amount)
  {
    this.m_manaCrystalsGained += amount;
  }

  public void NotifyOfManaCrystalFilled(int amount)
  {
    this.m_manaCrystalsFilled += amount;
  }

  public void NotifyOfCardDrawn(Entity drawnEntity)
  {
    this.m_cardsToDraw.Add(drawnEntity.GetCard());
  }

  public void NotifyOfExhaustedChange(Card card, TagDelta tagChange)
  {
    this.m_exhaustedChangesToHandle.Add(new TurnStartManager.CardChange()
    {
      m_card = card,
      m_tagDelta = tagChange
    });
  }

  public void NotifyOfSpellController(SpellController spellController)
  {
    this.m_spellController = spellController;
    this.BeginPlayingTurnEvents();
  }

  public SpellController GetSpellController()
  {
    return this.m_spellController;
  }

  public int GetNumCardsToDraw()
  {
    return this.m_cardsToDraw.Count;
  }

  public List<Card> GetCardsToDraw()
  {
    return this.m_cardsToDraw;
  }

  public bool IsCardDrawHandled(Card card)
  {
    if ((UnityEngine.Object) card == (UnityEngine.Object) null)
      return false;
    return this.m_cardsToDraw.Contains(card);
  }

  public void BeginPlayingTurnEvents()
  {
    this.StartCoroutine(this.RunTurnEventsWithTiming());
  }

  public void NotifyOfTriggerVisual()
  {
    this.DisplayTwoScoops();
  }

  public bool IsBlockingInput()
  {
    return this.m_blockingInput;
  }

  public bool IsTurnStartIndicatorShowing()
  {
    if ((UnityEngine.Object) this.m_turnStartInstance == (UnityEngine.Object) null)
      return false;
    return this.m_turnStartInstance.IsShown();
  }

  private void DisplayTwoScoops()
  {
    if (this.m_twoScoopsDisplayed)
      return;
    this.m_twoScoopsDisplayed = true;
    this.m_turnStartInstance.SetReminderText(GameState.Get().GetGameEntity().GetTurnStartReminderText());
    this.m_turnStartInstance.Show();
    SoundManager.Get().LoadAndPlay("ALERT_YourTurn_0v2");
  }

  [DebuggerHidden]
  private IEnumerator RunTurnEventsWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TurnStartManager.\u003CRunTurnEventsWithTiming\u003Ec__IteratorE7() { \u003C\u003Ef__this = this };
  }

  private bool AreDrawnCardsReady(Card[] cardsToDraw)
  {
    return !(bool) ((UnityEngine.Object) Array.Find<Card>(cardsToDraw, (Predicate<Card>) (card => !card.IsActorReady())));
  }

  private bool HasActionsAfterCardDraw()
  {
    if ((UnityEngine.Object) this.m_spellController != (UnityEngine.Object) null)
      return true;
    Network.EntityChoices friendlyEntityChoices = GameState.Get().GetFriendlyEntityChoices();
    return friendlyEntityChoices != null && friendlyEntityChoices.ChoiceType == CHOICE_TYPE.GENERAL;
  }

  private void HandleExhaustedChanges()
  {
    using (List<TurnStartManager.CardChange>.Enumerator enumerator = this.m_exhaustedChangesToHandle.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TurnStartManager.CardChange current = enumerator.Current;
        Card card = current.m_card;
        switch (card.GetEntity().GetZone())
        {
          case TAG_ZONE.PLAY:
          case TAG_ZONE.SECRET:
            card.ShowExhaustedChange(current.m_tagDelta.newValue);
            continue;
          default:
            continue;
        }
      }
    }
    this.m_exhaustedChangesToHandle.Clear();
  }

  private void OnGameOver(TAG_PLAYSTATE playState, object userData)
  {
    this.StopAllCoroutines();
  }

  private class CardChange
  {
    public Card m_card;
    public TagDelta m_tagDelta;
  }
}
