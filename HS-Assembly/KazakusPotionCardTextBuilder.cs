﻿// Decompiled with JetBrains decompiler
// Type: KazakusPotionCardTextBuilder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class KazakusPotionCardTextBuilder : CardTextBuilder
{
  public override string BuildCardTextInHand(Entity entity)
  {
    string power1;
    string power2;
    this.GetPowersText(entity, out power1, out power2);
    string powersText = string.Format(CardTextBuilder.GetRawCardTextInHand(entity.GetCardId()), (object) power1, (object) power2);
    return TextUtils.TransformCardText(entity.GetDamageBonus(), entity.GetDamageBonusDouble(), entity.GetHealingDouble(), powersText);
  }

  public override string BuildCardTextInHand(EntityDef entityDef)
  {
    return string.Empty;
  }

  public override string BuildCardTextInHistory(Entity entity)
  {
    string power1;
    string power2;
    this.GetPowersText(entity, out power1, out power2);
    CardTextHistoryData cardTextHistoryData = entity.GetCardTextHistoryData();
    if (cardTextHistoryData == null)
    {
      Log.All.Print("KazakusPotionCardTextBuilder.BuildCardTextInHistory: entity {0} does not have a CardTextHistoryData object.", (object) entity.GetEntityId());
      return string.Empty;
    }
    string powersText = string.Format(CardTextBuilder.GetRawCardTextInHand(entity.GetCardId()), (object) power1, (object) power2);
    return TextUtils.TransformCardText(cardTextHistoryData.m_damageBonus, cardTextHistoryData.m_damageBonusDouble, cardTextHistoryData.m_healingDouble, powersText);
  }

  private void GetPowersText(Entity entity, out string power1, out string power2)
  {
    power1 = string.Empty;
    if (entity.HasTag(GAME_TAG.KAZAKUS_POTION_POWER_1))
    {
      EntityDef entityDef = DefLoader.Get().GetEntityDef(entity.GetTag(GAME_TAG.KAZAKUS_POTION_POWER_1));
      if (entityDef != null)
      {
        power1 = CardTextBuilder.GetRawCardTextInHand(entityDef.GetCardId());
        power1 = this.GetPowerTextSubstring(power1);
      }
    }
    power2 = string.Empty;
    if (!entity.HasTag(GAME_TAG.KAZAKUS_POTION_POWER_2))
      return;
    EntityDef entityDef1 = DefLoader.Get().GetEntityDef(entity.GetTag(GAME_TAG.KAZAKUS_POTION_POWER_2));
    if (entityDef1 == null)
      return;
    power2 = CardTextBuilder.GetRawCardTextInHand(entityDef1.GetCardId());
    power2 = this.GetPowerTextSubstring(power2);
  }

  private string GetPowerTextSubstring(string powerText)
  {
    int num = powerText.IndexOf('@');
    if (num >= 0)
      return powerText.Substring(num + 1);
    return powerText;
  }
}
