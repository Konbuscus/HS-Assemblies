﻿// Decompiled with JetBrains decompiler
// Type: PlayMakerAnimatorMoveProxy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (PlayMakerFSM))]
public class PlayMakerAnimatorMoveProxy : MonoBehaviour
{
  public bool applyRootMotion;

  public event Action OnAnimatorMoveEvent;

  private void Start()
  {
  }

  private void Update()
  {
  }

  private void OnAnimatorMove()
  {
    if (this.OnAnimatorMoveEvent == null)
      return;
    this.OnAnimatorMoveEvent();
  }
}
