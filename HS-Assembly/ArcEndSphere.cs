﻿// Decompiled with JetBrains decompiler
// Type: ArcEndSphere
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArcEndSphere : MonoBehaviour
{
  private void Start()
  {
  }

  private void Update()
  {
    Vector2 mainTextureOffset = this.GetComponent<Renderer>().material.mainTextureOffset;
    mainTextureOffset.x += Time.deltaTime * 1f;
    this.GetComponent<Renderer>().material.mainTextureOffset = mainTextureOffset;
  }
}
