﻿// Decompiled with JetBrains decompiler
// Type: OptionsMigration
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public static class OptionsMigration
{
  private static readonly Map<int, OptionsMigration.UpgradeCallback> s_clientUpgradeCallbacks = new Map<int, OptionsMigration.UpgradeCallback>();
  private static readonly Map<int, OptionsMigration.UpgradeCallback> s_serverUpgradeCallbacks = new Map<int, OptionsMigration.UpgradeCallback>() { { 2, new OptionsMigration.UpgradeCallback(OptionsMigration.UpgradeServerOptions_V3) }, { 3, new OptionsMigration.UpgradeCallback(OptionsMigration.UpgradeServerOptions_V4) } };
  public const int LATEST_CLIENT_VERSION = 2;
  public const int LATEST_SERVER_VERSION = 4;

  public static bool UpgradeClientOptions()
  {
    int key = Options.Get().GetInt(Option.CLIENT_OPTIONS_VERSION);
    int startingVersion = key;
    if (!Options.Get().HasOption(Option.CLIENT_OPTIONS_VERSION))
    {
      if (!OptionsMigration.UpgradeClientOptions_V2())
        return false;
      key = 2;
    }
    for (; key < 2; ++key)
    {
      OptionsMigration.UpgradeCallback upgradeCallback;
      if (!OptionsMigration.s_clientUpgradeCallbacks.TryGetValue(key, out upgradeCallback))
      {
        Error.AddDevFatal("OptionsMigration.UpgradeClientOptions() - Current version is {0} and there is no function to upgrade to {1}. Latest is {2}.", (object) key, (object) (key + 1), (object) 2);
        return false;
      }
      if (!upgradeCallback(startingVersion))
        return false;
    }
    return true;
  }

  private static bool UpgradeClientOptions_V2()
  {
    Options.Get().SetInt(Option.CLIENT_OPTIONS_VERSION, 2);
    return Options.Get().GetInt(Option.CLIENT_OPTIONS_VERSION) == 2;
  }

  public static bool UpgradeServerOptions()
  {
    int key = Options.Get().GetInt(Option.SERVER_OPTIONS_VERSION);
    int startingVersion = key;
    if (!Options.Get().HasOption(Option.SERVER_OPTIONS_VERSION))
    {
      if (!OptionsMigration.UpgradeServerOptions_V2())
        return false;
      key = 2;
    }
    for (; key < 4; ++key)
    {
      OptionsMigration.UpgradeCallback upgradeCallback;
      if (!OptionsMigration.s_serverUpgradeCallbacks.TryGetValue(key, out upgradeCallback))
      {
        Error.AddDevFatal("OptionsMigration.UpgradeServerOptions() - Current version is {0} and there is no function to upgrade to {1}. Latest is {2}.", (object) key, (object) (key + 1), (object) 4);
        return false;
      }
      if (!upgradeCallback(startingVersion))
        return false;
    }
    return true;
  }

  private static bool UpgradeServerOptions_V2()
  {
    Options.Get().SetInt(Option.SERVER_OPTIONS_VERSION, 2);
    return Options.Get().GetInt(Option.SERVER_OPTIONS_VERSION) == 2;
  }

  private static bool UpgradeServerOptions_V3(int startingVersion)
  {
    if (startingVersion != 0)
    {
      bool flag = false;
      if (AchieveManager.Get() != null && AchieveManager.Get().IsReady())
        flag = AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES);
      else if (Options.Get().GetBool(Option.HAS_SEEN_EXPERT_AI_UNLOCK))
        flag = true;
      if (flag)
      {
        Options.Get().SetBool(Option.HAS_SEEN_UNLOCK_ALL_HEROES_TRANSITION, true);
        Options.Get().SetBool(Option.HAS_CLICKED_COLLECTION_BUTTON_FOR_NEW_CARD, true);
        Options.Get().SetBool(Option.HAS_CLICKED_COLLECTION_BUTTON_FOR_NEW_DECK, true);
      }
      if (Options.Get().GetBool(Option.HAS_STARTED_A_DECK))
      {
        Options.Get().SetBool(Option.HAS_REMOVED_CARD_FROM_DECK, true);
        Options.Get().SetBool(Option.HAS_SEEN_DELETE_DECK_REMINDER, true);
      }
    }
    Options.Get().SetInt(Option.SERVER_OPTIONS_VERSION, 3);
    return Options.Get().GetInt(Option.SERVER_OPTIONS_VERSION) == 3;
  }

  private static bool UpgradeServerOptions_V4(int startingVersion)
  {
    if (GameUtils.HasSeenStandardModeTutorial())
      Options.Get().SetBool(Option.HAS_SEEN_SET_FILTER_TUTORIAL, true);
    Options.Get().SetInt(Option.SERVER_OPTIONS_VERSION, 4);
    return Options.Get().GetInt(Option.SERVER_OPTIONS_VERSION) == 4;
  }

  private delegate bool UpgradeCallback(int startingVersion);
}
