﻿// Decompiled with JetBrains decompiler
// Type: ArcEnd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArcEnd : MonoBehaviour
{
  private Vector3 s;
  public Light l;

  private void Start()
  {
    this.s = this.transform.localScale;
  }

  private void FixedUpdate()
  {
    this.transform.rotation = Quaternion.LookRotation(Vector3.up, Camera.main.transform.position - this.transform.position);
    this.transform.Rotate(Vector3.up, Random.value * 360f);
    if ((double) Random.value > 0.800000011920929)
    {
      this.transform.localScale = this.s * 1.5f;
      if (!((Object) this.l != (Object) null))
        return;
      this.l.range = 100f;
      this.l.intensity = 1.5f;
    }
    else
    {
      this.transform.localScale = this.s;
      if (!((Object) this.l != (Object) null))
        return;
      this.l.range = 50f;
      this.l.intensity = 1f;
    }
  }
}
