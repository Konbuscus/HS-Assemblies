﻿// Decompiled with JetBrains decompiler
// Type: EmoteEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EmoteEntry
{
  private EmoteType m_emoteType;
  private CardSoundSpell m_emoteSoundSpell;
  private string m_emoteGameStringKey;
  private string m_emoteSoundSpellPath;
  private Card m_owner;

  public EmoteEntry(EmoteType type, string path, string stringKey, Card owner)
  {
    this.m_emoteType = type;
    this.m_emoteSoundSpellPath = path;
    this.m_emoteGameStringKey = stringKey;
    this.m_owner = owner;
  }

  public EmoteType GetEmoteType()
  {
    return this.m_emoteType;
  }

  public string GetGameStringKey()
  {
    return this.m_emoteGameStringKey;
  }

  private void LoadSpell()
  {
    if (string.IsNullOrEmpty(this.m_emoteSoundSpellPath))
      return;
    GameObject gameObject = AssetLoader.Get().LoadSpell(this.m_emoteSoundSpellPath, true, false);
    if ((Object) gameObject == (Object) null)
    {
      if (AssetLoader.DOWNLOADABLE_LANGUAGE_PACKS)
        return;
      Error.AddDevFatalUnlessWorkarounds("EmoteEntry.LoadSpell() - Failed to load \"{0}\"", (object) this.m_emoteSoundSpellPath);
    }
    else
    {
      this.m_emoteSoundSpell = gameObject.GetComponent<CardSoundSpell>();
      if ((Object) this.m_emoteSoundSpell == (Object) null)
      {
        Object.Destroy((Object) gameObject);
        Error.AddDevFatalUnlessWorkarounds("EmoteEntry.LoadSpell() - \"{0}\" does not have a Spell component.", (object) this.m_emoteSoundSpellPath);
      }
      else
      {
        if (!((Object) this.m_owner != (Object) null))
          return;
        SpellUtils.SetupSoundSpell(this.m_emoteSoundSpell, (Component) this.m_owner);
      }
    }
  }

  public CardSoundSpell GetSpell(bool loadIfNeeded = true)
  {
    if ((Object) this.m_emoteSoundSpell == (Object) null && loadIfNeeded)
      this.LoadSpell();
    return this.m_emoteSoundSpell;
  }

  public CardSoundSpell GetSpellIfLoaded()
  {
    return this.m_emoteSoundSpell;
  }

  public void Clear()
  {
    if (!((Object) this.m_emoteSoundSpell != (Object) null))
      return;
    Object.Destroy((Object) this.m_emoteSoundSpell.gameObject);
    this.m_emoteSoundSpell = (CardSoundSpell) null;
  }
}
