﻿// Decompiled with JetBrains decompiler
// Type: PlayMakerAnimatorStateSynchronization
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using HutongGames.PlayMaker;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class PlayMakerAnimatorStateSynchronization : MonoBehaviour
{
  public bool EveryFrame = true;
  public int LayerIndex;
  public PlayMakerFSM Fsm;
  public bool debug;
  private Animator animator;
  private int lastState;
  private int lastTransition;
  private Dictionary<int, FsmState> fsmStateLUT;

  private void Start()
  {
    this.animator = this.GetComponent<Animator>();
    if (!((Object) this.Fsm != (Object) null))
      return;
    string layerName = this.animator.GetLayerName(this.LayerIndex);
    this.fsmStateLUT = new Dictionary<int, FsmState>();
    foreach (FsmState state in this.Fsm.Fsm.States)
    {
      string name = state.Name;
      this.RegisterHash(state.Name, state);
      if (!name.StartsWith(layerName + "."))
        this.RegisterHash(layerName + "." + state.Name, state);
    }
  }

  private void RegisterHash(string key, FsmState state)
  {
    int hash = Animator.StringToHash(key);
    this.fsmStateLUT.Add(hash, state);
    if (!this.debug)
      return;
    Debug.Log((object) ("registered " + key + " ->" + (object) hash));
  }

  private void Update()
  {
    if (!this.EveryFrame)
      return;
    this.Synchronize();
  }

  public void Synchronize()
  {
    if ((Object) this.animator == (Object) null || (Object) this.Fsm == (Object) null)
      return;
    bool flag = false;
    if (this.animator.IsInTransition(this.LayerIndex))
    {
      int nameHash = this.animator.GetAnimatorTransitionInfo(this.LayerIndex).nameHash;
      int userNameHash = this.animator.GetAnimatorTransitionInfo(this.LayerIndex).userNameHash;
      if (this.lastTransition != nameHash)
      {
        if (this.debug)
          Debug.Log((object) "is in transition");
        if (this.fsmStateLUT.ContainsKey(userNameHash))
        {
          FsmState state = this.fsmStateLUT[userNameHash];
          if (this.Fsm.Fsm.ActiveState != state)
          {
            this.SwitchState(this.Fsm.Fsm, state);
            flag = true;
          }
        }
        if (!flag && this.fsmStateLUT.ContainsKey(nameHash))
        {
          FsmState state = this.fsmStateLUT[nameHash];
          if (this.Fsm.Fsm.ActiveState != state)
          {
            this.SwitchState(this.Fsm.Fsm, state);
            flag = true;
          }
        }
        if (!flag && this.debug)
          Debug.LogWarning((object) ("Fsm is missing animator transition name or username for hash:" + (object) nameHash));
        this.lastTransition = nameHash;
      }
    }
    if (flag)
      return;
    int fullPathHash = this.animator.GetCurrentAnimatorStateInfo(this.LayerIndex).fullPathHash;
    if (this.lastState == fullPathHash)
      return;
    if (this.debug)
      Debug.Log((object) "Net state switch");
    if (this.fsmStateLUT.ContainsKey(fullPathHash))
    {
      FsmState state = this.fsmStateLUT[fullPathHash];
      if (this.Fsm.Fsm.ActiveState != state)
        this.SwitchState(this.Fsm.Fsm, state);
    }
    else if (this.debug)
      Debug.LogWarning((object) ("Fsm is missing animator state hash:" + (object) fullPathHash));
    this.lastState = fullPathHash;
  }

  private void SwitchState(HutongGames.PlayMaker.Fsm fsm, FsmState state)
  {
    MethodInfo method = ((object) fsm).GetType().GetMethod("SwitchState", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
    if (method == null)
      return;
    method.Invoke((object) fsm, new object[1]
    {
      (object) state
    });
  }
}
