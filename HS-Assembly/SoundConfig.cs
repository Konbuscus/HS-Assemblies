﻿// Decompiled with JetBrains decompiler
// Type: SoundConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class SoundConfig : MonoBehaviour
{
  [CustomEditField(Sections = "Music")]
  public float m_SecondsBetweenMusicTracks = 10f;
  [CustomEditField(Sections = "Playback Limiting")]
  public List<SoundPlaybackLimitDef> m_PlaybackLimitDefs = new List<SoundPlaybackLimitDef>();
  [CustomEditField(Sections = "Ducking")]
  public List<SoundDuckingDef> m_DuckingDefs = new List<SoundDuckingDef>();
  [CustomEditField(Sections = "System Audio Sources")]
  public AudioSource m_PlayClipTemplate;
  [CustomEditField(Sections = "System Audio Sources")]
  public AudioSource m_PlaceholderSound;
}
