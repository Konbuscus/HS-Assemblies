﻿// Decompiled with JetBrains decompiler
// Type: CardStandIn
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardStandIn : MonoBehaviour
{
  public Card linkedCard;
  public Collider standInCollider;

  public void DisableStandIn()
  {
    this.standInCollider.enabled = false;
  }

  public void EnableStandIn()
  {
    this.standInCollider.enabled = true;
  }
}
