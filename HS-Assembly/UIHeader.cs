﻿// Decompiled with JetBrains decompiler
// Type: UIHeader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class UIHeader : ThreeSliceElement
{
  public UberText m_headerUberText;

  public void SetText(string t)
  {
    this.m_headerUberText.Text = t;
    this.SetMiddleWidth(this.m_headerUberText.GetTextWorldSpaceBounds().size.x);
  }
}
