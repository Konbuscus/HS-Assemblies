﻿// Decompiled with JetBrains decompiler
// Type: CollectionDeckBoxVisual
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class CollectionDeckBoxVisual : PegUIElement
{
  public static readonly float POPPED_UP_LOCAL_Z = 0.0f;
  public static readonly Vector3 POPPED_DOWN_LOCAL_POS = new Vector3(0.0f, -0.8598533f, 0.0f);
  public static readonly Vector3 SCALED_DOWN_LOCAL_SCALE = new Vector3(0.95f, 0.95f, 0.95f);
  private static readonly Color BASIC_SET_PROGRESS_ENABLED_COLOR = new Color(0.97f, 0.82f, 0.22f);
  private static readonly Color BASIC_SET_PROGRESS_DISABLED_COLOR = new Color(0.36f, 0.31f, 0.08f);
  private static readonly Color DECK_NAME_ENABLED_COLOR = Color.white;
  private static readonly Color DECK_NAME_DISABLED_COLOR = new Color(0.3f, 0.3f, 0.3f, 1f);
  private static readonly Color DECK_NAME_DISABLED_COLOR_THAI = new Color(0.7f, 0.7f, 0.7f, 1f);
  private Vector3 SCALED_UP_DECK_OFFSET = new Vector3(0.0f, 0.0f, 0.0f);
  private long m_deckID = -1;
  private bool m_isValid = true;
  private string m_heroCardID = string.Empty;
  private bool m_isWild = true;
  private bool m_animateButtonPress = true;
  public const float DECKBOX_SCALE = 0.95f;
  public const float SCALED_UP_LOCAL_Y_OFFSET = 3.238702f;
  public const float SCALED_DOWN_LOCAL_Y_OFFSET = 1.273138f;
  private const float BUTTON_POP_SPEED = 6f;
  private const string DECKBOX_POPUP_ANIM_NAME = "Deck_PopUp";
  private const string DECKBOX_POPDOWN_ANIM_NAME = "Deck_PopDown";
  private const float SCALE_TIME = 0.2f;
  private const float ADJUST_Y_OFFSET_ANIM_TIME = 0.05f;
  public UberText m_deckName;
  public UberText m_setProgressLabel;
  public GameObject m_labelGradient;
  public PegUIElement m_deleteButton;
  public GameObject m_highlight;
  public Texture2D m_standardHighlight;
  public Texture2D m_wildHighlight;
  public GameObject m_invalidDeckX;
  public GameObject m_portraitObject;
  public int m_portraitMaterialIndex;
  public GameObject m_wildPortraitObject;
  public int m_wildPortraitMaterialIndex;
  public GameObject m_classObject;
  public int m_classIconMaterialIndex;
  public int m_classBannerMaterialIndex;
  public GameObject m_wildClassObject;
  public int m_wildClassIconMaterialIndex;
  public int m_wildClassBannerMaterialIndex;
  public MeshRenderer m_topBannerRenderer;
  public MeshRenderer m_wildTopBannerRenderer;
  public int m_topBannerMaterialIndex;
  public Material m_xButtonMaterial;
  public Material m_xButtonWildMaterial;
  public GameObject m_pressedBone;
  public CustomDeckBones m_bones;
  public GameObject m_normalDeckVisuals;
  public GameObject m_lockedDeckVisuals;
  public TooltipZone m_tooltipZone;
  public GameObject m_renameVisuals;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_standardDeckSelectSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_wildDeckSelectSound;
  private Material m_classIconMaterial;
  private Material m_bannerMaterial;
  private bool m_isPoppedUp;
  private bool m_isShown;
  private FullDef m_fullDef;
  private EntityDef m_entityDef;
  private CardDef m_cardDef;
  private bool m_isMissingCards;
  private HighlightState m_highlightState;
  private Vector3 m_originalButtonPosition;
  private bool m_wasTouchModeEnabled;
  private int m_positionIndex;
  private bool m_showGlow;
  private bool m_isLocked;
  private Transform m_customDeckTransform;

  public static Vector3 SCALED_UP_LOCAL_SCALE { get; private set; }

  protected override void Awake()
  {
    base.Awake();
    this.SetEnabled(false);
    this.m_deleteButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDeleteButtonPressed));
    this.m_deleteButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnDeleteButtonOver));
    this.m_deleteButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnDeleteButtonRollout));
    this.ShowDeleteButton(false);
    this.m_invalidDeckX.SetActive(this.m_isMissingCards);
    this.m_deckName.RichText = false;
    SoundManager.Get().Load("tiny_button_press_1");
    SoundManager.Get().Load("tiny_button_mouseover_1");
    this.m_customDeckTransform = this.transform.FindChild("CustomDeck");
    this.m_highlightState = this.m_highlight.GetComponentsInChildren<HighlightState>(true)[0];
    CollectionDeckBoxVisual.SCALED_UP_LOCAL_SCALE = new Vector3(1.126f, 1.126f, 1.126f);
    if (PlatformSettings.s_screen == ScreenCategory.Phone)
    {
      CollectionDeckBoxVisual.SCALED_UP_LOCAL_SCALE = new Vector3(1.1f, 1.1f, 1.1f);
      this.SCALED_UP_DECK_OFFSET = new Vector3(0.0f, -0.2f, 0.0f);
    }
    this.m_wasTouchModeEnabled = UniversalInputManager.Get().IsTouchMode();
  }

  private void Update()
  {
    if (this.m_wasTouchModeEnabled == UniversalInputManager.Get().IsTouchMode())
      return;
    PegUIElement.InteractionState interactionState = this.GetInteractionState();
    if (this.m_wasTouchModeEnabled)
    {
      if (interactionState == PegUIElement.InteractionState.Down)
        this.OnPressEvent();
      else if (interactionState == PegUIElement.InteractionState.Over)
        this.OnOverEvent();
    }
    else
    {
      if (interactionState == PegUIElement.InteractionState.Down)
        this.OnReleaseEvent();
      else if (interactionState == PegUIElement.InteractionState.Over)
        this.OnOutEvent();
      this.ShowDeleteButton(false);
    }
    this.m_wasTouchModeEnabled = UniversalInputManager.Get().IsTouchMode();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.m_isShown = true;
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    this.m_isShown = false;
  }

  public bool IsShown()
  {
    return this.m_isShown;
  }

  public void SetDeckName(string deckName)
  {
    this.m_deckName.Text = deckName;
  }

  public UberText GetDeckNameText()
  {
    return this.m_deckName;
  }

  public void HideDeckName()
  {
    this.m_deckName.gameObject.SetActive(false);
  }

  public void ShowDeckName()
  {
    this.m_deckName.gameObject.SetActive(true);
  }

  public void HideRenameVisuals()
  {
    if (!((Object) this.m_renameVisuals != (Object) null))
      return;
    this.m_renameVisuals.SetActive(false);
  }

  public void ShowRenameVisuals()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL || !((Object) this.m_renameVisuals != (Object) null))
      return;
    this.m_renameVisuals.SetActive(true);
  }

  public void SetDeckID(long id)
  {
    this.m_deckID = id;
  }

  public long GetDeckID()
  {
    return this.m_deckID;
  }

  public CollectionDeck GetCollectionDeck()
  {
    return CollectionManager.Get().GetDeck(this.m_deckID);
  }

  public Texture GetHeroPortraitTexture()
  {
    if ((Object) this.m_cardDef == (Object) null)
      return (Texture) null;
    return this.m_cardDef.GetPortraitTexture();
  }

  public CardDef GetCardDef()
  {
    return this.m_cardDef;
  }

  public FullDef GetFullDef()
  {
    return this.m_fullDef;
  }

  public TAG_CLASS GetClass()
  {
    if (this.m_entityDef == null)
      return TAG_CLASS.INVALID;
    return this.m_entityDef.GetClass();
  }

  public string GetHeroCardID()
  {
    return this.m_heroCardID;
  }

  public void SetHeroCardID(string heroCardID)
  {
    this.m_heroCardID = heroCardID;
    DefLoader.Get().LoadFullDef(heroCardID, new DefLoader.LoadDefCallback<FullDef>(this.OnHeroFullDefLoaded));
  }

  public void SetShowGlow(bool showGlow)
  {
    this.m_showGlow = showGlow;
    if (!this.m_showGlow)
      return;
    this.SetHighlightState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }

  public bool IsWild()
  {
    return this.m_isWild;
  }

  public void TransitionFromStandardToWild()
  {
    this.SetFormat(false);
    Animator component = this.GetComponent<Animator>();
    if (!((Object) component != (Object) null))
      return;
    component.enabled = true;
    component.Play("CustomDeck_GlowOut", 0, 0.0f);
  }

  public void OnTransitionFromStandardToWild()
  {
    this.m_isWild = true;
    this.m_isValid = false;
    this.ReparentElements(this.m_isWild);
    this.m_highlightState.m_StaticSilouetteTexture = !this.m_isWild ? this.m_standardHighlight : this.m_wildHighlight;
    this.m_portraitObject.SetActive(false);
    if ((Object) this.m_wildPortraitObject != (Object) null)
    {
      this.m_wildPortraitObject.SetActive(true);
      if (!(bool) UniversalInputManager.UsePhoneUI)
        this.m_wildPortraitObject.GetComponent<Animator>().Play("Wild_RolldownActivate", 0, 1f);
      else
        this.m_wildPortraitObject.GetComponent<Animator>().Play("WildActivate", 0, 1f);
    }
    this.Invoke("PlayValidAnimation", 1f);
  }

  public void SetFormat(bool isWild)
  {
    this.m_isWild = isWild;
    this.ReparentElements(this.m_isWild);
    this.m_deleteButton.GetComponent<Renderer>().material = !isWild ? this.m_xButtonMaterial : this.m_xButtonWildMaterial;
    this.m_portraitObject.SetActive(!isWild);
    this.m_wildPortraitObject.SetActive(isWild);
    this.m_highlightState.m_StaticSilouetteTexture = !isWild ? this.m_standardHighlight : this.m_wildHighlight;
    this.PlayValidAnimation();
  }

  public void SetPositionIndex(int idx)
  {
    this.m_positionIndex = idx;
  }

  public int GetPositionIndex()
  {
    return this.m_positionIndex;
  }

  public void SetBasicSetProgress(TAG_CLASS classTag)
  {
    int basicCardsIown = CollectionManager.Get().GetBasicCardsIOwn(classTag);
    int num = 20;
    if (basicCardsIown == num || SceneMgr.Get().GetMode() == SceneMgr.Mode.COLLECTIONMANAGER)
    {
      this.m_deckName.transform.position = this.m_bones.m_deckLabelOneLine.position;
      this.m_labelGradient.transform.parent = this.m_bones.m_gradientOneLine;
      this.m_labelGradient.transform.localPosition = Vector3.zero;
      this.m_labelGradient.transform.localScale = Vector3.one;
      this.m_setProgressLabel.gameObject.SetActive(false);
    }
    else
    {
      this.m_deckName.transform.position = this.m_bones.m_deckLabelTwoLine.position;
      this.m_labelGradient.transform.parent = this.m_bones.m_gradientTwoLine;
      this.m_labelGradient.transform.localPosition = Vector3.zero;
      this.m_labelGradient.transform.localScale = Vector3.one;
      this.m_setProgressLabel.gameObject.SetActive(true);
      this.m_setProgressLabel.Text = GameStrings.Format(!(bool) UniversalInputManager.UsePhoneUI ? "GLUE_BASIC_SET_PROGRESS" : "GLUE_BASIC_SET_PROGRESS_PHONE", (object) basicCardsIown, (object) num);
    }
  }

  public bool IsMissingCards()
  {
    return this.m_isMissingCards;
  }

  public void SetIsMissingCards(bool isMissingCards)
  {
    if (this.m_isMissingCards == isMissingCards)
      return;
    this.m_isMissingCards = isMissingCards;
    this.m_invalidDeckX.SetActive(this.m_isMissingCards);
  }

  public bool IsValid()
  {
    return this.m_isValid;
  }

  public void SetIsValid(bool isValid)
  {
    if (this.m_isValid == isValid)
      return;
    this.m_isValid = isValid;
    this.PlayValidAnimation();
  }

  public bool IsLocked()
  {
    return this.m_isLocked;
  }

  public void SetIsLocked(bool isLocked)
  {
    if (this.m_isLocked == isLocked)
      return;
    this.m_isLocked = isLocked;
    this.m_normalDeckVisuals.SetActive(!this.m_isLocked);
    this.m_lockedDeckVisuals.SetActive(this.m_isLocked);
  }

  public void EnableButtonAnimation()
  {
    this.m_animateButtonPress = true;
  }

  public void DisableButtonAnimation()
  {
    this.m_animateButtonPress = false;
  }

  public void PlayScaleUpAnimation()
  {
    this.PlayScaleUpAnimation((CollectionDeckBoxVisual.DelOnAnimationFinished) null);
  }

  public void PlayScaleUpAnimation(CollectionDeckBoxVisual.DelOnAnimationFinished callback)
  {
    this.PlayScaleUpAnimation(callback, (object) null);
  }

  public void PlayScaleUpAnimation(CollectionDeckBoxVisual.DelOnAnimationFinished callback, object callbackData)
  {
    CollectionDeckBoxVisual.OnScaleFinishedCallbackData finishedCallbackData = new CollectionDeckBoxVisual.OnScaleFinishedCallbackData()
    {
      m_callback = callback,
      m_callbackData = callbackData
    };
    Vector3 localPosition = this.transform.localPosition;
    localPosition.y = 3.238702f;
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) localPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.05f, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "ScaleUpNow", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) finishedCallbackData));
  }

  private void ScaleUpNow(CollectionDeckBoxVisual.OnScaleFinishedCallbackData readyToScaleUpData)
  {
    this.ScaleDeckBox(true, readyToScaleUpData.m_callback, readyToScaleUpData.m_callbackData);
  }

  public void PlayScaleDownAnimation()
  {
    this.PlayScaleDownAnimation((CollectionDeckBoxVisual.DelOnAnimationFinished) null);
  }

  public void PlayScaleDownAnimation(CollectionDeckBoxVisual.DelOnAnimationFinished callback)
  {
    this.PlayScaleDownAnimation(callback, (object) null);
  }

  public void PlayScaleDownAnimation(CollectionDeckBoxVisual.DelOnAnimationFinished callback, object callbackData)
  {
    this.ScaleDeckBox(false, new CollectionDeckBoxVisual.DelOnAnimationFinished(this.OnScaledDown), (object) new CollectionDeckBoxVisual.OnScaleFinishedCallbackData()
    {
      m_callback = callback,
      m_callbackData = callbackData
    });
  }

  private void OnScaledDown(object callbackData)
  {
    CollectionDeckBoxVisual.OnScaleFinishedCallbackData finishedCallbackData = callbackData as CollectionDeckBoxVisual.OnScaleFinishedCallbackData;
    Vector3 localPosition = this.transform.localPosition;
    localPosition.y = 1.273138f;
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) localPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.05f, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "ScaleDownComplete", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) finishedCallbackData));
  }

  private void ScaleDownComplete(CollectionDeckBoxVisual.OnScaleFinishedCallbackData onScaledDownData)
  {
    if (onScaledDownData.m_callback == null)
      return;
    onScaledDownData.m_callback(onScaledDownData.m_callbackData);
  }

  public void PlayPopUpAnimation()
  {
    this.PlayPopUpAnimation((CollectionDeckBoxVisual.DelOnAnimationFinished) null);
  }

  public void PlayPopUpAnimation(CollectionDeckBoxVisual.DelOnAnimationFinished callback)
  {
    this.PlayPopUpAnimation(callback, (object) null);
  }

  public void PlayPopUpAnimation(CollectionDeckBoxVisual.DelOnAnimationFinished callback, object callbackData)
  {
    if (this.m_isPoppedUp)
    {
      if (callback == null)
        return;
      callback(callbackData);
    }
    else
    {
      this.m_isPoppedUp = true;
      if ((Object) this.m_customDeckTransform != (Object) null)
        this.m_customDeckTransform.localPosition += this.SCALED_UP_DECK_OFFSET;
      this.GetComponent<Animation>()["Deck_PopUp"].time = 0.0f;
      this.GetComponent<Animation>()["Deck_PopUp"].speed = 6f;
      this.PlayPopAnimation("Deck_PopUp", callback, callbackData);
    }
  }

  public void PlayPopDownAnimation()
  {
    this.PlayPopDownAnimation((CollectionDeckBoxVisual.DelOnAnimationFinished) null);
  }

  public void PlayPopDownAnimation(CollectionDeckBoxVisual.DelOnAnimationFinished callback)
  {
    this.PlayPopDownAnimation(callback, (object) null);
  }

  public void PlayPopDownAnimation(CollectionDeckBoxVisual.DelOnAnimationFinished callback, object callbackData)
  {
    if (!this.m_isPoppedUp)
    {
      if (callback == null)
        return;
      callback(callbackData);
    }
    else
    {
      this.m_isPoppedUp = false;
      if ((Object) this.m_customDeckTransform != (Object) null)
        this.m_customDeckTransform.localPosition -= this.SCALED_UP_DECK_OFFSET;
      this.GetComponent<Animation>()["Deck_PopDown"].time = 0.0f;
      this.GetComponent<Animation>()["Deck_PopDown"].speed = 6f;
      this.PlayPopAnimation("Deck_PopDown", callback, callbackData);
    }
  }

  public void PlayPopDownAnimationImmediately()
  {
    this.PlayPopDownAnimationImmediately((CollectionDeckBoxVisual.DelOnAnimationFinished) null);
  }

  public void PlayPopDownAnimationImmediately(CollectionDeckBoxVisual.DelOnAnimationFinished callback)
  {
    this.PlayPopDownAnimationImmediately(callback, (object) null);
  }

  public void PlayPopDownAnimationImmediately(CollectionDeckBoxVisual.DelOnAnimationFinished callback, object callbackData)
  {
    if (!this.m_isPoppedUp)
    {
      if (callback == null)
        return;
      callback(callbackData);
    }
    else
    {
      this.m_isPoppedUp = false;
      this.GetComponent<Animation>()["Deck_PopDown"].time = this.GetComponent<Animation>()["Deck_PopDown"].length;
      this.GetComponent<Animation>()["Deck_PopDown"].speed = 1f;
      this.PlayPopAnimation("Deck_PopDown", callback, callbackData);
    }
  }

  public void SetHighlightState(ActorStateType stateType)
  {
    if (!((Object) this.m_highlightState != (Object) null))
      return;
    if (!this.m_highlightState.IsReady())
      this.StartCoroutine(this.ChangeHighlightStateWhenReady(stateType));
    else
      this.m_highlightState.ChangeState(stateType);
  }

  [DebuggerHidden]
  private IEnumerator ChangeHighlightStateWhenReady(ActorStateType stateType)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionDeckBoxVisual.\u003CChangeHighlightStateWhenReady\u003Ec__Iterator2F()
    {
      stateType = stateType,
      \u003C\u0024\u003EstateType = stateType,
      \u003C\u003Ef__this = this
    };
  }

  public void ShowDeleteButton(bool show)
  {
    this.m_deleteButton.gameObject.SetActive(show);
  }

  public void SetOriginalButtonPosition()
  {
    if (this.m_isWild)
    {
      this.m_originalButtonPosition = this.m_portraitObject.transform.localPosition;
    }
    else
    {
      if (!((Object) this.m_wildPortraitObject != (Object) null))
        return;
      this.m_originalButtonPosition = this.m_wildPortraitObject.transform.localPosition;
    }
  }

  public void HideBanner()
  {
    this.m_classObject.SetActive(false);
    if ((Object) this.m_wildClassObject != (Object) null)
      this.m_wildClassObject.SetActive(false);
    if ((Object) this.m_topBannerRenderer != (Object) null)
      this.m_topBannerRenderer.gameObject.SetActive(false);
    if (!((Object) this.m_wildTopBannerRenderer != (Object) null))
      return;
    this.m_wildTopBannerRenderer.gameObject.SetActive(false);
  }

  public void ShowBanner()
  {
    this.m_classObject.SetActive(true);
    if ((Object) this.m_wildClassObject != (Object) null)
      this.m_wildClassObject.SetActive(true);
    if ((Object) this.m_topBannerRenderer != (Object) null)
      this.m_topBannerRenderer.gameObject.SetActive(true);
    if (!((Object) this.m_wildTopBannerRenderer != (Object) null))
      return;
    this.m_wildTopBannerRenderer.gameObject.SetActive(true);
  }

  public void AssignFromCollectionDeck(CollectionDeck deck, bool setTourneyValid)
  {
    if (deck == null)
      return;
    this.SetDeckName(deck.Name);
    this.SetDeckID(deck.ID);
    this.SetHeroCardID(deck.HeroCardID);
    this.SetShowGlow(deck.NeedsName || Options.Get().GetBool(Option.SHOW_INNKEEPER_DECK_DIALOGUE) && deck.IsInnkeeperDeck() && UserAttentionManager.CanShowAttentionGrabber("UserAttentionManager.CanShowAttentionGrabber:" + (object) Option.SHOW_INNKEEPER_DECK_DIALOGUE));
    this.SetFormat(CollectionManager.Get().IsShowingWildTheming(deck));
    if (!setTourneyValid)
      return;
    this.SetIsValid(deck.IsTourneyValid);
  }

  private void OnDeleteButtonRollout(UIEvent e)
  {
    this.ShowDeleteButton(false);
  }

  private void OnDeleteButtonOver(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("tiny_button_mouseover_1", this.gameObject);
  }

  private void OnDeleteButtonPressed(UIEvent e)
  {
    if (CollectionDeckTray.Get().IsShowingDeckContents())
      return;
    SoundManager.Get().LoadAndPlay("tiny_button_press_1", this.gameObject);
    AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
    info.m_headerText = GameStrings.Get("GLUE_COLLECTION_DELETE_CONFIRM_HEADER");
    info.m_showAlertIcon = false;
    if (AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
    {
      info.m_text = GameStrings.Get("GLUE_COLLECTION_DELETE_CONFIRM_DESC");
      info.m_alertTextAlignment = UberText.AlignmentOptions.Center;
      info.m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL;
      info.m_responseCallback = new AlertPopup.ResponseCallback(this.OnDeleteButtonConfirmationResponse);
    }
    else
    {
      info.m_text = GameStrings.Get("GLUE_COLLECTION_DELETE_UNAVAILABLE_DESC");
      info.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
      info.m_responseCallback = (AlertPopup.ResponseCallback) null;
    }
    DialogManager.Get().ShowPopup(info);
  }

  private void OnDeleteButtonConfirmationResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
      return;
    this.SetEnabled(false);
    CollectionDeckTray.Get().GetDecksContent().DeleteDeck(this.GetDeckID());
  }

  private void PlayValidAnimation()
  {
    if ((SceneMgr.Get().GetMode() == SceneMgr.Mode.TOURNAMENT || SceneMgr.Get().GetMode() == SceneMgr.Mode.FRIENDLY) && (this.m_isWild && (Object) this.m_wildPortraitObject != (Object) null))
    {
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        Animator component = this.m_wildPortraitObject.GetComponent<Animator>();
        if ((Object) component != (Object) null)
        {
          component.enabled = true;
          string stateName = !this.m_isValid ? "WildDeactivate" : "WildActivate";
          component.Play(stateName);
        }
      }
      else
      {
        Animator component = this.m_wildPortraitObject.GetComponent<Animator>();
        if ((Object) component != (Object) null)
        {
          component.enabled = true;
          string stateName = !this.m_isValid ? "Wild_RollupDeactivate" : "Wild_RolldownActivate";
          component.Play(stateName);
        }
      }
    }
    this.UpdateTextColors();
  }

  private void PlayPopAnimation(string animationName)
  {
    this.PlayPopAnimation(animationName, (CollectionDeckBoxVisual.DelOnAnimationFinished) null, (object) null);
  }

  private void PlayPopAnimation(string animationName, CollectionDeckBoxVisual.DelOnAnimationFinished callback, object callbackData)
  {
    this.GetComponent<Animation>().Play(animationName);
    CollectionDeckBoxVisual.OnPopAnimationFinishedCallbackData finishedCallbackData = new CollectionDeckBoxVisual.OnPopAnimationFinishedCallbackData()
    {
      m_callback = callback,
      m_callbackData = callbackData,
      m_animationName = animationName
    };
    this.StopCoroutine("WaitThenCallAnimationCallback");
    this.StartCoroutine("WaitThenCallAnimationCallback", (object) finishedCallbackData);
  }

  [DebuggerHidden]
  private IEnumerator WaitThenCallAnimationCallback(CollectionDeckBoxVisual.OnPopAnimationFinishedCallbackData callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionDeckBoxVisual.\u003CWaitThenCallAnimationCallback\u003Ec__Iterator30()
    {
      callbackData = callbackData,
      \u003C\u0024\u003EcallbackData = callbackData,
      \u003C\u003Ef__this = this
    };
  }

  private void ScaleDeckBox(bool scaleUp, CollectionDeckBoxVisual.DelOnAnimationFinished callback, object callbackData)
  {
    CollectionDeckBoxVisual.OnScaleFinishedCallbackData finishedCallbackData = new CollectionDeckBoxVisual.OnScaleFinishedCallbackData()
    {
      m_callback = callback,
      m_callbackData = callbackData
    };
    Hashtable args = iTween.Hash((object) "scale", (object) (!scaleUp ? CollectionDeckBoxVisual.SCALED_DOWN_LOCAL_SCALE : CollectionDeckBoxVisual.SCALED_UP_LOCAL_SCALE), (object) "isLocal", (object) true, (object) "time", (object) 0.2f, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "OnScaleComplete", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) finishedCallbackData, (object) "name", (object) "scale");
    iTween.StopByName(this.gameObject, "scale");
    iTween.ScaleTo(this.gameObject, args);
  }

  private void OnScaleComplete(CollectionDeckBoxVisual.OnScaleFinishedCallbackData callbackData)
  {
    if (callbackData.m_callback == null)
      return;
    callbackData.m_callback(callbackData.m_callbackData);
  }

  private void OnHeroFullDefLoaded(string cardID, FullDef def, object userData)
  {
    Log.Kyle.Print("OnHeroFullDefLoaded cardID: {0},  m_heroCardID: {1}", new object[2]
    {
      (object) cardID,
      (object) this.m_heroCardID
    });
    if (!cardID.Equals(this.m_heroCardID))
      return;
    this.m_fullDef = def;
    this.m_entityDef = def.GetEntityDef();
    this.m_cardDef = def.GetCardDef();
    this.SetPortrait(this.m_cardDef.GetCustomDeckPortrait());
    TAG_CLASS classTag = this.m_entityDef.GetClass();
    this.SetClassDisplay(classTag);
    this.SetBasicSetProgress(classTag);
  }

  private void UpdatePortraitMaterial(GameObject portraitObject, Material portraitMaterial, int portraitMaterialIndex)
  {
    if ((Object) portraitMaterial == (Object) null)
    {
      UnityEngine.Debug.LogError((object) "Custom Deck Portrait Material is null!");
    }
    else
    {
      RenderUtils.SetSharedMaterial(portraitObject, portraitMaterialIndex, portraitMaterial);
      if ((Object) this.m_cardDef == (Object) null || CollectionManager.Get().GetBestCardPremium(this.m_heroCardID) != TAG_PREMIUM.GOLDEN && this.m_entityDef.GetCardSet() != TAG_CARD_SET.HERO_SKINS)
        return;
      Material material = portraitObject.GetComponent<Renderer>().materials[portraitMaterialIndex];
      Texture texture = (Texture) null;
      if (material.HasProperty("_ShadowTex"))
        texture = material.GetTexture("_ShadowTex");
      RenderUtils.SetMaterial(portraitObject, portraitMaterialIndex, this.m_cardDef.GetPremiumPortraitMaterial());
      portraitObject.GetComponent<Renderer>().materials[portraitMaterialIndex].SetTexture("_ShadowTex", texture);
    }
  }

  private void SetPortrait(Material portraitMaterial)
  {
    this.UpdatePortraitMaterial(this.m_portraitObject, portraitMaterial, this.m_portraitMaterialIndex);
    if (!((Object) this.m_wildPortraitObject != (Object) null))
      return;
    this.UpdatePortraitMaterial(this.m_wildPortraitObject, portraitMaterial, this.m_wildPortraitMaterialIndex);
  }

  private void SetClassDisplay(TAG_CLASS classTag)
  {
    MeshRenderer component1 = this.m_classObject.GetComponent<MeshRenderer>();
    if ((Object) component1 != (Object) null)
    {
      this.m_classIconMaterial = component1.materials[this.m_classIconMaterialIndex];
      this.m_bannerMaterial = component1.materials[this.m_classBannerMaterialIndex];
      if ((Object) this.m_classIconMaterial == (Object) null || (Object) this.m_bannerMaterial == (Object) null)
        return;
      this.m_classIconMaterial.mainTextureOffset = CollectionPageManager.s_classTextureOffsets[classTag];
      this.m_bannerMaterial.color = CollectionPageManager.s_classColors[classTag];
      if ((Object) this.m_topBannerRenderer != (Object) null)
        this.m_topBannerRenderer.materials[this.m_topBannerMaterialIndex].color = CollectionPageManager.s_classColors[classTag];
    }
    if (!((Object) this.m_wildClassObject != (Object) null))
      return;
    MeshRenderer component2 = this.m_wildClassObject.GetComponent<MeshRenderer>();
    if (!((Object) component2 != (Object) null))
      return;
    this.m_classIconMaterial = component2.materials[this.m_wildClassIconMaterialIndex];
    this.m_bannerMaterial = component2.materials[this.m_wildClassBannerMaterialIndex];
    if ((Object) this.m_classIconMaterial == (Object) null || (Object) this.m_bannerMaterial == (Object) null)
      return;
    this.m_classIconMaterial.mainTextureOffset = CollectionPageManager.s_classTextureOffsets[classTag];
    this.m_bannerMaterial.color = CollectionPageManager.s_classColors[classTag];
    if (!((Object) this.m_wildTopBannerRenderer != (Object) null))
      return;
    this.m_wildTopBannerRenderer.materials[this.m_topBannerMaterialIndex].color = CollectionPageManager.s_classColors[classTag];
  }

  protected override void OnPress()
  {
    if (!this.m_animateButtonPress || this.m_isLocked)
      return;
    this.OnPressEvent();
  }

  protected override void OnRelease()
  {
    if (this.m_isLocked)
      return;
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL || UniversalInputManager.Get().IsTouchMode())
    {
      string path = !this.m_isWild ? this.m_standardDeckSelectSound : this.m_wildDeckSelectSound;
      if (!string.IsNullOrEmpty(path))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(path), this.gameObject);
    }
    this.OnReleaseEvent();
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    if ((Object) this.m_tooltipZone != (Object) null)
      this.m_tooltipZone.HideTooltip();
    this.OnOutEvent();
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    if ((Object) this.m_tooltipZone != (Object) null)
    {
      if (SceneMgr.Get().GetMode() == SceneMgr.Mode.COLLECTIONMANAGER && this.m_isLocked)
        this.m_tooltipZone.ShowTooltip(GameStrings.Get("GLUE_HERO_LOCKED_NAME"), GameStrings.Get("GLUE_TOOLTIP_HOW_TO_UNLOCK_DECKS"), 4.5f, true);
      else if (!this.m_isValid)
      {
        if (this.m_isMissingCards)
          this.m_tooltipZone.ShowTooltip(GameStrings.Get("GLUE_INCOMPLETE_DECK_HEADER"), GameStrings.Get("GLUE_INCOMPLETE_DECK_DESC"), 4f, (bool) UniversalInputManager.UsePhoneUI);
        else if (this.m_isWild && !Options.Get().GetBool(Option.IN_WILD_MODE))
          this.m_tooltipZone.ShowTooltip(GameStrings.Get("GLUE_DISABLED_WILD_DECK_HEADER"), GameStrings.Get("GLUE_DISABLED_WILD_DECK_DESC"), 4f, (bool) UniversalInputManager.UsePhoneUI);
      }
    }
    this.OnOverEvent();
  }

  public override void SetEnabled(bool enabled)
  {
    base.SetEnabled(enabled);
    if (enabled || !((Object) this.m_tooltipZone != (Object) null))
      return;
    this.m_tooltipZone.HideTooltip();
  }

  private void OnPressEvent()
  {
    iTween.MoveTo(!this.m_isWild || !((Object) this.m_wildPortraitObject != (Object) null) ? this.m_portraitObject : this.m_wildPortraitObject, iTween.Hash((object) "position", (object) this.m_pressedBone.transform.localPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.1, (object) "easeType", (object) iTween.EaseType.linear));
  }

  private void OnReleaseEvent()
  {
    iTween.MoveTo(!this.m_isWild || !((Object) this.m_wildPortraitObject != (Object) null) ? this.m_portraitObject : this.m_wildPortraitObject, iTween.Hash((object) "position", (object) this.m_originalButtonPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.1, (object) "easeType", (object) iTween.EaseType.linear));
  }

  private void OnOutEvent()
  {
    this.SetHighlightState(ActorStateType.HIGHLIGHT_OFF);
    iTween.MoveTo(!this.m_isWild || !((Object) this.m_wildPortraitObject != (Object) null) ? this.m_portraitObject : this.m_wildPortraitObject, iTween.Hash((object) "position", (object) this.m_originalButtonPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.1, (object) "easeType", (object) iTween.EaseType.linear));
  }

  private void OnOverEvent()
  {
    SoundManager.Get().LoadAndPlay("collection_manager_hero_mouse_over", this.gameObject);
    if (this.m_showGlow)
    {
      this.SetHighlightState(ActorStateType.HIGHLIGHT_PRIMARY_MOUSE_OVER);
      CollectionDeck collectionDeck = this.GetCollectionDeck();
      if (collectionDeck != null && collectionDeck.NeedsName)
      {
        Log.JMac.Print(string.Format("Sending deck changes for deck {0}, to clear the NEEDS_NAME flag.", (object) this.m_deckID));
        collectionDeck.SendChanges();
        collectionDeck.NeedsName = false;
      }
      this.m_showGlow = false;
    }
    else
      this.SetHighlightState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
  }

  private void ReparentElements(bool isWild)
  {
    Transform transform = !isWild ? this.m_portraitObject.transform : this.m_wildPortraitObject.transform;
    this.m_highlight.transform.parent = transform;
    this.m_deckName.gameObject.transform.parent = transform;
    this.m_setProgressLabel.gameObject.transform.parent = transform;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_classObject.transform.parent = transform;
    this.m_bones.m_gradientOneLine.parent = transform;
    this.m_bones.m_gradientTwoLine.parent = transform;
  }

  private void UpdateTextColors()
  {
    this.m_deckName.TextColor = Localization.GetLocale() != Locale.thTH ? (!this.m_isValid ? CollectionDeckBoxVisual.DECK_NAME_DISABLED_COLOR : CollectionDeckBoxVisual.DECK_NAME_ENABLED_COLOR) : (!this.m_isValid ? CollectionDeckBoxVisual.DECK_NAME_DISABLED_COLOR_THAI : CollectionDeckBoxVisual.DECK_NAME_ENABLED_COLOR);
    this.m_setProgressLabel.TextColor = !this.m_isValid ? CollectionDeckBoxVisual.BASIC_SET_PROGRESS_DISABLED_COLOR : CollectionDeckBoxVisual.BASIC_SET_PROGRESS_ENABLED_COLOR;
  }

  private class OnPopAnimationFinishedCallbackData
  {
    public string m_animationName;
    public CollectionDeckBoxVisual.DelOnAnimationFinished m_callback;
    public object m_callbackData;
  }

  private class OnScaleFinishedCallbackData
  {
    public CollectionDeckBoxVisual.DelOnAnimationFinished m_callback;
    public object m_callbackData;
  }

  public delegate void DelOnAnimationFinished(object callbackData);
}
