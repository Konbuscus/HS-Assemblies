﻿// Decompiled with JetBrains decompiler
// Type: KAR03_Chess
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR03_Chess : KAR_MissionEntity
{
  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BlackKing_Male_ChessPiece_ChessEmoteGreetings_01");
    this.PreloadSound("VO_BlackKing_Male_ChessPiece_ChessEmoteOops_01");
    this.PreloadSound("VO_BlackKing_Male_ChessPiece_ChessEmoteSorry_01");
    this.PreloadSound("VO_BlackKing_Male_ChessPiece_ChessEmoteThreaten_01");
    this.PreloadSound("VO_BlackKing_Male_ChessPiece_ChessEmoteWellPlayed_01");
    this.PreloadSound("VO_BlackKing_Male_ChessPiece_ChessEmoteWow_01");
    this.PreloadSound("VO_BlackKing_Male_ChessPiece_ChessEmoteThanks_01");
    this.PreloadSound("VO_Moroes_Male_Human_ChessTurn1_03");
    this.PreloadSound("VO_Moroes_Male_Human_ChessTurn3_01");
    this.PreloadSound("VO_Moroes_Male_Human_ChessTurn5_01");
    this.PreloadSound("VO_Moroes_Male_Human_ChessTurn7_01");
    this.PreloadSound("VO_Moroes_Male_Human_ChessTurn7_02");
    this.PreloadSound("VO_Moroes_Male_Human_Wing1Win_01");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>()
        {
          EmoteType.GREETINGS
        },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BlackKing_Male_ChessPiece_ChessEmoteGreetings_01",
            m_stringTag = "VO_BlackKing_Male_ChessPiece_ChessEmoteGreetings_01"
          }
        }
      },
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>()
        {
          EmoteType.OOPS
        },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BlackKing_Male_ChessPiece_ChessEmoteOops_01",
            m_stringTag = "VO_BlackKing_Male_ChessPiece_ChessEmoteOops_01"
          }
        }
      },
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>()
        {
          EmoteType.SORRY
        },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BlackKing_Male_ChessPiece_ChessEmoteSorry_01",
            m_stringTag = "VO_BlackKing_Male_ChessPiece_ChessEmoteSorry_01"
          }
        }
      },
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>()
        {
          EmoteType.THREATEN
        },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BlackKing_Male_ChessPiece_ChessEmoteThreaten_01",
            m_stringTag = "VO_BlackKing_Male_ChessPiece_ChessEmoteThreaten_01"
          }
        }
      },
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>()
        {
          EmoteType.WELL_PLAYED
        },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BlackKing_Male_ChessPiece_ChessEmoteWellPlayed_01",
            m_stringTag = "VO_BlackKing_Male_ChessPiece_ChessEmoteWellPlayed_01"
          }
        }
      },
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>() { EmoteType.WOW },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BlackKing_Male_ChessPiece_ChessEmoteWow_01",
            m_stringTag = "VO_BlackKing_Male_ChessPiece_ChessEmoteWow_01"
          }
        }
      },
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>()
        {
          EmoteType.THANKS
        },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BlackKing_Male_ChessPiece_ChessEmoteThanks_01",
            m_stringTag = "VO_BlackKing_Male_ChessPiece_ChessEmoteThanks_01"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR03_Chess.\u003CHandleMissionEventWithTiming\u003Ec__Iterator195() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR03_Chess.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator196() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR03_Chess.\u003CHandleGameOverWithTiming\u003Ec__Iterator197() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
