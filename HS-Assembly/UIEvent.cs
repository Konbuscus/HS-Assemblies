﻿// Decompiled with JetBrains decompiler
// Type: UIEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class UIEvent
{
  private UIEventType m_eventType;
  private PegUIElement m_element;

  public UIEvent(UIEventType eventType, PegUIElement element)
  {
    this.m_eventType = eventType;
    this.m_element = element;
  }

  public UIEventType GetEventType()
  {
    return this.m_eventType;
  }

  public PegUIElement GetElement()
  {
    return this.m_element;
  }

  public delegate void Handler(UIEvent e);
}
