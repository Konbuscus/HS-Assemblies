﻿// Decompiled with JetBrains decompiler
// Type: DeckCardBarSummonInForge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DeckCardBarSummonInForge : SpellImpl
{
  private static Color COMMON_COLOR = new Color(1f, 1f, 1f);
  private static Color COMMON_TINT_COLOR = new Color(0.9215686f, 0.945098f, 1f);
  private static Color RARE_COLOR = new Color(0.1647059f, 0.4078431f, 1f);
  private static Color RARE_TINT_COLOR = new Color(0.1647059f, 0.4078431f, 1f);
  private static Color EPIC_COLOR = new Color(0.4156863f, 0.1647059f, 1f);
  private static Color EPIC_TINT_COLOR = new Color(0.4156863f, 0.1647059f, 0.9921569f);
  private static Color LEGENDARY_COLOR = new Color(0.7686275f, 0.5411765f, 0.1490196f);
  private static Color LEGENDARY_TINT_COLOR = new Color(0.6666667f, 0.4745098f, 0.1294118f);
  public GameObject m_echoQuad;
  public Material m_echoQuadMaterial;
  public GameObject m_fxEvaporate;
  public Material m_fxEvaporateMaterial;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    this.StartCoroutine(this.BirthState());
  }

  [DebuggerHidden]
  private IEnumerator BirthState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckCardBarSummonInForge.\u003CBirthState\u003Ec__Iterator2B3() { \u003C\u003Ef__this = this };
  }
}
