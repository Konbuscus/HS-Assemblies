﻿// Decompiled with JetBrains decompiler
// Type: NotificationManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class NotificationManager : MonoBehaviour
{
  public static readonly float DEPTH = -5f;
  public static readonly Vector3 LEFT_OF_FRIENDLY_HERO = new Vector3(-1f, 0.0f, 1f);
  public static readonly Vector3 RIGHT_OF_FRIENDLY_HERO = new Vector3(-6f, 0.0f, 1f);
  public static readonly Vector3 LEFT_OF_ENEMY_HERO = new Vector3(-1f, 0.0f, -3.5f);
  public static readonly Vector3 RIGHT_OF_ENEMY_HERO = new Vector3(-6f, 0.0f, -3f);
  public static readonly Vector3 DEFAULT_CHARACTER_POS = new Vector3(100f, NotificationManager.DEPTH, 24.7f);
  public static readonly Vector3 ALT_ADVENTURE_SCREEN_POS = new Vector3(104.8f, NotificationManager.DEPTH, 131.1f);
  public static readonly Vector3 PHONE_CHARACTER_POS = new Vector3(124.1f, NotificationManager.DEPTH, 24.7f);
  public static readonly float PHONE_OVERLAY_UI_CHARACTER_X_OFFSET = -0.5f;
  private Vector3 NOTIFICATION_SCALE = 0.163f * Vector3.one;
  private Vector3 NOTIFICATION_SCALE_PHONE = 0.326f * Vector3.one;
  public const string TIRION_PREFAB_NAME = "Tirion_Quote";
  public const string KT_PREFAB_NAME = "KT_Quote";
  public const string NORMAL_NEFARIAN_PREFAB_NAME = "NormalNefarian_Quote";
  public const string ZOMBIE_NEFARIAN_PREFAB_NAME = "NefarianDragon_Quote";
  public const string RAGNAROS_PREFAB_NAME = "Ragnaros_Quote";
  private const float DEFAULT_QUOTE_DURATION = 8f;
  public GameObject speechBubblePrefab;
  public GameObject speechIndicatorPrefab;
  public GameObject bounceArrowPrefab;
  public GameObject fadeArrowPrefab;
  public GameObject popupTextPrefab;
  public GameObject dialogBoxPrefab;
  public GameObject innkeeperQuotePrefab;
  private static NotificationManager s_instance;
  private List<Notification> notificationsToDestroyUponNewNotifier;
  private List<Notification> arrows;
  private List<Notification> popUpTexts;
  private Notification popUpDialog;
  private Notification m_quote;
  private List<string> m_quotesThisSession;

  public static Vector3 NOTIFICATITON_WORLD_SCALE
  {
    get
    {
      if ((bool) UniversalInputManager.UsePhoneUI)
        return 25f * Vector3.one;
      return 18f * Vector3.one;
    }
  }

  public bool IsQuotePlaying
  {
    get
    {
      return (UnityEngine.Object) this.m_quote != (UnityEngine.Object) null;
    }
  }

  private void Awake()
  {
    NotificationManager.s_instance = this;
    this.m_quotesThisSession = new List<string>();
  }

  private void OnDestroy()
  {
    NotificationManager.s_instance = (NotificationManager) null;
  }

  private void Start()
  {
    this.notificationsToDestroyUponNewNotifier = new List<Notification>();
    this.arrows = new List<Notification>();
    this.popUpTexts = new List<Notification>();
  }

  public static NotificationManager Get()
  {
    return NotificationManager.s_instance;
  }

  public Notification CreatePopupDialog(string headlineText, string bodyText, string yesOrOKButtonText, string noButtonText)
  {
    return this.CreatePopupDialog(headlineText, bodyText, yesOrOKButtonText, noButtonText, new Vector3(0.0f, 0.0f, 0.0f));
  }

  public Notification CreatePopupDialog(string headlineText, string bodyText, string yesOrOKButtonText, string noButtonText, Vector3 offset)
  {
    if ((UnityEngine.Object) this.popUpDialog != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.popUpDialog.gameObject);
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.dialogBoxPrefab);
    Vector3 position = Camera.main.transform.position;
    gameObject.transform.position = position + new Vector3(-0.07040818f, -16.10709f, 1.79612f) + offset;
    this.popUpDialog = gameObject.GetComponent<Notification>();
    this.popUpDialog.ChangeDialogText(headlineText, bodyText, yesOrOKButtonText, noButtonText);
    this.popUpDialog.PlayBirth();
    UniversalInputManager.Get().SetGameDialogActive(true);
    return this.popUpDialog;
  }

  public Notification CreateSpeechBubble(string speechText, Actor actor)
  {
    return this.CreateSpeechBubble(speechText, Notification.SpeechBubbleDirection.BottomLeft, actor, false, true, 0.0f);
  }

  public Notification CreateSpeechBubble(string speechText, Actor actor, bool bDestroyWhenNewCreated)
  {
    return this.CreateSpeechBubble(speechText, Notification.SpeechBubbleDirection.BottomLeft, actor, bDestroyWhenNewCreated, true, 0.0f);
  }

  public Notification CreateSpeechBubble(string speechText, Notification.SpeechBubbleDirection direction, Actor actor)
  {
    return this.CreateSpeechBubble(speechText, direction, actor, false, true, 0.0f);
  }

  public Notification CreateSpeechBubble(string speechText, Notification.SpeechBubbleDirection direction, Actor actor, bool bDestroyWhenNewCreated, bool parentToActor = true, float bubbleScale = 0.0f)
  {
    this.DestroyOtherNotifications(direction);
    Notification component;
    if (speechText == string.Empty)
    {
      component = UnityEngine.Object.Instantiate<GameObject>(this.speechIndicatorPrefab).GetComponent<Notification>();
      component.PlaySmallBirthForFakeBubble();
      component.SetPositionForSmallBubble(actor);
    }
    else
    {
      component = UnityEngine.Object.Instantiate<GameObject>(this.speechBubblePrefab).GetComponent<Notification>();
      component.ChangeText(speechText);
      component.FaceDirection(direction);
      component.PlayBirth();
      component.SetPosition(actor, direction);
      if (!Mathf.Approximately(bubbleScale, 0.0f))
      {
        GameObject gameObject = new GameObject();
        gameObject.transform.SetParent(actor.transform);
        TransformUtil.Identity(gameObject);
        component.SetParentOffsetObject(gameObject);
        gameObject.transform.localScale = new Vector3(bubbleScale, bubbleScale, bubbleScale);
      }
    }
    if (bDestroyWhenNewCreated)
      this.notificationsToDestroyUponNewNotifier.Add(component);
    if (parentToActor)
      component.transform.parent = actor.transform;
    return component;
  }

  public Notification CreateBouncingArrow(UserAttentionBlocker blocker, bool addToList)
  {
    if (!SceneMgr.Get().IsInGame() && !UserAttentionManager.CanShowAttentionGrabber(blocker, "NotificationManger.CreateBouncingArrow"))
      return (Notification) null;
    Notification component = UnityEngine.Object.Instantiate<GameObject>(this.bounceArrowPrefab).GetComponent<Notification>();
    component.PlayBirth();
    if (addToList)
      this.arrows.Add(component);
    return component;
  }

  public Notification CreateBouncingArrow(UserAttentionBlocker blocker, Vector3 position, Vector3 rotation)
  {
    return this.CreateBouncingArrow(blocker, position, rotation, true);
  }

  public Notification CreateBouncingArrow(UserAttentionBlocker blocker, Vector3 position, Vector3 rotation, bool addToList)
  {
    if (!SceneMgr.Get().IsInGame() && !UserAttentionManager.CanShowAttentionGrabber(blocker, "NotificationManger.CreateBouncingArrow"))
      return (Notification) null;
    Notification bouncingArrow = this.CreateBouncingArrow(blocker, addToList);
    bouncingArrow.transform.position = position;
    bouncingArrow.transform.localEulerAngles = rotation;
    return bouncingArrow;
  }

  public Notification CreateFadeArrow(bool addToList)
  {
    Notification component = UnityEngine.Object.Instantiate<GameObject>(this.fadeArrowPrefab).GetComponent<Notification>();
    component.PlayBirth();
    if (addToList)
      this.arrows.Add(component);
    return component;
  }

  public Notification CreateFadeArrow(Vector3 position, Vector3 rotation)
  {
    return this.CreateFadeArrow(position, rotation, true);
  }

  public Notification CreateFadeArrow(Vector3 position, Vector3 rotation, bool addToList)
  {
    Notification fadeArrow = this.CreateFadeArrow(addToList);
    fadeArrow.transform.position = position;
    fadeArrow.transform.localEulerAngles = rotation;
    return fadeArrow;
  }

  public Notification CreatePopupText(UserAttentionBlocker blocker, Transform bone, string text, bool convertLegacyPosition = true)
  {
    if (convertLegacyPosition)
      return this.CreatePopupText(blocker, bone.position, bone.localScale, text, convertLegacyPosition);
    return this.CreatePopupText(blocker, bone.localPosition, bone.localScale, text, convertLegacyPosition);
  }

  public Notification CreatePopupText(UserAttentionBlocker blocker, Vector3 position, Vector3 scale, string text, bool convertLegacyPosition = true)
  {
    if (!SceneMgr.Get().IsInGame() && !UserAttentionManager.CanShowAttentionGrabber(blocker, "NotificationManager.CreatePopupText"))
      return (Notification) null;
    Vector3 vector3 = position;
    if (convertLegacyPosition)
    {
      Camera camera = SceneMgr.Get().GetMode() != SceneMgr.Mode.GAMEPLAY ? Box.Get().GetBoxCamera().GetComponent<Camera>() : BoardCameras.Get().GetComponentInChildren<Camera>();
      vector3 = OverlayUI.Get().GetRelativePosition(position, camera, OverlayUI.Get().m_heightScale.m_Center, 0.0f);
    }
    GameObject go = UnityEngine.Object.Instantiate<GameObject>(this.popupTextPrefab);
    SceneUtils.SetLayer(go, GameLayer.UI);
    go.transform.localPosition = vector3;
    go.transform.localScale = scale;
    OverlayUI.Get().AddGameObject(go, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    Notification component = go.GetComponent<Notification>();
    component.ChangeText(text);
    component.PlayBirth();
    this.popUpTexts.Add(component);
    return component;
  }

  public Notification CreateInnkeeperQuote(UserAttentionBlocker blocker, string text, string soundName, float durationSeconds = 0.0f, Action finishCallback = null, bool clickToDismiss = false)
  {
    return this.CreateInnkeeperQuote(blocker, NotificationManager.DEFAULT_CHARACTER_POS, text, soundName, durationSeconds, finishCallback, clickToDismiss);
  }

  public Notification CreateInnkeeperQuote(UserAttentionBlocker blocker, string text, string soundName, Action finishCallback, bool clickToDismiss = false)
  {
    return this.CreateInnkeeperQuote(blocker, NotificationManager.DEFAULT_CHARACTER_POS, text, soundName, 0.0f, finishCallback, clickToDismiss);
  }

  public Notification CreateInnkeeperQuote(UserAttentionBlocker blocker, Vector3 position, string text, string soundName, float durationSeconds = 0.0f, Action finishCallback = null, bool clickToDismiss = false)
  {
    if (!SceneMgr.Get().IsInGame() && !UserAttentionManager.CanShowAttentionGrabber(blocker, "NotificationManager.CreateInnkeeperQuote"))
    {
      if (finishCallback != null)
        finishCallback();
      return (Notification) null;
    }
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.innkeeperQuotePrefab);
    gameObject.GetComponentInChildren<BoxCollider>().enabled = clickToDismiss;
    Notification component = gameObject.GetComponent<Notification>();
    component.ignoreAudioOnDestroy = clickToDismiss;
    if (finishCallback != null)
      component.OnFinishDeathState += finishCallback;
    this.PlayCharacterQuote(component, position, text, soundName, durationSeconds, CanvasAnchor.BOTTOM_LEFT);
    return component;
  }

  public Notification CreateTirionQuote(string stringTag, string soundName, bool allowRepeatDuringSession = true)
  {
    return this.CreateTirionQuote(NotificationManager.DEFAULT_CHARACTER_POS, stringTag, soundName, allowRepeatDuringSession);
  }

  public Notification CreateTirionQuote(Vector3 position, string stringTag, string soundName, bool allowRepeatDuringSession = true)
  {
    return this.CreateCharacterQuote("Tirion_Quote", position, GameStrings.Get(stringTag), soundName, allowRepeatDuringSession, 0.0f, (Action) null, CanvasAnchor.BOTTOM_LEFT);
  }

  public Notification CreateKTQuote(string stringTag, string soundName, bool allowRepeatDuringSession = true)
  {
    return this.CreateKTQuote(NotificationManager.DEFAULT_CHARACTER_POS, stringTag, soundName, allowRepeatDuringSession);
  }

  public Notification CreateKTQuote(Vector3 position, string stringTag, string soundName, bool allowRepeatDuringSession = true)
  {
    return this.CreateCharacterQuote("KT_Quote", position, GameStrings.Get(stringTag), soundName, allowRepeatDuringSession, 0.0f, (Action) null, CanvasAnchor.BOTTOM_LEFT);
  }

  public Notification CreateZombieNefarianQuote(Vector3 position, string stringTag, string soundName, bool allowRepeatDuringSession)
  {
    return this.CreateCharacterQuote("NefarianDragon_Quote", position, GameStrings.Get(stringTag), soundName, allowRepeatDuringSession, 0.0f, (Action) null, CanvasAnchor.BOTTOM_LEFT);
  }

  public Notification CreateCharacterQuote(string prefabName, string text, string soundName, bool allowRepeatDuringSession = true, float durationSeconds = 0.0f, CanvasAnchor anchorPoint = CanvasAnchor.BOTTOM_LEFT)
  {
    return this.CreateCharacterQuote(prefabName, NotificationManager.DEFAULT_CHARACTER_POS, text, soundName, allowRepeatDuringSession, durationSeconds, (Action) null, anchorPoint);
  }

  public Notification CreateCharacterQuote(string prefabName, Vector3 position, string text, string soundName, bool allowRepeatDuringSession = true, float durationSeconds = 0.0f, Action finishCallback = null, CanvasAnchor anchorPoint = CanvasAnchor.BOTTOM_LEFT)
  {
    if (!allowRepeatDuringSession && this.m_quotesThisSession.Contains(soundName))
      return (Notification) null;
    this.m_quotesThisSession.Add(soundName);
    Notification quote = GameUtils.LoadGameObjectWithComponent<Notification>(prefabName);
    if ((UnityEngine.Object) quote == (UnityEngine.Object) null)
      return (Notification) null;
    if (finishCallback != null)
      quote.OnFinishDeathState += finishCallback;
    this.PlayCharacterQuote(quote, position, text, soundName, durationSeconds, anchorPoint);
    return quote;
  }

  public Notification CreateBigCharacterQuote(string prefabName, string soundName, bool allowRepeatDuringSession = true, float durationSeconds = 0.0f, Action finishCallback = null, bool useOverlayUI = false)
  {
    return this.CreateBigCharacterQuote(prefabName, NotificationManager.DEFAULT_CHARACTER_POS, soundName, soundName, allowRepeatDuringSession, durationSeconds, finishCallback, useOverlayUI, Notification.SpeechBubbleDirection.None, false);
  }

  public Notification CreateBigCharacterQuote(string prefabName, string soundName, bool allowRepeatDuringSession = true, float durationSeconds = 0.0f, Action finishCallback = null, bool useOverlayUI = false, Notification.SpeechBubbleDirection bubbleDir = Notification.SpeechBubbleDirection.None, bool persistCharacter = false)
  {
    return this.CreateBigCharacterQuote(prefabName, NotificationManager.DEFAULT_CHARACTER_POS, soundName, soundName, allowRepeatDuringSession, durationSeconds, finishCallback, useOverlayUI, bubbleDir, persistCharacter);
  }

  public Notification CreateBigCharacterQuote(string prefabName, Vector3 position, string soundName, string textID, bool allowRepeatDuringSession = true, float durationSeconds = 0.0f, Action finishCallback = null, bool useOverlayUI = false, Notification.SpeechBubbleDirection bubbleDir = Notification.SpeechBubbleDirection.None, bool persistCharacter = false)
  {
    if (!allowRepeatDuringSession && this.m_quotesThisSession.Contains(textID))
      return (Notification) null;
    this.m_quotesThisSession.Add(textID);
    bool animateSpeechBubble = false;
    Notification quote;
    if (prefabName != null && (UnityEngine.Object) this.m_quote != (UnityEngine.Object) null && this.m_quote.gameObject.name.Equals(prefabName + "(Clone)"))
    {
      quote = this.m_quote;
      animateSpeechBubble = true;
    }
    else
      quote = GameUtils.LoadGameObjectWithComponent<Notification>(prefabName);
    if ((UnityEngine.Object) quote == (UnityEngine.Object) null)
      return (Notification) null;
    if (bubbleDir != Notification.SpeechBubbleDirection.None)
      quote.RepositionSpeechBubbleAroundBigQuote(bubbleDir, animateSpeechBubble);
    if (finishCallback != null)
      quote.OnFinishDeathState += finishCallback;
    this.PlayBigCharacterQuote(quote, GameStrings.Get(textID), soundName, durationSeconds, position, useOverlayUI, persistCharacter);
    return quote;
  }

  public void ForceAddSoundToPlayedList(string soundName)
  {
    this.m_quotesThisSession.Add(soundName);
  }

  public void ForceRemoveSoundFromPlayedList(string soundName)
  {
    this.m_quotesThisSession.Remove(soundName);
  }

  public bool HasSoundPlayedThisSession(string soundName)
  {
    return this.m_quotesThisSession.Contains(soundName);
  }

  private void PlayBigCharacterQuote(Notification quote, string text, string soundName, float durationSeconds, Vector3 position, bool useOverlayUI = false, bool persistCharacter = false)
  {
    bool flag = true;
    if ((bool) ((UnityEngine.Object) this.m_quote))
    {
      if ((UnityEngine.Object) this.m_quote == (UnityEngine.Object) quote)
        flag = false;
      else
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_quote.gameObject);
    }
    this.m_quote = quote;
    this.m_quote.ChangeText(text);
    if (useOverlayUI)
      TransformUtil.AttachAndPreserveLocalTransform(this.m_quote.transform, OverlayUI.Get().FindBone("OffScreenSpeaker1"));
    else
      TransformUtil.AttachAndPreserveLocalTransform(this.m_quote.transform, Board.Get().FindBone("OffScreenSpeaker1"));
    Vector3 vector3 = Vector3.zero;
    if (position != NotificationManager.DEFAULT_CHARACTER_POS)
      vector3 = position;
    if (useOverlayUI && (bool) UniversalInputManager.UsePhoneUI)
      vector3.x += NotificationManager.PHONE_OVERLAY_UI_CHARACTER_X_OFFSET;
    this.m_quote.transform.localPosition = vector3;
    this.m_quote.transform.localEulerAngles = Vector3.zero;
    if (flag)
      this.m_quote.transform.localScale = Vector3.one * 0.01f;
    if (!string.IsNullOrEmpty(soundName))
    {
      AssetLoader.Get().LoadSound(soundName, new AssetLoader.GameObjectCallback(this.OnBigQuoteSoundLoaded), (object) new NotificationManager.QuoteSoundCallbackData()
      {
        m_quote = this.m_quote,
        m_durationSeconds = durationSeconds,
        m_persistCharacter = persistCharacter
      }, false, SoundManager.Get().GetPlaceholderSound());
    }
    else
    {
      this.m_quote.PlayBirthWithForcedScale(Vector3.one);
      if ((double) durationSeconds <= 0.0)
        return;
      if (persistCharacter)
        this.DestroySpeechBubble(this.m_quote, durationSeconds);
      else
        this.DestroyNotification(this.m_quote, durationSeconds);
    }
  }

  private void PlayCharacterQuote(Notification quote, Vector3 position, string text, string soundName, float durationSeconds, CanvasAnchor anchorPoint = CanvasAnchor.BOTTOM_LEFT)
  {
    if ((bool) ((UnityEngine.Object) this.m_quote))
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_quote.gameObject);
    this.m_quote = quote;
    this.m_quote.ChangeText(text);
    this.m_quote.transform.position = position;
    this.m_quote.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    OverlayUI.Get().AddGameObject(this.m_quote.gameObject, anchorPoint, false, CanvasScaleMode.HEIGHT);
    if (!string.IsNullOrEmpty(soundName))
      AssetLoader.Get().LoadSound(soundName, new AssetLoader.GameObjectCallback(this.OnQuoteSoundLoaded), (object) new NotificationManager.QuoteSoundCallbackData()
      {
        m_quote = this.m_quote,
        m_durationSeconds = durationSeconds
      }, false, SoundManager.Get().GetPlaceholderSound());
    else
      this.PlayQuoteWithoutSound(durationSeconds);
  }

  private void PlayQuoteWithoutSound(float durationSeconds)
  {
    this.m_quote.PlayBirthWithForcedScale(!(bool) UniversalInputManager.UsePhoneUI ? this.NOTIFICATION_SCALE : this.NOTIFICATION_SCALE_PHONE);
    if ((double) durationSeconds <= 0.0)
      return;
    this.DestroyNotification(this.m_quote, durationSeconds);
  }

  private void OnQuoteSoundLoaded(string name, GameObject go, object userData)
  {
    NotificationManager.QuoteSoundCallbackData soundCallbackData = (NotificationManager.QuoteSoundCallbackData) userData;
    if (!(bool) ((UnityEngine.Object) soundCallbackData.m_quote))
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else if (!(bool) ((UnityEngine.Object) go))
    {
      UnityEngine.Debug.LogWarning((object) "Quote Sound failed to load!");
      this.PlayQuoteWithoutSound((double) soundCallbackData.m_durationSeconds <= 0.0 ? 8f : soundCallbackData.m_durationSeconds);
    }
    else
    {
      AudioSource component = go.GetComponent<AudioSource>();
      this.m_quote.AssignAudio(component);
      SoundManager.Get().PlayPreloaded(component);
      this.m_quote.PlayBirthWithForcedScale(!(bool) UniversalInputManager.UsePhoneUI ? this.NOTIFICATION_SCALE : this.NOTIFICATION_SCALE_PHONE);
      this.DestroyNotification(this.m_quote, Mathf.Max(soundCallbackData.m_durationSeconds, component.clip.length));
      if (!((UnityEngine.Object) this.m_quote.clickOff != (UnityEngine.Object) null))
        return;
      this.m_quote.clickOff.SetData((object) this.m_quote);
      this.m_quote.clickOff.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.ClickNotification));
    }
  }

  private void OnBigQuoteSoundLoaded(string name, GameObject go, object userData)
  {
    NotificationManager.QuoteSoundCallbackData soundCallbackData = (NotificationManager.QuoteSoundCallbackData) userData;
    if (!(bool) ((UnityEngine.Object) soundCallbackData.m_quote))
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else if (!(bool) ((UnityEngine.Object) go))
    {
      UnityEngine.Debug.LogWarning((object) "Quote Sound failed to load!");
      this.PlayQuoteWithoutSound((double) soundCallbackData.m_durationSeconds <= 0.0 ? 8f : soundCallbackData.m_durationSeconds);
    }
    else
    {
      AudioSource component = go.GetComponent<AudioSource>();
      this.m_quote.AssignAudio(component);
      SoundManager.Get().PlayPreloaded(component);
      this.m_quote.PlayBirthWithForcedScale(Vector3.one);
      if ((double) soundCallbackData.m_durationSeconds < (double) component.clip.length)
        Log.Sound.PrintError("Big Quote Sound {0} is playing with requested length {1:0.0} but actual length is {2:0.0}. Sound will get cut off! This can happen if the sound is not preloaded: did you forget to call PreloadSound() for that sound in the appropriate script?", (object) name, (object) soundCallbackData.m_durationSeconds, (object) component.clip.length);
      float delaySeconds = Mathf.Max(soundCallbackData.m_durationSeconds, component.clip.length);
      Log.Notifications.Print("Destroying notification or speech bubble after {0} seconds. durationSeconds: {1} source.clip.length: {2} persistCharacter? {3}", (object) delaySeconds, (object) soundCallbackData.m_durationSeconds, (object) component.clip.length, (object) soundCallbackData.m_persistCharacter);
      if (soundCallbackData.m_persistCharacter)
        this.DestroySpeechBubble(this.m_quote, delaySeconds);
      else
        this.DestroyNotification(this.m_quote, delaySeconds);
      if (!((UnityEngine.Object) this.m_quote.clickOff != (UnityEngine.Object) null))
        return;
      this.m_quote.clickOff.SetData((object) this.m_quote);
      this.m_quote.clickOff.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.ClickNotification));
    }
  }

  public void DestroyAllArrows()
  {
    if (this.arrows.Count == 0)
      return;
    for (int index = 0; index < this.arrows.Count; ++index)
    {
      if ((UnityEngine.Object) this.arrows[index] != (UnityEngine.Object) null)
        this.NukeNotificationWithoutPlayingAnim(this.arrows[index]);
    }
  }

  public void DestroyAllPopUps()
  {
    if (this.popUpTexts.Count == 0)
      return;
    for (int index = 0; index < this.popUpTexts.Count; ++index)
    {
      if (!((UnityEngine.Object) this.popUpTexts[index] == (UnityEngine.Object) null))
        this.NukeNotification(this.popUpTexts[index]);
    }
    this.popUpTexts = new List<Notification>();
  }

  private void DestroyOtherNotifications(Notification.SpeechBubbleDirection direction)
  {
    if (this.notificationsToDestroyUponNewNotifier.Count == 0)
      return;
    for (int index = 0; index < this.notificationsToDestroyUponNewNotifier.Count; ++index)
    {
      if (!((UnityEngine.Object) this.notificationsToDestroyUponNewNotifier[index] == (UnityEngine.Object) null) && this.notificationsToDestroyUponNewNotifier[index].GetSpeechBubbleDirection() == direction)
        this.NukeNotificationWithoutPlayingAnim(this.notificationsToDestroyUponNewNotifier[index]);
    }
  }

  public void DestroyNotification(Notification notification, float delaySeconds)
  {
    if ((UnityEngine.Object) notification == (UnityEngine.Object) null)
      return;
    if ((double) delaySeconds == 0.0)
      this.NukeNotification(notification);
    else
      this.StartCoroutine(this.WaitAndThenDestroyNotification(notification, delaySeconds));
  }

  public void DestroySpeechBubble(Notification notification, float delaySeconds)
  {
    if ((UnityEngine.Object) notification == (UnityEngine.Object) null)
      return;
    if ((double) delaySeconds == 0.0)
      this.NukeSpeechBubble(notification);
    else
      this.StartCoroutine(this.WaitAndThenDestroySpeechBubble(notification, delaySeconds));
  }

  public void DestroyNotificationWithText(string text, float delaySeconds = 0.0f)
  {
    Notification notification = (Notification) null;
    for (int index = 0; index < this.popUpTexts.Count; ++index)
    {
      if (!((UnityEngine.Object) this.popUpTexts[index] == (UnityEngine.Object) null) && this.popUpTexts[index].speechUberText.Text == text)
        notification = this.popUpTexts[index];
    }
    this.DestroyNotification(notification, delaySeconds);
  }

  private void ClickNotification(UIEvent e)
  {
    Notification data = (Notification) e.GetElement().GetData();
    this.NukeNotification(data);
    data.clickOff.RemoveEventListener(UIEventType.PRESS, new UIEvent.Handler(this.ClickNotification));
  }

  public void DestroyAllNotificationsNowWithNoAnim()
  {
    if ((bool) ((UnityEngine.Object) this.popUpDialog))
      this.NukeNotificationWithoutPlayingAnim(this.popUpDialog);
    if ((bool) ((UnityEngine.Object) this.m_quote))
      this.NukeNotificationWithoutPlayingAnim(this.m_quote);
    for (int index = 0; index < this.notificationsToDestroyUponNewNotifier.Count; ++index)
    {
      Notification notification = this.notificationsToDestroyUponNewNotifier[index];
      if (!((UnityEngine.Object) notification == (UnityEngine.Object) null))
        this.NukeNotificationWithoutPlayingAnim(notification);
    }
    this.DestroyAllArrows();
    this.DestroyAllPopUps();
  }

  public void DestroyActiveQuote(float delaySeconds, bool ignoreAudio = false)
  {
    if ((UnityEngine.Object) this.m_quote == (UnityEngine.Object) null)
      return;
    if (ignoreAudio)
      this.m_quote.ignoreAudioOnDestroy = true;
    if ((double) delaySeconds == 0.0)
      this.NukeNotification(this.m_quote);
    else
      this.StartCoroutine(this.WaitAndThenDestroyNotification(this.m_quote, delaySeconds));
  }

  public void DestroyNotificationNowWithNoAnim(Notification notification)
  {
    if ((UnityEngine.Object) notification == (UnityEngine.Object) null)
      return;
    this.NukeNotificationWithoutPlayingAnim(notification);
  }

  [DebuggerHidden]
  private IEnumerator WaitAndThenDestroyNotification(Notification notification, float amountSeconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NotificationManager.\u003CWaitAndThenDestroyNotification\u003Ec__Iterator311() { amountSeconds = amountSeconds, notification = notification, \u003C\u0024\u003EamountSeconds = amountSeconds, \u003C\u0024\u003Enotification = notification, \u003C\u003Ef__this = this };
  }

  private void NukeNotification(Notification notification)
  {
    if ((UnityEngine.Object) notification == (UnityEngine.Object) null)
    {
      Log.All.PrintWarning("Attempting to Nuke a Notification that does not exist!");
    }
    else
    {
      if (this.notificationsToDestroyUponNewNotifier.Contains(notification))
        this.notificationsToDestroyUponNewNotifier.Remove(notification);
      if (notification.IsDying())
        return;
      notification.PlayDeath();
      UniversalInputManager.Get().SetGameDialogActive(false);
    }
  }

  private void NukeNotificationWithoutPlayingAnim(Notification notification)
  {
    if (this.notificationsToDestroyUponNewNotifier.Contains(notification))
      this.notificationsToDestroyUponNewNotifier.Remove(notification);
    UnityEngine.Object.Destroy((UnityEngine.Object) notification.gameObject);
    UniversalInputManager.Get().SetGameDialogActive(false);
  }

  [DebuggerHidden]
  private IEnumerator WaitAndThenDestroySpeechBubble(Notification notification, float amountSeconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NotificationManager.\u003CWaitAndThenDestroySpeechBubble\u003Ec__Iterator312() { amountSeconds = amountSeconds, notification = notification, \u003C\u0024\u003EamountSeconds = amountSeconds, \u003C\u0024\u003Enotification = notification, \u003C\u003Ef__this = this };
  }

  private void NukeSpeechBubble(Notification notification)
  {
    if ((UnityEngine.Object) notification == (UnityEngine.Object) null)
    {
      Log.All.PrintWarning("Attempting to Nuke a Speech Bubble for a Notification that does not exist!");
    }
    else
    {
      if (notification.IsDying())
        return;
      notification.PlaySpeechBubbleDeath();
    }
  }

  public TutorialNotification CreateTutorialDialog(string headlineGameString, string bodyTextGameString, string buttonGameString, UIEvent.Handler buttonHandler, Vector2 materialOffset, bool swapMaterial = false)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    NotificationManager.\u003CCreateTutorialDialog\u003Ec__AnonStorey44B dialogCAnonStorey44B = new NotificationManager.\u003CCreateTutorialDialog\u003Ec__AnonStorey44B();
    // ISSUE: reference to a compiler-generated field
    dialogCAnonStorey44B.buttonHandler = buttonHandler;
    // ISSUE: reference to a compiler-generated field
    dialogCAnonStorey44B.\u003C\u003Ef__this = this;
    GameObject gameObject = AssetLoader.Get().LoadActor("TutorialIntroDialog", true, false);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Unable to load tutorial dialog TutorialIntroDialog prefab.");
      return (TutorialNotification) null;
    }
    // ISSUE: reference to a compiler-generated field
    dialogCAnonStorey44B.notification = gameObject.GetComponent<TutorialNotification>();
    // ISSUE: reference to a compiler-generated field
    if ((UnityEngine.Object) dialogCAnonStorey44B.notification == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "TutorialNotification component does not exist on TutorialIntroDialog prefab.");
      return (TutorialNotification) null;
    }
    TransformUtil.AttachAndPreserveLocalTransform(gameObject.transform, OverlayUI.Get().m_heightScale.m_Center);
    if ((bool) UniversalInputManager.UsePhoneUI)
      gameObject.transform.localScale = 1.5f * gameObject.transform.localScale;
    // ISSUE: reference to a compiler-generated field
    this.popUpDialog = (Notification) dialogCAnonStorey44B.notification;
    // ISSUE: reference to a compiler-generated field
    dialogCAnonStorey44B.notification.headlineUberText.Text = GameStrings.Get(headlineGameString);
    // ISSUE: reference to a compiler-generated field
    dialogCAnonStorey44B.notification.speechUberText.Text = GameStrings.Get(bodyTextGameString);
    // ISSUE: reference to a compiler-generated field
    dialogCAnonStorey44B.notification.m_ButtonStart.SetText(GameStrings.Get(buttonGameString));
    if (swapMaterial)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      dialogCAnonStorey44B.notification.artOverlay.material = dialogCAnonStorey44B.notification.swapMaterial;
    }
    // ISSUE: reference to a compiler-generated field
    dialogCAnonStorey44B.notification.artOverlay.material.mainTextureOffset = materialOffset;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    dialogCAnonStorey44B.notification.m_ButtonStart.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(dialogCAnonStorey44B.\u003C\u003Em__301));
    this.popUpDialog.PlayBirth();
    UniversalInputManager.Get().SetGameDialogActive(true);
    // ISSUE: reference to a compiler-generated field
    return dialogCAnonStorey44B.notification;
  }

  private class QuoteSoundCallbackData
  {
    public Notification m_quote;
    public float m_durationSeconds;
    public bool m_persistCharacter;
  }
}
