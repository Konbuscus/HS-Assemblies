﻿// Decompiled with JetBrains decompiler
// Type: LogArchive
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using UnityEngine;

internal class LogArchive
{
  private int m_maxFileSizeKB = 5000;
  private static LogArchive s_instance;
  private string m_logPath;
  private ulong m_numLinesWritten;
  private bool m_stopLogging;

  public LogArchive()
  {
    string str = FileUtils.PersistentDataPath + "/Logs";
    this.MakeLogPath(str);
    try
    {
      Directory.CreateDirectory(str);
      this.CleanOldLogs(str);
      Application.logMessageReceived += new Application.LogCallback(this.HandleLog);
      Log.Cameron.Print("Logging Unity output to: " + this.m_logPath);
    }
    catch (IOException ex)
    {
      Log.All.PrintWarning("Failed to write archive logs to: \"" + this.m_logPath + "\"!");
      Log.All.PrintWarning(ex.ToString());
    }
  }

  public static void Init()
  {
    LogArchive.Get();
  }

  public static LogArchive Get()
  {
    if (LogArchive.s_instance == null)
      LogArchive.s_instance = new LogArchive();
    return LogArchive.s_instance;
  }

  private void CleanOldLogs(string logFolderPath)
  {
    int num1 = 5;
    FileInfo[] files = new DirectoryInfo(logFolderPath).GetFiles();
    Array.Sort<FileInfo>(files, (Comparison<FileInfo>) ((a, b) => a.LastWriteTime.CompareTo(b.LastWriteTime)));
    int num2 = files.Length - (num1 - 1);
    for (int index = 0; index < num2 && index < files.Length; ++index)
      files[index].Delete();
  }

  private void MakeLogPath(string logFolderPath)
  {
    if (!string.IsNullOrEmpty(this.m_logPath))
      return;
    string timestamp = LogArchive.GenerateTimestamp();
    string str = "hearthstone_" + timestamp.Replace("-", "_").Replace(" ", "_").Replace(":", "_").Remove(timestamp.Length - 4) + ".log";
    this.m_logPath = logFolderPath + "/" + str;
  }

  private void HandleLog(string logString, string stackTrace, LogType type)
  {
    if (this.m_stopLogging)
      return;
    try
    {
      if ((long) (this.m_numLinesWritten % 100UL) == 0L)
      {
        FileInfo fileInfo = new FileInfo(this.m_logPath);
        if (fileInfo.Exists && fileInfo.Length > (long) (this.m_maxFileSizeKB * 1024))
        {
          this.m_stopLogging = true;
          using (StreamWriter log = new StreamWriter(this.m_logPath, true))
          {
            LogArchive.WriteLogLine(log, string.Empty);
            LogArchive.WriteLogLine(log, string.Empty);
            LogArchive.WriteLogLine(log, "==================================================================");
            LogArchive.WriteLogLine(log, "Truncating log, which has reached the size limit of {0}KB", (object) this.m_maxFileSizeKB);
            LogArchive.WriteLogLine(log, "==================================================================\n\n");
            return;
          }
        }
      }
      using (StreamWriter log = new StreamWriter(this.m_logPath, true))
      {
        if (type == LogType.Error || type == LogType.Exception)
          LogArchive.WriteLogLine(log, "{0}\n{1}", (object) logString, (object) stackTrace);
        else
          LogArchive.WriteLogLine(log, "{0}", (object) logString, (object) stackTrace);
        ++this.m_numLinesWritten;
      }
    }
    catch (Exception ex)
    {
      Log.All.PrintError("LogArchive.HandleLog() - Failed to write \"{0}\". Exception={1}", new object[2]
      {
        (object) logString,
        (object) ex.Message
      });
    }
  }

  private static string GenerateTimestamp()
  {
    return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
  }

  private static void WriteLogLine(StreamWriter log, string format, params object[] args)
  {
    string str = LogArchive.GenerateTimestamp() + ": " + GeneralUtils.SafeFormat(format, args);
    try
    {
      log.WriteLine(str);
      log.Flush();
    }
    catch (Exception ex)
    {
    }
  }
}
