﻿// Decompiled with JetBrains decompiler
// Type: BloodWarriors
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class BloodWarriors : SpawnToHandSpell
{
  protected override void OnAction(SpellStateType prevStateType)
  {
    ZonePlay battlefieldZone = this.GetSourceCard().GetEntity().GetController().GetBattlefieldZone();
    for (int targetIndex = 0; targetIndex < this.m_targets.Count; ++targetIndex)
    {
      for (int index = 0; index < battlefieldZone.GetCardCount(); ++index)
      {
        Card cardAtIndex = battlefieldZone.GetCardAtIndex(index);
        if (cardAtIndex.GetPredictedZonePosition() == 0 && cardAtIndex.GetEntity().GetDamage() != 0 && this.AddUniqueOriginForTarget(targetIndex, cardAtIndex))
          break;
      }
    }
    base.OnAction(prevStateType);
  }
}
