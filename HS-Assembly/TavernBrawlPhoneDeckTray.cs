﻿// Decompiled with JetBrains decompiler
// Type: TavernBrawlPhoneDeckTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TavernBrawlPhoneDeckTray : BasePhoneDeckTray
{
  [CustomEditField(Sections = "Buttons")]
  public StandardPegButtonNew m_RetireButton;
  private static TavernBrawlPhoneDeckTray s_instance;

  protected override void Awake()
  {
    base.Awake();
    this.m_RetireButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRetireClicked));
    TavernBrawlPhoneDeckTray.s_instance = this;
  }

  private void OnDestroy()
  {
    TavernBrawlPhoneDeckTray.s_instance = (TavernBrawlPhoneDeckTray) null;
    CollectionManager.Get().ClearTaggedDeck(CollectionManager.DeckTag.Editing);
  }

  public static TavernBrawlPhoneDeckTray Get()
  {
    return TavernBrawlPhoneDeckTray.s_instance;
  }

  public override void Initialize()
  {
    CollectionDeck tavernBrawlDeck = TavernBrawlManager.Get().CurrentDeck();
    if (tavernBrawlDeck == null)
      return;
    this.OnTavernBrawlDeckInitialized(tavernBrawlDeck);
  }

  private void OnTavernBrawlDeckInitialized(CollectionDeck tavernBrawlDeck)
  {
    if (tavernBrawlDeck == null)
    {
      Debug.LogError((object) "Draft deck is null.");
    }
    else
    {
      CollectionManager.Get().SetTaggedDeck(CollectionManager.DeckTag.Editing, tavernBrawlDeck, (object) null);
      this.OnCardCountUpdated(tavernBrawlDeck.GetTotalCardCount());
      this.m_cardsContent.UpdateCardList(true, (Actor) null);
    }
  }

  private void OnRetireClicked(UIEvent e)
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_TAVERN_BRAWL_RETIRE_CONFIRM_HEADER"),
      m_showAlertIcon = false,
      m_text = GameStrings.Get("GLUE_TAVERN_BRAWL_RETIRE_CONFIRM_DESC"),
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_responseCallback = new AlertPopup.ResponseCallback(this.OnRetireButtonConfirmationResponse)
    });
  }

  private void OnRetireButtonConfirmationResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
      return;
    Network.TavernBrawlRetire();
  }
}
