﻿// Decompiled with JetBrains decompiler
// Type: RankChangeStar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class RankChangeStar : MonoBehaviour
{
  public MeshRenderer m_starMeshRenderer;
  public MeshRenderer m_bottomGlowRenderer;
  public MeshRenderer m_topGlowRenderer;

  public void BlackOut()
  {
    this.m_starMeshRenderer.enabled = false;
  }

  public void UnBlackOut()
  {
    this.m_starMeshRenderer.enabled = true;
  }

  public void FadeIn()
  {
    this.GetComponent<PlayMakerFSM>().SendEvent("FadeIn");
  }

  public void Spawn()
  {
    this.GetComponent<PlayMakerFSM>().SendEvent("Spawn");
  }

  public void Reset()
  {
    this.GetComponent<PlayMakerFSM>().SendEvent("Reset");
  }

  public void Blink(float delay)
  {
    this.StartCoroutine(this.DelayedBlink(delay));
  }

  [DebuggerHidden]
  public IEnumerator DelayedBlink(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeStar.\u003CDelayedBlink\u003Ec__Iterator93()
    {
      delay = delay,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u003Ef__this = this
    };
  }

  public void Burst(float delay)
  {
    this.StartCoroutine(this.DelayedBurst(delay));
  }

  [DebuggerHidden]
  public IEnumerator DelayedBurst(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeStar.\u003CDelayedBurst\u003Ec__Iterator94()
    {
      delay = delay,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  public IEnumerator DelayedDespawn(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeStar.\u003CDelayedDespawn\u003Ec__Iterator95()
    {
      delay = delay,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u003Ef__this = this
    };
  }

  public void Despawn()
  {
    this.GetComponent<PlayMakerFSM>().SendEvent("DeSpawn");
  }

  public void Wipe(float delay)
  {
    this.StartCoroutine(this.DelayedWipe(delay));
  }

  [DebuggerHidden]
  public IEnumerator DelayedWipe(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeStar.\u003CDelayedWipe\u003Ec__Iterator96()
    {
      delay = delay,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u003Ef__this = this
    };
  }
}
