﻿// Decompiled with JetBrains decompiler
// Type: GeneralStoreAdventureSelectorButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class GeneralStoreAdventureSelectorButton : PegUIElement
{
  public GameLayer m_unavailableTooltipLayer = GameLayer.PerspectiveUI;
  public float m_unavailableTooltipScale = 20f;
  public UberText m_adventureTitle;
  public HighlightState m_highlight;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_selectSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_unselectSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_mouseOverSound;
  public TooltipZone m_unavailableTooltip;
  public GameObject m_preorderRibbon;
  private bool m_selected;
  private AdventureDbId m_adventureId;

  public void SetAdventureId(AdventureDbId adventureId)
  {
    if ((Object) this.m_adventureTitle != (Object) null)
    {
      AdventureDbfRecord record = GameDbf.Adventure.GetRecord((int) adventureId);
      if (record != null)
        this.m_adventureTitle.Text = (string) record.StoreBuyButtonLabel;
    }
    this.m_adventureId = adventureId;
    this.UpdateState();
  }

  public AdventureDbId GetAdventureId()
  {
    return this.m_adventureId;
  }

  public void Select()
  {
    if (this.m_selected)
      return;
    this.m_selected = true;
    this.m_highlight.ChangeState(this.GetInteractionState() != PegUIElement.InteractionState.Up ? ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE : ActorStateType.HIGHLIGHT_SECONDARY_ACTIVE);
    if (string.IsNullOrEmpty(this.m_selectSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_selectSound));
  }

  public void Unselect()
  {
    if (!this.m_selected)
      return;
    this.m_selected = false;
    this.m_highlight.ChangeState(ActorStateType.NONE);
    if (string.IsNullOrEmpty(this.m_unselectSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_unselectSound));
  }

  public bool IsPrePurchase()
  {
    Network.Bundle bundle = (Network.Bundle) null;
    StoreManager.Get().GetAvailableAdventureBundle(this.m_adventureId, GeneralStoreAdventureContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, out bundle);
    if (bundle != null)
      return StoreManager.Get().IsProductPrePurchase(bundle);
    return false;
  }

  public void UpdateState()
  {
    if (!((Object) this.m_preorderRibbon != (Object) null))
      return;
    this.m_preorderRibbon.SetActive(this.IsPrePurchase());
  }

  public bool IsPurchasable()
  {
    ProductType adventureProductType = StoreManager.GetAdventureProductType(this.m_adventureId);
    if (adventureProductType == ProductType.PRODUCT_TYPE_UNKNOWN)
      return false;
    bool productTypeExists = false;
    List<Network.Bundle> bundlesForProduct = StoreManager.Get().GetAvailableBundlesForProduct(adventureProductType, GeneralStoreAdventureContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, false, out productTypeExists, 0, 0);
    if (bundlesForProduct != null)
      return bundlesForProduct.Count > 0;
    return false;
  }

  public bool IsAvailable()
  {
    Network.Bundle bundle = (Network.Bundle) null;
    bool productExists = false;
    StoreManager.Get().GetAvailableAdventureBundle(this.m_adventureId, GeneralStoreAdventureContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, out bundle, out productExists);
    return productExists;
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    base.OnOver(oldState);
    if (this.IsAvailable())
    {
      this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_SECONDARY_ACTIVE);
      if (string.IsNullOrEmpty(this.m_mouseOverSound))
        return;
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_mouseOverSound));
    }
    else
    {
      if (!((Object) this.m_unavailableTooltip != (Object) null))
        return;
      SceneUtils.SetLayer((Component) this.m_unavailableTooltip.ShowTooltip(GameStrings.Get("GLUE_STORE_ADVENTURE_BUTTON_UNAVAILABLE_HEADLINE"), GameStrings.Get("GLUE_STORE_ADVENTURE_BUTTON_UNAVAILABLE_DESCRIPTION"), this.m_unavailableTooltipScale, true), this.m_unavailableTooltipLayer);
    }
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    base.OnOut(oldState);
    if (this.IsAvailable())
    {
      this.m_highlight.ChangeState(!this.m_selected ? ActorStateType.NONE : ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    }
    else
    {
      if (!((Object) this.m_unavailableTooltip != (Object) null))
        return;
      this.m_unavailableTooltip.HideTooltip();
    }
  }

  protected override void OnRelease()
  {
    base.OnRelease();
    if (!this.IsAvailable())
      return;
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_SECONDARY_ACTIVE);
  }

  protected override void OnPress()
  {
    base.OnPress();
    if (!this.IsAvailable())
      return;
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }
}
