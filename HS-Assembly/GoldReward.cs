﻿// Decompiled with JetBrains decompiler
// Type: GoldReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GoldReward : Reward
{
  public bool m_RotateIn = true;
  public GameObject m_coin;

  protected override void InitData()
  {
    this.SetData((RewardData) new GoldRewardData(), false);
  }

  protected override void ShowReward(bool updateCacheValues)
  {
    if (!(this.Data as GoldRewardData).IsDummyReward)
    {
      if (this.Data.Origin != NetCache.ProfileNotice.NoticeOrigin.BETA_REIMBURSE ? updateCacheValues : NetCache.Get().GetNetObject<NetCache.NetCacheGoldBalance>().GetTotal() == 0L)
        NetCache.Get().RefreshNetObject<NetCache.NetCacheGoldBalance>();
    }
    this.m_root.SetActive(true);
    Vector3 localScale = this.m_coin.transform.localScale;
    this.m_coin.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    iTween.ScaleTo(this.m_coin.gameObject, iTween.Hash((object) "scale", (object) localScale, (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.easeOutElastic));
    if (!this.m_RotateIn)
      return;
    this.m_coin.transform.localEulerAngles = new Vector3(0.0f, 180f, 180f);
    iTween.RotateAdd(this.m_coin.gameObject, iTween.Hash((object) "amount", (object) new Vector3(0.0f, 0.0f, 540f), (object) "time", (object) 1.5f, (object) "easeType", (object) iTween.EaseType.easeOutElastic, (object) "space", (object) Space.Self));
  }

  protected override void HideReward()
  {
    base.HideReward();
    this.m_root.SetActive(false);
  }

  protected override void OnDataSet(bool updateVisuals)
  {
    if (!updateVisuals)
      return;
    GoldRewardData data = this.Data as GoldRewardData;
    if (data == null)
    {
      Debug.LogWarning((object) string.Format("goldRewardData.SetData() - data {0} is not GoldRewardData", (object) this.Data));
    }
    else
    {
      string headline = GameStrings.Get("GLOBAL_REWARD_GOLD_HEADLINE");
      string details = data.Amount.ToString();
      string source = string.Empty;
      UberText componentsInChild = this.m_coin.GetComponentsInChildren<UberText>(true)[0];
      if ((Object) componentsInChild != (Object) null)
      {
        this.m_rewardBanner.m_detailsText = componentsInChild;
        this.m_rewardBanner.AlignHeadlineToCenterBone();
      }
      NetCache.ProfileNotice.NoticeOrigin origin = this.Data.Origin;
      switch (origin)
      {
        case NetCache.ProfileNotice.NoticeOrigin.BETA_REIMBURSE:
          headline = GameStrings.Get("GLOBAL_BETA_REIMBURSEMENT_HEADLINE");
          source = GameStrings.Get("GLOBAL_BETA_REIMBURSEMENT_DETAILS");
          break;
        case NetCache.ProfileNotice.NoticeOrigin.TOURNEY:
          source = GameStrings.Format("GLOBAL_REWARD_GOLD_SOURCE_TOURNEY", (object) NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>().WinsPerGold);
          break;
        default:
          if (origin == NetCache.ProfileNotice.NoticeOrigin.IGR)
          {
            if (data.Date.HasValue)
            {
              source = GameStrings.Format("GLOBAL_REWARD_GOLD_SOURCE_IGR_DATED", (object) GameStrings.Format("GLOBAL_CURRENT_DATE", (object) data.Date));
              break;
            }
            source = GameStrings.Get("GLOBAL_REWARD_GOLD_SOURCE_IGR");
            break;
          }
          break;
      }
      this.SetRewardText(headline, details, source);
    }
  }
}
