﻿// Decompiled with JetBrains decompiler
// Type: NefarianSwapSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class NefarianSwapSpell : HeroSwapSpell
{
  public float m_obsoleteRemovalDelay;
  private Card m_obsoleteHeroCard;

  public override bool AddPowerTargets()
  {
    if (!base.AddPowerTargets())
      return false;
    int tag = this.m_oldHeroCard.GetEntity().GetTag(GAME_TAG.LINKED_ENTITY);
    if (tag != 0)
      this.m_obsoleteHeroCard = GameState.Get().GetEntity(tag).GetCard();
    return !((Object) this.m_obsoleteHeroCard == (Object) null);
  }

  public override void CustomizeFXProcess(Actor heroActor)
  {
    if (!((Object) heroActor == (Object) this.m_newHeroCard.GetActor()))
      return;
    this.StartCoroutine(this.DestroyObsolete());
  }

  [DebuggerHidden]
  private IEnumerator DestroyObsolete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NefarianSwapSpell.\u003CDestroyObsolete\u003Ec__Iterator2C5() { \u003C\u003Ef__this = this };
  }
}
