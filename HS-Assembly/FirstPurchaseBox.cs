﻿// Decompiled with JetBrains decompiler
// Type: FirstPurchaseBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class FirstPurchaseBox : MonoBehaviour
{
  public GameObject m_BoxBase;
  public GameObject m_BoxLid;
  public GameObject m_CardRootBone;
  public AnimationClip m_RevealCardAnimation;
  public AnimationClip m_GlowOutAnimation;
  private string m_CardId;
  private Actor m_CardActor;
  private PlayMakerFSM m_fsm;
  private PegUIElement m_cardUIElement;
  private GameObject m_InputBlockerPerspectiveUI;
  private GameObject m_InputBlockerCameraMask;

  private void Awake()
  {
    this.m_fsm = this.GetComponent<PlayMakerFSM>();
  }

  public void Reset()
  {
    Log.Kyle.Print("Reset()");
    this.m_CardRootBone.SetActive(false);
    this.m_BoxBase.SetActive(false);
    this.m_BoxLid.SetActive(true);
    SceneUtils.EnableColliders(this.m_CardRootBone, false);
  }

  public void RevealContents()
  {
    this.m_BoxBase.SetActive(true);
    Log.Kyle.Print("RevealContents()");
    if (!((UnityEngine.Object) this.m_fsm != (UnityEngine.Object) null))
      return;
    this.m_fsm.SendEvent("Action");
  }

  [ContextMenu("Fake Purchase")]
  public void FakePurchase()
  {
    this.PurchaseBundle("NEW1_030");
  }

  public void PurchaseBundle(string cardID)
  {
    if (string.IsNullOrEmpty(cardID))
    {
      UnityEngine.Debug.LogWarningFormat("PurchaseBundle() - CardID is empty");
    }
    else
    {
      this.m_BoxLid.SetActive(false);
      this.m_BoxBase.SetActive(true);
      GameObject gameObject = AssetLoader.Get().LoadActor(ActorNames.GetHandActor(TAG_CARDTYPE.MINION), false, false);
      gameObject.transform.parent = this.m_CardRootBone.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      gameObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      this.m_CardActor = gameObject.GetComponent<Actor>();
      DefLoader.Get().LoadCardDef(cardID, new DefLoader.LoadDefCallback<CardDef>(this.OnCardDefLoaded), (object) gameObject, new CardPortraitQuality(3, true));
    }
  }

  private void OnCardDefLoaded(string cardID, CardDef cardDef, object callbackData)
  {
    if ((UnityEngine.Object) cardDef == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarningFormat("OnCardDefLoaded() - cardDef for CardID {0} is null", (object) cardID);
    }
    else
    {
      EntityDef entityDef = DefLoader.Get().GetEntityDef(cardID);
      if (entityDef == null)
      {
        UnityEngine.Debug.LogWarningFormat("OnCardDefLoaded() - entityDef for CardID {0} is null", (object) cardID);
      }
      else
      {
        this.m_CardId = cardID;
        GameObject go = (GameObject) callbackData;
        this.m_CardActor.gameObject.SetActive(true);
        this.m_CardActor.SetCardDef(cardDef);
        this.m_CardActor.SetEntityDef(entityDef);
        this.m_CardActor.SetPremium(TAG_PREMIUM.NORMAL);
        this.m_CardActor.UpdateAllComponents();
        SceneUtils.SetLayer(go, this.m_CardRootBone.layer);
        if ((UnityEngine.Object) this.m_fsm != (UnityEngine.Object) null)
          this.m_fsm.SendEvent("Birth");
        this.StartCoroutine(this.RevealCardAndSetupWaitForClick());
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator RevealCardAndSetupWaitForClick()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FirstPurchaseBox.\u003CRevealCardAndSetupWaitForClick\u003Ec__Iterator264() { \u003C\u003Ef__this = this };
  }

  private void PlayInnkeeperLineForClass(TAG_CLASS cardClass)
  {
    bool usePhoneUi = (bool) UniversalInputManager.UsePhoneUI;
    string empty = string.Empty;
    string soundName = string.Empty;
    switch (cardClass)
    {
      case TAG_CLASS.DRUID:
        empty = GameStrings.Get("GLUE_INKEEPER_RANDOM_CARD_DECK_RECIPE_DRUID");
        soundName = "VO_INKEEPER_Male_Dwarf_ClassLegendaryDruid_01";
        break;
      case TAG_CLASS.HUNTER:
        empty = GameStrings.Get("GLUE_INKEEPER_RANDOM_CARD_DECK_RECIPE_HUNTER");
        soundName = "VO_INKEEPER_Male_Dwarf_ClassLegendaryHunter_01";
        break;
      case TAG_CLASS.MAGE:
        empty = GameStrings.Get("GLUE_INKEEPER_RANDOM_CARD_DECK_RECIPE_MAGE");
        soundName = "VO_INKEEPER_Male_Dwarf_ClassLegendaryMage_01";
        break;
      case TAG_CLASS.PALADIN:
        empty = GameStrings.Get("GLUE_INKEEPER_RANDOM_CARD_DECK_RECIPE_PALADIN");
        soundName = "VO_INKEEPER_Male_Dwarf_ClassLegendaryPaladin_01";
        break;
      case TAG_CLASS.PRIEST:
        empty = GameStrings.Get("GLUE_INKEEPER_RANDOM_CARD_DECK_RECIPE_PRIEST");
        soundName = "VO_INKEEPER_Male_Dwarf_ClassLegendaryPriest_01";
        break;
      case TAG_CLASS.ROGUE:
        empty = GameStrings.Get("GLUE_INKEEPER_RANDOM_CARD_DECK_RECIPE_ROGUE");
        soundName = "VO_INKEEPER_Male_Dwarf_ClassLegendaryRogue_01";
        break;
      case TAG_CLASS.SHAMAN:
        empty = GameStrings.Get("GLUE_INKEEPER_RANDOM_CARD_DECK_RECIPE_SHAMAN");
        soundName = "VO_INKEEPER_Male_Dwarf_ClassLegendaryShaman_01";
        break;
      case TAG_CLASS.WARLOCK:
        empty = GameStrings.Get("GLUE_INKEEPER_RANDOM_CARD_DECK_RECIPE_WARLOCK");
        soundName = "VO_INKEEPER_Male_Dwarf_ClassLegendaryWarlock_01";
        break;
      case TAG_CLASS.WARRIOR:
        empty = GameStrings.Get("GLUE_INKEEPER_RANDOM_CARD_DECK_RECIPE_WARRIOR");
        soundName = "VO_INKEEPER_Male_Dwarf_ClassLegendaryWarrior_01";
        break;
    }
    NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, empty, soundName, (Action) null, usePhoneUi);
  }

  private void OnCardClicked(UIEvent e)
  {
    this.OnCardClicked();
  }

  private void OnCardClicked()
  {
    if ((UnityEngine.Object) this.m_fsm != (UnityEngine.Object) null)
      this.m_fsm.SendEvent("Death");
    if ((UnityEngine.Object) this.m_CardRootBone != (UnityEngine.Object) null)
      SceneUtils.EnableColliders(this.m_CardRootBone, false);
    if ((UnityEngine.Object) this.m_cardUIElement != (UnityEngine.Object) null)
      this.m_cardUIElement.RemoveEventListener(UIEventType.PRESS, new UIEvent.Handler(this.OnCardClicked));
    this.ReturnToStore();
  }

  private void ReturnToStore()
  {
    ((GeneralStorePacksPane) ((GeneralStore) StoreManager.Get().GetCurrentStore()).GetCurrentPane()).RemoveFirstPurchaseBundle(this.m_GlowOutAnimation.length);
  }
}
