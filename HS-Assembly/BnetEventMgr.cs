﻿// Decompiled with JetBrains decompiler
// Type: BnetEventMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections.Generic;

public class BnetEventMgr
{
  private List<BnetEventMgr.ChangeListener> m_changeListeners = new List<BnetEventMgr.ChangeListener>();
  private static BnetEventMgr s_instance;

  public static BnetEventMgr Get()
  {
    if (BnetEventMgr.s_instance == null)
    {
      BnetEventMgr.s_instance = new BnetEventMgr();
      BnetEventMgr.s_instance.Initialize();
    }
    return BnetEventMgr.s_instance;
  }

  public void Initialize()
  {
    Network.Get().SetBnetStateHandler(new Network.BnetEventHandler(this.OnBnetEventsOccurred));
  }

  public void Shutdown()
  {
  }

  private void OnBnetEventsOccurred(BattleNet.BnetEvent[] bnetEvents)
  {
    foreach (BattleNet.BnetEvent bnetEvent in bnetEvents)
      this.FireChangeEvent(bnetEvent);
  }

  public bool AddChangeListener(BnetEventMgr.ChangeCallback callback)
  {
    return this.AddChangeListener(callback, (object) null);
  }

  public bool AddChangeListener(BnetEventMgr.ChangeCallback callback, object userData)
  {
    BnetEventMgr.ChangeListener changeListener = new BnetEventMgr.ChangeListener();
    changeListener.SetCallback(callback);
    changeListener.SetUserData(userData);
    if (this.m_changeListeners.Contains(changeListener))
      return false;
    this.m_changeListeners.Add(changeListener);
    return true;
  }

  public bool RemoveChangeListener(BnetEventMgr.ChangeCallback callback)
  {
    return this.RemoveChangeListener(callback, (object) null);
  }

  public bool RemoveChangeListener(BnetEventMgr.ChangeCallback callback, object userData)
  {
    BnetEventMgr.ChangeListener changeListener = new BnetEventMgr.ChangeListener();
    changeListener.SetCallback(callback);
    changeListener.SetUserData(userData);
    return this.m_changeListeners.Remove(changeListener);
  }

  private void FireChangeEvent(BattleNet.BnetEvent stateChange)
  {
    foreach (BnetEventMgr.ChangeListener changeListener in this.m_changeListeners.ToArray())
      changeListener.Fire(stateChange);
  }

  private class ChangeListener : EventListener<BnetEventMgr.ChangeCallback>
  {
    public void Fire(BattleNet.BnetEvent stateChange)
    {
      this.m_callback(stateChange, this.m_userData);
    }
  }

  public delegate void ChangeCallback(BattleNet.BnetEvent stateChange, object userData);
}
