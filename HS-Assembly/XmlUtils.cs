﻿// Decompiled with JetBrains decompiler
// Type: XmlUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Text;
using System.Xml;
using UnityEngine;

public class XmlUtils
{
  public static string EscapeXPathSearchString(string search)
  {
    char[] anyOf = new char[2]{ '\'', '"' };
    StringBuilder stringBuilder = new StringBuilder();
    int index = search.IndexOfAny(anyOf);
    if (index == -1)
    {
      stringBuilder.Append('\'');
      stringBuilder.Append(search);
      stringBuilder.Append('\'');
    }
    else
    {
      stringBuilder.Append("concat(");
      int startIndex = 0;
      for (; index != -1; index = search.IndexOfAny(anyOf, index + 1))
      {
        stringBuilder.Append('\'');
        stringBuilder.Append(search, startIndex, index - startIndex);
        stringBuilder.Append("', ");
        string str = (int) search[index] != 39 ? "'\"', " : "\"'\", ";
        stringBuilder.Append(str);
        startIndex = index + 1;
      }
      stringBuilder.Append('\'');
      stringBuilder.Append(search, startIndex, search.Length - startIndex);
      stringBuilder.Append("')");
    }
    return stringBuilder.ToString();
  }

  public static XmlDocument LoadXmlDocFromTextAsset(TextAsset asset)
  {
    string xml = (string) null;
    using (StringReader stringReader = new StringReader(asset.text))
      xml = stringReader.ReadToEnd();
    if (xml == null)
      return (XmlDocument) null;
    XmlDocument xmlDocument = new XmlDocument();
    xmlDocument.LoadXml(xml);
    return xmlDocument;
  }

  public static void RemoveAllChildNodes(XmlNode node)
  {
    if (node == null)
      return;
    while (node.HasChildNodes)
      node.RemoveChild(node.FirstChild);
  }
}
