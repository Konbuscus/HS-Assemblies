﻿// Decompiled with JetBrains decompiler
// Type: UIBHighlight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (PegUIElement), typeof (Collider))]
[CustomEditClass]
public class UIBHighlight : MonoBehaviour
{
  [CustomEditField(Sections = "Highlight Sounds", T = EditType.SOUND_PREFAB)]
  public string m_MouseOverSound = "Assets/Game/Sounds/Interface/Small_Mouseover";
  [CustomEditField(Sections = "Highlight Sounds", T = EditType.SOUND_PREFAB)]
  public string m_MouseDownSound = "Assets/Game/Sounds/Interface/Small_Click";
  [SerializeField]
  private bool m_EnableResponse = true;
  [CustomEditField(Sections = "Highlight Objects")]
  public GameObject m_MouseOverHighlight;
  [CustomEditField(Sections = "Highlight Objects")]
  public GameObject m_MouseDownHighlight;
  [CustomEditField(Sections = "Highlight Objects")]
  public GameObject m_MouseUpHighlight;
  [CustomEditField(Sections = "Highlight Sounds", T = EditType.SOUND_PREFAB)]
  public string m_MouseOutSound;
  [CustomEditField(Sections = "Highlight Sounds", T = EditType.SOUND_PREFAB)]
  public string m_MouseUpSound;
  [CustomEditField(Sections = "Behavior Settings")]
  public bool m_SelectOnRelease;
  [CustomEditField(Sections = "Behavior Settings")]
  public bool m_HideMouseOverOnPress;
  [SerializeField]
  private bool m_AlwaysOver;
  [CustomEditField(Label = "Enable", Sections = "Allow Selection")]
  public bool m_AllowSelection;
  [CustomEditField(Parent = "m_AllowSelection")]
  public GameObject m_SelectedHighlight;
  [CustomEditField(Parent = "m_AllowSelection")]
  public GameObject m_MouseOverSelectedHighlight;
  private PegUIElement m_PegUIElement;

  [CustomEditField(Sections = "Behavior Settings")]
  public bool AlwaysOver
  {
    get
    {
      return this.m_AlwaysOver;
    }
    set
    {
      this.m_AlwaysOver = value;
      this.ResetState();
    }
  }

  [CustomEditField(Sections = "Behavior Settings")]
  public bool EnableResponse
  {
    get
    {
      return this.m_EnableResponse;
    }
    set
    {
      this.m_EnableResponse = value;
      this.ResetState();
    }
  }

  private void Awake()
  {
    PegUIElement component = this.gameObject.GetComponent<PegUIElement>();
    if (!((Object) component != (Object) null))
      return;
    component.AddEventListener(UIEventType.ROLLOVER, (UIEvent.Handler) (e => this.OnRollOver(false)));
    component.AddEventListener(UIEventType.PRESS, (UIEvent.Handler) (e => this.OnPress(true)));
    component.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OnRelease()));
    component.AddEventListener(UIEventType.ROLLOUT, (UIEvent.Handler) (e => this.OnRollOut(false)));
    this.ResetState();
  }

  public void HighlightOnce()
  {
    this.OnRollOver(true);
  }

  public void Select()
  {
    if (this.m_SelectOnRelease)
      this.OnRelease(true);
    else
      this.OnPress(true);
  }

  public void SelectNoSound()
  {
    if (this.m_SelectOnRelease)
      this.OnRelease(false);
    else
      this.OnPress(false);
  }

  public void Reset()
  {
    this.ResetState();
    this.ShowHighlightObject(this.m_SelectedHighlight, false);
    this.ShowHighlightObject(this.m_MouseOverSelectedHighlight, false);
    this.ShowHighlightObject(this.m_MouseOverHighlight, false);
  }

  private void ResetState()
  {
    if (this.m_AlwaysOver)
      this.OnRollOver(true);
    else
      this.OnRollOut(true);
  }

  private void OnRollOver(bool force = false)
  {
    if (!this.m_EnableResponse && !force)
      return;
    if (!this.m_AlwaysOver)
      this.PlaySound(this.m_MouseOverSound);
    if (this.m_AllowSelection && ((Object) this.m_SelectedHighlight == (Object) null || this.m_SelectedHighlight.activeSelf))
    {
      this.ShowHighlightObject(this.m_MouseOverSelectedHighlight, true);
      this.ShowHighlightObject(this.m_SelectedHighlight, false);
      this.ShowHighlightObject(this.m_MouseOverHighlight, false);
      this.ShowHighlightObject(this.m_MouseUpHighlight, false);
      this.ShowHighlightObject(this.m_MouseDownHighlight, false);
    }
    else
    {
      this.ShowHighlightObject(this.m_MouseDownHighlight, false);
      this.ShowHighlightObject(this.m_MouseOverHighlight, true);
      this.ShowHighlightObject(this.m_MouseUpHighlight, false);
    }
  }

  private void OnRollOut(bool force = false)
  {
    if (!this.m_EnableResponse && !force)
      return;
    this.PlaySound(this.m_MouseOutSound);
    if (this.m_AllowSelection && ((Object) this.m_MouseOverSelectedHighlight == (Object) null || this.m_MouseOverSelectedHighlight.activeSelf))
    {
      this.ShowHighlightObject(this.m_SelectedHighlight, true);
      this.ShowHighlightObject(this.m_MouseOverSelectedHighlight, false);
      this.ShowHighlightObject(this.m_MouseOverHighlight, false);
      this.ShowHighlightObject(this.m_MouseUpHighlight, false);
      this.ShowHighlightObject(this.m_MouseDownHighlight, false);
    }
    else
    {
      this.ShowHighlightObject(this.m_MouseDownHighlight, false);
      this.ShowHighlightObject(this.m_MouseOverHighlight, this.m_AlwaysOver);
      this.ShowHighlightObject(this.m_MouseUpHighlight, !this.m_AlwaysOver);
    }
  }

  private void OnPress()
  {
    this.OnPress(true);
  }

  private void OnPress(bool playSound)
  {
    if (!this.m_EnableResponse)
      return;
    if (playSound)
      this.PlaySound(this.m_MouseDownSound);
    if (this.m_AllowSelection && !this.m_SelectOnRelease)
    {
      this.ShowHighlightObject(this.m_SelectedHighlight, true);
      this.ShowHighlightObject(this.m_MouseOverSelectedHighlight, false);
      this.ShowHighlightObject(this.m_MouseOverHighlight, false);
      this.ShowHighlightObject(this.m_MouseUpHighlight, false);
      this.ShowHighlightObject(this.m_MouseDownHighlight, false);
    }
    else
    {
      this.ShowHighlightObject(this.m_MouseDownHighlight, true);
      this.ShowHighlightObject(this.m_MouseOverHighlight, this.m_AlwaysOver || !this.m_HideMouseOverOnPress);
      this.ShowHighlightObject(this.m_MouseUpHighlight, !this.m_AlwaysOver);
    }
  }

  private void OnRelease()
  {
    this.OnRelease(true);
  }

  private void OnRelease(bool playSound)
  {
    if (!this.m_EnableResponse)
      return;
    if (playSound)
      this.PlaySound(this.m_MouseUpSound);
    if (this.m_AllowSelection && this.m_SelectOnRelease)
    {
      this.ShowHighlightObject(this.m_SelectedHighlight, true);
      this.ShowHighlightObject(this.m_MouseOverSelectedHighlight, false);
      this.ShowHighlightObject(this.m_MouseOverHighlight, false);
      this.ShowHighlightObject(this.m_MouseUpHighlight, false);
      this.ShowHighlightObject(this.m_MouseDownHighlight, false);
    }
    else
    {
      this.ShowHighlightObject(this.m_MouseDownHighlight, false);
      this.ShowHighlightObject(this.m_MouseOverHighlight, true);
      this.ShowHighlightObject(this.m_MouseUpHighlight, false);
    }
  }

  private void ShowHighlightObject(GameObject obj, bool show)
  {
    if (!((Object) obj != (Object) null) || obj.activeSelf == show)
      return;
    obj.SetActive(show);
  }

  private void PlaySound(string soundFilePath)
  {
    if (string.IsNullOrEmpty(soundFilePath))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(soundFilePath));
  }
}
