﻿// Decompiled with JetBrains decompiler
// Type: StoreChallengePrompt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using MiniJSON;
using PegasusUtil;
using System.Collections;
using System.Diagnostics;
using System.Text;
using UnityEngine;

public class StoreChallengePrompt : UIBPopup
{
  private static readonly StoreURL s_cvvURL = new StoreURL("https://nydus.battle.net/WTCG/{0}/client/support/cvv?targetRegion={1}", StoreURL.Param.LOCALE, StoreURL.Param.REGION);
  private string m_input = string.Empty;
  private const string FMT_URL_CVV_INFO = "https://nydus.battle.net/WTCG/{0}/client/support/cvv?targetRegion={1}";
  private const float TASSADAR_CHALLENGE_TIMEOUT_SECONDS = 15f;
  public UIBButton m_submitButton;
  public UIBButton m_cancelButton;
  public UberText m_messageText;
  public UberText m_inputText;
  public GameObject m_infoButtonFrame;
  public UIBButton m_infoButton;
  private string m_challengeID;
  private string m_challengeUrl;
  private JsonNode m_challengeJson;
  private JsonNode m_challengeInput;
  private string m_challengeType;

  public event StoreChallengePrompt.CancelListener OnCancel;

  public event StoreChallengePrompt.CompleteListener OnChallengeComplete;

  private void Awake()
  {
    this.m_inputText.RichText = false;
    this.m_submitButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnSubmitPressed));
    this.m_cancelButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCancelPressed));
    this.m_infoButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnInfoPressed));
  }

  [DebuggerHidden]
  public IEnumerator Show(string challengeUrl)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StoreChallengePrompt.\u003CShow\u003Ec__Iterator273() { challengeUrl = challengeUrl, \u003C\u0024\u003EchallengeUrl = challengeUrl, \u003C\u003Ef__this = this };
  }

  public string HideChallenge()
  {
    string challengeId = this.m_challengeID;
    this.Hide(false);
    return challengeId;
  }

  private void OnShown()
  {
    if (!this.IsShown())
      return;
    this.ShowInput();
  }

  protected override void Hide(bool animate)
  {
    if (!this.IsShown())
      return;
    this.m_shown = false;
    this.HideInput();
    this.DoHideAnimation(!animate, new UIBPopup.OnAnimationComplete(((UIBPopup) this).OnHidden));
  }

  protected override void OnHidden()
  {
    this.m_challengeID = (string) null;
  }

  private void Cancel()
  {
    string challengeId = this.m_challengeID;
    this.Hide(true);
    if (this.OnCancel == null)
      return;
    this.OnCancel(challengeId);
  }

  private void OnSubmitPressed(UIEvent e)
  {
    this.StartCoroutine(this.SubmitChallenge());
  }

  [DebuggerHidden]
  private IEnumerator SubmitChallenge()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StoreChallengePrompt.\u003CSubmitChallenge\u003Ec__Iterator274() { \u003C\u003Ef__this = this };
  }

  private void DisplayError(string header, string message, bool allowInputAgain, CancelPurchase.CancelReason? reason, string internalErrorInfo)
  {
    this.ClearInput();
    AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
    info.m_showAlertIcon = false;
    info.m_headerText = header;
    info.m_text = message;
    info.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
    info.m_alertTextAlignment = UberText.AlignmentOptions.Center;
    if (allowInputAgain)
      info.m_responseCallback = (AlertPopup.ResponseCallback) ((response, userData) => this.ShowInput());
    else
      this.FireComplete(this.HideChallenge(), false, reason, internalErrorInfo);
    DialogManager.Get().ShowPopup(info);
  }

  private void FireComplete(string challengeID, bool isSuccess, CancelPurchase.CancelReason? reason, string internalErrorInfo)
  {
    if (this.OnChallengeComplete == null)
      return;
    this.OnChallengeComplete(challengeID, isSuccess, reason, internalErrorInfo);
  }

  private void OnCancelPressed(UIEvent e)
  {
    this.Cancel();
  }

  private void OnInfoPressed(UIEvent e)
  {
    Application.OpenURL(StoreChallengePrompt.s_cvvURL.GetURL());
  }

  private void ShowInput()
  {
    this.m_inputText.gameObject.SetActive(false);
    Camera firstByLayer = CameraUtils.FindFirstByLayer(this.gameObject.layer);
    Bounds bounds = this.m_inputText.GetBounds();
    Rect guiViewportRect = CameraUtils.CreateGUIViewportRect(firstByLayer, bounds.min, bounds.max);
    UniversalInputManager.Get().UseTextInput(new UniversalInputManager.TextInputParams()
    {
      m_owner = this.gameObject,
      m_password = true,
      m_rect = guiViewportRect,
      m_updatedCallback = new UniversalInputManager.TextInputUpdatedCallback(this.OnInputUpdated),
      m_completedCallback = new UniversalInputManager.TextInputCompletedCallback(this.OnInputComplete),
      m_canceledCallback = new UniversalInputManager.TextInputCanceledCallback(this.OnInputCanceled),
      m_font = this.m_inputText.TrueTypeFont,
      m_alignment = new TextAnchor?(TextAnchor.MiddleCenter),
      m_maxCharacters = this.m_challengeInput == null ? 0 : (int) (long) this.m_challengeInput["max_length"]
    }, false);
    this.m_submitButton.SetEnabled(true);
  }

  private void HideInput()
  {
    UniversalInputManager.Get().CancelTextInput(this.gameObject, false);
    this.m_inputText.gameObject.SetActive(true);
    this.m_submitButton.SetEnabled(false);
  }

  private void ClearInput()
  {
    UniversalInputManager.Get().SetInputText(string.Empty, false);
  }

  private void OnInputUpdated(string input)
  {
    this.m_input = input;
    this.UpdateInputText();
  }

  private void OnInputComplete(string input)
  {
    this.m_input = input;
    this.UpdateInputText();
    this.StartCoroutine(this.SubmitChallenge());
  }

  private void OnInputCanceled(bool userRequested, GameObject requester)
  {
    this.m_input = string.Empty;
    this.UpdateInputText();
    this.Cancel();
  }

  private void UpdateInputText()
  {
    StringBuilder stringBuilder = new StringBuilder(this.m_input.Length);
    for (int index = 0; index < this.m_input.Length; ++index)
      stringBuilder.Append('*');
    this.m_inputText.Text = stringBuilder.ToString();
  }

  public delegate void CancelListener(string challengeID);

  public delegate void CompleteListener(string challengeID, bool isSuccess, CancelPurchase.CancelReason? reason, string internalErrorInfo);
}
