﻿// Decompiled with JetBrains decompiler
// Type: AdTrackingManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AdTrackingManager : MonoBehaviour
{
  private static AdTrackingManager s_Instance;

  private void Awake()
  {
    AdTrackingManager.s_Instance = this;
  }

  private void OnDestroy()
  {
    AdTrackingManager.s_Instance = (AdTrackingManager) null;
  }

  public static AdTrackingManager Get()
  {
    return AdTrackingManager.s_Instance;
  }

  public void TrackLogin()
  {
  }

  public void TrackFirstLogin()
  {
  }

  public void TrackAccountCreated()
  {
  }

  public void TrackAdventureProgress(string description)
  {
  }

  public void TrackTutorialProgress(string description)
  {
  }

  public void TrackSale(string price, string currencyCode, string productId, string transactionId)
  {
  }

  public void TrackCreditsLaunch()
  {
  }

  public void GetDeepLink()
  {
  }
}
