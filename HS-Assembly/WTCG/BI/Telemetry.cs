﻿// Decompiled with JetBrains decompiler
// Type: WTCG.BI.Telemetry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Text;

namespace WTCG.BI
{
  public class Telemetry : IProtoBuf
  {
    public bool HasBnetRegion_;
    private Telemetry.BnetRegion _BnetRegion_;
    public bool HasGameAccountId_;
    private ulong _GameAccountId_;
    public bool HasPlayerIdentity;
    private PlayerIdentity _PlayerIdentity;
    public bool HasErrorCode_;
    private long _ErrorCode_;
    public bool HasMessage_;
    private string _Message_;
    public bool HasPresenceChange;
    private PresenceChange _PresenceChange;

    public long Time_ { get; set; }

    public Telemetry.Level Level_ { get; set; }

    public Telemetry.Locale Locale_ { get; set; }

    public string Version_ { get; set; }

    public Telemetry.Platform Platform_ { get; set; }

    public string Os_ { get; set; }

    public Telemetry.ScreenUI ScreenUI_ { get; set; }

    public Telemetry.Store Store_ { get; set; }

    public string SessionId_ { get; set; }

    public string DeviceUniqueIdentifier_ { get; set; }

    public ulong Event_ { get; set; }

    public Telemetry.BnetRegion BnetRegion_
    {
      get
      {
        return this._BnetRegion_;
      }
      set
      {
        this._BnetRegion_ = value;
        this.HasBnetRegion_ = true;
      }
    }

    public ulong GameAccountId_
    {
      get
      {
        return this._GameAccountId_;
      }
      set
      {
        this._GameAccountId_ = value;
        this.HasGameAccountId_ = true;
      }
    }

    public PlayerIdentity PlayerIdentity
    {
      get
      {
        return this._PlayerIdentity;
      }
      set
      {
        this._PlayerIdentity = value;
        this.HasPlayerIdentity = value != null;
      }
    }

    public long ErrorCode_
    {
      get
      {
        return this._ErrorCode_;
      }
      set
      {
        this._ErrorCode_ = value;
        this.HasErrorCode_ = true;
      }
    }

    public string Message_
    {
      get
      {
        return this._Message_;
      }
      set
      {
        this._Message_ = value;
        this.HasMessage_ = value != null;
      }
    }

    public PresenceChange PresenceChange
    {
      get
      {
        return this._PresenceChange;
      }
      set
      {
        this._PresenceChange = value;
        this.HasPresenceChange = value != null;
      }
    }

    public void Deserialize(Stream stream)
    {
      Telemetry.Deserialize(stream, this);
    }

    public static Telemetry Deserialize(Stream stream, Telemetry instance)
    {
      return Telemetry.Deserialize(stream, instance, -1L);
    }

    public static Telemetry DeserializeLengthDelimited(Stream stream)
    {
      Telemetry instance = new Telemetry();
      Telemetry.DeserializeLengthDelimited(stream, instance);
      return instance;
    }

    public static Telemetry DeserializeLengthDelimited(Stream stream, Telemetry instance)
    {
      long limit = (long) ProtocolParser.ReadUInt32(stream) + stream.Position;
      return Telemetry.Deserialize(stream, instance, limit);
    }

    public static Telemetry Deserialize(Stream stream, Telemetry instance, long limit)
    {
      instance.BnetRegion_ = Telemetry.BnetRegion.REGION_UNINITIALIZED;
      while (limit < 0L || stream.Position < limit)
      {
        int num = stream.ReadByte();
        switch (num)
        {
          case -1:
            if (limit >= 0L)
              throw new EndOfStreamException();
            goto label_33;
          case 8:
            instance.Time_ = (long) ProtocolParser.ReadUInt64(stream);
            continue;
          case 16:
            instance.Level_ = (Telemetry.Level) ProtocolParser.ReadUInt64(stream);
            continue;
          case 24:
            instance.Locale_ = (Telemetry.Locale) ProtocolParser.ReadUInt64(stream);
            continue;
          case 34:
            instance.Version_ = ProtocolParser.ReadString(stream);
            continue;
          case 40:
            instance.Platform_ = (Telemetry.Platform) ProtocolParser.ReadUInt64(stream);
            continue;
          case 50:
            instance.Os_ = ProtocolParser.ReadString(stream);
            continue;
          case 56:
            instance.ScreenUI_ = (Telemetry.ScreenUI) ProtocolParser.ReadUInt64(stream);
            continue;
          case 64:
            instance.Store_ = (Telemetry.Store) ProtocolParser.ReadUInt64(stream);
            continue;
          case 74:
            instance.SessionId_ = ProtocolParser.ReadString(stream);
            continue;
          case 82:
            instance.DeviceUniqueIdentifier_ = ProtocolParser.ReadString(stream);
            continue;
          case 88:
            instance.Event_ = ProtocolParser.ReadUInt64(stream);
            continue;
          case 96:
            instance.BnetRegion_ = (Telemetry.BnetRegion) ProtocolParser.ReadUInt64(stream);
            continue;
          case 104:
            instance.GameAccountId_ = ProtocolParser.ReadUInt64(stream);
            continue;
          case 112:
            instance.ErrorCode_ = (long) ProtocolParser.ReadUInt64(stream);
            continue;
          case 122:
            instance.Message_ = ProtocolParser.ReadString(stream);
            continue;
          default:
            Key key = ProtocolParser.ReadKey((byte) num, stream);
            switch (key.Field)
            {
              case 16:
                if (key.WireType == Wire.LengthDelimited)
                {
                  if (instance.PlayerIdentity == null)
                  {
                    instance.PlayerIdentity = PlayerIdentity.DeserializeLengthDelimited(stream);
                    continue;
                  }
                  PlayerIdentity.DeserializeLengthDelimited(stream, instance.PlayerIdentity);
                  continue;
                }
                continue;
              case 17:
                if (key.WireType == Wire.LengthDelimited)
                {
                  if (instance.PresenceChange == null)
                  {
                    instance.PresenceChange = PresenceChange.DeserializeLengthDelimited(stream);
                    continue;
                  }
                  PresenceChange.DeserializeLengthDelimited(stream, instance.PresenceChange);
                  continue;
                }
                continue;
              case 0:
                throw new ProtocolBufferException("Invalid field id: 0, something went wrong in the stream");
              default:
                ProtocolParser.SkipKey(stream, key);
                continue;
            }
        }
      }
      if (stream.Position != limit)
        throw new ProtocolBufferException("Read past max limit");
label_33:
      return instance;
    }

    public void Serialize(Stream stream)
    {
      Telemetry.Serialize(stream, this);
    }

    public static void Serialize(Stream stream, Telemetry instance)
    {
      stream.WriteByte((byte) 8);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.Time_);
      stream.WriteByte((byte) 16);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.Level_);
      stream.WriteByte((byte) 24);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.Locale_);
      if (instance.Version_ == null)
        throw new ArgumentNullException("Version_", "Required by proto specification.");
      stream.WriteByte((byte) 34);
      ProtocolParser.WriteBytes(stream, Encoding.UTF8.GetBytes(instance.Version_));
      stream.WriteByte((byte) 40);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.Platform_);
      if (instance.Os_ == null)
        throw new ArgumentNullException("Os_", "Required by proto specification.");
      stream.WriteByte((byte) 50);
      ProtocolParser.WriteBytes(stream, Encoding.UTF8.GetBytes(instance.Os_));
      stream.WriteByte((byte) 56);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.ScreenUI_);
      stream.WriteByte((byte) 64);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.Store_);
      if (instance.SessionId_ == null)
        throw new ArgumentNullException("SessionId_", "Required by proto specification.");
      stream.WriteByte((byte) 74);
      ProtocolParser.WriteBytes(stream, Encoding.UTF8.GetBytes(instance.SessionId_));
      if (instance.DeviceUniqueIdentifier_ == null)
        throw new ArgumentNullException("DeviceUniqueIdentifier_", "Required by proto specification.");
      stream.WriteByte((byte) 82);
      ProtocolParser.WriteBytes(stream, Encoding.UTF8.GetBytes(instance.DeviceUniqueIdentifier_));
      stream.WriteByte((byte) 88);
      ProtocolParser.WriteUInt64(stream, instance.Event_);
      if (instance.HasBnetRegion_)
      {
        stream.WriteByte((byte) 96);
        ProtocolParser.WriteUInt64(stream, (ulong) instance.BnetRegion_);
      }
      if (instance.HasGameAccountId_)
      {
        stream.WriteByte((byte) 104);
        ProtocolParser.WriteUInt64(stream, instance.GameAccountId_);
      }
      if (instance.HasPlayerIdentity)
      {
        stream.WriteByte((byte) 130);
        stream.WriteByte((byte) 1);
        ProtocolParser.WriteUInt32(stream, instance.PlayerIdentity.GetSerializedSize());
        PlayerIdentity.Serialize(stream, instance.PlayerIdentity);
      }
      if (instance.HasErrorCode_)
      {
        stream.WriteByte((byte) 112);
        ProtocolParser.WriteUInt64(stream, (ulong) instance.ErrorCode_);
      }
      if (instance.HasMessage_)
      {
        stream.WriteByte((byte) 122);
        ProtocolParser.WriteBytes(stream, Encoding.UTF8.GetBytes(instance.Message_));
      }
      if (!instance.HasPresenceChange)
        return;
      stream.WriteByte((byte) 138);
      stream.WriteByte((byte) 1);
      ProtocolParser.WriteUInt32(stream, instance.PresenceChange.GetSerializedSize());
      PresenceChange.Serialize(stream, instance.PresenceChange);
    }

    public uint GetSerializedSize()
    {
      uint num1 = 0U + ProtocolParser.SizeOfUInt64((ulong) this.Time_) + ProtocolParser.SizeOfUInt64((ulong) this.Level_) + ProtocolParser.SizeOfUInt64((ulong) this.Locale_);
      uint byteCount1 = (uint) Encoding.UTF8.GetByteCount(this.Version_);
      uint num2 = num1 + (ProtocolParser.SizeOfUInt32(byteCount1) + byteCount1) + ProtocolParser.SizeOfUInt64((ulong) this.Platform_);
      uint byteCount2 = (uint) Encoding.UTF8.GetByteCount(this.Os_);
      uint num3 = num2 + (ProtocolParser.SizeOfUInt32(byteCount2) + byteCount2) + ProtocolParser.SizeOfUInt64((ulong) this.ScreenUI_) + ProtocolParser.SizeOfUInt64((ulong) this.Store_);
      uint byteCount3 = (uint) Encoding.UTF8.GetByteCount(this.SessionId_);
      uint num4 = num3 + (ProtocolParser.SizeOfUInt32(byteCount3) + byteCount3);
      uint byteCount4 = (uint) Encoding.UTF8.GetByteCount(this.DeviceUniqueIdentifier_);
      uint num5 = num4 + (ProtocolParser.SizeOfUInt32(byteCount4) + byteCount4) + ProtocolParser.SizeOfUInt64(this.Event_);
      if (this.HasBnetRegion_)
        num5 = num5 + 1U + ProtocolParser.SizeOfUInt64((ulong) this.BnetRegion_);
      if (this.HasGameAccountId_)
        num5 = num5 + 1U + ProtocolParser.SizeOfUInt64(this.GameAccountId_);
      if (this.HasPlayerIdentity)
      {
        uint num6 = num5 + 2U;
        uint serializedSize = this.PlayerIdentity.GetSerializedSize();
        num5 = num6 + (serializedSize + ProtocolParser.SizeOfUInt32(serializedSize));
      }
      if (this.HasErrorCode_)
        num5 = num5 + 1U + ProtocolParser.SizeOfUInt64((ulong) this.ErrorCode_);
      if (this.HasMessage_)
      {
        uint num6 = num5 + 1U;
        uint byteCount5 = (uint) Encoding.UTF8.GetByteCount(this.Message_);
        num5 = num6 + (ProtocolParser.SizeOfUInt32(byteCount5) + byteCount5);
      }
      if (this.HasPresenceChange)
      {
        uint num6 = num5 + 2U;
        uint serializedSize = this.PresenceChange.GetSerializedSize();
        num5 = num6 + (serializedSize + ProtocolParser.SizeOfUInt32(serializedSize));
      }
      return num5 + 11U;
    }

    public override int GetHashCode()
    {
      int num = this.GetType().GetHashCode() ^ this.Time_.GetHashCode() ^ this.Level_.GetHashCode() ^ this.Locale_.GetHashCode() ^ this.Version_.GetHashCode() ^ this.Platform_.GetHashCode() ^ this.Os_.GetHashCode() ^ this.ScreenUI_.GetHashCode() ^ this.Store_.GetHashCode() ^ this.SessionId_.GetHashCode() ^ this.DeviceUniqueIdentifier_.GetHashCode() ^ this.Event_.GetHashCode();
      if (this.HasBnetRegion_)
        num ^= this.BnetRegion_.GetHashCode();
      if (this.HasGameAccountId_)
        num ^= this.GameAccountId_.GetHashCode();
      if (this.HasPlayerIdentity)
        num ^= this.PlayerIdentity.GetHashCode();
      if (this.HasErrorCode_)
        num ^= this.ErrorCode_.GetHashCode();
      if (this.HasMessage_)
        num ^= this.Message_.GetHashCode();
      if (this.HasPresenceChange)
        num ^= this.PresenceChange.GetHashCode();
      return num;
    }

    public override bool Equals(object obj)
    {
      Telemetry telemetry = obj as Telemetry;
      return telemetry != null && (this.Time_.Equals(telemetry.Time_) && this.Level_.Equals((object) telemetry.Level_) && (this.Locale_.Equals((object) telemetry.Locale_) && this.Version_.Equals(telemetry.Version_)) && (this.Platform_.Equals((object) telemetry.Platform_) && this.Os_.Equals(telemetry.Os_) && (this.ScreenUI_.Equals((object) telemetry.ScreenUI_) && this.Store_.Equals((object) telemetry.Store_))) && (this.SessionId_.Equals(telemetry.SessionId_) && this.DeviceUniqueIdentifier_.Equals(telemetry.DeviceUniqueIdentifier_))) && (this.Event_.Equals(telemetry.Event_) && this.HasBnetRegion_ == telemetry.HasBnetRegion_ && (!this.HasBnetRegion_ || this.BnetRegion_.Equals((object) telemetry.BnetRegion_)) && this.HasGameAccountId_ == telemetry.HasGameAccountId_) && ((!this.HasGameAccountId_ || this.GameAccountId_.Equals(telemetry.GameAccountId_)) && (this.HasPlayerIdentity == telemetry.HasPlayerIdentity && (!this.HasPlayerIdentity || this.PlayerIdentity.Equals((object) telemetry.PlayerIdentity)) && this.HasErrorCode_ == telemetry.HasErrorCode_)) && ((!this.HasErrorCode_ || this.ErrorCode_.Equals(telemetry.ErrorCode_)) && (this.HasMessage_ == telemetry.HasMessage_ && (!this.HasMessage_ || this.Message_.Equals(telemetry.Message_)) && (this.HasPresenceChange == telemetry.HasPresenceChange && (!this.HasPresenceChange || this.PresenceChange.Equals((object) telemetry.PresenceChange)))));
    }

    public enum Level
    {
      LEVEL_NONE,
      LEVEL_INFO,
      LEVEL_WARN,
      LEVEL_ERROR,
    }

    public enum Locale
    {
      LOCALE_UNKNOWN = 0,
      LOCALE_ENUS = 1,
      LOCALE_ENGB = 2,
      LOCALE_FRFR = 3,
      LOCALE_DEDE = 4,
      LOCALE_KOKR = 5,
      LOCALE_ESES = 6,
      LOCALE_ESMX = 7,
      LOCALE_RURU = 8,
      LOCALE_ZHTW = 9,
      LOCALE_ZHCN = 10,
      LOCALE_ITIT = 11,
      LOCALE_PTBR = 12,
      LOCALE_PLPL = 13,
      LOCALE_15 = 15,
      LOCALE_16 = 16,
    }

    public enum Platform
    {
      PLATFORM_UNKNOWN,
      PLATFORM_PC,
      PLATFORM_MAC,
      PLATFORM_IOS,
      PLATFORM_ANDROID,
    }

    public enum ScreenUI
    {
      SCREENUI_UNKNOWN,
      SCREENUI_DESKTOP,
      SCREENUI_TABLET,
      SCREENUI_PHONE,
    }

    public enum Store
    {
      STORE_BLIZZARD,
      STORE_IOS,
      STORE_GOOGLEPLAY,
      STORE_AMAZON,
    }

    public enum BnetRegion
    {
      REGION_UNINITIALIZED = -1,
      REGION_UNKNOWN = 0,
      REGION_US = 1,
      REGION_EU = 2,
      REGION_KR = 3,
      REGION_TW = 4,
      REGION_CN = 5,
      REGION_LIVE_VERIFICATION = 40,
      REGION_PTR_LOC = 41,
      REGION_DEV = 60,
      REGION_PTR = 98,
    }
  }
}
