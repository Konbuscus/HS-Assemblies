﻿// Decompiled with JetBrains decompiler
// Type: WTCG.BI.DataOnlyPatching
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Text;

namespace WTCG.BI
{
  public class DataOnlyPatching : IProtoBuf
  {
    public bool HasCurrentBuild_;
    private int _CurrentBuild_;
    public bool HasNewBuild_;
    private int _NewBuild_;

    public DataOnlyPatching.Status Status_ { get; set; }

    public DataOnlyPatching.Locale Locale_ { get; set; }

    public DataOnlyPatching.Platform Platform_ { get; set; }

    public DataOnlyPatching.BnetRegion BnetRegion_ { get; set; }

    public ulong GameAccountId_ { get; set; }

    public int CurrentBuild_
    {
      get
      {
        return this._CurrentBuild_;
      }
      set
      {
        this._CurrentBuild_ = value;
        this.HasCurrentBuild_ = true;
      }
    }

    public int NewBuild_
    {
      get
      {
        return this._NewBuild_;
      }
      set
      {
        this._NewBuild_ = value;
        this.HasNewBuild_ = true;
      }
    }

    public string SessionId_ { get; set; }

    public string DeviceUniqueIdentifier_ { get; set; }

    public void Deserialize(Stream stream)
    {
      DataOnlyPatching.Deserialize(stream, this);
    }

    public static DataOnlyPatching Deserialize(Stream stream, DataOnlyPatching instance)
    {
      return DataOnlyPatching.Deserialize(stream, instance, -1L);
    }

    public static DataOnlyPatching DeserializeLengthDelimited(Stream stream)
    {
      DataOnlyPatching instance = new DataOnlyPatching();
      DataOnlyPatching.DeserializeLengthDelimited(stream, instance);
      return instance;
    }

    public static DataOnlyPatching DeserializeLengthDelimited(Stream stream, DataOnlyPatching instance)
    {
      long limit = (long) ProtocolParser.ReadUInt32(stream) + stream.Position;
      return DataOnlyPatching.Deserialize(stream, instance, limit);
    }

    public static DataOnlyPatching Deserialize(Stream stream, DataOnlyPatching instance, long limit)
    {
      while (limit < 0L || stream.Position < limit)
      {
        int num = stream.ReadByte();
        switch (num)
        {
          case -1:
            if (limit >= 0L)
              throw new EndOfStreamException();
            goto label_18;
          case 8:
            instance.Status_ = (DataOnlyPatching.Status) ProtocolParser.ReadUInt64(stream);
            continue;
          case 16:
            instance.Locale_ = (DataOnlyPatching.Locale) ProtocolParser.ReadUInt64(stream);
            continue;
          case 24:
            instance.Platform_ = (DataOnlyPatching.Platform) ProtocolParser.ReadUInt64(stream);
            continue;
          case 32:
            instance.BnetRegion_ = (DataOnlyPatching.BnetRegion) ProtocolParser.ReadUInt64(stream);
            continue;
          case 40:
            instance.GameAccountId_ = ProtocolParser.ReadUInt64(stream);
            continue;
          case 48:
            instance.CurrentBuild_ = (int) ProtocolParser.ReadUInt64(stream);
            continue;
          case 56:
            instance.NewBuild_ = (int) ProtocolParser.ReadUInt64(stream);
            continue;
          case 66:
            instance.SessionId_ = ProtocolParser.ReadString(stream);
            continue;
          case 74:
            instance.DeviceUniqueIdentifier_ = ProtocolParser.ReadString(stream);
            continue;
          default:
            Key key = ProtocolParser.ReadKey((byte) num, stream);
            if ((int) key.Field == 0)
              throw new ProtocolBufferException("Invalid field id: 0, something went wrong in the stream");
            ProtocolParser.SkipKey(stream, key);
            continue;
        }
      }
      if (stream.Position != limit)
        throw new ProtocolBufferException("Read past max limit");
label_18:
      return instance;
    }

    public void Serialize(Stream stream)
    {
      DataOnlyPatching.Serialize(stream, this);
    }

    public static void Serialize(Stream stream, DataOnlyPatching instance)
    {
      stream.WriteByte((byte) 8);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.Status_);
      stream.WriteByte((byte) 16);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.Locale_);
      stream.WriteByte((byte) 24);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.Platform_);
      stream.WriteByte((byte) 32);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.BnetRegion_);
      stream.WriteByte((byte) 40);
      ProtocolParser.WriteUInt64(stream, instance.GameAccountId_);
      if (instance.HasCurrentBuild_)
      {
        stream.WriteByte((byte) 48);
        ProtocolParser.WriteUInt64(stream, (ulong) instance.CurrentBuild_);
      }
      if (instance.HasNewBuild_)
      {
        stream.WriteByte((byte) 56);
        ProtocolParser.WriteUInt64(stream, (ulong) instance.NewBuild_);
      }
      if (instance.SessionId_ == null)
        throw new ArgumentNullException("SessionId_", "Required by proto specification.");
      stream.WriteByte((byte) 66);
      ProtocolParser.WriteBytes(stream, Encoding.UTF8.GetBytes(instance.SessionId_));
      if (instance.DeviceUniqueIdentifier_ == null)
        throw new ArgumentNullException("DeviceUniqueIdentifier_", "Required by proto specification.");
      stream.WriteByte((byte) 74);
      ProtocolParser.WriteBytes(stream, Encoding.UTF8.GetBytes(instance.DeviceUniqueIdentifier_));
    }

    public uint GetSerializedSize()
    {
      uint num1 = 0U + ProtocolParser.SizeOfUInt64((ulong) this.Status_) + ProtocolParser.SizeOfUInt64((ulong) this.Locale_) + ProtocolParser.SizeOfUInt64((ulong) this.Platform_) + ProtocolParser.SizeOfUInt64((ulong) this.BnetRegion_) + ProtocolParser.SizeOfUInt64(this.GameAccountId_);
      if (this.HasCurrentBuild_)
        num1 = num1 + 1U + ProtocolParser.SizeOfUInt64((ulong) this.CurrentBuild_);
      if (this.HasNewBuild_)
        num1 = num1 + 1U + ProtocolParser.SizeOfUInt64((ulong) this.NewBuild_);
      uint byteCount1 = (uint) Encoding.UTF8.GetByteCount(this.SessionId_);
      uint num2 = num1 + (ProtocolParser.SizeOfUInt32(byteCount1) + byteCount1);
      uint byteCount2 = (uint) Encoding.UTF8.GetByteCount(this.DeviceUniqueIdentifier_);
      return num2 + (ProtocolParser.SizeOfUInt32(byteCount2) + byteCount2) + 7U;
    }

    public override int GetHashCode()
    {
      int num = this.GetType().GetHashCode() ^ this.Status_.GetHashCode() ^ this.Locale_.GetHashCode() ^ this.Platform_.GetHashCode() ^ this.BnetRegion_.GetHashCode() ^ this.GameAccountId_.GetHashCode();
      if (this.HasCurrentBuild_)
        num ^= this.CurrentBuild_.GetHashCode();
      if (this.HasNewBuild_)
        num ^= this.NewBuild_.GetHashCode();
      return num ^ this.SessionId_.GetHashCode() ^ this.DeviceUniqueIdentifier_.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      DataOnlyPatching dataOnlyPatching = obj as DataOnlyPatching;
      return dataOnlyPatching != null && this.Status_.Equals((object) dataOnlyPatching.Status_) && (this.Locale_.Equals((object) dataOnlyPatching.Locale_) && this.Platform_.Equals((object) dataOnlyPatching.Platform_)) && this.BnetRegion_.Equals((object) dataOnlyPatching.BnetRegion_) && (this.GameAccountId_.Equals(dataOnlyPatching.GameAccountId_) && this.HasCurrentBuild_ == dataOnlyPatching.HasCurrentBuild_) && (!this.HasCurrentBuild_ || this.CurrentBuild_.Equals(dataOnlyPatching.CurrentBuild_)) && (this.HasNewBuild_ == dataOnlyPatching.HasNewBuild_ && (!this.HasNewBuild_ || this.NewBuild_.Equals(dataOnlyPatching.NewBuild_))) && (this.SessionId_.Equals(dataOnlyPatching.SessionId_) && this.DeviceUniqueIdentifier_.Equals(dataOnlyPatching.DeviceUniqueIdentifier_));
    }

    public enum Status
    {
      SUCCEED,
      SUCCEED_WITH_CACHE,
      SUCCEED_WITH_TIMEOVER,
      FAILED_GENERIC,
      FAILED_DOWNLOADING,
      FAILED_BAD_DATA,
      FAILED_MD5_MISMATCH,
      FAILED_BAD_ASSETBUNDLE,
      STARTED,
    }

    public enum Locale
    {
      UnknownLocale = 0,
      enUS = 1,
      enGB = 2,
      frFR = 3,
      deDE = 4,
      koKR = 5,
      esES = 6,
      esMX = 7,
      ruRU = 8,
      zhTW = 9,
      zhCN = 10,
      itIT = 11,
      ptBR = 12,
      plPL = 13,
      Locale15 = 15,
      Locale16 = 16,
    }

    public enum Platform
    {
      UnknownPlatform,
      Windows,
      Mac,
      iPad,
      iPhone,
      Android_Tablet,
      Android_Phone,
    }

    public enum BnetRegion
    {
      REGION_UNINITIALIZED = -1,
      REGION_UNKNOWN = 0,
      REGION_US = 1,
      REGION_EU = 2,
      REGION_KR = 3,
      REGION_TW = 4,
      REGION_CN = 5,
      REGION_LIVE_VERIFICATION = 40,
      REGION_PTR_LOC = 41,
      REGION_DEV = 60,
    }
  }
}
