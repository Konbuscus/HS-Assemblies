﻿// Decompiled with JetBrains decompiler
// Type: WTCG.BI.PlayerIdentity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.IO;

namespace WTCG.BI
{
  public class PlayerIdentity : IProtoBuf
  {
    public bool HasPlayerId;
    private long _PlayerId;
    public bool HasGameAccount;
    private BnetId _GameAccount;
    public bool HasAccount;
    private BnetId _Account;

    public long PlayerId
    {
      get
      {
        return this._PlayerId;
      }
      set
      {
        this._PlayerId = value;
        this.HasPlayerId = true;
      }
    }

    public BnetId GameAccount
    {
      get
      {
        return this._GameAccount;
      }
      set
      {
        this._GameAccount = value;
        this.HasGameAccount = value != null;
      }
    }

    public BnetId Account
    {
      get
      {
        return this._Account;
      }
      set
      {
        this._Account = value;
        this.HasAccount = value != null;
      }
    }

    public void Deserialize(Stream stream)
    {
      PlayerIdentity.Deserialize(stream, this);
    }

    public static PlayerIdentity Deserialize(Stream stream, PlayerIdentity instance)
    {
      return PlayerIdentity.Deserialize(stream, instance, -1L);
    }

    public static PlayerIdentity DeserializeLengthDelimited(Stream stream)
    {
      PlayerIdentity instance = new PlayerIdentity();
      PlayerIdentity.DeserializeLengthDelimited(stream, instance);
      return instance;
    }

    public static PlayerIdentity DeserializeLengthDelimited(Stream stream, PlayerIdentity instance)
    {
      long limit = (long) ProtocolParser.ReadUInt32(stream) + stream.Position;
      return PlayerIdentity.Deserialize(stream, instance, limit);
    }

    public static PlayerIdentity Deserialize(Stream stream, PlayerIdentity instance, long limit)
    {
      while (limit < 0L || stream.Position < limit)
      {
        int num = stream.ReadByte();
        switch (num)
        {
          case -1:
            if (limit >= 0L)
              throw new EndOfStreamException();
            goto label_16;
          case 8:
            instance.PlayerId = (long) ProtocolParser.ReadUInt64(stream);
            continue;
          case 18:
            if (instance.GameAccount == null)
            {
              instance.GameAccount = BnetId.DeserializeLengthDelimited(stream);
              continue;
            }
            BnetId.DeserializeLengthDelimited(stream, instance.GameAccount);
            continue;
          case 26:
            if (instance.Account == null)
            {
              instance.Account = BnetId.DeserializeLengthDelimited(stream);
              continue;
            }
            BnetId.DeserializeLengthDelimited(stream, instance.Account);
            continue;
          default:
            Key key = ProtocolParser.ReadKey((byte) num, stream);
            if ((int) key.Field == 0)
              throw new ProtocolBufferException("Invalid field id: 0, something went wrong in the stream");
            ProtocolParser.SkipKey(stream, key);
            continue;
        }
      }
      if (stream.Position != limit)
        throw new ProtocolBufferException("Read past max limit");
label_16:
      return instance;
    }

    public void Serialize(Stream stream)
    {
      PlayerIdentity.Serialize(stream, this);
    }

    public static void Serialize(Stream stream, PlayerIdentity instance)
    {
      if (instance.HasPlayerId)
      {
        stream.WriteByte((byte) 8);
        ProtocolParser.WriteUInt64(stream, (ulong) instance.PlayerId);
      }
      if (instance.HasGameAccount)
      {
        stream.WriteByte((byte) 18);
        ProtocolParser.WriteUInt32(stream, instance.GameAccount.GetSerializedSize());
        BnetId.Serialize(stream, instance.GameAccount);
      }
      if (!instance.HasAccount)
        return;
      stream.WriteByte((byte) 26);
      ProtocolParser.WriteUInt32(stream, instance.Account.GetSerializedSize());
      BnetId.Serialize(stream, instance.Account);
    }

    public uint GetSerializedSize()
    {
      uint num1 = 0;
      if (this.HasPlayerId)
        num1 = num1 + 1U + ProtocolParser.SizeOfUInt64((ulong) this.PlayerId);
      if (this.HasGameAccount)
      {
        uint num2 = num1 + 1U;
        uint serializedSize = this.GameAccount.GetSerializedSize();
        num1 = num2 + (serializedSize + ProtocolParser.SizeOfUInt32(serializedSize));
      }
      if (this.HasAccount)
      {
        uint num2 = num1 + 1U;
        uint serializedSize = this.Account.GetSerializedSize();
        num1 = num2 + (serializedSize + ProtocolParser.SizeOfUInt32(serializedSize));
      }
      return num1;
    }

    public override int GetHashCode()
    {
      int hashCode = this.GetType().GetHashCode();
      if (this.HasPlayerId)
        hashCode ^= this.PlayerId.GetHashCode();
      if (this.HasGameAccount)
        hashCode ^= this.GameAccount.GetHashCode();
      if (this.HasAccount)
        hashCode ^= this.Account.GetHashCode();
      return hashCode;
    }

    public override bool Equals(object obj)
    {
      PlayerIdentity playerIdentity = obj as PlayerIdentity;
      return playerIdentity != null && this.HasPlayerId == playerIdentity.HasPlayerId && (!this.HasPlayerId || this.PlayerId.Equals(playerIdentity.PlayerId)) && (this.HasGameAccount == playerIdentity.HasGameAccount && (!this.HasGameAccount || this.GameAccount.Equals((object) playerIdentity.GameAccount)) && (this.HasAccount == playerIdentity.HasAccount && (!this.HasAccount || this.Account.Equals((object) playerIdentity.Account))));
    }
  }
}
