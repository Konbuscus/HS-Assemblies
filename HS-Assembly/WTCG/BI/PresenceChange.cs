﻿// Decompiled with JetBrains decompiler
// Type: WTCG.BI.PresenceChange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;

namespace WTCG.BI
{
  public class PresenceChange : IProtoBuf
  {
    private List<int> _NewStatusParameters = new List<int>();
    private List<int> _PreviousStatusParameters = new List<int>();
    public bool HasMillisecondsSincePreviousStatus;
    private long _MillisecondsSincePreviousStatus;
    public bool HasPreviousStatus;
    private int _PreviousStatus;

    public int NewStatus { get; set; }

    public List<int> NewStatusParameters
    {
      get
      {
        return this._NewStatusParameters;
      }
      set
      {
        this._NewStatusParameters = value;
      }
    }

    public long MillisecondsSincePreviousStatus
    {
      get
      {
        return this._MillisecondsSincePreviousStatus;
      }
      set
      {
        this._MillisecondsSincePreviousStatus = value;
        this.HasMillisecondsSincePreviousStatus = true;
      }
    }

    public int PreviousStatus
    {
      get
      {
        return this._PreviousStatus;
      }
      set
      {
        this._PreviousStatus = value;
        this.HasPreviousStatus = true;
      }
    }

    public List<int> PreviousStatusParameters
    {
      get
      {
        return this._PreviousStatusParameters;
      }
      set
      {
        this._PreviousStatusParameters = value;
      }
    }

    public void Deserialize(Stream stream)
    {
      PresenceChange.Deserialize(stream, this);
    }

    public static PresenceChange Deserialize(Stream stream, PresenceChange instance)
    {
      return PresenceChange.Deserialize(stream, instance, -1L);
    }

    public static PresenceChange DeserializeLengthDelimited(Stream stream)
    {
      PresenceChange instance = new PresenceChange();
      PresenceChange.DeserializeLengthDelimited(stream, instance);
      return instance;
    }

    public static PresenceChange DeserializeLengthDelimited(Stream stream, PresenceChange instance)
    {
      long limit = (long) ProtocolParser.ReadUInt32(stream) + stream.Position;
      return PresenceChange.Deserialize(stream, instance, limit);
    }

    public static PresenceChange Deserialize(Stream stream, PresenceChange instance, long limit)
    {
      if (instance.NewStatusParameters == null)
        instance.NewStatusParameters = new List<int>();
      if (instance.PreviousStatusParameters == null)
        instance.PreviousStatusParameters = new List<int>();
      while (limit < 0L || stream.Position < limit)
      {
        int num = stream.ReadByte();
        switch (num)
        {
          case -1:
            if (limit >= 0L)
              throw new EndOfStreamException();
            goto label_18;
          case 8:
            instance.NewStatus = (int) ProtocolParser.ReadUInt64(stream);
            continue;
          case 16:
            instance.NewStatusParameters.Add((int) ProtocolParser.ReadUInt64(stream));
            continue;
          case 24:
            instance.MillisecondsSincePreviousStatus = (long) ProtocolParser.ReadUInt64(stream);
            continue;
          case 32:
            instance.PreviousStatus = (int) ProtocolParser.ReadUInt64(stream);
            continue;
          case 40:
            instance.PreviousStatusParameters.Add((int) ProtocolParser.ReadUInt64(stream));
            continue;
          default:
            Key key = ProtocolParser.ReadKey((byte) num, stream);
            if ((int) key.Field == 0)
              throw new ProtocolBufferException("Invalid field id: 0, something went wrong in the stream");
            ProtocolParser.SkipKey(stream, key);
            continue;
        }
      }
      if (stream.Position != limit)
        throw new ProtocolBufferException("Read past max limit");
label_18:
      return instance;
    }

    public void Serialize(Stream stream)
    {
      PresenceChange.Serialize(stream, this);
    }

    public static void Serialize(Stream stream, PresenceChange instance)
    {
      stream.WriteByte((byte) 8);
      ProtocolParser.WriteUInt64(stream, (ulong) instance.NewStatus);
      if (instance.NewStatusParameters.Count > 0)
      {
        using (List<int>.Enumerator enumerator = instance.NewStatusParameters.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            int current = enumerator.Current;
            stream.WriteByte((byte) 16);
            ProtocolParser.WriteUInt64(stream, (ulong) current);
          }
        }
      }
      if (instance.HasMillisecondsSincePreviousStatus)
      {
        stream.WriteByte((byte) 24);
        ProtocolParser.WriteUInt64(stream, (ulong) instance.MillisecondsSincePreviousStatus);
      }
      if (instance.HasPreviousStatus)
      {
        stream.WriteByte((byte) 32);
        ProtocolParser.WriteUInt64(stream, (ulong) instance.PreviousStatus);
      }
      if (instance.PreviousStatusParameters.Count <= 0)
        return;
      using (List<int>.Enumerator enumerator = instance.PreviousStatusParameters.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          int current = enumerator.Current;
          stream.WriteByte((byte) 40);
          ProtocolParser.WriteUInt64(stream, (ulong) current);
        }
      }
    }

    public uint GetSerializedSize()
    {
      uint num = 0U + ProtocolParser.SizeOfUInt64((ulong) this.NewStatus);
      if (this.NewStatusParameters.Count > 0)
      {
        using (List<int>.Enumerator enumerator = this.NewStatusParameters.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            int current = enumerator.Current;
            ++num;
            num += ProtocolParser.SizeOfUInt64((ulong) current);
          }
        }
      }
      if (this.HasMillisecondsSincePreviousStatus)
        num = num + 1U + ProtocolParser.SizeOfUInt64((ulong) this.MillisecondsSincePreviousStatus);
      if (this.HasPreviousStatus)
        num = num + 1U + ProtocolParser.SizeOfUInt64((ulong) this.PreviousStatus);
      if (this.PreviousStatusParameters.Count > 0)
      {
        using (List<int>.Enumerator enumerator = this.PreviousStatusParameters.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            int current = enumerator.Current;
            ++num;
            num += ProtocolParser.SizeOfUInt64((ulong) current);
          }
        }
      }
      return num + 1U;
    }

    public override int GetHashCode()
    {
      int num = this.GetType().GetHashCode() ^ this.NewStatus.GetHashCode();
      using (List<int>.Enumerator enumerator = this.NewStatusParameters.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          int current = enumerator.Current;
          num ^= current.GetHashCode();
        }
      }
      if (this.HasMillisecondsSincePreviousStatus)
        num ^= this.MillisecondsSincePreviousStatus.GetHashCode();
      if (this.HasPreviousStatus)
        num ^= this.PreviousStatus.GetHashCode();
      using (List<int>.Enumerator enumerator = this.PreviousStatusParameters.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          int current = enumerator.Current;
          num ^= current.GetHashCode();
        }
      }
      return num;
    }

    public override bool Equals(object obj)
    {
      PresenceChange presenceChange = obj as PresenceChange;
      if (presenceChange == null || (!this.NewStatus.Equals(presenceChange.NewStatus) || this.NewStatusParameters.Count != presenceChange.NewStatusParameters.Count))
        return false;
      for (int index = 0; index < this.NewStatusParameters.Count; ++index)
      {
        if (!this.NewStatusParameters[index].Equals(presenceChange.NewStatusParameters[index]))
          return false;
      }
      if (this.HasMillisecondsSincePreviousStatus != presenceChange.HasMillisecondsSincePreviousStatus || this.HasMillisecondsSincePreviousStatus && !this.MillisecondsSincePreviousStatus.Equals(presenceChange.MillisecondsSincePreviousStatus) || (this.HasPreviousStatus != presenceChange.HasPreviousStatus || this.HasPreviousStatus && !this.PreviousStatus.Equals(presenceChange.PreviousStatus)) || this.PreviousStatusParameters.Count != presenceChange.PreviousStatusParameters.Count)
        return false;
      for (int index = 0; index < this.PreviousStatusParameters.Count; ++index)
      {
        if (!this.PreviousStatusParameters[index].Equals(presenceChange.PreviousStatusParameters[index]))
          return false;
      }
      return true;
    }
  }
}
