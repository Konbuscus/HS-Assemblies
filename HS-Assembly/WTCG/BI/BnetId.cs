﻿// Decompiled with JetBrains decompiler
// Type: WTCG.BI.BnetId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.IO;

namespace WTCG.BI
{
  public class BnetId : IProtoBuf
  {
    public ulong Hi { get; set; }

    public ulong Lo { get; set; }

    public void Deserialize(Stream stream)
    {
      BnetId.Deserialize(stream, this);
    }

    public static BnetId Deserialize(Stream stream, BnetId instance)
    {
      return BnetId.Deserialize(stream, instance, -1L);
    }

    public static BnetId DeserializeLengthDelimited(Stream stream)
    {
      BnetId instance = new BnetId();
      BnetId.DeserializeLengthDelimited(stream, instance);
      return instance;
    }

    public static BnetId DeserializeLengthDelimited(Stream stream, BnetId instance)
    {
      long limit = (long) ProtocolParser.ReadUInt32(stream) + stream.Position;
      return BnetId.Deserialize(stream, instance, limit);
    }

    public static BnetId Deserialize(Stream stream, BnetId instance, long limit)
    {
      while (limit < 0L || stream.Position < limit)
      {
        int num = stream.ReadByte();
        switch (num)
        {
          case -1:
            if (limit >= 0L)
              throw new EndOfStreamException();
            goto label_11;
          case 8:
            instance.Hi = ProtocolParser.ReadUInt64(stream);
            continue;
          case 16:
            instance.Lo = ProtocolParser.ReadUInt64(stream);
            continue;
          default:
            Key key = ProtocolParser.ReadKey((byte) num, stream);
            if ((int) key.Field == 0)
              throw new ProtocolBufferException("Invalid field id: 0, something went wrong in the stream");
            ProtocolParser.SkipKey(stream, key);
            continue;
        }
      }
      if (stream.Position != limit)
        throw new ProtocolBufferException("Read past max limit");
label_11:
      return instance;
    }

    public void Serialize(Stream stream)
    {
      BnetId.Serialize(stream, this);
    }

    public static void Serialize(Stream stream, BnetId instance)
    {
      stream.WriteByte((byte) 8);
      ProtocolParser.WriteUInt64(stream, instance.Hi);
      stream.WriteByte((byte) 16);
      ProtocolParser.WriteUInt64(stream, instance.Lo);
    }

    public uint GetSerializedSize()
    {
      return 0U + ProtocolParser.SizeOfUInt64(this.Hi) + ProtocolParser.SizeOfUInt64(this.Lo) + 2U;
    }

    public override int GetHashCode()
    {
      return this.GetType().GetHashCode() ^ this.Hi.GetHashCode() ^ this.Lo.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      BnetId bnetId = obj as BnetId;
      return bnetId != null && this.Hi.Equals(bnetId.Hi) && this.Lo.Equals(bnetId.Lo);
    }
  }
}
