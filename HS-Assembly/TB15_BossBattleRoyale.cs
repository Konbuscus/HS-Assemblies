﻿// Decompiled with JetBrains decompiler
// Type: TB15_BossBattleRoyale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class TB15_BossBattleRoyale : MissionEntity
{
  private static readonly Dictionary<int, string> bossEmotes = new Dictionary<int, string>() { { 2354, "Razorgore the Untamed" }, { 1819, "Noth the Plaguebringer" }, { 42346, "White King" }, { 17192, "Lady Nazjar" }, { 19116, "Skelesaurus Hex" }, { 39640, "The Curator" }, { 2313, "Coren Direbrew" }, { 2120, "Gothik the Harvester HEROIC" }, { 1826, "Grobbulus" } };

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB15_BossBattleRoyale.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1FB() { \u003C\u003Ef__this = this };
  }
}
