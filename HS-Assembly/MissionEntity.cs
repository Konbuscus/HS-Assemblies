﻿// Decompiled with JetBrains decompiler
// Type: MissionEntity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class MissionEntity : GameEntity
{
  protected static readonly List<EmoteType> STANDARD_EMOTE_RESPONSE_TRIGGERS = new List<EmoteType>() { EmoteType.GREETINGS, EmoteType.WELL_PLAYED, EmoteType.OOPS, EmoteType.SORRY, EmoteType.THANKS, EmoteType.THREATEN, EmoteType.WOW };
  protected List<MissionEntity.EmoteResponseGroup> m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>();
  protected const float TIME_TO_WAIT_BEFORE_ENDING_QUOTE = 5f;
  protected const float MINIMUM_DISPLAY_TIME_FOR_BIG_QUOTE = 3f;
  protected bool m_enemySpeaking;
  protected bool m_delayCardSoundSpells;

  public MissionEntity()
  {
    this.InitEmoteResponses();
  }

  public override void OnTagChanged(TagDelta change)
  {
    switch ((GAME_TAG) change.tag)
    {
      case GAME_TAG.MISSION_EVENT:
        this.HandleMissionEvent(change.newValue);
        break;
      case GAME_TAG.STEP:
        if (change.newValue == 4)
        {
          this.HandleMulliganTagChange();
          break;
        }
        if (change.oldValue == 9 && change.newValue == 10 && !GameState.Get().IsFriendlySidePlayerTurn())
        {
          this.HandleStartOfTurn(this.GetTag(GAME_TAG.TURN));
          break;
        }
        break;
      case GAME_TAG.NEXT_STEP:
        if (change.newValue == 6)
        {
          if (GameState.Get().IsMulliganManagerActive())
          {
            GameState.Get().SetMulliganBusy(true);
            break;
          }
          break;
        }
        if (change.oldValue == 9 && change.newValue == 10 && GameState.Get().IsFriendlySidePlayerTurn())
        {
          TurnStartManager.Get().BeginPlayingTurnEvents();
          break;
        }
        break;
    }
    base.OnTagChanged(change);
  }

  public override void NotifyOfStartOfTurnEventsFinished()
  {
    this.HandleStartOfTurn(this.GetTag(GAME_TAG.TURN));
  }

  public override void SendCustomEvent(int eventID)
  {
    this.HandleMissionEvent(eventID);
  }

  public override void NotifyOfOpponentWillPlayCard(string cardId)
  {
    base.NotifyOfOpponentWillPlayCard(cardId);
    Gameplay.Get().StartCoroutine(this.RespondToWillPlayCardWithTiming(cardId));
  }

  public override void NotifyOfOpponentPlayedCard(Entity entity)
  {
    base.NotifyOfOpponentPlayedCard(entity);
    Gameplay.Get().StartCoroutine(this.RespondToPlayedCardWithTiming(entity));
  }

  public override void NotifyOfFriendlyPlayedCard(Entity entity)
  {
    base.NotifyOfFriendlyPlayedCard(entity);
    Gameplay.Get().StartCoroutine(this.RespondToFriendlyPlayedCardWithTiming(entity));
  }

  public override void NotifyOfGameOver(TAG_PLAYSTATE gameResult)
  {
    base.NotifyOfGameOver(gameResult);
    Gameplay.Get().StartCoroutine(this.HandleGameOverWithTiming(gameResult));
  }

  public override bool ShouldUseSecretClassNames()
  {
    return true;
  }

  public override void OnEmotePlayed(Card card, EmoteType emoteType, CardSoundSpell emoteSpell)
  {
    if (!card.GetEntity().IsControlledByLocalUser())
      return;
    Gameplay.Get().StartCoroutine(this.HandlePlayerEmoteWithTiming(emoteType, emoteSpell));
  }

  public override bool IsEnemySpeaking()
  {
    return this.m_enemySpeaking;
  }

  public override bool ShouldDelayCardSoundSpells()
  {
    return this.m_delayCardSoundSpells;
  }

  public override bool DoAlternateMulliganIntro()
  {
    if (!this.ShouldDoAlternateMulliganIntro())
      return false;
    Gameplay.Get().StartCoroutine(this.DoAlternateMulliganIntroWithTiming());
    return true;
  }

  public bool IsHeroic()
  {
    return GameMgr.Get().IsHeroicMission();
  }

  public bool IsClassChallenge()
  {
    return GameMgr.Get().IsClassChallengeMission();
  }

  public bool IsNormal()
  {
    if (!this.IsHeroic())
      return !this.IsClassChallenge();
    return false;
  }

  protected virtual void HandleMulliganTagChange()
  {
    MulliganManager.Get().BeginMulligan();
  }

  protected void HandleStartOfTurn(int turn)
  {
    Gameplay.Get().StartCoroutine(this.HandleStartOfTurnWithTiming(turn));
  }

  [DebuggerHidden]
  protected virtual IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MissionEntity.\u003CHandleStartOfTurnWithTiming\u003Ec__IteratorF8 timingCIteratorF8 = new MissionEntity.\u003CHandleStartOfTurnWithTiming\u003Ec__IteratorF8();
    return (IEnumerator) timingCIteratorF8;
  }

  protected void HandleMissionEvent(int missionEvent)
  {
    Gameplay.Get().StartCoroutine(this.HandleMissionEventWithTiming(missionEvent));
  }

  [DebuggerHidden]
  protected virtual IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MissionEntity.\u003CHandleMissionEventWithTiming\u003Ec__IteratorF9 timingCIteratorF9 = new MissionEntity.\u003CHandleMissionEventWithTiming\u003Ec__IteratorF9();
    return (IEnumerator) timingCIteratorF9;
  }

  [DebuggerHidden]
  protected virtual IEnumerator RespondToWillPlayCardWithTiming(string cardId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MissionEntity.\u003CRespondToWillPlayCardWithTiming\u003Ec__IteratorFA timingCIteratorFa = new MissionEntity.\u003CRespondToWillPlayCardWithTiming\u003Ec__IteratorFA();
    return (IEnumerator) timingCIteratorFa;
  }

  [DebuggerHidden]
  protected virtual IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MissionEntity.\u003CRespondToPlayedCardWithTiming\u003Ec__IteratorFB timingCIteratorFb = new MissionEntity.\u003CRespondToPlayedCardWithTiming\u003Ec__IteratorFB();
    return (IEnumerator) timingCIteratorFb;
  }

  [DebuggerHidden]
  protected virtual IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MissionEntity.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__IteratorFC timingCIteratorFc = new MissionEntity.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__IteratorFC();
    return (IEnumerator) timingCIteratorFc;
  }

  [DebuggerHidden]
  protected virtual IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MissionEntity.\u003CHandleGameOverWithTiming\u003Ec__IteratorFD timingCIteratorFd = new MissionEntity.\u003CHandleGameOverWithTiming\u003Ec__IteratorFD();
    return (IEnumerator) timingCIteratorFd;
  }

  [DebuggerHidden]
  protected IEnumerator DoAlternateMulliganIntroWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MissionEntity.\u003CDoAlternateMulliganIntroWithTiming\u003Ec__IteratorFE timingCIteratorFe = new MissionEntity.\u003CDoAlternateMulliganIntroWithTiming\u003Ec__IteratorFE();
    return (IEnumerator) timingCIteratorFe;
  }

  protected void PlaySound(string audioName, float waitTimeScale = 1f, bool parentBubbleToActor = true, bool delayCardSoundSpells = false)
  {
    Gameplay.Get().StartCoroutine(this.PlaySoundAndWait(audioName, (string) null, Notification.SpeechBubbleDirection.None, (Actor) null, waitTimeScale, parentBubbleToActor, delayCardSoundSpells, 3f, 0.0f));
  }

  [DebuggerHidden]
  protected IEnumerator PlaySoundAndBlockSpeech(string audioName, float waitTimeScale = 1f, bool parentBubbleToActor = true, bool delayCardSoundSpells = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlaySoundAndBlockSpeech\u003Ec__IteratorFF() { audioName = audioName, waitTimeScale = waitTimeScale, parentBubbleToActor = parentBubbleToActor, delayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EparentBubbleToActor = parentBubbleToActor, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlaySoundAndBlockSpeech(string audioName, string stringName, Notification.SpeechBubbleDirection direction, Actor actor, float waitTimeScale = 1f, bool parentBubbleToActor = true, bool delayCardSoundSpells = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlaySoundAndBlockSpeech\u003Ec__Iterator100() { audioName = audioName, stringName = stringName, direction = direction, actor = actor, waitTimeScale = waitTimeScale, parentBubbleToActor = parentBubbleToActor, delayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EstringName = stringName, \u003C\u0024\u003Edirection = direction, \u003C\u0024\u003Eactor = actor, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EparentBubbleToActor = parentBubbleToActor, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlaySoundAndBlockSpeech(string audioID, Notification.SpeechBubbleDirection direction, Actor actor, float testingDuration = 3f, float waitTimeScale = 1f, bool parentBubbleToActor = true, bool delayCardSoundSpells = false, float bubbleScale = 0.0f)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlaySoundAndBlockSpeech\u003Ec__Iterator101() { audioID = audioID, direction = direction, actor = actor, waitTimeScale = waitTimeScale, parentBubbleToActor = parentBubbleToActor, delayCardSoundSpells = delayCardSoundSpells, testingDuration = testingDuration, bubbleScale = bubbleScale, \u003C\u0024\u003EaudioID = audioID, \u003C\u0024\u003Edirection = direction, \u003C\u0024\u003Eactor = actor, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EparentBubbleToActor = parentBubbleToActor, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EbubbleScale = bubbleScale, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlaySoundAndBlockSpeechOnce(string audioName, string stringName, Notification.SpeechBubbleDirection direction, Actor actor, float waitTimeScale = 1f, bool parentBubbleToActor = true, bool delayCardSoundSpells = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlaySoundAndBlockSpeechOnce\u003Ec__Iterator102() { audioName = audioName, stringName = stringName, direction = direction, actor = actor, waitTimeScale = waitTimeScale, parentBubbleToActor = parentBubbleToActor, delayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EstringName = stringName, \u003C\u0024\u003Edirection = direction, \u003C\u0024\u003Eactor = actor, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EparentBubbleToActor = parentBubbleToActor, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlaySoundAndBlockSpeechOnce(string audioID, Notification.SpeechBubbleDirection direction, Actor actor, float testingDuration = 3f, float waitTimeScale = 1f, bool parentBubbleToActor = true, bool delayCardSoundSpells = false, float bubbleScale = 0.0f)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlaySoundAndBlockSpeechOnce\u003Ec__Iterator103() { audioID = audioID, direction = direction, actor = actor, waitTimeScale = waitTimeScale, parentBubbleToActor = parentBubbleToActor, delayCardSoundSpells = delayCardSoundSpells, testingDuration = testingDuration, bubbleScale = bubbleScale, \u003C\u0024\u003EaudioID = audioID, \u003C\u0024\u003Edirection = direction, \u003C\u0024\u003Eactor = actor, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EparentBubbleToActor = parentBubbleToActor, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EbubbleScale = bubbleScale, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlaySoundAndWait(string audioName, string stringName, Notification.SpeechBubbleDirection direction, Actor actor, float waitTimeScale = 1f, bool parentBubbleToActor = true, bool delayCardSoundSpells = false, float testingDuration = 3f, float bubbleScale = 0.0f)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlaySoundAndWait\u003Ec__Iterator104() { audioName = audioName, testingDuration = testingDuration, waitTimeScale = waitTimeScale, delayCardSoundSpells = delayCardSoundSpells, direction = direction, stringName = stringName, actor = actor, parentBubbleToActor = parentBubbleToActor, bubbleScale = bubbleScale, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003Edirection = direction, \u003C\u0024\u003EstringName = stringName, \u003C\u0024\u003Eactor = actor, \u003C\u0024\u003EparentBubbleToActor = parentBubbleToActor, \u003C\u0024\u003EbubbleScale = bubbleScale, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayCharacterQuoteAndWait(string prefabName, string audioID, float testingDuration = 0.0f, bool allowRepeatDuringSession = true, bool delayCardSoundSpells = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayCharacterQuoteAndWait\u003Ec__Iterator105() { prefabName = prefabName, audioID = audioID, testingDuration = testingDuration, allowRepeatDuringSession = allowRepeatDuringSession, delayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EprefabName = prefabName, \u003C\u0024\u003EaudioID = audioID, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EallowRepeatDuringSession = allowRepeatDuringSession, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayCharacterQuoteAndWait(string prefabName, string audioName, string stringName, float testingDuration = 0.0f, bool allowRepeatDuringSession = true, bool delayCardSoundSpells = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayCharacterQuoteAndWait\u003Ec__Iterator106() { prefabName = prefabName, audioName = audioName, stringName = stringName, testingDuration = testingDuration, allowRepeatDuringSession = allowRepeatDuringSession, delayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EprefabName = prefabName, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EstringName = stringName, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EallowRepeatDuringSession = allowRepeatDuringSession, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayCharacterQuoteAndWait(string prefabName, string audioName, string stringName, Vector3 position, float waitTimeScale = 1f, float testingDuration = 0.0f, bool allowRepeatDuringSession = true, bool delayCardSoundSpells = false, bool isBig = false, Notification.SpeechBubbleDirection bubbleDir = Notification.SpeechBubbleDirection.None, bool persistCharacter = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayCharacterQuoteAndWait\u003Ec__Iterator108() { audioName = audioName, testingDuration = testingDuration, persistCharacter = persistCharacter, waitTimeScale = waitTimeScale, delayCardSoundSpells = delayCardSoundSpells, isBig = isBig, prefabName = prefabName, position = position, stringName = stringName, allowRepeatDuringSession = allowRepeatDuringSession, bubbleDir = bubbleDir, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EpersistCharacter = persistCharacter, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EisBig = isBig, \u003C\u0024\u003EprefabName = prefabName, \u003C\u0024\u003Eposition = position, \u003C\u0024\u003EstringName = stringName, \u003C\u0024\u003EallowRepeatDuringSession = allowRepeatDuringSession, \u003C\u0024\u003EbubbleDir = bubbleDir, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayBigCharacterQuoteAndWait(string prefabName, string audioName, Vector3 characterPosition, Notification.SpeechBubbleDirection bubbleDir = Notification.SpeechBubbleDirection.None, float testingDuration = 3f, float waitTimeScale = 1f, bool allowRepeatDuringSession = true, bool delayCardSoundSpells = false, bool persistCharacter = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayBigCharacterQuoteAndWait\u003Ec__Iterator109() { prefabName = prefabName, audioName = audioName, characterPosition = characterPosition, waitTimeScale = waitTimeScale, testingDuration = testingDuration, allowRepeatDuringSession = allowRepeatDuringSession, delayCardSoundSpells = delayCardSoundSpells, bubbleDir = bubbleDir, persistCharacter = persistCharacter, \u003C\u0024\u003EprefabName = prefabName, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EcharacterPosition = characterPosition, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EallowRepeatDuringSession = allowRepeatDuringSession, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EbubbleDir = bubbleDir, \u003C\u0024\u003EpersistCharacter = persistCharacter, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayBigCharacterQuoteAndWait(string prefabName, string audioName, float testingDuration = 3f, float waitTimeScale = 1f, bool allowRepeatDuringSession = true, bool delayCardSoundSpells = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayBigCharacterQuoteAndWait\u003Ec__Iterator10A() { prefabName = prefabName, audioName = audioName, waitTimeScale = waitTimeScale, testingDuration = testingDuration, allowRepeatDuringSession = allowRepeatDuringSession, delayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EprefabName = prefabName, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EallowRepeatDuringSession = allowRepeatDuringSession, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayBigCharacterQuoteAndWait(string prefabName, string audioName, string textID, float testingDuration = 3f, float waitTimeScale = 1f, bool allowRepeatDuringSession = true, bool delayCardSoundSpells = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayBigCharacterQuoteAndWait\u003Ec__Iterator10B() { prefabName = prefabName, audioName = audioName, textID = textID, waitTimeScale = waitTimeScale, testingDuration = testingDuration, allowRepeatDuringSession = allowRepeatDuringSession, delayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EprefabName = prefabName, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EtextID = textID, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EallowRepeatDuringSession = allowRepeatDuringSession, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayBigCharacterQuoteAndWaitOnce(string prefabName, string audioName, float testingDuration = 3f, float waitTimeScale = 1f, bool delayCardSoundSpells = false, bool persistCharacter = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayBigCharacterQuoteAndWaitOnce\u003Ec__Iterator10C() { prefabName = prefabName, audioName = audioName, waitTimeScale = waitTimeScale, testingDuration = testingDuration, delayCardSoundSpells = delayCardSoundSpells, persistCharacter = persistCharacter, \u003C\u0024\u003EprefabName = prefabName, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EpersistCharacter = persistCharacter, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayBigCharacterQuoteAndWaitOnce(string prefabName, string audioName, string textID, float testingDuration = 3f, float waitTimeScale = 1f, bool delayCardSoundSpells = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayBigCharacterQuoteAndWaitOnce\u003Ec__Iterator10D() { prefabName = prefabName, audioName = audioName, textID = textID, waitTimeScale = waitTimeScale, testingDuration = testingDuration, delayCardSoundSpells = delayCardSoundSpells, \u003C\u0024\u003EprefabName = prefabName, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EtextID = textID, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EtestingDuration = testingDuration, \u003C\u0024\u003EdelayCardSoundSpells = delayCardSoundSpells, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator WaitForCardSoundSpellDelay(float sec)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CWaitForCardSoundSpellDelay\u003Ec__Iterator10E() { sec = sec, \u003C\u0024\u003Esec = sec, \u003C\u003Ef__this = this };
  }

  protected void ShowBubble(string textKey, Notification.SpeechBubbleDirection direction, Actor speakingActor, bool destroyOnNewNotification, float duration, bool parentToActor, float bubbleScale = 0.0f)
  {
    NotificationManager notificationManager = NotificationManager.Get();
    Notification speechBubble;
    if (SoundUtils.CanDetectVolume() && SoundUtils.IsVoiceAudible() && GameMgr.Get().IsTutorial())
    {
      speechBubble = notificationManager.CreateSpeechBubble(string.Empty, direction, speakingActor, destroyOnNewNotification, parentToActor, 0.0f);
      float num = 0.25f;
      Vector3 vector3 = new Vector3(speechBubble.transform.localScale.x * num, speechBubble.transform.localScale.y * num, speechBubble.transform.localScale.z * num);
      speechBubble.transform.localScale = vector3;
    }
    else
      speechBubble = notificationManager.CreateSpeechBubble(GameStrings.Get(textKey), direction, speakingActor, destroyOnNewNotification, parentToActor, bubbleScale);
    if ((double) duration <= 0.0)
      return;
    notificationManager.DestroyNotification(speechBubble, duration);
  }

  protected MissionEntity.ShouldPlayValue InternalShouldPlayOpeningLine()
  {
    return MissionEntity.ShouldPlayValue.Always;
  }

  protected MissionEntity.ShouldPlayValue InternalShouldPlayBossLine()
  {
    return MissionEntity.ShouldPlayValue.Always;
  }

  protected MissionEntity.ShouldPlayValue InternalShouldPlayMissionFlavorLine()
  {
    return this.IsHeroic() ? MissionEntity.ShouldPlayValue.Once : MissionEntity.ShouldPlayValue.Always;
  }

  protected MissionEntity.ShouldPlayValue InternalShouldPlayAdventureFlavorLine()
  {
    if (this.IsHeroic())
      return MissionEntity.ShouldPlayValue.Once;
    return this.IsClassChallenge() ? MissionEntity.ShouldPlayValue.Never : MissionEntity.ShouldPlayValue.Always;
  }

  protected MissionEntity.ShouldPlayValue InternalShouldPlayClosingLine()
  {
    return this.IsClassChallenge() ? MissionEntity.ShouldPlayValue.Never : MissionEntity.ShouldPlayValue.Always;
  }

  protected MissionEntity.ShouldPlayValue InternalShouldPlayEasterEggLine()
  {
    return MissionEntity.ShouldPlayValue.Always;
  }

  protected MissionEntity.ShouldPlayValue InternalShouldPlayCriticalLine()
  {
    return MissionEntity.ShouldPlayValue.Always;
  }

  protected Notification.SpeechBubbleDirection GetDirection(Actor actor)
  {
    return actor.GetEntity().IsControlledByFriendlySidePlayer() ? Notification.SpeechBubbleDirection.BottomLeft : Notification.SpeechBubbleDirection.TopRight;
  }

  [DebuggerHidden]
  protected IEnumerator PlayLittleCharacterLine(string speaker, string line, MissionEntity.ShouldPlay shouldPlay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayLittleCharacterLine\u003Ec__Iterator10F() { shouldPlay = shouldPlay, speaker = speaker, line = line, \u003C\u0024\u003EshouldPlay = shouldPlay, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayLine(string speaker, string line, MissionEntity.ShouldPlay shouldPlay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayLine\u003Ec__Iterator110() { speaker = speaker, line = line, shouldPlay = shouldPlay, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u0024\u003EshouldPlay = shouldPlay, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayLine(string speaker, string line, MissionEntity.ShouldPlay shouldPlay, Vector3 quotePosition, Notification.SpeechBubbleDirection direction = Notification.SpeechBubbleDirection.None, bool persistCharacter = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayLine\u003Ec__Iterator111() { shouldPlay = shouldPlay, speaker = speaker, line = line, quotePosition = quotePosition, direction = direction, persistCharacter = persistCharacter, \u003C\u0024\u003EshouldPlay = shouldPlay, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u0024\u003EquotePosition = quotePosition, \u003C\u0024\u003Edirection = direction, \u003C\u0024\u003EpersistCharacter = persistCharacter, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayLine(Actor speaker, string line, MissionEntity.ShouldPlay shouldPlay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayLine\u003Ec__Iterator112() { speaker = speaker, shouldPlay = shouldPlay, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003EshouldPlay = shouldPlay, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  protected bool ShouldPlayLine(string line, MissionEntity.ShouldPlay shouldPlay)
  {
    bool flag = false;
    switch (shouldPlay())
    {
      case MissionEntity.ShouldPlayValue.Once:
        if (DemoMgr.Get().IsExpoDemo() || !NotificationManager.Get().HasSoundPlayedThisSession(line))
        {
          flag = true;
          break;
        }
        break;
      case MissionEntity.ShouldPlayValue.Always:
        flag = true;
        break;
    }
    return flag;
  }

  [DebuggerHidden]
  protected IEnumerator PlayOpeningLine(string speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayOpeningLine\u003Ec__Iterator113() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayOpeningLine(Actor speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayOpeningLine\u003Ec__Iterator114() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayBossLine(string speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayBossLine\u003Ec__Iterator115() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayBossLine(Actor speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayBossLine\u003Ec__Iterator116() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayMissionFlavorLine(string speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayMissionFlavorLine\u003Ec__Iterator117() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayMissionFlavorLine(string speaker, string line, Vector3 quotePosition, Notification.SpeechBubbleDirection direction = Notification.SpeechBubbleDirection.None, bool persistCharacter = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayMissionFlavorLine\u003Ec__Iterator118() { speaker = speaker, line = line, quotePosition = quotePosition, direction = direction, persistCharacter = persistCharacter, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u0024\u003EquotePosition = quotePosition, \u003C\u0024\u003Edirection = direction, \u003C\u0024\u003EpersistCharacter = persistCharacter, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayMissionFlavorLine(Actor speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayMissionFlavorLine\u003Ec__Iterator119() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayAdventureFlavorLine(string speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayAdventureFlavorLine\u003Ec__Iterator11A() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayAdventureFlavorLine(Actor speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayAdventureFlavorLine\u003Ec__Iterator11B() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayClosingLine(string speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayClosingLine\u003Ec__Iterator11C() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayClosingLine(Actor speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayClosingLine\u003Ec__Iterator11D() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayEasterEggLine(string speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayEasterEggLine\u003Ec__Iterator11E() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayEasterEggLine(Actor speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayEasterEggLine\u003Ec__Iterator11F() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayCriticalLine(string speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayCriticalLine\u003Ec__Iterator120() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator PlayCriticalLine(Actor speaker, string line)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CPlayCriticalLine\u003Ec__Iterator121() { speaker = speaker, line = line, \u003C\u0024\u003Espeaker = speaker, \u003C\u0024\u003Eline = line, \u003C\u003Ef__this = this };
  }

  protected bool ShouldPlayClosingLine(string line)
  {
    return this.ShouldPlayLine(line, new MissionEntity.ShouldPlay(this.InternalShouldPlayClosingLine));
  }

  protected bool ShouldPlayCriticalLine(string line)
  {
    return this.ShouldPlayLine(line, new MissionEntity.ShouldPlay(this.InternalShouldPlayCriticalLine));
  }

  protected bool ShouldPlayAdventureFlavorLine(string line)
  {
    return this.ShouldPlayLine(line, new MissionEntity.ShouldPlay(this.InternalShouldPlayAdventureFlavorLine));
  }

  protected bool ShouldPlayMissionFlavorLine(string line)
  {
    return this.ShouldPlayLine(line, new MissionEntity.ShouldPlay(this.InternalShouldPlayMissionFlavorLine));
  }

  protected bool ShouldPlayBossLine(string line)
  {
    return this.ShouldPlayLine(line, new MissionEntity.ShouldPlay(this.InternalShouldPlayBossLine));
  }

  protected bool ShouldPlayEasterEggLine(string line)
  {
    return this.ShouldPlayLine(line, new MissionEntity.ShouldPlay(this.InternalShouldPlayEasterEggLine));
  }

  protected bool ShouldPlayOpeningLine(string line)
  {
    return this.ShouldPlayLine(line, new MissionEntity.ShouldPlay(this.InternalShouldPlayOpeningLine));
  }

  protected virtual void InitEmoteResponses()
  {
  }

  [DebuggerHidden]
  protected IEnumerator HandlePlayerEmoteWithTiming(EmoteType emoteType, CardSoundSpell emoteSpell)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MissionEntity.\u003CHandlePlayerEmoteWithTiming\u003Ec__Iterator122() { emoteSpell = emoteSpell, emoteType = emoteType, \u003C\u0024\u003EemoteSpell = emoteSpell, \u003C\u0024\u003EemoteType = emoteType, \u003C\u003Ef__this = this };
  }

  protected virtual void PlayEmoteResponse(EmoteType emoteType, CardSoundSpell emoteSpell)
  {
    Actor actor = GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor();
    using (List<MissionEntity.EmoteResponseGroup>.Enumerator enumerator = this.m_emoteResponseGroups.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MissionEntity.EmoteResponseGroup current = enumerator.Current;
        if (current.m_responses.Count != 0 && current.m_triggers.Contains(emoteType))
        {
          int responseIndex = current.m_responseIndex;
          MissionEntity.EmoteResponse response = current.m_responses[responseIndex];
          Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech(response.m_soundName, response.m_stringTag, Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
          this.CycleNextResponseGroupIndex(current);
        }
      }
    }
  }

  protected virtual void CycleNextResponseGroupIndex(MissionEntity.EmoteResponseGroup responseGroup)
  {
    if (responseGroup.m_responseIndex == responseGroup.m_responses.Count - 1)
      responseGroup.m_responseIndex = 0;
    else
      ++responseGroup.m_responseIndex;
  }

  protected class EmoteResponse
  {
    public string m_soundName;
    public string m_stringTag;
  }

  protected class EmoteResponseGroup
  {
    public List<EmoteType> m_triggers = new List<EmoteType>();
    public List<MissionEntity.EmoteResponse> m_responses = new List<MissionEntity.EmoteResponse>();
    public int m_responseIndex;
  }

  protected enum ShouldPlayValue
  {
    Never,
    Once,
    Always,
  }

  protected delegate MissionEntity.ShouldPlayValue ShouldPlay();
}
