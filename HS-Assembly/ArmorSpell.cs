﻿// Decompiled with JetBrains decompiler
// Type: ArmorSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArmorSpell : Spell
{
  public UberText m_ArmorText;
  private int m_armor;

  public int GetArmor()
  {
    return this.m_armor;
  }

  public void SetArmor(int armor)
  {
    this.m_armor = armor;
    this.UpdateArmorText();
  }

  private void UpdateArmorText()
  {
    if ((Object) this.m_ArmorText == (Object) null)
      return;
    string empty = this.m_armor.ToString();
    if (this.m_armor == 0)
      empty = string.Empty;
    this.m_ArmorText.Text = empty;
  }
}
