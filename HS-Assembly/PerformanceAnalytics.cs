﻿// Decompiled with JetBrains decompiler
// Type: PerformanceAnalytics
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using Unity.Performance;
using UnityEngine;

public class PerformanceAnalytics : MonoBehaviour
{
  private float m_initStartTime;
  private PerfRecorder m_perfRecorder;
  private bool m_isRecordingFPS;
  private static PerformanceAnalytics s_instance;

  public static PerformanceAnalytics Get()
  {
    return PerformanceAnalytics.s_instance;
  }

  private void Awake()
  {
    PerformanceAnalytics.s_instance = this;
    this.BeginStartupTimmer();
  }

  private void Start()
  {
    this.m_perfRecorder = this.GetComponent<PerfRecorder>();
    if (!(bool) ((Object) this.m_perfRecorder))
      this.m_perfRecorder = this.gameObject.AddComponent<PerfRecorder>();
    this.m_perfRecorder.SetBuildVersion("7.0");
    SceneMgr.Get().RegisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.ScenePreUnloadEvent));
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.SceneLoadedEvent));
  }

  public bool isRecordingFPS()
  {
    return this.m_isRecordingFPS;
  }

  public void BeginStartupTimmer()
  {
    this.m_initStartTime = Time.realtimeSinceStartup;
  }

  public void EndStartupTimmer()
  {
    float num1 = (float) (((double) Time.realtimeSinceStartup - (double) this.m_initStartTime) * 1000.0);
    Log.Kyle.Print("Startup time: {0}", (object) num1);
    int num2 = (int) UnityEngine.Analytics.Analytics.CustomEvent("perfGenericTimer", (IDictionary<string, object>) new Dictionary<string, object>() { { "build_version", (object) "7.0" }, { "context", (object) "StartupTime" }, { "time_elapsed", (object) num1 }, { "plugin_version", (object) "v1.0.1" } });
  }

  private void StartRecordingGameplayFPS()
  {
    Log.Kyle.Print("Starting gameplay FPS recording");
    this.m_perfRecorder.BeginExperiment("Gameplay");
    this.m_isRecordingFPS = true;
  }

  private void StartRecordingCollectionManagerFPS()
  {
    Log.Kyle.Print("Starting collection manager FPS recording");
    this.m_perfRecorder.BeginExperiment("CollectionManager");
    this.m_isRecordingFPS = true;
  }

  private void EndRecordingFPS()
  {
    Log.Kyle.Print("FPS recording stopped");
    this.m_perfRecorder.EndExperiment();
    this.m_isRecordingFPS = false;
  }

  private void SceneLoadedEvent(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    Log.Kyle.Print("RecordFPS.SceneLoadedEvent() - prevMode={0} nextMode={1}", new object[2]
    {
      (object) prevMode,
      (object) SceneMgr.Get().GetMode()
    });
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY)
      this.StartRecordingGameplayFPS();
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.COLLECTIONMANAGER)
      return;
    this.StartRecordingCollectionManagerFPS();
  }

  private void ScenePreUnloadEvent(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    Log.Kyle.Print("RecordFPS.ScenePreUnloadEvent() - prevMode={0} nextMode={1}", new object[2]
    {
      (object) prevMode,
      (object) SceneMgr.Get().GetMode()
    });
    if (!this.m_isRecordingFPS)
      return;
    this.EndRecordingFPS();
  }
}
