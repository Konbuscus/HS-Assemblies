﻿// Decompiled with JetBrains decompiler
// Type: ZoneChange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ZoneChange
{
  private ZoneChangeList m_parentList;
  private PowerTask m_powerTask;
  private Entity m_entity;
  private Zone m_sourceZone;
  private TAG_ZONE m_sourceZoneTag;
  private int? m_sourcePos;
  private Zone m_destinationZone;
  private TAG_ZONE m_destinationZoneTag;
  private int? m_destinationPos;
  private int? m_destinationControllerId;

  public ZoneChangeList GetParentList()
  {
    return this.m_parentList;
  }

  public void SetParentList(ZoneChangeList parentList)
  {
    this.m_parentList = parentList;
  }

  public PowerTask GetPowerTask()
  {
    return this.m_powerTask;
  }

  public void SetPowerTask(PowerTask powerTask)
  {
    this.m_powerTask = powerTask;
  }

  public Entity GetEntity()
  {
    return this.m_entity;
  }

  public void SetEntity(Entity entity)
  {
    this.m_entity = entity;
  }

  public Zone GetDestinationZone()
  {
    return this.m_destinationZone;
  }

  public void SetDestinationZone(Zone zone)
  {
    this.m_destinationZone = zone;
  }

  public TAG_ZONE GetDestinationZoneTag()
  {
    return this.m_destinationZoneTag;
  }

  public void SetDestinationZoneTag(TAG_ZONE tag)
  {
    this.m_destinationZoneTag = tag;
  }

  public int GetDestinationPosition()
  {
    if (!this.m_destinationPos.HasValue)
      return 0;
    return this.m_destinationPos.Value;
  }

  public void SetDestinationPosition(int pos)
  {
    this.m_destinationPos = new int?(pos);
  }

  public int GetDestinationControllerId()
  {
    if (!this.m_destinationControllerId.HasValue)
      return 0;
    return this.m_destinationControllerId.Value;
  }

  public void SetDestinationControllerId(int controllerId)
  {
    this.m_destinationControllerId = new int?(controllerId);
  }

  public Zone GetSourceZone()
  {
    return this.m_sourceZone;
  }

  public void SetSourceZone(Zone zone)
  {
    this.m_sourceZone = zone;
  }

  public TAG_ZONE GetSourceZoneTag()
  {
    return this.m_sourceZoneTag;
  }

  public void SetSourceZoneTag(TAG_ZONE tag)
  {
    this.m_sourceZoneTag = tag;
  }

  public int GetSourcePosition()
  {
    if (!this.m_sourcePos.HasValue)
      return 0;
    return this.m_sourcePos.Value;
  }

  public void SetSourcePosition(int pos)
  {
    this.m_sourcePos = new int?(pos);
  }

  public bool HasSourceZone()
  {
    return (Object) this.m_sourceZone != (Object) null;
  }

  public bool HasSourceZoneTag()
  {
    return this.m_sourceZoneTag != TAG_ZONE.INVALID;
  }

  public bool HasSourcePosition()
  {
    return this.m_sourcePos.HasValue;
  }

  public bool HasSourceData()
  {
    return this.HasSourceZoneTag() || this.HasSourcePosition();
  }

  public bool HasDestinationZone()
  {
    return (Object) this.m_destinationZone != (Object) null;
  }

  public bool HasDestinationZoneTag()
  {
    return this.m_destinationZoneTag != TAG_ZONE.INVALID;
  }

  public bool HasDestinationPosition()
  {
    return this.m_destinationPos.HasValue;
  }

  public bool HasDestinationControllerId()
  {
    return this.m_destinationControllerId.HasValue;
  }

  public bool HasDestinationData()
  {
    return this.HasDestinationZoneTag() || this.HasDestinationPosition() || this.HasDestinationControllerId();
  }

  public bool HasDestinationZoneChange()
  {
    return this.HasDestinationZoneTag() || this.HasDestinationControllerId();
  }

  public override string ToString()
  {
    return string.Format("powerTask=[{0}] entity={1} srcZoneTag={2} srcPos={3} dstZoneTag={4} dstPos={5}", (object) this.m_powerTask, (object) this.m_entity, (object) this.m_sourceZoneTag, (object) this.m_sourcePos, (object) this.m_destinationZoneTag, (object) this.m_destinationPos);
  }
}
