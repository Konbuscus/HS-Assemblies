﻿// Decompiled with JetBrains decompiler
// Type: TagMap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class TagMap
{
  private Map<int, int> m_values = new Map<int, int>();

  public void SetTag(int tag, int tagValue)
  {
    this.m_values[tag] = tagValue;
  }

  public void SetTag(GAME_TAG tag, int tagValue)
  {
    this.SetTag((int) tag, tagValue);
  }

  public void SetTag<TagEnum>(GAME_TAG tag, TagEnum tagValue)
  {
    this.SetTag((int) tag, Convert.ToInt32((object) tagValue));
  }

  public void SetTags(Map<int, int> tagMap)
  {
    using (Map<int, int>.Enumerator enumerator = tagMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, int> current = enumerator.Current;
        this.SetTag(current.Key, current.Value);
      }
    }
  }

  public void SetTags(Map<GAME_TAG, int> tagMap)
  {
    using (Map<GAME_TAG, int>.Enumerator enumerator = tagMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<GAME_TAG, int> current = enumerator.Current;
        this.SetTag(current.Key, current.Value);
      }
    }
  }

  public void SetTags(List<Network.Entity.Tag> tags)
  {
    using (List<Network.Entity.Tag>.Enumerator enumerator = tags.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        this.SetTag(current.Name, current.Value);
      }
    }
  }

  public Map<int, int> GetMap()
  {
    return this.m_values;
  }

  public int GetTag(int tag)
  {
    int num = 0;
    this.m_values.TryGetValue(tag, out num);
    return num;
  }

  public TagEnum GetTag<TagEnum>(GAME_TAG enumTag)
  {
    return (TagEnum) Enum.ToObject(typeof (TagEnum), this.GetTag(Convert.ToInt32((object) enumTag)));
  }

  public int GetTag(GAME_TAG enumTag)
  {
    return this.GetTag(Convert.ToInt32((object) enumTag));
  }

  public bool HasTag(int tag)
  {
    int num = 0;
    if (!this.m_values.TryGetValue(tag, out num))
      return false;
    return num > 0;
  }

  public bool HasTag<TagEnum>(GAME_TAG tag)
  {
    return Convert.ToUInt32((object) this.GetTag<TagEnum>(tag)) > 0U;
  }

  public void Replace(TagMap tags)
  {
    this.Clear();
    this.SetTags(tags.m_values);
  }

  public void Replace(List<Network.Entity.Tag> tags)
  {
    this.Clear();
    this.SetTags(tags);
  }

  public void Clear()
  {
    this.m_values = new Map<int, int>();
  }

  public TagDeltaList CreateDeltas(List<Network.Entity.Tag> comp)
  {
    TagDeltaList tagDeltaList = new TagDeltaList();
    using (List<Network.Entity.Tag>.Enumerator enumerator = comp.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        int name = current.Name;
        int prev = 0;
        this.m_values.TryGetValue(name, out prev);
        int curr = current.Value;
        if (prev != curr)
          tagDeltaList.Add(name, prev, curr);
      }
    }
    return tagDeltaList;
  }

  public bool TryGetValue(int tag, out int value)
  {
    return this.m_values.TryGetValue(tag, out value);
  }
}
