﻿// Decompiled with JetBrains decompiler
// Type: PageTurn
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class PageTurn : MonoBehaviour
{
  private readonly string FRONT_PAGE_NAME = "PageTurnFront";
  private readonly string BACK_PAGE_NAME = "PageTurnBack";
  private readonly string WAIT_THEN_COMPLETE_PAGE_TURN_COROUTINE = "WaitThenCompletePageTurn";
  private readonly string PAGE_TURN_LEFT_ANIM = "PageTurnLeft";
  private readonly string PAGE_TURN_RIGHT_ANIM = "PageTurnRight";
  private readonly string PAGE_TURN_MAT_ANIM = "PageTurnMaterialAnimation";
  public float m_TurnLeftSpeed = 1.65f;
  public float m_TurnRightSpeed = 1.65f;
  private float m_RenderOffset = 500f;
  public Shader m_MaskShader;
  private Bounds m_RenderBounds;
  private Camera m_OffscreenPageTurnCamera;
  private Camera m_OffscreenPageTurnMaskCamera;
  private GameObject m_OffscreenPageTurnCameraGO;
  private RenderTexture m_TempRenderBuffer;
  private RenderTexture m_TempMaskBuffer;
  private GameObject m_MeshGameObject;
  private GameObject m_FrontPageGameObject;
  private GameObject m_BackPageGameObject;
  private GameObject m_TheBoxOuterFrame;
  private Vector3 m_initialPosition;

  private void Awake()
  {
    this.m_initialPosition = this.transform.position;
    Transform transform1 = this.transform.Find(this.FRONT_PAGE_NAME);
    if ((Object) transform1 != (Object) null)
      this.m_FrontPageGameObject = transform1.gameObject;
    if ((Object) this.m_FrontPageGameObject == (Object) null)
      UnityEngine.Debug.LogError((object) ("Failed to find " + this.FRONT_PAGE_NAME + " Object."));
    Transform transform2 = this.transform.Find(this.BACK_PAGE_NAME);
    if ((Object) transform2 != (Object) null)
      this.m_BackPageGameObject = transform2.gameObject;
    if ((Object) this.m_BackPageGameObject == (Object) null)
      UnityEngine.Debug.LogError((object) ("Failed to find " + this.BACK_PAGE_NAME + " Object."));
    this.Show(false);
    this.m_TheBoxOuterFrame = Box.Get().m_OuterFrame;
    this.CreateCamera();
    this.CreateRenderTexture();
    this.SetupMaterial();
  }

  protected void OnEnable()
  {
    if ((Object) this.m_OffscreenPageTurnCameraGO != (Object) null)
      this.CreateCamera();
    if (!((Object) this.m_TempRenderBuffer != (Object) null) && !((Object) this.m_TempMaskBuffer != (Object) null))
      return;
    this.CreateRenderTexture();
    this.SetupMaterial();
  }

  protected void OnDisable()
  {
    if ((Object) this.m_TempRenderBuffer != (Object) null)
      Object.Destroy((Object) this.m_TempRenderBuffer);
    if ((Object) this.m_TempMaskBuffer != (Object) null)
      Object.Destroy((Object) this.m_TempMaskBuffer);
    if ((Object) this.m_OffscreenPageTurnCameraGO != (Object) null)
      Object.Destroy((Object) this.m_OffscreenPageTurnCameraGO);
    if ((Object) this.m_OffscreenPageTurnCamera != (Object) null)
      Object.Destroy((Object) this.m_OffscreenPageTurnCamera);
    if (!((Object) this.m_OffscreenPageTurnMaskCamera != (Object) null))
      return;
    Object.Destroy((Object) this.m_OffscreenPageTurnMaskCamera);
  }

  public void TurnRight(GameObject flippingPage, GameObject otherPage)
  {
    this.TurnRight(flippingPage, otherPage, (PageTurn.DelOnPageTurnComplete) null);
  }

  public void TurnRight(GameObject flippingPage, GameObject otherPage, PageTurn.DelOnPageTurnComplete callback)
  {
    this.TurnRight(flippingPage, otherPage, callback, (object) null);
  }

  public void TurnRight(GameObject flippingPage, GameObject otherPage, PageTurn.DelOnPageTurnComplete callback, object callbackData)
  {
    this.Render(flippingPage);
    Time.captureFramerate = GraphicsManager.Get().RenderQualityLevel != GraphicsQuality.Low ? (GraphicsManager.Get().RenderQualityLevel != GraphicsQuality.Medium ? 30 : 24) : 18;
    this.GetComponent<Animation>().Stop(this.PAGE_TURN_RIGHT_ANIM);
    this.GetComponent<Animation>()[this.PAGE_TURN_RIGHT_ANIM].time = 0.0f;
    this.GetComponent<Animation>()[this.PAGE_TURN_RIGHT_ANIM].speed = this.m_TurnRightSpeed;
    this.GetComponent<Animation>().Play(this.PAGE_TURN_RIGHT_ANIM);
    this.m_FrontPageGameObject.GetComponent<Renderer>().material.SetFloat("_Alpha", 1f);
    this.m_BackPageGameObject.GetComponent<Renderer>().material.SetFloat("_Alpha", 1f);
    float num = this.GetComponent<Animation>()[this.PAGE_TURN_RIGHT_ANIM].length / this.m_TurnRightSpeed;
    PageTurn.PageTurningData pageTurningData = new PageTurn.PageTurningData()
    {
      m_secondsToWait = num,
      m_callback = callback,
      m_callbackData = callbackData
    };
    this.StopCoroutine(this.WAIT_THEN_COMPLETE_PAGE_TURN_COROUTINE);
    this.StartCoroutine(this.WAIT_THEN_COMPLETE_PAGE_TURN_COROUTINE, (object) pageTurningData);
  }

  public void TurnLeft(GameObject flippingPage, GameObject otherPage)
  {
    this.TurnLeft(flippingPage, otherPage, (PageTurn.DelOnPageTurnComplete) null);
  }

  public void TurnLeft(GameObject flippingPage, GameObject otherPage, PageTurn.DelOnPageTurnComplete callback)
  {
    this.TurnLeft(flippingPage, otherPage, callback, (object) null);
  }

  public void TurnLeft(GameObject flippingPage, GameObject otherPage, PageTurn.DelOnPageTurnComplete callback, object callbackData)
  {
    PageTurn.TurnPageData turnPageData = new PageTurn.TurnPageData();
    turnPageData.flippingPage = flippingPage;
    turnPageData.otherPage = otherPage;
    turnPageData.callback = callback;
    turnPageData.callbackData = callbackData;
    this.StopCoroutine("TurnLeftPage");
    this.StartCoroutine("TurnLeftPage", (object) turnPageData);
  }

  [DebuggerHidden]
  private IEnumerator TurnLeftPage(PageTurn.TurnPageData pageData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PageTurn.\u003CTurnLeftPage\u003Ec__Iterator62()
    {
      pageData = pageData,
      \u003C\u0024\u003EpageData = pageData,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator FinishTurnLeftPage(PageTurn.PageTurningData pageTurningData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PageTurn.\u003CFinishTurnLeftPage\u003Ec__Iterator63()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void CreateCamera()
  {
    if ((Object) this.m_OffscreenPageTurnCameraGO == (Object) null)
    {
      if ((Object) this.m_OffscreenPageTurnCamera != (Object) null)
        Object.DestroyImmediate((Object) this.m_OffscreenPageTurnCamera);
      this.m_OffscreenPageTurnCameraGO = new GameObject();
      this.m_OffscreenPageTurnCamera = this.m_OffscreenPageTurnCameraGO.AddComponent<Camera>();
      this.m_OffscreenPageTurnCameraGO.name = this.name + "_OffScreenPageTurnCamera";
      this.SetupCamera(this.m_OffscreenPageTurnCamera);
    }
    if (!((Object) this.m_OffscreenPageTurnMaskCamera == (Object) null))
      return;
    GameObject gameObject = new GameObject();
    this.m_OffscreenPageTurnMaskCamera = gameObject.AddComponent<Camera>();
    gameObject.name = this.name + "_OffScreenPageTurnMaskCamera";
    this.SetupCamera(this.m_OffscreenPageTurnMaskCamera);
    this.m_OffscreenPageTurnMaskCamera.SetReplacementShader(this.m_MaskShader, "BasePage");
  }

  private void SetupCamera(Camera camera)
  {
    camera.orthographic = true;
    camera.orthographicSize = PageTurn.GetWorldScale(this.m_FrontPageGameObject.transform).x / 2f;
    camera.transform.parent = this.transform;
    camera.nearClipPlane = -20f;
    camera.farClipPlane = 20f;
    camera.depth = !((Object) Camera.main == (Object) null) ? Camera.main.depth + 100f : 0.0f;
    camera.backgroundColor = Color.black;
    camera.clearFlags = CameraClearFlags.Color;
    camera.cullingMask = GameLayer.Default.LayerBit() | GameLayer.CardRaycast.LayerBit();
    camera.enabled = false;
    camera.renderingPath = RenderingPath.Forward;
    camera.transform.Rotate(90f, 0.0f, 0.0f);
    SceneUtils.SetHideFlags((Object) camera, HideFlags.HideAndDontSave);
  }

  private void CreateRenderTexture()
  {
    int num1 = Screen.currentResolution.width;
    if (num1 < Screen.currentResolution.height)
      num1 = Screen.currentResolution.height;
    int num2 = 512;
    if (num1 > 640)
      num2 = 1024;
    if (num1 > 1280)
      num2 = 2048;
    if (num1 > 2500)
      num2 = 4096;
    GraphicsQuality renderQualityLevel = GraphicsManager.Get().RenderQualityLevel;
    switch (renderQualityLevel)
    {
      case GraphicsQuality.Medium:
        num2 = 1024;
        break;
      case GraphicsQuality.Low:
        num2 = 512;
        break;
    }
    if ((Object) this.m_TempRenderBuffer == (Object) null)
    {
      this.m_TempRenderBuffer = renderQualityLevel != GraphicsQuality.High ? (!SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGB1555) ? (renderQualityLevel != GraphicsQuality.Low || !SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGB4444) ? new RenderTexture(num2, num2, 16, RenderTextureFormat.Default, RenderTextureReadWrite.Default) : new RenderTexture(num2, num2, 16, RenderTextureFormat.ARGB4444, RenderTextureReadWrite.Default)) : new RenderTexture(num2, num2, 16, RenderTextureFormat.ARGB1555, RenderTextureReadWrite.Default)) : new RenderTexture(num2, num2, 16, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
      this.m_TempRenderBuffer.Create();
    }
    if ((Object) this.m_TempMaskBuffer == (Object) null)
    {
      this.m_TempMaskBuffer = renderQualityLevel != GraphicsQuality.High ? (!SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGB1555) ? (renderQualityLevel != GraphicsQuality.Low || !SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGB4444) ? new RenderTexture(num2, num2, 16, RenderTextureFormat.Default, RenderTextureReadWrite.Default) : new RenderTexture(num2, num2, 16, RenderTextureFormat.ARGB4444, RenderTextureReadWrite.Default)) : new RenderTexture(num2, num2, 16, RenderTextureFormat.ARGB1555, RenderTextureReadWrite.Default)) : new RenderTexture(num2, num2, 16, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
      this.m_TempMaskBuffer.Create();
    }
    if ((Object) this.m_OffscreenPageTurnCamera != (Object) null)
      this.m_OffscreenPageTurnCamera.targetTexture = this.m_TempRenderBuffer;
    if (!((Object) this.m_OffscreenPageTurnMaskCamera != (Object) null))
      return;
    this.m_OffscreenPageTurnMaskCamera.targetTexture = this.m_TempMaskBuffer;
  }

  private void Render(GameObject page)
  {
    this.Show(true);
    this.m_FrontPageGameObject.SetActive(true);
    this.m_BackPageGameObject.SetActive(true);
    this.m_OffscreenPageTurnCameraGO.transform.position = this.transform.position;
    bool enabled1 = this.m_FrontPageGameObject.GetComponent<Renderer>().enabled;
    bool enabled2 = this.m_BackPageGameObject.GetComponent<Renderer>().enabled;
    this.m_FrontPageGameObject.GetComponent<Renderer>().enabled = false;
    this.m_BackPageGameObject.GetComponent<Renderer>().enabled = false;
    bool activeSelf = this.m_TheBoxOuterFrame.activeSelf;
    this.m_TheBoxOuterFrame.SetActive(false);
    this.m_OffscreenPageTurnCamera.Render();
    this.m_OffscreenPageTurnMaskCamera.transform.position = this.transform.position;
    this.m_OffscreenPageTurnMaskCamera.RenderWithShader(this.m_MaskShader, "BasePage");
    this.m_FrontPageGameObject.GetComponent<Renderer>().enabled = enabled1;
    this.m_BackPageGameObject.GetComponent<Renderer>().enabled = enabled2;
    this.m_TheBoxOuterFrame.SetActive(activeSelf);
  }

  public void SetBackPageMaterial(Material material)
  {
    this.m_BackPageGameObject.GetComponent<Renderer>().material = material;
  }

  private void SetupMaterial()
  {
    Material material1 = this.m_FrontPageGameObject.GetComponent<Renderer>().material;
    material1.mainTexture = (Texture) this.m_TempRenderBuffer;
    material1.SetTexture("_MaskTex", (Texture) this.m_TempMaskBuffer);
    material1.renderQueue = 3001;
    Material material2 = this.m_BackPageGameObject.GetComponent<Renderer>().material;
    material2.SetTexture("_MaskTex", (Texture) this.m_TempMaskBuffer);
    material2.renderQueue = 3002;
  }

  private void Show(bool show)
  {
    this.transform.position = !show ? Vector3.right * this.m_RenderOffset : this.m_initialPosition;
  }

  [DebuggerHidden]
  private IEnumerator WaitThenCompletePageTurn(PageTurn.PageTurningData pageTurningData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PageTurn.\u003CWaitThenCompletePageTurn\u003Ec__Iterator64()
    {
      pageTurningData = pageTurningData,
      \u003C\u0024\u003EpageTurningData = pageTurningData,
      \u003C\u003Ef__this = this
    };
  }

  public static Vector3 GetWorldScale(Transform transform)
  {
    Vector3 a = transform.localScale;
    for (Transform parent = transform.parent; (Object) parent != (Object) null; parent = parent.parent)
      a = Vector3.Scale(a, parent.localScale);
    return a;
  }

  private class PageTurningData
  {
    public float m_secondsToWait;
    public PageTurn.DelOnPageTurnComplete m_callback;
    public object m_callbackData;
    public AnimationState m_animation;
  }

  private class TurnPageData
  {
    public GameObject flippingPage;
    public GameObject otherPage;
    public PageTurn.DelOnPageTurnComplete callback;
    public object callbackData;
  }

  public delegate void DelOnPageTurnComplete(object callbackData);
}
