﻿// Decompiled with JetBrains decompiler
// Type: CanvasAnchors
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class CanvasAnchors
{
  public Transform m_Center;
  public Transform m_Left;
  public Transform m_Right;
  public Transform m_Bottom;
  public Transform m_Top;
  public Transform m_BottomLeft;
  public Transform m_BottomRight;
  public Transform m_TopLeft;
  public Transform m_TopRight;

  public Transform GetAnchor(CanvasAnchor type)
  {
    if (type == CanvasAnchor.CENTER)
      return this.m_Center;
    if (type == CanvasAnchor.LEFT)
      return this.m_Left;
    if (type == CanvasAnchor.RIGHT)
      return this.m_Right;
    if (type == CanvasAnchor.BOTTOM)
      return this.m_Bottom;
    if (type == CanvasAnchor.TOP)
      return this.m_Top;
    if (type == CanvasAnchor.BOTTOM_LEFT)
      return this.m_BottomLeft;
    if (type == CanvasAnchor.BOTTOM_RIGHT)
      return this.m_BottomRight;
    if (type == CanvasAnchor.TOP_LEFT)
      return this.m_TopLeft;
    if (type == CanvasAnchor.TOP_RIGHT)
      return this.m_TopRight;
    return this.m_Center;
  }

  public void WillReset()
  {
    IEnumerator enumerator1 = this.m_Center.GetEnumerator();
    try
    {
      while (enumerator1.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) enumerator1.Current).gameObject);
    }
    finally
    {
      IDisposable disposable = enumerator1 as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    IEnumerator enumerator2 = this.m_Left.GetEnumerator();
    try
    {
      while (enumerator2.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) enumerator2.Current).gameObject);
    }
    finally
    {
      IDisposable disposable = enumerator2 as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    IEnumerator enumerator3 = this.m_Right.GetEnumerator();
    try
    {
      while (enumerator3.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) enumerator3.Current).gameObject);
    }
    finally
    {
      IDisposable disposable = enumerator3 as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    IEnumerator enumerator4 = this.m_Bottom.GetEnumerator();
    try
    {
      while (enumerator4.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) enumerator4.Current).gameObject);
    }
    finally
    {
      IDisposable disposable = enumerator4 as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    IEnumerator enumerator5 = this.m_Top.GetEnumerator();
    try
    {
      while (enumerator5.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) enumerator5.Current).gameObject);
    }
    finally
    {
      IDisposable disposable = enumerator5 as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    IEnumerator enumerator6 = this.m_BottomLeft.GetEnumerator();
    try
    {
      while (enumerator6.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) enumerator6.Current).gameObject);
    }
    finally
    {
      IDisposable disposable = enumerator6 as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    IEnumerator enumerator7 = this.m_BottomRight.GetEnumerator();
    try
    {
      while (enumerator7.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) enumerator7.Current).gameObject);
    }
    finally
    {
      IDisposable disposable = enumerator7 as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    IEnumerator enumerator8 = this.m_TopLeft.GetEnumerator();
    try
    {
      while (enumerator8.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) enumerator8.Current).gameObject);
    }
    finally
    {
      IDisposable disposable = enumerator8 as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    IEnumerator enumerator9 = this.m_TopRight.GetEnumerator();
    try
    {
      while (enumerator9.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) enumerator9.Current).gameObject);
    }
    finally
    {
      IDisposable disposable = enumerator9 as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
  }
}
