﻿// Decompiled with JetBrains decompiler
// Type: NoGTAPPTransactionData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;

public class NoGTAPPTransactionData
{
  public ProductType Product { get; set; }

  public int ProductData { get; set; }

  public int Quantity { get; set; }

  public NoGTAPPTransactionData()
  {
    this.Product = ProductType.PRODUCT_TYPE_UNKNOWN;
    this.ProductData = 0;
    this.Quantity = 0;
  }
}
