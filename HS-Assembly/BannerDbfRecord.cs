﻿// Decompiled with JetBrains decompiler
// Type: BannerDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BannerDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private DbfLocValue m_Text;
  [SerializeField]
  private string m_Prefab;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("TEXT", "")]
  public DbfLocValue Text
  {
    get
    {
      return this.m_Text;
    }
  }

  [DbfField("PREFAB", "")]
  public string Prefab
  {
    get
    {
      return this.m_Prefab;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    BannerDbfAsset bannerDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (BannerDbfAsset)) as BannerDbfAsset;
    if ((UnityEngine.Object) bannerDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("BannerDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < bannerDbfAsset.Records.Count; ++index)
      bannerDbfAsset.Records[index].StripUnusedLocales();
    records = (object) bannerDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Text.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetText(DbfLocValue v)
  {
    this.m_Text = v;
    v.SetDebugInfo(this.ID, "TEXT");
  }

  public void SetPrefab(string v)
  {
    this.m_Prefab = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (BannerDbfRecord.\u003C\u003Ef__switch\u0024map14 == null)
      {
        // ISSUE: reference to a compiler-generated field
        BannerDbfRecord.\u003C\u003Ef__switch\u0024map14 = new Dictionary<string, int>(4)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TEXT",
            2
          },
          {
            "PREFAB",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (BannerDbfRecord.\u003C\u003Ef__switch\u0024map14.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Text;
          case 3:
            return (object) this.Prefab;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (BannerDbfRecord.\u003C\u003Ef__switch\u0024map15 == null)
    {
      // ISSUE: reference to a compiler-generated field
      BannerDbfRecord.\u003C\u003Ef__switch\u0024map15 = new Dictionary<string, int>(4)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "TEXT",
          2
        },
        {
          "PREFAB",
          3
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!BannerDbfRecord.\u003C\u003Ef__switch\u0024map15.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetText((DbfLocValue) val);
        break;
      case 3:
        this.SetPrefab((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (BannerDbfRecord.\u003C\u003Ef__switch\u0024map16 == null)
      {
        // ISSUE: reference to a compiler-generated field
        BannerDbfRecord.\u003C\u003Ef__switch\u0024map16 = new Dictionary<string, int>(4)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TEXT",
            2
          },
          {
            "PREFAB",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (BannerDbfRecord.\u003C\u003Ef__switch\u0024map16.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (DbfLocValue);
          case 3:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
