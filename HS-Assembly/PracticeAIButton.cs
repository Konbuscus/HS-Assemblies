﻿// Decompiled with JetBrains decompiler
// Type: PracticeAIButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class PracticeAIButton : PegUIElement
{
  public int m_PortraitMaterialIdx = -1;
  private readonly string FLIP_COROUTINE = "WaitThenFlip";
  private readonly Vector3 GLOW_QUAD_NORMAL_LOCAL_POS = new Vector3(-0.1953466f, 1.336676f, 0.00721521f);
  private readonly Vector3 GLOW_QUAD_FLIPPED_LOCAL_POS = new Vector3(-0.1953466f, -1.336676f, 0.00721521f);
  private const float FLIPPED_X_ROTATION = 180f;
  private const float NORMAL_X_ROTATION = 0.0f;
  public UberText m_name;
  public UberText m_backsideName;
  public GameObject m_frontCover;
  public GameObject m_backsideCover;
  public HighlightState m_highlight;
  public GameObject m_unlockEffect;
  public GameObject m_questBang;
  public GameObject m_rootObject;
  public Transform m_upBone;
  public Transform m_downBone;
  public Transform m_coveredBone;
  private int m_missionID;
  private long m_deckID;
  private bool m_covered;
  private bool m_locked;
  private bool m_infoSet;
  private bool m_usingBackside;
  private TAG_CLASS m_class;

  public int GetMissionID()
  {
    return this.m_missionID;
  }

  public long GetDeckID()
  {
    return this.m_deckID;
  }

  public TAG_CLASS GetClass()
  {
    return this.m_class;
  }

  public void PlayUnlockGlow()
  {
    this.m_unlockEffect.GetComponent<Animation>().Play("AITileGlow");
  }

  public void Lock(bool locked)
  {
    this.m_locked = locked;
    float num = !this.m_locked ? 0.0f : 1f;
    this.SetEnabled(!this.m_locked);
    this.GetShowingMaterial().SetFloat("_Desaturate", num);
    this.m_rootObject.GetComponent<Renderer>().materials[0].SetFloat("_Desaturate", num);
  }

  public void SetInfo(string name, TAG_CLASS buttonClass, CardDef cardDef, int missionID, bool flip)
  {
    this.SetInfo(name, buttonClass, cardDef, missionID, 0L, flip);
  }

  public void SetInfo(string name, TAG_CLASS buttonClass, CardDef cardDef, long deckID, bool flip)
  {
    this.SetInfo(name, buttonClass, cardDef, 0, deckID, flip);
  }

  public void CoverUp(bool flip)
  {
    this.m_covered = true;
    if (flip)
    {
      this.GetHiddenNameMesh().Text = string.Empty;
      this.GetHiddenCover().GetComponent<Renderer>().enabled = true;
      this.Flip();
    }
    else
    {
      this.GetShowingNameMesh().Text = string.Empty;
      this.GetShowingCover().GetComponent<Renderer>().enabled = true;
    }
    iTween.MoveTo(this.m_rootObject, iTween.Hash((object) "position", (object) this.m_coveredBone.localPosition, (object) "time", (object) 0.25f, (object) "isLocal", (object) true, (object) "easeType", (object) iTween.EaseType.linear));
    this.SetEnabled(false);
  }

  public void Select()
  {
    SoundManager.Get().LoadAndPlay("select_AI_opponent", this.gameObject);
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    this.SetEnabled(false);
    this.Depress();
  }

  public void Deselect()
  {
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
    if (this.m_covered)
      return;
    this.Raise();
    if (this.m_locked)
      return;
    this.SetEnabled(true);
  }

  public void Raise()
  {
    this.Raise(0.1f);
  }

  public void ShowQuestBang(bool shown)
  {
    this.m_questBang.SetActive(shown);
  }

  private void Flip()
  {
    this.StopCoroutine(this.FLIP_COROUTINE);
    this.m_usingBackside = !this.m_usingBackside;
    this.StartCoroutine(this.FLIP_COROUTINE, (object) this.m_usingBackside);
  }

  [DebuggerHidden]
  private IEnumerator WaitThenFlip(bool flipToBackside)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PracticeAIButton.\u003CWaitThenFlip\u003Ec__Iterator231() { flipToBackside = flipToBackside, \u003C\u0024\u003EflipToBackside = flipToBackside, \u003C\u003Ef__this = this };
  }

  private UberText GetShowingNameMesh()
  {
    if (this.m_usingBackside)
      return this.m_backsideName;
    return this.m_name;
  }

  private UberText GetHiddenNameMesh()
  {
    if (this.m_usingBackside)
      return this.m_name;
    return this.m_backsideName;
  }

  private Material GetShowingMaterial()
  {
    return this.m_rootObject.GetComponent<Renderer>().materials[!this.m_usingBackside ? 1 : 2];
  }

  private void SetShowingMaterial(Material mat)
  {
    RenderUtils.SetMaterial(this.m_rootObject.GetComponent<Renderer>(), !this.m_usingBackside ? 1 : 2, mat);
  }

  private Material GetHiddenMaterial()
  {
    return this.m_rootObject.GetComponent<Renderer>().materials[!this.m_usingBackside ? 2 : 1];
  }

  private void SetHiddenMaterial(Material mat)
  {
    RenderUtils.SetMaterial(this.m_rootObject.GetComponent<Renderer>(), !this.m_usingBackside ? 2 : 1, mat);
  }

  private GameObject GetShowingCover()
  {
    if (this.m_usingBackside)
      return this.m_backsideCover;
    return this.m_frontCover;
  }

  private GameObject GetHiddenCover()
  {
    if (this.m_usingBackside)
      return this.m_frontCover;
    return this.m_backsideCover;
  }

  private void SetInfo(string name, TAG_CLASS buttonClass, CardDef cardDef, int missionID, long deckID, bool flip)
  {
    this.SetMissionID(missionID);
    this.SetDeckID(deckID);
    this.SetButtonClass(buttonClass);
    Material practiceAiPortrait = cardDef.GetPracticeAIPortrait();
    if (flip)
    {
      this.GetHiddenNameMesh().Text = name;
      if ((Object) practiceAiPortrait != (Object) null)
        this.SetHiddenMaterial(practiceAiPortrait);
      this.Flip();
    }
    else
    {
      if (this.m_infoSet)
        UnityEngine.Debug.LogWarning((object) "PracticeAIButton.SetInfo() - button is being re-initialized!");
      this.m_infoSet = true;
      if ((Object) practiceAiPortrait != (Object) null)
        this.SetShowingMaterial(practiceAiPortrait);
      this.GetShowingNameMesh().Text = name;
      this.SetOriginalLocalPosition();
    }
    this.m_covered = false;
    this.GetShowingCover().GetComponent<Renderer>().enabled = false;
  }

  private void SetMissionID(int missionID)
  {
    this.m_missionID = missionID;
  }

  private void SetDeckID(long deckID)
  {
    this.m_deckID = deckID;
  }

  private void SetButtonClass(TAG_CLASS buttonClass)
  {
    this.m_class = buttonClass;
  }

  private void Raise(float time)
  {
    iTween.MoveTo(this.m_rootObject, iTween.Hash((object) "position", (object) this.m_upBone.localPosition, (object) "time", (object) time, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true));
  }

  private void Depress()
  {
    iTween.MoveTo(this.m_rootObject, iTween.Hash((object) "position", (object) this.m_downBone.localPosition, (object) "time", (object) 0.1f, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true));
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    SoundManager.Get().LoadAndPlay("collection_manager_hero_mouse_over", this.gameObject);
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }
}
