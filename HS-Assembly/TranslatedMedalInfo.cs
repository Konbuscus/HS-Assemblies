﻿// Decompiled with JetBrains decompiler
// Type: TranslatedMedalInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class TranslatedMedalInfo
{
  public int rank = -1;
  public int bestRank = -1;
  public int totalStars;
  public int earnedStars;
  public int winStreak;
  public int streakStars;
  public int legendIndex;
  public bool canLoseStars;
  public bool canLoseLevel;
  public string name;
  public string nextMedalName;
  public string textureName;

  public bool IsHighestRankThatCannotBeLost()
  {
    if (this.canLoseStars)
      return !this.canLoseLevel;
    return false;
  }

  public bool CanGetRankedRewardChest()
  {
    if (!this.canLoseStars)
      return this.IsLegendRank();
    return true;
  }

  public bool IsLegendRank()
  {
    return this.rank == 0;
  }

  public override string ToString()
  {
    return string.Format("[{3} totalStars={0} earnedStars={1} rank={2} canLoseStars={4} canLoseLevel={5}]", (object) this.totalStars, (object) this.earnedStars, (object) this.rank, (object) this.name, (object) this.canLoseStars, (object) this.canLoseLevel);
  }
}
