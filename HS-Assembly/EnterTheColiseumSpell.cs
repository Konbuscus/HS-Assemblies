﻿// Decompiled with JetBrains decompiler
// Type: EnterTheColiseumSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class EnterTheColiseumSpell : Spell
{
  public float m_survivorLiftHeight = 2f;
  public float m_LiftTime = 0.5f;
  public float m_LiftOffset = 0.1f;
  public float m_DestroyMinionDelay = 0.5f;
  public float m_LowerDelay = 1.5f;
  public float m_LowerOffset = 0.05f;
  public float m_LowerTime = 0.7f;
  public float m_LightingFadeTime = 0.5f;
  public float m_CameraShakeMagnitude = 0.075f;
  public iTween.EaseType m_liftEaseType = iTween.EaseType.easeInQuart;
  public iTween.EaseType m_lowerEaseType = iTween.EaseType.easeOutCubic;
  public iTween.EaseType m_lightFadeEaseType = iTween.EaseType.easeOutCubic;
  public bool m_survivorsMeetInMiddle = true;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_SpellStartSoundPrefab;
  public Spell m_survivorSpellPrefab;
  public Spell m_DustSpellPrefab;
  public Spell m_ImpactSpellPrefab;
  public string m_RaiseSoundName;
  private List<Card> m_survivorCards;
  private bool m_effectsPlaying;
  private int m_numSurvivorSpellsPlaying;

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    this.m_survivorCards = this.FindSurvivors();
    this.StartCoroutine(this.PerformActions());
  }

  [DebuggerHidden]
  private IEnumerator PerformActions()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EnterTheColiseumSpell.\u003CPerformActions\u003Ec__Iterator2B7() { \u003C\u003Ef__this = this };
  }

  private void LiftCard(Card card)
  {
    GameObject gameObject = card.gameObject;
    Vector3 position1 = gameObject.transform.position;
    Vector3 position2 = card.GetZone().gameObject.transform.position;
    Hashtable args = iTween.Hash((object) "time", (object) this.m_LiftTime, (object) "position", (object) new Vector3(!this.m_survivorsMeetInMiddle ? position1.x : position2.x, position1.y + this.m_survivorLiftHeight, position1.z), (object) "onstart", (object) (Action<object>) (newVal => SoundManager.Get().LoadAndPlay(this.m_RaiseSoundName)), (object) "easetype", (object) this.m_liftEaseType);
    iTween.MoveTo(gameObject, args);
  }

  private void LowerCard(GameObject target, Vector3 finalPosition)
  {
    Hashtable args = iTween.Hash((object) "time", (object) this.m_LowerTime, (object) "position", (object) finalPosition, (object) "easetype", (object) this.m_lowerEaseType);
    iTween.MoveTo(target, args);
  }

  private List<Card> FindSurvivors()
  {
    List<Card> cardList = new List<Card>();
    using (List<GameObject>.Enumerator enumerator1 = this.m_targets.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Card component = enumerator1.Current.GetComponent<Card>();
        bool flag = true;
        using (List<PowerTask>.Enumerator enumerator2 = this.m_taskList.GetTaskList().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Network.PowerHistory power = enumerator2.Current.GetPower();
            if (power.Type == Network.PowerType.TAG_CHANGE)
            {
              Network.HistTagChange histTagChange = power as Network.HistTagChange;
              if (histTagChange.Tag == 360 && histTagChange.Value == 1)
              {
                Entity entity = GameState.Get().GetEntity(histTagChange.Entity);
                if (entity == null)
                {
                  string format = string.Format("{0}.FindSurvivors() - WARNING trying to get entity with id {1} but there is no entity with that id", (object) this, (object) histTagChange.Entity);
                  Log.Power.PrintWarning(format);
                }
                else if ((UnityEngine.Object) component == (UnityEngine.Object) entity.GetCard())
                {
                  flag = false;
                  break;
                }
              }
            }
          }
        }
        if (flag)
          cardList.Add(component);
      }
    }
    return cardList;
  }

  private void PlaySurvivorSpell(Card card)
  {
    if ((UnityEngine.Object) this.m_survivorSpellPrefab == (UnityEngine.Object) null)
      return;
    ++this.m_numSurvivorSpellsPlaying;
    Spell spell1 = UnityEngine.Object.Instantiate<Spell>(this.m_survivorSpellPrefab);
    spell1.transform.parent = card.GetActor().transform;
    spell1.AddFinishedCallback((Spell.FinishedCallback) ((spell, spellUserData) => --this.m_numSurvivorSpellsPlaying));
    spell1.AddStateFinishedCallback((Spell.StateFinishedCallback) ((spell, prevStateType, userData) =>
    {
      if (spell.GetActiveState() != SpellStateType.NONE)
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
    }));
    spell1.SetSource(card.gameObject);
    spell1.Activate();
  }

  private void PlayDustCloudSpell()
  {
    if ((UnityEngine.Object) this.m_DustSpellPrefab == (UnityEngine.Object) null)
      return;
    Spell spell1 = UnityEngine.Object.Instantiate<Spell>(this.m_DustSpellPrefab);
    spell1.AddStateFinishedCallback((Spell.StateFinishedCallback) ((spell, prevStateType, userData) =>
    {
      if (spell.GetActiveState() != SpellStateType.NONE)
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
    }));
    spell1.Activate();
  }
}
