﻿// Decompiled with JetBrains decompiler
// Type: DLLUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.InteropServices;

public class DLLUtils
{
  [DllImport("kernel32.dll")]
  public static extern IntPtr LoadLibrary(string filename);

  [DllImport("kernel32.dll")]
  public static extern IntPtr GetProcAddress(IntPtr module, string funcName);

  [DllImport("kernel32.dll")]
  public static extern IntPtr GetProcAddress(IntPtr module, IntPtr ordinalResource);

  [DllImport("kernel32.dll")]
  public static extern bool FreeLibrary(IntPtr module);
}
