﻿// Decompiled with JetBrains decompiler
// Type: KAR04_Julianne
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR04_Julianne : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Julianne_Female_Human_JulianneHeroPower_01");
    this.PreloadSound("VO_Julianne_Female_Human_JulianneEmoteResponse_01");
    this.PreloadSound("VO_KARA_06_01_Male_Human_JulianneTurn1_01");
    this.PreloadSound("VO_Moroes_Male_Human_JulianneTurn5_01");
    this.PreloadSound("VO_Moroes_Male_Human_JulianneTurn9_02");
    this.PreloadSound("VO_Barnes_Male_Human_JulianneTurn5_01");
    this.PreloadSound("VO_KARA_06_01_Male_Human_JulianneDeadlyPoison_02");
    this.PreloadSound("VO_Julianne_Female_Human_JulianneFeignDeath_03");
    this.PreloadSound("VO_Barnes_Male_Human_JulianneWin_01");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Julianne_Female_Human_JulianneEmoteResponse_01",
            m_stringTag = "VO_Julianne_Female_Human_JulianneEmoteResponse_01"
          }
        }
      }
    };
  }

  private Actor GetRomulo()
  {
    Player opposingSidePlayer = GameState.Get().GetOpposingSidePlayer();
    using (List<Card>.Enumerator enumerator = opposingSidePlayer.GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity = enumerator.Current.GetEntity();
        if (entity.GetControllerId() == opposingSidePlayer.GetPlayerId() && (entity.GetCardId() == "KARA_06_01" || entity.GetCardId() == "KARA_06_01heroic"))
          return entity.GetCard().GetActor();
      }
    }
    return (Actor) null;
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR04_Julianne.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator198() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR04_Julianne.\u003CHandleMissionEventWithTiming\u003Ec__Iterator199() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR04_Julianne.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator19A() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR04_Julianne.\u003CHandleGameOverWithTiming\u003Ec__Iterator19B() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
