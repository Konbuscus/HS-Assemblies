﻿// Decompiled with JetBrains decompiler
// Type: ServerOption
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum ServerOption
{
  INVALID = 0,
  FLAGS1 = 1,
  FLAGS2 = 2,
  FLAGS3 = 3,
  FLAGS4 = 4,
  FLAGS5 = 5,
  PAGE_MOUSE_OVERS = 6,
  COVER_MOUSE_OVERS = 7,
  LAST_PRECON_HERO_CHOSEN = 8,
  AI_MODE = 9,
  TIP_PRACTICE_PROGRESS = 10,
  TIP_PLAY_PROGRESS = 11,
  TIP_FORGE_PROGRESS = 12,
  FLAGS6 = 13,
  FLAGS7 = 14,
  FLAGS8 = 15,
  FLAGS9 = 16,
  FLAGS10 = 17,
  LAST_CUSTOM_DECK_CHOSEN = 18,
  DEPRECATED_DECK_PICKER_MODE = 19,
  SELECTED_ADVENTURE = 20,
  SELECTED_ADVENTURE_MODE = 21,
  LAST_SELECTED_STORE_BOOSTER_ID = 22,
  LAST_SELECTED_STORE_ADVENTURE_ID = 23,
  SERVER_OPTIONS_VERSION = 24,
  LATEST_SEEN_TAVERNBRAWL_SEASON = 25,
  LATEST_SEEN_TAVERNBRAWL_SEASON_CHALKBOARD = 26,
  LAST_SELECTED_STORE_HERO_ID = 27,
  TIMES_SEEN_TAVERNBRAWL_CRAZY_RULES_QUOTE = 28,
  SKIP_DECK_TEMPLATE_PAGE_FOR_CLASS_FLAGS = 29,
  SET_ROTATION_INTRO_PROGRESS = 30,
  TIMES_MOUSED_OVER_SWITCH_FORMAT_BUTTON = 31,
  RETURNING_PLAYER_BANNER_SEEN = 32,
  LATEST_SEEN_TAVERNBRAWL_SESSION_LIMIT = 33,
  LIMIT = 51,
}
