﻿// Decompiled with JetBrains decompiler
// Type: GeneralStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class GeneralStore : Store
{
  private static readonly int MIN_GOLD_FOR_CHANGE_QTY_TOOLTIP = 500;
  private static readonly float FLIP_BUY_PANEL_ANIM_TIME = 0.1f;
  private static readonly Vector3 MAIN_PANEL_ANGLE_TO_ROTATE = new Vector3(0.3333333f, 0.0f, 0.0f);
  private static readonly GeneralStoreMode[] s_ContentOrdering = new GeneralStoreMode[2]{ GeneralStoreMode.ADVENTURE, GeneralStoreMode.CARDS };
  private static readonly Vector3[] s_ContentTriangularPositions = new Vector3[3]{ new Vector3(0.0f, 0.125f, 0.0f), new Vector3(0.0f, -0.064f, -0.109f), new Vector3(0.0f, -0.064f, 0.109f) };
  private static readonly Vector3[] s_ContentTriangularRotations = new Vector3[3]{ new Vector3(-60f, 0.0f, -180f), new Vector3(0.0f, -180f, 0.0f), new Vector3(60f, 0.0f, 180f) };
  private static readonly Vector3[] s_MainPanelTriangularRotations = new Vector3[3]{ new Vector3(0.0f, 0.0f, 0.0f), new Vector3(-240f, 0.0f, 0.0f), new Vector3(-120f, 0.0f, 0.0f) };
  [CustomEditField(ListTable = true, Sections = "General Store")]
  public List<GeneralStore.ModeObjects> m_modeObjects = new List<GeneralStore.ModeObjects>();
  [CustomEditField(Sections = "General Store/Text")]
  public float m_productDetailsRegularHeight = 13f;
  [CustomEditField(Sections = "General Store/Text")]
  public float m_productDetailsExtendedHeight = 15.5f;
  [CustomEditField(Sections = "General Store/Text")]
  public float m_koreanProductDetailsRegularHeight = 8f;
  [CustomEditField(Sections = "General Store/Text")]
  public float m_koreanProductDetailsExtendedHeight = 10.5f;
  [CustomEditField(Sections = "General Store/Content")]
  public float m_contentFlipAnimationTime = 0.5f;
  [CustomEditField(Sections = "General Store/Content")]
  public iTween.EaseType m_contentFlipEaseType = iTween.EaseType.easeOutBounce;
  [CustomEditField(Sections = "General Store/Panes")]
  public Vector3 m_paneSwapOutOffset = new Vector3(0.05f, 0.0f, 0.0f);
  [CustomEditField(Sections = "General Store/Panes")]
  public Vector3 m_paneSwapInOffset = new Vector3(0.0f, -0.05f, 0.0f);
  [CustomEditField(Sections = "General Store/Panes")]
  public float m_paneSwapAnimationTime = 1f;
  [CustomEditField(Sections = "General Store/Shake Store")]
  public float m_multipleShakeTolerance = 2.5f;
  private Vector3 m_shakeyObjectOriginalLocalRotation = Vector3.zero;
  private Vector3 m_shakeyObjectOriginalLocalPosition = Vector3.zero;
  private List<GeneralStore.ModeChanged> m_modeChangedListeners = new List<GeneralStore.ModeChanged>();
  private Map<GeneralStoreMode, Vector3> m_paneStartPositions = new Map<GeneralStoreMode, Vector3>();
  [CustomEditField(Sections = "General Store")]
  public GameObject m_mainPanel;
  [CustomEditField(Sections = "General Store")]
  public GameObject m_buyGoldPanel;
  [CustomEditField(Sections = "General Store")]
  public GameObject m_buyMoneyPanel;
  [CustomEditField(Sections = "General Store")]
  public GameObject m_buyEmptyPanel;
  [CustomEditField(Sections = "General Store")]
  public MeshRenderer m_accentIcon;
  [CustomEditField(Sections = "General Store/Mode Buttons")]
  public GameObject m_modeButtonBlocker;
  [CustomEditField(Sections = "General Store/Text")]
  public UberText m_moneyCostText;
  [CustomEditField(Sections = "General Store/Text")]
  public UberText m_goldCostText;
  [CustomEditField(Sections = "General Store/Text")]
  public MultiSliceElement m_productDetailsContainer;
  [CustomEditField(Sections = "General Store/Text")]
  public UberText m_productDetailsHeadlineText;
  [CustomEditField(Sections = "General Store/Text")]
  public UberText m_productDetailsText;
  [CustomEditField(Sections = "General Store/Text")]
  public UberText m_koreanProductDetailsText;
  [CustomEditField(Sections = "General Store/Text")]
  public UberText m_koreanWarningText;
  [CustomEditField(Sections = "General Store/Text")]
  public GameObject m_chooseArrowContainer;
  [CustomEditField(Sections = "General Store/Text")]
  public UberText m_chooseArrowText;
  [CustomEditField(Sections = "General Store/Panes")]
  public GeneralStorePane m_defaultPane;
  [CustomEditField(Sections = "General Store/Panes")]
  public UIBScrollable m_paneScrollbar;
  [CustomEditField(Sections = "General Store/Shake Store")]
  public GameObject m_shakeyObject;
  [CustomEditField(Sections = "General Store/Sounds", T = EditType.SOUND_PREFAB)]
  public string m_contentFlipSound;
  private static GeneralStore s_instance;
  private GeneralStore.BuyPanelState m_buyPanelState;
  private bool m_staticTextResized;
  private GeneralStoreMode m_currentMode;
  private int m_settingNewModeCount;
  private Coroutine m_shakeyStoreAnimCoroutine;
  private float m_lastShakeyAmount;
  private bool m_stillShaking;
  private int m_currentContentPositionIdx;
  private MusicPlaylistType m_prevPlaylist;

  protected override void Start()
  {
    base.Start();
    StoreManager.Get().RegisterStoreAchievesListener(new StoreManager.StoreAchievesCallback(this.SuccessfulPurchaseEvent));
    StoreManager.Get().RegisterSuccessfulPurchaseAckListener(new StoreManager.SuccessfulPurchaseAckCallback(this.SuccessfulPurchaseAckEvent));
    SoundManager.Get().Load("gold_spend_plate_flip_on");
    SoundManager.Get().Load("gold_spend_plate_flip_off");
    this.UpdateModeButtons(this.m_currentMode);
    using (List<GeneralStore.ModeObjects>.Enumerator enumerator = this.m_modeObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GeneralStore.ModeObjects current = enumerator.Current;
        if ((UnityEngine.Object) current.m_content != (UnityEngine.Object) null)
          current.m_content.gameObject.SetActive(current.m_mode == this.m_currentMode);
      }
    }
    if (!((UnityEngine.Object) this.m_offClicker != (UnityEngine.Object) null))
      return;
    this.m_offClicker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClosePressed));
  }

  protected override void Awake()
  {
    GeneralStore.s_instance = this;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_scaleMode = CanvasScaleMode.WIDTH;
    base.Awake();
    if ((UnityEngine.Object) this.m_shakeyObject != (UnityEngine.Object) null)
      this.m_shakeyObjectOriginalLocalRotation = this.m_shakeyObject.transform.localEulerAngles;
    this.m_buyWithMoneyButton.SetText(GameStrings.Get("GLUE_STORE_BUY_TEXT"));
    this.m_buyWithGoldButton.SetText(GameStrings.Get("GLUE_STORE_BUY_TEXT"));
    using (List<GeneralStore.ModeObjects>.Enumerator enumerator = this.m_modeObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GeneralStore.ModeObjects current = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GeneralStore.\u003CAwake\u003Ec__AnonStorey3DF awakeCAnonStorey3Df = new GeneralStore.\u003CAwake\u003Ec__AnonStorey3DF();
        // ISSUE: reference to a compiler-generated field
        awakeCAnonStorey3Df.\u003C\u003Ef__this = this;
        GeneralStoreContent content = current.m_content;
        UIBButton button = current.m_button;
        // ISSUE: reference to a compiler-generated field
        awakeCAnonStorey3Df.mode = current.m_mode;
        GeneralStorePane pane = current.m_pane;
        if ((UnityEngine.Object) content != (UnityEngine.Object) null)
        {
          content.SetParentStore(this);
          // ISSUE: reference to a compiler-generated method
          content.RegisterCurrentBundleChanged(new GeneralStoreContent.BundleChanged(awakeCAnonStorey3Df.\u003C\u003Em__1A0));
        }
        if ((UnityEngine.Object) button != (UnityEngine.Object) null)
        {
          // ISSUE: reference to a compiler-generated method
          button.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(awakeCAnonStorey3Df.\u003C\u003Em__1A1));
        }
        if ((UnityEngine.Object) pane != (UnityEngine.Object) null)
        {
          pane.transform.localPosition = this.m_paneSwapOutOffset;
          // ISSUE: reference to a compiler-generated field
          this.m_paneStartPositions[awakeCAnonStorey3Df.mode] = pane.m_paneContainer.transform.localPosition;
        }
      }
    }
    if (!((UnityEngine.Object) this.m_defaultPane != (UnityEngine.Object) null))
      return;
    this.m_defaultPane.transform.localPosition = this.m_paneSwapOutOffset;
  }

  protected override void OnDestroy()
  {
    base.OnDestroy();
    StoreManager.Get().RemoveSuccessfulPurchaseAckListener(new StoreManager.SuccessfulPurchaseAckCallback(this.SuccessfulPurchaseAckEvent));
    StoreManager.Get().RemoveStoreAchievesListener(new StoreManager.StoreAchievesCallback(this.SuccessfulPurchaseEvent));
    this.m_mainPanel = (GameObject) null;
    GeneralStore.s_instance = (GeneralStore) null;
  }

  public GeneralStoreContent GetCurrentContent()
  {
    return this.GetContent(this.m_currentMode);
  }

  public GeneralStorePane GetCurrentPane()
  {
    return this.GetPane(this.m_currentMode);
  }

  public GeneralStoreContent GetContent(GeneralStoreMode mode)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    GeneralStore.ModeObjects modeObjects = this.m_modeObjects.Find(new Predicate<GeneralStore.ModeObjects>(new GeneralStore.\u003CGetContent\u003Ec__AnonStorey3E0() { mode = mode }.\u003C\u003Em__1A2));
    if (modeObjects != null)
      return modeObjects.m_content;
    return (GeneralStoreContent) null;
  }

  public GeneralStorePane GetPane(GeneralStoreMode mode)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    GeneralStore.ModeObjects modeObjects = this.m_modeObjects.Find(new Predicate<GeneralStore.ModeObjects>(new GeneralStore.\u003CGetPane\u003Ec__AnonStorey3E1() { mode = mode }.\u003C\u003Em__1A3));
    if (modeObjects != null && (UnityEngine.Object) modeObjects.m_pane != (UnityEngine.Object) null)
      return modeObjects.m_pane;
    return this.m_defaultPane;
  }

  public void Close(bool closeWithAnimation)
  {
    if (!this.m_shown)
      return;
    if ((bool) UniversalInputManager.UsePhoneUI)
      Navigation.RemoveHandler(new Navigation.NavigateBackHandler(GeneralStorePhoneCover.OnNavigateBack));
    Navigation.Pop();
    this.CloseImpl(closeWithAnimation);
  }

  public override void Close()
  {
    if (!this.m_shown)
      return;
    if ((bool) UniversalInputManager.UsePhoneUI)
      Navigation.RemoveHandler(new Navigation.NavigateBackHandler(GeneralStorePhoneCover.OnNavigateBack));
    if (this.m_settingNewModeCount != 0)
      return;
    Navigation.GoBack();
  }

  public void SetMode(GeneralStoreMode mode)
  {
    this.StartCoroutine(this.AnimateAndUpdateStoreMode(this.m_currentMode, mode));
  }

  public GeneralStoreMode GetMode()
  {
    return this.m_currentMode;
  }

  public void ShakeStore(float xRotationAmount, float shakeTime, float delay = 0.0f, float translateAmount = 0.0f)
  {
    if ((UnityEngine.Object) this.m_shakeyObject == (UnityEngine.Object) null)
      return;
    this.m_shakeyStoreAnimCoroutine = this.StartCoroutine(this.AnimateShakeyObjectCoroutine(xRotationAmount, translateAmount, shakeTime, delay));
  }

  public void SetDescription(string title, string desc, string warning = null)
  {
    this.HideChooseDescription();
    if ((UnityEngine.Object) this.m_productDetailsContainer != (UnityEngine.Object) null)
      this.m_productDetailsContainer.gameObject.SetActive(true);
    bool flag1 = StoreManager.Get().IsKoreanCustomer();
    bool flag2 = !string.IsNullOrEmpty(title);
    this.m_productDetailsHeadlineText.gameObject.SetActive(flag2);
    this.m_productDetailsText.gameObject.SetActive(!flag1);
    this.m_koreanProductDetailsText.gameObject.SetActive(flag1);
    this.m_koreanWarningText.gameObject.SetActive(flag1);
    this.m_productDetailsText.Height = !flag2 ? this.m_productDetailsExtendedHeight : this.m_productDetailsRegularHeight;
    this.m_productDetailsHeadlineText.Text = title;
    this.m_koreanProductDetailsText.Text = desc;
    this.m_productDetailsText.Text = desc;
    this.m_koreanProductDetailsText.Height = !flag2 ? this.m_koreanProductDetailsExtendedHeight : this.m_koreanProductDetailsRegularHeight;
    this.m_koreanWarningText.Text = warning != null ? warning : string.Empty;
    if (!((UnityEngine.Object) this.m_productDetailsContainer != (UnityEngine.Object) null))
      return;
    this.m_productDetailsContainer.UpdateSlices();
  }

  public void HideDescription()
  {
    if (!((UnityEngine.Object) this.m_productDetailsContainer != (UnityEngine.Object) null))
      return;
    this.m_productDetailsContainer.gameObject.SetActive(false);
  }

  public void SetChooseDescription(string chooseText)
  {
    this.HideDescription();
    this.SetAccentTexture((Texture) null);
    if ((UnityEngine.Object) this.m_chooseArrowContainer != (UnityEngine.Object) null)
      this.m_chooseArrowContainer.SetActive(true);
    if (!((UnityEngine.Object) this.m_chooseArrowText != (UnityEngine.Object) null))
      return;
    this.m_chooseArrowText.Text = chooseText;
  }

  public void HideChooseDescription()
  {
    if (!((UnityEngine.Object) this.m_chooseArrowContainer != (UnityEngine.Object) null))
      return;
    this.m_chooseArrowContainer.SetActive(false);
  }

  public void SetAccentTexture(Texture texture)
  {
    if (!((UnityEngine.Object) this.m_accentIcon != (UnityEngine.Object) null))
      return;
    bool flag = (UnityEngine.Object) texture != (UnityEngine.Object) null;
    this.m_accentIcon.gameObject.SetActive(flag);
    if (!flag)
      return;
    this.m_accentIcon.material.mainTexture = texture;
  }

  public void HideAccentTexture()
  {
    if (!((UnityEngine.Object) this.m_accentIcon != (UnityEngine.Object) null))
      return;
    this.m_accentIcon.gameObject.SetActive(false);
  }

  public void HidePacksPane(bool hide)
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      if (hide)
        this.StartCoroutine(this.AnimateAndUpdateStoreMode(GeneralStoreMode.CARDS, GeneralStoreMode.NONE));
      else
        this.StartCoroutine(this.AnimateAndUpdateStoreMode(GeneralStoreMode.NONE, GeneralStoreMode.CARDS));
    }
    else
      this.StartCoroutine(this.AnimateHideStorePane(hide));
  }

  public void ResumePreviousMusicPlaylist()
  {
    if (this.m_prevPlaylist == MusicPlaylistType.Invalid)
      return;
    MusicManager.Get().StartPlaylist(this.m_prevPlaylist);
  }

  public void RegisterModeChangedListener(GeneralStore.ModeChanged dlg)
  {
    this.m_modeChangedListeners.Add(dlg);
  }

  public void UnregisterModeChangedListener(GeneralStore.ModeChanged dlg)
  {
    this.m_modeChangedListeners.Remove(dlg);
  }

  public static GeneralStore Get()
  {
    return GeneralStore.s_instance;
  }

  public override bool IsReady()
  {
    return true;
  }

  public override void OnMoneySpent()
  {
    NetCache.Get().RefreshNetObject<NetCache.NetCacheBoosters>();
    GeneralStoreContent currentContent = this.GetCurrentContent();
    if ((UnityEngine.Object) currentContent != (UnityEngine.Object) null)
      currentContent.Refresh();
    GeneralStorePane currentPane = this.GetCurrentPane();
    if (!((UnityEngine.Object) currentPane != (UnityEngine.Object) null))
      return;
    currentPane.Refresh();
  }

  public override void OnGoldSpent()
  {
    NetCache.Get().RefreshNetObject<NetCache.NetCacheBoosters>();
  }

  public override void OnGoldBalanceChanged(NetCache.NetCacheGoldBalance balance)
  {
    this.UpdateGoldButtonState(balance);
  }

  protected override void ShowImpl(Store.DelOnStoreShown onStoreShownCB, bool isTotallyFake)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStore.\u003CShowImpl\u003Ec__AnonStorey3E2 implCAnonStorey3E2 = new GeneralStore.\u003CShowImpl\u003Ec__AnonStorey3E2();
    // ISSUE: reference to a compiler-generated field
    implCAnonStorey3E2.onStoreShownCB = onStoreShownCB;
    if (this.m_shown)
      return;
    this.m_prevPlaylist = MusicManager.Get().GetCurrentPlaylist();
    using (List<GeneralStore.ModeObjects>.Enumerator enumerator = this.m_modeObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GeneralStore.ModeObjects current = enumerator.Current;
        GeneralStoreContent content = current.m_content;
        GeneralStorePane pane = current.m_pane;
        if ((UnityEngine.Object) content != (UnityEngine.Object) null)
          content.StoreShown((UnityEngine.Object) this.GetCurrentContent() == (UnityEngine.Object) content);
        if ((UnityEngine.Object) pane != (UnityEngine.Object) null)
          pane.StoreShown((UnityEngine.Object) this.GetCurrentPane() == (UnityEngine.Object) pane);
      }
    }
    ShownUIMgr.Get().SetShownUI(ShownUIMgr.UI_WINDOW.GENERAL_STORE);
    FriendChallengeMgr.Get().OnStoreOpened();
    this.PreRender();
    PresenceMgr.Get().SetStatus((Enum) PresenceStatus.STORE);
    if (!(bool) UniversalInputManager.UsePhoneUI && !Options.Get().GetBool(Option.HAS_SEEN_GOLD_QTY_INSTRUCTION, false) && (UserAttentionManager.CanShowAttentionGrabber("GeneralStore.Show:" + (object) Option.HAS_SEEN_GOLD_QTY_INSTRUCTION) && NetCache.Get().GetNetObject<NetCache.NetCacheGoldBalance>().GetTotal() >= (long) GeneralStore.MIN_GOLD_FOR_CHANGE_QTY_TOOLTIP))
    {
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_attentionCategory = UserAttentionBlocker.NONE,
        m_headerText = GameStrings.Get("GLUE_STORE_GOLD_QTY_CHANGE_HEADER"),
        m_text = !UniversalInputManager.Get().IsTouchMode() ? GameStrings.Get("GLUE_STORE_GOLD_QTY_CHANGE_DESC") : GameStrings.Get("GLUE_STORE_GOLD_QTY_CHANGE_DESC_TOUCH"),
        m_showAlertIcon = false,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK
      });
      Options.Get().SetBool(Option.HAS_SEEN_GOLD_QTY_INSTRUCTION, true);
    }
    this.UpdateGoldButtonState();
    this.m_shown = true;
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    this.EnableFullScreenEffects(true);
    SoundManager.Get().LoadAndPlay("Store_window_expand", this.gameObject);
    // ISSUE: reference to a compiler-generated method
    this.DoShowAnimation(new UIBPopup.OnAnimationComplete(implCAnonStorey3E2.\u003C\u003Em__1A4));
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    BnetBar.Get().SetCurrencyType(new CurrencyFrame.CurrencyType?(CurrencyFrame.CurrencyType.GOLD));
    BnetBar.Get().UpdateForPhone();
  }

  private void OnClosePressed(UIEvent e)
  {
    if (!this.m_shown)
      return;
    this.Close();
  }

  protected override void BuyWithMoney(UIEvent e)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStore.\u003CBuyWithMoney\u003Ec__AnonStorey3E3 moneyCAnonStorey3E3 = new GeneralStore.\u003CBuyWithMoney\u003Ec__AnonStorey3E3();
    // ISSUE: reference to a compiler-generated field
    moneyCAnonStorey3E3.\u003C\u003Ef__this = this;
    GeneralStoreContent currentContent = this.GetCurrentContent();
    // ISSUE: reference to a compiler-generated field
    moneyCAnonStorey3E3.bundle = currentContent.GetCurrentMoneyBundle();
    // ISSUE: reference to a compiler-generated field
    if (moneyCAnonStorey3E3.bundle == null)
    {
      UnityEngine.Debug.LogWarning((object) "GeneralStore.OnBuyWithMoneyPressed(): SelectedBundle is null");
    }
    else
    {
      // ISSUE: reference to a compiler-generated method
      GeneralStoreContent.BuyEvent successBuyCB = new GeneralStoreContent.BuyEvent(moneyCAnonStorey3E3.\u003C\u003Em__1A5);
      // ISSUE: reference to a compiler-generated field
      currentContent.TryBuyWithMoney(moneyCAnonStorey3E3.bundle, successBuyCB, (GeneralStoreContent.BuyEvent) null);
    }
  }

  protected override void BuyWithGold(UIEvent e)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStore.\u003CBuyWithGold\u003Ec__AnonStorey3E4 goldCAnonStorey3E4 = new GeneralStore.\u003CBuyWithGold\u003Ec__AnonStorey3E4();
    // ISSUE: reference to a compiler-generated field
    goldCAnonStorey3E4.\u003C\u003Ef__this = this;
    GeneralStoreContent currentContent = this.GetCurrentContent();
    // ISSUE: reference to a compiler-generated field
    goldCAnonStorey3E4.bundle = currentContent.GetCurrentGoldBundle();
    // ISSUE: reference to a compiler-generated field
    if (goldCAnonStorey3E4.bundle == null)
    {
      UnityEngine.Debug.LogWarning((object) "GeneralStore.OnBuyWithGoldPressed(): SelectedGoldPrice is null");
    }
    else
    {
      // ISSUE: reference to a compiler-generated method
      GeneralStoreContent.BuyEvent buyEvent = new GeneralStoreContent.BuyEvent(goldCAnonStorey3E4.\u003C\u003Em__1A6);
      currentContent.TryBuyWithGold(buyEvent, buyEvent);
    }
  }

  private void UpdateMoneyButtonState()
  {
    Store.BuyButtonState state = Store.BuyButtonState.ENABLED;
    if (!StoreManager.Get().IsOpen())
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsBattlePayFeatureEnabled())
    {
      state = Store.BuyButtonState.DISABLED_FEATURE;
    }
    else
    {
      Network.Bundle currentMoneyBundle = this.GetCurrentContent().GetCurrentMoneyBundle();
      if (currentMoneyBundle == null || StoreManager.Get().IsProductAlreadyOwned(currentMoneyBundle))
        state = Store.BuyButtonState.DISABLED_OWNED;
    }
    this.SetMoneyButtonState(state);
  }

  private void UpdateGoldButtonState(NetCache.NetCacheGoldBalance balance)
  {
    Store.BuyButtonState state = Store.BuyButtonState.ENABLED;
    GeneralStoreContent currentContent = this.GetCurrentContent();
    if ((UnityEngine.Object) currentContent == (UnityEngine.Object) null)
      return;
    NoGTAPPTransactionData currentGoldBundle = currentContent.GetCurrentGoldBundle();
    if (currentGoldBundle == null)
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsOpen())
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsBuyWithGoldFeatureEnabled())
      state = Store.BuyButtonState.DISABLED_FEATURE;
    else if (balance == null)
    {
      state = Store.BuyButtonState.DISABLED;
    }
    else
    {
      long cost;
      if (!StoreManager.Get().GetGoldCostNoGTAPP(currentGoldBundle, out cost))
        state = Store.BuyButtonState.DISABLED_NO_TOOLTIP;
      else if (balance.GetTotal() < cost)
        state = Store.BuyButtonState.DISABLED_NOT_ENOUGH_GOLD;
    }
    this.SetGoldButtonState(state);
  }

  private void UpdateGoldButtonState()
  {
    this.UpdateGoldButtonState(NetCache.Get().GetNetObject<NetCache.NetCacheGoldBalance>());
  }

  private void UpdateCostDisplay(NoGTAPPTransactionData goldBundle)
  {
    long cost;
    if (goldBundle == null || !StoreManager.Get().GetGoldCostNoGTAPP(goldBundle, out cost))
      this.UpdateCostDisplay(GeneralStore.BuyPanelState.BUY_GOLD, string.Empty);
    else
      this.UpdateCostDisplay(GeneralStore.BuyPanelState.BUY_GOLD, cost.ToString());
  }

  private void UpdateCostDisplay(Network.Bundle moneyBundle)
  {
    if (moneyBundle == null)
      this.UpdateCostDisplay(GeneralStore.BuyPanelState.BUY_MONEY, GameStrings.Get("GLUE_STORE_DUNGEON_BUTTON_COST_OWNED_TEXT"));
    else
      this.UpdateCostDisplay(GeneralStore.BuyPanelState.BUY_MONEY, StoreManager.Get().FormatCostBundle(moneyBundle));
  }

  private void UpdateCostDisplay(GeneralStore.BuyPanelState newPanelState, string costText = "")
  {
    if (newPanelState == GeneralStore.BuyPanelState.BUY_MONEY)
    {
      this.m_moneyCostText.Text = costText;
      this.m_moneyCostText.UpdateNow();
    }
    else if (newPanelState == GeneralStore.BuyPanelState.BUY_GOLD)
    {
      this.m_goldCostText.Text = costText;
      this.m_goldCostText.UpdateNow();
    }
    this.ShowBuyPanel(newPanelState);
  }

  private void ShowBuyPanel(GeneralStore.BuyPanelState setPanelState)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStore.\u003CShowBuyPanel\u003Ec__AnonStorey3E5 panelCAnonStorey3E5 = new GeneralStore.\u003CShowBuyPanel\u003Ec__AnonStorey3E5();
    if (this.m_buyPanelState == setPanelState)
      return;
    GameObject buyPanelObject = this.GetBuyPanelObject(setPanelState);
    // ISSUE: reference to a compiler-generated field
    panelCAnonStorey3E5.oldPanelObject = this.GetBuyPanelObject(this.m_buyPanelState);
    this.m_buyPanelState = setPanelState;
    iTween.StopByName(buyPanelObject, "rotation");
    // ISSUE: reference to a compiler-generated field
    iTween.StopByName(panelCAnonStorey3E5.oldPanelObject, "rotation");
    buyPanelObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 180f);
    // ISSUE: reference to a compiler-generated field
    panelCAnonStorey3E5.oldPanelObject.transform.localEulerAngles = Vector3.zero;
    buyPanelObject.SetActive(true);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    iTween.RotateTo(panelCAnonStorey3E5.oldPanelObject, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, 0.0f, 180f), (object) "isLocal", (object) true, (object) "time", (object) GeneralStore.FLIP_BUY_PANEL_ANIM_TIME, (object) "easeType", (object) iTween.EaseType.linear, (object) "oncomplete", (object) new Action<object>(panelCAnonStorey3E5.\u003C\u003Em__1A7), (object) "name", (object) "rotation"));
    iTween.RotateTo(buyPanelObject, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, 0.0f, 0.0f), (object) "isLocal", (object) true, (object) "time", (object) GeneralStore.FLIP_BUY_PANEL_ANIM_TIME, (object) "easeType", (object) iTween.EaseType.linear, (object) "name", (object) "rotation"));
    SoundManager.Get().LoadAndPlay(setPanelState != GeneralStore.BuyPanelState.BUY_GOLD ? "gold_spend_plate_flip_off" : "gold_spend_plate_flip_on");
  }

  private GameObject GetBuyPanelObject(GeneralStore.BuyPanelState buyPanelState)
  {
    switch (buyPanelState)
    {
      case GeneralStore.BuyPanelState.BUY_GOLD:
        return this.m_buyGoldPanel;
      case GeneralStore.BuyPanelState.BUY_MONEY:
        return this.m_buyMoneyPanel;
      default:
        return this.m_buyEmptyPanel;
    }
  }

  public void RefreshContent()
  {
    GeneralStoreContent currentContent = this.GetCurrentContent();
    GeneralStorePane currentPane = this.GetCurrentPane();
    StoreManager storeManager = StoreManager.Get();
    this.ActivateCover(storeManager.TransactionInProgress() || storeManager.IsPromptShowing());
    if ((UnityEngine.Object) currentContent != (UnityEngine.Object) null)
      currentContent.Refresh();
    if (!((UnityEngine.Object) currentPane != (UnityEngine.Object) null))
      return;
    currentPane.Refresh();
  }

  protected override void Hide(bool animate)
  {
    if (this.m_settingNewModeCount > 0)
      return;
    if ((UnityEngine.Object) ShownUIMgr.Get() != (UnityEngine.Object) null)
      ShownUIMgr.Get().ClearShownUI();
    FriendChallengeMgr.Get().OnStoreClosed();
    this.ResumePreviousMusicPlaylist();
    this.DoHideAnimation(!animate, new UIBPopup.OnAnimationComplete(((UIBPopup) this).OnHidden));
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    BnetBar.Get().SetCurrencyType(new CurrencyFrame.CurrencyType?());
    BnetBar.Get().UpdateForPhone();
  }

  protected override void OnHidden()
  {
    this.m_shown = false;
    using (List<GeneralStore.ModeObjects>.Enumerator enumerator = this.m_modeObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GeneralStore.ModeObjects current = enumerator.Current;
        GeneralStorePane pane = current.m_pane;
        GeneralStoreContent content = current.m_content;
        if ((UnityEngine.Object) pane != (UnityEngine.Object) null)
          pane.StoreHidden((UnityEngine.Object) this.GetCurrentPane() == (UnityEngine.Object) pane);
        if ((UnityEngine.Object) content != (UnityEngine.Object) null)
          content.StoreHidden((UnityEngine.Object) this.GetCurrentContent() == (UnityEngine.Object) content);
      }
    }
  }

  private void PreRender()
  {
    if (!this.m_staticTextResized)
    {
      this.m_buyWithMoneyButton.m_ButtonText.UpdateNow();
      this.m_buyWithGoldButton.m_ButtonText.UpdateNow();
      this.m_staticTextResized = true;
    }
    this.RefreshContent();
  }

  private bool IsContentFlipClockwise(GeneralStoreMode oldMode, GeneralStoreMode newMode)
  {
    int num1 = 0;
    int num2 = 0;
    for (int index = 0; index < GeneralStore.s_ContentOrdering.Length; ++index)
    {
      if (GeneralStore.s_ContentOrdering[index] == oldMode)
        num1 = index;
      else if (GeneralStore.s_ContentOrdering[index] == newMode)
        num2 = index;
    }
    return num1 < num2;
  }

  [DebuggerHidden]
  private IEnumerator AnimateAndUpdateStoreMode(GeneralStoreMode oldMode, GeneralStoreMode newMode)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStore.\u003CAnimateAndUpdateStoreMode\u003Ec__Iterator265() { oldMode = oldMode, newMode = newMode, \u003C\u0024\u003EoldMode = oldMode, \u003C\u0024\u003EnewMode = newMode, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator AnimateHideStorePane(bool hide)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStore.\u003CAnimateHideStorePane\u003Ec__Iterator266() { hide = hide, \u003C\u0024\u003Ehide = hide, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator AnimateAndUpdateStorePane(GeneralStoreMode oldMode, GeneralStoreMode newMode)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStore.\u003CAnimateAndUpdateStorePane\u003Ec__Iterator267() { oldMode = oldMode, newMode = newMode, \u003C\u0024\u003EoldMode = oldMode, \u003C\u0024\u003EnewMode = newMode, \u003C\u003Ef__this = this };
  }

  private void OnStopShaking(object obj)
  {
    this.m_stillShaking = false;
  }

  [DebuggerHidden]
  private IEnumerator AnimateShakeyObjectCoroutine(float xRotationAmount, float translationAmount, float shakeTime, float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStore.\u003CAnimateShakeyObjectCoroutine\u003Ec__Iterator268() { xRotationAmount = xRotationAmount, delay = delay, shakeTime = shakeTime, translationAmount = translationAmount, \u003C\u0024\u003ExRotationAmount = xRotationAmount, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003EshakeTime = shakeTime, \u003C\u0024\u003EtranslationAmount = translationAmount, \u003C\u003Ef__this = this };
  }

  private void ResetAnimations()
  {
    if (this.m_shakeyStoreAnimCoroutine == null)
      return;
    this.StopCoroutine(this.m_shakeyStoreAnimCoroutine);
  }

  private void UpdateModeButtons(GeneralStoreMode mode)
  {
    using (List<GeneralStore.ModeObjects>.Enumerator enumerator = this.m_modeObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GeneralStore.ModeObjects current = enumerator.Current;
        if (!((UnityEngine.Object) current.m_button == (UnityEngine.Object) null))
        {
          UIBHighlight component = current.m_button.GetComponent<UIBHighlight>();
          if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
          {
            if (mode == current.m_mode)
              component.SelectNoSound();
            else
              component.Reset();
          }
        }
      }
    }
  }

  private void GetContentPositionIndex(bool clockwise, out Vector3 contentPosition, out Vector3 contentRotation, out Vector3 lastPanelRotation, out Vector3 newPanelRotation)
  {
    lastPanelRotation = GeneralStore.s_MainPanelTriangularRotations[this.m_currentContentPositionIdx];
    if (clockwise)
    {
      this.m_currentContentPositionIdx = (this.m_currentContentPositionIdx + 1) % GeneralStore.s_ContentTriangularPositions.Length;
    }
    else
    {
      --this.m_currentContentPositionIdx;
      if (this.m_currentContentPositionIdx < 0)
        this.m_currentContentPositionIdx = GeneralStore.s_ContentTriangularPositions.Length - 1;
    }
    contentPosition = GeneralStore.s_ContentTriangularPositions[this.m_currentContentPositionIdx];
    contentRotation = GeneralStore.s_ContentTriangularRotations[this.m_currentContentPositionIdx];
    newPanelRotation = GeneralStore.s_MainPanelTriangularRotations[this.m_currentContentPositionIdx];
  }

  private void SuccessfulPurchaseEvent(Network.Bundle bundle, PaymentMethod paymentMethod, object userData)
  {
    this.RefreshContent();
  }

  private void SuccessfulPurchaseAckEvent(Network.Bundle bundle, PaymentMethod paymentMethod, object userData)
  {
    if (this.IsShown() && SceneMgr.Get().GetMode() == SceneMgr.Mode.ADVENTURE)
      this.Close();
    else
      this.RefreshContent();
  }

  private void UpdateCostAndButtonState(NoGTAPPTransactionData goldBundle, Network.Bundle moneyBundle)
  {
    if (moneyBundle != null && !StoreManager.Get().IsProductAlreadyOwned(moneyBundle))
    {
      this.UpdateCostDisplay(moneyBundle);
      this.UpdateMoneyButtonState();
    }
    else if (goldBundle != null)
    {
      this.UpdateCostDisplay(goldBundle);
      this.UpdateGoldButtonState();
    }
    else
    {
      GeneralStoreContent currentContent = this.GetCurrentContent();
      if ((UnityEngine.Object) currentContent == (UnityEngine.Object) null || currentContent.IsPurchaseDisabled())
      {
        this.UpdateCostDisplay(GeneralStore.BuyPanelState.DISABLED, string.Empty);
      }
      else
      {
        this.UpdateCostDisplay(GeneralStore.BuyPanelState.BUY_MONEY, currentContent.GetMoneyDisplayOwnedText());
        this.UpdateMoneyButtonState();
      }
    }
  }

  private void FireModeChangedEvent(GeneralStoreMode oldMode, GeneralStoreMode newMode)
  {
    foreach (GeneralStore.ModeChanged modeChanged in this.m_modeChangedListeners.ToArray())
      modeChanged(oldMode, newMode);
  }

  private bool OnNavigateBack()
  {
    this.CloseImpl(true);
    return true;
  }

  private void CloseImpl(bool closeWithAnimation)
  {
    if (this.m_settingNewModeCount > 0)
      return;
    PresenceMgr.Get().SetPrevStatus();
    this.Hide(closeWithAnimation);
    SoundManager.Get().LoadAndPlay("Store_window_shrink", this.gameObject);
    this.EnableFullScreenEffects(false);
    this.FireExitEvent(false);
  }

  protected override string GetOwnedTooltipString()
  {
    switch (this.m_currentMode)
    {
      case GeneralStoreMode.CARDS:
        return GameStrings.Get("GLUE_STORE_PACK_BUTTON_TEXT_PURCHASED");
      case GeneralStoreMode.ADVENTURE:
        return GameStrings.Get("GLUE_STORE_DUNGEON_BUTTON_TEXT_PURCHASED");
      case GeneralStoreMode.HEROES:
        return GameStrings.Get("GLUE_STORE_HERO_BUTTON_TEXT_PURCHASED");
      default:
        return string.Empty;
    }
  }

  [Serializable]
  public class ModeObjects
  {
    public GeneralStoreMode m_mode;
    public GeneralStoreContent m_content;
    public GeneralStorePane m_pane;
    public UIBButton m_button;
  }

  private enum BuyPanelState
  {
    DISABLED,
    BUY_GOLD,
    BUY_MONEY,
  }

  public delegate void ModeChanged(GeneralStoreMode oldMode, GeneralStoreMode newMode);
}
