﻿// Decompiled with JetBrains decompiler
// Type: EmoteHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class EmoteHandler : MonoBehaviour
{
  private float m_timeSinceLastEmote = 4f;
  private const float MIN_TIME_BETWEEN_EMOTES = 4f;
  private const float TIME_WINDOW_TO_BE_CONSIDERED_A_CHAIN = 5f;
  private const float SPAMMER_MIN_TIME_BETWEEN_EMOTES = 15f;
  private const float UBER_SPAMMER_MIN_TIME_BETWEEN_EMOTES = 45f;
  private const int NUM_EMOTES_BEFORE_CONSIDERED_A_SPAMMER = 20;
  private const int NUM_EMOTES_BEFORE_CONSIDERED_UBER_SPAMMER = 25;
  private const int NUM_CHAIN_EMOTES_BEFORE_CONSIDERED_SPAM = 2;
  public List<EmoteOption> m_Emotes;
  public List<EmoteOption> m_HiddenEmotes;
  private static EmoteHandler s_instance;
  private bool m_emotesShown;
  private int m_shownAtFrame;
  private EmoteOption m_mousedOverEmote;
  private int m_totalEmotes;
  private int m_chainedEmotes;

  private void Awake()
  {
    EmoteHandler.s_instance = this;
    this.GetComponent<Collider>().enabled = false;
  }

  private void Start()
  {
    GameState.Get().RegisterHeroChangedListener(new GameState.HeroChangedCallback(this.OnHeroChanged), (object) null);
  }

  private void OnDestroy()
  {
    EmoteHandler.s_instance = (EmoteHandler) null;
  }

  private void Update()
  {
    this.m_timeSinceLastEmote += Time.unscaledDeltaTime;
  }

  public static EmoteHandler Get()
  {
    return EmoteHandler.s_instance;
  }

  public void ResetTimeSinceLastEmote()
  {
    if ((double) this.m_timeSinceLastEmote < 9.0)
      ++this.m_chainedEmotes;
    else
      this.m_chainedEmotes = 0;
    this.m_timeSinceLastEmote = 0.0f;
  }

  public void ShowEmotes()
  {
    if (this.m_emotesShown)
      return;
    this.m_shownAtFrame = Time.frameCount;
    this.m_emotesShown = true;
    this.GetComponent<Collider>().enabled = true;
    using (List<EmoteOption>.Enumerator enumerator = this.m_Emotes.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Enable();
    }
  }

  public void HideEmotes()
  {
    if (!this.m_emotesShown)
      return;
    this.m_mousedOverEmote = (EmoteOption) null;
    this.m_emotesShown = false;
    this.GetComponent<Collider>().enabled = false;
    using (List<EmoteOption>.Enumerator enumerator = this.m_Emotes.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Disable();
    }
  }

  public bool AreEmotesActive()
  {
    return this.m_emotesShown;
  }

  public void HandleInput()
  {
    RaycastHit hitInfo;
    if (!this.HitTestEmotes(out hitInfo))
    {
      this.HideEmotes();
    }
    else
    {
      EmoteOption component = hitInfo.transform.gameObject.GetComponent<EmoteOption>();
      if ((Object) component == (Object) null)
      {
        if ((Object) this.m_mousedOverEmote != (Object) null)
        {
          this.m_mousedOverEmote.HandleMouseOut();
          this.m_mousedOverEmote = (EmoteOption) null;
        }
      }
      else if ((Object) this.m_mousedOverEmote == (Object) null)
      {
        this.m_mousedOverEmote = component;
        this.m_mousedOverEmote.HandleMouseOver();
      }
      else if ((Object) this.m_mousedOverEmote != (Object) component)
      {
        this.m_mousedOverEmote.HandleMouseOut();
        this.m_mousedOverEmote = component;
        component.HandleMouseOver();
      }
      if (!UniversalInputManager.Get().GetMouseButtonUp(0))
        return;
      if ((Object) this.m_mousedOverEmote != (Object) null)
      {
        if (this.EmoteSpamBlocked())
          return;
        ++this.m_totalEmotes;
        if (GameState.Get().GetGameEntity().HasTag(GAME_TAG.ALL_TARGETS_RANDOM))
        {
          int count = this.m_Emotes.Count;
          int index = Random.Range(0, count + this.m_HiddenEmotes.Count);
          if (index < count)
            this.m_Emotes[index].DoClick();
          else
            this.m_HiddenEmotes[index - count].DoClick();
        }
        else
          this.m_mousedOverEmote.DoClick();
      }
      else
      {
        if (!UniversalInputManager.Get().IsTouchMode() || Time.frameCount == this.m_shownAtFrame)
          return;
        this.HideEmotes();
      }
    }
  }

  public bool IsMouseOverEmoteOption()
  {
    RaycastHit hitInfo;
    return UniversalInputManager.Get().GetInputHitInfo((LayerMask) GameLayer.Default.LayerBit(), out hitInfo) && (Object) hitInfo.transform.gameObject.GetComponent<EmoteOption>() != (Object) null;
  }

  public bool EmoteSpamBlocked()
  {
    if (GameMgr.Get().IsFriendly() || GameMgr.Get().IsAI())
      return false;
    if (this.m_totalEmotes >= 25)
      return (double) this.m_timeSinceLastEmote < 45.0;
    if (this.m_totalEmotes >= 20 || this.m_chainedEmotes >= 2)
      return (double) this.m_timeSinceLastEmote < 15.0;
    return (double) this.m_timeSinceLastEmote < 4.0;
  }

  private void OnHeroChanged(Player player, object userData)
  {
    if (!player.IsFriendlySide())
      return;
    using (List<EmoteOption>.Enumerator enumerator = this.m_Emotes.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateEmoteType();
    }
  }

  private bool HitTestEmotes(out RaycastHit hitInfo)
  {
    return UniversalInputManager.Get().GetInputHitInfo((LayerMask) GameLayer.CardRaycast.LayerBit(), out hitInfo) && (this.IsMousedOverHero(hitInfo) || this.IsMousedOverSelf(hitInfo) || this.IsMousedOverEmote(hitInfo));
  }

  private bool IsMousedOverHero(RaycastHit cardHitInfo)
  {
    Actor componentInParents = SceneUtils.FindComponentInParents<Actor>((Component) cardHitInfo.transform);
    if ((Object) componentInParents == (Object) null)
      return false;
    Card card = componentInParents.GetCard();
    return !((Object) card == (Object) null) && card.GetEntity().IsHero();
  }

  private bool IsMousedOverSelf(RaycastHit cardHitInfo)
  {
    return (Object) this.GetComponent<Collider>() == (Object) cardHitInfo.collider;
  }

  private bool IsMousedOverEmote(RaycastHit cardHitInfo)
  {
    using (List<EmoteOption>.Enumerator enumerator = this.m_Emotes.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EmoteOption current = enumerator.Current;
        if ((Object) cardHitInfo.transform == (Object) current.transform)
          return true;
      }
    }
    return false;
  }
}
