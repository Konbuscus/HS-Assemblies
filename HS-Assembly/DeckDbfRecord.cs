﻿// Decompiled with JetBrains decompiler
// Type: DeckDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DeckDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteName;
  [SerializeField]
  private int m_TopCardId;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private DbfLocValue m_Description;

  [DbfField("NOTE_NAME", "Designer name for the deck.  Not used by game.")]
  public string NoteName
  {
    get
    {
      return this.m_NoteName;
    }
  }

  [DbfField("TOP_CARD_ID", "DECK_CARD.ID of top card in deck")]
  public int TopCardId
  {
    get
    {
      return this.m_TopCardId;
    }
  }

  [DbfField("NAME", "")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("DESCRIPTION", "")]
  public DbfLocValue Description
  {
    get
    {
      return this.m_Description;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    DeckDbfAsset deckDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (DeckDbfAsset)) as DeckDbfAsset;
    if ((UnityEngine.Object) deckDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("DeckDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < deckDbfAsset.Records.Count; ++index)
      deckDbfAsset.Records[index].StripUnusedLocales();
    records = (object) deckDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Name.StripUnusedLocales();
    this.m_Description.StripUnusedLocales();
  }

  public void SetNoteName(string v)
  {
    this.m_NoteName = v;
  }

  public void SetTopCardId(int v)
  {
    this.m_TopCardId = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetDescription(DbfLocValue v)
  {
    this.m_Description = v;
    v.SetDebugInfo(this.ID, "DESCRIPTION");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckDbfRecord.\u003C\u003Ef__switch\u0024map2C == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckDbfRecord.\u003C\u003Ef__switch\u0024map2C = new Dictionary<string, int>(5)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_NAME",
            1
          },
          {
            "TOP_CARD_ID",
            2
          },
          {
            "NAME",
            3
          },
          {
            "DESCRIPTION",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckDbfRecord.\u003C\u003Ef__switch\u0024map2C.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteName;
          case 2:
            return (object) this.TopCardId;
          case 3:
            return (object) this.Name;
          case 4:
            return (object) this.Description;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (DeckDbfRecord.\u003C\u003Ef__switch\u0024map2D == null)
    {
      // ISSUE: reference to a compiler-generated field
      DeckDbfRecord.\u003C\u003Ef__switch\u0024map2D = new Dictionary<string, int>(5)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_NAME",
          1
        },
        {
          "TOP_CARD_ID",
          2
        },
        {
          "NAME",
          3
        },
        {
          "DESCRIPTION",
          4
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!DeckDbfRecord.\u003C\u003Ef__switch\u0024map2D.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteName((string) val);
        break;
      case 2:
        this.SetTopCardId((int) val);
        break;
      case 3:
        this.SetName((DbfLocValue) val);
        break;
      case 4:
        this.SetDescription((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckDbfRecord.\u003C\u003Ef__switch\u0024map2E == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckDbfRecord.\u003C\u003Ef__switch\u0024map2E = new Dictionary<string, int>(5)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_NAME",
            1
          },
          {
            "TOP_CARD_ID",
            2
          },
          {
            "NAME",
            3
          },
          {
            "DESCRIPTION",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckDbfRecord.\u003C\u003Ef__switch\u0024map2E.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (int);
          case 3:
            return typeof (DbfLocValue);
          case 4:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
