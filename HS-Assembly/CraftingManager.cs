﻿// Decompiled with JetBrains decompiler
// Type: CraftingManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class CraftingManager : MonoBehaviour
{
  public Vector3 m_cardCountTabShowScale = Vector3.one;
  public Vector3 m_cardCountTabHideScale = new Vector3(1f, 1f, 0.0f);
  public Transform m_floatingCardBone;
  public Transform m_faceDownCardBone;
  public Transform m_cardInfoPaneBone;
  public Transform m_cardCounterBone;
  public Transform m_showCraftingUIBone;
  public Transform m_hideCraftingUIBone;
  public BoxCollider m_offClickCatcher;
  public CraftCardCountTab m_cardCountTab;
  public PegUIElement m_dustJar;
  public float m_timeForCardToFlipUp;
  public float m_timeForBackCardToMoveUp;
  public float m_delayBeforeBackCardMovesUp;
  public iTween.EaseType m_easeTypeForCardFlip;
  public iTween.EaseType m_easeTypeForCardMoveUp;
  private static CraftingManager s_instance;
  public CraftingUI m_craftingUI;
  private Actor m_currentBigActor;
  private bool m_isCurrentActorAGhost;
  private Actor m_upsideDownActor;
  private Actor m_ghostWeaponActor;
  private Actor m_ghostMinionActor;
  private Actor m_ghostSpellActor;
  private Actor m_templateWeaponActor;
  private Actor m_templateSpellActor;
  private Actor m_templateMinionActor;
  private Actor m_templateHeroSkinActor;
  private Actor m_hiddenActor;
  private CardInfoPane m_cardInfoPane;
  private Actor m_templateGoldenWeaponActor;
  private Actor m_templateGoldenSpellActor;
  private Actor m_templateGoldenMinionActor;
  private Actor m_ghostGoldenWeaponActor;
  private Actor m_ghostGoldenSpellActor;
  private Actor m_ghostGoldenMinionActor;
  private bool m_cancellingCraftMode;
  private long m_arcaneDustBalance;
  private PendingTransaction m_pendingClientTransaction;
  private PendingTransaction m_pendingServerTransaction;
  private Vector3 m_craftSourcePosition;
  private Vector3 m_craftSourceScale;

  private void Awake()
  {
    this.m_arcaneDustBalance = NetCache.Get().GetNetObject<NetCache.NetCacheArcaneDustBalance>().Balance;
    CollectionManager.Get().RegisterMassDisenchantListener(new CollectionManager.OnMassDisenchant(this.OnMassDisenchant));
  }

  private void OnDestroy()
  {
    if (CollectionManager.Get() != null)
      CollectionManager.Get().RemoveMassDisenchantListener(new CollectionManager.OnMassDisenchant(this.OnMassDisenchant));
    CraftingManager.s_instance = (CraftingManager) null;
  }

  private void Start()
  {
    this.LoadActor("Card_Hand_Weapon", ref this.m_ghostWeaponActor, ref this.m_templateWeaponActor);
    this.LoadActor(ActorNames.GetHandActor(TAG_CARDTYPE.WEAPON, TAG_PREMIUM.GOLDEN), ref this.m_ghostGoldenWeaponActor, ref this.m_templateGoldenWeaponActor);
    this.LoadActor("Card_Hand_Ally", ref this.m_ghostMinionActor, ref this.m_templateMinionActor);
    this.LoadActor(ActorNames.GetHandActor(TAG_CARDTYPE.MINION, TAG_PREMIUM.GOLDEN), ref this.m_ghostGoldenMinionActor, ref this.m_templateGoldenMinionActor);
    this.LoadActor("Card_Hand_Ability", ref this.m_ghostSpellActor, ref this.m_templateSpellActor);
    this.LoadActor(ActorNames.GetHandActor(TAG_CARDTYPE.SPELL, TAG_PREMIUM.GOLDEN), ref this.m_ghostGoldenSpellActor, ref this.m_templateGoldenSpellActor);
    this.LoadActor("Card_Hero_Skin", ref this.m_templateHeroSkinActor);
    this.LoadActor("Card_Hidden", ref this.m_hiddenActor);
    this.m_hiddenActor.GetMeshRenderer().transform.localEulerAngles = new Vector3(0.0f, 180f, 180f);
    SceneUtils.SetLayer(this.m_hiddenActor.gameObject, GameLayer.IgnoreFullScreenEffects);
    SoundManager.Get().Load("Card_Transition_Out");
    SoundManager.Get().Load("Card_Transition_In");
  }

  public static CraftingManager Get()
  {
    if ((Object) CraftingManager.s_instance == (Object) null)
      CraftingManager.s_instance = AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "CraftingManager" : "CraftingManager_phone", true, false).GetComponent<CraftingManager>();
    return CraftingManager.s_instance;
  }

  public NetCache.CardValue GetCardValue(string cardID, TAG_PREMIUM premium)
  {
    NetCache.CardValue cardValue;
    if (!NetCache.Get().GetNetObject<NetCache.NetCacheCardValues>().Values.TryGetValue(new NetCache.CardDefinition()
    {
      Name = cardID,
      Premium = premium
    }, out cardValue))
      return (NetCache.CardValue) null;
    return cardValue;
  }

  public bool IsCardShowing()
  {
    return (Object) this.m_currentBigActor != (Object) null;
  }

  public bool GetShownCardInfo(out EntityDef entityDef, out TAG_PREMIUM premium)
  {
    entityDef = (EntityDef) null;
    premium = TAG_PREMIUM.NORMAL;
    if ((Object) this.m_currentBigActor == (Object) null)
      return false;
    entityDef = this.m_currentBigActor.GetEntityDef();
    premium = this.m_currentBigActor.GetPremium();
    return entityDef != null;
  }

  public Actor GetShownActor()
  {
    return this.m_currentBigActor;
  }

  public void OnMassDisenchant(int amount)
  {
    if ((bool) ((Object) MassDisenchant.Get()))
      return;
    this.AdjustLocalArcaneDustBalance(amount);
    this.m_craftingUI.UpdateBankText();
  }

  public long GetLocalArcaneDustBalance()
  {
    return this.m_arcaneDustBalance;
  }

  public void AdjustLocalArcaneDustBalance(int amt)
  {
    this.m_arcaneDustBalance += (long) amt;
  }

  public int GetNumClientTransactions()
  {
    if (this.m_pendingClientTransaction == null)
      return 0;
    return this.m_pendingClientTransaction.TransactionAmt;
  }

  public void NotifyOfTransaction(int amt)
  {
    if (this.m_pendingClientTransaction == null)
      return;
    this.m_pendingClientTransaction.TransactionAmt += amt;
  }

  public bool IsCancelling()
  {
    return this.m_cancellingCraftMode;
  }

  public void EnterCraftMode(Actor cardActor)
  {
    if (this.m_cancellingCraftMode || CollectionDeckTray.Get().IsWaitingToDeleteDeck())
      return;
    if ((Object) this.m_upsideDownActor != (Object) null)
      Object.Destroy((Object) this.m_upsideDownActor.gameObject);
    if ((Object) this.m_currentBigActor != (Object) null)
      Object.Destroy((Object) this.m_currentBigActor.gameObject);
    CollectionManagerDisplay.Get().HideAllTips();
    this.m_arcaneDustBalance = NetCache.Get().GetNetObject<NetCache.NetCacheArcaneDustBalance>().Balance;
    this.m_offClickCatcher.enabled = true;
    TooltipPanelManager.Get().HideKeywordHelp();
    this.MoveCardToBigSpot(cardActor);
    this.m_pendingClientTransaction = new PendingTransaction();
    this.m_pendingClientTransaction.CardID = cardActor.GetEntityDef().GetCardId();
    this.m_pendingClientTransaction.Premium = cardActor.GetPremium();
    this.m_pendingClientTransaction.TransactionAmt = 0;
    if ((Object) this.m_craftingUI == (Object) null)
    {
      this.m_craftingUI = AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "CraftingUI" : "CraftingUI_Phone", true, false).GetComponent<CraftingUI>();
      this.m_craftingUI.SetStartingActive();
      GameUtils.SetParent((Component) this.m_craftingUI, this.m_showCraftingUIBone.gameObject, false);
    }
    if ((Object) this.m_cardInfoPane == (Object) null && !(bool) UniversalInputManager.UsePhoneUI)
      this.m_cardInfoPane = AssetLoader.Get().LoadGameObject("CardInfoPane", true, false).GetComponent<CardInfoPane>();
    this.m_craftingUI.gameObject.SetActive(true);
    this.m_craftingUI.Enable(this.m_showCraftingUIBone.position, this.m_hideCraftingUIBone.position);
    this.FadeEffectsIn();
    this.UpdateCardInfoPane();
    Navigation.Push(new Navigation.NavigateBackHandler(this.CancelCraftMode));
  }

  public bool CancelCraftMode()
  {
    this.StopAllCoroutines();
    this.m_offClickCatcher.enabled = false;
    this.m_cancellingCraftMode = true;
    this.m_craftingUI.CleanUpEffects();
    float time = 0.2f;
    if ((Object) this.m_currentBigActor != (Object) null)
    {
      iTween.Stop(this.m_currentBigActor.gameObject);
      iTween.RotateTo(this.m_currentBigActor.gameObject, Vector3.zero, time);
      this.m_currentBigActor.ToggleForceIdle(false);
      if ((Object) this.m_upsideDownActor != (Object) null)
      {
        iTween.Stop(this.m_upsideDownActor.gameObject);
        this.m_upsideDownActor.transform.parent = this.m_currentBigActor.transform;
      }
      SoundManager.Get().LoadAndPlay("Card_Transition_In");
      iTween.MoveTo(this.m_currentBigActor.gameObject, iTween.Hash((object) "name", (object) "CancelCraftMode", (object) "position", (object) this.m_craftSourcePosition, (object) "time", (object) time, (object) "oncomplete", (object) "FinishActorMove", (object) "oncompletetarget", (object) this.gameObject, (object) "easetype", (object) iTween.EaseType.linear));
      iTween.ScaleTo(this.m_currentBigActor.gameObject, iTween.Hash((object) "scale", (object) this.m_craftSourceScale, (object) "time", (object) time, (object) "easetype", (object) iTween.EaseType.linear));
    }
    iTween.Stop(this.m_cardCountTab.gameObject);
    if (this.GetNumOwnedIncludePending() > 0)
    {
      iTween.MoveTo(this.m_cardCountTab.gameObject, iTween.Hash((object) "position", (object) (this.m_craftSourcePosition - new Vector3(0.0f, 12f, 0.0f)), (object) "time", (object) (float) (3.0 * (double) time), (object) "oncomplete", (object) iTween.EaseType.easeInQuad));
      iTween.ScaleTo(this.m_cardCountTab.gameObject, iTween.Hash((object) "scale", (object) (0.1f * Vector3.one), (object) "time", (object) (float) (3.0 * (double) time), (object) "oncomplete", (object) iTween.EaseType.easeInQuad));
    }
    if ((Object) this.m_upsideDownActor != (Object) null)
    {
      iTween.RotateTo(this.m_upsideDownActor.gameObject, new Vector3(0.0f, 359f, 180f), time);
      iTween.MoveTo(this.m_upsideDownActor.gameObject, iTween.Hash((object) "name", (object) "CancelCraftMode2", (object) "position", (object) new Vector3(0.0f, -1f, 0.0f), (object) "time", (object) time, (object) "islocal", (object) true));
      iTween.ScaleTo(this.m_upsideDownActor.gameObject, new Vector3(this.m_upsideDownActor.transform.localScale.x * 0.8f, this.m_upsideDownActor.transform.localScale.y * 0.8f, this.m_upsideDownActor.transform.localScale.z * 0.8f), time);
    }
    if ((Object) this.m_craftingUI != (Object) null && this.m_craftingUI.IsEnabled())
      this.m_craftingUI.Disable(this.m_hideCraftingUIBone.position);
    this.m_cardCountTab.m_shadow.GetComponent<Animation>().Play("Crafting2ndCardShadowOff");
    this.FadeEffectsOut();
    if ((Object) this.m_cardInfoPane != (Object) null)
    {
      iTween.Stop(this.m_cardInfoPane.gameObject);
      this.m_cardInfoPane.gameObject.SetActive(false);
    }
    this.TellServerAboutWhatUserDid();
    return true;
  }

  public void CreateButtonPressed()
  {
    this.m_craftingUI.DoCreate();
  }

  public void DisenchantButtonPressed()
  {
    this.m_craftingUI.DoDisenchant();
  }

  public void UpdateBankText()
  {
    if (!((Object) this.m_craftingUI != (Object) null))
      return;
    this.m_craftingUI.UpdateBankText();
  }

  private void TellServerAboutWhatUserDid()
  {
    Actor currentActor = this.GetCurrentActor();
    if ((Object) currentActor == (Object) null)
      return;
    TAG_PREMIUM premium = currentActor.GetPremium();
    string cardId = currentActor.GetEntityDef().GetCardId();
    int dbId = GameUtils.TranslateCardIdToDbId(cardId);
    Log.Crafting.Print("Final Transaction Amount = " + (object) this.m_pendingClientTransaction.TransactionAmt);
    if (this.m_pendingClientTransaction.TransactionAmt != 0)
    {
      this.m_pendingServerTransaction = new PendingTransaction();
      this.m_pendingServerTransaction.CardID = this.m_pendingClientTransaction.CardID;
      this.m_pendingServerTransaction.TransactionAmt = this.m_pendingClientTransaction.TransactionAmt;
      this.m_pendingServerTransaction.Premium = this.m_pendingClientTransaction.Premium;
    }
    int ownedCardCount = CollectionManager.Get().GetOwnedCardCount(cardId, premium);
    NetCache.CardValue cardValue = this.GetCardValue(cardId, premium);
    if (this.m_pendingClientTransaction.TransactionAmt < 0)
      Network.SellCard(dbId, premium, -this.m_pendingClientTransaction.TransactionAmt, cardValue.Sell, ownedCardCount);
    else if (this.m_pendingClientTransaction.TransactionAmt > 0)
      Network.BuyCard(dbId, premium, this.m_pendingClientTransaction.TransactionAmt, cardValue.Buy);
    this.m_pendingClientTransaction = (PendingTransaction) null;
  }

  public void OnCardGenericError(Network.CardSaleResult sale)
  {
    this.m_pendingServerTransaction = (PendingTransaction) null;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_COLLECTION_ERROR_HEADER"),
      m_text = GameStrings.Get("GLUE_COLLECTION_GENERIC_ERROR"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  public void OnCardPermissionError(Network.CardSaleResult sale)
  {
    this.m_pendingServerTransaction = (PendingTransaction) null;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_COLLECTION_ERROR_HEADER"),
      m_text = GameStrings.Get("GLUE_COLLECTION_CARD_PERMISSION_ERROR"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  public void OnCardDisenchantSoulboundError(Network.CardSaleResult sale)
  {
    this.m_pendingServerTransaction = (PendingTransaction) null;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_COLLECTION_ERROR_HEADER"),
      m_text = GameStrings.Get("GLUE_COLLECTION_CARD_SOULBOUND"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  public void OnCardCraftingEventNotActiveError(Network.CardSaleResult sale)
  {
    this.m_pendingServerTransaction = (PendingTransaction) null;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_COLLECTION_ERROR_HEADER"),
      m_text = GameStrings.Get("GLUE_COLLECTION_CARD_CRAFTING_EVENT_NOT_ACTIVE"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  public void OnCardUnknownError(Network.CardSaleResult sale)
  {
    this.m_pendingServerTransaction = (PendingTransaction) null;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_COLLECTION_ERROR_HEADER"),
      m_text = GameStrings.Format("GLUE_COLLECTION_CARD_UNKNOWN_ERROR", (object) sale.Action),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  public void OnCardDisenchanted(Network.CardSaleResult sale)
  {
    this.m_pendingServerTransaction = (PendingTransaction) null;
    NetCache.Get().OnArcaneDustBalanceChanged((long) sale.Amount);
    if (CollectionManager.Get().GetNumCopiesInCollection(sale.AssetName, sale.Premium) > 0)
      this.OnCardDisenchantedPageTransitioned((object) sale);
    CollectionManagerDisplay.Get().m_pageManager.RefreshCurrentPageContents(new CollectionPageManager.DelOnPageTransitionComplete(this.OnCardDisenchantedPageTransitioned), (object) sale);
    CollectionCardVisual cardVisual = CollectionManagerDisplay.Get().m_pageManager.GetCardVisual(sale.AssetName, sale.Premium);
    if (!((Object) cardVisual != (Object) null) || !cardVisual.IsShown())
      return;
    cardVisual.OnDoneCrafting();
  }

  public void OnCardCreated(Network.CardSaleResult sale)
  {
    this.m_pendingServerTransaction = (PendingTransaction) null;
    NetCache.Get().OnArcaneDustBalanceChanged((long) -sale.Amount);
    CollectionManagerDisplay.Get().m_pageManager.RefreshCurrentPageContents(new CollectionPageManager.DelOnPageTransitionComplete(this.OnCardCreatedPageTransitioned), (object) sale);
    CollectionCardVisual cardVisual = CollectionManagerDisplay.Get().m_pageManager.GetCardVisual(sale.AssetName, sale.Premium);
    if ((Object) cardVisual != (Object) null && cardVisual.IsShown())
      cardVisual.OnDoneCrafting();
    if (!((Object) CollectionDeckTray.Get() != (Object) null))
      return;
    for (int index = 0; index < sale.Count; ++index)
    {
      DeckTrayDeckTileVisual cardTileVisual = CollectionDeckTray.Get().m_cardsContent.GetCardTileVisual(sale.AssetName);
      if ((Object) cardTileVisual != (Object) null)
        CollectionDeckTray.Get().AddCard(!((Object) this.m_currentBigActor != (Object) null) ? cardTileVisual.GetActor().GetEntityDef() : this.m_currentBigActor.GetEntityDef(), sale.Premium, cardTileVisual, true, (Actor) null);
    }
  }

  public void LoadGhostActorIfNecessary()
  {
    if (this.m_cancellingCraftMode)
      return;
    iTween.ScaleTo(this.m_cardCountTab.gameObject, this.m_cardCountTabHideScale, 0.4f);
    if (this.GetNumOwnedIncludePending() > 0)
    {
      if ((Object) this.m_upsideDownActor == (Object) null)
      {
        this.m_currentBigActor = this.GetAndPositionNewActor(this.m_currentBigActor, 1);
        this.m_currentBigActor.name = "CurrentBigActor";
        this.m_currentBigActor.transform.position = this.m_floatingCardBone.position;
        this.m_currentBigActor.transform.localScale = this.m_floatingCardBone.localScale;
        this.m_cardCountTab.transform.position = new Vector3(0.0f, 307f, -10f);
        this.SetBigActorLayer(true);
      }
      else
      {
        this.m_upsideDownActor.transform.parent = (Transform) null;
        this.m_currentBigActor = this.m_upsideDownActor;
        this.m_currentBigActor.name = "CurrentBigActor";
        this.m_upsideDownActor = (Actor) null;
      }
    }
    else
    {
      if ((Object) this.m_upsideDownActor != (Object) null)
      {
        Log.JMac.Print("Deleting rogue m_upsideDownActor!");
        Object.Destroy((Object) this.m_upsideDownActor.gameObject);
      }
      this.m_currentBigActor = this.GetAndPositionNewActor(this.m_currentBigActor, 0);
      this.m_currentBigActor.name = "CurrentBigActor";
      this.m_currentBigActor.transform.position = this.m_floatingCardBone.position;
      this.m_currentBigActor.transform.localScale = this.m_floatingCardBone.localScale;
      this.m_cardCountTab.transform.position = new Vector3(0.0f, 307f, -10f);
      this.SetBigActorLayer(true);
    }
  }

  public Actor LoadNewActorAndConstructIt()
  {
    if (this.m_cancellingCraftMode)
      return (Actor) null;
    if (!this.m_isCurrentActorAGhost)
    {
      Actor oldActor = this.m_currentBigActor;
      if ((Object) this.m_currentBigActor == (Object) null)
        oldActor = this.m_upsideDownActor;
      else
        this.m_currentBigActor.name = "CurrentBigActor_Lost_Reference";
      this.m_currentBigActor = this.GetAndPositionNewActor(oldActor, 0);
      this.m_isCurrentActorAGhost = false;
      this.m_currentBigActor.name = "CurrentBigActor";
      this.m_currentBigActor.transform.position = this.m_floatingCardBone.position;
      this.m_currentBigActor.transform.localScale = this.m_floatingCardBone.localScale;
      this.SetBigActorLayer(true);
    }
    this.m_currentBigActor.ActivateSpellBirthState(SpellType.CONSTRUCT);
    return this.m_currentBigActor;
  }

  public void ForceNonGhostFlagOn()
  {
    this.m_isCurrentActorAGhost = false;
  }

  public void FinishCreateAnims()
  {
    if (this.m_cancellingCraftMode)
      return;
    iTween.ScaleTo(this.m_cardCountTab.gameObject, this.m_cardCountTabShowScale, 0.4f);
    this.m_currentBigActor.GetSpell(SpellType.GHOSTMODE).GetComponent<PlayMakerFSM>().SendEvent("Cancel");
    this.m_isCurrentActorAGhost = false;
    this.m_cardCountTab.UpdateText(this.GetNumOwnedIncludePending());
    this.m_cardCountTab.transform.position = this.m_cardCounterBone.position;
  }

  public void FlipCurrentActor()
  {
    if ((Object) this.m_currentBigActor == (Object) null || this.m_isCurrentActorAGhost)
      return;
    this.m_cardCountTab.transform.localScale = this.m_cardCountTabHideScale;
    this.m_upsideDownActor = this.m_currentBigActor;
    this.m_upsideDownActor.name = "UpsideDownActor";
    this.m_upsideDownActor.GetSpell(SpellType.GHOSTMODE).GetComponent<PlayMakerFSM>().SendEvent("Cancel");
    this.m_currentBigActor = (Actor) null;
    iTween.Stop(this.m_upsideDownActor.gameObject);
    iTween.RotateTo(this.m_upsideDownActor.gameObject, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, 350f, 180f), (object) "time", (object) 1f));
    iTween.MoveTo(this.m_upsideDownActor.gameObject, iTween.Hash((object) "name", (object) "FlipCurrentActor", (object) "position", (object) this.m_faceDownCardBone.position, (object) "time", (object) 1f));
    this.StartCoroutine(this.ReplaceFaceDownActorWithHiddenCard());
  }

  public void FinishFlipCurrentActorEarly()
  {
    this.StopAllCoroutines();
    if ((Object) this.m_currentBigActor != (Object) null)
      iTween.Stop(this.m_currentBigActor.gameObject);
    if ((Object) this.m_upsideDownActor != (Object) null)
      iTween.Stop(this.m_upsideDownActor.gameObject);
    this.m_currentBigActor.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
    this.m_currentBigActor.transform.position = this.m_floatingCardBone.position;
    this.m_currentBigActor.Show();
    GameObject hiddenStandIn = this.m_currentBigActor.GetHiddenStandIn();
    if ((Object) hiddenStandIn == (Object) null)
      return;
    hiddenStandIn.SetActive(false);
    Object.Destroy((Object) hiddenStandIn);
  }

  public void FlipUpsideDownCard(Actor oldActor)
  {
    if (this.m_cancellingCraftMode)
      return;
    int ownedIncludePending = this.GetNumOwnedIncludePending();
    if (ownedIncludePending > 1)
    {
      this.m_upsideDownActor = this.GetAndPositionNewUpsideDownActor(this.m_currentBigActor, false);
      this.m_upsideDownActor.name = "UpsideDownActor";
      this.StartCoroutine(this.ReplaceFaceDownActorWithHiddenCard());
    }
    if (ownedIncludePending >= 1)
    {
      iTween.ScaleTo(this.m_cardCountTab.gameObject, iTween.Hash((object) "scale", (object) this.m_cardCountTabShowScale, (object) "time", (object) 0.4f, (object) "delay", (object) this.m_timeForCardToFlipUp));
      this.m_cardCountTab.UpdateText(ownedIncludePending);
    }
    if (this.m_isCurrentActorAGhost)
      this.m_currentBigActor.gameObject.transform.position = this.m_floatingCardBone.position;
    else
      iTween.MoveTo(this.m_currentBigActor.gameObject, iTween.Hash((object) "name", (object) "FlipUpsideDownCard", (object) "position", (object) this.m_floatingCardBone.position, (object) "time", (object) this.m_timeForCardToFlipUp, (object) "easetype", (object) this.m_easeTypeForCardFlip));
    iTween.RotateTo(this.m_currentBigActor.gameObject, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, 0.0f, 0.0f), (object) "time", (object) this.m_timeForCardToFlipUp, (object) "easetype", (object) this.m_easeTypeForCardFlip));
    this.StartCoroutine(this.ReplaceHiddenCardwithRealActor(this.m_currentBigActor));
  }

  [DebuggerHidden]
  private IEnumerator ReplaceFaceDownActorWithHiddenCard()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingManager.\u003CReplaceFaceDownActorWithHiddenCard\u003Ec__Iterator4A()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator ReplaceHiddenCardwithRealActor(Actor actor)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingManager.\u003CReplaceHiddenCardwithRealActor\u003Ec__Iterator4B()
    {
      actor = actor,
      \u003C\u0024\u003Eactor = actor
    };
  }

  public PendingTransaction GetPendingClientTransaction()
  {
    return this.m_pendingClientTransaction;
  }

  public PendingTransaction GetPendingServerTransaction()
  {
    return this.m_pendingServerTransaction;
  }

  public void ShowCraftingUI(UIEvent e)
  {
    if (this.m_craftingUI.IsEnabled())
      this.m_craftingUI.Disable(this.m_hideCraftingUIBone.position);
    else
      this.m_craftingUI.Enable(this.m_showCraftingUIBone.position, this.m_hideCraftingUIBone.position);
  }

  private Actor GetCurrentActor()
  {
    if ((Object) this.m_currentBigActor != (Object) null)
      return this.m_currentBigActor;
    if ((Object) this.m_upsideDownActor != (Object) null)
      return this.m_upsideDownActor;
    return (Actor) null;
  }

  private void MoveCardToBigSpot(Actor cardActor)
  {
    if ((Object) cardActor == (Object) null)
      return;
    Actor oldActor = cardActor;
    if ((Object) oldActor == (Object) null)
      return;
    EntityDef entityDef = oldActor.GetEntityDef();
    if (entityDef == null)
      return;
    int copiesInCollection = CollectionManager.Get().GetNumCopiesInCollection(entityDef.GetCardId(), oldActor.GetPremium());
    this.m_currentBigActor = this.GetAndPositionNewActor(oldActor, copiesInCollection);
    this.m_currentBigActor.name = "CurrentBigActor";
    this.m_craftSourcePosition = oldActor.transform.position;
    this.m_craftSourceScale = oldActor.transform.lossyScale;
    this.m_craftSourceScale = Vector3.one * Mathf.Min(this.m_craftSourceScale.x, this.m_craftSourceScale.y, this.m_craftSourceScale.z);
    this.m_currentBigActor.transform.position = this.m_craftSourcePosition;
    TransformUtil.SetWorldScale((Component) this.m_currentBigActor, this.m_craftSourceScale);
    this.SetBigActorLayer(true);
    this.m_currentBigActor.ToggleForceIdle(true);
    this.m_currentBigActor.SetActorState(ActorStateType.CARD_IDLE);
    if (entityDef.IsHero())
    {
      this.m_cardCountTab.gameObject.SetActive(false);
    }
    else
    {
      this.m_cardCountTab.gameObject.SetActive(true);
      if (copiesInCollection > 1)
      {
        this.m_upsideDownActor = this.GetAndPositionNewUpsideDownActor(oldActor, true);
        this.m_upsideDownActor.name = "UpsideDownActor";
        this.StartCoroutine(this.ReplaceFaceDownActorWithHiddenCard());
      }
      if (copiesInCollection > 0)
      {
        this.m_cardCountTab.UpdateText(copiesInCollection);
        this.m_cardCountTab.transform.position = new Vector3(oldActor.transform.position.x, oldActor.transform.position.y - 2f, oldActor.transform.position.z);
      }
    }
    this.FinishBigCardMove();
  }

  private void FinishBigCardMove()
  {
    if ((Object) this.m_currentBigActor == (Object) null)
      return;
    int ownedIncludePending = this.GetNumOwnedIncludePending();
    SoundManager.Get().LoadAndPlay("Card_Transition_Out");
    iTween.MoveTo(this.m_currentBigActor.gameObject, iTween.Hash((object) "name", (object) "FinishBigCardMove", (object) "position", (object) this.m_floatingCardBone.position, (object) "time", (object) 0.4f));
    iTween.ScaleTo(this.m_currentBigActor.gameObject, iTween.Hash((object) "scale", (object) this.m_floatingCardBone.localScale, (object) "time", (object) 0.4f, (object) "easetype", (object) iTween.EaseType.easeOutQuad));
    if (ownedIncludePending <= 0)
      return;
    this.m_cardCountTab.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    iTween.MoveTo(this.m_cardCountTab.gameObject, this.m_cardCounterBone.position, 0.4f);
    iTween.ScaleTo(this.m_cardCountTab.gameObject, this.m_cardCountTabShowScale, 0.4f);
  }

  private void UpdateCardInfoPane()
  {
    if ((Object) this.m_cardInfoPane == (Object) null)
      return;
    this.m_cardInfoPane.gameObject.SetActive(true);
    this.m_cardInfoPane.UpdateContent();
    this.m_cardInfoPane.transform.position = this.m_currentBigActor.transform.position - new Vector3(0.0f, 1f, 0.0f);
    Vector3 localScale = this.m_cardInfoPaneBone.localScale;
    this.m_cardInfoPane.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    iTween.MoveTo(this.m_cardInfoPane.gameObject, this.m_cardInfoPaneBone.position, 0.5f);
    iTween.ScaleTo(this.m_cardInfoPane.gameObject, localScale, 0.5f);
  }

  private void FinishActorMove()
  {
    this.m_cancellingCraftMode = false;
    iTween.Stop(this.m_cardCountTab.gameObject);
    this.m_cardCountTab.transform.position = new Vector3(0.0f, 307f, -10f);
    if ((Object) this.m_upsideDownActor != (Object) null)
      Object.Destroy((Object) this.m_upsideDownActor.gameObject);
    if (!((Object) this.m_currentBigActor != (Object) null))
      return;
    Object.Destroy((Object) this.m_currentBigActor.gameObject);
  }

  private void FadeEffectsIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.SetBlurBrightness(1f);
    fullScreenFxMgr.SetBlurDesaturation(0.0f);
    fullScreenFxMgr.Vignette(0.4f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.Blur(1f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void FadeEffectsOut()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, new FullScreenFXMgr.EffectListener(this.OnVignetteFinished));
    fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void OnVignetteFinished()
  {
    this.SetBigActorLayer(false);
    if ((Object) this.GetCurrentCardVisual() != (Object) null)
      this.GetCurrentCardVisual().OnDoneCrafting();
    if ((Object) this.m_currentBigActor != (Object) null)
    {
      this.m_currentBigActor.name = "USED_TO_BE_CurrentBigActor";
      this.StartCoroutine(this.MakeSureActorIsCleanedUp(this.m_currentBigActor));
    }
    this.m_currentBigActor = (Actor) null;
    this.m_craftingUI.gameObject.SetActive(false);
  }

  [DebuggerHidden]
  private IEnumerator MakeSureActorIsCleanedUp(Actor oldActor)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingManager.\u003CMakeSureActorIsCleanedUp\u003Ec__Iterator4C()
    {
      oldActor = oldActor,
      \u003C\u0024\u003EoldActor = oldActor
    };
  }

  private Actor GetAndPositionNewUpsideDownActor(Actor oldActor, bool fromPage)
  {
    Actor positionNewActor = this.GetAndPositionNewActor(oldActor, 1);
    SceneUtils.SetLayer(positionNewActor.gameObject, GameLayer.IgnoreFullScreenEffects);
    if (fromPage)
    {
      positionNewActor.transform.position = oldActor.transform.position + new Vector3(0.0f, -2f, 0.0f);
      positionNewActor.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 180f);
      iTween.RotateTo(positionNewActor.gameObject, new Vector3(0.0f, 350f, 180f), 0.4f);
      iTween.MoveTo(positionNewActor.gameObject, iTween.Hash((object) "name", (object) "GetAndPositionNewUpsideDownActor", (object) "position", (object) this.m_faceDownCardBone.position, (object) "time", (object) 0.4f));
      iTween.ScaleTo(positionNewActor.gameObject, this.m_faceDownCardBone.localScale, 0.4f);
    }
    else
    {
      positionNewActor.transform.localEulerAngles = new Vector3(0.0f, 350f, 180f);
      positionNewActor.transform.position = this.m_faceDownCardBone.position + new Vector3(0.0f, -6f, 0.0f);
      positionNewActor.transform.localScale = this.m_faceDownCardBone.localScale;
      iTween.MoveTo(positionNewActor.gameObject, iTween.Hash((object) "name", (object) "GetAndPositionNewUpsideDownActor", (object) "position", (object) this.m_faceDownCardBone.position, (object) "time", (object) this.m_timeForBackCardToMoveUp, (object) "easetype", (object) this.m_easeTypeForCardMoveUp, (object) "delay", (object) this.m_delayBeforeBackCardMovesUp));
    }
    return positionNewActor;
  }

  private Actor GetAndPositionNewActor(Actor oldActor, int numCopies)
  {
    Actor actor = numCopies != 0 ? this.GetNonGhostActor(oldActor) : this.GetGhostActor(oldActor);
    actor.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    return actor;
  }

  private Actor GetGhostActor(Actor actor)
  {
    this.m_isCurrentActorAGhost = true;
    bool flag = actor.GetPremium() == TAG_PREMIUM.GOLDEN;
    Actor templateActor = this.m_ghostMinionActor;
    switch (actor.GetEntityDef().GetCardType())
    {
      case TAG_CARDTYPE.MINION:
        templateActor = !flag ? this.m_ghostMinionActor : this.m_ghostGoldenMinionActor;
        break;
      case TAG_CARDTYPE.SPELL:
        templateActor = !flag ? this.m_ghostSpellActor : this.m_ghostGoldenSpellActor;
        break;
      case TAG_CARDTYPE.WEAPON:
        templateActor = !flag ? this.m_ghostWeaponActor : this.m_ghostGoldenWeaponActor;
        break;
      default:
        UnityEngine.Debug.LogError((object) "CraftingManager.GetGhostActor() - tried to get a ghost actor for a cardtype that we haven't anticipated!!");
        break;
    }
    return this.SetUpGhostActor(templateActor, actor);
  }

  private Actor GetNonGhostActor(Actor actor)
  {
    this.m_isCurrentActorAGhost = false;
    return this.SetUpNonGhostActor(this.GetTemplateActor(actor), actor);
  }

  private Actor GetTemplateActor(Actor actor)
  {
    bool flag = actor.GetPremium() == TAG_PREMIUM.GOLDEN;
    switch (actor.GetEntityDef().GetCardType())
    {
      case TAG_CARDTYPE.HERO:
        return this.m_templateHeroSkinActor;
      case TAG_CARDTYPE.MINION:
        if (flag)
          return this.m_templateGoldenMinionActor;
        return this.m_templateMinionActor;
      case TAG_CARDTYPE.SPELL:
        if (flag)
          return this.m_templateGoldenSpellActor;
        return this.m_templateSpellActor;
      case TAG_CARDTYPE.WEAPON:
        if (flag)
          return this.m_templateGoldenWeaponActor;
        return this.m_templateWeaponActor;
      default:
        UnityEngine.Debug.LogError((object) "CraftingManager.GetGhostActor() - tried to get a ghost actor for a cardtype that we haven't anticipated!!");
        return this.m_templateMinionActor;
    }
  }

  private Actor SetUpNonGhostActor(Actor templateActor, Actor actor)
  {
    Actor actor1 = Object.Instantiate<Actor>(templateActor);
    actor1.SetEntityDef(actor.GetEntityDef());
    actor1.SetPremium(actor.GetPremium());
    actor1.SetCardDef(actor.GetCardDef());
    actor1.UpdateAllComponents();
    return actor1;
  }

  private Actor SetUpGhostActor(Actor templateActor, Actor actor)
  {
    Actor actorToShow = Object.Instantiate<Actor>(templateActor);
    actorToShow.SetEntityDef(actor.GetEntityDef());
    actorToShow.SetPremium(actor.GetPremium());
    actorToShow.SetCardDef(actor.GetCardDef());
    actorToShow.UpdateAllComponents();
    actorToShow.UpdatePortraitTexture();
    actorToShow.UpdateCardColor();
    actorToShow.Hide();
    if (actor.isMissingCard())
      actorToShow.ActivateSpellBirthState(SpellType.MISSING_BIGCARD);
    else
      actorToShow.ActivateSpellBirthState(SpellType.GHOSTMODE);
    this.StartCoroutine(this.ShowAfterTwoFrames(actorToShow));
    return actorToShow;
  }

  [DebuggerHidden]
  private IEnumerator ShowAfterTwoFrames(Actor actorToShow)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingManager.\u003CShowAfterTwoFrames\u003Ec__Iterator4D()
    {
      actorToShow = actorToShow,
      \u003C\u0024\u003EactorToShow = actorToShow,
      \u003C\u003Ef__this = this
    };
  }

  private void SetBigActorLayer(bool inCraftingMode)
  {
    if ((Object) this.m_currentBigActor == (Object) null)
      return;
    SceneUtils.SetLayer(this.m_currentBigActor.gameObject, !inCraftingMode ? GameLayer.CardRaycast : GameLayer.IgnoreFullScreenEffects);
  }

  private void OnCardCreatedPageTransitioned(object callbackData)
  {
    CollectionManagerDisplay.Get().UpdateCurrentPageCardLocks(false);
  }

  private void OnCardDisenchantedPageTransitioned(object callbackData)
  {
    CollectionManagerDisplay.Get().UpdateCurrentPageCardLocks(false);
  }

  private CollectionCardVisual GetCurrentCardVisual()
  {
    EntityDef entityDef;
    TAG_PREMIUM premium;
    if (!this.GetShownCardInfo(out entityDef, out premium))
      return (CollectionCardVisual) null;
    return CollectionManagerDisplay.Get().m_pageManager.GetCardVisual(entityDef.GetCardId(), premium);
  }

  public int GetNumOwnedIncludePending(TAG_PREMIUM premium)
  {
    string cardId = this.GetCurrentActor().GetEntityDef().GetCardId();
    int copiesInCollection = CollectionManager.Get().GetNumCopiesInCollection(cardId, premium);
    if (this.m_pendingClientTransaction != null && this.m_pendingClientTransaction.CardID == cardId && this.m_pendingClientTransaction.Premium == premium)
      return copiesInCollection + this.m_pendingClientTransaction.TransactionAmt;
    return copiesInCollection;
  }

  public int GetNumOwnedIncludePending()
  {
    Actor currentActor = this.GetCurrentActor();
    string cardId = currentActor.GetEntityDef().GetCardId();
    TAG_PREMIUM premium = currentActor.GetPremium();
    int copiesInCollection = CollectionManager.Get().GetNumCopiesInCollection(cardId, premium);
    if (this.m_pendingClientTransaction != null && this.m_pendingClientTransaction.CardID == cardId && this.m_pendingClientTransaction.Premium == premium)
      return copiesInCollection + this.m_pendingClientTransaction.TransactionAmt;
    return copiesInCollection;
  }

  private void LoadActor(string actorName, ref Actor actor)
  {
    GameObject gameObject = AssetLoader.Get().LoadActor(actorName, false, false);
    gameObject.transform.position = new Vector3(-99999f, 99999f, 99999f);
    actor = gameObject.GetComponent<Actor>();
    actor.TurnOffCollider();
  }

  private void LoadActor(string actorName, ref Actor actor, ref Actor actorCopy)
  {
    GameObject gameObject = AssetLoader.Get().LoadActor(actorName, false, false);
    gameObject.transform.position = new Vector3(-99999f, 99999f, 99999f);
    actor = gameObject.GetComponent<Actor>();
    actorCopy = Object.Instantiate<Actor>(actor);
    actor.TurnOffCollider();
    actorCopy.TurnOffCollider();
  }
}
