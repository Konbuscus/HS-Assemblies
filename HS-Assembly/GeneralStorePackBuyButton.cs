﻿// Decompiled with JetBrains decompiler
// Type: GeneralStorePackBuyButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class GeneralStorePackBuyButton : PegUIElement
{
  public List<Renderer> m_buttonRenderers = new List<Renderer>();
  public string m_materialPropName = "_MainTex";
  public UberText m_quantityText;
  public UberText m_costText;
  public UberText m_fullText;
  public Color m_goldQuantityTextColor;
  public Color m_moneyQuantityTextColor;
  public Color m_goldCostTextColor;
  public Color m_moneyCostTextColor;
  public GameObject m_goldIcon;
  public GameObject m_selectGlow;
  public int m_materialIndex;
  public Vector2 m_goldBtnMatOffset;
  public Vector2 m_goldBtnDownMatOffset;
  public Vector2 m_moneyBtnMatOffset;
  public Vector2 m_moneyBtnDownMatOffset;
  private bool m_selected;
  private bool m_isGold;

  public bool IsSelected()
  {
    return this.m_selected;
  }

  public void Select()
  {
    if (this.m_selected)
      return;
    this.m_selected = true;
    this.UpdateButtonState();
  }

  public void Unselect()
  {
    if (!this.m_selected)
      return;
    this.m_selected = false;
    this.UpdateButtonState();
  }

  public void UpdateFromGTAPP(NoGTAPPTransactionData noGTAPPGoldPrice)
  {
    string quantityText = string.Empty;
    long cost;
    if (StoreManager.Get().GetGoldCostNoGTAPP(noGTAPPGoldPrice, out cost))
      quantityText = StoreManager.Get().GetProductQuantityText(noGTAPPGoldPrice.Product, noGTAPPGoldPrice.ProductData, noGTAPPGoldPrice.Quantity);
    this.SetGoldValue(cost, quantityText);
  }

  public void UpdateFromMoneyBundle(Network.Bundle bundle)
  {
    Network.BundleItem bundleItem = bundle.Items[0];
    string productQuantityText = StoreManager.Get().GetProductQuantityText(bundleItem.Product, bundleItem.ProductData, bundleItem.Quantity);
    this.SetMoneyValue(bundle, productQuantityText);
  }

  public void SetGoldValue(long goldCost, string quantityText)
  {
    if ((Object) this.m_fullText != (Object) null)
    {
      this.m_quantityText.gameObject.SetActive(true);
      this.m_costText.gameObject.SetActive(true);
      this.m_fullText.gameObject.SetActive(false);
    }
    this.m_costText.Text = goldCost.ToString();
    this.m_costText.TextColor = this.m_goldCostTextColor;
    this.m_quantityText.Text = quantityText;
    this.m_quantityText.TextColor = this.m_goldQuantityTextColor;
    this.m_isGold = true;
    this.UpdateButtonState();
  }

  public void SetMoneyValue(Network.Bundle bundle, string quantityText)
  {
    if (bundle != null && !StoreManager.Get().IsProductAlreadyOwned(bundle))
    {
      if ((Object) this.m_fullText != (Object) null)
      {
        this.m_quantityText.gameObject.SetActive(true);
        this.m_costText.gameObject.SetActive(true);
        this.m_fullText.gameObject.SetActive(false);
      }
      this.m_costText.Text = StoreManager.Get().FormatCostBundle(bundle);
      this.m_costText.TextColor = this.m_moneyCostTextColor;
      this.m_quantityText.Text = quantityText;
      this.m_quantityText.TextColor = this.m_moneyQuantityTextColor;
    }
    else
    {
      this.m_costText.Text = string.Empty;
      UberText uberText = this.m_quantityText;
      if ((Object) this.m_fullText != (Object) null)
      {
        this.m_quantityText.gameObject.SetActive(false);
        this.m_costText.gameObject.SetActive(false);
        this.m_fullText.gameObject.SetActive(true);
        uberText = this.m_fullText;
      }
      uberText.Text = GameStrings.Get("GLUE_STORE_PACK_BUTTON_TEXT_PURCHASED");
    }
    this.m_isGold = false;
    this.UpdateButtonState();
  }

  private void UpdateButtonState()
  {
    if ((Object) this.m_goldIcon != (Object) null)
      this.m_goldIcon.SetActive(this.m_isGold);
    Vector2 zero = Vector2.zero;
    Vector2 offset = !this.m_isGold ? (!this.m_selected ? this.m_moneyBtnMatOffset : this.m_moneyBtnDownMatOffset) : (!this.m_selected ? this.m_goldBtnMatOffset : this.m_goldBtnDownMatOffset);
    using (List<Renderer>.Enumerator enumerator = this.m_buttonRenderers.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.materials[this.m_materialIndex].SetTextureOffset(this.m_materialPropName, offset);
    }
    if (!((Object) this.m_selectGlow != (Object) null))
      return;
    this.m_selectGlow.SetActive(this.m_selected);
  }

  protected override void OnDoubleClick()
  {
  }
}
