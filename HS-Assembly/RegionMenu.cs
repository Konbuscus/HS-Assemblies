﻿// Decompiled with JetBrains decompiler
// Type: RegionMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class RegionMenu : ButtonListMenu
{
  protected string m_menuDefPrefabOverride = "ButtonListMenuDef_RegionMenu";
  public Transform m_menuBone;
  private List<UIBButton> m_buttons;

  protected override void Awake()
  {
    Debug.Log((object) "region menu awake!");
    this.m_menuDefPrefab = this.m_menuDefPrefabOverride;
    this.m_menuParent = this.m_menuBone;
    base.Awake();
    this.SetTransform();
    this.m_menu.m_headerText.Text = GameStrings.Get("GLUE_PICK_A_REGION");
  }

  public void SetButtons(List<UIBButton> buttons)
  {
    this.m_buttons = buttons;
  }

  public override void Show()
  {
    base.Show();
    SplashScreen.Get().HideWebAuth();
  }

  public override void Hide()
  {
    base.Hide();
    SplashScreen.Get().UnHideWebAuth();
    Object.Destroy((Object) this.gameObject);
  }

  protected override List<UIBButton> GetButtons()
  {
    return this.m_buttons;
  }
}
