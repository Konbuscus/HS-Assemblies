﻿// Decompiled with JetBrains decompiler
// Type: ThreeSliceElement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class ThreeSliceElement : MonoBehaviour
{
  public float m_middleScale = 1f;
  private Vector3 m_initialScale = Vector3.zero;
  public GameObject m_left;
  public GameObject m_middle;
  public GameObject m_right;
  public ThreeSliceElement.PinnedPoint m_pinnedPoint;
  public Vector3 m_pinnedPointOffset;
  public ThreeSliceElement.Direction m_direction;
  public float m_width;
  public Vector3 m_leftOffset;
  public Vector3 m_middleOffset;
  public Vector3 m_rightOffset;
  private Bounds m_initialMiddleBounds;

  private void Awake()
  {
    if (!(bool) ((Object) this.m_middle))
      return;
    this.SetInitialValues();
  }

  public void UpdateDisplay()
  {
    if (!this.enabled)
      return;
    if (this.m_initialMiddleBounds.size == Vector3.zero)
      this.m_initialMiddleBounds = this.m_middle.GetComponent<Renderer>().bounds;
    float num = this.m_width - (this.m_left.GetComponent<Renderer>().bounds.size.x + this.m_right.GetComponent<Renderer>().bounds.size.x);
    switch (this.m_direction)
    {
      case ThreeSliceElement.Direction.X:
        Vector3 worldScale = TransformUtil.ComputeWorldScale((Component) this.m_middle.transform);
        worldScale.x = this.m_initialScale.x * num / this.m_initialMiddleBounds.size.x;
        TransformUtil.SetWorldScale((Component) this.m_middle.transform, worldScale);
        break;
    }
    switch (this.m_pinnedPoint)
    {
      case ThreeSliceElement.PinnedPoint.LEFT:
        this.m_left.transform.localPosition = this.m_pinnedPointOffset;
        TransformUtil.SetPoint(this.m_middle, Anchor.LEFT, this.m_left, Anchor.RIGHT, this.m_middleOffset);
        TransformUtil.SetPoint(this.m_right, Anchor.LEFT, this.m_middle, Anchor.RIGHT, this.m_rightOffset);
        break;
      case ThreeSliceElement.PinnedPoint.MIDDLE:
        this.m_middle.transform.localPosition = this.m_pinnedPointOffset;
        TransformUtil.SetPoint(this.m_left, Anchor.RIGHT, this.m_middle, Anchor.LEFT, this.m_leftOffset);
        TransformUtil.SetPoint(this.m_right, Anchor.LEFT, this.m_middle, Anchor.RIGHT, this.m_rightOffset);
        break;
      case ThreeSliceElement.PinnedPoint.RIGHT:
        this.m_right.transform.localPosition = this.m_pinnedPointOffset;
        TransformUtil.SetPoint(this.m_middle, Anchor.RIGHT, this.m_right, Anchor.LEFT, this.m_middleOffset);
        TransformUtil.SetPoint(this.m_left, Anchor.RIGHT, this.m_middle, Anchor.LEFT, this.m_leftOffset);
        break;
    }
  }

  public void SetWidth(float globalWidth)
  {
    this.m_width = globalWidth;
    this.UpdateDisplay();
  }

  public void SetMiddleWidth(float globalWidth)
  {
    this.m_width = globalWidth + this.m_left.GetComponent<Renderer>().bounds.size.x + this.m_right.GetComponent<Renderer>().bounds.size.x;
    this.UpdateDisplay();
  }

  public Vector3 GetMiddleSize()
  {
    return this.m_middle.GetComponent<Renderer>().bounds.size;
  }

  public Vector3 GetSize()
  {
    return this.GetSize(true);
  }

  public Vector3 GetSize(bool zIsHeight)
  {
    Vector3 size1 = this.m_left.GetComponent<Renderer>().bounds.size;
    Vector3 size2 = this.m_middle.GetComponent<Renderer>().bounds.size;
    Vector3 size3 = this.m_right.GetComponent<Renderer>().bounds.size;
    float x = size1.x + size3.x + size2.x;
    float num1 = Mathf.Max(Mathf.Max(size1.z, size2.z), size3.z);
    float num2 = Mathf.Max(Mathf.Max(size1.y, size2.y), size3.y);
    if (zIsHeight)
      return new Vector3(x, num1, num2);
    return new Vector3(x, num2, num1);
  }

  public void SetInitialValues()
  {
    this.m_initialMiddleBounds = this.m_middle.GetComponent<Renderer>().bounds;
    this.m_initialScale = this.m_middle.transform.lossyScale;
    this.m_width = this.m_middle.GetComponent<Renderer>().bounds.size.x + this.m_left.GetComponent<Renderer>().bounds.size.x + this.m_right.GetComponent<Renderer>().bounds.size.x;
  }

  public enum PinnedPoint
  {
    LEFT,
    MIDDLE,
    RIGHT,
    TOP,
    BOTTOM,
  }

  public enum Direction
  {
    X,
    Y,
    Z,
  }
}
