﻿// Decompiled with JetBrains decompiler
// Type: DeckRule_CountCardsInDeck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DeckRule_CountCardsInDeck : DeckRule
{
  public DeckRule_CountCardsInDeck(int min, int max)
  {
    this.m_ruleType = DeckRule.RuleType.COUNT_CARDS_IN_DECK;
    this.m_minValue = min;
    this.m_maxValue = max;
  }

  public DeckRule_CountCardsInDeck(DeckRulesetRuleDbfRecord record)
    : base(DeckRule.RuleType.COUNT_CARDS_IN_DECK, record)
  {
    if (this.m_appliesToSubset != null)
      return;
    Debug.LogError((object) "COUNT_CARDS_IN_DECK only supports rules with a defined \"applies to\" subset");
  }

  public override bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason)
  {
    reason = (RuleInvalidReason) null;
    int cardCountInSet = deck.GetCardCountInSet(this.m_appliesToSubset, this.m_appliesToIsNot);
    int countParam = 0;
    bool isMinimum = false;
    bool val = true;
    if (cardCountInSet < this.m_minValue)
    {
      val = false;
      countParam = this.m_minValue - cardCountInSet;
      isMinimum = true;
    }
    else if (cardCountInSet > this.m_maxValue)
    {
      val = false;
      countParam = cardCountInSet - this.m_maxValue;
    }
    bool result = this.GetResult(val);
    if (!result)
      reason = new RuleInvalidReason(this.m_errorString, countParam, isMinimum);
    return result;
  }
}
