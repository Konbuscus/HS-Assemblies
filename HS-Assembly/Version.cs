﻿// Decompiled with JetBrains decompiler
// Type: Version
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public abstract class Version
{
  public const int version = 15590;
  public const int clientChangelist = 301008;
  public const string androidTextureCompression = "";
  public const string cosmeticVersion = "7.0";
  private static int clientChangelist_;
  private static string serverChangelist_;
  private static string bobNetAddress_;
  private static string report_;

  public static string FullReport
  {
    get
    {
      if (string.IsNullOrEmpty(Version.report_))
        Version.createReport();
      return Version.report_;
    }
  }

  public static string serverChangelist
  {
    get
    {
      return Version.serverChangelist_ ?? string.Empty;
    }
    set
    {
      Version.serverChangelist_ = string.Format(", server {0}", (object) value);
      Version.Reset();
    }
  }

  public static string bobNetAddress
  {
    get
    {
      return Version.bobNetAddress_ ?? string.Empty;
    }
    set
    {
      Version.bobNetAddress_ = string.Format(", Battle.net {0}", (object) value);
      Version.Reset();
    }
  }

  public static void Reset()
  {
    Version.report_ = string.Empty;
  }

  private static void createReport()
  {
    Version.report_ = string.Format("Version {0} (client {1}{2}{3})", (object) 15590, (object) 301008, (object) Version.serverChangelist, (object) Version.bobNetAddress);
  }
}
