﻿// Decompiled with JetBrains decompiler
// Type: SetRenderQue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (Renderer))]
public class SetRenderQue : MonoBehaviour
{
  public int queue = 1;
  public bool includeChildren;
  public int[] queues;

  private void Start()
  {
    if (this.includeChildren)
    {
      foreach (Renderer componentsInChild in this.GetComponentsInChildren<Renderer>())
      {
        if (!((Object) componentsInChild == (Object) null))
        {
          foreach (Material material in componentsInChild.materials)
          {
            if (!((Object) material == (Object) null))
              material.renderQueue += this.queue;
          }
        }
      }
    }
    else
    {
      if ((Object) this.GetComponent<Renderer>() == (Object) null)
        return;
      this.GetComponent<Renderer>().material.renderQueue += this.queue;
    }
    if (this.queues == null || (Object) this.GetComponent<Renderer>() == (Object) null)
      return;
    Material[] materials = this.GetComponent<Renderer>().materials;
    if (materials == null)
      return;
    int length = materials.Length;
    for (int index = 0; index < this.queues.Length && index < length; ++index)
    {
      Material material = this.GetComponent<Renderer>().materials[index];
      if (!((Object) material == (Object) null))
      {
        if (this.queues[index] < 0)
          Debug.LogWarning((object) string.Format("WARNING: Using negative renderQueue for {0}'s {1} (renderQueue = {2})", (object) this.transform.root.name, (object) this.gameObject.name, (object) this.queues[index]));
        material.renderQueue += this.queues[index];
      }
    }
  }
}
