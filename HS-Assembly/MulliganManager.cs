﻿// Decompiled with JetBrains decompiler
// Type: MulliganManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class MulliganManager : MonoBehaviour
{
  private int m_coinCardIndex = -1;
  private int m_bonusCardIndex = -1;
  private bool[] m_handCardsMarkedForReplace = new bool[4];
  private const float PHONE_HEIGHT_OFFSET = 7f;
  private const float PHONE_CARD_Z_OFFSET = 0.2f;
  private const float PHONE_CARD_SCALE = 0.9f;
  private const float PHONE_ZONE_SIZE_ADJUST = 0.55f;
  private const float ANIMATION_TIME_DEAL_CARD = 1.5f;
  private const float DEFAULT_STARTING_TAUNT_DURATION = 2.5f;
  public AnimationClip cardAnimatesFromBoardToDeck;
  public AnimationClip cardAnimatesFromBoardToDeck_iPhone;
  public AnimationClip cardAnimatesFromTableToSky;
  public AnimationClip cardAnimatesFromDeckToBoard;
  public AnimationClip shuffleDeck;
  public AnimationClip myheroAnimatesToPosition;
  public AnimationClip hisheroAnimatesToPosition;
  public AnimationClip myheroAnimatesToPosition_iPhone;
  public AnimationClip hisheroAnimatesToPosition_iPhone;
  public GameObject coinPrefab;
  public GameObject weldPrefab;
  public GameObject mulliganChooseBannerPrefab;
  public GameObject mulliganKeepLabelPrefab;
  public MulliganReplaceLabel mulliganReplaceLabelPrefab;
  public GameObject mulliganXlabelPrefab;
  public GameObject mulliganTimerPrefab;
  public GameObject heroLabelPrefab;
  private static MulliganManager s_instance;
  private bool mulliganActive;
  private MulliganTimer m_mulliganTimer;
  private NormalButton mulliganButton;
  private GameObject myWeldEffect;
  private GameObject hisWeldEffect;
  private GameObject coinObject;
  private GameObject startingHandZone;
  private GameObject coinTossText;
  private ZoneHand friendlySideHandZone;
  private ZoneHand opposingSideHandZone;
  private ZoneDeck friendlySideDeck;
  private ZoneDeck opposingSideDeck;
  private Actor myHeroCardActor;
  private Actor hisHeroCardActor;
  private Actor myHeroPowerCardActor;
  private Actor hisHeroPowerCardActor;
  private bool waitingForVersusText;
  private GameStartVsLetters versusText;
  private bool waitingForVersusVo;
  private AudioSource versusVo;
  private bool introComplete;
  private bool skipCardChoosing;
  private List<Card> m_startingCards;
  private List<Card> m_startingOppCards;
  private GameObject mulliganChooseBanner;
  private List<MulliganReplaceLabel> m_replaceLabels;
  private GameObject[] m_xLabels;
  private Vector3 coinLocation;
  private bool friendlyPlayerGoesFirst;
  private HeroLabel myheroLabel;
  private HeroLabel hisheroLabel;
  private Spell m_MyCustomSocketInSpell;
  private Spell m_HisCustomSocketInSpell;
  private bool m_isLoadingMyCustomSocketIn;
  private bool m_isLoadingHisCustomSocketIn;
  private bool friendlyPlayerHasReplacementCards;
  private bool opponentPlayerHasReplacementCards;
  private bool m_waitingForUserInput;
  private Notification innkeeperMulliganDialog;
  private bool m_resuming;
  private Coroutine m_customIntro;

  private void Awake()
  {
    MulliganManager.s_instance = this;
  }

  private void OnDestroy()
  {
    if (GameState.Get() != null)
    {
      GameState.Get().UnregisterCreateGameListener(new GameState.CreateGameCallback(this.OnCreateGame));
      GameState.Get().UnregisterMulliganTimerUpdateListener(new GameState.TurnTimerUpdateCallback(this.OnMulliganTimerUpdate));
      GameState.Get().UnregisterEntitiesChosenReceivedListener(new GameState.EntitiesChosenReceivedCallback(this.OnEntitiesChosenReceived));
      GameState.Get().UnregisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
    }
    MulliganManager.s_instance = (MulliganManager) null;
  }

  private void Start()
  {
    if (GameState.Get().IsGameCreatedOrCreating())
      this.HandleGameStart();
    else
      GameState.Get().RegisterCreateGameListener(new GameState.CreateGameCallback(this.OnCreateGame));
    GameState.Get().RegisterMulliganTimerUpdateListener(new GameState.TurnTimerUpdateCallback(this.OnMulliganTimerUpdate));
    GameState.Get().RegisterEntitiesChosenReceivedListener(new GameState.EntitiesChosenReceivedCallback(this.OnEntitiesChosenReceived));
    GameState.Get().RegisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.myheroAnimatesToPosition = this.myheroAnimatesToPosition_iPhone;
    this.hisheroAnimatesToPosition = this.hisheroAnimatesToPosition_iPhone;
    this.cardAnimatesFromBoardToDeck = this.cardAnimatesFromBoardToDeck_iPhone;
  }

  public static MulliganManager Get()
  {
    return MulliganManager.s_instance;
  }

  public bool IsMulliganActive()
  {
    return this.mulliganActive;
  }

  public void ForceMulliganActive(bool active)
  {
    this.mulliganActive = active;
  }

  [DebuggerHidden]
  private IEnumerator WaitForBoardThenLoadButton()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CWaitForBoardThenLoadButton\u003Ec__IteratorC7() { \u003C\u003Ef__this = this };
  }

  private void OnMulliganButtonLoaded(string name, GameObject go, object userData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("MulliganManager.OnMulliganButtonLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      this.mulliganButton = go.GetComponent<NormalButton>();
      if ((UnityEngine.Object) this.mulliganButton == (UnityEngine.Object) null)
        UnityEngine.Debug.LogError((object) string.Format("MulliganManager.OnMulliganButtonLoaded() - ERROR \"{0}\" has no {1} component", (object) name, (object) typeof (NormalButton)));
      else
        this.mulliganButton.SetText(GameStrings.Get("GLOBAL_CONFIRM"));
    }
  }

  private void OnVersusVoLoaded(string name, GameObject go, object userData)
  {
    this.waitingForVersusVo = false;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("MulliganManager.OnVersusVoLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      this.versusVo = go.GetComponent<AudioSource>();
      if (!((UnityEngine.Object) this.versusVo == (UnityEngine.Object) null))
        return;
      UnityEngine.Debug.LogError((object) string.Format("MulliganManager.OnVersusVoLoaded() - ERROR \"{0}\" has no {1} component", (object) name, (object) typeof (AudioSource)));
    }
  }

  private void OnVersusTextLoaded(string name, GameObject go, object userData)
  {
    this.waitingForVersusText = false;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("MulliganManager.OnVersusTextLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      this.versusText = go.GetComponent<GameStartVsLetters>();
      if (!((UnityEngine.Object) this.versusText == (UnityEngine.Object) null))
        return;
      Log.All.PrintError("MulliganManager.OnVersusTextLoaded() object loaded does not have a GameStartVsLetters component");
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitForHeroesAndStartAnimations()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CWaitForHeroesAndStartAnimations\u003Ec__IteratorC8() { \u003C\u003Ef__this = this };
  }

  public void BeginMulligan()
  {
    bool mulliganActive = this.mulliganActive;
    this.mulliganActive = true;
    if (GameState.Get().WasConcedeRequested())
    {
      this.HandleGameOverDuringMulligan();
    }
    else
    {
      if (mulliganActive && SpectatorManager.Get().IsSpectatingOpposingSide())
        return;
      this.StartCoroutine(this.ContinueMulliganWhenBoardLoads());
    }
  }

  private void OnCreateGame(GameState.CreateGamePhase phase, object userData)
  {
    GameState.Get().UnregisterCreateGameListener(new GameState.CreateGameCallback(this.OnCreateGame));
    this.HandleGameStart();
  }

  private void HandleGameStart()
  {
    Log.LoadingScreen.Print("MulliganManager.HandleGameStart() - IsPastBeginPhase()={0}", (object) GameState.Get().IsPastBeginPhase());
    if (GameState.Get().IsPastBeginPhase())
    {
      this.StartCoroutine(this.SkipMulliganForResume());
    }
    else
    {
      this.StartCoroutine(this.DimLightsOnceBoardLoads());
      if (!GameState.Get().GetGameEntity().ShouldDoAlternateMulliganIntro())
      {
        this.m_xLabels = new GameObject[4];
        this.coinObject = UnityEngine.Object.Instantiate<GameObject>(this.coinPrefab);
        this.coinObject.SetActive(false);
        if (!Cheats.Get().ShouldSkipMulligan())
        {
          if (Cheats.Get().IsLaunchingQuickGame())
            Time.timeScale = SceneDebugger.GetDevTimescale();
          this.waitingForVersusVo = true;
          AssetLoader.Get().LoadSound("VO_ANNOUNCER_VERSUS_21", new AssetLoader.GameObjectCallback(this.OnVersusVoLoaded), (object) null, false, (GameObject) null);
        }
        this.waitingForVersusText = true;
        AssetLoader.Get().LoadGameObject("GameStart_VS_Letters", new AssetLoader.GameObjectCallback(this.OnVersusTextLoaded), (object) null, false);
        this.StartCoroutine("WaitForBoardThenLoadButton");
      }
      this.StartCoroutine("WaitForHeroesAndStartAnimations");
      Log.LoadingScreen.Print("MulliganManager.HandleGameStart() - IsMulliganPhase()={0}", (object) GameState.Get().IsMulliganPhase());
      if (!GameState.Get().IsMulliganPhase())
        return;
      this.StartCoroutine(this.ResumeMulligan());
    }
  }

  [DebuggerHidden]
  private IEnumerator DimLightsOnceBoardLoads()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MulliganManager.\u003CDimLightsOnceBoardLoads\u003Ec__IteratorC9 loadsCIteratorC9 = new MulliganManager.\u003CDimLightsOnceBoardLoads\u003Ec__IteratorC9();
    return (IEnumerator) loadsCIteratorC9;
  }

  [DebuggerHidden]
  private IEnumerator ResumeMulligan()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CResumeMulligan\u003Ec__IteratorCA() { \u003C\u003Ef__this = this };
  }

  private void OnMulliganTimerUpdate(TurnTimerUpdate update, object userData)
  {
    if ((double) update.GetSecondsRemaining() > (double) Mathf.Epsilon)
    {
      if (!update.ShouldShow())
        return;
      this.BeginMulliganCountdown(update.GetEndTimestamp());
    }
    else
      GameState.Get().UnregisterMulliganTimerUpdateListener(new GameState.TurnTimerUpdateCallback(this.OnMulliganTimerUpdate));
  }

  private bool OnEntitiesChosenReceived(Network.EntitiesChosen chosen, object userData)
  {
    if (!GameMgr.Get().IsSpectator() || chosen.PlayerId != GameState.Get().GetFriendlyPlayerId())
      return false;
    this.StartCoroutine(this.Spectator_WaitForFriendlyPlayerThenProcessEntitiesChosen(chosen));
    return true;
  }

  private void OnGameOver(TAG_PLAYSTATE playState, object userData)
  {
    this.HandleGameOverDuringMulligan();
  }

  [DebuggerHidden]
  private IEnumerator Spectator_WaitForFriendlyPlayerThenProcessEntitiesChosen(Network.EntitiesChosen chosen)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CSpectator_WaitForFriendlyPlayerThenProcessEntitiesChosen\u003Ec__IteratorCB() { chosen = chosen, \u003C\u0024\u003Echosen = chosen, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator ContinueMulliganWhenBoardLoads()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CContinueMulliganWhenBoardLoads\u003Ec__IteratorCC() { \u003C\u003Ef__this = this };
  }

  private void InitZones()
  {
    using (List<Zone>.Enumerator enumerator = ZoneMgr.Get().GetZones().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current is ZoneHand)
        {
          if (current.m_Side == Player.Side.FRIENDLY)
            this.friendlySideHandZone = (ZoneHand) current;
          else
            this.opposingSideHandZone = (ZoneHand) current;
        }
        if (current is ZoneDeck)
        {
          if (current.m_Side == Player.Side.FRIENDLY)
          {
            this.friendlySideDeck = (ZoneDeck) current;
            this.friendlySideDeck.SetSuppressEmotes(true);
            this.friendlySideDeck.UpdateLayout();
          }
          else
          {
            this.opposingSideDeck = (ZoneDeck) current;
            this.opposingSideDeck.SetSuppressEmotes(true);
            this.opposingSideDeck.UpdateLayout();
          }
        }
      }
    }
  }

  private bool ShouldWaitForMulliganCardsToBeProcessed()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MulliganManager.\u003CShouldWaitForMulliganCardsToBeProcessed\u003Ec__AnonStorey3BD processedCAnonStorey3Bd = new MulliganManager.\u003CShouldWaitForMulliganCardsToBeProcessed\u003Ec__AnonStorey3BD();
    // ISSUE: reference to a compiler-generated field
    processedCAnonStorey3Bd.\u003C\u003Ef__this = this;
    PowerProcessor powerProcessor = GameState.Get().GetPowerProcessor();
    // ISSUE: reference to a compiler-generated field
    processedCAnonStorey3Bd.receivedEndOfMulligan = false;
    // ISSUE: reference to a compiler-generated method
    powerProcessor.ForEachTaskList(new Action<int, PowerTaskList>(processedCAnonStorey3Bd.\u003C\u003Em__14C));
    // ISSUE: reference to a compiler-generated field
    if (processedCAnonStorey3Bd.receivedEndOfMulligan)
      return false;
    return powerProcessor.HasTaskLists();
  }

  private bool IsTaskListPuttingUsPastMulligan(PowerTaskList taskList)
  {
    using (List<PowerTask>.Enumerator enumerator = taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.PowerHistory power = enumerator.Current.GetPower();
        if (power.Type == Network.PowerType.TAG_CHANGE)
        {
          Network.HistTagChange histTagChange = power as Network.HistTagChange;
          if (histTagChange.Tag == 198 && GameUtils.IsPastBeginPhase((TAG_STEP) histTagChange.Value))
            return true;
        }
      }
    }
    return false;
  }

  private void GetStartingLists()
  {
    List<Card> cards1 = this.friendlySideHandZone.GetCards();
    List<Card> cards2 = this.opposingSideHandZone.GetCards();
    int num;
    if (this.ShouldHandleCoinCard())
    {
      if (this.friendlyPlayerGoesFirst)
      {
        num = cards1.Count;
        this.m_bonusCardIndex = cards2.Count - 2;
        this.m_coinCardIndex = cards2.Count - 1;
      }
      else
      {
        num = cards1.Count - 1;
        this.m_bonusCardIndex = cards1.Count - 2;
      }
    }
    else
    {
      num = cards1.Count;
      this.m_bonusCardIndex = !this.friendlyPlayerGoesFirst ? cards1.Count - 1 : cards2.Count - 1;
    }
    this.m_startingCards = new List<Card>();
    for (int index = 0; index < num; ++index)
      this.m_startingCards.Add(cards1[index]);
    this.m_startingOppCards = new List<Card>();
    for (int index = 0; index < cards2.Count; ++index)
      this.m_startingOppCards.Add(cards2[index]);
  }

  [DebuggerHidden]
  private IEnumerator PlayStartingTaunts()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CPlayStartingTaunts\u003Ec__IteratorCD() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator DealStartingCards()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CDealStartingCards\u003Ec__IteratorCE() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitAFrameBeforeSendingEventToMulliganButton()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CWaitAFrameBeforeSendingEventToMulliganButton\u003Ec__IteratorCF() { \u003C\u003Ef__this = this };
  }

  private void BeginMulliganCountdown(float endTimeStamp)
  {
    if (!this.m_waitingForUserInput)
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.mulliganTimerPrefab);
    this.m_mulliganTimer = gameObject.GetComponent<MulliganTimer>();
    if ((UnityEngine.Object) this.m_mulliganTimer == (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
    if (!this.m_waitingForUserInput)
      this.DestroyMulliganTimer();
    this.m_mulliganTimer.SetEndTime(endTimeStamp);
  }

  public NormalButton GetMulliganButton()
  {
    return this.mulliganButton;
  }

  private void CoinTossTextCallback(string actorName, GameObject actorObject, object callbackData)
  {
    this.coinTossText = actorObject;
    RenderUtils.SetAlpha(actorObject, 1f);
    actorObject.transform.position = this.coinLocation + new Vector3(0.0f, 0.0f, -1f);
    actorObject.transform.eulerAngles = new Vector3(90f, 0.0f, 0.0f);
    actorObject.transform.GetComponentInChildren<UberText>().Text = !this.friendlyPlayerGoesFirst ? GameStrings.Get("GAMEPLAY_COIN_TOSS_LOST") : GameStrings.Get("GAMEPLAY_COIN_TOSS_WON");
    GameState.Get().GetGameEntity().NotifyOfCoinFlipResult();
    this.StartCoroutine(this.AnimateCoinTossText());
  }

  [DebuggerHidden]
  private IEnumerator AnimateCoinTossText()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CAnimateCoinTossText\u003Ec__IteratorD0() { \u003C\u003Ef__this = this };
  }

  private MulliganReplaceLabel CreateNewUILabelAtCardPosition(MulliganReplaceLabel prefab, int cardPosition)
  {
    MulliganReplaceLabel mulliganReplaceLabel = UnityEngine.Object.Instantiate<MulliganReplaceLabel>(prefab);
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      mulliganReplaceLabel.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
      mulliganReplaceLabel.transform.position = new Vector3(this.m_startingCards[cardPosition].transform.position.x, this.m_startingCards[cardPosition].transform.position.y + 0.3f, this.m_startingCards[cardPosition].transform.position.z - 1.1f);
    }
    else
      mulliganReplaceLabel.transform.position = new Vector3(this.m_startingCards[cardPosition].transform.position.x, this.m_startingCards[cardPosition].transform.position.y + 0.3f, this.m_startingCards[cardPosition].transform.position.z - this.startingHandZone.GetComponent<Collider>().bounds.size.z / 2.6f);
    return mulliganReplaceLabel;
  }

  public void SetAllMulliganCardsToHold()
  {
    using (List<Card>.Enumerator enumerator = this.friendlySideHandZone.GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
        InputManager.Get().DoNetworkResponse(enumerator.Current.GetEntity(), true);
    }
  }

  private void ToggleHoldState(int startingCardsIndex)
  {
    if (startingCardsIndex >= this.m_startingCards.Count || !InputManager.Get().DoNetworkResponse(this.m_startingCards[startingCardsIndex].GetEntity(), true))
      return;
    this.m_handCardsMarkedForReplace[startingCardsIndex] = !this.m_handCardsMarkedForReplace[startingCardsIndex];
    if (!this.m_handCardsMarkedForReplace[startingCardsIndex])
    {
      SoundManager.Get().LoadAndPlay("GM_ChatWarning");
      if ((UnityEngine.Object) this.m_xLabels[startingCardsIndex] != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_xLabels[startingCardsIndex]);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_replaceLabels[startingCardsIndex].gameObject);
    }
    else
    {
      SoundManager.Get().LoadAndPlay("HeroDropItem1");
      if ((UnityEngine.Object) this.m_xLabels[startingCardsIndex] != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_xLabels[startingCardsIndex]);
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.mulliganXlabelPrefab);
      gameObject.transform.position = this.m_startingCards[startingCardsIndex].transform.position;
      gameObject.transform.rotation = this.m_startingCards[startingCardsIndex].transform.rotation;
      this.m_xLabels[startingCardsIndex] = gameObject;
      this.m_replaceLabels[startingCardsIndex] = this.CreateNewUILabelAtCardPosition(this.mulliganReplaceLabelPrefab, startingCardsIndex);
    }
  }

  private void DestroyXobjects()
  {
    if (this.m_xLabels == null)
      return;
    for (int index = 0; index < this.m_xLabels.Length; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_xLabels[index]);
    this.m_xLabels = (GameObject[]) null;
  }

  private void DestroyChooseBanner()
  {
    if ((UnityEngine.Object) this.mulliganChooseBanner == (UnityEngine.Object) null)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.mulliganChooseBanner);
  }

  private void DestroyMulliganTimer()
  {
    if ((UnityEngine.Object) this.m_mulliganTimer == (UnityEngine.Object) null)
      return;
    this.m_mulliganTimer.SelfDestruct();
    this.m_mulliganTimer = (MulliganTimer) null;
  }

  public void ToggleHoldState(Card toggleCard)
  {
    for (int startingCardsIndex = 0; startingCardsIndex < this.m_startingCards.Count; ++startingCardsIndex)
    {
      if ((UnityEngine.Object) this.m_startingCards[startingCardsIndex] == (UnityEngine.Object) toggleCard)
      {
        this.ToggleHoldState(startingCardsIndex);
        break;
      }
    }
  }

  public void ServerHasDealtReplacementCards(bool isFriendlySide)
  {
    if (isFriendlySide)
    {
      this.friendlyPlayerHasReplacementCards = true;
      if (!GameState.Get().IsFriendlySidePlayerTurn())
        return;
      TurnStartManager.Get().BeginListeningForTurnEvents();
    }
    else
      this.opponentPlayerHasReplacementCards = true;
  }

  public void AutomaticContinueMulligan()
  {
    if ((UnityEngine.Object) this.mulliganButton != (UnityEngine.Object) null)
      this.mulliganButton.SetEnabled(false);
    this.DestroyMulliganTimer();
    this.BeginDealNewCards();
  }

  private void OnMulliganButtonReleased(UIEvent e)
  {
    if (GameMgr.Get().IsSpectator())
      return;
    e.GetElement().SetEnabled(false);
    this.BeginDealNewCards();
  }

  private void BeginDealNewCards()
  {
    this.StartCoroutine("RemoveOldCardsAnimation");
  }

  [DebuggerHidden]
  private IEnumerator RemoveOldCardsAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CRemoveOldCardsAnimation\u003Ec__IteratorD1() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitForOpponentToFinishMulligan()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CWaitForOpponentToFinishMulligan\u003Ec__IteratorD2() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator RemoveUIButtons()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CRemoveUIButtons\u003Ec__IteratorD3() { \u003C\u003Ef__this = this };
  }

  private void DestroyButton(UnityEngine.Object buttonToDestroy)
  {
    UnityEngine.Object.Destroy(buttonToDestroy);
  }

  private void HandleGameOverDuringMulligan()
  {
    this.StopCoroutine("WaitForBoardThenLoadButton");
    this.StopCoroutine("WaitForHeroesAndStartAnimations");
    this.StopCoroutine("DealStartingCards");
    this.StopCoroutine("RemoveOldCardsAnimation");
    this.StopCoroutine("PlayStartingTaunts");
    if (this.m_customIntro != null)
    {
      this.StopCoroutine(this.m_customIntro);
      GameState.Get().GetGameEntity().OnCustomIntroCancelled(this.myHeroCardActor.GetCard(), this.hisHeroCardActor.GetCard(), this.myheroLabel, this.hisheroLabel, this.versusText);
      this.m_customIntro = (Coroutine) null;
    }
    this.m_waitingForUserInput = false;
    this.DestroyXobjects();
    this.DestroyChooseBanner();
    this.DestroyMulliganTimer();
    if ((UnityEngine.Object) this.coinObject != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.coinObject);
    if ((UnityEngine.Object) this.versusText != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.versusText.gameObject);
    if ((UnityEngine.Object) this.versusVo != (UnityEngine.Object) null)
      SoundManager.Get().Destroy(this.versusVo);
    if ((UnityEngine.Object) this.coinTossText != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.coinTossText);
    if ((bool) UniversalInputManager.UsePhoneUI)
      Gameplay.Get().RemoveNameBanners();
    else
      Gameplay.Get().RemoveClassNames();
    this.StartCoroutine(this.RemoveUIButtons());
    if ((UnityEngine.Object) this.mulliganButton != (UnityEngine.Object) null)
      this.mulliganButton.SetEnabled(false);
    this.DestoryHeroSkinSocketInEffects();
    if ((UnityEngine.Object) this.myheroLabel != (UnityEngine.Object) null && this.myheroLabel.isActiveAndEnabled)
      this.myheroLabel.FadeOut();
    if ((UnityEngine.Object) this.hisheroLabel != (UnityEngine.Object) null && this.hisheroLabel.isActiveAndEnabled)
      this.hisheroLabel.FadeOut();
    if ((UnityEngine.Object) this.friendlySideHandZone != (UnityEngine.Object) null)
    {
      using (List<Card>.Enumerator enumerator = this.friendlySideHandZone.GetCards().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Actor actor = enumerator.Current.GetActor();
          actor.SetActorState(ActorStateType.CARD_IDLE);
          actor.ToggleForceIdle(true);
        }
      }
      this.hisHeroCardActor.SetActorState(ActorStateType.CARD_IDLE);
      this.hisHeroCardActor.ToggleForceIdle(true);
      Card heroPowerCard = GameState.Get().GetFriendlySidePlayer().GetHeroPowerCard();
      if ((UnityEngine.Object) heroPowerCard != (UnityEngine.Object) null && (UnityEngine.Object) heroPowerCard.GetActor() != (UnityEngine.Object) null)
      {
        heroPowerCard.GetActor().SetActorState(ActorStateType.CARD_IDLE);
        heroPowerCard.GetActor().ToggleForceIdle(true);
      }
      if (!this.friendlyPlayerGoesFirst)
      {
        Card fromFriendlyHand = this.GetCoinCardFromFriendlyHand();
        fromFriendlyHand.SetDoNotSort(false);
        fromFriendlyHand.SetTransitionStyle(ZoneTransitionStyle.NORMAL);
        this.PutCoinCardInSpawnPosition(fromFriendlyHand);
        fromFriendlyHand.GetActor().Show();
      }
      this.friendlySideHandZone.ForceStandInUpdate();
      this.friendlySideHandZone.SetDoNotUpdateLayout(false);
      this.friendlySideHandZone.UpdateLayout();
    }
    Board board = Board.Get();
    if ((UnityEngine.Object) board != (UnityEngine.Object) null)
      board.RaiseTheLightsQuickly();
    Animation component1 = this.myHeroCardActor.gameObject.GetComponent<Animation>();
    if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
      component1.Stop();
    Animation component2 = this.hisHeroCardActor.gameObject.GetComponent<Animation>();
    if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
      component2.Stop();
    this.myHeroCardActor.transform.localScale = Vector3.one;
    this.myHeroCardActor.transform.rotation = Quaternion.identity;
    this.myHeroCardActor.transform.position = ZoneMgr.Get().FindZoneOfType<ZoneHero>(Player.Side.FRIENDLY).transform.position;
    this.hisHeroCardActor.transform.localScale = Vector3.one;
    this.hisHeroCardActor.transform.rotation = Quaternion.identity;
    this.hisHeroCardActor.transform.position = ZoneMgr.Get().FindZoneOfType<ZoneHero>(Player.Side.OPPOSING).transform.position;
  }

  public void EndMulligan()
  {
    this.m_waitingForUserInput = false;
    if (this.m_replaceLabels != null)
    {
      for (int index = 0; index < this.m_replaceLabels.Count; ++index)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_replaceLabels[index]);
    }
    if ((UnityEngine.Object) this.mulliganButton != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.mulliganButton.gameObject);
    this.DestroyXobjects();
    this.DestroyChooseBanner();
    if ((UnityEngine.Object) this.versusText != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.versusText.gameObject);
    if ((UnityEngine.Object) this.versusVo != (UnityEngine.Object) null)
      SoundManager.Get().Destroy(this.versusVo);
    if ((UnityEngine.Object) this.coinTossText != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.coinTossText);
    if ((UnityEngine.Object) this.hisheroLabel != (UnityEngine.Object) null)
      this.hisheroLabel.FadeOut();
    if ((UnityEngine.Object) this.myheroLabel != (UnityEngine.Object) null)
      this.myheroLabel.FadeOut();
    this.DestoryHeroSkinSocketInEffects();
    this.myHeroCardActor.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    this.hisHeroCardActor.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    if (GameState.Get().IsGameOver())
      return;
    this.myHeroCardActor.GetHealthObject().Show();
    this.hisHeroCardActor.GetHealthObject().Show();
    this.friendlySideHandZone.ForceStandInUpdate();
    this.friendlySideHandZone.SetDoNotUpdateLayout(false);
    this.friendlySideHandZone.UpdateLayout();
    if (this.m_startingOppCards != null && this.m_startingOppCards.Count > 0)
      this.m_startingOppCards[this.m_startingOppCards.Count - 1].SetDoNotSort(false);
    this.opposingSideHandZone.SetDoNotUpdateLayout(false);
    this.opposingSideHandZone.UpdateLayout();
    this.friendlySideDeck.SetSuppressEmotes(false);
    this.opposingSideDeck.SetSuppressEmotes(false);
    Board.Get().SplitSurface();
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      Gameplay.Get().RemoveNameBanners();
      Gameplay.Get().AddGamePlayNameBannerPhone();
    }
    if ((UnityEngine.Object) this.m_MyCustomSocketInSpell != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_MyCustomSocketInSpell);
    if ((UnityEngine.Object) this.m_HisCustomSocketInSpell != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_HisCustomSocketInSpell);
    this.StartCoroutine(this.EndMulliganWithTiming());
  }

  [DebuggerHidden]
  private IEnumerator EndMulliganWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CEndMulliganWithTiming\u003Ec__IteratorD4() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator HandleCoinCard()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CHandleCoinCard\u003Ec__IteratorD5() { \u003C\u003Ef__this = this };
  }

  private bool IsCoinCard(Card card)
  {
    return card.GetEntity().GetCardId() == "GAME_005";
  }

  private Card GetCoinCardFromFriendlyHand()
  {
    List<Card> cards = this.friendlySideHandZone.GetCards();
    return cards[cards.Count - 1];
  }

  private void PutCoinCardInSpawnPosition(Card coinCard)
  {
    coinCard.transform.position = Board.Get().FindBone("MulliganCoinCardSpawnPosition").position;
    coinCard.transform.localScale = Board.Get().FindBone("MulliganCoinCardSpawnPosition").localScale;
  }

  private bool ShouldHandleCoinCard()
  {
    return GameState.Get().IsMulliganPhase() && GameState.Get().GetGameEntity().ShouldHandleCoin();
  }

  private void CoinCardSummonFinishedCallback(Spell spell, object userData)
  {
    Card componentInParents = SceneUtils.FindComponentInParents<Card>((Component) spell);
    componentInParents.RefreshActor();
    componentInParents.UpdateActorComponents();
    componentInParents.SetDoNotSort(false);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.coinObject);
    componentInParents.SetTransitionStyle(ZoneTransitionStyle.VERY_SLOW);
    this.friendlySideHandZone.UpdateLayout(-1, true);
  }

  [DebuggerHidden]
  private IEnumerator EnableHandCollidersAfterCardsAreDealt()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CEnableHandCollidersAfterCardsAreDealt\u003Ec__IteratorD6() { \u003C\u003Ef__this = this };
  }

  public void SkipCardChoosing()
  {
    this.skipCardChoosing = true;
  }

  public void SkipMulliganForDev()
  {
    this.StopCoroutine("WaitForBoardThenLoadButton");
    this.StopCoroutine("WaitForHeroesAndStartAnimations");
    this.StopCoroutine("DealStartingCards");
    this.EndMulligan();
  }

  [DebuggerHidden]
  private IEnumerator SkipMulliganForResume()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CSkipMulliganForResume\u003Ec__IteratorD7() { \u003C\u003Ef__this = this };
  }

  public void SkipMulligan()
  {
    Gameplay.Get().RemoveClassNames();
    this.StartCoroutine(this.SkipMulliganWhenIntroComplete());
  }

  [DebuggerHidden]
  private IEnumerator SkipMulliganWhenIntroComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CSkipMulliganWhenIntroComplete\u003Ec__IteratorD8() { \u003C\u003Ef__this = this };
  }

  private void FadeOutMulliganMusicAndStartGameplayMusic()
  {
    GameState.Get().GetGameEntity().StartGameplaySoundtracks();
  }

  [DebuggerHidden]
  private IEnumerator WaitForBoardAnimToCompleteThenStartTurn()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CWaitForBoardAnimToCompleteThenStartTurn\u003Ec__IteratorD9() { \u003C\u003Ef__this = this };
  }

  private void ShuffleDeck()
  {
    SoundManager.Get().LoadAndPlay("FX_MulliganCoin09_DeckShuffle", this.friendlySideDeck.gameObject);
    Animation animation1 = this.friendlySideDeck.gameObject.GetComponent<Animation>();
    if ((UnityEngine.Object) animation1 == (UnityEngine.Object) null)
      animation1 = this.friendlySideDeck.gameObject.AddComponent<Animation>();
    animation1.AddClip(this.shuffleDeck, "shuffleDeckAnim");
    animation1.Play("shuffleDeckAnim");
    Animation animation2 = this.opposingSideDeck.gameObject.GetComponent<Animation>();
    if ((UnityEngine.Object) animation2 == (UnityEngine.Object) null)
      animation2 = this.opposingSideDeck.gameObject.AddComponent<Animation>();
    animation2.AddClip(this.shuffleDeck, "shuffleDeckAnim");
    animation2.Play("shuffleDeckAnim");
  }

  private void SlideCard(GameObject topCard)
  {
    iTween.MoveTo(topCard, iTween.Hash((object) "position", (object) new Vector3(topCard.transform.position.x - 0.5f, topCard.transform.position.y, topCard.transform.position.z), (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.linear));
  }

  [DebuggerHidden]
  private IEnumerator SampleAnimFrame(Animation animToUse, string animName, float startSec)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CSampleAnimFrame\u003Ec__IteratorDA() { animToUse = animToUse, animName = animName, startSec = startSec, \u003C\u0024\u003EanimToUse = animToUse, \u003C\u0024\u003EanimName = animName, \u003C\u0024\u003EstartSec = startSec };
  }

  private void SortHand(Zone zone)
  {
    zone.GetCards().Sort(new Comparison<Card>(Zone.CardSortComparison));
  }

  [DebuggerHidden]
  private IEnumerator ShrinkStartingHandBanner(GameObject banner)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MulliganManager.\u003CShrinkStartingHandBanner\u003Ec__IteratorDB() { banner = banner, \u003C\u0024\u003Ebanner = banner };
  }

  private void FadeHeroPowerIn(Card heroPowerCard)
  {
    if ((UnityEngine.Object) heroPowerCard == (UnityEngine.Object) null)
      return;
    Actor actor = heroPowerCard.GetActor();
    if ((UnityEngine.Object) actor == (UnityEngine.Object) null)
      return;
    actor.TurnOnCollider();
  }

  private void LoadMyHeroSkinSocketInEffect(CardDef cardDef)
  {
    if (string.IsNullOrEmpty(cardDef.m_SocketInEffectFriendly) && !(bool) UniversalInputManager.UsePhoneUI || string.IsNullOrEmpty(cardDef.m_SocketInEffectFriendlyPhone) && (bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_isLoadingMyCustomSocketIn = true;
    string name = cardDef.m_SocketInEffectFriendly;
    if ((bool) UniversalInputManager.UsePhoneUI)
      name = cardDef.m_SocketInEffectFriendlyPhone;
    AssetLoader.Get().LoadSpell(name, new AssetLoader.GameObjectCallback(this.OnMyHeroSkinSocketInEffectLoaded), (object) null, false);
  }

  private void OnMyHeroSkinSocketInEffectLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Failed to load My custom hero socket in effect!");
      this.m_isLoadingMyCustomSocketIn = false;
    }
    else
    {
      go.transform.position = Board.Get().FindBone("CustomSocketIn_Friendly").position;
      Spell component = go.GetComponent<Spell>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "Faild to locate Spell on custom socket in effect!");
        this.m_isLoadingMyCustomSocketIn = false;
      }
      else
      {
        this.m_MyCustomSocketInSpell = component;
        if (this.m_MyCustomSocketInSpell.HasUsableState(SpellStateType.IDLE))
          this.m_MyCustomSocketInSpell.ActivateState(SpellStateType.IDLE);
        else
          this.m_MyCustomSocketInSpell.gameObject.SetActive(false);
        this.m_isLoadingMyCustomSocketIn = false;
      }
    }
  }

  private void LoadHisHeroSkinSocketInEffect(CardDef cardDef)
  {
    if (string.IsNullOrEmpty(cardDef.m_SocketInEffectOpponent) && !(bool) UniversalInputManager.UsePhoneUI || string.IsNullOrEmpty(cardDef.m_SocketInEffectOpponentPhone) && (bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_isLoadingHisCustomSocketIn = true;
    string name = cardDef.m_SocketInEffectOpponent;
    if ((bool) UniversalInputManager.UsePhoneUI)
      name = cardDef.m_SocketInEffectOpponentPhone;
    AssetLoader.Get().LoadSpell(name, new AssetLoader.GameObjectCallback(this.OnHisHeroSkinSocketInEffectLoaded), (object) null, false);
  }

  private void OnHisHeroSkinSocketInEffectLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Failed to load His custom hero socket in effect!");
      this.m_isLoadingHisCustomSocketIn = false;
    }
    else
    {
      go.transform.position = Board.Get().FindBone("CustomSocketIn_Opposing").position;
      Spell component = go.GetComponent<Spell>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "Faild to locate Spell on custom socket in effect!");
        this.m_isLoadingHisCustomSocketIn = false;
      }
      else
      {
        this.m_HisCustomSocketInSpell = component;
        if (this.m_HisCustomSocketInSpell.HasUsableState(SpellStateType.IDLE))
          this.m_HisCustomSocketInSpell.ActivateState(SpellStateType.IDLE);
        else
          this.m_HisCustomSocketInSpell.gameObject.SetActive(false);
        this.m_isLoadingHisCustomSocketIn = false;
      }
    }
  }

  private void DestoryHeroSkinSocketInEffects()
  {
    if ((UnityEngine.Object) this.m_MyCustomSocketInSpell != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_MyCustomSocketInSpell.gameObject);
    if (!((UnityEngine.Object) this.m_HisCustomSocketInSpell != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_HisCustomSocketInSpell.gameObject);
  }
}
