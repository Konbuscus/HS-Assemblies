﻿// Decompiled with JetBrains decompiler
// Type: AssetFamilyBundleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class AssetFamilyBundleInfo
{
  public int NumberOfBundles = 1;
  public int NumberOfLocaleBundles = 1;
  public System.Type TypeOf;
  public string BundleName;
  public int NumberOfDownloadableLocaleBundles;
  public bool Updatable;
}
