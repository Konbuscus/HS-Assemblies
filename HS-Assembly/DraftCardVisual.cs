﻿// Decompiled with JetBrains decompiler
// Type: DraftCardVisual
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DraftCardVisual : PegUIElement
{
  private int m_cardChoice = -1;
  private const float MOUSE_OVER_DELAY = 0.4f;
  private Actor m_actor;
  private bool m_chosen;
  private float m_mouseOverTimer;

  public void SetActor(Actor actor)
  {
    this.m_actor = actor;
  }

  public Actor GetActor()
  {
    return this.m_actor;
  }

  public void SetChoiceNum(int num)
  {
    this.m_cardChoice = num;
  }

  public int GetChoiceNum()
  {
    return this.m_cardChoice;
  }

  public void ChooseThisCard()
  {
    if (GameUtils.IsAnyTransitionActive())
      return;
    Log.Arena.Print(string.Format("Client chooses: {0} ({1})", (object) this.m_actor.GetEntityDef().GetName(), (object) this.m_actor.GetEntityDef().GetCardId()));
    if (this.m_actor.GetEntityDef().IsHero())
    {
      DraftDisplay.Get().OnHeroClicked(this.m_cardChoice);
    }
    else
    {
      this.m_chosen = true;
      DraftManager.Get().MakeChoice(this.m_cardChoice);
    }
  }

  public bool IsChosen()
  {
    return this.m_chosen;
  }

  public void SetChosenFlag(bool bOn)
  {
    this.m_chosen = bOn;
  }

  protected override void OnPress()
  {
    this.m_mouseOverTimer = Time.realtimeSinceStartup;
  }

  protected override void OnRelease()
  {
    if (UniversalInputManager.Get().IsTouchMode() && (double) Time.realtimeSinceStartup - (double) this.m_mouseOverTimer >= 0.400000005960464)
      return;
    this.ChooseThisCard();
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    if (this.m_actor.GetEntityDef().IsHero())
      SoundManager.Get().LoadAndPlay("collection_manager_hero_mouse_over");
    else
      SoundManager.Get().LoadAndPlay("collection_manager_card_mouse_over");
    this.m_actor.SetActorState(ActorStateType.CARD_MOUSE_OVER);
    TooltipPanelManager.Get().UpdateKeywordHelpForForge(this.m_actor.GetEntityDef(), this.m_actor, this.m_cardChoice);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_actor.SetActorState(ActorStateType.CARD_IDLE);
    TooltipPanelManager.Get().HideKeywordHelp();
  }
}
