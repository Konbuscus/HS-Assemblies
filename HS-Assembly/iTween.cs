﻿// Decompiled with JetBrains decompiler
// Type: iTween
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iTween
{
  private static int nextId = 1;
  private static string[] CALLBACK_NAMES = new string[4]{ "onstart", "onupdate", "oncomplete", "onconflict" };
  private static string[] CALLBACK_TARGET_NAMES = new string[4]{ "onstarttarget", "onupdatetarget", "oncompletetarget", "onconflicttarget" };
  private static string[] CALLBACK_PARAMS_NAMES = new string[4]{ "onstartparams", "onupdateparams", "oncompleteparams", "onconflictparams" };
  public bool enabled = true;
  private static GameObject cameraFade;
  public int id;
  public string type;
  public string method;
  public iTween.EaseType easeType;
  public float time;
  public float delay;
  public iTween.LoopType loopType;
  public bool isRunning;
  public bool isPaused;
  public string _name;
  private bool waitForDelay;
  private float runningTime;
  private float percentage;
  private float delayStarted;
  private bool kinematic;
  private bool isLocal;
  private bool loop;
  private bool reverse;
  private bool physics;
  private Hashtable tweenArguments;
  private Space space;
  private iTween.EasingFunction ease;
  private iTween.ApplyTween apply;
  private AudioSource audioSource;
  private Vector3[] vector3s;
  private Vector2[] vector2s;
  private Color[,] colors;
  private float[] floats;
  private Rect[] rects;
  private iTween.CRSpline path;
  private Vector3 preUpdate;
  private Vector3 postUpdate;
  private iTween.NamedValueColor namedcolorvalue;
  private string namedColorValueString;
  private float lastRealTime;
  private bool useRealTime;
  public GameObject gameObject;
  public bool activeLastTick;
  public bool destroyed;

  private Transform transform
  {
    get
    {
      return this.gameObject.transform;
    }
  }

  private Renderer renderer
  {
    get
    {
      return this.gameObject.GetComponent<Renderer>();
    }
  }

  private Light light
  {
    get
    {
      return this.gameObject.GetComponent<Light>();
    }
  }

  private AudioSource audio
  {
    get
    {
      return this.gameObject.GetComponent<AudioSource>();
    }
  }

  private Rigidbody rigidbody
  {
    get
    {
      return this.gameObject.GetComponent<Rigidbody>();
    }
  }

  private GUITexture guiTexture
  {
    get
    {
      return this.gameObject.GetComponent<GUITexture>();
    }
  }

  private GUIText guiText
  {
    get
    {
      return this.gameObject.GetComponent<GUIText>();
    }
  }

  private bool activeInHierarchy
  {
    get
    {
      if (this.enabled && !this.destroyed && (UnityEngine.Object) this.gameObject != (UnityEngine.Object) null)
        return this.gameObject.activeInHierarchy;
      return false;
    }
  }

  public iTween(GameObject obj, Hashtable args)
  {
    this.gameObject = obj;
    this.tweenArguments = args;
  }

  private Component GetComponent(System.Type t)
  {
    return this.gameObject.GetComponent(t);
  }

  public static void Init(GameObject target)
  {
    iTween.MoveBy(target, Vector3.zero, 0.0f);
  }

  public static void CameraFadeFrom(float amount, float time)
  {
    if ((bool) ((UnityEngine.Object) iTween.cameraFade))
      iTween.CameraFadeFrom(iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
    else
      Debug.LogError((object) "iTween Error: You must first add a camera fade object with CameraFadeAdd() before atttempting to use camera fading.");
  }

  public static void CameraFadeFrom(Hashtable args)
  {
    if ((bool) ((UnityEngine.Object) iTween.cameraFade))
      iTween.ColorFrom(iTween.cameraFade, args);
    else
      Debug.LogError((object) "iTween Error: You must first add a camera fade object with CameraFadeAdd() before atttempting to use camera fading.");
  }

  public static void CameraFadeTo(float amount, float time)
  {
    if ((bool) ((UnityEngine.Object) iTween.cameraFade))
      iTween.CameraFadeTo(iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
    else
      Debug.LogError((object) "iTween Error: You must first add a camera fade object with CameraFadeAdd() before atttempting to use camera fading.");
  }

  public static void CameraFadeTo(Hashtable args)
  {
    if ((bool) ((UnityEngine.Object) iTween.cameraFade))
      iTween.ColorTo(iTween.cameraFade, args);
    else
      Debug.LogError((object) "iTween Error: You must first add a camera fade object with CameraFadeAdd() before atttempting to use camera fading.");
  }

  public static void ValueTo(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    if (!args.Contains((object) "onupdate") || !args.Contains((object) "from") || !args.Contains((object) "to"))
    {
      Debug.LogError((object) "iTween Error: ValueTo() requires an 'onupdate' callback function and a 'from' and 'to' property.  The supplied 'onupdate' callback must accept a single argument that is the same type as the supplied 'from' and 'to' properties!");
    }
    else
    {
      args[(object) "type"] = (object) "value";
      if (args[(object) "from"].GetType() == typeof (Vector2))
        args[(object) "method"] = (object) "vector2";
      else if (args[(object) "from"].GetType() == typeof (Vector3))
        args[(object) "method"] = (object) "vector3";
      else if (args[(object) "from"].GetType() == typeof (Rect))
        args[(object) "method"] = (object) "rect";
      else if (args[(object) "from"].GetType() == typeof (float))
        args[(object) "method"] = (object) "float";
      else if (args[(object) "from"].GetType() == typeof (Color))
      {
        args[(object) "method"] = (object) "color";
      }
      else
      {
        Debug.LogError((object) "iTween Error: ValueTo() only works with interpolating Vector3s, Vector2s, floats, ints, Rects and Colors!");
        return;
      }
      if (!args.Contains((object) "easetype"))
        args.Add((object) "easetype", (object) iTween.EaseType.linear);
      iTween.Launch(target, args);
    }
  }

  public static void FadeFrom(GameObject target, float alpha, float time)
  {
    iTween.FadeFrom(target, iTween.Hash((object) "alpha", (object) alpha, (object) "time", (object) time));
  }

  public static void FadeFrom(GameObject target, Hashtable args)
  {
    iTween.ColorFrom(target, args);
  }

  public static void FadeTo(GameObject target, float alpha, float time)
  {
    iTween.FadeTo(target, iTween.Hash((object) "alpha", (object) alpha, (object) "time", (object) time));
  }

  public static void FadeTo(GameObject target, Hashtable args)
  {
    iTween.ColorTo(target, args);
  }

  public static void ColorFrom(GameObject target, Color color, float time)
  {
    iTween.ColorFrom(target, iTween.Hash((object) "color", (object) color, (object) "time", (object) time));
  }

  public static void ColorFrom(GameObject target, Hashtable args)
  {
    Color color1 = new Color();
    Color color2 = new Color();
    args = iTween.CleanArgs(args);
    if (!args.Contains((object) "includechildren") || (bool) args[(object) "includechildren"])
    {
      IEnumerator enumerator = target.transform.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          Transform current = (Transform) enumerator.Current;
          Hashtable args1 = (Hashtable) args.Clone();
          args1[(object) "ischild"] = (object) true;
          iTween.ColorFrom(current.gameObject, args1);
        }
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
    }
    if (!args.Contains((object) "easetype"))
      args.Add((object) "easetype", (object) iTween.EaseType.linear);
    if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (GUITexture))))
      color2 = color1 = target.GetComponent<GUITexture>().color;
    else if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (GUIText))))
      color2 = color1 = target.GetComponent<GUIText>().material.color;
    else if ((bool) ((UnityEngine.Object) target.GetComponent<Renderer>()))
      color2 = color1 = target.GetComponent<Renderer>().material.color;
    else if ((bool) ((UnityEngine.Object) target.GetComponent<Light>()))
      color2 = color1 = target.GetComponent<Light>().color;
    if (args.Contains((object) "color"))
    {
      color1 = (Color) args[(object) "color"];
    }
    else
    {
      if (args.Contains((object) "r"))
        color1.r = (float) args[(object) "r"];
      if (args.Contains((object) "g"))
        color1.g = (float) args[(object) "g"];
      if (args.Contains((object) "b"))
        color1.b = (float) args[(object) "b"];
      if (args.Contains((object) "a"))
        color1.a = (float) args[(object) "a"];
    }
    if (args.Contains((object) "amount"))
    {
      color1.a = (float) args[(object) "amount"];
      args.Remove((object) "amount");
    }
    else if (args.Contains((object) "alpha"))
    {
      color1.a = (float) args[(object) "alpha"];
      args.Remove((object) "alpha");
    }
    if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (GUITexture))))
      target.GetComponent<GUITexture>().color = color1;
    else if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (GUIText))))
      target.GetComponent<GUIText>().material.color = color1;
    else if ((bool) ((UnityEngine.Object) target.GetComponent<Renderer>()))
      target.GetComponent<Renderer>().material.color = color1;
    else if ((bool) ((UnityEngine.Object) target.GetComponent<Light>()))
      target.GetComponent<Light>().color = color1;
    args[(object) "color"] = (object) color2;
    args[(object) "type"] = (object) "color";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void ColorTo(GameObject target, Color color, float time)
  {
    iTween.ColorTo(target, iTween.Hash((object) "color", (object) color, (object) "time", (object) time));
  }

  public static void ColorTo(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    if (!args.Contains((object) "includechildren") || (bool) args[(object) "includechildren"])
    {
      IEnumerator enumerator = target.transform.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          Transform current = (Transform) enumerator.Current;
          Hashtable args1 = (Hashtable) args.Clone();
          args1[(object) "ischild"] = (object) true;
          iTween.ColorTo(current.gameObject, args1);
        }
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
    }
    if (!args.Contains((object) "easetype"))
      args.Add((object) "easetype", (object) iTween.EaseType.linear);
    args[(object) "type"] = (object) "color";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void AudioFrom(GameObject target, float volume, float pitch, float time)
  {
    iTween.AudioFrom(target, iTween.Hash((object) "volume", (object) volume, (object) "pitch", (object) pitch, (object) "time", (object) time));
  }

  public static void AudioFrom(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    AudioSource component;
    if (args.Contains((object) "audiosource"))
      component = (AudioSource) args[(object) "audiosource"];
    else if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (AudioSource))))
    {
      component = target.GetComponent<AudioSource>();
    }
    else
    {
      Debug.LogError((object) "iTween Error: AudioFrom requires an AudioSource.");
      return;
    }
    Vector2 vector2_1;
    Vector2 vector2_2;
    vector2_1.x = vector2_2.x = component.volume;
    vector2_1.y = vector2_2.y = component.pitch;
    if (args.Contains((object) "volume"))
      vector2_2.x = (float) args[(object) "volume"];
    if (args.Contains((object) "pitch"))
      vector2_2.y = (float) args[(object) "pitch"];
    component.volume = vector2_2.x;
    component.pitch = vector2_2.y;
    args[(object) "volume"] = (object) vector2_1.x;
    args[(object) "pitch"] = (object) vector2_1.y;
    if (!args.Contains((object) "easetype"))
      args.Add((object) "easetype", (object) iTween.EaseType.linear);
    args[(object) "type"] = (object) "audio";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void AudioTo(GameObject target, float volume, float pitch, float time)
  {
    iTween.AudioTo(target, iTween.Hash((object) "volume", (object) volume, (object) "pitch", (object) pitch, (object) "time", (object) time));
  }

  public static void AudioTo(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    if (!args.Contains((object) "easetype"))
      args.Add((object) "easetype", (object) iTween.EaseType.linear);
    args[(object) "type"] = (object) "audio";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void Stab(GameObject target, AudioClip audioclip, float delay)
  {
    iTween.Stab(target, iTween.Hash((object) "audioclip", (object) audioclip, (object) "delay", (object) delay));
  }

  public static void Stab(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "stab";
    iTween.Launch(target, args);
  }

  public static void LookFrom(GameObject target, Vector3 looktarget, float time)
  {
    iTween.LookFrom(target, iTween.Hash((object) "looktarget", (object) looktarget, (object) "time", (object) time));
  }

  public static void LookFrom(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    Vector3 eulerAngles1 = target.transform.eulerAngles;
    if (args[(object) "looktarget"].GetType() == typeof (Transform))
    {
      Transform transform = target.transform;
      Transform target1 = (Transform) args[(object) "looktarget"];
      Vector3? nullable = (Vector3?) args[(object) "up"];
      Vector3 worldUp = !nullable.HasValue ? iTween.Defaults.up : nullable.Value;
      transform.LookAt(target1, worldUp);
    }
    else if (args[(object) "looktarget"].GetType() == typeof (Vector3))
    {
      Transform transform = target.transform;
      Vector3 worldPosition = (Vector3) args[(object) "looktarget"];
      Vector3? nullable = (Vector3?) args[(object) "up"];
      Vector3 worldUp = !nullable.HasValue ? iTween.Defaults.up : nullable.Value;
      transform.LookAt(worldPosition, worldUp);
    }
    if (args.Contains((object) "axis"))
    {
      Vector3 eulerAngles2 = target.transform.eulerAngles;
      string key = (string) args[(object) "axis"];
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD0 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD0 = new Dictionary<string, int>(3)
          {
            {
              "x",
              0
            },
            {
              "y",
              1
            },
            {
              "z",
              2
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD0.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              eulerAngles2.y = eulerAngles1.y;
              eulerAngles2.z = eulerAngles1.z;
              break;
            case 1:
              eulerAngles2.x = eulerAngles1.x;
              eulerAngles2.z = eulerAngles1.z;
              break;
            case 2:
              eulerAngles2.x = eulerAngles1.x;
              eulerAngles2.y = eulerAngles1.y;
              break;
          }
        }
      }
      target.transform.eulerAngles = eulerAngles2;
    }
    args[(object) "rotation"] = (object) eulerAngles1;
    args[(object) "type"] = (object) "rotate";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void LookTo(GameObject target, Vector3 looktarget, float time)
  {
    iTween.LookTo(target, iTween.Hash((object) "looktarget", (object) looktarget, (object) "time", (object) time));
  }

  public static void LookTo(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    if (args.Contains((object) "looktarget") && args[(object) "looktarget"].GetType() == typeof (Transform))
    {
      Transform transform = (Transform) args[(object) "looktarget"];
      args[(object) "position"] = (object) new Vector3(transform.position.x, transform.position.y, transform.position.z);
      args[(object) "rotation"] = (object) new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
    }
    args[(object) "type"] = (object) "look";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void MoveTo(GameObject target, Vector3 position, float time)
  {
    iTween.MoveTo(target, iTween.Hash((object) "position", (object) position, (object) "time", (object) time));
  }

  public static void MoveTo(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    if (args.Contains((object) "position") && args[(object) "position"].GetType() == typeof (Transform))
    {
      Transform transform = (Transform) args[(object) "position"];
      args[(object) "position"] = (object) new Vector3(transform.position.x, transform.position.y, transform.position.z);
      args[(object) "rotation"] = (object) new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
      args[(object) "scale"] = (object) new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
    args[(object) "type"] = (object) "move";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void MoveFrom(GameObject target, Vector3 position, float time)
  {
    iTween.MoveFrom(target, iTween.Hash((object) "position", (object) position, (object) "time", (object) time));
  }

  public static void MoveFrom(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    bool flag = !args.Contains((object) "islocal") ? iTween.Defaults.isLocal : (bool) args[(object) "islocal"];
    if (args.Contains((object) "path"))
    {
      Vector3[] vector3Array1;
      if (args[(object) "path"].GetType() == typeof (Vector3[]))
      {
        Vector3[] vector3Array2 = (Vector3[]) args[(object) "path"];
        vector3Array1 = new Vector3[vector3Array2.Length];
        Array.Copy((Array) vector3Array2, (Array) vector3Array1, vector3Array2.Length);
      }
      else
      {
        Transform[] transformArray = (Transform[]) args[(object) "path"];
        vector3Array1 = new Vector3[transformArray.Length];
        for (int index = 0; index < transformArray.Length; ++index)
          vector3Array1[index] = transformArray[index].position;
      }
      if (vector3Array1[vector3Array1.Length - 1] != target.transform.position)
      {
        Vector3[] vector3Array2 = new Vector3[vector3Array1.Length + 1];
        Array.Copy((Array) vector3Array1, (Array) vector3Array2, vector3Array1.Length);
        if (flag)
        {
          vector3Array2[vector3Array2.Length - 1] = target.transform.localPosition;
          target.transform.localPosition = vector3Array2[0];
        }
        else
        {
          vector3Array2[vector3Array2.Length - 1] = target.transform.position;
          target.transform.position = vector3Array2[0];
        }
        args[(object) "path"] = (object) vector3Array2;
      }
      else
      {
        if (flag)
          target.transform.localPosition = vector3Array1[0];
        else
          target.transform.position = vector3Array1[0];
        args[(object) "path"] = (object) vector3Array1;
      }
    }
    else
    {
      Vector3 vector3_1;
      Vector3 vector3_2 = !flag ? (vector3_1 = target.transform.position) : (vector3_1 = target.transform.localPosition);
      if (args.Contains((object) "position"))
      {
        if (args[(object) "position"].GetType() == typeof (Transform))
          vector3_1 = ((Transform) args[(object) "position"]).position;
        else if (args[(object) "position"].GetType() == typeof (Vector3))
          vector3_1 = (Vector3) args[(object) "position"];
      }
      else
      {
        if (args.Contains((object) "x"))
          vector3_1.x = (float) args[(object) "x"];
        if (args.Contains((object) "y"))
          vector3_1.y = (float) args[(object) "y"];
        if (args.Contains((object) "z"))
          vector3_1.z = (float) args[(object) "z"];
      }
      if (flag)
        target.transform.localPosition = vector3_1;
      else
        target.transform.position = vector3_1;
      args[(object) "position"] = (object) vector3_2;
    }
    args[(object) "type"] = (object) "move";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void MoveAdd(GameObject target, Vector3 amount, float time)
  {
    iTween.MoveAdd(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void MoveAdd(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "move";
    args[(object) "method"] = (object) "add";
    iTween.Launch(target, args);
  }

  public static void MoveBy(GameObject target, Vector3 amount, float time)
  {
    iTween.MoveBy(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void MoveBy(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "move";
    args[(object) "method"] = (object) "by";
    iTween.Launch(target, args);
  }

  public static void ScaleTo(GameObject target, Vector3 scale, float time)
  {
    iTween.ScaleTo(target, iTween.Hash((object) "scale", (object) scale, (object) "time", (object) time));
  }

  public static void ScaleTo(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    if (args.Contains((object) "scale") && args[(object) "scale"].GetType() == typeof (Transform))
    {
      Transform transform = (Transform) args[(object) "scale"];
      args[(object) "position"] = (object) new Vector3(transform.position.x, transform.position.y, transform.position.z);
      args[(object) "rotation"] = (object) new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
      args[(object) "scale"] = (object) new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
    args[(object) "type"] = (object) "scale";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void ScaleFrom(GameObject target, Vector3 scale, float time)
  {
    iTween.ScaleFrom(target, iTween.Hash((object) "scale", (object) scale, (object) "time", (object) time));
  }

  public static void ScaleFrom(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    Vector3 localScale;
    Vector3 vector3 = localScale = target.transform.localScale;
    if (args.Contains((object) "scale"))
    {
      if (args[(object) "scale"].GetType() == typeof (Transform))
        localScale = ((Transform) args[(object) "scale"]).localScale;
      else if (args[(object) "scale"].GetType() == typeof (Vector3))
        localScale = (Vector3) args[(object) "scale"];
    }
    else
    {
      if (args.Contains((object) "x"))
        localScale.x = (float) args[(object) "x"];
      if (args.Contains((object) "y"))
        localScale.y = (float) args[(object) "y"];
      if (args.Contains((object) "z"))
        localScale.z = (float) args[(object) "z"];
    }
    target.transform.localScale = localScale;
    args[(object) "scale"] = (object) vector3;
    args[(object) "type"] = (object) "scale";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void ScaleAdd(GameObject target, Vector3 amount, float time)
  {
    iTween.ScaleAdd(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void ScaleAdd(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "scale";
    args[(object) "method"] = (object) "add";
    iTween.Launch(target, args);
  }

  public static void ScaleBy(GameObject target, Vector3 amount, float time)
  {
    iTween.ScaleBy(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void ScaleBy(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "scale";
    args[(object) "method"] = (object) "by";
    iTween.Launch(target, args);
  }

  public static void RotateTo(GameObject target, Vector3 rotation, float time)
  {
    iTween.RotateTo(target, iTween.Hash((object) "rotation", (object) rotation, (object) "time", (object) time));
  }

  public static void RotateTo(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    if (args.Contains((object) "rotation") && args[(object) "rotation"].GetType() == typeof (Transform))
    {
      Transform transform = (Transform) args[(object) "rotation"];
      args[(object) "position"] = (object) new Vector3(transform.position.x, transform.position.y, transform.position.z);
      args[(object) "rotation"] = (object) new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
      args[(object) "scale"] = (object) new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
    args[(object) "type"] = (object) "rotate";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void RotateFrom(GameObject target, Vector3 rotation, float time)
  {
    iTween.RotateFrom(target, iTween.Hash((object) "rotation", (object) rotation, (object) "time", (object) time));
  }

  public static void RotateFrom(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    bool flag = !args.Contains((object) "islocal") ? iTween.Defaults.isLocal : (bool) args[(object) "islocal"];
    Vector3 vector3_1;
    Vector3 vector3_2 = !flag ? (vector3_1 = target.transform.eulerAngles) : (vector3_1 = target.transform.localEulerAngles);
    if (args.Contains((object) "rotation"))
    {
      if (args[(object) "rotation"].GetType() == typeof (Transform))
        vector3_1 = ((Transform) args[(object) "rotation"]).eulerAngles;
      else if (args[(object) "rotation"].GetType() == typeof (Vector3))
        vector3_1 = (Vector3) args[(object) "rotation"];
    }
    else
    {
      if (args.Contains((object) "x"))
        vector3_1.x = (float) args[(object) "x"];
      if (args.Contains((object) "y"))
        vector3_1.y = (float) args[(object) "y"];
      if (args.Contains((object) "z"))
        vector3_1.z = (float) args[(object) "z"];
    }
    if (flag)
      target.transform.localEulerAngles = vector3_1;
    else
      target.transform.eulerAngles = vector3_1;
    args[(object) "rotation"] = (object) vector3_2;
    args[(object) "type"] = (object) "rotate";
    args[(object) "method"] = (object) "to";
    iTween.Launch(target, args);
  }

  public static void RotateAdd(GameObject target, Vector3 amount, float time)
  {
    iTween.RotateAdd(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void RotateAdd(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "rotate";
    args[(object) "method"] = (object) "add";
    iTween.Launch(target, args);
  }

  public static void RotateBy(GameObject target, Vector3 amount, float time)
  {
    iTween.RotateBy(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void RotateBy(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "rotate";
    args[(object) "method"] = (object) "by";
    iTween.Launch(target, args);
  }

  public static void ShakePosition(GameObject target, Vector3 amount, float time)
  {
    iTween.ShakePosition(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void ShakePosition(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "shake";
    args[(object) "method"] = (object) "position";
    iTween.Launch(target, args);
  }

  public static void ShakeScale(GameObject target, Vector3 amount, float time)
  {
    iTween.ShakeScale(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void ShakeScale(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "shake";
    args[(object) "method"] = (object) "scale";
    iTween.Launch(target, args);
  }

  public static void ShakeRotation(GameObject target, Vector3 amount, float time)
  {
    iTween.ShakeRotation(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void ShakeRotation(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "shake";
    args[(object) "method"] = (object) "rotation";
    iTween.Launch(target, args);
  }

  public static void PunchPosition(GameObject target, Vector3 amount, float time)
  {
    iTween.PunchPosition(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void PunchPosition(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "punch";
    args[(object) "method"] = (object) "position";
    args[(object) "easetype"] = (object) iTween.EaseType.punch;
    iTween.Launch(target, args);
  }

  public static void PunchRotation(GameObject target, Vector3 amount, float time)
  {
    iTween.PunchRotation(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void PunchRotation(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "punch";
    args[(object) "method"] = (object) "rotation";
    args[(object) "easetype"] = (object) iTween.EaseType.punch;
    iTween.Launch(target, args);
  }

  public static void PunchScale(GameObject target, Vector3 amount, float time)
  {
    iTween.PunchScale(target, iTween.Hash((object) "amount", (object) amount, (object) "time", (object) time));
  }

  public static void PunchScale(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "punch";
    args[(object) "method"] = (object) "scale";
    args[(object) "easetype"] = (object) iTween.EaseType.punch;
    iTween.Launch(target, args);
  }

  public static void Timer(GameObject target, Hashtable args)
  {
    args = iTween.CleanArgs(args);
    args[(object) "type"] = (object) "timer";
    iTween.Launch(target, args);
  }

  private void GenerateTargets()
  {
    string type = this.type;
    if (type == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (iTween.\u003C\u003Ef__switch\u0024mapDA == null)
    {
      // ISSUE: reference to a compiler-generated field
      iTween.\u003C\u003Ef__switch\u0024mapDA = new Dictionary<string, int>(10)
      {
        {
          "value",
          0
        },
        {
          "color",
          1
        },
        {
          "audio",
          2
        },
        {
          "move",
          3
        },
        {
          "scale",
          4
        },
        {
          "rotate",
          5
        },
        {
          "shake",
          6
        },
        {
          "punch",
          7
        },
        {
          "look",
          8
        },
        {
          "stab",
          9
        }
      };
    }
    int num1;
    // ISSUE: reference to a compiler-generated field
    if (!iTween.\u003C\u003Ef__switch\u0024mapDA.TryGetValue(type, out num1))
      return;
    switch (num1)
    {
      case 0:
        string method1 = this.method;
        if (method1 == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD1 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD1 = new Dictionary<string, int>(5)
          {
            {
              "float",
              0
            },
            {
              "vector2",
              1
            },
            {
              "vector3",
              2
            },
            {
              "color",
              3
            },
            {
              "rect",
              4
            }
          };
        }
        int num2;
        // ISSUE: reference to a compiler-generated field
        if (!iTween.\u003C\u003Ef__switch\u0024mapD1.TryGetValue(method1, out num2))
          break;
        switch (num2)
        {
          case 0:
            this.GenerateFloatTargets();
            this.apply = new iTween.ApplyTween(this.ApplyFloatTargets);
            return;
          case 1:
            this.GenerateVector2Targets();
            this.apply = new iTween.ApplyTween(this.ApplyVector2Targets);
            return;
          case 2:
            this.GenerateVector3Targets();
            this.apply = new iTween.ApplyTween(this.ApplyVector3Targets);
            return;
          case 3:
            this.GenerateColorTargets();
            this.apply = new iTween.ApplyTween(this.ApplyColorTargets);
            return;
          case 4:
            this.GenerateRectTargets();
            this.apply = new iTween.ApplyTween(this.ApplyRectTargets);
            return;
          default:
            return;
        }
      case 1:
        string method2 = this.method;
        if (method2 == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD2 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD2 = new Dictionary<string, int>(1)
          {
            {
              "to",
              0
            }
          };
        }
        int num3;
        // ISSUE: reference to a compiler-generated field
        if (!iTween.\u003C\u003Ef__switch\u0024mapD2.TryGetValue(method2, out num3) || num3 != 0)
          break;
        this.GenerateColorToTargets();
        this.apply = new iTween.ApplyTween(this.ApplyColorToTargets);
        break;
      case 2:
        string method3 = this.method;
        if (method3 == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD3 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD3 = new Dictionary<string, int>(1)
          {
            {
              "to",
              0
            }
          };
        }
        int num4;
        // ISSUE: reference to a compiler-generated field
        if (!iTween.\u003C\u003Ef__switch\u0024mapD3.TryGetValue(method3, out num4) || num4 != 0)
          break;
        this.GenerateAudioToTargets();
        this.apply = new iTween.ApplyTween(this.ApplyAudioToTargets);
        break;
      case 3:
        string method4 = this.method;
        if (method4 == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD4 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD4 = new Dictionary<string, int>(3)
          {
            {
              "to",
              0
            },
            {
              "by",
              1
            },
            {
              "add",
              1
            }
          };
        }
        int num5;
        // ISSUE: reference to a compiler-generated field
        if (!iTween.\u003C\u003Ef__switch\u0024mapD4.TryGetValue(method4, out num5))
          break;
        if (num5 != 0)
        {
          if (num5 != 1)
            break;
          this.GenerateMoveByTargets();
          this.apply = new iTween.ApplyTween(this.ApplyMoveByTargets);
          break;
        }
        if (this.tweenArguments.Contains((object) "path"))
        {
          this.GenerateMoveToPathTargets();
          this.apply = new iTween.ApplyTween(this.ApplyMoveToPathTargets);
          break;
        }
        this.GenerateMoveToTargets();
        this.apply = new iTween.ApplyTween(this.ApplyMoveToTargets);
        break;
      case 4:
        string method5 = this.method;
        if (method5 == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD5 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD5 = new Dictionary<string, int>(3)
          {
            {
              "to",
              0
            },
            {
              "by",
              1
            },
            {
              "add",
              2
            }
          };
        }
        int num6;
        // ISSUE: reference to a compiler-generated field
        if (!iTween.\u003C\u003Ef__switch\u0024mapD5.TryGetValue(method5, out num6))
          break;
        switch (num6)
        {
          case 0:
            this.GenerateScaleToTargets();
            this.apply = new iTween.ApplyTween(this.ApplyScaleToTargets);
            return;
          case 1:
            this.GenerateScaleByTargets();
            this.apply = new iTween.ApplyTween(this.ApplyScaleToTargets);
            return;
          case 2:
            this.GenerateScaleAddTargets();
            this.apply = new iTween.ApplyTween(this.ApplyScaleToTargets);
            return;
          default:
            return;
        }
      case 5:
        string method6 = this.method;
        if (method6 == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD6 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD6 = new Dictionary<string, int>(3)
          {
            {
              "to",
              0
            },
            {
              "add",
              1
            },
            {
              "by",
              2
            }
          };
        }
        int num7;
        // ISSUE: reference to a compiler-generated field
        if (!iTween.\u003C\u003Ef__switch\u0024mapD6.TryGetValue(method6, out num7))
          break;
        switch (num7)
        {
          case 0:
            this.GenerateRotateToTargets();
            this.apply = new iTween.ApplyTween(this.ApplyRotateToTargets);
            return;
          case 1:
            this.GenerateRotateAddTargets();
            this.apply = new iTween.ApplyTween(this.ApplyRotateAddTargets);
            return;
          case 2:
            this.GenerateRotateByTargets();
            this.apply = new iTween.ApplyTween(this.ApplyRotateAddTargets);
            return;
          default:
            return;
        }
      case 6:
        string method7 = this.method;
        if (method7 == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD7 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD7 = new Dictionary<string, int>(3)
          {
            {
              "position",
              0
            },
            {
              "scale",
              1
            },
            {
              "rotation",
              2
            }
          };
        }
        int num8;
        // ISSUE: reference to a compiler-generated field
        if (!iTween.\u003C\u003Ef__switch\u0024mapD7.TryGetValue(method7, out num8))
          break;
        switch (num8)
        {
          case 0:
            this.GenerateShakePositionTargets();
            this.apply = new iTween.ApplyTween(this.ApplyShakePositionTargets);
            return;
          case 1:
            this.GenerateShakeScaleTargets();
            this.apply = new iTween.ApplyTween(this.ApplyShakeScaleTargets);
            return;
          case 2:
            this.GenerateShakeRotationTargets();
            this.apply = new iTween.ApplyTween(this.ApplyShakeRotationTargets);
            return;
          default:
            return;
        }
      case 7:
        string method8 = this.method;
        if (method8 == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD8 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD8 = new Dictionary<string, int>(3)
          {
            {
              "position",
              0
            },
            {
              "rotation",
              1
            },
            {
              "scale",
              2
            }
          };
        }
        int num9;
        // ISSUE: reference to a compiler-generated field
        if (!iTween.\u003C\u003Ef__switch\u0024mapD8.TryGetValue(method8, out num9))
          break;
        switch (num9)
        {
          case 0:
            this.GeneratePunchPositionTargets();
            this.apply = new iTween.ApplyTween(this.ApplyPunchPositionTargets);
            return;
          case 1:
            this.GeneratePunchRotationTargets();
            this.apply = new iTween.ApplyTween(this.ApplyPunchRotationTargets);
            return;
          case 2:
            this.GeneratePunchScaleTargets();
            this.apply = new iTween.ApplyTween(this.ApplyPunchScaleTargets);
            return;
          default:
            return;
        }
      case 8:
        string method9 = this.method;
        if (method9 == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapD9 == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapD9 = new Dictionary<string, int>(1)
          {
            {
              "to",
              0
            }
          };
        }
        int num10;
        // ISSUE: reference to a compiler-generated field
        if (!iTween.\u003C\u003Ef__switch\u0024mapD9.TryGetValue(method9, out num10) || num10 != 0)
          break;
        this.GenerateLookToTargets();
        this.apply = new iTween.ApplyTween(this.ApplyLookToTargets);
        break;
      case 9:
        this.GenerateStabTargets();
        this.apply = new iTween.ApplyTween(this.ApplyStabTargets);
        break;
    }
  }

  private void GenerateRectTargets()
  {
    this.rects = new Rect[3];
    this.rects[0] = (Rect) this.tweenArguments[(object) "from"];
    this.rects[1] = (Rect) this.tweenArguments[(object) "to"];
  }

  private void GenerateColorTargets()
  {
    this.colors = new Color[1, 3];
    this.colors[0, 0] = (Color) this.tweenArguments[(object) "from"];
    this.colors[0, 1] = (Color) this.tweenArguments[(object) "to"];
  }

  private void GenerateVector3Targets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = (Vector3) this.tweenArguments[(object) "from"];
    this.vector3s[1] = (Vector3) this.tweenArguments[(object) "to"];
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateVector2Targets()
  {
    this.vector2s = new Vector2[3];
    this.vector2s[0] = (Vector2) this.tweenArguments[(object) "from"];
    this.vector2s[1] = (Vector2) this.tweenArguments[(object) "to"];
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(new Vector3(this.vector2s[0].x, this.vector2s[0].y, 0.0f), new Vector3(this.vector2s[1].x, this.vector2s[1].y, 0.0f))) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateFloatTargets()
  {
    this.floats = new float[3];
    this.floats[0] = (float) this.tweenArguments[(object) "from"];
    this.floats[1] = (float) this.tweenArguments[(object) "to"];
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(this.floats[0] - this.floats[1]) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateColorToTargets()
  {
    if ((bool) ((UnityEngine.Object) this.GetComponent(typeof (GUITexture))))
    {
      this.colors = new Color[1, 3];
      this.colors[0, 0] = this.colors[0, 1] = this.guiTexture.color;
    }
    else if ((bool) ((UnityEngine.Object) this.GetComponent(typeof (GUIText))))
    {
      this.colors = new Color[1, 3];
      this.colors[0, 0] = this.colors[0, 1] = this.guiText.material.color;
    }
    else if ((bool) ((UnityEngine.Object) this.renderer))
    {
      int num = 0;
      this.colors = new Color[this.renderer.materials.Length, 3];
      for (int index = 0; index < this.renderer.materials.Length; ++index)
      {
        if (this.renderer.materials[index].HasProperty(this.namedColorValueString))
        {
          this.colors[index, 0] = this.renderer.materials[index].GetColor(this.namedColorValueString);
          this.colors[index, 1] = this.renderer.materials[index].GetColor(this.namedColorValueString);
          ++num;
        }
      }
    }
    else if ((bool) ((UnityEngine.Object) this.light))
    {
      this.colors = new Color[1, 3];
      this.colors[0, 0] = this.colors[0, 1] = this.light.color;
    }
    else
      this.colors = new Color[1, 3];
    if (this.tweenArguments.Contains((object) "color"))
    {
      for (int index = 0; index < this.colors.GetLength(0); ++index)
        this.colors[index, 1] = (Color) this.tweenArguments[(object) "color"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "r"))
      {
        for (int index = 0; index < this.colors.GetLength(0); ++index)
          this.colors[index, 1].r = (float) this.tweenArguments[(object) "r"];
      }
      if (this.tweenArguments.Contains((object) "g"))
      {
        for (int index = 0; index < this.colors.GetLength(0); ++index)
          this.colors[index, 1].g = (float) this.tweenArguments[(object) "g"];
      }
      if (this.tweenArguments.Contains((object) "b"))
      {
        for (int index = 0; index < this.colors.GetLength(0); ++index)
          this.colors[index, 1].b = (float) this.tweenArguments[(object) "b"];
      }
      if (this.tweenArguments.Contains((object) "a"))
      {
        for (int index = 0; index < this.colors.GetLength(0); ++index)
          this.colors[index, 1].a = (float) this.tweenArguments[(object) "a"];
      }
    }
    if (this.tweenArguments.Contains((object) "amount"))
    {
      for (int index = 0; index < this.colors.GetLength(0); ++index)
        this.colors[index, 1].a = (float) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (!this.tweenArguments.Contains((object) "alpha"))
        return;
      for (int index = 0; index < this.colors.GetLength(0); ++index)
        this.colors[index, 1].a = (float) this.tweenArguments[(object) "alpha"];
    }
  }

  private void GenerateAudioToTargets()
  {
    this.vector2s = new Vector2[3];
    if (this.tweenArguments.Contains((object) "audiosource"))
      this.audioSource = (AudioSource) this.tweenArguments[(object) "audiosource"];
    else if ((bool) ((UnityEngine.Object) this.GetComponent(typeof (AudioSource))))
    {
      this.audioSource = this.audio;
    }
    else
    {
      Debug.LogError((object) "iTween Error: AudioTo requires an AudioSource.");
      this.Dispose();
    }
    this.vector2s[0] = this.vector2s[1] = new Vector2(this.audioSource.volume, this.audioSource.pitch);
    if (this.tweenArguments.Contains((object) "volume"))
      this.vector2s[1].x = (float) this.tweenArguments[(object) "volume"];
    if (!this.tweenArguments.Contains((object) "pitch"))
      return;
    this.vector2s[1].y = (float) this.tweenArguments[(object) "pitch"];
  }

  private void GenerateStabTargets()
  {
    if (this.tweenArguments.Contains((object) "audiosource"))
      this.audioSource = (AudioSource) this.tweenArguments[(object) "audiosource"];
    else if ((bool) ((UnityEngine.Object) this.GetComponent(typeof (AudioSource))))
    {
      this.audioSource = this.audio;
    }
    else
    {
      this.gameObject.AddComponent(typeof (AudioSource));
      this.audioSource = this.audio;
      this.audioSource.playOnAwake = false;
    }
    this.audioSource.clip = (AudioClip) this.tweenArguments[(object) "audioclip"];
    if (this.tweenArguments.Contains((object) "pitch"))
      this.audioSource.pitch = (float) this.tweenArguments[(object) "pitch"];
    if (this.tweenArguments.Contains((object) "volume"))
      this.audioSource.volume = (float) this.tweenArguments[(object) "volume"];
    this.time = this.audioSource.clip.length / this.audioSource.pitch;
  }

  private void GenerateLookToTargets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = this.transform.eulerAngles;
    if (this.tweenArguments.Contains((object) "looktarget"))
    {
      if (this.tweenArguments[(object) "looktarget"].GetType() == typeof (Transform))
      {
        Transform transform = this.transform;
        Transform tweenArgument1 = (Transform) this.tweenArguments[(object) "looktarget"];
        Vector3? tweenArgument2 = (Vector3?) this.tweenArguments[(object) "up"];
        Vector3 worldUp = !tweenArgument2.HasValue ? iTween.Defaults.up : tweenArgument2.Value;
        transform.LookAt(tweenArgument1, worldUp);
      }
      else if (this.tweenArguments[(object) "looktarget"].GetType() == typeof (Vector3))
      {
        Transform transform = this.transform;
        Vector3 tweenArgument1 = (Vector3) this.tweenArguments[(object) "looktarget"];
        Vector3? tweenArgument2 = (Vector3?) this.tweenArguments[(object) "up"];
        Vector3 worldUp = !tweenArgument2.HasValue ? iTween.Defaults.up : tweenArgument2.Value;
        transform.LookAt(tweenArgument1, worldUp);
      }
    }
    else
    {
      Debug.LogError((object) "iTween Error: LookTo needs a 'looktarget' property!");
      this.Dispose();
    }
    this.vector3s[1] = this.transform.eulerAngles;
    this.transform.eulerAngles = this.vector3s[0];
    if (this.tweenArguments.Contains((object) "axis"))
    {
      string tweenArgument = (string) this.tweenArguments[(object) "axis"];
      if (tweenArgument != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapDB == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapDB = new Dictionary<string, int>(3)
          {
            {
              "x",
              0
            },
            {
              "y",
              1
            },
            {
              "z",
              2
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapDB.TryGetValue(tweenArgument, out num))
        {
          switch (num)
          {
            case 0:
              this.vector3s[1].y = this.vector3s[0].y;
              this.vector3s[1].z = this.vector3s[0].z;
              break;
            case 1:
              this.vector3s[1].x = this.vector3s[0].x;
              this.vector3s[1].z = this.vector3s[0].z;
              break;
            case 2:
              this.vector3s[1].x = this.vector3s[0].x;
              this.vector3s[1].y = this.vector3s[0].y;
              break;
          }
        }
      }
    }
    this.vector3s[1] = new Vector3(this.clerp(this.vector3s[0].x, this.vector3s[1].x, 1f), this.clerp(this.vector3s[0].y, this.vector3s[1].y, 1f), this.clerp(this.vector3s[0].z, this.vector3s[1].z, 1f));
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateMoveToPathTargets()
  {
    Vector3[] vector3Array1;
    if (this.tweenArguments[(object) "path"].GetType() == typeof (Vector3[]))
    {
      Vector3[] tweenArgument = (Vector3[]) this.tweenArguments[(object) "path"];
      if (tweenArgument.Length == 1)
      {
        Debug.LogError((object) "iTween Error: Attempting a path movement with MoveTo requires an array of more than 1 entry!");
        this.Dispose();
      }
      vector3Array1 = new Vector3[tweenArgument.Length];
      Array.Copy((Array) tweenArgument, (Array) vector3Array1, tweenArgument.Length);
    }
    else
    {
      Transform[] tweenArgument = (Transform[]) this.tweenArguments[(object) "path"];
      if (tweenArgument.Length == 1)
      {
        Debug.LogError((object) "iTween Error: Attempting a path movement with MoveTo requires an array of more than 1 entry!");
        this.Dispose();
      }
      vector3Array1 = new Vector3[tweenArgument.Length];
      for (int index = 0; index < tweenArgument.Length; ++index)
        vector3Array1[index] = tweenArgument[index].position;
    }
    bool flag;
    int num;
    if (this.transform.position != vector3Array1[0])
    {
      if (!this.tweenArguments.Contains((object) "movetopath") || (bool) this.tweenArguments[(object) "movetopath"])
      {
        flag = true;
        num = 3;
      }
      else
      {
        flag = false;
        num = 2;
      }
    }
    else
    {
      flag = false;
      num = 2;
    }
    this.vector3s = new Vector3[vector3Array1.Length + num];
    int destinationIndex;
    if (flag)
    {
      this.vector3s[1] = this.transform.position;
      destinationIndex = 2;
    }
    else
      destinationIndex = 1;
    Array.Copy((Array) vector3Array1, 0, (Array) this.vector3s, destinationIndex, vector3Array1.Length);
    this.vector3s[0] = this.vector3s[1] + this.vector3s[1] - this.vector3s[2];
    this.vector3s[this.vector3s.Length - 1] = this.vector3s[this.vector3s.Length - 2] + this.vector3s[this.vector3s.Length - 2] - this.vector3s[this.vector3s.Length - 3];
    if (this.vector3s[1] == this.vector3s[this.vector3s.Length - 2])
    {
      Vector3[] vector3Array2 = new Vector3[this.vector3s.Length];
      Array.Copy((Array) this.vector3s, (Array) vector3Array2, this.vector3s.Length);
      vector3Array2[0] = vector3Array2[vector3Array2.Length - 3];
      vector3Array2[vector3Array2.Length - 1] = vector3Array2[2];
      this.vector3s = new Vector3[vector3Array2.Length];
      Array.Copy((Array) vector3Array2, (Array) this.vector3s, vector3Array2.Length);
    }
    this.path = new iTween.CRSpline(this.vector3s);
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = iTween.PathLength(this.vector3s) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateMoveToTargets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = !this.isLocal ? (this.vector3s[1] = this.transform.position) : (this.vector3s[1] = this.transform.localPosition);
    if (this.tweenArguments.Contains((object) "position"))
    {
      if (this.tweenArguments[(object) "position"].GetType() == typeof (Transform))
        this.vector3s[1] = ((Transform) this.tweenArguments[(object) "position"]).position;
      else if (this.tweenArguments[(object) "position"].GetType() == typeof (Vector3))
        this.vector3s[1] = (Vector3) this.tweenArguments[(object) "position"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = (float) this.tweenArguments[(object) "y"];
      if (this.tweenArguments.Contains((object) "z"))
        this.vector3s[1].z = (float) this.tweenArguments[(object) "z"];
    }
    if (this.tweenArguments.Contains((object) "orienttopath") && (bool) this.tweenArguments[(object) "orienttopath"])
      this.tweenArguments[(object) "looktarget"] = (object) this.vector3s[1];
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateMoveByTargets()
  {
    this.vector3s = new Vector3[6];
    this.vector3s[4] = this.transform.eulerAngles;
    this.vector3s[0] = this.vector3s[1] = this.vector3s[3] = this.transform.position;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] = this.vector3s[0] + (Vector3) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = this.vector3s[0].x + (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = this.vector3s[0].y + (float) this.tweenArguments[(object) "y"];
      if (this.tweenArguments.Contains((object) "z"))
        this.vector3s[1].z = this.vector3s[0].z + (float) this.tweenArguments[(object) "z"];
    }
    this.transform.Translate(this.vector3s[1], this.space);
    this.vector3s[5] = this.transform.position;
    this.transform.position = this.vector3s[0];
    if (this.tweenArguments.Contains((object) "orienttopath") && (bool) this.tweenArguments[(object) "orienttopath"])
      this.tweenArguments[(object) "looktarget"] = (object) this.vector3s[1];
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateScaleToTargets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = this.vector3s[1] = this.transform.localScale;
    if (this.tweenArguments.Contains((object) "scale"))
    {
      if (this.tweenArguments[(object) "scale"].GetType() == typeof (Transform))
        this.vector3s[1] = ((Transform) this.tweenArguments[(object) "scale"]).localScale;
      else if (this.tweenArguments[(object) "scale"].GetType() == typeof (Vector3))
        this.vector3s[1] = (Vector3) this.tweenArguments[(object) "scale"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = (float) this.tweenArguments[(object) "y"];
      if (this.tweenArguments.Contains((object) "z"))
        this.vector3s[1].z = (float) this.tweenArguments[(object) "z"];
    }
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateScaleByTargets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = this.vector3s[1] = this.transform.localScale;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] = Vector3.Scale(this.vector3s[1], (Vector3) this.tweenArguments[(object) "amount"]);
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x *= (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y *= (float) this.tweenArguments[(object) "y"];
      if (this.tweenArguments.Contains((object) "z"))
        this.vector3s[1].z *= (float) this.tweenArguments[(object) "z"];
    }
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateScaleAddTargets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = this.vector3s[1] = this.transform.localScale;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] += (Vector3) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x += (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y += (float) this.tweenArguments[(object) "y"];
      if (this.tweenArguments.Contains((object) "z"))
        this.vector3s[1].z += (float) this.tweenArguments[(object) "z"];
    }
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateRotateToTargets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = !this.isLocal ? (this.vector3s[1] = this.transform.eulerAngles) : (this.vector3s[1] = this.transform.localEulerAngles);
    if (this.tweenArguments.Contains((object) "rotation"))
    {
      if (this.tweenArguments[(object) "rotation"].GetType() == typeof (Transform))
        this.vector3s[1] = ((Transform) this.tweenArguments[(object) "rotation"]).eulerAngles;
      else if (this.tweenArguments[(object) "rotation"].GetType() == typeof (Vector3))
        this.vector3s[1] = (Vector3) this.tweenArguments[(object) "rotation"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = (float) this.tweenArguments[(object) "y"];
      if (this.tweenArguments.Contains((object) "z"))
        this.vector3s[1].z = (float) this.tweenArguments[(object) "z"];
    }
    this.vector3s[1] = new Vector3(this.clerp(this.vector3s[0].x, this.vector3s[1].x, 1f), this.clerp(this.vector3s[0].y, this.vector3s[1].y, 1f), this.clerp(this.vector3s[0].z, this.vector3s[1].z, 1f));
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateRotateAddTargets()
  {
    this.vector3s = new Vector3[5];
    this.vector3s[0] = this.vector3s[1] = this.vector3s[3] = this.transform.eulerAngles;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] += (Vector3) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x += (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y += (float) this.tweenArguments[(object) "y"];
      if (this.tweenArguments.Contains((object) "z"))
        this.vector3s[1].z += (float) this.tweenArguments[(object) "z"];
    }
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateRotateByTargets()
  {
    this.vector3s = new Vector3[4];
    this.vector3s[0] = this.vector3s[1] = this.vector3s[3] = this.transform.eulerAngles;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] += Vector3.Scale((Vector3) this.tweenArguments[(object) "amount"], new Vector3(360f, 360f, 360f));
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x += 360f * (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y += 360f * (float) this.tweenArguments[(object) "y"];
      if (this.tweenArguments.Contains((object) "z"))
        this.vector3s[1].z += 360f * (float) this.tweenArguments[(object) "z"];
    }
    if (!this.tweenArguments.Contains((object) "speed"))
      return;
    this.time = Math.Abs(Vector3.Distance(this.vector3s[0], this.vector3s[1])) / (float) this.tweenArguments[(object) "speed"];
  }

  private void GenerateShakePositionTargets()
  {
    this.vector3s = new Vector3[4];
    this.vector3s[3] = this.transform.eulerAngles;
    this.vector3s[0] = !this.isLocal ? this.transform.position : this.transform.localPosition;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] = (Vector3) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = (float) this.tweenArguments[(object) "y"];
      if (!this.tweenArguments.Contains((object) "z"))
        return;
      this.vector3s[1].z = (float) this.tweenArguments[(object) "z"];
    }
  }

  private void GenerateShakeScaleTargets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = this.transform.localScale;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] = (Vector3) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = (float) this.tweenArguments[(object) "y"];
      if (!this.tweenArguments.Contains((object) "z"))
        return;
      this.vector3s[1].z = (float) this.tweenArguments[(object) "z"];
    }
  }

  private void GenerateShakeRotationTargets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = this.transform.eulerAngles;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] = (Vector3) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = (float) this.tweenArguments[(object) "y"];
      if (!this.tweenArguments.Contains((object) "z"))
        return;
      this.vector3s[1].z = (float) this.tweenArguments[(object) "z"];
    }
  }

  private void GeneratePunchPositionTargets()
  {
    this.vector3s = new Vector3[5];
    this.vector3s[4] = this.transform.eulerAngles;
    this.vector3s[0] = this.transform.position;
    this.vector3s[1] = this.vector3s[3] = Vector3.zero;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] = (Vector3) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = (float) this.tweenArguments[(object) "y"];
      if (!this.tweenArguments.Contains((object) "z"))
        return;
      this.vector3s[1].z = (float) this.tweenArguments[(object) "z"];
    }
  }

  private void GeneratePunchRotationTargets()
  {
    this.vector3s = new Vector3[4];
    this.vector3s[0] = this.transform.eulerAngles;
    this.vector3s[1] = this.vector3s[3] = Vector3.zero;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] = (Vector3) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = (float) this.tweenArguments[(object) "y"];
      if (!this.tweenArguments.Contains((object) "z"))
        return;
      this.vector3s[1].z = (float) this.tweenArguments[(object) "z"];
    }
  }

  private void GeneratePunchScaleTargets()
  {
    this.vector3s = new Vector3[3];
    this.vector3s[0] = this.transform.localScale;
    this.vector3s[1] = Vector3.zero;
    if (this.tweenArguments.Contains((object) "amount"))
    {
      this.vector3s[1] = (Vector3) this.tweenArguments[(object) "amount"];
    }
    else
    {
      if (this.tweenArguments.Contains((object) "x"))
        this.vector3s[1].x = (float) this.tweenArguments[(object) "x"];
      if (this.tweenArguments.Contains((object) "y"))
        this.vector3s[1].y = (float) this.tweenArguments[(object) "y"];
      if (!this.tweenArguments.Contains((object) "z"))
        return;
      this.vector3s[1].z = (float) this.tweenArguments[(object) "z"];
    }
  }

  private void ApplyRectTargets()
  {
    this.rects[2].x = this.ease(this.rects[0].x, this.rects[1].x, this.percentage);
    this.rects[2].y = this.ease(this.rects[0].y, this.rects[1].y, this.percentage);
    this.rects[2].width = this.ease(this.rects[0].width, this.rects[1].width, this.percentage);
    this.rects[2].height = this.ease(this.rects[0].height, this.rects[1].height, this.percentage);
    this.tweenArguments[(object) "onupdateparams"] = (object) this.rects[2];
    if ((double) this.percentage != 1.0)
      return;
    this.tweenArguments[(object) "onupdateparams"] = (object) this.rects[1];
  }

  private void ApplyColorTargets()
  {
    this.colors[0, 2].r = this.ease(this.colors[0, 0].r, this.colors[0, 1].r, this.percentage);
    this.colors[0, 2].g = this.ease(this.colors[0, 0].g, this.colors[0, 1].g, this.percentage);
    this.colors[0, 2].b = this.ease(this.colors[0, 0].b, this.colors[0, 1].b, this.percentage);
    this.colors[0, 2].a = this.ease(this.colors[0, 0].a, this.colors[0, 1].a, this.percentage);
    this.tweenArguments[(object) "onupdateparams"] = (object) this.colors[0, 2];
    if ((double) this.percentage != 1.0)
      return;
    this.tweenArguments[(object) "onupdateparams"] = (object) this.colors[0, 1];
  }

  private void ApplyVector3Targets()
  {
    this.vector3s[2].x = this.ease(this.vector3s[0].x, this.vector3s[1].x, this.percentage);
    this.vector3s[2].y = this.ease(this.vector3s[0].y, this.vector3s[1].y, this.percentage);
    this.vector3s[2].z = this.ease(this.vector3s[0].z, this.vector3s[1].z, this.percentage);
    this.tweenArguments[(object) "onupdateparams"] = (object) this.vector3s[2];
    if ((double) this.percentage != 1.0)
      return;
    this.tweenArguments[(object) "onupdateparams"] = (object) this.vector3s[1];
  }

  private void ApplyVector2Targets()
  {
    this.vector2s[2].x = this.ease(this.vector2s[0].x, this.vector2s[1].x, this.percentage);
    this.vector2s[2].y = this.ease(this.vector2s[0].y, this.vector2s[1].y, this.percentage);
    this.tweenArguments[(object) "onupdateparams"] = (object) this.vector2s[2];
    if ((double) this.percentage != 1.0)
      return;
    this.tweenArguments[(object) "onupdateparams"] = (object) this.vector2s[1];
  }

  private void ApplyFloatTargets()
  {
    this.floats[2] = this.ease(this.floats[0], this.floats[1], this.percentage);
    this.tweenArguments[(object) "onupdateparams"] = (object) this.floats[2];
    if ((double) this.percentage != 1.0)
      return;
    this.tweenArguments[(object) "onupdateparams"] = (object) this.floats[1];
  }

  private void ApplyColorToTargets()
  {
    for (int index = 0; index < this.colors.GetLength(0); ++index)
    {
      this.colors[index, 2].r = this.ease(this.colors[index, 0].r, this.colors[index, 1].r, this.percentage);
      this.colors[index, 2].g = this.ease(this.colors[index, 0].g, this.colors[index, 1].g, this.percentage);
      this.colors[index, 2].b = this.ease(this.colors[index, 0].b, this.colors[index, 1].b, this.percentage);
      this.colors[index, 2].a = this.ease(this.colors[index, 0].a, this.colors[index, 1].a, this.percentage);
    }
    if ((bool) ((UnityEngine.Object) this.GetComponent(typeof (GUITexture))))
      this.guiTexture.color = this.colors[0, 2];
    else if ((bool) ((UnityEngine.Object) this.GetComponent(typeof (GUIText))))
      this.guiText.material.color = this.colors[0, 2];
    else if ((bool) ((UnityEngine.Object) this.renderer))
    {
      for (int index = 0; index < this.colors.GetLength(0); ++index)
        this.renderer.materials[index].SetColor(this.namedColorValueString, this.colors[index, 2]);
    }
    else if ((bool) ((UnityEngine.Object) this.light))
      this.light.color = this.colors[0, 2];
    if ((double) this.percentage != 1.0)
      return;
    if ((bool) ((UnityEngine.Object) this.GetComponent(typeof (GUITexture))))
      this.guiTexture.color = this.colors[0, 1];
    else if ((bool) ((UnityEngine.Object) this.GetComponent(typeof (GUIText))))
      this.guiText.material.color = this.colors[0, 1];
    else if ((bool) ((UnityEngine.Object) this.renderer))
    {
      for (int index = 0; index < this.colors.GetLength(0); ++index)
        this.renderer.materials[index].SetColor(this.namedColorValueString, this.colors[index, 1]);
    }
    else
    {
      if (!(bool) ((UnityEngine.Object) this.light))
        return;
      this.light.color = this.colors[0, 1];
    }
  }

  private void ApplyAudioToTargets()
  {
    this.vector2s[2].x = this.ease(this.vector2s[0].x, this.vector2s[1].x, this.percentage);
    this.vector2s[2].y = this.ease(this.vector2s[0].y, this.vector2s[1].y, this.percentage);
    this.audioSource.volume = this.vector2s[2].x;
    this.audioSource.pitch = this.vector2s[2].y;
    if ((double) this.percentage != 1.0)
      return;
    this.audioSource.volume = this.vector2s[1].x;
    this.audioSource.pitch = this.vector2s[1].y;
  }

  private void ApplyStabTargets()
  {
  }

  private void ApplyMoveToPathTargets()
  {
    this.preUpdate = this.transform.position;
    float num = this.ease(0.0f, 1f, this.percentage);
    if (this.isLocal)
      this.transform.localPosition = this.path.Interp(Mathf.Clamp(num, 0.0f, 1f));
    else
      this.transform.position = this.path.Interp(Mathf.Clamp(num, 0.0f, 1f));
    if (this.tweenArguments.Contains((object) "orienttopath") && (bool) this.tweenArguments[(object) "orienttopath"])
      this.tweenArguments[(object) "looktarget"] = (object) this.path.Interp(Mathf.Clamp(this.ease(0.0f, 1f, Mathf.Min(1f, this.percentage + (!this.tweenArguments.Contains((object) "lookahead") ? iTween.Defaults.lookAhead : (float) this.tweenArguments[(object) "lookahead"]))), 0.0f, 1f));
    this.postUpdate = this.transform.position;
    if (!this.physics)
      return;
    this.transform.position = this.preUpdate;
    this.rigidbody.MovePosition(this.postUpdate);
  }

  private void ApplyMoveToTargets()
  {
    this.preUpdate = this.transform.position;
    this.vector3s[2].x = this.ease(this.vector3s[0].x, this.vector3s[1].x, this.percentage);
    this.vector3s[2].y = this.ease(this.vector3s[0].y, this.vector3s[1].y, this.percentage);
    this.vector3s[2].z = this.ease(this.vector3s[0].z, this.vector3s[1].z, this.percentage);
    if (this.isLocal)
      this.transform.localPosition = this.vector3s[2];
    else
      this.transform.position = this.vector3s[2];
    if ((double) this.percentage == 1.0)
    {
      if (this.isLocal)
        this.transform.localPosition = this.vector3s[1];
      else
        this.transform.position = this.vector3s[1];
    }
    this.postUpdate = this.transform.position;
    if (!this.physics)
      return;
    this.transform.position = this.preUpdate;
    this.rigidbody.MovePosition(this.postUpdate);
  }

  private void ApplyMoveByTargets()
  {
    this.preUpdate = this.transform.position;
    Vector3 vector3 = new Vector3();
    if (this.tweenArguments.Contains((object) "looktarget"))
    {
      vector3 = this.transform.eulerAngles;
      this.transform.eulerAngles = this.vector3s[4];
    }
    this.vector3s[2].x = this.ease(this.vector3s[0].x, this.vector3s[1].x, this.percentage);
    this.vector3s[2].y = this.ease(this.vector3s[0].y, this.vector3s[1].y, this.percentage);
    this.vector3s[2].z = this.ease(this.vector3s[0].z, this.vector3s[1].z, this.percentage);
    this.transform.Translate(this.vector3s[2] - this.vector3s[3], this.space);
    this.vector3s[3] = this.vector3s[2];
    if (this.tweenArguments.Contains((object) "looktarget"))
      this.transform.eulerAngles = vector3;
    this.postUpdate = this.transform.position;
    if (!this.physics)
      return;
    this.transform.position = this.preUpdate;
    this.rigidbody.MovePosition(this.postUpdate);
  }

  private void ApplyScaleToTargets()
  {
    this.vector3s[2].x = this.ease(this.vector3s[0].x, this.vector3s[1].x, this.percentage);
    this.vector3s[2].y = this.ease(this.vector3s[0].y, this.vector3s[1].y, this.percentage);
    this.vector3s[2].z = this.ease(this.vector3s[0].z, this.vector3s[1].z, this.percentage);
    this.transform.localScale = this.vector3s[2];
    if ((double) this.percentage != 1.0)
      return;
    this.transform.localScale = this.vector3s[1];
  }

  private void ApplyLookToTargets()
  {
    this.vector3s[2].x = this.ease(this.vector3s[0].x, this.vector3s[1].x, this.percentage);
    this.vector3s[2].y = this.ease(this.vector3s[0].y, this.vector3s[1].y, this.percentage);
    this.vector3s[2].z = this.ease(this.vector3s[0].z, this.vector3s[1].z, this.percentage);
    if (this.isLocal)
      this.transform.localRotation = Quaternion.Euler(this.vector3s[2]);
    else
      this.transform.rotation = Quaternion.Euler(this.vector3s[2]);
  }

  private void ApplyRotateToTargets()
  {
    this.preUpdate = this.transform.eulerAngles;
    this.vector3s[2].x = this.ease(this.vector3s[0].x, this.vector3s[1].x, this.percentage);
    this.vector3s[2].y = this.ease(this.vector3s[0].y, this.vector3s[1].y, this.percentage);
    this.vector3s[2].z = this.ease(this.vector3s[0].z, this.vector3s[1].z, this.percentage);
    if (this.isLocal)
      this.transform.localRotation = Quaternion.Euler(this.vector3s[2]);
    else
      this.transform.rotation = Quaternion.Euler(this.vector3s[2]);
    if ((double) this.percentage == 1.0)
    {
      if (this.isLocal)
        this.transform.localRotation = Quaternion.Euler(this.vector3s[1]);
      else
        this.transform.rotation = Quaternion.Euler(this.vector3s[1]);
    }
    this.postUpdate = this.transform.eulerAngles;
    if (!this.physics)
      return;
    this.transform.eulerAngles = this.preUpdate;
    this.rigidbody.MoveRotation(Quaternion.Euler(this.postUpdate));
  }

  private void ApplyRotateAddTargets()
  {
    this.preUpdate = this.transform.eulerAngles;
    this.vector3s[2].x = this.ease(this.vector3s[0].x, this.vector3s[1].x, this.percentage);
    this.vector3s[2].y = this.ease(this.vector3s[0].y, this.vector3s[1].y, this.percentage);
    this.vector3s[2].z = this.ease(this.vector3s[0].z, this.vector3s[1].z, this.percentage);
    this.transform.Rotate(this.vector3s[2] - this.vector3s[3], this.space);
    this.vector3s[3] = this.vector3s[2];
    this.postUpdate = this.transform.eulerAngles;
    if (!this.physics)
      return;
    this.transform.eulerAngles = this.preUpdate;
    this.rigidbody.MoveRotation(Quaternion.Euler(this.postUpdate));
  }

  private void ApplyShakePositionTargets()
  {
    this.preUpdate = !this.isLocal ? this.transform.position : this.transform.localPosition;
    Vector3 vector3 = new Vector3();
    if (this.tweenArguments.Contains((object) "looktarget"))
    {
      vector3 = this.transform.eulerAngles;
      this.transform.eulerAngles = this.vector3s[3];
    }
    float num = 1f - this.percentage;
    this.vector3s[2].x = UnityEngine.Random.Range(-this.vector3s[1].x * num, this.vector3s[1].x * num);
    this.vector3s[2].y = UnityEngine.Random.Range(-this.vector3s[1].y * num, this.vector3s[1].y * num);
    this.vector3s[2].z = UnityEngine.Random.Range(-this.vector3s[1].z * num, this.vector3s[1].z * num);
    if (this.isLocal)
      this.transform.localPosition = this.vector3s[0] + this.vector3s[2];
    else
      this.transform.position = this.vector3s[0] + this.vector3s[2];
    if (this.tweenArguments.Contains((object) "looktarget"))
      this.transform.eulerAngles = vector3;
    this.postUpdate = this.transform.position;
    if (!this.physics)
      return;
    this.transform.position = this.preUpdate;
    this.rigidbody.MovePosition(this.postUpdate);
  }

  private void ApplyShakeScaleTargets()
  {
    if ((double) this.percentage == 0.0)
      this.transform.localScale = this.vector3s[1];
    this.transform.localScale = this.vector3s[0];
    float num = 1f - this.percentage;
    this.vector3s[2].x = UnityEngine.Random.Range(-this.vector3s[1].x * num, this.vector3s[1].x * num);
    this.vector3s[2].y = UnityEngine.Random.Range(-this.vector3s[1].y * num, this.vector3s[1].y * num);
    this.vector3s[2].z = UnityEngine.Random.Range(-this.vector3s[1].z * num, this.vector3s[1].z * num);
    this.transform.localScale += this.vector3s[2];
  }

  private void ApplyShakeRotationTargets()
  {
    this.preUpdate = this.transform.eulerAngles;
    if ((double) this.percentage == 0.0)
      this.transform.Rotate(this.vector3s[1], this.space);
    this.transform.eulerAngles = this.vector3s[0];
    float num = 1f - this.percentage;
    this.vector3s[2].x = UnityEngine.Random.Range(-this.vector3s[1].x * num, this.vector3s[1].x * num);
    this.vector3s[2].y = UnityEngine.Random.Range(-this.vector3s[1].y * num, this.vector3s[1].y * num);
    this.vector3s[2].z = UnityEngine.Random.Range(-this.vector3s[1].z * num, this.vector3s[1].z * num);
    this.transform.Rotate(this.vector3s[2], this.space);
    this.postUpdate = this.transform.eulerAngles;
    if (!this.physics)
      return;
    this.transform.eulerAngles = this.preUpdate;
    this.rigidbody.MoveRotation(Quaternion.Euler(this.postUpdate));
  }

  private void ApplyPunchPositionTargets()
  {
    this.preUpdate = this.transform.position;
    Vector3 vector3 = new Vector3();
    if (this.tweenArguments.Contains((object) "looktarget"))
    {
      vector3 = this.transform.eulerAngles;
      this.transform.eulerAngles = this.vector3s[4];
    }
    if ((double) this.vector3s[1].x > 0.0)
      this.vector3s[2].x = this.punch(this.vector3s[1].x, this.percentage);
    else if ((double) this.vector3s[1].x < 0.0)
      this.vector3s[2].x = -this.punch(Mathf.Abs(this.vector3s[1].x), this.percentage);
    if ((double) this.vector3s[1].y > 0.0)
      this.vector3s[2].y = this.punch(this.vector3s[1].y, this.percentage);
    else if ((double) this.vector3s[1].y < 0.0)
      this.vector3s[2].y = -this.punch(Mathf.Abs(this.vector3s[1].y), this.percentage);
    if ((double) this.vector3s[1].z > 0.0)
      this.vector3s[2].z = this.punch(this.vector3s[1].z, this.percentage);
    else if ((double) this.vector3s[1].z < 0.0)
      this.vector3s[2].z = -this.punch(Mathf.Abs(this.vector3s[1].z), this.percentage);
    this.transform.Translate(this.vector3s[2] - this.vector3s[3], this.space);
    this.vector3s[3] = this.vector3s[2];
    if (this.tweenArguments.Contains((object) "looktarget"))
      this.transform.eulerAngles = vector3;
    this.postUpdate = this.transform.position;
    if (!this.physics)
      return;
    this.transform.position = this.preUpdate;
    this.rigidbody.MovePosition(this.postUpdate);
  }

  private void ApplyPunchRotationTargets()
  {
    this.preUpdate = this.transform.eulerAngles;
    if ((double) this.vector3s[1].x > 0.0)
      this.vector3s[2].x = this.punch(this.vector3s[1].x, this.percentage);
    else if ((double) this.vector3s[1].x < 0.0)
      this.vector3s[2].x = -this.punch(Mathf.Abs(this.vector3s[1].x), this.percentage);
    if ((double) this.vector3s[1].y > 0.0)
      this.vector3s[2].y = this.punch(this.vector3s[1].y, this.percentage);
    else if ((double) this.vector3s[1].y < 0.0)
      this.vector3s[2].y = -this.punch(Mathf.Abs(this.vector3s[1].y), this.percentage);
    if ((double) this.vector3s[1].z > 0.0)
      this.vector3s[2].z = this.punch(this.vector3s[1].z, this.percentage);
    else if ((double) this.vector3s[1].z < 0.0)
      this.vector3s[2].z = -this.punch(Mathf.Abs(this.vector3s[1].z), this.percentage);
    this.transform.Rotate(this.vector3s[2] - this.vector3s[3], this.space);
    this.vector3s[3] = this.vector3s[2];
    this.postUpdate = this.transform.eulerAngles;
    if (!this.physics)
      return;
    this.transform.eulerAngles = this.preUpdate;
    this.rigidbody.MoveRotation(Quaternion.Euler(this.postUpdate));
  }

  private void ApplyPunchScaleTargets()
  {
    if ((double) this.vector3s[1].x > 0.0)
      this.vector3s[2].x = this.punch(this.vector3s[1].x, this.percentage);
    else if ((double) this.vector3s[1].x < 0.0)
      this.vector3s[2].x = -this.punch(Mathf.Abs(this.vector3s[1].x), this.percentage);
    if ((double) this.vector3s[1].y > 0.0)
      this.vector3s[2].y = this.punch(this.vector3s[1].y, this.percentage);
    else if ((double) this.vector3s[1].y < 0.0)
      this.vector3s[2].y = -this.punch(Mathf.Abs(this.vector3s[1].y), this.percentage);
    if ((double) this.vector3s[1].z > 0.0)
      this.vector3s[2].z = this.punch(this.vector3s[1].z, this.percentage);
    else if ((double) this.vector3s[1].z < 0.0)
      this.vector3s[2].z = -this.punch(Mathf.Abs(this.vector3s[1].z), this.percentage);
    this.transform.localScale = this.vector3s[0] + this.vector3s[2];
  }

  private void ResetDelay()
  {
    this.delayStarted = Time.time;
    this.waitForDelay = true;
  }

  private void TweenStart()
  {
    if (this.tweenArguments == null)
      return;
    this.CallBack(iTween.CallbackType.OnStart);
    if (!this.loop)
    {
      this.ConflictCheck();
      this.GenerateTargets();
    }
    this.loop = true;
    if (this.type == "stab")
      this.audioSource.PlayOneShot(this.audioSource.clip);
    if (this.type == "move" || this.type == "scale" || (this.type == "rotate" || this.type == "punch") || (this.type == "shake" || this.type == "curve" || this.type == "look"))
      this.EnableKinematic();
    this.isRunning = true;
  }

  private void TweenRestart()
  {
    this.ResetDelay();
    this.loop = true;
  }

  private void TweenUpdate()
  {
    if (this.type != "timer")
    {
      if (this.apply == null)
        return;
      this.apply();
    }
    this.CallBack(iTween.CallbackType.OnUpdate);
    this.UpdatePercentage();
  }

  private void TweenComplete()
  {
    this.isRunning = false;
    this.percentage = (double) this.percentage <= 0.5 ? 0.0f : 1f;
    if (this.type != "timer")
      this.apply();
    if (this.type == "value" || this.type == "timer")
      this.CallBack(iTween.CallbackType.OnUpdate);
    if (this.loopType == iTween.LoopType.none)
      this.Dispose();
    else
      this.TweenLoop();
    this.CallBack(iTween.CallbackType.OnComplete);
  }

  private void TweenLoop()
  {
    this.DisableKinematic();
    switch (this.loopType)
    {
      case iTween.LoopType.loop:
        this.percentage = 0.0f;
        this.runningTime = 0.0f;
        if (this.apply != null)
          this.apply();
        this.TweenRestart();
        break;
      case iTween.LoopType.pingPong:
        this.reverse = !this.reverse;
        this.runningTime = 0.0f;
        this.TweenRestart();
        break;
    }
  }

  public static Rect RectUpdate(Rect currentValue, Rect targetValue, float speed)
  {
    return new Rect(iTween.FloatUpdate(currentValue.x, targetValue.x, speed), iTween.FloatUpdate(currentValue.y, targetValue.y, speed), iTween.FloatUpdate(currentValue.width, targetValue.width, speed), iTween.FloatUpdate(currentValue.height, targetValue.height, speed));
  }

  public static Vector3 Vector3Update(Vector3 currentValue, Vector3 targetValue, float speed)
  {
    Vector3 vector3 = targetValue - currentValue;
    currentValue += vector3 * speed * Time.deltaTime;
    return currentValue;
  }

  public static Vector2 Vector2Update(Vector2 currentValue, Vector2 targetValue, float speed)
  {
    Vector2 vector2 = targetValue - currentValue;
    currentValue += vector2 * speed * Time.deltaTime;
    return currentValue;
  }

  public static float FloatUpdate(float currentValue, float targetValue, float speed)
  {
    float num = targetValue - currentValue;
    currentValue += num * speed * Time.deltaTime;
    return currentValue;
  }

  public static void FadeUpdate(GameObject target, Hashtable args)
  {
    args[(object) "a"] = args[(object) "alpha"];
    iTween.ColorUpdate(target, args);
  }

  public static void FadeUpdate(GameObject target, float alpha, float time)
  {
    iTween.FadeUpdate(target, iTween.Hash((object) "alpha", (object) alpha, (object) "time", (object) time));
  }

  public static void ColorUpdate(GameObject target, Hashtable args)
  {
    iTween.CleanArgs(args);
    Color[] colorArray = new Color[4];
    if (!args.Contains((object) "includechildren") || (bool) args[(object) "includechildren"])
    {
      IEnumerator enumerator = target.transform.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
          iTween.ColorUpdate(((Component) enumerator.Current).gameObject, args);
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
    }
    float smoothTime = !args.Contains((object) "time") ? iTween.Defaults.updateTime : (float) args[(object) "time"] * iTween.Defaults.updateTimePercentage;
    if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (GUITexture))))
      colorArray[0] = colorArray[1] = target.GetComponent<GUITexture>().color;
    else if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (GUIText))))
      colorArray[0] = colorArray[1] = target.GetComponent<GUIText>().material.color;
    else if ((bool) ((UnityEngine.Object) target.GetComponent<Renderer>()))
      colorArray[0] = colorArray[1] = target.GetComponent<Renderer>().material.color;
    else if ((bool) ((UnityEngine.Object) target.GetComponent<Light>()))
      colorArray[0] = colorArray[1] = target.GetComponent<Light>().color;
    else if ((bool) ((UnityEngine.Object) target.GetComponent<UberText>()))
    {
      UberText component = target.GetComponent<UberText>();
      colorArray[0] = colorArray[1] = component.TextColor;
    }
    if (args.Contains((object) "color"))
    {
      colorArray[1] = (Color) args[(object) "color"];
    }
    else
    {
      if (args.Contains((object) "r"))
        colorArray[1].r = (float) args[(object) "r"];
      if (args.Contains((object) "g"))
        colorArray[1].g = (float) args[(object) "g"];
      if (args.Contains((object) "b"))
        colorArray[1].b = (float) args[(object) "b"];
      if (args.Contains((object) "a"))
        colorArray[1].a = (float) args[(object) "a"];
    }
    colorArray[3].r = Mathf.SmoothDamp(colorArray[0].r, colorArray[1].r, ref colorArray[2].r, smoothTime);
    colorArray[3].g = Mathf.SmoothDamp(colorArray[0].g, colorArray[1].g, ref colorArray[2].g, smoothTime);
    colorArray[3].b = Mathf.SmoothDamp(colorArray[0].b, colorArray[1].b, ref colorArray[2].b, smoothTime);
    colorArray[3].a = Mathf.SmoothDamp(colorArray[0].a, colorArray[1].a, ref colorArray[2].a, smoothTime);
    if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (GUITexture))))
      target.GetComponent<GUITexture>().color = colorArray[3];
    else if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (GUIText))))
      target.GetComponent<GUIText>().material.color = colorArray[3];
    else if ((bool) ((UnityEngine.Object) target.GetComponent<Renderer>()))
      target.GetComponent<Renderer>().material.color = colorArray[3];
    else if ((bool) ((UnityEngine.Object) target.GetComponent<Light>()))
    {
      target.GetComponent<Light>().color = colorArray[3];
    }
    else
    {
      if (!(bool) ((UnityEngine.Object) target.GetComponent<UberText>()))
        return;
      UberText component = target.GetComponent<UberText>();
      component.TextAlpha = colorArray[3].a;
      component.OutlineAlpha = colorArray[3].a;
      component.ShadowAlpha = colorArray[3].a;
    }
  }

  public static void ColorUpdate(GameObject target, Color color, float time)
  {
    iTween.ColorUpdate(target, iTween.Hash((object) "color", (object) color, (object) "time", (object) time));
  }

  public static void AudioUpdate(GameObject target, Hashtable args)
  {
    iTween.CleanArgs(args);
    Vector2[] vector2Array = new Vector2[4];
    float smoothTime = !args.Contains((object) "time") ? iTween.Defaults.updateTime : (float) args[(object) "time"] * iTween.Defaults.updateTimePercentage;
    AudioSource component;
    if (args.Contains((object) "audiosource"))
      component = (AudioSource) args[(object) "audiosource"];
    else if ((bool) ((UnityEngine.Object) target.GetComponent(typeof (AudioSource))))
    {
      component = target.GetComponent<AudioSource>();
    }
    else
    {
      Debug.LogError((object) "iTween Error: AudioUpdate requires an AudioSource.");
      return;
    }
    vector2Array[0] = vector2Array[1] = new Vector2(component.volume, component.pitch);
    if (args.Contains((object) "volume"))
      vector2Array[1].x = (float) args[(object) "volume"];
    if (args.Contains((object) "pitch"))
      vector2Array[1].y = (float) args[(object) "pitch"];
    vector2Array[3].x = Mathf.SmoothDampAngle(vector2Array[0].x, vector2Array[1].x, ref vector2Array[2].x, smoothTime);
    vector2Array[3].y = Mathf.SmoothDampAngle(vector2Array[0].y, vector2Array[1].y, ref vector2Array[2].y, smoothTime);
    component.volume = vector2Array[3].x;
    component.pitch = vector2Array[3].y;
  }

  public static void AudioUpdate(GameObject target, float volume, float pitch, float time)
  {
    iTween.AudioUpdate(target, iTween.Hash((object) "volume", (object) volume, (object) "pitch", (object) pitch, (object) "time", (object) time));
  }

  public static void RotateUpdate(GameObject target, Hashtable args)
  {
    iTween.CleanArgs(args);
    Vector3[] vector3Array = new Vector3[4];
    Vector3 eulerAngles1 = target.transform.eulerAngles;
    float smoothTime = !args.Contains((object) "time") ? iTween.Defaults.updateTime : (float) args[(object) "time"] * iTween.Defaults.updateTimePercentage;
    bool flag = !args.Contains((object) "islocal") ? iTween.Defaults.isLocal : (bool) args[(object) "islocal"];
    vector3Array[0] = !flag ? target.transform.eulerAngles : target.transform.localEulerAngles;
    if (args.Contains((object) "rotation"))
    {
      if (args[(object) "rotation"].GetType() == typeof (Transform))
      {
        Transform transform = (Transform) args[(object) "rotation"];
        vector3Array[1] = transform.eulerAngles;
      }
      else if (args[(object) "rotation"].GetType() == typeof (Vector3))
        vector3Array[1] = (Vector3) args[(object) "rotation"];
    }
    vector3Array[3].x = Mathf.SmoothDampAngle(vector3Array[0].x, vector3Array[1].x, ref vector3Array[2].x, smoothTime);
    vector3Array[3].y = Mathf.SmoothDampAngle(vector3Array[0].y, vector3Array[1].y, ref vector3Array[2].y, smoothTime);
    vector3Array[3].z = Mathf.SmoothDampAngle(vector3Array[0].z, vector3Array[1].z, ref vector3Array[2].z, smoothTime);
    if (flag)
      target.transform.localEulerAngles = vector3Array[3];
    else
      target.transform.eulerAngles = vector3Array[3];
    if (!((UnityEngine.Object) target.GetComponent<Rigidbody>() != (UnityEngine.Object) null))
      return;
    Vector3 eulerAngles2 = target.transform.eulerAngles;
    target.transform.eulerAngles = eulerAngles1;
    target.GetComponent<Rigidbody>().MoveRotation(Quaternion.Euler(eulerAngles2));
  }

  public static void RotateUpdate(GameObject target, Vector3 rotation, float time)
  {
    iTween.RotateUpdate(target, iTween.Hash((object) "rotation", (object) rotation, (object) "time", (object) time));
  }

  public static void ScaleUpdate(GameObject target, Hashtable args)
  {
    iTween.CleanArgs(args);
    Vector3[] vector3Array = new Vector3[4];
    float smoothTime = !args.Contains((object) "time") ? iTween.Defaults.updateTime : (float) args[(object) "time"] * iTween.Defaults.updateTimePercentage;
    vector3Array[0] = vector3Array[1] = target.transform.localScale;
    if (args.Contains((object) "scale"))
    {
      if (args[(object) "scale"].GetType() == typeof (Transform))
      {
        Transform transform = (Transform) args[(object) "scale"];
        vector3Array[1] = transform.localScale;
      }
      else if (args[(object) "scale"].GetType() == typeof (Vector3))
        vector3Array[1] = (Vector3) args[(object) "scale"];
    }
    else
    {
      if (args.Contains((object) "x"))
        vector3Array[1].x = (float) args[(object) "x"];
      if (args.Contains((object) "y"))
        vector3Array[1].y = (float) args[(object) "y"];
      if (args.Contains((object) "z"))
        vector3Array[1].z = (float) args[(object) "z"];
    }
    vector3Array[3].x = Mathf.SmoothDamp(vector3Array[0].x, vector3Array[1].x, ref vector3Array[2].x, smoothTime);
    vector3Array[3].y = Mathf.SmoothDamp(vector3Array[0].y, vector3Array[1].y, ref vector3Array[2].y, smoothTime);
    vector3Array[3].z = Mathf.SmoothDamp(vector3Array[0].z, vector3Array[1].z, ref vector3Array[2].z, smoothTime);
    target.transform.localScale = vector3Array[3];
  }

  public static void ScaleUpdate(GameObject target, Vector3 scale, float time)
  {
    iTween.ScaleUpdate(target, iTween.Hash((object) "scale", (object) scale, (object) "time", (object) time));
  }

  public static void MoveUpdate(GameObject target, Hashtable args)
  {
    iTween.CleanArgs(args);
    Vector3[] vector3Array = new Vector3[4];
    Vector3 position1 = target.transform.position;
    float smoothTime = !args.Contains((object) "time") ? iTween.Defaults.updateTime : (float) args[(object) "time"] * iTween.Defaults.updateTimePercentage;
    bool flag = !args.Contains((object) "islocal") ? iTween.Defaults.isLocal : (bool) args[(object) "islocal"];
    vector3Array[0] = !flag ? (vector3Array[1] = target.transform.position) : (vector3Array[1] = target.transform.localPosition);
    if (args.Contains((object) "position"))
    {
      if (args[(object) "position"].GetType() == typeof (Transform))
      {
        Transform transform = (Transform) args[(object) "position"];
        vector3Array[1] = transform.position;
      }
      else if (args[(object) "position"].GetType() == typeof (Vector3))
        vector3Array[1] = (Vector3) args[(object) "position"];
    }
    else
    {
      if (args.Contains((object) "x"))
        vector3Array[1].x = (float) args[(object) "x"];
      if (args.Contains((object) "y"))
        vector3Array[1].y = (float) args[(object) "y"];
      if (args.Contains((object) "z"))
        vector3Array[1].z = (float) args[(object) "z"];
    }
    vector3Array[3].x = Mathf.SmoothDamp(vector3Array[0].x, vector3Array[1].x, ref vector3Array[2].x, smoothTime);
    vector3Array[3].y = Mathf.SmoothDamp(vector3Array[0].y, vector3Array[1].y, ref vector3Array[2].y, smoothTime);
    vector3Array[3].z = Mathf.SmoothDamp(vector3Array[0].z, vector3Array[1].z, ref vector3Array[2].z, smoothTime);
    if (args.Contains((object) "orienttopath") && (bool) args[(object) "orienttopath"])
      args[(object) "looktarget"] = (object) vector3Array[3];
    if (args.Contains((object) "looktarget"))
      iTween.LookUpdate(target, args);
    if (flag)
      target.transform.localPosition = vector3Array[3];
    else
      target.transform.position = vector3Array[3];
    if (!((UnityEngine.Object) target.GetComponent<Rigidbody>() != (UnityEngine.Object) null))
      return;
    Vector3 position2 = target.transform.position;
    target.transform.position = position1;
    target.GetComponent<Rigidbody>().MovePosition(position2);
  }

  public static void MoveUpdate(GameObject target, Vector3 position, float time)
  {
    iTween.MoveUpdate(target, iTween.Hash((object) "position", (object) position, (object) "time", (object) time));
  }

  public static void LookUpdate(GameObject target, Hashtable args)
  {
    iTween.CleanArgs(args);
    Vector3[] vector3Array = new Vector3[5];
    float smoothTime = !args.Contains((object) "looktime") ? (!args.Contains((object) "time") ? iTween.Defaults.updateTime : (float) args[(object) "time"] * 0.15f * iTween.Defaults.updateTimePercentage) : (float) args[(object) "looktime"] * iTween.Defaults.updateTimePercentage;
    vector3Array[0] = target.transform.eulerAngles;
    if (args.Contains((object) "looktarget"))
    {
      if (args[(object) "looktarget"].GetType() == typeof (Transform))
      {
        Transform transform = target.transform;
        Transform target1 = (Transform) args[(object) "looktarget"];
        Vector3? nullable = (Vector3?) args[(object) "up"];
        Vector3 worldUp = !nullable.HasValue ? iTween.Defaults.up : nullable.Value;
        transform.LookAt(target1, worldUp);
      }
      else if (args[(object) "looktarget"].GetType() == typeof (Vector3))
      {
        Transform transform = target.transform;
        Vector3 worldPosition = (Vector3) args[(object) "looktarget"];
        Vector3? nullable = (Vector3?) args[(object) "up"];
        Vector3 worldUp = !nullable.HasValue ? iTween.Defaults.up : nullable.Value;
        transform.LookAt(worldPosition, worldUp);
      }
      vector3Array[1] = target.transform.eulerAngles;
      target.transform.eulerAngles = vector3Array[0];
      vector3Array[3].x = Mathf.SmoothDampAngle(vector3Array[0].x, vector3Array[1].x, ref vector3Array[2].x, smoothTime);
      vector3Array[3].y = Mathf.SmoothDampAngle(vector3Array[0].y, vector3Array[1].y, ref vector3Array[2].y, smoothTime);
      vector3Array[3].z = Mathf.SmoothDampAngle(vector3Array[0].z, vector3Array[1].z, ref vector3Array[2].z, smoothTime);
      target.transform.eulerAngles = vector3Array[3];
      if (!args.Contains((object) "axis"))
        return;
      vector3Array[4] = target.transform.eulerAngles;
      string key = (string) args[(object) "axis"];
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapDC == null)
        {
          // ISSUE: reference to a compiler-generated field
          iTween.\u003C\u003Ef__switch\u0024mapDC = new Dictionary<string, int>(3)
          {
            {
              "x",
              0
            },
            {
              "y",
              1
            },
            {
              "z",
              2
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (iTween.\u003C\u003Ef__switch\u0024mapDC.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              vector3Array[4].y = vector3Array[0].y;
              vector3Array[4].z = vector3Array[0].z;
              break;
            case 1:
              vector3Array[4].x = vector3Array[0].x;
              vector3Array[4].z = vector3Array[0].z;
              break;
            case 2:
              vector3Array[4].x = vector3Array[0].x;
              vector3Array[4].y = vector3Array[0].y;
              break;
          }
        }
      }
      target.transform.eulerAngles = vector3Array[4];
    }
    else
      Debug.LogError((object) "iTween Error: LookUpdate needs a 'looktarget' property!");
  }

  public static void LookUpdate(GameObject target, Vector3 looktarget, float time)
  {
    iTween.LookUpdate(target, iTween.Hash((object) "looktarget", (object) looktarget, (object) "time", (object) time));
  }

  public static float PathLength(Transform[] path)
  {
    Vector3[] path1 = new Vector3[path.Length];
    float num1 = 0.0f;
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    Vector3[] pts = iTween.PathControlPointGenerator(path1);
    Vector3 a = iTween.Interp(pts, 0.0f);
    int num2 = path.Length * 20;
    for (int index = 1; index <= num2; ++index)
    {
      float t = (float) index / (float) num2;
      Vector3 b = iTween.Interp(pts, t);
      num1 += Vector3.Distance(a, b);
      a = b;
    }
    return num1;
  }

  public static float PathLength(Vector3[] path)
  {
    float num1 = 0.0f;
    Vector3[] pts = iTween.PathControlPointGenerator(path);
    Vector3 a = iTween.Interp(pts, 0.0f);
    int num2 = path.Length * 20;
    for (int index = 1; index <= num2; ++index)
    {
      float t = (float) index / (float) num2;
      Vector3 b = iTween.Interp(pts, t);
      num1 += Vector3.Distance(a, b);
      a = b;
    }
    return num1;
  }

  public static Texture2D CameraTexture(Color color)
  {
    Texture2D texture2D = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
    Color[] colors = new Color[Screen.width * Screen.height];
    for (int index = 0; index < colors.Length; ++index)
      colors[index] = color;
    texture2D.SetPixels(colors);
    texture2D.Apply();
    return texture2D;
  }

  public static void PutOnPath(GameObject target, Vector3[] path, float percent)
  {
    target.transform.position = iTween.Interp(iTween.PathControlPointGenerator(path), percent);
  }

  public static void PutOnPath(Transform target, Vector3[] path, float percent)
  {
    target.position = iTween.Interp(iTween.PathControlPointGenerator(path), percent);
  }

  public static void PutOnPath(GameObject target, Transform[] path, float percent)
  {
    Vector3[] path1 = new Vector3[path.Length];
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    target.transform.position = iTween.Interp(iTween.PathControlPointGenerator(path1), percent);
  }

  public static void PutOnPath(Transform target, Transform[] path, float percent)
  {
    Vector3[] path1 = new Vector3[path.Length];
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    target.position = iTween.Interp(iTween.PathControlPointGenerator(path1), percent);
  }

  public static Vector3 PointOnPath(Transform[] path, float percent)
  {
    Vector3[] path1 = new Vector3[path.Length];
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    return iTween.Interp(iTween.PathControlPointGenerator(path1), percent);
  }

  public static void DrawLine(Vector3[] line)
  {
    if (line.Length <= 0)
      return;
    iTween.DrawLineHelper(line, iTween.Defaults.color, "gizmos");
  }

  public static void DrawLine(Vector3[] line, Color color)
  {
    if (line.Length <= 0)
      return;
    iTween.DrawLineHelper(line, color, "gizmos");
  }

  public static void DrawLine(Transform[] line)
  {
    if (line.Length <= 0)
      return;
    Vector3[] line1 = new Vector3[line.Length];
    for (int index = 0; index < line.Length; ++index)
      line1[index] = line[index].position;
    iTween.DrawLineHelper(line1, iTween.Defaults.color, "gizmos");
  }

  public static void DrawLine(Transform[] line, Color color)
  {
    if (line.Length <= 0)
      return;
    Vector3[] line1 = new Vector3[line.Length];
    for (int index = 0; index < line.Length; ++index)
      line1[index] = line[index].position;
    iTween.DrawLineHelper(line1, color, "gizmos");
  }

  public static void DrawLineGizmos(Vector3[] line)
  {
    if (line.Length <= 0)
      return;
    iTween.DrawLineHelper(line, iTween.Defaults.color, "gizmos");
  }

  public static void DrawLineGizmos(Vector3[] line, Color color)
  {
    if (line.Length <= 0)
      return;
    iTween.DrawLineHelper(line, color, "gizmos");
  }

  public static void DrawLineGizmos(Transform[] line)
  {
    if (line.Length <= 0)
      return;
    Vector3[] line1 = new Vector3[line.Length];
    for (int index = 0; index < line.Length; ++index)
      line1[index] = line[index].position;
    iTween.DrawLineHelper(line1, iTween.Defaults.color, "gizmos");
  }

  public static void DrawLineGizmos(Transform[] line, Color color)
  {
    if (line.Length <= 0)
      return;
    Vector3[] line1 = new Vector3[line.Length];
    for (int index = 0; index < line.Length; ++index)
      line1[index] = line[index].position;
    iTween.DrawLineHelper(line1, color, "gizmos");
  }

  public static void DrawLineHandles(Vector3[] line)
  {
    if (line.Length <= 0)
      return;
    iTween.DrawLineHelper(line, iTween.Defaults.color, "handles");
  }

  public static void DrawLineHandles(Vector3[] line, Color color)
  {
    if (line.Length <= 0)
      return;
    iTween.DrawLineHelper(line, color, "handles");
  }

  public static void DrawLineHandles(Transform[] line)
  {
    if (line.Length <= 0)
      return;
    Vector3[] line1 = new Vector3[line.Length];
    for (int index = 0; index < line.Length; ++index)
      line1[index] = line[index].position;
    iTween.DrawLineHelper(line1, iTween.Defaults.color, "handles");
  }

  public static void DrawLineHandles(Transform[] line, Color color)
  {
    if (line.Length <= 0)
      return;
    Vector3[] line1 = new Vector3[line.Length];
    for (int index = 0; index < line.Length; ++index)
      line1[index] = line[index].position;
    iTween.DrawLineHelper(line1, color, "handles");
  }

  public static Vector3 PointOnPath(Vector3[] path, float percent)
  {
    return iTween.Interp(iTween.PathControlPointGenerator(path), percent);
  }

  public static void DrawPath(Vector3[] path)
  {
    if (path.Length <= 0)
      return;
    iTween.DrawPathHelper(path, iTween.Defaults.color, "gizmos");
  }

  public static void DrawPath(Vector3[] path, Color color)
  {
    if (path.Length <= 0)
      return;
    iTween.DrawPathHelper(path, color, "gizmos");
  }

  public static void DrawPath(Transform[] path)
  {
    if (path.Length <= 0)
      return;
    Vector3[] path1 = new Vector3[path.Length];
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    iTween.DrawPathHelper(path1, iTween.Defaults.color, "gizmos");
  }

  public static void DrawPath(Transform[] path, Color color)
  {
    if (path.Length <= 0)
      return;
    Vector3[] path1 = new Vector3[path.Length];
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    iTween.DrawPathHelper(path1, color, "gizmos");
  }

  public static void DrawPathGizmos(Vector3[] path)
  {
    if (path.Length <= 0)
      return;
    iTween.DrawPathHelper(path, iTween.Defaults.color, "gizmos");
  }

  public static void DrawPathGizmos(Vector3[] path, Color color)
  {
    if (path.Length <= 0)
      return;
    iTween.DrawPathHelper(path, color, "gizmos");
  }

  public static void DrawPathGizmos(Transform[] path)
  {
    if (path.Length <= 0)
      return;
    Vector3[] path1 = new Vector3[path.Length];
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    iTween.DrawPathHelper(path1, iTween.Defaults.color, "gizmos");
  }

  public static void DrawPathGizmos(Transform[] path, Color color)
  {
    if (path.Length <= 0)
      return;
    Vector3[] path1 = new Vector3[path.Length];
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    iTween.DrawPathHelper(path1, color, "gizmos");
  }

  public static void DrawPathHandles(Vector3[] path)
  {
    if (path.Length <= 0)
      return;
    iTween.DrawPathHelper(path, iTween.Defaults.color, "handles");
  }

  public static void DrawPathHandles(Vector3[] path, Color color)
  {
    if (path.Length <= 0)
      return;
    iTween.DrawPathHelper(path, color, "handles");
  }

  public static void DrawPathHandles(Transform[] path)
  {
    if (path.Length <= 0)
      return;
    Vector3[] path1 = new Vector3[path.Length];
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    iTween.DrawPathHelper(path1, iTween.Defaults.color, "handles");
  }

  public static void DrawPathHandles(Transform[] path, Color color)
  {
    if (path.Length <= 0)
      return;
    Vector3[] path1 = new Vector3[path.Length];
    for (int index = 0; index < path.Length; ++index)
      path1[index] = path[index].position;
    iTween.DrawPathHelper(path1, color, "handles");
  }

  public static void CameraFadeDepth(int depth)
  {
    if (!(bool) ((UnityEngine.Object) iTween.cameraFade))
      return;
    iTween.cameraFade.transform.position = new Vector3(iTween.cameraFade.transform.position.x, iTween.cameraFade.transform.position.y, (float) depth);
  }

  public static void CameraFadeDestroy()
  {
    if (!(bool) ((UnityEngine.Object) iTween.cameraFade))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) iTween.cameraFade);
  }

  public static void CameraFadeSwap(Texture2D texture)
  {
    if (!(bool) ((UnityEngine.Object) iTween.cameraFade))
      return;
    iTween.cameraFade.GetComponent<GUITexture>().texture = (Texture) texture;
  }

  public static GameObject CameraFadeAdd(Texture2D texture, int depth)
  {
    if ((bool) ((UnityEngine.Object) iTween.cameraFade))
      return (GameObject) null;
    iTween.cameraFade = new GameObject("iTween Camera Fade");
    iTween.cameraFade.transform.position = new Vector3(0.5f, 0.5f, (float) depth);
    iTween.cameraFade.AddComponent<GUITexture>();
    iTween.cameraFade.GetComponent<GUITexture>().texture = (Texture) texture;
    iTween.cameraFade.GetComponent<GUITexture>().color = new Color(0.5f, 0.5f, 0.5f, 0.0f);
    return iTween.cameraFade;
  }

  public static GameObject CameraFadeAdd(Texture2D texture)
  {
    if ((bool) ((UnityEngine.Object) iTween.cameraFade))
      return (GameObject) null;
    iTween.cameraFade = new GameObject("iTween Camera Fade");
    iTween.cameraFade.transform.position = new Vector3(0.5f, 0.5f, (float) iTween.Defaults.cameraFadeDepth);
    iTween.cameraFade.AddComponent<GUITexture>();
    iTween.cameraFade.GetComponent<GUITexture>().texture = (Texture) texture;
    iTween.cameraFade.GetComponent<GUITexture>().color = new Color(0.5f, 0.5f, 0.5f, 0.0f);
    return iTween.cameraFade;
  }

  public static GameObject CameraFadeAdd()
  {
    if ((bool) ((UnityEngine.Object) iTween.cameraFade))
      return (GameObject) null;
    iTween.cameraFade = new GameObject("iTween Camera Fade");
    iTween.cameraFade.transform.position = new Vector3(0.5f, 0.5f, (float) iTween.Defaults.cameraFadeDepth);
    iTween.cameraFade.AddComponent<GUITexture>();
    iTween.cameraFade.GetComponent<GUITexture>().texture = (Texture) iTween.CameraTexture(Color.black);
    iTween.cameraFade.GetComponent<GUITexture>().color = new Color(0.5f, 0.5f, 0.5f, 0.0f);
    return iTween.cameraFade;
  }

  public static void EnableTween(iTween tween)
  {
    tween.enabled = true;
  }

  public static void Resume(GameObject target)
  {
    iTweenManager.ForEachByGameObject(new iTweenManager.TweenOperation(iTween.EnableTween), target);
  }

  public static void Resume(GameObject target, bool includechildren)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.EnableTween), target, (string) null, (string) null, includechildren);
  }

  public static void Resume(GameObject target, string type)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.EnableTween), target, (string) null, type, false);
  }

  public static void Resume(GameObject target, string type, bool includechildren)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.EnableTween), target, (string) null, type, includechildren);
  }

  public static void Resume()
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.EnableTween), (GameObject) null, (string) null, (string) null, false);
  }

  public static void Resume(string type)
  {
    iTweenManager.ForEachByType(new iTweenManager.TweenOperation(iTween.EnableTween), type);
  }

  public static void PauseTween(iTween tween)
  {
    tween.isPaused = true;
    tween.enabled = false;
  }

  public static void Pause(GameObject target)
  {
    iTweenManager.ForEachByGameObject(new iTweenManager.TweenOperation(iTween.PauseTween), target);
  }

  public static void Pause(GameObject target, bool includechildren)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.PauseTween), target, (string) null, (string) null, includechildren);
  }

  public static void Pause(GameObject target, string type)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.PauseTween), target, (string) null, type, false);
  }

  public static void Pause(GameObject target, string type, bool includechildren)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.PauseTween), target, (string) null, type, includechildren);
  }

  public static void Pause()
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.PauseTween), (GameObject) null, (string) null, (string) null, false);
  }

  public static void Pause(string type)
  {
    iTweenManager.ForEachByType(new iTweenManager.TweenOperation(iTween.PauseTween), type);
  }

  public static int Count()
  {
    return iTweenManager.GetTweenCount();
  }

  public static int Count(string type)
  {
    int num = 0;
    iTweenIterator iterator = iTweenManager.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      if ((next.type + next.method).Substring(0, type.Length).ToLower().Equals(type.ToLower()))
        ++num;
    }
    return num;
  }

  public static int Count(GameObject target)
  {
    int num = 0;
    iTweenIterator iterator = iTweenManager.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      if ((UnityEngine.Object) next.gameObject == (UnityEngine.Object) target)
        ++num;
    }
    return num;
  }

  public static int Count(GameObject target, string type)
  {
    int num = 0;
    foreach (iTween iTween in iTweenManager.GetTweensForObject(target))
    {
      if ((iTween.type + iTween.method).Substring(0, type.Length).ToLower() == type.ToLower())
        ++num;
    }
    return num;
  }

  public static int CountByName(GameObject target, string name)
  {
    int num = 0;
    foreach (iTween iTween in iTweenManager.GetTweensForObject(target))
    {
      if (iTween._name == name)
        ++num;
    }
    return num;
  }

  public static int CountOtherTypes(GameObject target, string type)
  {
    int num = 0;
    foreach (iTween iTween in iTweenManager.GetTweensForObject(target))
    {
      if ((iTween.type + iTween.method).Substring(0, type.Length).ToLower() != type.ToLower())
        ++num;
    }
    return num;
  }

  public static int CountOtherNames(GameObject target, string name)
  {
    int num = 0;
    foreach (iTween iTween in iTweenManager.GetTweensForObject(target))
    {
      if (iTween._name != name)
        ++num;
    }
    return num;
  }

  public static bool HasTween(GameObject target)
  {
    return iTween.Count(target) > 0;
  }

  public static bool HasType(GameObject target, string type)
  {
    return iTween.Count(target, type) > 0;
  }

  public static bool HasName(GameObject target, string name)
  {
    return iTween.CountByName(target, name) > 0;
  }

  public static bool HasOtherType(GameObject target, string type)
  {
    foreach (iTween iTween in iTweenManager.GetTweensForObject(target))
    {
      if ((iTween.type + iTween.method).Substring(0, type.Length).ToLower() != type.ToLower())
        return true;
    }
    return false;
  }

  public static bool HasOtherName(GameObject target, string name)
  {
    foreach (iTween iTween in iTweenManager.GetTweensForObject(target))
    {
      if (iTween._name != name)
        return true;
    }
    return false;
  }

  public static bool HasNameNotInList(GameObject target, params string[] names)
  {
    foreach (iTween iTween in iTweenManager.GetTweensForObject(target))
    {
      bool flag = false;
      for (int index = 0; index < names.Length; ++index)
      {
        if (iTween._name == names[index])
        {
          flag = true;
          break;
        }
      }
      if (!flag)
        return true;
    }
    return false;
  }

  public static void StopTween(iTween tween)
  {
    tween.Dispose();
  }

  public static void Stop()
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.StopTween), (GameObject) null, (string) null, (string) null, false);
  }

  public static void Stop(string type)
  {
    iTweenManager.ForEachByType(new iTweenManager.TweenOperation(iTween.StopTween), type);
  }

  public static void StopByName(string name)
  {
    iTweenManager.ForEachByName(new iTweenManager.TweenOperation(iTween.StopTween), name);
  }

  public static void Stop(GameObject target)
  {
    iTweenManager.ForEachByGameObject(new iTweenManager.TweenOperation(iTween.StopTween), target);
  }

  public static void Stop(GameObject target, bool includechildren)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.StopTween), target, (string) null, (string) null, includechildren);
  }

  public static void Stop(GameObject target, string type)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.StopTween), target, (string) null, type, false);
  }

  public static void StopByName(GameObject target, string name)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.StopTween), target, name, (string) null, false);
  }

  public static void Stop(GameObject target, string type, bool includechildren)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.StopTween), target, (string) null, type, includechildren);
  }

  public static void StopByName(GameObject target, string name, bool includechildren)
  {
    iTweenManager.ForEach(new iTweenManager.TweenOperation(iTween.StopTween), target, name, (string) null, includechildren);
  }

  public static void StopOthers(GameObject target, string type, bool includechildren = false)
  {
    iTweenManager.ForEachInverted(new iTweenManager.TweenOperation(iTween.StopTween), target, (string) null, type, includechildren);
  }

  public static void StopOthersByName(GameObject target, string name, bool includechildren = false)
  {
    iTweenManager.ForEachInverted(new iTweenManager.TweenOperation(iTween.StopTween), target, name, (string) null, includechildren);
  }

  public static Hashtable Hash(params object[] args)
  {
    Hashtable hashtable = new Hashtable(args.Length / 2);
    if (args.Length % 2 != 0)
    {
      Debug.LogError((object) "Tween Error: Hash requires an even number of arguments!");
      return (Hashtable) null;
    }
    int index = 0;
    while (index < args.Length - 1)
    {
      hashtable.Add(args[index], args[index + 1]);
      index += 2;
    }
    return hashtable;
  }

  public void Awake()
  {
    this.RetrieveArgs();
    this.lastRealTime = Time.realtimeSinceStartup;
    this.ResetDelay();
  }

  public void Update()
  {
    if (!this.activeInHierarchy)
      return;
    if (this.waitForDelay)
    {
      if ((double) this.delay > 0.0 && (double) this.delay > (double) Time.time - (double) this.delayStarted)
        return;
      this.TweenStart();
      this.waitForDelay = false;
    }
    if (!this.isRunning || this.physics)
      return;
    if (!this.reverse)
    {
      if ((double) this.percentage < 1.0)
        this.TweenUpdate();
      else
        this.TweenComplete();
    }
    else if ((double) this.percentage > 0.0)
      this.TweenUpdate();
    else
      this.TweenComplete();
  }

  public void FixedUpdate()
  {
    if (!this.activeInHierarchy || !this.isRunning || !this.physics)
      return;
    if (!this.reverse)
    {
      if ((double) this.percentage < 1.0)
        this.TweenUpdate();
      else
        this.TweenComplete();
    }
    else if ((double) this.percentage > 0.0)
      this.TweenUpdate();
    else
      this.TweenComplete();
  }

  public void LateUpdate()
  {
    if (!this.activeInHierarchy || this.waitForDelay || (this.tweenArguments == null || !this.tweenArguments.Contains((object) "looktarget")) || !this.isRunning || !(this.type == "move") && !(this.type == "shake") && !(this.type == "punch"))
      return;
    iTween.LookUpdate(this.gameObject, this.tweenArguments);
  }

  public void OnEnable()
  {
    if (this.isRunning)
      this.EnableKinematic();
    if (!this.isPaused)
      return;
    this.isPaused = false;
    if ((double) this.delay <= 0.0)
      return;
    this.ResumeDelay();
  }

  public void OnDisable()
  {
    this.DisableKinematic();
  }

  public void Upkeep()
  {
    if (this.destroyed)
      return;
    if ((UnityEngine.Object) this.gameObject == (UnityEngine.Object) null)
      iTweenManager.Remove(this);
    else if (!this.gameObject.activeInHierarchy || !this.enabled)
    {
      if (this.activeLastTick)
        this.OnDisable();
      this.activeLastTick = false;
    }
    else
    {
      if (!this.activeLastTick)
        this.OnEnable();
      this.activeLastTick = true;
    }
  }

  private static void DrawLineHelper(Vector3[] line, Color color, string method)
  {
    Gizmos.color = color;
    for (int index = 0; index < line.Length - 1; ++index)
    {
      if (method == "gizmos")
        Gizmos.DrawLine(line[index], line[index + 1]);
      else if (method == "handles")
        Debug.LogError((object) "iTween Error: Drawing a line with Handles is temporarily disabled because of compatability issues with Unity 2.6!");
    }
  }

  private static void DrawPathHelper(Vector3[] path, Color color, string method)
  {
    Vector3[] pts = iTween.PathControlPointGenerator(path);
    Vector3 to = iTween.Interp(pts, 0.0f);
    Gizmos.color = color;
    int num = path.Length * 20;
    for (int index = 1; index <= num; ++index)
    {
      float t = (float) index / (float) num;
      Vector3 from = iTween.Interp(pts, t);
      if (method == "gizmos")
        Gizmos.DrawLine(from, to);
      else if (method == "handles")
        Debug.LogError((object) "iTween Error: Drawing a path with Handles is temporarily disabled because of compatability issues with Unity 2.6!");
      to = from;
    }
  }

  private static Vector3[] PathControlPointGenerator(Vector3[] path)
  {
    Vector3[] vector3Array1 = path;
    int num = 2;
    Vector3[] vector3Array2 = new Vector3[vector3Array1.Length + num];
    Array.Copy((Array) vector3Array1, 0, (Array) vector3Array2, 1, vector3Array1.Length);
    vector3Array2[0] = vector3Array2[1] + vector3Array2[1] - vector3Array2[2];
    vector3Array2[vector3Array2.Length - 1] = vector3Array2[vector3Array2.Length - 2] + vector3Array2[vector3Array2.Length - 2] - vector3Array2[vector3Array2.Length - 3];
    if (vector3Array2[1] == vector3Array2[vector3Array2.Length - 2])
    {
      Vector3[] vector3Array3 = new Vector3[vector3Array2.Length];
      Array.Copy((Array) vector3Array2, (Array) vector3Array3, vector3Array2.Length);
      vector3Array3[0] = vector3Array3[vector3Array3.Length - 3];
      vector3Array3[vector3Array3.Length - 1] = vector3Array3[2];
      vector3Array2 = new Vector3[vector3Array3.Length];
      Array.Copy((Array) vector3Array3, (Array) vector3Array2, vector3Array3.Length);
    }
    return vector3Array2;
  }

  private static Vector3 Interp(Vector3[] pts, float t)
  {
    int num1 = pts.Length - 3;
    int index = Mathf.Min(Mathf.FloorToInt(t * (float) num1), num1 - 1);
    float num2 = t * (float) num1 - (float) index;
    Vector3 pt1 = pts[index];
    Vector3 pt2 = pts[index + 1];
    Vector3 pt3 = pts[index + 2];
    Vector3 pt4 = pts[index + 3];
    return 0.5f * ((-pt1 + 3f * pt2 - 3f * pt3 + pt4) * (num2 * num2 * num2) + (2f * pt1 - 5f * pt2 + 4f * pt3 - pt4) * (num2 * num2) + (-pt1 + pt3) * num2 + 2f * pt2);
  }

  private static void Launch(GameObject target, Hashtable args)
  {
    if (!args.Contains((object) "id"))
      args[(object) "id"] = (object) iTween.GenerateID();
    if (!args.Contains((object) "target"))
      args[(object) "target"] = (object) target;
    if (args.Contains((object) "oncomplete") && !args.Contains((object) "onconflict"))
    {
      args[(object) "onconflict"] = args[(object) "oncomplete"];
      if (args.Contains((object) "oncompletetarget") && !args.Contains((object) "onconflicttarget"))
        args[(object) "onconflicttarget"] = args[(object) "oncompletetarget"];
      if (args.Contains((object) "oncompleteparams") && !args.Contains((object) "onconflictparams"))
        args[(object) "onconflictparams"] = args[(object) "oncompleteparams"];
    }
    iTweenManager.Add(new iTween(target, args));
  }

  private static Hashtable CleanArgs(Hashtable args)
  {
    Hashtable hashtable1 = new Hashtable(args.Count);
    Hashtable hashtable2 = new Hashtable(args.Count);
    foreach (DictionaryEntry dictionaryEntry in args)
      hashtable1.Add(dictionaryEntry.Key, dictionaryEntry.Value);
    foreach (DictionaryEntry dictionaryEntry in hashtable1)
    {
      if (!((string) dictionaryEntry.Key == "id"))
      {
        if (dictionaryEntry.Value.GetType() == typeof (int))
        {
          float num = (float) (int) dictionaryEntry.Value;
          args[dictionaryEntry.Key] = (object) num;
        }
        if (dictionaryEntry.Value.GetType() == typeof (double))
        {
          float num = (float) (double) dictionaryEntry.Value;
          args[dictionaryEntry.Key] = (object) num;
        }
      }
    }
    foreach (DictionaryEntry dictionaryEntry in args)
      hashtable2.Add((object) dictionaryEntry.Key.ToString().ToLower(), dictionaryEntry.Value);
    args = hashtable2;
    return args;
  }

  private static int GenerateID()
  {
    int nextId = iTween.nextId;
    iTween.nextId = iTween.nextId != int.MaxValue ? iTween.nextId + 1 : 1;
    return nextId;
  }

  public Vector3 GetTargetPosition()
  {
    if (this.vector3s.Length >= 2)
      return this.vector3s[1];
    return new Vector3(0.0f, 0.0f, 0.0f);
  }

  private void RetrieveArgs()
  {
    if (this.tweenArguments == null)
      return;
    this.id = (int) this.tweenArguments[(object) "id"];
    this.type = (string) this.tweenArguments[(object) "type"];
    this._name = (string) this.tweenArguments[(object) "name"];
    this.method = (string) this.tweenArguments[(object) "method"];
    this.time = !this.tweenArguments.Contains((object) "time") ? iTween.Defaults.time : (float) this.tweenArguments[(object) "time"];
    if ((UnityEngine.Object) this.rigidbody != (UnityEngine.Object) null)
      this.physics = true;
    this.delay = !this.tweenArguments.Contains((object) "delay") ? iTween.Defaults.delay : (float) this.tweenArguments[(object) "delay"];
    if (this.tweenArguments.Contains((object) "namedcolorvalue"))
    {
      if (this.tweenArguments[(object) "namedcolorvalue"].GetType() == typeof (iTween.NamedValueColor))
        this.namedColorValueString = ((iTween.NamedValueColor) this.tweenArguments[(object) "namedcolorvalue"]).ToString();
      else if (this.tweenArguments[(object) "namedcolorvalue"].GetType() == typeof (string))
      {
        this.namedColorValueString = (string) this.tweenArguments[(object) "namedcolorvalue"];
      }
      else
      {
        try
        {
          this.namedColorValueString = ((iTween.NamedValueColor) Enum.Parse(typeof (iTween.NamedValueColor), (string) this.tweenArguments[(object) "namedcolorvalue"], true)).ToString();
        }
        catch
        {
          Debug.LogWarning((object) "iTween: Unsupported namedcolorvalue supplied! Default will be used.");
          this.namedcolorvalue = iTween.NamedValueColor._Color;
          this.namedColorValueString = this.namedcolorvalue.ToString();
        }
      }
    }
    else
    {
      this.namedcolorvalue = iTween.Defaults.namedColorValue;
      this.namedColorValueString = "_Color";
    }
    if (this.tweenArguments.Contains((object) "looptype"))
    {
      if (this.tweenArguments[(object) "looptype"].GetType() == typeof (iTween.LoopType))
      {
        this.loopType = (iTween.LoopType) this.tweenArguments[(object) "looptype"];
      }
      else
      {
        try
        {
          this.loopType = (iTween.LoopType) Enum.Parse(typeof (iTween.LoopType), (string) this.tweenArguments[(object) "looptype"], true);
        }
        catch
        {
          Debug.LogWarning((object) "iTween: Unsupported loopType supplied! Default will be used.");
          this.loopType = iTween.LoopType.none;
        }
      }
    }
    else
      this.loopType = iTween.LoopType.none;
    if (this.tweenArguments.Contains((object) "easetype"))
    {
      if (this.tweenArguments[(object) "easetype"].GetType() == typeof (iTween.EaseType))
      {
        this.easeType = (iTween.EaseType) this.tweenArguments[(object) "easetype"];
      }
      else
      {
        try
        {
          this.easeType = (iTween.EaseType) Enum.Parse(typeof (iTween.EaseType), (string) this.tweenArguments[(object) "easetype"], true);
        }
        catch
        {
          Debug.LogWarning((object) "iTween: Unsupported easeType supplied! Default will be used.");
          this.easeType = iTween.Defaults.easeType;
        }
      }
    }
    else
      this.easeType = iTween.Defaults.easeType;
    if (this.tweenArguments.Contains((object) "space"))
    {
      if (this.tweenArguments[(object) "space"].GetType() == typeof (Space))
      {
        this.space = (Space) this.tweenArguments[(object) "space"];
      }
      else
      {
        try
        {
          this.space = (Space) Enum.Parse(typeof (Space), (string) this.tweenArguments[(object) "space"], true);
        }
        catch
        {
          Debug.LogWarning((object) "iTween: Unsupported space supplied! Default will be used.");
          this.space = iTween.Defaults.space;
        }
      }
    }
    else
      this.space = iTween.Defaults.space;
    this.isLocal = !this.tweenArguments.Contains((object) "islocal") ? iTween.Defaults.isLocal : (bool) this.tweenArguments[(object) "islocal"];
    this.useRealTime = !this.tweenArguments.Contains((object) "ignoretimescale") ? iTween.Defaults.useRealTime : (bool) this.tweenArguments[(object) "ignoretimescale"];
    this.GetEasingFunction();
  }

  private void GetEasingFunction()
  {
    switch (this.easeType)
    {
      case iTween.EaseType.easeInQuad:
        this.ease = new iTween.EasingFunction(this.easeInQuad);
        break;
      case iTween.EaseType.easeOutQuad:
        this.ease = new iTween.EasingFunction(this.easeOutQuad);
        break;
      case iTween.EaseType.easeInOutQuad:
        this.ease = new iTween.EasingFunction(this.easeInOutQuad);
        break;
      case iTween.EaseType.easeInCubic:
        this.ease = new iTween.EasingFunction(this.easeInCubic);
        break;
      case iTween.EaseType.easeOutCubic:
        this.ease = new iTween.EasingFunction(this.easeOutCubic);
        break;
      case iTween.EaseType.easeInOutCubic:
        this.ease = new iTween.EasingFunction(this.easeInOutCubic);
        break;
      case iTween.EaseType.easeInQuart:
        this.ease = new iTween.EasingFunction(this.easeInQuart);
        break;
      case iTween.EaseType.easeOutQuart:
        this.ease = new iTween.EasingFunction(this.easeOutQuart);
        break;
      case iTween.EaseType.easeInOutQuart:
        this.ease = new iTween.EasingFunction(this.easeInOutQuart);
        break;
      case iTween.EaseType.easeInQuint:
        this.ease = new iTween.EasingFunction(this.easeInQuint);
        break;
      case iTween.EaseType.easeOutQuint:
        this.ease = new iTween.EasingFunction(this.easeOutQuint);
        break;
      case iTween.EaseType.easeInOutQuint:
        this.ease = new iTween.EasingFunction(this.easeInOutQuint);
        break;
      case iTween.EaseType.easeInSine:
        this.ease = new iTween.EasingFunction(this.easeInSine);
        break;
      case iTween.EaseType.easeOutSine:
        this.ease = new iTween.EasingFunction(this.easeOutSine);
        break;
      case iTween.EaseType.easeInOutSine:
        this.ease = new iTween.EasingFunction(this.easeInOutSine);
        break;
      case iTween.EaseType.easeInExpo:
        this.ease = new iTween.EasingFunction(this.easeInExpo);
        break;
      case iTween.EaseType.easeOutExpo:
        this.ease = new iTween.EasingFunction(this.easeOutExpo);
        break;
      case iTween.EaseType.easeInOutExpo:
        this.ease = new iTween.EasingFunction(this.easeInOutExpo);
        break;
      case iTween.EaseType.easeInCirc:
        this.ease = new iTween.EasingFunction(this.easeInCirc);
        break;
      case iTween.EaseType.easeOutCirc:
        this.ease = new iTween.EasingFunction(this.easeOutCirc);
        break;
      case iTween.EaseType.easeInOutCirc:
        this.ease = new iTween.EasingFunction(this.easeInOutCirc);
        break;
      case iTween.EaseType.linear:
        this.ease = new iTween.EasingFunction(this.linear);
        break;
      case iTween.EaseType.spring:
        this.ease = new iTween.EasingFunction(this.spring);
        break;
      case iTween.EaseType.easeInBounce:
        this.ease = new iTween.EasingFunction(this.easeInBounce);
        break;
      case iTween.EaseType.easeOutBounce:
        this.ease = new iTween.EasingFunction(this.easeOutBounce);
        break;
      case iTween.EaseType.easeInOutBounce:
        this.ease = new iTween.EasingFunction(this.easeInOutBounce);
        break;
      case iTween.EaseType.easeInBack:
        this.ease = new iTween.EasingFunction(this.easeInBack);
        break;
      case iTween.EaseType.easeOutBack:
        this.ease = new iTween.EasingFunction(this.easeOutBack);
        break;
      case iTween.EaseType.easeInOutBack:
        this.ease = new iTween.EasingFunction(this.easeInOutBack);
        break;
      case iTween.EaseType.easeInElastic:
        this.ease = new iTween.EasingFunction(this.easeInElastic);
        break;
      case iTween.EaseType.easeOutElastic:
        this.ease = new iTween.EasingFunction(this.easeOutElastic);
        break;
      case iTween.EaseType.easeInOutElastic:
        this.ease = new iTween.EasingFunction(this.easeInOutElastic);
        break;
      case iTween.EaseType.easeInSineOutExpo:
        this.ease = new iTween.EasingFunction(this.easeInSineOutExpo);
        break;
      case iTween.EaseType.easeOutElasticLight:
        this.ease = new iTween.EasingFunction(this.easeOutElasticLight);
        break;
    }
  }

  private void UpdatePercentage()
  {
    if (this.useRealTime)
      this.runningTime += Time.realtimeSinceStartup - this.lastRealTime;
    else
      this.runningTime += Time.deltaTime;
    this.percentage = !this.reverse ? this.runningTime / this.time : (float) (1.0 - (double) this.runningTime / (double) this.time);
    this.lastRealTime = Time.realtimeSinceStartup;
  }

  private void CallBack(iTween.CallbackType callbackType)
  {
    string str1 = iTween.CALLBACK_NAMES[(int) callbackType];
    string str2 = iTween.CALLBACK_PARAMS_NAMES[(int) callbackType];
    string str3 = iTween.CALLBACK_TARGET_NAMES[(int) callbackType];
    if (!this.tweenArguments.Contains((object) str1) || this.tweenArguments.Contains((object) "ischild"))
      return;
    GameObject gameObject = !this.tweenArguments.Contains((object) str3) ? this.gameObject : (GameObject) this.tweenArguments[(object) str3];
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
    {
      Debug.LogError((object) string.Format("iTween Error: target is null! callbackType={0} tween={1}", (object) str1, (object) this));
    }
    else
    {
      object tweenArgument = this.tweenArguments[(object) str1];
      if (tweenArgument is Action<object>)
        ((Action<object>) tweenArgument)(this.tweenArguments[(object) str2]);
      else if (tweenArgument is string)
      {
        gameObject.SendMessage((string) tweenArgument, this.tweenArguments[(object) str2], SendMessageOptions.DontRequireReceiver);
      }
      else
      {
        Debug.LogError((object) "iTween Error: Callback method references must be passed as a delegate or string!");
        iTweenManager.Remove(this);
      }
    }
  }

  private void Dispose()
  {
    iTweenManager.Remove(this);
  }

  private void ConflictCheck()
  {
    iTweenIterator iterator = iTweenManager.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      if (!next.destroyed && !((UnityEngine.Object) next.gameObject != (UnityEngine.Object) this.gameObject) && next != this)
      {
        if (next.type == "value" || next.type == "timer")
          break;
        if (next.isRunning && next.type == this.type)
        {
          if (next.method != this.method || next.tweenArguments == null)
            break;
          if (next.tweenArguments.Count != this.tweenArguments.Count)
          {
            next.Dispose();
            next.CallBack(iTween.CallbackType.OnConflict);
            break;
          }
          foreach (DictionaryEntry tweenArgument in this.tweenArguments)
          {
            if (!next.tweenArguments.Contains(tweenArgument.Key))
            {
              next.Dispose();
              next.CallBack(iTween.CallbackType.OnConflict);
              return;
            }
            if (!next.tweenArguments[tweenArgument.Key].Equals(this.tweenArguments[tweenArgument.Key]) && (string) tweenArgument.Key != "id")
            {
              next.Dispose();
              next.CallBack(iTween.CallbackType.OnConflict);
              return;
            }
          }
          this.Dispose();
          this.CallBack(iTween.CallbackType.OnConflict);
        }
      }
    }
  }

  private void EnableKinematic()
  {
  }

  private void DisableKinematic()
  {
  }

  private void ResumeDelay()
  {
    this.waitForDelay = true;
  }

  private float linear(float start, float end, float value)
  {
    return Mathf.Lerp(start, end, value);
  }

  private float clerp(float start, float end, float value)
  {
    float num1 = 0.0f;
    float num2 = 360f;
    float num3 = Mathf.Abs((float) (((double) num2 - (double) num1) / 2.0));
    float num4;
    if ((double) end - (double) start < -(double) num3)
    {
      float num5 = (num2 - start + end) * value;
      num4 = start + num5;
    }
    else if ((double) end - (double) start > (double) num3)
    {
      float num5 = (float) -((double) num2 - (double) end + (double) start) * value;
      num4 = start + num5;
    }
    else
      num4 = start + (end - start) * value;
    return num4;
  }

  private float spring(float start, float end, float value)
  {
    value = Mathf.Clamp01(value);
    value = (float) (((double) Mathf.Sin((float) ((double) value * 3.14159274101257 * (0.200000002980232 + 2.5 * (double) value * (double) value * (double) value))) * (double) Mathf.Pow(1f - value, 2.2f) + (double) value) * (1.0 + 1.20000004768372 * (1.0 - (double) value)));
    return start + (end - start) * value;
  }

  private float easeInQuad(float start, float end, float value)
  {
    end -= start;
    return end * value * value + start;
  }

  private float easeOutQuad(float start, float end, float value)
  {
    end -= start;
    return (float) (-(double) end * (double) value * ((double) value - 2.0)) + start;
  }

  private float easeInOutQuad(float start, float end, float value)
  {
    value /= 0.5f;
    end -= start;
    if ((double) value < 1.0)
      return end / 2f * value * value + start;
    --value;
    return (float) (-(double) end / 2.0 * ((double) value * ((double) value - 2.0) - 1.0)) + start;
  }

  private float easeInCubic(float start, float end, float value)
  {
    end -= start;
    return end * value * value * value + start;
  }

  private float easeOutCubic(float start, float end, float value)
  {
    --value;
    end -= start;
    return end * (float) ((double) value * (double) value * (double) value + 1.0) + start;
  }

  private float easeInOutCubic(float start, float end, float value)
  {
    value /= 0.5f;
    end -= start;
    if ((double) value < 1.0)
      return end / 2f * value * value * value + start;
    value -= 2f;
    return (float) ((double) end / 2.0 * ((double) value * (double) value * (double) value + 2.0)) + start;
  }

  private float easeInQuart(float start, float end, float value)
  {
    end -= start;
    return end * value * value * value * value + start;
  }

  private float easeOutQuart(float start, float end, float value)
  {
    --value;
    end -= start;
    return (float) (-(double) end * ((double) value * (double) value * (double) value * (double) value - 1.0)) + start;
  }

  private float easeInOutQuart(float start, float end, float value)
  {
    value /= 0.5f;
    end -= start;
    if ((double) value < 1.0)
      return end / 2f * value * value * value * value + start;
    value -= 2f;
    return (float) (-(double) end / 2.0 * ((double) value * (double) value * (double) value * (double) value - 2.0)) + start;
  }

  private float easeInQuint(float start, float end, float value)
  {
    end -= start;
    return end * value * value * value * value * value + start;
  }

  private float easeOutQuint(float start, float end, float value)
  {
    --value;
    end -= start;
    return end * (float) ((double) value * (double) value * (double) value * (double) value * (double) value + 1.0) + start;
  }

  private float easeInOutQuint(float start, float end, float value)
  {
    value /= 0.5f;
    end -= start;
    if ((double) value < 1.0)
      return end / 2f * value * value * value * value * value + start;
    value -= 2f;
    return (float) ((double) end / 2.0 * ((double) value * (double) value * (double) value * (double) value * (double) value + 2.0)) + start;
  }

  private float easeInSine(float start, float end, float value)
  {
    end -= start;
    return -end * Mathf.Cos((float) ((double) value / 1.0 * 1.57079637050629)) + end + start;
  }

  private float easeOutSine(float start, float end, float value)
  {
    end -= start;
    return end * Mathf.Sin((float) ((double) value / 1.0 * 1.57079637050629)) + start;
  }

  private float easeInOutSine(float start, float end, float value)
  {
    end -= start;
    return (float) (-(double) end / 2.0 * ((double) Mathf.Cos((float) (3.14159274101257 * (double) value / 1.0)) - 1.0)) + start;
  }

  private float easeInExpo(float start, float end, float value)
  {
    end -= start;
    return end * Mathf.Pow(2f, (float) (10.0 * ((double) value / 1.0 - 1.0))) + start;
  }

  private float easeOutExpo(float start, float end, float value)
  {
    end -= start;
    return end * (float) (-(double) Mathf.Pow(2f, (float) (-10.0 * (double) value / 1.0)) + 1.0) + start;
  }

  private float easeInOutExpo(float start, float end, float value)
  {
    value /= 0.5f;
    end -= start;
    if ((double) value < 1.0)
      return end / 2f * Mathf.Pow(2f, (float) (10.0 * ((double) value - 1.0))) + start;
    --value;
    return (float) ((double) end / 2.0 * (-(double) Mathf.Pow(2f, -10f * value) + 2.0)) + start;
  }

  private float easeInCirc(float start, float end, float value)
  {
    end -= start;
    return (float) (-(double) end * ((double) Mathf.Sqrt((float) (1.0 - (double) value * (double) value)) - 1.0)) + start;
  }

  private float easeOutCirc(float start, float end, float value)
  {
    --value;
    end -= start;
    return end * Mathf.Sqrt((float) (1.0 - (double) value * (double) value)) + start;
  }

  private float easeInOutCirc(float start, float end, float value)
  {
    value /= 0.5f;
    end -= start;
    if ((double) value < 1.0)
      return (float) (-(double) end / 2.0 * ((double) Mathf.Sqrt((float) (1.0 - (double) value * (double) value)) - 1.0)) + start;
    value -= 2f;
    return (float) ((double) end / 2.0 * ((double) Mathf.Sqrt((float) (1.0 - (double) value * (double) value)) + 1.0)) + start;
  }

  private float easeInBounce(float start, float end, float value)
  {
    end -= start;
    float num = 1f;
    return end - this.easeOutBounce(0.0f, end, num - value) + start;
  }

  private float easeOutBounce(float start, float end, float value)
  {
    value /= 1f;
    end -= start;
    if ((double) value < 0.363636374473572)
      return end * (121f / 16f * value * value) + start;
    if ((double) value < 0.727272748947144)
    {
      value -= 0.5454546f;
      return end * (float) (121.0 / 16.0 * (double) value * (double) value + 0.75) + start;
    }
    if ((double) value < 10.0 / 11.0)
    {
      value -= 0.8181818f;
      return end * (float) (121.0 / 16.0 * (double) value * (double) value + 15.0 / 16.0) + start;
    }
    value -= 0.9545454f;
    return end * (float) (121.0 / 16.0 * (double) value * (double) value + 63.0 / 64.0) + start;
  }

  private float easeInOutBounce(float start, float end, float value)
  {
    end -= start;
    float num = 1f;
    if ((double) value < (double) num / 2.0)
      return this.easeInBounce(0.0f, end, value * 2f) * 0.5f + start;
    return (float) ((double) this.easeOutBounce(0.0f, end, value * 2f - num) * 0.5 + (double) end * 0.5) + start;
  }

  private float easeInBack(float start, float end, float value)
  {
    end -= start;
    value /= 1f;
    float num = 1.70158f;
    return (float) ((double) end * (double) value * (double) value * (((double) num + 1.0) * (double) value - (double) num)) + start;
  }

  private float easeOutBack(float start, float end, float value)
  {
    float num = 1.70158f;
    end -= start;
    value = (float) ((double) value / 1.0 - 1.0);
    return end * (float) ((double) value * (double) value * (((double) num + 1.0) * (double) value + (double) num) + 1.0) + start;
  }

  private float easeInOutBack(float start, float end, float value)
  {
    float num1 = 1.70158f;
    end -= start;
    value /= 0.5f;
    if ((double) value < 1.0)
    {
      float num2 = num1 * 1.525f;
      return (float) ((double) end / 2.0 * ((double) value * (double) value * (((double) num2 + 1.0) * (double) value - (double) num2))) + start;
    }
    value -= 2f;
    float num3 = num1 * 1.525f;
    return (float) ((double) end / 2.0 * ((double) value * (double) value * (((double) num3 + 1.0) * (double) value + (double) num3) + 2.0)) + start;
  }

  private float punch(float amplitude, float value)
  {
    if ((double) value == 0.0 || (double) value == 1.0)
      return 0.0f;
    float num1 = 0.3f;
    float num2 = num1 / 6.283185f * Mathf.Asin(0.0f);
    return amplitude * Mathf.Pow(2f, -10f * value) * Mathf.Sin((float) (((double) value * 1.0 - (double) num2) * 6.28318548202515) / num1);
  }

  private float easeInElastic(float start, float end, float value)
  {
    end -= start;
    float num1 = 1f;
    float num2 = num1 * 0.3f;
    float num3 = 0.0f;
    if ((double) value == 0.0)
      return start;
    if ((double) (value /= num1) == 1.0)
      return start + end;
    float num4;
    if ((double) num3 == 0.0 || (double) num3 < (double) Mathf.Abs(end))
    {
      num3 = end;
      num4 = num2 / 4f;
    }
    else
      num4 = num2 / 6.283185f * Mathf.Asin(end / num3);
    return (float) -((double) num3 * (double) Mathf.Pow(2f, 10f * --value) * (double) Mathf.Sin((float) (((double) value * (double) num1 - (double) num4) * 6.28318548202515) / num2)) + start;
  }

  private float easeOutElastic(float start, float end, float value)
  {
    end -= start;
    float num1 = 1f;
    float num2 = num1 * 0.3f;
    float num3 = 0.0f;
    if ((double) value == 0.0)
      return start;
    if ((double) (value /= num1) == 1.0)
      return start + end;
    float num4;
    if ((double) num3 == 0.0 || (double) num3 < (double) Mathf.Abs(end))
    {
      num3 = end;
      num4 = num2 / 4f;
    }
    else
      num4 = num2 / 6.283185f * Mathf.Asin(end / num3);
    return num3 * Mathf.Pow(2f, -10f * value) * Mathf.Sin((float) (((double) value * (double) num1 - (double) num4) * 6.28318548202515) / num2) + end + start;
  }

  private float easeOutElasticLight(float start, float end, float value)
  {
    if ((double) value == 0.0)
      return start;
    if ((double) value == 1.0)
      return end;
    end -= start;
    float num1 = 0.6f;
    float num2 = num1 / 4f;
    return end * Mathf.Pow(2f, -10f * value) * Mathf.Sin((float) (((double) value - (double) num2) * 6.28318548202515) / num1) + end + start;
  }

  private float easeInOutElastic(float start, float end, float value)
  {
    end -= start;
    float num1 = 1f;
    float num2 = num1 * 0.3f;
    float num3 = 0.0f;
    if ((double) value == 0.0)
      return start;
    if ((double) (value /= num1 / 2f) == 2.0)
      return start + end;
    float num4;
    if ((double) num3 == 0.0 || (double) num3 < (double) Mathf.Abs(end))
    {
      num3 = end;
      num4 = num2 / 4f;
    }
    else
      num4 = num2 / 6.283185f * Mathf.Asin(end / num3);
    if ((double) value < 1.0)
      return (float) (-0.5 * ((double) num3 * (double) Mathf.Pow(2f, 10f * --value) * (double) Mathf.Sin((float) (((double) value * (double) num1 - (double) num4) * 6.28318548202515) / num2))) + start;
    return (float) ((double) num3 * (double) Mathf.Pow(2f, -10f * --value) * (double) Mathf.Sin((float) (((double) value * (double) num1 - (double) num4) * 6.28318548202515) / num2) * 0.5) + end + start;
  }

  private float easeInSineOutExpo(float start, float end, float value)
  {
    float num = value;
    if ((double) num > (double) start / 2.0)
      return this.easeOutExpo(start, end, num);
    return this.easeInSine(start, end, num);
  }

  private float easeInExpoFirstHalf(float start, float end, float value)
  {
    end -= start;
    return end * Mathf.Pow(2f, (float) (10.0 * ((double) value / 1.0 - 1.0))) + start;
  }

  private float easeInExpoSecondHalf(float start, float end, float value)
  {
    end -= start;
    return end * Mathf.Pow(2f, (float) (10.0 * ((double) value / 1.0 - 1.0))) + start;
  }

  public override string ToString()
  {
    string name = this.gameObject.name;
    if (this.tweenArguments == null)
      return string.Format("[iTween - gameObject={0}", (object) name);
    object tweenArgument1 = this.tweenArguments[(object) "type"];
    object tweenArgument2 = this.tweenArguments[(object) "method"];
    object tweenArgument3 = this.tweenArguments[(object) "name"];
    object tweenArgument4 = this.tweenArguments[(object) "id"];
    return string.Format("[iTween - gameObject={0} type={1} method={2} name={3} id={4}]", (object) name, tweenArgument1, tweenArgument2, tweenArgument3, tweenArgument4);
  }

  public enum EaseType
  {
    easeInQuad,
    easeOutQuad,
    easeInOutQuad,
    easeInCubic,
    easeOutCubic,
    easeInOutCubic,
    easeInQuart,
    easeOutQuart,
    easeInOutQuart,
    easeInQuint,
    easeOutQuint,
    easeInOutQuint,
    easeInSine,
    easeOutSine,
    easeInOutSine,
    easeInExpo,
    easeOutExpo,
    easeInOutExpo,
    easeInCirc,
    easeOutCirc,
    easeInOutCirc,
    linear,
    spring,
    easeInBounce,
    easeOutBounce,
    easeInOutBounce,
    easeInBack,
    easeOutBack,
    easeInOutBack,
    easeInElastic,
    easeOutElastic,
    easeInOutElastic,
    punch,
    easeInSineOutExpo,
    easeOutElasticLight,
  }

  private enum CallbackType
  {
    OnStart,
    OnUpdate,
    OnComplete,
    OnConflict,
  }

  public enum LoopType
  {
    none,
    loop,
    pingPong,
  }

  public enum NamedValueColor
  {
    _Color,
    _SpecColor,
    _Emission,
    _ReflectColor,
  }

  public static class Defaults
  {
    public static float time = 1f;
    public static float delay = 0.0f;
    public static iTween.NamedValueColor namedColorValue = iTween.NamedValueColor._Color;
    public static iTween.LoopType loopType = iTween.LoopType.none;
    public static iTween.EaseType easeType = iTween.EaseType.easeOutExpo;
    public static float lookSpeed = 3f;
    public static bool isLocal = false;
    public static Space space = Space.Self;
    public static bool orientToPath = false;
    public static Color color = Color.white;
    public static float updateTimePercentage = 0.05f;
    public static float updateTime = 1f * iTween.Defaults.updateTimePercentage;
    public static int cameraFadeDepth = 999999;
    public static float lookAhead = 0.05f;
    public static bool useRealTime = false;
    public static Vector3 up = Vector3.up;
  }

  private class CRSpline
  {
    public Vector3[] pts;

    public CRSpline(params Vector3[] pts)
    {
      this.pts = new Vector3[pts.Length];
      Array.Copy((Array) pts, (Array) this.pts, pts.Length);
    }

    public Vector3 Interp(float t)
    {
      int num1 = this.pts.Length - 3;
      int index = Mathf.Min(Mathf.FloorToInt(t * (float) num1), num1 - 1);
      float num2 = t * (float) num1 - (float) index;
      Vector3 pt1 = this.pts[index];
      Vector3 pt2 = this.pts[index + 1];
      Vector3 pt3 = this.pts[index + 2];
      Vector3 pt4 = this.pts[index + 3];
      return 0.5f * ((-pt1 + 3f * pt2 - 3f * pt3 + pt4) * (num2 * num2 * num2) + (2f * pt1 - 5f * pt2 + 4f * pt3 - pt4) * (num2 * num2) + (-pt1 + pt3) * num2 + 2f * pt2);
    }
  }

  private delegate float EasingFunction(float start, float end, float value);

  private delegate void ApplyTween();
}
