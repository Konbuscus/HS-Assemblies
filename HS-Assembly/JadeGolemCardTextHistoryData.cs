﻿// Decompiled with JetBrains decompiler
// Type: JadeGolemCardTextHistoryData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class JadeGolemCardTextHistoryData : CardTextHistoryData
{
  public int m_jadeGolem;
  public bool m_wasInPlay;

  public override void SetHistoryData(Entity entity, HistoryInfo historyInfo)
  {
    base.SetHistoryData(entity, historyInfo);
    this.m_jadeGolem = entity.GetJadeGolem();
    if (entity.GetZone() != TAG_ZONE.PLAY || historyInfo == null || (historyInfo.m_infoType == HistoryInfoType.CARD_PLAYED || historyInfo.m_infoType == HistoryInfoType.WEAPON_PLAYED))
      return;
    this.m_wasInPlay = true;
  }
}
