﻿// Decompiled with JetBrains decompiler
// Type: SoundManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
  private List<AudioSource> m_generatedSources = new List<AudioSource>();
  private List<SoundManager.ExtensionMapping> m_extensionMappings = new List<SoundManager.ExtensionMapping>();
  private Map<SoundCategory, List<AudioSource>> m_sourcesByCategory = new Map<SoundCategory, List<AudioSource>>();
  private Map<string, List<AudioSource>> m_sourcesByClipName = new Map<string, List<AudioSource>>();
  private Map<string, SoundManager.BundleInfo> m_bundleInfos = new Map<string, SoundManager.BundleInfo>();
  private Map<SoundCategory, List<SoundManager.DuckState>> m_duckStates = new Map<SoundCategory, List<SoundManager.DuckState>>();
  private List<AudioSource> m_inactiveSources = new List<AudioSource>();
  private List<MusicTrack> m_musicTracks = new List<MusicTrack>();
  private List<MusicTrack> m_ambienceTracks = new List<MusicTrack>();
  private List<AudioSource> m_fadingTracks = new List<AudioSource>();
  private int m_nextSourceId = 1;
  private List<Coroutine> m_fadingTracksIn = new List<Coroutine>();
  private static SoundManager s_instance;
  private SoundConfig m_config;
  private uint m_nextDuckStateTweenId;
  private bool m_musicIsAboutToPlay;
  private bool m_ambienceIsAboutToPlay;
  private AudioSource m_currentMusicTrack;
  private AudioSource m_currentAmbienceTrack;
  private int m_musicTrackIndex;
  private int m_ambienceTrackIndex;
  private bool m_mute;
  private uint m_frame;

  private void Awake()
  {
    SoundManager.s_instance = this;
    this.InitializeOptions();
  }

  private void OnDestroy()
  {
    SoundManager.s_instance = (SoundManager) null;
  }

  private void Start()
  {
    this.UpdateAppMute();
    if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
      ApplicationMgr.Get().AddFocusChangedListener(new ApplicationMgr.FocusChangedCallback(this.OnAppFocusChanged));
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
      this.m_config = new GameObject("SoundConfig").AddComponent<SoundConfig>();
    else
      SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
  }

  private void Update()
  {
    this.m_frame = (uint) ((int) this.m_frame + 1 & -1);
    this.UpdateMusicAndSources();
  }

  public static SoundManager Get()
  {
    return SoundManager.s_instance;
  }

  public SoundConfig GetConfig()
  {
    return this.m_config;
  }

  public void SetConfig(SoundConfig config)
  {
    this.m_config = config;
  }

  public bool IsInitialized()
  {
    return (UnityEngine.Object) this.m_config != (UnityEngine.Object) null;
  }

  public GameObject GetPlaceholderSound()
  {
    AudioSource placeholderSource = this.GetPlaceholderSource();
    if ((UnityEngine.Object) placeholderSource == (UnityEngine.Object) null)
      return (GameObject) null;
    return placeholderSource.gameObject;
  }

  public AudioSource GetPlaceholderSource()
  {
    if ((UnityEngine.Object) this.m_config == (UnityEngine.Object) null)
      return (AudioSource) null;
    if (ApplicationMgr.IsInternal())
      return this.m_config.m_PlaceholderSound;
    return (AudioSource) null;
  }

  public bool Play(AudioSource source, bool localizeAudio = true)
  {
    return (bool) ((UnityEngine.Object) this.PlayImpl(source, (AudioClip) null, localizeAudio));
  }

  public bool PlayOneShot(AudioSource source, AudioClip clip, float volume = 1f, bool localizeAudio = true)
  {
    if (!(bool) ((UnityEngine.Object) this.PlayImpl(source, clip, localizeAudio)))
      return false;
    if (this.IsActive(source))
      this.SetVolume(source, volume);
    return true;
  }

  public bool IsPlaying(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return false;
    return source.isPlaying;
  }

  public bool Pause(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null || this.IsPaused(source))
      return false;
    SoundManager.SourceExtension ext = this.RegisterExtension(source, false, (AudioClip) null, true);
    if (ext == null)
      return false;
    ext.m_paused = true;
    this.UpdateSource(source, ext);
    source.Pause();
    return true;
  }

  public bool IsPaused(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return false;
    SoundManager.SourceExtension extension = this.GetExtension(source);
    if (extension == null)
      return false;
    return extension.m_paused;
  }

  public bool Stop(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null || !this.IsActive(source))
      return false;
    source.Stop();
    this.FinishSource(source);
    return true;
  }

  public void Destroy(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return;
    this.FinishSource(source);
  }

  public bool IsActive(AudioSource source)
  {
    return !((UnityEngine.Object) source == (UnityEngine.Object) null) && (this.IsPlaying(source) || this.IsPaused(source));
  }

  public bool IsPlaybackFinished(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null || (UnityEngine.Object) source.clip == (UnityEngine.Object) null)
      return false;
    return source.timeSamples >= source.clip.samples;
  }

  public float GetVolume(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return 1f;
    SoundManager.SourceExtension sourceExtension = this.RegisterExtension(source, false, (AudioClip) null, true);
    if (sourceExtension == null)
      return 1f;
    return sourceExtension.m_codeVolume;
  }

  public void SetVolume(AudioSource source, float volume)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return;
    SoundManager.SourceExtension ext = this.RegisterExtension(source, false, (AudioClip) null, true);
    if (ext == null)
      return;
    ext.m_codeVolume = volume;
    this.UpdateVolume(source, ext);
  }

  public float GetPitch(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return 1f;
    SoundManager.SourceExtension sourceExtension = this.RegisterExtension(source, false, (AudioClip) null, true);
    if (sourceExtension == null)
      return 1f;
    return sourceExtension.m_codePitch;
  }

  public void SetPitch(AudioSource source, float pitch)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return;
    SoundManager.SourceExtension ext = this.RegisterExtension(source, false, (AudioClip) null, true);
    if (ext == null)
      return;
    ext.m_codePitch = pitch;
    this.UpdatePitch(source, ext);
  }

  public SoundCategory GetCategory(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return SoundCategory.NONE;
    return this.GetDefFromSource(source).m_Category;
  }

  public void SetCategory(AudioSource source, SoundCategory cat)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return;
    SoundDef soundDef = source.GetComponent<SoundDef>();
    if ((UnityEngine.Object) soundDef != (UnityEngine.Object) null)
    {
      if (soundDef.m_Category == cat)
        return;
    }
    else
      soundDef = source.gameObject.AddComponent<SoundDef>();
    soundDef.m_Category = cat;
    this.UpdateSource(source);
  }

  public bool Is3d(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return false;
    return (double) source.spatialBlend >= 1.0;
  }

  public void Set3d(AudioSource source, bool enable)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return;
    source.spatialBlend = !enable ? 0.0f : 1f;
  }

  public AudioSource GetCurrentMusicTrack()
  {
    return this.m_currentMusicTrack;
  }

  public AudioSource GetCurrentAmbienceTrack()
  {
    return this.m_currentAmbienceTrack;
  }

  public bool Load(string soundName)
  {
    return AssetLoader.Get().LoadSound(soundName, new AssetLoader.GameObjectCallback(this.OnLoadSoundLoaded), (object) null, true, (GameObject) null);
  }

  public void LoadAndPlay(string soundName)
  {
    this.LoadAndPlay(soundName, (GameObject) null, 1f, (SoundManager.LoadedCallback) null, (object) null);
  }

  public void LoadAndPlay(string soundName, float volume)
  {
    this.LoadAndPlay(soundName, (GameObject) null, volume, (SoundManager.LoadedCallback) null, (object) null);
  }

  public void LoadAndPlay(string soundName, GameObject parent)
  {
    this.LoadAndPlay(soundName, parent, 1f, (SoundManager.LoadedCallback) null, (object) null);
  }

  public void LoadAndPlay(string soundName, GameObject parent, float volume)
  {
    this.LoadAndPlay(soundName, parent, volume, (SoundManager.LoadedCallback) null, (object) null);
  }

  public void LoadAndPlay(string soundName, GameObject parent, float volume, SoundManager.LoadedCallback callback)
  {
    this.LoadAndPlay(soundName, parent, volume, callback, (object) null);
  }

  public void LoadAndPlay(string soundName, GameObject parent, float volume, SoundManager.LoadedCallback callback, object userData)
  {
    SoundManager.SoundLoadContext soundLoadContext = new SoundManager.SoundLoadContext();
    soundLoadContext.Init(parent, volume, callback, userData);
    AssetLoader.Get().LoadSound(soundName, new AssetLoader.GameObjectCallback(this.OnLoadAndPlaySoundLoaded), (object) soundLoadContext, true, this.GetPlaceholderSound());
  }

  public void LoadAndPlayTemplate(AudioSource template, AudioClip clip)
  {
    this.LoadAndPlayTemplate(template, clip, 1f, (SoundManager.LoadedCallback) null, (object) null);
  }

  public void LoadAndPlayTemplate(AudioSource template, AudioClip clip, float volume)
  {
    this.LoadAndPlayTemplate(template, clip, volume, (SoundManager.LoadedCallback) null, (object) null);
  }

  public void LoadAndPlayTemplate(AudioSource template, AudioClip clip, float volume, SoundManager.LoadedCallback callback)
  {
    this.LoadAndPlayTemplate(template, clip, volume, callback, (object) null);
  }

  public void LoadAndPlayTemplate(AudioSource template, AudioClip clip, float volume, SoundManager.LoadedCallback callback, object userData)
  {
    if ((UnityEngine.Object) template == (UnityEngine.Object) null)
      Error.AddDevFatal("SoundManager.LoadAndPlayTemplate() - template is null");
    else if ((UnityEngine.Object) clip == (UnityEngine.Object) null)
    {
      Error.AddDevFatal("SoundManager.LoadAndPlayTemplate() - Attempted to play template {0} with a null clip. Top-level parent is {1}.", (object) template, (object) SceneUtils.FindTopParent((Component) template));
    }
    else
    {
      SoundManager.SoundLoadContext soundLoadContext = new SoundManager.SoundLoadContext();
      soundLoadContext.m_template = template;
      soundLoadContext.Init(template.gameObject, volume, callback, userData);
      AssetLoader.Get().LoadSound(clip.name, new AssetLoader.GameObjectCallback(this.OnLoadAndPlaySoundLoaded), (object) soundLoadContext, true, this.GetPlaceholderSound());
    }
  }

  public void PlayPreloaded(AudioSource source)
  {
    this.PlayPreloaded(source, (GameObject) null);
  }

  public void PlayPreloaded(AudioSource source, float volume)
  {
    this.PlayPreloaded(source, (GameObject) null, volume);
  }

  public void PlayPreloaded(AudioSource source, GameObject parentObject)
  {
    this.PlayPreloaded(source, parentObject, 1f);
  }

  public void PlayPreloaded(AudioSource source, GameObject parentObject, float volume)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Preloaded audio source is null! Cannot play!");
    }
    else
    {
      this.RegisterExtension(source, false, (AudioClip) null, true).m_codeVolume = volume;
      this.InitSourceTransform(source, parentObject);
      this.m_generatedSources.Add(source);
      this.Play(source, true);
    }
  }

  public AudioSource PlayClip(SoundPlayClipArgs args)
  {
    if (args == null || (UnityEngine.Object) args.m_clip == (UnityEngine.Object) null)
      return this.PlayImpl((AudioSource) null, (AudioClip) null, true);
    AudioSource audioSource = this.GenerateAudioSource(args.m_templateSource, args.m_clip);
    audioSource.clip = args.m_clip;
    if (args.m_volume.HasValue)
      audioSource.volume = args.m_volume.Value;
    if (args.m_pitch.HasValue)
      audioSource.pitch = args.m_pitch.Value;
    if (args.m_spatialBlend.HasValue)
      audioSource.spatialBlend = args.m_spatialBlend.Value;
    if (args.m_category.HasValue)
      audioSource.GetComponent<SoundDef>().m_Category = args.m_category.Value;
    bool localizeAudio = true;
    if (args.m_localizeAudio.HasValue)
      localizeAudio = args.m_localizeAudio.Value;
    this.InitSourceTransform(audioSource, args.m_parentObject);
    if (this.Play(audioSource, localizeAudio))
      return audioSource;
    this.FinishGeneratedSource(audioSource);
    return (AudioSource) null;
  }

  private void OnLoadSoundLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SoundManager.OnLoadSoundLoaded() - ERROR \"{0}\" failed to load", (object) name));
    }
    else
    {
      AudioSource component = go.GetComponent<AudioSource>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
        UnityEngine.Debug.LogError((object) string.Format("SoundManager.OnLoadSoundLoaded() - ERROR \"{0}\" has no AudioSource", (object) name));
      }
      else
      {
        this.RegisterSourceBundle(name, component);
        component.volume = 0.0f;
        component.Play();
        component.Stop();
        this.UnregisterSourceBundle(name, component);
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) component.gameObject);
      }
    }
  }

  private void OnLoadAndPlaySoundLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SoundManager.OnLoadAndPlaySoundLoaded() - ERROR \"{0}\" failed to load", (object) name));
    }
    else
    {
      AudioSource component = go.GetComponent<AudioSource>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
        UnityEngine.Debug.LogError((object) string.Format("SoundManager.OnLoadAndPlaySoundLoaded() - ERROR \"{0}\" has no AudioSource", (object) name));
      }
      else
      {
        SoundManager.SoundLoadContext soundLoadContext = (SoundManager.SoundLoadContext) callbackData;
        if (soundLoadContext.m_sceneMode != SceneMgr.Mode.FATAL_ERROR && SceneMgr.Get().IsModeRequested(SceneMgr.Mode.FATAL_ERROR))
        {
          UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
        }
        else
        {
          this.RegisterSourceBundle(name, component);
          if (soundLoadContext.m_haveCallback && !GeneralUtils.IsCallbackValid((Delegate) soundLoadContext.m_callback))
          {
            UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
            this.UnregisterSourceBundle(name, component);
          }
          else
          {
            this.m_generatedSources.Add(component);
            if ((UnityEngine.Object) soundLoadContext.m_template != (UnityEngine.Object) null)
              SoundUtils.CopyAudioSource(soundLoadContext.m_template, component);
            this.RegisterExtension(component, false, (AudioClip) null, true).m_codeVolume = soundLoadContext.m_volume;
            this.InitSourceTransform(component, soundLoadContext.m_parent);
            this.Play(component, true);
            if (soundLoadContext.m_callback == null)
              return;
            soundLoadContext.m_callback(component, soundLoadContext.m_userData);
          }
        }
      }
    }
  }

  public void AddMusicTracks(List<MusicTrack> tracks)
  {
    this.AddTracks(tracks, this.m_musicTracks);
  }

  public void AddAmbienceTracks(List<MusicTrack> tracks)
  {
    this.AddTracks(tracks, this.m_ambienceTracks);
  }

  public List<MusicTrack> GetCurrentMusicTracks()
  {
    return this.m_musicTracks;
  }

  public List<MusicTrack> GetCurrentAmbienceTracks()
  {
    return this.m_ambienceTracks;
  }

  public void StopCurrentMusicTrack()
  {
    if (!((UnityEngine.Object) this.m_currentMusicTrack != (UnityEngine.Object) null))
      return;
    this.FadeTrackOut(this.m_currentMusicTrack);
    this.ChangeCurrentMusicTrack((AudioSource) null);
  }

  public void StopCurrentAmbienceTrack()
  {
    if (!((UnityEngine.Object) this.m_currentAmbienceTrack != (UnityEngine.Object) null))
      return;
    this.FadeTrackOut(this.m_currentAmbienceTrack);
    this.ChangeCurrentAmbienceTrack((AudioSource) null);
  }

  public void NukeMusicAndAmbiencePlaylists()
  {
    this.m_musicTracks.Clear();
    this.m_ambienceTracks.Clear();
    this.m_musicTrackIndex = 0;
    this.m_ambienceTrackIndex = 0;
  }

  public void NukePlaylistsAndStopPlayingCurrentTracks()
  {
    this.NukeMusicAndAmbiencePlaylists();
    this.StopCurrentMusicTrack();
    this.StopCurrentAmbienceTrack();
  }

  public void NukeMusicAndStopPlayingCurrentTrack()
  {
    this.m_musicTracks.Clear();
    this.m_musicTrackIndex = 0;
    this.StopCurrentMusicTrack();
  }

  public void NukeAmbienceAndStopPlayingCurrentTrack()
  {
    this.m_ambienceTracks.Clear();
    this.m_ambienceTrackIndex = 0;
    this.StopCurrentAmbienceTrack();
  }

  public void ImmediatelyKillMusicAndAmbience()
  {
    this.NukeMusicAndAmbiencePlaylists();
    foreach (AudioSource source in this.m_fadingTracks.ToArray())
      this.FinishSource(source);
    if ((UnityEngine.Object) this.m_currentMusicTrack != (UnityEngine.Object) null)
    {
      this.FinishSource(this.m_currentMusicTrack);
      this.ChangeCurrentMusicTrack((AudioSource) null);
    }
    if (!((UnityEngine.Object) this.m_currentAmbienceTrack != (UnityEngine.Object) null))
      return;
    this.FinishSource(this.m_currentAmbienceTrack);
    this.ChangeCurrentAmbienceTrack((AudioSource) null);
  }

  private void AddTracks(List<MusicTrack> sourceTracks, List<MusicTrack> destTracks)
  {
    using (List<MusicTrack>.Enumerator enumerator = sourceTracks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MusicTrack current = enumerator.Current;
        destTracks.Add(current);
      }
    }
  }

  private void OnMusicLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SoundManager.OnMusicLoaded() - ERROR \"{0}\" failed to load", (object) name));
    }
    else
    {
      AudioSource component = go.GetComponent<AudioSource>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("SoundManager.OnMusicLoaded() - ERROR \"{0}\" has no AudioSource", (object) name));
      }
      else
      {
        this.RegisterSourceBundle(name, component);
        MusicTrack musicTrack = (MusicTrack) callbackData;
        if (!this.m_musicTracks.Contains(musicTrack))
        {
          this.UnregisterSourceBundle(name, component);
          UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
        }
        else
        {
          this.m_generatedSources.Add(component);
          component.transform.parent = this.transform;
          component.volume *= musicTrack.m_volume;
          this.ChangeCurrentMusicTrack(component);
          this.Play(component, true);
        }
        this.m_musicIsAboutToPlay = false;
      }
    }
  }

  private void OnAmbienceLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SoundManager.OnAmbienceLoaded() - ERROR \"{0}\" failed to load", (object) name));
    }
    else
    {
      AudioSource component = go.GetComponent<AudioSource>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("SoundManager.OnAmbienceLoaded() - ERROR \"{0}\" has no AudioSource", (object) name));
      }
      else
      {
        this.RegisterSourceBundle(name, component);
        MusicTrack musicTrack = (MusicTrack) callbackData;
        if (!this.m_ambienceTracks.Contains(musicTrack))
        {
          this.UnregisterSourceBundle(name, component);
          UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
        }
        else
        {
          this.m_generatedSources.Add(component);
          component.transform.parent = this.transform;
          component.volume *= musicTrack.m_volume;
          this.ChangeCurrentAmbienceTrack(component);
          this.m_fadingTracksIn.Add(this.StartCoroutine(this.FadeTrackIn(component)));
          this.Play(component, true);
        }
        this.m_ambienceIsAboutToPlay = false;
      }
    }
  }

  private void ChangeCurrentMusicTrack(AudioSource source)
  {
    this.m_currentMusicTrack = source;
  }

  private void ChangeCurrentAmbienceTrack(AudioSource source)
  {
    this.m_currentAmbienceTrack = source;
  }

  private void UpdateMusicAndAmbience()
  {
    if (!SoundUtils.IsMusicEnabled())
      return;
    if (!this.m_musicIsAboutToPlay)
    {
      if ((UnityEngine.Object) this.m_currentMusicTrack != (UnityEngine.Object) null)
      {
        if (!this.IsPlaying(this.m_currentMusicTrack))
          this.StartCoroutine(this.PlayMusicInSeconds(this.m_config.m_SecondsBetweenMusicTracks));
      }
      else
        this.m_musicIsAboutToPlay = this.PlayNextMusic();
    }
    if (this.m_ambienceIsAboutToPlay)
      return;
    if ((UnityEngine.Object) this.m_currentAmbienceTrack != (UnityEngine.Object) null)
    {
      if (this.IsPlaying(this.m_currentAmbienceTrack))
        return;
      this.StartCoroutine(this.PlayAmbienceInSeconds(0.0f));
    }
    else
      this.m_ambienceIsAboutToPlay = this.PlayNextAmbience();
  }

  [DebuggerHidden]
  private IEnumerator PlayMusicInSeconds(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SoundManager.\u003CPlayMusicInSeconds\u003Ec__Iterator317() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds, \u003C\u003Ef__this = this };
  }

  private bool PlayNextMusic()
  {
    if (!SoundUtils.IsMusicEnabled() || this.m_musicTracks.Count <= 0)
      return false;
    MusicTrack musicTrack = this.m_musicTracks[this.m_musicTrackIndex];
    this.m_musicTrackIndex = (this.m_musicTrackIndex + 1) % this.m_musicTracks.Count;
    if (musicTrack == null)
      return false;
    if ((UnityEngine.Object) this.m_currentMusicTrack != (UnityEngine.Object) null)
    {
      this.FadeTrackOut(this.m_currentMusicTrack);
      this.ChangeCurrentMusicTrack((AudioSource) null);
    }
    return AssetLoader.Get().LoadSound(FileUtils.GameAssetPathToName(musicTrack.m_name), new AssetLoader.GameObjectCallback(this.OnMusicLoaded), (object) musicTrack, true, this.GetPlaceholderSound());
  }

  [DebuggerHidden]
  private IEnumerator PlayAmbienceInSeconds(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SoundManager.\u003CPlayAmbienceInSeconds\u003Ec__Iterator318() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds, \u003C\u003Ef__this = this };
  }

  private bool PlayNextAmbience()
  {
    if (!SoundUtils.IsMusicEnabled() || this.m_ambienceTracks.Count <= 0)
      return false;
    MusicTrack ambienceTrack = this.m_ambienceTracks[this.m_ambienceTrackIndex];
    this.m_ambienceTrackIndex = (this.m_ambienceTrackIndex + 1) % this.m_ambienceTracks.Count;
    if (ambienceTrack == null)
      return false;
    string name = FileUtils.GameAssetPathToName(ambienceTrack.m_name);
    using (List<Coroutine>.Enumerator enumerator = this.m_fadingTracksIn.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Coroutine current = enumerator.Current;
        if (current != null)
          this.StopCoroutine(current);
      }
    }
    this.m_fadingTracksIn.Clear();
    return AssetLoader.Get().LoadSound(name, new AssetLoader.GameObjectCallback(this.OnAmbienceLoaded), (object) ambienceTrack, true, this.GetPlaceholderSound());
  }

  private void FadeTrackOut(AudioSource source)
  {
    if (!this.IsActive(source))
      this.FinishSource(source);
    else
      this.StartCoroutine(this.FadeTrack(source, 0.0f));
  }

  [DebuggerHidden]
  private IEnumerator FadeTrackIn(AudioSource source)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SoundManager.\u003CFadeTrackIn\u003Ec__Iterator319() { source = source, \u003C\u0024\u003Esource = source, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator FadeTrack(AudioSource source, float targetVolume)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SoundManager.\u003CFadeTrack\u003Ec__Iterator31A() { source = source, targetVolume = targetVolume, \u003C\u0024\u003Esource = source, \u003C\u0024\u003EtargetVolume = targetVolume, \u003C\u003Ef__this = this };
  }

  private SoundManager.SourceExtension RegisterExtension(AudioSource source, bool aboutToPlay = false, AudioClip oneShotClip = null, bool localizeAudio = true)
  {
    SoundDef defFromSource = this.GetDefFromSource(source);
    SoundManager.SourceExtension sourceExtension = this.GetExtension(source);
    if (sourceExtension == null)
    {
      AudioClip clipForPlayback = this.DetermineClipForPlayback(source, defFromSource, oneShotClip, localizeAudio);
      if ((UnityEngine.Object) clipForPlayback == (UnityEngine.Object) null)
        return (SoundManager.SourceExtension) null;
      if (aboutToPlay && this.ProcessClipLimits(clipForPlayback))
        return (SoundManager.SourceExtension) null;
      sourceExtension = new SoundManager.SourceExtension();
      sourceExtension.m_sourceVolume = source.volume;
      sourceExtension.m_sourcePitch = source.pitch;
      sourceExtension.m_sourceClip = source.clip;
      sourceExtension.m_id = this.GetNextSourceId();
      this.AddExtensionMapping(source, sourceExtension);
      this.RegisterSourceByCategory(source, defFromSource.m_Category);
      this.InitNewClipOnSource(source, defFromSource, sourceExtension, clipForPlayback);
    }
    else if (aboutToPlay)
    {
      AudioClip clipForPlayback = this.DetermineClipForPlayback(source, defFromSource, oneShotClip, localizeAudio);
      if (!this.CanPlayClipOnExistingSource(source, clipForPlayback))
      {
        if (this.IsActive(source))
          this.Stop(source);
        else
          this.FinishSource(source);
        return (SoundManager.SourceExtension) null;
      }
      if ((UnityEngine.Object) source.clip != (UnityEngine.Object) clipForPlayback)
      {
        if ((UnityEngine.Object) source.clip != (UnityEngine.Object) null)
          this.UnregisterSourceByClip(source);
        this.InitNewClipOnSource(source, defFromSource, sourceExtension, clipForPlayback);
      }
    }
    return sourceExtension;
  }

  private AudioClip DetermineClipForPlayback(AudioSource source, SoundDef def, AudioClip oneShotClip, bool localizeAudio)
  {
    AudioClip audioClip1 = oneShotClip;
    if ((UnityEngine.Object) audioClip1 == (UnityEngine.Object) null)
    {
      audioClip1 = SoundUtils.GetRandomClipFromDef(def);
      if ((UnityEngine.Object) audioClip1 == (UnityEngine.Object) null)
      {
        audioClip1 = source.clip;
        if ((UnityEngine.Object) audioClip1 == (UnityEngine.Object) null)
        {
          string str = string.Empty;
          if (ApplicationMgr.IsInternal())
            str = " " + DebugUtils.GetHierarchyPathAndType((UnityEngine.Object) source, '.');
          Error.AddDevFatal("{0} has no AudioClip. Top-level parent is {1}{2}.", (object) source, (object) SceneUtils.FindTopParent((Component) source), (object) str);
          return (AudioClip) null;
        }
      }
    }
    if (localizeAudio)
    {
      AudioClip audioClip2 = AssetLoader.Get().LoadAudioClip(audioClip1.name, false) as AudioClip;
      if ((UnityEngine.Object) audioClip2 != (UnityEngine.Object) null)
      {
        audioClip1 = audioClip2;
      }
      else
      {
        source.volume = 0.0f;
        if ((UnityEngine.Object) source.gameObject != (UnityEngine.Object) null && (UnityEngine.Object) audioClip1 != (UnityEngine.Object) null)
          UnityEngine.Debug.LogErrorFormat("DetermineClipForPlayback: failed to load localized audio gameObject={0} clip={1}", new object[2]
          {
            (object) source.gameObject.name,
            (object) audioClip1.name
          });
        else if ((UnityEngine.Object) audioClip1 != (UnityEngine.Object) null)
          UnityEngine.Debug.LogErrorFormat("DetermineClipForPlayback: failed to load localized audio clip={0}", (object) audioClip1.name);
      }
    }
    return audioClip1;
  }

  private bool CanPlayClipOnExistingSource(AudioSource source, AudioClip clip)
  {
    return !((UnityEngine.Object) clip == (UnityEngine.Object) null) && (this.IsActive(source) && !((UnityEngine.Object) source.clip != (UnityEngine.Object) clip) || !this.ProcessClipLimits(clip));
  }

  private void InitNewClipOnSource(AudioSource source, SoundDef def, SoundManager.SourceExtension ext, AudioClip clip)
  {
    ext.m_defVolume = SoundUtils.GetRandomVolumeFromDef(def);
    ext.m_defPitch = SoundUtils.GetRandomPitchFromDef(def);
    source.clip = clip;
    this.RegisterSourceByClip(source, clip);
  }

  private void UnregisterExtension(AudioSource source, SoundManager.SourceExtension ext)
  {
    source.volume = ext.m_sourceVolume;
    source.pitch = ext.m_sourcePitch;
    source.clip = ext.m_sourceClip;
    this.RemoveExtensionMapping(source);
  }

  private void UpdateSource(AudioSource source)
  {
    SoundManager.SourceExtension extension = this.GetExtension(source);
    this.UpdateSource(source, extension);
  }

  private void UpdateSource(AudioSource source, SoundManager.SourceExtension ext)
  {
    this.UpdateMute(source);
    this.UpdateVolume(source, ext);
    this.UpdatePitch(source, ext);
  }

  private void UpdateMute(AudioSource source)
  {
    bool categoryEnabled = this.IsCategoryEnabled(source);
    this.UpdateMute(source, categoryEnabled);
  }

  private void UpdateMute(AudioSource source, bool categoryEnabled)
  {
    source.mute = this.m_mute || !categoryEnabled;
  }

  private void UpdateCategoryMute(SoundCategory cat)
  {
    List<AudioSource> audioSourceList;
    if (!this.m_sourcesByCategory.TryGetValue(cat, out audioSourceList))
      return;
    bool categoryEnabled = SoundUtils.IsCategoryEnabled(cat);
    for (int index = 0; index < audioSourceList.Count; ++index)
      this.UpdateMute(audioSourceList[index], categoryEnabled);
  }

  private void UpdateAllMutes()
  {
    using (List<SoundManager.ExtensionMapping>.Enumerator enumerator = this.m_extensionMappings.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.UpdateMute(enumerator.Current.Source);
    }
  }

  private void UpdateVolume(AudioSource source, SoundManager.SourceExtension ext)
  {
    float categoryVolume = this.GetCategoryVolume(source);
    float duckingVolume = this.GetDuckingVolume(source);
    this.UpdateVolume(source, ext, categoryVolume, duckingVolume);
  }

  private void UpdateVolume(AudioSource source, SoundManager.SourceExtension ext, float categoryVolume, float duckingVolume)
  {
    source.volume = ext.m_codeVolume * ext.m_sourceVolume * ext.m_defVolume * categoryVolume * duckingVolume;
  }

  private void UpdateCategoryVolume(SoundCategory cat)
  {
    List<AudioSource> audioSourceList;
    if (!this.m_sourcesByCategory.TryGetValue(cat, out audioSourceList))
      return;
    float categoryVolume = SoundUtils.GetCategoryVolume(cat);
    for (int index = 0; index < audioSourceList.Count; ++index)
    {
      AudioSource source = audioSourceList[index];
      if (!((UnityEngine.Object) source == (UnityEngine.Object) null))
      {
        SoundManager.SourceExtension extension = this.GetExtension(source);
        float duckingVolume = this.GetDuckingVolume(source);
        this.UpdateVolume(source, extension, categoryVolume, duckingVolume);
      }
    }
  }

  private void UpdateAllCategoryVolumes()
  {
    using (Map<SoundCategory, List<AudioSource>>.KeyCollection.Enumerator enumerator = this.m_sourcesByCategory.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.UpdateCategoryVolume(enumerator.Current);
    }
  }

  private void UpdatePitch(AudioSource source, SoundManager.SourceExtension ext)
  {
    source.pitch = ext.m_codePitch * ext.m_sourcePitch * ext.m_defPitch;
  }

  private void InitializeOptions()
  {
    Options.Get().RegisterChangedListener(Option.SOUND, new Options.ChangedCallback(this.OnMasterEnabledOptionChanged));
    Options.Get().RegisterChangedListener(Option.SOUND_VOLUME, new Options.ChangedCallback(this.OnMasterVolumeOptionChanged));
    Options.Get().RegisterChangedListener(Option.MUSIC, new Options.ChangedCallback(this.OnEnabledOptionChanged));
    Options.Get().RegisterChangedListener(Option.MUSIC_VOLUME, new Options.ChangedCallback(this.OnVolumeOptionChanged));
    Options.Get().RegisterChangedListener(Option.BACKGROUND_SOUND, new Options.ChangedCallback(this.OnBackgroundSoundOptionChanged));
  }

  private void OnMasterEnabledOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    this.UpdateAllMutes();
  }

  private void OnMasterVolumeOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    this.UpdateAllCategoryVolumes();
  }

  private void OnEnabledOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    using (Map<SoundCategory, Option>.Enumerator enumerator = SoundDataTables.s_categoryEnabledOptionMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<SoundCategory, Option> current = enumerator.Current;
        SoundCategory key = current.Key;
        if (current.Value == option)
          this.UpdateCategoryMute(key);
      }
    }
  }

  private void OnVolumeOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    using (Map<SoundCategory, Option>.Enumerator enumerator = SoundDataTables.s_categoryVolumeOptionMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<SoundCategory, Option> current = enumerator.Current;
        SoundCategory key = current.Key;
        if (current.Value == option)
          this.UpdateCategoryVolume(key);
      }
    }
  }

  private void OnBackgroundSoundOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    this.UpdateAppMute();
  }

  private void RegisterSourceByCategory(AudioSource source, SoundCategory cat)
  {
    List<AudioSource> audioSourceList1;
    if (!this.m_sourcesByCategory.TryGetValue(cat, out audioSourceList1))
    {
      List<AudioSource> audioSourceList2 = new List<AudioSource>();
      this.m_sourcesByCategory.Add(cat, audioSourceList2);
      audioSourceList2.Add(source);
    }
    else
    {
      if (audioSourceList1.Contains(source))
        return;
      audioSourceList1.Add(source);
    }
  }

  private void UnregisterSourceByCategory(AudioSource source)
  {
    SoundCategory category = this.GetCategory(source);
    List<AudioSource> audioSourceList;
    if (!this.m_sourcesByCategory.TryGetValue(category, out audioSourceList))
      UnityEngine.Debug.LogError((object) string.Format("SoundManager.UnregisterSourceByCategory() - {0} is untracked. category={1}", (object) this.GetSourceId(source), (object) category));
    else if (audioSourceList.Remove(source))
      ;
  }

  private bool IsCategoryEnabled(AudioSource source)
  {
    return SoundUtils.IsCategoryEnabled(source.GetComponent<SoundDef>().m_Category);
  }

  private float GetCategoryVolume(AudioSource source)
  {
    return SoundUtils.GetCategoryVolume(source.GetComponent<SoundDef>().m_Category);
  }

  private void RegisterSourceByClip(AudioSource source, AudioClip clip)
  {
    List<AudioSource> audioSourceList1;
    if (!this.m_sourcesByClipName.TryGetValue(clip.name, out audioSourceList1))
    {
      List<AudioSource> audioSourceList2 = new List<AudioSource>();
      this.m_sourcesByClipName.Add(clip.name, audioSourceList2);
      audioSourceList2.Add(source);
    }
    else
    {
      if (audioSourceList1.Contains(source))
        return;
      audioSourceList1.Add(source);
    }
  }

  private void UnregisterSourceByClip(AudioSource source)
  {
    AudioClip clip = source.clip;
    if ((UnityEngine.Object) clip == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SoundManager.UnregisterSourceByClip() - id {0} (source {1}) is untracked", (object) this.GetSourceId(source), (object) source));
    }
    else
    {
      List<AudioSource> audioSourceList;
      if (!this.m_sourcesByClipName.TryGetValue(clip.name, out audioSourceList))
      {
        UnityEngine.Debug.LogError((object) string.Format("SoundManager.UnregisterSourceByClip() - id {0} (source {1}) is untracked. clip={2}", (object) this.GetSourceId(source), (object) source, (object) clip));
      }
      else
      {
        audioSourceList.Remove(source);
        if (audioSourceList.Count != 0)
          return;
        this.m_sourcesByClipName.Remove(clip.name);
      }
    }
  }

  private bool ProcessClipLimits(AudioClip clip)
  {
    if ((UnityEngine.Object) this.m_config == (UnityEngine.Object) null || this.m_config.m_PlaybackLimitDefs == null)
      return false;
    string name = clip.name;
    bool flag = false;
    AudioSource source = (AudioSource) null;
    using (List<SoundPlaybackLimitDef>.Enumerator enumerator1 = this.m_config.m_PlaybackLimitDefs.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        SoundPlaybackLimitDef current1 = enumerator1.Current;
        SoundPlaybackLimitClipDef defInPlaybackDef = this.FindClipDefInPlaybackDef(name, current1);
        if (defInPlaybackDef != null)
        {
          int num1 = defInPlaybackDef.m_Priority;
          float num2 = 2f;
          int num3 = 0;
          using (List<SoundPlaybackLimitClipDef>.Enumerator enumerator2 = current1.m_ClipDefs.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              SoundPlaybackLimitClipDef current2 = enumerator2.Current;
              List<AudioSource> audioSourceList;
              if (this.m_sourcesByClipName.TryGetValue(Path.GetFileNameWithoutExtension(current2.m_Path), out audioSourceList))
              {
                int priority = current2.m_Priority;
                using (List<AudioSource>.Enumerator enumerator3 = audioSourceList.GetEnumerator())
                {
                  while (enumerator3.MoveNext())
                  {
                    AudioSource current3 = enumerator3.Current;
                    if (this.IsPlaying(current3))
                    {
                      float num4 = current3.time / current3.clip.length;
                      if ((double) num4 <= (double) current2.m_ExclusivePlaybackThreshold)
                      {
                        ++num3;
                        if (priority < num1 && (double) num4 < (double) num2)
                        {
                          source = current3;
                          num1 = priority;
                          num2 = num4;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          if (num3 >= current1.m_Limit)
          {
            flag = true;
            break;
          }
        }
      }
    }
    if (!flag)
      return false;
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return true;
    this.Stop(source);
    return false;
  }

  private SoundPlaybackLimitClipDef FindClipDefInPlaybackDef(string clipName, SoundPlaybackLimitDef def)
  {
    if (def.m_ClipDefs == null)
      return (SoundPlaybackLimitClipDef) null;
    using (List<SoundPlaybackLimitClipDef>.Enumerator enumerator = def.m_ClipDefs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SoundPlaybackLimitClipDef current = enumerator.Current;
        string withoutExtension = Path.GetFileNameWithoutExtension(current.m_Path);
        if (clipName == withoutExtension)
          return current;
      }
    }
    return (SoundPlaybackLimitClipDef) null;
  }

  public bool StartDucking(SoundDucker ducker)
  {
    if ((UnityEngine.Object) ducker == (UnityEngine.Object) null || ducker.m_DuckedCategoryDefs == null || ducker.m_DuckedCategoryDefs.Count == 0)
      return false;
    this.RegisterForDucking((object) ducker, ducker.GetDuckedCategoryDefs());
    return true;
  }

  public void StopDucking(SoundDucker ducker)
  {
    if ((UnityEngine.Object) ducker == (UnityEngine.Object) null || ducker.m_DuckedCategoryDefs == null || ducker.m_DuckedCategoryDefs.Count == 0)
      return;
    this.UnregisterForDucking((object) ducker, ducker.GetDuckedCategoryDefs());
  }

  public bool IsIgnoringDucking(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return true;
    SoundDef component = source.GetComponent<SoundDef>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return true;
    return component.m_IgnoreDucking;
  }

  public void SetIgnoreDucking(AudioSource source, bool enable)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return;
    SoundDef component = source.GetComponent<SoundDef>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    component.m_IgnoreDucking = enable;
  }

  private void RegisterSourceForDucking(AudioSource source, SoundManager.SourceExtension ext)
  {
    SoundDuckingDef duckingDefForSource = this.FindDuckingDefForSource(source);
    if (duckingDefForSource == null)
      return;
    this.RegisterForDucking((object) source, duckingDefForSource.m_DuckedCategoryDefs);
    ext.m_ducking = true;
  }

  private void RegisterForDucking(object trigger, List<SoundDuckedCategoryDef> defs)
  {
    using (List<SoundDuckedCategoryDef>.Enumerator enumerator = defs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SoundDuckedCategoryDef current = enumerator.Current;
        this.ChangeDuckState(this.RegisterDuckState(trigger, current), SoundManager.DuckMode.BEGINNING);
      }
    }
  }

  private SoundManager.DuckState RegisterDuckState(object trigger, SoundDuckedCategoryDef duckedCatDef)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SoundManager.\u003CRegisterDuckState\u003Ec__AnonStorey455 stateCAnonStorey455 = new SoundManager.\u003CRegisterDuckState\u003Ec__AnonStorey455();
    // ISSUE: reference to a compiler-generated field
    stateCAnonStorey455.trigger = trigger;
    SoundCategory category = duckedCatDef.m_Category;
    List<SoundManager.DuckState> duckStateList;
    if (this.m_duckStates.TryGetValue(category, out duckStateList))
    {
      // ISSUE: reference to a compiler-generated method
      SoundManager.DuckState duckState = duckStateList.Find(new Predicate<SoundManager.DuckState>(stateCAnonStorey455.\u003C\u003Em__31E));
      if (duckState != null)
        return duckState;
    }
    else
    {
      duckStateList = new List<SoundManager.DuckState>();
      this.m_duckStates.Add(category, duckStateList);
    }
    SoundManager.DuckState duckState1 = new SoundManager.DuckState();
    duckStateList.Add(duckState1);
    // ISSUE: reference to a compiler-generated field
    duckState1.SetTrigger(stateCAnonStorey455.trigger);
    duckState1.SetDuckedDef(duckedCatDef);
    return duckState1;
  }

  private void UnregisterSourceForDucking(AudioSource source, SoundManager.SourceExtension ext)
  {
    if (!ext.m_ducking)
      return;
    SoundDuckingDef duckingDefForSource = this.FindDuckingDefForSource(source);
    if (duckingDefForSource == null)
      return;
    this.UnregisterForDucking((object) source, duckingDefForSource.m_DuckedCategoryDefs);
  }

  private void UnregisterForDucking(object trigger, List<SoundDuckedCategoryDef> defs)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SoundManager.\u003CUnregisterForDucking\u003Ec__AnonStorey456 duckingCAnonStorey456 = new SoundManager.\u003CUnregisterForDucking\u003Ec__AnonStorey456();
    // ISSUE: reference to a compiler-generated field
    duckingCAnonStorey456.trigger = trigger;
    using (List<SoundDuckedCategoryDef>.Enumerator enumerator = defs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SoundCategory category = enumerator.Current.m_Category;
        List<SoundManager.DuckState> duckStateList;
        if (!this.m_duckStates.TryGetValue(category, out duckStateList))
        {
          // ISSUE: reference to a compiler-generated field
          UnityEngine.Debug.LogError((object) string.Format("SoundManager.UnregisterForDucking() - {0} ducks {1}, but no DuckStates were found for {1}", duckingCAnonStorey456.trigger, (object) category));
        }
        else
        {
          // ISSUE: reference to a compiler-generated method
          SoundManager.DuckState state = duckStateList.Find(new Predicate<SoundManager.DuckState>(duckingCAnonStorey456.\u003C\u003Em__31F));
          if (state != null)
            this.ChangeDuckState(state, SoundManager.DuckMode.RESTORING);
        }
      }
    }
  }

  private uint GetNextDuckStateTweenId()
  {
    this.m_nextDuckStateTweenId = (uint) ((int) this.m_nextDuckStateTweenId + 1 & -1);
    return this.m_nextDuckStateTweenId;
  }

  private void ChangeDuckState(SoundManager.DuckState state, SoundManager.DuckMode mode)
  {
    string tweenName = state.GetTweenName();
    if (tweenName != null)
      iTween.StopByName(this.gameObject, tweenName);
    state.SetMode(mode);
    state.SetTweenName((string) null);
    switch (mode)
    {
      case SoundManager.DuckMode.BEGINNING:
        this.AnimateBeginningDuckState(state);
        break;
      case SoundManager.DuckMode.RESTORING:
        this.AnimateRestoringDuckState(state);
        break;
    }
  }

  private void AnimateBeginningDuckState(SoundManager.DuckState state)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SoundManager.\u003CAnimateBeginningDuckState\u003Ec__AnonStorey457 stateCAnonStorey457 = new SoundManager.\u003CAnimateBeginningDuckState\u003Ec__AnonStorey457();
    // ISSUE: reference to a compiler-generated field
    stateCAnonStorey457.state = state;
    // ISSUE: reference to a compiler-generated field
    stateCAnonStorey457.\u003C\u003Ef__this = this;
    string name = string.Format("DuckState Begin id={0}", (object) this.GetNextDuckStateTweenId());
    // ISSUE: reference to a compiler-generated field
    stateCAnonStorey457.state.SetTweenName(name);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    stateCAnonStorey457.duckedDef = stateCAnonStorey457.state.GetDuckedDef();
    // ISSUE: reference to a compiler-generated method
    Action<object> action = new Action<object>(stateCAnonStorey457.\u003C\u003Em__320);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "name", (object) name, (object) "time", (object) stateCAnonStorey457.duckedDef.m_BeginSec, (object) "easeType", (object) stateCAnonStorey457.duckedDef.m_BeginEaseType, (object) "from", (object) stateCAnonStorey457.state.GetVolume(), (object) "to", (object) stateCAnonStorey457.duckedDef.m_Volume, (object) "onupdate", (object) action, (object) "onupdatetarget", (object) this.gameObject, (object) "oncomplete", (object) "OnDuckStateBeginningComplete", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) stateCAnonStorey457.state));
  }

  private void OnDuckStateBeginningComplete(SoundManager.DuckState state)
  {
    state.SetMode(SoundManager.DuckMode.HOLD);
    state.SetTweenName((string) null);
  }

  private void AnimateRestoringDuckState(SoundManager.DuckState state)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SoundManager.\u003CAnimateRestoringDuckState\u003Ec__AnonStorey458 stateCAnonStorey458 = new SoundManager.\u003CAnimateRestoringDuckState\u003Ec__AnonStorey458();
    // ISSUE: reference to a compiler-generated field
    stateCAnonStorey458.state = state;
    // ISSUE: reference to a compiler-generated field
    stateCAnonStorey458.\u003C\u003Ef__this = this;
    string name = string.Format("DuckState Finish id={0}", (object) this.GetNextDuckStateTweenId());
    // ISSUE: reference to a compiler-generated field
    stateCAnonStorey458.state.SetTweenName(name);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    stateCAnonStorey458.duckedDef = stateCAnonStorey458.state.GetDuckedDef();
    // ISSUE: reference to a compiler-generated method
    Action<object> action = new Action<object>(stateCAnonStorey458.\u003C\u003Em__321);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "name", (object) name, (object) "time", (object) stateCAnonStorey458.duckedDef.m_RestoreSec, (object) "easeType", (object) stateCAnonStorey458.duckedDef.m_RestoreEaseType, (object) "from", (object) stateCAnonStorey458.state.GetVolume(), (object) "to", (object) 1f, (object) "onupdate", (object) action, (object) "onupdatetarget", (object) this.gameObject, (object) "oncomplete", (object) "OnDuckStateRestoringComplete", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) stateCAnonStorey458.state));
  }

  private void OnDuckStateRestoringComplete(SoundManager.DuckState state)
  {
    SoundCategory category = state.GetDuckedDef().m_Category;
    List<SoundManager.DuckState> duckState = this.m_duckStates[category];
    for (int index = 0; index < duckState.Count; ++index)
    {
      if (duckState[index] == state)
      {
        duckState.RemoveAt(index);
        if (duckState.Count != 0)
          break;
        this.m_duckStates.Remove(category);
        break;
      }
    }
  }

  private SoundDuckingDef FindDuckingDefForSource(AudioSource source)
  {
    return this.FindDuckingDefForCategory(this.GetCategory(source));
  }

  private SoundDuckingDef FindDuckingDefForCategory(SoundCategory cat)
  {
    if ((UnityEngine.Object) this.m_config == (UnityEngine.Object) null || this.m_config.m_DuckingDefs == null)
      return (SoundDuckingDef) null;
    using (List<SoundDuckingDef>.Enumerator enumerator = this.m_config.m_DuckingDefs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SoundDuckingDef current = enumerator.Current;
        if (cat == current.m_TriggerCategory)
          return current;
      }
    }
    return (SoundDuckingDef) null;
  }

  private float GetDuckingVolume(AudioSource source)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      return 1f;
    SoundDef component = source.GetComponent<SoundDef>();
    if (component.m_IgnoreDucking)
      return 1f;
    return this.GetDuckingVolume(component.m_Category);
  }

  private float GetDuckingVolume(SoundCategory cat)
  {
    List<SoundManager.DuckState> duckStateList;
    if (!this.m_duckStates.TryGetValue(cat, out duckStateList))
      return 1f;
    float num = 1f;
    using (List<SoundManager.DuckState>.Enumerator enumerator = duckStateList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SoundManager.DuckState current = enumerator.Current;
        SoundCategory triggerCategory = current.GetTriggerCategory();
        if (triggerCategory == SoundCategory.NONE || SoundUtils.IsCategoryAudible(triggerCategory))
        {
          float volume = current.GetVolume();
          if ((double) num > (double) volume)
            num = volume;
        }
      }
    }
    return num;
  }

  private int GetNextSourceId()
  {
    int nextSourceId = this.m_nextSourceId;
    this.m_nextSourceId = this.m_nextSourceId != int.MaxValue ? this.m_nextSourceId + 1 : 1;
    return nextSourceId;
  }

  private int GetSourceId(AudioSource source)
  {
    SoundManager.SourceExtension extension = this.GetExtension(source);
    if (extension == null)
      return 0;
    return extension.m_id;
  }

  private AudioSource PlayImpl(AudioSource source, AudioClip oneShotClip = null, bool localizeAudio = true)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null)
    {
      AudioSource placeholderSource = this.GetPlaceholderSource();
      if ((UnityEngine.Object) placeholderSource == (UnityEngine.Object) null)
      {
        Error.AddDevFatal("SoundManager.Play() - source is null and fallback is null");
        return (AudioSource) null;
      }
      source = UnityEngine.Object.Instantiate<AudioSource>(placeholderSource);
      this.m_generatedSources.Add(source);
    }
    bool flag = this.IsActive(source);
    SoundManager.SourceExtension ext = this.RegisterExtension(source, true, oneShotClip, localizeAudio);
    if (ext == null)
      return (AudioSource) null;
    if (!flag)
      this.RegisterSourceForDucking(source, ext);
    this.UpdateSource(source, ext);
    source.Play();
    return source;
  }

  private SoundDef GetDefFromSource(AudioSource source)
  {
    SoundDef soundDef = source.GetComponent<SoundDef>();
    if ((UnityEngine.Object) soundDef == (UnityEngine.Object) null)
    {
      Log.Sound.Print("SoundUtils.GetDefFromSource() - source={0} has no def. adding new def.", (object) source);
      soundDef = source.gameObject.AddComponent<SoundDef>();
    }
    return soundDef;
  }

  private void OnAppFocusChanged(bool focus, object userData)
  {
    this.UpdateAppMute();
  }

  private void UpdateAppMute()
  {
    this.UpdateMusicAndSources();
    if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
      this.m_mute = (ApplicationMgr.Get().HasFocus() ? 1 : (Options.Get().GetBool(Option.BACKGROUND_SOUND) ? 1 : 0)) == 0;
    this.UpdateAllMutes();
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    this.GarbageCollectBundles();
  }

  private AudioSource GenerateAudioSource(AudioSource templateSource, AudioClip clip)
  {
    string name = string.Format("Audio Object - {0}", (object) clip.name);
    AudioSource component;
    if ((bool) ((UnityEngine.Object) templateSource))
    {
      GameObject go = new GameObject(name);
      SoundUtils.AddAudioSourceComponents(go, (AudioClip) null);
      component = go.GetComponent<AudioSource>();
      SoundUtils.CopyAudioSource(templateSource, component);
    }
    else if ((bool) ((UnityEngine.Object) this.m_config.m_PlayClipTemplate))
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_config.m_PlayClipTemplate.gameObject);
      gameObject.name = name;
      component = gameObject.GetComponent<AudioSource>();
    }
    else
    {
      GameObject go = new GameObject(name);
      SoundUtils.AddAudioSourceComponents(go, (AudioClip) null);
      component = go.GetComponent<AudioSource>();
    }
    this.m_generatedSources.Add(component);
    return component;
  }

  private void InitSourceTransform(AudioSource source, GameObject parentObject)
  {
    source.transform.parent = this.transform;
    if ((UnityEngine.Object) parentObject == (UnityEngine.Object) null)
      source.transform.position = Vector3.zero;
    else
      source.transform.position = parentObject.transform.position;
  }

  private void FinishSource(AudioSource source)
  {
    if ((UnityEngine.Object) this.m_currentMusicTrack == (UnityEngine.Object) source)
      this.ChangeCurrentMusicTrack((AudioSource) null);
    else if ((UnityEngine.Object) this.m_currentAmbienceTrack == (UnityEngine.Object) source)
      this.ChangeCurrentAmbienceTrack((AudioSource) null);
    for (int index = 0; index < this.m_fadingTracks.Count; ++index)
    {
      if ((UnityEngine.Object) this.m_fadingTracks[index] == (UnityEngine.Object) source)
      {
        this.m_fadingTracks.RemoveAt(index);
        break;
      }
    }
    this.UnregisterSourceByCategory(source);
    this.UnregisterSourceByClip(source);
    SoundManager.SourceExtension extension = this.GetExtension(source);
    if (extension != null)
    {
      this.UnregisterSourceForDucking(source, extension);
      this.UnregisterSourceBundle(source, extension);
      this.UnregisterExtension(source, extension);
    }
    this.FinishGeneratedSource(source);
  }

  private void FinishGeneratedSource(AudioSource source)
  {
    for (int index = 0; index < this.m_generatedSources.Count; ++index)
    {
      if ((UnityEngine.Object) this.m_generatedSources[index] == (UnityEngine.Object) source)
      {
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) source.gameObject);
        this.m_generatedSources.RemoveAt(index);
        break;
      }
    }
  }

  private SoundManager.BundleInfo RegisterSourceBundle(string name, AudioSource source)
  {
    SoundManager.BundleInfo bundleInfo;
    if (!this.m_bundleInfos.TryGetValue(name, out bundleInfo))
    {
      bundleInfo = new SoundManager.BundleInfo();
      bundleInfo.SetName(name);
      this.m_bundleInfos.Add(name, bundleInfo);
    }
    if ((UnityEngine.Object) source != (UnityEngine.Object) null)
    {
      bundleInfo.AddRef(source);
      this.RegisterExtension(source, false, (AudioClip) null, true).m_bundleName = name;
    }
    return bundleInfo;
  }

  private void UnregisterSourceBundle(AudioSource source, SoundManager.SourceExtension ext)
  {
    if (ext.m_bundleName == null)
      return;
    this.UnregisterSourceBundle(ext.m_bundleName, source);
  }

  private void UnregisterSourceBundle(string name, AudioSource source)
  {
    SoundManager.BundleInfo bundleInfo;
    if (!this.m_bundleInfos.TryGetValue(name, out bundleInfo) || !bundleInfo.RemoveRef(source) || !bundleInfo.CanGarbageCollect())
      return;
    this.m_bundleInfos.Remove(name);
    this.UnloadSoundBundle(name);
  }

  private void UnloadSoundBundle(string name)
  {
    AssetCache.ClearSound(name);
  }

  private void GarbageCollectBundles()
  {
    Map<string, SoundManager.BundleInfo> map = new Map<string, SoundManager.BundleInfo>();
    using (Map<string, SoundManager.BundleInfo>.Enumerator enumerator = this.m_bundleInfos.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, SoundManager.BundleInfo> current = enumerator.Current;
        string key = current.Key;
        SoundManager.BundleInfo bundleInfo = current.Value;
        bundleInfo.EnableGarbageCollect(true);
        if (bundleInfo.CanGarbageCollect())
          this.UnloadSoundBundle(key);
        else
          map.Add(key, bundleInfo);
      }
    }
    this.m_bundleInfos = map;
  }

  private void UpdateMusicAndSources()
  {
    this.UpdateMusicAndAmbience();
    this.UpdateSources();
  }

  private void UpdateSources()
  {
    this.UpdateSourceExtensionMappings();
    this.UpdateSourcesByCategory();
    this.UpdateSourcesByClipName();
    this.UpdateSourceBundles();
    this.UpdateGeneratedSources();
    this.UpdateDuckStates();
  }

  private void UpdateSourceExtensionMappings()
  {
    int index = 0;
    while (index < this.m_extensionMappings.Count)
    {
      AudioSource source = this.m_extensionMappings[index].Source;
      if ((UnityEngine.Object) source == (UnityEngine.Object) null)
      {
        this.m_extensionMappings.RemoveAt(index);
      }
      else
      {
        if (!this.IsActive(source))
          this.m_inactiveSources.Add(source);
        ++index;
      }
    }
    this.CleanInactiveSources();
  }

  private void CleanUpSourceList(List<AudioSource> sources)
  {
    if (sources == null)
      return;
    int index = 0;
    while (index < sources.Count)
    {
      if ((UnityEngine.Object) sources[index] == (UnityEngine.Object) null)
        sources.RemoveAt(index);
      else
        ++index;
    }
  }

  private void UpdateSourcesByCategory()
  {
    using (Map<SoundCategory, List<AudioSource>>.ValueCollection.Enumerator enumerator = this.m_sourcesByCategory.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.CleanUpSourceList(enumerator.Current);
    }
  }

  private void UpdateSourcesByClipName()
  {
    using (Map<string, List<AudioSource>>.ValueCollection.Enumerator enumerator = this.m_sourcesByClipName.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.CleanUpSourceList(enumerator.Current);
    }
  }

  private void UpdateSourceBundles()
  {
    using (Map<string, SoundManager.BundleInfo>.ValueCollection.Enumerator enumerator = this.m_bundleInfos.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SoundManager.BundleInfo current = enumerator.Current;
        List<AudioSource> refs = current.GetRefs();
        int index = 0;
        bool flag = false;
        while (index < refs.Count)
        {
          if ((UnityEngine.Object) refs[index] == (UnityEngine.Object) null)
          {
            flag = true;
            refs.RemoveAt(index);
          }
          else
            ++index;
        }
        if (flag)
        {
          string name = current.GetName();
          if (current.CanGarbageCollect())
          {
            this.m_bundleInfos.Remove(name);
            this.UnloadSoundBundle(name);
          }
        }
      }
    }
  }

  private void UpdateGeneratedSources()
  {
    this.CleanUpSourceList(this.m_generatedSources);
  }

  private void FinishDeadGeneratedSource(AudioSource source)
  {
    for (int index = 0; index < this.m_generatedSources.Count; ++index)
    {
      if ((UnityEngine.Object) this.m_generatedSources[index] == (UnityEngine.Object) source)
      {
        this.m_generatedSources.RemoveAt(index);
        break;
      }
    }
  }

  private void UpdateDuckStates()
  {
    using (Map<SoundCategory, List<SoundManager.DuckState>>.ValueCollection.Enumerator enumerator1 = this.m_duckStates.Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<SoundManager.DuckState>.Enumerator enumerator2 = enumerator1.Current.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            SoundManager.DuckState current = enumerator2.Current;
            if (!current.IsTriggerAlive() && current.GetMode() != SoundManager.DuckMode.RESTORING)
              this.ChangeDuckState(current, SoundManager.DuckMode.RESTORING);
          }
        }
      }
    }
  }

  private void CleanInactiveSources()
  {
    using (List<AudioSource>.Enumerator enumerator = this.m_inactiveSources.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.FinishSource(enumerator.Current);
    }
    this.m_inactiveSources.Clear();
  }

  [Conditional("SOUND_SOURCE_DEBUG")]
  private void SourcePrint(string format, params object[] args)
  {
    Log.Sound.Print(format, args);
  }

  [Conditional("SOUND_SOURCE_DEBUG")]
  private void SourceScreenPrint(string format, params object[] args)
  {
    Log.Sound.ScreenPrint(format, args);
  }

  [Conditional("SOUND_TRACK_DEBUG")]
  private void TrackPrint(string format, params object[] args)
  {
    Log.Sound.Print(format, args);
  }

  [Conditional("SOUND_TRACK_DEBUG")]
  private void TrackScreenPrint(string format, params object[] args)
  {
    Log.Sound.ScreenPrint(format, args);
  }

  [Conditional("SOUND_CATEGORY_DEBUG")]
  private void CategoryPrint(string format, params object[] args)
  {
    Log.Sound.Print(format, args);
  }

  [Conditional("SOUND_CATEGORY_DEBUG")]
  private void CategoryScreenPrint(string format, params object[] args)
  {
    Log.Sound.ScreenPrint(format, args);
  }

  [Conditional("SOUND_CATEGORY_DEBUG")]
  private void PrintAllCategorySources()
  {
    Log.Sound.Print("SoundManager.PrintAllCategorySources()");
    using (Map<SoundCategory, List<AudioSource>>.Enumerator enumerator = this.m_sourcesByCategory.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<SoundCategory, List<AudioSource>> current = enumerator.Current;
        SoundCategory key = current.Key;
        List<AudioSource> audioSourceList = current.Value;
        Log.Sound.Print("Category {0}:", (object) key);
        for (int index = 0; index < audioSourceList.Count; ++index)
          Log.Sound.Print("    {0} = {1}", new object[2]
          {
            (object) index,
            (object) audioSourceList[index]
          });
      }
    }
  }

  [Conditional("SOUND_BUNDLE_DEBUG")]
  private void BundlePrint(string format, params object[] args)
  {
    Log.Sound.Print(format, args);
  }

  [Conditional("SOUND_BUNDLE_DEBUG")]
  private void BundleScreenPrint(string format, params object[] args)
  {
    Log.Sound.ScreenPrint(format, args);
  }

  [Conditional("SOUND_DUCKING_DEBUG")]
  private void DuckingPrint(string format, params object[] args)
  {
    Log.Sound.Print(format, args);
  }

  [Conditional("SOUND_DUCKING_DEBUG")]
  private void DuckingScreenPrint(string format, params object[] args)
  {
    Log.Sound.ScreenPrint(format, args);
  }

  private void AddExtensionMapping(AudioSource source, SoundManager.SourceExtension extension)
  {
    if ((UnityEngine.Object) source == (UnityEngine.Object) null || extension == null)
      return;
    this.m_extensionMappings.Add(new SoundManager.ExtensionMapping()
    {
      Source = source,
      Extension = extension
    });
  }

  private void RemoveExtensionMapping(AudioSource source)
  {
    for (int index = 0; index < this.m_extensionMappings.Count; ++index)
    {
      if ((UnityEngine.Object) this.m_extensionMappings[index].Source == (UnityEngine.Object) source)
      {
        this.m_extensionMappings.RemoveAt(index);
        break;
      }
    }
  }

  private SoundManager.SourceExtension GetExtension(AudioSource source)
  {
    for (int index = 0; index < this.m_extensionMappings.Count; ++index)
    {
      SoundManager.ExtensionMapping extensionMapping = this.m_extensionMappings[index];
      if ((UnityEngine.Object) extensionMapping.Source == (UnityEngine.Object) source)
        return extensionMapping.Extension;
    }
    return (SoundManager.SourceExtension) null;
  }

  private class ExtensionMapping
  {
    public AudioSource Source;
    public SoundManager.SourceExtension Extension;
  }

  private class SoundLoadContext
  {
    public AudioSource m_template;
    public GameObject m_parent;
    public float m_volume;
    public SceneMgr.Mode m_sceneMode;
    public bool m_haveCallback;
    public SoundManager.LoadedCallback m_callback;
    public object m_userData;

    public void Init(GameObject parent, float volume, SoundManager.LoadedCallback callback, object userData)
    {
      this.m_parent = parent;
      this.m_volume = volume;
      this.Init(callback, userData);
    }

    public void Init(SoundManager.LoadedCallback callback, object userData)
    {
      this.m_sceneMode = !((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null) ? SceneMgr.Get().GetMode() : SceneMgr.Mode.INVALID;
      this.m_haveCallback = callback != null;
      this.m_callback = callback;
      this.m_userData = userData;
    }
  }

  private class SourceExtension
  {
    public float m_codeVolume = 1f;
    public float m_sourceVolume = 1f;
    public float m_defVolume = 1f;
    public float m_codePitch = 1f;
    public float m_sourcePitch = 1f;
    public float m_defPitch = 1f;
    public int m_id;
    public AudioClip m_sourceClip;
    public bool m_paused;
    public bool m_ducking;
    public string m_bundleName;
  }

  private class BundleInfo
  {
    private List<AudioSource> m_refs = new List<AudioSource>();
    private string m_name;
    private bool m_garbageCollect;

    public string GetName()
    {
      return this.m_name;
    }

    public void SetName(string name)
    {
      this.m_name = name;
    }

    public int GetRefCount()
    {
      return this.m_refs.Count;
    }

    public List<AudioSource> GetRefs()
    {
      return this.m_refs;
    }

    public void AddRef(AudioSource instance)
    {
      this.m_garbageCollect = false;
      this.m_refs.Add(instance);
    }

    public bool RemoveRef(AudioSource instance)
    {
      return this.m_refs.Remove(instance);
    }

    public bool CanGarbageCollect()
    {
      return this.m_garbageCollect && this.m_refs.Count <= 0 && !AssetCache.IsLoading(this.m_name);
    }

    public bool IsGarbageCollectEnabled()
    {
      return this.m_garbageCollect;
    }

    public void EnableGarbageCollect(bool enable)
    {
      this.m_garbageCollect = enable;
    }
  }

  private enum DuckMode
  {
    IDLE,
    BEGINNING,
    HOLD,
    RESTORING,
  }

  private class DuckState
  {
    private float m_volume = 1f;
    private object m_trigger;
    private SoundCategory m_triggerCategory;
    private SoundDuckedCategoryDef m_duckedDef;
    private SoundManager.DuckMode m_mode;
    private string m_tweenName;

    public object GetTrigger()
    {
      return this.m_trigger;
    }

    public void SetTrigger(object trigger)
    {
      this.m_trigger = trigger;
      AudioSource source = trigger as AudioSource;
      if (!((UnityEngine.Object) source != (UnityEngine.Object) null))
        return;
      this.m_triggerCategory = SoundManager.Get().GetCategory(source);
    }

    public bool IsTrigger(object trigger)
    {
      return this.m_trigger == trigger;
    }

    public bool IsTriggerAlive()
    {
      return GeneralUtils.IsObjectAlive(this.m_trigger);
    }

    public SoundCategory GetTriggerCategory()
    {
      return this.m_triggerCategory;
    }

    public SoundDuckedCategoryDef GetDuckedDef()
    {
      return this.m_duckedDef;
    }

    public void SetDuckedDef(SoundDuckedCategoryDef def)
    {
      this.m_duckedDef = def;
    }

    public SoundManager.DuckMode GetMode()
    {
      return this.m_mode;
    }

    public void SetMode(SoundManager.DuckMode mode)
    {
      this.m_mode = mode;
    }

    public string GetTweenName()
    {
      return this.m_tweenName;
    }

    public void SetTweenName(string name)
    {
      this.m_tweenName = name;
    }

    public float GetVolume()
    {
      return this.m_volume;
    }

    public void SetVolume(float volume)
    {
      this.m_volume = volume;
    }
  }

  public delegate void LoadedCallback(AudioSource source, object userData);
}
