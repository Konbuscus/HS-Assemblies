﻿// Decompiled with JetBrains decompiler
// Type: ThreeSliceTextElement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ThreeSliceTextElement : MonoBehaviour
{
  public UberText m_text;
  public ThreeSliceElement m_threeSlice;

  public void SetText(string text)
  {
    this.m_text.Text = text;
    this.m_text.UpdateNow();
    this.Resize();
  }

  public void Resize()
  {
    this.m_threeSlice.SetMiddleWidth(this.GetTextWidth());
  }

  private float GetTextWidth()
  {
    return this.m_text.GetTextBounds().size.x;
  }
}
