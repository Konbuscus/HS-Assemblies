﻿// Decompiled with JetBrains decompiler
// Type: BoxEventMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoxEventMgr : MonoBehaviour
{
  private Map<BoxEventType, Spell> m_eventMap = new Map<BoxEventType, Spell>();
  public BoxEventInfo m_EventInfo;

  private void Awake()
  {
    this.m_eventMap.Add(BoxEventType.STARTUP_HUB, this.m_EventInfo.m_StartupHub);
    this.m_eventMap.Add(BoxEventType.STARTUP_TUTORIAL, this.m_EventInfo.m_StartupTutorial);
    this.m_eventMap.Add(BoxEventType.TUTORIAL_PLAY, this.m_EventInfo.m_TutorialPlay);
    this.m_eventMap.Add(BoxEventType.DISK_LOADING, this.m_EventInfo.m_DiskLoading);
    this.m_eventMap.Add(BoxEventType.DISK_MAIN_MENU, this.m_EventInfo.m_DiskMainMenu);
    this.m_eventMap.Add(BoxEventType.DOORS_CLOSE, this.m_EventInfo.m_DoorsClose);
    this.m_eventMap.Add(BoxEventType.DOORS_OPEN, this.m_EventInfo.m_DoorsOpen);
    this.m_eventMap.Add(BoxEventType.DRAWER_OPEN, this.m_EventInfo.m_DrawerOpen);
    this.m_eventMap.Add(BoxEventType.DRAWER_CLOSE, this.m_EventInfo.m_DrawerClose);
    this.m_eventMap.Add(BoxEventType.SHADOW_FADE_IN, this.m_EventInfo.m_ShadowFadeIn);
    this.m_eventMap.Add(BoxEventType.SHADOW_FADE_OUT, this.m_EventInfo.m_ShadowFadeOut);
    this.m_eventMap.Add(BoxEventType.STARTUP_SET_ROTATION, this.m_EventInfo.m_StartupSetRotation);
  }

  public Spell GetEventSpell(BoxEventType eventType)
  {
    Spell spell = (Spell) null;
    this.m_eventMap.TryGetValue(eventType, out spell);
    return spell;
  }
}
