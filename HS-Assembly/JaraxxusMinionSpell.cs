﻿// Decompiled with JetBrains decompiler
// Type: JaraxxusMinionSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class JaraxxusMinionSpell : Spell
{
  public float m_MoveToLocationDuration = 1.5f;
  public iTween.EaseType m_MoveToLocationEaseType = iTween.EaseType.linear;
  public float m_MoveToHeroSpotDelay = 3.5f;
  public float m_MoveToHeroSpotDuration = 0.3f;
  public iTween.EaseType m_MoveToHeroSpotEaseType = iTween.EaseType.linear;
  public float m_MoveToLocationDelay;

  public override bool AddPowerTargets()
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.PowerHistory power = enumerator.Current.GetPower();
        if (power.Type == Network.PowerType.FULL_ENTITY)
        {
          int id = (power as Network.HistFullEntity).Entity.ID;
          Entity entity = GameState.Get().GetEntity(id);
          if (entity == null)
          {
            UnityEngine.Debug.LogWarning((object) string.Format("{0}.AddPowerTargets() - WARNING encountered HistFullEntity where entity id={1} but there is no entity with that id", (object) this, (object) id));
            return false;
          }
          if (!entity.IsHero())
          {
            UnityEngine.Debug.LogWarning((object) string.Format("{0}.AddPowerTargets() - WARNING HistFullEntity where entity id={1} is not a hero", (object) this, (object) id));
            return false;
          }
          this.AddTarget(entity.GetCard().gameObject);
          return true;
        }
      }
    }
    return false;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    this.StartCoroutine(this.SetupHero());
  }

  [DebuggerHidden]
  private IEnumerator SetupHero()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JaraxxusMinionSpell.\u003CSetupHero\u003Ec__Iterator25C() { \u003C\u003Ef__this = this };
  }

  private PowerTask FindFullEntityTask()
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PowerTask current = enumerator.Current;
        if (current.GetPower().Type == Network.PowerType.FULL_ENTITY)
          return current;
      }
    }
    return (PowerTask) null;
  }

  private void Finish()
  {
    this.GetSourceCard().GetActor().TurnOnCollider();
    Card targetCard = this.GetTargetCard();
    targetCard.GetActor().TurnOnCollider();
    targetCard.ShowCard();
    this.OnSpellFinished();
  }

  [DebuggerHidden]
  private IEnumerator PlaySummoningSpells(Card minionCard, Card heroCard)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JaraxxusMinionSpell.\u003CPlaySummoningSpells\u003Ec__Iterator25D() { minionCard = minionCard, heroCard = heroCard, \u003C\u0024\u003EminionCard = minionCard, \u003C\u0024\u003EheroCard = heroCard, \u003C\u003Ef__this = this };
  }

  private void MoveToSpellLocation(Card minionCard, Card heroCard)
  {
    Hashtable args1 = iTween.Hash((object) "position", (object) this.transform.position, (object) "time", (object) this.m_MoveToLocationDuration, (object) "easetype", (object) this.m_MoveToLocationEaseType);
    iTween.MoveTo(minionCard.gameObject, args1);
    Hashtable args2 = iTween.Hash((object) "position", (object) this.transform.position, (object) "time", (object) this.m_MoveToLocationDuration, (object) "easetype", (object) this.m_MoveToLocationEaseType);
    iTween.MoveTo(heroCard.gameObject, args2);
  }

  private void MoveToHeroSpot(Card minionCard, Card heroCard, Zone heroZone)
  {
    Hashtable args1 = iTween.Hash((object) "position", (object) heroZone.transform.position, (object) "time", (object) this.m_MoveToHeroSpotDuration, (object) "easetype", (object) this.m_MoveToHeroSpotEaseType);
    iTween.MoveTo(minionCard.gameObject, args1);
    Hashtable args2 = iTween.Hash((object) "position", (object) heroZone.transform.position, (object) "time", (object) this.m_MoveToHeroSpotDuration, (object) "easetype", (object) this.m_MoveToHeroSpotEaseType, (object) "oncomplete", (object) "OnMoveToHeroSpotComplete", (object) "oncompletetarget", (object) this.gameObject);
    iTween.MoveTo(heroCard.gameObject, args2);
  }

  private void OnMoveToHeroSpotComplete()
  {
    this.Finish();
  }
}
