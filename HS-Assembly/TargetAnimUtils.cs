﻿// Decompiled with JetBrains decompiler
// Type: TargetAnimUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TargetAnimUtils : MonoBehaviour
{
  public GameObject m_Target;

  private void Awake()
  {
    if (!((Object) this.m_Target == (Object) null))
      return;
    this.enabled = false;
  }

  public void PrintLog(string message)
  {
    Debug.Log((object) message);
  }

  public void PrintLogWarning(string message)
  {
    Debug.LogWarning((object) message);
  }

  public void PrintLogError(string message)
  {
    Debug.LogError((object) message);
  }

  public void PlayParticles()
  {
    if ((Object) this.m_Target == (Object) null || !((Object) this.m_Target.GetComponent<ParticleEmitter>() != (Object) null))
      return;
    this.m_Target.GetComponent<ParticleEmitter>().emit = true;
  }

  public void PlayNewParticles()
  {
    this.m_Target.GetComponent<ParticleSystem>().Play();
  }

  public void StopNewParticles()
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    this.m_Target.GetComponent<ParticleSystem>().Stop();
  }

  public void StopParticles()
  {
    if ((Object) this.m_Target == (Object) null || !((Object) this.m_Target.GetComponent<ParticleEmitter>() != (Object) null))
      return;
    this.m_Target.GetComponent<ParticleEmitter>().emit = false;
  }

  public void PlayParticlesInChildren()
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    foreach (ParticleEmitter componentsInChild in this.m_Target.GetComponentsInChildren<ParticleEmitter>())
      componentsInChild.emit = true;
  }

  public void StopParticlesInChildren()
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    foreach (ParticleEmitter componentsInChild in this.m_Target.GetComponentsInChildren<ParticleEmitter>())
      componentsInChild.emit = false;
  }

  public void KillParticlesInChildren()
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    Particle[] particleArray = new Particle[0];
    foreach (ParticleEmitter componentsInChild in this.m_Target.GetComponentsInChildren<ParticleEmitter>())
    {
      componentsInChild.emit = false;
      componentsInChild.particles = particleArray;
    }
  }

  public void PlayAnimation()
  {
    if ((Object) this.m_Target == (Object) null || !((Object) this.m_Target.GetComponent<Animation>() != (Object) null))
      return;
    this.m_Target.GetComponent<Animation>().Play();
  }

  public void StopAnimation()
  {
    if ((Object) this.m_Target == (Object) null || !((Object) this.m_Target.GetComponent<Animation>() != (Object) null))
      return;
    this.m_Target.GetComponent<Animation>().Stop();
  }

  public void PlayAnimationsInChildren()
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    foreach (Animation componentsInChild in this.m_Target.GetComponentsInChildren<Animation>())
      componentsInChild.Play();
  }

  public void StopAnimationsInChildren()
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    foreach (Animation componentsInChild in this.m_Target.GetComponentsInChildren<Animation>())
      componentsInChild.Stop();
  }

  public void ActivateHierarchy()
  {
    this.m_Target.SetActive(true);
  }

  public void DeactivateHierarchy()
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    this.m_Target.SetActive(false);
  }

  public void DestroyHierarchy()
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    Object.Destroy((Object) this.m_Target);
  }

  public void FadeIn(float FadeSec)
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    iTween.FadeTo(this.m_Target, 1f, FadeSec);
  }

  public void FadeOut(float FadeSec)
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    iTween.FadeTo(this.m_Target, 0.0f, FadeSec);
  }

  public void SetAlphaHierarchy(float alpha)
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    foreach (Renderer componentsInChild in this.m_Target.GetComponentsInChildren<Renderer>())
    {
      if (componentsInChild.material.HasProperty("_Color"))
      {
        Color color = componentsInChild.material.color;
        color.a = alpha;
        componentsInChild.material.color = color;
      }
    }
  }

  public void PlayDefaultSound()
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    if ((Object) this.m_Target.GetComponent<AudioSource>() == (Object) null)
      Debug.LogError((object) string.Format("TargetAnimUtils.PlayDefaultSound() - Tried to play the AudioSource on {0} but it has no AudioSource. You need an AudioSource to use this function.", (object) this.m_Target));
    else if ((Object) SoundManager.Get() == (Object) null)
      this.m_Target.GetComponent<AudioSource>().Play();
    else
      SoundManager.Get().Play(this.m_Target.GetComponent<AudioSource>(), true);
  }

  public void PlaySound(AudioClip clip)
  {
    if ((Object) this.m_Target == (Object) null)
      return;
    if ((Object) clip == (Object) null)
      Debug.LogError((object) string.Format("TargetAnimUtils.PlayDefaultSound() - No clip was given when trying to play the AudioSource on {0}. You need a clip to use this function.", (object) this.m_Target));
    else if ((Object) this.m_Target.GetComponent<AudioSource>() == (Object) null)
      Debug.LogError((object) string.Format("TargetAnimUtils.PlayDefaultSound() - Tried to play clip {0} on {1} but it has no AudioSource. You need an AudioSource to use this function.", (object) clip, (object) this.m_Target));
    else if ((Object) SoundManager.Get() == (Object) null)
      this.m_Target.GetComponent<AudioSource>().PlayOneShot(clip);
    else
      SoundManager.Get().PlayOneShot(this.m_Target.GetComponent<AudioSource>(), clip, 1f, true);
  }
}
