﻿// Decompiled with JetBrains decompiler
// Type: FatalErrorMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class FatalErrorMessage
{
  public bool m_allowClick = true;
  public string m_id;
  public string m_text;
  public Error.AcknowledgeCallback m_ackCallback;
  public object m_ackUserData;
  public bool m_redirectToStore;
  public float m_delayBeforeNextReset;
}
