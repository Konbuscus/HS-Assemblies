﻿// Decompiled with JetBrains decompiler
// Type: SoundDuckingDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[Serializable]
public class SoundDuckingDef
{
  public SoundCategory m_TriggerCategory;
  public List<SoundDuckedCategoryDef> m_DuckedCategoryDefs;

  public override string ToString()
  {
    return string.Format("[SoundDuckingDef: {0}]", (object) this.m_TriggerCategory);
  }
}
