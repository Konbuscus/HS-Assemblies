﻿// Decompiled with JetBrains decompiler
// Type: TutorialKeywordTooltip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TutorialKeywordTooltip : MonoBehaviour
{
  public UberText m_name;
  public UberText m_body;
  public PlayMakerFSM playMakerComponent;

  public void Initialize(string keywordName, string keywordText)
  {
    this.SetName(keywordName);
    this.SetBodyText(keywordText);
    this.StartCoroutine(this.WaitAFrameBeforeSendingEvent());
  }

  [DebuggerHidden]
  private IEnumerator WaitAFrameBeforeSendingEvent()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TutorialKeywordTooltip.\u003CWaitAFrameBeforeSendingEvent\u003Ec__Iterator202() { \u003C\u003Ef__this = this };
  }

  public void SetName(string s)
  {
    this.m_name.Text = s;
  }

  public void SetBodyText(string s)
  {
    this.m_body.Text = s;
  }

  public float GetHeight()
  {
    return this.GetComponent<Renderer>().bounds.size.z;
  }

  public float GetWidth()
  {
    return this.GetComponent<Renderer>().bounds.size.x;
  }
}
