﻿// Decompiled with JetBrains decompiler
// Type: Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Scene : MonoBehaviour
{
  protected virtual void Awake()
  {
    SceneMgr.Get().SetScene(this);
  }

  public virtual void PreUnload()
  {
  }

  public virtual bool IsUnloading()
  {
    return false;
  }

  public virtual void Unload()
  {
  }

  public virtual bool HandleKeyboardInput()
  {
    if (BackButton.backKey != KeyCode.None && Input.GetKeyUp(BackButton.backKey))
    {
      if (DialogManager.Get().ShowingDialog())
      {
        DialogManager.Get().GoBack();
        return true;
      }
      if (ChatMgr.Get().IsFriendListShowing() || ChatMgr.Get().IsChatLogFrameShown())
      {
        ChatMgr.Get().GoBack();
        return true;
      }
      if ((Object) OptionsMenu.Get() != (Object) null && OptionsMenu.Get().IsShown())
      {
        OptionsMenu.Get().Hide(true);
        return true;
      }
      if ((Object) GameMenu.Get() != (Object) null && GameMenu.Get().IsShown())
      {
        GameMenu.Get().Hide();
        return true;
      }
      if (Navigation.GoBack())
        return true;
    }
    return false;
  }

  public delegate void BackButtonPressedDelegate();
}
