﻿// Decompiled with JetBrains decompiler
// Type: CardBackDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CardBackDisplay : MonoBehaviour
{
  private bool m_FriendlySide = true;
  public Actor m_Actor;
  private CardBackManager m_CardBackManager;

  private void Start()
  {
    this.m_CardBackManager = CardBackManager.Get();
    if ((Object) this.m_CardBackManager == (Object) null)
    {
      UnityEngine.Debug.LogError((object) "Failed to get CardBackManager!");
      this.enabled = false;
    }
    this.UpdateCardBack();
  }

  public void UpdateCardBack()
  {
    if ((Object) this.m_CardBackManager == (Object) null)
      return;
    this.StartCoroutine(this.SetCardBackDisplay());
  }

  public void SetCardBack(bool friendlySide)
  {
    if ((Object) this.m_CardBackManager == (Object) null)
      this.m_CardBackManager = CardBackManager.Get();
    this.m_CardBackManager.UpdateCardBack(this.gameObject, friendlySide);
  }

  [DebuggerHidden]
  private IEnumerator SetCardBackDisplay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBackDisplay.\u003CSetCardBackDisplay\u003Ec__Iterator21() { \u003C\u003Ef__this = this };
  }
}
