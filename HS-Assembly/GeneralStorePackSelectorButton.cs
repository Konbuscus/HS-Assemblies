﻿// Decompiled with JetBrains decompiler
// Type: GeneralStorePackSelectorButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class GeneralStorePackSelectorButton : PegUIElement
{
  [CustomEditField(Parent = "m_checkNewPlayer")]
  public int m_recommendedExpertSetOwnedCardCount = 100;
  public UberText m_packText;
  public HighlightState m_highlight;
  public GameObject m_ribbonIndicator;
  public UberText m_ribbonIndicatorText;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_selectSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_unselectSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_mouseOverSound;
  public bool m_checkNewPlayer;
  private bool m_selected;
  private BoosterDbfRecord m_dbfRecord;
  private bool m_isLatestExpansion;

  public void SetBoosterId(BoosterDbId boosterId)
  {
    this.m_dbfRecord = GameDbf.Booster.GetRecord((int) boosterId);
    this.m_isLatestExpansion = GameUtils.IsBoosterLatestActiveExpansion((int) boosterId, this.IsPreorder());
    if (!((Object) this.m_packText != (Object) null))
      return;
    this.m_packText.Text = (string) this.m_dbfRecord.Name;
  }

  public BoosterDbId GetBoosterId()
  {
    if (this.m_dbfRecord == null)
      return BoosterDbId.INVALID;
    return (BoosterDbId) this.m_dbfRecord.ID;
  }

  public BoosterDbfRecord GetBooster()
  {
    return this.m_dbfRecord;
  }

  public void Select()
  {
    if (this.m_selected)
      return;
    this.m_selected = true;
    this.m_highlight.ChangeState(this.GetInteractionState() != PegUIElement.InteractionState.Up ? ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE : ActorStateType.HIGHLIGHT_SECONDARY_ACTIVE);
    if (string.IsNullOrEmpty(this.m_selectSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_selectSound));
  }

  public void Unselect()
  {
    if (!this.m_selected)
      return;
    this.m_selected = false;
    this.m_highlight.ChangeState(ActorStateType.NONE);
    if (string.IsNullOrEmpty(this.m_unselectSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_unselectSound));
  }

  public void UpdateRibbonIndicator(bool hideRibbon)
  {
    if ((Object) this.m_ribbonIndicator == (Object) null || this.GetBoosterId() == BoosterDbId.INVALID)
      return;
    if (hideRibbon)
    {
      this.m_ribbonIndicator.SetActive(false);
    }
    else
    {
      bool flag = false;
      if (StoreManager.IsFirstPurchaseBundleBooster((int) this.GetBoosterId()))
      {
        flag = true;
        this.m_ribbonIndicatorText.Text = GameStrings.Get("GLUE_STORE_PACKBUY_BEST_VALUE");
      }
      else if (this.IsRecommendedForNewPlayer() && StoreManager.IsFirstPurchaseBundleOwned())
      {
        flag = true;
        this.m_ribbonIndicatorText.Text = GameStrings.Get("GLUE_STORE_PACKBUY_SUGGESTION");
      }
      else if (this.IsPreorder())
      {
        flag = true;
        this.m_ribbonIndicatorText.Text = GameStrings.Get("GLUE_STORE_PACKS_PREORDER_TEXT");
      }
      else if (this.IsLatestExpansion())
      {
        flag = true;
        this.m_ribbonIndicatorText.Text = GameStrings.Get("GLUE_STORE_PACKS_LATEST_EXPANSION");
      }
      this.m_ribbonIndicator.SetActive(flag);
    }
  }

  public bool IsPurchasable()
  {
    List<Network.Bundle> bundleList = !StoreManager.IsFirstPurchaseBundleBooster((int) this.GetBoosterId()) ? StoreManager.Get().GetAllBundlesForProduct(ProductType.PRODUCT_TYPE_BOOSTER, GeneralStorePacksContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, (int) this.GetBoosterId(), 0) : StoreManager.Get().GetAllBundlesForProduct(ProductType.PRODUCT_TYPE_HIDDEN_LICENSE, GeneralStorePacksContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, 1, 0);
    if (bundleList != null)
      return bundleList.Count > 0;
    return false;
  }

  public bool IsRecommendedForNewPlayer()
  {
    return this.m_checkNewPlayer && GameUtils.GetBoosterCount(1) * 5 + CollectionManager.Get().NumCardsOwnedInSet(TAG_CARD_SET.EXPERT1) <= this.m_recommendedExpertSetOwnedCardCount;
  }

  public bool IsPreorder()
  {
    Network.Bundle preOrderBundle = (Network.Bundle) null;
    return StoreManager.Get().IsBoosterPreorderActive((int) this.GetBoosterId(), out preOrderBundle);
  }

  public bool IsLatestExpansion()
  {
    if (this.m_isLatestExpansion)
      return !this.IsPreorder();
    return false;
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    base.OnOver(oldState);
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_SECONDARY_ACTIVE);
    if (string.IsNullOrEmpty(this.m_mouseOverSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_mouseOverSound));
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    base.OnOut(oldState);
    this.m_highlight.ChangeState(!this.m_selected ? ActorStateType.NONE : ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }

  protected override void OnRelease()
  {
    base.OnRelease();
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_SECONDARY_ACTIVE);
  }

  protected override void OnPress()
  {
    base.OnPress();
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }
}
