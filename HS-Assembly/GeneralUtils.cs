﻿// Decompiled with JetBrains decompiler
// Type: GeneralUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using UnityEngine;

public static class GeneralUtils
{
  public const float DEVELOPMENT_BUILD_TEXT_WIDTH = 115f;

  public static void Swap<T>(ref T a, ref T b)
  {
    T obj = a;
    a = b;
    b = obj;
  }

  public static void ListSwap<T>(IList<T> list, int indexA, int indexB)
  {
    T obj = list[indexA];
    list[indexA] = list[indexB];
    list[indexB] = obj;
  }

  public static void ListMove<T>(IList<T> list, int srcIndex, int dstIndex)
  {
    if (srcIndex == dstIndex)
      return;
    T obj = list[srcIndex];
    list.RemoveAt(srcIndex);
    if (dstIndex > srcIndex)
      --dstIndex;
    list.Insert(dstIndex, obj);
  }

  public static T[] Combine<T>(T[] arr1, T[] arr2)
  {
    T[] objArray = new T[arr1.Length + arr2.Length];
    Array.Copy((Array) arr1, 0, (Array) objArray, 0, arr1.Length);
    Array.Copy((Array) arr2, 0, (Array) objArray, arr1.Length, arr2.Length);
    return objArray;
  }

  public static void Shuffle<T>(IList<T> arr)
  {
    for (int index = 0; index < arr.Count - 1; ++index)
    {
      int num = UnityEngine.Random.Range(0, arr.Count - index);
      T obj = arr[index];
      arr[index] = arr[index + num];
      arr[index + num] = obj;
    }
  }

  public static T[] Slice<T>(this T[] arr, int start, int end)
  {
    int length1 = arr.Length;
    if (start < 0)
      start = length1 + start;
    if (end < 0)
      end = length1 + end;
    int length2 = end - start;
    if (length2 <= 0)
      return new T[0];
    int num = length1 - start;
    if (length2 > num)
      length2 = num;
    T[] objArray = new T[length2];
    Array.Copy((Array) arr, start, (Array) objArray, 0, length2);
    return objArray;
  }

  public static T[] Slice<T>(this T[] arr, int start)
  {
    return arr.Slice<T>(start, arr.Length);
  }

  public static T[] Slice<T>(this T[] arr)
  {
    return arr.Slice<T>(0, arr.Length);
  }

  public static bool IsOverriddenMethod(MethodInfo childMethod, MethodInfo ancestorMethod)
  {
    if (childMethod == null || ancestorMethod == null || childMethod.Equals((object) ancestorMethod))
      return false;
    MethodInfo baseDefinition = childMethod.GetBaseDefinition();
    while (!baseDefinition.Equals((object) childMethod) && !baseDefinition.Equals((object) ancestorMethod))
    {
      MethodInfo methodInfo = baseDefinition;
      baseDefinition = baseDefinition.GetBaseDefinition();
      if (baseDefinition.Equals((object) methodInfo))
        return false;
    }
    return baseDefinition.Equals((object) ancestorMethod);
  }

  public static bool IsObjectAlive(object obj)
  {
    if (obj == null)
      return false;
    if ((object) (obj as UnityEngine.Object) == null)
      return true;
    return (bool) ((UnityEngine.Object) obj);
  }

  public static bool IsCallbackValid(Delegate callback)
  {
    bool flag = true;
    if ((object) callback == null)
      flag = false;
    else if (!callback.Method.IsStatic)
    {
      flag = GeneralUtils.IsObjectAlive(callback.Target);
      if (!flag)
        UnityEngine.Debug.LogError((object) string.Format("Target for callback {0} is null.", (object) callback.Method.Name));
    }
    return flag;
  }

  public static bool IsTestScene()
  {
    return (UnityEngine.Object) ApplicationMgr.Get() == (UnityEngine.Object) null;
  }

  public static bool IsEditorPlaying()
  {
    return false;
  }

  public static void ExitApplication()
  {
    Application.Quit();
  }

  public static bool IsDevelopmentBuildTextVisible()
  {
    return UnityEngine.Debug.isDebugBuild;
  }

  public static bool TryParseBool(string strVal, out bool boolVal)
  {
    if (bool.TryParse(strVal, out boolVal))
      return true;
    string str = strVal.ToLowerInvariant().Trim();
    if (str == "off" || str == "0" || str == "false")
    {
      boolVal = false;
      return true;
    }
    if (str == "on" || str == "1" || str == "true")
    {
      boolVal = true;
      return true;
    }
    boolVal = false;
    return false;
  }

  public static bool ForceBool(string strVal)
  {
    if (string.IsNullOrEmpty(strVal))
      return false;
    string str = strVal.ToLowerInvariant().Trim();
    return str == "on" || str == "1" || str == "true";
  }

  public static bool TryParseInt(string str, out int val)
  {
    return int.TryParse(str, NumberStyles.Any, (IFormatProvider) null, out val);
  }

  public static int ForceInt(string str)
  {
    int val = 0;
    GeneralUtils.TryParseInt(str, out val);
    return val;
  }

  public static bool TryParseLong(string str, out long val)
  {
    return long.TryParse(str, NumberStyles.Any, (IFormatProvider) null, out val);
  }

  public static long ForceLong(string str)
  {
    long val = 0;
    GeneralUtils.TryParseLong(str, out val);
    return val;
  }

  public static bool TryParseULong(string str, out ulong val)
  {
    return ulong.TryParse(str, NumberStyles.Any, (IFormatProvider) null, out val);
  }

  public static ulong ForceULong(string str)
  {
    ulong val = 0;
    GeneralUtils.TryParseULong(str, out val);
    return val;
  }

  public static bool TryParseFloat(string str, out float val)
  {
    return float.TryParse(str, NumberStyles.Any, (IFormatProvider) null, out val);
  }

  public static float ForceFloat(string str)
  {
    float val = 0.0f;
    GeneralUtils.TryParseFloat(str, out val);
    return val;
  }

  public static bool RandomBool()
  {
    return UnityEngine.Random.Range(0, 2) == 0;
  }

  public static float RandomSign()
  {
    return GeneralUtils.RandomBool() ? -1f : 1f;
  }

  public static int UnsignedMod(int x, int y)
  {
    int num = x % y;
    if (num < 0)
      num += y;
    return num;
  }

  public static bool IsEven(int n)
  {
    return (n & 1) == 0;
  }

  public static bool IsOdd(int n)
  {
    return (n & 1) == 1;
  }

  public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> func)
  {
    if (enumerable == null)
      return;
    foreach (T obj in enumerable)
      func(obj);
  }

  public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T, int> func)
  {
    if (enumerable == null)
      return;
    int num = 0;
    foreach (T obj in enumerable)
    {
      func(obj, num);
      ++num;
    }
  }

  public static void ForEachReassign<T>(this T[] array, Func<T, T> func)
  {
    if (array == null)
      return;
    for (int index = 0; index < array.Length; ++index)
      array[index] = func(array[index]);
  }

  public static void ForEachReassign<T>(this T[] array, Func<T, int, T> func)
  {
    if (array == null)
      return;
    for (int index = 0; index < array.Length; ++index)
      array[index] = func(array[index], index);
  }

  public static bool AreArraysEqual<T>(T[] arr1, T[] arr2)
  {
    if (arr1 == arr2)
      return true;
    if (arr1 == null || arr2 == null || arr1.Length != arr2.Length)
      return false;
    for (int index = 0; index < arr1.Length; ++index)
    {
      if (!arr1[index].Equals((object) arr2[index]))
        return false;
    }
    return true;
  }

  public static bool AreBytesEqual(byte[] bytes1, byte[] bytes2)
  {
    return GeneralUtils.AreArraysEqual<byte>(bytes1, bytes2);
  }

  public static T DeepClone<T>(T obj)
  {
    return (T) GeneralUtils.CloneValue((object) obj, ((object) obj).GetType());
  }

  private static object CloneClass(object obj, System.Type objType)
  {
    object newType = GeneralUtils.CreateNewType(objType);
    foreach (FieldInfo field in objType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
      field.SetValue(newType, GeneralUtils.CloneValue(field.GetValue(obj), field.FieldType));
    return newType;
  }

  private static object CloneValue(object src, System.Type type)
  {
    if (src != null && type != typeof (string) && type.IsClass)
    {
      if (!type.IsGenericType)
        return GeneralUtils.CloneClass(src, type);
      if (src is IDictionary)
      {
        IDictionary dictionary = src as IDictionary;
        IDictionary newType = GeneralUtils.CreateNewType(type) as IDictionary;
        System.Type genericArgument1 = type.GetGenericArguments()[0];
        System.Type genericArgument2 = type.GetGenericArguments()[1];
        foreach (DictionaryEntry dictionaryEntry in dictionary)
          newType.Add(GeneralUtils.CloneValue(dictionaryEntry.Key, genericArgument1), GeneralUtils.CloneValue(dictionaryEntry.Value, genericArgument2));
        return (object) newType;
      }
      if (src is IList)
      {
        IList list = src as IList;
        IList newType = GeneralUtils.CreateNewType(type) as IList;
        System.Type genericArgument = type.GetGenericArguments()[0];
        foreach (object src1 in (IEnumerable) list)
          newType.Add(GeneralUtils.CloneValue(src1, genericArgument));
        return (object) newType;
      }
    }
    return src;
  }

  private static object CreateNewType(System.Type type)
  {
    object instance = Activator.CreateInstance(type);
    if (instance == null)
      throw new SystemException(string.Format("Unable to instantiate type {0} with default constructor.", (object) type.Name));
    return instance;
  }

  public static void DeepReset<T>(T obj)
  {
    System.Type type = typeof (T);
    T instance = Activator.CreateInstance<T>();
    if ((object) instance == null)
      throw new SystemException(string.Format("Unable to instantiate type {0} with default constructor.", (object) type.Name));
    foreach (FieldInfo field in type.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
      field.SetValue((object) obj, field.GetValue((object) instance));
  }

  public static void CleanNullObjectsFromList<T>(List<T> list)
  {
    int index = 0;
    while (index < list.Count)
    {
      if ((object) list[index] == null)
        list.RemoveAt(index);
      else
        ++index;
    }
  }

  public static void CleanDeadObjectsFromList<T>(List<T> list) where T : Component
  {
    int index = 0;
    while (index < list.Count)
    {
      if ((bool) ((UnityEngine.Object) list[index]))
        ++index;
      else
        list.RemoveAt(index);
    }
  }

  public static void CleanDeadObjectsFromList(List<GameObject> list)
  {
    int index = 0;
    while (index < list.Count)
    {
      if ((bool) ((UnityEngine.Object) list[index]))
        ++index;
      else
        list.RemoveAt(index);
    }
  }

  public static string SafeFormat(string format, params object[] args)
  {
    return args.Length != 0 ? string.Format(format, args) : format;
  }

  public static string GetPatchDir()
  {
    string currentDirectory = Directory.GetCurrentDirectory();
    string str = currentDirectory.Substring(0, currentDirectory.LastIndexOf(Path.DirectorySeparatorChar));
    return str.Substring(0, str.LastIndexOf(Path.DirectorySeparatorChar));
  }

  public static Process RunPegasusCommonScriptWithParams(string scriptName, params string[] scriptParams)
  {
    try
    {
      string patchDir = GeneralUtils.GetPatchDir();
      string str1 = string.Join(" ", scriptParams);
      string str2 = "bat";
      string str3 = Path.Combine(Path.Combine(Path.Combine(patchDir, "Pegasus"), "Common"), string.Format("{0}.{1}", (object) scriptName, (object) str2));
      UnityEngine.Debug.LogFormat("Running command: {0} {1}", new object[2]
      {
        (object) str3,
        (object) str1
      });
      Process process = new Process() { StartInfo = { FileName = str3, Arguments = str1 } };
      process.Start();
      return process;
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogErrorFormat("Failed to run {0}: {1}", new object[2]
      {
        (object) scriptName,
        (object) ex.Message
      });
      return (Process) null;
    }
  }

  public static bool CompleteProcess(Process proc)
  {
    proc.WaitForExit();
    return GeneralUtils.LogCompletedProcess(proc);
  }

  public static bool CompleteProcess(Process proc, int millisecondTimout)
  {
    if (proc.WaitForExit(millisecondTimout))
      return GeneralUtils.LogCompletedProcess(proc);
    UnityEngine.Debug.LogError((object) (Path.GetFileNameWithoutExtension(proc.StartInfo.FileName) + " timed out after " + (object) millisecondTimout + "milliseconds"));
    return false;
  }

  public static bool LogCompletedProcess(Process proc)
  {
    return proc.ExitCode == 0;
  }

  public static void CopyToClipboard(string copyText)
  {
    GUIUtility.systemCopyBuffer = copyText;
  }
}
