﻿// Decompiled with JetBrains decompiler
// Type: SilentOrbit.ProtocolBuffers.ThreadUnsafeStack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;

namespace SilentOrbit.ProtocolBuffers
{
  public class ThreadUnsafeStack : IDisposable, MemoryStreamStack
  {
    private Stack<MemoryStream> stack = new Stack<MemoryStream>();

    public MemoryStream Pop()
    {
      if (this.stack.Count == 0)
        return new MemoryStream();
      return this.stack.Pop();
    }

    public void Push(MemoryStream stream)
    {
      this.stack.Push(stream);
    }

    public void Dispose()
    {
      this.stack.Clear();
    }
  }
}
