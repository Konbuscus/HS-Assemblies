﻿// Decompiled with JetBrains decompiler
// Type: SilentOrbit.ProtocolBuffers.ProtocolBufferException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

namespace SilentOrbit.ProtocolBuffers
{
  public class ProtocolBufferException : Exception
  {
    public ProtocolBufferException(string message)
      : base(message)
    {
    }
  }
}
