﻿// Decompiled with JetBrains decompiler
// Type: SilentOrbit.ProtocolBuffers.KeyValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace SilentOrbit.ProtocolBuffers
{
  public class KeyValue
  {
    public Key Key { get; set; }

    public byte[] Value { get; set; }

    public KeyValue(Key key, byte[] value)
    {
      this.Key = key;
      this.Value = value;
    }

    public override string ToString()
    {
      return string.Format("[KeyValue: {0}, {1}, {2} bytes]", (object) this.Key.Field, (object) this.Key.WireType, (object) this.Value.Length);
    }
  }
}
