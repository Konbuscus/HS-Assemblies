﻿// Decompiled with JetBrains decompiler
// Type: SilentOrbit.ProtocolBuffers.StreamRead
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;

namespace SilentOrbit.ProtocolBuffers
{
  [Obsolete("Renamed to PositionStream")]
  public class StreamRead : PositionStream
  {
    public StreamRead(Stream baseStream)
      : base(baseStream)
    {
    }
  }
}
