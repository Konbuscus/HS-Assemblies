﻿// Decompiled with JetBrains decompiler
// Type: SilentOrbit.ProtocolBuffers.PositionStream
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;

namespace SilentOrbit.ProtocolBuffers
{
  public class PositionStream : Stream
  {
    private Stream stream;

    public int BytesRead { get; private set; }

    public override bool CanRead
    {
      get
      {
        return true;
      }
    }

    public override bool CanSeek
    {
      get
      {
        return false;
      }
    }

    public override bool CanWrite
    {
      get
      {
        return false;
      }
    }

    public override long Length
    {
      get
      {
        return this.stream.Length;
      }
    }

    public override long Position
    {
      get
      {
        return (long) this.BytesRead;
      }
      set
      {
        throw new NotImplementedException();
      }
    }

    public PositionStream(Stream baseStream)
    {
      this.stream = baseStream;
    }

    public override void Flush()
    {
      throw new NotImplementedException();
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      int num = this.stream.Read(buffer, offset, count);
      this.BytesRead += num;
      return num;
    }

    public override int ReadByte()
    {
      int num = this.stream.ReadByte();
      ++this.BytesRead;
      return num;
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
      throw new NotImplementedException();
    }

    public override void SetLength(long value)
    {
      throw new NotImplementedException();
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
      throw new NotImplementedException();
    }

    public override void Close()
    {
      base.Close();
    }

    protected override void Dispose(bool disposing)
    {
      this.stream.Dispose();
      base.Dispose(disposing);
    }
  }
}
