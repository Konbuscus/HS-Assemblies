﻿// Decompiled with JetBrains decompiler
// Type: SilentOrbit.ProtocolBuffers.AllocationStack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;

namespace SilentOrbit.ProtocolBuffers
{
  public class AllocationStack : IDisposable, MemoryStreamStack
  {
    public MemoryStream Pop()
    {
      return new MemoryStream();
    }

    public void Push(MemoryStream stream)
    {
    }

    public void Dispose()
    {
    }
  }
}
