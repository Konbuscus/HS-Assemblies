﻿// Decompiled with JetBrains decompiler
// Type: SilentOrbit.ProtocolBuffers.Key
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace SilentOrbit.ProtocolBuffers
{
  public class Key
  {
    public uint Field { get; set; }

    public Wire WireType { get; set; }

    public Key(uint field, Wire wireType)
    {
      this.Field = field;
      this.WireType = wireType;
    }

    public override string ToString()
    {
      return string.Format("[Key: {0}, {1}]", (object) this.Field, (object) this.WireType);
    }
  }
}
