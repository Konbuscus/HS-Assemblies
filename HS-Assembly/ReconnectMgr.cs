﻿// Decompiled with JetBrains decompiler
// Type: ReconnectMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs.types;
using PegasusShared;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ReconnectMgr : MonoBehaviour
{
  private Map<GameType, string> m_gameTypeNameKeys = new Map<GameType, string>() { { GameType.GT_VS_FRIEND, "GLUE_RECONNECT_GAME_TYPE_FRIENDLY" }, { GameType.GT_ARENA, "GLUE_RECONNECT_GAME_TYPE_ARENA" }, { GameType.GT_RANKED, "GLUE_RECONNECT_GAME_TYPE_RANKED" }, { GameType.GT_CASUAL, "GLUE_RECONNECT_GAME_TYPE_UNRANKED" }, { GameType.GT_TAVERNBRAWL, "GLUE_RECONNECT_GAME_TYPE_TAVERN_BRAWL" } };
  private ReconnectMgr.SavedStartGameParameters m_savedStartGameParams = new ReconnectMgr.SavedStartGameParameters();
  private List<ReconnectMgr.TimeoutListener> m_timeoutListeners = new List<ReconnectMgr.TimeoutListener>();
  private static ReconnectMgr s_instance;
  private AlertPopup m_dialog;
  private ReconnectType m_reconnectType;
  private float m_reconnectStartTimestamp;
  private float m_retryStartTimestamp;
  private NetCache.ProfileNoticeDisconnectedGame m_pendingReconnectNotice;

  private void Awake()
  {
    ReconnectMgr.s_instance = this;
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
  }

  private void Start()
  {
    ApplicationMgr.Get().WillReset += new System.Action(this.WillReset);
  }

  private void OnDestroy()
  {
    GameMgr.Get().UnregisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    ApplicationMgr.Get().WillReset -= new System.Action(this.WillReset);
    ReconnectMgr.s_instance = (ReconnectMgr) null;
  }

  private void Update()
  {
    this.CheckReconnectTimeout();
  }

  public static ReconnectMgr Get()
  {
    return ReconnectMgr.s_instance;
  }

  public bool IsReconnectEnabled()
  {
    if (ApplicationMgr.IsPublic())
      return true;
    return Options.Get().GetBool(Option.RECONNECT);
  }

  public bool IsReconnecting()
  {
    return this.m_reconnectType != ReconnectType.INVALID;
  }

  public bool IsStartingReconnectGame()
  {
    return GameMgr.Get().IsReconnect() && (SceneMgr.Get().GetNextMode() == SceneMgr.Mode.GAMEPLAY || SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY && !SceneMgr.Get().IsSceneLoaded());
  }

  public float GetTimeout()
  {
    if (ApplicationMgr.IsInternal())
      return Options.Get().GetFloat(Option.RECONNECT_TIMEOUT);
    return (float) OptionDataTables.s_defaultsMap[Option.RECONNECT_TIMEOUT];
  }

  public float GetRetryTime()
  {
    if (ApplicationMgr.IsInternal())
      return Options.Get().GetFloat(Option.RECONNECT_RETRY_TIME);
    return (float) OptionDataTables.s_defaultsMap[Option.RECONNECT_RETRY_TIME];
  }

  public bool AddTimeoutListener(ReconnectMgr.TimeoutCallback callback)
  {
    return this.AddTimeoutListener(callback, (object) null);
  }

  public bool AddTimeoutListener(ReconnectMgr.TimeoutCallback callback, object userData)
  {
    ReconnectMgr.TimeoutListener timeoutListener = new ReconnectMgr.TimeoutListener();
    timeoutListener.SetCallback(callback);
    timeoutListener.SetUserData(userData);
    if (this.m_timeoutListeners.Contains(timeoutListener))
      return false;
    this.m_timeoutListeners.Add(timeoutListener);
    return true;
  }

  public bool RemoveTimeoutListener(ReconnectMgr.TimeoutCallback callback)
  {
    return this.RemoveTimeoutListener(callback, (object) null);
  }

  public bool RemoveTimeoutListener(ReconnectMgr.TimeoutCallback callback, object userData)
  {
    ReconnectMgr.TimeoutListener timeoutListener = new ReconnectMgr.TimeoutListener();
    timeoutListener.SetCallback(callback);
    timeoutListener.SetUserData(userData);
    return this.m_timeoutListeners.Remove(timeoutListener);
  }

  public bool ReconnectFromLogin()
  {
    NetCache.ProfileNoticeDisconnectedGame dcGameNotice = this.GetDCGameNotice();
    if (dcGameNotice == null || !this.IsReconnectEnabled())
      return false;
    if (dcGameNotice.GameResult != ProfileNoticeDisconnectedGameResult.GameResult.GR_PLAYING)
    {
      this.OnGameResult(dcGameNotice);
      return false;
    }
    if (dcGameNotice.GameType == GameType.GT_UNKNOWN)
      return false;
    this.m_pendingReconnectNotice = dcGameNotice;
    this.StartReconnecting(ReconnectType.LOGIN);
    NetCache.Get().RegisterReconnectMgr(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    return true;
  }

  public bool ReconnectFromGameplay()
  {
    if (!this.IsReconnectEnabled())
      return false;
    GameServerInfo gameServerJoined = Network.Get().GetLastGameServerJoined();
    if (gameServerJoined == null)
    {
      Debug.LogError((object) "serverInfo in ReconnectMgr.ReconnectFromGameplay is null and should not be!");
      return false;
    }
    if (!gameServerJoined.Resumable)
      return false;
    this.HideDialog();
    GameType gameType = GameMgr.Get().GetGameType();
    FormatType formatType = GameMgr.Get().GetFormatType();
    ReconnectType reconnectType = ReconnectType.GAMEPLAY;
    this.StartReconnecting(reconnectType);
    this.StartGame(gameType, formatType, reconnectType, gameServerJoined);
    return true;
  }

  public bool ShowDisconnectedGameResult(NetCache.ProfileNoticeDisconnectedGame dcGame)
  {
    if (!GameUtils.IsMatchmadeGameType(dcGame.GameType))
      return false;
    TimeSpan timeSpan = DateTime.UtcNow - DateTime.FromFileTimeUtc(dcGame.Date);
    Log.ReturningPlayer.Print("This user disconnected from his or her last game {0} minutes ago.", (object) timeSpan.TotalMinutes);
    if (timeSpan.TotalHours > 24.0)
    {
      Log.All.Print("Not showing the Disconnected Game Result because the game was disconnected from {0} hours ago.", (object) timeSpan.TotalHours);
      return false;
    }
    switch (dcGame.GameResult)
    {
      case ProfileNoticeDisconnectedGameResult.GameResult.GR_WINNER:
      case ProfileNoticeDisconnectedGameResult.GameResult.GR_TIE:
        if (dcGame.GameType == GameType.GT_UNKNOWN)
          return false;
        AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
        info.m_headerText = GameStrings.Get("GLUE_RECONNECT_RESULT_HEADER");
        string key;
        if (dcGame.GameResult == ProfileNoticeDisconnectedGameResult.GameResult.GR_TIE)
        {
          key = "GLUE_RECONNECT_RESULT_TIE";
        }
        else
        {
          switch (dcGame.YourResult)
          {
            case ProfileNoticeDisconnectedGameResult.PlayerResult.PR_WON:
              key = "GLUE_RECONNECT_RESULT_WIN";
              break;
            case ProfileNoticeDisconnectedGameResult.PlayerResult.PR_LOST:
            case ProfileNoticeDisconnectedGameResult.PlayerResult.PR_QUIT:
              key = "GLUE_RECONNECT_RESULT_LOSE";
              break;
            case ProfileNoticeDisconnectedGameResult.PlayerResult.PR_DISCONNECTED:
              key = "GLUE_RECONNECT_RESULT_DISCONNECT";
              break;
            default:
              Debug.LogError((object) string.Format("ReconnectMgr.ShowDisconnectedGameResult() - unhandled player result {0}", (object) dcGame.YourResult));
              return false;
          }
        }
        info.m_text = GameStrings.Format(key, (object) this.GetGameTypeName(dcGame.GameType, dcGame.MissionId));
        info.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
        info.m_showAlertIcon = true;
        DialogManager.Get().ShowPopup(info);
        return true;
      default:
        Debug.LogError((object) string.Format("ReconnectMgr.ShowDisconnectedGameResult() - unhandled game result {0}", (object) dcGame.GameResult));
        return false;
    }
  }

  private string GetGameTypeName(GameType gameType, int missionId)
  {
    AdventureDbfRecord adventureRecord = GameUtils.GetAdventureRecord(missionId);
    if (adventureRecord != null)
    {
      switch (adventureRecord.ID)
      {
        case 1:
          return GameStrings.Get("GLUE_RECONNECT_GAME_TYPE_TUTORIAL");
        case 2:
          return GameStrings.Get("GLUE_RECONNECT_GAME_TYPE_PRACTICE");
        case 3:
          return GameStrings.Get("GLUE_RECONNECT_GAME_TYPE_NAXXRAMAS");
        case 4:
          return GameStrings.Get("GLUE_RECONNECT_GAME_TYPE_BRM");
        case 7:
          return GameStrings.Get("GLUE_RECONNECT_GAME_TYPE_TAVERN_BRAWL");
        default:
          return (string) adventureRecord.Name;
      }
    }
    else
    {
      string key;
      if (this.m_gameTypeNameKeys.TryGetValue(gameType, out key))
        return GameStrings.Get(key);
      Error.AddDevFatal("ReconnectMgr.GetGameTypeName() - no name for mission {0} gameType {1}", (object) missionId, (object) gameType);
      return string.Empty;
    }
  }

  private void WillReset()
  {
    this.m_dialog = (AlertPopup) null;
    this.ClearReconnectData();
    this.m_timeoutListeners.Clear();
    this.m_pendingReconnectNotice = (NetCache.ProfileNoticeDisconnectedGame) null;
  }

  private void StartReconnecting(ReconnectType reconnectType)
  {
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    this.m_reconnectType = reconnectType;
    this.m_reconnectStartTimestamp = realtimeSinceStartup;
    this.m_retryStartTimestamp = realtimeSinceStartup;
    this.ShowReconnectingDialog();
  }

  private void CheckReconnectTimeout()
  {
    if (!this.IsReconnecting())
      return;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if ((double) (realtimeSinceStartup - this.m_reconnectStartTimestamp) >= (double) this.GetTimeout())
    {
      this.OnReconnectTimeout();
    }
    else
    {
      if (Network.IsConnectedToGameServer() || Network.GameServerHasEvents() || (double) (realtimeSinceStartup - this.m_retryStartTimestamp) < (double) this.GetRetryTime())
        return;
      this.m_retryStartTimestamp = realtimeSinceStartup;
      this.StartGame();
    }
  }

  private void OnReconnectTimeout()
  {
    this.ClearReconnectData();
    this.ChangeDialogToTimeout();
    if (this.m_pendingReconnectNotice != null)
    {
      this.AckNotice(this.m_pendingReconnectNotice);
      this.m_pendingReconnectNotice = (NetCache.ProfileNoticeDisconnectedGame) null;
    }
    this.FireTimeoutEvent();
  }

  private void OnGameResult(NetCache.ProfileNoticeDisconnectedGame dcGameNotice)
  {
    this.ShowDisconnectedGameResult(dcGameNotice);
    this.AckNotice(dcGameNotice);
  }

  private void ClearReconnectData()
  {
    this.m_reconnectType = ReconnectType.INVALID;
    this.m_reconnectStartTimestamp = 0.0f;
    this.m_retryStartTimestamp = 0.0f;
  }

  private void ShowReconnectingDialog()
  {
    AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
    info.m_headerText = GameStrings.Get("GLOBAL_RECONNECT_RECONNECTING_HEADER");
    info.m_text = this.m_reconnectType == ReconnectType.LOGIN ? GameStrings.Get("GLOBAL_RECONNECT_RECONNECTING_LOGIN") : GameStrings.Get("GLOBAL_RECONNECT_RECONNECTING");
    if ((bool) ApplicationMgr.CanQuitGame)
    {
      info.m_responseDisplay = AlertPopup.ResponseDisplay.CANCEL;
      info.m_cancelText = GameStrings.Get("GLOBAL_RECONNECT_EXIT_BUTTON");
    }
    else
      info.m_responseDisplay = AlertPopup.ResponseDisplay.NONE;
    info.m_showAlertIcon = true;
    info.m_responseCallback = new AlertPopup.ResponseCallback(this.OnReconnectingDialogResponse);
    DialogManager.Get().ShowPopup(info, new DialogManager.DialogProcessCallback(this.OnReconnectingDialogProcessed));
  }

  private bool OnReconnectingDialogProcessed(DialogBase dialog, object userData)
  {
    if (!this.IsReconnecting())
      return false;
    this.m_dialog = (AlertPopup) dialog;
    if (this.IsStartingReconnectGame())
      this.ChangeDialogToReconnected();
    return true;
  }

  private void OnReconnectingDialogResponse(AlertPopup.Response response, object userData)
  {
    this.m_dialog = (AlertPopup) null;
    ApplicationMgr.Get().Exit();
  }

  private void ChangeDialogToReconnected()
  {
    if ((UnityEngine.Object) this.m_dialog == (UnityEngine.Object) null)
      return;
    this.m_dialog.UpdateInfo(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_RECONNECT_RECONNECTED_HEADER"),
      m_text = this.m_reconnectType == ReconnectType.LOGIN ? GameStrings.Get("GLOBAL_RECONNECT_RECONNECTED_LOGIN") : GameStrings.Get("GLOBAL_RECONNECT_RECONNECTED"),
      m_responseDisplay = AlertPopup.ResponseDisplay.NONE,
      m_showAlertIcon = true
    });
    LoadingScreen.Get().RegisterPreviousSceneDestroyedListener(new LoadingScreen.PreviousSceneDestroyedCallback(this.OnPreviousSceneDestroyed));
  }

  private void OnPreviousSceneDestroyed(object userData)
  {
    LoadingScreen.Get().UnregisterPreviousSceneDestroyedListener(new LoadingScreen.PreviousSceneDestroyedCallback(this.OnPreviousSceneDestroyed));
    this.HideDialog();
  }

  private void ChangeDialogToTimeout()
  {
    if ((UnityEngine.Object) this.m_dialog == (UnityEngine.Object) null)
      return;
    this.m_dialog.UpdateInfo(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_RECONNECT_TIMEOUT_HEADER"),
      m_text = GameStrings.Get("GLOBAL_RECONNECT_TIMEOUT"),
      m_responseDisplay = AlertPopup.ResponseDisplay.OK,
      m_showAlertIcon = true,
      m_responseCallback = new AlertPopup.ResponseCallback(this.OnTimeoutDialogResponse)
    });
  }

  private void OnTimeoutDialogResponse(AlertPopup.Response response, object userData)
  {
    this.m_dialog = (AlertPopup) null;
    if (Network.IsLoggedIn())
      return;
    if ((bool) ApplicationMgr.AllowResetFromFatalError)
      ApplicationMgr.Get().Reset();
    else
      ApplicationMgr.Get().Exit();
  }

  private void HideDialog()
  {
    if ((UnityEngine.Object) this.m_dialog == (UnityEngine.Object) null)
      return;
    this.m_dialog.Hide();
    this.m_dialog = (AlertPopup) null;
  }

  private NetCache.ProfileNoticeDisconnectedGame GetDCGameNotice()
  {
    NetCache.NetCacheProfileNotices netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileNotices>();
    if (netObject == null || netObject.Notices == null || netObject.Notices.Count == 0)
      return (NetCache.ProfileNoticeDisconnectedGame) null;
    NetCache.ProfileNoticeDisconnectedGame disconnectedGame1 = (NetCache.ProfileNoticeDisconnectedGame) null;
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = netObject.Notices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.ProfileNoticeDisconnectedGame current = enumerator.Current as NetCache.ProfileNoticeDisconnectedGame;
        if (current != null)
        {
          if (disconnectedGame1 == null)
            disconnectedGame1 = current;
          else if (current.NoticeID > disconnectedGame1.NoticeID)
            disconnectedGame1 = current;
        }
      }
    }
    List<NetCache.ProfileNoticeDisconnectedGame> disconnectedGameList = new List<NetCache.ProfileNoticeDisconnectedGame>();
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = netObject.Notices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.ProfileNotice current = enumerator.Current;
        NetCache.ProfileNoticeDisconnectedGame disconnectedGame2 = current as NetCache.ProfileNoticeDisconnectedGame;
        if (disconnectedGame2 != null && current.NoticeID != disconnectedGame1.NoticeID)
          disconnectedGameList.Add(disconnectedGame2);
      }
    }
    using (List<NetCache.ProfileNoticeDisconnectedGame>.Enumerator enumerator = disconnectedGameList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.AckNotice(enumerator.Current);
    }
    return disconnectedGame1;
  }

  private void OnNetCacheReady()
  {
    GameType gameType = this.m_pendingReconnectNotice.GameType;
    if (gameType == GameType.GT_UNKNOWN)
      return;
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    ReconnectType reconnectType = ReconnectType.LOGIN;
    NetCache.NetCacheDisconnectedGame netObject = NetCache.Get().GetNetObject<NetCache.NetCacheDisconnectedGame>();
    if (netObject == null || netObject.ServerInfo == null)
      this.OnReconnectTimeout();
    else
      this.StartGame(gameType, this.m_pendingReconnectNotice.FormatType, reconnectType, netObject.ServerInfo);
  }

  private void AckNotice(NetCache.ProfileNoticeDisconnectedGame notice)
  {
    Network.AckNotice(notice.NoticeID);
  }

  private void StartGame(GameType gameType, FormatType formatType, ReconnectType reconnectType, GameServerInfo serverInfo)
  {
    this.m_savedStartGameParams.GameType = gameType;
    this.m_savedStartGameParams.FormatType = formatType;
    this.m_savedStartGameParams.ReconnectType = reconnectType;
    this.m_savedStartGameParams.ServerInfo = serverInfo;
    this.StartGame();
  }

  private void StartGame()
  {
    GameMgr.Get().ReconnectGame(this.m_savedStartGameParams.GameType, this.m_savedStartGameParams.FormatType, this.m_savedStartGameParams.ReconnectType, this.m_savedStartGameParams.ServerInfo);
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    switch (eventData.m_state)
    {
      case FindGameState.SERVER_GAME_STARTED:
        if (this.IsReconnecting())
        {
          this.m_timeoutListeners.Clear();
          this.ChangeDialogToReconnected();
          this.ClearReconnectData();
          this.m_pendingReconnectNotice = (NetCache.ProfileNoticeDisconnectedGame) null;
          break;
        }
        break;
      case FindGameState.SERVER_GAME_CANCELED:
        if (this.IsReconnecting())
        {
          this.OnReconnectTimeout();
          return true;
        }
        break;
    }
    return false;
  }

  private void FireTimeoutEvent()
  {
    ReconnectMgr.TimeoutListener[] array = this.m_timeoutListeners.ToArray();
    this.m_timeoutListeners.Clear();
    bool flag = false;
    for (int index = 0; index < array.Length; ++index)
      flag = array[index].Fire() || flag;
    if (flag || !Network.IsLoggedIn())
      return;
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
  }

  private void OnFatalError(FatalErrorMessage message, object userData)
  {
    this.ClearReconnectData();
  }

  private class TimeoutListener : EventListener<ReconnectMgr.TimeoutCallback>
  {
    public bool Fire()
    {
      return this.m_callback(this.m_userData);
    }
  }

  private class SavedStartGameParameters
  {
    public GameType GameType;
    public FormatType FormatType;
    public ReconnectType ReconnectType;
    public GameServerInfo ServerInfo;
  }

  public delegate bool TimeoutCallback(object userData);
}
