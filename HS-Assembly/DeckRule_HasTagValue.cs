﻿// Decompiled with JetBrains decompiler
// Type: DeckRule_HasTagValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class DeckRule_HasTagValue : DeckRule
{
  public DeckRule_HasTagValue(DeckRulesetRuleDbfRecord record)
    : base(DeckRule.RuleType.HAS_TAG_VALUE, record)
  {
  }

  public override bool Filter(EntityDef def)
  {
    if (!this.AppliesTo(def.GetCardId()))
      return true;
    int tag = def.GetTag(this.m_tag);
    bool val = true;
    if (tag < this.m_tagMinValue || tag > this.m_tagMaxValue)
      val = false;
    return this.GetResult(val);
  }

  public override bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason)
  {
    return this.DefaultYes(out reason);
  }
}
