﻿// Decompiled with JetBrains decompiler
// Type: StoreAdventureDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class StoreAdventureDef : MonoBehaviour
{
  public List<string> m_previewCards = new List<string>();
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_storeButtonPrefab;
  [CustomEditField(T = EditType.TEXTURE)]
  public string m_logoTextureName;
  public Material m_keyArt;
  public int m_preorderCardBackId;
  public string m_preorderCardBackTextName;
  [CustomEditField(T = EditType.TEXTURE)]
  public string m_accentTextureName;
  public MusicPlaylistType m_playlist;
}
