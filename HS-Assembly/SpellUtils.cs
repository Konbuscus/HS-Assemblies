﻿// Decompiled with JetBrains decompiler
// Type: SpellUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpellUtils
{
  public static SpellClassTag ConvertClassTagToSpellEnum(TAG_CLASS classTag)
  {
    switch (classTag)
    {
      case TAG_CLASS.DEATHKNIGHT:
        return SpellClassTag.DEATHKNIGHT;
      case TAG_CLASS.DRUID:
        return SpellClassTag.DRUID;
      case TAG_CLASS.HUNTER:
        return SpellClassTag.HUNTER;
      case TAG_CLASS.MAGE:
        return SpellClassTag.MAGE;
      case TAG_CLASS.PALADIN:
        return SpellClassTag.PALADIN;
      case TAG_CLASS.PRIEST:
        return SpellClassTag.PRIEST;
      case TAG_CLASS.ROGUE:
        return SpellClassTag.ROGUE;
      case TAG_CLASS.SHAMAN:
        return SpellClassTag.SHAMAN;
      case TAG_CLASS.WARLOCK:
        return SpellClassTag.WARLOCK;
      case TAG_CLASS.WARRIOR:
        return SpellClassTag.WARRIOR;
      default:
        return SpellClassTag.NONE;
    }
  }

  public static Player.Side ConvertSpellSideToPlayerSide(Spell spell, SpellPlayerSide spellSide)
  {
    Entity entity = spell.GetSourceCard().GetEntity();
    switch (spellSide)
    {
      case SpellPlayerSide.FRIENDLY:
        return Player.Side.FRIENDLY;
      case SpellPlayerSide.OPPONENT:
        return Player.Side.OPPOSING;
      case SpellPlayerSide.SOURCE:
        return entity.IsControlledByFriendlySidePlayer() ? Player.Side.FRIENDLY : Player.Side.OPPOSING;
      case SpellPlayerSide.TARGET:
        return entity.IsControlledByFriendlySidePlayer() ? Player.Side.OPPOSING : Player.Side.FRIENDLY;
      default:
        return Player.Side.NEUTRAL;
    }
  }

  public static List<Zone> FindZonesFromTag(SpellZoneTag zoneTag)
  {
    ZoneMgr zoneMgr = ZoneMgr.Get();
    if ((UnityEngine.Object) zoneMgr == (UnityEngine.Object) null)
      return (List<Zone>) null;
    switch (zoneTag)
    {
      case SpellZoneTag.PLAY:
        return zoneMgr.FindZonesOfType<Zone, ZonePlay>();
      case SpellZoneTag.HERO:
        return zoneMgr.FindZonesOfType<Zone, ZoneHero>();
      case SpellZoneTag.HERO_POWER:
        return zoneMgr.FindZonesOfType<Zone, ZoneHeroPower>();
      case SpellZoneTag.WEAPON:
        return zoneMgr.FindZonesOfType<Zone, ZoneWeapon>();
      case SpellZoneTag.DECK:
        return zoneMgr.FindZonesOfType<Zone, ZoneDeck>();
      case SpellZoneTag.HAND:
        return zoneMgr.FindZonesOfType<Zone, ZoneHand>();
      case SpellZoneTag.GRAVEYARD:
        return zoneMgr.FindZonesOfType<Zone, ZoneGraveyard>();
      case SpellZoneTag.SECRET:
        return zoneMgr.FindZonesOfType<Zone, ZoneSecret>();
      default:
        Debug.LogWarning((object) string.Format("SpellUtils.FindZonesFromTag() - unhandled zoneTag {0}", (object) zoneTag));
        return (List<Zone>) null;
    }
  }

  public static List<Zone> FindZonesFromTag(Spell spell, SpellZoneTag zoneTag, SpellPlayerSide spellSide)
  {
    if ((UnityEngine.Object) ZoneMgr.Get() == (UnityEngine.Object) null)
      return (List<Zone>) null;
    if (spellSide == SpellPlayerSide.NEUTRAL)
      return (List<Zone>) null;
    if (spellSide == SpellPlayerSide.BOTH)
      return SpellUtils.FindZonesFromTag(zoneTag);
    Player.Side playerSide = SpellUtils.ConvertSpellSideToPlayerSide(spell, spellSide);
    switch (zoneTag)
    {
      case SpellZoneTag.PLAY:
        return ZoneMgr.Get().FindZonesOfType<Zone, ZonePlay>(playerSide);
      case SpellZoneTag.HERO:
        return ZoneMgr.Get().FindZonesOfType<Zone, ZoneHero>(playerSide);
      case SpellZoneTag.HERO_POWER:
        return ZoneMgr.Get().FindZonesOfType<Zone, ZoneHeroPower>(playerSide);
      case SpellZoneTag.WEAPON:
        return ZoneMgr.Get().FindZonesOfType<Zone, ZoneWeapon>(playerSide);
      case SpellZoneTag.DECK:
        return ZoneMgr.Get().FindZonesOfType<Zone, ZoneDeck>(playerSide);
      case SpellZoneTag.HAND:
        return ZoneMgr.Get().FindZonesOfType<Zone, ZoneHand>(playerSide);
      case SpellZoneTag.GRAVEYARD:
        return ZoneMgr.Get().FindZonesOfType<Zone, ZoneGraveyard>(playerSide);
      case SpellZoneTag.SECRET:
        return ZoneMgr.Get().FindZonesOfType<Zone, ZoneSecret>(playerSide);
      default:
        Debug.LogWarning((object) string.Format("SpellUtils.FindZonesFromTag() - Unhandled zoneTag {0}. spellSide={1} playerSide={2}", (object) zoneTag, (object) spellSide, (object) playerSide));
        return (List<Zone>) null;
    }
  }

  public static Transform GetLocationTransform(Spell spell)
  {
    GameObject locationObject = SpellUtils.GetLocationObject(spell);
    if ((UnityEngine.Object) locationObject == (UnityEngine.Object) null)
      return (Transform) null;
    return locationObject.transform;
  }

  public static GameObject GetLocationObject(Spell spell)
  {
    SpellLocation location = spell.GetLocation();
    return SpellUtils.GetSpellLocationObject(spell, location, (string) null);
  }

  public static GameObject GetSpellLocationObject(Spell spell, SpellLocation location, string overrideTransformName = null)
  {
    if (location == SpellLocation.NONE)
      return (GameObject) null;
    GameObject parentObject = (GameObject) null;
    if (location == SpellLocation.SOURCE)
      parentObject = spell.GetSource();
    else if (location == SpellLocation.SOURCE_AUTO)
      parentObject = SpellUtils.FindSourceAutoObjectForSpell(spell);
    else if (location == SpellLocation.SOURCE_HERO)
    {
      Card heroCard = SpellUtils.FindHeroCard(spell.GetSourceCard());
      if ((UnityEngine.Object) heroCard == (UnityEngine.Object) null)
        return (GameObject) null;
      parentObject = heroCard.gameObject;
    }
    else if (location == SpellLocation.SOURCE_HERO_POWER)
    {
      Card heroPowerCard = SpellUtils.FindHeroPowerCard(spell.GetSourceCard());
      if ((UnityEngine.Object) heroPowerCard == (UnityEngine.Object) null)
        return (GameObject) null;
      parentObject = heroPowerCard.gameObject;
    }
    else if (location == SpellLocation.SOURCE_PLAY_ZONE)
    {
      Card sourceCard = spell.GetSourceCard();
      if ((UnityEngine.Object) sourceCard == (UnityEngine.Object) null)
        return (GameObject) null;
      ZonePlay zoneOfType = ZoneMgr.Get().FindZoneOfType<ZonePlay>(sourceCard.GetEntity().GetController().GetSide());
      if ((UnityEngine.Object) zoneOfType == (UnityEngine.Object) null)
        return (GameObject) null;
      parentObject = zoneOfType.gameObject;
    }
    else if (location == SpellLocation.TARGET)
      parentObject = spell.GetVisualTarget();
    else if (location == SpellLocation.TARGET_AUTO)
      parentObject = SpellUtils.FindTargetAutoObjectForSpell(spell);
    else if (location == SpellLocation.TARGET_HERO)
    {
      Card heroCard = SpellUtils.FindHeroCard(spell.GetVisualTargetCard());
      if ((UnityEngine.Object) heroCard == (UnityEngine.Object) null)
        return (GameObject) null;
      parentObject = heroCard.gameObject;
    }
    else if (location == SpellLocation.TARGET_HERO_POWER)
    {
      Card heroPowerCard = SpellUtils.FindHeroPowerCard(spell.GetVisualTargetCard());
      if ((UnityEngine.Object) heroPowerCard == (UnityEngine.Object) null)
        return (GameObject) null;
      parentObject = heroPowerCard.gameObject;
    }
    else if (location == SpellLocation.TARGET_PLAY_ZONE)
    {
      Card visualTargetCard = spell.GetVisualTargetCard();
      if ((UnityEngine.Object) visualTargetCard == (UnityEngine.Object) null)
        return (GameObject) null;
      ZonePlay zoneOfType = ZoneMgr.Get().FindZoneOfType<ZonePlay>(visualTargetCard.GetEntity().GetController().GetSide());
      if ((UnityEngine.Object) zoneOfType == (UnityEngine.Object) null)
        return (GameObject) null;
      parentObject = zoneOfType.gameObject;
    }
    else if (location == SpellLocation.BOARD)
    {
      if ((UnityEngine.Object) Board.Get() == (UnityEngine.Object) null)
        return (GameObject) null;
      parentObject = Board.Get().gameObject;
    }
    else if (location == SpellLocation.FRIENDLY_HERO)
    {
      Player friendlyPlayer = SpellUtils.FindFriendlyPlayer(spell);
      if (friendlyPlayer == null)
        return (GameObject) null;
      Card heroCard = friendlyPlayer.GetHeroCard();
      if (!(bool) ((UnityEngine.Object) heroCard))
        return (GameObject) null;
      parentObject = heroCard.gameObject;
    }
    else if (location == SpellLocation.FRIENDLY_HERO_POWER)
    {
      Player friendlyPlayer = SpellUtils.FindFriendlyPlayer(spell);
      if (friendlyPlayer == null)
        return (GameObject) null;
      Card heroPowerCard = friendlyPlayer.GetHeroPowerCard();
      if (!(bool) ((UnityEngine.Object) heroPowerCard))
        return (GameObject) null;
      parentObject = heroPowerCard.gameObject;
    }
    else if (location == SpellLocation.FRIENDLY_PLAY_ZONE)
    {
      ZonePlay friendlyPlayZone = SpellUtils.FindFriendlyPlayZone(spell);
      if (!(bool) ((UnityEngine.Object) friendlyPlayZone))
        return (GameObject) null;
      parentObject = friendlyPlayZone.gameObject;
    }
    else if (location == SpellLocation.OPPONENT_HERO)
    {
      Player opponentPlayer = SpellUtils.FindOpponentPlayer(spell);
      if (opponentPlayer == null)
        return (GameObject) null;
      Card heroCard = opponentPlayer.GetHeroCard();
      if (!(bool) ((UnityEngine.Object) heroCard))
        return (GameObject) null;
      parentObject = heroCard.gameObject;
    }
    else if (location == SpellLocation.OPPONENT_HERO_POWER)
    {
      Player opponentPlayer = SpellUtils.FindOpponentPlayer(spell);
      if (opponentPlayer == null)
        return (GameObject) null;
      Card heroPowerCard = opponentPlayer.GetHeroPowerCard();
      if (!(bool) ((UnityEngine.Object) heroPowerCard))
        return (GameObject) null;
      parentObject = heroPowerCard.gameObject;
    }
    else if (location == SpellLocation.OPPONENT_PLAY_ZONE)
    {
      ZonePlay opponentPlayZone = SpellUtils.FindOpponentPlayZone(spell);
      if (!(bool) ((UnityEngine.Object) opponentPlayZone))
        return (GameObject) null;
      parentObject = opponentPlayZone.gameObject;
    }
    else if (location == SpellLocation.CHOSEN_TARGET)
    {
      Card powerTargetCard = spell.GetPowerTargetCard();
      if ((UnityEngine.Object) powerTargetCard == (UnityEngine.Object) null)
        return (GameObject) null;
      parentObject = powerTargetCard.gameObject;
    }
    else if (location == SpellLocation.FRIENDLY_HAND_ZONE)
    {
      Player friendlyPlayer = SpellUtils.FindFriendlyPlayer(spell);
      if (friendlyPlayer == null)
        return (GameObject) null;
      ZoneHand zoneOfType = ZoneMgr.Get().FindZoneOfType<ZoneHand>(friendlyPlayer.GetSide());
      if (!(bool) ((UnityEngine.Object) zoneOfType))
        return (GameObject) null;
      parentObject = zoneOfType.gameObject;
    }
    else if (location == SpellLocation.OPPONENT_HAND_ZONE)
    {
      Player opponentPlayer = SpellUtils.FindOpponentPlayer(spell);
      if (opponentPlayer == null)
        return (GameObject) null;
      ZoneHand zoneOfType = ZoneMgr.Get().FindZoneOfType<ZoneHand>(opponentPlayer.GetSide());
      if (!(bool) ((UnityEngine.Object) zoneOfType))
        return (GameObject) null;
      parentObject = zoneOfType.gameObject;
    }
    else if (location == SpellLocation.FRIENDLY_DECK_ZONE)
    {
      Player friendlyPlayer = SpellUtils.FindFriendlyPlayer(spell);
      if (friendlyPlayer == null)
        return (GameObject) null;
      ZoneDeck zoneOfType = ZoneMgr.Get().FindZoneOfType<ZoneDeck>(friendlyPlayer.GetSide());
      if (!(bool) ((UnityEngine.Object) zoneOfType))
        return (GameObject) null;
      parentObject = zoneOfType.gameObject;
    }
    else if (location == SpellLocation.OPPONENT_DECK_ZONE)
    {
      Player opponentPlayer = SpellUtils.FindOpponentPlayer(spell);
      if (opponentPlayer == null)
        return (GameObject) null;
      ZoneDeck zoneOfType = ZoneMgr.Get().FindZoneOfType<ZoneDeck>(opponentPlayer.GetSide());
      if (!(bool) ((UnityEngine.Object) zoneOfType))
        return (GameObject) null;
      parentObject = zoneOfType.gameObject;
    }
    if ((UnityEngine.Object) parentObject == (UnityEngine.Object) null)
      return (GameObject) null;
    if (string.IsNullOrEmpty(overrideTransformName))
      overrideTransformName = spell.GetLocationTransformName();
    if (!string.IsNullOrEmpty(overrideTransformName))
    {
      GameObject childBySubstring = SceneUtils.FindChildBySubstring(parentObject, overrideTransformName);
      if ((UnityEngine.Object) childBySubstring != (UnityEngine.Object) null)
        return childBySubstring;
    }
    return parentObject;
  }

  public static bool SetPositionFromLocation(Spell spell)
  {
    return SpellUtils.SetPositionFromLocation(spell, false);
  }

  public static bool SetPositionFromLocation(Spell spell, bool setParent)
  {
    Transform locationTransform = SpellUtils.GetLocationTransform(spell);
    if ((UnityEngine.Object) locationTransform == (UnityEngine.Object) null)
      return false;
    if (setParent)
      spell.transform.parent = locationTransform;
    spell.transform.position = locationTransform.position;
    return true;
  }

  public static bool SetOrientationFromFacing(Spell spell)
  {
    SpellFacing facing = spell.GetFacing();
    if (facing == SpellFacing.NONE)
      return false;
    SpellFacingOptions options = spell.GetFacingOptions() ?? new SpellFacingOptions();
    if (facing == SpellFacing.SAME_AS_SOURCE)
    {
      GameObject source = spell.GetSource();
      if ((UnityEngine.Object) source == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceSameAs((Component) spell, source, options);
    }
    else if (facing == SpellFacing.SAME_AS_SOURCE_AUTO)
    {
      GameObject autoObjectForSpell = SpellUtils.FindSourceAutoObjectForSpell(spell);
      if ((UnityEngine.Object) autoObjectForSpell == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceSameAs((Component) spell, autoObjectForSpell, options);
    }
    else if (facing == SpellFacing.SAME_AS_SOURCE_HERO)
    {
      Card heroCard = SpellUtils.FindHeroCard(spell.GetSourceCard());
      if ((UnityEngine.Object) heroCard == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceSameAs((Component) spell, (Component) heroCard, options);
    }
    else if (facing == SpellFacing.TOWARDS_SOURCE)
    {
      GameObject source = spell.GetSource();
      if ((UnityEngine.Object) source == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceTowards((Component) spell, source, options);
    }
    else if (facing == SpellFacing.TOWARDS_SOURCE_AUTO)
    {
      GameObject autoObjectForSpell = SpellUtils.FindSourceAutoObjectForSpell(spell);
      if ((UnityEngine.Object) autoObjectForSpell == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceTowards((Component) spell, autoObjectForSpell, options);
    }
    else if (facing == SpellFacing.TOWARDS_SOURCE_HERO)
    {
      Card heroCard = SpellUtils.FindHeroCard(spell.GetSourceCard());
      if ((UnityEngine.Object) heroCard == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceTowards((Component) spell, (Component) heroCard, options);
    }
    else if (facing == SpellFacing.TOWARDS_TARGET)
    {
      GameObject visualTarget = spell.GetVisualTarget();
      if ((UnityEngine.Object) visualTarget == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceTowards((Component) spell, visualTarget, options);
    }
    else if (facing == SpellFacing.TOWARDS_TARGET_HERO)
    {
      Card heroCard = SpellUtils.FindHeroCard(SpellUtils.FindBestTargetCard(spell));
      if ((UnityEngine.Object) heroCard == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceTowards((Component) spell, (Component) heroCard, options);
    }
    else if (facing == SpellFacing.TOWARDS_CHOSEN_TARGET)
    {
      Card powerTargetCard = spell.GetPowerTargetCard();
      if ((UnityEngine.Object) powerTargetCard == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceTowards((Component) spell, (Component) powerTargetCard, options);
    }
    else if (facing == SpellFacing.OPPOSITE_OF_SOURCE)
    {
      GameObject source = spell.GetSource();
      if ((UnityEngine.Object) source == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceOppositeOf((Component) spell, source, options);
    }
    else if (facing == SpellFacing.OPPOSITE_OF_SOURCE_AUTO)
    {
      GameObject autoObjectForSpell = SpellUtils.FindSourceAutoObjectForSpell(spell);
      if ((UnityEngine.Object) autoObjectForSpell == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceOppositeOf((Component) spell, autoObjectForSpell, options);
    }
    else if (facing == SpellFacing.OPPOSITE_OF_SOURCE_HERO)
    {
      Card heroCard = SpellUtils.FindHeroCard(spell.GetSourceCard());
      if ((UnityEngine.Object) heroCard == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceOppositeOf((Component) spell, (Component) heroCard, options);
    }
    else
    {
      if (facing != SpellFacing.TOWARDS_OPPONENT_HERO)
        return false;
      Card opponentHeroCard = SpellUtils.FindOpponentHeroCard(spell);
      if ((UnityEngine.Object) opponentHeroCard == (UnityEngine.Object) null)
        return false;
      SpellUtils.FaceTowards((Component) spell, (Component) opponentHeroCard, options);
    }
    return true;
  }

  public static Player FindFriendlyPlayer(Spell spell)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
      return (Player) null;
    Card sourceCard = spell.GetSourceCard();
    if ((UnityEngine.Object) sourceCard == (UnityEngine.Object) null)
      return (Player) null;
    return sourceCard.GetEntity().GetController();
  }

  public static Player FindOpponentPlayer(Spell spell)
  {
    Player friendlyPlayer = SpellUtils.FindFriendlyPlayer(spell);
    if (friendlyPlayer == null)
      return (Player) null;
    return GameState.Get().GetFirstOpponentPlayer(friendlyPlayer);
  }

  public static ZonePlay FindFriendlyPlayZone(Spell spell)
  {
    Player friendlyPlayer = SpellUtils.FindFriendlyPlayer(spell);
    if (friendlyPlayer == null)
      return (ZonePlay) null;
    return ZoneMgr.Get().FindZoneOfType<ZonePlay>(friendlyPlayer.GetSide());
  }

  public static ZonePlay FindOpponentPlayZone(Spell spell)
  {
    Player opponentPlayer = SpellUtils.FindOpponentPlayer(spell);
    if (opponentPlayer == null)
      return (ZonePlay) null;
    return ZoneMgr.Get().FindZoneOfType<ZonePlay>(opponentPlayer.GetSide());
  }

  public static Card FindOpponentHeroCard(Spell spell)
  {
    Player opponentPlayer = SpellUtils.FindOpponentPlayer(spell);
    if (opponentPlayer == null)
      return (Card) null;
    return opponentPlayer.GetHeroCard();
  }

  public static Zone FindTargetZone(Spell spell)
  {
    Card targetCard = spell.GetTargetCard();
    if ((UnityEngine.Object) targetCard == (UnityEngine.Object) null)
      return (Zone) null;
    return ZoneMgr.Get().FindZoneForEntity(targetCard.GetEntity());
  }

  public static Actor GetParentActor(Spell spell)
  {
    return SceneUtils.FindComponentInThisOrParents<Actor>(spell.gameObject);
  }

  public static GameObject GetParentRootObject(Spell spell)
  {
    Actor parentActor = SpellUtils.GetParentActor(spell);
    if ((UnityEngine.Object) parentActor == (UnityEngine.Object) null)
      return (GameObject) null;
    return parentActor.GetRootObject();
  }

  public static MeshRenderer GetParentRootObjectMesh(Spell spell)
  {
    Actor parentActor = SpellUtils.GetParentActor(spell);
    if ((UnityEngine.Object) parentActor == (UnityEngine.Object) null)
      return (MeshRenderer) null;
    return parentActor.GetMeshRenderer();
  }

  public static bool IsNonMetaTaskListInMetaBlock(PowerTaskList taskList)
  {
    return taskList.DoesBlockHaveEffectTimingMetaData() && !taskList.HasEffectTimingMetaData();
  }

  public static bool CanAddPowerTargets(PowerTaskList taskList)
  {
    return !SpellUtils.IsNonMetaTaskListInMetaBlock(taskList) && (taskList.HasTasks() || taskList.IsEndOfBlock());
  }

  public static void SetCustomSpellParent(Spell spell, Component c)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null || (UnityEngine.Object) c == (UnityEngine.Object) null)
      return;
    spell.transform.parent = c.transform;
    spell.transform.localPosition = Vector3.zero;
  }

  public static void SetupSpell(Spell spell, Component c)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null || (UnityEngine.Object) c == (UnityEngine.Object) null)
      return;
    spell.SetSource(c.gameObject);
  }

  public static void SetupSoundSpell(CardSoundSpell spell, Component c)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null || (UnityEngine.Object) c == (UnityEngine.Object) null)
      return;
    spell.SetSource(c.gameObject);
    spell.transform.parent = c.transform;
    TransformUtil.Identity((Component) spell.transform);
  }

  public static bool ActivateBirthIfNecessary(Spell spell)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
      return false;
    switch (spell.GetActiveState())
    {
      case SpellStateType.BIRTH:
        return false;
      case SpellStateType.IDLE:
        return false;
      default:
        spell.ActivateState(SpellStateType.BIRTH);
        return true;
    }
  }

  public static bool ActivateDeathIfNecessary(Spell spell)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
      return false;
    switch (spell.GetActiveState())
    {
      case SpellStateType.DEATH:
        return false;
      case SpellStateType.NONE:
        return false;
      default:
        spell.ActivateState(SpellStateType.DEATH);
        return true;
    }
  }

  public static bool ActivateCancelIfNecessary(Spell spell)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
      return false;
    switch (spell.GetActiveState())
    {
      case SpellStateType.CANCEL:
        return false;
      case SpellStateType.NONE:
        return false;
      default:
        spell.ActivateState(SpellStateType.CANCEL);
        return true;
    }
  }

  public static void PurgeSpell(Spell spell)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null || !spell.CanPurge())
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
  }

  public static void PurgeSpells<T>(List<T> spells) where T : Spell
  {
    if (spells == null || spells.Count == 0)
      return;
    for (int index = 0; index < spells.Count; ++index)
      SpellUtils.PurgeSpell((Spell) spells[index]);
  }

  private static GameObject FindSourceAutoObjectForSpell(Spell spell)
  {
    GameObject source = spell.GetSource();
    Card sourceCard = spell.GetSourceCard();
    if ((UnityEngine.Object) sourceCard == (UnityEngine.Object) null)
      return source;
    Entity entity = sourceCard.GetEntity();
    TAG_CARDTYPE cardType = entity.GetCardType();
    PowerTaskList powerTaskList = spell.GetPowerTaskList();
    if (powerTaskList != null)
    {
      EntityDef effectEntityDef = powerTaskList.GetEffectEntityDef();
      if (effectEntityDef != null)
        cardType = effectEntityDef.GetCardType();
    }
    return SpellUtils.FindAutoObjectForSpell(entity, sourceCard, cardType);
  }

  private static GameObject FindTargetAutoObjectForSpell(Spell spell)
  {
    GameObject visualTarget = spell.GetVisualTarget();
    if ((UnityEngine.Object) visualTarget == (UnityEngine.Object) null)
      return (GameObject) null;
    Card component = visualTarget.GetComponent<Card>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return visualTarget;
    Entity entity = component.GetEntity();
    TAG_CARDTYPE cardType = entity.GetCardType();
    return SpellUtils.FindAutoObjectForSpell(entity, component, cardType);
  }

  private static GameObject FindAutoObjectForSpell(Entity entity, Card card, TAG_CARDTYPE cardType)
  {
    if (cardType == TAG_CARDTYPE.SPELL)
    {
      Card heroCard = entity.GetController().GetHeroCard();
      if ((UnityEngine.Object) heroCard == (UnityEngine.Object) null)
        return card.gameObject;
      return heroCard.gameObject;
    }
    if (cardType != TAG_CARDTYPE.HERO_POWER)
      return card.gameObject;
    Card heroPowerCard = entity.GetController().GetHeroPowerCard();
    if ((UnityEngine.Object) heroPowerCard == (UnityEngine.Object) null)
      return card.gameObject;
    return heroPowerCard.gameObject;
  }

  private static Card FindBestTargetCard(Spell spell)
  {
    Card sourceCard = spell.GetSourceCard();
    if ((UnityEngine.Object) sourceCard == (UnityEngine.Object) null)
      return spell.GetVisualTargetCard();
    Player controller = sourceCard.GetEntity().GetController();
    if (controller == null)
      return spell.GetVisualTargetCard();
    Player.Side side = controller.GetSide();
    List<GameObject> visualTargets = spell.GetVisualTargets();
    for (int index = 0; index < visualTargets.Count; ++index)
    {
      Card component = visualTargets[index].GetComponent<Card>();
      if (!((UnityEngine.Object) component == (UnityEngine.Object) null) && component.GetEntity().GetController().GetSide() != side)
        return component;
    }
    return spell.GetVisualTargetCard();
  }

  private static Card FindHeroCard(Card card)
  {
    if ((UnityEngine.Object) card == (UnityEngine.Object) null)
      return (Card) null;
    Player controller = card.GetEntity().GetController();
    if (controller == null)
      return (Card) null;
    return controller.GetHeroCard();
  }

  private static Card FindHeroPowerCard(Card card)
  {
    if ((UnityEngine.Object) card == (UnityEngine.Object) null)
      return (Card) null;
    Player controller = card.GetEntity().GetController();
    if (controller == null)
      return (Card) null;
    return controller.GetHeroPowerCard();
  }

  private static void FaceSameAs(GameObject source, GameObject target, SpellFacingOptions options)
  {
    SpellUtils.FaceSameAs(source.transform, target.transform, options);
  }

  private static void FaceSameAs(GameObject source, Component target, SpellFacingOptions options)
  {
    SpellUtils.FaceSameAs(source.transform, target.transform, options);
  }

  private static void FaceSameAs(Component source, GameObject target, SpellFacingOptions options)
  {
    SpellUtils.FaceSameAs(source.transform, target.transform, options);
  }

  private static void FaceSameAs(Component source, Component target, SpellFacingOptions options)
  {
    SpellUtils.FaceSameAs(source.transform, target.transform, options);
  }

  private static void FaceSameAs(Transform source, Transform target, SpellFacingOptions options)
  {
    SpellUtils.SetOrientation(source, target.position, target.position + target.forward, options);
  }

  private static void FaceOppositeOf(GameObject source, GameObject target, SpellFacingOptions options)
  {
    SpellUtils.FaceOppositeOf(source.transform, target.transform, options);
  }

  private static void FaceOppositeOf(GameObject source, Component target, SpellFacingOptions options)
  {
    SpellUtils.FaceOppositeOf(source.transform, target.transform, options);
  }

  private static void FaceOppositeOf(Component source, GameObject target, SpellFacingOptions options)
  {
    SpellUtils.FaceOppositeOf(source.transform, target.transform, options);
  }

  private static void FaceOppositeOf(Component source, Component target, SpellFacingOptions options)
  {
    SpellUtils.FaceOppositeOf(source.transform, target.transform, options);
  }

  private static void FaceOppositeOf(Transform source, Transform target, SpellFacingOptions options)
  {
    SpellUtils.SetOrientation(source, target.position, target.position - target.forward, options);
  }

  private static void FaceTowards(GameObject source, GameObject target, SpellFacingOptions options)
  {
    SpellUtils.FaceTowards(source.transform, target.transform, options);
  }

  private static void FaceTowards(GameObject source, Component target, SpellFacingOptions options)
  {
    SpellUtils.FaceTowards(source.transform, target.transform, options);
  }

  private static void FaceTowards(Component source, GameObject target, SpellFacingOptions options)
  {
    SpellUtils.FaceTowards(source.transform, target.transform, options);
  }

  private static void FaceTowards(Component source, Component target, SpellFacingOptions options)
  {
    SpellUtils.FaceTowards(source.transform, target.transform, options);
  }

  private static void FaceTowards(Transform source, Transform target, SpellFacingOptions options)
  {
    SpellUtils.SetOrientation(source, source.position, target.position, options);
  }

  private static void SetOrientation(Transform source, Vector3 sourcePosition, Vector3 targetPosition, SpellFacingOptions options)
  {
    if (!options.m_RotateX || !options.m_RotateY)
    {
      if (options.m_RotateX)
      {
        targetPosition.x = sourcePosition.x;
      }
      else
      {
        if (!options.m_RotateY)
          return;
        targetPosition.y = sourcePosition.y;
      }
    }
    Vector3 forward = targetPosition - sourcePosition;
    if ((double) forward.sqrMagnitude <= (double) Mathf.Epsilon)
      return;
    source.rotation = Quaternion.LookRotation(forward);
  }

  public static T GetAppropriateElementAccordingToRanges<T>(T[] elements, Func<T, ValueRange> rangeAccessor, int desiredValue)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SpellUtils.\u003CGetAppropriateElementAccordingToRanges\u003Ec__AnonStorey45A<T> rangesCAnonStorey45A = new SpellUtils.\u003CGetAppropriateElementAccordingToRanges\u003Ec__AnonStorey45A<T>();
    // ISSUE: reference to a compiler-generated field
    rangesCAnonStorey45A.rangeAccessor = rangeAccessor;
    if (elements.Length == 0)
      return default (T);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    rangesCAnonStorey45A.maxHandledValue = ((IEnumerable<T>) elements).Max<T>(new Func<T, int>(rangesCAnonStorey45A.\u003C\u003Em__326));
    // ISSUE: reference to a compiler-generated field
    if (desiredValue > rangesCAnonStorey45A.maxHandledValue)
    {
      // ISSUE: reference to a compiler-generated method
      return ((IEnumerable<T>) elements).First<T>(new Func<T, bool>(rangesCAnonStorey45A.\u003C\u003Em__327));
    }
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    rangesCAnonStorey45A.minHandledValue = ((IEnumerable<T>) elements).Min<T>(new Func<T, int>(rangesCAnonStorey45A.\u003C\u003Em__328));
    // ISSUE: reference to a compiler-generated field
    if (desiredValue < rangesCAnonStorey45A.minHandledValue)
    {
      // ISSUE: reference to a compiler-generated method
      return ((IEnumerable<T>) elements).First<T>(new Func<T, bool>(rangesCAnonStorey45A.\u003C\u003Em__329));
    }
    for (int index = 0; index < elements.Length; ++index)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      if (desiredValue >= rangesCAnonStorey45A.rangeAccessor(elements[index]).m_minValue && desiredValue <= rangesCAnonStorey45A.rangeAccessor(elements[index]).m_maxValue)
        return elements[index];
    }
    return default (T);
  }
}
