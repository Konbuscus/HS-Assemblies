﻿// Decompiled with JetBrains decompiler
// Type: ParticleEffects
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ParticleEffects : MonoBehaviour
{
  public List<ParticleSystem> m_ParticleSystems;
  public bool m_WorldSpace;
  public ParticleEffectsOrientation m_ParticleOrientation;
  public List<ParticleEffectsAttractor> m_ParticleAttractors;
  public List<ParticleEffectsRepulser> m_ParticleRepulsers;

  private void Update()
  {
    if (this.m_ParticleSystems == null)
      return;
    if (this.m_ParticleSystems.Count == 0)
    {
      ParticleSystem component = this.GetComponent<ParticleSystem>();
      if ((Object) component == (Object) null)
        this.enabled = false;
      this.m_ParticleSystems.Add(component);
    }
    for (int index = 0; index < this.m_ParticleSystems.Count; ++index)
    {
      ParticleSystem particleSystem = this.m_ParticleSystems[index];
      if (!((Object) particleSystem == (Object) null))
      {
        int particleCount = particleSystem.particleCount;
        if (particleCount == 0)
          break;
        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[particleCount];
        particleSystem.GetParticles(particles);
        if (this.m_ParticleAttractors != null)
          this.ParticleAttractor(particleSystem, particles, particleCount);
        if (this.m_ParticleRepulsers != null)
          this.ParticleRepulser(particleSystem, particles, particleCount);
        if (this.m_ParticleOrientation != null && this.m_ParticleOrientation.m_OrientToDirection)
          this.OrientParticlesToDirection(particleSystem, particles, particleCount);
        particleSystem.SetParticles(particles, particleCount);
      }
    }
  }

  private void OnDrawGizmos()
  {
    if (this.m_ParticleAttractors != null)
    {
      using (List<ParticleEffectsAttractor>.Enumerator enumerator = this.m_ParticleAttractors.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ParticleEffectsAttractor current = enumerator.Current;
          if (!((Object) current.m_Transform == (Object) null))
          {
            Gizmos.color = Color.green;
            float radius = current.m_Radius * (float) (((double) current.m_Transform.lossyScale.x + (double) current.m_Transform.lossyScale.y + (double) current.m_Transform.lossyScale.z) * 0.333000004291534);
            Gizmos.DrawWireSphere(current.m_Transform.position, radius);
          }
        }
      }
    }
    if (this.m_ParticleRepulsers == null)
      return;
    using (List<ParticleEffectsRepulser>.Enumerator enumerator = this.m_ParticleRepulsers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ParticleEffectsRepulser current = enumerator.Current;
        if (!((Object) current.m_Transform == (Object) null))
        {
          Gizmos.color = Color.red;
          float radius = current.m_Radius * (float) (((double) current.m_Transform.lossyScale.x + (double) current.m_Transform.lossyScale.y + (double) current.m_Transform.lossyScale.z) * 0.333000004291534);
          Gizmos.DrawWireSphere(current.m_Transform.position, radius);
        }
      }
    }
  }

  private void OrientParticlesToDirection(ParticleSystem particleSystem, ParticleSystem.Particle[] particles, int particleCount)
  {
    for (int index = 0; index < particleCount; ++index)
    {
      particles[index].angularVelocity = 0.0f;
      Vector3 targetVector = particles[index].velocity;
      if (!this.m_WorldSpace)
        targetVector = particleSystem.transform.TransformDirection(particles[index].velocity);
      if (this.m_ParticleOrientation.m_UpVector == ParticleEffectsOrientUpVectors.Horizontal)
        particles[index].rotation = ParticleEffects.VectorAngle(Vector3.forward, targetVector, Vector3.up);
      else if (this.m_ParticleOrientation.m_UpVector == ParticleEffectsOrientUpVectors.Vertical)
        particles[index].rotation = ParticleEffects.VectorAngle(Vector3.up, targetVector, Vector3.forward);
    }
  }

  private void ParticleAttractor(ParticleSystem particleSystem, ParticleSystem.Particle[] particles, int particleCount)
  {
    for (int index = 0; index < particleCount; ++index)
    {
      using (List<ParticleEffectsAttractor>.Enumerator enumerator = this.m_ParticleAttractors.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ParticleEffectsAttractor current = enumerator.Current;
          if (!((Object) current.m_Transform == (Object) null) && (double) current.m_Radius > 0.0 && (double) current.m_Power > 0.0)
          {
            Vector3 vector3_1 = particles[index].position;
            if (!this.m_WorldSpace)
              vector3_1 = particleSystem.transform.TransformPoint(particles[index].position);
            Vector3 vector3_2 = current.m_Transform.position - vector3_1;
            float num1 = current.m_Radius * (float) (((double) current.m_Transform.lossyScale.x + (double) current.m_Transform.lossyScale.y + (double) current.m_Transform.lossyScale.z) * 0.333000004291534);
            float num2 = (float) (1.0 - (double) vector3_2.magnitude / (double) num1) * current.m_Power;
            Vector3 b = vector3_2 * particles[index].velocity.magnitude;
            if (!this.m_WorldSpace)
              b = particleSystem.transform.InverseTransformDirection(vector3_2 * particles[index].velocity.magnitude);
            Vector3 vector3_3 = Vector3.Lerp(particles[index].velocity, b, num2 * Time.deltaTime).normalized * particles[index].velocity.magnitude;
            particles[index].velocity = vector3_3;
          }
        }
      }
    }
  }

  private void ParticleRepulser(ParticleSystem particleSystem, ParticleSystem.Particle[] particles, int particleCount)
  {
    for (int index = 0; index < particleCount; ++index)
    {
      using (List<ParticleEffectsRepulser>.Enumerator enumerator = this.m_ParticleRepulsers.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ParticleEffectsRepulser current = enumerator.Current;
          if (!((Object) current.m_Transform == (Object) null) && (double) current.m_Radius > 0.0 && (double) current.m_Power > 0.0)
          {
            Vector3 vector3_1 = particles[index].position;
            if (!this.m_WorldSpace)
              vector3_1 = particleSystem.transform.TransformPoint(particles[index].position);
            Vector3 vector3_2 = current.m_Transform.position - vector3_1;
            float num1 = current.m_Radius * (float) (((double) current.m_Transform.lossyScale.x + (double) current.m_Transform.lossyScale.y + (double) current.m_Transform.lossyScale.z) * 0.333000004291534);
            float num2 = (float) (1.0 - (double) vector3_2.magnitude / (double) num1) * current.m_Power + current.m_Power;
            Vector3 b = -vector3_2 * particles[index].velocity.magnitude;
            if (!this.m_WorldSpace)
              b = particleSystem.transform.InverseTransformDirection(-vector3_2 * particles[index].velocity.magnitude);
            Vector3 vector3_3 = Vector3.Lerp(particles[index].velocity, b, num2 * Time.deltaTime).normalized * particles[index].velocity.magnitude;
            particles[index].velocity = vector3_3;
          }
        }
      }
    }
  }

  private static float VectorAngle(Vector3 forwardVector, Vector3 targetVector, Vector3 upVector)
  {
    float num = Vector3.Angle(forwardVector, targetVector);
    if ((double) Vector3.Dot(Vector3.Cross(forwardVector, targetVector), upVector) < 0.0)
      return 360f - num;
    return num;
  }
}
