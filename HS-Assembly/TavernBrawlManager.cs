﻿// Decompiled with JetBrains decompiler
// Type: TavernBrawlManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusClient;
using PegasusShared;
using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEngine;

public class TavernBrawlManager
{
  public const int MINIMUM_CLASS_LEVEL = 20;
  private const float DEFAULT_REFRESH_SPEC_SLUSH_SECONDS_MIN = 0.0f;
  private const float DEFAULT_REFRESH_SPEC_SLUSH_SECONDS_MAX = 120f;
  private const int REWARD_REFRESH_PADDING_SECONDS = 2;
  private const int REWARD_REFRESH_RANDOM_DELAY_MIN = 0;
  private const int REWARD_REFRESH_RANDOM_DELAY_MAX = 30;
  private static TavernBrawlManager s_instance;
  private TavernBrawlMission m_currentMission;
  private bool m_scenarioAssetPendingLoad;
  private DateTime? m_scheduledRefreshTimeLocal;
  private DateTime? m_nextSeasonStartDateLocal;
  private List<TavernBrawlManager.CallbackEnsureServerDataReady> m_serverDataReadyCallbacks;
  private bool m_hasGottenClientOptionsAtLeastOnce;
  private bool m_isFirstTimeSeeingThisFeature;
  private bool m_isFirstTimeSeeingCurrentSeason;

  public bool IsTavernBrawlActive
  {
    get
    {
      if (this.CurrentMission() != null)
        return this.CurrentTavernBrawlSeasonEndInSeconds > 0L;
      return false;
    }
  }

  public bool CanChallengeToTavernBrawl
  {
    get
    {
      return this.IsTavernBrawlActive && this.m_currentMission != null && (!GameUtils.IsAIMission(this.m_currentMission.missionId) && !this.m_currentMission.friendlyChallengeDisabled);
    }
  }

  public bool HasUnlockedTavernBrawl
  {
    get
    {
      NetCache.NetCacheHeroLevels netObject = NetCache.Get().GetNetObject<NetCache.NetCacheHeroLevels>();
      if (netObject == null)
        return false;
      return netObject.Levels.Any<NetCache.HeroLevel>((Func<NetCache.HeroLevel, bool>) (l => l.CurrentLevel.Level >= 20));
    }
  }

  public bool IsFirstTimeSeeingThisFeature
  {
    get
    {
      if (this.m_isFirstTimeSeeingThisFeature)
        return this.IsTavernBrawlActive;
      return false;
    }
  }

  public bool IsFirstTimeSeeingCurrentSeason
  {
    get
    {
      if (this.m_isFirstTimeSeeingCurrentSeason)
        return this.IsTavernBrawlActive;
      return false;
    }
  }

  public bool HasSeenTavernBrawlScreen
  {
    get
    {
      return Options.Get().GetInt(Option.LATEST_SEEN_TAVERNBRAWL_SEASON_CHALKBOARD) > 0;
    }
  }

  public long CurrentTavernBrawlSeasonEndInSeconds
  {
    get
    {
      if (this.m_currentMission == null)
        return -1;
      if (!this.m_currentMission.endDateLocal.HasValue)
        return (long) int.MaxValue;
      return (long) (this.m_currentMission.endDateLocal.Value - DateTime.Now).TotalSeconds;
    }
  }

  public long NextTavernBrawlSeasonStartInSeconds
  {
    get
    {
      if (!this.m_nextSeasonStartDateLocal.HasValue)
        return -1;
      return (long) (this.m_nextSeasonStartDateLocal.Value - DateTime.Now).TotalSeconds;
    }
  }

  public float ScheduledSecondsToRefresh
  {
    get
    {
      if (!this.m_scheduledRefreshTimeLocal.HasValue)
        return -1f;
      return (float) (this.m_scheduledRefreshTimeLocal.Value - DateTime.Now).TotalSeconds;
    }
  }

  public bool IsRefreshingTavernBrawlInfo { get; set; }

  public bool IsDeckLocked
  {
    get
    {
      CollectionDeck collectionDeck = this.CurrentDeck();
      if (collectionDeck != null)
        return collectionDeck.Locked;
      return false;
    }
  }

  public bool IsCurrentSeasonSessionBased
  {
    get
    {
      if (this.m_currentMission != null)
        return this.m_currentMission.IsSessionBased;
      return false;
    }
  }

  public long CurrentTavernBrawlSeasonNewSessionsClosedInSeconds
  {
    get
    {
      if (this.m_currentMission == null || !this.m_currentMission.closedToNewSessionsDateLocal.HasValue)
        return (long) int.MaxValue;
      return (long) (this.m_currentMission.closedToNewSessionsDateLocal.Value - DateTime.Now).TotalSeconds;
    }
  }

  public bool IsCurrentTavernBrawlSeasonClosedToPlayer
  {
    get
    {
      if (this.CurrentTavernBrawlSeasonNewSessionsClosedInSeconds < 0L && this.MyRecord != null && (!this.MyRecord.HasNumTicketsOwned || this.MyRecord.NumTicketsOwned <= 0) && this.PlayerStatus != TavernBrawlStatus.TB_STATUS_ACTIVE)
        return this.PlayerStatus != TavernBrawlStatus.TB_STATUS_IN_REWARDS;
      return false;
    }
  }

  public bool IsPlayerAtSessionMaxForCurrentTavernBrawl
  {
    get
    {
      bool seasonSessionBased = this.IsCurrentSeasonSessionBased;
      bool flag1 = this.NumSessionsAvailableForPurchase == 0;
      bool flag2 = this.NumSessionsAllowedThisSeason == 0;
      bool flag3 = this.PlayerStatus == TavernBrawlStatus.TB_STATUS_TICKET_REQUIRED;
      bool flag4 = this.NumTicketsOwned == 0;
      if (seasonSessionBased && flag1 && (!flag2 && flag3))
        return flag4;
      return false;
    }
  }

  public TavernBrawlStatus PlayerStatus
  {
    get
    {
      if (this.MyRecord == null || !this.MyRecord.HasSessionStatus)
        return TavernBrawlStatus.TB_STATUS_INVALID;
      return this.MyRecord.SessionStatus;
    }
  }

  public int NumTicketsOwned
  {
    get
    {
      if (this.MyRecord == null || !this.MyRecord.HasNumTicketsOwned)
        return 0;
      return this.MyRecord.NumTicketsOwned;
    }
  }

  public int NumSessionsAllowedThisSeason
  {
    get
    {
      if (this.CurrentMission() == null)
        return -1;
      return this.CurrentMission().maxSessions;
    }
  }

  public int NumSessionsAvailableForPurchase
  {
    get
    {
      if (this.MyRecord == null || !this.MyRecord.HasNumSessionsPurchasable)
        return 0;
      return this.MyRecord.NumSessionsPurchasable;
    }
  }

  public TavernBrawlPlayerSession CurrentSession
  {
    get
    {
      if (this.MyRecord == null || !this.MyRecord.HasSession)
        return (TavernBrawlPlayerSession) null;
      return this.MyRecord.Session;
    }
  }

  public int WinStreak
  {
    get
    {
      return this.MyRecord == null || !this.MyRecord.HasWinStreak ? 0 : this.MyRecord.WinStreak;
    }
  }

  public int GamesWon
  {
    get
    {
      if (this.m_currentMission.IsSessionBased)
      {
        if (this.CurrentSession == null)
          return 0;
        return this.CurrentSession.Wins;
      }
      if (this.MyRecord == null)
        return 0;
      return this.MyRecord.GamesWon;
    }
  }

  public int GamesLost
  {
    get
    {
      if (!this.m_currentMission.IsSessionBased)
        return this.GamesPlayed - this.GamesWon;
      if (this.CurrentSession == null)
        return 0;
      return this.CurrentSession.Losses;
    }
  }

  public int GamesPlayed
  {
    get
    {
      if (this.MyRecord == null || !this.MyRecord.HasGamesPlayed)
        return 0;
      return this.MyRecord.GamesPlayed;
    }
  }

  public int RewardProgress
  {
    get
    {
      return this.MyRecord != null ? this.MyRecord.RewardProgress : 0;
    }
  }

  public string EndingTimeText
  {
    get
    {
      long num = this.m_currentMission != null ? this.CurrentTavernBrawlSeasonEndInSeconds : -1L;
      if (num < 0L)
        return (string) null;
      TimeUtils.ElapsedStringSet stringSet = new TimeUtils.ElapsedStringSet() { m_seconds = "GLUE_TAVERN_BRAWL_LABEL_ENDING_SECONDS", m_minutes = "GLUE_TAVERN_BRAWL_LABEL_ENDING_MINUTES", m_hours = "GLUE_TAVERN_BRAWL_LABEL_ENDING_HOURS", m_yesterday = (string) null, m_days = "GLUE_TAVERN_BRAWL_LABEL_ENDING_DAYS", m_weeks = "GLUE_TAVERN_BRAWL_LABEL_ENDING_WEEKS", m_monthAgo = "GLUE_TAVERN_BRAWL_LABEL_ENDING_OVER_1_MONTH" };
      return TimeUtils.GetElapsedTimeString((int) num, stringSet);
    }
  }

  public List<RewardData> SessionRewards
  {
    get
    {
      if (this.CurrentSession != null && this.CurrentSession.Chest != null)
        return Network.ConvertRewardChest(this.CurrentSession.Chest).Rewards;
      return new List<RewardData>();
    }
  }

  public bool IsTavernBrawlInfoReady
  {
    get
    {
      return NetCache.Get().GetNetObject<NetCache.NetCacheClientOptions>() != null && this.ServerInfo != null;
    }
  }

  public bool IsAllDataReady
  {
    get
    {
      return this.m_currentMission == null || this.ServerInfo == null || !this.m_scenarioAssetPendingLoad;
    }
  }

  private TavernBrawlInfo ServerInfo
  {
    get
    {
      NetCache.NetCacheTavernBrawlInfo netObject = NetCache.Get().GetNetObject<NetCache.NetCacheTavernBrawlInfo>();
      if (netObject == null)
        return (TavernBrawlInfo) null;
      return netObject.Info;
    }
  }

  private TavernBrawlPlayerRecord MyRecord
  {
    get
    {
      NetCache.NetCacheTavernBrawlRecord netObject = NetCache.Get().GetNetObject<NetCache.NetCacheTavernBrawlRecord>();
      if (netObject == null)
        return (TavernBrawlPlayerRecord) null;
      return netObject.Record;
    }
  }

  public bool IsCheated
  {
    get
    {
      if (ApplicationMgr.IsPublic())
        return false;
      if (this.m_currentMission == null)
        return this.ServerInfo != null && this.ServerInfo.CurrentTavernBrawl != null;
      return this.ServerInfo == null || this.ServerInfo.CurrentTavernBrawl == null || this.m_currentMission.missionId != this.ServerInfo.CurrentTavernBrawl.ScenarioId;
    }
  }

  public event Action OnTavernBrawlUpdated;

  public event TavernBrawlManager.TavernBrawlSessionLimitRaisedCallback OnSessionLimitRaised;

  public TavernBrawlMission CurrentMission()
  {
    return this.m_currentMission;
  }

  public bool SelectHeroBeforeMission()
  {
    if (this.m_currentMission != null && this.m_currentMission.canSelectHeroForDeck)
      return !this.m_currentMission.canCreateDeck;
    return false;
  }

  public static bool IsInTavernBrawlFriendlyChallenge()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL || SceneMgr.Get().GetMode() == SceneMgr.Mode.FRIENDLY)
      return FriendChallengeMgr.Get().IsChallengeTavernBrawl();
    return false;
  }

  public void SetFirstTimeSeeingCurrentSeason(bool seen)
  {
    this.m_isFirstTimeSeeingCurrentSeason = seen;
  }

  public DeckRuleset GetDeckRuleset()
  {
    if (this.m_currentMission.deckRuleset != null)
      return this.m_currentMission.deckRuleset;
    return DeckRuleset.GetWildRuleset();
  }

  public static void Init()
  {
    if (TavernBrawlManager.s_instance == null)
      TavernBrawlManager.s_instance = new TavernBrawlManager();
    TavernBrawlManager.s_instance.InitImpl();
  }

  public static TavernBrawlManager Get()
  {
    if (TavernBrawlManager.s_instance == null)
      UnityEngine.Debug.LogError((object) "Trying to retrieve the Tavern Brawl Manager without calling TavernBrawlManager.Init()!");
    return TavernBrawlManager.s_instance;
  }

  public void StartGame(long deckId = 0)
  {
    if (this.m_currentMission == null)
    {
      Error.AddDevFatal("TB: m_currentMission is null");
    }
    else
    {
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TAVERN_BRAWL_QUEUE);
      GameMgr.Get().FindGame(GameType.GT_TAVERNBRAWL, FormatType.FT_WILD, this.m_currentMission.missionId, deckId, 0L);
    }
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    if (!GameMgr.Get().IsNextTavernBrawl() || GameMgr.Get().IsNextSpectator())
      return false;
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_CANCELED:
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
      case FindGameState.SERVER_GAME_CANCELED:
        if (PresenceMgr.Get().CurrentStatus == PresenceStatus.TAVERN_BRAWL_QUEUE)
        {
          PresenceMgr.Get().SetPrevStatus();
          break;
        }
        break;
      case FindGameState.SERVER_GAME_CONNECTING:
        if (GameMgr.Get().IsNextTavernBrawl() && GameMgr.Get().IsNextReconnect() && this.IsCurrentSeasonSessionBased)
        {
          BnetPresenceMgr.Get().SetGameFieldBlob(22U, (IProtoBuf) new SessionRecord()
          {
            Wins = (uint) this.GamesWon,
            Losses = (uint) this.GamesLost,
            RunFinished = false,
            SessionRecordType = SessionRecordType.TAVERN_BRAWL
          });
          break;
        }
        break;
    }
    return false;
  }

  private void ShowSessionLimitWarning()
  {
    int allowedThisSeason = TavernBrawlManager.Get().NumSessionsAllowedThisSeason;
    int availableForPurchase = TavernBrawlManager.Get().NumSessionsAvailableForPurchase;
    if (allowedThisSeason == 0)
      return;
    string str;
    if (availableForPurchase == 0)
    {
      str = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_LIMIT_ALERT_DESCRIPTION_FINAL");
    }
    else
    {
      if (allowedThisSeason - availableForPurchase <= 1)
        return;
      str = GameStrings.Format("GLUE_HEROIC_BRAWL_SESSION_LIMIT_ALERT_DESCRIPTION_NORMAL", (object) availableForPurchase, (object) availableForPurchase);
    }
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_responseDisplay = AlertPopup.ResponseDisplay.OK,
      m_alertTextAlignment = UberText.AlignmentOptions.Center,
      m_alertTextAlignmentAnchor = UberText.AnchorOptions.Middle,
      m_headerText = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_LIMIT_ALERT_TITLE"),
      m_text = str
    });
  }

  public bool HasCreatedDeck()
  {
    return this.CurrentDeck() != null;
  }

  public CollectionDeck CurrentDeck()
  {
    using (SortedDictionary<long, CollectionDeck>.ValueCollection.Enumerator enumerator = CollectionManager.Get().GetDecks().Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeck current = enumerator.Current;
        if (current.Type == DeckType.TAVERN_BRAWL_DECK)
          return current;
      }
    }
    return (CollectionDeck) null;
  }

  public bool HasValidDeck()
  {
    if (this.m_currentMission == null || !this.m_currentMission.canCreateDeck)
      return false;
    CollectionDeck deck = this.CurrentDeck();
    if (deck == null)
      return false;
    if (!deck.NetworkContentsLoaded())
    {
      CollectionManager.Get().RequestDeckContents(deck.ID);
      return false;
    }
    return this.GetDeckRuleset() == null || this.GetDeckRuleset().IsDeckValid(deck);
  }

  public void EnsureAllDataReady(TavernBrawlManager.CallbackEnsureServerDataReady callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TavernBrawlManager.\u003CEnsureAllDataReady\u003Ec__AnonStorey3FF readyCAnonStorey3Ff = new TavernBrawlManager.\u003CEnsureAllDataReady\u003Ec__AnonStorey3FF();
    // ISSUE: reference to a compiler-generated field
    readyCAnonStorey3Ff.callback = callback;
    if (this.m_currentMission == null)
      return;
    if (this.IsAllDataReady)
    {
      // ISSUE: reference to a compiler-generated field
      if (readyCAnonStorey3Ff.callback == null)
        return;
      // ISSUE: reference to a compiler-generated field
      readyCAnonStorey3Ff.callback();
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      if (readyCAnonStorey3Ff.callback != null)
      {
        if (this.m_serverDataReadyCallbacks == null)
          this.m_serverDataReadyCallbacks = new List<TavernBrawlManager.CallbackEnsureServerDataReady>();
        // ISSUE: reference to a compiler-generated field
        this.m_serverDataReadyCallbacks.Add(readyCAnonStorey3Ff.callback);
      }
      TavernBrawlSpec currentTavernBrawl = this.ServerInfo.CurrentTavernBrawl;
      List<AssetRecordInfo> assetRecordInfoList = new List<AssetRecordInfo>();
      AssetRecordInfo assetRecordInfo = new AssetRecordInfo();
      assetRecordInfo.Asset = new AssetKey();
      assetRecordInfo.Asset.Type = AssetType.ASSET_TYPE_SCENARIO;
      assetRecordInfo.Asset.AssetId = currentTavernBrawl.ScenarioId;
      assetRecordInfo.RecordByteSize = currentTavernBrawl.ScenarioRecordByteSize;
      assetRecordInfo.RecordHash = currentTavernBrawl.ScenarioRecordHash;
      assetRecordInfoList.Add(assetRecordInfo);
      if (currentTavernBrawl.AdditionalAssets != null && currentTavernBrawl.AdditionalAssets.Count > 0)
        assetRecordInfoList.AddRange((IEnumerable<AssetRecordInfo>) currentTavernBrawl.AdditionalAssets);
      if (DownloadableDbfCache.IsAssetRequestInProgress(this.m_currentMission.missionId, AssetType.ASSET_TYPE_SCENARIO))
        DownloadableDbfCache.LoadCachedAssets(false, new DownloadableDbfCache.LoadCachedAssetCallback(this.OnTavernBrawlScenarioLoaded), assetRecordInfoList.ToArray());
      else if (ApplicationMgr.IsInternal())
      {
        // ISSUE: reference to a compiler-generated method
        ApplicationMgr.Get().ScheduleCallback(Mathf.Max(0.0f, UnityEngine.Random.Range(-3f, 3f)), false, new ApplicationMgr.ScheduledCallback(readyCAnonStorey3Ff.\u003C\u003Em__1FF), (object) null);
      }
      else
        DownloadableDbfCache.LoadCachedAssets(true, new DownloadableDbfCache.LoadCachedAssetCallback(this.OnTavernBrawlScenarioLoaded), assetRecordInfoList.ToArray());
    }
  }

  public void RefreshServerData()
  {
    NetCache.Get().ReloadNetObject<NetCache.NetCacheTavernBrawlInfo>();
    NetCache.Get().RefreshNetObject<NetCache.NetCacheTavernBrawlRecord>();
  }

  private void InitImpl()
  {
    this.CleanupOldCacheDirectory();
    Network network = Network.Get();
    NetCache netCache = NetCache.Get();
    network.RegisterNetHandler((object) TavernBrawlRequestSessionBeginResponse.PacketID.ID, new Network.NetHandler(this.OnBeginSession), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) TavernBrawlRequestSessionRetireResponse.PacketID.ID, new Network.NetHandler(this.OnRetireSession), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) TavernBrawlSessionAckRewardsResponse.PacketID.ID, new Network.NetHandler(this.OnAckRewards), (Network.TimeoutHandler) null);
    netCache.RegisterUpdatedListener(typeof (NetCache.NetCacheTavernBrawlInfo), new Action(this.NetCache_OnTavernBrawlInfo));
    netCache.RegisterUpdatedListener(typeof (NetCache.NetCacheHeroLevels), new Action(this.NetCache_OnClientOptions));
    netCache.RegisterUpdatedListener(typeof (NetCache.NetCacheTavernBrawlRecord), new Action(this.NetCache_OnTavernBrawlRecord));
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    this.RegisterOptionsListeners(true);
  }

  private void RegisterOptionsListeners(bool register)
  {
    if (register)
    {
      NetCache.Get().RegisterUpdatedListener(typeof (NetCache.NetCacheClientOptions), new Action(this.NetCache_OnClientOptions));
      Options.Get().RegisterChangedListener(Option.LATEST_SEEN_TAVERNBRAWL_SEASON, new Options.ChangedCallback(this.OnOptionChangedCallback));
      Options.Get().RegisterChangedListener(Option.LATEST_SEEN_TAVERNBRAWL_SEASON_CHALKBOARD, new Options.ChangedCallback(this.OnOptionChangedCallback));
    }
    else
    {
      NetCache.Get().RemoveUpdatedListener(typeof (NetCache.NetCacheClientOptions), new Action(this.NetCache_OnClientOptions));
      Options.Get().UnregisterChangedListener(Option.LATEST_SEEN_TAVERNBRAWL_SEASON, new Options.ChangedCallback(this.OnOptionChangedCallback));
      Options.Get().UnregisterChangedListener(Option.LATEST_SEEN_TAVERNBRAWL_SEASON_CHALKBOARD, new Options.ChangedCallback(this.OnOptionChangedCallback));
    }
  }

  private void NetCache_OnClientOptions()
  {
    this.RegisterOptionsListeners(false);
    this.CheckLatestSessionLimit(this.CheckLatestSeenSeason(true));
    this.RegisterOptionsListeners(true);
  }

  private void NetCache_OnTavernBrawlRecord()
  {
    if (this.OnTavernBrawlUpdated == null)
      return;
    this.OnTavernBrawlUpdated();
  }

  private void OnOptionChangedCallback(Option option, object prevValue, bool existed, object userData)
  {
    this.RegisterOptionsListeners(false);
    this.CheckLatestSessionLimit(this.CheckLatestSeenSeason(false));
    this.RegisterOptionsListeners(true);
  }

  private bool CheckLatestSeenSeason(bool canSetOption)
  {
    bool flag1 = false;
    if (!this.IsTavernBrawlInfoReady)
      return flag1;
    bool flag2 = !this.m_hasGottenClientOptionsAtLeastOnce;
    this.m_hasGottenClientOptionsAtLeastOnce = true;
    bool seeingThisFeature = this.IsFirstTimeSeeingThisFeature;
    bool seeingCurrentSeason = this.IsFirstTimeSeeingCurrentSeason;
    this.m_isFirstTimeSeeingThisFeature = false;
    this.m_isFirstTimeSeeingCurrentSeason = false;
    TavernBrawlInfo serverInfo = this.ServerInfo;
    if (serverInfo.HasCurrentTavernBrawl)
    {
      NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
      bool flag3 = netObject != null && netObject.Games.TavernBrawl && this.HasUnlockedTavernBrawl;
      TavernBrawlSpec currentTavernBrawl = serverInfo.CurrentTavernBrawl;
      int num = Options.Get().GetInt(Option.LATEST_SEEN_TAVERNBRAWL_SEASON);
      if (num == 0 && flag3)
      {
        this.m_isFirstTimeSeeingThisFeature = true;
        NotificationManager.Get().ForceRemoveSoundFromPlayedList("VO_INNKEEPER_TAVERNBRAWL_PUSH_32");
        NotificationManager.Get().ForceRemoveSoundFromPlayedList("VO_INNKEEPER_TAVERNBRAWL_WELCOME1_27");
      }
      if (num < currentTavernBrawl.SeasonId && flag3)
      {
        this.m_isFirstTimeSeeingCurrentSeason = true;
        NotificationManager.Get().ForceRemoveSoundFromPlayedList("VO_INNKEEPER_TAVERNBRAWL_DESC2_30");
        Hub.s_hasAlreadyShownTBAnimation = false;
        if (canSetOption)
          Options.Get().SetInt(Option.LATEST_SEEN_TAVERNBRAWL_SEASON, currentTavernBrawl.SeasonId);
        flag1 = true;
      }
    }
    if ((flag2 || seeingThisFeature != this.IsFirstTimeSeeingThisFeature || seeingCurrentSeason != this.IsFirstTimeSeeingCurrentSeason) && this.OnTavernBrawlUpdated != null)
      this.OnTavernBrawlUpdated();
    return flag1;
  }

  private void CheckLatestSessionLimit(bool seasonHasChanged)
  {
    if (!this.IsTavernBrawlInfoReady)
      return;
    TavernBrawlInfo serverInfo = this.ServerInfo;
    if (!serverInfo.HasCurrentTavernBrawl)
      return;
    TavernBrawlSpec currentTavernBrawl = serverInfo.CurrentTavernBrawl;
    if (seasonHasChanged)
    {
      Options.Get().SetInt(Option.LATEST_SEEN_TAVERNBRAWL_SESSION_LIMIT, currentTavernBrawl.MaxSessions);
    }
    else
    {
      int oldLimit = Options.Get().GetInt(Option.LATEST_SEEN_TAVERNBRAWL_SESSION_LIMIT);
      if (oldLimit == currentTavernBrawl.MaxSessions)
        return;
      if (oldLimit == 0)
      {
        Options.Get().SetInt(Option.LATEST_SEEN_TAVERNBRAWL_SESSION_LIMIT, currentTavernBrawl.MaxSessions);
      }
      else
      {
        if (currentTavernBrawl.MaxSessions > oldLimit && this.OnSessionLimitRaised != null)
          this.OnSessionLimitRaised(oldLimit, currentTavernBrawl.MaxSessions);
        Options.Get().SetInt(Option.LATEST_SEEN_TAVERNBRAWL_SESSION_LIMIT, currentTavernBrawl.MaxSessions);
      }
    }
  }

  private void NetCache_OnTavernBrawlInfo()
  {
    this.IsRefreshingTavernBrawlInfo = false;
    TavernBrawlInfo serverInfo = this.ServerInfo;
    if (serverInfo == null)
      return;
    if (serverInfo.HasCurrentTavernBrawl)
    {
      this.m_currentMission = new TavernBrawlMission();
      TavernBrawlSpec currentTavernBrawl = serverInfo.CurrentTavernBrawl;
      this.m_currentMission.seasonId = currentTavernBrawl.SeasonId;
      this.m_currentMission.missionId = currentTavernBrawl.ScenarioId;
      this.m_currentMission.formatType = currentTavernBrawl.FormatType;
      this.m_currentMission.rewardType = currentTavernBrawl.RewardType;
      this.m_currentMission.rewardTrigger = currentTavernBrawl.RewardTrigger;
      this.m_currentMission.RewardData1 = currentTavernBrawl.RewardData1;
      this.m_currentMission.RewardData2 = currentTavernBrawl.RewardData2;
      this.m_currentMission.ticketType = currentTavernBrawl.TicketType;
      this.m_currentMission.maxWins = currentTavernBrawl.MaxWins;
      this.m_currentMission.maxLosses = currentTavernBrawl.MaxLosses;
      this.m_currentMission.maxSessions = currentTavernBrawl.MaxSessions;
      this.m_currentMission.SeasonEndSecondsSpreadCount = currentTavernBrawl.SeasonEndSecondSpreadCount;
      this.m_currentMission.friendlyChallengeDisabled = currentTavernBrawl.FriendlyChallengeDisabled;
      this.CheckLatestSessionLimit(this.CheckLatestSeenSeason(true));
      this.m_currentMission.endDateLocal = !currentTavernBrawl.HasEndSecondsFromNow ? new DateTime?() : new DateTime?(DateTime.Now + new TimeSpan(0, 0, (int) currentTavernBrawl.EndSecondsFromNow));
      this.m_currentMission.closedToNewSessionsDateLocal = !currentTavernBrawl.HasClosedToNewSessionsSecondsFromNow ? new DateTime?() : new DateTime?(DateTime.Now + new TimeSpan(0, 0, (int) currentTavernBrawl.ClosedToNewSessionsSecondsFromNow));
      this.m_scenarioAssetPendingLoad = true;
    }
    else
      this.m_currentMission = (TavernBrawlMission) null;
    this.m_nextSeasonStartDateLocal = !serverInfo.HasNextStartSecondsFromNow ? new DateTime?() : new DateTime?(DateTime.Now + new TimeSpan(0, 0, (int) serverInfo.NextStartSecondsFromNow));
    ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.ScheduledEndOfCurrentTBCallback), (object) null);
    long seasonEndInSeconds = this.CurrentTavernBrawlSeasonEndInSeconds;
    if (this.IsTavernBrawlActive && seasonEndInSeconds > 0L)
    {
      Log.EventTiming.Print("Scheduling end of current TB {0} secs from now.", (object) seasonEndInSeconds);
      ApplicationMgr.Get().ScheduleCallback((float) seasonEndInSeconds, true, new ApplicationMgr.ScheduledCallback(this.ScheduledEndOfCurrentTBCallback), (object) null);
    }
    ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.ScheduledRefreshTBSpecCallback), (object) null);
    long seasonStartInSeconds = this.NextTavernBrawlSeasonStartInSeconds;
    if (seasonStartInSeconds >= 0L)
    {
      this.m_scheduledRefreshTimeLocal = new DateTime?(DateTime.Now + new TimeSpan(0, 0, 0, (int) seasonStartInSeconds, 0));
      Log.EventTiming.Print("Scheduling TB refresh for {0} secs from now.", (object) seasonStartInSeconds);
      ApplicationMgr.Get().ScheduleCallback((float) seasonStartInSeconds, true, new ApplicationMgr.ScheduledCallback(this.ScheduledRefreshTBSpecCallback), (object) null);
    }
    long sessionsClosedInSeconds = this.CurrentTavernBrawlSeasonNewSessionsClosedInSeconds;
    if (this.IsCurrentSeasonSessionBased && sessionsClosedInSeconds > 0L)
    {
      Log.EventTiming.Print("Scheduling TB Closed Update for {0} secs from now.", (object) sessionsClosedInSeconds);
      ApplicationMgr.Get().ScheduleCallback((float) sessionsClosedInSeconds, true, new ApplicationMgr.ScheduledCallback(this.ScheduleTBClosedUpdateCallback), (object) null);
    }
    if (this.OnTavernBrawlUpdated == null)
      return;
    this.OnTavernBrawlUpdated();
  }

  private void ScheduledEndOfCurrentTBCallback(object userData)
  {
    Log.EventTiming.Print("ScheduledEndOfCurrentTBCallback: ending current TB now.");
    if (this.m_currentMission != null && this.m_currentMission.IsSessionBased && (this.PlayerStatus == TavernBrawlStatus.TB_STATUS_ACTIVE || this.PlayerStatus == TavernBrawlStatus.TB_STATUS_IN_REWARDS) && (!((UnityEngine.Object) TavernBrawlDisplay.Get() != (UnityEngine.Object) null) || !TavernBrawlDisplay.Get().IsInRewards()))
    {
      int num = 2;
      ApplicationMgr.Get().ScheduleCallback(this.m_currentMission.SeasonEndSecondsSpreadCount <= 0 ? (float) (num + UnityEngine.Random.Range(0, 30)) : (float) (num + this.m_currentMission.SeasonEndSecondsSpreadCount), true, new ApplicationMgr.ScheduledCallback(this.ScheduledEndOfCurrentTBCallback_AfterSpreadWhenRewardsExpected), (object) null);
    }
    this.m_currentMission = (TavernBrawlMission) null;
    if (GameMgr.Get().IsFindingGame())
      GameMgr.Get().CancelFindGame();
    if (this.OnTavernBrawlUpdated == null)
      return;
    this.OnTavernBrawlUpdated();
  }

  private void ScheduledEndOfCurrentTBCallback_AfterSpreadWhenRewardsExpected(object userData)
  {
    NetCache.Get().RefreshNetObject<NetCache.NetCacheTavernBrawlRecord>();
  }

  private void ScheduledRefreshTBSpecCallback(object userData)
  {
    Log.EventTiming.Print("ScheduledRefreshTBSpecCallback: refreshing now.");
    this.RefreshServerData();
  }

  private void ScheduleTBClosedUpdateCallback(object userData)
  {
    Log.EventTiming.Print("ScheduledUpdateTBCallback: updating now.");
    if (this.OnTavernBrawlUpdated == null)
      return;
    this.OnTavernBrawlUpdated();
  }

  private void OnTavernBrawlScenarioLoaded(AssetKey requestedKey, PegasusShared.ErrorCode code, byte[] assetBytes)
  {
    if (requestedKey == null || requestedKey.Type != AssetType.ASSET_TYPE_SCENARIO)
    {
      Log.Henry.Print("OnTavernBrawlScenarioLoaded bad AssetType assetId={0} assetType={1} {2}", new object[3]
      {
        (object) (requestedKey != null ? requestedKey.AssetId : 0),
        (object) (requestedKey != null ? (int) requestedKey.Type : 0),
        (object) (requestedKey != null ? requestedKey.Type.ToString() : "(null)")
      });
    }
    else
    {
      if (assetBytes == null || assetBytes.Length == 0 || this.m_currentMission == null)
        return;
      ScenarioDbRecord from = ProtobufUtil.ParseFrom<ScenarioDbRecord>(assetBytes, 0, assetBytes.Length);
      if (this.m_currentMission.missionId != from.Id)
        return;
      this.m_scenarioAssetPendingLoad = false;
      this.m_currentMission.canSelectHeroForDeck = false;
      this.m_currentMission.canCreateDeck = false;
      this.m_currentMission.canEditDeck = false;
      using (List<GameSetupRule>.Enumerator enumerator = from.DeprecatedRules.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GameSetupRule current = enumerator.Current;
          if (current.RuleType == PegasusShared.RuleType.RULE_CHOOSE_HERO)
          {
            this.m_currentMission.canSelectHeroForDeck = true;
            this.m_currentMission.canCreateDeck = false;
            this.m_currentMission.canEditDeck = false;
          }
          else if (current.RuleType == PegasusShared.RuleType.RULE_CHOOSE_DECK)
          {
            this.m_currentMission.canSelectHeroForDeck = true;
            this.m_currentMission.canCreateDeck = true;
            this.m_currentMission.canEditDeck = true;
          }
        }
      }
      if (from.HasRuleType)
      {
        switch (from.RuleType)
        {
          case PegasusShared.RuleType.RULE_CHOOSE_HERO:
            this.m_currentMission.canSelectHeroForDeck = true;
            this.m_currentMission.canCreateDeck = false;
            this.m_currentMission.canEditDeck = false;
            break;
          case PegasusShared.RuleType.RULE_CHOOSE_DECK:
            this.m_currentMission.canSelectHeroForDeck = true;
            this.m_currentMission.canCreateDeck = true;
            this.m_currentMission.canEditDeck = true;
            break;
        }
      }
      this.m_currentMission.deckRuleset = DeckRuleset.GetDeckRuleset(from.DeckRulesetId);
      ApplicationMgr.Get().StartCoroutine(this.OnTavernBrawlScenarioLoaded_EnsureDeckContentsLoaded());
    }
  }

  private void CleanupOldCacheDirectory()
  {
    string path = string.Format("{0}/Cached", (object) FileUtils.PersistentDataPath);
    if (!Directory.Exists(path))
      return;
    try
    {
      Directory.Delete(path, true);
    }
    catch (Exception ex)
    {
    }
  }

  [DebuggerHidden]
  private IEnumerator OnTavernBrawlScenarioLoaded_EnsureDeckContentsLoaded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlManager.\u003COnTavernBrawlScenarioLoaded_EnsureDeckContentsLoaded\u003Ec__Iterator289() { \u003C\u003Ef__this = this };
  }

  private void OnBeginSession()
  {
    Log.TavernBrawl.Print(string.Format("TavernBrawlManager.OnBeginSession"));
    BnetPresenceMgr.Get().SetGameFieldBlob(22U, (IProtoBuf) new SessionRecord()
    {
      Wins = 0U,
      Losses = 0U,
      RunFinished = false,
      SessionRecordType = SessionRecordType.TAVERN_BRAWL
    });
    TavernBrawlRequestSessionBeginResponse brawlSessionBegin = Network.GetTavernBrawlSessionBegin();
    if (brawlSessionBegin.HasErrorCode && brawlSessionBegin.ErrorCode != PegasusShared.ErrorCode.ERROR_OK)
    {
      string str = brawlSessionBegin.ErrorCode.ToString();
      UnityEngine.Debug.LogWarning((object) ("TavernBrawlManager.OnBeginSession: Got Error " + (object) brawlSessionBegin.ErrorCode + " : " + str));
      if (!SceneMgr.Get().IsSceneLoaded() || SceneMgr.Get().IsModeRequested(SceneMgr.Mode.TAVERN_BRAWL) && TavernBrawlManager.Get().PlayerStatus == TavernBrawlStatus.TB_STATUS_ACTIVE)
        return;
      if ((UnityEngine.Object) TavernBrawlStore.Get() != (UnityEngine.Object) null)
        TavernBrawlStore.Get().Hide();
      if (!SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
        SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_ERROR_TITLE"),
        m_text = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_ERROR"),
        m_responseDisplay = AlertPopup.ResponseDisplay.OK
      });
    }
    else
    {
      this.ShowSessionLimitWarning();
      if (this.OnTavernBrawlUpdated == null)
        return;
      this.OnTavernBrawlUpdated();
    }
  }

  private void OnRetireSession()
  {
    Log.TavernBrawl.Print(string.Format("TavernBrawlManager.OnRetireSession"));
    TavernBrawlRequestSessionRetireResponse brawlSessionRetired = Network.GetTavernBrawlSessionRetired();
    if (brawlSessionRetired.ErrorCode != PegasusShared.ErrorCode.ERROR_OK)
    {
      string str = brawlSessionRetired.ErrorCode.ToString();
      UnityEngine.Debug.LogWarning((object) ("TavernBrawlManager.OnRetireSession: Got Error " + (object) brawlSessionRetired.ErrorCode + " : " + str));
    }
    else
    {
      this.MyRecord.SessionStatus = TavernBrawlStatus.TB_STATUS_IN_REWARDS;
      this.CurrentSession.Chest = brawlSessionRetired.Chest;
      if (this.OnTavernBrawlUpdated == null)
        return;
      this.OnTavernBrawlUpdated();
    }
  }

  private void OnAckRewards()
  {
    Log.TavernBrawl.Print(string.Format("TavernBrawlManager.OnAckRewards"));
    BnetPresenceMgr.Get().SetGameFieldBlob(22U, (IProtoBuf) new SessionRecord()
    {
      Wins = (uint) this.GamesWon,
      Losses = (uint) this.GamesLost,
      RunFinished = true,
      SessionRecordType = SessionRecordType.TAVERN_BRAWL
    });
    NetCache.Get().ReloadNetObject<NetCache.NetCacheTavernBrawlRecord>();
    if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
      return;
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
  }

  public void Cheat_SetScenario(int scenarioId)
  {
    if (ApplicationMgr.IsPublic())
      return;
    if (this.m_currentMission == null)
      this.m_currentMission = new TavernBrawlMission();
    this.m_currentMission.missionId = scenarioId;
    this.m_scenarioAssetPendingLoad = true;
    if (this.OnTavernBrawlUpdated != null)
      this.OnTavernBrawlUpdated();
    AssetRecordInfo assetRecordInfo = new AssetRecordInfo();
    assetRecordInfo.Asset = new AssetKey();
    assetRecordInfo.Asset.Type = AssetType.ASSET_TYPE_SCENARIO;
    assetRecordInfo.Asset.AssetId = scenarioId;
    assetRecordInfo.RecordByteSize = 0U;
    assetRecordInfo.RecordHash = (byte[]) null;
    DownloadableDbfCache.LoadCachedAssets(1 != 0, new DownloadableDbfCache.LoadCachedAssetCallback(this.OnTavernBrawlScenarioLoaded), assetRecordInfo);
  }

  public void Cheat_ResetToServerData()
  {
    if (ApplicationMgr.IsPublic())
      return;
    this.NetCache_OnTavernBrawlInfo();
    if (this.m_currentMission == null)
      return;
    AssetRecordInfo assetRecordInfo = new AssetRecordInfo();
    assetRecordInfo.Asset = new AssetKey();
    assetRecordInfo.Asset.Type = AssetType.ASSET_TYPE_SCENARIO;
    assetRecordInfo.Asset.AssetId = this.m_currentMission.missionId;
    assetRecordInfo.RecordByteSize = 0U;
    assetRecordInfo.RecordHash = (byte[]) null;
    DownloadableDbfCache.LoadCachedAssets(1 != 0, new DownloadableDbfCache.LoadCachedAssetCallback(this.OnTavernBrawlScenarioLoaded), assetRecordInfo);
  }

  public void Cheat_ResetSeenStuff(int newValue)
  {
    if (ApplicationMgr.IsPublic())
      return;
    this.RegisterOptionsListeners(false);
    Options.Get().SetInt(Option.LATEST_SEEN_TAVERNBRAWL_SEASON_CHALKBOARD, newValue);
    Options.Get().SetInt(Option.LATEST_SEEN_TAVERNBRAWL_SEASON, newValue);
    Options.Get().SetInt(Option.TIMES_SEEN_TAVERNBRAWL_CRAZY_RULES_QUOTE, 0);
    this.CheckLatestSessionLimit(this.CheckLatestSeenSeason(false));
    this.RegisterOptionsListeners(true);
  }

  public void Cheat_SetWins(int numWins)
  {
    if (ApplicationMgr.IsPublic())
      return;
    this.CurrentSession.Wins = numWins;
    if (this.OnTavernBrawlUpdated == null)
      return;
    this.OnTavernBrawlUpdated();
  }

  public void Cheat_SetLosses(int numLosses)
  {
    if (ApplicationMgr.IsPublic())
      return;
    this.CurrentSession.Losses = numLosses;
    if (this.OnTavernBrawlUpdated == null)
      return;
    this.OnTavernBrawlUpdated();
  }

  public void Cheat_SetActiveSession(int status)
  {
    NetCache.NetCacheTavernBrawlRecord netObject = NetCache.Get().GetNetObject<NetCache.NetCacheTavernBrawlRecord>();
    netObject.Record.SessionStatus = (TavernBrawlStatus) status;
    TavernBrawlPlayerSession brawlPlayerSession = new TavernBrawlPlayerSession();
    netObject.Record.Session = brawlPlayerSession;
  }

  public void Cheat_DoHeroicRewards(int wins)
  {
    NetCache.Get().GetNetObject<NetCache.NetCacheTavernBrawlRecord>().Record.SessionStatus = TavernBrawlStatus.TB_STATUS_IN_REWARDS;
    this.CurrentSession.Chest = RewardUtils.GenerateTavernBrawlRewardChest_CHEAT(wins);
    this.CurrentSession.Wins = wins;
    if (this.OnTavernBrawlUpdated == null)
      return;
    this.OnTavernBrawlUpdated();
  }

  public delegate void CallbackEnsureServerDataReady();

  public delegate void TavernBrawlSessionLimitRaisedCallback(int oldLimit, int newLimit);
}
