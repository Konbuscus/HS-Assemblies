﻿// Decompiled with JetBrains decompiler
// Type: MemUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.InteropServices;
using System.Text;

public static class MemUtils
{
  public static void FreePtr(IntPtr ptr)
  {
    if (ptr == IntPtr.Zero)
      return;
    Marshal.FreeHGlobal(ptr);
  }

  public static byte[] PtrToBytes(IntPtr ptr, int size)
  {
    if (ptr == IntPtr.Zero)
      return (byte[]) null;
    if (size == 0)
      return (byte[]) null;
    byte[] destination = new byte[size];
    Marshal.Copy(ptr, destination, 0, size);
    return destination;
  }

  public static IntPtr PtrFromBytes(byte[] bytes)
  {
    return MemUtils.PtrFromBytes(bytes, 0);
  }

  public static IntPtr PtrFromBytes(byte[] bytes, int offset)
  {
    if (bytes == null)
      return IntPtr.Zero;
    int len = bytes.Length - offset;
    return MemUtils.PtrFromBytes(bytes, offset, len);
  }

  public static IntPtr PtrFromBytes(byte[] bytes, int offset, int len)
  {
    if (bytes == null || len <= 0)
      return IntPtr.Zero;
    IntPtr destination = Marshal.AllocHGlobal(len);
    Marshal.Copy(bytes, offset, destination, len);
    return destination;
  }

  public static byte[] StructToBytes<T>(T t)
  {
    int length = Marshal.SizeOf(typeof (T));
    byte[] destination = new byte[length];
    IntPtr num = Marshal.AllocHGlobal(length);
    Marshal.StructureToPtr((object) t, num, true);
    Marshal.Copy(num, destination, 0, length);
    Marshal.FreeHGlobal(num);
    return destination;
  }

  public static T StructFromBytes<T>(byte[] bytes)
  {
    return MemUtils.StructFromBytes<T>(bytes, 0);
  }

  public static T StructFromBytes<T>(byte[] bytes, int offset)
  {
    System.Type type = typeof (T);
    int num1 = Marshal.SizeOf(type);
    if (bytes == null)
      return default (T);
    if (bytes.Length - offset < num1)
      return default (T);
    IntPtr num2 = Marshal.AllocHGlobal(num1);
    Marshal.Copy(bytes, offset, num2, num1);
    T structure = (T) Marshal.PtrToStructure(num2, type);
    Marshal.FreeHGlobal(num2);
    return structure;
  }

  public static IntPtr Utf8PtrFromString(string managedString)
  {
    if (managedString == null)
      return IntPtr.Zero;
    int length = 1 + Encoding.UTF8.GetByteCount(managedString);
    byte[] numArray = new byte[length];
    Encoding.UTF8.GetBytes(managedString, 0, managedString.Length, numArray, 0);
    IntPtr destination = Marshal.AllocHGlobal(length);
    Marshal.Copy(numArray, 0, destination, length);
    return destination;
  }

  public static string StringFromUtf8Ptr(IntPtr ptr)
  {
    int len;
    return MemUtils.StringFromUtf8Ptr(ptr, out len);
  }

  public static string StringFromUtf8Ptr(IntPtr ptr, out int len)
  {
    len = 0;
    if (ptr == IntPtr.Zero)
      return (string) null;
    len = MemUtils.StringPtrByteLen(ptr);
    if (len == 0)
      return (string) null;
    byte[] numArray = new byte[len];
    Marshal.Copy(ptr, numArray, 0, len);
    return Encoding.UTF8.GetString(numArray);
  }

  public static int StringPtrByteLen(IntPtr ptr)
  {
    if (ptr == IntPtr.Zero)
      return 0;
    int ofs = 0;
    while ((int) Marshal.ReadByte(ptr, ofs) != 0)
      ++ofs;
    return ofs;
  }
}
