﻿// Decompiled with JetBrains decompiler
// Type: StandardGameEntity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class StandardGameEntity : GameEntity
{
  public override void OnTagChanged(TagDelta change)
  {
    switch ((GAME_TAG) change.tag)
    {
      case GAME_TAG.STEP:
        if (change.newValue == 4)
        {
          MulliganManager.Get().BeginMulligan();
          break;
        }
        break;
      case GAME_TAG.NEXT_STEP:
        if (change.newValue == 6)
        {
          if (GameState.Get().IsMulliganManagerActive())
          {
            GameState.Get().SetMulliganBusy(true);
            break;
          }
          break;
        }
        if (change.oldValue == 9 && change.newValue == 10 && GameState.Get().IsFriendlySidePlayerTurn())
        {
          TurnStartManager.Get().BeginPlayingTurnEvents();
          break;
        }
        break;
    }
    base.OnTagChanged(change);
  }
}
