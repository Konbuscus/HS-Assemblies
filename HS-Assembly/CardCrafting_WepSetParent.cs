﻿// Decompiled with JetBrains decompiler
// Type: CardCrafting_WepSetParent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardCrafting_WepSetParent : MonoBehaviour
{
  public GameObject m_Parent;
  public Transform m_OrgParent;
  public GameObject m_ManaGem;
  public GameObject m_Portrait;
  public GameObject m_NameBanner;
  public GameObject m_RarityGem;
  public GameObject m_Discription;
  public GameObject m_Swords;
  public GameObject m_Shield;

  private void Start()
  {
    if ((bool) ((Object) this.m_Parent))
      return;
    Debug.LogError((object) "Animation Event Set Parent is null!");
    this.enabled = false;
  }

  public void SetParentManaGem()
  {
    if (!(bool) ((Object) this.m_Parent))
      return;
    this.m_ManaGem.transform.parent = this.m_Parent.transform;
  }

  public void SetParentPortrait()
  {
    if (!(bool) ((Object) this.m_Parent))
      return;
    this.m_Portrait.transform.parent = this.m_Parent.transform;
  }

  public void SetParentNameBanner()
  {
    if (!(bool) ((Object) this.m_Parent))
      return;
    this.m_NameBanner.transform.parent = this.m_Parent.transform;
  }

  public void SetParentRarityGem()
  {
    if (!(bool) ((Object) this.m_Parent))
      return;
    this.m_RarityGem.transform.parent = this.m_Parent.transform;
  }

  public void SetParentDiscription()
  {
    if (!(bool) ((Object) this.m_Parent))
      return;
    this.m_Discription.transform.parent = this.m_Parent.transform;
  }

  public void SetParentSwords()
  {
    if (!(bool) ((Object) this.m_Parent))
      return;
    this.m_Swords.transform.parent = this.m_Parent.transform;
  }

  public void SetParentShield()
  {
    if (!(bool) ((Object) this.m_Parent))
      return;
    this.m_Shield.transform.parent = this.m_Parent.transform;
  }

  public void SetBackToOrgParent()
  {
    if ((bool) ((Object) this.m_OrgParent))
      this.m_ManaGem.transform.parent = this.m_OrgParent;
    this.m_Portrait.transform.parent = this.m_OrgParent;
    this.m_NameBanner.transform.parent = this.m_OrgParent;
    this.m_RarityGem.transform.parent = this.m_OrgParent;
    this.m_Discription.transform.parent = this.m_OrgParent;
    this.m_Swords.transform.parent = this.m_OrgParent;
    this.m_Shield.transform.parent = this.m_OrgParent;
  }
}
