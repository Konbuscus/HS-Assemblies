﻿// Decompiled with JetBrains decompiler
// Type: RewardBanner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RewardBanner : MonoBehaviour
{
  public UberText m_headlineText;
  public UberText m_detailsText;
  public UberText m_sourceText;
  public GameObject m_headlineCenterBone;
  private float m_headlineHeight;

  public string HeadlineText
  {
    get
    {
      return this.m_headlineText.Text;
    }
  }

  public string DetailsText
  {
    get
    {
      return this.m_detailsText.Text;
    }
  }

  public string SourceText
  {
    get
    {
      return this.m_sourceText.Text;
    }
  }

  private void Awake()
  {
    if ((bool) UniversalInputManager.UsePhoneUI && (Object) this.m_sourceText != (Object) null)
      this.m_sourceText.gameObject.SetActive(false);
    this.m_headlineHeight = this.m_headlineText.Height;
  }

  public void SetText(string headline, string details, string source)
  {
    this.m_headlineText.Text = headline;
    this.m_detailsText.Text = details;
    this.m_sourceText.Text = source;
    if (!(details == string.Empty))
      return;
    this.AlignHeadlineToCenterBone();
    this.m_headlineText.Height = this.m_headlineHeight * 1.5f;
  }

  public void AlignHeadlineToCenterBone()
  {
    if (!((Object) this.m_headlineCenterBone != (Object) null))
      return;
    this.m_headlineText.transform.localPosition = this.m_headlineCenterBone.transform.localPosition;
  }
}
