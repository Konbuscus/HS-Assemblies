﻿// Decompiled with JetBrains decompiler
// Type: TGTArcheryTarget
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class TGTArcheryTarget : MonoBehaviour
{
  public int m_BullseyePercent = 5;
  public int m_TargetDummyPercent = 1;
  public float m_MaxRandomOffset = 0.3f;
  public int m_Levelup = 50;
  private int m_lastArrow = 1;
  public GameObject m_Collider01;
  public GameObject m_TargetPhysics;
  public GameObject m_TargetRoot;
  public GameObject m_Arrow;
  public GameObject m_SplitArrow;
  public float m_HitIntensity;
  public int m_MaxArrows;
  public List<TGTArrow> m_TargetDummyArrows;
  public GameObject m_ArrowBone01;
  public GameObject m_ArrowBone02;
  public BoxCollider m_BoxCollider01;
  public BoxCollider m_BoxCollider02;
  public BoxCollider m_BoxColliderBullseye;
  public Transform m_CenterBone;
  public Transform m_OuterRadiusBone;
  public Transform m_BullseyeCenterBone;
  public Transform m_BullseyeRadiusBone;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_HitTargetSoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_HitBullseyeSoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_HitTargetDummySoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_SplitArrowSoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_RemoveArrowSoundPrefab;
  private GameObject[] m_arrows;
  private float m_targetRadius;
  private float m_bullseyeRadius;
  private int m_ArrowCount;
  private List<int> m_AvailableTargetDummyArrows;
  private GameObject m_lastBullseyeArrow;
  private bool m_lastArrowWasBullseye;
  private bool m_clearingArrows;
  private float m_lastClickTime;

  private void Start()
  {
    this.m_arrows = new GameObject[this.m_MaxArrows];
    for (int index = 0; index < this.m_MaxArrows; ++index)
    {
      this.m_arrows[index] = Object.Instantiate<GameObject>(this.m_Arrow);
      this.m_arrows[index].transform.position = new Vector3(-15f, -15f, -15f);
      this.m_arrows[index].transform.parent = this.m_TargetRoot.transform;
      this.m_arrows[index].SetActive(false);
    }
    this.m_arrows[0].SetActive(true);
    this.m_arrows[0].transform.position = this.m_ArrowBone01.transform.position;
    this.m_arrows[0].transform.rotation = this.m_ArrowBone01.transform.rotation;
    this.m_arrows[1].SetActive(true);
    this.m_arrows[1].transform.position = this.m_ArrowBone02.transform.position;
    this.m_arrows[1].transform.rotation = this.m_ArrowBone02.transform.rotation;
    this.m_lastArrow = 2;
    this.m_targetRadius = Vector3.Distance(this.m_CenterBone.position, this.m_OuterRadiusBone.position);
    this.m_bullseyeRadius = Vector3.Distance(this.m_BullseyeCenterBone.position, this.m_BullseyeRadiusBone.position);
    this.m_AvailableTargetDummyArrows = new List<int>();
    for (int index = 0; index < this.m_TargetDummyArrows.Count; ++index)
      this.m_AvailableTargetDummyArrows.Add(index);
    this.m_SplitArrow.SetActive(false);
  }

  private void Update()
  {
    this.HandleHits();
  }

  private void HandleHits()
  {
    if (!UniversalInputManager.Get().GetMouseButtonDown(0) || !this.IsOver(this.m_Collider01))
      return;
    this.HnadleFireArrow();
  }

  private void HnadleFireArrow()
  {
    if (this.m_clearingArrows)
      return;
    ++this.m_ArrowCount;
    if (this.m_ArrowCount > this.m_Levelup)
    {
      this.m_ArrowCount = 0;
      this.m_MaxRandomOffset *= 0.95f;
      this.m_BullseyePercent += 4;
    }
    if (Random.Range(0, 100) < this.m_TargetDummyPercent && this.m_AvailableTargetDummyArrows.Count > 0)
    {
      this.HitTargetDummy();
    }
    else
    {
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      bool bullseye = false;
      RaycastHit hitInfo;
      if (this.m_BoxColliderBullseye.Raycast(ray, out hitInfo, 100f))
        bullseye = true;
      if (!this.m_BoxCollider02.Raycast(ray, out hitInfo, 100f))
        return;
      ++this.m_lastArrow;
      if (this.m_lastArrow >= this.m_MaxArrows)
      {
        this.m_lastArrow = 0;
        this.StartCoroutine(this.ClearArrows());
      }
      else
      {
        GameObject arrow = this.m_arrows[this.m_lastArrow];
        this.FireArrow(arrow.GetComponent<TGTArrow>(), hitInfo.point, bullseye);
        arrow.transform.eulerAngles = hitInfo.normal;
        this.ImpactTarget();
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator ClearArrows()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTArcheryTarget.\u003CClearArrows\u003Ec__Iterator13()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void FireArrow(TGTArrow arrow, Vector3 hitPosition, bool bullseye)
  {
    arrow.transform.position = hitPosition;
    bool flag = false;
    if ((double) Time.timeSinceLevelLoad > (double) this.m_lastClickTime + 0.800000011920929)
      flag = true;
    this.m_lastClickTime = Time.timeSinceLevelLoad;
    int num1 = this.m_BullseyePercent;
    if (flag)
      num1 *= 2;
    if (num1 > 80)
      num1 = 80;
    if (bullseye && Random.Range(0, 100) < num1)
    {
      int num2 = 2;
      if (flag)
        num2 = 8;
      if (this.m_lastArrowWasBullseye && !this.m_SplitArrow.activeSelf && (bullseye && Random.Range(0, 100) < num2))
      {
        this.m_SplitArrow.transform.position = this.m_lastBullseyeArrow.transform.position;
        this.m_SplitArrow.transform.rotation = this.m_lastBullseyeArrow.transform.rotation;
        TGTArrow component1 = this.m_SplitArrow.GetComponent<TGTArrow>();
        TGTArrow component2 = this.m_lastBullseyeArrow.GetComponent<TGTArrow>();
        this.m_SplitArrow.SetActive(true);
        component1.FireArrow(false);
        component1.Bullseye();
        this.PlaySound(this.m_SplitArrowSoundPrefab);
        component1.m_ArrowRoot.transform.position = component2.m_ArrowRoot.transform.position;
        component1.m_ArrowRoot.transform.rotation = component2.m_ArrowRoot.transform.rotation;
        this.m_lastBullseyeArrow.SetActive(false);
        this.m_lastArrowWasBullseye = false;
        this.m_lastBullseyeArrow = (GameObject) null;
      }
      else
      {
        arrow.gameObject.SetActive(true);
        arrow.Bullseye();
        this.PlaySound(this.m_HitBullseyeSoundPrefab);
        arrow.m_ArrowRoot.transform.localPosition = Vector3.zero;
        this.m_lastBullseyeArrow = arrow.gameObject;
        this.m_lastArrowWasBullseye = true;
      }
    }
    else
    {
      this.m_lastArrowWasBullseye = false;
      this.m_lastBullseyeArrow = (GameObject) null;
      arrow.gameObject.SetActive(true);
      if (bullseye)
      {
        Vector2 vector2 = Random.insideUnitCircle.normalized * this.m_bullseyeRadius * 2f;
        arrow.m_ArrowRoot.transform.localPosition = new Vector3(vector2.x, vector2.y, 0.0f);
        arrow.FireArrow(true);
        this.PlaySound(this.m_HitTargetSoundPrefab);
      }
      else
      {
        Vector2 vector2 = Random.insideUnitCircle * Random.Range(0.0f, this.m_MaxRandomOffset);
        arrow.m_ArrowRoot.transform.localPosition = new Vector3(vector2.x, vector2.y, 0.0f);
        if ((double) Vector3.Distance(arrow.m_ArrowRoot.transform.position, this.m_CenterBone.position) > (double) this.m_targetRadius)
          arrow.m_ArrowRoot.transform.localPosition = Vector3.zero;
        if ((double) Vector3.Distance(arrow.m_ArrowRoot.transform.position, this.m_BullseyeCenterBone.position) < (double) this.m_bullseyeRadius)
          arrow.m_ArrowRoot.transform.localPosition = Vector3.zero;
        arrow.FireArrow(true);
        this.PlaySound(this.m_HitTargetSoundPrefab);
      }
    }
  }

  private void HitTargetDummy()
  {
    int index = 0;
    if (this.m_AvailableTargetDummyArrows.Count > 1)
      index = Random.Range(0, this.m_AvailableTargetDummyArrows.Count);
    TGTArrow targetDummyArrow = this.m_TargetDummyArrows[this.m_AvailableTargetDummyArrows[index]];
    targetDummyArrow.gameObject.SetActive(true);
    targetDummyArrow.FireArrow(false);
    TGTTargetDummy.Get().ArrowHit();
    this.PlaySound(this.m_HitTargetDummySoundPrefab);
    if (this.m_AvailableTargetDummyArrows.Count > 1)
      this.m_AvailableTargetDummyArrows.RemoveAt(index);
    else
      this.m_AvailableTargetDummyArrows.Clear();
  }

  private void ImpactTarget()
  {
    this.m_TargetPhysics.GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(this.m_HitIntensity * 0.25f, this.m_HitIntensity), 0.0f, 0.0f);
  }

  private void PlaySound(string soundPrefab)
  {
    if (string.IsNullOrEmpty(soundPrefab))
      return;
    string name = FileUtils.GameAssetPathToName(soundPrefab);
    if (string.IsNullOrEmpty(name))
      return;
    SoundManager.Get().LoadAndPlay(name, this.gameObject);
  }

  private bool IsOver(GameObject go)
  {
    return (bool) ((Object) go) && InputUtil.IsPlayMakerMouseInputAllowed(go) && UniversalInputManager.Get().InputIsOver(go);
  }
}
