﻿// Decompiled with JetBrains decompiler
// Type: PlayMakerUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using HutongGames.PlayMaker;
using UnityEngine;

public static class PlayMakerUtils
{
  public static bool DetermineAnimData(GameObject go, ScreenCategory[] animNameScreens, FsmString[] animNames, FsmString defaultAnimName, out string animName, out AnimationState animState)
  {
    animName = (string) null;
    animState = (AnimationState) null;
    if ((Object) go == (Object) null)
      return false;
    if ((Object) go.GetComponent<Animation>() == (Object) null)
    {
      Debug.LogWarning((object) string.Format("PlayMakerUtils.DetermineAnimData() - GameObject {0} is missing an animation component", (object) go));
      return false;
    }
    animName = PlayMakerUtils.DetermineAnimName(animNameScreens, animNames, defaultAnimName);
    if (string.IsNullOrEmpty(animName))
    {
      Debug.LogWarning((object) string.Format("PlayMakerUtils.DetermineAnimData() - GameObject {0} has an animation action with no animation name", (object) go));
      return false;
    }
    animState = go.GetComponent<Animation>()[animName];
    if (!((TrackedReference) animState == (TrackedReference) null))
      return true;
    Debug.LogWarning((object) string.Format("PlayMakerUtils.DetermineAnimData() - GameObject {0} is missing animation {1}", (object) go, (object) animName));
    return false;
  }

  public static string DetermineAnimName(ScreenCategory[] animNameScreens, FsmString[] animNames, FsmString defaultAnimName)
  {
    if (animNameScreens != null)
    {
      for (int index = 0; index < animNameScreens.Length; ++index)
      {
        if (animNameScreens[index] == PlatformSettings.Screen)
          return animNames[index].Value;
      }
    }
    return defaultAnimName.Value;
  }
}
