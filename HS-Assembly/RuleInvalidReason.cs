﻿// Decompiled with JetBrains decompiler
// Type: RuleInvalidReason
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class RuleInvalidReason
{
  public string DisplayError;
  public int CountParam;
  public bool IsMinimum;

  public RuleInvalidReason(string error, int countParam = 0, bool isMinimum = false)
  {
    this.DisplayError = error;
    this.CountParam = countParam;
    this.IsMinimum = isMinimum;
  }
}
