﻿// Decompiled with JetBrains decompiler
// Type: TargetListAnimUtils2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class TargetListAnimUtils2 : MonoBehaviour
{
  public List<GameObject> m_TargetList;

  public void PlayParticlesList2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          current.GetComponent<ParticleEmitter>().emit = true;
      }
    }
  }

  public void StopParticlesList2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          current.GetComponent<ParticleEmitter>().emit = false;
      }
    }
  }

  public void KillParticlesList2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          current.GetComponent<ParticleEmitter>().particles = new Particle[0];
      }
    }
  }

  public void PlayParticlesListInChildren2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          foreach (ParticleEmitter componentsInChild in current.GetComponentsInChildren<ParticleEmitter>())
            componentsInChild.emit = true;
        }
      }
    }
  }

  public void StopParticlesListInChildren2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          foreach (ParticleEmitter componentsInChild in current.GetComponentsInChildren<ParticleEmitter>())
            componentsInChild.emit = false;
        }
      }
    }
  }

  public void KillParticlesListInChildren2()
  {
    Particle[] particleArray = new Particle[0];
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          foreach (ParticleEmitter componentsInChild in current.GetComponentsInChildren<ParticleEmitter>())
          {
            componentsInChild.emit = false;
            componentsInChild.particles = particleArray;
          }
        }
      }
    }
  }

  public void PlayAnimationList2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          current.GetComponent<Animation>().Play();
      }
    }
  }

  public void StopAnimationList2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          current.GetComponent<Animation>().Stop();
      }
    }
  }

  public void PlayAnimationListInChildren2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          foreach (Animation componentsInChild in current.GetComponentsInChildren<Animation>())
            componentsInChild.Play();
        }
      }
    }
  }

  public void StopAnimationListInChildren2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          foreach (Animation componentsInChild in current.GetComponentsInChildren<Animation>())
            componentsInChild.Stop();
        }
      }
    }
  }

  public void ActivateHierarchyList2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          current.SetActive(true);
      }
    }
  }

  public void DeactivateHierarchyList2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          current.SetActive(false);
      }
    }
  }

  public void DestroyHierarchyList2()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current);
    }
  }

  public void FadeInList2(float FadeSec)
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        iTween.FadeTo(enumerator.Current, 1f, FadeSec);
    }
  }

  public void FadeOutList2(float FadeSec)
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        iTween.FadeTo(enumerator.Current, 0.0f, FadeSec);
    }
  }

  public void SetAlphaHierarchyList2(float alpha)
  {
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          foreach (Renderer componentsInChild in current.GetComponentsInChildren<Renderer>())
          {
            if (componentsInChild.material.HasProperty("_Color"))
            {
              Color color = componentsInChild.material.color;
              color.a = alpha;
              componentsInChild.material.color = color;
            }
          }
        }
      }
    }
  }
}
