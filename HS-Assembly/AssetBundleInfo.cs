﻿// Decompiled with JetBrains decompiler
// Type: AssetBundleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AssetBundleInfo
{
  public static readonly bool UseSharedDependencyBundle = true;
  public static readonly Map<AssetFamily, AssetFamilyBundleInfo> FamilyInfo = new Map<AssetFamily, AssetFamilyBundleInfo>() { { AssetFamily.Actor, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "actors", NumberOfBundles = 1 } }, { AssetFamily.AudioClip, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "sounds", NumberOfBundles = 1, NumberOfDownloadableLocaleBundles = 1 } }, { AssetFamily.Board, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "boards" } }, { AssetFamily.CardBack, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "cardbacks", NumberOfBundles = 1, Updatable = true } }, { AssetFamily.CardPrefab, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "cards", NumberOfBundles = 1 } }, { AssetFamily.CardPremium, new AssetFamilyBundleInfo() { TypeOf = typeof (Material), BundleName = "premiummaterials", NumberOfBundles = 1 } }, { AssetFamily.CardTexture, new AssetFamilyBundleInfo() { TypeOf = typeof (Texture), BundleName = "cardtextures", NumberOfBundles = 1 } }, { AssetFamily.CardXML, new AssetFamilyBundleInfo() { TypeOf = typeof (TextAsset), BundleName = "cardxml", Updatable = true } }, { AssetFamily.FontDef, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "fonts" } }, { AssetFamily.GameObject, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "gameobjects", NumberOfBundles = 1 } }, { AssetFamily.Movie, new AssetFamilyBundleInfo() { TypeOf = typeof (MovieTexture), BundleName = "movies", NumberOfBundles = 1 } }, { AssetFamily.Screen, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "uiscreens" } }, { AssetFamily.Sound, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "soundprefabs", NumberOfBundles = 1 } }, { AssetFamily.Spell, new AssetFamilyBundleInfo() { TypeOf = typeof (GameObject), BundleName = "spells", NumberOfBundles = 1 } }, { AssetFamily.Texture, new AssetFamilyBundleInfo() { TypeOf = typeof (Texture), BundleName = "textures" } } };
  public const string SharedBundleName = "shared";
  public const int NUM_BUNDLES_DEFAULT = 1;
  public const int NUM_ACTOR_BUNDLES = 2;
  public const int NUM_CARD_BUNDLES = 3;
  public const int NUM_CARDBACKS_BUNDLES = 1;
  public const int NUM_CARDTEXTURES_BUNDLES = 3;
  public const int NUM_PREMIUMMATERIALS_BUNDLES = 2;
  public const int NUM_GAMEOBJECTS_BUNDLES = 2;
  public const int NUM_MOVIE_BUNDLES = 1;
  public const int NUM_SOUND_BUNDLES = 2;
  public const int NUM_SOUNDPREFAB_BUNDLES = 1;
  public const int NUM_SPELL_BUNDLES = 3;
  public const int NUM_DOWNLOADABLE_SOUND_LOCALE_BUNDLES = 3;
  public const int NUM_DOWNLOADABLE_SPELL_LOCALE_BUNDLES = 9;

  public static string BundlePathPlatformModifier()
  {
    return "win/";
  }
}
