﻿// Decompiled with JetBrains decompiler
// Type: TutorialEntity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TutorialEntity : MissionEntity
{
  public static readonly Vector3 TUTORIAL_DIALOG_SCALE_PHONE = 1.5f * Vector3.one;
  public static readonly Vector3 HELP_POPUP_SCALE = 16f * Vector3.one;
  private Notification thatsABadPlayPopup;
  private Notification manaReminder;
  private TooltipPanel historyTooltip;

  protected override void HandleMulliganTagChange()
  {
  }

  public override bool NotifyOfPlayError(PlayErrors.ErrorType error, Entity errorSource)
  {
    if (error == PlayErrors.ErrorType.REQ_ENOUGH_MANA)
    {
      Actor actor = GameState.Get().GetFriendlySidePlayer().GetHero().GetCard().GetActor();
      if (errorSource.GetCost() > GameState.Get().GetFriendlySidePlayer().GetTag(GAME_TAG.RESOURCES))
      {
        Notification speechBubble = NotificationManager.Get().CreateSpeechBubble(GameStrings.Get("TUTORIAL02_JAINA_05"), Notification.SpeechBubbleDirection.BottomLeft, actor, true, true, 0.0f);
        SoundManager.Get().LoadAndPlay("VO_TUTORIAL_02_JAINA_05_20");
        NotificationManager.Get().DestroyNotification(speechBubble, 3.5f);
        Gameplay.Get().StartCoroutine(this.DisplayManaReminder(GameStrings.Get("TUTORIAL02_HELP_01")));
      }
      else
      {
        Notification speechBubble = NotificationManager.Get().CreateSpeechBubble(GameStrings.Get("TUTORIAL02_JAINA_04"), Notification.SpeechBubbleDirection.BottomLeft, actor, true, true, 0.0f);
        SoundManager.Get().LoadAndPlay("VO_TUTORIAL_02_JAINA_04_19");
        NotificationManager.Get().DestroyNotification(speechBubble, 3.5f);
        Gameplay.Get().StartCoroutine(this.DisplayManaReminder(GameStrings.Get("TUTORIAL02_HELP_03")));
      }
      return true;
    }
    if (error == PlayErrors.ErrorType.REQ_ATTACK_GREATER_THAN_0 && errorSource.GetCardId() == "TU4a_006")
      return true;
    if (error != PlayErrors.ErrorType.REQ_TARGET_TAUNTER)
      return false;
    SoundManager.Get().LoadAndPlay("UI_no_can_do");
    GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_TAUNT);
    GameState.Get().ShowEnemyTauntCharacters();
    this.HighlightTaunters();
    return true;
  }

  [DebuggerHidden]
  private IEnumerator DisplayManaReminder(string reminderText)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TutorialEntity.\u003CDisplayManaReminder\u003Ec__Iterator201() { reminderText = reminderText, \u003C\u0024\u003EreminderText = reminderText, \u003C\u003Ef__this = this };
  }

  private void HighlightTaunters()
  {
    using (List<Card>.Enumerator enumerator = GameState.Get().GetOpposingSidePlayer().GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (current.GetEntity().HasTaunt() && !current.GetEntity().IsStealthed())
        {
          NotificationManager.Get().DestroyAllPopUps();
          NotificationManager.Get().DestroyNotification(NotificationManager.Get().CreateFadeArrow(new Vector3(current.transform.position.x - 2f, current.transform.position.y, current.transform.position.z), new Vector3(0.0f, 270f, 0.0f)), 3f);
          break;
        }
      }
    }
  }

  public override bool NotifyOfTooltipDisplay(TooltipZone tooltip)
  {
    ZoneDeck component = tooltip.targetObject.GetComponent<ZoneDeck>();
    if ((Object) component == (Object) null)
      return false;
    if (component.m_Side == Player.Side.FRIENDLY)
    {
      string headline = GameStrings.Get("GAMEPLAY_TOOLTIP_DECK_HEADLINE");
      string bodytext = GameStrings.Get("TUTORIAL_TOOLTIP_DECK_DESCRIPTION");
      if ((bool) UniversalInputManager.UsePhoneUI)
        tooltip.ShowGameplayTooltipLarge(headline, bodytext);
      else
        tooltip.ShowGameplayTooltip(headline, bodytext);
      return true;
    }
    if (component.m_Side != Player.Side.OPPOSING)
      return false;
    string headline1 = GameStrings.Get("GAMEPLAY_TOOLTIP_ENEMYDECK_HEADLINE");
    string bodytext1 = GameStrings.Get("TUTORIAL_TOOLTIP_ENEMYDECK_DESC");
    if ((bool) UniversalInputManager.UsePhoneUI)
      tooltip.ShowGameplayTooltipLarge(headline1, bodytext1);
    else
      tooltip.ShowGameplayTooltip(headline1, bodytext1);
    return true;
  }

  public override void NotifyOfHeroesFinishedAnimatingInMulligan()
  {
    Board.Get().FindCollider("DragPlane").GetComponent<Collider>().enabled = false;
    this.HandleMissionEvent(54);
  }

  public override bool ShouldDoOpeningTaunts()
  {
    return false;
  }

  public override bool ShouldHandleCoin()
  {
    return false;
  }

  public override bool NotifyOfBattlefieldCardClicked(Entity clickedEntity, bool wasInTargetMode)
  {
    if (!clickedEntity.IsControlledByLocalUser())
      return true;
    Network.Options.Option selectedNetworkOption = GameState.Get().GetSelectedNetworkOption();
    if (selectedNetworkOption == null || selectedNetworkOption.Main == null)
      return true;
    Entity entity = GameState.Get().GetEntity(selectedNetworkOption.Main.ID);
    if (!wasInTargetMode || entity == null || clickedEntity == entity)
      return true;
    string cardId = entity.GetCardId();
    if (!(cardId == "CS2_022") && !(cardId == "CS2_029") && !(cardId == "CS2_034"))
      return true;
    this.ShowDontHurtYourselfPopup(clickedEntity.GetCard().transform.position);
    return false;
  }

  private void ShowDontHurtYourselfPopup(Vector3 origin)
  {
    if ((Object) this.thatsABadPlayPopup != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.thatsABadPlayPopup);
    this.thatsABadPlayPopup = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(origin.x - 3f, origin.y, origin.z), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("TUTORIAL01_HELP_07"), true);
    NotificationManager.Get().DestroyNotification(this.thatsABadPlayPopup, 2.5f);
  }

  protected void HandleGameStartEvent()
  {
    MulliganManager.Get().ForceMulliganActive(true);
    MulliganManager.Get().SkipCardChoosing();
    TurnStartManager.Get().BeginListeningForTurnEvents();
  }

  protected void UserPressedStartButton(UIEvent e)
  {
    this.HandleMissionEvent(55);
  }

  protected TutorialNotification ShowTutorialDialog(string headlineGameString, string bodyTextGameString, string buttonGameString, Vector2 materialOffset, bool swapMaterial = false)
  {
    return NotificationManager.Get().CreateTutorialDialog(headlineGameString, bodyTextGameString, buttonGameString, new UIEvent.Handler(this.UserPressedStartButton), materialOffset, swapMaterial);
  }

  public override void NotifyOfHistoryTokenMousedOver(GameObject mousedOverTile)
  {
    this.historyTooltip = TooltipPanelManager.Get().CreateKeywordPanel(0);
    this.historyTooltip.Reset();
    this.historyTooltip.Initialize(GameStrings.Get("TUTORIAL_TOOLTIP_HISTORY_HEADLINE"), GameStrings.Get("TUTORIAL_TOOLTIP_HISTORY_DESC"));
    Vector3 vector3 = !UniversalInputManager.Get().IsTouchMode() ? new Vector3(-1.140343f, 0.1916952f, 0.4895353f) : new Vector3(1f, 0.1916952f, 1.2f);
    this.historyTooltip.transform.parent = mousedOverTile.GetComponent<HistoryCard>().m_mainCardActor.transform;
    float num = 0.4792188f;
    this.historyTooltip.transform.localPosition = vector3;
    this.historyTooltip.transform.localScale = new Vector3(num, num, num);
  }

  public override void NotifyOfHistoryTokenMousedOut()
  {
    if (!((Object) this.historyTooltip != (Object) null))
      return;
    Object.Destroy((Object) this.historyTooltip.gameObject);
  }

  protected virtual void NotifyOfManaError()
  {
  }

  protected void SetTutorialProgress(TutorialProgress val)
  {
    if (GameMgr.Get().IsSpectator())
      return;
    if (!Network.ShouldBeConnectedToAurora())
    {
      if (GameUtils.AreAllTutorialsComplete(val))
        Options.Get().SetBool(Option.CONNECT_TO_AURORA, true);
      Options.Get().SetEnum<TutorialProgress>(Option.LOCAL_TUTORIAL_PROGRESS, val);
    }
    AdTrackingManager.Get().TrackTutorialProgress(val.ToString());
    NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>().CampaignProgress = val;
    NetCache.Get().NetCacheChanged<NetCache.NetCacheProfileProgress>();
  }

  protected void SetTutorialLostProgress(TutorialProgress val)
  {
    Options.Get().SetInt(Option.TUTORIAL_LOST_PROGRESS, Options.Get().GetInt(Option.TUTORIAL_LOST_PROGRESS) | 1 << (int) val);
  }

  protected bool DidLoseTutorial(TutorialProgress val)
  {
    int num = Options.Get().GetInt(Option.TUTORIAL_LOST_PROGRESS);
    bool flag = false;
    if ((num & 1 << (int) val) > 0)
      flag = true;
    return flag;
  }

  protected void ResetTutorialLostProgress()
  {
    Options.Get().SetInt(Option.TUTORIAL_LOST_PROGRESS, 0);
  }
}
