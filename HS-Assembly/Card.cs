﻿// Decompiled with JetBrains decompiler
// Type: Card
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusGame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class Card : MonoBehaviour
{
  public static readonly Vector3 ABOVE_DECK_OFFSET = new Vector3(0.0f, 3.6f, 0.0f);
  public static readonly Vector3 IN_DECK_OFFSET = new Vector3(0.0f, 0.0f, 0.1f);
  public static readonly Vector3 IN_DECK_SCALE = new Vector3(0.81f, 0.81f, 0.81f);
  public static readonly Vector3 IN_DECK_ANGLES = new Vector3(-90f, 270f, 0.0f);
  public static readonly Quaternion IN_DECK_ROTATION = Quaternion.Euler(Card.IN_DECK_ANGLES);
  public static readonly Vector3 IN_DECK_HIDDEN_ANGLES = new Vector3(270f, 90f, 0.0f);
  public static readonly Quaternion IN_DECK_HIDDEN_ROTATION = Quaternion.Euler(Card.IN_DECK_HIDDEN_ANGLES);
  protected CardSound[] m_announcerLine = new CardSound[3];
  private bool m_actorReady = true;
  private bool m_cardStandInInteractive = true;
  protected bool m_shown = true;
  private bool m_inputEnabled = true;
  private float m_keywordDeathDelaySec = 0.6f;
  public const float DEFAULT_KEYWORD_DEATH_DELAY_SEC = 0.6f;
  protected Entity m_entity;
  protected CardDef m_cardDef;
  protected CardEffect m_playEffect;
  protected CardEffect m_attackEffect;
  protected CardEffect m_deathEffect;
  protected CardEffect m_lifetimeEffect;
  protected List<CardEffect> m_subOptionEffects;
  protected List<CardEffect> m_triggerEffects;
  protected Map<Network.HistBlockStart, CardEffect> m_proxyEffects;
  protected List<CardEffect> m_allEffects;
  protected CardEffect m_customKeywordSpell;
  protected List<EmoteEntry> m_emotes;
  protected Spell m_customSummonSpell;
  protected Spell m_customSpawnSpell;
  protected Spell m_customSpawnSpellOverride;
  protected Spell m_customDeathSpell;
  protected Spell m_customDeathSpellOverride;
  private int m_spellLoadCount;
  protected string m_actorName;
  protected Actor m_actor;
  protected Actor m_actorWaitingToBeReplaced;
  private bool m_actorLoading;
  private bool m_transitioningZones;
  private bool m_hasBeenGrabbedByEnemyActionHandler;
  private Zone m_zone;
  private Zone m_prevZone;
  private int m_zonePosition;
  private int m_predictedZonePosition;
  private bool m_doNotSort;
  private bool m_beingDrawnByOpponent;
  private ZoneTransitionStyle m_transitionStyle;
  private bool m_doNotWarpToNewZone;
  private float m_transitionDelay;
  private bool m_holdingForLinkedCardSwitch;
  protected bool m_shouldShowTooltip;
  protected bool m_showTooltip;
  protected bool m_overPlayfield;
  protected bool m_mousedOver;
  protected bool m_mousedOverByOpponent;
  protected bool m_attacking;
  private int m_activeDeathEffectCount;
  private bool m_ignoreDeath;
  private bool m_suppressDeathEffects;
  private bool m_suppressDeathSounds;
  private bool m_suppressKeywordDeaths;
  private bool m_suppressActorTriggerSpell;
  private bool m_suppressPlaySounds;
  private bool m_isBattleCrySource;
  private bool m_secretTriggered;
  private bool m_secretSheathed;
  private Spell m_activeSpawnSpell;
  private Player.Side? m_sideBeforeReturnFromDeath;

  public override string ToString()
  {
    if (this.m_entity == null)
      return "UNKNOWN CARD";
    return this.m_entity.ToString();
  }

  public Entity GetEntity()
  {
    return this.m_entity;
  }

  public void SetEntity(Entity entity)
  {
    this.m_entity = entity;
  }

  public void Destroy()
  {
    if ((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null)
      this.m_actor.Destroy();
    this.DestroyCardDefAssets();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  public Player GetController()
  {
    if (this.m_entity == null)
      return (Player) null;
    return this.m_entity.GetController();
  }

  public Player.Side GetControllerSide()
  {
    if (this.m_entity == null)
      return Player.Side.NEUTRAL;
    return this.m_entity.GetControllerSide();
  }

  public Entity GetHero()
  {
    Player controller = this.GetController();
    if (controller == null)
      return (Entity) null;
    return controller.GetHero();
  }

  public Card GetHeroCard()
  {
    Entity hero = this.GetHero();
    if (hero == null)
      return (Card) null;
    return hero.GetCard();
  }

  public Entity GetHeroPower()
  {
    Player controller = this.GetController();
    if (controller == null)
      return (Entity) null;
    return controller.GetHeroPower();
  }

  public Card GetHeroPowerCard()
  {
    Entity heroPower = this.GetHeroPower();
    if (heroPower == null)
      return (Card) null;
    return heroPower.GetCard();
  }

  public TAG_PREMIUM GetPremium()
  {
    if (this.m_entity == null)
      return TAG_PREMIUM.NORMAL;
    return this.m_entity.GetPremiumType();
  }

  public bool IsOverPlayfield()
  {
    return this.m_overPlayfield;
  }

  public void NotifyOverPlayfield()
  {
    this.m_overPlayfield = true;
    this.UpdateActorState();
  }

  public void NotifyLeftPlayfield()
  {
    this.m_overPlayfield = false;
    this.UpdateActorState();
  }

  public void NotifyMousedOver()
  {
    this.m_mousedOver = true;
    this.UpdateActorState();
    this.UpdateProposedManaUsage();
    if ((bool) ((UnityEngine.Object) RemoteActionHandler.Get()) && (bool) ((UnityEngine.Object) TargetReticleManager.Get()))
      RemoteActionHandler.Get().NotifyOpponentOfMouseOverEntity(this.GetEntity().GetCard());
    if (GameState.Get() != null)
      GameState.Get().GetGameEntity().NotifyOfCardMousedOver(this.GetEntity());
    if (this.m_zone is ZoneHand)
    {
      Spell actorSpell1 = this.GetActorSpell(SpellType.SPELL_POWER_HINT_BURST, true);
      if ((UnityEngine.Object) actorSpell1 != (UnityEngine.Object) null)
        actorSpell1.Deactivate();
      Spell actorSpell2 = this.GetActorSpell(SpellType.SPELL_POWER_HINT_IDLE, true);
      if ((UnityEngine.Object) actorSpell2 != (UnityEngine.Object) null)
        actorSpell2.Deactivate();
      ArmsDealingHintSpell actorSpell3 = this.GetActorSpell(SpellType.ARMS_DEALING, false) as ArmsDealingHintSpell;
      if ((UnityEngine.Object) actorSpell3 != (UnityEngine.Object) null)
        actorSpell3.NotifyCardMousedOver();
      if (GameState.Get().IsMulliganManagerActive())
        SoundManager.Get().LoadAndPlay("collection_manager_card_mouse_over", this.gameObject);
    }
    if (this.m_entity.IsControlledByFriendlySidePlayer() && (this.m_entity.IsHero() || this.m_zone is ZonePlay) && !this.m_transitioningZones)
    {
      bool flag1 = this.m_entity.HasSpellPower() || this.m_entity.HasSpellPowerDouble();
      bool flag2 = this.m_entity.HasHeroPowerDamage();
      if (flag1 || flag2)
      {
        Spell actorSpell = this.GetActorSpell(SpellType.SPELL_POWER_HINT_BURST, true);
        if ((UnityEngine.Object) actorSpell != (UnityEngine.Object) null)
          actorSpell.Reactivate();
        if (flag1)
          ZoneMgr.Get().FindZoneOfType<ZoneHand>(this.m_zone.m_Side).OnSpellPowerEntityMousedOver();
      }
    }
    if (!this.m_entity.IsWeapon() || !this.m_entity.IsExhausted() || (!((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_actor.GetAttackObject() != (UnityEngine.Object) null)))
      return;
    this.m_actor.GetAttackObject().Enlarge(1f);
  }

  public void NotifyMousedOut()
  {
    this.m_mousedOver = false;
    this.UpdateActorState();
    this.UpdateProposedManaUsage();
    if ((bool) ((UnityEngine.Object) RemoteActionHandler.Get()))
      RemoteActionHandler.Get().NotifyOpponentOfMouseOut();
    if ((bool) ((UnityEngine.Object) TooltipPanelManager.Get()))
      TooltipPanelManager.Get().HideKeywordHelp();
    if ((bool) ((UnityEngine.Object) CardTypeBanner.Get()))
      CardTypeBanner.Get().Hide(this.m_actor);
    if (GameState.Get() != null)
      GameState.Get().GetGameEntity().NotifyOfCardMousedOff(this.GetEntity());
    if (this.m_entity.HasSpellPower() && this.m_entity.IsControlledByFriendlySidePlayer() && (this.m_entity.IsHero() || this.m_zone is ZonePlay))
      ZoneMgr.Get().FindZoneOfType<ZoneHand>(this.m_zone.m_Side).OnSpellPowerEntityMousedOut();
    ArmsDealingHintSpell actorSpell = this.GetActorSpell(SpellType.ARMS_DEALING, false) as ArmsDealingHintSpell;
    if ((UnityEngine.Object) actorSpell != (UnityEngine.Object) null)
      actorSpell.NotifyCardMousedOut();
    if (!this.m_entity.IsWeapon() || !this.m_entity.IsExhausted() || (!((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_actor.GetAttackObject() != (UnityEngine.Object) null)))
      return;
    this.m_actor.GetAttackObject().ScaleToZero();
  }

  public bool IsMousedOver()
  {
    return this.m_mousedOver;
  }

  public void NotifyOpponentMousedOverThisCard()
  {
    this.m_mousedOverByOpponent = true;
    this.UpdateActorState();
  }

  public void NotifyOpponentMousedOffThisCard()
  {
    this.m_mousedOverByOpponent = false;
    this.UpdateActorState();
  }

  public void NotifyPickedUp()
  {
    this.m_transitioningZones = false;
    this.CutoffFriendlyCardDraw();
  }

  public void NotifyTargetingCanceled()
  {
    if (this.m_entity.IsCharacter() && !this.IsAttacking())
    {
      Spell attackSpellForInput = this.GetActorAttackSpellForInput();
      if ((UnityEngine.Object) attackSpellForInput != (UnityEngine.Object) null)
      {
        if (this.m_entity.HasTag(GAME_TAG.IMMUNE_WHILE_ATTACKING) && !this.ShouldShowImmuneVisuals())
          this.GetActor().ActivateSpellDeathState(SpellType.IMMUNE);
        switch (attackSpellForInput.GetActiveState())
        {
          case SpellStateType.NONE:
          case SpellStateType.CANCEL:
            break;
          default:
            attackSpellForInput.ActivateState(SpellStateType.CANCEL);
            break;
        }
      }
    }
    this.ActivateHandStateSpells();
  }

  public bool IsInputEnabled()
  {
    return this.m_inputEnabled;
  }

  public void SetInputEnabled(bool enabled)
  {
    this.m_inputEnabled = enabled;
    this.UpdateActorState();
  }

  public bool IsAllowedToShowTooltip()
  {
    return !((UnityEngine.Object) this.m_zone == (UnityEngine.Object) null) && (this.m_zone.m_ServerTag == TAG_ZONE.PLAY || this.m_zone.m_ServerTag == TAG_ZONE.SECRET || (this.m_zone.m_ServerTag != TAG_ZONE.HAND || this.m_zone.m_Side == Player.Side.OPPOSING)) && (GameState.Get() == null || !this.m_entity.IsHero() || GameState.Get().GetGameEntity().ShouldShowHeroTooltips());
  }

  public bool IsAbleToShowTooltip()
  {
    return this.m_entity != null && !((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null) && !((UnityEngine.Object) BigCard.Get() == (UnityEngine.Object) null);
  }

  public bool GetShouldShowTooltip()
  {
    return this.m_shouldShowTooltip;
  }

  public void SetShouldShowTooltip()
  {
    if (!this.IsAllowedToShowTooltip() || this.m_shouldShowTooltip)
      return;
    this.m_shouldShowTooltip = true;
  }

  public void ShowTooltip()
  {
    if (this.m_showTooltip)
      return;
    this.m_showTooltip = true;
    this.UpdateTooltip();
  }

  public void HideTooltip()
  {
    this.m_shouldShowTooltip = false;
    if (!this.m_showTooltip)
      return;
    this.m_showTooltip = false;
    this.UpdateTooltip();
  }

  public bool IsShowingTooltip()
  {
    return this.m_showTooltip;
  }

  public void UpdateTooltip()
  {
    if (this.GetShouldShowTooltip() && this.IsAllowedToShowTooltip() && this.IsAbleToShowTooltip() && this.m_showTooltip)
    {
      if (!((UnityEngine.Object) BigCard.Get() != (UnityEngine.Object) null))
        return;
      BigCard.Get().Show(this);
    }
    else
    {
      this.m_showTooltip = false;
      this.m_shouldShowTooltip = false;
      if (!((UnityEngine.Object) BigCard.Get() != (UnityEngine.Object) null))
        return;
      BigCard.Get().Hide(this);
    }
  }

  public bool IsAttacking()
  {
    return this.m_attacking;
  }

  public void EnableAttacking(bool enable)
  {
    this.m_attacking = enable;
  }

  public bool WillIgnoreDeath()
  {
    return this.m_ignoreDeath;
  }

  public void IgnoreDeath(bool ignore)
  {
    this.m_ignoreDeath = ignore;
  }

  public bool WillSuppressDeathEffects()
  {
    return this.m_suppressDeathEffects;
  }

  public void SuppressDeathEffects(bool suppress)
  {
    this.m_suppressDeathEffects = suppress;
  }

  public bool WillSuppressDeathSounds()
  {
    return this.m_suppressDeathSounds;
  }

  public void SuppressDeathSounds(bool suppress)
  {
    this.m_suppressDeathSounds = suppress;
  }

  public bool WillSuppressKeywordDeaths()
  {
    return this.m_suppressKeywordDeaths;
  }

  public void SuppressKeywordDeaths(bool suppress)
  {
    this.m_suppressKeywordDeaths = suppress;
  }

  public float GetKeywordDeathDelaySec()
  {
    return this.m_keywordDeathDelaySec;
  }

  public void SetKeywordDeathDelaySec(float sec)
  {
    this.m_keywordDeathDelaySec = sec;
  }

  public bool WillSuppressActorTriggerSpell()
  {
    return this.m_suppressActorTriggerSpell;
  }

  public void SuppressActorTriggerSpell(bool suppress)
  {
    this.m_suppressActorTriggerSpell = suppress;
  }

  public bool WillSuppressPlaySounds()
  {
    return this.m_suppressPlaySounds;
  }

  public void SuppressPlaySounds(bool suppress)
  {
    this.m_suppressPlaySounds = suppress;
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  public void ShowCard()
  {
    if (this.m_shown)
      return;
    this.m_shown = true;
    this.ShowImpl();
  }

  private void ShowImpl()
  {
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null)
      return;
    this.m_actor.Show();
    this.RefreshActor();
  }

  public void HideCard()
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    this.HideImpl();
  }

  private void HideImpl()
  {
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null)
      return;
    this.m_actor.Hide();
  }

  public void SetBattleCrySource(bool source)
  {
    this.m_isBattleCrySource = source;
    if (!((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null))
      return;
    if (source)
    {
      SceneUtils.SetLayer(this.m_actor.gameObject, GameLayer.IgnoreFullScreenEffects);
    }
    else
    {
      SceneUtils.SetLayer(this.m_actor.gameObject, GameLayer.Default);
      SceneUtils.SetLayer(this.m_actor.GetMeshRenderer().gameObject, GameLayer.CardRaycast);
    }
  }

  public void DoTauntNotification()
  {
    if ((UnityEngine.Object) this.m_activeSpawnSpell != (UnityEngine.Object) null && this.m_activeSpawnSpell.IsActive())
      return;
    iTween.PunchScale(this.m_actor.gameObject, new Vector3(0.2f, 0.2f, 0.2f), 0.5f);
  }

  public void UpdateProposedManaUsage()
  {
    if (GameState.Get().GetSelectedOption() != -1)
      return;
    Player player = GameState.Get().GetPlayer(this.GetEntity().GetControllerId());
    if (!player.IsFriendlySide() || !player.HasTag(GAME_TAG.CURRENT_PLAYER))
      return;
    if (this.m_mousedOver)
    {
      if (this.m_entity.GetZone() != TAG_ZONE.HAND && !this.m_entity.IsHeroPower() || !GameState.Get().IsOption(this.m_entity) || (this.m_entity.IsSpell() && player.HasTag(GAME_TAG.SPELLS_COST_HEALTH) || this.m_entity.HasTag(GAME_TAG.CARD_COSTS_HEALTH)))
        return;
      player.ProposeManaCrystalUsage(this.m_entity);
    }
    else
      player.CancelAllProposedMana(this.m_entity);
  }

  public CardDef GetCardDef()
  {
    return this.m_cardDef;
  }

  public void LoadCardDef(CardDef cardDef)
  {
    if ((UnityEngine.Object) this.m_cardDef == (UnityEngine.Object) cardDef)
      return;
    this.m_cardDef = cardDef;
    this.InitCardDefAssets();
    if (!((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null))
      return;
    this.m_actor.SetCardDef(this.m_cardDef);
    this.m_actor.UpdateAllComponents();
  }

  public void PurgeSpells()
  {
    using (List<CardEffect>.Enumerator enumerator = this.m_allEffects.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.PurgeSpells();
    }
  }

  private bool ShouldPreloadCardAssets()
  {
    if (ApplicationMgr.IsPublic())
      return false;
    return Options.Get().GetBool(Option.PRELOAD_CARD_ASSETS, false);
  }

  public void OverrideCustomSpawnSpell(Spell spell)
  {
    this.m_customSpawnSpellOverride = this.SetupOverrideSpell(this.m_customSpawnSpellOverride, spell);
  }

  public void OverrideCustomDeathSpell(Spell spell)
  {
    this.m_customDeathSpellOverride = this.SetupOverrideSpell(this.m_customDeathSpellOverride, spell);
  }

  public Texture GetPortraitTexture()
  {
    if ((UnityEngine.Object) this.m_cardDef == (UnityEngine.Object) null)
      return (Texture) null;
    return this.m_cardDef.GetPortraitTexture();
  }

  public Material GetGoldenMaterial()
  {
    if ((UnityEngine.Object) this.m_cardDef == (UnityEngine.Object) null)
      return (Material) null;
    return this.m_cardDef.GetPremiumPortraitMaterial();
  }

  public CardEffect GetPlayEffect()
  {
    return this.m_playEffect;
  }

  public CardEffect GetOrCreateProxyEffect(Network.HistBlockStart blockStart, CardEffectDef proxyEffectDef)
  {
    if (this.m_proxyEffects == null)
      this.m_proxyEffects = new Map<Network.HistBlockStart, CardEffect>();
    if (this.m_proxyEffects.ContainsKey(blockStart))
      return this.m_proxyEffects[blockStart];
    CardEffect effect = new CardEffect(proxyEffectDef, this);
    this.InitEffect(proxyEffectDef, ref effect);
    this.m_proxyEffects.Add(blockStart, effect);
    return effect;
  }

  public Spell GetPlaySpell(bool loadIfNeeded = true)
  {
    if (this.m_playEffect == null)
      return (Spell) null;
    return this.m_playEffect.GetSpell(loadIfNeeded);
  }

  public List<CardSoundSpell> GetPlaySoundSpells(bool loadIfNeeded = true)
  {
    if (this.m_playEffect == null)
      return (List<CardSoundSpell>) null;
    return this.m_playEffect.GetSoundSpells(loadIfNeeded);
  }

  public Spell GetAttackSpell(bool loadIfNeeded = true)
  {
    if (this.m_attackEffect == null)
      return (Spell) null;
    return this.m_attackEffect.GetSpell(loadIfNeeded);
  }

  public List<CardSoundSpell> GetAttackSoundSpells(bool loadIfNeeded = true)
  {
    if (this.m_attackEffect == null)
      return (List<CardSoundSpell>) null;
    return this.m_attackEffect.GetSoundSpells(loadIfNeeded);
  }

  public List<CardSoundSpell> GetDeathSoundSpells(bool loadIfNeeded = true)
  {
    if (this.m_deathEffect == null)
      return (List<CardSoundSpell>) null;
    return this.m_deathEffect.GetSoundSpells(loadIfNeeded);
  }

  public Spell GetLifetimeSpell(bool loadIfNeeded = true)
  {
    if (this.m_lifetimeEffect == null)
      return (Spell) null;
    return this.m_lifetimeEffect.GetSpell(loadIfNeeded);
  }

  public List<CardSoundSpell> GetLifetimeSoundSpells(bool loadIfNeeded = true)
  {
    if (this.m_lifetimeEffect == null)
      return (List<CardSoundSpell>) null;
    return this.m_lifetimeEffect.GetSoundSpells(loadIfNeeded);
  }

  public CardEffect GetSubOptionEffect(int index)
  {
    if (this.m_subOptionEffects == null)
      return (CardEffect) null;
    if (index < 0)
      return (CardEffect) null;
    if (index >= this.m_subOptionEffects.Count)
      return (CardEffect) null;
    return this.m_subOptionEffects[index];
  }

  public Spell GetSubOptionSpell(int index, bool loadIfNeeded = true)
  {
    CardEffect subOptionEffect = this.GetSubOptionEffect(index);
    if (subOptionEffect == null)
      return (Spell) null;
    return subOptionEffect.GetSpell(loadIfNeeded);
  }

  public List<CardSoundSpell> GetSubOptionSoundSpells(int index, bool loadIfNeeded = true)
  {
    CardEffect subOptionEffect = this.GetSubOptionEffect(index);
    if (subOptionEffect == null)
      return (List<CardSoundSpell>) null;
    return subOptionEffect.GetSoundSpells(loadIfNeeded);
  }

  public CardEffect GetTriggerEffect(int index)
  {
    if (this.m_triggerEffects == null)
      return (CardEffect) null;
    if (index < 0)
      return (CardEffect) null;
    if (index >= this.m_triggerEffects.Count)
      return (CardEffect) null;
    return this.m_triggerEffects[index];
  }

  public Spell GetTriggerSpell(int index, bool loadIfNeeded = true)
  {
    CardEffect triggerEffect = this.GetTriggerEffect(index);
    if (triggerEffect == null)
      return (Spell) null;
    return triggerEffect.GetSpell(loadIfNeeded);
  }

  public List<CardSoundSpell> GetTriggerSoundSpells(int index, bool loadIfNeeded = true)
  {
    CardEffect triggerEffect = this.GetTriggerEffect(index);
    if (triggerEffect == null)
      return (List<CardSoundSpell>) null;
    return triggerEffect.GetSoundSpells(loadIfNeeded);
  }

  public Spell GetCustomSummonSpell()
  {
    return this.m_customSummonSpell;
  }

  public Spell GetCustomSpawnSpell()
  {
    return this.m_customSpawnSpell;
  }

  public Spell GetCustomSpawnSpellOverride()
  {
    return this.m_customSpawnSpellOverride;
  }

  public Spell GetCustomDeathSpell()
  {
    return this.m_customDeathSpell;
  }

  public Spell GetCustomDeathSpellOverride()
  {
    return this.m_customDeathSpellOverride;
  }

  public AudioSource GetAnnouncerLine(Card.AnnouncerLineType type)
  {
    CardSound cardSound = this.m_announcerLine[(int) type];
    if (cardSound == null || (UnityEngine.Object) cardSound.GetSound(true) == (UnityEngine.Object) null)
      cardSound = this.m_announcerLine[0];
    return cardSound.GetSound(true);
  }

  public EmoteEntry GetEmoteEntry(EmoteType emoteType)
  {
    if (this.m_emotes == null)
      return (EmoteEntry) null;
    if (SpecialEventManager.Get().IsEventActive(SpecialEventType.LUNAR_NEW_YEAR, false))
    {
      if (emoteType == EmoteType.GREETINGS)
      {
        using (List<EmoteEntry>.Enumerator enumerator = this.m_emotes.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            EmoteEntry current = enumerator.Current;
            if (current.GetEmoteType() == EmoteType.EVENT_LUNAR_NEW_YEAR)
              return current;
          }
        }
      }
    }
    else if (SpecialEventManager.Get().IsEventActive(SpecialEventType.FEAST_OF_WINTER_VEIL, false) && emoteType == EmoteType.GREETINGS)
    {
      using (List<EmoteEntry>.Enumerator enumerator = this.m_emotes.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          EmoteEntry current = enumerator.Current;
          if (current.GetEmoteType() == EmoteType.EVENT_WINTER_VEIL)
            return current;
        }
      }
    }
    using (List<EmoteEntry>.Enumerator enumerator = this.m_emotes.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EmoteEntry current = enumerator.Current;
        if (current.GetEmoteType() == emoteType)
          return current;
      }
    }
    return (EmoteEntry) null;
  }

  public Spell GetBestSummonSpell()
  {
    bool standard;
    return this.GetBestSummonSpell(out standard);
  }

  public Spell GetBestSummonSpell(out bool standard)
  {
    if ((UnityEngine.Object) this.m_customSummonSpell != (UnityEngine.Object) null)
    {
      standard = false;
      return this.m_customSummonSpell;
    }
    standard = true;
    return this.GetActorSpell(this.m_cardDef.DetermineSummonInSpell_HandToPlay(this), true);
  }

  public Spell GetBestSpawnSpell()
  {
    bool standard;
    return this.GetBestSpawnSpell(out standard);
  }

  public Spell GetBestSpawnSpell(out bool standard)
  {
    standard = false;
    if ((bool) ((UnityEngine.Object) this.m_customSpawnSpellOverride))
      return this.m_customSpawnSpellOverride;
    if ((bool) ((UnityEngine.Object) this.m_customSpawnSpell))
      return this.m_customSpawnSpell;
    standard = true;
    if (this.m_entity.IsControlledByFriendlySidePlayer())
      return this.GetActorSpell(SpellType.FRIENDLY_SPAWN_MINION, true);
    return this.GetActorSpell(SpellType.OPPONENT_SPAWN_MINION, true);
  }

  public Spell GetBestDeathSpell()
  {
    bool standard;
    return this.GetBestDeathSpell(out standard);
  }

  public Spell GetBestDeathSpell(out bool standard)
  {
    return this.GetBestDeathSpell(this.m_actor, out standard);
  }

  private Spell GetBestDeathSpell(Actor actor)
  {
    bool standard;
    return this.GetBestDeathSpell(actor, out standard);
  }

  private Spell GetBestDeathSpell(Actor actor, out bool standard)
  {
    standard = false;
    if (!this.m_entity.IsSilenced())
    {
      if ((bool) ((UnityEngine.Object) this.m_customDeathSpellOverride))
        return this.m_customDeathSpellOverride;
      if ((bool) ((UnityEngine.Object) this.m_customDeathSpell))
        return this.m_customDeathSpell;
    }
    standard = true;
    return actor.GetSpell(SpellType.DEATH);
  }

  public void ActivateCharacterPlayEffects()
  {
    if (!this.m_suppressPlaySounds)
    {
      this.ActivateSoundSpellList(this.m_playEffect.GetSoundSpells(true));
      this.m_suppressPlaySounds = false;
    }
    this.ActivateLifetimeEffects();
  }

  public void ActivateCharacterAttackEffects()
  {
    this.ActivateSoundSpellList(this.m_attackEffect.GetSoundSpells(true));
  }

  public void ActivateCharacterDeathEffects()
  {
    if (this.m_suppressDeathEffects)
      return;
    if (!this.m_suppressDeathSounds)
    {
      if ((this.m_emotes != null ? this.m_emotes.FindIndex((Predicate<EmoteEntry>) (e =>
      {
        if (e != null)
          return e.GetEmoteType() == EmoteType.DEATH_LINE;
        return false;
      })) : -1) >= 0)
        this.PlayEmote(EmoteType.DEATH_LINE);
      else
        this.ActivateSoundSpellList(this.m_deathEffect.GetSoundSpells(true));
      this.m_suppressDeathSounds = false;
    }
    this.DeactivateLifetimeEffects();
  }

  public void ActivateLifetimeEffects()
  {
    if (this.m_lifetimeEffect == null || this.m_entity.IsSilenced())
      return;
    Spell spell = this.m_lifetimeEffect.GetSpell(true);
    if ((UnityEngine.Object) spell != (UnityEngine.Object) null)
    {
      spell.Deactivate();
      spell.ActivateState(SpellStateType.BIRTH);
    }
    if (this.m_lifetimeEffect.GetSoundSpells(true) == null)
      return;
    this.ActivateSoundSpellList(this.m_lifetimeEffect.GetSoundSpells(true));
  }

  public void DeactivateLifetimeEffects()
  {
    if (this.m_lifetimeEffect == null)
      return;
    Spell spell = this.m_lifetimeEffect.GetSpell(true);
    if (!((UnityEngine.Object) spell != (UnityEngine.Object) null))
      return;
    switch (spell.GetActiveState())
    {
      case SpellStateType.NONE:
        break;
      case SpellStateType.DEATH:
        break;
      default:
        spell.ActivateState(SpellStateType.DEATH);
        break;
    }
  }

  public void ActivateCustomKeywordEffect()
  {
    if (this.m_customKeywordSpell == null)
      return;
    Spell spell = this.m_customKeywordSpell.GetSpell(true);
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("Card.ActivateCustomKeywordEffect() -- failed to load custom keyword spell for card {0}", (object) this.ToString()));
    }
    else
    {
      spell.transform.parent = this.m_actor.transform;
      spell.ActivateState(SpellStateType.BIRTH);
    }
  }

  public void DeactivateCustomKeywordEffect()
  {
    if (this.m_customKeywordSpell == null)
      return;
    Spell spell = this.m_customKeywordSpell.GetSpell(false);
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null || !spell.IsActive())
      return;
    spell.ActivateState(SpellStateType.DEATH);
  }

  public bool ActivateSoundSpellList(List<CardSoundSpell> soundSpells)
  {
    if (soundSpells == null || soundSpells.Count == 0)
      return false;
    bool flag = false;
    for (int index = 0; index < soundSpells.Count; ++index)
    {
      this.ActivateSoundSpell(soundSpells[index]);
      flag = true;
    }
    return flag;
  }

  public bool ActivateSoundSpell(CardSoundSpell soundSpell)
  {
    if ((UnityEngine.Object) soundSpell == (UnityEngine.Object) null)
      return false;
    GameEntity gameEntity = GameState.Get().GetGameEntity();
    if (gameEntity == null)
      return false;
    if (gameEntity.ShouldDelayCardSoundSpells())
      this.StartCoroutine(this.WaitThenActivateSoundSpell(soundSpell));
    else
      soundSpell.Reactivate();
    return true;
  }

  public bool HasActiveEmoteSound()
  {
    if (this.m_emotes == null)
      return false;
    using (List<EmoteEntry>.Enumerator enumerator = this.m_emotes.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardSoundSpell spell = enumerator.Current.GetSpell(false);
        if ((UnityEngine.Object) spell != (UnityEngine.Object) null && spell.IsActive())
          return true;
      }
    }
    return false;
  }

  public CardSoundSpell PlayEmote(EmoteType emoteType)
  {
    return this.PlayEmote(emoteType, Notification.SpeechBubbleDirection.None);
  }

  public CardSoundSpell PlayEmote(EmoteType emoteType, Notification.SpeechBubbleDirection overrideDirection)
  {
    EmoteEntry emoteEntry = this.GetEmoteEntry(emoteType);
    CardSoundSpell emoteSpell = emoteEntry != null ? emoteEntry.GetSpell(true) : (CardSoundSpell) null;
    if (!this.m_entity.IsHero())
      return (CardSoundSpell) null;
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null)
      return (CardSoundSpell) null;
    if ((UnityEngine.Object) emoteSpell != (UnityEngine.Object) null)
    {
      emoteSpell.Reactivate();
      if (emoteSpell.IsActive())
      {
        for (int index = 0; index < this.m_emotes.Count; ++index)
        {
          EmoteEntry emote = this.m_emotes[index];
          if (emote != emoteEntry)
          {
            Spell spell = (Spell) emote.GetSpell(false);
            if ((bool) ((UnityEngine.Object) spell))
              spell.Deactivate();
          }
        }
      }
      GameState.Get().GetGameEntity().OnEmotePlayed(this, emoteType, emoteSpell);
    }
    Notification.SpeechBubbleDirection direction = Notification.SpeechBubbleDirection.BottomLeft;
    if (this.GetEntity().IsControlledByOpposingSidePlayer())
      direction = Notification.SpeechBubbleDirection.TopRight;
    if (overrideDirection != Notification.SpeechBubbleDirection.None)
      direction = overrideDirection;
    string speechText = (string) null;
    if ((UnityEngine.Object) emoteSpell != (UnityEngine.Object) null)
    {
      speechText = string.Empty;
      if (emoteSpell is CardSpecificVoSpell)
      {
        CardSpecificVoData bestVoiceData = ((CardSpecificVoSpell) emoteSpell).GetBestVoiceData();
        if (bestVoiceData != null && !string.IsNullOrEmpty(bestVoiceData.m_GameStringKey))
          speechText = GameStrings.Get(bestVoiceData.m_GameStringKey);
      }
    }
    if (string.IsNullOrEmpty(speechText) && emoteEntry != null && !string.IsNullOrEmpty(emoteEntry.GetGameStringKey()))
      speechText = GameStrings.Get(emoteEntry.GetGameStringKey());
    if (speechText != null)
    {
      Notification speechBubble = NotificationManager.Get().CreateSpeechBubble(speechText, direction, this.m_actor, true, true, 0.0f);
      float delaySeconds = 1.5f;
      if ((bool) ((UnityEngine.Object) emoteSpell))
      {
        AudioSource activeAudioSource = emoteSpell.GetActiveAudioSource();
        if ((bool) ((UnityEngine.Object) activeAudioSource) && (double) delaySeconds < (double) activeAudioSource.clip.length)
          delaySeconds = activeAudioSource.clip.length;
      }
      NotificationManager.Get().DestroyNotification(speechBubble, delaySeconds);
    }
    return emoteSpell;
  }

  private void InitCardDefAssets()
  {
    this.InitEffect(this.m_cardDef.m_PlayEffectDef, ref this.m_playEffect);
    this.InitEffect(this.m_cardDef.m_AttackEffectDef, ref this.m_attackEffect);
    this.InitEffect(this.m_cardDef.m_DeathEffectDef, ref this.m_deathEffect);
    this.InitEffect(this.m_cardDef.m_LifetimeEffectDef, ref this.m_lifetimeEffect);
    this.InitEffect(this.m_cardDef.m_CustomKeywordSpellPath, ref this.m_customKeywordSpell);
    this.InitEffectList(this.m_cardDef.m_SubOptionEffectDefs, ref this.m_subOptionEffects);
    this.InitEffectList(this.m_cardDef.m_TriggerEffectDefs, ref this.m_triggerEffects);
    this.InitSound(this.m_cardDef.m_AnnouncerLinePath, ref this.m_announcerLine[0], true);
    this.InitSound(this.m_cardDef.m_AnnouncerLineBeforeVersusPath, ref this.m_announcerLine[1], false);
    this.InitSound(this.m_cardDef.m_AnnouncerLineAfterVersusPath, ref this.m_announcerLine[2], false);
    this.InitEmoteList();
  }

  private void InitEffect(CardEffectDef effectDef, ref CardEffect effect)
  {
    this.DestroyCardEffect(ref effect);
    if (effectDef == null)
      return;
    effect = new CardEffect(effectDef, this);
    if (this.m_allEffects == null)
      this.m_allEffects = new List<CardEffect>();
    this.m_allEffects.Add(effect);
    if (!this.ShouldPreloadCardAssets())
      return;
    effect.LoadAll();
  }

  private void InitEffect(string spellPath, ref CardEffect effect)
  {
    this.DestroyCardEffect(ref effect);
    if (spellPath == null)
      return;
    effect = new CardEffect(spellPath, this);
    if (this.m_allEffects == null)
      this.m_allEffects = new List<CardEffect>();
    this.m_allEffects.Add(effect);
    if (!this.ShouldPreloadCardAssets())
      return;
    effect.LoadAll();
  }

  private void InitEffectList(List<CardEffectDef> effectDefs, ref List<CardEffect> effects)
  {
    this.DestroyCardEffectList(ref effects);
    if (effectDefs == null)
      return;
    effects = new List<CardEffect>();
    for (int index = 0; index < effectDefs.Count; ++index)
    {
      CardEffectDef effectDef = effectDefs[index];
      CardEffect cardEffect = (CardEffect) null;
      if (effectDef != null)
      {
        cardEffect = new CardEffect(effectDef, this);
        if (this.m_allEffects == null)
          this.m_allEffects = new List<CardEffect>();
        this.m_allEffects.Add(cardEffect);
        if (this.ShouldPreloadCardAssets())
          cardEffect.LoadAll();
      }
      effects.Add(cardEffect);
    }
  }

  private void InitSound(string path, ref CardSound cardSound, bool alwaysValid)
  {
    this.DestroyCardSound(ref cardSound);
    if (string.IsNullOrEmpty(path))
      return;
    cardSound = new CardSound(path, this, alwaysValid);
    if (!this.ShouldPreloadCardAssets())
      return;
    cardSound.GetSound(true);
  }

  private void InitEmoteList()
  {
    this.DestroyEmoteList();
    if (this.m_cardDef.m_EmoteDefs == null)
      return;
    this.m_emotes = new List<EmoteEntry>();
    for (int index = 0; index < this.m_cardDef.m_EmoteDefs.Count; ++index)
    {
      EmoteEntryDef emoteDef = this.m_cardDef.m_EmoteDefs[index];
      EmoteEntry emoteEntry = new EmoteEntry(emoteDef.m_emoteType, emoteDef.m_emoteSoundSpellPath, emoteDef.m_emoteGameStringKey, this);
      if (this.ShouldPreloadCardAssets())
        emoteEntry.GetSpell(true);
      this.m_emotes.Add(emoteEntry);
    }
  }

  private Spell SetupOverrideSpell(Spell existingSpell, Spell spell)
  {
    if ((UnityEngine.Object) existingSpell != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) existingSpell.gameObject);
    SpellUtils.SetupSpell(spell, (Component) this);
    return spell;
  }

  private void DestroyCardDefAssets()
  {
    this.DestroyCardEffect(ref this.m_playEffect);
    this.DestroyCardEffect(ref this.m_attackEffect);
    this.DestroyCardEffect(ref this.m_deathEffect);
    this.DestroyCardEffect(ref this.m_lifetimeEffect);
    this.DestroyCardEffectList(ref this.m_subOptionEffects);
    this.DestroyCardEffectList(ref this.m_triggerEffects);
    if (this.m_proxyEffects != null)
    {
      List<CardEffect> effects = new List<CardEffect>((IEnumerable<CardEffect>) this.m_proxyEffects.Values);
      this.DestroyCardEffectList(ref effects);
      this.m_proxyEffects.Clear();
    }
    this.DestroyCardEffect(ref this.m_customKeywordSpell);
    for (int index = 0; index < ((IEnumerable<CardSound>) this.m_announcerLine).Count<CardSound>(); ++index)
      this.DestroyCardSound(ref this.m_announcerLine[index]);
    this.DestroyEmoteList();
    this.DestroyCardAsset<Spell>(ref this.m_customSummonSpell);
    this.DestroyCardAsset<Spell>(ref this.m_customSpawnSpell);
    this.DestroyCardAsset<Spell>(ref this.m_customSpawnSpellOverride);
    this.DestroyCardAsset<Spell>(ref this.m_customDeathSpell);
    this.DestroyCardAsset<Spell>(ref this.m_customDeathSpellOverride);
  }

  private void DestroyCardEffect(ref CardEffect effect)
  {
    if (effect == null)
      return;
    effect.Clear();
    effect = (CardEffect) null;
  }

  private void DestroyCardSound(ref CardSound cardSound)
  {
    if (cardSound == null)
      return;
    cardSound.Clear();
    cardSound = (CardSound) null;
  }

  private void DestroyCardEffectList(ref List<CardEffect> effects)
  {
    if (effects == null)
      return;
    using (List<CardEffect>.Enumerator enumerator = effects.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Clear();
    }
    effects = (List<CardEffect>) null;
  }

  private void DestroyCardAsset<T>(ref T asset) where T : Component
  {
    if ((UnityEngine.Object) asset == (UnityEngine.Object) null)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) asset.gameObject);
    asset = (T) null;
  }

  private void DestroyCardAsset<T>(T asset) where T : Component
  {
    if ((UnityEngine.Object) asset == (UnityEngine.Object) null)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) asset.gameObject);
  }

  private void DestroySpellList<T>(List<T> spells) where T : Spell
  {
    if (spells == null)
      return;
    for (int index = 0; index < spells.Count; ++index)
      this.DestroyCardAsset<T>(spells[index]);
    spells = (List<T>) null;
  }

  private void DestroyEmoteList()
  {
    if (this.m_emotes == null)
      return;
    for (int index = 0; index < this.m_emotes.Count; ++index)
      this.m_emotes[index].Clear();
    this.m_emotes = (List<EmoteEntry>) null;
  }

  private void CancelActiveSpells()
  {
    SpellUtils.ActivateCancelIfNecessary(this.m_playEffect.GetSpell(false));
    if (this.m_subOptionEffects != null)
    {
      using (List<CardEffect>.Enumerator enumerator = this.m_subOptionEffects.GetEnumerator())
      {
        while (enumerator.MoveNext())
          SpellUtils.ActivateCancelIfNecessary(enumerator.Current.GetSpell(false));
      }
    }
    if (this.m_triggerEffects == null)
      return;
    using (List<CardEffect>.Enumerator enumerator = this.m_triggerEffects.GetEnumerator())
    {
      while (enumerator.MoveNext())
        SpellUtils.ActivateCancelIfNecessary(enumerator.Current.GetSpell(false));
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitThenActivateSoundSpell(CardSoundSpell soundSpell)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CWaitThenActivateSoundSpell\u003Ec__Iterator2E5() { soundSpell = soundSpell, \u003C\u0024\u003EsoundSpell = soundSpell };
  }

  public void OnTagsChanged(TagDeltaList changeList)
  {
    bool flag = false;
    for (int index = 0; index < changeList.Count; ++index)
    {
      TagDelta change = changeList[index];
      GAME_TAG tag = (GAME_TAG) change.tag;
      switch (tag)
      {
        case GAME_TAG.HEALTH:
        case GAME_TAG.ATK:
        case GAME_TAG.COST:
          flag = true;
          break;
        default:
          if (tag != GAME_TAG.DURABILITY && tag != GAME_TAG.ARMOR)
          {
            this.OnTagChanged(change);
            break;
          }
          goto case GAME_TAG.HEALTH;
      }
    }
    if (!flag || this.m_entity.IsLoadingAssets() || !this.IsActorReady())
      return;
    this.UpdateActorComponents();
  }

  public void OnMetaData(Network.HistMetaData metaData)
  {
    if (metaData.MetaType != HistoryMeta.Type.DAMAGE && metaData.MetaType != HistoryMeta.Type.HEALING || (!this.CanShowActorVisuals() || this.m_entity.GetZone() != TAG_ZONE.PLAY))
      return;
    Spell actorSpell = this.GetActorSpell(SpellType.DAMAGE, true);
    if ((UnityEngine.Object) actorSpell == (UnityEngine.Object) null)
    {
      this.UpdateActorComponents();
    }
    else
    {
      actorSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnSpellFinished_UpdateActorComponents));
      if (this.m_entity.IsCharacter())
      {
        int damage = metaData.MetaType != HistoryMeta.Type.HEALING ? metaData.Data : -metaData.Data;
        ((DamageSplatSpell) actorSpell).SetDamage(damage);
        actorSpell.ActivateState(SpellStateType.ACTION);
        BoardEvents boardEvents = BoardEvents.Get();
        if (!((UnityEngine.Object) boardEvents != (UnityEngine.Object) null))
          return;
        if (metaData.MetaType == HistoryMeta.Type.HEALING)
          boardEvents.HealEvent(this, (float) -metaData.Data);
        else
          boardEvents.DamageEvent(this, (float) metaData.Data);
      }
      else
        actorSpell.Activate();
    }
  }

  public void OnTagChanged(TagDelta change)
  {
    GAME_TAG tag = (GAME_TAG) change.tag;
    switch (tag)
    {
      case GAME_TAG.DURABILITY:
label_120:
        if (!this.CanShowActorVisuals())
          break;
        this.UpdateActorComponents();
        break;
      case GAME_TAG.SILENCED:
        if (!this.CanShowActorVisuals())
          break;
        if (change.newValue == 1)
        {
          this.m_actor.ActivateSpellBirthState(SpellType.SILENCE);
          this.DeactivateLifetimeEffects();
          break;
        }
        this.m_actor.ActivateSpellDeathState(SpellType.SILENCE);
        this.ActivateLifetimeEffects();
        break;
      case GAME_TAG.WINDFURY:
        if (!this.CanShowActorVisuals())
          break;
        if (change.newValue >= 1)
          this.m_actor.ActivateSpellBirthState(SpellType.WINDFURY_BURST);
        Spell spell = this.m_actor.GetSpell(SpellType.WINDFURY_IDLE);
        if (!((UnityEngine.Object) spell != (UnityEngine.Object) null))
          break;
        if (change.newValue >= 1)
        {
          spell.ActivateState(SpellStateType.BIRTH);
          break;
        }
        spell.ActivateState(SpellStateType.CANCEL);
        break;
      case GAME_TAG.TAUNT:
label_36:
        if (!this.CanShowActorVisuals())
          break;
        if (change.newValue == 1)
        {
          this.m_actor.ActivateTaunt();
          break;
        }
        this.m_actor.DeactivateTaunt();
        break;
      case GAME_TAG.STEALTH:
label_84:
        if (!this.CanShowActorVisuals())
          break;
        if (change.newValue == 1)
        {
          this.m_actor.ActivateSpellBirthState(SpellType.STEALTH);
          this.m_actor.ActivateSpellDeathState(SpellType.UNTARGETABLE);
        }
        else
        {
          this.m_actor.ActivateSpellDeathState(SpellType.STEALTH);
          if (this.m_entity.HasTag(GAME_TAG.CANT_BE_TARGETED_BY_HERO_POWERS))
            this.m_actor.ActivateSpellBirthState(SpellType.UNTARGETABLE);
        }
        if (!this.m_entity.HasTaunt())
          break;
        this.m_actor.ActivateTaunt();
        break;
      case GAME_TAG.SPELLPOWER:
label_95:
        if (!this.CanShowActorVisuals())
          break;
        if (change.newValue > 0)
        {
          this.m_actor.ActivateSpellBirthState(SpellType.SPELL_POWER);
          break;
        }
        this.m_actor.ActivateSpellDeathState(SpellType.SPELL_POWER);
        break;
      case GAME_TAG.DIVINE_SHIELD:
        if (!this.CanShowActorVisuals() || this.m_entity.GetController() != null && !this.m_entity.GetController().IsFriendlySide() && this.m_entity.IsObfuscated())
          break;
        if (change.newValue == 1)
        {
          this.m_actor.ActivateSpellBirthState(SpellType.DIVINE_SHIELD);
          break;
        }
        this.m_actor.ActivateSpellDeathState(SpellType.DIVINE_SHIELD);
        break;
      case GAME_TAG.CHARGE:
        if (!this.CanShowActorVisuals())
          break;
        Spell actorSpell = this.GetActorSpell(SpellType.Zzz, true);
        if ((UnityEngine.Object) actorSpell != (UnityEngine.Object) null)
          actorSpell.ActivateState(SpellStateType.DEATH);
        if (change.newValue != 1)
          break;
        this.m_actor.ActivateSpellBirthState(SpellType.CHARGE);
        break;
      default:
        switch (tag - 43)
        {
          case (GAME_TAG) 0:
            if (!this.CanShowActorVisuals() || change.newValue == change.oldValue)
              return;
            if (GameState.Get().IsTurnStartManagerActive() && this.m_entity.IsControlledByFriendlySidePlayer())
            {
              TurnStartManager.Get().NotifyOfExhaustedChange(this, change);
              return;
            }
            this.ShowExhaustedChange(change.newValue);
            return;
          case (GAME_TAG) 1:
            if (!this.CanShowActorVisuals())
              return;
            if (this.m_entity.IsEnraged())
            {
              if (!this.m_actor.GetSpell(SpellType.ENRAGE).IsActive())
                this.m_actor.ActivateSpellBirthState(SpellType.ENRAGE);
            }
            else
              this.m_actor.ActivateSpellDeathState(SpellType.ENRAGE);
            this.UpdateActorComponents();
            return;
          case GAME_TAG.TAG_SCRIPT_DATA_NUM_1:
          case GAME_TAG.TAG_SCRIPT_DATA_ENT_1:
          case GAME_TAG.TAG_SCRIPT_DATA_ENT_2:
            goto label_120;
          case GAME_TAG.TIMEOUT:
            if (!this.CanShowActorVisuals() || this.m_entity.HasCharge() || this.m_entity.ReferencesAutoAttack())
              return;
            this.m_actor.ActivateSpellBirthState(SpellType.Zzz);
            return;
          default:
            switch (tag - 362)
            {
              case (GAME_TAG) 0:
                if (!this.CanShowActorVisuals())
                  return;
                if (change.newValue == 1)
                {
                  this.m_actor.ActivateSpellBirthState(SpellType.AURA);
                  return;
                }
                this.m_actor.ActivateSpellDeathState(SpellType.AURA);
                return;
              case (GAME_TAG) 1:
                if (!this.CanShowActorVisuals())
                  return;
                this.ToggleBauble(change.newValue == 1, SpellType.POISONOUS);
                return;
              case GAME_TAG.TAG_SCRIPT_DATA_ENT_2:
                if (!this.CanShowActorVisuals())
                  return;
                if (change.newValue == 1)
                {
                  this.m_actor.ActivateSpellBirthState(SpellType.AUTO_CAST);
                  return;
                }
                this.m_actor.ActivateSpellDeathState(SpellType.AUTO_CAST);
                return;
              default:
                switch (tag - 391)
                {
                  case (GAME_TAG) 0:
                    if (!this.CanShowActorVisuals() || this.m_entity.GetController() != null && this.m_entity.GetController().IsFriendlySide())
                      return;
                    this.UpdateActor(false);
                    return;
                  case (GAME_TAG) 1:
                    if (!this.CanShowActorVisuals())
                      return;
                    if (change.oldValue == 0 && change.newValue > 0)
                    {
                      this.m_actor.ActivateSpellBirthState(SpellType.BURNING);
                      return;
                    }
                    if (change.oldValue <= 0 || change.newValue != 0)
                      return;
                    this.m_actor.ActivateSpellDeathState(SpellType.BURNING);
                    if (this.m_entity.IsSilenced())
                      return;
                    this.m_actor.ActivateSpellBirthState(SpellType.EXPLODE);
                    return;
                  case GAME_TAG.TAG_SCRIPT_DATA_ENT_2:
                    goto label_95;
                  default:
                    switch (tag - 401)
                    {
                      case (GAME_TAG) 0:
                        if (!this.CanShowActorVisuals())
                          return;
                        if (change.newValue == 1)
                        {
                          this.m_actor.ActivateSpellBirthState(SpellType.EVIL_GLOW);
                          return;
                        }
                        this.m_actor.ActivateSpellDeathState(SpellType.EVIL_GLOW);
                        return;
                      case GAME_TAG.TAG_SCRIPT_DATA_NUM_1:
                        if (!this.CanShowActorVisuals())
                          return;
                        this.ToggleBauble(change.newValue == 1, SpellType.INSPIRE);
                        return;
                      default:
                        switch (tag - 420)
                        {
                          case (GAME_TAG) 0:
                            if (!this.CanShowActorVisuals())
                              return;
                            if (change.newValue == 1)
                            {
                              this.m_actor.ActivateSpellDeathState(SpellType.ELECTRIC_CHARGE_LEVEL_SMALL);
                              this.m_actor.ActivateSpellBirthState(SpellType.ELECTRIC_CHARGE_LEVEL_MEDIUM);
                              return;
                            }
                            if (change.newValue == 2)
                            {
                              this.m_actor.ActivateSpellDeathState(SpellType.ELECTRIC_CHARGE_LEVEL_SMALL);
                              this.m_actor.ActivateSpellDeathState(SpellType.ELECTRIC_CHARGE_LEVEL_MEDIUM);
                              this.m_actor.ActivateSpellBirthState(SpellType.ELECTRIC_CHARGE_LEVEL_LARGE);
                              return;
                            }
                            this.m_actor.ActivateSpellDeathState(SpellType.ELECTRIC_CHARGE_LEVEL_LARGE);
                            return;
                          case (GAME_TAG) 1:
                            if (!this.CanShowActorVisuals())
                              return;
                            if (change.newValue == 1)
                            {
                              this.m_actor.ActivateSpellBirthState(SpellType.HEAVILY_ARMORED);
                              return;
                            }
                            this.m_actor.ActivateSpellDeathState(SpellType.HEAVILY_ARMORED);
                            return;
                          case GAME_TAG.TAG_SCRIPT_DATA_NUM_1:
                            if (!this.CanShowActorVisuals())
                              return;
                            if (this.ShouldShowImmuneVisuals())
                            {
                              this.m_actor.ActivateSpellBirthState(SpellType.IMMUNE);
                              return;
                            }
                            this.m_actor.ActivateSpellDeathState(SpellType.IMMUNE);
                            return;
                          default:
                            if (tag != GAME_TAG.TAUNT_READY)
                            {
                              if (tag != GAME_TAG.STEALTH_READY)
                              {
                                if (tag != GAME_TAG.TRIGGER_VISUAL)
                                {
                                  if (tag != GAME_TAG.ENRAGED)
                                  {
                                    if (tag != GAME_TAG.DEATHRATTLE)
                                    {
                                      if (tag != GAME_TAG.CANT_PLAY)
                                      {
                                        if (tag != GAME_TAG.IMMUNE)
                                        {
                                          if (tag != GAME_TAG.FROZEN)
                                          {
                                            if (tag != GAME_TAG.NUM_TURNS_IN_PLAY)
                                            {
                                              if (tag != GAME_TAG.ARMOR)
                                              {
                                                if (tag != GAME_TAG.CANT_BE_TARGETED_BY_HERO_POWERS)
                                                {
                                                  if (tag != GAME_TAG.HEALTH_MINIMUM)
                                                  {
                                                    if (tag != GAME_TAG.CUSTOM_KEYWORD_EFFECT)
                                                    {
                                                      if (tag != GAME_TAG.SHIFTING)
                                                      {
                                                        if (tag != GAME_TAG.ARMS_DEALING)
                                                        {
                                                          if (tag != GAME_TAG.CARD_COSTS_HEALTH)
                                                            return;
                                                          this.UpdateCardCostHealth(change);
                                                          return;
                                                        }
                                                        if (!this.CanShowActorVisuals())
                                                          return;
                                                        if (change.newValue >= 1)
                                                        {
                                                          this.m_actor.ActivateSpellBirthState(SpellType.ARMS_DEALING);
                                                          return;
                                                        }
                                                        this.m_actor.ActivateSpellDeathState(SpellType.ARMS_DEALING);
                                                        return;
                                                      }
                                                      if (!this.CanShowActorVisuals())
                                                        return;
                                                      if (change.newValue == 1)
                                                      {
                                                        this.m_actor.ActivateSpellBirthState(SpellType.SHIFTING);
                                                        return;
                                                      }
                                                      this.m_actor.ActivateSpellDeathState(SpellType.SHIFTING);
                                                      return;
                                                    }
                                                    if (!this.CanShowActorVisuals())
                                                      return;
                                                    if (change.newValue == 1)
                                                    {
                                                      this.ActivateCustomKeywordEffect();
                                                      return;
                                                    }
                                                    this.DeactivateCustomKeywordEffect();
                                                    return;
                                                  }
                                                  if (!this.CanShowActorVisuals())
                                                    return;
                                                  if (change.newValue > 0)
                                                  {
                                                    this.m_actor.ActivateSpellBirthState(SpellType.SHOUT_BUFF);
                                                    return;
                                                  }
                                                  this.m_actor.ActivateSpellDeathState(SpellType.SHOUT_BUFF);
                                                  return;
                                                }
                                                if (!this.CanShowActorVisuals() || this.m_entity.IsStealthed())
                                                  return;
                                                if (change.newValue == 1)
                                                {
                                                  this.m_actor.ActivateSpellBirthState(SpellType.UNTARGETABLE);
                                                  return;
                                                }
                                                this.m_actor.ActivateSpellDeathState(SpellType.UNTARGETABLE);
                                                return;
                                              }
                                              goto label_120;
                                            }
                                            else
                                            {
                                              if (!this.CanShowActorVisuals() || !this.m_entity.IsAsleep())
                                                return;
                                              this.m_actor.ActivateSpellBirthState(SpellType.Zzz);
                                              return;
                                            }
                                          }
                                          else
                                          {
                                            if (!this.CanShowActorVisuals())
                                              return;
                                            if (change.oldValue == 0 && change.newValue > 0)
                                            {
                                              SoundManager.Get().LoadAndPlay("FrostBoltHit1");
                                              this.m_actor.ActivateSpellBirthState(SpellType.FROZEN);
                                              return;
                                            }
                                            if (change.oldValue <= 0 || change.newValue != 0)
                                              return;
                                            SoundManager.Get().LoadAndPlay("FrostArmorTarget1");
                                            this.m_actor.ActivateSpellDeathState(SpellType.FROZEN);
                                            return;
                                          }
                                        }
                                        else
                                        {
                                          if (!this.CanShowActorVisuals())
                                            return;
                                          if (this.ShouldShowImmuneVisuals())
                                          {
                                            this.m_actor.ActivateSpellBirthState(SpellType.IMMUNE);
                                            return;
                                          }
                                          this.m_actor.ActivateSpellDeathState(SpellType.IMMUNE);
                                          return;
                                        }
                                      }
                                      else
                                      {
                                        if (change.newValue != 1)
                                          return;
                                        this.CancelActiveSpells();
                                        return;
                                      }
                                    }
                                    else
                                    {
                                      if (!this.CanShowActorVisuals())
                                        return;
                                      this.ToggleDeathrattle(change.newValue == 1);
                                      return;
                                    }
                                  }
                                  else
                                  {
                                    if (!this.CanShowActorVisuals())
                                      return;
                                    if (change.newValue == 1)
                                    {
                                      this.m_actor.ActivateSpellBirthState(SpellType.ENRAGE);
                                      return;
                                    }
                                    this.m_actor.ActivateSpellDeathState(SpellType.ENRAGE);
                                    return;
                                  }
                                }
                                else
                                {
                                  if (this.m_entity.IsEnchantment())
                                  {
                                    Entity entity = GameState.Get().GetEntity(this.m_entity.GetAttached());
                                    if (entity != null && (UnityEngine.Object) entity.GetCard() != (UnityEngine.Object) null && entity.GetCard().CanShowActorVisuals())
                                      entity.GetCard().ToggleBauble(change.newValue == 1, SpellType.TRIGGER);
                                  }
                                  if (!this.CanShowActorVisuals())
                                    return;
                                  this.ToggleBauble(change.newValue == 1, SpellType.TRIGGER);
                                  return;
                                }
                              }
                              else
                                goto label_84;
                            }
                            else
                              goto label_36;
                        }
                    }
                }
            }
        }
    }
  }

  public void UpdateCardCostHealth(TagDelta change)
  {
    if (this.m_entity.IsControlledByFriendlySidePlayer())
    {
      Card mousedOverCard = InputManager.Get().GetMousedOverCard();
      if ((UnityEngine.Object) mousedOverCard != (UnityEngine.Object) null)
      {
        Entity entity = mousedOverCard.GetEntity();
        if (entity == this.m_entity)
        {
          if (change.newValue > 0)
            ManaCrystalMgr.Get().CancelAllProposedMana(entity);
          else
            ManaCrystalMgr.Get().ProposeManaCrystalUsage(entity);
        }
      }
    }
    if (!this.CanShowActorVisuals())
      return;
    if (change.newValue > 0)
      this.m_actor.ActivateSpellBirthState(SpellType.SPELLS_COST_HEALTH);
    else
      this.m_actor.ActivateSpellDeathState(SpellType.SPELLS_COST_HEALTH);
  }

  public bool CanShowActorVisuals()
  {
    return !this.m_entity.IsLoadingAssets() && !((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null) && this.m_actor.IsShown();
  }

  public bool ShouldShowImmuneVisuals()
  {
    if (this.m_entity != null && this.m_entity.HasTag(GAME_TAG.IMMUNE))
      return !this.m_entity.HasTag(GAME_TAG.DONT_SHOW_IMMUNE);
    return false;
  }

  public void ActivateStateSpells()
  {
    if (this.m_entity.GetController() != null && !this.m_entity.GetController().IsFriendlySide() && this.m_entity.IsObfuscated())
      return;
    if (this.m_entity.HasCharge())
      this.m_actor.ActivateSpellBirthState(SpellType.CHARGE);
    if (this.m_entity.HasTaunt())
      this.m_actor.ActivateTaunt();
    if (this.m_entity.IsStealthed())
      this.m_actor.ActivateSpellBirthState(SpellType.STEALTH);
    else if (this.m_entity.HasTag(GAME_TAG.CANT_BE_TARGETED_BY_HERO_POWERS))
      this.m_actor.ActivateSpellBirthState(SpellType.UNTARGETABLE);
    if (this.m_entity.HasDivineShield())
      this.m_actor.ActivateSpellBirthState(SpellType.DIVINE_SHIELD);
    if (this.m_entity.HasSpellPower())
      this.m_actor.ActivateSpellBirthState(SpellType.SPELL_POWER);
    if (this.m_entity.HasHeroPowerDamage())
      this.m_actor.ActivateSpellBirthState(SpellType.SPELL_POWER);
    if (this.ShouldShowImmuneVisuals())
      this.m_actor.ActivateSpellBirthState(SpellType.IMMUNE);
    if (this.m_entity.HasHealthMin())
      this.m_actor.ActivateSpellBirthState(SpellType.SHOUT_BUFF);
    if (this.m_entity.IsAsleep())
      this.m_actor.ActivateSpellBirthState(SpellType.Zzz);
    if (this.m_entity.IsEnraged())
      this.m_actor.ActivateSpellBirthState(SpellType.ENRAGE);
    if (this.m_entity.HasWindfury())
    {
      Spell spell = this.m_actor.GetSpell(SpellType.WINDFURY_IDLE);
      if ((UnityEngine.Object) spell != (UnityEngine.Object) null)
        spell.ActivateState(SpellStateType.BIRTH);
    }
    if (this.m_entity.HasDeathrattle())
      this.m_actor.ActivateSpellBirthState(SpellType.DEATHRATTLE_IDLE);
    if (this.m_entity.IsSilenced())
      this.m_actor.ActivateSpellBirthState(SpellType.SILENCE);
    SpellType spellType = this.PrioritizedBauble();
    if (spellType != SpellType.NONE)
      this.m_actor.ActivateSpellBirthState(spellType);
    if (this.m_entity.HasAura())
      this.m_actor.ActivateSpellBirthState(SpellType.AURA);
    if (this.m_entity.HasTag(GAME_TAG.AI_MUST_PLAY))
      this.m_actor.ActivateSpellBirthState(SpellType.AUTO_CAST);
    if (this.m_entity.IsFrozen())
      this.m_actor.ActivateSpellBirthState(SpellType.FROZEN);
    if (this.m_entity.HasTag(GAME_TAG.HEAVILY_ARMORED))
      this.m_actor.ActivateSpellBirthState(SpellType.HEAVILY_ARMORED);
    if (this.m_entity.HasTag(GAME_TAG.ARMS_DEALING))
      this.m_actor.ActivateSpellBirthState(SpellType.ARMS_DEALING);
    Player controller = this.m_entity.GetController();
    if (controller != null)
    {
      if (this.m_entity.IsHeroPower())
      {
        if (controller.HasTag(GAME_TAG.STEADY_SHOT_CAN_TARGET))
          this.m_actor.ActivateSpellBirthState(SpellType.STEADY_SHOT_CAN_TARGET);
        if (controller.HasTag(GAME_TAG.CURRENT_HEROPOWER_DAMAGE_BONUS) && controller.IsHeroPowerAffectedByBonusDamage())
          this.m_actor.ActivateSpellBirthState(SpellType.CURRENT_HEROPOWER_DAMAGE_BONUS);
        if (this.m_entity.HasTag(GAME_TAG.ELECTRIC_CHARGE_LEVEL))
        {
          switch (this.m_entity.GetTag(GAME_TAG.ELECTRIC_CHARGE_LEVEL))
          {
            case 1:
              this.m_actor.ActivateSpellBirthState(SpellType.ELECTRIC_CHARGE_LEVEL_MEDIUM);
              break;
            case 2:
              this.m_actor.ActivateSpellBirthState(SpellType.ELECTRIC_CHARGE_LEVEL_LARGE);
              break;
          }
        }
      }
      if (this.m_entity.IsHero())
      {
        if (controller.HasTag(GAME_TAG.LOCK_AND_LOAD))
          this.m_actor.ActivateSpellBirthState(SpellType.LOCK_AND_LOAD);
        if (controller.HasTag(GAME_TAG.SHADOWFORM))
          this.m_actor.ActivateSpellBirthState(SpellType.SHADOWFORM);
        if (controller.HasTag(GAME_TAG.EMBRACE_THE_SHADOW))
          this.m_actor.ActivateSpellBirthState(SpellType.EMBRACE_THE_SHADOW);
      }
    }
    switch (this.m_entity.GetZone())
    {
      case TAG_ZONE.HAND:
        this.ActivateHandStateSpells();
        break;
      case TAG_ZONE.PLAY:
      case TAG_ZONE.SECRET:
        if (this.m_entity.HasCustomKeywordEffect())
          this.ActivateCustomKeywordEffect();
        this.ShowExhaustedChange(this.m_entity.IsExhausted());
        break;
    }
  }

  public void ActivateHandStateSpells()
  {
    Player controller = this.m_entity.GetController();
    if ((this.m_entity.IsHeroPower() || this.m_entity.IsSpell()) && this.m_playEffect != null)
      SpellUtils.ActivateCancelIfNecessary(this.m_playEffect.GetSpell(false));
    if (this.m_entity.IsSpell())
      SpellUtils.ActivateCancelIfNecessary(this.GetActorSpell(SpellType.POWER_UP, false));
    if (this.m_entity.HasTag(GAME_TAG.EVIL_GLOW))
      SpellUtils.ActivateBirthIfNecessary(this.GetActorSpell(SpellType.EVIL_GLOW, true));
    if (this.m_entity.HasTag(GAME_TAG.SHIFTING))
      SpellUtils.ActivateBirthIfNecessary(this.GetActorSpell(SpellType.SHIFTING, true));
    if (controller != null && controller.HasTag(GAME_TAG.SPELLS_COST_HEALTH) && this.m_entity.IsSpell() || this.m_entity.HasTag(GAME_TAG.CARD_COSTS_HEALTH))
      SpellUtils.ActivateBirthIfNecessary(this.GetActorSpell(SpellType.SPELLS_COST_HEALTH, true));
    if (controller == null || !controller.HasTag(GAME_TAG.CHOOSE_BOTH) || (!this.m_entity.HasTag(GAME_TAG.CHOOSE_ONE) || !GameState.Get().IsOption(this.m_entity)))
      return;
    SpellUtils.ActivateBirthIfNecessary(this.GetActorSpell(SpellType.CHOOSE_BOTH, true));
  }

  public void DeactivateHandStateSpells()
  {
    Player controller = this.m_entity.GetController();
    if (this.m_entity.HasTag(GAME_TAG.EVIL_GLOW))
      SpellUtils.ActivateDeathIfNecessary(this.GetActorSpell(SpellType.EVIL_GLOW, false));
    if (this.m_entity.HasTag(GAME_TAG.SHIFTING))
      SpellUtils.ActivateDeathIfNecessary(this.GetActorSpell(SpellType.SHIFTING, false));
    if (controller != null && controller.HasTag(GAME_TAG.SPELLS_COST_HEALTH) && this.m_entity.IsSpell() || this.m_entity.HasTag(GAME_TAG.CARD_COSTS_HEALTH))
      SpellUtils.ActivateDeathIfNecessary(this.GetActorSpell(SpellType.SPELLS_COST_HEALTH, false));
    if (controller == null || !controller.HasTag(GAME_TAG.CHOOSE_BOTH) || (!this.m_entity.HasTag(GAME_TAG.CHOOSE_ONE) || !GameState.Get().IsOption(this.m_entity)))
      return;
    SpellUtils.ActivateDeathIfNecessary(this.GetActorSpell(SpellType.CHOOSE_BOTH, false));
  }

  private void ToggleDeathrattle(bool on)
  {
    if (on)
      this.m_actor.ActivateSpellBirthState(SpellType.DEATHRATTLE_IDLE);
    else
      this.m_actor.ActivateSpellDeathState(SpellType.DEATHRATTLE_IDLE);
  }

  private void ToggleBauble(bool on, SpellType spellType)
  {
    if (on)
    {
      this.DeactivateBaubles();
      this.m_actor.ActivateSpellBirthState(spellType);
    }
    else
    {
      SpellType spellType1 = this.PrioritizedBauble();
      if (spellType == spellType1)
        return;
      this.m_actor.ActivateSpellDeathState(spellType);
      if (spellType1 == SpellType.NONE)
        return;
      this.m_actor.ActivateSpellBirthState(spellType1);
    }
  }

  public void DeactivateBaubles()
  {
    SpellUtils.ActivateDeathIfNecessary(this.GetActorSpell(SpellType.TRIGGER, false));
    SpellUtils.ActivateDeathIfNecessary(this.GetActorSpell(SpellType.POISONOUS, false));
    SpellUtils.ActivateDeathIfNecessary(this.GetActorSpell(SpellType.INSPIRE, false));
  }

  private SpellType PrioritizedBauble()
  {
    if (this.m_entity.HasTriggerVisual() || this.m_entity.DoEnchantmentsHaveTriggerVisuals())
      return SpellType.TRIGGER;
    if (this.m_entity.IsPoisonous())
      return SpellType.POISONOUS;
    return this.m_entity.HasInspire() ? SpellType.INSPIRE : SpellType.NONE;
  }

  public void ShowExhaustedChange(int val)
  {
    this.ShowExhaustedChange(val == 1);
  }

  public void ShowExhaustedChange(bool exhausted)
  {
    if (this.m_entity.IsHeroPower())
    {
      this.StopCoroutine("PlayHeroPowerAnimation");
      if (exhausted)
      {
        this.StartCoroutine("PlayHeroPowerAnimation", !(bool) UniversalInputManager.UsePhoneUI ? (object) "HeroPower_Used" : (object) "HeroPower_Used_phone");
        SoundManager.Get().LoadAndPlay("hero_power_icon_flip_off");
      }
      else
      {
        this.StartCoroutine("PlayHeroPowerAnimation", !(bool) UniversalInputManager.UsePhoneUI ? (object) "HeroPower_Restore" : (object) "HeroPower_Restore_phone");
        SoundManager.Get().LoadAndPlay("hero_power_icon_flip_on");
      }
    }
    else if (this.m_entity.IsWeapon())
    {
      if (exhausted)
        this.SheatheWeapon();
      else
        this.UnSheatheWeapon();
    }
    else
    {
      if (!this.m_entity.IsSecret())
        return;
      this.StartCoroutine(this.ShowSecretExhaustedChange(exhausted));
    }
  }

  [DebuggerHidden]
  private IEnumerator PlayHeroPowerAnimation(string animation)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CPlayHeroPowerAnimation\u003Ec__Iterator2E6() { animation = animation, \u003C\u0024\u003Eanimation = animation, \u003C\u003Ef__this = this };
  }

  private void SheatheWeapon()
  {
    this.m_actor.GetAttackObject().ScaleToZero();
    this.ActivateActorSpell(SpellType.SHEATHE);
  }

  private void UnSheatheWeapon()
  {
    this.m_actor.GetAttackObject().Enlarge(1f);
    this.ActivateActorSpell(SpellType.UNSHEATHE);
  }

  [DebuggerHidden]
  private IEnumerator ShowSecretExhaustedChange(bool exhausted)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CShowSecretExhaustedChange\u003Ec__Iterator2E7() { exhausted = exhausted, \u003C\u0024\u003Eexhausted = exhausted, \u003C\u003Ef__this = this };
  }

  private void SheatheSecret(Spell spell)
  {
    if (this.m_secretSheathed || !this.m_entity.IsExhausted())
      return;
    this.m_secretSheathed = true;
    spell.ActivateState(SpellStateType.IDLE);
  }

  private void UnSheatheSecret(Spell spell)
  {
    if (!this.m_secretSheathed || this.m_entity.IsExhausted())
      return;
    this.m_secretSheathed = false;
    spell.ActivateState(SpellStateType.DEATH);
  }

  public void OnEnchantmentAdded(int oldEnchantmentCount, Entity enchantment)
  {
    Spell spell = (Spell) null;
    switch (enchantment.GetEnchantmentBirthVisual())
    {
      case TAG_ENCHANTMENT_VISUAL.POSITIVE:
        spell = this.GetActorSpell(SpellType.ENCHANT_POSITIVE, true);
        break;
      case TAG_ENCHANTMENT_VISUAL.NEGATIVE:
        spell = this.GetActorSpell(SpellType.ENCHANT_NEGATIVE, true);
        break;
      case TAG_ENCHANTMENT_VISUAL.NEUTRAL:
        spell = this.GetActorSpell(SpellType.ENCHANT_NEUTRAL, true);
        break;
    }
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
    {
      this.UpdateEnchantments();
      this.UpdateTooltip();
    }
    else
    {
      if (enchantment.HasTriggerVisual())
        this.ToggleBauble(true, SpellType.TRIGGER);
      spell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnEnchantmentSpellStateFinished));
      spell.ActivateState(SpellStateType.BIRTH);
    }
  }

  public void OnEnchantmentRemoved(int oldEnchantmentCount, Entity enchantment)
  {
    Spell spell = (Spell) null;
    switch (enchantment.GetEnchantmentBirthVisual())
    {
      case TAG_ENCHANTMENT_VISUAL.POSITIVE:
        spell = this.GetActorSpell(SpellType.ENCHANT_POSITIVE, true);
        break;
      case TAG_ENCHANTMENT_VISUAL.NEGATIVE:
        spell = this.GetActorSpell(SpellType.ENCHANT_NEGATIVE, true);
        break;
      case TAG_ENCHANTMENT_VISUAL.NEUTRAL:
        spell = this.GetActorSpell(SpellType.ENCHANT_NEUTRAL, true);
        break;
    }
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
    {
      this.UpdateEnchantments();
      this.UpdateTooltip();
    }
    else
    {
      if (enchantment.HasTriggerVisual() && !this.m_entity.DoEnchantmentsHaveTriggerVisuals())
        this.ToggleBauble(false, SpellType.TRIGGER);
      spell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnEnchantmentSpellStateFinished));
      spell.ActivateState(SpellStateType.DEATH);
    }
  }

  private void OnEnchantmentSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (prevStateType != SpellStateType.BIRTH && prevStateType != SpellStateType.DEATH)
      return;
    spell.RemoveStateFinishedCallback(new Spell.StateFinishedCallback(this.OnEnchantmentSpellStateFinished));
    this.UpdateEnchantments();
    this.UpdateTooltip();
  }

  public void UpdateEnchantments()
  {
    List<Entity> enchantments = this.m_entity.GetEnchantments();
    Spell actorSpell1 = this.GetActorSpell(SpellType.ENCHANT_POSITIVE, true);
    Spell actorSpell2 = this.GetActorSpell(SpellType.ENCHANT_NEGATIVE, true);
    Spell actorSpell3 = this.GetActorSpell(SpellType.ENCHANT_NEUTRAL, true);
    Spell spell1 = (Spell) null;
    if ((UnityEngine.Object) actorSpell1 != (UnityEngine.Object) null && actorSpell1.GetActiveState() == SpellStateType.IDLE)
      spell1 = actorSpell1;
    else if ((UnityEngine.Object) actorSpell2 != (UnityEngine.Object) null && actorSpell2.GetActiveState() == SpellStateType.IDLE)
      spell1 = actorSpell2;
    else if ((UnityEngine.Object) actorSpell3 != (UnityEngine.Object) null && actorSpell3.GetActiveState() == SpellStateType.IDLE)
      spell1 = actorSpell3;
    if (enchantments.Count == 0)
    {
      if (!((UnityEngine.Object) spell1 != (UnityEngine.Object) null))
        return;
      spell1.ActivateState(SpellStateType.DEATH);
    }
    else
    {
      int num = 0;
      bool flag = false;
      using (List<Entity>.Enumerator enumerator = enchantments.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TAG_ENCHANTMENT_VISUAL enchantmentIdleVisual = enumerator.Current.GetEnchantmentIdleVisual();
          if (enchantmentIdleVisual == TAG_ENCHANTMENT_VISUAL.POSITIVE)
            ++num;
          else if (enchantmentIdleVisual == TAG_ENCHANTMENT_VISUAL.NEGATIVE)
            --num;
          if (enchantmentIdleVisual != TAG_ENCHANTMENT_VISUAL.INVALID)
            flag = true;
        }
      }
      Spell spell2 = (Spell) null;
      if (num > 0)
        spell2 = actorSpell1;
      else if (num < 0)
        spell2 = actorSpell2;
      else if (flag)
        spell2 = actorSpell3;
      if ((UnityEngine.Object) spell1 != (UnityEngine.Object) null && (UnityEngine.Object) spell1 != (UnityEngine.Object) spell2)
        spell1.Deactivate();
      if (!((UnityEngine.Object) spell2 != (UnityEngine.Object) null))
        return;
      spell2.ActivateState(SpellStateType.IDLE);
    }
  }

  public Spell GetActorSpell(SpellType spellType, bool loadIfNeeded = true)
  {
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null)
      return (Spell) null;
    return !loadIfNeeded ? this.m_actor.GetSpellIfLoaded(spellType) : this.m_actor.GetSpell(spellType);
  }

  public Spell ActivateActorSpell(SpellType spellType)
  {
    return this.ActivateActorSpell(this.m_actor, spellType, (Spell.FinishedCallback) null, (Spell.StateFinishedCallback) null);
  }

  public Spell ActivateActorSpell(SpellType spellType, Spell.FinishedCallback finishedCallback)
  {
    return this.ActivateActorSpell(this.m_actor, spellType, finishedCallback, (Spell.StateFinishedCallback) null);
  }

  public Spell ActivateActorSpell(SpellType spellType, Spell.FinishedCallback finishedCallback, Spell.StateFinishedCallback stateFinishedCallback)
  {
    return this.ActivateActorSpell(this.m_actor, spellType, finishedCallback, stateFinishedCallback);
  }

  private Spell ActivateActorSpell(Actor actor, SpellType spellType)
  {
    return this.ActivateActorSpell(actor, spellType, (Spell.FinishedCallback) null, (Spell.StateFinishedCallback) null);
  }

  private Spell ActivateActorSpell(Actor actor, SpellType spellType, Spell.FinishedCallback finishedCallback)
  {
    return this.ActivateActorSpell(actor, spellType, finishedCallback, (Spell.StateFinishedCallback) null);
  }

  private Spell ActivateActorSpell(Actor actor, SpellType spellType, Spell.FinishedCallback finishedCallback, Spell.StateFinishedCallback stateFinishedCallback)
  {
    if ((UnityEngine.Object) actor == (UnityEngine.Object) null)
    {
      Log.Mike.Print(string.Format("{0}.ActivateActorSpell() - actor IS NULL spellType={1}", (object) this, (object) spellType));
      return (Spell) null;
    }
    Spell spell = actor.GetSpell(spellType);
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
    {
      Log.Mike.Print(string.Format("{0}.ActivateActorSpell() - spell IS NULL actor={1} spellType={2}", (object) this, (object) actor, (object) spellType));
      return (Spell) null;
    }
    this.ActivateSpell(spell, finishedCallback, stateFinishedCallback);
    return spell;
  }

  private void ActivateSpell(Spell spell, Spell.FinishedCallback finishedCallback)
  {
    this.ActivateSpell(spell, finishedCallback, (object) null, (Spell.StateFinishedCallback) null, (object) null);
  }

  private void ActivateSpell(Spell spell, Spell.FinishedCallback finishedCallback, Spell.StateFinishedCallback stateFinishedCallback)
  {
    this.ActivateSpell(spell, finishedCallback, (object) null, stateFinishedCallback, (object) null);
  }

  private void ActivateSpell(Spell spell, Spell.FinishedCallback finishedCallback, object finishedUserData, Spell.StateFinishedCallback stateFinishedCallback)
  {
    this.ActivateSpell(spell, finishedCallback, finishedUserData, stateFinishedCallback, (object) null);
  }

  private void ActivateSpell(Spell spell, Spell.FinishedCallback finishedCallback, object finishedUserData, Spell.StateFinishedCallback stateFinishedCallback, object stateFinishedUserData)
  {
    if (finishedCallback != null)
      spell.AddFinishedCallback(finishedCallback, finishedUserData);
    if (stateFinishedCallback != null)
      spell.AddStateFinishedCallback(stateFinishedCallback, stateFinishedUserData);
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    spell.Activate();
  }

  public Spell GetActorAttackSpellForInput()
  {
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null)
    {
      Log.Mike.Print("{0}.GetActorAttackSpellForInput() - m_actor IS NULL", (object) this);
      return (Spell) null;
    }
    if ((UnityEngine.Object) this.m_zone == (UnityEngine.Object) null)
    {
      Log.Mike.Print("{0}.GetActorAttackSpellForInput() - m_zone IS NULL", (object) this);
      return (Spell) null;
    }
    Spell spell = this.m_actor.GetSpell(SpellType.FRIENDLY_ATTACK);
    if (!((UnityEngine.Object) spell == (UnityEngine.Object) null))
      return spell;
    Log.Mike.Print("{0}.GetActorAttackSpellForInput() - {1} spell is null", new object[2]
    {
      (object) this,
      (object) SpellType.FRIENDLY_ATTACK
    });
    return (Spell) null;
  }

  private Spell ActivateDeathSpell(Actor actor)
  {
    bool standard;
    Spell bestDeathSpell = this.GetBestDeathSpell(actor, out standard);
    if ((UnityEngine.Object) bestDeathSpell == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateDeathSpell() - {1} is null", (object) this, (object) SpellType.DEATH));
      return (Spell) null;
    }
    this.CleanUpCustomSpell(bestDeathSpell, ref this.m_customDeathSpell);
    this.CleanUpCustomSpell(bestDeathSpell, ref this.m_customDeathSpellOverride);
    ++this.m_activeDeathEffectCount;
    if (standard)
    {
      if ((UnityEngine.Object) this.m_actor != (UnityEngine.Object) actor)
        bestDeathSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnSpellStateFinished_DestroyActor));
    }
    else
    {
      bestDeathSpell.SetSource(this.gameObject);
      if ((UnityEngine.Object) this.m_actor != (UnityEngine.Object) actor)
        bestDeathSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnSpellStateFinished_CustomDeath));
      SpellUtils.SetCustomSpellParent(bestDeathSpell, (Component) actor);
    }
    bestDeathSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnSpellFinished_Death));
    bestDeathSpell.Activate();
    BoardEvents boardEvents = BoardEvents.Get();
    if ((UnityEngine.Object) boardEvents != (UnityEngine.Object) null)
      boardEvents.DeathEvent(this);
    return bestDeathSpell;
  }

  private Spell ActivateHandSpawnSpell()
  {
    if ((UnityEngine.Object) this.m_customSpawnSpellOverride == (UnityEngine.Object) null)
      return this.ActivateDefaultSpawnSpell(new Spell.FinishedCallback(this.OnSpellFinished_DefaultHandSpawn));
    Entity creator = this.m_entity.GetCreator();
    Card creatorCard = (Card) null;
    if (creator != null && creator.IsMinion())
      creatorCard = creator.GetCard();
    if ((UnityEngine.Object) creatorCard != (UnityEngine.Object) null)
      TransformUtil.CopyWorld((Component) this.transform, (Component) creatorCard.transform);
    this.ActivateCustomHandSpawnSpell(this.m_customSpawnSpellOverride, creatorCard);
    return this.m_customSpawnSpellOverride;
  }

  private Spell ActivateDefaultSpawnSpell(Spell.FinishedCallback finishedCallback)
  {
    this.m_inputEnabled = false;
    this.m_actor.ToggleForceIdle(true);
    SpellType spellType = !this.m_entity.IsWeapon() || !(this.m_zone is ZoneWeapon) ? SpellType.SUMMON_IN : (!this.m_entity.IsControlledByFriendlySidePlayer() ? SpellType.SUMMON_IN_OPPONENT : SpellType.SUMMON_IN_FRIENDLY);
    Spell spell = this.ActivateActorSpell(spellType, finishedCallback);
    if (!((UnityEngine.Object) spell == (UnityEngine.Object) null))
      return spell;
    UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateDefaultSpawnSpell() - {1} is null", (object) this, (object) spellType));
    return (Spell) null;
  }

  private void ActivateCustomHandSpawnSpell(Spell spell, Card creatorCard)
  {
    GameObject go = !((UnityEngine.Object) creatorCard == (UnityEngine.Object) null) ? creatorCard.gameObject : this.gameObject;
    spell.SetSource(go);
    spell.RemoveAllTargets();
    spell.AddTarget(this.gameObject);
    spell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnSpellStateFinished_DestroySpell));
    SpellUtils.SetCustomSpellParent(spell, (Component) this.m_actor);
    spell.AddFinishedCallback(new Spell.FinishedCallback(this.OnSpellFinished_CustomHandSpawn));
    spell.Activate();
  }

  private void ActivateMinionSpawnEffects()
  {
    Entity creator = this.m_entity.GetCreator();
    Card creatorCard = (Card) null;
    if (creator != null && creator.IsMinion())
      creatorCard = creator.GetCard();
    if ((UnityEngine.Object) creatorCard != (UnityEngine.Object) null)
      TransformUtil.CopyWorld((Component) this.transform, (Component) creatorCard.transform);
    bool standard;
    Spell bestSpawnSpell = this.GetBestSpawnSpell(out standard);
    if (standard)
    {
      if ((UnityEngine.Object) creatorCard == (UnityEngine.Object) null)
        this.ActivateStandardSpawnMinionSpell();
      else
        this.StartCoroutine(this.ActivateCreatorSpawnMinionSpell(creator, creatorCard));
    }
    else
      this.ActivateCustomSpawnMinionSpell(bestSpawnSpell, creatorCard);
  }

  [DebuggerHidden]
  private IEnumerator ActivateCreatorSpawnMinionSpell(Entity creator, Card creatorCard)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CActivateCreatorSpawnMinionSpell\u003Ec__Iterator2E8() { creator = creator, creatorCard = creatorCard, \u003C\u0024\u003Ecreator = creator, \u003C\u0024\u003EcreatorCard = creatorCard, \u003C\u003Ef__this = this };
  }

  private Spell ActivateCreatorSpawnMinionSpell()
  {
    if (this.m_entity.IsControlledByFriendlySidePlayer())
      return this.ActivateActorSpell(SpellType.FRIENDLY_SPAWN_MINION);
    return this.ActivateActorSpell(SpellType.OPPONENT_SPAWN_MINION);
  }

  private void ActivateStandardSpawnMinionSpell()
  {
    this.m_activeSpawnSpell = !this.m_entity.IsControlledByFriendlySidePlayer() ? this.ActivateActorSpell(SpellType.OPPONENT_SPAWN_MINION, new Spell.FinishedCallback(this.OnSpellFinished_StandardSpawnMinion)) : this.ActivateActorSpell(SpellType.FRIENDLY_SPAWN_MINION, new Spell.FinishedCallback(this.OnSpellFinished_StandardSpawnMinion));
    this.ActivateCharacterPlayEffects();
  }

  private void ActivateCustomSpawnMinionSpell(Spell spell, Card creatorCard)
  {
    this.m_activeSpawnSpell = spell;
    GameObject go = !((UnityEngine.Object) creatorCard == (UnityEngine.Object) null) ? creatorCard.gameObject : this.gameObject;
    spell.SetSource(go);
    spell.RemoveAllTargets();
    spell.AddTarget(this.gameObject);
    spell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnSpellStateFinished_DestroySpell));
    SpellUtils.SetCustomSpellParent(spell, (Component) this.m_actor);
    spell.AddFinishedCallback(new Spell.FinishedCallback(this.OnSpellFinished_CustomSpawnMinion));
    spell.Activate();
  }

  [DebuggerHidden]
  private IEnumerator ActivateReviveSpell()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CActivateReviveSpell\u003Ec__Iterator2E9() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator ActivateActorBattlecrySpell()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CActivateActorBattlecrySpell\u003Ec__Iterator2EA() { \u003C\u003Ef__this = this };
  }

  private void CleanUpCustomSpell(Spell chosenSpell, ref Spell customSpell)
  {
    if (!(bool) ((UnityEngine.Object) customSpell))
      return;
    if ((UnityEngine.Object) chosenSpell == (UnityEngine.Object) customSpell)
      customSpell = (Spell) null;
    else
      UnityEngine.Object.Destroy((UnityEngine.Object) customSpell.gameObject);
  }

  private void OnSpellFinished_StandardSpawnMinion(Spell spell, object userData)
  {
    this.m_actorReady = true;
    this.m_inputEnabled = true;
    this.m_actor.Show();
    this.ActivateStateSpells();
    this.RefreshActor();
    this.UpdateActorComponents();
    BoardEvents boardEvents = BoardEvents.Get();
    if (!((UnityEngine.Object) boardEvents != (UnityEngine.Object) null))
      return;
    boardEvents.SummonedEvent(this);
  }

  private void OnSpellFinished_CustomSpawnMinion(Spell spell, object userData)
  {
    this.OnSpellFinished_StandardSpawnMinion(spell, userData);
    this.CleanUpCustomSpell(spell, ref this.m_customSpawnSpell);
    this.CleanUpCustomSpell(spell, ref this.m_customSpawnSpellOverride);
    this.ActivateCharacterPlayEffects();
  }

  private void OnSpellFinished_DefaultHandSpawn(Spell spell, object userData)
  {
    this.m_actor.ToggleForceIdle(false);
    this.m_inputEnabled = true;
    this.ActivateStateSpells();
    this.RefreshActor();
    this.UpdateActorComponents();
  }

  private void OnSpellFinished_CustomHandSpawn(Spell spell, object userData)
  {
    this.OnSpellFinished_DefaultHandSpawn(spell, userData);
    this.CleanUpCustomSpell(spell, ref this.m_customSpawnSpellOverride);
  }

  private void OnSpellFinished_DefaultPlaySpawn(Spell spell, object userData)
  {
    this.m_actor.ToggleForceIdle(false);
    this.m_inputEnabled = true;
    this.ActivateStateSpells();
    this.RefreshActor();
    this.UpdateActorComponents();
  }

  private void OnSpellFinished_StandardCardSummon(Spell spell, object userData)
  {
    this.m_actorReady = true;
    this.m_inputEnabled = true;
    this.ActivateStateSpells();
    this.RefreshActor();
    this.UpdateActorComponents();
  }

  private void OnSpellFinished_UpdateActorComponents(Spell spell, object userData)
  {
    this.UpdateActorComponents();
  }

  private void OnSpellFinished_Death(Spell spell, object userData)
  {
    this.m_suppressKeywordDeaths = false;
    this.m_keywordDeathDelaySec = 0.6f;
    --this.m_activeDeathEffectCount;
    GameState.Get().ClearCardBeingDrawn(this);
  }

  private void OnSpellStateFinished_DestroyActor(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    if (this.m_zone is ZoneGraveyard)
      this.PurgeSpells();
    Actor componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<Actor>(spell.gameObject);
    if ((UnityEngine.Object) componentInThisOrParents == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) string.Format("Card.OnSpellStateFinished_DestroyActor() - spell {0} on Card {1} has no Actor ancestor", (object) spell, (object) this));
    else
      componentInThisOrParents.Destroy();
  }

  private void OnSpellStateFinished_DestroySpell(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
  }

  private void OnSpellStateFinished_CustomDeath(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    Actor componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<Actor>(spell.gameObject);
    if ((UnityEngine.Object) componentInThisOrParents == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) string.Format("Card.OnSpellStateFinished_CustomDeath() - spell {0} on Card {1} has no Actor ancestor", (object) spell, (object) this));
    else
      componentInThisOrParents.Destroy();
  }

  public void UpdateActorState()
  {
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null || !this.m_shown || (this.m_entity.IsBusy() || this.m_zone is ZoneGraveyard))
      return;
    if (!this.m_inputEnabled || (UnityEngine.Object) this.m_zone != (UnityEngine.Object) null && !this.m_zone.IsInputEnabled())
      this.m_actor.SetActorState(ActorStateType.CARD_IDLE);
    else if (this.m_overPlayfield)
    {
      this.m_actor.SetActorState(ActorStateType.CARD_OVER_PLAYFIELD);
    }
    else
    {
      GameState state = GameState.Get();
      if (state != null && state.IsEntityInputEnabled(this.m_entity))
      {
        switch (state.GetResponseMode())
        {
          case GameState.ResponseMode.OPTION:
            if (this.DoOptionHighlight(state))
              return;
            break;
          case GameState.ResponseMode.SUB_OPTION:
            if (this.DoSubOptionHighlight(state))
              return;
            break;
          case GameState.ResponseMode.OPTION_TARGET:
            if (this.DoOptionTargetHighlight(state))
              return;
            break;
          case GameState.ResponseMode.CHOICE:
            if (this.DoChoiceHighlight(state))
              return;
            break;
        }
      }
      if (this.m_mousedOver && !(this.m_zone is ZoneHand))
        this.m_actor.SetActorState(ActorStateType.CARD_MOUSE_OVER);
      else if (this.m_mousedOverByOpponent)
        this.m_actor.SetActorState(ActorStateType.CARD_OPPONENT_MOUSE_OVER);
      else
        this.m_actor.SetActorState(ActorStateType.CARD_IDLE);
    }
  }

  private bool DoChoiceHighlight(GameState state)
  {
    if (state.GetChosenEntities().Contains(this.m_entity))
    {
      if (this.m_mousedOver)
        this.m_actor.SetActorState(ActorStateType.CARD_PLAYABLE_MOUSE_OVER);
      else
        this.m_actor.SetActorState(ActorStateType.CARD_SELECTED);
      return true;
    }
    int entityId = this.m_entity.GetEntityId();
    if (!state.GetFriendlyEntityChoices().Entities.Contains(entityId))
      return false;
    if (GameState.Get().IsMulliganManagerActive())
      this.m_actor.SetActorState(ActorStateType.CARD_IDLE);
    else
      this.m_actor.SetActorState(ActorStateType.CARD_SELECTABLE);
    return true;
  }

  private bool DoOptionHighlight(GameState state)
  {
    if (!GameState.Get().IsOption(this.m_entity))
      return false;
    bool flag1 = this.m_entity.GetZone() == TAG_ZONE.HAND;
    bool flag2 = this.m_entity.GetController().IsRealTimeComboActive();
    if (flag1 && flag2 && this.m_entity.HasTag(GAME_TAG.COMBO))
    {
      this.m_actor.SetActorState(ActorStateType.CARD_COMBO);
      return true;
    }
    bool realTimePoweredUp = this.m_entity.GetRealTimePoweredUp();
    if (flag1 && realTimePoweredUp)
    {
      this.m_actor.SetActorState(ActorStateType.CARD_POWERED_UP);
      return true;
    }
    if (!flag1 && this.m_mousedOver)
    {
      this.m_actor.SetActorState(ActorStateType.CARD_PLAYABLE_MOUSE_OVER);
      return true;
    }
    this.m_actor.SetActorState(ActorStateType.CARD_PLAYABLE);
    return true;
  }

  private bool DoSubOptionHighlight(GameState state)
  {
    Network.Options.Option selectedNetworkOption = state.GetSelectedNetworkOption();
    int entityId = this.m_entity.GetEntityId();
    using (List<Network.Options.Option.SubOption>.Enumerator enumerator = selectedNetworkOption.Subs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Options.Option.SubOption current = enumerator.Current;
        if (entityId == current.ID)
        {
          if (this.m_mousedOver)
            this.m_actor.SetActorState(ActorStateType.CARD_PLAYABLE_MOUSE_OVER);
          else
            this.m_actor.SetActorState(ActorStateType.CARD_PLAYABLE);
          return true;
        }
      }
    }
    return false;
  }

  private bool DoOptionTargetHighlight(GameState state)
  {
    if (!state.GetSelectedNetworkSubOption().Targets.Contains(this.m_entity.GetEntityId()))
      return false;
    if (this.m_mousedOver)
      this.m_actor.SetActorState(ActorStateType.CARD_VALID_TARGET_MOUSE_OVER);
    else
      this.m_actor.SetActorState(ActorStateType.CARD_VALID_TARGET);
    return true;
  }

  public Actor GetActor()
  {
    return this.m_actor;
  }

  public void SetActor(Actor actor)
  {
    this.m_actor = actor;
  }

  public string GetActorName()
  {
    return this.m_actorName;
  }

  public void SetActorName(string actorName)
  {
    this.m_actorName = actorName;
  }

  public bool IsActorReady()
  {
    return this.m_actorReady;
  }

  public bool IsActorLoading()
  {
    return this.m_actorLoading;
  }

  public void UpdateActorComponents()
  {
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null)
      return;
    this.m_actor.UpdateAllComponents();
  }

  public void RefreshActor()
  {
    this.UpdateActorState();
    if (this.m_entity.IsEnchanted())
      this.UpdateEnchantments();
    this.UpdateTooltip();
  }

  public Zone GetZone()
  {
    return this.m_zone;
  }

  public Zone GetPrevZone()
  {
    return this.m_prevZone;
  }

  public void SetZone(Zone zone)
  {
    this.m_zone = zone;
  }

  public int GetZonePosition()
  {
    return this.m_zonePosition;
  }

  public void SetZonePosition(int pos)
  {
    this.m_zonePosition = pos;
  }

  public int GetPredictedZonePosition()
  {
    return this.m_predictedZonePosition;
  }

  public void SetPredictedZonePosition(int pos)
  {
    this.m_predictedZonePosition = pos;
  }

  public ZoneTransitionStyle GetTransitionStyle()
  {
    return this.m_transitionStyle;
  }

  public void SetTransitionStyle(ZoneTransitionStyle style)
  {
    this.m_transitionStyle = style;
  }

  public bool IsTransitioningZones()
  {
    return this.m_transitioningZones;
  }

  public void EnableTransitioningZones(bool enable)
  {
    this.m_transitioningZones = enable;
  }

  public bool HasBeenGrabbedByEnemyActionHandler()
  {
    return this.m_hasBeenGrabbedByEnemyActionHandler;
  }

  public void MarkAsGrabbedByEnemyActionHandler(bool enable)
  {
    Log.FaceDownCard.Print("Card.MarkAsGrabbedByEnemyActionHandler() - card={0} enable={1}", new object[2]
    {
      (object) this,
      (object) enable
    });
    this.m_hasBeenGrabbedByEnemyActionHandler = enable;
  }

  public bool IsDoNotSort()
  {
    return this.m_doNotSort;
  }

  public void SetDoNotSort(bool on)
  {
    if (this.m_entity.IsControlledByOpposingSidePlayer())
      Log.FaceDownCard.Print("Card.SetDoNotSort() - card={0} on={1}", new object[2]
      {
        (object) this,
        (object) on
      });
    this.m_doNotSort = on;
  }

  public bool IsDoNotWarpToNewZone()
  {
    return this.m_doNotWarpToNewZone;
  }

  public void SetDoNotWarpToNewZone(bool on)
  {
    this.m_doNotWarpToNewZone = on;
  }

  public float GetTransitionDelay()
  {
    return this.m_transitionDelay;
  }

  public void SetTransitionDelay(float delay)
  {
    this.m_transitionDelay = delay;
  }

  public bool IsHoldingForLinkedCardSwitch()
  {
    return this.m_holdingForLinkedCardSwitch;
  }

  public void SetHoldingForLinkedCardSwitch(bool hold)
  {
    this.m_holdingForLinkedCardSwitch = hold;
  }

  public void UpdateZoneFromTags()
  {
    this.m_zonePosition = this.m_entity.GetZonePosition();
    Zone zoneForEntity = ZoneMgr.Get().FindZoneForEntity(this.m_entity);
    this.TransitionToZone(zoneForEntity);
    if (!((UnityEngine.Object) zoneForEntity != (UnityEngine.Object) null))
      return;
    zoneForEntity.UpdateLayout();
  }

  public void TransitionToZone(Zone zone)
  {
    if ((UnityEngine.Object) this.m_zone == (UnityEngine.Object) zone)
      Log.Mike.Print("Card.TransitionToZone() - card={0} already in target zone", (object) this);
    else if ((UnityEngine.Object) zone == (UnityEngine.Object) null)
    {
      this.m_zone.RemoveCard(this);
      this.m_prevZone = this.m_zone;
      this.m_zone = (Zone) null;
      if (this.SwitchOutLinkedDrawnCard())
        return;
      this.DeactivateLifetimeEffects();
      this.DeactivateCustomKeywordEffect();
      if (this.m_prevZone is ZoneHand)
        this.DeactivateHandStateSpells();
      this.DoNullZoneVisuals();
    }
    else
    {
      this.m_prevZone = this.m_zone;
      this.m_zone = zone;
      if ((UnityEngine.Object) this.m_prevZone != (UnityEngine.Object) null)
        this.m_prevZone.RemoveCard(this);
      this.m_zone.AddCard(this);
      if (this.m_zone is ZoneGraveyard && GameState.Get().IsBeingDrawn(this))
      {
        this.m_actorReady = true;
        this.DiscardCardBeingDrawn();
      }
      else if (this.m_zone is ZoneGraveyard && this.m_ignoreDeath)
      {
        this.m_actorReady = true;
      }
      else
      {
        this.m_actorReady = false;
        this.LoadActorAndSpells();
      }
    }
  }

  public void UpdateActor(bool forceIfNullZone = false)
  {
    if (!forceIfNullZone && (UnityEngine.Object) this.m_zone == (UnityEngine.Object) null)
      return;
    string actorNameForZone = this.m_cardDef.DetermineActorNameForZone(this.m_entity, this.m_entity.GetZone());
    if ((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null && this.m_actorName == actorNameForZone)
      return;
    GameObject gameObject = AssetLoader.Get().LoadActor(actorNameForZone, false, false);
    if (!(bool) ((UnityEngine.Object) gameObject))
    {
      UnityEngine.Debug.LogWarningFormat("Card.UpdateActor() - FAILED to load actor \"{0}\"", (object) actorNameForZone);
    }
    else
    {
      Actor component = gameObject.GetComponent<Actor>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarningFormat("Card.UpdateActor() - ERROR actor \"{0}\" has no Actor component", (object) actorNameForZone);
      }
      else
      {
        if ((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null)
          this.m_actor.Destroy();
        this.m_actor = component;
        this.m_actorName = actorNameForZone;
        this.m_actor.SetEntity(this.m_entity);
        this.m_actor.SetCard(this);
        this.m_actor.SetCardDef(this.m_cardDef);
        this.m_actor.UpdateAllComponents();
        if (this.m_shown)
          this.ShowImpl();
        else
          this.HideImpl();
        this.RefreshActor();
      }
    }
  }

  private void LoadActorAndSpells()
  {
    this.m_actorLoading = true;
    List<Card.SpellLoadRequest> spellLoadRequestList = new List<Card.SpellLoadRequest>();
    if (this.m_prevZone is ZoneHand && this.m_zone is ZonePlay)
    {
      Card.SpellLoadRequest spellLoadRequest = this.MakeCustomSpellLoadRequest(this.m_cardDef.m_CustomSummonSpellPath, this.m_cardDef.m_GoldenCustomSummonSpellPath, new AssetLoader.GameObjectCallback(this.OnCustomSummonSpellLoaded));
      if (spellLoadRequest != null)
        spellLoadRequestList.Add(spellLoadRequest);
    }
    if (!(bool) ((UnityEngine.Object) this.m_customDeathSpell) && (this.m_zone is ZoneHand || this.m_zone is ZonePlay))
    {
      Card.SpellLoadRequest spellLoadRequest = this.MakeCustomSpellLoadRequest(this.m_cardDef.m_CustomDeathSpellPath, this.m_cardDef.m_GoldenCustomDeathSpellPath, new AssetLoader.GameObjectCallback(this.OnCustomDeathSpellLoaded));
      if (spellLoadRequest != null)
        spellLoadRequestList.Add(spellLoadRequest);
    }
    if (!(bool) ((UnityEngine.Object) this.m_customSpawnSpell) && this.m_zone is ZonePlay)
    {
      Card.SpellLoadRequest spellLoadRequest = this.MakeCustomSpellLoadRequest(this.m_cardDef.m_CustomSpawnSpellPath, this.m_cardDef.m_GoldenCustomSpawnSpellPath, new AssetLoader.GameObjectCallback(this.OnCustomSpawnSpellLoaded));
      if (spellLoadRequest != null)
        spellLoadRequestList.Add(spellLoadRequest);
    }
    this.m_spellLoadCount = spellLoadRequestList.Count;
    if (spellLoadRequestList.Count == 0)
    {
      this.LoadActor();
    }
    else
    {
      using (List<Card.SpellLoadRequest>.Enumerator enumerator = spellLoadRequestList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Card.SpellLoadRequest current = enumerator.Current;
          AssetLoader.Get().LoadSpell(current.m_path, current.m_loadCallback, (object) null, false);
        }
      }
    }
  }

  private Card.SpellLoadRequest MakeCustomSpellLoadRequest(string customPath, string goldenCustomPath, AssetLoader.GameObjectCallback loadCallback)
  {
    string str = customPath;
    if (this.m_entity.GetPremiumType() == TAG_PREMIUM.GOLDEN && !string.IsNullOrEmpty(goldenCustomPath))
      str = goldenCustomPath;
    else if (string.IsNullOrEmpty(str))
      return (Card.SpellLoadRequest) null;
    return new Card.SpellLoadRequest() { m_path = str, m_loadCallback = loadCallback };
  }

  private void OnCustomSummonSpellLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      Error.AddDevFatal("Card.OnCustomSummonSpellLoaded() - FAILED to load \"{0}\" for card {1}", (object) name, (object) this);
      this.FinishSpellLoad();
    }
    else
    {
      this.m_customSummonSpell = go.GetComponent<Spell>();
      if ((UnityEngine.Object) this.m_customSummonSpell == (UnityEngine.Object) null)
      {
        this.FinishSpellLoad();
      }
      else
      {
        SpellUtils.SetupSpell(this.m_customSummonSpell, (Component) this);
        this.FinishSpellLoad();
      }
    }
  }

  private void OnCustomDeathSpellLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      Error.AddDevFatal("Card.OnCustomDeathSpellLoaded() - FAILED to load \"{0}\" for card {1}", (object) name, (object) this);
      this.FinishSpellLoad();
    }
    else
    {
      this.m_customDeathSpell = go.GetComponent<Spell>();
      if ((UnityEngine.Object) this.m_customDeathSpell == (UnityEngine.Object) null)
      {
        this.FinishSpellLoad();
      }
      else
      {
        SpellUtils.SetupSpell(this.m_customDeathSpell, (Component) this);
        this.FinishSpellLoad();
      }
    }
  }

  private void OnCustomSpawnSpellLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      Error.AddDevFatal("Card.OnCustomSpawnSpellLoaded() - FAILED to load \"{0}\" for card {1}", (object) name, (object) this);
      this.FinishSpellLoad();
    }
    else
    {
      this.m_customSpawnSpell = go.GetComponent<Spell>();
      if ((UnityEngine.Object) this.m_customSpawnSpell == (UnityEngine.Object) null)
      {
        this.FinishSpellLoad();
      }
      else
      {
        SpellUtils.SetupSpell(this.m_customSpawnSpell, (Component) this);
        this.FinishSpellLoad();
      }
    }
  }

  private void FinishSpellLoad()
  {
    --this.m_spellLoadCount;
    if (this.m_spellLoadCount > 0)
      return;
    this.LoadActor();
  }

  private void LoadActor()
  {
    string actorNameForZone = this.m_cardDef.DetermineActorNameForZone(this.m_entity, this.m_zone.m_ServerTag);
    if (this.m_actorName == actorNameForZone || actorNameForZone == null)
    {
      this.m_actorName = actorNameForZone;
      this.FinishActorLoad(this.m_actor);
    }
    else
      AssetLoader.Get().LoadActor(actorNameForZone, new AssetLoader.GameObjectCallback(this.OnActorLoaded), (object) null, false);
  }

  private void OnActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("Card.OnActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("Card.OnActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        Actor actor = this.m_actor;
        this.m_actor = component;
        this.m_actorName = actorName;
        this.m_actor.SetEntity(this.m_entity);
        this.m_actor.SetCard(this);
        this.m_actor.SetCardDef(this.m_cardDef);
        this.m_actor.UpdateAllComponents();
        this.FinishActorLoad(actor);
      }
    }
  }

  private void FinishActorLoad(Actor oldActor)
  {
    this.m_actorLoading = false;
    this.OnZoneChanged();
    this.OnActorChanged(oldActor);
    if (this.m_isBattleCrySource)
      SceneUtils.SetLayer(this.m_actor.gameObject, GameLayer.IgnoreFullScreenEffects);
    this.RefreshActor();
  }

  public void ForceLoadHandActor()
  {
    string actorNameForZone = this.m_cardDef.DetermineActorNameForZone(this.m_entity, TAG_ZONE.HAND);
    if ((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null && this.m_actorName == actorNameForZone)
    {
      this.ShowCard();
      this.m_actor.Show();
      this.RefreshActor();
    }
    else
    {
      GameObject gameObject = AssetLoader.Get().LoadActor(actorNameForZone, false, false);
      if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarningFormat("Card.ForceLoadHandActor() - FAILED to load actor \"{0}\"", (object) actorNameForZone);
      }
      else
      {
        Actor component = gameObject.GetComponent<Actor>();
        if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        {
          UnityEngine.Debug.LogWarningFormat("Card.ForceLoadHandActor() - ERROR actor \"{0}\" has no Actor component", (object) actorNameForZone);
        }
        else
        {
          if ((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null)
            this.m_actor.Destroy();
          this.m_actor = component;
          this.m_actorName = actorNameForZone;
          this.m_actor.SetEntity(this.m_entity);
          this.m_actor.SetCard(this);
          this.m_actor.SetCardDef(this.m_cardDef);
          this.m_actor.UpdateAllComponents();
          if (this.m_shown)
            this.ShowImpl();
          else
            this.HideImpl();
          this.RefreshActor();
        }
      }
    }
  }

  private void OnZoneChanged()
  {
    if (this.m_prevZone is ZoneHand && this.m_zone is ZoneGraveyard)
      this.DoDiscardAnimation();
    else if (this.m_zone is ZoneGraveyard)
    {
      if (!this.m_entity.IsHero())
        return;
      this.m_doNotSort = true;
    }
    else if (this.m_zone is ZoneHand)
    {
      if (!this.m_doNotSort)
        this.ShowCard();
      if (!(this.m_prevZone is ZoneGraveyard) || !this.m_entity.IsSpell())
        return;
      this.m_actor.Hide();
      this.ActivateActorSpell(SpellType.SUMMON_IN);
    }
    else
    {
      if (!(this.m_prevZone is ZoneGraveyard) && !(this.m_prevZone is ZoneDeck) || this.m_zone.m_ServerTag != TAG_ZONE.PLAY)
        return;
      this.ShowCard();
    }
  }

  private void OnActorChanged(Actor oldActor)
  {
    this.HideTooltip();
    bool flag1 = false;
    bool flag2 = GameState.Get().IsGameCreating();
    if ((UnityEngine.Object) oldActor == (UnityEngine.Object) null)
    {
      bool flag3 = GameState.Get().IsMulliganPhaseNowOrPending();
      if (this.m_zone is ZoneHand && GameState.Get().IsBeginPhase())
      {
        bool flag4 = this.m_entity.GetCardId() == "GAME_005";
        if (flag3 && !GameState.Get().HasTheCoinBeenSpawned())
        {
          if (flag4)
          {
            GameState.Get().NotifyOfCoinSpawn();
            this.m_actor.TurnOffCollider();
            this.m_actor.Hide();
            this.m_actorReady = true;
            flag1 = true;
            this.transform.position = Vector3.zero;
            this.m_doNotWarpToNewZone = true;
            this.m_doNotSort = true;
          }
          else
          {
            Player controller = this.m_entity.GetController();
            if (controller.IsOpposingSide() && (UnityEngine.Object) this == (UnityEngine.Object) this.m_zone.GetLastCard() && !controller.HasTag(GAME_TAG.FIRST_PLAYER))
            {
              GameState.Get().NotifyOfCoinSpawn();
              this.m_actor.TurnOffCollider();
              this.m_actorReady = true;
              flag1 = true;
            }
          }
        }
        if (!flag4)
          ZoneMgr.Get().FindZoneOfType<ZoneDeck>(this.m_zone.m_Side).SetCardToInDeckState(this);
      }
      else if (flag2)
      {
        TransformUtil.CopyWorld((Component) this.transform, (Component) this.m_zone.transform);
        if (this.m_zone is ZonePlay)
          this.ActivateLifetimeEffects();
      }
      else
      {
        if (!this.m_doNotWarpToNewZone)
          TransformUtil.CopyWorld((Component) this.transform, (Component) this.m_zone.transform);
        if (this.m_zone is ZoneHand)
        {
          if (!this.m_doNotWarpToNewZone)
          {
            ZoneHand zone = (ZoneHand) this.m_zone;
            this.transform.localScale = zone.GetCardScale(this);
            this.transform.localEulerAngles = zone.GetCardRotation(this);
            this.transform.position = zone.GetCardPosition(this);
          }
          if (this.m_entity.HasTag(GAME_TAG.LINKED_ENTITY))
          {
            Entity entity = GameState.Get().GetEntity(this.m_entity.GetTag(GAME_TAG.LINKED_ENTITY));
            if (entity != null && (UnityEngine.Object) entity.GetCard() != (UnityEngine.Object) null)
            {
              this.m_actor.Hide();
              this.m_doNotSort = true;
              flag1 = true;
            }
          }
          else
          {
            this.m_actorReady = true;
            this.m_shown = true;
            if (!this.m_doNotWarpToNewZone)
            {
              this.m_actor.Hide();
              this.ActivateHandSpawnSpell();
              flag1 = true;
            }
          }
        }
        if ((UnityEngine.Object) this.m_prevZone == (UnityEngine.Object) null && this.m_zone is ZonePlay)
        {
          if (!this.m_doNotWarpToNewZone)
            this.transform.position = ((ZonePlay) this.m_zone).GetCardPosition(this);
          if (this.m_entity.HasTag(GAME_TAG.LINKED_ENTITY))
          {
            this.m_transitionStyle = ZoneTransitionStyle.INSTANT;
            this.ActivateCharacterPlayEffects();
            this.OnSpellFinished_StandardSpawnMinion((Spell) null, (object) null);
          }
          else
          {
            this.m_actor.Hide();
            this.ActivateMinionSpawnEffects();
          }
          flag1 = true;
        }
        else if (!flag3 && (this.m_zone is ZoneHeroPower || this.m_zone is ZoneWeapon) && this.IsShown())
        {
          this.ActivateDefaultSpawnSpell(new Spell.FinishedCallback(this.OnSpellFinished_DefaultPlaySpawn));
          flag1 = true;
          this.m_actorReady = true;
        }
      }
    }
    else if ((UnityEngine.Object) this.m_prevZone == (UnityEngine.Object) null && (this.m_zone is ZoneHeroPower || this.m_zone is ZoneWeapon))
    {
      oldActor.Destroy();
      TransformUtil.CopyWorld((Component) this.transform, (Component) this.m_zone.transform);
      this.m_transitionStyle = ZoneTransitionStyle.INSTANT;
      this.ActivateDefaultSpawnSpell(new Spell.FinishedCallback(this.OnSpellFinished_DefaultPlaySpawn));
      flag1 = true;
      this.m_actorReady = true;
    }
    else if ((UnityEngine.Object) this.m_prevZone == (UnityEngine.Object) null && this.m_zone is ZoneGraveyard)
    {
      if ((UnityEngine.Object) oldActor != (UnityEngine.Object) null && (UnityEngine.Object) oldActor != (UnityEngine.Object) this.m_actor)
        oldActor.Destroy();
      if (this.IsShown())
        this.HideCard();
      else
        this.HideImpl();
      flag1 = true;
      this.m_actorReady = true;
    }
    else if (this.m_prevZone is ZoneHand && this.m_zone is ZonePlay)
    {
      if (this.ActivateActorSpells_HandToPlay(oldActor))
      {
        if (this.m_cardDef.m_SuppressPlaySoundsOnSummon)
          this.m_suppressPlaySounds = true;
        this.ActivateCharacterPlayEffects();
        this.m_actor.Hide();
        flag1 = true;
        if ((UnityEngine.Object) CardTypeBanner.Get() != (UnityEngine.Object) null && (UnityEngine.Object) CardTypeBanner.Get().GetCardDef() != (UnityEngine.Object) null && (UnityEngine.Object) CardTypeBanner.Get().GetCardDef() == (UnityEngine.Object) this.GetCardDef())
          CardTypeBanner.Get().Hide();
      }
    }
    else if (this.m_prevZone is ZoneHand && this.m_zone is ZoneWeapon)
    {
      if (this.ActivateActorSpells_HandToWeapon(oldActor))
      {
        this.m_actor.Hide();
        flag1 = true;
        if ((UnityEngine.Object) CardTypeBanner.Get() != (UnityEngine.Object) null && (UnityEngine.Object) CardTypeBanner.Get().GetCardDef() != (UnityEngine.Object) null && (UnityEngine.Object) CardTypeBanner.Get().GetCardDef() == (UnityEngine.Object) this.GetCardDef())
          CardTypeBanner.Get().Hide();
      }
    }
    else if (this.m_prevZone is ZonePlay && this.m_zone is ZoneHand)
    {
      if (this.DoPlayToHandTransition(oldActor, false))
        flag1 = true;
    }
    else if (this.m_prevZone is ZoneHero && this.m_zone is ZoneGraveyard)
    {
      oldActor.DoCardDeathVisuals();
      this.DeactivateCustomKeywordEffect();
      flag1 = true;
      this.m_actorReady = true;
    }
    else if ((UnityEngine.Object) this.m_prevZone != (UnityEngine.Object) null && (this.m_prevZone is ZonePlay || this.m_prevZone is ZoneWeapon || this.m_prevZone is ZoneHeroPower) && this.m_zone is ZoneGraveyard)
    {
      if (this.m_mousedOver && this.m_entity.HasSpellPower() && (this.m_entity.IsControlledByFriendlySidePlayer() && this.m_prevZone is ZonePlay))
        ZoneMgr.Get().FindZoneOfType<ZoneHand>(this.m_prevZone.m_Side).OnSpellPowerEntityMousedOut();
      if (this.m_entity.HasTag(GAME_TAG.DEATHRATTLE_RETURN_ZONE) && this.DoesCardReturnFromGraveyard())
      {
        this.m_sideBeforeReturnFromDeath = new Player.Side?(this.m_prevZone.m_Side);
        this.m_prevZone.AddLayoutBlocker();
        Zone entityAndZoneTag = ZoneMgr.Get().FindZoneForEntityAndZoneTag(this.m_entity, this.m_entity.GetTag<TAG_ZONE>(GAME_TAG.DEATHRATTLE_RETURN_ZONE));
        if (entityAndZoneTag is ZoneDeck)
          entityAndZoneTag.AddLayoutBlocker();
        this.m_actorWaitingToBeReplaced = oldActor;
        this.m_actor.Hide();
        flag1 = true;
        this.m_actorReady = true;
      }
      else if (this.HandlePlayActorDeath(oldActor))
        flag1 = true;
    }
    else if (this.m_prevZone is ZoneDeck && this.m_zone is ZoneHand)
    {
      if (this.m_zone.m_Side == Player.Side.FRIENDLY)
      {
        if (GameState.Get().IsPastBeginPhase())
        {
          this.m_actorWaitingToBeReplaced = oldActor;
          this.m_cardStandInInteractive = false;
          if (!TurnStartManager.Get().IsCardDrawHandled(this))
            this.DrawFriendlyCard();
          flag1 = true;
        }
        else
        {
          this.m_actor.TurnOffCollider();
          this.m_actor.SetActorState(ActorStateType.CARD_IDLE);
        }
      }
      else if (GameState.Get().IsPastBeginPhase())
      {
        if ((UnityEngine.Object) oldActor != (UnityEngine.Object) null)
          oldActor.Destroy();
        this.DrawOpponentCard();
        flag1 = true;
      }
    }
    else if (this.m_prevZone is ZoneSecret && this.m_zone is ZoneGraveyard)
    {
      flag1 = true;
      this.m_actorReady = true;
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        this.m_shown = false;
        this.m_actor.Hide();
      }
      else
        this.ShowSecretDeath(oldActor);
    }
    else if (this.m_prevZone is ZoneGraveyard && this.m_zone is ZonePlay)
    {
      this.m_actor.Hide();
      this.StartCoroutine(this.ActivateReviveSpell());
      flag1 = true;
    }
    else if (this.m_prevZone is ZoneDeck && this.m_zone is ZoneGraveyard)
    {
      this.MillCard();
      flag1 = true;
    }
    else if (this.m_prevZone is ZoneDeck && this.m_zone is ZonePlay)
    {
      if ((UnityEngine.Object) oldActor != (UnityEngine.Object) null)
        oldActor.Destroy();
      this.AnimateDeckToPlay();
      flag1 = true;
    }
    else if (this.m_prevZone is ZonePlay && this.m_zone is ZoneDeck)
    {
      this.m_prevZone.AddLayoutBlocker();
      ZoneMgr.Get().FindZoneOfType<ZoneDeck>(this.m_zone.m_Side).AddLayoutBlocker();
      this.DoPlayToDeckTransition(oldActor);
      flag1 = true;
    }
    else if (this.m_prevZone is ZoneGraveyard && this.m_zone is ZoneDeck)
    {
      if (this.HandleGraveyardToDeck(oldActor))
        flag1 = true;
    }
    else if (this.m_prevZone is ZoneGraveyard && this.m_zone is ZoneHand && this.HandleGraveyardToHand(oldActor))
      flag1 = true;
    if (!flag1 && (UnityEngine.Object) oldActor == (UnityEngine.Object) this.m_actor)
    {
      if ((UnityEngine.Object) this.m_prevZone != (UnityEngine.Object) null && this.m_prevZone.m_Side != this.m_zone.m_Side && (this.m_prevZone is ZoneSecret && this.m_zone is ZoneSecret))
      {
        this.StartCoroutine(this.SwitchSecretSides());
        flag1 = true;
      }
      if (flag1)
        return;
      this.m_actorReady = true;
    }
    else
    {
      if (!flag1 && this.m_zone is ZoneSecret)
      {
        this.m_shown = true;
        if ((bool) ((UnityEngine.Object) oldActor))
          oldActor.Destroy();
        this.m_transitionStyle = ZoneTransitionStyle.INSTANT;
        this.ShowSecretBirth();
        flag1 = true;
        this.m_actorReady = true;
        if (flag2)
          this.ActivateStateSpells();
      }
      if (flag1)
        return;
      if ((bool) ((UnityEngine.Object) oldActor))
        oldActor.Destroy();
      if (this.IsShown() && (this.m_zone.m_ServerTag == TAG_ZONE.PLAY || this.m_zone.m_ServerTag == TAG_ZONE.SECRET || this.m_zone.m_ServerTag == TAG_ZONE.HAND))
        this.ActivateStateSpells();
      this.m_actorReady = true;
      if (this.IsShown())
        this.ShowImpl();
      else
        this.HideImpl();
    }
  }

  private bool HandleGraveyardToDeck(Actor oldActor)
  {
    if (!(bool) ((UnityEngine.Object) this.m_actorWaitingToBeReplaced))
      return false;
    if ((bool) ((UnityEngine.Object) oldActor))
      oldActor.Destroy();
    oldActor = this.m_actorWaitingToBeReplaced;
    this.m_actorWaitingToBeReplaced = (Actor) null;
    this.DoPlayToDeckTransition(oldActor);
    return true;
  }

  private bool HandleGraveyardToHand(Actor oldActor)
  {
    if ((bool) ((UnityEngine.Object) this.m_actorWaitingToBeReplaced))
    {
      if ((bool) ((UnityEngine.Object) oldActor) && (UnityEngine.Object) oldActor != (UnityEngine.Object) this.m_actor)
        oldActor.Destroy();
      oldActor = this.m_actorWaitingToBeReplaced;
      this.m_actorWaitingToBeReplaced = (Actor) null;
      if (this.DoPlayToHandTransition(oldActor, true))
        return true;
    }
    return false;
  }

  public bool CardStandInIsInteractive()
  {
    return this.m_cardStandInInteractive;
  }

  private void ReadyCardForDraw()
  {
    this.GetController().GetDeckZone().SetCardToInDeckState(this);
  }

  public void DrawFriendlyCard()
  {
    this.StartCoroutine(this.DrawFriendlyCardWithTiming());
  }

  [DebuggerHidden]
  private IEnumerator DrawFriendlyCardWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CDrawFriendlyCardWithTiming\u003Ec__Iterator2EB() { \u003C\u003Ef__this = this };
  }

  public bool IsBeingDrawnByOpponent()
  {
    return this.m_beingDrawnByOpponent;
  }

  private void DrawOpponentCard()
  {
    this.StartCoroutine(this.DrawOpponentCardWithTiming());
  }

  [DebuggerHidden]
  private IEnumerator DrawOpponentCardWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CDrawOpponentCardWithTiming\u003Ec__Iterator2EC() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator DrawUnknownOpponentCard(ZoneHand handZone)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CDrawUnknownOpponentCard\u003Ec__Iterator2ED() { handZone = handZone, \u003C\u0024\u003EhandZone = handZone, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator DrawKnownOpponentCard(ZoneHand handZone)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CDrawKnownOpponentCard\u003Ec__Iterator2EE() { handZone = handZone, \u003C\u0024\u003EhandZone = handZone, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator RevealDrawnOpponentCard(string handActorName, Actor handActor, ZoneHand handZone)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CRevealDrawnOpponentCard\u003Ec__Iterator2EF() { handActor = handActor, handActorName = handActorName, handZone = handZone, \u003C\u0024\u003EhandActor = handActor, \u003C\u0024\u003EhandActorName = handActorName, \u003C\u0024\u003EhandZone = handZone, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator HideRevealedOpponentCard(Actor handActor)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CHideRevealedOpponentCard\u003Ec__Iterator2F0() { handActor = handActor, \u003C\u0024\u003EhandActor = handActor, \u003C\u003Ef__this = this };
  }

  private void AnimateDeckToPlay()
  {
    if ((UnityEngine.Object) this.m_customSpawnSpellOverride == (UnityEngine.Object) null)
    {
      this.m_doNotSort = true;
      GameObject actorObject1 = AssetLoader.Get().LoadActor(ActorNames.GetHandActor(this.m_entity), false, false);
      Actor component1 = actorObject1.GetComponent<Actor>();
      this.SetupDeckToPlayActor(component1, actorObject1);
      SpellType outSpellHandToPlay = this.m_cardDef.DetermineSummonOutSpell_HandToPlay(this);
      Spell spell = component1.GetSpell(outSpellHandToPlay);
      GameObject actorObject2 = AssetLoader.Get().LoadActor("Card_Hidden", false, false);
      Actor component2 = actorObject2.GetComponent<Actor>();
      this.SetupDeckToPlayActor(component2, actorObject2);
      this.StartCoroutine(this.AnimateDeckToPlay(component1, spell, component2));
    }
    else
    {
      this.m_actor.Hide();
      ZonePlay zone = (ZonePlay) this.m_zone;
      this.SetTransitionStyle(ZoneTransitionStyle.INSTANT);
      zone.UpdateLayout();
      this.ActivateMinionSpawnEffects();
    }
  }

  private void SetupDeckToPlayActor(Actor actor, GameObject actorObject)
  {
    actor.SetEntity(this.m_entity);
    actor.SetCardDef(this.m_cardDef);
    actor.UpdateAllComponents();
    actorObject.transform.parent = this.transform;
    actorObject.transform.localPosition = Vector3.zero;
    actorObject.transform.localScale = Vector3.one;
    actorObject.transform.localRotation = Quaternion.identity;
  }

  [DebuggerHidden]
  private IEnumerator AnimateDeckToPlay(Actor cardFaceActor, Spell outSpell, Actor hiddenActor)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CAnimateDeckToPlay\u003Ec__Iterator2F1() { cardFaceActor = cardFaceActor, hiddenActor = hiddenActor, outSpell = outSpell, \u003C\u0024\u003EcardFaceActor = cardFaceActor, \u003C\u0024\u003EhiddenActor = hiddenActor, \u003C\u0024\u003EoutSpell = outSpell, \u003C\u003Ef__this = this };
  }

  private void MillCard()
  {
    this.StartCoroutine(this.MillCardWithTiming());
  }

  [DebuggerHidden]
  private IEnumerator MillCardWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CMillCardWithTiming\u003Ec__Iterator2F2() { \u003C\u003Ef__this = this };
  }

  private bool ActivateActorSpells_HandToPlay(Actor oldActor)
  {
    if ((UnityEngine.Object) oldActor == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_HandToPlay() - oldActor=null", (object) this));
      return false;
    }
    if ((UnityEngine.Object) this.m_cardDef == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_HandToPlay() - m_cardDef=null", (object) this));
      return false;
    }
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_HandToPlay() - m_actor=null", (object) this));
      return false;
    }
    SpellType outSpellHandToPlay = this.m_cardDef.DetermineSummonOutSpell_HandToPlay(this);
    Spell spell = oldActor.GetSpell(outSpellHandToPlay);
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_HandToPlay() - outSpell=null outSpellType={1}", (object) this, (object) outSpellHandToPlay));
      return false;
    }
    bool standard;
    if ((UnityEngine.Object) this.GetBestSummonSpell(out standard) == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_HandToPlay() - inSpell=null standard={1}", (object) this, (object) standard));
      return false;
    }
    this.m_inputEnabled = false;
    this.ActivateSpell(spell, new Spell.FinishedCallback(this.OnSpellFinished_HandToPlay_SummonOut), (object) null, new Spell.StateFinishedCallback(this.OnSpellStateFinished_DestroyActor));
    return true;
  }

  private void OnSpellFinished_HandToPlay_SummonOut(Spell spell, object userData)
  {
    this.m_actor.Show();
    bool standard;
    Spell bestSummonSpell = this.GetBestSummonSpell(out standard);
    if (!standard)
    {
      bestSummonSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnSpellStateFinished_DestroySpell));
      SpellUtils.SetCustomSpellParent(bestSummonSpell, (Component) this.m_actor);
    }
    bestSummonSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnSpellFinished_HandToPlay_SummonIn));
    bestSummonSpell.Activate();
  }

  private void OnSpellFinished_HandToPlay_SummonIn(Spell spell, object userData)
  {
    this.m_actorReady = true;
    this.m_inputEnabled = true;
    this.ActivateStateSpells();
    this.RefreshActor();
    if ((this.m_entity.HasSpellPower() || this.m_entity.HasSpellPowerDouble()) && this.m_entity.IsControlledByFriendlySidePlayer())
      ZoneMgr.Get().FindZoneOfType<ZoneHand>(this.m_zone.m_Side).OnSpellPowerEntityEnteredPlay();
    if (this.m_entity.HasWindfury())
      this.ActivateActorSpell(SpellType.WINDFURY_BURST);
    this.StartCoroutine(this.ActivateActorBattlecrySpell());
    BoardEvents boardEvents = BoardEvents.Get();
    if (!((UnityEngine.Object) boardEvents != (UnityEngine.Object) null))
      return;
    boardEvents.SummonedEvent(this);
  }

  private bool ActivateActorSpells_HandToWeapon(Actor oldActor)
  {
    if ((UnityEngine.Object) oldActor == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_HandToWeapon() - oldActor=null", (object) this));
      return false;
    }
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_HandToWeapon() - m_actor=null", (object) this));
      return false;
    }
    SpellType spellType1 = SpellType.SUMMON_OUT_WEAPON;
    Spell spell = oldActor.GetSpell(spellType1);
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_HandToWeapon() - outSpell=null outSpellType={1}", (object) this, (object) spellType1));
      return false;
    }
    SpellType spellType2 = !this.m_entity.IsControlledByFriendlySidePlayer() ? SpellType.SUMMON_IN_OPPONENT : SpellType.SUMMON_IN_FRIENDLY;
    Spell actorSpell = this.GetActorSpell(spellType2, true);
    if ((UnityEngine.Object) actorSpell == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_HandToWeapon() - inSpell=null inSpellType={1}", (object) this, (object) spellType2));
      return false;
    }
    this.m_inputEnabled = false;
    this.ActivateSpell(spell, new Spell.FinishedCallback(this.OnSpellFinished_HandToWeapon_SummonOut), (object) actorSpell, new Spell.StateFinishedCallback(this.OnSpellStateFinished_DestroyActor));
    return true;
  }

  private void OnSpellFinished_HandToWeapon_SummonOut(Spell spell, object userData)
  {
    this.m_actor.Show();
    this.ActivateSpell((Spell) userData, new Spell.FinishedCallback(this.OnSpellFinished_StandardCardSummon));
  }

  private void DiscardCardBeingDrawn()
  {
    if ((UnityEngine.Object) this == (UnityEngine.Object) GameState.Get().GetOpponentCardBeingDrawn())
    {
      this.m_actorWaitingToBeReplaced.Destroy();
      this.m_actorWaitingToBeReplaced = (Actor) null;
    }
    if (this.m_actor.IsShown())
      this.ActivateDeathSpell(this.m_actor);
    else
      GameState.Get().ClearCardBeingDrawn(this);
  }

  private void DoDiscardAnimation()
  {
    this.m_doNotSort = true;
    iTween.Stop(this.gameObject);
    float num = 3f;
    if (this.GetEntity().IsControlledByOpposingSidePlayer())
      num = -num;
    iTween.MoveTo(this.gameObject, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + num), 3f);
    iTween.ScaleTo(this.gameObject, this.transform.localScale * 1.5f, 3f);
    this.StartCoroutine(this.ActivateGraveyardActorDeathSpellAfterDelay());
  }

  private bool SwitchOutLinkedDrawnCard()
  {
    if (!(this.m_prevZone is ZoneHand))
      return false;
    Entity entity = GameState.Get().GetEntity(this.m_entity.GetTag(GAME_TAG.LINKED_ENTITY));
    if (entity == null)
    {
      GameState.Get().ClearCardBeingDrawn(this);
      return false;
    }
    Card card = entity.GetCard();
    if ((UnityEngine.Object) card == (UnityEngine.Object) null)
    {
      GameState.Get().ClearCardBeingDrawn(this);
      return false;
    }
    TransformUtil.CopyWorld((Component) card, (Component) this);
    card.m_actorReady = true;
    if (!GameState.Get().IsBeingDrawn(this))
    {
      this.m_doNotSort = false;
      this.DoNullZoneVisuals();
      card.m_actor.Show();
      card.m_doNotSort = false;
      this.m_prevZone.UpdateLayout();
      return true;
    }
    if (this.m_entity.IsControlledByFriendlySidePlayer())
      card.StartCoroutine(card.SwitchOutFriendlyLinkedDrawnCard(this));
    else
      card.StartCoroutine(card.SwitchOutOpponentLinkedDrawnCard(this));
    return true;
  }

  [DebuggerHidden]
  private IEnumerator SwitchOutFriendlyLinkedDrawnCard(Card oldCard)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CSwitchOutFriendlyLinkedDrawnCard\u003Ec__Iterator2F3() { oldCard = oldCard, \u003C\u0024\u003EoldCard = oldCard, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator SwitchOutOpponentLinkedDrawnCard(Card oldCard)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CSwitchOutOpponentLinkedDrawnCard\u003Ec__Iterator2F4() { oldCard = oldCard, \u003C\u0024\u003EoldCard = oldCard, \u003C\u003Ef__this = this };
  }

  private bool DoPlayToHandTransition(Actor oldActor, bool wasInGraveyard = false)
  {
    bool hand = this.ActivateActorSpells_PlayToHand(oldActor, wasInGraveyard);
    if (hand)
      this.m_actor.Hide();
    return hand;
  }

  private bool ActivateActorSpells_PlayToHand(Actor oldActor, bool wasInGraveyard)
  {
    if ((UnityEngine.Object) oldActor == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_PlayToHand() - oldActor=null", (object) this));
      return false;
    }
    if ((UnityEngine.Object) this.m_actor == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_PlayToHand() - m_actor=null", (object) this));
      return false;
    }
    SpellType spellType1 = SpellType.BOUNCE_OUT;
    Spell spell1 = oldActor.GetSpell(spellType1);
    if ((UnityEngine.Object) spell1 == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_PlayToHand() - outSpell=null outSpellType={1}", (object) this, (object) spellType1));
      return false;
    }
    SpellType spellType2 = SpellType.BOUNCE_IN;
    Spell actorSpell = this.GetActorSpell(spellType2, true);
    if ((UnityEngine.Object) actorSpell == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.ActivateActorSpells_PlayToHand() - inSpell=null inSpellType={1}", (object) this, (object) spellType2));
      return false;
    }
    this.m_inputEnabled = false;
    if (this.m_entity.IsControlledByFriendlySidePlayer())
    {
      Spell.FinishedCallback finishedCallback = !wasInGraveyard ? new Spell.FinishedCallback(this.OnSpellFinished_PlayToHand_SummonOut) : new Spell.FinishedCallback(this.OnSpellFinished_PlayToHand_SummonOut_FromGraveyard);
      this.ActivateSpell(spell1, finishedCallback, (object) actorSpell, new Spell.StateFinishedCallback(this.OnSpellStateFinished_DestroyActor));
    }
    else
    {
      if (this.m_entity.IsControlledByOpposingSidePlayer())
      {
        Log.FaceDownCard.Print("Card.ActivateActorSpells_PlayToHand() - {0} - {1} on {2}", new object[3]
        {
          (object) this,
          (object) spellType1,
          (object) oldActor
        });
        Log.FaceDownCard.Print("Card.ActivateActorSpells_PlayToHand() - {0} - {1} on {2}", new object[3]
        {
          (object) this,
          (object) spellType2,
          (object) this.m_actor
        });
      }
      Spell.FinishedCallback finishedCallback = !wasInGraveyard ? (Spell.FinishedCallback) null : (Spell.FinishedCallback) ((spell, userData) => this.ResumeLayoutForPlayZone());
      this.ActivateSpell(spell1, finishedCallback, (object) null, new Spell.StateFinishedCallback(this.OnSpellStateFinished_PlayToHand_OldActor_SummonOut));
      this.ActivateSpell(actorSpell, new Spell.FinishedCallback(this.OnSpellFinished_PlayToHand_SummonIn));
    }
    return true;
  }

  private void OnSpellFinished_PlayToHand_SummonOut(Spell spell, object userData)
  {
    this.ActivateSpell((Spell) userData, new Spell.FinishedCallback(this.OnSpellFinished_StandardCardSummon));
  }

  private void OnSpellFinished_PlayToHand_SummonOut_FromGraveyard(Spell spell, object userData)
  {
    this.OnSpellFinished_PlayToHand_SummonOut(spell, userData);
    this.ResumeLayoutForPlayZone();
  }

  private void ResumeLayoutForPlayZone()
  {
    ZonePlay zoneOfType = ZoneMgr.Get().FindZoneOfType<ZonePlay>(!this.m_sideBeforeReturnFromDeath.HasValue ? this.m_zone.m_Side : this.m_sideBeforeReturnFromDeath.Value);
    zoneOfType.RemoveLayoutBlocker();
    zoneOfType.UpdateLayout();
  }

  private void OnSpellStateFinished_PlayToHand_OldActor_SummonOut(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (this.m_entity.IsControlledByOpposingSidePlayer())
      Log.FaceDownCard.Print("Card.OnSpellStateFinished_PlayToHand_OldActor_SummonOut() - {0} stateType={1}", new object[2]
      {
        (object) this,
        (object) spell.GetActiveState()
      });
    this.OnSpellStateFinished_DestroyActor(spell, prevStateType, userData);
  }

  private void OnSpellFinished_PlayToHand_SummonIn(Spell spell, object userData)
  {
    if (this.m_entity.IsControlledByOpposingSidePlayer())
      Log.FaceDownCard.Print("Card.OnSpellFinished_PlayToHand_SummonIn() - {0}", (object) this);
    this.OnSpellFinished_StandardCardSummon(spell, userData);
  }

  private void DoPlayToDeckTransition(Actor playActor)
  {
    this.m_doNotSort = true;
    this.m_actor.Hide();
    this.StartCoroutine(this.AnimatePlayToDeck(playActor));
  }

  [DebuggerHidden]
  private IEnumerator AnimatePlayToDeck(Actor playActor)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CAnimatePlayToDeck\u003Ec__Iterator2F5() { playActor = playActor, \u003C\u0024\u003EplayActor = playActor, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  public IEnumerator AnimatePlayToDeck(GameObject mover, ZoneDeck deckZone, bool hideBackSide = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CAnimatePlayToDeck\u003Ec__Iterator2F6() { deckZone = deckZone, hideBackSide = hideBackSide, mover = mover, \u003C\u0024\u003EdeckZone = deckZone, \u003C\u0024\u003EhideBackSide = hideBackSide, \u003C\u0024\u003Emover = mover };
  }

  public void SetSecretTriggered(bool set)
  {
    this.m_secretTriggered = set;
  }

  public bool WasSecretTriggered()
  {
    return this.m_secretTriggered;
  }

  public bool CanShowSecretTrigger()
  {
    return !(bool) UniversalInputManager.UsePhoneUI || this.m_zone.IsOnlyCard(this);
  }

  public void ShowSecretTrigger()
  {
    this.m_actor.GetComponent<Spell>().ActivateState(SpellStateType.ACTION);
  }

  private bool CanShowSecret()
  {
    return !(bool) UniversalInputManager.UsePhoneUI || (UnityEngine.Object) this == (UnityEngine.Object) this.m_zone.GetCardAtIndex(0);
  }

  private void ShowSecretBirth()
  {
    Spell component = this.m_actor.GetComponent<Spell>();
    if (!this.CanShowSecret())
    {
      Spell.StateFinishedCallback callback = (Spell.StateFinishedCallback) ((thisSpell, prevStateType, userData) =>
      {
        if (thisSpell.GetActiveState() != SpellStateType.NONE || this.CanShowSecret())
          return;
        this.HideCard();
      });
      component.AddStateFinishedCallback(callback);
    }
    component.ActivateState(SpellStateType.BIRTH);
  }

  public bool CanShowSecretDeath()
  {
    return !(bool) UniversalInputManager.UsePhoneUI || this.m_prevZone.GetCardCount() == 0;
  }

  public void ShowSecretDeath(Actor oldActor)
  {
    Spell component = oldActor.GetComponent<Spell>();
    if (this.m_secretTriggered)
    {
      this.m_secretTriggered = false;
      if (component.GetActiveState() == SpellStateType.NONE)
        oldActor.Destroy();
      else
        component.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnSpellStateFinished_DestroyActor));
    }
    else
    {
      component.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnSpellStateFinished_DestroyActor));
      component.ActivateState(SpellStateType.ACTION);
      oldActor.transform.parent = (Transform) null;
      if ((bool) UniversalInputManager.UsePhoneUI)
        return;
      this.m_doNotSort = true;
      iTween.Stop(this.gameObject);
      this.m_actor.Hide();
      this.StartCoroutine(this.WaitAndThenShowDestroyedSecret());
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitAndThenShowDestroyedSecret()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CWaitAndThenShowDestroyedSecret\u003Ec__Iterator2F7() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator SwitchSecretSides()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CSwitchSecretSides\u003Ec__Iterator2F8() { \u003C\u003Ef__this = this };
  }

  private bool ShouldCardDrawWaitForTurnStartSpells()
  {
    SpellController spellController = TurnStartManager.Get().GetSpellController();
    return !((UnityEngine.Object) spellController == (UnityEngine.Object) null) && (spellController.IsSource(this) || spellController.IsTarget(this));
  }

  [DebuggerHidden]
  private IEnumerator WaitForCardDrawBlockingTurnStartSpells()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CWaitForCardDrawBlockingTurnStartSpells\u003Ec__Iterator2F9() { \u003C\u003Ef__this = this };
  }

  private bool ShouldCardDrawWaitForTaskLists()
  {
    PowerQueue powerQueue = GameState.Get().GetPowerProcessor().GetPowerQueue();
    if (powerQueue.Count == 0)
      return false;
    PowerTaskList taskList = powerQueue.Peek();
    return this.DoesTaskListBlockCardDraw(taskList) || this.DoesTaskListBlockCardDraw(taskList.GetParent());
  }

  [DebuggerHidden]
  private IEnumerator WaitForCardDrawBlockingTaskLists()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CWaitForCardDrawBlockingTaskLists\u003Ec__Iterator2FA() { \u003C\u003Ef__this = this };
  }

  private bool DoesTaskListBlockCardDraw(PowerTaskList taskList)
  {
    if (taskList == null)
      return false;
    Network.HistBlockStart blockStart = taskList.GetBlockStart();
    if (blockStart == null)
      return false;
    switch (blockStart.BlockType)
    {
      case HistoryBlock.Type.POWER:
      case HistoryBlock.Type.TRIGGER:
        if (!taskList.IsComplete())
        {
          if (this.m_entity == taskList.GetSourceEntity())
            return true;
          int entityId = this.m_entity.GetEntityId();
          List<PowerTask> taskList1 = taskList.GetTaskList();
          for (int index = 0; index < taskList1.Count; ++index)
          {
            Network.PowerHistory power = taskList1[index].GetPower();
            int num = 0;
            switch (power.Type)
            {
              case Network.PowerType.SHOW_ENTITY:
                Network.HistShowEntity histShowEntity = (Network.HistShowEntity) power;
                if (histShowEntity.Entity.ID == entityId)
                {
                  Network.Entity.Tag tag = histShowEntity.Entity.Tags.Find((Predicate<Network.Entity.Tag>) (currTag => currTag.Name == 49));
                  if (tag != null)
                  {
                    num = tag.Value;
                    break;
                  }
                  break;
                }
                break;
              case Network.PowerType.HIDE_ENTITY:
                Network.HistHideEntity histHideEntity = (Network.HistHideEntity) power;
                if (histHideEntity.Entity == entityId)
                {
                  num = histHideEntity.Zone;
                  break;
                }
                break;
              case Network.PowerType.TAG_CHANGE:
                Network.HistTagChange histTagChange = (Network.HistTagChange) power;
                if (histTagChange.Entity == entityId && histTagChange.Tag == 49)
                {
                  num = histTagChange.Value;
                  break;
                }
                break;
            }
            if (num != 0 && num != 3)
              return true;
          }
        }
        return this.DoesTaskListBlockCardDraw(taskList.GetNext());
      default:
        return false;
    }
  }

  private void CutoffFriendlyCardDraw()
  {
    if (this.m_actorReady)
      return;
    if ((UnityEngine.Object) this.m_actorWaitingToBeReplaced != (UnityEngine.Object) null)
    {
      this.m_actorWaitingToBeReplaced.Destroy();
      this.m_actorWaitingToBeReplaced = (Actor) null;
    }
    this.m_actor.Show();
    this.m_actor.TurnOffCollider();
    this.m_doNotSort = false;
    this.m_actorReady = true;
    this.ActivateStateSpells();
    this.RefreshActor();
    GameState.Get().ClearCardBeingDrawn(this);
    this.m_zone.UpdateLayout();
  }

  [DebuggerHidden]
  private IEnumerator WaitAndPrepareForDeathAnimation(Actor dyingActor)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CWaitAndPrepareForDeathAnimation\u003Ec__Iterator2FB() { dyingActor = dyingActor, \u003C\u0024\u003EdyingActor = dyingActor, \u003C\u003Ef__this = this };
  }

  private void PrepareForDeathAnimation(Actor dyingActor)
  {
    dyingActor.ToggleCollider(false);
    dyingActor.ToggleForceIdle(true);
    dyingActor.SetActorState(ActorStateType.CARD_IDLE);
    dyingActor.DoCardDeathVisuals();
    this.DeactivateCustomKeywordEffect();
  }

  [DebuggerHidden]
  private IEnumerator ActivateGraveyardActorDeathSpellAfterDelay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Card.\u003CActivateGraveyardActorDeathSpellAfterDelay\u003Ec__Iterator2FC() { \u003C\u003Ef__this = this };
  }

  private bool HandlePlayActorDeath(Actor oldActor)
  {
    bool flag = false;
    if (!this.m_cardDef.m_SuppressDeathrattleDeath && this.m_entity.HasDeathrattle())
      this.ActivateActorSpell(oldActor, SpellType.DEATHRATTLE_DEATH);
    if (this.m_suppressDeathEffects)
    {
      if ((bool) ((UnityEngine.Object) oldActor))
        oldActor.Destroy();
      if (this.IsShown())
        this.ShowImpl();
      else
        this.HideImpl();
      flag = true;
      this.m_actorReady = true;
    }
    else
    {
      if (!this.m_suppressKeywordDeaths)
        this.StartCoroutine(this.WaitAndPrepareForDeathAnimation(oldActor));
      if ((UnityEngine.Object) this.ActivateDeathSpell(oldActor) != (UnityEngine.Object) null)
      {
        this.m_actor.Hide();
        flag = true;
        this.m_actorReady = true;
      }
    }
    return flag;
  }

  private bool DoesCardReturnFromGraveyard()
  {
    foreach (PowerTaskList power in (QueueList<PowerTaskList>) GameState.Get().GetPowerProcessor().GetPowerQueue())
    {
      if (this.DoesTaskListReturnCardFromGraveyard(power))
      {
        Log.JMac.PrintWarning("Found the task for returning entity {0} from graveyard!", (object) this.m_entity);
        return true;
      }
    }
    return false;
  }

  private bool DoesTaskListReturnCardFromGraveyard(PowerTaskList taskList)
  {
    if (!taskList.IsTriggerBlock())
      return false;
    using (List<PowerTask>.Enumerator enumerator = taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.PowerHistory power = enumerator.Current.GetPower();
        if (power.Type == Network.PowerType.TAG_CHANGE)
        {
          Network.HistTagChange histTagChange = power as Network.HistTagChange;
          if (histTagChange.Tag == 49 && histTagChange.Entity == this.m_entity.GetEntityId())
            return histTagChange.Value != 6;
        }
      }
    }
    return false;
  }

  private void DoNullZoneVisuals()
  {
    if ((UnityEngine.Object) this.m_actor != (UnityEngine.Object) null)
      this.m_actor.DeactivateAllSpells();
    this.HideCard();
  }

  private class SpellLoadRequest
  {
    public string m_path;
    public AssetLoader.GameObjectCallback m_loadCallback;
  }

  public enum AnnouncerLineType
  {
    DEFAULT,
    BEFORE_VERSUS,
    AFTER_VERSUS,
    MAX,
  }
}
