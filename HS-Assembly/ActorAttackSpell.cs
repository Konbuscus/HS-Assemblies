﻿// Decompiled with JetBrains decompiler
// Type: ActorAttackSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;

public class ActorAttackSpell : Spell
{
  private bool m_waitingToAct = true;

  protected override void Start()
  {
    base.Start();
  }

  protected override void OnBirth(SpellStateType prevStateType)
  {
    this.m_waitingToAct = true;
    base.OnBirth(prevStateType);
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    this.StartCoroutine(this.WaitThenDoAction(prevStateType));
  }

  private void StopWaitingToAct()
  {
    this.m_waitingToAct = false;
  }

  [DebuggerHidden]
  protected IEnumerator WaitThenDoAction(SpellStateType prevStateType)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ActorAttackSpell.\u003CWaitThenDoAction\u003Ec__Iterator254() { prevStateType = prevStateType, \u003C\u0024\u003EprevStateType = prevStateType, \u003C\u003Ef__this = this };
  }
}
