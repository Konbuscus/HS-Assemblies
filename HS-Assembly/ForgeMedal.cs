﻿// Decompiled with JetBrains decompiler
// Type: ForgeMedal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ForgeMedal : PegUIElement
{
  private List<ForgeMedal.ForgeMedalInfo> m_forgeMedalInfos = new List<ForgeMedal.ForgeMedalInfo>();
  private Medal m_medal;
  private Medal m_nextMedal;

  protected override void Awake()
  {
    base.Awake();
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(Medal.Type.MEDAL_NOVICE), new Vector2(0.0f, 0.0f)));
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(Medal.Type.MEDAL_APPRENTICE), new Vector2(0.25f, 0.0f)));
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(Medal.Type.MEDAL_JOURNEYMAN), new Vector2(0.5f, 0.0f)));
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(Medal.Type.MEDAL_COPPER_A), new Vector2(0.75f, 0.0f)));
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(Medal.Type.MEDAL_SILVER_A), new Vector2(0.0f, -0.25f)));
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(Medal.Type.MEDAL_GOLD_A), new Vector2(0.25f, -0.25f)));
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(Medal.Type.MEDAL_PLATINUM_A), new Vector2(0.5f, -0.25f)));
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(Medal.Type.MEDAL_DIAMOND_A), new Vector2(0.75f, -0.25f)));
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(Medal.Type.MEDAL_MASTER_A), new Vector2(0.0f, -0.5f)));
    this.m_forgeMedalInfos.Add(new ForgeMedal.ForgeMedalInfo(new Medal(1), new Vector2(0.25f, -0.5f)));
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.MedalOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.MedalOut));
  }

  public void SetMedal(int wins)
  {
    ForgeMedal.ForgeMedalInfo forgeMedalInfo = this.m_forgeMedalInfos[wins];
    this.m_medal = forgeMedalInfo.ForgeMedal;
    int index = wins + 1;
    this.m_nextMedal = this.m_forgeMedalInfos.Count <= index ? (Medal) null : this.m_forgeMedalInfos[index].ForgeMedal;
    this.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", forgeMedalInfo.TextureCoords);
  }

  private void MedalOver(UIEvent e)
  {
    string headline = GameStrings.Format("GLOBAL_MEDAL_TOOLTIP_HEADER", (object) this.m_medal.GetBaseMedalName());
    string empty = string.Empty;
    string bodytext;
    if (this.m_nextMedal == null)
      bodytext = GameStrings.Get("GLOBAL_MEDAL_TOOLTIP_BODY_ARENA_GRAND_MASTER");
    else
      bodytext = GameStrings.Format("GLOBAL_MEDAL_TOOLTIP_BODY", (object) this.m_nextMedal.GetBaseMedalName());
    float scale = !SceneMgr.Get().IsInGame() ? (!SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB) ? 3f : 6f) : 0.3f;
    this.gameObject.GetComponent<TooltipZone>().ShowTooltip(headline, bodytext, scale, true);
  }

  private void MedalOut(UIEvent e)
  {
    this.gameObject.GetComponent<TooltipZone>().HideTooltip();
  }

  private class ForgeMedalInfo
  {
    public Vector2 TextureCoords { get; private set; }

    public Medal ForgeMedal { get; private set; }

    public ForgeMedalInfo(Medal forgeMedal, Vector2 textureCoords)
    {
      this.ForgeMedal = forgeMedal;
      this.TextureCoords = textureCoords;
    }
  }
}
