﻿// Decompiled with JetBrains decompiler
// Type: MobileChatLogUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MobileChatLogUI : IChatLogUI
{
  private ChatFrames m_chatFrames;

  public bool IsShowing
  {
    get
    {
      return (Object) this.m_chatFrames != (Object) null;
    }
  }

  public GameObject GameObject
  {
    get
    {
      if ((Object) this.m_chatFrames == (Object) null)
        return (GameObject) null;
      return this.m_chatFrames.gameObject;
    }
  }

  public BnetPlayer Receiver
  {
    get
    {
      if ((Object) this.m_chatFrames == (Object) null)
        return (BnetPlayer) null;
      return this.m_chatFrames.Receiver;
    }
  }

  public void ShowForPlayer(BnetPlayer player)
  {
    GameObject gameObject = AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "MobileChatFrames" : "MobileChatFrames_phone", true, false);
    if (!((Object) gameObject != (Object) null))
      return;
    this.m_chatFrames = gameObject.GetComponent<ChatFrames>();
    this.m_chatFrames.Receiver = player;
  }

  public void Hide()
  {
    if (!this.IsShowing)
      return;
    Object.Destroy((Object) this.m_chatFrames.gameObject);
    this.m_chatFrames = (ChatFrames) null;
  }

  public void GoBack()
  {
    if (!this.IsShowing)
      return;
    this.m_chatFrames.Back();
  }
}
