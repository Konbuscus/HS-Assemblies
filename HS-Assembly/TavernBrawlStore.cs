﻿// Decompiled with JetBrains decompiler
// Type: TavernBrawlStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System.Collections.Generic;
using UnityEngine;

public class TavernBrawlStore : Store
{
  private static readonly int NUM_BUNDLE_ITEMS_REQUIRED = 1;
  public UIBButton m_ContinueButton;
  public UIBButton m_backButton;
  public GameObject m_storeClosed;
  public PlayMakerFSM m_ButtonFlipper;
  public PlayMakerFSM m_PaperEffect;
  public UberText m_EndsInTextPaper;
  public UberText m_EndsInTextChalk;
  public UberText m_ChalkboardTitleText;
  public UberText m_ChalkboardDescriptionText;
  private Network.Bundle m_bundle;
  private static TavernBrawlStore s_instance;

  protected override void Start()
  {
    base.Start();
    this.m_ContinueButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnContinuePressed));
    this.m_backButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBackPressed));
  }

  protected override void Awake()
  {
    TavernBrawlStore.s_instance = this;
    this.m_destroyOnSceneLoad = false;
    base.Awake();
    this.m_backButton.SetText(GameStrings.Get("GLOBAL_BACK"));
    this.m_infoButton.GetComponent<BoxCollider>().enabled = false;
  }

  protected override void OnDestroy()
  {
    TavernBrawlStore.s_instance = (TavernBrawlStore) null;
  }

  public static TavernBrawlStore Get()
  {
    return TavernBrawlStore.s_instance;
  }

  public override void Hide()
  {
    if ((Object) ShownUIMgr.Get() != (Object) null)
      ShownUIMgr.Get().ClearShownUI();
    FriendChallengeMgr.Get().OnStoreClosed();
    StoreManager.Get().RemoveAuthorizationExitListener(new StoreManager.AuthorizationExitCallback(this.OnAuthExit));
    this.EnableFullScreenEffects(false);
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      BnetBar.Get().SetCurrencyType(new CurrencyFrame.CurrencyType?());
      BnetBar.Get().UpdateForPhone();
    }
    base.Hide();
  }

  public override void OnMoneySpent()
  {
    this.UpdateMoneyButtonState();
  }

  public override void OnGoldBalanceChanged(NetCache.NetCacheGoldBalance balance)
  {
    this.UpdateGoldButtonState(balance);
  }

  protected override void ShowImpl(Store.DelOnStoreShown onStoreShownCB, bool isTotallyFake)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TavernBrawlStore.\u003CShowImpl\u003Ec__AnonStorey3FE implCAnonStorey3Fe = new TavernBrawlStore.\u003CShowImpl\u003Ec__AnonStorey3FE();
    // ISSUE: reference to a compiler-generated field
    implCAnonStorey3Fe.isTotallyFake = isTotallyFake;
    // ISSUE: reference to a compiler-generated field
    implCAnonStorey3Fe.onStoreShownCB = onStoreShownCB;
    // ISSUE: reference to a compiler-generated field
    implCAnonStorey3Fe.\u003C\u003Ef__this = this;
    this.m_shown = true;
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    StoreManager.Get().RegisterAuthorizationExitListener(new StoreManager.AuthorizationExitCallback(this.OnAuthExit));
    this.EnableFullScreenEffects(true);
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(TavernBrawlManager.Get().CurrentMission().missionId);
    this.m_ChalkboardTitleText.Text = (string) record.Name;
    this.m_ChalkboardDescriptionText.Text = (string) record.Description;
    string endingTimeText = TavernBrawlManager.Get().EndingTimeText;
    this.m_EndsInTextPaper.Text = endingTimeText;
    this.m_EndsInTextChalk.Text = endingTimeText;
    this.SetUpBuyButtons();
    ShownUIMgr.Get().SetShownUI(ShownUIMgr.UI_WINDOW.TAVERN_BRAWL_STORE);
    FriendChallengeMgr.Get().OnStoreOpened();
    // ISSUE: reference to a compiler-generated method
    this.DoShowAnimation(new UIBPopup.OnAnimationComplete(implCAnonStorey3Fe.\u003C\u003Em__1F9));
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    BnetBar.Get().SetCurrencyType(new CurrencyFrame.CurrencyType?(CurrencyFrame.CurrencyType.GOLD));
    BnetBar.Get().UpdateForPhone();
  }

  protected override void BuyWithGold(UIEvent e)
  {
    if (this.m_bundle == null)
      return;
    this.FireBuyWithGoldEventGTAPP(this.m_bundle.ProductID, 1);
  }

  protected override void BuyWithMoney(UIEvent e)
  {
    if (this.m_bundle == null)
      return;
    this.FireBuyWithMoneyEvent(this.m_bundle.ProductID, 1);
  }

  private void OnAuthExit(object userData)
  {
    Navigation.Pop();
    this.ExitTavernBrawlStore(true);
  }

  private void OnBackPressed(UIEvent e)
  {
    Navigation.GoBack();
  }

  private void OnContinuePressed(UIEvent e)
  {
    this.m_ButtonFlipper.SendEvent("Flip");
    this.m_PaperEffect.SendEvent("BurnAway");
    this.m_infoButton.GetComponent<BoxCollider>().enabled = true;
  }

  private bool OnNavigateBack()
  {
    this.ExitTavernBrawlStore(false);
    return true;
  }

  private void ExitTavernBrawlStore(bool authorizationBackButtonPressed)
  {
    this.ActivateCover(false);
    SceneUtils.SetLayer(this.gameObject, GameLayer.Default);
    this.EnableFullScreenEffects(false);
    StoreManager.Get().RemoveAuthorizationExitListener(new StoreManager.AuthorizationExitCallback(this.OnAuthExit));
    this.FireExitEvent(authorizationBackButtonPressed);
  }

  private void UpdateMoneyButtonState()
  {
    Store.BuyButtonState state = Store.BuyButtonState.ENABLED;
    if (this.m_bundle == null || !StoreManager.Get().IsOpen())
    {
      state = Store.BuyButtonState.DISABLED;
      this.m_storeClosed.SetActive(true);
    }
    else if (!StoreManager.Get().IsBattlePayFeatureEnabled())
      state = Store.BuyButtonState.DISABLED_FEATURE;
    else if (StoreManager.Get().IsPromptShowing())
    {
      state = Store.BuyButtonState.DISABLED;
      this.SetGoldButtonState(state);
    }
    else
      this.m_storeClosed.SetActive(false);
    this.SetMoneyButtonState(state);
  }

  private void UpdateGoldButtonState(NetCache.NetCacheGoldBalance balance)
  {
    Store.BuyButtonState state = Store.BuyButtonState.ENABLED;
    if (StoreManager.Get().IsPromptShowing())
    {
      state = Store.BuyButtonState.DISABLED;
      this.SetMoneyButtonState(state);
    }
    else if (this.m_bundle == null)
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsOpen())
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsBuyWithGoldFeatureEnabled())
      state = Store.BuyButtonState.DISABLED_FEATURE;
    else if (!StoreManager.Get().IsBundleGoldOptionAvailableNow(this.m_bundle))
      state = Store.BuyButtonState.DISABLED_NO_TOOLTIP;
    else if (balance == null)
      state = Store.BuyButtonState.DISABLED;
    else if (balance.GetTotal() < this.m_bundle.GoldCost.Value)
      state = Store.BuyButtonState.DISABLED_NOT_ENOUGH_GOLD;
    this.SetGoldButtonState(state);
  }

  private void SetUpBuyButtons()
  {
    int ticketType = TavernBrawlManager.Get().CurrentMission().ticketType;
    bool productTypeExists = false;
    List<Network.Bundle> bundlesForProduct = StoreManager.Get().GetAvailableBundlesForProduct(ProductType.PRODUCT_TYPE_TAVERN_BRAWL_TICKET, true, false, out productTypeExists, ticketType, TavernBrawlStore.NUM_BUNDLE_ITEMS_REQUIRED);
    this.m_bundle = bundlesForProduct.Count != 0 ? bundlesForProduct[0] : (Network.Bundle) null;
    this.SetUpBuyWithGoldButton();
    this.SetUpBuyWithMoneyButton();
  }

  private void SetUpBuyWithGoldButton()
  {
    string empty = string.Empty;
    string text;
    if (this.m_bundle != null && this.m_bundle.GoldCost.HasValue)
    {
      text = this.m_bundle.GoldCost.ToString();
      this.UpdateGoldButtonState(NetCache.Get().GetNetObject<NetCache.NetCacheGoldBalance>());
    }
    else
    {
      Debug.LogWarning((object) "TavernBrawlStore.SetUpBuyWithGoldButton(): m_bundle is null");
      text = GameStrings.Get("GLUE_STORE_PRODUCT_PRICE_NA");
      this.SetGoldButtonState(Store.BuyButtonState.DISABLED);
    }
    this.m_buyWithGoldButton.SetText(text);
  }

  private void SetUpBuyWithMoneyButton()
  {
    string empty = string.Empty;
    string text;
    if (this.m_bundle != null)
    {
      text = StoreManager.Get().FormatCostBundle(this.m_bundle);
      this.UpdateMoneyButtonState();
    }
    else
    {
      Debug.LogWarning((object) "TavernBrawlStore.SetUpBuyWithMoneyButton(): m_bundle is null");
      text = GameStrings.Get("GLUE_STORE_PRODUCT_PRICE_NA");
      this.SetMoneyButtonState(Store.BuyButtonState.DISABLED);
    }
    this.m_buyWithMoneyButton.SetText(text);
  }
}
