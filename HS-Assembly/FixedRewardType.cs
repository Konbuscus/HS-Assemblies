﻿// Decompiled with JetBrains decompiler
// Type: FixedRewardType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.ComponentModel;

public enum FixedRewardType
{
  UNKNOWN,
  [Description("card")] VIRTUAL_CARD,
  [Description("real_card")] REAL_CARD,
  [Description("cardback")] CARD_BACK,
  [Description("craftable_card")] CRAFTABLE_CARD,
  [Description("meta_action_flags")] META_ACTION_FLAGS,
}
