﻿// Decompiled with JetBrains decompiler
// Type: ShakeMinionsAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using HutongGames.PlayMaker;
using UnityEngine;

[Tooltip("Shake Minions")]
[ActionCategory("Pegasus")]
public class ShakeMinionsAction : FsmStateAction
{
  [RequiredField]
  [Tooltip("Shake Type")]
  public ShakeMinionType shakeType = ShakeMinionType.RandomDirection;
  [RequiredField]
  [Tooltip("Shake Intensity")]
  public ShakeMinionIntensity shakeSize = ShakeMinionIntensity.SmallShake;
  [RequiredField]
  [Tooltip("Impact Object Location")]
  public FsmOwnerDefault gameObject;
  [Tooltip("Minions To Shake")]
  [RequiredField]
  public ShakeMinionsAction.MinionsToShakeEnum MinionsToShake;
  [Tooltip("Custom Shake Intensity 0-1. Used when Shake Size is Custom")]
  [RequiredField]
  public FsmFloat customShakeIntensity;
  [RequiredField]
  [Tooltip("Radius - 0 = for all objects")]
  public FsmFloat radius;

  public override void Reset()
  {
    this.gameObject = (FsmOwnerDefault) null;
    this.MinionsToShake = ShakeMinionsAction.MinionsToShakeEnum.All;
    this.shakeType = ShakeMinionType.RandomDirection;
    this.shakeSize = ShakeMinionIntensity.SmallShake;
    this.customShakeIntensity = (FsmFloat) 0.1f;
    this.radius = (FsmFloat) 0.0f;
  }

  public override void OnEnter()
  {
    this.DoShakeMinions();
    this.Finish();
  }

  private void DoShakeMinions()
  {
    GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
    if ((Object) ownerDefaultTarget == (Object) null)
      this.Finish();
    else if (this.MinionsToShake == ShakeMinionsAction.MinionsToShakeEnum.All)
      MinionShake.ShakeAllMinions(ownerDefaultTarget, this.shakeType, ownerDefaultTarget.transform.position, this.shakeSize, this.customShakeIntensity.Value, this.radius.Value, 0.0f);
    else if (this.MinionsToShake == ShakeMinionsAction.MinionsToShakeEnum.Target)
    {
      MinionShake.ShakeTargetMinion(ownerDefaultTarget, this.shakeType, ownerDefaultTarget.transform.position, this.shakeSize, this.customShakeIntensity.Value, 0.0f, 0.0f);
    }
    else
    {
      if (this.MinionsToShake != ShakeMinionsAction.MinionsToShakeEnum.SelectedGameObject)
        return;
      MinionShake.ShakeObject(ownerDefaultTarget, this.shakeType, ownerDefaultTarget.transform.position, this.shakeSize, this.customShakeIntensity.Value, 0.0f, 0.0f);
    }
  }

  public enum MinionsToShakeEnum
  {
    All,
    Target,
    SelectedGameObject,
  }
}
