﻿// Decompiled with JetBrains decompiler
// Type: HeroSwapSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class HeroSwapSpell : Spell
{
  public Spell m_OldHeroFX;
  public Spell m_NewHeroFX;
  public Spell m_OldHeroFX_long;
  public Spell m_NewHeroFX_long;
  public float m_FinishDelay;
  public bool removeOldStats;
  protected Card m_oldHeroCard;
  protected Card m_newHeroCard;
  protected Spell m_OldHeroFXToUse;
  protected Spell m_NewHeroFXToUse;

  public override bool AddPowerTargets()
  {
    KAR12_Portals gameEntity = GameState.Get().GetGameEntity() as KAR12_Portals;
    if (gameEntity != null && gameEntity.ShouldPlayLongMidmissionCutscene())
    {
      this.m_OldHeroFXToUse = this.m_OldHeroFX_long;
      this.m_NewHeroFXToUse = this.m_NewHeroFX_long;
    }
    else
    {
      this.m_OldHeroFXToUse = this.m_OldHeroFX;
      this.m_NewHeroFXToUse = this.m_NewHeroFX;
    }
    this.m_oldHeroCard = (Card) null;
    this.m_newHeroCard = (Card) null;
    using (List<PowerTask>.Enumerator enumerator = this.m_taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.PowerHistory power = enumerator.Current.GetPower();
        if (power.Type == Network.PowerType.FULL_ENTITY)
        {
          int id = ((Network.HistFullEntity) power).Entity.ID;
          Entity entity = GameState.Get().GetEntity(id);
          if (entity == null)
          {
            UnityEngine.Debug.LogWarning((object) string.Format("{0}.AddPowerTargets() - WARNING encountered HistFullEntity where entity id={1} but there is no entity with that id", (object) this, (object) id));
            return false;
          }
          if (entity.IsHero())
            this.m_newHeroCard = entity.GetCard();
        }
        else if (power.Type == Network.PowerType.TAG_CHANGE)
        {
          Network.HistTagChange histTagChange = (Network.HistTagChange) power;
          if (histTagChange.Tag == 49 && histTagChange.Value == 6)
          {
            int entity1 = histTagChange.Entity;
            Entity entity2 = GameState.Get().GetEntity(entity1);
            if (entity2 == null)
            {
              UnityEngine.Debug.LogWarning((object) string.Format("{0}.AddPowerTargets() - WARNING encountered HistTagChange where entity id={1} but there is no entity with that id", (object) this, (object) entity1));
              return false;
            }
            if (entity2.IsHero())
              this.m_oldHeroCard = entity2.GetCard();
          }
        }
      }
    }
    if (!(bool) ((Object) this.m_oldHeroCard))
    {
      this.m_newHeroCard = (Card) null;
      return false;
    }
    if ((bool) ((Object) this.m_newHeroCard))
      return true;
    this.m_oldHeroCard = (Card) null;
    return false;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    this.StartCoroutine(this.SetupHero());
  }

  [DebuggerHidden]
  private IEnumerator SetupHero()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroSwapSpell.\u003CSetupHero\u003Ec__Iterator2BA() { \u003C\u003Ef__this = this };
  }

  public virtual void CustomizeFXProcess(Actor heroActor)
  {
  }

  [DebuggerHidden]
  private IEnumerator PlaySwapFx(Spell heroFX, Card heroCard)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroSwapSpell.\u003CPlaySwapFx\u003Ec__Iterator2BB() { heroCard = heroCard, heroFX = heroFX, \u003C\u0024\u003EheroCard = heroCard, \u003C\u0024\u003EheroFX = heroFX, \u003C\u003Ef__this = this };
  }

  private PowerTask FindFullEntityTask()
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PowerTask current = enumerator.Current;
        if (current.GetPower().Type == Network.PowerType.FULL_ENTITY)
          return current;
      }
    }
    return (PowerTask) null;
  }

  private void Finish()
  {
    this.m_newHeroCard.GetActor().TurnOnCollider();
    this.m_newHeroCard.ShowCard();
    this.OnSpellFinished();
  }
}
