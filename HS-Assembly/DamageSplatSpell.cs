﻿// Decompiled with JetBrains decompiler
// Type: DamageSplatSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DamageSplatSpell : Spell
{
  private const float SCALE_IN_TIME = 1f;
  public GameObject m_BloodSplat;
  public GameObject m_HealSplat;
  public UberText m_DamageTextMesh;
  private GameObject m_activeSplat;
  private int m_damage;

  protected override void Awake()
  {
    base.Awake();
    this.EnableAllRenderers(false);
  }

  public float GetDamage()
  {
    return (float) this.m_damage;
  }

  public void SetDamage(int damage)
  {
    this.m_damage = damage;
  }

  public void DoSplatAnims()
  {
    this.StopAllCoroutines();
    iTween.Stop(this.gameObject);
    this.StartCoroutine(this.SplatAnimCoroutine());
  }

  [DebuggerHidden]
  private IEnumerator SplatAnimCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DamageSplatSpell.\u003CSplatAnimCoroutine\u003Ec__Iterator25A() { \u003C\u003Ef__this = this };
  }

  protected override void OnIdle(SpellStateType prevStateType)
  {
    this.UpdateElements();
    base.OnIdle(prevStateType);
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    this.UpdateElements();
    base.OnAction(prevStateType);
    this.DoSplatAnims();
  }

  protected override void OnNone(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    this.m_activeSplat = (GameObject) null;
  }

  protected override void ShowImpl()
  {
    base.ShowImpl();
    if ((Object) this.m_activeSplat == (Object) null)
      return;
    SceneUtils.EnableRenderers(this.m_activeSplat.gameObject, true);
    this.m_DamageTextMesh.gameObject.SetActive(true);
  }

  protected override void HideImpl()
  {
    base.HideImpl();
    this.EnableAllRenderers(false);
  }

  private void UpdateElements()
  {
    if (this.m_damage < 0)
    {
      this.m_DamageTextMesh.Text = string.Format("+{0}", (object) Mathf.Abs(this.m_damage));
      this.m_activeSplat = this.m_HealSplat;
      SceneUtils.EnableRenderers(this.m_BloodSplat.gameObject, false);
      SceneUtils.EnableRenderers(this.m_HealSplat.gameObject, true);
    }
    else
    {
      this.m_DamageTextMesh.Text = string.Format("-{0}", (object) this.m_damage);
      this.m_activeSplat = this.m_BloodSplat;
      SceneUtils.EnableRenderers(this.m_BloodSplat.gameObject, true);
      SceneUtils.EnableRenderers(this.m_HealSplat.gameObject, false);
    }
    this.m_DamageTextMesh.gameObject.SetActive(true);
  }

  private void EnableAllRenderers(bool enabled)
  {
    SceneUtils.EnableRenderers(this.m_BloodSplat.gameObject, enabled);
    SceneUtils.EnableRenderers(this.m_HealSplat.gameObject, enabled);
    this.m_DamageTextMesh.gameObject.SetActive(enabled);
  }
}
