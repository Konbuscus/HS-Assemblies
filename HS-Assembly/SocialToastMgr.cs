﻿// Decompiled with JetBrains decompiler
// Type: SocialToastMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using PegasusClient;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialToastMgr : MonoBehaviour
{
  private PlatformDependentValue<Vector3> TOAST_SCALE = new PlatformDependentValue<Vector3>(PlatformCategory.Screen)
  {
    PC = new Vector3(235f, 1f, 235f),
    Phone = new Vector3(470f, 1f, 470f)
  };
  private Map<BnetGameAccountId, SocialToastMgr.LastKnownMedalTracker> m_lastKnownMedals = new Map<BnetGameAccountId, SocialToastMgr.LastKnownMedalTracker>();
  private Map<int, SocialToastMgr.LastOnlineTracker> m_lastOnlineTracker = new Map<int, SocialToastMgr.LastOnlineTracker>();
  private const float FADE_IN_TIME = 0.25f;
  private const float FADE_OUT_TIME = 0.5f;
  private const float HOLD_TIME = 2f;
  private const float SHUTDOWN_MESSAGE_TIME = 3.5f;
  private const float OFFLINE_TOAST_DELAY = 5f;
  public SocialToast m_socialToastPrefab;
  private static SocialToastMgr s_instance;
  private SocialToast m_toast;
  private bool m_toastIsShown;

  private void Awake()
  {
    SocialToastMgr.s_instance = this;
    this.m_toast = UnityEngine.Object.Instantiate<SocialToast>(this.m_socialToastPrefab);
    RenderUtils.SetAlpha(this.m_toast.gameObject, 0.0f);
    this.m_toast.gameObject.SetActive(false);
    this.m_toast.transform.parent = BnetBar.Get().m_socialToastBone.transform;
    this.m_toast.transform.localRotation = Quaternion.Euler(new Vector3(90f, 180f, 0.0f));
    this.m_toast.transform.localScale = (Vector3) this.TOAST_SCALE;
    this.m_toast.transform.position = BnetBar.Get().m_socialToastBone.transform.position;
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    BnetPresenceMgr.Get().OnGameAccountPresenceChange += new System.Action<PresenceUpdate[]>(this.OnPresenceChanged);
    BnetFriendMgr.Get().AddChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
    Network.Get().SetShutdownHandler(new Network.ShutdownHandler(this.ShutdownHandler));
    SoundManager.Get().Load("UI_BnetToast");
  }

  private void OnDestroy()
  {
    BnetPresenceMgr.Get().OnGameAccountPresenceChange -= new System.Action<PresenceUpdate[]>(this.OnPresenceChanged);
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    BnetFriendMgr.Get().RemoveChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
    this.m_lastKnownMedals.Clear();
    SocialToastMgr.s_instance = (SocialToastMgr) null;
  }

  public static SocialToastMgr Get()
  {
    return SocialToastMgr.s_instance;
  }

  public void Reset()
  {
    iTween.Stop(this.m_toast.gameObject, true);
    iTween.Stop(this.gameObject, true);
    RenderUtils.SetAlpha(this.m_toast.gameObject, 0.0f);
    this.DeactivateToast();
  }

  public void AddToast(UserAttentionBlocker blocker, string textArg)
  {
    this.AddToast(blocker, textArg, SocialToastMgr.TOAST_TYPE.DEFAULT, 2f, true);
  }

  public void AddToast(UserAttentionBlocker blocker, string textArg, SocialToastMgr.TOAST_TYPE toastType)
  {
    this.AddToast(blocker, textArg, toastType, 2f, true);
  }

  public void AddToast(UserAttentionBlocker blocker, string textArg, SocialToastMgr.TOAST_TYPE toastType, bool playSound)
  {
    this.AddToast(blocker, textArg, toastType, 2f, playSound);
  }

  public void AddToast(UserAttentionBlocker blocker, string textArg, SocialToastMgr.TOAST_TYPE toastType, float displayTime)
  {
    this.AddToast(blocker, textArg, toastType, displayTime, true);
  }

  public void AddToast(UserAttentionBlocker blocker, string textArg, SocialToastMgr.TOAST_TYPE toastType, float displayTime, bool playSound)
  {
    if (!UserAttentionManager.CanShowAttentionGrabber(blocker, "SocialToastMgr.AddToast:" + (object) toastType))
      return;
    string text;
    switch (toastType)
    {
      case SocialToastMgr.TOAST_TYPE.DEFAULT:
        text = textArg;
        break;
      case SocialToastMgr.TOAST_TYPE.FRIEND_ONLINE:
        text = GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_ONLINE", (object) "5ecaf0ff", (object) textArg);
        break;
      case SocialToastMgr.TOAST_TYPE.FRIEND_OFFLINE:
        text = GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_OFFLINE", (object) "999999ff", (object) textArg);
        break;
      case SocialToastMgr.TOAST_TYPE.FRIEND_INVITE:
        text = GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_REQUEST", (object) "5ecaf0ff", (object) textArg);
        break;
      case SocialToastMgr.TOAST_TYPE.HEALTHY_GAMING:
        text = GameStrings.Format("GLOBAL_HEALTHY_GAMING_TOAST", (object) textArg);
        break;
      case SocialToastMgr.TOAST_TYPE.HEALTHY_GAMING_OVER_THRESHOLD:
        text = GameStrings.Format("GLOBAL_HEALTHY_GAMING_TOAST_OVER_THRESHOLD", (object) textArg);
        break;
      case SocialToastMgr.TOAST_TYPE.SPECTATOR_INVITE_SENT:
        text = GameStrings.Format("GLOBAL_SOCIAL_TOAST_SPECTATOR_INVITE_SENT", (object) "5ecaf0ff", (object) textArg);
        break;
      case SocialToastMgr.TOAST_TYPE.SPECTATOR_INVITE_RECEIVED:
        text = GameStrings.Format("GLOBAL_SOCIAL_TOAST_SPECTATOR_INVITE_RECEIVED", (object) "5ecaf0ff", (object) textArg);
        break;
      case SocialToastMgr.TOAST_TYPE.SPECTATOR_ADDED:
        text = GameStrings.Format("GLOBAL_SOCIAL_TOAST_SPECTATOR_ADDED", (object) "5ecaf0ff", (object) textArg);
        break;
      case SocialToastMgr.TOAST_TYPE.SPECTATOR_REMOVED:
        text = GameStrings.Format("GLOBAL_SOCIAL_TOAST_SPECTATOR_REMOVED", (object) "5ecaf0ff", (object) textArg);
        break;
      default:
        text = string.Empty;
        break;
    }
    if (this.m_toastIsShown)
    {
      iTween.Stop(this.m_toast.gameObject, true);
      iTween.Stop(this.gameObject, true);
      RenderUtils.SetAlpha(this.m_toast.gameObject, 0.0f);
    }
    this.m_toast.gameObject.SetActive(true);
    this.m_toast.SetText(text);
    Hashtable args = iTween.Hash((object) "amount", (object) 1f, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) "FadeOutToast", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) displayTime, (object) "name", (object) "fade");
    iTween.StopByName(this.gameObject, "fade");
    iTween.FadeTo(this.m_toast.gameObject, args);
    this.m_toastIsShown = true;
    if (!playSound)
      return;
    SoundManager.Get().LoadAndPlay("UI_BnetToast");
  }

  private void FadeOutToast(float displayTime)
  {
    iTween.FadeTo(this.m_toast.gameObject, iTween.Hash((object) "amount", (object) 0.0f, (object) "delay", (object) displayTime, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) "DeactivateToast", (object) "oncompletetarget", (object) this.gameObject, (object) "name", (object) "fade"));
  }

  private void DeactivateToast()
  {
    this.m_toast.gameObject.SetActive(false);
    this.m_toastIsShown = false;
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    if (!DemoMgr.Get().IsSocialEnabled())
      return;
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    using (List<BnetPlayerChange>.Enumerator enumerator = changelist.GetChanges().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayerChange current = enumerator.Current;
        if (current.GetPlayer() != null && current.GetNewPlayer() != null && (current != null && current.GetPlayer().IsDisplayable()) && (current.GetPlayer() != myPlayer && BnetFriendMgr.Get().IsFriend(current.GetPlayer())))
        {
          BnetPlayer oldPlayer = current.GetOldPlayer();
          BnetPlayer newPlayer = current.GetNewPlayer();
          this.CheckForOnlineStatusChanged(oldPlayer, newPlayer);
          if (oldPlayer != null)
          {
            BnetGameAccount hearthstoneGameAccount1 = newPlayer.GetHearthstoneGameAccount();
            BnetGameAccount hearthstoneGameAccount2 = oldPlayer.GetHearthstoneGameAccount();
            if (!(hearthstoneGameAccount2 == (BnetGameAccount) null) && !(hearthstoneGameAccount1 == (BnetGameAccount) null))
            {
              this.CheckForCardOpened(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForDruidLevelChanged(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForHunterLevelChanged(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForMageLevelChanged(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForPaladinLevelChanged(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForPriestLevelChanged(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForRogueLevelChanged(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForShamanLevelChanged(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForWarlockLevelChanged(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForWarriorLevelChanged(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
              this.CheckForMissionComplete(hearthstoneGameAccount2, hearthstoneGameAccount1, newPlayer);
            }
          }
        }
      }
    }
  }

  private void OnPresenceChanged(PresenceUpdate[] updates)
  {
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    foreach (PresenceUpdate update in updates)
    {
      if (!(update.programId != (bgs.FourCC) BnetProgramId.HEARTHSTONE))
      {
        BnetPlayer player = BnetUtils.GetPlayer(BnetGameAccountId.CreateFromEntityId(update.entityId));
        if (player != null && player != myPlayer && (player.IsDisplayable() && BnetFriendMgr.Get().IsFriend(player)))
        {
          switch (update.fieldId)
          {
            case 17:
              this.CheckSessionGameStarted(player);
              continue;
            case 18:
              this.CheckForNewRank(player);
              continue;
            case 22:
              this.CheckSessionRecordChanged(player);
              continue;
            default:
              continue;
          }
        }
      }
    }
  }

  private void CheckForOnlineStatusChanged(BnetPlayer oldPlayer, BnetPlayer newPlayer)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SocialToastMgr.\u003CCheckForOnlineStatusChanged\u003Ec__AnonStorey373 changedCAnonStorey373 = new SocialToastMgr.\u003CCheckForOnlineStatusChanged\u003Ec__AnonStorey373();
    // ISSUE: reference to a compiler-generated field
    changedCAnonStorey373.newPlayer = newPlayer;
    // ISSUE: reference to a compiler-generated field
    changedCAnonStorey373.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    if (oldPlayer != null && oldPlayer.IsOnline() == changedCAnonStorey373.newPlayer.IsOnline())
      return;
    // ISSUE: reference to a compiler-generated field
    ulong lastOnlineMicrosec1 = changedCAnonStorey373.newPlayer.GetBestLastOnlineMicrosec();
    ulong lastOnlineMicrosec2 = BnetPresenceMgr.Get().GetMyPlayer().GetBestLastOnlineMicrosec();
    if ((long) lastOnlineMicrosec1 == 0L || (long) lastOnlineMicrosec2 == 0L || lastOnlineMicrosec2 > lastOnlineMicrosec1)
      return;
    SocialToastMgr.LastOnlineTracker lastOnlineTracker = (SocialToastMgr.LastOnlineTracker) null;
    float fixedTime = Time.fixedTime;
    // ISSUE: reference to a compiler-generated field
    int hashCode = changedCAnonStorey373.newPlayer.GetAccountId().GetHashCode();
    if (!this.m_lastOnlineTracker.TryGetValue(hashCode, out lastOnlineTracker))
    {
      lastOnlineTracker = new SocialToastMgr.LastOnlineTracker();
      this.m_lastOnlineTracker[hashCode] = lastOnlineTracker;
    }
    // ISSUE: reference to a compiler-generated field
    if (changedCAnonStorey373.newPlayer.IsOnline())
    {
      if (lastOnlineTracker.m_callback != null)
        ApplicationMgr.Get().CancelScheduledCallback(lastOnlineTracker.m_callback, (object) null);
      lastOnlineTracker.m_callback = (ApplicationMgr.ScheduledCallback) null;
      if ((double) fixedTime - (double) lastOnlineTracker.m_localLastOnlineTime < 5.0)
        return;
      // ISSUE: reference to a compiler-generated field
      this.AddToast(UserAttentionBlocker.NONE, changedCAnonStorey373.newPlayer.GetBestName(), SocialToastMgr.TOAST_TYPE.FRIEND_ONLINE);
    }
    else
    {
      lastOnlineTracker.m_localLastOnlineTime = fixedTime;
      // ISSUE: reference to a compiler-generated method
      lastOnlineTracker.m_callback = new ApplicationMgr.ScheduledCallback(changedCAnonStorey373.\u003C\u003Em__78);
      ApplicationMgr.Get().ScheduleCallback(5f, false, lastOnlineTracker.m_callback, (object) null);
    }
  }

  private void CheckSessionGameStarted(BnetPlayer player)
  {
    if (PresenceMgr.Get().GetStatus(player) == PresenceStatus.TAVERN_BRAWL_GAME)
    {
      if (!TavernBrawlManager.Get().IsCurrentSeasonSessionBased)
        return;
    }
    else if (PresenceMgr.Get().GetStatus(player) != PresenceStatus.ARENA_GAME)
      return;
    BnetGameAccount hearthstoneGameAccount = player.GetHearthstoneGameAccount();
    if (hearthstoneGameAccount == (BnetGameAccount) null)
      return;
    SessionRecord sessionRecord = hearthstoneGameAccount.GetSessionRecord();
    if (sessionRecord == null || sessionRecord.Wins < 8U)
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format(sessionRecord.SessionRecordType != SessionRecordType.ARENA ? "GLOBAL_SOCIAL_TOAST_FRIEND_HEROIC_BRAWL_START_WITH_MANY_WINS" : "GLOBAL_SOCIAL_TOAST_FRIEND_ARENA_START_WITH_MANY_WINS", (object) "5ecaf0ff", (object) player.GetBestName(), (object) sessionRecord.Wins));
  }

  private void CheckSessionRecordChanged(BnetPlayer player)
  {
    BnetGameAccount hearthstoneGameAccount = player.GetHearthstoneGameAccount();
    if (hearthstoneGameAccount == (BnetGameAccount) null)
      return;
    SessionRecord sessionRecord = hearthstoneGameAccount.GetSessionRecord();
    if (sessionRecord == null)
      return;
    if (sessionRecord.RunFinished)
    {
      if (sessionRecord.Wins < 3U)
        return;
      this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format(sessionRecord.SessionRecordType != SessionRecordType.ARENA ? "GLOBAL_SOCIAL_TOAST_FRIEND_HEROIC_BRAWL_COMPLETE" : "GLOBAL_SOCIAL_TOAST_FRIEND_ARENA_COMPLETE", (object) "5ecaf0ff", (object) player.GetBestName(), (object) sessionRecord.Wins, (object) sessionRecord.Losses));
    }
    else
    {
      if ((int) sessionRecord.Wins != 0 || (int) sessionRecord.Losses != 0)
        return;
      this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format(sessionRecord.SessionRecordType != SessionRecordType.ARENA ? "GLOBAL_SOCIAL_TOAST_FRIEND_HEROIC_BRAWL_START" : "GLOBAL_SOCIAL_TOAST_FRIEND_ARENA_START", (object) "5ecaf0ff", (object) player.GetBestName()));
    }
  }

  private void CheckForCardOpened(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (newPlayerAccount.GetCardsOpened() == oldPlayerAccount.GetCardsOpened())
      return;
    string cardsOpened = newPlayerAccount.GetCardsOpened();
    if (string.IsNullOrEmpty(cardsOpened))
      return;
    string[] strArray = cardsOpened.Split(',');
    if (strArray.Length != 2)
      return;
    EntityDef entityDef = DefLoader.Get().GetEntityDef(strArray[0]);
    if (entityDef == null)
      return;
    if (strArray[1] == "1")
      this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_GOLDEN_LEGENDARY", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) entityDef.GetName(), (object) "ffd200"));
    else
      this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_LEGENDARY", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) entityDef.GetName(), (object) "ff9c00"));
  }

  private void CheckForNewRank(BnetPlayer player)
  {
    MedalInfoTranslator rankPresenceField = RankMgr.Get().GetRankPresenceField(player);
    if (rankPresenceField == null)
      return;
    BnetGameAccountId hearthstoneGameAccountId = player.GetHearthstoneGameAccountId();
    int rank1 = rankPresenceField.GetCurrentMedal(false).rank;
    int rank2 = rankPresenceField.GetCurrentMedal(true).rank;
    if (!this.m_lastKnownMedals.ContainsKey(hearthstoneGameAccountId))
    {
      this.m_lastKnownMedals[hearthstoneGameAccountId] = new SocialToastMgr.LastKnownMedalTracker()
      {
        m_standardRank = rank1,
        m_wildRank = rank2
      };
    }
    else
    {
      SocialToastMgr.LastKnownMedalTracker lastKnownMedal = this.m_lastKnownMedals[hearthstoneGameAccountId];
      if (rank1 <= 10 && rank1 < lastKnownMedal.m_standardRank)
      {
        lastKnownMedal.m_standardRank = rank1;
        if (rank1 == 0)
          this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_RANK_LEGEND", (object) "5ecaf0ff", (object) player.GetBestName()));
        else
          this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_GAINED_RANK", (object) "5ecaf0ff", (object) player.GetBestName(), (object) rankPresenceField.GetCurrentMedal(false).rank));
      }
      if (rank2 > 10 || rank2 >= lastKnownMedal.m_wildRank)
        return;
      lastKnownMedal.m_wildRank = rank2;
      if (rank2 == 0)
        this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_RANK_LEGEND_WILD", (object) "5ecaf0ff", (object) player.GetBestName()));
      else
        this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_GAINED_RANK_WILD", (object) "5ecaf0ff", (object) player.GetBestName(), (object) rankPresenceField.GetCurrentMedal(true).rank));
    }
  }

  private void CheckForMissionComplete(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (newPlayerAccount.GetTutorialBeaten() == oldPlayerAccount.GetTutorialBeaten() || newPlayerAccount.GetTutorialBeaten() != 1)
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_ILLIDAN_COMPLETE", (object) "5ecaf0ff", (object) newPlayer.GetBestName()));
  }

  private void CheckForMageLevelChanged(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (!this.ShouldToastThisLevel(oldPlayerAccount.GetMageLevel(), newPlayerAccount.GetMageLevel()))
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_MAGE_LEVEL", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) newPlayerAccount.GetMageLevel()));
  }

  private void CheckForPaladinLevelChanged(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (!this.ShouldToastThisLevel(oldPlayerAccount.GetPaladinLevel(), newPlayerAccount.GetPaladinLevel()))
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_PALADIN_LEVEL", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) newPlayerAccount.GetPaladinLevel()));
  }

  private void CheckForDruidLevelChanged(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (!this.ShouldToastThisLevel(oldPlayerAccount.GetDruidLevel(), newPlayerAccount.GetDruidLevel()))
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_DRUID_LEVEL", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) newPlayerAccount.GetDruidLevel()));
  }

  private void CheckForRogueLevelChanged(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (!this.ShouldToastThisLevel(oldPlayerAccount.GetRogueLevel(), newPlayerAccount.GetRogueLevel()))
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_ROGUE_LEVEL", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) newPlayerAccount.GetRogueLevel()));
  }

  private void CheckForHunterLevelChanged(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (!this.ShouldToastThisLevel(oldPlayerAccount.GetHunterLevel(), newPlayerAccount.GetHunterLevel()))
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_HUNTER_LEVEL", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) newPlayerAccount.GetHunterLevel()));
  }

  private void CheckForShamanLevelChanged(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (!this.ShouldToastThisLevel(oldPlayerAccount.GetShamanLevel(), newPlayerAccount.GetShamanLevel()))
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_SHAMAN_LEVEL", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) newPlayerAccount.GetShamanLevel()));
  }

  private void CheckForWarriorLevelChanged(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (!this.ShouldToastThisLevel(oldPlayerAccount.GetWarriorLevel(), newPlayerAccount.GetWarriorLevel()))
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_WARRIOR_LEVEL", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) newPlayerAccount.GetWarriorLevel()));
  }

  private void CheckForWarlockLevelChanged(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (!this.ShouldToastThisLevel(oldPlayerAccount.GetWarlockLevel(), newPlayerAccount.GetWarlockLevel()))
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_WARLOCK_LEVEL", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) newPlayerAccount.GetWarlockLevel()));
  }

  private void CheckForPriestLevelChanged(BnetGameAccount oldPlayerAccount, BnetGameAccount newPlayerAccount, BnetPlayer newPlayer)
  {
    if (!this.ShouldToastThisLevel(oldPlayerAccount.GetPriestLevel(), newPlayerAccount.GetPriestLevel()))
      return;
    this.AddToast(UserAttentionBlocker.NONE, GameStrings.Format("GLOBAL_SOCIAL_TOAST_FRIEND_PRIEST_LEVEL", (object) "5ecaf0ff", (object) newPlayer.GetBestName(), (object) newPlayerAccount.GetPriestLevel()));
  }

  private bool ShouldToastThisLevel(int oldLevel, int newLevel)
  {
    return oldLevel != newLevel && (newLevel == 20 || newLevel == 30 || (newLevel == 40 || newLevel == 50) || newLevel == 60);
  }

  private void OnFriendsChanged(BnetFriendChangelist changelist, object userData)
  {
    if (!DemoMgr.Get().IsSocialEnabled())
      return;
    List<BnetInvitation> addedReceivedInvites = changelist.GetAddedReceivedInvites();
    if (addedReceivedInvites == null)
      return;
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    if (myPlayer != null && myPlayer.IsBusy())
      return;
    using (List<BnetInvitation>.Enumerator enumerator = addedReceivedInvites.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetInvitation current = enumerator.Current;
        BnetPlayer recentOpponent = FriendMgr.Get().GetRecentOpponent();
        if (recentOpponent != null && recentOpponent.HasAccount(current.GetInviterId()))
          this.AddToast(UserAttentionBlocker.NONE, GameStrings.Get("GLOBAL_SOCIAL_TOAST_RECENT_OPPONENT_FRIEND_REQUEST"));
        else
          this.AddToast(UserAttentionBlocker.NONE, current.GetInviterName(), SocialToastMgr.TOAST_TYPE.FRIEND_INVITE);
      }
    }
  }

  private void ShutdownHandler(int minutes)
  {
    this.AddToast(UserAttentionBlocker.ALL, GameStrings.Format("GLOBAL_SHUTDOWN_TOAST", (object) "f61f1fff", (object) minutes), SocialToastMgr.TOAST_TYPE.DEFAULT, 3.5f);
  }

  public enum TOAST_TYPE
  {
    DEFAULT,
    FRIEND_ONLINE,
    FRIEND_OFFLINE,
    FRIEND_INVITE,
    HEALTHY_GAMING,
    HEALTHY_GAMING_OVER_THRESHOLD,
    FRIEND_ARENA_COMPLETE,
    SPECTATOR_INVITE_SENT,
    SPECTATOR_INVITE_RECEIVED,
    SPECTATOR_ADDED,
    SPECTATOR_REMOVED,
  }

  private class LastKnownMedalTracker
  {
    public int m_standardRank;
    public int m_wildRank;
  }

  private class LastOnlineTracker
  {
    public float m_localLastOnlineTime;
    public ApplicationMgr.ScheduledCallback m_callback;
  }
}
