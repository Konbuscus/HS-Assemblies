﻿// Decompiled with JetBrains decompiler
// Type: ZoneSecret
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ZoneSecret : Zone
{
  private const float MAX_LAYOUT_PYRAMID_LEVEL = 2f;
  private const float LAYOUT_ANIM_SEC = 1f;

  private void Awake()
  {
    if (GameState.Get() == null)
      return;
    GameState.Get().RegisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
  }

  public override void UpdateLayout()
  {
    this.m_updatingLayout = true;
    if (this.IsBlockingLayout())
      this.UpdateLayoutFinished();
    else if ((bool) UniversalInputManager.UsePhoneUI)
      this.UpdateLayout_Phone();
    else
      this.UpdateLayout_Default();
  }

  private void UpdateLayout_Default()
  {
    Vector2 vector2 = new Vector2(1f, 2f);
    if (this.m_controller != null)
    {
      Card heroCard = this.m_controller.GetHeroCard();
      if ((Object) heroCard != (Object) null)
      {
        Bounds bounds = heroCard.GetActor().GetMeshRenderer().bounds;
        vector2.x = bounds.extents.x;
        vector2.y = bounds.extents.z * 0.9f;
      }
    }
    float num1 = 0.6f * vector2.y;
    int num2 = 0;
    for (int index = 0; index < this.m_cards.Count; ++index)
    {
      Card card = this.m_cards[index];
      if (this.CanAnimateCard(card))
      {
        card.ShowCard();
        Vector3 position = this.transform.position;
        float a = (float) (index + 1 >> 1);
        int num3 = index & 1;
        float num4 = (double) a <= 2.0 ? (!Mathf.Approximately(a, 1f) ? a / 2f : 0.6f) : 1f;
        if (num3 == 0)
          position.x += vector2.x * num4;
        else
          position.x -= vector2.x * num4;
        position.z -= vector2.y * (num4 * num4);
        if ((double) a > 2.0)
          position.z -= num1 * (a - 2f);
        iTween.Stop(card.gameObject);
        ZoneTransitionStyle transitionStyle = card.GetTransitionStyle();
        card.SetTransitionStyle(ZoneTransitionStyle.NORMAL);
        if (transitionStyle == ZoneTransitionStyle.INSTANT)
        {
          card.EnableTransitioningZones(false);
          card.transform.position = position;
          card.transform.rotation = this.transform.rotation;
          card.transform.localScale = this.transform.localScale;
        }
        else
        {
          card.EnableTransitioningZones(true);
          ++num2;
          iTween.MoveTo(card.gameObject, position, 1f);
          iTween.RotateTo(card.gameObject, this.transform.localEulerAngles, 1f);
          iTween.ScaleTo(card.gameObject, this.transform.localScale, 1f);
        }
      }
    }
    if (num2 > 0)
      this.StartFinishLayoutTimer(1f);
    else
      this.UpdateLayoutFinished();
  }

  private void UpdateLayout_Phone()
  {
    for (int index = 0; index < this.m_cards.Count; ++index)
    {
      Card card = this.m_cards[index];
      if (this.CanAnimateCard(card))
      {
        card.EnableTransitioningZones(false);
        iTween.Stop(card.gameObject);
        if (index == 0)
        {
          if (!card.IsShown())
          {
            Entity entity = card.GetEntity();
            card.ShowExhaustedChange(entity.IsExhausted());
            card.ShowCard();
          }
          card.GetActor().UpdateAllComponents();
        }
        card.transform.position = this.transform.position;
        card.transform.rotation = this.transform.rotation;
        card.transform.localScale = this.transform.localScale;
      }
    }
    this.UpdateLayoutFinished();
  }

  private bool CanAnimateCard(Card card)
  {
    return !card.IsDoNotSort();
  }

  private void OnGameOver(TAG_PLAYSTATE playState, object userData)
  {
    if (this.GetController().GetTag<TAG_PLAYSTATE>(GAME_TAG.PLAYSTATE) == TAG_PLAYSTATE.WON)
      return;
    for (int index = 0; index < this.m_cards.Count; ++index)
    {
      Card card = this.m_cards[index];
      if (this.CanAnimateCard(card))
        card.HideCard();
    }
  }
}
