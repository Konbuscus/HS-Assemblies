﻿// Decompiled with JetBrains decompiler
// Type: DeckTrayDeckTileVisual
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DeckTrayDeckTileVisual : PegUIElement
{
  public static readonly GameLayer LAYER = GameLayer.CardRaycast;
  private readonly Vector3 BOX_COLLIDER_SIZE = new Vector3(25.34f, 2.14f, 3.68f);
  private readonly Vector3 BOX_COLLIDER_CENTER = new Vector3(-1.4f, 0.0f, 0.0f);
  protected const int DEFAULT_PORTRAIT_QUALITY = 1;
  protected CollectionDeck m_deck;
  protected CollectionDeckSlot m_slot;
  protected BoxCollider m_collider;
  protected CollectionDeckTileActor m_actor;
  protected bool m_isInUse;
  protected bool m_useSliderAnimations;
  protected bool m_inArena;

  protected override void Awake()
  {
    base.Awake();
    string name = !(bool) UniversalInputManager.UsePhoneUI ? "DeckCardBar" : "DeckCardBar_phone";
    GameObject gameObject = AssetLoader.Get().LoadActor(name, false, false);
    if ((Object) gameObject == (Object) null)
    {
      Debug.LogWarning((object) string.Format("DeckTrayDeckTileVisual.OnDeckTileActorLoaded() - FAILED to load actor \"{0}\"", (object) name));
    }
    else
    {
      this.m_actor = gameObject.GetComponent<CollectionDeckTileActor>();
      if ((Object) this.m_actor == (Object) null)
      {
        Debug.LogWarning((object) string.Format("DeckTrayDeckTileVisual.OnDeckTileActorLoaded() - ERROR game object \"{0}\" has no CollectionDeckTileActor component", (object) name));
      }
      else
      {
        GameUtils.SetParent((Component) this.m_actor, (Component) this, false);
        this.m_actor.transform.localEulerAngles = new Vector3(0.0f, 180f, 0.0f);
        UIBScrollableItem component = this.m_actor.GetComponent<UIBScrollableItem>();
        if ((Object) component != (Object) null)
          component.SetCustomActiveState(new UIBScrollableItem.ActiveStateCallback(this.IsInUse));
        this.SetUpActor();
        if ((Object) this.gameObject.GetComponent<BoxCollider>() == (Object) null)
        {
          this.m_collider = this.gameObject.AddComponent<BoxCollider>();
          this.m_collider.size = this.BOX_COLLIDER_SIZE;
          this.m_collider.center = this.BOX_COLLIDER_CENTER;
        }
        this.Hide();
        SceneUtils.SetLayer(this.gameObject, DeckTrayDeckTileVisual.LAYER);
        this.SetDragTolerance(5f);
      }
    }
  }

  public string GetCardID()
  {
    return this.m_actor.GetEntityDef().GetCardId();
  }

  public TAG_PREMIUM GetPremium()
  {
    return this.m_actor.GetPremium();
  }

  public CollectionDeckSlot GetSlot()
  {
    return this.m_slot;
  }

  public void SetSlot(CollectionDeck deck, CollectionDeckSlot s, bool useSliderAnimations)
  {
    this.m_deck = deck;
    this.m_slot = s;
    this.m_useSliderAnimations = useSliderAnimations;
    this.SetUpActor();
  }

  public CollectionDeckTileActor GetActor()
  {
    return this.m_actor;
  }

  public Bounds GetBounds()
  {
    return this.m_collider.bounds;
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void MarkAsUsed()
  {
    this.m_isInUse = true;
  }

  public void MarkAsUnused()
  {
    this.m_isInUse = false;
    if ((Object) this.m_actor == (Object) null)
      return;
    this.m_actor.UpdateDeckCardProperties(false, 1, false);
  }

  public bool IsInUse()
  {
    return this.m_isInUse;
  }

  public void SetInArena(bool inArena)
  {
    this.m_inArena = inArena;
  }

  public void SetHighlight(bool highlight)
  {
    if ((Object) this.m_actor.m_highlight != (Object) null)
      this.m_actor.m_highlight.SetActive(highlight);
    if (!((Object) this.m_actor.m_highlightGlow != (Object) null))
      return;
    if (this.GetGhostedState() == CollectionDeckTileActor.GhostedState.RED)
      this.m_actor.m_highlightGlow.SetActive(highlight);
    else
      this.m_actor.m_highlightGlow.SetActive(false);
  }

  public void UpdateGhostedState()
  {
    this.m_actor.SetGhosted(this.GetGhostedState());
    this.m_actor.UpdateGhostTileEffect();
  }

  private CollectionDeckTileActor.GhostedState GetGhostedState()
  {
    if (this.m_deck.ShouldSplitSlotsByValidity())
    {
      if (!CollectionManager.Get().GetEditedDeck().SlotMatchesFormat(this.m_slot))
        return CollectionDeckTileActor.GhostedState.RED;
      if (!this.m_slot.Owned)
        return CollectionDeckTileActor.GhostedState.BLUE;
    }
    return CollectionDeckTileActor.GhostedState.NONE;
  }

  private void SetUpActor()
  {
    if ((Object) this.m_actor == (Object) null || this.m_slot == null || string.IsNullOrEmpty(this.m_slot.CardID))
      return;
    EntityDef entityDef1 = this.m_actor.GetEntityDef();
    if (entityDef1 != null && (Object) this.m_actor.GetCardDef() != (Object) null)
      DefLoader.Get().ClearCardDef(entityDef1.GetCardId());
    EntityDef entityDef2 = DefLoader.Get().GetEntityDef(this.m_slot.CardID);
    this.m_actor.SetDisablePremiumPortrait(true);
    this.m_actor.SetEntityDef(entityDef2);
    this.m_actor.SetPremium(this.m_slot.Premium);
    this.m_actor.SetGhosted(this.GetGhostedState());
    bool cardIsUnique = entityDef2 != null && entityDef2.IsElite();
    if (cardIsUnique && this.m_inArena && this.m_slot.Count > 1)
      cardIsUnique = false;
    this.m_actor.UpdateDeckCardProperties(cardIsUnique, this.m_slot.Count, this.m_useSliderAnimations);
    DefLoader.Get().LoadCardDef(entityDef2.GetCardId(), (DefLoader.LoadDefCallback<CardDef>) ((cardID, cardDef, data) =>
    {
      if ((Object) this.m_actor == (Object) null || !cardID.Equals(this.m_actor.GetEntityDef().GetCardId()))
        return;
      this.m_actor.SetCardDef(cardDef);
      this.m_actor.UpdateAllComponents();
      this.m_actor.UpdateMaterial(cardDef.GetDeckCardBarPortrait());
      this.m_actor.UpdateGhostTileEffect();
    }), (object) null, new CardPortraitQuality(1, this.m_slot.Premium));
  }
}
