﻿// Decompiled with JetBrains decompiler
// Type: BoosterDbId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum BoosterDbId
{
  INVALID = 0,
  CLASSIC = 1,
  GOBLINS_VS_GNOMES = 9,
  THE_GRAND_TOURNAMENT = 10,
  OLD_GODS = 11,
  FIRST_PURCHASE = 17,
  MEAN_STREETS = 19,
}
