﻿// Decompiled with JetBrains decompiler
// Type: BoardDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BoardDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private string m_Prefab;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("PREFAB", "")]
  public string Prefab
  {
    get
    {
      return this.m_Prefab;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    BoardDbfAsset boardDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (BoardDbfAsset)) as BoardDbfAsset;
    if ((UnityEngine.Object) boardDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("BoardDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < boardDbfAsset.Records.Count; ++index)
      boardDbfAsset.Records[index].StripUnusedLocales();
    records = (object) boardDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetPrefab(string v)
  {
    this.m_Prefab = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (BoardDbfRecord.\u003C\u003Ef__switch\u0024map17 == null)
      {
        // ISSUE: reference to a compiler-generated field
        BoardDbfRecord.\u003C\u003Ef__switch\u0024map17 = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "PREFAB",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (BoardDbfRecord.\u003C\u003Ef__switch\u0024map17.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Prefab;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (BoardDbfRecord.\u003C\u003Ef__switch\u0024map18 == null)
    {
      // ISSUE: reference to a compiler-generated field
      BoardDbfRecord.\u003C\u003Ef__switch\u0024map18 = new Dictionary<string, int>(3)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "PREFAB",
          2
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!BoardDbfRecord.\u003C\u003Ef__switch\u0024map18.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetPrefab((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (BoardDbfRecord.\u003C\u003Ef__switch\u0024map19 == null)
      {
        // ISSUE: reference to a compiler-generated field
        BoardDbfRecord.\u003C\u003Ef__switch\u0024map19 = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "PREFAB",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (BoardDbfRecord.\u003C\u003Ef__switch\u0024map19.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
