﻿// Decompiled with JetBrains decompiler
// Type: ConnectionIndicator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ConnectionIndicator : MonoBehaviour
{
  private const float LATENCY_TOLERANCE = 3f;
  public GameObject m_indicator;
  private static ConnectionIndicator s_instance;
  private bool m_active;

  private void Awake()
  {
    ConnectionIndicator.s_instance = this;
    this.m_active = false;
    this.m_indicator.SetActive(false);
  }

  private void OnDestroy()
  {
    ConnectionIndicator.s_instance = (ConnectionIndicator) null;
  }

  public static ConnectionIndicator Get()
  {
    return ConnectionIndicator.s_instance;
  }

  private void SetIndicator(bool val)
  {
    if (val == this.m_active)
      return;
    this.m_active = val;
    this.m_indicator.SetActive(val);
    BnetBar.Get().UpdateLayout();
  }

  public bool IsVisible()
  {
    return this.m_active;
  }

  private void Update()
  {
    this.SetIndicator(Network.TimeSinceLastPong() > 3.0);
  }
}
