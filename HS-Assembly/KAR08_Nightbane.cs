﻿// Decompiled with JetBrains decompiler
// Type: KAR08_Nightbane
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR08_Nightbane : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_NightbaneTurn1_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_NightbaneUnstablePortal_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_NightbaneCorruption_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_NightbaneWin_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_NightbaneTurn3_01");
    this.PreloadSound("VO_Moroes_Male_Human_NightbaneTurn3_01");
    this.PreloadSound("VO_Moroes_Male_Human_NightbaneTurn7_01");
    this.PreloadSound("VO_Nightbane_Roar");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Nightbane_Roar",
            m_stringTag = "VO_Nightbane_Roar"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR08_Nightbane.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1A9() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR08_Nightbane.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator1AA() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR08_Nightbane.\u003CHandleGameOverWithTiming\u003Ec__Iterator1AB() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
