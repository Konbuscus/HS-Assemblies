﻿// Decompiled with JetBrains decompiler
// Type: AssetLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using UnityEngine;

public class AssetLoader : MonoBehaviour
{
  public static readonly bool DOWNLOADABLE_LANGUAGE_PACKS = false;
  private static HashSet<string> fileListInExtraBundles_ = new HashSet<string>();
  private readonly Vector3 SPAWN_POS_CAMERA_OFFSET = new Vector3(0.0f, 0.0f, -5000f);
  private List<GameObject> m_waitingOnObjects = new List<GameObject>();
  private const float FILE_CACHE_MAX_TIME_PER_FRAME = 0.2f;
  private const string s_fileInfoCachePath = "filecache.txt";
  public const string fileListInExtraBundlesTxtName_ = "manifest-filelist-extra.csv";
  private static AssetLoader s_instance;
  private bool m_ready;
  private static string s_workingDir;
  private AssetBundle[,] m_familyBundles;
  private AssetBundle[,] m_localizedFamilyBundles;
  private AssetBundle[,] m_downloadedLocalizedFamilyBundles;
  private AssetBundle m_sharedBundle;
  private static Map<AssetFamily, Map<string, List<LightFileInfo>>> s_fileInfos;
  private static string[] s_cardSetDirectories;

  private void Awake()
  {
    AssetLoader.s_instance = this;
    AssetCache.Initialize();
    int length1 = Enum.GetNames(typeof (AssetFamily)).Length;
    int num1 = 0;
    int num2 = 0;
    using (Map<AssetFamily, AssetFamilyBundleInfo>.ValueCollection.Enumerator enumerator = AssetBundleInfo.FamilyInfo.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AssetFamilyBundleInfo current = enumerator.Current;
        if (current.NumberOfBundles > num1)
          num1 = current.NumberOfBundles;
        if (current.NumberOfLocaleBundles > num2)
          num2 = current.NumberOfLocaleBundles;
      }
    }
    this.m_familyBundles = new AssetBundle[length1, num1 + AssetLoader.AvailableExtraAssetBundlesCount()];
    this.m_localizedFamilyBundles = new AssetBundle[length1, num2 + AssetLoader.AvailableExtraAssetBundlesCount()];
    int length2 = 0;
    using (Map<AssetFamily, AssetFamilyBundleInfo>.ValueCollection.Enumerator enumerator = AssetBundleInfo.FamilyInfo.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AssetFamilyBundleInfo current = enumerator.Current;
        if (current.NumberOfDownloadableLocaleBundles > length2)
          length2 = current.NumberOfDownloadableLocaleBundles;
      }
    }
    this.m_downloadedLocalizedFamilyBundles = new AssetBundle[length1, length2];
  }

  private void OnDestroy()
  {
    ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
    AssetLoader.s_instance = (AssetLoader) null;
  }

  private void WillReset()
  {
    AssetBundle[,] localizedFamilyBundles1 = this.m_localizedFamilyBundles;
    int length1 = localizedFamilyBundles1.GetLength(0);
    int length2 = localizedFamilyBundles1.GetLength(1);
    for (int index1 = 0; index1 < length1; ++index1)
    {
      for (int index2 = 0; index2 < length2; ++index2)
      {
        AssetBundle assetBundle = localizedFamilyBundles1[index1, index2];
        if ((UnityEngine.Object) assetBundle != (UnityEngine.Object) null)
          assetBundle.Unload(true);
      }
    }
    this.m_localizedFamilyBundles = new AssetBundle[this.m_localizedFamilyBundles.GetLength(0), this.m_localizedFamilyBundles.GetLength(1)];
    AssetBundle[,] localizedFamilyBundles2 = this.m_downloadedLocalizedFamilyBundles;
    int length3 = localizedFamilyBundles2.GetLength(0);
    int length4 = localizedFamilyBundles2.GetLength(1);
    for (int index1 = 0; index1 < length3; ++index1)
    {
      for (int index2 = 0; index2 < length4; ++index2)
      {
        AssetBundle assetBundle = localizedFamilyBundles2[index1, index2];
        if ((UnityEngine.Object) assetBundle != (UnityEngine.Object) null)
          assetBundle.Unload(true);
      }
    }
    this.m_downloadedLocalizedFamilyBundles = new AssetBundle[this.m_downloadedLocalizedFamilyBundles.GetLength(0), this.m_downloadedLocalizedFamilyBundles.GetLength(1)];
    AssetCache.ClearLocalizedAssets();
    this.PreloadBundles();
  }

  private void Start()
  {
    this.StartCoroutine(this.Init());
  }

  [DebuggerHidden]
  private IEnumerator Init()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AssetLoader.\u003CInit\u003Ec__Iterator2DA() { \u003C\u003Ef__this = this };
  }

  private void OnApplicationQuit()
  {
    AssetCache.ForceClearAllCaches();
  }

  public static AssetLoader Get()
  {
    return AssetLoader.s_instance;
  }

  public bool IsReady()
  {
    return this.m_ready;
  }

  public void SetReady(bool ready)
  {
    this.m_ready = ready;
  }

  public bool IsWaitingOnObject(GameObject go)
  {
    return this.m_waitingOnObjects.Contains(go);
  }

  public bool LoadFile(string path, AssetLoader.FileCallback callback, object callbackData)
  {
    if (string.IsNullOrEmpty(path))
    {
      UnityEngine.Debug.LogWarning((object) "AssetLoader.LoadFile() - path was null or empty");
      return false;
    }
    this.StartCoroutine(this.DownloadFile(path, callback, callbackData));
    return true;
  }

  public bool LoadActor(string cardName, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    return this.LoadPrefab(cardName, AssetFamily.Actor, false, callback, callbackData, persistent, (UnityEngine.Object) null);
  }

  public bool LoadActor(string cardName, bool usePrefabPosition, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    return this.LoadPrefab(cardName, AssetFamily.Actor, usePrefabPosition, callback, callbackData, persistent, (UnityEngine.Object) null);
  }

  public GameObject LoadActor(string name, bool usePrefabPosition = false, bool persistent = false)
  {
    if (name != null)
      return this.LoadGameObjectImmediately(new Asset(name, AssetFamily.Actor, persistent), usePrefabPosition, (GameObject) null);
    Error.AddDevFatal("AssetLoader.LoadActor() - An asset request was made but no file name was given.");
    return (GameObject) null;
  }

  public Map<string, EntityDef> LoadBatchCardXmls(List<string> cardIds, out int errors)
  {
    string outPath;
    if (ApplicationMgr.TryGetStandaloneLocalDataPath(string.Empty, out outPath))
      return this.LoadBatchCardXmlsFromLocalData(outPath, cardIds, out errors);
    return this.LoadBatchCardXmlsFromBundle(cardIds, out errors);
  }

  private Map<string, EntityDef> LoadBatchCardXmlsFromBundle(List<string> cardIds, out int errors)
  {
    errors = 0;
    Map<string, EntityDef> map = new Map<string, EntityDef>();
    string name = Locale.enUS.ToString();
    AssetBundle bundleForAsset = this.GetBundleForAsset(new Asset(name, AssetFamily.CardXML, true));
    if ((UnityEngine.Object) bundleForAsset == (UnityEngine.Object) null)
    {
      Error.AddDevFatal("AssetLoader.LoadCardXml: Could not load CardXml bundle");
      return (Map<string, EntityDef>) null;
    }
    UnityEngine.Object @object = bundleForAsset.LoadAsset(name);
    if (@object == (UnityEngine.Object) null)
    {
      Error.AddDevFatal("AssetLoader.LoadCardXml: Could not load CardXml for locale " + (object) Localization.GetLocale());
      return (Map<string, EntityDef>) null;
    }
    using (StringReader stringReader = new StringReader(((TextAsset) @object).text))
    {
      using (XmlReader reader = XmlReader.Create((TextReader) stringReader))
      {
        while (true)
        {
          EntityDef entityDef = new EntityDef();
          if (entityDef.LoadDataFromCardXml(reader))
          {
            string cardId = entityDef.GetCardId();
            if (map.ContainsKey(cardId))
              UnityEngine.Debug.LogError((object) string.Format("AssetLoader.LoadBatchedCardXmls: Loaded duplicate card id {0}", (object) cardId));
            else
              map.Add(cardId, entityDef);
          }
          else
            break;
        }
      }
    }
    return map;
  }

  private Map<string, EntityDef> LoadBatchCardXmlsFromLocalData(string localDataPath, List<string> cardIds, out int errors)
  {
    errors = 0;
    if (AssetLoader.s_cardSetDirectories == null)
      this.InitCardDirectories(localDataPath);
    Map<string, EntityDef> map = new Map<string, EntityDef>();
    using (List<string>.Enumerator enumerator = cardIds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (map.ContainsKey(current))
        {
          UnityEngine.Debug.LogError((object) string.Format("AssetLoader.LoadBatchCardXmlsFromLocalData: Loaded duplicate card id {0}", (object) current));
        }
        else
        {
          string xmlText = string.Empty;
          foreach (object cardSetDirectory in AssetLoader.s_cardSetDirectories)
          {
            string path1 = string.Format("{0}/{1}/{1}.xml", cardSetDirectory, (object) current);
            if (File.Exists(path1))
            {
              xmlText = File.ReadAllText(path1);
              break;
            }
            string sourceDir = AssetPathInfo.FamilyInfo[AssetFamily.CardXML].sourceDir;
            string path2 = string.Format("{0}/{1}/{2}/{2}.xml", (object) localDataPath, (object) sourceDir, (object) current);
            if (File.Exists(path2))
            {
              xmlText = File.ReadAllText(path2);
              break;
            }
          }
          if (string.IsNullOrEmpty(xmlText))
          {
            errors = errors + 1;
            UnityEngine.Debug.LogWarningFormat("AssetLoader.LoadBatchCardXmlsFromLocalData: Failed to load {0}.xml. Loading PlaceholderCard.xml instead.", (object) current);
            string sourceDir = AssetPathInfo.FamilyInfo[AssetFamily.CardXML].sourceDir;
            string path = string.Format("{0}/{1}/{2}/{2}.xml", (object) localDataPath, (object) sourceDir, (object) "PlaceholderCard");
            if (!File.Exists(path))
            {
              UnityEngine.Debug.LogErrorFormat("Failed to load PlaceholderCard.xml for {0}.", (object) current);
              break;
            }
            xmlText = File.ReadAllText(path);
          }
          map.Add(current, EntityDef.LoadFromString(current, xmlText, true));
        }
      }
    }
    return map;
  }

  public bool LoadCardPrefab(string cardName, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    return this.LoadPrefab(cardName, AssetFamily.CardPrefab, true, callback, callbackData, persistent, (UnityEngine.Object) null);
  }

  public GameObject LoadCardPrefab(string cardName, bool usePrefabPosition = true, bool persistent = false)
  {
    if (cardName != null)
      return this.LoadGameObjectImmediately(new Asset(cardName, AssetFamily.CardPrefab, persistent), usePrefabPosition, (GameObject) null);
    Error.AddDevFatal("AssetLoader.LoadCardPrefab() - An asset request was made but no file name was given.");
    return (GameObject) null;
  }

  public Texture LoadCardTexture(string name, bool persistent = false)
  {
    Texture texture = (Texture) null;
    if (name == null)
    {
      if (!ApplicationMgr.UseDevWorkarounds())
        return (Texture) null;
    }
    else
    {
      Locale[] loadOrder = Localization.GetLoadOrder(AssetFamily.CardTexture);
      for (int index = 0; index < loadOrder.Length; ++index)
      {
        string name1 = name;
        if (loadOrder[index] != Locale.enUS)
        {
          int length = name.LastIndexOf('/');
          int num = name.LastIndexOf("/LowResPortrait");
          if (num != -1)
            length = num;
          name1 = string.Format("{0}/{1}/{2}", (object) name.Substring(0, length), (object) loadOrder[index].ToString(), (object) name.Substring(length + 1));
        }
        UnityEngine.Object @object = this.LoadObjectImmediately(new Asset(name1, AssetFamily.CardTexture, persistent));
        if (@object != (UnityEngine.Object) null && @object is Texture)
        {
          texture = (Texture) @object;
          break;
        }
      }
      if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
      {
        UnityEngine.Object @object = this.LoadObjectImmediately(new Asset(name, AssetFamily.CardTexture, persistent));
        if (@object != (UnityEngine.Object) null && @object is Texture)
          texture = (Texture) @object;
      }
    }
    if ((UnityEngine.Object) texture == (UnityEngine.Object) null && ApplicationMgr.UseDevWorkarounds())
    {
      Log.Asset.PrintError("AssetLoader.LoadCardTexture() - Expected a Texture and loaded null or something else for asset {0}. Using a temp texture.", (object) name);
      Texture2D texture2D = new Texture2D(1, 1);
      texture2D.SetPixel(0, 0, Color.magenta);
      texture2D.Apply();
      texture = (Texture) texture2D;
    }
    return texture;
  }

  public Material LoadPremiumMaterial(string name, bool persistent = false)
  {
    if (name == null)
      return (Material) null;
    UnityEngine.Object @object = this.LoadObjectImmediately(new Asset(name, AssetFamily.CardPremium, persistent));
    if (@object == (UnityEngine.Object) null || !(@object is Material))
      return (Material) null;
    return (Material) @object;
  }

  public bool LoadBoard(string boardName, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    return this.LoadPrefab(boardName, AssetFamily.Board, true, callback, callbackData, persistent, (UnityEngine.Object) null);
  }

  public GameObject LoadBoard(string name, bool usePrefabPosition = true, bool persistent = false)
  {
    if (name != null)
      return this.LoadGameObjectImmediately(new Asset(name, AssetFamily.Board, persistent), usePrefabPosition, (GameObject) null);
    Error.AddDevFatal("AssetLoader.LoadBoard() - An asset request was made but no file name was given.");
    return (GameObject) null;
  }

  public bool LoadSound(string soundName, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false, GameObject fallback = null)
  {
    AssetLoader.LoadSoundCallbackData soundCallbackData = new AssetLoader.LoadSoundCallbackData() { callback = callback, callbackData = callbackData };
    return this.LoadPrefab(soundName, AssetFamily.Sound, true, new AssetLoader.GameObjectCallback(this.LoadSoundCallback), (object) soundCallbackData, persistent, (UnityEngine.Object) fallback);
  }

  private void LoadSoundCallback(string name, GameObject go, object callbackData)
  {
    AssetLoader.LocalizeSoundPrefab(go, true);
    AssetLoader.LoadSoundCallbackData soundCallbackData = (AssetLoader.LoadSoundCallbackData) callbackData;
    soundCallbackData.callback(name, go, soundCallbackData.callbackData);
  }

  private static bool LocalizeSoundPrefab(GameObject go, bool logLocalizationError = true)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return false;
    AudioSource component = go.GetComponent<AudioSource>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogErrorFormat("LocalizeSoundPrefab: trying to load sound prefab with no AudioSource components: \"{0}\"", (object) go.name);
      return false;
    }
    AudioClip clip = component.clip;
    if ((UnityEngine.Object) clip == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogErrorFormat("LocalizeSoundPrefab: trying to load sound prefab with an AudioSource that contains no AudoClip: \"{0}\"", (object) go.name);
      return false;
    }
    AudioClip audioClip = AssetLoader.Get().LoadAudioClip(clip.name, false) as AudioClip;
    if ((UnityEngine.Object) audioClip == (UnityEngine.Object) null)
    {
      if (logLocalizationError)
        UnityEngine.Debug.LogErrorFormat("LocalizeSoundPrefab: failed to load localized audio clip for sound prefab \"{0}\" with clip \"{1}\"", new object[2]
        {
          (object) go.name,
          (object) clip.name
        });
      return false;
    }
    go.GetComponent<AudioSource>().clip = audioClip;
    return true;
  }

  public GameObject LoadSound(string name, bool usePrefabPosition = true, bool persistent = false, bool logLocalizationError = true)
  {
    if (name == null)
    {
      Error.AddDevFatal("AssetLoader.LoadSound() - An asset request was made but no file name was given.");
      return (GameObject) null;
    }
    name = FileUtils.GameToSourceAssetPath(name, ".prefab");
    GameObject go = this.LoadGameObjectImmediately(new Asset(name, AssetFamily.Sound, persistent), usePrefabPosition, (GameObject) null);
    if (AssetLoader.LocalizeSoundPrefab(go, logLocalizationError))
      return go;
    UnityEngine.Object.Destroy((UnityEngine.Object) go);
    return (GameObject) null;
  }

  public UnityEngine.Object LoadAudioClip(string name, bool persistent = false)
  {
    if (name != null)
      return this.LoadObjectImmediately(new Asset(name, AssetFamily.AudioClip, persistent));
    Error.AddDevFatal("AssetLoader.LoadAudioClip() - An asset request was made but no file name was given.");
    return (UnityEngine.Object) null;
  }

  public bool LoadTexture(string textureName, AssetLoader.ObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    if (!Path.HasExtension(textureName))
      textureName = Path.GetFileNameWithoutExtension(textureName);
    return this.LoadObject(textureName, AssetFamily.Texture, callback, callbackData, persistent);
  }

  public Texture LoadTexture(string name, bool persistent = false)
  {
    if (name == null)
    {
      Error.AddDevFatal("AssetLoader.LoadTexture() - An asset request was made but no file name was given.");
      return (Texture) null;
    }
    UnityEngine.Object @object = this.LoadObjectImmediately(new Asset(name, AssetFamily.Texture, persistent));
    if (!(@object == (UnityEngine.Object) null) && @object is Texture)
      return (Texture) @object;
    Error.AddDevFatal("AssetLoader.LoadTexture() - Expected a Texture and loaded null or something else for asset {0}.", (object) name);
    return (Texture) null;
  }

  public bool LoadUIScreen(string screenName, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    return this.LoadPrefab(screenName, AssetFamily.Screen, true, callback, callbackData, false, (UnityEngine.Object) null);
  }

  public GameObject LoadUIScreen(string name, bool usePrefabPosition = true, bool persistent = false)
  {
    if (name != null)
      return this.LoadGameObjectImmediately(new Asset(name, AssetFamily.Screen, persistent), usePrefabPosition, (GameObject) null);
    Error.AddDevFatal("AssetLoader.LoadUIScreen() - An asset request was made but no file name was given.");
    return (GameObject) null;
  }

  public bool LoadSpell(string name, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    name = FileUtils.GameToSourceAssetPath(name, ".prefab");
    return this.LoadPrefab(name, AssetFamily.Spell, true, callback, callbackData, persistent, (UnityEngine.Object) null);
  }

  public GameObject LoadSpell(string name, bool usePrefabPosition = true, bool persistent = false)
  {
    if (name == null)
    {
      Error.AddDevFatal("AssetLoader.LoadSpell() - An asset request was made but no file name was given.");
      return (GameObject) null;
    }
    name = FileUtils.GameToSourceAssetPath(name, ".prefab");
    return this.LoadGameObjectImmediately(new Asset(name, AssetFamily.Spell, persistent), usePrefabPosition, (GameObject) null);
  }

  public bool LoadGameObject(string name, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    return this.LoadPrefab(name, AssetFamily.GameObject, true, callback, callbackData, persistent, (UnityEngine.Object) null);
  }

  public GameObject LoadGameObject(string name, bool usePrefabPosition = true, bool persistent = false)
  {
    return this.LoadGameObjectImmediately(new Asset(name, AssetFamily.GameObject, persistent), usePrefabPosition, (GameObject) null);
  }

  public bool LoadCardBack(string name, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    return this.LoadPrefab(name, AssetFamily.CardBack, true, callback, callbackData, persistent, (UnityEngine.Object) null);
  }

  public GameObject LoadCardBack(string name, bool usePrefabPosition = true, bool persistent = false)
  {
    if (name != null)
      return this.LoadGameObjectImmediately(new Asset(name, AssetFamily.CardBack, persistent), usePrefabPosition, (GameObject) null);
    Error.AddDevFatal("AssetLoader.LoadCardBack() - An asset request was made but no file name was given.");
    return (GameObject) null;
  }

  public bool LoadMovie(string name, AssetLoader.ObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    return this.LoadObject(name, AssetFamily.Movie, callback, callbackData, persistent);
  }

  public MovieTexture LoadMovie(string name, bool persistent = false)
  {
    if (name == null)
    {
      Error.AddDevFatal("AssetLoader.LoadMovie() - An asset request was made but no file name was given.");
      return (MovieTexture) null;
    }
    UnityEngine.Object @object = this.LoadObjectImmediately(new Asset(name, AssetFamily.Movie, persistent));
    if (!(@object == (UnityEngine.Object) null) && @object is MovieTexture)
      return (MovieTexture) @object;
    Error.AddDevFatal("AssetLoader.LoadMovie() - Expected a MovieTexture and loaded null or something else for asset {0}.", (object) name);
    return (MovieTexture) null;
  }

  public bool LoadFontDef(string name, AssetLoader.GameObjectCallback callback, object callbackData = null, bool persistent = false)
  {
    return this.LoadPrefab(name, AssetFamily.FontDef, true, callback, callbackData, persistent, (UnityEngine.Object) null);
  }

  public GameObject LoadFontDef(string name, bool usePrefabPosition = true, bool persistent = false)
  {
    if (name != null)
      return this.LoadGameObjectImmediately(new Asset(name, AssetFamily.FontDef, persistent), usePrefabPosition, (GameObject) null);
    Error.AddDevFatal("AssetLoader.LoadFontDef() - An asset request was made but no file name was given.");
    return (GameObject) null;
  }

  public void UnloadUpdatableBundles()
  {
    using (Map<AssetFamily, AssetFamilyBundleInfo>.Enumerator enumerator = AssetBundleInfo.FamilyInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<AssetFamily, AssetFamilyBundleInfo> current = enumerator.Current;
        AssetFamily key = current.Key;
        AssetFamilyBundleInfo familyBundleInfo = current.Value;
        if (familyBundleInfo.Updatable)
        {
          for (int bundleNum = 0; bundleNum < familyBundleInfo.NumberOfBundles; ++bundleNum)
          {
            AssetBundle bundleForFamily = this.GetBundleForFamily(key, bundleNum, new Locale?(), (string) null);
            if ((bool) ((UnityEngine.Object) bundleForFamily))
              bundleForFamily.Unload(true);
          }
          for (int bundleNum = 0; bundleNum < familyBundleInfo.NumberOfLocaleBundles; ++bundleNum)
          {
            foreach (Locale locale in Localization.GetLoadOrder(false))
            {
              AssetBundle bundleForFamily = this.GetBundleForFamily(key, bundleNum, new Locale?(locale), (string) null);
              if ((bool) ((UnityEngine.Object) bundleForFamily))
                bundleForFamily.Unload(true);
            }
          }
        }
      }
    }
  }

  public void ReloadUpdatableBundles()
  {
    using (Map<AssetFamily, AssetFamilyBundleInfo>.Enumerator enumerator = AssetBundleInfo.FamilyInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<AssetFamily, AssetFamilyBundleInfo> current = enumerator.Current;
        AssetFamily key = current.Key;
        AssetFamilyBundleInfo familyBundleInfo = current.Value;
        if (familyBundleInfo.Updatable)
        {
          for (int bundleNum = 0; bundleNum < familyBundleInfo.NumberOfBundles; ++bundleNum)
            this.GetBundleForFamily(key, bundleNum, new Locale?(), (string) null);
          for (int bundleNum = 0; bundleNum < familyBundleInfo.NumberOfLocaleBundles; ++bundleNum)
          {
            foreach (Locale locale in Localization.GetLoadOrder(false))
              this.GetBundleForFamily(key, bundleNum, new Locale?(locale), (string) null);
          }
        }
      }
    }
  }

  private bool LoadObject(string assetName, AssetFamily family, AssetLoader.ObjectCallback callback, object callbackData, bool persistent = false)
  {
    if (string.IsNullOrEmpty(assetName))
    {
      Log.Asset.Print("AssetLoader.LoadObject() - name was null or empty");
      return false;
    }
    this.LoadCachedObject(new Asset(assetName, family, persistent), callback, callbackData);
    return true;
  }

  private bool LoadPrefab(string assetName, AssetFamily family, bool usePrefabPosition, AssetLoader.GameObjectCallback callback, object callbackData, bool persistent = false, UnityEngine.Object fallback = null)
  {
    if (string.IsNullOrEmpty(assetName))
    {
      UnityEngine.Debug.LogWarning((object) "AssetLoader.LoadPrefab() - name was null or empty");
      return false;
    }
    this.LoadCachedPrefab(new Asset(assetName, family, persistent), usePrefabPosition, callback, callbackData, fallback);
    return true;
  }

  [DebuggerHidden]
  private IEnumerator DownloadFile(string path, AssetLoader.FileCallback callback, object callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AssetLoader.\u003CDownloadFile\u003Ec__Iterator2DB() { path = path, callback = callback, callbackData = callbackData, \u003C\u0024\u003Epath = path, \u003C\u0024\u003Ecallback = callback, \u003C\u0024\u003EcallbackData = callbackData, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private static IEnumerator WaitForDownload(WWW file)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AssetLoader.\u003CWaitForDownload\u003Ec__Iterator2DC() { file = file, \u003C\u0024\u003Efile = file };
  }

  private WWW CreateLocalFile(string absPath)
  {
    return new WWW(string.Format("file://{0}", (object) absPath));
  }

  public static string CreateLocalFilePath(string relPath)
  {
    if (AssetLoader.s_workingDir == null)
      AssetLoader.s_workingDir = Directory.GetCurrentDirectory().Replace("\\", "/");
    return string.Format("{0}/{1}", (object) AssetLoader.s_workingDir, (object) relPath);
  }

  private void LoadCachedObject(Asset asset, AssetLoader.ObjectCallback callback, object callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AssetLoader.\u003CLoadCachedObject\u003Ec__AnonStorey41C objectCAnonStorey41C = new AssetLoader.\u003CLoadCachedObject\u003Ec__AnonStorey41C();
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey41C.asset = asset;
    // ISSUE: reference to a compiler-generated field
    if (objectCAnonStorey41C.asset.GetName() == null)
    {
      if (callback == null)
        return;
      callback((string) null, (UnityEngine.Object) null, callbackData);
    }
    else
    {
      long timestamp = TimeUtils.BinaryStamp();
      // ISSUE: reference to a compiler-generated field
      AssetCache.CachedAsset cachedAsset = AssetCache.Find(objectCAnonStorey41C.asset);
      if (cachedAsset != null)
      {
        cachedAsset.SetLastRequestTimestamp(timestamp);
        if (callback == null)
          return;
        UnityEngine.Object assetObject = cachedAsset.GetAssetObject();
        // ISSUE: reference to a compiler-generated field
        callback(objectCAnonStorey41C.asset.GetName(), assetObject, callbackData);
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        AssetCache.ObjectCacheRequest request = AssetCache.GetRequest<AssetCache.ObjectCacheRequest>(objectCAnonStorey41C.asset);
        if (request != null)
        {
          request.SetLastRequestTimestamp(timestamp);
          if (request.DidFail())
          {
            if (callback == null)
              return;
            // ISSUE: reference to a compiler-generated field
            callback(objectCAnonStorey41C.asset.GetName(), (UnityEngine.Object) null, callbackData);
          }
          else
            request.AddRequester(callback, callbackData);
        }
        else
        {
          // ISSUE: reference to a compiler-generated field
          objectCAnonStorey41C.request = new AssetCache.ObjectCacheRequest();
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          objectCAnonStorey41C.request.SetPersistent(objectCAnonStorey41C.asset.IsPersistent());
          // ISSUE: reference to a compiler-generated field
          objectCAnonStorey41C.request.SetCreatedTimestamp(timestamp);
          // ISSUE: reference to a compiler-generated field
          objectCAnonStorey41C.request.SetLastRequestTimestamp(timestamp);
          // ISSUE: reference to a compiler-generated field
          objectCAnonStorey41C.request.AddRequester(callback, callbackData);
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          AssetCache.AddRequest(objectCAnonStorey41C.asset, (AssetCache.CacheRequest) objectCAnonStorey41C.request);
          // ISSUE: reference to a compiler-generated method
          Action<AssetCache.CachedAsset> successCallback = new Action<AssetCache.CachedAsset>(objectCAnonStorey41C.\u003C\u003Em__264);
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          this.StartCoroutine(this.CreateCachedAsset<AssetCache.ObjectCacheRequest, UnityEngine.Object>(objectCAnonStorey41C.request, objectCAnonStorey41C.asset, successCallback, (UnityEngine.Object) null));
        }
      }
    }
  }

  private void LoadCachedPrefab(Asset asset, bool usePrefabPosition, AssetLoader.GameObjectCallback callback, object callbackData, UnityEngine.Object fallback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AssetLoader.\u003CLoadCachedPrefab\u003Ec__AnonStorey41D prefabCAnonStorey41D = new AssetLoader.\u003CLoadCachedPrefab\u003Ec__AnonStorey41D();
    // ISSUE: reference to a compiler-generated field
    prefabCAnonStorey41D.asset = asset;
    // ISSUE: reference to a compiler-generated field
    prefabCAnonStorey41D.usePrefabPosition = usePrefabPosition;
    // ISSUE: reference to a compiler-generated field
    prefabCAnonStorey41D.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    if (prefabCAnonStorey41D.asset.GetName() == null)
    {
      if (callback == null)
        return;
      callback((string) null, (GameObject) null, callbackData);
    }
    else
    {
      long timestamp = TimeUtils.BinaryStamp();
      // ISSUE: reference to a compiler-generated field
      AssetCache.CachedAsset cachedAsset = AssetCache.Find(prefabCAnonStorey41D.asset);
      if (cachedAsset != null)
      {
        cachedAsset.SetLastRequestTimestamp(timestamp);
        UnityEngine.Object assetObject = cachedAsset.GetAssetObject();
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        this.StartCoroutine(this.WaitThenCallGameObjectCallback(prefabCAnonStorey41D.asset, assetObject, prefabCAnonStorey41D.usePrefabPosition, callback, callbackData));
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        AssetCache.PrefabCacheRequest request = AssetCache.GetRequest<AssetCache.PrefabCacheRequest>(prefabCAnonStorey41D.asset);
        if (request != null)
        {
          request.SetLastRequestTimestamp(timestamp);
          if (request.DidFail())
          {
            if (callback == null)
              return;
            // ISSUE: reference to a compiler-generated field
            callback(prefabCAnonStorey41D.asset.GetName(), (GameObject) null, callbackData);
          }
          else
            request.AddRequester(callback, callbackData);
        }
        else
        {
          // ISSUE: reference to a compiler-generated field
          prefabCAnonStorey41D.request = new AssetCache.PrefabCacheRequest();
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          prefabCAnonStorey41D.request.SetPersistent(prefabCAnonStorey41D.asset.IsPersistent());
          // ISSUE: reference to a compiler-generated field
          prefabCAnonStorey41D.request.SetCreatedTimestamp(timestamp);
          // ISSUE: reference to a compiler-generated field
          prefabCAnonStorey41D.request.SetLastRequestTimestamp(timestamp);
          // ISSUE: reference to a compiler-generated field
          prefabCAnonStorey41D.request.AddRequester(callback, callbackData);
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          AssetCache.AddRequest(prefabCAnonStorey41D.asset, (AssetCache.CacheRequest) prefabCAnonStorey41D.request);
          // ISSUE: reference to a compiler-generated method
          Action<AssetCache.CachedAsset> successCallback = new Action<AssetCache.CachedAsset>(prefabCAnonStorey41D.\u003C\u003Em__265);
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          this.StartCoroutine(this.CreateCachedAsset<AssetCache.PrefabCacheRequest, GameObject>(prefabCAnonStorey41D.request, prefabCAnonStorey41D.asset, successCallback, fallback));
        }
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator CreateCachedAsset<RequestType, AssetType>(RequestType request, Asset asset, Action<AssetCache.CachedAsset> successCallback, UnityEngine.Object fallback) where RequestType : AssetCache.CacheRequest
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AssetLoader.\u003CCreateCachedAsset\u003Ec__Iterator2DD<RequestType, AssetType>() { asset = asset, request = request, fallback = fallback, successCallback = successCallback, \u003C\u0024\u003Easset = asset, \u003C\u0024\u003Erequest = request, \u003C\u0024\u003Efallback = fallback, \u003C\u0024\u003EsuccessCallback = successCallback, \u003C\u003Ef__this = this };
  }

  private void CacheAsset(Asset asset, UnityEngine.Object assetObject)
  {
    long timestamp = TimeUtils.BinaryStamp();
    AssetCache.CachedAsset cachedAsset = new AssetCache.CachedAsset();
    cachedAsset.SetAsset(asset);
    cachedAsset.SetAssetObject(assetObject);
    cachedAsset.SetCreatedTimestamp(timestamp);
    cachedAsset.SetLastRequestTimestamp(timestamp);
    cachedAsset.SetPersistent(asset.IsPersistent());
    AssetCache.Add(asset, cachedAsset);
  }

  private void InitCardDirectories(string root = null)
  {
    string sourceDir = AssetPathInfo.FamilyInfo[AssetFamily.CardXML].sourceDir;
    string path = root == null ? sourceDir : Path.Combine(root, sourceDir);
    DirectoryInfo[] directories = new DirectoryInfo(path).GetDirectories();
    AssetLoader.s_cardSetDirectories = new string[directories.Length];
    for (int index = 0; index < directories.Length; ++index)
    {
      string name = directories[index].Name;
      AssetLoader.s_cardSetDirectories[index] = string.Format("{0}/{1}", (object) path, (object) name);
    }
  }

  private void PreloadBundles()
  {
    if (AssetBundleInfo.UseSharedDependencyBundle && (UnityEngine.Object) this.m_sharedBundle == (UnityEngine.Object) null)
      this.m_sharedBundle = AssetBundle.LoadFromFile(AssetLoader.CreateLocalFilePath(string.Format("Data/{0}{1}.unity3d", (object) AssetBundleInfo.BundlePathPlatformModifier(), (object) "shared")));
    using (Map<AssetFamily, AssetFamilyBundleInfo>.Enumerator enumerator = AssetBundleInfo.FamilyInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<AssetFamily, AssetFamilyBundleInfo> current = enumerator.Current;
        AssetFamily key = current.Key;
        AssetFamilyBundleInfo familyBundleInfo = current.Value;
        for (int bundleNum = 0; bundleNum < familyBundleInfo.NumberOfBundles + AssetLoader.AvailableExtraAssetBundlesCount(); ++bundleNum)
          this.GetBundleForFamily(key, bundleNum, new Locale?(), (string) null);
        for (int bundleNum = 0; bundleNum < familyBundleInfo.NumberOfLocaleBundles; ++bundleNum)
        {
          foreach (Locale locale in Localization.GetLoadOrder(false))
            this.GetBundleForFamily(key, bundleNum, new Locale?(locale), (string) null);
        }
      }
    }
    if (AssetLoader.DOWNLOADABLE_LANGUAGE_PACKS)
      DownloadManifest.Get();
    AssetLoader.InitFileListInExtraBundles();
  }

  private AssetBundle GetBundleForFamily(AssetFamily family, string assetName, Locale? locale = null)
  {
    return this.GetBundleForFamily(family, -1, locale, assetName);
  }

  private AssetBundle GetBundleForFamily(AssetFamily family, int bundleNum, Locale? locale = null, string assetName = null)
  {
    int index = (int) family;
    AssetFamilyBundleInfo familyBundleInfo;
    try
    {
      familyBundleInfo = AssetBundleInfo.FamilyInfo[family];
    }
    catch (IndexOutOfRangeException ex)
    {
      UnityEngine.Debug.LogErrorFormat("GetBundleForFamily: Failed to find bundle family: \"{0}\" for asset: \"{1}\".", new object[2]
      {
        (object) family,
        (object) assetName
      });
      return (AssetBundle) null;
    }
    int y = locale.HasValue ? familyBundleInfo.NumberOfLocaleBundles : familyBundleInfo.NumberOfBundles;
    if (!string.IsNullOrEmpty(assetName))
      bundleNum = !AssetLoader.FileIsInFileListInExtraBundles(assetName) ? (y > 1 ? GeneralUtils.UnsignedMod(assetName.GetHashCode(), y) : 0) : y;
    if (locale.HasValue)
    {
      string lower = EnumUtils.GetString<Locale>(locale.Value).ToLower();
      string fileName = string.Format("{0}{1}{2}.unity3d", (object) familyBundleInfo.BundleName, (object) lower, (object) bundleNum);
      if (UpdateManager.Get().ContainsFile(fileName))
      {
        Log.UpdateManager.Print("AssetLoader.GetBundleForFamily - Use UpdateManager.GetAssetBundle {0}", (object) fileName);
        this.m_familyBundles[index, bundleNum] = UpdateManager.Get().GetAssetBundle(fileName);
      }
      if ((UnityEngine.Object) this.m_localizedFamilyBundles[index, bundleNum] == (UnityEngine.Object) null)
      {
        string localFilePath = AssetLoader.CreateLocalFilePath(string.Format("Data/{0}{1}", (object) AssetBundleInfo.BundlePathPlatformModifier(), (object) fileName));
        this.m_localizedFamilyBundles[index, bundleNum] = !File.Exists(localFilePath) ? (AssetBundle) null : AssetBundle.LoadFromFile(localFilePath);
      }
      return this.m_localizedFamilyBundles[index, bundleNum];
    }
    if ((UnityEngine.Object) this.m_familyBundles[index, bundleNum] == (UnityEngine.Object) null)
    {
      string fileName = string.Format("{0}{1}.unity3d", (object) familyBundleInfo.BundleName, (object) bundleNum);
      if (UpdateManager.Get().ContainsFile(fileName))
      {
        Log.UpdateManager.Print("AssetLoader.GetBundleForFamily - Use UpdateManager.GetAssetBundle {0}", (object) fileName);
        this.m_familyBundles[index, bundleNum] = UpdateManager.Get().GetAssetBundle(fileName);
      }
      if ((UnityEngine.Object) this.m_familyBundles[index, bundleNum] == (UnityEngine.Object) null)
      {
        string localFilePath = AssetLoader.CreateLocalFilePath(string.Format("Data/{0}{1}", (object) AssetBundleInfo.BundlePathPlatformModifier(), (object) fileName));
        if (File.Exists(localFilePath))
        {
          AssetBundle assetBundle = AssetBundle.LoadFromFile(localFilePath);
          if ((UnityEngine.Object) assetBundle != (UnityEngine.Object) null)
            this.m_familyBundles[index, bundleNum] = assetBundle;
        }
      }
    }
    return this.m_familyBundles[index, bundleNum];
  }

  private AssetBundle GetDownloadedBundleForFamily(AssetFamily family, string assetName, Locale locale)
  {
    int hashCode = assetName.GetHashCode();
    int index1 = (int) family;
    int downloadableLocaleBundles = AssetBundleInfo.FamilyInfo[family].NumberOfDownloadableLocaleBundles;
    int index2 = downloadableLocaleBundles > 1 ? GeneralUtils.UnsignedMod(hashCode, downloadableLocaleBundles) : 0;
    if ((UnityEngine.Object) this.m_downloadedLocalizedFamilyBundles[index1, index2] == (UnityEngine.Object) null)
    {
      string fileName = DownloadManifest.Get().DownloadableBundleFileName(string.Format("{0}{1}_dlc_{2}.unity3d", (object) AssetBundleInfo.FamilyInfo[family].BundleName, (object) EnumUtils.GetString<Locale>(locale).ToLower(), (object) index2));
      if (fileName == null)
        return (AssetBundle) null;
      Log.Asset.Print("Attempting to load localized, downloaded AssetBundle {0}", (object) fileName);
      this.m_downloadedLocalizedFamilyBundles[index1, index2] = Downloader.Get().GetDownloadedBundle(fileName);
    }
    return this.m_downloadedLocalizedFamilyBundles[index1, index2];
  }

  private AssetBundle GetBundleForAsset(Asset asset)
  {
    bool flag = this.AssetFromDownloadablePack(asset);
    Locale[] loadOrder = Localization.GetLoadOrder(asset.GetFamily());
    for (int index = 0; index < loadOrder.Length; ++index)
    {
      AssetBundle bundleForFamily = this.GetBundleForFamily(asset.GetFamily(), asset.GetName(), new Locale?(loadOrder[index]));
      if ((bool) ((UnityEngine.Object) bundleForFamily) && bundleForFamily.Contains(asset.GetName()))
      {
        asset.SetLocale(loadOrder[index]);
        return bundleForFamily;
      }
      if (flag)
      {
        AssetBundle downloadedBundleForFamily = this.GetDownloadedBundleForFamily(asset.GetFamily(), asset.GetName(), loadOrder[index]);
        if ((bool) ((UnityEngine.Object) downloadedBundleForFamily) && downloadedBundleForFamily.Contains(asset.GetName()))
        {
          asset.SetLocale(loadOrder[index]);
          return downloadedBundleForFamily;
        }
      }
    }
    if (!flag)
    {
      AssetBundle bundleForFamily = this.GetBundleForFamily(asset.GetFamily(), asset.GetName(), new Locale?());
      if ((bool) ((UnityEngine.Object) bundleForFamily) && bundleForFamily.Contains(asset.GetName()))
        return bundleForFamily;
      if (this.m_sharedBundle.Contains(asset.GetName()))
        return this.m_sharedBundle;
    }
    return (AssetBundle) null;
  }

  [DebuggerHidden]
  private IEnumerator CreateCachedAsset_FromBundle<RequestType>(RequestType request, Asset asset, UnityEngine.Object fallback) where RequestType : AssetCache.CacheRequest
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AssetLoader.\u003CCreateCachedAsset_FromBundle\u003Ec__Iterator2DE<RequestType>() { asset = asset, fallback = fallback, request = request, \u003C\u0024\u003Easset = asset, \u003C\u0024\u003Efallback = fallback, \u003C\u0024\u003Erequest = request, \u003C\u003Ef__this = this };
  }

  private GameObject LoadGameObjectImmediately(Asset asset, bool usePrefabPosition, GameObject fallback = null)
  {
    GameObject original = this.LoadObjectImmediately(asset) as GameObject;
    if (!(bool) ((UnityEngine.Object) original))
    {
      UnityEngine.Debug.LogErrorFormat("AssetLoader.LoadGameObjectImmediately() - Expected a prefab and loaded \"{2}\" for asset \"{0}\" from family {1}.", (object) asset.GetName(), (object) asset.GetFamily(), (object) original);
      if ((UnityEngine.Object) fallback == (UnityEngine.Object) null)
        return (GameObject) null;
      original = fallback;
    }
    return !usePrefabPosition ? (GameObject) UnityEngine.Object.Instantiate((UnityEngine.Object) original, this.NewGameObjectSpawnPosition((UnityEngine.Object) original), original.transform.rotation) : UnityEngine.Object.Instantiate<GameObject>(original);
  }

  private UnityEngine.Object LoadObjectImmediately(Asset asset)
  {
    if (string.IsNullOrEmpty(asset.GetName()))
      return (UnityEngine.Object) null;
    UnityEngine.Object assetObject = (UnityEngine.Object) null;
    AssetCache.CachedAsset cachedAsset = AssetCache.Find(asset);
    if (cachedAsset != null)
    {
      cachedAsset.SetLastRequestTimestamp(TimeUtils.BinaryStamp());
      assetObject = cachedAsset.GetAssetObject();
    }
    else
    {
      AssetBundle bundleForAsset = this.GetBundleForAsset(asset);
      if ((UnityEngine.Object) bundleForAsset != (UnityEngine.Object) null)
      {
        assetObject = bundleForAsset.LoadAsset(asset.GetName());
        if (assetObject != (UnityEngine.Object) null)
          this.CacheAsset(asset, assetObject);
        else
          UnityEngine.Debug.LogError((object) ("Unable to find asset " + asset.GetName() + " in bundle " + bundleForAsset.name));
      }
      else
        UnityEngine.Debug.LogError((object) ("Unable to find asset bundle for " + (object) asset.GetFamily() + " " + asset.GetName()));
    }
    return assetObject;
  }

  [DebuggerHidden]
  private IEnumerator WaitThenCallGameObjectCallback(Asset asset, UnityEngine.Object prefab, bool usePrefabPosition, AssetLoader.GameObjectCallback callback, object callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AssetLoader.\u003CWaitThenCallGameObjectCallback\u003Ec__Iterator2DF() { asset = asset, prefab = prefab, callback = callback, callbackData = callbackData, usePrefabPosition = usePrefabPosition, \u003C\u0024\u003Easset = asset, \u003C\u0024\u003Eprefab = prefab, \u003C\u0024\u003Ecallback = callback, \u003C\u0024\u003EcallbackData = callbackData, \u003C\u0024\u003EusePrefabPosition = usePrefabPosition, \u003C\u003Ef__this = this };
  }

  private Vector3 NewGameObjectSpawnPosition(UnityEngine.Object prefab)
  {
    if ((UnityEngine.Object) Camera.main == (UnityEngine.Object) null)
      return Vector3.zero;
    return Camera.main.transform.position + this.SPAWN_POS_CAMERA_OFFSET;
  }

  private bool AssetFromDownloadablePack(Asset asset)
  {
    if (!AssetLoader.DOWNLOADABLE_LANGUAGE_PACKS || Localization.GetLocale() == Locale.enUS || (Localization.GetLocale() == Locale.enGB || AssetBundleInfo.FamilyInfo[asset.GetFamily()].NumberOfDownloadableLocaleBundles == 0))
      return false;
    return DownloadManifest.Get().ContainsFile(Path.GetFileNameWithoutExtension(asset.GetName()));
  }

  private static void LogMissingAsset(AssetFamily family, string assetname)
  {
    Log.MissingAssets.Print(LogLevel.Error, string.Format("[{0}] {1}", (object) family, (object) assetname), new object[0]);
  }

  private static void InitFileListInExtraBundles()
  {
    AssetLoader.fileListInExtraBundles_.Clear();
    string path = string.Format("{0}/{1}", (object) FileUtils.PersistentDataPath, (object) "manifest-filelist-extra.csv");
    if (!File.Exists(path))
      path = FileUtils.GetAssetPath("manifest-filelist-extra.csv");
    Log.UpdateManager.Print("InitFileListInExtraBundles - {0}", (object) path);
    if (!File.Exists(path))
      return;
    using (StreamReader streamReader = new StreamReader(path))
    {
      string str;
      while ((str = streamReader.ReadLine()) != null)
        AssetLoader.fileListInExtraBundles_.Add(str);
    }
    Log.UpdateManager.Print("InitFileListInExtraBundles - Success");
  }

  private static bool UseFileListInExtraBundles()
  {
    return AssetLoader.fileListInExtraBundles_.Count != 0;
  }

  private static bool FileIsInFileListInExtraBundles(string fileName)
  {
    if (!AssetLoader.UseFileListInExtraBundles())
      return false;
    return AssetLoader.fileListInExtraBundles_.Contains(Path.GetFileNameWithoutExtension(fileName));
  }

  private static int AvailableExtraAssetBundlesCount()
  {
    return AssetLoader.UseFileListInExtraBundles() ? 1 : 0;
  }

  private class LoadSoundCallbackData
  {
    public AssetLoader.GameObjectCallback callback;
    public object callbackData;
  }

  public delegate void ObjectCallback(string name, UnityEngine.Object obj, object callbackData);

  public delegate void GameObjectCallback(string name, GameObject go, object callbackData);

  public delegate void FileCallback(string path, WWW file, object callbackData);
}
