﻿// Decompiled with JetBrains decompiler
// Type: StoreLegalBAMLinks
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class StoreLegalBAMLinks : UIBPopup
{
  private static readonly string SEND_TO_BAM_THEN_HIDE_COROUTINE = "SendToBAMThenHide";
  private static readonly string FMT_URL_TERMS_OF_SALE = "https://nydus.battle.net/WTCG/{0}/client/legal/terms-of-sale?targetRegion={1}";
  private static readonly StoreURL TERMS_OF_SALE_URL = new StoreURL(StoreLegalBAMLinks.FMT_URL_TERMS_OF_SALE, StoreURL.Param.LOCALE, StoreURL.Param.REGION);
  private static readonly string FMT_URL_CHANGE_PAYMENT = "https://nydus.battle.net/WTCG/{0}/client/choose-payment-method?targetRegion={1}";
  private static readonly StoreURL CHANGE_PAYMENT_URL = new StoreURL(StoreLegalBAMLinks.FMT_URL_CHANGE_PAYMENT, StoreURL.Param.LOCALE, StoreURL.Param.REGION);
  private List<StoreLegalBAMLinks.SendToBAMListener> m_sendToBAMListeners = new List<StoreLegalBAMLinks.SendToBAMListener>();
  private List<StoreLegalBAMLinks.CancelListener> m_cancelListeners = new List<StoreLegalBAMLinks.CancelListener>();
  public GameObject m_root;
  public UIBButton m_paymentMethodButton;
  public UIBButton m_termsOfSaleButton;
  public PegUIElement m_offClickCatcher;

  private void Awake()
  {
    this.m_termsOfSaleButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnTermsOfSalePressed));
    this.m_paymentMethodButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnPaymentMethodPressed));
    this.m_offClickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClickCatcherPressed));
  }

  public override void Show()
  {
    base.Show();
    this.EnableButtons(true);
    if (this.m_shown)
      ;
  }

  public void RegisterSendToBAMListener(StoreLegalBAMLinks.SendToBAMListener listener)
  {
    if (this.m_sendToBAMListeners.Contains(listener))
      return;
    this.m_sendToBAMListeners.Add(listener);
  }

  public void RemoveSendToBAMListener(StoreLegalBAMLinks.SendToBAMListener listener)
  {
    this.m_sendToBAMListeners.Remove(listener);
  }

  public void RegisterCancelListener(StoreLegalBAMLinks.CancelListener listener)
  {
    if (this.m_cancelListeners.Contains(listener))
      return;
    this.m_cancelListeners.Add(listener);
  }

  public void RemoveCancelListener(StoreLegalBAMLinks.CancelListener listener)
  {
    this.m_cancelListeners.Remove(listener);
  }

  private void OnTermsOfSalePressed(UIEvent e)
  {
    this.StopCoroutine(StoreLegalBAMLinks.SEND_TO_BAM_THEN_HIDE_COROUTINE);
    this.StartCoroutine(StoreLegalBAMLinks.SEND_TO_BAM_THEN_HIDE_COROUTINE, (object) StoreLegalBAMLinks.BAMReason.READ_TERMS_OF_SALE);
  }

  private void OnPaymentMethodPressed(UIEvent e)
  {
    this.StopCoroutine(StoreLegalBAMLinks.SEND_TO_BAM_THEN_HIDE_COROUTINE);
    this.StartCoroutine(StoreLegalBAMLinks.SEND_TO_BAM_THEN_HIDE_COROUTINE, (object) StoreLegalBAMLinks.BAMReason.CHANGE_PAYMENT_METHOD);
  }

  private void OnClickCatcherPressed(UIEvent e)
  {
    this.Hide(true);
    foreach (StoreLegalBAMLinks.CancelListener cancelListener in this.m_cancelListeners.ToArray())
      cancelListener();
  }

  [DebuggerHidden]
  private IEnumerator SendToBAMThenHide(StoreLegalBAMLinks.BAMReason reason)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StoreLegalBAMLinks.\u003CSendToBAMThenHide\u003Ec__Iterator275() { reason = reason, \u003C\u0024\u003Ereason = reason, \u003C\u003Ef__this = this };
  }

  private void EnableButtons(bool enabled)
  {
    this.m_termsOfSaleButton.SetEnabled(enabled);
    this.m_paymentMethodButton.SetEnabled(enabled);
  }

  public enum BAMReason
  {
    CHANGE_PAYMENT_METHOD,
    READ_TERMS_OF_SALE,
  }

  public delegate void SendToBAMListener(StoreLegalBAMLinks.BAMReason urlType);

  public delegate void CancelListener();
}
