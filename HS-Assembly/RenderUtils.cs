﻿// Decompiled with JetBrains decompiler
// Type: RenderUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class RenderUtils
{
  public static Material GetMaterial(Component c, int materialIndex)
  {
    return RenderUtils.GetMaterial(c.GetComponent<Renderer>(), materialIndex);
  }

  public static Material GetMaterial(GameObject go, int materialIndex)
  {
    return RenderUtils.GetMaterial(go.GetComponent<Renderer>(), materialIndex);
  }

  public static Material GetMaterial(Renderer renderer, int materialIndex)
  {
    if (materialIndex < 0)
      return (Material) null;
    Material[] materials = renderer.materials;
    if (materialIndex >= materials.Length)
      return (Material) null;
    Material material = materials[materialIndex];
    material.shaderKeywords = renderer.sharedMaterials[materialIndex].shaderKeywords;
    return material;
  }

  public static Material GetSharedMaterial(Component c, int materialIndex)
  {
    return RenderUtils.GetSharedMaterial(c.GetComponent<Renderer>(), materialIndex);
  }

  public static Material GetSharedMaterial(GameObject go, int materialIndex)
  {
    return RenderUtils.GetSharedMaterial(go.GetComponent<Renderer>(), materialIndex);
  }

  public static Material GetSharedMaterial(Renderer renderer, int materialIndex)
  {
    if (materialIndex < 0)
      return (Material) null;
    Material[] sharedMaterials = renderer.sharedMaterials;
    if (materialIndex >= sharedMaterials.Length)
      return (Material) null;
    return sharedMaterials[materialIndex];
  }

  public static void SetMaterial(Component c, int materialIndex, Material material)
  {
    RenderUtils.SetMaterial(c.GetComponent<Renderer>(), materialIndex, material);
  }

  public static void SetMaterial(GameObject go, int materialIndex, Material material)
  {
    RenderUtils.SetMaterial(go.GetComponent<Renderer>(), materialIndex, material);
  }

  public static void SetMaterial(Renderer renderer, int materialIndex, Material material)
  {
    if (materialIndex < 0)
      return;
    Material[] materials = renderer.materials;
    if (materialIndex >= materials.Length)
      return;
    materials[materialIndex] = material;
    renderer.materials = materials;
    renderer.materials[materialIndex].shaderKeywords = material.shaderKeywords;
  }

  public static void SetSharedMaterial(Component c, int materialIndex, Material material)
  {
    RenderUtils.SetSharedMaterial(c.GetComponent<Renderer>(), materialIndex, material);
  }

  public static void SetSharedMaterial(GameObject go, int materialIndex, Material material)
  {
    RenderUtils.SetSharedMaterial(go.GetComponent<Renderer>(), materialIndex, material);
  }

  public static void SetSharedMaterial(Renderer renderer, int materialIndex, Material material)
  {
    if ((Object) material == (Object) null || materialIndex < 0)
      return;
    Material[] sharedMaterials = renderer.sharedMaterials;
    if (materialIndex >= sharedMaterials.Length)
      return;
    sharedMaterials[materialIndex] = material;
    sharedMaterials[materialIndex].shaderKeywords = material.shaderKeywords;
    renderer.sharedMaterials = sharedMaterials;
  }

  public static void SetAlpha(Component c, float alpha)
  {
    RenderUtils.SetAlpha(c.gameObject, alpha, false);
  }

  public static void SetAlpha(Component c, float alpha, bool includeInactive)
  {
    RenderUtils.SetAlpha(c.gameObject, alpha, includeInactive);
  }

  public static void SetAlpha(GameObject go, float alpha)
  {
    RenderUtils.SetAlpha(go, alpha, false);
  }

  public static void SetAlpha(GameObject go, float alpha, bool includeInactive)
  {
    foreach (Renderer componentsInChild in go.GetComponentsInChildren<Renderer>(includeInactive))
    {
      foreach (Material material in componentsInChild.materials)
      {
        if (material.HasProperty("_Color"))
        {
          Color color = material.color;
          color.a = alpha;
          material.color = color;
        }
        else if (material.HasProperty("_TintColor"))
        {
          Color color = material.GetColor("_TintColor");
          color.a = alpha;
          material.SetColor("_TintColor", color);
        }
      }
      if ((Object) componentsInChild.GetComponent<Light>() != (Object) null)
      {
        Color color = componentsInChild.GetComponent<Light>().color;
        color.a = alpha;
        componentsInChild.GetComponent<Light>().color = color;
      }
    }
    foreach (UberText componentsInChild in go.GetComponentsInChildren<UberText>(includeInactive))
    {
      Color textColor = componentsInChild.TextColor;
      componentsInChild.TextColor = new Color(textColor.r, textColor.g, textColor.b, alpha);
    }
  }

  public static float GetMainTextureScaleX(GameObject go)
  {
    return RenderUtils.GetMainTextureScaleX(go.GetComponent<Renderer>());
  }

  public static float GetMainTextureScaleX(Component c)
  {
    return RenderUtils.GetMainTextureScaleX(c.GetComponent<Renderer>());
  }

  public static float GetMainTextureScaleX(Renderer r)
  {
    return r.material.mainTextureScale.x;
  }

  public static void SetMainTextureScaleX(Component c, float x)
  {
    RenderUtils.SetMainTextureScaleX(c.GetComponent<Renderer>(), x);
  }

  public static void SetMainTextureScaleX(GameObject go, float x)
  {
    RenderUtils.SetMainTextureScaleX(go.GetComponent<Renderer>(), x);
  }

  public static void SetMainTextureScaleX(Renderer r, float x)
  {
    Vector2 mainTextureScale = r.material.mainTextureScale;
    mainTextureScale.x = x;
    r.material.mainTextureScale = mainTextureScale;
  }

  public static float GetMainTextureScaleY(GameObject go)
  {
    return RenderUtils.GetMainTextureScaleY(go.GetComponent<Renderer>());
  }

  public static float GetMainTextureScaleY(Component c)
  {
    return RenderUtils.GetMainTextureScaleY(c.GetComponent<Renderer>());
  }

  public static float GetMainTextureScaleY(Renderer r)
  {
    return r.material.mainTextureScale.y;
  }

  public static void SetMainTextureScaleY(Component c, float y)
  {
    RenderUtils.SetMainTextureScaleY(c.GetComponent<Renderer>(), y);
  }

  public static void SetMainTextureScaleY(GameObject go, float y)
  {
    RenderUtils.SetMainTextureScaleY(go.GetComponent<Renderer>(), y);
  }

  public static void SetMainTextureScaleY(Renderer r, float y)
  {
    Vector2 mainTextureScale = r.material.mainTextureScale;
    mainTextureScale.y = y;
    r.material.mainTextureScale = mainTextureScale;
  }

  public static float GetMainTextureOffsetX(GameObject go)
  {
    return RenderUtils.GetMainTextureOffsetX(go.GetComponent<Renderer>());
  }

  public static float GetMainTextureOffsetX(Component c)
  {
    return RenderUtils.GetMainTextureOffsetX(c.GetComponent<Renderer>());
  }

  public static float GetMainTextureOffsetX(Renderer r)
  {
    return r.material.mainTextureOffset.x;
  }

  public static void SetMainTextureOffsetX(Component c, float x)
  {
    RenderUtils.SetMainTextureOffsetY(c.GetComponent<Renderer>(), x);
  }

  public static void SetMainTextureOffsetX(GameObject go, float x)
  {
    RenderUtils.SetMainTextureOffsetY(go.GetComponent<Renderer>(), x);
  }

  public static void SetMainTextureOffsetX(Renderer r, float x)
  {
    Vector2 mainTextureOffset = r.material.mainTextureOffset;
    mainTextureOffset.x = x;
    r.material.mainTextureOffset = mainTextureOffset;
  }

  public static float GetMainTextureOffsetY(GameObject go)
  {
    return RenderUtils.GetMainTextureOffsetY(go.GetComponent<Renderer>());
  }

  public static float GetMainTextureOffsetY(Component c)
  {
    return RenderUtils.GetMainTextureOffsetY(c.GetComponent<Renderer>());
  }

  public static float GetMainTextureOffsetY(Renderer r)
  {
    return r.material.mainTextureOffset.y;
  }

  public static void SetMainTextureOffsetY(Component c, float y)
  {
    RenderUtils.SetMainTextureOffsetY(c.GetComponent<Renderer>(), y);
  }

  public static void SetMainTextureOffsetY(GameObject go, float y)
  {
    RenderUtils.SetMainTextureOffsetY(go.GetComponent<Renderer>(), y);
  }

  public static void SetMainTextureOffsetY(Renderer r, float y)
  {
    Vector2 mainTextureOffset = r.material.mainTextureOffset;
    mainTextureOffset.y = y;
    r.material.mainTextureOffset = mainTextureOffset;
  }
}
