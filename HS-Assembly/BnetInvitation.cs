﻿// Decompiled with JetBrains decompiler
// Type: BnetInvitation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;

public class BnetInvitation
{
  private BnetInvitationId m_id;
  private BnetEntityId m_inviterId;
  private string m_inviterName;
  private BnetEntityId m_inviteeId;
  private string m_inviteeName;
  private string m_message;
  private ulong m_creationTimeMicrosec;
  private ulong m_expirationTimeMicrosec;

  public static bool operator ==(BnetInvitation a, BnetInvitation b)
  {
    if (object.ReferenceEquals((object) a, (object) b))
      return true;
    if ((object) a == null || (object) b == null)
      return false;
    return a.m_id == b.m_id;
  }

  public static bool operator !=(BnetInvitation a, BnetInvitation b)
  {
    return !(a == b);
  }

  public static BnetInvitation CreateFromFriendsUpdate(FriendsUpdate src)
  {
    return new BnetInvitation() { m_id = new BnetInvitationId(src.long1), m_inviterId = src.entity1.Clone(), m_inviteeId = src.entity2.Clone(), m_inviterName = src.string1, m_inviteeName = src.string2, m_message = src.string3, m_creationTimeMicrosec = src.long2, m_expirationTimeMicrosec = src.long3 };
  }

  public BnetInvitationId GetId()
  {
    return this.m_id;
  }

  public void SetId(BnetInvitationId id)
  {
    this.m_id = id;
  }

  public BnetEntityId GetInviterId()
  {
    return this.m_inviterId;
  }

  public void SetInviterId(BnetEntityId id)
  {
    this.m_inviterId = id;
  }

  public string GetInviterName()
  {
    return this.m_inviterName;
  }

  public void SetInviterName(string name)
  {
    this.m_inviterName = name;
  }

  public BnetEntityId GetInviteeId()
  {
    return this.m_inviteeId;
  }

  public void SetInviteeId(BnetEntityId id)
  {
    this.m_inviteeId = id;
  }

  public string GetInviteeName()
  {
    return this.m_inviteeName;
  }

  public void SetInviteeName(string name)
  {
    this.m_inviteeName = name;
  }

  public string GetMessage()
  {
    return this.m_message;
  }

  public void SetMessage(string message)
  {
    this.m_message = message;
  }

  public ulong GetCreationTimeMicrosec()
  {
    return this.m_creationTimeMicrosec;
  }

  public void SetCreationTimeMicrosec(ulong microsec)
  {
    this.m_creationTimeMicrosec = microsec;
  }

  public ulong GetExpirationTimeMicrosec()
  {
    return this.m_expirationTimeMicrosec;
  }

  public void SetExpirationTimeMicroSec(ulong microsec)
  {
    this.m_expirationTimeMicrosec = microsec;
  }

  public override bool Equals(object obj)
  {
    if (obj == null)
      return false;
    BnetInvitation bnetInvitation = obj as BnetInvitation;
    if ((object) bnetInvitation == null)
      return false;
    return this.m_id.Equals(bnetInvitation.m_id);
  }

  public bool Equals(BnetInvitationId other)
  {
    if ((object) other == null)
      return false;
    return this.m_id.Equals(other);
  }

  public override int GetHashCode()
  {
    return this.m_id.GetHashCode();
  }

  public override string ToString()
  {
    if (this.m_id == (BnetInvitationId) null)
      return "UNKNOWN INVITATION";
    return string.Format("[id={0} inviterId={1} inviterName={2} inviteeId={3} inviteeName={4} message={5}]", (object) this.m_id, (object) this.m_inviterId, (object) this.m_inviterName, (object) this.m_inviteeId, (object) this.m_inviteeName, (object) this.m_message);
  }
}
