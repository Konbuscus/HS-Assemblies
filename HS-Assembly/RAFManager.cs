﻿// Decompiled with JetBrains decompiler
// Type: RAFManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using PegasusUtil;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RAFManager : MonoBehaviour
{
  private const string SHOW_RAF_FRAME_COROUTINE = "ShowRAFFrameWhenReady";
  private const string SEND_TO_RAF_WEBSITE_THEN_HIDE_COROUTINE = "SendToRAFWebsiteThenHide";
  public const int MAX_RECRUITS_SHOWN = 5;
  public const int MAX_PROGRESS_LEVEL = 20;
  public const int REWARDED_HERO_ID = 17;
  private static RAFManager s_Instance;
  private bool m_isRAFLoading;
  private RAFFrame m_RAFFrame;
  private string m_rafDisplayURL;
  private string m_rafFullURL;
  private bool m_hasRAFData;
  private uint m_totalRecruitCount;
  private List<RAFManager.RecruitData> m_topRecruits;

  private void Awake()
  {
    RAFManager.s_Instance = this;
  }

  private void OnDestroy()
  {
    RAFManager.s_Instance = (RAFManager) null;
  }

  public static RAFManager Get()
  {
    return RAFManager.s_Instance;
  }

  public void Initialize()
  {
    Network.Get().RegisterNetHandler((object) ProcessRecruitAFriendResponse.PacketID.ID, new Network.NetHandler(this.OnProcessRecruitResponse), (Network.TimeoutHandler) null);
    Network.Get().RegisterNetHandler((object) RecruitAFriendURLResponse.PacketID.ID, new Network.NetHandler(this.OnURLResponse), (Network.TimeoutHandler) null);
    Network.Get().RegisterNetHandler((object) RecruitAFriendDataResponse.PacketID.ID, new Network.NetHandler(this.OnDataResponse), (Network.TimeoutHandler) null);
    ApplicationMgr.Get().WillReset += new System.Action(this.WillReset);
  }

  public void WillReset()
  {
    ApplicationMgr.Get().WillReset -= new System.Action(this.WillReset);
    Network.Get().RemoveNetHandler((object) ProcessRecruitAFriendResponse.PacketID.ID, new Network.NetHandler(this.OnProcessRecruitResponse));
    Network.Get().RemoveNetHandler((object) RecruitAFriendURLResponse.PacketID.ID, new Network.NetHandler(this.OnURLResponse));
    Network.Get().RemoveNetHandler((object) RecruitAFriendDataResponse.PacketID.ID, new Network.NetHandler(this.OnDataResponse));
    BnetPresenceMgr.Get().OnGameAccountPresenceChange -= new System.Action<PresenceUpdate[]>(this.OnPresenceChanged);
    this.m_RAFFrame = (RAFFrame) null;
    this.m_rafDisplayURL = (string) null;
    this.m_rafFullURL = (string) null;
    this.m_hasRAFData = false;
    this.m_totalRecruitCount = 0U;
    this.m_topRecruits = (List<RAFManager.RecruitData>) null;
  }

  public void InitializeRequests()
  {
    Network.RequestProcessRecruitAFriend();
  }

  public void ShowRAFFrame()
  {
    SoundManager.Get().LoadAndPlay("Small_Click", this.gameObject);
    if (!this.m_hasRAFData)
    {
      Log.RAF.Print("Network.RequestRecruitAFriendData");
      Network.RequestRecruitAFriendData();
    }
    this.StopCoroutine("ShowRAFFrameWhenReady");
    this.StartCoroutine("ShowRAFFrameWhenReady");
  }

  public RAFFrame GetRAFFrame()
  {
    return this.m_RAFFrame;
  }

  public void ShowRAFHeroFrame()
  {
    if (!((UnityEngine.Object) this.m_RAFFrame != (UnityEngine.Object) null))
      return;
    this.m_RAFFrame.ShowHeroFrame();
  }

  public void ShowRAFProgressFrame()
  {
    if (!((UnityEngine.Object) this.m_RAFFrame != (UnityEngine.Object) null))
      return;
    this.m_RAFFrame.ShowProgressFrame();
  }

  public void SetRAFProgress(int progress)
  {
    if (!((UnityEngine.Object) this.m_RAFFrame != (UnityEngine.Object) null))
      return;
    this.m_RAFFrame.SetProgress(progress);
  }

  public string GetRecruitDisplayURL()
  {
    if (this.m_rafDisplayURL != null)
      return this.m_rafDisplayURL;
    Log.RAF.Print("Network.RequestRecruitAFriendURL");
    Network.RequestRecruitAFriendUrl();
    return (string) null;
  }

  public string GetRecruitFullURL()
  {
    if (this.m_rafFullURL != null)
      return this.m_rafFullURL;
    return (string) null;
  }

  public void GotoRAFWebsite()
  {
    this.StopCoroutine("SendToRAFWebsiteThenHide");
    this.StartCoroutine("SendToRAFWebsiteThenHide");
  }

  [DebuggerHidden]
  private IEnumerator ShowRAFFrameWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RAFManager.\u003CShowRAFFrameWhenReady\u003Ec__Iterator23A() { \u003C\u003Ef__this = this };
  }

  private void OnRAFLoaded(string name, GameObject go, object cbData)
  {
    this.m_isRAFLoading = false;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      Log.RAF.PrintError("RAFManager.OnRAFLoaded() - FAILED to load " + name);
    }
    else
    {
      this.m_RAFFrame = go.GetComponent<RAFFrame>();
      if ((UnityEngine.Object) this.m_RAFFrame == (UnityEngine.Object) null)
      {
        Log.RAF.PrintError("RAFManager.OnRAFLoaded() - ERROR " + name + " has no " + (object) typeof (RAFFrame) + " component");
      }
      else
      {
        if (!this.m_hasRAFData)
          return;
        if (this.m_totalRecruitCount > 0U)
        {
          this.m_RAFFrame.SetProgressData(this.m_totalRecruitCount, this.m_topRecruits);
          this.m_RAFFrame.ShowProgressFrame();
        }
        else
          this.m_RAFFrame.ShowHeroFrame();
      }
    }
  }

  private void OnProcessRecruitResponse()
  {
  }

  private void OnURLResponse()
  {
    RecruitAFriendURLResponse afriendUrlResponse = Network.GetRecruitAFriendUrlResponse();
    if (afriendUrlResponse == null || afriendUrlResponse.RafServiceStatus == RAFServiceStatus.RAFServiceStatus_NotAvailable || string.IsNullOrEmpty(afriendUrlResponse.RafUrl))
    {
      string format = "RAFManager.OnURLResponse() - Response not valid!";
      if (afriendUrlResponse != null)
        format += " " + (object) afriendUrlResponse.RafServiceStatus + ", " + afriendUrlResponse.RafUrl != null ? afriendUrlResponse.RafUrl : "null";
      Log.RAF.PrintError(format);
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_RAF_ERROR_HEADER"),
        m_showAlertIcon = true,
        m_text = GameStrings.Get("GLUE_RAF_ERROR_BODY"),
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_responseCallback = (AlertPopup.ResponseCallback) null,
        m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
      });
    }
    else
    {
      this.m_rafDisplayURL = afriendUrlResponse.RafUrl;
      Log.RAF.Print("Recruit URL = " + this.m_rafDisplayURL);
      if (!((UnityEngine.Object) this.m_RAFFrame != (UnityEngine.Object) null))
        return;
      this.m_rafFullURL = afriendUrlResponse.RafUrlFull;
      this.m_RAFFrame.ShowLinkFrame(this.m_rafDisplayURL, this.m_rafFullURL);
    }
  }

  private void OnDataResponse()
  {
    RecruitAFriendDataResponse afriendDataResponse = Network.GetRecruitAFriendDataResponse();
    if (afriendDataResponse == null)
    {
      Log.RAF.PrintError("RAFManager.OnDataResponse() - Recruit Data is NULL!");
    }
    else
    {
      this.m_hasRAFData = true;
      this.m_totalRecruitCount = afriendDataResponse.TotalRecruitCount;
      this.m_topRecruits = new List<RAFManager.RecruitData>();
      BnetPresenceMgr.Get().OnGameAccountPresenceChange -= new System.Action<PresenceUpdate[]>(this.OnPresenceChanged);
      BnetPresenceMgr.Get().OnGameAccountPresenceChange += new System.Action<PresenceUpdate[]>(this.OnPresenceChanged);
      for (int index = 0; index < afriendDataResponse.TopRecruits.Count; ++index)
      {
        RAFManager.RecruitData recruitData = new RAFManager.RecruitData();
        this.m_topRecruits.Add(recruitData);
        recruitData.m_recruit = afriendDataResponse.TopRecruits[index];
        if (recruitData.m_recruit.GameAccountId == null)
        {
          Log.RAF.PrintWarning("RAFManager.OnDataResponse() - GameAccountId is NULL for recruit!");
        }
        else
        {
          BnetGameAccountId bnetGameAccountId = new BnetGameAccountId();
          bnetGameAccountId.SetHi(recruitData.m_recruit.GameAccountId.Hi);
          bnetGameAccountId.SetLo(recruitData.m_recruit.GameAccountId.Lo);
          EntityId entityId = new EntityId();
          entityId.hi = bnetGameAccountId.GetHi();
          entityId.lo = bnetGameAccountId.GetLo();
          List<PresenceFieldKey> presenceFieldKeyList = new List<PresenceFieldKey>();
          PresenceFieldKey presenceFieldKey = new PresenceFieldKey();
          presenceFieldKey.programId = BnetProgramId.BNET.GetValue();
          presenceFieldKey.groupId = 2U;
          presenceFieldKey.fieldId = 7U;
          presenceFieldKey.index = 0UL;
          presenceFieldKeyList.Add(presenceFieldKey);
          presenceFieldKey.programId = BnetProgramId.BNET.GetValue();
          presenceFieldKey.groupId = 2U;
          presenceFieldKey.fieldId = 3U;
          presenceFieldKey.index = 0UL;
          presenceFieldKeyList.Add(presenceFieldKey);
          presenceFieldKey.programId = BnetProgramId.BNET.GetValue();
          presenceFieldKey.groupId = 2U;
          presenceFieldKey.fieldId = 5U;
          presenceFieldKey.index = 0UL;
          presenceFieldKeyList.Add(presenceFieldKey);
          PresenceFieldKey[] array = presenceFieldKeyList.ToArray();
          BattleNet.RequestPresenceFields(true, entityId, array);
        }
      }
      if (!((UnityEngine.Object) this.m_RAFFrame != (UnityEngine.Object) null))
        return;
      if (this.m_totalRecruitCount > 0U)
      {
        this.m_RAFFrame.SetProgressData(this.m_totalRecruitCount, this.m_topRecruits);
        this.m_RAFFrame.ShowProgressFrame();
      }
      else
        this.m_RAFFrame.ShowHeroFrame();
    }
  }

  private void OnPresenceChanged(PresenceUpdate[] updates)
  {
    if (this.m_topRecruits == null)
      return;
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    foreach (PresenceUpdate update in updates)
    {
      if (!(update.programId != (bgs.FourCC) BnetProgramId.BNET) && (int) update.groupId == 2 && (int) update.fieldId == 5)
      {
        BnetPlayer player = BnetUtils.GetPlayer(BnetGameAccountId.CreateFromEntityId(update.entityId));
        if (player != null && player != myPlayer && !(player.GetBattleTag() == (BnetBattleTag) null))
        {
          EntityId entityId = update.entityId;
          using (List<RAFManager.RecruitData>.Enumerator enumerator = this.m_topRecruits.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              RAFManager.RecruitData current = enumerator.Current;
              if ((long) current.m_recruit.GameAccountId.Lo == (long) entityId.lo && (long) current.m_recruit.GameAccountId.Hi == (long) entityId.hi)
              {
                current.m_recruitBattleTag = player.GetBattleTag().GetString();
                Log.RAF.Print("Found Battle Tag for Game Account ID: " + current.m_recruitBattleTag);
                if ((UnityEngine.Object) this.m_RAFFrame != (UnityEngine.Object) null)
                {
                  this.m_RAFFrame.UpdateBattleTag(current.m_recruit.GameAccountId, current.m_recruitBattleTag);
                  break;
                }
                break;
              }
            }
          }
        }
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator SendToRAFWebsiteThenHide()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RAFManager.\u003CSendToRAFWebsiteThenHide\u003Ec__Iterator23B() { \u003C\u003Ef__this = this };
  }

  public class RecruitData
  {
    public PegasusUtil.RecruitData m_recruit;
    public string m_recruitBattleTag;
  }
}
