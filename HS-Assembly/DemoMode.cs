﻿// Decompiled with JetBrains decompiler
// Type: DemoMode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.ComponentModel;

public enum DemoMode
{
  [Description("None")] NONE,
  [Description("PAX East 2013")] PAX_EAST_2013,
  [Description("gamescom 2013")] GAMESCOM_2013,
  [Description("BlizzCon 2013")] BLIZZCON_2013,
  [Description("BlizzCon 2014")] BLIZZCON_2014,
  [Description("BlizzCon 2015")] BLIZZCON_2015,
  [Description("Apple Store")] APPLE_STORE,
  [Description("5.0 Announcement")] ANNOUNCEMENT_5_0,
  [Description("BlizzCon 2016")] BLIZZCON_2016,
}
