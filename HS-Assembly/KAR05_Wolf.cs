﻿// Decompiled with JetBrains decompiler
// Type: KAR05_Wolf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR05_Wolf : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Barnes_Male_Human_WolfBigMinion_01");
    this.PreloadSound("VO_Barnes_Male_Human_WolfClaws_01");
    this.PreloadSound("VO_Barnes_Male_Human_WolfTurn5_01");
    this.PreloadSound("VO_Barnes_Male_Human_WolfTurn9_01");
    this.PreloadSound("VO_Barnes_Male_Human_WolfWin_01");
    this.PreloadSound("VO_BigBadWolf_Male_Worgen_WolfBigMinion_01");
    this.PreloadSound("VO_BigBadWolf_Male_Worgen_WolfTurn1_01");
    this.PreloadSound("VO_BigBadWolf_Male_Worgen_WolfEmoteResponse_01");
    this.PreloadSound("VO_BigBadWolf_Male_Worgen_WolfDireWolfAlpha_01");
    this.PreloadSound("VO_BigBadWolf_Male_Worgen_WolfDireWolfAlpha_02");
    this.PreloadSound("VO_BigBadWolf_Male_Worgen_WolfScarletCrusader_01");
    this.PreloadSound("VO_Moroes_Male_Human_WolfClaws_03");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BigBadWolf_Male_Worgen_WolfEmoteResponse_01",
            m_stringTag = "VO_BigBadWolf_Male_Worgen_WolfEmoteResponse_01"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR05_Wolf.\u003CHandleMissionEventWithTiming\u003Ec__Iterator19C() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR05_Wolf.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator19D() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  private Actor GetDireWolf()
  {
    Player friendlySidePlayer = GameState.Get().GetFriendlySidePlayer();
    using (List<Card>.Enumerator enumerator = friendlySidePlayer.GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity = enumerator.Current.GetEntity();
        if (entity.GetControllerId() == friendlySidePlayer.GetPlayerId() && entity.GetCardId() == "EX1_162")
          return entity.GetCard().GetActor();
      }
    }
    return (Actor) null;
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR05_Wolf.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator19E() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR05_Wolf.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator19F() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR05_Wolf.\u003CHandleGameOverWithTiming\u003Ec__Iterator1A0() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
