﻿// Decompiled with JetBrains decompiler
// Type: FBSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FBSettings : ScriptableObject
{
  [SerializeField]
  private string[] appIds = new string[1]{ "0" };
  [SerializeField]
  private string[] appLabels = new string[1]{ "App Name" };
  [SerializeField]
  private bool cookie = true;
  [SerializeField]
  private bool logging = true;
  [SerializeField]
  private bool status = true;
  [SerializeField]
  private bool frictionlessRequests = true;
  [SerializeField]
  private string iosURLSuffix = string.Empty;
  private const string facebookSettingsAssetName = "FacebookSettings";
  private const string facebookSettingsPath = "MobileAdTracking/Facebook/Resources";
  private const string facebookSettingsAssetExtension = ".asset";
  private static FBSettings instance;
  [SerializeField]
  private int selectedAppIndex;
  [SerializeField]
  private bool xfbml;

  private static FBSettings Instance
  {
    get
    {
      if ((Object) FBSettings.instance == (Object) null)
      {
        FBSettings.instance = Resources.Load("FacebookSettings") as FBSettings;
        if ((Object) FBSettings.instance == (Object) null)
          FBSettings.instance = ScriptableObject.CreateInstance<FBSettings>();
      }
      return FBSettings.instance;
    }
  }

  public int SelectedAppIndex
  {
    get
    {
      return this.selectedAppIndex;
    }
  }

  public string[] AppIds
  {
    get
    {
      return this.appIds;
    }
    set
    {
      if (this.appIds == value)
        return;
      this.appIds = value;
      FBSettings.DirtyEditor();
    }
  }

  public string[] AppLabels
  {
    get
    {
      return this.appLabels;
    }
    set
    {
      if (this.appLabels == value)
        return;
      this.appLabels = value;
      FBSettings.DirtyEditor();
    }
  }

  public static string[] AllAppIds
  {
    get
    {
      return FBSettings.Instance.AppIds;
    }
  }

  public static string AppId
  {
    get
    {
      return FBSettings.Instance.AppIds[FBSettings.Instance.SelectedAppIndex];
    }
  }

  public static bool IsValidAppId
  {
    get
    {
      if (FBSettings.AppId != null && FBSettings.AppId.Length > 0)
        return !FBSettings.AppId.Equals("0");
      return false;
    }
  }

  public static bool Cookie
  {
    get
    {
      return FBSettings.Instance.cookie;
    }
    set
    {
      if (FBSettings.Instance.cookie == value)
        return;
      FBSettings.Instance.cookie = value;
      FBSettings.DirtyEditor();
    }
  }

  public static bool Logging
  {
    get
    {
      return FBSettings.Instance.logging;
    }
    set
    {
      if (FBSettings.Instance.logging == value)
        return;
      FBSettings.Instance.logging = value;
      FBSettings.DirtyEditor();
    }
  }

  public static bool Status
  {
    get
    {
      return FBSettings.Instance.status;
    }
    set
    {
      if (FBSettings.Instance.status == value)
        return;
      FBSettings.Instance.status = value;
      FBSettings.DirtyEditor();
    }
  }

  public static bool Xfbml
  {
    get
    {
      return FBSettings.Instance.xfbml;
    }
    set
    {
      if (FBSettings.Instance.xfbml == value)
        return;
      FBSettings.Instance.xfbml = value;
      FBSettings.DirtyEditor();
    }
  }

  public static string IosURLSuffix
  {
    get
    {
      return FBSettings.Instance.iosURLSuffix;
    }
    set
    {
      if (!(FBSettings.Instance.iosURLSuffix != value))
        return;
      FBSettings.Instance.iosURLSuffix = value;
      FBSettings.DirtyEditor();
    }
  }

  public static string ChannelUrl
  {
    get
    {
      return "/channel.html";
    }
  }

  public static bool FrictionlessRequests
  {
    get
    {
      return FBSettings.Instance.frictionlessRequests;
    }
    set
    {
      if (FBSettings.Instance.frictionlessRequests == value)
        return;
      FBSettings.Instance.frictionlessRequests = value;
      FBSettings.DirtyEditor();
    }
  }

  public void SetAppIndex(int index)
  {
    if (this.selectedAppIndex == index)
      return;
    this.selectedAppIndex = index;
    FBSettings.DirtyEditor();
  }

  public void SetAppId(int index, string value)
  {
    if (!(this.appIds[index] != value))
      return;
    this.appIds[index] = value;
    FBSettings.DirtyEditor();
  }

  public void SetAppLabel(int index, string value)
  {
    if (!(this.appLabels[index] != value))
      return;
    this.AppLabels[index] = value;
    FBSettings.DirtyEditor();
  }

  private static void DirtyEditor()
  {
  }
}
