﻿// Decompiled with JetBrains decompiler
// Type: AddFriendFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class AddFriendFrame : MonoBehaviour
{
  private string m_inputText = string.Empty;
  public AddFriendFrameBones m_Bones;
  public UberText m_HeaderText;
  public UberText m_InstructionText;
  public TextField m_InputTextField;
  public Font m_InputFont;
  private PegUIElement m_inputBlocker;
  private BnetPlayer m_player;
  private bool m_usePlayer;
  private string m_playerDisplayName;
  private Font m_localizedInputFont;

  public event Action Closed;

  private void Awake()
  {
    this.InitItems();
    this.Layout();
    this.InitInput();
    this.InitInputTextField();
    DialogManager.Get().OnDialogShown += new Action(this.OnDialogShown);
    DialogManager.Get().OnDialogHidden += new Action(this.OnDialogHidden);
  }

  private void Start()
  {
    this.m_InputTextField.SetInputFont(this.m_localizedInputFont);
    this.m_InputTextField.Activate();
    if (DialogManager.Get().ShowingDialog())
      return;
    this.m_InputTextField.Text = this.m_inputText;
    this.UpdateInstructions();
  }

  private void InitInput()
  {
    FontDef fontDef = FontTable.Get().GetFontDef(this.m_InputFont);
    if ((UnityEngine.Object) fontDef == (UnityEngine.Object) null)
      this.m_localizedInputFont = this.m_InputFont;
    else
      this.m_localizedInputFont = fontDef.m_Font;
  }

  public void UpdateLayout()
  {
    this.Layout();
  }

  public void Close()
  {
    if ((UnityEngine.Object) this.m_inputBlocker != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_inputBlocker.gameObject);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  public void SetPlayer(BnetPlayer player)
  {
    this.m_player = player;
    if (player == null)
    {
      this.m_usePlayer = false;
      this.m_playerDisplayName = (string) null;
    }
    else
    {
      this.m_usePlayer = true;
      this.m_playerDisplayName = FriendUtils.GetUniqueName(this.m_player);
    }
    if (DialogManager.Get().ShowingDialog())
    {
      this.SaveAndHideText(this.m_playerDisplayName);
    }
    else
    {
      this.m_inputText = this.m_playerDisplayName;
      this.m_InputTextField.Text = this.m_inputText;
      this.UpdateInstructions();
    }
  }

  private void InitItems()
  {
    this.m_HeaderText.Text = GameStrings.Get("GLOBAL_ADDFRIEND_HEADER");
    this.m_InstructionText.Text = GameStrings.Get("GLOBAL_ADDFRIEND_INSTRUCTION");
    this.InitInputBlocker();
  }

  private void Layout()
  {
    this.transform.parent = BaseUI.Get().transform;
    this.transform.position = BaseUI.Get().GetAddFriendBone().position;
    if ((!UniversalInputManager.Get().UseWindowsTouch() || !W8Touch.s_isWindows8OrGreater) && !W8Touch.Get().IsVirtualKeyboardVisible())
      return;
    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 100f, this.transform.position.z);
  }

  private void UpdateInstructions()
  {
    if (!((UnityEngine.Object) this.m_InstructionText != (UnityEngine.Object) null))
      return;
    this.m_InstructionText.gameObject.SetActive(string.IsNullOrEmpty(this.m_inputText) && string.IsNullOrEmpty(Input.compositionString));
  }

  private void InitInputBlocker()
  {
    GameObject inputBlocker = CameraUtils.CreateInputBlocker(CameraUtils.FindFirstByLayer(this.gameObject.layer), "AddFriendInputBlocker");
    inputBlocker.transform.parent = this.transform.parent;
    this.m_inputBlocker = inputBlocker.AddComponent<PegUIElement>();
    this.m_inputBlocker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnInputBlockerReleased));
    TransformUtil.SetPosZ((Component) this.m_inputBlocker, this.transform.position.z + 1f);
  }

  private void OnInputBlockerReleased(UIEvent e)
  {
    this.OnClosed();
  }

  private void InitInputTextField()
  {
    this.m_InputTextField.Preprocess += new Action<Event>(this.OnInputPreprocess);
    this.m_InputTextField.Changed += new Action<string>(this.OnInputChanged);
    this.m_InputTextField.Submitted += new Action<string>(this.OnInputSubmitted);
    this.m_InputTextField.Canceled += new Action(this.OnInputCanceled);
    this.m_InstructionText.gameObject.SetActive(true);
  }

  private void OnInputPreprocess(Event e)
  {
    if (!Input.imeIsSelected)
      return;
    this.UpdateInstructions();
  }

  private void OnInputChanged(string text)
  {
    if (DialogManager.Get().ShowingDialog())
    {
      this.m_InputTextField.Text = string.Empty;
    }
    else
    {
      this.m_inputText = text;
      this.UpdateInstructions();
      this.m_usePlayer = string.Compare(this.m_playerDisplayName, text.Trim(), true) == 0;
    }
  }

  private void OnInputSubmitted(string input)
  {
    string str = !this.m_usePlayer ? input.Trim() : this.m_player.GetBattleTag().ToString();
    if (str.Contains("@"))
      BnetFriendMgr.Get().SendInviteByEmail(str);
    else if (str.Contains("#"))
      BnetFriendMgr.Get().SendInviteByBattleTag(str);
    else
      UIStatus.Get().AddError(GameStrings.Get("GLOBAL_ADDFRIEND_ERROR_MALFORMED"), -1f);
    this.OnClosed();
  }

  private void OnInputCanceled()
  {
    this.OnClosed();
  }

  private void OnClosed()
  {
    if (this.Closed == null)
      return;
    this.Closed();
  }

  private void SaveAndHideText(string text)
  {
    this.m_inputText = text;
    this.m_InputTextField.Text = string.Empty;
  }

  private void ShowSavedText()
  {
    this.m_InputTextField.Text = this.m_inputText;
    this.UpdateInstructions();
  }

  private void OnDialogShown()
  {
    this.SaveAndHideText(this.m_inputText);
  }

  private void OnDialogHidden()
  {
    this.ShowSavedText();
  }
}
