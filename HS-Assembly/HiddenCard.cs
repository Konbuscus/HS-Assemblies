﻿// Decompiled with JetBrains decompiler
// Type: HiddenCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class HiddenCard : CardDef
{
  public override string DetermineActorNameForZone(Entity entity, TAG_ZONE zoneTag)
  {
    if (zoneTag == TAG_ZONE.DECK || zoneTag == TAG_ZONE.GRAVEYARD || (zoneTag == TAG_ZONE.REMOVEDFROMGAME || zoneTag == TAG_ZONE.SETASIDE))
      return "Card_Invisible";
    if (zoneTag == TAG_ZONE.SECRET)
      return base.DetermineActorNameForZone(entity, zoneTag);
    return "Card_Hidden";
  }
}
