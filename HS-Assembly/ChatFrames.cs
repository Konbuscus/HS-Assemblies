﻿// Decompiled with JetBrains decompiler
// Type: ChatFrames
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using UnityEngine;

public class ChatFrames : MonoBehaviour
{
  public MobileChatLogFrame chatLogFrame;
  private bool wasShowingDialog;

  public BnetPlayer Receiver
  {
    get
    {
      return this.chatLogFrame.Receiver;
    }
    set
    {
      this.chatLogFrame.Receiver = value;
      if (this.chatLogFrame.Receiver == null)
        ChatMgr.Get().CloseChatUI();
      this.OnFramesMoved();
    }
  }

  private void Awake()
  {
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    BnetEventMgr.Get().AddChangeListener(new BnetEventMgr.ChangeCallback(this.OnBnetEventOccurred));
    this.chatLogFrame.CloseButtonReleased += new Action(this.OnCloseButtonReleased);
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null)
      SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    if (BnetEventMgr.Get() != null)
      BnetEventMgr.Get().RemoveChangeListener(new BnetEventMgr.ChangeCallback(this.OnBnetEventOccurred));
    this.OnFramesMoved();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  private void Update()
  {
    bool flag = DialogManager.Get().ShowingDialog();
    if (flag == this.wasShowingDialog)
      return;
    if (flag && this.chatLogFrame.HasFocus)
      this.OnPopupOpened();
    else if (!flag && !ChatMgr.Get().FriendListFrame.ShowingAddFriendFrame && !this.chatLogFrame.HasFocus)
      this.OnPopupClosed();
    this.wasShowingDialog = flag;
  }

  public void Back()
  {
    if (DialogManager.Get().ShowingDialog())
      return;
    if (ChatMgr.Get().FriendListFrame.ShowingAddFriendFrame)
      ChatMgr.Get().FriendListFrame.CloseAddFriendFrame();
    else if (this.Receiver != null)
      this.Receiver = (BnetPlayer) null;
    else
      ChatMgr.Get().CloseChatUI();
  }

  private void OnFramesMoved()
  {
    if (!((UnityEngine.Object) ChatMgr.Get() != (UnityEngine.Object) null))
      return;
    ChatMgr.Get().OnChatFramesMoved();
  }

  private void OnCloseButtonReleased()
  {
    ChatMgr.Get().CloseChatUI();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    ChatMgr.Get().ShowFriendsList();
  }

  private void OnPopupOpened()
  {
    if (!this.chatLogFrame.HasFocus)
      return;
    this.chatLogFrame.Focus(false);
  }

  private void OnPopupClosed()
  {
    if (this.Receiver == null)
      return;
    this.chatLogFrame.Focus(true);
  }

  private void OnBnetEventOccurred(BattleNet.BnetEvent bnetEvent, object userData)
  {
    if (bnetEvent != BattleNet.BnetEvent.Disconnected)
      return;
    ChatMgr.Get().CleanUp();
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (mode != SceneMgr.Mode.FATAL_ERROR)
      return;
    ChatMgr.Get().CleanUp();
  }
}
