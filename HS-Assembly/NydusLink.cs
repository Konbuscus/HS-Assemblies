﻿// Decompiled with JetBrains decompiler
// Type: NydusLink
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections.Generic;
using UnityEngine;

public class NydusLink
{
  private static string DevHost = "https://nydus-qa.web.blizzard.net";
  private static string ProdHost = "https://nydus.battle.net";
  private static readonly Map<string, string> TargetServerToRegion = new Map<string, string>() { { "us.actual.battle.net", "US" }, { "eu.actual.battle.net", "EU" }, { "kr.actual.battle.net", "KR" }, { "cn.actual.battle.net", "CN" } };
  private static readonly Map<constants.BnetRegion, string> s_regionToStrMap = new Map<constants.BnetRegion, string>() { { constants.BnetRegion.REGION_US, "US" }, { constants.BnetRegion.REGION_EU, "EU" }, { constants.BnetRegion.REGION_KR, "KR" }, { constants.BnetRegion.REGION_TW, "TW" }, { constants.BnetRegion.REGION_CN, "CN" }, { constants.BnetRegion.REGION_PTR, "US" } };

  public static string RegionToStr(constants.BnetRegion region)
  {
    if (NydusLink.s_regionToStrMap.ContainsKey(region))
      return NydusLink.s_regionToStrMap[region];
    return (string) null;
  }

  public static string GetBreakingNewsLink()
  {
    return NydusLink.GetLink("alert", true);
  }

  public static string GetAccountCreationLink()
  {
    return NydusLink.GetLink("creation", true);
  }

  public static string GetLink(string linkType, bool isMobile)
  {
    string baseUrl;
    string localeString;
    string regionString;
    NydusLink.GetLocalizedLinkVars(out baseUrl, out localeString, out regionString);
    string str = !isMobile ? string.Empty : "-mobile";
    return string.Format("{0}/WTCG/{1}/client{2}/{3}?targetRegion={4}", (object) baseUrl, (object) localeString, (object) str, (object) linkType, (object) regionString);
  }

  public static string GetSupportLink(string linkType, bool isMobile)
  {
    string baseUrl;
    string localeString;
    string regionString;
    NydusLink.GetLocalizedLinkVars(out baseUrl, out localeString, out regionString);
    string str = !isMobile ? string.Empty : "-mobile";
    return string.Format("{0}/WTCG/{1}/client{2}/support/{3}?targetRegion={4}", (object) baseUrl, (object) localeString, (object) str, (object) linkType, (object) regionString);
  }

  private static void GetLocalizedLinkVars(out string baseUrl, out string localeString, out string regionString)
  {
    localeString = Localization.GetLocaleName();
    bool isDev = ApplicationMgr.GetMobileEnvironment() == MobileEnv.DEVELOPMENT || ApplicationMgr.IsInternal();
    MobileDeviceLocale.ConnectionData dataFromRegionId = MobileDeviceLocale.GetConnectionDataFromRegionId(MobileDeviceLocale.GetCurrentRegionId(), isDev);
    try
    {
      if (isDev)
      {
        regionString = NydusLink.RegionToStr(MobileDeviceLocale.FindDevRegionByServerVersion(dataFromRegionId.version));
        if (regionString == null)
        {
          Debug.Log((object) ("[DEV] No matching region found for " + dataFromRegionId.version + " to get Nydus Link"));
          regionString = "US";
        }
      }
      else
        regionString = NydusLink.TargetServerToRegion[dataFromRegionId.address];
    }
    catch (KeyNotFoundException ex)
    {
      Debug.LogWarning((object) ("No matching region found for " + dataFromRegionId.address + " to get Nydus Link"));
      regionString = "US";
    }
    baseUrl = !isDev ? NydusLink.ProdHost : NydusLink.DevHost;
  }
}
