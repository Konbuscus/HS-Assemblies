﻿// Decompiled with JetBrains decompiler
// Type: AdventureClassChallengeButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AdventureClassChallengeButton : PegUIElement
{
  public UberText m_Text;
  public int m_ScenarioID;
  public HighlightState m_Highlight;
  public GameObject m_RootObject;
  public GameObject m_Chest;
  public GameObject m_Checkmark;
  public Transform m_UpBone;
  public Transform m_DownBone;

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    SoundManager.Get().LoadAndPlay("collection_manager_hero_mouse_over", this.gameObject);
    this.m_Highlight.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_Highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }

  public void Select(bool playSound)
  {
    if (playSound)
      SoundManager.Get().LoadAndPlay("select_AI_opponent", this.gameObject);
    this.m_Highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    this.SetEnabled(false);
    this.Depress();
  }

  public void Deselect()
  {
    this.m_Highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
    this.Raise(0.1f);
    this.SetEnabled(true);
  }

  public void SetPortraitMaterial(Material portraitMat)
  {
    Renderer component = this.m_RootObject.GetComponent<Renderer>();
    Material[] materials = component.materials;
    materials[1] = portraitMat;
    component.materials = materials;
  }

  private void Raise(float time)
  {
    iTween.MoveTo(this.m_RootObject, iTween.Hash((object) "position", (object) this.m_UpBone.localPosition, (object) "time", (object) time, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true));
  }

  private void Depress()
  {
    iTween.MoveTo(this.m_RootObject, iTween.Hash((object) "position", (object) this.m_DownBone.localPosition, (object) "time", (object) 0.1f, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true));
  }
}
