﻿// Decompiled with JetBrains decompiler
// Type: FriendListItemHeader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class FriendListItemHeader : PegUIElement, ITouchListItem
{
  public float m_AnimRotateTime = 0.25f;
  private List<FriendListItemHeader.ToggleContentsListener> m_ToggleEventListeners = new List<FriendListItemHeader.ToggleContentsListener>();
  private bool m_ShowContents = true;
  public UberText m_Text;
  public GameObject m_Arrow;
  public Transform m_FoldinBone;
  public Transform m_FoldoutBone;
  private MultiSliceElement m_multiSlice;

  public GameObject Background { get; set; }

  public Bounds LocalBounds { get; private set; }

  public bool IsHeader
  {
    get
    {
      return true;
    }
  }

  public bool Visible
  {
    get
    {
      return this.IsShowingContents;
    }
    set
    {
    }
  }

  public bool IsShowingContents
  {
    get
    {
      return this.m_ShowContents;
    }
  }

  public MobileFriendListItem.TypeFlags SubType { get; set; }

  public Option Option { get; set; }

  public void SetText(string text)
  {
    this.m_Text.Text = text;
  }

  public void SetInitialShowContents(bool show)
  {
    this.m_ShowContents = show;
    this.m_Arrow.transform.rotation = this.GetCurrentBoneTransform().rotation;
  }

  public void AddToggleListener(FriendListItemHeader.ToggleContentsFunc func, object userdata)
  {
    FriendListItemHeader.ToggleContentsListener contentsListener = new FriendListItemHeader.ToggleContentsListener();
    contentsListener.SetCallback(func);
    contentsListener.SetUserData(userdata);
    this.m_ToggleEventListeners.Add(contentsListener);
  }

  public void ClearToggleListeners()
  {
    this.m_ToggleEventListeners.Clear();
  }

  public new T GetComponent<T>() where T : Component
  {
    return base.GetComponent<T>();
  }

  protected override void Awake()
  {
    base.Awake();
    this.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnHeaderButtonReleased));
    if (!((Object) this.m_multiSlice == (Object) null))
      return;
    this.m_multiSlice = this.GetComponentInChildren<MultiSliceElement>();
    if (!(bool) ((Object) this.m_multiSlice))
      return;
    this.m_multiSlice.UpdateSlices();
  }

  private void OnHeaderButtonReleased(UIEvent e)
  {
    this.m_ShowContents = !this.m_ShowContents;
    foreach (FriendListItemHeader.ToggleContentsListener contentsListener in this.m_ToggleEventListeners.ToArray())
      contentsListener.Fire(this.m_ShowContents);
    this.UpdateFoldoutArrow();
  }

  private void UpdateFoldoutArrow()
  {
    if ((Object) this.m_Arrow == (Object) null || (Object) this.m_FoldinBone == (Object) null || (Object) this.m_FoldoutBone == (Object) null)
      return;
    iTween.RotateTo(this.m_Arrow, this.GetCurrentBoneTransform().rotation.eulerAngles, this.m_AnimRotateTime);
  }

  private Transform GetCurrentBoneTransform()
  {
    if (this.m_ShowContents)
      return this.m_FoldoutBone;
    return this.m_FoldinBone;
  }

  GameObject ITouchListItem.get_gameObject()
  {
    return this.gameObject;
  }

  Transform ITouchListItem.get_transform()
  {
    return this.transform;
  }

  private class ToggleContentsListener : EventListener<FriendListItemHeader.ToggleContentsFunc>
  {
    public void Fire(bool show)
    {
      this.m_callback(show, this.m_userData);
    }
  }

  public delegate void ToggleContentsFunc(bool show, object userdata);
}
