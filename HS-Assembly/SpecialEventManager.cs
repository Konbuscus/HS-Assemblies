﻿// Decompiled with JetBrains decompiler
// Type: SpecialEventManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class SpecialEventManager
{
  private Map<SpecialEventType, SpecialEventManager.EventTiming> m_eventTimings = new Map<SpecialEventType, SpecialEventManager.EventTiming>();
  private static SpecialEventManager s_instance;
  private HashSet<SpecialEventType> m_forcedActiveEvents;

  private SpecialEventManager()
  {
  }

  public static SpecialEventManager Get()
  {
    if (SpecialEventManager.s_instance == null)
      SpecialEventManager.s_instance = new SpecialEventManager();
    return SpecialEventManager.s_instance;
  }

  public void InitEventTiming(IList<SpecialEventTiming> serverEventTimingList)
  {
    if (this.m_eventTimings.Count > 0)
    {
      Debug.LogWarning((object) "SpecialEventManager.InitEventTiming(): m_eventTimings was not empty; clearing it first");
      this.m_eventTimings.Clear();
    }
    DateTime now = DateTime.Now;
    bool flag = false;
    for (int index = 0; index < serverEventTimingList.Count; ++index)
    {
      SpecialEventTiming serverEventTiming = serverEventTimingList[index];
      SpecialEventType key;
      try
      {
        key = EnumUtils.GetEnum<SpecialEventType>(serverEventTiming.Event);
      }
      catch (ArgumentException ex)
      {
        Error.AddDevWarning("GetEnum Error", ex.Message);
        flag = true;
        continue;
      }
      if (this.m_eventTimings.ContainsKey(key))
      {
        Debug.LogWarning((object) string.Format("SpecialEventManager.InitEventTiming duplicate entry for event {0} received", (object) key));
        flag = true;
      }
      else
      {
        DateTime? startTime = new DateTime?();
        if (serverEventTiming.HasStart)
          startTime = serverEventTiming.Start <= 0UL ? new DateTime?(now) : new DateTime?(now.AddSeconds((double) serverEventTiming.Start));
        DateTime? endTime = new DateTime?();
        if (serverEventTiming.HasEnd)
          endTime = serverEventTiming.End <= 0UL ? new DateTime?(now) : new DateTime?(now.AddSeconds((double) serverEventTiming.End));
        this.m_eventTimings[key] = new SpecialEventManager.EventTiming(startTime, endTime);
      }
    }
    if (!flag || !ApplicationMgr.IsInternal())
      return;
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < serverEventTimingList.Count; ++index)
    {
      SpecialEventTiming serverEventTiming = serverEventTimingList[index];
      stringBuilder.Append("\n   serverEvent=").Append(serverEventTiming.Event);
      stringBuilder.Append(" start=").Append(!serverEventTiming.HasStart ? "null" : serverEventTiming.Start.ToString());
      stringBuilder.Append(" end=").Append(!serverEventTiming.HasEnd ? "null" : serverEventTiming.End.ToString());
    }
    using (Map<SpecialEventType, SpecialEventManager.EventTiming>.KeyCollection.Enumerator enumerator = this.m_eventTimings.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpecialEventType current = enumerator.Current;
        SpecialEventManager.EventTiming eventTiming = this.m_eventTimings[current];
        stringBuilder.Append("\n   mgrEvent=").Append((object) current);
        stringBuilder.Append(" start=").Append(!eventTiming.StartTime.HasValue ? "none" : eventTiming.StartTime.Value.ToString());
        stringBuilder.Append(" end=").Append(!eventTiming.EndTime.HasValue ? "none" : eventTiming.EndTime.Value.ToString());
      }
    }
    Debug.LogWarning((object) string.Format("EventTiming dump: {0}", (object) stringBuilder.ToString()));
  }

  public DateTime? GetEventLocalStartTime(SpecialEventType eventType)
  {
    if (!this.m_eventTimings.ContainsKey(eventType))
      return new DateTime?();
    return this.m_eventTimings[eventType].StartTime;
  }

  public DateTime? GetEventLocalEndTime(SpecialEventType eventType)
  {
    if (!this.m_eventTimings.ContainsKey(eventType))
      return new DateTime?();
    return this.m_eventTimings[eventType].EndTime;
  }

  public bool HasEventStarted(SpecialEventType eventType)
  {
    if (this.ForceEventActive(eventType))
      return true;
    if (!this.m_eventTimings.ContainsKey(eventType))
      return false;
    return this.m_eventTimings[eventType].HasStarted();
  }

  public bool HasEventEnded(SpecialEventType eventType)
  {
    if (this.ForceEventActive(eventType))
      return false;
    if (!this.m_eventTimings.ContainsKey(eventType))
      return true;
    return this.m_eventTimings[eventType].HasEnded();
  }

  public bool IsEventActive(SpecialEventType eventType, bool activeIfDoesNotExist)
  {
    if (this.ForceEventActive(eventType))
      return true;
    if (!this.m_eventTimings.ContainsKey(eventType))
      return activeIfDoesNotExist;
    return this.m_eventTimings[eventType].IsActiveNow();
  }

  public bool IsEventActive(string eventName, bool activeIfDoesNotExist)
  {
    if ("always" == eventName)
      return true;
    SpecialEventType eventType = SpecialEventManager.GetEventType(eventName, SpecialEventType.UNKNOWN);
    if (eventType != SpecialEventType.UNKNOWN)
      return this.IsEventActive(eventType, activeIfDoesNotExist);
    Debug.LogWarning((object) string.Format("SpecialEventManager.IsEventActive could not find SpecialEventType record for event '{0}'", (object) eventName));
    return activeIfDoesNotExist;
  }

  public static SpecialEventType GetEventType(string eventName, SpecialEventType defaultIfNotExists = SpecialEventType.UNKNOWN)
  {
    SpecialEventType outVal;
    if (EnumUtils.TryGetEnum<SpecialEventType>(eventName, out outVal))
      return outVal;
    Debug.LogWarning((object) string.Format("SpecialEventManager.GetEventType could not find SpecialEventType record for event '{0}'", (object) eventName));
    return defaultIfNotExists;
  }

  private void OnReset()
  {
    this.m_eventTimings.Clear();
    this.m_forcedActiveEvents.Clear();
  }

  private bool ForceEventActive(SpecialEventType eventType)
  {
    if (!ApplicationMgr.IsInternal())
      return false;
    if (this.m_forcedActiveEvents == null)
    {
      this.m_forcedActiveEvents = new HashSet<SpecialEventType>();
      string str1 = Vars.Key("Events.ForceActive").GetStr((string) null);
      if (string.IsNullOrEmpty(str1))
        return false;
      string str2 = str1;
      char[] separator = new char[3]{ ' ', ',', ';' };
      int num = 1;
      foreach (string str3 in str2.Split(separator, (StringSplitOptions) num))
      {
        SpecialEventType result;
        if (EnumUtils.TryGetEnum<SpecialEventType>(str3, StringComparison.OrdinalIgnoreCase, out result))
          this.m_forcedActiveEvents.Add(result);
      }
    }
    return this.m_forcedActiveEvents.Contains(eventType);
  }

  private class EventTiming
  {
    public DateTime? StartTime { get; private set; }

    public DateTime? EndTime { get; private set; }

    public EventTiming(DateTime? startTime, DateTime? endTime)
    {
      this.StartTime = startTime;
      this.EndTime = endTime;
    }

    public bool HasStarted()
    {
      if (!this.StartTime.HasValue)
        return true;
      return DateTime.Now >= this.StartTime.Value;
    }

    public bool HasEnded()
    {
      if (!this.EndTime.HasValue)
        return false;
      return DateTime.Now > this.EndTime.Value;
    }

    public bool IsActiveNow()
    {
      if (this.StartTime.HasValue && this.EndTime.HasValue && this.EndTime.Value < this.StartTime.Value || !this.HasStarted())
        return false;
      return !this.HasEnded();
    }
  }
}
