﻿// Decompiled with JetBrains decompiler
// Type: VectorUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public static class VectorUtils
{
  public static Vector2 Abs(Vector2 vector)
  {
    return new Vector2(Mathf.Abs(vector.x), Mathf.Abs(vector.y));
  }

  public static Vector2 CreateFromAngle(float degrees)
  {
    float f = (float) Math.PI / 180f * degrees;
    return new Vector2(Mathf.Cos(f), Mathf.Sin(f));
  }

  public static Vector3 Abs(Vector3 vector)
  {
    return new Vector3(Mathf.Abs(vector.x), Mathf.Abs(vector.y), Mathf.Abs(vector.z));
  }
}
