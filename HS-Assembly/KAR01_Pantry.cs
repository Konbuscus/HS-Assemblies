﻿// Decompiled with JetBrains decompiler
// Type: KAR01_Pantry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR01_Pantry : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_SilverwareGolem_Male_SilverwareGolem_SilverwareEmoteResponse_01");
    this.PreloadSound("VO_SilverwareGolem_Male_SilverwareGolem_SilverwareKnifeJuggler_01");
    this.PreloadSound("VO_Moroes_Male_Human_SilverwareResponse_01");
    this.PreloadSound("VO_Moroes_Male_Human_SilverwareTurn3_02");
    this.PreloadSound("VO_SilverwareGolem_Male_SilverwareGolem_SilverwareForkedLightning_01");
    this.PreloadSound("VO_Moroes_Male_Human_MedivhSkinResponse_01");
    this.PreloadSound("VO_Moroes_Male_Human_SilverwareWin_01");
    this.PreloadSound("VO_SilverwareGolem_Male_SilverwareGolem_SilverwarePlateTossing_01");
    this.PreloadSound("VO_SilverwareGolem_Male_SilverwareGolem_SilverwareHeroPower_01");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_SilverwareGolem_Male_SilverwareGolem_SilverwareEmoteResponse_01",
            m_stringTag = "VO_SilverwareGolem_Male_SilverwareGolem_SilverwareEmoteResponse_01"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR01_Pantry.\u003CHandleMissionEventWithTiming\u003Ec__Iterator18C() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR01_Pantry.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator18D() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR01_Pantry.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator18E() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR01_Pantry.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator18F() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR01_Pantry.\u003CHandleGameOverWithTiming\u003Ec__Iterator190() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
