﻿// Decompiled with JetBrains decompiler
// Type: UnityUrlDownloader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections.Generic;
using UnityEngine;

public class UnityUrlDownloader : IUrlDownloader
{
  private HashSet<UnityUrlDownloader.DownloadState> m_downloadsToStart = new HashSet<UnityUrlDownloader.DownloadState>();
  private HashSet<UnityUrlDownloader.DownloadState> m_downloadsRunning = new HashSet<UnityUrlDownloader.DownloadState>();
  private HashSet<UnityUrlDownloader.DownloadState> m_downloadsDone = new HashSet<UnityUrlDownloader.DownloadState>();

  public void Process()
  {
    using (HashSet<UnityUrlDownloader.DownloadState>.Enumerator enumerator = this.m_downloadsToStart.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UnityUrlDownloader.DownloadState current = enumerator.Current;
        current.startTime = Time.realtimeSinceStartup;
        current.handle = new WWW(current.url);
        this.m_downloadsRunning.Add(current);
      }
    }
    this.m_downloadsToStart.Clear();
    if (this.m_downloadsRunning.Count <= 0)
      return;
    HashSet<UnityUrlDownloader.DownloadState> downloadStateSet = (HashSet<UnityUrlDownloader.DownloadState>) null;
    using (HashSet<UnityUrlDownloader.DownloadState>.Enumerator enumerator = this.m_downloadsRunning.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UnityUrlDownloader.DownloadState current = enumerator.Current;
        bool flag = false;
        if (current.handle.isDone)
        {
          current.success = string.IsNullOrEmpty(current.handle.error);
          flag = true;
        }
        else if (current.timeoutMs >= 0 && (double) (Time.realtimeSinceStartup - current.startTime) > (double) current.timeoutMs / 1000.0)
        {
          current.success = false;
          flag = true;
        }
        if (flag)
        {
          if (downloadStateSet == null)
            downloadStateSet = new HashSet<UnityUrlDownloader.DownloadState>();
          downloadStateSet.Add(current);
        }
      }
    }
    if (downloadStateSet != null)
    {
      using (HashSet<UnityUrlDownloader.DownloadState>.Enumerator enumerator = downloadStateSet.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          UnityUrlDownloader.DownloadState current = enumerator.Current;
          this.m_downloadsRunning.Remove(current);
          this.m_downloadsDone.Add(current);
        }
      }
    }
    using (HashSet<UnityUrlDownloader.DownloadState>.Enumerator enumerator = this.m_downloadsDone.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UnityUrlDownloader.DownloadState current = enumerator.Current;
        if (!current.success && current.numRetriesLeft > 0)
        {
          --current.numRetriesLeft;
          this.m_downloadsToStart.Add(current);
        }
        else if (current.cb != null)
          current.cb(current.success, current.handle.bytes);
      }
    }
    this.m_downloadsDone.Clear();
  }

  public void Download(string url, UrlDownloadCompletedCallback cb)
  {
    UrlDownloaderConfig config = new UrlDownloaderConfig();
    this.Download(url, cb, config);
  }

  public void Download(string url, UrlDownloadCompletedCallback cb, UrlDownloaderConfig config)
  {
    this.m_downloadsToStart.Add(new UnityUrlDownloader.DownloadState()
    {
      url = url,
      timeoutMs = config.timeoutMs,
      numRetriesLeft = config.numRetries,
      cb = cb
    });
  }

  internal class DownloadState
  {
    public int timeoutMs = -1;
    public string url;
    public int numRetriesLeft;
    public WWW handle;
    public bool success;
    public UrlDownloadCompletedCallback cb;
    public float startTime;
  }
}
