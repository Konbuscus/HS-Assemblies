﻿// Decompiled with JetBrains decompiler
// Type: RewardUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RewardUtils
{
  public static readonly Vector3 REWARD_HIDDEN_SCALE = new Vector3(1f / 1000f, 1f / 1000f, 1f / 1000f);
  public static readonly float REWARD_HIDE_TIME = 0.25f;

  public static List<RewardData> GetRewards(List<NetCache.ProfileNotice> notices)
  {
    List<RewardData> existingRewardDataList = new List<RewardData>();
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = notices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.ProfileNotice current = enumerator.Current;
        RewardData newRewardData;
        switch (current.Type)
        {
          case NetCache.ProfileNotice.NoticeType.REWARD_BOOSTER:
            NetCache.ProfileNoticeRewardBooster noticeRewardBooster = current as NetCache.ProfileNoticeRewardBooster;
            newRewardData = (RewardData) new BoosterPackRewardData(noticeRewardBooster.Id, noticeRewardBooster.Count);
            break;
          case NetCache.ProfileNotice.NoticeType.REWARD_CARD:
            NetCache.ProfileNoticeRewardCard noticeRewardCard = current as NetCache.ProfileNoticeRewardCard;
            newRewardData = (RewardData) new CardRewardData(noticeRewardCard.CardID, noticeRewardCard.Premium, noticeRewardCard.Quantity);
            break;
          case NetCache.ProfileNotice.NoticeType.REWARD_DUST:
            newRewardData = (RewardData) new ArcaneDustRewardData((current as NetCache.ProfileNoticeRewardDust).Amount);
            break;
          case NetCache.ProfileNotice.NoticeType.REWARD_MOUNT:
            newRewardData = (RewardData) new MountRewardData((MountRewardData.MountType) (current as NetCache.ProfileNoticeRewardMount).MountID);
            break;
          case NetCache.ProfileNotice.NoticeType.REWARD_FORGE:
            newRewardData = (RewardData) new ForgeTicketRewardData((current as NetCache.ProfileNoticeRewardForge).Quantity);
            break;
          case NetCache.ProfileNotice.NoticeType.REWARD_GOLD:
            NetCache.ProfileNoticeRewardGold noticeRewardGold = current as NetCache.ProfileNoticeRewardGold;
            newRewardData = (RewardData) new GoldRewardData((long) noticeRewardGold.Amount, new DateTime?(DateTime.FromFileTimeUtc(noticeRewardGold.Date)));
            break;
          case NetCache.ProfileNotice.NoticeType.REWARD_CARD_BACK:
            newRewardData = (RewardData) new CardBackRewardData((current as NetCache.ProfileNoticeRewardCardBack).CardBackID);
            break;
          default:
            continue;
        }
        if (newRewardData != null)
        {
          newRewardData.SetOrigin(current.Origin, current.OriginData);
          newRewardData.AddNoticeID(current.NoticeID);
          RewardUtils.AddRewardDataToList(newRewardData, existingRewardDataList);
        }
      }
    }
    return existingRewardDataList;
  }

  public static void GetViewableRewards(List<RewardData> rewardDataList, HashSet<RewardVisualTiming> rewardTimings, ref List<RewardData> rewardsToShow, ref List<Achievement> completedQuests)
  {
    List<RewardData> outOfBandRewardsToShow = (List<RewardData>) null;
    RewardUtils.GetViewableRewards(rewardDataList, rewardTimings, ref rewardsToShow, ref outOfBandRewardsToShow, ref completedQuests);
  }

  public static void GetViewableRewards(List<RewardData> rewardDataList, HashSet<RewardVisualTiming> rewardTimings, ref List<RewardData> rewardsToShow, ref List<RewardData> outOfBandRewardsToShow, ref List<Achievement> completedQuests)
  {
    if (rewardsToShow == null)
      rewardsToShow = new List<RewardData>();
    if (completedQuests == null)
      completedQuests = new List<Achievement>();
    using (List<RewardData>.Enumerator enumerator1 = rewardDataList.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        RewardData current1 = enumerator1.Current;
        Log.Rachelle.Print("RewardUtils.GetViewableRewards() - processing reward {0}", (object) current1);
        if (current1.Origin == NetCache.ProfileNotice.NoticeOrigin.ACHIEVEMENT)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          RewardUtils.\u003CGetViewableRewards\u003Ec__AnonStorey3D3 rewardsCAnonStorey3D3 = new RewardUtils.\u003CGetViewableRewards\u003Ec__AnonStorey3D3();
          // ISSUE: reference to a compiler-generated field
          rewardsCAnonStorey3D3.completedQuest = AchieveManager.Get().GetAchievement((int) current1.OriginData);
          // ISSUE: reference to a compiler-generated field
          if (rewardsCAnonStorey3D3.completedQuest != null)
          {
            List<long> noticeIds = current1.GetNoticeIDs();
            // ISSUE: reference to a compiler-generated method
            Achievement achievement = completedQuests.Find(new Predicate<Achievement>(rewardsCAnonStorey3D3.\u003C\u003Em__186));
            if (achievement != null)
            {
              using (List<long>.Enumerator enumerator2 = noticeIds.GetEnumerator())
              {
                while (enumerator2.MoveNext())
                {
                  long current2 = enumerator2.Current;
                  achievement.AddRewardNoticeID(current2);
                }
              }
            }
            else
            {
              using (List<long>.Enumerator enumerator2 = noticeIds.GetEnumerator())
              {
                while (enumerator2.MoveNext())
                {
                  long current2 = enumerator2.Current;
                  // ISSUE: reference to a compiler-generated field
                  rewardsCAnonStorey3D3.completedQuest.AddRewardNoticeID(current2);
                }
              }
              // ISSUE: reference to a compiler-generated field
              if (rewardTimings.Contains(rewardsCAnonStorey3D3.completedQuest.RewardTiming))
              {
                // ISSUE: reference to a compiler-generated field
                completedQuests.Add(rewardsCAnonStorey3D3.completedQuest);
              }
            }
          }
        }
        else
        {
          bool flag1 = false;
          switch (current1.RewardType)
          {
            case Reward.Type.ARCANE_DUST:
            case Reward.Type.BOOSTER_PACK:
            case Reward.Type.GOLD:
              flag1 = true;
              break;
            case Reward.Type.CARD:
              CardRewardData cardReward = current1 as CardRewardData;
              if (cardReward.CardID.Equals("HERO_08") && cardReward.Premium == TAG_PREMIUM.NORMAL)
              {
                flag1 = false;
                current1.AcknowledgeNotices();
                CollectionManager.Get().AddCardReward(cardReward, false);
                break;
              }
              if (current1.Origin == NetCache.ProfileNotice.NoticeOrigin.FROM_PURCHASE)
              {
                flag1 = false;
                current1.AcknowledgeNotices();
                CollectionManager.Get().AddCardReward(cardReward, true);
                break;
              }
              if (current1.Origin == NetCache.ProfileNotice.NoticeOrigin.OUT_OF_BAND_LICENSE && outOfBandRewardsToShow != null)
              {
                flag1 = false;
                outOfBandRewardsToShow.Add(current1);
                break;
              }
              flag1 = true;
              break;
            case Reward.Type.CARD_BACK:
              flag1 = NetCache.ProfileNotice.NoticeOrigin.SEASON != current1.Origin;
              break;
            case Reward.Type.FORGE_TICKET:
              bool flag2 = false;
              if (current1.Origin == NetCache.ProfileNotice.NoticeOrigin.BLIZZCON && current1.OriginData == 2013L)
                flag2 = true;
              if (current1.Origin == NetCache.ProfileNotice.NoticeOrigin.OUT_OF_BAND_LICENSE)
              {
                Log.Rachelle.Print(string.Format("RewardUtils.GetViewableRewards(): auto-acking notices for out of band license reward {0}", (object) current1));
                flag2 = true;
              }
              if (flag2)
                current1.AcknowledgeNotices();
              flag1 = false;
              break;
          }
          if (flag1)
            rewardsToShow.Add(current1);
        }
      }
    }
  }

  public static void SortRewards(ref List<Reward> rewards)
  {
    if (rewards == null)
      return;
    rewards.Sort((Comparison<Reward>) ((r1, r2) =>
    {
      if (r1.RewardType == r2.RewardType)
      {
        if (r1.RewardType != Reward.Type.CARD)
          return 0;
        CardRewardData data1 = r1.Data as CardRewardData;
        CardRewardData data2 = r2.Data as CardRewardData;
        EntityDef entityDef1 = DefLoader.Get().GetEntityDef(data1.CardID);
        EntityDef entityDef2 = DefLoader.Get().GetEntityDef(data2.CardID);
        bool flag1 = TAG_CARDTYPE.HERO == entityDef1.GetCardType();
        bool flag2 = TAG_CARDTYPE.HERO == entityDef2.GetCardType();
        if (flag1 == flag2)
          return 0;
        return flag1 ? -1 : 1;
      }
      if (r1.RewardType == Reward.Type.CARD_BACK)
        return -1;
      if (r2.RewardType == Reward.Type.CARD_BACK)
        return 1;
      if (r1.RewardType == Reward.Type.CARD)
        return -1;
      if (r2.RewardType == Reward.Type.CARD)
        return 1;
      if (r1.RewardType == Reward.Type.BOOSTER_PACK)
        return -1;
      if (r2.RewardType == Reward.Type.BOOSTER_PACK)
        return 1;
      if (r1.RewardType == Reward.Type.MOUNT)
        return -1;
      return r2.RewardType == Reward.Type.MOUNT ? 1 : 0;
    }));
  }

  public static void AddRewardDataToList(RewardData newRewardData, List<RewardData> existingRewardDataList)
  {
    CardRewardData duplicateCardDataReward = RewardUtils.GetDuplicateCardDataReward(newRewardData, existingRewardDataList);
    if (duplicateCardDataReward == null)
    {
      existingRewardDataList.Add(newRewardData);
    }
    else
    {
      CardRewardData other = newRewardData as CardRewardData;
      duplicateCardDataReward.Merge(other);
    }
  }

  public static string GetRewardText(RewardData rewardData)
  {
    string str;
    switch (rewardData.RewardType)
    {
      case Reward.Type.ARCANE_DUST:
        str = GameStrings.Format("GLOBAL_HERO_LEVEL_REWARD_ARCANE_DUST", (object) (rewardData as ArcaneDustRewardData).Amount);
        break;
      case Reward.Type.BOOSTER_PACK:
        BoosterPackRewardData boosterPackRewardData = rewardData as BoosterPackRewardData;
        str = GameStrings.Format("GLOBAL_HERO_LEVEL_REWARD_BOOSTER", (object) (string) GameDbf.Booster.GetRecord(boosterPackRewardData.Id).Name);
        break;
      case Reward.Type.CARD:
        CardRewardData cardRewardData = rewardData as CardRewardData;
        EntityDef entityDef = DefLoader.Get().GetEntityDef(cardRewardData.CardID);
        if (cardRewardData.Premium == TAG_PREMIUM.GOLDEN)
        {
          str = GameStrings.Format("GLOBAL_HERO_LEVEL_REWARD_GOLDEN_CARD", (object) GameStrings.Get("GLOBAL_COLLECTION_GOLDEN"), (object) entityDef.GetName());
          break;
        }
        str = entityDef.GetName();
        break;
      case Reward.Type.GOLD:
        str = GameStrings.Format("GLOBAL_HERO_LEVEL_REWARD_GOLD", (object) (rewardData as GoldRewardData).Amount);
        break;
      default:
        str = "UNKNOWN";
        break;
    }
    return str;
  }

  public static bool ShowReward(UserAttentionBlocker blocker, Reward reward, bool updateCacheValues, Vector3 rewardPunchScale, Vector3 rewardScale, AnimationUtil.DelOnShownWithPunch callback, object callbackData)
  {
    return RewardUtils.ShowReward_Internal(blocker, reward, updateCacheValues, rewardPunchScale, rewardScale, string.Empty, (GameObject) null, callback, callbackData);
  }

  public static bool ShowReward(UserAttentionBlocker blocker, Reward reward, bool updateCacheValues, Vector3 rewardPunchScale, Vector3 rewardScale, string callbackName = "", object callbackData = null, GameObject callbackGO = null)
  {
    return RewardUtils.ShowReward_Internal(blocker, reward, updateCacheValues, rewardPunchScale, rewardScale, callbackName, callbackGO, (AnimationUtil.DelOnShownWithPunch) null, callbackData);
  }

  public static void SetRewardMaterialOffset(List<RewardData> rewards, Material rewardMaterial, UberText rewardAmountLabel, out float amountToScaleReward)
  {
    UnityEngine.Vector2 vector2 = UnityEngine.Vector2.zero;
    amountToScaleReward = 1f;
    rewardAmountLabel.gameObject.SetActive(false);
    RewardData reward = rewards[0];
    switch (reward.RewardType)
    {
      case Reward.Type.BOOSTER_PACK:
        BoosterPackRewardData boosterPackRewardData = reward as BoosterPackRewardData;
        vector2 = boosterPackRewardData.Id != 11 || boosterPackRewardData.Count <= 1 ? (boosterPackRewardData.Id != 1 || boosterPackRewardData.Count < 3 ? (boosterPackRewardData.Id != 19 ? new UnityEngine.Vector2(0.0f, 0.75f) : new UnityEngine.Vector2(0.5f, 0.5f)) : new UnityEngine.Vector2(0.25f, 0.5f)) : new UnityEngine.Vector2(0.0f, 0.5f);
        break;
      case Reward.Type.CARD:
        vector2 = new UnityEngine.Vector2(0.5f, 0.0f);
        break;
      case Reward.Type.FORGE_TICKET:
        vector2 = new UnityEngine.Vector2(0.75f, 0.75f);
        amountToScaleReward = 1.46881f;
        break;
      case Reward.Type.GOLD:
        vector2 = new UnityEngine.Vector2(0.25f, 0.75f);
        GoldRewardData goldRewardData = (GoldRewardData) reward;
        rewardAmountLabel.Text = goldRewardData.Amount.ToString();
        rewardAmountLabel.gameObject.SetActive(true);
        break;
      case Reward.Type.MOUNT:
        long num = 0;
        if (rewards.Count > 1 && rewards[1].RewardType == Reward.Type.GOLD)
          num = (rewards[1] as GoldRewardData).Amount;
        vector2 = new UnityEngine.Vector2(0.25f, 0.75f);
        rewardAmountLabel.Text = num.ToString();
        rewardAmountLabel.gameObject.SetActive(true);
        break;
    }
    rewardMaterial.mainTextureOffset = vector2;
  }

  public static void ShowTavernBrawlRewards(int wins, List<RewardData> rewards, Transform rewardBone, Action doneCallback, bool fromNotice = false, long noticeID = -1)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    AssetLoader.Get().LoadGameObject("HeroicBrawlReward", new AssetLoader.GameObjectCallback(new RewardUtils.\u003CShowTavernBrawlRewards\u003Ec__AnonStorey3D4()
    {
      doneCallback = doneCallback,
      rewardBone = rewardBone,
      wins = wins,
      rewards = rewards,
      fromNotice = fromNotice,
      noticeID = noticeID
    }.\u003C\u003Em__188), (object) null, false);
  }

  public static PegasusShared.RewardChest GenerateTavernBrawlRewardChest_CHEAT(int wins)
  {
    PegasusShared.RewardChest rewardChest = new PegasusShared.RewardChest();
    RewardBag rewardBag1 = new RewardBag();
    rewardBag1.RewardBooster = new PegasusShared.ProfileNoticeRewardBooster();
    rewardBag1.RewardBooster.BoosterType = 1;
    int num1 = 0;
    int num2 = 0;
    switch (wins)
    {
      case 0:
        num1 = 1;
        break;
      case 1:
        num1 = 2;
        break;
      case 2:
        num1 = 4;
        break;
      case 3:
        num1 = 4;
        num2 = 120;
        break;
      case 4:
        num1 = 5;
        num2 = 230;
        break;
      case 5:
        num1 = 6;
        num2 = 260;
        break;
      case 6:
        num1 = 7;
        num2 = 290;
        break;
      case 7:
        num1 = 8;
        num2 = 320;
        break;
      case 8:
        num1 = 9;
        num2 = 350;
        break;
      case 9:
        num1 = 14;
        num2 = 500;
        break;
      case 10:
        num1 = 15;
        num2 = 550;
        break;
      case 11:
        num1 = 20;
        num2 = 600;
        break;
      case 12:
        num1 = 50;
        num2 = 1000;
        break;
    }
    rewardBag1.RewardBooster.BoosterCount = num1;
    rewardChest.Bag.Add(rewardBag1);
    if (wins > 2)
    {
      RewardBag rewardBag2 = new RewardBag();
      rewardBag2.RewardDust = new PegasusShared.ProfileNoticeRewardDust();
      rewardBag2.RewardDust.Amount = num2 + UnityEngine.Random.Range(-4, 4) * 5;
      RewardBag rewardBag3 = new RewardBag();
      rewardBag3.RewardGold = new PegasusShared.ProfileNoticeRewardGold();
      rewardBag3.RewardGold.Amount = num2 + UnityEngine.Random.Range(-4, 4) * 5;
      rewardChest.Bag.Add(rewardBag3);
      rewardChest.Bag.Add(rewardBag2);
    }
    if (wins > 9)
    {
      RewardBag rewardBag2 = new RewardBag();
      rewardBag2.RewardCard = new PegasusShared.ProfileNoticeRewardCard();
      rewardBag2.RewardCard.Card = new PegasusShared.CardDef();
      rewardBag2.RewardCard.Card.Premium = 1;
      rewardBag2.RewardCard.Card.Asset = 834;
      rewardChest.Bag.Add(rewardBag2);
    }
    if (wins > 10)
    {
      RewardBag rewardBag2 = new RewardBag();
      rewardBag2.RewardCard = new PegasusShared.ProfileNoticeRewardCard();
      rewardBag2.RewardCard.Card = new PegasusShared.CardDef();
      rewardBag2.RewardCard.Card.Premium = 1;
      rewardBag2.RewardCard.Card.Asset = 374;
      rewardChest.Bag.Add(rewardBag2);
    }
    if (wins > 11)
    {
      RewardBag rewardBag2 = new RewardBag();
      rewardBag2.RewardCard = new PegasusShared.ProfileNoticeRewardCard();
      rewardBag2.RewardCard.Card = new PegasusShared.CardDef();
      rewardBag2.RewardCard.Card.Premium = 1;
      rewardBag2.RewardCard.Card.Asset = 640;
      rewardChest.Bag.Add(rewardBag2);
    }
    return rewardChest;
  }

  public static void SetQuestTileNameLinePosition(GameObject nameLine, UberText questName, float padding)
  {
    bool flag = questName.isHidden();
    if (flag)
      questName.Show();
    TransformUtil.SetPoint(nameLine, Anchor.TOP, (Component) questName, Anchor.BOTTOM);
    nameLine.transform.localPosition = new Vector3(nameLine.transform.localPosition.x, nameLine.transform.localPosition.y, nameLine.transform.localPosition.z + padding);
    if (!flag)
      return;
    questName.Hide();
  }

  private static bool ShowReward_Internal(UserAttentionBlocker blocker, Reward reward, bool updateCacheValues, Vector3 rewardPunchScale, Vector3 rewardScale, string gameObjectCallbackName, GameObject callbackGO, AnimationUtil.DelOnShownWithPunch onShowPunchCallback, object callbackData)
  {
    if ((UnityEngine.Object) reward == (UnityEngine.Object) null)
      return false;
    int num = (int) blocker;
    string str1 = "RewardUtils.ShowReward:";
    string str2;
    if ((UnityEngine.Object) reward == (UnityEngine.Object) null || reward.Data == null)
      str2 = "null";
    else
      str2 = ((int) reward.Data.Origin).ToString() + ":" + (object) reward.Data.OriginData + ":" + (object) reward.Data.RewardType;
    string callerName = str1 + str2;
    if (!UserAttentionManager.CanShowAttentionGrabber((UserAttentionBlocker) num, callerName))
      return false;
    Log.Achievements.Print("RewardUtils: Showing Reward: reward={0} reward.Data={1}", new object[2]
    {
      (object) reward,
      (object) reward.Data
    });
    RenderUtils.SetAlpha(reward.gameObject, 0.0f);
    AnimationUtil.ShowWithPunch(reward.gameObject, RewardUtils.REWARD_HIDDEN_SCALE, rewardPunchScale, rewardScale, gameObjectCallbackName, false, callbackGO, callbackData, onShowPunchCallback);
    reward.Show(updateCacheValues);
    RewardUtils.ShowInnkeeperQuoteForReward(reward);
    return true;
  }

  private static CardRewardData GetDuplicateCardDataReward(RewardData newRewardData, List<RewardData> existingRewardData)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    RewardUtils.\u003CGetDuplicateCardDataReward\u003Ec__AnonStorey3D5 rewardCAnonStorey3D5 = new RewardUtils.\u003CGetDuplicateCardDataReward\u003Ec__AnonStorey3D5();
    if (!(newRewardData is CardRewardData))
      return (CardRewardData) null;
    // ISSUE: reference to a compiler-generated field
    rewardCAnonStorey3D5.newCardRewardData = newRewardData as CardRewardData;
    // ISSUE: reference to a compiler-generated method
    return existingRewardData.Find(new Predicate<RewardData>(rewardCAnonStorey3D5.\u003C\u003Em__189)) as CardRewardData;
  }

  private static void ShowInnkeeperQuoteForReward(Reward reward)
  {
    if ((UnityEngine.Object) reward == (UnityEngine.Object) null || reward.RewardType != Reward.Type.CARD)
      return;
    switch ((reward.Data as CardRewardData).InnKeeperLine)
    {
      case CardRewardData.InnKeeperTrigger.CORE_CLASS_SET_COMPLETE:
        Notification innkeeperQuote = NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_BASIC_DONE1_11"), "VO_INNKEEPER_BASIC_DONE1_11", 0.0f, (Action) null, false);
        if (Options.Get().GetBool(Option.HAS_SEEN_ALL_BASIC_CLASS_CARDS_COMPLETE, false))
          break;
        SceneMgr.Get().StartCoroutine(RewardUtils.NotifyOfExpertPacksNeeded(innkeeperQuote));
        break;
      case CardRewardData.InnKeeperTrigger.SECOND_REWARD_EVER:
        if (Options.Get().GetBool(Option.HAS_BEEN_NUDGED_TO_CM, false))
          break;
        NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_NUDGE_CM_X"), "VO_INNKEEPER2_NUDGE_COLLECTION_10", 0.0f, (Action) null, false);
        Options.Get().SetBool(Option.HAS_BEEN_NUDGED_TO_CM, true);
        break;
    }
  }

  [DebuggerHidden]
  private static IEnumerator NotifyOfExpertPacksNeeded(Notification innkeeperQuote)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardUtils.\u003CNotifyOfExpertPacksNeeded\u003Ec__Iterator240() { innkeeperQuote = innkeeperQuote, \u003C\u0024\u003EinnkeeperQuote = innkeeperQuote };
  }
}
