﻿// Decompiled with JetBrains decompiler
// Type: ClassFilterButtonContainer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ClassFilterButtonContainer : MonoBehaviour
{
  public int m_rowSize = 5;
  public TAG_CLASS[] m_classTags;
  public ClassFilterButton[] m_classButtons;
  public Material[] m_classMaterials;
  public Material m_inactiveMaterial;
  public Material m_templateMaterial;
  public PegUIElement m_cardBacksButton;
  public PegUIElement m_heroSkinsButton;
  public GameObject m_cardBacksDisabled;
  public GameObject m_heroSkinsDisabled;
  private int m_neutralIndex;

  public void Awake()
  {
    this.m_neutralIndex = this.GetIndex(TAG_CLASS.NEUTRAL);
  }

  public int GetNumVisibleClasses()
  {
    int num = 0;
    CollectionPageManager pageManager = CollectionManagerDisplay.Get().m_pageManager;
    for (int index = 0; index < this.m_classTags.Length; ++index)
    {
      if (pageManager.GetNumPagesForClass(this.m_classTags[index]) > 0)
        ++num;
    }
    return num;
  }

  private void SetCardBacksEnabled(bool enabled)
  {
    this.m_cardBacksButton.SetEnabled(enabled);
    this.m_cardBacksDisabled.SetActive(!enabled);
  }

  private void SetHeroSkinsEnabled(bool enabled)
  {
    this.m_heroSkinsButton.SetEnabled(enabled);
    this.m_heroSkinsDisabled.SetActive(!enabled);
  }

  public void SetDefaults()
  {
    this.SetCardBacksEnabled(true);
    this.SetHeroSkinsEnabled(true);
    for (int index = 0; index < this.m_classTags.Length; ++index)
      this.m_classButtons[index].SetClass(new TAG_CLASS?(), this.m_inactiveMaterial);
    CollectionPageManager pageManager = CollectionManagerDisplay.Get().m_pageManager;
    int index1 = 0;
    for (int index2 = 0; index2 < this.m_classTags.Length; ++index2)
    {
      if (pageManager.GetNumPagesForClass(this.m_classTags[index2]) > 0)
      {
        this.m_classButtons[index1].SetClass(new TAG_CLASS?(this.m_classTags[index2]), this.m_classMaterials[index2]);
        int newCardsForClass = CollectionManagerDisplay.Get().m_pageManager.GetNumNewCardsForClass(this.m_classTags[index2]);
        this.m_classButtons[index1].SetNewCardCount(newCardsForClass);
        ++index1;
      }
    }
  }

  public void SetClass(TAG_CLASS classTag)
  {
    int count1 = CardBackManager.Get().GetCardBacksOwned().Count;
    int count2 = CollectionManager.Get().GetBestHeroesIOwn(classTag).Count;
    this.SetCardBacksEnabled(count1 > 1);
    this.SetHeroSkinsEnabled(count2 > 1);
    int index1 = this.GetIndex(classTag);
    for (int index2 = 0; index2 < this.m_classTags.Length; ++index2)
    {
      this.m_classButtons[index2].SetClass(new TAG_CLASS?(), this.m_inactiveMaterial);
      this.m_classButtons[index2].SetNewCardCount(0);
    }
    this.m_classButtons[0].SetClass(new TAG_CLASS?(classTag), this.m_classMaterials[index1]);
    this.m_classButtons[0].SetNewCardCount(CollectionManagerDisplay.Get().m_pageManager.GetNumNewCardsForClass(classTag));
    this.m_classButtons[1].SetClass(new TAG_CLASS?(TAG_CLASS.NEUTRAL), this.m_classMaterials[this.m_neutralIndex]);
    this.m_classButtons[1].SetNewCardCount(CollectionManagerDisplay.Get().m_pageManager.GetNumNewCardsForClass(TAG_CLASS.NEUTRAL));
  }

  private int GetIndex(TAG_CLASS classTag)
  {
    int num = 0;
    for (int index = 0; index < this.m_classTags.Length; ++index)
    {
      if (this.m_classTags[index] == classTag)
      {
        num = index;
        break;
      }
    }
    return num;
  }
}
