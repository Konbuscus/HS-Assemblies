﻿// Decompiled with JetBrains decompiler
// Type: GeneralStoreHeroesContentLite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class GeneralStoreHeroesContentLite : MonoBehaviour
{
  public string m_keyArtFadeAnim = "HeroSkinArt_WipeAway";
  public string m_keyArtAppearAnim = "HeroSkinArtGlowIn";
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_keyArtFadeSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_keyArtAppearSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_previewButtonClick;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_backgroundFlipSound;
  public MeshRenderer m_renderQuad;
  public GameObject m_renderToTexture;
}
