﻿// Decompiled with JetBrains decompiler
// Type: VS
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class VS : MonoBehaviour
{
  public GameObject m_shadow;

  private void Start()
  {
    this.SetDefaults();
  }

  private void OnDestroy()
  {
    this.SetDefaults();
  }

  private void SetDefaults()
  {
    this.ActivateShadow(false);
  }

  public void ActivateShadow(bool active = true)
  {
    this.m_shadow.SetActive(active);
  }

  public void ActivateAnimation(bool active = true)
  {
    this.gameObject.GetComponentInChildren<Animation>().enabled = active;
  }
}
