﻿// Decompiled with JetBrains decompiler
// Type: FriendListNearbyPlayerFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FriendListNearbyPlayerFrame : MonoBehaviour
{
  public UberText m_PlayerNameText;
  public FriendListChallengeButton m_ChallengeButton;
  public FriendListNearbyPlayerFrameBones m_Bones;
  public FriendListNearbyPlayerFrameOffsets m_Offsets;
  private Component[] m_rightComponentOrder;
  protected BnetPlayer m_player;

  protected virtual void Awake()
  {
    this.m_ChallengeButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnChallengeButtonReleased));
    this.m_rightComponentOrder = new Component[1]
    {
      (Component) this.m_ChallengeButton
    };
  }

  private void OnEnable()
  {
    this.UpdateNearbyPlayer();
  }

  private void OnChallengeButtonReleased(UIEvent e)
  {
    if (this.m_ChallengeButton.CanChallenge())
      return;
    FriendListFriendFrame.OnPlayerChallengeButtonPressed(this.m_ChallengeButton, this.m_player);
  }

  private void UpdateLayout()
  {
    Component component = (Component) this.m_Bones.m_RightComponent;
    for (int index = this.m_rightComponentOrder.Length - 1; index >= 0; --index)
    {
      Component src = this.m_rightComponentOrder[index];
      if (src.gameObject.activeSelf)
      {
        TransformUtil.SetPoint(src, Anchor.RIGHT, component, Anchor.LEFT, this.m_Offsets.m_RightComponent);
        component = src;
      }
    }
    this.LayoutLeftText(this.m_PlayerNameText, this.m_Bones.m_PlayerNameText, this.m_Offsets.m_PlayerNameText, component);
  }

  private void LayoutLeftText(UberText text, Transform bone, Vector3 offset, Component rightComponent)
  {
    if (!text.gameObject.activeInHierarchy)
      return;
    text.Width = this.ComputeLeftComponentWidth(bone, offset, rightComponent);
    TransformUtil.SetPoint((Component) text, Anchor.LEFT, (Component) bone, Anchor.RIGHT, offset);
  }

  private float ComputeLeftComponentWidth(Transform bone, Vector3 offset, Component rightComponent)
  {
    Vector3 vector3 = bone.position + offset;
    Bounds setPointBounds = TransformUtil.ComputeSetPointBounds(rightComponent);
    return setPointBounds.center.x - setPointBounds.extents.x + this.m_Offsets.m_RightComponent.x - vector3.x;
  }

  public BnetPlayer GetNearbyPlayer()
  {
    return this.m_player;
  }

  public virtual bool SetNearbyPlayer(BnetPlayer player)
  {
    if (this.m_player == player)
      return false;
    this.m_player = player;
    this.m_ChallengeButton.SetPlayer(player);
    this.UpdateNearbyPlayer();
    return true;
  }

  public virtual void UpdateNearbyPlayer()
  {
    if (!this.gameObject.activeSelf)
      return;
    if (this.m_player == null)
    {
      this.m_PlayerNameText.Text = string.Empty;
    }
    else
    {
      BnetPlayer friend = BnetFriendMgr.Get().FindFriend(this.m_player.GetAccountId());
      this.m_PlayerNameText.Text = friend == null ? FriendUtils.GetFriendListName(this.m_player, true) : FriendUtils.GetFriendListName(friend, true);
    }
    this.m_ChallengeButton.UpdateButton();
    this.UpdateLayout();
  }
}
