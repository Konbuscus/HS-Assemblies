﻿// Decompiled with JetBrains decompiler
// Type: ChatMgrBubbleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class ChatMgrBubbleInfo
{
  public float m_ScaleInSec = 1f;
  public iTween.EaseType m_ScaleInEaseType = iTween.EaseType.easeOutElastic;
  public float m_HoldSec = 7f;
  public float m_FadeOutSec = 2f;
  public iTween.EaseType m_FadeOutEaseType = iTween.EaseType.linear;
  public float m_MoveOverSec = 1f;
  public iTween.EaseType m_MoveOverEaseType = iTween.EaseType.easeOutExpo;
  public Transform m_Parent;
}
