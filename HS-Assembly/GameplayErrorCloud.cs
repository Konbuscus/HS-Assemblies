﻿// Decompiled with JetBrains decompiler
// Type: GameplayErrorCloud
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class GameplayErrorCloud : MonoBehaviour
{
  private readonly string START_COROUTINE_NAME = "StartHideMessageDelay";
  private const float ERROR_MESSAGE_DURATION = 2f;
  private const float ERROR_MESSAGE_FADEIN = 0.15f;
  private const float ERROR_MESSAGE_FADEOUT = 0.5f;
  public UberText m_errorText;
  public float initTime;
  public ParticleEmitter m_emitter;
  private float m_holdDuration;
  private Coroutine m_coroutine;

  private void Start()
  {
    RenderUtils.SetAlpha(this.gameObject, 0.0f);
    this.Hide();
  }

  public void Show()
  {
    this.m_emitter.gameObject.SetActive(true);
  }

  public void Hide()
  {
    this.m_coroutine = (Coroutine) null;
    this.m_emitter.gameObject.SetActive(false);
  }

  public void ShowMessage(string message, float timeToDisplay)
  {
    if (this.m_coroutine != null)
    {
      this.StopCoroutine(this.START_COROUTINE_NAME);
      this.Hide();
    }
    this.m_holdDuration = Mathf.Max(2f, timeToDisplay);
    ParticleEmitter emitter = this.m_emitter;
    float num1 = (float) (0.150000005960464 + (double) this.m_holdDuration * 1.39999997615814 + 0.5);
    this.m_emitter.minEnergy = num1;
    double num2 = (double) num1;
    emitter.maxEnergy = (float) num2;
    this.Show();
    this.m_errorText.Text = message;
    iTween.FadeTo(this.gameObject, iTween.Hash((object) "alpha", (object) 1f, (object) "time", (object) 0.15f));
    this.m_coroutine = this.StartCoroutine(this.START_COROUTINE_NAME);
  }

  public void HideMessage()
  {
    iTween.FadeTo(this.gameObject, iTween.Hash((object) "alpha", (object) 0.0f, (object) "time", (object) 0.5f, (object) "oncomplete", (object) "Hide"));
  }

  [DebuggerHidden]
  public IEnumerator StartHideMessageDelay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GameplayErrorCloud.\u003CStartHideMessageDelay\u003Ec__IteratorB4() { \u003C\u003Ef__this = this };
  }
}
