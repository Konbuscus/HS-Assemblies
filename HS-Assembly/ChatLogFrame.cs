﻿// Decompiled with JetBrains decompiler
// Type: ChatLogFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ChatLogFrame : MonoBehaviour
{
  public ChatLogFrameBones m_Bones;
  public ChatLogFramePrefabs m_Prefabs;
  public UberText m_NameText;
  public ChatLog m_chatLog;
  private PlayerIcon m_playerIcon;
  private BnetPlayer m_receiver;

  public BnetPlayer Receiver
  {
    get
    {
      return this.m_receiver;
    }
    set
    {
      if (this.m_receiver == value)
        return;
      this.m_receiver = value;
      if (this.m_receiver == null)
        return;
      this.m_playerIcon.SetPlayer(this.m_receiver);
      this.UpdateReceiver();
      this.m_chatLog.Receiver = this.m_receiver;
    }
  }

  private void Awake()
  {
    this.InitPlayerIcon();
  }

  private void Start()
  {
    this.UpdateLayout();
  }

  public void UpdateLayout()
  {
    this.OnResize();
  }

  private void InitPlayerIcon()
  {
    this.m_playerIcon = UnityEngine.Object.Instantiate<PlayerIcon>(this.m_Prefabs.m_PlayerIcon);
    this.m_playerIcon.transform.parent = this.transform;
    TransformUtil.CopyWorld((Component) this.m_playerIcon, (Component) this.m_Bones.m_PlayerIcon);
    SceneUtils.SetLayer((Component) this.m_playerIcon, this.gameObject.layer);
  }

  private void OnResize()
  {
    float viewWindowMaxValue = this.m_chatLog.messageFrames.ViewWindowMaxValue;
    this.m_chatLog.messageFrames.transform.position = (this.m_Bones.m_MessagesTopLeft.position + this.m_Bones.m_MessagesBottomRight.position) / 2f;
    Vector3 vector3 = this.m_Bones.m_MessagesBottomRight.localPosition - this.m_Bones.m_MessagesTopLeft.localPosition;
    this.m_chatLog.messageFrames.ClipSize = new Vector2(vector3.x, Math.Abs(vector3.y));
    this.m_chatLog.messageFrames.ViewWindowMaxValue = viewWindowMaxValue;
    this.m_chatLog.messageFrames.ScrollValue = Mathf.Clamp01(this.m_chatLog.messageFrames.ScrollValue);
    this.m_chatLog.OnResize();
  }

  private void UpdateReceiver()
  {
    this.m_playerIcon.UpdateIcon();
    this.m_NameText.Text = FriendUtils.GetUniqueNameWithColor(this.m_receiver);
  }
}
