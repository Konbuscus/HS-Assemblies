﻿// Decompiled with JetBrains decompiler
// Type: ShowAllCardsTab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ShowAllCardsTab : MonoBehaviour
{
  public CheckBox m_showAllCardsCheckBox;
  public CheckBox m_includePremiumsCheckBox;

  private void Awake()
  {
    this.m_showAllCardsCheckBox.SetButtonText(GameStrings.Get("GLUE_COLLECTION_SHOW_ALL_CARDS"));
    this.m_includePremiumsCheckBox.SetButtonText(GameStrings.Get("GLUE_COLLECTION_INCLUDE_PREMIUMS"));
  }

  private void Start()
  {
    this.m_includePremiumsCheckBox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ToggleIncludePremiums));
    this.m_showAllCardsCheckBox.SetChecked(false);
    this.m_includePremiumsCheckBox.SetChecked(false);
    this.m_includePremiumsCheckBox.gameObject.SetActive(false);
  }

  public bool IsShowAllChecked()
  {
    return this.m_showAllCardsCheckBox.IsChecked();
  }

  private void ToggleIncludePremiums(UIEvent e)
  {
    bool show = this.m_includePremiumsCheckBox.IsChecked();
    CollectionManagerDisplay.Get().ShowPremiumCardsNotOwned(show);
    if (show)
      SoundManager.Get().LoadAndPlay("checkbox_toggle_on", this.gameObject);
    else
      SoundManager.Get().LoadAndPlay("checkbox_toggle_off", this.gameObject);
  }
}
