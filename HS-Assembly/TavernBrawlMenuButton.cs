﻿// Decompiled with JetBrains decompiler
// Type: TavernBrawlMenuButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TavernBrawlMenuButton : BoxMenuButton
{
  public float m_hoverDelay = 0.5f;
  public UberText m_returnsInfo;
  private bool isPoppedUp;

  public override void TriggerOver()
  {
    if (!NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>().Games.TavernBrawl || !TavernBrawlManager.Get().HasUnlockedTavernBrawl || TavernBrawlManager.Get().IsTavernBrawlActive)
    {
      base.TriggerOver();
    }
    else
    {
      this.UpdateTimeText();
      this.StartCoroutine("DoPopup");
    }
  }

  [DebuggerHidden]
  public IEnumerator DoPopup()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlMenuButton.\u003CDoPopup\u003Ec__Iterator28A() { \u003C\u003Ef__this = this };
  }

  public override void TriggerOut()
  {
    if (!NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>().Games.TavernBrawl || !TavernBrawlManager.Get().HasUnlockedTavernBrawl || TavernBrawlManager.Get().IsTavernBrawlActive)
    {
      base.TriggerOut();
    }
    else
    {
      if (!UniversalInputManager.Get().IsTouchMode())
        this.StopCoroutine("DoPopup");
      if (!this.isPoppedUp)
        return;
      if (Box.Get().m_tavernBrawlPopdownSound != string.Empty)
        SoundManager.Get().LoadAndPlay(Box.Get().m_tavernBrawlPopdownSound);
      Box.Get().m_TavernBrawlButtonVisual.GetComponent<Animator>().Play("TavernBrawl_ButtonPopdown");
      this.isPoppedUp = false;
    }
  }

  public void ClearHighlightAndTooltip()
  {
    base.TriggerOut();
  }

  public override void TriggerPress()
  {
    if (!NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>().Games.TavernBrawl || !TavernBrawlManager.Get().HasUnlockedTavernBrawl || !TavernBrawlManager.Get().IsTavernBrawlActive)
      return;
    base.TriggerPress();
  }

  public override void TriggerRelease()
  {
    if (!NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>().Games.TavernBrawl || !TavernBrawlManager.Get().HasUnlockedTavernBrawl || !TavernBrawlManager.Get().IsTavernBrawlActive)
      return;
    base.TriggerRelease();
  }

  private void UpdateTimeText()
  {
    long seasonStartInSeconds = TavernBrawlManager.Get().NextTavernBrawlSeasonStartInSeconds;
    if (seasonStartInSeconds < 0L)
    {
      this.m_returnsInfo.Text = GameStrings.Get("GLUE_TAVERN_BRAWL_RETURNS_UNKNOWN");
    }
    else
    {
      TimeUtils.ElapsedStringSet stringSet = new TimeUtils.ElapsedStringSet() { m_seconds = "GLUE_TAVERN_BRAWL_RETURNS_LESS_THAN_1_HOUR", m_minutes = "GLUE_TAVERN_BRAWL_RETURNS_LESS_THAN_1_HOUR", m_hours = "GLUE_TAVERN_BRAWL_RETURNS_HOURS", m_yesterday = (string) null, m_days = "GLUE_TAVERN_BRAWL_RETURNS_DAYS", m_weeks = "GLUE_TAVERN_BRAWL_RETURNS_WEEKS", m_monthAgo = "GLUE_TAVERN_BRAWL_RETURNS_OVER_1_MONTH" };
      this.m_returnsInfo.Text = TimeUtils.GetElapsedTimeString(seasonStartInSeconds, stringSet);
    }
  }
}
