﻿// Decompiled with JetBrains decompiler
// Type: WebLoginCanvas
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;
using UnityEngine;

public class WebLoginCanvas : MonoBehaviour
{
  private static Map<constants.BnetRegion, string> s_regionStringNames = new Map<constants.BnetRegion, string>() { { constants.BnetRegion.REGION_UNKNOWN, "Cuba" }, { constants.BnetRegion.REGION_US, "GLOBAL_REGION_AMERICAS" }, { constants.BnetRegion.REGION_EU, "GLOBAL_REGION_EUROPE" }, { constants.BnetRegion.REGION_KR, "GLOBAL_REGION_ASIA" }, { constants.BnetRegion.REGION_TW, "Taiwan" }, { constants.BnetRegion.REGION_CN, "GLOBAL_REGION_CHINA" }, { constants.BnetRegion.REGION_LIVE_VERIFICATION, "LiveVerif" } };
  private float m_acFlipbookSwap = 30f;
  private PlatformDependentValue<bool> KOBOLD_SHOWN_ON_ACCOUNT_CREATION = new PlatformDependentValue<bool>(PlatformCategory.Screen) { PC = true, Tablet = true, Phone = false };
  private PlatformDependentValue<bool> USE_REGION_DROPDOWN = new PlatformDependentValue<bool>(PlatformCategory.Screen) { PC = true, Tablet = true, Phone = false };
  public DropdownControl m_regionSelectDropdownPrefab;
  public GameObject m_regionSelectDropdownBone;
  public GameObject m_regionSelectContents;
  public GameObject m_accountCreation;
  public AccountCreationFlipbook m_flipbook;
  public PegUIElement m_backButton;
  public GameObject m_regionSelectTooltipBone;
  public GameObject m_topLeftBone;
  public GameObject m_bottomRightBone;
  public UIBButton m_regionButton;
  private DropdownControl m_regionSelector;
  private object m_selection;
  private object m_prevSelection;
  private float m_acFlipbookCur;
  private TooltipPanel m_regionSelectTooltip;
  private bool m_canGoBack;
  private Map<constants.BnetRegion, string> m_regionNames;
  private RegionMenu m_regionMenu;

  private void Awake()
  {
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    bool flag = ApplicationMgr.GetMobileEnvironment() == MobileEnv.DEVELOPMENT;
    this.m_regionNames = new Map<constants.BnetRegion, string>();
    using (Map<constants.BnetRegion, string>.KeyCollection.Enumerator enumerator = WebLoginCanvas.s_regionStringNames.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        constants.BnetRegion current = enumerator.Current;
        if (flag)
          this.m_regionNames[current] = GameStrings.Get(WebLoginCanvas.s_regionStringNames[current]).Split(' ')[0];
        else
          this.m_regionNames[current] = GameStrings.Get(WebLoginCanvas.s_regionStringNames[current]);
      }
    }
    if ((bool) this.USE_REGION_DROPDOWN)
      this.SetUpRegionDropdown();
    else
      this.SetUpRegionButton();
    if ((bool) UniversalInputManager.UsePhoneUI && flag)
      this.SetUpRegionDropdown();
    this.m_backButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBackPressed));
    if (Options.Get().GetBool(Option.CONNECT_TO_AURORA))
      return;
    this.m_backButton.gameObject.SetActive(true);
  }

  private void Start()
  {
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  private void Update()
  {
    if (!(bool) this.KOBOLD_SHOWN_ON_ACCOUNT_CREATION)
      return;
    if ((double) this.m_acFlipbookCur < (double) this.m_acFlipbookSwap)
    {
      this.m_acFlipbookCur += Time.deltaTime * 60f;
    }
    else
    {
      if ((UnityEngine.Object) this.m_flipbook.m_acFlipbook.GetComponent<Renderer>().sharedMaterial.mainTexture == (UnityEngine.Object) this.m_flipbook.m_acFlipbookTextures[0])
      {
        this.m_flipbook.m_acFlipbook.GetComponent<Renderer>().sharedMaterial.mainTexture = this.m_flipbook.m_acFlipbookTextures[1];
        this.m_acFlipbookSwap = this.m_flipbook.m_acFlipbookTimeAlt;
      }
      else
      {
        this.m_flipbook.m_acFlipbook.GetComponent<Renderer>().sharedMaterial.mainTexture = this.m_flipbook.m_acFlipbookTextures[0];
        this.m_acFlipbookSwap = UnityEngine.Random.Range(this.m_flipbook.m_acFlipbookTimeMin, this.m_flipbook.m_acFlipbookTimeMax);
      }
      this.m_acFlipbookCur = 0.0f;
    }
  }

  private void OnDestroy()
  {
    if (!((UnityEngine.Object) this.m_regionSelectTooltip != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_regionSelectTooltip.gameObject);
  }

  public void WebViewDidFinishLoad(string pageState)
  {
    Debug.Log((object) ("web view page state: " + pageState));
    if (pageState == null)
      return;
    string[] strArray = pageState.Split(new string[1]{ "|" }, StringSplitOptions.None);
    if (strArray.Length < 2)
    {
      Debug.LogWarning((object) string.Format("WebViewDidFinishLoad() - Invalid parsed pageState ({0})", (object) pageState));
    }
    else
    {
      this.m_canGoBack = strArray[strArray.Length - 1].Equals("canGoBack");
      bool flag1 = false;
      bool flag2 = false;
      bool flag3 = false;
      for (int index = 0; index < strArray.Length - 1; ++index)
      {
        string str = strArray[index];
        if (str.Equals("STATE_ACCOUNT_CREATION", StringComparison.InvariantCultureIgnoreCase))
          flag1 = true;
        if (str.Equals("STATE_ACCOUNT_CREATED", StringComparison.InvariantCultureIgnoreCase))
          flag2 = true;
        if (str.Equals("STATE_NO_BACK", StringComparison.InvariantCultureIgnoreCase))
          flag3 = true;
      }
      if ((bool) this.KOBOLD_SHOWN_ON_ACCOUNT_CREATION)
        this.m_accountCreation.SetActive(flag1);
      bool flag4 = flag3 | flag2;
      if (flag2)
        WebAuth.SetIsNewCreatedAccount(true);
      this.m_backButton.gameObject.SetActive(!flag4 && (this.m_canGoBack || !Options.Get().GetBool(Option.CONNECT_TO_AURORA)));
    }
  }

  public void WebViewBackButtonPressed(string dummyState)
  {
    Navigation.GoBack();
  }

  private void SetUpRegionButton()
  {
    if (!((UnityEngine.Object) this.m_regionButton != (UnityEngine.Object) null))
      return;
    this.m_regionButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ShowRegionMenu()));
    this.m_regionButton.SetText(this.onRegionText((object) MobileDeviceLocale.GetCurrentRegionId()));
  }

  private void SetUpRegionDropdown()
  {
    bool flag = ApplicationMgr.GetMobileEnvironment() == MobileEnv.DEVELOPMENT;
    this.m_regionSelector = UnityEngine.Object.Instantiate<DropdownControl>(this.m_regionSelectDropdownPrefab);
    this.m_regionSelector.transform.parent = this.gameObject.transform;
    TransformUtil.CopyLocal((Component) this.m_regionSelector.transform, (Component) this.m_regionSelectDropdownBone.transform);
    this.m_regionSelector.clearItems();
    this.m_regionSelector.setItemTextCallback(new DropdownControl.itemTextCallback(this.onRegionText));
    this.m_regionSelector.setMenuShownCallback(new DropdownControl.menuShownCallback(this.onMenuShown));
    this.m_regionSelector.setItemChosenCallback(new DropdownControl.itemChosenCallback(this.onRegionWarning));
    if (flag)
    {
      using (Map<constants.BnetRegion, MobileDeviceLocale.ConnectionData>.KeyCollection.Enumerator enumerator = MobileDeviceLocale.s_regionIdToDevIP.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.m_regionSelector.addItem((object) enumerator.Current);
      }
    }
    else
    {
      this.m_regionSelector.addItem((object) constants.BnetRegion.REGION_US);
      this.m_regionSelector.addItem((object) constants.BnetRegion.REGION_EU);
      this.m_regionSelector.addItem((object) constants.BnetRegion.REGION_KR);
    }
    this.m_regionSelector.setSelection((object) MobileDeviceLocale.GetCurrentRegionId());
    if (MobileDeviceLocale.UseClientConfigForEnv())
      this.m_regionSelector.gameObject.SetActive(false);
    this.m_regionSelectTooltip = TooltipPanelManager.Get().CreateKeywordPanel(0);
    this.m_regionSelectTooltip.Reset();
    this.m_regionSelectTooltip.Initialize(GameStrings.Get("GLUE_MOBILE_REGION_SELECT_TOOLTIP_HEADER"), GameStrings.Get("GLUE_MOBILE_REGION_SELECT_TOOLTIP"));
    this.m_regionSelectTooltip.transform.position = this.m_regionSelectTooltipBone.transform.position;
    this.m_regionSelectTooltip.transform.localScale = this.m_regionSelectTooltipBone.transform.localScale;
    this.m_regionSelectTooltip.transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
    SceneUtils.SetLayer(this.m_regionSelectTooltip.gameObject, GameLayer.UI);
    this.m_regionSelectTooltip.gameObject.SetActive(false);
  }

  private void onMenuShown(bool shown)
  {
    if (shown)
    {
      this.m_regionSelectTooltip.gameObject.SetActive(true);
      WebAuth.UpdateRegionSelectVisualState(true);
      if (!(bool) UniversalInputManager.UsePhoneUI)
        return;
      SplashScreen.Get().HideWebAuth();
    }
    else
    {
      this.m_regionSelectTooltip.gameObject.SetActive(false);
      WebAuth.UpdateRegionSelectVisualState(false);
      if (!(bool) UniversalInputManager.UsePhoneUI)
        return;
      SplashScreen.Get().UnHideWebAuth();
    }
  }

  private void onRegionChange(object selection, object prevSelection)
  {
    if (selection == prevSelection)
      return;
    Options.Get().SetInt(Option.PREFERRED_REGION, (int) selection);
    this.CauseReconnect();
  }

  private void onRegionChangeCB(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CONFIRM)
    {
      this.onRegionChange(this.m_selection, this.m_prevSelection);
    }
    else
    {
      if ((UnityEngine.Object) this.m_regionSelector != (UnityEngine.Object) null)
        this.m_regionSelector.setSelection(this.m_prevSelection);
      SplashScreen.Get().UnHideWebAuth();
    }
    this.m_regionSelectContents.SetActive(false);
  }

  private void ShowRegionMenu()
  {
    if ((UnityEngine.Object) this.m_regionMenu != (UnityEngine.Object) null)
    {
      this.m_regionMenu.Show();
    }
    else
    {
      this.m_regionMenu = ((GameObject) GameUtils.InstantiateGameObject("RegionMenu", (GameObject) null, false)).GetComponent<RegionMenu>();
      List<UIBButton> buttons = new List<UIBButton>();
      Debug.Log((object) "creating region menu..");
      this.AddButtonForRegion(buttons, constants.BnetRegion.REGION_US);
      this.AddButtonForRegion(buttons, constants.BnetRegion.REGION_EU);
      this.AddButtonForRegion(buttons, constants.BnetRegion.REGION_KR);
      this.m_regionMenu.SetButtons(buttons);
      this.m_regionMenu.Show();
    }
  }

  private void AddButtonForRegion(List<UIBButton> buttons, constants.BnetRegion region)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    WebLoginCanvas.\u003CAddButtonForRegion\u003Ec__AnonStorey3DC regionCAnonStorey3Dc = new WebLoginCanvas.\u003CAddButtonForRegion\u003Ec__AnonStorey3DC();
    // ISSUE: reference to a compiler-generated field
    regionCAnonStorey3Dc.region = region;
    // ISSUE: reference to a compiler-generated field
    regionCAnonStorey3Dc.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    regionCAnonStorey3Dc.currentRegion = MobileDeviceLocale.GetCurrentRegionId();
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    buttons.Add(this.m_regionMenu.CreateMenuButton((string) null, this.onRegionText((object) regionCAnonStorey3Dc.region), new UIEvent.Handler(regionCAnonStorey3Dc.\u003C\u003Em__19C)));
  }

  private void onRegionWarning(object selection, object prevSelection)
  {
    this.m_selection = selection;
    this.m_prevSelection = prevSelection;
    if (selection.Equals(prevSelection))
      return;
    AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
    info.m_headerText = GameStrings.Get("GLUE_MOBILE_REGION_SELECT_WARNING_HEADER");
    info.m_text = GameStrings.Get("GLUE_MOBILE_REGION_SELECT_WARNING");
    info.m_showAlertIcon = false;
    info.m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL;
    info.m_responseCallback = new AlertPopup.ResponseCallback(this.onRegionChangeCB);
    info.m_padding = 60f;
    if ((bool) UniversalInputManager.UsePhoneUI)
      info.m_padding = 80f;
    info.m_scaleOverride = new Vector3?(new Vector3(300f, 300f, 300f));
    SplashScreen.Get().HideWebAuth();
    DialogManager.Get().ShowPopup(info, new DialogManager.DialogProcessCallback(this.OnDialogProcess));
  }

  private string onRegionText(object val)
  {
    constants.BnetRegion bnetRegion = (constants.BnetRegion) val;
    string str1 = string.Empty;
    this.m_regionNames.TryGetValue(bnetRegion, out str1);
    if (ApplicationMgr.GetMobileEnvironment() == MobileEnv.DEVELOPMENT)
    {
      MobileDeviceLocale.ConnectionData dataFromRegionId = MobileDeviceLocale.GetConnectionDataFromRegionId(bnetRegion, true);
      string str2 = dataFromRegionId.name;
      if (string.IsNullOrEmpty(str2))
        str2 = string.Format("{0}:{1}:{2}", (object) dataFromRegionId.address.Split('-')[0], (object) dataFromRegionId.port, (object) dataFromRegionId.version);
      str1 = !string.IsNullOrEmpty(str1) ? string.Format("{0} ({1})", (object) str2, (object) str1) : str2;
    }
    return str1;
  }

  private bool OnDialogProcess(DialogBase dialog, object userData)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_regionSelectContents);
    TransformUtil.AttachAndPreserveLocalTransform(gameObject.transform, dialog.transform);
    gameObject.SetActive(true);
    return true;
  }

  private void OnBackPressed(UIEvent e)
  {
    Navigation.GoBack();
  }

  private bool OnNavigateBack()
  {
    if (this.m_canGoBack)
      WebAuth.GoBackWebPage();
    else if (!Options.Get().GetBool(Option.CONNECT_TO_AURORA))
    {
      ApplicationMgr.Get().Reset();
      return true;
    }
    return false;
  }

  private void CauseReconnect()
  {
    WebAuth.ClearLoginData();
    BattleNet.RequestCloseAurora();
    ApplicationMgr.Get().ResetAndForceLogin();
  }
}
