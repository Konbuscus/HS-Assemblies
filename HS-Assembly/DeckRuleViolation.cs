﻿// Decompiled with JetBrains decompiler
// Type: DeckRuleViolation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class DeckRuleViolation
{
  public DeckRule Rule;
  public string DisplayError;

  public DeckRuleViolation(DeckRule rule, string displayError)
  {
    this.Rule = rule;
    this.DisplayError = displayError;
  }

  private static int SortComparison_RuleType(DeckRule.RuleType lhs, DeckRule.RuleType rhs)
  {
    if (lhs == rhs)
      return 0;
    if (lhs == DeckRule.RuleType.COUNT_COPIES_OF_EACH_CARD)
      return -1;
    if (rhs == DeckRule.RuleType.COUNT_COPIES_OF_EACH_CARD)
      return 1;
    return lhs.CompareTo((object) rhs);
  }

  public static int SortComparison_Rule(DeckRule lhs, DeckRule rhs)
  {
    return DeckRuleViolation.SortComparison_RuleType(lhs != null ? lhs.Type : DeckRule.RuleType.UNKNOWN, rhs != null ? rhs.Type : DeckRule.RuleType.UNKNOWN);
  }

  public static int SortComparison_Violation(DeckRuleViolation lhs, DeckRuleViolation rhs)
  {
    return DeckRuleViolation.SortComparison_Rule(lhs != null ? lhs.Rule : (DeckRule) null, rhs != null ? rhs.Rule : (DeckRule) null);
  }
}
