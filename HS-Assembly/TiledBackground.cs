﻿// Decompiled with JetBrains decompiler
// Type: TiledBackground
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TiledBackground : MonoBehaviour
{
  public float Depth;

  public Vector2 Offset
  {
    get
    {
      return new Vector2(this.GetComponent<Renderer>().material.mainTextureOffset.x / this.GetComponent<Renderer>().material.mainTextureScale.x, this.GetComponent<Renderer>().material.mainTextureOffset.y / this.GetComponent<Renderer>().material.mainTextureScale.y);
    }
    set
    {
      this.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(this.GetComponent<Renderer>().material.mainTextureScale.x * value.x, this.GetComponent<Renderer>().material.mainTextureScale.y * value.y);
    }
  }

  private void Awake()
  {
    if (!((Object) this.GetComponent<Renderer>().material == (Object) null))
      return;
    Debug.LogError((object) "TiledBackground requires the mesh renderer to have a material");
    Object.Destroy((Object) this);
  }

  public void SetBounds(Bounds bounds)
  {
    this.transform.localScale = Vector3.one;
    Vector3 position1 = this.GetComponent<Renderer>().bounds.min;
    Vector3 position2 = this.GetComponent<Renderer>().bounds.max;
    if ((Object) this.transform.parent != (Object) null)
    {
      position1 = this.transform.parent.InverseTransformPoint(position1);
      position2 = this.transform.parent.InverseTransformPoint(position2);
    }
    Vector3 vector3_1 = VectorUtils.Abs(position2 - position1);
    Vector3 vector3_2 = new Vector3((double) vector3_1.x <= 0.0 ? 0.0f : bounds.size.x / vector3_1.x, (double) vector3_1.y <= 0.0 ? 0.0f : bounds.size.y / vector3_1.y, (double) vector3_1.z <= 0.0 ? 0.0f : bounds.size.z / vector3_1.z);
    this.transform.localScale = vector3_2;
    this.transform.localPosition = bounds.center + new Vector3(0.0f, 0.0f, -this.Depth);
    this.GetComponent<Renderer>().material.mainTextureScale = (Vector2) vector3_2;
  }
}
