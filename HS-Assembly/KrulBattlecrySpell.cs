﻿// Decompiled with JetBrains decompiler
// Type: KrulBattlecrySpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class KrulBattlecrySpell : Spell
{
  protected override void OnDeath(SpellStateType prevStateType)
  {
    if (this.m_targets.Count == 0)
      this.OnStateFinished();
    else
      this.OnAction(prevStateType);
  }
}
