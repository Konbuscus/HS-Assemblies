﻿// Decompiled with JetBrains decompiler
// Type: JaraxxusHeroSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class JaraxxusHeroSpell : Spell
{
  private PowerTask m_heroPowerTask;
  private PowerTask m_weaponTask;

  public override bool AddPowerTargets()
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PowerTask current = enumerator.Current;
        Network.PowerHistory power = current.GetPower();
        if (power.Type == Network.PowerType.FULL_ENTITY)
        {
          int id = (power as Network.HistFullEntity).Entity.ID;
          Entity entity = GameState.Get().GetEntity(id);
          if (entity == null)
          {
            UnityEngine.Debug.LogWarning((object) string.Format("{0}.AddPowerTargets() - WARNING encountered HistFullEntity where entity id={1} but there is no entity with that id", (object) this, (object) id));
            return false;
          }
          if (entity.IsHeroPower())
          {
            this.m_heroPowerTask = current;
            this.AddTarget(entity.GetCard().gameObject);
            if (this.m_weaponTask != null)
              return true;
          }
          else if (entity.IsWeapon())
          {
            this.m_weaponTask = current;
            this.AddTarget(entity.GetCard().gameObject);
            if (this.m_heroPowerTask != null)
              return true;
          }
        }
      }
    }
    this.Reset();
    return false;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    this.StartCoroutine(this.SetupCards());
  }

  [DebuggerHidden]
  private IEnumerator SetupCards()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JaraxxusHeroSpell.\u003CSetupCards\u003Ec__Iterator25B() { \u003C\u003Ef__this = this };
  }

  private Entity LoadCardFromTask(PowerTask task)
  {
    Network.Entity entity1 = (task.GetPower() as Network.HistFullEntity).Entity;
    Entity entity2 = GameState.Get().GetEntity(entity1.ID);
    entity2.LoadCard(entity1.CardID);
    return entity2;
  }

  private Card GetCardFromTask(PowerTask task)
  {
    return GameState.Get().GetEntity((task.GetPower() as Network.HistFullEntity).Entity.ID).GetCard();
  }

  private void Reset()
  {
    this.m_heroPowerTask = (PowerTask) null;
    this.m_weaponTask = (PowerTask) null;
  }

  private void Finish()
  {
    this.Reset();
    this.OnSpellFinished();
  }

  private void PlayCardSpells(Card heroPowerCard, Card weaponCard)
  {
    heroPowerCard.ShowCard();
    heroPowerCard.ActivateStateSpells();
    heroPowerCard.ActivateActorSpell(SpellType.SUMMON_JARAXXUS, new Spell.FinishedCallback(this.OnSpellFinished_HeroPower));
    weaponCard.ActivateActorSpell(SpellType.SUMMON_JARAXXUS, new Spell.FinishedCallback(this.OnSpellFinished_Weapon));
  }

  private void OnSpellFinished_HeroPower(Spell spell, object userData)
  {
    this.m_heroPowerTask.SetCompleted(true);
    if (!this.m_weaponTask.IsCompleted())
      return;
    this.Finish();
  }

  private void OnSpellFinished_Weapon(Spell spell, object userData)
  {
    Card cardFromTask = this.GetCardFromTask(this.m_weaponTask);
    cardFromTask.ShowCard();
    cardFromTask.ActivateStateSpells();
    this.m_weaponTask.SetCompleted(true);
    if (!this.m_heroPowerTask.IsCompleted())
      return;
    this.Finish();
  }
}
