﻿// Decompiled with JetBrains decompiler
// Type: OptionsMenuPhone
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class OptionsMenuPhone : MonoBehaviour
{
  public OptionsMenu m_optionsMenu;
  public UIBButton m_doneButton;
  public GameObject m_mainContentsPanel;

  private void Start()
  {
    this.m_doneButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.m_optionsMenu.Hide(true)));
  }
}
