﻿// Decompiled with JetBrains decompiler
// Type: DebugUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityEngine;

public class DebugUtils
{
  [Conditional("UNITY_EDITOR")]
  public static void Assert(bool test)
  {
    if (test)
      return;
    UnityEngine.Debug.Break();
  }

  [Conditional("UNITY_EDITOR")]
  public static void Assert(bool test, string message)
  {
    if (test)
      return;
    UnityEngine.Debug.LogWarning((object) message);
    UnityEngine.Debug.Break();
  }

  public static string HashtableToString(Hashtable table)
  {
    string str = string.Empty;
    foreach (DictionaryEntry dictionaryEntry in table)
      str = str + dictionaryEntry.Key + " = " + dictionaryEntry.Value + "\n";
    return str;
  }

  public static int CountParents(GameObject go)
  {
    int num = 0;
    if ((Object) go != (Object) null)
    {
      for (Transform parent = go.transform.parent; (Object) parent != (Object) null; parent = parent.transform.parent)
        ++num;
    }
    return num;
  }

  public static string GetHierarchyPath(Object obj, char separator = '.')
  {
    StringBuilder b = new StringBuilder();
    DebugUtils.GetHierarchyPath_Internal(b, obj, separator);
    return b.ToString();
  }

  public static string GetHierarchyPathAndType(Object obj, char separator = '.')
  {
    StringBuilder b = new StringBuilder();
    b.Append("[Type]=").Append(((object) obj).GetType().FullName).Append(" [Path]=");
    DebugUtils.GetHierarchyPath_Internal(b, obj, separator);
    return b.ToString();
  }

  private static bool GetHierarchyPath_Internal(StringBuilder b, Object obj, char separator)
  {
    if (obj == (Object) null)
      return false;
    Transform transform = !(obj is GameObject) ? (!(obj is Component) ? (Transform) null : ((Component) obj).transform) : ((GameObject) obj).transform;
    List<string> stringList = new List<string>();
    for (; (Object) transform != (Object) null; transform = transform.parent)
      stringList.Insert(0, transform.gameObject.name);
    if (stringList.Count > 0 && (int) separator == 47)
      b.Append(separator);
    for (int index = 0; index < stringList.Count; ++index)
    {
      b.Append(stringList[index]);
      if (index < stringList.Count - 1)
        b.Append(separator);
    }
    return true;
  }
}
