﻿// Decompiled with JetBrains decompiler
// Type: HistoryChildCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HistoryChildCard : HistoryItem
{
  public void SetCardInfo(Entity entity, Texture portraitTexture, Material portraitGoldenMaterial, int splatAmount, bool isDead)
  {
    this.m_entity = entity;
    this.m_portraitTexture = portraitTexture;
    this.m_portraitGoldenMaterial = portraitGoldenMaterial;
    this.m_splatAmount = splatAmount;
    this.m_dead = isDead;
  }

  public void LoadMainCardActor()
  {
    string historyActor = ActorNames.GetHistoryActor(this.m_entity);
    GameObject gameObject = AssetLoader.Get().LoadActor(historyActor, false, false);
    if ((Object) gameObject == (Object) null)
    {
      Debug.LogWarningFormat("HistoryChildCard.LoadActorCallback() - FAILED to load actor \"{0}\"", (object) historyActor);
    }
    else
    {
      Actor component = gameObject.GetComponent<Actor>();
      if ((Object) component == (Object) null)
      {
        Debug.LogWarningFormat("HistoryChildCard.LoadActorCallback() - ERROR actor \"{0}\" has no Actor component", (object) historyActor);
      }
      else
      {
        this.m_mainCardActor = component;
        this.m_mainCardActor.SetPremium(this.m_entity.GetPremiumType());
        this.m_mainCardActor.SetHistoryItem((HistoryItem) this);
        this.m_mainCardActor.UpdateAllComponents();
        SceneUtils.SetLayer(this.m_mainCardActor.gameObject, GameLayer.Tooltip);
        this.m_mainCardActor.Hide();
      }
    }
  }
}
