﻿// Decompiled with JetBrains decompiler
// Type: FriendListFriendFrameOffsets
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class FriendListFriendFrameOffsets
{
  public Vector3 m_PlayerNameText;
  public Vector3 m_StatusText;
  public Vector3 m_RightComponent;
}
