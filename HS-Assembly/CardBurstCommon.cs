﻿// Decompiled with JetBrains decompiler
// Type: CardBurstCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardBurstCommon : Spell
{
  public ParticleSystem m_BurstMotes;
  public GameObject m_EdgeGlow;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    if ((bool) ((Object) this.m_BurstMotes))
      this.m_BurstMotes.Play();
    if ((bool) ((Object) this.m_EdgeGlow))
      this.m_EdgeGlow.GetComponent<Renderer>().enabled = true;
    this.OnSpellFinished();
  }
}
