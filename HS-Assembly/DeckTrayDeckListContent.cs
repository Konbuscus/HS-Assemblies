﻿// Decompiled with JetBrains decompiler
// Type: DeckTrayDeckListContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class DeckTrayDeckListContent : DeckTrayContent
{
  private List<TraySection> m_traySections = new List<TraySection>();
  private List<long> m_decksToDelete = new List<long>();
  private List<DeckTrayDeckListContent.DeckCountChanged> m_deckCountChangedListeners = new List<DeckTrayDeckListContent.DeckCountChanged>();
  private List<DeckTrayDeckListContent.BusyWithDeck> m_busyWithDeckListeners = new List<DeckTrayDeckListContent.BusyWithDeck>();
  private int m_centeringDeckList = -1;
  private const float DECK_BUTTON_ROTATION_TIME = 0.1f;
  private const int MAX_NUM_DECKBOXES_AVAILABLE = 18;
  private const int NUM_DECKBOXES_TO_DISPLAY = 20;
  private const float TIME_BETWEEN_TRAY_DOOR_ANIMS = 0.015f;
  private const float DELETE_DECK_ANIM_TIME = 0.5f;
  [CustomEditField(Sections = "Deck Tray Settings")]
  public Transform m_deckEditTopPos;
  [CustomEditField(Sections = "Deck Tray Settings")]
  public Transform m_traySectionStartPos;
  [CustomEditField(Sections = "Deck Tray Settings")]
  public GameObject m_deckInfoTooltipBone;
  [CustomEditField(Sections = "Deck Tray Settings")]
  public GameObject m_deckOptionsBone;
  [SerializeField]
  private Vector3 m_deckButtonOffset;
  [CustomEditField(Sections = "Deck Button Settings")]
  public GameObject m_newDeckButtonContainer;
  [CustomEditField(Sections = "Deck Button Settings")]
  public CollectionNewDeckButton m_newDeckButton;
  [CustomEditField(Sections = "Deck Button Settings")]
  public ParticleSystem m_deleteDeckPoof;
  [CustomEditField(Sections = "Prefabs")]
  public TraySection m_traySectionPrefab;
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public string m_deckInfoActorPrefab;
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public string m_deckOptionsPrefab;
  [CustomEditField(Sections = "Scroll Settings")]
  public UIBScrollable m_scrollbar;
  private CollectionDeckInfo m_deckInfoTooltip;
  private bool m_initialized;
  private TraySection m_editingTraySection;
  private TraySection m_newlyCreatedTraySection;
  private bool m_animatingExit;
  private bool m_deletingDecks;
  private bool m_wasTouchModeEnabled;
  private string m_previousDeckName;
  private bool m_waitingToDeleteDeck;
  private DeckOptionsMenu m_deckOptionsMenu;
  private bool m_doneEntering;

  [CustomEditField(Sections = "Deck Button Settings")]
  public Vector3 DeckButtonOffset
  {
    get
    {
      return this.m_deckButtonOffset;
    }
    set
    {
      this.m_deckButtonOffset = value;
      this.UpdateNewDeckButton((TraySection) null);
    }
  }

  private void Awake()
  {
    CollectionManager.Get().RegisterFavoriteHeroChangedListener(new CollectionManager.FavoriteHeroChangedCallback(this.OnFavoriteHeroChanged));
  }

  private void Update()
  {
    if (this.m_wasTouchModeEnabled == UniversalInputManager.Get().IsTouchMode())
      return;
    this.m_wasTouchModeEnabled = UniversalInputManager.Get().IsTouchMode();
    if (!UniversalInputManager.Get().IsTouchMode() || !((UnityEngine.Object) this.m_deckInfoTooltip != (UnityEngine.Object) null))
      return;
    this.HideDeckInfo();
  }

  private void OnDestroy()
  {
    CollectionManager collectionManager = CollectionManager.Get();
    collectionManager.RemoveFavoriteHeroChangedListener(new CollectionManager.FavoriteHeroChangedCallback(this.OnFavoriteHeroChanged));
    collectionManager.RemoveDeckDeletedListener(new CollectionManager.DelOnDeckDeleted(this.OnDeckDeleted));
    if (!((UnityEngine.Object) Box.Get() != (UnityEngine.Object) null))
      return;
    Box.Get().RemoveTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
  }

  public bool IsDoneEntering()
  {
    return this.m_doneEntering;
  }

  [DebuggerHidden]
  public IEnumerator ShowTrayDoors(bool show)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTrayDeckListContent.\u003CShowTrayDoors\u003Ec__Iterator303() { show = show, \u003C\u0024\u003Eshow = show, \u003C\u003Ef__this = this };
  }

  public override bool AnimateContentEntranceStart()
  {
    this.Initialize();
    long editDeckID = -1;
    if ((UnityEngine.Object) this.m_editingTraySection != (UnityEngine.Object) null)
      editDeckID = this.m_editingTraySection.m_deckBox.GetDeckID();
    this.InitializeTraysFromDecks();
    this.SwapEditTrayIfNeeded(editDeckID);
    this.UpdateAllTrays(SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL, false);
    if ((UnityEngine.Object) this.m_editingTraySection != (UnityEngine.Object) null)
    {
      this.FinishRenamingEditingDeck((string) null);
      this.m_editingTraySection.MoveDeckBoxBackToOriginalPosition(0.25f, (TraySection.DelOnDoorStateChangedCallback) (o => this.m_editingTraySection = (TraySection) null));
    }
    this.m_newDeckButton.SetIsUsable(this.CanShowNewDeckButton());
    this.FireBusyWithDeckEvent(true);
    this.FireDeckCountChangedEvent();
    CollectionManager.Get().DoneEditing();
    return true;
  }

  public override bool AnimateContentEntranceEnd()
  {
    if ((UnityEngine.Object) this.m_editingTraySection != (UnityEngine.Object) null)
      return false;
    this.m_newDeckButton.SetEnabled(true);
    this.FireBusyWithDeckEvent(false);
    this.DeleteQueuedDecks(true);
    return true;
  }

  public override bool AnimateContentExitStart()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTrayDeckListContent.\u003CAnimateContentExitStart\u003Ec__AnonStorey432 startCAnonStorey432 = new DeckTrayDeckListContent.\u003CAnimateContentExitStart\u003Ec__AnonStorey432();
    // ISSUE: reference to a compiler-generated field
    startCAnonStorey432.\u003C\u003Ef__this = this;
    this.m_animatingExit = true;
    this.FireBusyWithDeckEvent(true);
    float? speed = new float?();
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
      speed = new float?(500f);
    this.ShowNewDeckButton(false, speed, (CollectionNewDeckButton.DelOnAnimationFinished) null);
    // ISSUE: reference to a compiler-generated field
    startCAnonStorey432.animationWaitTime = 0.5f;
    // ISSUE: reference to a compiler-generated method
    ApplicationMgr.Get().ScheduleCallback(0.5f, false, new ApplicationMgr.ScheduledCallback(startCAnonStorey432.\u003C\u003Em__2C2), (object) null);
    return true;
  }

  public override bool AnimateContentExitEnd()
  {
    return !this.m_animatingExit;
  }

  public override bool PreAnimateContentExit()
  {
    if ((UnityEngine.Object) this.m_scrollbar == (UnityEngine.Object) null)
      return true;
    if (this.m_centeringDeckList != -1 && (UnityEngine.Object) this.m_editingTraySection != (UnityEngine.Object) null)
    {
      BoxCollider component = this.m_editingTraySection.m_deckBox.GetComponent<BoxCollider>();
      if (this.m_scrollbar.ScrollObjectIntoView(this.m_editingTraySection.m_deckBox.gameObject, component.center.y, component.size.y / 2f, (UIBScrollable.OnScrollComplete) (f => this.m_animatingExit = false), iTween.EaseType.linear, this.m_scrollbar.m_ScrollTweenTime, true))
      {
        this.m_animatingExit = true;
        this.m_centeringDeckList = -1;
      }
    }
    return !this.m_animatingExit;
  }

  public override bool PostAnimateContentExit()
  {
    this.StartCoroutine(this.ShowTrayDoors(false));
    return true;
  }

  public override bool PreAnimateContentEntrance()
  {
    this.m_doneEntering = false;
    this.StartCoroutine(this.ShowTrayDoors(true));
    return true;
  }

  public override void OnTaggedDeckChanged(CollectionManager.DeckTag tag, CollectionDeck newDeck, CollectionDeck oldDeck, bool isNewDeck)
  {
    if (tag != CollectionManager.DeckTag.Editing)
      return;
    if (newDeck != null && (UnityEngine.Object) this.m_deckInfoTooltip != (UnityEngine.Object) null)
    {
      this.m_deckInfoTooltip.SetDeck(newDeck);
      if ((UnityEngine.Object) this.m_deckOptionsMenu != (UnityEngine.Object) null)
        this.m_deckOptionsMenu.SetDeck(newDeck);
    }
    if (this.IsModeActive())
      this.InitializeTraysFromDecks();
    if (!isNewDeck || newDeck == null)
      return;
    this.m_newlyCreatedTraySection = this.GetExistingTrayFromDeck(newDeck);
    if (!((UnityEngine.Object) this.m_newlyCreatedTraySection != (UnityEngine.Object) null))
      return;
    this.m_centeringDeckList = this.m_newlyCreatedTraySection.m_deckBox.GetPositionIndex();
  }

  public void CreateNewDeckFromUserSelection(TAG_CLASS heroClass, string heroCardID, string customDeckName = null)
  {
    bool flag = SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL;
    DeckType deckType = DeckType.NORMAL_DECK;
    string name = customDeckName;
    if (flag)
    {
      deckType = DeckType.TAVERN_BRAWL_DECK;
      name = GameStrings.Get("GLUE_COLLECTION_TAVERN_BRAWL_DECKNAME");
    }
    else if (string.IsNullOrEmpty(name))
      name = CollectionManager.Get().AutoGenerateDeckName(heroClass);
    CollectionManager.Get().SendCreateDeck(deckType, name, heroCardID);
    this.EndCreateNewDeck(true);
  }

  public void CreateNewDeckCancelled()
  {
    this.EndCreateNewDeck(false);
  }

  public bool IsWaitingToDeleteDeck()
  {
    return this.m_waitingToDeleteDeck;
  }

  public int NumDecksToDelete()
  {
    return this.m_decksToDelete.Count;
  }

  public bool IsDeletingDecks()
  {
    return this.m_deletingDecks;
  }

  public void DeleteDeck(long deckID)
  {
    this.m_decksToDelete.Add(deckID);
    CollectionDeck deck = CollectionManager.Get().GetDeck(deckID);
    if (deck != null)
      deck.MarkBeingDeleted();
    this.DeleteQueuedDecks(false);
  }

  public void DeleteEditingDeck()
  {
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    if (taggedDeck == null)
    {
      UnityEngine.Debug.LogWarning((object) "No deck currently being edited!");
    }
    else
    {
      this.m_waitingToDeleteDeck = true;
      this.DeleteDeck(taggedDeck.ID);
    }
  }

  public void CancelRenameEditingDeck()
  {
    this.FinishRenamingEditingDeck((string) null);
  }

  public Vector3 GetNewDeckButtonPosition()
  {
    return this.m_newDeckButton.transform.position;
  }

  public void UpdateEditingDeckBoxVisual(string heroCardId)
  {
    if ((UnityEngine.Object) this.m_editingTraySection == (UnityEngine.Object) null)
      return;
    this.m_editingTraySection.m_deckBox.SetHeroCardID(heroCardId);
  }

  private void OnDrawGizmos()
  {
    if ((UnityEngine.Object) this.m_editingTraySection == (UnityEngine.Object) null)
      return;
    Bounds bounds = this.m_editingTraySection.m_deckBox.GetDeckNameText().GetBounds();
    Gizmos.DrawWireSphere(bounds.min, 0.1f);
    Gizmos.DrawWireSphere(bounds.max, 0.1f);
  }

  public void RenameCurrentlyEditingDeck()
  {
    if ((UnityEngine.Object) this.m_editingTraySection == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "Unable to rename deck. No deck currently being edited.", (UnityEngine.Object) this.gameObject);
    }
    else
    {
      if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
        return;
      CollectionDeckBoxVisual deckBox = this.m_editingTraySection.m_deckBox;
      deckBox.HideDeckName();
      Camera camera = Box.Get().GetCamera();
      Bounds bounds = deckBox.GetDeckNameText().GetBounds();
      Rect guiViewportRect = CameraUtils.CreateGUIViewportRect(camera, bounds.min, bounds.max);
      Font localizedFont = deckBox.GetDeckNameText().GetLocalizedFont();
      this.m_previousDeckName = deckBox.GetDeckNameText().Text;
      UniversalInputManager.Get().UseTextInput(new UniversalInputManager.TextInputParams()
      {
        m_owner = this.gameObject,
        m_rect = guiViewportRect,
        m_updatedCallback = (UniversalInputManager.TextInputUpdatedCallback) (newName => this.UpdateRenamingEditingDeck(newName)),
        m_completedCallback = (UniversalInputManager.TextInputCompletedCallback) (newName => this.FinishRenamingEditingDeck(newName)),
        m_canceledCallback = (UniversalInputManager.TextInputCanceledCallback) ((a1, a2) => this.FinishRenamingEditingDeck(this.m_previousDeckName)),
        m_maxCharacters = 24,
        m_font = localizedFont,
        m_text = deckBox.GetDeckNameText().Text
      }, false);
    }
  }

  public void RegisterDeckCountUpdated(DeckTrayDeckListContent.DeckCountChanged dlg)
  {
    this.m_deckCountChangedListeners.Add(dlg);
  }

  public void UnregisterDeckCountUpdated(DeckTrayDeckListContent.DeckCountChanged dlg)
  {
    this.m_deckCountChangedListeners.Remove(dlg);
  }

  public void RegisterBusyWithDeck(DeckTrayDeckListContent.BusyWithDeck dlg)
  {
    this.m_busyWithDeckListeners.Add(dlg);
  }

  public void UnregisterBusyWithDeck(DeckTrayDeckListContent.BusyWithDeck dlg)
  {
    this.m_busyWithDeckListeners.Remove(dlg);
  }

  public void UpdateDeckName(string deckName = null)
  {
    if (deckName == null)
    {
      CollectionDeck editingDeck = CollectionDeckTray.Get().GetCardsContent().GetEditingDeck();
      if (editingDeck == null)
        return;
      deckName = editingDeck.Name;
    }
    this.FinishRenamingEditingDeck(deckName);
  }

  public void HideTraySectionsNotInBounds(Bounds bounds)
  {
    int num = 0;
    using (List<TraySection>.Enumerator enumerator = this.m_traySections.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.HideIfNotInBounds(bounds))
          ++num;
      }
    }
    Log.JMac.Print("Hid {0} tray sections that were not visible.", (object) num);
    UIBScrollableItem component = this.m_newDeckButtonContainer.GetComponent<UIBScrollableItem>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "UIBScrollableItem not found on m_newDeckButtonContainer! This button may not be hidden properly while exiting Collection Manager!");
    }
    else
    {
      Bounds bounds1 = new Bounds();
      Vector3 min;
      Vector3 max;
      component.GetWorldBounds(out min, out max);
      bounds1.SetMinMax(min, max);
      if (bounds.Intersects(bounds1))
        return;
      Log.JMac.Print("Hiding the New Deck button because it's out of the visible scroll area.");
      this.m_newDeckButton.gameObject.SetActive(false);
    }
  }

  private CollectionDeck UpdateRenamingEditingDeck(string newDeckName)
  {
    CollectionDeck editingDeck = CollectionDeckTray.Get().GetCardsContent().GetEditingDeck();
    if (editingDeck != null && !string.IsNullOrEmpty(newDeckName))
      editingDeck.Name = newDeckName;
    return editingDeck;
  }

  private void FinishRenamingEditingDeck(string newDeckName = null)
  {
    if ((UnityEngine.Object) this.m_editingTraySection == (UnityEngine.Object) null)
      return;
    CollectionDeckBoxVisual deckBox = this.m_editingTraySection.m_deckBox;
    CollectionDeck collectionDeck = this.UpdateRenamingEditingDeck(newDeckName);
    if (collectionDeck != null && (UnityEngine.Object) this.m_editingTraySection != (UnityEngine.Object) null)
      deckBox.SetDeckName(collectionDeck.Name);
    if ((UnityEngine.Object) UniversalInputManager.Get() != (UnityEngine.Object) null && UniversalInputManager.Get().IsTextInputActive())
      UniversalInputManager.Get().CancelTextInput(this.gameObject, false);
    deckBox.ShowDeckName();
  }

  private void Initialize()
  {
    if (this.m_initialized)
      return;
    this.m_newDeckButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OnNewDeckButtonPress()));
    CollectionManager.Get().RegisterDeckDeletedListener(new CollectionManager.DelOnDeckDeleted(this.OnDeckDeleted));
    GameObject gameObject = AssetLoader.Get().LoadActor(FileUtils.GameAssetPathToName(this.m_deckInfoActorPrefab), false, false);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("Unable to load actor {0}: null", (object) this.m_deckInfoActorPrefab), (UnityEngine.Object) this.gameObject);
    }
    else
    {
      this.m_deckInfoTooltip = gameObject.GetComponent<CollectionDeckInfo>();
      if ((UnityEngine.Object) this.m_deckInfoTooltip == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("Actor {0} does not contain CollectionDeckInfo component.", (object) this.m_deckInfoActorPrefab), (UnityEngine.Object) this.gameObject);
      }
      else
      {
        GameUtils.SetParent((Component) this.m_deckInfoTooltip, this.m_deckInfoTooltipBone, false);
        this.m_deckInfoTooltip.RegisterHideListener(new CollectionDeckInfo.HideListener(this.HideDeckInfoListener));
        this.m_deckOptionsMenu = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(this.m_deckOptionsPrefab), true, false).GetComponent<DeckOptionsMenu>();
        GameUtils.SetParent(this.m_deckOptionsMenu.gameObject, this.m_deckOptionsBone, false);
        this.m_deckOptionsMenu.SetDeckInfo(this.m_deckInfoTooltip);
        this.HideDeckInfo();
        this.CreateTraySections();
        this.m_initialized = true;
      }
    }
  }

  private void HideDeckInfoListener()
  {
    if ((UnityEngine.Object) this.m_editingTraySection != (UnityEngine.Object) null)
    {
      SceneUtils.SetLayer(this.m_editingTraySection.m_deckBox.gameObject, GameLayer.Default);
      SceneUtils.SetLayer(this.m_deckOptionsMenu.gameObject, GameLayer.Default);
      this.m_editingTraySection.m_deckBox.HideRenameVisuals();
    }
    FullScreenFXMgr.Get().StopDesaturate(0.25f, iTween.EaseType.easeInOutQuad, (FullScreenFXMgr.EffectListener) null);
    if (UniversalInputManager.Get().IsTouchMode())
    {
      if ((UnityEngine.Object) this.m_editingTraySection != (UnityEngine.Object) null)
      {
        this.m_editingTraySection.m_deckBox.SetHighlightState(ActorStateType.NONE);
        this.m_editingTraySection.m_deckBox.ShowDeckName();
      }
      this.FinishRenamingEditingDeck((string) null);
    }
    this.m_deckOptionsMenu.Hide(true);
  }

  private void ShowDeckInfo()
  {
    if (!UniversalInputManager.Get().IsTouchMode())
      this.m_editingTraySection.m_deckBox.ShowRenameVisuals();
    SceneUtils.SetLayer(this.m_editingTraySection.m_deckBox.gameObject, GameLayer.IgnoreFullScreenEffects);
    SceneUtils.SetLayer(this.m_deckInfoTooltip.gameObject, GameLayer.IgnoreFullScreenEffects);
    SceneUtils.SetLayer(this.m_deckOptionsMenu.gameObject, GameLayer.IgnoreFullScreenEffects);
    FullScreenFXMgr.Get().Desaturate(0.9f, 0.25f, iTween.EaseType.easeInOutQuad, (FullScreenFXMgr.EffectListener) null);
    this.m_deckInfoTooltip.UpdateManaCurve();
    this.m_deckInfoTooltip.Show();
    this.m_deckOptionsMenu.Show();
  }

  private void HideDeckInfo()
  {
    this.m_deckInfoTooltip.Hide();
  }

  private void CreateTraySections()
  {
    Vector3 localScale = this.m_traySectionStartPos.localScale;
    Vector3 localEulerAngles = this.m_traySectionStartPos.localEulerAngles;
    GameObject gameObject = this.m_traySectionStartPos.gameObject;
    for (int idx = 0; idx < 20; ++idx)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      DeckTrayDeckListContent.\u003CCreateTraySections\u003Ec__AnonStorey433 sectionsCAnonStorey433 = new DeckTrayDeckListContent.\u003CCreateTraySections\u003Ec__AnonStorey433();
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.\u003C\u003Ef__this = this;
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.traySection = (TraySection) GameUtils.Instantiate((Component) this.m_traySectionPrefab, this.gameObject, false);
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.traySection.transform.localScale = localScale;
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.traySection.transform.localEulerAngles = localEulerAngles;
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.traySection.EnableDoors(idx < 18);
      if (idx == 0)
      {
        // ISSUE: reference to a compiler-generated field
        sectionsCAnonStorey433.traySection.transform.localPosition = this.m_traySectionStartPos.localPosition;
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        TransformUtil.SetPoint(sectionsCAnonStorey433.traySection.gameObject, Anchor.FRONT, gameObject, Anchor.BACK);
      }
      Material material1 = (Material) null;
      // ISSUE: reference to a compiler-generated field
      foreach (Material material2 in sectionsCAnonStorey433.traySection.m_door.GetComponent<Renderer>().materials)
      {
        if (material2.name.Equals("DeckTray", StringComparison.OrdinalIgnoreCase) || material2.name.Equals("DeckTray (Instance)", StringComparison.OrdinalIgnoreCase))
        {
          material1 = material2;
          break;
        }
      }
      UnityEngine.Vector2 vector2 = new UnityEngine.Vector2(0.0f, -0.0825f * (float) idx);
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.traySection.GetComponent<Renderer>().material.mainTextureOffset = vector2;
      if ((UnityEngine.Object) material1 != (UnityEngine.Object) null)
        material1.mainTextureOffset = vector2;
      // ISSUE: reference to a compiler-generated field
      gameObject = sectionsCAnonStorey433.traySection.gameObject;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.deckBox = sectionsCAnonStorey433.traySection.m_deckBox;
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.deckBox.SetPositionIndex(idx);
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      sectionsCAnonStorey433.deckBox.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(sectionsCAnonStorey433.\u003C\u003Em__2C8));
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      sectionsCAnonStorey433.deckBox.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(sectionsCAnonStorey433.\u003C\u003Em__2C9));
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      sectionsCAnonStorey433.deckBox.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(sectionsCAnonStorey433.\u003C\u003Em__2CA));
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      sectionsCAnonStorey433.deckBox.AddEventListener(UIEventType.TAP, new UIEvent.Handler(sectionsCAnonStorey433.\u003C\u003Em__2CB));
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.deckBox.SetOriginalButtonPosition();
      // ISSUE: reference to a compiler-generated field
      sectionsCAnonStorey433.deckBox.HideBanner();
      // ISSUE: reference to a compiler-generated field
      this.m_traySections.Add(sectionsCAnonStorey433.traySection);
    }
    if ((bool) UniversalInputManager.UsePhoneUI)
      return;
    this.HideTraySectionsNotInBounds(CollectionDeckTray.Get().m_scrollbar.m_ScrollBounds.bounds);
    Box.Get().AddTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
  }

  private void OnBoxTransitionFinished(object userData)
  {
    Box.Get().RemoveTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    using (List<TraySection>.Enumerator enumerator = this.m_traySections.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.gameObject.SetActive(true);
    }
  }

  private TraySection GetExistingTrayFromDeck(CollectionDeck deck)
  {
    return this.GetExistingTrayFromDeck(deck.ID);
  }

  private TraySection GetExistingTrayFromDeck(long deckID)
  {
    using (List<TraySection>.Enumerator enumerator = this.m_traySections.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TraySection current = enumerator.Current;
        if (current.m_deckBox.GetDeckID() == deckID)
          return current;
      }
    }
    return (TraySection) null;
  }

  public TraySection GetEditingTraySection()
  {
    return this.m_editingTraySection;
  }

  private void InitializeTraysFromDecks()
  {
    this.UpdateDeckTrayVisuals();
  }

  private void UpdateAllTrays(bool immediate = false, bool initializeTrays = true)
  {
    if (initializeTrays)
      this.InitializeTraysFromDecks();
    List<TraySection> showTraySections = new List<TraySection>();
    using (List<TraySection>.Enumerator enumerator = this.m_traySections.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TraySection current = enumerator.Current;
        if (current.m_deckBox.GetDeckID() == -1L && !current.m_deckBox.IsLocked())
          current.HideDeckBox(immediate, (TraySection.DelOnDoorStateChangedCallback) null);
        else if ((UnityEngine.Object) this.m_editingTraySection != (UnityEngine.Object) current && !current.IsOpen())
          showTraySections.Add(current);
      }
    }
    this.StartCoroutine(this.UpdateAllTraysAnimation(showTraySections, immediate));
  }

  [DebuggerHidden]
  private IEnumerator UpdateAllTraysAnimation(List<TraySection> showTraySections, bool immediate)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTrayDeckListContent.\u003CUpdateAllTraysAnimation\u003Ec__Iterator304() { showTraySections = showTraySections, immediate = immediate, \u003C\u0024\u003EshowTraySections = showTraySections, \u003C\u0024\u003Eimmediate = immediate, \u003C\u003Ef__this = this };
  }

  private void UpdateNewDeckButton(TraySection setNewDeckButtonPosition = null)
  {
    this.ShowNewDeckButton(this.UpdateNewDeckButtonPosition(setNewDeckButtonPosition) && this.CanShowNewDeckButton(), (CollectionNewDeckButton.DelOnAnimationFinished) null);
  }

  private bool UpdateNewDeckButtonPosition(TraySection setNewDeckButtonPosition = null)
  {
    bool outActive = false;
    Vector3 outPosition;
    this.GetIdealNewDeckButtonLocalPosition(setNewDeckButtonPosition, out outPosition, out outActive);
    this.m_newDeckButtonContainer.transform.localPosition = outPosition;
    return outActive;
  }

  private void GetIdealNewDeckButtonLocalPosition(TraySection setNewDeckButtonPosition, out Vector3 outPosition, out bool outActive)
  {
    TraySection unusedTraySection = this.GetLastUnusedTraySection();
    TraySection traySection = !((UnityEngine.Object) setNewDeckButtonPosition == (UnityEngine.Object) null) ? setNewDeckButtonPosition : unusedTraySection;
    outActive = (UnityEngine.Object) unusedTraySection != (UnityEngine.Object) null;
    outPosition = (!((UnityEngine.Object) traySection != (UnityEngine.Object) null) ? this.m_traySectionStartPos.localPosition : traySection.transform.localPosition) + this.m_deckButtonOffset;
  }

  public TraySection GetLastUnusedTraySection()
  {
    int num = 0;
    using (List<TraySection>.Enumerator enumerator = this.m_traySections.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TraySection current = enumerator.Current;
        if (num < 18)
        {
          if (current.m_deckBox.GetDeckID() == -1L)
            return current;
          ++num;
        }
        else
          break;
      }
    }
    return (TraySection) null;
  }

  public TraySection GetLastUsedTraySection()
  {
    int num = 0;
    TraySection traySection = (TraySection) null;
    using (List<TraySection>.Enumerator enumerator = this.m_traySections.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TraySection current = enumerator.Current;
        if (num < 18)
        {
          if (current.m_deckBox.GetDeckID() == -1L)
            return traySection;
          traySection = current;
          ++num;
        }
        else
          break;
      }
    }
    return traySection;
  }

  public TraySection GetTraySection(int index)
  {
    if (index >= 0 && index < this.m_traySections.Count)
      return this.m_traySections[index];
    return (TraySection) null;
  }

  public void ShowNewDeckButton(bool newDeckButtonActive, CollectionNewDeckButton.DelOnAnimationFinished callback = null)
  {
    this.ShowNewDeckButton(newDeckButtonActive, new float?(), callback);
  }

  public void ShowNewDeckButton(bool newDeckButtonActive, float? speed, CollectionNewDeckButton.DelOnAnimationFinished callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTrayDeckListContent.\u003CShowNewDeckButton\u003Ec__AnonStorey434 buttonCAnonStorey434 = new DeckTrayDeckListContent.\u003CShowNewDeckButton\u003Ec__AnonStorey434();
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey434.callback = callback;
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey434.\u003C\u003Ef__this = this;
    if (this.m_newDeckButton.IsPoppedUp() != newDeckButtonActive)
    {
      if (newDeckButtonActive)
      {
        this.m_newDeckButton.gameObject.SetActive(true);
        // ISSUE: reference to a compiler-generated method
        this.m_newDeckButton.PlayPopUpAnimation(new CollectionNewDeckButton.DelOnAnimationFinished(buttonCAnonStorey434.\u003C\u003Em__2CC), (object) null, speed);
      }
      else
      {
        // ISSUE: reference to a compiler-generated method
        this.m_newDeckButton.PlayPopDownAnimation(new CollectionNewDeckButton.DelOnAnimationFinished(buttonCAnonStorey434.\u003C\u003Em__2CD), (object) null, speed);
      }
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      if (buttonCAnonStorey434.callback == null)
        return;
      // ISSUE: reference to a compiler-generated field
      buttonCAnonStorey434.callback((object) this);
    }
  }

  public bool CanShowNewDeckButton()
  {
    if (AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
      return CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK).Count < 18;
    return false;
  }

  public void SetEditingTraySection(int index)
  {
    this.m_editingTraySection = this.m_traySections[index];
    this.m_centeringDeckList = this.m_editingTraySection.m_deckBox.GetPositionIndex();
  }

  private bool IsEditingCards()
  {
    return CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing) != null;
  }

  private void OnDeckBoxVisualOver(CollectionDeckBoxVisual deckBox)
  {
    if (deckBox.IsLocked() || UniversalInputManager.Get().IsTouchMode())
      return;
    if (this.IsEditingCards() && (UnityEngine.Object) this.m_deckInfoTooltip != (UnityEngine.Object) null)
    {
      this.ShowDeckInfo();
    }
    else
    {
      if (!this.IsModeTryingOrActive())
        return;
      deckBox.ShowDeleteButton(true);
    }
  }

  private void OnDeckBoxVisualOut(CollectionDeckBoxVisual deckBox)
  {
    if (deckBox.IsLocked())
      return;
    if (UniversalInputManager.Get().IsTouchMode())
    {
      if (!((UnityEngine.Object) this.m_deckInfoTooltip != (UnityEngine.Object) null) || !this.m_deckInfoTooltip.IsShown())
        return;
      deckBox.SetHighlightState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
    }
    else
    {
      if (UniversalInputManager.Get().InputIsOver(deckBox.m_deleteButton.gameObject))
        return;
      deckBox.ShowDeleteButton(false);
    }
  }

  private void OnDeckBoxVisualPress(CollectionDeckBoxVisual deckBox)
  {
    if (deckBox.IsLocked())
      return;
    deckBox.enabled = false;
  }

  private void OnDeckBoxVisualRelease(TraySection traySection)
  {
    if (traySection.m_deckBox.IsLocked())
      return;
    CollectionDeckBoxVisual deckBox = traySection.m_deckBox;
    deckBox.enabled = true;
    if ((UnityEngine.Object) this.m_scrollbar != (UnityEngine.Object) null && this.m_scrollbar.IsTouchDragging() || CollectionDeckTray.Get().IsUpdatingTrayMode())
      return;
    long deckId = deckBox.GetDeckID();
    CollectionDeck deck = CollectionManager.Get().GetDeck(deckId);
    if (deck.IsBeingDeleted())
      Log.JMac.Print(string.Format("DeckTrayDeckListContent.OnDeckBoxVisualRelease(): cannot edit deck {0}; it is being deleted", (object) deck));
    else if (deck.IsSavingChanges())
      Log.All.PrintWarning("DeckTrayDeckListContent.OnDeckBoxVisualRelease(): cannot edit deck {0}; waiting for changes to be saved", (object) deck);
    else if (this.IsEditingCards())
    {
      if (!UniversalInputManager.Get().IsTouchMode())
      {
        this.RenameCurrentlyEditingDeck();
      }
      else
      {
        if (!((UnityEngine.Object) this.m_deckInfoTooltip != (UnityEngine.Object) null) || this.m_deckInfoTooltip.IsShown())
          return;
        this.ShowDeckInfo();
      }
    }
    else
    {
      if (!this.IsModeActive())
        return;
      this.m_editingTraySection = traySection;
      this.m_centeringDeckList = this.m_editingTraySection.m_deckBox.GetPositionIndex();
      CollectionManagerDisplay.Get().RequestContentsToShowDeck(deckId);
      this.m_newDeckButton.SetEnabled(false);
      CollectionManagerDisplay.Get().HideDeckHelpPopup();
      CollectionManagerDisplay.Get().HideSetFilterTutorial();
      Options.Get().SetBool(Option.HAS_STARTED_A_DECK, true);
    }
  }

  private void OnNewDeckButtonPress()
  {
    if (!this.IsModeActive() || (UnityEngine.Object) this.m_scrollbar != (UnityEngine.Object) null && this.m_scrollbar.IsTouchDragging())
      return;
    SoundManager.Get().LoadAndPlay("Hub_Click");
    this.StartCreateNewDeck();
  }

  private void StartCreateNewDeck()
  {
    PresenceMgr.Get().SetStatus((Enum) PresenceStatus.DECKEDITOR);
    this.ShowNewDeckButton(false, (CollectionNewDeckButton.DelOnAnimationFinished) null);
    CollectionManagerDisplay.Get().EnterSelectNewDeckHeroMode();
  }

  private void EndCreateNewDeck(bool newDeck)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTrayDeckListContent.\u003CEndCreateNewDeck\u003Ec__AnonStorey435 deckCAnonStorey435 = new DeckTrayDeckListContent.\u003CEndCreateNewDeck\u003Ec__AnonStorey435();
    // ISSUE: reference to a compiler-generated field
    deckCAnonStorey435.newDeck = newDeck;
    // ISSUE: reference to a compiler-generated field
    deckCAnonStorey435.\u003C\u003Ef__this = this;
    CollectionManagerDisplay.Get().ExitSelectNewDeckHeroMode();
    // ISSUE: reference to a compiler-generated method
    this.ShowNewDeckButton(true, new CollectionNewDeckButton.DelOnAnimationFinished(deckCAnonStorey435.\u003C\u003Em__2CE));
  }

  private void DeleteQueuedDecks(bool force = false)
  {
    if (this.m_decksToDelete.Count == 0 || !this.IsModeActive() && !force)
      return;
    using (List<long>.Enumerator enumerator = this.m_decksToDelete.GetEnumerator())
    {
      while (enumerator.MoveNext())
        CollectionManager.Get().SendDeleteDeck(enumerator.Current);
    }
    this.m_decksToDelete.Clear();
  }

  private void OnDeckDeleted(long deckID)
  {
    this.m_waitingToDeleteDeck = false;
    this.StartCoroutine(this.DeleteDeckAnimation(deckID, (Action) null));
  }

  [DebuggerHidden]
  private IEnumerator DeleteDeckAnimation(long deckID, Action callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTrayDeckListContent.\u003CDeleteDeckAnimation\u003Ec__Iterator305() { deckID = deckID, callback = callback, \u003C\u0024\u003EdeckID = deckID, \u003C\u0024\u003Ecallback = callback, \u003C\u003Ef__this = this };
  }

  private void FireDeckCountChangedEvent()
  {
    DeckTrayDeckListContent.DeckCountChanged[] array = this.m_deckCountChangedListeners.ToArray();
    int count = CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK).Count;
    foreach (DeckTrayDeckListContent.DeckCountChanged deckCountChanged in array)
      deckCountChanged(count);
  }

  private void FireBusyWithDeckEvent(bool busy)
  {
    foreach (DeckTrayDeckListContent.BusyWithDeck busyWithDeck in this.m_busyWithDeckListeners.ToArray())
      busyWithDeck(busy);
  }

  private int GetTotalDeckBoxesInUse()
  {
    int num = 0;
    using (List<TraySection>.Enumerator enumerator = this.m_traySections.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.m_deckBox.GetDeckID() > -1L)
          ++num;
      }
    }
    return num;
  }

  private void OnFavoriteHeroChanged(TAG_CLASS heroClass, NetCache.CardDefinition favoriteHero, object userData)
  {
    this.UpdateDeckTrayVisuals();
  }

  private int UpdateDeckTrayVisuals()
  {
    List<CollectionDeck> collectionDeckList = SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL ? CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK) : CollectionManager.Get().GetDecks(DeckType.TAVERN_BRAWL_DECK);
    int num = collectionDeckList.Count;
    if (!AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
      num = 9;
    for (int index = 0; index < num && index < this.m_traySections.Count; ++index)
    {
      if (index < collectionDeckList.Count)
      {
        CollectionDeck deck = collectionDeckList[index];
        this.m_traySections[index].m_deckBox.AssignFromCollectionDeck(deck, false);
      }
      this.m_traySections[index].m_deckBox.SetIsLocked(index >= collectionDeckList.Count);
    }
    return collectionDeckList.Count;
  }

  private void SwapEditTrayIfNeeded(long editDeckID)
  {
    if (editDeckID < 0L)
      return;
    TraySection traySection = (TraySection) null;
    using (List<TraySection>.Enumerator enumerator = this.m_traySections.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TraySection current = enumerator.Current;
        if (current.m_deckBox.GetDeckID() == editDeckID)
        {
          traySection = current;
          break;
        }
      }
    }
    if ((UnityEngine.Object) traySection == (UnityEngine.Object) this.m_editingTraySection)
      return;
    CollectionDeckTray.Get().TryEnableScrollbar();
    this.m_scrollbar.SetScrollImmediate((float) traySection.m_deckBox.GetPositionIndex() / (float) (this.GetTotalDeckBoxesInUse() - 1));
    CollectionDeckTray.Get().SaveScrollbarPosition(CollectionDeckTray.DeckContentTypes.Decks);
    this.m_editingTraySection.m_deckBox.transform.localScale = CollectionDeckBoxVisual.SCALED_DOWN_LOCAL_SCALE;
    Vector3 zero = Vector3.zero;
    zero.y = 1.273138f;
    this.m_editingTraySection.m_deckBox.transform.localPosition = zero;
    this.m_editingTraySection.m_deckBox.Hide();
    this.m_editingTraySection.m_deckBox.EnableButtonAnimation();
    traySection.m_deckBox.transform.localScale = CollectionDeckBoxVisual.SCALED_UP_LOCAL_SCALE;
    traySection.m_deckBox.transform.parent = (Transform) null;
    traySection.m_deckBox.transform.position = this.m_deckEditTopPos.position;
    traySection.ShowDeckBoxNoAnim();
    traySection.m_deckBox.SetEnabled(true);
    this.m_editingTraySection = traySection;
  }

  public delegate void BusyWithDeck(bool busy);

  public delegate void DeckCountChanged(int deckCount);
}
