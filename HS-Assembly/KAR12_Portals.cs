﻿// Decompiled with JetBrains decompiler
// Type: KAR12_Portals
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class KAR12_Portals : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();
  private const string m_introSpellPath = "Nazra_PreMissionSummon";
  private Spell m_introSpellInstance;

  public override void StartGameplaySoundtracks()
  {
    MusicManager.Get().StartPlaylist(MusicPlaylistType.InGame_KarazhanFreeMedivh);
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Moroes_Male_Human_FinalThirdSequence_01");
    this.PreloadSound("VO_Moroes_Male_Human_FinalAltTurn5_01");
    this.PreloadSound("VO_Moroes_Male_Human_FinalMaidenTurn7_03");
    this.PreloadSound("VO_Malchezaar_Male_Demon_FinalThirdSequence_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_FinalThirdSequence_04");
    this.PreloadSound("VO_Malchezaar_Male_Demon_FinalMalchezaarTurn7_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_FinalMalchezaarSacrificialPact_03");
    this.PreloadSound("VO_Malchezaar_Male_Demon_FinalMalchezaarMedivhSkin_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_FinalMalchezaarJaraxxus_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_FinalMalchezaarHeroPower_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_FinalMalchezaarEmoteResponse_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_Brawl_06");
    this.PreloadSound("VO_Malchezaar_Male_Demon_FinalAltOpening_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_EmoteParty_01");
    this.PreloadSound("VO_Medivh_Male_Human_FinalThirdSequence_01");
    this.PreloadSound("VO_Medivh_Male_Human_FinalThirdSequence_03");
    this.PreloadSound("VO_Medivh_Male_Human_FinalMedivhMedivhSkin_01");
    this.PreloadSound("VO_Medivh_Male_Human_FinalMalchezaarTurn5_01");
    this.PreloadSound("VO_Medivh_Male_Human_FinalMalchezaarWin_01");
    this.PreloadSound("VO_Jaraxxus_Male_Demon_FinalMalchezaarJaraxxus_01");
    this.PreloadSound("VO_Jaraxxus_Male_Demon_FinalMalchezaarJaraxxus_02");
    this.PreloadSound("VO_Medivh_Male_Human_FinalThirdSequence_02");
    this.PreloadSound("VO_Moroes_Male_Human_FinalSecondSequence_01");
    this.PreloadSound("VO_Nazra_Female_Orc_FinalNazraEmoteResponse_01");
    this.PreloadSound("VO_Nazra_Female_Orc_FinalNazraGrom_01");
    this.PreloadSound("VO_Nazra_Female_Orc_FinalNazraChogall_02");
    this.PreloadSound("VO_Moroes_Male_Human_FinalMaidenTurn1_01");
    this.PreloadSound("VO_Moroes_Male_Human_FinalNazraTurn7_02");
    this.PreloadSound("VO_Nazra_Female_Orc_FinalNazraHeroPower_01");
    this.PreloadSound("VO_Moroes_Male_Human_FinalTurn1_02");
  }

  protected override void PlayEmoteResponse(EmoteType emoteType, CardSoundSpell emoteSpell)
  {
    if (!MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS.Contains(emoteType))
      return;
    Actor actor = GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor();
    string cardId = GameState.Get().GetOpposingSidePlayer().GetHero().GetCardId();
    if (cardId == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (KAR12_Portals.\u003C\u003Ef__switch\u0024map9D == null)
    {
      // ISSUE: reference to a compiler-generated field
      KAR12_Portals.\u003C\u003Ef__switch\u0024map9D = new Dictionary<string, int>(4)
      {
        {
          "KARA_13_01",
          0
        },
        {
          "KARA_13_01H",
          0
        },
        {
          "KARA_13_06",
          1
        },
        {
          "KARA_13_06H",
          1
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!KAR12_Portals.\u003C\u003Ef__switch\u0024map9D.TryGetValue(cardId, out num))
      return;
    if (num != 0)
    {
      if (num != 1)
        return;
      Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_Malchezaar_Male_Demon_FinalAltOpening_01", Notification.SpeechBubbleDirection.TopRight, actor, 3f, 1f, true, false, 0.0f));
    }
    else
      Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_Nazra_Female_Orc_FinalNazraEmoteResponse_01", Notification.SpeechBubbleDirection.TopRight, actor, 3f, 1f, true, false, 0.0f));
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR12_Portals.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1BB() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  public override void NotifyOfRealTimeTagChange(Entity entity, Network.HistTagChange tagChange)
  {
    if (tagChange.Tag != 6 || tagChange.Value != 9)
      return;
    if ((Object) TurnTimer.Get() != (Object) null)
      TurnTimer.Get().OnEndTurnRequested();
    EndTurnButton.Get().OnEndTurnRequested();
    GameState.Get().UpdateOptionHighlights();
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR12_Portals.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1BC() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR12_Portals.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1BD() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR12_Portals.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator1BE() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR12_Portals.\u003CHandleGameOverWithTiming\u003Ec__Iterator1BF() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }

  private void OnIntroSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if ((Object) this.m_introSpellInstance != (Object) spell || this.m_introSpellInstance.GetActiveState() != SpellStateType.NONE)
      return;
    Object.Destroy((Object) this.m_introSpellInstance.gameObject, 5f);
    this.m_introSpellInstance = (Spell) null;
  }

  protected MissionEntity.ShouldPlayValue ShouldPlayLongCutscene()
  {
    return MissionEntity.ShouldPlayValue.Once;
  }

  public bool ShouldPlayLongMidmissionCutscene()
  {
    return this.ShouldPlayLine("VO_Malchezaar_Male_Demon_FinalThirdSequence_01", new MissionEntity.ShouldPlay(this.ShouldPlayLongCutscene));
  }

  [DebuggerHidden]
  public override IEnumerator DoCustomIntro(Card friendlyHero, Card enemyHero, HeroLabel friendlyHeroLabel, HeroLabel enemyHeroLabel, GameStartVsLetters versusText)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR12_Portals.\u003CDoCustomIntro\u003Ec__Iterator1C0() { friendlyHeroLabel = friendlyHeroLabel, enemyHeroLabel = enemyHeroLabel, versusText = versusText, friendlyHero = friendlyHero, enemyHero = enemyHero, \u003C\u0024\u003EfriendlyHeroLabel = friendlyHeroLabel, \u003C\u0024\u003EenemyHeroLabel = enemyHeroLabel, \u003C\u0024\u003EversusText = versusText, \u003C\u0024\u003EfriendlyHero = friendlyHero, \u003C\u0024\u003EenemyHero = enemyHero, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  public IEnumerator FlipInHeroPower()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    KAR12_Portals.\u003CFlipInHeroPower\u003Ec__Iterator1C1 powerCIterator1C1 = new KAR12_Portals.\u003CFlipInHeroPower\u003Ec__Iterator1C1();
    return (IEnumerator) powerCIterator1C1;
  }

  public override void OnCustomIntroCancelled(Card friendlyHero, Card enemyHero, HeroLabel friendlyHeroLabel, HeroLabel enemyHeroLabel, GameStartVsLetters versusText)
  {
    if (!((Object) this.m_introSpellInstance != (Object) null))
      return;
    this.m_introSpellInstance.ActivateState(SpellStateType.CANCEL);
  }
}
