﻿// Decompiled with JetBrains decompiler
// Type: BnetBarMenuButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BnetBarMenuButton : PegUIElement
{
  private int m_phoneBarStatus = -1;
  public GameObject m_highlight;
  public GameObject m_phoneBar;
  public Transform m_phoneBarOneElementBone;
  public Transform m_phoneBarTwoElementBone;
  private bool m_selected;

  public void LockHighlight(bool isLocked)
  {
    this.m_highlight.SetActive(isLocked);
  }

  protected override void Awake()
  {
    base.Awake();
    this.UpdateHighlight();
  }

  public bool IsSelected()
  {
    return this.m_selected;
  }

  public void SetSelected(bool enable)
  {
    if (enable == this.m_selected)
      return;
    this.m_selected = enable;
    this.UpdateHighlight();
  }

  public void SetPhoneStatusBarState(int nElements)
  {
    if (nElements == this.m_phoneBarStatus)
      return;
    this.m_phoneBarStatus = nElements;
    switch (nElements)
    {
      case 0:
        this.m_phoneBar.SetActive(false);
        break;
      case 1:
        this.m_phoneBar.SetActive(true);
        iTween.Stop(this.m_phoneBar);
        iTween.MoveTo(this.m_phoneBar, iTween.Hash((object) "position", (object) this.m_phoneBarOneElementBone.position, (object) "time", (object) 1f, (object) "isLocal", (object) false, (object) "easetype", (object) iTween.EaseType.easeOutExpo, (object) "onupdate", (object) "OnStatusBarUpdate", (object) "onupdatetarget", (object) this.gameObject));
        break;
      case 2:
        this.m_phoneBar.SetActive(true);
        iTween.Stop(this.m_phoneBar);
        iTween.MoveTo(this.m_phoneBar, iTween.Hash((object) "position", (object) this.m_phoneBarTwoElementBone.position, (object) "time", (object) 1f, (object) "isLocal", (object) false, (object) "easetype", (object) iTween.EaseType.easeOutExpo, (object) "onupdate", (object) "OnStatusBarUpdate", (object) "onupdatetarget", (object) this.gameObject));
        break;
      default:
        Debug.LogError((object) ("Invalid phone status bar state " + (object) nElements));
        break;
    }
  }

  public void OnStatusBarUpdate()
  {
    BnetBar.Get().UpdateLayout();
  }

  private bool ShouldBeHighlighted()
  {
    if (!this.m_selected)
      return this.GetInteractionState() == PegUIElement.InteractionState.Over;
    return true;
  }

  protected virtual void UpdateHighlight()
  {
    bool flag = this.ShouldBeHighlighted();
    if (!flag && (Object) GameMenu.Get() != (Object) null && GameMenu.Get().IsShown())
      flag = true;
    if (this.m_highlight.activeSelf == flag)
      return;
    this.m_highlight.SetActive(flag);
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    TooltipPanel tooltipPanel = this.GetComponent<TooltipZone>().ShowTooltip(GameStrings.Get("GLOBAL_TOOLTIP_MENU_HEADER"), GameStrings.Get("GLOBAL_TOOLTIP_MENU_DESC"), 0.7f, true);
    SceneUtils.SetLayer(tooltipPanel.gameObject, GameLayer.BattleNet);
    tooltipPanel.transform.localEulerAngles = new Vector3(270f, 0.0f, 0.0f);
    tooltipPanel.transform.localScale = new Vector3(82.35294f, 70f, 90.32258f);
    TransformUtil.SetPoint((Component) tooltipPanel, Anchor.BOTTOM, this.gameObject, Anchor.TOP, new Vector3(-98.22766f, 0.0f, 0.0f));
    SoundManager.Get().LoadAndPlay("Small_Mouseover");
    this.UpdateHighlight();
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.GetComponent<TooltipZone>().HideTooltip();
    this.UpdateHighlight();
  }
}
