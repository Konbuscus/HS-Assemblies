﻿// Decompiled with JetBrains decompiler
// Type: FriendlyDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class FriendlyDisplay : MonoBehaviour
{
  public GameObject m_deckPickerTrayContainer;
  private static FriendlyDisplay s_instance;
  private DeckPickerTrayDisplay m_deckPickerTray;

  private void Awake()
  {
    FriendlyDisplay.s_instance = this;
    AssetLoader.Get().LoadActor(!(bool) UniversalInputManager.UsePhoneUI ? "DeckPickerTray" : "DeckPickerTray_phone", (AssetLoader.GameObjectCallback) ((name, go, data) =>
    {
      if ((Object) go == (Object) null)
      {
        UnityEngine.Debug.LogError((object) "Unable to load DeckPickerTray.");
      }
      else
      {
        this.m_deckPickerTray = go.GetComponent<DeckPickerTrayDisplay>();
        if ((Object) this.m_deckPickerTray == (Object) null)
        {
          UnityEngine.Debug.LogError((object) "DeckPickerTrayDisplay component not found in DeckPickerTray object.");
        }
        else
        {
          GameUtils.SetParent((Component) this.m_deckPickerTray, this.m_deckPickerTrayContainer, false);
          this.m_deckPickerTray.SetHeaderText(GameStrings.Get(!FriendChallengeMgr.Get().IsChallengeTavernBrawl() ? "GLOBAL_FRIEND_CHALLENGE_TITLE" : "GLOBAL_TAVERN_BRAWL"));
          this.m_deckPickerTray.Init();
          this.DisableOtherModeStuff();
          NetCache.Get().RegisterScreenFriendly(new NetCache.NetCacheCallback(this.OnNetCacheReady));
          MusicManager.Get().StartPlaylist(!FriendChallengeMgr.Get().IsChallengeTavernBrawl() ? MusicPlaylistType.UI_Friendly : MusicPlaylistType.UI_TavernBrawl);
        }
      }
    }), (object) null, false);
  }

  private void OnDestroy()
  {
    FriendlyDisplay.s_instance = (FriendlyDisplay) null;
  }

  public static FriendlyDisplay Get()
  {
    return FriendlyDisplay.s_instance;
  }

  public void Unload()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
  }

  private void DisableOtherModeStuff()
  {
    if (SceneMgr.Get().GetPrevMode() == SceneMgr.Mode.GAMEPLAY)
      return;
    Camera screenEffectsCamera = CameraUtils.FindFullScreenEffectsCamera(true);
    if (!((Object) screenEffectsCamera != (Object) null))
      return;
    screenEffectsCamera.GetComponent<FullScreenEffects>().Disable();
  }

  private void OnNetCacheReady()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    this.StartCoroutine(this.ShowQuestPopups());
  }

  [DebuggerHidden]
  private IEnumerator ShowQuestPopups()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FriendlyDisplay.\u003CShowQuestPopups\u003Ec__IteratorA5() { \u003C\u003Ef__this = this };
  }
}
