﻿// Decompiled with JetBrains decompiler
// Type: AdventureRewardsChest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class AdventureRewardsChest : MonoBehaviour
{
  private const string s_EventBlinkChest = "BlinkChest";
  private const string s_EventOpenChest = "OpenChest";
  private const string s_EventSlamInCheckmark = "SlamInCheckmark";
  private const string s_EventBurstCheckmark = "BurstCheckmark";
  private const string s_EventFadeInChest = "FadeChestIn";
  private const string s_EventFadeOutChest = "FadeChestOut";
  [CustomEditField(Sections = "Event Table")]
  public StateEventTable m_EventTable;
  [CustomEditField(Sections = "UI")]
  public PegUIElement m_ChestClickArea;
  [CustomEditField(Sections = "UI")]
  public GameObject m_CheckmarkContainer;
  [CustomEditField(Sections = "UI")]
  public GameObject m_ChestContainer;
  [CustomEditField(Sections = "UI")]
  public MeshRenderer m_ChestQuad;

  public bool m_fadedOut { get; private set; }

  public void AddChestEventListener(UIEventType type, UIEvent.Handler handler)
  {
    this.m_ChestClickArea.AddEventListener(type, handler);
  }

  public void RemoveChestEventListener(UIEventType type, UIEvent.Handler handler)
  {
    this.m_ChestClickArea.RemoveEventListener(type, handler);
  }

  public void SlamInCheckmark()
  {
    this.ShowCheckmark();
    this.m_EventTable.TriggerState("SlamInCheckmark", true, (string) null);
  }

  public void ShowCheckmark()
  {
    this.m_CheckmarkContainer.SetActive(true);
    this.m_ChestContainer.SetActive(false);
  }

  public void BurstCheckmark()
  {
    this.ShowCheckmark();
    this.m_EventTable.TriggerState("BurstCheckmark", true, (string) null);
  }

  public void BlinkChest()
  {
    if (this.m_fadedOut)
      return;
    this.ShowCheckmark();
    this.m_EventTable.TriggerState("BlinkChest", true, (string) null);
  }

  public void ShowChest()
  {
    this.m_CheckmarkContainer.SetActive(false);
    this.m_ChestContainer.SetActive(true);
  }

  public void HideAll()
  {
    this.m_CheckmarkContainer.SetActive(false);
    this.m_ChestContainer.SetActive(false);
  }

  public void Enable(bool enable)
  {
    if (!((Object) this.m_ChestClickArea != (Object) null))
      return;
    this.m_ChestClickArea.gameObject.SetActive(enable);
  }

  public void FadeInChest()
  {
    this.m_EventTable.TriggerState("FadeChestIn", true, (string) null);
    this.m_fadedOut = false;
  }

  public void FadeOutChest()
  {
    this.m_EventTable.TriggerState("FadeChestOut", true, (string) null);
    this.m_fadedOut = true;
  }

  public void FadeOutChestImmediate()
  {
    Color white = Color.white;
    white.a = 0.0f;
    this.m_ChestQuad.material.SetColor("_Color", white);
    this.m_fadedOut = true;
  }
}
