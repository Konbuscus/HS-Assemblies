﻿// Decompiled with JetBrains decompiler
// Type: ScreenResizeDetector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ScreenResizeDetector : MonoBehaviour
{
  private List<ScreenResizeDetector.SizeChangedListener> m_sizeChangedListeners = new List<ScreenResizeDetector.SizeChangedListener>();
  private float m_screenWidth;
  private float m_screenHeight;

  private void Awake()
  {
    this.SaveScreenSize();
  }

  private void OnPreCull()
  {
    if (Mathf.Approximately(this.m_screenWidth, (float) Screen.width) && Mathf.Approximately(this.m_screenHeight, (float) Screen.height))
      return;
    this.SaveScreenSize();
    this.FireSizeChangedEvent();
  }

  public bool AddSizeChangedListener(ScreenResizeDetector.SizeChangedCallback callback)
  {
    return this.AddSizeChangedListener(callback, (object) null);
  }

  public bool AddSizeChangedListener(ScreenResizeDetector.SizeChangedCallback callback, object userData)
  {
    ScreenResizeDetector.SizeChangedListener sizeChangedListener = new ScreenResizeDetector.SizeChangedListener();
    sizeChangedListener.SetCallback(callback);
    sizeChangedListener.SetUserData(userData);
    if (this.m_sizeChangedListeners.Contains(sizeChangedListener))
      return false;
    this.m_sizeChangedListeners.Add(sizeChangedListener);
    return true;
  }

  public bool RemoveSizeChangedListener(ScreenResizeDetector.SizeChangedCallback callback)
  {
    return this.RemoveSizeChangedListener(callback, (object) null);
  }

  public bool RemoveSizeChangedListener(ScreenResizeDetector.SizeChangedCallback callback, object userData)
  {
    ScreenResizeDetector.SizeChangedListener sizeChangedListener = new ScreenResizeDetector.SizeChangedListener();
    sizeChangedListener.SetCallback(callback);
    sizeChangedListener.SetUserData(userData);
    return this.m_sizeChangedListeners.Remove(sizeChangedListener);
  }

  private void SaveScreenSize()
  {
    this.m_screenWidth = (float) Screen.width;
    this.m_screenHeight = (float) Screen.height;
  }

  private void FireSizeChangedEvent()
  {
    foreach (ScreenResizeDetector.SizeChangedListener sizeChangedListener in this.m_sizeChangedListeners.ToArray())
      sizeChangedListener.Fire();
  }

  private class SizeChangedListener : EventListener<ScreenResizeDetector.SizeChangedCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  public delegate void SizeChangedCallback(object userData);
}
