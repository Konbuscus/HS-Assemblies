﻿// Decompiled with JetBrains decompiler
// Type: CardBackInfoManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[CustomEditClass]
public class CardBackInfoManager : MonoBehaviour
{
  public float m_animationTime = 0.5f;
  public GameObject m_previewPane;
  public GameObject m_cardBackContainer;
  public UberText m_title;
  public UberText m_description;
  public UIBButton m_favoriteButton;
  public PegUIElement m_offClicker;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_enterPreviewSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_exitPreviewSound;
  private int m_currentCardBackIdx;
  private GameObject m_currentCardBack;
  private bool m_animating;
  private static CardBackInfoManager s_instance;

  public bool IsPreviewing { get; private set; }

  public static CardBackInfoManager Get()
  {
    if ((UnityEngine.Object) CardBackInfoManager.s_instance == (UnityEngine.Object) null)
      CardBackInfoManager.s_instance = AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "CardBackInfoManager" : "CardBackInfoManager_phone", true, false).GetComponent<CardBackInfoManager>();
    return CardBackInfoManager.s_instance;
  }

  private void Awake()
  {
    this.m_previewPane.SetActive(false);
    this.SetupUI();
  }

  private void OnDestroy()
  {
    CardBackInfoManager.s_instance = (CardBackInfoManager) null;
  }

  public void EnterPreview(CollectionCardVisual cardVisual)
  {
    Actor actor = cardVisual.GetActor();
    if ((UnityEngine.Object) actor == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "Unable to obtain actor from card visual.");
    }
    else
    {
      CollectionCardBack component = actor.GetComponent<CollectionCardBack>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        Debug.LogError((object) "Actor does not contain a CollectionCardBack component!");
      else
        this.EnterPreview(component.GetCardBackId(), cardVisual);
    }
  }

  public void EnterPreview(int cardBackIdx, CollectionCardVisual cardVisual)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CardBackInfoManager.\u003CEnterPreview\u003Ec__AnonStorey377 previewCAnonStorey377 = new CardBackInfoManager.\u003CEnterPreview\u003Ec__AnonStorey377();
    // ISSUE: reference to a compiler-generated field
    previewCAnonStorey377.cardBackIdx = cardBackIdx;
    // ISSUE: reference to a compiler-generated field
    previewCAnonStorey377.cardVisual = cardVisual;
    // ISSUE: reference to a compiler-generated field
    previewCAnonStorey377.\u003C\u003Ef__this = this;
    if (this.m_animating)
      return;
    if ((UnityEngine.Object) this.m_currentCardBack != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_currentCardBack);
      this.m_currentCardBack = (GameObject) null;
    }
    this.m_animating = true;
    // ISSUE: reference to a compiler-generated field
    CardBackDbfRecord record = GameDbf.CardBack.GetRecord(previewCAnonStorey377.cardBackIdx);
    this.m_title.Text = (string) record.Name;
    this.m_description.Text = (string) record.Description;
    bool flag = false;
    if (!CollectionManager.Get().IsInEditMode())
    {
      int defaultCardBack = NetCache.Get().GetNetObject<NetCache.NetCacheCardBacks>().DefaultCardBack;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      flag = CardBackManager.Get().IsCardBackOwned(previewCAnonStorey377.cardBackIdx) && defaultCardBack != previewCAnonStorey377.cardBackIdx;
    }
    this.m_favoriteButton.SetEnabled(flag);
    this.m_favoriteButton.Flip(flag);
    // ISSUE: reference to a compiler-generated field
    this.m_currentCardBackIdx = previewCAnonStorey377.cardBackIdx;
    this.IsPreviewing = true;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    if (!CardBackManager.Get().LoadCardBackByIndex(previewCAnonStorey377.cardBackIdx, new CardBackManager.LoadCardBackData.LoadCardBackCallback(previewCAnonStorey377.\u003C\u003Em__92), "Card_Hidden"))
    {
      // ISSUE: reference to a compiler-generated field
      Debug.LogError((object) string.Format("Unable to load card back ID {0} for preview.", (object) previewCAnonStorey377.cardBackIdx));
      this.m_animating = false;
    }
    if (!string.IsNullOrEmpty(this.m_enterPreviewSound))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_enterPreviewSound));
    FullScreenFXMgr.Get().StartStandardBlurVignette(this.m_animationTime);
  }

  public void CancelPreview()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CardBackInfoManager.\u003CCancelPreview\u003Ec__AnonStorey378 previewCAnonStorey378 = new CardBackInfoManager.\u003CCancelPreview\u003Ec__AnonStorey378();
    // ISSUE: reference to a compiler-generated field
    previewCAnonStorey378.\u003C\u003Ef__this = this;
    if (this.m_animating)
      return;
    // ISSUE: reference to a compiler-generated field
    previewCAnonStorey378.origScale = this.m_previewPane.transform.localScale;
    this.IsPreviewing = false;
    this.m_animating = true;
    // ISSUE: reference to a compiler-generated method
    iTween.ScaleTo(this.m_previewPane, iTween.Hash((object) "scale", (object) new Vector3(0.01f, 0.01f, 0.01f), (object) "time", (object) this.m_animationTime, (object) "easeType", (object) iTween.EaseType.easeOutCirc, (object) "oncomplete", (object) new Action<object>(previewCAnonStorey378.\u003C\u003Em__93)));
    // ISSUE: reference to a compiler-generated method
    iTween.ScaleTo(this.m_currentCardBack, iTween.Hash((object) "scale", (object) new Vector3(0.01f, 0.01f, 0.01f), (object) "time", (object) this.m_animationTime, (object) "easeType", (object) iTween.EaseType.easeOutCirc, (object) "oncomplete", (object) new Action<object>(previewCAnonStorey378.\u003C\u003Em__94)));
    if (!string.IsNullOrEmpty(this.m_exitPreviewSound))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_exitPreviewSound));
    FullScreenFXMgr.Get().EndStandardBlurVignette(this.m_animationTime, (FullScreenFXMgr.EffectListener) null);
  }

  private void SetupUI()
  {
    this.m_favoriteButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e =>
    {
      this.SetFavorite();
      this.CancelPreview();
    }));
    this.m_offClicker.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.CancelPreview()));
    this.m_offClicker.AddEventListener(UIEventType.RIGHTCLICK, (UIEvent.Handler) (e => this.CancelPreview()));
  }

  private void SetFavorite()
  {
    if (this.m_currentCardBackIdx == NetCache.Get().GetNetObject<NetCache.NetCacheCardBacks>().DefaultCardBack)
      return;
    Network.SetDefaultCardBack(this.m_currentCardBackIdx);
  }
}
