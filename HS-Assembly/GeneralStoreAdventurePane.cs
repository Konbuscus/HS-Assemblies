﻿// Decompiled with JetBrains decompiler
// Type: GeneralStoreAdventurePane
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class GeneralStoreAdventurePane : GeneralStorePane
{
  private List<GeneralStoreAdventureSelectorButton> m_adventureButtons = new List<GeneralStoreAdventureSelectorButton>();
  [SerializeField]
  private Vector3 m_adventureButtonSpacing;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_adventureSelectionSound;
  private GeneralStoreAdventureContent m_adventureContent;
  private bool m_paneInitialized;

  [CustomEditField(Sections = "Layout")]
  public Vector3 AdventureButtonSpacing
  {
    get
    {
      return this.m_adventureButtonSpacing;
    }
    set
    {
      this.m_adventureButtonSpacing = value;
      this.UpdateAdventureButtonPositions();
    }
  }

  private void Awake()
  {
    this.m_adventureContent = this.m_parentContent as GeneralStoreAdventureContent;
    if (!((Object) this.m_adventureContent == (Object) null))
      return;
    Debug.LogError((object) "m_adventureContent is not the correct type: GeneralStoreAdventureContent");
  }

  public override void StoreShown(bool isCurrent)
  {
    if (!this.m_paneInitialized)
    {
      this.m_paneInitialized = true;
      this.SetUpAdventureButtons();
    }
    this.UpdateAdventureButtonPositions();
    this.SetupInitialSelectedAdventure();
    if (!AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
      return;
    AchieveManager.Get().NotifyOfClick(Achievement.ClickTriggerType.BUTTON_ADVENTURE);
  }

  protected override void OnRefresh()
  {
    using (List<GeneralStoreAdventureSelectorButton>.Enumerator enumerator = this.m_adventureButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateState();
    }
  }

  private void SetUpAdventureButtons()
  {
    using (Map<int, StoreAdventureDef>.Enumerator enumerator = this.m_adventureContent.GetStoreAdventureDefs().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, StoreAdventureDef> current = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GeneralStoreAdventurePane.\u003CSetUpAdventureButtons\u003Ec__AnonStorey3EC buttonsCAnonStorey3Ec = new GeneralStoreAdventurePane.\u003CSetUpAdventureButtons\u003Ec__AnonStorey3EC();
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey3Ec.\u003C\u003Ef__this = this;
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey3Ec.adventureId = (AdventureDbId) current.Key;
        Network.Bundle bundle;
        bool productExists;
        // ISSUE: reference to a compiler-generated field
        StoreManager.Get().GetAvailableAdventureBundle(buttonsCAnonStorey3Ec.adventureId, GeneralStoreAdventureContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, out bundle, out productExists);
        if (productExists)
        {
          string storeButtonPrefab = current.Value.m_storeButtonPrefab;
          GameObject gameObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(storeButtonPrefab), true, false);
          if (!((Object) gameObject == (Object) null))
          {
            // ISSUE: reference to a compiler-generated field
            buttonsCAnonStorey3Ec.advButton = gameObject.GetComponent<GeneralStoreAdventureSelectorButton>();
            // ISSUE: reference to a compiler-generated field
            if ((Object) buttonsCAnonStorey3Ec.advButton == (Object) null)
            {
              Debug.LogError((object) string.Format("{0} does not contain GeneralStoreAdventureSelectorButton component.", (object) storeButtonPrefab));
              Object.Destroy((Object) gameObject);
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              GameUtils.SetParent((Component) buttonsCAnonStorey3Ec.advButton, this.m_paneContainer, true);
              // ISSUE: reference to a compiler-generated field
              SceneUtils.SetLayer((Component) buttonsCAnonStorey3Ec.advButton, this.m_paneContainer.layer);
              // ISSUE: reference to a compiler-generated field
              // ISSUE: reference to a compiler-generated method
              buttonsCAnonStorey3Ec.advButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(buttonsCAnonStorey3Ec.\u003C\u003Em__1B6));
              // ISSUE: reference to a compiler-generated field
              // ISSUE: reference to a compiler-generated field
              buttonsCAnonStorey3Ec.advButton.SetAdventureId(buttonsCAnonStorey3Ec.adventureId);
              // ISSUE: reference to a compiler-generated field
              this.m_adventureButtons.Add(buttonsCAnonStorey3Ec.advButton);
            }
          }
        }
      }
    }
    this.UpdateAdventureButtonPositions();
  }

  private void OnAdventureSelectorButtonClicked(GeneralStoreAdventureSelectorButton btn, AdventureDbId adventureId)
  {
    if (!this.m_parentContent.IsContentActive() || !btn.IsAvailable())
      return;
    this.m_adventureContent.SetAdventureId(adventureId, false);
    using (List<GeneralStoreAdventureSelectorButton>.Enumerator enumerator = this.m_adventureButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Unselect();
    }
    btn.Select();
    Options.Get().SetInt(Option.LAST_SELECTED_STORE_ADVENTURE_ID, (int) btn.GetAdventureId());
    if (string.IsNullOrEmpty(this.m_adventureSelectionSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_adventureSelectionSound));
  }

  private void UpdateAdventureButtonPositions()
  {
    GeneralStoreAdventureSelectorButton[] array = this.m_adventureButtons.ToArray();
    int index = 0;
    int num = 0;
    for (; index < array.Length; ++index)
      array[index].transform.localPosition = this.m_adventureButtonSpacing * (float) num++;
  }

  private void SetupInitialSelectedAdventure()
  {
    AdventureDbId adventureId = Options.Get().GetEnum<AdventureDbId>(Option.LAST_SELECTED_STORE_ADVENTURE_ID, AdventureDbId.INVALID);
    Network.Bundle bundle = (Network.Bundle) null;
    bool productExists = false;
    StoreManager.Get().GetAvailableAdventureBundle(adventureId, GeneralStoreAdventureContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, out bundle, out productExists);
    if (!productExists)
      adventureId = AdventureDbId.INVALID;
    using (List<GeneralStoreAdventureSelectorButton>.Enumerator enumerator = this.m_adventureButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GeneralStoreAdventureSelectorButton current = enumerator.Current;
        if (current.GetAdventureId() == adventureId)
        {
          this.m_adventureContent.SetAdventureId(adventureId, false);
          current.Select();
        }
        else
          current.Unselect();
      }
    }
  }
}
