﻿// Decompiled with JetBrains decompiler
// Type: DeckBigCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class DeckBigCard : MonoBehaviour
{
  private HandActorCache m_actorCache = new HandActorCache();
  public GameObject m_topPosition;
  public GameObject m_bottomPosition;
  public Material m_missingCardMaterial;
  public Material m_ghostCardMaterial;
  public Material m_invalidCardMaterial;
  private bool m_actorCacheInit;
  private bool m_shown;
  private EntityDef m_entityDef;
  private TAG_PREMIUM m_premium;
  private CardDef m_cardDef;
  private Actor m_shownActor;
  private GhostCard.Type m_ghosted;
  private int m_firstShowFrame;

  private void Awake()
  {
    this.m_firstShowFrame = 0;
  }

  public void Show(EntityDef entityDef, TAG_PREMIUM premium, CardDef cardDef, Vector3 sourcePosition, GhostCard.Type ghosted, float delay = 0)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckBigCard.\u003CShow\u003Ec__AnonStorey39C showCAnonStorey39C = new DeckBigCard.\u003CShow\u003Ec__AnonStorey39C();
    // ISSUE: reference to a compiler-generated field
    showCAnonStorey39C.entityDef = entityDef;
    // ISSUE: reference to a compiler-generated field
    showCAnonStorey39C.premium = premium;
    // ISSUE: reference to a compiler-generated field
    showCAnonStorey39C.cardDef = cardDef;
    // ISSUE: reference to a compiler-generated field
    showCAnonStorey39C.sourcePosition = sourcePosition;
    // ISSUE: reference to a compiler-generated field
    showCAnonStorey39C.ghosted = ghosted;
    // ISSUE: reference to a compiler-generated field
    showCAnonStorey39C.\u003C\u003Ef__this = this;
    if (false)
    {
      int frameCount = Time.frameCount;
      if (this.m_firstShowFrame == 0)
        this.m_firstShowFrame = frameCount;
      else if (frameCount - this.m_firstShowFrame <= 1)
        return;
    }
    this.StopCoroutine("ShowWithDelayInternal");
    this.m_shown = true;
    // ISSUE: reference to a compiler-generated field
    this.m_entityDef = showCAnonStorey39C.entityDef;
    // ISSUE: reference to a compiler-generated field
    this.m_premium = showCAnonStorey39C.premium;
    // ISSUE: reference to a compiler-generated field
    this.m_cardDef = showCAnonStorey39C.cardDef;
    // ISSUE: reference to a compiler-generated field
    this.m_ghosted = showCAnonStorey39C.ghosted;
    if ((double) delay > 0.0)
    {
      // ISSUE: reference to a compiler-generated method
      this.StartCoroutine("ShowWithDelayInternal", (object) new KeyValuePair<float, Action>(delay, new Action(showCAnonStorey39C.\u003C\u003Em__EC)));
    }
    else
    {
      if (!(bool) UniversalInputManager.UsePhoneUI)
      {
        float z1 = this.m_bottomPosition.transform.position.z;
        float z2 = this.m_topPosition.transform.position.z;
        // ISSUE: reference to a compiler-generated field
        TransformUtil.SetPosZ((Component) this.transform, Mathf.Clamp(showCAnonStorey39C.sourcePosition.z, z1, z2));
      }
      if (!this.m_actorCacheInit)
      {
        this.m_actorCacheInit = true;
        this.m_actorCache.AddActorLoadedListener(new HandActorCache.ActorLoadedCallback(this.OnActorLoaded));
        this.m_actorCache.Initialize();
      }
      if (this.m_actorCache.IsInitializing())
        return;
      // ISSUE: reference to a compiler-generated field
      this.Show(showCAnonStorey39C.sourcePosition.z);
    }
  }

  public void Hide(EntityDef entityDef, TAG_PREMIUM premium)
  {
    if (this.m_entityDef != entityDef || this.m_premium != premium)
      return;
    this.Hide();
  }

  public void ForceHide()
  {
    this.Hide();
  }

  [DebuggerHidden]
  private IEnumerator ShowWithDelayInternal(KeyValuePair<float, Action> args)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckBigCard.\u003CShowWithDelayInternal\u003Ec__Iterator54()
    {
      args = args,
      \u003C\u0024\u003Eargs = args
    };
  }

  private void OnActorLoaded(string name, Actor actor, object callbackData)
  {
    if ((UnityEngine.Object) actor == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DeckBigCard.OnActorLoaded() - FAILED to load {0}", (object) name));
    }
    else
    {
      actor.TurnOffCollider();
      actor.Hide();
      actor.transform.parent = this.transform;
      TransformUtil.Identity((Component) actor.transform);
      SceneUtils.SetLayer((Component) actor, this.gameObject.layer);
      if (this.m_actorCache.IsInitializing() || !this.m_shown)
        return;
      this.Show(0.0f);
    }
  }

  private void Show(float sourceZ = 0)
  {
    this.m_shownActor = this.m_actorCache.GetActor(this.m_entityDef, this.m_premium);
    if ((UnityEngine.Object) this.m_shownActor == (UnityEngine.Object) null)
      return;
    this.m_shownActor.SetEntityDef(this.m_entityDef);
    this.m_shownActor.SetPremium(this.m_premium);
    this.m_cardDef = DefLoader.Get().GetCardDef(this.m_entityDef.GetCardId(), new CardPortraitQuality(3, this.m_premium));
    this.m_shownActor.SetCardDef(this.m_cardDef);
    this.m_shownActor.GhostCardEffect(this.m_ghosted);
    if (this.m_shownActor.isGhostCard())
    {
      GhostCard component = this.m_shownActor.m_ghostCardGameObject.GetComponent<GhostCard>();
      component.SetRenderQueue(70);
      component.SetBigCard(true);
    }
    this.m_shownActor.UpdateAllComponents();
    if ((UnityEngine.Object) this.m_missingCardMaterial != (UnityEngine.Object) null)
      this.m_shownActor.SetMissingCardMaterial(this.m_missingCardMaterial);
    this.m_shownActor.Show();
    CollectionManagerDisplay collectionManagerDisplay = CollectionManagerDisplay.Get();
    bool flag = (UnityEngine.Object) collectionManagerDisplay != (UnityEngine.Object) null && collectionManagerDisplay.GetViewMode() == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE;
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    if (taggedDeck != null && taggedDeck.Locked || this.m_ghosted == GhostCard.Type.NONE)
      return;
    TooltipPanelManager.Orientation orientation = TooltipPanelManager.Orientation.LeftMiddle;
    if ((bool) UniversalInputManager.UsePhoneUI && flag)
      orientation = (double) sourceZ <= 0.0 ? TooltipPanelManager.Orientation.RightTop : TooltipPanelManager.Orientation.RightBottom;
    TooltipPanelManager.Get().UpdateGhostCardHelpForCollectionManager(this.m_shownActor, this.m_ghosted, orientation);
  }

  private void Hide()
  {
    this.StopCoroutine("ShowWithDelayInternal");
    this.m_shown = false;
    if ((UnityEngine.Object) this.m_shownActor == (UnityEngine.Object) null)
      return;
    this.m_shownActor.Hide();
    this.m_shownActor = (Actor) null;
    TooltipPanelManager.Get().HideTooltipPanels();
  }
}
