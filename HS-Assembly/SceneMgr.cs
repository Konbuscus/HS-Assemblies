﻿// Decompiled with JetBrains decompiler
// Type: SceneMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMgr : MonoBehaviour
{
  private SceneMgr.Mode m_mode = SceneMgr.Mode.STARTUP;
  private List<SceneMgr.ScenePreUnloadListener> m_scenePreUnloadListeners = new List<SceneMgr.ScenePreUnloadListener>();
  private List<SceneMgr.SceneUnloadedListener> m_sceneUnloadedListeners = new List<SceneMgr.SceneUnloadedListener>();
  private List<SceneMgr.ScenePreLoadListener> m_scenePreLoadListeners = new List<SceneMgr.ScenePreLoadListener>();
  private List<SceneMgr.SceneLoadedListener> m_sceneLoadedListeners = new List<SceneMgr.SceneLoadedListener>();
  private const float SCENE_UNLOAD_DELAY = 0.15f;
  private const float SCENE_LOADED_DELAY = 0.15f;
  public GameObject m_StartupCamera;
  private static SceneMgr s_instance;
  private int m_startupAssetLoads;
  private SceneMgr.Mode m_nextMode;
  private SceneMgr.Mode m_prevMode;
  private bool m_reloadMode;
  private Scene m_scene;
  private bool m_sceneLoaded;
  private bool m_transitioning;
  private bool m_performFullCleanup;
  private long m_boxLoadTimestamp;
  private bool m_textInputGUISkinLoaded;

  private void Awake()
  {
    SceneMgr.s_instance = this;
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    this.m_transitioning = true;
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
  }

  private void OnDestroy()
  {
    ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
    SceneMgr.s_instance = (SceneMgr) null;
  }

  private void Start()
  {
    if (this.IsModeRequested(SceneMgr.Mode.FATAL_ERROR))
      return;
    this.StartCoroutine("LoadAssetsWhenReady");
  }

  [DebuggerHidden]
  private IEnumerator LoadAssetsWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SceneMgr.\u003CLoadAssetsWhenReady\u003Ec__Iterator241() { \u003C\u003Ef__this = this };
  }

  public void LoadShaderPreCompiler()
  {
  }

  private void Update()
  {
    if (!this.m_reloadMode)
    {
      if (this.m_nextMode == SceneMgr.Mode.INVALID)
        return;
      if (this.m_mode == this.m_nextMode)
      {
        this.m_nextMode = SceneMgr.Mode.INVALID;
        return;
      }
    }
    this.m_transitioning = true;
    this.m_performFullCleanup = !this.m_reloadMode;
    this.m_prevMode = this.m_mode;
    this.m_mode = this.m_nextMode;
    this.m_nextMode = SceneMgr.Mode.INVALID;
    this.m_reloadMode = false;
    if ((UnityEngine.Object) this.m_scene != (UnityEngine.Object) null)
    {
      this.StopCoroutine("SwitchMode");
      this.StartCoroutine("SwitchMode");
    }
    else
      this.LoadMode();
  }

  public static SceneMgr Get()
  {
    return SceneMgr.s_instance;
  }

  private void WillReset()
  {
    Log.Reset.Print("SceneMgr.WillReset()");
    if (ApplicationMgr.IsPublic())
      Time.timeScale = 1f;
    this.StopAllCoroutines();
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    this.m_mode = SceneMgr.Mode.STARTUP;
    this.m_nextMode = SceneMgr.Mode.INVALID;
    this.m_prevMode = SceneMgr.Mode.INVALID;
    this.m_reloadMode = false;
    Scene scene = this.m_scene;
    if ((UnityEngine.Object) scene != (UnityEngine.Object) null)
      scene.PreUnload();
    this.FireScenePreUnloadEvent(scene);
    if ((UnityEngine.Object) this.m_scene != (UnityEngine.Object) null)
    {
      this.m_scene.Unload();
      this.m_scene = (Scene) null;
      this.m_sceneLoaded = false;
    }
    this.FireSceneUnloadedEvent(scene);
    this.PostUnloadCleanup();
    this.StartCoroutine("WaitThenLoadAssets");
    Log.Reset.Print("\tSceneMgr.WillReset() completed");
  }

  [DebuggerHidden]
  private IEnumerator WaitThenLoadAssets()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SceneMgr.\u003CWaitThenLoadAssets\u003Ec__Iterator242() { \u003C\u003Ef__this = this };
  }

  public void SetNextMode(SceneMgr.Mode mode)
  {
    if (this.IsModeRequested(SceneMgr.Mode.FATAL_ERROR))
      return;
    this.CacheModeForResume(mode);
    this.m_nextMode = mode;
    this.m_reloadMode = false;
  }

  public void ReloadMode()
  {
    if (this.IsModeRequested(SceneMgr.Mode.FATAL_ERROR))
      return;
    this.m_nextMode = this.m_mode;
    this.m_reloadMode = true;
  }

  public SceneMgr.Mode GetPrevMode()
  {
    return this.m_prevMode;
  }

  public SceneMgr.Mode GetMode()
  {
    return this.m_mode;
  }

  public SceneMgr.Mode GetNextMode()
  {
    return this.m_nextMode;
  }

  public Scene GetScene()
  {
    return this.m_scene;
  }

  public void SetScene(Scene scene)
  {
    this.m_scene = scene;
  }

  public bool IsSceneLoaded()
  {
    return this.m_sceneLoaded;
  }

  public bool WillTransition()
  {
    return this.m_reloadMode || this.m_nextMode != SceneMgr.Mode.INVALID && this.m_nextMode != this.m_mode;
  }

  public bool IsTransitioning()
  {
    return this.m_transitioning;
  }

  public bool IsTransitionNowOrPending()
  {
    return this.IsTransitioning() || this.WillTransition();
  }

  public bool IsModeRequested(SceneMgr.Mode mode)
  {
    return this.m_mode == mode || this.m_nextMode == mode;
  }

  public bool IsInGame()
  {
    return this.IsModeRequested(SceneMgr.Mode.GAMEPLAY);
  }

  public void NotifySceneLoaded()
  {
    this.m_sceneLoaded = true;
    if (this.ShouldUseSceneLoadDelays())
      this.StartCoroutine(this.WaitThenFireSceneLoadedEvent());
    else
      this.FireSceneLoadedEvent();
  }

  public void RegisterScenePreUnloadEvent(SceneMgr.ScenePreUnloadCallback callback)
  {
    this.RegisterScenePreUnloadEvent(callback, (object) null);
  }

  public void RegisterScenePreUnloadEvent(SceneMgr.ScenePreUnloadCallback callback, object userData)
  {
    SceneMgr.ScenePreUnloadListener preUnloadListener = new SceneMgr.ScenePreUnloadListener();
    preUnloadListener.SetCallback(callback);
    preUnloadListener.SetUserData(userData);
    if (this.m_scenePreUnloadListeners.Contains(preUnloadListener))
      return;
    this.m_scenePreUnloadListeners.Add(preUnloadListener);
  }

  public bool UnregisterScenePreUnloadEvent(SceneMgr.ScenePreUnloadCallback callback)
  {
    return this.UnregisterScenePreUnloadEvent(callback, (object) null);
  }

  public bool UnregisterScenePreUnloadEvent(SceneMgr.ScenePreUnloadCallback callback, object userData)
  {
    SceneMgr.ScenePreUnloadListener preUnloadListener = new SceneMgr.ScenePreUnloadListener();
    preUnloadListener.SetCallback(callback);
    preUnloadListener.SetUserData(userData);
    return this.m_scenePreUnloadListeners.Remove(preUnloadListener);
  }

  public void RegisterSceneUnloadedEvent(SceneMgr.SceneUnloadedCallback callback)
  {
    this.RegisterSceneUnloadedEvent(callback, (object) null);
  }

  public void RegisterSceneUnloadedEvent(SceneMgr.SceneUnloadedCallback callback, object userData)
  {
    SceneMgr.SceneUnloadedListener unloadedListener = new SceneMgr.SceneUnloadedListener();
    unloadedListener.SetCallback(callback);
    unloadedListener.SetUserData(userData);
    if (this.m_sceneUnloadedListeners.Contains(unloadedListener))
      return;
    this.m_sceneUnloadedListeners.Add(unloadedListener);
  }

  public bool UnregisterSceneUnloadedEvent(SceneMgr.SceneUnloadedCallback callback)
  {
    return this.UnregisterSceneUnloadedEvent(callback, (object) null);
  }

  public bool UnregisterSceneUnloadedEvent(SceneMgr.SceneUnloadedCallback callback, object userData)
  {
    SceneMgr.SceneUnloadedListener unloadedListener = new SceneMgr.SceneUnloadedListener();
    unloadedListener.SetCallback(callback);
    unloadedListener.SetUserData(userData);
    return this.m_sceneUnloadedListeners.Remove(unloadedListener);
  }

  public void RegisterScenePreLoadEvent(SceneMgr.ScenePreLoadCallback callback)
  {
    this.RegisterScenePreLoadEvent(callback, (object) null);
  }

  public void RegisterScenePreLoadEvent(SceneMgr.ScenePreLoadCallback callback, object userData)
  {
    SceneMgr.ScenePreLoadListener scenePreLoadListener = new SceneMgr.ScenePreLoadListener();
    scenePreLoadListener.SetCallback(callback);
    scenePreLoadListener.SetUserData(userData);
    if (this.m_scenePreLoadListeners.Contains(scenePreLoadListener))
      return;
    this.m_scenePreLoadListeners.Add(scenePreLoadListener);
  }

  public bool UnregisterScenePreLoadEvent(SceneMgr.ScenePreLoadCallback callback)
  {
    return this.UnregisterScenePreLoadEvent(callback, (object) null);
  }

  public bool UnregisterScenePreLoadEvent(SceneMgr.ScenePreLoadCallback callback, object userData)
  {
    SceneMgr.ScenePreLoadListener scenePreLoadListener = new SceneMgr.ScenePreLoadListener();
    scenePreLoadListener.SetCallback(callback);
    scenePreLoadListener.SetUserData(userData);
    return this.m_scenePreLoadListeners.Remove(scenePreLoadListener);
  }

  public void RegisterSceneLoadedEvent(SceneMgr.SceneLoadedCallback callback)
  {
    this.RegisterSceneLoadedEvent(callback, (object) null);
  }

  public void RegisterSceneLoadedEvent(SceneMgr.SceneLoadedCallback callback, object userData)
  {
    SceneMgr.SceneLoadedListener sceneLoadedListener = new SceneMgr.SceneLoadedListener();
    sceneLoadedListener.SetCallback(callback);
    sceneLoadedListener.SetUserData(userData);
    if (this.m_sceneLoadedListeners.Contains(sceneLoadedListener))
      return;
    this.m_sceneLoadedListeners.Add(sceneLoadedListener);
  }

  public bool UnregisterSceneLoadedEvent(SceneMgr.SceneLoadedCallback callback)
  {
    return this.UnregisterSceneLoadedEvent(callback, (object) null);
  }

  public bool UnregisterSceneLoadedEvent(SceneMgr.SceneLoadedCallback callback, object userData)
  {
    SceneMgr.SceneLoadedListener sceneLoadedListener = new SceneMgr.SceneLoadedListener();
    sceneLoadedListener.SetCallback(callback);
    sceneLoadedListener.SetUserData(userData);
    return this.m_sceneLoadedListeners.Remove(sceneLoadedListener);
  }

  [DebuggerHidden]
  private IEnumerator WaitThenFireSceneLoadedEvent()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SceneMgr.\u003CWaitThenFireSceneLoadedEvent\u003Ec__Iterator243() { \u003C\u003Ef__this = this };
  }

  private void FireScenePreUnloadEvent(Scene prevScene)
  {
    foreach (SceneMgr.ScenePreUnloadListener preUnloadListener in this.m_scenePreUnloadListeners.ToArray())
      preUnloadListener.Fire(this.m_prevMode, prevScene);
  }

  private void FireSceneUnloadedEvent(Scene prevScene)
  {
    foreach (SceneMgr.SceneUnloadedListener unloadedListener in this.m_sceneUnloadedListeners.ToArray())
      unloadedListener.Fire(this.m_prevMode, prevScene);
  }

  private void FireScenePreLoadEvent()
  {
    foreach (SceneMgr.ScenePreLoadListener scenePreLoadListener in this.m_scenePreLoadListeners.ToArray())
      scenePreLoadListener.Fire(this.m_prevMode, this.m_mode);
  }

  private void FireSceneLoadedEvent()
  {
    this.m_transitioning = false;
    foreach (SceneMgr.SceneLoadedListener sceneLoadedListener in this.m_sceneLoadedListeners.ToArray())
      sceneLoadedListener.Fire(this.m_mode, this.m_scene);
  }

  private void OnFontTableInitialized(object userData)
  {
    if ((UnityEngine.Object) OverlayUI.Get() == (UnityEngine.Object) null)
      AssetLoader.Get().LoadUIScreen("OverlayUI", new AssetLoader.GameObjectCallback(this.OnOverlayUILoaded), (object) null, false);
    AssetLoader.Get().LoadGameObject("SplashScreen", new AssetLoader.GameObjectCallback(this.OnSplashScreenLoaded), (object) null, false);
  }

  private void OnBaseUILoaded(string name, GameObject go, object callbackData)
  {
    if (!((UnityEngine.Object) go == (UnityEngine.Object) null))
      return;
    UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnBaseUILoaded() - FAILED to load \"{0}\"", (object) name));
  }

  private void OnOverlayUILoaded(string name, GameObject go, object callbackData)
  {
    if (!((UnityEngine.Object) go == (UnityEngine.Object) null))
      return;
    UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnOverlayUILoaded() - FAILED to load \"{0}\"", (object) name));
  }

  private void OnSplashScreenLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnSplashScreenLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      go.GetComponent<SplashScreen>().AddFinishedListener(new SplashScreen.FinishedHandler(this.OnSplashScreenFinished));
      if (!((UnityEngine.Object) BaseUI.Get() == (UnityEngine.Object) null))
        return;
      AssetLoader.Get().LoadUIScreen("BaseUI", new AssetLoader.GameObjectCallback(this.OnBaseUILoaded), (object) null, false);
    }
  }

  private void OnSplashScreenFinished()
  {
    this.LoadStartupAssets();
    this.m_StartupCamera.SetActive(false);
  }

  private void LoadStartupAssets()
  {
    this.m_startupAssetLoads = 6;
    if ((UnityEngine.Object) SoundManager.Get().GetConfig() == (UnityEngine.Object) null)
      AssetLoader.Get().LoadGameObject("SoundConfig", new AssetLoader.GameObjectCallback(this.OnSoundConfigLoaded), (object) null, false);
    else
      --this.m_startupAssetLoads;
    if ((UnityEngine.Object) MusicConfig.Get() == (UnityEngine.Object) null)
      AssetLoader.Get().LoadGameObject("MusicConfig", new AssetLoader.GameObjectCallback(this.OnStartupAssetLoaded<MusicConfig>), (object) null, false);
    else
      --this.m_startupAssetLoads;
    if ((UnityEngine.Object) AdventureConfig.Get() == (UnityEngine.Object) null)
      AssetLoader.Get().LoadGameObject("AdventureConfig", new AssetLoader.GameObjectCallback(this.OnStartupAssetLoaded<AdventureConfig>), (object) null, false);
    else
      --this.m_startupAssetLoads;
    if ((UnityEngine.Object) CardColorSwitcher.Get() == (UnityEngine.Object) null)
      AssetLoader.Get().LoadGameObject("CardColorSwitcher", new AssetLoader.GameObjectCallback(this.OnColorSwitcherLoaded), (object) null, false);
    else
      --this.m_startupAssetLoads;
    if ((UnityEngine.Object) SpecialEventVisualMgr.Get() == (UnityEngine.Object) null)
      AssetLoader.Get().LoadGameObject("SpecialEventVisualMgr", new AssetLoader.GameObjectCallback(this.OnSpecialEventVisualMgrLoaded), (object) null, false);
    else
      --this.m_startupAssetLoads;
    if (!this.m_textInputGUISkinLoaded)
      AssetLoader.Get().LoadGameObject("TextInputGUISkin", new AssetLoader.GameObjectCallback(this.OnTextInputGUISkinLoaded), (object) null, false);
    else
      --this.m_startupAssetLoads;
    if (this.m_startupAssetLoads != 0)
      return;
    this.OnStartupAssetFinishedLoading();
  }

  private void OnColorSwitcherLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnColorSwitcherLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      go.transform.parent = this.transform;
      this.OnStartupAssetFinishedLoading();
    }
  }

  private void OnSpecialEventVisualMgrLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnSpecialEventMgrLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      go.transform.parent = this.transform;
      this.OnStartupAssetFinishedLoading();
    }
  }

  private void OnSoundConfigLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnSoundConfigLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      SoundConfig component = go.GetComponent<SoundConfig>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnSoundConfigLoaded() - ERROR \"{0}\" has no {1} component", (object) name, (object) typeof (SoundConfig)));
      }
      else
      {
        go.transform.parent = this.transform;
        SoundManager.Get().SetConfig(component);
        this.OnStartupAssetFinishedLoading();
      }
    }
  }

  private void OnStartupAssetLoaded<T>(string name, GameObject go, object callbackData) where T : UnityEngine.Component
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnStartupAssetLoaded<{0}>() - FAILED to load \"{1}\"", (object) typeof (T).Name, (object) name));
    else if ((UnityEngine.Object) go.GetComponent<T>() == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnStartupAssetLoaded<{0}>() - ERROR \"{1}\" has no {2} component", (object) typeof (T).Name, (object) name, (object) typeof (T)));
    }
    else
    {
      go.transform.parent = this.transform;
      this.OnStartupAssetFinishedLoading();
    }
  }

  private void OnTextInputGUISkinLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnTextGUISkinLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      this.m_textInputGUISkinLoaded = true;
      UniversalInputManager.Get().SetGUISkin(go.GetComponent<GUISkinContainer>());
      this.OnStartupAssetFinishedLoading();
    }
  }

  private void OnStartupAssetFinishedLoading()
  {
    if (this.IsModeRequested(SceneMgr.Mode.FATAL_ERROR))
      return;
    --this.m_startupAssetLoads;
    if (this.m_startupAssetLoads > 0)
      return;
    this.LoadBox(new AssetLoader.GameObjectCallback(this.OnBoxLoaded));
  }

  private void OnBoxLoaded(string name, GameObject screen, object callbackData)
  {
    if ((UnityEngine.Object) screen == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnBoxLoaded() - failed to load {0}", (object) name));
    }
    else
    {
      if (this.IsModeRequested(SceneMgr.Mode.FATAL_ERROR))
        return;
      this.m_nextMode = SceneMgr.Mode.LOGIN;
      if (!((UnityEngine.Object) PerformanceAnalytics.Get() != (UnityEngine.Object) null))
        return;
      PerformanceAnalytics.Get().EndStartupTimmer();
    }
  }

  private void LoadMode()
  {
    this.FireScenePreLoadEvent();
    SceneManager.LoadSceneAsync(EnumUtils.GetString<SceneMgr.Mode>(this.m_mode), LoadSceneMode.Additive);
  }

  [DebuggerHidden]
  private IEnumerator SwitchMode()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SceneMgr.\u003CSwitchMode\u003Ec__Iterator244() { \u003C\u003Ef__this = this };
  }

  private bool ShouldUseSceneUnloadDelays()
  {
    return this.m_prevMode != this.m_mode;
  }

  private bool ShouldUseSceneLoadDelays()
  {
    return this.m_mode != SceneMgr.Mode.LOGIN && this.m_mode != SceneMgr.Mode.HUB && this.m_mode != SceneMgr.Mode.FATAL_ERROR;
  }

  private void PostUnloadCleanup()
  {
    Time.captureFramerate = 0;
    if (Application.isEditor && this.m_mode == SceneMgr.Mode.FATAL_ERROR)
    {
      foreach (Behaviour behaviour in UnityEngine.Object.FindObjectsOfType<AudioListener>())
        behaviour.enabled = false;
    }
    else
    {
      this.DestroyAllObjectsOnModeSwitch();
      if (!this.m_performFullCleanup)
        return;
      ApplicationMgr.Get().UnloadUnusedAssets();
    }
  }

  private void DestroyAllObjectsOnModeSwitch()
  {
    foreach (GameObject go in (GameObject[]) UnityEngine.Object.FindObjectsOfType(typeof (GameObject)))
    {
      if (this.ShouldDestroyOnModeSwitch(go))
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
    }
  }

  private bool ShouldDestroyOnModeSwitch(GameObject go)
  {
    return !((UnityEngine.Object) go == (UnityEngine.Object) null) && !((UnityEngine.Object) go.transform.parent != (UnityEngine.Object) null) && !((UnityEngine.Object) go == (UnityEngine.Object) this.gameObject) && ((!((UnityEngine.Object) PegUI.Get() != (UnityEngine.Object) null) || !((UnityEngine.Object) go == (UnityEngine.Object) PegUI.Get().gameObject)) && (!((UnityEngine.Object) OverlayUI.Get() != (UnityEngine.Object) null) || !((UnityEngine.Object) go == (UnityEngine.Object) OverlayUI.Get().gameObject))) && ((!((UnityEngine.Object) Box.Get() != (UnityEngine.Object) null) || !((UnityEngine.Object) go == (UnityEngine.Object) Box.Get().gameObject) || !this.DoesModeShowBox(this.m_mode)) && (!DefLoader.Get().HasDef(go) && !AssetLoader.Get().IsWaitingOnObject(go) && !((UnityEngine.Object) go == (UnityEngine.Object) iTweenManager.Get().gameObject)));
  }

  private void CacheModeForResume(SceneMgr.Mode mode)
  {
    if (PlatformSettings.OS != OSCategory.iOS && PlatformSettings.OS != OSCategory.Android)
      return;
    switch (mode)
    {
      case SceneMgr.Mode.HUB:
      case SceneMgr.Mode.FRIENDLY:
        Options.Get().SetInt(Option.LAST_SCENE_MODE, 0);
        break;
      case SceneMgr.Mode.COLLECTIONMANAGER:
      case SceneMgr.Mode.TOURNAMENT:
      case SceneMgr.Mode.DRAFT:
      case SceneMgr.Mode.CREDITS:
      case SceneMgr.Mode.ADVENTURE:
      case SceneMgr.Mode.TAVERN_BRAWL:
        Options.Get().SetInt(Option.LAST_SCENE_MODE, (int) mode);
        break;
    }
  }

  private bool DoesModeShowBox(SceneMgr.Mode mode)
  {
    SceneMgr.Mode mode1 = mode;
    switch (mode1)
    {
      case SceneMgr.Mode.STARTUP:
      case SceneMgr.Mode.GAMEPLAY:
label_2:
        return false;
      default:
        switch (mode1 - 9)
        {
          case SceneMgr.Mode.INVALID:
          case SceneMgr.Mode.HUB:
            goto label_2;
          default:
            return true;
        }
    }
  }

  private void LoadModeFromModeSwitch()
  {
    bool flag1 = this.DoesModeShowBox(this.m_prevMode);
    bool flag2 = this.DoesModeShowBox(this.m_mode);
    if (!flag1 && flag2)
    {
      this.LoadBox(new AssetLoader.GameObjectCallback(this.OnBoxReloaded));
    }
    else
    {
      if (flag1 && !flag2)
      {
        LoadingScreen.Get().SetAssetLoadStartTimestamp(this.m_boxLoadTimestamp);
        this.m_boxLoadTimestamp = 0L;
      }
      this.LoadMode();
    }
  }

  private void OnBoxReloaded(string name, GameObject screen, object callbackData)
  {
    if ((UnityEngine.Object) screen == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("SceneMgr.OnBoxReloaded() - failed to load {0}", (object) name));
    else
      this.LoadMode();
  }

  private void LoadBox(AssetLoader.GameObjectCallback callback)
  {
    this.m_boxLoadTimestamp = TimeUtils.BinaryStamp();
    AssetLoader.Get().LoadUIScreen("TheBox", callback, (object) null, false);
  }

  private void OnFatalError(FatalErrorMessage message, object userData)
  {
    FatalErrorMgr.Get().RemoveErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    if (ApplicationMgr.Get().ResetOnErrorIfNecessary())
      return;
    this.SetNextMode(SceneMgr.Mode.FATAL_ERROR);
  }

  public enum Mode
  {
    INVALID,
    STARTUP,
    [Description("Login")] LOGIN,
    [Description("Hub")] HUB,
    [Description("Gameplay")] GAMEPLAY,
    [Description("CollectionManager")] COLLECTIONMANAGER,
    [Description("PackOpening")] PACKOPENING,
    [Description("Tournament")] TOURNAMENT,
    [Description("Friendly")] FRIENDLY,
    [Description("FatalError")] FATAL_ERROR,
    [Description("Draft")] DRAFT,
    [Description("Credits")] CREDITS,
    [Description("Reset")] RESET,
    [Description("Adventure")] ADVENTURE,
    [Description("TavernBrawl")] TAVERN_BRAWL,
  }

  private class ScenePreUnloadListener : EventListener<SceneMgr.ScenePreUnloadCallback>
  {
    public void Fire(SceneMgr.Mode prevMode, Scene prevScene)
    {
      this.m_callback(prevMode, prevScene, this.m_userData);
    }
  }

  private class SceneUnloadedListener : EventListener<SceneMgr.SceneUnloadedCallback>
  {
    public void Fire(SceneMgr.Mode prevMode, Scene prevScene)
    {
      this.m_callback(prevMode, prevScene, this.m_userData);
    }
  }

  private class ScenePreLoadListener : EventListener<SceneMgr.ScenePreLoadCallback>
  {
    public void Fire(SceneMgr.Mode prevMode, SceneMgr.Mode mode)
    {
      this.m_callback(prevMode, mode, this.m_userData);
    }
  }

  private class SceneLoadedListener : EventListener<SceneMgr.SceneLoadedCallback>
  {
    public void Fire(SceneMgr.Mode mode, Scene scene)
    {
      this.m_callback(mode, scene, this.m_userData);
    }
  }

  public delegate void ScenePreUnloadCallback(SceneMgr.Mode prevMode, Scene prevScene, object userData);

  public delegate void SceneUnloadedCallback(SceneMgr.Mode prevMode, Scene prevScene, object userData);

  public delegate void ScenePreLoadCallback(SceneMgr.Mode prevMode, SceneMgr.Mode mode, object userData);

  public delegate void SceneLoadedCallback(SceneMgr.Mode mode, Scene scene, object userData);
}
