﻿// Decompiled with JetBrains decompiler
// Type: QuickChatFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;
using UnityEngine;

public class QuickChatFrame : MonoBehaviour
{
  private List<BnetPlayer> m_recentPlayers = new List<BnetPlayer>();
  public QuickChatFrameBones m_Bones;
  public QuickChatFramePrefabs m_Prefabs;
  public GameObject m_Background;
  public UberText m_ReceiverNameText;
  public UberText m_LastMessageText;
  public GameObject m_LastMessageShadow;
  public PegUIElement m_ChatLogButton;
  public Font m_InputFont;
  private DropdownControl m_recentPlayerDropdown;
  private ChatLogFrame m_chatLogFrame;
  private PegUIElement m_inputBlocker;
  private BnetPlayer m_receiver;
  private float m_initialLastMessageTextHeight;
  private float m_initialLastMessageShadowScaleZ;
  private Font m_localizedInputFont;

  private void Awake()
  {
    this.InitRecentPlayers();
    if (!this.InitReceiver())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }
    else
    {
      BnetWhisperMgr.Get().AddWhisperListener(new BnetWhisperMgr.WhisperCallback(this.OnWhisper));
      BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
      this.InitTransform();
      this.InitInputBlocker();
      this.InitLastMessage();
      this.InitChatLogFrame();
      this.InitInput();
      this.ShowInput(true);
    }
  }

  private void Start()
  {
    this.InitRecentPlayerDropdown();
    if (ChatMgr.Get().IsChatLogFrameShown())
      this.ShowChatLogFrame();
    this.UpdateReceiver();
    ChatMgr.Get().OnChatReceiverChanged(this.m_receiver);
  }

  private void OnDestroy()
  {
    BnetWhisperMgr.Get().RemoveWhisperListener(new BnetWhisperMgr.WhisperCallback(this.OnWhisper));
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    if (!((UnityEngine.Object) UniversalInputManager.Get() != (UnityEngine.Object) null))
      return;
    UniversalInputManager.Get().CancelTextInput(this.gameObject, false);
  }

  public ChatLogFrame GetChatLogFrame()
  {
    return this.m_chatLogFrame;
  }

  public BnetPlayer GetReceiver()
  {
    return this.m_receiver;
  }

  public void SetReceiver(BnetPlayer player)
  {
    UniversalInputManager.Get().FocusTextInput(this.gameObject);
    if (this.m_receiver == player)
      return;
    this.m_receiver = player;
    this.UpdateReceiver();
    this.m_recentPlayerDropdown.setSelection((object) player);
    ChatMgr.Get().OnChatReceiverChanged(player);
  }

  public void UpdateLayout()
  {
    if (!((UnityEngine.Object) this.m_chatLogFrame != (UnityEngine.Object) null))
      return;
    this.m_chatLogFrame.UpdateLayout();
  }

  private void InitRecentPlayers()
  {
    this.UpdateRecentPlayers();
  }

  private void UpdateRecentPlayers()
  {
    this.m_recentPlayers.Clear();
    List<BnetPlayer> recentWhisperPlayers = ChatMgr.Get().GetRecentWhisperPlayers();
    for (int index = 0; index < recentWhisperPlayers.Count; ++index)
      this.m_recentPlayers.Add(recentWhisperPlayers[index]);
  }

  private bool InitReceiver()
  {
    this.m_receiver = (BnetPlayer) null;
    if (this.m_recentPlayers.Count == 0)
    {
      UIStatus.Get().AddError(BnetFriendMgr.Get().GetOnlineFriendCount() != 0 ? GameStrings.Get("GLOBAL_CHAT_NO_RECENT_CONVERSATIONS") : GameStrings.Get("GLOBAL_CHAT_NO_FRIENDS_ONLINE"), -1f);
      return false;
    }
    this.m_receiver = this.m_recentPlayers[0];
    return true;
  }

  private void OnWhisper(BnetWhisper whisper, object userData)
  {
    if (this.m_receiver == null || !WhisperUtil.IsSpeaker(this.m_receiver, whisper))
      return;
    this.UpdateReceiver();
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    BnetPlayerChange change = changelist.FindChange(this.m_receiver);
    if (change == null)
      return;
    BnetPlayer oldPlayer = change.GetOldPlayer();
    BnetPlayer newPlayer = change.GetNewPlayer();
    if (oldPlayer != null && oldPlayer.IsOnline() == newPlayer.IsOnline())
      return;
    this.UpdateReceiver();
  }

  private BnetWhisper FindLastWhisperFromReceiver()
  {
    List<BnetWhisper> whispersWithPlayer = BnetWhisperMgr.Get().GetWhispersWithPlayer(this.m_receiver);
    if (whispersWithPlayer == null)
      return (BnetWhisper) null;
    for (int index = whispersWithPlayer.Count - 1; index >= 0; --index)
    {
      BnetWhisper whisper = whispersWithPlayer[index];
      if (WhisperUtil.IsSpeaker(this.m_receiver, whisper))
        return whisper;
    }
    return (BnetWhisper) null;
  }

  private void InitTransform()
  {
    this.transform.parent = BaseUI.Get().transform;
    this.DefaultChatTransform();
    if ((!UniversalInputManager.Get().UseWindowsTouch() || !W8Touch.s_isWindows8OrGreater) && !W8Touch.Get().IsVirtualKeyboardVisible())
      return;
    this.TransformChatForKeyboard();
  }

  private void InitLastMessage()
  {
    this.m_initialLastMessageTextHeight = this.m_LastMessageText.GetTextWorldSpaceBounds().size.y;
    this.m_initialLastMessageShadowScaleZ = this.m_LastMessageShadow.transform.localScale.z;
  }

  private void InitInputBlocker()
  {
    this.m_inputBlocker = CameraUtils.CreateInputBlocker(CameraUtils.FindFirstByLayer(this.gameObject.layer), "QuickChatInputBlocker", (Component) this, this.m_Bones.m_InputBlocker.position.z - this.transform.position.z).AddComponent<PegUIElement>();
    this.m_inputBlocker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnInputBlockerReleased));
  }

  private void OnInputBlockerReleased(UIEvent e)
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  private void InitChatLogFrame()
  {
    this.m_ChatLogButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnChatLogButtonReleased));
  }

  private void OnChatLogButtonReleased(UIEvent e)
  {
    if (ChatMgr.Get().IsChatLogFrameShown())
      this.HideChatLogFrame();
    else
      this.ShowChatLogFrame();
    this.UpdateReceiver();
    UniversalInputManager.Get().FocusTextInput(this.gameObject);
  }

  private void ShowChatLogFrame()
  {
    this.m_chatLogFrame = UnityEngine.Object.Instantiate<ChatLogFrame>(this.m_Prefabs.m_ChatLogFrame);
    bool flag = this.transform.localScale == BaseUI.Get().m_Bones.m_QuickChatVirtualKeyboard.localScale;
    if ((UniversalInputManager.Get().IsTouchMode() && W8Touch.s_isWindows8OrGreater || W8Touch.Get().IsVirtualKeyboardVisible()) && flag || flag)
      this.DefaultChatTransform();
    this.m_chatLogFrame.transform.parent = this.transform;
    this.m_chatLogFrame.transform.position = this.m_Bones.m_ChatLog.position;
    if ((UniversalInputManager.Get().UseWindowsTouch() && W8Touch.s_isWindows8OrGreater || W8Touch.Get().IsVirtualKeyboardVisible()) && flag || flag)
      this.TransformChatForKeyboard();
    ChatMgr.Get().OnChatLogFrameShown();
  }

  private void HideChatLogFrame()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_chatLogFrame.gameObject);
    this.m_chatLogFrame = (ChatLogFrame) null;
    ChatMgr.Get().OnChatLogFrameHidden();
  }

  private void InitRecentPlayerDropdown()
  {
    this.m_recentPlayerDropdown = UnityEngine.Object.Instantiate<DropdownControl>(this.m_Prefabs.m_Dropdown);
    this.m_recentPlayerDropdown.transform.parent = this.transform;
    this.m_recentPlayerDropdown.transform.position = this.m_Bones.m_RecentPlayerDropdown.position;
    this.m_recentPlayerDropdown.setItemTextCallback(new DropdownControl.itemTextCallback(this.OnRecentPlayerDropdownText));
    this.m_recentPlayerDropdown.setItemChosenCallback(new DropdownControl.itemChosenCallback(this.OnRecentPlayerDropdownItemChosen));
    this.UpdateRecentPlayerDropdown();
    this.m_recentPlayerDropdown.setSelection((object) this.m_receiver);
  }

  private void UpdateRecentPlayerDropdown()
  {
    this.m_recentPlayerDropdown.clearItems();
    for (int index = 0; index < this.m_recentPlayers.Count; ++index)
      this.m_recentPlayerDropdown.addItem((object) this.m_recentPlayers[index]);
  }

  private string OnRecentPlayerDropdownText(object val)
  {
    return FriendUtils.GetUniqueName((BnetPlayer) val);
  }

  private void OnRecentPlayerDropdownItemChosen(object selection, object prevSelection)
  {
    this.SetReceiver((BnetPlayer) selection);
  }

  private void UpdateReceiver()
  {
    this.UpdateLastMessage();
    if (!((UnityEngine.Object) this.m_chatLogFrame != (UnityEngine.Object) null))
      return;
    this.m_chatLogFrame.Receiver = this.m_receiver;
  }

  private void UpdateLastMessage()
  {
    if ((UnityEngine.Object) this.m_chatLogFrame != (UnityEngine.Object) null)
    {
      this.HideLastMessage();
    }
    else
    {
      BnetWhisper whisperFromReceiver = this.FindLastWhisperFromReceiver();
      if (whisperFromReceiver == null)
      {
        this.HideLastMessage();
      }
      else
      {
        this.m_LastMessageText.gameObject.SetActive(true);
        this.m_LastMessageText.Text = ChatUtils.GetMessage(whisperFromReceiver);
        TransformUtil.SetPoint((Component) this.m_LastMessageText, Anchor.BOTTOM_LEFT, (Component) this.m_Bones.m_LastMessage, Anchor.TOP_LEFT);
        this.m_ReceiverNameText.gameObject.SetActive(true);
        this.m_ReceiverNameText.TextColor = !this.m_receiver.IsOnline() ? GameColors.PLAYER_NAME_OFFLINE : GameColors.PLAYER_NAME_ONLINE;
        this.m_ReceiverNameText.Text = FriendUtils.GetUniqueName(this.m_receiver);
        TransformUtil.SetPoint((Component) this.m_ReceiverNameText, Anchor.BOTTOM_LEFT, (Component) this.m_LastMessageText, Anchor.TOP_LEFT);
        this.m_LastMessageShadow.SetActive(true);
        Bounds worldSpaceBounds1 = this.m_LastMessageText.GetTextWorldSpaceBounds();
        Bounds worldSpaceBounds2 = this.m_ReceiverNameText.GetTextWorldSpaceBounds();
        TransformUtil.SetLocalScaleZ(this.m_LastMessageShadow, (Mathf.Max(worldSpaceBounds1.max.y, worldSpaceBounds2.max.y) - Mathf.Min(worldSpaceBounds1.min.y, worldSpaceBounds2.min.y)) * this.m_initialLastMessageShadowScaleZ / this.m_initialLastMessageTextHeight);
      }
    }
  }

  private void HideLastMessage()
  {
    this.m_ReceiverNameText.gameObject.SetActive(false);
    this.m_LastMessageText.gameObject.SetActive(false);
    this.m_LastMessageShadow.SetActive(false);
  }

  private void CyclePrevReceiver()
  {
    int index = this.m_recentPlayers.FindIndex((Predicate<BnetPlayer>) (currReceiver => this.m_receiver == currReceiver));
    this.SetReceiver(index != 0 ? this.m_recentPlayers[index - 1] : this.m_recentPlayers[this.m_recentPlayers.Count - 1]);
  }

  private void CycleNextReceiver()
  {
    int index = this.m_recentPlayers.FindIndex((Predicate<BnetPlayer>) (currReceiver => this.m_receiver == currReceiver));
    this.SetReceiver(index != this.m_recentPlayers.Count - 1 ? this.m_recentPlayers[index + 1] : this.m_recentPlayers[0]);
  }

  private void InitInput()
  {
    FontDef fontDef = FontTable.Get().GetFontDef(this.m_InputFont);
    if ((UnityEngine.Object) fontDef == (UnityEngine.Object) null)
      this.m_localizedInputFont = this.m_InputFont;
    else
      this.m_localizedInputFont = fontDef.m_Font;
  }

  private void ShowInput(bool fromAwake)
  {
    Camera bnetCamera = BaseUI.Get().GetBnetCamera();
    Rect rect = CameraUtils.CreateGUIViewportRect(bnetCamera, (Component) this.m_Bones.m_InputTopLeft, (Component) this.m_Bones.m_InputBottomRight);
    if (Localization.GetLocale() == Locale.thTH)
    {
      Vector3 vector3_1 = bnetCamera.WorldToViewportPoint(this.m_Bones.m_InputTopLeft.position);
      Vector3 vector3_2 = bnetCamera.WorldToViewportPoint(this.m_Bones.m_InputBottomRight.position);
      float num = (float) (((double) vector3_1.y - (double) vector3_2.y) * 0.100000001490116);
      vector3_1 = new Vector3(vector3_1.x, vector3_1.y - num, vector3_1.z);
      vector3_2 = new Vector3(vector3_2.x, vector3_2.y + num, vector3_2.z);
      rect = new Rect(vector3_1.x, 1f - vector3_1.y, vector3_2.x - vector3_1.x, vector3_1.y - vector3_2.y);
    }
    UniversalInputManager.Get().UseTextInput(new UniversalInputManager.TextInputParams()
    {
      m_owner = this.gameObject,
      m_rect = rect,
      m_preprocessCallback = new UniversalInputManager.TextInputPreprocessCallback(this.OnInputPreprocess),
      m_completedCallback = new UniversalInputManager.TextInputCompletedCallback(this.OnInputComplete),
      m_canceledCallback = new UniversalInputManager.TextInputCanceledCallback(this.OnInputCanceled),
      m_font = this.m_localizedInputFont,
      m_maxCharacters = 512,
      m_touchScreenKeyboardHideInput = true,
      m_showVirtualKeyboard = fromAwake,
      m_hideVirtualKeyboardOnComplete = fromAwake
    }, false);
  }

  private bool OnInputPreprocess(Event e)
  {
    if (this.m_recentPlayers.Count < 2 || e.type != EventType.KeyDown)
      return false;
    KeyCode keyCode = e.keyCode;
    bool flag = (e.modifiers & EventModifiers.Shift) != EventModifiers.None;
    if (keyCode == KeyCode.UpArrow || keyCode == KeyCode.Tab && flag)
    {
      this.CyclePrevReceiver();
      return true;
    }
    if (keyCode != KeyCode.DownArrow && keyCode != KeyCode.Tab)
      return false;
    this.CycleNextReceiver();
    return true;
  }

  private void OnInputComplete(string input)
  {
    if (!string.IsNullOrEmpty(input))
    {
      if (this.m_receiver.IsOnline())
      {
        BnetWhisperMgr.Get().SendWhisper(this.m_receiver, input);
        ChatMgr.Get().AddRecentWhisperPlayerToTop(this.m_receiver);
      }
      else if (ChatMgr.Get().IsChatLogFrameShown())
      {
        if (!BnetWhisperMgr.Get().SendWhisper(this.m_receiver, input))
          this.m_chatLogFrame.m_chatLog.OnWhisperFailed();
        ChatMgr.Get().AddRecentWhisperPlayerToTop(this.m_receiver);
      }
      else
        UIStatus.Get().AddError(GameStrings.Format("GLOBAL_CHAT_RECEIVER_OFFLINE", (object) this.m_receiver.GetBestName()), -1f);
    }
    if (ChatMgr.Get().IsChatLogFrameShown())
      this.ShowInput(false);
    else
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  private void OnInputCanceled(bool userRequested, GameObject requester)
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  private void DefaultChatTransform()
  {
    this.transform.position = BaseUI.Get().m_Bones.m_QuickChat.position;
    this.transform.localScale = BaseUI.Get().m_Bones.m_QuickChat.localScale;
    if (!((UnityEngine.Object) this.m_chatLogFrame != (UnityEngine.Object) null))
      return;
    this.m_chatLogFrame.UpdateLayout();
  }

  private void TransformChatForKeyboard()
  {
    this.transform.position = BaseUI.Get().m_Bones.m_QuickChatVirtualKeyboard.position;
    this.transform.localScale = BaseUI.Get().m_Bones.m_QuickChatVirtualKeyboard.localScale;
    this.m_Prefabs.m_Dropdown.transform.localScale = new Vector3(50f, 50f, 50f);
    if (!((UnityEngine.Object) this.m_chatLogFrame != (UnityEngine.Object) null))
      return;
    this.m_chatLogFrame.UpdateLayout();
  }
}
