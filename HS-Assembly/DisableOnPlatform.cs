﻿// Decompiled with JetBrains decompiler
// Type: DisableOnPlatform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DisableOnPlatform : MonoBehaviour
{
  public ScreenCategory m_screenCategory;

  public void Awake()
  {
    if (!Application.isPlaying || PlatformSettings.Screen != this.m_screenCategory)
      return;
    this.gameObject.SetActive(false);
  }

  public void Update()
  {
    if (!Application.isPlaying || PlatformSettings.Screen != this.m_screenCategory)
      return;
    this.gameObject.SetActive(false);
  }
}
