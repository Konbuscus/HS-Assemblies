﻿// Decompiled with JetBrains decompiler
// Type: TB_ChooseYourFateRandom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TB_ChooseYourFateRandom : MissionEntity
{
  private string newFate = "TB_PICKYOURFATE_RANDOM_NEWFATE";
  private string opponentFate = "TB_PICKYOURFATE_RANDOM_OPPONENTFATE";
  private string firstFate = "TB_PICKYOURFATE_RANDOM_FIRSTFATE";
  private string firstOpponenentFate = "TB_PICKYOURFATE_BUILDAROUND_OPPONENT_FIRSTFATE";
  private HashSet<int> seen = new HashSet<int>();
  private Notification ChooseYourFatePopup;
  private Vector3 popUpPos;
  private string textID;

  public override void PreloadAssets()
  {
    this.PreloadSound("tutorial_mission_hero_coin_mouse_away");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB_ChooseYourFateRandom.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1F0() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
