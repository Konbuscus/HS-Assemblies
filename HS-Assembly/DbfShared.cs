﻿// Decompiled with JetBrains decompiler
// Type: DbfShared
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DbfShared
{
  public static float s_totalLoadTime;
  private static AssetBundle s_assetBundle;

  public static AssetBundle GetAssetBundle()
  {
    if ((Object) DbfShared.s_assetBundle == (Object) null)
    {
      string path = !Application.isEditor ? AssetLoader.CreateLocalFilePath(string.Format("Data/{0}dbf.unity3d", (object) AssetBundleInfo.BundlePathPlatformModifier())) : "Assets/Game/DBF-Asset/dbf.unity3d";
      DbfShared.s_assetBundle = AssetBundle.LoadFromFile(path);
      if ((Object) DbfShared.s_assetBundle == (Object) null)
        Debug.LogErrorFormat("Failed to load DBF asset bundle from: \"{0}\"", (object) path);
    }
    return DbfShared.s_assetBundle;
  }

  public static void Reset()
  {
    DbfShared.s_totalLoadTime = 0.0f;
  }
}
