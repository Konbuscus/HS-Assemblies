﻿// Decompiled with JetBrains decompiler
// Type: FriendListChallengeMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using UnityEngine;

public class FriendListChallengeMenu : MonoBehaviour
{
  public UIBButton m_StandardDuelButton;
  public UIBButton m_WildDuelButton;
  public UIBButton m_TavernBrawlButton;
  public TooltipZone m_StandardDuelTooltipZone;
  public TooltipZone m_WildDuelTooltipZone;
  public TooltipZone m_TavernBrawlTooltipZone;
  public GameObject m_StandardDuelButtonX;
  public GameObject m_WildDuelButtonX;
  public GameObject m_TavernBrawlButtonX;
  public GameObject m_TavernBrawlButtonDisabled;
  public MultiSliceElement m_FrameContainer;
  public MultiSliceElement m_ShadowContainer;
  public GameObject m_MiddleFrame;
  public GameObject m_MiddleShadow;
  private bool m_bHasStandardDeck;
  private bool m_bHasWildDeck;
  private bool m_bHasTavernBrawlDeck;
  private bool m_bCanChallengeTavernBrawl;
  private bool m_bIsTavernBrawlUnlocked;

  private void Awake()
  {
    this.m_StandardDuelButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnStandardDuelButtonReleased));
    this.m_WildDuelButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnWildDuelButtonReleased));
    this.m_TavernBrawlButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnTavernBrawlButtonReleased));
    if (!CollectionManager.Get().ShouldAccountSeeStandardWild())
    {
      this.m_StandardDuelButton.SetText("GLOBAL_FRIENDLIST_CHALLENGE_MENU_DUEL_BUTTON");
      this.m_TavernBrawlButton.gameObject.transform.position = this.m_WildDuelButton.gameObject.transform.position;
      this.m_WildDuelButton.gameObject.SetActive(false);
      this.m_MiddleFrame.transform.localScale = new Vector3(this.m_MiddleFrame.transform.localScale.x, 0.75f, this.m_MiddleFrame.transform.localScale.z);
      this.m_MiddleShadow.transform.localScale = new Vector3(this.m_MiddleShadow.transform.localScale.x, 0.2f, this.m_MiddleShadow.transform.localScale.z);
      this.m_FrameContainer.UpdateSlices();
      this.m_ShadowContainer.UpdateSlices();
    }
    this.m_bHasStandardDeck = CollectionManager.Get().AccountHasValidStandardDeck();
    this.m_bHasWildDeck = CollectionManager.Get().AccountHasAnyValidDeck();
    this.m_bCanChallengeTavernBrawl = TavernBrawlManager.Get().CanChallengeToTavernBrawl;
    this.m_bIsTavernBrawlUnlocked = TavernBrawlManager.Get().HasUnlockedTavernBrawl;
    if (this.m_bCanChallengeTavernBrawl)
    {
      this.m_bHasTavernBrawlDeck = (!TavernBrawlManager.Get().CurrentMission().canCreateDeck ? 0 : (!TavernBrawlManager.Get().HasValidDeck() ? 1 : 0)) == 0;
    }
    else
    {
      this.m_TavernBrawlButtonDisabled.SetActive(true);
      this.m_TavernBrawlButton.gameObject.GetComponent<UIBHighlight>().m_MouseOverHighlight = (GameObject) null;
      this.m_TavernBrawlButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnTavernBrawlButtonReleased));
      this.m_TavernBrawlButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTavernBrawlButtonOver));
      this.m_TavernBrawlButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnButtonOut));
    }
    if (!CollectionManager.Get().AreAllDeckContentsReady() && CollectionManager.Get().RequestDeckContentsForDecksWithoutContentsLoaded(new CollectionManager.DelOnAllDeckContents(this.OnDeckContents_UpdateButtons)))
    {
      this.m_bHasStandardDeck = true;
      this.m_bHasWildDeck = true;
    }
    this.UpdateButtons();
  }

  private void UpdateButtons()
  {
    this.m_StandardDuelButtonX.SetActive(!this.m_bHasStandardDeck);
    this.m_WildDuelButtonX.SetActive(!this.m_bHasWildDeck);
    this.m_TavernBrawlButtonX.SetActive(this.m_bCanChallengeTavernBrawl && (!this.m_bIsTavernBrawlUnlocked || !this.m_bHasTavernBrawlDeck));
  }

  private void OnDeckContents_UpdateButtons()
  {
    this.m_bHasStandardDeck = CollectionManager.Get().AccountHasValidStandardDeck();
    this.m_bHasWildDeck = CollectionManager.Get().AccountHasAnyValidDeck();
    this.UpdateButtons();
  }

  private void OnStandardDuelButtonOver(UIEvent e)
  {
    string headerKey = "GLOBAL_FRIENDLIST_CHALLENGE_BUTTON_HEADER";
    string descriptionFormat = "GLOBAL_FRIENDLIST_CHALLENGE_BUTTON_AVAILABLE";
    if (UniversalInputManager.Get().IsTouchMode())
    {
      if (GameStrings.HasKey(headerKey + "_TOUCH"))
        headerKey += "_TOUCH";
      if (GameStrings.HasKey(descriptionFormat + "_TOUCH"))
        descriptionFormat += "_TOUCH";
    }
    this.ShowTooltip(headerKey, descriptionFormat, this.m_StandardDuelTooltipZone, this.m_StandardDuelButton);
  }

  private void OnWildDuelButtonOver(UIEvent e)
  {
    string headerKey = "GLOBAL_FRIENDLIST_CHALLENGE_BUTTON_HEADER";
    string descriptionFormat = "GLOBAL_FRIENDLIST_CHALLENGE_BUTTON_AVAILABLE";
    if (UniversalInputManager.Get().IsTouchMode())
    {
      if (GameStrings.HasKey(headerKey + "_TOUCH"))
        headerKey += "_TOUCH";
      if (GameStrings.HasKey(descriptionFormat + "_TOUCH"))
        descriptionFormat += "_TOUCH";
    }
    this.ShowTooltip(headerKey, descriptionFormat, this.m_WildDuelTooltipZone, this.m_WildDuelButton);
  }

  private void OnTavernBrawlButtonOver(UIEvent e)
  {
    string headerKey = "GLOBAL_FRIENDLIST_CHALLENGE_BUTTON_HEADER";
    string descriptionFormat = "GLOBAL_FRIENDLIST_CHALLENGE_TOOLTIP_NO_TAVERN_BRAWL";
    if (TavernBrawlManager.Get().IsTavernBrawlActive && !TavernBrawlManager.Get().CanChallengeToTavernBrawl)
      descriptionFormat = "GLOBAL_FRIENDLIST_CHALLENGE_TOOLTIP_TAVERN_BRAWL_NOT_CHALLENGEABLE";
    if (UniversalInputManager.Get().IsTouchMode())
    {
      if (GameStrings.HasKey(headerKey + "_TOUCH"))
        headerKey += "_TOUCH";
      if (GameStrings.HasKey(descriptionFormat + "_TOUCH"))
        descriptionFormat += "_TOUCH";
    }
    this.ShowTooltip(headerKey, descriptionFormat, this.m_TavernBrawlTooltipZone, this.m_TavernBrawlButton);
  }

  private void OnButtonOut(UIEvent e)
  {
    this.HideTooltip();
  }

  private void OnStandardDuelButtonReleased(UIEvent e)
  {
    if (!this.m_bHasStandardDeck)
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
        m_text = GameStrings.Format("GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGER_NO_STANDARD_DECK"),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
      });
    else
      FriendChallengeMgr.Get().SendChallenge(this.GetComponentInParent<FriendListChallengeButton>().GetPlayer(), FormatType.FT_STANDARD);
  }

  private void OnWildDuelButtonReleased(UIEvent e)
  {
    if (!this.m_bHasWildDeck)
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
        m_text = GameStrings.Format("GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGER_NO_DECK"),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
      });
    else
      FriendChallengeMgr.Get().SendChallenge(this.GetComponentInParent<FriendListChallengeButton>().GetPlayer(), FormatType.FT_WILD);
  }

  private void OnTavernBrawlButtonReleased(UIEvent e)
  {
    if (!this.m_bIsTavernBrawlUnlocked)
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
        m_text = GameStrings.Format("GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGER_TAVERN_BRAWL_LOCKED"),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
      });
    else if (!this.m_bCanChallengeTavernBrawl)
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
        m_text = GameStrings.Format("GLOBAL_TAVERN_BRAWL_ERROR_SEASON_INCREMENTED"),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
      });
    else if (!this.m_bHasTavernBrawlDeck)
      FriendChallengeMgr.ShowChallengerNeedsToCreateTavernBrawlDeckAlert();
    else
      FriendChallengeMgr.Get().SendTavernBrawlChallenge(this.GetComponentInParent<FriendListChallengeButton>().GetPlayer());
  }

  private void ShowTooltip(string headerKey, string descriptionFormat, TooltipZone tooltipZone, UIBButton button)
  {
    string headline = GameStrings.Get(headerKey);
    BnetPlayer player = this.GetComponentInParent<FriendListChallengeButton>().GetPlayer();
    string bodytext = GameStrings.Format(descriptionFormat, (object) player.GetBestName());
    this.HideTooltip();
    tooltipZone.ShowSocialTooltip((Component) button, headline, bodytext, 75f, GameLayer.BattleNetDialog);
    tooltipZone.AnchorTooltipTo(button.gameObject, Anchor.TOP_RIGHT, Anchor.TOP_LEFT);
  }

  private void UpdateTooltip()
  {
  }

  private void HideTooltip()
  {
    if ((Object) this.m_StandardDuelTooltipZone != (Object) null)
      this.m_StandardDuelTooltipZone.HideTooltip();
    if ((Object) this.m_WildDuelTooltipZone != (Object) null)
      this.m_WildDuelTooltipZone.HideTooltip();
    if (!((Object) this.m_TavernBrawlTooltipZone != (Object) null))
      return;
    this.m_TavernBrawlTooltipZone.HideTooltip();
  }
}
