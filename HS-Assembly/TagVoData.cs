﻿// Decompiled with JetBrains decompiler
// Type: TagVoData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TagVoData
{
  public List<TagVoRequirement> m_TagRequirements = new List<TagVoRequirement>();
  public AudioSource m_AudioSource;
}
