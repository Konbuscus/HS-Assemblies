﻿// Decompiled with JetBrains decompiler
// Type: PlayButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlayButton : PegUIElement
{
  public Vector3 m_pressMovement = new Vector3(0.0f, -0.9f, 0.0f);
  private HighlightState m_playButtonHighlightState;
  private bool m_isStarted;
  public UberText m_newPlayButtonText;

  protected override void Awake()
  {
    base.Awake();
    SoundManager.Get().Load("play_button_mouseover");
    this.m_playButtonHighlightState = this.gameObject.GetComponentInChildren<HighlightState>();
    this.SetOriginalLocalPosition();
  }

  protected void Start()
  {
    this.m_isStarted = true;
    if (this.IsEnabled())
      this.Enable();
    else
      this.Disable();
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    SoundManager.Get().LoadAndPlay("play_button_mouseover", this.gameObject);
    if (!((Object) this.m_playButtonHighlightState != (Object) null))
      return;
    this.m_playButtonHighlightState.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_MOUSE_OVER);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) this.GetOriginalLocalPosition(), (object) "isLocal", (object) true, (object) "time", (object) 0.25f));
    if (!((Object) this.m_playButtonHighlightState != (Object) null))
      return;
    this.m_playButtonHighlightState.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }

  public void ChangeHighlightState(ActorStateType stateType)
  {
    if ((Object) this.m_playButtonHighlightState == (Object) null)
      return;
    this.m_playButtonHighlightState.ChangeState(stateType);
  }

  public void Disable()
  {
    this.SetEnabled(false);
    if (!this.m_isStarted || !((Object) this.m_playButtonHighlightState != (Object) null))
      return;
    this.GetComponent<PlayMakerFSM>().SendEvent("Cancel");
    this.m_playButtonHighlightState.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }

  public void Enable()
  {
    this.SetEnabled(true);
    this.m_newPlayButtonText.UpdateNow();
    if (!this.m_isStarted)
      return;
    if ((Object) this.m_newPlayButtonText != (Object) null)
      this.m_newPlayButtonText.TextAlpha = 1f;
    if (!((Object) this.m_playButtonHighlightState != (Object) null))
      return;
    this.GetComponent<PlayMakerFSM>().SendEvent("Birth");
    if (!((Object) this.m_playButtonHighlightState != (Object) null))
      return;
    this.m_playButtonHighlightState.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }

  protected override void OnPress()
  {
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) (this.GetOriginalLocalPosition() + this.m_pressMovement), (object) "isLocal", (object) true, (object) "time", (object) 0.25f));
    this.ChangeHighlightState(ActorStateType.HIGHLIGHT_OFF);
    SoundManager.Get().LoadAndPlay("collection_manager_select_hero");
  }

  protected override void OnRelease()
  {
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) this.GetOriginalLocalPosition(), (object) "isLocal", (object) true, (object) "time", (object) 0.25f));
  }

  public void SetText(string newText)
  {
    if (!((Object) this.m_newPlayButtonText != (Object) null))
      return;
    this.m_newPlayButtonText.Text = newText;
  }
}
