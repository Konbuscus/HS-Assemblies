﻿// Decompiled with JetBrains decompiler
// Type: MineCartRushArt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class MineCartRushArt : MonoBehaviour
{
  public List<Texture2D> m_portraits = new List<Texture2D>();
  public float m_portraitSwapDelay = 0.5f;
  public Spell m_portraitSwapSpell;

  public void DoPortraitSwap(Actor actor)
  {
    this.StartCoroutine(this.DoPortraitSwapWithTiming(actor));
  }

  [DebuggerHidden]
  private IEnumerator DoPortraitSwapWithTiming(Actor actor)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MineCartRushArt.\u003CDoPortraitSwapWithTiming\u003Ec__Iterator2B()
    {
      actor = actor,
      \u003C\u0024\u003Eactor = actor,
      \u003C\u003Ef__this = this
    };
  }

  private Texture2D GetNextPortrait()
  {
    if (this.m_portraits.Count == 0)
      return (Texture2D) null;
    if (this.m_portraits.Count == 1)
      return this.m_portraits[0];
    Texture2D portrait = this.m_portraits[0];
    int index = Random.Range(1, this.m_portraits.Count);
    this.m_portraits[0] = this.m_portraits[index];
    this.m_portraits[index] = portrait;
    return this.m_portraits[0];
  }
}
