﻿// Decompiled with JetBrains decompiler
// Type: CollectibleCardHeroesFilter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;

public class CollectibleCardHeroesFilter : CollectibleCardFilter
{
  private int m_heroesPerPage = 6;
  private List<CollectibleCard> m_allResults = new List<CollectibleCard>();

  public void Init(int heroesPerPage)
  {
    this.m_heroesPerPage = heroesPerPage;
    this.FilterHero(true);
    this.FilterOnlyOwned(false);
  }

  public void UpdateResults()
  {
    this.m_allResults = this.GenerateList().OrderByDescending<CollectibleCard, int>((Func<CollectibleCard, int>) (c => c.OwnedCount)).ThenBy<CollectibleCard, TAG_CLASS>((Func<CollectibleCard, TAG_CLASS>) (c => c.Class)).ToList<CollectibleCard>();
    this.m_allResults.RemoveAll((Predicate<CollectibleCard>) (c =>
    {
      if (c.PremiumType == TAG_PREMIUM.GOLDEN)
        return c.OwnedCount == 0;
      return false;
    }));
    this.m_allResults.RemoveAll((Predicate<CollectibleCard>) (c =>
    {
      if (c.PremiumType == TAG_PREMIUM.GOLDEN)
        return false;
      CollectibleCard card = CollectionManager.Get().GetCard(c.CardId, TAG_PREMIUM.GOLDEN);
      if (card != null)
        return card.OwnedCount > 0;
      return false;
    }));
  }

  public List<CollectibleCard> GetHeroesContents(int currentPage)
  {
    int totalNumPages = this.GetTotalNumPages();
    if (currentPage > totalNumPages)
      return (List<CollectibleCard>) null;
    return this.m_allResults.Skip<CollectibleCard>(this.m_heroesPerPage * (currentPage - 1)).Take<CollectibleCard>(this.m_heroesPerPage).ToList<CollectibleCard>();
  }

  public int GetTotalNumPages()
  {
    int count = this.m_allResults.Count;
    return count / this.m_heroesPerPage + (count % this.m_heroesPerPage <= 0 ? 0 : 1);
  }
}
