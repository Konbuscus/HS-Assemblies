﻿// Decompiled with JetBrains decompiler
// Type: PegasusPacket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.IO;

public class PegasusPacket : PacketFormat
{
  private const int TYPE_BYTES = 4;
  private const int SIZE_BYTES = 4;
  public int Size;
  public int Type;
  public int Context;
  public object Body;
  private bool sizeRead;
  private bool typeRead;

  public PegasusPacket()
  {
  }

  public PegasusPacket(int type, int context, object body)
  {
    this.Type = type;
    this.Context = context;
    this.Size = -1;
    this.Body = body;
  }

  public PegasusPacket(int type, int context, int size, object body)
  {
    this.Type = type;
    this.Context = context;
    this.Size = size;
    this.Body = body;
  }

  public object GetBody()
  {
    return this.Body;
  }

  public override bool IsLoaded()
  {
    return this.Body != null;
  }

  public override int Decode(byte[] bytes, int offset, int available)
  {
    string str = string.Empty;
    for (int index = 0; index < 8 && index < available; ++index)
      str = str + (object) bytes[offset + index] + " ";
    int num = 0;
    if (!this.typeRead)
    {
      if (available < 4)
        return num;
      this.Type = BitConverter.ToInt32(bytes, offset);
      this.typeRead = true;
      available -= 4;
      num += 4;
      offset += 4;
    }
    if (!this.sizeRead)
    {
      if (available < 4)
        return num;
      this.Size = BitConverter.ToInt32(bytes, offset);
      this.sizeRead = true;
      available -= 4;
      num += 4;
      offset += 4;
    }
    if (this.Body == null && available >= this.Size)
    {
      byte[] numArray = new byte[this.Size];
      Array.Copy((Array) bytes, offset, (Array) numArray, 0, this.Size);
      this.Body = (object) numArray;
      num += this.Size;
    }
    return num;
  }

  public override byte[] Encode()
  {
    if (!(this.Body is IProtoBuf))
      return (byte[]) null;
    IProtoBuf body = (IProtoBuf) this.Body;
    this.Size = (int) body.GetSerializedSize();
    byte[] buffer = new byte[this.Size + 4 + 4];
    Array.Copy((Array) BitConverter.GetBytes(this.Type), 0, (Array) buffer, 0, 4);
    Array.Copy((Array) BitConverter.GetBytes(this.Size), 0, (Array) buffer, 4, 4);
    body.Serialize((Stream) new MemoryStream(buffer, 8, this.Size));
    return buffer;
  }
}
