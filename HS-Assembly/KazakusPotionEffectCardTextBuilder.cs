﻿// Decompiled with JetBrains decompiler
// Type: KazakusPotionEffectCardTextBuilder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class KazakusPotionEffectCardTextBuilder : CardTextBuilder
{
  private string GetCorrectSubstring(string text)
  {
    int length = text.IndexOf('@');
    if (length >= 0)
      return text.Substring(0, length);
    return text;
  }

  public override string BuildCardTextInHand(Entity entity)
  {
    return this.GetCorrectSubstring(base.BuildCardTextInHand(entity));
  }

  public override string BuildCardTextInHand(EntityDef entityDef)
  {
    return this.GetCorrectSubstring(base.BuildCardTextInHand(entityDef));
  }

  public override string BuildCardTextInHistory(Entity entity)
  {
    return this.GetCorrectSubstring(base.BuildCardTextInHistory(entity));
  }
}
