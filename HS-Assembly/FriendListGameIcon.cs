﻿// Decompiled with JetBrains decompiler
// Type: FriendListGameIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using UnityEngine;

public class FriendListGameIcon : MonoBehaviour
{
  public GameObject m_Icon;
  private BnetProgramId m_programId;
  private string m_currentIcon;
  private string m_loadingIcon;

  public BnetProgramId GetProgramId()
  {
    return this.m_programId;
  }

  public void SetProgramId(BnetProgramId programId)
  {
    if ((bgs.FourCC) this.m_programId == (bgs.FourCC) programId)
      return;
    this.m_programId = programId;
    this.UpdateIcon();
  }

  public bool IsIconReady()
  {
    if (this.m_loadingIcon == null)
      return this.m_currentIcon != null;
    return false;
  }

  public bool IsIconLoading()
  {
    return this.m_loadingIcon != null;
  }

  private void UpdateIcon()
  {
    string str = !((bgs.FourCC) this.m_programId == (bgs.FourCC) null) ? BnetProgramId.GetTextureName(this.m_programId) : (string) null;
    if (str == null)
    {
      this.m_currentIcon = (string) null;
      this.m_loadingIcon = (string) null;
      this.m_Icon.GetComponent<Renderer>().material.mainTexture = (Texture) null;
    }
    else
    {
      if (this.m_currentIcon == str || this.m_loadingIcon == str)
        return;
      this.m_loadingIcon = str;
      AssetLoader.Get().LoadTexture(this.m_loadingIcon, new AssetLoader.ObjectCallback(this.OnIconLoaded), (object) null, false);
    }
  }

  private void OnIconLoaded(string name, Object obj, object callbackData)
  {
    if (name != this.m_loadingIcon)
      return;
    Texture texture = obj as Texture;
    if ((Object) texture == (Object) null)
    {
      Error.AddDevFatal("FriendListGameIcon.OnIconLoaded() - Failed to load {0}. ProgramId={1}", (object) name, (object) this.m_programId);
      this.m_currentIcon = (string) null;
      this.m_loadingIcon = (string) null;
    }
    else
    {
      this.m_currentIcon = this.m_loadingIcon;
      this.m_loadingIcon = (string) null;
      this.m_Icon.GetComponent<Renderer>().material.mainTexture = texture;
    }
  }
}
