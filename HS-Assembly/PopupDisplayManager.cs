﻿// Decompiled with JetBrains decompiler
// Type: PopupDisplayManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class PopupDisplayManager : MonoBehaviour
{
  private List<Reward> m_rewards = new List<Reward>();
  private List<Reward> m_outOfBandCardRewards = new List<Reward>();
  private List<Achievement> m_completedAchieves = new List<Achievement>();
  private HashSet<long> m_seenNoticeIDs = new HashSet<long>();
  private List<Action> m_continueListeners = new List<Action>();
  private List<PopupDisplayManager.AllPopupsShownListener> m_allPopupsShownListeners = new List<PopupDisplayManager.AllPopupsShownListener>();
  private List<PopupDisplayManager.CompletedQuestShownListener> m_questCompletedShownListeners = new List<PopupDisplayManager.CompletedQuestShownListener>();
  public Transform m_TavernBrawlRewardsBone;
  private int m_numRewardsToLoad;
  private static PopupDisplayManager s_instance;
  private bool m_isShowing;
  private bool m_readyToShowPopups;

  public bool IsShowing
  {
    get
    {
      return this.m_isShowing || (UnityEngine.Object) DialogManager.Get() != (UnityEngine.Object) null && DialogManager.Get().ShowingDialog() || (UnityEngine.Object) WelcomeQuests.Get() != (UnityEngine.Object) null;
    }
  }

  public void Awake()
  {
    PopupDisplayManager.s_instance = this;
  }

  public void Start()
  {
    AchieveManager.Get().RegisterAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(this.OnAchievesUpdated), (object) null);
    NetCache.Get().RegisterNewNoticesListener(new NetCache.DelNewNoticesListener(PopupDisplayManager.s_instance.OnNewNotices));
  }

  public void Update()
  {
    if (!this.m_readyToShowPopups)
      return;
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    switch (mode)
    {
      case SceneMgr.Mode.GAMEPLAY:
        break;
      case SceneMgr.Mode.STARTUP:
        break;
      default:
        if (this.IsShowing)
          break;
        if (ReturningPlayerMgr.Get().ShowReturningPlayerWelcomeBannerIfNeeded(new ReturningPlayerMgr.WelcomeBannerCloseCallback(this.OnBannerClosed)))
        {
          this.m_isShowing = true;
          break;
        }
        if (BannerManager.Get().ShowOutstandingBannerEvent(new BannerManager.DelOnCloseBanner(this.OnBannerClosed)))
        {
          this.m_isShowing = true;
          break;
        }
        if ((this.m_completedAchieves.Count > 0 || this.m_rewards.Count > 0 || this.m_outOfBandCardRewards.Count > 0) && (this.ShowNextCompletedQuest() || this.ShowNextUnAckedReward() || this.ShowNextUnAckedOutOfBandCardReward()) || this.ShowNextTavernBrawlReward())
          break;
        if (mode == SceneMgr.Mode.LOGIN)
          this.ShowFixedRewards(new HashSet<RewardVisualTiming>()
          {
            RewardVisualTiming.OUT_OF_BAND,
            RewardVisualTiming.IMMEDIATE
          });
        else
          this.ShowFixedRewards(new HashSet<RewardVisualTiming>()
          {
            RewardVisualTiming.IMMEDIATE
          });
        if (this.m_allPopupsShownListeners.Count <= 0)
          break;
        Log.Achievements.Print("PopupDisplayManager: Calling AllAchievesShownListeners callbacks");
        this.FireAllPopupsShownEvents();
        break;
    }
  }

  public static PopupDisplayManager Get()
  {
    return PopupDisplayManager.s_instance;
  }

  public void Initialize()
  {
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
  }

  public void WillReset()
  {
    ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
    this.m_readyToShowPopups = false;
    this.m_seenNoticeIDs.Clear();
  }

  public void ClearSeenNotices()
  {
    this.m_seenNoticeIDs.Clear();
  }

  public void ReadyToShowPopups()
  {
    this.m_readyToShowPopups = true;
    this.Update();
  }

  public bool RewardsLeftToShow()
  {
    if (!this.m_isShowing && this.m_numRewardsToLoad <= 0)
      return this.m_completedAchieves.Count > 0;
    return true;
  }

  public void ShowAnyOutstandingPopups()
  {
    this.ShowAnyOutstandingPopups((PopupDisplayManager.AllPopupsShownCallback) null);
  }

  public void ShowAnyOutstandingPopups(PopupDisplayManager.AllPopupsShownCallback callback)
  {
    this.OnAchievesUpdated(new List<Achievement>(), new List<Achievement>(), (object) null);
    if (callback != null)
      this.AddAllPopupsShownListener(callback, (object) null);
    this.ReadyToShowPopups();
  }

  public bool RegisterContinueListener(Action callback)
  {
    if (callback == null || this.m_continueListeners.Contains(callback))
      return false;
    this.m_continueListeners.Add(callback);
    return true;
  }

  public bool RemoveContinueListener(Action callback)
  {
    if (callback == null || !this.m_continueListeners.Contains(callback))
      return false;
    this.m_continueListeners.Remove(callback);
    return true;
  }

  private void FireContinueListeners()
  {
    for (int index = 0; index < this.m_continueListeners.Count; ++index)
      this.m_continueListeners[index]();
  }

  public bool AddAllPopupsShownListener(PopupDisplayManager.AllPopupsShownCallback callback)
  {
    return this.AddAllPopupsShownListener(callback, (object) null);
  }

  public bool AddAllPopupsShownListener(PopupDisplayManager.AllPopupsShownCallback callback, object userData)
  {
    if (callback == null)
      return false;
    PopupDisplayManager.AllPopupsShownListener popupsShownListener = new PopupDisplayManager.AllPopupsShownListener();
    popupsShownListener.SetCallback(callback);
    popupsShownListener.SetUserData(userData);
    if (this.m_allPopupsShownListeners.Contains(popupsShownListener))
      return false;
    this.m_allPopupsShownListeners.Add(popupsShownListener);
    return true;
  }

  private void FireAllPopupsShownEvents()
  {
    foreach (PopupDisplayManager.AllPopupsShownListener popupsShownListener in this.m_allPopupsShownListeners.ToArray())
      popupsShownListener.Fire();
    this.m_allPopupsShownListeners.Clear();
  }

  public bool RegisterCompletedQuestShownListener(PopupDisplayManager.CompletedQuestShownCallback callback)
  {
    if (callback == null)
      return false;
    PopupDisplayManager.CompletedQuestShownListener questShownListener = new PopupDisplayManager.CompletedQuestShownListener();
    questShownListener.SetCallback(callback);
    questShownListener.SetUserData((object) null);
    if (this.m_questCompletedShownListeners.Contains(questShownListener))
      return false;
    this.m_questCompletedShownListeners.Add(questShownListener);
    return true;
  }

  public bool RemoveCompletedQuestShownListener(PopupDisplayManager.CompletedQuestShownCallback callback)
  {
    if (callback == null)
      return false;
    PopupDisplayManager.CompletedQuestShownListener questShownListener = new PopupDisplayManager.CompletedQuestShownListener();
    questShownListener.SetCallback(callback);
    questShownListener.SetUserData((object) null);
    return this.m_questCompletedShownListeners.Remove(questShownListener);
  }

  private void FireAllCompletedQuestShownListeners(int achieveId)
  {
    foreach (PopupDisplayManager.CompletedQuestShownListener questShownListener in this.m_questCompletedShownListeners.ToArray())
      questShownListener.Fire(achieveId);
  }

  private void OnRewardObjectLoaded(Reward reward, object callbackData)
  {
    this.LoadReward(reward, ref this.m_rewards);
  }

  private void OnOutOfBandCardRewardObjectLoaded(Reward reward, object callbackData)
  {
    this.LoadReward(reward, ref this.m_outOfBandCardRewards);
  }

  private void LoadReward(Reward reward, ref List<Reward> allRewards)
  {
    reward.Hide(false);
    this.PositionReward(reward);
    allRewards.Add(reward);
    if (reward.RewardType == Reward.Type.CARD)
      (reward as CardReward).MakeActorsUnlit();
    SceneUtils.SetLayer(reward.gameObject, GameLayer.Default);
    --this.m_numRewardsToLoad;
    if (this.m_numRewardsToLoad > 0)
      return;
    RewardUtils.SortRewards(ref allRewards);
  }

  private void DisplayLoadedRewardObject(Reward reward, object callbackData)
  {
    reward.Hide(false);
    this.PositionReward(reward);
    SceneUtils.SetLayer(reward.gameObject, GameLayer.IgnoreFullScreenEffects);
    RewardUtils.ShowReward(UserAttentionBlocker.NONE, reward, false, this.GetRewardPunchScale(), this.GetRewardScale(), new AnimationUtil.DelOnShownWithPunch(this.OnRewardShown), (object) reward);
    this.m_isShowing = true;
  }

  private void PositionReward(Reward reward)
  {
    reward.transform.parent = this.transform;
    reward.transform.localRotation = Quaternion.identity;
    reward.transform.localPosition = this.GetRewardLocalPos();
  }

  private bool ShowNextCompletedQuest()
  {
    UserAttentionBlocker blocker = UserAttentionBlocker.NONE;
    if (QuestToast.IsQuestActive())
      QuestToast.GetCurrentToast().CloseQuestToast();
    if (this.m_completedAchieves.Count == 0)
      return false;
    Achievement completedAchieve = this.m_completedAchieves[0];
    this.m_isShowing = true;
    if ((long) completedAchieve.ID == 339L)
      blocker = UserAttentionBlocker.SET_ROTATION_INTRO;
    this.m_completedAchieves.Remove(completedAchieve);
    if (!completedAchieve.UseGenericRewardVisual)
    {
      bool updateCacheValues = SceneMgr.Get().GetMode() != SceneMgr.Mode.LOGIN;
      QuestToast.ShowQuestToast(blocker, (QuestToast.DelOnCloseQuestToast) (userData =>
      {
        this.m_isShowing = false;
        this.FireContinueListeners();
      }), updateCacheValues, completedAchieve);
      this.FireAllCompletedQuestShownListeners(completedAchieve.ID);
    }
    else
    {
      completedAchieve.AckCurrentProgressAndRewardNotices();
      completedAchieve.Rewards[0].LoadRewardObject(new Reward.DelOnRewardLoaded(this.DisplayLoadedRewardObject));
    }
    return true;
  }

  private bool ShowNextUnAckedReward()
  {
    UserAttentionBlocker blocker = UserAttentionBlocker.NONE;
    if (this.m_rewards.Count == 0)
      return false;
    Reward reward = this.m_rewards[0];
    this.m_rewards.RemoveAt(0);
    if (reward.Data.Origin == NetCache.ProfileNotice.NoticeOrigin.ACHIEVEMENT && reward.Data.OriginData == 339L)
      blocker = UserAttentionBlocker.SET_ROTATION_INTRO;
    if (RewardUtils.ShowReward(blocker, reward, false, this.GetRewardPunchScale(), this.GetRewardScale(), new AnimationUtil.DelOnShownWithPunch(this.OnRewardShown), (object) reward))
      this.m_isShowing = true;
    return true;
  }

  private bool ShowNextUnAckedOutOfBandCardReward()
  {
    UserAttentionBlocker blocker = UserAttentionBlocker.NONE;
    if (QuestToast.IsQuestActive())
      QuestToast.GetCurrentToast().CloseQuestToast();
    if (this.m_outOfBandCardRewards.Count == 0)
      return false;
    this.m_isShowing = true;
    Reward ofBandCardReward = this.m_outOfBandCardRewards[0];
    this.m_outOfBandCardRewards.RemoveAt(0);
    if (ofBandCardReward.Data.Origin == NetCache.ProfileNotice.NoticeOrigin.ACHIEVEMENT && ofBandCardReward.Data.OriginData == 339L)
      blocker = UserAttentionBlocker.SET_ROTATION_INTRO;
    EntityDef entityDef = DefLoader.Get().GetEntityDef((ofBandCardReward.Data as CardRewardData).CardID);
    string name = GameStrings.Get("GLUE_WELCOME_BUNDLE_RANDOM_CARD_SCROLL_TITLE");
    string description = GameStrings.Format("GLUE_WELCOME_BUNDLE_RANDOM_CARD_SCROLL_DESC", (object) entityDef.GetName());
    QuestToast.ShowFixedRewardQuestToast(blocker, (QuestToast.DelOnCloseQuestToast) (userData =>
    {
      this.m_isShowing = false;
      this.FireContinueListeners();
    }), ofBandCardReward.Data, name, description);
    return true;
  }

  private bool ShowNextTavernBrawlReward()
  {
    if (UserAttentionManager.IsBlockedBy(UserAttentionBlocker.FATAL_ERROR_SCENE) || !UserAttentionManager.CanShowAttentionGrabber("PopupDisplayManager.UpdateTavernBrawlRewards") || SceneMgr.Get().GetMode() != SceneMgr.Mode.HUB)
      return false;
    NetCache.ProfileNoticeTavernBrawlRewards tavernBrawlRewards = (NetCache.ProfileNoticeTavernBrawlRewards) NetCache.Get().GetNetObject<NetCache.NetCacheProfileNotices>().Notices.Find((Predicate<NetCache.ProfileNotice>) (obj => obj.Type == NetCache.ProfileNotice.NoticeType.TAVERN_BRAWL_REWARDS));
    if (tavernBrawlRewards == null)
      return false;
    if (ReturningPlayerMgr.Get().SuppressOldPopups)
    {
      Network.AckNotice(tavernBrawlRewards.NoticeID);
      Log.ReturningPlayer.Print("Suppressing popup for TavernBrawlRewardRewards due to being a Returning Player!");
    }
    else
    {
      this.m_isShowing = true;
      List<RewardData> rewards = Network.ConvertRewardChest(tavernBrawlRewards.Chest).Rewards;
      RewardUtils.ShowTavernBrawlRewards(tavernBrawlRewards.Wins, rewards, this.m_TavernBrawlRewardsBone, new Action(this.ShowTavernBrawlRewardsWhenReady_DoneCallback), true, tavernBrawlRewards.NoticeID);
    }
    return true;
  }

  private bool ShowFixedRewards(HashSet<RewardVisualTiming> rewardTimings)
  {
    bool flag = false;
    using (HashSet<RewardVisualTiming>.Enumerator enumerator = rewardTimings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (FixedRewardsMgr.Get().HasRewardsToShow(enumerator.Current))
        {
          flag = true;
          break;
        }
      }
    }
    if (!flag)
      return false;
    UserAttentionBlocker blocker = UserAttentionBlocker.NONE;
    FixedRewardsMgr.DelOnAllFixedRewardsShown allRewardsShownCallback = (FixedRewardsMgr.DelOnAllFixedRewardsShown) (userData => this.m_isShowing = false);
    Log.Achievements.Print("PopupDisplayManager: Showing Fixed Rewards");
    if (!FixedRewardsMgr.Get().ShowFixedRewards(blocker, rewardTimings, allRewardsShownCallback, (FixedRewardsMgr.DelPositionNonToastReward) null, this.GetRewardPunchScale(), this.GetRewardScale()))
    {
      allRewardsShownCallback((object) null);
      return false;
    }
    this.m_isShowing = true;
    return true;
  }

  public Vector3 GetRewardLocalPos()
  {
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.GAMEPLAY)
      return new Vector3(0.1438589f, 31.27692f, 12.97332f);
    return (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(-7.72f, 8.371922f, -3.883112f), Phone = new Vector3(-7.72f, 7.3f, -3.94f) };
  }

  public Vector3 GetRewardScale()
  {
    switch (SceneMgr.Get().GetMode())
    {
      case SceneMgr.Mode.GAMEPLAY:
        return (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = Vector3.one, Phone = new Vector3(0.8f, 0.8f, 0.8f) };
      case SceneMgr.Mode.ADVENTURE:
        return (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(10f, 10f, 10f), Phone = new Vector3(7f, 7f, 7f) };
      default:
        return (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(15f, 15f, 15f), Phone = new Vector3(8f, 8f, 8f) };
    }
  }

  public Vector3 GetRewardPunchScale()
  {
    switch (SceneMgr.Get().GetMode())
    {
      case SceneMgr.Mode.GAMEPLAY:
        return (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(1.2f, 1.2f, 1.2f), Phone = new Vector3(1.25f, 1.25f, 1.25f) };
      case SceneMgr.Mode.ADVENTURE:
        return (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(10.2f, 10.2f, 10.2f), Phone = new Vector3(7.1f, 7.1f, 7.1f) };
      default:
        return (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(15.1f, 15.1f, 15.1f), Phone = new Vector3(8.1f, 8.1f, 8.1f) };
    }
  }

  private void UpdateRewards()
  {
    NetCache.NetCacheProfileNotices netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileNotices>();
    List<RewardData> rewardsToShow = new List<RewardData>();
    List<RewardData> outOfBandRewardsToShow = new List<RewardData>();
    if (netObject != null)
    {
      List<RewardData> rewards = RewardUtils.GetRewards(netObject.Notices);
      HashSet<RewardVisualTiming> rewardTimings = new HashSet<RewardVisualTiming>();
      foreach (int num in Enum.GetValues(typeof (RewardVisualTiming)))
      {
        RewardVisualTiming rewardVisualTiming = (RewardVisualTiming) num;
        rewardTimings.Add(rewardVisualTiming);
      }
      RewardUtils.GetViewableRewards(rewards, rewardTimings, ref rewardsToShow, ref outOfBandRewardsToShow, ref this.m_completedAchieves);
    }
    if (ReturningPlayerMgr.Get().SuppressOldPopups)
    {
      List<Achievement> achievementList = new List<Achievement>();
      using (List<Achievement>.Enumerator enumerator = this.m_completedAchieves.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Achievement current = enumerator.Current;
          if (current.ShowToReturningPlayer)
          {
            achievementList.Add(current);
          }
          else
          {
            Log.ReturningPlayer.Print("Suppressing popup for Achievement {0} due to being a Returning Player!", (object) current);
            current.AckCurrentProgressAndRewardNotices();
          }
        }
      }
      this.m_completedAchieves = achievementList;
    }
    this.LoadRewards(rewardsToShow, new Reward.DelOnRewardLoaded(this.OnRewardObjectLoaded));
    this.LoadRewards(outOfBandRewardsToShow, new Reward.DelOnRewardLoaded(this.OnOutOfBandCardRewardObjectLoaded));
    Log.Achievements.Print("PopupDisplayManager: adding {0} rewards to load total={1}", new object[2]
    {
      (object) rewardsToShow.Count,
      (object) this.m_numRewardsToLoad
    });
  }

  private void LoadRewards(List<RewardData> rewardsToLoad, Reward.DelOnRewardLoaded callback)
  {
    using (List<RewardData>.Enumerator enumerator1 = rewardsToLoad.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        RewardData current1 = enumerator1.Current;
        List<long> noticeIds = current1.GetNoticeIDs();
        bool flag = false;
        using (List<long>.Enumerator enumerator2 = noticeIds.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            long current2 = enumerator2.Current;
            if (!this.m_seenNoticeIDs.Contains(current2))
            {
              flag = true;
              this.m_seenNoticeIDs.Add(current2);
            }
          }
        }
        if (flag)
        {
          if (ReturningPlayerMgr.Get().SuppressOldPopups && (current1.Origin == NetCache.ProfileNotice.NoticeOrigin.TOURNEY || current1.Origin == NetCache.ProfileNotice.NoticeOrigin.TAVERN_BRAWL_REWARD))
          {
            Log.ReturningPlayer.Print("Suppressing popup for Reward {0} due to being a Returning Player!", (object) current1);
            current1.AcknowledgeNotices();
          }
          else
          {
            ++this.m_numRewardsToLoad;
            current1.LoadRewardObject(callback);
          }
        }
      }
    }
  }

  private void OnRewardShown(object callbackData)
  {
    Reward reward = callbackData as Reward;
    if ((UnityEngine.Object) reward == (UnityEngine.Object) null)
      return;
    reward.RegisterClickListener(new Reward.OnClickedCallback(this.OnRewardClicked));
    reward.EnableClickCatcher(true);
  }

  private void ShowTavernBrawlRewardsWhenReady_DoneCallback()
  {
    this.m_isShowing = false;
  }

  private void OnRewardClicked(Reward reward, object userData)
  {
    reward.RemoveClickListener(new Reward.OnClickedCallback(this.OnRewardClicked));
    reward.Hide(true);
    this.m_isShowing = false;
    this.FireContinueListeners();
  }

  private void OnBannerClosed()
  {
    this.m_isShowing = false;
    this.FireContinueListeners();
  }

  private bool CanShowPopups()
  {
    return SceneMgr.Get().GetMode() != SceneMgr.Mode.GAMEPLAY || !((UnityEngine.Object) EndGameScreen.Get() == (UnityEngine.Object) null) && EndGameScreen.Get().IsDoneDisplayingRewards();
  }

  private void OnAchievesUpdated(List<Achievement> updatedAchieves, List<Achievement> completedAchieves, object userData)
  {
    if (!this.CanShowPopups())
      return;
    List<Achievement> completedAchieves1 = AchieveManager.Get().GetNewCompletedAchieves();
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    PopupDisplayManager.\u003COnAchievesUpdated\u003Ec__AnonStorey44D updatedCAnonStorey44D = new PopupDisplayManager.\u003COnAchievesUpdated\u003Ec__AnonStorey44D();
    using (List<Achievement>.Enumerator enumerator = completedAchieves1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        updatedCAnonStorey44D.achieve = enumerator.Current;
        // ISSUE: reference to a compiler-generated method
        if (this.m_completedAchieves.Find(new Predicate<Achievement>(updatedCAnonStorey44D.\u003C\u003Em__307)) != null)
        {
          // ISSUE: reference to a compiler-generated field
          Log.Achievements.Print("PopupDisplayManager: skipping completed achievement already being processed: " + (object) updatedCAnonStorey44D.achieve);
        }
        else
        {
          // ISSUE: reference to a compiler-generated field
          Log.Achievements.Print("PopupDisplayManager: adding completed achievement " + (object) updatedCAnonStorey44D.achieve);
          // ISSUE: reference to a compiler-generated field
          this.m_completedAchieves.Add(updatedCAnonStorey44D.achieve);
        }
      }
    }
    this.UpdateRewards();
  }

  private void OnNewNotices(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList)
  {
    if (!this.CanShowPopups() || newNotices.Count <= 0)
      return;
    this.UpdateRewards();
  }

  private class AllPopupsShownListener : EventListener<PopupDisplayManager.AllPopupsShownCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  private class CompletedQuestShownListener : EventListener<PopupDisplayManager.CompletedQuestShownCallback>
  {
    public void Fire(int achieveId)
    {
      this.m_callback(achieveId);
    }
  }

  public delegate void AllPopupsShownCallback(object userData);

  public delegate void CompletedQuestShownCallback(int achieveId);
}
