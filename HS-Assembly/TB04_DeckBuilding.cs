﻿// Decompiled with JetBrains decompiler
// Type: TB04_DeckBuilding
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TB04_DeckBuilding : MissionEntity
{
  private Notification PickThreePopup;
  private Notification EndOfTurnPopup;
  private Notification StartOfTurnPopup;
  private Notification CardPlayedPopup;
  private Vector3 popUpPos;
  private string textID;

  public override void PreloadAssets()
  {
    this.PreloadSound("tutorial_mission_hero_coin_mouse_away");
  }

  public override bool ShouldDoAlternateMulliganIntro()
  {
    return true;
  }

  public override bool ShouldHandleCoin()
  {
    return false;
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB04_DeckBuilding.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1F1() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
