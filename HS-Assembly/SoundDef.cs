﻿// Decompiled with JetBrains decompiler
// Type: SoundDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class SoundDef : MonoBehaviour
{
  public SoundCategory m_Category = SoundCategory.FX;
  public float m_RandomPitchMin = 1f;
  public float m_RandomPitchMax = 1f;
  public float m_RandomVolumeMin = 1f;
  public float m_RandomVolumeMax = 1f;
  public List<RandomAudioClip> m_RandomClips;
  public bool m_IgnoreDucking;
}
