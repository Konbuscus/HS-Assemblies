﻿// Decompiled with JetBrains decompiler
// Type: NAX7_05_Spell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;

public class NAX7_05_Spell : Spell
{
  protected override void OnBirth(SpellStateType prevStateType)
  {
    this.StartCoroutine(this.SpellEffect(prevStateType));
  }

  [DebuggerHidden]
  private IEnumerator SpellEffect(SpellStateType prevStateType)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX7_05_Spell.\u003CSpellEffect\u003Ec__Iterator2D()
    {
      prevStateType = prevStateType,
      \u003C\u0024\u003EprevStateType = prevStateType,
      \u003C\u003Ef__this = this
    };
  }
}
