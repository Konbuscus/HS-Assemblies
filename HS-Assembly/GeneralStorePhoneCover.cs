﻿// Decompiled with JetBrains decompiler
// Type: GeneralStorePhoneCover
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class GeneralStorePhoneCover : MonoBehaviour
{
  [CustomEditField(Sections = "Animation")]
  public string m_buttonEnterAnimation = string.Empty;
  [CustomEditField(Sections = "Animation")]
  public List<GeneralStorePhoneCover.ModeAnimation> m_buttonExitAnimations = new List<GeneralStorePhoneCover.ModeAnimation>();
  private const string s_coverAnimationCoroutine = "PlayAndWaitForAnimation";
  [CustomEditField(Sections = "General UI")]
  public GeneralStore m_parentStore;
  [CustomEditField(Sections = "General UI")]
  public PegUIElement m_backToCoverButton;
  [CustomEditField(Sections = "Animation")]
  public Animator m_animationController;
  [CustomEditField(Sections = "UI Blockers")]
  public GameObject m_coverClickArea;
  [CustomEditField(Sections = "UI Blockers")]
  public GameObject m_animationClickBlocker;
  [CustomEditField(Sections = "Aspect Ratio Positioning")]
  public float m_yPos3to2;
  [CustomEditField(Sections = "Aspect Ratio Positioning")]
  public float m_yPos16to9;
  private static GeneralStorePhoneCover s_instance;

  private void Awake()
  {
    GeneralStorePhoneCover.s_instance = this;
    StoreManager.Get().RegisterStoreShownListener(new StoreManager.StoreShownCallback(this.UpdateCoverPosition));
    this.m_parentStore.RegisterModeChangedListener(new GeneralStore.ModeChanged(this.OnGeneralStoreModeChanged));
    this.m_backToCoverButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => Navigation.GoBack()));
  }

  private void OnDestroy()
  {
    GeneralStorePhoneCover.s_instance = (GeneralStorePhoneCover) null;
    StoreManager.Get().RemoveStoreShownListener(new StoreManager.StoreShownCallback(this.UpdateCoverPosition));
  }

  private void Start()
  {
    this.ShowCover();
  }

  public void ShowCover()
  {
    this.StopCoroutine("PlayAndWaitForAnimation");
    this.StartCoroutine("PlayAndWaitForAnimation", (object) this.m_buttonEnterAnimation);
    this.m_coverClickArea.SetActive(true);
  }

  public void HideCover(GeneralStoreMode selectedMode)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStorePhoneCover.\u003CHideCover\u003Ec__AnonStorey3FA coverCAnonStorey3Fa = new GeneralStorePhoneCover.\u003CHideCover\u003Ec__AnonStorey3FA();
    // ISSUE: reference to a compiler-generated field
    coverCAnonStorey3Fa.selectedMode = selectedMode;
    this.StartCoroutine(this.PushBackMethodWhenShown());
    // ISSUE: reference to a compiler-generated method
    GeneralStorePhoneCover.ModeAnimation modeAnimation = this.m_buttonExitAnimations.Find(new Predicate<GeneralStorePhoneCover.ModeAnimation>(coverCAnonStorey3Fa.\u003C\u003Em__1E3));
    if (modeAnimation == null)
    {
      // ISSUE: reference to a compiler-generated field
      UnityEngine.Debug.LogError((object) string.Format("Unable to find animation for {0} mode.", (object) coverCAnonStorey3Fa.selectedMode));
    }
    else if (string.IsNullOrEmpty(modeAnimation.m_playAnimationName))
    {
      // ISSUE: reference to a compiler-generated field
      UnityEngine.Debug.LogError((object) string.Format("Animation name not defined for {0} mode.", (object) coverCAnonStorey3Fa.selectedMode));
    }
    else
    {
      this.StopCoroutine("PlayAndWaitForAnimation");
      this.StartCoroutine("PlayAndWaitForAnimation", (object) modeAnimation.m_playAnimationName);
      this.m_coverClickArea.SetActive(false);
    }
  }

  [DebuggerHidden]
  private IEnumerator PushBackMethodWhenShown()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStorePhoneCover.\u003CPushBackMethodWhenShown\u003Ec__Iterator270() { \u003C\u003Ef__this = this };
  }

  private void OnGeneralStoreModeChanged(GeneralStoreMode oldMode, GeneralStoreMode newMode)
  {
    if (newMode != GeneralStoreMode.NONE)
      this.HideCover(newMode);
    else
      this.ShowCover();
  }

  [DebuggerHidden]
  private IEnumerator PlayAndWaitForAnimation(string animationName)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStorePhoneCover.\u003CPlayAndWaitForAnimation\u003Ec__Iterator271() { animationName = animationName, \u003C\u0024\u003EanimationName = animationName, \u003C\u003Ef__this = this };
  }

  private void UpdateCoverPosition(object data)
  {
    TransformUtil.SetLocalPosY((Component) this, TransformUtil.GetAspectRatioDependentValue(this.m_yPos3to2, this.m_yPos16to9));
  }

  public static bool OnNavigateBack()
  {
    if ((UnityEngine.Object) GeneralStorePhoneCover.s_instance == (UnityEngine.Object) null)
      return false;
    GeneralStorePhoneCover.s_instance.m_parentStore.SetMode(GeneralStoreMode.NONE);
    return true;
  }

  [Serializable]
  public class ModeAnimation
  {
    public GeneralStoreMode m_mode;
    public string m_playAnimationName;
  }

  public delegate void AnimationCallback();
}
