﻿// Decompiled with JetBrains decompiler
// Type: HistoryCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HistoryCard : HistoryItem
{
  private readonly Color OPPONENT_COLOR = new Color(0.7137f, 0.2f, 0.1333f, 1f);
  private readonly Color FRIENDLY_COLOR = new Color(0.6509f, 0.6705f, 0.9843f, 1f);
  private PlatformDependentValue<float> MOUSE_OVER_X_OFFSET = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 4.326718f, Tablet = 4.7f, Phone = 5.4f };
  private PlatformDependentValue<float> MOUSE_OVER_SCALE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 1f, Tablet = 1.35f, Phone = 1.35f };
  private PlatformDependentValue<float> X_SIZE_OF_MOUSE_OVER_CHILD = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 2.5f, Tablet = 2.5f, Phone = 2.5f };
  private List<HistoryChildCard> m_historyChildren = new List<HistoryChildCard>();
  private const float ABILITY_CARD_ANIMATE_TO_BIG_CARD_AREA_TIME = 1f;
  private const float BIG_CARD_SCALE = 1.03f;
  private const float MOUSE_OVER_Z_OFFSET_TOP = -1.404475f;
  private const float MOUSE_OVER_Z_OFFSET_BOTTOM = 0.1681719f;
  private const float MOUSE_OVER_Z_OFFSET_PHONE = -4.75f;
  private const float MOUSE_OVER_Z_OFFSET_SECRET_PHONE = -4.3f;
  private const float MOUSE_OVER_Z_OFFSET_WITH_CREATOR_PHONE = -4.3f;
  private const float MOUSE_OVER_HEIGHT_OFFSET = 7.524521f;
  private const float MAX_WIDTH_OF_CHILDREN = 5f;
  private const string CREATED_BY_BONE_NAME = "HistoryCreatedByBone";
  public Actor m_tileActor;
  public UberText m_createdByText;
  private Material m_fullTileMaterial;
  private Material m_halfTileMaterial;
  private bool m_mousedOver;
  private bool m_halfSize;
  private bool m_hasBeenShown;
  private Actor m_separator;
  private bool m_haveDisplayedCreator;
  private bool m_gameEntityMousedOver;
  private List<HistoryInfo> m_childInfos;
  private bool m_bigCardFinishedCallbackHasRun;
  private HistoryManager.BigCardFinishedCallback m_bigCardFinishedCallback;
  private bool m_bigCardCountered;
  private bool m_bigCardWaitingForSecret;
  private bool m_bigCardFromMetaData;
  private Entity m_bigCardPostTransformedEntity;
  private float m_tileSize;

  public void LoadMainCardActor()
  {
    string name = !this.m_fatigue ? ActorNames.GetHistoryActor(this.m_entity) : "Card_Hand_Fatigue";
    GameObject gameObject = AssetLoader.Get().LoadActor(name, false, false);
    if ((Object) gameObject == (Object) null)
    {
      Debug.LogWarningFormat("HistoryCard.LoadMainCardActor() - FAILED to load actor \"{0}\"", (object) name);
    }
    else
    {
      Actor component = gameObject.GetComponent<Actor>();
      if ((Object) component == (Object) null)
      {
        Debug.LogWarningFormat("HistoryCard.LoadMainCardActor() - ERROR actor \"{0}\" has no Actor component", (object) name);
      }
      else
      {
        this.m_mainCardActor = component;
        if (this.m_fatigue)
          this.m_mainCardActor.GetPowersText().Text = GameStrings.Get("GAMEPLAY_FATIGUE_HISTORY_TEXT");
        else
          this.m_mainCardActor.SetPremium(this.m_entity.GetPremiumType());
        this.m_mainCardActor.SetHistoryItem((HistoryItem) this);
        this.InitDisplayedCreator();
      }
    }
  }

  private void InitDisplayedCreator()
  {
    if (this.m_entity == null)
      return;
    Entity displayedCreator = this.m_entity.GetDisplayedCreator();
    if (displayedCreator == null)
      return;
    GameObject bone = this.m_mainCardActor.FindBone("HistoryCreatedByBone");
    if (!(bool) ((Object) bone))
    {
      Error.AddDevWarning("Missing Bone", "Missing {0} on {1}", (object) "HistoryCreatedByBone", (object) this.m_mainCardActor);
    }
    else
    {
      this.m_createdByText.Text = GameStrings.Format("GAMEPLAY_HISTORY_CREATED_BY", (object) displayedCreator.GetName());
      this.m_createdByText.transform.parent = this.m_mainCardActor.GetRootObject().transform;
      this.m_createdByText.gameObject.SetActive(true);
      TransformUtil.SetPoint((Component) this.m_createdByText, new Vector3(0.5f, 0.0f, 1f), bone, new Vector3(0.5f, 0.0f, 0.0f));
      this.m_createdByText.gameObject.SetActive(false);
      this.m_haveDisplayedCreator = true;
    }
  }

  private void ShowDisplayedCreator()
  {
    this.m_createdByText.gameObject.SetActive(this.m_haveDisplayedCreator);
  }

  public bool HasBeenShown()
  {
    return this.m_hasBeenShown;
  }

  public void MarkAsShown()
  {
    if (this.m_hasBeenShown)
      return;
    this.m_hasBeenShown = true;
  }

  public Collider GetTileCollider()
  {
    if ((Object) this.m_tileActor == (Object) null)
      return (Collider) null;
    if ((Object) this.m_tileActor.GetMeshRenderer() == (Object) null)
      return (Collider) null;
    Transform child = this.m_tileActor.GetMeshRenderer().transform.FindChild("Collider");
    if ((Object) child == (Object) null)
      return (Collider) null;
    return child.GetComponent<Collider>();
  }

  public bool IsHalfSize()
  {
    return this.m_halfSize;
  }

  public float GetTileSize()
  {
    return this.m_tileSize;
  }

  public void LoadTile(HistoryTileInitInfo info)
  {
    this.m_childInfos = info.m_childInfos;
    if ((Object) info.m_fatigueTexture != (Object) null)
    {
      this.m_portraitTexture = info.m_fatigueTexture;
      this.m_fatigue = true;
    }
    else
    {
      this.m_entity = info.m_entity;
      this.m_portraitTexture = info.m_portraitTexture;
      this.m_portraitGoldenMaterial = info.m_portraitGoldenMaterial;
      this.m_fullTileMaterial = info.m_fullTileMaterial;
      this.m_halfTileMaterial = info.m_halfTileMaterial;
      this.m_splatAmount = info.m_splatAmount;
      this.m_dead = info.m_dead;
    }
    switch (info.m_type)
    {
      case HistoryInfoType.NONE:
      case HistoryInfoType.WEAPON_PLAYED:
      case HistoryInfoType.CARD_PLAYED:
      case HistoryInfoType.FATIGUE:
        this.LoadPlayTile();
        break;
      case HistoryInfoType.ATTACK:
        this.LoadAttackTile();
        break;
      case HistoryInfoType.TRIGGER:
        this.LoadTriggerTile();
        break;
      case HistoryInfoType.WEAPON_BREAK:
        this.LoadWeaponBreak();
        break;
    }
  }

  public void NotifyMousedOver()
  {
    if (this.m_mousedOver || (Object) this == (Object) HistoryManager.Get().GetCurrentBigCard())
      return;
    this.LoadChildCardsFromInfos();
    this.m_mousedOver = true;
    SoundManager.Get().LoadAndPlay("history_event_mouseover", this.m_tileActor.gameObject);
    if (!(bool) ((Object) this.m_mainCardActor))
    {
      this.LoadMainCardActor();
      SceneUtils.SetLayer((Component) this.m_mainCardActor, GameLayer.Tooltip);
    }
    this.ShowTile();
  }

  public void NotifyMousedOut()
  {
    if (!this.m_mousedOver)
      return;
    this.m_mousedOver = false;
    if (this.m_gameEntityMousedOver)
    {
      GameState.Get().GetGameEntity().NotifyOfHistoryTokenMousedOut();
      this.m_gameEntityMousedOver = false;
    }
    TooltipPanelManager.Get().HideKeywordHelp();
    if ((bool) ((Object) this.m_mainCardActor))
    {
      this.m_mainCardActor.ActivateAllSpellsDeathStates();
      this.m_mainCardActor.Hide();
    }
    for (int index = 0; index < this.m_historyChildren.Count; ++index)
    {
      if (!((Object) this.m_historyChildren[index].m_mainCardActor == (Object) null))
      {
        this.m_historyChildren[index].m_mainCardActor.ActivateAllSpellsDeathStates();
        this.m_historyChildren[index].m_mainCardActor.Hide();
      }
    }
    if ((bool) ((Object) this.m_separator))
      this.m_separator.Hide();
    HistoryManager.Get().UpdateLayout();
  }

  private void LoadPlayTile()
  {
    this.m_halfSize = false;
    this.LoadTileImpl("HistoryTile_Card");
    this.LoadArrowSeparator();
  }

  private void LoadAttackTile()
  {
    this.m_halfSize = true;
    this.LoadTileImpl("HistoryTile_Attack");
    this.LoadSwordsSeparator();
  }

  private void LoadWeaponBreak()
  {
    this.m_halfSize = true;
    this.LoadTileImpl("HistoryTile_Attack");
  }

  private void LoadTriggerTile()
  {
    this.m_halfSize = true;
    this.LoadTileImpl("HistoryTile_Trigger");
    this.LoadArrowSeparator();
  }

  private void LoadTileImpl(string actorName)
  {
    GameObject gameObject = AssetLoader.Get().LoadActor(actorName, false, false);
    if ((Object) gameObject == (Object) null)
    {
      Debug.LogWarningFormat("HistoryCard.LoadTileImpl() - FAILED to load actor \"{0}\"", (object) actorName);
    }
    else
    {
      Actor component = gameObject.GetComponent<Actor>();
      if ((Object) component == (Object) null)
      {
        Debug.LogWarningFormat("HistoryCard.LoadTileImpl() - ERROR actor \"{0}\" has no Actor component", (object) actorName);
      }
      else
      {
        this.m_tileActor = component;
        this.m_tileActor.transform.parent = this.transform;
        TransformUtil.Identity((Component) this.m_tileActor.transform);
        this.m_tileActor.transform.localScale = HistoryManager.Get().transform.localScale;
        Material[] materialArray = new Material[2]{ this.m_tileActor.GetMeshRenderer().materials[0], null };
        if (this.m_halfSize)
        {
          if ((Object) this.m_halfTileMaterial != (Object) null)
          {
            materialArray[1] = this.m_halfTileMaterial;
            this.m_tileActor.GetMeshRenderer().materials = materialArray;
          }
          else
            this.m_tileActor.GetMeshRenderer().materials[1].mainTexture = this.m_portraitTexture;
        }
        else if ((Object) this.m_fullTileMaterial != (Object) null)
        {
          materialArray[1] = this.m_fullTileMaterial;
          this.m_tileActor.GetMeshRenderer().materials = materialArray;
        }
        else
          this.m_tileActor.GetMeshRenderer().materials[1].mainTexture = this.m_portraitTexture;
        Color color = Color.white;
        if ((Object) Board.Get() != (Object) null)
          color = Board.Get().m_HistoryTileColor;
        if (!this.m_fatigue)
        {
          if (this.m_entity.IsControlledByFriendlySidePlayer())
            color *= this.FRIENDLY_COLOR;
          else
            color *= this.OPPONENT_COLOR;
        }
        foreach (Renderer componentsInChild in this.m_tileActor.GetMeshRenderer().GetComponentsInChildren<Renderer>())
        {
          if (!(componentsInChild.tag == "FakeShadow"))
            componentsInChild.material.color = Board.Get().m_HistoryTileColor;
        }
        this.m_tileActor.GetMeshRenderer().materials[0].color = color;
        this.m_tileActor.GetMeshRenderer().materials[1].color = Board.Get().m_HistoryTileColor;
        if (!((Object) this.GetTileCollider() != (Object) null))
          return;
        this.m_tileSize = this.GetTileCollider().bounds.size.z;
      }
    }
  }

  private void LoadSwordsSeparator()
  {
    this.LoadSeparator("History_Swords");
  }

  private void LoadArrowSeparator()
  {
    if (this.m_childInfos == null || this.m_childInfos.Count == 0)
      return;
    this.LoadSeparator("History_Arrow");
  }

  private void LoadSeparator(string actorName)
  {
    GameObject gameObject = AssetLoader.Get().LoadActor(actorName, false, false);
    if ((Object) gameObject == (Object) null)
    {
      Debug.LogWarning((object) string.Format("HistoryCard.LoadSeparator() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      Actor component1 = gameObject.GetComponent<Actor>();
      if ((Object) component1 == (Object) null)
      {
        Debug.LogWarning((object) string.Format("HistoryCard.LoadSeparator() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        this.m_separator = component1;
        MeshRenderer component2 = this.m_separator.GetRootObject().transform.FindChild("Blue").gameObject.GetComponent<MeshRenderer>();
        MeshRenderer component3 = this.m_separator.GetRootObject().transform.FindChild("Red").gameObject.GetComponent<MeshRenderer>();
        if (this.m_fatigue)
        {
          component3.enabled = true;
          component2.enabled = false;
        }
        else
        {
          bool flag = this.m_entity.IsControlledByFriendlySidePlayer();
          component2.enabled = flag;
          component3.enabled = !flag;
        }
        this.m_separator.transform.parent = this.transform;
        TransformUtil.Identity((Component) this.m_separator.transform);
        if ((Object) this.m_separator.GetRootObject() != (Object) null)
          TransformUtil.Identity((Component) this.m_separator.GetRootObject().transform);
        this.m_separator.Hide();
      }
    }
  }

  private void LoadChildCardsFromInfos()
  {
    if (this.m_childInfos == null)
      return;
    using (List<HistoryInfo>.Enumerator enumerator = this.m_childInfos.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HistoryInfo current = enumerator.Current;
        HistoryChildCard component = AssetLoader.Get().LoadActor("HistoryChildCard", false, false).GetComponent<HistoryChildCard>();
        this.m_historyChildren.Add(component);
        Entity duplicatedEntity = current.GetDuplicatedEntity();
        CardDef cardDef = duplicatedEntity.GetCardDef();
        component.SetCardInfo(duplicatedEntity, cardDef.GetPortraitTexture(), cardDef.GetPremiumPortraitMaterial(), current.GetSplatAmount(), current.HasDied());
        component.transform.parent = this.transform;
        component.LoadMainCardActor();
      }
    }
    this.m_childInfos = (List<HistoryInfo>) null;
  }

  private void ShowTile()
  {
    if (!this.m_mousedOver)
    {
      this.m_mainCardActor.Hide();
    }
    else
    {
      this.m_mainCardActor.Show();
      this.ShowDisplayedCreator();
      this.InitializeMainCardActor();
      this.DisplaySpells();
      if ((bool) UniversalInputManager.UsePhoneUI)
        this.m_mainCardActor.transform.position = new Vector3(this.transform.position.x + (float) this.MOUSE_OVER_X_OFFSET, this.transform.position.y + 7.524521f, this.GetZOffsetForThisTilesMouseOverCard());
      else
        this.m_mainCardActor.transform.position = new Vector3(this.transform.position.x + (float) this.MOUSE_OVER_X_OFFSET, this.transform.position.y + 7.524521f, this.transform.position.z + this.GetZOffsetForThisTilesMouseOverCard());
      this.m_mainCardActor.transform.localScale = new Vector3((float) this.MOUSE_OVER_SCALE, 1f, (float) this.MOUSE_OVER_SCALE);
      if ((bool) UniversalInputManager.UsePhoneUI && this.m_fatigue)
        this.m_mainCardActor.transform.localScale = new Vector3(1f, 1f, 1f);
      if (!this.m_gameEntityMousedOver)
      {
        this.m_gameEntityMousedOver = true;
        GameState.Get().GetGameEntity().NotifyOfHistoryTokenMousedOver(this.gameObject);
      }
      if (!this.m_fatigue)
        TooltipPanelManager.Get().UpdateKeywordHelpForHistoryCard(this.m_entity, this.m_mainCardActor, this.m_createdByText);
      if (this.m_historyChildren.Count <= 0)
        return;
      float max = 1f;
      float num1 = 1f;
      if (this.m_historyChildren.Count > 4 && this.m_historyChildren.Count < 9)
      {
        num1 = 2f;
        max = 0.5f;
      }
      else if (this.m_historyChildren.Count >= 9)
      {
        num1 = 3f;
        max = 0.3f;
      }
      int num2 = Mathf.CeilToInt((float) this.m_historyChildren.Count / num1);
      float num3 = Mathf.Clamp(5f / ((float) num2 * (float) this.X_SIZE_OF_MOUSE_OVER_CHILD), 0.1f, max);
      int num4 = 0;
      int num5 = 1;
      for (int index = 0; index < this.m_historyChildren.Count; ++index)
      {
        this.m_historyChildren[index].m_mainCardActor.Show();
        this.m_historyChildren[index].InitializeMainCardActor();
        this.m_historyChildren[index].DisplaySpells();
        float z = this.m_mainCardActor.transform.position.z;
        if ((double) num1 == 2.0)
        {
          if (num5 == 1)
            z += 0.78f;
          else
            z -= 0.78f;
        }
        else if ((double) num1 == 3.0)
        {
          if (num5 == 1)
            z += 0.98f;
          else if (num5 == 3)
            z -= 0.93f;
        }
        float num6 = this.m_mainCardActor.transform.position.x + (float) ((double) (float) this.X_SIZE_OF_MOUSE_OVER_CHILD * (1.0 + (double) num3) / 2.0);
        this.m_historyChildren[index].m_mainCardActor.transform.position = new Vector3(num6 + (float) this.X_SIZE_OF_MOUSE_OVER_CHILD * (float) num4 * num3, this.m_mainCardActor.transform.position.y, z);
        this.m_historyChildren[index].m_mainCardActor.transform.localScale = new Vector3(num3, num3, num3);
        ++num4;
        if (num4 >= num2)
        {
          num4 = 0;
          ++num5;
        }
      }
      if (!((Object) this.m_separator != (Object) null))
        return;
      float num7 = 0.4f;
      float num8 = (float) this.X_SIZE_OF_MOUSE_OVER_CHILD / 2f;
      this.m_separator.Show();
      this.m_separator.transform.position = new Vector3(this.m_mainCardActor.transform.position.x + num8, this.m_mainCardActor.transform.position.y + num7, this.m_mainCardActor.transform.position.z);
    }
  }

  private float GetZOffsetForThisTilesMouseOverCard()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      return this.m_entity != null && this.m_entity.IsSecret() && this.m_entity.IsHidden() || this.m_haveDisplayedCreator ? -4.3f : -4.75f;
    float num = Mathf.Abs(-1.572647f);
    HistoryManager historyManager = HistoryManager.Get();
    return (float) ((double) (num / (float) historyManager.GetNumHistoryTiles()) * (double) (historyManager.GetNumHistoryTiles() - historyManager.GetIndexForTile(this) - 1) - 1.40447497367859);
  }

  public void LoadBigCard(HistoryBigCardInitInfo info)
  {
    this.m_entity = info.m_entity;
    this.m_portraitTexture = info.m_portraitTexture;
    this.m_portraitGoldenMaterial = info.m_portraitGoldenMaterial;
    this.m_bigCardFinishedCallback = info.m_finishedCallback;
    this.m_bigCardCountered = info.m_countered;
    this.m_bigCardWaitingForSecret = info.m_waitForSecretSpell;
    this.m_bigCardFromMetaData = info.m_fromMetaData;
    this.m_bigCardPostTransformedEntity = info.m_postTransformedEntity;
    this.LoadMainCardActor();
  }

  public void LoadBigCardPostTransformedEntity()
  {
    if (this.m_bigCardPostTransformedEntity == null)
      return;
    this.m_entity = this.m_bigCardPostTransformedEntity;
    Card card = this.m_entity.GetCard();
    this.m_portraitTexture = card.GetPortraitTexture();
    this.m_portraitGoldenMaterial = card.GetGoldenMaterial();
    this.LoadMainCardActor();
  }

  public HistoryManager.BigCardFinishedCallback GetBigCardFinishedCallback()
  {
    return this.m_bigCardFinishedCallback;
  }

  public void RunBigCardFinishedCallback()
  {
    if (this.m_bigCardFinishedCallbackHasRun)
      return;
    this.m_bigCardFinishedCallbackHasRun = true;
    if (this.m_bigCardFinishedCallback == null)
      return;
    this.m_bigCardFinishedCallback();
  }

  public bool WasBigCardCountered()
  {
    return this.m_bigCardCountered;
  }

  public bool IsBigCardWaitingForSecret()
  {
    return this.m_bigCardWaitingForSecret;
  }

  public bool IsBigCardFromMetaData()
  {
    return this.m_bigCardFromMetaData;
  }

  public Entity GetBigCardPostTransformedEntity()
  {
    return this.m_bigCardPostTransformedEntity;
  }

  public bool HasBigCardPostTransformedEntity()
  {
    return this.m_bigCardPostTransformedEntity != null;
  }

  public void ShowBigCard(Vector3[] pathToFollow)
  {
    this.m_mainCardActor.transform.localScale = new Vector3(1.03f, 1.03f, 1.03f);
    if (this.m_entity == null)
      return;
    if (this.m_entity.GetCardType() == TAG_CARDTYPE.SPELL || this.m_entity.GetCardType() == TAG_CARDTYPE.HERO_POWER)
    {
      pathToFollow[0] = this.m_mainCardActor.transform.position;
      iTween.MoveTo(this.m_mainCardActor.gameObject, iTween.Hash((object) "path", (object) pathToFollow, (object) "time", (object) 1f, (object) "oncomplete", (object) "OnBigCardPathComplete", (object) "oncompletetarget", (object) this.gameObject));
      iTween.ScaleTo(this.gameObject, new Vector3(1f, 1f, 1f), 1f);
      SoundManager.Get().LoadAndPlay("play_card_from_hand_1");
    }
    else
      this.ShowDisplayedCreator();
  }

  private void OnBigCardPathComplete()
  {
    this.ShowDisplayedCreator();
  }
}
