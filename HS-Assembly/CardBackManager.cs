﻿// Decompiled with JetBrains decompiler
// Type: CardBackManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class CardBackManager : MonoBehaviour
{
  private string m_FriendlyCardBackName = string.Empty;
  private string m_OpponentCardBackName = string.Empty;
  private CardBack m_DefaultCardBack;
  private CardBack m_FriendlyCardBack;
  private bool m_isFriendlyLoading;
  private CardBack m_OpponentCardBack;
  private bool m_isOpponentLoading;
  private Map<int, CardBackData> m_cardBackData;
  private Map<string, CardBack> m_LoadedCardBacks;
  private bool m_ResetDefaultRegistered;
  private bool m_DefaultCardBackChangeListenerRegistered;
  private string m_searchText;
  private static CardBackManager s_instance;

  private void Awake()
  {
    CardBackManager.s_instance = this;
    this.InitCardBackData();
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
    NetCache.Get().RegisterUpdatedListener(typeof (NetCache.NetCacheCardBacks), new Action(this.NetCache_OnNetCacheCardBacksUpdated));
  }

  private void OnDestroy()
  {
    CardBackManager.s_instance = (CardBackManager) null;
    if ((UnityEngine.Object) ApplicationMgr.Get() == (UnityEngine.Object) null)
      return;
    ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
  }

  private void Start()
  {
    Options.Get().RegisterChangedListener(Option.CARD_BACK, new Options.ChangedCallback(this.OnCheatOptionChanged));
    Options.Get().RegisterChangedListener(Option.CARD_BACK2, new Options.ChangedCallback(this.OnCheatOptionChanged));
    if (!this.m_ResetDefaultRegistered)
    {
      SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneChangeResetDefaultCardBack));
      this.m_ResetDefaultRegistered = true;
    }
    this.InitCardBacks();
    this.StartCoroutine(this.RegisterForDefaultCardBackChangesWhenPossible());
    this.StartCoroutine(this.UpdateDefaultCardBackWhenReady());
  }

  private void WillReset()
  {
    this.InitCardBackData();
  }

  public static CardBackManager Get()
  {
    return CardBackManager.s_instance;
  }

  public void SetSearchText(string searchText)
  {
    this.m_searchText = searchText == null ? (string) null : searchText.ToLower();
  }

  public int GetDeckCardBackID(long deck)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CardBackManager.\u003CGetDeckCardBackID\u003Ec__AnonStorey376 idCAnonStorey376 = new CardBackManager.\u003CGetDeckCardBackID\u003Ec__AnonStorey376();
    // ISSUE: reference to a compiler-generated field
    idCAnonStorey376.deck = deck;
    // ISSUE: reference to a compiler-generated method
    NetCache.DeckHeader deckHeader = NetCache.Get().GetNetObject<NetCache.NetCacheDecks>().Decks.Find(new Predicate<NetCache.DeckHeader>(idCAnonStorey376.\u003C\u003Em__8A));
    if (deckHeader != null)
      return deckHeader.CardBack;
    // ISSUE: reference to a compiler-generated field
    UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager.GetDeckCardBackID() could not find deck with ID {0}", (object) idCAnonStorey376.deck));
    return 0;
  }

  public CardBack GetFriendlyCardBack()
  {
    return this.m_FriendlyCardBack;
  }

  public CardBack GetOpponentCardBack()
  {
    return this.m_OpponentCardBack;
  }

  public void UpdateAllCardBacks()
  {
    this.StartCoroutine(this.UpdateAllCardBacksImpl());
  }

  public void SetGameCardBackIDs(int friendlyCardBackID, int opponentCardBackID)
  {
    this.LoadCardBack(FileUtils.GameAssetPathToName(this.m_cardBackData[this.GetValidCardBackID(friendlyCardBackID)].PrefabName), true);
    this.LoadCardBack(FileUtils.GameAssetPathToName(this.m_cardBackData[this.GetValidCardBackID(opponentCardBackID)].PrefabName), false);
    this.UpdateAllCardBacks();
  }

  public bool LoadCardBackByIndex(int cardBackIdx, CardBackManager.LoadCardBackData.LoadCardBackCallback callback, string actorName = "Card_Hidden")
  {
    return this.LoadCardBackByIndex(cardBackIdx, callback, false, actorName);
  }

  public bool LoadCardBackByIndex(int cardBackIdx, CardBackManager.LoadCardBackData.LoadCardBackCallback callback, bool unlit, string actorName = "Card_Hidden")
  {
    if (!this.m_cardBackData.ContainsKey(cardBackIdx))
    {
      Log.CardbackMgr.Print("CardBackManager.LoadCardBackByIndex() - wrong cardBackIdx {0}", (object) cardBackIdx);
      return false;
    }
    AssetLoader.Get().LoadActor(actorName, new AssetLoader.GameObjectCallback(this.OnHiddenActorLoaded), (object) new CardBackManager.LoadCardBackData()
    {
      m_CardBackIndex = cardBackIdx,
      m_Callback = callback,
      m_Unlit = unlit,
      m_Name = this.m_cardBackData[cardBackIdx].Name
    }, false);
    return true;
  }

  public CardBackManager.LoadCardBackData LoadCardBackByIndex(int cardBackIdx, bool unlit = false, string actorName = "Card_Hidden")
  {
    if (!this.m_cardBackData.ContainsKey(cardBackIdx))
    {
      Log.CardbackMgr.Print("CardBackManager.LoadCardBackByIndex() - wrong cardBackIdx {0}", (object) cardBackIdx);
      return (CardBackManager.LoadCardBackData) null;
    }
    CardBackManager.LoadCardBackData loadCardBackData = new CardBackManager.LoadCardBackData();
    loadCardBackData.m_CardBackIndex = cardBackIdx;
    loadCardBackData.m_Unlit = unlit;
    loadCardBackData.m_Name = this.m_cardBackData[cardBackIdx].Name;
    loadCardBackData.m_GameObject = AssetLoader.Get().LoadActor(actorName, false, false);
    if ((UnityEngine.Object) loadCardBackData.m_GameObject == (UnityEngine.Object) null)
    {
      Log.CardbackMgr.Print("CardBackManager.LoadCardBackByIndex() - failed to load Actor {0}", (object) actorName);
      return (CardBackManager.LoadCardBackData) null;
    }
    string prefabName = this.m_cardBackData[cardBackIdx].PrefabName;
    GameObject gameObject = AssetLoader.Get().LoadCardBack(FileUtils.GameAssetPathToName(prefabName), true, false);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
    {
      Log.CardbackMgr.Print("CardBackManager.LoadCardBackByIndex() - failed to load CardBack {0}", (object) prefabName);
      return (CardBackManager.LoadCardBackData) null;
    }
    CardBack componentInChildren = gameObject.GetComponentInChildren<CardBack>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "CardBackManager.LoadCardBackByIndex() - cardback=null");
      return (CardBackManager.LoadCardBackData) null;
    }
    loadCardBackData.m_CardBack = componentInChildren;
    Actor component = loadCardBackData.m_GameObject.GetComponent<Actor>();
    this.SetCardBack(component.m_cardMesh, loadCardBackData.m_CardBack, loadCardBackData.m_Unlit);
    component.SetCardbackUpdateIgnore(true);
    loadCardBackData.m_CardBack.gameObject.transform.parent = loadCardBackData.m_GameObject.transform;
    return loadCardBackData;
  }

  public void AddNewCardBack(int cardBackID)
  {
    NetCache.NetCacheCardBacks netObject = NetCache.Get().GetNetObject<NetCache.NetCacheCardBacks>();
    if (netObject == null)
      UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager.AddNewCardBack({0}): trying to access NetCacheCardBacks before it's been loaded", (object) cardBackID));
    else
      netObject.CardBacks.Add(cardBackID);
  }

  public int GetDefaultCardBackID()
  {
    NetCache.NetCacheCardBacks netObject = NetCache.Get().GetNetObject<NetCache.NetCacheCardBacks>();
    if (netObject != null)
      return netObject.DefaultCardBack;
    UnityEngine.Debug.LogWarning((object) "CardBackManager.GetDefaultCardBackID(): trying to access NetCacheCardBacks before it's been loaded");
    return 0;
  }

  public string GetCardBackName(int cardBackId)
  {
    CardBackData cardBackData;
    if (this.m_cardBackData.TryGetValue(cardBackId, out cardBackData))
      return cardBackData.Name;
    return (string) null;
  }

  public int GetNumCardBacksOwned()
  {
    NetCache.NetCacheCardBacks netObject = NetCache.Get().GetNetObject<NetCache.NetCacheCardBacks>();
    if (netObject != null)
      return netObject.CardBacks.Count;
    UnityEngine.Debug.LogWarning((object) "CardBackManager.GetNumCardBacksOwned(): trying to access NetCacheCardBacks before it's been loaded");
    return 0;
  }

  public HashSet<int> GetCardBacksOwned()
  {
    NetCache.NetCacheCardBacks netObject = NetCache.Get().GetNetObject<NetCache.NetCacheCardBacks>();
    if (netObject != null)
      return netObject.CardBacks;
    UnityEngine.Debug.LogWarning((object) "CardBackManager.GetCardBacksOwned(): trying to access NetCacheCardBacks before it's been loaded");
    return (HashSet<int>) null;
  }

  public HashSet<int> GetCardBackIds(bool all = true)
  {
    HashSet<int> intSet = new HashSet<int>();
    HashSet<int> cardBacksOwned = this.GetCardBacksOwned();
    using (Map<int, CardBackData>.Enumerator enumerator = this.m_cardBackData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, CardBackData> current = enumerator.Current;
        if (current.Value.Enabled && (string.IsNullOrEmpty(this.m_searchText) || current.Value.Name.ToLower().Contains(this.m_searchText)) && (all || cardBacksOwned.Contains(current.Key)))
          intSet.Add(current.Key);
      }
    }
    return intSet;
  }

  public bool IsCardBackOwned(int cardBackID)
  {
    NetCache.NetCacheCardBacks netObject = NetCache.Get().GetNetObject<NetCache.NetCacheCardBacks>();
    if (netObject != null)
      return netObject.CardBacks.Contains(cardBackID);
    UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager.IsCardBackOwned({0}): trying to access NetCacheCardBacks before it's been loaded", (object) cardBackID));
    return false;
  }

  public bool IsCardBackEnabled(int cardBackID)
  {
    CardBackData cardBackData;
    if (!this.m_cardBackData.TryGetValue(cardBackID, out cardBackData))
      return false;
    return cardBackData.Enabled;
  }

  public List<CardBackData> GetEnabledCardBacks()
  {
    return this.m_cardBackData.Values.ToList<CardBackData>().FindAll((Predicate<CardBackData>) (obj => obj.Enabled));
  }

  public List<CardBackManager.OwnedCardBack> GetOrderedEnabledCardBacks(bool checkOwned)
  {
    List<CardBackData> enabledCardBacks = this.GetEnabledCardBacks();
    List<CardBackManager.OwnedCardBack> ownedCardBackList = new List<CardBackManager.OwnedCardBack>();
    using (List<CardBackData>.Enumerator enumerator = enabledCardBacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardBackData current = enumerator.Current;
        bool flag = this.IsCardBackOwned(current.ID);
        if (!checkOwned || flag)
        {
          CardBackDbfRecord record = GameDbf.CardBack.GetRecord(current.ID);
          long num = -1;
          if (record.Source == "season")
            num = record.Data1;
          if (string.IsNullOrEmpty(this.m_searchText) || current.Name.ToLower().Contains(this.m_searchText))
            ownedCardBackList.Add(new CardBackManager.OwnedCardBack()
            {
              m_cardBackId = current.ID,
              m_owned = flag,
              m_sortOrder = record.SortOrder,
              m_sortCategory = record.SortCategory,
              m_seasonId = num
            });
        }
      }
    }
    ownedCardBackList.Sort((Comparison<CardBackManager.OwnedCardBack>) ((lhs, rhs) =>
    {
      if (lhs.m_owned != rhs.m_owned)
        return lhs.m_owned ? -1 : 1;
      if (lhs.m_sortCategory != rhs.m_sortCategory)
        return lhs.m_sortCategory < rhs.m_sortCategory ? -1 : 1;
      if (lhs.m_sortOrder != rhs.m_sortOrder)
        return lhs.m_sortOrder < rhs.m_sortOrder ? -1 : 1;
      if (lhs.m_seasonId == rhs.m_seasonId)
        return Mathf.Clamp(lhs.m_cardBackId - rhs.m_cardBackId, -1, 1);
      return lhs.m_seasonId > rhs.m_seasonId ? -1 : 1;
    }));
    return ownedCardBackList;
  }

  public int GetNumEnabledCardBacks()
  {
    return this.GetEnabledCardBacks().Count;
  }

  public void SetCardBackTexture(Renderer renderer, int matIdx, bool friendlySide)
  {
    if (friendlySide && this.m_isFriendlyLoading)
      this.StartCoroutine(this.SetTextureWhenLoaded(renderer, matIdx, friendlySide));
    else if (!friendlySide && this.m_isOpponentLoading)
      this.StartCoroutine(this.SetTextureWhenLoaded(renderer, matIdx, friendlySide));
    else
      this.SetTexture(renderer, matIdx, friendlySide);
  }

  public void UpdateCardBack(Actor actor, CardBack cardBack)
  {
    if ((UnityEngine.Object) actor.gameObject == (UnityEngine.Object) null || (UnityEngine.Object) actor.m_cardMesh == (UnityEngine.Object) null || (UnityEngine.Object) cardBack == (UnityEngine.Object) null)
      return;
    this.SetCardBack(actor.m_cardMesh, cardBack);
  }

  public void UpdateCardBackWithInternalCardBack(Actor actor)
  {
    if ((UnityEngine.Object) actor.gameObject == (UnityEngine.Object) null || (UnityEngine.Object) actor.m_cardMesh == (UnityEngine.Object) null)
      return;
    CardBack componentInChildren = actor.gameObject.GetComponentInChildren<CardBack>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
      return;
    this.SetCardBack(actor.m_cardMesh, componentInChildren);
  }

  public void UpdateCardBack(GameObject go, bool friendlySide)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return;
    if (friendlySide && this.m_isFriendlyLoading)
      this.StartCoroutine(this.SetCardBackWhenLoaded(go, friendlySide));
    else if (!friendlySide && this.m_isOpponentLoading)
      this.StartCoroutine(this.SetCardBackWhenLoaded(go, friendlySide));
    else
      this.SetCardBack(go, friendlySide);
  }

  public void UpdateDeck(GameObject go, bool friendlySide)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return;
    if (friendlySide && this.m_isFriendlyLoading)
      this.StartCoroutine(this.SetDeckCardBackWhenLoaded(go, friendlySide));
    else if (!friendlySide && this.m_isOpponentLoading)
      this.StartCoroutine(this.SetDeckCardBackWhenLoaded(go, friendlySide));
    else
      this.SetDeckCardBack(go, friendlySide);
  }

  public void UpdateDragEffect(GameObject go, bool friendlySide)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return;
    if (friendlySide && this.m_isFriendlyLoading)
      this.StartCoroutine(this.SetDragEffectsWhenLoaded(go, friendlySide));
    else if (!friendlySide && this.m_isOpponentLoading)
      this.StartCoroutine(this.SetDragEffectsWhenLoaded(go, friendlySide));
    else
      this.SetDragEffects(go, friendlySide);
  }

  public bool IsActorFriendly(Actor actor)
  {
    if ((UnityEngine.Object) actor == (UnityEngine.Object) null)
    {
      Log.Kyle.Print("CardBack IsActorFriendly: actor is null!");
      return true;
    }
    Entity entity = actor.GetEntity();
    if (entity != null)
    {
      Player controller = entity.GetController();
      if (controller != null && controller.GetSide() == Player.Side.OPPOSING)
        return false;
    }
    return true;
  }

  public CardBack GetCardBack(Actor actor)
  {
    if (this.IsActorFriendly(actor))
      return this.m_FriendlyCardBack;
    return this.m_OpponentCardBack;
  }

  private void InitCardBacks()
  {
    this.LoadDefaultCardBack();
  }

  public void InitCardBackData()
  {
    List<CardBackData> cardBackDataList = new List<CardBackData>();
    using (List<CardBackDbfRecord>.Enumerator enumerator = GameDbf.CardBack.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardBackDbfRecord current = enumerator.Current;
        cardBackDataList.Add(new CardBackData(current.ID, EnumUtils.GetEnum<CardBackData.CardBackSource>(current.Source), current.Data1, (string) current.Name, current.Enabled, current.PrefabName));
      }
    }
    this.m_cardBackData = new Map<int, CardBackData>();
    using (List<CardBackData>.Enumerator enumerator = cardBackDataList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardBackData current = enumerator.Current;
        this.m_cardBackData[current.ID] = current;
      }
    }
    this.m_LoadedCardBacks = new Map<string, CardBack>();
  }

  [DebuggerHidden]
  private IEnumerator SetTextureWhenLoaded(Renderer renderer, int matIdx, bool friendlySide)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBackManager.\u003CSetTextureWhenLoaded\u003Ec__Iterator22()
    {
      friendlySide = friendlySide,
      renderer = renderer,
      matIdx = matIdx,
      \u003C\u0024\u003EfriendlySide = friendlySide,
      \u003C\u0024\u003Erenderer = renderer,
      \u003C\u0024\u003EmatIdx = matIdx,
      \u003C\u003Ef__this = this
    };
  }

  private void SetTexture(Renderer renderer, int matIdx, bool friendlySide)
  {
    if (friendlySide && (UnityEngine.Object) this.m_FriendlyCardBack == (UnityEngine.Object) null || !friendlySide && (UnityEngine.Object) this.m_OpponentCardBack == (UnityEngine.Object) null)
      return;
    Texture texture = !friendlySide ? (Texture) this.m_OpponentCardBack.m_CardBackTexture : (Texture) this.m_FriendlyCardBack.m_CardBackTexture;
    if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager SetTexture(): texture is null!   obj: {0}  friendly: {1}", (object) renderer.gameObject.name, (object) friendlySide));
    }
    else
    {
      if ((UnityEngine.Object) renderer == (UnityEngine.Object) null)
        return;
      renderer.materials[matIdx].mainTexture = texture;
    }
  }

  [DebuggerHidden]
  private IEnumerator SetCardBackWhenLoaded(GameObject go, bool friendlySide)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBackManager.\u003CSetCardBackWhenLoaded\u003Ec__Iterator23()
    {
      friendlySide = friendlySide,
      go = go,
      \u003C\u0024\u003EfriendlySide = friendlySide,
      \u003C\u0024\u003Ego = go,
      \u003C\u003Ef__this = this
    };
  }

  private void SetCardBack(GameObject go, bool friendlySide)
  {
    CardBack cardBack;
    if (friendlySide)
    {
      if ((UnityEngine.Object) this.m_FriendlyCardBack == (UnityEngine.Object) null)
      {
        cardBack = this.m_DefaultCardBack;
        UnityEngine.Debug.LogWarning((object) "CardBackManager.SetCardBack() m_FriendlyCardBack=null");
      }
      else
        cardBack = this.m_FriendlyCardBack;
    }
    else if ((UnityEngine.Object) this.m_OpponentCardBack == (UnityEngine.Object) null)
    {
      cardBack = this.m_DefaultCardBack;
      UnityEngine.Debug.LogWarning((object) "CardBackManager.SetCardBack() m_OpponentCardBack=null");
    }
    else
      cardBack = this.m_OpponentCardBack;
    if ((UnityEngine.Object) cardBack == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) "CardBackManager SetCardBack() cardBack=null");
    else
      this.SetCardBack(go, cardBack);
  }

  private void SetCardBack(GameObject go, CardBack cardBack)
  {
    this.SetCardBack(go, cardBack, false);
  }

  private void SetCardBack(GameObject go, CardBack cardBack, bool unlit)
  {
    if ((UnityEngine.Object) cardBack == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) "CardBackManager SetCardBack() cardback=null");
    else if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "CardBackManager SetCardBack() go=null");
    }
    else
    {
      Mesh cardBackMesh = cardBack.m_CardBackMesh;
      if ((UnityEngine.Object) cardBackMesh != (UnityEngine.Object) null)
      {
        MeshFilter component = go.GetComponent<MeshFilter>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          component.mesh = cardBackMesh;
      }
      else
        UnityEngine.Debug.LogWarning((object) "CardBackManager SetCardBack() mesh=null");
      float num1 = 0.0f;
      if (!unlit && SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY)
        num1 = 1f;
      Material cardBackMaterial = cardBack.m_CardBackMaterial;
      Material cardBackMaterial1 = cardBack.m_CardBackMaterial1;
      if ((UnityEngine.Object) cardBackMaterial != (UnityEngine.Object) null)
      {
        int length = 1;
        if ((UnityEngine.Object) cardBackMaterial1 != (UnityEngine.Object) null)
          length = 2;
        Material[] materialArray = new Material[length];
        materialArray[0] = UnityEngine.Object.Instantiate<Material>(cardBackMaterial);
        if ((UnityEngine.Object) cardBackMaterial1 != (UnityEngine.Object) null)
          materialArray[1] = UnityEngine.Object.Instantiate<Material>(cardBackMaterial1);
        float num2 = UnityEngine.Random.Range(0.0f, 1f);
        foreach (Material material in materialArray)
        {
          if (!((UnityEngine.Object) material == (UnityEngine.Object) null))
          {
            if (material.HasProperty("_Seed") && (double) material.GetFloat("_Seed") == 0.0)
              material.SetFloat("_Seed", num2);
            if (material.HasProperty("_LightingBlend"))
              material.SetFloat("_LightingBlend", num1);
          }
        }
        go.GetComponent<Renderer>().materials = materialArray;
      }
      else
        UnityEngine.Debug.LogWarning((object) "CardBackManager SetCardBack() material=null");
      Actor componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<Actor>(go);
      if (!((UnityEngine.Object) componentInThisOrParents != (UnityEngine.Object) null))
        return;
      componentInThisOrParents.UpdateMissingCardArt();
    }
  }

  [DebuggerHidden]
  private IEnumerator SetDragEffectsWhenLoaded(GameObject go, bool friendlySide)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBackManager.\u003CSetDragEffectsWhenLoaded\u003Ec__Iterator24()
    {
      friendlySide = friendlySide,
      go = go,
      \u003C\u0024\u003EfriendlySide = friendlySide,
      \u003C\u0024\u003Ego = go,
      \u003C\u003Ef__this = this
    };
  }

  private void SetDragEffects(GameObject go, bool friendlySide)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return;
    CardBackDragEffect componentInChildren = go.GetComponentInChildren<CardBackDragEffect>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null || friendlySide && (bool) ((UnityEngine.Object) this.m_FriendlyCardBack))
      return;
    CardBack cardBack = this.m_FriendlyCardBack;
    if (!friendlySide && (bool) ((UnityEngine.Object) this.m_OpponentCardBack))
      return;
    if (!friendlySide)
      cardBack = this.m_OpponentCardBack;
    if ((UnityEngine.Object) componentInChildren.m_EffectsRoot != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) componentInChildren.m_EffectsRoot);
    if ((UnityEngine.Object) cardBack == (UnityEngine.Object) null || (UnityEngine.Object) cardBack.m_DragEffect == (UnityEngine.Object) null)
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(cardBack.m_DragEffect);
    componentInChildren.m_EffectsRoot = gameObject;
    gameObject.transform.parent = componentInChildren.gameObject.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
  }

  [DebuggerHidden]
  private IEnumerator SetDeckCardBackWhenLoaded(GameObject go, bool friendlySide)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBackManager.\u003CSetDeckCardBackWhenLoaded\u003Ec__Iterator25()
    {
      friendlySide = friendlySide,
      go = go,
      \u003C\u0024\u003EfriendlySide = friendlySide,
      \u003C\u0024\u003Ego = go,
      \u003C\u003Ef__this = this
    };
  }

  private void SetDeckCardBack(GameObject go, bool friendlySide)
  {
    if (friendlySide)
    {
      if ((UnityEngine.Object) this.m_FriendlyCardBack == (UnityEngine.Object) null)
        return;
    }
    else if ((UnityEngine.Object) this.m_OpponentCardBack == (UnityEngine.Object) null)
      return;
    Texture cardBackTexture = (Texture) this.m_FriendlyCardBack.m_CardBackTexture;
    if (!friendlySide)
      cardBackTexture = (Texture) this.m_OpponentCardBack.m_CardBackTexture;
    if ((UnityEngine.Object) cardBackTexture == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager SetDeckCardBack(): texture is null!"));
    }
    else
    {
      foreach (Renderer componentsInChild in go.GetComponentsInChildren<Renderer>())
        componentsInChild.material.mainTexture = cardBackTexture;
    }
  }

  private void OnCheatOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    Log.Kyle.Print("Cheat Option Change Called");
    int key = Options.Get().GetInt(option, 0);
    if (!this.m_cardBackData.ContainsKey(key))
      return;
    bool friendlySide = true;
    if (option == Option.CARD_BACK2)
      friendlySide = false;
    this.LoadCardBack(FileUtils.GetAssetPath(this.m_cardBackData[key].PrefabName), friendlySide);
    this.UpdateAllCardBacks();
  }

  [DebuggerHidden]
  private IEnumerator RegisterForDefaultCardBackChangesWhenPossible()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBackManager.\u003CRegisterForDefaultCardBackChangesWhenPossible\u003Ec__Iterator26()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void NetCache_OnNetCacheCardBacksUpdated()
  {
    this.StartCoroutine(this.UpdateDefaultCardBackWhenReady());
  }

  [DebuggerHidden]
  private IEnumerator UpdateDefaultCardBackWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBackManager.\u003CUpdateDefaultCardBackWhenReady\u003Ec__Iterator27()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator UpdateAllCardBacksImpl()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBackManager.\u003CUpdateAllCardBacksImpl\u003Ec__Iterator28()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void LoadCardBack(string cardBackPath, bool friendlySide)
  {
    if (this.m_LoadedCardBacks.ContainsKey(cardBackPath))
    {
      if ((UnityEngine.Object) this.m_LoadedCardBacks[cardBackPath] == (UnityEngine.Object) null)
      {
        this.m_LoadedCardBacks.Remove(cardBackPath);
      }
      else
      {
        if (friendlySide)
        {
          this.m_FriendlyCardBack = this.m_LoadedCardBacks[cardBackPath];
          return;
        }
        this.m_OpponentCardBack = this.m_LoadedCardBacks[cardBackPath];
        return;
      }
    }
    if (friendlySide)
    {
      if (this.m_FriendlyCardBackName == cardBackPath)
        return;
      this.m_isFriendlyLoading = true;
      this.m_FriendlyCardBackName = cardBackPath;
      this.m_FriendlyCardBack = (CardBack) null;
    }
    else
    {
      if (this.m_OpponentCardBackName == cardBackPath)
        return;
      this.m_isOpponentLoading = true;
      this.m_OpponentCardBackName = cardBackPath;
      this.m_OpponentCardBack = (CardBack) null;
    }
    AssetLoader.Get().LoadCardBack(FileUtils.GameAssetPathToName(cardBackPath), new AssetLoader.GameObjectCallback(this.OnCardBackLoaded), (object) new CardBackManager.LoadCardBackData()
    {
      m_FriendlySide = friendlySide,
      m_Path = cardBackPath
    }, false);
  }

  private void OnCardBackLoaded(string name, GameObject go, object callbackData)
  {
    CardBackManager.LoadCardBackData loadCardBackData = callbackData as CardBackManager.LoadCardBackData;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager OnCardBackLoaded(): Failed to load CardBack: {0}", (object) name));
      this.FailedLoad(loadCardBackData.m_FriendlySide);
    }
    else
    {
      go.transform.parent = this.transform;
      go.transform.position = new Vector3(1000f, -1000f, -1000f);
      CardBack component = go.GetComponent<CardBack>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager OnCardBackLoaded(): Failed to find CardBack component: {0}", (object) loadCardBackData.m_Path));
      else if ((UnityEngine.Object) component.m_CardBackMesh == (UnityEngine.Object) null)
        UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager OnCardBackLoaded(): cardBack.m_CardBackMesh in null! - {0}", (object) loadCardBackData.m_Path));
      else if ((UnityEngine.Object) component.m_CardBackMaterial == (UnityEngine.Object) null)
        UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager OnCardBackLoaded(): cardBack.m_CardBackMaterial in null! - {0}", (object) loadCardBackData.m_Path));
      else if ((UnityEngine.Object) component.m_CardBackTexture == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager OnCardBackLoaded(): cardBack.m_CardBackTexture in null! - {0}", (object) loadCardBackData.m_Path));
      }
      else
      {
        this.m_LoadedCardBacks[loadCardBackData.m_Path] = component;
        if (loadCardBackData.m_FriendlySide)
        {
          this.m_isFriendlyLoading = false;
          if ((UnityEngine.Object) component == (UnityEngine.Object) null)
            UnityEngine.Debug.LogError((object) string.Format("CardBackManager OnCardBackLoaded(): Failed to find CardBack component for: {0}", (object) name));
          this.m_FriendlyCardBack = component;
        }
        else
        {
          this.m_isOpponentLoading = false;
          if ((UnityEngine.Object) component == (UnityEngine.Object) null)
            UnityEngine.Debug.LogError((object) string.Format("CardBackManager OnCardBackLoaded(): Failed to find CardBack component for: {0}", (object) name));
          this.m_OpponentCardBack = component;
        }
      }
    }
  }

  private void LoadDefaultCardBack()
  {
    CardBackManager.LoadCardBackData loadCardBackData = new CardBackManager.LoadCardBackData();
    loadCardBackData.m_FriendlySide = true;
    loadCardBackData.m_Path = this.m_cardBackData[0].PrefabName;
    AssetLoader.Get().LoadCardBack(FileUtils.GameAssetPathToName(loadCardBackData.m_Path), new AssetLoader.GameObjectCallback(this.OnDefaultCardBackLoaded), (object) loadCardBackData, false);
  }

  private void OnDefaultCardBackLoaded(string name, GameObject go, object callbackData)
  {
    CardBackManager.LoadCardBackData loadCardBackData = callbackData as CardBackManager.LoadCardBackData;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CardBackManager OnDefaultCardBackLoaded(): Failed to load CardBack: {0}", (object) name));
    }
    else
    {
      go.transform.parent = this.transform;
      go.transform.position = new Vector3(1000f, -1000f, -1000f);
      CardBack component = go.GetComponent<CardBack>();
      this.m_LoadedCardBacks[loadCardBackData.m_Path] = component;
      this.m_DefaultCardBack = component;
    }
  }

  private void FailedLoad(bool friendlySide)
  {
    if (friendlySide)
    {
      this.m_FriendlyCardBack = (CardBack) null;
      this.m_FriendlyCardBackName = string.Empty;
      this.m_isFriendlyLoading = false;
    }
    else
    {
      this.m_OpponentCardBack = (CardBack) null;
      this.m_OpponentCardBackName = string.Empty;
      this.m_isOpponentLoading = false;
    }
  }

  private void OnHiddenActorLoaded(string name, GameObject go, object userData)
  {
    CardBackManager.LoadCardBackData loadCardBackData = (CardBackManager.LoadCardBackData) userData;
    string prefabName = this.m_cardBackData[loadCardBackData.m_CardBackIndex].PrefabName;
    loadCardBackData.m_GameObject = go;
    AssetLoader.Get().LoadCardBack(FileUtils.GameAssetPathToName(prefabName), new AssetLoader.GameObjectCallback(this.OnHiddenActorCardBackLoaded), userData, false);
  }

  private void OnHiddenActorCardBackLoaded(string name, GameObject go, object userData)
  {
    CardBack componentInChildren = go.GetComponentInChildren<CardBack>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarningFormat("CardBackManager OnHiddenActorCardBackLoaded() name={0}, gameobject={1}, cardback=null", new object[2]
      {
        (object) name,
        (object) go.name
      });
    }
    else
    {
      CardBackManager.LoadCardBackData data = (CardBackManager.LoadCardBackData) userData;
      data.m_CardBack = componentInChildren;
      this.StartCoroutine(this.HiddenActorCardBackLoadedSetup(data));
    }
  }

  [DebuggerHidden]
  private IEnumerator HiddenActorCardBackLoadedSetup(CardBackManager.LoadCardBackData data)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBackManager.\u003CHiddenActorCardBackLoadedSetup\u003Ec__Iterator29()
    {
      data = data,
      \u003C\u0024\u003Edata = data,
      \u003C\u003Ef__this = this
    };
  }

  private int GetValidCardBackID(int cardBackID)
  {
    if (this.m_cardBackData.ContainsKey(cardBackID))
      return cardBackID;
    Log.CardbackMgr.Print("No cardback for {0}, use 0 instead", (object) cardBackID);
    return 0;
  }

  private void OnDefaultCardBackChanged(int defaultCardBackID, object userData)
  {
    int validCardBackId = this.GetValidCardBackID(defaultCardBackID);
    bool flag = false;
    if (GameMgr.Get() != null && GameMgr.Get().IsSpectator())
      flag = true;
    if (flag)
      return;
    this.LoadCardBack(FileUtils.GameAssetPathToName(this.m_cardBackData[validCardBackId].PrefabName), true);
    this.UpdateAllCardBacks();
  }

  private void OnSceneChangeResetDefaultCardBack(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (SceneMgr.Get().GetPrevMode() != SceneMgr.Mode.GAMEPLAY)
      return;
    this.LoadCardBack(FileUtils.GameAssetPathToName(this.m_cardBackData[this.GetDefaultCardBackID()].PrefabName), true);
  }

  public class LoadCardBackData
  {
    public int m_CardBackIndex;
    public GameObject m_GameObject;
    public CardBack m_CardBack;
    public CardBackManager.LoadCardBackData.LoadCardBackCallback m_Callback;
    public string m_Name;
    public string m_Path;
    public bool m_FriendlySide;
    public bool m_Unlit;

    public delegate void LoadCardBackCallback(CardBackManager.LoadCardBackData cardBackData);
  }

  public class OwnedCardBack
  {
    public long m_seasonId = -1;
    public int m_cardBackId;
    public bool m_owned;
    public int m_sortOrder;
    public int m_sortCategory;
  }
}
