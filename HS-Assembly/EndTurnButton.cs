﻿// Decompiled with JetBrains decompiler
// Type: EndTurnButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class EndTurnButton : MonoBehaviour
{
  public ActorStateMgr m_ActorStateMgr;
  public UberText m_MyTurnText;
  public UberText m_WaitingText;
  public GameObject m_GreenHighlight;
  public GameObject m_WhiteHighlight;
  public GameObject m_EndTurnButtonMesh;
  private static EndTurnButton s_instance;
  private bool m_inputBlocked;
  private bool m_pressed;
  private bool m_playedNmpSoundThisTurn;
  private bool m_mousedOver;

  private void Awake()
  {
    EndTurnButton.s_instance = this;
    this.m_MyTurnText.Text = GameStrings.Get("GAMEPLAY_END_TURN");
    this.m_WaitingText.Text = string.Empty;
    this.GetComponent<Collider>().enabled = false;
  }

  private void OnDestroy()
  {
    EndTurnButton.s_instance = (EndTurnButton) null;
  }

  private void Start()
  {
    this.StartCoroutine(this.WaitAFrameAndThenChangeState());
  }

  public static EndTurnButton Get()
  {
    return EndTurnButton.s_instance;
  }

  public GameObject GetButtonContainer()
  {
    return this.transform.FindChild("ButtonContainer").gameObject;
  }

  public void PlayPushDownAnimation()
  {
    if (this.m_inputBlocked || this.IsInWaitingState() || this.m_pressed)
      return;
    this.m_pressed = true;
    this.GetButtonContainer().GetComponent<Animation>().Play("ENDTURN_PRESSED_DOWN");
    SoundManager.Get().LoadAndPlay("FX_EndTurn_Down");
  }

  public void PlayButtonUpAnimation()
  {
    if (this.m_inputBlocked || this.IsInWaitingState() || !this.m_pressed)
      return;
    this.m_pressed = false;
    this.GetButtonContainer().GetComponent<Animation>().Play("ENDTURN_PRESSED_UP");
    SoundManager.Get().LoadAndPlay("FX_EndTurn_Up");
  }

  public bool IsInWaitingState()
  {
    switch (this.m_ActorStateMgr.GetActiveStateType())
    {
      case ActorStateType.ENDTURN_WAITING:
        return true;
      case ActorStateType.ENDTURN_NMP_2_WAITING:
        return true;
      case ActorStateType.ENDTURN_WAITING_TIMER:
        return true;
      default:
        return false;
    }
  }

  public bool IsInNMPState()
  {
    return this.m_ActorStateMgr.GetActiveStateType() == ActorStateType.ENDTURN_NO_MORE_PLAYS;
  }

  public bool IsInYouHavePlaysState()
  {
    return this.m_ActorStateMgr.GetActiveStateType() == ActorStateType.ENDTURN_YOUR_TURN;
  }

  public bool HasNoMorePlays()
  {
    Network.Options optionsPacket = GameState.Get().GetOptionsPacket();
    return optionsPacket != null && optionsPacket.List != null && optionsPacket.List.Count <= 1;
  }

  public bool IsInputBlocked()
  {
    return this.m_inputBlocked;
  }

  public void HandleMouseOver()
  {
    this.m_mousedOver = true;
    if (this.m_inputBlocked)
      return;
    this.PutInMouseOverState();
  }

  public void HandleMouseOut()
  {
    this.m_mousedOver = false;
    if (this.m_inputBlocked)
      return;
    if (this.m_pressed)
      this.PlayButtonUpAnimation();
    this.PutInMouseOffState();
  }

  private void PutInMouseOverState()
  {
    if (this.IsInNMPState())
    {
      this.m_WhiteHighlight.SetActive(false);
      this.m_GreenHighlight.SetActive(true);
      Hashtable args = iTween.Hash((object) "from", (object) this.m_GreenHighlight.GetComponent<Renderer>().material.GetFloat("_Intensity"), (object) "to", (object) 1.4f, (object) "time", (object) 0.15f, (object) "easetype", (object) iTween.EaseType.linear, (object) "onupdate", (object) "OnUpdateIntensityValue", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "ENDTURN_INTENSITY");
      iTween.StopByName(this.gameObject, "ENDTURN_INTENSITY");
      iTween.ValueTo(this.gameObject, args);
    }
    else if (this.IsInYouHavePlaysState())
    {
      this.m_WhiteHighlight.SetActive(true);
      this.m_GreenHighlight.SetActive(false);
    }
    else
    {
      this.m_WhiteHighlight.SetActive(false);
      this.m_GreenHighlight.SetActive(false);
    }
  }

  private void PutInMouseOffState()
  {
    this.m_WhiteHighlight.SetActive(false);
    if (this.IsInNMPState())
    {
      this.m_GreenHighlight.SetActive(true);
      Hashtable args = iTween.Hash((object) "from", (object) this.m_GreenHighlight.GetComponent<Renderer>().material.GetFloat("_Intensity"), (object) "to", (object) 1.1f, (object) "time", (object) 0.15f, (object) "easetype", (object) iTween.EaseType.linear, (object) "onupdate", (object) "OnUpdateIntensityValue", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "ENDTURN_INTENSITY");
      iTween.StopByName(this.gameObject, "ENDTURN_INTENSITY");
      iTween.ValueTo(this.gameObject, args);
    }
    else
      this.m_GreenHighlight.SetActive(false);
  }

  private void OnUpdateIntensityValue(float newValue)
  {
    this.m_GreenHighlight.GetComponent<Renderer>().material.SetFloat("_Intensity", newValue);
  }

  [DebuggerHidden]
  private IEnumerator WaitAFrameAndThenChangeState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EndTurnButton.\u003CWaitAFrameAndThenChangeState\u003Ec__IteratorAA() { \u003C\u003Ef__this = this };
  }

  private void HandleGameStart()
  {
    this.UpdateState();
    GameState gameState = GameState.Get();
    if (!gameState.IsPastBeginPhase() || !gameState.IsFriendlySidePlayerTurn())
      return;
    this.GetComponent<Collider>().enabled = true;
    GameState.Get().RegisterOptionsReceivedListener(new GameState.OptionsReceivedCallback(this.OnOptionsReceived));
  }

  private void SetButtonState(ActorStateType stateType)
  {
    if ((Object) this.m_ActorStateMgr == (Object) null)
    {
      UnityEngine.Debug.Log((object) "End Turn Button Actor State Manager is missing!");
    }
    else
    {
      if (this.m_ActorStateMgr.GetActiveStateType() == stateType || this.m_inputBlocked)
        return;
      this.m_ActorStateMgr.ChangeState(stateType);
      switch (stateType)
      {
        case ActorStateType.ENDTURN_YOUR_TURN:
        case ActorStateType.ENDTURN_WAITING_TIMER:
          this.m_inputBlocked = true;
          this.StartCoroutine(this.WaitUntilAnimationIsCompleteAndThenUnblockInput());
          break;
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitUntilAnimationIsCompleteAndThenUnblockInput()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EndTurnButton.\u003CWaitUntilAnimationIsCompleteAndThenUnblockInput\u003Ec__IteratorAB() { \u003C\u003Ef__this = this };
  }

  private void UpdateState()
  {
    if (!GameState.Get().IsFriendlySidePlayerTurn())
    {
      this.SetStateToWaiting();
    }
    else
    {
      if (GameState.Get().IsMulliganManagerActive() || GameState.Get().IsTurnStartManagerBlockingInput() || GameState.Get().GetResponseMode() == GameState.ResponseMode.NONE)
        return;
      this.SetStateToYourTurn();
    }
  }

  private void SetStateToYourTurn()
  {
    if ((Object) this.m_ActorStateMgr == (Object) null)
      return;
    if (this.HasNoMorePlays())
    {
      this.SetStateToNoMorePlays();
    }
    else
    {
      this.SetButtonState(ActorStateType.ENDTURN_YOUR_TURN);
      if (this.m_mousedOver)
        this.PutInMouseOverState();
      else
        this.PutInMouseOffState();
    }
  }

  private void SetStateToNoMorePlays()
  {
    if ((Object) this.m_ActorStateMgr == (Object) null)
      return;
    if (this.IsInWaitingState())
    {
      this.SetButtonState(ActorStateType.ENDTURN_YOUR_TURN);
    }
    else
    {
      this.SetButtonState(ActorStateType.ENDTURN_NO_MORE_PLAYS);
      if (this.m_mousedOver)
        this.PutInMouseOverState();
      else
        this.PutInMouseOffState();
    }
    if (this.m_playedNmpSoundThisTurn)
      return;
    this.m_playedNmpSoundThisTurn = true;
    this.StartCoroutine(this.PlayEndTurnSound());
  }

  private void SetStateToWaiting()
  {
    if ((Object) this.m_ActorStateMgr == (Object) null || this.IsInWaitingState() || GameState.Get().IsGameOver())
      return;
    if (this.IsInNMPState())
      this.SetButtonState(ActorStateType.ENDTURN_NMP_2_WAITING);
    else
      this.SetButtonState(ActorStateType.ENDTURN_WAITING);
    this.PutInMouseOffState();
  }

  [DebuggerHidden]
  private IEnumerator PlayEndTurnSound()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EndTurnButton.\u003CPlayEndTurnSound\u003Ec__IteratorAC() { \u003C\u003Ef__this = this };
  }

  private void OnCreateGame(GameState.CreateGamePhase phase, object userData)
  {
    if (phase != GameState.CreateGamePhase.CREATED)
      return;
    GameState.Get().UnregisterCreateGameListener(new GameState.CreateGameCallback(this.OnCreateGame));
    this.HandleGameStart();
  }

  public void OnMulliganEnded()
  {
    this.m_WaitingText.Text = GameStrings.Get("GAMEPLAY_ENEMY_TURN");
  }

  public void OnTurnStartManagerFinished()
  {
    PegCursor.Get().SetMode(PegCursor.Mode.STOPWAITING);
    this.m_playedNmpSoundThisTurn = false;
    this.SetStateToYourTurn();
    this.GetComponent<Collider>().enabled = true;
    GameState.Get().RegisterOptionsReceivedListener(new GameState.OptionsReceivedCallback(this.OnOptionsReceived));
  }

  public void OnEndTurnRequested()
  {
    PegCursor.Get().SetMode(PegCursor.Mode.WAITING);
    this.SetStateToWaiting();
    this.GetComponent<Collider>().enabled = false;
    GameState.Get().UnregisterOptionsReceivedListener(new GameState.OptionsReceivedCallback(this.OnOptionsReceived));
  }

  private void OnOptionsReceived(object userData)
  {
    this.UpdateState();
  }

  public void OnTurnTimerStart()
  {
    if (this.m_inputBlocked || !this.m_mousedOver)
      ;
  }

  public void OnTurnTimerEnded(bool isFriendlyPlayerTurnTimer)
  {
    if (!isFriendlyPlayerTurnTimer)
      return;
    this.SetButtonState(ActorStateType.ENDTURN_WAITING_TIMER);
  }
}
