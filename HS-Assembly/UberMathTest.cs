﻿// Decompiled with JetBrains decompiler
// Type: UberMathTest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UberMathTest : MonoBehaviour
{
  public float m_NoiseScale = 0.01f;
  public int m_Resolution = 256;
  public float m_GridSize = 1f;
  public float m_ParticleSize = 0.1f;
  public float m_TimeScale;
  private GameObject m_particleGameObject;
  private ParticleSystem m_particleSystem;
  private ParticleSystem.Particle[] m_particles;
  private float m_gridIncrement;
  private float m_time;

  private void Start()
  {
    this.m_particleGameObject = new GameObject("UberMathParticles");
    this.m_particleGameObject.transform.position = Vector3.zero;
    this.m_particleSystem = this.m_particleGameObject.AddComponent<ParticleSystem>();
    this.m_particleSystem.loop = false;
    this.m_particleSystem.emission.rate = new ParticleSystem.MinMaxCurve()
    {
      constantMax = 0.0f,
      constantMin = 0.0f
    };
    this.Simplex2DTest();
  }

  private void Update()
  {
    this.m_time = Time.timeSinceLevelLoad * this.m_TimeScale;
    this.m_gridIncrement = this.m_GridSize / (float) (this.m_Resolution - 1);
    this.Update2Dparticles();
  }

  private void Simplex2DTest()
  {
    this.CreateParticles(this.m_Resolution);
  }

  private void CreateParticles(int res)
  {
    this.m_particles = new ParticleSystem.Particle[res * res];
    int index1 = 0;
    for (int index2 = 0; index2 < res; ++index2)
    {
      for (int index3 = 0; index3 < res; ++index3)
      {
        Vector3 vector3 = new Vector3((float) index2 * this.m_gridIncrement, 0.0f, (float) index3 * this.m_gridIncrement);
        this.m_particles[index1].position = vector3;
        this.m_particles[index1].startColor = (Color32) Color.white;
        this.m_particles[index1].startSize = this.m_ParticleSize;
        ++index1;
      }
    }
    Debug.LogFormat("Particle count: {0}", (object) this.m_particles.Length);
    this.m_particleSystem.SetParticles(this.m_particles, this.m_particles.Length);
  }

  private void Update2Dparticles()
  {
    int index1 = 0;
    for (int index2 = 0; index2 < this.m_Resolution; ++index2)
    {
      for (int index3 = 0; index3 < this.m_Resolution; ++index3)
      {
        float num = UberMath.SimplexNoise((float) index2 * this.m_NoiseScale, this.m_time * this.m_NoiseScale, (float) index3 * this.m_NoiseScale);
        Vector3 position = this.m_particles[index1].position;
        position.x = (float) index2 * this.m_gridIncrement;
        position.y = num * 0.1f;
        position.z = (float) index3 * this.m_gridIncrement;
        this.m_particles[index1].position = position;
        Color white = Color.white;
        white.r = white.g = white.b = (float) (((double) num + 1.0) * 0.5);
        this.m_particles[index1].startColor = (Color32) white;
        ++index1;
      }
    }
    this.m_particleSystem.SetParticles(this.m_particles, this.m_particles.Length);
  }
}
