﻿// Decompiled with JetBrains decompiler
// Type: VictoryTwoScoop
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class VictoryTwoScoop : EndGameTwoScoop
{
  private const float GOD_RAY_ANGLE = 20f;
  private const float GOD_RAY_DURATION = 20f;
  private const float LAUREL_ROTATION = 2f;
  public GameObject m_godRays;
  public GameObject m_godRays2;
  public GameObject m_rightTrumpet;
  public GameObject m_rightBanner;
  public GameObject m_rightCloud;
  public GameObject m_rightLaurel;
  public GameObject m_leftTrumpet;
  public GameObject m_leftBanner;
  public GameObject m_leftCloud;
  public GameObject m_leftLaurel;
  public GameObject m_crown;

  protected override void ShowImpl()
  {
    this.m_heroActor.SetEntityDef(GameState.Get().GetFriendlySidePlayer().GetHero().GetEntityDef());
    this.m_heroActor.SetCardDef(GameState.Get().GetFriendlySidePlayer().GetHero().GetCardDef());
    this.m_heroActor.UpdateAllComponents();
    this.m_heroActor.TurnOffCollider();
    this.SetBannerLabel(GameState.Get().GetGameEntity().GetVictoryScreenBannerText());
    this.GetComponent<PlayMakerFSM>().SendEvent("Action");
    iTween.FadeTo(this.gameObject, 1f, 0.25f);
    this.gameObject.transform.localScale = new Vector3(EndGameTwoScoop.START_SCALE_VAL, EndGameTwoScoop.START_SCALE_VAL, EndGameTwoScoop.START_SCALE_VAL);
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) new Vector3(EndGameTwoScoop.END_SCALE_VAL, EndGameTwoScoop.END_SCALE_VAL, EndGameTwoScoop.END_SCALE_VAL), (object) "time", (object) 0.5f, (object) "oncomplete", (object) "PunchEndGameTwoScoop", (object) "oncompletetarget", (object) this.gameObject));
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) (this.gameObject.transform.position + new Vector3(0.005f, 0.005f, 0.005f)), (object) "time", (object) 1.5f, (object) "oncomplete", (object) "TokyoDriftTo", (object) "oncompletetarget", (object) this.gameObject));
    this.AnimateGodraysTo();
    this.AnimateCrownTo();
    this.StartCoroutine(this.AnimateAll());
  }

  protected override void ResetPositions()
  {
    this.gameObject.transform.localPosition = EndGameTwoScoop.START_POSITION;
    this.gameObject.transform.eulerAngles = new Vector3(0.0f, 180f, 0.0f);
    this.m_rightTrumpet.transform.localPosition = new Vector3(0.23f, -0.6f, 0.16f);
    this.m_rightTrumpet.transform.localScale = new Vector3(1f, 1f, 1f);
    this.m_leftTrumpet.transform.localPosition = new Vector3(-0.23f, -0.6f, 0.16f);
    this.m_leftTrumpet.transform.localScale = new Vector3(-1f, 1f, 1f);
    this.m_rightBanner.transform.localScale = new Vector3(1f, 1f, 0.08f);
    this.m_leftBanner.transform.localScale = new Vector3(1f, 1f, 0.08f);
    this.m_rightCloud.transform.localPosition = new Vector3(-0.2f, -0.8f, 0.26f);
    this.m_leftCloud.transform.localPosition = new Vector3(0.16f, -0.8f, 0.2f);
    this.m_godRays.transform.localEulerAngles = new Vector3(0.0f, 29f, 0.0f);
    this.m_godRays2.transform.localEulerAngles = new Vector3(0.0f, -29f, 0.0f);
    this.m_crown.transform.localPosition = new Vector3(-0.041f, -0.06f, -0.834f);
    this.m_rightLaurel.transform.localEulerAngles = new Vector3(0.0f, -90f, 0.0f);
    this.m_rightLaurel.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
    this.m_leftLaurel.transform.localEulerAngles = new Vector3(0.0f, 90f, 0.0f);
    this.m_leftLaurel.transform.localScale = new Vector3(-0.7f, 0.7f, 0.7f);
  }

  public override void StopAnimating()
  {
    this.StopCoroutine("AnimateAll");
    iTween.Stop(this.gameObject, true);
    this.StartCoroutine(this.ResetPositionsForGoldEvent());
  }

  [DebuggerHidden]
  private IEnumerator AnimateAll()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new VictoryTwoScoop.\u003CAnimateAll\u003Ec__Iterator9F() { \u003C\u003Ef__this = this };
  }

  private void TokyoDriftTo()
  {
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) (EndGameTwoScoop.START_POSITION + new Vector3(0.2f, 0.2f, 0.2f)), (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "TokyoDriftFro", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void TokyoDriftFro()
  {
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) EndGameTwoScoop.START_POSITION, (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "TokyoDriftTo", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void CloudTo()
  {
    iTween.MoveTo(this.m_rightCloud, iTween.Hash((object) "x", (object) -0.92f, (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "CloudFro", (object) "oncompletetarget", (object) this.gameObject));
    iTween.MoveTo(this.m_leftCloud, iTween.Hash((object) "x", (object) 0.82f, (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear));
  }

  private void CloudFro()
  {
    iTween.MoveTo(this.m_rightCloud, iTween.Hash((object) "x", (object) -1.227438f, (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "CloudTo", (object) "oncompletetarget", (object) this.gameObject));
    iTween.MoveTo(this.m_leftCloud, iTween.Hash((object) "x", (object) 1.053244f, (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear));
  }

  private void LaurelWaveTo()
  {
    iTween.RotateTo(this.m_rightLaurel, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, 0.0f, 0.0f), (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "LaurelWaveFro", (object) "oncompletetarget", (object) this.gameObject));
    iTween.RotateTo(this.m_leftLaurel, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, 0.0f, 0.0f), (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear));
  }

  private void LaurelWaveFro()
  {
    iTween.RotateTo(this.m_rightLaurel, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, 2f, 0.0f), (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "LaurelWaveTo", (object) "oncompletetarget", (object) this.gameObject));
    iTween.RotateTo(this.m_leftLaurel, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, -2f, 0.0f), (object) "time", (object) 10, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear));
  }

  private void AnimateCrownTo()
  {
    iTween.MoveTo(this.m_crown, iTween.Hash((object) "z", (object) -0.78f, (object) "time", (object) 5, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.easeInOutBack, (object) "oncomplete", (object) "AnimateCrownFro", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void AnimateCrownFro()
  {
    iTween.MoveTo(this.m_crown, iTween.Hash((object) "z", (object) -0.834f, (object) "time", (object) 5, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.easeInOutBack, (object) "oncomplete", (object) "AnimateCrownTo", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void AnimateGodraysTo()
  {
    iTween.RotateTo(this.m_godRays, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, -20f, 0.0f), (object) "time", (object) 20f, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "AnimateGodraysFro", (object) "oncompletetarget", (object) this.gameObject));
    iTween.RotateTo(this.m_godRays2, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, 20f, 0.0f), (object) "time", (object) 20f, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear));
  }

  private void AnimateGodraysFro()
  {
    iTween.RotateTo(this.m_godRays, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, 20f, 0.0f), (object) "time", (object) 20f, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "AnimateGodraysTo", (object) "oncompletetarget", (object) this.gameObject));
    iTween.RotateTo(this.m_godRays2, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, -20f, 0.0f), (object) "time", (object) 20f, (object) "isLocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear));
  }

  [DebuggerHidden]
  private IEnumerator ResetPositionsForGoldEvent()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new VictoryTwoScoop.\u003CResetPositionsForGoldEvent\u003Ec__IteratorA0() { \u003C\u003Ef__this = this };
  }
}
