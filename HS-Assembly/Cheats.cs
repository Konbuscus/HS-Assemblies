﻿// Decompiled with JetBrains decompiler
// Type: Cheats
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using MiniJSON;
using PegasusShared;
using PegasusUtil;
using SpectatorProto;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cheats
{
  private static readonly Map<KeyCode, ScenarioDbId> s_quickPlayKeyMap = new Map<KeyCode, ScenarioDbId>() { { KeyCode.F1, ScenarioDbId.PRACTICE_EXPERT_MAGE }, { KeyCode.F2, ScenarioDbId.PRACTICE_EXPERT_HUNTER }, { KeyCode.F3, ScenarioDbId.PRACTICE_EXPERT_WARRIOR }, { KeyCode.F4, ScenarioDbId.PRACTICE_EXPERT_SHAMAN }, { KeyCode.F5, ScenarioDbId.PRACTICE_EXPERT_DRUID }, { KeyCode.F6, ScenarioDbId.PRACTICE_EXPERT_PRIEST }, { KeyCode.F7, ScenarioDbId.PRACTICE_EXPERT_ROGUE }, { KeyCode.F8, ScenarioDbId.PRACTICE_EXPERT_PALADIN }, { KeyCode.F9, ScenarioDbId.PRACTICE_EXPERT_WARLOCK } };
  private static readonly Map<KeyCode, string> s_opponentHeroKeyMap = new Map<KeyCode, string>() { { KeyCode.F1, "HERO_08" }, { KeyCode.F2, "HERO_05" }, { KeyCode.F3, "HERO_01" }, { KeyCode.F4, "HERO_02" }, { KeyCode.F5, "HERO_06" }, { KeyCode.F6, "HERO_09" }, { KeyCode.F7, "HERO_03" }, { KeyCode.F8, "HERO_04" }, { KeyCode.F9, "HERO_07" } };
  private static bool s_hasSubscribedToPartyEvents = false;
  private Cheats.QuickLaunchState m_quickLaunchState = new Cheats.QuickLaunchState();
  public const string CONFIG_INSTANT_GAMEPLAY_KEY = "Cheats.InstantGameplay";
  public const string CONFIG_INSTANT_CHEAT_COMMANDS_KEY = "Cheats.InstantCheatCommands";
  public const char CONFIG_INSTANT_CHEAT_COMMANDS_DELIMITER = ',';
  private static Cheats s_instance;
  private string m_board;
  private bool m_loadingStoreChallengePrompt;
  private StoreChallengePrompt m_storeChallengePrompt;
  private bool m_isYourMindFree;
  private AlertPopup m_alert;
  private string[] m_lastUtilServerCmd;

  private static Logger PartyLogger
  {
    get
    {
      return Log.Henry;
    }
  }

  public static Cheats Get()
  {
    return Cheats.s_instance;
  }

  public static void Initialize()
  {
    Cheats.s_instance = new Cheats();
    Cheats.s_instance.InitializeImpl();
  }

  public void OnCollectionManagerReady()
  {
    ConfigFile configFile = new ConfigFile();
    if (!configFile.FullLoad("client.config") || !configFile.Get("Cheats.InstantGameplay", false))
      return;
    configFile.Set("Cheats.InstantGameplay", false);
    configFile.Save((string) null);
    this.m_quickLaunchState = new Cheats.QuickLaunchState();
    this.m_quickLaunchState.m_skipMulligan = true;
    this.LaunchQuickGame(260, GameType.GT_VS_AI, FormatType.FT_WILD, (CollectionDeck) null);
  }

  public void OnMulliganEnded()
  {
    ConfigFile configFile = new ConfigFile();
    if (!configFile.FullLoad("client.config"))
      return;
    string str1 = configFile.Get("Cheats.InstantCheatCommands", (string) null);
    if (string.IsNullOrEmpty(str1))
      return;
    configFile.Set("Cheats.InstantCheatCommands", (string) null);
    configFile.Save((string) null);
    string str2 = str1;
    char[] chArray = new char[1]{ ',' };
    foreach (string command in str2.Split(chArray))
      Network.SendDebugConsoleCommand(command);
  }

  public string GetBoard()
  {
    return this.m_board;
  }

  public bool IsYourMindFree()
  {
    return this.m_isYourMindFree;
  }

  public bool IsLaunchingQuickGame()
  {
    return this.m_quickLaunchState.m_launching;
  }

  public bool ShouldSkipMulligan()
  {
    if (Options.Get().GetBool(Option.SKIP_ALL_MULLIGANS))
      return true;
    return this.m_quickLaunchState.m_skipMulligan;
  }

  public bool QuickGameFlipHeroes()
  {
    return this.m_quickLaunchState.m_flipHeroes;
  }

  public bool QuickGameMirrorHeroes()
  {
    return this.m_quickLaunchState.m_mirrorHeroes;
  }

  public string QuickGameOpponentHeroCardId()
  {
    return this.m_quickLaunchState.m_opponentHeroCardId;
  }

  public bool HandleKeyboardInput()
  {
    return ApplicationMgr.IsInternal() && this.HandleQuickPlayInput();
  }

  private void InitializeImpl()
  {
    CheatMgr cheatMgr = CheatMgr.Get();
    if (ApplicationMgr.IsInternal())
    {
      cheatMgr.RegisterCheatHandler("error", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_error), "Make the client throw an arbitrary error.", "<warning | fatal | exception> <optional error message>", "warning This is an example warning message.");
      cheatMgr.RegisterCheatHandler("collectionfirstxp", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_collectionfirstxp), "Set the number of page and cover flips to zero", string.Empty, string.Empty);
      cheatMgr.RegisterCheatHandler("board", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_board), "Set which board will be loaded on the next game", "<BRM|STW|GVG>", "BRM");
      cheatMgr.RegisterCheatHandler("brode", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_brode), "Brode's personal cheat", string.Empty, string.Empty);
      cheatMgr.RegisterCheatHandler("resettips", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_resettips), "Resets Innkeeper tips for collection manager", string.Empty, string.Empty);
      cheatMgr.RegisterCheatHandler("questcompletepopup", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_questcompletepopup), "Shows the quest complete achievement screen", "<quest_id>", "58");
      cheatMgr.RegisterCheatHandler("questprogresspopup", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_questprogresspopup), "Pop up a quest progress toast", "<title> <description> <progress> <maxprogress>", "Hello World 3 10");
      cheatMgr.RegisterCheatHandler("questwelcome", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_questwelcome), "Open list of daily quests", "<fromLogin>", "true");
      cheatMgr.RegisterCheatHandler("newquestvisual", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_newquestvisual), "Shows a new quest tile, only usable while a quest popup is active", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("storepassword", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_storepassword), "Show store challenge popup", string.Empty, string.Empty);
      cheatMgr.RegisterCheatHandler("retire", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_retire), "Retires your draft deck", string.Empty, string.Empty);
      cheatMgr.RegisterCheatHandler("defaultcardback", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_defaultcardback), "Set your cardback as if through the options menu", "<cardback id>", (string) null);
      cheatMgr.RegisterCheatHandler("disconnect", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_disconnect), "Disconnects you from a game in progress (disconnects from game server only). If you want to disconnect from just battle.net, use 'disconnect bnet'.", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("restart", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_restart), "Restarts any non-PvP game.", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("seasondialog", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_seasondialog), "Open the season end dialog", "<season number> <ending rank> <is wild rank>", "20 7 false");
      cheatMgr.RegisterCheatHandler("playnullsound", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_playnullsound), "Tell SoundManager to play a null sound.", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("spectate", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_spectate), "Connects to a game server to spectate", "<ip_address> <port> <game_handle> <spectator_password> [gameType] [missionId]", (string) null);
      cheatMgr.RegisterCheatHandler("party", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_party), "Run a variety of party related commands", "[sub command] [subcommand args]", "list");
      cheatMgr.RegisterCheatHandler("cheat", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_cheat), "Send a cheat command to the server", "<command> <arguments>", (string) null);
      cheatMgr.RegisterCheatAlias("cheat", "c");
      cheatMgr.RegisterCheatHandler("autohand", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_autohand), "Set whether PhoneUI automatically hides your hand after playing a card", "<true/false>", "true");
      cheatMgr.RegisterCheatHandler("fixedrewardcomplete", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_fixedrewardcomplete), "Shows the visual for a fixed reward", "<fixed_reward_map_id>", (string) null);
      cheatMgr.RegisterCheatHandler("iks", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_iks), "Open InnKeepersSpecial with a custom url", "<url>", (string) null);
      cheatMgr.RegisterCheatHandler("adventureChallengeUnlock", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_adventureChallengeUnlock), "Show adventure challenge unlock", "<wing number>", (string) null);
      cheatMgr.RegisterCheatHandler("quote", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_quote), string.Empty, "<character> <line> [sound]", "Innkeeper VO_INNKEEPER_FORGE_COMPLETE_22 VO_INNKEEPER_ARENA_COMPLETE");
      cheatMgr.RegisterCheatHandler("demotext", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_demotext), string.Empty, "<line>", "HelloWorld!");
      cheatMgr.RegisterCheatHandler("popuptext", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_popuptext), string.Empty, "<line>", "HelloWorld!");
      cheatMgr.RegisterCheatHandler("favoritehero", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_favoritehero), "Change your favorite hero for a class (only works from CollectionManager)", "<class_id> <hero_card_id> <hero_premium>", (string) null);
      cheatMgr.RegisterCheatHandler("rewardboxes", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_rewardboxes), "Open the reward box screen with example rewards", "<card|cardback|gold|dust|random> <num_boxes>", string.Empty);
      cheatMgr.RegisterCheatHandler("rankchange", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_rankchange), "Open the rankchange twoscoop", "<start_rank> <end_rank> [start_stars] [end_stars] [chest|winstreak]", "6 5 chest");
      cheatMgr.RegisterCheatHandler("easyrank", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_easyrank), "Easier cheat command to set your rank on the util server", "<rank>", "16");
      cheatMgr.RegisterCheatHandler("timescale", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_timescale), "Cheat to change the timescale", "<timescale>", "0.5");
      cheatMgr.RegisterCheatHandler("reset", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_reset), "Reset the client", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("onlygold", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_onlygold), "In collection manager, do you want to see gold, nogold, or both?", "<command name>", string.Empty);
      cheatMgr.RegisterCheatHandler("help", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_help), "Get help for a specific command or list of commands", "<command name>", string.Empty);
      cheatMgr.RegisterCheatHandler("example", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_example), "Run an example of this command if one exists", "<command name>", (string) null);
      cheatMgr.RegisterCheatHandler("tb", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_tavernbrawl), "Run a variety of Tavern Brawl related commands", "[subcommand] [subcommand args]", "view");
      cheatMgr.RegisterCheatHandler("util", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_utilservercmd), "Run a cheat on the UTIL server you're connected to.", "[subcommand] [subcommand args]", "help");
      cheatMgr.RegisterCheatHandler("game", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_gameservercmd), "[NYI] Run a cheat on the GAME server you're connected to.", "[subcommand] [subcommand args]", "help");
      Network.Get().RegisterNetHandler((object) DebugCommandResponse.PacketID.ID, new Network.NetHandler(this.OnProcessCheat_utilservercmd_OnResponse), (Network.TimeoutHandler) null);
      cheatMgr.RegisterCheatHandler("scenario", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_scenario), "Launch a scenario.", "<scenario_id> [<game_type_id>] [<deck_name>|<deck_id>]", (string) null);
      cheatMgr.RegisterCheatAlias("scenario", "mission");
      cheatMgr.RegisterCheatHandler("exportcards", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_exportcards), "Export images of cards", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("freeyourmind", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_freeyourmind), "And the rest will follow", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("reloadgamestrings", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_reloadgamestrings), "Reload all game strings from GLUE/GLOBAL/etc.", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("attn", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_userattentionmanager), "Prints out what UserAttentionBlockers, if any, are currently active.", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("banner", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_banner), "Shows the specified wooden banner (supply a banner_id). If none is supplied, it'll show the latest known banner. Use 'banner list' to view all known banners.", "<banner_id> | list", "33");
      cheatMgr.RegisterCheatHandler("raf", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_raf), "Run a RAF UI related commands", "[subcommand]", "showprogress");
      cheatMgr.RegisterCheatHandler("returningplayer", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_returningplayer), "Set the Returning Player progress", "<0|1|2|3>", "1");
      cheatMgr.RegisterCheatHandler("innkeeperdeck", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_innkeeperdeck), "Generate an Innkeeper's Deck, as part of the Returning Player flow.", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("lowmemorywarning", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_lowmemorywarning), "Simulate a low memory warning from mobile.", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("auto_exportgamestate", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_autoexportgamestate), "Save JSON file serializing some of GameState", (string) null, (string) null);
      cheatMgr.RegisterCheatHandler("notice", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_notice), "Show a notice", (string) null, (string) null);
    }
    cheatMgr.RegisterCheatHandler("has", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_HasOption), "Query whether a Game Option exists.", (string) null, (string) null);
    cheatMgr.RegisterCheatHandler("get", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_GetOption), "Get the value of a Game Option.", (string) null, (string) null);
    cheatMgr.RegisterCheatHandler("set", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_SetOption), "Set the value of a Game Option.", (string) null, (string) null);
    cheatMgr.RegisterCheatHandler("getvar", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_GetVar), "Get the value of a client.config var.", (string) null, (string) null);
    cheatMgr.RegisterCheatHandler("setvar", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_SetVar), "Set the value of a client.config var.", (string) null, (string) null);
    cheatMgr.RegisterCheatHandler("nav", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_navigation), "Debug Navigation.GoBack", (string) null, (string) null);
    cheatMgr.RegisterCheatHandler("delete", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_DeleteOption), "Delete a Game Option; the absence of option may trigger default behavior", (string) null, (string) null);
    cheatMgr.RegisterCheatAlias("delete", "del");
    cheatMgr.RegisterCheatHandler("warning", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_warning), "Show a warning message", "<message>", "Test You're a cheater and you've been warned!");
    cheatMgr.RegisterCheatHandler("fatal", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_fatal), "Brings up the Fatal Error screen", "<error to display>", "Hearthstone cheated and failed!");
    cheatMgr.RegisterCheatHandler("exit", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_exit), "Exit the application", string.Empty, string.Empty);
    cheatMgr.RegisterCheatAlias("exit", "quit");
    cheatMgr.RegisterCheatHandler("log", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_log), (string) null, (string) null, (string) null);
    cheatMgr.RegisterCheatHandler("autodraft", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_autodraft), "Sets Arena autodraft on/off.", "<on | off>", "on");
    cheatMgr.RegisterCheatHandler("alert", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_alert), "Show a popup alert", "header=<string> text=<string> icon=<bool> response=<ok|confirm|cancel|confirm_cancel> oktext=<string> confirmtext=<string>", "header=header text=body text icon=true response=confirm");
    cheatMgr.RegisterCheatAlias("alert", "popup", "dialog");
  }

  private void ParseErrorText(string[] args, string rawArgs, out string header, out string message)
  {
    header = args.Length != 0 ? args[0] : "[PH] Header";
    if (args.Length <= 1)
    {
      message = "[PH] Message";
    }
    else
    {
      int startIndex = 0;
      bool flag = false;
      for (int index = 0; index < rawArgs.Length; ++index)
      {
        if (char.IsWhiteSpace(rawArgs[index]))
        {
          if (flag)
          {
            startIndex = index;
            break;
          }
        }
        else
          flag = true;
      }
      message = rawArgs.Substring(startIndex).Trim();
    }
  }

  private AlertPopup.PopupInfo GenerateAlertInfo(string rawArgs)
  {
    Map<string, string> alertArgs = this.ParseAlertArgs(rawArgs);
    AlertPopup.PopupInfo popupInfo = new AlertPopup.PopupInfo();
    popupInfo.m_showAlertIcon = false;
    popupInfo.m_headerText = "Header";
    popupInfo.m_text = "Message";
    popupInfo.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
    popupInfo.m_okText = "OK";
    popupInfo.m_confirmText = "Confirm";
    popupInfo.m_cancelText = "Cancel";
    using (Map<string, string>.Enumerator enumerator = alertArgs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        string key = current.Key;
        string str1 = current.Value;
        if (key.Equals("header"))
          popupInfo.m_headerText = str1;
        else if (key.Equals("text"))
          popupInfo.m_text = str1;
        else if (key.Equals("response"))
        {
          string lowerInvariant = str1.ToLowerInvariant();
          if (lowerInvariant.Equals("ok"))
            popupInfo.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
          else if (lowerInvariant.Equals("confirm"))
            popupInfo.m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM;
          else if (lowerInvariant.Equals("cancel"))
            popupInfo.m_responseDisplay = AlertPopup.ResponseDisplay.CANCEL;
          else if (lowerInvariant.Equals("confirm_cancel") || lowerInvariant.Equals("cancel_confirm"))
            popupInfo.m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL;
        }
        else if (key.Equals("icon"))
          popupInfo.m_showAlertIcon = GeneralUtils.ForceBool(str1);
        else if (key.Equals("oktext"))
          popupInfo.m_okText = str1;
        else if (key.Equals("confirmtext"))
          popupInfo.m_confirmText = str1;
        else if (key.Equals("canceltext"))
          popupInfo.m_cancelText = str1;
        else if (key.Equals("offset"))
        {
          string[] strArray = str1.Split();
          Vector3 vector3 = new Vector3();
          if (strArray.Length % 2 == 0)
          {
            int index = 0;
            while (index < strArray.Length)
            {
              string lowerInvariant = strArray[index].ToLowerInvariant();
              string str2 = strArray[index + 1];
              if (lowerInvariant.Equals("x"))
                vector3.x = GeneralUtils.ForceFloat(str2);
              else if (lowerInvariant.Equals("y"))
                vector3.y = GeneralUtils.ForceFloat(str2);
              else if (lowerInvariant.Equals("z"))
                vector3.z = GeneralUtils.ForceFloat(str2);
              index += 2;
            }
          }
          popupInfo.m_offset = vector3;
        }
        else if (key.Equals("padding"))
          popupInfo.m_padding = GeneralUtils.ForceFloat(str1);
        else if (key.Equals("align"))
        {
          string str2 = str1;
          char[] chArray = new char[1]{ '|' };
          foreach (string str3 in str2.Split(chArray))
          {
            string lower = str3.ToLower();
            if (lower != null)
            {
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapBA == null)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.\u003C\u003Ef__switch\u0024mapBA = new Dictionary<string, int>(6)
                {
                  {
                    "left",
                    0
                  },
                  {
                    "center",
                    1
                  },
                  {
                    "right",
                    2
                  },
                  {
                    "top",
                    3
                  },
                  {
                    "middle",
                    4
                  },
                  {
                    "bottom",
                    5
                  }
                };
              }
              int num;
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapBA.TryGetValue(lower, out num))
              {
                switch (num)
                {
                  case 0:
                    popupInfo.m_alertTextAlignment = UberText.AlignmentOptions.Left;
                    continue;
                  case 1:
                    popupInfo.m_alertTextAlignment = UberText.AlignmentOptions.Center;
                    continue;
                  case 2:
                    popupInfo.m_alertTextAlignment = UberText.AlignmentOptions.Right;
                    continue;
                  case 3:
                    popupInfo.m_alertTextAlignmentAnchor = UberText.AnchorOptions.Upper;
                    continue;
                  case 4:
                    popupInfo.m_alertTextAlignmentAnchor = UberText.AnchorOptions.Middle;
                    continue;
                  case 5:
                    popupInfo.m_alertTextAlignmentAnchor = UberText.AnchorOptions.Lower;
                    continue;
                  default:
                    continue;
                }
              }
            }
          }
        }
      }
    }
    return popupInfo;
  }

  private Map<string, string> ParseAlertArgs(string rawArgs)
  {
    Map<string, string> map = new Map<string, string>();
    int startIndex1 = -1;
    string index1 = (string) null;
    for (int index2 = 0; index2 < rawArgs.Length; ++index2)
    {
      if ((int) rawArgs[index2] == 61)
      {
        int startIndex2 = -1;
        for (int index3 = index2 - 1; index3 >= 0; --index3)
        {
          char rawArg1 = rawArgs[index3];
          char rawArg2 = rawArgs[index3 + 1];
          if (!char.IsWhiteSpace(rawArg1))
            startIndex2 = index3;
          if (char.IsWhiteSpace(rawArg1) && !char.IsWhiteSpace(rawArg2))
            break;
        }
        if (startIndex2 >= 0)
        {
          int num = startIndex2 - 2;
          if (index1 != null)
            map[index1] = rawArgs.Substring(startIndex1, num - startIndex1 + 1);
          startIndex1 = index2 + 1;
          index1 = rawArgs.Substring(startIndex2, index2 - startIndex2).Trim().ToLowerInvariant().Replace("\\n", "\n");
        }
      }
    }
    int num1 = rawArgs.Length - 1;
    if (index1 != null)
      map[index1] = rawArgs.Substring(startIndex1, num1 - startIndex1 + 1).Replace("\\n", "\n");
    return map;
  }

  private bool OnAlertProcessed(DialogBase dialog, object userData)
  {
    this.m_alert = (AlertPopup) dialog;
    return true;
  }

  private void HideAlert()
  {
    if (!((UnityEngine.Object) this.m_alert != (UnityEngine.Object) null))
      return;
    this.m_alert.Hide();
    this.m_alert = (AlertPopup) null;
  }

  private bool HandleQuickPlayInput()
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null || !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
      return false;
    if (Input.GetKeyDown(KeyCode.F12))
    {
      this.PrintQuickPlayLegend();
      return false;
    }
    if (this.GetQuickLaunchAvailability() != Cheats.QuickLaunchAvailability.OK)
      return false;
    ScenarioDbId scenarioDbId1 = ScenarioDbId.INVALID;
    string str = (string) null;
    using (Map<KeyCode, ScenarioDbId>.Enumerator enumerator = Cheats.s_quickPlayKeyMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<KeyCode, ScenarioDbId> current = enumerator.Current;
        KeyCode key = current.Key;
        ScenarioDbId scenarioDbId2 = current.Value;
        if (Input.GetKeyDown(key))
        {
          scenarioDbId1 = scenarioDbId2;
          str = Cheats.s_opponentHeroKeyMap[key];
          break;
        }
      }
    }
    if (scenarioDbId1 == ScenarioDbId.INVALID)
      return false;
    this.m_quickLaunchState.m_mirrorHeroes = false;
    this.m_quickLaunchState.m_flipHeroes = false;
    this.m_quickLaunchState.m_skipMulligan = true;
    this.m_quickLaunchState.m_opponentHeroCardId = str;
    if ((Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.LeftAlt)) && (Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl)))
    {
      this.m_quickLaunchState.m_mirrorHeroes = true;
      this.m_quickLaunchState.m_skipMulligan = false;
      this.m_quickLaunchState.m_flipHeroes = false;
    }
    else if (Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl))
    {
      this.m_quickLaunchState.m_flipHeroes = false;
      this.m_quickLaunchState.m_skipMulligan = false;
      this.m_quickLaunchState.m_mirrorHeroes = false;
    }
    else if (Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.LeftAlt))
    {
      this.m_quickLaunchState.m_flipHeroes = true;
      this.m_quickLaunchState.m_skipMulligan = false;
      this.m_quickLaunchState.m_mirrorHeroes = false;
    }
    this.LaunchQuickGame((int) scenarioDbId1, GameType.GT_VS_AI, FormatType.FT_WILD, (CollectionDeck) null);
    return true;
  }

  private void PrintQuickPlayLegend()
  {
    string message = string.Format("F1: {0}\nF2: {1}\nF3: {2}\nF4: {3}\nF5: {4}\nF6: {5}\nF7: {6}\nF8: {7}\nF9: {8}\n(CTRL and ALT will Show mulligan)\nSHIFT + CTRL = Hero on players side\nSHIFT + ALT = Hero on opponent side\nSHIFT + ALT + CTRL = Hero on both sides", (object) this.GetQuickPlayMissionName(KeyCode.F1), (object) this.GetQuickPlayMissionName(KeyCode.F2), (object) this.GetQuickPlayMissionName(KeyCode.F3), (object) this.GetQuickPlayMissionName(KeyCode.F4), (object) this.GetQuickPlayMissionName(KeyCode.F5), (object) this.GetQuickPlayMissionName(KeyCode.F6), (object) this.GetQuickPlayMissionName(KeyCode.F7), (object) this.GetQuickPlayMissionName(KeyCode.F8), (object) this.GetQuickPlayMissionName(KeyCode.F9));
    if ((UnityEngine.Object) UIStatus.Get() != (UnityEngine.Object) null)
      UIStatus.Get().AddInfo(message);
    UnityEngine.Debug.Log((object) string.Format("F1: {0}  F2: {1}  F3: {2}  F4: {3}  F5: {4}  F6: {5}  F7: {6}  F8: {7}  F9: {8}\n(CTRL and ALT will Show mulligan) -- SHIFT + CTRL = Hero on players side -- SHIFT + ALT = Hero on opponent side -- SHIFT + ALT + CTRL = Hero on both sides", (object) this.GetQuickPlayMissionShortName(KeyCode.F1), (object) this.GetQuickPlayMissionShortName(KeyCode.F2), (object) this.GetQuickPlayMissionShortName(KeyCode.F3), (object) this.GetQuickPlayMissionShortName(KeyCode.F4), (object) this.GetQuickPlayMissionShortName(KeyCode.F5), (object) this.GetQuickPlayMissionShortName(KeyCode.F6), (object) this.GetQuickPlayMissionShortName(KeyCode.F7), (object) this.GetQuickPlayMissionShortName(KeyCode.F8), (object) this.GetQuickPlayMissionShortName(KeyCode.F9)));
  }

  private string GetQuickPlayMissionName(KeyCode keyCode)
  {
    return this.GetQuickPlayMissionName((int) Cheats.s_quickPlayKeyMap[keyCode]);
  }

  private string GetQuickPlayMissionShortName(KeyCode keyCode)
  {
    return this.GetQuickPlayMissionShortName((int) Cheats.s_quickPlayKeyMap[keyCode]);
  }

  private string GetQuickPlayMissionName(int missionId)
  {
    return this.GetQuickPlayMissionNameImpl(missionId, "NAME");
  }

  private string GetQuickPlayMissionShortName(int missionId)
  {
    return this.GetQuickPlayMissionNameImpl(missionId, "SHORT_NAME");
  }

  private string GetQuickPlayMissionNameImpl(int missionId, string columnName)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record != null)
    {
      DbfLocValue var = (DbfLocValue) record.GetVar(columnName);
      if (var != null)
        return var.GetString(true);
    }
    string str = missionId.ToString();
    try
    {
      str = ((ScenarioDbId) missionId).ToString();
    }
    catch (Exception ex)
    {
    }
    return str;
  }

  private Cheats.QuickLaunchAvailability GetQuickLaunchAvailability()
  {
    if (this.m_quickLaunchState.m_launching || SceneMgr.Get().IsInGame())
      return Cheats.QuickLaunchAvailability.ACTIVE_GAME;
    if (GameMgr.Get().IsFindingGame())
      return Cheats.QuickLaunchAvailability.FINDING_GAME;
    if (SceneMgr.Get().GetNextMode() != SceneMgr.Mode.INVALID || !SceneMgr.Get().IsSceneLoaded())
      return Cheats.QuickLaunchAvailability.SCENE_TRANSITION;
    if (LoadingScreen.Get().IsTransitioning())
      return Cheats.QuickLaunchAvailability.ACTIVE_GAME;
    return CollectionManager.Get() == null || !CollectionManager.Get().IsFullyLoaded() ? Cheats.QuickLaunchAvailability.COLLECTION_NOT_READY : Cheats.QuickLaunchAvailability.OK;
  }

  private void LaunchQuickGame(int missionId, GameType gameType = GameType.GT_VS_AI, FormatType formatType = FormatType.FT_WILD, CollectionDeck deck = null)
  {
    this.m_quickLaunchState.m_launching = true;
    long id;
    string str;
    if (deck == null)
    {
      id = Options.Get().GetLong(Option.LAST_CUSTOM_DECK_CHOSEN);
      deck = CollectionManager.Get().GetDeck(id);
      if (deck == null)
      {
        TAG_CLASS tagClass = TAG_CLASS.MAGE;
        id = CollectionManager.Get().GetPreconDeck(tagClass).ID;
        str = string.Format("Precon {0}", (object) GameStrings.GetClassName(tagClass));
      }
      else
        str = deck.Name;
    }
    else
    {
      id = deck.ID;
      str = deck.Name;
    }
    UIStatus.Get().AddInfo(string.Format("Launching {0}\nDeck: {1}", (object) this.GetQuickPlayMissionName(missionId), (object) str));
    Time.timeScale = SceneDebugger.Get().m_MaxTimeScale;
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    GameMgr.Get().SetPendingAutoConcede(true);
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    GameMgr.Get().FindGame(gameType, formatType, missionId, id, 0L);
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (mode == SceneMgr.Mode.GAMEPLAY)
      this.HideAlert();
    if (SceneMgr.Get().GetPrevMode() != SceneMgr.Mode.GAMEPLAY || mode == SceneMgr.Mode.GAMEPLAY)
      return;
    SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    this.m_quickLaunchState = new Cheats.QuickLaunchState();
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_CANCELED:
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
      case FindGameState.SERVER_GAME_CANCELED:
        GameMgr.Get().UnregisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
        this.m_quickLaunchState = new Cheats.QuickLaunchState();
        break;
    }
    return false;
  }

  private JsonList GetCardlistJson(List<Card> list)
  {
    JsonList jsonList = new JsonList();
    for (int index = 0; index < list.Count; ++index)
    {
      JsonNode cardJson = this.GetCardJson(list[index].GetEntity());
      jsonList.Add((object) cardJson);
    }
    return jsonList;
  }

  private JsonNode GetCardJson(Entity card)
  {
    if (card == null)
      return (JsonNode) null;
    JsonNode jsonNode1 = new JsonNode();
    jsonNode1["cardName"] = (object) card.GetName();
    jsonNode1["cardID"] = (object) card.GetCardId();
    jsonNode1["entityID"] = (object) (long) card.GetEntityId();
    JsonList jsonList1 = new JsonList();
    if (card.GetTags() != null)
    {
      using (Map<int, int>.Enumerator enumerator = card.GetTags().GetMap().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, int> current = enumerator.Current;
          JsonNode jsonNode2 = new JsonNode();
          string index = Enum.GetName(typeof (GAME_TAG), (object) current.Key) ?? "NOTAG_" + current.Key.ToString();
          jsonNode2[index] = (object) (long) current.Value;
          jsonList1.Add((object) jsonNode2);
        }
      }
      jsonNode1["tags"] = (object) jsonList1;
    }
    JsonList jsonList2 = new JsonList();
    List<Entity> enchantments = card.GetEnchantments();
    for (int index = 0; index < enchantments.Count<Entity>(); ++index)
    {
      JsonNode cardJson = this.GetCardJson(enchantments[index]);
      jsonList2.Add((object) cardJson);
    }
    jsonNode1["enchantments"] = (object) jsonList2;
    return jsonNode1;
  }

  private bool OnProcessCheat_error(string func, string[] args, string rawArgs)
  {
    bool flag1 = args.Length > 0 && (args[0] == "ex" || "except".Equals(args[0], StringComparison.InvariantCultureIgnoreCase) || "exception".Equals(args[0], StringComparison.InvariantCultureIgnoreCase));
    bool flag2 = args.Length > 0 && (args[0] == "f" || "fatal".Equals(args[0], StringComparison.InvariantCultureIgnoreCase));
    string message = args.Length > 1 ? string.Join(" ", ((IEnumerable<string>) args).Skip<string>(1).ToArray<string>()) : (string) null;
    if (flag1)
    {
      if (message == null)
        message = "This is a simulated Exception.";
      throw new Exception(message);
    }
    if (flag2)
    {
      if (message == null)
        message = "This is a simulated Fatal Error.";
      Error.AddFatal(message);
    }
    else
    {
      if (message == null)
        message = "This is a simulated Warning message.";
      Error.AddWarning("Warning", message);
    }
    return true;
  }

  private bool OnProcessCheat_HasOption(string func, string[] args, string rawArgs)
  {
    string str = args[0];
    Option option;
    try
    {
      option = EnumUtils.GetEnum<Option>(str, StringComparison.OrdinalIgnoreCase);
    }
    catch (ArgumentException ex)
    {
      return false;
    }
    string message = string.Format("HasOption: {0} = {1}", (object) EnumUtils.GetString<Option>(option), (object) Options.Get().HasOption(option));
    UnityEngine.Debug.Log((object) message);
    UIStatus.Get().AddInfo(message);
    return true;
  }

  private bool OnProcessCheat_GetOption(string func, string[] args, string rawArgs)
  {
    string str = args[0];
    Option option;
    try
    {
      option = EnumUtils.GetEnum<Option>(str, StringComparison.OrdinalIgnoreCase);
    }
    catch (ArgumentException ex)
    {
      return false;
    }
    string message = string.Format("GetOption: {0} = {1}", (object) EnumUtils.GetString<Option>(option), Options.Get().GetOption(option));
    UnityEngine.Debug.Log((object) message);
    UIStatus.Get().AddInfo(message);
    return true;
  }

  private bool OnProcessCheat_SetOption(string func, string[] args, string rawArgs)
  {
    string str1 = args[0];
    Option option;
    try
    {
      option = EnumUtils.GetEnum<Option>(str1, StringComparison.OrdinalIgnoreCase);
    }
    catch (ArgumentException ex)
    {
      return false;
    }
    if (args.Length < 2)
      return false;
    string str2 = EnumUtils.GetString<Option>(option);
    string str3 = args[1];
    System.Type optionType = Options.Get().GetOptionType(option);
    if (optionType == typeof (bool))
    {
      bool boolVal;
      if (!GeneralUtils.TryParseBool(str3, out boolVal))
        return false;
      Options.Get().SetBool(option, boolVal);
    }
    else if (optionType == typeof (int))
    {
      int val;
      if (!GeneralUtils.TryParseInt(str3, out val))
        return false;
      Options.Get().SetInt(option, val);
    }
    else if (optionType == typeof (long))
    {
      long val;
      if (!GeneralUtils.TryParseLong(str3, out val))
        return false;
      Options.Get().SetLong(option, val);
    }
    else if (optionType == typeof (float))
    {
      float val;
      if (!GeneralUtils.TryParseFloat(str3, out val))
        return false;
      Options.Get().SetFloat(option, val);
    }
    else if (optionType == typeof (string))
    {
      str3 = rawArgs.Remove(0, str1.Length + 1);
      Options.Get().SetString(option, str3);
    }
    else
    {
      UIStatus.Get().AddError(string.Format("SetOption: {0} has unsupported underlying type {1}", (object) str2, (object) optionType), -1f);
      return true;
    }
    if (option == Option.CURSOR)
      Cursor.visible = Options.Get().GetBool(Option.CURSOR);
    else if (option == Option.FAKE_PACK_OPENING)
      NetCache.Get().ReloadNetObject<NetCache.NetCacheBoosters>();
    else if (option == Option.FAKE_PACK_COUNT && GameUtils.IsFakePackOpeningEnabled())
      NetCache.Get().ReloadNetObject<NetCache.NetCacheBoosters>();
    string message = string.Format("SetOption: {0} to {1}. GetOption = {2}", (object) str2, (object) str3, Options.Get().GetOption(option));
    UnityEngine.Debug.Log((object) message);
    UIStatus.Get().AddInfo(message);
    return true;
  }

  private bool OnProcessCheat_GetVar(string func, string[] args, string rawArgs)
  {
    string key = args[0];
    string message = string.Format("Var: {0} = {1}", (object) key, (object) (Vars.Key(key).GetStr((string) null) ?? "(null)"));
    UnityEngine.Debug.Log((object) message);
    UIStatus.Get().AddInfo(message);
    return true;
  }

  private bool OnProcessCheat_SetVar(string func, string[] args, string rawArgs)
  {
    string key = args[0];
    string str = args.Length >= 2 ? args[1] : (string) null;
    VarsInternal.Get().Set(key, str);
    string message = string.Format("Var: {0} = {1}", (object) key, (object) (str ?? "(null)"));
    UnityEngine.Debug.Log((object) message);
    UIStatus.Get().AddInfo(message);
    if (key.Equals("Arena.AutoDraft", StringComparison.InvariantCultureIgnoreCase) && (UnityEngine.Object) DraftDisplay.Get() != (UnityEngine.Object) null)
      DraftDisplay.Get().StartCoroutine(DraftDisplay.Get().RunAutoDraftCheat());
    return true;
  }

  private bool OnProcessCheat_autodraft(string func, string[] args, string rawArgs)
  {
    string strVal = args[0];
    bool flag = string.IsNullOrEmpty(strVal) || GeneralUtils.ForceBool(strVal);
    VarsInternal.Get().Set("Arena.AutoDraft", !flag ? "false" : "true");
    if (flag && (UnityEngine.Object) DraftDisplay.Get() != (UnityEngine.Object) null)
      DraftDisplay.Get().StartCoroutine(DraftDisplay.Get().RunAutoDraftCheat());
    else if (!flag)
      SceneDebugger.SetDevTimescale(1f);
    string message = string.Format("Arena autodraft turned {0}.", !flag ? (object) "off" : (object) "on");
    UnityEngine.Debug.Log((object) message);
    UIStatus.Get().AddInfo(message);
    return true;
  }

  private bool OnProcessCheat_onlygold(string func, string[] args, string rawArgs)
  {
    string lowerInvariant = args[0].ToLowerInvariant();
    string key = lowerInvariant;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapBB == null)
      {
        // ISSUE: reference to a compiler-generated field
        Cheats.\u003C\u003Ef__switch\u0024mapBB = new Dictionary<string, int>(4)
        {
          {
            "gold",
            0
          },
          {
            "normal",
            0
          },
          {
            "standard",
            0
          },
          {
            "both",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapBB.TryGetValue(key, out num))
      {
        if (num != 0)
        {
          if (num == 1)
            Options.Get().DeleteOption(Option.COLLECTION_PREMIUM_TYPE);
          else
            goto label_8;
        }
        else
          Options.Get().SetString(Option.COLLECTION_PREMIUM_TYPE, lowerInvariant);
        return true;
      }
    }
label_8:
    UIStatus.Get().AddError("Unknown cmd: " + (!string.IsNullOrEmpty(lowerInvariant) ? lowerInvariant : "(blank)") + "\nValid cmds: gold, standard, both", -1f);
    return false;
  }

  private bool OnProcessCheat_navigation(string func, string[] args, string rawArgs)
  {
    string lowerInvariant = args[0].ToLowerInvariant();
    string key = lowerInvariant;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapBC == null)
      {
        // ISSUE: reference to a compiler-generated field
        Cheats.\u003C\u003Ef__switch\u0024mapBC = new Dictionary<string, int>(2)
        {
          {
            "debug",
            0
          },
          {
            "dump",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapBC.TryGetValue(key, out num))
      {
        if (num != 0)
        {
          if (num == 1)
          {
            Navigation.DumpStack();
            UIStatus.Get().AddInfo("Navigation dumped, see Console or output log.");
            goto label_11;
          }
        }
        else
        {
          Navigation.NAVIGATION_DEBUG = args.Length < 2 || GeneralUtils.ForceBool(args[1]);
          if (Navigation.NAVIGATION_DEBUG)
          {
            Navigation.DumpStack();
            UIStatus.Get().AddInfo("Navigation debugging turned on - see Console or output log for nav dump.");
            goto label_11;
          }
          else
          {
            UIStatus.Get().AddInfo("Navigation debugging turned off.");
            goto label_11;
          }
        }
      }
    }
    UIStatus.Get().AddError("Unknown cmd: " + (!string.IsNullOrEmpty(lowerInvariant) ? lowerInvariant : "(blank)") + "\nValid cmds: debug, dump", -1f);
label_11:
    return true;
  }

  private bool OnProcessCheat_DeleteOption(string func, string[] args, string rawArgs)
  {
    string str = args[0];
    Option option;
    try
    {
      option = EnumUtils.GetEnum<Option>(str, StringComparison.OrdinalIgnoreCase);
    }
    catch (ArgumentException ex)
    {
      return false;
    }
    Options.Get().DeleteOption(option);
    string message = string.Format("DeleteOption: {0}. HasOption = {1}.", (object) EnumUtils.GetString<Option>(option), (object) Options.Get().HasOption(option));
    UnityEngine.Debug.Log((object) message);
    UIStatus.Get().AddInfo(message);
    return true;
  }

  private bool OnProcessCheat_collectionfirstxp(string func, string[] args, string rawArgs)
  {
    Options.Get().SetInt(Option.COVER_MOUSE_OVERS, 0);
    Options.Get().SetInt(Option.PAGE_MOUSE_OVERS, 0);
    return true;
  }

  private bool OnProcessCheat_board(string func, string[] args, string rawArgs)
  {
    this.m_board = args[0].ToUpperInvariant();
    return true;
  }

  private bool OnProcessCheat_resettips(string func, string[] args, string rawArgs)
  {
    Options.Get().SetBool(Option.HAS_SEEN_COLLECTIONMANAGER, false);
    return true;
  }

  private bool OnProcessCheat_brode(string func, string[] args, string rawArgs)
  {
    NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.ALL, new Vector3(133.1f, NotificationManager.DEPTH, 54.2f), GameStrings.Get("VO_INNKEEPER_FORGE_1WIN"), "VO_INNKEEPER_ARENA_1WIN", 0.0f, (Action) null, false);
    return true;
  }

  private bool OnProcessCheat_questcompletepopup(string func, string[] args, string rawArgs)
  {
    int result = 0;
    Achievement quest = !int.TryParse(rawArgs, out result) ? (Achievement) null : AchieveManager.Get().GetAchievement(result);
    if (quest == null)
    {
      UIStatus.Get().AddError(string.Format("{0}: please specify a valid Quest ID", (object) func), -1f);
      return true;
    }
    QuestToast.ShowQuestToast(UserAttentionBlocker.ALL, (QuestToast.DelOnCloseQuestToast) null, false, quest);
    return true;
  }

  private bool OnProcessCheat_questwelcome(string func, string[] args, string rawArgs)
  {
    bool boolVal = false;
    if (args.Length > 0 && !string.IsNullOrEmpty(args[0]))
      GeneralUtils.TryParseBool(args[0], out boolVal);
    WelcomeQuests.Show(UserAttentionBlocker.ALL, boolVal, (WelcomeQuests.DelOnWelcomeQuestsClosed) null, false);
    return true;
  }

  private bool OnProcessCheat_newquestvisual(string func, string[] args, string rawArgs)
  {
    if ((UnityEngine.Object) WelcomeQuests.Get() == (UnityEngine.Object) null)
    {
      UIStatus.Get().AddError("WelcomeQuests object is not active - try using 'questwelcome' cheat first.", -1f);
      return true;
    }
    int result = 0;
    Achievement quest = !int.TryParse(rawArgs, out result) ? (Achievement) null : AchieveManager.Get().GetAchievement(result);
    if (quest == null)
    {
      UIStatus.Get().AddError(string.Format("{0}: please specify a valid Quest ID", (object) func), -1f);
      return true;
    }
    QuestTile firstQuestTile = WelcomeQuests.Get().GetFirstQuestTile();
    firstQuestTile.SetupTile(quest);
    firstQuestTile.PlayBirth();
    return true;
  }

  private bool OnProcessCheat_questprogresspopup(string func, string[] args, string rawArgs)
  {
    int result1 = 0;
    Achievement achievement = !int.TryParse(rawArgs, out result1) ? (Achievement) null : AchieveManager.Get().GetAchievement(result1);
    string name;
    string description;
    int result2;
    int result3;
    if (achievement == null)
    {
      if (result1 != 0)
      {
        UIStatus.Get().AddError("unknown Achieve with ID " + (object) result1, -1f);
        return true;
      }
      if (args.Length != 4)
      {
        UIStatus.Get().AddError("please specify an Achieve ID or the following params:\n<title> <description> <progress> <maxprogress>", -1f);
        return true;
      }
      name = args[0];
      description = args[1];
      int.TryParse(args[2], out result2);
      int.TryParse(args[3], out result3);
    }
    else
    {
      name = achievement.Name;
      description = achievement.Description;
      result2 = achievement.Progress;
      result3 = achievement.MaxProgress;
    }
    if ((UnityEngine.Object) GameToastMgr.Get() != (UnityEngine.Object) null)
    {
      if (result2 >= result3)
        result2 = result3 - 1;
      GameToastMgr.Get().AddQuestProgressToast(result1, name, description, result2, result3);
      return true;
    }
    UIStatus.Get().AddError("GameToastMgr is null!", -1f);
    return true;
  }

  private bool OnProcessCheat_retire(string func, string[] args, string rawArgs)
  {
    if (DemoMgr.Get().GetMode() != DemoMode.BLIZZCON_2013)
      return false;
    DraftManager draftManager = DraftManager.Get();
    if (draftManager == null)
      return false;
    Network.RetireDraftDeck(draftManager.GetDraftDeck().ID, draftManager.GetSlot());
    return true;
  }

  private bool OnProcessCheat_storepassword(string func, string[] args, string rawArgs)
  {
    if (this.m_loadingStoreChallengePrompt)
      return true;
    if ((UnityEngine.Object) this.m_storeChallengePrompt == (UnityEngine.Object) null)
    {
      this.m_loadingStoreChallengePrompt = true;
      AssetLoader.Get().LoadGameObject("StoreChallengePrompt", (AssetLoader.GameObjectCallback) ((name, go, callbackData) => CheatMgr.Get().StartCoroutine(this.StorePasswordCoroutine(name, go, callbackData))), (object) null, false);
    }
    else if (this.m_storeChallengePrompt.IsShown())
      this.m_storeChallengePrompt.Hide();
    else
      CheatMgr.Get().StartCoroutine(this.StorePasswordCoroutine(this.m_storeChallengePrompt.name, this.m_storeChallengePrompt.gameObject, (object) null));
    return true;
  }

  private bool OnProcessCheat_notice(string func, string[] args, string rawArgs)
  {
    if (((IEnumerable<string>) args).Count<string>() < 2)
    {
      UIStatus.Get().AddError("notice cheat requires 2 params: [string]type [int]data [OPTIONAL int]data2 [OPTIONAL bool]quest toast?", -1f);
      return true;
    }
    int result1 = -1;
    int.TryParse(args[1], out result1);
    if (result1 < 0)
    {
      UIStatus.Get().AddError(string.Format("{0}: please specify a valid Notice Data Value", (object) result1), -1f);
      return true;
    }
    string s = (string) null;
    if (args.Length > 2)
      s = args[2];
    bool flag = false;
    if (args.Length > 3)
      flag = GeneralUtils.ForceBool(args[3]);
    NetCache.ProfileNotice notice = (NetCache.ProfileNotice) null;
    Achievement quest = new Achievement();
    List<RewardData> childRewards = new List<RewardData>();
    string key = args[0];
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapBD == null)
      {
        // ISSUE: reference to a compiler-generated field
        Cheats.\u003C\u003Ef__switch\u0024mapBD = new Dictionary<string, int>(6)
        {
          {
            "gold",
            0
          },
          {
            "dust",
            1
          },
          {
            "booster",
            2
          },
          {
            "card",
            3
          },
          {
            "cardback",
            4
          },
          {
            "tavern_brawl_rewards",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapBD.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            if (flag)
            {
              childRewards.Add((RewardData) new GoldRewardData()
              {
                Amount = (long) result1
              });
              break;
            }
            notice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardGold()
            {
              Amount = result1
            };
            break;
          case 1:
            if (flag)
            {
              childRewards.Add((RewardData) new ArcaneDustRewardData()
              {
                Amount = result1
              });
              break;
            }
            notice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardDust()
            {
              Amount = result1
            };
            break;
          case 2:
            int result2 = 1;
            if (!string.IsNullOrEmpty(s))
              int.TryParse(s, out result2);
            if (GameDbf.Booster.GetRecord(result2) == null)
            {
              UIStatus.Get().AddError(string.Format("Booster ID is invalid: {0}", (object) result2), -1f);
              return true;
            }
            if (flag)
            {
              childRewards.Add((RewardData) new BoosterPackRewardData()
              {
                Id = result2,
                Count = result1
              });
              break;
            }
            notice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardBooster()
            {
              Count = result1,
              Id = result2
            };
            break;
          case 3:
            string cardId = "NEW1_040";
            if (!string.IsNullOrEmpty(s))
            {
              int result3 = -1;
              int.TryParse(s, out result3);
              cardId = result3 <= 0 ? s : GameUtils.TranslateDbIdToCardId(result3);
            }
            if (GameUtils.GetCardRecord(cardId) == null)
            {
              UIStatus.Get().AddError(string.Format("Card ID is invalid: {0}", (object) cardId), -1f);
              return true;
            }
            if (flag)
            {
              childRewards.Add((RewardData) new CardRewardData()
              {
                CardID = cardId,
                Count = Mathf.Clamp(result1, 1, 2)
              });
              break;
            }
            notice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardCard()
            {
              CardID = cardId,
              Quantity = Mathf.Clamp(result1, 1, 2)
            };
            break;
          case 4:
            if (GameDbf.CardBack.GetRecord(result1) == null)
            {
              UIStatus.Get().AddError(string.Format("Cardback ID is invalid: {0}", (object) result1), -1f);
              return true;
            }
            if (flag)
            {
              childRewards.Add((RewardData) new CardBackRewardData()
              {
                CardBackID = result1
              });
              break;
            }
            notice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardCardBack()
            {
              CardBackID = result1
            };
            break;
          case 5:
            notice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeTavernBrawlRewards()
            {
              Wins = result1,
              Chest = RewardUtils.GenerateTavernBrawlRewardChest_CHEAT(result1)
            };
            break;
          default:
            goto label_39;
        }
        if (flag)
        {
          quest.AddChildRewards(childRewards);
          quest.SetDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", string.Empty);
          quest.SetName("Title Text", string.Empty);
          QuestToast.ShowQuestToast(UserAttentionBlocker.ALL, (QuestToast.DelOnCloseQuestToast) null, false, quest);
        }
        else
          NetCache.Get().Cheat_AddNotice(notice);
        return true;
      }
    }
label_39:
    UIStatus.Get().AddError(string.Format("{0}: please specify a valid Notice Type.\nValid Types are: 'gold','dust','booster','card','cardback','tavern_brawl_rewards'", (object) args[0]), -1f);
    return true;
  }

  private string GetChallengeUrl(string type)
  {
    string str1 = string.Format("https://login-qa-us.web.blizzard.net/login/admin/challenge/create/ct_{0}", (object) type.ToLower());
    string format = "{0}?email={1}&programId={2}&platformId={3}&redirectUrl={4}&messageKey={5}&notifyRisk={6}&chooseChallenge={7}&challengeType={8}&riskTransId={9}";
    string str2 = "joe_balance@zmail.blizzard.com";
    string str3 = "wtcg";
    string str4 = "*";
    string str5 = "none";
    string empty1 = string.Empty;
    bool flag1 = false;
    bool flag2 = false;
    string empty2 = string.Empty;
    string empty3 = string.Empty;
    return string.Format(format, (object) str1, (object) str2, (object) str3, (object) str4, (object) str5, (object) empty1, (object) flag1, (object) flag2, (object) empty2, (object) empty3);
  }

  [DebuggerHidden]
  private IEnumerator StorePasswordCoroutine(string name, GameObject go, object callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Cheats.\u003CStorePasswordCoroutine\u003Ec__Iterator2FD() { go = go, \u003C\u0024\u003Ego = go, \u003C\u003Ef__this = this };
  }

  private bool OnProcessCheat_defaultcardback(string func, string[] args, string rawArgs)
  {
    int result;
    if (args.Length == 0 || !int.TryParse(args[0].ToLowerInvariant(), out result))
      return false;
    Network.SetDefaultCardBack(result);
    return true;
  }

  private bool OnProcessCheat_disconnect(string func, string[] args, string rawArgs)
  {
    if (args != null && args.Length >= 1 && args[0] == "bnet")
    {
      if (Network.BattleNetStatus() != Network.BnetLoginState.BATTLE_NET_LOGGED_IN)
      {
        UIStatus.Get().AddError("Not connected to Battle.net, status=" + (object) Network.BattleNetStatus(), -1f);
        return true;
      }
      BattleNet.RequestCloseAurora();
      UIStatus.Get().AddInfo("Disconnecting from Battle.net.");
      return true;
    }
    if (!Network.IsConnectedToGameServer())
    {
      UIStatus.Get().AddError("Not connected to game server.", -1f);
      return true;
    }
    bool flag = args == null || args.Length == 0 || args[0] != "force";
    Log.LoadingScreen.Print("Cheats.OnProcessCheat_disconnect() - reconnect={0}", (object) ReconnectMgr.Get().IsReconnectEnabled());
    if (flag)
    {
      if (ReconnectMgr.Get().IsReconnectEnabled())
        Network.DisconnectFromGameServer();
      else
        Network.Concede();
    }
    else
      Network.SimulateUncleanDisconnectFromGameServer();
    return true;
  }

  private bool OnProcessCheat_restart(string func, string[] args, string rawArgs)
  {
    if (!Network.IsConnectedToGameServer())
    {
      UIStatus.Get().AddError("Not connected to game server.", -1f);
      return true;
    }
    if (!GameUtils.CanRestartCurrentMission(false))
    {
      UIStatus.Get().AddError("This game cannot be restarted.", -1f);
      return true;
    }
    GameState.Get().Restart();
    return true;
  }

  private bool OnProcessCheat_warning(string func, string[] args, string rawArgs)
  {
    string header;
    string message;
    this.ParseErrorText(args, rawArgs, out header, out message);
    Error.AddWarning(header, message);
    return true;
  }

  private bool OnProcessCheat_fatal(string func, string[] args, string rawArgs)
  {
    Error.AddFatal(rawArgs);
    return true;
  }

  private bool OnProcessCheat_exit(string func, string[] args, string rawArgs)
  {
    GeneralUtils.ExitApplication();
    return true;
  }

  private bool OnProcessCheat_log(string func, string[] args, string rawArgs)
  {
    string lowerInvariant = args[0].ToLowerInvariant();
    if (!(lowerInvariant == "load") && !(lowerInvariant == "reload"))
      return false;
    Log.Get().Load();
    return true;
  }

  private bool OnProcessCheat_alert(string func, string[] args, string rawArgs)
  {
    AlertPopup.PopupInfo alertInfo = this.GenerateAlertInfo(rawArgs);
    if ((UnityEngine.Object) this.m_alert == (UnityEngine.Object) null)
      DialogManager.Get().ShowPopup(alertInfo, new DialogManager.DialogProcessCallback(this.OnAlertProcessed));
    else
      this.m_alert.UpdateInfo(alertInfo);
    return true;
  }

  private bool GetBonusStarsAndLevel(int lastSeasonRank, out int bonusStars, out int newSeasonRank)
  {
    int num1 = 26 - lastSeasonRank;
    bonusStars = num1 - 1;
    int num2 = 1;
    newSeasonRank = 26 - num2;
    int num3;
    switch (num1)
    {
      case 1:
        num3 = 1;
        break;
      case 2:
        num3 = 1;
        break;
      case 3:
        num3 = 1;
        break;
      case 4:
        num3 = 2;
        break;
      case 5:
        num3 = 2;
        break;
      case 6:
        num3 = 3;
        break;
      case 7:
        num3 = 3;
        break;
      case 8:
        num3 = 4;
        break;
      case 9:
        num3 = 4;
        break;
      case 10:
        num3 = 5;
        break;
      case 11:
        num3 = 5;
        break;
      case 12:
        num3 = 6;
        break;
      case 13:
        num3 = 6;
        break;
      case 14:
        num3 = 6;
        break;
      case 15:
        num3 = 7;
        break;
      case 16:
        num3 = 7;
        break;
      case 17:
        num3 = 7;
        break;
      case 18:
        num3 = 8;
        break;
      case 19:
        num3 = 8;
        break;
      case 20:
        num3 = 8;
        break;
      case 21:
        num3 = 9;
        break;
      case 22:
        num3 = 9;
        break;
      case 23:
        num3 = 9;
        break;
      case 24:
        num3 = 10;
        break;
      case 25:
        num3 = 10;
        break;
      case 26:
        num3 = 10;
        break;
      default:
        return false;
    }
    newSeasonRank = 26 - num3;
    return true;
  }

  private bool OnProcessCheat_seasondialog(string func, string[] args, string rawArgs)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Cheats.\u003COnProcessCheat_seasondialog\u003Ec__AnonStorey41E seasondialogCAnonStorey41E = new Cheats.\u003COnProcessCheat_seasondialog\u003Ec__AnonStorey41E();
    if (args.Length < 3)
    {
      UIStatus.Get().AddInfo("please specify the following params:\n<season number> <ending rank> <is wild rank>");
      return true;
    }
    // ISSUE: reference to a compiler-generated field
    if (!int.TryParse(args[0].ToLowerInvariant(), out seasondialogCAnonStorey41E.seasonID))
    {
      UIStatus.Get().AddInfo("please enter a valid season #");
      return true;
    }
    int result;
    if (!int.TryParse(args[1].ToLowerInvariant(), out result))
    {
      UIStatus.Get().AddInfo("please enter a valid rank #");
      return true;
    }
    bool boolVal;
    if (!GeneralUtils.TryParseBool(args[2], out boolVal))
    {
      UIStatus.Get().AddInfo("please enter a valid bool value for 3rd parameter <is wild rank>");
      return true;
    }
    int bonusStars;
    int newSeasonRank;
    if (!this.GetBonusStarsAndLevel(result, out bonusStars, out newSeasonRank))
    {
      UIStatus.Get().AddInfo("could not find an appropriate BonusStarsAndLevel for Rank " + (object) result + " :(");
      return true;
    }
    SeasonEndDialog.SeasonEndInfo seasonEndInfo = new SeasonEndDialog.SeasonEndInfo();
    // ISSUE: reference to a compiler-generated field
    seasonEndInfo.m_seasonID = seasondialogCAnonStorey41E.seasonID;
    seasonEndInfo.m_rank = result;
    seasonEndInfo.m_chestRank = result;
    seasonEndInfo.m_legendIndex = 0;
    seasonEndInfo.m_bonusStars = bonusStars;
    seasonEndInfo.m_boostedRank = newSeasonRank;
    seasonEndInfo.m_isWild = boolVal;
    seasonEndInfo.m_rankedRewards = new List<RewardData>();
    if (result <= 20)
    {
      // ISSUE: reference to a compiler-generated method
      CardBackDbfRecord record = GameDbf.CardBack.GetRecord(new Predicate<CardBackDbfRecord>(seasondialogCAnonStorey41E.\u003C\u003Em__27A));
      int cardBackID = record != null ? record.ID : 0;
      if (record == null)
      {
        IEnumerable<CardBackDbfRecord> source = GameDbf.CardBack.GetRecords().Where<CardBackDbfRecord>((Func<CardBackDbfRecord, bool>) (r => r.Source == "season"));
        long num1 = source.Min<CardBackDbfRecord>((Func<CardBackDbfRecord, long>) (r => r.Data1));
        long num2 = source.Max<CardBackDbfRecord>((Func<CardBackDbfRecord, long>) (r => r.Data1));
        // ISSUE: reference to a compiler-generated field
        UIStatus.Get().AddInfo(string.Format("NOTE: there is no cardback for Season {0}, using default cardback.\nKnown seasons with cardbacks: {1}-{2}", (object) seasondialogCAnonStorey41E.seasonID, (object) num1, (object) num2), 10f);
      }
      seasonEndInfo.m_rankedRewards.Add((RewardData) new CardBackRewardData(cardBackID));
      seasonEndInfo.m_rankedRewards.Add((RewardData) new CardRewardData("EX1_279", TAG_PREMIUM.GOLDEN, 1));
      if (result <= 15)
        seasonEndInfo.m_rankedRewards.Add((RewardData) new CardRewardData("EX1_279", TAG_PREMIUM.GOLDEN, 1));
      if (result <= 10)
        seasonEndInfo.m_rankedRewards.Add((RewardData) new CardRewardData("EX1_279", TAG_PREMIUM.GOLDEN, 1));
      if (result <= 5)
        seasonEndInfo.m_rankedRewards.Add((RewardData) new CardRewardData("EX1_279", TAG_PREMIUM.GOLDEN, 1));
    }
    seasonEndInfo.m_isFake = true;
    DialogManager.Get().AddToQueue(new DialogManager.DialogRequest()
    {
      m_type = DialogManager.DialogType.SEASON_END,
      m_info = (object) seasonEndInfo,
      m_isFake = true
    });
    return true;
  }

  private bool OnProcessCheat_playnullsound(string func, string[] args, string rawArgs)
  {
    SoundManager.Get().Play((AudioSource) null, true);
    return true;
  }

  private bool OnProcessCheat_spectate(string func, string[] args, string rawArgs)
  {
    if (args.Length >= 1 && args[0] == "waiting")
    {
      SpectatorManager.Get().ShowWaitingForNextGameDialog();
      return true;
    }
    if (args.Length < 4 || ((IEnumerable<string>) args).Any<string>((Func<string, bool>) (a => string.IsNullOrEmpty(a))))
    {
      Error.AddWarning("Spectate Cheat Error", "spectate cheat must have the following args:\n\nspectate ipaddress port game_handle spectator_password [gameType] [missionId]");
      return false;
    }
    JoinInfo joinInfo = new JoinInfo();
    joinInfo.ServerIpAddress = args[0];
    joinInfo.SecretKey = args[3];
    uint result1;
    if (!uint.TryParse(args[1], out result1))
    {
      Error.AddWarning("Spectate Cheat Error", "error parsing the port # (uint) argument: " + args[1]);
      return false;
    }
    joinInfo.ServerPort = result1;
    int result2;
    if (!int.TryParse(args[2], out result2))
    {
      Error.AddWarning("Spectate Cheat Error", "error parsing the game_handle (int) argument: " + args[2]);
      return false;
    }
    joinInfo.GameHandle = result2;
    joinInfo.GameType = GameType.GT_UNKNOWN;
    joinInfo.MissionId = 2;
    if (args.Length >= 5 && int.TryParse(args[4], out result2))
      joinInfo.GameType = (GameType) result2;
    if (args.Length >= 6 && int.TryParse(args[5], out result2))
      joinInfo.MissionId = result2;
    GameMgr.Get().SpectateGame(joinInfo);
    return true;
  }

  private static void SubscribePartyEvents()
  {
    if (Cheats.s_hasSubscribedToPartyEvents)
      return;
    BnetParty.OnError += (BnetParty.PartyErrorHandler) (error => Cheats.PartyLogger.Print("{0} code={1} feature={2} party={3} str={4}", (object) error.DebugContext, (object) error.ErrorCode, (object) error.FeatureEvent.ToString(), (object) new PartyInfo(error.PartyId, error.PartyType), (object) error.StringData));
    BnetParty.OnJoined += (BnetParty.JoinedHandler) ((e, party, reason) => Cheats.PartyLogger.Print("Party.OnJoined {0} party={1} reason={2}", new object[3]
    {
      (object) e,
      (object) party,
      !reason.HasValue ? (object) "null" : (object) reason.Value.ToString()
    }));
    BnetParty.OnPrivacyLevelChanged += (BnetParty.PrivacyLevelChangedHandler) ((party, privacy) => Cheats.PartyLogger.Print("Party.OnPrivacyLevelChanged party={0} privacy={1}", new object[2]
    {
      (object) party,
      (object) privacy
    }));
    BnetParty.OnMemberEvent += (BnetParty.MemberEventHandler) ((e, party, memberId, isRolesUpdate, reason) => Cheats.PartyLogger.Print("Party.OnMemberEvent {0} party={1} memberId={2} isRolesUpdate={3} reason={4}", (object) e, (object) party, (object) memberId, (object) isRolesUpdate, !reason.HasValue ? (object) "null" : (object) reason.Value.ToString()));
    BnetParty.OnReceivedInvite += (BnetParty.ReceivedInviteHandler) ((e, party, inviteId, reason) => Cheats.PartyLogger.Print("Party.OnReceivedInvite {0} party={1} inviteId={2} reason={3}", (object) e, (object) party, (object) inviteId, !reason.HasValue ? (object) "null" : (object) reason.Value.ToString()));
    BnetParty.OnSentInvite += (BnetParty.SentInviteHandler) ((e, party, inviteId, senderIsMyself, reason) =>
    {
      PartyInvite sentInvite = BnetParty.GetSentInvite(party.Id, inviteId);
      Cheats.PartyLogger.Print("Party.OnSentInvite {0} party={1} inviteId={2} senderIsMyself={3} isRejoin={4} reason={5}", (object) e, (object) party, (object) inviteId, (object) senderIsMyself, sentInvite != null ? (object) sentInvite.IsRejoin.ToString() : (object) "null", !reason.HasValue ? (object) "null" : (object) reason.Value.ToString());
    });
    BnetParty.OnReceivedInviteRequest += (BnetParty.ReceivedInviteRequestHandler) ((e, party, request, reason) => Cheats.PartyLogger.Print("Party.OnReceivedInviteRequest {0} party={1} target={2} {3} requester={4} {5} reason={6}", (object) e, (object) party, (object) request.TargetName, (object) request.TargetId, (object) request.RequesterName, (object) request.RequesterId, !reason.HasValue ? (object) "null" : (object) reason.Value.ToString()));
    BnetParty.OnChatMessage += (BnetParty.ChatMessageHandler) ((party, speakerId, msg) => Cheats.PartyLogger.Print("Party.OnChatMessage party={0} speakerId={1} msg={2}", new object[3]
    {
      (object) party,
      (object) speakerId,
      (object) msg
    }));
    BnetParty.OnPartyAttributeChanged += (BnetParty.PartyAttributeChangedHandler) ((party, key, attrVal) =>
    {
      string str1 = "null";
      if (attrVal.HasIntValue)
        str1 = "[long]" + attrVal.IntValue.ToString();
      else if (attrVal.HasStringValue)
        str1 = "[string]" + attrVal.StringValue;
      else if (attrVal.HasBlobValue)
      {
        byte[] blobValue = attrVal.BlobValue;
        if (blobValue != null)
        {
          str1 = "blobLength=" + (object) blobValue.Length;
          try
          {
            string str2 = Encoding.UTF8.GetString(blobValue);
            if (str2 != null)
              str1 = str1 + " decodedUtf8=" + str2;
          }
          catch (ArgumentException ex)
          {
          }
        }
      }
      Cheats.PartyLogger.Print("BnetParty.OnPartyAttributeChanged party={0} key={1} value={2}", new object[3]
      {
        (object) party,
        (object) key,
        (object) str1
      });
    });
    Cheats.s_hasSubscribedToPartyEvents = true;
  }

  private static PartyId ParsePartyId(string cmd, string arg, int argIndex, ref string errorMsg)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Cheats.\u003CParsePartyId\u003Ec__AnonStorey41F idCAnonStorey41F = new Cheats.\u003CParsePartyId\u003Ec__AnonStorey41F();
    PartyId partyId = (PartyId) null;
    // ISSUE: reference to a compiler-generated field
    if (ulong.TryParse(arg, out idCAnonStorey41F.low))
    {
      PartyId[] joinedPartyIds = BnetParty.GetJoinedPartyIds();
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      // ISSUE: reference to a compiler-generated field
      partyId = idCAnonStorey41F.low < 0UL || joinedPartyIds.Length <= 0 || idCAnonStorey41F.low >= (ulong) joinedPartyIds.LongLength ? ((IEnumerable<PartyId>) joinedPartyIds).FirstOrDefault<PartyId>(new Func<PartyId, bool>(idCAnonStorey41F.\u003C\u003Em__288)) : joinedPartyIds[idCAnonStorey41F.low];
      if (partyId == (PartyId) null)
      {
        // ISSUE: reference to a compiler-generated field
        errorMsg = "party " + cmd + ": couldn't find party at index, or with PartyId low bits: " + (object) idCAnonStorey41F.low;
      }
    }
    else
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      Cheats.\u003CParsePartyId\u003Ec__AnonStorey420 idCAnonStorey420 = new Cheats.\u003CParsePartyId\u003Ec__AnonStorey420();
      // ISSUE: reference to a compiler-generated field
      if (!EnumUtils.TryGetEnum<PartyType>(arg, out idCAnonStorey420.type))
      {
        errorMsg = "party " + cmd + ": unable to parse party (index or LowBits or type)" + (argIndex < 0 ? string.Empty : " at arg index=" + (object) argIndex) + " (" + arg + "), please specify the Low bits of a PartyId or a PartyType.";
      }
      else
      {
        // ISSUE: reference to a compiler-generated method
        partyId = ((IEnumerable<PartyInfo>) BnetParty.GetJoinedParties()).Where<PartyInfo>(new Func<PartyInfo, bool>(idCAnonStorey420.\u003C\u003Em__289)).Select<PartyInfo, PartyId>((Func<PartyInfo, PartyId>) (info => info.Id)).FirstOrDefault<PartyId>();
        if (partyId == (PartyId) null)
          errorMsg = "party " + cmd + ": no joined party with PartyType: " + arg;
      }
    }
    return partyId;
  }

  private bool OnProcessCheat_party(string func, string[] args, string rawArgs)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey425 partyCAnonStorey425 = new Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey425();
    if (args.Length < 1 || ((IEnumerable<string>) args).Any<string>((Func<string, bool>) (a => string.IsNullOrEmpty(a))))
    {
      Error.AddWarning("Party Cheat Error", "USAGE: party [cmd] [args]\nCommands: create | join | leave | dissolve | list | invite | accept | decline | revoke | requestinvite | ignorerequest | setleader | kick | chat | setprivacy | setlong | setstring | setblob | clearattr | subscribe | unsubscribe");
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    partyCAnonStorey425.cmd = args[0];
    // ISSUE: reference to a compiler-generated field
    if (partyCAnonStorey425.cmd == "unsubscribe")
    {
      BnetParty.RemoveFromAllEventHandlers((object) this);
      Cheats.s_hasSubscribedToPartyEvents = false;
      // ISSUE: reference to a compiler-generated field
      Cheats.PartyLogger.Print("party {0}: unsubscribed.", (object) partyCAnonStorey425.cmd);
      return true;
    }
    bool flag1 = true;
    string[] array1 = ((IEnumerable<string>) args).Skip<string>(1).ToArray<string>();
    string errorMsg1 = (string) null;
    Cheats.SubscribePartyEvents();
    // ISSUE: reference to a compiler-generated field
    string cmd = partyCAnonStorey425.cmd;
    if (cmd != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapBE == null)
      {
        // ISSUE: reference to a compiler-generated field
        Cheats.\u003C\u003Ef__switch\u0024mapBE = new Dictionary<string, int>(20)
        {
          {
            "create",
            0
          },
          {
            "leave",
            1
          },
          {
            "dissolve",
            1
          },
          {
            "join",
            2
          },
          {
            "chat",
            3
          },
          {
            "invite",
            4
          },
          {
            "accept",
            5
          },
          {
            "decline",
            5
          },
          {
            "revoke",
            6
          },
          {
            "requestinvite",
            7
          },
          {
            "ignorerequest",
            8
          },
          {
            "setleader",
            9
          },
          {
            "kick",
            10
          },
          {
            "setprivacy",
            11
          },
          {
            "setlong",
            12
          },
          {
            "setstring",
            12
          },
          {
            "setblob",
            12
          },
          {
            "clearattr",
            13
          },
          {
            "subscribe",
            14
          },
          {
            "list",
            14
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapBE.TryGetValue(cmd, out num))
      {
        switch (num)
        {
          case 0:
            if (array1.Length < 1)
            {
              errorMsg1 = "party create: requires a PartyType: " + string.Join(" | ", Enum.GetValues(typeof (PartyType)).Cast<PartyType>().Select<PartyType, string>((Func<PartyType, string>) (v => ((int) v).ToString() + " (" + (object) v + ")")).ToArray<string>());
              goto label_299;
            }
            else
            {
              int result;
              PartyType outVal;
              if (int.TryParse(array1[0], out result))
                outVal = (PartyType) result;
              else if (!EnumUtils.TryGetEnum<PartyType>(array1[0], out outVal))
                errorMsg1 = "party create: unknown PartyType specified: " + array1[0];
              if (errorMsg1 == null)
              {
                byte[] byteArray = ProtobufUtil.ToByteArray((IProtoBuf) BnetUtils.CreatePegasusBnetId((BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId()));
                BnetParty.CreateParty(outVal, PrivacyLevel.OPEN_INVITATION_AND_FRIEND, byteArray, (BnetParty.CreateSuccessCallback) ((t, partyId) => Cheats.PartyLogger.Print("BnetParty.CreateSuccessCallback type={0} partyId={1}", new object[2]
                {
                  (object) t,
                  (object) partyId
                })));
                goto label_299;
              }
              else
                goto label_299;
            }
          case 1:
            // ISSUE: reference to a compiler-generated field
            bool flag2 = partyCAnonStorey425.cmd == "dissolve";
            if (array1.Length == 0)
            {
              // ISSUE: reference to a compiler-generated field
              Cheats.PartyLogger.Print("NOTE: party {0} without any arguments will {0} all joined parties.", (object) partyCAnonStorey425.cmd);
              PartyInfo[] joinedParties = BnetParty.GetJoinedParties();
              if (joinedParties.Length == 0)
                Cheats.PartyLogger.Print("No joined parties.");
              foreach (PartyInfo partyInfo in joinedParties)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.PartyLogger.Print("party {0}: {1} party {2}", new object[3]
                {
                  (object) partyCAnonStorey425.cmd,
                  (object) (!flag2 ? "leaving" : "dissolving"),
                  (object) partyInfo
                });
                if (flag2)
                  BnetParty.DissolveParty(partyInfo.Id);
                else
                  BnetParty.Leave(partyInfo.Id);
              }
              goto label_299;
            }
            else
            {
              for (int argIndex = 0; argIndex < array1.Length; ++argIndex)
              {
                string str = array1[argIndex];
                string errorMsg2 = (string) null;
                // ISSUE: reference to a compiler-generated field
                PartyId partyId = Cheats.ParsePartyId(partyCAnonStorey425.cmd, str, argIndex, ref errorMsg2);
                if (errorMsg2 != null)
                  Cheats.PartyLogger.Print(errorMsg2);
                if (partyId != (PartyId) null)
                {
                  // ISSUE: reference to a compiler-generated field
                  Cheats.PartyLogger.Print("party {0}: {1} party {2}", new object[3]
                  {
                    (object) partyCAnonStorey425.cmd,
                    (object) (!flag2 ? "leaving" : "dissolving"),
                    (object) BnetParty.GetJoinedParty(partyId)
                  });
                  if (flag2)
                    BnetParty.DissolveParty(partyId);
                  else
                    BnetParty.Leave(partyId);
                }
              }
              goto label_299;
            }
          case 2:
            if (array1.Length < 1)
            {
              // ISSUE: reference to a compiler-generated field
              errorMsg1 = "party " + partyCAnonStorey425.cmd + ": must specify an online friend index or a partyId (Hi-Lo format)";
              goto label_299;
            }
            else
            {
              PartyType partyType = PartyType.DEFAULT;
              foreach (string s1 in array1)
              {
                int length = s1.IndexOf('-');
                int result1 = -1;
                PartyId partyId = (PartyId) null;
                if (length >= 0)
                {
                  string s2 = s1.Substring(0, length);
                  string s3 = s1.Length <= length ? string.Empty : s1.Substring(length + 1);
                  ulong result2;
                  ulong result3;
                  if (ulong.TryParse(s2, out result2) && ulong.TryParse(s3, out result3))
                  {
                    partyId = new PartyId(result2, result3);
                  }
                  else
                  {
                    // ISSUE: reference to a compiler-generated field
                    errorMsg1 = "party " + partyCAnonStorey425.cmd + ": unable to parse partyId (in format Hi-Lo).";
                  }
                }
                else if (int.TryParse(s1, out result1))
                {
                  BnetPlayer[] array2 = BnetFriendMgr.Get().GetFriends().Where<BnetPlayer>((Func<BnetPlayer, bool>) (p =>
                  {
                    if (p.IsOnline())
                      return p.GetHearthstoneGameAccount() != (BnetGameAccount) null;
                    return false;
                  })).ToArray<BnetPlayer>();
                  if (result1 < 0 || result1 >= array2.Length)
                  {
                    // ISSUE: reference to a compiler-generated field
                    errorMsg1 = "party " + partyCAnonStorey425.cmd + ": no online friend at index " + (object) result1;
                  }
                  else
                  {
                    // ISSUE: reference to a compiler-generated field
                    errorMsg1 = "party " + partyCAnonStorey425.cmd + ": Not-Yet-Implemented: find partyId from online friend's presence.";
                  }
                }
                else
                {
                  // ISSUE: reference to a compiler-generated field
                  errorMsg1 = "party " + partyCAnonStorey425.cmd + ": unable to parse online friend index.";
                }
                if (partyId != (PartyId) null)
                  BnetParty.JoinParty(partyId, partyType);
              }
              goto label_299;
            }
          case 3:
            PartyId[] joinedPartyIds1 = BnetParty.GetJoinedPartyIds();
            if (array1.Length < 1)
            {
              errorMsg1 = "party chat: must specify 1-2 arguments: party (index or LowBits or type) or a message to send.";
              goto label_299;
            }
            else
            {
              int count = 1;
              // ISSUE: reference to a compiler-generated field
              PartyId partyId = Cheats.ParsePartyId(partyCAnonStorey425.cmd, array1[0], -1, ref errorMsg1);
              if (partyId == (PartyId) null && joinedPartyIds1.Length > 0)
              {
                errorMsg1 = (string) null;
                partyId = joinedPartyIds1[0];
                count = 0;
              }
              if (partyId != (PartyId) null)
              {
                BnetParty.SendChatMessage(partyId, string.Join(" ", ((IEnumerable<string>) array1).Skip<string>(count).ToArray<string>()));
                goto label_299;
              }
              else
                goto label_299;
            }
          case 4:
            PartyId partyId1 = (PartyId) null;
            int count1 = 1;
            if (array1.Length == 0)
            {
              PartyId[] joinedPartyIds2 = BnetParty.GetJoinedPartyIds();
              if (joinedPartyIds2.Length > 0)
              {
                partyId1 = joinedPartyIds2[0];
                count1 = 0;
              }
              else
                errorMsg1 = "party invite: no joined parties to invite to.";
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              partyId1 = Cheats.ParsePartyId(partyCAnonStorey425.cmd, array1[0], -1, ref errorMsg1);
            }
            if (partyId1 != (PartyId) null)
            {
              string[] array2 = ((IEnumerable<string>) array1).Skip<string>(count1).ToArray<string>();
              HashSet<BnetPlayer> bnetPlayerSet = new HashSet<BnetPlayer>();
              IEnumerable<BnetPlayer> source1 = BnetFriendMgr.Get().GetFriends().Where<BnetPlayer>((Func<BnetPlayer, bool>) (p =>
              {
                if (p.IsOnline())
                  return p.GetHearthstoneGameAccount() != (BnetGameAccount) null;
                return false;
              }));
              if (array2.Length == 0)
              {
                Cheats.PartyLogger.Print("NOTE: party invite without any arguments will pick the first online friend.");
                BnetPlayer bnetPlayer = source1.FirstOrDefault<BnetPlayer>();
                if (bnetPlayer == null)
                  errorMsg1 = "party invite: no online Hearthstone friend found.";
                else
                  bnetPlayerSet.Add(bnetPlayer);
              }
              else
              {
                for (int index = 0; index < array2.Length; ++index)
                {
                  // ISSUE: object of a compiler-generated type is created
                  // ISSUE: variable of a compiler-generated type
                  Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey421 partyCAnonStorey421 = new Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey421();
                  // ISSUE: reference to a compiler-generated field
                  partyCAnonStorey421.arg = array2[index];
                  int result;
                  // ISSUE: reference to a compiler-generated field
                  if (int.TryParse(partyCAnonStorey421.arg, out result))
                  {
                    BnetPlayer bnetPlayer = source1.ElementAtOrDefault<BnetPlayer>(result);
                    if (bnetPlayer == null)
                      errorMsg1 = "party invite: no online Hearthstone friend index " + (object) result;
                    else
                      bnetPlayerSet.Add(bnetPlayer);
                  }
                  else
                  {
                    // ISSUE: reference to a compiler-generated method
                    IEnumerable<BnetPlayer> source2 = source1.Where<BnetPlayer>(new Func<BnetPlayer, bool>(partyCAnonStorey421.\u003C\u003Em__290));
                    if (!source2.Any<BnetPlayer>())
                    {
                      // ISSUE: reference to a compiler-generated field
                      errorMsg1 = "party invite: no online Hearthstone friend matching name " + partyCAnonStorey421.arg + " (arg index " + (object) index + ")";
                    }
                    else
                    {
                      foreach (BnetPlayer bnetPlayer in source2)
                      {
                        if (!bnetPlayerSet.Contains(bnetPlayer))
                        {
                          bnetPlayerSet.Add(bnetPlayer);
                          break;
                        }
                      }
                    }
                  }
                }
              }
              using (HashSet<BnetPlayer>.Enumerator enumerator = bnetPlayerSet.GetEnumerator())
              {
                while (enumerator.MoveNext())
                {
                  BnetPlayer current = enumerator.Current;
                  BnetGameAccountId hearthstoneGameAccountId = current.GetHearthstoneGameAccountId();
                  if (BnetParty.IsMember(partyId1, hearthstoneGameAccountId))
                  {
                    Cheats.PartyLogger.Print("party invite: already a party member of {0}: {1}", new object[2]
                    {
                      (object) current,
                      (object) BnetParty.GetJoinedParty(partyId1)
                    });
                  }
                  else
                  {
                    Cheats.PartyLogger.Print("party invite: inviting {0} {1} to party {2}", new object[3]
                    {
                      (object) hearthstoneGameAccountId,
                      (object) current,
                      (object) BnetParty.GetJoinedParty(partyId1)
                    });
                    BnetParty.SendInvite(partyId1, hearthstoneGameAccountId, true);
                  }
                }
                goto label_299;
              }
            }
            else
              goto label_299;
          case 5:
            // ISSUE: reference to a compiler-generated field
            bool flag3 = partyCAnonStorey425.cmd == "accept";
            PartyInvite[] receivedInvites1 = BnetParty.GetReceivedInvites();
            if (receivedInvites1.Length == 0)
            {
              // ISSUE: reference to a compiler-generated field
              errorMsg1 = "party " + partyCAnonStorey425.cmd + ": no received party invites.";
              goto label_299;
            }
            else if (array1.Length == 0)
            {
              // ISSUE: reference to a compiler-generated field
              Cheats.PartyLogger.Print("NOTE: party {0} without any arguments will {0} all received invites.", (object) partyCAnonStorey425.cmd);
              foreach (PartyInvite partyInvite in receivedInvites1)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.PartyLogger.Print("party {0}: {1} inviteId={2} from {3} for party {4}.", (object) partyCAnonStorey425.cmd, (object) (!flag3 ? "declining" : "accepting"), (object) partyInvite.InviteId, (object) partyInvite.InviterName, (object) new PartyInfo(partyInvite.PartyId, partyInvite.PartyType));
                if (flag3)
                  BnetParty.AcceptReceivedInvite(partyInvite.InviteId);
                else
                  BnetParty.DeclineReceivedInvite(partyInvite.InviteId);
              }
              goto label_299;
            }
            else
            {
              for (int index = 0; index < array1.Length; ++index)
              {
                // ISSUE: object of a compiler-generated type is created
                // ISSUE: variable of a compiler-generated type
                Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey422 partyCAnonStorey422 = new Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey422();
                // ISSUE: reference to a compiler-generated field
                if (ulong.TryParse(array1[index], out partyCAnonStorey422.indexOrId))
                {
                  PartyInvite partyInvite;
                  // ISSUE: reference to a compiler-generated field
                  if (partyCAnonStorey422.indexOrId < (ulong) receivedInvites1.LongLength)
                  {
                    // ISSUE: reference to a compiler-generated field
                    partyInvite = receivedInvites1[partyCAnonStorey422.indexOrId];
                  }
                  else
                  {
                    // ISSUE: reference to a compiler-generated method
                    partyInvite = ((IEnumerable<PartyInvite>) receivedInvites1).FirstOrDefault<PartyInvite>(new Func<PartyInvite, bool>(partyCAnonStorey422.\u003C\u003Em__291));
                    if (partyInvite == null)
                    {
                      // ISSUE: reference to a compiler-generated field
                      Cheats.PartyLogger.Print("party {0}: unable to find received invite (id or index): {1}", new object[2]
                      {
                        (object) partyCAnonStorey425.cmd,
                        (object) array1[index]
                      });
                    }
                  }
                  if (partyInvite != null)
                  {
                    // ISSUE: reference to a compiler-generated field
                    Cheats.PartyLogger.Print("party {0}: {1} inviteId={2} from {3} for party {4}.", (object) partyCAnonStorey425.cmd, (object) (!flag3 ? "declining" : "accepting"), (object) partyInvite.InviteId, (object) partyInvite.InviterName, (object) new PartyInfo(partyInvite.PartyId, partyInvite.PartyType));
                    if (flag3)
                      BnetParty.AcceptReceivedInvite(partyInvite.InviteId);
                    else
                      BnetParty.DeclineReceivedInvite(partyInvite.InviteId);
                  }
                }
                else
                {
                  // ISSUE: reference to a compiler-generated field
                  Cheats.PartyLogger.Print("party {0}: unable to parse invite (id or index): {1}", new object[2]
                  {
                    (object) partyCAnonStorey425.cmd,
                    (object) array1[index]
                  });
                }
              }
              goto label_299;
            }
          case 6:
            PartyId partyId2 = (PartyId) null;
            if (array1.Length == 0)
            {
              // ISSUE: reference to a compiler-generated field
              Cheats.PartyLogger.Print("NOTE: party {0} without any arguments will {0} all sent invites for all parties.", (object) partyCAnonStorey425.cmd);
              PartyId[] joinedPartyIds2 = BnetParty.GetJoinedPartyIds();
              if (joinedPartyIds2.Length == 0)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.PartyLogger.Print("party {0}: no joined parties.", (object) partyCAnonStorey425.cmd);
              }
              foreach (PartyId partyId3 in joinedPartyIds2)
              {
                foreach (PartyInvite sentInvite in BnetParty.GetSentInvites(partyId3))
                {
                  // ISSUE: reference to a compiler-generated field
                  Cheats.PartyLogger.Print("party {0}: revoking inviteId={1} from {2} for party {3}.", (object) partyCAnonStorey425.cmd, (object) sentInvite.InviteId, (object) sentInvite.InviterName, (object) BnetParty.GetJoinedParty(partyId3));
                  BnetParty.RevokeSentInvite(partyId3, sentInvite.InviteId);
                }
              }
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              partyId2 = Cheats.ParsePartyId(partyCAnonStorey425.cmd, array1[0], -1, ref errorMsg1);
            }
            if (partyId2 != (PartyId) null)
            {
              PartyInfo joinedParty = BnetParty.GetJoinedParty(partyId2);
              PartyInvite[] sentInvites = BnetParty.GetSentInvites(partyId2);
              if (sentInvites.Length == 0)
              {
                // ISSUE: reference to a compiler-generated field
                errorMsg1 = "party " + partyCAnonStorey425.cmd + ": no sent invites for party " + (object) joinedParty;
                goto label_299;
              }
              else
              {
                string[] array2 = ((IEnumerable<string>) array1).Skip<string>(1).ToArray<string>();
                if (array2.Length == 0)
                {
                  // ISSUE: reference to a compiler-generated field
                  Cheats.PartyLogger.Print("NOTE: party {0} without specifying InviteId (or index) will {0} all sent invites.", (object) partyCAnonStorey425.cmd);
                  foreach (PartyInvite partyInvite in sentInvites)
                  {
                    // ISSUE: reference to a compiler-generated field
                    Cheats.PartyLogger.Print("party {0}: revoking inviteId={1} from {2} for party {3}.", (object) partyCAnonStorey425.cmd, (object) partyInvite.InviteId, (object) partyInvite.InviterName, (object) joinedParty);
                    BnetParty.RevokeSentInvite(partyId2, partyInvite.InviteId);
                  }
                  goto label_299;
                }
                else
                {
                  for (int index = 0; index < array2.Length; ++index)
                  {
                    // ISSUE: object of a compiler-generated type is created
                    // ISSUE: variable of a compiler-generated type
                    Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey423 partyCAnonStorey423 = new Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey423();
                    // ISSUE: reference to a compiler-generated field
                    if (ulong.TryParse(array2[index], out partyCAnonStorey423.indexOrId))
                    {
                      PartyInvite partyInvite;
                      // ISSUE: reference to a compiler-generated field
                      if (partyCAnonStorey423.indexOrId < (ulong) sentInvites.LongLength)
                      {
                        // ISSUE: reference to a compiler-generated field
                        partyInvite = sentInvites[partyCAnonStorey423.indexOrId];
                      }
                      else
                      {
                        // ISSUE: reference to a compiler-generated method
                        partyInvite = ((IEnumerable<PartyInvite>) sentInvites).FirstOrDefault<PartyInvite>(new Func<PartyInvite, bool>(partyCAnonStorey423.\u003C\u003Em__292));
                        if (partyInvite == null)
                        {
                          // ISSUE: reference to a compiler-generated field
                          Cheats.PartyLogger.Print("party {0}: unable to find sent invite (id or index): {1} for party {2}", new object[3]
                          {
                            (object) partyCAnonStorey425.cmd,
                            (object) array2[index],
                            (object) joinedParty
                          });
                        }
                      }
                      if (partyInvite != null)
                      {
                        // ISSUE: reference to a compiler-generated field
                        Cheats.PartyLogger.Print("party {0}: revoking inviteId={1} from {2} for party {3}.", (object) partyCAnonStorey425.cmd, (object) partyInvite.InviteId, (object) partyInvite.InviterName, (object) joinedParty);
                        BnetParty.RevokeSentInvite(partyId2, partyInvite.InviteId);
                      }
                    }
                    else
                    {
                      // ISSUE: reference to a compiler-generated field
                      Cheats.PartyLogger.Print("party {0}: unable to parse invite (id or index): {1}", new object[2]
                      {
                        (object) partyCAnonStorey425.cmd,
                        (object) array2[index]
                      });
                    }
                  }
                  goto label_299;
                }
              }
            }
            else
              goto label_299;
          case 7:
            if (array1.Length < 2)
            {
              // ISSUE: reference to a compiler-generated field
              errorMsg1 = "party " + partyCAnonStorey425.cmd + ": must specify a partyId (Hi-Lo format) and an online friend index";
              goto label_299;
            }
            else
            {
              PartyType partyType = PartyType.DEFAULT;
              foreach (string s1 in array1)
              {
                int length = s1.IndexOf('-');
                int result1 = -1;
                PartyId partyId3 = (PartyId) null;
                BnetGameAccountId whomToAskForApproval = (BnetGameAccountId) null;
                if (length >= 0)
                {
                  string s2 = s1.Substring(0, length);
                  string s3 = s1.Length <= length ? string.Empty : s1.Substring(length + 1);
                  ulong result2;
                  ulong result3;
                  if (ulong.TryParse(s2, out result2) && ulong.TryParse(s3, out result3))
                  {
                    partyId3 = new PartyId(result2, result3);
                  }
                  else
                  {
                    // ISSUE: reference to a compiler-generated field
                    errorMsg1 = "party " + partyCAnonStorey425.cmd + ": unable to parse partyId (in format Hi-Lo).";
                  }
                }
                else if (int.TryParse(s1, out result1))
                {
                  BnetPlayer[] array2 = BnetFriendMgr.Get().GetFriends().Where<BnetPlayer>((Func<BnetPlayer, bool>) (p =>
                  {
                    if (p.IsOnline())
                      return p.GetHearthstoneGameAccount() != (BnetGameAccount) null;
                    return false;
                  })).ToArray<BnetPlayer>();
                  if (result1 < 0 || result1 >= array2.Length)
                  {
                    // ISSUE: reference to a compiler-generated field
                    errorMsg1 = "party " + partyCAnonStorey425.cmd + ": no online friend at index " + (object) result1;
                  }
                  else
                    whomToAskForApproval = array2[result1].GetHearthstoneGameAccountId();
                }
                else
                {
                  // ISSUE: reference to a compiler-generated field
                  errorMsg1 = "party " + partyCAnonStorey425.cmd + ": unable to parse online friend index.";
                }
                if (partyId3 != (PartyId) null && (BnetEntityId) whomToAskForApproval != (BnetEntityId) null)
                  BnetParty.RequestInvite(partyId3, whomToAskForApproval, BnetPresenceMgr.Get().GetMyGameAccountId(), partyType);
              }
              goto label_299;
            }
          case 8:
            PartyId[] joinedPartyIds3 = BnetParty.GetJoinedPartyIds();
            if (joinedPartyIds3.Length == 0)
            {
              // ISSUE: reference to a compiler-generated field
              Cheats.PartyLogger.Print("party {0}: no joined parties.", (object) partyCAnonStorey425.cmd);
              goto label_299;
            }
            else
            {
              foreach (PartyId partyId3 in joinedPartyIds3)
              {
                foreach (InviteRequest inviteRequest in BnetParty.GetInviteRequests(partyId3))
                {
                  Cheats.PartyLogger.Print("party {0}: ignoring request to invite {0} {1} from {2} {3}.", (object) inviteRequest.TargetName, (object) inviteRequest.TargetId, (object) inviteRequest.RequesterName, (object) inviteRequest.RequesterId);
                  BnetParty.IgnoreInviteRequest(partyId3, inviteRequest.TargetId);
                }
              }
              goto label_299;
            }
          case 9:
            IEnumerable<PartyId> partyIds1 = (IEnumerable<PartyId>) null;
            int result4 = -1;
            if (array1.Length >= 2 && (!int.TryParse(array1[1], out result4) || result4 < 0))
            {
              // ISSUE: reference to a compiler-generated field
              errorMsg1 = string.Format("party {0}: invalid memberIndex={1}", (object) partyCAnonStorey425.cmd, (object) array1[1]);
            }
            if (array1.Length == 0)
            {
              // ISSUE: reference to a compiler-generated field
              Cheats.PartyLogger.Print("NOTE: party {0} without any arguments will {0} to first member in all parties.", (object) partyCAnonStorey425.cmd);
              PartyId[] joinedPartyIds2 = BnetParty.GetJoinedPartyIds();
              if (joinedPartyIds2.Length == 0)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.PartyLogger.Print("party {0}: no joined parties.", (object) partyCAnonStorey425.cmd);
              }
              else
                partyIds1 = (IEnumerable<PartyId>) joinedPartyIds2;
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              PartyId partyId3 = Cheats.ParsePartyId(partyCAnonStorey425.cmd, array1[0], -1, ref errorMsg1);
              if (partyId3 != (PartyId) null)
                partyIds1 = (IEnumerable<PartyId>) new PartyId[1]
                {
                  partyId3
                };
            }
            if (partyIds1 != null)
            {
              using (IEnumerator<PartyId> enumerator = partyIds1.GetEnumerator())
              {
                while (enumerator.MoveNext())
                {
                  PartyId current = enumerator.Current;
                  PartyMember[] members = BnetParty.GetMembers(current);
                  if (result4 >= 0)
                  {
                    if (result4 >= members.Length)
                    {
                      // ISSUE: reference to a compiler-generated field
                      Cheats.PartyLogger.Print("party {0}: party={1} has no member at index={2}", new object[3]
                      {
                        (object) partyCAnonStorey425.cmd,
                        (object) BnetParty.GetJoinedParty(current),
                        (object) result4
                      });
                    }
                    else
                    {
                      PartyMember partyMember = members[result4];
                      BnetParty.SetLeader(current, partyMember.GameAccountId);
                    }
                  }
                  else if (((IEnumerable<PartyMember>) members).Any<PartyMember>((Func<PartyMember, bool>) (m => (BnetEntityId) m.GameAccountId != (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId())))
                  {
                    BnetParty.SetLeader(current, ((IEnumerable<PartyMember>) members).First<PartyMember>((Func<PartyMember, bool>) (m => (BnetEntityId) m.GameAccountId != (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId())).GameAccountId);
                  }
                  else
                  {
                    // ISSUE: reference to a compiler-generated field
                    Cheats.PartyLogger.Print("party {0}: party={1} has no member not myself to set as leader.", new object[2]
                    {
                      (object) partyCAnonStorey425.cmd,
                      (object) BnetParty.GetJoinedParty(current)
                    });
                  }
                }
                goto label_299;
              }
            }
            else
              goto label_299;
          case 10:
            PartyId partyId4 = (PartyId) null;
            if (array1.Length == 0)
            {
              // ISSUE: reference to a compiler-generated field
              Cheats.PartyLogger.Print("NOTE: party {0} without any arguments will {0} all members for all parties (other than self).", (object) partyCAnonStorey425.cmd);
              PartyId[] joinedPartyIds2 = BnetParty.GetJoinedPartyIds();
              if (joinedPartyIds2.Length == 0)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.PartyLogger.Print("party {0}: no joined parties.", (object) partyCAnonStorey425.cmd);
              }
              foreach (PartyId partyId3 in joinedPartyIds2)
              {
                foreach (PartyMember member in BnetParty.GetMembers(partyId3))
                {
                  if (!((BnetEntityId) member.GameAccountId == (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId()))
                  {
                    // ISSUE: reference to a compiler-generated field
                    Cheats.PartyLogger.Print("party {0}: kicking memberId={1} from party {2}.", new object[3]
                    {
                      (object) partyCAnonStorey425.cmd,
                      (object) member.GameAccountId,
                      (object) BnetParty.GetJoinedParty(partyId3)
                    });
                    BnetParty.KickMember(partyId3, member.GameAccountId);
                  }
                }
              }
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              partyId4 = Cheats.ParsePartyId(partyCAnonStorey425.cmd, array1[0], -1, ref errorMsg1);
            }
            if (partyId4 != (PartyId) null)
            {
              PartyInfo joinedParty = BnetParty.GetJoinedParty(partyId4);
              PartyMember[] members = BnetParty.GetMembers(partyId4);
              if (members.Length == 1)
              {
                // ISSUE: reference to a compiler-generated field
                errorMsg1 = "party " + partyCAnonStorey425.cmd + ": no members (other than self) for party " + (object) joinedParty;
                goto label_299;
              }
              else
              {
                string[] array2 = ((IEnumerable<string>) array1).Skip<string>(1).ToArray<string>();
                if (array2.Length == 0)
                {
                  // ISSUE: reference to a compiler-generated field
                  Cheats.PartyLogger.Print("NOTE: party {0} without specifying member index will {0} all members (other than self).", (object) partyCAnonStorey425.cmd);
                  foreach (PartyMember partyMember in members)
                  {
                    if (!((BnetEntityId) partyMember.GameAccountId == (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId()))
                    {
                      // ISSUE: reference to a compiler-generated field
                      Cheats.PartyLogger.Print("party {0}: kicking memberId={1} from party {2}.", new object[3]
                      {
                        (object) partyCAnonStorey425.cmd,
                        (object) partyMember.GameAccountId,
                        (object) joinedParty
                      });
                      BnetParty.KickMember(partyId4, partyMember.GameAccountId);
                    }
                  }
                  goto label_299;
                }
                else
                {
                  for (int index = 0; index < array2.Length; ++index)
                  {
                    // ISSUE: object of a compiler-generated type is created
                    // ISSUE: variable of a compiler-generated type
                    Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey424 partyCAnonStorey424 = new Cheats.\u003COnProcessCheat_party\u003Ec__AnonStorey424();
                    // ISSUE: reference to a compiler-generated field
                    if (ulong.TryParse(array2[index], out partyCAnonStorey424.indexOrId))
                    {
                      PartyMember partyMember;
                      // ISSUE: reference to a compiler-generated field
                      if (partyCAnonStorey424.indexOrId < (ulong) members.LongLength)
                      {
                        // ISSUE: reference to a compiler-generated field
                        partyMember = members[partyCAnonStorey424.indexOrId];
                      }
                      else
                      {
                        // ISSUE: reference to a compiler-generated method
                        partyMember = ((IEnumerable<PartyMember>) members).FirstOrDefault<PartyMember>(new Func<PartyMember, bool>(partyCAnonStorey424.\u003C\u003Em__296));
                        if (partyMember == null)
                        {
                          // ISSUE: reference to a compiler-generated field
                          Cheats.PartyLogger.Print("party {0}: unable to find member (id or index): {1} for party {2}", new object[3]
                          {
                            (object) partyCAnonStorey425.cmd,
                            (object) array2[index],
                            (object) joinedParty
                          });
                        }
                      }
                      if (partyMember != null)
                      {
                        if ((BnetEntityId) partyMember.GameAccountId == (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId())
                        {
                          // ISSUE: reference to a compiler-generated field
                          Cheats.PartyLogger.Print("party {0}: cannot kick yourself (argIndex={1}); party={2}", new object[3]
                          {
                            (object) partyCAnonStorey425.cmd,
                            (object) index,
                            (object) joinedParty
                          });
                        }
                        else
                        {
                          // ISSUE: reference to a compiler-generated field
                          Cheats.PartyLogger.Print("party {0}: kicking memberId={1} from party {2}.", new object[3]
                          {
                            (object) partyCAnonStorey425.cmd,
                            (object) partyMember.GameAccountId,
                            (object) joinedParty
                          });
                          BnetParty.KickMember(partyId4, partyMember.GameAccountId);
                        }
                      }
                    }
                    else
                    {
                      // ISSUE: reference to a compiler-generated field
                      Cheats.PartyLogger.Print("party {0}: unable to parse member (id or index): {1}", new object[2]
                      {
                        (object) partyCAnonStorey425.cmd,
                        (object) array2[index]
                      });
                    }
                  }
                  goto label_299;
                }
              }
            }
            else
              goto label_299;
          case 11:
            PartyId partyId5 = (PartyId) null;
            if (array1.Length < 2)
            {
              errorMsg1 = "party setprivacy: must specify a party (index or LowBits or type) and a PrivacyLevel: " + string.Join(" | ", Enum.GetValues(typeof (PrivacyLevel)).Cast<PrivacyLevel>().Select<PrivacyLevel, string>((Func<PrivacyLevel, string>) (v => ((int) v).ToString() + " (" + (object) v + ")")).ToArray<string>());
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              partyId5 = Cheats.ParsePartyId(partyCAnonStorey425.cmd, array1[0], -1, ref errorMsg1);
            }
            if (partyId5 != (PartyId) null)
            {
              PrivacyLevel? nullable = new PrivacyLevel?();
              int result1;
              if (int.TryParse(array1[1], out result1))
              {
                nullable = new PrivacyLevel?((PrivacyLevel) result1);
              }
              else
              {
                PrivacyLevel outVal;
                if (!EnumUtils.TryGetEnum<PrivacyLevel>(array1[1], out outVal))
                  errorMsg1 = "party setprivacy: unknown PrivacyLevel specified: " + array1[1];
                else
                  nullable = new PrivacyLevel?(outVal);
              }
              if (nullable.HasValue)
              {
                Cheats.PartyLogger.Print("party setprivacy: setting PrivacyLevel={0} for party {1}.", new object[2]
                {
                  (object) nullable.Value,
                  (object) BnetParty.GetJoinedParty(partyId5)
                });
                BnetParty.SetPrivacy(partyId5, nullable.Value);
                goto label_299;
              }
              else
                goto label_299;
            }
            else
              goto label_299;
          case 12:
            // ISSUE: reference to a compiler-generated field
            bool flag4 = partyCAnonStorey425.cmd == "setlong";
            // ISSUE: reference to a compiler-generated field
            bool flag5 = partyCAnonStorey425.cmd == "setstring";
            // ISSUE: reference to a compiler-generated field
            bool flag6 = partyCAnonStorey425.cmd == "setblob";
            int index1 = 1;
            PartyId partyId6 = (PartyId) null;
            if (array1.Length < 2)
            {
              // ISSUE: reference to a compiler-generated field
              errorMsg1 = "party " + partyCAnonStorey425.cmd + ": must specify attributeKey and a value.";
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              partyId6 = Cheats.ParsePartyId(partyCAnonStorey425.cmd, array1[0], -1, ref errorMsg1);
              if (partyId6 == (PartyId) null)
              {
                PartyId[] joinedPartyIds2 = BnetParty.GetJoinedPartyIds();
                if (joinedPartyIds2.Length > 0)
                {
                  // ISSUE: reference to a compiler-generated field
                  Cheats.PartyLogger.Print("party {0}: treating first argument as attributeKey (and not PartyId) - will use PartyId at index 0", (object) partyCAnonStorey425.cmd);
                  errorMsg1 = (string) null;
                  partyId6 = joinedPartyIds2[0];
                }
              }
              else
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.PartyLogger.Print("party {0}: treating first argument as PartyId (second argument will be attributeKey)", (object) partyCAnonStorey425.cmd);
              }
            }
            if (partyId6 != (PartyId) null)
            {
              bool flag7 = false;
              string attributeKey = array1[index1];
              string s = string.Join(" ", ((IEnumerable<string>) array1).Skip<string>(index1 + 1).ToArray<string>());
              if (flag4)
              {
                long result1;
                if (long.TryParse(s, out result1))
                {
                  BnetParty.SetPartyAttributeLong(partyId6, attributeKey, result1);
                  flag7 = true;
                }
              }
              else if (flag5)
              {
                BnetParty.SetPartyAttributeString(partyId6, attributeKey, s);
                flag7 = true;
              }
              else if (flag6)
              {
                byte[] bytes = Encoding.UTF8.GetBytes(s);
                BnetParty.SetPartyAttributeBlob(partyId6, attributeKey, bytes);
                flag7 = true;
              }
              else
              {
                // ISSUE: reference to a compiler-generated field
                errorMsg1 = "party " + partyCAnonStorey425.cmd + ": unhandled attribute type!";
              }
              if (flag7)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.PartyLogger.Print("party {0}: complete key={1} val={2} party={3}", (object) partyCAnonStorey425.cmd, (object) attributeKey, (object) s, (object) BnetParty.GetJoinedParty(partyId6));
                goto label_299;
              }
              else
                goto label_299;
            }
            else
              goto label_299;
          case 13:
            PartyId partyId7 = (PartyId) null;
            if (array1.Length < 2)
            {
              // ISSUE: reference to a compiler-generated field
              errorMsg1 = "party " + partyCAnonStorey425.cmd + ": must specify attributeKey.";
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              partyId7 = Cheats.ParsePartyId(partyCAnonStorey425.cmd, array1[0], -1, ref errorMsg1);
              if (partyId7 == (PartyId) null)
              {
                PartyId[] joinedPartyIds2 = BnetParty.GetJoinedPartyIds();
                if (joinedPartyIds2.Length > 0)
                {
                  // ISSUE: reference to a compiler-generated field
                  Cheats.PartyLogger.Print("party {0}: treating first argument as attributeKey (and not PartyId) - will use PartyId at index 0", (object) partyCAnonStorey425.cmd);
                  errorMsg1 = (string) null;
                  partyId7 = joinedPartyIds2[0];
                }
              }
              else
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.PartyLogger.Print("party {0}: treating first argument as PartyId (second argument will be attributeKey)", (object) partyCAnonStorey425.cmd);
              }
            }
            if (partyId7 != (PartyId) null)
            {
              string attributeKey = array1[1];
              BnetParty.ClearPartyAttribute(partyId7, attributeKey);
              // ISSUE: reference to a compiler-generated field
              Cheats.PartyLogger.Print("party {0}: cleared key={1} party={2}", new object[3]
              {
                (object) partyCAnonStorey425.cmd,
                (object) attributeKey,
                (object) BnetParty.GetJoinedParty(partyId7)
              });
              goto label_299;
            }
            else
              goto label_299;
          case 14:
            IEnumerable<PartyId> partyIds2 = (IEnumerable<PartyId>) null;
            if (array1.Length == 0)
            {
              PartyInfo[] joinedParties = BnetParty.GetJoinedParties();
              if (joinedParties.Length == 0)
              {
                Cheats.PartyLogger.Print("party list: no joined parties.");
              }
              else
              {
                Cheats.PartyLogger.Print("party list: listing all joined parties and the details of the party at index 0.");
                partyIds2 = (IEnumerable<PartyId>) new PartyId[1]
                {
                  joinedParties[0].Id
                };
              }
              for (int index2 = 0; index2 < joinedParties.Length; ++index2)
                Cheats.PartyLogger.Print("   {0}", (object) Cheats.GetPartySummary(joinedParties[index2], index2));
            }
            else
            {
              // ISSUE: reference to a compiler-generated method
              partyIds2 = ((IEnumerable<string>) array1).Select<string, PartyId>(new Func<string, int, PartyId>(partyCAnonStorey425.\u003C\u003Em__298)).Where<PartyId>((Func<PartyId, bool>) (p => p != (PartyId) null));
            }
            if (partyIds2 != null)
            {
              int index2 = -1;
              foreach (PartyId partyId3 in partyIds2)
              {
                ++index2;
                PartyInfo joinedParty = BnetParty.GetJoinedParty(partyId3);
                // ISSUE: reference to a compiler-generated field
                Cheats.PartyLogger.Print("party {0}: {1}", new object[2]
                {
                  (object) partyCAnonStorey425.cmd,
                  (object) Cheats.GetPartySummary(BnetParty.GetJoinedParty(partyId3), index2)
                });
                PartyMember[] members = BnetParty.GetMembers(partyId3);
                if (members.Length == 0)
                  Cheats.PartyLogger.Print("   no members.");
                else
                  Cheats.PartyLogger.Print("   members:");
                for (int index3 = 0; index3 < members.Length; ++index3)
                {
                  bool flag7 = (BnetEntityId) members[index3].GameAccountId == (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId();
                  Cheats.PartyLogger.Print("      [{0}] {1} isMyself={2} isLeader={3} roleIds={4}", (object) index3, (object) members[index3].GameAccountId, (object) flag7, (object) members[index3].IsLeader(joinedParty.Type), (object) string.Join(",", ((IEnumerable<uint>) members[index3].RoleIds).Select<uint, string>((Func<uint, string>) (r => r.ToString())).ToArray<string>()));
                }
                PartyInvite[] sentInvites = BnetParty.GetSentInvites(partyId3);
                if (sentInvites.Length == 0)
                  Cheats.PartyLogger.Print("   no sent invites.");
                else
                  Cheats.PartyLogger.Print("   sent invites:");
                for (int index3 = 0; index3 < sentInvites.Length; ++index3)
                  Cheats.PartyLogger.Print("      {0}", (object) Cheats.GetPartyInviteSummary(sentInvites[index3], index3));
                KeyValuePair<string, object>[] allPartyAttributes = BnetParty.GetAllPartyAttributes(partyId3);
                if (allPartyAttributes.Length == 0)
                  Cheats.PartyLogger.Print("   no party attributes.");
                else
                  Cheats.PartyLogger.Print("   party attributes:");
                for (int index3 = 0; index3 < allPartyAttributes.Length; ++index3)
                {
                  KeyValuePair<string, object> keyValuePair = allPartyAttributes[index3];
                  string str1 = keyValuePair.Value != null ? string.Format("[{0}]{1}", (object) keyValuePair.Value.GetType().Name, (object) keyValuePair.Value.ToString()) : "<null>";
                  if (keyValuePair.Value is byte[])
                  {
                    byte[] bytes = (byte[]) keyValuePair.Value;
                    str1 = "blobLength=" + (object) bytes.Length;
                    try
                    {
                      string str2 = Encoding.UTF8.GetString(bytes);
                      if (str2 != null)
                        str1 = str1 + " decodedUtf8=" + str2;
                    }
                    catch (ArgumentException ex)
                    {
                    }
                  }
                  Cheats.PartyLogger.Print("      {0}={1}", new object[2]
                  {
                    (object) (keyValuePair.Key ?? "<null>"),
                    (object) str1
                  });
                }
              }
            }
            PartyInvite[] receivedInvites2 = BnetParty.GetReceivedInvites();
            if (receivedInvites2.Length == 0)
              Cheats.PartyLogger.Print("party list: no received party invites.");
            else
              Cheats.PartyLogger.Print("party list: received party invites:");
            for (int index2 = 0; index2 < receivedInvites2.Length; ++index2)
              Cheats.PartyLogger.Print("   {0}", (object) Cheats.GetPartyInviteSummary(receivedInvites2[index2], index2));
            goto label_299;
        }
      }
    }
    // ISSUE: reference to a compiler-generated field
    errorMsg1 = "party: unknown party cmd: " + partyCAnonStorey425.cmd;
label_299:
    if (errorMsg1 != null)
    {
      Cheats.PartyLogger.Print(errorMsg1);
      Error.AddWarning("Party Cheat Error", errorMsg1);
      flag1 = false;
    }
    return flag1;
  }

  private static string GetPartyInviteSummary(PartyInvite invite, int index)
  {
    return string.Format("{0}: inviteId={1} sender={2} recipient={3} party={4}", (object) (index < 0 ? string.Empty : string.Format("[{0}] ", (object) index)), (object) invite.InviteId, (object) (invite.InviterId.ToString() + " " + invite.InviterName), (object) invite.InviteeId, (object) new PartyInfo(invite.PartyId, invite.PartyType));
  }

  private static string GetPartySummary(PartyInfo info, int index)
  {
    PartyMember leader = BnetParty.GetLeader(info.Id);
    return string.Format("{0}{1}: members={2} invites={3} privacy={4} leader={5}", (object) (index < 0 ? string.Empty : string.Format("[{0}] ", (object) index)), (object) info, (object) (BnetParty.CountMembers(info.Id).ToString() + (!BnetParty.IsPartyFull(info.Id, true) ? (object) string.Empty : (object) "(full)")), (object) BnetParty.GetSentInvites(info.Id).Length, (object) BnetParty.GetPrivacyLevel(info.Id), (object) (leader != null ? leader.GameAccountId.ToString() : "null"));
  }

  private bool OnProcessCheat_cheat(string func, string[] args, string rawArgs)
  {
    Network.SendDebugConsoleCommand(rawArgs);
    return true;
  }

  private bool OnProcessCheat_autohand(string func, string[] args, string rawArgs)
  {
    bool boolVal;
    if (args.Length == 0 || !GeneralUtils.TryParseBool(args[0], out boolVal) || (UnityEngine.Object) InputManager.Get() == (UnityEngine.Object) null)
      return false;
    string message = !boolVal ? "auto hand hiding is off" : "auto hand hiding is on";
    UnityEngine.Debug.Log((object) message);
    UIStatus.Get().AddInfo(message);
    InputManager.Get().SetHideHandAfterPlayingCard(boolVal);
    return true;
  }

  private bool OnProcessCheat_adventureChallengeUnlock(string func, string[] args, string rawArgs)
  {
    int result;
    if (args.Length < 1 || !int.TryParse(args[0].ToLowerInvariant(), out result))
      return false;
    AdventureMissionDisplay.Get().ShowClassChallengeUnlock(new List<int>()
    {
      result
    });
    return true;
  }

  private bool OnProcessCheat_iks(string func, string[] args, string rawArgs)
  {
    if (args.Length < 1)
      return false;
    InnKeepersSpecial.Get().adUrlOverride = args[0];
    WelcomeQuests.Show(UserAttentionBlocker.ALL, true, (WelcomeQuests.DelOnWelcomeQuestsClosed) null, false);
    return true;
  }

  private bool OnProcessCheat_quote(string func, string[] args, string rawArgs)
  {
    if (args.Length < 2)
      return false;
    string prefabName = args[0];
    string key = args[1];
    string soundName = key;
    if (args.Length > 2)
      soundName = args[2];
    if (prefabName.ToLowerInvariant().Contains("innkeeper"))
    {
      NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.ALL, NotificationManager.DEFAULT_CHARACTER_POS, GameStrings.Get(key), soundName, 0.0f, (Action) null, false);
    }
    else
    {
      if (!prefabName.EndsWith("_Quote"))
        prefabName += "_Quote";
      NotificationManager.Get().CreateCharacterQuote(prefabName, NotificationManager.DEFAULT_CHARACTER_POS, GameStrings.Get(key), soundName, true, 0.0f, (Action) null, CanvasAnchor.BOTTOM_LEFT);
    }
    return true;
  }

  private bool OnProcessCheat_popuptext(string func, string[] args, string rawArgs)
  {
    if (args.Length < 1)
      return false;
    string text = args[0];
    NotificationManager.Get().CreatePopupText(UserAttentionBlocker.ALL, Box.Get().m_LeftDoor.transform.position, TutorialEntity.HELP_POPUP_SCALE, text, true);
    return true;
  }

  private bool OnProcessCheat_demotext(string func, string[] args, string rawArgs)
  {
    if (args.Length < 1)
      return false;
    DemoMgr.Get().CreateDemoText(args[0]);
    return true;
  }

  private bool OnProcessCheat_favoritehero(string func, string[] args, string rawArgs)
  {
    if (!(SceneMgr.Get().GetScene() is CollectionManagerScene))
    {
      UnityEngine.Debug.LogWarning((object) "OnProcessCheat_favoritehero must be used from the CollectionManagaer!");
      return false;
    }
    int result1;
    TAG_CLASS outVal1;
    if (args.Length != 3 || !int.TryParse(args[0].ToLowerInvariant(), out result1) || !EnumUtils.TryCast<TAG_CLASS>((object) result1, out outVal1))
      return false;
    string str = args[1];
    int result2;
    TAG_PREMIUM outVal2;
    if (!int.TryParse(args[2].ToLowerInvariant(), out result2) || !EnumUtils.TryCast<TAG_PREMIUM>((object) result2, out outVal2))
      return false;
    NetCache.CardDefinition hero = new NetCache.CardDefinition() { Name = str, Premium = outVal2 };
    Log.Rachelle.Print("OnProcessCheat_favoritehero setting favorite hero to {0} for class {1}", new object[2]
    {
      (object) hero,
      (object) outVal1
    });
    Network.SetFavoriteHero(outVal1, hero);
    return true;
  }

  private bool OnProcessCheat_help(string func, string[] args, string rawArgs)
  {
    StringBuilder stringBuilder = new StringBuilder();
    string key = (string) null;
    if (args.Length > 0 && !string.IsNullOrEmpty(args[0]))
      key = args[0];
    List<string> stringList = new List<string>();
    if (key != null)
    {
      using (Map<string, List<CheatMgr.ProcessCheatCallback>>.KeyCollection.Enumerator enumerator = CheatMgr.Get().GetCheatCommands().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          if (current.Contains(key))
            stringList.Add(current);
        }
      }
    }
    else
    {
      using (Map<string, List<CheatMgr.ProcessCheatCallback>>.KeyCollection.Enumerator enumerator = CheatMgr.Get().GetCheatCommands().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          stringList.Add(current);
        }
      }
    }
    UnityEngine.Debug.Log((object) ("found commands " + (object) stringList + " " + (object) stringList.Count));
    if (stringList.Count == 1)
      key = stringList[0];
    if (key == null || stringList.Count != 1)
    {
      if (key == null)
        stringBuilder.Append("All available cheat commands:\n");
      else
        stringBuilder.Append("Cheat commands containing: \"" + key + "\"\n");
      int num = 0;
      string str = string.Empty;
      using (List<string>.Enumerator enumerator = stringList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          str = str + current + ", ";
          ++num;
          if (num > 4)
          {
            num = 0;
            stringBuilder.Append(str);
            str = string.Empty;
          }
        }
      }
      if (!string.IsNullOrEmpty(str))
        stringBuilder.Append(str);
      UIStatus.Get().AddInfo(stringBuilder.ToString(), 10f);
    }
    else
    {
      string empty1 = string.Empty;
      CheatMgr.Get().cheatDesc.TryGetValue(key, out empty1);
      string empty2 = string.Empty;
      CheatMgr.Get().cheatArgs.TryGetValue(key, out empty2);
      stringBuilder.Append("Usage: ");
      stringBuilder.Append(key);
      if (!string.IsNullOrEmpty(empty2))
        stringBuilder.Append(" " + empty2);
      if (!string.IsNullOrEmpty(empty1))
        stringBuilder.Append("\n(" + empty1 + ")");
      UIStatus.Get().AddInfo(stringBuilder.ToString(), 10f);
    }
    return true;
  }

  private bool OnProcessCheat_fixedrewardcomplete(string func, string[] args, string rawArgs)
  {
    int val;
    if (args.Length < 1 || string.IsNullOrEmpty(args[0]) || !GeneralUtils.TryParseInt(args[0], out val))
      return false;
    return FixedRewardsMgr.Get().Cheat_ShowFixedReward(val, new FixedRewardsMgr.DelPositionNonToastReward(this.PositionLoginFixedReward), PopupDisplayManager.Get().GetRewardPunchScale(), PopupDisplayManager.Get().GetRewardScale());
  }

  private void PositionLoginFixedReward(Reward reward)
  {
    Scene scene = SceneMgr.Get().GetScene();
    reward.transform.parent = scene.transform;
    reward.transform.localRotation = Quaternion.identity;
    reward.transform.localPosition = PopupDisplayManager.Get().GetRewardLocalPos();
  }

  private bool OnProcessCheat_example(string func, string[] args, string rawArgs)
  {
    if (args.Length < 1 || string.IsNullOrEmpty(args[0]))
      return false;
    string key = args[0];
    string empty = string.Empty;
    if (!CheatMgr.Get().cheatExamples.TryGetValue(key, out empty))
      return false;
    CheatMgr.Get().ProcessCheat(key + " " + empty);
    return true;
  }

  private bool OnProcessCheat_tavernbrawl(string func, string[] args, string rawArgs)
  {
    string message1 = "USAGE: tb [cmd] [args]\nCommands: view, get, set, refresh, scenario, reset";
    if (args.Length < 1 || ((IEnumerable<string>) args).Any<string>((Func<string, bool>) (a => string.IsNullOrEmpty(a))))
    {
      UIStatus.Get().AddInfo(message1, 10f);
      return true;
    }
    string str1 = args[0];
    string[] array = ((IEnumerable<string>) args).Skip<string>(1).ToArray<string>();
    string message2 = (string) null;
    string key = str1;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapC0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        Cheats.\u003C\u003Ef__switch\u0024mapC0 = new Dictionary<string, int>(10)
        {
          {
            "help",
            0
          },
          {
            "reset",
            1
          },
          {
            "refresh",
            2
          },
          {
            "fake_active_session",
            3
          },
          {
            "do_rewards",
            4
          },
          {
            "get",
            5
          },
          {
            "set",
            5
          },
          {
            "view",
            6
          },
          {
            "scen",
            7
          },
          {
            "scenario",
            7
          }
        };
      }
      int num1;
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapC0.TryGetValue(key, out num1))
      {
        switch (num1)
        {
          case 0:
            message2 = "usage";
            break;
          case 1:
            if (array.Length == 0)
            {
              message2 = "Please specify what to reset: seen, toserver";
              break;
            }
            if ("toserver".Equals(array[0], StringComparison.InvariantCultureIgnoreCase))
            {
              if (TavernBrawlManager.Get().IsCheated)
              {
                TavernBrawlManager.Get().Cheat_ResetToServerData();
                TavernBrawlMission tavernBrawlMission = TavernBrawlManager.Get().CurrentMission();
                message2 = tavernBrawlMission != null ? "TB settings reset to server-specified Scenario ID " + (object) tavernBrawlMission.missionId : "TB settings reset to server-specified Scenario ID <null>";
                break;
              }
              message2 = "TB not locally cheated. Already using server-specified data.";
              break;
            }
            if ("seen".Equals(array[0], StringComparison.InvariantCultureIgnoreCase))
            {
              int result = 0;
              if (array.Length > 1 && !int.TryParse(array[1], out result))
                message2 = "Error parsing new seen value: " + array[1];
              if (message2 == null)
              {
                TavernBrawlManager.Get().Cheat_ResetSeenStuff(result);
                message2 = "all \"seentb*\" client-options reset to " + (object) result;
                break;
              }
              break;
            }
            message2 = "Unknown reset parameter: " + array[0];
            break;
          case 2:
            TavernBrawlManager.Get().RefreshServerData();
            message2 = "TB refreshing";
            break;
          case 3:
            int result1 = 0;
            int.TryParse(args[1], out result1);
            TavernBrawlManager.Get().Cheat_SetActiveSession(result1);
            message2 = "Fake Tavern Brawl Session set.";
            break;
          case 4:
            int result2 = 0;
            int.TryParse(args[1], out result2);
            TavernBrawlManager.Get().Cheat_DoHeroicRewards(result2);
            message2 = "Doing reward animation and ending fake session if one exists.";
            break;
          case 5:
            bool flag = str1 == "set";
            string str2 = ((IEnumerable<string>) array).FirstOrDefault<string>();
            if (string.IsNullOrEmpty(str2))
            {
              message2 = string.Format("Please specify a TB variable to {0}. Variables:RefreshTime", (object) str1);
              break;
            }
            string str3 = (string) null;
            string lower = str2.ToLower();
            if (lower != null)
            {
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapBF == null)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.\u003C\u003Ef__switch\u0024mapBF = new Dictionary<string, int>(3)
                {
                  {
                    "refreshtime",
                    0
                  },
                  {
                    "wins",
                    1
                  },
                  {
                    "losses",
                    2
                  }
                };
              }
              int num2;
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapBF.TryGetValue(lower, out num2))
              {
                switch (num2)
                {
                  case 0:
                    if (flag)
                    {
                      message2 = "cannot set RefreshTime";
                      break;
                    }
                    if (TavernBrawlManager.Get().IsRefreshingTavernBrawlInfo)
                    {
                      message2 = "refreshing right now";
                      break;
                    }
                    str3 = ((double) TavernBrawlManager.Get().ScheduledSecondsToRefresh).ToString() + " secs";
                    break;
                  case 1:
                    int result3 = 0;
                    int.TryParse(args[2], out result3);
                    TavernBrawlManager.Get().Cheat_SetWins(result3);
                    message2 = string.Format("tb set wins {0} successful", (object) result3);
                    break;
                  case 2:
                    int result4 = 0;
                    int.TryParse(args[2], out result4);
                    TavernBrawlManager.Get().Cheat_SetLosses(result4);
                    message2 = string.Format("tb set losses {0} successful", (object) result4);
                    break;
                }
              }
            }
            if (flag)
            {
              message2 = string.Format("tb set {0} {1} successful.", (object) str2, array.Length < 2 ? (object) "null" : (object) array[1]);
              break;
            }
            if (string.IsNullOrEmpty(message2))
            {
              message2 = string.Format("tb variable {0}: {1}", (object) str2, (object) (str3 ?? "null"));
              break;
            }
            break;
          case 6:
            TavernBrawlMission tavernBrawlMission1 = TavernBrawlManager.Get().CurrentMission();
            if (tavernBrawlMission1 == null)
            {
              message2 = "No active Tavern Brawl at this time.";
              break;
            }
            string str4 = string.Empty;
            string str5 = string.Empty;
            ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(tavernBrawlMission1.missionId);
            if (record != null)
            {
              str4 = (string) record.Name;
              str5 = (string) record.Description;
            }
            message2 = string.Format("Active TB: [{0}] {1}\n{2}", (object) tavernBrawlMission1.missionId, (object) str4, (object) str5);
            break;
          case 7:
            if (array.Length < 1)
            {
              message2 = "tb scenario: requires an ID parameter";
              break;
            }
            int result5;
            if (!int.TryParse(array[0], out result5))
              message2 = "tb scenario: invalid non-integer Scenario ID " + array[0];
            if (message2 == null)
            {
              TavernBrawlManager.Get().Cheat_SetScenario(result5);
              break;
            }
            break;
        }
      }
    }
    if (message2 != null)
      UIStatus.Get().AddInfo(message2, 5f);
    return true;
  }

  private bool OnProcessCheat_utilservercmd(string func, string[] args, string rawArgs)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Cheats.\u003COnProcessCheat_utilservercmd\u003Ec__AnonStorey426 utilservercmdCAnonStorey426 = new Cheats.\u003COnProcessCheat_utilservercmd\u003Ec__AnonStorey426();
    if (args.Length < 1 || string.IsNullOrEmpty(rawArgs))
    {
      UIStatus.Get().AddError("Must specify a sub-command.", -1f);
      return true;
    }
    // ISSUE: reference to a compiler-generated field
    utilservercmdCAnonStorey426.cmd = args[0].ToLower();
    // ISSUE: reference to a compiler-generated field
    utilservercmdCAnonStorey426.cmdArgs = ((IEnumerable<string>) args).Skip<string>(1).ToArray<string>();
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    string str = utilservercmdCAnonStorey426.cmdArgs.Length != 0 ? utilservercmdCAnonStorey426.cmdArgs[0].ToLower() : (string) null;
    // ISSUE: reference to a compiler-generated method
    AlertPopup.ResponseCallback responseCallback = new AlertPopup.ResponseCallback(utilservercmdCAnonStorey426.\u003C\u003Em__29C);
    bool flag = true;
    // ISSUE: reference to a compiler-generated field
    string cmd = utilservercmdCAnonStorey426.cmd;
    if (cmd != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapC4 == null)
      {
        // ISSUE: reference to a compiler-generated field
        Cheats.\u003C\u003Ef__switch\u0024mapC4 = new Dictionary<string, int>(6)
        {
          {
            "help",
            0
          },
          {
            "tb",
            1
          },
          {
            "arena",
            2
          },
          {
            "ranked",
            3
          },
          {
            "deck",
            4
          },
          {
            "banner",
            5
          }
        };
      }
      int num1;
      // ISSUE: reference to a compiler-generated field
      if (Cheats.\u003C\u003Ef__switch\u0024mapC4.TryGetValue(cmd, out num1))
      {
        switch (num1)
        {
          case 0:
            flag = false;
            break;
          case 1:
            string key1 = str;
            if (key1 != null)
            {
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapC1 == null)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.\u003C\u003Ef__switch\u0024mapC1 = new Dictionary<string, int>(4)
                {
                  {
                    "help",
                    0
                  },
                  {
                    "view",
                    0
                  },
                  {
                    "list",
                    0
                  },
                  {
                    "reset",
                    1
                  }
                };
              }
              int num2;
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapC1.TryGetValue(key1, out num2))
              {
                if (num2 != 0)
                {
                  if (num2 == 1)
                  {
                    // ISSUE: reference to a compiler-generated field
                    // ISSUE: reference to a compiler-generated field
                    flag = (utilservercmdCAnonStorey426.cmdArgs.Length >= 2 ? utilservercmdCAnonStorey426.cmdArgs[1].ToLower() : (string) null) != "help";
                    break;
                  }
                  break;
                }
                flag = false;
                break;
              }
              break;
            }
            break;
          case 2:
            flag = false;
            break;
          case 3:
            flag = false;
            string key2 = str;
            if (key2 != null)
            {
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapC2 == null)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.\u003C\u003Ef__switch\u0024mapC2 = new Dictionary<string, int>(2)
                {
                  {
                    "seasonroll",
                    0
                  },
                  {
                    "seasonreset",
                    0
                  }
                };
              }
              int num2;
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapC2.TryGetValue(key2, out num2) && num2 == 0)
              {
                flag = true;
                break;
              }
              break;
            }
            break;
          case 4:
            string key3 = str;
            if (key3 != null)
            {
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapC3 == null)
              {
                // ISSUE: reference to a compiler-generated field
                Cheats.\u003C\u003Ef__switch\u0024mapC3 = new Dictionary<string, int>(2)
                {
                  {
                    "view",
                    0
                  },
                  {
                    "test",
                    0
                  }
                };
              }
              int num2;
              // ISSUE: reference to a compiler-generated field
              if (Cheats.\u003C\u003Ef__switch\u0024mapC3.TryGetValue(key3, out num2) && num2 == 0)
              {
                flag = false;
                break;
              }
              break;
            }
            break;
          case 5:
            flag = false;
            if (string.IsNullOrEmpty(str) || str == "help")
            {
              UIStatus.Get().AddInfo("Usage: util banner <list | reset bannerId=#>\n\nClear seen banners (wooden signs at login) with IDs >= bannerId arg. If no parameters, clears out just latest known bannerId. If bannerId=0, all seen banners are cleared.", 5f);
              return true;
            }
            if (str == "list")
            {
              this.Cheat_ShowBannerList();
              return true;
            }
            break;
        }
      }
    }
    this.m_lastUtilServerCmd = args;
    if (flag)
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = "Run UTIL server command?",
        m_text = "You are about to run a UTIL Server command - this may affect other players on this environment and possibly change configuration on this environment.\n\nPlease confirm you want to do this.",
        m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
        m_responseCallback = responseCallback
      });
    else
      responseCallback(AlertPopup.Response.OK, (object) null);
    return true;
  }

  private void OnProcessCheat_utilservercmd_OnResponse()
  {
    DebugCommandResponse debugCommandResponse = Network.GetDebugCommandResponse();
    bool flag1 = false;
    string str1 = "null response";
    string str2 = this.m_lastUtilServerCmd == null || this.m_lastUtilServerCmd.Length == 0 ? string.Empty : this.m_lastUtilServerCmd[0];
    string[] strArray1 = this.m_lastUtilServerCmd != null ? ((IEnumerable<string>) this.m_lastUtilServerCmd).Skip<string>(1).ToArray<string>() : new string[0];
    string str3 = strArray1.Length != 0 ? strArray1[0] : (string) null;
    string str4 = strArray1.Length >= 2 ? strArray1[1].ToLower() : (string) null;
    this.m_lastUtilServerCmd = (string[]) null;
    if (debugCommandResponse != null)
    {
      flag1 = debugCommandResponse.Success;
      str1 = string.Format("{0} {1}", !debugCommandResponse.Success ? (object) "FAILED:" : (object) string.Empty, !debugCommandResponse.HasResponse ? (object) "reply=<blank>" : (object) debugCommandResponse.Response);
    }
    LogLevel level = !flag1 ? LogLevel.Error : LogLevel.Info;
    Log.Net.Print(level, str1, new object[0]);
    bool flag2 = true;
    float delay = 5f;
    if (flag1)
    {
      if (str2 == "tb")
      {
        if (str3 == "scenario" || str3 == "scen" || (str3 == "season" || str3 == "end_offset") || (str3 == "start_offset" || str3 == "wins" || (str3 == "losses" || str3 == "ticket")) || str3 == "reset" && str4 != "help")
          TavernBrawlManager.Get().RefreshServerData();
      }
      else if (str2 == "ranked")
      {
        if (str3 == "medal")
          Time.timeScale = SceneDebugger.Get().m_MaxTimeScale;
        if (str3 == "medal" || str3 == "seasonroll")
        {
          flag1 = flag1 && (!debugCommandResponse.HasResponse || !debugCommandResponse.Response.StartsWith("Error"));
          if (flag1)
          {
            str1 = "Success";
            delay = 0.5f;
          }
          else if (debugCommandResponse.HasResponse)
            str1 = debugCommandResponse.Response;
        }
      }
      else if (str2 == "banner")
      {
        if (str3 == "reset")
        {
          NetCache.Get().ReloadNetObject<NetCache.NetCacheProfileProgress>();
          bool flag3 = false;
          int result = 0;
          foreach (string str5 in strArray1)
          {
            string[] strArray2;
            if (str5 == null)
              strArray2 = (string[]) null;
            else
              strArray2 = str5.Split('=');
            string[] strArray3 = strArray2;
            if (strArray3 != null && strArray3.Length >= 2 && (strArray3[0].Equals("banner", StringComparison.InvariantCultureIgnoreCase) || strArray3[0].Equals("bannerId", StringComparison.InvariantCultureIgnoreCase)))
            {
              flag3 = true;
              int.TryParse(strArray3[1], out result);
            }
          }
          if (flag3)
            BannerManager.Get().Cheat_ClearSeenBannersNewerThan(result);
          else
            BannerManager.Get().Cheat_ClearSeenBanners();
        }
      }
      else if (str2 == "returningplayer")
      {
        flag1 = flag1 && (!debugCommandResponse.HasResponse || !debugCommandResponse.Response.StartsWith("Error"));
        if (flag1)
        {
          ReturningPlayerMgr.Get().Cheat_ResetReturningPlayer();
          if (true)
            str1 += "\nYou may want to log out/in to take effect.";
        }
      }
      if ((str2 == "ranked" || str2 == "arena") && str3 == "reward")
      {
        flag1 = flag1 && (!debugCommandResponse.HasResponse || !debugCommandResponse.Response.StartsWith("Error"));
        if (flag1)
        {
          str1 = Cheats.Cheat_ShowRewardBoxes(str1);
          if (str1 == null)
          {
            delay = 0.5f;
            str1 = "Success";
          }
          else
            flag1 = false;
        }
      }
    }
    if (!flag2)
      return;
    if (flag1)
      UIStatus.Get().AddInfo(str1, delay);
    else
      UIStatus.Get().AddError(str1, -1f);
  }

  private static string Cheat_ShowRewardBoxes(string parsableRewardBags)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Cheats.\u003CCheat_ShowRewardBoxes\u003Ec__AnonStorey427 boxesCAnonStorey427 = new Cheats.\u003CCheat_ShowRewardBoxes\u003Ec__AnonStorey427();
    // ISSUE: reference to a compiler-generated field
    boxesCAnonStorey427.parsableRewardBags = parsableRewardBags;
    if (SceneMgr.Get().IsInGame())
      return "Cannot display reward boxes in gameplay.";
    // ISSUE: reference to a compiler-generated field
    string[] strArray = boxesCAnonStorey427.parsableRewardBags.Trim().Split(new char[1]{ ' ' }, StringSplitOptions.RemoveEmptyEntries);
    if (strArray.Length < 2)
    {
      // ISSUE: reference to a compiler-generated field
      return "Error parsing reply, should start with 'Success:' then player_id: " + boxesCAnonStorey427.parsableRewardBags;
    }
    if (strArray.Length < 3)
    {
      // ISSUE: reference to a compiler-generated field
      return "No rewards returned by server: reply=" + boxesCAnonStorey427.parsableRewardBags;
    }
    List<NetCache.ProfileNotice> notices = new List<NetCache.ProfileNotice>();
    string[] array = ((IEnumerable<string>) strArray).Skip<string>(1).ToArray<string>();
    for (int index1 = 0; index1 < array.Length; ++index1)
    {
      int result1 = 0;
      int index2 = index1 * 2;
      if (index2 < array.Length)
      {
        if (!int.TryParse(array[index2], out result1))
        {
          // ISSUE: reference to a compiler-generated field
          return "Reward at index " + (object) index2 + " (" + array[index2] + ") is not an int: reply=" + boxesCAnonStorey427.parsableRewardBags;
        }
        if (result1 != 0)
        {
          int index3 = index2 + 1;
          if (index3 >= array.Length)
          {
            // ISSUE: reference to a compiler-generated field
            return "No reward bag data at index " + (object) index3 + ": reply=" + boxesCAnonStorey427.parsableRewardBags;
          }
          long result2 = 0;
          if (!long.TryParse(array[index3], out result2))
          {
            // ISSUE: reference to a compiler-generated field
            return "Reward Data at index " + (object) index3 + " (" + array[index3] + ") is not a long int: reply=" + boxesCAnonStorey427.parsableRewardBags;
          }
          NetCache.ProfileNotice profileNotice = (NetCache.ProfileNotice) null;
          TAG_PREMIUM tagPremium = TAG_PREMIUM.NORMAL;
          switch (result1)
          {
            case 1:
            case 12:
            case 14:
            case 15:
              profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardBooster()
              {
                Id = (int) result2,
                Count = 1
              };
              break;
            case 2:
              profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardGold()
              {
                Amount = (int) result2
              };
              break;
            case 3:
              profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardDust()
              {
                Amount = (int) result2
              };
              break;
            case 4:
            case 5:
            case 6:
            case 7:
              profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardCard()
              {
                CardID = GameUtils.TranslateDbIdToCardId((int) result2),
                Premium = tagPremium
              };
              break;
            case 8:
            case 9:
            case 10:
            case 11:
              tagPremium = TAG_PREMIUM.GOLDEN;
              goto case 4;
            case 13:
              profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardCardBack()
              {
                CardBackID = (int) result2
              };
              break;
            default:
              // ISSUE: reference to a compiler-generated field
              UnityEngine.Debug.LogError((object) ("Unknown Reward Bag Type: " + (object) result1 + " (data=" + (object) result2 + ") at index " + (object) index3 + ": reply=" + boxesCAnonStorey427.parsableRewardBags));
              break;
          }
          if (profileNotice != null)
            notices.Add(profileNotice);
        }
      }
      else
        break;
    }
    RewardBoxesDisplay objectOfType = UnityEngine.Object.FindObjectOfType<RewardBoxesDisplay>();
    if ((UnityEngine.Object) objectOfType != (UnityEngine.Object) null)
    {
      float secondsToWait = 0.0f;
      if (objectOfType.IsClosing)
        secondsToWait = 0.1f;
      else
        objectOfType.Close();
      // ISSUE: reference to a compiler-generated method
      ApplicationMgr.Get().ScheduleCallback(secondsToWait, false, new ApplicationMgr.ScheduledCallback(boxesCAnonStorey427.\u003C\u003Em__29D), (object) null);
      return (string) null;
    }
    AssetLoader.Get().LoadGameObject("RewardBoxes", (AssetLoader.GameObjectCallback) ((name, go, callbackData) =>
    {
      RewardBoxesDisplay component = go.GetComponent<RewardBoxesDisplay>();
      component.SetRewards(callbackData as List<RewardData>);
      component.m_Root.transform.position = !(bool) UniversalInputManager.UsePhoneUI ? new Vector3(0.0f, 131.2f, -3.2f) : new Vector3(0.0f, 14.7f, 3f);
      if ((UnityEngine.Object) Box.Get() != (UnityEngine.Object) null && (UnityEngine.Object) Box.Get().GetBoxCamera() != (UnityEngine.Object) null && Box.Get().GetBoxCamera().GetState() == BoxCamera.State.OPENED)
      {
        component.m_Root.transform.position += new Vector3(-3f, 0.0f, 4.6f);
        if ((bool) UniversalInputManager.UsePhoneUI)
          component.m_Root.transform.position += new Vector3(0.0f, 0.0f, -7f);
        else
          component.transform.localScale = Vector3.one * 0.6f;
      }
      component.AnimateRewards();
    }), (object) RewardUtils.GetRewards(notices), false);
    return (string) null;
  }

  private bool OnProcessCheat_gameservercmd(string func, string[] args, string rawArgs)
  {
    return true;
  }

  private bool OnProcessCheat_rewardboxes(string func, string[] args, string rawArgs)
  {
    if (string.IsNullOrEmpty(args[0].ToLower()))
      ;
    int val = 5;
    if (args.Length > 1)
      GeneralUtils.TryParseInt(args[1], out val);
    BoosterDbId[] array = Enum.GetValues(typeof (BoosterDbId)).Cast<BoosterDbId>().Where<BoosterDbId>((Func<BoosterDbId, bool>) (i => i != BoosterDbId.INVALID)).ToArray<BoosterDbId>();
    string message = Cheats.Cheat_ShowRewardBoxes("Success: 123456" + " " + (object) 13 + " " + (object) UnityEngine.Random.Range(1, 34) + " " + (object) 1 + " " + (object) array[UnityEngine.Random.Range(0, array.Length)] + " " + (object) 3 + " " + (object) (UnityEngine.Random.Range(1, 31) * 5) + " " + (object) 2 + " " + (object) (UnityEngine.Random.Range(1, 31) * 5) + " " + (object) (UnityEngine.Random.Range(0, 2) != 0 ? 10 : 6) + " " + (object) GameUtils.TranslateCardIdToDbId("EX1_279"));
    if (message != null)
      UIStatus.Get().AddError(message, -1f);
    return true;
  }

  private bool OnProcessCheat_rankchange(string func, string[] args, string rawArgs)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Cheats.\u003COnProcessCheat_rankchange\u003Ec__AnonStorey428 rankchangeCAnonStorey428 = new Cheats.\u003COnProcessCheat_rankchange\u003Ec__AnonStorey428();
    // ISSUE: reference to a compiler-generated field
    rankchangeCAnonStorey428.args = args;
    // ISSUE: reference to a compiler-generated field
    if (string.IsNullOrEmpty(rankchangeCAnonStorey428.args[0].ToLower()))
      ;
    // ISSUE: reference to a compiler-generated method
    AssetLoader.Get().LoadGameObject("RankChangeTwoScoop", new AssetLoader.GameObjectCallback(rankchangeCAnonStorey428.\u003C\u003Em__2A0), (object) null, false);
    return true;
  }

  private bool OnProcessCheat_easyrank(string func, string[] args, string rawArgs)
  {
    string s = args[0].ToLower();
    if (string.IsNullOrEmpty(s))
      s = "20";
    int result = 25;
    if (!int.TryParse(s, out result))
      return false;
    int a = 26 - result;
    int num = 0 + Mathf.Min(a, 5) * 2;
    if (a > 5)
      num += Mathf.Min(a - 5, 5) * 3;
    if (a > 10)
      num += Mathf.Min(a - 10, 5) * 4;
    if (a > 15)
      num += Mathf.Min(a - 15, 10) * 5;
    CheatMgr.Get().ProcessCheat(string.Format("util ranked set starlevel={0}", (object) a));
    CheatMgr.Get().ProcessCheat(string.Format("util ranked set beststarlevel={0}", (object) a));
    CheatMgr.Get().ProcessCheat(string.Format("util ranked set stars={0}", (object) num));
    return true;
  }

  private bool OnProcessCheat_timescale(string func, string[] args, string rawArgs)
  {
    string lower = args[0].ToLower();
    if (string.IsNullOrEmpty(lower))
    {
      UIStatus.Get().AddInfo(string.Format("Current timeScale is: {0}", (object) SceneDebugger.GetDevTimescale()), 3f * SceneDebugger.GetDevTimescale());
      return true;
    }
    float result = 1f;
    if (!float.TryParse(lower, out result))
      return false;
    SceneDebugger.SetDevTimescale(result);
    UIStatus.Get().AddInfo(string.Format("Setting timescale to: {0}", (object) result), 3f * result);
    return true;
  }

  private bool OnProcessCheat_reset(string func, string[] args, string rawArgs)
  {
    ApplicationMgr.Get().Reset();
    return true;
  }

  private bool OnProcessCheat_scenario(string func, string[] args, string rawArgs)
  {
    string str1 = args[0];
    int val1;
    if (!GeneralUtils.TryParseInt(str1, out val1))
    {
      Error.AddWarning("scenario Cheat Error", "Error reading a scenario id from \"{0}\"", (object) str1);
      return false;
    }
    GameType gameType = GameType.GT_VS_AI;
    FormatType outVal = FormatType.FT_WILD;
    CollectionDeck deck = (CollectionDeck) null;
    if (args.Length > 1)
    {
      string str2 = args[1];
      int val2;
      if (!GeneralUtils.TryParseInt(str2, out val2))
      {
        Error.AddWarning("scenario Cheat Error", "Error reading a game type id from \"{0}\"", (object) str2);
        return false;
      }
      gameType = (GameType) val2;
      if (args.Length > 2)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        Cheats.\u003COnProcessCheat_scenario\u003Ec__AnonStorey429 scenarioCAnonStorey429 = new Cheats.\u003COnProcessCheat_scenario\u003Ec__AnonStorey429();
        // ISSUE: reference to a compiler-generated field
        scenarioCAnonStorey429.strDeckId = args[2];
        long val3;
        // ISSUE: reference to a compiler-generated field
        if (GeneralUtils.TryParseLong(scenarioCAnonStorey429.strDeckId, out val3))
          deck = CollectionManager.Get().GetDeck(val3);
        if (deck == null)
        {
          // ISSUE: reference to a compiler-generated method
          deck = CollectionManager.Get().GetDecks().Where<KeyValuePair<long, CollectionDeck>>(new Func<KeyValuePair<long, CollectionDeck>, bool>(scenarioCAnonStorey429.\u003C\u003Em__2A1)).FirstOrDefault<KeyValuePair<long, CollectionDeck>>().Value;
        }
      }
      if (args.Length > 3)
      {
        string str3 = args[3];
        int result;
        if (int.TryParse(str3, out result))
          outVal = (FormatType) result;
        else if (!EnumUtils.TryGetEnum<FormatType>(str3, out outVal))
        {
          string lower = str3.ToLower();
          if (lower != null)
          {
            // ISSUE: reference to a compiler-generated field
            if (Cheats.\u003C\u003Ef__switch\u0024mapC5 == null)
            {
              // ISSUE: reference to a compiler-generated field
              Cheats.\u003C\u003Ef__switch\u0024mapC5 = new Dictionary<string, int>(3)
              {
                {
                  "wild",
                  0
                },
                {
                  "standard",
                  1
                },
                {
                  "std",
                  1
                }
              };
            }
            int num;
            // ISSUE: reference to a compiler-generated field
            if (Cheats.\u003C\u003Ef__switch\u0024mapC5.TryGetValue(lower, out num))
            {
              if (num != 0)
              {
                if (num == 1)
                {
                  outVal = FormatType.FT_STANDARD;
                  goto label_23;
                }
              }
              else
              {
                outVal = FormatType.FT_WILD;
                goto label_23;
              }
            }
          }
          Error.AddWarning("scenario Cheat Error", "Error reading a 3rd parameter for FormatType \"{0}\", please use \"wild\" or \"standard\"", (object) str3);
          return false;
        }
      }
    }
label_23:
    Cheats.QuickLaunchAvailability launchAvailability = this.GetQuickLaunchAvailability();
    switch (launchAvailability)
    {
      case Cheats.QuickLaunchAvailability.OK:
        this.LaunchQuickGame(val1, gameType, outVal, deck);
        return true;
      case Cheats.QuickLaunchAvailability.FINDING_GAME:
        Error.AddDevWarning("scenario Cheat Error", "You are already finding a game.");
        break;
      case Cheats.QuickLaunchAvailability.ACTIVE_GAME:
        Error.AddDevWarning("scenario Cheat Error", "You are already in a game.");
        break;
      case Cheats.QuickLaunchAvailability.SCENE_TRANSITION:
        Error.AddDevWarning("scenario Cheat Error", "Can't start a game because a scene transition is active.");
        break;
      case Cheats.QuickLaunchAvailability.COLLECTION_NOT_READY:
        Error.AddDevWarning("scenario Cheat Error", "Can't start a game because your collection is not fully loaded.");
        break;
      default:
        Error.AddDevWarning("scenario Cheat Error", "Can't start a game: {0}", (object) launchAvailability);
        break;
    }
    return false;
  }

  private bool OnProcessCheat_exportcards(string func, string[] args, string rawArgs)
  {
    SceneManager.LoadScene("ExportCards");
    return true;
  }

  private bool OnProcessCheat_freeyourmind(string func, string[] args, string rawArgs)
  {
    this.m_isYourMindFree = true;
    return true;
  }

  private bool OnProcessCheat_reloadgamestrings(string func, string[] args, string rawArgs)
  {
    GameStrings.ReloadAll();
    return true;
  }

  private bool OnProcessCheat_userattentionmanager(string func, string[] args, string rawArgs)
  {
    UIStatus.Get().AddInfo(string.Format("Current UserAttentionBlockers: {0}", (object) UserAttentionManager.DumpUserAttentionBlockers("OnProcessCheat_userattentionmanager")));
    return true;
  }

  private void Cheat_ShowBannerList()
  {
    StringBuilder stringBuilder = new StringBuilder();
    bool flag = true;
    foreach (BannerDbfRecord bannerDbfRecord in (IEnumerable<BannerDbfRecord>) GameDbf.Banner.GetRecords().OrderByDescending<BannerDbfRecord, int>((Func<BannerDbfRecord, int>) (r => r.ID)))
    {
      if (!flag)
        stringBuilder.Append("\n");
      flag = false;
      stringBuilder.AppendFormat("{0}. {1}", (object) bannerDbfRecord.ID, (object) bannerDbfRecord.NoteDesc);
    }
    UIStatus.Get().AddInfo(stringBuilder.ToString(), 5f);
  }

  private bool OnProcessCheat_banner(string func, string[] args, string rawArgs)
  {
    int result = 0;
    if (args.Length < 1 || string.IsNullOrEmpty(args[0]))
      result = GameDbf.Banner.GetRecords().Max<BannerDbfRecord>((Func<BannerDbfRecord, int>) (r => r.ID));
    else if (int.TryParse(args[0], out result))
    {
      if (GameDbf.Banner.GetRecord(result) == null)
      {
        UIStatus.Get().AddInfo(string.Format("Unknown bannerId: {0}", (object) result));
        return true;
      }
    }
    else
    {
      if (args[0].Equals("list", StringComparison.InvariantCultureIgnoreCase))
      {
        this.Cheat_ShowBannerList();
        return true;
      }
      UIStatus.Get().AddInfo(string.Format("Unknown parameter: {0}", (object) args[0]));
      return true;
    }
    BannerManager.Get().ShowBanner(result, (BannerManager.DelOnCloseBanner) null);
    return true;
  }

  private bool OnProcessCheat_raf(string func, string[] args, string rawArgs)
  {
    string lower = args[0].ToLower();
    if (string.Equals(lower, "showhero"))
      RAFManager.Get().ShowRAFHeroFrame();
    else if (string.Equals(lower, "showprogress"))
      RAFManager.Get().ShowRAFProgressFrame();
    else if (string.Equals(lower, "setprogress"))
    {
      if (args.Length > 1)
        RAFManager.Get().SetRAFProgress(Convert.ToInt32(args[1]));
    }
    else if (string.Equals(lower, "showglows"))
    {
      Options.Get().SetBool(Option.HAS_SEEN_RAF, false);
      Options.Get().SetBool(Option.HAS_SEEN_RAF_RECRUIT_URL, false);
      FriendListFrame friendListFrame = ChatMgr.Get().FriendListFrame;
      if ((UnityEngine.Object) friendListFrame != (UnityEngine.Object) null)
        friendListFrame.UpdateRAFButtonGlow();
      RAFFrame rafFrame = RAFManager.Get().GetRAFFrame();
      if ((UnityEngine.Object) rafFrame != (UnityEngine.Object) null)
        rafFrame.UpdateRecruitFriendsButtonGlow();
      RAFManager.Get().ShowRAFProgressFrame();
    }
    return true;
  }

  private bool OnProcessCheat_returningplayer(string func, string[] args, string rawArgs)
  {
    if (args.Length < 1)
      Error.AddWarning("returningplayer Cheat Error", "No parameter provided.");
    int val;
    if (!GeneralUtils.TryParseInt(args[0], out val))
    {
      Error.AddWarning("returningplayer Cheat Error", "Error reading an int from \"{0}\"", (object) args[0]);
      return false;
    }
    ReturningPlayerMgr.Get().Cheat_SetReturningPlayerProgress(val);
    return true;
  }

  private bool OnProcessCheat_innkeeperdeck(string func, string[] args, string rawArgs)
  {
    CollectionManager.Get().AutoGenerateNewDeckForPlayer();
    return true;
  }

  private bool OnProcessCheat_lowmemorywarning(string func, string[] args, string rawArgs)
  {
    if (args.Length < 1)
      MobileCallbackManager.Get().LowMemoryWarning(string.Empty);
    else
      MobileCallbackManager.Get().LowMemoryWarning(args[0]);
    return true;
  }

  private bool OnProcessCheat_autoexportgamestate(string func, string[] args, string rawArgs)
  {
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.GAMEPLAY)
      return false;
    string str = !string.IsNullOrEmpty(args[0]) ? args[0] : "GameStateExportFile";
    JsonNode jsonNode1 = new JsonNode();
    using (Map<int, Player>.Enumerator enumerator = GameState.Get().GetPlayerMap().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, Player> current = enumerator.Current;
        string key = "Player" + (object) current.Key;
        JsonNode jsonNode2 = new JsonNode();
        jsonNode1.Add(key, (object) jsonNode2);
        jsonNode2["Hero"] = (object) this.GetCardJson(current.Value.GetHero());
        jsonNode2["HeroPower"] = (object) this.GetCardJson(current.Value.GetHeroPower());
        if (current.Value.HasWeapon())
          jsonNode2["Weapon"] = (object) this.GetCardJson(current.Value.GetWeaponCard().GetEntity());
        jsonNode2["CardsInBattlefield"] = (object) this.GetCardlistJson(current.Value.GetBattlefieldZone().GetCards());
        if (current.Value.GetSide() == Player.Side.FRIENDLY)
        {
          jsonNode2["CardsInHand"] = (object) this.GetCardlistJson(current.Value.GetHandZone().GetCards());
          jsonNode2["ActiveSecrets"] = (object) this.GetCardlistJson(current.Value.GetSecretZone().GetCards());
        }
      }
    }
    File.WriteAllText(string.Format("{0}\\{1}.json", (object) Environment.GetFolderPath(Environment.SpecialFolder.Desktop), (object) str), Json.Serialize((object) jsonNode1));
    return true;
  }

  private enum QuickLaunchAvailability
  {
    OK,
    FINDING_GAME,
    ACTIVE_GAME,
    SCENE_TRANSITION,
    COLLECTION_NOT_READY,
  }

  private class QuickLaunchState
  {
    public bool m_launching;
    public bool m_skipMulligan;
    public bool m_flipHeroes;
    public bool m_mirrorHeroes;
    public string m_opponentHeroCardId;
  }
}
