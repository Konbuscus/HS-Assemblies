﻿// Decompiled with JetBrains decompiler
// Type: FriendUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections.Generic;

public class FriendUtils
{
  public static string GetUniqueName(BnetPlayer friend)
  {
    BnetBattleTag battleTag;
    string name;
    if (FriendUtils.GetUniqueName(friend, out battleTag, out name))
      return battleTag.ToString();
    return name;
  }

  public static string GetUniqueNameWithColor(BnetPlayer friend)
  {
    string nameColorStr = !friend.IsOnline() ? "999999ff" : "5ecaf0ff";
    BnetBattleTag battleTag;
    string name;
    if (FriendUtils.GetUniqueName(friend, out battleTag, out name))
      return FriendUtils.GetBattleTagWithColor(battleTag, nameColorStr);
    return string.Format("<color=#{0}>{1}</color>", (object) nameColorStr, (object) name);
  }

  public static string GetBattleTagWithColor(BnetBattleTag battleTag, string nameColorStr)
  {
    return string.Format("<color=#{0}>{1}</color><color=#{2}>#{3}</color>", (object) nameColorStr, (object) battleTag.GetName(), (object) "a1a1a1ff", (object) battleTag.GetNumber());
  }

  public static string GetFriendListName(BnetPlayer friend, bool addColorTags)
  {
    string str = (string) null;
    BnetAccount account = friend.GetAccount();
    if (account != (BnetAccount) null)
    {
      str = account.GetFullName();
      if (str == null && account.GetBattleTag() != (BnetBattleTag) null)
        str = account.GetBattleTag().ToString();
    }
    if (str == null)
    {
      using (Map<BnetGameAccountId, BnetGameAccount>.Enumerator enumerator = friend.GetGameAccounts().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<BnetGameAccountId, BnetGameAccount> current = enumerator.Current;
          if (current.Value.GetBattleTag() != (BnetBattleTag) null)
          {
            str = current.Value.GetBattleTag().ToString();
            break;
          }
        }
      }
    }
    if (addColorTags)
      return string.Format("<color=#{0}>{1}</color>", !friend.IsOnline() ? (object) "999999ff" : (object) "5ecaf0ff", (object) str);
    return str;
  }

  public static string GetRequestElapsedTimeString(ulong epochMicrosec)
  {
    TimeUtils.ElapsedStringSet stringSet = new TimeUtils.ElapsedStringSet()
    {
      m_seconds = "GLOBAL_DATETIME_FRIENDREQUEST_SECONDS",
      m_minutes = "GLOBAL_DATETIME_FRIENDREQUEST_MINUTES",
      m_hours = "GLOBAL_DATETIME_FRIENDREQUEST_HOURS",
      m_yesterday = "GLOBAL_DATETIME_FRIENDREQUEST_DAY",
      m_days = "GLOBAL_DATETIME_FRIENDREQUEST_DAYS",
      m_weeks = "GLOBAL_DATETIME_FRIENDREQUEST_WEEKS",
      m_monthAgo = "GLOBAL_DATETIME_FRIENDREQUEST_MONTH"
    };
    return TimeUtils.GetElapsedTimeStringFromEpochMicrosec(epochMicrosec, stringSet);
  }

  public static string GetLastOnlineElapsedTimeString(ulong epochMicrosec)
  {
    if ((long) epochMicrosec == 0L)
      return GameStrings.Get("GLOBAL_OFFLINE");
    TimeUtils.ElapsedStringSet stringSet = new TimeUtils.ElapsedStringSet()
    {
      m_seconds = "GLOBAL_DATETIME_LASTONLINE_SECONDS",
      m_minutes = "GLOBAL_DATETIME_LASTONLINE_MINUTES",
      m_hours = "GLOBAL_DATETIME_LASTONLINE_HOURS",
      m_yesterday = "GLOBAL_DATETIME_LASTONLINE_DAY",
      m_days = "GLOBAL_DATETIME_LASTONLINE_DAYS",
      m_weeks = "GLOBAL_DATETIME_LASTONLINE_WEEKS",
      m_monthAgo = "GLOBAL_DATETIME_LASTONLINE_MONTH"
    };
    return TimeUtils.GetElapsedTimeStringFromEpochMicrosec(epochMicrosec, stringSet);
  }

  public static string GetAwayTimeString(ulong epochMicrosec)
  {
    TimeUtils.ElapsedStringSet stringSet = new TimeUtils.ElapsedStringSet()
    {
      m_seconds = "GLOBAL_DATETIME_AFK_SECONDS",
      m_minutes = "GLOBAL_DATETIME_AFK_MINUTES",
      m_hours = "GLOBAL_DATETIME_AFK_HOURS",
      m_yesterday = "GLOBAL_DATETIME_AFK_DAY",
      m_days = "GLOBAL_DATETIME_AFK_DAYS",
      m_weeks = "GLOBAL_DATETIME_AFK_WEEKS",
      m_monthAgo = "GLOBAL_DATETIME_AFK_MONTH"
    };
    return TimeUtils.GetElapsedTimeStringFromEpochMicrosec(epochMicrosec, stringSet);
  }

  public static int FriendSortCompare(BnetPlayer friend1, BnetPlayer friend2)
  {
    int result = 0;
    if (friend1 == null || friend2 == null)
    {
      if (friend1 == friend2)
        return 0;
      return friend1 == null ? 1 : -1;
    }
    if (!friend1.IsOnline() && !friend2.IsOnline())
      return FriendUtils.FriendNameSortCompare(friend1, friend2);
    if (friend1.IsOnline() && !friend2.IsOnline())
      return -1;
    if (!friend1.IsOnline() && friend2.IsOnline())
      return 1;
    BnetProgramId bestProgramId1 = friend1.GetBestProgramId();
    BnetProgramId bestProgramId2 = friend2.GetBestProgramId();
    if (FriendUtils.FriendSortFlagCompare(friend1, friend2, (bgs.FourCC) bestProgramId1 == (bgs.FourCC) BnetProgramId.HEARTHSTONE, (bgs.FourCC) bestProgramId2 == (bgs.FourCC) BnetProgramId.HEARTHSTONE, out result))
      return result;
    bool lhsflag1 = !((bgs.FourCC) bestProgramId1 == (bgs.FourCC) null) && bestProgramId1.IsGame();
    bool rhsflag1 = !((bgs.FourCC) bestProgramId2 == (bgs.FourCC) null) && bestProgramId2.IsGame();
    if (FriendUtils.FriendSortFlagCompare(friend1, friend2, lhsflag1, rhsflag1, out result))
      return result;
    bool lhsflag2 = !((bgs.FourCC) bestProgramId1 == (bgs.FourCC) null) && bestProgramId1.IsPhoenix();
    bool rhsflag2 = !((bgs.FourCC) bestProgramId2 == (bgs.FourCC) null) && bestProgramId2.IsPhoenix();
    if (FriendUtils.FriendSortFlagCompare(friend1, friend2, lhsflag2, rhsflag2, out result))
      return result;
    return FriendUtils.FriendNameSortCompare(friend1, friend2);
  }

  public static bool IsValidEmail(string emailString)
  {
    if (emailString == null)
      return false;
    int num1 = emailString.IndexOf('@');
    if (num1 >= 1 && num1 < emailString.Length - 1)
    {
      int num2 = emailString.LastIndexOf('.');
      if (num2 > num1 + 1 && num2 < emailString.Length - 1)
        return true;
    }
    return false;
  }

  private static bool GetUniqueName(BnetPlayer friend, out BnetBattleTag battleTag, out string name)
  {
    battleTag = friend.GetBattleTag();
    name = friend.GetBestName();
    if (battleTag == (BnetBattleTag) null)
      return false;
    if (BnetNearbyPlayerMgr.Get().IsNearbyStranger(friend))
      return true;
    using (List<BnetPlayer>.Enumerator enumerator = BnetFriendMgr.Get().GetFriends().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer current = enumerator.Current;
        if (current != friend)
        {
          string bestName = current.GetBestName();
          if (string.Compare(name, bestName, true) == 0)
            return true;
        }
      }
    }
    return false;
  }

  private static bool FriendSortFlagCompare(BnetPlayer lhs, BnetPlayer rhs, bool lhsflag, bool rhsflag, out int result)
  {
    if (lhsflag && !rhsflag)
    {
      result = -1;
      return true;
    }
    if (!lhsflag && rhsflag)
    {
      result = 1;
      return true;
    }
    result = 0;
    return false;
  }

  private static int FriendNameSortCompare(BnetPlayer friend1, BnetPlayer friend2)
  {
    int num = string.Compare(FriendUtils.GetFriendListName(friend1, false), FriendUtils.GetFriendListName(friend2, false), true);
    if (num != 0)
      return num;
    return (int) ((long) friend1.GetAccountId().GetLo() - (long) friend2.GetAccountId().GetLo());
  }
}
