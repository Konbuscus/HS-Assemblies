﻿// Decompiled with JetBrains decompiler
// Type: UIBPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

[CustomEditClass]
public class UIBPopup : MonoBehaviour
{
  [CustomEditField(Sections = "Animation & Positioning")]
  public Vector3 m_showPosition = Vector3.zero;
  [CustomEditField(Sections = "Animation & Positioning")]
  public Vector3 m_showScale = Vector3.one;
  [CustomEditField(Sections = "Animation & Positioning")]
  public float m_showAnimTime = 0.5f;
  [CustomEditField(Sections = "Animation & Positioning")]
  public Vector3 m_hidePosition = new Vector3(-1000f, 0.0f, 0.0f);
  [CustomEditField(Sections = "Animation & Positioning")]
  public float m_hideAnimTime = 0.1f;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_showAnimationSound = "Assets/Game/Sounds/Interface/Expand_Up";
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_hideAnimationSound = "Assets/Game/Sounds/Interface/Shrink_Down";
  protected CanvasScaleMode m_scaleMode = CanvasScaleMode.HEIGHT;
  protected bool m_destroyOnSceneLoad = true;
  protected bool m_useOverlayUI = true;
  private const string s_ShowiTweenAnimationName = "SHOW_ANIMATION";
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public bool m_playShowSoundWithNoAnimation;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public bool m_playHideSoundWithNoAnimation;
  [CustomEditField(Sections = "Click Blockers")]
  public BoxCollider m_animationClickBlocker;
  protected bool m_shown;

  public virtual bool IsShown()
  {
    return this.m_shown;
  }

  public virtual void Show()
  {
    this.Show(true);
  }

  public virtual void Show(bool useOverlayUI)
  {
    if (this.m_shown)
      return;
    this.m_useOverlayUI = useOverlayUI;
    if (this.m_useOverlayUI)
    {
      OverlayUI overlayUi = OverlayUI.Get();
      CanvasScaleMode scaleMode = this.m_scaleMode;
      GameObject gameObject = this.gameObject;
      int num1 = 0;
      int num2 = this.m_destroyOnSceneLoad ? 1 : 0;
      int num3 = (int) scaleMode;
      overlayUi.AddGameObject(gameObject, (CanvasAnchor) num1, num2 != 0, (CanvasScaleMode) num3);
    }
    this.m_shown = true;
    this.DoShowAnimation((UIBPopup.OnAnimationComplete) null);
  }

  public virtual void Hide()
  {
    this.Hide(false);
  }

  protected virtual void Hide(bool animate)
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    this.DoHideAnimation(!animate, new UIBPopup.OnAnimationComplete(this.OnHidden));
  }

  protected virtual void OnHidden()
  {
  }

  protected void DoShowAnimation(UIBPopup.OnAnimationComplete animationDoneCallback = null)
  {
    this.DoShowAnimation(false, animationDoneCallback);
  }

  protected void DoShowAnimation(bool disableAnimation, UIBPopup.OnAnimationComplete animationDoneCallback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    UIBPopup.\u003CDoShowAnimation\u003Ec__AnonStorey355 animationCAnonStorey355 = new UIBPopup.\u003CDoShowAnimation\u003Ec__AnonStorey355();
    // ISSUE: reference to a compiler-generated field
    animationCAnonStorey355.animationDoneCallback = animationDoneCallback;
    // ISSUE: reference to a compiler-generated field
    animationCAnonStorey355.\u003C\u003Ef__this = this;
    this.transform.localPosition = this.m_showPosition;
    if (this.m_useOverlayUI)
    {
      OverlayUI overlayUi = OverlayUI.Get();
      CanvasScaleMode scaleMode = this.m_scaleMode;
      GameObject gameObject = this.gameObject;
      int num1 = 0;
      int num2 = this.m_destroyOnSceneLoad ? 1 : 0;
      int num3 = (int) scaleMode;
      overlayUi.AddGameObject(gameObject, (CanvasAnchor) num1, num2 != 0, (CanvasScaleMode) num3);
    }
    this.EnableAnimationClickBlocker(true);
    if (!disableAnimation && (double) this.m_showAnimTime > 0.0)
    {
      this.transform.localScale = this.m_showScale * 0.01f;
      if (!string.IsNullOrEmpty(this.m_showAnimationSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_showAnimationSound));
      Hashtable args = iTween.Hash((object) "scale", (object) this.m_showScale, (object) "isLocal", (object) false, (object) "time", (object) this.m_showAnimTime, (object) "easetype", (object) iTween.EaseType.easeOutBounce, (object) "name", (object) "SHOW_ANIMATION");
      // ISSUE: reference to a compiler-generated field
      if (animationCAnonStorey355.animationDoneCallback != null)
      {
        // ISSUE: reference to a compiler-generated method
        args.Add((object) "oncomplete", (object) new Action<object>(animationCAnonStorey355.\u003C\u003Em__24));
      }
      iTween.StopByName(this.gameObject, "SHOW_ANIMATION");
      iTween.ScaleTo(this.gameObject, args);
    }
    else
    {
      if (this.m_playShowSoundWithNoAnimation && !string.IsNullOrEmpty(this.m_showAnimationSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_showAnimationSound));
      this.transform.localScale = this.m_showScale;
      // ISSUE: reference to a compiler-generated field
      if (animationCAnonStorey355.animationDoneCallback == null)
        return;
      this.EnableAnimationClickBlocker(false);
      // ISSUE: reference to a compiler-generated field
      animationCAnonStorey355.animationDoneCallback();
    }
  }

  protected void DoHideAnimation(UIBPopup.OnAnimationComplete animationDoneCallback = null)
  {
    this.DoHideAnimation(false, animationDoneCallback);
  }

  protected void DoHideAnimation(bool disableAnimation, UIBPopup.OnAnimationComplete animationDoneCallback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    UIBPopup.\u003CDoHideAnimation\u003Ec__AnonStorey356 animationCAnonStorey356 = new UIBPopup.\u003CDoHideAnimation\u003Ec__AnonStorey356();
    // ISSUE: reference to a compiler-generated field
    animationCAnonStorey356.animationDoneCallback = animationDoneCallback;
    // ISSUE: reference to a compiler-generated field
    animationCAnonStorey356.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    animationCAnonStorey356.setHidePosition = new Action(animationCAnonStorey356.\u003C\u003Em__25);
    if (!disableAnimation && (double) this.m_hideAnimTime > 0.0)
    {
      if (!string.IsNullOrEmpty(this.m_showAnimationSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_hideAnimationSound));
      Hashtable args = iTween.Hash((object) "scale", (object) (this.m_showScale * 0.01f), (object) "isLocal", (object) true, (object) "time", (object) this.m_hideAnimTime, (object) "easetype", (object) iTween.EaseType.linear, (object) "name", (object) "SHOW_ANIMATION");
      // ISSUE: reference to a compiler-generated field
      if (animationCAnonStorey356.animationDoneCallback != null)
      {
        // ISSUE: reference to a compiler-generated method
        args.Add((object) "oncomplete", (object) new Action<object>(animationCAnonStorey356.\u003C\u003Em__26));
      }
      else
      {
        // ISSUE: reference to a compiler-generated method
        args.Add((object) "oncomplete", (object) new Action<object>(animationCAnonStorey356.\u003C\u003Em__27));
      }
      iTween.StopByName(this.gameObject, "SHOW_ANIMATION");
      iTween.ScaleTo(this.gameObject, args);
    }
    else
    {
      if (this.m_playHideSoundWithNoAnimation && !string.IsNullOrEmpty(this.m_hideAnimationSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_hideAnimationSound));
      // ISSUE: reference to a compiler-generated field
      animationCAnonStorey356.setHidePosition();
      // ISSUE: reference to a compiler-generated field
      if (animationCAnonStorey356.animationDoneCallback == null)
        return;
      // ISSUE: reference to a compiler-generated field
      animationCAnonStorey356.animationDoneCallback();
    }
  }

  private void EnableAnimationClickBlocker(bool enable)
  {
    if (!((UnityEngine.Object) this.m_animationClickBlocker != (UnityEngine.Object) null))
      return;
    this.m_animationClickBlocker.gameObject.SetActive(enable);
  }

  public delegate void OnAnimationComplete();
}
