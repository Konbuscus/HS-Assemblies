﻿// Decompiled with JetBrains decompiler
// Type: CollectionDeckSlot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class CollectionDeckSlot
{
  private bool m_owned = true;
  public CollectionDeckSlot.DelOnSlotEmptied OnSlotEmptied;
  private int m_index;
  private TAG_PREMIUM m_premium;
  private int m_count;
  private string m_cardId;

  public long UID
  {
    get
    {
      return GameUtils.CardUID(this.CardID, this.Premium);
    }
  }

  public long ClientUID
  {
    get
    {
      return GameUtils.ClientCardUID(this.CardID, this.Premium, this.Owned);
    }
  }

  public int Index
  {
    get
    {
      return this.m_index;
    }
    set
    {
      this.m_index = value;
    }
  }

  public TAG_PREMIUM Premium
  {
    get
    {
      return this.m_premium;
    }
    set
    {
      this.m_premium = value;
    }
  }

  public int Count
  {
    get
    {
      return this.m_count;
    }
    set
    {
      this.m_count = value;
      if (this.m_count > 0 || this.OnSlotEmptied == null)
        return;
      this.OnSlotEmptied(this);
    }
  }

  public string CardID
  {
    get
    {
      return this.m_cardId;
    }
    set
    {
      this.m_cardId = value;
    }
  }

  public bool Owned
  {
    get
    {
      return this.m_owned;
    }
    set
    {
      this.m_owned = value;
    }
  }

  public override string ToString()
  {
    return string.Format("[CollectionDeckSlot: Index={0}, Premium={1}, Count={2}, CardID={3}]", (object) this.Index, (object) this.Premium, (object) this.Count, (object) this.CardID);
  }

  public long GetUID(CollectionDeck deck)
  {
    if (deck.ShouldSplitSlotsByValidity())
      return this.ClientUID;
    return this.UID;
  }

  public void CopyFrom(CollectionDeckSlot otherSlot)
  {
    this.Index = otherSlot.Index;
    this.Count = otherSlot.Count;
    this.CardID = otherSlot.CardID;
    this.Premium = otherSlot.Premium;
    this.Owned = otherSlot.Owned;
  }

  public delegate void DelOnSlotEmptied(CollectionDeckSlot slot);
}
