﻿// Decompiled with JetBrains decompiler
// Type: DeckHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class DeckHelper : MonoBehaviour
{
  public Vector3 m_deckCardLocalScale = new Vector3(5.75f, 5.75f, 5.75f);
  private List<DeckHelper.DelStateChangedListener> m_listeners = new List<DeckHelper.DelStateChangedListener>();
  private List<Actor> m_choiceActors = new List<Actor>();
  private bool m_ReplaceSingleTemplateCard = true;
  private const float INNKEEPER_POPUP_DURATION = 7f;
  public UberText m_instructionText;
  public UberText m_replaceText;
  public GameObject m_rootObject;
  public UIBButton m_suggestDoneButton;
  public UIBButton m_replaceDoneButton;
  public PegUIElement m_inputBlocker;
  public GameObject m_3choiceContainer;
  public GameObject m_replaceContainer;
  public GameObject m_2choiceContainer;
  public Vector3 m_cardSpacing;
  public GameObject m_suggestACardPane;
  public GameObject m_replaceACardPane;
  public UIBButton m_innkeeperPopup;
  private static DeckHelper s_instance;
  private Actor m_replaceCardActor;
  private bool m_shown;
  private DeckTrayDeckTileVisual m_tileToRemove;
  private EntityDef m_cardToRemove;
  private bool m_continueAfterReplace;
  private Vector3 m_innkeeperFullScale;
  private bool m_innkeeperPopupShown;

  private void Awake()
  {
    DeckHelper.s_instance = this;
    this.m_rootObject.SetActive(false);
    this.m_replaceDoneButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.EndButtonClick));
    this.m_suggestDoneButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.EndButtonClick));
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      if (!((UnityEngine.Object) this.m_innkeeperPopup != (UnityEngine.Object) null))
        return;
      this.m_innkeeperFullScale = this.m_innkeeperPopup.gameObject.transform.localScale;
      this.m_innkeeperPopup.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.InnkeeperPopupClicked));
    }
    else
      this.m_inputBlocker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.EndButtonClick));
  }

  private void OnDestroy()
  {
    DeckHelper.s_instance = (DeckHelper) null;
  }

  private void EndButtonClick(UIEvent e)
  {
    Navigation.GoBack();
  }

  public static DeckHelper Get()
  {
    if ((UnityEngine.Object) DeckHelper.s_instance == (UnityEngine.Object) null)
      DeckHelper.s_instance = AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "DeckHelper" : "DeckHelper_phone", true, false).GetComponent<DeckHelper>();
    return DeckHelper.s_instance;
  }

  public bool IsActive()
  {
    return this.m_shown;
  }

  public void RegisterStateChangedListener(DeckHelper.DelStateChangedListener listener)
  {
    if (this.m_listeners.Contains(listener))
      return;
    this.m_listeners.Add(listener);
  }

  public void RemoveStateChangedListener(DeckHelper.DelStateChangedListener listener)
  {
    this.m_listeners.Remove(listener);
  }

  public void UpdateChoices()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckHelper.\u003CUpdateChoices\u003Ec__AnonStorey39E choicesCAnonStorey39E = new DeckHelper.\u003CUpdateChoices\u003Ec__AnonStorey39E();
    // ISSUE: reference to a compiler-generated field
    choicesCAnonStorey39E.\u003C\u003Ef__this = this;
    this.CleanOldChoices();
    if (!this.IsActive())
      return;
    EntityDef entityDef = this.m_cardToRemove;
    this.m_cardToRemove = (EntityDef) null;
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    // ISSUE: reference to a compiler-generated field
    choicesCAnonStorey39E.cardsToShow = DeckMaker.GetFillCardChoices(taggedDeck, entityDef, 3, (DeckRuleset) null);
    // ISSUE: reference to a compiler-generated field
    if (entityDef == null && choicesCAnonStorey39E.cardsToShow.m_removeTemplate != null)
    {
      // ISSUE: reference to a compiler-generated field
      entityDef = choicesCAnonStorey39E.cardsToShow.m_removeTemplate;
    }
    // ISSUE: reference to a compiler-generated field
    string reason = choicesCAnonStorey39E.cardsToShow.m_reason;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (choicesCAnonStorey39E.cardsToShow == null || choicesCAnonStorey39E.cardsToShow.m_addChoices.Count == 0)
    {
      UnityEngine.Debug.LogError((object) "DeckHelper.GetChoices() - Can't find choices!!!!");
    }
    else
    {
      if ((UnityEngine.Object) this.m_instructionText != (UnityEngine.Object) null)
      {
        bool flag = !this.m_instructionText.Text.Equals(reason);
        this.m_instructionText.Text = reason;
        if ((bool) UniversalInputManager.UsePhoneUI && flag)
        {
          if (NotificationManager.Get().IsQuotePlaying)
            this.m_instructionText.Text = string.Empty;
          else
            this.ShowInnkeeperPopup();
        }
      }
      this.m_replaceACardPane.SetActive(entityDef != null);
      this.m_suggestACardPane.SetActive(entityDef == null);
      if (entityDef != null)
      {
        if ((UnityEngine.Object) this.m_tileToRemove != (UnityEngine.Object) null)
          this.m_tileToRemove.SetHighlight(false);
        this.m_tileToRemove = CollectionDeckTray.Get().GetCardTileVisual(entityDef.GetCardId());
        GhostCard.Type ghostTypeFromSlot = GhostCard.GetGhostTypeFromSlot(taggedDeck, this.m_tileToRemove.GetSlot());
        this.m_replaceCardActor = this.LoadBestCardActor(entityDef, TAG_PREMIUM.NORMAL, ghostTypeFromSlot);
        if ((UnityEngine.Object) this.m_replaceCardActor != (UnityEngine.Object) null)
          GameUtils.SetParent((Component) this.m_replaceCardActor, this.m_replaceContainer, false);
        if ((UnityEngine.Object) this.m_replaceText != (UnityEngine.Object) null)
          this.m_replaceText.Text = ghostTypeFromSlot != GhostCard.Type.NOT_VALID ? GameStrings.Get("GLUE_COLLECTION_DECK_HELPER_REPLACE_CARD") : GameStrings.Get("GLUE_COLLECTION_DECK_HELPER_REPLACE_INVALID_CARD");
        if (this.m_tileToRemove.GetSlot().Owned && !Options.Get().GetBool(Option.HAS_SEEN_DECK_TEMPLATE_GHOST_CARD, false))
          Options.Get().SetBool(Option.HAS_SEEN_DECK_TEMPLATE_GHOST_CARD, true);
        if (!taggedDeck.IsValidSlot(this.m_tileToRemove.GetSlot()) && !Options.Get().GetBool(Option.HAS_SEEN_INVALID_ROTATED_CARD, false))
          Options.Get().SetBool(Option.HAS_SEEN_INVALID_ROTATED_CARD, true);
      }
      bool flag1 = entityDef != null;
      // ISSUE: reference to a compiler-generated field
      int num = Mathf.Min(!flag1 ? 3 : 2, choicesCAnonStorey39E.cardsToShow.m_addChoices.Count);
      GameObject parent = !flag1 ? this.m_3choiceContainer : this.m_2choiceContainer;
      for (int index = 0; index < num; ++index)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        DeckHelper.\u003CUpdateChoices\u003Ec__AnonStorey39D choicesCAnonStorey39D = new DeckHelper.\u003CUpdateChoices\u003Ec__AnonStorey39D();
        // ISSUE: reference to a compiler-generated field
        choicesCAnonStorey39D.\u003C\u003Ef__ref\u0024926 = choicesCAnonStorey39E;
        // ISSUE: reference to a compiler-generated field
        choicesCAnonStorey39D.\u003C\u003Ef__this = this;
        // ISSUE: reference to a compiler-generated field
        EntityDef addChoice = choicesCAnonStorey39E.cardsToShow.m_addChoices[index];
        TAG_PREMIUM premiumToUse = !taggedDeck.CanAddOwnedCard(addChoice.GetCardId(), TAG_PREMIUM.GOLDEN) ? TAG_PREMIUM.NORMAL : TAG_PREMIUM.GOLDEN;
        // ISSUE: reference to a compiler-generated field
        choicesCAnonStorey39D.actor = this.LoadBestCardActor(addChoice, premiumToUse, GhostCard.Type.NONE);
        // ISSUE: reference to a compiler-generated field
        if (!((UnityEngine.Object) choicesCAnonStorey39D.actor == (UnityEngine.Object) null))
        {
          // ISSUE: reference to a compiler-generated field
          GameUtils.SetParent((Component) choicesCAnonStorey39D.actor, parent, false);
          // ISSUE: reference to a compiler-generated field
          PegUIElement pegUiElement = choicesCAnonStorey39D.actor.GetCollider().gameObject.AddComponent<PegUIElement>();
          // ISSUE: reference to a compiler-generated method
          pegUiElement.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(choicesCAnonStorey39D.\u003C\u003Em__ED));
          // ISSUE: reference to a compiler-generated method
          pegUiElement.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(choicesCAnonStorey39D.\u003C\u003Em__EE));
          // ISSUE: reference to a compiler-generated method
          pegUiElement.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(choicesCAnonStorey39D.\u003C\u003Em__EF));
          // ISSUE: reference to a compiler-generated field
          this.m_choiceActors.Add(choicesCAnonStorey39D.actor);
        }
      }
      this.PositionAndShowChoices();
    }
  }

  private Actor LoadBestCardActor(EntityDef entityDef, TAG_PREMIUM premiumToUse, GhostCard.Type ghostCard = GhostCard.Type.NONE)
  {
    CardDef cardDef = DefLoader.Get().GetCardDef(entityDef.GetCardId(), new CardPortraitQuality(3, premiumToUse));
    GameObject gameObject = AssetLoader.Get().LoadActor(ActorNames.GetHandActor(entityDef, premiumToUse), false, false);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DeckHelper - FAILED to load actor \"{0}\"", (object) this.name));
      return (Actor) null;
    }
    Actor component = gameObject.GetComponent<Actor>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DeckHelper - ERROR actor \"{0}\" has no Actor component", (object) this.name));
      return (Actor) null;
    }
    component.transform.parent = this.transform;
    SceneUtils.SetLayer((Component) component, this.gameObject.layer);
    component.SetEntityDef(entityDef);
    component.SetCardDef(cardDef);
    component.SetPremium(premiumToUse);
    component.GhostCardEffect(ghostCard);
    component.UpdateAllComponents();
    component.Hide();
    component.gameObject.name = cardDef.name + "_actor";
    return component;
  }

  private void CleanOldChoices()
  {
    using (List<Actor>.Enumerator enumerator = this.m_choiceActors.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.m_choiceActors.Clear();
    if (!((UnityEngine.Object) this.m_replaceCardActor != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_replaceCardActor.gameObject);
    this.m_replaceCardActor = (Actor) null;
  }

  private void PositionAndShowChoices()
  {
    for (int index = 0; index < this.m_choiceActors.Count; ++index)
    {
      Actor choiceActor = this.m_choiceActors[index];
      choiceActor.transform.localPosition = this.m_cardSpacing * (float) index;
      choiceActor.Show();
      CollectionCardVisual.ShowActorShadow(choiceActor, true);
    }
    if ((UnityEngine.Object) this.m_replaceCardActor != (UnityEngine.Object) null)
      this.m_replaceCardActor.Show();
    if ((UnityEngine.Object) this.m_tileToRemove != (UnityEngine.Object) null)
      this.m_tileToRemove.SetHighlight(true);
    this.StartCoroutine(this.WaitAndAnimateChoices());
  }

  [DebuggerHidden]
  private IEnumerator WaitAndAnimateChoices()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckHelper.\u003CWaitAndAnimateChoices\u003Ec__Iterator55()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void FireStateChangedEvent()
  {
    foreach (DeckHelper.DelStateChangedListener stateChangedListener in this.m_listeners.ToArray())
      stateChangedListener(this.m_shown);
  }

  public void Show(DeckTrayDeckTileVisual tileToRemove, bool continueAfterReplace, bool replacingCard = false)
  {
    if (this.m_shown)
      return;
    Navigation.PushUnique(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    SoundManager.Get().LoadAndPlay("bar_button_A_press", this.gameObject);
    this.m_shown = true;
    this.m_rootObject.SetActive(true);
    if (!Options.Get().GetBool(Option.HAS_SEEN_DECK_HELPER, false) && UserAttentionManager.CanShowAttentionGrabber("DeckHelper.Show:" + (object) Option.HAS_SEEN_DECK_HELPER))
    {
      NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_ANNOUNCER_CM_HELP_DECK_50"), "VO_ANNOUNCER_CM_HELP_DECK_50", 0.0f, (Action) null, false);
      Options.Get().SetBool(Option.HAS_SEEN_DECK_HELPER, true);
    }
    if ((bool) UniversalInputManager.UsePhoneUI)
      FullScreenFXMgr.Get().StartStandardBlurVignette(0.1f);
    this.m_tileToRemove = tileToRemove;
    if ((UnityEngine.Object) this.m_tileToRemove != (UnityEngine.Object) null)
      this.m_cardToRemove = tileToRemove.GetActor().GetEntityDef();
    this.m_continueAfterReplace = continueAfterReplace;
    this.FireStateChangedEvent();
    this.UpdateChoices();
    NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL_TEMPLATE_REPLACE_1"), 0.0f);
    NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL_TEMPLATE_REPLACE_2"), 0.0f);
    NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL_REPLACE_WILD_CARDS"), 0.0f);
  }

  private bool OnNavigateBack()
  {
    this.Hide(false);
    return true;
  }

  public void Hide(bool popnavigation = true)
  {
    if (!this.m_shown)
      return;
    if (popnavigation)
      Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    this.m_shown = false;
    this.CleanOldChoices();
    this.m_rootObject.SetActive(false);
    if ((UnityEngine.Object) this.m_tileToRemove != (UnityEngine.Object) null)
      this.m_tileToRemove.SetHighlight(false);
    if ((bool) UniversalInputManager.UsePhoneUI)
      FullScreenFXMgr.Get().EndStandardBlurVignette(0.1f, (FullScreenFXMgr.EffectListener) null);
    this.FireStateChangedEvent();
  }

  private void ShowInnkeeperPopup()
  {
    if ((UnityEngine.Object) this.m_innkeeperPopup == (UnityEngine.Object) null)
      return;
    this.m_innkeeperPopup.gameObject.SetActive(true);
    this.m_innkeeperPopupShown = true;
    this.m_innkeeperPopup.gameObject.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    iTween.ScaleTo(this.m_innkeeperPopup.gameObject, iTween.Hash((object) "scale", (object) this.m_innkeeperFullScale, (object) "easetype", (object) iTween.EaseType.easeOutElastic, (object) "time", (object) 1f));
    this.StopCoroutine("WaitThenHidePopup");
    this.StartCoroutine("WaitThenHidePopup");
  }

  [DebuggerHidden]
  private IEnumerator WaitThenHidePopup()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckHelper.\u003CWaitThenHidePopup\u003Ec__Iterator56()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void InnkeeperPopupClicked(UIEvent e)
  {
    this.HideInnkeeperPopup();
  }

  private void HideInnkeeperPopup()
  {
    if ((UnityEngine.Object) this.m_innkeeperPopup == (UnityEngine.Object) null || !this.m_innkeeperPopupShown)
      return;
    this.m_innkeeperPopupShown = false;
    iTween.ScaleTo(this.m_innkeeperPopup.gameObject, iTween.Hash((object) "scale", (object) new Vector3(0.01f, 0.01f, 0.01f), (object) "easetype", (object) iTween.EaseType.easeInExpo, (object) "time", (object) 0.2f, (object) "oncomplete", (object) "FinishHidePopup", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void FinishHidePopup()
  {
    this.m_innkeeperPopup.gameObject.SetActive(false);
  }

  public void OnVisualRelease(Actor addCardActor, EntityDef cardToRemove)
  {
    TooltipPanelManager.Get().HideKeywordHelp();
    addCardActor.GetSpell(SpellType.DEATHREVERSE).ActivateState(SpellStateType.BIRTH);
    bool flag1 = cardToRemove != null;
    bool flag2 = this.m_continueAfterReplace;
    CollectionDeckTray collectionDeckTray = CollectionDeckTray.Get();
    CollectionDeck editingDeck = collectionDeckTray.GetCardsContent().GetEditingDeck();
    if (flag1)
    {
      int standard;
      int golden;
      CollectionManager.Get().GetOwnedCardCount(addCardActor.GetEntityDef().GetCardId(), out standard, out golden);
      int a = !this.m_ReplaceSingleTemplateCard ? (!cardToRemove.IsElite() ? 2 : 1) : 1;
      int invalidCardIdCount = editingDeck.GetInvalidCardIdCount(cardToRemove.GetCardId());
      Log.DeckHelper.Print("checking invalid card " + (object) editingDeck.IsWild + " " + (object) invalidCardIdCount + " " + (object) cardToRemove);
      int sameRemoveCount = Mathf.Min(a, standard + golden);
      int num1 = collectionDeckTray.RemoveClosestInvalidCard(cardToRemove, sameRemoveCount);
      Log.DeckHelper.Print("removed cards " + (object) num1);
      int num2 = 0;
      for (int index = 0; index < num1; ++index)
      {
        TAG_PREMIUM premium = TAG_PREMIUM.NORMAL;
        if (golden > 0)
        {
          --golden;
          premium = TAG_PREMIUM.GOLDEN;
        }
        else if (standard == 0)
          break;
        if (collectionDeckTray.AddCard(addCardActor.GetEntityDef(), premium, (DeckTrayDeckTileVisual) null, false, addCardActor))
          ++num2;
      }
      Log.DeckHelper.Print("did replace " + (object) num2 + " " + (object) invalidCardIdCount);
      if (num2 < invalidCardIdCount)
      {
        this.m_cardToRemove = cardToRemove;
        flag2 = true;
      }
    }
    else
      collectionDeckTray.AddCard(addCardActor.GetEntityDef(), addCardActor.GetPremium(), (DeckTrayDeckTileVisual) null, false, addCardActor);
    if (flag2)
      this.UpdateChoices();
    else
      this.Hide(true);
  }

  private void OnVisualOver(Actor actor)
  {
    SoundManager.Get().LoadAndPlay("collection_manager_card_mouse_over");
    actor.SetActorState(ActorStateType.CARD_MOUSE_OVER);
    TooltipPanelManager.Get().UpdateKeywordHelpForDeckHelper(actor.GetEntityDef(), actor);
  }

  private void OnVisualOut(Actor actor)
  {
    actor.SetActorState(ActorStateType.CARD_IDLE);
    TooltipPanelManager.Get().HideKeywordHelp();
  }

  public delegate void DelStateChangedListener(bool isActive);
}
