﻿// Decompiled with JetBrains decompiler
// Type: InputUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class InputUtil
{
  public static InputScheme GetInputScheme()
  {
    switch (Application.platform)
    {
      case RuntimePlatform.Android:
      case RuntimePlatform.IPhonePlayer:
        return InputScheme.TOUCH;
      case RuntimePlatform.PS3:
      case RuntimePlatform.XBOX360:
        return InputScheme.GAMEPAD;
      default:
        return InputScheme.KEYBOARD_MOUSE;
    }
  }

  public static bool IsMouseOnScreen()
  {
    if ((double) UniversalInputManager.Get().GetMousePosition().x >= 0.0 && (double) UniversalInputManager.Get().GetMousePosition().x <= (double) Screen.width && (double) UniversalInputManager.Get().GetMousePosition().y >= 0.0)
      return (double) UniversalInputManager.Get().GetMousePosition().y <= (double) Screen.height;
    return false;
  }

  public static bool IsPlayMakerMouseInputAllowed(GameObject go)
  {
    if ((Object) UniversalInputManager.Get() == (Object) null)
      return false;
    if (InputUtil.ShouldCheckGameplayForPlayMakerMouseInput(go))
    {
      GameState gameState = GameState.Get();
      if (gameState != null && gameState.IsMulliganManagerActive())
        return false;
      TargetReticleManager targetReticleManager = TargetReticleManager.Get();
      if ((Object) targetReticleManager != (Object) null && targetReticleManager.IsLocalArrowActive())
        return false;
    }
    return true;
  }

  private static bool ShouldCheckGameplayForPlayMakerMouseInput(GameObject go)
  {
    return !((Object) SceneMgr.Get() == (Object) null) && SceneMgr.Get().IsInGame() && (!((Object) LoadingScreen.Get() != (Object) null) || !LoadingScreen.Get().IsPreviousSceneActive() || !((Object) SceneUtils.FindComponentInThisOrParents<LoadingScreen>(go) != (Object) null)) && !((Object) SceneUtils.FindComponentInThisOrParents<BaseUI>(go) != (Object) null);
  }
}
