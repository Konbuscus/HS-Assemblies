﻿// Decompiled with JetBrains decompiler
// Type: BoxDiskStateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class BoxDiskStateInfo
{
  public Vector3 m_MainMenuRotation = new Vector3(0.0f, 0.0f, 180f);
  public float m_MainMenuDelaySec = 0.1f;
  public float m_MainMenuRotateSec = 0.17f;
  public Vector3 m_LoadingRotation = new Vector3(0.0f, 0.0f, 0.0f);
  public float m_LoadingDelaySec = 0.1f;
  public float m_LoadingRotateSec = 0.17f;
  public iTween.EaseType m_MainMenuRotateEaseType;
  public iTween.EaseType m_LoadingRotateEaseType;
}
