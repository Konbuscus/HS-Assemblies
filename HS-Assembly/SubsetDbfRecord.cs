﻿// Decompiled with JetBrains decompiler
// Type: SubsetDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SubsetDbfRecord : DbfRecord
{
  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    SubsetDbfAsset subsetDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (SubsetDbfAsset)) as SubsetDbfAsset;
    if ((UnityEngine.Object) subsetDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("SubsetDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < subsetDbfAsset.Records.Count; ++index)
      subsetDbfAsset.Records[index].StripUnusedLocales();
    records = (object) subsetDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (SubsetDbfRecord.\u003C\u003Ef__switch\u0024map6B == null)
      {
        // ISSUE: reference to a compiler-generated field
        SubsetDbfRecord.\u003C\u003Ef__switch\u0024map6B = new Dictionary<string, int>(1)
        {
          {
            "ID",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (SubsetDbfRecord.\u003C\u003Ef__switch\u0024map6B.TryGetValue(key, out num) && num == 0)
        return (object) this.ID;
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (SubsetDbfRecord.\u003C\u003Ef__switch\u0024map6C == null)
    {
      // ISSUE: reference to a compiler-generated field
      SubsetDbfRecord.\u003C\u003Ef__switch\u0024map6C = new Dictionary<string, int>(1)
      {
        {
          "ID",
          0
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!SubsetDbfRecord.\u003C\u003Ef__switch\u0024map6C.TryGetValue(key, out num) || num != 0)
      return;
    this.SetID((int) val);
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (SubsetDbfRecord.\u003C\u003Ef__switch\u0024map6D == null)
      {
        // ISSUE: reference to a compiler-generated field
        SubsetDbfRecord.\u003C\u003Ef__switch\u0024map6D = new Dictionary<string, int>(1)
        {
          {
            "ID",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (SubsetDbfRecord.\u003C\u003Ef__switch\u0024map6D.TryGetValue(key, out num) && num == 0)
        return typeof (int);
    }
    return (System.Type) null;
  }
}
