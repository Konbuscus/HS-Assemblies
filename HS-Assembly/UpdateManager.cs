﻿// Decompiled with JetBrains decompiler
// Type: UpdateManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;
using WTCG.BI;

public class UpdateManager : MonoBehaviour
{
  private Map<string, AssetBundle> m_assetBundles = new Map<string, AssetBundle>();
  private string m_error;
  private string[] m_remoteUri;
  private int m_remoteUriIndex;
  private int m_assetsVersion;
  private UpdateManager.UpdateProgress m_updateProgress;
  private WWW m_currentDownload;
  private bool m_updateIsRequired;
  private bool m_skipLoadingAssetBundle;
  private UpdateManager.InitCallback m_callback;
  private DataOnlyPatching.Status m_reportStatus;
  private static UpdateManager s_instance;

  private void Awake()
  {
    UpdateManager.s_instance = this;
  }

  private void OnDestroy()
  {
    UpdateManager.s_instance = (UpdateManager) null;
  }

  public static UpdateManager Get()
  {
    return UpdateManager.s_instance;
  }

  public void StartInitialize(int version, UpdateManager.InitCallback callback)
  {
    this.m_callback = callback;
    this.m_assetsVersion = version;
    this.StartCoroutine(this.Initialize());
  }

  [DebuggerHidden]
  public IEnumerator Initialize()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UpdateManager.\u003CInitialize\u003Ec__Iterator314() { \u003C\u003Ef__this = this };
  }

  public bool ContainsFile(string fileName)
  {
    return this.m_assetBundles.ContainsKey(fileName);
  }

  public AssetBundle GetAssetBundle(string fileName)
  {
    AssetBundle assetBundle = (AssetBundle) null;
    this.m_assetBundles.TryGetValue(fileName, out assetBundle);
    return assetBundle;
  }

  public string GetError()
  {
    return this.m_error;
  }

  public bool UpdateIsRequired()
  {
    return this.m_updateIsRequired;
  }

  public void SetLastFailedDOPVersion(bool success)
  {
    Log.UpdateManager.Print("Set LastFailedDOPVersion: {0}", (object) (!success ? this.m_assetsVersion : -1));
    Options.Get().SetInt(Option.LAST_FAILED_DOP_VERSION, !success ? this.m_assetsVersion : -1);
  }

  public bool StopWaitingForUpdate()
  {
    if (this.m_updateIsRequired)
    {
      UnityEngine.Debug.LogWarning((object) "Cannot stop waiting for update, update is required!");
      return false;
    }
    this.m_skipLoadingAssetBundle = true;
    this.CallCallback();
    return true;
  }

  private bool CurrentRegionIsCN()
  {
    return BattleNet.GetCurrentRegion() == constants.BnetRegion.REGION_CN;
  }

  private int GetTryCount()
  {
    return this.m_remoteUri.Length;
  }

  private string DownloadURI(string fileName)
  {
    return string.Format("{0}{1}", (object) (this.m_remoteUri[this.m_remoteUriIndex] + "win/" + string.Format("{0}/", (object) this.m_assetsVersion)), (object) fileName);
  }

  private void MoveToNextCDN()
  {
    this.m_remoteUriIndex = (this.m_remoteUriIndex + 1) % this.GetTryCount();
    Options.Get().SetInt(Option.PREFERRED_CDN_INDEX, this.m_remoteUriIndex);
  }

  private string CachedFileFolder()
  {
    return string.Format("Updates/{0}", (object) this.m_assetsVersion);
  }

  private string CachedFilePath(string fileName)
  {
    return string.Format("{0}/{1}", (object) this.CachedFileFolder(), (object) fileName);
  }

  private bool CalculateAndCompareMD5(byte[] buf, string md5)
  {
    string str = BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(buf)).Replace("-", string.Empty);
    Log.UpdateManager.Print("md5 expected {0} current {1}", new object[2]
    {
      (object) md5,
      (object) str
    });
    if (!(md5 == "0"))
      return str == md5;
    return true;
  }

  private void SetDownloadURI()
  {
    if (ApplicationMgr.IsPublic())
    {
      if (this.CurrentRegionIsCN())
        this.m_remoteUri = new string[4]
        {
          "http://client01.pdl.battlenet.com.cn/hs-pod/update/",
          "http://client02.pdl.battlenet.com.cn/hs-pod/update/",
          "http://client03.pdl.battlenet.com.cn/hs-pod/update/",
          "http://dist.blizzard.com/hs-pod/update/"
        };
      else
        this.m_remoteUri = new string[2]
        {
          "http://dist.blizzard.com/hs-pod/update/",
          "http://llnw.blizzard.com/hs-pod/update/"
        };
    }
    else
      this.m_remoteUri = Vars.Key("Application.CDN").GetStr("http://streaming-t5.corp.blizzard.net/hearthstone/update/").Split(';');
    foreach (string str in this.m_remoteUri)
      Log.UpdateManager.Print("CDN: {0}", (object) str);
  }

  private void LoadManifest(byte[] buf, ref List<UpdateManager.UpdateItem> list)
  {
    try
    {
      using (StreamReader streamReader = new StreamReader((Stream) new MemoryStream(buf)))
      {
        string str;
        while ((str = streamReader.ReadLine()) != null)
        {
          if (!string.IsNullOrEmpty(str))
          {
            string[] strArray = str.Split(';');
            if (strArray.Length != 3)
            {
              Log.UpdateManager.Print("Bad update manifest");
              this.m_error = "Bad update manifest";
              this.m_reportStatus = DataOnlyPatching.Status.FAILED_BAD_DATA;
              break;
            }
            if (strArray[0].ToLower() == "flag")
            {
              string lower = strArray[1].ToLower();
              if (lower != null)
              {
                // ISSUE: reference to a compiler-generated field
                if (UpdateManager.\u003C\u003Ef__switch\u0024mapCC == null)
                {
                  // ISSUE: reference to a compiler-generated field
                  UpdateManager.\u003C\u003Ef__switch\u0024mapCC = new Dictionary<string, int>(2)
                  {
                    {
                      "required",
                      0
                    },
                    {
                      "skip",
                      1
                    }
                  };
                }
                int num;
                // ISSUE: reference to a compiler-generated field
                if (UpdateManager.\u003C\u003Ef__switch\u0024mapCC.TryGetValue(lower, out num))
                {
                  if (num != 0)
                  {
                    if (num == 1)
                    {
                      this.m_updateProgress.maxPatchingBarDisplayTime = (float) Convert.ToInt32(strArray[2]);
                      continue;
                    }
                  }
                  else
                  {
                    this.m_updateIsRequired = strArray[2].ToLower() == "true";
                    continue;
                  }
                }
              }
              Error.AddDevFatal("UpdateManager: {0} is unsupported", (object) strArray[1]);
            }
            else
              list.Add(new UpdateManager.UpdateItem()
              {
                fileName = strArray[0],
                size = Convert.ToInt32(strArray[1]),
                md5 = strArray[2]
              });
          }
        }
      }
    }
    catch (Exception ex)
    {
      this.m_error = string.Format("LoadManifest Error: {0}", (object) ex.Message);
      this.m_reportStatus = DataOnlyPatching.Status.FAILED_BAD_DATA;
    }
  }

  [DebuggerHidden]
  private IEnumerator DownloadToCache(UpdateManager.UpdateItem item)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UpdateManager.\u003CDownloadToCache\u003Ec__Iterator315() { item = item, \u003C\u0024\u003Eitem = item, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator LoadFromCache(UpdateManager.UpdateItem item)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UpdateManager.\u003CLoadFromCache\u003Ec__Iterator316() { item = item, \u003C\u0024\u003Eitem = item, \u003C\u003Ef__this = this };
  }

  private void CallCallback()
  {
    if (this.m_callback == null)
      return;
    this.m_callback();
    this.m_callback = (UpdateManager.InitCallback) null;
  }

  public UpdateManager.UpdateProgress GetProgressForCurrentFile()
  {
    this.m_updateProgress.downloadPercentage = 0.0f;
    if (this.m_currentDownload != null)
      this.m_updateProgress.downloadPercentage = this.m_currentDownload.progress;
    return this.m_updateProgress;
  }

  private void InitValues()
  {
    this.m_assetBundles.Clear();
    this.m_updateProgress = new UpdateManager.UpdateProgress();
    this.m_currentDownload = (WWW) null;
    this.m_error = (string) null;
    this.m_updateIsRequired = false;
    this.m_skipLoadingAssetBundle = false;
    this.m_remoteUriIndex = Options.Get().GetInt(Option.PREFERRED_CDN_INDEX, 0) % this.m_remoteUri.Length;
    this.m_reportStatus = DataOnlyPatching.Status.SUCCEED_WITH_CACHE;
  }

  private class UpdateItem
  {
    public string fileName;
    public int size;
    public string md5;
    public byte[] bytes;
  }

  public class UpdateProgress
  {
    public int numFilesToDownload = 4;
    public float maxPatchingBarDisplayTime = 20f;
    public int numFilesDownloaded;
    public float downloadPercentage;
    public bool showProgressBar;
  }

  public delegate void InitCallback();
}
