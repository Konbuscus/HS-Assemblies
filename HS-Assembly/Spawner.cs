﻿// Decompiled with JetBrains decompiler
// Type: Spawner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Spawner : MonoBehaviour
{
  public bool destroyOnSpawn = true;
  public GameObject prefab;
  public bool spawnOnAwake;

  protected virtual void Awake()
  {
    if (!this.spawnOnAwake)
      return;
    this.Spawn();
  }

  public GameObject Spawn()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this.prefab);
    gameObject.transform.parent = this.transform.parent;
    TransformUtil.CopyLocal(gameObject, (Component) this.transform);
    SceneUtils.SetLayer(gameObject, this.gameObject.layer);
    if (this.destroyOnSpawn)
      Object.Destroy((Object) this.gameObject);
    return gameObject;
  }

  public T Spawn<T>() where T : MonoBehaviour
  {
    if ((Object) this.prefab.GetComponent<T>() != (Object) null)
      return this.Spawn().GetComponent<T>();
    Debug.Log((object) string.Format("The prefab for spawner {0} does not have component {1}", (object) this.gameObject.name, (object) typeof (T).Name));
    return (T) null;
  }
}
