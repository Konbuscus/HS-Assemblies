﻿// Decompiled with JetBrains decompiler
// Type: CardTextTool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CardTextTool : MonoBehaviour
{
  private const string PREFS_LOCALE = "CARD_TEXT_LOCALE";
  private const string PREFS_NAME = "CARD_TEXT_NAME";
  private const string PREFS_DESCRIPTION = "CARD_TEXT_DESCRIPTION";
  public GameObject m_CardsRoot;
  public Actor m_AbilityActor;
  public Actor m_AllyActor;
  public Actor m_WeaponActor;
  public Texture2D m_AbilityPortraitTexture;
  public Texture2D m_AllyPortraitTexture;
  public Texture2D m_WeaponPortraitTexture;
  public UberText m_AbilityCardDescription;
  public UberText m_AllyCardDescription;
  public UberText m_WeaponCardDescription;
  public InputField m_DescriptionInputFiled;
  public UberText m_AbilityCardName;
  public UberText m_AllyCardName;
  public UberText m_WeaponCardName;
  public InputField m_NameInputFiled;
  public Button m_LocaleDropDownMainButton;
  public Button m_LocaleDropDownSelectionButton;
  public List<CardTextTool.LocalizedFonts> m_LocalizedFonts;
  private string m_nameText;
  private string m_descriptionText;
  private Locale m_locale;

  private void Start()
  {
    GraphicsManager.Get().RenderQualityLevel = GraphicsQuality.Medium;
    Application.targetFrameRate = 20;
    Application.runInBackground = false;
    this.StartCoroutine(this.Initialize());
  }

  private void OnApplicationQuit()
  {
    PlayerPrefs.SetString("CARD_TEXT_NAME", this.m_nameText);
    PlayerPrefs.SetString("CARD_TEXT_DESCRIPTION", this.m_descriptionText);
    PlayerPrefs.Save();
  }

  public void UpdateDescriptionText()
  {
    string text = this.m_DescriptionInputFiled.text;
    this.m_descriptionText = text;
    string str = this.FixedNewline(text);
    this.m_AbilityCardDescription.Text = str;
    this.m_AllyCardDescription.Text = str;
    this.m_WeaponCardDescription.Text = str;
  }

  public void UpdateNameText()
  {
    string text = this.m_NameInputFiled.text;
    this.m_nameText = text;
    this.m_AbilityCardName.Text = text;
    this.m_AllyCardName.Text = text;
    this.m_WeaponCardName.Text = text;
  }

  public void PasteClipboard()
  {
    this.m_descriptionText = (string) typeof (GUIUtility).GetProperty("systemCopyBuffer", BindingFlags.Static | BindingFlags.NonPublic).GetValue((object) null, (object[]) null);
    this.m_DescriptionInputFiled.text = this.m_descriptionText;
    this.UpdateDescriptionText();
  }

  public void CopyToClipboard()
  {
    typeof (GUIUtility).GetProperty("systemCopyBuffer", BindingFlags.Static | BindingFlags.NonPublic).SetValue((object) null, (object) this.m_descriptionText, (object[]) null);
  }

  [DebuggerHidden]
  private IEnumerator Initialize()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardTextTool.\u003CInitialize\u003Ec__Iterator34B() { \u003C\u003Ef__this = this };
  }

  private string FixedNewline(string text)
  {
    if (text.Length < 2)
      return text;
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < text.Length; ++index)
    {
      if (index + 1 < text.Length && (int) text[index] == 92 && (int) text[index + 1] == 110)
      {
        stringBuilder.Append('\n');
        ++index;
      }
      else
        stringBuilder.Append(text[index]);
    }
    return stringBuilder.ToString();
  }

  private void SetupLocaleDropDown()
  {
    GameObject gameObject1 = this.m_LocaleDropDownSelectionButton.transform.parent.gameObject;
    gameObject1.SetActive(true);
    foreach (int num in Enum.GetValues(typeof (Locale)))
    {
      Locale locale = (Locale) num;
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      CardTextTool.\u003CSetupLocaleDropDown\u003Ec__AnonStorey46C downCAnonStorey46C = new CardTextTool.\u003CSetupLocaleDropDown\u003Ec__AnonStorey46C();
      // ISSUE: reference to a compiler-generated field
      downCAnonStorey46C.\u003C\u003Ef__this = this;
      if (locale != Locale.UNKNOWN)
      {
        GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.m_LocaleDropDownSelectionButton.gameObject);
        gameObject2.transform.parent = this.m_LocaleDropDownSelectionButton.transform.parent;
        Button component = gameObject2.GetComponent<Button>();
        component.GetComponentInChildren<UnityEngine.UI.Text>().text = locale.ToString();
        // ISSUE: reference to a compiler-generated field
        downCAnonStorey46C.locSet = locale;
        // ISSUE: reference to a compiler-generated method
        component.onClick.AddListener(new UnityAction(downCAnonStorey46C.\u003C\u003Em__372));
      }
    }
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_LocaleDropDownSelectionButton.gameObject);
    this.SetLocaleButtonText(this.m_locale);
    gameObject1.SetActive(false);
  }

  private void OnClick_LocaleSetButton(Locale locale)
  {
    this.m_LocaleDropDownMainButton.GetComponentInChildren<UnityEngine.UI.Text>().text = locale.ToString();
    this.m_locale = locale;
    this.SaveLocale(this.m_locale);
    this.SetLocale();
  }

  private void SetLocaleButtonText(Locale loc)
  {
    this.m_LocaleDropDownMainButton.GetComponentInChildren<UnityEngine.UI.Text>().text = loc.ToString();
  }

  private void SaveLocale(Locale loc)
  {
    PlayerPrefs.SetInt("CARD_TEXT_LOCALE", (int) this.m_locale);
    PlayerPrefs.Save();
  }

  private void SetLocale()
  {
    this.StartCoroutine(this.SetLocaleCoroutine());
  }

  [DebuggerHidden]
  private IEnumerator SetLocaleCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardTextTool.\u003CSetLocaleCoroutine\u003Ec__Iterator34C() { \u003C\u003Ef__this = this };
  }

  private void UpdateCardFonts(Locale loc)
  {
    using (List<CardTextTool.LocalizedFonts>.Enumerator enumerator = this.m_LocalizedFonts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardTextTool.LocalizedFonts current = enumerator.Current;
        if (current.m_Locale == loc)
        {
          if (current.m_FontDef.name == "FranklinGothic")
          {
            this.m_AbilityCardDescription.SetFontWithoutLocalization(current.m_FontDef);
            this.m_AllyCardDescription.SetFontWithoutLocalization(current.m_FontDef);
            this.m_WeaponCardDescription.SetFontWithoutLocalization(current.m_FontDef);
          }
          if (current.m_FontDef.name == "Belwe_Outline")
          {
            this.m_AbilityCardName.SetFontWithoutLocalization(current.m_FontDef);
            this.m_AllyCardName.SetFontWithoutLocalization(current.m_FontDef);
            this.m_WeaponCardName.SetFontWithoutLocalization(current.m_FontDef);
          }
        }
      }
    }
  }

  [Serializable]
  public class LocalizedFonts
  {
    public Locale m_Locale;
    public FontDef m_FontDef;
  }
}
