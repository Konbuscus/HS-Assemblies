﻿// Decompiled with JetBrains decompiler
// Type: EditorFacebookAccessToken
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using Facebook;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class EditorFacebookAccessToken : MonoBehaviour
{
  private float windowHeight = 200f;
  private string accessToken = string.Empty;
  private const float windowWidth = 592f;
  private bool isLoggingIn;
  private static GUISkin fbSkin;
  private GUIStyle greyButton;

  [DebuggerHidden]
  private IEnumerator Start()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    EditorFacebookAccessToken.\u003CStart\u003Ec__Iterator295 startCIterator295 = new EditorFacebookAccessToken.\u003CStart\u003Ec__Iterator295();
    return (IEnumerator) startCIterator295;
  }

  private void OnGUI()
  {
    float y = (float) (Screen.height / 2) - this.windowHeight / 2f;
    float x = (float) (Screen.width / 2) - 296f;
    if ((Object) EditorFacebookAccessToken.fbSkin != (Object) null)
    {
      GUI.skin = EditorFacebookAccessToken.fbSkin;
      this.greyButton = EditorFacebookAccessToken.fbSkin.GetStyle("greyButton");
    }
    else
      this.greyButton = GUI.skin.button;
    GUI.ModalWindow(this.GetHashCode(), new Rect(x, y, 592f, this.windowHeight), new GUI.WindowFunction(this.OnGUIDialog), "Unity Editor Facebook Login");
  }

  private void OnGUIDialog(int windowId)
  {
    GUI.enabled = !this.isLoggingIn;
    GUILayout.Space(10f);
    GUILayout.BeginHorizontal();
    GUILayout.BeginVertical();
    GUILayout.Space(10f);
    GUILayout.Label("User Access Token:");
    GUILayout.EndVertical();
    this.accessToken = GUILayout.TextField(this.accessToken, GUI.skin.textArea, new GUILayoutOption[1]
    {
      GUILayout.MinWidth(400f)
    });
    GUILayout.EndHorizontal();
    GUILayout.Space(20f);
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Find Access Token"))
      Application.OpenURL(string.Format("https://developers.facebook.com/tools/accesstoken/?app_id={0}", (object) FB.AppId));
    GUILayout.FlexibleSpace();
    GUIContent content1 = new GUIContent("Login");
    if (GUI.Button(GUILayoutUtility.GetRect(content1, GUI.skin.button), content1))
    {
      EditorFacebook component = FBComponentFactory.GetComponent<EditorFacebook>(IfNotExist.AddNew);
      component.AccessToken = this.accessToken;
      Dictionary<string, string> formData = new Dictionary<string, string>();
      formData["batch"] = "[{\"method\":\"GET\", \"relative_url\":\"me?fields=id\"},{\"method\":\"GET\", \"relative_url\":\"app?fields=id\"}]";
      formData["method"] = "POST";
      formData["access_token"] = this.accessToken;
      FB.API("/", HttpMethod.GET, new FacebookDelegate(component.MockLoginCallback), formData);
      this.isLoggingIn = true;
    }
    GUI.enabled = true;
    GUIContent content2 = new GUIContent("Cancel");
    Rect rect = GUILayoutUtility.GetRect(content2, this.greyButton);
    if (GUI.Button(rect, content2, this.greyButton))
    {
      FBComponentFactory.GetComponent<EditorFacebook>(IfNotExist.AddNew).MockCancelledLoginCallback();
      Object.Destroy((Object) this);
    }
    GUILayout.EndHorizontal();
    if (Event.current.type != EventType.Repaint)
      return;
    this.windowHeight = rect.y + rect.height + (float) GUI.skin.window.padding.bottom;
  }
}
