﻿// Decompiled with JetBrains decompiler
// Type: BoxEventType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum BoxEventType
{
  INVALID,
  STARTUP_HUB,
  STARTUP_TUTORIAL,
  TUTORIAL_PLAY,
  DISK_MAIN_MENU,
  DISK_LOADING,
  DOORS_CLOSE,
  DOORS_OPEN,
  DRAWER_OPEN,
  DRAWER_CLOSE,
  SHADOW_FADE_IN,
  SHADOW_FADE_OUT,
  STARTUP_SET_ROTATION,
}
