﻿// Decompiled with JetBrains decompiler
// Type: GhostCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GhostCard : MonoBehaviour
{
  public Vector3 m_CardOffset = Vector3.zero;
  private int m_PremiumRibbonIdx = -1;
  public Actor m_Actor;
  public RenderToTexture m_R2T_EffectGhost;
  public GameObject m_EffectRoot;
  public GameObject m_GlowPlane;
  public GameObject m_GlowPlaneElite;
  private static GhostStyleDef s_ghostStyles;
  private bool m_isBigCard;
  private bool m_Init;
  private RenderToTexture m_R2T_BaseCard;
  private GhostCard.Type m_ghostType;
  private int m_renderQueue;
  private GameObject m_CardMesh;
  private int m_CardFrontIdx;
  private GameObject m_PortraitMesh;
  private int m_PortraitFrameIdx;
  private GameObject m_NameMesh;
  private GameObject m_DescriptionMesh;
  private GameObject m_DescriptionTrimMesh;
  private GameObject m_RarityFrameMesh;
  private GameObject m_ManaCostMesh;
  private GameObject m_AttackMesh;
  private GameObject m_HealthMesh;
  private GameObject m_RacePlateMesh;
  private GameObject m_EliteMesh;
  private Material m_OrgMat_CardFront;
  private Material m_OrgMat_PremiumRibbon;
  private Material m_OrgMat_PortraitFrame;
  private Material m_OrgMat_Name;
  private Material m_OrgMat_Description;
  private Material m_OrgMat_Description2;
  private Material m_OrgMat_DescriptionTrim;
  private Material m_OrgMat_RarityFrame;
  private Material m_OrgMat_ManaCost;
  private Material m_OrgMat_Attack;
  private Material m_OrgMat_Health;
  private Material m_OrgMat_RacePlate;
  private Material m_OrgMat_Elite;

  public static GhostCard.Type GetGhostTypeFromSlot(CollectionDeck deck, CollectionDeckSlot slot)
  {
    GhostCard.Type type = GhostCard.Type.NONE;
    if (deck == null || slot == null)
      return type;
    if (!slot.Owned)
      type = GhostCard.Type.MISSING;
    else if (!deck.IsValidSlot(slot))
      type = GhostCard.Type.NOT_VALID;
    return type;
  }

  private void Awake()
  {
    if (!((Object) GhostCard.s_ghostStyles == (Object) null) || !((Object) AssetLoader.Get() != (Object) null))
      return;
    GhostCard.s_ghostStyles = AssetLoader.Get().LoadGameObject("GhostStyleDef", true, false).GetComponent<GhostStyleDef>();
  }

  private void OnDisable()
  {
    this.Disable();
  }

  private void OnDestroy()
  {
    if (!(bool) ((Object) this.m_EffectRoot))
      return;
    ParticleSystem componentInChildren = this.m_EffectRoot.GetComponentInChildren<ParticleSystem>();
    if (!(bool) ((Object) componentInChildren))
      return;
    componentInChildren.Stop();
  }

  public void SetBigCard(bool isBigCard)
  {
    this.m_isBigCard = isBigCard;
  }

  public void SetGhostType(GhostCard.Type ghostType)
  {
    this.m_ghostType = ghostType;
  }

  public void SetRenderQueue(int renderQueue)
  {
    this.m_renderQueue = renderQueue;
  }

  public void RenderGhostCard()
  {
    this.RenderGhostCard(false);
  }

  public void RenderGhostCard(bool forceRender)
  {
    this.RenderGhost(forceRender);
  }

  public void Reset()
  {
    this.m_Init = false;
  }

  private void RenderGhost()
  {
    this.RenderGhost(false);
  }

  private void RenderGhost(bool forceRender)
  {
    this.Init(forceRender);
    this.m_R2T_BaseCard.enabled = true;
    this.m_R2T_BaseCard.m_RenderQueue = this.m_renderQueue;
    if ((bool) ((Object) this.m_R2T_EffectGhost))
    {
      this.m_R2T_EffectGhost.enabled = true;
      this.m_R2T_EffectGhost.m_RenderQueue = this.m_renderQueue;
    }
    this.m_R2T_BaseCard.m_ObjectToRender = this.m_Actor.GetRootObject();
    this.m_Actor.GetRootObject().transform.localPosition = this.m_CardOffset;
    this.m_Actor.ShowAllText();
    this.ApplyGhostMaterials();
    this.m_R2T_BaseCard.Render();
    Material renderMaterial = this.m_R2T_BaseCard.GetRenderMaterial();
    if ((bool) ((Object) this.m_GlowPlane))
    {
      if (!this.m_Actor.IsElite())
        this.m_GlowPlane.GetComponent<Renderer>().enabled = true;
      else
        this.m_GlowPlane.GetComponent<Renderer>().enabled = false;
    }
    if ((bool) ((Object) this.m_GlowPlaneElite))
    {
      if (this.m_Actor.IsElite())
        this.m_GlowPlaneElite.GetComponent<Renderer>().enabled = true;
      else
        this.m_GlowPlaneElite.GetComponent<Renderer>().enabled = false;
    }
    if ((bool) ((Object) this.m_EffectRoot))
    {
      this.m_EffectRoot.transform.parent = (Transform) null;
      this.m_EffectRoot.transform.position = new Vector3(-500f, -500f, -500f);
      this.m_EffectRoot.transform.localScale = Vector3.one;
      if ((bool) ((Object) this.m_R2T_EffectGhost))
      {
        this.m_R2T_EffectGhost.enabled = true;
        RenderTexture renderTexture = this.m_R2T_EffectGhost.RenderNow();
        if ((Object) renderTexture != (Object) null)
        {
          renderMaterial.SetTexture("_FxTex", (Texture) renderTexture);
          if ((bool) ((Object) this.m_GlowPlane))
            this.m_GlowPlane.GetComponent<Renderer>().material.SetTexture("_FxTex", (Texture) renderTexture);
          if ((bool) ((Object) this.m_GlowPlaneElite))
            this.m_GlowPlaneElite.GetComponent<Renderer>().material.SetTexture("_FxTex", (Texture) renderTexture);
        }
      }
      ParticleSystem componentInChildren = this.m_EffectRoot.GetComponentInChildren<ParticleSystem>();
      if ((bool) ((Object) componentInChildren))
        componentInChildren.Play();
    }
    if ((bool) ((Object) this.m_GlowPlane))
      this.m_GlowPlane.GetComponent<Renderer>().material.renderQueue = 3071;
    if (!(bool) ((Object) this.m_GlowPlaneElite))
      return;
    this.m_GlowPlaneElite.GetComponent<Renderer>().material.renderQueue = 3071;
  }

  public void DisableGhost()
  {
    this.Disable();
    this.enabled = false;
  }

  private void Init(bool forceRender)
  {
    if (this.m_Init && !forceRender)
      return;
    if ((Object) this.m_Actor == (Object) null)
    {
      this.m_Actor = SceneUtils.FindComponentInThisOrParents<Actor>(this.gameObject);
      if ((Object) this.m_Actor == (Object) null)
      {
        Debug.LogError((object) string.Format("{0} Ghost card effect failed to find Actor!", (object) this.transform.root.name));
        this.enabled = false;
        return;
      }
    }
    this.m_CardMesh = this.m_Actor.m_cardMesh;
    this.m_CardFrontIdx = this.m_Actor.m_cardFrontMatIdx;
    this.m_PremiumRibbonIdx = this.m_Actor.m_premiumRibbon;
    this.m_PortraitMesh = this.m_Actor.m_portraitMesh;
    this.m_PortraitFrameIdx = this.m_Actor.m_portraitFrameMatIdx;
    this.m_NameMesh = this.m_Actor.m_nameBannerMesh;
    this.m_DescriptionMesh = this.m_Actor.m_descriptionMesh;
    this.m_DescriptionTrimMesh = this.m_Actor.m_descriptionTrimMesh;
    this.m_RarityFrameMesh = this.m_Actor.m_rarityFrameMesh;
    if ((bool) ((Object) this.m_Actor.m_attackObject))
    {
      Renderer component = this.m_Actor.m_attackObject.GetComponent<Renderer>();
      if ((Object) component != (Object) null)
        this.m_AttackMesh = component.gameObject;
      if ((Object) this.m_AttackMesh == (Object) null)
      {
        foreach (Renderer componentsInChild in this.m_Actor.m_attackObject.GetComponentsInChildren<Renderer>())
        {
          if (!(bool) ((Object) componentsInChild.GetComponent<UberText>()))
            this.m_AttackMesh = componentsInChild.gameObject;
        }
      }
    }
    if ((bool) ((Object) this.m_Actor.m_healthObject))
    {
      Renderer component = this.m_Actor.m_healthObject.GetComponent<Renderer>();
      if ((Object) component != (Object) null)
        this.m_HealthMesh = component.gameObject;
      if ((Object) this.m_HealthMesh == (Object) null)
      {
        foreach (Renderer componentsInChild in this.m_Actor.m_healthObject.GetComponentsInChildren<Renderer>())
        {
          if (!(bool) ((Object) componentsInChild.GetComponent<UberText>()))
            this.m_HealthMesh = componentsInChild.gameObject;
        }
      }
    }
    this.m_ManaCostMesh = this.m_Actor.m_manaObject;
    this.m_RacePlateMesh = this.m_Actor.m_racePlateObject;
    this.m_EliteMesh = this.m_Actor.m_eliteObject;
    this.StoreOrgMaterials();
    this.m_R2T_BaseCard = this.GetComponent<RenderToTexture>();
    this.m_R2T_BaseCard.m_ObjectToRender = this.m_Actor.GetRootObject();
    if ((bool) ((Object) this.m_R2T_BaseCard.m_Material) && this.m_R2T_BaseCard.m_Material.HasProperty("_Seed"))
      this.m_R2T_BaseCard.m_Material.SetFloat("_Seed", Random.Range(0.0f, 1f));
    if (this.m_Actor.IsMultiClass())
      this.m_Actor.GetMultiClassBanner().TurnOffShadowsAndFX();
    this.m_Init = true;
  }

  private void StoreOrgMaterials()
  {
    if ((bool) ((Object) this.m_CardMesh))
    {
      if (this.m_CardFrontIdx > -1)
        this.m_OrgMat_CardFront = this.m_CardMesh.GetComponent<Renderer>().materials[this.m_CardFrontIdx];
      if (this.m_PremiumRibbonIdx > -1)
        this.m_OrgMat_PremiumRibbon = this.m_CardMesh.GetComponent<Renderer>().materials[this.m_PremiumRibbonIdx];
    }
    if ((bool) ((Object) this.m_PortraitMesh) && this.m_PortraitFrameIdx > -1)
      this.m_OrgMat_PortraitFrame = this.m_PortraitMesh.GetComponent<Renderer>().materials[this.m_PortraitFrameIdx];
    if ((bool) ((Object) this.m_NameMesh))
      this.m_OrgMat_Name = this.m_NameMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_ManaCostMesh))
      this.m_OrgMat_ManaCost = this.m_ManaCostMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_AttackMesh))
      this.m_OrgMat_Attack = this.m_AttackMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_HealthMesh))
      this.m_OrgMat_Health = this.m_HealthMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_RacePlateMesh))
      this.m_OrgMat_RacePlate = this.m_RacePlateMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_RarityFrameMesh))
      this.m_OrgMat_RarityFrame = this.m_RarityFrameMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_DescriptionMesh))
    {
      Material[] materials = this.m_DescriptionMesh.GetComponent<Renderer>().materials;
      if (materials.Length > 1)
      {
        this.m_OrgMat_Description = materials[0];
        this.m_OrgMat_Description2 = materials[1];
      }
      else
        this.m_OrgMat_Description = this.m_DescriptionMesh.GetComponent<Renderer>().material;
    }
    if ((bool) ((Object) this.m_DescriptionTrimMesh))
      this.m_OrgMat_DescriptionTrim = this.m_DescriptionTrimMesh.GetComponent<Renderer>().material;
    if (!(bool) ((Object) this.m_EliteMesh))
      return;
    this.m_OrgMat_Elite = this.m_EliteMesh.GetComponent<Renderer>().material;
  }

  private void RestoreOrgMaterials()
  {
    this.ApplyMaterialByIdx(this.m_CardMesh, this.m_OrgMat_CardFront, this.m_CardFrontIdx);
    this.ApplyMaterialByIdx(this.m_CardMesh, this.m_OrgMat_PremiumRibbon, this.m_PremiumRibbonIdx);
    this.ApplyMaterialByIdx(this.m_PortraitMesh, this.m_OrgMat_PortraitFrame, this.m_PortraitFrameIdx);
    this.ApplyMaterialByIdx(this.m_DescriptionMesh, this.m_OrgMat_Description, 0);
    this.ApplyMaterialByIdx(this.m_DescriptionMesh, this.m_OrgMat_Description2, 1);
    this.ApplyMaterial(this.m_NameMesh, this.m_OrgMat_Name);
    this.ApplyMaterial(this.m_ManaCostMesh, this.m_OrgMat_ManaCost);
    this.ApplyMaterial(this.m_AttackMesh, this.m_OrgMat_Attack);
    this.ApplyMaterial(this.m_HealthMesh, this.m_OrgMat_Health);
    this.ApplyMaterial(this.m_RacePlateMesh, this.m_OrgMat_RacePlate);
    this.ApplyMaterial(this.m_RarityFrameMesh, this.m_OrgMat_RarityFrame);
    this.ApplyMaterial(this.m_DescriptionTrimMesh, this.m_OrgMat_DescriptionTrim);
    this.ApplyMaterial(this.m_EliteMesh, this.m_OrgMat_Elite);
  }

  private void ApplyGhostMaterials()
  {
    GhostStyle ghostStyle = this.m_ghostType != GhostCard.Type.NOT_VALID ? GhostCard.s_ghostStyles.m_missing : GhostCard.s_ghostStyles.m_invalid;
    Material material = Object.Instantiate<Material>(ghostStyle.m_GhostCardMaterial);
    if (this.m_isBigCard)
      material = Object.Instantiate<Material>(ghostStyle.m_GhostBigCardMaterial);
    this.m_R2T_BaseCard.m_Material = material;
    if ((bool) ((Object) this.m_R2T_EffectGhost))
      this.m_R2T_EffectGhost.m_Material = material;
    this.ApplyMaterialByIdx(this.m_CardMesh, ghostStyle.m_GhostMaterial, this.m_CardFrontIdx);
    this.ApplyMaterialByIdx(this.m_CardMesh, ghostStyle.m_GhostMaterial, this.m_PremiumRibbonIdx);
    this.ApplyMaterialByIdx(this.m_PortraitMesh, ghostStyle.m_GhostMaterial, this.m_PortraitFrameIdx);
    if ((bool) ((Object) this.m_GlowPlane))
    {
      if ((Object) this.m_AttackMesh != (Object) null)
        this.m_GlowPlane.GetComponent<Renderer>().material = ghostStyle.m_GhostMaterialGlowPlane;
      else
        this.m_GlowPlane.GetComponent<Renderer>().material = ghostStyle.m_GhostMaterialAbilityGlowPlane;
    }
    if ((bool) ((Object) this.m_GlowPlaneElite))
    {
      if ((Object) this.m_AttackMesh != (Object) null)
        this.m_GlowPlaneElite.GetComponent<Renderer>().material = ghostStyle.m_GhostMaterialGlowPlane;
      else
        this.m_GlowPlaneElite.GetComponent<Renderer>().material = ghostStyle.m_GhostMaterialAbilityGlowPlane;
    }
    this.ApplyMaterialByIdx(this.m_DescriptionMesh, ghostStyle.m_GhostMaterialMod2x, 0);
    this.ApplyMaterialByIdx(this.m_DescriptionMesh, ghostStyle.m_GhostMaterial, 1);
    this.ApplyMaterial(this.m_NameMesh, ghostStyle.m_GhostMaterial);
    this.ApplyMaterial(this.m_ManaCostMesh, ghostStyle.m_GhostMaterial);
    this.ApplyMaterial(this.m_AttackMesh, ghostStyle.m_GhostMaterial);
    this.ApplyMaterial(this.m_HealthMesh, ghostStyle.m_GhostMaterial);
    this.ApplyMaterial(this.m_RacePlateMesh, ghostStyle.m_GhostMaterial);
    this.ApplyMaterial(this.m_RarityFrameMesh, ghostStyle.m_GhostMaterial);
    if ((bool) ((Object) ghostStyle.m_GhostMaterialTransparent))
      this.ApplyMaterial(this.m_DescriptionTrimMesh, ghostStyle.m_GhostMaterialTransparent);
    this.ApplyMaterial(this.m_EliteMesh, ghostStyle.m_GhostMaterial);
    SceneUtils.SetRenderQueue(this.gameObject, this.m_R2T_BaseCard.m_RenderQueueOffset + this.m_renderQueue, true);
  }

  private void ApplyMaterial(GameObject go, Material mat)
  {
    if ((Object) go == (Object) null)
      return;
    Texture mainTexture = go.GetComponent<Renderer>().material.mainTexture;
    go.GetComponent<Renderer>().material = mat;
    go.GetComponent<Renderer>().material.mainTexture = mainTexture;
  }

  private void ApplyMaterialByIdx(GameObject go, Material mat, int idx)
  {
    if ((Object) go == (Object) null || (Object) mat == (Object) null || idx < 0)
      return;
    Material[] materials = go.GetComponent<Renderer>().materials;
    if (idx >= materials.Length)
      return;
    Texture mainTexture = go.GetComponent<Renderer>().materials[idx].mainTexture;
    Texture texture = (Texture) null;
    Material material = go.GetComponent<Renderer>().materials[idx];
    if ((Object) material == (Object) null)
      return;
    if (material.HasProperty("_SecondTex"))
      texture = material.GetTexture("_SecondTex");
    Color color = Color.clear;
    bool flag = material.HasProperty("_SecondTint");
    if (flag)
      color = material.GetColor("_SecondTint");
    materials[idx] = mat;
    go.GetComponent<Renderer>().materials = materials;
    go.GetComponent<Renderer>().materials[idx].mainTexture = mainTexture;
    if ((Object) texture != (Object) null)
      go.GetComponent<Renderer>().materials[idx].SetTexture("_SecondTex", texture);
    if (!flag)
      return;
    go.GetComponent<Renderer>().materials[idx].SetColor("_SecondTint", color);
  }

  private void ApplySharedMaterialByIdx(GameObject go, Material mat, int idx)
  {
    if ((Object) go == (Object) null || (Object) mat == (Object) null || idx < 0)
      return;
    Material[] materials = go.GetComponent<Renderer>().materials;
    if (idx >= materials.Length)
      return;
    Texture mainTexture = go.GetComponent<Renderer>().materials[idx].mainTexture;
    materials[idx] = mat;
    go.GetComponent<Renderer>().materials = materials;
    go.GetComponent<Renderer>().materials[idx].mainTexture = mainTexture;
  }

  private void Disable()
  {
    this.RestoreOrgMaterials();
    if ((bool) ((Object) this.m_R2T_BaseCard))
      this.m_R2T_BaseCard.enabled = false;
    if ((bool) ((Object) this.m_R2T_EffectGhost))
      this.m_R2T_EffectGhost.enabled = false;
    if ((bool) ((Object) this.m_GlowPlane))
      this.m_GlowPlane.GetComponent<Renderer>().enabled = false;
    if ((bool) ((Object) this.m_GlowPlaneElite))
      this.m_GlowPlaneElite.GetComponent<Renderer>().enabled = false;
    if (!(bool) ((Object) this.m_EffectRoot))
      return;
    ParticleSystem componentInChildren = this.m_EffectRoot.GetComponentInChildren<ParticleSystem>();
    if (!(bool) ((Object) componentInChildren))
      return;
    componentInChildren.Stop();
    componentInChildren.GetComponent<Renderer>().enabled = false;
  }

  public enum Type
  {
    NONE,
    MISSING_UNCRAFTABLE,
    MISSING,
    NOT_VALID,
  }
}
