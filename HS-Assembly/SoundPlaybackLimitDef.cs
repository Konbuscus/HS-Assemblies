﻿// Decompiled with JetBrains decompiler
// Type: SoundPlaybackLimitDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[Serializable]
public class SoundPlaybackLimitDef
{
  [CustomEditField(Label = "Playback Limit")]
  public int m_Limit = 1;
  [CustomEditField(Label = "Clip Group")]
  public List<SoundPlaybackLimitClipDef> m_ClipDefs = new List<SoundPlaybackLimitClipDef>();
}
