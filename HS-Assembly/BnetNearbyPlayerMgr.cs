﻿// Decompiled with JetBrains decompiler
// Type: BnetNearbyPlayerMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class BnetNearbyPlayerMgr
{
  private bool m_enabled = true;
  private List<BnetNearbyPlayerMgr.NearbyPlayer> m_nearbyPlayers = new List<BnetNearbyPlayerMgr.NearbyPlayer>();
  private List<BnetPlayer> m_nearbyBnetPlayers = new List<BnetPlayer>();
  private List<BnetPlayer> m_nearbyFriends = new List<BnetPlayer>();
  private List<BnetPlayer> m_nearbyStrangers = new List<BnetPlayer>();
  private object m_mutex = new object();
  private List<BnetNearbyPlayerMgr.NearbyPlayer> m_nearbyAdds = new List<BnetNearbyPlayerMgr.NearbyPlayer>();
  private List<BnetNearbyPlayerMgr.NearbyPlayer> m_nearbyUpdates = new List<BnetNearbyPlayerMgr.NearbyPlayer>();
  private List<BnetNearbyPlayerMgr.ChangeListener> m_changeListeners = new List<BnetNearbyPlayerMgr.ChangeListener>();
  private const int UDP_PORT = 1228;
  private const float UPDATE_INTERVAL = 12f;
  private const float INACTIVITY_TIMEOUT = 60f;
  private static BnetNearbyPlayerMgr s_instance;
  private bool m_listening;
  private ulong m_myGameAccountLo;
  private string m_bnetVersion;
  private string m_bnetEnvironment;
  private string m_idString;
  private bool m_availability;
  private UdpClient m_client;
  private int m_port;
  private float m_lastCallTime;

  public static BnetNearbyPlayerMgr Get()
  {
    if (BnetNearbyPlayerMgr.s_instance == null)
    {
      BnetNearbyPlayerMgr.s_instance = new BnetNearbyPlayerMgr();
      ApplicationMgr.Get().WillReset += new Action(BnetNearbyPlayerMgr.s_instance.Clear);
    }
    return BnetNearbyPlayerMgr.s_instance;
  }

  public void Initialize()
  {
    this.m_bnetVersion = BattleNet.GetVersion();
    this.m_bnetEnvironment = BattleNet.GetEnvironment();
    this.UpdateEnabled();
    Options.Get().RegisterChangedListener(Option.NEARBY_PLAYERS, new Options.ChangedCallback(this.OnEnabledOptionChanged));
    BnetFriendMgr.Get().AddChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
  }

  public void Shutdown()
  {
    if (this.m_listening)
      this.m_client.Close();
    Options.Get().UnregisterChangedListener(Option.NEARBY_PLAYERS, new Options.ChangedCallback(this.OnEnabledOptionChanged));
    BnetFriendMgr.Get().RemoveChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
  }

  public bool IsEnabled()
  {
    return Options.Get().GetBool(Option.NEARBY_PLAYERS) && this.m_enabled;
  }

  public void SetEnabled(bool enabled)
  {
    this.m_enabled = enabled;
    this.UpdateEnabled();
  }

  public bool GetNearbySessionStartTime(BnetPlayer bnetPlayer, out ulong sessionStartTime)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BnetNearbyPlayerMgr.\u003CGetNearbySessionStartTime\u003Ec__AnonStorey466 timeCAnonStorey466 = new BnetNearbyPlayerMgr.\u003CGetNearbySessionStartTime\u003Ec__AnonStorey466();
    // ISSUE: reference to a compiler-generated field
    timeCAnonStorey466.bnetPlayer = bnetPlayer;
    sessionStartTime = 0UL;
    // ISSUE: reference to a compiler-generated field
    if (timeCAnonStorey466.bnetPlayer == null)
      return false;
    BnetNearbyPlayerMgr.NearbyPlayer nearbyPlayer = (BnetNearbyPlayerMgr.NearbyPlayer) null;
    lock (this.m_mutex)
    {
      // ISSUE: reference to a compiler-generated method
      nearbyPlayer = this.m_nearbyPlayers.Find(new Predicate<BnetNearbyPlayerMgr.NearbyPlayer>(timeCAnonStorey466.\u003C\u003Em__349));
    }
    if (nearbyPlayer == null)
      return false;
    sessionStartTime = nearbyPlayer.m_sessionStartTime;
    return true;
  }

  public bool HasNearbyStrangers()
  {
    if (this.m_nearbyStrangers.Count > 0)
      return this.m_nearbyStrangers.Any<BnetPlayer>((Func<BnetPlayer, bool>) (p =>
      {
        if (p != null)
          return p.IsOnline();
        return false;
      }));
    return false;
  }

  public List<BnetPlayer> GetNearbyPlayers()
  {
    return this.m_nearbyBnetPlayers;
  }

  public List<BnetPlayer> GetNearbyFriends()
  {
    return this.m_nearbyFriends;
  }

  public List<BnetPlayer> GetNearbyStrangers()
  {
    return this.m_nearbyStrangers;
  }

  public bool IsNearbyPlayer(BnetPlayer player)
  {
    return this.FindNearbyPlayer(player) != null;
  }

  public bool IsNearbyPlayer(BnetGameAccountId id)
  {
    return this.FindNearbyPlayer(id) != null;
  }

  public bool IsNearbyPlayer(BnetAccountId id)
  {
    return this.FindNearbyPlayer(id) != null;
  }

  public bool IsNearbyFriend(BnetPlayer player)
  {
    return this.FindNearbyFriend(player) != null;
  }

  public bool IsNearbyFriend(BnetGameAccountId id)
  {
    return this.FindNearbyFriend(id) != null;
  }

  public bool IsNearbyFriend(BnetAccountId id)
  {
    return this.FindNearbyFriend(id) != null;
  }

  public bool IsNearbyStranger(BnetPlayer player)
  {
    return this.FindNearbyStranger(player) != null;
  }

  public bool IsNearbyStranger(BnetGameAccountId id)
  {
    return this.FindNearbyStranger(id) != null;
  }

  public bool IsNearbyStranger(BnetAccountId id)
  {
    return this.FindNearbyStranger(id) != null;
  }

  public BnetPlayer FindNearbyPlayer(BnetPlayer player)
  {
    return this.FindNearbyPlayer(player, this.m_nearbyBnetPlayers);
  }

  public BnetPlayer FindNearbyPlayer(BnetGameAccountId id)
  {
    return this.FindNearbyPlayer(id, this.m_nearbyBnetPlayers);
  }

  public BnetPlayer FindNearbyPlayer(BnetAccountId id)
  {
    return this.FindNearbyPlayer(id, this.m_nearbyBnetPlayers);
  }

  public BnetPlayer FindNearbyFriend(BnetGameAccountId id)
  {
    return this.FindNearbyPlayer(id, this.m_nearbyFriends);
  }

  public BnetPlayer FindNearbyFriend(BnetPlayer player)
  {
    return this.FindNearbyPlayer(player, this.m_nearbyFriends);
  }

  public BnetPlayer FindNearbyFriend(BnetAccountId id)
  {
    return this.FindNearbyPlayer(id, this.m_nearbyFriends);
  }

  public BnetPlayer FindNearbyStranger(BnetPlayer player)
  {
    return this.FindNearbyPlayer(player, this.m_nearbyStrangers);
  }

  public BnetPlayer FindNearbyStranger(BnetGameAccountId id)
  {
    return this.FindNearbyPlayer(id, this.m_nearbyStrangers);
  }

  public BnetPlayer FindNearbyStranger(BnetAccountId id)
  {
    return this.FindNearbyPlayer(id, this.m_nearbyStrangers);
  }

  public bool GetAvailability()
  {
    return this.m_availability;
  }

  public void SetAvailability(bool av)
  {
    this.m_availability = av;
  }

  public bool AddChangeListener(BnetNearbyPlayerMgr.ChangeCallback callback)
  {
    return this.AddChangeListener(callback, (object) null);
  }

  public bool AddChangeListener(BnetNearbyPlayerMgr.ChangeCallback callback, object userData)
  {
    BnetNearbyPlayerMgr.ChangeListener changeListener = new BnetNearbyPlayerMgr.ChangeListener();
    changeListener.SetCallback(callback);
    changeListener.SetUserData(userData);
    if (this.m_changeListeners.Contains(changeListener))
      return false;
    this.m_changeListeners.Add(changeListener);
    return true;
  }

  public bool RemoveChangeListener(BnetNearbyPlayerMgr.ChangeCallback callback)
  {
    return this.RemoveChangeListener(callback, (object) null);
  }

  public bool RemoveChangeListener(BnetNearbyPlayerMgr.ChangeCallback callback, object userData)
  {
    BnetNearbyPlayerMgr.ChangeListener changeListener = new BnetNearbyPlayerMgr.ChangeListener();
    changeListener.SetCallback(callback);
    changeListener.SetUserData(userData);
    return this.m_changeListeners.Remove(changeListener);
  }

  private void BeginListening()
  {
    if (this.m_listening)
      return;
    this.m_listening = true;
    IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 1228);
    UdpClient udpClient = new UdpClient();
    udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
    udpClient.Client.Bind((EndPoint) ipEndPoint);
    this.m_port = 1228;
    this.m_client = udpClient;
    BnetNearbyPlayerMgr.UdpState udpState = new BnetNearbyPlayerMgr.UdpState();
    udpState.e = ipEndPoint;
    udpState.u = this.m_client;
    this.m_lastCallTime = Time.realtimeSinceStartup;
    this.m_client.BeginReceive(new AsyncCallback(this.OnUdpReceive), (object) udpState);
  }

  private void OnUdpReceive(IAsyncResult ar)
  {
    UdpClient u = ((BnetNearbyPlayerMgr.UdpState) ar.AsyncState).u;
    IPEndPoint e = ((BnetNearbyPlayerMgr.UdpState) ar.AsyncState).e;
    byte[] bytes = u.EndReceive(ar, ref e);
    u.BeginReceive(new AsyncCallback(this.OnUdpReceive), ar.AsyncState);
    string[] strArray1 = Encoding.UTF8.GetString(bytes).Split(',');
    ulong result1 = 0;
    ulong result2 = 0;
    ulong result3 = 0;
    ulong result4 = 0;
    int result5 = 0;
    ulong result6 = 0;
    int num1 = 0;
    if (num1 >= strArray1.Length)
      return;
    string[] strArray2 = strArray1;
    int index1 = num1;
    int num2 = 1;
    int num3 = index1 + num2;
    if (!ulong.TryParse(strArray2[index1], out result1) || num3 >= strArray1.Length)
      return;
    string[] strArray3 = strArray1;
    int index2 = num3;
    int num4 = 1;
    int num5 = index2 + num4;
    if (!ulong.TryParse(strArray3[index2], out result2) || num5 >= strArray1.Length)
      return;
    string[] strArray4 = strArray1;
    int index3 = num5;
    int num6 = 1;
    int num7 = index3 + num6;
    if (!ulong.TryParse(strArray4[index3], out result3) || num7 >= strArray1.Length)
      return;
    string[] strArray5 = strArray1;
    int index4 = num7;
    int num8 = 1;
    int num9 = index4 + num8;
    if (!ulong.TryParse(strArray5[index4], out result4) || (long) this.m_myGameAccountLo == (long) result4 || num9 >= strArray1.Length)
      return;
    string[] strArray6 = strArray1;
    int index5 = num9;
    int num10 = 1;
    int num11 = index5 + num10;
    string name = strArray6[index5];
    if (num11 >= strArray1.Length)
      return;
    string[] strArray7 = strArray1;
    int index6 = num11;
    int num12 = 1;
    int num13 = index6 + num12;
    if (!int.TryParse(strArray7[index6], out result5) || num13 >= strArray1.Length)
      return;
    string[] strArray8 = strArray1;
    int index7 = num13;
    int num14 = 1;
    int num15 = index7 + num14;
    string str1 = strArray8[index7];
    if (string.IsNullOrEmpty(str1) || str1 != this.m_bnetVersion || num15 >= strArray1.Length)
      return;
    string[] strArray9 = strArray1;
    int index8 = num15;
    int num16 = 1;
    int num17 = index8 + num16;
    string str2 = strArray9[index8];
    if (string.IsNullOrEmpty(str2) || str2 != this.m_bnetEnvironment || num17 >= strArray1.Length)
      return;
    string[] strArray10 = strArray1;
    int index9 = num17;
    int num18 = 1;
    int num19 = index9 + num18;
    string str3 = strArray10[index9];
    bool available;
    if (str3 == "1")
    {
      available = true;
    }
    else
    {
      if (!(str3 == "0"))
        return;
      available = false;
    }
    if (num19 >= strArray1.Length)
      return;
    string[] strArray11 = strArray1;
    int index10 = num19;
    int num20 = 1;
    int num21 = index10 + num20;
    if (!ulong.TryParse(strArray11[index10], out result6))
      return;
    BnetBattleTag battleTag = new BnetBattleTag();
    battleTag.SetName(name);
    battleTag.SetNumber(result5);
    BnetAccountId id1 = new BnetAccountId();
    id1.SetHi(result1);
    id1.SetLo(result2);
    BnetAccount account = new BnetAccount();
    account.SetId(id1);
    account.SetBattleTag(battleTag);
    BnetGameAccountId id2 = new BnetGameAccountId();
    id2.SetHi(result3);
    id2.SetLo(result4);
    BnetGameAccount gameAccount = new BnetGameAccount();
    gameAccount.SetId(id2);
    gameAccount.SetBattleTag(battleTag);
    gameAccount.SetOnline(true);
    gameAccount.SetProgramId(BnetProgramId.HEARTHSTONE);
    gameAccount.SetGameField(1U, (object) available);
    gameAccount.SetGameField(19U, (object) str1);
    gameAccount.SetGameField(20U, (object) str2);
    BnetPlayer bnetPlayer = new BnetPlayer();
    bnetPlayer.SetAccount(account);
    bnetPlayer.AddGameAccount(gameAccount);
    BnetNearbyPlayerMgr.NearbyPlayer other = new BnetNearbyPlayerMgr.NearbyPlayer();
    other.m_bnetPlayer = bnetPlayer;
    other.m_availability = available;
    other.m_sessionStartTime = result6;
    lock (this.m_mutex)
    {
      if (!this.m_listening)
        return;
      using (List<BnetNearbyPlayerMgr.NearbyPlayer>.Enumerator resource_0 = this.m_nearbyAdds.GetEnumerator())
      {
        while (resource_0.MoveNext())
        {
          BnetNearbyPlayerMgr.NearbyPlayer local_25 = resource_0.Current;
          if (local_25.Equals(other))
          {
            this.UpdateNearbyPlayer(local_25, available, result6);
            return;
          }
        }
      }
      using (List<BnetNearbyPlayerMgr.NearbyPlayer>.Enumerator resource_1 = this.m_nearbyUpdates.GetEnumerator())
      {
        while (resource_1.MoveNext())
        {
          BnetNearbyPlayerMgr.NearbyPlayer local_27 = resource_1.Current;
          if (local_27.Equals(other))
          {
            this.UpdateNearbyPlayer(local_27, available, result6);
            return;
          }
        }
      }
      using (List<BnetNearbyPlayerMgr.NearbyPlayer>.Enumerator resource_2 = this.m_nearbyPlayers.GetEnumerator())
      {
        while (resource_2.MoveNext())
        {
          BnetNearbyPlayerMgr.NearbyPlayer local_29 = resource_2.Current;
          if (local_29.Equals(other))
          {
            this.UpdateNearbyPlayer(local_29, available, result6);
            this.m_nearbyUpdates.Add(local_29);
            return;
          }
        }
      }
      this.m_nearbyAdds.Add(other);
    }
  }

  private void StopListening()
  {
    if (!this.m_listening)
      return;
    this.m_listening = false;
    this.m_client.Close();
    BnetNearbyPlayerChangelist changelist = new BnetNearbyPlayerChangelist();
    lock (this.m_mutex)
    {
      using (List<BnetPlayer>.Enumerator resource_0 = this.m_nearbyBnetPlayers.GetEnumerator())
      {
        while (resource_0.MoveNext())
        {
          BnetPlayer local_2 = resource_0.Current;
          changelist.AddRemovedPlayer(local_2);
        }
      }
      using (List<BnetPlayer>.Enumerator resource_1 = this.m_nearbyFriends.GetEnumerator())
      {
        while (resource_1.MoveNext())
        {
          BnetPlayer local_4 = resource_1.Current;
          changelist.AddRemovedFriend(local_4);
        }
      }
      using (List<BnetPlayer>.Enumerator resource_2 = this.m_nearbyStrangers.GetEnumerator())
      {
        while (resource_2.MoveNext())
        {
          BnetPlayer local_6 = resource_2.Current;
          changelist.AddRemovedStranger(local_6);
        }
      }
      this.m_nearbyPlayers.Clear();
      this.m_nearbyBnetPlayers.Clear();
      this.m_nearbyFriends.Clear();
      this.m_nearbyStrangers.Clear();
      this.m_nearbyAdds.Clear();
      this.m_nearbyUpdates.Clear();
    }
    this.FireChangeEvent(changelist);
  }

  public void Update()
  {
    if (!this.m_listening)
      return;
    this.CacheMyAccountInfo();
    this.CheckIntervalAndBroadcast();
    this.ProcessPlayerChanges();
  }

  private void Clear()
  {
    lock (this.m_mutex)
    {
      this.m_nearbyPlayers.Clear();
      this.m_nearbyBnetPlayers.Clear();
      this.m_nearbyFriends.Clear();
      this.m_nearbyStrangers.Clear();
      this.m_nearbyAdds.Clear();
      this.m_nearbyUpdates.Clear();
    }
  }

  private void UpdateEnabled()
  {
    bool flag = this.IsEnabled();
    if (flag == this.m_listening)
      return;
    if (flag)
      this.BeginListening();
    else
      this.StopListening();
  }

  private void FireChangeEvent(BnetNearbyPlayerChangelist changelist)
  {
    if (changelist.IsEmpty())
      return;
    foreach (BnetNearbyPlayerMgr.ChangeListener changeListener in this.m_changeListeners.ToArray())
      changeListener.Fire(changelist);
  }

  private void CacheMyAccountInfo()
  {
    if (this.m_idString != null)
      return;
    BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
    if ((BnetEntityId) myGameAccountId == (BnetEntityId) null)
      return;
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    if (myPlayer == null)
      return;
    BnetAccountId accountId = myPlayer.GetAccountId();
    if ((BnetEntityId) accountId == (BnetEntityId) null)
      return;
    BnetBattleTag battleTag = myPlayer.GetBattleTag();
    if (battleTag == (BnetBattleTag) null)
      return;
    this.m_myGameAccountLo = myGameAccountId.GetLo();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.Append(accountId.GetHi());
    stringBuilder.Append(',');
    stringBuilder.Append(accountId.GetLo());
    stringBuilder.Append(',');
    stringBuilder.Append(myGameAccountId.GetHi());
    stringBuilder.Append(',');
    stringBuilder.Append(myGameAccountId.GetLo());
    stringBuilder.Append(',');
    stringBuilder.Append(battleTag.GetName());
    stringBuilder.Append(',');
    stringBuilder.Append(battleTag.GetNumber());
    stringBuilder.Append(',');
    stringBuilder.Append(BattleNet.GetVersion());
    stringBuilder.Append(',');
    stringBuilder.Append(BattleNet.GetEnvironment());
    this.m_idString = stringBuilder.ToString();
  }

  private void ProcessPlayerChanges()
  {
    BnetNearbyPlayerChangelist changelist = new BnetNearbyPlayerChangelist();
    lock (this.m_mutex)
    {
      this.ProcessAddedPlayers(changelist);
      this.ProcessUpdatedPlayers(changelist);
      this.RemoveInactivePlayers(changelist);
    }
    this.FireChangeEvent(changelist);
  }

  private void ProcessAddedPlayers(BnetNearbyPlayerChangelist changelist)
  {
    if (this.m_nearbyAdds.Count == 0)
      return;
    for (int index = 0; index < this.m_nearbyAdds.Count; ++index)
    {
      BnetNearbyPlayerMgr.NearbyPlayer nearbyAdd = this.m_nearbyAdds[index];
      nearbyAdd.m_lastReceivedTime = Time.realtimeSinceStartup;
      this.m_nearbyPlayers.Add(nearbyAdd);
      this.m_nearbyBnetPlayers.Add(nearbyAdd.m_bnetPlayer);
      changelist.AddAddedPlayer(nearbyAdd.m_bnetPlayer);
      if (nearbyAdd.IsFriend())
      {
        this.m_nearbyFriends.Add(nearbyAdd.m_bnetPlayer);
        changelist.AddAddedFriend(nearbyAdd.m_bnetPlayer);
      }
      else
      {
        this.m_nearbyStrangers.Add(nearbyAdd.m_bnetPlayer);
        changelist.AddAddedStranger(nearbyAdd.m_bnetPlayer);
      }
    }
    this.m_nearbyAdds.Clear();
  }

  private void ProcessUpdatedPlayers(BnetNearbyPlayerChangelist changelist)
  {
    if (this.m_nearbyUpdates.Count == 0)
      return;
    for (int index = 0; index < this.m_nearbyUpdates.Count; ++index)
    {
      BnetNearbyPlayerMgr.NearbyPlayer nearbyUpdate = this.m_nearbyUpdates[index];
      nearbyUpdate.m_lastReceivedTime = Time.realtimeSinceStartup;
      changelist.AddUpdatedPlayer(nearbyUpdate.m_bnetPlayer);
      if (nearbyUpdate.IsFriend())
        changelist.AddUpdatedFriend(nearbyUpdate.m_bnetPlayer);
      else
        changelist.AddUpdatedStranger(nearbyUpdate.m_bnetPlayer);
    }
    this.m_nearbyUpdates.Clear();
  }

  private void RemoveInactivePlayers(BnetNearbyPlayerChangelist changelist)
  {
    List<BnetNearbyPlayerMgr.NearbyPlayer> nearbyPlayerList = (List<BnetNearbyPlayerMgr.NearbyPlayer>) null;
    for (int index = 0; index < this.m_nearbyPlayers.Count; ++index)
    {
      BnetNearbyPlayerMgr.NearbyPlayer nearbyPlayer = this.m_nearbyPlayers[index];
      if ((double) (Time.realtimeSinceStartup - nearbyPlayer.m_lastReceivedTime) >= 60.0)
      {
        if (nearbyPlayerList == null)
          nearbyPlayerList = new List<BnetNearbyPlayerMgr.NearbyPlayer>();
        nearbyPlayerList.Add(nearbyPlayer);
      }
    }
    if (nearbyPlayerList == null)
      return;
    using (List<BnetNearbyPlayerMgr.NearbyPlayer>.Enumerator enumerator = nearbyPlayerList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetNearbyPlayerMgr.NearbyPlayer current = enumerator.Current;
        this.m_nearbyPlayers.Remove(current);
        if (this.m_nearbyBnetPlayers.Remove(current.m_bnetPlayer))
          changelist.AddRemovedPlayer(current.m_bnetPlayer);
        if (this.m_nearbyFriends.Remove(current.m_bnetPlayer))
          changelist.AddRemovedFriend(current.m_bnetPlayer);
        if (this.m_nearbyStrangers.Remove(current.m_bnetPlayer))
          changelist.AddRemovedStranger(current.m_bnetPlayer);
      }
    }
  }

  private bool CheckIntervalAndBroadcast()
  {
    if ((double) (Time.realtimeSinceStartup - this.m_lastCallTime) < 12.0)
      return false;
    this.m_lastCallTime = Time.realtimeSinceStartup;
    this.Broadcast();
    return true;
  }

  private void Broadcast()
  {
    byte[] bytes = Encoding.UTF8.GetBytes(this.CreateBroadcastString());
    IPEndPoint endPoint = new IPEndPoint(IPAddress.Broadcast, this.m_port);
    UdpClient udpClient = new UdpClient();
    udpClient.EnableBroadcast = true;
    try
    {
      udpClient.Send(bytes, bytes.Length, endPoint);
    }
    catch
    {
    }
    finally
    {
      udpClient.Close();
    }
  }

  private string CreateBroadcastString()
  {
    ulong sessionStartTime = HealthyGamingMgr.Get().GetSessionStartTime();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.Append(this.m_idString);
    stringBuilder.Append(',');
    stringBuilder.Append(!this.m_availability ? "0" : "1");
    stringBuilder.Append(',');
    stringBuilder.Append(sessionStartTime);
    return stringBuilder.ToString();
  }

  private int FindNearbyPlayerIndex(BnetPlayer bnetPlayer, List<BnetPlayer> bnetPlayers)
  {
    if (bnetPlayer == null)
      return -1;
    BnetAccountId accountId = bnetPlayer.GetAccountId();
    if ((BnetEntityId) accountId != (BnetEntityId) null)
      return this.FindNearbyPlayerIndex(accountId, bnetPlayers);
    return this.FindNearbyPlayerIndex(bnetPlayer.GetHearthstoneGameAccountId(), bnetPlayers);
  }

  private int FindNearbyPlayerIndex(BnetGameAccountId id, List<BnetPlayer> bnetPlayers)
  {
    if ((BnetEntityId) id == (BnetEntityId) null)
      return -1;
    for (int index = 0; index < bnetPlayers.Count; ++index)
    {
      BnetPlayer bnetPlayer = bnetPlayers[index];
      if ((BnetEntityId) id == (BnetEntityId) bnetPlayer.GetHearthstoneGameAccountId())
        return index;
    }
    return -1;
  }

  private int FindNearbyPlayerIndex(BnetAccountId id, List<BnetPlayer> bnetPlayers)
  {
    if ((BnetEntityId) id == (BnetEntityId) null)
      return -1;
    for (int index = 0; index < bnetPlayers.Count; ++index)
    {
      BnetPlayer bnetPlayer = bnetPlayers[index];
      if ((BnetEntityId) id == (BnetEntityId) bnetPlayer.GetAccountId())
        return index;
    }
    return -1;
  }

  private BnetPlayer FindNearbyPlayer(BnetPlayer bnetPlayer, List<BnetPlayer> bnetPlayers)
  {
    if (bnetPlayer == null)
      return (BnetPlayer) null;
    BnetAccountId accountId = bnetPlayer.GetAccountId();
    if ((BnetEntityId) accountId != (BnetEntityId) null)
      return this.FindNearbyPlayer(accountId, bnetPlayers);
    return this.FindNearbyPlayer(bnetPlayer.GetHearthstoneGameAccountId(), bnetPlayers);
  }

  private BnetPlayer FindNearbyPlayer(BnetGameAccountId id, List<BnetPlayer> bnetPlayers)
  {
    int nearbyPlayerIndex = this.FindNearbyPlayerIndex(id, bnetPlayers);
    if (nearbyPlayerIndex < 0)
      return (BnetPlayer) null;
    return bnetPlayers[nearbyPlayerIndex];
  }

  private BnetPlayer FindNearbyPlayer(BnetAccountId id, List<BnetPlayer> bnetPlayers)
  {
    int nearbyPlayerIndex = this.FindNearbyPlayerIndex(id, bnetPlayers);
    if (nearbyPlayerIndex < 0)
      return (BnetPlayer) null;
    return bnetPlayers[nearbyPlayerIndex];
  }

  private void UpdateNearbyPlayer(BnetNearbyPlayerMgr.NearbyPlayer player, bool available, ulong sessionStartTime)
  {
    player.GetGameAccount().SetGameField(1U, (object) available);
    player.m_sessionStartTime = sessionStartTime;
  }

  private void OnEnabledOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    this.UpdateEnabled();
  }

  private void OnFriendsChanged(BnetFriendChangelist friendChangelist, object userData)
  {
    List<BnetPlayer> addedFriends = friendChangelist.GetAddedFriends();
    List<BnetPlayer> removedFriends = friendChangelist.GetRemovedFriends();
    if ((addedFriends == null || addedFriends.Count <= 0) && (removedFriends == null || removedFriends.Count <= 0))
      return;
    BnetNearbyPlayerChangelist changelist = new BnetNearbyPlayerChangelist();
    lock (this.m_mutex)
    {
      if (addedFriends != null)
      {
        using (List<BnetPlayer>.Enumerator resource_0 = addedFriends.GetEnumerator())
        {
          while (resource_0.MoveNext())
          {
            int local_8 = this.FindNearbyPlayerIndex(resource_0.Current, this.m_nearbyStrangers);
            if (local_8 >= 0)
            {
              BnetPlayer local_9 = this.m_nearbyStrangers[local_8];
              this.m_nearbyStrangers.RemoveAt(local_8);
              this.m_nearbyFriends.Add(local_9);
              changelist.AddAddedFriend(local_9);
              changelist.AddRemovedStranger(local_9);
            }
          }
        }
      }
      if (removedFriends != null)
      {
        using (List<BnetPlayer>.Enumerator resource_1 = removedFriends.GetEnumerator())
        {
          while (resource_1.MoveNext())
          {
            int local_12 = this.FindNearbyPlayerIndex(resource_1.Current, this.m_nearbyFriends);
            if (local_12 >= 0)
            {
              BnetPlayer local_13 = this.m_nearbyFriends[local_12];
              this.m_nearbyFriends.RemoveAt(local_12);
              this.m_nearbyStrangers.Add(local_13);
              changelist.AddAddedStranger(local_13);
              changelist.AddRemovedFriend(local_13);
            }
          }
        }
      }
    }
    this.FireChangeEvent(changelist);
  }

  private class ChangeListener : EventListener<BnetNearbyPlayerMgr.ChangeCallback>
  {
    public void Fire(BnetNearbyPlayerChangelist changelist)
    {
      this.m_callback(changelist, this.m_userData);
    }
  }

  private class NearbyPlayer : IEquatable<BnetNearbyPlayerMgr.NearbyPlayer>
  {
    public float m_lastReceivedTime;
    public BnetPlayer m_bnetPlayer;
    public bool m_availability;
    public ulong m_sessionStartTime;

    public bool Equals(BnetNearbyPlayerMgr.NearbyPlayer other)
    {
      if (other == null)
        return false;
      return (BnetEntityId) this.GetGameAccountId() == (BnetEntityId) other.GetGameAccountId();
    }

    public BnetAccountId GetAccountId()
    {
      return this.m_bnetPlayer.GetAccountId();
    }

    public BnetGameAccountId GetGameAccountId()
    {
      return this.m_bnetPlayer.GetHearthstoneGameAccountId();
    }

    public BnetGameAccount GetGameAccount()
    {
      return this.m_bnetPlayer.GetHearthstoneGameAccount();
    }

    public bool IsFriend()
    {
      return BnetFriendMgr.Get().IsFriend(this.GetAccountId());
    }
  }

  private class UdpState
  {
    public UdpClient u;
    public IPEndPoint e;
  }

  public delegate void ChangeCallback(BnetNearbyPlayerChangelist changelist, object userData);
}
