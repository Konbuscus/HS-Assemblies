﻿// Decompiled with JetBrains decompiler
// Type: AdventureChooserButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class AdventureChooserButton : AdventureGenericButton
{
  [SerializeField]
  private float m_SubButtonHeight = 3.75f;
  [SerializeField]
  private float m_SubButtonContainerBtmPadding = 0.1f;
  [SerializeField]
  [CustomEditField(Sections = "Sub Button Settings")]
  public iTween.EaseType m_ActivateEaseType = iTween.EaseType.easeOutBounce;
  [SerializeField]
  [CustomEditField(Sections = "Sub Button Settings")]
  public iTween.EaseType m_DeactivateEaseType = iTween.EaseType.easeOutSine;
  [SerializeField]
  [CustomEditField(Sections = "Sub Button Settings")]
  public float m_SubButtonVisibilityPadding = 5f;
  [SerializeField]
  [CustomEditField(Sections = "Sub Button Settings")]
  public float m_SubButtonAnimationTime = 0.25f;
  private Vector3 m_MainButtonExtents = Vector3.zero;
  private List<AdventureChooserSubButton> m_SubButtons = new List<AdventureChooserSubButton>();
  private List<AdventureChooserButton.VisualUpdated> m_VisualUpdatedEventList = new List<AdventureChooserButton.VisualUpdated>();
  private List<AdventureChooserButton.Toggled> m_ToggleEventList = new List<AdventureChooserButton.Toggled>();
  private List<AdventureChooserButton.ModeSelection> m_ModeSelectionEventList = new List<AdventureChooserButton.ModeSelection>();
  private List<AdventureChooserButton.Expanded> m_ExpandedEventList = new List<AdventureChooserButton.Expanded>();
  private const string s_EventButtonExpand = "Expand";
  private const string s_EventButtonContract = "Contract";
  [CustomEditField(Sections = "Button State Table")]
  [SerializeField]
  public StateEventTable m_ButtonStateTable;
  [SerializeField]
  private float m_ButtonBottomPadding;
  [SerializeField]
  [CustomEditField(Sections = "Sub Button Settings")]
  public GameObject m_SubButtonContainer;
  [SerializeField]
  [CustomEditField(Sections = "Sub Button Settings")]
  public float m_SubButtonShowPosZ;
  private bool m_Toggled;
  private bool m_SelectSubButtonOnToggle;
  private AdventureDbId m_AdventureId;
  private AdventureChooserSubButton m_LastSelectedSubButton;

  [CustomEditField(Sections = "Button Settings")]
  public float ButtonBottomPadding
  {
    get
    {
      return this.m_ButtonBottomPadding;
    }
    set
    {
      this.m_ButtonBottomPadding = value;
      this.UpdateButtonPositions();
    }
  }

  [CustomEditField(Sections = "Sub Button Settings")]
  public float SubButtonHeight
  {
    get
    {
      return this.m_SubButtonHeight;
    }
    set
    {
      this.m_SubButtonHeight = value;
      this.UpdateButtonPositions();
    }
  }

  [CustomEditField(Sections = "Sub Button Settings")]
  public float SubButtonContainerBtmPadding
  {
    get
    {
      return this.m_SubButtonContainerBtmPadding;
    }
    set
    {
      this.m_SubButtonContainerBtmPadding = value;
      this.UpdateButtonPositions();
    }
  }

  [CustomEditField(Sections = "Button Settings")]
  public bool Toggle
  {
    get
    {
      return this.m_Toggled;
    }
    set
    {
      this.ToggleButton(value);
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.m_SubButtonContainer.SetActive(this.m_Toggled);
    this.m_SubButtonContainer.transform.localPosition = this.GetHiddenPosition();
    this.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ToggleButton(!this.m_Toggled)));
    if (!((UnityEngine.Object) this.m_PortraitRenderer != (UnityEngine.Object) null))
      return;
    this.m_MainButtonExtents = this.m_PortraitRenderer.bounds.extents;
  }

  public AdventureChooserSubButton[] GetSubButtons()
  {
    return this.m_SubButtons.ToArray();
  }

  public void SetAdventure(AdventureDbId id)
  {
    this.m_AdventureId = id;
  }

  public AdventureDbId GetAdventure()
  {
    return this.m_AdventureId;
  }

  public void SetSelectSubButtonOnToggle(bool flag)
  {
    this.m_SelectSubButtonOnToggle = flag;
  }

  public AdventureChooserSubButton CreateSubButton(AdventureModeDbId id, AdventureSubDef subDef, string subButtonPrefab, bool useAsLastSelected)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureChooserButton.\u003CCreateSubButton\u003Ec__AnonStorey34D buttonCAnonStorey34D = new AdventureChooserButton.\u003CCreateSubButton\u003Ec__AnonStorey34D();
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34D.\u003C\u003Ef__this = this;
    if ((UnityEngine.Object) this.m_SubButtonContainer == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "m_SubButtonContainer cannot be null. Unable to create subbutton.", (UnityEngine.Object) this);
      return (AdventureChooserSubButton) null;
    }
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34D.newsubbutton = GameUtils.LoadGameObjectWithComponent<AdventureChooserSubButton>(subButtonPrefab);
    // ISSUE: reference to a compiler-generated field
    if ((UnityEngine.Object) buttonCAnonStorey34D.newsubbutton == (UnityEngine.Object) null)
      return (AdventureChooserSubButton) null;
    // ISSUE: reference to a compiler-generated field
    GameUtils.SetParent((Component) buttonCAnonStorey34D.newsubbutton, this.m_SubButtonContainer, false);
    if (useAsLastSelected || (UnityEngine.Object) this.m_LastSelectedSubButton == (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated field
      this.m_LastSelectedSubButton = buttonCAnonStorey34D.newsubbutton;
    }
    // ISSUE: reference to a compiler-generated field
    GameUtils.SetAutomationName(buttonCAnonStorey34D.newsubbutton.gameObject, (object) id);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34D.newsubbutton.SetAdventure(this.m_AdventureId, id);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34D.newsubbutton.SetButtonText(subDef.GetShortName());
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34D.newsubbutton.SetPortraitTexture(subDef.m_Texture);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34D.newsubbutton.SetPortraitTiling(subDef.m_TextureTiling);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34D.newsubbutton.SetPortraitOffset(subDef.m_TextureOffset);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    buttonCAnonStorey34D.newsubbutton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(buttonCAnonStorey34D.\u003C\u003Em__1));
    // ISSUE: reference to a compiler-generated field
    this.m_SubButtons.Add(buttonCAnonStorey34D.newsubbutton);
    this.UpdateButtonPositions();
    this.m_SubButtonContainer.transform.localPosition = this.GetHiddenPosition();
    // ISSUE: reference to a compiler-generated field
    return buttonCAnonStorey34D.newsubbutton;
  }

  public void UpdateButtonPositions()
  {
    float subButtonHeight = this.m_SubButtonHeight;
    for (int index = 1; index < this.m_SubButtons.Count; ++index)
      TransformUtil.SetLocalPosZ((Component) this.m_SubButtons[index], subButtonHeight * (float) index);
  }

  public void AddVisualUpdatedListener(AdventureChooserButton.VisualUpdated dlg)
  {
    this.m_VisualUpdatedEventList.Add(dlg);
  }

  public void AddToggleListener(AdventureChooserButton.Toggled dlg)
  {
    this.m_ToggleEventList.Add(dlg);
  }

  public void AddModeSelectionListener(AdventureChooserButton.ModeSelection dlg)
  {
    this.m_ModeSelectionEventList.Add(dlg);
  }

  public void AddExpandedListener(AdventureChooserButton.Expanded dlg)
  {
    this.m_ExpandedEventList.Add(dlg);
  }

  public float GetFullButtonHeight()
  {
    if ((UnityEngine.Object) this.m_PortraitRenderer == (UnityEngine.Object) null || (UnityEngine.Object) this.m_SubButtonContainer == (UnityEngine.Object) null)
      return TransformUtil.GetBoundsOfChildren(this.gameObject).size.z;
    return Math.Max(this.m_PortraitRenderer.transform.localPosition.z + this.m_MainButtonExtents.z, this.m_SubButtonContainer.transform.localPosition.z + this.m_SubButtonHeight * (float) this.m_SubButtons.Count + this.m_SubButtonContainerBtmPadding) - (this.m_PortraitRenderer.transform.localPosition.z - this.m_MainButtonExtents.z) - this.m_ButtonBottomPadding;
  }

  public void DisableSubButtonHighlights()
  {
    using (List<AdventureChooserSubButton>.Enumerator enumerator = this.m_SubButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetHighlight(false);
    }
  }

  public bool ContainsSubButton(AdventureChooserSubButton btn)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_SubButtons.Exists(new Predicate<AdventureChooserSubButton>(new AdventureChooserButton.\u003CContainsSubButton\u003Ec__AnonStorey34E()
    {
      btn = btn
    }.\u003C\u003Em__2));
  }

  public void ToggleButton(bool toggle)
  {
    if (toggle == this.m_Toggled)
      return;
    this.m_Toggled = toggle;
    this.m_ButtonStateTable.CancelQueuedStates();
    this.m_ButtonStateTable.TriggerState(!this.m_Toggled ? "Contract" : "Expand", true, (string) null);
    if (this.m_Toggled)
      this.m_SubButtonContainer.SetActive(true);
    Vector3 hiddenPosition = this.GetHiddenPosition();
    Vector3 showPosition = this.GetShowPosition();
    Vector3 curr = !this.m_Toggled ? showPosition : hiddenPosition;
    Vector3 vector3 = !this.m_Toggled ? hiddenPosition : showPosition;
    this.m_SubButtonContainer.transform.localPosition = curr;
    this.UpdateSubButtonsVisibility(curr, this.m_SubButtonShowPosZ);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "islocal", (object) true, (object) "from", (object) curr, (object) "to", (object) vector3, (object) "time", (object) this.m_SubButtonAnimationTime, (object) "easeType", (object) (iTween.EaseType) (!this.m_Toggled ? (int) this.m_DeactivateEaseType : (int) this.m_ActivateEaseType), (object) "oncomplete", (object) "OnExpandAnimationComplete", (object) "oncompletetarget", (object) this.gameObject, (object) "onupdate", (object) (Action<object>) (newVal => this.OnButtonAnimating((Vector3) newVal, this.m_SubButtonShowPosZ)), (object) "onupdatetarget", (object) this.gameObject));
    this.FireToggleEvent();
    if (!this.m_Toggled || !this.m_SelectSubButtonOnToggle || !((UnityEngine.Object) this.m_LastSelectedSubButton != (UnityEngine.Object) null))
      return;
    this.OnSubButtonClicked(this.m_LastSelectedSubButton);
  }

  private Vector3 GetHiddenPosition()
  {
    Vector3 localPosition = this.m_SubButtonContainer.transform.localPosition;
    return new Vector3(localPosition.x, localPosition.y, this.m_SubButtonShowPosZ - this.m_SubButtonHeight * (float) this.m_SubButtons.Count - this.m_SubButtonContainerBtmPadding);
  }

  private Vector3 GetShowPosition()
  {
    Vector3 localPosition = this.m_SubButtonContainer.transform.localPosition;
    return new Vector3(localPosition.x, localPosition.y, this.m_SubButtonShowPosZ);
  }

  private void OnButtonAnimating(Vector3 curr, float zposshowlimit)
  {
    this.m_SubButtonContainer.transform.localPosition = curr;
    this.UpdateSubButtonsVisibility(curr, zposshowlimit);
    this.FireVisualUpdatedEvent();
  }

  private void UpdateSubButtonsVisibility(Vector3 curr, float zposshowlimit)
  {
    float subButtonHeight = this.m_SubButtonHeight;
    for (int index = 0; index < this.m_SubButtons.Count; ++index)
    {
      float num = subButtonHeight * (float) (index + 1) + curr.z;
      bool flag = (double) zposshowlimit - (double) num <= (double) this.m_SubButtonVisibilityPadding;
      GameObject gameObject = this.m_SubButtons[index].gameObject;
      if (gameObject.activeSelf != flag)
        gameObject.SetActive(flag);
    }
  }

  private void OnExpandAnimationComplete()
  {
    if (this.m_SubButtonContainer.activeSelf != this.m_Toggled)
      this.m_SubButtonContainer.SetActive(this.m_Toggled);
    this.FireExpandedEvent(this.m_Toggled);
  }

  private void FireVisualUpdatedEvent()
  {
    foreach (AdventureChooserButton.VisualUpdated visualUpdated in this.m_VisualUpdatedEventList.ToArray())
      visualUpdated();
  }

  private void FireToggleEvent()
  {
    foreach (AdventureChooserButton.Toggled toggled in this.m_ToggleEventList.ToArray())
      toggled(this.m_Toggled);
  }

  private void FireModeSelectedEvent(AdventureChooserSubButton btn)
  {
    foreach (AdventureChooserButton.ModeSelection modeSelection in this.m_ModeSelectionEventList.ToArray())
      modeSelection(btn);
  }

  private void FireExpandedEvent(bool expand)
  {
    foreach (AdventureChooserButton.Expanded expanded in this.m_ExpandedEventList.ToArray())
      expanded(this, expand);
  }

  private void OnSubButtonClicked(AdventureChooserSubButton btn)
  {
    this.m_LastSelectedSubButton = btn;
    this.FireModeSelectedEvent(btn);
    using (List<AdventureChooserSubButton>.Enumerator enumerator = this.m_SubButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureChooserSubButton current = enumerator.Current;
        current.SetHighlight((UnityEngine.Object) current == (UnityEngine.Object) btn);
      }
    }
  }

  public delegate void VisualUpdated();

  public delegate void Toggled(bool toggle);

  public delegate void ModeSelection(AdventureChooserSubButton btn);

  public delegate void Expanded(AdventureChooserButton button, bool expand);
}
