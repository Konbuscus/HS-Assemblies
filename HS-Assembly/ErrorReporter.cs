﻿// Decompiled with JetBrains decompiler
// Type: ErrorReporter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Security;
using System.Text;
using System.Xml;
using UnityEngine;

public class ErrorReporter : MonoBehaviour
{
  private static readonly HashSet<string> sentReports_ = new HashSet<string>();
  private List<string> m_previousExceptions = new List<string>();
  private IPAddress unknownAddress_ = new IPAddress(new byte[4]);
  private const int hearthstoneProjectID_ = 70;
  private static ErrorReporter instance_;
  private int sendCount_;

  public bool busy
  {
    get
    {
      return this.sendCount_ > 0;
    }
  }

  private IPAddress ipAddress
  {
    get
    {
      try
      {
        foreach (IPAddress address in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
        {
          if (address.AddressFamily == AddressFamily.InterNetwork)
            return address;
        }
      }
      catch (SocketException ex)
      {
      }
      catch (ArgumentException ex)
      {
      }
      return this.unknownAddress_;
    }
  }

  private static string localTime
  {
    get
    {
      return DateTime.Now.ToString("F", (IFormatProvider) CultureInfo.CreateSpecificCulture("en-US"));
    }
  }

  private string submitURL
  {
    get
    {
      return "http://iir.blizzard.com:3724/submit/" + (object) 70;
    }
  }

  public void send(string message, string stackTrace)
  {
    string hash = ErrorReporter.createHash(message + stackTrace);
    if (ErrorReporter.alreadySent(hash))
      return;
    ErrorReporter.sentReports_.Add(hash);
    try
    {
      byte[] bytes = Encoding.UTF8.GetBytes(ErrorReporter.buildMarkup(message, stackTrace, hash));
      WWWForm form = new WWWForm();
      form.AddBinaryData("file", bytes, "ReportedIssue.xml", "application/octet-stream");
      WWW www = new WWW(this.submitURL, form);
      ++this.sendCount_;
      this.StartCoroutine(this.wait(www));
    }
    catch (SecurityException ex)
    {
      this.unregister();
      Log.All.PrintError("Unable to send error report (security): {0}", (object) ex.Message);
    }
    catch (Exception ex)
    {
      this.unregister();
      Log.All.PrintError("Unable to send error report (unknown): {0}", (object) ex.Message);
    }
  }

  public static ErrorReporter Get()
  {
    return ErrorReporter.instance_;
  }

  private void Awake()
  {
    ErrorReporter.instance_ = this;
    this.register();
  }

  private void OnEnable()
  {
    this.register();
  }

  private void OnDisable()
  {
    this.unregister();
  }

  private void OnApplicationQuit()
  {
    this.unregister();
  }

  public void register()
  {
    Application.logMessageReceived += new Application.LogCallback(this.callback);
  }

  public void unregister()
  {
    Application.logMessageReceived -= new Application.LogCallback(this.callback);
  }

  private void callback(string message, string stackTrace, LogType logType)
  {
    switch (logType)
    {
      case LogType.Error:
        if (!Vars.Key("Application.SendErrors").GetBool(false))
          break;
        this.send(message, stackTrace);
        break;
      case LogType.Assert:
        if (!Vars.Key("Application.SendAsserts").GetBool(false))
          break;
        this.send(message, stackTrace);
        break;
      case LogType.Exception:
        if (Vars.Key("Application.SendExceptions").GetBool(true))
          this.send(message, stackTrace);
        this.reportUnhandledException(message, stackTrace);
        break;
    }
  }

  private static string buildMarkup(string title, string stackTrace, string hashBlock)
  {
    string escapedSgml = ErrorReporter.createEscapedSGML(stackTrace);
    return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ReportedIssue xmlns=\"http://schemas.datacontract.org/2004/07/Inspector.Models\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">\n\t<Summary>" + title + "</Summary>\n\t<Assertion>" + escapedSgml + "</Assertion>\n\t<HashBlock>" + hashBlock + "</HashBlock>\n\t<BuildNumber>" + (object) 15590 + "</BuildNumber>\n\t<Module>Hearthstone Client</Module>\n\t<EnteredBy>0</EnteredBy>\n\t<IssueType>Exception</IssueType>\n\t<ProjectId>" + (object) 70 + "</ProjectId>\n\t<Metadata><NameValuePairs>\n\t\t<NameValuePair><Name>Build</Name><Value>" + (object) 15590 + "</Value></NameValuePair>\n\t\t<NameValuePair><Name>OS.Platform</Name><Value>" + (object) Application.platform + "</Value></NameValuePair>\n\t\t<NameValuePair><Name>Unity.Version</Name><Value>" + Application.unityVersion + "</Value></NameValuePair>\n\t\t<NameValuePair><Name>Unity.Genuine</Name><Value>" + (object) Application.genuine + "</Value></NameValuePair>\n\t\t<NameValuePair><Name>Locale</Name><Value>" + Localization.GetLocaleName() + "</Value></NameValuePair>\n\t</NameValuePairs></Metadata>\n</ReportedIssue>\n";
  }

  private static string createHash(string blob)
  {
    return Crypto.SHA1.Calc(blob);
  }

  private static string createEscapedSGML(string blob)
  {
    XmlElement element = new XmlDocument().CreateElement("root");
    element.InnerText = blob;
    return element.InnerXml;
  }

  [DebuggerHidden]
  private IEnumerator wait(WWW www)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ErrorReporter.\u003Cwait\u003Ec__Iterator308() { www = www, \u003C\u0024\u003Ewww = www, \u003C\u003Ef__this = this };
  }

  private void reportUnhandledException(string message, string stackTrace)
  {
    string hash = ErrorReporter.createHash(message + stackTrace);
    if (this.m_previousExceptions.Contains(hash))
      return;
    this.m_previousExceptions.Add(hash);
    Error.AddDevFatal("Uncaught Exception!\n{0}\nAt:\n{1}", (object) message, (object) stackTrace);
  }

  private static bool alreadySent(string hash)
  {
    return ErrorReporter.sentReports_.Contains(hash);
  }
}
