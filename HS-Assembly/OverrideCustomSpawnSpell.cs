﻿// Decompiled with JetBrains decompiler
// Type: OverrideCustomSpawnSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class OverrideCustomSpawnSpell : SuperSpell
{
  public Spell m_CustomSpawnSpell;

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    using (List<GameObject>.Enumerator enumerator = this.GetVisualTargets().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          current.GetComponent<Card>().OverrideCustomSpawnSpell(Object.Instantiate<Spell>(this.m_CustomSpawnSpell));
      }
    }
    --this.m_effectsPendingFinish;
    this.FinishIfPossible();
  }
}
