﻿// Decompiled with JetBrains decompiler
// Type: FullDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FullDef
{
  private EntityDef m_entityDef;
  private CardDef m_cardDef;

  public bool IsEmpty()
  {
    return this.m_entityDef == null && !((Object) this.m_cardDef != (Object) null);
  }

  public EntityDef GetEntityDef()
  {
    return this.m_entityDef;
  }

  public void SetEntityDef(EntityDef entityDef)
  {
    this.m_entityDef = entityDef;
  }

  public CardDef GetCardDef()
  {
    return this.m_cardDef;
  }

  public void SetCardDef(CardDef cardDef)
  {
    this.m_cardDef = cardDef;
  }
}
