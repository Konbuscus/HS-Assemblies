﻿// Decompiled with JetBrains decompiler
// Type: SpellState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class SpellState : MonoBehaviour
{
  private bool m_shown = true;
  public SpellStateType m_StateType;
  public float m_StartDelaySec;
  public List<SpellStateAnimObject> m_ExternalAnimatedObjects;
  public List<SpellStateAudioSource> m_AudioSources;
  private Spell m_spell;
  private bool m_playing;
  private bool m_initialized;

  private void Start()
  {
    this.m_spell = SceneUtils.FindComponentInParents<Spell>(this.gameObject);
    for (int index = 0; index < this.m_ExternalAnimatedObjects.Count; ++index)
      this.m_ExternalAnimatedObjects[index].Init();
    for (int index = 0; index < this.m_AudioSources.Count; ++index)
      this.m_AudioSources[index].Init();
    this.m_initialized = true;
    if (this.m_shown && this.m_playing)
      this.PlayImpl();
    else
      this.StopImpl((List<SpellState>) null);
  }

  public void Play()
  {
    if (this.m_playing || !this.m_shown)
      return;
    this.m_playing = true;
    if (!this.m_initialized)
      return;
    this.PlayImpl();
  }

  public void Stop(List<SpellState> nextStateList)
  {
    if (!this.m_playing)
      return;
    this.m_playing = false;
    if (!this.m_initialized)
      return;
    this.StopImpl(nextStateList);
  }

  public void ShowState()
  {
    if (this.m_shown)
      return;
    this.m_shown = true;
    if (!this.m_initialized || !this.m_playing)
      return;
    this.PlayImpl();
  }

  public void HideState()
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    if (!this.m_initialized || !this.m_playing)
      return;
    this.StopImpl((List<SpellState>) null);
  }

  public void OnLoad()
  {
    this.gameObject.SetActive(true);
    using (List<SpellStateAnimObject>.Enumerator enumerator = this.m_ExternalAnimatedObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnLoad(this);
    }
  }

  private void OnStateFinished()
  {
    this.m_spell.OnStateFinished();
  }

  private void OnSpellFinished()
  {
    this.m_spell.OnSpellFinished();
  }

  private void OnChangeState(SpellStateType stateType)
  {
    this.m_spell.ChangeState(stateType);
  }

  [DebuggerHidden]
  private IEnumerator DelayedPlay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpellState.\u003CDelayedPlay\u003Ec__Iterator31E() { \u003C\u003Ef__this = this };
  }

  private void PlayImpl()
  {
    this.gameObject.SetActive(true);
    if (Mathf.Approximately(this.m_StartDelaySec, 0.0f))
      this.PlayNow();
    else
      this.StartCoroutine(this.DelayedPlay());
  }

  private void StopImpl(List<SpellState> nextStateList)
  {
    if (nextStateList == null)
    {
      using (List<SpellStateAnimObject>.Enumerator enumerator = this.m_ExternalAnimatedObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Stop();
      }
    }
    else
    {
      using (List<SpellStateAnimObject>.Enumerator enumerator = this.m_ExternalAnimatedObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Stop(nextStateList);
      }
    }
    using (List<SpellStateAudioSource>.Enumerator enumerator = this.m_AudioSources.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Stop();
    }
    this.gameObject.SetActive(false);
  }

  private void PlayNow()
  {
    using (List<SpellStateAnimObject>.Enumerator enumerator = this.m_ExternalAnimatedObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Play();
    }
    using (List<SpellStateAudioSource>.Enumerator enumerator = this.m_AudioSources.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Play(this);
    }
  }
}
