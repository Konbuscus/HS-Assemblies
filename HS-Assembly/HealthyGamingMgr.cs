﻿// Decompiled with JetBrains decompiler
// Type: HealthyGamingMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class HealthyGamingMgr : MonoBehaviour
{
  private string m_AccountCountry = string.Empty;
  private bool m_HealthyGamingArenaEnabled = true;
  private const float CAIS_MESSAGE_DISPLAY_TIME = 60f;
  private const float CAIS_ACTIVE_MESSAGE_DISPLAY_TIME = 60f;
  private const float KOREA_MESSAGE_DISPLAY_TIME = 5f;
  private const float CHECK_INTERVAL = 300f;
  private bool m_BattleNetReady;
  private bool m_FeaturesPacketReady;
  private Lockouts m_Restrictions;
  private bool m_NetworkDataReady;
  private float m_NextCheckTime;
  private float m_NextMessageDisplayTime;
  private int m_TimePlayed;
  private int m_TimeRested;
  private ulong m_SessionStartTime;
  private bool m_DebugMode;
  private static HealthyGamingMgr s_Instance;

  private bool IsInitializationReady
  {
    get
    {
      return this.m_BattleNetReady && this.m_FeaturesPacketReady;
    }
  }

  private void Awake()
  {
    HealthyGamingMgr.s_Instance = this;
    if (Options.Get().GetBool(Option.HEALTHY_GAMING_DEBUG, false))
      this.m_DebugMode = true;
    this.m_NextCheckTime = Time.realtimeSinceStartup + 45f;
    ApplicationMgr.Get().WillReset += new System.Action(this.WillReset);
    ApplicationMgr.Get().Resetting += new System.Action(this.OnReset);
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    this.StartCoroutine(this.InitNetworkData());
  }

  private void Update()
  {
    if (!this.m_NetworkDataReady || (double) Time.realtimeSinceStartup < (double) this.m_NextCheckTime)
      return;
    this.m_NextCheckTime = Time.realtimeSinceStartup + 300f;
    string accountCountry = this.m_AccountCountry;
    if (accountCountry != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (HealthyGamingMgr.\u003C\u003Ef__switch\u0024mapC8 == null)
      {
        // ISSUE: reference to a compiler-generated field
        HealthyGamingMgr.\u003C\u003Ef__switch\u0024mapC8 = new Dictionary<string, int>(2)
        {
          {
            "CHN",
            0
          },
          {
            "KOR",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (HealthyGamingMgr.\u003C\u003Ef__switch\u0024mapC8.TryGetValue(accountCountry, out num))
      {
        if (num != 0)
        {
          if (num == 1)
          {
            this.KoreaRestrictions();
            return;
          }
        }
        else
        {
          this.ChinaRestrictions();
          return;
        }
      }
    }
    this.enabled = false;
  }

  private void OnDestroy()
  {
    ApplicationMgr.Get().WillReset -= new System.Action(this.WillReset);
    ApplicationMgr.Get().Resetting -= new System.Action(this.OnReset);
    FatalErrorMgr.Get().RemoveErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    HealthyGamingMgr.s_Instance = (HealthyGamingMgr) null;
  }

  public static HealthyGamingMgr Get()
  {
    return HealthyGamingMgr.s_Instance;
  }

  public void OnLoggedIn()
  {
    this.m_BattleNetReady = true;
    NetCache.Get().RegisterFeatures(new NetCache.NetCacheCallback(this.OnFeaturesReady));
  }

  public bool isArenaEnabled()
  {
    return this.m_HealthyGamingArenaEnabled;
  }

  public ulong GetSessionStartTime()
  {
    return this.m_SessionStartTime;
  }

  private void WillReset()
  {
    this.StopCoroutinesAndResetState();
  }

  private void OnReset()
  {
    this.StartCoroutine(this.InitNetworkData());
  }

  private void OnFatalError(FatalErrorMessage message, object userData)
  {
    this.StopCoroutinesAndResetState();
  }

  private void OnFeaturesReady()
  {
    if (NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>() != null)
      this.m_FeaturesPacketReady = true;
    else
      Log.HealthyGaming.PrintError("HealthyGamingMgr.OnFeaturesReady: features packet received by NetCacheFeatures is null.");
  }

  private void StopCoroutinesAndResetState()
  {
    this.m_BattleNetReady = false;
    this.m_NetworkDataReady = false;
    this.m_FeaturesPacketReady = false;
    this.StopAllCoroutines();
  }

  [DebuggerHidden]
  private IEnumerator InitNetworkData()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HealthyGamingMgr.\u003CInitNetworkData\u003Ec__Iterator30A() { \u003C\u003Ef__this = this };
  }

  private void KoreaRestrictions()
  {
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if (this.m_DebugMode)
      Log.HealthyGaming.Print("Minutes Played: " + (object) (float) ((double) realtimeSinceStartup / 60.0));
    if ((double) realtimeSinceStartup < (double) this.m_NextMessageDisplayTime)
      return;
    this.m_NextMessageDisplayTime += 3600f;
    SocialToastMgr.Get().AddToast(UserAttentionBlocker.ALL_EXCEPT_FATAL_ERROR_SCENE, GameStrings.Format("GLOBAL_HEALTHY_GAMING_TOAST", (object) ((int) ((double) realtimeSinceStartup / 60.0) / 60)), SocialToastMgr.TOAST_TYPE.DEFAULT, 5f);
  }

  private void ChinaRestrictions()
  {
    BattleNet.GetPlayRestrictions(ref this.m_Restrictions, true);
    this.StartCoroutine(this.ChinaRestrictionsUpdate());
  }

  [DebuggerHidden]
  private IEnumerator ChinaRestrictionsUpdate()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HealthyGamingMgr.\u003CChinaRestrictionsUpdate\u003Ec__Iterator30B() { \u003C\u003Ef__this = this };
  }

  private void ChinaRestrictions_LessThan3Hours(int minutesPlayed, int hours)
  {
    this.m_NextMessageDisplayTime = (double) this.m_NextMessageDisplayTime >= 0.0 ? (float) (this.m_TimePlayed + 60) : (float) (this.m_TimePlayed + (60 - minutesPlayed % 60));
    string key = "GLOBAL_HEALTHY_GAMING_CHINA_LESS_THAN_THREE_HOURS";
    if ((bool) UniversalInputManager.UsePhoneUI && GameStrings.HasKey(key + "_PHONE"))
      key += "_PHONE";
    string textArg = GameStrings.Format(key, (object) hours);
    SocialToastMgr.Get().AddToast(UserAttentionBlocker.ALL_EXCEPT_FATAL_ERROR_SCENE, textArg, SocialToastMgr.TOAST_TYPE.DEFAULT, 60f);
    if (this.m_DebugMode)
    {
      Log.HealthyGaming.Print("GLOBAL_HEALTHY_GAMING_CHINA_LESS_THAN_THREE_HOURS: " + minutesPlayed.ToString());
      Log.HealthyGaming.Print(GameStrings.Format("GLOBAL_HEALTHY_GAMING_CHINA_LESS_THAN_THREE_HOURS", (object) hours));
      Log.HealthyGaming.Print("NextMessageDisplayTime: " + this.m_NextMessageDisplayTime.ToString());
    }
    else
      Log.HealthyGaming.Print(string.Format("Time: {0},  Played: {1},  {2}", (object) Time.realtimeSinceStartup, (object) minutesPlayed, (object) textArg));
  }

  private void ChinaRestrictions_3to5Hours(int minutesPlayed)
  {
    this.m_NextMessageDisplayTime = (double) this.m_NextMessageDisplayTime >= 0.0 ? (float) (this.m_TimePlayed + 30) : (float) (this.m_TimePlayed + (30 - minutesPlayed % 30));
    string key = "GLOBAL_HEALTHY_GAMING_CHINA_THREE_TO_FIVE_HOURS";
    if ((bool) UniversalInputManager.UsePhoneUI && GameStrings.HasKey(key + "_PHONE"))
      key += "_PHONE";
    string textArg = GameStrings.Get(key);
    SocialToastMgr.Get().AddToast(UserAttentionBlocker.ALL_EXCEPT_FATAL_ERROR_SCENE, textArg, SocialToastMgr.TOAST_TYPE.DEFAULT, 60f);
    if (this.m_DebugMode)
    {
      Log.HealthyGaming.Print("GLOBAL_HEALTHY_GAMING_CHINA_THREE_TO_FIVE_HOURS: " + minutesPlayed.ToString());
      Log.HealthyGaming.Print(GameStrings.Get("GLOBAL_HEALTHY_GAMING_CHINA_THREE_TO_FIVE_HOURS"));
      Log.HealthyGaming.Print("NextMessageDisplayTime: " + this.m_NextMessageDisplayTime.ToString());
    }
    else
      Log.HealthyGaming.Print(string.Format("Time: {0},  Played: {1},  {2}", (object) Time.realtimeSinceStartup, (object) minutesPlayed, (object) textArg));
  }

  private void ChinaRestrictions_MoreThan5Hours(int minutesPlayed)
  {
    this.m_NextMessageDisplayTime = (double) this.m_NextMessageDisplayTime >= 0.0 ? (float) (this.m_TimePlayed + 15) : (float) (this.m_TimePlayed + (15 - minutesPlayed % 15));
    string key = "GLOBAL_HEALTHY_GAMING_CHINA_MORE_THAN_FIVE_HOURS";
    if ((bool) UniversalInputManager.UsePhoneUI && GameStrings.HasKey(key + "_PHONE"))
      key += "_PHONE";
    string textArg = GameStrings.Get(key);
    SocialToastMgr.Get().AddToast(UserAttentionBlocker.ALL_EXCEPT_FATAL_ERROR_SCENE, textArg, SocialToastMgr.TOAST_TYPE.DEFAULT, 60f);
    if (this.m_DebugMode)
    {
      Log.HealthyGaming.Print("GLOBAL_HEALTHY_GAMING_CHINA_MORE_THAN_FIVE_HOURS: " + minutesPlayed.ToString());
      Log.HealthyGaming.Print(GameStrings.Get("GLOBAL_HEALTHY_GAMING_CHINA_MORE_THAN_FIVE_HOURS"));
      Log.HealthyGaming.Print("NextMessageDisplayTime: " + this.m_NextMessageDisplayTime.ToString());
    }
    else
      Log.HealthyGaming.Print(string.Format("Time: {0},  Played: {1},  {2}", (object) Time.realtimeSinceStartup, (object) minutesPlayed, (object) textArg));
  }

  private void ChinaRestrictions_LockoutFeatures(int minutesPlayed)
  {
    this.m_HealthyGamingArenaEnabled = false;
    Box box = Box.Get();
    if (!((UnityEngine.Object) box != (UnityEngine.Object) null))
      return;
    box.UpdateUI(false);
  }
}
