﻿// Decompiled with JetBrains decompiler
// Type: UserAttentionBlocker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Flags]
public enum UserAttentionBlocker
{
  NONE = 0,
  FATAL_ERROR_SCENE = 1,
  SET_ROTATION_INTRO = 2,
  SET_ROTATION_CM_TUTORIALS = 4,
  ALL = -1,
  ALL_EXCEPT_FATAL_ERROR_SCENE = -2,
}
