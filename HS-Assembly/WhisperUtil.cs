﻿// Decompiled with JetBrains decompiler
// Type: WhisperUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;

public static class WhisperUtil
{
  public static BnetPlayer GetSpeaker(BnetWhisper whisper)
  {
    return BnetUtils.GetPlayer(whisper.GetSpeakerId());
  }

  public static BnetPlayer GetReceiver(BnetWhisper whisper)
  {
    return BnetUtils.GetPlayer(whisper.GetReceiverId());
  }

  public static BnetPlayer GetTheirPlayer(BnetWhisper whisper)
  {
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    if (myPlayer == null)
      return (BnetPlayer) null;
    BnetPlayer speaker = WhisperUtil.GetSpeaker(whisper);
    BnetPlayer receiver = WhisperUtil.GetReceiver(whisper);
    if (myPlayer == speaker)
      return receiver;
    if (myPlayer == receiver)
      return speaker;
    return (BnetPlayer) null;
  }

  public static BnetGameAccountId GetTheirGameAccountId(BnetWhisper whisper)
  {
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    if (myPlayer == null)
      return (BnetGameAccountId) null;
    if (myPlayer.HasGameAccount(whisper.GetSpeakerId()))
      return whisper.GetReceiverId();
    if (myPlayer.HasGameAccount(whisper.GetReceiverId()))
      return whisper.GetSpeakerId();
    return (BnetGameAccountId) null;
  }

  public static bool IsDisplayable(BnetWhisper whisper)
  {
    BnetPlayer speaker = WhisperUtil.GetSpeaker(whisper);
    BnetPlayer receiver = WhisperUtil.GetReceiver(whisper);
    return speaker != null && speaker.IsDisplayable() && (receiver != null && receiver.IsDisplayable());
  }

  public static bool IsSpeaker(BnetPlayer player, BnetWhisper whisper)
  {
    if (player == null)
      return false;
    return player.HasGameAccount(whisper.GetSpeakerId());
  }

  public static bool IsReceiver(BnetPlayer player, BnetWhisper whisper)
  {
    if (player == null)
      return false;
    return player.HasGameAccount(whisper.GetReceiverId());
  }

  public static bool IsSpeakerOrReceiver(BnetPlayer player, BnetWhisper whisper)
  {
    if (player == null)
      return false;
    if (!player.HasGameAccount(whisper.GetSpeakerId()))
      return player.HasGameAccount(whisper.GetReceiverId());
    return true;
  }
}
