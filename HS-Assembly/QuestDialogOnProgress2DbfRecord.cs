﻿// Decompiled with JetBrains decompiler
// Type: QuestDialogOnProgress2DbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QuestDialogOnProgress2DbfRecord : DbfRecord
{
  [SerializeField]
  private int m_QuestDialogId;
  [SerializeField]
  private int m_PlayOrder;
  [SerializeField]
  private string m_PrefabName;
  [SerializeField]
  private string m_AudioName;
  [SerializeField]
  private bool m_AltBubblePosition;
  [SerializeField]
  private float m_WaitBefore;
  [SerializeField]
  private float m_WaitAfter;
  [SerializeField]
  private bool m_PersistPrefab;

  [DbfField("QUEST_DIALOG_ID", "the ASSET.QUEST_DIALOG.ID")]
  public int QuestDialogId
  {
    get
    {
      return this.m_QuestDialogId;
    }
  }

  [DbfField("PLAY_ORDER", "sort ordering of when each line should play (ascending, as in 0 is first, then 1, etc)")]
  public int PlayOrder
  {
    get
    {
      return this.m_PlayOrder;
    }
  }

  [DbfField("PREFAB_NAME", "name for the associated prefab")]
  public string PrefabName
  {
    get
    {
      return this.m_PrefabName;
    }
  }

  [DbfField("AUDIO_NAME", "name for the associated audio prefab")]
  public string AudioName
  {
    get
    {
      return this.m_AudioName;
    }
  }

  [DbfField("ALT_BUBBLE_POSITION", "if true, will attempt to use the alternative speech bubble position (if one exists)")]
  public bool AltBubblePosition
  {
    get
    {
      return this.m_AltBubblePosition;
    }
  }

  [DbfField("WAIT_BEFORE", "the amount of time (in seconds) to wait BEFORE the voice line is executed.")]
  public float WaitBefore
  {
    get
    {
      return this.m_WaitBefore;
    }
  }

  [DbfField("WAIT_AFTER", "the amount of time (in seconds) to wait AFTER the voice line is executed.")]
  public float WaitAfter
  {
    get
    {
      return this.m_WaitAfter;
    }
  }

  [DbfField("PERSIST_PREFAB", "if true, the prefab will persist after the VO line plays. In this case, any subsiquent VO lines will appear on the existing prefab as new bubbles, and not as a seperate character entrance.")]
  public bool PersistPrefab
  {
    get
    {
      return this.m_PersistPrefab;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    QuestDialogOnProgress2DbfAsset progress2DbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (QuestDialogOnProgress2DbfAsset)) as QuestDialogOnProgress2DbfAsset;
    if ((UnityEngine.Object) progress2DbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("QuestDialogOnProgress2DbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < progress2DbfAsset.Records.Count; ++index)
      progress2DbfAsset.Records[index].StripUnusedLocales();
    records = (object) progress2DbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetQuestDialogId(int v)
  {
    this.m_QuestDialogId = v;
  }

  public void SetPlayOrder(int v)
  {
    this.m_PlayOrder = v;
  }

  public void SetPrefabName(string v)
  {
    this.m_PrefabName = v;
  }

  public void SetAudioName(string v)
  {
    this.m_AudioName = v;
  }

  public void SetAltBubblePosition(bool v)
  {
    this.m_AltBubblePosition = v;
  }

  public void SetWaitBefore(float v)
  {
    this.m_WaitBefore = v;
  }

  public void SetWaitAfter(float v)
  {
    this.m_WaitAfter = v;
  }

  public void SetPersistPrefab(bool v)
  {
    this.m_PersistPrefab = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (QuestDialogOnProgress2DbfRecord.\u003C\u003Ef__switch\u0024map56 == null)
      {
        // ISSUE: reference to a compiler-generated field
        QuestDialogOnProgress2DbfRecord.\u003C\u003Ef__switch\u0024map56 = new Dictionary<string, int>(9)
        {
          {
            "ID",
            0
          },
          {
            "QUEST_DIALOG_ID",
            1
          },
          {
            "PLAY_ORDER",
            2
          },
          {
            "PREFAB_NAME",
            3
          },
          {
            "AUDIO_NAME",
            4
          },
          {
            "ALT_BUBBLE_POSITION",
            5
          },
          {
            "WAIT_BEFORE",
            6
          },
          {
            "WAIT_AFTER",
            7
          },
          {
            "PERSIST_PREFAB",
            8
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (QuestDialogOnProgress2DbfRecord.\u003C\u003Ef__switch\u0024map56.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.QuestDialogId;
          case 2:
            return (object) this.PlayOrder;
          case 3:
            return (object) this.PrefabName;
          case 4:
            return (object) this.AudioName;
          case 5:
            return (object) this.AltBubblePosition;
          case 6:
            return (object) this.WaitBefore;
          case 7:
            return (object) this.WaitAfter;
          case 8:
            return (object) this.PersistPrefab;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (QuestDialogOnProgress2DbfRecord.\u003C\u003Ef__switch\u0024map57 == null)
    {
      // ISSUE: reference to a compiler-generated field
      QuestDialogOnProgress2DbfRecord.\u003C\u003Ef__switch\u0024map57 = new Dictionary<string, int>(9)
      {
        {
          "ID",
          0
        },
        {
          "QUEST_DIALOG_ID",
          1
        },
        {
          "PLAY_ORDER",
          2
        },
        {
          "PREFAB_NAME",
          3
        },
        {
          "AUDIO_NAME",
          4
        },
        {
          "ALT_BUBBLE_POSITION",
          5
        },
        {
          "WAIT_BEFORE",
          6
        },
        {
          "WAIT_AFTER",
          7
        },
        {
          "PERSIST_PREFAB",
          8
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!QuestDialogOnProgress2DbfRecord.\u003C\u003Ef__switch\u0024map57.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetQuestDialogId((int) val);
        break;
      case 2:
        this.SetPlayOrder((int) val);
        break;
      case 3:
        this.SetPrefabName((string) val);
        break;
      case 4:
        this.SetAudioName((string) val);
        break;
      case 5:
        this.SetAltBubblePosition((bool) val);
        break;
      case 6:
        this.SetWaitBefore((float) val);
        break;
      case 7:
        this.SetWaitAfter((float) val);
        break;
      case 8:
        this.SetPersistPrefab((bool) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (QuestDialogOnProgress2DbfRecord.\u003C\u003Ef__switch\u0024map58 == null)
      {
        // ISSUE: reference to a compiler-generated field
        QuestDialogOnProgress2DbfRecord.\u003C\u003Ef__switch\u0024map58 = new Dictionary<string, int>(9)
        {
          {
            "ID",
            0
          },
          {
            "QUEST_DIALOG_ID",
            1
          },
          {
            "PLAY_ORDER",
            2
          },
          {
            "PREFAB_NAME",
            3
          },
          {
            "AUDIO_NAME",
            4
          },
          {
            "ALT_BUBBLE_POSITION",
            5
          },
          {
            "WAIT_BEFORE",
            6
          },
          {
            "WAIT_AFTER",
            7
          },
          {
            "PERSIST_PREFAB",
            8
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (QuestDialogOnProgress2DbfRecord.\u003C\u003Ef__switch\u0024map58.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (int);
          case 3:
            return typeof (string);
          case 4:
            return typeof (string);
          case 5:
            return typeof (bool);
          case 6:
            return typeof (float);
          case 7:
            return typeof (float);
          case 8:
            return typeof (bool);
        }
      }
    }
    return (System.Type) null;
  }
}
