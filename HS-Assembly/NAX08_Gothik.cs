﻿// Decompiled with JetBrains decompiler
// Type: NAX08_Gothik
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class NAX08_Gothik : NAX_MissionEntity
{
  private bool m_cardLinePlayed;
  private bool m_unrelentingMinionLinePlayed;
  private bool m_deadReturnLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_NAX8_01_CARD_02");
    this.PreloadSound("VO_NAX8_01_CUSTOM_03");
    this.PreloadSound("VO_NAX8_01_EMOTE1_06");
    this.PreloadSound("VO_NAX8_01_EMOTE2_07");
    this.PreloadSound("VO_NAX8_01_EMOTE3_08");
    this.PreloadSound("VO_NAX8_01_EMOTE4_09");
    this.PreloadSound("VO_NAX8_01_EMOTE5_10");
    this.PreloadSound("VO_NAX8_01_CUSTOM2_04");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX8_01_EMOTE1_06",
            m_stringTag = "VO_NAX8_01_EMOTE1_06"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX8_01_EMOTE2_07",
            m_stringTag = "VO_NAX8_01_EMOTE2_07"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX8_01_EMOTE3_08",
            m_stringTag = "VO_NAX8_01_EMOTE3_08"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX8_01_EMOTE4_09",
            m_stringTag = "VO_NAX8_01_EMOTE4_09"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX8_01_EMOTE5_10",
            m_stringTag = "VO_NAX8_01_EMOTE5_10"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX08_Gothik.\u003CHandleGameOverWithTiming\u003Ec__Iterator1D5() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX08_Gothik.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1D6() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX08_Gothik.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1D7() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
