﻿// Decompiled with JetBrains decompiler
// Type: CardBackData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.ComponentModel;

public class CardBackData
{
  public int ID { get; private set; }

  public CardBackData.CardBackSource Source { get; private set; }

  public long SourceData { get; private set; }

  public string Name { get; private set; }

  public bool Enabled { get; private set; }

  public string PrefabName { get; private set; }

  public CardBackData(int id, CardBackData.CardBackSource source, long sourceData, string name, bool enabled, string prefabName)
  {
    this.ID = id;
    this.Source = source;
    this.SourceData = sourceData;
    this.Name = name;
    this.Enabled = enabled;
    this.PrefabName = prefabName;
  }

  public override string ToString()
  {
    return string.Format("[CardBackData: ID={0}, Source={1}, SourceData={2}, Name={3}, Enabled={4}, PrefabName={5}]", (object) this.ID, (object) this.Name, (object) this.Source, (object) this.SourceData, (object) this.Enabled, (object) this.PrefabName);
  }

  public enum CardBackSource
  {
    [Description("startup")] STARTUP = 0,
    [Description("season")] SEASON = 1,
    [Description("achieve")] ACHIEVE = 2,
    [Description("fixed_reward")] FIXED_REWARD = 3,
    [Description("tavern_brawl")] TAVERN_BRAWL = 5,
  }
}
