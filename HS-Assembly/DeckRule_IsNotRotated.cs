﻿// Decompiled with JetBrains decompiler
// Type: DeckRule_IsNotRotated
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Linq;

public class DeckRule_IsNotRotated : DeckRule
{
  public DeckRule_IsNotRotated()
  {
    this.m_ruleType = DeckRule.RuleType.IS_NOT_ROTATED;
  }

  public DeckRule_IsNotRotated(DeckRulesetRuleDbfRecord record)
    : base(DeckRule.RuleType.IS_NOT_ROTATED, record)
  {
  }

  public override bool Filter(EntityDef def)
  {
    return !GameUtils.IsCardRotated(def);
  }

  public override bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason)
  {
    reason = (RuleInvalidReason) null;
    int countParam = deck.GetSlots().Sum<CollectionDeckSlot>((Func<CollectionDeckSlot, int>) (s =>
    {
      if (this.AppliesTo(s.CardID) && GameUtils.IsCardRotated(s.CardID))
        return s.Count;
      return 0;
    }));
    bool result = this.GetResult(countParam <= 0);
    if (!result)
      reason = new RuleInvalidReason(GameStrings.Format("GLUE_COLLECTION_DECK_RULE_INVALID_CARDS", (object) countParam), countParam, false);
    return result;
  }
}
