﻿// Decompiled with JetBrains decompiler
// Type: JadeGolemCardTextBuilder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class JadeGolemCardTextBuilder : CardTextBuilder
{
  protected bool m_showJadeGolemStatsInPlay;

  public JadeGolemCardTextBuilder()
  {
    this.m_useEntityForTextInPlay = true;
    this.m_showJadeGolemStatsInPlay = false;
  }

  public override string BuildCardTextInHand(Entity entity)
  {
    string builtText = base.BuildCardTextInHand(entity);
    int length = builtText.IndexOf('@');
    if (length >= 0)
      builtText = entity.GetZone() != TAG_ZONE.PLAY || this.m_showJadeGolemStatsInPlay ? builtText.Substring(0, length) : builtText.Substring(length + 1);
    return this.FormatJadeGolemText(builtText, entity.GetJadeGolem());
  }

  private string FormatJadeGolemText(string builtText, int jadeGolemValue)
  {
    string str = string.Empty;
    if (jadeGolemValue == 8 || jadeGolemValue == 11 || jadeGolemValue == 18)
      str = "n";
    return string.Format(builtText, (object) (jadeGolemValue.ToString() + "/" + (object) jadeGolemValue), (object) str);
  }

  public override string BuildCardTextInHand(EntityDef entityDef)
  {
    string str = base.BuildCardTextInHand(entityDef);
    int num = str.IndexOf('@');
    if (num >= 0)
      str = str.Substring(num + 1);
    return str;
  }

  public override string BuildCardTextInHistory(Entity entity)
  {
    JadeGolemCardTextHistoryData cardTextHistoryData = entity.GetCardTextHistoryData() as JadeGolemCardTextHistoryData;
    if (cardTextHistoryData == null)
    {
      Log.All.Print("JadeGolemCardTextBuilder.BuildCardTextInHistory: entity {0} does not have a JadeGolemCardTextHistoryData object.", (object) entity.GetEntityId());
      return string.Empty;
    }
    string builtText = base.BuildCardTextInHistory(entity);
    int length = builtText.IndexOf('@');
    if (length >= 0)
      builtText = !cardTextHistoryData.m_wasInPlay || this.m_showJadeGolemStatsInPlay ? builtText.Substring(0, length) : builtText.Substring(length + 1);
    return this.FormatJadeGolemText(builtText, entity.GetJadeGolem());
  }

  public override CardTextHistoryData CreateCardTextHistoryData()
  {
    return (CardTextHistoryData) new JadeGolemCardTextHistoryData();
  }
}
