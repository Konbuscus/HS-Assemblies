﻿// Decompiled with JetBrains decompiler
// Type: Medal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Medal
{
  private static readonly Medal.Type GRAND_MASTER_MEDAL_TYPE = Medal.Type.MEDAL_MASTER_C;
  private static readonly Map<Medal.Type, string> s_medalNameKey = new Map<Medal.Type, string>() { { Medal.Type.MEDAL_NOVICE, "GLOBAL_MEDAL_NOVICE" }, { Medal.Type.MEDAL_APPRENTICE, "GLOBAL_MEDAL_APPRENTICE" }, { Medal.Type.MEDAL_JOURNEYMAN, "GLOBAL_MEDAL_JOURNEYMAN" }, { Medal.Type.MEDAL_COPPER_A, "GLOBAL_MEDAL_COPPER" }, { Medal.Type.MEDAL_COPPER_B, "GLOBAL_MEDAL_COPPER" }, { Medal.Type.MEDAL_COPPER_C, "GLOBAL_MEDAL_COPPER" }, { Medal.Type.MEDAL_SILVER_A, "GLOBAL_MEDAL_SILVER" }, { Medal.Type.MEDAL_SILVER_B, "GLOBAL_MEDAL_SILVER" }, { Medal.Type.MEDAL_SILVER_C, "GLOBAL_MEDAL_SILVER" }, { Medal.Type.MEDAL_GOLD_A, "GLOBAL_MEDAL_GOLD" }, { Medal.Type.MEDAL_GOLD_B, "GLOBAL_MEDAL_GOLD" }, { Medal.Type.MEDAL_GOLD_C, "GLOBAL_MEDAL_GOLD" }, { Medal.Type.MEDAL_PLATINUM_A, "GLOBAL_MEDAL_PLATINUM" }, { Medal.Type.MEDAL_PLATINUM_B, "GLOBAL_MEDAL_PLATINUM" }, { Medal.Type.MEDAL_PLATINUM_C, "GLOBAL_MEDAL_PLATINUM" }, { Medal.Type.MEDAL_DIAMOND_A, "GLOBAL_MEDAL_DIAMOND" }, { Medal.Type.MEDAL_DIAMOND_B, "GLOBAL_MEDAL_DIAMOND" }, { Medal.Type.MEDAL_DIAMOND_C, "GLOBAL_MEDAL_DIAMOND" }, { Medal.Type.MEDAL_MASTER_A, "GLOBAL_MEDAL_MASTER" }, { Medal.Type.MEDAL_MASTER_B, "GLOBAL_MEDAL_MASTER" }, { Medal.Type.MEDAL_MASTER_C, "GLOBAL_MEDAL_MASTER" } };
  private Medal.Type m_medalType;
  private int m_grandMasterRank;

  public Medal.Type MedalType
  {
    get
    {
      return this.m_medalType;
    }
  }

  public int GrandMasterRank
  {
    get
    {
      return this.m_grandMasterRank;
    }
  }

  public Medal(Medal.Type medalType)
    : this(medalType, 0)
  {
  }

  public Medal(int grandMasterRank)
    : this(Medal.GRAND_MASTER_MEDAL_TYPE, grandMasterRank)
  {
  }

  private Medal(Medal.Type medalType, int grandMasterRank)
  {
    this.m_medalType = medalType;
    this.m_grandMasterRank = grandMasterRank;
  }

  public bool IsGrandMaster()
  {
    if (Medal.GRAND_MASTER_MEDAL_TYPE == this.MedalType)
      return this.GrandMasterRank > 0;
    return false;
  }

  public int GetNumStars()
  {
    if (this.IsGrandMaster())
      return 0;
    switch (this.MedalType)
    {
      case Medal.Type.MEDAL_NOVICE:
      case Medal.Type.MEDAL_JOURNEYMAN:
      case Medal.Type.MEDAL_APPRENTICE:
        return 0;
      case Medal.Type.MEDAL_COPPER_A:
      case Medal.Type.MEDAL_SILVER_A:
      case Medal.Type.MEDAL_GOLD_A:
      case Medal.Type.MEDAL_PLATINUM_A:
      case Medal.Type.MEDAL_DIAMOND_A:
      case Medal.Type.MEDAL_MASTER_A:
        return 1;
      case Medal.Type.MEDAL_COPPER_B:
      case Medal.Type.MEDAL_SILVER_B:
      case Medal.Type.MEDAL_GOLD_B:
      case Medal.Type.MEDAL_PLATINUM_B:
      case Medal.Type.MEDAL_DIAMOND_B:
      case Medal.Type.MEDAL_MASTER_B:
        return 2;
      case Medal.Type.MEDAL_COPPER_C:
      case Medal.Type.MEDAL_SILVER_C:
      case Medal.Type.MEDAL_GOLD_C:
      case Medal.Type.MEDAL_PLATINUM_C:
      case Medal.Type.MEDAL_DIAMOND_C:
      case Medal.Type.MEDAL_MASTER_C:
        return 3;
      default:
        Debug.LogWarning((object) string.Format("Medal.GetNumStars(): Don't know how many stars are in medal {0}", (object) this));
        return 0;
    }
  }

  public string GetMedalName()
  {
    if (this.IsGrandMaster())
      return GameStrings.Get("GLOBAL_MEDAL_GRANDMASTER");
    string baseMedalName = this.GetBaseMedalName();
    string starString = this.GetStarString();
    if (string.IsNullOrEmpty(starString))
      return baseMedalName;
    return GameStrings.Format("GLOBAL_MEDAL_STARRED_FORMAT", (object) starString, (object) baseMedalName);
  }

  public string GetBaseMedalName()
  {
    if (this.IsGrandMaster())
      return GameStrings.Get("GLOBAL_MEDAL_GRANDMASTER");
    string empty = string.Empty;
    string str;
    if (Medal.s_medalNameKey.ContainsKey(this.MedalType))
    {
      str = GameStrings.Get(Medal.s_medalNameKey[this.MedalType]);
    }
    else
    {
      Debug.LogWarning((object) string.Format("Medal.GetMedalName(): don't have a medal name key for {0}", (object) this));
      str = GameStrings.Get("GLOBAL_MEDAL_NO_NAME");
    }
    return str;
  }

  public string GetNextMedalName()
  {
    Medal nextMedal = this.GetNextMedal();
    if (nextMedal == null)
      return string.Empty;
    return nextMedal.GetMedalName();
  }

  public override string ToString()
  {
    return string.Format("[Medal: MedalType={0}, GrandMasterRank={1}]", (object) this.MedalType, (object) this.GrandMasterRank);
  }

  private Medal GetNextMedal()
  {
    if (this.IsGrandMaster())
      return (Medal) null;
    if (this.MedalType == Medal.GRAND_MASTER_MEDAL_TYPE)
      return new Medal(1);
    return new Medal(this.MedalType + 1);
  }

  private string GetStarString()
  {
    int numStars = this.GetNumStars();
    switch (numStars)
    {
      case 0:
        return string.Empty;
      case 1:
        return GameStrings.Get("GLOBAL_MEDAL_ONE_STAR");
      case 2:
        return GameStrings.Get("GLOBAL_MEDAL_TWO_STAR");
      case 3:
        return GameStrings.Get("GLOBAL_MEDAL_THREE_STAR");
      default:
        Debug.LogWarning((object) string.Format("Medal.GetStarString(): don't have a star string for {0} stars", (object) numStars));
        return string.Empty;
    }
  }

  public enum Type
  {
    MEDAL_NOVICE,
    MEDAL_JOURNEYMAN,
    MEDAL_COPPER_A,
    MEDAL_COPPER_B,
    MEDAL_COPPER_C,
    MEDAL_SILVER_A,
    MEDAL_SILVER_B,
    MEDAL_SILVER_C,
    MEDAL_GOLD_A,
    MEDAL_GOLD_B,
    MEDAL_GOLD_C,
    MEDAL_PLATINUM_A,
    MEDAL_PLATINUM_B,
    MEDAL_PLATINUM_C,
    MEDAL_DIAMOND_A,
    MEDAL_DIAMOND_B,
    MEDAL_DIAMOND_C,
    MEDAL_MASTER_A,
    MEDAL_MASTER_B,
    MEDAL_MASTER_C,
    MEDAL_APPRENTICE,
  }
}
