﻿// Decompiled with JetBrains decompiler
// Type: BRM12_Chromaggus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class BRM12_Chromaggus : BRM_MissionEntity
{
  private bool m_heroPowerLinePlayed;
  private bool m_cardLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("ChromaggusBoss_EmoteResponse_1");
    this.PreloadSound("VO_NEFARIAN_CHROMAGGUS_DEAD_63");
    this.PreloadSound("VO_NEFARIAN_CHROMAGGUS1_59");
    this.PreloadSound("VO_NEFARIAN_CHROMAGGUS2_60");
    this.PreloadSound("VO_NEFARIAN_CHROMAGGUS3_61");
    this.PreloadSound("VO_NEFARIAN_CHROMAGGUS4_62");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "ChromaggusBoss_EmoteResponse_1",
            m_stringTag = "ChromaggusBoss_EmoteResponse_1"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM12_Chromaggus.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator149() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM12_Chromaggus.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator14A() { turn = turn, \u003C\u0024\u003Eturn = turn };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM12_Chromaggus.\u003CHandleGameOverWithTiming\u003Ec__Iterator14B() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }
}
