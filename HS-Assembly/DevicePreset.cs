﻿// Decompiled with JetBrains decompiler
// Type: DevicePreset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class DevicePreset : ICloneable
{
  public string name = "No Emulation";
  public OSCategory os = OSCategory.PC;
  public ScreenCategory screen = ScreenCategory.PC;
  public ScreenDensityCategory screenDensity = ScreenDensityCategory.High;
  public static readonly DevicePresetList s_devicePresets;
  public InputCategory input;

  static DevicePreset()
  {
    DevicePresetList devicePresetList = new DevicePresetList();
    devicePresetList.Add(new DevicePreset()
    {
      name = "No Emulation"
    });
    devicePresetList.Add(new DevicePreset()
    {
      name = "PC",
      os = OSCategory.PC,
      screen = ScreenCategory.PC,
      input = InputCategory.Mouse
    });
    devicePresetList.Add(new DevicePreset()
    {
      name = "iPhone",
      os = OSCategory.iOS,
      screen = ScreenCategory.Phone,
      input = InputCategory.Touch
    });
    devicePresetList.Add(new DevicePreset()
    {
      name = "iPad",
      os = OSCategory.iOS,
      screen = ScreenCategory.Tablet,
      input = InputCategory.Touch
    });
    devicePresetList.Add(new DevicePreset()
    {
      name = "Android Phone",
      os = OSCategory.Android,
      screen = ScreenCategory.Phone,
      input = InputCategory.Touch
    });
    devicePresetList.Add(new DevicePreset()
    {
      name = "Android Tablet",
      os = OSCategory.Android,
      screen = ScreenCategory.Tablet,
      input = InputCategory.Touch,
      screenDensity = ScreenDensityCategory.Normal
    });
    DevicePreset.s_devicePresets = devicePresetList;
  }

  public object Clone()
  {
    return this.MemberwiseClone();
  }

  public void ReadFromConfig(ConfigFile config)
  {
    this.name = config.Get("Emulation.DeviceName", this.name.ToString());
    DevicePreset devicePreset = DevicePreset.s_devicePresets.Find((Predicate<DevicePreset>) (x => x.name.Equals(this.name)));
    this.os = devicePreset.os;
    this.input = devicePreset.input;
    this.screen = devicePreset.screen;
    this.screenDensity = devicePreset.screenDensity;
  }

  public void WriteToConfig(ConfigFile config)
  {
    Log.ConfigFile.Print("Writing Emulated Device: " + this.name + " to " + config.GetPath());
    config.Set("Emulation.DeviceName", this.name.ToString());
    config.Delete("Emulation.OSCategory", true);
    config.Delete("Emulation.InputCategory", true);
    config.Delete("Emulation.ScreenCategory", true);
    config.Delete("Emulation.ScreenDensityCategory", true);
    config.Save((string) null);
  }
}
