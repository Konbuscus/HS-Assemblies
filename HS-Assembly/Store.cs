﻿// Decompiled with JetBrains decompiler
// Type: Store
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class Store : UIBPopup
{
  [CustomEditField(Sections = "Store/Materials")]
  public List<MeshRenderer> m_goldButtonMeshes = new List<MeshRenderer>();
  [CustomEditField(Sections = "Store/Materials")]
  public List<MeshRenderer> m_moneyButtonMeshes = new List<MeshRenderer>();
  protected bool m_buyButtonsEnabled = true;
  private List<Store.ExitListener> m_exitListeners = new List<Store.ExitListener>();
  private List<Store.BuyWithMoneyListener> m_buyWithMoneyListeners = new List<Store.BuyWithMoneyListener>();
  private List<Store.BuyWithGoldGTAPPListener> m_buyWithGoldGTAPPListeners = new List<Store.BuyWithGoldGTAPPListener>();
  private List<Store.BuyWithGoldNoGTAPPListener> m_buyWithGoldNoGTAPPListeners = new List<Store.BuyWithGoldNoGTAPPListener>();
  private List<Store.ReadyListener> m_readyListeners = new List<Store.ReadyListener>();
  private List<Store.InfoListener> m_infoListeners = new List<Store.InfoListener>();
  [CustomEditField(Sections = "Store/UI")]
  public GameObject m_root;
  [CustomEditField(Sections = "Store/UI")]
  public GameObject m_cover;
  [CustomEditField(Sections = "Store/UI")]
  public UIBButton m_buyWithMoneyButton;
  [CustomEditField(Sections = "Store/UI")]
  public TooltipZone m_buyWithMoneyTooltip;
  [CustomEditField(Sections = "Store/UI")]
  public PegUIElement m_buyWithMoneyTooltipTrigger;
  [CustomEditField(Sections = "Store/UI")]
  public UIBButton m_buyWithGoldButton;
  [CustomEditField(Sections = "Store/UI")]
  public TooltipZone m_buyWithGoldTooltip;
  [CustomEditField(Sections = "Store/UI")]
  public PegUIElement m_buyWithGoldTooltipTrigger;
  [CustomEditField(Sections = "Store/UI")]
  public UIBButton m_infoButton;
  [CustomEditField(Sections = "Store/Materials")]
  public Material m_enabledGoldButtonMaterial;
  [CustomEditField(Sections = "Store/Materials")]
  public Material m_disabledGoldButtonMaterial;
  [CustomEditField(Sections = "Store/Materials")]
  public Material m_enabledMoneyButtonMaterial;
  [CustomEditField(Sections = "Store/Materials")]
  public Material m_disabledMoneyButtonMaterial;
  [CustomEditField(Sections = "Store/UI")]
  public PegUIElement m_offClicker;
  protected StoreType m_storeType;
  private Store.BuyButtonState m_moneyButtonState;
  private Store.BuyButtonState m_goldButtonState;

  protected virtual void Awake()
  {
    this.m_infoButton.SetText(GameStrings.Get("GLUE_STORE_INFO_BUTTON_TEXT"));
  }

  protected virtual void Start()
  {
    this.m_buyWithGoldButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBuyWithGoldPressed));
    this.m_buyWithMoneyButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBuyWithMoneyPressed));
    this.m_buyWithGoldTooltipTrigger.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnShowBuyWithGoldTooltip));
    this.m_buyWithMoneyTooltipTrigger.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnShowBuyWithMoneyTooltip));
    this.m_buyWithGoldTooltipTrigger.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnHideBuyWithGoldTooltip));
    this.m_buyWithMoneyTooltipTrigger.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnHideBuyWithMoneyTooltip));
    this.m_infoButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnInfoPressed));
    this.StartCoroutine(this.NotifyListenersWhenReady());
  }

  protected virtual void OnDestroy()
  {
    this.m_enabledGoldButtonMaterial = (Material) null;
    this.m_disabledGoldButtonMaterial = (Material) null;
    this.m_enabledMoneyButtonMaterial = (Material) null;
    this.m_disabledMoneyButtonMaterial = (Material) null;
    if (!(bool) ((Object) FullScreenFXMgr.Get()))
      return;
    this.EnableFullScreenEffects(false);
  }

  public virtual void OnGoldBalanceChanged(NetCache.NetCacheGoldBalance balance)
  {
  }

  public virtual void OnGoldSpent()
  {
  }

  public virtual void OnMoneySpent()
  {
  }

  public void Show(Store.DelOnStoreShown onStoreShownCB, bool isTotallyFake)
  {
    this.ShowImpl(onStoreShownCB, isTotallyFake);
  }

  public virtual void Close()
  {
  }

  public void SetStoreType(StoreType storeType)
  {
    this.m_storeType = storeType;
  }

  public StoreType GetStoreType()
  {
    return this.m_storeType;
  }

  public virtual bool IsReady()
  {
    return true;
  }

  public bool IsCovered()
  {
    return this.m_cover.activeSelf;
  }

  public void ActivateCover(bool coverActive)
  {
    this.m_cover.SetActive(coverActive);
    this.EnableBuyButtons(!coverActive);
  }

  public void EnableClickCatcher(bool enabled)
  {
    this.m_offClicker.gameObject.SetActive(enabled);
  }

  public bool RegisterBuyWithMoneyListener(Store.BuyWithMoneyCallback callback)
  {
    return this.RegisterBuyWithMoneyListener(callback, (object) null);
  }

  public bool RegisterBuyWithMoneyListener(Store.BuyWithMoneyCallback callback, object userData)
  {
    Store.BuyWithMoneyListener withMoneyListener = new Store.BuyWithMoneyListener();
    withMoneyListener.SetCallback(callback);
    withMoneyListener.SetUserData(userData);
    if (this.m_buyWithMoneyListeners.Contains(withMoneyListener))
      return false;
    this.m_buyWithMoneyListeners.Add(withMoneyListener);
    return true;
  }

  public bool RemoveBuyWithMoneyListener(Store.BuyWithMoneyCallback callback)
  {
    return this.RemoveBuyWithMoneyListener(callback, (object) null);
  }

  public bool RemoveBuyWithMoneyListener(Store.BuyWithMoneyCallback callback, object userData)
  {
    Store.BuyWithMoneyListener withMoneyListener = new Store.BuyWithMoneyListener();
    withMoneyListener.SetCallback(callback);
    withMoneyListener.SetUserData(userData);
    return this.m_buyWithMoneyListeners.Remove(withMoneyListener);
  }

  public bool RegisterBuyWithGoldGTAPPListener(Store.BuyWithGoldGTAPPCallback callback)
  {
    return this.RegisterBuyWithGoldGTAPPListener(callback, (object) null);
  }

  public bool RegisterBuyWithGoldGTAPPListener(Store.BuyWithGoldGTAPPCallback callback, object userData)
  {
    Store.BuyWithGoldGTAPPListener goldGtappListener = new Store.BuyWithGoldGTAPPListener();
    goldGtappListener.SetCallback(callback);
    goldGtappListener.SetUserData(userData);
    if (this.m_buyWithGoldGTAPPListeners.Contains(goldGtappListener))
      return false;
    this.m_buyWithGoldGTAPPListeners.Add(goldGtappListener);
    return true;
  }

  public bool RemoveBuyWithGoldGTAPPListener(Store.BuyWithGoldGTAPPCallback callback)
  {
    return this.RemoveBuyWithGoldGTAPPListener(callback, (object) null);
  }

  public bool RemoveBuyWithGoldGTAPPListener(Store.BuyWithGoldGTAPPCallback callback, object userData)
  {
    Store.BuyWithGoldGTAPPListener goldGtappListener = new Store.BuyWithGoldGTAPPListener();
    goldGtappListener.SetCallback(callback);
    goldGtappListener.SetUserData(userData);
    return this.m_buyWithGoldGTAPPListeners.Remove(goldGtappListener);
  }

  public bool RegisterBuyWithGoldNoGTAPPListener(Store.BuyWithGoldNoGTAPPCallback callback)
  {
    return this.RegisterBuyWithGoldNoGTAPPListener(callback, (object) null);
  }

  public bool RegisterBuyWithGoldNoGTAPPListener(Store.BuyWithGoldNoGTAPPCallback callback, object userData)
  {
    Store.BuyWithGoldNoGTAPPListener goldNoGtappListener = new Store.BuyWithGoldNoGTAPPListener();
    goldNoGtappListener.SetCallback(callback);
    goldNoGtappListener.SetUserData(userData);
    if (this.m_buyWithGoldNoGTAPPListeners.Contains(goldNoGtappListener))
      return false;
    this.m_buyWithGoldNoGTAPPListeners.Add(goldNoGtappListener);
    return true;
  }

  public bool RemoveBuyWithGoldNoGTAPPListener(Store.BuyWithGoldNoGTAPPCallback callback)
  {
    return this.RemoveBuyWithGoldNoGTAPPListener(callback, (object) null);
  }

  public bool RemoveBuyWithGoldNoGTAPPListener(Store.BuyWithGoldNoGTAPPCallback callback, object userData)
  {
    Store.BuyWithGoldNoGTAPPListener goldNoGtappListener = new Store.BuyWithGoldNoGTAPPListener();
    goldNoGtappListener.SetCallback(callback);
    goldNoGtappListener.SetUserData(userData);
    return this.m_buyWithGoldNoGTAPPListeners.Remove(goldNoGtappListener);
  }

  public bool RegisterExitListener(Store.ExitCallback callback)
  {
    return this.RegisterExitListener(callback, (object) null);
  }

  public bool RegisterExitListener(Store.ExitCallback callback, object userData)
  {
    Store.ExitListener exitListener = new Store.ExitListener();
    exitListener.SetCallback(callback);
    exitListener.SetUserData(userData);
    if (this.m_exitListeners.Contains(exitListener))
      return false;
    this.m_exitListeners.Add(exitListener);
    return true;
  }

  public bool RemoveExitListener(Store.ExitCallback callback)
  {
    return this.RemoveExitListener(callback, (object) null);
  }

  public bool RemoveExitListener(Store.ExitCallback callback, object userData)
  {
    Store.ExitListener exitListener = new Store.ExitListener();
    exitListener.SetCallback(callback);
    exitListener.SetUserData(userData);
    return this.m_exitListeners.Remove(exitListener);
  }

  public bool RegisterReadyListener(Store.ReadyCallback callback)
  {
    return this.RegisterReadyListener(callback, (object) null);
  }

  public bool RegisterReadyListener(Store.ReadyCallback callback, object userData)
  {
    Store.ReadyListener readyListener = new Store.ReadyListener();
    readyListener.SetCallback(callback);
    readyListener.SetUserData(userData);
    if (this.m_readyListeners.Contains(readyListener))
      return false;
    this.m_readyListeners.Add(readyListener);
    return true;
  }

  public bool RemoveReadyListener(Store.ReadyCallback callback)
  {
    return this.RemoveReadyListener(callback, (object) null);
  }

  public bool RemoveReadyListener(Store.ReadyCallback callback, object userData)
  {
    Store.ReadyListener readyListener = new Store.ReadyListener();
    readyListener.SetCallback(callback);
    readyListener.SetUserData(userData);
    return this.m_readyListeners.Remove(readyListener);
  }

  public bool RegisterInfoListener(Store.InfoCallback callback)
  {
    return this.RegisterInfoListener(callback, (object) null);
  }

  public bool RegisterInfoListener(Store.InfoCallback callback, object userData)
  {
    Store.InfoListener infoListener = new Store.InfoListener();
    infoListener.SetCallback(callback);
    infoListener.SetUserData(userData);
    if (this.m_infoListeners.Contains(infoListener))
      return false;
    this.m_infoListeners.Add(infoListener);
    return true;
  }

  public bool RemoveInfoListener(Store.InfoCallback callback)
  {
    return this.RemoveInfoListener(callback, (object) null);
  }

  public bool RemoveInfoListener(Store.InfoCallback callback, object userData)
  {
    Store.InfoListener infoListener = new Store.InfoListener();
    infoListener.SetCallback(callback);
    infoListener.SetUserData(userData);
    return this.m_infoListeners.Remove(infoListener);
  }

  protected virtual void BuyWithGold(UIEvent e)
  {
  }

  protected virtual void BuyWithMoney(UIEvent e)
  {
  }

  protected virtual void ShowImpl(Store.DelOnStoreShown onStoreShownCB, bool isTotallyFake)
  {
  }

  public void FireExitEvent(bool authorizationBackButtonPressed)
  {
    foreach (Store.ExitListener exitListener in this.m_exitListeners.ToArray())
      exitListener.Fire(authorizationBackButtonPressed);
  }

  protected void EnableFullScreenEffects(bool enable)
  {
    if ((Object) FullScreenFXMgr.Get() == (Object) null)
      return;
    if (enable)
      FullScreenFXMgr.Get().StartStandardBlurVignette(1f);
    else
      FullScreenFXMgr.Get().EndStandardBlurVignette(1f, (FullScreenFXMgr.EffectListener) null);
  }

  protected void FireBuyWithMoneyEvent(string productID, int quantity)
  {
    foreach (Store.BuyWithMoneyListener withMoneyListener in this.m_buyWithMoneyListeners.ToArray())
      withMoneyListener.Fire(productID, quantity);
  }

  protected void FireBuyWithGoldEventGTAPP(string productID, int quantity)
  {
    foreach (Store.BuyWithGoldGTAPPListener goldGtappListener in this.m_buyWithGoldGTAPPListeners.ToArray())
      goldGtappListener.Fire(productID, quantity);
  }

  protected void FireBuyWithGoldEventNoGTAPP(NoGTAPPTransactionData noGTAPPTransactionData)
  {
    foreach (Store.BuyWithGoldNoGTAPPListener goldNoGtappListener in this.m_buyWithGoldNoGTAPPListeners.ToArray())
      goldNoGtappListener.Fire(noGTAPPTransactionData);
  }

  protected void SetGoldButtonState(Store.BuyButtonState state)
  {
    this.m_goldButtonState = state;
    this.UpdateBuyButtonsState();
  }

  protected void SetMoneyButtonState(Store.BuyButtonState state)
  {
    this.m_moneyButtonState = state;
    this.UpdateBuyButtonsState();
  }

  protected bool AllowBuyingWithGold()
  {
    if (this.m_buyButtonsEnabled)
      return Store.BuyButtonState.ENABLED == this.m_goldButtonState;
    return false;
  }

  protected bool AllowBuyingWithMoney()
  {
    if (this.m_buyButtonsEnabled)
      return Store.BuyButtonState.ENABLED == this.m_moneyButtonState;
    return false;
  }

  protected bool CanShowBuyWithGoldTooltip()
  {
    if (this.AllowBuyingWithGold())
      return false;
    return this.m_goldButtonState != Store.BuyButtonState.DISABLED_NO_TOOLTIP;
  }

  protected bool CanShowBuyWithMoneyTooltip()
  {
    if (this.AllowBuyingWithMoney())
      return false;
    return this.m_moneyButtonState != Store.BuyButtonState.DISABLED_NO_TOOLTIP;
  }

  private void EnableBuyButtons(bool buyButtonsEnabled)
  {
    this.m_buyButtonsEnabled = buyButtonsEnabled;
    this.UpdateBuyButtonsState();
  }

  private void UpdateBuyButtonsState()
  {
    bool flag1 = this.AllowBuyingWithMoney();
    bool flag2 = this.AllowBuyingWithGold();
    this.UpdateBuyButtonMaterial(this.m_moneyButtonMeshes, !flag1 ? this.m_disabledMoneyButtonMaterial : this.m_enabledMoneyButtonMaterial);
    this.UpdateBuyButtonMaterial(this.m_goldButtonMeshes, !flag2 ? this.m_disabledGoldButtonMaterial : this.m_enabledGoldButtonMaterial);
    this.m_buyWithGoldButton.GetComponent<Collider>().enabled = flag2;
    this.m_buyWithGoldTooltipTrigger.GetComponent<Collider>().enabled = this.CanShowBuyWithGoldTooltip();
    this.m_buyWithMoneyButton.GetComponent<Collider>().enabled = flag1;
    this.m_buyWithMoneyTooltipTrigger.GetComponent<Collider>().enabled = this.CanShowBuyWithMoneyTooltip();
  }

  private void UpdateBuyButtonMaterial(List<MeshRenderer> renderers, Material material)
  {
    using (List<MeshRenderer>.Enumerator enumerator = renderers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MeshRenderer current = enumerator.Current;
        if ((Object) current != (Object) null)
          current.material = material;
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator NotifyListenersWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Store.\u003CNotifyListenersWhenReady\u003Ec__IteratorA() { \u003C\u003Ef__this = this };
  }

  protected void OnShowBuyWithMoneyTooltip(UIEvent e)
  {
    if (this.m_moneyButtonState == Store.BuyButtonState.ENABLED)
      return;
    this.ShowBuyTooltip(this.m_buyWithMoneyTooltip, "GLUE_STORE_MONEY_BUTTON_TOOLTIP_HEADLINE", this.m_moneyButtonState);
  }

  protected void OnShowBuyWithGoldTooltip(UIEvent e)
  {
    if (this.m_goldButtonState == Store.BuyButtonState.ENABLED)
      return;
    this.ShowBuyTooltip(this.m_buyWithGoldTooltip, "GLUE_STORE_GOLD_BUTTON_TOOLTIP_HEADLINE", this.m_goldButtonState);
  }

  private void ShowBuyTooltip(TooltipZone tooltipZone, string tooltipText, Store.BuyButtonState buttonState)
  {
    tooltipZone.ShowLayerTooltip(GameStrings.Get(tooltipText), this.GetBuyButtonTooltipMessage(buttonState));
  }

  protected void OnHideBuyWithMoneyTooltip(UIEvent e)
  {
    if (this.m_moneyButtonState == Store.BuyButtonState.ENABLED)
      return;
    this.m_buyWithMoneyTooltip.HideTooltip();
  }

  protected void OnHideBuyWithGoldTooltip(UIEvent e)
  {
    if (this.m_goldButtonState == Store.BuyButtonState.ENABLED)
      return;
    this.m_buyWithGoldTooltip.HideTooltip();
  }

  protected void OnBuyWithGoldPressed(UIEvent e)
  {
    if (!this.AllowBuyingWithGold())
      return;
    this.BuyWithGold(e);
  }

  protected void OnBuyWithMoneyPressed(UIEvent e)
  {
    if (!this.AllowBuyingWithMoney())
      return;
    this.BuyWithMoney(e);
  }

  protected void OnInfoPressed(UIEvent e)
  {
    foreach (Store.InfoListener infoListener in this.m_infoListeners.ToArray())
      infoListener.Fire();
  }

  protected virtual string GetOwnedTooltipString()
  {
    return GameStrings.Get("GLUE_STORE_DUNGEON_BUTTON_TEXT_PURCHASED");
  }

  private string GetBuyButtonTooltipMessage(Store.BuyButtonState state)
  {
    switch (state)
    {
      case Store.BuyButtonState.DISABLED_NOT_ENOUGH_GOLD:
        return GameStrings.Get("GLUE_STORE_FAIL_NOT_ENOUGH_GOLD");
      case Store.BuyButtonState.DISABLED_FEATURE:
        return GameStrings.Get("GLUE_STORE_DISABLED");
      case Store.BuyButtonState.DISABLED_OWNED:
        return this.GetOwnedTooltipString();
      case Store.BuyButtonState.DISABLED_NO_TOOLTIP:
        return string.Empty;
      default:
        return GameStrings.Get("GLUE_TOOLTIP_BUTTON_DISABLED_DESC");
    }
  }

  protected enum BuyButtonState
  {
    ENABLED,
    DISABLED_NOT_ENOUGH_GOLD,
    DISABLED_FEATURE,
    DISABLED,
    DISABLED_OWNED,
    DISABLED_NO_TOOLTIP,
  }

  private class ExitListener : EventListener<Store.ExitCallback>
  {
    public void Fire(bool authorizationBackButtonPressed)
    {
      this.m_callback(authorizationBackButtonPressed, this.m_userData);
    }
  }

  private class BuyWithMoneyListener : EventListener<Store.BuyWithMoneyCallback>
  {
    public void Fire(string productID, int quantity)
    {
      this.m_callback(productID, quantity, this.m_userData);
    }
  }

  private class BuyWithGoldGTAPPListener : EventListener<Store.BuyWithGoldGTAPPCallback>
  {
    public void Fire(string productID, int quantity)
    {
      this.m_callback(productID, quantity, this.m_userData);
    }
  }

  private class BuyWithGoldNoGTAPPListener : EventListener<Store.BuyWithGoldNoGTAPPCallback>
  {
    public void Fire(NoGTAPPTransactionData noGTAPPTransactionData)
    {
      this.m_callback(noGTAPPTransactionData, this.m_userData);
    }
  }

  private class ReadyListener : EventListener<Store.ReadyCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  private class InfoListener : EventListener<Store.InfoCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  public delegate void ExitCallback(bool authorizationBackButtonPressed, object userData);

  public delegate void BuyWithMoneyCallback(string productID, int quantity, object userData);

  public delegate void BuyWithGoldGTAPPCallback(string productID, int quantity, object userData);

  public delegate void BuyWithGoldNoGTAPPCallback(NoGTAPPTransactionData noGTAPPTransactionData, object userData);

  public delegate void ReadyCallback(object userData);

  public delegate void InfoCallback(object userData);

  public delegate void DelOnStoreShown();
}
