﻿// Decompiled with JetBrains decompiler
// Type: ErrorParams
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class ErrorParams
{
  public bool m_allowClick = true;
  public ErrorType m_type;
  public string m_header;
  public string m_message;
  public Error.AcknowledgeCallback m_ackCallback;
  public object m_ackUserData;
  public bool m_redirectToStore;
  public float m_delayBeforeNextReset;
}
