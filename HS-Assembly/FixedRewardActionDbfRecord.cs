﻿// Decompiled with JetBrains decompiler
// Type: FixedRewardActionDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FixedRewardActionDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private string m_Type;
  [SerializeField]
  private int m_WingId;
  [SerializeField]
  private int m_WingProgress;
  [SerializeField]
  private ulong m_WingFlags;
  [SerializeField]
  private int m_ClassId;
  [SerializeField]
  private int m_HeroLevel;
  [SerializeField]
  private int m_TutorialProgress;
  [SerializeField]
  private ulong m_MetaActionFlags;
  [SerializeField]
  private int m_AchieveId;
  [SerializeField]
  private long m_AccountLicenseId;
  [SerializeField]
  private ulong m_AccountLicenseFlags;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("TYPE", "action trigger")]
  public string Type
  {
    get
    {
      return this.m_Type;
    }
  }

  [DbfField("WING_ID", "WING.ID required for this action. 0 means ignore.")]
  public int WingId
  {
    get
    {
      return this.m_WingId;
    }
  }

  [DbfField("WING_PROGRESS", "wing progress required for this action. 0 means ignore.")]
  public int WingProgress
  {
    get
    {
      return this.m_WingProgress;
    }
  }

  [DbfField("WING_FLAGS", "wing flags required for this action. 0 means ignore. flags are defined by data in ADVENTURE_MISSION table's GRANTS_FLAGS column.")]
  public ulong WingFlags
  {
    get
    {
      return this.m_WingFlags;
    }
  }

  [DbfField("CLASS_ID", "CLASS.ID required for this action. 0 means ignore.")]
  public int ClassId
  {
    get
    {
      return this.m_ClassId;
    }
  }

  [DbfField("HERO_LEVEL", "hero level required for this action. 0 means ignore.")]
  public int HeroLevel
  {
    get
    {
      return this.m_HeroLevel;
    }
  }

  [DbfField("TUTORIAL_PROGRESS", "tutorial progress required for this action. 0 means ignore.")]
  public int TutorialProgress
  {
    get
    {
      return this.m_TutorialProgress;
    }
  }

  [DbfField("META_ACTION_FLAGS", "flags required for this meta-action. 0 means ignore. flags are defined by data in FIXED_REWARD table's META_ACTION_FLAGS column only (not defined in code anywhere).")]
  public ulong MetaActionFlags
  {
    get
    {
      return this.m_MetaActionFlags;
    }
  }

  [DbfField("ACHIEVE_ID", "ACHIEVE.ID required for this action. 0 means ignore.")]
  public int AchieveId
  {
    get
    {
      return this.m_AchieveId;
    }
  }

  [DbfField("ACCOUNT_LICENSE_ID", "ACCOUNT_LICENSE.ID required for this action. 0 means ignore.")]
  public long AccountLicenseId
  {
    get
    {
      return this.m_AccountLicenseId;
    }
  }

  [DbfField("ACCOUNT_LICENSE_FLAGS", "account license flags required for this action. 0 means ignore.")]
  public ulong AccountLicenseFlags
  {
    get
    {
      return this.m_AccountLicenseFlags;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    FixedRewardActionDbfAsset rewardActionDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (FixedRewardActionDbfAsset)) as FixedRewardActionDbfAsset;
    if ((UnityEngine.Object) rewardActionDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("FixedRewardActionDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < rewardActionDbfAsset.Records.Count; ++index)
      rewardActionDbfAsset.Records[index].StripUnusedLocales();
    records = (object) rewardActionDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetType(string v)
  {
    this.m_Type = v;
  }

  public void SetWingId(int v)
  {
    this.m_WingId = v;
  }

  public void SetWingProgress(int v)
  {
    this.m_WingProgress = v;
  }

  public void SetWingFlags(ulong v)
  {
    this.m_WingFlags = v;
  }

  public void SetClassId(int v)
  {
    this.m_ClassId = v;
  }

  public void SetHeroLevel(int v)
  {
    this.m_HeroLevel = v;
  }

  public void SetTutorialProgress(int v)
  {
    this.m_TutorialProgress = v;
  }

  public void SetMetaActionFlags(ulong v)
  {
    this.m_MetaActionFlags = v;
  }

  public void SetAchieveId(int v)
  {
    this.m_AchieveId = v;
  }

  public void SetAccountLicenseId(long v)
  {
    this.m_AccountLicenseId = v;
  }

  public void SetAccountLicenseFlags(ulong v)
  {
    this.m_AccountLicenseFlags = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardActionDbfRecord.\u003C\u003Ef__switch\u0024map3B == null)
      {
        // ISSUE: reference to a compiler-generated field
        FixedRewardActionDbfRecord.\u003C\u003Ef__switch\u0024map3B = new Dictionary<string, int>(13)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TYPE",
            2
          },
          {
            "WING_ID",
            3
          },
          {
            "WING_PROGRESS",
            4
          },
          {
            "WING_FLAGS",
            5
          },
          {
            "CLASS_ID",
            6
          },
          {
            "HERO_LEVEL",
            7
          },
          {
            "TUTORIAL_PROGRESS",
            8
          },
          {
            "META_ACTION_FLAGS",
            9
          },
          {
            "ACHIEVE_ID",
            10
          },
          {
            "ACCOUNT_LICENSE_ID",
            11
          },
          {
            "ACCOUNT_LICENSE_FLAGS",
            12
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardActionDbfRecord.\u003C\u003Ef__switch\u0024map3B.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Type;
          case 3:
            return (object) this.WingId;
          case 4:
            return (object) this.WingProgress;
          case 5:
            return (object) this.WingFlags;
          case 6:
            return (object) this.ClassId;
          case 7:
            return (object) this.HeroLevel;
          case 8:
            return (object) this.TutorialProgress;
          case 9:
            return (object) this.MetaActionFlags;
          case 10:
            return (object) this.AchieveId;
          case 11:
            return (object) this.AccountLicenseId;
          case 12:
            return (object) this.AccountLicenseFlags;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (FixedRewardActionDbfRecord.\u003C\u003Ef__switch\u0024map3C == null)
    {
      // ISSUE: reference to a compiler-generated field
      FixedRewardActionDbfRecord.\u003C\u003Ef__switch\u0024map3C = new Dictionary<string, int>(13)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "TYPE",
          2
        },
        {
          "WING_ID",
          3
        },
        {
          "WING_PROGRESS",
          4
        },
        {
          "WING_FLAGS",
          5
        },
        {
          "CLASS_ID",
          6
        },
        {
          "HERO_LEVEL",
          7
        },
        {
          "TUTORIAL_PROGRESS",
          8
        },
        {
          "META_ACTION_FLAGS",
          9
        },
        {
          "ACHIEVE_ID",
          10
        },
        {
          "ACCOUNT_LICENSE_ID",
          11
        },
        {
          "ACCOUNT_LICENSE_FLAGS",
          12
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!FixedRewardActionDbfRecord.\u003C\u003Ef__switch\u0024map3C.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetType((string) val);
        break;
      case 3:
        this.SetWingId((int) val);
        break;
      case 4:
        this.SetWingProgress((int) val);
        break;
      case 5:
        this.SetWingFlags((ulong) val);
        break;
      case 6:
        this.SetClassId((int) val);
        break;
      case 7:
        this.SetHeroLevel((int) val);
        break;
      case 8:
        this.SetTutorialProgress((int) val);
        break;
      case 9:
        this.SetMetaActionFlags((ulong) val);
        break;
      case 10:
        this.SetAchieveId((int) val);
        break;
      case 11:
        this.SetAccountLicenseId((long) val);
        break;
      case 12:
        this.SetAccountLicenseFlags((ulong) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardActionDbfRecord.\u003C\u003Ef__switch\u0024map3D == null)
      {
        // ISSUE: reference to a compiler-generated field
        FixedRewardActionDbfRecord.\u003C\u003Ef__switch\u0024map3D = new Dictionary<string, int>(13)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TYPE",
            2
          },
          {
            "WING_ID",
            3
          },
          {
            "WING_PROGRESS",
            4
          },
          {
            "WING_FLAGS",
            5
          },
          {
            "CLASS_ID",
            6
          },
          {
            "HERO_LEVEL",
            7
          },
          {
            "TUTORIAL_PROGRESS",
            8
          },
          {
            "META_ACTION_FLAGS",
            9
          },
          {
            "ACHIEVE_ID",
            10
          },
          {
            "ACCOUNT_LICENSE_ID",
            11
          },
          {
            "ACCOUNT_LICENSE_FLAGS",
            12
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardActionDbfRecord.\u003C\u003Ef__switch\u0024map3D.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (string);
          case 3:
            return typeof (int);
          case 4:
            return typeof (int);
          case 5:
            return typeof (ulong);
          case 6:
            return typeof (int);
          case 7:
            return typeof (int);
          case 8:
            return typeof (int);
          case 9:
            return typeof (ulong);
          case 10:
            return typeof (int);
          case 11:
            return typeof (long);
          case 12:
            return typeof (ulong);
        }
      }
    }
    return (System.Type) null;
  }
}
