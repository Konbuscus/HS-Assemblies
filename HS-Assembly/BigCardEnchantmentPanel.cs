﻿// Decompiled with JetBrains decompiler
// Type: BigCardEnchantmentPanel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BigCardEnchantmentPanel : MonoBehaviour
{
  public Actor m_Actor;
  public UberText m_HeaderText;
  public UberText m_BodyText;
  public GameObject m_Background;
  private Entity m_enchantment;
  private Vector3 m_initialScale;
  private float m_initialBackgroundHeight;
  private Vector3 m_initialBackgroundScale;
  private bool m_shown;

  private void Awake()
  {
    this.m_initialScale = this.transform.localScale;
    this.m_initialBackgroundHeight = this.m_Background.GetComponentInChildren<MeshRenderer>().bounds.size.z;
    this.m_initialBackgroundScale = this.m_Background.transform.localScale;
  }

  public void SetEnchantment(Entity enchantment)
  {
    this.m_enchantment = enchantment;
    DefLoader.Get().LoadCardDef(this.m_enchantment.GetCardId(), new DefLoader.LoadDefCallback<CardDef>(this.OnCardDefLoaded), (object) null, new CardPortraitQuality(1, this.m_enchantment.GetPremiumType()));
  }

  public void Show()
  {
    if (this.m_shown)
      return;
    this.m_shown = true;
    this.gameObject.SetActive(true);
    this.UpdateLayout();
  }

  public void Hide()
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    this.gameObject.SetActive(false);
  }

  public void ResetScale()
  {
    this.transform.localScale = this.m_initialScale;
    this.m_Background.transform.localScale = this.m_initialBackgroundScale;
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  public float GetHeight()
  {
    return this.m_Background.GetComponentInChildren<MeshRenderer>().bounds.size.z;
  }

  private void OnCardDefLoaded(string cardId, CardDef cardDef, object callbackData)
  {
    if ((Object) cardDef != (Object) null)
    {
      if ((Object) cardDef.GetEnchantmentPortrait() != (Object) null)
        this.m_Actor.GetMeshRenderer().material = cardDef.GetEnchantmentPortrait();
      else
        this.m_Actor.SetPortraitTextureOverride(cardDef.GetPortraitTexture());
    }
    this.m_HeaderText.Text = this.m_enchantment.GetName();
    this.m_BodyText.Text = this.m_enchantment.GetCardTextInHand();
  }

  private void UpdateLayout()
  {
    this.m_HeaderText.UpdateNow();
    this.m_BodyText.UpdateNow();
    Bounds bounds = this.m_Actor.GetMeshRenderer().bounds;
    Bounds worldSpaceBounds1 = this.m_HeaderText.GetTextWorldSpaceBounds();
    Bounds worldSpaceBounds2 = this.m_BodyText.GetTextWorldSpaceBounds();
    float z1 = bounds.min.z;
    float z2 = bounds.max.z;
    float z3 = worldSpaceBounds1.min.z;
    float z4 = worldSpaceBounds1.max.z;
    float z5 = worldSpaceBounds2.min.z;
    float z6 = worldSpaceBounds2.max.z;
    float num1 = Mathf.Min(Mathf.Min(z1, z3), z5);
    float num2 = (float) ((double) Mathf.Max(Mathf.Max(z2, z4), z6) - (double) num1 + 0.100000001490116);
    this.transform.localScale = this.m_initialScale;
    this.transform.localEulerAngles = Vector3.zero;
    TransformUtil.SetLocalScaleZ(this.m_Background, this.m_initialBackgroundScale.z * (num2 / this.m_initialBackgroundHeight));
  }
}
