﻿// Decompiled with JetBrains decompiler
// Type: VarianWrynn
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;

public class VarianWrynn : SuperSpell
{
  public float m_spellLeadTime = 1f;
  public string m_perMinionSound;
  public Spell m_varianSpellPrefab;
  public Spell m_deckSpellPrefab;

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    this.StartCoroutine(this.DoVariansCoolThing());
  }

  [DebuggerHidden]
  private IEnumerator DoVariansCoolThing()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new VarianWrynn.\u003CDoVariansCoolThing\u003Ec__Iterator2D5() { \u003C\u003Ef__this = this };
  }

  private bool IsMinion(Network.HistShowEntity showEntity)
  {
    for (int index = 0; index < showEntity.Entity.Tags.Count; ++index)
    {
      Network.Entity.Tag tag = showEntity.Entity.Tags[index];
      if (tag.Name == 202)
        return tag.Value == 4;
    }
    return false;
  }
}
