﻿// Decompiled with JetBrains decompiler
// Type: TriggerSpellController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TriggerSpellController : SpellController
{
  private List<CardSoundSpell> m_triggerSoundSpells = new List<CardSoundSpell>();
  private Spell m_triggerSpell;
  private Spell m_actorTriggerSpell;
  private int m_cardEffectsBlockingFinish;
  private int m_cardEffectsBlockingTaskListFinish;

  protected override bool AddPowerSourceAndTargets(PowerTaskList taskList)
  {
    if (!this.HasSourceCard(taskList))
      return false;
    Entity sourceEntity = taskList.GetSourceEntity();
    Card card = sourceEntity.GetCard();
    CardEffect effect = this.InitEffect(card);
    if (effect != null)
    {
      this.InitTriggerSpell(effect, card);
      this.InitTriggerSounds(effect, card);
    }
    if (this.CanPlayActorTriggerSpell(sourceEntity))
      this.m_actorTriggerSpell = this.GetActorTriggerSpell(sourceEntity);
    if ((Object) this.m_triggerSpell == (Object) null && this.m_triggerSoundSpells.Count == 0 && (Object) this.m_actorTriggerSpell == (Object) null)
    {
      this.Reset();
      return TurnStartManager.Get().IsCardDrawHandled(card);
    }
    this.SetSource(card);
    return true;
  }

  protected override bool HasSourceCard(PowerTaskList taskList)
  {
    return taskList.GetSourceEntity() != null && !((Object) this.GetCardWithActorTrigger(taskList) == (Object) null);
  }

  protected override void OnProcessTaskList()
  {
    if (!this.ActivateInitialSpell())
    {
      base.OnProcessTaskList();
    }
    else
    {
      if (!GameState.Get().IsTurnStartManagerActive())
        return;
      TurnStartManager.Get().NotifyOfTriggerVisual();
    }
  }

  protected override void OnFinished()
  {
    if (this.m_processingTaskList)
      this.m_pendingFinish = true;
    else
      this.StartCoroutine(this.WaitThenFinish());
  }

  private void Reset()
  {
    if ((Object) this.m_triggerSpell != (Object) null && this.m_triggerSpell.GetPowerTaskList().GetId() == this.m_taskListId)
      SpellUtils.PurgeSpell(this.m_triggerSpell);
    if (this.m_triggerSoundSpells != null)
    {
      for (int index = 0; index < this.m_triggerSoundSpells.Count; ++index)
      {
        CardSoundSpell triggerSoundSpell = this.m_triggerSoundSpells[index];
        if ((Object) triggerSoundSpell != (Object) null && triggerSoundSpell.GetPowerTaskList().GetId() == this.m_taskListId)
          SpellUtils.PurgeSpell((Spell) triggerSoundSpell);
      }
    }
    this.m_triggerSpell = (Spell) null;
    this.m_triggerSoundSpells.Clear();
    this.m_actorTriggerSpell = (Spell) null;
    this.m_cardEffectsBlockingFinish = 0;
    this.m_cardEffectsBlockingTaskListFinish = 0;
  }

  [DebuggerHidden]
  private IEnumerator WaitThenFinish()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TriggerSpellController.\u003CWaitThenFinish\u003Ec__Iterator253() { \u003C\u003Ef__this = this };
  }

  private bool ActivateInitialSpell()
  {
    return this.ActivateActorTriggerSpell() || this.ActivateCardEffects();
  }

  private Card GetCardWithActorTrigger(PowerTaskList taskList)
  {
    return this.GetCardWithActorTrigger(taskList.GetSourceEntity());
  }

  private Card GetCardWithActorTrigger(Entity entity)
  {
    if (entity == null)
      return (Card) null;
    Card card;
    if (entity.IsEnchantment())
    {
      Entity entity1 = GameState.Get().GetEntity(entity.GetAttached());
      if (entity1 == null)
        return (Card) null;
      card = entity1.GetCard();
    }
    else
      card = entity.GetCard();
    return card;
  }

  private bool CanPlayActorTriggerSpell(Entity entity)
  {
    if (!entity.HasTriggerVisual() && !entity.IsPoisonous() && !entity.HasInspire() || entity.GetController() != null && !entity.GetController().IsFriendlySide() && entity.IsObfuscated())
      return false;
    Card withActorTrigger = this.GetCardWithActorTrigger(entity);
    return !((Object) withActorTrigger == (Object) null) && !withActorTrigger.WillSuppressActorTriggerSpell() && ((Object) this.m_triggerSpell != (Object) null || SpellUtils.CanAddPowerTargets(this.m_taskList));
  }

  private Spell GetActorTriggerSpell(Entity entity)
  {
    Card withActorTrigger = this.GetCardWithActorTrigger(entity);
    SpellType spellType;
    if (entity.HasTriggerVisual())
      spellType = !GameState.Get().IsUsingFastActorTriggers() ? SpellType.TRIGGER : SpellType.FAST_TRIGGER;
    else if (entity.IsPoisonous())
    {
      spellType = SpellType.POISONOUS;
    }
    else
    {
      if (!entity.HasInspire())
        return (Spell) null;
      spellType = SpellType.INSPIRE;
    }
    return withActorTrigger.GetActorSpell(spellType, true);
  }

  private bool ActivateActorTriggerSpell()
  {
    if ((Object) this.m_actorTriggerSpell == (Object) null)
      return false;
    this.GetCardWithActorTrigger(this.m_taskList.GetSourceEntity()).DeactivateBaubles();
    this.m_actorTriggerSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnActorTriggerSpellStateFinished));
    this.m_actorTriggerSpell.ActivateState(SpellStateType.ACTION);
    return true;
  }

  private void OnActorTriggerSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (prevStateType != SpellStateType.ACTION)
      return;
    spell.RemoveStateFinishedCallback(new Spell.StateFinishedCallback(this.OnActorTriggerSpellStateFinished));
    if (this.ActivateCardEffects())
      return;
    base.OnProcessTaskList();
  }

  private CardEffect InitEffect(Card card)
  {
    if ((Object) card == (Object) null)
      return (CardEffect) null;
    Network.HistBlockStart blockStart = this.m_taskList.GetBlockStart();
    string effectCardId = blockStart.EffectCardId;
    int effectIndex = blockStart.EffectIndex;
    if (effectIndex < 0)
      return (CardEffect) null;
    CardEffect cardEffect;
    if (string.IsNullOrEmpty(effectCardId))
    {
      cardEffect = card.GetTriggerEffect(effectIndex);
    }
    else
    {
      CardDef cardDef = DefLoader.Get().GetCardDef(effectCardId, (CardPortraitQuality) null);
      if (cardDef.m_TriggerEffectDefs == null)
        return (CardEffect) null;
      if (effectIndex >= cardDef.m_TriggerEffectDefs.Count)
        return (CardEffect) null;
      cardEffect = new CardEffect(cardDef.m_TriggerEffectDefs[effectIndex], card);
    }
    return cardEffect;
  }

  private bool ActivateCardEffects()
  {
    bool flag1 = this.ActivateTriggerSpell();
    bool flag2 = this.ActivateTriggerSounds();
    if (!flag1)
      return flag2;
    return true;
  }

  private void OnCardSpellFinished(Spell spell, object userData)
  {
    this.CardSpellFinished();
  }

  private void OnCardSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    this.CardSpellNoneStateEntered();
  }

  private void CardSpellFinished()
  {
    --this.m_cardEffectsBlockingTaskListFinish;
    if (this.m_cardEffectsBlockingTaskListFinish > 0)
      return;
    this.OnFinishedTaskList();
  }

  private void CardSpellNoneStateEntered()
  {
    --this.m_cardEffectsBlockingFinish;
    if (this.m_cardEffectsBlockingFinish > 0)
      return;
    this.OnFinished();
  }

  private void InitTriggerSpell(CardEffect effect, Card card)
  {
    Spell spell = effect.GetSpell(true);
    if ((Object) spell == (Object) null)
      return;
    if (!spell.AttachPowerTaskList(this.m_taskList))
    {
      Log.Power.Print("{0}.InitTriggerSpell() - FAILED to add targets to spell for {1}", new object[2]
      {
        (object) this,
        (object) card
      });
    }
    else
    {
      this.m_triggerSpell = spell;
      ++this.m_cardEffectsBlockingFinish;
      ++this.m_cardEffectsBlockingTaskListFinish;
    }
  }

  private bool ActivateTriggerSpell()
  {
    if ((Object) this.m_triggerSpell == (Object) null)
      return false;
    this.m_triggerSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnCardSpellFinished));
    this.m_triggerSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnCardSpellStateFinished));
    this.m_triggerSpell.ActivateState(SpellStateType.ACTION);
    return true;
  }

  private bool InitTriggerSounds(CardEffect effect, Card card)
  {
    List<CardSoundSpell> soundSpells = effect.GetSoundSpells(true);
    if (soundSpells == null || soundSpells.Count == 0)
      return false;
    using (List<CardSoundSpell>.Enumerator enumerator = soundSpells.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardSoundSpell current = enumerator.Current;
        if ((bool) ((Object) current))
        {
          if (!current.AttachPowerTaskList(this.m_taskList))
            Log.Power.Print("{0}.InitTriggerSounds() - FAILED to attach task list to TriggerSoundSpell {1} for Card {2}", new object[3]
            {
              (object) this.name,
              (object) current,
              (object) card
            });
          else
            this.m_triggerSoundSpells.Add(current);
        }
      }
    }
    if (this.m_triggerSoundSpells.Count == 0)
      return false;
    ++this.m_cardEffectsBlockingFinish;
    ++this.m_cardEffectsBlockingTaskListFinish;
    return true;
  }

  private bool ActivateTriggerSounds()
  {
    if (this.m_triggerSoundSpells.Count == 0)
      return false;
    Card source = this.GetSource();
    using (List<CardSoundSpell>.Enumerator enumerator = this.m_triggerSoundSpells.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardSoundSpell current = enumerator.Current;
        if ((bool) ((Object) current))
          source.ActivateSoundSpell(current);
      }
    }
    this.CardSpellFinished();
    this.CardSpellNoneStateEntered();
    return true;
  }
}
