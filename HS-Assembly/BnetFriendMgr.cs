﻿// Decompiled with JetBrains decompiler
// Type: BnetFriendMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using System.Collections.Generic;

public class BnetFriendMgr
{
  private List<BnetPlayer> m_friends = new List<BnetPlayer>();
  private List<BnetInvitation> m_receivedInvites = new List<BnetInvitation>();
  private List<BnetInvitation> m_sentInvites = new List<BnetInvitation>();
  private List<BnetFriendMgr.ChangeListener> m_changeListeners = new List<BnetFriendMgr.ChangeListener>();
  private PendingBnetFriendChangelist m_pendingChangelist = new PendingBnetFriendChangelist();
  private static BnetFriendMgr s_instance;
  private int m_maxFriends;
  private int m_maxReceivedInvites;
  private int m_maxSentInvites;

  public static BnetFriendMgr Get()
  {
    if (BnetFriendMgr.s_instance == null)
    {
      BnetFriendMgr.s_instance = new BnetFriendMgr();
      ApplicationMgr.Get().WillReset += new System.Action(BnetFriendMgr.s_instance.Clear);
    }
    return BnetFriendMgr.s_instance;
  }

  public void Initialize()
  {
    FriendMgr.Get();
    BnetEventMgr.Get().AddChangeListener(new BnetEventMgr.ChangeCallback(this.OnBnetEventOccurred));
    Network.Get().SetFriendsHandler(new Network.FriendsHandler(this.OnFriendsUpdate));
    Network.Get().AddBnetErrorListener(BnetFeature.Friends, new Network.BnetErrorCallback(this.OnBnetError));
    this.InitMaximums();
  }

  public void Shutdown()
  {
    Network.Get().RemoveBnetErrorListener(BnetFeature.Friends, new Network.BnetErrorCallback(this.OnBnetError));
    Network.Get().SetFriendsHandler((Network.FriendsHandler) null);
  }

  public int GetMaxFriends()
  {
    return this.m_maxFriends;
  }

  public int GetMaxReceivedInvites()
  {
    return this.m_maxReceivedInvites;
  }

  public int GetMaxSentInvites()
  {
    return this.m_maxSentInvites;
  }

  public BnetPlayer FindFriend(BnetAccountId id)
  {
    return this.FindNonPendingFriend(id) ?? this.FindPendingFriend(id) ?? (BnetPlayer) null;
  }

  public BnetPlayer FindFriend(BnetGameAccountId id)
  {
    return this.FindNonPendingFriend(id) ?? this.FindPendingFriend(id) ?? (BnetPlayer) null;
  }

  public bool IsFriend(BnetPlayer player)
  {
    return this.IsNonPendingFriend(player) || this.IsPendingFriend(player);
  }

  public bool IsFriend(BnetAccountId id)
  {
    return this.IsNonPendingFriend(id) || this.IsPendingFriend(id);
  }

  public bool IsFriend(BnetGameAccountId id)
  {
    return this.IsNonPendingFriend(id) || this.IsPendingFriend(id);
  }

  public List<BnetPlayer> GetFriends()
  {
    return this.m_friends;
  }

  public int GetFriendCount()
  {
    return this.m_friends.Count;
  }

  public bool HasOnlineFriends()
  {
    using (List<BnetPlayer>.Enumerator enumerator = this.m_friends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.IsOnline())
          return true;
      }
    }
    return false;
  }

  public int GetOnlineFriendCount()
  {
    int num = 0;
    using (List<BnetPlayer>.Enumerator enumerator = this.m_friends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.IsOnline())
          ++num;
      }
    }
    return num;
  }

  public int GetActiveOnlineFriendCount()
  {
    int num = 0;
    using (List<BnetPlayer>.Enumerator enumerator = this.m_friends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer current = enumerator.Current;
        if (current.IsOnline() && !((bgs.FourCC) current.GetBestProgramId() == (bgs.FourCC) null) && (!current.GetBestProgramId().IsPhoenix() || current.GetBestAwayTimeMicrosec() <= 0UL && !current.IsBusy()))
          ++num;
      }
    }
    return num;
  }

  public BnetPlayer FindNonPendingFriend(BnetAccountId id)
  {
    using (List<BnetPlayer>.Enumerator enumerator = this.m_friends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer current = enumerator.Current;
        if ((BnetEntityId) current.GetAccountId() == (BnetEntityId) id)
          return current;
      }
    }
    return (BnetPlayer) null;
  }

  public BnetPlayer FindNonPendingFriend(BnetGameAccountId id)
  {
    using (List<BnetPlayer>.Enumerator enumerator = this.m_friends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer current = enumerator.Current;
        if (current.HasGameAccount(id))
          return current;
      }
    }
    return (BnetPlayer) null;
  }

  public bool IsNonPendingFriend(BnetPlayer player)
  {
    if (player == null)
      return false;
    if (this.m_friends.Contains(player))
      return true;
    BnetAccountId accountId = player.GetAccountId();
    if ((BnetEntityId) accountId != (BnetEntityId) null)
      return this.IsFriend(accountId);
    using (Map<BnetGameAccountId, BnetGameAccount>.KeyCollection.Enumerator enumerator = player.GetGameAccounts().Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (this.IsFriend(enumerator.Current))
          return true;
      }
    }
    return false;
  }

  public bool IsNonPendingFriend(BnetAccountId id)
  {
    return this.FindNonPendingFriend(id) != null;
  }

  public bool IsNonPendingFriend(BnetGameAccountId id)
  {
    return this.FindNonPendingFriend(id) != null;
  }

  public BnetPlayer FindPendingFriend(BnetAccountId id)
  {
    return this.m_pendingChangelist.FindFriend(id);
  }

  public BnetPlayer FindPendingFriend(BnetGameAccountId id)
  {
    return this.m_pendingChangelist.FindFriend(id);
  }

  public bool IsPendingFriend(BnetPlayer player)
  {
    return this.m_pendingChangelist.IsFriend(player);
  }

  public bool IsPendingFriend(BnetAccountId id)
  {
    return this.m_pendingChangelist.IsFriend(id);
  }

  public bool IsPendingFriend(BnetGameAccountId id)
  {
    return this.m_pendingChangelist.IsFriend(id);
  }

  public List<BnetPlayer> GetPendingFriends()
  {
    return this.m_pendingChangelist.GetFriends();
  }

  public List<BnetInvitation> GetReceivedInvites()
  {
    return this.m_receivedInvites;
  }

  public List<BnetInvitation> GetSentInvites()
  {
    return this.m_sentInvites;
  }

  public void AcceptInvite(BnetInvitationId inviteId)
  {
    Network.AcceptFriendInvite(inviteId);
  }

  public void DeclineInvite(BnetInvitationId inviteId)
  {
    Network.DeclineFriendInvite(inviteId);
  }

  public void IgnoreInvite(BnetInvitationId inviteId)
  {
    Network.IgnoreFriendInvite(inviteId);
  }

  public void RevokeInvite(BnetInvitationId inviteId)
  {
    Network.RevokeFriendInvite(inviteId);
  }

  public void SendInvite(string name)
  {
    if (name.Contains("@"))
      this.SendInviteByEmail(name);
    else
      this.SendInviteByBattleTag(name);
  }

  public void SendInviteByEmail(string email)
  {
    Network.SendFriendInviteByEmail(BnetPresenceMgr.Get().GetMyPlayer().GetFullName(), email);
  }

  public void SendInviteByBattleTag(string battleTagString)
  {
    Network.SendFriendInviteByBattleTag(BnetPresenceMgr.Get().GetMyPlayer().GetBattleTag().GetString(), battleTagString);
  }

  public bool RemoveFriend(BnetPlayer friend)
  {
    if (!this.m_friends.Contains(friend))
      return false;
    Network.RemoveFriend(friend.GetAccountId());
    return true;
  }

  public bool AddChangeListener(BnetFriendMgr.ChangeCallback callback)
  {
    return this.AddChangeListener(callback, (object) null);
  }

  public bool AddChangeListener(BnetFriendMgr.ChangeCallback callback, object userData)
  {
    BnetFriendMgr.ChangeListener changeListener = new BnetFriendMgr.ChangeListener();
    changeListener.SetCallback(callback);
    changeListener.SetUserData(userData);
    if (this.m_changeListeners.Contains(changeListener))
      return false;
    this.m_changeListeners.Add(changeListener);
    return true;
  }

  public bool RemoveChangeListener(BnetFriendMgr.ChangeCallback callback)
  {
    return this.RemoveChangeListener(callback, (object) null);
  }

  public bool RemoveChangeListener(BnetFriendMgr.ChangeCallback callback, object userData)
  {
    BnetFriendMgr.ChangeListener changeListener = new BnetFriendMgr.ChangeListener();
    changeListener.SetCallback(callback);
    changeListener.SetUserData(userData);
    return this.m_changeListeners.Remove(changeListener);
  }

  private void InitMaximums()
  {
    FriendsInfo info = new FriendsInfo();
    BattleNet.GetFriendsInfo(ref info);
    this.m_maxFriends = info.maxFriends;
    this.m_maxReceivedInvites = info.maxRecvInvites;
    this.m_maxSentInvites = info.maxSentInvites;
  }

  private void ProcessPendingFriends()
  {
    bool flag = false;
    using (List<BnetPlayer>.Enumerator enumerator = this.m_pendingChangelist.GetFriends().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer current = enumerator.Current;
        if (current.IsDisplayable())
        {
          flag = true;
          this.m_friends.Add(current);
        }
      }
    }
    if (!flag)
      return;
    this.FirePendingFriendsChangedEvent();
  }

  private void OnBnetEventOccurred(BattleNet.BnetEvent bnetEvent, object userData)
  {
    if (bnetEvent != BattleNet.BnetEvent.Disconnected)
      return;
    this.Clear();
  }

  private void OnFriendsUpdate(FriendsUpdate[] updates)
  {
    BnetFriendChangelist changelist = new BnetFriendChangelist();
    foreach (FriendsUpdate update in updates)
    {
      switch ((FriendsUpdate.Action) update.action)
      {
        case FriendsUpdate.Action.FRIEND_ADDED:
          BnetPlayer friend = BnetPresenceMgr.Get().RegisterPlayer(BnetAccountId.CreateFromBnetEntityId(update.entity1));
          if (friend.IsDisplayable())
          {
            this.m_friends.Add(friend);
            changelist.AddAddedFriend(friend);
            break;
          }
          this.AddPendingFriend(friend);
          break;
        case FriendsUpdate.Action.FRIEND_REMOVED:
          BnetPlayer player = BnetPresenceMgr.Get().GetPlayer(BnetAccountId.CreateFromBnetEntityId(update.entity1));
          this.m_friends.Remove(player);
          changelist.AddRemovedFriend(player);
          this.RemovePendingFriend(player);
          break;
        case FriendsUpdate.Action.FRIEND_INVITE:
          BnetInvitation fromFriendsUpdate1 = BnetInvitation.CreateFromFriendsUpdate(update);
          this.m_receivedInvites.Add(fromFriendsUpdate1);
          changelist.AddAddedReceivedInvite(fromFriendsUpdate1);
          break;
        case FriendsUpdate.Action.FRIEND_INVITE_REMOVED:
          BnetInvitation fromFriendsUpdate2 = BnetInvitation.CreateFromFriendsUpdate(update);
          this.m_receivedInvites.Remove(fromFriendsUpdate2);
          changelist.AddRemovedReceivedInvite(fromFriendsUpdate2);
          break;
        case FriendsUpdate.Action.FRIEND_SENT_INVITE:
          BnetInvitation fromFriendsUpdate3 = BnetInvitation.CreateFromFriendsUpdate(update);
          this.m_sentInvites.Add(fromFriendsUpdate3);
          changelist.AddAddedSentInvite(fromFriendsUpdate3);
          break;
        case FriendsUpdate.Action.FRIEND_SENT_INVITE_REMOVED:
          BnetInvitation fromFriendsUpdate4 = BnetInvitation.CreateFromFriendsUpdate(update);
          this.m_sentInvites.Remove(fromFriendsUpdate4);
          changelist.AddRemovedSentInvite(fromFriendsUpdate4);
          break;
      }
    }
    if (changelist.IsEmpty())
      return;
    this.FireChangeEvent(changelist);
  }

  private void OnPendingPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    this.ProcessPendingFriends();
  }

  private bool OnBnetError(BnetErrorInfo info, object userData)
  {
    Log.Mike.Print("BnetFriendMgr.OnBnetError() - event={0} error={1}", new object[2]
    {
      (object) info.GetFeatureEvent(),
      (object) info.GetError()
    });
    return true;
  }

  private void Clear()
  {
    this.m_friends.Clear();
    this.m_receivedInvites.Clear();
    this.m_sentInvites.Clear();
    this.m_pendingChangelist.Clear();
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPendingPlayersChanged));
  }

  private void FireChangeEvent(BnetFriendChangelist changelist)
  {
    foreach (BnetFriendMgr.ChangeListener changeListener in this.m_changeListeners.ToArray())
      changeListener.Fire(changelist);
  }

  private void AddPendingFriend(BnetPlayer friend)
  {
    if (!this.m_pendingChangelist.Add(friend) || this.m_pendingChangelist.GetCount() != 1)
      return;
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPendingPlayersChanged));
  }

  private void RemovePendingFriend(BnetPlayer friend)
  {
    if (!this.m_pendingChangelist.Remove(friend))
      return;
    if (this.m_pendingChangelist.GetCount() == 0)
      BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPendingPlayersChanged));
    else
      this.ProcessPendingFriends();
  }

  private void FirePendingFriendsChangedEvent()
  {
    BnetFriendChangelist changelist = this.m_pendingChangelist.CreateChangelist();
    if (this.m_pendingChangelist.GetCount() == 0)
      BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPendingPlayersChanged));
    this.FireChangeEvent(changelist);
  }

  private class ChangeListener : EventListener<BnetFriendMgr.ChangeCallback>
  {
    public void Fire(BnetFriendChangelist changelist)
    {
      this.m_callback(changelist, this.m_userData);
    }
  }

  public delegate void ChangeCallback(BnetFriendChangelist changelist, object userData);
}
