﻿// Decompiled with JetBrains decompiler
// Type: CollectionPageManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class CollectionPageManager : MonoBehaviour
{
  public static readonly Map<TAG_CLASS, Vector2> s_classTextureOffsets = new Map<TAG_CLASS, Vector2>()
  {
    {
      TAG_CLASS.MAGE,
      new Vector2(0.0f, 0.0f)
    },
    {
      TAG_CLASS.PALADIN,
      new Vector2(0.205f, 0.0f)
    },
    {
      TAG_CLASS.PRIEST,
      new Vector2(0.392f, 0.0f)
    },
    {
      TAG_CLASS.ROGUE,
      new Vector2(0.58f, 0.0f)
    },
    {
      TAG_CLASS.SHAMAN,
      new Vector2(0.774f, 0.0f)
    },
    {
      TAG_CLASS.WARLOCK,
      new Vector2(0.0f, -0.2f)
    },
    {
      TAG_CLASS.WARRIOR,
      new Vector2(0.205f, -0.2f)
    },
    {
      TAG_CLASS.DRUID,
      new Vector2(0.392f, -0.2f)
    },
    {
      TAG_CLASS.HUNTER,
      new Vector2(0.58f, -0.2f)
    },
    {
      TAG_CLASS.NEUTRAL,
      new Vector2(0.774f, -0.2f)
    }
  };
  public static readonly Map<TAG_CLASS, Color> s_classColors = new Map<TAG_CLASS, Color>()
  {
    {
      TAG_CLASS.MAGE,
      new Color(0.1294118f, 0.2666667f, 0.3882353f)
    },
    {
      TAG_CLASS.PALADIN,
      new Color(0.4392157f, 0.2941177f, 0.09019608f)
    },
    {
      TAG_CLASS.PRIEST,
      new Color(0.5215687f, 0.5215687f, 0.5215687f)
    },
    {
      TAG_CLASS.ROGUE,
      new Color(0.09019608f, 0.07450981f, 0.07450981f)
    },
    {
      TAG_CLASS.SHAMAN,
      new Color(0.1294118f, 0.172549f, 0.372549f)
    },
    {
      TAG_CLASS.WARLOCK,
      new Color(0.2117647f, 0.1098039f, 0.282353f)
    },
    {
      TAG_CLASS.WARRIOR,
      new Color(0.2745098f, 0.05098039f, 0.08235294f)
    },
    {
      TAG_CLASS.DRUID,
      new Color(0.2313726f, 0.1607843f, 0.08627451f)
    },
    {
      TAG_CLASS.HUNTER,
      new Color(0.07450981f, 0.2313726f, 0.06666667f)
    },
    {
      TAG_CLASS.NEUTRAL,
      new Color(0.0f, 0.0f, 0.0f)
    }
  };
  public static TAG_CLASS[] CLASS_TAB_ORDER = new TAG_CLASS[10]
  {
    TAG_CLASS.DRUID,
    TAG_CLASS.HUNTER,
    TAG_CLASS.MAGE,
    TAG_CLASS.PALADIN,
    TAG_CLASS.PRIEST,
    TAG_CLASS.ROGUE,
    TAG_CLASS.SHAMAN,
    TAG_CLASS.WARLOCK,
    TAG_CLASS.WARRIOR,
    TAG_CLASS.NEUTRAL
  };
  private static CollectionManagerDisplay.ViewMode[] TAG_ORDERING = new CollectionManagerDisplay.ViewMode[3]
  {
    CollectionManagerDisplay.ViewMode.CARDS,
    CollectionManagerDisplay.ViewMode.CARD_BACKS,
    CollectionManagerDisplay.ViewMode.HERO_SKINS
  };
  public static readonly float SELECT_TAB_ANIM_TIME = 0.2f;
  private static readonly Vector3 CURRENT_PAGE_LOCAL_POS = new Vector3(0.0f, 0.25f, 0.0f);
  private static readonly Vector3 NEXT_PAGE_LOCAL_POS = Vector3.zero;
  private static readonly Vector3 CLASS_TAB_LOCAL_EULERS = new Vector3(0.0f, 180f, 0.0f);
  private static readonly float HIDDEN_TAB_LOCAL_Z_POS = -0.42f;
  private static readonly float ARROW_SCALE_TIME = 0.6f;
  private static readonly string ANIMATE_TABS_COROUTINE_NAME = "AnimateTabs";
  private static readonly string SELECT_TAB_COROUTINE_NAME = "SelectTabWhenReady";
  private static readonly string SHOW_ARROWS_COROUTINE_NAME = "WaitThenShowArrows";
  private static readonly int NUM_PAGE_FLIPS_BEFORE_STOP_SHOWING_ARROWS = 20;
  private static readonly int NUM_PAGE_FLIPS_BEFORE_SET_FILTER_TUTORIAL = 3;
  private static readonly int MASS_DISENCHANT_PAGE_NUM = 1000;
  private static Map<TAG_CLASS, int> CLASS_TO_TAB_IDX = (Map<TAG_CLASS, int>) null;
  private List<CollectionClassTab> m_classTabs = new List<CollectionClassTab>();
  private List<CollectionClassTab> m_allTabs = new List<CollectionClassTab>();
  private Map<CollectionClassTab, bool> m_tabVisibility = new Map<CollectionClassTab, bool>();
  private CollectibleCardClassFilter m_cardsCollection = new CollectibleCardClassFilter();
  private CollectibleCardHeroesFilter m_heroesCollection = new CollectibleCardHeroesFilter();
  private readonly PlatformDependentValue<bool> ANIMATE_PAGE_TRANSITIONS = new PlatformDependentValue<bool>(PlatformCategory.OS)
  {
    iOS = true,
    Android = true,
    PC = true,
    Mac = true
  };
  private readonly PlatformDependentValue<bool> ALWAYS_SHOW_PAGING_ARROWS = new PlatformDependentValue<bool>(PlatformCategory.OS)
  {
    iOS = true,
    Android = true,
    PC = false,
    Mac = false
  };
  public GameObject m_classTabContainer;
  public CollectionClassTab m_classTabPrefab;
  public GameObject m_pageRightArrowBone;
  public GameObject m_pageLeftArrowBone;
  public PegUIElement m_pageRightClickableRegion;
  public PegUIElement m_pageLeftClickableRegion;
  public PegUIElement m_pageDraggableRegion;
  public CollectionPageDisplay m_pageDisplayPrefab;
  public PageTurn m_pageTurn;
  public float m_turnLeftPageSwapTiming;
  public float m_spaceBetweenTabs;
  public CollectionClassTab m_heroSkinsTab;
  public CollectionClassTab m_cardBacksTab;
  public ClassFilterHeaderButton m_classFilterHeader;
  public CollectionClassTab m_deckTemplateTab;
  [CustomEditField(Sections = "Deck Template", T = EditType.GAME_OBJECT)]
  public string m_deckTemplatePickerPrefab;
  private CollectionPageDisplay m_pageA;
  private CollectionPageDisplay m_pageB;
  private bool m_currentPageIsPageA;
  private int m_currentPageNum;
  private int m_lastPageNum;
  private CollectionClassTab m_currentClassTab;
  private bool m_tabsAreAnimating;
  private CollectibleCard m_lastCardAnchor;
  private GameObject m_pageRightArrow;
  private GameObject m_pageLeftArrow;
  private bool m_rightArrowShown;
  private bool m_leftArrowShown;
  private bool m_delayShowingArrows;
  private float m_deselectedClassTabHalfWidth;
  private bool m_initializedTabPositions;
  private bool m_fullyLoaded;
  private MassDisenchant m_massDisenchant;
  private DeckTemplatePicker m_deckTemplatePicker;
  private bool m_wasTouchModeEnabled;
  private bool m_useLastPage;
  private bool m_skipNextPageTurn;
  private bool m_pagesCurrentlyTurning;
  private Vector3 m_heroSkinsTabPos;
  private Vector3 m_cardBacksTabPos;
  private bool m_hideNonDeckTemplateTabs;
  private int m_numPageFlipsThisSession;
  private Coroutine m_turnPageCoroutine;

  private void Awake()
  {
    if (CollectionPageManager.CLASS_TO_TAB_IDX == null)
    {
      CollectionPageManager.CLASS_TO_TAB_IDX = new Map<TAG_CLASS, int>();
      for (int index = 0; index < CollectionPageManager.CLASS_TAB_ORDER.Length; ++index)
        CollectionPageManager.CLASS_TO_TAB_IDX.Add(CollectionPageManager.CLASS_TAB_ORDER[index], index);
    }
    this.m_cardsCollection.Init(CollectionPageManager.CLASS_TAB_ORDER, CollectionPageDisplay.GetMaxCardsPerPage(CollectionManagerDisplay.ViewMode.CARDS));
    this.m_heroesCollection.Init(CollectionPageDisplay.GetMaxCardsPerPage(CollectionManagerDisplay.ViewMode.HERO_SKINS));
    this.UpdateFilteredHeroes();
    this.UpdateFilteredCards();
    if ((bool) ((UnityEngine.Object) this.m_massDisenchant))
      this.m_massDisenchant.Hide();
    this.m_pageLeftClickableRegion.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnPageLeftPressed));
    this.m_pageLeftClickableRegion.SetCursorOver(PegCursor.Mode.LEFTARROW);
    this.m_pageLeftClickableRegion.SetCursorDown(PegCursor.Mode.LEFTARROW);
    this.m_pageRightClickableRegion.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnPageRightPressed));
    this.m_pageRightClickableRegion.SetCursorOver(PegCursor.Mode.RIGHTARROW);
    this.m_pageRightClickableRegion.SetCursorDown(PegCursor.Mode.RIGHTARROW);
    CollectionManagerDisplay.Get().RegisterSwitchViewModeListener(new CollectionManagerDisplay.OnSwitchViewMode(this.OnCollectionManagerViewModeChanged));
    this.m_wasTouchModeEnabled = UniversalInputManager.Get().IsTouchMode();
    if (UniversalInputManager.Get().IsTouchMode())
      this.gameObject.AddComponent<CollectionPageManagerTouchBehavior>();
    this.m_pageA = UnityEngine.Object.Instantiate<CollectionPageDisplay>(this.m_pageDisplayPrefab);
    this.m_pageB = UnityEngine.Object.Instantiate<CollectionPageDisplay>(this.m_pageDisplayPrefab);
    TransformUtil.AttachAndPreserveLocalTransform(this.m_pageA.transform, this.transform);
    TransformUtil.AttachAndPreserveLocalTransform(this.m_pageB.transform, this.transform);
    CollectionManager collectionManager = CollectionManager.Get();
    collectionManager.RegisterFavoriteHeroChangedListener(new CollectionManager.FavoriteHeroChangedCallback(this.OnFavoriteHeroChanged));
    collectionManager.RegisterDefaultCardbackChangedListener(new CollectionManager.DefaultCardbackChangedCallback(this.OnDefaultCardbackChanged));
  }

  private void Start()
  {
    this.SetUpClassTabs();
    CollectionPageDisplay alternatePage = this.GetAlternatePage();
    CollectionPageDisplay currentPage = this.GetCurrentPage();
    this.AssembleEmptyPageUI(alternatePage, false);
    this.AssembleEmptyPageUI(currentPage, false);
    this.PositionNextPage(alternatePage);
    this.PositionCurrentPage(currentPage);
    this.m_fullyLoaded = true;
  }

  private void Update()
  {
    bool receiveReleaseWithoutMouseDown = UniversalInputManager.Get().IsTouchMode();
    if (this.m_wasTouchModeEnabled == receiveReleaseWithoutMouseDown)
      return;
    this.m_wasTouchModeEnabled = receiveReleaseWithoutMouseDown;
    if (receiveReleaseWithoutMouseDown)
      this.gameObject.AddComponent<CollectionPageManagerTouchBehavior>();
    else
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject.GetComponent<CollectionPageManagerTouchBehavior>());
    using (List<CollectionClassTab>.Enumerator enumerator = this.m_allTabs.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetReceiveReleaseWithoutMouseDown(receiveReleaseWithoutMouseDown);
    }
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) CollectionManagerDisplay.Get() != (UnityEngine.Object) null)
      CollectionManagerDisplay.Get().RemoveSwitchViewModeListener(new CollectionManagerDisplay.OnSwitchViewMode(this.OnCollectionManagerViewModeChanged));
    CollectionManager collectionManager = CollectionManager.Get();
    if (collectionManager == null)
      return;
    collectionManager.RemoveFavoriteHeroChangedListener(new CollectionManager.FavoriteHeroChangedCallback(this.OnFavoriteHeroChanged));
    collectionManager.RemoveDefaultCardbackChangedListener(new CollectionManager.DefaultCardbackChangedCallback(this.OnDefaultCardbackChanged));
  }

  public bool HideNonDeckTemplateTabs(bool hide, bool updateTabs = false)
  {
    if (this.m_hideNonDeckTemplateTabs == hide)
      return false;
    this.m_hideNonDeckTemplateTabs = hide;
    if (updateTabs)
      this.UpdateVisibleTabs();
    return true;
  }

  public bool IsNonDeckTemplateTabsHidden()
  {
    return this.m_hideNonDeckTemplateTabs;
  }

  public void OnCollectionLoaded()
  {
    this.ShowOnlyCardsIOwn();
  }

  public void OnBookOpening()
  {
    this.StopCoroutine(CollectionPageManager.SHOW_ARROWS_COROUTINE_NAME);
    this.StartCoroutine(CollectionPageManager.SHOW_ARROWS_COROUTINE_NAME);
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, true, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void SetClassFilter(TAG_CLASS shownClass, bool skipPageTurn, bool updatePages, CollectionPageManager.DelOnPageTransitionComplete callback = null, object callbackData = null)
  {
    this.m_skipNextPageTurn = skipPageTurn;
    this.m_cardsCollection.FilterTheseClasses(shownClass, TAG_CLASS.NEUTRAL);
    this.m_heroesCollection.FilterTheseClasses(shownClass);
    this.m_heroesCollection.FilterOnlyOwned(true);
    this.UpdateFilteredCards();
    this.UpdateFilteredHeroes();
    if (!updatePages)
      return;
    CollectionManagerDisplay.ViewMode viewMode = CollectionManagerDisplay.Get().GetViewMode();
    if (viewMode == CollectionManagerDisplay.ViewMode.HERO_SKINS)
      this.m_currentPageNum = 0;
    if (viewMode == CollectionManagerDisplay.ViewMode.CARDS)
      this.JumpToCollectionClassPage(shownClass, callback, callbackData);
    else
      this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, false, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public bool JumpToPageWithCard(string cardID, TAG_PREMIUM premium)
  {
    return this.JumpToPageWithCard(cardID, premium, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public bool JumpToPageWithCard(string cardID, TAG_PREMIUM premium, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    int collectionPage;
    if (this.m_cardsCollection.GetPageContentsForCard(cardID, premium, out collectionPage).Count == 0 || this.m_currentPageNum == collectionPage)
      return false;
    this.FlipToPage(collectionPage, callback, callbackData);
    return true;
  }

  private void RemoveAllClassFilters()
  {
    this.RemoveAllClassFilters((CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  private void RemoveAllClassFilters(CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    this.m_cardsCollection.FilterTheseClasses((TAG_CLASS[]) null);
    this.m_heroesCollection.FilterTheseClasses((TAG_CLASS[]) null);
    this.m_heroesCollection.FilterOnlyOwned(false);
    this.UpdateFilteredCards();
    this.UpdateFilteredHeroes();
    this.TransitionPageWhenReady(CollectionManagerDisplay.Get().GetViewMode() != CollectionManagerDisplay.ViewMode.CARDS ? CollectionPageManager.PageTransitionType.NONE : CollectionPageManager.PageTransitionType.SINGLE_PAGE_LEFT, false, callback, callbackData);
  }

  public void FilterByManaCost(int cost)
  {
    this.FilterByManaCost(cost, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void FilterByManaCost(int cost, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    if (cost == ManaFilterTab.ALL_TAB_IDX)
      this.m_cardsCollection.FilterManaCost(new int?());
    else
      this.m_cardsCollection.FilterManaCost(new int?(cost));
    this.UpdateFilteredCards();
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, false, callback, callbackData);
  }

  public void FilterByCardSets(List<TAG_CARD_SET> cardSets)
  {
    this.FilterByCardSets(cardSets, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void FilterByCardSets(List<TAG_CARD_SET> cardSets, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    TAG_CARD_SET[] tagCardSetArray = (TAG_CARD_SET[]) null;
    if (cardSets != null && cardSets.Count > 0)
      tagCardSetArray = cardSets.ToArray();
    this.m_cardsCollection.FilterTheseCardSets(tagCardSetArray);
    this.UpdateFilteredCards();
    this.TransitionPageWhenReady(!SceneMgr.Get().IsTransitioning() ? CollectionPageManager.PageTransitionType.SINGLE_PAGE_RIGHT : CollectionPageManager.PageTransitionType.NONE, false, callback, callbackData);
  }

  public bool CardSetFilterIncludesWild()
  {
    return this.m_cardsCollection.CardSetFilterIncludesWild();
  }

  public void ChangeSearchTextFilter(string newSearchText, bool updateVisuals = true)
  {
    this.ChangeSearchTextFilter(newSearchText, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null, updateVisuals);
  }

  public void ChangeSearchTextFilter(string newSearchText, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData, bool updateVisuals = true)
  {
    this.m_cardsCollection.FilterSearchText(newSearchText);
    this.m_heroesCollection.FilterSearchText(newSearchText);
    CardBackManager.Get().SetSearchText(newSearchText);
    this.UpdateFilteredCards();
    this.UpdateFilteredHeroes();
    if (!updateVisuals)
      return;
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, false, callback, callbackData);
  }

  public void RemoveSearchTextFilter()
  {
    this.RemoveSearchTextFilter((CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void RemoveSearchTextFilter(CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    this.m_cardsCollection.FilterSearchText((string) null);
    this.m_heroesCollection.FilterSearchText((string) null);
    CardBackManager.Get().SetSearchText((string) null);
    this.UpdateFilteredCards();
    this.UpdateFilteredHeroes();
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, false, callback, callbackData);
  }

  public void ShowOnlyCardsIOwn()
  {
    this.ShowOnlyCardsIOwn((CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void ShowOnlyCardsIOwn(CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    this.m_cardsCollection.FilterOnlyOwned(true);
    this.m_cardsCollection.FilterByMask((List<CollectibleCardFilter.FilterMask>) null);
    this.m_cardsCollection.FilterOnlyCraftable(false);
    this.UpdateFilteredCards();
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, false, callback, callbackData);
  }

  public void ShowCardsNotOwned(bool includePremiums)
  {
    this.ShowCardsNotOwned(includePremiums, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void ShowCardsNotOwned(bool includePremiums, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    this.m_cardsCollection.FilterOnlyOwned(false);
    this.m_cardsCollection.FilterByMask((List<CollectibleCardFilter.FilterMask>) null);
    this.UpdateFilteredCards();
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, false, callback, callbackData);
  }

  public void ShowPremiumCardsOnly()
  {
    this.ShowPremiumCardsOnly((CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void ShowPremiumCardsOnly(CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    this.m_cardsCollection.FilterOnlyOwned(false);
    this.m_cardsCollection.FilterByMask(TAG_PREMIUM.GOLDEN);
    this.UpdateFilteredCards();
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, false, callback, callbackData);
  }

  public void ShowCraftableCardsOnly(bool showCraftableCardsOnly)
  {
    this.ShowCraftableCardsOnly(showCraftableCardsOnly, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void ShowCraftableCardsOnly(bool showCraftableCardsOnly, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    this.m_cardsCollection.FilterOnlyCraftable(showCraftableCardsOnly);
    this.UpdateFilteredCards();
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, false, callback, callbackData);
  }

  public void ShowCraftingModeCards(bool showCraftableCardsOnly, bool showGolden, bool updatePage = true, bool toggleChanged = false)
  {
    this.ShowCraftingModeCards(showCraftableCardsOnly, showGolden, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null, updatePage, toggleChanged);
  }

  public void ShowCraftingModeCards(bool showCraftableCardsOnly, bool showGolden, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData, bool updatePage = true, bool toggleChanged = false)
  {
    List<CollectibleCardFilter.FilterMask> filterMasks = new List<CollectibleCardFilter.FilterMask>()
    {
      CollectibleCardFilter.FilterMask.PREMIUM_ALL | CollectibleCardFilter.FilterMask.OWNED,
      CollectibleCardFilter.FilterMask.PREMIUM_NORMAL | CollectibleCardFilter.FilterMask.UNOWNED
    };
    if (showGolden)
      filterMasks = new List<CollectibleCardFilter.FilterMask>()
      {
        ~CollectibleCardFilter.FilterMask.PREMIUM_NORMAL
      };
    this.m_cardsCollection.FilterByMask(filterMasks);
    this.m_cardsCollection.FilterOnlyOwned(false);
    this.m_cardsCollection.FilterOnlyCraftable(showCraftableCardsOnly);
    this.UpdateFilteredCards();
    CollectionPageManager.PageTransitionType transitionType = !toggleChanged ? CollectionPageManager.PageTransitionType.NONE : CollectionPageManager.PageTransitionType.MANY_PAGE_LEFT;
    if (toggleChanged)
      this.m_lastCardAnchor = (CollectibleCard) null;
    if (!updatePage)
      return;
    this.TransitionPageWhenReady(transitionType, false, callback, callbackData);
  }

  public void UpdateCurrentPageCardLocks(bool playSound)
  {
    this.GetCurrentPage().UpdateCurrentPageCardLocks(playSound);
  }

  public void UpdateClassTabNewCardCounts()
  {
    using (List<CollectionClassTab>.Enumerator enumerator = this.m_classTabs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionClassTab current = enumerator.Current;
        TAG_CLASS tagClass = current.GetClass();
        int numNewItems = current.m_tabViewMode != CollectionManagerDisplay.ViewMode.DECK_TEMPLATE ? this.GetNumNewCardsForClass(tagClass) : 0;
        current.UpdateNewItemCount(numNewItems);
      }
    }
  }

  public int GetNumNewCardsForClass(TAG_CLASS tagClass)
  {
    return this.m_cardsCollection.GetNumNewCardsForClass(tagClass);
  }

  public void NotifyOfCollectionChanged()
  {
    this.UpdateMassDisenchant();
  }

  public void OnDoneEditingDeck()
  {
    this.RemoveAllClassFilters();
    this.UpdateCraftingModeButtonDustBottleVisibility();
    NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL_TEMPLATE_REPLACE_1"), 0.0f);
    NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL_TEMPLATE_REPLACE_2"), 0.0f);
    NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL_REPLACE_WILD_CARDS"), 0.0f);
    CollectionDeckTray.Get().GetCardsContent().HideDeckHelpPopup();
  }

  public void UpdateCraftingModeButtonDustBottleVisibility()
  {
    CollectionManagerDisplay.Get().m_craftingModeButton.ShowDustBottle(CollectionManagerDisplay.Get().GetViewMode() == CollectionManagerDisplay.ViewMode.CARDS && CollectionManager.Get().GetCardsToDisenchantCount() > 0 && CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing) == null);
  }

  public int GetMassDisenchantAmount()
  {
    return CollectionManager.Get().GetCardsToDisenchantCount();
  }

  public void RefreshCurrentPageContents()
  {
    this.RefreshCurrentPageContents(CollectionPageManager.PageTransitionType.NONE, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void RefreshCurrentPageContents(CollectionPageManager.PageTransitionType transition)
  {
    this.RefreshCurrentPageContents(transition, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void RefreshCurrentPageContents(CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    this.RefreshCurrentPageContents(CollectionPageManager.PageTransitionType.NONE, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void RefreshCurrentPageContents(CollectionPageManager.PageTransitionType transition, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    this.UpdateFilteredCards();
    this.TransitionPageWhenReady(transition, true, callback, callbackData);
  }

  public CollectionCardVisual GetCardVisual(string cardID, TAG_PREMIUM premium)
  {
    return this.GetCurrentPage().GetCardVisual(cardID, premium);
  }

  public bool IsFullyLoaded()
  {
    return this.m_fullyLoaded;
  }

  public void LoadMassDisenchantScreen()
  {
    if ((UnityEngine.Object) this.m_massDisenchant != (UnityEngine.Object) null)
      return;
    this.m_massDisenchant = AssetLoader.Get().LoadUIScreen(!(bool) UniversalInputManager.UsePhoneUI ? "MassDisenchant" : "MassDisenchant_phone", true, false).GetComponent<MassDisenchant>();
    this.m_massDisenchant.Hide();
  }

  public void ShowMassDisenchant()
  {
    this.m_lastPageNum = this.m_currentPageNum;
    this.m_currentPageNum = CollectionPageManager.MASS_DISENCHANT_PAGE_NUM;
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.MANY_PAGE_RIGHT, false, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void HideMassDisenchant()
  {
    bool flag = this.IsShowingMassDisenchant();
    this.m_currentPageNum = this.m_lastPageNum;
    this.m_cardsCollection.FilterOnlyCraftable(false);
    this.m_cardsCollection.FilterByMask((List<CollectibleCardFilter.FilterMask>) null);
    this.m_cardsCollection.FilterOnlyOwned(true);
    this.UpdateFilteredCards();
    this.TransitionPageWhenReady(!flag ? CollectionPageManager.PageTransitionType.NONE : CollectionPageManager.PageTransitionType.MANY_PAGE_LEFT, false, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public bool IsShowingMassDisenchant()
  {
    return this.m_currentPageNum == CollectionPageManager.MASS_DISENCHANT_PAGE_NUM;
  }

  public bool ArePagesTurning()
  {
    return this.m_pagesCurrentlyTurning;
  }

  public int GetNumPagesForClass(TAG_CLASS classTag)
  {
    return this.m_cardsCollection.GetNumPagesForClass(classTag);
  }

  private bool ShouldShowTab(CollectionClassTab tab)
  {
    if (!this.m_initializedTabPositions)
      return true;
    if (this.m_hideNonDeckTemplateTabs)
      return tab.m_tabViewMode == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE;
    if (tab.m_tabViewMode == CollectionManagerDisplay.ViewMode.CARDS)
      return this.m_cardsCollection.GetNumPagesForClass(tab.GetClass()) > 0;
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    bool flag = taggedDeck != null;
    if (tab.m_tabViewMode == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
    {
      if (flag)
        return SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL;
      return false;
    }
    if (flag)
    {
      switch (tab.m_tabViewMode)
      {
        case CollectionManagerDisplay.ViewMode.HERO_SKINS:
          if (CollectionManager.Get().GetBestHeroesIOwn(taggedDeck.GetClass()).Count <= 1)
            return false;
          break;
        case CollectionManagerDisplay.ViewMode.CARD_BACKS:
          if (CardBackManager.Get().GetCardBacksOwned().Count <= 1)
            return false;
          break;
      }
    }
    return true;
  }

  private void SetUpClassTabs()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      return;
    bool receiveReleaseWithoutMouseDown = UniversalInputManager.Get().IsTouchMode();
    if ((UnityEngine.Object) this.m_deckTemplateTab != (UnityEngine.Object) null && this.m_deckTemplateTab.gameObject.activeSelf)
    {
      this.m_allTabs.Add(this.m_deckTemplateTab);
      this.m_classTabs.Add(this.m_deckTemplateTab);
      this.m_deckTemplateTab.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDeckTemplateTabPressed));
      this.m_deckTemplateTab.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTabOver));
      this.m_deckTemplateTab.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnTabOut));
      this.m_deckTemplateTab.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTabOver_Touch));
      this.m_deckTemplateTab.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnTabOut_Touch));
      this.m_deckTemplateTab.SetReceiveReleaseWithoutMouseDown(receiveReleaseWithoutMouseDown);
      this.m_tabVisibility[this.m_deckTemplateTab] = true;
    }
    for (int index1 = 0; index1 < CollectionPageManager.CLASS_TAB_ORDER.Length; ++index1)
    {
      TAG_CLASS classTag = CollectionPageManager.CLASS_TAB_ORDER[index1];
      CollectionClassTab index2 = (CollectionClassTab) GameUtils.Instantiate((Component) this.m_classTabPrefab, this.m_classTabContainer, false);
      index2.Init(classTag);
      index2.transform.localScale = index2.m_DeselectedLocalScale;
      index2.transform.localEulerAngles = CollectionPageManager.CLASS_TAB_LOCAL_EULERS;
      index2.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClassTabPressed));
      index2.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTabOver));
      index2.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnTabOut));
      index2.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTabOver_Touch));
      index2.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnTabOut_Touch));
      index2.SetReceiveReleaseWithoutMouseDown(receiveReleaseWithoutMouseDown);
      index2.gameObject.name = classTag.ToString();
      this.m_allTabs.Add(index2);
      this.m_classTabs.Add(index2);
      this.m_tabVisibility[index2] = true;
      if (index1 <= 0)
        this.m_deselectedClassTabHalfWidth = index2.GetComponent<BoxCollider>().bounds.extents.x;
    }
    if ((UnityEngine.Object) this.m_heroSkinsTab != (UnityEngine.Object) null)
    {
      this.m_heroSkinsTab.Init(TAG_CLASS.NEUTRAL);
      this.m_heroSkinsTab.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnHeroSkinsTabPressed));
      this.m_heroSkinsTab.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTabOver));
      this.m_heroSkinsTab.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnTabOut));
      this.m_heroSkinsTab.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTabOver_Touch));
      this.m_heroSkinsTab.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnTabOut_Touch));
      this.m_heroSkinsTab.SetReceiveReleaseWithoutMouseDown(receiveReleaseWithoutMouseDown);
      this.m_allTabs.Add(this.m_heroSkinsTab);
      this.m_tabVisibility[this.m_heroSkinsTab] = true;
      this.m_heroSkinsTabPos = this.m_heroSkinsTab.transform.localPosition;
    }
    if ((UnityEngine.Object) this.m_cardBacksTab != (UnityEngine.Object) null)
    {
      this.m_cardBacksTab.Init(TAG_CLASS.NEUTRAL);
      this.m_cardBacksTab.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCardBacksTabPressed));
      this.m_cardBacksTab.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTabOver));
      this.m_cardBacksTab.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnTabOut));
      this.m_cardBacksTab.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTabOver_Touch));
      this.m_cardBacksTab.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnTabOut_Touch));
      this.m_cardBacksTab.SetReceiveReleaseWithoutMouseDown(receiveReleaseWithoutMouseDown);
      this.m_allTabs.Add(this.m_cardBacksTab);
      this.m_tabVisibility[this.m_cardBacksTab] = true;
      this.m_cardBacksTabPos = this.m_cardBacksTab.transform.localPosition;
    }
    this.PositionClassTabs(false);
    this.m_initializedTabPositions = true;
  }

  private void PositionClassTabs(bool animate)
  {
    Vector3 position = this.m_classTabContainer.transform.position;
    int length = CollectionPageManager.CLASS_TAB_ORDER.Length;
    if ((UnityEngine.Object) this.m_deckTemplateTab != (UnityEngine.Object) null && this.m_deckTemplateTab.gameObject.activeSelf)
      ++length;
    for (int index = 0; index < length; ++index)
    {
      CollectionClassTab classTab = this.m_classTabs[index];
      Vector3 targetLocalPos;
      if (this.ShouldShowTab(classTab))
      {
        classTab.SetTargetVisibility(true);
        position.x += this.m_spaceBetweenTabs;
        position.x += this.m_deselectedClassTabHalfWidth;
        targetLocalPos = this.m_classTabContainer.transform.InverseTransformPoint(position);
        if ((UnityEngine.Object) classTab == (UnityEngine.Object) this.m_currentClassTab)
          targetLocalPos.y = classTab.m_SelectedLocalYPos;
        position.x += this.m_deselectedClassTabHalfWidth;
      }
      else
      {
        classTab.SetTargetVisibility(false);
        targetLocalPos = classTab.transform.localPosition;
        targetLocalPos.z = CollectionPageManager.HIDDEN_TAB_LOCAL_Z_POS;
      }
      if (animate)
      {
        classTab.SetTargetLocalPosition(targetLocalPos);
      }
      else
      {
        classTab.SetIsVisible(classTab.ShouldBeVisible());
        classTab.transform.localPosition = targetLocalPos;
      }
    }
    this.PositionFixedTab(this.ShouldShowTab(this.m_heroSkinsTab), this.m_heroSkinsTab, this.m_heroSkinsTabPos, animate);
    this.PositionFixedTab(this.ShouldShowTab(this.m_cardBacksTab), this.m_cardBacksTab, this.m_cardBacksTabPos, animate);
    if (!animate)
      return;
    this.StopCoroutine(CollectionPageManager.ANIMATE_TABS_COROUTINE_NAME);
    this.StartCoroutine(CollectionPageManager.ANIMATE_TABS_COROUTINE_NAME);
  }

  private void PositionFixedTab(bool showTab, CollectionClassTab tab, Vector3 originalPos, bool animate)
  {
    if (!showTab)
      originalPos.z -= 0.5f;
    tab.SetTargetVisibility(showTab);
    tab.SetTargetLocalPosition(originalPos);
    if (animate)
    {
      tab.AnimateToTargetPosition(0.4f, iTween.EaseType.easeOutQuad);
    }
    else
    {
      tab.SetIsVisible(tab.ShouldBeVisible());
      tab.transform.localPosition = originalPos;
    }
  }

  [DebuggerHidden]
  private IEnumerator AnimateTabs()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionPageManager.\u003CAnimateTabs\u003Ec__Iterator44()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void DeselectCurrentClassTab()
  {
    if ((UnityEngine.Object) this.m_currentClassTab == (UnityEngine.Object) null)
      return;
    this.m_currentClassTab.SetSelected(false);
    this.m_currentClassTab.SetLargeTab(false);
    this.m_currentClassTab = (CollectionClassTab) null;
  }

  private void SetCurrentClassTab(TAG_CLASS? tabClass)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionPageManager.\u003CSetCurrentClassTab\u003Ec__AnonStorey397 tabCAnonStorey397 = new CollectionPageManager.\u003CSetCurrentClassTab\u003Ec__AnonStorey397();
    // ISSUE: reference to a compiler-generated field
    tabCAnonStorey397.tabClass = tabClass;
    CollectionClassTab collectionClassTab = (CollectionClassTab) null;
    CollectionManagerDisplay.ViewMode viewMode = CollectionManagerDisplay.Get().GetViewMode();
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      // ISSUE: reference to a compiler-generated field
      this.m_classFilterHeader.SetMode(viewMode, tabCAnonStorey397.tabClass);
    }
    else
    {
      switch (viewMode)
      {
        case CollectionManagerDisplay.ViewMode.CARDS:
          // ISSUE: reference to a compiler-generated field
          if (tabCAnonStorey397.tabClass.HasValue)
          {
            // ISSUE: reference to a compiler-generated method
            collectionClassTab = this.m_classTabs.Find(new Predicate<CollectionClassTab>(tabCAnonStorey397.\u003C\u003Em__E5));
            break;
          }
          break;
        case CollectionManagerDisplay.ViewMode.HERO_SKINS:
          collectionClassTab = this.m_heroSkinsTab;
          break;
        case CollectionManagerDisplay.ViewMode.CARD_BACKS:
          collectionClassTab = this.m_cardBacksTab;
          break;
        default:
          collectionClassTab = (CollectionClassTab) null;
          break;
      }
      if ((UnityEngine.Object) collectionClassTab == (UnityEngine.Object) this.m_currentClassTab)
        return;
      this.DeselectCurrentClassTab();
      this.m_currentClassTab = collectionClassTab;
      if (!((UnityEngine.Object) this.m_currentClassTab != (UnityEngine.Object) null))
        return;
      this.StopCoroutine(CollectionPageManager.SELECT_TAB_COROUTINE_NAME);
      this.StartCoroutine(CollectionPageManager.SELECT_TAB_COROUTINE_NAME, (object) this.m_currentClassTab);
    }
  }

  public void SetDeckRuleset(DeckRuleset deckRuleset, bool refresh = false)
  {
    this.m_cardsCollection.SetDeckRuleset(deckRuleset);
    if (!refresh)
      return;
    this.UpdateFilteredCards();
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.NONE, false, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  [DebuggerHidden]
  private IEnumerator SelectTabWhenReady(CollectionClassTab tab)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionPageManager.\u003CSelectTabWhenReady\u003Ec__Iterator45()
    {
      tab = tab,
      \u003C\u0024\u003Etab = tab,
      \u003C\u003Ef__this = this
    };
  }

  private void UpdateVisibleTabs()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      return;
    bool flag1 = false;
    using (List<CollectionClassTab>.Enumerator enumerator = this.m_allTabs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionClassTab current = enumerator.Current;
        bool flag2 = this.m_tabVisibility[current];
        bool flag3 = this.ShouldShowTab(current);
        if (flag2 != flag3)
        {
          flag1 = true;
          this.m_tabVisibility[current] = flag3;
        }
      }
    }
    if (!flag1)
      return;
    this.PositionClassTabs(true);
  }

  private void OnTabOver(UIEvent e)
  {
    CollectionClassTab element = e.GetElement() as CollectionClassTab;
    if ((UnityEngine.Object) element == (UnityEngine.Object) null)
      return;
    element.SetGlowActive(true);
  }

  private void OnTabOut(UIEvent e)
  {
    CollectionClassTab element = e.GetElement() as CollectionClassTab;
    if ((UnityEngine.Object) element == (UnityEngine.Object) null)
      return;
    element.SetGlowActive(false);
  }

  private void OnTabOver_Touch(UIEvent e)
  {
    if (!UniversalInputManager.Get().IsTouchMode())
      return;
    (e.GetElement() as CollectionClassTab).SetLargeTab(true);
  }

  private void OnTabOut_Touch(UIEvent e)
  {
    if (!UniversalInputManager.Get().IsTouchMode())
      return;
    CollectionClassTab element = e.GetElement() as CollectionClassTab;
    if (!((UnityEngine.Object) element != (UnityEngine.Object) this.m_currentClassTab))
      return;
    element.SetLargeTab(false);
  }

  private bool CanUserTurnPages()
  {
    return !this.m_pagesCurrentlyTurning;
  }

  private void OnClassTabPressed(UIEvent e)
  {
    if (!this.CanUserTurnPages())
      return;
    CollectionClassTab element = e.GetElement() as CollectionClassTab;
    if ((UnityEngine.Object) element == (UnityEngine.Object) null || (UnityEngine.Object) element == (UnityEngine.Object) this.m_currentClassTab)
      return;
    this.JumpToCollectionClassPage(element.GetClass());
  }

  private void OnDeckTemplateTabPressed(UIEvent e)
  {
    if (!this.CanUserTurnPages())
      return;
    CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.DECK_TEMPLATE, (CollectionManagerDisplay.ViewModeData) null);
  }

  private void OnHeroSkinsTabPressed(UIEvent e)
  {
    if (!this.CanUserTurnPages())
      return;
    CollectionClassTab element = e.GetElement() as CollectionClassTab;
    if ((UnityEngine.Object) element == (UnityEngine.Object) null || (UnityEngine.Object) element == (UnityEngine.Object) this.m_currentClassTab)
      return;
    CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.HERO_SKINS, (CollectionManagerDisplay.ViewModeData) null);
  }

  private void OnCardBacksTabPressed(UIEvent e)
  {
    if (!this.CanUserTurnPages())
      return;
    CollectionClassTab element = e.GetElement() as CollectionClassTab;
    if ((UnityEngine.Object) element == (UnityEngine.Object) null || (UnityEngine.Object) element == (UnityEngine.Object) this.m_currentClassTab)
      return;
    CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.CARD_BACKS, (CollectionManagerDisplay.ViewModeData) null);
  }

  public void UpdateMassDisenchant()
  {
    if ((UnityEngine.Object) this.m_massDisenchant == (UnityEngine.Object) null)
    {
      this.UpdateCraftingModeButtonDustBottleVisibility();
    }
    else
    {
      this.m_massDisenchant.UpdateContents(CollectionManager.Get().GetMassDisenchantCards());
      if ((UnityEngine.Object) CraftingTray.Get() != (UnityEngine.Object) null)
        CraftingTray.Get().SetMassDisenchantAmount();
      this.UpdateCraftingModeButtonDustBottleVisibility();
    }
  }

  public void JumpToCollectionClassPage(TAG_CLASS pageClass)
  {
    this.JumpToCollectionClassPage(pageClass, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void JumpToCollectionClassPage(TAG_CLASS pageClass, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    CollectionManagerDisplay collectionManagerDisplay = CollectionManagerDisplay.Get();
    if (collectionManagerDisplay.GetViewMode() != CollectionManagerDisplay.ViewMode.CARDS)
    {
      collectionManagerDisplay.SetViewMode(CollectionManagerDisplay.ViewMode.CARDS, new CollectionManagerDisplay.ViewModeData()
      {
        m_setPageByClass = new TAG_CLASS?(pageClass)
      });
    }
    else
    {
      int collectionPage = 0;
      this.m_cardsCollection.GetPageContentsForClass(pageClass, 1, true, out collectionPage);
      this.FlipToPage(collectionPage, callback, callbackData);
    }
  }

  public void FlipToPage(int newCollectionPage, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    int num = newCollectionPage - this.m_currentPageNum;
    bool flag = num < 0;
    if (Math.Abs(num) != 1)
    {
      this.m_currentPageNum = newCollectionPage;
      this.TransitionPageWhenReady(!flag ? CollectionPageManager.PageTransitionType.MANY_PAGE_RIGHT : CollectionPageManager.PageTransitionType.MANY_PAGE_LEFT, true, callback, callbackData);
    }
    else if (flag)
      this.PageLeft(callback, callbackData);
    else
      this.PageRight(callback, callbackData);
  }

  private void SwapCurrentAndAltPages()
  {
    this.m_currentPageIsPageA = !this.m_currentPageIsPageA;
  }

  private CollectionPageDisplay GetCurrentPage()
  {
    if (this.m_currentPageIsPageA)
      return this.m_pageA;
    return this.m_pageB;
  }

  private CollectionPageDisplay GetAlternatePage()
  {
    if (this.m_currentPageIsPageA)
      return this.m_pageB;
    return this.m_pageA;
  }

  private void AssembleEmptyPageUI(CollectionPageDisplay page, bool displayNoMatchesText)
  {
    page.SetClass(new TAG_CLASS?());
    page.ShowNoMatchesFound(displayNoMatchesText);
    if (CollectionManagerDisplay.Get().GetViewMode() == CollectionManagerDisplay.ViewMode.CARDS)
      this.DeselectCurrentClassTab();
    page.SetPageCountText(GameStrings.Get("GLUE_COLLECTION_EMPTY_PAGE"));
    this.ActivateArrows(false, false);
  }

  private bool AssembleBasePage(CollectionPageManager.TransitionReadyCallbackData transitionReadyCallbackData, bool emptyPage, bool wildPage)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionPageManager.\u003CAssembleBasePage\u003Ec__AnonStorey398 pageCAnonStorey398 = new CollectionPageManager.\u003CAssembleBasePage\u003Ec__AnonStorey398();
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey398.transitionReadyCallbackData = transitionReadyCallbackData;
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey398.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey398.page = pageCAnonStorey398.transitionReadyCallbackData.m_assembledPage;
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey398.page.UpdateBasePage();
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey398.page.SetIsWild(wildPage);
    bool flag = CollectionPageManager.MASS_DISENCHANT_PAGE_NUM == this.m_currentPageNum;
    if (flag)
    {
      this.StartCoroutine(this.ShowMassDisenchantPage());
      // ISSUE: reference to a compiler-generated field
      pageCAnonStorey398.page.ActivatePageCountText(false);
    }
    else
    {
      if ((UnityEngine.Object) this.m_massDisenchant != (UnityEngine.Object) null)
        this.m_massDisenchant.Hide();
      // ISSUE: reference to a compiler-generated field
      pageCAnonStorey398.page.ActivatePageCountText(true);
    }
    if (!emptyPage)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.AssembleEmptyPageUI(pageCAnonStorey398.page, !flag);
    // ISSUE: reference to a compiler-generated method
    CollectionManagerDisplay.Get().CollectionPageContentsChanged((List<CollectibleCard>) null, new CollectionManagerDisplay.CollectionActorsReadyCallback(pageCAnonStorey398.\u003C\u003Em__E6), (object) null);
    return true;
  }

  private void AssembleCardPage(CollectionPageManager.TransitionReadyCallbackData transitionReadyCallbackData, List<CollectibleCard> cardsToDisplay, int totalNumPages)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionPageManager.\u003CAssembleCardPage\u003Ec__AnonStorey399 pageCAnonStorey399 = new CollectionPageManager.\u003CAssembleCardPage\u003Ec__AnonStorey399();
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey399.transitionReadyCallbackData = transitionReadyCallbackData;
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey399.\u003C\u003Ef__this = this;
    bool emptyPage = cardsToDisplay == null || cardsToDisplay.Count == 0;
    CollectionManagerDisplay.ViewMode viewMode = CollectionManagerDisplay.Get().GetViewMode();
    bool wildPage = viewMode != CollectionManagerDisplay.ViewMode.HERO_SKINS && CollectionManager.Get().IsShowingWildTheming((CollectionDeck) null);
    // ISSUE: reference to a compiler-generated field
    if (this.AssembleBasePage(pageCAnonStorey399.transitionReadyCallbackData, emptyPage, wildPage))
      return;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey399.page = pageCAnonStorey399.transitionReadyCallbackData.m_assembledPage;
    this.m_lastCardAnchor = cardsToDisplay[0];
    if (viewMode == CollectionManagerDisplay.ViewMode.HERO_SKINS)
    {
      // ISSUE: reference to a compiler-generated field
      pageCAnonStorey399.page.SetHeroSkins();
    }
    else
    {
      EntityDef entityDef = DefLoader.Get().GetEntityDef(cardsToDisplay[0].CardId);
      // ISSUE: reference to a compiler-generated field
      pageCAnonStorey399.page.SetClass(new TAG_CLASS?(entityDef.GetClass()));
    }
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey399.page.SetPageCountText(GameStrings.Format("GLUE_COLLECTION_PAGE_NUM", (object) this.m_currentPageNum));
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey399.page.ShowNoMatchesFound(false);
    this.ActivateArrows(this.m_currentPageNum > 1, this.m_currentPageNum < totalNumPages);
    // ISSUE: reference to a compiler-generated method
    CollectionManagerDisplay.Get().CollectionPageContentsChanged(cardsToDisplay, new CollectionManagerDisplay.CollectionActorsReadyCallback(pageCAnonStorey399.\u003C\u003Em__E7), (object) null);
  }

  private void AssembleDeckTemplatePage(CollectionPageManager.TransitionReadyCallbackData transitionReadyCallbackData)
  {
    bool emptyPage = this.m_currentPageNum == CollectionPageManager.MASS_DISENCHANT_PAGE_NUM;
    if (this.AssembleBasePage(transitionReadyCallbackData, emptyPage, false))
      return;
    CollectionPageDisplay assembledPage = transitionReadyCallbackData.m_assembledPage;
    if ((UnityEngine.Object) this.m_deckTemplatePicker == (UnityEngine.Object) null && !string.IsNullOrEmpty(this.m_deckTemplatePickerPrefab))
    {
      this.m_deckTemplatePicker = GameUtils.LoadGameObjectWithComponent<DeckTemplatePicker>(this.m_deckTemplatePickerPrefab);
      this.m_deckTemplatePicker.RegisterOnTemplateDeckChosen((DeckTemplatePicker.OnTemplateDeckChosen) (() =>
      {
        this.HideNonDeckTemplateTabs(false, true);
        CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.CARDS, (CollectionManagerDisplay.ViewModeData) null);
      }));
    }
    assembledPage.UpdateDeckTemplatePage((Component) this.m_deckTemplatePicker);
    assembledPage.SetDeckTemplates();
    assembledPage.ShowNoMatchesFound(false);
    assembledPage.SetPageCountText(string.Empty);
    this.ActivateArrows(false, false);
    this.UpdateDeckTemplate(this.m_deckTemplatePicker);
    this.TransitionPage((object) transitionReadyCallbackData);
  }

  public DeckTemplatePicker GetDeckTemplatePicker()
  {
    return this.m_deckTemplatePicker;
  }

  public void UpdateDeckTemplate(DeckTemplatePicker deckTemplatePicker)
  {
    if (!((UnityEngine.Object) deckTemplatePicker != (UnityEngine.Object) null))
      return;
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    if (taggedDeck != null)
      deckTemplatePicker.SetDeckClass(taggedDeck.GetClass());
    this.StartCoroutine(deckTemplatePicker.Show(true));
  }

  private void AssembleCardBackPage(CollectionPageManager.TransitionReadyCallbackData transitionReadyCallbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionPageManager.\u003CAssembleCardBackPage\u003Ec__AnonStorey39A pageCAnonStorey39A = new CollectionPageManager.\u003CAssembleCardBackPage\u003Ec__AnonStorey39A();
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey39A.transitionReadyCallbackData = transitionReadyCallbackData;
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey39A.\u003C\u003Ef__this = this;
    bool emptyPage = this.m_currentPageNum == CollectionPageManager.MASS_DISENCHANT_PAGE_NUM;
    // ISSUE: reference to a compiler-generated field
    if (this.AssembleBasePage(pageCAnonStorey39A.transitionReadyCallbackData, emptyPage, false))
      return;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey39A.page = pageCAnonStorey39A.transitionReadyCallbackData.m_assembledPage;
    int count = this.GetCurrentDeckTrayModeCardBackIds().Count;
    int maxCardsPerPage = CollectionPageDisplay.GetMaxCardsPerPage();
    int max = count / maxCardsPerPage + (count % maxCardsPerPage <= 0 ? 0 : 1);
    this.m_currentPageNum = Mathf.Clamp(this.m_currentPageNum, 1, max);
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey39A.page.SetCardBacks();
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey39A.page.ShowNoMatchesFound(false);
    // ISSUE: reference to a compiler-generated field
    pageCAnonStorey39A.page.SetPageCountText(GameStrings.Format("GLUE_COLLECTION_PAGE_NUM", (object) this.m_currentPageNum));
    this.ActivateArrows(this.m_currentPageNum > 1, this.m_currentPageNum < max);
    // ISSUE: reference to a compiler-generated method
    CollectionManagerDisplay.Get().CollectionPageContentsChangedToCardBacks(this.m_currentPageNum, maxCardsPerPage, new CollectionManagerDisplay.CollectionActorsReadyCallback(pageCAnonStorey39A.\u003C\u003Em__E9), (object) null, !CollectionManager.Get().IsInEditMode());
  }

  [DebuggerHidden]
  private IEnumerator ShowMassDisenchantPage()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionPageManager.\u003CShowMassDisenchantPage\u003Ec__Iterator46()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void PositionCurrentPage(CollectionPageDisplay page)
  {
    page.transform.localPosition = CollectionPageManager.CURRENT_PAGE_LOCAL_POS;
  }

  private void PositionNextPage(CollectionPageDisplay page)
  {
    page.transform.localPosition = CollectionPageManager.NEXT_PAGE_LOCAL_POS;
  }

  private void TransitionPageWhenReady(CollectionPageManager.PageTransitionType transitionType, bool useCurrentPageNum, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    if ((UnityEngine.Object) HeroPickerDisplay.Get() != (UnityEngine.Object) null && HeroPickerDisplay.Get().IsShown())
      transitionType = CollectionPageManager.PageTransitionType.NONE;
    this.m_pagesCurrentlyTurning = true;
    this.SwapCurrentAndAltPages();
    CollectionPageManager.TransitionReadyCallbackData transitionReadyCallbackData = new CollectionPageManager.TransitionReadyCallbackData()
    {
      m_assembledPage = this.GetCurrentPage(),
      m_otherPage = this.GetAlternatePage(),
      m_transitionType = transitionType,
      m_callback = callback,
      m_callbackData = callbackData
    };
    switch (transitionType)
    {
      case CollectionPageManager.PageTransitionType.SINGLE_PAGE_RIGHT:
      case CollectionPageManager.PageTransitionType.MANY_PAGE_RIGHT:
        SoundManager.Get().LoadAndPlay("collection_manager_book_page_flip_forward");
        break;
      case CollectionPageManager.PageTransitionType.SINGLE_PAGE_LEFT:
      case CollectionPageManager.PageTransitionType.MANY_PAGE_LEFT:
        SoundManager.Get().LoadAndPlay("collection_manager_book_page_flip_back");
        break;
    }
    switch (CollectionManagerDisplay.Get().GetViewMode())
    {
      case CollectionManagerDisplay.ViewMode.CARD_BACKS:
        if (this.m_currentPageNum < 1)
          this.m_currentPageNum = 1;
        this.AssembleCardBackPage(transitionReadyCallbackData);
        break;
      case CollectionManagerDisplay.ViewMode.DECK_TEMPLATE:
        this.AssembleDeckTemplatePage(transitionReadyCallbackData);
        break;
      case CollectionManagerDisplay.ViewMode.HERO_SKINS:
        List<CollectibleCard> heroesContents = this.m_heroesCollection.GetHeroesContents(this.m_currentPageNum);
        this.AssembleCardPage(transitionReadyCallbackData, heroesContents, this.m_heroesCollection.GetTotalNumPages());
        break;
      case CollectionManagerDisplay.ViewMode.CARDS:
        List<CollectibleCard> cardsToDisplay = (List<CollectibleCard>) null;
        if (useCurrentPageNum)
          cardsToDisplay = this.m_cardsCollection.GetPageContents(this.m_currentPageNum);
        else if (this.m_currentPageNum != CollectionPageManager.MASS_DISENCHANT_PAGE_NUM)
        {
          if (this.m_lastCardAnchor == null)
          {
            this.m_currentPageNum = 1;
            cardsToDisplay = this.m_cardsCollection.GetPageContents(this.m_currentPageNum);
          }
          else
          {
            int collectionPage;
            cardsToDisplay = this.m_cardsCollection.GetPageContentsForCard(this.m_lastCardAnchor.CardId, this.m_lastCardAnchor.PremiumType, out collectionPage);
            if (cardsToDisplay.Count == 0)
              cardsToDisplay = this.m_cardsCollection.GetPageContentsForClass(this.m_lastCardAnchor.Class, 1, true, out collectionPage);
            if (cardsToDisplay.Count == 0)
            {
              cardsToDisplay = this.m_cardsCollection.GetPageContents(1);
              collectionPage = 1;
            }
            this.m_currentPageNum = cardsToDisplay.Count != 0 ? collectionPage : 0;
          }
        }
        if (CollectionPageManager.MASS_DISENCHANT_PAGE_NUM != this.m_currentPageNum && (cardsToDisplay == null || cardsToDisplay.Count == 0))
        {
          int collectionPage;
          cardsToDisplay = this.m_cardsCollection.GetFirstNonEmptyPage(out collectionPage);
          if (cardsToDisplay.Count > 0)
            this.m_currentPageNum = collectionPage;
        }
        this.AssembleCardPage(transitionReadyCallbackData, cardsToDisplay, this.m_cardsCollection.GetTotalNumPages());
        break;
    }
  }

  private void TransitionPage(object callbackData)
  {
    CollectionPageManager.TransitionReadyCallbackData readyCallbackData = callbackData as CollectionPageManager.TransitionReadyCallbackData;
    CollectionPageDisplay assembledPage = readyCallbackData.m_assembledPage;
    CollectionPageDisplay otherPage = readyCallbackData.m_otherPage;
    this.m_pageTurn.SetBackPageMaterial(assembledPage.m_basePageRenderer.material);
    if ((bool) this.ANIMATE_PAGE_TRANSITIONS)
    {
      CollectionPageManager.PageTransitionType pageTransitionType = readyCallbackData.m_transitionType;
      if (TavernBrawlDisplay.IsTavernBrawlViewing())
        pageTransitionType = CollectionPageManager.PageTransitionType.NONE;
      if (this.m_skipNextPageTurn)
      {
        pageTransitionType = CollectionPageManager.PageTransitionType.NONE;
        this.m_skipNextPageTurn = false;
      }
      switch (pageTransitionType)
      {
        case CollectionPageManager.PageTransitionType.NONE:
          this.PositionNextPage(otherPage);
          this.PositionCurrentPage(assembledPage);
          this.OnPageTurnComplete((object) readyCallbackData);
          break;
        case CollectionPageManager.PageTransitionType.SINGLE_PAGE_RIGHT:
        case CollectionPageManager.PageTransitionType.MANY_PAGE_RIGHT:
          this.m_pageTurn.TurnRight(otherPage.gameObject, assembledPage.gameObject, new PageTurn.DelOnPageTurnComplete(this.OnPageTurnComplete), (object) readyCallbackData);
          this.PositionCurrentPage(assembledPage);
          this.PositionNextPage(otherPage);
          break;
        case CollectionPageManager.PageTransitionType.SINGLE_PAGE_LEFT:
        case CollectionPageManager.PageTransitionType.MANY_PAGE_LEFT:
          this.m_pageTurn.TurnLeft(assembledPage.gameObject, otherPage.gameObject, new PageTurn.DelOnPageTurnComplete(this.OnPageTurnComplete), (object) readyCallbackData);
          break;
      }
    }
    this.UpdateVisibleTabs();
    if (CollectionPageManager.MASS_DISENCHANT_PAGE_NUM == this.m_currentPageNum)
      this.DeselectCurrentClassTab();
    else
      this.SetCurrentClassTab(assembledPage.GetFirstCardClass());
    if ((bool) this.ANIMATE_PAGE_TRANSITIONS)
      return;
    this.PositionNextPage(otherPage);
    this.PositionCurrentPage(assembledPage);
    this.OnPageTurnComplete((object) readyCallbackData);
  }

  private void OnPageTurnComplete(object callbackData)
  {
    Resources.UnloadUnusedAssets();
    CollectionPageManager.TransitionReadyCallbackData readyCallbackData = callbackData as CollectionPageManager.TransitionReadyCallbackData;
    CollectionPageDisplay assembledPage = readyCallbackData.m_assembledPage;
    CollectionPageDisplay otherPage = readyCallbackData.m_otherPage;
    switch (readyCallbackData.m_transitionType)
    {
      case CollectionPageManager.PageTransitionType.SINGLE_PAGE_LEFT:
      case CollectionPageManager.PageTransitionType.MANY_PAGE_LEFT:
        this.PositionCurrentPage(assembledPage);
        this.PositionNextPage(otherPage);
        break;
    }
    if ((UnityEngine.Object) otherPage != (UnityEngine.Object) this.GetCurrentPage())
    {
      otherPage.transform.position = new Vector3(-300f, 0.0f, -300f);
      assembledPage.Show();
      otherPage.Hide();
    }
    if (readyCallbackData.m_callback != null)
      readyCallbackData.m_callback(readyCallbackData.m_callbackData);
    if (!((UnityEngine.Object) readyCallbackData.m_assembledPage == (UnityEngine.Object) this.GetCurrentPage()))
      return;
    this.m_pagesCurrentlyTurning = false;
  }

  private void UpdateFilteredHeroes()
  {
    this.m_heroesCollection.UpdateResults();
  }

  private void UpdateFilteredCards()
  {
    this.m_cardsCollection.UpdateResults();
    this.UpdateClassTabNewCardCounts();
  }

  private void PageRight(CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    ++this.m_currentPageNum;
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.SINGLE_PAGE_RIGHT, true, callback, callbackData);
  }

  private void PageLeft(CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    --this.m_currentPageNum;
    this.TransitionPageWhenReady(CollectionPageManager.PageTransitionType.SINGLE_PAGE_LEFT, true, callback, callbackData);
  }

  private void ActivateArrows(bool leftArrow, bool rightArrow)
  {
    this.m_pageLeftClickableRegion.enabled = leftArrow;
    this.m_pageLeftClickableRegion.SetEnabled(leftArrow);
    this.m_pageRightClickableRegion.enabled = rightArrow;
    this.m_pageRightClickableRegion.SetEnabled(rightArrow);
    this.ShowArrow(this.m_pageLeftArrow, leftArrow, false);
    this.ShowArrow(this.m_pageRightArrow, rightArrow, true);
  }

  private void OnPageLeftPressed(UIEvent e)
  {
    if (!this.CanUserTurnPages())
      return;
    this.OnPageFlip();
    this.PageLeft((CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  private void OnPageFlip()
  {
    int num = Options.Get().GetInt(Option.PAGE_MOUSE_OVERS);
    int val = num + 1;
    if (num < CollectionPageManager.NUM_PAGE_FLIPS_BEFORE_STOP_SHOWING_ARROWS)
      Options.Get().SetInt(Option.PAGE_MOUSE_OVERS, val);
    this.ShowSetFilterTutorialIfNeeded();
  }

  private void ShowSetFilterTutorialIfNeeded()
  {
    if (Options.Get().GetBool(Option.HAS_SEEN_SET_FILTER_TUTORIAL) || CollectionManager.Get().IsInEditMode() || (CollectionManagerDisplay.Get().GetViewMode() != CollectionManagerDisplay.ViewMode.CARDS || !this.m_cardsCollection.CardSetFilterIsAllStandardSets()) || (CollectionManagerDisplay.Get().IsShowingSetFilterTray() || !CollectionManager.Get().AccountHasWildCards()))
      return;
    ++this.m_numPageFlipsThisSession;
    if (this.m_numPageFlipsThisSession < CollectionPageManager.NUM_PAGE_FLIPS_BEFORE_SET_FILTER_TUTORIAL)
      return;
    CollectionManagerDisplay.Get().ShowSetFilterTutorial(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS);
    Options.Get().SetBool(Option.HAS_SEEN_SET_FILTER_TUTORIAL, true);
    this.m_numPageFlipsThisSession = 0;
  }

  private void OnPageRightPressed(UIEvent e)
  {
    if (!this.CanUserTurnPages())
      return;
    this.OnPageFlip();
    this.PageRight((CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
  }

  public void LoadPagingArrows()
  {
    if (Options.Get().GetInt(Option.PAGE_MOUSE_OVERS) >= CollectionPageManager.NUM_PAGE_FLIPS_BEFORE_STOP_SHOWING_ARROWS & !(bool) this.ALWAYS_SHOW_PAGING_ARROWS || (bool) ((UnityEngine.Object) this.m_pageLeftArrow) && (bool) ((UnityEngine.Object) this.m_pageRightArrow))
      return;
    AssetLoader.Get().LoadGameObject("PagingArrow", new AssetLoader.GameObjectCallback(this.OnPagingArrowLoaded), (object) null, false);
  }

  private void OnPagingArrowLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return;
    if (!(bool) ((UnityEngine.Object) this.m_pageLeftArrow))
    {
      this.m_pageLeftArrow = go;
      this.m_pageLeftArrow.transform.parent = this.transform;
      this.m_pageLeftArrow.transform.localEulerAngles = new Vector3(0.0f, 180f, 0.0f);
      this.m_pageLeftArrow.transform.position = this.m_pageLeftArrowBone.transform.position;
      this.m_pageLeftArrow.transform.localScale = Vector3.zero;
      bool enabled = this.m_pageLeftClickableRegion.enabled;
      SceneUtils.SetLayer(this.m_pageLeftArrow, GameLayer.TransparentFX);
      this.ShowArrow(this.m_pageLeftArrow, enabled, false);
    }
    if ((bool) ((UnityEngine.Object) this.m_pageRightArrow))
      return;
    this.m_pageRightArrow = UnityEngine.Object.Instantiate<GameObject>(this.m_pageLeftArrow);
    this.m_pageRightArrow.transform.parent = this.transform;
    this.m_pageRightArrow.transform.localEulerAngles = Vector3.zero;
    this.m_pageRightArrow.transform.position = this.m_pageRightArrowBone.transform.position;
    this.m_pageRightArrow.transform.localScale = Vector3.zero;
    bool enabled1 = this.m_pageRightClickableRegion.enabled;
    SceneUtils.SetLayer(this.m_pageRightArrow, GameLayer.TransparentFX);
    this.ShowArrow(this.m_pageRightArrow, enabled1, true);
  }

  [DebuggerHidden]
  private IEnumerator WaitThenShowArrows()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionPageManager.\u003CWaitThenShowArrows\u003Ec__Iterator47()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ShowArrow(GameObject arrow, bool show, bool isRightArrow)
  {
    if ((UnityEngine.Object) arrow == (UnityEngine.Object) null || this.m_delayShowingArrows)
      return;
    if (isRightArrow)
    {
      if (this.m_rightArrowShown == show)
        return;
      this.m_rightArrowShown = show;
    }
    else
    {
      if (this.m_leftArrowShown == show)
        return;
      this.m_leftArrowShown = show;
    }
    Vector3 localScale = (!isRightArrow ? this.m_pageLeftArrowBone : this.m_pageRightArrowBone).transform.localScale;
    Vector3 vector3 = !show ? Vector3.zero : localScale;
    iTween.EaseType easeType = !show ? iTween.EaseType.linear : iTween.EaseType.easeOutElastic;
    Hashtable args = iTween.Hash((object) "scale", (object) vector3, (object) "time", (object) CollectionPageManager.ARROW_SCALE_TIME, (object) "easetype", (object) easeType, (object) "name", (object) "ArrowScale");
    iTween.StopByName(arrow, "ArrowScale");
    iTween.ScaleTo(arrow, args);
  }

  private void OnCollectionManagerViewModeChanged(CollectionManagerDisplay.ViewMode prevMode, CollectionManagerDisplay.ViewMode mode, CollectionManagerDisplay.ViewModeData userdata, bool triggerResponse)
  {
    if (!triggerResponse)
      return;
    this.UpdateCraftingModeButtonDustBottleVisibility();
    if (mode == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
      this.HideNonDeckTemplateTabs(true, false);
    if (mode != CollectionManagerDisplay.ViewMode.CARDS)
      CollectionDeckTray.Get().GetCardsContent().HideDeckHelpPopup();
    if (this.m_useLastPage)
    {
      this.m_useLastPage = false;
      this.m_currentPageNum = this.GetLastPageInCurrentMode();
    }
    else
    {
      this.m_currentPageNum = 1;
      if (userdata != null)
      {
        if (userdata.m_setPageByClass.HasValue)
          this.m_cardsCollection.GetPageContentsForClass(userdata.m_setPageByClass.Value, 1, true, out this.m_currentPageNum);
        else if (userdata.m_setPageByCard != null)
          this.m_cardsCollection.GetPageContentsForCard(userdata.m_setPageByCard, userdata.m_setPageByPremium, out this.m_currentPageNum);
      }
    }
    int num1 = 0;
    int num2 = 0;
    for (int index = 0; index < CollectionPageManager.TAG_ORDERING.Length; ++index)
    {
      if (prevMode == CollectionPageManager.TAG_ORDERING[index])
        num1 = index;
      if (mode == CollectionPageManager.TAG_ORDERING[index])
        num2 = index;
    }
    CollectionPageManager.PageTransitionType transition = num2 - num1 >= 0 ? CollectionPageManager.PageTransitionType.SINGLE_PAGE_RIGHT : CollectionPageManager.PageTransitionType.SINGLE_PAGE_LEFT;
    CollectionPageManager.DelOnPageTransitionComplete callback = (CollectionPageManager.DelOnPageTransitionComplete) null;
    object callbackData = (object) null;
    if (userdata != null)
    {
      callback = userdata.m_pageTransitionCompleteCallback;
      callbackData = userdata.m_pageTransitionCompleteData;
    }
    if (this.m_turnPageCoroutine != null)
      this.StopCoroutine(this.m_turnPageCoroutine);
    CollectionDeckTray.Get().m_decksContent.UpdateDeckName((string) null);
    CollectionDeckTray.Get().UpdateDoneButtonText();
    this.m_turnPageCoroutine = this.StartCoroutine(this.ViewModeChangedWaitToTurnPage(transition, prevMode == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE, callback, callbackData));
  }

  [DebuggerHidden]
  private IEnumerator ViewModeChangedWaitToTurnPage(CollectionPageManager.PageTransitionType transition, bool hideDeckTemplateBottomPanel, CollectionPageManager.DelOnPageTransitionComplete callback, object callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionPageManager.\u003CViewModeChangedWaitToTurnPage\u003Ec__Iterator48()
    {
      hideDeckTemplateBottomPanel = hideDeckTemplateBottomPanel,
      transition = transition,
      callback = callback,
      callbackData = callbackData,
      \u003C\u0024\u003EhideDeckTemplateBottomPanel = hideDeckTemplateBottomPanel,
      \u003C\u0024\u003Etransition = transition,
      \u003C\u0024\u003Ecallback = callback,
      \u003C\u0024\u003EcallbackData = callbackData,
      \u003C\u003Ef__this = this
    };
  }

  public void OnFavoriteHeroChanged(TAG_CLASS heroClass, NetCache.CardDefinition favoriteHero, object userData)
  {
    this.GetCurrentPage().UpdateFavoriteHeroSkins(CollectionManagerDisplay.Get().GetViewMode(), this.IsShowingMassDisenchant());
  }

  public void OnDefaultCardbackChanged(int newDefaultCardBackID, object userData)
  {
    this.GetCurrentPage().UpdateFavoriteCardBack(CollectionManagerDisplay.Get().GetViewMode());
  }

  private int GetLastPageInCurrentMode()
  {
    switch (CollectionManagerDisplay.Get().GetViewMode())
    {
      case CollectionManagerDisplay.ViewMode.CARD_BACKS:
        int count = this.GetCurrentDeckTrayModeCardBackIds().Count;
        int maxCardsPerPage = CollectionPageDisplay.GetMaxCardsPerPage();
        return count / maxCardsPerPage + (count % maxCardsPerPage <= 0 ? 0 : 1);
      default:
        return this.m_cardsCollection.GetTotalNumPages();
    }
  }

  private HashSet<int> GetCurrentDeckTrayModeCardBackIds()
  {
    return CardBackManager.Get().GetCardBackIds(!CollectionManager.Get().IsInEditMode());
  }

  private enum ArrowClickType
  {
    DISABLED,
    ENABLED,
    SWITCH_MODE,
  }

  public enum PageTransitionType
  {
    NONE,
    SINGLE_PAGE_RIGHT,
    SINGLE_PAGE_LEFT,
    MANY_PAGE_RIGHT,
    MANY_PAGE_LEFT,
  }

  private class TransitionReadyCallbackData
  {
    public CollectionPageDisplay m_assembledPage;
    public CollectionPageDisplay m_otherPage;
    public CollectionPageManager.PageTransitionType m_transitionType;
    public CollectionPageManager.DelOnPageTransitionComplete m_callback;
    public object m_callbackData;
  }

  public delegate void DelOnPageTransitionComplete(object callbackData);
}
