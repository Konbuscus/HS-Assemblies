﻿// Decompiled with JetBrains decompiler
// Type: CardTextBuilder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class CardTextBuilder
{
  protected bool m_useEntityForTextInPlay;

  public CardTextBuilder()
  {
    this.m_useEntityForTextInPlay = false;
  }

  public static string GetRawCardTextInHand(string cardId)
  {
    CardDbfRecord cardRecord = GameDbf.GetIndex().GetCardRecord(cardId);
    if (cardRecord != null && cardRecord.TextInHand != null)
      return (string) cardRecord.TextInHand;
    return string.Empty;
  }

  public static string GetDefaultCardTextInHand(EntityDef entityDef)
  {
    return TextUtils.TransformCardText(CardTextBuilder.GetRawCardTextInHand(entityDef.GetCardId()));
  }

  public virtual string BuildCardTextInHand(Entity entity)
  {
    if (entity.IsEnchantment() && entity.HasTag(GAME_TAG.COPY_DEATHRATTLE))
    {
      EntityDef entityDef = DefLoader.Get().GetEntityDef(entity.GetTag(GAME_TAG.COPY_DEATHRATTLE));
      if (entityDef != null)
      {
        string powersText = GameStrings.Format("GAMEPLAY_COPY_DEATHRATTLE", (object) entityDef.GetName());
        return TextUtils.TransformCardText(entity.GetDamageBonus(), entity.GetDamageBonusDouble(), entity.GetHealingDouble(), powersText);
      }
    }
    return TextUtils.TransformCardText(entity, CardTextBuilder.GetRawCardTextInHand(entity.GetCardId()));
  }

  public virtual string BuildCardTextInHand(EntityDef entityDef)
  {
    return CardTextBuilder.GetDefaultCardTextInHand(entityDef);
  }

  public virtual string BuildCardTextInHistory(Entity entity)
  {
    CardTextHistoryData cardTextHistoryData = entity.GetCardTextHistoryData();
    if (cardTextHistoryData == null)
    {
      Log.All.Print("CardTextBuilder.BuildCardTextInHistory: entity {0} does not have a CardTextHistoryData object.", (object) entity.GetEntityId());
      return string.Empty;
    }
    string rawCardTextInHand = CardTextBuilder.GetRawCardTextInHand(entity.GetCardId());
    return TextUtils.TransformCardText(cardTextHistoryData.m_damageBonus, cardTextHistoryData.m_damageBonusDouble, cardTextHistoryData.m_healingDouble, rawCardTextInHand);
  }

  public virtual CardTextHistoryData CreateCardTextHistoryData()
  {
    return new CardTextHistoryData();
  }

  public bool ShouldUseEntityForTextInPlay()
  {
    return this.m_useEntityForTextInPlay;
  }
}
