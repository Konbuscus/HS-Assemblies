﻿// Decompiled with JetBrains decompiler
// Type: ConstructCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class ConstructCard : MonoBehaviour
{
  private readonly Vector3 IMPACT_CAMERA_SHAKE_AMOUNT = new Vector3(0.35f, 0.35f, 0.35f);
  private readonly float IMPACT_CAMERA_SHAKE_TIME = 0.25f;
  public float m_ImpactRotationTime = 0.5f;
  public float m_RandomDelayVariance = 0.2f;
  public float m_AnimationRarityScaleCommon = 1f;
  public float m_AnimationRarityScaleRare = 0.9f;
  public float m_AnimationRarityScaleEpic = 0.8f;
  public float m_AnimationRarityScaleLegendary = 0.7f;
  public float m_ManaGemAnimTime = 1f;
  public Vector3 m_ManaGemImpactRotation = new Vector3(20f, 0.0f, 20f);
  public float m_DescriptionAnimTime = 1f;
  public Vector3 m_DescriptionImpactRotation = new Vector3(-15f, 0.0f, 0.0f);
  public float m_AttackAnimTime = 1f;
  public Vector3 m_AttackImpactRotation = new Vector3(-15f, 0.0f, 0.0f);
  public float m_HealthAnimTime = 1f;
  public Vector3 m_HealthImpactRotation = new Vector3(-15f, 0.0f, 0.0f);
  public float m_PortraitAnimTime = 1f;
  public Vector3 m_PortraitImpactRotation = new Vector3(-15f, 0.0f, 0.0f);
  public float m_NameAnimTime = 1f;
  public Vector3 m_NameImpactRotation = new Vector3(-15f, 0.0f, 0.0f);
  public float m_RarityAnimTime = 1f;
  public Vector3 m_RarityImpactRotation = new Vector3(-15f, 0.0f, 0.0f);
  private float m_AnimationScale = 1f;
  public Material m_GhostMaterial;
  public Material m_GhostMaterialTransparent;
  public GameObject m_GhostGlow;
  public Texture m_GhostTextureUnique;
  public GameObject m_FuseGlow;
  public ParticleSystem m_RarityBurstCommon;
  public ParticleSystem m_RarityBurstRare;
  public ParticleSystem m_RarityBurstEpic;
  public ParticleSystem m_RarityBurstLegendary;
  public Transform m_ManaGemStartPosition;
  public Transform m_ManaGemTargetPosition;
  public float m_ManaGemStartDelay;
  public GameObject m_ManaGemGlow;
  public ParticleSystem m_ManaGemHitBlastParticle;
  public Transform m_DescriptionStartPosition;
  public Transform m_DescriptionTargetPosition;
  public float m_DescriptionStartDelay;
  public GameObject m_DescriptionGlow;
  public ParticleSystem m_DescriptionHitBlastParticle;
  public Transform m_AttackStartPosition;
  public Transform m_AttackTargetPosition;
  public float m_AttackStartDelay;
  public GameObject m_AttackGlow;
  public ParticleSystem m_AttackHitBlastParticle;
  public Transform m_HealthStartPosition;
  public Transform m_HealthTargetPosition;
  public float m_HealthStartDelay;
  public GameObject m_HealthGlow;
  public ParticleSystem m_HealthHitBlastParticle;
  public Transform m_PortraitStartPosition;
  public Transform m_PortraitTargetPosition;
  public float m_PortraitStartDelay;
  public GameObject m_PortraitGlow;
  public GameObject m_PortraitGlowStandard;
  public GameObject m_PortraitGlowUnique;
  public ParticleSystem m_PortraitHitBlastParticle;
  public Transform m_NameStartPosition;
  public Transform m_NameTargetPosition;
  public float m_NameStartDelay;
  public GameObject m_NameGlow;
  public ParticleSystem m_NameHitBlastParticle;
  public Transform m_RarityStartPosition;
  public Transform m_RarityTargetPosition;
  public float m_RarityStartDelay;
  public GameObject m_RarityGlowCommon;
  public GameObject m_RarityGlowRare;
  public GameObject m_RarityGlowEpic;
  public GameObject m_RarityGlowLegendary;
  public ParticleSystem m_RarityHitBlastParticle;
  private Actor m_Actor;
  private Spell m_GhostSpell;
  private bool isInit;
  private GameObject m_ManaGemInstance;
  private GameObject m_DescriptionInstance;
  private GameObject m_AttackInstance;
  private GameObject m_HealthInstance;
  private GameObject m_PortraitInstance;
  private GameObject m_NameInstance;
  private GameObject m_RarityInstance;
  private GameObject m_CardMesh;
  private int m_CardFrontIdx;
  private GameObject m_PortraitMesh;
  private int m_PortraitFrameIdx;
  private GameObject m_NameMesh;
  private GameObject m_DescriptionMesh;
  private GameObject m_DescriptionTrimMesh;
  private GameObject m_RarityGemMesh;
  private GameObject m_RarityFrameMesh;
  private GameObject m_ManaCostMesh;
  private GameObject m_AttackMesh;
  private GameObject m_HealthMesh;
  private GameObject m_RacePlateMesh;
  private GameObject m_EliteMesh;
  private GameObject m_ManaGemClone;
  private Material m_OrgMat_CardFront;
  private Material m_OrgMat_PortraitFrame;
  private Material m_OrgMat_Name;
  private Material m_OrgMat_Description;
  private Material m_OrgMat_Description2;
  private Material m_OrgMat_DescriptionTrim;
  private Material m_OrgMat_RarityFrame;
  private Material m_OrgMat_ManaCost;
  private Material m_OrgMat_Attack;
  private Material m_OrgMat_Health;
  private Material m_OrgMat_RacePlate;
  private Material m_OrgMat_Elite;

  private void OnDisable()
  {
    this.Cancel();
  }

  public void Construct()
  {
    this.StartCoroutine(this.DoConstruct());
  }

  [DebuggerHidden]
  private IEnumerator DoConstruct()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CDoConstruct\u003Ec__Iterator326() { \u003C\u003Ef__this = this };
  }

  private void Init()
  {
    if (this.isInit)
      return;
    this.m_Actor = SceneUtils.FindComponentInThisOrParents<Actor>(this.gameObject);
    if ((Object) this.m_Actor == (Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0} Ghost card effect failed to find Actor!", (object) this.transform.root.name));
      this.enabled = false;
    }
    else
    {
      this.m_CardMesh = this.m_Actor.m_cardMesh;
      this.m_CardFrontIdx = this.m_Actor.m_cardFrontMatIdx;
      this.m_PortraitMesh = this.m_Actor.m_portraitMesh;
      this.m_PortraitFrameIdx = this.m_Actor.m_portraitFrameMatIdx;
      this.m_NameMesh = this.m_Actor.m_nameBannerMesh;
      this.m_DescriptionMesh = this.m_Actor.m_descriptionMesh;
      this.m_DescriptionTrimMesh = this.m_Actor.m_descriptionTrimMesh;
      this.m_RarityGemMesh = this.m_Actor.m_rarityGemMesh;
      this.m_RarityFrameMesh = this.m_Actor.m_rarityFrameMesh;
      if ((bool) ((Object) this.m_Actor.m_attackObject))
      {
        Renderer component = this.m_Actor.m_attackObject.GetComponent<Renderer>();
        if ((Object) component != (Object) null)
          this.m_AttackMesh = component.gameObject;
        if ((Object) this.m_AttackMesh == (Object) null)
        {
          foreach (Renderer componentsInChild in this.m_Actor.m_attackObject.GetComponentsInChildren<Renderer>())
          {
            if (!(bool) ((Object) componentsInChild.GetComponent<UberText>()))
              this.m_AttackMesh = componentsInChild.gameObject;
          }
        }
      }
      if ((bool) ((Object) this.m_Actor.m_healthObject))
      {
        Renderer component = this.m_Actor.m_healthObject.GetComponent<Renderer>();
        if ((Object) component != (Object) null)
          this.m_HealthMesh = component.gameObject;
        if ((Object) this.m_HealthMesh == (Object) null)
        {
          foreach (Renderer componentsInChild in this.m_Actor.m_healthObject.GetComponentsInChildren<Renderer>())
          {
            if (!(bool) ((Object) componentsInChild.GetComponent<UberText>()))
              this.m_HealthMesh = componentsInChild.gameObject;
          }
        }
      }
      this.m_ManaCostMesh = this.m_Actor.m_manaObject;
      this.m_RacePlateMesh = this.m_Actor.m_racePlateObject;
      this.m_EliteMesh = this.m_Actor.m_eliteObject;
      this.StoreOrgMaterials();
      switch (this.m_Actor.GetRarity())
      {
        case TAG_RARITY.RARE:
          this.m_AnimationScale = this.m_AnimationRarityScaleRare;
          break;
        case TAG_RARITY.EPIC:
          this.m_AnimationScale = this.m_AnimationRarityScaleEpic;
          break;
        case TAG_RARITY.LEGENDARY:
          this.m_AnimationScale = this.m_AnimationRarityScaleLegendary;
          break;
        default:
          this.m_AnimationScale = this.m_AnimationRarityScaleCommon;
          break;
      }
      this.isInit = true;
    }
  }

  private void Cancel()
  {
    this.StopAllCoroutines();
    this.RestoreOrgMaterials();
    this.DisableManaGem();
    this.DisableDescription();
    this.DisableAttack();
    this.DisableHealth();
    this.DisablePortrait();
    this.DisableName();
    this.DisableRarity();
    this.DestroyInstances();
    this.StopAllParticles();
    this.HideAllMeshObjects();
    if ((bool) ((Object) this.m_Actor))
      this.m_Actor.ShowAllText();
    if (!((Object) this.m_Actor != (Object) null))
      return;
    iTween.StopByName(this.m_Actor.gameObject, "CardConstructImpactRotation");
  }

  private void StopAllParticles()
  {
    foreach (ParticleSystem componentsInChild in this.GetComponentsInChildren<ParticleSystem>())
    {
      if (componentsInChild.isPlaying)
        componentsInChild.Stop();
    }
  }

  private void HideAllMeshObjects()
  {
    foreach (Component componentsInChild in this.GetComponentsInChildren<MeshRenderer>())
      componentsInChild.GetComponent<Renderer>().enabled = false;
  }

  private void CreateInstances()
  {
    Vector3 vector3 = new Vector3(0.0f, -5000f, 0.0f);
    if ((bool) ((Object) this.m_RarityGemMesh))
      this.m_RarityGemMesh.GetComponent<Renderer>().enabled = false;
    if ((bool) ((Object) this.m_RarityFrameMesh))
      this.m_RarityFrameMesh.GetComponent<Renderer>().enabled = false;
    if ((bool) ((Object) this.m_ManaGemStartPosition))
    {
      this.m_ManaGemInstance = Object.Instantiate<GameObject>(this.m_ManaCostMesh);
      this.m_ManaGemInstance.transform.parent = this.transform.parent;
      this.m_ManaGemInstance.transform.position = vector3;
    }
    if ((bool) ((Object) this.m_DescriptionStartPosition))
    {
      this.m_DescriptionInstance = Object.Instantiate<GameObject>(this.m_DescriptionMesh);
      this.m_DescriptionInstance.transform.parent = this.transform.parent;
      this.m_DescriptionInstance.transform.position = vector3;
    }
    if ((bool) ((Object) this.m_AttackStartPosition))
    {
      this.m_AttackInstance = Object.Instantiate<GameObject>(this.m_AttackMesh);
      this.m_AttackInstance.transform.parent = this.transform.parent;
      this.m_AttackInstance.transform.position = vector3;
    }
    if ((bool) ((Object) this.m_HealthStartPosition))
    {
      this.m_HealthInstance = Object.Instantiate<GameObject>(this.m_HealthMesh);
      this.m_HealthInstance.transform.parent = this.transform.parent;
      this.m_HealthInstance.transform.position = vector3;
    }
    if ((bool) ((Object) this.m_PortraitStartPosition))
    {
      this.m_PortraitInstance = Object.Instantiate<GameObject>(this.m_PortraitMesh);
      this.m_PortraitInstance.transform.parent = this.transform.parent;
      this.m_PortraitInstance.transform.position = vector3;
    }
    if ((bool) ((Object) this.m_NameStartPosition))
    {
      this.m_NameInstance = Object.Instantiate<GameObject>(this.m_NameMesh);
      this.m_NameInstance.transform.parent = this.transform.parent;
      this.m_NameInstance.transform.position = vector3;
    }
    if (!(bool) ((Object) this.m_RarityStartPosition))
      return;
    this.m_RarityInstance = Object.Instantiate<GameObject>(this.m_RarityGemMesh);
    this.m_RarityInstance.transform.parent = this.transform.parent;
    this.m_RarityInstance.transform.position = vector3;
  }

  private void DestroyInstances()
  {
    if ((bool) ((Object) this.m_ManaGemInstance))
      Object.Destroy((Object) this.m_ManaGemInstance);
    if ((bool) ((Object) this.m_DescriptionInstance))
      Object.Destroy((Object) this.m_DescriptionInstance);
    if ((bool) ((Object) this.m_AttackInstance))
      Object.Destroy((Object) this.m_AttackInstance);
    if ((bool) ((Object) this.m_HealthInstance))
      Object.Destroy((Object) this.m_HealthInstance);
    if ((bool) ((Object) this.m_PortraitInstance))
      Object.Destroy((Object) this.m_PortraitInstance);
    if ((bool) ((Object) this.m_NameInstance))
      Object.Destroy((Object) this.m_NameInstance);
    if (!(bool) ((Object) this.m_RarityInstance))
      return;
    Object.Destroy((Object) this.m_RarityInstance);
  }

  private void AnimateManaGem()
  {
    GameObject manaGemInstance = this.m_ManaGemInstance;
    manaGemInstance.transform.parent = (Transform) null;
    manaGemInstance.transform.localScale = this.m_ManaCostMesh.transform.lossyScale;
    manaGemInstance.transform.position = this.m_ManaGemStartPosition.transform.position;
    manaGemInstance.transform.parent = this.transform.parent;
    manaGemInstance.GetComponent<Renderer>().material = this.m_OrgMat_ManaCost;
    float num = Random.Range(this.m_ManaGemStartDelay - this.m_ManaGemStartDelay * this.m_RandomDelayVariance, this.m_ManaGemStartDelay + this.m_ManaGemStartDelay * this.m_RandomDelayVariance);
    this.StartCoroutine("AnimateObject", (object) new ConstructCard.AnimationData()
    {
      Name = "ManaGem",
      AnimateTransform = manaGemInstance.transform,
      StartTransform = this.m_ManaGemStartPosition.transform,
      TargetTransform = this.m_ManaGemTargetPosition.transform,
      HitBlastParticle = this.m_ManaGemHitBlastParticle,
      AnimationTime = this.m_ManaGemAnimTime,
      StartDelay = num,
      GlowObject = this.m_ManaGemGlow,
      ImpactRotation = this.m_ManaGemImpactRotation,
      OnComplete = "ManaGemOnComplete"
    });
  }

  [DebuggerHidden]
  private IEnumerator ManaGemOnComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CManaGemOnComplete\u003Ec__Iterator327() { \u003C\u003Ef__this = this };
  }

  private void DisableManaGem()
  {
    if (!(bool) ((Object) this.m_ManaGemGlow))
      return;
    foreach (ParticleSystem componentsInChild in this.m_ManaGemGlow.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
  }

  private void AnimateDescription()
  {
    GameObject descriptionInstance = this.m_DescriptionInstance;
    descriptionInstance.transform.parent = (Transform) null;
    descriptionInstance.transform.localScale = this.m_DescriptionMesh.transform.lossyScale;
    descriptionInstance.transform.position = this.m_DescriptionStartPosition.transform.position;
    descriptionInstance.transform.parent = this.transform.parent;
    descriptionInstance.GetComponent<Renderer>().material = this.m_OrgMat_Description;
    float num = Random.Range(this.m_DescriptionStartDelay - this.m_DescriptionStartDelay * this.m_RandomDelayVariance, this.m_DescriptionStartDelay + this.m_DescriptionStartDelay * this.m_RandomDelayVariance);
    this.StartCoroutine("AnimateObject", (object) new ConstructCard.AnimationData()
    {
      Name = "Description",
      AnimateTransform = descriptionInstance.transform,
      StartTransform = this.m_DescriptionStartPosition.transform,
      TargetTransform = this.m_DescriptionTargetPosition.transform,
      HitBlastParticle = this.m_DescriptionHitBlastParticle,
      AnimationTime = this.m_DescriptionAnimTime,
      StartDelay = num,
      GlowObject = this.m_DescriptionGlow,
      ImpactRotation = this.m_DescriptionImpactRotation,
      OnComplete = "DescriptionOnComplete"
    });
  }

  [DebuggerHidden]
  private IEnumerator DescriptionOnComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CDescriptionOnComplete\u003Ec__Iterator328() { \u003C\u003Ef__this = this };
  }

  private void DisableDescription()
  {
    if (!(bool) ((Object) this.m_DescriptionGlow))
      return;
    foreach (ParticleSystem componentsInChild in this.m_DescriptionGlow.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
  }

  private void AnimateAttack()
  {
    GameObject attackInstance = this.m_AttackInstance;
    attackInstance.transform.parent = (Transform) null;
    attackInstance.transform.localScale = this.m_AttackMesh.transform.lossyScale;
    attackInstance.transform.position = this.m_AttackStartPosition.transform.position;
    attackInstance.transform.parent = this.transform.parent;
    attackInstance.GetComponent<Renderer>().material = this.m_OrgMat_Attack;
    float num = Random.Range(this.m_AttackStartDelay - this.m_AttackStartDelay * this.m_RandomDelayVariance, this.m_AttackStartDelay + this.m_AttackStartDelay * this.m_RandomDelayVariance);
    this.StartCoroutine("AnimateObject", (object) new ConstructCard.AnimationData()
    {
      Name = "Attack",
      AnimateTransform = attackInstance.transform,
      StartTransform = this.m_AttackStartPosition.transform,
      TargetTransform = this.m_AttackTargetPosition.transform,
      HitBlastParticle = this.m_AttackHitBlastParticle,
      AnimationTime = this.m_AttackAnimTime,
      StartDelay = num,
      GlowObject = this.m_AttackGlow,
      ImpactRotation = this.m_AttackImpactRotation,
      OnComplete = "AttackOnComplete"
    });
  }

  [DebuggerHidden]
  private IEnumerator AttackOnComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CAttackOnComplete\u003Ec__Iterator329() { \u003C\u003Ef__this = this };
  }

  private void DisableAttack()
  {
    if (!(bool) ((Object) this.m_AttackGlow))
      return;
    foreach (ParticleSystem componentsInChild in this.m_AttackGlow.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
  }

  private void AnimateHealth()
  {
    GameObject healthInstance = this.m_HealthInstance;
    healthInstance.transform.parent = (Transform) null;
    healthInstance.transform.localScale = this.m_HealthMesh.transform.lossyScale;
    healthInstance.transform.position = this.m_HealthStartPosition.transform.position;
    healthInstance.transform.parent = this.transform.parent;
    healthInstance.GetComponent<Renderer>().material = this.m_OrgMat_Health;
    float num = Random.Range(this.m_HealthStartDelay - this.m_HealthStartDelay * this.m_RandomDelayVariance, this.m_HealthStartDelay + this.m_HealthStartDelay * this.m_RandomDelayVariance);
    this.StartCoroutine("AnimateObject", (object) new ConstructCard.AnimationData()
    {
      Name = "Health",
      AnimateTransform = healthInstance.transform,
      StartTransform = this.m_HealthStartPosition.transform,
      TargetTransform = this.m_HealthTargetPosition.transform,
      HitBlastParticle = this.m_HealthHitBlastParticle,
      AnimationTime = this.m_HealthAnimTime,
      StartDelay = num,
      GlowObject = this.m_HealthGlow,
      ImpactRotation = this.m_HealthImpactRotation,
      OnComplete = "HealthOnComplete"
    });
  }

  [DebuggerHidden]
  private IEnumerator HealthOnComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CHealthOnComplete\u003Ec__Iterator32A() { \u003C\u003Ef__this = this };
  }

  private void DisableHealth()
  {
    if (!(bool) ((Object) this.m_HealthGlow))
      return;
    foreach (ParticleSystem componentsInChild in this.m_HealthGlow.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
  }

  private void AnimatePortrait()
  {
    GameObject portraitInstance = this.m_PortraitInstance;
    portraitInstance.transform.parent = (Transform) null;
    portraitInstance.transform.localScale = this.m_PortraitMesh.transform.lossyScale;
    portraitInstance.transform.position = this.m_PortraitStartPosition.transform.position;
    portraitInstance.transform.parent = this.transform.parent;
    float num = Random.Range(this.m_PortraitStartDelay - this.m_PortraitStartDelay * this.m_RandomDelayVariance, this.m_PortraitStartDelay + this.m_PortraitStartDelay * this.m_RandomDelayVariance);
    this.StartCoroutine("AnimateObject", (object) new ConstructCard.AnimationData()
    {
      Name = "Portrait",
      AnimateTransform = portraitInstance.transform,
      StartTransform = this.m_PortraitStartPosition.transform,
      TargetTransform = this.m_PortraitTargetPosition.transform,
      HitBlastParticle = this.m_PortraitHitBlastParticle,
      AnimationTime = this.m_PortraitAnimTime,
      StartDelay = num,
      GlowObject = this.m_PortraitGlow,
      GlowObjectStandard = this.m_PortraitGlowStandard,
      GlowObjectUnique = this.m_PortraitGlowUnique,
      ImpactRotation = this.m_PortraitImpactRotation,
      OnComplete = "PortraitOnComplete"
    });
  }

  [DebuggerHidden]
  private IEnumerator PortraitOnComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CPortraitOnComplete\u003Ec__Iterator32B() { \u003C\u003Ef__this = this };
  }

  private void DisablePortrait()
  {
    if (!(bool) ((Object) this.m_PortraitGlow))
      return;
    foreach (ParticleSystem componentsInChild in this.m_PortraitGlow.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
  }

  private void AnimateName()
  {
    GameObject nameInstance = this.m_NameInstance;
    nameInstance.transform.parent = (Transform) null;
    nameInstance.transform.localScale = this.m_NameMesh.transform.lossyScale;
    nameInstance.transform.position = this.m_NameStartPosition.transform.position;
    nameInstance.transform.parent = this.transform.parent;
    nameInstance.GetComponent<Renderer>().material = this.m_OrgMat_Name;
    float num = Random.Range(this.m_NameStartDelay - this.m_NameStartDelay * this.m_RandomDelayVariance, this.m_NameStartDelay + this.m_NameStartDelay * this.m_RandomDelayVariance);
    this.StartCoroutine("AnimateObject", (object) new ConstructCard.AnimationData()
    {
      Name = "Name",
      AnimateTransform = nameInstance.transform,
      StartTransform = this.m_NameStartPosition.transform,
      TargetTransform = this.m_NameTargetPosition.transform,
      HitBlastParticle = this.m_NameHitBlastParticle,
      AnimationTime = this.m_NameAnimTime,
      StartDelay = num,
      GlowObject = this.m_NameGlow,
      ImpactRotation = this.m_NameImpactRotation,
      OnComplete = "NameOnComplete"
    });
  }

  [DebuggerHidden]
  private IEnumerator NameOnComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CNameOnComplete\u003Ec__Iterator32C() { \u003C\u003Ef__this = this };
  }

  private void DisableName()
  {
    if (!(bool) ((Object) this.m_NameGlow))
      return;
    foreach (ParticleSystem componentsInChild in this.m_NameGlow.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
  }

  private void AnimateRarity()
  {
    if (this.m_Actor.GetCardSet() == TAG_CARD_SET.CORE)
      return;
    GameObject rarityInstance = this.m_RarityInstance;
    rarityInstance.transform.parent = (Transform) null;
    rarityInstance.transform.localScale = this.m_RarityGemMesh.transform.lossyScale;
    rarityInstance.transform.position = this.m_RarityStartPosition.transform.position;
    rarityInstance.transform.parent = this.transform.parent;
    this.m_RarityInstance.GetComponent<Renderer>().enabled = true;
    GameObject gameObject = this.m_RarityGlowCommon;
    switch (this.m_Actor.GetRarity())
    {
      case TAG_RARITY.RARE:
        gameObject = this.m_RarityGlowRare;
        break;
      case TAG_RARITY.EPIC:
        gameObject = this.m_RarityGlowEpic;
        break;
      case TAG_RARITY.LEGENDARY:
        gameObject = this.m_RarityGlowLegendary;
        break;
    }
    float num = Random.Range(this.m_RarityStartDelay - this.m_RarityStartDelay * this.m_RandomDelayVariance, this.m_RarityStartDelay + this.m_RarityStartDelay * this.m_RandomDelayVariance);
    this.StartCoroutine("AnimateObject", (object) new ConstructCard.AnimationData()
    {
      Name = "Rarity",
      AnimateTransform = rarityInstance.transform,
      StartTransform = this.m_RarityStartPosition.transform,
      TargetTransform = this.m_RarityTargetPosition.transform,
      HitBlastParticle = this.m_RarityHitBlastParticle,
      AnimationTime = this.m_RarityAnimTime,
      StartDelay = num,
      GlowObject = gameObject,
      ImpactRotation = this.m_RarityImpactRotation,
      OnComplete = "RarityOnComplete"
    });
  }

  [DebuggerHidden]
  private IEnumerator RarityOnComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CRarityOnComplete\u003Ec__Iterator32D() { \u003C\u003Ef__this = this };
  }

  private void DisableRarity()
  {
    if (!(bool) ((Object) this.m_RarityGlowCommon))
      return;
    foreach (ParticleSystem componentsInChild in this.m_RarityGlowCommon.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
  }

  [DebuggerHidden]
  private IEnumerator EndAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CEndAnimation\u003Ec__Iterator32E() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator AnimateObject(ConstructCard.AnimationData animData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ConstructCard.\u003CAnimateObject\u003Ec__Iterator32F() { animData = animData, \u003C\u0024\u003EanimData = animData, \u003C\u003Ef__this = this };
  }

  private void StoreOrgMaterials()
  {
    if ((bool) ((Object) this.m_CardMesh))
      this.m_OrgMat_CardFront = this.m_CardMesh.GetComponent<Renderer>().materials[this.m_CardFrontIdx];
    if ((bool) ((Object) this.m_PortraitMesh))
      this.m_OrgMat_PortraitFrame = this.m_PortraitMesh.GetComponent<Renderer>().sharedMaterials[this.m_PortraitFrameIdx];
    if ((bool) ((Object) this.m_NameMesh))
      this.m_OrgMat_Name = this.m_NameMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_ManaCostMesh))
      this.m_OrgMat_ManaCost = this.m_ManaCostMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_AttackMesh))
      this.m_OrgMat_Attack = this.m_AttackMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_HealthMesh))
      this.m_OrgMat_Health = this.m_HealthMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_RacePlateMesh))
      this.m_OrgMat_RacePlate = this.m_RacePlateMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_RarityFrameMesh))
      this.m_OrgMat_RarityFrame = this.m_RarityFrameMesh.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_DescriptionMesh))
    {
      Material[] materials = this.m_DescriptionMesh.GetComponent<Renderer>().materials;
      if (materials.Length > 1)
      {
        this.m_OrgMat_Description = materials[0];
        this.m_OrgMat_Description2 = materials[1];
      }
      else
        this.m_OrgMat_Description = this.m_DescriptionMesh.GetComponent<Renderer>().material;
    }
    if ((bool) ((Object) this.m_DescriptionTrimMesh))
      this.m_OrgMat_DescriptionTrim = this.m_DescriptionTrimMesh.GetComponent<Renderer>().material;
    if (!(bool) ((Object) this.m_EliteMesh))
      return;
    this.m_OrgMat_Elite = this.m_EliteMesh.GetComponent<Renderer>().material;
  }

  private void RestoreOrgMaterials()
  {
    this.ApplyMaterialByIdx(this.m_CardMesh, this.m_OrgMat_CardFront, this.m_CardFrontIdx);
    this.ApplySharedMaterialByIdx(this.m_PortraitMesh, this.m_OrgMat_PortraitFrame, this.m_PortraitFrameIdx);
    this.ApplyMaterialByIdx(this.m_DescriptionMesh, this.m_OrgMat_Description, 0);
    this.ApplyMaterialByIdx(this.m_DescriptionMesh, this.m_OrgMat_Description2, 1);
    this.ApplyMaterial(this.m_NameMesh, this.m_OrgMat_Name);
    this.ApplyMaterial(this.m_ManaCostMesh, this.m_OrgMat_ManaCost);
    this.ApplyMaterial(this.m_AttackMesh, this.m_OrgMat_Attack);
    this.ApplyMaterial(this.m_HealthMesh, this.m_OrgMat_Health);
    this.ApplyMaterial(this.m_RacePlateMesh, this.m_OrgMat_RacePlate);
    this.ApplyMaterial(this.m_RarityFrameMesh, this.m_OrgMat_RarityFrame);
    this.ApplyMaterial(this.m_DescriptionTrimMesh, this.m_OrgMat_DescriptionTrim);
    this.ApplyMaterial(this.m_EliteMesh, this.m_OrgMat_Elite);
  }

  private void ApplyGhostMaterials()
  {
    this.ApplyMaterialByIdx(this.m_CardMesh, this.m_GhostMaterial, this.m_CardFrontIdx);
    this.ApplySharedMaterialByIdx(this.m_PortraitMesh, this.m_GhostMaterial, this.m_PortraitFrameIdx);
    this.ApplyMaterialByIdx(this.m_DescriptionMesh, this.m_GhostMaterial, 0);
    this.ApplyMaterialByIdx(this.m_DescriptionMesh, this.m_GhostMaterial, 1);
    this.ApplyMaterial(this.m_NameMesh, this.m_GhostMaterial);
    this.ApplyMaterial(this.m_ManaCostMesh, this.m_GhostMaterial);
    this.ApplyMaterial(this.m_AttackMesh, this.m_GhostMaterial);
    this.ApplyMaterial(this.m_HealthMesh, this.m_GhostMaterial);
    this.ApplyMaterial(this.m_RacePlateMesh, this.m_GhostMaterial);
    this.ApplyMaterial(this.m_RarityFrameMesh, this.m_GhostMaterial);
    if ((bool) ((Object) this.m_GhostMaterialTransparent))
      this.ApplyMaterial(this.m_DescriptionTrimMesh, this.m_GhostMaterialTransparent);
    this.ApplyMaterial(this.m_EliteMesh, this.m_GhostMaterial);
  }

  private void ApplyMaterial(GameObject go, Material mat)
  {
    if ((Object) go == (Object) null)
      return;
    Texture mainTexture = go.GetComponent<Renderer>().material.mainTexture;
    go.GetComponent<Renderer>().material = mat;
    go.GetComponent<Renderer>().material.mainTexture = mainTexture;
  }

  private void ApplyMaterialByIdx(GameObject go, Material mat, int idx)
  {
    if ((Object) go == (Object) null || (Object) mat == (Object) null || idx < 0)
      return;
    Material[] materials = go.GetComponent<Renderer>().materials;
    if (idx >= materials.Length)
      return;
    Texture mainTexture = go.GetComponent<Renderer>().materials[idx].mainTexture;
    materials[idx] = mat;
    go.GetComponent<Renderer>().materials = materials;
    go.GetComponent<Renderer>().materials[idx].mainTexture = mainTexture;
  }

  private void ApplySharedMaterialByIdx(GameObject go, Material mat, int idx)
  {
    if ((Object) go == (Object) null || (Object) mat == (Object) null || idx < 0)
      return;
    Material[] sharedMaterials = go.GetComponent<Renderer>().sharedMaterials;
    if (idx >= sharedMaterials.Length)
      return;
    Texture mainTexture = go.GetComponent<Renderer>().sharedMaterials[idx].mainTexture;
    sharedMaterials[idx] = mat;
    go.GetComponent<Renderer>().sharedMaterials = sharedMaterials;
    go.GetComponent<Renderer>().sharedMaterials[idx].mainTexture = mainTexture;
  }

  private class AnimationData
  {
    public float AnimationTime = 1f;
    public string OnComplete = string.Empty;
    public string Name;
    public Transform AnimateTransform;
    public Transform StartTransform;
    public Transform TargetTransform;
    public float StartDelay;
    public GameObject GlowObject;
    public GameObject GlowObjectStandard;
    public GameObject GlowObjectUnique;
    public ParticleSystem HitBlastParticle;
    public Vector3 ImpactRotation;
  }
}
