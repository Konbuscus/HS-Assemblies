﻿// Decompiled with JetBrains decompiler
// Type: FeignDeath
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusGame;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class FeignDeath : SuperSpell
{
  public float m_Height = 1f;
  public GameObject m_RootObject;
  public GameObject m_Glow;

  protected override void Awake()
  {
    base.Awake();
    this.m_RootObject.SetActive(false);
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    if (!this.m_taskList.IsStartOfBlock())
    {
      base.OnAction(prevStateType);
    }
    else
    {
      ++this.m_effectsPendingFinish;
      base.OnAction(prevStateType);
      this.m_targets.Clear();
      for (PowerTaskList powerTaskList = this.m_taskList; powerTaskList != null; powerTaskList = powerTaskList.GetNext())
      {
        using (List<PowerTask>.Enumerator enumerator1 = powerTaskList.GetTaskList().GetEnumerator())
        {
          while (enumerator1.MoveNext())
          {
            Network.HistMetaData power = enumerator1.Current.GetPower() as Network.HistMetaData;
            if (power != null && power.MetaType == HistoryMeta.Type.TARGET)
            {
              using (List<int>.Enumerator enumerator2 = power.Info.GetEnumerator())
              {
                while (enumerator2.MoveNext())
                  this.m_targets.Add(GameState.Get().GetEntity(enumerator2.Current).GetCard().gameObject);
              }
            }
          }
        }
      }
      this.StartCoroutine(this.ActionVisual());
    }
  }

  [DebuggerHidden]
  private IEnumerator ActionVisual()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FeignDeath.\u003CActionVisual\u003Ec__Iterator2B8() { \u003C\u003Ef__this = this };
  }
}
