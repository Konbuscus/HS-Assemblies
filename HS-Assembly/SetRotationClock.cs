﻿// Decompiled with JetBrains decompiler
// Type: SetRotationClock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class SetRotationClock : MonoBehaviour
{
  public float m_AnimationWaitTime = 5.5f;
  public float m_CenterPanelFlipTime = 1f;
  public float m_SetRotationButtonDelay = 0.75f;
  public float m_SetRotationButtonWobbleTime = 0.5f;
  public float m_ButtonRotationHoldTime = 1.5f;
  public Color m_ButtonBannerTextColor = Color.white;
  public float m_ButtonRiseTime = 1.75f;
  public float m_BlurScreenDelay = 0.5f;
  public float m_BlurScreenTime = 1f;
  public float m_MoveButtonUpZ = -0.1f;
  public float m_MoveButtonUpZphone = -0.3f;
  public float m_MoveButtonUpTime = 1f;
  public float m_ButtonFlipTime = 0.5f;
  public float m_ButtonToTrayAnimTime = 0.5f;
  public float m_EndBlurScreenDelay = 0.5f;
  public float m_EndBlurScreenTime = 1f;
  public float m_MoveButtonToTrayDelay = 1.5f;
  public float m_TextDelayTime = 1f;
  public float m_TheClockAmbientSoundVolume = 1f;
  public float m_TheClockAmbientSoundFadeInTime = 2f;
  public float m_TheClockAmbientSoundFadeOutTime = 1f;
  public GameObject m_CenterPanel;
  public GameObject m_SetRotationButton;
  public GameObject m_SetRotationButtonMesh;
  public GameObject m_ButtonRiseBone;
  public GameObject m_ButtonBanner;
  public UberText m_ButtonBannerStandard;
  public UberText m_ButtonBannerWild;
  public ClockOverlayText m_overlayText;
  public GameObject m_ButtonGlowPlaneYellow;
  public GameObject m_ButtonGlowPlaneGreen;
  public ParticleSystem m_ImpactParticles;
  public AnimationCurve m_ButtonGlowAnimation;
  public PegUIElement m_clickCatcher;
  public AudioSource m_TheClockAmbientSound;
  public AudioSource m_ClickSound;
  public AudioSource m_Stage1Sound;
  public AudioSource m_Stage2Sound;
  public AudioSource m_Stage3Sound;
  public AudioSource m_Stage4Sound;
  public AudioSource m_Stage5Sound;
  private bool m_clickCaptured;
  private Vector3 m_buttonBannerScale;
  private AudioSource m_ambientSound;
  private static SetRotationClock s_instance;

  private void Awake()
  {
    SetRotationClock.s_instance = this;
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.transform.position = new Vector3(-60.7f, -18.939f, -43f);
      this.transform.localScale = new Vector3(9.043651f, 9.043651f, 9.043651f);
    }
    else
    {
      this.transform.position = new Vector3(-47.234f, -18.939f, -31.837f);
      this.transform.localScale = new Vector3(6.970411f, 6.970411f, 6.970411f);
    }
    this.m_overlayText.HideImmediate();
    this.m_clickCatcher.gameObject.SetActive(false);
    this.m_clickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClick));
    this.m_buttonBannerScale = this.m_ButtonBanner.transform.localScale;
    this.m_ButtonBannerStandard.TextColor = this.m_ButtonBannerTextColor;
    this.m_ButtonBannerWild.TextColor = this.m_ButtonBannerTextColor;
    this.m_ButtonBanner.SetActive(false);
    this.m_ButtonBannerStandard.gameObject.SetActive(false);
    this.m_ButtonBannerWild.gameObject.SetActive(false);
  }

  public static SetRotationClock Get()
  {
    return SetRotationClock.s_instance;
  }

  public void StartTheClock()
  {
    this.m_SetRotationButton.SetActive(true);
    this.StartCoroutine(this.ClockAnimation());
  }

  public void ShakeCamera()
  {
    CameraShakeMgr.Shake(Camera.main, new Vector3(0.1f, 0.1f, 0.1f), 0.4f);
  }

  [DebuggerHidden]
  public IEnumerator ClockAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SetRotationClock.\u003CClockAnimation\u003Ec__Iterator245() { \u003C\u003Ef__this = this };
  }

  private void FadeInAmbientSound()
  {
    if ((UnityEngine.Object) this.m_TheClockAmbientSound == (UnityEngine.Object) null)
      return;
    this.m_ambientSound = UnityEngine.Object.Instantiate<AudioSource>(this.m_TheClockAmbientSound);
    SoundManager.Get().SetVolume(this.m_ambientSound, 0.01f);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "name", (object) "TheClockAmbientSound", (object) "from", (object) 0.01f, (object) "to", (object) this.m_TheClockAmbientSoundVolume, (object) "time", (object) this.m_TheClockAmbientSoundFadeInTime, (object) "easetype", (object) iTween.EaseType.linear, (object) "onupdate", (object) (Action<object>) (amount =>
    {
      SoundManager.Get().SetVolume(this.m_ambientSound, (float) amount);
      Log.Kyle.Print("ambient vol: {0}, {1}", new object[2]
      {
        (object) this.m_ambientSound.volume,
        (object) (float) amount
      });
    }), (object) "onupdatetarget", (object) this.gameObject));
    SoundManager.Get().Play(this.m_ambientSound, true);
  }

  private void FadeOutAmbientSound()
  {
    if ((UnityEngine.Object) this.m_ambientSound == (UnityEngine.Object) null)
      return;
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "name", (object) "TheClockAmbientSound", (object) "from", (object) this.m_TheClockAmbientSoundVolume, (object) "to", (object) 0.0f, (object) "time", (object) this.m_TheClockAmbientSoundFadeOutTime, (object) "easetype", (object) iTween.EaseType.linear, (object) "onupdate", (object) (Action<object>) (amount => SoundManager.Get().SetVolume(this.m_ambientSound, (float) amount)), (object) "onupdatetarget", (object) this.gameObject, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "StopAmbientSound"));
  }

  private void StopAmbientSound()
  {
    if ((UnityEngine.Object) this.m_ambientSound == (UnityEngine.Object) null)
      return;
    SoundManager.Get().Stop(this.m_ambientSound);
  }

  private void PlayClockAnimation()
  {
    Animator component = this.GetComponent<Animator>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    component.SetTrigger("StartClock");
  }

  private void AnimateButtonToTournamentTray()
  {
    TournamentDisplay.Get().SetRotationSlideIn();
  }

  private void FlipCenterPanelButton()
  {
    iTween.RotateTo(this.m_CenterPanel, iTween.Hash((object) "z", (object) 180f, (object) "time", (object) this.m_CenterPanelFlipTime, (object) "islocal", (object) true, (object) "easetype", (object) iTween.EaseType.easeOutBounce));
    this.m_SetRotationButton.transform.localEulerAngles = new Vector3(0.0f, 0.0f, -10f);
    iTween.RotateTo(this.m_SetRotationButton, iTween.Hash((object) "z", (object) 0.0f, (object) "delay", (object) this.m_SetRotationButtonDelay, (object) "time", (object) this.m_SetRotationButtonWobbleTime, (object) "islocal", (object) true, (object) "easetype", (object) iTween.EaseType.easeOutBounce));
  }

  private void RaiseButton()
  {
    this.GetComponent<Animator>().SetTrigger("RaiseButton");
    SceneUtils.SetLayer(this.m_SetRotationButton, GameLayer.IgnoreFullScreenEffects);
    iTween.MoveTo(this.m_SetRotationButton, iTween.Hash((object) "position", (object) this.m_ButtonRiseBone.transform.position, (object) "delay", (object) 0.0f, (object) "time", (object) this.m_ButtonRiseTime, (object) "islocal", (object) false, (object) "easetype", (object) iTween.EaseType.easeInOutQuint, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "RaiseButtonComplete"));
  }

  private void RaiseButtonComplete()
  {
    TokyoDrift componentInChildren = this.m_SetRotationButton.GetComponentInChildren<TokyoDrift>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
      return;
    componentInChildren.enabled = true;
  }

  private void StopButtonDrift()
  {
    this.m_ButtonBanner.SetActive(false);
    this.m_ButtonBannerStandard.gameObject.SetActive(false);
    this.m_ButtonBannerWild.gameObject.SetActive(false);
    TokyoDrift componentInChildren = this.m_SetRotationButton.GetComponentInChildren<TokyoDrift>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
      return;
    componentInChildren.enabled = false;
  }

  private void ShowButtonBanner()
  {
    this.m_ButtonBanner.SetActive(true);
    this.m_ButtonBannerStandard.gameObject.SetActive(true);
    this.m_ButtonBanner.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    iTween.ScaleTo(this.m_ButtonBanner, iTween.Hash((object) "scale", (object) this.m_buttonBannerScale, (object) "time", (object) 0.15f, (object) "easetype", (object) iTween.EaseType.easeOutQuad));
  }

  private void ShowButtonYellowGlow()
  {
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "islocal", (object) true, (object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) 0.3f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "onupdate", (object) (Action<object>) (value => this.m_ButtonGlowPlaneYellow.GetComponent<Renderer>().material.SetFloat("_Intensity", (float) value)), (object) "onupdatetarget", (object) this.gameObject));
  }

  private void CrossFadeToGreenGlow()
  {
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "islocal", (object) true, (object) "from", (object) 1f, (object) "to", (object) 0.0f, (object) "time", (object) 0.3f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "onupdate", (object) (Action<object>) (value => this.m_ButtonGlowPlaneYellow.GetComponent<Renderer>().material.SetFloat("_Intensity", (float) value)), (object) "onupdatetarget", (object) this.m_ButtonGlowPlaneYellow));
    iTween.ValueTo(this.m_ButtonGlowPlaneGreen, iTween.Hash((object) "islocal", (object) true, (object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) 0.3f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "onupdate", (object) (Action<object>) (value => this.m_ButtonGlowPlaneGreen.GetComponent<Renderer>().material.SetFloat("_Intensity", (float) value)), (object) "onupdatetarget", (object) this.gameObject));
  }

  private void ButtonBannerCrossFadeText()
  {
    this.m_ButtonBannerStandard.gameObject.SetActive(true);
    this.m_ButtonBannerWild.gameObject.SetActive(true);
    Color textColor = this.m_ButtonBannerWild.TextColor;
    textColor.a = 0.0f;
    this.m_ButtonBannerWild.TextColor = textColor;
    iTween.FadeTo(this.m_ButtonBannerStandard.gameObject, 0.0f, this.m_ButtonFlipTime * 0.1f);
    iTween.FadeTo(this.m_ButtonBannerWild.gameObject, 1f, this.m_ButtonFlipTime * 0.1f);
  }

  private void ButtonBannerPunch()
  {
    Vector3 localScale = this.m_ButtonBanner.transform.localScale;
    iTween.ScaleTo(this.m_ButtonBanner, iTween.Hash((object) "scale", (object) (localScale * 1.5f), (object) "time", (object) 0.075f, (object) "delay", (object) (float) ((double) this.m_ButtonFlipTime * 0.25), (object) "easetype", (object) iTween.EaseType.easeOutQuad, (object) "onupdatetarget", (object) this.gameObject));
    iTween.ScaleTo(this.m_ButtonBanner, iTween.Hash((object) "scale", (object) localScale, (object) "time", (object) 0.25f, (object) "delay", (object) (float) ((double) this.m_ButtonFlipTime * 0.25 + 0.0750000029802322), (object) "easetype", (object) iTween.EaseType.easeInOutQuad, (object) "onupdatetarget", (object) this.gameObject));
  }

  private void HideButtonBanner()
  {
    iTween.ScaleTo(this.m_ButtonBanner, iTween.Hash((object) "scale", (object) Vector3.zero, (object) "time", (object) 0.25f, (object) "easetype", (object) iTween.EaseType.easeInQuad, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "HideButtonBannerComplete"));
  }

  private void HideButtonBannerComplete()
  {
    this.m_ButtonBanner.SetActive(false);
  }

  private void FlipButton()
  {
    iTween.RotateTo(this.m_SetRotationButtonMesh, iTween.Hash((object) "z", (object) 0.0f, (object) "time", (object) this.m_ButtonFlipTime, (object) "islocal", (object) true, (object) "easetype", (object) iTween.EaseType.easeOutElastic));
  }

  private void MoveButtonUp()
  {
    float num = this.m_MoveButtonUpZ;
    if ((bool) UniversalInputManager.UsePhoneUI)
      num = this.m_MoveButtonUpZphone;
    iTween.MoveTo(this.m_SetRotationButton, iTween.Hash((object) "z", (object) num, (object) "delay", (object) 0.0f, (object) "time", (object) this.m_MoveButtonUpTime, (object) "islocal", (object) true, (object) "easetype", (object) iTween.EaseType.easeInOutQuint));
  }

  private void VignetteBackground(float time)
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((UnityEngine.Object) fullScreenFxMgr == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) "FullScreenFXMgr is NULL!");
    else
      fullScreenFxMgr.Vignette(0.99f, time, iTween.EaseType.easeOutCubic, (FullScreenFXMgr.EffectListener) null);
  }

  private void StopVignetteBackground(float time)
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((UnityEngine.Object) fullScreenFxMgr == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) "FullScreenFXMgr is NULL!");
    else
      fullScreenFxMgr.Vignette(0.0f, time, iTween.EaseType.easeInCubic, (FullScreenFXMgr.EffectListener) null);
  }

  private void BlurBackground(float time)
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((UnityEngine.Object) fullScreenFxMgr == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) "FullScreenFXMgr is NULL!");
    else
      fullScreenFxMgr.StartStandardBlurVignette(time);
  }

  private void StopBlurBackground(float time)
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((UnityEngine.Object) fullScreenFxMgr == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) "FullScreenFXMgr is NULL!");
    else
      fullScreenFxMgr.EndStandardBlurVignette(time, (FullScreenFXMgr.EffectListener) null);
  }

  private void MoveButtonToDeckPickerTray()
  {
    this.StopButtonDrift();
    Vector3 b = Vector3.zero;
    Vector3 vector3_1 = Vector3.one;
    GameObject theClockButtonBone = DeckPickerTrayDisplay.Get().m_TheClockButtonBone;
    if ((UnityEngine.Object) theClockButtonBone != (UnityEngine.Object) null)
    {
      b = theClockButtonBone.transform.position;
      vector3_1 = theClockButtonBone.transform.localScale;
    }
    Vector3 vector3_2 = Vector3.Lerp(this.m_SetRotationButton.transform.position, b, 0.75f);
    vector3_2 = new Vector3(vector3_2.x + 7f, vector3_2.y, vector3_2.z);
    Vector3[] vector3Array = new Vector3[3]{ this.m_SetRotationButton.transform.position, vector3_2, b };
    this.GetComponent<Animator>().SetTrigger("SocketButton");
    iTween.MoveTo(this.m_SetRotationButton, iTween.Hash((object) "path", (object) vector3Array, (object) "delay", (object) 0.0f, (object) "time", (object) this.m_ButtonToTrayAnimTime, (object) "islocal", (object) false, (object) "easetype", (object) iTween.EaseType.easeInOutQuint, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "ButtonImpactAndShutdownTheClock"));
    iTween.RotateTo(this.m_SetRotationButtonMesh, iTween.Hash((object) "rotation", (object) Vector3.zero, (object) "time", (object) this.m_ButtonToTrayAnimTime, (object) "islocal", (object) true, (object) "easetype", (object) iTween.EaseType.easeInOutQuint));
    iTween.RotateTo(this.m_SetRotationButton, iTween.Hash((object) "rotation", (object) Vector3.zero, (object) "time", (object) this.m_ButtonToTrayAnimTime, (object) "islocal", (object) true, (object) "easetype", (object) iTween.EaseType.easeInOutQuint));
    iTween.ScaleTo(this.m_SetRotationButton, iTween.Hash((object) "scale", (object) vector3_1, (object) "delay", (object) 0.0f, (object) "time", (object) this.m_ButtonToTrayAnimTime, (object) "easetype", (object) iTween.EaseType.easeInOutQuint));
  }

  private void ButtonImpactAndShutdownTheClock()
  {
    this.ShakeCamera();
    this.m_ImpactParticles.Play();
    this.StartCoroutine(this.FinalGlowAndDisableTheClock());
  }

  [DebuggerHidden]
  private IEnumerator FinalGlowAndDisableTheClock()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SetRotationClock.\u003CFinalGlowAndDisableTheClock\u003Ec__Iterator246() { \u003C\u003Ef__this = this };
  }

  private void EndClockStartTutorial()
  {
    Options.Get().SetInt(Option.SET_ROTATION_INTRO_PROGRESS, 1);
    Options.Get().SetBool(Option.SHOW_SET_ROTATION_INTRO_VISUALS, false);
    DeckPickerTrayDisplay.Get().StartSetRotationTutorial();
  }

  private void OnClick(UIEvent e)
  {
    this.m_clickCaptured = true;
  }
}
