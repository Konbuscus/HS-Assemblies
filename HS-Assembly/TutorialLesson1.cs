﻿// Decompiled with JetBrains decompiler
// Type: TutorialLesson1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TutorialLesson1 : MonoBehaviour
{
  public UberText m_health;
  public UberText m_attack;
  public UberText m_minion;

  private void Awake()
  {
    this.m_health.SetGameStringText("GLOBAL_TUTORIAL_HEALTH");
    this.m_attack.SetGameStringText("GLOBAL_TUTORIAL_ATTACK");
    this.m_minion.SetGameStringText("GLOBAL_TUTORIAL_MINION");
  }
}
