﻿// Decompiled with JetBrains decompiler
// Type: UIBFollowObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
[CustomEditClass]
public class UIBFollowObject : MonoBehaviour
{
  public GameObject m_rootObject;
  public GameObject m_objectToFollow;
  public Vector3 m_offset;
  public bool m_useWorldOffset;

  public void UpdateFollowPosition()
  {
    if ((Object) this.m_rootObject == (Object) null || (Object) this.m_objectToFollow == (Object) null)
      return;
    Vector3 position = this.m_objectToFollow.transform.position;
    if ((double) this.m_offset.sqrMagnitude > 0.0)
      position += (Vector3) (this.m_objectToFollow.transform.localToWorldMatrix * (Vector4) this.m_offset);
    this.m_rootObject.transform.position = position;
  }
}
