﻿// Decompiled with JetBrains decompiler
// Type: ParticleSystemScaler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ParticleSystemScaler : MonoBehaviour
{
  public float ParticleSystemScale = 1f;
  private Map<ParticleSystem, ParticleSystemSizes> m_initialValues = new Map<ParticleSystem, ParticleSystemSizes>();
  public GameObject ObjectToInherit;
  private float m_unitMagnitude;

  private void Awake()
  {
    this.m_unitMagnitude = Vector3.one.magnitude;
  }

  private void Update()
  {
    if ((Object) this.ObjectToInherit != (Object) null)
      this.ParticleSystemScale = this.ObjectToInherit.transform.lossyScale.magnitude / this.m_unitMagnitude;
    foreach (ParticleSystem componentsInChild in this.GetComponentsInChildren<ParticleSystem>())
    {
      if (!this.m_initialValues.ContainsKey(componentsInChild))
      {
        this.m_initialValues.Add(componentsInChild, new ParticleSystemSizes());
        this.m_initialValues[componentsInChild].startSpeed = componentsInChild.startSpeed;
        this.m_initialValues[componentsInChild].startSize = componentsInChild.startSize;
        this.m_initialValues[componentsInChild].gravityModifier = componentsInChild.gravityModifier;
      }
      componentsInChild.startSize = this.m_initialValues[componentsInChild].startSize * this.ParticleSystemScale;
      componentsInChild.startSpeed = this.m_initialValues[componentsInChild].startSpeed * this.ParticleSystemScale;
      componentsInChild.gravityModifier = this.m_initialValues[componentsInChild].gravityModifier * this.ParticleSystemScale;
    }
  }

  private void ScaleParticleSystems(float scaleFactor)
  {
    foreach (ParticleSystem componentsInChild in this.GetComponentsInChildren<ParticleSystem>())
    {
      componentsInChild.startSpeed *= scaleFactor;
      componentsInChild.startSize *= scaleFactor;
      componentsInChild.gravityModifier *= scaleFactor;
    }
  }
}
