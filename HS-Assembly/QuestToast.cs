﻿// Decompiled with JetBrains decompiler
// Type: QuestToast
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class QuestToast : MonoBehaviour
{
  private static bool m_showFullscreenEffects = true;
  private string m_toastName = string.Empty;
  private string m_toastDescription = string.Empty;
  public UberText m_questName;
  public GameObject m_nameLine;
  public UberText m_requirement;
  public Transform m_rewardBone;
  public PegUIElement m_clickCatcher;
  public Vector3 m_rewardScale;
  public Vector3_MobileOverride m_boosterRewardRootScale;
  public Vector3_MobileOverride m_boosterRewardPosition;
  public Vector3_MobileOverride m_boosterRewardScale;
  public Vector3_MobileOverride m_cardRewardRootScale;
  public Vector3_MobileOverride m_cardRewardScale;
  public Vector3_MobileOverride m_cardRewardLocation;
  public Vector3_MobileOverride m_cardDuplicateRewardScale;
  public Vector3_MobileOverride m_cardDuplicateRewardLocation;
  public Vector3_MobileOverride m_cardBackRootScale;
  public Vector3_MobileOverride m_cardbackRewardScale;
  public Vector3_MobileOverride m_cardbackRewardLocation;
  public Vector3_MobileOverride m_goldRewardScale;
  public Vector3_MobileOverride m_goldBannerOffset;
  public Vector3_MobileOverride m_goldBannerScale;
  public Vector3_MobileOverride m_dustRewardScale;
  public Vector3_MobileOverride m_dustRewardOffset;
  public Vector3_MobileOverride m_dustBannerOffset;
  public Vector3_MobileOverride m_dustBannerScale;
  private QuestToast.DelOnCloseQuestToast m_onCloseCallback;
  private object m_onCloseCallbackData;
  private List<RewardData> m_toastRewards;
  private static bool m_questActive;
  private static QuestToast m_activeQuest;

  public void Awake()
  {
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
  }

  public void OnDestroy()
  {
    if (!((Object) this == (Object) QuestToast.m_activeQuest))
      return;
    if (QuestToast.m_questActive)
    {
      this.FadeEffectsOut();
      QuestToast.m_questActive = false;
    }
    QuestToast.m_activeQuest = (QuestToast) null;
  }

  public static void ShowQuestToast(UserAttentionBlocker blocker, QuestToast.DelOnCloseQuestToast onClosedCallback, bool updateCacheValues, Achievement quest)
  {
    QuestToast.ShowQuestToast(blocker, onClosedCallback, updateCacheValues, quest, true);
  }

  public static void ShowQuestToast(UserAttentionBlocker blocker, QuestToast.DelOnCloseQuestToast onClosedCallback, bool updateCacheValues, Achievement quest, bool fullScreenEffects)
  {
    QuestToast.ShowQuestToast(blocker, onClosedCallback, (object) null, updateCacheValues, quest, fullScreenEffects);
  }

  public static void ShowQuestToast(UserAttentionBlocker blocker, QuestToast.DelOnCloseQuestToast onClosedCallback, object callbackUserData, bool updateCacheValues, Achievement quest)
  {
    QuestToast.ShowQuestToast(blocker, onClosedCallback, callbackUserData, updateCacheValues, quest, true);
  }

  public static void ShowQuestToast(UserAttentionBlocker blocker, QuestToast.DelOnCloseQuestToast onClosedCallback, object callbackUserData, bool updateCacheValues, Achievement quest, bool fullscreenEffects)
  {
    if (!UserAttentionManager.CanShowAttentionGrabber(blocker, "ShowQuestToast:" + (quest != null ? quest.ID.ToString() : "null")))
    {
      if (onClosedCallback == null)
        return;
      onClosedCallback(callbackUserData);
    }
    else
    {
      Log.Achievements.Print("QuestToast: ShowQuestToast: {0}", (object) quest);
      quest.AckCurrentProgressAndRewardNotices();
      if (quest.ID == 56)
        return;
      QuestToast.m_showFullscreenEffects = fullscreenEffects;
      QuestToast.m_questActive = true;
      AssetLoader.Get().LoadActor("QuestToast", true, new AssetLoader.GameObjectCallback(QuestToast.PositionActor), (object) new QuestToast.ToastCallbackData()
      {
        m_toastRewards = quest.Rewards,
        m_toastName = quest.Name,
        m_toastDescription = quest.Description,
        m_onCloseCallback = onClosedCallback,
        m_onCloseCallbackData = callbackUserData,
        m_updateCacheValues = updateCacheValues
      }, false);
    }
  }

  public static void ShowFixedRewardQuestToast(UserAttentionBlocker blocker, QuestToast.DelOnCloseQuestToast onClosedCallback, RewardData rewardData, string name, string description)
  {
    QuestToast.ShowFixedRewardQuestToast(blocker, onClosedCallback, (object) null, rewardData, name, description, true);
  }

  public static void ShowFixedRewardQuestToast(UserAttentionBlocker blocker, QuestToast.DelOnCloseQuestToast onClosedCallback, RewardData rewardData, string name, string description, bool fullscreenEffects)
  {
    QuestToast.ShowFixedRewardQuestToast(blocker, onClosedCallback, (object) null, rewardData, name, description, fullscreenEffects);
  }

  public static void ShowFixedRewardQuestToast(UserAttentionBlocker blocker, QuestToast.DelOnCloseQuestToast onClosedCallback, object callbackUserData, RewardData rewardData, string name, string description)
  {
    QuestToast.ShowFixedRewardQuestToast(blocker, onClosedCallback, (object) null, rewardData, name, description, true);
  }

  public static void ShowFixedRewardQuestToast(UserAttentionBlocker blocker, QuestToast.DelOnCloseQuestToast onClosedCallback, object callbackUserData, RewardData rewardData, string name, string description, bool fullscreenEffects)
  {
    int num = (int) blocker;
    string str1 = "ShowFixedRewardQuestToast:";
    string str2;
    if (rewardData == null)
      str2 = "null";
    else
      str2 = ((int) rewardData.Origin).ToString() + ":" + (object) rewardData.OriginData + ":" + (object) rewardData.RewardType;
    string callerName = str1 + str2;
    if (!UserAttentionManager.CanShowAttentionGrabber((UserAttentionBlocker) num, callerName))
      return;
    Log.Achievements.Print("ShowFixedRewardQuestToast: name={0} desc={1}", new object[2]
    {
      (object) name,
      (object) description
    });
    QuestToast.m_showFullscreenEffects = fullscreenEffects;
    QuestToast.m_questActive = true;
    AssetLoader.Get().LoadActor("QuestToast", true, new AssetLoader.GameObjectCallback(QuestToast.PositionActor), (object) new QuestToast.ToastCallbackData()
    {
      m_toastRewards = new List<RewardData>() { rewardData },
      m_toastName = name,
      m_toastDescription = description,
      m_onCloseCallback = onClosedCallback,
      m_onCloseCallbackData = callbackUserData,
      m_updateCacheValues = true
    }, false);
  }

  private static void PositionActor(string actorName, GameObject actorObject, object c)
  {
    actorObject.transform.localPosition = new Vector3(6f, 85f, 3f);
    Vector3 localScale = actorObject.transform.localScale;
    actorObject.transform.localScale = 0.01f * Vector3.one;
    actorObject.SetActive(true);
    iTween.ScaleTo(actorObject, localScale, 0.5f);
    QuestToast component = actorObject.GetComponent<QuestToast>();
    if ((Object) component == (Object) null)
    {
      Debug.LogWarning((object) "QuestToast.PositionActor(): actor has no QuestToast component");
      QuestToast.m_questActive = false;
    }
    else
    {
      QuestToast.m_activeQuest = component;
      QuestToast.ToastCallbackData toastCallbackData = c as QuestToast.ToastCallbackData;
      component.m_onCloseCallback = toastCallbackData.m_onCloseCallback;
      component.m_toastRewards = toastCallbackData.m_toastRewards;
      component.m_toastName = toastCallbackData.m_toastName;
      component.m_toastDescription = toastCallbackData.m_toastDescription;
      component.SetUpToast(toastCallbackData.m_updateCacheValues);
    }
  }

  private void CloseQuestToast(UIEvent e)
  {
    this.CloseQuestToast();
  }

  public void CloseQuestToast()
  {
    if ((Object) this.gameObject == (Object) null)
      return;
    QuestToast.m_questActive = false;
    this.m_clickCatcher.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.CloseQuestToast));
    this.FadeEffectsOut();
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) Vector3.zero, (object) "time", (object) 0.5f, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "DestroyQuestToast"));
    if (this.m_onCloseCallback == null)
      return;
    this.m_onCloseCallback(this.m_onCloseCallbackData);
  }

  public static bool IsQuestActive()
  {
    if (QuestToast.m_questActive)
      return (Object) QuestToast.m_activeQuest != (Object) null;
    return false;
  }

  public static QuestToast GetCurrentToast()
  {
    return QuestToast.m_activeQuest;
  }

  private void DestroyQuestToast()
  {
    Object.Destroy((Object) this.gameObject);
  }

  public void SetUpToast(bool updateCacheValues)
  {
    this.m_clickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.CloseQuestToast));
    this.m_questName.Text = this.m_toastName;
    this.m_requirement.Text = this.m_toastDescription;
    RewardData rewardData = (RewardData) null;
    if (this.m_toastRewards != null)
    {
      using (List<RewardData>.Enumerator enumerator = this.m_toastRewards.GetEnumerator())
      {
        if (enumerator.MoveNext())
          rewardData = enumerator.Current;
      }
    }
    if (rewardData != null)
      rewardData.LoadRewardObject(new Reward.DelOnRewardLoaded(this.RewardObjectLoaded), (object) updateCacheValues);
    this.FadeEffectsIn();
  }

  private void RewardObjectLoaded(Reward reward, object callbackData)
  {
    bool updateCacheValues = (bool) callbackData;
    reward.Hide(false);
    reward.transform.parent = this.m_rewardBone;
    reward.transform.localEulerAngles = Vector3.zero;
    reward.transform.localScale = this.m_rewardScale;
    reward.transform.localPosition = Vector3.zero;
    BoosterPackReward componentInChildren1 = reward.gameObject.GetComponentInChildren<BoosterPackReward>();
    if ((Object) componentInChildren1 != (Object) null)
    {
      reward.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_boosterRewardRootScale);
      reward.m_MeshRoot.transform.localPosition = (Vector3) ((MobileOverrideValue<Vector3>) this.m_boosterRewardPosition);
      reward.m_MeshRoot.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_boosterRewardScale);
      componentInChildren1.m_Layer = (GameLayer) this.gameObject.layer;
    }
    CardReward componentInChildren2 = reward.gameObject.GetComponentInChildren<CardReward>();
    if ((Object) componentInChildren2 != (Object) null)
    {
      reward.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_cardRewardRootScale);
      componentInChildren2.m_cardParent.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_cardRewardScale);
      componentInChildren2.m_cardParent.transform.localPosition = (Vector3) ((MobileOverrideValue<Vector3>) this.m_cardRewardLocation);
      componentInChildren2.m_duplicateCardParent.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_cardDuplicateRewardScale);
      componentInChildren2.m_duplicateCardParent.transform.localPosition = (Vector3) ((MobileOverrideValue<Vector3>) this.m_cardDuplicateRewardLocation);
    }
    CardBackReward componentInChildren3 = reward.gameObject.GetComponentInChildren<CardBackReward>();
    if ((Object) componentInChildren3 != (Object) null)
    {
      reward.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_cardBackRootScale);
      componentInChildren3.m_cardbackBone.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_cardbackRewardScale);
      componentInChildren3.m_cardbackBone.transform.localPosition = (Vector3) ((MobileOverrideValue<Vector3>) this.m_cardbackRewardLocation);
    }
    GoldReward componentInChildren4 = reward.gameObject.GetComponentInChildren<GoldReward>();
    if ((Object) componentInChildren4 != (Object) null)
    {
      componentInChildren4.m_root.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_goldRewardScale);
      componentInChildren4.m_rewardBannerBone.transform.localPosition += (Vector3) ((MobileOverrideValue<Vector3>) this.m_goldBannerOffset);
      componentInChildren4.m_rewardBannerBone.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_goldBannerScale);
    }
    ArcaneDustReward componentInChildren5 = reward.gameObject.GetComponentInChildren<ArcaneDustReward>();
    if ((Object) componentInChildren5 != (Object) null)
    {
      componentInChildren5.m_root.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_dustRewardScale);
      componentInChildren5.m_root.transform.localPosition += (Vector3) ((MobileOverrideValue<Vector3>) this.m_dustRewardOffset);
      componentInChildren5.m_rewardBannerBone.transform.localPosition += (Vector3) ((MobileOverrideValue<Vector3>) this.m_dustBannerOffset);
      componentInChildren5.m_rewardBannerBone.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) this.m_dustBannerScale);
    }
    SceneUtils.SetLayer(reward.gameObject, this.gameObject.layer);
    reward.Show(updateCacheValues);
  }

  private void FadeEffectsIn()
  {
    if (!QuestToast.m_showFullscreenEffects)
      return;
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.SetBlurBrightness(1f);
    fullScreenFxMgr.SetBlurDesaturation(0.0f);
    fullScreenFxMgr.Vignette(0.4f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.Blur(1f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void FadeEffectsOut()
  {
    if (!QuestToast.m_showFullscreenEffects)
      return;
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private class ToastCallbackData
  {
    public string m_toastName = string.Empty;
    public string m_toastDescription = string.Empty;
    public QuestToast.DelOnCloseQuestToast m_onCloseCallback;
    public object m_onCloseCallbackData;
    public List<RewardData> m_toastRewards;
    public bool m_updateCacheValues;
  }

  public delegate void DelOnCloseQuestToast(object userData);
}
