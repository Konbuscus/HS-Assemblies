﻿// Decompiled with JetBrains decompiler
// Type: QueueList`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class QueueList<T> : IEnumerable, IEnumerable<T>
{
  protected List<T> m_list = new List<T>();

  public int Count
  {
    get
    {
      return this.m_list.Count;
    }
  }

  public T this[int index]
  {
    get
    {
      return this.m_list[index];
    }
    set
    {
      this.m_list[index] = value;
    }
  }

  IEnumerator IEnumerable.GetEnumerator()
  {
    return (IEnumerator) this.GetEnumerator();
  }

  public int Enqueue(T item)
  {
    int count = this.m_list.Count;
    this.m_list.Add(item);
    return count;
  }

  public T Dequeue()
  {
    T obj = this.m_list[0];
    this.m_list.RemoveAt(0);
    return obj;
  }

  public T Peek()
  {
    return this.m_list[0];
  }

  public int GetCount()
  {
    return this.m_list.Count;
  }

  public T GetItem(int index)
  {
    return this.m_list[index];
  }

  public void Clear()
  {
    this.m_list.Clear();
  }

  public T RemoveAt(int position)
  {
    if (this.m_list.Count <= position)
      return default (T);
    T obj = this.m_list[position];
    this.m_list.RemoveAt(position);
    return obj;
  }

  public bool Remove(T item)
  {
    return this.m_list.Remove(item);
  }

  public List<T> GetList()
  {
    return this.m_list;
  }

  public bool Contains(T item)
  {
    return this.m_list.Contains(item);
  }

  public IEnumerator<T> GetEnumerator()
  {
    return this.Enumerate().GetEnumerator();
  }

  public override string ToString()
  {
    return string.Format("Count={0}", (object) this.Count);
  }

  [DebuggerHidden]
  protected IEnumerable<T> Enumerate()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    QueueList<T>.\u003CEnumerate\u003Ec__IteratorDE enumerateCIteratorDe = new QueueList<T>.\u003CEnumerate\u003Ec__IteratorDE() { \u003C\u003Ef__this = this };
    // ISSUE: reference to a compiler-generated field
    enumerateCIteratorDe.\u0024PC = -2;
    return (IEnumerable<T>) enumerateCIteratorDe;
  }
}
