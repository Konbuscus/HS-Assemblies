﻿// Decompiled with JetBrains decompiler
// Type: AdventureScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class AdventureScene : Scene
{
  [CustomEditField(Sections = "Transition Motions")]
  public Vector3 m_SubScenePosition = Vector3.zero;
  [CustomEditField(Sections = "Transition Motions")]
  public float m_DefaultTransitionAnimationTime = 1f;
  [CustomEditField(Sections = "Transition Motions")]
  public iTween.EaseType m_TransitionEaseType = iTween.EaseType.easeInOutSine;
  [CustomEditField(Sections = "Adventure Subscene Prefabs")]
  public List<AdventureScene.AdventureSubSceneDef> m_SubSceneDefs = new List<AdventureScene.AdventureSubSceneDef>();
  [CustomEditField(Sections = "Music Settings")]
  public List<AdventureScene.AdventureModeMusic> m_AdventureModeMusic = new List<AdventureScene.AdventureModeMusic>();
  private Map<AdventureDbId, AdventureDef> m_adventureDefs = new Map<AdventureDbId, AdventureDef>();
  private Map<WingDbId, AdventureWingDef> m_wingDefs = new Map<WingDbId, AdventureWingDef>();
  private const AdventureSubScenes s_StartMode = AdventureSubScenes.Chooser;
  private static AdventureScene s_instance;
  [CustomEditField(Sections = "Transition Blocker")]
  public GameObject m_transitionClickBlocker;
  [CustomEditField(Sections = "Transition Motions")]
  public AdventureScene.TransitionDirection m_TransitionDirection;
  [CustomEditField(Sections = "Transition Sounds", T = EditType.SOUND_PREFAB)]
  public string m_SlideInSound;
  [CustomEditField(Sections = "Transition Sounds", T = EditType.SOUND_PREFAB)]
  public string m_SlideOutSound;
  private GameObject m_TransitionOutSubScene;
  private GameObject m_CurrentSubScene;
  private bool m_ReverseTransition;
  private int m_StartupAssetLoads;
  private int m_SubScenesLoaded;
  private bool m_MusicStopped;
  private bool m_Unloading;

  public bool IsDevMode { get; set; }

  public int DevModeSetting { get; set; }

  protected override void Awake()
  {
    base.Awake();
    AdventureScene.s_instance = this;
    this.m_CurrentSubScene = (GameObject) null;
    this.m_TransitionOutSubScene = (GameObject) null;
    AdventureConfig adventureConfig = AdventureConfig.Get();
    adventureConfig.OnAdventureSceneAwake();
    adventureConfig.AddSubSceneChangeListener(new AdventureConfig.SubSceneChange(this.OnSubSceneChange));
    adventureConfig.AddSelectedModeChangeListener(new AdventureConfig.SelectedModeChange(this.OnSelectedModeChanged));
    adventureConfig.AddAdventureModeChangeListener(new AdventureConfig.AdventureModeChange(this.OnAdventureModeChanged));
    ++this.m_StartupAssetLoads;
    bool flag = Options.Get().GetBool(Option.HAS_SEEN_NAXX);
    if (!flag)
      ++this.m_StartupAssetLoads;
    if (!flag)
    {
      SoundManager.Get().Load("VO_KT_INTRO_39");
      AssetLoader.Get().LoadGameObject("KT_Quote", new AssetLoader.GameObjectCallback(this.OnKTQuoteLoaded), (object) null, false);
    }
    Options.Get().SetBool(Option.BUNDLE_JUST_PURCHASE_IN_HUB, false);
    if (ApplicationMgr.IsInternal())
      CheatMgr.Get().RegisterCheatHandler("advdev", new CheatMgr.ProcessCheatCallback(this.OnDevCheat), (string) null, (string) null, (string) null);
    this.InitializeAllDefs();
    this.LoadSubScene(adventureConfig.GetCurrentSubScene(), new AssetLoader.GameObjectCallback(this.OnFirstSubSceneLoaded));
  }

  private void Start()
  {
    AdventureConfig.Get().UpdatePresence();
  }

  private void OnDestroy()
  {
    AdventureScene.s_instance = (AdventureScene) null;
  }

  private void Update()
  {
    Network.Get().ProcessNetwork();
  }

  public static AdventureScene Get()
  {
    return AdventureScene.s_instance;
  }

  public override bool IsUnloading()
  {
    return this.m_Unloading;
  }

  public override void Unload()
  {
    this.m_Unloading = true;
    AdventureConfig adventureConfig = AdventureConfig.Get();
    adventureConfig.ClearBossDefs();
    DeckPickerTray.Get().Unload();
    adventureConfig.RemoveAdventureModeChangeListener(new AdventureConfig.AdventureModeChange(this.OnAdventureModeChanged));
    adventureConfig.RemoveSelectedModeChangeListener(new AdventureConfig.SelectedModeChange(this.OnSelectedModeChanged));
    adventureConfig.RemoveSubSceneChangeListener(new AdventureConfig.SubSceneChange(this.OnSubSceneChange));
    adventureConfig.OnAdventureSceneUnload();
    CheatMgr.Get().UnregisterCheatHandler("advdev", new CheatMgr.ProcessCheatCallback(this.OnDevCheat));
    this.m_Unloading = false;
  }

  public bool IsInitialScreen()
  {
    return this.m_SubScenesLoaded <= 1;
  }

  public AdventureDef GetAdventureDef(AdventureDbId advId)
  {
    AdventureDef adventureDef = (AdventureDef) null;
    this.m_adventureDefs.TryGetValue(advId, out adventureDef);
    return adventureDef;
  }

  public List<AdventureDef> GetSortedAdventureDefs()
  {
    List<AdventureDef> adventureDefList = new List<AdventureDef>((IEnumerable<AdventureDef>) this.m_adventureDefs.Values);
    adventureDefList.Sort((Comparison<AdventureDef>) ((l, r) => r.GetSortOrder() - l.GetSortOrder()));
    return adventureDefList;
  }

  public AdventureWingDef GetWingDef(WingDbId wingId)
  {
    AdventureWingDef adventureWingDef = (AdventureWingDef) null;
    this.m_wingDefs.TryGetValue(wingId, out adventureWingDef);
    return adventureWingDef;
  }

  public bool IsAdventureOpen(AdventureDbId advId)
  {
    bool flag = true;
    using (Map<WingDbId, AdventureWingDef>.Enumerator enumerator = this.m_wingDefs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<WingDbId, AdventureWingDef> current = enumerator.Current;
        if (current.Value.GetAdventureId() == advId)
        {
          if (AdventureProgressMgr.Get().IsWingOpen((int) current.Value.GetWingId()))
            return true;
          flag = false;
        }
      }
    }
    return flag;
  }

  private void InitializeAllDefs()
  {
    List<AdventureDbfRecord> recordsWithDefPrefab = GameUtils.GetAdventureRecordsWithDefPrefab();
    List<AdventureDataDbfRecord> withSubDefPrefab = GameUtils.GetAdventureDataRecordsWithSubDefPrefab();
    using (List<AdventureDbfRecord>.Enumerator enumerator = recordsWithDefPrefab.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureDbfRecord current = enumerator.Current;
        AdventureDef adventureDef = GameUtils.LoadGameObjectWithComponent<AdventureDef>(current.AdventureDefPrefab);
        if (!((UnityEngine.Object) adventureDef == (UnityEngine.Object) null))
        {
          adventureDef.Init(current, withSubDefPrefab);
          this.m_adventureDefs.Add(adventureDef.GetAdventureId(), adventureDef);
        }
      }
    }
    using (List<WingDbfRecord>.Enumerator enumerator = GameDbf.Wing.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WingDbfRecord current = enumerator.Current;
        if (!string.IsNullOrEmpty(current.AdventureWingDefPrefab))
        {
          AdventureWingDef adventureWingDef = GameUtils.LoadGameObjectWithComponent<AdventureWingDef>(current.AdventureWingDefPrefab);
          if (!((UnityEngine.Object) adventureWingDef == (UnityEngine.Object) null))
          {
            adventureWingDef.Init(current);
            this.m_wingDefs.Add(adventureWingDef.GetWingId(), adventureWingDef);
          }
        }
      }
    }
  }

  private void OnKTQuoteLoaded(string name, GameObject go, object userData)
  {
    if ((UnityEngine.Object) go != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    this.OnStartupAssetLoaded();
  }

  private void UpdateAdventureModeMusic()
  {
    AdventureDbId selectedAdventure = AdventureConfig.Get().GetSelectedAdventure();
    AdventureSubScenes currentSubScene = AdventureConfig.Get().GetCurrentSubScene();
    AdventureScene.AdventureModeMusic adventureModeMusic = (AdventureScene.AdventureModeMusic) null;
    using (List<AdventureScene.AdventureModeMusic>.Enumerator enumerator = this.m_AdventureModeMusic.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureScene.AdventureModeMusic current = enumerator.Current;
        if (current.m_subsceneId == currentSubScene && current.m_adventureId == selectedAdventure)
        {
          adventureModeMusic = current;
          break;
        }
        if (current.m_subsceneId == currentSubScene && current.m_adventureId == AdventureDbId.INVALID)
          adventureModeMusic = current;
      }
    }
    if (adventureModeMusic == null)
      return;
    MusicManager.Get().StartPlaylist(adventureModeMusic.m_playlist);
  }

  private void OnStartupAssetLoaded()
  {
    --this.m_StartupAssetLoads;
    if (this.m_StartupAssetLoads > 0)
      return;
    this.UpdateAdventureModeMusic();
    SceneMgr.Get().NotifySceneLoaded();
  }

  private void LoadSubScene(AdventureSubScenes subscene)
  {
    this.LoadSubScene(subscene, new AssetLoader.GameObjectCallback(this.OnSubSceneLoaded));
  }

  private void LoadSubScene(AdventureSubScenes subscene, AssetLoader.GameObjectCallback callback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureScene.\u003CLoadSubScene\u003Ec__AnonStorey35B sceneCAnonStorey35B = new AdventureScene.\u003CLoadSubScene\u003Ec__AnonStorey35B();
    // ISSUE: reference to a compiler-generated field
    sceneCAnonStorey35B.subscene = subscene;
    // ISSUE: reference to a compiler-generated field
    sceneCAnonStorey35B.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated method
    AdventureScene.AdventureSubSceneDef adventureSubSceneDef = this.m_SubSceneDefs.Find(new Predicate<AdventureScene.AdventureSubSceneDef>(sceneCAnonStorey35B.\u003C\u003Em__32));
    if (adventureSubSceneDef == null)
    {
      // ISSUE: reference to a compiler-generated field
      UnityEngine.Debug.LogError((object) string.Format("Subscene {0} prefab not defined in m_SubSceneDefs", (object) sceneCAnonStorey35B.subscene));
    }
    else
    {
      this.EnableTransitionBlocker(true);
      // ISSUE: reference to a compiler-generated field
      sceneCAnonStorey35B.runCallback = callback;
      // ISSUE: reference to a compiler-generated method
      AssetLoader.Get().LoadUIScreen(FileUtils.GameAssetPathToName((string) ((MobileOverrideValue<string>) adventureSubSceneDef.m_Prefab)), new AssetLoader.GameObjectCallback(sceneCAnonStorey35B.\u003C\u003Em__33), (object) null, false);
    }
  }

  private void OnSubSceneChange(AdventureSubScenes newscene, bool forward)
  {
    this.m_ReverseTransition = !forward;
    this.LoadSubScene(newscene);
  }

  private Vector3 GetMoveDirection()
  {
    float num = 1f;
    if (this.m_TransitionDirection >= AdventureScene.TransitionDirection.NX)
      num *= -1f;
    Vector3 zero = Vector3.zero;
    zero[(int) this.m_TransitionDirection % 3] = num;
    return zero;
  }

  private void OnFirstSubSceneLoaded(string name, GameObject screen, object callbackData)
  {
    this.ShowExpertAIUnlockTip();
    this.OnSubSceneLoaded(name, screen, callbackData);
    this.OnStartupAssetLoaded();
  }

  private void OnSubSceneLoaded(string name, GameObject screen, object callbackData)
  {
    this.m_TransitionOutSubScene = this.m_CurrentSubScene;
    this.m_CurrentSubScene = screen;
    this.m_CurrentSubScene.transform.position = new Vector3(-500f, 0.0f, 0.0f);
    Vector3 localScale = this.m_CurrentSubScene.transform.localScale;
    this.m_CurrentSubScene.transform.parent = this.transform;
    this.m_CurrentSubScene.transform.localScale = localScale;
    AdventureSubScene component = this.m_CurrentSubScene.GetComponent<AdventureSubScene>();
    ++this.m_SubScenesLoaded;
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      this.DoSubSceneTransition(component);
    else
      this.StartCoroutine(this.WaitForSubSceneToLoad());
  }

  private void DoSubSceneTransition(AdventureSubScene subscene)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureScene.\u003CDoSubSceneTransition\u003Ec__AnonStorey35C transitionCAnonStorey35C = new AdventureScene.\u003CDoSubSceneTransition\u003Ec__AnonStorey35C();
    // ISSUE: reference to a compiler-generated field
    transitionCAnonStorey35C.\u003C\u003Ef__this = this;
    this.m_CurrentSubScene.transform.localPosition = this.m_SubScenePosition;
    if ((UnityEngine.Object) this.m_TransitionOutSubScene == (UnityEngine.Object) null)
    {
      this.CompleteTransition();
    }
    else
    {
      float num = !((UnityEngine.Object) subscene == (UnityEngine.Object) null) ? subscene.m_TransitionAnimationTime : this.m_DefaultTransitionAnimationTime;
      Vector3 moveDirection = this.GetMoveDirection();
      // ISSUE: reference to a compiler-generated field
      transitionCAnonStorey35C.delobj = this.m_TransitionOutSubScene;
      if (this.m_ReverseTransition)
      {
        AdventureSubScene component = this.m_TransitionOutSubScene.GetComponent<AdventureSubScene>();
        Vector3 vector3 = !((UnityEngine.Object) component == (UnityEngine.Object) null) ? (Vector3) ((MobileOverrideValue<Vector3>) component.m_SubSceneBounds) : TransformUtil.GetBoundsOfChildren(this.m_TransitionOutSubScene).size;
        Vector3 localPosition = this.m_TransitionOutSubScene.transform.localPosition;
        localPosition.x -= vector3.x * moveDirection.x;
        localPosition.y -= vector3.y * moveDirection.y;
        localPosition.z -= vector3.z * moveDirection.z;
        // ISSUE: reference to a compiler-generated method
        iTween.MoveTo(this.m_TransitionOutSubScene, iTween.Hash((object) "islocal", (object) true, (object) "position", (object) localPosition, (object) "time", (object) num, (object) "easeType", (object) this.m_TransitionEaseType, (object) "oncomplete", (object) new Action<object>(transitionCAnonStorey35C.\u003C\u003Em__34), (object) "oncompletetarget", (object) this.gameObject));
        if (!string.IsNullOrEmpty(this.m_SlideOutSound))
          SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_SlideOutSound));
      }
      else
      {
        AdventureSubScene component = this.m_CurrentSubScene.GetComponent<AdventureSubScene>();
        Vector3 vector3 = !((UnityEngine.Object) component == (UnityEngine.Object) null) ? (Vector3) ((MobileOverrideValue<Vector3>) component.m_SubSceneBounds) : TransformUtil.GetBoundsOfChildren(this.m_CurrentSubScene).size;
        Vector3 localPosition1 = this.m_CurrentSubScene.transform.localPosition;
        Vector3 localPosition2 = this.m_CurrentSubScene.transform.localPosition;
        localPosition2.x -= vector3.x * moveDirection.x;
        localPosition2.y -= vector3.y * moveDirection.y;
        localPosition2.z -= vector3.z * moveDirection.z;
        this.m_CurrentSubScene.transform.localPosition = localPosition2;
        // ISSUE: reference to a compiler-generated method
        iTween.MoveTo(this.m_CurrentSubScene, iTween.Hash((object) "islocal", (object) true, (object) "position", (object) localPosition1, (object) "time", (object) num, (object) "easeType", (object) this.m_TransitionEaseType, (object) "oncomplete", (object) new Action<object>(transitionCAnonStorey35C.\u003C\u003Em__35), (object) "oncompletetarget", (object) this.gameObject));
        if (!string.IsNullOrEmpty(this.m_SlideInSound))
          SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_SlideInSound));
      }
      this.m_TransitionOutSubScene = (GameObject) null;
    }
  }

  private void DestroyTransitioningSubScene(GameObject destroysubscene)
  {
    if (!((UnityEngine.Object) destroysubscene != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.DestroyObject((UnityEngine.Object) destroysubscene);
  }

  private void CompleteTransition()
  {
    AdventureSubScene component = this.m_CurrentSubScene.GetComponent<AdventureSubScene>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
    {
      component.NotifyTransitionComplete();
      this.UpdateAdventureModeMusic();
    }
    this.EnableTransitionBlocker(false);
  }

  [DebuggerHidden]
  private IEnumerator WaitForSubSceneToLoad()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureScene.\u003CWaitForSubSceneToLoad\u003Ec__IteratorC()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnSelectedModeChanged(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    if (AdventureConfig.Get().CanPlayMode(adventureId, modeId))
    {
      if (adventureId == AdventureDbId.NAXXRAMAS)
      {
        if (!Options.Get().GetBool(Option.HAS_SEEN_NAXX))
          this.OnSelectedModeChanged_CreateKTIntroConversation();
      }
      else if (adventureId == AdventureDbId.BRM)
      {
        if ((UnityEngine.Object) AdventureScene.Get() != (UnityEngine.Object) null && AdventureScene.Get().IsDevMode || !Options.Get().GetBool(Option.HAS_SEEN_BRM))
          this.OnSelectedModeChanged_CreateIntroConversation(0, InitialConversationLines.BRM_INITIAL_CONVO_LINES, Option.HAS_SEEN_BRM);
      }
      else if (adventureId == AdventureDbId.LOE)
      {
        if ((UnityEngine.Object) AdventureScene.Get() != (UnityEngine.Object) null && AdventureScene.Get().IsDevMode || !Options.Get().GetBool(Option.HAS_SEEN_LOE))
          this.OnSelectedModeChanged_CreateIntroConversation(0, InitialConversationLines.LOE_INITIAL_CONVO_LINES, Option.HAS_SEEN_LOE);
      }
      else if (adventureId == AdventureDbId.KARA && ((UnityEngine.Object) AdventureScene.Get() != (UnityEngine.Object) null && AdventureScene.Get().IsDevMode || !Options.Get().GetBool(Option.HAS_SEEN_KARA)))
        this.OnSelectedModeChanged_CreateIntroConversation(0, InitialConversationLines.KARA_INITIAL_CONVO_LINES, Option.HAS_SEEN_KARA);
    }
    this.UpdateAdventureModeMusic();
  }

  private void OnSelectedModeChanged_CreateKTIntroConversation()
  {
    NotificationManager.Get().CreateKTQuote("VO_KT_INTRO_39", "VO_KT_INTRO_39", true);
    Options.Get().SetBool(Option.HAS_SEEN_NAXX, true);
  }

  private void OnSelectedModeChanged_CreateIntroConversation(int index, string[][] convoLines, Option hasSeen)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureScene.\u003COnSelectedModeChanged_CreateIntroConversation\u003Ec__AnonStorey35D conversationCAnonStorey35D = new AdventureScene.\u003COnSelectedModeChanged_CreateIntroConversation\u003Ec__AnonStorey35D();
    // ISSUE: reference to a compiler-generated field
    conversationCAnonStorey35D.index = index;
    // ISSUE: reference to a compiler-generated field
    conversationCAnonStorey35D.convoLines = convoLines;
    // ISSUE: reference to a compiler-generated field
    conversationCAnonStorey35D.hasSeen = hasSeen;
    // ISSUE: reference to a compiler-generated field
    conversationCAnonStorey35D.\u003C\u003Ef__this = this;
    Action finishCallback = (Action) null;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (conversationCAnonStorey35D.index < conversationCAnonStorey35D.convoLines.Length - 1)
    {
      // ISSUE: reference to a compiler-generated method
      finishCallback = new Action(conversationCAnonStorey35D.\u003C\u003Em__36);
    }
    bool flag = (UnityEngine.Object) AdventureScene.Get() != (UnityEngine.Object) null && AdventureScene.Get().IsDevMode;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (conversationCAnonStorey35D.index >= conversationCAnonStorey35D.convoLines.Length - 1 && !flag)
    {
      // ISSUE: reference to a compiler-generated field
      Options.Get().SetBool(conversationCAnonStorey35D.hasSeen, true);
    }
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    string text = GameStrings.Get(conversationCAnonStorey35D.convoLines[conversationCAnonStorey35D.index][1]);
    bool allowRepeatDuringSession = flag;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    NotificationManager.Get().CreateCharacterQuote(conversationCAnonStorey35D.convoLines[conversationCAnonStorey35D.index][0], NotificationManager.DEFAULT_CHARACTER_POS, text, conversationCAnonStorey35D.convoLines[conversationCAnonStorey35D.index][1], allowRepeatDuringSession, 0.0f, finishCallback, CanvasAnchor.BOTTOM_LEFT);
  }

  private void OnAdventureModeChanged(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    if (modeId == AdventureModeDbId.HEROIC)
      this.ShowHeroicWarning();
    if (adventureId == AdventureDbId.NAXXRAMAS && !Options.Get().GetBool(Option.HAS_ENTERED_NAXX))
    {
      NotificationManager.Get().CreateKTQuote("VO_KT_INTRO2_40", "VO_KT_INTRO2_40", true);
      Options.Get().SetBool(Option.HAS_ENTERED_NAXX, true);
    }
    this.UpdateAdventureModeMusic();
  }

  private void ShowHeroicWarning()
  {
    if (Options.Get().GetBool(Option.HAS_SEEN_HEROIC_WARNING))
      return;
    Options.Get().SetBool(Option.HAS_SEEN_HEROIC_WARNING, true);
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_HEROIC_WARNING_TITLE"),
      m_text = GameStrings.Get("GLUE_HEROIC_WARNING"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  private void ShowExpertAIUnlockTip()
  {
    if (AchieveManager.Get().GetAchievesInGroup(Achievement.AchType.UNLOCK_HERO, false).Count > 0 || SceneMgr.Get().GetPrevMode() == SceneMgr.Mode.GAMEPLAY && !Options.Get().GetBool(Option.HAS_SEEN_UNLOCK_ALL_HEROES_TRANSITION) || (Options.Get().GetBool(Option.HAS_SEEN_EXPERT_AI_UNLOCK, false) || !UserAttentionManager.CanShowAttentionGrabber("AdventureScene.ShowExpertAIUnlockTip")))
      return;
    NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_EXPERT_AI_10"), "VO_INNKEEPER_EXPERT_AI_10", 0.0f, (Action) null, false);
    Options.Get().SetBool(Option.HAS_SEEN_EXPERT_AI_UNLOCK, true);
  }

  private bool OnDevCheat(string func, string[] args, string rawArgs)
  {
    if (!ApplicationMgr.IsInternal())
      return true;
    this.IsDevMode = true;
    if (args.Length > 0)
    {
      int result = 1;
      if (int.TryParse(args[0], out result) && result > 0)
      {
        this.IsDevMode = true;
        this.DevModeSetting = result;
      }
    }
    if ((UnityEngine.Object) UIStatus.Get() != (UnityEngine.Object) null)
      UIStatus.Get().AddInfo(string.Format("{0}: IsDevMode={1} DevModeSetting={2}", (object) func, (object) this.IsDevMode, (object) this.DevModeSetting));
    return true;
  }

  private void EnableTransitionBlocker(bool block)
  {
    if (!((UnityEngine.Object) this.m_transitionClickBlocker != (UnityEngine.Object) null))
      return;
    this.m_transitionClickBlocker.SetActive(block);
  }

  public enum TransitionDirection
  {
    X,
    Y,
    Z,
    NX,
    NY,
    NZ,
  }

  [Serializable]
  public class AdventureModeMusic
  {
    public AdventureSubScenes m_subsceneId;
    public AdventureDbId m_adventureId;
    public MusicPlaylistType m_playlist;
  }

  [Serializable]
  public class AdventureSubSceneDef
  {
    [CustomEditField(ListSortable = true)]
    public AdventureSubScenes m_SubScene;
    [CustomEditField(T = EditType.GAME_OBJECT)]
    public String_MobileOverride m_Prefab;
  }
}
