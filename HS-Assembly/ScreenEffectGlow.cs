﻿// Decompiled with JetBrains decompiler
// Type: ScreenEffectGlow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class ScreenEffectGlow : ScreenEffect
{
  public bool m_RenderGlowOnly;
  private bool m_PreviousRenderGlowOnly;
  private int m_PreviousLayer;

  private void Awake()
  {
    this.m_PreviousLayer = this.gameObject.layer;
  }

  private void Start()
  {
    this.SetLayer();
  }

  private void Update()
  {
  }

  private void SetLayer()
  {
    if (this.m_PreviousRenderGlowOnly == this.m_RenderGlowOnly)
      return;
    this.m_PreviousRenderGlowOnly = this.m_RenderGlowOnly;
    if (this.m_RenderGlowOnly)
    {
      this.m_PreviousLayer = this.gameObject.layer;
      SceneUtils.SetLayer(this.gameObject, GameLayer.ScreenEffects);
    }
    else
      SceneUtils.SetLayer(this.gameObject, this.m_PreviousLayer);
  }
}
