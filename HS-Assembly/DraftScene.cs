﻿// Decompiled with JetBrains decompiler
// Type: DraftScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DraftScene : Scene
{
  private bool m_unloading;

  protected override void Awake()
  {
    base.Awake();
    if ((bool) UniversalInputManager.UsePhoneUI)
      AssetLoader.Get().LoadUIScreen("Draft_phone", new AssetLoader.GameObjectCallback(this.OnPhoneUIScreenLoaded), (object) null, false);
    else
      AssetLoader.Get().LoadUIScreen("Draft", new AssetLoader.GameObjectCallback(this.OnUIScreenLoaded), (object) null, false);
  }

  public override bool IsUnloading()
  {
    return this.m_unloading;
  }

  public override void Unload()
  {
    this.m_unloading = true;
    DraftDisplay.Get().Unload();
    this.m_unloading = false;
  }

  private void OnUIScreenLoaded(string name, GameObject screen, object callbackData)
  {
    if ((Object) screen == (Object) null)
      Debug.LogError((object) string.Format("DraftScene.OnUIScreenLoaded() - failed to load screen {0}", (object) name));
    else
      screen.transform.position = new Vector3(-0.5f, 1.27f, 0.0f);
  }

  private void OnPhoneUIScreenLoaded(string name, GameObject screen, object callbackData)
  {
    if ((Object) screen == (Object) null)
    {
      Debug.LogError((object) string.Format("DraftScene.OnUIScreenLoaded() - failed to load screen {0}", (object) name));
    }
    else
    {
      screen.transform.position = new Vector3(26.1f, 0.0f, -9.88f);
      screen.transform.localScale = new Vector3(1.38f, 1.38f, 1.38f);
    }
  }
}
