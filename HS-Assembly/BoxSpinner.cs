﻿// Decompiled with JetBrains decompiler
// Type: BoxSpinner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoxSpinner : MonoBehaviour
{
  private Box m_parent;
  private BoxSpinnerStateInfo m_info;
  private bool m_spinning;
  private float m_spinY;
  private Material m_spinnerMat;

  private void Awake()
  {
    this.m_spinnerMat = this.GetComponent<Renderer>().material;
  }

  private void Update()
  {
    if (!this.IsSpinning())
      return;
    this.m_spinnerMat.SetFloat("_RotAngle", this.m_spinY);
    this.m_spinY += (float) ((double) this.m_info.m_DegreesPerSec * (double) Time.deltaTime * 0.00999999977648258);
  }

  public Box GetParent()
  {
    return this.m_parent;
  }

  public void SetParent(Box parent)
  {
    this.m_parent = parent;
  }

  public BoxSpinnerStateInfo GetInfo()
  {
    return this.m_info;
  }

  public void SetInfo(BoxSpinnerStateInfo info)
  {
    this.m_info = info;
  }

  public void Spin()
  {
    this.m_spinning = true;
  }

  public bool IsSpinning()
  {
    return this.m_spinning;
  }

  public void Stop()
  {
    this.m_spinning = false;
  }

  public void Reset()
  {
    this.m_spinning = false;
    this.m_spinnerMat.SetFloat("_RotAngle", 0.0f);
  }
}
