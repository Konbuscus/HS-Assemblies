﻿// Decompiled with JetBrains decompiler
// Type: Reset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;

public class Reset : Scene
{
  private void Start()
  {
    SceneMgr.Get().NotifySceneLoaded();
    this.StartCoroutine("WaitThenReset");
  }

  [DebuggerHidden]
  private IEnumerator WaitThenReset()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Reset.\u003CWaitThenReset\u003Ec__Iterator23C resetCIterator23C = new Reset.\u003CWaitThenReset\u003Ec__Iterator23C();
    return (IEnumerator) resetCIterator23C;
  }
}
