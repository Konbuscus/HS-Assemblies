﻿// Decompiled with JetBrains decompiler
// Type: MissingStandardDeckDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MissingStandardDeckDisplay : MonoBehaviour
{
  public float m_animateInTime = 1f;
  public float m_animateOutTime = 0.5f;
  private Color m_quadHiddenColor = Color.white;
  private Color m_quadShownColor = new Color(0.53f, 0.53f, 0.53f, 1f);
  public GameObject m_missingStandardDeckObject;
  public UberText m_text;
  public MeshRenderer m_darkenQuad;
  public Transform m_shownBone;
  public Transform m_hiddenBone;
  private bool m_isShown;
  private float m_currentQuadFade;

  public void Awake()
  {
    this.m_isShown = false;
    this.m_missingStandardDeckObject.transform.position = this.m_hiddenBone.transform.position;
    this.m_missingStandardDeckObject.SetActive(false);
    this.m_darkenQuad.gameObject.SetActive(false);
    this.m_darkenQuad.material.color = this.m_quadHiddenColor;
    this.m_text.SetGameStringText("GLUE_TOURNAMENT_NO_STANDARD_DECKS");
  }

  public bool IsShown()
  {
    return this.m_isShown;
  }

  public void ShowImmediately()
  {
    this.m_isShown = true;
    this.m_darkenQuad.gameObject.SetActive(true);
    this.m_missingStandardDeckObject.SetActive(true);
    iTween.Stop(this.m_missingStandardDeckObject);
    iTween.Stop(this.m_darkenQuad.gameObject);
    this.m_currentQuadFade = 1f;
    this.m_darkenQuad.material.color = Color.Lerp(this.m_quadHiddenColor, this.m_quadShownColor, this.m_currentQuadFade);
    this.m_missingStandardDeckObject.transform.position = this.m_shownBone.transform.position;
  }

  public void Show()
  {
    if (this.m_isShown)
      return;
    this.m_isShown = true;
    this.m_darkenQuad.gameObject.SetActive(true);
    this.m_missingStandardDeckObject.SetActive(true);
    iTween.Stop(this.m_missingStandardDeckObject);
    iTween.Stop(this.m_darkenQuad.gameObject);
    iTween.MoveTo(this.m_missingStandardDeckObject, iTween.Hash((object) "position", (object) this.m_shownBone.transform.position, (object) "time", (object) this.m_animateInTime, (object) "easetype", (object) iTween.EaseType.easeOutBounce));
    iTween.ValueTo(this.m_darkenQuad.gameObject, iTween.Hash((object) "from", (object) this.m_currentQuadFade, (object) "to", (object) 1f, (object) "time", (object) this.m_animateInTime, (object) "onupdate", (object) "DarkenQuadFade_Update", (object) "onupdatetarget", (object) this.gameObject));
  }

  public void Hide()
  {
    if (!this.m_isShown)
      return;
    this.m_isShown = false;
    iTween.Stop(this.m_missingStandardDeckObject);
    iTween.Stop(this.m_darkenQuad.gameObject);
    iTween.MoveTo(this.m_missingStandardDeckObject, iTween.Hash((object) "position", (object) this.m_hiddenBone.transform.position, (object) "time", (object) this.m_animateOutTime, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "OnHideComplete", (object) "oncompletetarget", (object) this.gameObject));
    iTween.ValueTo(this.m_darkenQuad.gameObject, iTween.Hash((object) "from", (object) this.m_currentQuadFade, (object) "to", (object) 0.0f, (object) "time", (object) this.m_animateOutTime, (object) "onupdate", (object) "DarkenQuadFade_Update", (object) "onupdatetarget", (object) this.gameObject));
  }

  private void DarkenQuadFade_Update(float fade)
  {
    this.m_currentQuadFade = fade;
    this.m_darkenQuad.material.color = Color.Lerp(this.m_quadHiddenColor, this.m_quadShownColor, this.m_currentQuadFade);
  }

  private void OnHideComplete()
  {
    this.m_missingStandardDeckObject.SetActive(false);
    this.m_darkenQuad.gameObject.SetActive(false);
  }
}
