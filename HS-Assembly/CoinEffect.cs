﻿// Decompiled with JetBrains decompiler
// Type: CoinEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CoinEffect : MonoBehaviour
{
  private string coinSpawnAnim = "CoinSpawn1_edit";
  private string coinDropAnim = "MulliganCoinDropGo2Card";
  private string coinDropAnim2 = "MulliganCoinDrop2_Edit";
  private string coinGlowDropAnim = "MulliganCoinDrop1Glow_Edit";
  private string coinGlowDropAnim2 = "MulliganCoinDrop2Glow_Edit";
  public GameObject coinSpawnObject;
  public GameObject coin;
  public GameObject coinGlow;
  private string animToUse;
  private string GlowanimToUse;

  private void Start()
  {
  }

  public void DoAnim(bool localWin)
  {
    if (localWin)
    {
      this.animToUse = this.coinDropAnim2;
      this.GlowanimToUse = this.coinGlowDropAnim2;
    }
    else
    {
      this.animToUse = this.coinDropAnim;
      this.GlowanimToUse = this.coinGlowDropAnim;
    }
    this.coinSpawnObject.SetActive(true);
    this.coin.SetActive(true);
    this.coinGlow.SetActive(true);
    this.coinSpawnObject.GetComponent<Animation>().Stop(this.coinSpawnAnim);
    this.coin.GetComponent<Animation>().Stop(this.animToUse);
    this.coinGlow.GetComponent<Animation>().Stop(this.GlowanimToUse);
    this.coinSpawnObject.GetComponent<Animation>().Play(this.coinSpawnAnim);
    this.coin.GetComponent<Animation>().Play(this.animToUse);
    this.coinGlow.GetComponent<Animation>().Play(this.GlowanimToUse);
  }
}
