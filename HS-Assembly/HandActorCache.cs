﻿// Decompiled with JetBrains decompiler
// Type: HandActorCache
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class HandActorCache
{
  private readonly TAG_CARDTYPE[] ACTOR_CARD_TYPES = new TAG_CARDTYPE[4]{ TAG_CARDTYPE.MINION, TAG_CARDTYPE.SPELL, TAG_CARDTYPE.WEAPON, TAG_CARDTYPE.HERO };
  private Map<HandActorCache.ActorKey, Actor> m_actorMap = new Map<HandActorCache.ActorKey, Actor>();
  private List<HandActorCache.ActorLoadedListener> m_loadedListeners = new List<HandActorCache.ActorLoadedListener>();

  public void Initialize()
  {
    foreach (TAG_CARDTYPE tagCardtype in this.ACTOR_CARD_TYPES)
    {
      foreach (int num in Enum.GetValues(typeof (TAG_PREMIUM)))
      {
        TAG_PREMIUM tagPremium = (TAG_PREMIUM) num;
        AssetLoader.Get().LoadActor(ActorNames.GetHeroSkinOrHandActor(tagCardtype, tagPremium), new AssetLoader.GameObjectCallback(this.OnActorLoaded), (object) this.MakeActorKey(tagCardtype, tagPremium), false);
      }
    }
  }

  public bool IsInitializing()
  {
    foreach (TAG_CARDTYPE cardType in this.ACTOR_CARD_TYPES)
    {
      foreach (int num in Enum.GetValues(typeof (TAG_PREMIUM)))
      {
        TAG_PREMIUM premiumType = (TAG_PREMIUM) num;
        if (!this.m_actorMap.ContainsKey(this.MakeActorKey(cardType, premiumType)))
          return true;
      }
    }
    return false;
  }

  public Actor GetActor(EntityDef entityDef, TAG_PREMIUM premium)
  {
    Actor actor;
    if (this.m_actorMap.TryGetValue(this.MakeActorKey(entityDef, premium), out actor))
      return actor;
    Debug.LogError((object) string.Format("HandActorCache.GetActor() - FAILED to get actor with cardType={0} premiumType={1}", (object) entityDef.GetCardType(), (object) premium));
    return (Actor) null;
  }

  public void AddActorLoadedListener(HandActorCache.ActorLoadedCallback callback)
  {
    this.AddActorLoadedListener(callback, (object) null);
  }

  public void AddActorLoadedListener(HandActorCache.ActorLoadedCallback callback, object userData)
  {
    HandActorCache.ActorLoadedListener actorLoadedListener = new HandActorCache.ActorLoadedListener();
    actorLoadedListener.SetCallback(callback);
    actorLoadedListener.SetUserData(userData);
    if (this.m_loadedListeners.Contains(actorLoadedListener))
      return;
    this.m_loadedListeners.Add(actorLoadedListener);
  }

  public bool RemoveActorLoadedListener(HandActorCache.ActorLoadedCallback callback)
  {
    return this.RemoveActorLoadedListener(callback, (object) null);
  }

  public bool RemoveActorLoadedListener(HandActorCache.ActorLoadedCallback callback, object userData)
  {
    HandActorCache.ActorLoadedListener actorLoadedListener = new HandActorCache.ActorLoadedListener();
    actorLoadedListener.SetCallback(callback);
    actorLoadedListener.SetUserData(userData);
    return this.m_loadedListeners.Remove(actorLoadedListener);
  }

  private HandActorCache.ActorKey MakeActorKey(EntityDef entityDef, TAG_PREMIUM premium)
  {
    return this.MakeActorKey(entityDef.GetCardType(), premium);
  }

  private HandActorCache.ActorKey MakeActorKey(TAG_CARDTYPE cardType, TAG_PREMIUM premiumType)
  {
    return new HandActorCache.ActorKey() { m_cardType = cardType, m_premiumType = premiumType };
  }

  private void OnActorLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      Debug.LogWarning((object) string.Format("HandActorCache.OnActorLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      Actor component = go.GetComponent<Actor>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        Debug.LogWarning((object) string.Format("HandActorCache.OnActorLoaded() - ERROR \"{0}\" has no Actor component", (object) name));
      }
      else
      {
        go.transform.position = new Vector3(-99999f, -99999f, 99999f);
        this.m_actorMap.Add((HandActorCache.ActorKey) callbackData, component);
        this.FireActorLoadedListeners(name, component);
      }
    }
  }

  private void FireActorLoadedListeners(string name, Actor actor)
  {
    foreach (HandActorCache.ActorLoadedListener actorLoadedListener in this.m_loadedListeners.ToArray())
      actorLoadedListener.Fire(name, actor);
  }

  private class ActorLoadedListener : EventListener<HandActorCache.ActorLoadedCallback>
  {
    public void Fire(string name, Actor actor)
    {
      this.m_callback(name, actor, this.m_userData);
    }
  }

  private class ActorKey
  {
    public TAG_CARDTYPE m_cardType;
    public TAG_PREMIUM m_premiumType;

    public static bool operator ==(HandActorCache.ActorKey a, HandActorCache.ActorKey b)
    {
      if (object.ReferenceEquals((object) a, (object) b))
        return true;
      if ((object) a == null || (object) b == null)
        return false;
      return a.Equals(b);
    }

    public static bool operator !=(HandActorCache.ActorKey a, HandActorCache.ActorKey b)
    {
      return !(a == b);
    }

    public override bool Equals(object obj)
    {
      if (obj == null)
        return false;
      return this.Equals(obj as HandActorCache.ActorKey);
    }

    public bool Equals(HandActorCache.ActorKey other)
    {
      if ((object) other == null || this.m_cardType != other.m_cardType)
        return false;
      return this.m_premiumType == other.m_premiumType;
    }

    public override int GetHashCode()
    {
      return (23 * 17 + this.m_cardType.GetHashCode()) * 17 + this.m_premiumType.GetHashCode();
    }
  }

  public delegate void ActorLoadedCallback(string name, Actor actor, object userData);
}
