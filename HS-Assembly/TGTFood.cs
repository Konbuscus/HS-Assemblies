﻿// Decompiled with JetBrains decompiler
// Type: TGTFood
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class TGTFood : MonoBehaviour
{
  public float m_NewFoodDelay = 1f;
  public bool m_Phone;
  public GameObject m_Triangle;
  public Animator m_TriangleAnimator;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_TriangleSoundPrefab;
  public int m_StartingFoodIndex;
  public List<TGTFood.FoodItem> m_Food;
  public TGTFood.FoodItem m_Drink;
  private bool m_isAnimating;
  private int m_lastFoodIdx;
  private TGTFood.FoodItem m_CurrentFoodItem;
  private float m_phoneNextCheckTime;

  private void Start()
  {
    this.m_CurrentFoodItem = this.m_Food[this.m_StartingFoodIndex];
    this.m_lastFoodIdx = this.m_StartingFoodIndex;
    this.m_CurrentFoodItem.m_FSM.gameObject.SetActive(true);
    this.m_CurrentFoodItem.m_FSM.SendEvent("Birth");
    this.m_Drink.m_FSM.gameObject.SetActive(true);
    this.m_Drink.m_FSM.SendEvent("Birth");
    if (!this.m_Phone)
      return;
    this.m_Triangle.SetActive(false);
  }

  private void Update()
  {
    this.HandleHits();
    if (!this.m_Phone || this.m_Triangle.activeSelf || (double) Time.timeSinceLevelLoad < (double) this.m_phoneNextCheckTime)
      return;
    this.m_phoneNextCheckTime = Time.timeSinceLevelLoad + 0.25f;
    if (this.m_CurrentFoodItem.m_FSM.FsmVariables.FindFsmBool("isEmpty").Value && this.m_Drink.m_FSM.FsmVariables.FindFsmBool("isEmpty").Value && !this.m_isAnimating)
    {
      this.m_Triangle.SetActive(true);
    }
    else
    {
      if (!this.m_Triangle.activeSelf)
        return;
      this.m_Triangle.SetActive(false);
    }
  }

  private void HandleHits()
  {
    if (!UniversalInputManager.Get().GetMouseButtonUp(0) || !this.IsOver(this.m_Triangle) || this.m_isAnimating)
      return;
    this.StartCoroutine(this.RingTheBell());
  }

  [DebuggerHidden]
  private IEnumerator RingTheBell()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTFood.\u003CRingTheBell\u003Ec__Iterator14()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void BellAnimation()
  {
    if (!this.m_Phone)
      this.m_TriangleAnimator.SetTrigger("Clicked");
    if (string.IsNullOrEmpty(this.m_TriangleSoundPrefab))
      return;
    string name = FileUtils.GameAssetPathToName(this.m_TriangleSoundPrefab);
    if (string.IsNullOrEmpty(name))
      return;
    SoundManager.Get().LoadAndPlay(name, this.m_Triangle);
  }

  private bool IsOver(GameObject go)
  {
    return (bool) ((UnityEngine.Object) go) && InputUtil.IsPlayMakerMouseInputAllowed(go) && UniversalInputManager.Get().InputIsOver(go);
  }

  [Serializable]
  public class FoodItem
  {
    public PlayMakerFSM m_FSM;
  }
}
