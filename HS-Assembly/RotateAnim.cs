﻿// Decompiled with JetBrains decompiler
// Type: RotateAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RotateAnim : MonoBehaviour
{
  private Quaternion targetRotation;
  private bool gogogo;
  private float timeValue;
  private float timePassed;
  private float startingAngle;

  private void Start()
  {
  }

  private void Update()
  {
    if (!this.gogogo)
      return;
    this.timePassed = this.timePassed + Time.deltaTime;
    float timePassed = this.timePassed;
    float startingAngle = this.startingAngle;
    float num = startingAngle - Quaternion.Angle(this.transform.rotation, this.targetRotation);
    float timeValue = this.timeValue;
    this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, this.targetRotation, (num * (float) (-(double) Mathf.Pow(2f, -10f * timePassed / timeValue) + 1.0) + startingAngle) * Time.deltaTime);
    if ((double) Quaternion.Angle(this.transform.rotation, this.targetRotation) > (double) Mathf.Epsilon)
      return;
    this.gogogo = false;
    Object.Destroy((Object) this);
  }

  public void SetTargetRotation(Vector3 target, float timeValueInput)
  {
    this.targetRotation = Quaternion.Euler(target);
    this.gogogo = true;
    this.timeValue = timeValueInput;
    this.startingAngle = Quaternion.Angle(this.transform.rotation, this.targetRotation);
  }
}
