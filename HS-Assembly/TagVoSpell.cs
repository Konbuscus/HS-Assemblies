﻿// Decompiled with JetBrains decompiler
// Type: TagVoSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class TagVoSpell : CardSoundSpell
{
  public TagVoData m_TagVoData = new TagVoData();

  public override AudioSource DetermineBestAudioSource()
  {
    if (this.CanPlayTagVo())
      return this.m_TagVoData.m_AudioSource;
    return base.DetermineBestAudioSource();
  }

  private bool CanPlayTagVo()
  {
    if (this.m_TagVoData.m_TagRequirements.Count == 0)
      return false;
    Card sourceCard = this.GetSourceCard();
    if ((Object) sourceCard == (Object) null)
      return false;
    Entity entity = sourceCard.GetEntity();
    using (List<TagVoRequirement>.Enumerator enumerator = this.m_TagVoData.m_TagRequirements.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TagVoRequirement current = enumerator.Current;
        if (entity.GetTag(current.m_Tag) != current.m_Value)
          return false;
      }
    }
    return true;
  }
}
