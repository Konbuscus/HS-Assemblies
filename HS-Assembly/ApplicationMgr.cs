﻿// Decompiled with JetBrains decompiler
// Type: ApplicationMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using Networking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
using WTCG.BI;

public class ApplicationMgr : MonoBehaviour
{
  public static readonly PlatformDependentValue<bool> CanQuitGame = new PlatformDependentValue<bool>(PlatformCategory.OS) { PC = true, Mac = true, Android = false, iOS = false };
  public static readonly PlatformDependentValue<bool> AllowResetFromFatalError = new PlatformDependentValue<bool>(PlatformCategory.OS) { PC = false, Mac = false, Android = true, iOS = true };
  private bool m_focused = true;
  private List<ApplicationMgr.FocusChangedListener> m_focusChangedListeners = new List<ApplicationMgr.FocusChangedListener>();
  private float m_lastResumeTime = -999999f;
  private const ApplicationMode DEFAULT_MODE = ApplicationMode.INTERNAL;
  private const float AUTO_RESET_ON_ERROR_TIMEOUT = 1f;
  private static ApplicationMgr s_instance;
  private static ApplicationMode s_mode;
  private bool m_exiting;
  private float m_lastPauseTime;
  private bool m_hasResetSinceLastResume;
  private bool m_resetting;
  private float m_lastResetTime;
  private LinkedList<ApplicationMgr.SchedulerContext> m_schedulerContexts;

  public event Action WillReset;

  public event Action Resetting;

  public event Action Paused;

  public event Action Unpaused;

  private void Awake()
  {
    ApplicationMgr.s_instance = this;
    this.Initialize();
  }

  private void OnDestroy()
  {
  }

  private void Start()
  {
    AutomationInterpretor.Get().Start();
  }

  private void Update()
  {
    this.ProcessScheduledCallbacks();
    Network.Heartbeat();
    AutomationInterpretor.Get().Update();
  }

  private void OnGUI()
  {
    UnityEngine.Debug.developerConsoleVisible = false;
    BugReporter.OnGUI();
  }

  private void OnApplicationQuit()
  {
    UberText.StoreCachedData();
    Network.AppQuit();
    W8Touch.AppQuit();
    this.UnloadUnusedAssets();
  }

  private void OnApplicationFocus(bool focus)
  {
    if (this.m_focused == focus)
      return;
    this.m_focused = focus;
    this.FireFocusChangedEvent();
  }

  private void OnApplicationPause(bool pauseStatus)
  {
    if (Time.frameCount == 0)
      return;
    if (pauseStatus)
    {
      this.m_lastPauseTime = Time.realtimeSinceStartup;
      if (this.Paused != null)
        this.Paused();
      UberText.StoreCachedData();
      Network.ApplicationPaused();
    }
    else
    {
      this.m_hasResetSinceLastResume = false;
      float num = Time.realtimeSinceStartup - this.m_lastPauseTime;
      UnityEngine.Debug.Log((object) ("Time spent paused: " + (object) num));
      if (DemoMgr.Get().GetMode() == DemoMode.APPLE_STORE && (double) num > 180.0)
        this.ResetImmediately(false, false);
      this.m_lastResumeTime = Time.realtimeSinceStartup;
      if (this.Unpaused != null)
        this.Unpaused();
      Network.ApplicationUnpaused();
      if (!SceneMgr.Get().IsModeRequested(SceneMgr.Mode.FATAL_ERROR))
        return;
      this.ResetImmediately(false, false);
    }
  }

  public static ApplicationMgr Get()
  {
    return ApplicationMgr.s_instance;
  }

  public static ApplicationMode GetMode()
  {
    ApplicationMgr.InitializeMode();
    return ApplicationMgr.s_mode;
  }

  public static bool IsInternal()
  {
    return ApplicationMgr.GetMode() == ApplicationMode.INTERNAL;
  }

  public static bool IsPublic()
  {
    return ApplicationMgr.GetMode() == ApplicationMode.PUBLIC;
  }

  public static bool UseDevWorkarounds()
  {
    return Application.isEditor || ApplicationMgr.UsingStandaloneLocalData();
  }

  public static MobileEnv GetMobileEnvironment()
  {
    string str = Vars.Key("Mobile.Mode").GetStr("undefined");
    if (str == "undefined")
      str = "Production";
    return str == "Production" ? MobileEnv.PRODUCTION : MobileEnv.DEVELOPMENT;
  }

  public static AndroidStore GetAndroidStore()
  {
    return AndroidStore.NONE;
  }

  public bool IsResetting()
  {
    return this.m_resetting;
  }

  public void Reset()
  {
    this.StartCoroutine(this.WaitThenReset(false, false));
  }

  public void ResetAndForceLogin()
  {
    this.StartCoroutine(this.WaitThenReset(true, false));
  }

  public void ResetAndGoBackToNoAccountTutorial()
  {
    this.StartCoroutine(this.WaitThenReset(false, true));
  }

  public bool ResetOnErrorIfNecessary()
  {
    if (this.m_hasResetSinceLastResume || (double) Time.realtimeSinceStartup >= (double) this.m_lastResumeTime + 1.0)
      return false;
    this.StartCoroutine(this.WaitThenReset(false, false));
    return true;
  }

  public void Exit()
  {
    this.m_exiting = true;
    if (ErrorReporter.Get().busy)
      this.StartCoroutine(this.WaitThenExit());
    else
      GeneralUtils.ExitApplication();
  }

  public bool IsExiting()
  {
    return this.m_exiting;
  }

  public bool HasFocus()
  {
    return this.m_focused;
  }

  public bool AddFocusChangedListener(ApplicationMgr.FocusChangedCallback callback)
  {
    return this.AddFocusChangedListener(callback, (object) null);
  }

  public bool AddFocusChangedListener(ApplicationMgr.FocusChangedCallback callback, object userData)
  {
    ApplicationMgr.FocusChangedListener focusChangedListener = new ApplicationMgr.FocusChangedListener();
    focusChangedListener.SetCallback(callback);
    focusChangedListener.SetUserData(userData);
    if (this.m_focusChangedListeners.Contains(focusChangedListener))
      return false;
    this.m_focusChangedListeners.Add(focusChangedListener);
    return true;
  }

  public bool RemoveFocusChangedListener(ApplicationMgr.FocusChangedCallback callback)
  {
    return this.RemoveFocusChangedListener(callback, (object) null);
  }

  public bool RemoveFocusChangedListener(ApplicationMgr.FocusChangedCallback callback, object userData)
  {
    ApplicationMgr.FocusChangedListener focusChangedListener = new ApplicationMgr.FocusChangedListener();
    focusChangedListener.SetCallback(callback);
    focusChangedListener.SetUserData(userData);
    return this.m_focusChangedListeners.Remove(focusChangedListener);
  }

  public float LastResetTime()
  {
    return this.m_lastResetTime;
  }

  public void UnloadUnusedAssets()
  {
    Resources.UnloadUnusedAssets();
  }

  public bool ScheduleCallback(float secondsToWait, bool realTime, ApplicationMgr.ScheduledCallback cb, object userData = null)
  {
    if (!GeneralUtils.IsCallbackValid((Delegate) cb))
      return false;
    if (this.m_schedulerContexts == null)
    {
      this.m_schedulerContexts = new LinkedList<ApplicationMgr.SchedulerContext>();
    }
    else
    {
      using (LinkedList<ApplicationMgr.SchedulerContext>.Enumerator enumerator = this.m_schedulerContexts.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ApplicationMgr.SchedulerContext current = enumerator.Current;
          if (!((MulticastDelegate) current.m_callback != (MulticastDelegate) cb) && current.m_userData == userData)
            return false;
        }
      }
    }
    ApplicationMgr.SchedulerContext schedulerContext = new ApplicationMgr.SchedulerContext();
    schedulerContext.m_startTime = Time.realtimeSinceStartup;
    schedulerContext.m_secondsToWait = secondsToWait;
    schedulerContext.m_realTime = realTime;
    schedulerContext.m_callback = cb;
    schedulerContext.m_userData = userData;
    float num = schedulerContext.EstimateTargetTime();
    bool flag = false;
    for (LinkedListNode<ApplicationMgr.SchedulerContext> node = this.m_schedulerContexts.Last; node != null; node = node.Previous)
    {
      if ((double) node.Value.EstimateTargetTime() <= (double) num)
      {
        flag = true;
        this.m_schedulerContexts.AddAfter(node, schedulerContext);
        break;
      }
    }
    if (!flag)
      this.m_schedulerContexts.AddFirst(schedulerContext);
    return true;
  }

  public bool CancelScheduledCallback(ApplicationMgr.ScheduledCallback cb, object userData = null)
  {
    if (!GeneralUtils.IsCallbackValid((Delegate) cb) || this.m_schedulerContexts == null || this.m_schedulerContexts.Count == 0)
      return false;
    for (LinkedListNode<ApplicationMgr.SchedulerContext> node = this.m_schedulerContexts.First; node != null; node = node.Next)
    {
      ApplicationMgr.SchedulerContext schedulerContext = node.Value;
      if ((MulticastDelegate) schedulerContext.m_callback == (MulticastDelegate) cb && schedulerContext.m_userData == userData)
      {
        this.m_schedulerContexts.Remove(node);
        return true;
      }
    }
    return false;
  }

  private void ProcessScheduledCallbacks()
  {
    if (this.m_schedulerContexts == null || this.m_schedulerContexts.Count == 0)
      return;
    LinkedList<ApplicationMgr.SchedulerContext> linkedList = (LinkedList<ApplicationMgr.SchedulerContext>) null;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    LinkedListNode<ApplicationMgr.SchedulerContext> node = this.m_schedulerContexts.First;
    while (node != null)
    {
      ApplicationMgr.SchedulerContext schedulerContext = node.Value;
      if (schedulerContext.m_realTime)
        schedulerContext.m_secondsWaited = realtimeSinceStartup - schedulerContext.m_startTime;
      else
        schedulerContext.m_secondsWaited += Time.deltaTime;
      if ((double) schedulerContext.m_secondsWaited >= (double) schedulerContext.m_secondsToWait)
      {
        if (linkedList == null)
          linkedList = new LinkedList<ApplicationMgr.SchedulerContext>();
        linkedList.AddLast(schedulerContext);
        LinkedListNode<ApplicationMgr.SchedulerContext> next = node.Next;
        this.m_schedulerContexts.Remove(node);
        node = next;
      }
      else if (!GeneralUtils.IsCallbackValid((Delegate) schedulerContext.m_callback))
      {
        LinkedListNode<ApplicationMgr.SchedulerContext> next = node.Next;
        this.m_schedulerContexts.Remove(node);
        node = next;
      }
      else
        node = node.Next;
    }
    if (linkedList == null)
      return;
    using (LinkedList<ApplicationMgr.SchedulerContext>.Enumerator enumerator = linkedList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ApplicationMgr.SchedulerContext current = enumerator.Current;
        current.m_callback(current.m_userData);
      }
    }
  }

  public static bool UsingStandaloneLocalData()
  {
    return !string.IsNullOrEmpty(ApplicationMgr.GetStandaloneLocalDataPath());
  }

  public static bool TryGetStandaloneLocalDataPath(string subPath, out string outPath)
  {
    string standaloneLocalDataPath = ApplicationMgr.GetStandaloneLocalDataPath();
    if (!string.IsNullOrEmpty(standaloneLocalDataPath))
    {
      outPath = Path.Combine(standaloneLocalDataPath, subPath);
      return true;
    }
    outPath = (string) null;
    return false;
  }

  private static string GetStandaloneLocalDataPath()
  {
    return (string) null;
  }

  private static void InitializeMode()
  {
    if (ApplicationMgr.s_mode != ApplicationMode.INVALID)
      return;
    ApplicationMgr.s_mode = ApplicationMode.PUBLIC;
  }

  private void Initialize()
  {
    LogArchive.Init();
    ApplicationMgr.InitializeMode();
    this.InitializeUnity();
    this.InitializeGame();
    this.InitializeWindowTitle();
    this.InitializeOptionValues();
    this.WillReset += new Action(GameStrings.WillReset);
  }

  private void InitializeUnity()
  {
    Application.runInBackground = true;
    Application.targetFrameRate = 30;
    Application.backgroundLoadingPriority = ThreadPriority.Low;
  }

  private void InitializeGame()
  {
    BugReporter.Init();
    DownloadableDbfCache.Initialize();
    DemoMgr.Get().Initialize();
    LocalOptions.Get().Initialize();
    if (DemoMgr.Get().GetMode() == DemoMode.APPLE_STORE)
      DemoMgr.Get().ApplyAppleStoreDemoDefaults();
    if ((bool) Network.TUTORIALS_WITHOUT_ACCOUNT)
      Network.SetShouldBeConnectedToAurora(Options.Get().GetBool(Option.CONNECT_TO_AURORA));
    Localization.Initialize();
    GameStrings.LoadAll();
    GameDbf.Load(false);
    IDebugConnectionManager debugConnectionManager = (IDebugConnectionManager) new DebugConnectionManager();
    Network.Initialize((IDispatcher) new QueueDispatcher(debugConnectionManager, (IClientRequestManager) new ClientRequestManager(), (IPacketDecoderManager) new PacketDecoderManager(debugConnectionManager.AllowDebugConnections())));
    if (!PlayErrors.Init())
      UnityEngine.Debug.LogError((object) string.Format("{0} failed to load!", (object) "PlayErrors32"));
    GameMgr.Get().Initialize();
    ChangedCardMgr.Get().Initialize();
    TavernBrawlManager.Init();
    AdventureProgressMgr.Init();
    AchieveManager.Init();
    AccountLicenseMgr.Init();
    FixedRewardsMgr.Initialize();
    ReturningPlayerMgr.Initialize();
  }

  [DllImport("user32.dll")]
  public static extern int SetWindowTextW(IntPtr hWnd, [MarshalAs(UnmanagedType.LPWStr)] string text);

  [DllImport("user32.dll")]
  public static extern IntPtr FindWindow(string className, string windowName);

  private void InitializeWindowTitle()
  {
    IntPtr window = ApplicationMgr.FindWindow((string) null, "Hearthstone");
    if (!(window != IntPtr.Zero))
      return;
    ApplicationMgr.SetWindowTextW(window, GameStrings.Get("GLOBAL_PROGRAMNAME_HEARTHSTONE"));
  }

  private void InitializeOptionValues()
  {
    if (!ApplicationMgr.IsPublic())
      return;
    Options.Get().SetOption(Option.SOUND, OptionDataTables.s_defaultsMap[Option.SOUND]);
    Options.Get().SetOption(Option.MUSIC, OptionDataTables.s_defaultsMap[Option.MUSIC]);
  }

  [DebuggerHidden]
  private IEnumerator WaitThenReset(bool forceLogin, bool forceNoAccountTutorial = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ApplicationMgr.\u003CWaitThenReset\u003Ec__Iterator2D8() { forceLogin = forceLogin, forceNoAccountTutorial = forceNoAccountTutorial, \u003C\u0024\u003EforceLogin = forceLogin, \u003C\u0024\u003EforceNoAccountTutorial = forceNoAccountTutorial, \u003C\u003Ef__this = this };
  }

  private void ResetImmediately(bool forceLogin, bool forceNoAccountTutorial = false)
  {
    Log.Reset.Print("ApplicationMgr.ResetImmediately - forceLogin? " + (object) forceLogin + "  Stack trace: " + Environment.StackTrace);
    BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, !forceLogin ? BIReport.TelemetryEvent.EVENT_ON_RESET : BIReport.TelemetryEvent.EVENT_ON_RESET_WITH_LOGIN);
    GameDbf.Load(false);
    if (this.WillReset != null)
      this.WillReset();
    this.m_resetting = true;
    this.m_lastResetTime = Time.realtimeSinceStartup;
    if ((UnityEngine.Object) DialogManager.Get() != (UnityEngine.Object) null)
      DialogManager.Get().ClearAllImmediately();
    if ((bool) Network.TUTORIALS_WITHOUT_ACCOUNT)
      Network.SetShouldBeConnectedToAurora(forceLogin || Options.Get().GetBool(Option.CONNECT_TO_AURORA));
    FatalErrorMgr.Get().ClearAllErrors();
    this.m_hasResetSinceLastResume = true;
    if (forceNoAccountTutorial)
    {
      Options.Get().SetBool(Option.CONNECT_TO_AURORA, false);
      Network.SetShouldBeConnectedToAurora(false);
    }
    if (this.Resetting != null)
      this.Resetting();
    Network.Reset();
    Navigation.Clear();
    this.m_resetting = false;
    Log.Reset.Print("\tApplicationMgr.ResetImmediately completed");
  }

  [DebuggerHidden]
  private IEnumerator WaitThenExit()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ApplicationMgr.\u003CWaitThenExit\u003Ec__Iterator2D9 exitCIterator2D9 = new ApplicationMgr.\u003CWaitThenExit\u003Ec__Iterator2D9();
    return (IEnumerator) exitCIterator2D9;
  }

  private void FireFocusChangedEvent()
  {
    foreach (ApplicationMgr.FocusChangedListener focusChangedListener in this.m_focusChangedListeners.ToArray())
      focusChangedListener.Fire(this.m_focused);
  }

  private class FocusChangedListener : EventListener<ApplicationMgr.FocusChangedCallback>
  {
    public void Fire(bool focused)
    {
      this.m_callback(focused, this.m_userData);
    }
  }

  private class SchedulerContext
  {
    public float m_startTime;
    public float m_secondsToWait;
    public bool m_realTime;
    public ApplicationMgr.ScheduledCallback m_callback;
    public object m_userData;
    public float m_secondsWaited;

    public float EstimateTargetTime()
    {
      return this.m_startTime + (!this.m_realTime ? this.m_secondsToWait * Time.timeScale : this.m_secondsToWait);
    }
  }

  public delegate void FocusChangedCallback(bool focused, object userData);

  public delegate void ScheduledCallback(object userData);
}
