﻿// Decompiled with JetBrains decompiler
// Type: Carousel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class Carousel : MonoBehaviour
{
  public int m_maxPosition = 4;
  private float[] m_momentumHistory = new float[5];
  private const float MIN_VELOCITY = 0.03f;
  private const float DRAG = 0.015f;
  private const float SCROLL_THRESHOLD = 10f;
  public GameObject[] m_bones;
  public Collider m_collider;
  public bool m_useFlyIn;
  public bool m_trackItemHit;
  public bool m_noMouseMovement;
  private int m_intPosition;
  private float m_position;
  private float m_targetPosition;
  private CarouselItem[] m_items;
  private int m_numItems;
  private int m_radius;
  private bool m_touchActive;
  private Vector2 m_touchStart;
  private float m_startX;
  private float m_touchX;
  private float m_velocity;
  private int m_hitIndex;
  private CarouselItem m_hitItem;
  private Vector3 m_hitWorldPosition;
  private float m_hitCarouselPosition;
  private float m_totalMove;
  private float m_moveAdjustment;
  private int m_momentumCounter;
  private int m_momentumTotal;
  private bool m_doFlyIn;
  private float m_flyInState;
  private bool m_settleCallbackCalled;
  private bool m_scrolling;
  private Carousel.ItemPulled m_itemPulledListener;
  private Carousel.ItemClicked m_itemClickedListener;
  private Carousel.ItemReleased m_itemReleasedListener;
  private Carousel.CarouselSettled m_carouselSettledListener;
  private Carousel.CarouselStartedScrolling m_carouselStartedScrollingListener;

  private void Awake()
  {
    this.m_numItems = this.m_bones.Length;
    this.m_radius = this.m_numItems / 2;
  }

  private void Start()
  {
    this.m_trackItemHit = true;
  }

  private void OnDestroy()
  {
    this.m_itemPulledListener = (Carousel.ItemPulled) null;
    this.m_itemClickedListener = (Carousel.ItemClicked) null;
    this.m_itemReleasedListener = (Carousel.ItemReleased) null;
    this.m_carouselSettledListener = (Carousel.CarouselSettled) null;
    this.m_carouselStartedScrollingListener = (Carousel.CarouselStartedScrolling) null;
  }

  public void UpdateUI(bool mouseDown)
  {
    if (this.m_items == null)
      return;
    bool flag = (double) this.m_position < 0.0 || (double) this.m_position > (double) this.m_maxPosition;
    if (this.m_touchActive)
    {
      float x = Input.mousePosition.x;
      float num1 = x - this.m_touchX;
      if (!this.m_scrolling && (double) Math.Abs(this.m_touchStart.x - x) >= 10.0)
        this.StartScrolling();
      float num2 = num1 * 4.5f / (float) Screen.width;
      if ((double) this.m_position < 0.0)
        num2 /= (float) (1.0 + (double) Math.Abs(this.m_position) * 5.0);
      if (!this.m_noMouseMovement)
      {
        if (this.m_trackItemHit)
        {
          Vector3 point;
          UniversalInputManager.Get().GetInputPointOnPlane(this.m_hitWorldPosition, out point);
          this.m_position = this.m_startX + this.m_hitCarouselPosition - this.GetCarouselPosition(point.x);
        }
        else
          this.m_position -= num2;
      }
      this.m_momentumHistory[this.m_momentumCounter] = num2;
      ++this.m_momentumCounter;
      ++this.m_momentumTotal;
      flag = (double) this.m_position < 0.0 || (double) this.m_position > (double) this.m_maxPosition;
      if (this.m_momentumCounter >= this.m_momentumHistory.Length)
        this.m_momentumCounter = 0;
      if (this.m_momentumTotal >= this.m_momentumHistory.Length)
        this.m_momentumTotal = this.m_momentumHistory.Length;
      this.m_touchX = x;
      if (this.m_itemPulledListener != null && (double) Input.mousePosition.y - (double) this.m_touchStart.y > (double) ((float) Screen.height * 0.1f) && (double) Input.mousePosition.y > (double) ((float) Screen.height * 0.275f))
      {
        this.m_itemPulledListener(this.m_hitItem, this.m_hitIndex);
        this.m_touchActive = false;
        this.m_velocity = 0.0f;
        this.SettlePosition(0.0f);
      }
      if (!Input.GetMouseButton(0))
      {
        if (!this.m_noMouseMovement && this.m_scrolling)
        {
          this.m_velocity = this.CalculateVelocity();
          if ((double) this.m_position < 0.0)
          {
            this.m_targetPosition = 0.0f;
            this.m_velocity = 0.0f;
          }
          else if ((double) this.m_position >= (double) this.m_maxPosition)
          {
            this.m_targetPosition = (float) this.m_maxPosition;
            this.m_velocity = 0.0f;
          }
          else if ((double) Math.Abs(this.m_velocity) < 0.0299999993294477)
          {
            this.SettlePosition(this.m_velocity);
            this.m_velocity = 0.0f;
          }
        }
        if (this.m_itemReleasedListener != null)
          this.m_itemReleasedListener();
        this.m_touchActive = false;
      }
    }
    CarouselItem itemHit;
    int index = this.MouseHit(out itemHit);
    if (mouseDown && index >= 0)
    {
      this.m_touchActive = true;
      this.m_touchX = Input.mousePosition.x;
      this.m_touchStart = (Vector2) Input.mousePosition;
      this.m_velocity = 0.0f;
      this.m_hitIndex = index;
      this.m_hitItem = itemHit;
      this.m_scrolling = false;
      this.m_settleCallbackCalled = false;
      if (this.m_trackItemHit)
      {
        RaycastHit hitInfo;
        UniversalInputManager.Get().GetInputHitInfo(out hitInfo);
        this.m_hitWorldPosition = hitInfo.point;
        this.m_hitCarouselPosition = this.GetCarouselPosition(this.m_hitWorldPosition.x);
        this.m_startX = this.m_position;
      }
      this.InitVelocity();
      if (this.m_itemClickedListener != null)
        this.m_itemClickedListener(this.m_hitItem, index);
    }
    if (!this.m_touchActive && (double) this.m_velocity != 0.0)
    {
      if ((double) Math.Abs(this.m_velocity) < 0.0299999993294477 || flag)
      {
        this.SettlePosition(this.m_velocity);
        this.m_velocity = 0.0f;
      }
      else
      {
        this.m_position += this.m_velocity;
        this.m_velocity -= 0.015f * (float) Math.Sign(this.m_velocity);
      }
    }
    if (!this.m_touchActive && (double) this.m_targetPosition != (double) this.m_position && (double) this.m_velocity == 0.0)
    {
      this.m_position = (float) ((double) this.m_targetPosition * 0.150000005960464 + (double) this.m_position * 0.850000023841858);
      if (!this.m_settleCallbackCalled && (double) Math.Abs(this.m_position - this.m_targetPosition) < 0.100000001490116)
      {
        this.m_settleCallbackCalled = true;
        if (this.m_carouselSettledListener != null)
          this.m_carouselSettledListener();
      }
      if ((double) Math.Abs(this.m_position - this.m_targetPosition) < 0.00999999977648258)
      {
        this.m_position = this.m_targetPosition;
        this.m_scrolling = false;
      }
    }
    this.m_intPosition = (int) Mathf.Round(this.m_position);
    this.UpdateVisibleItems();
    if (!this.m_doFlyIn)
      return;
    this.m_flyInState += Math.Min(0.03f, Time.deltaTime) * 8f;
    if ((double) this.m_flyInState <= (double) this.m_bones.Length)
      return;
    this.m_doFlyIn = false;
  }

  public bool MouseOver()
  {
    CarouselItem itemHit;
    return this.MouseHit(out itemHit) >= 0;
  }

  private float GetCarouselPosition(float x)
  {
    if ((double) x < (double) this.m_bones[0].transform.position.x)
      return 0.0f;
    if ((double) x > (double) this.m_bones[this.m_bones.Length - 1].transform.position.x)
      return (float) this.m_bones.Length - 1f;
    float num = this.m_bones[0].transform.position.x;
    for (int index = 1; index < this.m_bones.Length; ++index)
    {
      float x1 = this.m_bones[index].transform.position.x;
      if ((double) x >= (double) num && (double) x <= (double) x1)
        return (float) index + (float) (((double) x - (double) num) / ((double) x1 - (double) num));
      num = x1;
    }
    return 0.0f;
  }

  private int MouseHit(out CarouselItem itemHit)
  {
    int num = -1;
    itemHit = (CarouselItem) null;
    if (this.m_items == null || this.m_items.Length <= 0)
      return -1;
    for (int index = 0; index < this.m_items.Length; ++index)
    {
      CarouselItem carouselItem = this.m_items[index];
      RaycastHit hitInfo;
      if ((UnityEngine.Object) carouselItem.GetGameObject() != (UnityEngine.Object) null && UniversalInputManager.Get().InputIsOver(carouselItem.GetGameObject(), out hitInfo))
      {
        itemHit = carouselItem;
        num = index;
        break;
      }
    }
    return num;
  }

  public void SetListeners(Carousel.ItemPulled pulled, Carousel.ItemClicked clicked, Carousel.ItemReleased released, Carousel.CarouselSettled settled = null, Carousel.CarouselStartedScrolling scrolling = null)
  {
    this.m_itemPulledListener = pulled;
    this.m_itemClickedListener = clicked;
    this.m_itemReleasedListener = released;
    this.m_carouselSettledListener = settled;
    this.m_carouselStartedScrollingListener = scrolling;
  }

  private void InitVelocity()
  {
    for (int index = 0; index < this.m_momentumHistory.Length; ++index)
      this.m_momentumHistory[index] = 0.0f;
    this.m_momentumCounter = 0;
    this.m_momentumTotal = 0;
  }

  private float CalculateVelocity()
  {
    float num = 0.0f;
    for (int index = 0; index < this.m_momentumTotal; ++index)
      num += this.m_momentumHistory[index];
    return -0.9f * num / (float) this.m_momentumTotal;
  }

  private float DistanceFromSettle()
  {
    float num = this.m_position - (float) (int) this.m_position;
    if ((double) num > 0.5)
      return 1f - num;
    return num;
  }

  private void SettlePosition(float bias = 0.0f)
  {
    this.m_targetPosition = Math.Min((float) this.m_maxPosition, Math.Max(0.0f, (double) bias <= 1.0 / 1000.0 ? ((double) bias >= -1.0 / 1000.0 ? Mathf.Round(this.m_position) : Mathf.Round(this.m_position - 0.5f)) : Mathf.Round(this.m_position + 0.5f)));
  }

  private void UpdateVisibleItems()
  {
    float position = this.m_position;
    int num1 = Mathf.FloorToInt(position);
    float num2 = position - (float) num1;
    float num3 = 1f - num2;
    int num4 = 0;
    for (int index1 = 0; index1 < this.m_items.Length; ++index1)
    {
      int index2 = index1 - num1 + this.m_radius - 1;
      int index3 = index1 - num1 + this.m_radius;
      if (index2 < 0 || index3 >= this.m_bones.Length)
      {
        this.m_items[index1].Hide();
      }
      else
      {
        this.m_items[index1].Show(this);
        if (this.m_items[index1].IsLoaded())
        {
          Vector3 localPosition1 = this.m_bones[index2].transform.localPosition;
          Vector3 localPosition2 = this.m_bones[index3].transform.localPosition;
          Vector3 localScale1 = this.m_bones[index2].transform.localScale;
          Vector3 localScale2 = this.m_bones[index3].transform.localScale;
          Quaternion localRotation1 = this.m_bones[index2].transform.localRotation;
          Quaternion localRotation2 = this.m_bones[index3].transform.localRotation;
          Vector3 vector3_1 = new Vector3((float) ((double) localPosition1.x * (double) num2 + (double) localPosition2.x * (double) num3), (float) ((double) localPosition1.y * (double) num2 + (double) localPosition2.y * (double) num3), (float) ((double) localPosition1.z * (double) num2 + (double) localPosition2.z * (double) num3));
          Vector3 vector3_2 = new Vector3((float) ((double) localScale1.x * (double) num2 + (double) localScale2.x * (double) num3), (float) ((double) localScale1.y * (double) num2 + (double) localScale2.y * (double) num3), (float) ((double) localScale1.z * (double) num2 + (double) localScale2.z * (double) num3));
          Quaternion quaternion = new Quaternion((float) ((double) localRotation1.x * (double) num2 + (double) localRotation2.x * (double) num3), (float) ((double) localRotation1.y * (double) num2 + (double) localRotation2.y * (double) num3), (float) ((double) localRotation1.z * (double) num2 + (double) localRotation2.z * (double) num3), (float) ((double) localRotation1.w * (double) num2 + (double) localRotation2.w * (double) num3));
          if (this.m_doFlyIn)
          {
            float num5 = 1f;
            if (num4 >= (int) this.m_flyInState + 1)
              num5 = 0.0f;
            else if (num4 >= (int) this.m_flyInState)
              num5 = this.m_flyInState - (float) Math.Floor((double) this.m_flyInState);
            float num6 = 1f - num5;
            Vector3 vector3_3 = new Vector3(81f, 9.4f, 4f);
            this.m_items[index1].GetGameObject().transform.localPosition = new Vector3((float) ((double) num5 * (double) vector3_1.x + (double) num6 * (double) vector3_3.x), (float) ((double) num5 * (double) vector3_1.y + (double) num6 * (double) vector3_3.y), (float) ((double) num5 * (double) vector3_1.z + (double) num6 * (double) vector3_3.z));
          }
          else
            this.m_items[index1].GetGameObject().transform.localPosition = vector3_1;
          this.m_items[index1].GetGameObject().transform.localScale = vector3_2;
          this.m_items[index1].GetGameObject().transform.localRotation = quaternion;
        }
        ++num4;
      }
    }
  }

  public void Initialize(CarouselItem[] items, int position = 0)
  {
    if (this.m_items != null)
      this.ClearItems();
    this.m_items = items;
    this.m_position = this.m_targetPosition = (float) position;
    this.m_intPosition = position;
    this.DoFlyIn();
  }

  public void ClearItems()
  {
    if (this.m_items == null)
      return;
    foreach (CarouselItem carouselItem in this.m_items)
      carouselItem.Clear();
  }

  public bool AreVisibleItemsLoaded()
  {
    for (int index1 = 0; index1 < this.m_numItems; ++index1)
    {
      int index2 = this.m_intPosition + index1 - this.m_radius;
      if (index2 >= 0 && !this.m_items[index2].IsLoaded())
      {
        Debug.Log((object) ("Not loaded " + (object) index2));
        return false;
      }
    }
    return true;
  }

  public int GetCurrentIndex()
  {
    return this.m_intPosition;
  }

  public int GetTargetPosition()
  {
    return (int) this.m_targetPosition;
  }

  public CarouselItem GetCurrentItem()
  {
    if (this.m_items == null)
      return (CarouselItem) null;
    return this.m_items[this.m_intPosition];
  }

  public void SetPosition(int n, bool animate = false)
  {
    this.m_targetPosition = (float) n;
    if (!animate)
    {
      this.m_position = this.m_targetPosition;
    }
    else
    {
      this.StartScrolling();
      this.m_settleCallbackCalled = false;
    }
    this.DoFlyIn();
  }

  public bool IsScrolling()
  {
    return this.m_scrolling;
  }

  private void DoFlyIn()
  {
    if (!this.m_useFlyIn)
      return;
    this.m_doFlyIn = true;
    this.m_flyInState = 0.0f;
  }

  private void StartScrolling()
  {
    this.m_scrolling = true;
    if (this.m_carouselStartedScrollingListener == null)
      return;
    this.m_carouselStartedScrollingListener();
  }

  public delegate void ItemPulled(CarouselItem item, int index);

  public delegate void ItemClicked(CarouselItem item, int index);

  public delegate void ItemReleased();

  public delegate void CarouselSettled();

  public delegate void CarouselMoved();

  public delegate void CarouselStartedScrolling();
}
