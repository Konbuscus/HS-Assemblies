﻿// Decompiled with JetBrains decompiler
// Type: LocalOptions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class LocalOptions
{
  private LocalOptions.LineParser[] m_lineParsers = new LocalOptions.LineParser[2]{ new LocalOptions.LineParser() { m_pattern = "(?<key>[^\\:]+):(?<type>[bilds])=(?<value>.*)", m_callback = new LocalOptions.ParseLineCallback(LocalOptions.ParseLine_V1) }, new LocalOptions.LineParser() { m_pattern = "(?<key>[^=]+)=(?<value>.*)", m_callback = new LocalOptions.ParseLineCallback(LocalOptions.ParseLine_V2) } };
  private Map<string, object> m_options = new Map<string, object>();
  private List<string> m_sortedKeys = new List<string>();
  public const int MAX_OPTIONS_LINE_LENGTH = 4096;
  private const string OPTIONS_FILE_NAME = "options.txt";
  private const int LOAD_LINE_FAIL_THRESHOLD = 4;
  private static LocalOptions s_instance;
  private string m_path;
  private LocalOptions.LoadResult m_loadResult;
  private int m_currentLineVersion;
  private bool m_dirty;

  public static LocalOptions Get()
  {
    if (LocalOptions.s_instance == null)
      LocalOptions.s_instance = new LocalOptions();
    return LocalOptions.s_instance;
  }

  public void Initialize()
  {
    this.m_path = string.Format("{0}/{1}", (object) FileUtils.PersistentDataPath, (object) "options.txt");
    foreach (LocalOptions.LineParser lineParser in this.m_lineParsers)
      lineParser.m_regex = new Regex(lineParser.m_pattern, RegexOptions.IgnoreCase);
    this.m_currentLineVersion = this.m_lineParsers.Length;
    if (!this.Load())
      return;
    OptionsMigration.UpgradeClientOptions();
  }

  public void Clear()
  {
    this.m_dirty = false;
    this.m_options.Clear();
    this.m_sortedKeys.Clear();
  }

  public bool Has(string key)
  {
    return this.m_options.ContainsKey(key);
  }

  public void Delete(string key)
  {
    if (!this.m_options.Remove(key))
      return;
    this.m_sortedKeys.Remove(key);
    this.m_dirty = true;
    this.Save(key);
  }

  public T Get<T>(string key)
  {
    object obj;
    if (!this.m_options.TryGetValue(key, out obj))
      return default (T);
    return (T) obj;
  }

  public bool GetBool(string key)
  {
    return this.Get<bool>(key);
  }

  public int GetInt(string key)
  {
    return this.Get<int>(key);
  }

  public long GetLong(string key)
  {
    return this.Get<long>(key);
  }

  public ulong GetULong(string key)
  {
    return this.Get<ulong>(key);
  }

  public float GetFloat(string key)
  {
    return this.Get<float>(key);
  }

  public string GetString(string key)
  {
    return this.Get<string>(key);
  }

  public void Set(string key, object val)
  {
    object obj;
    if (this.m_options.TryGetValue(key, out obj))
    {
      if (obj == val || obj != null && obj.Equals(val))
        return;
    }
    else
    {
      this.m_sortedKeys.Add(key);
      this.SortKeys();
    }
    this.m_options[key] = val;
    this.m_dirty = true;
    this.Save(key);
  }

  private bool Load()
  {
    this.Clear();
    if (!File.Exists(this.m_path))
    {
      this.m_loadResult = LocalOptions.LoadResult.SUCCESS;
      return true;
    }
    string[] lines;
    if (!this.LoadFile(out lines))
    {
      this.m_loadResult = LocalOptions.LoadResult.FAIL;
      return false;
    }
    bool formatChanged = false;
    if (!this.LoadAllLines(lines, out formatChanged))
    {
      this.m_loadResult = LocalOptions.LoadResult.FAIL;
      return false;
    }
    using (Map<string, object>.KeyCollection.Enumerator enumerator = this.m_options.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.m_sortedKeys.Add(enumerator.Current);
    }
    this.SortKeys();
    this.m_loadResult = LocalOptions.LoadResult.SUCCESS;
    if (formatChanged)
    {
      this.m_dirty = true;
      this.Save();
    }
    return true;
  }

  private bool LoadFile(out string[] lines)
  {
    try
    {
      lines = File.ReadAllLines(this.m_path);
      return true;
    }
    catch (Exception ex)
    {
      Debug.LogError((object) string.Format("LocalOptions.LoadFile() - Failed to read {0}. Exception={1}", (object) this.m_path, (object) ex.Message));
      lines = (string[]) null;
      return false;
    }
  }

  private bool LoadAllLines(string[] lines, out bool formatChanged)
  {
    formatChanged = false;
    int num = 0;
    for (int index = 0; index < lines.Length; ++index)
    {
      string line = lines[index].Trim();
      if (line.Length != 0 && !line.StartsWith("#"))
      {
        int version;
        string key;
        object val;
        bool formatChanged1;
        if (!this.LoadLine(line, out version, out key, out val, out formatChanged1))
        {
          Debug.LogError((object) string.Format("LocalOptions.LoadAllLines() - Failed to load line {0}.", (object) (index + 1)));
          ++num;
          if (num > 4)
          {
            this.m_loadResult = LocalOptions.LoadResult.FAIL;
            return false;
          }
        }
        else
        {
          this.m_options[key] = val;
          formatChanged = formatChanged || version != this.m_currentLineVersion || formatChanged1;
        }
      }
    }
    return true;
  }

  private bool LoadLine(string line, out int version, out string key, out object val, out bool formatChanged)
  {
    version = 0;
    key = (string) null;
    val = (object) null;
    formatChanged = false;
    int num = 0;
    string key1 = (string) null;
    string val1 = (string) null;
    for (int index = 0; index < this.m_lineParsers.Length; ++index)
    {
      LocalOptions.LineParser lineParser = this.m_lineParsers[index];
      if (lineParser.m_callback(lineParser.m_regex, line, out key1, out val1))
      {
        num = index + 1;
        break;
      }
    }
    if (num == 0)
      return false;
    Option index1;
    try
    {
      index1 = EnumUtils.GetEnum<Option>(key1, StringComparison.OrdinalIgnoreCase);
    }
    catch (ArgumentException ex)
    {
      version = num;
      key = key1;
      val = (object) val1;
      return true;
    }
    bool flag = false;
    int val2;
    Locale outVal;
    if (index1 == Option.LOCALE && GeneralUtils.TryParseInt(val1, out val2) && EnumUtils.TryCast<Locale>((object) val2, out outVal))
    {
      val1 = outVal.ToString();
      flag = true;
    }
    System.Type type = OptionDataTables.s_typeMap[index1];
    if (type == typeof (bool))
      val = (object) GeneralUtils.ForceBool(val1);
    else if (type == typeof (int))
      val = (object) GeneralUtils.ForceInt(val1);
    else if (type == typeof (long))
      val = (object) GeneralUtils.ForceLong(val1);
    else if (type == typeof (ulong))
      val = (object) GeneralUtils.ForceULong(val1);
    else if (type == typeof (float))
    {
      val = (object) GeneralUtils.ForceFloat(val1);
    }
    else
    {
      if (type != typeof (string))
        return false;
      val = (object) val1;
    }
    version = num;
    key = key1;
    formatChanged = flag;
    return true;
  }

  private static bool ParseLine_V1(Regex regex, string line, out string key, out string val)
  {
    int length = 4096;
    key = (string) null;
    val = (string) null;
    if (line.Length > length)
    {
      Debug.LogErrorFormat("LocalOptions: failed to parse options.txt line with length {0}, greater than the maximum allowed length of {1}:\n{2}", (object) line.Length, (object) length, (object) line.Substring(0, length));
      return false;
    }
    Match match = regex.Match(line);
    if (!match.Success)
      return false;
    GroupCollection groups = match.Groups;
    key = groups["key"].Value;
    if (string.IsNullOrEmpty(key))
      return false;
    string str = groups["type"].Value;
    if (string.IsNullOrEmpty(str))
      return false;
    string key1 = str;
    if (key1 != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (LocalOptions.\u003C\u003Ef__switch\u0024mapCD == null)
      {
        // ISSUE: reference to a compiler-generated field
        LocalOptions.\u003C\u003Ef__switch\u0024mapCD = new Dictionary<string, int>(5)
        {
          {
            "b",
            0
          },
          {
            "i",
            0
          },
          {
            "l",
            0
          },
          {
            "d",
            0
          },
          {
            "s",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (LocalOptions.\u003C\u003Ef__switch\u0024mapCD.TryGetValue(key1, out num) && num == 0)
      {
        val = groups["value"].Value;
        return true;
      }
    }
    return false;
  }

  private static bool ParseLine_V2(Regex regex, string line, out string key, out string val)
  {
    key = (string) null;
    val = (string) null;
    Match match = regex.Match(line);
    if (!match.Success)
      return false;
    GroupCollection groups = match.Groups;
    key = groups["key"].Value;
    if (string.IsNullOrEmpty(key))
      return false;
    val = groups["value"].Value;
    return true;
  }

  private bool Save(string triggerKey)
  {
    switch (this.m_loadResult)
    {
      case LocalOptions.LoadResult.INVALID:
      case LocalOptions.LoadResult.FAIL:
        return false;
      default:
        return this.Save();
    }
  }

  private bool Save()
  {
    if (!this.m_dirty)
      return true;
    List<string> stringList = new List<string>();
    for (int index = 0; index < this.m_sortedKeys.Count; ++index)
    {
      string sortedKey = this.m_sortedKeys[index];
      object option = this.m_options[sortedKey];
      string str = string.Format("{0}={1}", (object) sortedKey, option);
      stringList.Add(str);
    }
    try
    {
      File.WriteAllLines(this.m_path, stringList.ToArray(), (Encoding) new UTF8Encoding());
      this.m_dirty = false;
      return true;
    }
    catch (Exception ex)
    {
      Debug.LogError((object) string.Format("LocalOptions.Save() - Failed to save {0}. Exception={1}", (object) this.m_path, (object) ex.Message));
      return false;
    }
  }

  private void SortKeys()
  {
    this.m_sortedKeys.Sort(new Comparison<string>(this.KeyComparison));
  }

  private int KeyComparison(string key1, string key2)
  {
    return string.Compare(key1, key2, true);
  }

  private enum LoadResult
  {
    INVALID,
    SUCCESS,
    FAIL,
  }

  private class LineParser
  {
    public string m_pattern;
    public Regex m_regex;
    public LocalOptions.ParseLineCallback m_callback;
  }

  private delegate bool ParseLineCallback(Regex regex, string line, out string key, out string val);
}
