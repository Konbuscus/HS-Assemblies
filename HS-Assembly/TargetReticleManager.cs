﻿// Decompiled with JetBrains decompiler
// Type: TargetReticleManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TargetReticleManager : MonoBehaviour
{
  private static readonly PlatformDependentValue<bool> SHOW_DAMAGE_INDICATOR_ON_ENTITY = new PlatformDependentValue<bool>(PlatformCategory.Input) { Mouse = false, Touch = true };
  private static readonly PlatformDependentValue<float> DAMAGE_INDICATOR_SCALE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 2.5f, Tablet = 3.75f };
  private static readonly PlatformDependentValue<float> DAMAGE_INDICATOR_Z_OFFSET = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 0.75f, Tablet = -1.2f };
  private bool m_showArrow = true;
  private int m_originLocationEntityID = -1;
  private int m_sourceEntityID = -1;
  private const int MAX_TARGET_ARROW_LINKS = 15;
  private const float LINK_Y_LENGTH = 1f;
  private const float LENGTH_BETWEEN_LINKS = 1.2f;
  private const float LINK_PARABOLA_HEIGHT = 1.5f;
  private const float LINK_ANIMATION_SPEED = 0.5f;
  private const float STARTING_X_ROTATION_FOR_DEFAULT_ARROW = 300f;
  private const float FRIENDLY_HERO_ORIGIN_Z_OFFSET = 1f;
  private const float LINK_FADE_OFFSET = -1.2f;
  private TARGET_RETICLE_TYPE m_ReticleType;
  private static TargetReticleManager s_instance;
  private bool m_isEnemyArrow;
  private bool m_isActive;
  private int m_numActiveLinks;
  private float m_linkAnimationZOffset;
  private Vector3 m_targetArrowOrigin;
  private Vector3 m_remoteArrowPosition;
  private GameObject m_arrow;
  private GameObject m_damageIndicator;
  private GameObject m_hunterReticle;
  private GameObject m_questionMark;
  private List<GameObject> m_targetArrowLinks;
  private bool m_useHandAsOrigin;

  public int ArrowSourceEntityID
  {
    get
    {
      return this.m_originLocationEntityID;
    }
  }

  private void Awake()
  {
    TargetReticleManager.s_instance = this;
  }

  private void OnDestroy()
  {
    TargetReticleManager.s_instance = (TargetReticleManager) null;
  }

  public static TargetReticleManager Get()
  {
    return TargetReticleManager.s_instance;
  }

  public bool IsActive()
  {
    if ((Object) this.GetAppropriateReticle() != (Object) null)
      return this.m_isActive;
    return false;
  }

  public bool IsLocalArrow()
  {
    return !this.m_isEnemyArrow;
  }

  public bool IsEnemyArrow()
  {
    return this.m_isEnemyArrow;
  }

  public bool IsLocalArrowActive()
  {
    if (this.m_isEnemyArrow)
      return false;
    return this.IsActive();
  }

  public bool IsEnemyArrowActive()
  {
    if (!this.m_isEnemyArrow)
      return false;
    return this.IsActive();
  }

  public void ShowBullseye(bool show)
  {
    if (this.m_ReticleType == TARGET_RETICLE_TYPE.DefaultArrow)
    {
      if (!this.IsActive() || !this.m_showArrow)
        return;
      Transform child = this.m_arrow.transform.FindChild("TargetArrow_TargetMesh");
      if (!(bool) ((Object) child))
        return;
      SceneUtils.EnableRenderers(child.gameObject, show);
    }
    else if (this.m_ReticleType == TARGET_RETICLE_TYPE.HunterReticle)
    {
      if ((Object) this.m_hunterReticle == (Object) null)
        return;
      RenderToTexture component = this.m_hunterReticle.GetComponent<RenderToTexture>();
      if ((Object) component == (Object) null)
        return;
      Material renderMaterial = component.GetRenderMaterial();
      if ((Object) renderMaterial == (Object) null)
        return;
      if (show)
        renderMaterial.color = Color.red;
      else
        renderMaterial.color = Color.white;
    }
    else
    {
      if (this.m_ReticleType != TARGET_RETICLE_TYPE.QuestionMark || !this.IsActive() || !this.m_showArrow)
        return;
      Transform child = this.m_questionMark.transform.FindChild("TargetQuestionMark_TargetMesh");
      if (!(bool) ((Object) child))
        return;
      SceneUtils.EnableRenderers(child.gameObject, show);
    }
  }

  public void CreateFriendlyTargetArrow(Entity originLocationEntity, Entity sourceEntity, bool showDamageIndicatorText, bool showArrow = true, string overrideText = null, bool useHandAsOrigin = false)
  {
    if (GameMgr.Get() == null || !GameMgr.Get().IsSpectator())
      this.DisableCollidersForUntargetableCards(sourceEntity.GetCard());
    if (GameState.Get().GetGameEntity().HasTag(GAME_TAG.ALL_TARGETS_RANDOM))
    {
      this.m_ReticleType = TARGET_RETICLE_TYPE.QuestionMark;
    }
    else
    {
      Spell playSpell = sourceEntity.GetCard().GetPlaySpell(true);
      this.m_ReticleType = !((Object) playSpell != (Object) null) ? TARGET_RETICLE_TYPE.DefaultArrow : playSpell.m_TargetReticle;
    }
    string damageIndicatorText = (string) null;
    if (overrideText != null)
      damageIndicatorText = overrideText;
    else if (showDamageIndicatorText)
      damageIndicatorText = sourceEntity.GetTargetingArrowText();
    this.CreateTargetArrow(false, originLocationEntity.GetEntityId(), sourceEntity.GetEntityId(), damageIndicatorText, showArrow, useHandAsOrigin);
    this.AttachLinksToAppropriateReticle();
  }

  private void AttachLinksToAppropriateReticle()
  {
    GameObject appropriateReticle = this.GetAppropriateReticle();
    using (List<GameObject>.Enumerator enumerator = this.m_targetArrowLinks.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.transform.parent = appropriateReticle.transform;
    }
  }

  public void CreateEnemyTargetArrow(Entity originEntity)
  {
    this.m_ReticleType = !GameState.Get().GetGameEntity().HasTag(GAME_TAG.ALL_TARGETS_RANDOM) ? TARGET_RETICLE_TYPE.DefaultArrow : TARGET_RETICLE_TYPE.QuestionMark;
    this.CreateTargetArrow(true, originEntity.GetEntityId(), originEntity.GetEntityId(), (string) null, true, false);
    this.AttachLinksToAppropriateReticle();
  }

  public void DestroyEnemyTargetArrow()
  {
    this.DestroyTargetArrow(true, false);
  }

  public void DestroyFriendlyTargetArrow(bool isLocallyCanceled)
  {
    this.EnableCollidersThatWereDisabled();
    this.DestroyTargetArrow(false, isLocallyCanceled);
  }

  public void UpdateArrowPosition()
  {
    if (!this.IsActive())
      return;
    if (!this.m_showArrow)
    {
      this.UpdateArrowOriginPosition();
      this.UpdateDamageIndicator();
    }
    else
    {
      float y = 0.0f;
      Vector3 point;
      if (this.m_isEnemyArrow || GameMgr.Get() != null && GameMgr.Get().IsSpectator())
      {
        Vector3 zero = Vector3.zero;
        Vector3 position = this.GetAppropriateReticle().transform.position;
        point.x = Mathf.Lerp(position.x, this.m_remoteArrowPosition.x, 0.1f);
        point.y = Mathf.Lerp(position.y, this.m_remoteArrowPosition.y, 0.1f);
        point.z = Mathf.Lerp(position.z, this.m_remoteArrowPosition.z, 0.1f);
        Card card = !this.m_isEnemyArrow ? RemoteActionHandler.Get().GetFriendlyHeldCard() : RemoteActionHandler.Get().GetOpponentHeldCard();
        if ((Object) card != (Object) null)
          this.m_targetArrowOrigin = card.transform.position;
      }
      else
      {
        RaycastHit hitInfo;
        if (!UniversalInputManager.Get().GetInputHitInfo(Camera.main, GameLayer.DragPlane, out hitInfo))
          return;
        point = hitInfo.point;
        this.UpdateArrowOriginPosition();
      }
      if (!Mathf.Approximately(point.z - this.m_targetArrowOrigin.z, 0.0f))
        y = 57.29578f * Mathf.Atan((float) (((double) point.x - (double) this.m_targetArrowOrigin.x) / ((double) point.z - (double) this.m_targetArrowOrigin.z)));
      if ((double) point.z < (double) this.m_targetArrowOrigin.z)
        y -= 180f;
      if (this.m_ReticleType == TARGET_RETICLE_TYPE.DefaultArrow || this.m_ReticleType == TARGET_RETICLE_TYPE.QuestionMark)
      {
        GameObject appropriateReticle = this.GetAppropriateReticle();
        appropriateReticle.transform.localEulerAngles = new Vector3(0.0f, y, 0.0f);
        appropriateReticle.transform.position = point;
        this.UpdateTargetArrowLinks(Mathf.Sqrt(Mathf.Pow(this.m_targetArrowOrigin.x - point.x, 2f) + Mathf.Pow(this.m_targetArrowOrigin.z - point.z, 2f)));
      }
      else if (this.m_ReticleType == TARGET_RETICLE_TYPE.HunterReticle)
        this.m_hunterReticle.transform.position = point;
      else
        UnityEngine.Debug.LogError((object) "Unknown Target Reticle Type!");
      this.UpdateDamageIndicator();
    }
  }

  public void SetRemotePlayerArrowPosition(Vector3 newPosition)
  {
    this.m_remoteArrowPosition = newPosition;
  }

  private void DestroyCurrentArrow(bool isLocallyCanceled)
  {
    if (this.m_isEnemyArrow)
      this.DestroyEnemyTargetArrow();
    else
      this.DestroyFriendlyTargetArrow(isLocallyCanceled);
  }

  private void DisableCollidersForUntargetableCards(Card sourceCard)
  {
    List<Card> cards = new List<Card>();
    using (Map<int, Player>.ValueCollection.Enumerator enumerator1 = GameState.Get().GetPlayerMap().Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Player current1 = enumerator1.Current;
        this.AddUntargetableCard(sourceCard, cards, current1.GetHeroPowerCard());
        this.AddUntargetableCard(sourceCard, cards, current1.GetWeaponCard());
        using (List<Card>.Enumerator enumerator2 = current1.GetSecretZone().GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Card current2 = enumerator2.Current;
            this.AddUntargetableCard(sourceCard, cards, current2);
          }
        }
      }
    }
    using (List<Card>.Enumerator enumerator = cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          Actor actor = current.GetActor();
          if (!((Object) actor == (Object) null))
            actor.TurnOffCollider();
        }
      }
    }
  }

  private void AddUntargetableCard(Card sourceCard, List<Card> cards, Card card)
  {
    if ((Object) sourceCard == (Object) card)
      return;
    cards.Add(card);
  }

  private void EnableCollidersThatWereDisabled()
  {
    List<Card> cardList = new List<Card>();
    using (Map<int, Player>.ValueCollection.Enumerator enumerator1 = GameState.Get().GetPlayerMap().Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Player current1 = enumerator1.Current;
        cardList.Add(current1.GetHeroPowerCard());
        cardList.Add(current1.GetWeaponCard());
        using (List<Card>.Enumerator enumerator2 = current1.GetSecretZone().GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Card current2 = enumerator2.Current;
            cardList.Add(current2);
          }
        }
      }
    }
    using (List<Card>.Enumerator enumerator = cardList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (!((Object) current == (Object) null) && !((Object) current.GetActor() == (Object) null))
          current.GetActor().TurnOnCollider();
      }
    }
  }

  private void CreateTargetArrow(bool isEnemyArrow, int originLocationEntityID, int sourceEntityID, string damageIndicatorText, bool showArrow, bool useHandAsOrigin = false)
  {
    if (this.IsActive())
    {
      Log.Rachelle.Print("Uh-oh... creating a targeting arrow but one is already active...");
      this.DestroyCurrentArrow(false);
    }
    this.m_isEnemyArrow = isEnemyArrow;
    this.m_sourceEntityID = sourceEntityID;
    this.m_originLocationEntityID = originLocationEntityID;
    this.m_showArrow = showArrow;
    this.m_useHandAsOrigin = useHandAsOrigin;
    this.UpdateArrowOriginPosition();
    bool flag = GameMgr.Get() != null && GameMgr.Get().IsSpectator();
    if (this.m_isEnemyArrow || flag)
    {
      this.m_remoteArrowPosition = this.m_targetArrowOrigin;
      this.m_arrow.transform.position = this.m_targetArrowOrigin;
    }
    this.ActivateArrow(true);
    this.ShowBullseye(false);
    this.ShowDamageIndicator(!this.m_isEnemyArrow);
    this.UpdateArrowPosition();
    if (this.m_isEnemyArrow)
      return;
    this.StartCoroutine(this.SetDamageText(damageIndicatorText));
    if (flag)
      return;
    PegCursor.Get().Hide();
  }

  public void PreloadTargetArrows()
  {
    this.m_targetArrowLinks = new List<GameObject>();
    AssetLoader.Get().LoadActor("Target_Arrow_Bullseye", new AssetLoader.GameObjectCallback(this.LoadArrowCallback), (object) null, false);
    AssetLoader.Get().LoadActor("TargetDamageIndicator", new AssetLoader.GameObjectCallback(this.LoadDamageIndicatorCallback), (object) null, false);
    AssetLoader.Get().LoadActor("Target_Arrow_Link", new AssetLoader.GameObjectCallback(this.LoadLinkCallback), (object) null, false);
    AssetLoader.Get().LoadActor("HunterReticle", new AssetLoader.GameObjectCallback(this.LoadHunterReticleCallback), (object) null, false);
    AssetLoader.Get().LoadActor("Target_Question_Mark", new AssetLoader.GameObjectCallback(this.LoadQuestionCallback), (object) null, false);
  }

  private void DestroyTargetArrow(bool destroyEnemyArrow, bool isLocallyCanceled)
  {
    if (!this.IsActive())
      return;
    if (destroyEnemyArrow != this.m_isEnemyArrow)
    {
      Log.Rachelle.Print(string.Format("trying to destroy {0} arrow but the active arrow is {1}", !destroyEnemyArrow ? (object) "friendly" : (object) "enemy", !this.m_isEnemyArrow ? (object) "friendly" : (object) "enemy"));
    }
    else
    {
      if (isLocallyCanceled)
      {
        Entity entity = GameState.Get().GetEntity(this.m_sourceEntityID);
        if (entity != null)
          entity.GetCard().NotifyTargetingCanceled();
      }
      this.m_originLocationEntityID = -1;
      this.m_sourceEntityID = -1;
      if (!this.m_isEnemyArrow)
      {
        RemoteActionHandler.Get().NotifyOpponentOfTargetEnd();
        PegCursor.Get().Show();
      }
      this.ActivateArrow(false);
      this.ShowDamageIndicator(false);
    }
  }

  private void LoadArrowCallback(string actorName, GameObject actorObject, object callbackData)
  {
    this.m_arrow = actorObject;
    SceneUtils.EnableRenderers(this.m_arrow.gameObject, false);
    this.ShowBullseye(false);
  }

  private void LoadQuestionCallback(string actorName, GameObject actorObject, object callbackData)
  {
    this.m_questionMark = actorObject;
    SceneUtils.EnableRenderers(this.m_questionMark.gameObject, false);
    this.ShowBullseye(false);
  }

  private void LoadLinkCallback(string actorName, GameObject actorObject, object callbackData)
  {
    this.StartCoroutine(this.OnLinkLoaded(actorObject));
  }

  private void LoadDamageIndicatorCallback(string actorName, GameObject actorObject, object callbackData)
  {
    this.m_damageIndicator = actorObject;
    this.m_damageIndicator.transform.eulerAngles = new Vector3(90f, 0.0f, 0.0f);
    this.m_damageIndicator.transform.localScale = new Vector3((float) TargetReticleManager.DAMAGE_INDICATOR_SCALE, (float) TargetReticleManager.DAMAGE_INDICATOR_SCALE, (float) TargetReticleManager.DAMAGE_INDICATOR_SCALE);
    this.ShowDamageIndicator(false);
  }

  private void LoadHunterReticleCallback(string actorName, GameObject actorObject, object callbackData)
  {
    this.m_hunterReticle = actorObject;
    this.m_hunterReticle.transform.parent = this.transform;
    this.m_hunterReticle.SetActive(false);
  }

  [DebuggerHidden]
  private IEnumerator OnLinkLoaded(GameObject linkActorObject)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TargetReticleManager.\u003COnLinkLoaded\u003Ec__IteratorDF() { linkActorObject = linkActorObject, \u003C\u0024\u003ElinkActorObject = linkActorObject, \u003C\u003Ef__this = this };
  }

  private int NumberOfRequiredLinks(float lengthOfArrow)
  {
    int num = (int) Mathf.Floor(lengthOfArrow / 1.2f) + 1;
    if (num == 1)
      num = 0;
    return num;
  }

  private GameObject GetAppropriateReticle()
  {
    switch (this.m_ReticleType)
    {
      case TARGET_RETICLE_TYPE.DefaultArrow:
        return this.m_arrow;
      case TARGET_RETICLE_TYPE.HunterReticle:
        return this.m_hunterReticle;
      case TARGET_RETICLE_TYPE.QuestionMark:
        return this.m_questionMark;
      default:
        Log.All.PrintError("Unknown Target Reticle Type!");
        return (GameObject) null;
    }
  }

  private Transform GetAppropriateArrowMeshTransform()
  {
    switch (this.m_ReticleType)
    {
      case TARGET_RETICLE_TYPE.DefaultArrow:
      case TARGET_RETICLE_TYPE.HunterReticle:
        return this.m_arrow.transform.FindChild("TargetArrow_ArrowMesh");
      case TARGET_RETICLE_TYPE.QuestionMark:
        return this.m_questionMark.transform.FindChild("TargetQuestionMark_QuestionMarkMesh");
      default:
        Log.All.PrintError("Unknown Target Reticle Type!");
        return (Transform) null;
    }
  }

  private float GetStartingXRotationForArrowMesh()
  {
    switch (this.m_ReticleType)
    {
      case TARGET_RETICLE_TYPE.DefaultArrow:
      case TARGET_RETICLE_TYPE.HunterReticle:
        return 300f;
      case TARGET_RETICLE_TYPE.QuestionMark:
        return 0.0f;
      default:
        Log.All.PrintError("Unknown Target Reticle Type!");
        return 0.0f;
    }
  }

  private void UpdateTargetArrowLinks(float lengthOfArrow)
  {
    this.m_numActiveLinks = this.NumberOfRequiredLinks(lengthOfArrow);
    int count = this.m_targetArrowLinks.Count;
    Transform arrowMeshTransform = this.GetAppropriateArrowMeshTransform();
    if (this.m_numActiveLinks == 0)
    {
      arrowMeshTransform.localEulerAngles = new Vector3(this.GetStartingXRotationForArrowMesh(), 180f, 0.0f);
      for (int index = 0; index < count; ++index)
        SceneUtils.EnableRenderers(this.m_targetArrowLinks[index].gameObject, false);
    }
    else
    {
      float num1 = (float) (-(double) lengthOfArrow / 2.0);
      float num2 = (float) (-1.5 / ((double) num1 * (double) num1));
      for (int index = 0; index < count; ++index)
      {
        if (!((Object) this.m_targetArrowLinks[index] == (Object) null))
        {
          if (index >= this.m_numActiveLinks)
          {
            SceneUtils.EnableRenderers(this.m_targetArrowLinks[index].gameObject, false);
          }
          else
          {
            float z = (float) -(1.20000004768372 * (double) (index + 1)) + this.m_linkAnimationZOffset;
            float y = (float) ((double) num2 * (double) Mathf.Pow(z - num1, 2f) + 1.5);
            float x = (float) (180.0 - (double) Mathf.Atan((float) (2.0 * (double) num2 * ((double) z - (double) num1))) * 57.2957801818848);
            SceneUtils.EnableRenderers(this.m_targetArrowLinks[index].gameObject, true);
            this.m_targetArrowLinks[index].transform.localPosition = new Vector3(0.0f, y, z);
            this.m_targetArrowLinks[index].transform.eulerAngles = new Vector3(x, this.GetAppropriateReticle().transform.localEulerAngles.y, 0.0f);
            float alpha = 1f;
            if (index == 0)
            {
              if ((double) z > -1.20000004768372)
                alpha = Mathf.Pow(z / -1.2f, 6f);
            }
            else if (index == this.m_numActiveLinks - 1)
            {
              float num3 = this.m_linkAnimationZOffset / 1.2f;
              alpha = num3 * num3;
            }
            this.SetLinkAlpha(this.m_targetArrowLinks[index], alpha);
          }
        }
      }
      float y1 = (float) ((double) num2 * (double) Mathf.Pow(arrowMeshTransform.localPosition.z - num1, 2f) + 1.5);
      float x1 = 0.0f;
      if (this.m_ReticleType != TARGET_RETICLE_TYPE.QuestionMark)
      {
        x1 = Mathf.Atan((float) (2.0 * (double) num2 * ((double) arrowMeshTransform.localPosition.z - (double) num1))) * 57.29578f;
        if ((double) x1 < 0.0)
          x1 += 360f;
      }
      arrowMeshTransform.localPosition = new Vector3(0.0f, y1, arrowMeshTransform.localPosition.z);
      arrowMeshTransform.localEulerAngles = new Vector3(x1, 180f, 0.0f);
      this.m_linkAnimationZOffset += Time.deltaTime * 0.5f;
      if ((double) this.m_linkAnimationZOffset <= 1.20000004768372)
        return;
      this.m_linkAnimationZOffset -= 1.2f;
    }
  }

  private void SetLinkAlpha(GameObject linkGameObject, float alpha)
  {
    alpha = Mathf.Clamp(alpha, 0.0f, 1f);
    foreach (Renderer component in linkGameObject.GetComponents<Renderer>())
    {
      Color color = component.material.color;
      color.a = alpha;
      component.material.color = color;
    }
  }

  private void UpdateDamageIndicator()
  {
    if ((Object) this.m_damageIndicator == (Object) null)
      return;
    Vector3 zero = Vector3.zero;
    Vector3 vector3;
    if ((bool) TargetReticleManager.SHOW_DAMAGE_INDICATOR_ON_ENTITY)
    {
      vector3 = this.m_targetArrowOrigin;
      vector3.z += (float) TargetReticleManager.DAMAGE_INDICATOR_Z_OFFSET;
    }
    else
    {
      vector3 = this.GetAppropriateReticle().transform.position;
      vector3.z += (float) TargetReticleManager.DAMAGE_INDICATOR_Z_OFFSET;
    }
    this.m_damageIndicator.transform.position = vector3;
  }

  private void ShowDamageIndicator(bool show)
  {
    if (!(bool) ((Object) this.m_damageIndicator) || !this.m_damageIndicator.activeInHierarchy)
      return;
    SceneUtils.EnableRenderers(this.m_damageIndicator.gameObject, show);
  }

  [DebuggerHidden]
  private IEnumerator SetDamageText(string damageText)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TargetReticleManager.\u003CSetDamageText\u003Ec__IteratorE0() { damageText = damageText, \u003C\u0024\u003EdamageText = damageText, \u003C\u003Ef__this = this };
  }

  private void UpdateArrowOriginPosition()
  {
    Entity entity = GameState.Get().GetEntity(this.m_originLocationEntityID);
    if (entity == null)
    {
      Log.Rachelle.Print(string.Format("entity with ID {0} does not exist... can't update arrow origin position!", (object) this.m_originLocationEntityID));
      this.DestroyCurrentArrow(false);
    }
    else
    {
      this.m_targetArrowOrigin = entity.GetCard().transform.position;
      if (this.m_useHandAsOrigin)
        this.m_targetArrowOrigin = InputManager.Get().GetFriendlyHand().transform.position;
      if (!entity.IsHero() || this.m_isEnemyArrow)
        return;
      ++this.m_targetArrowOrigin.z;
    }
  }

  private void ActivateArrow(bool active)
  {
    this.m_isActive = active;
    SceneUtils.EnableRenderers(this.m_arrow.gameObject, false);
    this.m_hunterReticle.SetActive(false);
    SceneUtils.EnableRenderers(this.m_questionMark.gameObject, false);
    if (!active)
      return;
    if (this.m_ReticleType == TARGET_RETICLE_TYPE.DefaultArrow)
      SceneUtils.EnableRenderers(this.m_arrow.gameObject, active && this.m_showArrow);
    else if (this.m_ReticleType == TARGET_RETICLE_TYPE.HunterReticle)
      this.m_hunterReticle.SetActive(active && this.m_showArrow);
    else if (this.m_ReticleType == TARGET_RETICLE_TYPE.QuestionMark)
      SceneUtils.EnableRenderers(this.m_questionMark.gameObject, active && this.m_showArrow);
    else
      UnityEngine.Debug.LogError((object) "Unknown Target Reticle Type!");
  }

  public void ShowArrow(bool show)
  {
    this.m_showArrow = show;
    SceneUtils.EnableRenderers(this.m_arrow.gameObject, false);
    this.m_hunterReticle.SetActive(false);
    SceneUtils.EnableRenderers(this.m_questionMark.gameObject, false);
    if (!show)
      return;
    if (this.m_ReticleType == TARGET_RETICLE_TYPE.DefaultArrow)
      SceneUtils.EnableRenderers(this.m_arrow.gameObject, show);
    else if (this.m_ReticleType == TARGET_RETICLE_TYPE.HunterReticle)
      this.m_hunterReticle.SetActive(show);
    else if (this.m_ReticleType == TARGET_RETICLE_TYPE.QuestionMark)
      SceneUtils.EnableRenderers(this.m_questionMark.gameObject, show);
    else
      UnityEngine.Debug.LogError((object) "Unknown Target Reticle Type!");
  }
}
