﻿// Decompiled with JetBrains decompiler
// Type: Facebook.AndroidFacebook
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System;
using System.Collections.Generic;

namespace Facebook
{
  internal sealed class AndroidFacebook : AbstractFacebook, IFacebook
  {
    public const int BrowserDialogMode = 0;
    private const string AndroidJavaFacebookClass = "com.facebook.unity.FB";
    private const string CallbackIdKey = "callback_id";
    private string keyHash;
    private FacebookDelegate deepLinkDelegate;
    private InitDelegate onInitComplete;

    public string KeyHash
    {
      get
      {
        return this.keyHash;
      }
    }

    public override int DialogMode
    {
      get
      {
        return 0;
      }
      set
      {
      }
    }

    public override bool LimitEventUsage
    {
      get
      {
        return this.limitEventUsage;
      }
      set
      {
        this.limitEventUsage = value;
        this.CallFB("SetLimitEventUsage", value.ToString());
      }
    }

    private void CallFB(string method, string args)
    {
      FbDebug.Error("Using Android when not on an Android build!  Doesn't Work!");
    }

    protected override void OnAwake()
    {
      this.keyHash = string.Empty;
    }

    private bool IsErrorResponse(string response)
    {
      return false;
    }

    public override void Init(InitDelegate onInitComplete, string appId, bool cookie = false, bool logging = true, bool status = true, bool xfbml = false, string channelUrl = "", string authResponse = null, bool frictionlessRequests = false, HideUnityDelegate hideUnityDelegate = null)
    {
      if (string.IsNullOrEmpty(appId))
        throw new ArgumentException("appId cannot be null or empty!");
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary.Add("appId", (object) appId);
      if (cookie)
        dictionary.Add("cookie", (object) true);
      if (!logging)
        dictionary.Add("logging", (object) false);
      if (!status)
        dictionary.Add("status", (object) false);
      if (xfbml)
        dictionary.Add("xfbml", (object) true);
      if (!string.IsNullOrEmpty(channelUrl))
        dictionary.Add("channelUrl", (object) channelUrl);
      if (!string.IsNullOrEmpty(authResponse))
        dictionary.Add("authResponse", (object) authResponse);
      if (frictionlessRequests)
        dictionary.Add("frictionlessRequests", (object) true);
      string str = Json.Serialize((object) dictionary);
      this.onInitComplete = onInitComplete;
      this.CallFB("Init", str.ToString());
    }

    public void OnInitComplete(string message)
    {
      this.isInitialized = true;
      this.OnLoginComplete(message);
      if (this.onInitComplete == null)
        return;
      this.onInitComplete();
    }

    public override void Login(string scope = "", FacebookDelegate callback = null)
    {
      string args = Json.Serialize((object) new Dictionary<string, object>() { { "scope", (object) scope } });
      this.AddAuthDelegate(callback);
      this.CallFB("Login", args);
    }

    public void OnLoginComplete(string message)
    {
      Dictionary<string, object> dictionary = (Dictionary<string, object>) Json.Deserialize(message);
      if (dictionary.ContainsKey("user_id"))
      {
        this.isLoggedIn = true;
        this.userId = (string) dictionary["user_id"];
        this.accessToken = (string) dictionary["access_token"];
        this.accessTokenExpiresAt = this.FromTimestamp(int.Parse((string) dictionary["expiration_timestamp"]));
      }
      if (dictionary.ContainsKey("key_hash"))
        this.keyHash = (string) dictionary["key_hash"];
      this.OnAuthResponse(new FBResult(message, (string) null));
    }

    public void OnGroupCreateComplete(string message)
    {
      Dictionary<string, object> dictionary = (Dictionary<string, object>) Json.Deserialize(message);
      string uniqueId = (string) dictionary["callback_id"];
      dictionary.Remove("callback_id");
      this.OnFacebookResponse(uniqueId, new FBResult(Json.Serialize((object) dictionary), (string) null));
    }

    public void OnAccessTokenRefresh(string message)
    {
      Dictionary<string, object> dictionary = (Dictionary<string, object>) Json.Deserialize(message);
      if (!dictionary.ContainsKey("access_token"))
        return;
      this.accessToken = (string) dictionary["access_token"];
      this.accessTokenExpiresAt = this.FromTimestamp(int.Parse((string) dictionary["expiration_timestamp"]));
    }

    public override void Logout()
    {
      this.CallFB("Logout", string.Empty);
    }

    public void OnLogoutComplete(string message)
    {
      this.isLoggedIn = false;
      this.userId = string.Empty;
      this.accessToken = string.Empty;
    }

    public override void AppRequest(string message, OGActionType actionType, string objectId, string[] to = null, List<object> filters = null, string[] excludeIds = null, int? maxRecipients = null, string data = "", string title = "", FacebookDelegate callback = null)
    {
      if (string.IsNullOrEmpty(message))
        throw new ArgumentNullException("message", "message cannot be null or empty!");
      if (actionType != null && string.IsNullOrEmpty(objectId))
        throw new ArgumentNullException("objectId", "You cannot provide an actionType without an objectId");
      if (actionType == null && !string.IsNullOrEmpty(objectId))
        throw new ArgumentNullException("actionType", "You cannot provide an objectId without an actionType");
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["message"] = (object) message;
      if (callback != null)
        dictionary["callback_id"] = (object) this.AddFacebookDelegate(callback);
      if (actionType != null && !string.IsNullOrEmpty(objectId))
      {
        dictionary["action_type"] = (object) actionType.ToString();
        dictionary["object_id"] = (object) objectId;
      }
      if (to != null)
        dictionary["to"] = (object) string.Join(",", to);
      if (filters != null && filters.Count > 0)
      {
        string filter = filters[0] as string;
        if (filter != null)
          dictionary["filters"] = (object) filter;
      }
      if (maxRecipients.HasValue)
        dictionary["max_recipients"] = (object) maxRecipients.Value;
      if (!string.IsNullOrEmpty(data))
        dictionary["data"] = (object) data;
      if (!string.IsNullOrEmpty(title))
        dictionary["title"] = (object) title;
      this.CallFB("AppRequest", Json.Serialize((object) dictionary));
    }

    public void OnAppRequestsComplete(string message)
    {
      Dictionary<string, object> dictionary1 = (Dictionary<string, object>) Json.Deserialize(message);
      if (!dictionary1.ContainsKey("callback_id"))
        return;
      Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
      string uniqueId = (string) dictionary1["callback_id"];
      dictionary1.Remove("callback_id");
      if (dictionary1.Count > 0)
      {
        List<string> stringList = new List<string>(dictionary1.Count - 1);
        using (Dictionary<string, object>.KeyCollection.Enumerator enumerator = dictionary1.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            if (!current.StartsWith("to"))
              dictionary2[current] = dictionary1[current];
            else
              stringList.Add((string) dictionary1[current]);
          }
        }
        dictionary2.Add("to", (object) stringList);
        dictionary1.Clear();
        this.OnFacebookResponse(uniqueId, new FBResult(Json.Serialize((object) dictionary2), (string) null));
      }
      else
        this.OnFacebookResponse(uniqueId, new FBResult(Json.Serialize((object) dictionary2), "Malformed request response.  Please file a bug with facebook here: https://developers.facebook.com/bugs/create"));
    }

    public override void FeedRequest(string toId = "", string link = "", string linkName = "", string linkCaption = "", string linkDescription = "", string picture = "", string mediaSource = "", string actionName = "", string actionLink = "", string reference = "", Dictionary<string, string[]> properties = null, FacebookDelegate callback = null)
    {
      Dictionary<string, object> dictionary1 = new Dictionary<string, object>();
      if (callback != null)
        dictionary1["callback_id"] = (object) this.AddFacebookDelegate(callback);
      if (!string.IsNullOrEmpty(toId))
        dictionary1.Add("to", (object) toId);
      if (!string.IsNullOrEmpty(link))
        dictionary1.Add("link", (object) link);
      if (!string.IsNullOrEmpty(linkName))
        dictionary1.Add("name", (object) linkName);
      if (!string.IsNullOrEmpty(linkCaption))
        dictionary1.Add("caption", (object) linkCaption);
      if (!string.IsNullOrEmpty(linkDescription))
        dictionary1.Add("description", (object) linkDescription);
      if (!string.IsNullOrEmpty(picture))
        dictionary1.Add("picture", (object) picture);
      if (!string.IsNullOrEmpty(mediaSource))
        dictionary1.Add("source", (object) mediaSource);
      if (!string.IsNullOrEmpty(actionName) && !string.IsNullOrEmpty(actionLink))
        dictionary1.Add("actions", (object) new Dictionary<string, object>[1]
        {
          new Dictionary<string, object>()
          {
            {
              "name",
              (object) actionName
            },
            {
              "link",
              (object) actionLink
            }
          }
        });
      if (!string.IsNullOrEmpty(reference))
        dictionary1.Add("ref", (object) reference);
      if (properties != null)
      {
        Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
        using (Dictionary<string, string[]>.Enumerator enumerator = properties.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, string[]> current = enumerator.Current;
            if (current.Value.Length >= 1)
            {
              if (current.Value.Length == 1)
                dictionary2.Add(current.Key, (object) current.Value[0]);
              else
                dictionary2.Add(current.Key, (object) new Dictionary<string, object>()
                {
                  {
                    "text",
                    (object) current.Value[0]
                  },
                  {
                    "href",
                    (object) current.Value[1]
                  }
                });
            }
          }
        }
        dictionary1.Add("properties", (object) dictionary2);
      }
      this.CallFB("FeedRequest", Json.Serialize((object) dictionary1));
    }

    public void OnFeedRequestComplete(string message)
    {
      Dictionary<string, object> dictionary1 = (Dictionary<string, object>) Json.Deserialize(message);
      if (!dictionary1.ContainsKey("callback_id"))
        return;
      Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
      string uniqueId = (string) dictionary1["callback_id"];
      dictionary1.Remove("callback_id");
      if (dictionary1.Count > 0)
      {
        using (Dictionary<string, object>.KeyCollection.Enumerator enumerator = dictionary1.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            dictionary2[current] = dictionary1[current];
          }
        }
        dictionary1.Clear();
        this.OnFacebookResponse(uniqueId, new FBResult(Json.Serialize((object) dictionary2), (string) null));
      }
      else
        this.OnFacebookResponse(uniqueId, new FBResult(Json.Serialize((object) dictionary2), "Malformed request response.  Please file a bug with facebook here: https://developers.facebook.com/bugs/create"));
    }

    public override void Pay(string product, string action = "purchaseitem", int quantity = 1, int? quantityMin = null, int? quantityMax = null, string requestId = null, string pricepointId = null, string testCurrency = null, FacebookDelegate callback = null)
    {
      throw new PlatformNotSupportedException("There is no Facebook Pay Dialog on Android");
    }

    public override void GameGroupCreate(string name, string description, string privacy = "CLOSED", FacebookDelegate callback = null)
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["name"] = (object) name;
      dictionary["description"] = (object) description;
      dictionary["privacy"] = (object) privacy;
      if (callback != null)
        dictionary["callback_id"] = (object) this.AddFacebookDelegate(callback);
      this.CallFB("GameGroupCreate", Json.Serialize((object) dictionary));
    }

    public override void GameGroupJoin(string id, FacebookDelegate callback = null)
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["id"] = (object) id;
      if (callback != null)
        dictionary["callback_id"] = (object) this.AddFacebookDelegate(callback);
      this.CallFB("GameGroupJoin", Json.Serialize((object) dictionary));
    }

    public override void GetDeepLink(FacebookDelegate callback)
    {
      if (callback == null)
        return;
      this.deepLinkDelegate = callback;
      this.CallFB("GetDeepLink", string.Empty);
    }

    public void OnGetDeepLinkComplete(string message)
    {
      Dictionary<string, object> dictionary = (Dictionary<string, object>) Json.Deserialize(message);
      if (this.deepLinkDelegate == null)
        return;
      object empty = (object) string.Empty;
      dictionary.TryGetValue("deep_link", out empty);
      this.deepLinkDelegate(new FBResult(empty.ToString(), (string) null));
    }

    public override void AppEventsLogEvent(string logEvent, float? valueToSum = null, Dictionary<string, object> parameters = null)
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["logEvent"] = (object) logEvent;
      if (valueToSum.HasValue)
        dictionary["valueToSum"] = (object) valueToSum.Value;
      if (parameters != null)
        dictionary["parameters"] = (object) this.ToStringDict(parameters);
      this.CallFB("AppEvents", Json.Serialize((object) dictionary));
    }

    public override void AppEventsLogPurchase(float logPurchase, string currency = "USD", Dictionary<string, object> parameters = null)
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["logPurchase"] = (object) logPurchase;
      dictionary["currency"] = string.IsNullOrEmpty(currency) ? (object) "USD" : (object) currency;
      if (parameters != null)
        dictionary["parameters"] = (object) this.ToStringDict(parameters);
      this.CallFB("AppEvents", Json.Serialize((object) dictionary));
    }

    public override void PublishInstall(string appId, FacebookDelegate callback = null)
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>(2);
      dictionary["app_id"] = appId;
      if (callback != null)
        dictionary["callback_id"] = this.AddFacebookDelegate(callback);
      this.CallFB("PublishInstall", Json.Serialize((object) dictionary));
    }

    public void OnPublishInstallComplete(string message)
    {
      Dictionary<string, object> dictionary = (Dictionary<string, object>) Json.Deserialize(message);
      if (!dictionary.ContainsKey("callback_id"))
        return;
      this.OnFacebookResponse((string) dictionary["callback_id"], new FBResult(string.Empty, (string) null));
    }

    public override void ActivateApp(string appId = null)
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>(1);
      if (!string.IsNullOrEmpty(appId))
        dictionary["app_id"] = appId;
      this.CallFB("ActivateApp", Json.Serialize((object) dictionary));
    }

    private Dictionary<string, string> ToStringDict(Dictionary<string, object> dict)
    {
      if (dict == null)
        return (Dictionary<string, string>) null;
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      using (Dictionary<string, object>.Enumerator enumerator = dict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          dictionary[current.Key] = current.Value.ToString();
        }
      }
      return dictionary;
    }

    private DateTime FromTimestamp(int timestamp)
    {
      return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds((double) timestamp);
    }
  }
}
