﻿// Decompiled with JetBrains decompiler
// Type: Facebook.EditorFacebook
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Facebook
{
  internal class EditorFacebook : AbstractFacebook, IFacebook
  {
    private AbstractFacebook fb;
    private FacebookDelegate loginCallback;

    public override int DialogMode
    {
      get
      {
        return 0;
      }
      set
      {
      }
    }

    public override bool LimitEventUsage
    {
      get
      {
        return this.limitEventUsage;
      }
      set
      {
        this.limitEventUsage = value;
      }
    }

    protected override void OnAwake()
    {
      this.StartCoroutine(FB.RemoteFacebookLoader.LoadFacebookClass("CanvasFacebook", new FB.RemoteFacebookLoader.LoadedDllCallback(this.OnDllLoaded)));
    }

    public override void Init(InitDelegate onInitComplete, string appId, bool cookie = false, bool logging = true, bool status = true, bool xfbml = false, string channelUrl = "", string authResponse = null, bool frictionlessRequests = false, HideUnityDelegate hideUnityDelegate = null)
    {
      this.StartCoroutine(this.OnInit(onInitComplete, appId, cookie, logging, status, xfbml, channelUrl, authResponse, frictionlessRequests, hideUnityDelegate));
    }

    [DebuggerHidden]
    private IEnumerator OnInit(InitDelegate onInitComplete, string appId, bool cookie = false, bool logging = true, bool status = true, bool xfbml = false, string channelUrl = "", string authResponse = null, bool frictionlessRequests = false, HideUnityDelegate hideUnityDelegate = null)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new EditorFacebook.\u003COnInit\u003Ec__Iterator294() { onInitComplete = onInitComplete, appId = appId, cookie = cookie, logging = logging, status = status, xfbml = xfbml, channelUrl = channelUrl, authResponse = authResponse, frictionlessRequests = frictionlessRequests, hideUnityDelegate = hideUnityDelegate, \u003C\u0024\u003EonInitComplete = onInitComplete, \u003C\u0024\u003EappId = appId, \u003C\u0024\u003Ecookie = cookie, \u003C\u0024\u003Elogging = logging, \u003C\u0024\u003Estatus = status, \u003C\u0024\u003Exfbml = xfbml, \u003C\u0024\u003EchannelUrl = channelUrl, \u003C\u0024\u003EauthResponse = authResponse, \u003C\u0024\u003EfrictionlessRequests = frictionlessRequests, \u003C\u0024\u003EhideUnityDelegate = hideUnityDelegate, \u003C\u003Ef__this = this };
    }

    private void OnDllLoaded(IFacebook fb)
    {
      this.fb = (AbstractFacebook) fb;
    }

    public override void Login(string scope = "", FacebookDelegate callback = null)
    {
      this.AddAuthDelegate(callback);
      FBComponentFactory.GetComponent<EditorFacebookAccessToken>(IfNotExist.AddNew);
    }

    public override void Logout()
    {
      this.isLoggedIn = false;
      this.userId = string.Empty;
      this.accessToken = string.Empty;
      this.fb.UserId = string.Empty;
      this.fb.AccessToken = string.Empty;
    }

    public override void AppRequest(string message, OGActionType actionType, string objectId, string[] to = null, List<object> filters = null, string[] excludeIds = null, int? maxRecipients = null, string data = "", string title = "", FacebookDelegate callback = null)
    {
      this.fb.AppRequest(message, actionType, objectId, to, filters, excludeIds, maxRecipients, data, title, callback);
    }

    public override void FeedRequest(string toId = "", string link = "", string linkName = "", string linkCaption = "", string linkDescription = "", string picture = "", string mediaSource = "", string actionName = "", string actionLink = "", string reference = "", Dictionary<string, string[]> properties = null, FacebookDelegate callback = null)
    {
      this.fb.FeedRequest(toId, link, linkName, linkCaption, linkDescription, picture, mediaSource, actionName, actionLink, reference, properties, callback);
    }

    public override void Pay(string product, string action = "purchaseitem", int quantity = 1, int? quantityMin = null, int? quantityMax = null, string requestId = null, string pricepointId = null, string testCurrency = null, FacebookDelegate callback = null)
    {
      FbDebugOverride.Info("Pay method only works with Facebook Canvas.  Does nothing in the Unity Editor, iOS or Android");
    }

    public override void GameGroupCreate(string name, string description, string privacy = "CLOSED", FacebookDelegate callback = null)
    {
      throw new PlatformNotSupportedException("There is no Facebook GameGroupCreate Dialog on Editor");
    }

    public override void GameGroupJoin(string id, FacebookDelegate callback = null)
    {
      throw new PlatformNotSupportedException("There is no Facebook GameGroupJoin Dialog on Editor");
    }

    public override void GetAuthResponse(FacebookDelegate callback = null)
    {
      this.fb.GetAuthResponse(callback);
    }

    public override void PublishInstall(string appId, FacebookDelegate callback = null)
    {
    }

    public override void ActivateApp(string appId = null)
    {
      FbDebugOverride.Info("This only needs to be called for iOS or Android.");
    }

    public override void GetDeepLink(FacebookDelegate callback)
    {
      FbDebugOverride.Info("No Deep Linking in the Editor");
      if (callback == null)
        return;
      callback(new FBResult("<platform dependent>", (string) null));
    }

    public override void AppEventsLogEvent(string logEvent, float? valueToSum = null, Dictionary<string, object> parameters = null)
    {
      FbDebugOverride.Log("Pew! Pretending to send this off.  Doesn't actually work in the editor");
    }

    public override void AppEventsLogPurchase(float logPurchase, string currency = "USD", Dictionary<string, object> parameters = null)
    {
      FbDebugOverride.Log("Pew! Pretending to send this off.  Doesn't actually work in the editor");
    }

    public void MockLoginCallback(FBResult result)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) FBComponentFactory.GetComponent<EditorFacebookAccessToken>(IfNotExist.AddNew));
      if (result.Error != null)
      {
        this.BadAccessToken(result.Error);
      }
      else
      {
        try
        {
          List<object> objectList = (List<object>) Json.Deserialize(result.Text);
          List<string> stringList = new List<string>();
          using (List<object>.Enumerator enumerator = objectList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              object current = enumerator.Current;
              if (current is Dictionary<string, object>)
              {
                Dictionary<string, object> dictionary = (Dictionary<string, object>) current;
                if (dictionary.ContainsKey("body"))
                  stringList.Add((string) dictionary["body"]);
              }
            }
          }
          Dictionary<string, object> dictionary1 = (Dictionary<string, object>) Json.Deserialize(stringList[0]);
          if (FB.AppId != (string) ((Dictionary<string, object>) Json.Deserialize(stringList[1]))["id"])
          {
            this.BadAccessToken("Access token is not for current app id: " + FB.AppId);
          }
          else
          {
            this.userId = (string) dictionary1["id"];
            this.fb.UserId = this.userId;
            this.fb.AccessToken = this.accessToken;
            this.isLoggedIn = true;
            this.OnAuthResponse(new FBResult(string.Empty, (string) null));
          }
        }
        catch (Exception ex)
        {
          this.BadAccessToken("Could not get data from access token: " + ex.Message);
        }
      }
    }

    public void MockCancelledLoginCallback()
    {
      this.OnAuthResponse(new FBResult(string.Empty, (string) null));
    }

    private void BadAccessToken(string error)
    {
      FbDebugOverride.Error(error);
      this.userId = string.Empty;
      this.fb.UserId = string.Empty;
      this.accessToken = string.Empty;
      this.fb.AccessToken = string.Empty;
      FBComponentFactory.GetComponent<EditorFacebookAccessToken>(IfNotExist.AddNew);
    }
  }
}
