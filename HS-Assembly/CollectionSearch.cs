﻿// Decompiled with JetBrains decompiler
// Type: CollectionSearch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class CollectionSearch : MonoBehaviour
{
  private List<CollectionSearch.ActivatedListener> m_activatedListeners = new List<CollectionSearch.ActivatedListener>();
  private List<CollectionSearch.DeactivatedListener> m_deactivatedListeners = new List<CollectionSearch.DeactivatedListener>();
  private List<CollectionSearch.ClearedListener> m_clearedListeners = new List<CollectionSearch.ClearedListener>();
  private const float ANIM_TIME = 0.1f;
  private const int MAX_SEARCH_LENGTH = 31;
  public UberText m_searchText;
  public PegUIElement m_background;
  public PegUIElement m_clearButton;
  public GameObject m_xMesh;
  public Material m_altSearchMaterial;
  public Color m_altSearchColor;
  private Material m_origSearchMaterial;
  private Vector3 m_origSearchPos;
  private bool m_isActive;
  private string m_prevText;
  private string m_text;
  private GameLayer m_originalLayer;
  private GameLayer m_activeLayer;
  private bool m_isTouchKeyboardDisplayMode;

  private void Start()
  {
    this.m_background.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBackgroundReleased));
    this.m_clearButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClearReleased));
    W8Touch.Get().VirtualKeyboardDidShow += new Action(this.OnKeyboardShown);
    W8Touch.Get().VirtualKeyboardDidHide += new Action(this.OnKeyboardHidden);
    if ((UnityEngine.Object) this.m_background.GetComponent<Renderer>() != (UnityEngine.Object) null)
      this.m_origSearchMaterial = this.m_background.GetComponent<Renderer>().material;
    this.m_origSearchPos = this.transform.localPosition;
    this.UpdateSearchText();
  }

  private void OnDestroy()
  {
    W8Touch.Get().VirtualKeyboardDidShow -= new Action(this.OnKeyboardShown);
    W8Touch.Get().VirtualKeyboardDidHide -= new Action(this.OnKeyboardHidden);
    if (!((UnityEngine.Object) UniversalInputManager.Get() != (UnityEngine.Object) null))
      return;
    UniversalInputManager.Get().CancelTextInput(this.gameObject, false);
  }

  public bool IsActive()
  {
    return this.m_isActive;
  }

  public void SetActiveLayer(GameLayer activeLayer)
  {
    if (activeLayer == this.m_activeLayer)
      return;
    this.m_activeLayer = activeLayer;
    if (!this.IsActive())
      return;
    this.MoveToActiveLayer(false);
  }

  public void Activate(bool ignoreTouchMode = false)
  {
    if (this.m_isActive)
      return;
    this.m_background.SetEnabled(false);
    this.MoveToActiveLayer(true);
    this.m_isActive = true;
    this.m_prevText = this.m_text;
    foreach (CollectionSearch.ActivatedListener activatedListener in this.m_activatedListeners.ToArray())
      activatedListener();
    if (!ignoreTouchMode && (UniversalInputManager.Get().UseWindowsTouch() && W8Touch.s_isWindows8OrGreater || W8Touch.Get().IsVirtualKeyboardVisible()))
      this.TouchKeyboardSearchDisplay(true);
    else
      this.ShowInput(true);
  }

  public void Deactivate()
  {
    if (!this.m_isActive)
      return;
    this.m_background.SetEnabled(true);
    this.MoveToOriginalLayer();
    this.m_isActive = false;
    this.HideInput();
    this.ResetSearchDisplay();
    foreach (CollectionSearch.DeactivatedListener deactivatedListener in this.m_deactivatedListeners.ToArray())
      deactivatedListener(this.m_prevText, this.m_text);
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    Navigation.GoBack();
  }

  public void Cancel()
  {
    if (!this.m_isActive)
      return;
    this.m_text = this.m_prevText;
    this.UpdateSearchText();
    this.Deactivate();
  }

  public string GetText()
  {
    return this.m_text;
  }

  public void ClearFilter(bool updateVisuals = true)
  {
    this.m_text = string.Empty;
    this.UpdateSearchText();
    this.ClearInput();
    foreach (CollectionSearch.ClearedListener clearedListener in this.m_clearedListeners.ToArray())
      clearedListener(updateVisuals);
    if ((!UniversalInputManager.Get().IsTouchMode() || !W8Touch.s_isWindows8OrGreater) && !W8Touch.Get().IsVirtualKeyboardVisible())
      return;
    this.Deactivate();
  }

  public void RegisterActivatedListener(CollectionSearch.ActivatedListener listener)
  {
    if (this.m_activatedListeners.Contains(listener))
      return;
    this.m_activatedListeners.Add(listener);
  }

  public void RemoveActivatedListener(CollectionSearch.ActivatedListener listener)
  {
    this.m_activatedListeners.Remove(listener);
  }

  public void RegisterDeactivatedListener(CollectionSearch.DeactivatedListener listener)
  {
    if (this.m_deactivatedListeners.Contains(listener))
      return;
    this.m_deactivatedListeners.Add(listener);
  }

  public void RemoveDeactivatedListener(CollectionSearch.DeactivatedListener listener)
  {
    this.m_deactivatedListeners.Remove(listener);
  }

  public void RegisterClearedListener(CollectionSearch.ClearedListener listener)
  {
    if (this.m_clearedListeners.Contains(listener))
      return;
    this.m_clearedListeners.Add(listener);
  }

  public void RemoveClearedListener(CollectionSearch.ClearedListener listener)
  {
    this.m_clearedListeners.Remove(listener);
  }

  public void SetEnabled(bool enabled)
  {
    this.m_background.SetEnabled(enabled);
    this.m_clearButton.SetEnabled(enabled);
  }

  private void OnBackgroundReleased(UIEvent e)
  {
    this.Activate(false);
  }

  private void OnClearReleased(UIEvent e)
  {
    this.ClearFilter(true);
  }

  private void OnActivateAnimComplete()
  {
    this.ShowInput(true);
  }

  private void OnDeactivateAnimComplete()
  {
    foreach (CollectionSearch.DeactivatedListener deactivatedListener in this.m_deactivatedListeners.ToArray())
      deactivatedListener(this.m_prevText, this.m_text);
  }

  private void ShowInput(bool fromActivate = true)
  {
    Bounds bounds = this.m_searchText.GetBounds();
    this.m_searchText.gameObject.SetActive(false);
    Rect guiViewportRect = CameraUtils.CreateGUIViewportRect(Box.Get().GetCamera(), bounds.min, bounds.max);
    Color? nullable = new Color?();
    if (W8Touch.Get().IsVirtualKeyboardVisible())
      nullable = new Color?(this.m_altSearchColor);
    UniversalInputManager.TextInputParams parms = new UniversalInputManager.TextInputParams()
    {
      m_owner = this.gameObject,
      m_rect = guiViewportRect,
      m_updatedCallback = new UniversalInputManager.TextInputUpdatedCallback(this.OnInputUpdated),
      m_completedCallback = new UniversalInputManager.TextInputCompletedCallback(this.OnInputComplete),
      m_canceledCallback = new UniversalInputManager.TextInputCanceledCallback(this.OnInputCanceled),
      m_font = this.m_searchText.GetLocalizedFont(),
      m_text = this.m_text,
      m_color = nullable,
      m_touchScreenKeyboardHideInput = false
    };
    parms.m_showVirtualKeyboard = fromActivate;
    UniversalInputManager.Get().UseTextInput(parms, false);
  }

  private void HideInput()
  {
    UniversalInputManager.Get().CancelTextInput(this.gameObject, false);
    this.m_searchText.gameObject.SetActive(true);
  }

  private void ClearInput()
  {
    if (!this.m_isActive)
      return;
    SoundManager.Get().LoadAndPlay("text_box_delete_text");
    UniversalInputManager.Get().SetInputText(string.Empty, false);
  }

  private void OnInputUpdated(string input)
  {
    this.m_text = input;
    this.UpdateSearchText();
  }

  private void OnInputComplete(string input)
  {
    this.m_text = input;
    this.UpdateSearchText();
    SoundManager.Get().LoadAndPlay("text_commit");
    this.Deactivate();
  }

  private void OnInputCanceled(bool userRequested, GameObject requester)
  {
    this.Cancel();
  }

  private void UpdateSearchText()
  {
    if (string.IsNullOrEmpty(this.m_text))
    {
      this.m_searchText.Text = GameStrings.Get("GLUE_COLLECTION_SEARCH");
      this.m_clearButton.gameObject.SetActive(false);
    }
    else
    {
      this.m_searchText.Text = this.m_text;
      this.m_clearButton.gameObject.SetActive(true);
    }
  }

  private void MoveToActiveLayer(bool saveOriginalLayer)
  {
    if (saveOriginalLayer)
      this.m_originalLayer = (GameLayer) this.gameObject.layer;
    SceneUtils.SetLayer(this.gameObject, this.m_activeLayer);
  }

  private void MoveToOriginalLayer()
  {
    SceneUtils.SetLayer(this.gameObject, this.m_originalLayer);
  }

  private void TouchKeyboardSearchDisplay(bool fromActivate = false)
  {
    if (this.m_isTouchKeyboardDisplayMode)
      return;
    this.m_isTouchKeyboardDisplayMode = true;
    if ((UnityEngine.Object) this.m_background.GetComponent<Renderer>() != (UnityEngine.Object) null)
      this.m_background.GetComponent<Renderer>().material = this.m_altSearchMaterial;
    this.transform.localPosition = CollectionManagerDisplay.Get().m_activeSearchBone_Win8.transform.localPosition;
    this.HideInput();
    this.ShowInput(fromActivate || W8Touch.Get().IsVirtualKeyboardVisible());
    this.m_xMesh.GetComponent<Renderer>().material.SetColor("_Color", this.m_altSearchColor);
  }

  private void ResetSearchDisplay()
  {
    if (!this.m_isTouchKeyboardDisplayMode)
      return;
    this.m_isTouchKeyboardDisplayMode = false;
    this.m_background.GetComponent<Renderer>().material = this.m_origSearchMaterial;
    this.transform.localPosition = this.m_origSearchPos;
    this.HideInput();
    this.ShowInput(false);
    this.m_xMesh.GetComponent<Renderer>().material.SetColor("_Color", Color.white);
  }

  private void OnKeyboardShown()
  {
    if (!this.m_isActive || this.m_isTouchKeyboardDisplayMode)
      return;
    this.TouchKeyboardSearchDisplay(false);
  }

  private void OnKeyboardHidden()
  {
    if (!this.m_isActive || !this.m_isTouchKeyboardDisplayMode)
      return;
    this.ResetSearchDisplay();
  }

  public delegate void ActivatedListener();

  public delegate void DeactivatedListener(string oldSearchText, string newSearchText);

  public delegate void ClearedListener(bool updateVisuals);
}
