﻿// Decompiled with JetBrains decompiler
// Type: TB11_CoOpv3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TB11_CoOpv3 : MissionEntity
{
  private Card m_bossCard;

  private void SetUpBossCard()
  {
    if (!((Object) this.m_bossCard == (Object) null))
      return;
    Entity entity = GameState.Get().GetEntity(GameState.Get().GetGameEntity().GetTag(GAME_TAG.TAG_SCRIPT_DATA_ENT_1));
    if (entity == null)
      return;
    this.m_bossCard = entity.GetCard();
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BRMA13_1_RESPONSE_05");
    this.PreloadSound("VO_NEFARIAN_NEF2_65");
    this.PreloadSound("VO_BRMA17_1_RESPONSE_85");
    this.PreloadSound("VO_BRMA17_1_TRANSFORM1_80");
    this.PreloadSound("VO_BRMA17_1_TRANSFORM2_81");
    this.PreloadSound("VO_BRMA13_1_TURN1_PT1_02");
    this.PreloadSound("VO_COOPV3_START_01");
    this.PreloadSound("VO_BRMA13_1_HP_PRIEST_08");
    this.PreloadSound("VO_BRMA13_1_HP_SHAMAN_13");
    this.PreloadSound("VO_Innkeeper_Male_Dwarf_Brawl_01");
    this.PreloadSound("VO_Innkeeper_Male_Dwarf_Brawl_02");
    this.PreloadSound("VO_Innkeeper_Male_Dwarf_NEFARIAN_Tavern_Brawl");
  }

  public override AudioSource GetAnnouncerLine(Card heroCard, Card.AnnouncerLineType type)
  {
    if (heroCard.GetEntity().IsControlledByFriendlySidePlayer())
    {
      switch (Random.Range(0, 2))
      {
        case 0:
          return this.GetPreloadedSound("VO_Innkeeper_Male_Dwarf_Brawl_01");
        case 1:
          return this.GetPreloadedSound("VO_Innkeeper_Male_Dwarf_Brawl_02");
      }
    }
    if (heroCard.GetEntity().IsControlledByOpposingSidePlayer())
      return this.GetPreloadedSound("VO_Innkeeper_Male_Dwarf_NEFARIAN_Tavern_Brawl");
    return base.GetAnnouncerLine(heroCard, type);
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB11_CoOpv3.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1F6() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB11_CoOpv3.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1F7() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  public override bool ShouldPlayHeroBlowUpSpells(TAG_PLAYSTATE playState)
  {
    return playState != TAG_PLAYSTATE.WON;
  }
}
