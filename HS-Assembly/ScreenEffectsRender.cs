﻿// Decompiled with JetBrains decompiler
// Type: ScreenEffectsRender
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class ScreenEffectsRender : MonoBehaviour
{
  private const int GLOW_RANDER_BUFFER_RESOLUTION = 512;
  private const string BLOOM_SHADER_NAME = "Hidden/ScreenEffectsGlow";
  private const string DISTORTION_SHADER_NAME = "Hidden/ScreenEffectDistortion";
  private const string GLOW_MASK_SHADER = "Hidden/ScreenEffectsMask";
  [HideInInspector]
  public RenderTexture m_EffectsTexture;
  [HideInInspector]
  public Camera m_EffectsObjectsCamera;
  public bool m_Debug;
  private int m_width;
  private int m_height;
  private int m_previousWidth;
  private int m_previousHeight;
  private RenderTexture m_MaskRenderTexture;
  private Shader m_MaskShader;
  private Shader m_GlowShader;
  private Material m_GlowMaterial;
  private Shader m_DistortionShader;
  private Material m_DistortionMaterial;

  protected Material GlowMaterial
  {
    get
    {
      if ((Object) this.m_GlowMaterial == (Object) null)
      {
        if ((Object) this.m_GlowShader == (Object) null)
        {
          this.m_GlowShader = Shader.Find("Hidden/ScreenEffectsGlow");
          if (!(bool) ((Object) this.m_GlowShader))
            Debug.LogError((object) "Failed to load ScreenEffectsRender Shader: Hidden/ScreenEffectsGlow");
        }
        this.m_GlowMaterial = new Material(this.m_GlowShader);
        SceneUtils.SetHideFlags((Object) this.m_GlowMaterial, HideFlags.HideAndDontSave);
      }
      return this.m_GlowMaterial;
    }
  }

  protected Material DistortionMaterial
  {
    get
    {
      if ((Object) this.m_DistortionMaterial == (Object) null)
      {
        if ((Object) this.m_DistortionShader == (Object) null)
        {
          this.m_DistortionShader = Shader.Find("Hidden/ScreenEffectDistortion");
          if (!(bool) ((Object) this.m_DistortionShader))
            Debug.LogError((object) "Failed to load ScreenEffectsRender Shader: Hidden/ScreenEffectDistortion");
        }
        this.m_DistortionMaterial = new Material(this.m_DistortionShader);
        SceneUtils.SetHideFlags((Object) this.m_DistortionMaterial, HideFlags.HideAndDontSave);
      }
      return this.m_DistortionMaterial;
    }
  }

  private void Awake()
  {
    if ((Object) ScreenEffectsMgr.Get() == (Object) null)
      this.enabled = false;
    if (!((Object) this.m_MaskShader == (Object) null))
      return;
    this.m_MaskShader = Shader.Find("Hidden/ScreenEffectsMask");
    if ((bool) ((Object) this.m_MaskShader))
      return;
    Debug.LogError((object) "Failed to load ScreenEffectsRender Shader: Hidden/ScreenEffectsMask");
  }

  private void Start()
  {
  }

  private void Update()
  {
  }

  private void OnDisable()
  {
    Object.DestroyImmediate((Object) this.GlowMaterial);
    Object.DestroyImmediate((Object) this.DistortionMaterial);
    if ((Object) this.m_MaskRenderTexture != (Object) null)
    {
      Object.DestroyImmediate((Object) this.m_MaskRenderTexture);
      this.m_MaskRenderTexture = (RenderTexture) null;
    }
    if ((Object) this.m_GlowMaterial != (Object) null)
      Object.DestroyImmediate((Object) this.m_GlowMaterial);
    if (!((Object) this.m_DistortionMaterial != (Object) null))
      return;
    Object.DestroyImmediate((Object) this.m_DistortionMaterial);
  }

  private void OnDestroy()
  {
  }

  private void OnEnable()
  {
    this.SetupEffect();
  }

  private void OnPreRender()
  {
    this.RenderEffectsObjects();
  }

  private void OnRenderImage(RenderTexture source, RenderTexture destination)
  {
    this.RenderGlow(source, destination);
  }

  private void SetupEffect()
  {
  }

  private void RenderEffectsObjects()
  {
    if ((Object) this.m_EffectsObjectsCamera == (Object) null)
    {
      this.enabled = false;
    }
    else
    {
      int width = (int) (512.0 * (double) ((float) Screen.width / (float) Screen.height));
      int height = 512;
      if (width != this.m_previousWidth || height != this.m_previousHeight)
      {
        Object.DestroyImmediate((Object) this.m_MaskRenderTexture);
        this.m_MaskRenderTexture = (RenderTexture) null;
      }
      if ((Object) this.m_MaskRenderTexture == (Object) null)
      {
        this.m_MaskRenderTexture = new RenderTexture(width, height, 32);
        this.m_MaskRenderTexture.filterMode = FilterMode.Bilinear;
        this.m_previousWidth = width;
        this.m_previousHeight = height;
      }
      if ((Object) this.m_EffectsObjectsCamera.targetTexture == (Object) null && (Object) this.m_MaskRenderTexture != (Object) null)
        this.m_EffectsObjectsCamera.targetTexture = this.m_MaskRenderTexture;
      this.m_MaskRenderTexture.DiscardContents();
      this.m_EffectsObjectsCamera.RenderWithShader(this.m_MaskShader, "RenderType");
    }
  }

  private void RenderGlow(RenderTexture source, RenderTexture destination)
  {
    int width = this.m_MaskRenderTexture.width;
    int height = this.m_MaskRenderTexture.height;
    RenderTexture temporary1 = RenderTexture.GetTemporary(width, height, 0, source.format);
    temporary1.filterMode = FilterMode.Bilinear;
    this.GlowMaterial.SetFloat("_BlurOffset", 1f);
    Graphics.Blit((Texture) this.m_MaskRenderTexture, temporary1, this.GlowMaterial, 0);
    RenderTexture temporary2 = RenderTexture.GetTemporary(width, height, 0, source.format);
    temporary2.filterMode = FilterMode.Bilinear;
    this.GlowMaterial.SetFloat("_BlurOffset", 2f);
    Graphics.Blit((Texture) temporary1, temporary2, this.GlowMaterial, 0);
    this.GlowMaterial.SetFloat("_BlurOffset", 3f);
    temporary1.DiscardContents();
    Graphics.Blit((Texture) temporary2, temporary1, this.GlowMaterial, 0);
    this.GlowMaterial.SetFloat("_BlurOffset", 5f);
    temporary2.DiscardContents();
    Graphics.Blit((Texture) temporary1, temporary2, this.GlowMaterial, 0);
    this.GlowMaterial.SetTexture("_BlurTex", (Texture) temporary2);
    if (!this.m_Debug)
      Graphics.Blit((Texture) source, destination, this.GlowMaterial, 1);
    else
      Graphics.Blit((Texture) source, destination, this.GlowMaterial, 2);
    RenderTexture.ReleaseTemporary(temporary1);
    RenderTexture.ReleaseTemporary(temporary2);
  }

  private void SetupDistortion()
  {
    if ((Object) this.m_EffectsTexture == (Object) null)
      return;
    this.DistortionMaterial.SetTexture("_EffectTex", (Texture) this.m_EffectsTexture);
  }

  private Material DistortionMaterialRender(RenderTexture source)
  {
    return this.DistortionMaterial;
  }

  private void RenderDistortion(RenderTexture source, RenderTexture destination)
  {
    Graphics.Blit((Texture) source, destination, this.DistortionMaterialRender(source));
  }
}
