﻿// Decompiled with JetBrains decompiler
// Type: FB
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using Facebook;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public sealed class FB : ScriptableObject
{
  public static InitDelegate OnInitComplete;
  public static HideUnityDelegate OnHideUnity;
  private static IFacebook facebook;
  private static string authResponse;
  private static bool isInitCalled;
  private static string appId;
  private static bool cookie;
  private static bool logging;
  private static bool status;
  private static bool xfbml;
  private static bool frictionlessRequests;

  private static IFacebook FacebookImpl
  {
    get
    {
      if (FB.facebook == null)
        throw new NullReferenceException("Facebook object is not yet loaded.  Did you call FB.Init()?");
      return FB.facebook;
    }
  }

  public static string AppId
  {
    get
    {
      return FB.appId;
    }
  }

  public static string UserId
  {
    get
    {
      if (FB.facebook != null)
        return FB.facebook.UserId;
      return string.Empty;
    }
  }

  public static string AccessToken
  {
    get
    {
      if (FB.facebook != null)
        return FB.facebook.AccessToken;
      return string.Empty;
    }
  }

  public static DateTime AccessTokenExpiresAt
  {
    get
    {
      if (FB.facebook != null)
        return FB.facebook.AccessTokenExpiresAt;
      return DateTime.MinValue;
    }
  }

  public static bool IsLoggedIn
  {
    get
    {
      if (FB.facebook != null)
        return FB.facebook.IsLoggedIn;
      return false;
    }
  }

  public static bool IsInitialized
  {
    get
    {
      if (FB.facebook != null)
        return FB.facebook.IsInitialized;
      return false;
    }
  }

  public static void Init(InitDelegate onInitComplete, HideUnityDelegate onHideUnity = null, string authResponse = null)
  {
    FB.Init(onInitComplete, FBSettings.AppId, FBSettings.Cookie, FBSettings.Logging, FBSettings.Status, FBSettings.Xfbml, FBSettings.FrictionlessRequests, onHideUnity, authResponse);
  }

  public static void Init(InitDelegate onInitComplete, string appId, bool cookie = true, bool logging = true, bool status = true, bool xfbml = false, bool frictionlessRequests = true, HideUnityDelegate onHideUnity = null, string authResponse = null)
  {
    FB.appId = appId;
    FB.cookie = cookie;
    FB.logging = logging;
    FB.status = status;
    FB.xfbml = xfbml;
    FB.frictionlessRequests = frictionlessRequests;
    FB.authResponse = authResponse;
    FB.OnInitComplete = onInitComplete;
    FB.OnHideUnity = onHideUnity;
    if (!FB.isInitCalled)
    {
      FBBuildVersionAttribute versionAttributeOfType = FBBuildVersionAttribute.GetVersionAttributeOfType(typeof (IFacebook));
      if (versionAttributeOfType == null)
        FbDebugOverride.Warn("Cannot find Facebook SDK Version");
      else
        FbDebugOverride.Info(string.Format("Using SDK {0}, Build {1}", (object) versionAttributeOfType.SdkVersion, (object) versionAttributeOfType.BuildVersion));
      throw new NotImplementedException("Facebook API does not yet support this platform");
    }
    FbDebugOverride.Warn("FB.Init() has already been called.  You only need to call this once and only once.");
    if (FB.FacebookImpl == null)
      return;
    FB.OnDllLoaded();
  }

  private static void OnDllLoaded()
  {
    FBBuildVersionAttribute versionAttributeOfType = FBBuildVersionAttribute.GetVersionAttributeOfType(FB.FacebookImpl.GetType());
    if (versionAttributeOfType != null)
      FbDebugOverride.Log(string.Format("Finished loading Facebook dll. Version {0} Build {1}", (object) versionAttributeOfType.SdkVersion, (object) versionAttributeOfType.BuildVersion));
    FB.FacebookImpl.Init(FB.OnInitComplete, FB.appId, FB.cookie, FB.logging, FB.status, FB.xfbml, FBSettings.ChannelUrl, FB.authResponse, FB.frictionlessRequests, FB.OnHideUnity);
  }

  public static void Login(string scope = "", FacebookDelegate callback = null)
  {
    FB.FacebookImpl.Login(scope, callback);
  }

  public static void Logout()
  {
    FB.FacebookImpl.Logout();
  }

  public static void AppRequest(string message, OGActionType actionType, string objectId, string[] to, string data = "", string title = "", FacebookDelegate callback = null)
  {
    FB.FacebookImpl.AppRequest(message, actionType, objectId, to, (List<object>) null, (string[]) null, new int?(), data, title, callback);
  }

  public static void AppRequest(string message, OGActionType actionType, string objectId, List<object> filters = null, string[] excludeIds = null, int? maxRecipients = null, string data = "", string title = "", FacebookDelegate callback = null)
  {
    FB.FacebookImpl.AppRequest(message, actionType, objectId, (string[]) null, filters, excludeIds, maxRecipients, data, title, callback);
  }

  public static void AppRequest(string message, string[] to = null, List<object> filters = null, string[] excludeIds = null, int? maxRecipients = null, string data = "", string title = "", FacebookDelegate callback = null)
  {
    FB.FacebookImpl.AppRequest(message, (OGActionType) null, (string) null, to, filters, excludeIds, maxRecipients, data, title, callback);
  }

  public static void Feed(string toId = "", string link = "", string linkName = "", string linkCaption = "", string linkDescription = "", string picture = "", string mediaSource = "", string actionName = "", string actionLink = "", string reference = "", Dictionary<string, string[]> properties = null, FacebookDelegate callback = null)
  {
    FB.FacebookImpl.FeedRequest(toId, link, linkName, linkCaption, linkDescription, picture, mediaSource, actionName, actionLink, reference, properties, callback);
  }

  public static void API(string query, HttpMethod method, FacebookDelegate callback = null, Dictionary<string, string> formData = null)
  {
    FB.FacebookImpl.API(query, method, formData, callback);
  }

  public static void API(string query, HttpMethod method, FacebookDelegate callback, WWWForm formData)
  {
    FB.FacebookImpl.API(query, method, formData, callback);
  }

  [Obsolete("use FB.ActivateApp()")]
  public static void PublishInstall(FacebookDelegate callback = null)
  {
    FB.FacebookImpl.PublishInstall(FB.AppId, callback);
  }

  public static void ActivateApp()
  {
    FB.FacebookImpl.ActivateApp(FB.AppId);
  }

  public static void GetDeepLink(FacebookDelegate callback)
  {
    FB.FacebookImpl.GetDeepLink(callback);
  }

  public static void GameGroupCreate(string name, string description, string privacy = "CLOSED", FacebookDelegate callback = null)
  {
    FB.FacebookImpl.GameGroupCreate(name, description, privacy, callback);
  }

  public static void GameGroupJoin(string id, FacebookDelegate callback = null)
  {
    FB.FacebookImpl.GameGroupJoin(id, callback);
  }

  public sealed class AppEvents
  {
    public static bool LimitEventUsage
    {
      get
      {
        if (FB.facebook != null)
          return FB.facebook.LimitEventUsage;
        return false;
      }
      set
      {
        FB.facebook.LimitEventUsage = value;
      }
    }

    public static void LogEvent(string logEvent, float? valueToSum = null, Dictionary<string, object> parameters = null)
    {
      FB.FacebookImpl.AppEventsLogEvent(logEvent, valueToSum, parameters);
    }

    public static void LogPurchase(float logPurchase, string currency = "USD", Dictionary<string, object> parameters = null)
    {
      FB.FacebookImpl.AppEventsLogPurchase(logPurchase, currency, parameters);
    }
  }

  public sealed class Canvas
  {
    public static void Pay(string product, string action = "purchaseitem", int quantity = 1, int? quantityMin = null, int? quantityMax = null, string requestId = null, string pricepointId = null, string testCurrency = null, FacebookDelegate callback = null)
    {
      FB.FacebookImpl.Pay(product, action, quantity, quantityMin, quantityMax, requestId, pricepointId, testCurrency, callback);
    }

    public static void SetResolution(int width, int height, bool fullscreen, int preferredRefreshRate = 0, params FBScreen.Layout[] layoutParams)
    {
      FBScreen.SetResolution(width, height, fullscreen, preferredRefreshRate, layoutParams);
    }

    public static void SetAspectRatio(int width, int height, params FBScreen.Layout[] layoutParams)
    {
      FBScreen.SetAspectRatio(width, height, layoutParams);
    }
  }

  public sealed class Android
  {
    public static string KeyHash
    {
      get
      {
        AndroidFacebook facebook = FB.facebook as AndroidFacebook;
        if ((UnityEngine.Object) facebook != (UnityEngine.Object) null)
          return facebook.KeyHash;
        return string.Empty;
      }
    }
  }

  public abstract class RemoteFacebookLoader : MonoBehaviour
  {
    private const string facebookNamespace = "Facebook.";
    private const int maxRetryLoadCount = 3;
    private static int retryLoadCount;

    protected abstract string className { get; }

    [DebuggerHidden]
    public static IEnumerator LoadFacebookClass(string className, FB.RemoteFacebookLoader.LoadedDllCallback callback)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new FB.RemoteFacebookLoader.\u003CLoadFacebookClass\u003Ec__Iterator292() { className = className, callback = callback, \u003C\u0024\u003EclassName = className, \u003C\u0024\u003Ecallback = callback };
    }

    [DebuggerHidden]
    private IEnumerator Start()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new FB.RemoteFacebookLoader.\u003CStart\u003Ec__Iterator293() { \u003C\u003Ef__this = this };
    }

    private void OnDllLoaded(IFacebook fb)
    {
      FB.facebook = fb;
      FB.OnDllLoaded();
    }

    public delegate void LoadedDllCallback(IFacebook fb);
  }

  public abstract class CompiledFacebookLoader : MonoBehaviour
  {
    protected abstract IFacebook fb { get; }

    private void Start()
    {
      FB.facebook = this.fb;
      FB.OnDllLoaded();
      UnityEngine.Object.Destroy((UnityEngine.Object) this);
    }
  }
}
