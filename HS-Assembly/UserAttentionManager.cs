﻿// Decompiled with JetBrains decompiler
// Type: UserAttentionManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Linq;

public static class UserAttentionManager
{
  private static UserAttentionBlocker s_blockedReasons;

  private static bool IsBlocked
  {
    get
    {
      return UserAttentionManager.s_blockedReasons != UserAttentionBlocker.NONE;
    }
  }

  private static string CurrentActiveBlockersString
  {
    get
    {
      return string.Join(", ", Enum.GetValues(typeof (UserAttentionBlocker)).Cast<UserAttentionBlocker>().Where<UserAttentionBlocker>((Func<UserAttentionBlocker, bool>) (r => UserAttentionManager.IsBlockedBy(r))).Select<UserAttentionBlocker, string>((Func<UserAttentionBlocker, string>) (r => r.ToString())).ToArray<string>());
    }
  }

  public static event Action<UserAttentionBlocker> OnBlockingStart;

  public static event Action OnBlockingEnd;

  public static bool IsBlockedBy(UserAttentionBlocker attentionCategory)
  {
    return attentionCategory != UserAttentionBlocker.NONE && (UserAttentionManager.s_blockedReasons & attentionCategory) == attentionCategory;
  }

  public static bool CanShowAttentionGrabber(string callerName)
  {
    return UserAttentionManager.CanShowAttentionGrabber(UserAttentionBlocker.NONE, callerName);
  }

  public static bool CanShowAttentionGrabber(UserAttentionBlocker attentionCategory, string callerName)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    UserAttentionManager.\u003CCanShowAttentionGrabber\u003Ec__AnonStorey453 grabberCAnonStorey453 = new UserAttentionManager.\u003CCanShowAttentionGrabber\u003Ec__AnonStorey453();
    // ISSUE: reference to a compiler-generated field
    grabberCAnonStorey453.attentionCategory = attentionCategory;
    // ISSUE: reference to a compiler-generated field
    if ((UserAttentionManager.s_blockedReasons & ~grabberCAnonStorey453.attentionCategory) == UserAttentionBlocker.NONE)
      return true;
    // ISSUE: reference to a compiler-generated method
    string str = string.Join(", ", Enum.GetValues(typeof (UserAttentionBlocker)).Cast<UserAttentionBlocker>().Where<UserAttentionBlocker>(new Func<UserAttentionBlocker, bool>(grabberCAnonStorey453.\u003C\u003Em__31B)).Select<UserAttentionBlocker, string>((Func<UserAttentionBlocker, string>) (r => r.ToString())).ToArray<string>());
    Log.UserAttention.Print("UserAttentionManager attention grabber [{0}] blocked by: {1}", new object[2]
    {
      (object) callerName,
      (object) str
    });
    return false;
  }

  public static void StartBlocking(UserAttentionBlocker attentionCategory)
  {
    if (UserAttentionManager.IsBlockedBy(attentionCategory))
      return;
    bool isBlocked = UserAttentionManager.IsBlocked;
    if (isBlocked)
      Error.AddDevFatal("UserAttentionBlocker.{0} already active, cannot StartBlocking {1}", (object) UserAttentionManager.DumpUserAttentionBlockers("StartBlocking"), (object) attentionCategory);
    UserAttentionManager.s_blockedReasons |= attentionCategory;
    UserAttentionManager.DumpUserAttentionBlockers("StartBlocking[" + (object) attentionCategory + "]");
    if (isBlocked || UserAttentionManager.OnBlockingStart == null)
      return;
    UserAttentionManager.OnBlockingStart(attentionCategory);
  }

  public static void StopBlocking(UserAttentionBlocker attentionCategory)
  {
    bool isBlocked = UserAttentionManager.IsBlocked;
    UserAttentionManager.s_blockedReasons &= ~attentionCategory;
    if (!isBlocked)
      return;
    if (UserAttentionManager.s_blockedReasons == UserAttentionBlocker.NONE)
    {
      Log.UserAttention.Print("UserAttentionManager.StopBlocking[{0}] - all blockers cleared.", (object) attentionCategory);
      if (UserAttentionManager.OnBlockingEnd == null)
        return;
      UserAttentionManager.OnBlockingEnd();
    }
    else
      Log.UserAttention.Print("UserAttentionManager.StopBlocking[{0}]", (object) attentionCategory);
  }

  public static string DumpUserAttentionBlockers(string callerName)
  {
    string activeBlockersString = UserAttentionManager.CurrentActiveBlockersString;
    Log.UserAttention.Print("UserAttentionManager:{0} current blockers: {1}", new object[2]
    {
      (object) callerName,
      (object) activeBlockersString
    });
    return activeBlockersString;
  }
}
