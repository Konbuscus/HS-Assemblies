﻿// Decompiled with JetBrains decompiler
// Type: RewardVisualTiming
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.ComponentModel;

public enum RewardVisualTiming
{
  [Description("adventure_chest")] ADVENTURE_CHEST,
  [Description("out_of_band")] OUT_OF_BAND,
  [Description("immediate")] IMMEDIATE,
  [Description("never")] NEVER,
}
