﻿// Decompiled with JetBrains decompiler
// Type: CollectionHeroDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class CollectionHeroDef : MonoBehaviour
{
  public Material m_previewTexture;
  public Texture m_fauxPlateTexture;
  public MusicPlaylistType m_heroPlaylist;
  public EmoteType m_storePreviewEmote;
  public EmoteType m_storePurchaseEmote;
  public EmoteType m_collectionManagerPreviewEmote;
}
