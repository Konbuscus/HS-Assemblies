﻿// Decompiled with JetBrains decompiler
// Type: AnimPositionResetter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class AnimPositionResetter : MonoBehaviour
{
  private Vector3 m_initialPosition;
  private float m_endTimestamp;
  private float m_delay;

  private void Awake()
  {
    this.m_initialPosition = this.transform.position;
  }

  public static AnimPositionResetter OnAnimStarted(GameObject go, float animTime)
  {
    if ((double) animTime <= 0.0)
      return (AnimPositionResetter) null;
    AnimPositionResetter positionResetter = AnimPositionResetter.RegisterResetter(go);
    positionResetter.OnAnimStarted(animTime);
    return positionResetter;
  }

  public Vector3 GetInitialPosition()
  {
    return this.m_initialPosition;
  }

  public float GetEndTimestamp()
  {
    return this.m_endTimestamp;
  }

  public float GetDelay()
  {
    return this.m_delay;
  }

  private static AnimPositionResetter RegisterResetter(GameObject go)
  {
    if ((Object) go == (Object) null)
      return (AnimPositionResetter) null;
    AnimPositionResetter component = go.GetComponent<AnimPositionResetter>();
    if ((Object) component != (Object) null)
      return component;
    return go.AddComponent<AnimPositionResetter>();
  }

  private void OnAnimStarted(float animTime)
  {
    float num = Time.realtimeSinceStartup + animTime;
    float a = num - this.m_endTimestamp;
    if ((double) a <= 0.0)
      return;
    this.m_delay = Mathf.Min(a, animTime);
    this.m_endTimestamp = num;
    this.StopCoroutine("ResetPosition");
    this.StartCoroutine("ResetPosition");
  }

  [DebuggerHidden]
  private IEnumerator ResetPosition()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AnimPositionResetter.\u003CResetPosition\u003Ec__Iterator323() { \u003C\u003Ef__this = this };
  }
}
