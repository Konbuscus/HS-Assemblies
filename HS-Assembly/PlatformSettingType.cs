﻿// Decompiled with JetBrains decompiler
// Type: PlatformSettingType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum PlatformSettingType
{
  PC,
  Mac,
  iOS,
  Android,
  Tablet,
  MiniTablet,
  Phone,
  Mouse,
  Touch,
  LowMemory,
  MediumMemory,
  HighMemory,
  NormalScreenDensity,
  HighScreenDensity,
  COUNT,
}
