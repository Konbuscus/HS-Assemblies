﻿// Decompiled with JetBrains decompiler
// Type: iTweenIterator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public struct iTweenIterator
{
  private iTweenCollection TweenCollection;
  private int Index;

  public iTweenIterator(iTweenCollection collection)
  {
    this.TweenCollection = collection;
    this.Index = 0;
  }

  public iTween GetNext()
  {
    if (this.TweenCollection == null)
      return (iTween) null;
    while (this.Index < this.TweenCollection.LastIndex)
    {
      iTween tween = this.TweenCollection.Tweens[this.Index];
      ++this.Index;
      if (tween != null)
        return tween;
    }
    return (iTween) null;
  }
}
