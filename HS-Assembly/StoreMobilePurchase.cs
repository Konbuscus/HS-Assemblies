﻿// Decompiled with JetBrains decompiler
// Type: StoreMobilePurchase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class StoreMobilePurchase
{
  public static void ClearProductList()
  {
    StoreMobilePurchase.ClearMobileStoreProducts();
  }

  public static void SetGameAccountIdAndRegion(ulong gameAccountId, int gameRegion)
  {
    Log.Yim.Print("SetGameAccountIdAndRegion(" + (object) gameAccountId + ", " + (object) gameRegion + ")");
    StoreMobilePurchase.SetBattleNetGameAccountIdAndRegion(gameAccountId, gameRegion);
  }

  public static void AddProductById(string mobileProductId)
  {
    StoreMobilePurchase.AddProductToAllProductsList(mobileProductId);
  }

  public static void ValidateAllProducts(string gameObjectName)
  {
    StoreMobilePurchase.ValidateProducts(gameObjectName);
  }

  public static double GetProductPrice(string mobileProductId)
  {
    return StoreMobilePurchase.ProductPriceById(mobileProductId);
  }

  public static string GetLocalizedProductPrice(string mobileProductId)
  {
    return StoreMobilePurchase.LocalizedProductPriceById(mobileProductId);
  }

  public static string GetLocalizedPrice(double price)
  {
    return StoreMobilePurchase.LocalizedPrice(price);
  }

  public static void PurchaseProductById(string mobileProductId)
  {
    StoreMobilePurchase.RequestPurchaseByProductId(mobileProductId, string.Empty);
  }

  public static void PurchaseProductById(string mobileProductId, string transactionId)
  {
    StoreMobilePurchase.RequestPurchaseByProductId(mobileProductId, transactionId);
  }

  public static bool IsWaitingForPurchase()
  {
    return StoreMobilePurchase.DeviceIsWaitingForPurchase();
  }

  public static StoreMobilePurchase.PURCHASE_STATUS_CODE GetStatusCodeOfLastPurchase()
  {
    return (StoreMobilePurchase.PURCHASE_STATUS_CODE) StoreMobilePurchase.GetLastPurchaseStatusCode();
  }

  public static string GetStatusDescriptionOfLastPurchase()
  {
    return StoreMobilePurchase.GetLastPurchaseStatusDescription();
  }

  public static int NumReceiptsAvailable()
  {
    return StoreMobilePurchase.GetNumReceiptsAvailable();
  }

  public static IntPtr GetNextReceipt()
  {
    return StoreMobilePurchase.NextReceipt();
  }

  public static void DismissNextReceipt(bool consume)
  {
    StoreMobilePurchase.DismissReceipt(consume);
  }

  public static void FinishTransactionForId(string transactionId)
  {
    Debug.LogError((object) "FinishTransactionForId should be not be called. Handled natively on mobile platforms.");
  }

  public static void GamePurchaseStatusResponse(string transactionId, bool isSuccess)
  {
    StoreMobilePurchase.OnGamePurchaseStatusResponse(transactionId, isSuccess);
  }

  public static void ThirdPartyPurchaseStatus(string transactionId, Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus status)
  {
    StoreMobilePurchase.OnThirdPartyPurchaseStatus(transactionId, (int) status);
  }

  public static void Reset()
  {
    if (!(bool) StoreManager.HAS_THIRD_PARTY_APP_STORE || ApplicationMgr.GetAndroidStore() == AndroidStore.BLIZZARD)
      return;
    StoreMobilePurchase.OnReset();
  }

  public static void TransactionStatusNotReady()
  {
    StoreMobilePurchase.StoreTransactionStatusNotReady();
  }

  public static void TransactionStatusReady()
  {
    Log.Yim.Print("TransactionStatusReady");
    StoreMobilePurchase.StoreTransactionStatusReady();
  }

  public static void WaitingOnThirdPartyReceipt(string mobileProductId)
  {
    StoreMobilePurchase.RequestThirdPartyReceipt(mobileProductId);
  }

  public static void ProcessNextPendingTransaction()
  {
    StoreMobilePurchase.SubmitNextPendingTransaction();
  }

  public static string ThirdPartyUserId()
  {
    return StoreMobilePurchase.GetThirdPartyUserId();
  }

  private static void SetBattleNetGameAccountIdAndRegion(ulong gameAccountId, int gameRegion)
  {
  }

  private static void ClearMobileStoreProducts()
  {
  }

  private static void AddProductToAllProductsList(string mobileProductId)
  {
  }

  private static void ValidateProducts(string gameObjectName)
  {
  }

  private static void RequestPurchaseByProductId(string mobileProductId, string transactionId)
  {
  }

  private static double ProductPriceById(string mobileProductId)
  {
    return -1.0;
  }

  private static string LocalizedProductPriceById(string mobileProductId)
  {
    return string.Empty;
  }

  private static string LocalizedPrice(double price)
  {
    return string.Empty;
  }

  private static bool DeviceIsWaitingForPurchase()
  {
    return false;
  }

  private static int GetLastPurchaseStatusCode()
  {
    return 3;
  }

  private static string GetLastPurchaseStatusDescription()
  {
    return string.Empty;
  }

  private static int GetNumReceiptsAvailable()
  {
    return 0;
  }

  private static IntPtr NextReceipt()
  {
    return IntPtr.Zero;
  }

  private static void DismissReceipt(bool consume)
  {
  }

  private static int ReceiptForTransactionId(string transactionId, out IntPtr data)
  {
    data = IntPtr.Zero;
    return 0;
  }

  private static string ReceiptStringForTransactionId(string transactionId)
  {
    return string.Empty;
  }

  private static void FinishTransactionId(string transactionId)
  {
  }

  private static void OnGamePurchaseStatusResponse(string transactionId, bool isSuccess)
  {
  }

  private static void OnThirdPartyPurchaseStatus(string transactionId, int status)
  {
  }

  private static void OnReset()
  {
  }

  private static void StoreTransactionStatusNotReady()
  {
  }

  private static void StoreTransactionStatusReady()
  {
  }

  private static void RequestThirdPartyReceipt(string mobileProductId)
  {
  }

  private static void SubmitNextPendingTransaction()
  {
  }

  private static string GetThirdPartyUserId()
  {
    return string.Empty;
  }

  public enum PURCHASE_STATUS_CODE
  {
    PURCHASE_SUCCESSFUL,
    PURCHASE_FAILED,
    PURCHASE_RECOVERED,
    PURCHASE_NOT_AVAILABLE,
  }
}
