﻿// Decompiled with JetBrains decompiler
// Type: TB10_DeckRecipe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TB10_DeckRecipe : MissionEntity
{
  private static readonly Dictionary<int, TB10_DeckRecipe.RecipeMessage> popupMsgs = new Dictionary<int, TB10_DeckRecipe.RecipeMessage>() { { 939, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_OG_DRUID", Delay = 7f } }, { 946, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_OG_HUNTER", Delay = 7f } }, { 947, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_OG_MAGE", Delay = 7f } }, { 938, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_OG_PALADIN", Delay = 7f } }, { 945, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_OG_PRIEST", Delay = 7f } }, { 944, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_OG_ROGUE", Delay = 7f } }, { 937, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_OG_SHAMAN", Delay = 7f } }, { 940, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_OG_WARLOCK", Delay = 7f } }, { 936, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_OG_WARRIOR", Delay = 7f } }, { 1125, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_MSG_DRUID", Delay = 2.5f } }, { 1130, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_MSG_HUNTER", Delay = 2.5f } }, { 1131, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_MSG_MAGE", Delay = 2.5f } }, { 1124, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_MSG_PALADIN", Delay = 2.5f } }, { 1129, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_MSG_PRIEST", Delay = 2.5f } }, { 1128, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_MSG_ROGUE", Delay = 2.5f } }, { 1123, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_MSG_SHAMAN", Delay = 2.5f } }, { 1126, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_MSG_WARLOCK", Delay = 2.5f } }, { 1122, new TB10_DeckRecipe.RecipeMessage() { Message = "TB_DECKRECIPE_MSG_WARRIOR", Delay = 2.5f } } };
  private float delayTime = 2.5f;
  private float popupDuration = 7f;
  private float popupScale = 2.5f;
  private HashSet<int> seen = new HashSet<int>();
  private Notification DeckRecipePopup;
  private Vector3 popUpPos;
  private string textID;
  private bool doPopup;
  private bool doLeftArrow;
  private bool doUpArrow;
  private bool doDownArrow;

  public override void PreloadAssets()
  {
    this.PreloadSound("tutorial_mission_hero_coin_mouse_away");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB10_DeckRecipe.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1F5() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  public struct RecipeMessage
  {
    public string Message;
    public float Delay;
  }
}
