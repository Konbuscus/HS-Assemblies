﻿// Decompiled with JetBrains decompiler
// Type: FriendListChallengeButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using UnityEngine;

public class FriendListChallengeButton : FriendListUIElement
{
  public GameObject m_AvailableIcon;
  public GameObject m_BusyIcon;
  public TooltipZone m_TooltipZone;
  public TextureOffsetStates m_SpectatorIcon;
  public GameObject m_SpectatorIconHighlight;
  public GameObject m_DeleteIcon;
  public NestedPrefab m_ChallengeMenu;
  private BnetPlayer m_player;
  private PegUIElement m_challengeMenuInputBlocker;
  private bool m_isChallengeMenuOpen;
  private Vector3? m_challengeMenuOrigLocalPos;

  public bool CanChallenge()
  {
    return FriendChallengeMgr.Get().CanChallenge(this.m_player);
  }

  public BnetPlayer GetPlayer()
  {
    return this.m_player;
  }

  public bool SetPlayer(BnetPlayer player)
  {
    if (this.m_player == player)
      return false;
    this.m_player = player;
    this.UpdateButton();
    return true;
  }

  public void UpdateButton()
  {
    if (this.UpdateEditModeButtonState())
      this.CloseChallengeMenu();
    else if (this.m_player == null || !this.m_player.IsOnline() || (bgs.FourCC) this.m_player.GetBestProgramId() != (bgs.FourCC) BnetProgramId.HEARTHSTONE)
    {
      this.gameObject.SetActive(false);
      this.CloseChallengeMenu();
    }
    else
    {
      this.gameObject.SetActive(true);
      if (this.UpdateSpectateButtonState())
      {
        this.CloseChallengeMenu();
      }
      else
      {
        bool flag1 = false;
        bool flag2 = false;
        if (this.CanChallenge())
        {
          flag1 = true;
        }
        else
        {
          this.CloseChallengeMenu();
          flag2 = true;
        }
        this.m_AvailableIcon.SetActive(flag1);
        this.m_BusyIcon.SetActive(flag2);
        this.UpdateTooltip();
      }
    }
  }

  protected override bool ShouldBeHighlighted()
  {
    if (this.IsChallengeMenuOpen())
      return true;
    if (!this.CanChallenge())
      return false;
    return base.ShouldBeHighlighted();
  }

  protected override void OnPress()
  {
    base.OnPress();
    if (!UniversalInputManager.Get().IsTouchMode())
      return;
    this.ShowTooltip();
  }

  protected override void OnRelease()
  {
    base.OnRelease();
    if (!this.CanChallenge() || ChatMgr.Get().FriendListFrame.IsInEditMode)
      return;
    if (this.IsChallengeMenuOpen())
    {
      this.CloseChallengeMenu();
    }
    else
    {
      this.HideTooltip();
      this.OpenChallengeMenu();
    }
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    base.OnOver(oldState);
    this.ShowTooltip();
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    base.OnOut(oldState);
    this.HideTooltip();
  }

  private bool UpdateSpectateButtonState()
  {
    BnetGameAccountId hearthstoneGameAccountId = this.m_player.GetHearthstoneGameAccountId();
    SpectatorManager spectatorManager = SpectatorManager.Get();
    bool flag = false;
    string str = (string) null;
    if (spectatorManager.HasInvitedMeToSpectate(hearthstoneGameAccountId))
    {
      str = "HasInvitedMeToSpectate";
      flag = true;
    }
    else if (spectatorManager.CanSpectate(this.m_player))
      str = "CanSpectateThisFriend";
    else if (spectatorManager.IsSpectatingMe(hearthstoneGameAccountId))
      str = "CurrentlySpectatingMe";
    else if (spectatorManager.CanInviteToSpectateMyGame(hearthstoneGameAccountId))
      str = "CanInviteToSpectateMe";
    else if (spectatorManager.IsSpectatingPlayer(hearthstoneGameAccountId))
      str = "CurrentlySpectatingThisFriend";
    else if (spectatorManager.IsInvitedToSpectateMyGame(hearthstoneGameAccountId))
      str = "DisabledInviteToSpectateMe";
    if ((Object) this.m_SpectatorIcon != (Object) null)
    {
      this.m_SpectatorIcon.gameObject.SetActive(str != null);
      if (str != null)
      {
        this.m_SpectatorIcon.CurrentState = str;
        this.m_AvailableIcon.SetActive(false);
        this.m_BusyIcon.SetActive(false);
      }
      if ((Object) this.m_SpectatorIconHighlight != (Object) null)
        this.m_SpectatorIconHighlight.gameObject.SetActive(flag);
    }
    return str != null;
  }

  private bool UpdateEditModeButtonState()
  {
    if (this.m_player == null)
    {
      this.gameObject.SetActive(false);
      return true;
    }
    if ((Object) ChatMgr.Get() != (Object) null && (Object) ChatMgr.Get().FriendListFrame != (Object) null && ChatMgr.Get().FriendListFrame.IsInEditMode)
    {
      this.gameObject.SetActive(true);
      this.m_AvailableIcon.SetActive(false);
      this.m_BusyIcon.SetActive(false);
      this.m_SpectatorIcon.gameObject.SetActive(false);
      if ((Object) this.m_DeleteIcon != (Object) null)
        this.m_DeleteIcon.SetActive(true);
      return true;
    }
    if ((Object) this.m_DeleteIcon != (Object) null)
      this.m_DeleteIcon.SetActive(false);
    return false;
  }

  private void ShowTooltip()
  {
    if (this.IsChallengeMenuOpen())
      return;
    BnetGameAccountId hearthstoneGameAccountId = this.m_player.GetHearthstoneGameAccountId();
    SpectatorManager spectatorManager = SpectatorManager.Get();
    string key1;
    string key2;
    if (spectatorManager.HasInvitedMeToSpectate(hearthstoneGameAccountId))
    {
      key1 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_AVAILABLE_HEADER";
      key2 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_RECEIVED_INVITE_TEXT";
    }
    else if (spectatorManager.CanSpectate(this.m_player))
    {
      key1 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_AVAILABLE_HEADER";
      key2 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_AVAILABLE_TEXT";
    }
    else if (spectatorManager.IsSpectatingMe(hearthstoneGameAccountId))
    {
      key1 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_KICK_HEADER";
      key2 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_KICK_TEXT";
    }
    else if (spectatorManager.CanInviteToSpectateMyGame(hearthstoneGameAccountId))
    {
      if (spectatorManager.IsPlayerSpectatingMyGamesOpposingSide(hearthstoneGameAccountId))
      {
        key1 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_INVITE_OTHER_SIDE_HEADER";
        key2 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_INVITE_OTHER_SIDE_TEXT";
      }
      else
      {
        key1 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_INVITE_HEADER";
        key2 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_INVITE_TEXT";
      }
    }
    else if (spectatorManager.IsInvitedToSpectateMyGame(hearthstoneGameAccountId))
    {
      key1 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_INVITED_HEADER";
      key2 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_INVITED_TEXT";
    }
    else if (spectatorManager.IsSpectatingPlayer(hearthstoneGameAccountId))
    {
      key1 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_SPECTATING_HEADER";
      key2 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_SPECTATING_TEXT";
    }
    else if (spectatorManager.HasPreviouslyKickedMeFromGame(hearthstoneGameAccountId))
    {
      key1 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_PREVIOUSLY_KICKED_HEADER";
      key2 = "GLOBAL_FRIENDLIST_SPECTATE_TOOLTIP_PREVIOUSLY_KICKED_TEXT";
    }
    else
    {
      key1 = "GLOBAL_FRIENDLIST_CHALLENGE_BUTTON_HEADER";
      key2 = FriendChallengeMgr.Get().AmIAvailable() ? (FriendChallengeMgr.Get().CanChallenge(this.m_player) ? "GLOBAL_FRIENDLIST_CHALLENGE_BUTTON_AVAILABLE" : "GLOBAL_FRIENDLIST_CHALLENGE_BUTTON_THEYRE_UNAVAILABLE") : "GLOBAL_FRIENDLIST_CHALLENGE_BUTTON_IM_UNAVAILABLE";
    }
    if (UniversalInputManager.Get().IsTouchMode())
    {
      if (GameStrings.HasKey(key1 + "_TOUCH"))
        key1 += "_TOUCH";
      if (GameStrings.HasKey(key2 + "_TOUCH"))
        key2 += "_TOUCH";
    }
    this.m_TooltipZone.ShowSocialTooltip((Component) this, GameStrings.Get(key1), GameStrings.Format(key2, (object) this.m_player.GetBestName()), 75f, GameLayer.BattleNetDialog);
  }

  private void UpdateTooltip()
  {
    if (!this.m_TooltipZone.IsShowingTooltip())
      return;
    this.HideTooltip();
    this.ShowTooltip();
  }

  private void HideTooltip()
  {
    this.m_TooltipZone.HideTooltip();
  }

  private bool IsChallengeMenuOpen()
  {
    return this.m_isChallengeMenuOpen;
  }

  private void OpenChallengeMenu()
  {
    if ((Object) this.m_ChallengeMenu == (Object) null || this.m_isChallengeMenuOpen)
      return;
    this.m_isChallengeMenuOpen = true;
    this.m_ChallengeMenu.gameObject.SetActive(true);
    if (this.m_challengeMenuOrigLocalPos.HasValue)
      this.m_ChallengeMenu.gameObject.transform.localPosition = this.m_challengeMenuOrigLocalPos.Value;
    Bounds bounds = this.m_ChallengeMenu.PrefabGameObject(false).GetComponent<Collider>().bounds;
    Camera firstByLayer = CameraUtils.FindFirstByLayer(this.m_ChallengeMenu.PrefabGameObject(false).layer);
    Vector3 screenPoint1 = firstByLayer.WorldToScreenPoint(new Vector3(bounds.min.x, bounds.min.y, bounds.center.z));
    if ((double) screenPoint1.y < 0.0)
    {
      if (!this.m_challengeMenuOrigLocalPos.HasValue)
        this.m_challengeMenuOrigLocalPos = new Vector3?(this.m_ChallengeMenu.gameObject.transform.localPosition);
      Vector3 screenPoint2 = firstByLayer.WorldToScreenPoint(this.m_ChallengeMenu.gameObject.transform.position);
      this.m_ChallengeMenu.gameObject.transform.position = firstByLayer.ScreenToWorldPoint(new Vector3(screenPoint2.x, screenPoint2.y - screenPoint1.y, screenPoint2.z));
    }
    this.InitChallengeMenuInputBlocker();
    this.UpdateHighlight();
  }

  private void CloseChallengeMenu()
  {
    if ((Object) this.m_ChallengeMenu == (Object) null || !this.m_isChallengeMenuOpen)
      return;
    this.m_isChallengeMenuOpen = false;
    this.m_ChallengeMenu.gameObject.SetActive(false);
    if ((Object) this.m_challengeMenuInputBlocker != (Object) null)
    {
      Object.Destroy((Object) this.m_challengeMenuInputBlocker.gameObject);
      this.m_challengeMenuInputBlocker = (PegUIElement) null;
    }
    this.UpdateHighlight();
  }

  private void InitChallengeMenuInputBlocker()
  {
    if ((Object) this.m_challengeMenuInputBlocker != (Object) null)
    {
      Object.Destroy((Object) this.m_challengeMenuInputBlocker.gameObject);
      this.m_challengeMenuInputBlocker = (PegUIElement) null;
    }
    GameObject inputBlocker = CameraUtils.CreateInputBlocker(CameraUtils.FindFirstByLayer(this.m_ChallengeMenu.PrefabGameObject(false).layer), "ChallengeMenuInputBlocker");
    inputBlocker.transform.parent = this.m_ChallengeMenu.PrefabGameObject(false).transform;
    this.m_challengeMenuInputBlocker = inputBlocker.AddComponent<PegUIElement>();
    this.m_challengeMenuInputBlocker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnChallengeMenuInputBlockerReleased));
    TransformUtil.SetPosZ((Component) this.m_challengeMenuInputBlocker, this.m_ChallengeMenu.PrefabGameObject(false).transform.position.z + 1f);
  }

  private void OnChallengeMenuInputBlockerReleased(UIEvent e)
  {
    this.CloseChallengeMenu();
  }
}
