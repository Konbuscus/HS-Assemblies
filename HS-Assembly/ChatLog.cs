﻿// Decompiled with JetBrains decompiler
// Type: ChatLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class ChatLog : MonoBehaviour
{
  private const int maxMessageFrames = 500;
  private const GameLayer messageLayer = GameLayer.BattleNetChat;
  public TouchList messageFrames;
  public GameObject cameraTarget;
  public ChatLog.Prefabs prefabs;
  public ChatLog.MessageInfo messageInfo;
  public MobileChatNotification notifications;
  private BnetPlayer receiver;
  private Camera messagesCamera;

  public BnetPlayer Receiver
  {
    get
    {
      return this.receiver;
    }
    set
    {
      if (this.receiver == value)
        return;
      this.receiver = value;
      if (this.receiver == null)
        return;
      this.UpdateMessages();
      if (!this.receiver.IsOnline())
        this.AddOfflineMessage();
      this.messageFrames.ScrollValue = 1f;
    }
  }

  private void Awake()
  {
    this.CreateMessagesCamera();
    if ((UnityEngine.Object) this.notifications != (UnityEngine.Object) null)
      this.notifications.Notified += new MobileChatNotification.NotifiedEvent(this.OnNotified);
    BnetWhisperMgr.Get().AddWhisperListener(new BnetWhisperMgr.WhisperCallback(this.OnWhisper));
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
  }

  private void OnDestroy()
  {
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    BnetWhisperMgr.Get().RemoveWhisperListener(new BnetWhisperMgr.WhisperCallback(this.OnWhisper));
    if (!((UnityEngine.Object) this.notifications != (UnityEngine.Object) null))
      return;
    this.notifications.Notified -= new MobileChatNotification.NotifiedEvent(this.OnNotified);
  }

  public void OnResize()
  {
    this.UpdateMessagesCamera();
  }

  public void OnWhisperFailed()
  {
    this.AddOfflineMessage();
  }

  private void OnWhisper(BnetWhisper whisper, object userData)
  {
    if (this.receiver == null || !WhisperUtil.IsSpeakerOrReceiver(this.receiver, whisper))
      return;
    this.AddWhisperMessage(whisper);
    this.messageFrames.ScrollValue = 1f;
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    BnetPlayerChange change = changelist.FindChange(this.receiver);
    if (change == null)
      return;
    BnetPlayer oldPlayer = change.GetOldPlayer();
    BnetPlayer newPlayer = change.GetNewPlayer();
    if (oldPlayer != null && oldPlayer.IsOnline() == newPlayer.IsOnline())
      return;
    if (newPlayer.IsOnline())
      this.AddOnlineMessage();
    else
      this.AddOfflineMessage();
  }

  private void OnNotified(string text)
  {
    this.AddSystemMessage(text, this.messageInfo.notificationColor);
  }

  private void UpdateMessages()
  {
    List<MobileChatLogMessageFrame> list = this.messageFrames.Select<ITouchListItem, MobileChatLogMessageFrame>((Func<ITouchListItem, MobileChatLogMessageFrame>) (i => i.GetComponent<MobileChatLogMessageFrame>())).ToList<MobileChatLogMessageFrame>();
    this.messageFrames.Clear();
    using (List<MobileChatLogMessageFrame>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    List<BnetWhisper> whispersWithPlayer = BnetWhisperMgr.Get().GetWhispersWithPlayer(this.receiver);
    if (whispersWithPlayer != null && whispersWithPlayer.Count > 0)
    {
      for (int index = Mathf.Max(whispersWithPlayer.Count - 500, 0); index < whispersWithPlayer.Count; ++index)
        this.AddWhisperMessage(whispersWithPlayer[index]);
    }
    this.OnMessagesAdded();
  }

  private void AddWhisperMessage(BnetWhisper whisper)
  {
    string message = ChatUtils.GetMessage(whisper);
    this.messageFrames.Add((ITouchListItem) this.CreateMessage(!WhisperUtil.IsSpeaker(this.receiver, whisper) ? this.prefabs.myMessage : this.prefabs.theirMessage, message));
  }

  private void AddMyMessage(string message)
  {
    this.messageFrames.Add((ITouchListItem) this.CreateMessage(this.prefabs.myMessage, ChatUtils.GetMessage(message)));
    this.OnMessagesAdded();
  }

  private void AddSystemMessage(string message, Color color)
  {
    this.messageFrames.Add((ITouchListItem) this.CreateMessage(this.prefabs.systemMessage, message, color));
    this.OnMessagesAdded();
  }

  private void AddOnlineMessage()
  {
    this.AddSystemMessage(GameStrings.Format("GLOBAL_CHAT_RECEIVER_ONLINE", (object) this.receiver.GetBestName()), this.messageInfo.infoColor);
  }

  private void AddOfflineMessage()
  {
    this.AddSystemMessage(GameStrings.Format("GLOBAL_CHAT_RECEIVER_OFFLINE", (object) this.receiver.GetBestName()), this.messageInfo.errorColor);
  }

  private void OnMessagesAdded()
  {
    if (this.messageFrames.Count > 500)
    {
      ITouchListItem messageFrame = this.messageFrames[0];
      this.messageFrames.RemoveAt(0);
      UnityEngine.Object.Destroy((UnityEngine.Object) messageFrame.gameObject);
    }
    this.messageFrames.ScrollValue = 1f;
  }

  private MobileChatLogMessageFrame CreateMessage(MobileChatLogMessageFrame prefab, string message)
  {
    MobileChatLogMessageFrame chatLogMessageFrame = UnityEngine.Object.Instantiate<MobileChatLogMessageFrame>(prefab);
    chatLogMessageFrame.Width = (float) ((double) this.messageFrames.ClipSize.x - (double) this.messageFrames.padding.x - 10.0);
    chatLogMessageFrame.Message = message;
    SceneUtils.SetLayer((Component) chatLogMessageFrame, GameLayer.BattleNetChat);
    return chatLogMessageFrame;
  }

  private MobileChatLogMessageFrame CreateMessage(MobileChatLogMessageFrame prefab, string message, Color color)
  {
    MobileChatLogMessageFrame message1 = this.CreateMessage(prefab, message);
    message1.Color = color;
    return message1;
  }

  private void CreateMessagesCamera()
  {
    this.messagesCamera = new GameObject("MessagesCamera")
    {
      transform = {
        parent = this.messageFrames.transform,
        localPosition = new Vector3(0.0f, 0.0f, -100f)
      }
    }.AddComponent<Camera>();
    this.messagesCamera.orthographic = true;
    this.messagesCamera.depth = (float) (BnetBar.CameraDepth + 1);
    this.messagesCamera.clearFlags = CameraClearFlags.Depth;
    this.messagesCamera.cullingMask = GameLayer.BattleNetChat.LayerBit();
    this.UpdateMessagesCamera();
  }

  private Bounds GetBoundsFromGameObject(GameObject go)
  {
    Renderer component1 = go.GetComponent<Renderer>();
    if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
      return component1.bounds;
    Collider component2 = go.GetComponent<Collider>();
    if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
      return component2.bounds;
    return new Bounds();
  }

  private void UpdateMessagesCamera()
  {
    Camera bnetCamera = BaseUI.Get().GetBnetCamera();
    Bounds boundsFromGameObject = this.GetBoundsFromGameObject(this.cameraTarget);
    Vector3 screenPoint1 = bnetCamera.WorldToScreenPoint(boundsFromGameObject.min);
    Vector3 screenPoint2 = bnetCamera.WorldToScreenPoint(boundsFromGameObject.max);
    this.messagesCamera.pixelRect = new Rect(screenPoint1.x, screenPoint1.y, screenPoint2.x - screenPoint1.x, screenPoint2.y - screenPoint1.y);
    this.messagesCamera.orthographicSize = this.messagesCamera.rect.height * bnetCamera.orthographicSize;
  }

  [Conditional("CHATLOG_DEBUG")]
  private void AssignMessageFrameNames()
  {
    for (int index = 0; index < this.messageFrames.Count; ++index)
    {
      MobileChatLogMessageFrame component = this.messageFrames[index].GetComponent<MobileChatLogMessageFrame>();
      component.name = string.Format("MessageFrame {0} ({1})", (object) index, (object) component.Message);
    }
  }

  [Serializable]
  public class Prefabs
  {
    public MobileChatLogMessageFrame myMessage;
    public MobileChatLogMessageFrame theirMessage;
    public MobileChatLogMessageFrame systemMessage;
  }

  [Serializable]
  public class MessageInfo
  {
    public Color infoColor = Color.yellow;
    public Color errorColor = Color.red;
    public Color notificationColor = Color.cyan;
  }
}
