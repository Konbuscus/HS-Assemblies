﻿// Decompiled with JetBrains decompiler
// Type: RotatedItemDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RotatedItemDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_RotationEvent;
  [SerializeField]
  private int m_ItemType;
  [SerializeField]
  private int m_ItemId;
  [SerializeField]
  private int m_CardSetId;
  [SerializeField]
  private int m_CardId;

  [DbfField("ROTATION_EVENT", "EVENT_TIMING.EVENT key that specifies the date and time that rotation occurs.")]
  public string RotationEvent
  {
    get
    {
      return this.m_RotationEvent;
    }
  }

  [DbfField("ITEM_TYPE", "Type of item (see enum RotatedItemType in PegasusShared.proto) E.g. {Booster = 1, Adventure = 2, CardSet = 3, etc.}")]
  public int ItemType
  {
    get
    {
      return this.m_ItemType;
    }
  }

  [DbfField("ITEM_ID", "BOOSTER.ID if this is an expansion (for STORE). ADVENTURE.ID if this is an adventure (for STORE).")]
  public int ItemId
  {
    get
    {
      return this.m_ItemId;
    }
  }

  [DbfField("CARD_SET_ID", "CARD.CARD_SET_ID if this is Card Set (for VALIDATION)")]
  public int CardSetId
  {
    get
    {
      return this.m_CardSetId;
    }
  }

  [DbfField("CARD_ID", "CARD.ID if this is a card (for VALIDATION).")]
  public int CardId
  {
    get
    {
      return this.m_CardId;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    RotatedItemDbfAsset rotatedItemDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (RotatedItemDbfAsset)) as RotatedItemDbfAsset;
    if ((UnityEngine.Object) rotatedItemDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("RotatedItemDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < rotatedItemDbfAsset.Records.Count; ++index)
      rotatedItemDbfAsset.Records[index].StripUnusedLocales();
    records = (object) rotatedItemDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetRotationEvent(string v)
  {
    this.m_RotationEvent = v;
  }

  public void SetItemType(int v)
  {
    this.m_ItemType = v;
  }

  public void SetItemId(int v)
  {
    this.m_ItemId = v;
  }

  public void SetCardSetId(int v)
  {
    this.m_CardSetId = v;
  }

  public void SetCardId(int v)
  {
    this.m_CardId = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (RotatedItemDbfRecord.\u003C\u003Ef__switch\u0024map5C == null)
      {
        // ISSUE: reference to a compiler-generated field
        RotatedItemDbfRecord.\u003C\u003Ef__switch\u0024map5C = new Dictionary<string, int>(6)
        {
          {
            "ID",
            0
          },
          {
            "ROTATION_EVENT",
            1
          },
          {
            "ITEM_TYPE",
            2
          },
          {
            "ITEM_ID",
            3
          },
          {
            "CARD_SET_ID",
            4
          },
          {
            "CARD_ID",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (RotatedItemDbfRecord.\u003C\u003Ef__switch\u0024map5C.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.RotationEvent;
          case 2:
            return (object) this.ItemType;
          case 3:
            return (object) this.ItemId;
          case 4:
            return (object) this.CardSetId;
          case 5:
            return (object) this.CardId;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (RotatedItemDbfRecord.\u003C\u003Ef__switch\u0024map5D == null)
    {
      // ISSUE: reference to a compiler-generated field
      RotatedItemDbfRecord.\u003C\u003Ef__switch\u0024map5D = new Dictionary<string, int>(6)
      {
        {
          "ID",
          0
        },
        {
          "ROTATION_EVENT",
          1
        },
        {
          "ITEM_TYPE",
          2
        },
        {
          "ITEM_ID",
          3
        },
        {
          "CARD_SET_ID",
          4
        },
        {
          "CARD_ID",
          5
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!RotatedItemDbfRecord.\u003C\u003Ef__switch\u0024map5D.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetRotationEvent((string) val);
        break;
      case 2:
        this.SetItemType((int) val);
        break;
      case 3:
        this.SetItemId((int) val);
        break;
      case 4:
        this.SetCardSetId((int) val);
        break;
      case 5:
        this.SetCardId((int) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (RotatedItemDbfRecord.\u003C\u003Ef__switch\u0024map5E == null)
      {
        // ISSUE: reference to a compiler-generated field
        RotatedItemDbfRecord.\u003C\u003Ef__switch\u0024map5E = new Dictionary<string, int>(6)
        {
          {
            "ID",
            0
          },
          {
            "ROTATION_EVENT",
            1
          },
          {
            "ITEM_TYPE",
            2
          },
          {
            "ITEM_ID",
            3
          },
          {
            "CARD_SET_ID",
            4
          },
          {
            "CARD_ID",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (RotatedItemDbfRecord.\u003C\u003Ef__switch\u0024map5E.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (int);
          case 3:
            return typeof (int);
          case 4:
            return typeof (int);
          case 5:
            return typeof (int);
        }
      }
    }
    return (System.Type) null;
  }
}
