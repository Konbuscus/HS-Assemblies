﻿// Decompiled with JetBrains decompiler
// Type: SpellAreaEffectInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class SpellAreaEffectInfo
{
  public bool m_Enabled = true;
  public Spell m_Prefab;
  public bool m_UseSuperSpellLocation;
  public SpellLocation m_Location;
  public bool m_SetParentToLocation;
  public SpellFacing m_Facing;
  public SpellFacingOptions m_FacingOptions;
  public float m_SpawnDelaySecMin;
  public float m_SpawnDelaySecMax;
}
