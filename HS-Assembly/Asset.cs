﻿// Decompiled with JetBrains decompiler
// Type: Asset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class Asset
{
  private string m_name;
  private AssetFamily m_family;
  private bool m_persistent;
  private string m_path;
  private Locale m_locale;

  public Asset(string name, AssetFamily family, bool persistent = false)
  {
    this.m_name = name;
    this.m_family = family;
    this.m_persistent = persistent;
    this.m_path = string.Format(AssetPathInfo.FamilyInfo[family].format, (object) name);
  }

  public string GetName()
  {
    return this.m_name;
  }

  public AssetFamily GetFamily()
  {
    return this.m_family;
  }

  public bool IsPersistent()
  {
    return this.m_persistent;
  }

  public string GetDirectory()
  {
    return AssetPathInfo.FamilyInfo[this.m_family].sourceDir;
  }

  public string[] GetExtensions()
  {
    return AssetPathInfo.FamilyInfo[this.m_family].exts;
  }

  public string GetPath()
  {
    return this.m_path;
  }

  public string GetPath(Locale locale)
  {
    return FileUtils.MakeLocalizedPathFromSourcePath(locale, this.m_path);
  }

  public Locale GetLocale()
  {
    return this.m_locale;
  }

  public void SetLocale(Locale locale)
  {
    this.m_locale = locale;
  }
}
