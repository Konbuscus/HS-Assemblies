﻿// Decompiled with JetBrains decompiler
// Type: KAR00_Prologue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR00_Prologue : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void StartGameplaySoundtracks()
  {
    MusicManager.Get().StartPlaylist(MusicPlaylistType.InGame_KarazhanPrologue);
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Malchezaar_Male_Demon_PrologueEmoteResponse_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_PrologueHeroPower_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_PrologueWin_01");
    this.PreloadSound("VO_Malchezaar_Male_Demon_PrologueTurn1_04");
    this.PreloadSound("VO_Malchezaar_Male_Demon_PrologueTurn11_02");
    this.PreloadSound("VO_Medivh_Male_Human_PrologueTurn1_02");
    this.PreloadSound("VO_Medivh_Male_Human_PrologueTurn3_02");
    this.PreloadSound("VO_Medivh_Male_Human_PrologueTurn9_01");
    this.PreloadSound("VO_Medivh_Male_Human_PrologueTurn11_01");
    this.PreloadSound("VO_Medivh_Male_Human_PrologueHeroPower_01");
    this.PreloadSound("VO_Medivh_Male_Human_PrologueWin_01");
    this.PreloadSound("VO_Moroes_Male_Human_PrologueTurn5_03");
    this.PreloadSound("VO_Moroes_Male_Human_PrologueTurn7_01");
    this.PreloadSound("VO_Moroes_Male_Human_PrologueTurn9_02");
    this.PreloadSound("VO_Moroes_Male_Human_PrologueTurn11_01");
    this.PreloadSound("VO_Moroes_Male_Human_PrologueWin_02");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Malchezaar_Male_Demon_PrologueEmoteResponse_01",
            m_stringTag = "VO_Malchezaar_Male_Demon_PrologueEmoteResponse_01"
          }
        }
      }
    };
  }

  public override bool ShouldPlayHeroBlowUpSpells(TAG_PLAYSTATE playState)
  {
    return playState != TAG_PLAYSTATE.WON;
  }

  public override string GetVictoryScreenBannerText()
  {
    return GameStrings.Get("GAMEPLAY_END_OF_GAME_VICTORY_MAYBE");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR00_Prologue.\u003CHandleMissionEventWithTiming\u003Ec__Iterator188() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR00_Prologue.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator189() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR00_Prologue.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator18A() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR00_Prologue.\u003CHandleGameOverWithTiming\u003Ec__Iterator18B() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
