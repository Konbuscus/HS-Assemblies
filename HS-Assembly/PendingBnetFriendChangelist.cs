﻿// Decompiled with JetBrains decompiler
// Type: PendingBnetFriendChangelist
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections.Generic;

public class PendingBnetFriendChangelist
{
  private List<BnetPlayer> m_friends = new List<BnetPlayer>();

  public List<BnetPlayer> GetFriends()
  {
    return this.m_friends;
  }

  public bool Add(BnetPlayer friend)
  {
    if (this.m_friends.Contains(friend))
      return false;
    this.m_friends.Add(friend);
    return true;
  }

  public bool Remove(BnetPlayer friend)
  {
    return this.m_friends.Remove(friend);
  }

  public void Clear()
  {
    this.m_friends.Clear();
  }

  public int GetCount()
  {
    return this.m_friends.Count;
  }

  public BnetPlayer FindFriend(BnetAccountId id)
  {
    using (List<BnetPlayer>.Enumerator enumerator = this.m_friends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer current = enumerator.Current;
        if ((BnetEntityId) current.GetAccountId() == (BnetEntityId) id)
          return current;
      }
    }
    return (BnetPlayer) null;
  }

  public BnetPlayer FindFriend(BnetGameAccountId id)
  {
    using (List<BnetPlayer>.Enumerator enumerator = this.m_friends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer current = enumerator.Current;
        if (current.HasGameAccount(id))
          return current;
      }
    }
    return (BnetPlayer) null;
  }

  public bool IsFriend(BnetPlayer player)
  {
    if (this.m_friends.Contains(player))
      return true;
    if (player == null)
      return false;
    BnetAccountId accountId = player.GetAccountId();
    if ((BnetEntityId) accountId != (BnetEntityId) null)
      return this.IsFriend(accountId);
    using (Map<BnetGameAccountId, BnetGameAccount>.KeyCollection.Enumerator enumerator = player.GetGameAccounts().Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (this.IsFriend(enumerator.Current))
          return true;
      }
    }
    return false;
  }

  public bool IsFriend(BnetAccountId id)
  {
    return this.FindFriend(id) != null;
  }

  public bool IsFriend(BnetGameAccountId id)
  {
    return this.FindFriend(id) != null;
  }

  public BnetFriendChangelist CreateChangelist()
  {
    BnetFriendChangelist friendChangelist = new BnetFriendChangelist();
    for (int index = this.m_friends.Count - 1; index >= 0; --index)
    {
      BnetPlayer friend = this.m_friends[index];
      if (friend.IsDisplayable())
      {
        friendChangelist.AddAddedFriend(friend);
        this.m_friends.RemoveAt(index);
      }
    }
    return friendChangelist;
  }
}
