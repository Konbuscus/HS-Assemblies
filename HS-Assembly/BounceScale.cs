﻿// Decompiled with JetBrains decompiler
// Type: BounceScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BounceScale : MonoBehaviour
{
  public float m_Time;

  public void BounceyScale()
  {
    Vector3 localScale = this.transform.localScale;
    this.transform.localScale = Vector3.zero;
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) localScale, (object) "time", (object) this.m_Time, (object) "easetype", (object) iTween.EaseType.easeOutElastic));
  }
}
