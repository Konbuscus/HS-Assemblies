﻿// Decompiled with JetBrains decompiler
// Type: UnopenedPack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UnopenedPack : PegUIElement
{
  public DragRotatorInfo m_DragRotatorInfo = new DragRotatorInfo() { m_PitchInfo = new DragRotatorAxisInfo() { m_ForceMultiplier = 3f, m_MinDegrees = -55f, m_MaxDegrees = 55f, m_RestSeconds = 2f }, m_RollInfo = new DragRotatorAxisInfo() { m_ForceMultiplier = 4.5f, m_MinDegrees = -60f, m_MaxDegrees = 60f, m_RestSeconds = 2f } };
  private NetCache.BoosterStack m_boosterStack = new NetCache.BoosterStack() { Id = 0, Count = 0 };
  public UnopenedPackStack m_SingleStack;
  public UnopenedPackStack m_MultipleStack;
  public GameObject m_LockRibbon;
  public Spell m_AlertEvent;
  public Spell m_DragStartEvent;
  public Spell m_DragStopEvent;
  private UnopenedPack m_draggedPack;
  private UnopenedPack m_creatorPack;

  protected override void Awake()
  {
    base.Awake();
    this.UpdateState();
  }

  public NetCache.BoosterStack GetBoosterStack()
  {
    return this.m_boosterStack;
  }

  public void AddBoosters(int numNewBoosters)
  {
    this.m_boosterStack.Count += numNewBoosters;
    this.UpdateState();
  }

  public void AddBooster()
  {
    this.AddBoosters(1);
  }

  public void SetBoosterStack(NetCache.BoosterStack boosterStack)
  {
    this.m_boosterStack = boosterStack;
    this.UpdateState();
  }

  public void RemoveBooster()
  {
    --this.m_boosterStack.Count;
    if (this.m_boosterStack.Count < 0)
    {
      Debug.LogWarning((object) "UnopenedPack.RemoveBooster(): Removed a booster pack from a stack with no boosters");
      this.m_boosterStack.Count = 0;
    }
    this.UpdateState();
  }

  public UnopenedPack AcquireDraggedPack()
  {
    if (this.m_boosterStack.Id == 10 && !Options.Get().GetBool(Option.HAS_HEARD_TGT_PACK_VO, false))
    {
      Options.Get().SetBool(Option.HAS_HEARD_TGT_PACK_VO, true);
      NotificationManager.Get().CreateTirionQuote("VO_TIRION_INTRO_02", "VO_TIRION_INTRO_02", true);
    }
    if ((Object) this.m_draggedPack != (Object) null)
      return this.m_draggedPack;
    this.m_draggedPack = (UnopenedPack) Object.Instantiate((Object) this, this.transform.position, this.transform.rotation);
    TransformUtil.CopyWorldScale((Component) this.m_draggedPack, (Component) this);
    this.m_draggedPack.transform.parent = this.transform.parent;
    UIBScrollableItem component = this.m_draggedPack.GetComponent<UIBScrollableItem>();
    if ((Object) component != (Object) null)
      component.m_active = UIBScrollableItem.ActiveState.Inactive;
    this.m_draggedPack.m_creatorPack = this;
    this.m_draggedPack.gameObject.AddComponent<DragRotator>().SetInfo(this.m_DragRotatorInfo);
    this.m_draggedPack.m_DragStartEvent.Activate();
    return this.m_draggedPack;
  }

  public void ReleaseDraggedPack()
  {
    if ((Object) this.m_draggedPack == (Object) null)
      return;
    UnopenedPack draggedPack = this.m_draggedPack;
    this.m_draggedPack = (UnopenedPack) null;
    draggedPack.m_DragStopEvent.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnDragStopSpellStateFinished), (object) draggedPack);
    draggedPack.m_DragStopEvent.Activate();
    this.UpdateState();
  }

  public UnopenedPack GetDraggedPack()
  {
    return this.m_draggedPack;
  }

  public UnopenedPack GetCreatorPack()
  {
    return this.m_creatorPack;
  }

  public void PlayAlert()
  {
    this.m_AlertEvent.ActivateState(SpellStateType.BIRTH);
  }

  public void StopAlert()
  {
    this.m_AlertEvent.ActivateState(SpellStateType.DEATH);
  }

  public bool CanOpenPack()
  {
    NetCache.BoosterStack boosterStack = this.GetBoosterStack();
    if (boosterStack == null)
      return false;
    BoosterDbfRecord record = GameDbf.Booster.GetRecord(boosterStack.Id);
    SpecialEventType outVal;
    if (record == null || string.IsNullOrEmpty(record.OpenPackEvent) || !EnumUtils.TryGetEnum<SpecialEventType>(record.OpenPackEvent, out outVal))
      return false;
    if (outVal == SpecialEventType.IGNORE)
      return true;
    return SpecialEventManager.Get().IsEventActive(outVal, false);
  }

  private void UpdateState()
  {
    bool flag1 = this.CanOpenPack();
    if ((Object) this.m_LockRibbon != (Object) null)
      this.m_LockRibbon.SetActive(!flag1);
    bool flag2 = this.m_boosterStack.Count == 0;
    bool flag3 = this.m_boosterStack.Count > 1 && flag1;
    this.m_SingleStack.m_RootObject.SetActive(!flag3 && !flag2);
    this.m_MultipleStack.m_RootObject.SetActive(flag3 && !flag2);
    this.m_MultipleStack.m_AmountText.enabled = flag3;
    if (!flag3)
      return;
    this.m_MultipleStack.m_AmountText.Text = this.m_boosterStack.Count.ToString();
  }

  private void OnDragStopSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    Object.Destroy((Object) ((Component) userData).gameObject);
  }
}
