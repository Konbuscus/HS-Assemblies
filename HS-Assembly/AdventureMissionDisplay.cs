﻿// Decompiled with JetBrains decompiler
// Type: AdventureMissionDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class AdventureMissionDisplay : MonoBehaviour
{
  [CustomEditField(Sections = "UI")]
  public float m_BossPowerCardScale = 1f;
  [CustomEditField(Sections = "UI/Animation")]
  public float m_CoinFlipDelayTime = 1.25f;
  [CustomEditField(Sections = "UI/Animation")]
  public float m_CoinFlipAnimationTime = 1f;
  [SerializeField]
  private Vector3 m_BossWingOffset = Vector3.zero;
  private List<AdventureWing> m_BossWings = new List<AdventureWing>();
  private Map<ScenarioDbId, FullDef> m_BossPortraitDefCache = new Map<ScenarioDbId, FullDef>();
  private Map<ScenarioDbId, FullDef> m_BossPowerDefCache = new Map<ScenarioDbId, FullDef>();
  private List<AdventureWing> m_WingsToGiveBigChest = new List<AdventureWing>();
  private Map<ScenarioDbId, AdventureMissionDisplay.BossInfo> m_BossInfoCache = new Map<ScenarioDbId, AdventureMissionDisplay.BossInfo>();
  private const float s_ScreenBackTransitionDelay = 1.8f;
  [CustomEditField(Sections = "Boss Layout Settings")]
  public GameObject m_BossWingContainer;
  [CustomEditField(Sections = "Boss Info")]
  public UberText m_BossTitle;
  [CustomEditField(Sections = "Boss Info")]
  public UberText m_BossDescription;
  [CustomEditField(Sections = "UI")]
  public UberText m_AdventureTitle;
  [CustomEditField(Sections = "UI")]
  public PegUIElement m_BackButton;
  [CustomEditField(Sections = "UI")]
  public PlayButton m_ChooseButton;
  [CustomEditField(Sections = "UI")]
  public GameObject m_BossPortraitContainer;
  [CustomEditField(Sections = "UI")]
  public GameObject m_BossPowerContainer;
  [CustomEditField(Sections = "UI")]
  public PegUIElement m_BossPowerHoverArea;
  [CustomEditField(Sections = "UI", T = EditType.GAME_OBJECT)]
  public MeshRenderer m_WatermarkIcon;
  [CustomEditField(Sections = "UI")]
  public AdventureRewardsDisplayArea m_RewardsDisplay;
  [CustomEditField(Sections = "UI")]
  public GameObject m_ClickBlocker;
  [CustomEditField(Sections = "UI/Scroll Bar")]
  public UIBScrollable m_ScrollBar;
  [CustomEditField(Sections = "UI/Preview Pane")]
  public AdventureRewardsPreview m_PreviewPane;
  public AdventureMissionDisplayTray m_advMissionDisplayTray;
  private static AdventureMissionDisplay s_instance;
  private AdventureWingProgressDisplay m_progressDisplay;
  private GameObject m_BossWingBorder;
  private AdventureBossCoin m_SelectedCoin;
  private int m_AssetsLoading;
  private Actor m_BossActor;
  private Actor m_BossPowerActor;
  private Actor m_BossPowerBigCard;
  private FullDef m_SelectedHeroPowerFullDef;
  private int m_DisableSelectionCount;
  private bool m_ShowingRewardsPreview;
  private int m_TotalBosses;
  private int m_TotalBossesDefeated;
  private bool m_BossJustDefeated;
  private bool m_WaitingForClassChallengeUnlocks;
  private int m_ClassChallengeUnlockShowing;
  private MusicPlaylistType m_mainMusic;

  [CustomEditField(Sections = "Boss Layout Settings")]
  public Vector3 BossWingOffset
  {
    get
    {
      return this.m_BossWingOffset;
    }
    set
    {
      this.m_BossWingOffset = value;
      this.UpdateWingPositions();
    }
  }

  public static AdventureMissionDisplay Get()
  {
    return AdventureMissionDisplay.s_instance;
  }

  private void Awake()
  {
    AdventureMissionDisplay.s_instance = this;
    this.m_mainMusic = MusicManager.Get().GetCurrentPlaylist();
    AdventureConfig adventureConfig = AdventureConfig.Get();
    AdventureDbId selectedAdventure = adventureConfig.GetSelectedAdventure();
    AdventureModeDbId selectedMode = adventureConfig.GetSelectedMode();
    this.m_AdventureTitle.Text = (string) GameUtils.GetAdventureDataRecord((int) selectedAdventure, (int) selectedMode).Name;
    List<AdventureMissionDisplay.WingCreateParams> paramsList = this.BuildWingCreateParamsList();
    this.m_WingsToGiveBigChest.Clear();
    AdventureDef adventureDef = AdventureScene.Get().GetAdventureDef(selectedAdventure);
    AdventureSubDef subDef = adventureDef.GetSubDef(selectedMode);
    if (!string.IsNullOrEmpty(adventureDef.m_WingBottomBorderPrefab))
    {
      this.m_BossWingBorder = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(adventureDef.m_WingBottomBorderPrefab), true, false);
      GameUtils.SetParent(this.m_BossWingBorder, this.m_BossWingContainer, false);
    }
    this.AddAssetToLoad(3);
    using (List<AdventureMissionDisplay.WingCreateParams>.Enumerator enumerator = paramsList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.AddAssetToLoad(enumerator.Current.m_BossCreateParams.Count * 2);
    }
    this.m_TotalBosses = 0;
    this.m_TotalBossesDefeated = 0;
    int num1 = 0;
    int num2 = paramsList.Count - 1;
    if (!string.IsNullOrEmpty((string) ((MobileOverrideValue<string>) adventureDef.m_ProgressDisplayPrefab)))
    {
      this.m_progressDisplay = GameUtils.LoadGameObjectWithComponent<AdventureWingProgressDisplay>((string) ((MobileOverrideValue<string>) adventureDef.m_ProgressDisplayPrefab));
      if ((UnityEngine.Object) this.m_progressDisplay != (UnityEngine.Object) null)
      {
        ++num1;
        ++num2;
        if ((UnityEngine.Object) this.m_BossWingContainer != (UnityEngine.Object) null)
          GameUtils.SetParent((Component) this.m_progressDisplay, this.m_BossWingContainer, false);
      }
    }
    AdventureWing adventureWing = (AdventureWing) null;
    using (List<AdventureMissionDisplay.WingCreateParams>.Enumerator enumerator1 = paramsList.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        AdventureMissionDisplay.WingCreateParams current1 = enumerator1.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        AdventureMissionDisplay.\u003CAwake\u003Ec__AnonStorey350 awakeCAnonStorey350 = new AdventureMissionDisplay.\u003CAwake\u003Ec__AnonStorey350();
        // ISSUE: reference to a compiler-generated field
        awakeCAnonStorey350.\u003C\u003Ef__this = this;
        WingDbId wingId = current1.m_WingDef.GetWingId();
        AdventureWingDef wingDef1 = current1.m_WingDef;
        // ISSUE: reference to a compiler-generated field
        awakeCAnonStorey350.wing = GameUtils.LoadGameObjectWithComponent<AdventureWing>((string) ((MobileOverrideValue<string>) wingDef1.m_WingPrefab));
        // ISSUE: reference to a compiler-generated field
        if (!((UnityEngine.Object) awakeCAnonStorey350.wing == (UnityEngine.Object) null))
        {
          if ((UnityEngine.Object) this.m_BossWingContainer != (UnityEngine.Object) null)
          {
            // ISSUE: reference to a compiler-generated field
            GameUtils.SetParent((Component) awakeCAnonStorey350.wing, this.m_BossWingContainer, false);
          }
          AdventureWingDef wingDef2 = AdventureScene.Get().GetWingDef(current1.m_WingDef.GetOwnershipPrereqId());
          // ISSUE: reference to a compiler-generated field
          awakeCAnonStorey350.wing.Initialize(wingDef1, wingDef2);
          // ISSUE: reference to a compiler-generated field
          awakeCAnonStorey350.wing.SetBigChestRewards(wingId);
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          awakeCAnonStorey350.wing.AddBossSelectedListener(new AdventureWing.BossSelected(awakeCAnonStorey350.\u003C\u003Em__C));
          // ISSUE: reference to a compiler-generated field
          awakeCAnonStorey350.wing.AddOpenPlateStartListener(new AdventureWing.OpenPlateStart(this.OnStartUnlockPlate));
          // ISSUE: reference to a compiler-generated field
          awakeCAnonStorey350.wing.AddOpenPlateEndListener(new AdventureWing.OpenPlateEnd(this.OnEndUnlockPlate));
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          awakeCAnonStorey350.wing.AddTryPurchaseWingListener(new AdventureWing.TryPurchaseWing(awakeCAnonStorey350.\u003C\u003Em__D));
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          awakeCAnonStorey350.wing.AddShowCardRewardsListener(new AdventureWing.ShowCardRewards(awakeCAnonStorey350.\u003C\u003Em__E));
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          awakeCAnonStorey350.wing.AddHideCardRewardsListener(new AdventureWing.HideCardRewards(awakeCAnonStorey350.\u003C\u003Em__F));
          // ISSUE: reference to a compiler-generated field
          awakeCAnonStorey350.wingScenarios = new List<int>();
          int num3 = 0;
          using (List<AdventureMissionDisplay.BossCreateParams>.Enumerator enumerator2 = current1.m_BossCreateParams.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              AdventureMissionDisplay.BossCreateParams current2 = enumerator2.Current;
              // ISSUE: object of a compiler-generated type is created
              // ISSUE: variable of a compiler-generated type
              AdventureMissionDisplay.\u003CAwake\u003Ec__AnonStorey351 awakeCAnonStorey351 = new AdventureMissionDisplay.\u003CAwake\u003Ec__AnonStorey351();
              // ISSUE: reference to a compiler-generated field
              awakeCAnonStorey351.\u003C\u003Ef__this = this;
              bool enabled = adventureConfig.IsMissionAvailable((int) current2.m_MissionId);
              // ISSUE: reference to a compiler-generated field
              // ISSUE: reference to a compiler-generated field
              awakeCAnonStorey351.coin = awakeCAnonStorey350.wing.CreateBoss(wingDef1.m_CoinPrefab, wingDef1.m_RewardsPrefab, current2.m_MissionId, enabled);
              // ISSUE: reference to a compiler-generated method
              AdventureConfig.Get().LoadBossDef(current2.m_MissionId, new AdventureConfig.DelBossDefLoaded(awakeCAnonStorey351.\u003C\u003Em__10));
              if (AdventureConfig.Get().GetLastSelectedMission() == current2.m_MissionId)
              {
                // ISSUE: reference to a compiler-generated field
                this.StartCoroutine(this.RememberLastBossSelection(awakeCAnonStorey351.coin, current2.m_MissionId));
              }
              if (AdventureProgressMgr.Get().HasDefeatedScenario((int) current2.m_MissionId))
              {
                ++num3;
                ++this.m_TotalBossesDefeated;
              }
              ++this.m_TotalBosses;
              DefLoader.Get().LoadFullDef(current2.m_CardDefId, new DefLoader.LoadDefCallback<FullDef>(this.OnHeroFullDefLoaded), (object) current2.m_MissionId);
              // ISSUE: reference to a compiler-generated field
              awakeCAnonStorey350.wingScenarios.Add((int) current2.m_MissionId);
            }
          }
          int wingBossesDefeated = adventureConfig.GetWingBossesDefeated(selectedAdventure, selectedMode, wingId, num3);
          if (wingBossesDefeated != num3)
            this.m_BossJustDefeated = true;
          bool flag = num3 == current1.m_BossCreateParams.Count || AdventureScene.Get().IsDevMode;
          // ISSUE: reference to a compiler-generated field
          if (!awakeCAnonStorey350.wing.HasBigChestRewards())
          {
            // ISSUE: reference to a compiler-generated field
            awakeCAnonStorey350.wing.HideBigChest();
          }
          else if (flag)
          {
            if (wingBossesDefeated != num3)
            {
              // ISSUE: reference to a compiler-generated field
              this.m_WingsToGiveBigChest.Add(awakeCAnonStorey350.wing);
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              awakeCAnonStorey350.wing.BigChestStayOpen();
            }
          }
          if ((UnityEngine.Object) this.m_progressDisplay != (UnityEngine.Object) null)
          {
            bool normalComplete = AdventureProgressMgr.Get().IsWingComplete(selectedAdventure, AdventureModeDbId.NORMAL, wingId);
            this.m_progressDisplay.UpdateProgress(current1.m_WingDef.GetWingId(), normalComplete);
          }
          adventureConfig.UpdateWingBossesDefeated(selectedAdventure, selectedMode, wingId, num3);
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          awakeCAnonStorey350.wing.AddShowRewardsPreviewListeners(new AdventureWing.ShowRewardsPreview(awakeCAnonStorey350.\u003C\u003Em__11));
          // ISSUE: reference to a compiler-generated field
          awakeCAnonStorey350.wing.UpdateRewardsPreviewCover();
          // ISSUE: reference to a compiler-generated field
          awakeCAnonStorey350.wing.RandomizeBackground();
          // ISSUE: reference to a compiler-generated field
          awakeCAnonStorey350.focusScrollPos = (float) num1++ / (float) num2;
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          awakeCAnonStorey350.wing.SetBringToFocusCallback(new AdventureWing.BringToFocusCallback(awakeCAnonStorey350.\u003C\u003Em__12));
          // ISSUE: reference to a compiler-generated field
          if ((UnityEngine.Object) adventureWing == (UnityEngine.Object) null || awakeCAnonStorey350.wing.GetWingDef().GetUnlockOrder() < adventureWing.GetWingDef().GetUnlockOrder())
          {
            // ISSUE: reference to a compiler-generated field
            adventureWing = awakeCAnonStorey350.wing;
          }
          // ISSUE: reference to a compiler-generated field
          this.m_BossWings.Add(awakeCAnonStorey350.wing);
        }
      }
    }
    AssetLoader.Get().LoadActor("Card_Play_Hero", new AssetLoader.GameObjectCallback(this.OnHeroActorLoaded), (object) null, false);
    AssetLoader.Get().LoadActor("Card_Play_HeroPower", new AssetLoader.GameObjectCallback(this.OnHeroPowerActorLoaded), (object) null, false);
    AssetLoader.Get().LoadActor("History_HeroPower_Opponent", new AssetLoader.GameObjectCallback(this.OnHeroPowerBigCardLoaded), (object) null, false);
    if ((UnityEngine.Object) this.m_BossPowerHoverArea != (UnityEngine.Object) null)
    {
      this.m_BossPowerHoverArea.AddEventListener(UIEventType.ROLLOVER, (UIEvent.Handler) (e => this.ShowHeroPowerBigCard()));
      this.m_BossPowerHoverArea.AddEventListener(UIEventType.ROLLOUT, (UIEvent.Handler) (e => this.HideHeroPowerBigCard()));
    }
    this.m_BackButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBackButtonPress));
    this.m_ChooseButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ChangeToDeckPicker()));
    this.UpdateWingPositions();
    this.m_ChooseButton.Disable();
    this.DoAutoPurchaseWings(selectedAdventure, selectedMode);
    StoreManager.Get().RegisterStoreShownListener(new StoreManager.StoreShownCallback(this.OnStoreShown));
    StoreManager.Get().RegisterStoreHiddenListener(new StoreManager.StoreHiddenCallback(this.OnStoreHidden));
    AdventureProgressMgr.Get().RegisterProgressUpdatedListener(new AdventureProgressMgr.AdventureProgressUpdatedCallback(this.OnAdventureProgressUpdate));
    if ((UnityEngine.Object) this.m_ScrollBar != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) adventureWing != (UnityEngine.Object) null && (UnityEngine.Object) adventureWing != (UnityEngine.Object) this.m_BossWings[0])
        adventureWing.BringToFocus();
      this.m_ScrollBar.LoadScroll(AdventureConfig.Get().GetSelectedAdventureAndModeString());
    }
    if ((UnityEngine.Object) this.m_WatermarkIcon != (UnityEngine.Object) null && !string.IsNullOrEmpty(subDef.m_WatermarkTexture))
    {
      Texture texture = AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(subDef.m_WatermarkTexture), false);
      if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
        this.m_WatermarkIcon.material.mainTexture = texture;
      else
        UnityEngine.Debug.LogWarning((object) string.Format("Adventure Watermark texture is null: {0}", (object) subDef.m_WatermarkTexture));
    }
    else
      UnityEngine.Debug.LogWarning((object) string.Format("Adventure Watermark texture is null: m_WatermarkIcon: {0},  advSubDef.m_WatermarkTexture: {1}", (object) this.m_WatermarkIcon, (object) subDef.m_WatermarkTexture));
    this.m_BackButton.gameObject.SetActive(true);
    this.m_PreviewPane.AddHideListener(new AdventureRewardsPreview.OnHide(this.OnHideRewardsPreview));
  }

  private void Start()
  {
    Navigation.PushUnique(new Navigation.NavigateBackHandler(AdventureMissionDisplay.OnNavigateBack));
  }

  private void OnDestroy()
  {
    AdventureProgressMgr.Get().RemoveProgressUpdatedListener(new AdventureProgressMgr.AdventureProgressUpdatedCallback(this.OnAdventureProgressUpdate));
    StoreManager.Get().RemoveStoreHiddenListener(new StoreManager.StoreHiddenCallback(this.OnStoreHidden));
    StoreManager.Get().RemoveStoreShownListener(new StoreManager.StoreShownCallback(this.OnStoreShown));
    if ((UnityEngine.Object) this.m_ScrollBar != (UnityEngine.Object) null && (UnityEngine.Object) AdventureConfig.Get() != (UnityEngine.Object) null)
      this.m_ScrollBar.SaveScroll(AdventureConfig.Get().GetSelectedAdventureAndModeString());
    AdventureMissionDisplay.s_instance = (AdventureMissionDisplay) null;
  }

  private void Update()
  {
    if (!AdventureScene.Get().IsDevMode || AdventureScene.Get().DevModeSetting != 2)
      return;
    if (Input.GetKeyDown(KeyCode.Z))
      this.StartCoroutine(this.AnimateFancyCheckmarksEffects());
    if (!Input.GetKeyDown(KeyCode.X))
      return;
    this.ShowAdventureComplete();
  }

  public bool IsDisabledSelection()
  {
    return this.m_DisableSelectionCount > 0;
  }

  private void UpdateWingPositions()
  {
    float num = 0.0f;
    if ((UnityEngine.Object) this.m_progressDisplay != (UnityEngine.Object) null)
    {
      this.m_progressDisplay.transform.localPosition = this.m_BossWingOffset;
      TransformUtil.SetLocalPosZ((Component) this.m_progressDisplay, this.m_BossWingOffset.z - num);
      num += this.HeightForScrollableItem(this.m_progressDisplay.gameObject);
    }
    using (List<AdventureWing>.Enumerator enumerator = this.m_BossWings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureWing current = enumerator.Current;
        current.transform.localPosition = this.m_BossWingOffset;
        TransformUtil.SetLocalPosZ((Component) current, this.m_BossWingOffset.z - num);
        num += this.HeightForScrollableItem(current.gameObject);
      }
    }
    if (!((UnityEngine.Object) this.m_BossWingBorder != (UnityEngine.Object) null))
      return;
    this.m_BossWingBorder.transform.localPosition = this.m_BossWingOffset;
    TransformUtil.SetLocalPosZ(this.m_BossWingBorder, this.m_BossWingOffset.z - num);
  }

  private float HeightForScrollableItem(GameObject go)
  {
    UIBScrollableItem component = go.GetComponent<UIBScrollableItem>();
    if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
      return component.m_size.z;
    Log.All.PrintError("No UIBScrollableItem component on the GameObject {0}!", (object) go);
    return 0.0f;
  }

  private void OnHeroFullDefLoaded(string cardId, FullDef def, object userData)
  {
    if (def == null)
    {
      UnityEngine.Debug.LogError((object) string.Format("Unable to load {0} hero def for Adventure boss.", (object) cardId), (UnityEngine.Object) this.gameObject);
      this.AssetLoadCompleted();
    }
    else
    {
      ScenarioDbId index = (ScenarioDbId) userData;
      this.m_BossPortraitDefCache[index] = def;
      string powerCardIdFromHero = GameUtils.GetHeroPowerCardIdFromHero(def.GetEntityDef().GetCardId());
      if (!string.IsNullOrEmpty(powerCardIdFromHero))
      {
        this.AddAssetToLoad(1);
        DefLoader.Get().LoadFullDef(powerCardIdFromHero, new DefLoader.LoadDefCallback<FullDef>(this.OnHeroPowerFullDefLoaded), (object) index);
      }
      this.AssetLoadCompleted();
    }
  }

  private void OnHeroPowerFullDefLoaded(string cardId, FullDef def, object userData)
  {
    if (def == null)
    {
      UnityEngine.Debug.LogError((object) string.Format("Unable to load {0} hero power def for Adventure boss.", (object) cardId), (UnityEngine.Object) this.gameObject);
      this.AssetLoadCompleted();
    }
    else
    {
      this.m_BossPowerDefCache[(ScenarioDbId) userData] = def;
      this.AssetLoadCompleted();
    }
  }

  private void OnHeroActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    this.m_BossActor = this.OnActorLoaded(actorName, actorObject, this.m_BossPortraitContainer);
    if ((UnityEngine.Object) this.m_BossActor != (UnityEngine.Object) null && (UnityEngine.Object) this.m_BossActor.GetHealthObject() != (UnityEngine.Object) null)
      this.m_BossActor.GetHealthObject().Hide();
    this.AssetLoadCompleted();
  }

  private void OnHeroPowerActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    this.m_BossPowerActor = this.OnActorLoaded(actorName, actorObject, this.m_BossPowerContainer);
    this.AssetLoadCompleted();
  }

  private void OnHeroPowerBigCardLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    this.m_BossPowerBigCard = this.OnActorLoaded(actorName, actorObject, !((UnityEngine.Object) this.m_BossPowerActor == (UnityEngine.Object) null) ? this.m_BossPowerActor.gameObject : (GameObject) null);
    if ((UnityEngine.Object) this.m_BossPowerBigCard != (UnityEngine.Object) null)
      this.m_BossPowerBigCard.TurnOffCollider();
    this.AssetLoadCompleted();
  }

  private Actor OnActorLoaded(string actorName, GameObject actorObject, GameObject container)
  {
    Actor component = actorObject.GetComponent<Actor>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) container != (UnityEngine.Object) null)
        GameUtils.SetParent((Component) component, container, false);
      SceneUtils.SetLayer((Component) component, container.layer);
      component.SetUnlit();
      component.Hide();
    }
    else
      UnityEngine.Debug.LogWarning((object) string.Format("ERROR actor \"{0}\" has no Actor component", (object) actorName));
    return component;
  }

  private void AddAssetToLoad(int assetCount = 1)
  {
    this.m_AssetsLoading += assetCount;
  }

  private void AssetLoadCompleted()
  {
    if (this.m_AssetsLoading > 0)
    {
      --this.m_AssetsLoading;
      if (this.m_AssetsLoading != 0)
        return;
      if ((UnityEngine.Object) this.m_BossPowerBigCard != (UnityEngine.Object) null && (UnityEngine.Object) this.m_BossPowerActor != (UnityEngine.Object) null && (UnityEngine.Object) this.m_BossPowerBigCard.transform.parent != (UnityEngine.Object) this.m_BossPowerActor.transform)
        GameUtils.SetParent((Component) this.m_BossPowerBigCard, this.m_BossPowerActor.gameObject, false);
      this.OnSubSceneLoaded();
    }
    else
      UnityEngine.Debug.LogError((object) "AssetLoadCompleted() called when no assets left.", (UnityEngine.Object) this.gameObject);
  }

  private void OnSubSceneLoaded()
  {
    AdventureSubScene component = this.GetComponent<AdventureSubScene>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.AddSubSceneTransitionFinishedListener(new AdventureSubScene.SubSceneTransitionFinished(this.OnSubSceneTransitionComplete));
    component.SetIsLoaded(true);
  }

  private void OnSubSceneTransitionComplete()
  {
    AdventureSubScene component = this.GetComponent<AdventureSubScene>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.RemoveSubSceneTransitionFinishedListener(new AdventureSubScene.SubSceneTransitionFinished(this.OnSubSceneTransitionComplete));
    this.StartCoroutine(this.UpdateAndAnimateWingCoinsAndChests(this.m_BossWings, true, false));
  }

  private static bool OnNavigateBack()
  {
    AdventureConfig.Get().ChangeToLastSubScene(true);
    return true;
  }

  private void OnBackButtonPress(UIEvent e)
  {
    using (List<AdventureWing>.Enumerator enumerator = this.m_BossWings.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.NavigateBackCleanup();
    }
    Navigation.GoBack();
  }

  private void OnZeroCostTransactionStoreExit(bool authorizationBackButtonPressed, object userData)
  {
    if (!authorizationBackButtonPressed)
      return;
    this.OnBackButtonPress((UIEvent) null);
  }

  private void OnBossSelected(AdventureBossCoin coin, ScenarioDbId mission, bool showDetails = true)
  {
    if (this.IsDisabledSelection())
      return;
    if ((UnityEngine.Object) this.m_SelectedCoin != (UnityEngine.Object) null)
      this.m_SelectedCoin.Select(false);
    this.m_SelectedCoin = coin;
    this.m_SelectedCoin.Select(true);
    if ((UnityEngine.Object) this.m_ChooseButton != (UnityEngine.Object) null)
    {
      if (!this.m_ChooseButton.IsEnabled())
        this.m_ChooseButton.Enable();
      this.m_ChooseButton.SetText(GameStrings.Get(!AdventureConfig.Get().DoesMissionRequireDeck(mission) ? "GLOBAL_PLAY" : "GLUE_CHOOSE"));
    }
    this.ShowBossFrame(mission);
    AdventureConfig.Get().SetMission(mission, showDetails);
    AdventureBossDef bossDef = AdventureConfig.Get().GetBossDef(mission);
    if (bossDef.m_MissionMusic != MusicPlaylistType.Invalid && !MusicManager.Get().StartPlaylist(bossDef.m_MissionMusic))
      this.ResumeMainMusic();
    if (bossDef.m_IntroLinePlayTime != AdventureBossDef.IntroLinePlayTime.MissionSelect)
      return;
    this.PlayMissionQuote(bossDef, this.DetermineCharacterQuotePos(coin.gameObject));
  }

  private void PlayMissionQuote(AdventureBossDef bossDef, Vector3 position)
  {
    if ((UnityEngine.Object) bossDef == (UnityEngine.Object) null || string.IsNullOrEmpty(bossDef.m_IntroLine))
      return;
    AdventureDef adventureDef = AdventureScene.Get().GetAdventureDef(AdventureConfig.Get().GetSelectedAdventure());
    string path = (string) null;
    if ((UnityEngine.Object) adventureDef != (UnityEngine.Object) null)
      path = adventureDef.m_DefaultQuotePrefab;
    if (!string.IsNullOrEmpty(bossDef.m_quotePrefabOverride))
      path = bossDef.m_quotePrefabOverride;
    if (string.IsNullOrEmpty(path))
      return;
    bool allowRepeatDuringSession = (UnityEngine.Object) AdventureScene.Get() != (UnityEngine.Object) null && AdventureScene.Get().IsDevMode;
    NotificationManager.Get().CreateCharacterQuote(FileUtils.GameAssetPathToName(path), position, GameStrings.Get(bossDef.m_IntroLine), bossDef.m_IntroLine, allowRepeatDuringSession, 0.0f, (Action) null, CanvasAnchor.BOTTOM_LEFT);
  }

  private Vector3 DetermineCharacterQuotePos(GameObject coin)
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      return NotificationManager.PHONE_CHARACTER_POS;
    Bounds boundsOfChildren = TransformUtil.GetBoundsOfChildren(coin);
    Vector3 center = boundsOfChildren.center;
    center.y -= boundsOfChildren.extents.y;
    Camera camera = Box.Get().GetCamera();
    if ((double) camera.WorldToScreenPoint(center).y < (double) (0.4f * (float) camera.pixelHeight))
      return NotificationManager.ALT_ADVENTURE_SCREEN_POS;
    return NotificationManager.DEFAULT_CHARACTER_POS;
  }

  private void ShowBossFrame(ScenarioDbId mission)
  {
    AdventureMissionDisplay.BossInfo bossInfo;
    if (this.m_BossInfoCache.TryGetValue(mission, out bossInfo))
    {
      this.m_BossTitle.Text = bossInfo.m_Title;
      if ((UnityEngine.Object) this.m_BossDescription != (UnityEngine.Object) null)
        this.m_BossDescription.Text = bossInfo.m_Description;
    }
    FullDef fullDef;
    if (this.m_BossPortraitDefCache.TryGetValue(mission, out fullDef))
    {
      this.m_BossActor.SetPremium(TAG_PREMIUM.NORMAL);
      this.m_BossActor.SetEntityDef(fullDef.GetEntityDef());
      this.m_BossActor.SetCardDef(fullDef.GetCardDef());
      this.m_BossActor.UpdateAllComponents();
      this.m_BossActor.SetUnlit();
      this.m_BossActor.Show();
    }
    if (this.m_BossPowerDefCache.TryGetValue(mission, out fullDef))
    {
      this.m_BossPowerActor.SetPremium(TAG_PREMIUM.NORMAL);
      this.m_BossPowerActor.SetEntityDef(fullDef.GetEntityDef());
      this.m_BossPowerActor.SetCardDef(fullDef.GetCardDef());
      this.m_BossPowerActor.UpdateAllComponents();
      this.m_BossPowerActor.SetUnlit();
      this.m_BossPowerActor.Show();
      this.m_SelectedHeroPowerFullDef = fullDef;
      if (!((UnityEngine.Object) this.m_BossPowerContainer != (UnityEngine.Object) null) || this.m_BossPowerContainer.activeSelf)
        return;
      this.m_BossPowerContainer.SetActive(true);
    }
    else
    {
      if (!((UnityEngine.Object) this.m_BossPowerContainer != (UnityEngine.Object) null))
        return;
      this.m_BossPowerContainer.SetActive(false);
    }
  }

  private void UnselectBoss()
  {
    if ((UnityEngine.Object) this.m_BossTitle != (UnityEngine.Object) null)
      this.m_BossTitle.Text = string.Empty;
    if ((UnityEngine.Object) this.m_BossDescription != (UnityEngine.Object) null)
      this.m_BossDescription.Text = string.Empty;
    this.m_BossActor.Hide();
    if ((UnityEngine.Object) this.m_BossPowerContainer != (UnityEngine.Object) null)
      this.m_BossPowerContainer.SetActive(false);
    if ((UnityEngine.Object) this.m_SelectedCoin != (UnityEngine.Object) null)
      this.m_SelectedCoin.Select(false);
    this.m_SelectedCoin = (AdventureBossCoin) null;
    AdventureConfig.Get().SetMission(ScenarioDbId.INVALID, true);
    if (!this.m_ChooseButton.IsEnabled())
      return;
    this.m_ChooseButton.Disable();
  }

  private void ShowHeroPowerBigCard()
  {
    FullDef heroPowerFullDef = this.m_SelectedHeroPowerFullDef;
    if (heroPowerFullDef == null)
      return;
    CardDef cardDef = heroPowerFullDef.GetCardDef();
    if ((UnityEngine.Object) cardDef == (UnityEngine.Object) null)
      return;
    if ((bool) UniversalInputManager.UsePhoneUI)
      NotificationManager.Get().DestroyActiveQuote(0.2f, true);
    this.m_BossPowerBigCard.SetCardDef(cardDef);
    this.m_BossPowerBigCard.SetEntityDef(heroPowerFullDef.GetEntityDef());
    this.m_BossPowerBigCard.UpdateAllComponents();
    this.m_BossPowerBigCard.Show();
    this.m_BossPowerBigCard.transform.localScale = Vector3.one * this.m_BossPowerCardScale;
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_BossPowerBigCard.transform.localPosition = new Vector3(-7.77f, 1.56f, 0.39f);
      this.TweenPower(this.m_BossPowerBigCard.gameObject, new Vector3?(this.m_BossPowerActor.gameObject.transform.position));
    }
    else
    {
      this.m_BossPowerBigCard.transform.localPosition = !UniversalInputManager.Get().IsTouchMode() ? new Vector3(0.019f, 0.54f, -1.12f) : new Vector3(-3.18f, 0.54f, 0.1f);
      this.TweenPower(this.m_BossPowerBigCard.gameObject, new Vector3?());
    }
  }

  private void TweenPower(GameObject go, Vector3? origin = null)
  {
    Vector3 a = !UniversalInputManager.Get().IsTouchMode() || (bool) UniversalInputManager.UsePhoneUI ? new Vector3(0.1f, 0.1f, 0.1f) : new Vector3(0.0f, 0.1f, 0.1f);
    if (!origin.HasValue)
    {
      iTween.ScaleFrom(go, go.transform.localScale * 0.5f, 0.15f);
      iTween.MoveTo(go, iTween.Hash((object) "position", (object) (go.transform.localPosition + a), (object) "isLocal", (object) true, (object) "time", (object) 10));
    }
    else
    {
      Vector3 worldScale = TransformUtil.ComputeWorldScale((Component) go.transform.parent);
      Vector3 driftOffset = Vector3.Scale(a, worldScale);
      AnimationUtil.GrowThenDrift(go, origin.Value, driftOffset);
    }
  }

  private void HideHeroPowerBigCard()
  {
    iTween.Stop(this.m_BossPowerBigCard.gameObject);
    this.m_BossPowerBigCard.Hide();
  }

  private void ChangeToDeckPicker()
  {
    AdventureBossDef bossDef = AdventureConfig.Get().GetBossDef(AdventureConfig.Get().GetMission());
    if ((UnityEngine.Object) bossDef != (UnityEngine.Object) null && bossDef.m_IntroLinePlayTime == AdventureBossDef.IntroLinePlayTime.MissionStart)
      this.PlayMissionQuote(bossDef, this.DetermineCharacterQuotePos(this.m_ChooseButton.gameObject));
    if (AdventureConfig.Get().DoesSelectedMissionRequireDeck())
    {
      this.m_ChooseButton.Disable();
      this.DisableSelection(true);
      AdventureConfig.Get().ChangeSubScene(AdventureSubScenes.MissionDeckPicker);
    }
    else
    {
      if ((UnityEngine.Object) this.m_advMissionDisplayTray != (UnityEngine.Object) null)
        this.m_advMissionDisplayTray.EnableRewardsChest(false);
      GameMgr.Get().FindGame(GameType.GT_VS_AI, FormatType.FT_WILD, (int) AdventureConfig.Get().GetMission(), 0L, 0L);
    }
  }

  private void DisableSelection(bool yes)
  {
    if ((UnityEngine.Object) this.m_ClickBlocker == (UnityEngine.Object) null)
      return;
    this.m_DisableSelectionCount += !yes ? -1 : 1;
    bool flag = this.IsDisabledSelection();
    if (this.m_ClickBlocker.gameObject.activeSelf == flag)
      return;
    this.m_ClickBlocker.gameObject.SetActive(flag);
    this.m_ScrollBar.Enable(!flag);
  }

  [DebuggerHidden]
  private IEnumerator UpdateAndAnimateWingCoinsAndChests(List<AdventureWing> wings, bool scrollToCoin, bool forceCoinAnimation = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureMissionDisplay.\u003CUpdateAndAnimateWingCoinsAndChests\u003Ec__Iterator3()
    {
      wings = wings,
      scrollToCoin = scrollToCoin,
      forceCoinAnimation = forceCoinAnimation,
      \u003C\u0024\u003Ewings = wings,
      \u003C\u0024\u003EscrollToCoin = scrollToCoin,
      \u003C\u0024\u003EforceCoinAnimation = forceCoinAnimation,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator AnimateWingBigChestsAndProgressDisplay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureMissionDisplay.\u003CAnimateWingBigChestsAndProgressDisplay\u003Ec__Iterator4()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator AnimateProgressDisplay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureMissionDisplay.\u003CAnimateProgressDisplay\u003Ec__Iterator5()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void CheckForWingUnlocks()
  {
    using (List<AdventureWing>.Enumerator enumerator = this.m_BossWings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureWing current = enumerator.Current;
        if (current.GetWingDef().GetOpenPrereqId() != WingDbId.INVALID)
          current.UpdatePlateState();
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator AnimateFancyCheckmarksEffects()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureMissionDisplay.\u003CAnimateFancyCheckmarksEffects\u003Ec__Iterator6()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void ShowClassChallengeUnlock(List<int> classChallengeUnlocks)
  {
    if (classChallengeUnlocks == null || classChallengeUnlocks.Count == 0)
    {
      this.m_WaitingForClassChallengeUnlocks = false;
    }
    else
    {
      using (List<int>.Enumerator enumerator = classChallengeUnlocks.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          int current = enumerator.Current;
          ++this.m_ClassChallengeUnlockShowing;
          new ClassChallengeUnlockData(current).LoadRewardObject((Reward.DelOnRewardLoaded) ((reward, data) =>
          {
            reward.RegisterHideListener((Reward.OnHideCallback) (userData =>
            {
              --this.m_ClassChallengeUnlockShowing;
              if (this.m_ClassChallengeUnlockShowing != 0)
                return;
              this.m_WaitingForClassChallengeUnlocks = false;
            }));
            this.OnRewardObjectLoaded(reward, data);
          }));
        }
      }
    }
  }

  private void ShowAdventureComplete()
  {
    AdventureDbId selectedAdventure = AdventureConfig.Get().GetSelectedAdventure();
    AdventureModeDbId selectedMode = AdventureConfig.Get().GetSelectedMode();
    this.DisableSelection(true);
    AdventureDef adventureDef = AdventureScene.Get().GetAdventureDef(selectedAdventure);
    AdventureSubDef subDef = adventureDef.GetSubDef(selectedMode);
    switch (adventureDef.m_BannerRewardType)
    {
      case AdventureDef.BannerRewardType.AdventureCompleteReward:
        new AdventureCompleteRewardData(selectedMode, FileUtils.GameAssetPathToName(adventureDef.m_BannerRewardPrefab), subDef.GetCompleteBannerText()).LoadRewardObject((Reward.DelOnRewardLoaded) ((reward, data) =>
        {
          reward.RegisterHideListener((Reward.OnHideCallback) (userData => this.DisableSelection(false)));
          this.OnRewardObjectLoaded(reward, data);
        }));
        break;
      case AdventureDef.BannerRewardType.BannerManagerPopup:
        BannerManager.Get().ShowCustomBanner(adventureDef.m_BannerRewardPrefab, subDef.GetCompleteBannerText(), (BannerManager.DelOnCloseBanner) (() => this.DisableSelection(false)));
        break;
    }
    if (string.IsNullOrEmpty(adventureDef.m_AdventureCompleteQuotePrefab) || string.IsNullOrEmpty(adventureDef.m_AdventureCompleteQuoteVOLine))
      return;
    NotificationManager.Get().CreateCharacterQuote(FileUtils.GameAssetPathToName(adventureDef.m_AdventureCompleteQuotePrefab), GameStrings.Get(adventureDef.m_AdventureCompleteQuoteVOLine), adventureDef.m_AdventureCompleteQuoteVOLine, true, 0.0f, CanvasAnchor.BOTTOM_LEFT);
  }

  private void PositionReward(Reward reward)
  {
    GameUtils.SetParent((Component) reward, (Component) this.transform, false);
  }

  private void OnRewardObjectLoaded(Reward reward, object callbackData)
  {
    this.PositionReward(reward);
    reward.Show(false);
  }

  private List<AdventureMissionDisplay.WingCreateParams> BuildWingCreateParamsList()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureMissionDisplay.\u003CBuildWingCreateParamsList\u003Ec__AnonStorey352 listCAnonStorey352 = new AdventureMissionDisplay.\u003CBuildWingCreateParamsList\u003Ec__AnonStorey352();
    AdventureConfig adventureConfig = AdventureConfig.Get();
    AdventureDbId selectedAdventure = adventureConfig.GetSelectedAdventure();
    AdventureModeDbId selectedMode = adventureConfig.GetSelectedMode();
    // ISSUE: reference to a compiler-generated field
    listCAnonStorey352.adventureDbId = (int) selectedAdventure;
    // ISSUE: reference to a compiler-generated field
    listCAnonStorey352.modeDbId = (int) selectedMode;
    List<AdventureMissionDisplay.WingCreateParams> wingCreateParamsList = new List<AdventureMissionDisplay.WingCreateParams>();
    int num = 0;
    // ISSUE: reference to a compiler-generated method
    using (List<ScenarioDbfRecord>.Enumerator enumerator = GameDbf.Scenario.GetRecords(new Predicate<ScenarioDbfRecord>(listCAnonStorey352.\u003C\u003Em__19)).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ScenarioDbfRecord current = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        AdventureMissionDisplay.\u003CBuildWingCreateParamsList\u003Ec__AnonStorey353 listCAnonStorey353 = new AdventureMissionDisplay.\u003CBuildWingCreateParamsList\u003Ec__AnonStorey353();
        ScenarioDbId id = (ScenarioDbId) current.ID;
        // ISSUE: reference to a compiler-generated field
        listCAnonStorey353.wingId = (WingDbId) current.WingId;
        int player2HeroCardId = current.ClientPlayer2HeroCardId;
        if (player2HeroCardId == 0)
          player2HeroCardId = current.Player2HeroCardId;
        // ISSUE: reference to a compiler-generated field
        AdventureWingDef wingDef = AdventureScene.Get().GetWingDef(listCAnonStorey353.wingId);
        if ((UnityEngine.Object) wingDef == (UnityEngine.Object) null)
        {
          // ISSUE: reference to a compiler-generated field
          UnityEngine.Debug.LogError((object) string.Format("Unable to find wing record for scenario {0} with ID: {1}", (object) id, (object) listCAnonStorey353.wingId));
        }
        else
        {
          CardDbfRecord record = GameDbf.Card.GetRecord(player2HeroCardId);
          // ISSUE: reference to a compiler-generated method
          AdventureMissionDisplay.WingCreateParams wingCreateParams = wingCreateParamsList.Find(new Predicate<AdventureMissionDisplay.WingCreateParams>(listCAnonStorey353.\u003C\u003Em__1A));
          if (wingCreateParams == null)
          {
            wingCreateParams = new AdventureMissionDisplay.WingCreateParams();
            wingCreateParams.m_WingDef = wingDef;
            if ((UnityEngine.Object) wingCreateParams.m_WingDef == (UnityEngine.Object) null)
            {
              // ISSUE: reference to a compiler-generated field
              Error.AddDevFatal("AdventureDisplay.BuildWingCreateParamsMap() - failed to find a WingDef for adventure {0} wing {1}", (object) selectedAdventure, (object) listCAnonStorey353.wingId);
              continue;
            }
            wingCreateParamsList.Add(wingCreateParams);
          }
          AdventureMissionDisplay.BossCreateParams bossCreateParams = new AdventureMissionDisplay.BossCreateParams();
          bossCreateParams.m_ScenarioRecord = current;
          bossCreateParams.m_MissionId = id;
          bossCreateParams.m_CardDefId = record.NoteMiniGuid;
          if (!this.m_BossInfoCache.ContainsKey(id))
          {
            AdventureMissionDisplay.BossInfo bossInfo = new AdventureMissionDisplay.BossInfo()
            {
              m_Title = (string) current.ShortName,
              m_Description = (string) current.Description
            };
            this.m_BossInfoCache[id] = bossInfo;
          }
          wingCreateParams.m_BossCreateParams.Add(bossCreateParams);
          ++num;
        }
      }
    }
    if (num == 0)
      UnityEngine.Debug.LogError((object) string.Format("Unable to find any bosses associated with wing {0} and mode {1}.\nCheck if the scenario DBF has valid entries!", (object) selectedAdventure, (object) selectedMode));
    wingCreateParamsList.Sort(new Comparison<AdventureMissionDisplay.WingCreateParams>(this.WingCreateParamsSortComparison));
    using (List<AdventureMissionDisplay.WingCreateParams>.Enumerator enumerator = wingCreateParamsList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.m_BossCreateParams.Sort(new Comparison<AdventureMissionDisplay.BossCreateParams>(this.BossCreateParamsSortComparison));
    }
    return wingCreateParamsList;
  }

  private int WingCreateParamsSortComparison(AdventureMissionDisplay.WingCreateParams params1, AdventureMissionDisplay.WingCreateParams params2)
  {
    return params1.m_WingDef.GetSortOrder() - params2.m_WingDef.GetSortOrder();
  }

  private int BossCreateParamsSortComparison(AdventureMissionDisplay.BossCreateParams params1, AdventureMissionDisplay.BossCreateParams params2)
  {
    return GameUtils.MissionSortComparison(params1.m_ScenarioRecord, params2.m_ScenarioRecord);
  }

  private void ShowAdventureStore(AdventureWing selectedWing)
  {
    StoreManager.Get().StartAdventureTransaction(selectedWing.GetProductType(), selectedWing.GetProductData(), (Store.ExitCallback) null, (object) null);
  }

  private void OnStoreShown(object userData)
  {
    this.DisableSelection(true);
  }

  private void OnStoreHidden(object userData)
  {
    this.DisableSelection(false);
  }

  private void OnAdventureProgressUpdate(bool isStartupAction, AdventureMission.WingProgress oldProgress, AdventureMission.WingProgress newProgress, object userData)
  {
    if ((oldProgress != null && oldProgress.IsOwned()) == (newProgress != null && newProgress.IsOwned()))
      return;
    this.StartCoroutine(this.UpdateWingPlateStates());
  }

  [DebuggerHidden]
  private IEnumerator UpdateWingPlateStates()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureMissionDisplay.\u003CUpdateWingPlateStates\u003Ec__Iterator7()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ShowRewardsPreview(AdventureWing wing, int[] scenarioids, List<CardRewardData> wingRewards, string wingName)
  {
    if (this.m_ShowingRewardsPreview)
      return;
    if ((UnityEngine.Object) this.m_ClickBlocker != (UnityEngine.Object) null)
      this.m_ClickBlocker.SetActive(true);
    this.m_ShowingRewardsPreview = true;
    this.m_PreviewPane.Reset();
    this.m_PreviewPane.SetHeaderText(wingName);
    List<string> rewardsPreviewCards = wing.GetWingDef().m_SpecificRewardsPreviewCards;
    int rewardsPreviewCount = wing.GetWingDef().m_HiddenRewardsPreviewCount;
    if (rewardsPreviewCards != null && rewardsPreviewCards.Count > 0)
    {
      this.m_PreviewPane.AddSpecificCards(rewardsPreviewCards);
    }
    else
    {
      foreach (int scenarioid in scenarioids)
        this.m_PreviewPane.AddCardBatch(scenarioid);
      if (wingRewards != null && wingRewards.Count > 0)
        this.m_PreviewPane.AddCardBatch(wingRewards);
    }
    this.m_PreviewPane.SetHiddenCardCount(rewardsPreviewCount);
    this.m_PreviewPane.Show(true);
  }

  private void OnHideRewardsPreview()
  {
    if ((UnityEngine.Object) this.m_ClickBlocker != (UnityEngine.Object) null)
      this.m_ClickBlocker.SetActive(false);
    this.m_ShowingRewardsPreview = false;
  }

  private void OnStartUnlockPlate(AdventureWing wing)
  {
    this.UnselectBoss();
    this.DisableSelection(true);
  }

  private void OnEndUnlockPlate(AdventureWing wing)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureMissionDisplay.\u003COnEndUnlockPlate\u003Ec__AnonStorey354 plateCAnonStorey354 = new AdventureMissionDisplay.\u003COnEndUnlockPlate\u003Ec__AnonStorey354();
    // ISSUE: reference to a compiler-generated field
    plateCAnonStorey354.wing = wing;
    // ISSUE: reference to a compiler-generated field
    plateCAnonStorey354.\u003C\u003Ef__this = this;
    this.DisableSelection(false);
    // ISSUE: reference to a compiler-generated field
    if (!string.IsNullOrEmpty(plateCAnonStorey354.wing.GetWingDef().m_WingOpenPopup))
    {
      // ISSUE: reference to a compiler-generated field
      AdventureWingOpenBanner adventureWingOpenBanner = GameUtils.LoadGameObjectWithComponent<AdventureWingOpenBanner>(plateCAnonStorey354.wing.GetWingDef().m_WingOpenPopup);
      if (!((UnityEngine.Object) adventureWingOpenBanner != (UnityEngine.Object) null))
        return;
      // ISSUE: reference to a compiler-generated method
      adventureWingOpenBanner.ShowBanner(new AdventureWingOpenBanner.OnBannerHidden(plateCAnonStorey354.\u003C\u003Em__1B));
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      this.StartCoroutine(this.UpdateAndAnimateWingCoinsAndChests(new List<AdventureWing>()
      {
        plateCAnonStorey354.wing
      }, false, true));
    }
  }

  private void BringWingToFocus(float scrollPos)
  {
    if ((UnityEngine.Object) this.m_ScrollBar == (UnityEngine.Object) null)
      return;
    this.m_ScrollBar.SetScroll(scrollPos, false, true);
  }

  [DebuggerHidden]
  private IEnumerator RememberLastBossSelection(AdventureBossCoin coin, ScenarioDbId mission)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureMissionDisplay.\u003CRememberLastBossSelection\u003Ec__Iterator8()
    {
      coin = coin,
      mission = mission,
      \u003C\u0024\u003Ecoin = coin,
      \u003C\u0024\u003Emission = mission,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator PlayWingNotifications()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureMissionDisplay.\u003CPlayWingNotifications\u003Ec__Iterator9()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void DoAutoPurchaseWings(AdventureDbId selectedAdv, AdventureModeDbId selectedMode)
  {
    if (selectedMode != AdventureModeDbId.NORMAL)
      return;
    ProductType adventureProductType = StoreManager.GetAdventureProductType(selectedAdv);
    if (adventureProductType == ProductType.PRODUCT_TYPE_UNKNOWN)
      return;
    StoreManager.Get().DoZeroCostTransactionIfPossible(StoreType.ADVENTURE_STORE, new Store.ExitCallback(this.OnZeroCostTransactionStoreExit), (object) null, adventureProductType, 0, 0);
  }

  private void ResumeMainMusic()
  {
    if (this.m_mainMusic == MusicPlaylistType.Invalid)
      return;
    MusicManager.Get().StartPlaylist(this.m_mainMusic);
  }

  protected class BossCreateParams
  {
    public ScenarioDbfRecord m_ScenarioRecord;
    public ScenarioDbId m_MissionId;
    public string m_CardDefId;
  }

  protected class WingCreateParams
  {
    [CustomEditField(ListTable = true)]
    public List<AdventureMissionDisplay.BossCreateParams> m_BossCreateParams = new List<AdventureMissionDisplay.BossCreateParams>();
    public AdventureWingDef m_WingDef;
  }

  protected class BossInfo
  {
    public string m_Title;
    public string m_Description;
  }
}
