﻿// Decompiled with JetBrains decompiler
// Type: PackOpeningCardCarouselItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PackOpeningCardCarouselItem : CarouselItem
{
  private PackOpeningCard m_card;

  public PackOpeningCardCarouselItem(PackOpeningCard card)
  {
    this.m_card = card;
  }

  public override void Show(Carousel card)
  {
  }

  public override void Hide()
  {
  }

  public override void Clear()
  {
    this.m_card = (PackOpeningCard) null;
  }

  public override GameObject GetGameObject()
  {
    return this.m_card.gameObject;
  }

  public override bool IsLoaded()
  {
    return true;
  }
}
