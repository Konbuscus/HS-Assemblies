﻿// Decompiled with JetBrains decompiler
// Type: SummonOutForge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class SummonOutForge : SpellImpl
{
  private static Color COMMON_COLOR = new Color(0.7333333f, 0.8235294f, 1f);
  private static Color RARE_COLOR = new Color(0.2f, 0.4745098f, 1f);
  private static Color EPIC_COLOR = new Color(0.5450981f, 0.2313726f, 1f);
  private static Color LEGENDARY_COLOR = new Color(1f, 0.6666667f, 0.2f);
  public GameObject m_scryLines;
  public Material m_scryLinesMaterial;
  public GameObject m_burstMotes;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    this.StartCoroutine(this.BirthState());
  }

  [DebuggerHidden]
  private IEnumerator BirthState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SummonOutForge.\u003CBirthState\u003Ec__Iterator2D4() { \u003C\u003Ef__this = this };
  }
}
