﻿// Decompiled with JetBrains decompiler
// Type: DisableMesh_ColorBlack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DisableMesh_ColorBlack : MonoBehaviour
{
  private Color m_color = Color.black;
  private Material m_material;
  private bool m_tintColor;

  private void Start()
  {
    this.m_material = this.GetComponent<Renderer>().material;
    if ((Object) this.m_material == (Object) null)
      this.enabled = false;
    if (!this.m_material.HasProperty("_Color") && !this.m_material.HasProperty("_TintColor"))
      this.enabled = false;
    if (!this.m_material.HasProperty("_TintColor"))
      return;
    this.m_tintColor = true;
  }

  private void Update()
  {
    this.m_color = !this.m_tintColor ? this.m_material.color : this.m_material.GetColor("_TintColor");
    if ((double) this.m_color.r < 0.00999999977648258 && (double) this.m_color.g < 0.00999999977648258 && (double) this.m_color.b < 0.00999999977648258)
      this.GetComponent<Renderer>().enabled = false;
    else
      this.GetComponent<Renderer>().enabled = true;
  }
}
