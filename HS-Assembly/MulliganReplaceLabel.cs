﻿// Decompiled with JetBrains decompiler
// Type: MulliganReplaceLabel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MulliganReplaceLabel : MonoBehaviour
{
  public UberText m_labelText;

  private void Awake()
  {
    this.m_labelText.Text = GameStrings.Get("GAMEPLAY_MULLIGAN_REPLACE");
  }
}
