﻿// Decompiled with JetBrains decompiler
// Type: ScreenEffectsMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ScreenEffectsMgr : MonoBehaviour
{
  private Camera m_EffectsObjectsCamera;
  private GameObject m_EffectsObjectsCameraGO;
  private Camera m_MainCamera;
  private ScreenEffectsRender m_ScreenEffectsRender;
  private static ScreenEffectsMgr s_Instance;
  private static List<ScreenEffect> m_ActiveScreenEffects;

  private void Awake()
  {
    ScreenEffectsMgr.s_Instance = this;
    if (ScreenEffectsMgr.m_ActiveScreenEffects == null)
      ScreenEffectsMgr.m_ActiveScreenEffects = new List<ScreenEffect>();
    if (SystemInfo.supportsRenderTextures || !(SystemInfo.graphicsDeviceName != "Null Device"))
      return;
    this.enabled = false;
  }

  private void Update()
  {
    if ((Object) this.m_MainCamera == (Object) null)
    {
      if ((Object) Camera.main == (Object) null)
        return;
      this.Init();
    }
    if ((Object) this.m_ScreenEffectsRender == (Object) null)
      return;
    if (ScreenEffectsMgr.m_ActiveScreenEffects != null && ScreenEffectsMgr.m_ActiveScreenEffects.Count > 0)
    {
      if (!this.m_ScreenEffectsRender.enabled)
        this.m_ScreenEffectsRender.enabled = true;
    }
    else if (this.m_ScreenEffectsRender.enabled)
      this.m_ScreenEffectsRender.enabled = false;
    this.UpdateCameraTransform();
  }

  private void OnDisable()
  {
    if ((Object) this.m_EffectsObjectsCameraGO != (Object) null)
      Object.DestroyImmediate((Object) this.m_EffectsObjectsCameraGO);
    if (!((Object) this.m_ScreenEffectsRender != (Object) null))
      return;
    this.m_ScreenEffectsRender.enabled = false;
  }

  private void OnDestroy()
  {
    ScreenEffectsMgr.s_Instance = (ScreenEffectsMgr) null;
    if (ScreenEffectsMgr.m_ActiveScreenEffects == null)
      return;
    ScreenEffectsMgr.m_ActiveScreenEffects.Clear();
    ScreenEffectsMgr.m_ActiveScreenEffects = (List<ScreenEffect>) null;
  }

  private void OnEnable()
  {
    if ((Object) Camera.main == (Object) null)
      return;
    this.Init();
  }

  public static ScreenEffectsMgr Get()
  {
    return ScreenEffectsMgr.s_Instance;
  }

  public static void RegisterScreenEffect(ScreenEffect effect)
  {
    if (ScreenEffectsMgr.m_ActiveScreenEffects == null)
      ScreenEffectsMgr.m_ActiveScreenEffects = new List<ScreenEffect>();
    if (ScreenEffectsMgr.m_ActiveScreenEffects.Contains(effect))
      return;
    ScreenEffectsMgr.m_ActiveScreenEffects.Add(effect);
  }

  public static void UnRegisterScreenEffect(ScreenEffect effect)
  {
    if (ScreenEffectsMgr.m_ActiveScreenEffects == null)
      return;
    ScreenEffectsMgr.m_ActiveScreenEffects.Remove(effect);
  }

  public int GetActiveScreenEffectsCount()
  {
    if (ScreenEffectsMgr.m_ActiveScreenEffects == null)
      return 0;
    return ScreenEffectsMgr.m_ActiveScreenEffects.Count;
  }

  private void Init()
  {
    this.m_MainCamera = Camera.main;
    if ((Object) this.m_MainCamera == (Object) null)
      return;
    this.m_ScreenEffectsRender = this.m_MainCamera.GetComponent<ScreenEffectsRender>();
    if ((Object) this.m_ScreenEffectsRender == (Object) null)
    {
      this.m_ScreenEffectsRender = this.m_MainCamera.gameObject.AddComponent<ScreenEffectsRender>();
      this.m_MainCamera.hdr = false;
    }
    else
      this.m_ScreenEffectsRender.enabled = true;
    this.CreateCamera(out this.m_EffectsObjectsCamera, out this.m_EffectsObjectsCameraGO, "ScreenEffectsObjectRenderCamera");
    this.m_EffectsObjectsCamera.depth = this.m_MainCamera.depth - 1f;
    this.m_EffectsObjectsCamera.clearFlags = CameraClearFlags.Color;
    this.m_EffectsObjectsCamera.backgroundColor = Color.clear;
    this.m_EffectsObjectsCameraGO.hideFlags = HideFlags.HideAndDontSave;
    SceneUtils.SetLayer(this.m_EffectsObjectsCameraGO, 23);
    this.m_EffectsObjectsCamera.enabled = false;
    this.m_ScreenEffectsRender.m_EffectsObjectsCamera = this.m_EffectsObjectsCamera;
    this.m_EffectsObjectsCamera.cullingMask = 8388865;
  }

  private void CreateCamera(out Camera camera, out GameObject cameraGO, string cameraName)
  {
    cameraGO = new GameObject(cameraName);
    SceneUtils.SetLayer(cameraGO, GameLayer.CameraMask);
    this.UpdateCameraTransform();
    camera = cameraGO.AddComponent<Camera>();
    camera.CopyFrom(this.m_MainCamera);
    camera.clearFlags = CameraClearFlags.Nothing;
    if (!(bool) ((Object) UniversalInputManager.Get()))
      return;
    UniversalInputManager.Get().AddIgnoredCamera(camera);
  }

  private void UpdateCameraTransform()
  {
    if ((Object) this.m_EffectsObjectsCameraGO == (Object) null || (Object) this.m_MainCamera == (Object) null)
      return;
    Transform transform = this.m_MainCamera.transform;
    this.m_EffectsObjectsCameraGO.transform.position = transform.position;
    this.m_EffectsObjectsCameraGO.transform.rotation = transform.rotation;
  }

  private void CreateBackPlane(Camera camera)
  {
    Vector3 worldPoint1 = camera.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, camera.farClipPlane));
    Vector3 worldPoint2 = camera.ViewportToWorldPoint(new Vector3(1f, 1f, camera.farClipPlane));
    Vector3 vector3 = new Vector3((float) (((double) worldPoint2.x - (double) worldPoint1.x) * 0.5), (float) (((double) worldPoint2.y - (double) worldPoint1.y) * 0.5), (float) (((double) worldPoint2.z - (double) worldPoint1.z) * 0.5));
    float farClipPlane = camera.farClipPlane;
    camera.gameObject.AddComponent<MeshFilter>();
    camera.gameObject.AddComponent<MeshRenderer>();
    camera.gameObject.GetComponent<Renderer>().GetComponent<MeshFilter>().mesh = new Mesh()
    {
      vertices = new Vector3[4]
      {
        new Vector3(-vector3.x, -vector3.z, farClipPlane),
        new Vector3(vector3.x, -vector3.z, farClipPlane),
        new Vector3(-vector3.x, vector3.z, farClipPlane),
        new Vector3(vector3.x, vector3.z, farClipPlane)
      },
      colors = new Color[4]
      {
        Color.black,
        Color.black,
        Color.black,
        Color.black
      },
      uv = new Vector2[4]
      {
        new Vector2(0.0f, 0.0f),
        new Vector2(1f, 0.0f),
        new Vector2(0.0f, 1f),
        new Vector2(1f, 1f)
      },
      normals = new Vector3[4]
      {
        Vector3.up,
        Vector3.up,
        Vector3.up,
        Vector3.up
      },
      triangles = new int[6]{ 3, 1, 2, 2, 1, 0 }
    };
    Material material = new Material(Shader.Find("Hidden/ScreenEffectsBackPlane"));
    camera.gameObject.GetComponent<Renderer>().sharedMaterial = material;
  }

  public enum EFFECT_TYPE
  {
    Glow,
    Distortion,
  }
}
