﻿// Decompiled with JetBrains decompiler
// Type: NTree`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class NTree<T>
{
  private T data;
  private LinkedList<NTree<T>> children;

  public NTree(T data)
  {
    this.data = data;
    this.children = new LinkedList<NTree<T>>();
  }

  public void AddDeepChild(params T[] traverse)
  {
    LinkedList<NTree<T>> children = this.children;
    foreach (T obj in traverse)
    {
      bool flag = false;
      using (LinkedList<NTree<T>>.Enumerator enumerator = children.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          NTree<T> current = enumerator.Current;
          if (EqualityComparer<T>.Default.Equals(current.data, obj))
          {
            children = current.children;
            flag = true;
            break;
          }
        }
      }
      if (!flag)
      {
        NTree<T> ntree = new NTree<T>(obj);
        children.AddLast(ntree);
        children = ntree.children;
      }
    }
  }

  public void SetData(T data)
  {
    this.data = data;
  }

  public void Traverse(TreeVisitor<T> visitor, TreePreTraverse previsitor, TreePostTraverse postvisitor, int ignoredepth = -1)
  {
    this.traverse(this, visitor, previsitor, postvisitor, ignoredepth);
  }

  private void traverse(NTree<T> node, TreeVisitor<T> visitor, TreePreTraverse previsitor, TreePostTraverse postvisitor, int ignoredepth)
  {
    if (visitor != null && ignoredepth < 0 && !visitor(node.data))
      return;
    using (LinkedList<NTree<T>>.Enumerator enumerator = node.children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NTree<T> current = enumerator.Current;
        if (previsitor != null && ignoredepth < 0)
          previsitor();
        this.traverse(current, visitor, previsitor, postvisitor, ignoredepth - 1);
        if (postvisitor != null && ignoredepth < 0)
          postvisitor();
      }
    }
  }
}
