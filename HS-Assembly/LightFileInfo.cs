﻿// Decompiled with JetBrains decompiler
// Type: LightFileInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LightFileInfo
{
  public readonly string FullName;
  public readonly string Name;
  public readonly string DirectoryName;

  public string Path
  {
    get
    {
      return this.FullName;
    }
  }

  public LightFileInfo(string path)
  {
    this.FullName = path;
    string[] strArray = path.Split(FileUtils.FOLDER_SEPARATOR_CHARS);
    this.Name = strArray[strArray.Length - 1];
    this.DirectoryName = strArray[strArray.Length - 2];
  }

  public static IEnumerable<LightFileInfo> Search(string path, string extension)
  {
    return LightFileInfo.Search(new string[1]{ path }, extension);
  }

  public static IEnumerable<LightFileInfo> Search(string[] paths, string extension)
  {
    List<LightFileInfo> matches = new List<LightFileInfo>();
    foreach (string path in paths)
    {
      float realtimeSinceStartup1 = Time.realtimeSinceStartup;
      LightFileInfo._Search(matches, path, extension);
      float realtimeSinceStartup2 = Time.realtimeSinceStartup;
      Log.Cameron.Print("PROFILE Search {0} {1} took {2}s", new object[3]
      {
        (object) path,
        (object) extension,
        (object) (float) ((double) realtimeSinceStartup2 - (double) realtimeSinceStartup1)
      });
    }
    return (IEnumerable<LightFileInfo>) matches;
  }

  public static void _Search(List<LightFileInfo> matches, string path, string extension)
  {
    foreach (FileInfo file in new DirectoryInfo(path).GetFiles(string.Format("*{0}", (object) extension), SearchOption.AllDirectories))
      matches.Add(new LightFileInfo(file.FullName));
  }
}
