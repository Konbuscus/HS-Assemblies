﻿// Decompiled with JetBrains decompiler
// Type: DevicePresetList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DevicePresetList : List<DevicePreset>
{
  public string[] GetNames()
  {
    string[] strArray = new string[this.Count];
    for (int index = 0; index < this.Count; ++index)
      strArray[index] = this[index].name;
    return strArray;
  }
}
