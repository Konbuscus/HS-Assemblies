﻿// Decompiled with JetBrains decompiler
// Type: CollectionPageDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class CollectionPageDisplay : MonoBehaviour
{
  private List<CollectionCardVisual> m_collectionCardVisuals = new List<CollectionCardVisual>();
  public UberText m_noMatchesFoundText;
  public GameObject m_cardStartPositionEightCards;
  public UberText m_pageCountText;
  public UberText m_classNameText;
  public GameObject m_classFlavorHeader;
  public GameObject m_heroSkinsDecor;
  public GameObject m_deckTemplateContainer;
  public GameObject m_basePage;
  public Material m_deckTemplatePageMaterial;
  private Material m_basePageMaterial;
  public MeshRenderer m_basePageRenderer;
  public Material m_wildHeaderMaterial;
  public Material m_standardHeaderMaterial;
  public Material m_wildPageMaterial;
  public Material m_standardPageMaterial;
  public Color m_standardTextColor;
  public Color m_wildTextColor;
  private bool m_isWild;

  private void Awake()
  {
    this.m_noMatchesFoundText.Text = GameStrings.Get("GLUE_COLLECTION_NO_MATCHES");
  }

  public static int GetMaxCardsPerPage()
  {
    CollectionPageLayoutSettings.Variables pageLayoutSettings = CollectionManagerDisplay.Get().GetCurrentPageLayoutSettings();
    return pageLayoutSettings.m_ColumnCount * pageLayoutSettings.m_RowCount;
  }

  public static int GetMaxCardsPerPage(CollectionManagerDisplay.ViewMode viewMode)
  {
    CollectionPageLayoutSettings.Variables pageLayoutSettings = CollectionManagerDisplay.Get().GetPageLayoutSettings(viewMode);
    return pageLayoutSettings.m_ColumnCount * pageLayoutSettings.m_RowCount;
  }

  public CollectionCardVisual GetCardVisual(string cardID, TAG_PREMIUM premium)
  {
    using (List<CollectionCardVisual>.Enumerator enumerator = this.m_collectionCardVisuals.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionCardVisual current = enumerator.Current;
        if (current.IsShown() && current.GetVisualType() == CollectionManagerDisplay.ViewMode.CARDS)
        {
          Actor actor = current.GetActor();
          if (actor.GetEntityDef().GetCardId().Equals(cardID) && actor.GetPremium() == premium)
            return current;
        }
      }
    }
    return (CollectionCardVisual) null;
  }

  public void Show()
  {
    for (int index = 0; index < this.m_collectionCardVisuals.Count; ++index)
    {
      CollectionCardVisual collectionCardVisual = this.GetCollectionCardVisual(index);
      if ((UnityEngine.Object) collectionCardVisual.GetActor() != (UnityEngine.Object) null)
        collectionCardVisual.Show();
    }
  }

  public void Hide()
  {
    for (int index = 0; index < this.m_collectionCardVisuals.Count; ++index)
      this.GetCollectionCardVisual(index).Hide();
  }

  public void UpdateCollectionCards(List<Actor> actorList, CollectionManagerDisplay.ViewMode mode, bool isMassDisenchanting)
  {
    int index1;
    for (index1 = 0; index1 < actorList.Count && index1 < CollectionPageDisplay.GetMaxCardsPerPage(); ++index1)
    {
      Actor actor = actorList[index1];
      CollectionCardVisual collectionCardVisual = this.GetCollectionCardVisual(index1);
      collectionCardVisual.SetActor(actor, mode);
      collectionCardVisual.Show();
      if (mode == CollectionManagerDisplay.ViewMode.HERO_SKINS)
        collectionCardVisual.SetHeroSkinBoxCollider();
      else
        collectionCardVisual.SetDefaultBoxCollider();
    }
    for (int index2 = index1; index2 < this.m_collectionCardVisuals.Count; ++index2)
    {
      CollectionCardVisual collectionCardVisual = this.GetCollectionCardVisual(index2);
      collectionCardVisual.SetActor((Actor) null, CollectionManagerDisplay.ViewMode.CARDS);
      collectionCardVisual.Hide();
    }
    this.UpdateFavoriteCardBack(mode);
    this.UpdateFavoriteHeroSkins(mode, isMassDisenchanting);
    this.UpdateHeroSkinNames(mode);
    this.UpdateCurrentPageCardLocks(false);
  }

  public void UpdateFavoriteHeroSkins(CollectionManagerDisplay.ViewMode mode, bool isMassDisenchanting)
  {
    bool flag1 = mode == CollectionManagerDisplay.ViewMode.HERO_SKINS;
    if ((UnityEngine.Object) this.m_heroSkinsDecor != (UnityEngine.Object) null)
      this.m_heroSkinsDecor.SetActive(flag1 && !isMassDisenchanting);
    if (!flag1)
      return;
    bool flag2 = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing) == null;
    using (List<CollectionCardVisual>.Enumerator enumerator = this.m_collectionCardVisuals.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionCardVisual current = enumerator.Current;
        if (current.IsShown())
        {
          Actor actor = current.GetActor();
          CollectionHeroSkin component = actor.GetComponent<CollectionHeroSkin>();
          if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
          {
            component.ShowShadow(actor.IsShown());
            EntityDef entityDef = actor.GetEntityDef();
            if (entityDef != null)
            {
              component.SetClass(entityDef.GetClass());
              bool show = false;
              if (flag2)
              {
                NetCache.CardDefinition favoriteHero = CollectionManager.Get().GetFavoriteHero(entityDef.GetClass());
                if (favoriteHero != null)
                  show = CollectionManager.Get().GetBestHeroesIOwn(entityDef.GetClass()).Count > 1 && !string.IsNullOrEmpty(favoriteHero.Name) && favoriteHero.Name == entityDef.GetCardId();
              }
              component.ShowFavoriteBanner(show);
            }
          }
        }
      }
    }
  }

  public void UpdateHeroSkinNames(CollectionManagerDisplay.ViewMode mode)
  {
    if (!(bool) UniversalInputManager.UsePhoneUI || mode != CollectionManagerDisplay.ViewMode.HERO_SKINS)
      return;
    using (List<CollectionCardVisual>.Enumerator enumerator = this.m_collectionCardVisuals.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionCardVisual current = enumerator.Current;
        if (current.IsShown())
        {
          CollectionHeroSkin component = current.GetActor().GetComponent<CollectionHeroSkin>();
          if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
            component.ShowCollectionManagerText();
        }
      }
    }
  }

  public void UpdateFavoriteCardBack(CollectionManagerDisplay.ViewMode mode)
  {
    if (mode != CollectionManagerDisplay.ViewMode.CARD_BACKS)
      return;
    int num = -1;
    if (!CollectionManager.Get().IsInEditMode())
      num = NetCache.Get().GetNetObject<NetCache.NetCacheCardBacks>().DefaultCardBack;
    using (List<CollectionCardVisual>.Enumerator enumerator = this.m_collectionCardVisuals.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionCardVisual current = enumerator.Current;
        if (current.IsShown())
        {
          CollectionCardBack component = current.GetActor().GetComponent<CollectionCardBack>();
          if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
            component.ShowFavoriteBanner(num == component.GetCardBackId());
        }
      }
    }
  }

  public void UpdateBasePage()
  {
    if (!((UnityEngine.Object) this.m_basePageMaterial != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_basePage != (UnityEngine.Object) null))
      return;
    this.m_basePage.GetComponent<MeshRenderer>().material = this.m_basePageMaterial;
  }

  public void UpdateDeckTemplatePage(Component deckTemplatePicker)
  {
    this.HideHeroSkinsDecor();
    if (!((UnityEngine.Object) deckTemplatePicker != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_deckTemplateContainer != (UnityEngine.Object) null))
      return;
    using (List<CollectionCardVisual>.Enumerator enumerator = this.m_collectionCardVisuals.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionCardVisual current = enumerator.Current;
        current.SetActor((Actor) null, CollectionManagerDisplay.ViewMode.CARDS);
        current.Hide();
      }
    }
    if ((UnityEngine.Object) this.m_basePage != (UnityEngine.Object) null)
    {
      this.m_basePageMaterial = this.m_basePage.GetComponent<MeshRenderer>().material;
      this.m_basePage.GetComponent<MeshRenderer>().material = this.m_deckTemplatePageMaterial;
    }
    GameUtils.SetParent(deckTemplatePicker, this.m_deckTemplateContainer, false);
    GameUtils.ResetTransform(deckTemplatePicker);
  }

  public void ShowNoMatchesFound(bool show)
  {
    this.m_noMatchesFoundText.gameObject.SetActive(show);
  }

  public void HideHeroSkinsDecor()
  {
    if (!((UnityEngine.Object) this.m_heroSkinsDecor != (UnityEngine.Object) null))
      return;
    this.m_heroSkinsDecor.SetActive(false);
  }

  public void UpdateCurrentPageCardLocks(bool playSound = false)
  {
    if (CollectionDeckTray.Get().GetCurrentContentType() != CollectionDeckTray.DeckContentTypes.Cards)
    {
      using (List<CollectionCardVisual>.Enumerator enumerator = this.m_collectionCardVisuals.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CollectionCardVisual current = enumerator.Current;
          if (current.IsShown())
            current.ShowLock(CollectionCardVisual.LockType.NONE);
        }
      }
    }
    else
    {
      CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
      using (List<CollectionCardVisual>.Enumerator enumerator = this.m_collectionCardVisuals.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CollectionCardVisual current = enumerator.Current;
          if (current.GetVisualType() != CollectionManagerDisplay.ViewMode.CARD_BACKS && current.GetVisualType() != CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
          {
            if (!current.IsShown())
            {
              current.ShowLock(CollectionCardVisual.LockType.NONE);
            }
            else
            {
              Actor actor = current.GetActor();
              string cardId = actor.GetEntityDef().GetCardId();
              TAG_PREMIUM premium = actor.GetPremium();
              CollectibleCard card = CollectionManager.Get().GetCard(cardId, premium);
              if (card.OwnedCount <= 0)
              {
                current.ShowLock(CollectionCardVisual.LockType.NONE);
              }
              else
              {
                DeckRuleset deckRuleset = CollectionManager.Get().GetDeckRuleset();
                List<RuleInvalidReason> reasons;
                List<DeckRule> brokenRules;
                if (deckRuleset == null || deckRuleset.CanAddToDeck(actor.GetEntityDef(), premium, taggedDeck, out reasons, out brokenRules))
                {
                  current.ShowLock(CollectionCardVisual.LockType.NONE, (string) null, playSound);
                }
                else
                {
                  string displayError = reasons[0].DisplayError;
                  CollectionCardVisual.LockType lockType = brokenRules[0].Type != DeckRule.RuleType.PLAYER_OWNS_EACH_COPY ? CollectionCardVisual.LockType.MAX_COPIES_IN_DECK : CollectionCardVisual.LockType.NO_MORE_INSTANCES;
                  if (brokenRules.Count > 1)
                  {
                    int index = brokenRules.FindIndex((Predicate<DeckRule>) (r => r.Type == DeckRule.RuleType.PLAYER_OWNS_EACH_COPY));
                    if (index >= 0)
                    {
                      int allMatchingSlots1 = taggedDeck.GetCardCountAllMatchingSlots(cardId, premium);
                      int ownedCount = card.OwnedCount;
                      int allMatchingSlots2 = taggedDeck.GetCardCountAllMatchingSlots(cardId, premium != TAG_PREMIUM.NORMAL ? TAG_PREMIUM.NORMAL : TAG_PREMIUM.GOLDEN);
                      bool flag = false;
                      if (premium == TAG_PREMIUM.GOLDEN && allMatchingSlots2 > 0)
                        flag = true;
                      else if (premium == TAG_PREMIUM.NORMAL && allMatchingSlots1 > ownedCount)
                        flag = true;
                      if (flag)
                      {
                        lockType = CollectionCardVisual.LockType.NO_MORE_INSTANCES;
                        displayError = reasons[index].DisplayError;
                      }
                    }
                  }
                  current.ShowLock(lockType, displayError, playSound);
                }
              }
            }
          }
          else
            current.ShowLock(CollectionCardVisual.LockType.NONE);
        }
      }
    }
  }

  public void SetIsWild(bool isWild)
  {
    if (isWild == this.m_isWild)
      return;
    this.m_isWild = isWild;
    if ((UnityEngine.Object) this.m_classFlavorHeader != (UnityEngine.Object) null)
      this.m_classFlavorHeader.GetComponent<Renderer>().material = !isWild ? this.m_standardHeaderMaterial : this.m_wildHeaderMaterial;
    if ((UnityEngine.Object) this.m_pageCountText != (UnityEngine.Object) null)
      this.m_pageCountText.TextColor = !CollectionManager.Get().IsShowingWildTheming((CollectionDeck) null) ? this.m_standardTextColor : this.m_wildTextColor;
    this.m_basePageRenderer.material = !isWild ? this.m_standardPageMaterial : this.m_wildPageMaterial;
  }

  public void SetClass(TAG_CLASS? classTag)
  {
    if (!classTag.HasValue)
    {
      this.SetClassNameText(string.Empty);
      if (!((UnityEngine.Object) this.m_classFlavorHeader != (UnityEngine.Object) null))
        return;
      this.m_classFlavorHeader.SetActive(false);
    }
    else
    {
      TAG_CLASS tagClass = classTag.Value;
      this.SetClassNameText(GameStrings.GetClassName(tagClass));
      CollectionPageDisplay.SetClassFlavorTextures(this.m_classFlavorHeader, CollectionPageDisplay.TagClassToHeaderClass(tagClass));
    }
  }

  public void SetHeroSkins()
  {
    this.SetClassNameText(GameStrings.Get("GLUE_COLLECTION_MANAGER_HERO_SKINS_TITLE"));
    CollectionPageDisplay.SetClassFlavorTextures(this.m_classFlavorHeader, CollectionPageDisplay.HEADER_CLASS.HEROSKINS);
  }

  public void SetCardBacks()
  {
    this.SetClassNameText(GameStrings.Get("GLUE_COLLECTION_MANAGER_CARD_BACKS_TITLE"));
    CollectionPageDisplay.SetClassFlavorTextures(this.m_classFlavorHeader, CollectionPageDisplay.HEADER_CLASS.CARDBACKS);
  }

  public void SetDeckTemplates()
  {
    this.SetClassNameText(string.Empty);
    if (!((UnityEngine.Object) this.m_classFlavorHeader != (UnityEngine.Object) null))
      return;
    this.m_classFlavorHeader.SetActive(false);
  }

  public void SetPageCountText(string text)
  {
    if (!((UnityEngine.Object) this.m_pageCountText != (UnityEngine.Object) null))
      return;
    this.m_pageCountText.Text = text;
  }

  public void ActivatePageCountText(bool active)
  {
    if (!((UnityEngine.Object) this.m_pageCountText != (UnityEngine.Object) null))
      return;
    this.m_pageCountText.gameObject.SetActive(active);
  }

  public TAG_CLASS? GetFirstCardClass()
  {
    if (this.m_collectionCardVisuals.Count == 0)
      return new TAG_CLASS?();
    CollectionCardVisual collectionCardVisual = this.m_collectionCardVisuals[0];
    if (!collectionCardVisual.IsShown())
      return new TAG_CLASS?();
    Actor actor = collectionCardVisual.GetActor();
    if (!actor.IsShown())
      return new TAG_CLASS?();
    EntityDef entityDef = actor.GetEntityDef();
    if (entityDef == null)
      return new TAG_CLASS?();
    return new TAG_CLASS?(entityDef.GetClass());
  }

  private CollectionCardVisual GetCollectionCardVisual(int index)
  {
    CollectionPageLayoutSettings.Variables pageLayoutSettings = CollectionManagerDisplay.Get().GetCurrentPageLayoutSettings();
    float columnSpacing = pageLayoutSettings.m_ColumnSpacing;
    int columnCount = pageLayoutSettings.m_ColumnCount;
    float num = columnSpacing * (float) (columnCount - 1);
    float scale = pageLayoutSettings.m_Scale;
    float rowSpacing = pageLayoutSettings.m_RowSpacing;
    Vector3 position = this.m_cardStartPositionEightCards.transform.localPosition + pageLayoutSettings.m_Offset;
    int rowNum = index / columnCount;
    position.x += (float) ((double) (index % columnCount) * (double) columnSpacing - (double) num * 0.5);
    position.z -= rowSpacing * (float) rowNum;
    CollectionCardVisual collectionCardVisual;
    if (index == this.m_collectionCardVisuals.Count)
    {
      collectionCardVisual = (CollectionCardVisual) GameUtils.Instantiate((Component) CollectionManagerDisplay.Get().GetCardVisualPrefab(), this.gameObject, false);
      this.m_collectionCardVisuals.Insert(index, collectionCardVisual);
    }
    else
      collectionCardVisual = this.m_collectionCardVisuals[index];
    collectionCardVisual.SetCMRow(rowNum);
    collectionCardVisual.transform.localScale = new Vector3(scale, scale, scale);
    collectionCardVisual.transform.position = this.transform.TransformPoint(position);
    return collectionCardVisual;
  }

  private void SetClassNameText(string className)
  {
    if (!((UnityEngine.Object) this.m_classNameText != (UnityEngine.Object) null))
      return;
    this.m_classNameText.Text = className;
  }

  public static CollectionPageDisplay.HEADER_CLASS TagClassToHeaderClass(TAG_CLASS classTag)
  {
    string str = classTag.ToString();
    if (Enum.IsDefined(typeof (CollectionPageDisplay.HEADER_CLASS), (object) str))
      return (CollectionPageDisplay.HEADER_CLASS) Enum.Parse(typeof (CollectionPageDisplay.HEADER_CLASS), str);
    return CollectionPageDisplay.HEADER_CLASS.INVALID;
  }

  public static void SetClassFlavorTextures(GameObject header, CollectionPageDisplay.HEADER_CLASS headerClass)
  {
    if ((UnityEngine.Object) header == (UnityEngine.Object) null)
      return;
    int num = (int) headerClass;
    float x = (double) num >= 8.0 ? 0.5f : 0.0f;
    float y = (float) (-(double) num / 8.0);
    header.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(x, y));
    if (!((UnityEngine.Object) header != (UnityEngine.Object) null))
      return;
    header.SetActive(true);
  }

  public enum HEADER_CLASS
  {
    INVALID,
    SHAMAN,
    PALADIN,
    MAGE,
    DRUID,
    HUNTER,
    ROGUE,
    WARRIOR,
    PRIEST,
    WARLOCK,
    HEROSKINS,
    CARDBACKS,
  }
}
