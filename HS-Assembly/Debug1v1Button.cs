﻿// Decompiled with JetBrains decompiler
// Type: Debug1v1Button
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using UnityEngine;

public class Debug1v1Button : PegUIElement
{
  public int m_missionId;
  public GameObject m_heroImage;
  public UberText m_name;
  private GameObject m_heroPowerObject;

  public static bool HasUsedDebugMenu { get; set; }

  private void Start()
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(this.m_missionId);
    if (record == null)
      return;
    string shortName = (string) record.ShortName;
    if (!((Object) this.m_name != (Object) null) || string.IsNullOrEmpty(shortName))
      return;
    this.m_name.Text = shortName;
  }

  private void OnCardDefLoaded(string cardID, CardDef cardDef, object userData)
  {
    this.m_heroImage.GetComponent<Renderer>().material.mainTexture = cardDef.GetPortraitTexture();
  }

  protected override void OnRelease()
  {
    base.OnRelease();
    long selectedDeckId = DeckPickerTrayDisplay.Get().GetSelectedDeckID();
    Debug1v1Button.HasUsedDebugMenu = true;
    GameMgr.Get().FindGame(GameType.GT_TAVERNBRAWL, FormatType.FT_WILD, this.m_missionId, selectedDeckId, 0L);
    Object.Destroy((Object) this.transform.parent.gameObject);
  }
}
