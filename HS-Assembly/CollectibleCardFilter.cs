﻿// Decompiled with JetBrains decompiler
// Type: CollectibleCardFilter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

public class CollectibleCardFilter
{
  private static Map<char, string> s_europeanConversionTable = new Map<char, string>()
  {
    {
      'œ',
      "oe"
    },
    {
      'æ',
      "ae"
    },
    {
      '’',
      "'"
    },
    {
      '«',
      "\""
    },
    {
      '»',
      "\""
    },
    {
      'ä',
      "ae"
    },
    {
      'ü',
      "ue"
    },
    {
      'ö',
      "oe"
    },
    {
      'ß',
      "ss"
    }
  };
  private int? m_filterOwnedMinimum = new int?(1);
  private TAG_CARD_SET[] m_filterCardSets;
  private TAG_CLASS[] m_filterClasses;
  private TAG_CARDTYPE[] m_filterCardTypes;
  private int? m_filterManaCost;
  private List<CollectibleCardFilter.FilterMask> m_filterMasks;
  private bool? m_filterOnlyCraftable;
  private string m_filterText;
  private bool m_filterIsHero;
  private DeckRuleset m_deckRuleset;

  public static CollectibleCardFilter.FilterMask FilterMaskFromPremiumType(TAG_PREMIUM? premiumType)
  {
    if (!premiumType.HasValue)
      return CollectibleCardFilter.FilterMask.PREMIUM_ALL;
    CollectibleCardFilter.FilterMask filterMask = CollectibleCardFilter.FilterMask.NONE;
    if ((premiumType.GetValueOrDefault() != TAG_PREMIUM.NORMAL ? 0 : (premiumType.HasValue ? 1 : 0)) != 0)
      filterMask |= CollectibleCardFilter.FilterMask.PREMIUM_NORMAL;
    if ((premiumType.GetValueOrDefault() != TAG_PREMIUM.GOLDEN ? 0 : (premiumType.HasValue ? 1 : 0)) != 0)
      filterMask |= CollectibleCardFilter.FilterMask.PREMIUM_GOLDEN;
    return filterMask;
  }

  public void SetDeckRuleset(DeckRuleset deckRuleset)
  {
    this.m_deckRuleset = deckRuleset;
  }

  public void FilterTheseCardSets(params TAG_CARD_SET[] cardSets)
  {
    this.m_filterCardSets = (TAG_CARD_SET[]) null;
    if (cardSets == null || cardSets.Length <= 0)
      return;
    this.m_filterCardSets = cardSets;
  }

  public bool CardSetFilterIncludesWild()
  {
    if (this.m_filterCardSets == null)
      return true;
    foreach (TAG_CARD_SET filterCardSet in this.m_filterCardSets)
    {
      if (GameUtils.IsSetRotated(filterCardSet))
        return true;
    }
    return false;
  }

  public bool CardSetFilterIsAllStandardSets()
  {
    if (this.m_filterCardSets == null)
      return false;
    return new HashSet<TAG_CARD_SET>((IEnumerable<TAG_CARD_SET>) this.m_filterCardSets).SetEquals((IEnumerable<TAG_CARD_SET>) new List<TAG_CARD_SET>((IEnumerable<TAG_CARD_SET>) GameUtils.GetStandardSets()));
  }

  public void FilterTheseClasses(params TAG_CLASS[] classTypes)
  {
    this.m_filterClasses = (TAG_CLASS[]) null;
    if (classTypes == null || classTypes.Length <= 0)
      return;
    this.m_filterClasses = classTypes;
  }

  public void FilterTheseCardTypes(params TAG_CARDTYPE[] cardTypes)
  {
    this.m_filterCardTypes = (TAG_CARDTYPE[]) null;
    if (cardTypes == null || cardTypes.Length <= 0)
      return;
    this.m_filterCardTypes = cardTypes;
  }

  public void FilterManaCost(int? manaCost)
  {
    this.m_filterManaCost = manaCost;
  }

  public void FilterOnlyOwned(bool owned)
  {
    this.m_filterOwnedMinimum = new int?();
    if (!owned)
      return;
    this.m_filterOwnedMinimum = new int?(1);
  }

  public void FilterByMask(TAG_PREMIUM premiumType)
  {
    this.FilterByMask(new List<CollectibleCardFilter.FilterMask>()
    {
      CollectibleCardFilter.FilterMaskFromPremiumType(new TAG_PREMIUM?(premiumType))
    });
  }

  public void FilterByMask(List<CollectibleCardFilter.FilterMask> filterMasks)
  {
    if (filterMasks == null)
      filterMasks = new List<CollectibleCardFilter.FilterMask>()
      {
        CollectibleCardFilter.FilterMask.ALL
      };
    this.m_filterMasks = filterMasks;
  }

  public void FilterOnlyCraftable(bool onlyCraftable)
  {
    this.m_filterOnlyCraftable = new bool?();
    if (!onlyCraftable)
      return;
    this.m_filterOnlyCraftable = new bool?(true);
  }

  public void FilterSearchText(string searchText)
  {
    this.m_filterText = searchText;
  }

  public bool HasSearchText()
  {
    return !string.IsNullOrEmpty(this.m_filterText);
  }

  public void FilterHero(bool isHero)
  {
    this.m_filterIsHero = isHero;
  }

  public List<CollectibleCard> GenerateList()
  {
    CollectionManager collectionManager = CollectionManager.Get();
    int? filterManaCost = this.m_filterManaCost;
    bool? nullable = new bool?(this.m_filterIsHero);
    int? filterOwnedMinimum = this.m_filterOwnedMinimum;
    string filterText = this.m_filterText;
    List<CollectibleCardFilter.FilterMask> filterMasks = this.m_filterMasks;
    int? manaCost = filterManaCost;
    TAG_CARD_SET[] filterCardSets = this.m_filterCardSets;
    TAG_CLASS[] filterClasses = this.m_filterClasses;
    TAG_CARDTYPE[] filterCardTypes = this.m_filterCardTypes;
    TAG_RARITY? rarity = new TAG_RARITY?();
    TAG_RACE? race = new TAG_RACE?();
    bool? isHero = nullable;
    int? minOwned = filterOwnedMinimum;
    bool? notSeen = new bool?();
    bool? filterOnlyCraftable = this.m_filterOnlyCraftable;
    // ISSUE: variable of the null type
    __Null local = null;
    DeckRuleset deckRuleset = this.m_deckRuleset;
    return collectionManager.FindOrderedCards(filterText, filterMasks, manaCost, filterCardSets, filterClasses, filterCardTypes, rarity, race, isHero, minOwned, notSeen, filterOnlyCraftable, (CollectionManager.CollectibleCardFilterFunc[]) local, deckRuleset);
  }

  public static void AddSearchableTokensToSet(string str, HashSet<string> addToList, bool split = true)
  {
    string[] strArray1;
    if (split)
      strArray1 = str.Split(new char[2]{ ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
    else
      strArray1 = new string[1]{ str };
    string[] strArray2 = strArray1;
    foreach (string token in strArray2)
      CollectibleCardFilter.AddSingleSearchableTokenToSet(token, addToList);
    if (strArray2.Length <= 1)
      return;
    CollectibleCardFilter.AddSingleSearchableTokenToSet(str, addToList);
  }

  public static void AddSearchableTokensToSet<T>(T structType, Func<T, bool> hasTypeString, Func<T, string> getTypeString, HashSet<string> addToList) where T : struct
  {
    if (!hasTypeString(structType))
      return;
    CollectibleCardFilter.AddSearchableTokensToSet(getTypeString(structType), addToList, true);
  }

  private static void AddSingleSearchableTokenToSet(string token, HashSet<string> addToList)
  {
    string lower = token.ToLower();
    string str1 = CollectibleCardFilter.ConvertEuropeanCharacters(lower);
    string str2 = CollectibleCardFilter.RemoveDiacritics(lower);
    addToList.Add(lower);
    if (!lower.Equals(str1))
      addToList.Add(str1);
    if (lower.Equals(str2))
      return;
    addToList.Add(str2);
  }

  public static List<CollectionManager.CollectibleCardFilterFunc> FiltersFromSearchString(string searchString)
  {
    char[] chArray1 = new char[1]{ '+' };
    char[] chArray2 = new char[1]{ '-' };
    List<CollectionManager.CollectibleCardFilterFunc> collectibleCardFilterFuncList = new List<CollectionManager.CollectibleCardFilterFunc>();
    if (!string.IsNullOrEmpty(searchString))
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      CollectibleCardFilter.\u003CFiltersFromSearchString\u003Ec__AnonStorey37A stringCAnonStorey37A = new CollectibleCardFilter.\u003CFiltersFromSearchString\u003Ec__AnonStorey37A();
      string str1 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_GOLDEN");
      string str2 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_ARTIST");
      string str3 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_HEALTH");
      string str4 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_ATTACK");
      string str5 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_OWNED");
      string str6 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_MANA");
      string str7 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_MISSING");
      string str8 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_EXTRA");
      string str9 = GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_NEW");
      string[] strArray1 = searchString.ToLower().Split(' ');
      // ISSUE: reference to a compiler-generated field
      stringCAnonStorey37A.regularTokens = new StringBuilder();
      for (int index = 0; index < strArray1.Length; ++index)
      {
        if (!(strArray1[index] == str8) && !(strArray1[index] == str7))
        {
          if (strArray1[index] == str9)
            collectibleCardFilterFuncList.Add((CollectionManager.CollectibleCardFilterFunc) (card => card.IsNewCard));
          else if (strArray1[index] == str1)
          {
            collectibleCardFilterFuncList.Add((CollectionManager.CollectibleCardFilterFunc) (card => card.PremiumType == TAG_PREMIUM.GOLDEN));
          }
          else
          {
            bool flag1 = false;
            char[] chArray3 = new char[2]{ ':', '：' };
            if (((IEnumerable<char>) chArray3).Any<char>(new Func<char, bool>(((StringUtils) strArray1[index]).Contains)))
            {
              string[] strArray2 = strArray1[index].Split(chArray3);
              if (strArray2.Length == 2)
              {
                // ISSUE: object of a compiler-generated type is created
                // ISSUE: variable of a compiler-generated type
                CollectibleCardFilter.\u003CFiltersFromSearchString\u003Ec__AnonStorey379 stringCAnonStorey379 = new CollectibleCardFilter.\u003CFiltersFromSearchString\u003Ec__AnonStorey379();
                string str10 = strArray2[0].Trim();
                // ISSUE: reference to a compiler-generated field
                stringCAnonStorey379.val = strArray2[1].Trim();
                bool flag2 = false;
                bool flag3 = false;
                // ISSUE: reference to a compiler-generated field
                // ISSUE: reference to a compiler-generated field
                if (stringCAnonStorey379.val.TrimEnd(chArray1) != stringCAnonStorey379.val)
                {
                  // ISSUE: reference to a compiler-generated field
                  // ISSUE: reference to a compiler-generated field
                  stringCAnonStorey379.val = stringCAnonStorey379.val.TrimEnd(chArray1);
                  flag2 = true;
                }
                // ISSUE: reference to a compiler-generated field
                // ISSUE: reference to a compiler-generated field
                if (stringCAnonStorey379.val.TrimEnd(chArray2) != stringCAnonStorey379.val)
                {
                  // ISSUE: reference to a compiler-generated field
                  // ISSUE: reference to a compiler-generated field
                  stringCAnonStorey379.val = stringCAnonStorey379.val.TrimEnd(chArray2);
                  flag3 = true;
                }
                // ISSUE: reference to a compiler-generated field
                stringCAnonStorey379.minVal = -1;
                // ISSUE: reference to a compiler-generated field
                stringCAnonStorey379.maxVal = -1;
                bool flag4 = false;
                // ISSUE: reference to a compiler-generated field
                if (stringCAnonStorey379.val.Contains("-"))
                {
                  // ISSUE: reference to a compiler-generated field
                  string[] strArray3 = stringCAnonStorey379.val.Split('-');
                  if (strArray3.Length == 2)
                  {
                    // ISSUE: reference to a compiler-generated field
                    // ISSUE: reference to a compiler-generated field
                    flag4 = int.TryParse(strArray3[0].Trim(), out stringCAnonStorey379.minVal) && int.TryParse(strArray3[1].Trim(), out stringCAnonStorey379.maxVal);
                  }
                }
                else
                {
                  // ISSUE: reference to a compiler-generated field
                  // ISSUE: reference to a compiler-generated field
                  flag4 = int.TryParse(stringCAnonStorey379.val, out stringCAnonStorey379.minVal);
                  // ISSUE: reference to a compiler-generated field
                  // ISSUE: reference to a compiler-generated field
                  stringCAnonStorey379.maxVal = stringCAnonStorey379.minVal;
                }
                if (flag4)
                {
                  if (flag3)
                  {
                    // ISSUE: reference to a compiler-generated field
                    stringCAnonStorey379.minVal = int.MinValue;
                  }
                  if (flag2)
                  {
                    // ISSUE: reference to a compiler-generated field
                    stringCAnonStorey379.maxVal = int.MaxValue;
                  }
                  if (str10 == str4)
                  {
                    // ISSUE: reference to a compiler-generated method
                    collectibleCardFilterFuncList.Add(new CollectionManager.CollectibleCardFilterFunc(stringCAnonStorey379.\u003C\u003Em__9D));
                    collectibleCardFilterFuncList.Add((CollectionManager.CollectibleCardFilterFunc) (card =>
                    {
                      if (card.CardType != TAG_CARDTYPE.MINION)
                        return card.CardType == TAG_CARDTYPE.WEAPON;
                      return true;
                    }));
                    flag1 = true;
                  }
                  if (str10 == str3)
                  {
                    // ISSUE: reference to a compiler-generated method
                    collectibleCardFilterFuncList.Add(new CollectionManager.CollectibleCardFilterFunc(stringCAnonStorey379.\u003C\u003Em__9F));
                    collectibleCardFilterFuncList.Add((CollectionManager.CollectibleCardFilterFunc) (card => card.CardType == TAG_CARDTYPE.MINION));
                    flag1 = true;
                  }
                  if (str10 == str6)
                  {
                    // ISSUE: reference to a compiler-generated method
                    collectibleCardFilterFuncList.Add(new CollectionManager.CollectibleCardFilterFunc(stringCAnonStorey379.\u003C\u003Em__A1));
                    flag1 = true;
                  }
                  if (str10 == str5)
                  {
                    // ISSUE: reference to a compiler-generated method
                    collectibleCardFilterFuncList.Add(new CollectionManager.CollectibleCardFilterFunc(stringCAnonStorey379.\u003C\u003Em__A2));
                    flag1 = true;
                  }
                }
                else if (str10 == str2)
                {
                  // ISSUE: reference to a compiler-generated method
                  collectibleCardFilterFuncList.Add(new CollectionManager.CollectibleCardFilterFunc(stringCAnonStorey379.\u003C\u003Em__A3));
                  flag1 = true;
                }
              }
            }
            if (!flag1)
            {
              // ISSUE: reference to a compiler-generated field
              stringCAnonStorey37A.regularTokens.Append(strArray1[index]);
              // ISSUE: reference to a compiler-generated field
              stringCAnonStorey37A.regularTokens.Append(" ");
            }
          }
        }
      }
      // ISSUE: reference to a compiler-generated method
      collectibleCardFilterFuncList.Add(new CollectionManager.CollectibleCardFilterFunc(stringCAnonStorey37A.\u003C\u003Em__A4));
    }
    return collectibleCardFilterFuncList;
  }

  public static string ConvertEuropeanCharacters(string input)
  {
    int length = input.Length;
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < length; ++index)
    {
      string str;
      if (CollectibleCardFilter.s_europeanConversionTable.TryGetValue(input[index], out str))
        stringBuilder.Append(str);
      else
        stringBuilder.Append(input[index]);
    }
    return stringBuilder.ToString();
  }

  public static string RemoveDiacritics(string input)
  {
    string str = input.Normalize(NormalizationForm.FormD);
    int length = str.Length;
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < length; ++index)
    {
      if (CharUnicodeInfo.GetUnicodeCategory(str[index]) != UnicodeCategory.NonSpacingMark)
        stringBuilder.Append(str[index]);
    }
    return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
  }

  [Flags]
  public enum FilterMask
  {
    NONE = 0,
    PREMIUM_NORMAL = 2,
    PREMIUM_GOLDEN = 4,
    PREMIUM_ALL = PREMIUM_GOLDEN | PREMIUM_NORMAL,
    OWNED = 8,
    UNOWNED = 16,
    ALL = -1,
  }

  public delegate void OnResultsUpdated();
}
