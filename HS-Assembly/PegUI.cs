﻿// Decompiled with JetBrains decompiler
// Type: PegUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class PegUI : MonoBehaviour
{
  private static readonly GameLayer[] HIT_TEST_PRIORITY = new GameLayer[10]{ GameLayer.IgnoreFullScreenEffects, GameLayer.BackgroundUI, GameLayer.PerspectiveUI, GameLayer.CameraMask, GameLayer.UI, GameLayer.BattleNet, GameLayer.BattleNetFriendList, GameLayer.BattleNetChat, GameLayer.BattleNetDialog, GameLayer.HighPriorityUI };
  private List<PegUICustomBehavior> m_customBehaviors = new List<PegUICustomBehavior>();
  private bool m_hasFocus = true;
  private const float PRESS_VS_TAP_TOLERANCE = 0.4f;
  private const float DOUBLECLICK_TOLERANCE = 0.7f;
  private const float DOUBLECLICK_COUNT_DISABLED = -1f;
  private const float MOUSEDOWN_COUNT_DISABLED = -1f;
  public Camera orthographicUICam;
  private Camera m_UICam;
  private PegUIElement m_prevElement;
  private PegUIElement m_currentElement;
  private PegUIElement m_mouseDownElement;
  public static PegUI s_instance;
  private float m_mouseDownTimer;
  private float m_lastClickTimer;
  private Vector3 m_lastClickPosition;
  private PegUI.DelSwipeListener m_swipeListener;

  private void Awake()
  {
    PegUI.s_instance = this;
    this.m_lastClickPosition = Vector3.zero;
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) UniversalInputManager.Get() != (UnityEngine.Object) null)
      UniversalInputManager.Get().UnregisterMouseOnOrOffScreenListener(new UniversalInputManager.MouseOnOrOffScreenCallback(this.OnMouseOnOrOffScreen));
    if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
      ApplicationMgr.Get().RemoveFocusChangedListener(new ApplicationMgr.FocusChangedCallback(this.OnAppFocusChanged));
    PegUI.s_instance = (PegUI) null;
  }

  private void Start()
  {
    UniversalInputManager.Get().RegisterMouseOnOrOffScreenListener(new UniversalInputManager.MouseOnOrOffScreenCallback(this.OnMouseOnOrOffScreen));
    ApplicationMgr.Get().AddFocusChangedListener(new ApplicationMgr.FocusChangedCallback(this.OnAppFocusChanged));
  }

  private void Update()
  {
    this.MouseInputUpdate();
  }

  public static PegUI Get()
  {
    return PegUI.s_instance;
  }

  public PegUIElement GetMousedOverElement()
  {
    return this.m_currentElement;
  }

  public PegUIElement GetPrevMousedOverElement()
  {
    return this.m_prevElement;
  }

  public void SetInputCamera(Camera cam)
  {
    this.m_UICam = cam;
    if (!((UnityEngine.Object) this.m_UICam == (UnityEngine.Object) null))
      return;
    Debug.Log((object) "UICam is null!");
  }

  public PegUIElement FindHitElement()
  {
    if (UniversalInputManager.Get().IsTouchMode() && !UniversalInputManager.Get().GetMouseButton(0) && !UniversalInputManager.Get().GetMouseButtonUp(0))
      return (PegUIElement) null;
    RaycastHit hitInfo;
    foreach (GameLayer layer in PegUI.HIT_TEST_PRIORITY)
    {
      if (UniversalInputManager.Get().GetInputHitInfo(layer, out hitInfo))
        return hitInfo.transform.GetComponent<PegUIElement>();
    }
    if (UniversalInputManager.Get().GetInputHitInfo(this.m_UICam, out hitInfo))
      return hitInfo.transform.GetComponent<PegUIElement>();
    return (PegUIElement) null;
  }

  public void DoMouseDown(PegUIElement element, Vector3 mouseDownPos)
  {
    this.m_currentElement = element;
    this.m_mouseDownElement = element;
    this.m_currentElement.TriggerPress();
    this.m_lastClickPosition = mouseDownPos;
    if ((double) Vector3.Distance(this.m_lastClickPosition, UniversalInputManager.Get().GetMousePosition()) <= (double) this.m_currentElement.GetDragTolerance())
      return;
    this.m_currentElement.TriggerHold();
  }

  public void RemoveAsMouseDownElement(PegUIElement element)
  {
    if ((UnityEngine.Object) this.m_mouseDownElement == (UnityEngine.Object) null || (UnityEngine.Object) element != (UnityEngine.Object) this.m_mouseDownElement)
      return;
    this.m_mouseDownElement.TriggerReleaseAll((UnityEngine.Object) this.m_currentElement == (UnityEngine.Object) this.m_mouseDownElement);
    this.m_mouseDownElement = (PegUIElement) null;
  }

  private void MouseInputUpdate()
  {
    if ((UnityEngine.Object) this.m_UICam == (UnityEngine.Object) null || !this.m_hasFocus)
      return;
    bool flag = false;
    using (List<PegUICustomBehavior>.Enumerator enumerator = this.m_customBehaviors.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.UpdateUI())
        {
          flag = true;
          break;
        }
      }
    }
    if (flag)
    {
      if ((UnityEngine.Object) this.m_mouseDownElement != (UnityEngine.Object) null)
        this.m_mouseDownElement.TriggerOut();
      this.m_mouseDownElement = (PegUIElement) null;
    }
    else
    {
      if (UniversalInputManager.Get().GetMouseButton(0) && (UnityEngine.Object) this.m_mouseDownElement != (UnityEngine.Object) null && (this.m_lastClickPosition != Vector3.zero && (double) Vector3.Distance(this.m_lastClickPosition, UniversalInputManager.Get().GetMousePosition()) > (double) this.m_mouseDownElement.GetDragTolerance()))
        this.m_mouseDownElement.TriggerHold();
      if ((double) this.m_lastClickTimer != -1.0)
        this.m_lastClickTimer += Time.deltaTime;
      if ((double) this.m_mouseDownTimer != -1.0)
        this.m_mouseDownTimer += Time.deltaTime;
      PegUIElement hitElement = this.FindHitElement();
      if ((UnityEngine.Object) hitElement != (UnityEngine.Object) null && ApplicationMgr.IsInternal() && Options.Get().GetInt(Option.PEGUI_DEBUG) >= 3)
        Debug.Log((object) string.Format("{0,-7} {1}", (object) "HIT:", (object) DebugUtils.GetHierarchyPath((UnityEngine.Object) hitElement, '/')));
      this.m_prevElement = this.m_currentElement;
      this.m_currentElement = !(bool) ((UnityEngine.Object) hitElement) || !hitElement.IsEnabled() ? (PegUIElement) null : hitElement;
      if ((bool) ((UnityEngine.Object) this.m_prevElement) && (UnityEngine.Object) this.m_currentElement != (UnityEngine.Object) this.m_prevElement)
      {
        PegCursor.Get().SetMode(PegCursor.Mode.UP);
        this.m_prevElement.TriggerOut();
      }
      if ((UnityEngine.Object) this.m_currentElement == (UnityEngine.Object) null)
      {
        if (UniversalInputManager.Get().GetMouseButtonDown(0))
          PegCursor.Get().SetMode(PegCursor.Mode.DOWN);
        else if (!UniversalInputManager.Get().GetMouseButton(0))
          PegCursor.Get().SetMode(PegCursor.Mode.UP);
        if (!(bool) ((UnityEngine.Object) this.m_mouseDownElement) || !UniversalInputManager.Get().GetMouseButtonUp(0))
          return;
        this.m_mouseDownElement.TriggerReleaseAll(false);
        this.m_mouseDownElement = (PegUIElement) null;
      }
      else
      {
        if (this.UpdateMouseLeftClick() || this.UpdateMouseLeftHold() || this.UpdateMouseRightClick())
          return;
        this.UpdateMouseOver();
      }
    }
  }

  private bool UpdateMouseLeftClick()
  {
    bool flag = false;
    if (UniversalInputManager.Get().GetMouseButtonDown(0))
    {
      flag = true;
      if (this.m_currentElement.GetCursorDown() != PegCursor.Mode.NONE)
        PegCursor.Get().SetMode(this.m_currentElement.GetCursorDown());
      else
        PegCursor.Get().SetMode(PegCursor.Mode.DOWN);
      this.m_mouseDownTimer = 0.0f;
      if (UniversalInputManager.Get().IsTouchMode() && this.m_currentElement.GetReceiveOverWithMouseDown())
        this.m_currentElement.TriggerOver();
      this.m_currentElement.TriggerPress();
      this.m_lastClickPosition = UniversalInputManager.Get().GetMousePosition();
      this.m_mouseDownElement = this.m_currentElement;
    }
    if (UniversalInputManager.Get().GetMouseButtonUp(0))
    {
      flag = true;
      if ((double) this.m_lastClickTimer > 0.0 && (double) this.m_lastClickTimer <= 0.699999988079071 && this.m_currentElement.GetDoubleClickEnabled())
      {
        this.m_currentElement.TriggerDoubleClick();
        this.m_lastClickTimer = -1f;
      }
      else
      {
        if ((UnityEngine.Object) this.m_mouseDownElement == (UnityEngine.Object) this.m_currentElement || this.m_currentElement.GetReceiveReleaseWithoutMouseDown())
        {
          if ((double) this.m_mouseDownTimer <= 0.400000005960464)
            this.m_currentElement.TriggerTap();
          this.m_currentElement.TriggerRelease();
        }
        if (this.m_currentElement.GetReceiveOverWithMouseDown())
          this.m_currentElement.TriggerOut();
        if ((bool) ((UnityEngine.Object) this.m_mouseDownElement))
        {
          this.m_lastClickTimer = 0.0f;
          this.m_mouseDownElement.TriggerReleaseAll((UnityEngine.Object) this.m_currentElement == (UnityEngine.Object) this.m_mouseDownElement);
          this.m_mouseDownElement = (PegUIElement) null;
        }
      }
      if (this.m_currentElement.GetCursorOver() != PegCursor.Mode.NONE)
        PegCursor.Get().SetMode(this.m_currentElement.GetCursorOver());
      else
        PegCursor.Get().SetMode(PegCursor.Mode.OVER);
      this.m_mouseDownTimer = -1f;
      this.m_lastClickPosition = Vector3.zero;
    }
    return flag;
  }

  private bool UpdateMouseLeftHold()
  {
    if (!UniversalInputManager.Get().GetMouseButton(0))
      return false;
    if (this.m_currentElement.GetReceiveOverWithMouseDown() && (UnityEngine.Object) this.m_currentElement != (UnityEngine.Object) this.m_prevElement)
    {
      if (this.m_currentElement.GetCursorOver() != PegCursor.Mode.NONE)
        PegCursor.Get().SetMode(this.m_currentElement.GetCursorOver());
      else
        PegCursor.Get().SetMode(PegCursor.Mode.OVER);
      this.m_currentElement.TriggerOver();
    }
    return true;
  }

  private bool UpdateMouseRightClick()
  {
    bool flag = false;
    if (UniversalInputManager.Get().GetMouseButtonDown(1))
    {
      flag = true;
      this.m_currentElement.TriggerRightClick();
    }
    return flag;
  }

  private void UpdateMouseOver()
  {
    if (UniversalInputManager.Get().IsTouchMode() && (!UniversalInputManager.Get().GetMouseButton(0) || !this.m_currentElement.GetReceiveOverWithMouseDown()) || (UnityEngine.Object) this.m_currentElement == (UnityEngine.Object) this.m_prevElement)
      return;
    if (this.m_currentElement.GetCursorOver() != PegCursor.Mode.NONE)
      PegCursor.Get().SetMode(this.m_currentElement.GetCursorOver());
    else
      PegCursor.Get().SetMode(PegCursor.Mode.OVER);
    this.m_currentElement.TriggerOver();
  }

  private void OnAppFocusChanged(bool focus, object userData)
  {
    this.m_hasFocus = focus;
  }

  private void OnMouseOnOrOffScreen(bool onScreen)
  {
    if (onScreen)
      return;
    this.m_lastClickPosition = Vector3.zero;
    if ((UnityEngine.Object) this.m_currentElement != (UnityEngine.Object) null)
    {
      this.m_currentElement.TriggerOut();
      this.m_currentElement = (PegUIElement) null;
    }
    PegCursor.Get().SetMode(PegCursor.Mode.UP);
    if ((UnityEngine.Object) this.m_prevElement != (UnityEngine.Object) null)
    {
      this.m_prevElement.TriggerOut();
      this.m_prevElement = (PegUIElement) null;
    }
    this.m_lastClickTimer = -1f;
  }

  public void EnableSwipeDetection(Rect swipeRect, PegUI.DelSwipeListener listener)
  {
    this.m_swipeListener = listener;
  }

  public void CancelSwipeDetection(PegUI.DelSwipeListener listener)
  {
    if (!((MulticastDelegate) listener == (MulticastDelegate) this.m_swipeListener))
      return;
    this.m_swipeListener = (PegUI.DelSwipeListener) null;
  }

  public void RegisterCustomBehavior(PegUICustomBehavior behavior)
  {
    this.m_customBehaviors.Add(behavior);
  }

  public void UnregisterCustomBehavior(PegUICustomBehavior behavior)
  {
    this.m_customBehaviors.Remove(behavior);
  }

  public enum Layer
  {
    MANUAL,
    RELATIVE_TO_PARENT,
    BACKGROUND,
    HUD,
    DIALOG,
  }

  public enum SWIPE_DIRECTION
  {
    RIGHT,
    LEFT,
  }

  public delegate void DelSwipeListener(PegUI.SWIPE_DIRECTION direction);
}
