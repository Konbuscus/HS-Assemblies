﻿// Decompiled with JetBrains decompiler
// Type: BnetBarFriendButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BnetBarFriendButton : FriendListUIElement
{
  public UberText m_OnlineCountText;
  public Color m_AnyOnlineColor;
  public Color m_AllOfflineColor;
  public GameObject m_PendingInvitesIcon;
  private static BnetBarFriendButton s_instance;

  protected override void Awake()
  {
    BnetBarFriendButton.s_instance = this;
    base.Awake();
    this.UpdateOnlineCount();
    BnetFriendMgr.Get().AddChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    this.ShowPendingInvitesIcon(false);
  }

  private void OnDestroy()
  {
    BnetFriendMgr.Get().RemoveChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    BnetBarFriendButton.s_instance = (BnetBarFriendButton) null;
  }

  public static BnetBarFriendButton Get()
  {
    return BnetBarFriendButton.s_instance;
  }

  public void HideTooltip()
  {
    TooltipZone component = this.GetComponent<TooltipZone>();
    if (!((Object) component != (Object) null))
      return;
    component.HideTooltip();
  }

  private void OnFriendsChanged(BnetFriendChangelist changelist, object userData)
  {
    this.UpdateOnlineCount();
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    this.UpdateOnlineCount();
  }

  private void UpdateOnlineCount()
  {
    int onlineFriendCount = BnetFriendMgr.Get().GetActiveOnlineFriendCount();
    this.m_OnlineCountText.TextColor = onlineFriendCount != 0 ? this.m_AnyOnlineColor : this.m_AllOfflineColor;
    this.m_OnlineCountText.Text = onlineFriendCount.ToString();
  }

  public void ShowPendingInvitesIcon(bool show)
  {
    if (!((Object) this.m_PendingInvitesIcon != (Object) null))
      return;
    this.m_PendingInvitesIcon.SetActive(show);
    this.m_OnlineCountText.gameObject.SetActive(!show);
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    SoundManager.Get().LoadAndPlay("Small_Mouseover");
    this.UpdateHighlight();
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    base.OnOut(oldState);
  }
}
