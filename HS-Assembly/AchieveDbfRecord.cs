﻿// Decompiled with JetBrains decompiler
// Type: AchieveDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AchieveDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private string m_AchType;
  [SerializeField]
  private bool m_Enabled;
  [SerializeField]
  private string m_ParentAch;
  [SerializeField]
  private string m_LinkTo;
  [SerializeField]
  private int m_SharedAchieveId;
  [SerializeField]
  private int m_ClientFlags;
  [SerializeField]
  private string m_Triggered;
  [SerializeField]
  private int m_AchQuota;
  [SerializeField]
  private string m_Event;
  [SerializeField]
  private string m_GameMode;
  [SerializeField]
  private int m_Race;
  [SerializeField]
  private int m_CardSet;
  [SerializeField]
  private int m_MaxDefense;
  [SerializeField]
  private string m_PlayerType;
  [SerializeField]
  private int m_ScenarioId;
  [SerializeField]
  private int m_AdventureWingId;
  [SerializeField]
  private int m_Booster;
  [SerializeField]
  private string m_RewardTiming;
  [SerializeField]
  private string m_Reward;
  [SerializeField]
  private long m_RewardData1;
  [SerializeField]
  private long m_RewardData2;
  [SerializeField]
  private int m_RewardableLimit;
  [SerializeField]
  private float m_RewardableInterval;
  [SerializeField]
  private string m_Unlocks;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private DbfLocValue m_Description;
  [SerializeField]
  private string m_AltTextPredicate;
  [SerializeField]
  private DbfLocValue m_AltName;
  [SerializeField]
  private DbfLocValue m_AltDescription;
  [SerializeField]
  private bool m_UseGenericRewardVisual;
  [SerializeField]
  private bool m_ShowToReturningPlayer;
  [SerializeField]
  private int m_QuestDialogId;
  [SerializeField]
  private bool m_AutoDestroy;
  [SerializeField]
  private string m_QuestTilePrefab;

  [DbfField("NOTE_DESC", "designer description of achievement")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("ACH_TYPE", "a comment field to show the type of quest for designers")]
  public string AchType
  {
    get
    {
      return this.m_AchType;
    }
  }

  [DbfField("ENABLED", "is awarding this achievement and making progress on it currently enabled?")]
  public bool Enabled
  {
    get
    {
      return this.m_Enabled;
    }
  }

  [DbfField("PARENT_ACH", "NOTE_DESC of the parent of this achieve, which means progressing in this achieve also progresses the parent achieve.")]
  public string ParentAch
  {
    get
    {
      return this.m_ParentAch;
    }
  }

  [DbfField("LINK_TO", "activate another ACHIEVE when this one is completed - value should be other achieve's NOTE_DESC; default is 'none'")]
  public string LinkTo
  {
    get
    {
      return this.m_LinkTo;
    }
  }

  [DbfField("SHARED_ACHIEVE_ID", "activate and progress another player's ACHIEVE when this one is completed - value should be other achieve's ID. 0 means no shared achieve")]
  public int SharedAchieveId
  {
    get
    {
      return this.m_SharedAchieveId;
    }
  }

  [DbfField("CLIENT_FLAGS", "additional properties bitfield. 0x1=is_legendary, 0x2=show_in_quest_log, 0x4=is_affected_by_friend_week, see enum PegasusShared.AchieveClientFlags")]
  public int ClientFlags
  {
    get
    {
      return this.m_ClientFlags;
    }
  }

  [DbfField("TRIGGERED", "trigger known to server, gets converted for text to an enum on the server; default is 'none' (see server s_achieveTriggerNames)")]
  public string Triggered
  {
    get
    {
      return this.m_Triggered;
    }
  }

  [DbfField("ACH_QUOTA", "how many times must whatever causes this to advance be done before it's completed?")]
  public int AchQuota
  {
    get
    {
      return this.m_AchQuota;
    }
  }

  [DbfField("EVENT", "event required to progress this achieve. default is 'none'")]
  public string Event
  {
    get
    {
      return this.m_Event;
    }
  }

  [DbfField("GAME_MODE", "game mode like 'play' or 'ai' known on server and converted to an enum there")]
  public string GameMode
  {
    get
    {
      return this.m_GameMode;
    }
  }

  [DbfField("RACE", "CARD.RACE required for the trigger condition")]
  public int Race
  {
    get
    {
      return this.m_Race;
    }
  }

  [DbfField("CARD_SET", "CARD.CARD_SET_ID required for the trigger condition")]
  public int CardSet
  {
    get
    {
      return this.m_CardSet;
    }
  }

  [DbfField("MAX_DEFENSE", "if players concede in a game with defense (health + armor) over this value they do not advance quest; 0 = this value is not used")]
  public int MaxDefense
  {
    get
    {
      return this.m_MaxDefense;
    }
  }

  [DbfField("PLAYER_TYPE", "the PegasusShared::PlayerType that player must be for this achievement to progress; default is 'any' (see server ParsePlayerType)")]
  public string PlayerType
  {
    get
    {
      return this.m_PlayerType;
    }
  }

  [DbfField("SCENARIO_ID", "SCENARIO.ID required for the trigger condition")]
  public int ScenarioId
  {
    get
    {
      return this.m_ScenarioId;
    }
  }

  [DbfField("ADVENTURE_WING_ID", "adventure wing required for trigger condition")]
  public int AdventureWingId
  {
    get
    {
      return this.m_AdventureWingId;
    }
  }

  [DbfField("BOOSTER", "BOOSTER.ID required for the trigger condition")]
  public int Booster
  {
    get
    {
      return this.m_Booster;
    }
  }

  [DbfField("REWARD_TIMING", "when should the client show the reward associated with this achievement? see client enum RewardVisualTiming; default is 'immediate'")]
  public string RewardTiming
  {
    get
    {
      return this.m_RewardTiming;
    }
  }

  [DbfField("REWARD", "reward name that we give for completing this achieve; can be 'none' for those achieves that reveal other achieves (see server sParseRewardType)")]
  public string Reward
  {
    get
    {
      return this.m_Reward;
    }
  }

  [DbfField("REWARD_DATA1", "reward parameter (used by server for the reward ID for this achieve)")]
  public long RewardData1
  {
    get
    {
      return this.m_RewardData1;
    }
  }

  [DbfField("REWARD_DATA2", "reward parameter (used by server for the reward ID for this achieve)")]
  public long RewardData2
  {
    get
    {
      return this.m_RewardData2;
    }
  }

  [DbfField("REWARDABLE_LIMIT", "the amount of times a player can get a reward from this achieve per rewardable_interval")]
  public int RewardableLimit
  {
    get
    {
      return this.m_RewardableLimit;
    }
  }

  [DbfField("REWARDABLE_INTERVAL", "the number of days that must pass for a player to get rewards again if at rewardable_limit")]
  public float RewardableInterval
  {
    get
    {
      return this.m_RewardableInterval;
    }
  }

  [DbfField("UNLOCKS", "game system/feature that is unlocked by this; default is 'none' (see server ParseAchieveUnlockableFeature)")]
  public string Unlocks
  {
    get
    {
      return this.m_Unlocks;
    }
  }

  [DbfField("NAME", "name of this achieve displayed to players")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("DESCRIPTION", "description of this achieve displayed to players")]
  public DbfLocValue Description
  {
    get
    {
      return this.m_Description;
    }
  }

  [DbfField("ALT_TEXT_PREDICATE", "how to determine whether or not use alternate text fields, see client enum Achievement.Predicate")]
  public string AltTextPredicate
  {
    get
    {
      return this.m_AltTextPredicate;
    }
  }

  [DbfField("ALT_NAME", "if not-null, this name is used when the condition specified by ALT_TEXT_PREDICATE evaluates to true.")]
  public DbfLocValue AltName
  {
    get
    {
      return this.m_AltName;
    }
  }

  [DbfField("ALT_DESCRIPTION", "if not-null, this description is used when the condition specified by ALT_TEXT_PREDICATE evaluates to true.")]
  public DbfLocValue AltDescription
  {
    get
    {
      return this.m_AltDescription;
    }
  }

  [DbfField("USE_GENERIC_REWARD_VISUAL", "hides the scroll portion of the achieve display effect.")]
  public bool UseGenericRewardVisual
  {
    get
    {
      return this.m_UseGenericRewardVisual;
    }
  }

  [DbfField("SHOW_TO_RETURNING_PLAYER", "should the reward for this be shown to a player who hasn't played in months? usually no, unless they could have done something out of Hearthstone to trigger this.")]
  public bool ShowToReturningPlayer
  {
    get
    {
      return this.m_ShowToReturningPlayer;
    }
  }

  [DbfField("QUEST_DIALOG_ID", "if this isn't null, it points to the id of the associated dialog lines in QUEST_DIALOG")]
  public int QuestDialogId
  {
    get
    {
      return this.m_QuestDialogId;
    }
  }

  [DbfField("AUTO_DESTROY", "if this is set to true, then this is a fake quest that we will automatically be destroyed by completing it.")]
  public bool AutoDestroy
  {
    get
    {
      return this.m_AutoDestroy;
    }
  }

  [DbfField("QUEST_TILE_PREFAB", "if specified, load this custom QuestTile prefab for this achieve. Used for the quest tiles that include the custom destroy effects for the auto destroy quests.")]
  public string QuestTilePrefab
  {
    get
    {
      return this.m_QuestTilePrefab;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    AchieveDbfAsset achieveDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (AchieveDbfAsset)) as AchieveDbfAsset;
    if ((UnityEngine.Object) achieveDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("AchieveDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < achieveDbfAsset.Records.Count; ++index)
      achieveDbfAsset.Records[index].StripUnusedLocales();
    records = (object) achieveDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Name.StripUnusedLocales();
    this.m_Description.StripUnusedLocales();
    this.m_AltName.StripUnusedLocales();
    this.m_AltDescription.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetAchType(string v)
  {
    this.m_AchType = v;
  }

  public void SetEnabled(bool v)
  {
    this.m_Enabled = v;
  }

  public void SetParentAch(string v)
  {
    this.m_ParentAch = v;
  }

  public void SetLinkTo(string v)
  {
    this.m_LinkTo = v;
  }

  public void SetSharedAchieveId(int v)
  {
    this.m_SharedAchieveId = v;
  }

  public void SetClientFlags(int v)
  {
    this.m_ClientFlags = v;
  }

  public void SetTriggered(string v)
  {
    this.m_Triggered = v;
  }

  public void SetAchQuota(int v)
  {
    this.m_AchQuota = v;
  }

  public void SetEvent(string v)
  {
    this.m_Event = v;
  }

  public void SetGameMode(string v)
  {
    this.m_GameMode = v;
  }

  public void SetRace(int v)
  {
    this.m_Race = v;
  }

  public void SetCardSet(int v)
  {
    this.m_CardSet = v;
  }

  public void SetMaxDefense(int v)
  {
    this.m_MaxDefense = v;
  }

  public void SetPlayerType(string v)
  {
    this.m_PlayerType = v;
  }

  public void SetScenarioId(int v)
  {
    this.m_ScenarioId = v;
  }

  public void SetAdventureWingId(int v)
  {
    this.m_AdventureWingId = v;
  }

  public void SetBooster(int v)
  {
    this.m_Booster = v;
  }

  public void SetRewardTiming(string v)
  {
    this.m_RewardTiming = v;
  }

  public void SetReward(string v)
  {
    this.m_Reward = v;
  }

  public void SetRewardData1(long v)
  {
    this.m_RewardData1 = v;
  }

  public void SetRewardData2(long v)
  {
    this.m_RewardData2 = v;
  }

  public void SetRewardableLimit(int v)
  {
    this.m_RewardableLimit = v;
  }

  public void SetRewardableInterval(float v)
  {
    this.m_RewardableInterval = v;
  }

  public void SetUnlocks(string v)
  {
    this.m_Unlocks = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetDescription(DbfLocValue v)
  {
    this.m_Description = v;
    v.SetDebugInfo(this.ID, "DESCRIPTION");
  }

  public void SetAltTextPredicate(string v)
  {
    this.m_AltTextPredicate = v;
  }

  public void SetAltName(DbfLocValue v)
  {
    this.m_AltName = v;
    v.SetDebugInfo(this.ID, "ALT_NAME");
  }

  public void SetAltDescription(DbfLocValue v)
  {
    this.m_AltDescription = v;
    v.SetDebugInfo(this.ID, "ALT_DESCRIPTION");
  }

  public void SetUseGenericRewardVisual(bool v)
  {
    this.m_UseGenericRewardVisual = v;
  }

  public void SetShowToReturningPlayer(bool v)
  {
    this.m_ShowToReturningPlayer = v;
  }

  public void SetQuestDialogId(int v)
  {
    this.m_QuestDialogId = v;
  }

  public void SetAutoDestroy(bool v)
  {
    this.m_AutoDestroy = v;
  }

  public void SetQuestTilePrefab(string v)
  {
    this.m_QuestTilePrefab = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AchieveDbfRecord.\u003C\u003Ef__switch\u0024map5 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AchieveDbfRecord.\u003C\u003Ef__switch\u0024map5 = new Dictionary<string, int>(36)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "ACH_TYPE",
            2
          },
          {
            "ENABLED",
            3
          },
          {
            "PARENT_ACH",
            4
          },
          {
            "LINK_TO",
            5
          },
          {
            "SHARED_ACHIEVE_ID",
            6
          },
          {
            "CLIENT_FLAGS",
            7
          },
          {
            "TRIGGERED",
            8
          },
          {
            "ACH_QUOTA",
            9
          },
          {
            "EVENT",
            10
          },
          {
            "GAME_MODE",
            11
          },
          {
            "RACE",
            12
          },
          {
            "CARD_SET",
            13
          },
          {
            "MAX_DEFENSE",
            14
          },
          {
            "PLAYER_TYPE",
            15
          },
          {
            "SCENARIO_ID",
            16
          },
          {
            "ADVENTURE_WING_ID",
            17
          },
          {
            "BOOSTER",
            18
          },
          {
            "REWARD_TIMING",
            19
          },
          {
            "REWARD",
            20
          },
          {
            "REWARD_DATA1",
            21
          },
          {
            "REWARD_DATA2",
            22
          },
          {
            "REWARDABLE_LIMIT",
            23
          },
          {
            "REWARDABLE_INTERVAL",
            24
          },
          {
            "UNLOCKS",
            25
          },
          {
            "NAME",
            26
          },
          {
            "DESCRIPTION",
            27
          },
          {
            "ALT_TEXT_PREDICATE",
            28
          },
          {
            "ALT_NAME",
            29
          },
          {
            "ALT_DESCRIPTION",
            30
          },
          {
            "USE_GENERIC_REWARD_VISUAL",
            31
          },
          {
            "SHOW_TO_RETURNING_PLAYER",
            32
          },
          {
            "QUEST_DIALOG_ID",
            33
          },
          {
            "AUTO_DESTROY",
            34
          },
          {
            "QUEST_TILE_PREFAB",
            35
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AchieveDbfRecord.\u003C\u003Ef__switch\u0024map5.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.AchType;
          case 3:
            return (object) this.Enabled;
          case 4:
            return (object) this.ParentAch;
          case 5:
            return (object) this.LinkTo;
          case 6:
            return (object) this.SharedAchieveId;
          case 7:
            return (object) this.ClientFlags;
          case 8:
            return (object) this.Triggered;
          case 9:
            return (object) this.AchQuota;
          case 10:
            return (object) this.Event;
          case 11:
            return (object) this.GameMode;
          case 12:
            return (object) this.Race;
          case 13:
            return (object) this.CardSet;
          case 14:
            return (object) this.MaxDefense;
          case 15:
            return (object) this.PlayerType;
          case 16:
            return (object) this.ScenarioId;
          case 17:
            return (object) this.AdventureWingId;
          case 18:
            return (object) this.Booster;
          case 19:
            return (object) this.RewardTiming;
          case 20:
            return (object) this.Reward;
          case 21:
            return (object) this.RewardData1;
          case 22:
            return (object) this.RewardData2;
          case 23:
            return (object) this.RewardableLimit;
          case 24:
            return (object) this.RewardableInterval;
          case 25:
            return (object) this.Unlocks;
          case 26:
            return (object) this.Name;
          case 27:
            return (object) this.Description;
          case 28:
            return (object) this.AltTextPredicate;
          case 29:
            return (object) this.AltName;
          case 30:
            return (object) this.AltDescription;
          case 31:
            return (object) this.UseGenericRewardVisual;
          case 32:
            return (object) this.ShowToReturningPlayer;
          case 33:
            return (object) this.QuestDialogId;
          case 34:
            return (object) this.AutoDestroy;
          case 35:
            return (object) this.QuestTilePrefab;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (AchieveDbfRecord.\u003C\u003Ef__switch\u0024map6 == null)
    {
      // ISSUE: reference to a compiler-generated field
      AchieveDbfRecord.\u003C\u003Ef__switch\u0024map6 = new Dictionary<string, int>(36)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "ACH_TYPE",
          2
        },
        {
          "ENABLED",
          3
        },
        {
          "PARENT_ACH",
          4
        },
        {
          "LINK_TO",
          5
        },
        {
          "SHARED_ACHIEVE_ID",
          6
        },
        {
          "CLIENT_FLAGS",
          7
        },
        {
          "TRIGGERED",
          8
        },
        {
          "ACH_QUOTA",
          9
        },
        {
          "EVENT",
          10
        },
        {
          "GAME_MODE",
          11
        },
        {
          "RACE",
          12
        },
        {
          "CARD_SET",
          13
        },
        {
          "MAX_DEFENSE",
          14
        },
        {
          "PLAYER_TYPE",
          15
        },
        {
          "SCENARIO_ID",
          16
        },
        {
          "ADVENTURE_WING_ID",
          17
        },
        {
          "BOOSTER",
          18
        },
        {
          "REWARD_TIMING",
          19
        },
        {
          "REWARD",
          20
        },
        {
          "REWARD_DATA1",
          21
        },
        {
          "REWARD_DATA2",
          22
        },
        {
          "REWARDABLE_LIMIT",
          23
        },
        {
          "REWARDABLE_INTERVAL",
          24
        },
        {
          "UNLOCKS",
          25
        },
        {
          "NAME",
          26
        },
        {
          "DESCRIPTION",
          27
        },
        {
          "ALT_TEXT_PREDICATE",
          28
        },
        {
          "ALT_NAME",
          29
        },
        {
          "ALT_DESCRIPTION",
          30
        },
        {
          "USE_GENERIC_REWARD_VISUAL",
          31
        },
        {
          "SHOW_TO_RETURNING_PLAYER",
          32
        },
        {
          "QUEST_DIALOG_ID",
          33
        },
        {
          "AUTO_DESTROY",
          34
        },
        {
          "QUEST_TILE_PREFAB",
          35
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!AchieveDbfRecord.\u003C\u003Ef__switch\u0024map6.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetAchType((string) val);
        break;
      case 3:
        this.SetEnabled((bool) val);
        break;
      case 4:
        this.SetParentAch((string) val);
        break;
      case 5:
        this.SetLinkTo((string) val);
        break;
      case 6:
        this.SetSharedAchieveId((int) val);
        break;
      case 7:
        this.SetClientFlags((int) val);
        break;
      case 8:
        this.SetTriggered((string) val);
        break;
      case 9:
        this.SetAchQuota((int) val);
        break;
      case 10:
        this.SetEvent((string) val);
        break;
      case 11:
        this.SetGameMode((string) val);
        break;
      case 12:
        this.SetRace((int) val);
        break;
      case 13:
        this.SetCardSet((int) val);
        break;
      case 14:
        this.SetMaxDefense((int) val);
        break;
      case 15:
        this.SetPlayerType((string) val);
        break;
      case 16:
        this.SetScenarioId((int) val);
        break;
      case 17:
        this.SetAdventureWingId((int) val);
        break;
      case 18:
        this.SetBooster((int) val);
        break;
      case 19:
        this.SetRewardTiming((string) val);
        break;
      case 20:
        this.SetReward((string) val);
        break;
      case 21:
        this.SetRewardData1((long) val);
        break;
      case 22:
        this.SetRewardData2((long) val);
        break;
      case 23:
        this.SetRewardableLimit((int) val);
        break;
      case 24:
        this.SetRewardableInterval((float) val);
        break;
      case 25:
        this.SetUnlocks((string) val);
        break;
      case 26:
        this.SetName((DbfLocValue) val);
        break;
      case 27:
        this.SetDescription((DbfLocValue) val);
        break;
      case 28:
        this.SetAltTextPredicate((string) val);
        break;
      case 29:
        this.SetAltName((DbfLocValue) val);
        break;
      case 30:
        this.SetAltDescription((DbfLocValue) val);
        break;
      case 31:
        this.SetUseGenericRewardVisual((bool) val);
        break;
      case 32:
        this.SetShowToReturningPlayer((bool) val);
        break;
      case 33:
        this.SetQuestDialogId((int) val);
        break;
      case 34:
        this.SetAutoDestroy((bool) val);
        break;
      case 35:
        this.SetQuestTilePrefab((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AchieveDbfRecord.\u003C\u003Ef__switch\u0024map7 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AchieveDbfRecord.\u003C\u003Ef__switch\u0024map7 = new Dictionary<string, int>(36)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "ACH_TYPE",
            2
          },
          {
            "ENABLED",
            3
          },
          {
            "PARENT_ACH",
            4
          },
          {
            "LINK_TO",
            5
          },
          {
            "SHARED_ACHIEVE_ID",
            6
          },
          {
            "CLIENT_FLAGS",
            7
          },
          {
            "TRIGGERED",
            8
          },
          {
            "ACH_QUOTA",
            9
          },
          {
            "EVENT",
            10
          },
          {
            "GAME_MODE",
            11
          },
          {
            "RACE",
            12
          },
          {
            "CARD_SET",
            13
          },
          {
            "MAX_DEFENSE",
            14
          },
          {
            "PLAYER_TYPE",
            15
          },
          {
            "SCENARIO_ID",
            16
          },
          {
            "ADVENTURE_WING_ID",
            17
          },
          {
            "BOOSTER",
            18
          },
          {
            "REWARD_TIMING",
            19
          },
          {
            "REWARD",
            20
          },
          {
            "REWARD_DATA1",
            21
          },
          {
            "REWARD_DATA2",
            22
          },
          {
            "REWARDABLE_LIMIT",
            23
          },
          {
            "REWARDABLE_INTERVAL",
            24
          },
          {
            "UNLOCKS",
            25
          },
          {
            "NAME",
            26
          },
          {
            "DESCRIPTION",
            27
          },
          {
            "ALT_TEXT_PREDICATE",
            28
          },
          {
            "ALT_NAME",
            29
          },
          {
            "ALT_DESCRIPTION",
            30
          },
          {
            "USE_GENERIC_REWARD_VISUAL",
            31
          },
          {
            "SHOW_TO_RETURNING_PLAYER",
            32
          },
          {
            "QUEST_DIALOG_ID",
            33
          },
          {
            "AUTO_DESTROY",
            34
          },
          {
            "QUEST_TILE_PREFAB",
            35
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AchieveDbfRecord.\u003C\u003Ef__switch\u0024map7.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (string);
          case 3:
            return typeof (bool);
          case 4:
            return typeof (string);
          case 5:
            return typeof (string);
          case 6:
            return typeof (int);
          case 7:
            return typeof (int);
          case 8:
            return typeof (string);
          case 9:
            return typeof (int);
          case 10:
            return typeof (string);
          case 11:
            return typeof (string);
          case 12:
            return typeof (int);
          case 13:
            return typeof (int);
          case 14:
            return typeof (int);
          case 15:
            return typeof (string);
          case 16:
            return typeof (int);
          case 17:
            return typeof (int);
          case 18:
            return typeof (int);
          case 19:
            return typeof (string);
          case 20:
            return typeof (string);
          case 21:
            return typeof (long);
          case 22:
            return typeof (long);
          case 23:
            return typeof (int);
          case 24:
            return typeof (float);
          case 25:
            return typeof (string);
          case 26:
            return typeof (DbfLocValue);
          case 27:
            return typeof (DbfLocValue);
          case 28:
            return typeof (string);
          case 29:
            return typeof (DbfLocValue);
          case 30:
            return typeof (DbfLocValue);
          case 31:
            return typeof (bool);
          case 32:
            return typeof (bool);
          case 33:
            return typeof (int);
          case 34:
            return typeof (bool);
          case 35:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
