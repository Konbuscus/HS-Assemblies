﻿// Decompiled with JetBrains decompiler
// Type: RadioButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RadioButton : PegUIElement
{
  public GameObject m_hoverGlow;
  public GameObject m_selectedGlow;
  private int m_id;
  private object m_userData;

  protected override void Awake()
  {
    base.Awake();
    this.m_hoverGlow.SetActive(false);
    this.m_selectedGlow.SetActive(false);
    SoundManager.Get().Load("tiny_button_press_2");
    SoundManager.Get().Load("tiny_button_mouseover_2");
  }

  public void SetButtonID(int id)
  {
    this.m_id = id;
  }

  public int GetButtonID()
  {
    return this.m_id;
  }

  public void SetUserData(object userData)
  {
    this.m_userData = userData;
  }

  public object GetUserData()
  {
    return this.m_userData;
  }

  public void SetSelected(bool selected)
  {
    this.m_selectedGlow.SetActive(selected);
  }

  public bool IsSelected()
  {
    return this.m_selectedGlow.activeSelf;
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    SoundManager.Get().LoadAndPlay("tiny_button_mouseover_2");
    this.m_hoverGlow.SetActive(true);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_hoverGlow.SetActive(false);
  }

  protected override void OnRelease()
  {
    base.OnRelease();
    SoundManager.Get().LoadAndPlay("tiny_button_press_2");
  }

  protected override void OnDoubleClick()
  {
  }
}
