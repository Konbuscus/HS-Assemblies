﻿// Decompiled with JetBrains decompiler
// Type: LOE02_Sun_Raider_Phaerix
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LOE02_Sun_Raider_Phaerix : LOE_MissionEntity
{
  private int m_staffLinesPlayed;
  private bool m_damageLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOE_01_RESPONSE");
    this.PreloadSound("VO_LOE_01_WOUNDED");
    this.PreloadSound("VO_LOE_01_STAFF");
    this.PreloadSound("VO_LOE_01_STAFF_2");
    this.PreloadSound("VO_LOE_02_PHAERIX_STAFF_RECOVER");
    this.PreloadSound("VO_LOE_01_STAFF_2_RENO");
    this.PreloadSound("VO_LOE_01_WIN_2");
    this.PreloadSound("VO_LOE_01_WIN_2_ALT_2");
    this.PreloadSound("VO_LOE_01_START");
    this.PreloadSound("VO_LOE_01_WIN");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_LOE_01_RESPONSE",
            m_stringTag = "VO_LOE_01_RESPONSE"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE02_Sun_Raider_Phaerix.\u003CHandleMissionEventWithTiming\u003Ec__Iterator160() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE02_Sun_Raider_Phaerix.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator161() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE02_Sun_Raider_Phaerix.\u003CHandleGameOverWithTiming\u003Ec__Iterator162() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
