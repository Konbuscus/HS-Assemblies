﻿// Decompiled with JetBrains decompiler
// Type: BreakingNews
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class BreakingNews : MonoBehaviour
{
  private string m_text = string.Empty;
  private const float TIMEOUT = 15f;
  public static bool SHOWS_BREAKING_NEWS;
  private static BreakingNews s_instance;
  private BreakingNews.Status m_status;
  private string m_error;
  private float m_timeFetched;

  public static BreakingNews Get()
  {
    return BreakingNews.s_instance;
  }

  public void Awake()
  {
    BreakingNews.SHOWS_BREAKING_NEWS = (bool) Network.TUTORIALS_WITHOUT_ACCOUNT;
    BreakingNews.s_instance = this;
  }

  public void OnDestroy()
  {
    BreakingNews.s_instance = (BreakingNews) null;
  }

  public static void FetchBreakingNews(string url, BreakingNews.BreakingNewsRecievedDelegate callback)
  {
    WWW request = new WWW(url);
    BreakingNews.s_instance.StartCoroutine(BreakingNews.s_instance.FetchBreakingNewsProgress(request, callback));
  }

  [DebuggerHidden]
  public IEnumerator FetchBreakingNewsProgress(WWW request, BreakingNews.BreakingNewsRecievedDelegate callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BreakingNews.\u003CFetchBreakingNewsProgress\u003Ec__Iterator2E4() { request = request, callback = callback, \u003C\u0024\u003Erequest = request, \u003C\u0024\u003Ecallback = callback };
  }

  public BreakingNews.Status GetStatus()
  {
    if (!BreakingNews.SHOWS_BREAKING_NEWS)
      return BreakingNews.Status.Available;
    if (this.m_status == BreakingNews.Status.Fetching && (double) Time.realtimeSinceStartup - (double) this.m_timeFetched > 15.0)
      this.m_status = BreakingNews.Status.TimedOut;
    return this.m_status;
  }

  public void Fetch()
  {
    if (!BreakingNews.SHOWS_BREAKING_NEWS)
      return;
    this.m_error = (string) null;
    this.m_status = BreakingNews.Status.Fetching;
    this.m_text = string.Empty;
    this.m_timeFetched = Time.realtimeSinceStartup;
    BreakingNews.FetchBreakingNews(NydusLink.GetBreakingNewsLink(), (BreakingNews.BreakingNewsRecievedDelegate) ((response, error) =>
    {
      if (error)
        BreakingNews.s_instance.OnBreakingNewsError(response);
      else
        BreakingNews.s_instance.OnBreakingNewsResponse(response);
    }));
  }

  public void OnBreakingNewsResponse(string response)
  {
    Log.JMac.Print("Breaking News response received: {0}", (object) response);
    this.m_text = response;
    if (this.m_text.Length <= 2 || this.m_text.ToLowerInvariant().Contains("<html>"))
      this.m_text = string.Empty;
    this.m_status = BreakingNews.Status.Available;
  }

  public void OnBreakingNewsError(string error)
  {
    this.m_error = error;
    Log.JMac.Print("Breaking News error received: {0}", (object) error);
  }

  public string GetText()
  {
    if (!BreakingNews.SHOWS_BREAKING_NEWS)
      return string.Empty;
    if (this.m_status != BreakingNews.Status.Fetching && this.m_status != BreakingNews.Status.TimedOut)
      return this.m_text;
    UnityEngine.Debug.LogError((object) string.Format("Fetched breaking news when it was unavailable, status={0}", (object) this.m_status));
    return string.Empty;
  }

  public string GetError()
  {
    return this.m_error;
  }

  public enum Status
  {
    Fetching,
    Available,
    TimedOut,
  }

  public delegate void BreakingNewsRecievedDelegate(string response, bool error);
}
