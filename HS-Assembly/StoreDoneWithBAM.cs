﻿// Decompiled with JetBrains decompiler
// Type: StoreDoneWithBAM
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class StoreDoneWithBAM : UIBPopup
{
  private List<StoreDoneWithBAM.ButtonPressedListener> m_okayListeners = new List<StoreDoneWithBAM.ButtonPressedListener>();
  public UIBButton m_okayButton;
  public UberText m_headlineText;
  public UberText m_messageText;

  private void Awake()
  {
    this.m_okayButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnOkayPressed));
  }

  public void RegisterOkayListener(StoreDoneWithBAM.ButtonPressedListener listener)
  {
    if (this.m_okayListeners.Contains(listener))
      return;
    this.m_okayListeners.Add(listener);
  }

  public void RemoveOkayListener(StoreDoneWithBAM.ButtonPressedListener listener)
  {
    this.m_okayListeners.Remove(listener);
  }

  private void OnOkayPressed(UIEvent e)
  {
    this.Hide(true);
    foreach (StoreDoneWithBAM.ButtonPressedListener buttonPressedListener in this.m_okayListeners.ToArray())
      buttonPressedListener();
  }

  public delegate void ButtonPressedListener();
}
