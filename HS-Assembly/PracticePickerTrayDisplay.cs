﻿// Decompiled with JetBrains decompiler
// Type: PracticePickerTrayDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class PracticePickerTrayDisplay : MonoBehaviour
{
  private static readonly int NUM_AI_BUTTONS_TO_SHOW = 14;
  [SerializeField]
  private float m_AIButtonHeight = 5f;
  [CustomEditField(Sections = "Animation Settings")]
  public float m_trayAnimationTime = 0.5f;
  [CustomEditField(Sections = "Animation Settings")]
  public iTween.EaseType m_trayInEaseType = iTween.EaseType.easeOutBounce;
  [CustomEditField(Sections = "Animation Settings")]
  public iTween.EaseType m_trayOutEaseType = iTween.EaseType.easeOutCubic;
  private List<ScenarioDbfRecord> m_sortedMissionRecords = new List<ScenarioDbfRecord>();
  private List<PracticeAIButton> m_practiceAIButtons = new List<PracticeAIButton>();
  private Map<string, FullDef> m_heroDefs = new Map<string, FullDef>();
  private List<PracticePickerTrayDisplay.TrayLoaded> m_TrayLoadedListeners = new List<PracticePickerTrayDisplay.TrayLoaded>();
  private const float PRACTICE_TRAY_MATERIAL_Y_OFFSET = -0.045f;
  [CustomEditField(Sections = "UI")]
  public UberText m_trayLabel;
  [CustomEditField(Sections = "UI")]
  public StandardPegButtonNew m_backButton;
  [CustomEditField(Sections = "UI")]
  public PlayButton m_playButton;
  [CustomEditField(Sections = "AI Button Settings")]
  public PracticeAIButton m_AIButtonPrefab;
  [CustomEditField(Sections = "AI Button Settings")]
  public GameObject m_AIButtonsContainer;
  private static PracticePickerTrayDisplay s_instance;
  private List<Achievement> m_lockedHeroes;
  private PracticeAIButton m_selectedPracticeAIButton;
  private int m_heroDefsToLoad;
  private bool m_buttonsCreated;
  private bool m_buttonsReady;
  private bool m_heroesLoaded;
  private bool m_shown;

  [CustomEditField(Sections = "AI Button Settings")]
  public float AIButtonHeight
  {
    get
    {
      return this.m_AIButtonHeight;
    }
    set
    {
      this.m_AIButtonHeight = value;
      this.UpdateAIButtonPositions();
    }
  }

  private void Awake()
  {
    PracticePickerTrayDisplay.s_instance = this;
    this.InitMissionRecords();
    foreach (Component component in this.gameObject.GetComponents<Transform>())
      component.gameObject.SetActive(false);
    this.gameObject.SetActive(true);
    if ((UnityEngine.Object) this.m_backButton != (UnityEngine.Object) null)
    {
      this.m_backButton.SetText(GameStrings.Get("GLOBAL_BACK"));
      this.m_backButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.BackButtonReleased));
    }
    this.m_trayLabel.Text = GameStrings.Get("GLUE_CHOOSE_OPPONENT");
    this.m_playButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.PlayGameButtonRelease));
    this.m_heroDefsToLoad = this.m_sortedMissionRecords.Count;
    using (List<ScenarioDbfRecord>.Enumerator enumerator = this.m_sortedMissionRecords.GetEnumerator())
    {
      while (enumerator.MoveNext())
        DefLoader.Get().LoadFullDef(GameUtils.GetMissionHeroCardId(enumerator.Current.ID), new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
    }
    SoundManager.Get().Load("choose_opponent_panel_slide_on");
    SoundManager.Get().Load("choose_opponent_panel_slide_off");
    this.SetupHeroAchieves();
    this.StartCoroutine(this.NotifyWhenTrayLoaded());
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
  }

  private void OnDestroy()
  {
    GameMgr.Get().UnregisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    PracticePickerTrayDisplay.s_instance = (PracticePickerTrayDisplay) null;
  }

  private void Start()
  {
    this.m_playButton.SetText(GameStrings.Get("GLOBAL_PLAY"));
    this.m_playButton.SetOriginalLocalPosition();
    this.m_playButton.Disable();
  }

  public static PracticePickerTrayDisplay Get()
  {
    return PracticePickerTrayDisplay.s_instance;
  }

  public void Init()
  {
    int num = Mathf.Min(PracticePickerTrayDisplay.NUM_AI_BUTTONS_TO_SHOW, this.m_sortedMissionRecords.Count);
    for (int index = 0; index < num; ++index)
    {
      PracticeAIButton practiceAiButton = (PracticeAIButton) GameUtils.Instantiate((Component) this.m_AIButtonPrefab, this.m_AIButtonsContainer, false);
      SceneUtils.SetLayer((Component) practiceAiButton, this.m_AIButtonsContainer.gameObject.layer);
      this.m_practiceAIButtons.Add(practiceAiButton);
    }
    this.UpdateAIButtonPositions();
    using (List<PracticeAIButton>.Enumerator enumerator = this.m_practiceAIButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PracticeAIButton current = enumerator.Current;
        current.SetOriginalLocalPosition();
        current.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.AIButtonPressed));
      }
    }
    this.m_buttonsCreated = true;
  }

  public void Show()
  {
    this.m_shown = true;
    iTween.Stop(this.gameObject);
    foreach (Component component in this.gameObject.GetComponents<Transform>())
      component.gameObject.SetActive(true);
    this.gameObject.SetActive(true);
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) PracticeDisplay.Get().GetPracticePickerShowPosition(), (object) "isLocal", (object) true, (object) "time", (object) this.m_trayAnimationTime, (object) "easetype", (object) this.m_trayInEaseType, (object) "delay", (object) (1f / 1000f)));
    SoundManager.Get().LoadAndPlay("choose_opponent_panel_slide_on");
    if (!Options.Get().GetBool(Option.HAS_SEEN_PRACTICE_TRAY, false) && UserAttentionManager.CanShowAttentionGrabber("PracticePickerTrayDisplay.Show:" + (object) Option.HAS_SEEN_PRACTICE_TRAY))
    {
      Options.Get().SetBool(Option.HAS_SEEN_PRACTICE_TRAY, true);
      this.StartCoroutine(this.DoPickHeroLines());
    }
    if ((UnityEngine.Object) this.m_selectedPracticeAIButton != (UnityEngine.Object) null)
      this.m_playButton.Enable();
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  [DebuggerHidden]
  private IEnumerator DoPickHeroLines()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PracticePickerTrayDisplay.\u003CDoPickHeroLines\u003Ec__Iterator233() { \u003C\u003Ef__this = this };
  }

  public void Hide()
  {
    this.m_shown = false;
    iTween.Stop(this.gameObject);
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) PracticeDisplay.Get().GetPracticePickerHidePosition(), (object) "isLocal", (object) true, (object) "time", (object) this.m_trayAnimationTime, (object) "easetype", (object) this.m_trayOutEaseType, (object) "oncomplete", (object) (Action<object>) (e => this.gameObject.SetActive(false)), (object) "delay", (object) (1f / 1000f)));
    SoundManager.Get().LoadAndPlay("choose_opponent_panel_slide_off");
  }

  public void OnGameDenied()
  {
    this.UpdateAIButtons();
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  public void AddTrayLoadedListener(PracticePickerTrayDisplay.TrayLoaded dlg)
  {
    this.m_TrayLoadedListeners.Add(dlg);
  }

  public void RemoveTrayLoadedListener(PracticePickerTrayDisplay.TrayLoaded dlg)
  {
    this.m_TrayLoadedListeners.Remove(dlg);
  }

  public bool IsLoaded()
  {
    return this.m_buttonsReady;
  }

  private void InitMissionRecords()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    PracticePickerTrayDisplay.\u003CInitMissionRecords\u003Ec__AnonStorey3CE recordsCAnonStorey3Ce = new PracticePickerTrayDisplay.\u003CInitMissionRecords\u003Ec__AnonStorey3CE();
    // ISSUE: reference to a compiler-generated field
    recordsCAnonStorey3Ce.practiceDbId = 2;
    AdventureModeDbId selectedMode = AdventureConfig.Get().GetSelectedMode();
    // ISSUE: reference to a compiler-generated field
    recordsCAnonStorey3Ce.modeDbId = (int) selectedMode;
    // ISSUE: reference to a compiler-generated method
    this.m_sortedMissionRecords = GameDbf.Scenario.GetRecords(new Predicate<ScenarioDbfRecord>(recordsCAnonStorey3Ce.\u003C\u003Em__17C));
    this.m_sortedMissionRecords.Sort(new Comparison<ScenarioDbfRecord>(GameUtils.MissionSortComparison));
  }

  private void SetupHeroAchieves()
  {
    this.m_lockedHeroes = AchieveManager.Get().GetAchievesInGroup(Achievement.AchType.UNLOCK_HERO, false);
    if (this.m_lockedHeroes.Count <= 7 && !Options.Get().GetBool(Option.HAS_SEEN_PRACTICE_MODE, false))
      Options.Get().SetBool(Option.HAS_SEEN_PRACTICE_MODE, true);
    this.StartCoroutine(this.InitButtonsWhenReady());
  }

  [DebuggerHidden]
  private IEnumerator InitButtonsWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PracticePickerTrayDisplay.\u003CInitButtonsWhenReady\u003Ec__Iterator234() { \u003C\u003Ef__this = this };
  }

  private void OnFullDefLoaded(string cardId, FullDef def, object userData)
  {
    this.m_heroDefs[cardId] = def;
    --this.m_heroDefsToLoad;
    if (this.m_heroDefsToLoad > 0)
      return;
    this.m_heroesLoaded = true;
  }

  private void SetSelectedButton(PracticeAIButton button)
  {
    if ((UnityEngine.Object) this.m_selectedPracticeAIButton != (UnityEngine.Object) null)
      this.m_selectedPracticeAIButton.Deselect();
    this.m_selectedPracticeAIButton = button;
  }

  private void DisableAIButtons()
  {
    for (int index = 0; index < this.m_practiceAIButtons.Count; ++index)
      this.m_practiceAIButtons[index].SetEnabled(false);
  }

  private void EnableAIButtons()
  {
    for (int index = 0; index < this.m_practiceAIButtons.Count; ++index)
      this.m_practiceAIButtons[index].SetEnabled(true);
  }

  private bool OnNavigateBack()
  {
    this.Hide();
    DeckPickerTrayDisplay.Get().ResetCurrentMode();
    return true;
  }

  private void BackButtonReleased(UIEvent e)
  {
    Navigation.GoBack();
  }

  private void PlayGameButtonRelease(UIEvent e)
  {
    SceneUtils.SetLayer(PracticeDisplay.Get().gameObject, GameLayer.Default);
    long selectedDeckId = DeckPickerTrayDisplay.Get().GetSelectedDeckID();
    if (selectedDeckId == 0L)
    {
      UnityEngine.Debug.LogError((object) "Trying to play practice game with deck ID 0!");
    }
    else
    {
      e.GetElement().SetEnabled(false);
      this.DisableAIButtons();
      if (AdventureConfig.Get().GetSelectedMode() == AdventureModeDbId.EXPERT && !Options.Get().GetBool(Option.HAS_PLAYED_EXPERT_AI, false))
        Options.Get().SetBool(Option.HAS_PLAYED_EXPERT_AI, true);
      GameMgr.Get().FindGame(GameType.GT_VS_AI, FormatType.FT_WILD, this.m_selectedPracticeAIButton.GetMissionID(), selectedDeckId, 0L);
    }
  }

  private void AIButtonPressed(UIEvent e)
  {
    PracticeAIButton element = (PracticeAIButton) e.GetElement();
    this.SetSelectedButton(element);
    this.m_playButton.Enable();
    element.Select();
  }

  private void UpdateAIButtons()
  {
    this.UpdateAIDeckButtons();
    if ((UnityEngine.Object) this.m_selectedPracticeAIButton == (UnityEngine.Object) null)
      this.m_playButton.Disable();
    else
      this.m_playButton.Enable();
  }

  private void UpdateAIButtonPositions()
  {
    int num = 0;
    using (List<PracticeAIButton>.Enumerator enumerator = this.m_practiceAIButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        TransformUtil.SetLocalPosZ((Component) enumerator.Current, -this.m_AIButtonHeight * (float) num++);
    }
  }

  private void UpdateAIDeckButtons()
  {
    for (int index = 0; index < this.m_sortedMissionRecords.Count; ++index)
    {
      ScenarioDbfRecord sortedMissionRecord = this.m_sortedMissionRecords[index];
      int id = sortedMissionRecord.ID;
      FullDef heroDef = this.m_heroDefs[GameUtils.GetMissionHeroCardId(id)];
      EntityDef entityDef = heroDef.GetEntityDef();
      CardDef cardDef = heroDef.GetCardDef();
      TAG_CLASS buttonClass = entityDef.GetClass();
      string shortName = (string) sortedMissionRecord.ShortName;
      PracticeAIButton practiceAiButton = this.m_practiceAIButtons[index];
      practiceAiButton.SetInfo(shortName, buttonClass, cardDef, id, false);
      bool shown = false;
      using (List<Achievement>.Enumerator enumerator = this.m_lockedHeroes.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.ClassRequirement.Value == buttonClass)
          {
            shown = true;
            break;
          }
        }
      }
      practiceAiButton.ShowQuestBang(shown);
      if ((UnityEngine.Object) practiceAiButton == (UnityEngine.Object) this.m_selectedPracticeAIButton)
        practiceAiButton.Select();
      else
        practiceAiButton.Deselect();
    }
    if (AdventureConfig.Get().GetSelectedMode() != AdventureModeDbId.EXPERT || Options.Get().GetBool(Option.HAS_SEEN_EXPERT_AI, false))
      return;
    Options.Get().SetBool(Option.HAS_SEEN_EXPERT_AI, true);
  }

  [DebuggerHidden]
  private IEnumerator NotifyWhenTrayLoaded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PracticePickerTrayDisplay.\u003CNotifyWhenTrayLoaded\u003Ec__Iterator235() { \u003C\u003Ef__this = this };
  }

  private void FireTrayLoadedEvent()
  {
    foreach (PracticePickerTrayDisplay.TrayLoaded trayLoaded in this.m_TrayLoadedListeners.ToArray())
      trayLoaded();
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    if (eventData.m_state == FindGameState.INVALID)
      this.EnableAIButtons();
    return false;
  }

  public delegate void TrayLoaded();
}
