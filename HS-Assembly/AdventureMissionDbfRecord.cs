﻿// Decompiled with JetBrains decompiler
// Type: AdventureMissionDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AdventureMissionDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_ScenarioId;
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private int m_ReqWingId;
  [SerializeField]
  private int m_ReqProgress;
  [SerializeField]
  private ulong m_ReqFlags;
  [SerializeField]
  private int m_GrantsWingId;
  [SerializeField]
  private int m_GrantsProgress;
  [SerializeField]
  private ulong m_GrantsFlags;
  [SerializeField]
  private string m_BossDefAssetPath;
  [SerializeField]
  private string m_ClassChallengePrefabPopup;

  [DbfField("SCENARIO_ID", "primary key - SCENARIO.ID that this mission corresponds to")]
  public int ScenarioId
  {
    get
    {
      return this.m_ScenarioId;
    }
  }

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("REQ_WING_ID", "adventure WING.ID requirement to play this mission. 0 is ignored.")]
  public int ReqWingId
  {
    get
    {
      return this.m_ReqWingId;
    }
  }

  [DbfField("REQ_PROGRESS", "player must have this wing_progress on the REQ_WING_ID in order to play this mission. 0 is ignored.")]
  public int ReqProgress
  {
    get
    {
      return this.m_ReqProgress;
    }
  }

  [DbfField("REQ_FLAGS", "player must have this wing_flags on the REQ_WING_ID in order to play this mission. 0 is ignored.")]
  public ulong ReqFlags
  {
    get
    {
      return this.m_ReqFlags;
    }
  }

  [DbfField("GRANTS_WING_ID", "adventure WING.ID granted by defeating this mission. 0 is ignored.")]
  public int GrantsWingId
  {
    get
    {
      return this.m_GrantsWingId;
    }
  }

  [DbfField("GRANTS_PROGRESS", "wing_progress granted for GRANTS_WING_ID by defeating this mission. 0 is ignored.")]
  public int GrantsProgress
  {
    get
    {
      return this.m_GrantsProgress;
    }
  }

  [DbfField("GRANTS_FLAGS", "wing_flags granted for GRANTS_WING_ID  by defeating this mission. 0 is ignored.")]
  public ulong GrantsFlags
  {
    get
    {
      return this.m_GrantsFlags;
    }
  }

  [DbfField("BOSS_DEF_ASSET_PATH", "")]
  public string BossDefAssetPath
  {
    get
    {
      return this.m_BossDefAssetPath;
    }
  }

  [DbfField("CLASS_CHALLENGE_PREFAB_POPUP", "")]
  public string ClassChallengePrefabPopup
  {
    get
    {
      return this.m_ClassChallengePrefabPopup;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    AdventureMissionDbfAsset adventureMissionDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (AdventureMissionDbfAsset)) as AdventureMissionDbfAsset;
    if ((UnityEngine.Object) adventureMissionDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("AdventureMissionDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < adventureMissionDbfAsset.Records.Count; ++index)
      adventureMissionDbfAsset.Records[index].StripUnusedLocales();
    records = (object) adventureMissionDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetScenarioId(int v)
  {
    this.m_ScenarioId = v;
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetReqWingId(int v)
  {
    this.m_ReqWingId = v;
  }

  public void SetReqProgress(int v)
  {
    this.m_ReqProgress = v;
  }

  public void SetReqFlags(ulong v)
  {
    this.m_ReqFlags = v;
  }

  public void SetGrantsWingId(int v)
  {
    this.m_GrantsWingId = v;
  }

  public void SetGrantsProgress(int v)
  {
    this.m_GrantsProgress = v;
  }

  public void SetGrantsFlags(ulong v)
  {
    this.m_GrantsFlags = v;
  }

  public void SetBossDefAssetPath(string v)
  {
    this.m_BossDefAssetPath = v;
  }

  public void SetClassChallengePrefabPopup(string v)
  {
    this.m_ClassChallengePrefabPopup = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureMissionDbfRecord.\u003C\u003Ef__switch\u0024mapE == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureMissionDbfRecord.\u003C\u003Ef__switch\u0024mapE = new Dictionary<string, int>(11)
        {
          {
            "ID",
            0
          },
          {
            "SCENARIO_ID",
            1
          },
          {
            "NOTE_DESC",
            2
          },
          {
            "REQ_WING_ID",
            3
          },
          {
            "REQ_PROGRESS",
            4
          },
          {
            "REQ_FLAGS",
            5
          },
          {
            "GRANTS_WING_ID",
            6
          },
          {
            "GRANTS_PROGRESS",
            7
          },
          {
            "GRANTS_FLAGS",
            8
          },
          {
            "BOSS_DEF_ASSET_PATH",
            9
          },
          {
            "CLASS_CHALLENGE_PREFAB_POPUP",
            10
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureMissionDbfRecord.\u003C\u003Ef__switch\u0024mapE.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.ScenarioId;
          case 2:
            return (object) this.NoteDesc;
          case 3:
            return (object) this.ReqWingId;
          case 4:
            return (object) this.ReqProgress;
          case 5:
            return (object) this.ReqFlags;
          case 6:
            return (object) this.GrantsWingId;
          case 7:
            return (object) this.GrantsProgress;
          case 8:
            return (object) this.GrantsFlags;
          case 9:
            return (object) this.BossDefAssetPath;
          case 10:
            return (object) this.ClassChallengePrefabPopup;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (AdventureMissionDbfRecord.\u003C\u003Ef__switch\u0024mapF == null)
    {
      // ISSUE: reference to a compiler-generated field
      AdventureMissionDbfRecord.\u003C\u003Ef__switch\u0024mapF = new Dictionary<string, int>(11)
      {
        {
          "ID",
          0
        },
        {
          "SCENARIO_ID",
          1
        },
        {
          "NOTE_DESC",
          2
        },
        {
          "REQ_WING_ID",
          3
        },
        {
          "REQ_PROGRESS",
          4
        },
        {
          "REQ_FLAGS",
          5
        },
        {
          "GRANTS_WING_ID",
          6
        },
        {
          "GRANTS_PROGRESS",
          7
        },
        {
          "GRANTS_FLAGS",
          8
        },
        {
          "BOSS_DEF_ASSET_PATH",
          9
        },
        {
          "CLASS_CHALLENGE_PREFAB_POPUP",
          10
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!AdventureMissionDbfRecord.\u003C\u003Ef__switch\u0024mapF.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetScenarioId((int) val);
        break;
      case 2:
        this.SetNoteDesc((string) val);
        break;
      case 3:
        this.SetReqWingId((int) val);
        break;
      case 4:
        this.SetReqProgress((int) val);
        break;
      case 5:
        this.SetReqFlags((ulong) val);
        break;
      case 6:
        this.SetGrantsWingId((int) val);
        break;
      case 7:
        this.SetGrantsProgress((int) val);
        break;
      case 8:
        this.SetGrantsFlags((ulong) val);
        break;
      case 9:
        this.SetBossDefAssetPath((string) val);
        break;
      case 10:
        this.SetClassChallengePrefabPopup((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureMissionDbfRecord.\u003C\u003Ef__switch\u0024map10 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureMissionDbfRecord.\u003C\u003Ef__switch\u0024map10 = new Dictionary<string, int>(11)
        {
          {
            "ID",
            0
          },
          {
            "SCENARIO_ID",
            1
          },
          {
            "NOTE_DESC",
            2
          },
          {
            "REQ_WING_ID",
            3
          },
          {
            "REQ_PROGRESS",
            4
          },
          {
            "REQ_FLAGS",
            5
          },
          {
            "GRANTS_WING_ID",
            6
          },
          {
            "GRANTS_PROGRESS",
            7
          },
          {
            "GRANTS_FLAGS",
            8
          },
          {
            "BOSS_DEF_ASSET_PATH",
            9
          },
          {
            "CLASS_CHALLENGE_PREFAB_POPUP",
            10
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureMissionDbfRecord.\u003C\u003Ef__switch\u0024map10.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (string);
          case 3:
            return typeof (int);
          case 4:
            return typeof (int);
          case 5:
            return typeof (ulong);
          case 6:
            return typeof (int);
          case 7:
            return typeof (int);
          case 8:
            return typeof (ulong);
          case 9:
            return typeof (string);
          case 10:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
