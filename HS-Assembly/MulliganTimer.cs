﻿// Decompiled with JetBrains decompiler
// Type: MulliganTimer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MulliganTimer : MonoBehaviour
{
  public UberText m_timeText;
  private bool m_remainingTimeSet;
  private float m_endTimeStamp;

  private void Start()
  {
    if ((Object) MulliganManager.Get() == (Object) null)
      return;
    Vector3 position = MulliganManager.Get().GetMulliganButton().transform.position;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.transform.position = new Vector3(position.x + 1.8f, position.y, position.z);
    else
      this.transform.position = new Vector3(position.x, position.y, position.z - 1f);
  }

  private void Update()
  {
    if (!this.m_remainingTimeSet)
      return;
    float countdownRemainingSec = this.ComputeCountdownRemainingSec();
    int num = Mathf.RoundToInt(countdownRemainingSec);
    if (num < 0)
      num = 0;
    this.m_timeText.Text = string.Format(":{0:D2}", (object) num);
    if ((double) countdownRemainingSec > 0.0)
      return;
    if ((bool) ((Object) MulliganManager.Get()))
      MulliganManager.Get().AutomaticContinueMulligan();
    else
      this.SelfDestruct();
  }

  private float ComputeCountdownRemainingSec()
  {
    float num = this.m_endTimeStamp - Time.realtimeSinceStartup;
    if ((double) num < 0.0)
      return 0.0f;
    return num;
  }

  public void SetEndTime(float endTimeStamp)
  {
    this.m_endTimeStamp = endTimeStamp;
    this.m_remainingTimeSet = true;
  }

  public void SelfDestruct()
  {
    Object.Destroy((Object) this.gameObject);
  }
}
