﻿// Decompiled with JetBrains decompiler
// Type: CollectionDeckTileActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class CollectionDeckTileActor : Actor
{
  [CustomEditField(Sections = "Ghosting Effect")]
  public CollectionDeckTileActor.DeckTileFrameColorSet m_normalColorSet = new CollectionDeckTileActor.DeckTileFrameColorSet();
  [CustomEditField(Sections = "Ghosting Effect")]
  public CollectionDeckTileActor.DeckTileFrameColorSet m_ghostedColorSet = new CollectionDeckTileActor.DeckTileFrameColorSet();
  [CustomEditField(Sections = "Ghosting Effect")]
  public CollectionDeckTileActor.DeckTileFrameColorSet m_redColorSet = new CollectionDeckTileActor.DeckTileFrameColorSet();
  private const float SLIDER_ANIM_TIME = 0.35f;
  public Material m_standardFrameMaterial;
  public Material m_premiumFrameMaterial;
  public Material m_standardFrameInteriorMaterial;
  public GameObject m_frame;
  public GameObject m_frameInterior;
  public GameObject m_uniqueStar;
  public GameObject m_highlight;
  public GameObject m_highlightGlow;
  public UberText m_countText;
  [CustomEditField(Sections = "Ghosting Effect")]
  public Material m_ghostedFrameMaterial;
  [CustomEditField(Sections = "Ghosting Effect")]
  public Material m_redFrameMaterial;
  [CustomEditField(Sections = "Ghosting Effect")]
  public MeshRenderer m_manaGem;
  [CustomEditField(Sections = "Ghosting Effect")]
  public MeshRenderer m_slider;
  [CustomEditField(Sections = "Ghosting Effect")]
  public Material m_manaGemNormalMaterial;
  [CustomEditField(Sections = "Ghosting Effect")]
  public Material m_manaGemGhostedMaterial;
  [CustomEditField(Sections = "Ghosting Effect")]
  public Material m_manaGemRedGhostedMaterial;
  [CustomEditField(Sections = "Ghosting Effect")]
  public Material m_redFrameInteriorMaterial;
  private UberText m_countTextMesh;
  private bool m_sliderIsOpen;
  private Vector3 m_originalSliderLocalPos;
  private Vector3 m_openSliderLocalPos;
  private CollectionDeckTileActor.GhostedState m_ghosted;

  public override void Awake()
  {
    base.Awake();
    this.AssignSlider();
    this.AssignCardCount();
  }

  public void UpdateDeckCardProperties(bool cardIsUnique, int numCards, bool useSliderAnimations)
  {
    if (cardIsUnique)
    {
      this.m_uniqueStar.SetActive(this.m_shown);
      this.m_countTextMesh.gameObject.SetActive(false);
    }
    else
    {
      this.m_uniqueStar.SetActive(false);
      this.m_countTextMesh.gameObject.SetActive(this.m_shown);
      this.m_countTextMesh.Text = Convert.ToString(numCards);
    }
    if (cardIsUnique || numCards > 1)
      this.OpenSlider(useSliderAnimations);
    else
      this.CloseSlider(useSliderAnimations);
  }

  public void UpdateMaterial(Material material)
  {
    if (!((UnityEngine.Object) material != (UnityEngine.Object) null))
      return;
    this.m_portraitMesh.GetComponent<MeshRenderer>().material = material;
  }

  public void SetGhosted(CollectionDeckTileActor.GhostedState state)
  {
    this.m_ghosted = state;
  }

  public override void SetPremium(TAG_PREMIUM premium)
  {
    base.SetPremium(premium);
    this.UpdateFrameMaterial();
  }

  public void UpdateGhostTileEffect()
  {
    if ((UnityEngine.Object) this.m_manaGem == (UnityEngine.Object) null)
      return;
    this.UpdateFrameMaterial();
    CollectionDeckTileActor.DeckTileFrameColorSet colorSet;
    Material material;
    if (this.m_ghosted == CollectionDeckTileActor.GhostedState.NONE)
    {
      colorSet = this.m_normalColorSet;
      material = this.m_manaGemNormalMaterial;
    }
    else if (this.m_ghosted == CollectionDeckTileActor.GhostedState.BLUE)
    {
      colorSet = this.m_ghostedColorSet;
      material = this.m_manaGemGhostedMaterial;
    }
    else
    {
      colorSet = this.m_redColorSet;
      material = this.m_manaGemRedGhostedMaterial;
    }
    this.m_manaGem.material = material;
    this.m_countText.TextColor = colorSet.m_countTextColor;
    this.m_nameTextMesh.TextColor = colorSet.m_nameTextColor;
    this.m_costTextMesh.TextColor = colorSet.m_costTextColor;
    if (this.m_countText.Outline)
      this.m_countText.OutlineColor = colorSet.m_outlineColor;
    if (this.m_nameTextMesh.Outline)
      this.m_nameTextMesh.OutlineColor = colorSet.m_outlineColor;
    if (this.m_costTextMesh.Outline)
      this.m_costTextMesh.OutlineColor = colorSet.m_outlineColor;
    if ((bool) ((UnityEngine.Object) this.m_highlight) && (bool) ((UnityEngine.Object) colorSet.m_highlightMaterial))
      this.m_highlight.GetComponent<Renderer>().material = colorSet.m_highlightMaterial;
    if ((bool) ((UnityEngine.Object) this.m_highlightGlow) && (bool) ((UnityEngine.Object) colorSet.m_highlightGlowMaterial))
      this.m_highlightGlow.GetComponent<Renderer>().material = colorSet.m_highlightGlowMaterial;
    this.SetDesaturationAmount(this.GetPortraitMaterial(), colorSet);
    this.SetDesaturationAmount(this.m_uniqueStar.GetComponent<MeshRenderer>().material, colorSet);
  }

  protected override Material GetPortraitMaterial()
  {
    return this.m_portraitMesh.GetComponent<MeshRenderer>().material;
  }

  private void SetDesaturationAmount(Material material, CollectionDeckTileActor.DeckTileFrameColorSet colorSet)
  {
    material.SetColor("_Color", colorSet.m_desatColor);
    material.SetFloat("_Desaturate", colorSet.m_desatAmount);
    material.SetFloat("_Contrast", colorSet.m_desatContrast);
  }

  private void UpdateFrameMaterial()
  {
    Material interiorMaterial = this.m_standardFrameInteriorMaterial;
    Material material;
    if (this.m_ghosted == CollectionDeckTileActor.GhostedState.BLUE)
      material = this.m_ghostedFrameMaterial;
    else if (this.m_ghosted == CollectionDeckTileActor.GhostedState.RED)
    {
      material = this.m_redFrameMaterial;
      interiorMaterial = this.m_redFrameInteriorMaterial;
    }
    else
      material = this.GetPremium() == TAG_PREMIUM.GOLDEN ? this.m_premiumFrameMaterial : this.m_standardFrameMaterial;
    if ((UnityEngine.Object) material != (UnityEngine.Object) null)
      this.m_frame.GetComponent<Renderer>().material = material;
    if (!((UnityEngine.Object) interiorMaterial != (UnityEngine.Object) null))
      return;
    this.m_frameInterior.GetComponent<Renderer>().material = interiorMaterial;
  }

  private void AssignSlider()
  {
    this.m_originalSliderLocalPos = this.m_slider.transform.localPosition;
    this.m_openSliderLocalPos = this.m_rootObject.transform.FindChild("OpenSliderPosition").transform.localPosition;
  }

  private void AssignCardCount()
  {
    this.m_countTextMesh = this.m_rootObject.transform.FindChild("CardCountText").GetComponent<UberText>();
  }

  private void OpenSlider(bool useSliderAnimations)
  {
    if (this.m_sliderIsOpen)
      return;
    this.m_sliderIsOpen = true;
    iTween.StopByName(this.m_slider.gameObject, "position");
    if (useSliderAnimations)
      iTween.MoveTo(this.m_slider.gameObject, iTween.Hash((object) "position", (object) this.m_openSliderLocalPos, (object) "isLocal", (object) true, (object) "time", (object) 0.35f, (object) "easetype", (object) iTween.EaseType.easeOutBounce, (object) "name", (object) "position"));
    else
      this.m_slider.transform.localPosition = this.m_openSliderLocalPos;
  }

  private void CloseSlider(bool useSliderAnimations)
  {
    if (!this.m_sliderIsOpen)
      return;
    this.m_sliderIsOpen = false;
    iTween.StopByName(this.m_slider.gameObject, "position");
    if (useSliderAnimations)
      iTween.MoveTo(this.m_slider.gameObject, iTween.Hash((object) "position", (object) this.m_originalSliderLocalPos, (object) "isLocal", (object) true, (object) "time", (object) 0.35f, (object) "easetype", (object) iTween.EaseType.easeOutBounce, (object) "name", (object) "position"));
    else
      this.m_slider.transform.localPosition = this.m_originalSliderLocalPos;
  }

  [Serializable]
  public class DeckTileFrameColorSet
  {
    public Color m_desatColor = Color.white;
    public Color m_costTextColor = Color.white;
    public Color m_countTextColor = new Color(1f, 0.9f, 0.0f, 1f);
    public Color m_nameTextColor = Color.white;
    public Color m_sliderColor = new Color(0.62f, 0.62f, 0.62f, 1f);
    public Color m_outlineColor = Color.black;
    public float m_desatContrast;
    public float m_desatAmount;
    public Material m_highlightMaterial;
    public Material m_highlightGlowMaterial;
  }

  public enum GhostedState
  {
    NONE,
    BLUE,
    RED,
  }
}
