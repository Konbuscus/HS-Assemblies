﻿// Decompiled with JetBrains decompiler
// Type: GetWorldSpace
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GetWorldSpace : MonoBehaviour
{
  [ContextMenu("Get Xforms")]
  public void GetWorldXforms()
  {
    Debug.LogFormat("position = {0}", (object) this.transform.position);
    Debug.LogFormat("rotation = {0}", (object) this.transform.eulerAngles);
  }
}
