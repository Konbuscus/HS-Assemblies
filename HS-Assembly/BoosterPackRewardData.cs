﻿// Decompiled with JetBrains decompiler
// Type: BoosterPackRewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class BoosterPackRewardData : RewardData
{
  public int Id { get; set; }

  public int Count { get; set; }

  public BoosterPackRewardData()
    : this(0, 0)
  {
  }

  public BoosterPackRewardData(int id, int count)
    : base(Reward.Type.BOOSTER_PACK)
  {
    this.Id = id;
    this.Count = count;
  }

  public override string ToString()
  {
    return string.Format("[BoosterPackRewardData: BoosterType={0} Count={1} Origin={2} OriginData={3}]", (object) this.Id, (object) this.Count, (object) this.Origin, (object) this.OriginData);
  }

  protected override string GetGameObjectName()
  {
    return "BoosterPackReward";
  }
}
