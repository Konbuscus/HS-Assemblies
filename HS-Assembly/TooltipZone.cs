﻿// Decompiled with JetBrains decompiler
// Type: TooltipZone
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TooltipZone : MonoBehaviour
{
  public GameObject tooltipPrefab;
  public Transform tooltipDisplayLocation;
  public Transform touchTooltipLocation;
  public GameObject targetObject;
  private GameObject m_tooltip;

  public GameObject GetTooltipObject()
  {
    return this.m_tooltip;
  }

  public bool IsShowingTooltip()
  {
    return (Object) this.m_tooltip != (Object) null;
  }

  public TooltipPanel ShowTooltip(string headline, string bodytext, float scale, bool enablePhoneScale = true)
  {
    if ((Object) this.m_tooltip != (Object) null)
      return this.m_tooltip.GetComponent<TooltipPanel>();
    if ((bool) UniversalInputManager.UsePhoneUI && enablePhoneScale)
      scale *= 2f;
    this.m_tooltip = Object.Instantiate<GameObject>(this.tooltipPrefab);
    TooltipPanel component = this.m_tooltip.GetComponent<TooltipPanel>();
    component.Reset();
    component.Initialize(headline, bodytext);
    component.SetScale(scale);
    if (UniversalInputManager.Get().IsTouchMode() && (Object) this.touchTooltipLocation != (Object) null)
    {
      component.transform.position = this.touchTooltipLocation.position;
      component.transform.rotation = this.touchTooltipLocation.rotation;
    }
    else if ((Object) this.tooltipDisplayLocation != (Object) null)
    {
      component.transform.position = this.tooltipDisplayLocation.position;
      component.transform.rotation = this.tooltipDisplayLocation.rotation;
    }
    component.transform.parent = this.transform;
    return component;
  }

  public TooltipPanel ShowTooltip(string headline, string bodytext)
  {
    float scale = !SceneMgr.Get().IsInGame() ? (float) TooltipPanel.COLLECTION_MANAGER_SCALE : (float) TooltipPanel.GAMEPLAY_SCALE;
    return this.ShowTooltip(headline, bodytext, scale, true);
  }

  public void ShowGameplayTooltip(string headline, string bodytext)
  {
    this.ShowTooltip(headline, bodytext, (float) TooltipPanel.GAMEPLAY_SCALE, true);
  }

  public void ShowGameplayTooltipLarge(string headline, string bodytext)
  {
    this.ShowTooltip(headline, bodytext, (float) TooltipPanel.GAMEPLAY_SCALE_LARGE, false);
  }

  public void ShowBoxTooltip(string headline, string bodytext)
  {
    this.ShowTooltip(headline, bodytext, (float) TooltipPanel.BOX_SCALE, true);
  }

  public TooltipPanel ShowLayerTooltip(string headline, string bodytext)
  {
    TooltipPanel tooltipPanel = this.ShowTooltip(headline, bodytext, 1f, true);
    if ((Object) this.tooltipDisplayLocation == (Object) null)
      return tooltipPanel;
    tooltipPanel.transform.parent = this.tooltipDisplayLocation.transform;
    tooltipPanel.transform.localScale = Vector3.one;
    SceneUtils.SetLayer(this.m_tooltip, this.tooltipDisplayLocation.gameObject.layer);
    return tooltipPanel;
  }

  public TooltipPanel ShowLayerTooltip(string headline, string bodytext, float scale)
  {
    TooltipPanel tooltipPanel = this.ShowTooltip(headline, bodytext, scale, true);
    if ((Object) this.tooltipDisplayLocation == (Object) null)
      return tooltipPanel;
    tooltipPanel.transform.parent = this.tooltipDisplayLocation.transform;
    Vector3 vector3 = new Vector3(scale, scale, scale);
    tooltipPanel.transform.localScale = vector3;
    SceneUtils.SetLayer(this.m_tooltip, this.tooltipDisplayLocation.gameObject.layer);
    return tooltipPanel;
  }

  public void ShowSocialTooltip(Component target, string headline, string bodytext, float scale, GameLayer layer)
  {
    this.ShowSocialTooltip(target.gameObject, headline, bodytext, scale, layer);
  }

  public void ShowSocialTooltip(GameObject targetObject, string headline, string bodytext, float scale, GameLayer layer)
  {
    this.ShowTooltip(headline, bodytext, scale, true);
    SceneUtils.SetLayer(this.m_tooltip, layer);
    Camera firstByLayer1 = CameraUtils.FindFirstByLayer(targetObject.layer);
    Camera firstByLayer2 = CameraUtils.FindFirstByLayer(this.m_tooltip.layer);
    if (!((Object) firstByLayer1 != (Object) firstByLayer2))
      return;
    Vector3 screenPoint = firstByLayer1.WorldToScreenPoint(this.m_tooltip.transform.position);
    this.m_tooltip.transform.position = firstByLayer2.ScreenToWorldPoint(screenPoint);
  }

  public void AnchorTooltipTo(GameObject target, Anchor targetAnchorPoint, Anchor tooltipAnchorPoint)
  {
    if ((Object) this.m_tooltip == (Object) null)
      return;
    TransformUtil.SetPoint(this.m_tooltip, tooltipAnchorPoint, target, targetAnchorPoint);
  }

  public void HideTooltip()
  {
    if (!((Object) this.m_tooltip != (Object) null))
      return;
    Object.Destroy((Object) this.m_tooltip);
  }
}
