﻿// Decompiled with JetBrains decompiler
// Type: MassDisenchantSound
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class MassDisenchantSound
{
  public string m_intro;
  public RaritySound m_common;
  public RaritySound m_rare;
  public RaritySound m_epic;
  public RaritySound m_legendary;
}
