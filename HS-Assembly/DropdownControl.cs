﻿// Decompiled with JetBrains decompiler
// Type: DropdownControl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class DropdownControl : PegUIElement
{
  private DropdownControl.itemChosenCallback m_itemChosenCallback = (DropdownControl.itemChosenCallback) ((param0, param1) => {});
  private DropdownControl.itemTextCallback m_itemTextCallback = new DropdownControl.itemTextCallback(DropdownControl.defaultItemTextCallback);
  private DropdownControl.menuShownCallback m_menuShownCallback = (DropdownControl.menuShownCallback) (param0 => {});
  private List<DropdownMenuItem> m_items = new List<DropdownMenuItem>();
  [CustomEditField(Sections = "Buttons")]
  public DropdownMenuItem m_selectedItem;
  [CustomEditField(Sections = "Buttons")]
  public PegUIElement m_cancelCatcher;
  [CustomEditField(Sections = "Buttons")]
  public UIBButton m_button;
  [CustomEditField(Sections = "Menu")]
  public MultiSliceElement m_menu;
  [CustomEditField(Sections = "Menu")]
  public GameObject m_menuMiddle;
  [CustomEditField(Sections = "Menu")]
  public MultiSliceElement m_menuItemContainer;
  [CustomEditField(Sections = "Menu Templates")]
  public DropdownMenuItem m_menuItemTemplate;
  private Font m_overrideFont;

  public void Start()
  {
    this.m_button.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.onUserPressedButton()));
    this.m_selectedItem.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.onUserPressedSelection(this.m_selectedItem)));
    this.m_cancelCatcher.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.onUserCancelled()));
    this.hideMenu();
  }

  public void addItem(object value)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DropdownControl.\u003CaddItem\u003Ec__AnonStorey46B itemCAnonStorey46B = new DropdownControl.\u003CaddItem\u003Ec__AnonStorey46B();
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey46B.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey46B.item = (DropdownMenuItem) GameUtils.Instantiate((Component) this.m_menuItemTemplate, this.m_menuItemContainer.gameObject, false);
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey46B.item.gameObject.transform.localRotation = this.m_menuItemTemplate.transform.localRotation;
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey46B.item.gameObject.transform.localScale = this.m_menuItemTemplate.transform.localScale;
    // ISSUE: reference to a compiler-generated field
    this.m_items.Add(itemCAnonStorey46B.item);
    if ((Object) this.m_overrideFont != (Object) null)
    {
      // ISSUE: reference to a compiler-generated field
      itemCAnonStorey46B.item.m_text.TrueTypeFont = this.m_overrideFont;
    }
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey46B.item.SetValue(value, this.m_itemTextCallback(value));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    itemCAnonStorey46B.item.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(itemCAnonStorey46B.\u003C\u003Em__36B));
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey46B.item.gameObject.SetActive(true);
    this.layoutMenu();
  }

  public bool removeItem(object value)
  {
    int itemIndex = this.findItemIndex(value);
    if (itemIndex < 0)
      return false;
    DropdownMenuItem dropdownMenuItem = this.m_items[itemIndex];
    this.m_items.RemoveAt(itemIndex);
    Object.Destroy((Object) dropdownMenuItem.gameObject);
    this.layoutMenu();
    return true;
  }

  public void clearItems()
  {
    using (List<DropdownMenuItem>.Enumerator enumerator = this.m_items.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current.gameObject);
    }
    this.layoutMenu();
  }

  public void setSelectionToLastItem()
  {
    this.m_selectedItem.SetValue((object) null, string.Empty);
    if (this.m_items.Count == 0)
      return;
    for (int index = 0; index < this.m_items.Count - 1; ++index)
      this.m_items[index].SetSelected(false);
    DropdownMenuItem dropdownMenuItem = this.m_items[this.m_items.Count - 1];
    dropdownMenuItem.SetSelected(true);
    this.m_selectedItem.SetValue(dropdownMenuItem.GetValue(), this.m_itemTextCallback(dropdownMenuItem.GetValue()));
  }

  public object getSelection()
  {
    return this.m_selectedItem.GetValue();
  }

  public void setSelection(object val)
  {
    this.m_selectedItem.SetValue((object) null, string.Empty);
    for (int index = 0; index < this.m_items.Count; ++index)
    {
      DropdownMenuItem dropdownMenuItem = this.m_items[index];
      object val1 = dropdownMenuItem.GetValue();
      if (val1 == null && val == null || val1.Equals(val))
      {
        dropdownMenuItem.SetSelected(true);
        this.m_selectedItem.SetValue(val1, this.m_itemTextCallback(val1));
      }
      else
        dropdownMenuItem.SetSelected(false);
    }
  }

  public void onUserPressedButton()
  {
    this.showMenu();
  }

  public void onUserPressedSelection(DropdownMenuItem item)
  {
    this.showMenu();
  }

  public void onUserItemClicked(DropdownMenuItem item)
  {
    this.hideMenu();
    object selection = this.getSelection();
    object obj = item.GetValue();
    this.setSelection(obj);
    this.m_itemChosenCallback(obj, selection);
  }

  public void onUserCancelled()
  {
    if (SoundManager.Get().IsInitialized())
      SoundManager.Get().LoadAndPlay("Small_Click");
    this.hideMenu();
  }

  public DropdownControl.itemChosenCallback getItemChosenCallback()
  {
    return this.m_itemChosenCallback;
  }

  public void setItemChosenCallback(DropdownControl.itemChosenCallback callback)
  {
    this.m_itemChosenCallback = callback ?? (DropdownControl.itemChosenCallback) ((param0, param1) => {});
  }

  public DropdownControl.itemTextCallback getItemTextCallback()
  {
    return this.m_itemTextCallback;
  }

  public void setItemTextCallback(DropdownControl.itemTextCallback callback)
  {
    this.m_itemTextCallback = callback ?? new DropdownControl.itemTextCallback(DropdownControl.defaultItemTextCallback);
  }

  public static string defaultItemTextCallback(object val)
  {
    if (val == null)
      return string.Empty;
    return val.ToString();
  }

  public bool isMenuShown()
  {
    return this.m_menu.gameObject.activeInHierarchy;
  }

  public DropdownControl.menuShownCallback getMenuShownCallback()
  {
    return this.m_menuShownCallback;
  }

  public void setMenuShownCallback(DropdownControl.menuShownCallback callback)
  {
    this.m_menuShownCallback = callback;
  }

  public void setFont(Font font)
  {
    this.m_overrideFont = font;
    this.m_selectedItem.m_text.TrueTypeFont = font;
    this.m_menuItemTemplate.m_text.TrueTypeFont = font;
  }

  private void showMenu()
  {
    this.m_cancelCatcher.gameObject.SetActive(true);
    this.m_menu.gameObject.SetActive(true);
    this.layoutMenu();
    this.m_menuShownCallback(true);
  }

  private void hideMenu()
  {
    this.m_cancelCatcher.gameObject.SetActive(false);
    this.m_menu.gameObject.SetActive(false);
    this.m_menuShownCallback(false);
  }

  private void layoutMenu()
  {
    if (!this.gameObject.activeSelf)
      return;
    this.m_menuItemTemplate.gameObject.SetActive(true);
    OrientedBounds orientedWorldBounds = TransformUtil.ComputeOrientedWorldBounds(this.m_menuItemTemplate.gameObject, true);
    if (orientedWorldBounds == null)
      return;
    float num = orientedWorldBounds.Extents[1].magnitude * 2f;
    this.m_menuItemTemplate.gameObject.SetActive(false);
    this.m_menuItemContainer.ClearSlices();
    for (int index = 0; index < this.m_items.Count; ++index)
      this.m_menuItemContainer.AddSlice(this.m_items[index].gameObject);
    this.m_menuItemContainer.UpdateSlices();
    if (this.m_items.Count <= 1)
      TransformUtil.SetLocalScaleZ(this.m_menuMiddle, 1f / 1000f);
    else
      TransformUtil.SetLocalScaleToWorldDimension(this.m_menuMiddle, new WorldDimensionIndex(num * (float) (this.m_items.Count - 1), 2));
    this.m_menu.UpdateSlices();
  }

  private int findItemIndex(object value)
  {
    for (int index = 0; index < this.m_items.Count; ++index)
    {
      if (this.m_items[index].GetValue() == value)
        return index;
    }
    return -1;
  }

  private DropdownMenuItem findItem(object value)
  {
    for (int index = 0; index < this.m_items.Count; ++index)
    {
      DropdownMenuItem dropdownMenuItem = this.m_items[index];
      if (dropdownMenuItem.GetValue() == value)
        return dropdownMenuItem;
    }
    return (DropdownMenuItem) null;
  }

  public delegate void itemChosenCallback(object selection, object prevSelection);

  public delegate string itemTextCallback(object val);

  public delegate void menuShownCallback(bool shown);
}
