﻿// Decompiled with JetBrains decompiler
// Type: MusicTrack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class MusicTrack
{
  public float m_volume = 1f;
  public bool m_shuffle = true;
  [CustomEditField(ListSortable = true)]
  public MusicTrackType m_trackType;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_name;

  public MusicTrack Clone()
  {
    return (MusicTrack) this.MemberwiseClone();
  }
}
