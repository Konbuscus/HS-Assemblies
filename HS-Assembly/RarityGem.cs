﻿// Decompiled with JetBrains decompiler
// Type: RarityGem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RarityGem : MonoBehaviour
{
  public void SetRarityGem(TAG_RARITY rarity, TAG_CARD_SET cardSet)
  {
    if (cardSet == TAG_CARD_SET.CORE)
    {
      this.GetComponent<Renderer>().enabled = false;
    }
    else
    {
      this.GetComponent<Renderer>().enabled = true;
      switch (rarity)
      {
        case TAG_RARITY.RARE:
          this.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.118f, 0.0f);
          break;
        case TAG_RARITY.EPIC:
          this.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.239f, 0.0f);
          break;
        case TAG_RARITY.LEGENDARY:
          this.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.3575f, 0.0f);
          break;
        default:
          this.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.0f, 0.0f);
          break;
      }
    }
  }
}
