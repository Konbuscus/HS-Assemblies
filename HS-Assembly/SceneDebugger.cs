﻿// Decompiled with JetBrains decompiler
// Type: SceneDebugger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Diagnostics;
using System.Text;
using UnityEngine;

public class SceneDebugger : MonoBehaviour
{
  public float m_MinTimeScale = 0.01f;
  public float m_MaxTimeScale = 4f;
  public Vector2 m_GUIPosition = new Vector2(0.01f, 0.065f);
  public Vector2 m_GUISize = new Vector2(175f, 30f);
  private const int MAX_MESSAGES = 60;
  private static SceneDebugger s_instance;
  private QueueList<string> m_messages;
  private StringBuilder m_messageBuilder;
  private GUIStyle m_messageStyle;
  private bool m_hideMessages;

  private void Awake()
  {
    SceneDebugger.s_instance = this;
    if (ApplicationMgr.IsPublic())
      Object.Destroy((Object) this);
    else
      Time.timeScale = SceneDebugger.GetDevTimescale();
  }

  private void OnDestroy()
  {
    SceneDebugger.s_instance = (SceneDebugger) null;
  }

  private void OnGUI()
  {
    if (!Options.Get().GetBool(Option.HUD))
      return;
    this.LayoutLeftScreenControls();
  }

  public static float GetDevTimescale()
  {
    if (ApplicationMgr.IsPublic())
      return 1f;
    return Options.Get().GetFloat(Option.DEV_TIMESCALE, 1f);
  }

  public static void SetDevTimescale(float f)
  {
    if (ApplicationMgr.IsPublic())
      return;
    Options.Get().SetFloat(Option.DEV_TIMESCALE, f);
    Time.timeScale = f;
  }

  public static SceneDebugger Get()
  {
    return SceneDebugger.s_instance;
  }

  public void AddMessage(string message)
  {
    this.InitMessagesIfNecessary();
    if (this.m_messages.Count >= 60)
      this.m_messages.Dequeue();
    this.m_messages.Enqueue(message);
  }

  private void LayoutLeftScreenControls()
  {
    Vector2 guiSize = this.m_GUISize;
    Vector2 vector2_1 = new Vector2((float) Screen.width * this.m_GUIPosition.x, (float) Screen.height * this.m_GUIPosition.y);
    Vector2 vector2_2 = new Vector2(vector2_1.x, vector2_1.y);
    Vector2 vector2_3 = new Vector2();
    Vector2 offset = vector2_2;
    this.LayoutTimeControls(ref offset, guiSize);
    this.LayoutQualityControls(ref offset, guiSize);
    this.LayoutStats(ref offset, guiSize);
    this.LayoutMessages(ref offset, guiSize);
  }

  private void LayoutTimeControls(ref Vector2 offset, Vector2 size)
  {
    GUI.Box(new Rect(offset.x, offset.y, size.x, size.y), string.Format("Time Scale: {0}", (object) SceneDebugger.GetDevTimescale()));
    offset.y += 1f * size.y;
    SceneDebugger.SetDevTimescale(GUI.HorizontalSlider(new Rect(offset.x, offset.y, size.x, size.y), Time.timeScale, this.m_MinTimeScale, this.m_MaxTimeScale));
    offset.y += 1f * size.y;
    if (GUI.Button(new Rect(offset.x, offset.y, size.x, size.y), "Reset Time Scale"))
      SceneDebugger.SetDevTimescale(1f);
    offset.y += 1.5f * size.y;
  }

  private void LayoutQualityControls(ref Vector2 offset, Vector2 size)
  {
    float width = size.x / 3f;
    if (GUI.Button(new Rect(offset.x, offset.y, width, size.y), "Low"))
      GraphicsManager.Get().RenderQualityLevel = GraphicsQuality.Low;
    if (GUI.Button(new Rect(offset.x + width, offset.y, width, size.y), "Medium"))
      GraphicsManager.Get().RenderQualityLevel = GraphicsQuality.Medium;
    if (GUI.Button(new Rect(offset.x + width * 2f, offset.y, width, size.y), "High"))
      GraphicsManager.Get().RenderQualityLevel = GraphicsQuality.High;
    offset.y += 1.5f * size.y;
  }

  private void LayoutStats(ref Vector2 offset, Vector2 size)
  {
  }

  [Conditional("UNITY_EDITOR")]
  private void LayoutCursorControls(ref Vector2 offset, Vector2 size)
  {
    if (Cursor.visible)
    {
      if (GUI.Button(new Rect(offset.x, offset.y, size.x, size.y), "Force Hardware Cursor Off"))
        Cursor.visible = false;
    }
    else if (GUI.Button(new Rect(offset.x, offset.y, size.x, size.y), "Force Hardware Cursor On"))
      Cursor.visible = true;
    offset.y += 1.5f * size.y;
  }

  private void InitMessagesIfNecessary()
  {
    if (this.m_messages != null)
      return;
    this.m_messages = new QueueList<string>();
  }

  private void InitMessageStyleIfNecessary()
  {
    if (this.m_messageStyle != null)
      return;
    this.m_messageStyle = new GUIStyle((GUIStyle) "box")
    {
      alignment = TextAnchor.UpperLeft,
      wordWrap = true,
      clipping = TextClipping.Overflow,
      stretchWidth = true
    };
  }

  private void LayoutMessages(ref Vector2 offset, Vector2 size)
  {
    if (this.m_messages == null || this.m_messages.Count == 0)
      return;
    this.InitMessageStyleIfNecessary();
    if (this.m_hideMessages)
    {
      if (!GUI.Button(new Rect(offset.x, offset.y, size.x, size.y), "Show Messages"))
        return;
      this.m_hideMessages = false;
    }
    else if (GUI.Button(new Rect(offset.x, offset.y, size.x, size.y), "Hide Messages"))
    {
      this.m_hideMessages = true;
      return;
    }
    if (GUI.Button(new Rect(size.x + offset.x, offset.y, size.x, size.y), "Clear Messages"))
    {
      this.m_messages.Clear();
    }
    else
    {
      offset.y += size.y;
      string messageText = this.GetMessageText();
      float height = (float) Screen.height - offset.y;
      GUI.Box(new Rect(offset.x, offset.y, (float) Screen.width, height), messageText, this.m_messageStyle);
      offset.y += height;
    }
  }

  private string GetMessageText()
  {
    this.m_messageBuilder = new StringBuilder();
    for (int index = 0; index < this.m_messages.Count; ++index)
      this.m_messageBuilder.AppendLine(this.m_messages[index]);
    return this.m_messageBuilder.ToString();
  }
}
