﻿// Decompiled with JetBrains decompiler
// Type: RankedRewardChest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class RankedRewardChest : MonoBehaviour
{
  public static int NUM_REWARD_TIERS = 4;
  private static string s_rewardChestEarnedText = "GLOBAL_REWARD_CHEST_TIER{0}_EARNED";
  private static string s_rewardChestNameText = "GLOBAL_REWARD_CHEST_TIER{0}";
  public GameObject m_starDestinationBone;
  public MeshFilter m_baseMeshFilter;
  public MeshRenderer m_baseMeshRenderer;
  public MeshRenderer m_glowMeshRenderer;
  public List<ChestVisual> m_chests;
  public UberText m_rankNumber;
  public UberText m_rankBanner;
  public GameObject m_legendaryGem;

  public static int GetChestIndexFromRank(int rank)
  {
    if (rank <= 5)
      return 3;
    if (rank <= 10)
      return 2;
    if (rank <= 15)
      return 1;
    return rank <= 20 ? 0 : -1;
  }

  public static string GetChestNameFromRank(int rank)
  {
    int chestIndexFromRank = RankedRewardChest.GetChestIndexFromRank(rank);
    return GameStrings.Format(string.Format(RankedRewardChest.s_rewardChestNameText, (object) (RankedRewardChest.NUM_REWARD_TIERS - chestIndexFromRank)));
  }

  public static string GetChestEarnedFromRank(int rank)
  {
    int chestIndexFromRank = RankedRewardChest.GetChestIndexFromRank(rank);
    return GameStrings.Format(string.Format(RankedRewardChest.s_rewardChestEarnedText, (object) (RankedRewardChest.NUM_REWARD_TIERS - chestIndexFromRank)));
  }

  public ChestVisual GetChestVisualFromRank(int rank)
  {
    int chestIndexFromRank = RankedRewardChest.GetChestIndexFromRank(rank);
    if (chestIndexFromRank >= 0)
      return this.m_chests[chestIndexFromRank];
    return (ChestVisual) null;
  }

  public bool DoesChestVisualChange(int rank1, int rank2)
  {
    return this.GetChestVisualFromRank(rank1) != this.GetChestVisualFromRank(rank2);
  }

  public void SetRank(int rank)
  {
    Log.EndOfGame.Print("setting chest to rank " + (object) rank);
    ChestVisual chestVisualFromRank = this.GetChestVisualFromRank(rank);
    this.m_baseMeshFilter.mesh = chestVisualFromRank.m_chestMesh;
    this.m_baseMeshRenderer.material = chestVisualFromRank.m_chestMaterial;
    if ((Object) this.m_glowMeshRenderer != (Object) null)
      this.m_glowMeshRenderer.material = chestVisualFromRank.m_glowMaterial;
    if (rank == 0)
    {
      this.m_legendaryGem.SetActive(true);
      this.m_rankNumber.gameObject.SetActive(false);
    }
    else
    {
      this.m_legendaryGem.SetActive(false);
      this.m_rankNumber.gameObject.SetActive(true);
    }
    this.m_rankNumber.gameObject.SetActive(true);
    this.m_rankNumber.Text = rank.ToString();
    this.m_rankBanner.Text = GameStrings.Get(chestVisualFromRank.chestName);
  }
}
