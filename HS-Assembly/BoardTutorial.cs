﻿// Decompiled with JetBrains decompiler
// Type: BoardTutorial
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoardTutorial : MonoBehaviour
{
  public GameObject m_Highlight;
  public GameObject m_EnemyHighlight;
  public Light m_ManaSpotlight;
  private static BoardTutorial s_instance;
  private bool m_highlightEnabled;
  private bool m_enemyHighlightEnabled;

  private void Awake()
  {
    BoardTutorial.s_instance = this;
    SceneUtils.EnableRenderers(this.m_Highlight, false);
    SceneUtils.EnableRenderers(this.m_EnemyHighlight, false);
    if (!((Object) LoadingScreen.Get() != (Object) null))
      return;
    LoadingScreen.Get().NotifyMainSceneObjectAwoke(this.gameObject);
  }

  private void OnDestroy()
  {
    BoardTutorial.s_instance = (BoardTutorial) null;
  }

  public static BoardTutorial Get()
  {
    return BoardTutorial.s_instance;
  }

  public void EnableHighlight(bool enable)
  {
    if (this.m_highlightEnabled == enable)
      return;
    this.m_highlightEnabled = enable;
    this.UpdateHighlight();
  }

  public void EnableEnemyHighlight(bool enable)
  {
    if (this.m_enemyHighlightEnabled == enable)
      return;
    this.m_enemyHighlightEnabled = enable;
    this.UpdateEnemyHighlight();
  }

  public void EnableFullHighlight(bool enable)
  {
    this.EnableHighlight(enable);
    this.EnableEnemyHighlight(enable);
  }

  public bool IsHighlightEnabled()
  {
    return this.m_highlightEnabled;
  }

  private void UpdateHighlight()
  {
    if (this.m_highlightEnabled)
    {
      SceneUtils.EnableRenderers(this.m_Highlight, this.m_highlightEnabled);
      this.m_Highlight.GetComponent<Animation>().Play("Glow_PlayArea_Player_On");
    }
    else
      this.m_Highlight.GetComponent<Animation>().Play("Glow_PlayArea_Player_Off");
  }

  private void UpdateEnemyHighlight()
  {
    if (this.m_enemyHighlightEnabled)
    {
      SceneUtils.EnableRenderers(this.m_EnemyHighlight, this.m_enemyHighlightEnabled);
      this.m_EnemyHighlight.GetComponent<Animation>().Play("Glow_PlayArea_Player_On");
    }
    else
      this.m_EnemyHighlight.GetComponent<Animation>().Play("Glow_PlayArea_Player_Off");
  }
}
