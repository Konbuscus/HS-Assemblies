﻿// Decompiled with JetBrains decompiler
// Type: CollectibleCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleCard
{
  private int m_CardDbId = -1;
  private DateTime m_LatestInsertDate = new DateTime(0L);
  private HashSet<string> m_SearchableTokens;
  private string m_LongSearchableName;
  private string m_LongSearchableNameNonEuropean;
  private string m_LongSearchableNameNoDiacritics;
  private EntityDef m_EntityDef;
  private TAG_PREMIUM m_PremiumType;
  private CardDbfRecord m_CardRecord;

  public int CardDbId
  {
    get
    {
      return this.m_CardDbId;
    }
  }

  public string CardId
  {
    get
    {
      return this.m_EntityDef.GetCardId();
    }
  }

  public string Name
  {
    get
    {
      return this.m_EntityDef.GetName();
    }
  }

  public string CardInHandText
  {
    get
    {
      return CardTextBuilder.GetDefaultCardTextInHand(this.m_EntityDef);
    }
  }

  public string ArtistName
  {
    get
    {
      return this.m_EntityDef.GetArtistName();
    }
  }

  public int ManaCost
  {
    get
    {
      return this.m_EntityDef.GetCost();
    }
  }

  public int Attack
  {
    get
    {
      return this.m_EntityDef.GetATK();
    }
  }

  public int Health
  {
    get
    {
      return this.m_EntityDef.GetHealth();
    }
  }

  public TAG_CARD_SET Set
  {
    get
    {
      return this.m_EntityDef.GetCardSet();
    }
  }

  public TAG_CLASS Class
  {
    get
    {
      return this.m_EntityDef.GetClass();
    }
  }

  public TAG_MULTI_CLASS_GROUP MultiClassGroup
  {
    get
    {
      return this.m_EntityDef.GetMultiClassGroup();
    }
  }

  public TAG_RARITY Rarity
  {
    get
    {
      return this.m_EntityDef.GetRarity();
    }
  }

  public TAG_RACE Race
  {
    get
    {
      return this.m_EntityDef.GetRace();
    }
  }

  public TAG_CARDTYPE CardType
  {
    get
    {
      return this.m_EntityDef.GetCardType();
    }
  }

  public bool IsHero
  {
    get
    {
      return this.m_EntityDef.IsHero();
    }
  }

  public TAG_PREMIUM PremiumType
  {
    get
    {
      return this.m_PremiumType;
    }
  }

  public int SeenCount { get; set; }

  public int OwnedCount { get; set; }

  public int DisenchantCount
  {
    get
    {
      return Mathf.Max(this.OwnedCount - this.DefaultMaxCopiesPerDeck, 0);
    }
  }

  public int DefaultMaxCopiesPerDeck
  {
    get
    {
      return this.m_EntityDef.IsElite() ? 1 : 2;
    }
  }

  public int CraftBuyCost { get; set; }

  public int CraftSellCost { get; set; }

  public bool IsCraftable
  {
    get
    {
      if (CraftingManager.Get().GetCardValue(this.CardId, this.PremiumType) == null || this.IsHero)
        return false;
      return FixedRewardsMgr.Get().CanCraftCard(this.CardId, this.PremiumType);
    }
  }

  public bool IsNewCard
  {
    get
    {
      if (this.OwnedCount > 0 && this.SeenCount < this.OwnedCount)
        return this.SeenCount < this.DefaultMaxCopiesPerDeck;
      return false;
    }
  }

  public int SuggestWeight
  {
    get
    {
      return this.m_CardRecord.SuggestionWeight;
    }
  }

  public bool IsManaCostChanged
  {
    get
    {
      return this.m_CardRecord.ChangedManaCost;
    }
  }

  public bool IsHealthChanged
  {
    get
    {
      return this.m_CardRecord.ChangedHealth;
    }
  }

  public bool IsAttackChanged
  {
    get
    {
      return this.m_CardRecord.ChangedAttack;
    }
  }

  public bool IsCardInHandTextChanged
  {
    get
    {
      return this.m_CardRecord.ChangedCardTextInHand;
    }
  }

  public int ChangeVersion
  {
    get
    {
      return this.m_CardRecord.ChangeVersion;
    }
  }

  public DateTime LatestInsertDate
  {
    get
    {
      return this.m_LatestInsertDate;
    }
    set
    {
      if (!(value > this.m_LatestInsertDate))
        return;
      this.m_LatestInsertDate = value;
    }
  }

  public CollectibleCard(CardDbfRecord cardRecord, EntityDef refEntityDef, TAG_PREMIUM premiumType)
  {
    this.m_CardDbId = cardRecord.ID;
    this.m_EntityDef = refEntityDef;
    this.m_PremiumType = premiumType;
    this.m_CardRecord = cardRecord;
  }

  public HashSet<string> GetSearchableTokens()
  {
    if (this.m_SearchableTokens == null)
    {
      this.m_SearchableTokens = new HashSet<string>();
      CollectibleCardFilter.AddSearchableTokensToSet<TAG_CARD_SET>(this.Set, new Func<TAG_CARD_SET, bool>(GameStrings.HasCardSetName), new Func<TAG_CARD_SET, string>(GameStrings.GetCardSetName), this.m_SearchableTokens);
      CollectibleCardFilter.AddSearchableTokensToSet<TAG_CARD_SET>(this.Set, new Func<TAG_CARD_SET, bool>(GameStrings.HasCardSetNameShortened), new Func<TAG_CARD_SET, string>(GameStrings.GetCardSetNameShortened), this.m_SearchableTokens);
      CollectibleCardFilter.AddSearchableTokensToSet<TAG_CARD_SET>(this.Set, new Func<TAG_CARD_SET, bool>(GameStrings.HasCardSetNameInitials), new Func<TAG_CARD_SET, string>(GameStrings.GetCardSetNameInitials), this.m_SearchableTokens);
      CollectibleCardFilter.AddSearchableTokensToSet<TAG_CLASS>(this.Class, new Func<TAG_CLASS, bool>(GameStrings.HasClassName), new Func<TAG_CLASS, string>(GameStrings.GetClassName), this.m_SearchableTokens);
      CollectibleCardFilter.AddSearchableTokensToSet<TAG_RARITY>(this.Rarity, new Func<TAG_RARITY, bool>(GameStrings.HasRarityText), new Func<TAG_RARITY, string>(GameStrings.GetRarityText), this.m_SearchableTokens);
      CollectibleCardFilter.AddSearchableTokensToSet<TAG_RACE>(this.Race, new Func<TAG_RACE, bool>(GameStrings.HasRaceName), new Func<TAG_RACE, string>(GameStrings.GetRaceName), this.m_SearchableTokens);
      CollectibleCardFilter.AddSearchableTokensToSet<TAG_CARDTYPE>(this.CardType, new Func<TAG_CARDTYPE, bool>(GameStrings.HasCardTypeName), new Func<TAG_CARDTYPE, string>(GameStrings.GetCardTypeName), this.m_SearchableTokens);
      CollectibleCardFilter.AddSearchableTokensToSet<TAG_MULTI_CLASS_GROUP>(this.m_EntityDef.GetMultiClassGroup(), new Func<TAG_MULTI_CLASS_GROUP, bool>(GameStrings.HasMultiClassGroupName), new Func<TAG_MULTI_CLASS_GROUP, string>(GameStrings.GetMultiClassGroupName), this.m_SearchableTokens);
    }
    return this.m_SearchableTokens;
  }

  public bool FindTextInCard(string searchStr)
  {
    searchStr = searchStr.Trim();
    if (this.GetSearchableTokens().Contains(searchStr))
      return true;
    if (this.m_LongSearchableName == null)
    {
      this.m_LongSearchableName = UberText.RemoveMarkupAndCollapseWhitespaces(this.Name + " " + this.CardInHandText);
      this.m_LongSearchableName = this.m_LongSearchableName.Trim().ToLower();
      this.m_LongSearchableNameNonEuropean = CollectibleCardFilter.ConvertEuropeanCharacters(this.m_LongSearchableName);
      this.m_LongSearchableNameNoDiacritics = CollectibleCardFilter.RemoveDiacritics(this.m_LongSearchableName);
    }
    if (!this.m_LongSearchableName.Contains(searchStr, StringComparison.OrdinalIgnoreCase) && !this.m_LongSearchableNameNonEuropean.Contains(searchStr, StringComparison.OrdinalIgnoreCase))
      return this.m_LongSearchableNameNoDiacritics.Contains(searchStr, StringComparison.OrdinalIgnoreCase);
    return true;
  }

  public static bool FindTextInternational(string searchStr, string stringToSearch)
  {
    string str1 = CollectibleCardFilter.ConvertEuropeanCharacters(stringToSearch);
    string str2 = CollectibleCardFilter.RemoveDiacritics(stringToSearch);
    if (!stringToSearch.Contains(searchStr, StringComparison.OrdinalIgnoreCase) && !str1.Contains(searchStr, StringComparison.OrdinalIgnoreCase))
      return str2.Contains(searchStr, StringComparison.OrdinalIgnoreCase);
    return true;
  }

  public void AddCounts(int addOwnedCount, int addSeenCount, DateTime latestInsertDate)
  {
    this.OwnedCount += addOwnedCount;
    this.SeenCount += addSeenCount;
    this.LatestInsertDate = latestInsertDate;
  }

  public void RemoveCounts(int removeOwnedCount)
  {
    this.OwnedCount = Mathf.Max(this.OwnedCount - removeOwnedCount, 0);
  }

  public EntityDef GetEntityDef()
  {
    return this.m_EntityDef;
  }

  public override bool Equals(object obj)
  {
    if (object.ReferenceEquals((object) null, obj))
      return false;
    if (object.ReferenceEquals((object) this, obj))
      return true;
    if (this.CardDbId == ((CollectibleCard) obj).CardDbId)
      return this.PremiumType == ((CollectibleCard) obj).PremiumType;
    return false;
  }

  public override int GetHashCode()
  {
    return (int) (this.CardId.GetHashCode() + this.PremiumType);
  }

  public bool IsCardChanged()
  {
    if (!this.m_CardRecord.ChangedManaCost && !this.m_CardRecord.ChangedHealth && !this.m_CardRecord.ChangedAttack)
      return this.m_CardRecord.ChangedCardTextInHand;
    return true;
  }
}
