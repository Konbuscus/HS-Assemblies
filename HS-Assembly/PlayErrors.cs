﻿// Decompiled with JetBrains decompiler
// Type: PlayErrors
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class PlayErrors
{
  private static bool PLAYERRORS_ENABLED = true;
  private static bool s_initialized = false;
  private static Map<PlayErrors.ErrorType, string> s_playErrorsMessages = new Map<PlayErrors.ErrorType, string>() { { PlayErrors.ErrorType.REQ_MINION_TARGET, "GAMEPLAY_PlayErrors_REQ_MINION_TARGET" }, { PlayErrors.ErrorType.REQ_FRIENDLY_TARGET, "GAMEPLAY_PlayErrors_REQ_FRIENDLY_TARGET" }, { PlayErrors.ErrorType.REQ_ENEMY_TARGET, "GAMEPLAY_PlayErrors_REQ_ENEMY_TARGET" }, { PlayErrors.ErrorType.REQ_DAMAGED_TARGET, "GAMEPLAY_PlayErrors_REQ_DAMAGED_TARGET" }, { PlayErrors.ErrorType.REQ_ENCHANTED_TARGET, "GAMEPLAY_PlayErrors_REQ_ENCHANTED_TARGET" }, { PlayErrors.ErrorType.REQ_FROZEN_TARGET, "GAMEPLAY_PlayErrors_REQ_FROZEN_TARGET" }, { PlayErrors.ErrorType.REQ_CHARGE_TARGET, "GAMEPLAY_PlayErrors_REQ_CHARGE_TARGET" }, { PlayErrors.ErrorType.REQ_TARGET_MAX_ATTACK, "GAMEPLAY_PlayErrors_REQ_TARGET_MAX_ATTACK" }, { PlayErrors.ErrorType.REQ_NONSELF_TARGET, "GAMEPLAY_PlayErrors_REQ_NONSELF_TARGET" }, { PlayErrors.ErrorType.REQ_TARGET_WITH_RACE, "GAMEPLAY_PlayErrors_REQ_TARGET_WITH_RACE" }, { PlayErrors.ErrorType.REQ_TARGET_TO_PLAY, "GAMEPLAY_PlayErrors_REQ_TARGET_TO_PLAY" }, { PlayErrors.ErrorType.REQ_NUM_MINION_SLOTS, "GAMEPLAY_PlayErrors_REQ_NUM_MINION_SLOTS" }, { PlayErrors.ErrorType.REQ_WEAPON_EQUIPPED, "GAMEPLAY_PlayErrors_REQ_WEAPON_EQUIPPED" }, { PlayErrors.ErrorType.REQ_YOUR_TURN, "GAMEPLAY_PlayErrors_REQ_YOUR_TURN" }, { PlayErrors.ErrorType.REQ_NONSTEALTH_ENEMY_TARGET, "GAMEPLAY_PlayErrors_REQ_NONSTEALTH_ENEMY_TARGET" }, { PlayErrors.ErrorType.REQ_HERO_TARGET, "GAMEPLAY_PlayErrors_REQ_HERO_TARGET" }, { PlayErrors.ErrorType.REQ_SECRET_CAP, "GAMEPLAY_PlayErrors_REQ_SECRET_CAP" }, { PlayErrors.ErrorType.REQ_MINION_CAP_IF_TARGET_AVAILABLE, "GAMEPLAY_PlayErrors_REQ_MINION_CAP_IF_TARGET_AVAILABLE" }, { PlayErrors.ErrorType.REQ_MINION_CAP, "GAMEPLAY_PlayErrors_REQ_MINION_CAP" }, { PlayErrors.ErrorType.REQ_TARGET_ATTACKED_THIS_TURN, "GAMEPLAY_PlayErrors_REQ_TARGET_ATTACKED_THIS_TURN" }, { PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE, "GAMEPLAY_PlayErrors_REQ_TARGET_IF_AVAILABLE" }, { PlayErrors.ErrorType.REQ_MINIMUM_ENEMY_MINIONS, "GAMEPLAY_PlayErrors_REQ_MINIMUM_ENEMY_MINIONS" }, { PlayErrors.ErrorType.REQ_TARGET_FOR_COMBO, "GAMEPLAY_PlayErrors_REQ_TARGET_FOR_COMBO" }, { PlayErrors.ErrorType.REQ_NOT_EXHAUSTED_ACTIVATE, "GAMEPLAY_PlayErrors_REQ_NOT_EXHAUSTED_ACTIVATE" }, { PlayErrors.ErrorType.REQ_UNIQUE_SECRET, "GAMEPLAY_PlayErrors_REQ_UNIQUE_SECRET" }, { PlayErrors.ErrorType.REQ_CAN_BE_ATTACKED, "GAMEPLAY_PlayErrors_REQ_CAN_BE_ATTACKED" }, { PlayErrors.ErrorType.REQ_ACTION_PWR_IS_MASTER_PWR, "GAMEPLAY_PlayErrors_REQ_ACTION_PWR_IS_MASTER_PWR" }, { PlayErrors.ErrorType.REQ_TARGET_MAGNET, "GAMEPLAY_PlayErrors_REQ_TARGET_MAGNET" }, { PlayErrors.ErrorType.REQ_ATTACK_GREATER_THAN_0, "GAMEPLAY_PlayErrors_REQ_ATTACK_GREATER_THAN_0" }, { PlayErrors.ErrorType.REQ_ATTACKER_NOT_FROZEN, "GAMEPLAY_PlayErrors_REQ_ATTACKER_NOT_FROZEN" }, { PlayErrors.ErrorType.REQ_HERO_OR_MINION_TARGET, "GAMEPLAY_PlayErrors_REQ_HERO_OR_MINION_TARGET" }, { PlayErrors.ErrorType.REQ_CAN_BE_TARGETED_BY_SPELLS, "GAMEPLAY_PlayErrors_REQ_CAN_BE_TARGETED_BY_SPELLS" }, { PlayErrors.ErrorType.REQ_SUBCARD_IS_PLAYABLE, "GAMEPLAY_PlayErrors_REQ_SUBCARD_IS_PLAYABLE" }, { PlayErrors.ErrorType.REQ_TARGET_FOR_NO_COMBO, "GAMEPLAY_PlayErrors_REQ_TARGET_FOR_NO_COMBO" }, { PlayErrors.ErrorType.REQ_NOT_MINION_JUST_PLAYED, "GAMEPLAY_PlayErrors_REQ_NOT_MINION_JUST_PLAYED" }, { PlayErrors.ErrorType.REQ_NOT_EXHAUSTED_HERO_POWER, "GAMEPLAY_PlayErrors_REQ_NOT_EXHAUSTED_HERO_POWER" }, { PlayErrors.ErrorType.REQ_CAN_BE_TARGETED_BY_OPPONENTS, "GAMEPLAY_PlayErrors_REQ_CAN_BE_TARGETED_BY_OPPONENTS" }, { PlayErrors.ErrorType.REQ_ATTACKER_CAN_ATTACK, "GAMEPLAY_PlayErrors_REQ_ATTACKER_CAN_ATTACK" }, { PlayErrors.ErrorType.REQ_TARGET_MIN_ATTACK, "GAMEPLAY_PlayErrors_REQ_TARGET_MIN_ATTACK" }, { PlayErrors.ErrorType.REQ_CAN_BE_TARGETED_BY_HERO_POWERS, "GAMEPLAY_PlayErrors_REQ_CAN_BE_TARGETED_BY_HERO_POWERS" }, { PlayErrors.ErrorType.REQ_ENEMY_TARGET_NOT_IMMUNE, "GAMEPLAY_PlayErrors_REQ_ENEMY_TARGET_NOT_IMMUNE" }, { PlayErrors.ErrorType.REQ_ENTIRE_ENTOURAGE_NOT_IN_PLAY, "GAMEPLAY_PlayErrors_REQ_ENTIRE_ENTOURAGE_NOT_IN_PLAY" }, { PlayErrors.ErrorType.REQ_MINIMUM_TOTAL_MINIONS, "GAMEPLAY_PlayErrors_REQ_MINIMUM_TOTAL_MINIONS" }, { PlayErrors.ErrorType.REQ_MUST_TARGET_TAUNTER, "GAMEPLAY_PlayErrors_REQ_MUST_TARGET_TAUNTER" }, { PlayErrors.ErrorType.REQ_UNDAMAGED_TARGET, "GAMEPLAY_PlayErrors_REQ_UNDAMAGED_TARGET" }, { PlayErrors.ErrorType.REQ_CAN_BE_TARGETED_BY_BATTLECRIES, "GAMEPLAY_PlayErrors_REQ_CAN_BE_TARGETED_BY_BATTLECRIES" }, { PlayErrors.ErrorType.REQ_STEADY_SHOT, "GAMEPLAY_PlayErrors_REQ_STEADY_SHOT" }, { PlayErrors.ErrorType.REQ_MINION_OR_ENEMY_HERO, "GAMEPLAY_PlayErrors_REQ_MINION_OR_ENEMY_HERO" }, { PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_DRAGON_IN_HAND, "GAMEPLAY_PlayErrors_REQ_TARGET_IF_AVAILABLE_AND_DRAGON_IN_HAND" }, { PlayErrors.ErrorType.REQ_LEGENDARY_TARGET, "GAMEPLAY_PlayErrors_REQ_LEGENDARY_TARGET" }, { PlayErrors.ErrorType.REQ_FRIENDLY_MINION_DIED_THIS_TURN, "GAMEPLAY_PlayErrors_REQ_FRIENDLY_MINION_DIED_THIS_TURN" }, { PlayErrors.ErrorType.REQ_FRIENDLY_MINION_DIED_THIS_GAME, "GAMEPLAY_PlayErrors_REQ_FRIENDLY_MINION_DIED_THIS_GAME" }, { PlayErrors.ErrorType.REQ_ENEMY_WEAPON_EQUIPPED, "GAMEPLAY_PlayErrors_REQ_ENEMY_WEAPON_EQUIPPED" }, { PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_MINIONS, "GAMEPLAY_PlayErrors_REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_MINIONS" }, { PlayErrors.ErrorType.REQ_TARGET_WITH_BATTLECRY, "GAMEPLAY_PlayErrors_REQ_TARGET_WITH_BATTLECRY" }, { PlayErrors.ErrorType.REQ_TARGET_WITH_DEATHRATTLE, "GAMEPLAY_PlayErrors_REQ_TARGET_WITH_DEATHRATTLE" }, { PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_SECRETS, "GAMEPLAY_PlayErrors_REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_SECRETS" }, { PlayErrors.ErrorType.REQ_STEALTHED_TARGET, "GAMEPLAY_PlayErrors_REQ_STEALTHED_TARGET" }, { PlayErrors.ErrorType.REQ_MINION_SLOT_OR_MANA_CRYSTAL_SLOT, "GAMEPLAY_PlayErrors_REQ_MINION_SLOT_OR_MANA_CRYSTAL_SLOT" }, { PlayErrors.ErrorType.REQ_DRAG_TO_PLAY, "GAMEPLAY_PlayErrors_REQ_DRAG_TO_PLAY" } };
  private static IntPtr s_DLL = IntPtr.Zero;
  public const string PLAYERRORS_DLL_FILENAME = "PlayErrors32";
  private static PlayErrors.DelPlayErrorsInit DLL_PlayErrorsInit;
  private static PlayErrors.DelGetRequirementsMap DLL_GetRequirementsMap;
  private static PlayErrors.DelGetPlayEntityError DLL_GetPlayEntityError;
  private static PlayErrors.DelGetTargetEntityError DLL_GetTargetEntityError;

  public static bool Init()
  {
    if (!PlayErrors.PLAYERRORS_ENABLED || PlayErrors.s_initialized)
      return true;
    if (!PlayErrors.LoadDLL())
      return false;
    PlayErrors.s_initialized = PlayErrors.DLL_PlayErrorsInit();
    Log.PlayErrors.Print("Init: " + (object) PlayErrors.s_initialized);
    if (!PlayErrors.s_initialized)
      PlayErrors.UnloadDLL();
    return PlayErrors.s_initialized;
  }

  public static bool IsInitialized()
  {
    return PlayErrors.s_initialized;
  }

  public static void AppQuit()
  {
    if (!PlayErrors.PLAYERRORS_ENABLED)
      return;
    PlayErrors.UnloadDLL();
    PlayErrors.s_initialized = false;
    Log.PlayErrors.Print("AppQuit: " + (object) PlayErrors.s_initialized);
  }

  public static void DisplayPlayError(PlayErrors.ErrorType error, Entity errorSource)
  {
    Log.PlayErrors.Print("DisplayPlayError: (" + (object) PlayErrors.s_initialized + ") " + (object) error + " " + (object) errorSource);
    if (!PlayErrors.s_initialized || GameState.Get().GetGameEntity().NotifyOfPlayError(error, errorSource))
      return;
    switch (error)
    {
      case PlayErrors.ErrorType.REQ_MINION_TARGET:
      case PlayErrors.ErrorType.REQ_FRIENDLY_TARGET:
      case PlayErrors.ErrorType.REQ_ENEMY_TARGET:
      case PlayErrors.ErrorType.REQ_DAMAGED_TARGET:
      case PlayErrors.ErrorType.REQ_FROZEN_TARGET:
      case PlayErrors.ErrorType.REQ_TARGET_MAX_ATTACK:
      case PlayErrors.ErrorType.REQ_TARGET_WITH_RACE:
      case PlayErrors.ErrorType.REQ_HERO_TARGET:
      case PlayErrors.ErrorType.REQ_HERO_OR_MINION_TARGET:
      case PlayErrors.ErrorType.REQ_CAN_BE_TARGETED_BY_SPELLS:
      case PlayErrors.ErrorType.REQ_CAN_BE_TARGETED_BY_OPPONENTS:
      case PlayErrors.ErrorType.REQ_TARGET_MIN_ATTACK:
      case PlayErrors.ErrorType.REQ_CAN_BE_TARGETED_BY_HERO_POWERS:
      case PlayErrors.ErrorType.REQ_ENEMY_TARGET_NOT_IMMUNE:
      case PlayErrors.ErrorType.REQ_CAN_BE_TARGETED_BY_BATTLECRIES:
      case PlayErrors.ErrorType.REQ_MINION_OR_ENEMY_HERO:
      case PlayErrors.ErrorType.REQ_LEGENDARY_TARGET:
      case PlayErrors.ErrorType.REQ_TARGET_WITH_BATTLECRY:
      case PlayErrors.ErrorType.REQ_TARGET_WITH_DEATHRATTLE:
      case PlayErrors.ErrorType.REQ_TARGET_EXACT_COST:
      case PlayErrors.ErrorType.REQ_STEALTHED_TARGET:
        GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_TARGET);
        goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
      case PlayErrors.ErrorType.REQ_TARGET_TO_PLAY:
      case PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE:
      case PlayErrors.ErrorType.REQ_TARGET_FOR_COMBO:
      case PlayErrors.ErrorType.REQ_TARGET_FOR_NO_COMBO:
      case PlayErrors.ErrorType.REQ_STEADY_SHOT:
      case PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_DRAGON_IN_HAND:
      case PlayErrors.ErrorType.REQ_FRIENDLY_MINION_DIED_THIS_TURN:
      case PlayErrors.ErrorType.REQ_FRIENDLY_MINION_DIED_THIS_GAME:
      case PlayErrors.ErrorType.REQ_ENEMY_WEAPON_EQUIPPED:
      case PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_MINIONS:
      case PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_SECRETS:
      case PlayErrors.ErrorType.REQ_MINION_SLOT_OR_MANA_CRYSTAL_SLOT:
        GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_PLAY);
        goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
      case PlayErrors.ErrorType.REQ_NUM_MINION_SLOTS:
      case PlayErrors.ErrorType.REQ_MINION_CAP_IF_TARGET_AVAILABLE:
      case PlayErrors.ErrorType.REQ_MINION_CAP:
        GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_FULL_MINIONS);
        goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
      case PlayErrors.ErrorType.REQ_WEAPON_EQUIPPED:
        GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_NEED_WEAPON);
        goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
      case PlayErrors.ErrorType.REQ_ENOUGH_MANA:
        if (errorSource.IsSpell() && PlayErrors.DoSpellsCostHealth() || errorSource.HasTag(GAME_TAG.CARD_COSTS_HEALTH))
        {
          GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_PLAY);
          goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
        }
        else
        {
          GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_NEED_MANA);
          goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
        }
      case PlayErrors.ErrorType.REQ_YOUR_TURN:
        break;
      case PlayErrors.ErrorType.REQ_NONSTEALTH_ENEMY_TARGET:
        GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_STEALTH);
        goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
      case PlayErrors.ErrorType.REQ_NOT_EXHAUSTED_ACTIVATE:
        if (errorSource.IsHero())
        {
          GameState.Get().GetCurrentPlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_I_ATTACKED);
          goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
        }
        else
        {
          GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_MINION_ATTACKED);
          goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
        }
      case PlayErrors.ErrorType.REQ_TARGET_TAUNTER:
        PlayErrors.DisplayTauntErrorEffects();
        goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
      case PlayErrors.ErrorType.REQ_NOT_MINION_JUST_PLAYED:
        GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_JUST_PLAYED);
        goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
      case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY:
        PlayErrors.PlayRequirementInfo playRequirementInfo = PlayErrors.GetPlayRequirementInfo(errorSource);
        string errorDescription = PlayErrors.GetErrorDescription(error, errorSource, playRequirementInfo);
        if (string.IsNullOrEmpty(errorDescription))
          break;
        GameplayErrorManager.Get().DisplayMessage(errorDescription);
        break;
      default:
        GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_GENERIC);
        goto case PlayErrors.ErrorType.REQ_DRAG_TO_PLAY;
    }
  }

  public static PlayErrors.ErrorType GetPlayEntityError(Entity source)
  {
    Log.PlayErrors.Print("GetPlayEntityError (" + (object) PlayErrors.s_initialized + ") " + (object) source);
    if (!PlayErrors.s_initialized)
      return PlayErrors.ErrorType.NONE;
    Player owningPlayer = PlayErrors.GetOwningPlayer(source);
    if (owningPlayer == null)
      return PlayErrors.ErrorType.NONE;
    PlayErrors.Marshaled_PlayErrorsParams playErrorsParams = new PlayErrors.Marshaled_PlayErrorsParams(source.ConvertToSourceInfo(PlayErrors.GetPlayRequirementInfo(source), (Entity) null), owningPlayer.ConvertToPlayerInfo(), GameState.Get().ConvertToGameStateInfo());
    List<PlayErrors.Marshaled_TargetEntityInfo> marshaledEntitiesInPlay = PlayErrors.GetMarshaledEntitiesInPlay();
    List<PlayErrors.Marshaled_SourceEntityInfo> marshaledSubCards = PlayErrors.GetMarshaledSubCards(source);
    return PlayErrors.DLL_GetPlayEntityError(playErrorsParams, marshaledSubCards.ToArray(), marshaledSubCards.Count, marshaledEntitiesInPlay.ToArray(), marshaledEntitiesInPlay.Count);
  }

  public static PlayErrors.ErrorType GetTargetEntityError(Entity source, Entity target)
  {
    if (!PlayErrors.s_initialized)
      return PlayErrors.ErrorType.NONE;
    Player owningPlayer = PlayErrors.GetOwningPlayer(source);
    if (owningPlayer == null)
      return PlayErrors.ErrorType.NONE;
    PlayErrors.Marshaled_PlayErrorsParams playErrorsParams = new PlayErrors.Marshaled_PlayErrorsParams(source.ConvertToSourceInfo(PlayErrors.GetPlayRequirementInfo(source), (Entity) null), owningPlayer.ConvertToPlayerInfo(), GameState.Get().ConvertToGameStateInfo());
    PlayErrors.Marshaled_TargetEntityInfo target1 = PlayErrors.Marshaled_TargetEntityInfo.ConvertFromTargetEntityInfo(target.ConvertToTargetInfo());
    List<PlayErrors.Marshaled_TargetEntityInfo> marshaledEntitiesInPlay = PlayErrors.GetMarshaledEntitiesInPlay();
    return PlayErrors.DLL_GetTargetEntityError(playErrorsParams, target1, marshaledEntitiesInPlay.ToArray(), marshaledEntitiesInPlay.Count);
  }

  public static ulong GetRequirementsMap(List<PlayErrors.ErrorType> requirements)
  {
    if (!PlayErrors.s_initialized)
      return 0;
    return PlayErrors.DLL_GetRequirementsMap(requirements.ToArray(), requirements.Count);
  }

  private static Player GetOwningPlayer(Entity entity)
  {
    Player player = GameState.Get().GetPlayer(entity.GetControllerId());
    if (player == null)
      Log.PlayErrors.Print(string.Format("Error retrieving controlling player of entity {0} in PlayErrors.GetOwningPlayer()!", (object) entity.GetName()));
    return player;
  }

  private static PlayErrors.PlayRequirementInfo GetPlayRequirementInfo(Entity entity)
  {
    if (entity.GetZone() == TAG_ZONE.HAND || entity.GetZone() == TAG_ZONE.SETASIDE || (entity.IsHeroPower() || entity.ShouldUseBattlecryPower()))
      return entity.GetMasterPower().GetPlayRequirementInfo();
    return entity.GetAttackPower().GetPlayRequirementInfo();
  }

  private static List<PlayErrors.Marshaled_SourceEntityInfo> GetMarshaledSubCards(Entity source)
  {
    List<PlayErrors.Marshaled_SourceEntityInfo> sourceEntityInfoList = new List<PlayErrors.Marshaled_SourceEntityInfo>();
    using (List<int>.Enumerator enumerator = source.GetSubCardIDs().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity = GameState.Get().GetEntity(enumerator.Current);
        if (entity == null)
        {
          Log.PlayErrors.Print(string.Format("Subcard of {0} is null in GetMarshaledSubCards()!", (object) source.GetName()));
        }
        else
        {
          PlayErrors.SourceEntityInfo sourceInfo = entity.ConvertToSourceInfo(Power.GetDefaultMasterPower().GetPlayRequirementInfo(), source);
          sourceEntityInfoList.Add(PlayErrors.Marshaled_SourceEntityInfo.ConvertFromSourceEntityInfo(sourceInfo));
        }
      }
    }
    return sourceEntityInfoList;
  }

  private static List<PlayErrors.Marshaled_TargetEntityInfo> GetMarshaledEntitiesInPlay()
  {
    List<PlayErrors.Marshaled_TargetEntityInfo> targetEntityInfoList = new List<PlayErrors.Marshaled_TargetEntityInfo>();
    using (List<Zone>.Enumerator enumerator1 = ZoneMgr.Get().FindZonesForTag(TAG_ZONE.PLAY).GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Card>.Enumerator enumerator2 = enumerator1.Current.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Entity entity = enumerator2.Current.GetEntity();
            if (entity.GetZone() == TAG_ZONE.PLAY)
              targetEntityInfoList.Add(PlayErrors.Marshaled_TargetEntityInfo.ConvertFromTargetEntityInfo(entity.ConvertToTargetInfo()));
          }
        }
      }
    }
    return targetEntityInfoList;
  }

  private static bool CanShowMinionTauntError()
  {
    int minionCount;
    int heroCount;
    GameState.Get().GetTauntCounts(GameState.Get().GetOpposingSidePlayer(), out minionCount, out heroCount);
    if (minionCount > 0)
      return heroCount == 0;
    return false;
  }

  private static void DisplayTauntErrorEffects()
  {
    if (PlayErrors.CanShowMinionTauntError())
      GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(EmoteType.ERROR_TAUNT);
    GameState.Get().ShowEnemyTauntCharacters();
  }

  private static bool DoSpellsCostHealth()
  {
    return GameState.Get().GetFriendlySidePlayer().HasTag(GAME_TAG.SPELLS_COST_HEALTH);
  }

  private static string GetErrorDescription(PlayErrors.ErrorType type, Entity errorSource, PlayErrors.PlayRequirementInfo requirementInfo)
  {
    Log.PlayErrors.Print("GetErrorDescription: " + (object) type + " " + (object) requirementInfo);
    PlayErrors.ErrorType errorType = type;
    switch (errorType)
    {
      case PlayErrors.ErrorType.REQ_ENOUGH_MANA:
        if (errorSource.IsSpell() && PlayErrors.DoSpellsCostHealth() || errorSource.HasTag(GAME_TAG.CARD_COSTS_HEALTH))
          return GameStrings.Get("GAMEPLAY_PlayErrors_REQ_ENOUGH_HEALTH");
        return GameStrings.Get("GAMEPLAY_PlayErrors_REQ_ENOUGH_MANA");
      case PlayErrors.ErrorType.REQ_YOUR_TURN:
        return string.Empty;
      case PlayErrors.ErrorType.REQ_SECRET_CAP:
        return GameStrings.Format("GAMEPLAY_PlayErrors_REQ_SECRET_CAP", (object) GameState.Get().GetMaxSecretsPerPlayer());
      default:
        switch (errorType - 8)
        {
          case PlayErrors.ErrorType.NONE:
            return GameStrings.Format("GAMEPLAY_PlayErrors_REQ_TARGET_MAX_ATTACK", (object) requirementInfo.paramMaxAtk);
          case PlayErrors.ErrorType.REQ_FRIENDLY_TARGET:
            return GameStrings.Format("GAMEPLAY_PlayErrors_REQ_TARGET_WITH_RACE", (object) GameStrings.GetRaceName((TAG_RACE) requirementInfo.paramRace));
          default:
            switch (errorType - 27)
            {
              case PlayErrors.ErrorType.NONE:
                if (PlayErrors.CanShowMinionTauntError())
                  return GameStrings.Get("GAMEPLAY_PlayErrors_REQ_TARGET_TAUNTER_MINION");
                return GameStrings.Get("GAMEPLAY_PlayErrors_REQ_TARGET_TAUNTER_CHARACTER");
              case PlayErrors.ErrorType.REQ_FRIENDLY_TARGET:
                return PlayErrors.ErrorInEditorOnly("[Unity Editor] Action power must be master power");
              default:
                if (errorType != PlayErrors.ErrorType.REQ_SECRET_CAP_FOR_NON_SECRET)
                {
                  if (errorType != PlayErrors.ErrorType.REQ_TARGET_EXACT_COST)
                  {
                    if (errorType != PlayErrors.ErrorType.NONE)
                    {
                      if (errorType != PlayErrors.ErrorType.REQ_MINIMUM_ENEMY_MINIONS)
                      {
                        if (errorType != PlayErrors.ErrorType.REQ_TARGET_MIN_ATTACK)
                        {
                          if (errorType == PlayErrors.ErrorType.REQ_MINIMUM_TOTAL_MINIONS)
                            return GameStrings.Format("GAMEPLAY_PlayErrors_REQ_MINIMUM_TOTAL_MINIONS", (object) requirementInfo.paramMinNumTotalMinions);
                          string key = (string) null;
                          if (PlayErrors.s_playErrorsMessages.TryGetValue(type, out key))
                            return GameStrings.Get(key);
                          return PlayErrors.ErrorInEditorOnly("[Unity Editor] Unknown play error ({0})", (object) type);
                        }
                        return GameStrings.Format("GAMEPLAY_PlayErrors_REQ_TARGET_MIN_ATTACK", (object) requirementInfo.paramMinAtk);
                      }
                      return GameStrings.Format("GAMEPLAY_PlayErrors_REQ_MINIMUM_ENEMY_MINIONS", (object) requirementInfo.paramMinNumEnemyMinions);
                    }
                    Debug.LogWarning((object) "PlayErrors.GetErrorDescription() - Action is not valid, but no error string found.");
                    return string.Empty;
                  }
                  return GameStrings.Format("GAMEPLAY_PlayErrors_REQ_TARGET_EXACT_COST", (object) requirementInfo.paramExactCost);
                }
                return GameStrings.Format("GAMEPLAY_PlayErrors_REQ_SECRET_CAP", (object) GameState.Get().GetMaxSecretsPerPlayer());
            }
        }
    }
  }

  private static string ErrorInEditorOnly(string format, params object[] args)
  {
    return string.Empty;
  }

  private static IntPtr GetFunction(string name)
  {
    IntPtr procAddress = DLLUtils.GetProcAddress(PlayErrors.s_DLL, name);
    if (procAddress == IntPtr.Zero)
    {
      Debug.LogError((object) ("Could not load PlayErrors." + name + "()"));
      PlayErrors.AppQuit();
    }
    return procAddress;
  }

  private static bool LoadDLL()
  {
    PlayErrors.s_DLL = FileUtils.LoadPlugin("PlayErrors32", true);
    if (PlayErrors.s_DLL == IntPtr.Zero)
      return false;
    PlayErrors.DLL_PlayErrorsInit = (PlayErrors.DelPlayErrorsInit) Marshal.GetDelegateForFunctionPointer(PlayErrors.GetFunction("PlayErrorsInit"), typeof (PlayErrors.DelPlayErrorsInit));
    PlayErrors.DLL_GetRequirementsMap = (PlayErrors.DelGetRequirementsMap) Marshal.GetDelegateForFunctionPointer(PlayErrors.GetFunction("GetRequirementsMap"), typeof (PlayErrors.DelGetRequirementsMap));
    PlayErrors.DLL_GetPlayEntityError = (PlayErrors.DelGetPlayEntityError) Marshal.GetDelegateForFunctionPointer(PlayErrors.GetFunction("GetPlayEntityError"), typeof (PlayErrors.DelGetPlayEntityError));
    PlayErrors.DLL_GetTargetEntityError = (PlayErrors.DelGetTargetEntityError) Marshal.GetDelegateForFunctionPointer(PlayErrors.GetFunction("GetTargetEntityError"), typeof (PlayErrors.DelGetTargetEntityError));
    return PlayErrors.s_DLL != IntPtr.Zero;
  }

  private static void UnloadDLL()
  {
    if (!PlayErrors.s_initialized)
      return;
    Log.PlayErrors.Print("Unloading PlayErrors DLL..");
    if (!DLLUtils.FreeLibrary(PlayErrors.s_DLL))
      Debug.LogError((object) string.Format("error unloading {0}", (object) "PlayErrors32"));
    PlayErrors.s_DLL = IntPtr.Zero;
  }

  public enum ErrorType
  {
    NONE,
    REQ_MINION_TARGET,
    REQ_FRIENDLY_TARGET,
    REQ_ENEMY_TARGET,
    REQ_DAMAGED_TARGET,
    REQ_ENCHANTED_TARGET,
    REQ_FROZEN_TARGET,
    REQ_CHARGE_TARGET,
    REQ_TARGET_MAX_ATTACK,
    REQ_NONSELF_TARGET,
    REQ_TARGET_WITH_RACE,
    REQ_TARGET_TO_PLAY,
    REQ_NUM_MINION_SLOTS,
    REQ_WEAPON_EQUIPPED,
    REQ_ENOUGH_MANA,
    REQ_YOUR_TURN,
    REQ_NONSTEALTH_ENEMY_TARGET,
    REQ_HERO_TARGET,
    REQ_SECRET_CAP,
    REQ_MINION_CAP_IF_TARGET_AVAILABLE,
    REQ_MINION_CAP,
    REQ_TARGET_ATTACKED_THIS_TURN,
    REQ_TARGET_IF_AVAILABLE,
    REQ_MINIMUM_ENEMY_MINIONS,
    REQ_TARGET_FOR_COMBO,
    REQ_NOT_EXHAUSTED_ACTIVATE,
    REQ_UNIQUE_SECRET,
    REQ_TARGET_TAUNTER,
    REQ_CAN_BE_ATTACKED,
    REQ_ACTION_PWR_IS_MASTER_PWR,
    REQ_TARGET_MAGNET,
    REQ_ATTACK_GREATER_THAN_0,
    REQ_ATTACKER_NOT_FROZEN,
    REQ_HERO_OR_MINION_TARGET,
    REQ_CAN_BE_TARGETED_BY_SPELLS,
    REQ_SUBCARD_IS_PLAYABLE,
    REQ_TARGET_FOR_NO_COMBO,
    REQ_NOT_MINION_JUST_PLAYED,
    REQ_NOT_EXHAUSTED_HERO_POWER,
    REQ_CAN_BE_TARGETED_BY_OPPONENTS,
    REQ_ATTACKER_CAN_ATTACK,
    REQ_TARGET_MIN_ATTACK,
    REQ_CAN_BE_TARGETED_BY_HERO_POWERS,
    REQ_ENEMY_TARGET_NOT_IMMUNE,
    REQ_ENTIRE_ENTOURAGE_NOT_IN_PLAY,
    REQ_MINIMUM_TOTAL_MINIONS,
    REQ_MUST_TARGET_TAUNTER,
    REQ_UNDAMAGED_TARGET,
    REQ_CAN_BE_TARGETED_BY_BATTLECRIES,
    REQ_STEADY_SHOT,
    REQ_MINION_OR_ENEMY_HERO,
    REQ_TARGET_IF_AVAILABLE_AND_DRAGON_IN_HAND,
    REQ_LEGENDARY_TARGET,
    REQ_FRIENDLY_MINION_DIED_THIS_TURN,
    REQ_FRIENDLY_MINION_DIED_THIS_GAME,
    REQ_ENEMY_WEAPON_EQUIPPED,
    REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_MINIONS,
    REQ_TARGET_WITH_BATTLECRY,
    REQ_TARGET_WITH_DEATHRATTLE,
    REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_SECRETS,
    REQ_SECRET_CAP_FOR_NON_SECRET,
    REQ_TARGET_EXACT_COST,
    REQ_STEALTHED_TARGET,
    REQ_MINION_SLOT_OR_MANA_CRYSTAL_SLOT,
    REQ_DRAG_TO_PLAY,
  }

  public class PlayRequirementInfo
  {
    public ulong requirementsMap;
    public int paramMinAtk;
    public int paramMaxAtk;
    public int paramRace;
    public int paramNumMinionSlots;
    public int paramNumMinionSlotsWithTarget;
    public int paramMinNumTotalMinions;
    public int paramMinNumFriendlyMinions;
    public int paramMinNumEnemyMinions;
    public int paramMinNumFriendlySecrets;
    public int paramExactCost;

    public PlayRequirementInfo()
    {
      this.requirementsMap = 0UL;
      this.paramMinAtk = 0;
      this.paramMaxAtk = 0;
      this.paramRace = 0;
      this.paramNumMinionSlots = 0;
      this.paramNumMinionSlotsWithTarget = 0;
      this.paramMinNumTotalMinions = 0;
      this.paramMinNumFriendlyMinions = 0;
      this.paramMinNumEnemyMinions = 0;
      this.paramMinNumFriendlySecrets = 0;
      this.paramExactCost = 0;
    }
  }

  public class TargetEntityInfo
  {
    public int id;
    public int owningPlayerID;
    public int damage;
    public int attack;
    public int cost;
    public int race;
    public int rarity;
    public TAG_CARDTYPE cardType;
    public bool isImmune;
    public bool canBeAttacked;
    public bool canBeTargetedByOpponents;
    public bool canBeTargetedBySpells;
    public bool canBeTargetedByHeroPowers;
    public bool canBeTargetedByBattlecries;
    public bool isFrozen;
    public bool isEnchanted;
    public bool isStealthed;
    public bool isTaunter;
    public bool isMagnet;
    public bool hasCharge;
    public bool hasAttackedThisTurn;
    public bool hasBattlecry;
    public bool hasDeathrattle;

    public TargetEntityInfo()
    {
      this.id = 0;
      this.owningPlayerID = 0;
      this.damage = 0;
      this.attack = 0;
      this.cost = 0;
      this.race = 0;
      this.rarity = 0;
      this.cardType = TAG_CARDTYPE.MINION;
      this.isImmune = false;
      this.canBeAttacked = true;
      this.canBeTargetedByOpponents = true;
      this.canBeTargetedBySpells = true;
      this.canBeTargetedByHeroPowers = true;
      this.canBeTargetedByBattlecries = true;
      this.isFrozen = false;
      this.isEnchanted = false;
      this.isStealthed = false;
      this.isTaunter = false;
      this.isMagnet = false;
      this.hasCharge = false;
      this.hasAttackedThisTurn = false;
      this.hasBattlecry = false;
      this.hasDeathrattle = false;
    }
  }

  public class SourceEntityInfo
  {
    public ulong requirementsMap;
    public int id;
    public int cost;
    public int attack;
    public int minAttackRequirement;
    public int maxAttackRequirement;
    public int exactCostRequirement;
    public int raceRequirement;
    public int numMinionSlotsRequirement;
    public int numMinionSlotsWithTargetRequirement;
    public int minTotalMinionsRequirement;
    public int minFriendlyMinionsRequirement;
    public int minEnemyMinionsRequirement;
    public int numTurnsInPlay;
    public int numAttacksThisTurn;
    public int numAttacksAllowedThisTurn;
    public int minFriendlySecretsRequirement;
    public TAG_CARDTYPE cardType;
    public TAG_ZONE zone;
    public bool isSecret;
    public bool isDuplicateSecret;
    public bool isExhausted;
    public bool isMasterPower;
    public bool isActionPower;
    public bool isActivatePower;
    public bool isAttackPower;
    public bool isFrozen;
    public bool hasBattlecry;
    public bool canAttack;
    public bool entireEntourageInPlay;
    public bool hasCharge;
    public bool isChoiceMinion;
    public bool cannotAttackHeroes;
    public bool costsHealth;

    public SourceEntityInfo()
    {
      this.requirementsMap = 0UL;
      this.id = 0;
      this.cost = 0;
      this.attack = 0;
      this.minAttackRequirement = 0;
      this.maxAttackRequirement = 0;
      this.exactCostRequirement = 0;
      this.raceRequirement = 0;
      this.numMinionSlotsRequirement = 0;
      this.numMinionSlotsWithTargetRequirement = 0;
      this.minTotalMinionsRequirement = 0;
      this.minFriendlyMinionsRequirement = 0;
      this.minEnemyMinionsRequirement = 0;
      this.numTurnsInPlay = 0;
      this.numAttacksThisTurn = 0;
      this.numAttacksAllowedThisTurn = 1;
      this.minFriendlySecretsRequirement = 0;
      this.cardType = TAG_CARDTYPE.MINION;
      this.zone = TAG_ZONE.SETASIDE;
      this.isSecret = false;
      this.isDuplicateSecret = false;
      this.isExhausted = false;
      this.isMasterPower = false;
      this.isActionPower = false;
      this.isActivatePower = false;
      this.isAttackPower = false;
      this.isFrozen = false;
      this.hasBattlecry = false;
      this.canAttack = true;
      this.entireEntourageInPlay = false;
      this.hasCharge = false;
      this.isChoiceMinion = false;
      this.cannotAttackHeroes = false;
      this.costsHealth = false;
    }
  }

  public class PlayerInfo
  {
    public int id;
    public int numResources;
    public int numPermanentResources;
    public int numMaxPermanentResources;
    public int numTouchableFriendlyMinionsInPlay;
    public int numTouchableEnemyMinionsInPlay;
    public int numTotalFriendlyMinionsInPlay;
    public int numMinionSlotsPerPlayer;
    public int numSecretSlotsPerPlayer;
    public int numDragonsInHand;
    public int numFriendlyMinionsThatDiedThisTurn;
    public int numFriendlyMinionsThatDiedThisGame;
    public int currentDefense;
    public int numFriendlySecretsInPlay;
    public bool isCurrentPlayer;
    public bool weaponEquipped;
    public bool enemyWeaponEquipped;
    public bool comboActive;
    public bool steadyShotRequiresTarget;
    public bool spellsCostHealth;

    public PlayerInfo()
    {
      this.id = 0;
      this.numResources = 0;
      this.numPermanentResources = 0;
      this.numMaxPermanentResources = 0;
      this.numTouchableFriendlyMinionsInPlay = 0;
      this.numTouchableEnemyMinionsInPlay = 0;
      this.numTotalFriendlyMinionsInPlay = 0;
      this.numMinionSlotsPerPlayer = 0;
      this.numSecretSlotsPerPlayer = 0;
      this.numDragonsInHand = 0;
      this.numFriendlyMinionsThatDiedThisTurn = 0;
      this.numFriendlyMinionsThatDiedThisGame = 0;
      this.currentDefense = 0;
      this.numFriendlySecretsInPlay = 0;
      this.isCurrentPlayer = false;
      this.weaponEquipped = false;
      this.enemyWeaponEquipped = false;
      this.comboActive = false;
      this.steadyShotRequiresTarget = false;
      this.spellsCostHealth = false;
    }
  }

  public class GameStateInfo
  {
    public TAG_STEP currentStep;

    public GameStateInfo()
    {
      this.currentStep = TAG_STEP.MAIN_BEGIN;
    }
  }

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  private struct Marshaled_TargetEntityInfo
  {
    public int id;
    public int owningPlayerID;
    public int damage;
    public int attack;
    public int cost;
    public int race;
    public int rarity;
    [MarshalAs(UnmanagedType.U4)]
    public TAG_CARDTYPE cardType;
    [MarshalAs(UnmanagedType.U1)]
    public bool isImmune;
    [MarshalAs(UnmanagedType.U1)]
    public bool canBeAttacked;
    [MarshalAs(UnmanagedType.U1)]
    public bool canBeTargetedByOpponents;
    [MarshalAs(UnmanagedType.U1)]
    public bool canBeTargetedBySpells;
    [MarshalAs(UnmanagedType.U1)]
    public bool canBeTargetedByHeroPowers;
    [MarshalAs(UnmanagedType.U1)]
    public bool canBeTargetedByBattlecries;
    [MarshalAs(UnmanagedType.U1)]
    public bool isFrozen;
    [MarshalAs(UnmanagedType.U1)]
    public bool isEnchanted;
    [MarshalAs(UnmanagedType.U1)]
    public bool isStealthed;
    [MarshalAs(UnmanagedType.U1)]
    public bool isTaunter;
    [MarshalAs(UnmanagedType.U1)]
    public bool isMagnet;
    [MarshalAs(UnmanagedType.U1)]
    public bool hasCharge;
    [MarshalAs(UnmanagedType.U1)]
    public bool hasAttackedThisTurn;
    [MarshalAs(UnmanagedType.U1)]
    public bool hasBattlecry;
    [MarshalAs(UnmanagedType.U1)]
    public bool hasDeathrattle;

    public static PlayErrors.Marshaled_TargetEntityInfo ConvertFromTargetEntityInfo(PlayErrors.TargetEntityInfo targetInfo)
    {
      return new PlayErrors.Marshaled_TargetEntityInfo() { id = targetInfo.id, owningPlayerID = targetInfo.owningPlayerID, damage = targetInfo.damage, attack = targetInfo.attack, cost = targetInfo.cost, cardType = targetInfo.cardType, race = targetInfo.race, rarity = targetInfo.rarity, isImmune = targetInfo.isImmune, canBeAttacked = targetInfo.canBeAttacked, canBeTargetedByOpponents = targetInfo.canBeTargetedByOpponents, canBeTargetedBySpells = targetInfo.canBeTargetedBySpells, canBeTargetedByHeroPowers = targetInfo.canBeTargetedByHeroPowers, canBeTargetedByBattlecries = targetInfo.canBeTargetedByBattlecries, isFrozen = targetInfo.isFrozen, isEnchanted = targetInfo.isEnchanted, isStealthed = targetInfo.isStealthed, isTaunter = targetInfo.isTaunter, isMagnet = targetInfo.isMagnet, hasCharge = targetInfo.hasCharge, hasAttackedThisTurn = targetInfo.hasAttackedThisTurn, hasBattlecry = targetInfo.hasBattlecry, hasDeathrattle = targetInfo.hasDeathrattle };
    }
  }

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  private struct Marshaled_SourceEntityInfo
  {
    public ulong requirementsMap;
    public int id;
    public int cost;
    public int attack;
    public int minAttackRequirement;
    public int maxAttackRequirement;
    public int exactCostRequirement;
    public int raceRequirement;
    public int numMinionSlotsRequirement;
    public int numMinionSlotsWithTargetRequirement;
    public int minTotalMinionsRequirement;
    public int minFriendlyMinionsRequirement;
    public int minEnemyMinionsRequirement;
    public int numTurnsInPlay;
    public int numAttacksThisTurn;
    public int numAttacksAllowedThisTurn;
    public int minFriendlySecretsRequirement;
    [MarshalAs(UnmanagedType.U4)]
    public TAG_CARDTYPE cardType;
    [MarshalAs(UnmanagedType.U4)]
    public TAG_ZONE zone;
    [MarshalAs(UnmanagedType.U1)]
    public bool isSecret;
    [MarshalAs(UnmanagedType.U1)]
    public bool isDuplicateSecret;
    [MarshalAs(UnmanagedType.U1)]
    public bool isExhausted;
    [MarshalAs(UnmanagedType.U1)]
    public bool isMasterPower;
    [MarshalAs(UnmanagedType.U1)]
    public bool isActionPower;
    [MarshalAs(UnmanagedType.U1)]
    public bool isActivatePower;
    [MarshalAs(UnmanagedType.U1)]
    public bool isAttackPower;
    [MarshalAs(UnmanagedType.U1)]
    public bool isFrozen;
    [MarshalAs(UnmanagedType.U1)]
    public bool hasBattlecry;
    [MarshalAs(UnmanagedType.U1)]
    public bool canAttack;
    [MarshalAs(UnmanagedType.U1)]
    public bool entireEntourageInPlay;
    [MarshalAs(UnmanagedType.U1)]
    public bool hasCharge;
    [MarshalAs(UnmanagedType.U1)]
    public bool isChoiceMinion;
    [MarshalAs(UnmanagedType.U1)]
    public bool cannotAttackHeroes;
    [MarshalAs(UnmanagedType.U1)]
    public bool costsHealth;

    public static PlayErrors.Marshaled_SourceEntityInfo ConvertFromSourceEntityInfo(PlayErrors.SourceEntityInfo sourceInfo)
    {
      return new PlayErrors.Marshaled_SourceEntityInfo() { requirementsMap = sourceInfo.requirementsMap, id = sourceInfo.id, cost = sourceInfo.cost, attack = sourceInfo.attack, minAttackRequirement = sourceInfo.minAttackRequirement, maxAttackRequirement = sourceInfo.maxAttackRequirement, exactCostRequirement = sourceInfo.exactCostRequirement, raceRequirement = sourceInfo.raceRequirement, numMinionSlotsRequirement = sourceInfo.numMinionSlotsRequirement, numMinionSlotsWithTargetRequirement = sourceInfo.numMinionSlotsWithTargetRequirement, minTotalMinionsRequirement = sourceInfo.minTotalMinionsRequirement, minFriendlyMinionsRequirement = sourceInfo.minFriendlyMinionsRequirement, minEnemyMinionsRequirement = sourceInfo.minEnemyMinionsRequirement, numTurnsInPlay = sourceInfo.numTurnsInPlay, numAttacksThisTurn = sourceInfo.numAttacksThisTurn, numAttacksAllowedThisTurn = sourceInfo.numAttacksAllowedThisTurn, minFriendlySecretsRequirement = sourceInfo.minFriendlySecretsRequirement, cardType = sourceInfo.cardType, zone = sourceInfo.zone, isSecret = sourceInfo.isSecret, isDuplicateSecret = sourceInfo.isDuplicateSecret, isExhausted = sourceInfo.isExhausted, isMasterPower = sourceInfo.isMasterPower, isActionPower = sourceInfo.isActionPower, isActivatePower = sourceInfo.isActivatePower, isAttackPower = sourceInfo.isAttackPower, isFrozen = sourceInfo.isFrozen, hasBattlecry = sourceInfo.hasBattlecry, canAttack = sourceInfo.canAttack, entireEntourageInPlay = sourceInfo.entireEntourageInPlay, hasCharge = sourceInfo.hasCharge, isChoiceMinion = sourceInfo.isChoiceMinion, cannotAttackHeroes = sourceInfo.cannotAttackHeroes, costsHealth = sourceInfo.costsHealth };
    }
  }

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  private struct Marshaled_PlayerInfo
  {
    public int id;
    public int numResources;
    public int numPermanentResources;
    public int numMaxPermanentResources;
    public int numTouchableFriendlyMinionsInPlay;
    public int numTouchableEnemyMinionsInPlay;
    public int numTotalFriendlyMinionsInPlay;
    public int numMinionSlotsPerPlayer;
    public int numSecretSlotsPerPlayer;
    public int numDragonsInHand;
    public int numFriendlyMinionsThatDiedThisTurn;
    public int numFriendlyMinionsThatDiedThisGame;
    public int currentDefense;
    public int numFriendlySecretsInPlay;
    [MarshalAs(UnmanagedType.U1)]
    public bool isCurrentPlayer;
    [MarshalAs(UnmanagedType.U1)]
    public bool weaponEquipped;
    [MarshalAs(UnmanagedType.U1)]
    public bool enemyWeaponEquipped;
    [MarshalAs(UnmanagedType.U1)]
    public bool comboActive;
    [MarshalAs(UnmanagedType.U1)]
    public bool steadyShotRequiresTarget;
    [MarshalAs(UnmanagedType.U1)]
    public bool spellsCostHealth;

    public static PlayErrors.Marshaled_PlayerInfo ConvertFromPlayerInfo(PlayErrors.PlayerInfo playerInfo)
    {
      return new PlayErrors.Marshaled_PlayerInfo() { id = playerInfo.id, numResources = playerInfo.numResources, numPermanentResources = playerInfo.numPermanentResources, numMaxPermanentResources = playerInfo.numMaxPermanentResources, numTouchableFriendlyMinionsInPlay = playerInfo.numTouchableFriendlyMinionsInPlay, numTouchableEnemyMinionsInPlay = playerInfo.numTouchableEnemyMinionsInPlay, numTotalFriendlyMinionsInPlay = playerInfo.numTotalFriendlyMinionsInPlay, numMinionSlotsPerPlayer = playerInfo.numMinionSlotsPerPlayer, numSecretSlotsPerPlayer = playerInfo.numSecretSlotsPerPlayer, numDragonsInHand = playerInfo.numDragonsInHand, numFriendlyMinionsThatDiedThisTurn = playerInfo.numFriendlyMinionsThatDiedThisTurn, numFriendlyMinionsThatDiedThisGame = playerInfo.numFriendlyMinionsThatDiedThisGame, currentDefense = playerInfo.currentDefense, numFriendlySecretsInPlay = playerInfo.numFriendlySecretsInPlay, isCurrentPlayer = playerInfo.isCurrentPlayer, weaponEquipped = playerInfo.weaponEquipped, enemyWeaponEquipped = playerInfo.enemyWeaponEquipped, comboActive = playerInfo.comboActive, steadyShotRequiresTarget = playerInfo.steadyShotRequiresTarget, spellsCostHealth = playerInfo.spellsCostHealth };
    }
  }

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  private struct Marshaled_GameStateInfo
  {
    [MarshalAs(UnmanagedType.U4)]
    public TAG_STEP currentStep;

    public static PlayErrors.Marshaled_GameStateInfo ConvertFromGameStateInfo(PlayErrors.GameStateInfo gameInfo)
    {
      return new PlayErrors.Marshaled_GameStateInfo() { currentStep = gameInfo.currentStep };
    }
  }

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  private struct Marshaled_PlayErrorsParams
  {
    [MarshalAs(UnmanagedType.Struct)]
    public PlayErrors.Marshaled_SourceEntityInfo source;
    [MarshalAs(UnmanagedType.Struct)]
    public PlayErrors.Marshaled_PlayerInfo player;
    [MarshalAs(UnmanagedType.U4)]
    public PlayErrors.Marshaled_GameStateInfo game;

    public Marshaled_PlayErrorsParams(PlayErrors.SourceEntityInfo sourceInfo, PlayErrors.PlayerInfo playerInfo, PlayErrors.GameStateInfo gameInfo)
    {
      this.source = PlayErrors.Marshaled_SourceEntityInfo.ConvertFromSourceEntityInfo(sourceInfo);
      this.player = PlayErrors.Marshaled_PlayerInfo.ConvertFromPlayerInfo(playerInfo);
      this.game = PlayErrors.Marshaled_GameStateInfo.ConvertFromGameStateInfo(gameInfo);
    }
  }

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate bool DelPlayErrorsInit();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate ulong DelGetRequirementsMap(PlayErrors.ErrorType[] requirements, int numRequirements);

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate PlayErrors.ErrorType DelGetPlayEntityError(PlayErrors.Marshaled_PlayErrorsParams playErrorsParams, PlayErrors.Marshaled_SourceEntityInfo[] subCards, int numSubCards, PlayErrors.Marshaled_TargetEntityInfo[] enititiesInPlay, int numEntitiesInPlay);

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate PlayErrors.ErrorType DelGetTargetEntityError(PlayErrors.Marshaled_PlayErrorsParams playErrorsParams, PlayErrors.Marshaled_TargetEntityInfo target, PlayErrors.Marshaled_TargetEntityInfo[] entitiesInPlay, int numEntitiesInPlay);
}
