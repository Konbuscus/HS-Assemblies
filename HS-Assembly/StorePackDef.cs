﻿// Decompiled with JetBrains decompiler
// Type: StorePackDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class StorePackDef : MonoBehaviour
{
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_buttonPrefab;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_lowPolyPrefab;
  [CustomEditField(T = EditType.TEXTURE)]
  public string m_logoTextureName;
  [CustomEditField(T = EditType.TEXTURE)]
  public string m_logoTextureGlowName;
  [CustomEditField(T = EditType.TEXTURE)]
  public string m_accentTextureName;
  public Material m_background;
  public MusicPlaylistType m_playlist;
  public string m_preorderAvailableDateString;
}
