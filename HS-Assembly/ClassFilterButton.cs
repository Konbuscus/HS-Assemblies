﻿// Decompiled with JetBrains decompiler
// Type: ClassFilterButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ClassFilterButton : PegUIElement
{
  public GameObject m_newCardCount;
  public UberText m_newCardCountText;
  public GameObject m_disabled;
  public CollectionManagerDisplay.ViewMode m_tabViewMode;
  private TAG_CLASS? m_class;

  protected override void Awake()
  {
    this.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.HandleRelease()));
    base.Awake();
  }

  public void HandleRelease()
  {
    switch (this.m_tabViewMode)
    {
      case CollectionManagerDisplay.ViewMode.CARDS:
        if (this.m_class.HasValue)
        {
          CollectionManagerDisplay.Get().m_pageManager.JumpToCollectionClassPage(this.m_class.Value);
          break;
        }
        break;
      case CollectionManagerDisplay.ViewMode.HERO_SKINS:
        CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.HERO_SKINS, (CollectionManagerDisplay.ViewModeData) null);
        break;
      case CollectionManagerDisplay.ViewMode.CARD_BACKS:
        CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.CARD_BACKS, (CollectionManagerDisplay.ViewModeData) null);
        break;
    }
    this.GetComponentInParent<SlidingTray>().HideTray();
  }

  public void SetClass(TAG_CLASS? classTag, Material material)
  {
    this.m_class = classTag;
    this.GetComponent<Renderer>().material = material;
    bool flag = !this.m_class.HasValue;
    this.GetComponent<Renderer>().enabled = !flag;
    if ((Object) this.m_newCardCount != (Object) null)
      this.m_newCardCount.SetActive(!flag);
    if (!((Object) this.m_disabled != (Object) null))
      return;
    this.m_disabled.SetActive(flag);
  }

  public void SetNewCardCount(int count)
  {
    if ((Object) this.m_newCardCount != (Object) null)
      this.m_newCardCount.SetActive(count > 0);
    if (count <= 0 || !((Object) this.m_newCardCountText != (Object) null))
      return;
    this.m_newCardCountText.Text = GameStrings.Format("GLUE_COLLECTION_NEW_CARD_CALLOUT", (object) count);
  }
}
