﻿// Decompiled with JetBrains decompiler
// Type: SpellTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class SpellTable : MonoBehaviour
{
  public List<SpellTableEntry> m_Table = new List<SpellTableEntry>();

  public SpellTableEntry FindEntry(SpellType type)
  {
    using (List<SpellTableEntry>.Enumerator enumerator = this.m_Table.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpellTableEntry current = enumerator.Current;
        if (current.m_Type == type)
          return current;
      }
    }
    return (SpellTableEntry) null;
  }

  public Spell GetSpell(SpellType spellType)
  {
    using (List<SpellTableEntry>.Enumerator enumerator = this.m_Table.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpellTableEntry current = enumerator.Current;
        if (current.m_Type == spellType)
        {
          if ((Object) current.m_Spell == (Object) null && current.m_SpellPrefabName != null)
          {
            GameObject gameObject = AssetLoader.Get().LoadActor(FileUtils.GameAssetPathToName(current.m_SpellPrefabName), true, true);
            Spell component = gameObject.GetComponent<Spell>();
            if ((Object) component != (Object) null)
            {
              current.m_Spell = component;
              TransformUtil.AttachAndPreserveLocalTransform(gameObject.transform, this.gameObject.transform);
            }
          }
          if ((Object) current.m_Spell == (Object) null)
          {
            Debug.LogError((object) ("Unable to load spell " + (object) spellType + " from spell table " + this.gameObject.name));
            return (Spell) null;
          }
          Spell component1 = Object.Instantiate<GameObject>(current.m_Spell.gameObject).GetComponent<Spell>();
          component1.SetSpellType(spellType);
          return component1;
        }
      }
    }
    return (Spell) null;
  }

  public void ReleaseSpell(GameObject spellObject)
  {
    Object.Destroy((Object) spellObject);
  }

  public void ReleaseAllSpells()
  {
    using (List<SpellTableEntry>.Enumerator enumerator = this.m_Table.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpellTableEntry current = enumerator.Current;
        if ((Object) current.m_Spell != (Object) null)
        {
          Object.DestroyImmediate((Object) current.m_Spell.gameObject);
          Object.DestroyImmediate((Object) current.m_Spell);
          current.m_Spell = (Spell) null;
        }
      }
    }
  }

  public bool IsLoaded(SpellType spellType)
  {
    Spell spell;
    this.FindSpell(spellType, out spell);
    return (Object) spell != (Object) null;
  }

  public void SetSpell(SpellType type, Spell spell)
  {
    using (List<SpellTableEntry>.Enumerator enumerator = this.m_Table.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpellTableEntry current = enumerator.Current;
        if (current.m_Type == type)
        {
          if (!((Object) current.m_Spell == (Object) null))
            return;
          current.m_Spell = spell;
          TransformUtil.AttachAndPreserveLocalTransform(spell.gameObject.transform, this.gameObject.transform);
          return;
        }
      }
    }
    Debug.LogError((object) ("Set invalid spell type " + (object) type + " in spell table " + this.gameObject.name));
  }

  public bool FindSpell(SpellType spellType, out Spell spell)
  {
    using (List<SpellTableEntry>.Enumerator enumerator = this.m_Table.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpellTableEntry current = enumerator.Current;
        if (current.m_Type == spellType)
        {
          spell = current.m_Spell;
          return true;
        }
      }
    }
    spell = (Spell) null;
    return false;
  }

  public void Show()
  {
    using (List<SpellTableEntry>.Enumerator enumerator = this.m_Table.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpellTableEntry current = enumerator.Current;
        if (!((Object) current.m_Spell == (Object) null) && current.m_Type != SpellType.NONE)
          current.m_Spell.Show();
      }
    }
  }

  public void Hide()
  {
    using (List<SpellTableEntry>.Enumerator enumerator = this.m_Table.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpellTableEntry current = enumerator.Current;
        if (!((Object) current.m_Spell == (Object) null))
          current.m_Spell.Hide();
      }
    }
  }
}
