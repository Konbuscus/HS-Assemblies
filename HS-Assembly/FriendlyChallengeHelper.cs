﻿// Decompiled with JetBrains decompiler
// Type: FriendlyChallengeHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FriendlyChallengeHelper
{
  private static FriendlyChallengeHelper s_instance;
  private AlertPopup m_friendChallengeWaitingPopup;

  public static FriendlyChallengeHelper Get()
  {
    if (FriendlyChallengeHelper.s_instance == null)
      FriendlyChallengeHelper.s_instance = new FriendlyChallengeHelper();
    return FriendlyChallengeHelper.s_instance;
  }

  public void StartChallengeOrWaitForOpponent(string waitingDialogText, AlertPopup.ResponseCallback waitingCallback)
  {
    if (FriendChallengeMgr.Get().DidOpponentSelectDeck())
      return;
    this.ShowFriendChallengeWaitingForOpponentDialog(waitingDialogText, waitingCallback);
  }

  public void HideFriendChallengeWaitingForOpponentDialog()
  {
    if ((Object) this.m_friendChallengeWaitingPopup == (Object) null)
      return;
    this.m_friendChallengeWaitingPopup.Hide();
    this.m_friendChallengeWaitingPopup = (AlertPopup) null;
  }

  public void WaitForFriendChallengeToStart()
  {
    GameMgr.Get().WaitForFriendChallengeToStart(FriendChallengeMgr.Get().GetFormatType(), FriendChallengeMgr.Get().GetScenarioId());
  }

  public void StopWaitingForFriendChallenge()
  {
    this.HideFriendChallengeWaitingForOpponentDialog();
  }

  private void ShowFriendChallengeWaitingForOpponentDialog(string dialogText, AlertPopup.ResponseCallback callback)
  {
    BnetPlayer myOpponent = FriendChallengeMgr.Get().GetMyOpponent();
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_text = GameStrings.Format(dialogText, (object) FriendUtils.GetUniqueName(myOpponent)),
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.CANCEL,
      m_responseCallback = callback
    }, new DialogManager.DialogProcessCallback(this.OnFriendChallengeWaitingForOpponentDialogProcessed));
  }

  private bool OnFriendChallengeWaitingForOpponentDialogProcessed(DialogBase dialog, object userData)
  {
    if (!FriendChallengeMgr.Get().HasChallenge() || FriendChallengeMgr.Get().DidOpponentSelectDeck())
      return false;
    this.m_friendChallengeWaitingPopup = (AlertPopup) dialog;
    return true;
  }
}
