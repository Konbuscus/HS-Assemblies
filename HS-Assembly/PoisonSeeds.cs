﻿// Decompiled with JetBrains decompiler
// Type: PoisonSeeds
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PoisonSeeds : SuperSpell
{
  public float m_StartDeathSpellAdjustment = 0.01f;
  public Spell m_CustomSpawnSpell;
  public Spell m_CustomDeathSpell;
  public AnimationCurve m_HeightCurve;
  public float m_RotationDriftAmount;
  public AnimationCurve m_RotationDriftCurve;
  public ParticleSystem m_ImpactParticles;
  public ParticleSystem m_DustParticles;
  private PoisonSeeds.SpellTargetType m_TargetType;
  private float m_HeightCurveLength;
  private float m_AnimTime;
  private AudioSource m_Sound;

  protected override void Awake()
  {
    this.m_Sound = this.GetComponent<AudioSource>();
    base.Awake();
  }

  public override bool AddPowerTargets()
  {
    this.m_visualToTargetIndexMap.Clear();
    this.m_targetToMetaDataMap.Clear();
    this.m_targets.Clear();
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    for (int index = 0; index < taskList.Count; ++index)
    {
      PowerTask task = taskList[index];
      Card cardFromPowerTask = this.GetTargetCardFromPowerTask(index, task);
      if (!((Object) cardFromPowerTask == (Object) null))
        this.m_targets.Add(cardFromPowerTask.gameObject);
    }
    return this.m_targets.Count > 0;
  }

  protected override Card GetTargetCardFromPowerTask(int index, PowerTask task)
  {
    Network.PowerHistory power = task.GetPower();
    int id;
    if (power.Type == Network.PowerType.FULL_ENTITY)
    {
      this.m_TargetType = PoisonSeeds.SpellTargetType.Create;
      id = (power as Network.HistFullEntity).Entity.ID;
    }
    else
    {
      Network.HistTagChange histTagChange = power as Network.HistTagChange;
      if (histTagChange == null || histTagChange.Tag != 360)
        return (Card) null;
      this.m_TargetType = PoisonSeeds.SpellTargetType.Death;
      id = histTagChange.Entity;
    }
    Entity entity = GameState.Get().GetEntity(id);
    if (entity != null)
      return entity.GetCard();
    UnityEngine.Debug.LogWarning((object) string.Format("{0}.GetTargetCardFromPowerTask() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) id));
    return (Card) null;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    if (this.m_TargetType == PoisonSeeds.SpellTargetType.Death)
      this.DeathEffect();
    else if (this.m_TargetType == PoisonSeeds.SpellTargetType.Create)
    {
      this.StartCoroutine(this.CreateEffect());
    }
    else
    {
      --this.m_effectsPendingFinish;
      this.FinishIfPossible();
    }
  }

  [DebuggerHidden]
  private IEnumerator CreateEffect()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PoisonSeeds.\u003CCreateEffect\u003Ec__Iterator2C7() { \u003C\u003Ef__this = this };
  }

  private void DeathEffect()
  {
    if (this.m_HeightCurve.length == 0)
      UnityEngine.Debug.LogWarning((object) "PoisonSeeds Spell height animation curve in not defined");
    else if (this.m_RotationDriftCurve.length == 0)
    {
      UnityEngine.Debug.LogWarning((object) "PoisonSeeds Spell rotation drift animation curve in not defined");
    }
    else
    {
      if ((Object) this.m_CustomDeathSpell != (Object) null)
      {
        using (List<GameObject>.Enumerator enumerator = this.GetTargets().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            GameObject current = enumerator.Current;
            if (!((Object) current == (Object) null))
              current.GetComponent<Card>().OverrideCustomDeathSpell(Object.Instantiate<Spell>(this.m_CustomDeathSpell));
          }
        }
      }
      this.m_HeightCurveLength = this.m_HeightCurve.keys[this.m_HeightCurve.length - 1].time;
      List<PoisonSeeds.MinionData> minions = new List<PoisonSeeds.MinionData>();
      using (List<GameObject>.Enumerator enumerator = this.GetTargets().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GameObject current = enumerator.Current;
          PoisonSeeds.MinionData minionData = new PoisonSeeds.MinionData();
          minionData.card = current.GetComponent<Card>();
          minionData.gameObject = current;
          minionData.orgLocPos = current.transform.localPosition;
          minionData.orgLocRot = current.transform.localRotation;
          float x = Mathf.Lerp(-this.m_RotationDriftAmount, this.m_RotationDriftAmount, Random.value);
          float y = Mathf.Lerp(-this.m_RotationDriftAmount, this.m_RotationDriftAmount, Random.value) * 0.1f;
          float z = Mathf.Lerp(-this.m_RotationDriftAmount, this.m_RotationDriftAmount, Random.value);
          minionData.rotationDrift = new Vector3(x, y, z);
          minions.Add(minionData);
        }
      }
      this.StartCoroutine(this.AnimateDeathEffect(minions));
    }
  }

  [DebuggerHidden]
  private IEnumerator AnimateDeathEffect(List<PoisonSeeds.MinionData> minions)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PoisonSeeds.\u003CAnimateDeathEffect\u003Ec__Iterator2C8() { minions = minions, \u003C\u0024\u003Eminions = minions, \u003C\u003Ef__this = this };
  }

  private void ShakeCamera()
  {
    CameraShakeMgr.Shake(Camera.main, new Vector3(0.15f, 0.15f, 0.15f), 0.9f);
  }

  public class MinionData
  {
    public GameObject gameObject;
    public Vector3 orgLocPos;
    public Quaternion orgLocRot;
    public Vector3 rotationDrift;
    public Card card;
  }

  private enum SpellTargetType
  {
    None,
    Death,
    Create,
  }
}
