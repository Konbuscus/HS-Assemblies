﻿// Decompiled with JetBrains decompiler
// Type: DemoMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DemoMgr
{
  private const bool LOAD_STORED_SETTING = false;
  private static DemoMgr s_instance;
  private DemoMode m_mode;
  private Notification m_demoText;
  private bool m_shouldGiveArenaInstruction;
  private bool m_nextTipUnclickable;
  private bool m_nextDemoTipIsNewArenaMatch;

  public static DemoMgr Get()
  {
    if (DemoMgr.s_instance == null)
      DemoMgr.s_instance = new DemoMgr();
    ApplicationMgr.Get().WillReset += new Action(DemoMgr.s_instance.WillReset);
    return DemoMgr.s_instance;
  }

  private string GetStoredGameMode()
  {
    return (string) null;
  }

  public void Initialize()
  {
    this.SetModeFromString(this.GetStoredGameMode() ?? Vars.Key("Demo.Mode").GetStr("NONE"));
  }

  public bool IsDemo()
  {
    return this.m_mode != DemoMode.NONE;
  }

  public bool IsExpoDemo()
  {
    switch (this.m_mode)
    {
      case DemoMode.PAX_EAST_2013:
      case DemoMode.GAMESCOM_2013:
      case DemoMode.BLIZZCON_2013:
      case DemoMode.BLIZZCON_2014:
      case DemoMode.BLIZZCON_2015:
      case DemoMode.APPLE_STORE:
      case DemoMode.ANNOUNCEMENT_5_0:
      case DemoMode.BLIZZCON_2016:
        return true;
      default:
        return false;
    }
  }

  public bool IsSocialEnabled()
  {
    switch (this.m_mode)
    {
      case DemoMode.BLIZZCON_2013:
      case DemoMode.BLIZZCON_2015:
      case DemoMode.APPLE_STORE:
        return false;
      default:
        return true;
    }
  }

  public bool IsCurrencyEnabled()
  {
    switch (this.m_mode)
    {
      case DemoMode.BLIZZCON_2013:
      case DemoMode.BLIZZCON_2014:
      case DemoMode.BLIZZCON_2015:
      case DemoMode.ANNOUNCEMENT_5_0:
      case DemoMode.BLIZZCON_2016:
        return false;
      default:
        return true;
    }
  }

  public bool IsHubEscMenuEnabled()
  {
    switch (this.m_mode)
    {
      case DemoMode.BLIZZCON_2013:
      case DemoMode.BLIZZCON_2014:
      case DemoMode.BLIZZCON_2015:
      case DemoMode.APPLE_STORE:
      case DemoMode.ANNOUNCEMENT_5_0:
      case DemoMode.BLIZZCON_2016:
        return false;
      default:
        return true;
    }
  }

  public bool CantExitArena()
  {
    return this.m_mode == DemoMode.BLIZZCON_2013;
  }

  public bool ArenaIs1WinMode()
  {
    return this.m_mode == DemoMode.BLIZZCON_2013;
  }

  public bool ShouldShowWelcomeQuests()
  {
    switch (this.m_mode)
    {
      case DemoMode.BLIZZCON_2013:
      case DemoMode.BLIZZCON_2014:
      case DemoMode.BLIZZCON_2015:
      case DemoMode.ANNOUNCEMENT_5_0:
      case DemoMode.BLIZZCON_2016:
        return false;
      default:
        return true;
    }
  }

  public DemoMode GetMode()
  {
    return this.m_mode;
  }

  public void SetMode(DemoMode mode)
  {
    this.m_mode = mode;
  }

  public void SetModeFromString(string modeString)
  {
    this.m_mode = this.GetModeFromString(modeString);
  }

  public DemoMode GetModeFromString(string modeString)
  {
    try
    {
      return EnumUtils.GetEnum<DemoMode>(modeString, StringComparison.OrdinalIgnoreCase);
    }
    catch (Exception ex)
    {
      return DemoMode.NONE;
    }
  }

  public void CreateDemoText(string demoText)
  {
    this.CreateDemoText(demoText, false, false);
  }

  public void CreateDemoText(string demoText, bool unclickable)
  {
    this.CreateDemoText(demoText, unclickable, false);
  }

  public void CreateDemoText(string demoText, bool unclickable, bool shouldDoArenaInstruction)
  {
    if ((UnityEngine.Object) this.m_demoText != (UnityEngine.Object) null)
      return;
    this.m_shouldGiveArenaInstruction = shouldDoArenaInstruction;
    this.m_nextTipUnclickable = unclickable;
    GameObject go = AssetLoader.Get().LoadGameObject("DemoText", true, false);
    OverlayUI.Get().AddGameObject(go, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    this.m_demoText = go.GetComponent<Notification>();
    this.m_demoText.ChangeText(demoText);
    UniversalInputManager.Get().SetSystemDialogActive(true);
    go.transform.GetComponentInChildren<PegUIElement>().AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.RemoveDemoTextDialog));
    if (!this.m_nextTipUnclickable)
      return;
    this.m_nextTipUnclickable = false;
    this.MakeDemoTextClickable(false);
  }

  public void ChangeDemoText(string demoText)
  {
    this.m_demoText.ChangeText(demoText);
  }

  public void NextDemoTipIsNewArenaMatch()
  {
    this.m_nextDemoTipIsNewArenaMatch = true;
  }

  private void DemoTextLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    this.m_demoText = actorObject.GetComponent<Notification>();
    this.m_demoText.ChangeText((string) callbackData);
    UniversalInputManager.Get().SetSystemDialogActive(true);
    actorObject.transform.GetComponentInChildren<PegUIElement>().AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.RemoveDemoTextDialog));
    if (!this.m_nextTipUnclickable)
      return;
    this.m_nextTipUnclickable = false;
    this.MakeDemoTextClickable(false);
  }

  private void RemoveDemoTextDialog(UIEvent e)
  {
    this.RemoveDemoTextDialog();
  }

  public void RemoveDemoTextDialog()
  {
    UniversalInputManager.Get().SetSystemDialogActive(false);
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_demoText.gameObject);
    if (this.m_shouldGiveArenaInstruction)
    {
      NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_FORGE_INST1_19"), "VO_INNKEEPER_FORGE_INST1_19", 3f, (Action) null, false);
      this.m_shouldGiveArenaInstruction = false;
    }
    if (!this.m_nextDemoTipIsNewArenaMatch)
      return;
    this.m_nextDemoTipIsNewArenaMatch = false;
    this.CreateDemoText(GameStrings.Get("GLUE_BLIZZCON2013_ARENA"), false, true);
  }

  public void MakeDemoTextClickable(bool clickable)
  {
    if (!clickable)
    {
      this.m_demoText.transform.GetComponentInChildren<BoxCollider>().enabled = false;
      this.m_demoText.transform.FindChild("continue").gameObject.SetActive(false);
    }
    else
    {
      this.m_demoText.transform.GetComponentInChildren<BoxCollider>().enabled = true;
      this.m_demoText.transform.FindChild("continue").gameObject.SetActive(true);
    }
  }

  public void ApplyAppleStoreDemoDefaults()
  {
    Options.Get().SetBool(Option.CONNECT_TO_AURORA, false);
    Options.Get().SetBool(Option.HAS_SEEN_CINEMATIC, true);
    Options.Get().SetEnum<TutorialProgress>(Option.LOCAL_TUTORIAL_PROGRESS, TutorialProgress.NOTHING_COMPLETE);
  }

  [DebuggerHidden]
  public IEnumerator CompleteAppleStoreDemo()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DemoMgr.\u003CCompleteAppleStoreDemo\u003Ec__Iterator306 demoCIterator306 = new DemoMgr.\u003CCompleteAppleStoreDemo\u003Ec__Iterator306();
    return (IEnumerator) demoCIterator306;
  }

  private void WillReset()
  {
    if (this.m_mode != DemoMode.APPLE_STORE)
      return;
    this.ApplyAppleStoreDemoDefaults();
  }
}
