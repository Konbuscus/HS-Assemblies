﻿// Decompiled with JetBrains decompiler
// Type: SpellLocation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum SpellLocation
{
  NONE,
  SOURCE,
  SOURCE_AUTO,
  SOURCE_HERO,
  SOURCE_HERO_POWER,
  SOURCE_PLAY_ZONE,
  TARGET,
  TARGET_AUTO,
  TARGET_HERO,
  TARGET_HERO_POWER,
  TARGET_PLAY_ZONE,
  BOARD,
  FRIENDLY_HERO,
  FRIENDLY_HERO_POWER,
  FRIENDLY_PLAY_ZONE,
  OPPONENT_HERO,
  OPPONENT_HERO_POWER,
  OPPONENT_PLAY_ZONE,
  CHOSEN_TARGET,
  FRIENDLY_HAND_ZONE,
  OPPONENT_HAND_ZONE,
  FRIENDLY_DECK_ZONE,
  OPPONENT_DECK_ZONE,
}
