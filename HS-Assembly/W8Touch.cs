﻿// Decompiled with JetBrains decompiler
// Type: W8Touch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class W8Touch : MonoBehaviour
{
  public static bool s_initialized = false;
  public static bool s_isWindows8OrGreater = false;
  private static IntPtr s_DLL = IntPtr.Zero;
  private Vector3 m_touchPosition = new Vector3(-1f, -1f, 0.0f);
  private Vector2 m_touchDelta = new Vector2(0.0f, 0.0f);
  private W8Touch.RECT m_desktopRect = new W8Touch.RECT();
  private W8Touch.PowerSource m_lastPowerSourceState = W8Touch.PowerSource.Unintialized;
  private const int MaxTouches = 5;
  private const int MaxInitializationAttempts = 10;
  private static W8Touch s_instance;
  private int m_intializationAttemptCount;
  private W8Touch.TouchState[] m_touchState;
  private bool m_isVirtualKeyboardVisible;
  private bool m_isVirtualKeyboardShowRequested;
  private bool m_isVirtualKeyboardHideRequested;
  private bool m_bWindowFeedbackSettingValue;
  private bool m_bIsWindowFeedbackDisabled;
  private static W8Touch.DelW8ShowKeyboard DLL_W8ShowKeyboard;
  private static W8Touch.DelW8HideKeyboard DLL_W8HideKeyboard;
  private static W8Touch.DelW8ShowOSK DLL_W8ShowOSK;
  private static W8Touch.DelW8Initialize DLL_W8Initialize;
  private static W8Touch.DelW8Shutdown DLL_W8Shutdown;
  private static W8Touch.DelW8GetDeviceId DLL_W8GetDeviceId;
  private static W8Touch.DelW8IsWindows8OrGreater DLL_W8IsWindows8OrGreater;
  private static W8Touch.DelW8IsLastEventFromTouch DLL_W8IsLastEventFromTouch;
  private static W8Touch.DelW8GetBatteryMode DLL_W8GetBatteryMode;
  private static W8Touch.DelW8GetPercentBatteryLife DLL_W8GetPercentBatteryLife;
  private static W8Touch.DelW8GetDesktopRect DLL_W8GetDesktopRect;
  private static W8Touch.DelW8IsVirtualKeyboardVisible DLL_W8IsVirtualKeyboardVisible;
  private static W8Touch.DelW8GetTouchPointCount DLL_W8GetTouchPointCount;
  private static W8Touch.DelW8GetTouchPoint DLL_W8GetTouchPoint;

  public event Action VirtualKeyboardDidShow;

  public event Action VirtualKeyboardDidHide;

  [DllImport("User32.dll")]
  public static extern IntPtr FindWindow(string className, string windowName);

  private void Start()
  {
    this.m_touchState = new W8Touch.TouchState[5];
    for (int index = 0; index < 5; ++index)
      this.m_touchState[index] = W8Touch.TouchState.None;
  }

  private void Awake()
  {
    W8Touch.s_instance = this;
    if (!this.LoadW8TouchDLL())
      return;
    W8Touch.s_isWindows8OrGreater = W8Touch.DLL_W8IsWindows8OrGreater();
  }

  private void Destroy()
  {
    W8Touch.s_instance = (W8Touch) null;
  }

  private void Update()
  {
    if (!W8Touch.IsInitialized())
      return;
    W8Touch.DLL_W8GetDesktopRect(out this.m_desktopRect);
    bool flag1 = W8Touch.DLL_W8IsVirtualKeyboardVisible();
    if (flag1 != this.m_isVirtualKeyboardVisible)
    {
      this.m_isVirtualKeyboardVisible = flag1;
      if (flag1 && this.VirtualKeyboardDidShow != null)
        this.VirtualKeyboardDidShow();
      else if (!flag1 && this.VirtualKeyboardDidHide != null)
        this.VirtualKeyboardDidHide();
    }
    if (this.m_isVirtualKeyboardVisible)
      this.m_isVirtualKeyboardShowRequested = false;
    else
      this.m_isVirtualKeyboardHideRequested = false;
    W8Touch.PowerSource batteryMode = this.GetBatteryMode();
    if (batteryMode != this.m_lastPowerSourceState)
    {
      Log.Yim.Print("PowerSource Change Detected: {0}", (object) batteryMode);
      this.m_lastPowerSourceState = batteryMode;
      GraphicsManager.Get().RenderQualityLevel = (GraphicsQuality) Options.Get().GetInt(Option.GFX_QUALITY);
    }
    if (!W8Touch.DLL_W8IsLastEventFromTouch() && UniversalInputManager.Get().UseWindowsTouch() || W8Touch.DLL_W8IsLastEventFromTouch() && !UniversalInputManager.Get().UseWindowsTouch())
      this.ToggleTouchMode();
    if (this.m_touchState == null)
      return;
    int num = W8Touch.DLL_W8GetTouchPointCount();
    for (int i = 0; i < 5; ++i)
    {
      W8Touch.tTouchData n = new W8Touch.tTouchData();
      bool flag2 = false;
      if (i < num)
        flag2 = W8Touch.DLL_W8GetTouchPoint(i, n);
      if (flag2 && i == 0)
      {
        Vector2 vector2 = this.TransformTouchPosition(new Vector2((float) n.m_x, (float) n.m_y));
        if ((double) this.m_touchPosition.x != -1.0 && (double) this.m_touchPosition.y != -1.0 && this.m_touchState[i] == W8Touch.TouchState.Down)
        {
          this.m_touchDelta.x = vector2.x - this.m_touchPosition.x;
          this.m_touchDelta.y = vector2.y - this.m_touchPosition.y;
        }
        else
          this.m_touchDelta.x = this.m_touchDelta.y = 0.0f;
        this.m_touchPosition.x = vector2.x;
        this.m_touchPosition.y = vector2.y;
      }
      this.m_touchState[i] = !flag2 || n.m_ID == -1 ? (this.m_touchState[i] == W8Touch.TouchState.Down || this.m_touchState[i] == W8Touch.TouchState.InitialDown ? W8Touch.TouchState.InitialUp : W8Touch.TouchState.None) : (this.m_touchState[i] == W8Touch.TouchState.Down || this.m_touchState[i] == W8Touch.TouchState.InitialDown ? W8Touch.TouchState.Down : W8Touch.TouchState.InitialDown);
    }
  }

  private void OnGUI()
  {
    if (!W8Touch.s_isWindows8OrGreater && W8Touch.s_DLL == IntPtr.Zero || W8Touch.s_initialized)
      return;
    this.InitializeDLL();
  }

  public static W8Touch Get()
  {
    return W8Touch.s_instance;
  }

  private Vector2 TransformTouchPosition(Vector2 touchInput)
  {
    Vector2 vector2 = new Vector2();
    if (Screen.fullScreen)
    {
      float num1 = (float) Screen.width / (float) Screen.height;
      float num2 = (float) this.m_desktopRect.Right / (float) this.m_desktopRect.Bottom;
      if ((double) Mathf.Abs(num1 - num2) < (double) Mathf.Epsilon)
      {
        float num3 = (float) Screen.width / (float) this.m_desktopRect.Right;
        float num4 = (float) Screen.height / (float) this.m_desktopRect.Bottom;
        vector2.x = touchInput.x * num3;
        vector2.y = ((float) this.m_desktopRect.Bottom - touchInput.y) * num4;
      }
      else if ((double) num1 < (double) num2)
      {
        float bottom = (float) this.m_desktopRect.Bottom;
        float num3 = bottom * num1;
        float num4 = (float) Screen.height / bottom;
        float num5 = (float) Screen.width / num3;
        float num6 = (float) (((double) this.m_desktopRect.Right - (double) num3) / 2.0);
        vector2.x = (touchInput.x - num6) * num5;
        vector2.y = ((float) this.m_desktopRect.Bottom - touchInput.y) * num4;
      }
      else
      {
        float right = (float) this.m_desktopRect.Right;
        float num3 = right / num1;
        float num4 = (float) Screen.height / num3;
        float num5 = (float) Screen.width / right;
        float num6 = (float) (((double) this.m_desktopRect.Bottom - (double) num3) / 2.0);
        vector2.x = touchInput.x * num5;
        vector2.y = ((float) this.m_desktopRect.Bottom - touchInput.y - num6) * num4;
      }
    }
    else
    {
      vector2.x = touchInput.x;
      vector2.y = (float) Screen.height - touchInput.y;
    }
    return vector2;
  }

  private void ToggleTouchMode()
  {
    if (!W8Touch.IsInitialized())
      return;
    Options.Get().SetBool(Option.TOUCH_MODE, !Options.Get().GetBool(Option.TOUCH_MODE));
  }

  public void ShowKeyboard()
  {
    if (!W8Touch.IsInitialized() || this.m_isVirtualKeyboardShowRequested || this.m_isVirtualKeyboardVisible && !this.m_isVirtualKeyboardHideRequested)
      return;
    if (this.m_isVirtualKeyboardHideRequested)
      this.m_isVirtualKeyboardHideRequested = false;
    W8Touch.KeyboardFlags keyboardFlags = (W8Touch.KeyboardFlags) W8Touch.DLL_W8ShowKeyboard();
    if ((keyboardFlags & W8Touch.KeyboardFlags.Shown) == W8Touch.KeyboardFlags.Shown)
      ;
    if ((keyboardFlags & W8Touch.KeyboardFlags.Shown) != W8Touch.KeyboardFlags.Shown || (keyboardFlags & W8Touch.KeyboardFlags.SuccessTabTip) != W8Touch.KeyboardFlags.SuccessTabTip)
      return;
    this.m_isVirtualKeyboardShowRequested = true;
  }

  public void HideKeyboard()
  {
    if (!W8Touch.IsInitialized() && !this.m_isVirtualKeyboardVisible)
      return;
    if (this.m_isVirtualKeyboardShowRequested)
      this.m_isVirtualKeyboardShowRequested = false;
    if (W8Touch.DLL_W8HideKeyboard() != 0)
      return;
    this.m_isVirtualKeyboardHideRequested = true;
  }

  public void ShowOSK()
  {
    if (!W8Touch.IsInitialized() || (W8Touch.DLL_W8ShowOSK() & 1) == 1)
      ;
  }

  public string GetIntelDeviceName()
  {
    if (!W8Touch.IsInitialized())
      return (string) null;
    return W8Touch.IntelDevice.GetDeviceName(W8Touch.DLL_W8GetDeviceId());
  }

  public W8Touch.PowerSource GetBatteryMode()
  {
    if (!W8Touch.IsInitialized())
      return W8Touch.PowerSource.Unintialized;
    return (W8Touch.PowerSource) W8Touch.DLL_W8GetBatteryMode();
  }

  public int GetPercentBatteryLife()
  {
    if (!W8Touch.IsInitialized())
      return -1;
    return W8Touch.DLL_W8GetPercentBatteryLife();
  }

  public bool IsVirtualKeyboardVisible()
  {
    if (!W8Touch.IsInitialized())
      return false;
    return this.m_isVirtualKeyboardVisible;
  }

  public bool GetTouch(int touchCount)
  {
    return W8Touch.IsInitialized() && this.m_touchState != null && touchCount < 5 && (this.m_touchState[touchCount] == W8Touch.TouchState.InitialDown || this.m_touchState[touchCount] == W8Touch.TouchState.Down);
  }

  public bool GetTouchDown(int touchCount)
  {
    return W8Touch.IsInitialized() && this.m_touchState != null && (touchCount < 5 && this.m_touchState[touchCount] == W8Touch.TouchState.InitialDown);
  }

  public bool GetTouchUp(int touchCount)
  {
    return W8Touch.IsInitialized() && this.m_touchState != null && (touchCount < 5 && this.m_touchState[touchCount] == W8Touch.TouchState.InitialUp);
  }

  public Vector3 GetTouchPosition()
  {
    if (!W8Touch.IsInitialized() || this.m_touchState == null)
      return new Vector3(0.0f, 0.0f, 0.0f);
    return new Vector3(this.m_touchPosition.x, this.m_touchPosition.y, this.m_touchPosition.z);
  }

  public Vector2 GetTouchDelta()
  {
    if (!W8Touch.IsInitialized() || this.m_touchState == null)
      return new Vector2(0.0f, 0.0f);
    return new Vector2(this.m_touchDelta.x, this.m_touchDelta.y);
  }

  public Vector3 GetTouchPositionForGUI()
  {
    if (!W8Touch.IsInitialized() || this.m_touchState == null)
      return new Vector3(0.0f, 0.0f, 0.0f);
    Vector2 vector2 = this.TransformTouchPosition((Vector2) this.m_touchPosition);
    return new Vector3(vector2.x, vector2.y, this.m_touchPosition.z);
  }

  private IntPtr GetFunction(string name)
  {
    IntPtr procAddress = DLLUtils.GetProcAddress(W8Touch.s_DLL, name);
    if (procAddress == IntPtr.Zero)
    {
      Debug.LogError((object) ("Could not load W8TouchDLL." + name + "()"));
      W8Touch.AppQuit();
    }
    return procAddress;
  }

  private bool LoadW8TouchDLL()
  {
    if (Environment.OSVersion.Version.Major < 6 || Environment.OSVersion.Version.Major == 6 && Environment.OSVersion.Version.Minor < 2)
    {
      Log.Yim.Print("Windows Version is Pre-Windows 8");
      return false;
    }
    if (W8Touch.s_DLL == IntPtr.Zero)
    {
      W8Touch.s_DLL = FileUtils.LoadPlugin("W8TouchDLL", false);
      if (W8Touch.s_DLL == IntPtr.Zero)
      {
        Log.Yim.Print("Could not load W8TouchDLL.dll");
        return false;
      }
    }
    W8Touch.DLL_W8ShowKeyboard = (W8Touch.DelW8ShowKeyboard) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_ShowKeyboard"), typeof (W8Touch.DelW8ShowKeyboard));
    W8Touch.DLL_W8HideKeyboard = (W8Touch.DelW8HideKeyboard) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_HideKeyboard"), typeof (W8Touch.DelW8HideKeyboard));
    W8Touch.DLL_W8ShowOSK = (W8Touch.DelW8ShowOSK) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_ShowOSK"), typeof (W8Touch.DelW8ShowOSK));
    W8Touch.DLL_W8Initialize = (W8Touch.DelW8Initialize) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_Initialize"), typeof (W8Touch.DelW8Initialize));
    W8Touch.DLL_W8Shutdown = (W8Touch.DelW8Shutdown) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_Shutdown"), typeof (W8Touch.DelW8Shutdown));
    W8Touch.DLL_W8GetDeviceId = (W8Touch.DelW8GetDeviceId) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_GetDeviceId"), typeof (W8Touch.DelW8GetDeviceId));
    W8Touch.DLL_W8IsWindows8OrGreater = (W8Touch.DelW8IsWindows8OrGreater) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_IsWindows8OrGreater"), typeof (W8Touch.DelW8IsWindows8OrGreater));
    W8Touch.DLL_W8IsLastEventFromTouch = (W8Touch.DelW8IsLastEventFromTouch) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_IsLastEventFromTouch"), typeof (W8Touch.DelW8IsLastEventFromTouch));
    W8Touch.DLL_W8GetBatteryMode = (W8Touch.DelW8GetBatteryMode) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_GetBatteryMode"), typeof (W8Touch.DelW8GetBatteryMode));
    W8Touch.DLL_W8GetPercentBatteryLife = (W8Touch.DelW8GetPercentBatteryLife) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_GetPercentBatteryLife"), typeof (W8Touch.DelW8GetPercentBatteryLife));
    W8Touch.DLL_W8GetDesktopRect = (W8Touch.DelW8GetDesktopRect) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_GetDesktopRect"), typeof (W8Touch.DelW8GetDesktopRect));
    W8Touch.DLL_W8IsVirtualKeyboardVisible = (W8Touch.DelW8IsVirtualKeyboardVisible) Marshal.GetDelegateForFunctionPointer(this.GetFunction("W8_IsVirtualKeyboardVisible"), typeof (W8Touch.DelW8IsVirtualKeyboardVisible));
    W8Touch.DLL_W8GetTouchPointCount = (W8Touch.DelW8GetTouchPointCount) Marshal.GetDelegateForFunctionPointer(this.GetFunction("GetTouchPointCount"), typeof (W8Touch.DelW8GetTouchPointCount));
    W8Touch.DLL_W8GetTouchPoint = (W8Touch.DelW8GetTouchPoint) Marshal.GetDelegateForFunctionPointer(this.GetFunction("GetTouchPoint"), typeof (W8Touch.DelW8GetTouchPoint));
    return true;
  }

  public static void AppQuit()
  {
    Log.Yim.Print("W8Touch.AppQuit()");
    if (W8Touch.s_DLL == IntPtr.Zero)
      return;
    if ((bool) ((UnityEngine.Object) W8Touch.s_instance))
      W8Touch.s_instance.ResetWindowFeedbackSetting();
    if (W8Touch.DLL_W8Shutdown != null && W8Touch.s_initialized)
    {
      W8Touch.DLL_W8Shutdown();
      W8Touch.s_initialized = false;
    }
    if (!DLLUtils.FreeLibrary(W8Touch.s_DLL))
      Debug.Log((object) "Error unloading W8TouchDLL.dll");
    W8Touch.s_DLL = IntPtr.Zero;
  }

  private static bool IsInitialized()
  {
    if (W8Touch.s_DLL != IntPtr.Zero && W8Touch.s_isWindows8OrGreater)
      return W8Touch.s_initialized;
    return false;
  }

  private void InitializeDLL()
  {
    if (this.m_intializationAttemptCount >= 10)
      return;
    string windowName = GameStrings.Get("GLOBAL_PROGRAMNAME_HEARTHSTONE");
    if (W8Touch.DLL_W8Initialize(windowName) < 0)
    {
      ++this.m_intializationAttemptCount;
    }
    else
    {
      Log.Yim.Print("W8Touch Start Success!");
      W8Touch.s_initialized = true;
      IntPtr module = DLLUtils.LoadLibrary("User32.DLL");
      if (module == IntPtr.Zero)
      {
        Log.Yim.Print("Could not load User32.DLL");
      }
      else
      {
        IntPtr procAddress = DLLUtils.GetProcAddress(module, "SetWindowFeedbackSetting");
        if (procAddress == IntPtr.Zero)
        {
          Log.Yim.Print("Could not load User32.SetWindowFeedbackSetting()");
        }
        else
        {
          IntPtr window = W8Touch.FindWindow((string) null, "Hearthstone");
          if (window == IntPtr.Zero)
            window = W8Touch.FindWindow((string) null, GameStrings.Get("GLOBAL_PROGRAMNAME_HEARTHSTONE"));
          if (window == IntPtr.Zero)
          {
            Log.Yim.Print("Unable to retrieve Hearthstone window handle!");
          }
          else
          {
            W8Touch.DelSetWindowFeedbackSetting forFunctionPointer = (W8Touch.DelSetWindowFeedbackSetting) Marshal.GetDelegateForFunctionPointer(procAddress, typeof (W8Touch.DelSetWindowFeedbackSetting));
            int cb = Marshal.SizeOf(typeof (int));
            IntPtr num = Marshal.AllocHGlobal(cb);
            Marshal.WriteInt32(num, 0, !this.m_bWindowFeedbackSettingValue ? 0 : 1);
            bool flag = true;
            if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_CONTACTVISUALIZATION, 0U, Convert.ToUInt32(cb), num))
            {
              Log.Yim.Print("FEEDBACK_TOUCH_CONTACTVISUALIZATION failed!");
              flag = false;
            }
            if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_TAP, 0U, Convert.ToUInt32(cb), num))
            {
              Log.Yim.Print("FEEDBACK_TOUCH_TAP failed!");
              flag = false;
            }
            if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_PRESSANDHOLD, 0U, Convert.ToUInt32(cb), num))
            {
              Log.Yim.Print("FEEDBACK_TOUCH_PRESSANDHOLD failed!");
              flag = false;
            }
            if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_DOUBLETAP, 0U, Convert.ToUInt32(cb), num))
            {
              Log.Yim.Print("FEEDBACK_TOUCH_DOUBLETAP failed!");
              flag = false;
            }
            if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_RIGHTTAP, 0U, Convert.ToUInt32(cb), num))
            {
              Log.Yim.Print("FEEDBACK_TOUCH_RIGHTTAP failed!");
              flag = false;
            }
            if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_GESTURE_PRESSANDTAP, 0U, Convert.ToUInt32(cb), num))
            {
              Log.Yim.Print("FEEDBACK_GESTURE_PRESSANDTAP failed!");
              flag = false;
            }
            this.m_bIsWindowFeedbackDisabled = flag;
            if (this.m_bIsWindowFeedbackDisabled)
              Log.Yim.Print("Windows 8 Feedback Touch Gestures Disabled!");
            Marshal.FreeHGlobal(num);
          }
        }
        if (DLLUtils.FreeLibrary(module))
          return;
        Log.Yim.Print("Error unloading User32.dll");
      }
    }
  }

  private void ResetWindowFeedbackSetting()
  {
    if (!W8Touch.s_initialized || !this.m_bIsWindowFeedbackDisabled)
      return;
    IntPtr module = DLLUtils.LoadLibrary("User32.DLL");
    if (module == IntPtr.Zero)
    {
      Log.Yim.Print("Could not load User32.DLL");
    }
    else
    {
      IntPtr procAddress = DLLUtils.GetProcAddress(module, "SetWindowFeedbackSetting");
      if (procAddress == IntPtr.Zero)
      {
        Log.Yim.Print("Could not load User32.SetWindowFeedbackSetting()");
      }
      else
      {
        IntPtr window = W8Touch.FindWindow((string) null, "Hearthstone");
        if (window == IntPtr.Zero)
          window = W8Touch.FindWindow((string) null, GameStrings.Get("GLOBAL_PROGRAMNAME_HEARTHSTONE"));
        if (window == IntPtr.Zero)
        {
          Log.Yim.Print("Unable to retrieve Hearthstone window handle!");
        }
        else
        {
          W8Touch.DelSetWindowFeedbackSetting forFunctionPointer = (W8Touch.DelSetWindowFeedbackSetting) Marshal.GetDelegateForFunctionPointer(procAddress, typeof (W8Touch.DelSetWindowFeedbackSetting));
          IntPtr num = Marshal.AllocHGlobal(Marshal.SizeOf(typeof (int)));
          Marshal.WriteInt32(num, 0, !this.m_bWindowFeedbackSettingValue ? 0 : 1);
          bool flag = true;
          if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_CONTACTVISUALIZATION, 0U, 0U, IntPtr.Zero))
          {
            Log.Yim.Print("FEEDBACK_TOUCH_CONTACTVISUALIZATION failed!");
            flag = false;
          }
          if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_TAP, 0U, 0U, IntPtr.Zero))
          {
            Log.Yim.Print("FEEDBACK_TOUCH_TAP failed!");
            flag = false;
          }
          if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_PRESSANDHOLD, 0U, 0U, IntPtr.Zero))
          {
            Log.Yim.Print("FEEDBACK_TOUCH_PRESSANDHOLD failed!");
            flag = false;
          }
          if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_DOUBLETAP, 0U, 0U, IntPtr.Zero))
          {
            Log.Yim.Print("FEEDBACK_TOUCH_DOUBLETAP failed!");
            flag = false;
          }
          if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_TOUCH_RIGHTTAP, 0U, 0U, IntPtr.Zero))
          {
            Log.Yim.Print("FEEDBACK_TOUCH_RIGHTTAP failed!");
            flag = false;
          }
          if (!forFunctionPointer(window, W8Touch.FEEDBACK_TYPE.FEEDBACK_GESTURE_PRESSANDTAP, 0U, 0U, IntPtr.Zero))
          {
            Log.Yim.Print("FEEDBACK_GESTURE_PRESSANDTAP failed!");
            flag = false;
          }
          this.m_bIsWindowFeedbackDisabled = !flag;
          if (!this.m_bIsWindowFeedbackDisabled)
            Log.Yim.Print("Windows 8 Feedback Touch Gestures Reset!");
          Marshal.FreeHGlobal(num);
        }
      }
      if (DLLUtils.FreeLibrary(module))
        return;
      Log.Yim.Print("Error unloading User32.dll");
    }
  }

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public class tTouchData
  {
    public int m_x;
    public int m_y;
    public int m_ID;
    public int m_Time;
  }

  public struct RECT
  {
    public int Left;
    public int Top;
    public int Right;
    public int Bottom;
  }

  [Flags]
  public enum KeyboardFlags
  {
    Shown = 1,
    NotShown = 2,
    SuccessTabTip = 4,
    SuccessOSK = 8,
    ErrorTabTip = 16,
    ErrorOSK = 32,
    NotFoundTabTip = 64,
    NotFoundOSK = 128,
  }

  public enum TouchState
  {
    None,
    InitialDown,
    Down,
    InitialUp,
  }

  public class IntelDevice
  {
    private static readonly Map<int, string> DeviceIdMap = new Map<int, string>() { { 30720, "Auburn" }, { 28961, "Whitney" }, { 28963, "Whitney" }, { 28965, "Whitney" }, { 4402, "Solono" }, { 9570, "Brookdale" }, { 13698, "Montara" }, { 9586, "Springdale" }, { 9602, "Grantsdale" }, { 10114, "Grantsdale" }, { 9618, "Alviso" }, { 10130, "Alviso" }, { 10098, "Lakeport-G" }, { 10102, "Lakeport-G" }, { 10146, "Calistoga" }, { 10150, "Calistoga" }, { 10626, "Broadwater-G" }, { 10627, "Broadwater-G" }, { 10610, "Broadwater-G" }, { 10611, "Broadwater-G" }, { 10642, "Broadwater-G" }, { 10643, "Broadwater-G" }, { 10658, "Broadwater-G" }, { 10659, "Broadwater-G" }, { 10754, "Crestline" }, { 10755, "Crestline" }, { 10770, "Crestline" }, { 10771, "Crestline" }, { 10674, "Bearlake" }, { 10675, "Bearlake" }, { 10690, "Bearlake" }, { 10691, "Bearlake" }, { 10706, "Bearlake" }, { 10707, "Bearlake" }, { 10818, "Cantiga" }, { 10819, "Cantiga" }, { 11778, "Eaglelake" }, { 11779, "Eaglelake" }, { 11810, "Eaglelake" }, { 11811, "Eaglelake" }, { 11794, "Eaglelake" }, { 11795, "Eaglelake" }, { 11826, "Eaglelake" }, { 11827, "Eaglelake" }, { 11842, "Eaglelake" }, { 11843, "Eaglelake" }, { 11922, "Eaglelake" }, { 11923, "Eaglelake" }, { 70, "Arrandale" }, { 66, "Clarkdale" }, { 262, "Mobile_SandyBridge_GT1" }, { 278, "Mobile_SandyBridge_GT2" }, { 294, "Mobile_SandyBridge_GT2+" }, { 258, "DT_SandyBridge_GT2+" }, { 274, "DT_SandyBridge_GT2+" }, { 290, "DT_SandyBridge_GT2+" }, { 266, "SandyBridge_Server" }, { 270, "SandyBridge_Reserved" }, { 338, "Desktop_IvyBridge_GT1" }, { 342, "Mobile_IvyBridge_GT1" }, { 346, "Server_IvyBridge_GT1" }, { 350, "Reserved_IvyBridge_GT1" }, { 354, "Desktop_IvyBridge_GT2" }, { 358, "Mobile_IvyBridge_GT2" }, { 362, "Server_IvyBridge_GT2" }, { 1026, "Desktop_Haswell_GT1_Y6W" }, { 1030, "Mobile_Haswell_GT1_Y6W" }, { 1034, "Server_Haswell_GT1" }, { 1042, "Desktop_Haswell_GT2_U15W" }, { 1046, "Mobile_Haswell_GT2_U15W" }, { 1051, "Workstation_Haswell_GT2" }, { 1050, "Server_Haswell_GT2" }, { 1054, "Reserved_Haswell_DT_GT1.5_U15W" }, { 2566, "Mobile_Haswell_ULT_GT1_Y6W" }, { 2574, "Mobile_Haswell_ULX_GT1_Y6W" }, { 2582, "Mobile_Haswell_ULT_GT2_U15W" }, { 2590, "Mobile_Haswell_ULX_GT2_Y6W" }, { 2598, "Mobile_Haswell_ULT_GT3_U28W" }, { 2606, "Mobile_Haswell_ULT_GT3@28_U28W" }, { 3346, "Desktop_Haswell_GT2F" }, { 3350, "Mobile_Haswell_GT2F" }, { 3362, "Desktop_Crystal-Well_GT3" }, { 3366, "Mobile_Crystal-Well_GT3" }, { 3370, "Server_Crystal-Well_GT3" }, { 3889, "BayTrail" }, { 33032, "Poulsbo" }, { 33033, "Poulsbo" }, { 2255, "CloverTrail" }, { 40961, "CloverTrail" }, { 40962, "CloverTrail" }, { 40977, "CloverTrail" }, { 40978, "CloverTrail" } };

    public static string GetDeviceName(int deviceId)
    {
      string str;
      if (!W8Touch.IntelDevice.DeviceIdMap.TryGetValue(deviceId, out str))
        return string.Empty;
      return str;
    }
  }

  public enum PowerSource
  {
    Unintialized = -1,
    BatteryPower = 0,
    ACPower = 1,
    UndefinedPower = 255,
  }

  public enum FEEDBACK_TYPE
  {
    FEEDBACK_TOUCH_CONTACTVISUALIZATION = 1,
    FEEDBACK_PEN_BARRELVISUALIZATION = 2,
    FEEDBACK_PEN_TAP = 3,
    FEEDBACK_PEN_DOUBLETAP = 4,
    FEEDBACK_PEN_PRESSANDHOLD = 5,
    FEEDBACK_PEN_RIGHTTAP = 6,
    FEEDBACK_TOUCH_TAP = 7,
    FEEDBACK_TOUCH_DOUBLETAP = 8,
    FEEDBACK_TOUCH_PRESSANDHOLD = 9,
    FEEDBACK_TOUCH_RIGHTTAP = 10,
    FEEDBACK_GESTURE_PRESSANDTAP = 11,
  }

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate int DelW8ShowKeyboard();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate int DelW8HideKeyboard();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate int DelW8ShowOSK();

  [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Auto)]
  private delegate int DelW8Initialize(string windowName);

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate void DelW8Shutdown();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate int DelW8GetDeviceId();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate bool DelW8IsWindows8OrGreater();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate bool DelW8IsLastEventFromTouch();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate int DelW8GetBatteryMode();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate int DelW8GetPercentBatteryLife();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate void DelW8GetDesktopRect(out W8Touch.RECT desktopRect);

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate bool DelW8IsVirtualKeyboardVisible();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate int DelW8GetTouchPointCount();

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate bool DelW8GetTouchPoint(int i, W8Touch.tTouchData n);

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate bool DelSetWindowFeedbackSetting(IntPtr hwnd, W8Touch.FEEDBACK_TYPE feedback, uint dwFlags, uint size, IntPtr configuration);
}
