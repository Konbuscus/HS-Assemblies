﻿// Decompiled with JetBrains decompiler
// Type: BnetGameAccount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using PegasusClient;
using SpectatorProto;
using System.Collections.Generic;

public class BnetGameAccount
{
  private Map<uint, object> m_gameFields = new Map<uint, object>();
  private BnetGameAccountId m_id;
  private BnetAccountId m_ownerId;
  private BnetProgramId m_programId;
  private BnetBattleTag m_battleTag;
  private bool m_away;
  private ulong m_awayTimeMicrosec;
  private bool m_busy;
  private bool m_online;
  private ulong m_lastOnlineMicrosec;
  private string m_richPresence;

  public static bool operator ==(BnetGameAccount a, BnetGameAccount b)
  {
    if (object.ReferenceEquals((object) a, (object) b))
      return true;
    if ((object) a == null || (object) b == null)
      return false;
    return (BnetEntityId) a.m_id == (BnetEntityId) b.m_id;
  }

  public static bool operator !=(BnetGameAccount a, BnetGameAccount b)
  {
    return !(a == b);
  }

  public BnetGameAccount Clone()
  {
    BnetGameAccount bnetGameAccount = (BnetGameAccount) this.MemberwiseClone();
    if ((BnetEntityId) this.m_id != (BnetEntityId) null)
      bnetGameAccount.m_id = this.m_id.Clone();
    if ((BnetEntityId) this.m_ownerId != (BnetEntityId) null)
      bnetGameAccount.m_ownerId = this.m_ownerId.Clone();
    if ((bgs.FourCC) this.m_programId != (bgs.FourCC) null)
      bnetGameAccount.m_programId = this.m_programId.Clone();
    if (this.m_battleTag != (BnetBattleTag) null)
      bnetGameAccount.m_battleTag = this.m_battleTag.Clone();
    bnetGameAccount.m_gameFields = new Map<uint, object>();
    using (Map<uint, object>.Enumerator enumerator = this.m_gameFields.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<uint, object> current = enumerator.Current;
        bnetGameAccount.m_gameFields.Add(current.Key, current.Value);
      }
    }
    return bnetGameAccount;
  }

  public BnetGameAccountId GetId()
  {
    return this.m_id;
  }

  public void SetId(BnetGameAccountId id)
  {
    this.m_id = id;
  }

  public BnetAccountId GetOwnerId()
  {
    return this.m_ownerId;
  }

  public void SetOwnerId(BnetAccountId id)
  {
    this.m_ownerId = id;
  }

  public BnetProgramId GetProgramId()
  {
    return this.m_programId;
  }

  public void SetProgramId(BnetProgramId programId)
  {
    this.m_programId = programId;
  }

  public BnetBattleTag GetBattleTag()
  {
    return this.m_battleTag;
  }

  public void SetBattleTag(BnetBattleTag battleTag)
  {
    this.m_battleTag = battleTag;
  }

  public bool IsAway()
  {
    return this.m_away;
  }

  public void SetAway(bool away)
  {
    this.m_away = away;
  }

  public ulong GetAwayTimeMicrosec()
  {
    return this.m_awayTimeMicrosec;
  }

  public void SetAwayTimeMicrosec(ulong awayTimeMicrosec)
  {
    this.m_awayTimeMicrosec = awayTimeMicrosec;
  }

  public bool IsBusy()
  {
    return this.m_busy;
  }

  public void SetBusy(bool busy)
  {
    this.m_busy = busy;
  }

  public bool IsOnline()
  {
    return this.m_online;
  }

  public void SetOnline(bool online)
  {
    this.m_online = online;
  }

  public ulong GetLastOnlineMicrosec()
  {
    return this.m_lastOnlineMicrosec;
  }

  public void SetLastOnlineMicrosec(ulong microsec)
  {
    this.m_lastOnlineMicrosec = microsec;
  }

  public string GetRichPresence()
  {
    return this.m_richPresence;
  }

  public void SetRichPresence(string richPresence)
  {
    this.m_richPresence = richPresence;
  }

  public Map<uint, object> GetGameFields()
  {
    return this.m_gameFields;
  }

  public bool HasGameField(uint fieldId)
  {
    return this.m_gameFields.ContainsKey(fieldId);
  }

  public void SetGameField(uint fieldId, object val)
  {
    this.m_gameFields[fieldId] = val;
  }

  public bool RemoveGameField(uint fieldId)
  {
    return this.m_gameFields.Remove(fieldId);
  }

  public bool TryGetGameField(uint fieldId, out object val)
  {
    return this.m_gameFields.TryGetValue(fieldId, out val);
  }

  public bool TryGetGameFieldBool(uint fieldId, out bool val)
  {
    val = false;
    object obj = (object) null;
    if (!this.m_gameFields.TryGetValue(fieldId, out obj))
      return false;
    val = (bool) obj;
    return true;
  }

  public bool TryGetGameFieldInt(uint fieldId, out int val)
  {
    val = 0;
    object obj = (object) null;
    if (!this.m_gameFields.TryGetValue(fieldId, out obj))
      return false;
    val = (int) obj;
    return true;
  }

  public bool TryGetGameFieldString(uint fieldId, out string val)
  {
    val = (string) null;
    object obj = (object) null;
    if (!this.m_gameFields.TryGetValue(fieldId, out obj))
      return false;
    val = (string) obj;
    return true;
  }

  public bool TryGetGameFieldBytes(uint fieldId, out byte[] val)
  {
    val = (byte[]) null;
    object obj = (object) null;
    if (!this.m_gameFields.TryGetValue(fieldId, out obj))
      return false;
    val = (byte[]) obj;
    return true;
  }

  public object GetGameField(uint fieldId)
  {
    object obj = (object) null;
    this.m_gameFields.TryGetValue(fieldId, out obj);
    return obj;
  }

  public bool GetGameFieldBool(uint fieldId)
  {
    object obj = (object) null;
    if (this.m_gameFields.TryGetValue(fieldId, out obj))
      return (bool) obj;
    return false;
  }

  public int GetGameFieldInt(uint fieldId)
  {
    object obj = (object) null;
    if (this.m_gameFields.TryGetValue(fieldId, out obj))
      return (int) obj;
    return 0;
  }

  public string GetGameFieldString(uint fieldId)
  {
    object obj = (object) null;
    if (this.m_gameFields.TryGetValue(fieldId, out obj))
      return (string) obj;
    return (string) null;
  }

  public byte[] GetGameFieldBytes(uint fieldId)
  {
    object obj = (object) null;
    if (this.m_gameFields.TryGetValue(fieldId, out obj))
      return (byte[]) obj;
    return (byte[]) null;
  }

  public bool CanBeInvitedToGame()
  {
    return this.GetGameFieldBool(1U);
  }

  public string GetClientVersion()
  {
    return this.GetGameFieldString(19U);
  }

  public string GetClientEnv()
  {
    return this.GetGameFieldString(20U);
  }

  public string GetDebugString()
  {
    return this.GetGameFieldString(2U);
  }

  public SessionRecord GetSessionRecord()
  {
    byte[] gameFieldBytes = this.GetGameFieldBytes(22U);
    if (gameFieldBytes != null && gameFieldBytes.Length > 0)
      return ProtobufUtil.ParseFrom<SessionRecord>(gameFieldBytes, 0, -1);
    return (SessionRecord) null;
  }

  public string GetCardsOpened()
  {
    return this.GetGameFieldString(4U);
  }

  public int GetDruidLevel()
  {
    return this.GetGameFieldInt(5U);
  }

  public int GetHunterLevel()
  {
    return this.GetGameFieldInt(6U);
  }

  public int GetMageLevel()
  {
    return this.GetGameFieldInt(7U);
  }

  public int GetPaladinLevel()
  {
    return this.GetGameFieldInt(8U);
  }

  public int GetPriestLevel()
  {
    return this.GetGameFieldInt(9U);
  }

  public int GetRogueLevel()
  {
    return this.GetGameFieldInt(10U);
  }

  public int GetShamanLevel()
  {
    return this.GetGameFieldInt(11U);
  }

  public int GetWarlockLevel()
  {
    return this.GetGameFieldInt(12U);
  }

  public int GetWarriorLevel()
  {
    return this.GetGameFieldInt(13U);
  }

  public int GetGainMedal()
  {
    return this.GetGameFieldInt(14U);
  }

  public int GetTutorialBeaten()
  {
    return this.GetGameFieldInt(15U);
  }

  public int GetCollectionEvent()
  {
    return this.GetGameFieldInt(16U);
  }

  public JoinInfo GetSpectatorJoinInfo()
  {
    byte[] gameFieldBytes = this.GetGameFieldBytes(21U);
    if (gameFieldBytes != null && gameFieldBytes.Length > 0)
      return ProtobufUtil.ParseFrom<JoinInfo>(gameFieldBytes, 0, -1);
    return (JoinInfo) null;
  }

  public bool IsSpectatorSlotAvailable()
  {
    return BnetGameAccount.IsSpectatorSlotAvailable(this.GetSpectatorJoinInfo());
  }

  public static bool IsSpectatorSlotAvailable(JoinInfo info)
  {
    return info != null && (info.HasPartyId || info.HasServerIpAddress && info.HasSecretKey && !string.IsNullOrEmpty(info.SecretKey)) && ((!info.HasIsJoinable || info.IsJoinable) && (!info.HasMaxNumSpectators || !info.HasCurrentNumSpectators || info.CurrentNumSpectators < info.MaxNumSpectators));
  }

  public override bool Equals(object obj)
  {
    if (obj == null)
      return false;
    BnetGameAccount bnetGameAccount = obj as BnetGameAccount;
    if ((object) bnetGameAccount == null)
      return false;
    return this.m_id.Equals((BnetEntityId) bnetGameAccount.m_id);
  }

  public bool Equals(BnetGameAccountId other)
  {
    if (other == null)
      return false;
    return this.m_id.Equals((BnetEntityId) other);
  }

  public override int GetHashCode()
  {
    return this.m_id.GetHashCode();
  }

  public override string ToString()
  {
    if ((BnetEntityId) this.m_id == (BnetEntityId) null)
      return "UNKNOWN GAME ACCOUNT";
    return string.Format("[id={0} programId={1} battleTag={2} online={3}]", (object) this.m_id, (object) this.m_programId, (object) this.m_battleTag, (object) this.m_online);
  }
}
