﻿// Decompiled with JetBrains decompiler
// Type: ChatBubbleFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using UnityEngine;

public class ChatBubbleFrame : MonoBehaviour
{
  public GameObject m_VisualRoot;
  public GameObject m_MyDecoration;
  public GameObject m_TheirDecoration;
  public UberText m_NameText;
  public UberText m_MessageText;
  public Vector3_MobileOverride m_ScaleOverride;
  private BnetWhisper m_whisper;

  private void Awake()
  {
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
  }

  private void OnDestroy()
  {
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
  }

  public BnetWhisper GetWhisper()
  {
    return this.m_whisper;
  }

  public void SetWhisper(BnetWhisper whisper)
  {
    if (this.m_whisper == whisper)
      return;
    this.m_whisper = whisper;
    this.UpdateWhisper();
  }

  public bool DoesMessageFit()
  {
    return !this.m_MessageText.IsEllipsized();
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    BnetPlayer player = BnetPresenceMgr.Get().GetPlayer(WhisperUtil.GetTheirGameAccountId(this.m_whisper));
    BnetPlayerChange change = changelist.FindChange(player);
    if (change == null)
      return;
    BnetPlayer oldPlayer = change.GetOldPlayer();
    BnetPlayer newPlayer = change.GetNewPlayer();
    if (oldPlayer != null && oldPlayer.IsOnline() == newPlayer.IsOnline())
      return;
    this.UpdateWhisper();
  }

  private void UpdateWhisper()
  {
    if (this.m_whisper == null)
      return;
    if ((BnetEntityId) this.m_whisper.GetSpeakerId() == (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId())
    {
      this.m_MyDecoration.SetActive(true);
      this.m_TheirDecoration.SetActive(false);
      this.m_NameText.Text = GameStrings.Format("GLOBAL_CHAT_BUBBLE_RECEIVER_NAME", (object) WhisperUtil.GetReceiver(this.m_whisper).GetBestName());
    }
    else
    {
      this.m_MyDecoration.SetActive(false);
      this.m_TheirDecoration.SetActive(true);
      BnetPlayer speaker = WhisperUtil.GetSpeaker(this.m_whisper);
      this.m_NameText.TextColor = !speaker.IsOnline() ? GameColors.PLAYER_NAME_OFFLINE : GameColors.PLAYER_NAME_ONLINE;
      this.m_NameText.Text = speaker.GetBestName();
    }
    this.m_MessageText.Text = ChatUtils.GetMessage(this.m_whisper);
  }
}
