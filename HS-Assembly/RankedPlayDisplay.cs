﻿// Decompiled with JetBrains decompiler
// Type: RankedPlayDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class RankedPlayDisplay : MonoBehaviour
{
  public RankedPlayToggleButton m_casualButton;
  public RankedPlayToggleButton m_rankedButton;
  public Transform m_medalBone;
  public TournamentMedal m_medalPrefab;
  public PlayMakerFSM m_formatSwitchGlowBurst;
  public float m_medalSwitchFormatDelay;
  private TournamentMedal m_medal;
  private bool m_inSetRotationTutorial;
  private bool m_usingWildVisuals;

  private void Awake()
  {
    this.m_medal = Object.Instantiate<TournamentMedal>(this.m_medalPrefab);
    this.SetRankedMedalTransform(this.m_medalBone);
    this.m_medal.GetComponent<Collider>().enabled = false;
    this.m_usingWildVisuals = Options.Get().GetBool(Option.IN_WILD_MODE);
    this.m_casualButton.SetFormat(this.m_usingWildVisuals);
    this.m_rankedButton.SetFormat(this.m_usingWildVisuals);
    this.m_medal.SetFormat(this.m_usingWildVisuals);
    this.m_casualButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnCasualButtonOver));
    this.m_rankedButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnRankedButtonOver));
    this.m_casualButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnCasualButtonOut));
    this.m_rankedButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnRankedButtonOut));
  }

  public void UpdateMode()
  {
    bool shown = Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE);
    NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
    if (netObject != null && netObject.Games != null && !netObject.Games.Casual)
    {
      this.m_casualButton.SetEnabled(false);
      if (!shown)
      {
        shown = true;
        Options.Get().SetBool(Option.IN_RANKED_PLAY_MODE, true);
      }
    }
    else
      this.m_casualButton.SetEnabled(true);
    if ((bool) UniversalInputManager.UsePhoneUI)
      DeckPickerTrayDisplay.Get().ToggleRankedDetailsTray(shown);
    else
      DeckPickerTrayDisplay.Get().UpdateRankedClassWinsPlate();
    if (shown)
    {
      this.m_casualButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCasualButtonRelease));
      this.m_rankedButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRankedButtonRelease));
      this.m_casualButton.Up();
      this.m_rankedButton.Down();
    }
    else
    {
      this.m_casualButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCasualButtonRelease));
      this.m_rankedButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRankedButtonRelease));
      this.m_casualButton.Down();
      this.m_rankedButton.Up();
    }
  }

  public void StartSetRotationTutorial()
  {
    this.m_inSetRotationTutorial = true;
    bool shown = Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE);
    if ((bool) UniversalInputManager.UsePhoneUI)
      DeckPickerTrayDisplay.Get().ToggleRankedDetailsTray(shown);
    this.m_usingWildVisuals = false;
    this.m_casualButton.SetFormat(false);
    this.m_rankedButton.SetFormat(false);
    this.m_casualButton.Up();
    this.m_rankedButton.Up();
    this.m_casualButton.SetEnabled(false);
    this.m_rankedButton.SetEnabled(false);
    DeckPickerTrayDisplay.Get().SetPlayButtonText(!shown ? GameStrings.Get("GLOBAL_PLAY") : GameStrings.Get("GLOBAL_PLAY_RANKED"));
    DeckPickerTrayDisplay.Get().m_playButton.m_newPlayButtonText.TextAlpha = 0.0f;
    DeckPickerTrayDisplay.Get().UpdateRankedClassWinsPlate();
    this.m_medal.SetFormat(false);
  }

  public void EndSetRotationTutorial()
  {
    this.m_inSetRotationTutorial = false;
    this.m_casualButton.SetEnabled(true);
    this.m_rankedButton.SetEnabled(true);
    this.UpdateMode();
  }

  public void SetRankedMedalTransform(Transform bone)
  {
    this.m_medal.transform.parent = bone;
    this.m_medal.transform.localScale = Vector3.one;
    this.m_medal.transform.localPosition = Vector3.zero;
  }

  public void SetRankedMedal(NetCache.NetCacheMedalInfo medal)
  {
    this.m_medal.SetMedal(medal);
  }

  public void OnSwitchFormat()
  {
    if (this.m_inSetRotationTutorial)
      return;
    bool flag = Options.Get().GetBool(Option.IN_WILD_MODE);
    if (this.m_usingWildVisuals != flag)
    {
      this.m_usingWildVisuals = flag;
      this.StopCoroutine("WaitThenSetRankedButtonsFormat");
      if ((Object) this.m_formatSwitchGlowBurst != (Object) null)
      {
        this.m_formatSwitchGlowBurst.SendEvent(!this.m_usingWildVisuals ? "Glow" : "GlowNoFX");
        this.StartCoroutine("WaitThenSetRankedButtonsFormat", (object) this.m_usingWildVisuals);
      }
      else
      {
        this.m_casualButton.SetFormat(this.m_usingWildVisuals);
        this.m_rankedButton.SetFormat(this.m_usingWildVisuals);
        this.m_medal.SetFormat(this.m_usingWildVisuals);
      }
    }
    this.UpdateMode();
  }

  private void OnCasualButtonRelease(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("tournament_screen_select_hero");
    Options.Get().SetBool(Option.IN_RANKED_PLAY_MODE, false);
    this.UpdateMode();
  }

  private void OnRankedButtonRelease(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("tournament_screen_select_hero");
    Options.Get().SetBool(Option.IN_RANKED_PLAY_MODE, true);
    this.UpdateMode();
  }

  private void OnCasualButtonOver(UIEvent e)
  {
    this.m_casualButton.GetComponent<TooltipZone>().ShowTooltip(GameStrings.Get(!Options.Get().GetBool(Option.IN_WILD_MODE) ? "GLUE_TOURNAMENT_CASUAL" : "GLUE_TOURNAMENT_CASUAL_WILD"), GameStrings.Get("GLUE_TOOLTIP_CASUAL_BUTTON"), 5f, true);
  }

  private void OnRankedButtonOver(UIEvent e)
  {
    this.m_rankedButton.GetComponent<TooltipZone>().ShowTooltip(GameStrings.Get(!Options.Get().GetBool(Option.IN_WILD_MODE) ? "GLUE_TOURNAMENT_RANKED" : "GLUE_TOURNAMENT_RANKED_WILD"), GameStrings.Get("GLUE_TOOLTIP_RANKED_BUTTON"), 5f, true);
  }

  private void OnCasualButtonOut(UIEvent e)
  {
    this.m_casualButton.GetComponent<TooltipZone>().HideTooltip();
  }

  private void OnRankedButtonOut(UIEvent e)
  {
    this.m_rankedButton.GetComponent<TooltipZone>().HideTooltip();
  }

  [DebuggerHidden]
  private IEnumerator WaitThenSetRankedButtonsFormat(bool isWild)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankedPlayDisplay.\u003CWaitThenSetRankedButtonsFormat\u003Ec__Iterator7D()
    {
      isWild = isWild,
      \u003C\u0024\u003EisWild = isWild,
      \u003C\u003Ef__this = this
    };
  }
}
