﻿// Decompiled with JetBrains decompiler
// Type: UnopenedPackStack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class UnopenedPackStack
{
  public GameObject m_RootObject;
  public Renderer m_MeshRenderer;
  public UberText m_AmountText;
  public GameObject m_Shadow;
}
