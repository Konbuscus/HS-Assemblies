﻿// Decompiled with JetBrains decompiler
// Type: AdventureWingProgressDisplay_LOE
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class AdventureWingProgressDisplay_LOE : AdventureWingProgressDisplay
{
  public List<GameObject> m_emptyStaffObjects = new List<GameObject>();
  public List<GameObject> m_visibleStaffObjects = new List<GameObject>();
  public List<GameObject> m_rodObjects = new List<GameObject>();
  public List<GameObject> m_headObjects = new List<GameObject>();
  public List<GameObject> m_pearlObjects = new List<GameObject>();
  private const string s_WingDisappearAnimateEventName = "OnWingDisappear";
  private const string s_WingReappearAnimateEventName = "OnWingReappear";
  private const string s_CompleteAnimationVarName = "AnimationComplete";
  public UberText m_hangingSignText;
  public PegUIElement m_hangingSignHitArea;
  public PegUIElement m_completeStaffHitArea;
  [CustomEditField(Sections = "VO")]
  public string m_hangingSignQuotePrefab;
  [CustomEditField(Sections = "VO")]
  public string m_hangingSignQuoteVOLine;
  [CustomEditField(Sections = "VO")]
  public string m_completeStaffQuotePrefab;
  [CustomEditField(Sections = "VO")]
  public string m_completeStaffQuoteVOLine;
  private bool m_rodComplete;
  private bool m_headComplete;
  private bool m_pearlComplete;
  private bool m_finalWingComplete;
  private bool m_animating;

  private void Awake()
  {
    AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_emptyStaffObjects, true);
    AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_rodObjects, false);
    AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_headObjects, false);
    AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_pearlObjects, false);
    AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_visibleStaffObjects, false);
    if ((Object) this.m_hangingSignHitArea != (Object) null)
      this.m_hangingSignHitArea.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OnHangingSignClick()));
    if (!((Object) this.m_completeStaffHitArea != (Object) null))
      return;
    this.m_completeStaffHitArea.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OnCompleteStaffClick()));
  }

  private void Update()
  {
    if (!AdventureScene.Get().IsDevMode)
      return;
    if (Input.GetKeyDown(KeyCode.C))
    {
      this.StartCoroutine(this.PlayCompleteAnimationCoroutine(this.GetComponent<PlayMakerFSM>(), "OnWingDisappear", (AdventureWingProgressDisplay.OnAnimationComplete) null, Option.INVALID));
    }
    else
    {
      if (!Input.GetKeyDown(KeyCode.V))
        return;
      this.StartCoroutine(this.PlayCompleteAnimationCoroutine(this.GetComponent<PlayMakerFSM>(), "OnWingReappear", (AdventureWingProgressDisplay.OnAnimationComplete) null, Option.INVALID));
    }
  }

  public override void UpdateProgress(WingDbId wingDbId, bool normalComplete)
  {
    switch (wingDbId)
    {
      case WingDbId.LOE_TEMPLE_OF_ORSIS:
        this.m_rodComplete = normalComplete;
        break;
      case WingDbId.LOE_ULDAMAN:
        this.m_headComplete = normalComplete;
        break;
      case WingDbId.LOE_RUINED_CITY:
        this.m_pearlComplete = normalComplete;
        break;
      case WingDbId.LOE_HALL_OF_EXPLORERS:
        this.m_finalWingComplete = normalComplete;
        break;
    }
    this.UpdatePartVisibility();
  }

  public override bool HasProgressAnimationToPlay()
  {
    if (!this.m_rodComplete || !this.m_headComplete || !this.m_pearlComplete)
      return false;
    if (this.m_finalWingComplete)
      return !Options.Get().GetBool(Option.HAS_SEEN_LOE_STAFF_REAPPEAR, false);
    return !Options.Get().GetBool(Option.HAS_SEEN_LOE_STAFF_DISAPPEAR, false);
  }

  public override void PlayProgressAnimation(AdventureWingProgressDisplay.OnAnimationComplete onAnimComplete = null)
  {
    if (!this.m_rodComplete || !this.m_headComplete || !this.m_pearlComplete)
    {
      if (onAnimComplete == null)
        return;
      onAnimComplete();
    }
    else
    {
      PlayMakerFSM component = this.GetComponent<PlayMakerFSM>();
      if ((Object) component == (Object) null)
      {
        if (onAnimComplete == null)
          return;
        onAnimComplete();
      }
      else if (!this.m_finalWingComplete)
      {
        if (Options.Get().GetBool(Option.HAS_SEEN_LOE_STAFF_DISAPPEAR, false))
        {
          if (onAnimComplete == null)
            return;
          onAnimComplete();
        }
        else
          this.StartCoroutine(this.PlayCompleteAnimationCoroutine(component, "OnWingDisappear", onAnimComplete, Option.HAS_SEEN_LOE_STAFF_DISAPPEAR));
      }
      else if (Options.Get().GetBool(Option.HAS_SEEN_LOE_STAFF_REAPPEAR, false))
      {
        if (onAnimComplete == null)
          return;
        onAnimComplete();
      }
      else
        this.StartCoroutine(this.PlayCompleteAnimationCoroutine(component, "OnWingReappear", onAnimComplete, Option.HAS_SEEN_LOE_STAFF_REAPPEAR));
    }
  }

  private void UpdatePartVisibility()
  {
    bool flag1 = Options.Get().GetBool(Option.HAS_SEEN_LOE_STAFF_DISAPPEAR, false);
    if (this.m_finalWingComplete)
    {
      bool flag2 = Options.Get().GetBool(Option.HAS_SEEN_LOE_STAFF_REAPPEAR, false);
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_emptyStaffObjects, false);
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_rodObjects, this.m_rodComplete && flag2);
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_headObjects, this.m_headComplete && flag2);
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_pearlObjects, this.m_pearlComplete && flag2);
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_visibleStaffObjects, true);
    }
    else
    {
      bool show1 = this.m_rodComplete && !flag1;
      bool show2 = this.m_headComplete && !flag1;
      bool show3 = this.m_pearlComplete && !flag1;
      bool show4 = show1 || show2 || show3;
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_emptyStaffObjects, !show4);
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_rodObjects, show1);
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_headObjects, show2);
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_pearlObjects, show3);
      AdventureWingProgressDisplay_LOE.SetObjectsVisibility(this.m_visibleStaffObjects, show4);
    }
    if ((Object) this.m_hangingSignText != (Object) null)
      this.m_hangingSignText.Text = !flag1 ? GameStrings.Get("GLUE_ADVENTURE_LOE_STAFF_RESERVED") : GameStrings.Get("GLUE_ADVENTURE_LOE_STAFF_DISAPPEARED");
    if ((Object) this.m_completeStaffHitArea != (Object) null)
      this.m_completeStaffHitArea.gameObject.SetActive(this.m_finalWingComplete && this.m_rodComplete && this.m_headComplete && this.m_pearlComplete);
    if (!((Object) this.m_hangingSignHitArea != (Object) null))
      return;
    this.m_hangingSignHitArea.SetEnabled(!this.m_finalWingComplete && !this.m_rodComplete && !this.m_headComplete && !this.m_pearlComplete);
  }

  private static void SetObjectsVisibility(List<GameObject> objs, bool show)
  {
    using (List<GameObject>.Enumerator enumerator = objs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if ((Object) current != (Object) null)
          current.SetActive(show);
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator PlayCompleteAnimationCoroutine(PlayMakerFSM fsm, string eventName, AdventureWingProgressDisplay.OnAnimationComplete onAnimComplete, Option seenOption)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureWingProgressDisplay_LOE.\u003CPlayCompleteAnimationCoroutine\u003Ec__Iterator11()
    {
      fsm = fsm,
      eventName = eventName,
      seenOption = seenOption,
      onAnimComplete = onAnimComplete,
      \u003C\u0024\u003Efsm = fsm,
      \u003C\u0024\u003EeventName = eventName,
      \u003C\u0024\u003EseenOption = seenOption,
      \u003C\u0024\u003EonAnimComplete = onAnimComplete,
      \u003C\u003Ef__this = this
    };
  }

  private void OnHangingSignClick()
  {
    if (this.m_animating || this.m_rodComplete || (this.m_headComplete || this.m_pearlComplete) || (string.IsNullOrEmpty(this.m_hangingSignQuotePrefab) || string.IsNullOrEmpty(this.m_hangingSignQuoteVOLine)))
      return;
    NotificationManager.Get().CreateCharacterQuote(FileUtils.GameAssetPathToName(this.m_hangingSignQuotePrefab), GameStrings.Get(this.m_hangingSignQuoteVOLine), this.m_hangingSignQuoteVOLine, true, 0.0f, CanvasAnchor.BOTTOM_LEFT);
  }

  private void OnCompleteStaffClick()
  {
    if (this.m_animating || !this.m_rodComplete || (!this.m_headComplete || !this.m_pearlComplete) || (!this.m_finalWingComplete || string.IsNullOrEmpty(this.m_completeStaffQuotePrefab) || string.IsNullOrEmpty(this.m_completeStaffQuoteVOLine)))
      return;
    NotificationManager.Get().CreateCharacterQuote(FileUtils.GameAssetPathToName(this.m_completeStaffQuotePrefab), GameStrings.Get(this.m_completeStaffQuoteVOLine), this.m_completeStaffQuoteVOLine, true, 0.0f, CanvasAnchor.BOTTOM_LEFT);
  }
}
