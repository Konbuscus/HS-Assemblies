﻿// Decompiled with JetBrains decompiler
// Type: CollectionPageLayoutSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CollectionPageLayoutSettings
{
  [CustomEditField(ListTable = true)]
  public List<CollectionPageLayoutSettings.Variables> m_layoutVariables = new List<CollectionPageLayoutSettings.Variables>();

  public CollectionPageLayoutSettings.Variables GetVariables(CollectionManagerDisplay.ViewMode mode)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_layoutVariables.Find(new Predicate<CollectionPageLayoutSettings.Variables>(new CollectionPageLayoutSettings.\u003CGetVariables\u003Ec__AnonStorey391()
    {
      mode = mode
    }.\u003C\u003Em__DC)) ?? new CollectionPageLayoutSettings.Variables();
  }

  [Serializable]
  public class Variables
  {
    public int m_ColumnCount = 4;
    public int m_RowCount = 2;
    public CollectionManagerDisplay.ViewMode m_ViewMode;
    public float m_Scale;
    public float m_ColumnSpacing;
    public float m_RowSpacing;
    public Vector3 m_Offset;
  }
}
