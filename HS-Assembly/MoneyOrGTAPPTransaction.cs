﻿// Decompiled with JetBrains decompiler
// Type: MoneyOrGTAPPTransaction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;

public class MoneyOrGTAPPTransaction
{
  public static readonly BattlePayProvider? UNKNOWN_PROVIDER;

  public long ID { get; private set; }

  public string ProductID { get; private set; }

  public bool IsGTAPP { get; private set; }

  public BattlePayProvider? Provider { get; private set; }

  public bool ClosedStore { get; set; }

  public MoneyOrGTAPPTransaction(long id, string productID, BattlePayProvider? provider, bool isGTAPP)
  {
    this.ID = id;
    this.ProductID = productID;
    this.IsGTAPP = isGTAPP;
    this.Provider = provider;
    this.ClosedStore = false;
  }

  public override int GetHashCode()
  {
    return this.ID.GetHashCode() * this.ProductID.GetHashCode();
  }

  public override bool Equals(object obj)
  {
    MoneyOrGTAPPTransaction gtappTransaction = obj as MoneyOrGTAPPTransaction;
    if (gtappTransaction == null)
      return false;
    bool flag = !this.Provider.HasValue || !gtappTransaction.Provider.HasValue || this.Provider.Value == gtappTransaction.Provider.Value;
    if (gtappTransaction.ID == this.ID && gtappTransaction.ProductID == this.ProductID)
      return flag;
    return false;
  }

  public override string ToString()
  {
    return string.Format("[MoneyOrGTAPPTransaction: ID={0},ProductID='{1}',IsGTAPP={2},Provider={3}]", (object) this.ID, (object) this.ProductID, (object) this.IsGTAPP, (object) (!this.Provider.HasValue ? "UNKNOWN" : this.Provider.Value.ToString()));
  }

  public bool ShouldShowMiniSummary()
  {
    if ((bool) StoreManager.HAS_THIRD_PARTY_APP_STORE && ApplicationMgr.GetAndroidStore() != AndroidStore.BLIZZARD)
      return true;
    return this.ClosedStore;
  }
}
