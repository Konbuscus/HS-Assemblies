﻿// Decompiled with JetBrains decompiler
// Type: CardBackReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardBackReward : Reward
{
  public GameObject m_cardbackBone;
  private int m_numCardBacksLoaded;

  protected override void InitData()
  {
    this.SetData((RewardData) new CardBackRewardData(), false);
  }

  protected override void ShowReward(bool updateCacheValues)
  {
    CardBackRewardData data = this.Data as CardBackRewardData;
    if (data == null)
    {
      Debug.LogWarning((object) string.Format("CardBackReward.ShowReward() - Data {0} is not CardBackRewardData", (object) this.Data));
    }
    else
    {
      if (!data.IsDummyReward && updateCacheValues)
        CardBackManager.Get().AddNewCardBack(data.CardBackID);
      this.m_root.SetActive(true);
      this.m_cardbackBone.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 180f);
      iTween.RotateAdd(this.m_cardbackBone.gameObject, iTween.Hash((object) "amount", (object) new Vector3(0.0f, 0.0f, 540f), (object) "time", (object) 1.5f, (object) "easeType", (object) iTween.EaseType.easeOutElastic, (object) "space", (object) Space.Self));
    }
  }

  protected override void HideReward()
  {
    base.HideReward();
    this.m_root.SetActive(false);
  }

  protected override void OnDataSet(bool updateVisuals)
  {
    if (!updateVisuals)
      return;
    this.SetRewardText(GameStrings.Get("GLOBAL_REWARD_CARD_BACK_HEADLINE"), string.Empty, string.Empty);
    CardBackRewardData data = this.Data as CardBackRewardData;
    if (data == null)
    {
      Debug.LogWarning((object) string.Format("CardBackReward.OnDataSet() - Data {0} is not CardBackRewardData", (object) this.Data));
    }
    else
    {
      this.SetReady(false);
      CardBackManager.Get().LoadCardBackByIndex(data.CardBackID, new CardBackManager.LoadCardBackData.LoadCardBackCallback(this.OnFrontCardBackLoaded), true, "Card_Hidden");
      CardBackManager.Get().LoadCardBackByIndex(data.CardBackID, new CardBackManager.LoadCardBackData.LoadCardBackCallback(this.OnBackCardBackLoaded), true, "Card_Hidden");
    }
  }

  private void OnFrontCardBackLoaded(CardBackManager.LoadCardBackData cardbackData)
  {
    GameObject gameObject = cardbackData.m_GameObject;
    gameObject.transform.parent = this.m_cardbackBone.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
    gameObject.transform.localScale = Vector3.one;
    SceneUtils.SetLayer(gameObject, this.gameObject.layer);
    ++this.m_numCardBacksLoaded;
    if (this.m_numCardBacksLoaded != 2)
      return;
    this.SetReady(true);
  }

  private void OnBackCardBackLoaded(CardBackManager.LoadCardBackData cardbackData)
  {
    GameObject gameObject = cardbackData.m_GameObject;
    gameObject.transform.parent = this.m_cardbackBone.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 180f));
    gameObject.transform.localScale = Vector3.one;
    SceneUtils.SetLayer(gameObject, this.gameObject.layer);
    ++this.m_numCardBacksLoaded;
    if (this.m_numCardBacksLoaded != 2)
      return;
    this.SetReady(true);
  }
}
