﻿// Decompiled with JetBrains decompiler
// Type: MedalInfoTranslator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using UnityEngine;

public class MedalInfoTranslator
{
  public const string MEDAL_TEXTURE_PREFIX = "Medal_Ranked_";
  public const string MEDAL_NAME_PREFIX = "GLOBAL_MEDAL_";
  public const int MAX_RANK = 26;
  public const int WORST_DISPLAYABLE_RANK = 25;
  public const int LEGEND_RANK_VALUE = 0;
  public const int LOWEST_LEGEND_VALUE = -1;
  private TranslatedMedalInfo m_currMedalInfo;
  private TranslatedMedalInfo m_prevMedalInfo;
  private TranslatedMedalInfo m_currWildMedalInfo;
  private TranslatedMedalInfo m_prevWildMedalInfo;
  private bool m_validPrevMedal;

  public MedalInfoTranslator()
  {
    MedalInfoTranslator medalInfoTranslator = new MedalInfoTranslator(-1, 0, -1, 0);
  }

  public MedalInfoTranslator(int rank, int legendIndex, int wildRank = -1, int wildLegendIndex = 0)
  {
    TranslatedMedalInfo translatedMedalInfo1 = new TranslatedMedalInfo();
    rank = Mathf.Clamp(rank, 0, 25);
    legendIndex = Mathf.Clamp(legendIndex, -1, legendIndex);
    translatedMedalInfo1.rank = rank;
    translatedMedalInfo1.legendIndex = legendIndex;
    translatedMedalInfo1.textureName = "Medal_Ranked_" + (object) translatedMedalInfo1.rank;
    translatedMedalInfo1.name = GameStrings.Get("GLOBAL_MEDAL_" + (object) translatedMedalInfo1.rank);
    string key1 = "GLOBAL_MEDAL_" + (object) (translatedMedalInfo1.rank - 1);
    string str1 = GameStrings.Get(key1);
    translatedMedalInfo1.nextMedalName = !(str1 != key1) ? string.Empty : str1;
    this.m_currMedalInfo = translatedMedalInfo1;
    TranslatedMedalInfo translatedMedalInfo2 = new TranslatedMedalInfo();
    wildRank = Mathf.Clamp(wildRank, 0, 25);
    wildLegendIndex = Mathf.Clamp(wildLegendIndex, -1, wildLegendIndex);
    translatedMedalInfo2.rank = wildRank;
    translatedMedalInfo2.legendIndex = wildLegendIndex;
    translatedMedalInfo2.textureName = "Medal_Ranked_" + (object) translatedMedalInfo2.rank;
    translatedMedalInfo2.name = GameStrings.Get("GLOBAL_MEDAL_" + (object) translatedMedalInfo2.rank);
    string key2 = "GLOBAL_MEDAL_" + (object) (translatedMedalInfo2.rank - 1);
    string str2 = GameStrings.Get(key2);
    translatedMedalInfo2.nextMedalName = !(str2 != key2) ? string.Empty : str2;
    this.m_currWildMedalInfo = translatedMedalInfo2;
  }

  public MedalInfoTranslator(NetCache.NetCacheMedalInfo currMedalInfo)
  {
    if (currMedalInfo == null)
      return;
    this.m_currMedalInfo = this.Translate(currMedalInfo.Standard);
    this.m_currWildMedalInfo = this.Translate(currMedalInfo.Wild);
  }

  public MedalInfoTranslator(NetCache.NetCacheMedalInfo currMedalInfo, NetCache.NetCacheMedalInfo prevMedalInfo)
  {
    this.m_currMedalInfo = this.Translate(currMedalInfo.Standard);
    this.m_currWildMedalInfo = this.Translate(currMedalInfo.Wild);
    this.m_validPrevMedal = prevMedalInfo != null;
    this.m_prevMedalInfo = !this.m_validPrevMedal ? this.Translate(currMedalInfo.Standard) : this.Translate(prevMedalInfo.Standard);
    this.m_prevWildMedalInfo = !this.m_validPrevMedal ? this.Translate(currMedalInfo.Wild) : this.Translate(prevMedalInfo.Wild);
  }

  public bool IsDisplayable(bool useWildMedal)
  {
    TranslatedMedalInfo translatedMedalInfo = !useWildMedal ? this.m_currMedalInfo : this.m_currWildMedalInfo;
    return translatedMedalInfo != null && translatedMedalInfo.rank != -1;
  }

  public TranslatedMedalInfo Translate(MedalInfoData medalInfoData)
  {
    TranslatedMedalInfo translatedMedalInfo = new TranslatedMedalInfo();
    if (medalInfoData == null)
      return translatedMedalInfo;
    translatedMedalInfo.rank = 26 - medalInfoData.StarLevel;
    translatedMedalInfo.bestRank = 26 - medalInfoData.BestStarLevel;
    translatedMedalInfo.legendIndex = !medalInfoData.HasLegendRank ? 0 : medalInfoData.LegendRank;
    translatedMedalInfo.totalStars = medalInfoData.LevelEnd - medalInfoData.LevelStart;
    translatedMedalInfo.earnedStars = medalInfoData.Stars;
    if (medalInfoData.StarLevel != 1)
      translatedMedalInfo.earnedStars -= medalInfoData.LevelStart - 1;
    translatedMedalInfo.winStreak = medalInfoData.Streak;
    translatedMedalInfo.textureName = "Medal_Ranked_" + (object) translatedMedalInfo.rank;
    translatedMedalInfo.name = GameStrings.Get("GLOBAL_MEDAL_" + (object) translatedMedalInfo.rank);
    translatedMedalInfo.canLoseStars = medalInfoData.CanLoseStars;
    translatedMedalInfo.canLoseLevel = medalInfoData.CanLoseLevel;
    string key = "GLOBAL_MEDAL_" + (object) (translatedMedalInfo.rank - 1);
    string str = GameStrings.Get(key);
    translatedMedalInfo.nextMedalName = !(str != key) ? string.Empty : str;
    return translatedMedalInfo;
  }

  public TranslatedMedalInfo GetCurrentMedal(bool useWildMedal)
  {
    if (useWildMedal)
      return this.m_currWildMedalInfo;
    return this.m_currMedalInfo;
  }

  public TranslatedMedalInfo GetPreviousMedal(bool useWildMedal)
  {
    if (useWildMedal)
      return this.m_prevWildMedalInfo;
    return this.m_prevMedalInfo;
  }

  public bool IsPreviousMedalValid()
  {
    return this.m_validPrevMedal;
  }

  public bool IsBestCurrentRankWild()
  {
    if (this.m_currWildMedalInfo == null)
      return false;
    if (this.m_currMedalInfo == null || this.m_currMedalInfo.rank > this.m_currWildMedalInfo.rank)
      return true;
    if (this.m_currMedalInfo.rank < this.m_currWildMedalInfo.rank)
      return false;
    if (this.m_currMedalInfo.IsLegendRank())
      return this.m_currMedalInfo.legendIndex > this.m_currWildMedalInfo.legendIndex;
    return this.m_currMedalInfo.earnedStars < this.m_currWildMedalInfo.earnedStars;
  }

  public bool ShowRewardChest(bool useWildMedal)
  {
    TranslatedMedalInfo translatedMedalInfo1 = !useWildMedal ? this.m_prevMedalInfo : this.m_prevWildMedalInfo;
    TranslatedMedalInfo translatedMedalInfo2 = !useWildMedal ? this.m_currMedalInfo : this.m_currWildMedalInfo;
    return translatedMedalInfo1 != null && translatedMedalInfo2 != null && (translatedMedalInfo2.rank != 0 && translatedMedalInfo2.bestRank < translatedMedalInfo1.bestRank) && translatedMedalInfo2.CanGetRankedRewardChest();
  }

  public RankChangeType GetChangeType(bool useWildMedal)
  {
    TranslatedMedalInfo translatedMedalInfo1 = !useWildMedal ? this.m_prevMedalInfo : this.m_prevWildMedalInfo;
    TranslatedMedalInfo translatedMedalInfo2 = !useWildMedal ? this.m_currMedalInfo : this.m_currWildMedalInfo;
    if (translatedMedalInfo1 == null || translatedMedalInfo2 == null)
      return RankChangeType.UNKNOWN;
    if (translatedMedalInfo1.rank < translatedMedalInfo2.rank)
      return RankChangeType.RANK_DOWN;
    return translatedMedalInfo2.rank < translatedMedalInfo1.rank ? RankChangeType.RANK_UP : RankChangeType.RANK_SAME;
  }

  public void TestSetMedalInfo(TranslatedMedalInfo currMedal, TranslatedMedalInfo prevMedal)
  {
    this.m_currMedalInfo = currMedal;
    this.m_prevMedalInfo = prevMedal;
    this.m_validPrevMedal = true;
  }
}
