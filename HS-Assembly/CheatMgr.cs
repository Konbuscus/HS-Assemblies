﻿// Decompiled with JetBrains decompiler
// Type: CheatMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class CheatMgr : MonoBehaviour
{
  private Map<string, List<CheatMgr.ProcessCheatCallback>> m_funcMap = new Map<string, List<CheatMgr.ProcessCheatCallback>>();
  private Map<string, string> m_cheatAlias = new Map<string, string>();
  private Map<string, string> m_cheatDesc = new Map<string, string>();
  private Map<string, string> m_cheatArgs = new Map<string, string>();
  private Map<string, string> m_cheatExamples = new Map<string, string>();
  private int m_cheatHistoryIndex = -1;
  private int m_autofillMatchIndex = -1;
  private const int MAX_HISTORY_LINES = 25;
  private static CheatMgr s_instance;
  private Rect m_cheatInputBackground;
  private bool m_inputActive;
  private List<string> m_cheatHistory;
  private string m_cheatTextBeforeScrollingThruHistory;
  private string m_cheatTextBeforeAutofill;

  public Map<string, string> cheatDesc
  {
    get
    {
      return this.m_cheatDesc;
    }
  }

  public Map<string, string> cheatArgs
  {
    get
    {
      return this.m_cheatArgs;
    }
  }

  public Map<string, string> cheatExamples
  {
    get
    {
      return this.m_cheatExamples;
    }
  }

  public static CheatMgr Get()
  {
    return CheatMgr.s_instance;
  }

  public void Awake()
  {
    CheatMgr.s_instance = this;
    this.m_cheatHistory = new List<string>();
    Cheats.Initialize();
  }

  public Map<string, List<CheatMgr.ProcessCheatCallback>>.KeyCollection GetCheatCommands()
  {
    return this.m_funcMap.Keys;
  }

  public bool HandleKeyboardInput()
  {
    if (ApplicationMgr.IsPublic() || !Input.GetKeyUp(KeyCode.BackQuote))
      return false;
    Rect rect = new Rect(0.0f, 0.0f, 1f, 0.05f);
    this.m_cheatInputBackground = rect;
    this.m_cheatInputBackground.x *= (float) Screen.width * 0.95f;
    this.m_cheatInputBackground.y *= (float) Screen.height;
    this.m_cheatInputBackground.width *= (float) Screen.width;
    this.m_cheatInputBackground.height *= (float) Screen.height * 1.03f;
    this.m_inputActive = true;
    this.m_cheatHistoryIndex = -1;
    this.m_cheatTextBeforeAutofill = (string) null;
    this.m_autofillMatchIndex = -1;
    this.ReadCheatHistoryOption();
    this.m_cheatTextBeforeScrollingThruHistory = (string) null;
    UniversalInputManager.Get().UseTextInput(new UniversalInputManager.TextInputParams()
    {
      m_owner = this.gameObject,
      m_preprocessCallback = new UniversalInputManager.TextInputPreprocessCallback(this.OnInputPreprocess),
      m_rect = rect,
      m_color = new Color?(Color.white),
      m_completedCallback = new UniversalInputManager.TextInputCompletedCallback(this.OnInputComplete)
    }, false);
    return true;
  }

  private void ReadCheatHistoryOption()
  {
    this.m_cheatHistory = new List<string>((IEnumerable<string>) Options.Get().GetString(Option.CHEAT_HISTORY).Split(';'));
  }

  private void WriteCheatHistoryOption()
  {
    Options.Get().SetString(Option.CHEAT_HISTORY, string.Join(";", this.m_cheatHistory.ToArray()));
  }

  private bool OnInputPreprocess(Event e)
  {
    if (e.type != EventType.KeyDown)
      return false;
    KeyCode keyCode1 = e.keyCode;
    if (keyCode1 == KeyCode.BackQuote && string.IsNullOrEmpty(UniversalInputManager.Get().GetInputText()))
    {
      UniversalInputManager.Get().CancelTextInput(this.gameObject, false);
      return true;
    }
    if (this.m_cheatHistory.Count < 1)
      return false;
    if (keyCode1 == KeyCode.UpArrow)
    {
      if (this.m_cheatHistoryIndex >= this.m_cheatHistory.Count - 1)
        return true;
      string inputText = UniversalInputManager.Get().GetInputText();
      if (this.m_cheatTextBeforeScrollingThruHistory == null)
        this.m_cheatTextBeforeScrollingThruHistory = inputText;
      UniversalInputManager.Get().SetInputText(this.m_cheatHistory[++this.m_cheatHistoryIndex], true);
      return true;
    }
    if (keyCode1 == KeyCode.DownArrow)
    {
      string scrollingThruHistory;
      if (this.m_cheatHistoryIndex <= 0)
      {
        this.m_cheatHistoryIndex = -1;
        if (this.m_cheatTextBeforeScrollingThruHistory == null)
          return false;
        scrollingThruHistory = this.m_cheatTextBeforeScrollingThruHistory;
        this.m_cheatTextBeforeScrollingThruHistory = (string) null;
      }
      else
        scrollingThruHistory = this.m_cheatHistory[--this.m_cheatHistoryIndex];
      UniversalInputManager.Get().SetInputText(scrollingThruHistory, false);
      return true;
    }
    if (keyCode1 == KeyCode.Tab && ApplicationMgr.IsInternal())
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      CheatMgr.\u003COnInputPreprocess\u003Ec__AnonStorey45D preprocessCAnonStorey45D = new CheatMgr.\u003COnInputPreprocess\u003Ec__AnonStorey45D();
      // ISSUE: reference to a compiler-generated field
      preprocessCAnonStorey45D.text = UniversalInputManager.Get().GetInputText();
      // ISSUE: reference to a compiler-generated field
      if (!preprocessCAnonStorey45D.text.Contains(' '))
      {
        bool flag = true;
        if (this.m_cheatTextBeforeAutofill != null)
        {
          // ISSUE: reference to a compiler-generated field
          preprocessCAnonStorey45D.text = this.m_cheatTextBeforeAutofill;
          flag = false;
        }
        else
        {
          // ISSUE: reference to a compiler-generated field
          this.m_cheatTextBeforeAutofill = preprocessCAnonStorey45D.text;
        }
        // ISSUE: reference to a compiler-generated method
        List<string> list = this.m_funcMap.Keys.Where<string>(new Func<string, bool>(preprocessCAnonStorey45D.\u003C\u003Em__32D)).ToList<string>();
        if (list.Count > 0)
        {
          list.Sort();
          int index = 0;
          this.m_autofillMatchIndex += !Event.current.shift ? 1 : -1;
          if (this.m_autofillMatchIndex >= list.Count)
            this.m_autofillMatchIndex = 0;
          else if (this.m_autofillMatchIndex < 0)
            this.m_autofillMatchIndex = list.Count - 1;
          if (this.m_autofillMatchIndex >= 0 && this.m_autofillMatchIndex < list.Count)
            index = this.m_autofillMatchIndex;
          // ISSUE: reference to a compiler-generated field
          preprocessCAnonStorey45D.text = list[index];
          // ISSUE: reference to a compiler-generated field
          UniversalInputManager.Get().SetInputText(preprocessCAnonStorey45D.text, true);
          if (flag && list.Count > 1)
          {
            float delay = 5f + Mathf.Max(0.0f, (float) (list.Count - 3));
            UIStatus.Get().AddInfo("Available cheats:\n" + string.Join("   ", list.ToArray()), delay);
          }
        }
      }
    }
    else
    {
      bool flag = true;
      KeyCode keyCode2 = keyCode1;
      switch (keyCode2)
      {
        case KeyCode.CapsLock:
        case KeyCode.RightShift:
        case KeyCode.LeftShift:
        case KeyCode.RightControl:
        case KeyCode.LeftControl:
        case KeyCode.RightAlt:
        case KeyCode.LeftAlt:
        case KeyCode.RightCommand:
        case KeyCode.LeftCommand:
        case KeyCode.LeftWindows:
        case KeyCode.RightWindows:
        case KeyCode.Menu:
label_36:
          flag = false;
          break;
        default:
          switch (keyCode2 - 273)
          {
            case KeyCode.None:
            case (KeyCode) 1:
            case (KeyCode) 2:
            case (KeyCode) 3:
            case (KeyCode) 4:
            case (KeyCode) 5:
            case (KeyCode) 6:
            case (KeyCode) 7:
            case KeyCode.Backspace:
              goto label_36;
            default:
              if (keyCode2 == KeyCode.None)
                goto label_36;
              else
                break;
          }
      }
      if (flag)
      {
        this.m_cheatTextBeforeAutofill = (string) null;
        this.m_autofillMatchIndex = -1;
      }
    }
    return false;
  }

  public void RegisterCheatHandler(string func, CheatMgr.ProcessCheatCallback callback, string desc = null, string argDesc = null, string exampleArgs = null)
  {
    this.RegisterCheatHandler_(func, callback);
    if (desc != null)
      this.m_cheatDesc[func] = desc;
    if (argDesc != null)
      this.m_cheatArgs[func] = argDesc;
    if (exampleArgs == null)
      return;
    this.m_cheatExamples[func] = exampleArgs;
  }

  public void RegisterCheatAlias(string func, params string[] aliases)
  {
    List<CheatMgr.ProcessCheatCallback> processCheatCallbackList;
    if (!this.m_funcMap.TryGetValue(func, out processCheatCallbackList))
    {
      Debug.LogError((object) string.Format("CheatMgr.RegisterCheatAlias() - cannot register aliases for func {0} because it does not exist", (object) func));
    }
    else
    {
      foreach (string aliase in aliases)
        this.m_cheatAlias[aliase] = func;
    }
  }

  public void UnregisterCheatHandler(string func, CheatMgr.ProcessCheatCallback callback)
  {
    this.UnregisterCheatHandler_(func, callback);
  }

  public void OnGUI()
  {
    if (!this.m_inputActive)
      return;
    if (!UniversalInputManager.Get().IsTextInputActive())
    {
      this.m_inputActive = false;
    }
    else
    {
      GUI.depth = 1000;
      GUI.backgroundColor = Color.black;
      GUI.Box(this.m_cheatInputBackground, GUIContent.none);
      GUI.Box(this.m_cheatInputBackground, GUIContent.none);
      GUI.Box(this.m_cheatInputBackground, GUIContent.none);
    }
  }

  private void RegisterCheatHandler_(string func, CheatMgr.ProcessCheatCallback callback)
  {
    if (string.IsNullOrEmpty(func.Trim()))
    {
      Debug.LogError((object) "CheatMgr.RegisterCheatHandler() - FAILED to register a null, empty, or all-whitespace function name");
    }
    else
    {
      List<CheatMgr.ProcessCheatCallback> processCheatCallbackList;
      if (this.m_funcMap.TryGetValue(func, out processCheatCallbackList))
      {
        if (processCheatCallbackList.Contains(callback))
          return;
        processCheatCallbackList.Add(callback);
      }
      else
      {
        processCheatCallbackList = new List<CheatMgr.ProcessCheatCallback>();
        this.m_funcMap.Add(func, processCheatCallbackList);
        processCheatCallbackList.Add(callback);
      }
    }
  }

  private void UnregisterCheatHandler_(string func, CheatMgr.ProcessCheatCallback callback)
  {
    List<CheatMgr.ProcessCheatCallback> processCheatCallbackList;
    if (!this.m_funcMap.TryGetValue(func, out processCheatCallbackList))
      return;
    processCheatCallbackList.Remove(callback);
  }

  private void OnInputComplete(string inputCommand)
  {
    this.m_inputActive = false;
    inputCommand = inputCommand.TrimStart();
    if (string.IsNullOrEmpty(inputCommand))
      return;
    this.m_cheatTextBeforeAutofill = (string) null;
    this.m_autofillMatchIndex = -1;
    string message = this.ProcessCheat(inputCommand);
    if (string.IsNullOrEmpty(message))
      return;
    UIStatus.Get().AddError(message, 4f);
  }

  public string ProcessCheat(string inputCommand)
  {
    if (this.m_cheatHistory.Count < 1 || !this.m_cheatHistory[0].Equals(inputCommand))
    {
      this.m_cheatHistory.Remove(inputCommand);
      this.m_cheatHistory.Insert(0, inputCommand);
    }
    if (this.m_cheatHistory.Count > 25)
      this.m_cheatHistory.RemoveRange(24, this.m_cheatHistory.Count - 25);
    this.m_cheatHistoryIndex = -1;
    this.m_cheatTextBeforeScrollingThruHistory = (string) null;
    this.WriteCheatHistoryOption();
    string func1 = this.ExtractFunc(inputCommand);
    if (func1 == null)
      return "\"" + inputCommand.Split(' ')[0] + "\" cheat command not found!";
    UIStatus.Get().AddInfo(string.Empty, 0.0f);
    int length = func1.Length;
    string str;
    string[] args;
    if (length == inputCommand.Length)
    {
      str = string.Empty;
      args = new string[1]{ string.Empty };
    }
    else
    {
      str = inputCommand.Remove(0, length + 1);
      MatchCollection matchCollection = Regex.Matches(str, "\\S+");
      if (matchCollection.Count == 0)
      {
        args = new string[1]{ string.Empty };
      }
      else
      {
        args = new string[matchCollection.Count];
        for (int index = 0; index < matchCollection.Count; ++index)
          args[index] = matchCollection[index].Value;
      }
    }
    List<CheatMgr.ProcessCheatCallback> func2 = this.m_funcMap[this.GetOriginalFunc(func1)];
    bool flag = false;
    for (int index = 0; index < func2.Count; ++index)
      flag = func2[index](func1, args, str) || flag;
    if (!flag)
      return "\"" + func1 + "\" cheat command executed, but failed!";
    return (string) null;
  }

  private string ExtractFunc(string inputCommand)
  {
    inputCommand = inputCommand.TrimStart('/');
    inputCommand = inputCommand.Trim();
    int index1 = 0;
    List<string> funcs = new List<string>();
    using (Map<string, List<CheatMgr.ProcessCheatCallback>>.KeyCollection.Enumerator enumerator = this.m_funcMap.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        funcs.Add(current);
        if (current.Length > funcs[index1].Length)
          index1 = funcs.Count - 1;
      }
    }
    using (Map<string, string>.KeyCollection.Enumerator enumerator = this.m_cheatAlias.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        funcs.Add(current);
        if (current.Length > funcs[index1].Length)
          index1 = funcs.Count - 1;
      }
    }
    int index2;
    for (index2 = 0; index2 < inputCommand.Length; ++index2)
    {
      char c = inputCommand[index2];
      int index3 = 0;
      while (index3 < funcs.Count)
      {
        string str = funcs[index3];
        if (index2 == str.Length)
        {
          if (char.IsWhiteSpace(c))
            return str;
          funcs.RemoveAt(index3);
          if (index3 <= index1)
            index1 = this.ComputeLongestFuncIndex(funcs);
        }
        else if ((int) str[index2] != (int) c)
        {
          funcs.RemoveAt(index3);
          if (index3 <= index1)
            index1 = this.ComputeLongestFuncIndex(funcs);
        }
        else
          ++index3;
      }
      if (funcs.Count == 0)
        return (string) null;
    }
    if (funcs.Count > 1)
    {
      using (List<string>.Enumerator enumerator = funcs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          if (inputCommand == current)
            return current;
        }
      }
      return (string) null;
    }
    string str1 = funcs[0];
    if (index2 < str1.Length)
      return (string) null;
    return str1;
  }

  private int ComputeLongestFuncIndex(List<string> funcs)
  {
    int index1 = 0;
    for (int index2 = 1; index2 < funcs.Count; ++index2)
    {
      if (funcs[index2].Length > funcs[index1].Length)
        index1 = index2;
    }
    return index1;
  }

  private string GetOriginalFunc(string func)
  {
    string str;
    if (!this.m_cheatAlias.TryGetValue(func, out str))
      str = func;
    return str;
  }

  public delegate bool ProcessCheatCallback(string func, string[] args, string rawArgs);
}
