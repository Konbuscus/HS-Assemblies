﻿// Decompiled with JetBrains decompiler
// Type: OrientedBounds
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class OrientedBounds
{
  public Vector3[] Extents;
  public Vector3 Origin;
  public Vector3 CenterOffset;

  public Vector3 GetTrueCenterPosition()
  {
    return this.Origin + this.CenterOffset;
  }
}
