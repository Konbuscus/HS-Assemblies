﻿// Decompiled with JetBrains decompiler
// Type: BattleNetLogger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Diagnostics;
using System.Text;

public class BattleNetLogger : LoggerBase
{
  private void Log(bgs.LogLevel logLevel, string str)
  {
    LogLevel level;
    switch (logLevel)
    {
      case bgs.LogLevel.None:
        level = LogLevel.None;
        break;
      case bgs.LogLevel.Debug:
        level = LogLevel.Debug;
        break;
      case bgs.LogLevel.Info:
        level = LogLevel.Info;
        break;
      case bgs.LogLevel.Warning:
        level = LogLevel.Warning;
        break;
      case bgs.LogLevel.Error:
      case bgs.LogLevel.Fatal:
        level = LogLevel.Error;
        break;
      default:
        level = LogLevel.Error;
        break;
    }
    Log.BattleNet.Print(level, str, new object[0]);
    if (level < LogLevel.Warning || Log.BattleNet.CanPrint(LogTarget.CONSOLE, level, false))
      return;
    string str1 = string.Format("[{0}] {1}", (object) "BattleNet", (object) str);
    if (level == LogLevel.Warning)
      UnityEngine.Debug.LogWarning((object) str1);
    else
      UnityEngine.Debug.LogError((object) str1);
  }

  public override void Log(bgs.LogLevel logLevel, string message, string sourceName)
  {
    if (logLevel == bgs.LogLevel.Fatal)
    {
      string message1 = GameStrings.Get(message);
      if (ApplicationMgr.IsInternal())
        Error.AddDevFatal(message1);
      else
        Error.AddFatal(message1);
    }
    this.LogFromSource(logLevel, message, sourceName, true);
  }

  public override void LogDebug(string message, string sourceName = "")
  {
    this.LogFromSource(bgs.LogLevel.Debug, message, sourceName, true);
  }

  public override void LogInfo(string message, string sourceName = "")
  {
    this.LogFromSource(bgs.LogLevel.Info, message, sourceName, true);
  }

  public override void LogWarning(string message, string sourceName = "")
  {
    this.LogFromSource(bgs.LogLevel.Warning, message, sourceName, true);
  }

  public override void LogError(string message, string sourceName = "")
  {
    this.LogFromSource(bgs.LogLevel.Error, message, sourceName, true);
  }

  private void LogFromSource(bgs.LogLevel logLevel, string message, string sourceName, bool includeFileName = true)
  {
    StackTrace stackTrace = new StackTrace(new StackFrame(2, true));
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.Append("[");
    stringBuilder.Append(sourceName);
    stringBuilder.Append("] ");
    stringBuilder.Append(message);
    if (includeFileName)
    {
      StackFrame frame = stackTrace.GetFrame(0);
      stringBuilder.Append(this.FormatStackTrace(frame, false));
    }
    this.Log(logLevel, stringBuilder.ToString());
  }
}
