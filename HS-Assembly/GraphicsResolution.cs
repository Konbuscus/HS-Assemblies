﻿// Decompiled with JetBrains decompiler
// Type: GraphicsResolution
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class GraphicsResolution : IComparable
{
  public static readonly List<GraphicsResolution> resolutions_ = new List<GraphicsResolution>();

  public static List<GraphicsResolution> list
  {
    get
    {
      if (GraphicsResolution.resolutions_.Count == 0)
      {
        lock (GraphicsResolution.resolutions_)
        {
          foreach (Resolution item_0 in Screen.resolutions)
            GraphicsResolution.add(item_0.width, item_0.height);
        }
      }
      return GraphicsResolution.resolutions_;
    }
  }

  public static GraphicsResolution current
  {
    get
    {
      return GraphicsResolution.create(Screen.currentResolution);
    }
  }

  public int x { get; private set; }

  public int y { get; private set; }

  public float aspectRatio { get; private set; }

  private GraphicsResolution()
  {
  }

  private GraphicsResolution(int width, int height)
  {
    this.x = width;
    this.y = height;
    this.aspectRatio = (float) this.x / (float) this.y;
  }

  public static GraphicsResolution create(Resolution res)
  {
    return new GraphicsResolution(res.width, res.height);
  }

  public static GraphicsResolution create(int width, int height)
  {
    return new GraphicsResolution(width, height);
  }

  private static bool add(int width, int height)
  {
    GraphicsResolution graphicsResolution = new GraphicsResolution(width, height);
    if (GraphicsResolution.resolutions_.BinarySearch(graphicsResolution) >= 0)
      return false;
    GraphicsResolution.resolutions_.Add(graphicsResolution);
    GraphicsResolution.resolutions_.Sort();
    return true;
  }

  public int CompareTo(object obj)
  {
    GraphicsResolution graphicsResolution = obj as GraphicsResolution;
    if (graphicsResolution == null)
      return 1;
    if (this.x < graphicsResolution.x)
      return -1;
    if (this.x > graphicsResolution.x)
      return 1;
    if (this.y < graphicsResolution.y)
      return -1;
    return this.y > graphicsResolution.y ? 1 : 0;
  }

  public override bool Equals(object obj)
  {
    if (obj == null)
      return false;
    GraphicsResolution graphicsResolution = obj as GraphicsResolution;
    if (graphicsResolution == null || this.x != graphicsResolution.x)
      return false;
    return this.y == graphicsResolution.y;
  }

  public override int GetHashCode()
  {
    return (23 * 17 + this.x.GetHashCode()) * 17 + this.y.GetHashCode();
  }
}
