﻿// Decompiled with JetBrains decompiler
// Type: WeaponSocketRequirement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class WeaponSocketRequirement
{
  public Player.Side m_Side;
  public bool m_HasWeapon;
}
