﻿// Decompiled with JetBrains decompiler
// Type: KAR10_Aran
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR10_Aran : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Aran_Male_Shade_AranEmoteResponse_01");
    this.PreloadSound("VO_Aran_Male_Shade_AranCrackle_01");
    this.PreloadSound("VO_Aran_Male_Shade_AranHeroPower_01");
    this.PreloadSound("VO_Aran_Male_Shade_AranTurn1_01");
    this.PreloadSound("VO_Aran_Male_Shade_AranTurn5_01");
    this.PreloadSound("VO_Aran_Male_Shade_AranSpell_03");
    this.PreloadSound("VO_Aran_Male_Shade_AranSpell_04");
    this.PreloadSound("VO_Aran_Male_Shade_AranMedivhSkin_01");
    this.PreloadSound("VO_Aran_Male_Shade_AranTrons_02");
    this.PreloadSound("VO_Moroes_Male_Human_AranWin_01");
    this.PreloadSound("VO_Moroes_Male_Human_AranTurn1_01");
    this.PreloadSound("VO_Moroes_Male_Human_AranTurn9_04");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Aran_Male_Shade_AranEmoteResponse_01",
            m_stringTag = "VO_Aran_Male_Shade_AranEmoteResponse_01"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR10_Aran.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1B1() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR10_Aran.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1B2() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR10_Aran.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1B3() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR10_Aran.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator1B4() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR10_Aran.\u003CHandleGameOverWithTiming\u003Ec__Iterator1B5() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
