﻿// Decompiled with JetBrains decompiler
// Type: CardBurstRare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CardBurstRare : Spell
{
  public string m_EdgeGlowBirthAnimation = "StandardEdgeGlowFade_Forge";
  public string m_EdgeGlowDeathAnimation = "StandardEdgeGlowFadeOut_Forge";
  public GameObject m_RenderPlane;
  public GameObject m_RaysMask;
  public GameObject m_EdgeGlow;
  public ParticleSystem m_Bang;
  public ParticleSystem m_BangLinger;
  public ParticleSystem m_BurstMotes;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    if ((bool) ((Object) this.m_RenderPlane))
      this.m_RenderPlane.SetActive(true);
    if ((bool) ((Object) this.m_RaysMask))
      this.m_RaysMask.SetActive(true);
    if ((bool) ((Object) this.m_EdgeGlow))
    {
      this.m_EdgeGlow.GetComponent<Renderer>().enabled = true;
      this.m_EdgeGlow.GetComponent<Animation>().Play(this.m_EdgeGlowBirthAnimation, PlayMode.StopAll);
    }
    if ((bool) ((Object) this.m_BurstMotes))
      this.m_BurstMotes.Play();
    if ((bool) ((Object) this.m_Bang))
      this.m_Bang.Play();
    if ((bool) ((Object) this.m_BangLinger))
      this.m_BangLinger.Play();
    this.OnSpellFinished();
  }

  protected override void OnDeath(SpellStateType prevStateType)
  {
    if ((bool) ((Object) this.m_EdgeGlow))
      this.m_EdgeGlow.GetComponent<Animation>().Play(this.m_EdgeGlowDeathAnimation, PlayMode.StopAll);
    this.StartCoroutine(this.DeathState());
  }

  [DebuggerHidden]
  private IEnumerator DeathState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBurstRare.\u003CDeathState\u003Ec__Iterator2AD() { \u003C\u003Ef__this = this };
  }
}
