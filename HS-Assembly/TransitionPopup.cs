﻿// Decompiled with JetBrains decompiler
// Type: TransitionPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public abstract class TransitionPopup : MonoBehaviour
{
  public Vector3_MobileOverride m_startPosition = new Vector3_MobileOverride(new Vector3(-0.05f, 8.2f, -1.8f));
  protected List<TransitionPopup.MatchCanceledEvent> m_matchCanceledListeners = new List<TransitionPopup.MatchCanceledEvent>();
  private float POPUP_TIME = 0.3f;
  private float START_SCALE_VAL = 0.1f;
  public UberText m_title;
  public MatchingQueueTab m_queueTab;
  public UIBButton m_cancelButton;
  public Float_MobileOverride m_endScale;
  public Float_MobileOverride m_scaleAfterPunch;
  protected bool m_shown;
  protected bool m_blockingLoadingScreen;
  protected Camera m_fullScreenEffectsCamera;
  protected AdventureDbId m_adventureId;
  protected FormatType m_formatType;
  protected bool m_showAnimationFinished;
  private Vector3 END_POSITION;
  private bool m_blurEnabled;

  public event Action<TransitionPopup> OnHidden;

  public void SetAdventureId(AdventureDbId adventureId)
  {
    this.m_adventureId = adventureId;
  }

  public void SetFormatType(FormatType formatType)
  {
    this.m_formatType = formatType;
  }

  protected virtual void Awake()
  {
    this.m_fullScreenEffectsCamera = Camera.main;
    this.m_cancelButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCancelButtonReleased));
    this.m_cancelButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnCancelButtonOver));
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    this.gameObject.transform.localPosition = (Vector3) ((MobileOverrideValue<Vector3>) this.m_startPosition);
  }

  protected virtual void Start()
  {
    if ((UnityEngine.Object) this.m_fullScreenEffectsCamera == (UnityEngine.Object) null)
      this.m_fullScreenEffectsCamera = Camera.main;
    if (this.m_shown)
      return;
    iTween.FadeTo(this.gameObject, 0.0f, 0.0f);
    this.gameObject.SetActive(false);
  }

  protected virtual void OnDestroy()
  {
    if ((bool) ((UnityEngine.Object) FullScreenFXMgr.Get()))
      this.DisableFullScreenBlur();
    this.StopBlockingTransition();
    GameMgr.Get().UnregisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    if ((UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null)
      SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    if (!this.m_shown || this.OnHidden == null)
      return;
    this.OnHidden(this);
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  public void Show()
  {
    if (this.m_shown)
      return;
    this.AnimateShow();
  }

  public void Hide()
  {
    if (!this.m_shown)
      return;
    this.AnimateHide();
  }

  public void Cancel()
  {
    if (!this.m_shown || (UnityEngine.Object) this.m_fullScreenEffectsCamera == (UnityEngine.Object) null)
      return;
    this.DisableFullScreenBlur();
  }

  public void RegisterMatchCanceledEvent(TransitionPopup.MatchCanceledEvent callback)
  {
    this.m_matchCanceledListeners.Add(callback);
  }

  public bool UnregisterMatchCanceledEvent(TransitionPopup.MatchCanceledEvent callback)
  {
    return this.m_matchCanceledListeners.Remove(callback);
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    if (!this.m_shown)
      return false;
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_ERROR:
        this.OnGameError(eventData);
        break;
      case FindGameState.BNET_QUEUE_ENTERED:
        this.OnGameEntered(eventData);
        break;
      case FindGameState.BNET_QUEUE_DELAYED:
        this.OnGameDelayed(eventData);
        break;
      case FindGameState.BNET_QUEUE_UPDATED:
        this.OnGameUpdated(eventData);
        break;
      case FindGameState.BNET_QUEUE_CANCELED:
        this.OnGameCanceled(eventData);
        break;
      case FindGameState.SERVER_GAME_CONNECTING:
        this.OnGameConnecting(eventData);
        break;
      case FindGameState.SERVER_GAME_STARTED:
        this.OnGameStarted(eventData);
        break;
    }
    return false;
  }

  protected virtual void OnGameEntered(FindGameEventData eventData)
  {
    this.m_queueTab.UpdateDisplay(eventData.m_queueMinSeconds, eventData.m_queueMaxSeconds);
  }

  protected virtual void OnGameDelayed(FindGameEventData eventData)
  {
  }

  protected virtual void OnGameUpdated(FindGameEventData eventData)
  {
    this.m_queueTab.UpdateDisplay(eventData.m_queueMinSeconds, eventData.m_queueMaxSeconds);
  }

  protected virtual void OnGameConnecting(FindGameEventData eventData)
  {
    this.DisableCancelButton();
  }

  protected virtual void OnGameStarted(FindGameEventData eventData)
  {
    this.StartBlockingTransition();
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
  }

  protected virtual void OnGameCanceled(FindGameEventData eventData)
  {
  }

  protected virtual void OnGameError(FindGameEventData eventData)
  {
  }

  protected virtual bool EnableCancelButtonIfPossible()
  {
    if (!this.m_showAnimationFinished || GameMgr.Get().IsAboutToStopFindingGame() || this.m_cancelButton.IsEnabled())
      return false;
    this.EnableCancelButton();
    return true;
  }

  protected virtual void EnableCancelButton()
  {
    this.m_cancelButton.Flip(true);
    this.m_cancelButton.SetEnabled(true);
  }

  protected virtual void DisableCancelButton()
  {
    this.m_cancelButton.Flip(false);
    this.m_cancelButton.SetEnabled(false);
  }

  protected virtual void OnCancelButtonReleased(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("Back_Click");
    this.m_cancelButton.SetEnabled(false);
  }

  protected virtual void OnCancelButtonOver(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("Small_Mouseover");
  }

  protected void FireMatchCanceledEvent()
  {
    TransitionPopup.MatchCanceledEvent[] array = this.m_matchCanceledListeners.ToArray();
    if (array.Length == 0)
      UnityEngine.Debug.LogError((object) "TransitionPopup.FireMatchCanceledEvent() - Cancel triggered, but nobody was listening!!");
    foreach (TransitionPopup.MatchCanceledEvent matchCanceledEvent in array)
      matchCanceledEvent();
  }

  protected virtual void AnimateShow()
  {
    iTween.Stop(this.gameObject);
    this.m_shown = true;
    this.m_showAnimationFinished = false;
    this.gameObject.SetActive(true);
    SceneUtils.EnableRenderers(this.gameObject, false);
    this.DisableCancelButton();
    this.ShowPopup();
    this.AnimateBlurBlendOn();
  }

  protected virtual void ShowPopup()
  {
    SceneUtils.EnableRenderers(this.gameObject, true);
    iTween.FadeTo(this.gameObject, 1f, this.POPUP_TIME);
    this.gameObject.transform.localScale = new Vector3(this.START_SCALE_VAL, this.START_SCALE_VAL, this.START_SCALE_VAL);
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) new Vector3((float) ((MobileOverrideValue<float>) this.m_endScale), (float) ((MobileOverrideValue<float>) this.m_endScale), (float) ((MobileOverrideValue<float>) this.m_endScale)), (object) "time", (object) this.POPUP_TIME, (object) "oncomplete", (object) "PunchPopup", (object) "oncompletetarget", (object) this.gameObject));
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) (this.gameObject.transform.localPosition + new Vector3(0.02f, 0.02f, 0.02f)), (object) "time", (object) 1.5f, (object) "islocal", (object) true));
    this.m_queueTab.ResetTimer();
  }

  private void PunchPopup()
  {
    iTween.ScaleTo(this.gameObject, new Vector3((float) ((MobileOverrideValue<float>) this.m_scaleAfterPunch), (float) ((MobileOverrideValue<float>) this.m_scaleAfterPunch), (float) ((MobileOverrideValue<float>) this.m_scaleAfterPunch)), 0.15f);
    this.OnAnimateShowFinished();
  }

  protected virtual void OnAnimateShowFinished()
  {
    this.m_showAnimationFinished = true;
  }

  protected virtual void AnimateHide()
  {
    this.m_shown = false;
    this.DisableCancelButton();
    iTween.FadeTo(this.gameObject, 0.0f, this.POPUP_TIME);
    Hashtable args = iTween.Hash((object) "scale", (object) new Vector3(this.START_SCALE_VAL, this.START_SCALE_VAL, this.START_SCALE_VAL), (object) "time", (object) this.POPUP_TIME);
    if (this.OnHidden != null)
      args[(object) "oncomplete"] = (object) (Action<object>) (data => this.OnHidden(this));
    iTween.ScaleTo(this.gameObject, args);
    this.AnimateBlurBlendOff();
  }

  private void AnimateBlurBlendOn()
  {
    this.EnableFullScreenBlur();
  }

  protected void AnimateBlurBlendOff()
  {
    this.DisableFullScreenBlur();
    this.StartCoroutine(this.DelayDeactivatePopup(0.5f));
  }

  [DebuggerHidden]
  private IEnumerator DelayDeactivatePopup(float waitTime)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TransitionPopup.\u003CDelayDeactivatePopup\u003Ec__IteratorF1() { waitTime = waitTime, \u003C\u0024\u003EwaitTime = waitTime, \u003C\u003Ef__this = this };
  }

  protected void DeactivatePopup()
  {
    this.gameObject.SetActive(false);
    this.StopBlockingTransition();
  }

  protected void StartBlockingTransition()
  {
    this.m_blockingLoadingScreen = true;
    LoadingScreen.Get().AddTransitionBlocker();
    LoadingScreen.Get().AddTransitionObject(this.gameObject);
  }

  protected void StopBlockingTransition()
  {
    if (!this.m_blockingLoadingScreen)
      return;
    this.m_blockingLoadingScreen = false;
    if (!(bool) ((UnityEngine.Object) LoadingScreen.Get()))
      return;
    LoadingScreen.Get().NotifyTransitionBlockerComplete();
  }

  protected virtual void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (!this.m_shown)
      return;
    SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    this.OnGameplaySceneLoaded();
  }

  private void EnableFullScreenBlur()
  {
    if (this.m_blurEnabled)
      return;
    this.m_blurEnabled = true;
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.SetBlurAmount(0.3f);
    fullScreenFxMgr.SetBlurBrightness(0.4f);
    fullScreenFxMgr.SetBlurDesaturation(0.5f);
    fullScreenFxMgr.Blur(1f, 0.5f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void DisableFullScreenBlur()
  {
    if (!this.m_blurEnabled)
      return;
    this.m_blurEnabled = false;
    FullScreenFXMgr.Get().StopBlur(0.5f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  protected abstract void OnGameplaySceneLoaded();

  public delegate void MatchCanceledEvent();
}
