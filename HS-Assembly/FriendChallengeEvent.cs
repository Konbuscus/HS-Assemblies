﻿// Decompiled with JetBrains decompiler
// Type: FriendChallengeEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum FriendChallengeEvent
{
  I_SENT_CHALLENGE,
  I_RESCINDED_CHALLENGE,
  OPPONENT_ACCEPTED_CHALLENGE,
  OPPONENT_DECLINED_CHALLENGE,
  I_RECEIVED_CHALLENGE,
  I_ACCEPTED_CHALLENGE,
  I_DECLINED_CHALLENGE,
  OPPONENT_RESCINDED_CHALLENGE,
  SELECTED_DECK,
  DESELECTED_DECK,
  OPPONENT_CANCELED_CHALLENGE,
  OPPONENT_REMOVED_FROM_FRIENDS,
}
