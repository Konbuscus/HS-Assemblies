﻿// Decompiled with JetBrains decompiler
// Type: BnetBar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CustomEditClass]
public class BnetBar : MonoBehaviour
{
  public static readonly int CameraDepth = 47;
  private bool m_isInitting = true;
  private float m_lightingBlend = 1f;
  private bool m_isEnabled = true;
  public UberText m_currentTime;
  public BnetBarMenuButton m_menuButton;
  public GameObject m_menuButtonMesh;
  public BnetBarFriendButton m_friendButton;
  public CurrencyFrame m_currencyFrame;
  public Flipbook m_batteryLevel;
  public Flipbook m_batteryLevelPhone;
  public GameObject m_socialToastBone;
  public GameObject m_questProgressToastBone;
  public ConnectionIndicator m_connectionIndicator;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_spectatorCountPrefabPath;
  public TooltipZone m_spectatorCountTooltipZone;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_spectatorModeIndicatorPrefab;
  private static BnetBar s_instance;
  private float m_initialWidth;
  private float m_initialFriendButtonScaleX;
  private float m_initialMenuButtonScaleX;
  private float m_initialConnectionIndicatorScaleX;
  private GameMenu m_gameMenu;
  private bool m_gameMenuLoading;
  private GameObject m_loginTooltip;
  private bool m_hasUnacknowledgedPendingInvites;
  private GameObject m_spectatorCountPanel;
  private GameObject m_spectatorModeIndicator;
  private bool m_isLoggedIn;
  private bool m_shouldDisableButtons;
  private bool m_buttonToggleState;
  private bool m_isInitialized;
  private bool m_suppressLoginTooltip;
  private float m_lastClockUpdate;

  private void Awake()
  {
    BnetBar.s_instance = this;
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_menuButton.transform.localScale *= 2f;
      this.m_friendButton.transform.localScale *= 2f;
    }
    else
      this.m_connectionIndicator.gameObject.SetActive(false);
    this.m_initialWidth = this.GetComponent<Renderer>().bounds.size.x;
    this.m_initialFriendButtonScaleX = this.m_friendButton.transform.localScale.x;
    this.m_initialMenuButtonScaleX = this.m_menuButton.transform.localScale.x;
    this.m_menuButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnMenuButtonReleased));
    this.m_friendButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnFriendButtonReleased));
    this.ToggleEnableButtons(false);
    this.m_batteryLevel.gameObject.SetActive(false);
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    SpectatorManager.Get().OnInviteReceived += new SpectatorManager.InviteReceivedHandler(this.SpectatorManager_OnInviteReceived);
    SpectatorManager.Get().OnSpectatorToMyGame += new SpectatorManager.SpectatorToMyGameHandler(this.SpectatorManager_OnSpectatorToMyGame);
    SpectatorManager.Get().OnSpectatorModeChanged += new SpectatorManager.SpectatorModeChangedHandler(this.SpectatorManager_OnSpectatorModeChanged);
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
    this.m_lightingBlend = this.m_menuButtonMesh.GetComponent<Renderer>().material.GetFloat("_LightingBlend");
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_batteryLevel = this.m_batteryLevelPhone;
      this.m_currentTime.gameObject.SetActive(false);
    }
    this.m_menuButton.SetPhoneStatusBarState(0);
  }

  private void OnDestroy()
  {
    SpectatorManager.Get().OnInviteReceived -= new SpectatorManager.InviteReceivedHandler(this.SpectatorManager_OnInviteReceived);
    SpectatorManager.Get().OnSpectatorToMyGame -= new SpectatorManager.SpectatorToMyGameHandler(this.SpectatorManager_OnSpectatorToMyGame);
    SpectatorManager.Get().OnSpectatorModeChanged -= new SpectatorManager.SpectatorModeChangedHandler(this.SpectatorManager_OnSpectatorModeChanged);
    ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
    BnetBar.s_instance = (BnetBar) null;
  }

  private void Start()
  {
    this.m_friendButton.gameObject.SetActive(false);
    this.m_hasUnacknowledgedPendingInvites = SpectatorManager.Get().HasAnyReceivedInvites();
    if ((UnityEngine.Object) this.m_friendButton != (UnityEngine.Object) null)
      this.m_friendButton.ShowPendingInvitesIcon(this.m_hasUnacknowledgedPendingInvites);
    this.ToggleActive(false);
  }

  private void Update()
  {
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if ((double) realtimeSinceStartup - (double) this.m_lastClockUpdate > 1.0)
    {
      this.m_lastClockUpdate = realtimeSinceStartup;
      if (Localization.GetLocale() == Locale.enGB)
        this.m_currentTime.Text = string.Format("{0:HH:mm}", (object) DateTime.Now);
      else
        this.m_currentTime.Text = GameStrings.Format("GLOBAL_CURRENT_TIME", (object) DateTime.Now);
    }
    if (this.m_isInitialized && this.m_shouldDisableButtons == this.m_buttonToggleState)
      this.ToggleEnableButtons(!this.m_shouldDisableButtons);
    this.m_shouldDisableButtons = false;
  }

  public static BnetBar Get()
  {
    return BnetBar.s_instance;
  }

  private void WillReset()
  {
    if ((UnityEngine.Object) this.m_gameMenu != (UnityEngine.Object) null)
    {
      if (this.m_gameMenu.IsShown())
        this.m_gameMenu.Hide();
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_gameMenu.gameObject);
      this.m_gameMenu = (GameMenu) null;
    }
    this.DestroyLoginTooltip();
    this.ToggleActive(false);
    this.m_isLoggedIn = false;
  }

  public void OnLoggedIn()
  {
    if (Network.ShouldBeConnectedToAurora())
      this.m_friendButton.gameObject.SetActive(true);
    this.m_isLoggedIn = true;
    this.ToggleActive(true);
    this.Update();
    this.UpdateLayout();
  }

  public void UpdateLayout()
  {
    if (!this.m_isLoggedIn)
      return;
    float num1 = 0.5f;
    Bounds nearClipBounds = CameraUtils.GetNearClipBounds(PegUI.Get().orthographicUICam);
    float x1 = (nearClipBounds.size.x + num1) / this.m_initialWidth;
    TransformUtil.SetLocalPosX(this.gameObject, nearClipBounds.min.x - this.transform.parent.localPosition.x - num1);
    TransformUtil.SetLocalScaleX(this.gameObject, x1);
    Vector3 zero = Vector3.zero;
    float x2 = -0.03f * x1;
    if (GeneralUtils.IsDevelopmentBuildTextVisible())
      x2 -= CameraUtils.ScreenToWorldDist(PegUI.Get().orthographicUICam, 115f);
    float y1 = 1f * this.transform.localScale.y;
    bool flag1 = true;
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.GAMEPLAY && !DemoMgr.Get().IsHubEscMenuEnabled())
      flag1 = false;
    this.m_menuButton.gameObject.SetActive(flag1);
    TransformUtil.SetLocalScaleX((Component) this.m_menuButton, this.m_initialMenuButtonScaleX / x1);
    TransformUtil.SetPoint((Component) this.m_menuButton, Anchor.RIGHT, this.gameObject, Anchor.RIGHT, new Vector3(x2, y1, 0.0f) - zero);
    float y2 = 1.5f;
    float y3 = -4f;
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      y2 = 190f;
      y3 = 0.0f;
    }
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      TransformUtil.SetPoint((Component) this.m_menuButton, Anchor.RIGHT, this.gameObject, Anchor.RIGHT, new Vector3(x2, y1, 0.0f));
      TransformUtil.SetLocalPosX((Component) this.m_menuButton, this.m_menuButton.transform.localPosition.x + 0.05f);
      TransformUtil.SetLocalPosY((Component) this.m_menuButton, y2);
      this.m_batteryLevel.gameObject.SetActive(true);
      this.m_menuButton.SetPhoneStatusBarState(1 + (!this.m_connectionIndicator.IsVisible() ? 0 : 1));
      TransformUtil.SetLocalScaleX((Component) this.m_currencyFrame, 2f / x1);
      TransformUtil.SetLocalScaleY((Component) this.m_currencyFrame, 0.4f);
      if (this.m_menuButton.gameObject.activeInHierarchy)
        this.PositionCurrencyFrame(this.m_batteryLevel.gameObject, new Vector3(-35f, y3, 0.0f));
      else
        this.PositionCurrencyFrame(this.m_batteryLevel.gameObject, new Vector3(100f, y3, 0.0f));
    }
    else
    {
      TransformUtil.SetPoint((Component) this.m_menuButton, Anchor.RIGHT, this.gameObject, Anchor.RIGHT, new Vector3(x2, y1, 0.0f));
      TransformUtil.SetLocalScaleX((Component) this.m_currencyFrame, 1f / x1);
      this.PositionCurrencyFrame(this.m_menuButton.gameObject, new Vector3(-25f, -5f, 0.0f));
    }
    MultiSliceElement componentInChildren = this.m_currencyFrame.GetComponentInChildren<MultiSliceElement>();
    if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
      componentInChildren.UpdateSlices();
    bool flag2 = (UnityEngine.Object) this.m_spectatorCountPanel != (UnityEngine.Object) null && this.m_spectatorCountPanel.activeInHierarchy && SpectatorManager.Get().IsBeingSpectated();
    bool flag3 = (UnityEngine.Object) this.m_spectatorModeIndicator != (UnityEngine.Object) null && this.m_spectatorModeIndicator.activeInHierarchy && SpectatorManager.Get().IsInSpectatorMode();
    if ((bool) UniversalInputManager.UsePhoneUI && (UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null && !SceneMgr.Get().IsInGame())
    {
      flag2 = false;
      flag3 = false;
    }
    bool flag4 = flag2 || flag3;
    if (this.m_friendButton.gameObject.activeInHierarchy)
    {
      TransformUtil.SetLocalScaleX((Component) this.m_friendButton, this.m_initialFriendButtonScaleX / x1);
      TransformUtil.SetPoint((Component) this.m_friendButton, Anchor.LEFT, this.gameObject, Anchor.LEFT, new Vector3(6f, 5f, 0.0f));
      TransformUtil.SetLocalScaleX((Component) this.m_currentTime, 1f / x1);
      TransformUtil.SetLocalScaleX(this.m_socialToastBone, 1f / x1);
      if ((bool) UniversalInputManager.UsePhoneUI)
        TransformUtil.SetLocalPosY((Component) this.m_friendButton, y2);
      if (!flag4)
      {
        TransformUtil.SetPoint((Component) this.m_currentTime, Anchor.LEFT, (Component) this.m_friendButton, Anchor.RIGHT, new Vector3(22f, y3, 0.0f) + zero);
        TransformUtil.SetPoint(this.m_socialToastBone, Anchor.LEFT, (Component) this.m_friendButton, Anchor.RIGHT, new Vector3(15f, 0.0f, -1f) + zero);
      }
      else if (flag2)
      {
        if ((bool) UniversalInputManager.UsePhoneUI)
        {
          TransformUtil.SetPoint(this.m_spectatorCountPanel, Anchor.LEFT, this.gameObject, Anchor.LEFT, new Vector3(6f, 5f, 0.0f));
        }
        else
        {
          TransformUtil.SetPoint(this.m_spectatorCountPanel, Anchor.LEFT, (Component) this.m_friendButton, Anchor.RIGHT, new Vector3(8f, 0.0f, 0.0f) + zero);
          TransformUtil.SetPoint(this.m_socialToastBone, Anchor.LEFT, this.m_spectatorCountPanel, Anchor.RIGHT, new Vector3(7f, 0.0f, -1f) + zero);
        }
        TransformUtil.SetPoint((Component) this.m_currentTime, Anchor.LEFT, this.m_spectatorCountPanel, Anchor.RIGHT, new Vector3(14f, -4f, 0.0f));
      }
      else if (flag3)
      {
        if ((bool) UniversalInputManager.UsePhoneUI)
        {
          TransformUtil.SetPoint(this.m_spectatorModeIndicator, Anchor.LEFT, this.gameObject, Anchor.LEFT, new Vector3(6f, 5f, 0.0f));
        }
        else
        {
          TransformUtil.SetPoint(this.m_spectatorModeIndicator, Anchor.LEFT, (Component) this.m_friendButton, Anchor.RIGHT, new Vector3(8f, 0.0f, 0.0f) + zero);
          TransformUtil.SetPoint(this.m_socialToastBone, Anchor.LEFT, this.m_spectatorModeIndicator, Anchor.RIGHT, new Vector3(7f, 0.0f, -1f) + zero);
        }
        TransformUtil.SetPoint((Component) this.m_currentTime, Anchor.LEFT, this.m_spectatorModeIndicator, Anchor.RIGHT, new Vector3(14f, -4f, 0.0f));
      }
      float num2 = 1f;
      if ((bool) UniversalInputManager.UsePhoneUI)
        num2 = 2.5f;
      TransformUtil.SetLocalScaleX(this.m_questProgressToastBone, num2 / x1);
    }
    else
    {
      GameObject gameObject1 = this.gameObject;
      GameObject gameObject2;
      if (flag4)
      {
        TransformUtil.SetPoint(this.m_spectatorCountPanel, Anchor.LEFT, gameObject1, Anchor.RIGHT, new Vector3(0.0f, 0.0f, 0.0f));
        gameObject2 = this.m_spectatorCountPanel;
      }
      else if (flag3)
      {
        TransformUtil.SetPoint(this.m_spectatorModeIndicator, Anchor.LEFT, gameObject1, Anchor.RIGHT, new Vector3(0.0f, 0.0f, 0.0f) + zero);
        gameObject2 = this.m_spectatorModeIndicator;
      }
      TransformUtil.SetLocalScaleX((Component) this.m_currentTime, 1f / x1);
      TransformUtil.SetPoint((Component) this.m_currentTime, Anchor.LEFT, this.gameObject, Anchor.LEFT, new Vector3(6f, 5f, 0.0f) + zero);
    }
    this.UpdateLoginTooltip();
    if (!this.m_isInitting)
      return;
    this.m_currencyFrame.DeactivateCurrencyFrame();
    this.m_isInitting = false;
  }

  private void PositionCurrencyFrame(GameObject parent, Vector3 offset)
  {
    GameObject tooltipObject = this.m_currencyFrame.GetTooltipObject();
    if ((UnityEngine.Object) tooltipObject != (UnityEngine.Object) null)
      tooltipObject.SetActive(false);
    TransformUtil.SetPoint((Component) this.m_currencyFrame, Anchor.RIGHT, parent, Anchor.LEFT, offset, false);
    if (!((UnityEngine.Object) tooltipObject != (UnityEngine.Object) null))
      return;
    tooltipObject.SetActive(true);
  }

  public bool HandleKeyboardInput()
  {
    if (Input.GetKeyUp(BackButton.backKey) || Input.GetKeyUp(KeyCode.Escape))
      return this.HandleEscapeKey();
    ChatMgr chatMgr = ChatMgr.Get();
    return (UnityEngine.Object) chatMgr != (UnityEngine.Object) null && chatMgr.HandleKeyboardInput();
  }

  public void ToggleGameMenu()
  {
    if ((UnityEngine.Object) this.m_gameMenu == (UnityEngine.Object) null)
    {
      if (this.m_gameMenuLoading)
        return;
      this.m_gameMenuLoading = true;
      AssetLoader.Get().LoadGameObject("GameMenu", new AssetLoader.GameObjectCallback(this.ShowGameMenu), (object) null, false);
    }
    else if (this.m_gameMenu.IsShown())
      this.m_gameMenu.Hide();
    else
      this.m_gameMenu.Show();
  }

  public void ToggleEnableButtons(bool enabled)
  {
    this.m_menuButton.SetEnabled(enabled);
    this.m_friendButton.SetEnabled(enabled);
    this.m_buttonToggleState = enabled;
  }

  public void ToggleFriendsButton(bool enabled)
  {
    this.m_friendButton.gameObject.SetActive(enabled);
  }

  public void ToggleActive(bool active)
  {
    this.gameObject.SetActive(active);
  }

  public void Enable()
  {
    this.m_isEnabled = true;
    this.m_menuButtonMesh.GetComponent<Renderer>().sharedMaterial.SetFloat("_LightingBlend", this.m_lightingBlend);
  }

  public void Disable()
  {
    this.m_isEnabled = false;
    this.m_menuButtonMesh.GetComponent<Renderer>().sharedMaterial.SetFloat("_LightingBlend", 0.6f);
    if ((UnityEngine.Object) this.m_gameMenu != (UnityEngine.Object) null && this.m_gameMenu.IsShown())
      this.m_gameMenu.Hide();
    if (!((UnityEngine.Object) OptionsMenu.Get() != (UnityEngine.Object) null) || !OptionsMenu.Get().IsShown())
      return;
    OptionsMenu.Get().Hide(true);
  }

  public bool IsEnabled()
  {
    return this.m_isEnabled;
  }

  public void SetCurrencyType(CurrencyFrame.CurrencyType? type)
  {
    this.m_currencyFrame.SetCurrencyOverride(type);
  }

  public void Init()
  {
    this.m_isInitialized = true;
  }

  public void RequestDisableButtons()
  {
    this.m_shouldDisableButtons = true;
  }

  public void UpdateLoginTooltip()
  {
    if (!Network.ShouldBeConnectedToAurora() && !this.m_suppressLoginTooltip && (SceneMgr.Get().IsInGame() && GameMgr.Get().IsTutorial()) && !GameMgr.Get().IsSpectator() && DemoMgr.Get().GetMode() != DemoMode.APPLE_STORE)
    {
      if ((UnityEngine.Object) this.m_loginTooltip == (UnityEngine.Object) null)
      {
        this.m_loginTooltip = AssetLoader.Get().LoadGameObject("LoginPointer", true, false);
        this.m_loginTooltip.transform.localScale = !(bool) UniversalInputManager.UsePhoneUI ? new Vector3(40f, 40f, 40f) : new Vector3(60f, 60f, 60f);
        TransformUtil.SetEulerAngleX(this.m_loginTooltip, 270f);
        SceneUtils.SetLayer(this.m_loginTooltip, GameLayer.BattleNet);
        this.m_loginTooltip.transform.parent = this.transform;
      }
      if ((bool) UniversalInputManager.UsePhoneUI)
        TransformUtil.SetPoint(this.m_loginTooltip, Anchor.RIGHT, this.m_batteryLevel.gameObject, Anchor.LEFT, new Vector3(-32f, 0.0f, 0.0f));
      else
        TransformUtil.SetPoint(this.m_loginTooltip, Anchor.RIGHT, (Component) this.m_menuButton, Anchor.LEFT, new Vector3(5f, 0.0f, 0.0f));
    }
    else
      this.DestroyLoginTooltip();
  }

  private void DestroyLoginTooltip()
  {
    if (!((UnityEngine.Object) this.m_loginTooltip != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_loginTooltip);
    this.m_loginTooltip = (GameObject) null;
  }

  public void SuppressLoginTooltip(bool val)
  {
    this.m_suppressLoginTooltip = val;
    this.UpdateLayout();
  }

  public void ShowFriendList()
  {
    ChatMgr.Get().ShowFriendsList();
    this.m_hasUnacknowledgedPendingInvites = false;
    this.m_friendButton.ShowPendingInvitesIcon(this.m_hasUnacknowledgedPendingInvites);
  }

  public void HideFriendList()
  {
    ChatMgr.Get().CloseChatUI();
  }

  public void UpdateForPhone()
  {
    int num;
    switch (SceneMgr.Get().GetMode())
    {
      case SceneMgr.Mode.HUB:
      case SceneMgr.Mode.LOGIN:
      case SceneMgr.Mode.GAMEPLAY:
        num = 1;
        break;
      case SceneMgr.Mode.ADVENTURE:
        num = ReturningPlayerMgr.Get().IsInReturningPlayerModeAndOnRails ? 1 : 0;
        break;
      default:
        num = 0;
        break;
    }
    this.m_menuButton.gameObject.SetActive(num != 0);
  }

  private bool HandleEscapeKey()
  {
    if ((UnityEngine.Object) this.m_gameMenu != (UnityEngine.Object) null && this.m_gameMenu.IsShown())
    {
      this.m_gameMenu.Hide();
      return true;
    }
    if ((UnityEngine.Object) OptionsMenu.Get() != (UnityEngine.Object) null && OptionsMenu.Get().IsShown())
    {
      OptionsMenu.Get().Hide(true);
      return true;
    }
    if ((UnityEngine.Object) QuestLog.Get() != (UnityEngine.Object) null && QuestLog.Get().IsShown())
    {
      QuestLog.Get().Hide();
      return true;
    }
    if ((UnityEngine.Object) GeneralStore.Get() != (UnityEngine.Object) null && GeneralStore.Get().IsShown())
    {
      GeneralStore.Get().Close();
      return true;
    }
    ChatMgr chatMgr = ChatMgr.Get();
    if ((UnityEngine.Object) chatMgr != (UnityEngine.Object) null && chatMgr.HandleKeyboardInput())
      return true;
    if ((UnityEngine.Object) CraftingTray.Get() != (UnityEngine.Object) null && CraftingTray.Get().IsShown())
    {
      CraftingTray.Get().Hide();
      return true;
    }
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    bool flag = PlatformSettings.OS == OSCategory.Android;
    if (mode == SceneMgr.Mode.FATAL_ERROR || mode == SceneMgr.Mode.LOGIN || mode == SceneMgr.Mode.STARTUP)
      return !flag;
    if (flag && mode == SceneMgr.Mode.HUB)
      return false;
    if (mode == SceneMgr.Mode.ADVENTURE && AdventureConfig.Get().GetCurrentSubScene() == AdventureSubScenes.ClassChallenge && AdventureConfig.Get().GetSelectedAdventure() == AdventureDbId.RETURNING_PLAYER)
      return !flag;
    if (mode != SceneMgr.Mode.GAMEPLAY && !DemoMgr.Get().IsHubEscMenuEnabled())
      return true;
    this.ToggleGameMenu();
    return true;
  }

  private void OnMenuButtonReleased(UIEvent e)
  {
    if (!GameMgr.Get().IsSpectator() && GameState.Get() != null && GameState.Get().IsInTargetMode())
      return;
    this.ToggleGameMenu();
  }

  private void ShowGameMenu(string name, GameObject go, object callbackData)
  {
    this.m_gameMenu = go.GetComponent<GameMenu>();
    this.m_gameMenu.GetComponent<GameMenu>().Show();
    this.m_gameMenuLoading = false;
  }

  private void UpdateForDemoMode()
  {
    if (!DemoMgr.Get().IsExpoDemo())
      return;
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    bool flag1 = true;
    bool flag2;
    switch (DemoMgr.Get().GetMode())
    {
      case DemoMode.PAX_EAST_2013:
      case DemoMode.BLIZZCON_2013:
      case DemoMode.BLIZZCON_2015:
        flag2 = mode == SceneMgr.Mode.GAMEPLAY;
        flag1 = false;
        this.m_currencyFrame.gameObject.SetActive(false);
        break;
      case DemoMode.BLIZZCON_2014:
        bool flag3 = mode != SceneMgr.Mode.FRIENDLY;
        flag2 = flag3;
        flag1 = flag3;
        break;
      case DemoMode.APPLE_STORE:
        flag2 = flag1 = false;
        break;
      case DemoMode.ANNOUNCEMENT_5_0:
        flag1 = true;
        flag2 = true;
        break;
      case DemoMode.BLIZZCON_2016:
        flag2 = mode == SceneMgr.Mode.GAMEPLAY;
        flag1 = mode == SceneMgr.Mode.HUB;
        break;
      default:
        flag2 = mode != SceneMgr.Mode.FRIENDLY && mode != SceneMgr.Mode.TOURNAMENT;
        break;
    }
    switch (mode)
    {
      case SceneMgr.Mode.GAMEPLAY:
      case SceneMgr.Mode.TOURNAMENT:
      case SceneMgr.Mode.FRIENDLY:
        if (DemoMgr.Get().GetMode() != DemoMode.ANNOUNCEMENT_5_0)
        {
          flag1 = false;
          break;
        }
        break;
    }
    if (!flag2)
      this.m_menuButton.gameObject.SetActive(false);
    if (flag1)
      return;
    this.m_friendButton.gameObject.SetActive(false);
  }

  private void OnFriendButtonReleased(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("Small_Click");
    this.ToggleFriendListShowing();
  }

  private void ToggleFriendListShowing()
  {
    if (ChatMgr.Get().IsFriendListShowing())
      this.HideFriendList();
    else
      this.ShowFriendList();
    this.m_friendButton.HideTooltip();
  }

  private void OnFatalError(FatalErrorMessage message, object userData)
  {
    this.ToggleEnableButtons(false);
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (mode == SceneMgr.Mode.FATAL_ERROR)
      return;
    this.m_suppressLoginTooltip = false;
    this.m_currencyFrame.RefreshContents();
    bool flag1 = mode != SceneMgr.Mode.INVALID && mode != SceneMgr.Mode.FATAL_ERROR;
    if (flag1 && SpectatorManager.Get().IsInSpectatorMode())
      this.SpectatorManager_OnSpectatorModeChanged(OnlineEventType.ADDED, (BnetPlayer) null);
    else if ((UnityEngine.Object) this.m_spectatorModeIndicator != (UnityEngine.Object) null && this.m_spectatorModeIndicator.activeSelf)
      this.m_spectatorModeIndicator.SetActive(false);
    if (flag1 && (UnityEngine.Object) this.m_spectatorCountPanel != (UnityEngine.Object) null)
    {
      bool flag2 = SpectatorManager.Get().IsBeingSpectated();
      if ((bool) UniversalInputManager.UsePhoneUI && (UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null && !SceneMgr.Get().IsInGame())
        flag2 = false;
      this.m_spectatorCountPanel.SetActive(flag2);
    }
    this.UpdateForDemoMode();
    this.UpdateLayout();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.UpdateForPhone();
  }

  private void SpectatorManager_OnInviteReceived(OnlineEventType evt, BnetPlayer inviter)
  {
    this.m_hasUnacknowledgedPendingInvites = !ChatMgr.Get().IsFriendListShowing() && SpectatorManager.Get().HasAnyReceivedInvites() && (this.m_hasUnacknowledgedPendingInvites || evt == OnlineEventType.ADDED);
    if (!((UnityEngine.Object) this.m_friendButton != (UnityEngine.Object) null))
      return;
    this.m_friendButton.ShowPendingInvitesIcon(this.m_hasUnacknowledgedPendingInvites);
  }

  private void SpectatorManager_OnSpectatorToMyGame(OnlineEventType evt, BnetPlayer spectator)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BnetBar.\u003CSpectatorManager_OnSpectatorToMyGame\u003Ec__AnonStorey363 gameCAnonStorey363 = new BnetBar.\u003CSpectatorManager_OnSpectatorToMyGame\u003Ec__AnonStorey363();
    // ISSUE: reference to a compiler-generated field
    gameCAnonStorey363.evt = evt;
    // ISSUE: reference to a compiler-generated field
    gameCAnonStorey363.spectator = spectator;
    int countSpectatingMe = SpectatorManager.Get().GetCountSpectatingMe();
    if (countSpectatingMe <= 0)
    {
      if ((UnityEngine.Object) this.m_spectatorCountPanel == (UnityEngine.Object) null)
        return;
    }
    else if ((UnityEngine.Object) this.m_spectatorCountPanel == (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated method
      AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(this.m_spectatorCountPrefabPath), new AssetLoader.GameObjectCallback(gameCAnonStorey363.\u003C\u003Em__47), (object) null, false);
      return;
    }
    this.m_spectatorCountPanel.transform.FindChild("UberText").GetComponent<UberText>().Text = countSpectatingMe.ToString();
    bool flag = countSpectatingMe > 0;
    if ((bool) UniversalInputManager.UsePhoneUI && (UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null && !SceneMgr.Get().IsInGame())
      flag = false;
    this.m_spectatorCountPanel.SetActive(flag);
    this.UpdateLayout();
    GameObject gameObject = this.m_spectatorCountPanel.transform.FindChild("BeingWatchedHighlight").gameObject;
    iTween.Stop(gameObject, true);
    Hashtable args = iTween.Hash((object) "alpha", (object) 1f, (object) "time", (object) 0.5f, (object) "oncomplete", (object) (Action<object>) (ud =>
    {
      if ((UnityEngine.Object) BnetBar.Get() == (UnityEngine.Object) null)
        return;
      iTween.FadeTo(BnetBar.Get().m_spectatorCountPanel.transform.FindChild("BeingWatchedHighlight").gameObject, 0.0f, 0.5f);
    }));
    iTween.FadeTo(gameObject, args);
  }

  private static void SpectatorCount_OnRollover(UIEvent evt)
  {
    BnetBar bnetBar = BnetBar.Get();
    if ((UnityEngine.Object) bnetBar == (UnityEngine.Object) null)
      return;
    string headline = GameStrings.Get("GLOBAL_SPECTATOR_COUNT_PANEL_HEADER");
    BnetGameAccountId[] spectatorPartyMembers = SpectatorManager.Get().GetSpectatorPartyMembers(true, false);
    string bodytext;
    if (spectatorPartyMembers.Length == 1)
      bodytext = GameStrings.Format("GLOBAL_SPECTATOR_COUNT_PANEL_TEXT_ONE", (object) BnetUtils.GetPlayerBestName(spectatorPartyMembers[0]));
    else
      bodytext = string.Join(", ", ((IEnumerable<BnetGameAccountId>) spectatorPartyMembers).Select<BnetGameAccountId, string>((Func<BnetGameAccountId, string>) (id => BnetUtils.GetPlayerBestName(id))).ToArray<string>());
    bnetBar.m_spectatorCountTooltipZone.ShowSocialTooltip(bnetBar.m_spectatorCountPanel, headline, bodytext, 75f, GameLayer.BattleNetDialog);
    bnetBar.m_spectatorCountTooltipZone.AnchorTooltipTo(bnetBar.m_spectatorCountPanel, Anchor.TOP_LEFT, Anchor.BOTTOM_LEFT);
  }

  private static void SpectatorCount_OnRollout(UIEvent evt)
  {
    BnetBar bnetBar = BnetBar.Get();
    if ((UnityEngine.Object) bnetBar == (UnityEngine.Object) null)
      return;
    bnetBar.m_spectatorCountTooltipZone.HideTooltip();
  }

  private void SpectatorManager_OnSpectatorModeChanged(OnlineEventType evt, BnetPlayer spectatee)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BnetBar.\u003CSpectatorManager_OnSpectatorModeChanged\u003Ec__AnonStorey364 changedCAnonStorey364 = new BnetBar.\u003CSpectatorManager_OnSpectatorModeChanged\u003Ec__AnonStorey364();
    // ISSUE: reference to a compiler-generated field
    changedCAnonStorey364.evt = evt;
    // ISSUE: reference to a compiler-generated field
    changedCAnonStorey364.spectatee = spectatee;
    // ISSUE: reference to a compiler-generated field
    if (changedCAnonStorey364.evt == OnlineEventType.ADDED && (UnityEngine.Object) this.m_spectatorModeIndicator == (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated method
      AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(this.m_spectatorModeIndicatorPrefab), new AssetLoader.GameObjectCallback(changedCAnonStorey364.\u003C\u003Em__4A), (object) null, false);
    }
    else
    {
      if ((UnityEngine.Object) this.m_spectatorModeIndicator == (UnityEngine.Object) null)
        return;
      // ISSUE: reference to a compiler-generated field
      bool flag = changedCAnonStorey364.evt == OnlineEventType.ADDED && SpectatorManager.Get().IsInSpectatorMode();
      if ((bool) UniversalInputManager.UsePhoneUI && (UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null && !SceneMgr.Get().IsInGame())
        flag = false;
      this.m_spectatorModeIndicator.SetActive(flag);
      this.UpdateLayout();
    }
  }
}
