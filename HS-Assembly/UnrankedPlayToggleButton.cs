﻿// Decompiled with JetBrains decompiler
// Type: UnrankedPlayToggleButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UnrankedPlayToggleButton : PegUIElement
{
  public GameObject m_xIcon;
  public HighlightState m_highlight;
  private bool m_isRankedMode;

  public void SetRankedMode(bool isRankedMode)
  {
    this.m_isRankedMode = isRankedMode;
    if (isRankedMode)
    {
      this.m_xIcon.SetActive(true);
      SoundManager.Get().LoadAndPlay("checkbox_toggle_off");
    }
    else
    {
      SoundManager.Get().LoadAndPlay("checkbox_toggle_on");
      this.m_xIcon.SetActive(false);
    }
  }

  public bool GetIsRanked()
  {
    return this.m_isRankedMode;
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    SoundManager.Get().LoadAndPlay("Small_Mouseover");
    if (!((Object) this.m_highlight != (Object) null))
      return;
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    if (!((Object) this.m_highlight != (Object) null))
      return;
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }
}
