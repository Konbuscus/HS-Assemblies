﻿// Decompiled with JetBrains decompiler
// Type: LOE_DeckTakeEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class LOE_DeckTakeEvent : MonoBehaviour
{
  public string m_takeDeckAnimName = "LOE_TakeDeck";
  public string m_replacementDeckAnimName = "CardsToPlayerDeck";
  public Renderer m_friendlyDeckRenderer;
  public Animator m_takeDeckAnimator;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_takeDeckSoundPrefab;
  public Animator m_replacementDeckAnimator;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_replacementDeckSoundPrefab;
  private bool m_animIsPlaying;

  private void Start()
  {
    CardBackManager.Get().SetCardBackTexture(this.m_friendlyDeckRenderer, 0, true);
  }

  [DebuggerHidden]
  public IEnumerator PlayTakeDeckAnim()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE_DeckTakeEvent.\u003CPlayTakeDeckAnim\u003Ec__Iterator186() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  public IEnumerator PlayReplacementDeckAnim()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE_DeckTakeEvent.\u003CPlayReplacementDeckAnim\u003Ec__Iterator187() { \u003C\u003Ef__this = this };
  }

  public bool AnimIsPlaying()
  {
    return this.m_animIsPlaying;
  }
}
