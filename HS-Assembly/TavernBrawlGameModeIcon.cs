﻿// Decompiled with JetBrains decompiler
// Type: TavernBrawlGameModeIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TavernBrawlGameModeIcon : PegUIElement
{
  public float m_tooltipScale = 1f;
  private string m_brawlName;
  private string m_brawlDescription;

  protected override void Awake()
  {
    base.Awake();
    if (!((Object) this.gameObject.GetComponent<TooltipZone>() != (Object) null))
      return;
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.MedalOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.MedalOut));
    TavernBrawlManager.Get().EnsureAllDataReady((TavernBrawlManager.CallbackEnsureServerDataReady) (() =>
    {
      int missionId = GameMgr.Get().GetMissionId();
      ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
      if (record == null)
        return;
      this.m_brawlName = (string) record.Name;
      this.m_brawlDescription = (string) record.Description;
    }));
  }

  public void MedalOver(UIEvent e)
  {
    if (string.IsNullOrEmpty(this.m_brawlName))
      return;
    this.gameObject.GetComponent<TooltipZone>().ShowLayerTooltip(this.m_brawlName, this.m_brawlDescription, this.m_tooltipScale);
  }

  private void MedalOut(UIEvent e)
  {
    this.gameObject.GetComponent<TooltipZone>().HideTooltip();
  }
}
