﻿// Decompiled with JetBrains decompiler
// Type: CardSpecificMultiVoData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[Serializable]
public class CardSpecificMultiVoData
{
  public SpellPlayerSide m_SideToSearch = SpellPlayerSide.TARGET;
  public List<SpellZoneTag> m_ZonesToSearch = new List<SpellZoneTag>() { SpellZoneTag.PLAY, SpellZoneTag.HERO, SpellZoneTag.HERO_POWER, SpellZoneTag.WEAPON };
  public string m_CardId;
  public CardSpecificMultiVoLine[] m_Lines;
}
