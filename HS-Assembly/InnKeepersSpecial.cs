﻿// Decompiled with JetBrains decompiler
// Type: InnKeepersSpecial
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using MiniJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class InnKeepersSpecial : MonoBehaviour
{
  public string adUrlOverride;
  public GameObject adImage;
  public GameObject adBackground;
  public PegUIElement adButton;
  public UberText adButtonText;
  public UberText adTitle;
  public UberText adSubtitle;
  public GameObject content;
  private string m_lastUrl;
  private JsonNode m_response;
  private JsonNode m_adToDisplay;
  private JsonNode m_adMetadata;
  private string m_url;
  private WWW m_textureWWW;
  private string m_link;
  private string m_gameAction;
  private GeneralStoreMode m_storeMode;
  private Dictionary<string, string> m_headers;
  private static InnKeepersSpecial s_instance;
  private bool m_loadedSuccessfully;
  private bool m_hasSeenResponse;
  private bool m_forceShowIks;
  private bool m_isShown;

  public bool IsShown
  {
    get
    {
      return this.m_isShown;
    }
  }

  public static InnKeepersSpecial Get()
  {
    InnKeepersSpecial.Init();
    return InnKeepersSpecial.s_instance;
  }

  public bool HasAlreadySeenResponse()
  {
    return this.m_hasSeenResponse;
  }

  public static void Init()
  {
    if (!((UnityEngine.Object) InnKeepersSpecial.s_instance == (UnityEngine.Object) null))
      return;
    InnKeepersSpecial.s_instance = AssetLoader.Get().LoadGameObject("InnKeepersSpecial", true, false).GetComponent<InnKeepersSpecial>();
    OverlayUI.Get().AddGameObject(InnKeepersSpecial.s_instance.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
  }

  public bool LoadedSuccessfully()
  {
    return this.m_loadedSuccessfully;
  }

  protected void Awake()
  {
    this.m_forceShowIks = Options.Get().GetBool(Option.FORCE_SHOW_IKS);
    this.Hide();
    this.m_headers = new Dictionary<string, string>();
    this.m_headers["Accept"] = "application/json";
    this.m_url = string.Format("https://nydus.battle.net/WTCG/{0}/client/ads?targetRegion={1}", (object) Localization.GetLocaleName(), BattleNet.GetCurrentRegion() != constants.BnetRegion.REGION_CN ? (object) "US" : (object) "CN");
    Log.InnKeepersSpecial.Print("Inkeeper Ad: " + this.m_url);
    this.m_link = (string) null;
    this.adButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.Click));
    this.Update();
  }

  public void Show()
  {
    float num = 0.5f;
    this.content.SetActive(true);
    Color color = this.adImage.gameObject.GetComponent<Renderer>().material.color;
    color.a = 0.0f;
    this.adImage.gameObject.GetComponent<Renderer>().material.color = color;
    iTween.FadeTo(this.adImage.gameObject, iTween.Hash((object) "amount", (object) 1f, (object) "time", (object) num, (object) "easeType", (object) iTween.EaseType.linear));
    this.adTitle.Show();
    iTween.ValueTo(this.adTitle.gameObject, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) num, (object) "easeType", (object) iTween.EaseType.linear, (object) "onupdate", (object) (Action<object>) (newVal => this.adTitle.TextAlpha = (float) newVal)));
    this.adSubtitle.Show();
    iTween.ValueTo(this.adSubtitle.gameObject, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) num, (object) "easeType", (object) iTween.EaseType.linear, (object) "onupdate", (object) (Action<object>) (newVal => this.adSubtitle.TextAlpha = (float) newVal)));
    this.m_isShown = true;
  }

  public void Hide()
  {
    this.content.SetActive(false);
    this.adTitle.Hide();
    this.adSubtitle.Hide();
    this.m_isShown = false;
  }

  public void Close()
  {
    this.Hide();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    InnKeepersSpecial.s_instance = (InnKeepersSpecial) null;
  }

  private void ShowStore(GeneralStoreMode mode = GeneralStoreMode.NONE)
  {
    this.m_storeMode = mode;
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.HUB)
      StoreManager.Get().StartGeneralTransaction(this.m_storeMode);
    else
      SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (mode != SceneMgr.Mode.HUB)
      return;
    StoreManager.Get().StartGeneralTransaction(this.m_storeMode);
    SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
  }

  private void Click(UIEvent e)
  {
    Log.InnKeepersSpecial.Print("IKS on release! " + this.m_link);
    if (this.m_gameAction != null)
    {
      bool flag = true;
      string lowerInvariant = this.m_gameAction.ToLowerInvariant();
      if (lowerInvariant.StartsWith("store"))
      {
        string[] strArray = lowerInvariant.Split(' ');
        if (strArray.Length > 1)
        {
          BoosterDbId boosterDbId = BoosterDbId.INVALID;
          AdventureDbId adventureDbId = AdventureDbId.INVALID;
          HeroDbId heroDbId = HeroDbId.INVALID;
          string str = strArray[1];
          try
          {
            boosterDbId = (BoosterDbId) Enum.Parse(typeof (BoosterDbId), str.ToUpper());
          }
          catch (ArgumentException ex)
          {
          }
          try
          {
            adventureDbId = (AdventureDbId) Enum.Parse(typeof (AdventureDbId), str.ToUpper());
          }
          catch (ArgumentException ex)
          {
          }
          try
          {
            heroDbId = (HeroDbId) Enum.Parse(typeof (HeroDbId), str.ToUpper());
          }
          catch (ArgumentException ex)
          {
          }
          if (boosterDbId != BoosterDbId.INVALID)
          {
            Options.Get().SetInt(Option.LAST_SELECTED_STORE_BOOSTER_ID, (int) boosterDbId);
            this.ShowStore(GeneralStoreMode.CARDS);
          }
          else if (adventureDbId != AdventureDbId.INVALID)
          {
            Options.Get().SetInt(Option.LAST_SELECTED_STORE_ADVENTURE_ID, (int) adventureDbId);
            this.ShowStore(GeneralStoreMode.ADVENTURE);
          }
          else if (heroDbId != HeroDbId.INVALID)
          {
            Options.Get().SetInt(Option.LAST_SELECTED_STORE_HERO_ID, (int) heroDbId);
            this.ShowStore(GeneralStoreMode.HEROES);
          }
          else
            this.ShowStore(GeneralStoreMode.NONE);
        }
        else
          this.ShowStore(GeneralStoreMode.NONE);
      }
      else if (lowerInvariant.Equals("recruitafriend"))
      {
        flag = false;
        RAFManager.Get().ShowRAFFrame();
      }
      if (!flag)
        return;
      WelcomeQuests.OnNavigateBack();
      this.Hide();
    }
    else
    {
      if (this.m_link == null)
        return;
      Application.OpenURL(this.m_link);
    }
  }

  private static int GetCacheAge(WWW www)
  {
    string str1;
    if (www.get_responseHeaders() != null && www.get_responseHeaders().TryGetValue("CACHE-CONTROL", out str1))
    {
      string str2 = str1;
      char[] chArray = new char[1]{ ',' };
      foreach (string str3 in str2.Split(chArray))
      {
        string str4 = str3.ToLowerInvariant().Trim();
        if (str4.StartsWith("max-age"))
        {
          string[] strArray = str4.Split('=');
          int result;
          if (strArray.Length == 2 && int.TryParse(strArray[1], out result))
            return result;
        }
      }
    }
    return -1;
  }

  [DebuggerHidden]
  private IEnumerator UpdateAdJson(string url)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new InnKeepersSpecial.\u003CUpdateAdJson\u003Ec__Iterator236() { url = url, \u003C\u0024\u003Eurl = url, \u003C\u003Ef__this = this };
  }

  private void Update()
  {
    string url = string.IsNullOrEmpty(this.adUrlOverride) ? this.m_url : this.adUrlOverride;
    if (!(url != this.m_lastUrl) || string.IsNullOrEmpty(url))
      return;
    this.m_lastUrl = url;
    this.m_link = (string) null;
    this.Hide();
    this.StartCoroutine(this.UpdateAdJson(url));
  }

  [DebuggerHidden]
  private IEnumerator UpdateAdTexture()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new InnKeepersSpecial.\u003CUpdateAdTexture\u003Ec__Iterator237() { \u003C\u003Ef__this = this };
  }

  private void UpdateText(JsonNode rawMetadata)
  {
    if (this.m_adMetadata.ContainsKey("gameAction"))
      this.m_gameAction = (string) this.m_adMetadata["gameAction"];
    if (this.m_adMetadata.ContainsKey("buttonText"))
    {
      this.adButtonText.GameStringLookup = false;
      this.adButtonText.Text = (string) this.m_adMetadata["buttonText"];
    }
    Vector3 localPosition1 = this.adTitle.transform.localPosition;
    if (this.m_adMetadata.ContainsKey("titleOffsetX"))
      localPosition1.x += (float) this.m_adMetadata["titleOffsetX"];
    if (this.m_adMetadata.ContainsKey("titleOffsetY"))
      localPosition1.y += (float) this.m_adMetadata["titleOffsetY"];
    this.adTitle.transform.localPosition = localPosition1;
    Vector3 localPosition2 = this.adSubtitle.transform.localPosition;
    if (this.m_adMetadata.ContainsKey("subtitleOffsetX"))
      localPosition2.x += (float) this.m_adMetadata["subtitleOffsetX"];
    if (this.m_adMetadata.ContainsKey("subtitleOffsetY"))
      localPosition2.y += (float) this.m_adMetadata["subtitleOffsetY"];
    this.adSubtitle.transform.localPosition = localPosition2;
    if (this.m_adMetadata.ContainsKey("titleFontSize"))
      this.adTitle.FontSize = (int) this.m_adMetadata["titleFontSize"];
    if (!this.m_adMetadata.ContainsKey("subtitleFontSize"))
      return;
    this.adSubtitle.FontSize = (int) this.m_adMetadata["subtitleFontSize"];
  }

  private JsonNode GetAdToDisplay(JsonNode response)
  {
    try
    {
      JsonNode jsonNode1 = (JsonNode) null;
      int num1 = 0;
      double num2 = 0.0;
      if (!response.ContainsKey("ads"))
        return (JsonNode) null;
      using (List<object>.Enumerator enumerator1 = ((List<object>) response["ads"]).GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          JsonNode current1 = enumerator1.Current as JsonNode;
          long num3 = (long) current1["importance"];
          if ((bool) current1["maxImportance"])
            num3 = 6L;
          long num4 = (long) current1["publish"];
          object obj = (object) null;
          if (current1.ContainsKey("metadata"))
            obj = current1["metadata"];
          JsonNode jsonNode2 = new JsonNode();
          if (obj != null)
          {
            using (List<object>.Enumerator enumerator2 = ((List<object>) obj).GetEnumerator())
            {
              while (enumerator2.MoveNext())
              {
                JsonNode current2 = enumerator2.Current as JsonNode;
                jsonNode2[(string) current2["key"]] = (object) (string) current2["value"];
              }
            }
          }
          bool flag1 = false;
          if (jsonNode2.ContainsKey("visibility") && StringUtils.CompareIgnoreCase((string) jsonNode2["visibility"], "public"))
            flag1 = true;
          if (!this.m_forceShowIks && ApplicationMgr.IsPublic() && !flag1)
            Log.InnKeepersSpecial.Print("Skipping IKS: {0}, not flagged as publicly visible", (object) (string) current1["campaignName"]);
          else if (jsonNode2.ContainsKey("clientVersion") && !this.m_forceShowIks && !StringUtils.CompareIgnoreCase((string) jsonNode2["clientVersion"], "7.0"))
          {
            Log.InnKeepersSpecial.Print("Skipping IKS: {0}, mis-matched client version {0} != {1}", new object[3]
            {
              (object) (string) current1["campaignName"],
              (object) (string) jsonNode2["clientVersion"],
              (object) "7.0"
            });
          }
          else
          {
            if (jsonNode2.ContainsKey("platform"))
            {
              string[] strArray = ((string) jsonNode2["platform"]).Trim().Split(',');
              bool flag2 = false;
              foreach (string str in strArray)
              {
                if (StringUtils.CompareIgnoreCase(str.Trim(), PlatformSettings.OS.ToString()))
                  flag2 = true;
              }
              if (!this.m_forceShowIks && !flag2)
              {
                Log.InnKeepersSpecial.Print("Skipping IKS: {0}, supported on: {1}; current platform is {2}", new object[3]
                {
                  (object) (string) current1["campaignName"],
                  (object) (string) jsonNode2["platform"],
                  (object) PlatformSettings.OS.ToString()
                });
                continue;
              }
            }
            if (jsonNode2.ContainsKey("androidStore"))
            {
              string[] strArray = ((string) jsonNode2["androidStore"]).Trim().Split(',');
              bool flag2 = false;
              foreach (string str in strArray)
              {
                if (StringUtils.CompareIgnoreCase(str.Trim(), ApplicationMgr.GetAndroidStore().ToString()))
                  flag2 = true;
              }
              if (!this.m_forceShowIks && !flag2)
              {
                Log.InnKeepersSpecial.Print("Skipping IKS: {0}, supported on: {1}; current android store is {2}", new object[3]
                {
                  (object) (string) current1["campaignName"],
                  (object) (string) jsonNode2["androidStore"],
                  (object) ApplicationMgr.GetAndroidStore().ToString()
                });
                continue;
              }
            }
            if (num3 > (long) num1 || num3 == (long) num1 && (double) num4 > num2)
            {
              jsonNode1 = current1;
              this.m_adMetadata = jsonNode2;
              num1 = (int) num3;
              num2 = (double) num4;
            }
          }
        }
      }
      return jsonNode1;
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogError((object) ("Failed to get correct advertisement: " + (object) ex));
      return (JsonNode) null;
    }
  }
}
