﻿// Decompiled with JetBrains decompiler
// Type: RafaamStaffOfOriginationSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RafaamStaffOfOriginationSpell : Spell
{
  public Spell m_CustomSpawnSpell;
  private int m_spawnTaskIndex;

  public override bool AddPowerTargets()
  {
    if (!this.m_taskList.DoesBlockHaveMetaDataTasks())
      return false;
    this.m_spawnTaskIndex = -1;
    bool flag = false;
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    for (int index = 0; index < taskList.Count; ++index)
    {
      Network.PowerHistory power = taskList[index].GetPower();
      Network.HistTagChange histTagChange = power as Network.HistTagChange;
      if (histTagChange != null && histTagChange.Tag == 420)
      {
        flag = true;
      }
      else
      {
        Network.HistFullEntity histFullEntity = power as Network.HistFullEntity;
        if (histFullEntity != null && flag)
        {
          Card card = GameState.Get().GetEntity(histFullEntity.Entity.ID).GetCard();
          if (!((Object) card == (Object) null))
          {
            this.m_targets.Add(card.gameObject);
            this.m_spawnTaskIndex = index;
            break;
          }
        }
      }
    }
    return this.m_spawnTaskIndex >= 0;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    this.ApplyCustomSpawnOverride();
    this.DoTasksUntilSpawn();
  }

  private void ApplyCustomSpawnOverride()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_targets.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.GetComponent<Card>().OverrideCustomSpawnSpell(Object.Instantiate<Spell>(this.m_CustomSpawnSpell));
    }
  }

  private void DoTasksUntilSpawn()
  {
    this.m_taskList.DoTasks(0, this.m_spawnTaskIndex, (PowerTaskList.CompleteCallback) ((taskList, startIndex, count, userData) => this.StartCoroutine(this.WaitThenFinish())));
  }

  [DebuggerHidden]
  private IEnumerator WaitThenFinish()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RafaamStaffOfOriginationSpell.\u003CWaitThenFinish\u003Ec__Iterator2CC() { \u003C\u003Ef__this = this };
  }
}
