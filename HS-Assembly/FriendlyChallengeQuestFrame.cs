﻿// Decompiled with JetBrains decompiler
// Type: FriendlyChallengeQuestFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FriendlyChallengeQuestFrame : MonoBehaviour
{
  public UberText m_questName;
  public UberText m_questDesc;
  public Transform m_rewardBone;
  public GameObject m_nameLine;
  public MeshRenderer m_rewardMesh;
  public UberText m_rewardAmountLabel;
}
