﻿// Decompiled with JetBrains decompiler
// Type: RAFLinkFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RAFLinkFrame : MonoBehaviour
{
  public UIBButton m_copyButton;
  public UberText m_url;
  public HighlightState m_copyButtonHighlight;
  private PegUIElement m_inputBlockerPegUIElement;
  private bool m_isShown;
  private string m_fullUrl;

  private void Awake()
  {
    this.m_copyButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCopyButtonReleased));
    this.m_copyButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnCopyButtonOver));
    this.m_copyButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnCopyButtonOut));
  }

  private void OnDestroy()
  {
  }

  public void Show()
  {
    if (this.m_isShown)
      return;
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    this.m_isShown = true;
    this.gameObject.SetActive(true);
    if ((Object) this.m_inputBlockerPegUIElement != (Object) null)
    {
      Object.Destroy((Object) this.m_inputBlockerPegUIElement.gameObject);
      this.m_inputBlockerPegUIElement = (PegUIElement) null;
    }
    GameObject inputBlocker = CameraUtils.CreateInputBlocker(CameraUtils.FindFirstByLayer(this.gameObject.layer), "RAFLinkInputBlocker");
    SceneUtils.SetLayer(inputBlocker, this.gameObject.layer);
    this.m_inputBlockerPegUIElement = inputBlocker.AddComponent<PegUIElement>();
    this.m_inputBlockerPegUIElement.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnInputBlockerRelease));
    TransformUtil.SetPosY((Component) this.m_inputBlockerPegUIElement, this.gameObject.transform.position.y - 5f);
    RAFManager.Get().GetRAFFrame().DarkenInputBlocker(inputBlocker, 0.5f);
  }

  public void Hide()
  {
    if (!this.m_isShown)
      return;
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    if ((Object) this.m_inputBlockerPegUIElement != (Object) null)
    {
      Object.Destroy((Object) this.m_inputBlockerPegUIElement.gameObject);
      this.m_inputBlockerPegUIElement = (PegUIElement) null;
    }
    this.m_isShown = false;
    this.gameObject.SetActive(false);
  }

  public void SetURL(string displayUrl, string fullUrl)
  {
    this.m_url.Text = displayUrl;
    this.m_fullUrl = fullUrl;
  }

  private bool OnNavigateBack()
  {
    this.Hide();
    return true;
  }

  private void OnInputBlockerRelease(UIEvent e)
  {
    this.Hide();
  }

  private void OnCopyButtonReleased(UIEvent e)
  {
    GeneralUtils.CopyToClipboard(this.m_fullUrl);
    UIStatus.Get().AddInfo(GameStrings.Get("GLUE_RAF_COPY_COMPLETE"));
    this.Hide();
  }

  private void OnCopyButtonOver(UIEvent e)
  {
    this.m_copyButtonHighlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_MOUSE_OVER);
  }

  private void OnCopyButtonOut(UIEvent e)
  {
    this.m_copyButtonHighlight.ChangeState(ActorStateType.NONE);
  }
}
