﻿// Decompiled with JetBrains decompiler
// Type: CollectionHeroSkin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CollectionHeroSkin : MonoBehaviour
{
  public MeshRenderer m_classIcon;
  public GameObject m_favoriteBanner;
  public UberText m_favoriteBannerText;
  public GameObject m_shadow;
  public Spell m_socketFX;
  public UberText m_name;
  public GameObject m_nameShadow;
  public UberText m_collectionManagerName;

  public void Awake()
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    Actor component = this.gameObject.GetComponent<Actor>();
    if ((Object) component != (Object) null)
      component.OverrideNameText((UberText) null);
    this.m_nameShadow.SetActive(false);
  }

  public void SetClass(TAG_CLASS classTag)
  {
    if ((Object) this.m_classIcon != (Object) null)
      this.m_classIcon.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", CollectionPageManager.s_classTextureOffsets[classTag]);
    if (!((Object) this.m_favoriteBannerText != (Object) null))
      return;
    this.m_favoriteBannerText.Text = GameStrings.Format("GLUE_COLLECTION_MANAGER_FAVORITE_DEFAULT_TEXT", (object) GameStrings.GetClassName(classTag));
  }

  public void ShowShadow(bool show)
  {
    if ((Object) this.m_shadow == (Object) null)
      return;
    this.m_shadow.SetActive(show);
  }

  public void ShowFavoriteBanner(bool show)
  {
    if ((Object) this.m_favoriteBanner == (Object) null)
      return;
    this.m_favoriteBanner.SetActive(show);
  }

  public void ShowSocketFX()
  {
    if ((Object) this.m_socketFX == (Object) null || !this.m_socketFX.gameObject.activeInHierarchy)
      return;
    this.m_socketFX.gameObject.SetActive(true);
    this.m_socketFX.Activate();
  }

  public void ShowCollectionManagerText()
  {
    Actor component = this.gameObject.GetComponent<Actor>();
    if (!((Object) component != (Object) null))
      return;
    component.OverrideNameText(this.m_collectionManagerName);
  }
}
