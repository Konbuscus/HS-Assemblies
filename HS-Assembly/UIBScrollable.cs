﻿// Decompiled with JetBrains decompiler
// Type: UIBScrollable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class UIBScrollable : PegUICustomBehavior
{
  private static Map<string, float> s_SavedScrollValues = new Map<string, float>();
  [CustomEditField(Sections = "Preferences")]
  public float m_ScrollWheelAmount = 0.1f;
  [CustomEditField(Sections = "Preferences")]
  public iTween.EaseType m_ScrollEaseType = iTween.Defaults.easeType;
  [CustomEditField(Sections = "Preferences")]
  public float m_ScrollTweenTime = 0.2f;
  [CustomEditField(Sections = "Preferences")]
  public UIBScrollable.ScrollDirection m_ScrollPlane = UIBScrollable.ScrollDirection.Z;
  [CustomEditField(Sections = "Touch Settings")]
  [Tooltip("Drag distance required to initiate deck tile dragging (inches)")]
  public float m_DeckTileDragThreshold = 0.04f;
  [Tooltip("Drag distance required to initiate scroll dragging (inches)")]
  [CustomEditField(Sections = "Touch Settings")]
  public float m_ScrollDragThreshold = 0.04f;
  [CustomEditField(Sections = "Touch Settings")]
  [Tooltip("Stopping speed for scrolling after the user has let go")]
  public float m_MinKineticScrollSpeed = 0.01f;
  [CustomEditField(Sections = "Touch Settings")]
  [Tooltip("Resistance for slowing down scrolling after the user has let go")]
  public float m_KineticScrollFriction = 6f;
  [CustomEditField(Sections = "Touch Settings")]
  [Tooltip("Strength of the boundary springs")]
  public float m_ScrollBoundsSpringK = 700f;
  [Tooltip("Distance at which the out-of-bounds scroll value will snapped to 0 or 1")]
  [CustomEditField(Sections = "Touch Settings")]
  public float m_MinOutOfBoundsScrollValue = 1f / 1000f;
  [CustomEditField(Sections = "Touch Settings")]
  [Tooltip("Use this to match scaling issues.")]
  public float m_ScrollDeltaMultiplier = 1f;
  [CustomEditField(Sections = "Touch Settings")]
  public List<BoxCollider> m_TouchScrollBlockers = new List<BoxCollider>();
  public UIBScrollable.HeightMode m_HeightMode = UIBScrollable.HeightMode.UseScrollableItem;
  private bool m_Enabled = true;
  private List<UIBScrollable.EnableScroll> m_EnableScrollListeners = new List<UIBScrollable.EnableScroll>();
  private List<UIBScrollable.VisibleAffectedObject> m_VisibleAffectedObjects = new List<UIBScrollable.VisibleAffectedObject>();
  private List<UIBScrollable.OnTouchScrollStarted> m_TouchScrollStartedListeners = new List<UIBScrollable.OnTouchScrollStarted>();
  private List<UIBScrollable.OnTouchScrollEnded> m_TouchScrollEndedListeners = new List<UIBScrollable.OnTouchScrollEnded>();
  [CustomEditField(Sections = "Camera Settings")]
  public bool m_UseCameraFromLayer;
  [CustomEditField(Sections = "Preferences")]
  public float m_ScrollBottomPadding;
  [CustomEditField(Sections = "Preferences")]
  public bool m_ScrollDirectionReverse;
  [CustomEditField(Sections = "Preferences")]
  [Tooltip("If scrolling is active, all PegUI calls will be suppressed")]
  public bool m_OverridePegUI;
  [CustomEditField(Sections = "Preferences")]
  public bool m_ForceScrollAreaHitTest;
  [CustomEditField(Sections = "Bounds Settings")]
  public BoxCollider m_ScrollBounds;
  [CustomEditField(Sections = "Optional Bounds Settings")]
  public BoxCollider m_TouchDragFullArea;
  [CustomEditField(Sections = "Thumb Settings")]
  public BoxCollider m_ScrollTrack;
  [CustomEditField(Sections = "Thumb Settings")]
  public ScrollBarThumb m_ScrollThumb;
  [CustomEditField(Sections = "Thumb Settings")]
  public bool m_HideThumbWhenDisabled;
  [CustomEditField(Sections = "Thumb Settings")]
  public GameObject m_scrollTrackCover;
  [CustomEditField(Sections = "Bounds Settings")]
  public GameObject m_ScrollObject;
  [CustomEditField(Sections = "Bounds Settings")]
  public float m_VisibleObjectThreshold;
  private float m_ScrollValue;
  private float m_LastTouchScrollValue;
  private bool m_InputBlocked;
  private bool m_Pause;
  private Vector2? m_TouchBeginScreenPos;
  private Vector3? m_TouchDragBeginWorldPos;
  private float m_TouchDragBeginScrollValue;
  private Vector3 m_ScrollAreaStartPos;
  private UIBScrollable.ScrollHeightCallback m_ScrollHeightCallback;
  private float m_LastScrollHeightRecorded;
  private float m_PolledScrollHeight;
  private bool m_ForceShowVisibleAffectedObjects;

  [CustomEditField(Sections = "Scroll")]
  public float ScrollValue
  {
    get
    {
      return this.m_ScrollValue;
    }
    set
    {
      if (Application.isEditor)
        return;
      this.SetScroll(value, false, false);
    }
  }

  public static void DefaultVisibleAffectedCallback(GameObject obj, bool visible)
  {
    if (obj.activeSelf == visible)
      return;
    obj.SetActive(visible);
  }

  protected override void Awake()
  {
    this.ResetScrollStartPosition();
    if ((UnityEngine.Object) this.m_ScrollTrack != (UnityEngine.Object) null && !(bool) UniversalInputManager.UsePhoneUI)
    {
      PegUIElement component = this.m_ScrollTrack.GetComponent<PegUIElement>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.AddEventListener(UIEventType.PRESS, (UIEvent.Handler) (e => this.StartDragging()));
    }
    if (!this.m_OverridePegUI)
      return;
    base.Awake();
  }

  public void Start()
  {
    if (!((UnityEngine.Object) this.m_scrollTrackCover != (UnityEngine.Object) null))
      return;
    this.m_scrollTrackCover.SetActive(false);
  }

  protected override void OnDestroy()
  {
    if (!this.m_OverridePegUI)
      return;
    base.OnDestroy();
  }

  private void Update()
  {
    this.UpdateScroll();
    if (!this.m_Enabled || this.m_InputBlocked || this.m_Pause)
      return;
    if (!this.m_ForceScrollAreaHitTest ? UniversalInputManager.Get().InputIsOver(this.GetScrollCamera(), this.m_ScrollBounds.gameObject) : UniversalInputManager.Get().ForcedInputIsOver(this.GetScrollCamera(), this.m_ScrollBounds.gameObject))
    {
      float axis = Input.GetAxis("Mouse ScrollWheel");
      if ((double) axis != 0.0)
      {
        float num = this.m_ScrollWheelAmount * 10f;
        this.AddScroll((float) (0.0 - (double) axis * (double) num));
      }
    }
    if ((UnityEngine.Object) this.m_ScrollThumb != (UnityEngine.Object) null && this.m_ScrollThumb.IsDragging())
    {
      this.Drag();
    }
    else
    {
      if (!UniversalInputManager.Get().IsTouchMode())
        return;
      this.UpdateTouch();
    }
  }

  public override bool UpdateUI()
  {
    if (this.IsTouchDragging())
      return this.m_Enabled;
    return false;
  }

  public void ResetScrollStartPosition()
  {
    if (!((UnityEngine.Object) this.m_ScrollObject != (UnityEngine.Object) null))
      return;
    this.m_ScrollAreaStartPos = this.m_ScrollObject.transform.localPosition;
  }

  public void ResetScrollStartPosition(Vector3 position)
  {
    if (!((UnityEngine.Object) this.m_ScrollObject != (UnityEngine.Object) null))
      return;
    this.m_ScrollAreaStartPos = position;
  }

  public void AddVisibleAffectedObject(GameObject obj, Vector3 extents, bool visible, UIBScrollable.VisibleAffected callback = null)
  {
    this.m_VisibleAffectedObjects.Add(new UIBScrollable.VisibleAffectedObject()
    {
      Obj = obj,
      Extents = extents,
      Visible = visible,
      Callback = callback != null ? callback : new UIBScrollable.VisibleAffected(UIBScrollable.DefaultVisibleAffectedCallback)
    });
  }

  public void RemoveVisibleAffectedObject(GameObject obj, UIBScrollable.VisibleAffected callback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.m_VisibleAffectedObjects.RemoveAll(new Predicate<UIBScrollable.VisibleAffectedObject>(new UIBScrollable.\u003CRemoveVisibleAffectedObject\u003Ec__AnonStorey469()
    {
      obj = obj,
      callback = callback
    }.\u003C\u003Em__360));
  }

  public void ClearVisibleAffectObjects()
  {
    this.m_VisibleAffectedObjects.Clear();
  }

  public void ForceVisibleAffectedObjectsShow(bool show)
  {
    if (this.m_ForceShowVisibleAffectedObjects == show)
      return;
    this.m_ForceShowVisibleAffectedObjects = show;
    this.UpdateAndFireVisibleAffectedObjects();
  }

  public void AddEnableScrollListener(UIBScrollable.EnableScroll dlg)
  {
    this.m_EnableScrollListeners.Add(dlg);
  }

  public void RemoveEnableScrollListener(UIBScrollable.EnableScroll dlg)
  {
    this.m_EnableScrollListeners.Remove(dlg);
  }

  public void AddTouchScrollStartedListener(UIBScrollable.OnTouchScrollStarted dlg)
  {
    this.m_TouchScrollStartedListeners.Add(dlg);
  }

  public void RemoveTouchScrollStartedListener(UIBScrollable.OnTouchScrollStarted dlg)
  {
    this.m_TouchScrollStartedListeners.Remove(dlg);
  }

  public void AddTouchScrollEndedListener(UIBScrollable.OnTouchScrollEnded dlg)
  {
    this.m_TouchScrollEndedListeners.Add(dlg);
  }

  public void RemoveTouchScrollEndedListener(UIBScrollable.OnTouchScrollEnded dlg)
  {
    this.m_TouchScrollEndedListeners.Remove(dlg);
  }

  public void Pause(bool pause)
  {
    this.m_Pause = pause;
  }

  public void Enable(bool enable)
  {
    if (this.m_Enabled == enable)
      return;
    if ((UnityEngine.Object) this.m_ScrollThumb != (UnityEngine.Object) null)
    {
      if ((bool) UniversalInputManager.UsePhoneUI)
        this.m_ScrollThumb.gameObject.SetActive(false);
      else
        this.m_ScrollThumb.gameObject.SetActive(!this.m_HideThumbWhenDisabled || enable);
    }
    if ((UnityEngine.Object) this.m_scrollTrackCover != (UnityEngine.Object) null)
      this.m_scrollTrackCover.SetActive(!enable);
    this.m_Enabled = enable;
    if (this.m_Enabled)
      this.ResetTouchDrag();
    this.FireEnableScrollEvent();
  }

  public bool IsEnabled()
  {
    return this.m_Enabled;
  }

  public bool IsEnabledAndScrollable()
  {
    if (this.m_Enabled)
      return this.IsScrollNeeded();
    return false;
  }

  public float GetScroll()
  {
    return this.m_ScrollValue;
  }

  public void SaveScroll(string savedName)
  {
    UIBScrollable.s_SavedScrollValues[savedName] = this.m_ScrollValue;
  }

  public void LoadScroll(string savedName)
  {
    float percentage = 0.0f;
    if (!UIBScrollable.s_SavedScrollValues.TryGetValue(savedName, out percentage))
      return;
    this.SetScroll(percentage, false, true);
    this.ResetTouchDrag();
  }

  public bool EnableIfNeeded()
  {
    bool enable = this.IsScrollNeeded();
    this.Enable(enable);
    return enable;
  }

  public bool IsScrollNeeded()
  {
    return (double) this.GetTotalScrollHeight() > 0.0;
  }

  public float PollScrollHeight()
  {
    switch (this.m_HeightMode)
    {
      case UIBScrollable.HeightMode.UseHeightCallback:
        if (this.m_ScrollHeightCallback != null)
          return this.m_ScrollHeightCallback();
        return this.m_PolledScrollHeight;
      case UIBScrollable.HeightMode.UseScrollableItem:
        return this.GetScrollableItemsHeight();
      default:
        return 0.0f;
    }
  }

  public void SetScroll(float percentage, bool blockInputWhileScrolling = false, bool clamp = true)
  {
    this.SetScroll(percentage, (UIBScrollable.OnScrollComplete) null, blockInputWhileScrolling, clamp);
  }

  public void SetScroll(float percentage, iTween.EaseType tweenType, float tweenTime, bool blockInputWhileScrolling = false, bool clamp = true)
  {
    this.SetScroll(percentage, (UIBScrollable.OnScrollComplete) null, tweenType, tweenTime, blockInputWhileScrolling, clamp);
  }

  public void SetScrollSnap(float percentage, bool clamp = true)
  {
    this.SetScrollSnap(percentage, (UIBScrollable.OnScrollComplete) null, clamp);
  }

  public void SetScroll(float percentage, UIBScrollable.OnScrollComplete scrollComplete, bool blockInputWhileScrolling = false, bool clamp = true)
  {
    this.StartCoroutine(this.SetScrollWait(percentage, scrollComplete, blockInputWhileScrolling, true, new iTween.EaseType?(), new float?(), clamp));
  }

  public void SetScroll(float percentage, UIBScrollable.OnScrollComplete scrollComplete, iTween.EaseType tweenType, float tweenTime, bool blockInputWhileScrolling = false, bool clamp = true)
  {
    this.StartCoroutine(this.SetScrollWait(percentage, scrollComplete, blockInputWhileScrolling, true, new iTween.EaseType?(tweenType), new float?(tweenTime), clamp));
  }

  public void SetScrollSnap(float percentage, UIBScrollable.OnScrollComplete scrollComplete, bool clamp = true)
  {
    this.m_PolledScrollHeight = this.PollScrollHeight();
    this.m_LastScrollHeightRecorded = this.m_PolledScrollHeight;
    this.ScrollTo(percentage, scrollComplete, false, false, new iTween.EaseType?(), new float?(), clamp);
    this.ResetTouchDrag();
  }

  public void SetScrollHeightCallback(UIBScrollable.ScrollHeightCallback dlg, bool refresh = false, bool resetScroll = false)
  {
    float? setResetScroll = new float?();
    if (resetScroll)
      setResetScroll = new float?(0.0f);
    this.SetScrollHeightCallback(dlg, setResetScroll, refresh);
  }

  public void SetScrollHeightCallback(UIBScrollable.ScrollHeightCallback dlg, float? setResetScroll, bool refresh = false)
  {
    this.m_VisibleAffectedObjects.Clear();
    this.m_ScrollHeightCallback = dlg;
    if (setResetScroll.HasValue)
    {
      this.m_ScrollValue = setResetScroll.Value;
      this.ResetTouchDrag();
    }
    if (refresh)
    {
      this.UpdateScroll();
      this.UpdateThumbPosition();
      this.UpdateScrollObjectPosition(true, (UIBScrollable.OnScrollComplete) null, new iTween.EaseType?(), new float?(), false);
    }
    this.m_PolledScrollHeight = this.PollScrollHeight();
    this.m_LastScrollHeightRecorded = this.m_PolledScrollHeight;
  }

  public void SetHeight(float height)
  {
    this.m_ScrollHeightCallback = (UIBScrollable.ScrollHeightCallback) null;
    this.m_PolledScrollHeight = height;
    this.UpdateHeight();
  }

  public void UpdateScroll()
  {
    this.m_PolledScrollHeight = this.PollScrollHeight();
    this.UpdateHeight();
  }

  public void CenterWorldPosition(Vector3 position)
  {
    float percentage = (float) ((double) this.m_ScrollObject.transform.InverseTransformPoint(position)[(int) this.m_ScrollPlane] / -((double) this.m_PolledScrollHeight + (double) this.m_ScrollBottomPadding) * 2.0 - 0.5);
    this.StartCoroutine(this.BlockInput(this.m_ScrollTweenTime));
    this.SetScroll(percentage, false, true);
  }

  public bool IsObjectVisibleInScrollArea(GameObject obj, Vector3 extents, bool fullyVisible = false)
  {
    int scrollPlane = (int) this.m_ScrollPlane;
    float num1 = obj.transform.position[scrollPlane] - extents[scrollPlane];
    float num2 = obj.transform.position[scrollPlane] + extents[scrollPlane];
    Bounds bounds = this.m_ScrollBounds.bounds;
    float num3 = bounds.min[scrollPlane] - this.m_VisibleObjectThreshold;
    float num4 = bounds.max[scrollPlane] + this.m_VisibleObjectThreshold;
    bool flag1 = (double) num1 >= (double) num3 && (double) num1 <= (double) num4;
    bool flag2 = (double) num2 >= (double) num3 && (double) num2 <= (double) num4;
    if (fullyVisible)
    {
      if (flag1)
        return flag2;
      return false;
    }
    if (!flag1)
      return flag2;
    return true;
  }

  public bool ScrollObjectIntoView(GameObject obj, float positionOffset, float axisExtent, UIBScrollable.OnScrollComplete scrollComplete, iTween.EaseType tweenType, float tweenTime, bool blockInputWhileScrolling = false)
  {
    int scrollPlane = (int) this.m_ScrollPlane;
    float num1 = obj.transform.position[scrollPlane] + positionOffset - axisExtent;
    float num2 = obj.transform.position[scrollPlane] + positionOffset + axisExtent;
    Bounds bounds = this.m_ScrollBounds.bounds;
    float num3 = bounds.min[scrollPlane] - this.m_VisibleObjectThreshold;
    float num4 = bounds.max[scrollPlane] + this.m_VisibleObjectThreshold;
    bool flag1 = (double) num1 >= (double) num3;
    bool flag2 = (double) num2 <= (double) num4;
    if (flag1 && flag2)
      return false;
    float percentage = 0.0f;
    if (!flag1)
      percentage = this.m_ScrollValue - Math.Abs(num3 - num1) / this.GetTotalScrollHeightVector().z;
    else if (!flag2)
      percentage = this.m_ScrollValue + Math.Abs(num4 - num2) / this.GetTotalScrollHeightVector().z;
    this.SetScroll(percentage, scrollComplete, tweenType, tweenTime, blockInputWhileScrolling, true);
    return true;
  }

  public bool IsDragging()
  {
    if (!((UnityEngine.Object) this.m_ScrollThumb != (UnityEngine.Object) null) || !this.m_ScrollThumb.IsDragging())
      return this.m_TouchBeginScreenPos.HasValue;
    return true;
  }

  public bool IsTouchDragging()
  {
    if (!this.m_TouchBeginScreenPos.HasValue)
      return false;
    return (double) this.m_ScrollDragThreshold * ((double) Screen.dpi <= 0.0 ? 150.0 : (double) Screen.dpi) <= (double) Mathf.Abs(UniversalInputManager.Get().GetMousePosition().y - this.m_TouchBeginScreenPos.Value.y);
  }

  public void SetScrollImmediate(float percentage)
  {
    this.ScrollTo(percentage, (UIBScrollable.OnScrollComplete) null, false, false, new iTween.EaseType?(), new float?(0.0f), true);
    this.ResetTouchDrag();
  }

  public void SetScrollImmediate(float percentage, UIBScrollable.OnScrollComplete scrollComplete, bool blockInputWhileScrolling, bool tween, iTween.EaseType? tweenType, float? tweenTime, bool clamp)
  {
    this.ScrollTo(percentage, scrollComplete, blockInputWhileScrolling, tween, tweenType, tweenTime, clamp);
    this.ResetTouchDrag();
  }

  private void StartDragging()
  {
    if (this.m_InputBlocked || this.m_Pause || !this.m_Enabled)
      return;
    this.m_ScrollThumb.StartDragging();
  }

  private void UpdateHeight()
  {
    if ((double) Mathf.Abs(this.m_PolledScrollHeight - this.m_LastScrollHeightRecorded) > 1.0 / 1000.0)
    {
      if (!this.EnableIfNeeded())
        this.m_ScrollValue = 0.0f;
      this.UpdateThumbPosition();
      this.UpdateScrollObjectPosition(false, (UIBScrollable.OnScrollComplete) null, new iTween.EaseType?(), new float?(), false);
      this.ResetTouchDrag();
    }
    this.m_LastScrollHeightRecorded = this.m_PolledScrollHeight;
  }

  private void UpdateTouch()
  {
    if (UniversalInputManager.Get().GetMouseButtonDown(0))
    {
      if (this.GetWorldTouchPosition().HasValue)
      {
        this.m_TouchBeginScreenPos = new Vector2?((Vector2) UniversalInputManager.Get().GetMousePosition());
        return;
      }
    }
    else if (UniversalInputManager.Get().GetMouseButtonUp(0))
    {
      this.m_TouchBeginScreenPos = new Vector2?();
      this.m_TouchDragBeginWorldPos = new Vector3?();
      this.FireTouchEndEvent();
    }
    if (this.m_TouchDragBeginWorldPos.HasValue)
    {
      Vector3? positionOnDragArea = this.GetWorldTouchPositionOnDragArea();
      if (!positionOnDragArea.HasValue)
        return;
      int scrollPlane = (int) this.m_ScrollPlane;
      this.m_LastTouchScrollValue = this.m_ScrollValue;
      float num1 = this.m_TouchDragBeginScrollValue + this.GetScrollValueDelta(positionOnDragArea.Value[scrollPlane] - this.m_TouchDragBeginWorldPos.Value[scrollPlane]);
      float outOfBoundsDist = this.GetOutOfBoundsDist(num1);
      if ((double) outOfBoundsDist != 0.0)
      {
        float num2 = Mathf.Log10(Mathf.Abs(outOfBoundsDist) + 1f) * Mathf.Sign(outOfBoundsDist);
        num1 = (double) num2 >= 0.0 ? num2 + 1f : num2;
      }
      this.ScrollTo(num1, (UIBScrollable.OnScrollComplete) null, false, false, new iTween.EaseType?(), new float?(), false);
    }
    else if (this.m_TouchBeginScreenPos.HasValue)
    {
      float num1 = Mathf.Abs(UniversalInputManager.Get().GetMousePosition().x - this.m_TouchBeginScreenPos.Value.x);
      float num2 = Mathf.Abs(UniversalInputManager.Get().GetMousePosition().y - this.m_TouchBeginScreenPos.Value.y);
      bool flag1 = (double) num1 > (double) this.m_DeckTileDragThreshold * ((double) Screen.dpi <= 0.0 ? 150.0 : (double) Screen.dpi);
      bool flag2 = (double) num2 > (double) this.m_ScrollDragThreshold * ((double) Screen.dpi <= 0.0 ? 150.0 : (double) Screen.dpi);
      if (flag1 && ((double) num1 >= (double) num2 || !flag2))
      {
        this.m_TouchBeginScreenPos = new Vector2?();
      }
      else
      {
        if (!flag2)
          return;
        this.m_TouchDragBeginWorldPos = this.GetWorldTouchPositionOnDragArea();
        this.m_TouchDragBeginScrollValue = this.m_ScrollValue;
        this.m_LastTouchScrollValue = this.m_ScrollValue;
        this.FireTouchStartEvent();
      }
    }
    else
    {
      float f1 = (this.m_ScrollValue - this.m_LastTouchScrollValue) / Time.fixedDeltaTime;
      float outOfBoundsDist = this.GetOutOfBoundsDist(this.m_ScrollValue);
      if ((double) outOfBoundsDist != 0.0)
      {
        if ((double) Mathf.Abs(outOfBoundsDist) >= (double) this.m_MinOutOfBoundsScrollValue)
        {
          float num1 = (float) (-(double) this.m_ScrollBoundsSpringK * (double) outOfBoundsDist - (double) Mathf.Sqrt(4f * this.m_ScrollBoundsSpringK) * (double) f1);
          float num2 = f1 + num1 * Time.fixedDeltaTime;
          this.m_LastTouchScrollValue = this.m_ScrollValue;
          this.ScrollTo(this.m_ScrollValue + num2 * Time.fixedDeltaTime, (UIBScrollable.OnScrollComplete) null, false, false, new iTween.EaseType?(), new float?(), false);
        }
        if ((double) Mathf.Abs(this.GetOutOfBoundsDist(this.m_ScrollValue)) >= (double) this.m_MinOutOfBoundsScrollValue)
          return;
        this.ScrollTo(Mathf.Round(this.m_ScrollValue), (UIBScrollable.OnScrollComplete) null, false, false, new iTween.EaseType?(), new float?(), false);
        this.m_LastTouchScrollValue = this.m_ScrollValue;
      }
      else
      {
        if ((double) this.m_LastTouchScrollValue == (double) this.m_ScrollValue)
          return;
        float num = Mathf.Sign(f1);
        float f2 = f1 - num * this.m_KineticScrollFriction * Time.fixedDeltaTime;
        this.m_LastTouchScrollValue = this.m_ScrollValue;
        if ((double) Mathf.Abs(f2) < (double) this.m_MinKineticScrollSpeed || (double) Mathf.Sign(f2) != (double) num)
          return;
        this.ScrollTo(this.m_ScrollValue + f2 * Time.fixedDeltaTime, (UIBScrollable.OnScrollComplete) null, false, false, new iTween.EaseType?(), new float?(), false);
      }
    }
  }

  private void Drag()
  {
    Vector3 min = this.m_ScrollTrack.bounds.min;
    Camera firstByLayer = CameraUtils.FindFirstByLayer(this.m_ScrollTrack.gameObject.layer);
    Plane plane = new Plane(-firstByLayer.transform.forward, min);
    Ray ray = firstByLayer.ScreenPointToRay(UniversalInputManager.Get().GetMousePosition());
    float enter;
    if (plane.Raycast(ray, out enter))
    {
      Vector3 point = ray.GetPoint(enter);
      float scrollTrackTop1D = this.GetScrollTrackTop1D();
      float scrollTrackBtm1D = this.GetScrollTrackBtm1D();
      float num = Mathf.Clamp01((float) (((double) point[(int) this.m_ScrollPlane] - (double) scrollTrackTop1D) / ((double) scrollTrackBtm1D - (double) scrollTrackTop1D)));
      if ((double) Mathf.Abs(this.m_ScrollValue - num) > (double) Mathf.Epsilon)
      {
        this.m_ScrollValue = num;
        this.UpdateThumbPosition();
        this.UpdateScrollObjectPosition(false, (UIBScrollable.OnScrollComplete) null, new iTween.EaseType?(), new float?(), false);
      }
    }
    this.ResetTouchDrag();
  }

  private void ResetTouchDrag()
  {
    bool hasValue = this.m_TouchDragBeginWorldPos.HasValue;
    this.m_TouchBeginScreenPos = new Vector2?();
    this.m_TouchDragBeginWorldPos = new Vector3?();
    this.m_TouchDragBeginScrollValue = this.m_ScrollValue;
    this.m_LastTouchScrollValue = this.m_ScrollValue;
    if (!hasValue)
      return;
    this.FireTouchEndEvent();
  }

  private float GetScrollTrackTop1D()
  {
    return this.GetScrollTrackTop()[(int) this.m_ScrollPlane];
  }

  private float GetScrollTrackBtm1D()
  {
    return this.GetScrollTrackBtm()[(int) this.m_ScrollPlane];
  }

  private Vector3 GetScrollTrackTop()
  {
    if ((UnityEngine.Object) this.m_ScrollTrack == (UnityEngine.Object) null)
      return Vector3.zero;
    return this.m_ScrollTrack.bounds.max;
  }

  private Vector3 GetScrollTrackBtm()
  {
    if ((UnityEngine.Object) this.m_ScrollTrack == (UnityEngine.Object) null)
      return Vector3.zero;
    return this.m_ScrollTrack.bounds.min;
  }

  private void AddScroll(float amount)
  {
    this.ScrollTo(this.m_ScrollValue + amount, (UIBScrollable.OnScrollComplete) null, false, true, new iTween.EaseType?(), new float?(), true);
    this.ResetTouchDrag();
  }

  private void ScrollTo(float percentage, UIBScrollable.OnScrollComplete scrollComplete, bool blockInputWhileScrolling, bool tween, iTween.EaseType? tweenType, float? tweenTime, bool clamp)
  {
    this.m_ScrollValue = !clamp ? percentage : Mathf.Clamp01(percentage);
    this.UpdateThumbPosition();
    this.UpdateScrollObjectPosition(tween, scrollComplete, tweenType, tweenTime, blockInputWhileScrolling);
  }

  private void UpdateThumbPosition()
  {
    if ((UnityEngine.Object) this.m_ScrollThumb == (UnityEngine.Object) null)
      return;
    Vector3 scrollTrackTop = this.GetScrollTrackTop();
    Vector3 scrollTrackBtm = this.GetScrollTrackBtm();
    float num1 = scrollTrackTop[(int) this.m_ScrollPlane];
    float num2 = scrollTrackBtm[(int) this.m_ScrollPlane];
    Vector3 vector3 = scrollTrackTop + (scrollTrackBtm - scrollTrackTop) * 0.5f;
    vector3[(int) this.m_ScrollPlane] = num1 + (num2 - num1) * Mathf.Clamp01(this.m_ScrollValue);
    this.m_ScrollThumb.transform.position = vector3;
  }

  private void UpdateScrollObjectPosition(bool tween, UIBScrollable.OnScrollComplete scrollComplete, iTween.EaseType? tweenType, float? tweenTime, bool blockInputWhileScrolling = false)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    UIBScrollable.\u003CUpdateScrollObjectPosition\u003Ec__AnonStorey46A positionCAnonStorey46A = new UIBScrollable.\u003CUpdateScrollObjectPosition\u003Ec__AnonStorey46A();
    // ISSUE: reference to a compiler-generated field
    positionCAnonStorey46A.scrollComplete = scrollComplete;
    // ISSUE: reference to a compiler-generated field
    positionCAnonStorey46A.\u003C\u003Ef__this = this;
    if ((UnityEngine.Object) this.m_ScrollObject == (UnityEngine.Object) null)
      return;
    Vector3 scrollAreaStartPos = this.m_ScrollAreaStartPos;
    Vector3 vector3_1 = scrollAreaStartPos + this.GetTotalScrollHeightVector() * (!this.m_ScrollDirectionReverse ? 1f : -1f);
    Vector3 vector3_2 = scrollAreaStartPos + this.m_ScrollValue * (vector3_1 - scrollAreaStartPos);
    if (tween)
    {
      // ISSUE: reference to a compiler-generated method
      // ISSUE: reference to a compiler-generated method
      iTween.MoveTo(this.m_ScrollObject, iTween.Hash((object) "position", (object) vector3_2, (object) "time", (object) (float) (!tweenTime.HasValue ? (double) this.m_ScrollTweenTime : (double) tweenTime.Value), (object) "isLocal", (object) true, (object) "easetype", (object) (iTween.EaseType) (!tweenType.HasValue ? (int) this.m_ScrollEaseType : (int) tweenType.Value), (object) "onupdate", (object) new Action<object>(positionCAnonStorey46A.\u003C\u003Em__361), (object) "oncomplete", (object) new Action<object>(positionCAnonStorey46A.\u003C\u003Em__362)));
    }
    else
    {
      this.m_ScrollObject.transform.localPosition = vector3_2;
      this.UpdateAndFireVisibleAffectedObjects();
      // ISSUE: reference to a compiler-generated field
      if (positionCAnonStorey46A.scrollComplete == null)
        return;
      // ISSUE: reference to a compiler-generated field
      positionCAnonStorey46A.scrollComplete(this.m_ScrollValue);
    }
  }

  [DebuggerHidden]
  private IEnumerator SetScrollWait(float percentage, UIBScrollable.OnScrollComplete scrollComplete, bool blockInputWhileScrolling, bool tween, iTween.EaseType? tweenType, float? tweenTime, bool clamp)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UIBScrollable.\u003CSetScrollWait\u003Ec__Iterator346() { percentage = percentage, scrollComplete = scrollComplete, blockInputWhileScrolling = blockInputWhileScrolling, tween = tween, tweenType = tweenType, tweenTime = tweenTime, clamp = clamp, \u003C\u0024\u003Epercentage = percentage, \u003C\u0024\u003EscrollComplete = scrollComplete, \u003C\u0024\u003EblockInputWhileScrolling = blockInputWhileScrolling, \u003C\u0024\u003Etween = tween, \u003C\u0024\u003EtweenType = tweenType, \u003C\u0024\u003EtweenTime = tweenTime, \u003C\u0024\u003Eclamp = clamp, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator BlockInput(float blockTime)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UIBScrollable.\u003CBlockInput\u003Ec__Iterator347() { blockTime = blockTime, \u003C\u0024\u003EblockTime = blockTime, \u003C\u003Ef__this = this };
  }

  private Vector3 GetTotalScrollHeightVector()
  {
    if ((UnityEngine.Object) this.m_ScrollObject == (UnityEngine.Object) null)
      return Vector3.zero;
    float num = this.m_PolledScrollHeight - this.GetScrollBoundsHeight();
    if ((double) num < 0.0)
      return Vector3.zero;
    Vector3 zero = Vector3.zero;
    zero[(int) this.m_ScrollPlane] = num;
    Vector3 vector3 = (Vector3) (this.m_ScrollObject.transform.parent.worldToLocalMatrix * (Vector4) zero);
    if ((double) this.m_ScrollBottomPadding > 0.0)
      vector3 += vector3.normalized * this.m_ScrollBottomPadding;
    return vector3;
  }

  private float GetTotalScrollHeight()
  {
    return this.GetTotalScrollHeightVector().magnitude;
  }

  private Vector3? GetWorldTouchPosition()
  {
    return this.GetWorldTouchPosition(this.m_ScrollBounds);
  }

  private Vector3? GetWorldTouchPositionOnDragArea()
  {
    return this.GetWorldTouchPosition(!((UnityEngine.Object) this.m_TouchDragFullArea != (UnityEngine.Object) null) ? this.m_ScrollBounds : this.m_TouchDragFullArea);
  }

  private Vector3? GetWorldTouchPosition(BoxCollider bounds)
  {
    Ray ray = this.GetScrollCamera().ScreenPointToRay(UniversalInputManager.Get().GetMousePosition());
    RaycastHit hitInfo;
    using (List<BoxCollider>.Enumerator enumerator = this.m_TouchScrollBlockers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Raycast(ray, out hitInfo, float.MaxValue))
          return new Vector3?();
      }
    }
    if (bounds.Raycast(ray, out hitInfo, float.MaxValue))
      return new Vector3?(ray.GetPoint(hitInfo.distance));
    return new Vector3?();
  }

  private float GetScrollValueDelta(float worldDelta)
  {
    return this.m_ScrollDeltaMultiplier * worldDelta / this.GetTotalScrollHeight();
  }

  private float GetOutOfBoundsDist(float scrollValue)
  {
    if ((double) scrollValue < 0.0)
      return scrollValue;
    if ((double) scrollValue > 1.0)
      return scrollValue - 1f;
    return 0.0f;
  }

  private void FireEnableScrollEvent()
  {
    foreach (UIBScrollable.EnableScroll enableScroll in this.m_EnableScrollListeners.ToArray())
      enableScroll(this.m_Enabled);
  }

  public void UpdateAndFireVisibleAffectedObjects()
  {
    foreach (UIBScrollable.VisibleAffectedObject visibleAffectedObject in this.m_VisibleAffectedObjects.ToArray())
    {
      bool visible = this.IsObjectVisibleInScrollArea(visibleAffectedObject.Obj, visibleAffectedObject.Extents, false) || this.m_ForceShowVisibleAffectedObjects;
      if (visible != visibleAffectedObject.Visible)
      {
        visibleAffectedObject.Visible = visible;
        visibleAffectedObject.Callback(visibleAffectedObject.Obj, visible);
      }
    }
  }

  private float GetScrollBoundsHeight()
  {
    if (!((UnityEngine.Object) this.m_ScrollObject == (UnityEngine.Object) null))
      return this.m_ScrollBounds.bounds.size[(int) this.m_ScrollPlane];
    UnityEngine.Debug.LogWarning((object) "No m_ScrollObject set for this UIBScrollable!");
    return 0.0f;
  }

  private void FireTouchStartEvent()
  {
    foreach (UIBScrollable.OnTouchScrollStarted touchScrollStarted in this.m_TouchScrollStartedListeners.ToArray())
      touchScrollStarted();
  }

  private void FireTouchEndEvent()
  {
    foreach (UIBScrollable.OnTouchScrollEnded touchScrollEnded in this.m_TouchScrollEndedListeners.ToArray())
      touchScrollEnded();
  }

  private float GetScrollableItemsHeight()
  {
    Vector3 zero1 = Vector3.zero;
    Vector3 zero2 = Vector3.zero;
    if (this.GetScrollableItemsMinMax(ref zero1, ref zero2) == null)
      return 0.0f;
    int scrollPlane = (int) this.m_ScrollPlane;
    return zero2[scrollPlane] - zero1[scrollPlane];
  }

  private UIBScrollableItem[] GetScrollableItemsMinMax(ref Vector3 min, ref Vector3 max)
  {
    if ((UnityEngine.Object) this.m_ScrollObject == (UnityEngine.Object) null)
      return (UIBScrollableItem[]) null;
    UIBScrollableItem[] componentsInChildren = this.m_ScrollObject.GetComponentsInChildren<UIBScrollableItem>(true);
    if (componentsInChildren == null || componentsInChildren.Length == 0)
      return (UIBScrollableItem[]) null;
    min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
    max = new Vector3(float.MinValue, float.MinValue, float.MinValue);
    foreach (UIBScrollableItem uibScrollableItem in componentsInChildren)
    {
      if (uibScrollableItem.IsActive())
      {
        Vector3 min1;
        Vector3 max1;
        uibScrollableItem.GetWorldBounds(out min1, out max1);
        min.x = Mathf.Min(min.x, min1.x, max1.x);
        min.y = Mathf.Min(min.y, min1.y, max1.y);
        min.z = Mathf.Min(min.z, min1.z, max1.z);
        max.x = Mathf.Max(max.x, min1.x, max1.x);
        max.y = Mathf.Max(max.y, min1.y, max1.y);
        max.z = Mathf.Max(max.z, min1.z, max1.z);
      }
    }
    return componentsInChildren;
  }

  private BoxCollider[] GetBoxCollidersMinMax(ref Vector3 min, ref Vector3 max)
  {
    return (BoxCollider[]) null;
  }

  private Camera GetScrollCamera()
  {
    if (this.m_UseCameraFromLayer)
      return CameraUtils.FindFirstByLayer(this.gameObject.layer);
    return Box.Get().GetCamera();
  }

  public enum ScrollDirection
  {
    X,
    Y,
    Z,
  }

  public enum HeightMode
  {
    UseHeightCallback,
    UseScrollableItem,
    UseBoxCollider,
  }

  protected class VisibleAffectedObject
  {
    public GameObject Obj;
    public Vector3 Extents;
    public bool Visible;
    public UIBScrollable.VisibleAffected Callback;
  }

  public delegate void EnableScroll(bool enabled);

  public delegate float ScrollHeightCallback();

  public delegate void ScrollTurnedOn(bool on);

  public delegate void OnScrollComplete(float percentage);

  public delegate void OnTouchScrollStarted();

  public delegate void OnTouchScrollEnded();

  public delegate void VisibleAffected(GameObject obj, bool visible);
}
