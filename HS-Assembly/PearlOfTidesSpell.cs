﻿// Decompiled with JetBrains decompiler
// Type: PearlOfTidesSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class PearlOfTidesSpell : SuperSpell
{
  protected override void OnAction(SpellStateType prevStateType)
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.HistFullEntity power = enumerator.Current.GetPower() as Network.HistFullEntity;
        if (power != null)
          GameState.Get().GetEntity(power.Entity.ID).GetCard().SuppressPlaySounds(true);
      }
    }
    base.OnAction(prevStateType);
  }
}
