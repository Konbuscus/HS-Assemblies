﻿// Decompiled with JetBrains decompiler
// Type: CreditsScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CreditsScene : Scene
{
  private bool m_unloading;

  protected override void Awake()
  {
    base.Awake();
    AssetLoader.Get().LoadUIScreen("Credits", new AssetLoader.GameObjectCallback(this.OnUIScreenLoaded), (object) null, false);
    if (!((Object) InactivePlayerKicker.Get() != (Object) null))
      return;
    InactivePlayerKicker.Get().SetShouldCheckForInactivity(false);
  }

  public override bool IsUnloading()
  {
    return this.m_unloading;
  }

  public override void Unload()
  {
    this.m_unloading = true;
    if ((Object) InactivePlayerKicker.Get() != (Object) null)
      InactivePlayerKicker.Get().SetShouldCheckForInactivity(true);
    this.m_unloading = false;
  }

  private void OnUIScreenLoaded(string name, GameObject screen, object callbackData)
  {
    if (!((Object) screen == (Object) null))
      return;
    Debug.LogError((object) string.Format("CreditsScene.OnUIScreenLoaded() - failed to load screen {0}", (object) name));
  }
}
