﻿// Decompiled with JetBrains decompiler
// Type: PackOpeningCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PackOpeningCard : MonoBehaviour
{
  private List<PackOpeningCard.RevealedListener> m_revealedListeners = new List<PackOpeningCard.RevealedListener>();
  private const TAG_RARITY FALLBACK_RARITY = TAG_RARITY.COMMON;
  public GameObject m_CardParent;
  public GameObject m_SharedHiddenCardObject;
  public Spell m_ClassNameSpell;
  public Spell m_IsNewSpell;
  private PackOpeningCardRarityInfo[] m_RarityInfos;
  private NetCache.BoosterCard m_boosterCard;
  private TAG_PREMIUM m_premium;
  private EntityDef m_entityDef;
  private CardDef m_cardDef;
  private Actor m_actor;
  private PackOpeningCardRarityInfo m_rarityInfo;
  private Spell m_spell;
  private PegUIElement m_revealButton;
  private bool m_ready;
  private bool m_inputEnabled;
  private bool m_revealEnabled;
  private bool m_revealed;
  private bool m_isNew;

  private void Awake()
  {
    this.StartCoroutine("HackWaitThenDeactivateRarityInfo");
  }

  [DebuggerHidden]
  private IEnumerator HackWaitThenDeactivateRarityInfo()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    PackOpeningCard.\u003CHackWaitThenDeactivateRarityInfo\u003Ec__Iterator22E infoCIterator22E = new PackOpeningCard.\u003CHackWaitThenDeactivateRarityInfo\u003Ec__Iterator22E();
    return (IEnumerator) infoCIterator22E;
  }

  public NetCache.BoosterCard GetCard()
  {
    return this.m_boosterCard;
  }

  public string GetCardId()
  {
    if (this.m_boosterCard == null)
      return (string) null;
    return this.m_boosterCard.Def.Name;
  }

  public EntityDef GetEntityDef()
  {
    return this.m_entityDef;
  }

  public CardDef GetCardDef()
  {
    return this.m_cardDef;
  }

  public Actor GetActor()
  {
    return this.m_actor;
  }

  public void AttachBoosterCard(NetCache.BoosterCard boosterCard)
  {
    if (this.m_boosterCard == null && boosterCard == null)
      return;
    this.m_boosterCard = boosterCard;
    this.m_premium = this.m_boosterCard.Def.Premium;
    this.Destroy();
    if (this.m_boosterCard == null)
      this.BecomeReady();
    else
      DefLoader.Get().LoadFullDef(this.m_boosterCard.Def.Name, new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
  }

  public bool IsReady()
  {
    return this.m_ready;
  }

  public bool IsRevealed()
  {
    return this.m_revealed;
  }

  private void OnDestroy()
  {
    if (this.m_entityDef == null)
      return;
    DefLoader.Get().ClearCardDef(this.m_entityDef.GetCardId());
  }

  public void Destroy()
  {
    this.m_ready = false;
    if ((Object) this.m_actor != (Object) null)
    {
      this.m_actor.Destroy();
      this.m_actor = (Actor) null;
    }
    this.m_rarityInfo = (PackOpeningCardRarityInfo) null;
    this.m_spell = (Spell) null;
    this.m_revealButton = (PegUIElement) null;
    this.m_revealed = false;
  }

  public bool IsInputEnabled()
  {
    return this.m_inputEnabled;
  }

  public void EnableInput(bool enable)
  {
    this.m_inputEnabled = enable;
    this.UpdateInput();
  }

  public bool IsRevealEnabled()
  {
    return this.m_revealEnabled;
  }

  public void EnableReveal(bool enable)
  {
    this.m_revealEnabled = enable;
    this.UpdateActor();
  }

  public void AddRevealedListener(PackOpeningCard.RevealedCallback callback)
  {
    this.AddRevealedListener(callback, (object) null);
  }

  public void AddRevealedListener(PackOpeningCard.RevealedCallback callback, object userData)
  {
    PackOpeningCard.RevealedListener revealedListener = new PackOpeningCard.RevealedListener();
    revealedListener.SetCallback(callback);
    revealedListener.SetUserData(userData);
    this.m_revealedListeners.Add(revealedListener);
  }

  public void RemoveRevealedListener(PackOpeningCard.RevealedCallback callback)
  {
    this.RemoveRevealedListener(callback, (object) null);
  }

  public void RemoveRevealedListener(PackOpeningCard.RevealedCallback callback, object userData)
  {
    PackOpeningCard.RevealedListener revealedListener = new PackOpeningCard.RevealedListener();
    revealedListener.SetCallback(callback);
    revealedListener.SetUserData(userData);
    this.m_revealedListeners.Remove(revealedListener);
  }

  public void RemoveOnOverWhileFlippedListeners()
  {
    this.m_revealButton.RemoveEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnOverWhileFlipped));
    this.m_revealButton.RemoveEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnOutWhileFlipped));
  }

  public void ForceReveal()
  {
    this.OnPress((UIEvent) null);
  }

  public void ShowRarityGlow()
  {
    if (this.IsRevealed())
      return;
    this.OnOver((UIEvent) null);
  }

  public void HideRarityGlow()
  {
    if (this.IsRevealed())
      return;
    this.OnOut((UIEvent) null);
  }

  private void OnFullDefLoaded(string cardId, FullDef fullDef, object userData)
  {
    if (fullDef == null)
    {
      this.BecomeReady();
      UnityEngine.Debug.LogWarning((object) string.Format("PackOpeningCard.OnFullDefLoaded() - FAILED to load \"{0}\"", (object) cardId));
    }
    else
    {
      this.m_entityDef = fullDef.GetEntityDef();
      this.m_cardDef = fullDef.GetCardDef();
      if (!this.DetermineRarityInfo())
      {
        this.BecomeReady();
      }
      else
      {
        AssetLoader.Get().LoadActor(ActorNames.GetHandActor(this.m_entityDef, this.m_premium), new AssetLoader.GameObjectCallback(this.OnActorLoaded), (object) null, false);
        if (!Cheats.Get().IsYourMindFree())
          return;
        CollectibleCard card = CollectionManager.Get().GetCard(this.m_entityDef.GetCardId(), this.m_premium);
        this.m_isNew = card.SeenCount < 1 && card.OwnedCount < 2;
      }
    }
  }

  private void OnActorLoaded(string name, GameObject actorObject, object userData)
  {
    if ((Object) actorObject == (Object) null)
    {
      this.BecomeReady();
      UnityEngine.Debug.LogWarning((object) string.Format("PackOpeningCard.OnActorLoaded() - FAILED to load actor \"{0}\"", (object) name));
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((Object) component == (Object) null)
      {
        this.BecomeReady();
        UnityEngine.Debug.LogWarning((object) string.Format("PackOpeningCard.OnActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) name));
      }
      else
      {
        this.m_actor = component;
        this.m_actor.TurnOffCollider();
        this.SetupActor();
        SceneUtils.SetLayer(component.gameObject, GameLayer.IgnoreFullScreenEffects);
        this.BecomeReady();
      }
    }
  }

  private bool DetermineRarityInfo()
  {
    PackOpeningRarity packOpeningRarity = GameUtils.GetPackOpeningRarity(this.m_entityDef != null ? this.m_entityDef.GetRarity() : TAG_RARITY.COMMON);
    if (packOpeningRarity == PackOpeningRarity.NONE)
    {
      UnityEngine.Debug.LogError((object) string.Format("PackOpeningCard.DetermineRarityInfo() - FAILED to determine rarity for {0}", (object) this.GetCardId()));
      return false;
    }
    GameObject openingCardEffects = SceneUtils.FindComponentInParents<PackOpening>((Component) this).GetPackOpeningCardEffects();
    if ((Object) openingCardEffects == (Object) null)
    {
      UnityEngine.Debug.LogError((object) "PackOpeningCard.DetermineRarityInfo() - Fail to get card effect from PackOpening");
      return false;
    }
    this.m_RarityInfos = openingCardEffects.GetComponentsInChildren<PackOpeningCardRarityInfo>();
    if (this.m_RarityInfos == null)
    {
      UnityEngine.Debug.LogError((object) string.Format("PackOpeningCard.DetermineRarityInfo() - {0} has no rarity info list. cardId={1}", (object) this, (object) this.GetCardId()));
      return false;
    }
    for (int index = 0; index < this.m_RarityInfos.Length; ++index)
    {
      PackOpeningCardRarityInfo rarityInfo = this.m_RarityInfos[index];
      if (packOpeningRarity == rarityInfo.m_RarityType)
      {
        this.m_rarityInfo = rarityInfo;
        this.SetupRarity();
        return true;
      }
    }
    UnityEngine.Debug.LogError((object) string.Format("PackOpeningCard.DetermineRarityInfo() - {0} has no rarity info for {1}. cardId={2}", (object) this, (object) packOpeningRarity, (object) this.GetCardId()));
    return false;
  }

  private void SetupActor()
  {
    this.m_actor.SetEntityDef(this.m_entityDef);
    this.m_actor.SetCardDef(this.m_cardDef);
    this.m_actor.SetPremium(this.m_premium);
    this.m_actor.UpdateAllComponents();
  }

  private void UpdateActor()
  {
    if ((Object) this.m_actor == (Object) null)
      return;
    if (!this.IsRevealEnabled())
    {
      this.m_actor.Hide();
    }
    else
    {
      if (!this.IsRevealed())
        this.m_actor.Hide();
      Vector3 localScale = this.m_actor.transform.localScale;
      this.m_actor.transform.parent = this.m_rarityInfo.m_RevealedCardObject.transform;
      this.m_actor.transform.localPosition = Vector3.zero;
      this.m_actor.transform.localRotation = Quaternion.identity;
      this.m_actor.transform.localScale = localScale;
      if (!this.m_isNew)
        return;
      this.m_actor.SetActorState(ActorStateType.CARD_RECENTLY_ACQUIRED);
    }
  }

  private void SetupRarity()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this.m_rarityInfo.gameObject);
    if ((Object) gameObject == (Object) null)
      return;
    gameObject.transform.parent = this.m_CardParent.transform;
    this.m_rarityInfo = gameObject.GetComponent<PackOpeningCardRarityInfo>();
    this.m_rarityInfo.m_RarityObject.SetActive(true);
    this.m_rarityInfo.m_HiddenCardObject.SetActive(true);
    Vector3 localPosition = this.m_rarityInfo.m_HiddenCardObject.transform.localPosition;
    this.m_rarityInfo.m_HiddenCardObject.transform.parent = this.m_CardParent.transform;
    this.m_rarityInfo.m_HiddenCardObject.transform.localPosition = localPosition;
    this.m_rarityInfo.m_HiddenCardObject.transform.localRotation = Quaternion.identity;
    this.m_rarityInfo.m_HiddenCardObject.transform.localScale = new Vector3(7.646f, 7.646f, 7.646f);
    TransformUtil.AttachAndPreserveLocalTransform(this.m_rarityInfo.m_RarityObject.transform, this.m_CardParent.transform);
    this.m_spell = this.m_rarityInfo.m_RarityObject.GetComponent<Spell>();
    this.m_revealButton = this.m_rarityInfo.m_RarityObject.GetComponent<PegUIElement>();
    if (UniversalInputManager.Get().IsTouchMode())
      this.m_revealButton.SetReceiveReleaseWithoutMouseDown(true);
    this.m_SharedHiddenCardObject.transform.parent = this.m_rarityInfo.m_HiddenCardObject.transform;
    TransformUtil.Identity((Component) this.m_SharedHiddenCardObject.transform);
  }

  private void EnableRarityInfo(PackOpeningCardRarityInfo info, bool enable)
  {
    if ((Object) info.m_RarityObject != (Object) null)
      info.m_RarityObject.SetActive(enable);
    if (!((Object) info.m_HiddenCardObject != (Object) null))
      return;
    info.m_HiddenCardObject.SetActive(enable);
  }

  private void OnOver(UIEvent e)
  {
    this.m_spell.ActivateState(SpellStateType.BIRTH);
  }

  private void OnOut(UIEvent e)
  {
    this.m_spell.ActivateState(SpellStateType.CANCEL);
  }

  private void OnOverWhileFlipped(UIEvent e)
  {
    if (this.m_isNew)
      this.m_actor.SetActorState(ActorStateType.CARD_RECENTLY_ACQUIRED_MOUSE_OVER);
    else
      this.m_actor.SetActorState(ActorStateType.CARD_HISTORY);
    TooltipPanelManager.Get().UpdateKeywordHelpForPackOpening(this.m_actor.GetEntityDef(), this.m_actor);
  }

  private void OnOutWhileFlipped(UIEvent e)
  {
    if (this.m_isNew)
      this.m_actor.SetActorState(ActorStateType.CARD_RECENTLY_ACQUIRED);
    else
      this.m_actor.SetActorState(ActorStateType.CARD_IDLE);
    TooltipPanelManager.Get().HideKeywordHelp();
  }

  private void OnPress(UIEvent e)
  {
    this.m_revealed = true;
    this.UpdateInput();
    this.m_spell.AddFinishedCallback(new Spell.FinishedCallback(this.OnSpellFinished));
    this.m_spell.ActivateState(SpellStateType.ACTION);
    this.PlayCorrectSound();
  }

  private void UpdateInput()
  {
    if (!this.IsReady())
      return;
    bool flag = !this.IsRevealed() && this.IsInputEnabled();
    if (!((Object) this.m_revealButton != (Object) null) || (bool) UniversalInputManager.UsePhoneUI)
      return;
    if (flag)
    {
      this.m_revealButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnOver));
      this.m_revealButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnOut));
      this.m_revealButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnPress));
      if (!((Object) PegUI.Get().FindHitElement() == (Object) this.m_revealButton))
        return;
      this.OnOver((UIEvent) null);
    }
    else
    {
      this.m_revealButton.RemoveEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnOver));
      this.m_revealButton.RemoveEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnOut));
      this.m_revealButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnPress));
    }
  }

  private void BecomeReady()
  {
    this.m_ready = true;
    this.UpdateInput();
    this.UpdateActor();
  }

  private void FireRevealedEvent()
  {
    foreach (PackOpeningCard.RevealedListener revealedListener in this.m_revealedListeners.ToArray())
      revealedListener.Fire();
  }

  private void OnSpellFinished(Spell spell, object userData)
  {
    this.FireRevealedEvent();
    this.UpdateInput();
    this.ShowClassName();
    this.ShowIsNew();
    this.m_revealButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnOverWhileFlipped));
    this.m_revealButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnOutWhileFlipped));
  }

  private void ShowClassName()
  {
    string className = this.GetClassName();
    foreach (UberText componentsInChild in this.m_ClassNameSpell.GetComponentsInChildren<UberText>(true))
    {
      componentsInChild.Text = className;
      if (this.m_entityDef.IsMultiClass())
        componentsInChild.OutlineSize = 3f;
    }
    this.m_ClassNameSpell.ActivateState(SpellStateType.BIRTH);
  }

  private void ShowIsNew()
  {
    if (!this.m_isNew || !((Object) this.m_IsNewSpell != (Object) null))
      return;
    this.m_IsNewSpell.ActivateState(SpellStateType.BIRTH);
  }

  private string GetClassName()
  {
    TAG_CLASS tag = this.m_entityDef.GetClass();
    if (this.m_entityDef.IsMultiClass())
      return this.GetFamilyClassNames();
    if (tag == TAG_CLASS.NEUTRAL)
      return GameStrings.Get("GLUE_PACK_OPENING_ALL_CLASSES");
    return GameStrings.GetClassName(tag);
  }

  private string GetFamilyClassNames()
  {
    if (this.m_entityDef.HasTag(GAME_TAG.GRIMY_GOONS))
      return GameStrings.Get("GLUE_GOONS_CLASS_NAMES");
    if (this.m_entityDef.HasTag(GAME_TAG.JADE_LOTUS))
      return GameStrings.Get("GLUE_LOTUS_CLASS_NAMES");
    if (this.m_entityDef.HasTag(GAME_TAG.KABAL))
      return GameStrings.Get("GLUE_KABAL_CLASS_NAMES");
    return GameStrings.Get("GLUE_PACK_OPENING_ALL_CLASSES");
  }

  private void PlayCorrectSound()
  {
    switch (this.m_rarityInfo.m_RarityType)
    {
      case PackOpeningRarity.COMMON:
        if (this.m_premium != TAG_PREMIUM.GOLDEN)
          break;
        SoundManager.Get().LoadAndPlay("VO_ANNOUNCER_FOIL_C_29");
        break;
      case PackOpeningRarity.RARE:
        if (this.m_premium == TAG_PREMIUM.GOLDEN)
        {
          SoundManager.Get().LoadAndPlay("VO_ANNOUNCER_FOIL_R_30");
          break;
        }
        SoundManager.Get().LoadAndPlay("VO_ANNOUNCER_RARE_27");
        break;
      case PackOpeningRarity.EPIC:
        if (this.m_premium == TAG_PREMIUM.GOLDEN)
        {
          SoundManager.Get().LoadAndPlay("VO_ANNOUNCER_FOIL_E_31");
          break;
        }
        SoundManager.Get().LoadAndPlay("VO_ANNOUNCER_EPIC_26");
        break;
      case PackOpeningRarity.LEGENDARY:
        if (this.m_premium == TAG_PREMIUM.GOLDEN)
        {
          SoundManager.Get().LoadAndPlay("VO_ANNOUNCER_FOIL_L_32");
          break;
        }
        SoundManager.Get().LoadAndPlay("VO_ANNOUNCER_LEGENDARY_25");
        break;
    }
  }

  private class RevealedListener : EventListener<PackOpeningCard.RevealedCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  public delegate void RevealedCallback(object userData);
}
