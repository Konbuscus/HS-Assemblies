﻿// Decompiled with JetBrains decompiler
// Type: NormalButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class NormalButton : PegUIElement
{
  [CustomEditField(Sections = "Mouse Over Settings")]
  public float m_userOverYOffset = -0.05f;
  [CustomEditField(Sections = "Button Properties")]
  public GameObject m_button;
  [CustomEditField(Sections = "Button Properties")]
  public TextMesh m_buttonText;
  [CustomEditField(Sections = "Button Properties")]
  public UberText m_buttonUberText;
  [CustomEditField(Sections = "Mouse Over Settings")]
  public GameObject m_mouseOverBone;
  private Vector3 m_originalButtonPosition;
  private int buttonID;

  protected override void Awake()
  {
    this.SetOriginalButtonPosition();
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    if ((Object) this.m_mouseOverBone != (Object) null)
      this.m_button.transform.position = this.m_mouseOverBone.transform.position;
    else
      TransformUtil.SetLocalPosY(this.m_button.gameObject, this.m_originalButtonPosition.y + this.m_userOverYOffset);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_button.gameObject.transform.localPosition = this.m_originalButtonPosition;
  }

  public void SetUserOverYOffset(float userOverYOffset)
  {
    this.m_userOverYOffset = userOverYOffset;
  }

  public void SetButtonID(int newID)
  {
    this.buttonID = newID;
  }

  public int GetButtonID()
  {
    return this.buttonID;
  }

  public void SetText(string t)
  {
    if ((Object) this.m_buttonUberText == (Object) null)
      this.m_buttonText.text = t;
    else
      this.m_buttonUberText.Text = t;
  }

  public float GetTextWidth()
  {
    if ((Object) this.m_buttonUberText == (Object) null)
      return this.m_buttonText.GetComponent<Renderer>().bounds.extents.x * 2f;
    return this.m_buttonUberText.Width;
  }

  public float GetTextHeight()
  {
    if ((Object) this.m_buttonUberText == (Object) null)
      return this.m_buttonText.GetComponent<Renderer>().bounds.extents.y * 2f;
    return this.m_buttonUberText.Height;
  }

  public float GetRight()
  {
    return this.GetComponent<BoxCollider>().bounds.max.x;
  }

  public float GetLeft()
  {
    Bounds bounds = this.GetComponent<BoxCollider>().bounds;
    return bounds.center.x - bounds.extents.x;
  }

  public float GetTop()
  {
    Bounds bounds = this.GetComponent<BoxCollider>().bounds;
    return bounds.center.y + bounds.extents.y;
  }

  public float GetBottom()
  {
    Bounds bounds = this.GetComponent<BoxCollider>().bounds;
    return bounds.center.y - bounds.extents.y;
  }

  public void SetOriginalButtonPosition()
  {
    this.m_originalButtonPosition = this.m_button.transform.localPosition;
  }

  public GameObject GetButtonTextGO()
  {
    if ((Object) this.m_buttonUberText == (Object) null)
      return this.m_buttonText.gameObject;
    return this.m_buttonUberText.gameObject;
  }

  public UberText GetButtonUberText()
  {
    return this.m_buttonUberText;
  }

  public string GetText()
  {
    if ((Object) this.m_buttonUberText == (Object) null)
      return this.m_buttonText.text;
    return this.m_buttonUberText.Text;
  }
}
