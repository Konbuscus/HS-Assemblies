﻿// Decompiled with JetBrains decompiler
// Type: FbDebugOverride
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public static class FbDebugOverride
{
  private static bool allowLogging = true;

  public static void Error(string msg)
  {
    if (!FbDebugOverride.allowLogging)
      return;
    FbDebug.Error(msg);
  }

  public static void Info(string msg)
  {
    if (!FbDebugOverride.allowLogging)
      return;
    FbDebug.Info(msg);
  }

  public static void Log(string msg)
  {
    if (!FbDebugOverride.allowLogging)
      return;
    FbDebug.Log(msg);
  }

  public static void Warn(string msg)
  {
    if (!FbDebugOverride.allowLogging)
      return;
    FbDebug.Warn(msg);
  }
}
