﻿// Decompiled with JetBrains decompiler
// Type: MATSample
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MATSample : MonoBehaviour
{
  private void Awake()
  {
  }

  private void OnGUI()
  {
    GUI.Label(new Rect(10f, 5f, (float) (Screen.width - 20), (float) (Screen.height / 10)), "MAT Unity Test App", new GUIStyle()
    {
      fontStyle = FontStyle.Bold,
      fontSize = 50,
      alignment = TextAnchor.MiddleCenter,
      normal = {
        textColor = Color.white
      }
    });
    GUI.skin.button.fontSize = 40;
    if (GUI.Button(new Rect(10f, (float) (Screen.height / 10), (float) (Screen.width - 20), (float) (Screen.height / 10)), "Start MAT"))
      MonoBehaviour.print((object) "Start MAT clicked");
    else if (GUI.Button(new Rect(10f, (float) (2 * Screen.height / 10), (float) (Screen.width - 20), (float) (Screen.height / 10)), "Set Delegate"))
      MonoBehaviour.print((object) "Set Delegate clicked");
    else if (GUI.Button(new Rect(10f, (float) (3 * Screen.height / 10), (float) (Screen.width - 20), (float) (Screen.height / 10)), "Enable Debug Mode"))
      MonoBehaviour.print((object) "Enable Debug Mode clicked");
    else if (GUI.Button(new Rect(10f, (float) (4 * Screen.height / 10), (float) (Screen.width - 20), (float) (Screen.height / 10)), "Allow Duplicates"))
      MonoBehaviour.print((object) "Allow Duplicates clicked");
    else if (GUI.Button(new Rect(10f, (float) (5 * Screen.height / 10), (float) (Screen.width - 20), (float) (Screen.height / 10)), "Measure Session"))
      MonoBehaviour.print((object) "Measure Session clicked");
    else if (GUI.Button(new Rect(10f, (float) (6 * Screen.height / 10), (float) (Screen.width - 20), (float) (Screen.height / 10)), "Measure Event"))
      MonoBehaviour.print((object) "Measure Event clicked");
    else if (GUI.Button(new Rect(10f, (float) (7 * Screen.height / 10), (float) (Screen.width - 20), (float) (Screen.height / 10)), "Measure Event With Event Items"))
      MonoBehaviour.print((object) "Measure Event With Event Items clicked");
    else if (GUI.Button(new Rect(10f, (float) (8 * Screen.height / 10), (float) (Screen.width - 20), (float) (Screen.height / 10)), "Test Setter Methods"))
    {
      MonoBehaviour.print((object) "Test Setter Methods clicked");
    }
    else
    {
      if (!GUI.Button(new Rect(10f, (float) (9 * Screen.height / 10), (float) (Screen.width - 20), (float) (Screen.height / 10)), "Test Getter Methods"))
        return;
      MonoBehaviour.print((object) "Test Getter Methods clicked");
    }
  }

  public static string getSampleiTunesIAPReceipt()
  {
    return "dGhpcyBpcyBhIHNhbXBsZSBpb3MgYXBwIHN0b3JlIHJlY2VpcHQ=";
  }
}
