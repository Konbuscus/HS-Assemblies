﻿// Decompiled with JetBrains decompiler
// Type: ManaFilterTab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class ManaFilterTab : PegUIElement
{
  public static readonly int ALL_TAB_IDX = -1;
  public static readonly int SEVEN_PLUS_TAB_IDX = 7;
  public UberText m_costText;
  public UberText m_otherText;
  public ManaCrystal m_crystal;
  private int m_manaID;
  private ManaFilterTab.FilterState m_filterState;
  private AudioSource m_mouseOverSound;

  protected override void Awake()
  {
    this.m_crystal.MarkAsNotInGame();
    base.Awake();
  }

  public void SetFilterState(ManaFilterTab.FilterState state)
  {
    this.m_filterState = state;
    switch (this.m_filterState)
    {
      case ManaFilterTab.FilterState.ON:
        this.m_crystal.state = ManaCrystal.State.PROPOSED;
        break;
      case ManaFilterTab.FilterState.OFF:
        this.m_crystal.state = ManaCrystal.State.READY;
        break;
      case ManaFilterTab.FilterState.DISABLED:
        this.m_crystal.state = ManaCrystal.State.USED;
        break;
    }
  }

  public void NotifyMousedOver()
  {
    if (this.m_filterState == ManaFilterTab.FilterState.ON)
      return;
    this.m_crystal.state = ManaCrystal.State.PROPOSED;
    SoundManager.Get().LoadAndPlay("mana_crystal_highlight_lp", this.gameObject, 1f, new SoundManager.LoadedCallback(this.ManaCrystalSoundCallback));
  }

  public void NotifyMousedOut()
  {
    Hashtable args = iTween.Hash((object) "from", (object) 1f, (object) "to", (object) 0.0f, (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.linear, (object) "onupdate", (object) (Action<object>) (amount => SoundManager.Get().SetVolume(this.m_mouseOverSound, (float) amount)));
    iTween.Stop(this.gameObject);
    iTween.ValueTo(this.gameObject, args);
    if (this.m_filterState == ManaFilterTab.FilterState.ON)
      return;
    this.m_crystal.state = ManaCrystal.State.READY;
  }

  private void ManaCrystalSoundCallback(AudioSource source, object userData)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ManaFilterTab.\u003CManaCrystalSoundCallback\u003Ec__AnonStorey3A6 callbackCAnonStorey3A6 = new ManaFilterTab.\u003CManaCrystalSoundCallback\u003Ec__AnonStorey3A6();
    // ISSUE: reference to a compiler-generated field
    callbackCAnonStorey3A6.source = source;
    if ((UnityEngine.Object) this.m_mouseOverSound != (UnityEngine.Object) null)
      SoundManager.Get().Stop(this.m_mouseOverSound);
    // ISSUE: reference to a compiler-generated field
    this.m_mouseOverSound = callbackCAnonStorey3A6.source;
    // ISSUE: reference to a compiler-generated field
    SoundManager.Get().SetVolume(callbackCAnonStorey3A6.source, 0.0f);
    if (this.m_crystal.state != ManaCrystal.State.PROPOSED)
      SoundManager.Get().Stop(this.m_mouseOverSound);
    // ISSUE: reference to a compiler-generated method
    Hashtable args = iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.linear, (object) "onupdate", (object) new Action<object>(callbackCAnonStorey3A6.\u003C\u003Em__101));
    iTween.Stop(this.gameObject);
    iTween.ValueTo(this.gameObject, args);
  }

  public void SetManaID(int manaID)
  {
    this.m_manaID = manaID;
    this.UpdateManaText();
  }

  public int GetManaID()
  {
    return this.m_manaID;
  }

  private void UpdateManaText()
  {
    string empty1 = string.Empty;
    string empty2 = string.Empty;
    if (this.m_manaID == ManaFilterTab.ALL_TAB_IDX)
    {
      empty2 = GameStrings.Get("GLUE_COLLECTION_ALL");
    }
    else
    {
      empty1 = this.m_manaID.ToString();
      if (this.m_manaID == ManaFilterTab.SEVEN_PLUS_TAB_IDX)
      {
        if ((bool) UniversalInputManager.UsePhoneUI)
          empty1 += GameStrings.Get("GLUE_COLLECTION_PLUS");
        else
          empty2 = GameStrings.Get("GLUE_COLLECTION_PLUS");
      }
    }
    if ((UnityEngine.Object) this.m_costText != (UnityEngine.Object) null)
      this.m_costText.Text = empty1;
    if (!((UnityEngine.Object) this.m_otherText != (UnityEngine.Object) null))
      return;
    this.m_otherText.Text = empty2;
  }

  public enum FilterState
  {
    ON,
    OFF,
    DISABLED,
  }
}
