﻿// Decompiled with JetBrains decompiler
// Type: GeneralStorePacksContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

[CustomEditClass]
public class GeneralStorePacksContent : GeneralStoreContent
{
  public static readonly bool REQUIRE_REAL_MONEY_BUNDLE_OPTION = true;
  private static readonly int MAX_QUANTITY_BOUGHT_WITH_GOLD = 50;
  [CustomEditField(ListTable = true, Sections = "Pack Buy Buttons")]
  public List<GeneralStorePacksContent.ToggleableButtonFrame> m_toggleableButtonFrames = new List<GeneralStorePacksContent.ToggleableButtonFrame>();
  [CustomEditField(Sections = "Packs")]
  public int m_maxPackBuyButtons = 10;
  [CustomEditField(Sections = "Animation")]
  public float m_packFlyOutAnimTime = 0.1f;
  [CustomEditField(Sections = "Animation")]
  public float m_packFlyOutDelay = 0.005f;
  [CustomEditField(Sections = "Animation")]
  public float m_packFlyInAnimTime = 0.2f;
  [CustomEditField(Sections = "Animation")]
  public float m_packFlyInDelay = 0.01f;
  [CustomEditField(Sections = "Animation")]
  public float m_boxFlyOutAnimTime = 0.2f;
  [CustomEditField(Sections = "Animation")]
  public float m_boxFlyOutDelay = 0.005f;
  [CustomEditField(Sections = "Animation")]
  public float m_boxFlyInAnimTime = 0.5f;
  [CustomEditField(Sections = "Animation")]
  public float m_boxFlyInDelay = 0.1f;
  [CustomEditField(Sections = "Animation")]
  public float m_boxFlyInXShake = 35f;
  [CustomEditField(Sections = "Animation")]
  public float m_boxStoreImpactTranslation = -70f;
  [CustomEditField(Sections = "Animation")]
  public float m_shakeObjectDelayMultiplier = 0.7f;
  [CustomEditField(Sections = "Animation")]
  public float m_backgroundFlipAnimTime = 0.5f;
  [CustomEditField(Sections = "Animation")]
  public float m_maxPackFlyInXShake = 20f;
  [CustomEditField(Sections = "Animation")]
  public float m_maxPackFlyOutXShake = 12f;
  [CustomEditField(Sections = "Animation")]
  public float m_packFlyShakeTime = 2f;
  [CustomEditField(Sections = "Animation")]
  public float m_backgroundFlipShake = 20f;
  [CustomEditField(Sections = "Animation")]
  public float m_PackYDegreeVariationMag = 2f;
  [CustomEditField(Sections = "Animation")]
  public float m_BoxYDegreeVariationMag = 1f;
  [CustomEditField(Sections = "Animation/Appear")]
  public float m_logoHoldTime = 1f;
  [CustomEditField(Sections = "Animation/Appear")]
  public float m_logoDisplayPunchTime = 0.5f;
  [CustomEditField(Sections = "Animation/Appear")]
  public float m_logoIntroTime = 0.25f;
  [CustomEditField(Sections = "Animation/Appear")]
  public float m_logoOutroTime = 0.25f;
  private List<GeneralStorePackBuyButton> m_packBuyButtons = new List<GeneralStorePackBuyButton>();
  private int m_currentGoldPackQuantity = 1;
  private int m_currentDisplay = -1;
  private Map<int, StorePackDef> m_storePackDefs = new Map<int, StorePackDef>();
  private const float FIRST_PURCHASE_BUNDLE_INIT_DELAY = 0.5f;
  private const string PREV_PLAYLIST_NAME = "StorePrevCurrentPlaylist";
  public StoreQuantityPrompt m_quantityPrompt;
  public GameObject m_packContainer;
  public GameObject m_packEmptyDisplay;
  public GeneralStorePacksContentDisplay m_packDisplay;
  [CustomEditField(Sections = "Pack Buy Buttons")]
  public GameObject m_packBuyContainer;
  [CustomEditField(Sections = "Pack Buy Buttons")]
  public MultiSliceElement m_packBuyButtonContainer;
  [CustomEditField(Sections = "Pack Buy Buttons")]
  public GeneralStorePackBuyButton m_packBuyButtonPrefab;
  [CustomEditField(Sections = "Pack Buy Buttons")]
  public MultiSliceElement m_packBuyFrameContainer;
  [CustomEditField(Sections = "Pack Buy Buttons/Preorder")]
  public GameObject m_packBuyPreorderContainer;
  [CustomEditField(Sections = "Pack Buy Buttons/Preorder")]
  public GeneralStorePackBuyButton m_packBuyPreorderButton;
  [CustomEditField(Sections = "Pack Buy Buttons/Preorder")]
  public UberText m_availableDateText;
  [CustomEditField(Sections = "China Button")]
  public UIBButton m_ChinaInfoButton;
  [CustomEditField(Sections = "Packs")]
  public GeneralStorePacksContent.LogoAnimation m_logoAnimation;
  [CustomEditField(Sections = "Animation")]
  public float m_backgroundFlipShakeDelay;
  [CustomEditField(Sections = "Animation/Appear")]
  public GameObject m_logoAnimationStartBone;
  [CustomEditField(Sections = "Animation/Appear")]
  public GameObject m_logoAnimationEndBone;
  [CustomEditField(Sections = "Animation/Appear")]
  public MeshRenderer m_logoMesh;
  [CustomEditField(Sections = "Animation/Appear")]
  public MeshRenderer m_logoGlowMesh;
  [CustomEditField(Sections = "Animation/Appear")]
  public Vector3 m_punchAmount;
  [CustomEditField(Sections = "Animation/Appear")]
  public Vector3 m_logoAppearOffset;
  [CustomEditField(Sections = "Animation/Preorder")]
  public GeneralStoreRewardsCardBack m_preorderCardBackReward;
  [CustomEditField(Sections = "Sounds & Music", T = EditType.SOUND_PREFAB)]
  public string m_backgroundFlipSound;
  private int m_selectedBoosterId;
  private int m_visiblePackCount;
  private int m_lastBundleIndex;
  private GeneralStorePacksContentDisplay m_packDisplay1;
  private GeneralStorePacksContentDisplay m_packDisplay2;
  private MeshRenderer m_logoMesh1;
  private MeshRenderer m_logoMesh2;
  private MeshRenderer m_logoGlowMesh1;
  private MeshRenderer m_logoGlowMesh2;
  private Coroutine m_logoAnimCoroutine;
  private Coroutine m_packAnimCoroutine;
  private Coroutine m_preorderCardBackAnimCoroutine;
  private Vector3 m_savedLocalPosition;
  private Vector3 m_availableDateTextOrigScale;
  private bool m_animatingLogo;
  private bool m_animatingPacks;
  private bool m_hasLogo;
  private bool m_waitingForBoxAnim;

  public override void PostStoreFlipIn(bool animatedFlipIn)
  {
    this.UpdatePacksTypeMusic();
    this.AnimateLogo(animatedFlipIn);
    if (StoreManager.IsFirstPurchaseBundleBooster(this.m_selectedBoosterId))
    {
      if ((bool) UniversalInputManager.UsePhoneUI)
        this.AnimatePacksFlying(this.m_visiblePackCount, false, 1f);
      else
        this.AnimatePacksFlying(this.m_visiblePackCount, false, 0.0f);
    }
    else
      this.AnimatePacksFlying(this.m_visiblePackCount, !animatedFlipIn, 0.0f);
    this.UpdateChinaKoreaInfoButton();
    this.m_savedLocalPosition = this.gameObject.transform.localPosition;
  }

  public override void PreStoreFlipOut()
  {
    this.ResetAnimations();
    this.GetCurrentDisplay().ClearPacks();
    this.UpdateChinaKoreaInfoButton();
  }

  public override void StoreShown(bool isCurrent)
  {
    if (!isCurrent)
      return;
    this.AnimateLogo(false);
    if (StoreManager.IsFirstPurchaseBundleBooster(this.m_selectedBoosterId))
      this.AnimatePacksFlying(this.m_visiblePackCount, false, 0.0f);
    else
      this.AnimatePacksFlying(this.m_visiblePackCount, true, 0.0f);
    this.UpdatePackBuyButtons();
    this.UpdatePacksTypeMusic();
    this.UpdateChinaKoreaInfoButton();
  }

  public override void StoreHidden(bool isCurrent)
  {
    if (!isCurrent)
      return;
    this.ResetAnimations();
    if ((UnityEngine.Object) this.m_quantityPrompt != (UnityEngine.Object) null)
      this.m_quantityPrompt.Hide();
    this.GetCurrentDisplay().ClearPacks();
  }

  public override bool IsPurchaseDisabled()
  {
    return this.m_selectedBoosterId == 0;
  }

  public override string GetMoneyDisplayOwnedText()
  {
    return GameStrings.Get("GLUE_STORE_PACK_BUTTON_COST_OWNED_TEXT");
  }

  public void SetBoosterId(int id, bool forceImmediate = false, bool InitialSelection = false)
  {
    if (this.m_selectedBoosterId == id)
      return;
    bool flag = this.m_selectedBoosterId == 0;
    this.GetCurrentDisplay().ClearPacks();
    this.m_visiblePackCount = 0;
    this.m_selectedBoosterId = id;
    if (flag)
      this.UpdateSelectedBundle(false);
    this.ResetAnimations();
    this.AnimateAndUpdateDisplay(id, forceImmediate);
    this.AnimateLogo(!forceImmediate);
    Log.Kyle.Print("InitialSelection = {0}", (object) InitialSelection);
    if (StoreManager.IsFirstPurchaseBundleBooster(this.m_selectedBoosterId))
    {
      float delay = !InitialSelection ? 0.0f : 0.5f;
      Log.Kyle.Print("InitialSelection delay={0}", (object) delay);
      this.AnimatePacksFlying(this.m_visiblePackCount, false, delay);
    }
    else
      this.AnimatePacksFlying(this.m_visiblePackCount, forceImmediate, 0.0f);
    this.UpdatePackBuyButtons();
    this.UpdatePacksDescription();
    this.UpdatePacksTypeMusic();
    this.UpdateChinaKoreaInfoButton();
    if (this.GetCurrentGoldBundle() != null)
    {
      this.SetCurrentGoldBundle(this.GetCurrentGTAPPTransactionData());
    }
    else
    {
      if (this.GetCurrentMoneyBundle() == null)
        return;
      this.HandleMoneyPackBuyButtonClick(this.m_lastBundleIndex);
    }
  }

  public int GetBoosterId()
  {
    return this.m_selectedBoosterId;
  }

  public void FirstPurchaseBundlePurchased(string cardID)
  {
    GeneralStorePacksContentDisplay currentDisplay = this.GetCurrentDisplay();
    if ((UnityEngine.Object) currentDisplay == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarningFormat("FirstPurchaseBundlePurchased() failed to get GeneralStorePacksContentDisplay for cardID {0}", (object) cardID);
    else
      currentDisplay.PurchaseBundleBox(cardID);
  }

  public Map<int, StorePackDef> GetStorePackDefs()
  {
    return this.m_storePackDefs;
  }

  public StorePackDef GetStorePackDef(int packDbId)
  {
    StorePackDef storePackDef = (StorePackDef) null;
    this.m_storePackDefs.TryGetValue(packDbId, out storePackDef);
    return storePackDef;
  }

  public void ShakeStore(int numPacks, float maxXRotation, float delay = 0.0f, float translationAmount = 0.0f)
  {
    if (numPacks == 0)
      return;
    int b = 1;
    using (List<Network.Bundle>.Enumerator enumerator = this.GetPackBundles(false).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.BundleItem bundleItem = enumerator.Current.Items.Find((Predicate<Network.BundleItem>) (obj => obj.Product == ProductType.PRODUCT_TYPE_BOOSTER));
        if (bundleItem != null)
          b = Mathf.Max(bundleItem.Quantity, b);
      }
    }
    int num = b - 1;
    if (num == 0)
      return;
    float xRotationAmount = (float) numPacks / (float) num * maxXRotation;
    float translateAmount = 0.0f;
    if (StoreManager.IsFirstPurchaseBundleBooster(this.m_selectedBoosterId))
      translateAmount = translationAmount;
    this.m_parentStore.ShakeStore(xRotationAmount, this.m_packFlyShakeTime, delay, translateAmount);
  }

  protected override void OnBundleChanged(NoGTAPPTransactionData goldBundle, Network.Bundle moneyBundle)
  {
    if (this.m_selectedBoosterId == 17)
      return;
    if (goldBundle != null)
      this.m_visiblePackCount = goldBundle.Quantity;
    else if (moneyBundle != null)
    {
      Network.BundleItem bundleItem = moneyBundle.Items.Find((Predicate<Network.BundleItem>) (obj => obj.Product == ProductType.PRODUCT_TYPE_BOOSTER));
      this.m_visiblePackCount = bundleItem != null ? bundleItem.Quantity : 0;
    }
    this.AnimatePacksFlying(this.m_visiblePackCount, false, 0.0f);
  }

  protected override void OnRefresh()
  {
    this.UpdatePackBuyButtons();
    this.UpdatePacksDescription();
    if (this.HasBundleSet() || this.m_selectedBoosterId == 0)
      return;
    this.UpdateSelectedBundle(true);
  }

  private void Awake()
  {
    this.m_packDisplay1 = this.m_packDisplay;
    this.m_packDisplay2 = UnityEngine.Object.Instantiate<GeneralStorePacksContentDisplay>(this.m_packDisplay);
    this.m_packDisplay2.transform.parent = this.m_packDisplay1.transform.parent;
    this.m_packDisplay2.transform.localPosition = this.m_packDisplay1.transform.localPosition;
    this.m_packDisplay2.transform.localScale = this.m_packDisplay1.transform.localScale;
    this.m_packDisplay2.transform.localRotation = this.m_packDisplay1.transform.localRotation;
    this.m_packDisplay2.gameObject.SetActive(false);
    this.m_logoMesh1 = this.m_logoMesh;
    this.m_logoMesh2 = UnityEngine.Object.Instantiate<MeshRenderer>(this.m_logoMesh);
    this.m_logoMesh2.transform.parent = this.m_logoMesh1.transform.parent;
    this.m_logoMesh2.transform.localPosition = this.m_logoMesh1.transform.localPosition;
    this.m_logoMesh2.transform.localScale = this.m_logoMesh1.transform.localScale;
    this.m_logoMesh2.transform.localRotation = this.m_logoMesh1.transform.localRotation;
    this.m_logoMesh2.gameObject.SetActive(false);
    this.m_logoGlowMesh1 = this.m_logoGlowMesh;
    this.m_logoGlowMesh2 = UnityEngine.Object.Instantiate<MeshRenderer>(this.m_logoGlowMesh);
    this.m_logoGlowMesh2.transform.parent = this.m_logoGlowMesh1.transform.parent;
    this.m_logoGlowMesh2.transform.localPosition = this.m_logoGlowMesh1.transform.localPosition;
    this.m_logoGlowMesh2.transform.localScale = this.m_logoGlowMesh1.transform.localScale;
    this.m_logoGlowMesh2.transform.localRotation = this.m_logoGlowMesh1.transform.localRotation;
    this.m_logoGlowMesh2.gameObject.SetActive(false);
    this.m_packDisplay1.SetParent(this);
    this.m_packDisplay2.SetParent(this);
    this.m_productType = ProductType.PRODUCT_TYPE_BOOSTER;
    this.m_packBuyContainer.SetActive(false);
    if ((UnityEngine.Object) this.m_availableDateText != (UnityEngine.Object) null)
      this.m_availableDateTextOrigScale = this.m_availableDateText.transform.localScale;
    if ((UnityEngine.Object) this.m_ChinaInfoButton != (UnityEngine.Object) null)
      this.m_ChinaInfoButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnChinaKoreaInfoPressed));
    using (List<BoosterDbfRecord>.Enumerator enumerator = GameUtils.GetPackRecordsWithStorePrefab().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoosterDbfRecord current = enumerator.Current;
        int id = current.ID;
        string storePrefab = current.StorePrefab;
        GameObject gameObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(storePrefab), false, false);
        if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
        {
          UnityEngine.Debug.LogError((object) string.Format("Unable to load store pack def: {0}", (object) storePrefab));
        }
        else
        {
          StorePackDef component = gameObject.GetComponent<StorePackDef>();
          if ((UnityEngine.Object) component == (UnityEngine.Object) null)
            UnityEngine.Debug.LogError((object) string.Format("StorePackDef component not found: {0}", (object) storePrefab));
          else
            this.m_storePackDefs.Add(id, component);
        }
      }
    }
    this.UpdateChinaKoreaInfoButton();
  }

  private GameObject GetCurrentDisplayContainer()
  {
    return this.GetCurrentDisplay().gameObject;
  }

  private GameObject GetNextDisplayContainer()
  {
    if ((this.m_currentDisplay + 1) % 2 == 0)
      return this.m_packDisplay1.gameObject;
    return this.m_packDisplay2.gameObject;
  }

  private GeneralStorePacksContentDisplay GetCurrentDisplay()
  {
    if (this.m_currentDisplay == 0)
      return this.m_packDisplay1;
    return this.m_packDisplay2;
  }

  private MeshRenderer GetCurrentLogo()
  {
    if (this.m_currentDisplay == 0)
      return this.m_logoMesh1;
    return this.m_logoMesh2;
  }

  private MeshRenderer GetCurrentGlowLogo()
  {
    if (this.m_currentDisplay == 0)
      return this.m_logoGlowMesh1;
    return this.m_logoGlowMesh2;
  }

  private void UpdateSelectedBundle(bool forceUpdate = false)
  {
    NoGTAPPTransactionData gtappTransactionData = new NoGTAPPTransactionData() { Product = this.m_productType, ProductData = this.m_selectedBoosterId, Quantity = 1 };
    long cost;
    if (StoreManager.Get().GetGoldCostNoGTAPP(gtappTransactionData, out cost))
    {
      this.SetCurrentGoldBundle(gtappTransactionData);
    }
    else
    {
      Network.Bundle costUnownedBundle = StoreManager.Get().GetLowestCostUnownedBundle(this.m_productType, false, this.m_selectedBoosterId, 0);
      if (costUnownedBundle == null)
        return;
      this.SetCurrentMoneyBundle(costUnownedBundle, forceUpdate);
    }
  }

  private void UpdatePacksDescription()
  {
    if (this.m_selectedBoosterId == 0)
    {
      this.m_parentStore.HideAccentTexture();
      this.m_parentStore.SetChooseDescription(GameStrings.Get("GLUE_STORE_CHOOSE_PACK"));
    }
    else
    {
      BoosterDbfRecord record = GameDbf.Booster.GetRecord(this.m_selectedBoosterId);
      string name = (string) record.Name;
      string title = GameStrings.Get("GLUE_STORE_PRODUCT_DETAILS_HEADLINE_PACK");
      string desc = GameStrings.Format("GLUE_STORE_PRODUCT_DETAILS_PACK", (object) name);
      Network.Bundle currentMoneyBundle = this.GetCurrentMoneyBundle();
      bool flag = false;
      if (currentMoneyBundle != null)
      {
        flag = StoreManager.Get().IsProductPrePurchase(currentMoneyBundle);
        if (flag && record.ID == 10)
        {
          desc = GameStrings.Get("GLUE_STORE_PRODUCT_DETAILS_TGT_PACK_PRESALE");
          title = GameStrings.Get("GLUE_STORE_PRODUCT_DETAILS_HEADLINE_TGT_PACK_PRESALE");
        }
        if (flag && record.ID == 11)
        {
          desc = GameStrings.Get("GLUE_STORE_PRODUCT_DETAILS_OG_PACK_PRESALE");
          title = GameStrings.Get("GLUE_STORE_PRODUCT_DETAILS_HEADLINE_OG_PACK_PRESALE");
        }
        if (StoreManager.IsFirstPurchaseBundleBooster(this.m_selectedBoosterId))
        {
          desc = GameStrings.Get("GLUE_STORE_PRODUCT_DETAILS_FIRST_PURCHASE_BUNDLE");
          title = GameStrings.Get("GLUE_STORE_PRODUCT_DETAILS_HEADLINE_FIRST_PURCHASE_BUNDLE");
        }
      }
      string warning = string.Empty;
      if (StoreManager.Get().IsKoreanCustomer())
        warning = !flag ? (!StoreManager.IsFirstPurchaseBundleBooster(this.m_selectedBoosterId) ? GameStrings.Get("GLUE_STORE_KOREAN_PRODUCT_DETAILS_EXPERT_PACK") : GameStrings.Get("GLUE_STORE_KOREAN_PRODUCT_DETAILS_FIRST_PURCHASE_BUNDLE")) : GameStrings.Get("GLUE_STORE_KOREAN_PRODUCT_DETAILS_PACKS_PREORDER");
      this.m_parentStore.SetDescription(title, desc, warning);
      StorePackDef storePackDef = this.GetStorePackDef(this.m_selectedBoosterId);
      if (!((UnityEngine.Object) storePackDef != (UnityEngine.Object) null))
        return;
      Texture texture = (Texture) null;
      if (!string.IsNullOrEmpty(storePackDef.m_accentTextureName))
        texture = AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(storePackDef.m_accentTextureName), false);
      this.m_parentStore.SetAccentTexture(texture);
    }
  }

  private NoGTAPPTransactionData GetCurrentGTAPPTransactionData()
  {
    return new NoGTAPPTransactionData() { Product = this.m_productType, ProductData = this.m_selectedBoosterId, Quantity = this.m_currentGoldPackQuantity };
  }

  private void UpdatePackBuyButtons()
  {
    if (this.m_selectedBoosterId == 0)
      return;
    Network.Bundle firstPurchaseBundle;
    bool flag1 = StoreManager.Get().IsBoosterFirstPurchaseBundle(this.m_selectedBoosterId, out firstPurchaseBundle);
    Network.Bundle preOrderBundle;
    bool flag2 = StoreManager.Get().IsBoosterPreorderActive(this.m_selectedBoosterId, out preOrderBundle);
    if (flag1)
      this.ShowFirstPurchaseBundleBuyButtons(firstPurchaseBundle);
    else if (flag2)
      this.ShowPreorderBuyButtons(preOrderBundle);
    else
      this.ShowStandardBuyButtons();
  }

  private void ShowStandardBuyButtons()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStorePacksContent.\u003CShowStandardBuyButtons\u003Ec__AnonStorey3F2 buttonsCAnonStorey3F2 = new GeneralStorePacksContent.\u003CShowStandardBuyButtons\u003Ec__AnonStorey3F2();
    // ISSUE: reference to a compiler-generated field
    buttonsCAnonStorey3F2.\u003C\u003Ef__this = this;
    this.m_packBuyPreorderContainer.SetActive(false);
    this.m_packBuyContainer.SetActive(true);
    int num1 = 0;
    // ISSUE: reference to a compiler-generated field
    buttonsCAnonStorey3F2.goldButton = this.GetPackBuyButton(num1);
    // ISSUE: reference to a compiler-generated field
    if ((UnityEngine.Object) buttonsCAnonStorey3F2.goldButton == (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated field
      buttonsCAnonStorey3F2.goldButton = this.CreatePackBuyButton(num1);
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      buttonsCAnonStorey3F2.goldButton.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(buttonsCAnonStorey3F2.\u003C\u003Em__1C6));
      if (!(bool) UniversalInputManager.UsePhoneUI)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        buttonsCAnonStorey3F2.goldButton.AddEventListener(UIEventType.DOUBLECLICK, new UIEvent.Handler(buttonsCAnonStorey3F2.\u003C\u003Em__1C7));
      }
    }
    if (this.m_selectedBoosterId != 0)
    {
      // ISSUE: reference to a compiler-generated field
      buttonsCAnonStorey3F2.goldButton.UpdateFromGTAPP(this.GetCurrentGTAPPTransactionData());
    }
    // ISSUE: reference to a compiler-generated method
    Action action = new Action(buttonsCAnonStorey3F2.\u003C\u003Em__1C8);
    // ISSUE: reference to a compiler-generated field
    buttonsCAnonStorey3F2.goldButton.Unselect();
    List<Network.Bundle> bundleList = this.GetPackBundles(true);
    if (bundleList.Count > this.m_maxPackBuyButtons - 1)
      bundleList = bundleList.GetRange(0, this.m_maxPackBuyButtons - 1);
    for (int index = 0; index < bundleList.Count; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      GeneralStorePacksContent.\u003CShowStandardBuyButtons\u003Ec__AnonStorey3F3 buttonsCAnonStorey3F3 = new GeneralStorePacksContent.\u003CShowStandardBuyButtons\u003Ec__AnonStorey3F3();
      // ISSUE: reference to a compiler-generated field
      buttonsCAnonStorey3F3.\u003C\u003Ef__this = this;
      ++num1;
      // ISSUE: reference to a compiler-generated field
      buttonsCAnonStorey3F3.bundleIndexCopy = index;
      Network.Bundle bundle = bundleList[index];
      Network.BundleItem bundleItem = bundle.Items.Find((Predicate<Network.BundleItem>) (obj => obj.Product == ProductType.PRODUCT_TYPE_BOOSTER));
      if (bundleItem == null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("GeneralStorePacksContent.UpdatePackBuyButtons() bundle {0} has no packs bundle item!", (object) bundle.ProductID));
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey3F3.moneyButton = this.GetPackBuyButton(num1);
        // ISSUE: reference to a compiler-generated field
        if ((UnityEngine.Object) buttonsCAnonStorey3F3.moneyButton == (UnityEngine.Object) null)
        {
          // ISSUE: reference to a compiler-generated field
          buttonsCAnonStorey3F3.moneyButton = this.CreatePackBuyButton(num1);
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          buttonsCAnonStorey3F3.moneyButton.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(buttonsCAnonStorey3F3.\u003C\u003Em__1CA));
        }
        string productQuantityText = StoreManager.Get().GetProductQuantityText(bundleItem.Product, bundleItem.ProductData, bundleItem.Quantity);
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey3F3.moneyButton.SetMoneyValue(bundle, productQuantityText);
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey3F3.moneyButton.gameObject.SetActive(true);
        // ISSUE: reference to a compiler-generated field
        if (buttonsCAnonStorey3F3.moneyButton.IsSelected() || this.GetCurrentMoneyBundle() == bundle)
        {
          // ISSUE: reference to a compiler-generated method
          action = new Action(buttonsCAnonStorey3F3.\u003C\u003Em__1CB);
        }
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey3F3.moneyButton.Unselect();
      }
    }
    bool flag1 = StoreManager.Get().CanBuyBoosterWithGold(this.m_selectedBoosterId);
    // ISSUE: reference to a compiler-generated field
    buttonsCAnonStorey3F2.goldButton.gameObject.SetActive(flag1);
    if (!flag1 && (UnityEngine.Object) this.m_packBuyButtons[0] != (UnityEngine.Object) null)
      this.m_packBuyButtons[0].gameObject.SetActive(false);
    for (int index = num1 + 1; index < this.m_packBuyButtons.Count; ++index)
    {
      GeneralStorePackBuyButton packBuyButton = this.m_packBuyButtons[index];
      if ((UnityEngine.Object) packBuyButton != (UnityEngine.Object) null)
        packBuyButton.gameObject.SetActive(false);
    }
    int num2 = 1;
    using (List<GeneralStorePacksContent.ToggleableButtonFrame>.Enumerator enumerator = this.m_toggleableButtonFrames.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GeneralStorePacksContent.ToggleableButtonFrame current = enumerator.Current;
        bool flag2 = num2 <= num1;
        current.m_IBar.SetActive(flag2);
        current.m_Middle.SetActive(flag2);
        ++num2;
      }
    }
    if ((UnityEngine.Object) this.m_packBuyFrameContainer != (UnityEngine.Object) null)
      this.m_packBuyFrameContainer.UpdateSlices();
    this.m_packBuyButtonContainer.UpdateSlices();
    if (action == null)
      return;
    action();
  }

  private void ShowFirstPurchaseBundleBuyButtons(Network.Bundle bundle)
  {
    this.m_packBuyContainer.SetActive(false);
    this.m_packBuyPreorderContainer.SetActive(false);
    this.HandleMoneyPackBuyButtonClick(0);
  }

  private void ShowPreorderBuyButtons(Network.Bundle preOrderBundle)
  {
    this.m_packBuyContainer.SetActive(false);
    this.m_packBuyPreorderContainer.SetActive(true);
    if (!this.m_packBuyPreorderButton.HasEventListener(UIEventType.PRESS))
      this.m_packBuyPreorderButton.AddEventListener(UIEventType.PRESS, (UIEvent.Handler) (e =>
      {
        if (!this.IsContentActive())
          return;
        this.HandleMoneyPackBuyButtonClick(0);
        this.m_packBuyPreorderButton.Select();
      }));
    string quantityText = string.Empty;
    if (preOrderBundle != null)
      quantityText = GameStrings.Format("GLUE_STORE_PACKS_BUTTON_PREORDER_TEXT", (object) preOrderBundle.Items.Find((Predicate<Network.BundleItem>) (obj => obj.Product == ProductType.PRODUCT_TYPE_BOOSTER)).Quantity);
    this.m_packBuyPreorderButton.SetMoneyValue(preOrderBundle, quantityText);
    this.HandleMoneyPackBuyButtonClick(0);
    this.m_packBuyPreorderButton.Select();
  }

  private void UpdatePacksTypeMusic()
  {
    if (this.m_parentStore.GetMode() == GeneralStoreMode.NONE)
      return;
    StorePackDef storePackDef = this.GetStorePackDef(this.m_selectedBoosterId);
    if (!((UnityEngine.Object) storePackDef == (UnityEngine.Object) null) && storePackDef.m_playlist != MusicPlaylistType.Invalid && MusicManager.Get().StartPlaylist(storePackDef.m_playlist))
      return;
    this.m_parentStore.ResumePreviousMusicPlaylist();
  }

  private void HandleGoldPackBuyButtonClick()
  {
    this.SetCurrentGoldBundle(new NoGTAPPTransactionData()
    {
      Product = this.m_productType,
      ProductData = this.m_selectedBoosterId,
      Quantity = this.m_currentGoldPackQuantity
    });
    this.UpdatePacksDescription();
  }

  private void HandleGoldPackBuyButtonDoubleClick(GeneralStorePackBuyButton button)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStorePacksContent.\u003CHandleGoldPackBuyButtonDoubleClick\u003Ec__AnonStorey3F4 clickCAnonStorey3F4 = new GeneralStorePacksContent.\u003CHandleGoldPackBuyButtonDoubleClick\u003Ec__AnonStorey3F4();
    // ISSUE: reference to a compiler-generated field
    clickCAnonStorey3F4.button = button;
    // ISSUE: reference to a compiler-generated field
    clickCAnonStorey3F4.\u003C\u003Ef__this = this;
    this.m_parentStore.ActivateCover(true);
    // ISSUE: reference to a compiler-generated method
    // ISSUE: reference to a compiler-generated method
    this.m_quantityPrompt.Show(GeneralStorePacksContent.MAX_QUANTITY_BOUGHT_WITH_GOLD, new StoreQuantityPrompt.OkayListener(clickCAnonStorey3F4.\u003C\u003Em__1CE), new StoreQuantityPrompt.CancelListener(clickCAnonStorey3F4.\u003C\u003Em__1CF));
  }

  private void HandleMoneyPackBuyButtonClick(int bundleIndex)
  {
    Network.Bundle bundle = (Network.Bundle) null;
    List<Network.Bundle> packBundles = this.GetPackBundles(true);
    if (packBundles != null && packBundles.Count > 0)
    {
      if (bundleIndex >= packBundles.Count)
        bundleIndex = 0;
      bundle = packBundles[bundleIndex];
    }
    this.SetCurrentMoneyBundle(bundle, true);
    this.m_lastBundleIndex = bundleIndex;
    this.UpdatePacksDescription();
  }

  private void SelectPackBuyButton(GeneralStorePackBuyButton packBuyBtn)
  {
    using (List<GeneralStorePackBuyButton>.Enumerator enumerator = this.m_packBuyButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Unselect();
    }
    packBuyBtn.Select();
  }

  private GeneralStorePackBuyButton GetPackBuyButton(int index)
  {
    if (index < this.m_packBuyButtons.Count)
      return this.m_packBuyButtons[index];
    return (GeneralStorePackBuyButton) null;
  }

  private GeneralStorePackBuyButton CreatePackBuyButton(int buttonIndex)
  {
    if (buttonIndex >= this.m_packBuyButtons.Count)
    {
      int num = buttonIndex - this.m_packBuyButtons.Count + 1;
      for (int index = 0; index < num; ++index)
      {
        GeneralStorePackBuyButton storePackBuyButton = (GeneralStorePackBuyButton) GameUtils.Instantiate((Component) this.m_packBuyButtonPrefab, this.m_packBuyButtonContainer.gameObject, true);
        SceneUtils.SetLayer(storePackBuyButton.gameObject, this.m_packBuyButtonContainer.gameObject.layer);
        storePackBuyButton.transform.localRotation = Quaternion.identity;
        storePackBuyButton.transform.localScale = Vector3.one;
        this.m_packBuyButtonContainer.AddSlice(storePackBuyButton.gameObject);
        this.m_packBuyButtons.Add(storePackBuyButton);
      }
      this.m_packBuyButtonContainer.UpdateSlices();
    }
    return this.m_packBuyButtons[buttonIndex];
  }

  private List<Network.Bundle> GetPackBundles(bool sortByPackQuantity)
  {
    List<Network.Bundle> bundlesForProduct;
    if (StoreManager.IsFirstPurchaseBundleBooster(this.m_selectedBoosterId))
    {
      bundlesForProduct = StoreManager.Get().GetAllBundlesForProduct(ProductType.PRODUCT_TYPE_HIDDEN_LICENSE, GeneralStorePacksContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, 1, 0);
    }
    else
    {
      bundlesForProduct = StoreManager.Get().GetAllBundlesForProduct(ProductType.PRODUCT_TYPE_BOOSTER, GeneralStorePacksContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, this.m_selectedBoosterId, 0);
      bundlesForProduct.RemoveAll((Predicate<Network.Bundle>) (obj => obj.Items.Find((Predicate<Network.BundleItem>) (item => item.Product == ProductType.PRODUCT_TYPE_HIDDEN_LICENSE)) != null));
    }
    if (sortByPackQuantity)
      bundlesForProduct.Sort((Comparison<Network.Bundle>) ((left, right) => (left != null ? left.Items.Where<Network.BundleItem>((Func<Network.BundleItem, bool>) (i => i.Product == ProductType.PRODUCT_TYPE_BOOSTER)).Max<Network.BundleItem>((Func<Network.BundleItem, int>) (i => i.Quantity)) : 0) - (right != null ? right.Items.Where<Network.BundleItem>((Func<Network.BundleItem, bool>) (i => i.Product == ProductType.PRODUCT_TYPE_BOOSTER)).Max<Network.BundleItem>((Func<Network.BundleItem, int>) (i => i.Quantity)) : 0)));
    return bundlesForProduct;
  }

  private void AnimateLogo(bool animateLogo)
  {
    if (!this.m_hasLogo || !this.gameObject.activeInHierarchy || this.m_selectedBoosterId == 0)
      return;
    MeshRenderer currentLogo = this.GetCurrentLogo();
    currentLogo.gameObject.SetActive(true);
    switch (this.m_logoAnimation)
    {
      case GeneralStorePacksContent.LogoAnimation.Slam:
        if (animateLogo)
        {
          this.m_logoAnimCoroutine = this.StartCoroutine(this.AnimateSlamLogo(currentLogo));
          break;
        }
        if (this.m_animatingLogo)
          break;
        currentLogo.transform.localPosition = this.m_logoAnimationEndBone.transform.localPosition;
        currentLogo.gameObject.SetActive(true);
        break;
      case GeneralStorePacksContent.LogoAnimation.Fade:
        if (animateLogo)
        {
          this.m_logoAnimCoroutine = this.StartCoroutine(this.AnimateFadeLogo(currentLogo));
          break;
        }
        if (this.m_animatingLogo)
          break;
        currentLogo.gameObject.SetActive(false);
        break;
    }
  }

  private void AnimatePacksFlying(int numVisiblePacks, bool forceImmediate = false, float delay = 0.0f)
  {
    if (!this.gameObject.activeInHierarchy)
      return;
    GeneralStorePacksContentDisplay currentDisplay = this.GetCurrentDisplay();
    if (this.m_packAnimCoroutine != null)
      this.StopCoroutine(this.m_packAnimCoroutine);
    if (this.m_preorderCardBackAnimCoroutine != null)
      this.StopCoroutine(this.m_preorderCardBackAnimCoroutine);
    this.m_packAnimCoroutine = !StoreManager.IsFirstPurchaseBundleBooster(this.m_selectedBoosterId) || !StoreManager.IsFirstPurchaseBundleOwned() ? (!StoreManager.IsFirstPurchaseBundleBooster(this.m_selectedBoosterId) ? this.StartCoroutine(this.AnimatePacks(currentDisplay, numVisiblePacks, forceImmediate)) : this.StartCoroutine(this.AnimateBundleBox(currentDisplay, delay, forceImmediate))) : this.StartCoroutine(this.AnimateBundleBox(currentDisplay, delay, true));
    this.m_preorderCardBackAnimCoroutine = this.StartCoroutine(this.AnimatePreorderUI(currentDisplay));
  }

  [DebuggerHidden]
  private IEnumerator AnimateFadeLogo(MeshRenderer logo)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStorePacksContent.\u003CAnimateFadeLogo\u003Ec__Iterator26A() { logo = logo, \u003C\u0024\u003Elogo = logo, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator AnimateSlamLogo(MeshRenderer logo)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStorePacksContent.\u003CAnimateSlamLogo\u003Ec__Iterator26B() { logo = logo, \u003C\u0024\u003Elogo = logo, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator AnimatePacks(GeneralStorePacksContentDisplay display, int numVisiblePacks, bool forceImmediate)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStorePacksContent.\u003CAnimatePacks\u003Ec__Iterator26C() { display = display, numVisiblePacks = numVisiblePacks, forceImmediate = forceImmediate, \u003C\u0024\u003Edisplay = display, \u003C\u0024\u003EnumVisiblePacks = numVisiblePacks, \u003C\u0024\u003EforceImmediate = forceImmediate, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator AnimateBundleBox(GeneralStorePacksContentDisplay display, float delayAnim, bool forceImmediate)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStorePacksContent.\u003CAnimateBundleBox\u003Ec__Iterator26D() { delayAnim = delayAnim, display = display, forceImmediate = forceImmediate, \u003C\u0024\u003EdelayAnim = delayAnim, \u003C\u0024\u003Edisplay = display, \u003C\u0024\u003EforceImmediate = forceImmediate, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator AnimatePreorderUI(GeneralStorePacksContentDisplay display)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStorePacksContent.\u003CAnimatePreorderUI\u003Ec__Iterator26E() { \u003C\u003Ef__this = this };
  }

  private void ResetAnimations()
  {
    if ((UnityEngine.Object) this.m_preorderCardBackReward != (UnityEngine.Object) null)
      this.m_preorderCardBackReward.HideCardBackReward();
    if ((UnityEngine.Object) this.m_availableDateText != (UnityEngine.Object) null)
      this.m_availableDateText.gameObject.SetActive(false);
    if (this.m_logoAnimCoroutine != null)
    {
      iTween.Stop(this.m_logoMesh1.gameObject);
      iTween.Stop(this.m_logoMesh2.gameObject);
      this.StopCoroutine(this.m_logoAnimCoroutine);
    }
    this.m_logoMesh1.gameObject.SetActive(false);
    this.m_logoMesh2.gameObject.SetActive(false);
    if (this.m_packAnimCoroutine != null)
      this.StopCoroutine(this.m_packAnimCoroutine);
    if (this.m_preorderCardBackAnimCoroutine != null)
      this.StopCoroutine(this.m_preorderCardBackAnimCoroutine);
    this.m_animatingLogo = false;
    this.m_animatingPacks = false;
    this.m_waitingForBoxAnim = false;
  }

  private void AnimateAndUpdateDisplay(int id, bool forceImmediate = false)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStorePacksContent.\u003CAnimateAndUpdateDisplay\u003Ec__AnonStorey3F5 displayCAnonStorey3F5 = new GeneralStorePacksContent.\u003CAnimateAndUpdateDisplay\u003Ec__AnonStorey3F5();
    if ((UnityEngine.Object) this.m_preorderCardBackReward != (UnityEngine.Object) null)
      this.m_preorderCardBackReward.HideCardBackReward();
    // ISSUE: reference to a compiler-generated field
    displayCAnonStorey3F5.currDisplay = (GameObject) null;
    if (this.m_currentDisplay == -1)
    {
      this.m_currentDisplay = 1;
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3F5.currDisplay = this.m_packEmptyDisplay;
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3F5.currDisplay = this.GetCurrentDisplayContainer();
    }
    GameObject displayContainer = this.GetNextDisplayContainer();
    this.GetCurrentLogo().gameObject.SetActive(false);
    this.GetCurrentDisplay().ClearPacks();
    this.m_currentDisplay = (this.m_currentDisplay + 1) % 2;
    displayContainer.SetActive(true);
    if (!forceImmediate)
    {
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3F5.currDisplay.transform.localRotation = Quaternion.identity;
      displayContainer.transform.localEulerAngles = new Vector3(180f, 0.0f, 0.0f);
      // ISSUE: reference to a compiler-generated field
      iTween.StopByName(displayCAnonStorey3F5.currDisplay, "ROTATION_TWEEN");
      iTween.StopByName(displayContainer, "ROTATION_TWEEN");
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      iTween.RotateBy(displayCAnonStorey3F5.currDisplay, iTween.Hash((object) "amount", (object) new Vector3(0.5f, 0.0f, 0.0f), (object) "time", (object) 0.5f, (object) "name", (object) "ROTATION_TWEEN", (object) "oncomplete", (object) new Action<object>(displayCAnonStorey3F5.\u003C\u003Em__1D2)));
      iTween.RotateBy(displayContainer, iTween.Hash((object) "amount", (object) new Vector3(0.5f, 0.0f, 0.0f), (object) "time", (object) 0.5f, (object) "name", (object) "ROTATION_TWEEN"));
      if (!string.IsNullOrEmpty(this.m_backgroundFlipSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_backgroundFlipSound));
    }
    else
    {
      displayContainer.transform.localRotation = Quaternion.identity;
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3F5.currDisplay.transform.localEulerAngles = new Vector3(180f, 0.0f, 0.0f);
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3F5.currDisplay.SetActive(false);
    }
    StorePackDef storePackDef = this.GetStorePackDef(id);
    this.GetCurrentDisplay().UpdatePackType(storePackDef);
    // ISSUE: reference to a compiler-generated field
    displayCAnonStorey3F5.currLogo = this.GetCurrentLogo();
    // ISSUE: reference to a compiler-generated field
    if ((UnityEngine.Object) displayCAnonStorey3F5.currLogo != (UnityEngine.Object) null)
    {
      this.m_hasLogo = !string.IsNullOrEmpty(storePackDef.m_logoTextureName);
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3F5.currLogo.gameObject.SetActive(this.m_hasLogo);
      if (this.m_hasLogo)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GeneralStorePacksContent.\u003CAnimateAndUpdateDisplay\u003Ec__AnonStorey3F6 displayCAnonStorey3F6 = new GeneralStorePacksContent.\u003CAnimateAndUpdateDisplay\u003Ec__AnonStorey3F6();
        // ISSUE: reference to a compiler-generated field
        displayCAnonStorey3F6.\u003C\u003Ef__ref\u00241013 = displayCAnonStorey3F5;
        // ISSUE: reference to a compiler-generated method
        AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(storePackDef.m_logoTextureName), new AssetLoader.ObjectCallback(displayCAnonStorey3F6.\u003C\u003Em__1D3), (object) null, false);
        // ISSUE: reference to a compiler-generated field
        displayCAnonStorey3F6.glowLogo = this.GetCurrentGlowLogo();
        // ISSUE: reference to a compiler-generated field
        if ((UnityEngine.Object) displayCAnonStorey3F6.glowLogo != (UnityEngine.Object) null)
        {
          // ISSUE: reference to a compiler-generated method
          AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(storePackDef.m_logoTextureGlowName), new AssetLoader.ObjectCallback(displayCAnonStorey3F6.\u003C\u003Em__1D4), (object) null, false);
        }
      }
    }
    this.AnimateBuyBar();
  }

  private void AnimateBuyBar()
  {
    Network.Bundle preOrderBundle;
    GameObject target = !StoreManager.Get().IsBoosterPreorderActive(this.m_selectedBoosterId, out preOrderBundle) ? this.m_packBuyPreorderContainer : this.m_packBuyContainer;
    if (this.m_selectedBoosterId == 0)
      return;
    iTween.Stop(target);
    target.transform.localRotation = Quaternion.identity;
    iTween.RotateBy(target, iTween.Hash((object) "amount", (object) new Vector3(-1f, 0.0f, 0.0f), (object) "time", (object) this.m_backgroundFlipAnimTime, (object) "delay", (object) (1f / 1000f)));
  }

  private void UpdateChinaKoreaInfoButton()
  {
    if ((UnityEngine.Object) this.m_ChinaInfoButton == (UnityEngine.Object) null)
      return;
    this.m_ChinaInfoButton.gameObject.SetActive((StoreManager.Get().IsChinaCustomer() || StoreManager.Get().IsKoreanCustomer()) && this.IsContentActive() && this.m_selectedBoosterId != 0);
  }

  private void OnChinaKoreaInfoPressed(UIEvent e)
  {
    bool flag = StoreManager.Get().IsChinaCustomer();
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get(!flag ? "GLUE_STORE_KOREAN_DISCLAIMER_HEADLINE" : "GLUE_STORE_CHINA_DISCLAIMER_HEADLINE"),
      m_text = GameStrings.Get(!flag ? "GLUE_STORE_KOREAN_DISCLAIMER_DETAILS" : "GLUE_STORE_CHINA_DISCLAIMER_DETAILS"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  [Serializable]
  public class ToggleableButtonFrame
  {
    public GameObject m_Middle;
    public GameObject m_IBar;
  }

  public enum LogoAnimation
  {
    None,
    Slam,
    Fade,
  }
}
