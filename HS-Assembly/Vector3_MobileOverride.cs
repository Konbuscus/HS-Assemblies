﻿// Decompiled with JetBrains decompiler
// Type: Vector3_MobileOverride
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class Vector3_MobileOverride : MobileOverrideValue<Vector3>
{
  public Vector3_MobileOverride()
  {
  }

  public Vector3_MobileOverride(Vector3 defaultValue)
    : base(defaultValue)
  {
  }
}
