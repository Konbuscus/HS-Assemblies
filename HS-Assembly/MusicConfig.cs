﻿// Decompiled with JetBrains decompiler
// Type: MusicConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class MusicConfig : MonoBehaviour
{
  [CustomEditField(Sections = "Playlists")]
  public List<MusicPlaylist> m_playlists = new List<MusicPlaylist>();
  private static MusicConfig s_instance;

  private void Awake()
  {
    MusicConfig.s_instance = this;
  }

  private void OnDestroy()
  {
    MusicConfig.s_instance = (MusicConfig) null;
  }

  public static MusicConfig Get()
  {
    return MusicConfig.s_instance;
  }

  public MusicPlaylist GetPlaylist(MusicPlaylistType type)
  {
    return this.FindPlaylist(type) ?? new MusicPlaylist();
  }

  public MusicPlaylist FindPlaylist(MusicPlaylistType type)
  {
    for (int index = 0; index < this.m_playlists.Count; ++index)
    {
      MusicPlaylist playlist = this.m_playlists[index];
      if (playlist.m_type == type)
        return playlist;
    }
    return (MusicPlaylist) null;
  }
}
