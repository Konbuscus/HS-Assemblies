﻿// Decompiled with JetBrains decompiler
// Type: PlayAnimator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlayAnimator : MonoBehaviour
{
  public GameObject m_Target1;
  public string m_Target1State;
  public GameObject m_Target2;
  public string m_Target2State;
  public GameObject m_Target3;
  public string m_Target3State;

  public void PlayAnimator1()
  {
    if ((Object) this.m_Target1 == (Object) null)
      return;
    this.m_Target1.GetComponent<Animator>().enabled = true;
    this.m_Target1.GetComponent<Animator>().Play(this.m_Target1State, -1, 0.0f);
  }

  public void PlayAnimator2()
  {
    if ((Object) this.m_Target1 == (Object) null)
      return;
    this.m_Target2.GetComponent<Animator>().enabled = true;
    this.m_Target2.GetComponent<Animator>().Play(this.m_Target2State, -1, 0.0f);
  }

  public void PlayAnimator3()
  {
    if ((Object) this.m_Target1 == (Object) null)
      return;
    this.m_Target3.GetComponent<Animator>().enabled = true;
    this.m_Target3.GetComponent<Animator>().Play(this.m_Target3State, -1, 0.0f);
  }
}
