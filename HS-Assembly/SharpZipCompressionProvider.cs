﻿// Decompiled with JetBrains decompiler
// Type: SharpZipCompressionProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using ICSharpCode.SharpZipLib.Zip.Compression;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using System.IO;

public class SharpZipCompressionProvider : ICompressionProvider
{
  public Stream GetDeflateStream(Stream baseOutputStream)
  {
    return (Stream) new DeflaterOutputStream(baseOutputStream, new Deflater());
  }

  public Stream GetInflateStream(Stream baseInputStream)
  {
    return (Stream) new InflaterInputStream(baseInputStream, new Inflater(false));
  }
}
