﻿// Decompiled with JetBrains decompiler
// Type: DeckRule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public abstract class DeckRule
{
  protected int m_id;
  protected int m_deckRulesetId;
  protected int m_appliesToSubsetId;
  protected HashSet<string> m_appliesToSubset;
  protected bool m_appliesToIsNot;
  protected DeckRule.RuleType m_ruleType;
  protected bool m_ruleIsNot;
  protected int m_minValue;
  protected int m_maxValue;
  protected int m_tag;
  protected int m_tagMinValue;
  protected int m_tagMaxValue;
  protected string m_stringValue;
  protected string m_errorString;
  protected List<HashSet<string>> m_subsets;

  public DeckRule.RuleType Type
  {
    get
    {
      return this.m_ruleType;
    }
  }

  public bool RuleIsNot
  {
    get
    {
      return this.m_ruleIsNot;
    }
  }

  public bool HasAppliesToSubset
  {
    get
    {
      return this.m_appliesToSubsetId != 0;
    }
  }

  public DeckRule()
  {
    this.m_ruleType = DeckRule.RuleType.UNKNOWN;
  }

  public DeckRule(DeckRule.RuleType ruleType, DeckRulesetRuleDbfRecord record)
  {
    this.m_ruleType = ruleType;
    this.m_id = record.ID;
    this.m_deckRulesetId = record.DeckRulesetId;
    this.m_appliesToSubsetId = record.AppliesToSubsetId;
    this.m_appliesToIsNot = record.AppliesToIsNot;
    this.m_ruleIsNot = record.RuleIsNot;
    this.m_minValue = record.MinValue;
    this.m_maxValue = record.MaxValue;
    this.m_tag = record.Tag;
    this.m_tagMinValue = record.TagMinValue;
    this.m_tagMaxValue = record.TagMaxValue;
    this.m_stringValue = record.StringValue;
    this.m_errorString = record.ErrorString == null ? string.Empty : record.ErrorString.GetString(true);
    this.m_subsets = new List<HashSet<string>>();
    if (this.m_appliesToSubsetId != 0)
      this.m_appliesToSubset = GameDbf.GetIndex().GetSubsetById(this.m_appliesToSubsetId);
    this.m_subsets = GameDbf.GetIndex().GetSubsetsForRule(this.m_id);
  }

  public static DeckRule CreateFromDBF(DeckRulesetRuleDbfRecord record)
  {
    return DeckRule.GetRule(record);
  }

  public static DeckRule GetRule(DeckRulesetRuleDbfRecord record)
  {
    string ruleType = record.RuleType;
    if (ruleType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckRule.\u003C\u003Ef__switch\u0024mapB6 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckRule.\u003C\u003Ef__switch\u0024mapB6 = new Dictionary<string, int>(15)
        {
          {
            "is_class_or_neutral_card",
            0
          },
          {
            "is_in_any_subset",
            1
          },
          {
            "count_cards_in_deck",
            2
          },
          {
            "has_tag_value",
            3
          },
          {
            "count_copies_of_each_card",
            4
          },
          {
            "player_owns_each_copy",
            5
          },
          {
            "deck_size",
            6
          },
          {
            "is_not_rotated",
            7
          },
          {
            "has_odd_numbered_tag_value",
            8
          },
          {
            "count_cards_with_tag_value",
            8
          },
          {
            "count_cards_with_tag_value_odd_numbered",
            8
          },
          {
            "count_cards_with_same_tag_value",
            8
          },
          {
            "count_unique_tag_values",
            8
          },
          {
            "is_in_all_subsets",
            8
          },
          {
            "card_text_contains_substring",
            8
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckRule.\u003C\u003Ef__switch\u0024mapB6.TryGetValue(ruleType, out num))
      {
        switch (num)
        {
          case 0:
            return (DeckRule) new DeckRule_IsClassCardOrNeutral(record);
          case 1:
            return (DeckRule) new DeckRule_IsInAnySubset(record);
          case 2:
            return (DeckRule) new DeckRule_CountCardsInDeck(record);
          case 3:
            return (DeckRule) new DeckRule_HasTagValue(record);
          case 4:
            return (DeckRule) new DeckRule_CountCopiesOfEachCard(record);
          case 5:
            return (DeckRule) new DeckRule_PlayerOwnsEachCopy(record);
          case 6:
            return (DeckRule) new DeckRule_DeckSize(record);
          case 7:
            return (DeckRule) new DeckRule_IsNotRotated(record);
        }
      }
    }
    return (DeckRule) new DeckRule_DefaultType(record.RuleType, record);
  }

  public virtual bool Filter(EntityDef def)
  {
    return true;
  }

  public virtual bool CanAddToDeck(EntityDef def, TAG_PREMIUM premium, CollectionDeck deck, out RuleInvalidReason reason)
  {
    return this.DefaultYes(out reason);
  }

  public virtual bool CanRemoveFromDeck(EntityDef def, CollectionDeck deck)
  {
    return true;
  }

  public abstract bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason);

  public override string ToString()
  {
    return string.Format("{0}, id:{1}, deckruleset:{2}", (object) this.m_ruleType, (object) this.m_id, (object) this.m_deckRulesetId);
  }

  protected bool GetResult(bool val)
  {
    return val == !this.m_ruleIsNot;
  }

  protected bool AppliesTo(string cardId)
  {
    if (this.m_appliesToSubset == null)
      return true;
    return this.m_appliesToSubset.Contains(cardId) == !this.m_appliesToIsNot;
  }

  protected bool DefaultYes(out RuleInvalidReason reason)
  {
    reason = (RuleInvalidReason) null;
    return true;
  }

  public enum RuleType
  {
    IS_IN_ANY_SUBSET,
    IS_NOT_ROTATED,
    COUNT_COPIES_OF_EACH_CARD,
    PLAYER_OWNS_EACH_COPY,
    IS_CLASS_CARD_OR_NEUTRAL,
    COUNT_CARDS_IN_DECK,
    HAS_TAG_VALUE,
    DECK_SIZE,
    UNKNOWN,
  }
}
