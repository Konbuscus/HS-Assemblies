﻿// Decompiled with JetBrains decompiler
// Type: Spell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using HutongGames.PlayMaker;
using HutongGames.PlayMaker.Actions;
using PegasusGame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using UnityEngine;

public class Spell : MonoBehaviour
{
  public SpellLocation m_Location = SpellLocation.SOURCE_AUTO;
  private List<Spell.FinishedListener> m_finishedListeners = new List<Spell.FinishedListener>();
  private List<Spell.StateFinishedListener> m_stateFinishedListeners = new List<Spell.StateFinishedListener>();
  private List<Spell.StateStartedListener> m_stateStartedListeners = new List<Spell.StateStartedListener>();
  private List<Spell.SpellEventListener> m_spellEventListeners = new List<Spell.SpellEventListener>();
  protected List<GameObject> m_targets = new List<GameObject>();
  protected bool m_shown = true;
  protected bool m_positionDirty = true;
  protected bool m_orientationDirty = true;
  public bool m_BlockServerEvents;
  public GameObject m_ObjectContainer;
  public string m_LocationTransformName;
  public bool m_SetParentToLocation;
  public SpellFacing m_Facing;
  public SpellFacingOptions m_FacingOptions;
  public TARGET_RETICLE_TYPE m_TargetReticle;
  public List<SpellZoneTag> m_ZonesToDisable;
  public float m_ZoneLayoutDelayForDeaths;
  public bool m_UseFastActorTriggers;
  protected SpellType m_spellType;
  private Map<SpellStateType, List<SpellState>> m_spellStateMap;
  protected SpellStateType m_activeStateType;
  protected SpellStateType m_activeStateChange;
  protected GameObject m_source;
  protected PowerTaskList m_taskList;
  private PlayMakerFSM m_fsm;
  private Map<SpellStateType, FsmState> m_fsmStateMap;
  private bool m_fsmSkippedFirstFrame;
  private bool m_fsmReady;
  protected bool m_finished;

  protected virtual void Awake()
  {
    this.BuildSpellStateMap();
    this.m_fsm = this.GetComponent<PlayMakerFSM>();
    if (string.IsNullOrEmpty(this.m_LocationTransformName))
      return;
    this.m_LocationTransformName = this.m_LocationTransformName.Trim();
  }

  protected virtual void OnDestroy()
  {
    if ((UnityEngine.Object) this.gameObject != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    if (!((UnityEngine.Object) this.m_ObjectContainer != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_ObjectContainer);
    this.m_ObjectContainer = (GameObject) null;
  }

  protected virtual void Start()
  {
    if (this.m_activeStateType == SpellStateType.NONE)
      this.ActivateObjectContainer(false);
    else if (this.m_shown)
      this.ShowImpl();
    else
      this.HideImpl();
  }

  private void Update()
  {
    if (this.m_fsmReady)
      return;
    if ((UnityEngine.Object) this.m_fsm == (UnityEngine.Object) null)
      this.m_fsmReady = true;
    else if (!this.m_fsmSkippedFirstFrame)
    {
      this.m_fsmSkippedFirstFrame = true;
    }
    else
    {
      if (!this.m_fsm.enabled)
        return;
      this.BuildFsmStateMap();
      this.m_fsmReady = true;
    }
  }

  public SpellType GetSpellType()
  {
    return this.m_spellType;
  }

  public void SetSpellType(SpellType spellType)
  {
    this.m_spellType = spellType;
  }

  public bool DoesBlockServerEvents()
  {
    if (GameState.Get() == null)
      return false;
    return this.m_BlockServerEvents;
  }

  public SuperSpell GetSuperSpellParent()
  {
    if ((UnityEngine.Object) this.transform.parent == (UnityEngine.Object) null)
      return (SuperSpell) null;
    return this.transform.parent.GetComponent<SuperSpell>();
  }

  public PowerTaskList GetPowerTaskList()
  {
    return this.m_taskList;
  }

  public Entity GetPowerSource()
  {
    if (this.m_taskList == null)
      return (Entity) null;
    return this.m_taskList.GetSourceEntity();
  }

  public Card GetPowerSourceCard()
  {
    Entity powerSource = this.GetPowerSource();
    if (powerSource == null)
      return (Card) null;
    return powerSource.GetCard();
  }

  public Entity GetPowerTarget()
  {
    if (this.m_taskList == null)
      return (Entity) null;
    return this.m_taskList.GetTargetEntity();
  }

  public Card GetPowerTargetCard()
  {
    Entity powerTarget = this.GetPowerTarget();
    if (powerTarget == null)
      return (Card) null;
    return powerTarget.GetCard();
  }

  public virtual bool CanPurge()
  {
    return !this.IsActive();
  }

  public SpellLocation GetLocation()
  {
    return this.m_Location;
  }

  public string GetLocationTransformName()
  {
    return this.m_LocationTransformName;
  }

  public SpellFacing GetFacing()
  {
    return this.m_Facing;
  }

  public SpellFacingOptions GetFacingOptions()
  {
    return this.m_FacingOptions;
  }

  public void SetPosition(Vector3 position)
  {
    this.transform.position = position;
    this.m_positionDirty = false;
  }

  public void SetLocalPosition(Vector3 position)
  {
    this.transform.localPosition = position;
    this.m_positionDirty = false;
  }

  public void SetOrientation(Quaternion orientation)
  {
    this.transform.rotation = orientation;
    this.m_orientationDirty = false;
  }

  public void SetLocalOrientation(Quaternion orientation)
  {
    this.transform.localRotation = orientation;
    this.m_orientationDirty = false;
  }

  public void UpdateTransform()
  {
    this.UpdatePosition();
    this.UpdateOrientation();
  }

  public void UpdatePosition()
  {
    if (!this.m_positionDirty)
      return;
    SpellUtils.SetPositionFromLocation(this, this.m_SetParentToLocation);
    this.m_positionDirty = false;
  }

  public void UpdateOrientation()
  {
    if (!this.m_orientationDirty)
      return;
    SpellUtils.SetOrientationFromFacing(this);
    this.m_orientationDirty = false;
  }

  public GameObject GetSource()
  {
    return this.m_source;
  }

  public virtual void SetSource(GameObject go)
  {
    this.m_source = go;
  }

  public virtual void RemoveSource()
  {
    this.m_source = (GameObject) null;
  }

  public bool IsSource(GameObject go)
  {
    return (UnityEngine.Object) this.m_source == (UnityEngine.Object) go;
  }

  public Card GetSourceCard()
  {
    if ((UnityEngine.Object) this.m_source == (UnityEngine.Object) null)
      return (Card) null;
    return this.m_source.GetComponent<Card>();
  }

  public List<GameObject> GetTargets()
  {
    return this.m_targets;
  }

  public GameObject GetTarget()
  {
    if (this.m_targets.Count == 0)
      return (GameObject) null;
    return this.m_targets[0];
  }

  public virtual void AddTarget(GameObject go)
  {
    this.m_targets.Add(go);
  }

  public virtual void AddTargets(List<GameObject> targets)
  {
    this.m_targets.AddRange((IEnumerable<GameObject>) targets);
  }

  public virtual bool RemoveTarget(GameObject go)
  {
    return this.m_targets.Remove(go);
  }

  public virtual void RemoveAllTargets()
  {
    this.m_targets.Clear();
  }

  public bool IsTarget(GameObject go)
  {
    return this.m_targets.Contains(go);
  }

  public Card GetTargetCard()
  {
    GameObject target = this.GetTarget();
    if ((UnityEngine.Object) target == (UnityEngine.Object) null)
      return (Card) null;
    return target.GetComponent<Card>();
  }

  public virtual List<GameObject> GetVisualTargets()
  {
    return this.GetTargets();
  }

  public virtual GameObject GetVisualTarget()
  {
    return this.GetTarget();
  }

  public virtual void AddVisualTarget(GameObject go)
  {
    this.AddTarget(go);
  }

  public virtual void AddVisualTargets(List<GameObject> targets)
  {
    this.AddTargets(targets);
  }

  public virtual bool RemoveVisualTarget(GameObject go)
  {
    return this.RemoveTarget(go);
  }

  public virtual void RemoveAllVisualTargets()
  {
    this.RemoveAllTargets();
  }

  public virtual bool IsVisualTarget(GameObject go)
  {
    return this.IsTarget(go);
  }

  public virtual Card GetVisualTargetCard()
  {
    return this.GetTargetCard();
  }

  public bool IsValidSpellTarget(Entity ent)
  {
    return !ent.IsEnchantment();
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  public void Show()
  {
    if (this.m_shown)
      return;
    this.m_shown = true;
    if (this.m_activeStateType != SpellStateType.NONE)
      this.OnExitedNoneState();
    this.ShowImpl();
  }

  public void Hide()
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    this.HideImpl();
    if (this.m_activeStateType == SpellStateType.NONE)
      return;
    this.OnEnteredNoneState();
  }

  public void ActivateObjectContainer(bool enable)
  {
    if ((UnityEngine.Object) this.m_ObjectContainer == (UnityEngine.Object) null)
      return;
    SceneUtils.EnableRenderers(this.m_ObjectContainer, enable);
  }

  public bool IsActive()
  {
    return this.m_activeStateType != SpellStateType.NONE;
  }

  public void Activate()
  {
    SpellStateType stateType = this.GuessNextStateType();
    if (stateType == SpellStateType.NONE)
      this.Deactivate();
    else
      this.ChangeState(stateType);
  }

  public void Reactivate()
  {
    SpellStateType stateType = this.GuessNextStateType(SpellStateType.NONE);
    if (stateType == SpellStateType.NONE)
      this.Deactivate();
    else
      this.ChangeState(stateType);
  }

  public void Deactivate()
  {
    if (this.m_activeStateType == SpellStateType.NONE)
      return;
    this.ForceDeactivate();
  }

  public void ForceDeactivate()
  {
    this.ChangeState(SpellStateType.NONE);
  }

  public void ActivateState(SpellStateType stateType)
  {
    if (!this.HasUsableState(stateType))
      this.Deactivate();
    else
      this.ChangeState(stateType);
  }

  public void SafeActivateState(SpellStateType stateType)
  {
    if (!this.HasUsableState(stateType))
      this.ForceDeactivate();
    else
      this.ChangeState(stateType);
  }

  public virtual bool HasUsableState(SpellStateType stateType)
  {
    return stateType != SpellStateType.NONE && (this.HasStateContent(stateType) || this.HasOverriddenStateMethod(stateType) || this.m_activeStateType == SpellStateType.NONE && this.m_ZonesToDisable != null && this.m_ZonesToDisable.Count > 0);
  }

  public SpellStateType GetActiveState()
  {
    return this.m_activeStateType;
  }

  public SpellState GetFirstSpellState(SpellStateType stateType)
  {
    if (this.m_spellStateMap == null)
      return (SpellState) null;
    List<SpellState> spellStateList = (List<SpellState>) null;
    if (!this.m_spellStateMap.TryGetValue(stateType, out spellStateList))
      return (SpellState) null;
    if (spellStateList.Count == 0)
      return (SpellState) null;
    return spellStateList[0];
  }

  public List<SpellState> GetActiveStateList()
  {
    if (this.m_spellStateMap == null)
      return (List<SpellState>) null;
    List<SpellState> spellStateList = (List<SpellState>) null;
    if (!this.m_spellStateMap.TryGetValue(this.m_activeStateType, out spellStateList))
      return (List<SpellState>) null;
    return spellStateList;
  }

  public bool IsFinished()
  {
    return this.m_finished;
  }

  public void AddFinishedCallback(Spell.FinishedCallback callback)
  {
    this.AddFinishedCallback(callback, (object) null);
  }

  public void AddFinishedCallback(Spell.FinishedCallback callback, object userData)
  {
    Spell.FinishedListener finishedListener = new Spell.FinishedListener();
    finishedListener.SetCallback(callback);
    finishedListener.SetUserData(userData);
    if (this.m_finishedListeners.Contains(finishedListener))
      return;
    this.m_finishedListeners.Add(finishedListener);
  }

  public bool RemoveFinishedCallback(Spell.FinishedCallback callback)
  {
    return this.RemoveFinishedCallback(callback, (object) null);
  }

  public bool RemoveFinishedCallback(Spell.FinishedCallback callback, object userData)
  {
    Spell.FinishedListener finishedListener = new Spell.FinishedListener();
    finishedListener.SetCallback(callback);
    finishedListener.SetUserData(userData);
    return this.m_finishedListeners.Remove(finishedListener);
  }

  public void AddStateFinishedCallback(Spell.StateFinishedCallback callback)
  {
    this.AddStateFinishedCallback(callback, (object) null);
  }

  public void AddStateFinishedCallback(Spell.StateFinishedCallback callback, object userData)
  {
    Spell.StateFinishedListener finishedListener = new Spell.StateFinishedListener();
    finishedListener.SetCallback(callback);
    finishedListener.SetUserData(userData);
    if (this.m_stateFinishedListeners.Contains(finishedListener))
      return;
    this.m_stateFinishedListeners.Add(finishedListener);
  }

  public bool RemoveStateFinishedCallback(Spell.StateFinishedCallback callback)
  {
    return this.RemoveStateFinishedCallback(callback, (object) null);
  }

  public bool RemoveStateFinishedCallback(Spell.StateFinishedCallback callback, object userData)
  {
    Spell.StateFinishedListener finishedListener = new Spell.StateFinishedListener();
    finishedListener.SetCallback(callback);
    finishedListener.SetUserData(userData);
    return this.m_stateFinishedListeners.Remove(finishedListener);
  }

  public void AddStateStartedCallback(Spell.StateStartedCallback callback)
  {
    this.AddStateStartedCallback(callback, (object) null);
  }

  public void AddStateStartedCallback(Spell.StateStartedCallback callback, object userData)
  {
    Spell.StateStartedListener stateStartedListener = new Spell.StateStartedListener();
    stateStartedListener.SetCallback(callback);
    stateStartedListener.SetUserData(userData);
    if (this.m_stateStartedListeners.Contains(stateStartedListener))
      return;
    this.m_stateStartedListeners.Add(stateStartedListener);
  }

  public bool RemoveStateStartedCallback(Spell.StateStartedCallback callback)
  {
    return this.RemoveStateStartedCallback(callback, (object) null);
  }

  public bool RemoveStateStartedCallback(Spell.StateStartedCallback callback, object userData)
  {
    Spell.StateStartedListener stateStartedListener = new Spell.StateStartedListener();
    stateStartedListener.SetCallback(callback);
    stateStartedListener.SetUserData(userData);
    return this.m_stateStartedListeners.Remove(stateStartedListener);
  }

  public void AddSpellEventCallback(Spell.SpellEventCallback callback)
  {
    this.AddSpellEventCallback(callback, (object) null);
  }

  public void AddSpellEventCallback(Spell.SpellEventCallback callback, object userData)
  {
    Spell.SpellEventListener spellEventListener = new Spell.SpellEventListener();
    spellEventListener.SetCallback(callback);
    spellEventListener.SetUserData(userData);
    if (this.m_spellEventListeners.Contains(spellEventListener))
      return;
    this.m_spellEventListeners.Add(spellEventListener);
  }

  public bool RemoveSpellEventCallback(Spell.SpellEventCallback callback)
  {
    return this.RemoveSpellEventCallback(callback, (object) null);
  }

  public bool RemoveSpellEventCallback(Spell.SpellEventCallback callback, object userData)
  {
    Spell.SpellEventListener spellEventListener = new Spell.SpellEventListener();
    spellEventListener.SetCallback(callback);
    spellEventListener.SetUserData(userData);
    return this.m_spellEventListeners.Remove(spellEventListener);
  }

  public virtual void ChangeState(SpellStateType stateType)
  {
    this.ChangeStateImpl(stateType);
    if (this.m_activeStateType != stateType)
      return;
    this.ChangeFsmState(stateType);
  }

  public SpellStateType GuessNextStateType()
  {
    return this.GuessNextStateType(this.m_activeStateType);
  }

  public SpellStateType GuessNextStateType(SpellStateType stateType)
  {
    switch (stateType)
    {
      case SpellStateType.NONE:
        if (this.HasUsableState(SpellStateType.BIRTH))
          return SpellStateType.BIRTH;
        if (this.HasUsableState(SpellStateType.IDLE))
          return SpellStateType.IDLE;
        if (this.HasUsableState(SpellStateType.ACTION))
          return SpellStateType.ACTION;
        if (this.HasUsableState(SpellStateType.DEATH))
          return SpellStateType.DEATH;
        if (this.HasUsableState(SpellStateType.CANCEL))
          return SpellStateType.CANCEL;
        break;
      case SpellStateType.BIRTH:
        if (this.HasUsableState(SpellStateType.IDLE))
          return SpellStateType.IDLE;
        break;
      case SpellStateType.IDLE:
        if (this.HasUsableState(SpellStateType.ACTION))
          return SpellStateType.ACTION;
        break;
      case SpellStateType.ACTION:
        if (this.HasUsableState(SpellStateType.DEATH))
          return SpellStateType.DEATH;
        break;
    }
    return SpellStateType.NONE;
  }

  public bool AttachPowerTaskList(PowerTaskList taskList)
  {
    this.m_taskList = taskList;
    this.RemoveAllTargets();
    if (!this.AddPowerTargets())
      return false;
    this.OnAttachPowerTaskList();
    return true;
  }

  public virtual bool AddPowerTargets()
  {
    if (!this.CanAddPowerTargets())
      return false;
    return this.AddMultiplePowerTargets();
  }

  public PowerTaskList GetLastHandledTaskList(PowerTaskList taskList)
  {
    if (taskList == null)
      return (PowerTaskList) null;
    Spell spell = UnityEngine.Object.Instantiate<Spell>(this);
    PowerTaskList powerTaskList1 = (PowerTaskList) null;
    for (PowerTaskList powerTaskList2 = taskList.GetLast(); powerTaskList2 != null; powerTaskList2 = powerTaskList2.GetPrevious())
    {
      spell.m_taskList = powerTaskList2;
      spell.RemoveAllTargets();
      if (spell.AddPowerTargets())
      {
        powerTaskList1 = powerTaskList2;
        break;
      }
    }
    UnityEngine.Object.Destroy((UnityEngine.Object) spell);
    return powerTaskList1;
  }

  public bool IsHandlingLastTaskList()
  {
    return this.GetLastHandledTaskList(this.m_taskList) == this.m_taskList;
  }

  public virtual void OnStateFinished()
  {
    this.ChangeState(this.GuessNextStateType());
  }

  public virtual void OnSpellFinished()
  {
    this.m_finished = true;
    this.m_positionDirty = true;
    this.m_orientationDirty = true;
    if (this.DoesBlockServerEvents())
      GameState.Get().RemoveServerBlockingSpell(this);
    this.BlockZones(false);
    if (this.m_UseFastActorTriggers && GameState.Get() != null && this.IsHandlingLastTaskList())
      GameState.Get().SetUsingFastActorTriggers(false);
    this.FireFinishedCallbacks();
  }

  public virtual void OnSpellEvent(string eventName, object eventData)
  {
    this.FireSpellEventCallbacks(eventName, eventData);
  }

  public virtual void OnFsmStateStarted(FsmState state, SpellStateType stateType)
  {
    if (this.m_activeStateChange == stateType)
      return;
    this.ChangeStateImpl(stateType);
  }

  protected virtual void OnAttachPowerTaskList()
  {
    if (!this.m_UseFastActorTriggers || !this.m_taskList.IsStartOfBlock())
      return;
    GameState.Get().SetUsingFastActorTriggers(true);
  }

  protected virtual void OnBirth(SpellStateType prevStateType)
  {
    this.UpdateTransform();
    this.FireStateStartedCallbacks(prevStateType);
  }

  protected virtual void OnIdle(SpellStateType prevStateType)
  {
    this.FireStateStartedCallbacks(prevStateType);
  }

  protected virtual void OnAction(SpellStateType prevStateType)
  {
    this.UpdateTransform();
    this.FireStateStartedCallbacks(prevStateType);
  }

  protected virtual void OnCancel(SpellStateType prevStateType)
  {
    this.FireStateStartedCallbacks(prevStateType);
  }

  protected virtual void OnDeath(SpellStateType prevStateType)
  {
    this.FireStateStartedCallbacks(prevStateType);
  }

  protected virtual void OnNone(SpellStateType prevStateType)
  {
    this.FireStateStartedCallbacks(prevStateType);
  }

  private void BuildSpellStateMap()
  {
    IEnumerator enumerator = this.transform.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
      {
        SpellState component = ((Component) enumerator.Current).gameObject.GetComponent<SpellState>();
        if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        {
          SpellStateType stateType = component.m_StateType;
          if (stateType != SpellStateType.NONE)
          {
            if (this.m_spellStateMap == null)
              this.m_spellStateMap = new Map<SpellStateType, List<SpellState>>();
            List<SpellState> spellStateList;
            if (!this.m_spellStateMap.TryGetValue(stateType, out spellStateList))
            {
              spellStateList = new List<SpellState>();
              this.m_spellStateMap.Add(stateType, spellStateList);
            }
            spellStateList.Add(component);
          }
        }
      }
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
  }

  private void BuildFsmStateMap()
  {
    if ((UnityEngine.Object) this.m_fsm == (UnityEngine.Object) null)
      return;
    List<FsmState> spellFsmStateList = this.GenerateSpellFsmStateList();
    if (spellFsmStateList.Count > 0)
      this.m_fsmStateMap = new Map<SpellStateType, FsmState>();
    Map<SpellStateType, int> map1 = new Map<SpellStateType, int>();
    foreach (int num in Enum.GetValues(typeof (SpellStateType)))
    {
      SpellStateType index = (SpellStateType) num;
      map1[index] = 0;
    }
    Map<SpellStateType, int> map2 = new Map<SpellStateType, int>();
    foreach (int num in Enum.GetValues(typeof (SpellStateType)))
    {
      SpellStateType index = (SpellStateType) num;
      map2[index] = 0;
    }
    foreach (FsmTransition globalTransition in this.m_fsm.FsmGlobalTransitions)
    {
      SpellStateType key;
      try
      {
        key = EnumUtils.GetEnum<SpellStateType>(globalTransition.EventName);
      }
      catch (ArgumentException ex)
      {
        continue;
      }
      Map<SpellStateType, int> map3;
      SpellStateType index1;
      (map3 = map2)[index1 = key] = map3[index1] + 1;
      using (List<FsmState>.Enumerator enumerator = spellFsmStateList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          FsmState current = enumerator.Current;
          if (globalTransition.ToState.Equals(current.Name))
          {
            Map<SpellStateType, int> map4;
            SpellStateType index2;
            (map4 = map1)[index2 = key] = map4[index2] + 1;
            if (!this.m_fsmStateMap.ContainsKey(key))
              this.m_fsmStateMap.Add(key, current);
          }
        }
      }
    }
    using (Map<SpellStateType, int>.Enumerator enumerator = map1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<SpellStateType, int> current = enumerator.Current;
        if (current.Value > 1)
          UnityEngine.Debug.LogWarning((object) string.Format("{0}.BuildFsmStateMap() - Found {1} states for SpellStateType {2}. There should be 1.", (object) this, (object) current.Value, (object) current.Key));
      }
    }
    using (Map<SpellStateType, int>.Enumerator enumerator = map2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<SpellStateType, int> current = enumerator.Current;
        if (current.Value > 1)
          UnityEngine.Debug.LogWarning((object) string.Format("{0}.BuildFsmStateMap() - Found {1} transitions for SpellStateType {2}. There should be 1.", (object) this, (object) current.Value, (object) current.Key));
        if (current.Value > 0 && map1[current.Key] == 0)
          UnityEngine.Debug.LogWarning((object) string.Format("{0}.BuildFsmStateMap() - SpellStateType {1} is missing a SpellStateAction.", (object) this, (object) current.Key));
      }
    }
    if (this.m_fsmStateMap == null || this.m_fsmStateMap.Values.Count != 0)
      return;
    this.m_fsmStateMap = (Map<SpellStateType, FsmState>) null;
  }

  private List<FsmState> GenerateSpellFsmStateList()
  {
    List<FsmState> fsmStateList = new List<FsmState>();
    foreach (FsmState fsmState in this.m_fsm.FsmStates)
    {
      SpellStateAction spellStateAction = (SpellStateAction) null;
      int num = 0;
      for (int index = 0; index < fsmState.Actions.Length; ++index)
      {
        SpellStateAction action = fsmState.Actions[index] as SpellStateAction;
        if (action != null)
        {
          ++num;
          if (spellStateAction == null)
            spellStateAction = action;
        }
      }
      if (spellStateAction != null)
        fsmStateList.Add(fsmState);
      if (num > 1)
        UnityEngine.Debug.LogWarning((object) string.Format("{0}.GenerateSpellFsmStateList() - State \"{1}\" has {2} SpellStateActions. There should be 1.", (object) this, (object) fsmState.Name, (object) num));
    }
    return fsmStateList;
  }

  protected void ChangeStateImpl(SpellStateType stateType)
  {
    this.m_activeStateChange = stateType;
    SpellStateType activeStateType = this.m_activeStateType;
    this.m_activeStateType = stateType;
    if (stateType == SpellStateType.NONE)
      this.FinishIfNecessary();
    List<SpellState> nextStateList = (List<SpellState>) null;
    if (this.m_spellStateMap != null)
      this.m_spellStateMap.TryGetValue(stateType, out nextStateList);
    if (activeStateType != SpellStateType.NONE)
    {
      List<SpellState> spellStateList;
      if (this.m_spellStateMap != null && this.m_spellStateMap.TryGetValue(activeStateType, out spellStateList))
      {
        using (List<SpellState>.Enumerator enumerator = spellStateList.GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.Stop(nextStateList);
        }
      }
      this.FireStateFinishedCallbacks(activeStateType);
    }
    else if (stateType != SpellStateType.NONE)
    {
      this.m_finished = false;
      this.OnExitedNoneState();
    }
    if (nextStateList != null)
    {
      using (List<SpellState>.Enumerator enumerator = nextStateList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Play();
      }
    }
    this.CallStateFunction(activeStateType, stateType);
    if (activeStateType == SpellStateType.NONE || stateType != SpellStateType.NONE)
      return;
    this.OnEnteredNoneState();
  }

  protected void ChangeFsmState(SpellStateType stateType)
  {
    if ((UnityEngine.Object) this.m_fsm == (UnityEngine.Object) null)
      return;
    this.StartCoroutine(this.WaitThenChangeFsmState(stateType));
  }

  [DebuggerHidden]
  private IEnumerator WaitThenChangeFsmState(SpellStateType stateType)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Spell.\u003CWaitThenChangeFsmState\u003Ec__Iterator2C() { stateType = stateType, \u003C\u0024\u003EstateType = stateType, \u003C\u003Ef__this = this };
  }

  private void ChangeFsmStateNow(SpellStateType stateType)
  {
    if (this.m_fsmStateMap == null)
    {
      UnityEngine.Debug.LogError((object) string.Format("Spell.ChangeFsmStateNow() - stateType {0} was requested but the m_fsmStateMap is null", (object) stateType));
    }
    else
    {
      FsmState fsmState = (FsmState) null;
      if (!this.m_fsmStateMap.TryGetValue(stateType, out fsmState))
        return;
      this.m_fsm.SendEvent(EnumUtils.GetString<SpellStateType>(stateType));
    }
  }

  protected void FinishIfNecessary()
  {
    if (this.m_finished)
      return;
    this.OnSpellFinished();
  }

  protected void CallStateFunction(SpellStateType prevStateType, SpellStateType stateType)
  {
    switch (stateType)
    {
      case SpellStateType.BIRTH:
        this.OnBirth(prevStateType);
        break;
      case SpellStateType.IDLE:
        this.OnIdle(prevStateType);
        break;
      case SpellStateType.ACTION:
        this.OnAction(prevStateType);
        break;
      case SpellStateType.CANCEL:
        this.OnCancel(prevStateType);
        break;
      case SpellStateType.DEATH:
        this.OnDeath(prevStateType);
        break;
      default:
        this.OnNone(prevStateType);
        break;
    }
  }

  protected void FireFinishedCallbacks()
  {
    Spell.FinishedListener[] array = this.m_finishedListeners.ToArray();
    this.m_finishedListeners.Clear();
    foreach (Spell.FinishedListener finishedListener in array)
      finishedListener.Fire(this);
  }

  protected void FireStateFinishedCallbacks(SpellStateType prevStateType)
  {
    Spell.StateFinishedListener[] array = this.m_stateFinishedListeners.ToArray();
    if (this.m_activeStateType == SpellStateType.NONE)
      this.m_stateFinishedListeners.Clear();
    foreach (Spell.StateFinishedListener finishedListener in array)
      finishedListener.Fire(this, prevStateType);
  }

  protected void FireStateStartedCallbacks(SpellStateType prevStateType)
  {
    Spell.StateStartedListener[] array = this.m_stateStartedListeners.ToArray();
    if (this.m_activeStateType == SpellStateType.NONE)
      this.m_stateStartedListeners.Clear();
    foreach (Spell.StateStartedListener stateStartedListener in array)
      stateStartedListener.Fire(this, prevStateType);
  }

  protected void FireSpellEventCallbacks(string eventName, object eventData)
  {
    foreach (Spell.SpellEventListener spellEventListener in this.m_spellEventListeners.ToArray())
      spellEventListener.Fire(eventName, eventData);
  }

  protected bool HasStateContent(SpellStateType stateType)
  {
    if (this.m_spellStateMap != null && this.m_spellStateMap.ContainsKey(stateType))
      return true;
    if (!this.m_fsmReady)
    {
      if ((UnityEngine.Object) this.m_fsm != (UnityEngine.Object) null && this.m_fsm.Fsm.HasEvent(EnumUtils.GetString<SpellStateType>(stateType)))
        return true;
    }
    else if (this.m_fsmStateMap != null && this.m_fsmStateMap.ContainsKey(stateType))
      return true;
    return false;
  }

  protected bool HasOverriddenStateMethod(SpellStateType stateType)
  {
    string stateMethodName = this.GetStateMethodName(stateType);
    if (stateMethodName == null)
      return false;
    System.Type type = ((object) this).GetType();
    MethodInfo method = typeof (Spell).GetMethod(stateMethodName, BindingFlags.Instance | BindingFlags.NonPublic);
    return GeneralUtils.IsOverriddenMethod(type.GetMethod(stateMethodName, BindingFlags.Instance | BindingFlags.NonPublic), method);
  }

  protected string GetStateMethodName(SpellStateType stateType)
  {
    switch (stateType)
    {
      case SpellStateType.BIRTH:
        return "OnBirth";
      case SpellStateType.IDLE:
        return "OnIdle";
      case SpellStateType.ACTION:
        return "OnAction";
      case SpellStateType.CANCEL:
        return "OnCancel";
      case SpellStateType.DEATH:
        return "OnDeath";
      default:
        return (string) null;
    }
  }

  protected bool CanAddPowerTargets()
  {
    return SpellUtils.CanAddPowerTargets(this.m_taskList);
  }

  protected bool AddSinglePowerTarget()
  {
    Card sourceCard = this.GetSourceCard();
    if ((UnityEngine.Object) sourceCard == (UnityEngine.Object) null)
    {
      Log.Power.PrintWarning("{0}.AddSinglePowerTarget() - a source card was never added", (object) this);
      return false;
    }
    Network.HistBlockStart blockStart = this.m_taskList.GetBlockStart();
    if (blockStart == null)
    {
      Log.Power.PrintError("{0}.AddSinglePowerTarget() - got a task list with no block start", (object) this);
      return false;
    }
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    return this.AddSinglePowerTarget_FromBlockStart(blockStart) || this.AddSinglePowerTarget_FromMetaData(taskList) || this.AddSinglePowerTarget_FromAnyPower(sourceCard, taskList);
  }

  protected bool AddSinglePowerTarget_FromBlockStart(Network.HistBlockStart blockStart)
  {
    Entity entity = GameState.Get().GetEntity(blockStart.Target);
    if (entity == null)
      return false;
    Card card = entity.GetCard();
    if ((UnityEngine.Object) card == (UnityEngine.Object) null)
    {
      Log.Power.Print("{0}.AddSinglePowerTarget_FromSourceAction() - FAILED Target {1} in blockStart has no Card", new object[2]
      {
        (object) this,
        (object) blockStart.Target
      });
      return false;
    }
    this.AddTarget(card.gameObject);
    return true;
  }

  protected bool AddSinglePowerTarget_FromMetaData(List<PowerTask> tasks)
  {
    GameState gameState = GameState.Get();
    for (int metaDataIndex = 0; metaDataIndex < tasks.Count; ++metaDataIndex)
    {
      Network.PowerHistory power = tasks[metaDataIndex].GetPower();
      if (power.Type == Network.PowerType.META_DATA)
      {
        Network.HistMetaData histMetaData = (Network.HistMetaData) power;
        if (histMetaData.MetaType == HistoryMeta.Type.TARGET)
        {
          if (histMetaData.Info == null || histMetaData.Info.Count == 0)
          {
            UnityEngine.Debug.LogError((object) string.Format("{0}.AddSinglePowerTarget_FromMetaData() - META_DATA at index {1} has no Info", (object) this, (object) metaDataIndex));
          }
          else
          {
            for (int index = 0; index < histMetaData.Info.Count; ++index)
            {
              Entity entity = gameState.GetEntity(histMetaData.Info[index]);
              if (entity == null)
              {
                UnityEngine.Debug.LogError((object) string.Format("{0}.AddSinglePowerTarget_FromMetaData() - Entity is null for META_DATA at index {1} Info index {2}", (object) this, (object) metaDataIndex, (object) index));
              }
              else
              {
                Card card = entity.GetCard();
                this.AddTargetFromMetaData(metaDataIndex, card);
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }

  protected bool AddSinglePowerTarget_FromAnyPower(Card sourceCard, List<PowerTask> tasks)
  {
    for (int index = 0; index < tasks.Count; ++index)
    {
      PowerTask task = tasks[index];
      Card cardFromPowerTask = this.GetTargetCardFromPowerTask(index, task);
      if (!((UnityEngine.Object) cardFromPowerTask == (UnityEngine.Object) null) && !((UnityEngine.Object) sourceCard == (UnityEngine.Object) cardFromPowerTask) && this.IsValidSpellTarget(cardFromPowerTask.GetEntity()))
      {
        this.AddTarget(cardFromPowerTask.gameObject);
        return true;
      }
    }
    return false;
  }

  protected bool AddMultiplePowerTargets()
  {
    Card sourceCard = this.GetSourceCard();
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    if (this.AddMultiplePowerTargets_FromMetaData(taskList))
      return true;
    this.AddMultiplePowerTargets_FromAnyPower(sourceCard, taskList);
    return true;
  }

  protected bool AddMultiplePowerTargets_FromMetaData(List<PowerTask> tasks)
  {
    int count = this.m_targets.Count;
    GameState gameState = GameState.Get();
    for (int metaDataIndex = 0; metaDataIndex < tasks.Count; ++metaDataIndex)
    {
      Network.PowerHistory power = tasks[metaDataIndex].GetPower();
      if (power.Type == Network.PowerType.META_DATA)
      {
        Network.HistMetaData histMetaData = (Network.HistMetaData) power;
        if (histMetaData.MetaType == HistoryMeta.Type.TARGET)
        {
          if (histMetaData.Info == null || histMetaData.Info.Count == 0)
          {
            UnityEngine.Debug.LogError((object) string.Format("{0}.AddMultiplePowerTargets_FromMetaData() - META_DATA at index {1} has no Info", (object) this, (object) metaDataIndex));
          }
          else
          {
            for (int index = 0; index < histMetaData.Info.Count; ++index)
            {
              Entity entity = gameState.GetEntity(histMetaData.Info[index]);
              if (entity == null)
              {
                UnityEngine.Debug.LogError((object) string.Format("{0}.AddMultiplePowerTargets_FromMetaData() - Entity is null for META_DATA at index {1} Info index {2}", (object) this, (object) metaDataIndex, (object) index));
              }
              else
              {
                Card card = entity.GetCard();
                this.AddTargetFromMetaData(metaDataIndex, card);
              }
            }
          }
        }
      }
    }
    return this.m_targets.Count != count;
  }

  protected void AddMultiplePowerTargets_FromAnyPower(Card sourceCard, List<PowerTask> tasks)
  {
    for (int index = 0; index < tasks.Count; ++index)
    {
      PowerTask task = tasks[index];
      Card cardFromPowerTask = this.GetTargetCardFromPowerTask(index, task);
      if (!((UnityEngine.Object) cardFromPowerTask == (UnityEngine.Object) null) && !((UnityEngine.Object) sourceCard == (UnityEngine.Object) cardFromPowerTask) && (!this.IsTarget(cardFromPowerTask.gameObject) && this.IsValidSpellTarget(cardFromPowerTask.GetEntity())))
        this.AddTarget(cardFromPowerTask.gameObject);
    }
  }

  protected virtual Card GetTargetCardFromPowerTask(int index, PowerTask task)
  {
    Network.PowerHistory power = task.GetPower();
    if (power.Type != Network.PowerType.TAG_CHANGE)
      return (Card) null;
    Network.HistTagChange histTagChange = power as Network.HistTagChange;
    Entity entity = GameState.Get().GetEntity(histTagChange.Entity);
    if (entity != null)
      return entity.GetCard();
    UnityEngine.Debug.LogWarning((object) string.Format("{0}.GetTargetCardFromPowerTask() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) histTagChange.Entity));
    return (Card) null;
  }

  protected virtual void AddTargetFromMetaData(int metaDataIndex, Card targetCard)
  {
    this.AddTarget(targetCard.gameObject);
  }

  protected bool CompleteMetaDataTasks(int metaDataIndex)
  {
    return this.CompleteMetaDataTasks(metaDataIndex, (PowerTaskList.CompleteCallback) null, (object) null);
  }

  protected bool CompleteMetaDataTasks(int metaDataIndex, PowerTaskList.CompleteCallback completeCallback)
  {
    return this.CompleteMetaDataTasks(metaDataIndex, completeCallback, (object) null);
  }

  protected bool CompleteMetaDataTasks(int metaDataIndex, PowerTaskList.CompleteCallback completeCallback, object callbackData)
  {
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    int count = 1;
    for (int index = metaDataIndex + 1; index < taskList.Count; ++index)
    {
      Network.PowerHistory power = taskList[index].GetPower();
      if (power.Type != Network.PowerType.META_DATA || ((Network.HistMetaData) power).MetaType != HistoryMeta.Type.TARGET)
        ++count;
      else
        break;
    }
    if (count == 0)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0}.CompleteMetaDataTasks() - there are no tasks to complete for meta data {1}", (object) this, (object) metaDataIndex));
      return false;
    }
    this.m_taskList.DoTasks(metaDataIndex, count, completeCallback, callbackData);
    return true;
  }

  protected virtual void ShowImpl()
  {
    List<SpellState> activeStateList = this.GetActiveStateList();
    if (activeStateList == null)
      return;
    using (List<SpellState>.Enumerator enumerator = activeStateList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.ShowState();
    }
  }

  protected virtual void HideImpl()
  {
    List<SpellState> activeStateList = this.GetActiveStateList();
    if (activeStateList == null)
      return;
    using (List<SpellState>.Enumerator enumerator = activeStateList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.HideState();
    }
  }

  protected void OnExitedNoneState()
  {
    if (this.DoesBlockServerEvents())
      GameState.Get().AddServerBlockingSpell(this);
    this.ActivateObjectContainer(true);
    this.BlockZones(true);
    if (!((UnityEngine.Object) ZoneMgr.Get() != (UnityEngine.Object) null))
      return;
    ZoneMgr.Get().RequestNextDeathBlockLayoutDelaySec(this.m_ZoneLayoutDelayForDeaths);
  }

  protected void OnEnteredNoneState()
  {
    if (this.DoesBlockServerEvents())
      GameState.Get().RemoveServerBlockingSpell(this);
    this.ActivateObjectContainer(false);
  }

  protected void BlockZones(bool block)
  {
    if (this.m_ZonesToDisable == null)
      return;
    using (List<SpellZoneTag>.Enumerator enumerator1 = this.m_ZonesToDisable.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        List<Zone> zonesFromTag = SpellUtils.FindZonesFromTag(enumerator1.Current);
        if (zonesFromTag != null)
        {
          using (List<Zone>.Enumerator enumerator2 = zonesFromTag.GetEnumerator())
          {
            while (enumerator2.MoveNext())
              enumerator2.Current.BlockInput(block);
          }
        }
      }
    }
  }

  public void OnLoad()
  {
    IEnumerator enumerator = this.transform.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
      {
        SpellState component = ((Component) enumerator.Current).gameObject.GetComponent<SpellState>();
        if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
          component.OnLoad();
      }
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
  }

  private class FinishedListener : EventListener<Spell.FinishedCallback>
  {
    public void Fire(Spell spell)
    {
      this.m_callback(spell, this.m_userData);
    }
  }

  private class StateFinishedListener : EventListener<Spell.StateFinishedCallback>
  {
    public void Fire(Spell spell, SpellStateType prevStateType)
    {
      this.m_callback(spell, prevStateType, this.m_userData);
    }
  }

  private class StateStartedListener : EventListener<Spell.StateStartedCallback>
  {
    public void Fire(Spell spell, SpellStateType prevStateType)
    {
      this.m_callback(spell, prevStateType, this.m_userData);
    }
  }

  private class SpellEventListener : EventListener<Spell.SpellEventCallback>
  {
    public void Fire(string eventName, object eventData)
    {
      this.m_callback(eventName, eventData, this.m_userData);
    }
  }

  public delegate void FinishedCallback(Spell spell, object userData);

  public delegate void StateFinishedCallback(Spell spell, SpellStateType prevStateType, object userData);

  public delegate void StateStartedCallback(Spell spell, SpellStateType prevStateType, object userData);

  public delegate void SpellEventCallback(string eventName, object eventData, object userData);
}
