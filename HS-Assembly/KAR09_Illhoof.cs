﻿// Decompiled with JetBrains decompiler
// Type: KAR09_Illhoof
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR09_Illhoof : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Illhoof_Male_Demon_IllhoofSummonImps_01");
    this.PreloadSound("VO_Illhoof_Male_Demon_IllhoofSummoningPortal_01");
    this.PreloadSound("VO_Illhoof_Male_Demon_IllhoofEmoteResponse_01");
    this.PreloadSound("VO_Illhoof_Male_Demon_IllhoofWounded_01");
    this.PreloadSound("VO_Illhoof_Male_Demon_IllhoofTurn1_01");
    this.PreloadSound("VO_Illhoof_Male_Demon_IlhoofKilrek_01");
    this.PreloadSound("VO_Moroes_Male_Human_IllhoofKilrekResponse_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_IllhoofKilrekResponse_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_IllhoofSenseDemons_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_IllhoofWin_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_IllhoofTurn1_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_IllhoofTurn5_01");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Illhoof_Male_Demon_IllhoofEmoteResponse_01",
            m_stringTag = "VO_Illhoof_Male_Demon_IllhoofEmoteResponse_01"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR09_Illhoof.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1AC() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR09_Illhoof.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1AD() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR09_Illhoof.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1AE() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR09_Illhoof.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator1AF() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR09_Illhoof.\u003CHandleGameOverWithTiming\u003Ec__Iterator1B0() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
