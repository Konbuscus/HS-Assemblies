﻿// Decompiled with JetBrains decompiler
// Type: AdventureClassChallengeChestButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class AdventureClassChallengeChestButton : PegUIElement
{
  public GameObject m_RootObject;
  public Transform m_UpBone;
  public Transform m_DownBone;
  public GameObject m_HighlightPlane;
  public GameObject m_RewardBone;
  public GameObject m_RewardCard;
  public bool m_IsRewardLoading;

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    SoundManager.Get().LoadAndPlay("collection_manager_hero_mouse_over", this.gameObject);
    this.ShowHighlight(true);
    this.StartCoroutine(this.ShowRewardCard());
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.ShowHighlight(false);
    this.HideRewardCard();
  }

  public void Press()
  {
    SoundManager.Get().LoadAndPlay("collection_manager_hero_mouse_over", this.gameObject);
    this.Depress();
    this.ShowHighlight(true);
    this.StartCoroutine(this.ShowRewardCard());
  }

  public void Release()
  {
    this.Raise();
    this.ShowHighlight(false);
    this.HideRewardCard();
  }

  private void Raise()
  {
    iTween.MoveTo(this.m_RootObject, iTween.Hash((object) "position", (object) this.m_UpBone.localPosition, (object) "time", (object) 0.1f, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true));
  }

  private void Depress()
  {
    iTween.MoveTo(this.m_RootObject, iTween.Hash((object) "position", (object) this.m_DownBone.localPosition, (object) "time", (object) 0.1f, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true));
  }

  private void ShowHighlight(bool show)
  {
    this.m_HighlightPlane.GetComponent<Renderer>().enabled = show;
  }

  [DebuggerHidden]
  private IEnumerator ShowRewardCard()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureClassChallengeChestButton.\u003CShowRewardCard\u003Ec__Iterator2()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void HideRewardCard()
  {
    iTween.ScaleTo(this.m_RewardBone, new Vector3(0.1f, 0.1f, 0.1f), 0.2f);
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, new FullScreenFXMgr.EffectListener(this.EffectFadeOutFinished));
  }

  private void EffectFadeOutFinished()
  {
    SceneUtils.SetLayer(this.gameObject, GameLayer.Default);
    if (!((Object) this.m_RewardCard != (Object) null))
      return;
    this.m_RewardCard.SetActive(false);
  }
}
