﻿// Decompiled with JetBrains decompiler
// Type: LOE08_Archaedas
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LOE08_Archaedas : LOE_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOE_08_RESPONSE");
    this.PreloadSound("VO_LOEA08_TURN_1_BRANN");
    this.PreloadSound("VO_LOE_ARCHAEDAS_TURN_1_CARTOGRAPHER");
    this.PreloadSound("VO_LOE_08_LANDSLIDE");
    this.PreloadSound("VO_LOE_08_ANIMATE_STONE");
    this.PreloadSound("VO_LOE_08_WIN");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_LOE_08_RESPONSE",
            m_stringTag = "VO_LOE_08_RESPONSE"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE08_Archaedas.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator16C() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE08_Archaedas.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator16D() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE08_Archaedas.\u003CHandleGameOverWithTiming\u003Ec__Iterator16E() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
