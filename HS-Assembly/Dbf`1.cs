﻿// Decompiled with JetBrains decompiler
// Type: Dbf`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class Dbf<T> : IDbf where T : DbfRecord, new()
{
  private List<T> m_records = new List<T>();
  private Map<int, T> m_recordsById = new Map<int, T>();
  private string m_name;
  private Dbf<T>.RecordAddedListener m_recordAddedListener;
  private Dbf<T>.RecordsRemovedListener m_recordsRemovedListener;

  private Dbf(string name)
  {
    this.m_name = name;
  }

  private Dbf(string name, Dbf<T>.RecordAddedListener addedListener, Dbf<T>.RecordsRemovedListener removedListener)
    : this(name)
  {
    this.m_recordAddedListener = addedListener;
    this.m_recordsRemovedListener = removedListener;
  }

  public DbfRecord CreateNewRecord()
  {
    return (DbfRecord) Activator.CreateInstance<T>();
  }

  public void AddRecord(DbfRecord record)
  {
    T record1 = (T) record;
    this.m_records.Add(record1);
    this.m_recordsById[record.ID] = record1;
    if (this.m_recordAddedListener == null)
      return;
    this.m_recordAddedListener(record1);
  }

  public System.Type GetRecordType()
  {
    return typeof (T);
  }

  public List<T> GetRecords()
  {
    return this.m_records;
  }

  public List<T> GetRecords(Predicate<T> predicate)
  {
    return this.m_records.FindAll(predicate);
  }

  public static Dbf<T> Load(string name, DbfFormat format)
  {
    string assetPath = format != DbfFormat.XML ? Dbf<T>.GetAssetPath(name) : Dbf<T>.GetXmlPath(name);
    return Dbf<T>.Load(name, assetPath, (Dbf<T>.RecordAddedListener) null, (Dbf<T>.RecordsRemovedListener) null, format);
  }

  public static Dbf<T> Load(string name, string assetPath, DbfFormat format)
  {
    return Dbf<T>.Load(name, assetPath, (Dbf<T>.RecordAddedListener) null, (Dbf<T>.RecordsRemovedListener) null, format);
  }

  public static Dbf<T> Load(string name, Dbf<T>.RecordAddedListener addedListener, Dbf<T>.RecordsRemovedListener removedListener, DbfFormat format)
  {
    string assetPath = format != DbfFormat.XML ? Dbf<T>.GetAssetPath(name) : Dbf<T>.GetXmlPath(name);
    return Dbf<T>.Load(name, assetPath, addedListener, removedListener, format);
  }

  public static Dbf<T> Load(string name, string assetPath, Dbf<T>.RecordAddedListener addedListener, Dbf<T>.RecordsRemovedListener removedListener, DbfFormat format)
  {
    float realtimeSinceStartup1 = Time.realtimeSinceStartup;
    Dbf<T> dbf = new Dbf<T>(name, addedListener, removedListener);
    dbf.Clear();
    bool flag = format == DbfFormat.XML;
    if (!(!flag ? dbf.LoadScriptableObject(assetPath) : DbfXml.Load(assetPath, (IDbf) dbf)))
    {
      dbf.Clear();
      Log.Dbf.Print(string.Format("Dbf.Load[{0}] - failed to load {1}", !flag ? (object) "ScriptableObject" : (object) "Xml", (object) name));
    }
    float realtimeSinceStartup2 = Time.realtimeSinceStartup;
    DbfShared.s_totalLoadTime += realtimeSinceStartup2 - realtimeSinceStartup1;
    Log.Dbf.Print("Loading format={0}, name={1} time={2} cumulativeTime={3}", (object) (!flag ? "ScriptableObject" : "Xml"), (object) name, (object) (float) ((double) realtimeSinceStartup2 - (double) realtimeSinceStartup1), (object) DbfShared.s_totalLoadTime);
    return dbf;
  }

  public string GetName()
  {
    return this.m_name;
  }

  public void Clear()
  {
    this.m_records.Clear();
    this.m_recordsById.Clear();
  }

  public T GetRecord(int id)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Dbf<T>.\u003CGetRecord\u003Ec__AnonStorey402 recordCAnonStorey402 = new Dbf<T>.\u003CGetRecord\u003Ec__AnonStorey402();
    // ISSUE: reference to a compiler-generated field
    recordCAnonStorey402.id = id;
    T obj;
    // ISSUE: reference to a compiler-generated field
    if (this.m_recordsById.TryGetValue(recordCAnonStorey402.id, out obj))
      return obj;
    // ISSUE: reference to a compiler-generated method
    return this.m_records.Find(new Predicate<T>(recordCAnonStorey402.\u003C\u003Em__204));
  }

  public T GetRecord(Predicate<T> match)
  {
    return this.m_records.Find(match);
  }

  public bool HasRecord(int id)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Dbf<T>.\u003CHasRecord\u003Ec__AnonStorey403 recordCAnonStorey403 = new Dbf<T>.\u003CHasRecord\u003Ec__AnonStorey403();
    // ISSUE: reference to a compiler-generated field
    recordCAnonStorey403.id = id;
    T obj = (T) null;
    // ISSUE: reference to a compiler-generated field
    if (!this.m_recordsById.TryGetValue(recordCAnonStorey403.id, out obj))
    {
      // ISSUE: reference to a compiler-generated method
      obj = this.m_records.Find(new Predicate<T>(recordCAnonStorey403.\u003C\u003Em__205));
    }
    return (object) obj != null;
  }

  public bool HasRecord(Predicate<T> match)
  {
    return (object) this.GetRecord(match) != null;
  }

  public void ReplaceRecordByRecordId(T record)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Dbf<T>.\u003CReplaceRecordByRecordId\u003Ec__AnonStorey404 idCAnonStorey404 = new Dbf<T>.\u003CReplaceRecordByRecordId\u003Ec__AnonStorey404();
    // ISSUE: reference to a compiler-generated field
    idCAnonStorey404.record = record;
    // ISSUE: reference to a compiler-generated method
    int index = this.m_records.FindIndex(new Predicate<T>(idCAnonStorey404.\u003C\u003Em__206));
    if (index == -1)
    {
      // ISSUE: reference to a compiler-generated field
      this.AddRecord((DbfRecord) idCAnonStorey404.record);
    }
    else
    {
      T record1 = this.m_records[index];
      // ISSUE: reference to a compiler-generated field
      bool flag = (object) record1 != (object) idCAnonStorey404.record;
      if (flag && this.m_recordsRemovedListener != null)
        this.m_recordsRemovedListener(new List<T>()
        {
          record1
        });
      // ISSUE: reference to a compiler-generated field
      this.m_records[index] = idCAnonStorey404.record;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      this.m_recordsById[idCAnonStorey404.record.ID] = idCAnonStorey404.record;
      if (!flag || this.m_recordAddedListener == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.m_recordAddedListener(idCAnonStorey404.record);
    }
  }

  public void RemoveRecordsWhere(Predicate<T> match)
  {
    List<int> intList = (List<int>) null;
    for (int index = 0; index < this.m_records.Count; ++index)
    {
      if (match(this.m_records[index]))
      {
        if (intList == null)
          intList = new List<int>();
        intList.Add(index);
      }
    }
    if (intList == null)
      return;
    List<T> removedRecords = (List<T>) null;
    if (this.m_recordsRemovedListener != null)
      removedRecords = new List<T>(intList.Count);
    for (int index1 = intList.Count - 1; index1 >= 0; --index1)
    {
      int index2 = intList[index1];
      T record = this.m_records[index2];
      if (removedRecords != null && (object) record != null)
        removedRecords.Add(record);
      T obj;
      if (this.m_recordsById.TryGetValue(record.ID, out obj))
        this.m_recordsById.Remove(obj.ID);
      this.m_records.RemoveAt(index2);
    }
    if (this.m_recordsRemovedListener == null)
      return;
    this.m_recordsRemovedListener(removedRecords);
  }

  public override string ToString()
  {
    return this.m_name;
  }

  private static string GetXmlPath(string name)
  {
    string subPath = string.Format("Assets/Game/DBF/{0}.xml", (object) name);
    string outPath;
    if (ApplicationMgr.TryGetStandaloneLocalDataPath(subPath, out outPath))
      return outPath;
    return subPath;
  }

  private static string GetAssetPath(string name)
  {
    return string.Format("Assets/Game/DBF-Asset/{0}.asset", (object) name);
  }

  public bool LoadScriptableObject(string resourcePath)
  {
    if (!Activator.CreateInstance<T>().LoadRecordsFromAsset<T>(resourcePath, out this.m_records))
      return false;
    for (int index = 0; index < this.m_records.Count; ++index)
    {
      this.m_recordsById[this.m_records[index].ID] = this.m_records[index];
      if (this.m_recordAddedListener != null)
        this.m_recordAddedListener(this.m_records[index]);
    }
    return true;
  }

  public string SaveScriptableObject()
  {
    string assetPath = Dbf<T>.GetAssetPath(this.m_name);
    if (Activator.CreateInstance<T>().SaveRecordsToAsset<T>(assetPath, this.m_records))
      return assetPath;
    return (string) null;
  }

  public delegate void RecordAddedListener(T record);

  public delegate void RecordsRemovedListener(List<T> removedRecords);
}
