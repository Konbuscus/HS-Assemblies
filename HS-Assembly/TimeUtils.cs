﻿// Decompiled with JetBrains decompiler
// Type: TimeUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class TimeUtils
{
  public static readonly DateTime EPOCH_TIME = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
  public static readonly TimeUtils.ElapsedStringSet SPLASHSCREEN_DATETIME_STRINGSET = new TimeUtils.ElapsedStringSet() { m_seconds = "GLOBAL_DATETIME_SPLASHSCREEN_SECONDS", m_minutes = "GLOBAL_DATETIME_SPLASHSCREEN_MINUTES", m_hours = "GLOBAL_DATETIME_SPLASHSCREEN_HOURS", m_yesterday = "GLOBAL_DATETIME_SPLASHSCREEN_DAY", m_days = "GLOBAL_DATETIME_SPLASHSCREEN_DAYS", m_weeks = "GLOBAL_DATETIME_SPLASHSCREEN_WEEKS", m_monthAgo = "GLOBAL_DATETIME_SPLASHSCREEN_MONTH" };
  public const int SEC_PER_MINUTE = 60;
  public const int SEC_PER_HOUR = 3600;
  public const int SEC_PER_DAY = 86400;
  public const int SEC_PER_WEEK = 604800;
  public const int MS_PER_SEC = 1000;
  public const int MS_PER_MINUTE = 60000;
  public const int MS_PER_HOUR = 3600000;
  public const string DEFAULT_TIME_UNITS_STR = "sec";

  public static long BinaryStamp()
  {
    return DateTime.UtcNow.ToBinary();
  }

  public static DateTime ConvertEpochMicrosecToDateTime(ulong microsec)
  {
    return TimeUtils.EPOCH_TIME.AddMilliseconds((double) microsec / 1000.0);
  }

  public static TimeSpan GetElapsedTimeSinceEpoch(DateTime? endDateTime = null)
  {
    return (!endDateTime.HasValue ? DateTime.UtcNow : endDateTime.Value) - TimeUtils.EPOCH_TIME;
  }

  public static string GetElapsedTimeStringFromEpochMicrosec(ulong microsec, TimeUtils.ElapsedStringSet stringSet)
  {
    return TimeUtils.GetElapsedTimeString((int) (DateTime.UtcNow - TimeUtils.ConvertEpochMicrosecToDateTime(microsec)).TotalSeconds, stringSet);
  }

  public static ulong DateTimeToUnixTimeStamp(DateTime time)
  {
    return (ulong) (time.ToUniversalTime() - TimeUtils.EPOCH_TIME).TotalSeconds;
  }

  public static DateTime UnixTimeStampToDateTime(ulong secondsSinceEpoch)
  {
    return TimeUtils.EPOCH_TIME.AddSeconds((double) secondsSinceEpoch);
  }

  public static long GetEpochTime()
  {
    return (long) (DateTime.UtcNow - TimeUtils.EPOCH_TIME).TotalSeconds;
  }

  public static string GetElapsedTimeString(long seconds, TimeUtils.ElapsedStringSet stringSet)
  {
    return TimeUtils.GetElapsedTimeString((int) seconds, stringSet);
  }

  public static string GetElapsedTimeString(int seconds, TimeUtils.ElapsedStringSet stringSet)
  {
    TimeUtils.ElapsedTimeType timeType;
    int time;
    TimeUtils.GetElapsedTime(seconds, out timeType, out time);
    return TimeUtils.GetElapsedTimeString(timeType, time, stringSet);
  }

  public static string GetElapsedTimeString(TimeUtils.ElapsedTimeType timeType, int time, TimeUtils.ElapsedStringSet stringSet)
  {
    switch (timeType)
    {
      case TimeUtils.ElapsedTimeType.SECONDS:
        return GameStrings.Format(stringSet.m_seconds, (object) time);
      case TimeUtils.ElapsedTimeType.MINUTES:
        return GameStrings.Format(stringSet.m_minutes, (object) time);
      case TimeUtils.ElapsedTimeType.HOURS:
        return GameStrings.Format(stringSet.m_hours, (object) time);
      case TimeUtils.ElapsedTimeType.YESTERDAY:
        if (stringSet.m_yesterday != null)
          return GameStrings.Get(stringSet.m_yesterday);
        return GameStrings.Format(stringSet.m_days, (object) 1);
      case TimeUtils.ElapsedTimeType.DAYS:
        return GameStrings.Format(stringSet.m_days, (object) time);
      case TimeUtils.ElapsedTimeType.WEEKS:
        return GameStrings.Format(stringSet.m_weeks, (object) time);
      default:
        return GameStrings.Get(stringSet.m_monthAgo);
    }
  }

  public static void GetElapsedTime(int seconds, out TimeUtils.ElapsedTimeType timeType, out int time)
  {
    time = 0;
    if (seconds < 60)
    {
      timeType = TimeUtils.ElapsedTimeType.SECONDS;
      time = seconds;
    }
    else if (seconds < 3600)
    {
      timeType = TimeUtils.ElapsedTimeType.MINUTES;
      time = seconds / 60;
    }
    else
    {
      int num1 = seconds / 86400;
      switch (num1)
      {
        case 0:
          timeType = TimeUtils.ElapsedTimeType.HOURS;
          time = seconds / 3600;
          break;
        case 1:
          timeType = TimeUtils.ElapsedTimeType.YESTERDAY;
          break;
        default:
          int num2 = seconds / 604800;
          if (num2 == 0)
          {
            timeType = TimeUtils.ElapsedTimeType.DAYS;
            time = num1;
            break;
          }
          if (num2 < 4)
          {
            timeType = TimeUtils.ElapsedTimeType.WEEKS;
            time = num2;
            break;
          }
          timeType = TimeUtils.ElapsedTimeType.MONTH_AGO;
          break;
      }
    }
  }

  public static string GetDevElapsedTimeString(TimeSpan span)
  {
    return TimeUtils.GetDevElapsedTimeString((long) span.TotalMilliseconds);
  }

  public static string GetDevElapsedTimeString(long ms)
  {
    StringBuilder builder = new StringBuilder();
    int unitCount = 0;
    if (ms >= 3600000L)
      TimeUtils.AppendDevTimeUnitsString("{0}h", 3600000, builder, ref ms, ref unitCount);
    if (ms >= 60000L)
      TimeUtils.AppendDevTimeUnitsString("{0}m", 60000, builder, ref ms, ref unitCount);
    if (ms >= 1000L)
      TimeUtils.AppendDevTimeUnitsString("{0}s", 1000, builder, ref ms, ref unitCount);
    if (unitCount <= 1)
    {
      if (unitCount > 0)
        builder.Append(' ');
      builder.AppendFormat("{0}ms", (object) ms);
    }
    return builder.ToString();
  }

  public static string GetDevElapsedTimeString(float sec)
  {
    StringBuilder builder = new StringBuilder();
    int unitCount = 0;
    if ((double) sec >= 3600.0)
      TimeUtils.AppendDevTimeUnitsString("{0}h", 3600f, builder, ref sec, ref unitCount);
    if ((double) sec >= 60.0)
      TimeUtils.AppendDevTimeUnitsString("{0}m", 60f, builder, ref sec, ref unitCount);
    if ((double) sec >= 1.0)
      TimeUtils.AppendDevTimeUnitsString("{0}s", 1f, builder, ref sec, ref unitCount);
    if (unitCount <= 1)
    {
      if (unitCount > 0)
        builder.Append(' ');
      float num = sec * 1000f;
      if ((double) num > 0.0)
        builder.AppendFormat("{0:f0}ms", (object) num);
      else
        builder.AppendFormat("{0}ms", (object) num);
    }
    return builder.ToString();
  }

  public static bool TryParseDevSecFromElapsedTimeString(string timeStr, out float sec)
  {
    sec = 0.0f;
    MatchCollection matchCollection = Regex.Matches(timeStr, "(?<number>(?:[0-9]+,)*[0-9]+)\\s*(?<units>[a-zA-Z]+)");
    if (matchCollection.Count == 0)
      return false;
    Match match = matchCollection[0];
    if (!match.Groups[0].Success)
      return false;
    Group group1 = match.Groups["number"];
    Group group2 = match.Groups["units"];
    if (!group1.Success || !group2.Success)
      return false;
    string s = group1.Value;
    string unitsStr = group2.Value;
    if (!float.TryParse(s, out sec))
      return false;
    string timeUnitsStr = TimeUtils.ParseTimeUnitsStr(unitsStr);
    if (timeUnitsStr == "min")
      sec = sec * 60f;
    else if (timeUnitsStr == "hour")
      sec = sec * 3600f;
    return true;
  }

  public static float ForceDevSecFromElapsedTimeString(string timeStr)
  {
    float sec;
    TimeUtils.TryParseDevSecFromElapsedTimeString(timeStr, out sec);
    return sec;
  }

  public static long PegDateToFileTimeUtc(Date date)
  {
    return new DateTime(date.Year, date.Month, date.Day, date.Hours, date.Min, date.Sec).ToFileTimeUtc();
  }

  public static Date FileTimeUtcToPegDate(long fileTimeUtc)
  {
    DateTime dateTime = DateTime.FromFileTimeUtc(fileTimeUtc);
    return new Date() { Year = dateTime.Year, Month = dateTime.Month, Day = dateTime.Day, Hours = dateTime.Hour, Min = dateTime.Minute, Sec = dateTime.Second };
  }

  private static void AppendDevTimeUnitsString(string formatString, int msPerUnit, StringBuilder builder, ref long ms, ref int unitCount)
  {
    long num = ms / (long) msPerUnit;
    if (num > 0L)
    {
      if (unitCount > 0)
        builder.Append(' ');
      builder.AppendFormat(formatString, (object) num);
      unitCount = unitCount + 1;
    }
    ms = ms - num * (long) msPerUnit;
  }

  private static void AppendDevTimeUnitsString(string formatString, float secPerUnit, StringBuilder builder, ref float sec, ref int unitCount)
  {
    float num = Mathf.Floor(sec / secPerUnit);
    if ((double) num > 0.0)
    {
      if (unitCount > 0)
        builder.Append(' ');
      builder.AppendFormat(formatString, (object) num);
      unitCount = unitCount + 1;
    }
    sec = sec - num * secPerUnit;
  }

  private static string ParseTimeUnitsStr(string unitsStr)
  {
    if (unitsStr == null)
      return "sec";
    unitsStr = unitsStr.ToLowerInvariant();
    string key = unitsStr;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (TimeUtils.\u003C\u003Ef__switch\u0024mapCE == null)
      {
        // ISSUE: reference to a compiler-generated field
        TimeUtils.\u003C\u003Ef__switch\u0024mapCE = new Dictionary<string, int>(13)
        {
          {
            "s",
            0
          },
          {
            "sec",
            0
          },
          {
            "secs",
            0
          },
          {
            "second",
            0
          },
          {
            "seconds",
            0
          },
          {
            "m",
            1
          },
          {
            "min",
            1
          },
          {
            "mins",
            1
          },
          {
            "minute",
            1
          },
          {
            "minutes",
            1
          },
          {
            "h",
            2
          },
          {
            "hour",
            2
          },
          {
            "hours",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (TimeUtils.\u003C\u003Ef__switch\u0024mapCE.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return "sec";
          case 1:
            return "min";
          case 2:
            return "hour";
        }
      }
    }
    return "sec";
  }

  public enum ElapsedTimeType
  {
    SECONDS,
    MINUTES,
    HOURS,
    YESTERDAY,
    DAYS,
    WEEKS,
    MONTH_AGO,
  }

  public class ElapsedStringSet
  {
    public string m_seconds;
    public string m_minutes;
    public string m_hours;
    public string m_yesterday;
    public string m_days;
    public string m_weeks;
    public string m_monthAgo;
  }
}
