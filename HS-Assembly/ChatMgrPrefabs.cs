﻿// Decompiled with JetBrains decompiler
// Type: ChatMgrPrefabs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class ChatMgrPrefabs
{
  public ChatBubbleFrame m_ChatBubbleOneLineFrame;
  public ChatBubbleFrame m_ChatBubbleSmallFrame;
  public FriendListFrame m_friendListFramePrefab;
}
