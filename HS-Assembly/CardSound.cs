﻿// Decompiled with JetBrains decompiler
// Type: CardSound
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardSound
{
  private string m_path;
  private AudioSource m_source;
  private Card m_owner;
  private bool m_alwaysValid;

  public CardSound(string path, Card owner, bool alwaysValid)
  {
    this.m_path = path;
    this.m_owner = owner;
    this.m_alwaysValid = alwaysValid;
  }

  public AudioSource GetSound(bool loadIfNeeded = true)
  {
    if ((Object) this.m_source == (Object) null && loadIfNeeded)
      this.LoadSound();
    return this.m_source;
  }

  public void Clear()
  {
    if ((Object) this.m_source == (Object) null)
      return;
    Object.Destroy((Object) this.m_source.gameObject);
  }

  private void LoadSound()
  {
    if (string.IsNullOrEmpty(this.m_path))
      return;
    AssetLoader assetLoader = AssetLoader.Get();
    bool alwaysValid = this.m_alwaysValid;
    string path = this.m_path;
    int num1 = 1;
    int num2 = 0;
    int num3 = alwaysValid ? 1 : 0;
    GameObject gameObject = assetLoader.LoadSound(path, num1 != 0, num2 != 0, num3 != 0);
    if ((Object) gameObject == (Object) null)
    {
      if (AssetLoader.DOWNLOADABLE_LANGUAGE_PACKS || !this.m_alwaysValid)
        return;
      string message = string.Format("CardSound.LoadSound() - Failed to load \"{0}\"", (object) this.m_path);
      if (ApplicationMgr.UseDevWorkarounds())
        Debug.LogError((object) message);
      else
        Error.AddDevFatal(message);
    }
    else
    {
      this.m_source = gameObject.GetComponent<AudioSource>();
      if ((Object) this.m_source == (Object) null)
      {
        Object.Destroy((Object) gameObject);
        if (!this.m_alwaysValid)
          return;
        string message = string.Format("CardSound.LoadSound() - \"{0}\" does not have an AudioSource component.", (object) this.m_path);
        if (ApplicationMgr.UseDevWorkarounds())
          Debug.LogError((object) message);
        else
          Error.AddDevFatal(message);
      }
      else
        this.SetupSound();
    }
  }

  private void SetupSound()
  {
    if ((Object) this.m_source == (Object) null || (Object) this.m_owner == (Object) null)
      return;
    this.m_source.transform.parent = this.m_owner.transform;
    TransformUtil.Identity((Component) this.m_source.transform);
  }
}
