﻿// Decompiled with JetBrains decompiler
// Type: TavernBrawlTicketDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TavernBrawlTicketDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private bool m_CanBeOwned;
  [SerializeField]
  private bool m_CanBePurchased;
  [SerializeField]
  private DbfLocValue m_StoreName;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("CAN_BE_OWNED", "")]
  public bool CanBeOwned
  {
    get
    {
      return this.m_CanBeOwned;
    }
  }

  [DbfField("CAN_BE_PURCHASED", "")]
  public bool CanBePurchased
  {
    get
    {
      return this.m_CanBePurchased;
    }
  }

  [DbfField("STORE_NAME", "")]
  public DbfLocValue StoreName
  {
    get
    {
      return this.m_StoreName;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    TavernBrawlTicketDbfAsset brawlTicketDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (TavernBrawlTicketDbfAsset)) as TavernBrawlTicketDbfAsset;
    if ((UnityEngine.Object) brawlTicketDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("TavernBrawlTicketDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < brawlTicketDbfAsset.Records.Count; ++index)
      brawlTicketDbfAsset.Records[index].StripUnusedLocales();
    records = (object) brawlTicketDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_StoreName.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetCanBeOwned(bool v)
  {
    this.m_CanBeOwned = v;
  }

  public void SetCanBePurchased(bool v)
  {
    this.m_CanBePurchased = v;
  }

  public void SetStoreName(DbfLocValue v)
  {
    this.m_StoreName = v;
    v.SetDebugInfo(this.ID, "STORE_NAME");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (TavernBrawlTicketDbfRecord.\u003C\u003Ef__switch\u0024map6E == null)
      {
        // ISSUE: reference to a compiler-generated field
        TavernBrawlTicketDbfRecord.\u003C\u003Ef__switch\u0024map6E = new Dictionary<string, int>(5)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "CAN_BE_OWNED",
            2
          },
          {
            "CAN_BE_PURCHASED",
            3
          },
          {
            "STORE_NAME",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (TavernBrawlTicketDbfRecord.\u003C\u003Ef__switch\u0024map6E.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.CanBeOwned;
          case 3:
            return (object) this.CanBePurchased;
          case 4:
            return (object) this.StoreName;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (TavernBrawlTicketDbfRecord.\u003C\u003Ef__switch\u0024map6F == null)
    {
      // ISSUE: reference to a compiler-generated field
      TavernBrawlTicketDbfRecord.\u003C\u003Ef__switch\u0024map6F = new Dictionary<string, int>(5)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "CAN_BE_OWNED",
          2
        },
        {
          "CAN_BE_PURCHASED",
          3
        },
        {
          "STORE_NAME",
          4
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!TavernBrawlTicketDbfRecord.\u003C\u003Ef__switch\u0024map6F.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetCanBeOwned((bool) val);
        break;
      case 3:
        this.SetCanBePurchased((bool) val);
        break;
      case 4:
        this.SetStoreName((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (TavernBrawlTicketDbfRecord.\u003C\u003Ef__switch\u0024map70 == null)
      {
        // ISSUE: reference to a compiler-generated field
        TavernBrawlTicketDbfRecord.\u003C\u003Ef__switch\u0024map70 = new Dictionary<string, int>(5)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "CAN_BE_OWNED",
            2
          },
          {
            "CAN_BE_PURCHASED",
            3
          },
          {
            "STORE_NAME",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (TavernBrawlTicketDbfRecord.\u003C\u003Ef__switch\u0024map70.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (bool);
          case 3:
            return typeof (bool);
          case 4:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
