﻿// Decompiled with JetBrains decompiler
// Type: GamesWonCrown
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class GamesWonCrown
{
  public GameObject m_crown;

  public void Show()
  {
    this.m_crown.SetActive(true);
  }

  public void Hide()
  {
    this.m_crown.SetActive(false);
  }

  public void Animate()
  {
    this.Show();
    this.m_crown.GetComponent<PlayMakerFSM>().SendEvent("Birth");
  }
}
