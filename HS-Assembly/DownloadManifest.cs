﻿// Decompiled with JetBrains decompiler
// Type: DownloadManifest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DownloadManifest
{
  private static string s_downloadManifestFilePath = FileUtils.GetAssetPath("manifest-downloads.csv");
  private const string MANIFEST_DIVIDER = "<END OF HASHES>";
  private static DownloadManifest s_downloadManifest;
  private Map<string, string> m_hashNames;
  private HashSet<string> m_fileSet;
  private static List<string> s_filesToWrite;
  private static Map<string, string> s_hashesToWrite;

  private DownloadManifest()
  {
    this.m_fileSet = new HashSet<string>();
    this.m_hashNames = new Map<string, string>();
  }

  public static DownloadManifest Get()
  {
    if (DownloadManifest.s_downloadManifest == null)
      DownloadManifest.s_downloadManifest = new DownloadManifest();
    return DownloadManifest.s_downloadManifest;
  }

  public bool ContainsFile(string filePath)
  {
    return this.m_fileSet.Contains(filePath);
  }

  public string HashForBundle(string bundleName)
  {
    string str;
    this.m_hashNames.TryGetValue(bundleName, out str);
    return str;
  }

  public string DownloadableBundleFileName(string bundleName)
  {
    if (this.HashForBundle(bundleName) == null)
      return (string) null;
    return bundleName;
  }

  public static void ClearDataToWrite()
  {
    if (DownloadManifest.s_filesToWrite != null)
      DownloadManifest.s_filesToWrite.Clear();
    if (DownloadManifest.s_hashesToWrite == null)
      return;
    DownloadManifest.s_hashesToWrite.Clear();
  }

  public static void AddFileToWrite(string filePath)
  {
    if (DownloadManifest.s_filesToWrite == null)
      DownloadManifest.s_filesToWrite = new List<string>();
    lock (DownloadManifest.s_filesToWrite)
    {
      if (DownloadManifest.s_filesToWrite.Contains(filePath))
        return;
      DownloadManifest.s_filesToWrite.Add(filePath);
    }
  }

  public static void AddHashNameForBundle(string name, string hash)
  {
    if (DownloadManifest.s_hashesToWrite == null)
      DownloadManifest.s_hashesToWrite = new Map<string, string>();
    lock (DownloadManifest.s_hashesToWrite)
    {
      try
      {
        DownloadManifest.s_hashesToWrite.Add(name, hash);
      }
      catch (ArgumentException exception_0)
      {
        Debug.LogError((object) string.Format("Exception adding key {0} with value {1} to hashesToWrite dict. Exception: {2}", (object) name, (object) hash, (object) exception_0.Message));
      }
    }
  }

  public static void WriteToFile(string path)
  {
    if (DownloadManifest.s_filesToWrite == null)
      return;
    string directoryName = Path.GetDirectoryName(path);
    if (!string.IsNullOrEmpty(directoryName) && !Directory.Exists(directoryName))
      Directory.CreateDirectory(directoryName);
    using (StreamWriter streamWriter = new StreamWriter(path))
    {
      if (DownloadManifest.s_hashesToWrite != null)
      {
        lock (DownloadManifest.s_hashesToWrite)
        {
          using (Map<string, string>.Enumerator resource_0 = DownloadManifest.s_hashesToWrite.GetEnumerator())
          {
            while (resource_0.MoveNext())
            {
              KeyValuePair<string, string> local_4 = resource_0.Current;
              streamWriter.Write(local_4.Value);
              streamWriter.Write(";");
              streamWriter.WriteLine(local_4.Key);
            }
          }
        }
      }
      streamWriter.WriteLine("<END OF HASHES>");
      lock (DownloadManifest.s_filesToWrite)
      {
        using (List<string>.Enumerator resource_1 = DownloadManifest.s_filesToWrite.GetEnumerator())
        {
          while (resource_1.MoveNext())
          {
            string local_6 = resource_1.Current;
            streamWriter.WriteLine(local_6);
          }
        }
      }
    }
  }

  private void Load()
  {
    string manifestFilePath = DownloadManifest.s_downloadManifestFilePath;
    int num = 0;
    try
    {
      using (StreamReader streamReader = new StreamReader(manifestFilePath))
      {
        bool flag = true;
        string line;
        while ((line = streamReader.ReadLine()) != null)
        {
          ++num;
          if (!string.IsNullOrEmpty(line))
          {
            if (flag)
            {
              if (line.Equals("<END OF HASHES>"))
                flag = false;
              else
                this.ParseAndAddHashName(line);
            }
            else
              this.m_fileSet.Add(line);
          }
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      Error.AddDevFatal(string.Format("Failed to find download manifest at '{0}': {1}", (object) manifestFilePath, (object) ex.Message));
    }
    catch (IOException ex)
    {
      Error.AddDevFatal(string.Format("Failed to read download manifest at '{0}': {1}", (object) manifestFilePath, (object) ex.Message));
    }
    catch (NullReferenceException ex)
    {
      Error.AddDevFatal(string.Format("Failed to read from download manifest '{0}' line {1}: {2}", (object) manifestFilePath, (object) num, (object) ex.Message));
    }
    catch (Exception ex)
    {
      Error.AddDevFatal(string.Format("An unknown error occurred loading download manifest '{0}' line {1}: {2}", (object) manifestFilePath, (object) num, (object) ex.Message));
    }
  }

  private bool ParseAndAddHashName(string line)
  {
    string[] strArray = line.Split(';');
    if (strArray.Length != 2)
      return false;
    string str = strArray[0];
    this.m_hashNames.Add(strArray[1], str);
    return true;
  }
}
