﻿// Decompiled with JetBrains decompiler
// Type: Unity.Performance.ValueBinUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Unity.Performance
{
  public static class ValueBinUtils
  {
    public static void AddValue(this ValueBin[] bins, float value)
    {
      for (int index = 0; index < bins.Length; ++index)
      {
        if ((double) bins[index].v >= (double) value)
        {
          ++bins[index].f;
          break;
        }
      }
    }

    public static float EstimatedPercentile(this ValueBin[] bins, float percentile)
    {
      int totalSamples = 0;
      foreach (ValueBin bin in bins)
        totalSamples += bin.f;
      return bins.EstimatedPercentile(totalSamples, percentile);
    }

    public static float EstimatedPercentile(this ValueBin[] bins, int totalSamples, float percentile)
    {
      int num1 = Mathf.RoundToInt(percentile * (float) totalSamples);
      if (num1 >= totalSamples)
        num1 = totalSamples - 1;
      int num2 = 0;
      while (num1 >= 0 && num2 < bins.Length)
        num1 -= bins[num2++].f;
      int index = num2 - 1;
      if (index >= bins.Length)
        return bins[bins.Length - 1].v;
      return (float) (((double) bins[index].v + (index <= 0 ? 0.0 : (double) bins[index - 1].v)) * 0.5);
    }
  }
}
