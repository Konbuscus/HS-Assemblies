﻿// Decompiled with JetBrains decompiler
// Type: Unity.Performance.IDataSource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace Unity.Performance
{
  public interface IDataSource
  {
    float unscaledDeltaTimeSeconds { get; }

    float realtimeSinceStartup { get; }

    int frameCount { get; }

    uint memoryAllocated { get; }
  }
}
