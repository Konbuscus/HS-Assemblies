﻿// Decompiled with JetBrains decompiler
// Type: Unity.Performance.Result
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Unity.Performance
{
  [Serializable]
  public class Result
  {
    public string name;
    public ValueBin[] deltaTimeMSBins;
    public float deltaTimeMSMax;
    public float deltaTimeMSMin;
    public uint memoryUsageAtStart;
    public uint memoryUsageAtEnd;
    public uint memoryUsageAtPeak;
    public long memoryTotal;
    public double memoryAverage;
    public long memorySystem;
    public int firstFrameNumber;
    public int lastFrameNumber;
    public float realtimeAtStart;
    public float realtimeAtStop;
    public double totalPauseTime;
    public int totalSamples;
    public int refreshRate;
    private float expectedFrames;
    private float droppedFrames;
    private float targetFrameTime;
    private int deviceRefreshRate;
    private int appRefreshRate;
    private float[] percList;
    private string[] upperboundList;

    public int memoryUsageDelta
    {
      get
      {
        return (int) ((long) this.memoryUsageAtEnd - (long) this.memoryUsageAtStart);
      }
    }

    public float totalTime
    {
      get
      {
        return (float) ((double) this.realtimeAtStop - (double) this.realtimeAtStart - this.totalPauseTime / 1000.0);
      }
    }

    public Result(string experimentName)
    {
      this.name = experimentName;
      this.totalSamples = 0;
      this.firstFrameNumber = Benchmarking.DataSource.frameCount;
      this.memoryUsageAtStart = Benchmarking.DataSource.memoryAllocated;
      this.realtimeAtStart = Benchmarking.DataSource.realtimeSinceStartup;
      this.deltaTimeMSMin = float.MaxValue;
      this.deltaTimeMSMax = 0.0f;
      this.totalPauseTime = 0.0;
      this.populatePercList();
      this.populateUpperboundList();
      this.memoryUsageAtPeak = 0U;
      this.memoryTotal = 0L;
      this.memorySystem = (long) SystemInfo.systemMemorySize * 1048576L;
      this.deviceRefreshRate = Screen.currentResolution.refreshRate;
      this.appRefreshRate = Application.targetFrameRate;
      if (QualitySettings.vSyncCount == 2)
        this.deviceRefreshRate /= 2;
      if (this.deviceRefreshRate <= 0 && this.appRefreshRate <= 0)
      {
        this.refreshRate = 60;
        this.setDefaultRefreshRate();
      }
      else if (this.deviceRefreshRate <= 0)
        this.refreshRate = this.appRefreshRate;
      else if (this.appRefreshRate <= 0)
      {
        this.refreshRate = this.deviceRefreshRate;
        this.setDefaultRefreshRate();
      }
      else
        this.refreshRate = this.deviceRefreshRate >= this.appRefreshRate ? this.appRefreshRate : this.deviceRefreshRate;
      this.targetFrameTime = (float) (1.0 / (double) this.refreshRate * 1000.0);
      ValueBinBuilder valueBinBuilder = new ValueBinBuilder();
      valueBinBuilder.AddBin(this.percList[0] * this.targetFrameTime);
      for (int index = 0; index < this.percList.Length - 1; ++index)
      {
        float size = (this.percList[index + 1] - this.percList[index]) * this.targetFrameTime;
        valueBinBuilder.AddBin(size);
      }
      this.deltaTimeMSBins = valueBinBuilder.Result;
    }

    public void setDefaultRefreshRate()
    {
    }

    public void printHist()
    {
      foreach (ValueBin deltaTimeMsBin in this.deltaTimeMSBins)
        Debug.Log((object) ("upperbound: " + (object) deltaTimeMsBin.v + ", \n\tamount: " + (object) deltaTimeMsBin.f));
    }

    public void sendFrameData()
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary.Add("experiment_name", (object) string.Copy(Benchmarking.getExperimentName()));
      dictionary.Add("build_version", (object) Benchmarking.getBuildVersion());
      dictionary.Add("plugin_version", (object) "v1.0.1");
      dictionary.Add("refresh_rate", (object) this.refreshRate);
      dictionary.Add("experiment_time", (object) this.totalTime);
      for (int index = 0; index < this.percList.Length; ++index)
        dictionary.Add(this.upperboundList[index], (object) this.deltaTimeMSBins[index].f);
      this.expectedFrames = (float) this.refreshRate * this.totalTime;
      this.droppedFrames = this.expectedFrames - (float) this.totalSamples;
      float num1 = 60f / this.totalTime;
      if ((double) this.droppedFrames < 0.0)
      {
        Debug.Log((object) "Dropped frames is neg");
        this.droppedFrames = 0.0f;
      }
      else
        this.droppedFrames = this.droppedFrames * num1;
      dictionary.Add("dropped_frames_per_min", (object) this.droppedFrames);
      dictionary.Add("total_pause_time ", (object) (this.totalPauseTime / 1000.0));
      dictionary.Add("elapsed_edited ", (object) this.totalTime);
      int num2 = (int) UnityEngine.Analytics.Analytics.CustomEvent("perfFrameData", (IDictionary<string, object>) dictionary);
    }

    public void populatePercList()
    {
      this.percList = new float[27]
      {
        0.5f,
        0.75f,
        0.85f,
        0.9f,
        0.92f,
        0.94f,
        0.96f,
        0.98f,
        1f,
        1.01f,
        1.02f,
        1.03f,
        1.04f,
        1.05f,
        1.06f,
        1.07f,
        1.08f,
        1.09f,
        1.1f,
        1.15f,
        1.2f,
        2f,
        4f,
        8f,
        16f,
        32f,
        float.PositiveInfinity
      };
    }

    public void populateUpperboundList()
    {
      this.upperboundList = new string[27]
      {
        "upper_bound: 0.5",
        "upper_bound: 0.75",
        "upper_bound: 0.85",
        "upper_bound: 0.90",
        "upper_bound: 0.92",
        "upper_bound: 0.94",
        "upper_bound: 0.96",
        "upper_bound: 0.98",
        "upper_bound: 1",
        "upper_bound: 1.01",
        "upper_bound: 1.02",
        "upper_bound: 1.03",
        "upper_bound: 1.04",
        "upper_bound: 1.05",
        "upper_bound: 1.06",
        "upper_bound: 1.07",
        "upper_bound: 1.08",
        "upper_bound: 1.09",
        "upper_bound: 1.1",
        "upper_bound: 1.15",
        "upper_bound: 1.2",
        "upper_bound: 2",
        "upper_bound: 4",
        "upper_bound: 8",
        "upper_bound: 16",
        "upper_bound: 32",
        "upper_bound: Infinity"
      };
    }
  }
}
