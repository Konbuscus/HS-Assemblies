﻿// Decompiled with JetBrains decompiler
// Type: Unity.Performance.DefaultDataSource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Unity.Performance
{
  public class DefaultDataSource : IDataSource
  {
    public float unscaledDeltaTimeSeconds
    {
      get
      {
        return Time.unscaledDeltaTime;
      }
    }

    public float realtimeSinceStartup
    {
      get
      {
        return Time.realtimeSinceStartup;
      }
    }

    public int frameCount
    {
      get
      {
        return Time.frameCount;
      }
    }

    public uint memoryAllocated
    {
      get
      {
        return Profiler.GetTotalAllocatedMemory();
      }
    }
  }
}
