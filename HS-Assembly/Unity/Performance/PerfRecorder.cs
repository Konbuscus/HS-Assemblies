﻿// Decompiled with JetBrains decompiler
// Type: Unity.Performance.PerfRecorder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Unity.Performance
{
  public class PerfRecorder : MonoBehaviour
  {
    public void SetBuildVersion(string buildVersion)
    {
      Benchmarking.setBuildVersion(buildVersion);
    }

    public void BeginExperiment(string experimentName)
    {
      Benchmarking.BeginExperiment(experimentName);
    }

    public void EndExperiment()
    {
      Benchmarking.EndExperiment();
      Benchmarking.Finished();
    }

    public void LoadScene(string sceneName)
    {
      Benchmarking.LoadScene(sceneName);
    }

    public void ClearAllResults()
    {
      Benchmarking.Clear();
    }

    private void Update()
    {
      Benchmarking.Update();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
      Benchmarking.OnApplicationPause(pauseStatus);
    }
  }
}
