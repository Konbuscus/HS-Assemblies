﻿// Decompiled with JetBrains decompiler
// Type: Unity.Performance.IProfilerControl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace Unity.Performance
{
  public interface IProfilerControl
  {
    bool supported { get; }

    bool recording { get; }

    void StartRecording(string filePath);

    void StopRecording();
  }
}
