﻿// Decompiled with JetBrains decompiler
// Type: Unity.Performance.Benchmarking
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine.SceneManagement;

namespace Unity.Performance
{
  public static class Benchmarking
  {
    private static List<Result> _results = new List<Result>();
    private static Stopwatch pauseTimer = new Stopwatch();
    public static IDataSource DataSource = (IDataSource) new DefaultDataSource();
    public static IProfilerControl ProfilerControl = (IProfilerControl) new DefaultProfilerControl();
    private static Result _activeExperiment;
    private static float _deltaTimeMSSum;
    private static float _deltaTimeMSSqrSum;
    private static int _lastFrameRecorded;
    private static string _experimentName;
    public static string ProfilerCapturePath;
    private static string buildVersion;

    public static IEnumerable<Result> Results
    {
      get
      {
        return (IEnumerable<Result>) Benchmarking._results;
      }
    }

    public static string getBuildVersion()
    {
      return Benchmarking.buildVersion;
    }

    public static void setBuildVersion(string newBuildVersion)
    {
      Benchmarking.buildVersion = newBuildVersion;
    }

    public static string getExperimentName()
    {
      return Benchmarking._experimentName;
    }

    public static void setExperimentName(string newExperimentName)
    {
      Benchmarking._experimentName = newExperimentName;
    }

    public static Result BeginExperiment(string experimentName)
    {
      UnityEngine.Debug.Log((object) ("Beginning Experiment: " + experimentName));
      if (Benchmarking._activeExperiment != null)
      {
        Benchmarking.EndExperiment();
        Benchmarking.Finished();
        Benchmarking.Clear();
      }
      Benchmarking._activeExperiment = new Result(experimentName);
      Benchmarking.setExperimentName(experimentName);
      Benchmarking._deltaTimeMSSum = 0.0f;
      Benchmarking._deltaTimeMSSqrSum = 0.0f;
      Benchmarking._lastFrameRecorded = Benchmarking.DataSource.frameCount;
      if (!string.IsNullOrEmpty(Benchmarking.ProfilerCapturePath) && Benchmarking.ProfilerControl != null && Benchmarking.ProfilerControl.supported)
        Benchmarking.ProfilerControl.StartRecording(Benchmarking.ProfilerCapturePath + experimentName);
      return Benchmarking._activeExperiment;
    }

    public static Result EndExperiment()
    {
      UnityEngine.Debug.Log((object) "Ending Experiment");
      if (Benchmarking._activeExperiment == null)
        throw new InvalidOperationException("EndExperiment cannot be called when there is no active experiment.");
      Benchmarking._activeExperiment.lastFrameNumber = Benchmarking.DataSource.frameCount;
      Benchmarking._activeExperiment.realtimeAtStop = Benchmarking.DataSource.realtimeSinceStartup;
      Benchmarking._activeExperiment.memoryUsageAtEnd = Benchmarking.DataSource.memoryAllocated;
      Benchmarking._activeExperiment.totalPauseTime = Benchmarking.pauseTimer.Elapsed.TotalMilliseconds;
      Benchmarking.pauseTimer.Reset();
      Result activeExperiment = Benchmarking._activeExperiment;
      Benchmarking._results.Add(activeExperiment);
      Benchmarking._activeExperiment = (Result) null;
      if (Benchmarking.ProfilerControl != null && Benchmarking.ProfilerControl.recording)
        Benchmarking.ProfilerControl.StopRecording();
      return activeExperiment;
    }

    public static void Finished()
    {
      using (List<Result>.Enumerator enumerator = Benchmarking._results.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.sendFrameData();
      }
      Benchmarking.Clear();
    }

    public static void LoadScene(string sceneName)
    {
      Stopwatch stopwatch = new Stopwatch();
      uint memoryAllocated1 = Benchmarking.DataSource.memoryAllocated;
      stopwatch.Start();
      SceneManager.LoadScene(sceneName);
      stopwatch.Stop();
      uint memoryAllocated2 = Benchmarking.DataSource.memoryAllocated;
      TimeSpan elapsed = stopwatch.Elapsed;
      uint num1 = memoryAllocated2 - memoryAllocated1;
      int num2 = (int) UnityEngine.Analytics.Analytics.CustomEvent("perfSceneLoad", (IDictionary<string, object>) new Dictionary<string, object>() { { "plugin_version", (object) "v1.0.1" }, { "scene_name", (object) sceneName }, { "build_version", (object) Benchmarking.buildVersion }, { "load_time", (object) elapsed.TotalMilliseconds }, { "memory_delta_between_loads(bytes)", (object) num1 } });
    }

    public static void Clear()
    {
      Benchmarking._results = new List<Result>();
    }

    private static void LogWarning(string format, params object[] args)
    {
      UnityEngine.Debug.LogWarningFormat(format, args);
    }

    public static void OnApplicationPause(bool pauseStatus)
    {
      if (pauseStatus && Benchmarking._activeExperiment != null)
      {
        Benchmarking.pauseTimer.Start();
        UnityEngine.Debug.Log((object) "Pause Timer Started");
      }
      else
        Benchmarking.pauseTimer.Stop();
    }

    public static void Update()
    {
      if (Benchmarking._activeExperiment == null || Benchmarking.DataSource.frameCount == Benchmarking._lastFrameRecorded)
        return;
      if (Benchmarking.DataSource.frameCount != Benchmarking._lastFrameRecorded + 1)
        Benchmarking.LogWarning("{0} missing frames detected. The experiment results will be inaccurate. Please check that you are calling Benchmarking.Update every frame!", (object) (Benchmarking.DataSource.frameCount - Benchmarking._lastFrameRecorded - 1));
      Benchmarking._lastFrameRecorded = Benchmarking.DataSource.frameCount;
      float num = Benchmarking.DataSource.unscaledDeltaTimeSeconds * 1000f;
      if ((double) num < (double) Benchmarking._activeExperiment.deltaTimeMSMin)
        Benchmarking._activeExperiment.deltaTimeMSMin = num;
      if ((double) num > (double) Benchmarking._activeExperiment.deltaTimeMSMax)
        Benchmarking._activeExperiment.deltaTimeMSMax = num;
      ++Benchmarking._activeExperiment.totalSamples;
      Benchmarking._deltaTimeMSSum += num;
      Benchmarking._deltaTimeMSSqrSum += num * num;
      Benchmarking._activeExperiment.deltaTimeMSBins.AddValue(num);
      uint memoryAllocated = Benchmarking.DataSource.memoryAllocated;
      Benchmarking._activeExperiment.memoryTotal += (long) memoryAllocated;
      if (memoryAllocated <= Benchmarking._activeExperiment.memoryUsageAtPeak)
        return;
      Benchmarking._activeExperiment.memoryUsageAtPeak = memoryAllocated;
    }
  }
}
