﻿// Decompiled with JetBrains decompiler
// Type: Unity.Performance.ValueBinBuilder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Unity.Performance
{
  internal class ValueBinBuilder
  {
    private readonly List<ValueBin> _bins;

    public float CurrentHighestValue
    {
      get
      {
        if (this._bins.Count > 0)
          return this._bins[this._bins.Count - 1].v;
        return 0.0f;
      }
    }

    public ValueBin[] Result
    {
      get
      {
        return this._bins.ToArray();
      }
    }

    public ValueBinBuilder()
    {
      this._bins = new List<ValueBin>();
    }

    public void AddBin(float size)
    {
      this._bins.Add(new ValueBin()
      {
        v = this.CurrentHighestValue + size,
        f = 0
      });
    }

    public void AddBinsUpTo(float size, float limit)
    {
      while ((double) this.CurrentHighestValue < (double) limit)
        this.AddBin(size);
    }
  }
}
