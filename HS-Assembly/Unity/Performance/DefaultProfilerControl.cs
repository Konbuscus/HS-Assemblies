﻿// Decompiled with JetBrains decompiler
// Type: Unity.Performance.DefaultProfilerControl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Unity.Performance
{
  internal class DefaultProfilerControl : IProfilerControl
  {
    public bool supported
    {
      get
      {
        return Profiler.supported;
      }
    }

    public bool recording
    {
      get
      {
        if (Profiler.supported)
          return Profiler.enabled;
        return false;
      }
    }

    public void StartRecording(string filePath)
    {
      Profiler.logFile = filePath;
      Profiler.enableBinaryLog = true;
      Profiler.enabled = true;
    }

    public void StopRecording()
    {
      Profiler.enabled = false;
    }
  }
}
