﻿// Decompiled with JetBrains decompiler
// Type: TentacleFX
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class TentacleFX : MonoBehaviour
{
  public bool doRotate = true;
  public GameObject m_TentacleRoot;
  public List<GameObject> m_Tentacles;

  private void Start()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_Tentacles.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.GetComponent<Renderer>().material.SetFloat("_Seed", Random.Range(0.0f, 2f));
    }
    if (!this.doRotate)
      return;
    this.m_TentacleRoot.transform.Rotate(Vector3.up, Random.Range(0.0f, 360f), Space.Self);
  }
}
