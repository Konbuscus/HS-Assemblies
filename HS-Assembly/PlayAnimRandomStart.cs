﻿// Decompiled with JetBrains decompiler
// Type: PlayAnimRandomStart
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PlayAnimRandomStart : MonoBehaviour
{
  public float maxWait = 10f;
  public float MinSpeed = 0.2f;
  public float MaxSpeed = 1.1f;
  public string animName = "Bubble1";
  public List<GameObject> m_Bubbles;
  public float minWait;

  private void Start()
  {
    this.StartCoroutine(this.PlayRandomBubbles());
  }

  [DebuggerHidden]
  private IEnumerator PlayRandomBubbles()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PlayAnimRandomStart.\u003CPlayRandomBubbles\u003Ec__Iterator339() { \u003C\u003Ef__this = this };
  }
}
