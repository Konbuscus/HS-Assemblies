﻿// Decompiled with JetBrains decompiler
// Type: LOE16_Boss2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class LOE16_Boss2 : LOE_MissionEntity
{
  private bool m_artifactLinePlayed;
  private bool m_firstExplorerHelp;

  public override void StartGameplaySoundtracks()
  {
    MusicManager.Get().StartPlaylist(MusicPlaylistType.InGame_LOE_Wing4Mission4);
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOE_16_TURN_2");
    this.PreloadSound("VO_ELISE_LOE16_ALT_1_FIRST_HALF_02");
    this.PreloadSound("VO_ELISE_LOE16_ALT_1_SECOND_HALF_03");
    this.PreloadSound("VO_LOE_16_TURN_2_2");
    this.PreloadSound("VO_LOE_16_TURN_2_3");
    this.PreloadSound("VO_LOE_16_TURN_3");
    this.PreloadSound("VO_LOE_16_TURN_3_2");
    this.PreloadSound("VO_LOE_16_TURN_4");
    this.PreloadSound("VO_LOE_16_TURN_5");
    this.PreloadSound("VO_LOE_16_TURN_5_2");
    this.PreloadSound("VO_LOE_16_TURN_6");
    this.PreloadSound("VO_LOE_16_FIRST_ITEM");
    this.PreloadSound("VO_LOE_092_Attack_02");
    this.PreloadSound("VO_LOE_16_GOBLET");
    this.PreloadSound("VO_LOE_16_CROWN");
    this.PreloadSound("VO_LOE_16_EYE");
    this.PreloadSound("VO_LOE_16_PIPE");
    this.PreloadSound("VO_LOE_16_TEAR");
    this.PreloadSound("VO_LOE_16_SHARD");
    this.PreloadSound("VO_LOE_16_LOCKET");
    this.PreloadSound("VO_LOE_16_SPLINTER");
    this.PreloadSound("VO_LOE_16_VIAL");
    this.PreloadSound("VO_LOE_16_GREAVE");
    this.PreloadSound("VO_LOE_16_BOOM_BOT");
    this.PreloadSound("VO_LOEA16_1_CARD_04");
    this.PreloadSound("VO_LOEA16_1_RESPONSE_03");
    this.PreloadSound("VO_LOEA16_1_TURN1_02");
  }

  public override void OnTagChanged(TagDelta change)
  {
    base.OnTagChanged(change);
    Gameplay.Get().StartCoroutine(this.OnTagChangedHandler(change));
  }

  public override string UpdateCardText(Card card, Actor bigCardActor, string text)
  {
    Player opposingSidePlayer = GameState.Get().GetOpposingSidePlayer();
    if ((Object) opposingSidePlayer.GetHeroPowerCard() != (Object) card)
      return text;
    int num = opposingSidePlayer.GetHeroPower().GetTag(GAME_TAG.ELECTRIC_CHARGE_LEVEL);
    if (GameState.Get().GetGameEntity().GetTag(GAME_TAG.TURN) < 2)
      num = 3;
    string key = string.Empty;
    switch (num)
    {
      case 0:
        key = "LOEA16_2_STAFF_TEXT_CHARGE_EXPLODED";
        break;
      case 1:
        key = "LOEA16_2_STAFF_TEXT_CHARGE_1";
        break;
      case 2:
        key = "LOEA16_2_STAFF_TEXT_CHARGE_2";
        break;
      case 3:
        key = "LOEA16_2_STAFF_TEXT_CHARGE_0";
        break;
    }
    return GameStrings.Get(key);
  }

  [DebuggerHidden]
  private IEnumerator OnTagChangedHandler(TagDelta change)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE16_Boss2.\u003COnTagChangedHandler\u003Ec__Iterator182() { change = change, \u003C\u0024\u003Echange = change, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    LOE16_Boss2.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator183 timingCIterator183 = new LOE16_Boss2.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator183();
    return (IEnumerator) timingCIterator183;
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE16_Boss2.\u003CHandleMissionEventWithTiming\u003Ec__Iterator184() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE16_Boss2.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator185() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_LOEA16_1_RESPONSE_03",
            m_stringTag = "VO_LOEA16_1_RESPONSE_03"
          }
        }
      }
    };
  }
}
