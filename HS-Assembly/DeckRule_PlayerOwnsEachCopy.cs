﻿// Decompiled with JetBrains decompiler
// Type: DeckRule_PlayerOwnsEachCopy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DeckRule_PlayerOwnsEachCopy : DeckRule
{
  public DeckRule_PlayerOwnsEachCopy()
  {
    this.m_ruleType = DeckRule.RuleType.PLAYER_OWNS_EACH_COPY;
  }

  public DeckRule_PlayerOwnsEachCopy(DeckRulesetRuleDbfRecord record)
    : base(DeckRule.RuleType.PLAYER_OWNS_EACH_COPY, record)
  {
  }

  public override bool CanAddToDeck(EntityDef def, TAG_PREMIUM premium, CollectionDeck deck, out RuleInvalidReason reason)
  {
    reason = (RuleInvalidReason) null;
    string cardId = def.GetCardId();
    if (!this.AppliesTo(cardId))
      return true;
    CollectibleCard card = CollectionManager.Get().GetCard(cardId, premium);
    if (deck.GetOwnedCardCount(cardId, premium, true) < card.OwnedCount)
      return this.GetResult(true);
    reason = new RuleInvalidReason(GameStrings.Get("GLUE_COLLECTION_LOCK_NO_MORE_INSTANCES"), 0, false);
    return this.GetResult(false);
  }

  public override bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason)
  {
    reason = (RuleInvalidReason) null;
    if (deck.Locked)
      return true;
    CollectionManager collectionManager = CollectionManager.Get();
    List<CollectionDeckSlot> slots = deck.GetSlots();
    Map<KeyValuePair<string, TAG_PREMIUM>, int> map = new Map<KeyValuePair<string, TAG_PREMIUM>, int>();
    for (int index = 0; index < slots.Count; ++index)
    {
      CollectionDeckSlot collectionDeckSlot = slots[index];
      if (collectionDeckSlot.Count > 0 && this.AppliesTo(collectionDeckSlot.CardID))
      {
        KeyValuePair<string, TAG_PREMIUM> key = new KeyValuePair<string, TAG_PREMIUM>(collectionDeckSlot.CardID, collectionDeckSlot.Premium);
        int num = 0;
        map.TryGetValue(key, out num);
        map[key] = num + collectionDeckSlot.Count;
      }
    }
    int countParam = 0;
    using (Map<KeyValuePair<string, TAG_PREMIUM>, int>.Enumerator enumerator = map.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<KeyValuePair<string, TAG_PREMIUM>, int> current = enumerator.Current;
        string key = current.Key.Key;
        TAG_PREMIUM premium = current.Key.Value;
        int num1 = current.Value;
        CollectibleCard card = collectionManager.GetCard(key, premium);
        int num2 = card != null ? card.OwnedCount : 0;
        if (num2 < num1)
          countParam += num1 - num2;
      }
    }
    bool result = this.GetResult(countParam == 0);
    if (!result)
      reason = new RuleInvalidReason(GameStrings.Format("GLUE_COLLECTION_DECK_RULE_MISSING_CARDS", (object) countParam), countParam, false);
    return result;
  }
}
