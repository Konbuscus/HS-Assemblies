﻿// Decompiled with JetBrains decompiler
// Type: ExistingAccountPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ExistingAccountPopup : DialogBase
{
  private Vector3 m_buttonOffset = new Vector3(0.2f, 0.0f, 0.6f);
  public PegUIElement m_haveAccountButton;
  public PegUIElement m_noAccountButton;
  public GameObject m_bubble;
  public ExistingAccoundSound m_sound;
  private bool m_haveAccount;
  private ExistingAccountPopup.ResponseCallback m_responseCallback;

  private void Start()
  {
    this.transform.position = new Vector3(this.transform.position.x, -525f, 800f);
    this.m_haveAccountButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.HaveAccountButtonRelease));
    this.m_noAccountButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.NoAccountButtonRelease));
    this.m_haveAccountButton.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.HaveAccountButtonPress));
    this.m_noAccountButton.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.NoAccountButtonPress));
    this.FadeEffectsIn();
  }

  public override void Show()
  {
    base.Show();
    BaseUI.Get().m_BnetBar.Disable();
    this.m_bubble.SetActive(true);
    iTween.FadeTo(this.m_bubble, iTween.Hash((object) "time", (object) 0.0f, (object) "amount", (object) 1f, (object) "oncomplete", (object) "ShowBubble", (object) "oncompletetarget", (object) this.gameObject));
    this.m_showAnimState = DialogBase.ShowAnimState.IN_PROGRESS;
    UniversalInputManager.Get().SetSystemDialogActive(true);
    SoundManager.Get().LoadAndPlay(this.m_sound.m_popupShow);
    SoundManager.Get().LoadAndPlay(this.m_sound.m_innkeeperWelcome);
  }

  public void SetInfo(ExistingAccountPopup.Info info)
  {
    this.m_responseCallback = info.m_callback;
  }

  protected void FadeBubble()
  {
    iTween.FadeTo(this.m_bubble, iTween.Hash((object) "delay", (object) 6f, (object) "time", (object) 1f, (object) "amount", (object) 0.0f));
  }

  protected void ShowBubble()
  {
    iTween.FadeFrom(this.m_bubble, iTween.Hash((object) "delay", (object) 1f, (object) "time", (object) 0.5f, (object) "amount", (object) 0.0f, (object) "oncomplete", (object) "FadeBubble", (object) "oncompletetarget", (object) this.gameObject));
  }

  protected void DownScale()
  {
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) new Vector3(0.0f, 0.0f, 0.0f), (object) "delay", (object) 0.1, (object) "easetype", (object) iTween.EaseType.easeInOutCubic, (object) "oncomplete", (object) "OnHideAnimFinished", (object) "time", (object) 0.2f));
  }

  protected override void OnHideAnimFinished()
  {
    base.OnHideAnimFinished();
    this.m_shown = false;
    SoundManager.Get().LoadAndPlay(this.m_sound.m_popupHide);
    BaseUI.Get().m_BnetBar.Enable();
    this.m_responseCallback(this.m_haveAccount);
  }

  private void HaveAccountButtonRelease(UIEvent e)
  {
    this.m_haveAccount = true;
    this.m_haveAccountButton.transform.localPosition -= this.m_buttonOffset;
    this.ScaleAway();
  }

  private void NoAccountButtonRelease(UIEvent e)
  {
    this.m_haveAccount = false;
    this.m_noAccountButton.transform.localPosition -= this.m_buttonOffset;
    this.FadeEffectsOut();
  }

  private void HaveAccountButtonPress(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay(this.m_sound.m_buttonClick);
    this.m_haveAccountButton.transform.localPosition += this.m_buttonOffset;
  }

  private void NoAccountButtonPress(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay(this.m_sound.m_buttonClick);
    this.m_noAccountButton.transform.localPosition += this.m_buttonOffset;
  }

  private void ScaleAway()
  {
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) Vector3.Scale(this.PUNCH_SCALE, this.gameObject.transform.localScale), (object) "easetype", (object) iTween.EaseType.easeInOutCubic, (object) "oncomplete", (object) "DownScale", (object) "time", (object) 0.1f));
  }

  private void FadeEffectsIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if (!((Object) fullScreenFxMgr != (Object) null))
      return;
    fullScreenFxMgr.SetBlurBrightness(1f);
    fullScreenFxMgr.SetBlurDesaturation(0.0f);
    fullScreenFxMgr.Vignette(0.4f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.Blur(1f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void FadeEffectsOut()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((Object) fullScreenFxMgr != (Object) null)
    {
      fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
    this.ScaleAway();
  }

  public class Info
  {
    public ExistingAccountPopup.ResponseCallback m_callback;
  }

  public delegate void ResponseCallback(bool hasAccount);
}
