﻿// Decompiled with JetBrains decompiler
// Type: ScreenEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ScreenEffect : MonoBehaviour
{
  private ScreenEffectsMgr m_ScreenEffectsMgr;

  private void Awake()
  {
    this.m_ScreenEffectsMgr = ScreenEffectsMgr.Get();
  }

  private void OnEnable()
  {
    if ((Object) this.m_ScreenEffectsMgr == (Object) null)
      this.m_ScreenEffectsMgr = ScreenEffectsMgr.Get();
    ScreenEffectsMgr.RegisterScreenEffect(this);
  }

  private void OnDisable()
  {
    if ((Object) this.m_ScreenEffectsMgr == (Object) null)
      this.m_ScreenEffectsMgr = ScreenEffectsMgr.Get();
    if (!((Object) this.m_ScreenEffectsMgr != (Object) null))
      return;
    ScreenEffectsMgr.UnRegisterScreenEffect(this);
  }
}
