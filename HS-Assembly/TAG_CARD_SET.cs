﻿// Decompiled with JetBrains decompiler
// Type: TAG_CARD_SET
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum TAG_CARD_SET
{
  INVALID,
  TEST_TEMPORARY,
  CORE,
  EXPERT1,
  REWARD,
  MISSIONS,
  DEMO,
  NONE,
  CHEAT,
  BLANK,
  DEBUG_SP,
  PROMO,
  FP1,
  PE1,
  BRM,
  TGT,
  CREDITS,
  HERO_SKINS,
  TB,
  SLUSH,
  LOE,
  OG,
  OG_RESERVE,
  KARA,
  KARA_RESERVE,
  GANGS,
  GANGS_RESERVE,
}
