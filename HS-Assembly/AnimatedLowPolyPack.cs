﻿// Decompiled with JetBrains decompiler
// Type: AnimatedLowPolyPack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

[CustomEditClass]
public class AnimatedLowPolyPack : MonoBehaviour
{
  public Vector3 PUNCH_POSITION_AMOUNT = new Vector3(0.0f, 5f, 0.0f);
  public float PUNCH_POSITION_TIME = 0.25f;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_FlyOutSound = "Assets/Game/Sounds/Pack Purchasing/purchase_pack_lift_whoosh_1";
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_FlyInSound = "Assets/Game/Sounds/Pack Purchasing/purchase_pack_drop_impact_1";
  private Vector3 m_flyInLocalAngles = Vector3.zero;
  private Vector3 m_flyOutLocalAngles = Vector3.zero;
  private Vector3 m_targetOffScreenLocalPos = Vector3.zero;
  private Vector3 m_targetLocalPos = Vector3.zero;
  private bool m_changeActivation = true;
  public ParticleSystem m_DustParticle;
  public FirstPurchaseBox m_FirstPurchaseBox;
  private AnimatedLowPolyPack.State m_state;

  public int Column { get; private set; }

  public void Init(int column, Vector3 targetLocalPos, Vector3 offScreenOffset, bool ignoreFullscreenEffects = true, bool changeActivation = true)
  {
    this.m_targetLocalPos = targetLocalPos;
    this.m_targetOffScreenLocalPos = targetLocalPos + offScreenOffset;
    this.m_changeActivation = changeActivation;
    this.Column = column;
    if (ignoreFullscreenEffects)
      SceneUtils.SetLayer(this.gameObject, GameLayer.IgnoreFullScreenEffects);
    this.PositionOffScreen();
  }

  public void FlyInImmediate()
  {
    iTween.Stop(this.gameObject);
    this.transform.localEulerAngles = this.m_flyInLocalAngles;
    this.transform.localPosition = this.m_targetLocalPos;
    this.m_state = AnimatedLowPolyPack.State.FLOWN_IN;
    if (this.m_changeActivation)
      this.gameObject.SetActive(true);
    if (!((Object) this.m_FirstPurchaseBox != (Object) null))
      return;
    this.m_FirstPurchaseBox.RevealContents();
  }

  public bool FlyIn(float animTime, float delay)
  {
    if (this.m_state == AnimatedLowPolyPack.State.FLOWN_IN || this.m_state == AnimatedLowPolyPack.State.FLYING_IN)
      return false;
    this.m_state = AnimatedLowPolyPack.State.FLYING_IN;
    if (this.m_changeActivation)
      this.gameObject.SetActive(true);
    this.transform.localEulerAngles = this.m_flyInLocalAngles;
    if ((Object) this.m_FirstPurchaseBox != (Object) null)
      this.m_FirstPurchaseBox.Reset();
    Hashtable args = iTween.Hash((object) "position", (object) this.m_targetLocalPos, (object) "isLocal", (object) true, (object) "time", (object) animTime, (object) "delay", (object) delay, (object) "easetype", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) "OnFlownIn", (object) "oncompletetarget", (object) this.gameObject);
    iTween.Stop(this.gameObject);
    iTween.MoveTo(this.gameObject, args);
    return true;
  }

  public void FlyOutImmediate()
  {
    iTween.Stop(this.gameObject);
    this.transform.localEulerAngles = this.m_flyOutLocalAngles;
    this.transform.localPosition = this.m_targetOffScreenLocalPos;
    this.OnHidden();
  }

  public bool FlyOut(float animTime, float delay)
  {
    if (this.m_state == AnimatedLowPolyPack.State.HIDDEN || this.m_state == AnimatedLowPolyPack.State.FLYING_OUT)
      return false;
    this.m_state = AnimatedLowPolyPack.State.FLYING_OUT;
    this.transform.localEulerAngles = this.m_flyOutLocalAngles;
    Hashtable args = iTween.Hash((object) "position", (object) this.m_targetOffScreenLocalPos, (object) "isLocal", (object) true, (object) "time", (object) animTime, (object) "delay", (object) delay, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "OnHidden", (object) "oncompletetarget", (object) this.gameObject);
    iTween.Stop(this.gameObject);
    iTween.MoveTo(this.gameObject, args);
    if (!string.IsNullOrEmpty(this.m_FlyOutSound))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_FlyOutSound));
    return true;
  }

  public void SetFlyingLocalRotations(Vector3 flyInLocalAngles, Vector3 flyOutLocalAngles)
  {
    this.m_flyInLocalAngles = flyInLocalAngles;
    this.m_flyOutLocalAngles = flyOutLocalAngles;
  }

  public AnimatedLowPolyPack.State GetState()
  {
    return this.m_state;
  }

  public void Hide()
  {
    this.OnHidden();
  }

  public FirstPurchaseBox GetFirstPurchaseBox()
  {
    return this.m_FirstPurchaseBox;
  }

  private void OnHidden()
  {
    this.m_state = AnimatedLowPolyPack.State.HIDDEN;
    if (!this.m_changeActivation)
      return;
    this.gameObject.SetActive(false);
  }

  private void OnFlownIn()
  {
    this.m_DustParticle.Play();
    this.m_state = AnimatedLowPolyPack.State.FLOWN_IN;
    iTween.PunchPosition(this.gameObject, this.PUNCH_POSITION_AMOUNT, this.PUNCH_POSITION_TIME);
    if (!string.IsNullOrEmpty(this.m_FlyInSound))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_FlyInSound));
    if (!((Object) this.m_FirstPurchaseBox != (Object) null))
      return;
    this.m_FirstPurchaseBox.RevealContents();
  }

  private void PositionOffScreen()
  {
    iTween.Stop(this.gameObject);
    this.transform.localPosition = this.m_targetOffScreenLocalPos;
    this.OnHidden();
  }

  public enum State
  {
    UNKNOWN,
    FLOWN_IN,
    FLYING_IN,
    FLYING_OUT,
    HIDDEN,
  }
}
