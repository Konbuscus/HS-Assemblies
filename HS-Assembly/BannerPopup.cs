﻿// Decompiled with JetBrains decompiler
// Type: BannerPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class BannerPopup : MonoBehaviour
{
  private bool m_showSpellComplete = true;
  public GameObject m_root;
  public UberText m_text;
  public Spell m_ShowSpell;
  public Spell m_LoopingSpell;
  public Spell m_HideSpell;
  private BannerManager.DelOnCloseBanner m_onCloseBannerPopup;
  private PegUIElement m_inputBlocker;
  private bool m_onCloseCallbackCalled;

  private void Awake()
  {
    this.gameObject.SetActive(false);
  }

  private void Start()
  {
    if ((Object) this.m_ShowSpell == (Object) null)
    {
      this.OnShowSpellFinished((Spell) null, (object) null);
    }
    else
    {
      this.m_ShowSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnShowSpellFinished));
      this.m_ShowSpell.Activate();
    }
  }

  private void OnDestroy()
  {
    if (this.m_onCloseCallbackCalled)
      return;
    this.m_onCloseCallbackCalled = true;
    if (this.m_onCloseBannerPopup == null)
      return;
    this.m_onCloseBannerPopup();
  }

  public void Show(string bannerText, BannerManager.DelOnCloseBanner onCloseCallback = null)
  {
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    this.m_text.Text = bannerText;
    this.m_onCloseBannerPopup = onCloseCallback;
    this.gameObject.SetActive(true);
    this.m_root.GetComponent<Animation>().Play();
    GameObject inputBlocker = CameraUtils.CreateInputBlocker(CameraUtils.FindFirstByLayer(this.gameObject.layer), "ClosedSignInputBlocker", (Component) this);
    SceneUtils.SetLayer(inputBlocker, this.gameObject.layer);
    this.m_inputBlocker = inputBlocker.AddComponent<PegUIElement>();
    iTween.ScaleFrom(this.gameObject, iTween.Hash((object) "scale", (object) new Vector3(0.01f, 0.01f, 0.01f), (object) "time", (object) 0.25f, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "EnableClickHandler"));
    this.FadeEffectsIn();
    this.m_showSpellComplete = false;
  }

  private void FadeEffectsIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.SetBlurBrightness(1f);
    fullScreenFxMgr.SetBlurDesaturation(0.0f);
    fullScreenFxMgr.Vignette(0.4f, 0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.Blur(1f, 0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void FadeEffectsOut()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void CloseBannerPopup(UIEvent e)
  {
    this.m_inputBlocker.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.CloseBannerPopup));
    this.Close();
  }

  public void Close()
  {
    this.FadeEffectsOut();
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) Vector3.zero, (object) "time", (object) 0.5f, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "DestroyBannerPopup"));
    SoundManager.Get().LoadAndPlay("new_quest_click_and_shrink");
    ParticleSystem[] componentsInChildren = this.gameObject.GetComponentsInChildren<ParticleSystem>();
    if (componentsInChildren != null)
    {
      foreach (Component component in componentsInChildren)
        component.gameObject.SetActive(false);
    }
    if ((Object) this.m_LoopingSpell != (Object) null)
    {
      this.m_LoopingSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnLoopingSpellFinished));
      this.m_LoopingSpell.ActivateState(SpellStateType.DEATH);
    }
    else
    {
      if (!((Object) this.m_HideSpell != (Object) null))
        return;
      this.m_HideSpell.Activate();
    }
  }

  private void EnableClickHandler()
  {
    this.m_inputBlocker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.CloseBannerPopup));
  }

  private void DestroyBannerPopup()
  {
    this.m_onCloseCallbackCalled = true;
    if (this.m_onCloseBannerPopup != null)
      this.m_onCloseBannerPopup();
    this.StartCoroutine(this.DestroyPopupObject());
  }

  [DebuggerHidden]
  private IEnumerator DestroyPopupObject()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BannerPopup.\u003CDestroyPopupObject\u003Ec__Iterator2E1() { \u003C\u003Ef__this = this };
  }

  private void OnShowSpellFinished(Spell spell, object userData)
  {
    this.m_showSpellComplete = true;
    if ((Object) this.m_LoopingSpell == (Object) null)
      this.OnLoopingSpellFinished((Spell) null, (object) null);
    else
      this.m_LoopingSpell.ActivateState(SpellStateType.ACTION);
  }

  private void OnLoopingSpellFinished(Spell spell, object userData)
  {
    if (!((Object) this.m_HideSpell != (Object) null))
      return;
    this.m_HideSpell.Activate();
  }
}
