﻿// Decompiled with JetBrains decompiler
// Type: StoreMiniSummary
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StoreMiniSummary : MonoBehaviour
{
  public UberText m_headlineText;
  public UberText m_itemsHeadlineText;
  public UberText m_itemsText;

  private void Awake()
  {
    this.m_headlineText.Text = GameStrings.Get("GLUE_STORE_SUMMARY_HEADLINE");
    this.m_itemsHeadlineText.Text = GameStrings.Get("GLUE_STORE_SUMMARY_ITEMS_ORDERED_HEADLINE");
  }

  public void SetDetails(string productID, int quantity)
  {
    this.m_itemsText.Text = this.GetItemsText(productID, quantity);
  }

  private string GetItemsText(string productID, int quantity)
  {
    Network.Bundle bundle = StoreManager.Get().GetBundle(productID);
    string str = bundle != null ? StoreManager.Get().GetProductName(bundle.Items) : GameStrings.Get("GLUE_STORE_PRODUCT_NAME_MOBILE_UNKNOWN");
    return GameStrings.Format("GLUE_STORE_SUMMARY_ITEM_ORDERED", (object) quantity, (object) str);
  }
}
