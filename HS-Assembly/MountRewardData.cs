﻿// Decompiled with JetBrains decompiler
// Type: MountRewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class MountRewardData : RewardData
{
  public MountRewardData.MountType Mount { get; set; }

  public MountRewardData()
    : this(MountRewardData.MountType.UNKNOWN)
  {
  }

  public MountRewardData(MountRewardData.MountType mount)
    : base(Reward.Type.MOUNT)
  {
    this.Mount = mount;
  }

  public override string ToString()
  {
    return string.Format("[MountRewardData Mount={0} Origin={1} OriginData={2}]", (object) this.Mount, (object) this.Origin, (object) this.OriginData);
  }

  protected override string GetGameObjectName()
  {
    switch (this.Mount)
    {
      case MountRewardData.MountType.WOW_HEARTHSTEED:
        return "HearthSteedReward";
      case MountRewardData.MountType.HEROES_MAGIC_CARPET_CARD:
        return "CardMountReward";
      default:
        return string.Empty;
    }
  }

  public enum MountType
  {
    UNKNOWN,
    WOW_HEARTHSTEED,
    HEROES_MAGIC_CARPET_CARD,
  }
}
