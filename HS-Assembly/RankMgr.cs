﻿// Decompiled with JetBrains decompiler
// Type: RankMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class RankMgr
{
  public const int INVALID_RANK = -1;
  private static RankMgr s_instance;
  private TranslatedMedalInfo m_medalInfo;

  public bool AmILegendRankPlayer
  {
    get
    {
      if (!this.AmILegendRank(false))
        return this.AmILegendRank(true);
      return true;
    }
  }

  public static RankMgr Get()
  {
    if (RankMgr.s_instance == null)
      RankMgr.s_instance = new RankMgr();
    return RankMgr.s_instance;
  }

  public bool AmILegendRank(bool useWildRank)
  {
    bool flag = false;
    NetCache.NetCacheMedalInfo netObject = NetCache.Get().GetNetObject<NetCache.NetCacheMedalInfo>();
    if (netObject != null)
    {
      TranslatedMedalInfo currentMedal = new MedalInfoTranslator(netObject).GetCurrentMedal(useWildRank);
      if (currentMedal != null)
        flag = currentMedal.IsLegendRank();
    }
    return flag;
  }

  public bool SetRankPresenceField(NetCache.NetCacheMedalInfo medalInfo)
  {
    List<byte> byteList = new List<byte>();
    TranslatedMedalInfo currentMedal1 = new MedalInfoTranslator(medalInfo).GetCurrentMedal(false);
    byte num1 = Convert.ToByte(currentMedal1.rank);
    int legendIndex1 = currentMedal1.legendIndex;
    byteList.Add(num1);
    byte[] bytes1 = BitConverter.GetBytes(legendIndex1);
    byteList.Add(bytes1[0]);
    byteList.Add(bytes1[1]);
    TranslatedMedalInfo currentMedal2 = new MedalInfoTranslator(medalInfo).GetCurrentMedal(true);
    byte num2 = Convert.ToByte(currentMedal2.rank);
    int legendIndex2 = currentMedal2.legendIndex;
    byteList.Add(num2);
    byte[] bytes2 = BitConverter.GetBytes(legendIndex2);
    byteList.Add(bytes2[0]);
    byteList.Add(bytes2[1]);
    return BnetPresenceMgr.Get().SetGameField(18U, byteList.ToArray());
  }

  public MedalInfoTranslator GetRankPresenceField(BnetPlayer player)
  {
    if (player == null)
      return (MedalInfoTranslator) null;
    return this.GetRankPresenceField(player.GetHearthstoneGameAccount());
  }

  public MedalInfoTranslator GetRankPresenceField(BnetGameAccount gameAccount)
  {
    byte[] val;
    if (!(gameAccount != (BnetGameAccount) null) || !gameAccount.TryGetGameFieldBytes(18U, out val))
      return (MedalInfoTranslator) null;
    if (val == null)
      return (MedalInfoTranslator) null;
    if (val.Length < 6)
      return (MedalInfoTranslator) null;
    return new MedalInfoTranslator(Convert.ToInt32(val[0]), (int) BitConverter.ToUInt16(val, 1), Convert.ToInt32(val[3]), (int) BitConverter.ToUInt16(val, 4));
  }
}
