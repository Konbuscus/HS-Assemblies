﻿// Decompiled with JetBrains decompiler
// Type: BoxLogo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoxLogo : MonoBehaviour
{
  private Box m_parent;
  private BoxLogoStateInfo m_info;
  private BoxLogo.State m_state;

  public Box GetParent()
  {
    return this.m_parent;
  }

  public void SetParent(Box parent)
  {
    this.m_parent = parent;
  }

  public BoxLogoStateInfo GetInfo()
  {
    return this.m_info;
  }

  public void SetInfo(BoxLogoStateInfo info)
  {
    this.m_info = info;
  }

  public bool ChangeState(BoxLogo.State state)
  {
    if (this.m_state == state)
      return false;
    this.m_state = state;
    if (state == BoxLogo.State.SHOWN)
    {
      this.m_parent.OnAnimStarted();
      iTween.FadeTo(this.gameObject, iTween.Hash((object) "amount", (object) this.m_info.m_ShownAlpha, (object) "delay", (object) this.m_info.m_ShownDelaySec, (object) "time", (object) this.m_info.m_ShownFadeSec, (object) "easeType", (object) this.m_info.m_ShownFadeEaseType, (object) "oncomplete", (object) "OnAnimFinished", (object) "oncompletetarget", (object) this.m_parent.gameObject));
    }
    else if (state == BoxLogo.State.HIDDEN)
    {
      this.m_parent.OnAnimStarted();
      iTween.FadeTo(this.gameObject, iTween.Hash((object) "amount", (object) this.m_info.m_HiddenAlpha, (object) "delay", (object) this.m_info.m_HiddenDelaySec, (object) "time", (object) this.m_info.m_HiddenFadeSec, (object) "easeType", (object) this.m_info.m_HiddenFadeEaseType, (object) "oncomplete", (object) "OnAnimFinished", (object) "oncompletetarget", (object) this.m_parent.gameObject));
    }
    return true;
  }

  public void UpdateState(BoxLogo.State state)
  {
    this.m_state = state;
    if (state == BoxLogo.State.SHOWN)
    {
      RenderUtils.SetAlpha(this.gameObject, this.m_info.m_ShownAlpha);
    }
    else
    {
      if (state != BoxLogo.State.HIDDEN)
        return;
      RenderUtils.SetAlpha(this.gameObject, this.m_info.m_HiddenAlpha);
    }
  }

  public enum State
  {
    SHOWN,
    HIDDEN,
  }
}
