﻿// Decompiled with JetBrains decompiler
// Type: ChatMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChatMgr : MonoBehaviour
{
  private List<ChatBubbleFrame> m_chatBubbleFrames = new List<ChatBubbleFrame>();
  private List<BnetPlayer> m_recentWhisperPlayers = new List<BnetPlayer>();
  private Map<BnetPlayer, PlayerChatInfo> m_playerChatInfos = new Map<BnetPlayer, PlayerChatInfo>();
  private List<ChatMgr.PlayerChatInfoChangedListener> m_playerChatInfoChangedListeners = new List<ChatMgr.PlayerChatInfoChangedListener>();
  private Rect keyboardArea = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
  public ChatMgrPrefabs m_Prefabs;
  public ChatMgrBubbleInfo m_ChatBubbleInfo;
  public Float_MobileOverride m_friendsListXOffset;
  public Float_MobileOverride m_friendsListYOffset;
  public Float_MobileOverride m_friendsListWidthPadding;
  public Float_MobileOverride m_friendsListHeightPadding;
  public float m_chatLogXOffset;
  public Float_MobileOverride m_friendsListWidth;
  private static ChatMgr s_instance;
  private IChatLogUI m_chatLogUI;
  private FriendListFrame m_friendListFrame;
  private PegUIElement m_closeCatcher;
  private bool m_chatLogFrameShown;
  private ChatMgr.KeyboardState keyboardState;

  public FriendListFrame FriendListFrame
  {
    get
    {
      return this.m_friendListFrame;
    }
  }

  public Rect KeyboardRect
  {
    get
    {
      return this.keyboardArea;
    }
  }

  private void Awake()
  {
    ChatMgr.s_instance = this;
    BnetWhisperMgr.Get().AddWhisperListener(new BnetWhisperMgr.WhisperCallback(this.OnWhisper));
    BnetFriendMgr.Get().AddChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    W8Touch.Get().VirtualKeyboardDidShow += new Action(this.OnKeyboardShow);
    W8Touch.Get().VirtualKeyboardDidHide += new Action(this.OnKeyboardHide);
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
    this.InitCloseCatcher();
    this.InitChatLogUI();
  }

  private void OnDestroy()
  {
    ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
    W8Touch.Get().VirtualKeyboardDidShow -= new Action(this.OnKeyboardShow);
    W8Touch.Get().VirtualKeyboardDidHide -= new Action(this.OnKeyboardHide);
    ChatMgr.s_instance = (ChatMgr) null;
  }

  private void Start()
  {
    SoundManager.Get().Load("receive_message");
    this.UpdateLayout();
    if (!W8Touch.Get().IsVirtualKeyboardVisible())
      return;
    this.OnKeyboardShow();
  }

  private void Update()
  {
    Rect keyboardArea = this.keyboardArea;
    this.keyboardArea = TextField.KeyboardArea;
    if (!(this.keyboardArea != keyboardArea))
      return;
    this.UpdateLayout();
  }

  public static ChatMgr Get()
  {
    return ChatMgr.s_instance;
  }

  private void WillReset()
  {
    this.CleanUp();
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
  }

  private ChatMgr.KeyboardState ComputeKeyboardState()
  {
    if ((double) this.keyboardArea.height <= 0.0)
      return ChatMgr.KeyboardState.None;
    return (double) this.keyboardArea.y > (double) ((float) Screen.height - this.keyboardArea.yMax) ? ChatMgr.KeyboardState.Below : ChatMgr.KeyboardState.Above;
  }

  private void InitCloseCatcher()
  {
    this.m_closeCatcher = CameraUtils.CreateInputBlocker(BaseUI.Get().GetBnetCamera(), "CloseCatcher", (Component) this).AddComponent<PegUIElement>();
    this.m_closeCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCloseCatcherRelease));
    this.m_closeCatcher.gameObject.SetActive(false);
  }

  private void InitChatLogUI()
  {
    if (this.IsMobilePlatform())
      this.m_chatLogUI = (IChatLogUI) new MobileChatLogUI();
    else
      this.m_chatLogUI = (IChatLogUI) new DesktopChatLogUI();
  }

  private FriendListFrame CreateFriendsListUI()
  {
    GameObject gameObject = AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "FriendListFrame" : "FriendListFrame_phone", true, false);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      return (FriendListFrame) null;
    gameObject.transform.parent = this.transform;
    return gameObject.GetComponent<FriendListFrame>();
  }

  public void UpdateLayout()
  {
    if ((UnityEngine.Object) this.m_friendListFrame != (UnityEngine.Object) null || this.m_chatLogUI.IsShowing)
      this.UpdateLayoutForOnScreenKeyboard();
    this.UpdateChatBubbleParentLayout();
  }

  private void UpdateLayoutForOnScreenKeyboard()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.UpdateLayoutForOnScreenKeyboardOnPhone();
    }
    else
    {
      this.keyboardState = this.ComputeKeyboardState();
      bool flag = this.IsMobilePlatform();
      Camera bnetCamera = BaseUI.Get().GetBnetCamera();
      float num1 = bnetCamera.orthographicSize * 2f;
      float num2 = num1 * bnetCamera.aspect;
      float num3 = bnetCamera.transform.position.y + num1 / 2f;
      float num4 = bnetCamera.transform.position.x - num2 / 2f;
      float num5 = 0.0f;
      if (this.keyboardState != ChatMgr.KeyboardState.None && flag)
        num5 = num1 * this.keyboardArea.height / (float) Screen.height;
      float num6 = 0.0f;
      if ((UnityEngine.Object) this.m_friendListFrame != (UnityEngine.Object) null)
      {
        OrientedBounds orientedWorldBounds1 = TransformUtil.ComputeOrientedWorldBounds(BaseUI.Get().m_BnetBar.m_friendButton.gameObject, true);
        if (flag)
        {
          float num7 = this.keyboardState != ChatMgr.KeyboardState.Below ? orientedWorldBounds1.Extents[1].y * 2f : num5;
          this.m_friendListFrame.SetWorldHeight(num1 - num7);
        }
        OrientedBounds orientedWorldBounds2 = TransformUtil.ComputeOrientedWorldBounds(this.m_friendListFrame.gameObject, this.m_friendListFrame.GetIgnoreListForWorldBounds(), true);
        if (orientedWorldBounds2 != null)
        {
          if (!flag || this.keyboardState != ChatMgr.KeyboardState.Below)
            this.m_friendListFrame.SetWorldPosition(num4 + orientedWorldBounds2.Extents[0].x + orientedWorldBounds2.CenterOffset.x + (float) ((MobileOverrideValue<float>) this.m_friendsListXOffset), orientedWorldBounds1.GetTrueCenterPosition().y + orientedWorldBounds1.Extents[1].y + orientedWorldBounds2.Extents[1].y + orientedWorldBounds2.CenterOffset.y);
          else if (flag && this.keyboardState == ChatMgr.KeyboardState.Below)
            this.m_friendListFrame.SetWorldPosition(num4 + orientedWorldBounds2.Extents[0].x + orientedWorldBounds2.CenterOffset.x + (float) ((MobileOverrideValue<float>) this.m_friendsListXOffset), bnetCamera.transform.position.y - num1 / 2f + num5 + orientedWorldBounds2.Extents[1].y + orientedWorldBounds2.CenterOffset.y);
          num6 = orientedWorldBounds2.Extents[0].magnitude * 2f;
        }
      }
      if (this.m_chatLogUI.IsShowing)
      {
        ChatFrames component = this.m_chatLogUI.GameObject.GetComponent<ChatFrames>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        {
          float y = num3;
          if (this.keyboardState == ChatMgr.KeyboardState.Above)
            y -= num5;
          float height = num1 - num5;
          float x = num4;
          if (!(bool) UniversalInputManager.UsePhoneUI)
            x += num6 + (float) ((MobileOverrideValue<float>) this.m_friendsListXOffset) + this.m_chatLogXOffset;
          float width = num2;
          if (!(bool) UniversalInputManager.UsePhoneUI)
            width -= num6 + (float) ((MobileOverrideValue<float>) this.m_friendsListXOffset) + this.m_chatLogXOffset;
          component.chatLogFrame.SetWorldRect(x, y, width, height);
        }
      }
      this.OnChatFramesMoved();
    }
  }

  private void UpdateLayoutForOnScreenKeyboardOnPhone()
  {
    this.keyboardState = this.ComputeKeyboardState();
    bool flag = UniversalInputManager.Get().IsTouchMode();
    Camera bnetCamera = BaseUI.Get().GetBnetCamera();
    float num1 = bnetCamera.orthographicSize * 2f;
    float num2 = num1 * bnetCamera.aspect;
    float num3 = bnetCamera.transform.position.y + num1 / 2f;
    float num4 = bnetCamera.transform.position.x - num2 / 2f;
    float num5 = 0.0f;
    float num6 = 0.0f;
    if (this.keyboardState != ChatMgr.KeyboardState.None && flag)
    {
      num5 = num1 * this.keyboardArea.height / (float) Screen.height;
      num6 = num2 * this.keyboardArea.width / (float) Screen.width;
    }
    if ((UnityEngine.Object) this.m_friendListFrame != (UnityEngine.Object) null)
      this.m_friendListFrame.SetWorldRect(num4 + (float) ((MobileOverrideValue<float>) this.m_friendsListXOffset), num3 + (float) ((MobileOverrideValue<float>) this.m_friendsListYOffset), (float) ((MobileOverrideValue<float>) this.m_friendsListWidth) + (float) ((MobileOverrideValue<float>) this.m_friendsListWidthPadding), num1 + (float) ((MobileOverrideValue<float>) this.m_friendsListHeightPadding));
    if (this.m_chatLogUI.IsShowing)
    {
      ChatFrames component = this.m_chatLogUI.GameObject.GetComponent<ChatFrames>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      {
        float y = num3;
        if (this.keyboardState == ChatMgr.KeyboardState.Above)
          y -= num5;
        float height = num1 - num5;
        float x = num4;
        if (!(bool) UniversalInputManager.UsePhoneUI)
          x += (float) ((MobileOverrideValue<float>) this.m_friendsListWidth);
        float width = (double) num6 != 0.0 ? num6 : num2;
        if (!(bool) UniversalInputManager.UsePhoneUI)
          width -= (float) ((MobileOverrideValue<float>) this.m_friendsListWidth);
        component.chatLogFrame.SetWorldRect(x, y, width, height);
      }
    }
    this.OnChatFramesMoved();
  }

  public bool IsChatLogFrameShown()
  {
    if (this.IsMobilePlatform())
      return this.m_chatLogUI.IsShowing;
    return this.m_chatLogFrameShown;
  }

  private void OnCloseCatcherRelease(UIEvent e)
  {
    this.CloseChatUI();
  }

  public bool IsFriendListShowing()
  {
    if ((UnityEngine.Object) this.m_friendListFrame == (UnityEngine.Object) null)
      return false;
    return this.m_friendListFrame.gameObject.activeSelf;
  }

  public void ShowFriendsList()
  {
    if ((UnityEngine.Object) this.m_friendListFrame == (UnityEngine.Object) null)
      this.m_friendListFrame = this.CreateFriendsListUI();
    this.m_friendListFrame.gameObject.SetActive(true);
    this.m_closeCatcher.gameObject.SetActive(true);
    TransformUtil.SetPosZ((Component) this.m_closeCatcher, this.m_friendListFrame.transform.position.z + 100f);
    this.UpdateLayout();
  }

  private void HideFriendsList()
  {
    if (this.IsFriendListShowing())
      this.m_friendListFrame.gameObject.SetActive(false);
    if (!((UnityEngine.Object) this.m_closeCatcher != (UnityEngine.Object) null))
      return;
    this.m_closeCatcher.gameObject.SetActive(false);
  }

  public void CloseFriendsList()
  {
    this.DestroyFriendListFrame();
  }

  public void GoBack()
  {
    if (this.IsFriendListShowing())
    {
      this.CloseChatUI();
    }
    else
    {
      if (!this.m_chatLogUI.IsShowing)
        return;
      this.m_chatLogUI.Hide();
      this.ShowFriendsList();
    }
  }

  public void CloseChatUI()
  {
    if (this.m_chatLogUI.IsShowing)
      this.m_chatLogUI.Hide();
    this.CloseFriendsList();
  }

  public void CleanUp()
  {
    this.DestroyFriendListFrame();
  }

  private void DestroyFriendListFrame()
  {
    this.HideFriendsList();
    if ((UnityEngine.Object) this.m_friendListFrame == (UnityEngine.Object) null)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_friendListFrame.gameObject);
    this.m_friendListFrame = (FriendListFrame) null;
  }

  public List<BnetPlayer> GetRecentWhisperPlayers()
  {
    return this.m_recentWhisperPlayers;
  }

  public void AddRecentWhisperPlayerToTop(BnetPlayer player)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ChatMgr.\u003CAddRecentWhisperPlayerToTop\u003Ec__AnonStorey365 topCAnonStorey365 = new ChatMgr.\u003CAddRecentWhisperPlayerToTop\u003Ec__AnonStorey365();
    // ISSUE: reference to a compiler-generated field
    topCAnonStorey365.player = player;
    // ISSUE: reference to a compiler-generated method
    int index = this.m_recentWhisperPlayers.FindIndex(new Predicate<BnetPlayer>(topCAnonStorey365.\u003C\u003Em__4C));
    if (index < 0)
    {
      if (this.m_recentWhisperPlayers.Count == 10)
        this.m_recentWhisperPlayers.RemoveAt(this.m_recentWhisperPlayers.Count - 1);
    }
    else
      this.m_recentWhisperPlayers.RemoveAt(index);
    // ISSUE: reference to a compiler-generated field
    this.m_recentWhisperPlayers.Insert(0, topCAnonStorey365.player);
  }

  public void AddRecentWhisperPlayerToBottom(BnetPlayer player)
  {
    if (this.m_recentWhisperPlayers.Contains(player))
      return;
    if (this.m_recentWhisperPlayers.Count == 10)
      this.m_recentWhisperPlayers.RemoveAt(this.m_recentWhisperPlayers.Count - 1);
    this.m_recentWhisperPlayers.Add(player);
  }

  public void AddPlayerChatInfoChangedListener(ChatMgr.PlayerChatInfoChangedCallback callback)
  {
    this.AddPlayerChatInfoChangedListener(callback, (object) null);
  }

  public void AddPlayerChatInfoChangedListener(ChatMgr.PlayerChatInfoChangedCallback callback, object userData)
  {
    ChatMgr.PlayerChatInfoChangedListener infoChangedListener = new ChatMgr.PlayerChatInfoChangedListener();
    infoChangedListener.SetCallback(callback);
    infoChangedListener.SetUserData(userData);
    if (this.m_playerChatInfoChangedListeners.Contains(infoChangedListener))
      return;
    this.m_playerChatInfoChangedListeners.Add(infoChangedListener);
  }

  public bool RemovePlayerChatInfoChangedListener(ChatMgr.PlayerChatInfoChangedCallback callback)
  {
    return this.RemovePlayerChatInfoChangedListener(callback, (object) null);
  }

  public bool RemovePlayerChatInfoChangedListener(ChatMgr.PlayerChatInfoChangedCallback callback, object userData)
  {
    ChatMgr.PlayerChatInfoChangedListener infoChangedListener = new ChatMgr.PlayerChatInfoChangedListener();
    infoChangedListener.SetCallback(callback);
    infoChangedListener.SetUserData(userData);
    return this.m_playerChatInfoChangedListeners.Remove(infoChangedListener);
  }

  public PlayerChatInfo GetPlayerChatInfo(BnetPlayer player)
  {
    PlayerChatInfo playerChatInfo = (PlayerChatInfo) null;
    this.m_playerChatInfos.TryGetValue(player, out playerChatInfo);
    return playerChatInfo;
  }

  public PlayerChatInfo RegisterPlayerChatInfo(BnetPlayer player)
  {
    PlayerChatInfo playerChatInfo;
    if (!this.m_playerChatInfos.TryGetValue(player, out playerChatInfo))
    {
      playerChatInfo = new PlayerChatInfo();
      playerChatInfo.SetPlayer(player);
      this.m_playerChatInfos.Add(player, playerChatInfo);
    }
    return playerChatInfo;
  }

  public void OnFriendListOpened()
  {
    if (W8Touch.Get().IsVirtualKeyboardVisible())
      this.OnKeyboardShow();
    else
      this.UpdateChatBubbleParentLayout();
  }

  public void OnFriendListClosed()
  {
    if (W8Touch.Get().IsVirtualKeyboardVisible())
      this.OnKeyboardShow();
    else
      this.UpdateChatBubbleParentLayout();
  }

  public void OnFriendListFriendSelected(BnetPlayer friend)
  {
    this.ShowChatForPlayer(friend);
  }

  public void OnChatLogFrameShown()
  {
    this.m_chatLogFrameShown = true;
  }

  public void OnChatLogFrameHidden()
  {
    this.m_chatLogFrameShown = false;
  }

  public void OnChatReceiverChanged(BnetPlayer player)
  {
    this.UpdatePlayerFocusTime(player);
  }

  public void OnChatFramesMoved()
  {
    this.UpdateChatBubbleParentLayout();
  }

  public bool HandleKeyboardInput()
  {
    if (FatalErrorMgr.Get().HasError())
      return false;
    if (Input.GetKeyUp(KeyCode.Escape) && this.m_chatLogUI.IsShowing)
    {
      this.m_chatLogUI.Hide();
      return true;
    }
    if (!this.IsMobilePlatform() || !this.m_chatLogUI.IsShowing || !Input.GetKeyUp(KeyCode.Escape))
      return false;
    this.m_chatLogUI.GoBack();
    return true;
  }

  public void HandleGUIInput()
  {
    if (FatalErrorMgr.Get().HasError() || this.IsMobilePlatform())
      return;
    this.HandleGUIInputForQuickChat();
  }

  private void OnWhisper(BnetWhisper whisper, object userData)
  {
    this.AddRecentWhisperPlayerToTop(WhisperUtil.GetTheirPlayer(whisper));
    PlayerChatInfo chatInfo = this.RegisterPlayerChatInfo(WhisperUtil.GetTheirPlayer(whisper));
    try
    {
      if (this.m_chatLogUI.IsShowing && WhisperUtil.IsSpeakerOrReceiver(this.m_chatLogUI.Receiver, whisper) && this.IsMobilePlatform())
        chatInfo.SetLastSeenWhisper(whisper);
      else
        this.PopupNewChatBubble(whisper);
    }
    finally
    {
      this.FireChatInfoChangedEvent(chatInfo);
    }
  }

  private void OnFriendsChanged(BnetFriendChangelist changelist, object userData)
  {
    List<BnetPlayer> removedFriends = changelist.GetRemovedFriends();
    if (removedFriends == null)
      return;
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ChatMgr.\u003COnFriendsChanged\u003Ec__AnonStorey366 changedCAnonStorey366 = new ChatMgr.\u003COnFriendsChanged\u003Ec__AnonStorey366();
    using (List<BnetPlayer>.Enumerator enumerator = removedFriends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        changedCAnonStorey366.friend = enumerator.Current;
        // ISSUE: reference to a compiler-generated method
        int index = this.m_recentWhisperPlayers.FindIndex(new Predicate<BnetPlayer>(changedCAnonStorey366.\u003C\u003Em__4D));
        if (index >= 0)
          this.m_recentWhisperPlayers.RemoveAt(index);
      }
    }
  }

  private void OnFatalError(FatalErrorMessage message, object userData)
  {
    FatalErrorMgr.Get().RemoveErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    this.CleanUp();
  }

  private void HandleGUIInputForQuickChat()
  {
    if (!this.m_chatLogUI.IsShowing)
    {
      if (Event.current.type != EventType.KeyDown || Event.current.keyCode != KeyCode.Return)
        return;
      this.ShowChatForPlayer(this.GetMostRecentWhisperedPlayer());
    }
    else
    {
      if (Event.current.type != EventType.KeyUp || Event.current.keyCode != KeyCode.Escape)
        return;
      this.m_chatLogUI.Hide();
    }
  }

  public bool IsMobilePlatform()
  {
    if (UniversalInputManager.Get().IsTouchMode())
      return PlatformSettings.OS != OSCategory.PC;
    return false;
  }

  private void ShowChatForPlayer(BnetPlayer player)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ChatMgr.\u003CShowChatForPlayer\u003Ec__AnonStorey367 playerCAnonStorey367 = new ChatMgr.\u003CShowChatForPlayer\u003Ec__AnonStorey367();
    // ISSUE: reference to a compiler-generated field
    playerCAnonStorey367.player = player;
    // ISSUE: reference to a compiler-generated field
    if (playerCAnonStorey367.player != null)
    {
      // ISSUE: reference to a compiler-generated field
      this.AddRecentWhisperPlayerToTop(playerCAnonStorey367.player);
      // ISSUE: reference to a compiler-generated field
      PlayerChatInfo chatInfo = this.RegisterPlayerChatInfo(playerCAnonStorey367.player);
      // ISSUE: reference to a compiler-generated field
      List<BnetWhisper> whispersWithPlayer = BnetWhisperMgr.Get().GetWhispersWithPlayer(playerCAnonStorey367.player);
      if (whispersWithPlayer != null)
      {
        // ISSUE: reference to a compiler-generated method
        chatInfo.SetLastSeenWhisper(whispersWithPlayer.LastOrDefault<BnetWhisper>(new Func<BnetWhisper, bool>(playerCAnonStorey367.\u003C\u003Em__4E)));
        this.FireChatInfoChangedEvent(chatInfo);
      }
    }
    if (this.m_chatLogUI.IsShowing)
      this.m_chatLogUI.Hide();
    if (this.m_chatLogUI.IsShowing)
      return;
    if ((UnityEngine.Object) OptionsMenu.Get() != (UnityEngine.Object) null && OptionsMenu.Get().IsShown())
      OptionsMenu.Get().Hide(true);
    if ((UnityEngine.Object) GameMenu.Get() != (UnityEngine.Object) null && GameMenu.Get().IsShown())
      GameMenu.Get().Hide();
    this.m_chatLogUI.ShowForPlayer(this.GetMostRecentWhisperedPlayer());
    this.UpdateLayout();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.CloseFriendsList();
  }

  private BnetPlayer GetMostRecentWhisperedPlayer()
  {
    if (this.m_recentWhisperPlayers.Count > 0)
      return this.m_recentWhisperPlayers[0];
    return (BnetPlayer) null;
  }

  private void UpdatePlayerFocusTime(BnetPlayer player)
  {
    PlayerChatInfo chatInfo = this.RegisterPlayerChatInfo(player);
    chatInfo.SetLastFocusTime(Time.realtimeSinceStartup);
    this.FireChatInfoChangedEvent(chatInfo);
  }

  private void FireChatInfoChangedEvent(PlayerChatInfo chatInfo)
  {
    foreach (ChatMgr.PlayerChatInfoChangedListener infoChangedListener in this.m_playerChatInfoChangedListeners.ToArray())
      infoChangedListener.Fire(chatInfo);
  }

  private void UpdateChatBubbleParentLayout()
  {
    if (!((UnityEngine.Object) BaseUI.Get().GetChatBubbleBone() != (UnityEngine.Object) null))
      return;
    this.m_ChatBubbleInfo.m_Parent.transform.position = BaseUI.Get().GetChatBubbleBone().transform.position;
  }

  private void UpdateChatBubbleLayout()
  {
    int count = this.m_chatBubbleFrames.Count;
    if (count == 0)
      return;
    Component dst = (Component) this.m_ChatBubbleInfo.m_Parent;
    for (int index = count - 1; index >= 0; --index)
    {
      ChatBubbleFrame chatBubbleFrame = this.m_chatBubbleFrames[index];
      Anchor dstAnchor = !(bool) UniversalInputManager.UsePhoneUI ? Anchor.TOP_LEFT : Anchor.BOTTOM_LEFT;
      Anchor srcAnchor = !(bool) UniversalInputManager.UsePhoneUI ? Anchor.BOTTOM_LEFT : Anchor.TOP_LEFT;
      TransformUtil.SetPoint((Component) chatBubbleFrame, srcAnchor, dst, dstAnchor, Vector3.zero);
      dst = (Component) chatBubbleFrame;
    }
  }

  private void PopupNewChatBubble(BnetWhisper whisper)
  {
    ChatBubbleFrame chatBubble = this.CreateChatBubble(whisper);
    this.m_chatBubbleFrames.Add(chatBubble);
    this.UpdateChatBubbleParentLayout();
    chatBubble.transform.parent = this.m_ChatBubbleInfo.m_Parent.transform;
    chatBubble.transform.localScale = (Vector3) ((MobileOverrideValue<Vector3>) chatBubble.m_ScaleOverride);
    SoundManager.Get().LoadAndPlay("receive_message");
    Hashtable args = iTween.Hash((object) "scale", (object) chatBubble.m_VisualRoot.transform.localScale, (object) "time", (object) this.m_ChatBubbleInfo.m_ScaleInSec, (object) "easeType", (object) this.m_ChatBubbleInfo.m_ScaleInEaseType, (object) "oncomplete", (object) "OnChatBubbleScaleInComplete", (object) "oncompleteparams", (object) chatBubble, (object) "oncompletetarget", (object) this.gameObject);
    chatBubble.m_VisualRoot.transform.localScale = new Vector3(0.0001f, 0.0001f, 0.0001f);
    iTween.ScaleTo(chatBubble.m_VisualRoot, args);
    this.MoveChatBubbles(chatBubble);
  }

  private ChatBubbleFrame CreateChatBubble(BnetWhisper whisper)
  {
    ChatBubbleFrame chatBubbleFrame = this.InstantiateChatBubble(this.m_Prefabs.m_ChatBubbleOneLineFrame, whisper);
    if (!chatBubbleFrame.DoesMessageFit())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) chatBubbleFrame.gameObject);
      chatBubbleFrame = this.InstantiateChatBubble(this.m_Prefabs.m_ChatBubbleSmallFrame, whisper);
    }
    SceneUtils.SetLayer((Component) chatBubbleFrame, GameLayer.BattleNetDialog);
    return chatBubbleFrame;
  }

  private ChatBubbleFrame InstantiateChatBubble(ChatBubbleFrame prefab, BnetWhisper whisper)
  {
    ChatBubbleFrame chatBubbleFrame = UnityEngine.Object.Instantiate<ChatBubbleFrame>(prefab);
    chatBubbleFrame.SetWhisper(whisper);
    chatBubbleFrame.GetComponent<PegUIElement>().AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnChatBubbleReleased));
    return chatBubbleFrame;
  }

  private void MoveChatBubbles(ChatBubbleFrame newBubbleFrame)
  {
    Anchor dstAnchor = Anchor.TOP_LEFT;
    Anchor srcAnchor = Anchor.BOTTOM_LEFT;
    if ((bool) UniversalInputManager.UsePhoneUI && (double) this.m_ChatBubbleInfo.m_Parent.transform.localPosition.y > -900.0)
    {
      dstAnchor = Anchor.BOTTOM_LEFT;
      srcAnchor = Anchor.TOP_LEFT;
    }
    TransformUtil.SetPoint((Component) newBubbleFrame, srcAnchor, (Component) this.m_ChatBubbleInfo.m_Parent, dstAnchor, Vector3.zero);
    int count = this.m_chatBubbleFrames.Count;
    if (count == 1)
      return;
    Vector3[] vector3Array = new Vector3[count - 1];
    Component dst = (Component) newBubbleFrame;
    for (int index = count - 2; index >= 0; --index)
    {
      ChatBubbleFrame chatBubbleFrame = this.m_chatBubbleFrames[index];
      vector3Array[index] = chatBubbleFrame.transform.position;
      TransformUtil.SetPoint((Component) chatBubbleFrame, srcAnchor, dst, dstAnchor, Vector3.zero);
      dst = (Component) chatBubbleFrame;
    }
    for (int index = count - 2; index >= 0; --index)
    {
      ChatBubbleFrame chatBubbleFrame = this.m_chatBubbleFrames[index];
      Hashtable args = iTween.Hash((object) "islocal", (object) true, (object) "position", (object) chatBubbleFrame.transform.localPosition, (object) "time", (object) this.m_ChatBubbleInfo.m_MoveOverSec, (object) "easeType", (object) this.m_ChatBubbleInfo.m_MoveOverEaseType);
      chatBubbleFrame.transform.position = vector3Array[index];
      iTween.Stop(chatBubbleFrame.gameObject, "move");
      iTween.MoveTo(chatBubbleFrame.gameObject, args);
    }
  }

  private void OnChatBubbleScaleInComplete(ChatBubbleFrame bubbleFrame)
  {
    Hashtable args = iTween.Hash((object) "amount", (object) 0.0f, (object) "delay", (object) this.m_ChatBubbleInfo.m_HoldSec, (object) "time", (object) this.m_ChatBubbleInfo.m_FadeOutSec, (object) "easeType", (object) this.m_ChatBubbleInfo.m_FadeOutEaseType, (object) "oncomplete", (object) "OnChatBubbleFadeOutComplete", (object) "oncompleteparams", (object) bubbleFrame, (object) "oncompletetarget", (object) this.gameObject);
    iTween.FadeTo(bubbleFrame.gameObject, args);
  }

  private void OnChatBubbleFadeOutComplete(ChatBubbleFrame bubbleFrame)
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) bubbleFrame.gameObject);
    this.m_chatBubbleFrames.Remove(bubbleFrame);
  }

  private void RemoveAllChatBubbles()
  {
    using (List<ChatBubbleFrame>.Enumerator enumerator = this.m_chatBubbleFrames.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.m_chatBubbleFrames.Clear();
  }

  private void OnChatBubbleReleased(UIEvent e)
  {
    this.ShowChatForPlayer(WhisperUtil.GetTheirPlayer(e.GetElement().GetComponent<ChatBubbleFrame>().GetWhisper()));
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.RemoveAllChatBubbles();
  }

  public void OnKeyboardShow()
  {
    if (this.m_chatLogUI.IsShowing && BaseUI.Get().m_Bones.m_QuickChatVirtualKeyboard.position != this.m_chatLogUI.GameObject.transform.position)
    {
      W8Touch.Get().VirtualKeyboardDidShow -= new Action(this.OnKeyboardShow);
      W8Touch.Get().VirtualKeyboardDidHide -= new Action(this.OnKeyboardHide);
      this.m_chatLogUI.Hide();
      this.m_chatLogUI.ShowForPlayer(this.GetMostRecentWhisperedPlayer());
      W8Touch.Get().VirtualKeyboardDidShow += new Action(this.OnKeyboardShow);
      W8Touch.Get().VirtualKeyboardDidHide += new Action(this.OnKeyboardHide);
    }
    TransformUtil.SetPoint((Component) this.m_ChatBubbleInfo.m_Parent, Anchor.BOTTOM_LEFT, BnetBarFriendButton.Get().gameObject, Anchor.BOTTOM_RIGHT, (Vector3) new Vector2(0.0f, (float) (Screen.height - 150)));
    int count = this.m_chatBubbleFrames.Count;
    if (count == 0)
      return;
    Component dst = (Component) this.m_ChatBubbleInfo.m_Parent;
    for (int index = count - 1; index >= 0; --index)
    {
      ChatBubbleFrame chatBubbleFrame = this.m_chatBubbleFrames[index];
      TransformUtil.SetPoint((Component) chatBubbleFrame, Anchor.TOP_LEFT, dst, Anchor.BOTTOM_LEFT, Vector3.zero);
      dst = (Component) chatBubbleFrame;
    }
  }

  public void OnKeyboardHide()
  {
    this.UpdateLayout();
    this.UpdateChatBubbleLayout();
  }

  private class PlayerChatInfoChangedListener : EventListener<ChatMgr.PlayerChatInfoChangedCallback>
  {
    public void Fire(PlayerChatInfo chatInfo)
    {
      this.m_callback(chatInfo, this.m_userData);
    }
  }

  private enum KeyboardState
  {
    None,
    Below,
    Above,
  }

  public delegate void PlayerChatInfoChangedCallback(PlayerChatInfo chatInfo, object userData);
}
