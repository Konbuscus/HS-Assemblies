﻿// Decompiled with JetBrains decompiler
// Type: CardBackDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CardBackDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private long m_Data1;
  [SerializeField]
  private string m_Source;
  [SerializeField]
  private bool m_Enabled;
  [SerializeField]
  private int m_SortCategory;
  [SerializeField]
  private int m_SortOrder;
  [SerializeField]
  private string m_PrefabName;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private DbfLocValue m_Description;
  [SerializeField]
  private DbfLocValue m_SourceDescription;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("DATA1", "depends on source")]
  public long Data1
  {
    get
    {
      return this.m_Data1;
    }
  }

  [DbfField("SOURCE", "how is this card back earned?")]
  public string Source
  {
    get
    {
      return this.m_Source;
    }
  }

  [DbfField("ENABLED", "")]
  public bool Enabled
  {
    get
    {
      return this.m_Enabled;
    }
  }

  [DbfField("SORT_CATEGORY", "categorical display order of this card back in the collection manager")]
  public int SortCategory
  {
    get
    {
      return this.m_SortCategory;
    }
  }

  [DbfField("SORT_ORDER", "display order of this card back within its SORT_CATEGORY in the collection manager")]
  public int SortOrder
  {
    get
    {
      return this.m_SortOrder;
    }
  }

  [DbfField("PREFAB_NAME", "")]
  public string PrefabName
  {
    get
    {
      return this.m_PrefabName;
    }
  }

  [DbfField("NAME", "")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("DESCRIPTION", "")]
  public DbfLocValue Description
  {
    get
    {
      return this.m_Description;
    }
  }

  [DbfField("SOURCE_DESCRIPTION", "")]
  public DbfLocValue SourceDescription
  {
    get
    {
      return this.m_SourceDescription;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    CardBackDbfAsset cardBackDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (CardBackDbfAsset)) as CardBackDbfAsset;
    if ((UnityEngine.Object) cardBackDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("CardBackDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < cardBackDbfAsset.Records.Count; ++index)
      cardBackDbfAsset.Records[index].StripUnusedLocales();
    records = (object) cardBackDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Name.StripUnusedLocales();
    this.m_Description.StripUnusedLocales();
    this.m_SourceDescription.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetData1(long v)
  {
    this.m_Data1 = v;
  }

  public void SetSource(string v)
  {
    this.m_Source = v;
  }

  public void SetEnabled(bool v)
  {
    this.m_Enabled = v;
  }

  public void SetSortCategory(int v)
  {
    this.m_SortCategory = v;
  }

  public void SetSortOrder(int v)
  {
    this.m_SortOrder = v;
  }

  public void SetPrefabName(string v)
  {
    this.m_PrefabName = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetDescription(DbfLocValue v)
  {
    this.m_Description = v;
    v.SetDebugInfo(this.ID, "DESCRIPTION");
  }

  public void SetSourceDescription(DbfLocValue v)
  {
    this.m_SourceDescription = v;
    v.SetDebugInfo(this.ID, "SOURCE_DESCRIPTION");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CardBackDbfRecord.\u003C\u003Ef__switch\u0024map1D == null)
      {
        // ISSUE: reference to a compiler-generated field
        CardBackDbfRecord.\u003C\u003Ef__switch\u0024map1D = new Dictionary<string, int>(11)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "DATA1",
            2
          },
          {
            "SOURCE",
            3
          },
          {
            "ENABLED",
            4
          },
          {
            "SORT_CATEGORY",
            5
          },
          {
            "SORT_ORDER",
            6
          },
          {
            "PREFAB_NAME",
            7
          },
          {
            "NAME",
            8
          },
          {
            "DESCRIPTION",
            9
          },
          {
            "SOURCE_DESCRIPTION",
            10
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CardBackDbfRecord.\u003C\u003Ef__switch\u0024map1D.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Data1;
          case 3:
            return (object) this.Source;
          case 4:
            return (object) this.Enabled;
          case 5:
            return (object) this.SortCategory;
          case 6:
            return (object) this.SortOrder;
          case 7:
            return (object) this.PrefabName;
          case 8:
            return (object) this.Name;
          case 9:
            return (object) this.Description;
          case 10:
            return (object) this.SourceDescription;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (CardBackDbfRecord.\u003C\u003Ef__switch\u0024map1E == null)
    {
      // ISSUE: reference to a compiler-generated field
      CardBackDbfRecord.\u003C\u003Ef__switch\u0024map1E = new Dictionary<string, int>(11)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "DATA1",
          2
        },
        {
          "SOURCE",
          3
        },
        {
          "ENABLED",
          4
        },
        {
          "SORT_CATEGORY",
          5
        },
        {
          "SORT_ORDER",
          6
        },
        {
          "PREFAB_NAME",
          7
        },
        {
          "NAME",
          8
        },
        {
          "DESCRIPTION",
          9
        },
        {
          "SOURCE_DESCRIPTION",
          10
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!CardBackDbfRecord.\u003C\u003Ef__switch\u0024map1E.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetData1((long) val);
        break;
      case 3:
        this.SetSource((string) val);
        break;
      case 4:
        this.SetEnabled((bool) val);
        break;
      case 5:
        this.SetSortCategory((int) val);
        break;
      case 6:
        this.SetSortOrder((int) val);
        break;
      case 7:
        this.SetPrefabName((string) val);
        break;
      case 8:
        this.SetName((DbfLocValue) val);
        break;
      case 9:
        this.SetDescription((DbfLocValue) val);
        break;
      case 10:
        this.SetSourceDescription((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CardBackDbfRecord.\u003C\u003Ef__switch\u0024map1F == null)
      {
        // ISSUE: reference to a compiler-generated field
        CardBackDbfRecord.\u003C\u003Ef__switch\u0024map1F = new Dictionary<string, int>(11)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "DATA1",
            2
          },
          {
            "SOURCE",
            3
          },
          {
            "ENABLED",
            4
          },
          {
            "SORT_CATEGORY",
            5
          },
          {
            "SORT_ORDER",
            6
          },
          {
            "PREFAB_NAME",
            7
          },
          {
            "NAME",
            8
          },
          {
            "DESCRIPTION",
            9
          },
          {
            "SOURCE_DESCRIPTION",
            10
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CardBackDbfRecord.\u003C\u003Ef__switch\u0024map1F.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (long);
          case 3:
            return typeof (string);
          case 4:
            return typeof (bool);
          case 5:
            return typeof (int);
          case 6:
            return typeof (int);
          case 7:
            return typeof (string);
          case 8:
            return typeof (DbfLocValue);
          case 9:
            return typeof (DbfLocValue);
          case 10:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
