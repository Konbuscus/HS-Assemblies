﻿// Decompiled with JetBrains decompiler
// Type: PlayerChatInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;

public class PlayerChatInfo
{
  private BnetPlayer m_player;
  private float m_lastFocusTime;
  private BnetWhisper m_lastSeenWhisper;

  public BnetPlayer GetPlayer()
  {
    return this.m_player;
  }

  public void SetPlayer(BnetPlayer player)
  {
    this.m_player = player;
  }

  public float GetLastFocusTime()
  {
    return this.m_lastFocusTime;
  }

  public void SetLastFocusTime(float time)
  {
    this.m_lastFocusTime = time;
  }

  public BnetWhisper GetLastSeenWhisper()
  {
    return this.m_lastSeenWhisper;
  }

  public void SetLastSeenWhisper(BnetWhisper whisper)
  {
    this.m_lastSeenWhisper = whisper;
  }
}
