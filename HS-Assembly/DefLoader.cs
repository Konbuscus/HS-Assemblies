﻿// Decompiled with JetBrains decompiler
// Type: DefLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class DefLoader
{
  private Map<string, EntityDef> m_entityDefCache = new Map<string, EntityDef>();
  private Map<string, CardDef> m_cachedCardDefs = new Map<string, CardDef>();
  private static DefLoader s_instance;
  private bool m_loadedEntityDefs;
  private GameObject m_placeholderCardPrefab;
  private bool m_isPlaying;

  public static DefLoader Get()
  {
    if (DefLoader.s_instance != null && DefLoader.s_instance.m_isPlaying != Application.isPlaying)
      DefLoader.s_instance = (DefLoader) null;
    if (DefLoader.s_instance == null)
    {
      DefLoader.s_instance = new DefLoader();
      DefLoader.s_instance.m_isPlaying = Application.isPlaying;
      if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
        ApplicationMgr.Get().WillReset += new Action(DefLoader.s_instance.WillReset);
      else if (Application.isPlaying)
        Log.All.PrintWarning("DefLoader being initialized before ApplicationMgr is initialized! This is very bad if you're running the game!");
      if ((UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null)
        SceneMgr.Get().RegisterSceneUnloadedEvent(new SceneMgr.SceneUnloadedCallback(DefLoader.s_instance.OnSceneUnloaded));
      else if (Application.isPlaying)
        Log.All.PrintWarning("DefLoader being initialized before SceneMgr is initialized! This is very bad if you're running the game!");
    }
    return DefLoader.s_instance;
  }

  public void Initialize()
  {
    this.LoadAllEntityDefs();
  }

  public void Clear()
  {
    this.ClearEntityDefs();
    this.ClearCardDefs();
  }

  public bool HasDef(GameObject go)
  {
    return this.HasCardDef(go);
  }

  public Map<string, EntityDef> GetAllEntityDefs()
  {
    return this.m_entityDefCache;
  }

  public void ClearEntityDefs()
  {
    this.m_entityDefCache.Clear();
    this.m_loadedEntityDefs = false;
  }

  public EntityDef GetEntityDef(string cardId)
  {
    if (string.IsNullOrEmpty(cardId))
      return (EntityDef) null;
    EntityDef entityDef1 = (EntityDef) null;
    this.m_entityDefCache.TryGetValue(cardId, out entityDef1);
    if (entityDef1 == null)
    {
      if (ApplicationMgr.UseDevWorkarounds())
      {
        Debug.LogErrorFormat("DefLoader.GetEntityDef() - Failed to load {0}. Loading {1} instead.", new object[2]
        {
          (object) cardId,
          (object) "PlaceholderCard"
        });
        EntityDef entityDef2;
        this.m_entityDefCache.TryGetValue("PlaceholderCard", out entityDef2);
        if (entityDef2 == null)
        {
          Error.AddDevFatal("DefLoader.GetEntityDef() - Failed to load {0} in place of {1}", (object) "PlaceholderCard", (object) cardId);
          return (EntityDef) null;
        }
        entityDef1 = entityDef2.Clone();
        entityDef1.SetCardId(cardId);
        this.m_entityDefCache[cardId] = entityDef1;
      }
      else
        Error.AddDevFatal("DefLoader.GetEntityDef() - Failed to load {0}", (object) cardId);
    }
    return entityDef1;
  }

  public EntityDef GetEntityDef(int dbId)
  {
    string cardId = GameUtils.TranslateDbIdToCardId(dbId);
    if (cardId != null)
      return this.GetEntityDef(cardId);
    Debug.LogErrorFormat("DefLoader.GetEntityDef() - dbId {0} does not map to a cardId", (object) dbId);
    return (EntityDef) null;
  }

  public void LoadAllEntityDefs()
  {
    int errors = 0;
    this.m_entityDefCache = AssetLoader.Get().LoadBatchCardXmls(GameUtils.GetAllCardIds(), out errors);
    this.m_loadedEntityDefs = true;
    if (errors <= 0)
      return;
    Error.AddDevWarning("Missing Cards", "Failed to load {0} card(s) on startup.  Proceed with caution.  Check errors in the console for more details.", (object) errors);
  }

  public bool HasLoadedEntityDefs()
  {
    return this.m_loadedEntityDefs;
  }

  public bool HasCardDef(GameObject go)
  {
    CardDef componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<CardDef>(go);
    if ((UnityEngine.Object) componentInThisOrParents == (UnityEngine.Object) null)
      return false;
    return this.m_cachedCardDefs.ContainsValue(componentInThisOrParents);
  }

  public void ClearCardDef(string cardID)
  {
    if (!this.m_cachedCardDefs.ContainsKey(cardID))
      return;
    CardDef cachedCardDef = this.m_cachedCardDefs[cardID];
    this.m_cachedCardDefs.Remove(cardID);
    UnityEngine.Object.Destroy((UnityEngine.Object) cachedCardDef.gameObject);
  }

  public void ClearCardDefs()
  {
    if (this.m_cachedCardDefs == null)
      return;
    using (Map<string, CardDef>.ValueCollection.Enumerator enumerator = this.m_cachedCardDefs.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardDef current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null && (UnityEngine.Object) current.gameObject != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_cachedCardDefs.Clear();
  }

  public void LoadCardDef(string cardId, DefLoader.LoadDefCallback<CardDef> callback, object userData = null, CardPortraitQuality quality = null)
  {
    CardDef cardDef = this.GetCardDef(cardId, quality);
    callback(cardId, cardDef, userData);
  }

  public CardDef GetCardDef(int dbId)
  {
    string cardId = GameUtils.TranslateDbIdToCardId(dbId);
    if (cardId != null)
      return this.GetCardDef(cardId, (CardPortraitQuality) null);
    Debug.LogError((object) string.Format("DefLoader.GetCardDef() - dbId {0} does not map to a cardId", (object) dbId));
    return (CardDef) null;
  }

  public CardDef GetCardDef(string cardId, CardPortraitQuality quality = null)
  {
    if (string.IsNullOrEmpty(cardId))
      return (CardDef) null;
    if ((UnityEngine.Object) AssetLoader.Get() == (UnityEngine.Object) null)
      return (CardDef) null;
    if (quality == null)
      quality = CardPortraitQuality.GetDefault();
    if (true)
      quality.TextureQuality = 3;
    CardDef cardDef = (CardDef) null;
    CardDef def;
    if (this.m_cachedCardDefs.TryGetValue(cardId, out def))
    {
      cardDef = def;
      if (CardPortraitQuality.GetFromDef(def) >= quality)
        return def;
    }
    if ((UnityEngine.Object) cardDef == (UnityEngine.Object) null)
    {
      GameObject gameObject1 = AssetLoader.Get().LoadCardPrefab(cardId, true, false);
      if ((bool) ((UnityEngine.Object) gameObject1))
        cardDef = gameObject1.GetComponent<CardDef>();
      if ((UnityEngine.Object) cardDef == (UnityEngine.Object) null)
      {
        if ((bool) ((UnityEngine.Object) gameObject1))
          UnityEngine.Object.Destroy((UnityEngine.Object) gameObject1);
        if (ApplicationMgr.UseDevWorkarounds())
        {
          Debug.LogErrorFormat("DefLoader.GetCardDef() - Failed to load {0}. Loading {1} instead.", new object[2]
          {
            (object) cardId,
            (object) "PlaceholderCard"
          });
          GameObject gameObject2 = this.LoadPlaceholderCardPrefab();
          if ((UnityEngine.Object) gameObject2 == (UnityEngine.Object) null)
          {
            Error.AddDevFatal("DefLoader.GetCardDef() - Failed to load {0} in place of {1}", (object) "PlaceholderCard", (object) cardId);
            return (CardDef) null;
          }
          cardDef = gameObject2.GetComponent<CardDef>();
        }
        else
        {
          Error.AddDevFatal("DefLoader.GetCardDef() - Failed to load {0}", (object) cardId);
          return (CardDef) null;
        }
      }
      this.m_cachedCardDefs.Add(cardId, cardDef);
    }
    this.UpdateCardAssets(cardDef, quality);
    return cardDef;
  }

  private GameObject LoadPlaceholderCardPrefab()
  {
    if ((UnityEngine.Object) this.m_placeholderCardPrefab != (UnityEngine.Object) null)
      return this.m_placeholderCardPrefab;
    this.m_placeholderCardPrefab = AssetLoader.Get().LoadCardPrefab("PlaceholderCard", true, false);
    if (!((UnityEngine.Object) this.m_placeholderCardPrefab == (UnityEngine.Object) null))
      return this.m_placeholderCardPrefab;
    Debug.LogErrorFormat("DefLoader.LoadPlaceholderCardPrefab() - Failed to load {0}", (object) "PlaceholderCard");
    return (GameObject) null;
  }

  private static string GetTextureName(string path, int quality)
  {
    switch (quality)
    {
      case 1:
      case 2:
        int length = path.LastIndexOf('/');
        return string.Format("{0}/LowResPortrait/{1}", (object) path.Substring(0, length), (object) path.Substring(length + 1));
      case 3:
        return path;
      default:
        Debug.LogError((object) "Invalid texture quality value.");
        return string.Empty;
    }
  }

  private void UpdateCardAssets(CardDef cardDef, CardPortraitQuality quality)
  {
    CardPortraitQuality portraitQuality = cardDef.GetPortraitQuality();
    if (quality <= portraitQuality || string.IsNullOrEmpty(cardDef.m_PortraitTexturePath))
      return;
    if (portraitQuality.TextureQuality < quality.TextureQuality)
    {
      Texture portrait = AssetLoader.Get().LoadCardTexture(DefLoader.GetTextureName(cardDef.m_PortraitTexturePath, quality.TextureQuality), false);
      if ((UnityEngine.Object) portrait == (UnityEngine.Object) null)
      {
        Error.AddDevFatal("DefLoader.UpdateCardAssets() - Failed to load portrait texture {0} for card {1}", (object) cardDef.m_PortraitTexturePath, (object) cardDef);
        return;
      }
      cardDef.OnPortraitLoaded(portrait, quality.TextureQuality);
    }
    if ((!quality.LoadPremium || portraitQuality.LoadPremium) && !cardDef.m_AlwaysRenderPremiumPortrait || string.IsNullOrEmpty(cardDef.m_PremiumPortraitMaterialPath))
      return;
    Material material = AssetLoader.Get().LoadPremiumMaterial(cardDef.m_PremiumPortraitMaterialPath, false);
    Texture portrait1 = (Texture) null;
    if ((UnityEngine.Object) material == (UnityEngine.Object) null)
    {
      Error.AddDevFatalUnlessWorkarounds("DefLoader.UpdateCardAssets() - Failed to load premium material {0} for card {1}", (object) cardDef.m_PremiumPortraitMaterialPath, (object) cardDef);
    }
    else
    {
      if (!string.IsNullOrEmpty(cardDef.m_PremiumPortraitTexturePath))
      {
        portrait1 = AssetLoader.Get().LoadCardTexture(cardDef.m_PremiumPortraitTexturePath, false);
        if ((UnityEngine.Object) portrait1 == (UnityEngine.Object) null)
        {
          Error.AddDevFatal("DefLoader.UpdateCardAssets() - Failed to load premium portrait texture {0} for card {1}", (object) cardDef.m_PremiumPortraitTexturePath, (object) cardDef);
          return;
        }
      }
      cardDef.OnPremiumMaterialLoaded(material, portrait1);
    }
  }

  private void WillReset()
  {
    this.ClearEntityDefs();
  }

  private void OnSceneUnloaded(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    this.ClearCardDefs();
  }

  public void LoadFullDef(string cardId, DefLoader.LoadDefCallback<FullDef> callback)
  {
    this.LoadFullDef(cardId, callback, (object) null);
  }

  public FullDef GetFullDef(string cardId, CardPortraitQuality quality = null)
  {
    EntityDef entityDef = this.GetEntityDef(cardId);
    CardDef cardDef = this.GetCardDef(cardId, quality);
    FullDef fullDef = new FullDef();
    fullDef.SetEntityDef(entityDef);
    fullDef.SetCardDef(cardDef);
    return fullDef;
  }

  public void LoadFullDef(string cardId, DefLoader.LoadDefCallback<FullDef> callback, object userData)
  {
    callback(cardId, this.GetFullDef(cardId, (CardPortraitQuality) null), userData);
  }

  public delegate void LoadDefCallback<T>(string cardId, T def, object userData);
}
