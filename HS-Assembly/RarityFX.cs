﻿// Decompiled with JetBrains decompiler
// Type: RarityFX
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public struct RarityFX
{
  public GameObject burstFX;
  public GameObject explodeFX;
  public Material glowBallMat;
  public Material glowTrailMat;
}
