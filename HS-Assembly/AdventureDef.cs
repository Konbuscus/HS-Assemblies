﻿// Decompiled with JetBrains decompiler
// Type: AdventureDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class AdventureDef : MonoBehaviour
{
  [CustomEditField(Sections = "Chooser Button")]
  public Vector2 m_TextureTiling = Vector2.one;
  [CustomEditField(Sections = "Chooser Button")]
  public Vector2 m_TextureOffset = Vector2.zero;
  private Map<AdventureModeDbId, AdventureSubDef> m_SubDefs = new Map<AdventureModeDbId, AdventureSubDef>();
  [CustomEditField(Sections = "Reward Banners")]
  public AdventureDef.BannerRewardType m_BannerRewardType;
  [CustomEditField(Sections = "Reward Banners", T = EditType.GAME_OBJECT)]
  public string m_BannerRewardPrefab;
  [CustomEditField(Sections = "Reward Banners", T = EditType.GAME_OBJECT)]
  public string m_AdventureCompleteQuotePrefab;
  [CustomEditField(Sections = "Reward Banners")]
  public string m_AdventureCompleteQuoteVOLine;
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public String_MobileOverride m_ProgressDisplayPrefab;
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public string m_WingBottomBorderPrefab;
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public string m_DefaultQuotePrefab;
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public string m_ChooserButtonPrefab;
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public string m_ChooserSubButtonPrefab;
  [CustomEditField(Sections = "Chooser Button", T = EditType.TEXTURE)]
  public string m_Texture;
  private AdventureDbId m_AdventureId;
  private string m_AdventureName;
  private int m_SortOrder;

  public void Init(AdventureDbfRecord advRecord, List<AdventureDataDbfRecord> advDataRecords)
  {
    this.m_AdventureId = (AdventureDbId) advRecord.ID;
    this.m_AdventureName = (string) advRecord.Name;
    this.m_SortOrder = advRecord.SortOrder;
    using (List<AdventureDataDbfRecord>.Enumerator enumerator = advDataRecords.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureDataDbfRecord current = enumerator.Current;
        if ((AdventureDbId) current.AdventureId == this.m_AdventureId)
        {
          string adventureSubDefPrefab = current.AdventureSubDefPrefab;
          if (!string.IsNullOrEmpty(adventureSubDefPrefab))
          {
            GameObject gameObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(adventureSubDefPrefab), true, false);
            if (!((UnityEngine.Object) gameObject == (UnityEngine.Object) null))
            {
              AdventureSubDef component = gameObject.GetComponent<AdventureSubDef>();
              if ((UnityEngine.Object) component == (UnityEngine.Object) null)
              {
                Debug.LogError((object) string.Format("{0} object does not contain AdventureSubDef component.", (object) adventureSubDefPrefab));
                UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
              }
              else
              {
                component.Init(current);
                this.m_SubDefs.Add(component.GetAdventureModeId(), component);
              }
            }
          }
        }
      }
    }
  }

  public AdventureDbId GetAdventureId()
  {
    return this.m_AdventureId;
  }

  public string GetAdventureName()
  {
    return this.m_AdventureName;
  }

  public AdventureSubDef GetSubDef(AdventureModeDbId modeId)
  {
    AdventureSubDef adventureSubDef = (AdventureSubDef) null;
    this.m_SubDefs.TryGetValue(modeId, out adventureSubDef);
    return adventureSubDef;
  }

  public List<AdventureSubDef> GetSortedSubDefs()
  {
    List<AdventureSubDef> adventureSubDefList = new List<AdventureSubDef>((IEnumerable<AdventureSubDef>) this.m_SubDefs.Values);
    adventureSubDefList.Sort((Comparison<AdventureSubDef>) ((l, r) => l.GetSortOrder() - r.GetSortOrder()));
    return adventureSubDefList;
  }

  public int GetSortOrder()
  {
    return this.m_SortOrder;
  }

  public bool IsActiveAndPlayable()
  {
    using (List<WingDbfRecord>.Enumerator enumerator = GameDbf.Wing.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WingDbfRecord current = enumerator.Current;
        if ((AdventureDbId) current.AdventureId == this.GetAdventureId() && AdventureProgressMgr.Get().IsWingOpen(current.ID))
          return true;
      }
    }
    return false;
  }

  public enum BannerRewardType
  {
    AdventureCompleteReward,
    BannerManagerPopup,
  }
}
