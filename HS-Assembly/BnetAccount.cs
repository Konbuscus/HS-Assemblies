﻿// Decompiled with JetBrains decompiler
// Type: BnetAccount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;

public class BnetAccount
{
  private BnetAccountId m_id;
  private string m_fullName;
  private BnetBattleTag m_battleTag;
  private ulong m_lastOnlineMicrosec;
  private bool m_away;
  private ulong m_awayTimeMicrosec;
  private bool m_busy;

  public static bool operator ==(BnetAccount a, BnetAccount b)
  {
    if (object.ReferenceEquals((object) a, (object) b))
      return true;
    if ((object) a == null || (object) b == null)
      return false;
    return (BnetEntityId) a.m_id == (BnetEntityId) b.m_id;
  }

  public static bool operator !=(BnetAccount a, BnetAccount b)
  {
    return !(a == b);
  }

  public BnetAccount Clone()
  {
    BnetAccount bnetAccount = (BnetAccount) this.MemberwiseClone();
    if ((BnetEntityId) this.m_id != (BnetEntityId) null)
      bnetAccount.m_id = this.m_id.Clone();
    if (this.m_battleTag != (BnetBattleTag) null)
      bnetAccount.m_battleTag = this.m_battleTag.Clone();
    return bnetAccount;
  }

  public BnetAccountId GetId()
  {
    return this.m_id;
  }

  public void SetId(BnetAccountId id)
  {
    this.m_id = id;
  }

  public string GetFullName()
  {
    return this.m_fullName;
  }

  public void SetFullName(string fullName)
  {
    this.m_fullName = fullName;
  }

  public BnetBattleTag GetBattleTag()
  {
    return this.m_battleTag;
  }

  public void SetBattleTag(BnetBattleTag battleTag)
  {
    this.m_battleTag = battleTag;
  }

  public ulong GetLastOnlineMicrosec()
  {
    return this.m_lastOnlineMicrosec;
  }

  public void SetLastOnlineMicrosec(ulong microsec)
  {
    this.m_lastOnlineMicrosec = microsec;
  }

  public bool IsAway()
  {
    return this.m_away;
  }

  public void SetAway(bool away)
  {
    this.m_away = away;
  }

  public ulong GetAwayTimeMicrosec()
  {
    return this.m_awayTimeMicrosec;
  }

  public void SetAwayTimeMicrosec(ulong awayTimeMicrosec)
  {
    this.m_awayTimeMicrosec = awayTimeMicrosec;
  }

  public bool IsBusy()
  {
    return this.m_busy;
  }

  public void SetBusy(bool busy)
  {
    this.m_busy = busy;
  }

  public override bool Equals(object obj)
  {
    if (obj == null)
      return false;
    BnetAccount bnetAccount = obj as BnetAccount;
    if ((object) bnetAccount == null)
      return false;
    return this.m_id.Equals((BnetEntityId) bnetAccount.m_id);
  }

  public bool Equals(BnetAccountId other)
  {
    if (other == null)
      return false;
    return this.m_id.Equals((BnetEntityId) other);
  }

  public override int GetHashCode()
  {
    return this.m_id.GetHashCode();
  }

  public override string ToString()
  {
    if ((BnetEntityId) this.m_id == (BnetEntityId) null)
      return "UNKNOWN ACCOUNT";
    return string.Format("[id={0} m_fullName={1} battleTag={2} lastOnline={3}]", (object) this.m_id, (object) this.m_fullName, (object) this.m_battleTag, (object) TimeUtils.ConvertEpochMicrosecToDateTime(this.m_lastOnlineMicrosec));
  }
}
