﻿// Decompiled with JetBrains decompiler
// Type: MinionPieces
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class MinionPieces
{
  public GameObject m_main;
  public GameObject m_premium;
  public GameObject m_taunt;
  public GameObject m_legendary;
}
