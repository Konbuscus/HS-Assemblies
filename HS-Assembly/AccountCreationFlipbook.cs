﻿// Decompiled with JetBrains decompiler
// Type: AccountCreationFlipbook
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class AccountCreationFlipbook
{
  public float m_acFlipbookTimeAlt = 120f;
  public float m_acFlipbookTimeMin = 180f;
  public float m_acFlipbookTimeMax = 240f;
  public GameObject m_acFlipbook;
  public Texture[] m_acFlipbookTextures;
}
