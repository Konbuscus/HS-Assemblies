﻿// Decompiled with JetBrains decompiler
// Type: ActorState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ActorState : MonoBehaviour
{
  public ActorStateType m_StateType;
  public List<ActorStateAnimObject> m_ExternalAnimatedObjects;
  private ActorStateMgr m_stateMgr;
  private bool m_playing;
  private bool m_initialized;

  private void Start()
  {
    this.m_stateMgr = SceneUtils.FindComponentInParents<ActorStateMgr>(this.gameObject);
    using (List<ActorStateAnimObject>.Enumerator enumerator = this.m_ExternalAnimatedObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Init();
    }
    this.m_initialized = true;
    if (!this.m_playing)
      return;
    this.gameObject.SetActive(true);
    this.PlayNow();
  }

  public void Play()
  {
    if (this.m_playing)
      return;
    this.m_playing = true;
    if (!this.m_initialized)
      return;
    this.gameObject.SetActive(true);
    this.PlayNow();
  }

  public void Stop(List<ActorState> nextStateList)
  {
    if (!this.m_playing)
      return;
    this.m_playing = false;
    if (!this.m_initialized)
      return;
    if ((Object) this.GetComponent<Animation>() != (Object) null)
      this.GetComponent<Animation>().Stop();
    if ((Object) this.GetComponent<ParticleEmitter>() != (Object) null)
      this.GetComponent<ParticleEmitter>().emit = false;
    if (nextStateList == null)
    {
      using (List<ActorStateAnimObject>.Enumerator enumerator = this.m_ExternalAnimatedObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Stop();
      }
    }
    else
    {
      using (List<ActorStateAnimObject>.Enumerator enumerator = this.m_ExternalAnimatedObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Stop(nextStateList);
      }
    }
    this.gameObject.SetActive(false);
  }

  public float GetAnimationDuration()
  {
    float b = 0.0f;
    for (int index = 0; index < this.m_ExternalAnimatedObjects.Count; ++index)
    {
      if ((Object) this.m_ExternalAnimatedObjects[index].m_GameObject != (Object) null)
        b = Mathf.Max(this.m_ExternalAnimatedObjects[index].m_AnimClip.length, b);
    }
    return b;
  }

  public void ShowState()
  {
    this.gameObject.SetActive(true);
    this.Play();
  }

  public void HideState()
  {
    this.Stop((List<ActorState>) null);
    this.gameObject.SetActive(false);
  }

  private void OnChangeState(ActorStateType stateType)
  {
    this.m_stateMgr.ChangeState(stateType);
  }

  private void PlayNow()
  {
    if ((Object) this.GetComponent<Animation>() != (Object) null)
      this.GetComponent<Animation>().Play();
    if ((Object) this.GetComponent<ParticleEmitter>() != (Object) null)
      this.GetComponent<ParticleEmitter>().emit = true;
    using (List<ActorStateAnimObject>.Enumerator enumerator = this.m_ExternalAnimatedObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Play();
    }
  }
}
