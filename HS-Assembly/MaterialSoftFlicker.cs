﻿// Decompiled with JetBrains decompiler
// Type: MaterialSoftFlicker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (Material))]
public class MaterialSoftFlicker : MonoBehaviour
{
  public float minIntensity = 0.25f;
  public float maxIntensity = 0.5f;
  public float m_timeScale = 1f;
  public Color m_color = new Color(1f, 1f, 1f, 1f);
  private float random;

  private void Start()
  {
    this.random = Random.Range(0.0f, (float) ushort.MaxValue);
  }

  private void Update()
  {
    this.gameObject.GetComponent<Renderer>().material.SetColor("_TintColor", new Color(this.m_color.r, this.m_color.g, this.m_color.b, Mathf.Lerp(this.minIntensity, this.maxIntensity, Mathf.PerlinNoise(this.random, Time.time * this.m_timeScale))));
  }
}
