﻿// Decompiled with JetBrains decompiler
// Type: RewardCardBack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RewardCardBack : MonoBehaviour
{
  public int m_CardBackID = -1;
  private GameLayer m_layer = GameLayer.IgnoreFullScreenEffects;
  public GameObject m_cardbackBone;
  public UberText m_cardbackTitle;
  public UberText m_cardbackName;
  private bool m_Ready;
  private Actor m_actor;

  private void Awake()
  {
  }

  private void OnDestroy()
  {
    this.m_Ready = false;
  }

  public bool IsReady()
  {
    return this.m_Ready;
  }

  public void LoadCardBack(CardBackRewardData cardbackData, GameLayer layer = GameLayer.IgnoreFullScreenEffects)
  {
    this.m_layer = layer;
    this.m_CardBackID = cardbackData.CardBackID;
    CardBackManager.Get().LoadCardBackByIndex(this.m_CardBackID, new CardBackManager.LoadCardBackData.LoadCardBackCallback(this.OnCardBackLoaded), "Card_Hidden");
  }

  public void Death()
  {
    this.m_actor.ActivateSpellBirthState(SpellType.DEATH);
  }

  private void OnCardBackLoaded(CardBackManager.LoadCardBackData cardbackData)
  {
    GameObject gameObject = cardbackData.m_GameObject;
    gameObject.transform.parent = this.m_cardbackBone.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
    gameObject.transform.localScale = Vector3.one;
    SceneUtils.SetLayer(gameObject, this.m_layer);
    this.m_actor = gameObject.GetComponent<Actor>();
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_cardbackTitle.Text = "GLOBAL_SEASON_END_NEW_CARDBACK_TITLE_PHONE";
    this.m_cardbackName.Text = cardbackData.m_Name;
    this.m_Ready = true;
  }
}
