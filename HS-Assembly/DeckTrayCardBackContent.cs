﻿// Decompiled with JetBrains decompiler
// Type: DeckTrayCardBackContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

[CustomEditClass]
public class DeckTrayCardBackContent : DeckTrayContent
{
  [CustomEditField(Sections = "Animation & Sounds")]
  public iTween.EaseType m_traySlideSlideInAnimation = iTween.EaseType.easeOutBounce;
  [CustomEditField(Sections = "Animation & Sounds")]
  public float m_traySlideAnimationTime = 0.25f;
  private const string ADD_CARD_TO_DECK_SOUND = "collection_manager_card_add_to_deck_instant";
  [CustomEditField(Sections = "Positioning")]
  public GameObject m_root;
  [CustomEditField(Sections = "Positioning")]
  public Vector3 m_trayHiddenOffset;
  [CustomEditField(Sections = "Positioning")]
  public GameObject m_cardBackContainer;
  [CustomEditField(Sections = "Animation & Sounds")]
  public iTween.EaseType m_traySlideSlideOutAnimation;
  [CustomEditField(Sections = "Animation & Sounds", T = EditType.SOUND_PREFAB)]
  public string m_socketSound;
  [CustomEditField(Sections = "Card Effects")]
  public Material m_sepiaCardMaterial;
  private GameObject m_currentCardBack;
  private Vector3 m_originalLocalPosition;
  private bool m_animating;
  private bool m_waitingToLoadCardback;
  private DeckTrayCardBackContent.AnimatedCardBack m_animData;

  private void Awake()
  {
    this.m_originalLocalPosition = this.transform.localPosition;
    this.transform.localPosition = this.m_originalLocalPosition + this.m_trayHiddenOffset;
    this.m_root.SetActive(false);
  }

  public void AnimateInNewCardBack(CardBackManager.LoadCardBackData cardBackData, GameObject original)
  {
    GameObject gameObject = cardBackData.m_GameObject;
    gameObject.GetComponent<Actor>().GetSpell(SpellType.DEATHREVERSE).Reactivate();
    DeckTrayCardBackContent.AnimatedCardBack animatedCardBack = new DeckTrayCardBackContent.AnimatedCardBack();
    animatedCardBack.CardBackId = cardBackData.m_CardBackIndex;
    animatedCardBack.GameObject = gameObject;
    animatedCardBack.OriginalScale = gameObject.transform.localScale;
    animatedCardBack.OriginalPosition = original.transform.position;
    this.m_animData = animatedCardBack;
    gameObject.transform.position = new Vector3(original.transform.position.x, original.transform.position.y + 0.5f, original.transform.position.z);
    gameObject.transform.localScale = this.m_cardBackContainer.transform.lossyScale;
    Hashtable args = iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) 0.6f, (object) "easetype", (object) iTween.EaseType.easeOutCubic, (object) "onupdate", (object) "AnimateNewCardUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "oncomplete", (object) "AnimateNewCardFinished", (object) "oncompleteparams", (object) animatedCardBack, (object) "oncompletetarget", (object) this.gameObject);
    iTween.ValueTo(gameObject, args);
    SoundManager.Get().LoadAndPlay("collection_manager_card_add_to_deck_instant", this.gameObject);
  }

  private void AnimateNewCardFinished(DeckTrayCardBackContent.AnimatedCardBack cardBack)
  {
    cardBack.GameObject.transform.localScale = cardBack.OriginalScale;
    this.UpdateCardBack(cardBack.CardBackId, true, cardBack.GameObject);
    this.m_animData = (DeckTrayCardBackContent.AnimatedCardBack) null;
  }

  private void AnimateNewCardUpdate(float val)
  {
    GameObject gameObject = this.m_animData.GameObject;
    Vector3 originalPosition = this.m_animData.OriginalPosition;
    Vector3 position = this.m_cardBackContainer.transform.position;
    if ((double) val <= 0.850000023841858)
    {
      val /= 0.85f;
      gameObject.transform.position = new Vector3(Mathf.Lerp(originalPosition.x, position.x, val), (float) ((double) Mathf.Lerp(originalPosition.y, position.y, val) + (double) Mathf.Sin(val * 3.141593f) * 15.0 + (double) val * 4.0), Mathf.Lerp(originalPosition.z, position.z, val));
    }
    else
    {
      if ((UnityEngine.Object) this.m_currentCardBack != (UnityEngine.Object) null)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_currentCardBack);
        this.m_currentCardBack = (GameObject) null;
      }
      val = (float) (((double) val - 0.850000023841858) / 0.149999976158142);
      gameObject.transform.position = new Vector3(position.x, position.y + Mathf.Lerp(4f, 0.0f, val), position.z);
    }
  }

  public bool SetNewCardBack(int cardBackId, GameObject original)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTrayCardBackContent.\u003CSetNewCardBack\u003Ec__AnonStorey42D backCAnonStorey42D = new DeckTrayCardBackContent.\u003CSetNewCardBack\u003Ec__AnonStorey42D();
    // ISSUE: reference to a compiler-generated field
    backCAnonStorey42D.original = original;
    // ISSUE: reference to a compiler-generated field
    backCAnonStorey42D.\u003C\u003Ef__this = this;
    if (this.m_animData != null)
      return false;
    // ISSUE: reference to a compiler-generated method
    if (CardBackManager.Get().LoadCardBackByIndex(cardBackId, new CardBackManager.LoadCardBackData.LoadCardBackCallback(backCAnonStorey42D.\u003C\u003Em__2B0), "Card_Hidden"))
      return true;
    Debug.LogError((object) ("Could not load CardBack " + (object) cardBackId));
    return false;
  }

  public void UpdateCardBack(int cardBackId, bool assigning, GameObject obj = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTrayCardBackContent.\u003CUpdateCardBack\u003Ec__AnonStorey42E backCAnonStorey42E = new DeckTrayCardBackContent.\u003CUpdateCardBack\u003Ec__AnonStorey42E();
    // ISSUE: reference to a compiler-generated field
    backCAnonStorey42E.assigning = assigning;
    // ISSUE: reference to a compiler-generated field
    backCAnonStorey42E.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    backCAnonStorey42E.currentDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    // ISSUE: reference to a compiler-generated field
    if (backCAnonStorey42E.assigning)
    {
      if (!string.IsNullOrEmpty(this.m_socketSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_socketSound));
      // ISSUE: reference to a compiler-generated field
      backCAnonStorey42E.currentDeck.CardBackOverridden = true;
    }
    // ISSUE: reference to a compiler-generated field
    backCAnonStorey42E.currentDeck.CardBackID = cardBackId;
    if ((UnityEngine.Object) obj != (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      this.SetCardBack(obj, backCAnonStorey42E.currentDeck.CardBackOverridden, backCAnonStorey42E.assigning);
    }
    else
    {
      this.m_waitingToLoadCardback = true;
      // ISSUE: reference to a compiler-generated method
      if (CardBackManager.Get().LoadCardBackByIndex(cardBackId, new CardBackManager.LoadCardBackData.LoadCardBackCallback(backCAnonStorey42E.\u003C\u003Em__2B1), "Card_Hidden"))
        return;
      this.m_waitingToLoadCardback = false;
      Debug.LogWarning((object) string.Format("CardBackManager was unable to load card back ID: {0}", (object) cardBackId));
    }
  }

  private void SetCardBack(GameObject go, bool overriden, bool assigning)
  {
    GameUtils.SetParent(go, this.m_cardBackContainer, true);
    Actor component = go.GetComponent<Actor>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    }
    else
    {
      if (assigning)
      {
        Spell spell = component.GetSpell(SpellType.DEATHREVERSE);
        if ((UnityEngine.Object) spell != (UnityEngine.Object) null)
          spell.ActivateState(SpellStateType.BIRTH);
      }
      if ((UnityEngine.Object) this.m_currentCardBack != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_currentCardBack);
      this.m_currentCardBack = go;
      GameObject cardMesh = component.m_cardMesh;
      component.SetCardbackUpdateIgnore(true);
      component.SetUnlit();
      this.UpdateMissingEffect(component, overriden);
      if (!((UnityEngine.Object) cardMesh != (UnityEngine.Object) null))
        return;
      Material material = cardMesh.GetComponent<Renderer>().material;
      if (!material.HasProperty("_SpecularIntensity"))
        return;
      material.SetFloat("_SpecularIntensity", 0.0f);
    }
  }

  public override bool PreAnimateContentEntrance()
  {
    this.UpdateCardBack(CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing).CardBackID, false, (GameObject) null);
    return true;
  }

  public override bool AnimateContentEntranceStart()
  {
    if (this.m_waitingToLoadCardback)
      return false;
    this.m_root.SetActive(true);
    this.transform.localPosition = this.m_originalLocalPosition;
    this.m_animating = true;
    iTween.MoveFrom(this.gameObject, iTween.Hash((object) "position", (object) (this.m_originalLocalPosition + this.m_trayHiddenOffset), (object) "islocal", (object) true, (object) "time", (object) this.m_traySlideAnimationTime, (object) "easetype", (object) this.m_traySlideSlideInAnimation, (object) "oncomplete", (object) (Action<object>) (o => this.m_animating = false)));
    return true;
  }

  public override bool AnimateContentEntranceEnd()
  {
    return !this.m_animating;
  }

  public override bool AnimateContentExitStart()
  {
    this.transform.localPosition = this.m_originalLocalPosition;
    this.m_animating = true;
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) (this.m_originalLocalPosition + this.m_trayHiddenOffset), (object) "islocal", (object) true, (object) "time", (object) this.m_traySlideAnimationTime, (object) "easetype", (object) this.m_traySlideSlideOutAnimation, (object) "oncomplete", (object) (Action<object>) (o =>
    {
      this.m_animating = false;
      this.m_root.SetActive(false);
    })));
    return true;
  }

  public override bool AnimateContentExitEnd()
  {
    return !this.m_animating;
  }

  private void UpdateMissingEffect(Actor cardBackActor, bool overriden)
  {
    if ((UnityEngine.Object) cardBackActor == (UnityEngine.Object) null)
      return;
    if (overriden)
    {
      cardBackActor.DisableMissingCardEffect();
    }
    else
    {
      cardBackActor.SetMissingCardMaterial(this.m_sepiaCardMaterial);
      cardBackActor.MissingCardEffect();
    }
    cardBackActor.UpdateAllComponents();
  }

  private class AnimatedCardBack
  {
    public int CardBackId;
    public GameObject GameObject;
    public Vector3 OriginalScale;
    public Vector3 OriginalPosition;
  }
}
