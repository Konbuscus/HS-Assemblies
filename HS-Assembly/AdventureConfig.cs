﻿// Decompiled with JetBrains decompiler
// Type: AdventureConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class AdventureConfig : MonoBehaviour
{
  private AdventureDbId m_SelectedAdventure = AdventureDbId.PRACTICE;
  private AdventureModeDbId m_SelectedMode = AdventureModeDbId.NORMAL;
  private Stack<AdventureSubScenes> m_LastSubScenes = new Stack<AdventureSubScenes>();
  private List<AdventureConfig.AdventureModeChange> m_AdventureModeChangeEventList = new List<AdventureConfig.AdventureModeChange>();
  private List<AdventureConfig.SubSceneChange> m_SubSceneChangeEventList = new List<AdventureConfig.SubSceneChange>();
  private List<AdventureConfig.SelectedModeChange> m_SelectedModeChangeEventList = new List<AdventureConfig.SelectedModeChange>();
  private List<AdventureConfig.AdventureMissionSet> m_AdventureMissionSetEventList = new List<AdventureConfig.AdventureMissionSet>();
  private Map<string, int> m_WingBossesDefeatedCache = new Map<string, int>();
  private Map<string, ScenarioDbId> m_LastSelectedMissions = new Map<string, ScenarioDbId>();
  private Map<ScenarioDbId, bool> m_CachedDefeatedScenario = new Map<ScenarioDbId, bool>();
  private Map<ScenarioDbId, AdventureBossDef> m_CachedBossDef = new Map<ScenarioDbId, AdventureBossDef>();
  private Map<AdventureDbId, AdventureModeDbId> m_ClientChooserAdventureModes = new Map<AdventureDbId, AdventureModeDbId>();
  private static AdventureConfig s_instance;
  private AdventureSubScenes m_CurrentSubScene;
  private ScenarioDbId m_StartMission;

  public static AdventureConfig Get()
  {
    return AdventureConfig.s_instance;
  }

  public static AdventureSubScenes GetSubSceneFromMode(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    AdventureSubScenes adventureSubScenes = AdventureSubScenes.Chooser;
    string subscenePrefab = GameUtils.GetAdventureDataRecord((int) adventureId, (int) modeId).SubscenePrefab;
    if (subscenePrefab != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureConfig.\u003C\u003Ef__switch\u0024mapB9 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureConfig.\u003C\u003Ef__switch\u0024mapB9 = new Dictionary<string, int>(3)
        {
          {
            "Assets/Game/UIScreens/AdventurePractice",
            0
          },
          {
            "Assets/Game/UIScreens/AdventureMissionDisplay",
            1
          },
          {
            "Assets/Game/UIScreens/AdventureClassChallenge",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureConfig.\u003C\u003Ef__switch\u0024mapB9.TryGetValue(subscenePrefab, out num))
      {
        switch (num)
        {
          case 0:
            adventureSubScenes = AdventureSubScenes.Practice;
            goto label_9;
          case 1:
            adventureSubScenes = AdventureSubScenes.MissionDisplay;
            goto label_9;
          case 2:
            adventureSubScenes = AdventureSubScenes.ClassChallenge;
            goto label_9;
        }
      }
    }
    Debug.LogError((object) string.Format("Adventure sub scene asset not defined for {0}.{1}.", (object) adventureId, (object) modeId));
label_9:
    return adventureSubScenes;
  }

  public AdventureDbId GetSelectedAdventure()
  {
    return this.m_SelectedAdventure;
  }

  public AdventureModeDbId GetSelectedMode()
  {
    return this.m_SelectedMode;
  }

  public AdventureModeDbId GetClientChooserAdventureMode(AdventureDbId adventureDbId)
  {
    AdventureModeDbId adventureModeDbId;
    if (this.m_ClientChooserAdventureModes.TryGetValue(adventureDbId, out adventureModeDbId))
      return adventureModeDbId;
    if (this.m_SelectedAdventure == adventureDbId)
      return this.m_SelectedMode;
    return AdventureModeDbId.NORMAL;
  }

  public bool CanPlayMode(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureConfig.\u003CCanPlayMode\u003Ec__AnonStorey418 modeCAnonStorey418 = new AdventureConfig.\u003CCanPlayMode\u003Ec__AnonStorey418();
    // ISSUE: reference to a compiler-generated field
    modeCAnonStorey418.adventureId = adventureId;
    // ISSUE: reference to a compiler-generated field
    modeCAnonStorey418.modeId = modeId;
    bool flag = AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES);
    // ISSUE: reference to a compiler-generated field
    if (modeCAnonStorey418.adventureId == AdventureDbId.PRACTICE)
    {
      // ISSUE: reference to a compiler-generated field
      if (modeCAnonStorey418.modeId == AdventureModeDbId.EXPERT)
        return flag;
      return true;
    }
    if (!flag)
      return false;
    // ISSUE: reference to a compiler-generated field
    if (modeCAnonStorey418.modeId == AdventureModeDbId.NORMAL)
      return true;
    // ISSUE: reference to a compiler-generated method
    return GameDbf.Scenario.GetRecord(new Predicate<ScenarioDbfRecord>(modeCAnonStorey418.\u003C\u003Em__260)) != null;
  }

  public bool IsFeaturedMode(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    if (!this.CanPlayMode(adventureId, modeId) || adventureId != AdventureDbId.NAXXRAMAS || modeId != AdventureModeDbId.CLASS_CHALLENGE)
      return false;
    return !Options.Get().GetBool(Option.HAS_SEEN_NAXX_CLASS_CHALLENGE, false);
  }

  public bool MarkFeaturedMode(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    if (!this.CanPlayMode(adventureId, modeId) || adventureId != AdventureDbId.NAXXRAMAS || modeId != AdventureModeDbId.CLASS_CHALLENGE)
      return false;
    Options.Get().SetBool(Option.HAS_SEEN_NAXX_CLASS_CHALLENGE, true);
    return true;
  }

  public string GetSelectedAdventureAndModeString()
  {
    return string.Format("{0}_{1}", (object) this.m_SelectedAdventure, (object) this.m_SelectedMode);
  }

  public void SetSelectedAdventureMode(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    this.m_SelectedAdventure = adventureId;
    this.m_SelectedMode = modeId;
    this.m_ClientChooserAdventureModes[adventureId] = modeId;
    Options.Get().SetInt(Option.SELECTED_ADVENTURE, (int) this.m_SelectedAdventure);
    Options.Get().SetInt(Option.SELECTED_ADVENTURE_MODE, (int) this.m_SelectedMode);
    this.FireSelectedModeChangeEvent();
  }

  public AdventureSubScenes GetCurrentSubScene()
  {
    return this.m_CurrentSubScene;
  }

  public ScenarioDbId GetMission()
  {
    return this.m_StartMission;
  }

  public ScenarioDbId GetLastSelectedMission()
  {
    string adventureAndModeString = this.GetSelectedAdventureAndModeString();
    ScenarioDbId scenarioDbId = ScenarioDbId.INVALID;
    this.m_LastSelectedMissions.TryGetValue(adventureAndModeString, out scenarioDbId);
    return scenarioDbId;
  }

  public bool IsScenarioDefeatedAndInitCache(ScenarioDbId mission)
  {
    bool flag = AdventureProgressMgr.Get().HasDefeatedScenario((int) mission);
    if (!this.m_CachedDefeatedScenario.ContainsKey(mission))
      this.m_CachedDefeatedScenario[mission] = flag;
    return flag;
  }

  public bool IsScenarioJustDefeated(ScenarioDbId mission)
  {
    bool flag1 = AdventureProgressMgr.Get().HasDefeatedScenario((int) mission);
    bool flag2 = false;
    this.m_CachedDefeatedScenario.TryGetValue(mission, out flag2);
    this.m_CachedDefeatedScenario[mission] = flag1;
    return flag1 != flag2;
  }

  public AdventureBossDef GetBossDef(ScenarioDbId mission)
  {
    AdventureBossDef adventureBossDef = (AdventureBossDef) null;
    if (!this.m_CachedBossDef.TryGetValue(mission, out adventureBossDef))
      Debug.LogError((object) string.Format("Boss def for mission not loaded: {0}\nCall LoadBossDef first.", (object) mission));
    return adventureBossDef;
  }

  public void LoadBossDef(ScenarioDbId mission, AdventureConfig.DelBossDefLoaded callback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureConfig.\u003CLoadBossDef\u003Ec__AnonStorey419 defCAnonStorey419 = new AdventureConfig.\u003CLoadBossDef\u003Ec__AnonStorey419();
    // ISSUE: reference to a compiler-generated field
    defCAnonStorey419.callback = callback;
    // ISSUE: reference to a compiler-generated field
    defCAnonStorey419.mission = mission;
    // ISSUE: reference to a compiler-generated field
    defCAnonStorey419.\u003C\u003Ef__this = this;
    AdventureBossDef bossDef = (AdventureBossDef) null;
    // ISSUE: reference to a compiler-generated field
    if (this.m_CachedBossDef.TryGetValue(defCAnonStorey419.mission, out bossDef))
    {
      // ISSUE: reference to a compiler-generated field
      defCAnonStorey419.callback(bossDef, true);
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      string bossDefAssetPath = this.GetBossDefAssetPath(defCAnonStorey419.mission);
      if (string.IsNullOrEmpty(bossDefAssetPath))
      {
        // ISSUE: reference to a compiler-generated field
        if (defCAnonStorey419.callback == null)
          return;
        // ISSUE: reference to a compiler-generated field
        defCAnonStorey419.callback((AdventureBossDef) null, false);
      }
      else
      {
        // ISSUE: reference to a compiler-generated method
        AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(bossDefAssetPath), new AssetLoader.GameObjectCallback(defCAnonStorey419.\u003C\u003Em__261), (object) null, false);
      }
    }
  }

  public string GetBossDefAssetPath(ScenarioDbId mission)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    AdventureMissionDbfRecord record = GameDbf.AdventureMission.GetRecord(new Predicate<AdventureMissionDbfRecord>(new AdventureConfig.\u003CGetBossDefAssetPath\u003Ec__AnonStorey41A() { mission = mission }.\u003C\u003Em__262));
    if (record == null)
      return (string) null;
    return FileUtils.GameAssetPathToName(record.BossDefAssetPath);
  }

  public void ClearBossDefs()
  {
    using (Map<ScenarioDbId, AdventureBossDef>.Enumerator enumerator = this.m_CachedBossDef.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.Value);
    }
    this.m_CachedBossDef.Clear();
  }

  public void SetMission(ScenarioDbId mission, bool showDetails = true)
  {
    this.m_StartMission = mission;
    this.m_LastSelectedMissions[this.GetSelectedAdventureAndModeString()] = mission;
    foreach (AdventureConfig.AdventureMissionSet adventureMissionSet in this.m_AdventureMissionSetEventList.ToArray())
      adventureMissionSet(mission, showDetails);
  }

  public bool DoesSelectedMissionRequireDeck()
  {
    return this.DoesMissionRequireDeck(this.m_StartMission);
  }

  public bool DoesMissionRequireDeck(ScenarioDbId scenario)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord((int) scenario);
    if (record == null)
      return true;
    return record.Player1DeckId == 0;
  }

  public void AddAdventureMissionSetListener(AdventureConfig.AdventureMissionSet dlg)
  {
    this.m_AdventureMissionSetEventList.Add(dlg);
  }

  public void RemoveAdventureMissionSetListener(AdventureConfig.AdventureMissionSet dlg)
  {
    this.m_AdventureMissionSetEventList.Remove(dlg);
  }

  public void AddAdventureModeChangeListener(AdventureConfig.AdventureModeChange dlg)
  {
    this.m_AdventureModeChangeEventList.Add(dlg);
  }

  public void RemoveAdventureModeChangeListener(AdventureConfig.AdventureModeChange dlg)
  {
    this.m_AdventureModeChangeEventList.Remove(dlg);
  }

  public void AddSubSceneChangeListener(AdventureConfig.SubSceneChange dlg)
  {
    this.m_SubSceneChangeEventList.Add(dlg);
  }

  public void RemoveSubSceneChangeListener(AdventureConfig.SubSceneChange dlg)
  {
    this.m_SubSceneChangeEventList.Remove(dlg);
  }

  public void AddSelectedModeChangeListener(AdventureConfig.SelectedModeChange dlg)
  {
    this.m_SelectedModeChangeEventList.Add(dlg);
  }

  public void RemoveSelectedModeChangeListener(AdventureConfig.SelectedModeChange dlg)
  {
    this.m_SelectedModeChangeEventList.Remove(dlg);
  }

  public void ResetSubScene(AdventureSubScenes subscene)
  {
    this.m_CurrentSubScene = subscene;
    this.m_LastSubScenes.Clear();
  }

  public void ChangeSubScene(AdventureSubScenes subscene)
  {
    if (subscene == this.m_CurrentSubScene)
    {
      Debug.Log((object) string.Format("Sub scene {0} is already set.", (object) subscene));
    }
    else
    {
      this.m_LastSubScenes.Push(this.m_CurrentSubScene);
      this.m_CurrentSubScene = subscene;
      this.FireSubSceneChangeEvent(true);
      this.FireAdventureModeChangeEvent();
    }
  }

  public void ChangeToLastSubScene(bool fireevent = true)
  {
    if (this.m_LastSubScenes.Count == 0)
    {
      Debug.Log((object) "No last sub scenes were loaded.");
    }
    else
    {
      this.m_CurrentSubScene = this.m_LastSubScenes.Pop();
      if (fireevent)
        this.FireSubSceneChangeEvent(false);
      this.FireAdventureModeChangeEvent();
    }
  }

  public void ChangeSubSceneToSelectedAdventure()
  {
    this.ChangeSubScene(AdventureConfig.GetSubSceneFromMode(this.m_SelectedAdventure, this.m_SelectedMode));
  }

  public bool IsMissionAvailable(int missionId)
  {
    bool flag = AdventureProgressMgr.Get().CanPlayScenario(missionId);
    if (!flag)
      return false;
    int missionReqProgress = 0;
    int wingId = 0;
    if (!this.GetMissionPlayableParameters(missionId, ref missionReqProgress, ref wingId))
      return false;
    int ack = 0;
    AdventureProgressMgr.Get().GetWingAck(wingId, out ack);
    if (flag)
      return missionReqProgress <= ack;
    return false;
  }

  public bool IsMissionNewlyAvailableAndGetReqs(int missionId, ref int wingId, ref int missionReqProgress)
  {
    if (!this.GetMissionPlayableParameters(missionId, ref missionReqProgress, ref wingId))
      return false;
    bool flag = AdventureProgressMgr.Get().CanPlayScenario(missionId);
    int ack = 0;
    AdventureProgressMgr.Get().GetWingAck(wingId, out ack);
    return ack < missionReqProgress && flag;
  }

  public void SetWingAckIfGreater(int wingId, int ackProgress)
  {
    int ack = 0;
    AdventureProgressMgr.Get().GetWingAck(wingId, out ack);
    if (ackProgress <= ack)
      return;
    AdventureProgressMgr.Get().SetWingAck(wingId, ackProgress);
  }

  private bool GetMissionPlayableParameters(int missionId, ref int missionReqProgress, ref int wingId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureConfig.\u003CGetMissionPlayableParameters\u003Ec__AnonStorey41B parametersCAnonStorey41B = new AdventureConfig.\u003CGetMissionPlayableParameters\u003Ec__AnonStorey41B();
    // ISSUE: reference to a compiler-generated field
    parametersCAnonStorey41B.scenarioRecord = GameDbf.Scenario.GetRecord(missionId);
    // ISSUE: reference to a compiler-generated field
    if (parametersCAnonStorey41B.scenarioRecord == null)
      return false;
    // ISSUE: reference to a compiler-generated field
    WingDbfRecord record1 = GameDbf.Wing.GetRecord(parametersCAnonStorey41B.scenarioRecord.WingId);
    if (record1 == null)
      return false;
    // ISSUE: reference to a compiler-generated method
    AdventureMissionDbfRecord record2 = GameDbf.AdventureMission.GetRecord(new Predicate<AdventureMissionDbfRecord>(parametersCAnonStorey41B.\u003C\u003Em__263));
    if (record2 == null)
      return false;
    missionReqProgress = record2.ReqProgress;
    wingId = record1.ID;
    return true;
  }

  public int GetWingBossesDefeated(AdventureDbId advId, AdventureModeDbId mode, WingDbId wing, int defaultvalue = 0)
  {
    int num = 0;
    if (this.m_WingBossesDefeatedCache.TryGetValue(this.GetWingUniqueId(advId, mode, wing), out num))
      return num;
    return defaultvalue;
  }

  public void UpdateWingBossesDefeated(AdventureDbId advId, AdventureModeDbId mode, WingDbId wing, int bossesDefeated)
  {
    this.m_WingBossesDefeatedCache[this.GetWingUniqueId(advId, mode, wing)] = bossesDefeated;
  }

  private string GetWingUniqueId(AdventureDbId advId, AdventureModeDbId modeId, WingDbId wing)
  {
    return string.Format("{0}_{1}_{2}", (object) advId, (object) modeId, (object) wing);
  }

  private void Awake()
  {
    AdventureConfig.s_instance = this;
  }

  private void OnDestroy()
  {
    AdventureConfig.s_instance = (AdventureConfig) null;
  }

  public void OnAdventureSceneAwake()
  {
    this.m_SelectedAdventure = Options.Get().GetEnum<AdventureDbId>(Option.SELECTED_ADVENTURE, AdventureDbId.PRACTICE);
    this.m_SelectedMode = Options.Get().GetEnum<AdventureModeDbId>(Option.SELECTED_ADVENTURE_MODE, AdventureModeDbId.NORMAL);
    if (this.m_SelectedAdventure != AdventureDbId.RETURNING_PLAYER || ReturningPlayerMgr.Get().IsInReturningPlayerModeAndOnRails)
      return;
    Log.ReturningPlayer.Print("AdventureConfig.OnAdventureSceneAwake: No longer allowed to player the Returning Player adventure, so reassigning m_SelectedAdventure to PRACTICE.");
    this.m_SelectedAdventure = AdventureDbId.PRACTICE;
    this.m_SelectedMode = AdventureModeDbId.NORMAL;
  }

  public void OnAdventureSceneUnload()
  {
    this.m_SelectedAdventure = AdventureDbId.INVALID;
    this.m_SelectedMode = AdventureModeDbId.INVALID;
  }

  public void ResetSubScene()
  {
    this.m_CurrentSubScene = AdventureSubScenes.Chooser;
    this.m_LastSubScenes.Clear();
  }

  private void FireAdventureModeChangeEvent()
  {
    foreach (AdventureConfig.AdventureModeChange adventureModeChange in this.m_AdventureModeChangeEventList.ToArray())
      adventureModeChange(this.m_SelectedAdventure, this.m_SelectedMode);
  }

  private void FireSubSceneChangeEvent(bool forward)
  {
    this.UpdatePresence();
    foreach (AdventureConfig.SubSceneChange subSceneChange in this.m_SubSceneChangeEventList.ToArray())
      subSceneChange(this.m_CurrentSubScene, forward);
  }

  private void FireSelectedModeChangeEvent()
  {
    foreach (AdventureConfig.SelectedModeChange selectedModeChange in this.m_SelectedModeChangeEventList.ToArray())
      selectedModeChange(this.m_SelectedAdventure, this.m_SelectedMode);
  }

  public void UpdatePresence()
  {
    switch (this.m_CurrentSubScene)
    {
      case AdventureSubScenes.MissionDeckPicker:
      case AdventureSubScenes.MissionDisplay:
      case AdventureSubScenes.ClassChallenge:
        PresenceMgr.Get().SetStatus_EnteringAdventure(this.m_SelectedAdventure, this.m_SelectedMode);
        break;
      default:
        if (!((UnityEngine.Object) AdventureScene.Get() != (UnityEngine.Object) null) || AdventureScene.Get().IsUnloading())
          break;
        PresenceMgr.Get().SetStatus((Enum) PresenceStatus.ADVENTURE_CHOOSING_MODE);
        break;
    }
  }

  public delegate void DelBossDefLoaded(AdventureBossDef bossDef, bool success);

  public delegate void AdventureModeChange(AdventureDbId adventureId, AdventureModeDbId modeId);

  public delegate void AdventureMissionSet(ScenarioDbId mission, bool showDetails);

  public delegate void SubSceneChange(AdventureSubScenes newscene, bool forward);

  public delegate void SelectedModeChange(AdventureDbId adventureId, AdventureModeDbId modeId);
}
