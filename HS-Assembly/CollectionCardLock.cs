﻿// Decompiled with JetBrains decompiler
// Type: CollectionCardLock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CollectionCardLock : MonoBehaviour
{
  public GameObject m_allyBg;
  public GameObject m_spellBg;
  public GameObject m_weaponBg;
  public GameObject m_lockPlate;
  public UberText m_lockText;
  public GameObject m_lockPlateBone;
  public GameObject m_weaponLockPlateBone;

  private void Start()
  {
  }

  public void UpdateLockVisual(EntityDef entityDef, CollectionCardVisual.LockType lockType, string reason)
  {
    if (entityDef == null || lockType == CollectionCardVisual.LockType.NONE)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      this.gameObject.SetActive(true);
      TAG_CARDTYPE cardType = entityDef.GetCardType();
      this.m_allyBg.SetActive(false);
      this.m_spellBg.SetActive(false);
      this.m_weaponBg.SetActive(false);
      GameObject gameObject;
      switch (cardType)
      {
        case TAG_CARDTYPE.MINION:
          gameObject = this.m_allyBg;
          this.m_lockPlate.transform.localPosition = this.m_lockPlateBone.transform.localPosition;
          break;
        case TAG_CARDTYPE.SPELL:
          gameObject = this.m_spellBg;
          this.m_lockPlate.transform.localPosition = this.m_lockPlateBone.transform.localPosition;
          break;
        case TAG_CARDTYPE.WEAPON:
          gameObject = this.m_weaponBg;
          this.m_lockPlate.transform.localPosition = this.m_weaponLockPlateBone.transform.localPosition;
          break;
        default:
          gameObject = this.m_spellBg;
          break;
      }
      float num = 0.0f;
      switch (lockType)
      {
        case CollectionCardVisual.LockType.MAX_COPIES_IN_DECK:
          num = 0.0f;
          this.SetLockText(GameStrings.Format("GLUE_COLLECTION_LOCK_MAX_DECK_COPIES", (object) (!entityDef.IsElite() ? 2 : 1)));
          break;
        case CollectionCardVisual.LockType.NO_MORE_INSTANCES:
          num = 1f;
          this.SetLockText(GameStrings.Get("GLUE_COLLECTION_LOCK_NO_MORE_INSTANCES"));
          break;
      }
      this.SetLockText(reason);
      this.m_lockPlate.GetComponent<Renderer>().material.SetFloat("_Desaturate", num);
      gameObject.GetComponent<Renderer>().material.SetFloat("_Desaturate", num);
      gameObject.SetActive(true);
    }
  }

  public void SetLockText(string text)
  {
    this.m_lockText.Text = text;
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }
}
