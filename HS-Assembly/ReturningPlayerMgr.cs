﻿// Decompiled with JetBrains decompiler
// Type: ReturningPlayerMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using PegasusUtil;
using System;
using UnityEngine;

public class ReturningPlayerMgr
{
  private ReturningPlayerStatus m_returningPlayerProgress = ReturningPlayerStatus.RPS_NOT_RETURNING_PLAYER;
  private const int RETURNING_PLAYER_BANNER_ID = 1;
  private static ReturningPlayerMgr s_instance;
  private uint m_abTestGroup;

  public bool IsInReturningPlayerMode
  {
    get
    {
      return this.m_returningPlayerProgress == ReturningPlayerStatus.RPS_ACTIVE;
    }
  }

  public bool IsInReturningPlayerModeAndOnRails
  {
    get
    {
      if (!this.IsInReturningPlayerMode)
        return false;
      Achievement achievement = (Achievement) null;
      if (AchieveManager.Get() != null)
        achievement = AchieveManager.Get().GetAchievement(568);
      if (achievement == null)
        Log.All.PrintWarning("Attempting to check if in Returning Player, but the achievement is not available yet!");
      else if (!achievement.IsCompleted())
        return true;
      return false;
    }
  }

  public bool SuppressOldPopups
  {
    get
    {
      return this.IsInReturningPlayerModeAndOnRails;
    }
  }

  public uint AbTestGroup
  {
    get
    {
      return this.m_abTestGroup;
    }
  }

  public static void Initialize()
  {
    ReturningPlayerMgr.Get();
  }

  public static ReturningPlayerMgr Get()
  {
    if (ReturningPlayerMgr.s_instance == null)
      ReturningPlayerMgr.s_instance = new ReturningPlayerMgr();
    return ReturningPlayerMgr.s_instance;
  }

  public void SetReturningPlayerInfo(ReturningPlayerInfo info)
  {
    if (info == null)
    {
      Debug.LogError((object) "SetReturningPlayerInfo called with no ReturningPlayerInfo!");
    }
    else
    {
      this.m_returningPlayerProgress = info.Status;
      if (!info.HasAbTestGroup)
        return;
      this.m_abTestGroup = info.AbTestGroup;
    }
  }

  public bool ShowReturningPlayerWelcomeBannerIfNeeded(ReturningPlayerMgr.WelcomeBannerCloseCallback callback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ReturningPlayerMgr.\u003CShowReturningPlayerWelcomeBannerIfNeeded\u003Ec__AnonStorey3D2 neededCAnonStorey3D2 = new ReturningPlayerMgr.\u003CShowReturningPlayerWelcomeBannerIfNeeded\u003Ec__AnonStorey3D2();
    // ISSUE: reference to a compiler-generated field
    neededCAnonStorey3D2.callback = callback;
    bool flag = false;
    if (this.ShouldShowReturningPlayerWelcomeBanner())
    {
      // ISSUE: reference to a compiler-generated method
      BannerManager.Get().ShowCustomBanner("NewAdvPopUp_Welcome_Back", GameStrings.Get("GLUE_RETURNING_PLAYER_WELCOME_DESC"), new BannerManager.DelOnCloseBanner(neededCAnonStorey3D2.\u003C\u003Em__184));
      this.SetSeenReturningPlayerWelcomeBanner();
      flag = true;
    }
    return flag;
  }

  public bool ShouldShowReturningPlayerWelcomeBanner()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.LOGIN && Options.Get().GetInt(Option.RETURNING_PLAYER_BANNER_SEEN) != 1)
      return this.IsInReturningPlayerModeAndOnRails;
    return false;
  }

  public void SetSeenReturningPlayerWelcomeBanner()
  {
    if (Options.Get().GetInt(Option.RETURNING_PLAYER_BANNER_SEEN) == 1)
      return;
    Options.Get().SetInt(Option.RETURNING_PLAYER_BANNER_SEEN, 1);
    this.MakeOneTimeAccountChanges();
  }

  private void MakeOneTimeAccountChanges()
  {
    CollectionManager.Get().AutoGenerateNewDeckForPlayer();
    Options.Get().SetBool(Option.IN_RANKED_PLAY_MODE, false);
  }

  public bool PlayReturningPlayerInnkeeperGreetingIfNecessary()
  {
    if (!this.IsInReturningPlayerMode || Options.Get().GetBool(Option.HAS_HEARD_RETURNING_PLAYER_WELCOME_BACK_VO))
      return false;
    SoundManager.Get().LoadAndPlay("VO_Innkeeper_Male_Dwarf_ReturningPlayers_01");
    Options.Get().SetBool(Option.HAS_HEARD_RETURNING_PLAYER_WELCOME_BACK_VO, true);
    return true;
  }

  public bool PlayReturningPlayerInnkeeperChallengeIntroIfNecessary()
  {
    if (!this.IsInReturningPlayerMode || Options.Get().GetBool(Option.HAS_SEEN_RETURNING_PLAYER_INNKEEPER_CHALLENGE_INTRO))
      return false;
    if ((UnityEngine.Object) NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_RETURNING_PLAYER_INNKEEPER_CHALLENGE_INTRO"), "VO_Innkeeper_Male_Dwarf_ReturningPlayers_02", 0.0f, (Action) null, false) != (UnityEngine.Object) null)
      Options.Get().SetBool(Option.HAS_SEEN_RETURNING_PLAYER_INNKEEPER_CHALLENGE_INTRO, true);
    return true;
  }

  public void TEST_SetReturningPlayerProgressComplete()
  {
    this.m_returningPlayerProgress = ReturningPlayerStatus.RPS_COMPLETE;
  }

  public void Cheat_SetReturningPlayerProgress(int progress)
  {
    this.m_returningPlayerProgress = (ReturningPlayerStatus) progress;
  }

  public void Cheat_ResetReturningPlayer()
  {
    Options.Get().SetInt(Option.RETURNING_PLAYER_BANNER_SEEN, 0);
    Options.Get().SetBool(Option.HAS_HEARD_RETURNING_PLAYER_WELCOME_BACK_VO, false);
    Options.Get().SetBool(Option.HAS_SEEN_RETURNING_PLAYER_INNKEEPER_CHALLENGE_INTRO, false);
  }

  public delegate void WelcomeBannerCloseCallback();
}
