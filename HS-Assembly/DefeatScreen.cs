﻿// Decompiled with JetBrains decompiler
// Type: DefeatScreen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class DefeatScreen : EndGameScreen
{
  protected override void Awake()
  {
    base.Awake();
    if (!this.ShouldMakeUtilRequests())
      return;
    NetCache.Get().RegisterScreenEndOfGame(new NetCache.NetCacheCallback(((EndGameScreen) this).OnNetCacheReady));
  }

  protected override void ShowStandardFlow()
  {
    base.ShowStandardFlow();
    if (GameMgr.Get().IsTutorial() && !GameMgr.Get().IsSpectator())
      this.m_hitbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(((EndGameScreen) this).ContinueButtonPress_TutorialProgress));
    else
      this.m_hitbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(((EndGameScreen) this).ContinueButtonPress_PrevMode));
  }

  protected override void InitGoldRewardUI()
  {
    string challengeRewardText = EndGameScreen.GetFriendlyChallengeRewardText();
    if (string.IsNullOrEmpty(challengeRewardText))
      return;
    this.m_noGoldRewardText.gameObject.SetActive(true);
    this.m_noGoldRewardText.Text = challengeRewardText;
  }
}
