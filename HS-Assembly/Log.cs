﻿// Decompiled with JetBrains decompiler
// Type: Log
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;

public class Log
{
  public static Logger Bob = new Logger("Bob");
  public static Logger Mike = new Logger("Mike");
  public static Logger Brian = new Logger("Brian");
  public static Logger Jay = new Logger("Jay");
  public static Logger Rachelle = new Logger("Rachelle");
  public static Logger Ben = new Logger("Ben");
  public static Logger Derek = new Logger("Derek");
  public static Logger Kyle = new Logger("Kyle");
  public static Logger Cameron = new Logger("Cameron");
  public static Logger Ryan = new Logger("Ryan");
  public static Logger JMac = new Logger("JMac");
  public static Logger Yim = new Logger("Yim");
  public static Logger Becca = new Logger("Becca");
  public static Logger Henry = new Logger("Henry");
  public static Logger MikeH = new Logger("MikeH");
  public static Logger Robin = new Logger("Robin");
  public static Logger Josh = new Logger("Josh");
  public static Logger Alex = new Logger("Alex");
  public static Logger All = new Logger("All");
  public static Logger BattleNet = new Logger("BattleNet");
  public static Logger Net = new Logger("Net");
  public static Logger Packets = new Logger("Packet");
  public static Logger Power = new Logger("Power");
  public static Logger Zone = new Logger("Zone");
  public static Logger Asset = new Logger("Asset");
  public static Logger Sound = new Logger("Sound");
  public static Logger HealthyGaming = new Logger("HealthyGaming");
  public static Logger FaceDownCard = new Logger("FaceDownCard");
  public static Logger LoadingScreen = new Logger("LoadingScreen");
  public static Logger MissingAssets = new Logger("MissingAssets");
  public static Logger UpdateManager = new Logger("UpdateManager");
  public static Logger GameMgr = new Logger("GameMgr");
  public static Logger CardbackMgr = new Logger("CardbackMgr");
  public static Logger Reset = new Logger("Reset");
  public static Logger Dbf = new Logger("Dbf");
  public static Logger BIReport = new Logger("BIReport");
  public static Logger Downloader = new Logger("Downloader");
  public static Logger PlayErrors = new Logger("PlayErrors");
  public static Logger Hand = new Logger("Hand");
  public static Logger ConfigFile = new Logger("ConfigFile");
  public static Logger DeviceEmulation = new Logger("DeviceEmulation");
  public static Logger Spectator = new Logger("Spectator");
  public static Logger Party = new Logger("Party");
  public static Logger FullScreenFX = new Logger("FullScreenFX");
  public static Logger InnKeepersSpecial = new Logger("InnKeepersSpecial");
  public static Logger EventTiming = new Logger("EventTiming");
  public static Logger Arena = new Logger("Arena");
  public static Logger EndOfGame = new Logger("EndOfGame");
  public static Logger Achievements = new Logger("Achievements");
  public static Logger AdTracking = new Logger("AdTracking");
  public static Logger ClientRequestManager = new Logger("ClientRequestManager");
  public static Logger BugReporter = new Logger("BugReporter");
  public static Logger Graphics = new Logger("Graphics");
  public static Logger Store = new Logger("Store");
  public static Logger DeckTray = new Logger("DeckTray");
  public static Logger ChangedCards = new Logger("ChangedCards");
  public static Logger DeckRuleset = new Logger("DeckRuleset");
  public static Logger DeckHelper = new Logger("DeckHelper");
  public static Logger UserAttention = new Logger("UserAttention");
  public static Logger RAF = new Logger("RAF");
  public static Logger Crafting = new Logger("Crafting");
  public static Logger TavernBrawl = new Logger("TavernBrawl");
  public static Logger ReturningPlayer = new Logger("ReturningPlayer");
  public static Logger Spells = new Logger("Spells");
  public static Logger Notifications = new Logger("Notifications");
  private readonly LogInfo[] DEFAULT_LOG_INFOS = new LogInfo[0];
  private Map<string, LogInfo> m_logInfos = new Map<string, LogInfo>();
  private const string CONFIG_FILE_NAME = "log.config";
  private static Log s_instance;

  public static Log Get()
  {
    if (Log.s_instance == null)
    {
      Log.s_instance = new Log();
      Log.s_instance.Initialize();
    }
    return Log.s_instance;
  }

  public void Load()
  {
    string path = string.Format("{0}/{1}", (object) FileUtils.PersistentDataPath, (object) "log.config");
    if (File.Exists(path))
    {
      this.m_logInfos.Clear();
      this.LoadConfig(path);
    }
    foreach (LogInfo logInfo in this.DEFAULT_LOG_INFOS)
    {
      if (!this.m_logInfos.ContainsKey(logInfo.m_name))
        this.m_logInfos.Add(logInfo.m_name, logInfo);
    }
    Log.ConfigFile.Print("log.config location: " + path);
  }

  public LogInfo GetLogInfo(string name)
  {
    LogInfo logInfo = (LogInfo) null;
    this.m_logInfos.TryGetValue(name, out logInfo);
    return logInfo;
  }

  private void Initialize()
  {
    this.Load();
  }

  private void LoadConfig(string path)
  {
    global::ConfigFile configFile = new global::ConfigFile();
    if (!configFile.LightLoad(path))
      return;
    using (List<global::ConfigFile.Line>.Enumerator enumerator = configFile.GetLines().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        global::ConfigFile.Line current = enumerator.Current;
        string sectionName = current.m_sectionName;
        string lineKey = current.m_lineKey;
        string str = current.m_value;
        LogInfo logInfo;
        if (!this.m_logInfos.TryGetValue(sectionName, out logInfo))
        {
          logInfo = new LogInfo() { m_name = sectionName };
          this.m_logInfos.Add(logInfo.m_name, logInfo);
        }
        if (lineKey.Equals("ConsolePrinting", StringComparison.OrdinalIgnoreCase))
          logInfo.m_consolePrinting = GeneralUtils.ForceBool(str);
        else if (lineKey.Equals("ScreenPrinting", StringComparison.OrdinalIgnoreCase))
          logInfo.m_screenPrinting = GeneralUtils.ForceBool(str);
        else if (lineKey.Equals("FilePrinting", StringComparison.OrdinalIgnoreCase))
          logInfo.m_filePrinting = GeneralUtils.ForceBool(str);
        else if (lineKey.Equals("MinLevel", StringComparison.OrdinalIgnoreCase))
        {
          try
          {
            LogLevel logLevel = EnumUtils.GetEnum<LogLevel>(str, StringComparison.OrdinalIgnoreCase);
            logInfo.m_minLevel = logLevel;
          }
          catch (ArgumentException ex)
          {
          }
        }
        else if (lineKey.Equals("DefaultLevel", StringComparison.OrdinalIgnoreCase))
        {
          try
          {
            LogLevel logLevel = EnumUtils.GetEnum<LogLevel>(str, StringComparison.OrdinalIgnoreCase);
            logInfo.m_defaultLevel = logLevel;
          }
          catch (ArgumentException ex)
          {
          }
        }
        else if (lineKey.Equals("AlwaysPrintErrors", StringComparison.OrdinalIgnoreCase))
          logInfo.m_alwaysPrintErrors = GeneralUtils.ForceBool(str);
        else if (lineKey.Equals("Verbose", StringComparison.OrdinalIgnoreCase))
          logInfo.m_verbose = GeneralUtils.ForceBool(str);
      }
    }
  }
}
