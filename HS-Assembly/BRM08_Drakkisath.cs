﻿// Decompiled with JetBrains decompiler
// Type: BRM08_Drakkisath
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class BRM08_Drakkisath : BRM_MissionEntity
{
  private bool m_cardLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BRMA08_1_RESPONSE_04");
    this.PreloadSound("VO_BRMA08_1_CARD_05");
    this.PreloadSound("VO_BRMA08_1_TURN1_03");
    this.PreloadSound("VO_NEFARIAN_DRAKKISATH_RESPOND_48");
    this.PreloadSound("VO_BRMA08_1_TURN1_ALT_02");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA08_1_RESPONSE_04",
            m_stringTag = "VO_BRMA08_1_RESPONSE_04"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM08_Drakkisath.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator13B() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM08_Drakkisath.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator13C() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM08_Drakkisath.\u003CHandleGameOverWithTiming\u003Ec__Iterator13D() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }
}
