﻿// Decompiled with JetBrains decompiler
// Type: TooltipPanel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TooltipPanel : MonoBehaviour
{
  public static readonly PlatformDependentValue<float> HAND_SCALE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 0.65f, Phone = 0.8f };
  public static readonly PlatformDependentValue<float> GAMEPLAY_SCALE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 0.75f, Phone = 1.4f };
  public static readonly PlatformDependentValue<float> GAMEPLAY_SCALE_LARGE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 0.9f, Phone = 1.25f };
  public static PlatformDependentValue<float> BOX_SCALE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 8f, Phone = 4.5f };
  public static PlatformDependentValue<float> HISTORY_SCALE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 0.48f, Phone = 0.853f };
  public static PlatformDependentValue<float> MULLIGAN_SCALE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 0.65f, Phone = 0.4f };
  public static PlatformDependentValue<float> COLLECTION_MANAGER_SCALE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 4f, Phone = 8f };
  public static PlatformDependentValue<float> FORGE_SCALE = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 4f, Phone = 8f };
  protected Vector3 m_initialBackgroundScale = Vector3.zero;
  protected float scaleToUse = (float) TooltipPanel.GAMEPLAY_SCALE;
  public const float PACK_OPENING_SCALE = 2.75f;
  public const float UNOPENED_PACK_SCALE = 5f;
  public const float DECK_HELPER_SCALE = 3.75f;
  public const float GAMEPLAY_HERO_POWER_SCALE = 0.6f;
  public UberText m_name;
  public UberText m_body;
  public GameObject m_background;
  protected float m_initialBackgroundHeight;

  public bool Destroyed { get; private set; }

  private void Awake()
  {
    SceneUtils.SetLayer(this.gameObject, GameLayer.Tooltip);
  }

  private void OnDestroy()
  {
    Object.Destroy((Object) this.m_name);
    this.m_name = (UberText) null;
    Object.Destroy((Object) this.m_body);
    this.m_body = (UberText) null;
    Object.Destroy((Object) this.m_background);
    this.m_background = (GameObject) null;
    this.Destroyed = true;
  }

  public void Reset()
  {
    this.transform.localScale = Vector3.one;
    this.transform.eulerAngles = Vector3.zero;
  }

  public void SetScale(float newScale)
  {
    this.scaleToUse = newScale;
    this.transform.localScale = new Vector3(this.scaleToUse, this.scaleToUse, this.scaleToUse);
  }

  public virtual void Initialize(string keywordName, string keywordText)
  {
    this.SetName(keywordName);
    this.SetBodyText(keywordText);
    this.gameObject.SetActive(true);
    this.m_name.UpdateNow();
    this.m_body.UpdateNow();
  }

  public void SetName(string s)
  {
    this.m_name.Text = s;
  }

  public void SetBodyText(string s)
  {
    this.m_body.Text = s;
  }

  public virtual float GetHeight()
  {
    return this.m_background.GetComponent<Renderer>().bounds.size.z;
  }

  public virtual float GetWidth()
  {
    return this.m_background.GetComponent<Renderer>().bounds.size.x;
  }

  public bool IsTextRendered()
  {
    if (this.m_name.IsDone())
      return this.m_body.IsDone();
    return false;
  }
}
