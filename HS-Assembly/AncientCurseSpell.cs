﻿// Decompiled with JetBrains decompiler
// Type: AncientCurseSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AncientCurseSpell : SuperSpell
{
  public void DoHeroDamage()
  {
    Log.JMac.Print("AncientCurseSpell - DoHeroDamage()!");
    PowerTaskList currentTaskList = GameState.Get().GetPowerProcessor().GetCurrentTaskList();
    if (currentTaskList == null)
      Debug.LogWarning((object) "AncientCurseSpell.DoHeroDamage() called when there was no current PowerTaskList!");
    else
      GameUtils.DoDamageTasks(currentTaskList, this.GetSourceCard(), this.GetVisualTargetCard());
  }
}
