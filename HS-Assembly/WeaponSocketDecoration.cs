﻿// Decompiled with JetBrains decompiler
// Type: WeaponSocketDecoration
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class WeaponSocketDecoration : MonoBehaviour
{
  public List<WeaponSocketRequirement> m_VisibilityRequirements;

  public bool IsShown()
  {
    return this.GetComponent<Renderer>().enabled;
  }

  public void UpdateVisibility()
  {
    if (this.AreVisibilityRequirementsMet())
      this.Show();
    else
      this.Hide();
  }

  public bool AreVisibilityRequirementsMet()
  {
    Map<int, Player> playerMap = GameState.Get().GetPlayerMap();
    if (playerMap == null || this.m_VisibilityRequirements == null)
      return false;
    using (List<WeaponSocketRequirement>.Enumerator enumerator1 = this.m_VisibilityRequirements.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        WeaponSocketRequirement current1 = enumerator1.Current;
        bool flag = false;
        using (Map<int, Player>.ValueCollection.Enumerator enumerator2 = playerMap.Values.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Player current2 = enumerator2.Current;
            if (current1.m_Side == current2.GetSide())
            {
              Entity hero = current2.GetHero();
              if (hero == null)
              {
                Debug.LogWarning((object) string.Format("WeaponSocketDecoration.AreVisibilityRequirementsMet() - player {0} has no hero", (object) current2));
                return false;
              }
              if (current1.m_HasWeapon != WeaponSocketMgr.ShouldSeeWeaponSocket(hero.GetClass()))
                return false;
              flag = true;
            }
          }
        }
        if (!flag)
          return false;
      }
    }
    return true;
  }

  public void Show()
  {
    SceneUtils.EnableRenderers(this.gameObject, true);
  }

  public void Hide()
  {
    SceneUtils.EnableRenderers(this.gameObject, false);
  }
}
