﻿// Decompiled with JetBrains decompiler
// Type: TavernBrawlScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using PegasusUtil;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class TavernBrawlScene : Scene
{
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public String_MobileOverride m_CollectionManagerPrefab;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public String_MobileOverride m_TavernBrawlPrefab;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public String_MobileOverride m_TavernBrawlNoDeckPrefab;
  private bool m_unloading;
  private bool m_tavernBrawlPrefabLoaded;
  private bool m_collectionManagerNeeded;
  private bool m_collectionManagerPrefabLoaded;
  private bool m_pendingSessionBegin;

  protected override void Awake()
  {
    base.Awake();
  }

  private void Start()
  {
    Network.Get().RegisterNetHandler((object) TavernBrawlRequestSessionBeginResponse.PacketID.ID, new Network.NetHandler(this.OnSessionBeginResponse), (Network.TimeoutHandler) null);
    TavernBrawlManager.Get().EnsureAllDataReady(new TavernBrawlManager.CallbackEnsureServerDataReady(this.OnServerDataReady));
  }

  private void Update()
  {
    Network.Get().ProcessNetwork();
  }

  public override bool IsUnloading()
  {
    return this.m_unloading;
  }

  public override void Unload()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      BnetBar.Get().ToggleActive(true);
    this.m_unloading = true;
    if ((Object) CollectionManagerDisplay.Get() != (Object) null)
      CollectionManagerDisplay.Get().Unload();
    if ((Object) TavernBrawlDisplay.Get() != (Object) null)
      TavernBrawlDisplay.Get().Unload();
    Network.SendAckCardsSeen();
    StoreManager.Get().RemoveSuccessfulPurchaseAckListener(new StoreManager.SuccessfulPurchaseAckCallback(this.OnTavernBrawlTicketPurchaseAck));
    this.m_unloading = false;
  }

  private void OnServerDataReady()
  {
    if (TavernBrawlManager.Get().PlayerStatus == TavernBrawlStatus.TB_STATUS_INVALID)
    {
      UnityEngine.Debug.LogErrorFormat("TavernBrawlScene.OnServerDataReady(): don't know how to handle currentStatus={0}. Kicking to HUB", (object) TavernBrawlManager.Get().PlayerStatus);
      if (!SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
        SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_ERROR_TITLE"),
        m_text = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_ERROR"),
        m_responseCallback = (AlertPopup.ResponseCallback) ((response, userData) => TavernBrawlManager.Get().RefreshServerData()),
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_alertTextAlignment = UberText.AlignmentOptions.Center
      });
    }
    else
    {
      CollectionDeck collectionDeck = TavernBrawlManager.Get().CurrentDeck();
      if (TavernBrawlManager.Get().CurrentSession != null && collectionDeck != null)
        collectionDeck.Locked = TavernBrawlManager.Get().CurrentSession.DeckLocked;
      this.m_collectionManagerNeeded = TavernBrawlManager.Get().CurrentMission() != null && TavernBrawlManager.Get().CurrentMission().canEditDeck;
      if (this.m_collectionManagerNeeded)
      {
        AssetLoader.Get().LoadUIScreen(FileUtils.GameAssetPathToName((string) ((MobileOverrideValue<string>) this.m_TavernBrawlPrefab)), new AssetLoader.GameObjectCallback(this.OnTavernBrawlLoaded), (object) null, false);
        AssetLoader.Get().LoadUIScreen(FileUtils.GameAssetPathToName((string) ((MobileOverrideValue<string>) this.m_CollectionManagerPrefab)), new AssetLoader.GameObjectCallback(this.OnCollectionManagerLoaded), (object) null, false);
      }
      else
        AssetLoader.Get().LoadUIScreen(FileUtils.GameAssetPathToName((string) ((MobileOverrideValue<string>) this.m_TavernBrawlNoDeckPrefab)), new AssetLoader.GameObjectCallback(this.OnTavernBrawlLoaded), (object) null, false);
      if (TavernBrawlManager.Get().PlayerStatus == TavernBrawlStatus.TB_STATUS_TICKET_REQUIRED)
      {
        this.m_pendingSessionBegin = true;
        Network.RequestTavernBrawlSessionBegin();
      }
      this.StartCoroutine(this.NotifySceneLoadedWhenReady());
    }
  }

  [DebuggerHidden]
  private IEnumerator NotifySceneLoadedWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlScene.\u003CNotifySceneLoadedWhenReady\u003Ec__Iterator28B() { \u003C\u003Ef__this = this };
  }

  private void OnCollectionManagerLoaded(string name, GameObject screen, object callbackData)
  {
    this.m_collectionManagerPrefabLoaded = true;
    if (!((Object) screen == (Object) null))
      return;
    UnityEngine.Debug.LogError((object) string.Format("TavernBrawlScene.OnCollectionManagerLoaded() - failed to load screen {0}", (object) name));
  }

  private void OnTavernBrawlLoaded(string name, GameObject screen, object callbackData)
  {
    this.m_tavernBrawlPrefabLoaded = true;
    if (!((Object) screen == (Object) null))
      return;
    UnityEngine.Debug.LogError((object) string.Format("TavernBrawlScene.OnTavernBrawlLoaded() - failed to load screen {0}", (object) name));
  }

  private void OnSessionBeginResponse()
  {
    this.m_pendingSessionBegin = false;
  }

  private void OnTavernBrawlTicketPurchaseAck(Network.Bundle bundle, PaymentMethod paymentMethod, object userData)
  {
    Log.TavernBrawl.Print("TavernBrawlScene.OnTavernBrawlTicketPurchaseAck");
    if (!SceneMgr.Get().IsModeRequested(SceneMgr.Mode.TAVERN_BRAWL))
      return;
    Network.RequestTavernBrawlSessionBegin();
  }
}
