﻿// Decompiled with JetBrains decompiler
// Type: PowerHistoryInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class PowerHistoryInfo
{
  private int mEffectIndex = -1;
  private bool mShowInHistory;

  public PowerHistoryInfo()
  {
  }

  public PowerHistoryInfo(int index, bool show)
  {
    this.mEffectIndex = index;
    this.mShowInHistory = show;
  }

  public bool ShouldShowInHistory()
  {
    return this.mShowInHistory;
  }

  public int GetEffectIndex()
  {
    return this.mEffectIndex;
  }
}
