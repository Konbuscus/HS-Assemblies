﻿// Decompiled with JetBrains decompiler
// Type: BIReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using WTCG.BI;

public class BIReport : MonoBehaviour
{
  private const string BIURL = "http://iir.blizzard.com:3724/submit/WTCG";
  private const string PROTO_MESSAGE_TYPE_CLIENT_TELEMETRY = "WTCG.BI.ClientTelemetry";
  private const string PROTO_MESSAGE_TYPE_DATA_ONLY_PATCHING = "WTCG.BI.Session.DataOnlyPatching";
  private static BIReport s_instance;
  private static string s_sessionId;
  private static PresenceStatus? s_lastPresenceChangeStatus;
  private static IEnumerable<int> s_lastPresenceChangeParameters;
  private static DateTime? s_lastPresenceChangeTimeLocal;

  private void Awake()
  {
    BIReport.s_instance = this;
    this.GenerateSessionID();
    ApplicationMgr.Get().WillReset += new System.Action(this.BIReport_WillReset);
  }

  private void OnDestroy()
  {
    BIReport.s_instance = (BIReport) null;
  }

  public static BIReport Get()
  {
    return BIReport.s_instance;
  }

  private void BIReport_WillReset()
  {
    this.Report_PresenceChange(PresenceStatus.UNKNOWN, (IEnumerable<int>) null);
    BIReport.s_lastPresenceChangeStatus = new PresenceStatus?();
    BIReport.s_lastPresenceChangeParameters = (IEnumerable<int>) null;
    BIReport.s_lastPresenceChangeTimeLocal = new DateTime?();
  }

  public void Report_DataOnlyPatching(DataOnlyPatching.Status status, Locale locale, int currentBuild, int newBuild)
  {
    DataOnlyPatching.Locale locale1 = DataOnlyPatching.Locale.UnknownLocale;
    switch (locale)
    {
      case Locale.enUS:
        locale1 = DataOnlyPatching.Locale.enUS;
        break;
      case Locale.enGB:
        locale1 = DataOnlyPatching.Locale.enGB;
        break;
      case Locale.frFR:
        locale1 = DataOnlyPatching.Locale.frFR;
        break;
      case Locale.deDE:
        locale1 = DataOnlyPatching.Locale.deDE;
        break;
      case Locale.koKR:
        locale1 = DataOnlyPatching.Locale.koKR;
        break;
      case Locale.esES:
        locale1 = DataOnlyPatching.Locale.esES;
        break;
      case Locale.esMX:
        locale1 = DataOnlyPatching.Locale.esMX;
        break;
      case Locale.ruRU:
        locale1 = DataOnlyPatching.Locale.ruRU;
        break;
      case Locale.zhTW:
        locale1 = DataOnlyPatching.Locale.zhTW;
        break;
      case Locale.zhCN:
        locale1 = DataOnlyPatching.Locale.zhCN;
        break;
      case Locale.itIT:
        locale1 = DataOnlyPatching.Locale.itIT;
        break;
      case Locale.ptBR:
        locale1 = DataOnlyPatching.Locale.ptBR;
        break;
      case Locale.plPL:
        locale1 = DataOnlyPatching.Locale.plPL;
        break;
      case Locale.jaJP:
        locale1 = DataOnlyPatching.Locale.Locale15;
        break;
      case Locale.thTH:
        locale1 = DataOnlyPatching.Locale.Locale16;
        break;
    }
    DataOnlyPatching.Platform platform = DataOnlyPatching.Platform.UnknownPlatform;
    switch (Application.platform)
    {
      case RuntimePlatform.OSXEditor:
        platform = DataOnlyPatching.Platform.Mac;
        break;
      case RuntimePlatform.OSXPlayer:
        platform = DataOnlyPatching.Platform.Mac;
        break;
      case RuntimePlatform.WindowsPlayer:
        platform = DataOnlyPatching.Platform.Windows;
        break;
      case RuntimePlatform.WindowsEditor:
        platform = DataOnlyPatching.Platform.Windows;
        break;
      case RuntimePlatform.IPhonePlayer:
        platform = !(bool) UniversalInputManager.UsePhoneUI ? DataOnlyPatching.Platform.iPad : DataOnlyPatching.Platform.iPhone;
        break;
      case RuntimePlatform.Android:
        platform = !(bool) UniversalInputManager.UsePhoneUI ? DataOnlyPatching.Platform.Android_Tablet : DataOnlyPatching.Platform.Android_Phone;
        break;
    }
    DataOnlyPatching dataOnlyPatching = new DataOnlyPatching();
    dataOnlyPatching.Status_ = status;
    dataOnlyPatching.Locale_ = locale1;
    dataOnlyPatching.Platform_ = platform;
    dataOnlyPatching.BnetRegion_ = (DataOnlyPatching.BnetRegion) BattleNet.GetCurrentRegion();
    dataOnlyPatching.GameAccountId_ = BattleNet.GetMyGameAccountId().lo;
    dataOnlyPatching.CurrentBuild_ = currentBuild;
    dataOnlyPatching.NewBuild_ = newBuild;
    dataOnlyPatching.Locale_ = locale1;
    dataOnlyPatching.Platform_ = platform;
    dataOnlyPatching.BnetRegion_ = (DataOnlyPatching.BnetRegion) BattleNet.GetCurrentRegion();
    dataOnlyPatching.GameAccountId_ = BattleNet.GetMyGameAccountId().lo;
    dataOnlyPatching.CurrentBuild_ = currentBuild;
    dataOnlyPatching.NewBuild_ = newBuild;
    dataOnlyPatching.SessionId_ = BIReport.s_sessionId;
    dataOnlyPatching.DeviceUniqueIdentifier_ = SystemInfo.deviceUniqueIdentifier;
    Log.BIReport.Print("Report " + dataOnlyPatching.ToString());
    this.StartCoroutine(this.Report(ProtobufUtil.ToByteArray((IProtoBuf) dataOnlyPatching), "WTCG.BI.Session.DataOnlyPatching"));
  }

  public void TelemetryInfo(BIReport.TelemetryEvent telemetryEvent, int errorCode = 0, string message = null, constants.BnetRegion overrideBnetRegion = constants.BnetRegion.REGION_UNINITIALIZED)
  {
    this.Report_Telemetry(Telemetry.Level.LEVEL_INFO, telemetryEvent, errorCode, message, overrideBnetRegion);
  }

  public void TelemetryWarn(BIReport.TelemetryEvent telemetryEvent, int errorCode = 0, string message = null, constants.BnetRegion overrideBnetRegion = constants.BnetRegion.REGION_UNINITIALIZED)
  {
    this.Report_Telemetry(Telemetry.Level.LEVEL_WARN, telemetryEvent, errorCode, message, overrideBnetRegion);
  }

  public void TelemetryError(BIReport.TelemetryEvent telemetryEvent, int errorCode = 0, string message = null, constants.BnetRegion overrideBnetRegion = constants.BnetRegion.REGION_UNINITIALIZED)
  {
    this.Report_Telemetry(Telemetry.Level.LEVEL_ERROR, telemetryEvent, errorCode, message, overrideBnetRegion);
  }

  public void Report_Telemetry(Telemetry.Level level, BIReport.TelemetryEvent telemetryEvent)
  {
    this.Report_Telemetry(level, telemetryEvent, 0, (string) null);
  }

  public void Report_Telemetry(Telemetry.Level level, BIReport.TelemetryEvent telemetryEvent, constants.BnetRegion overrideBnetRegion)
  {
    this.Report_Telemetry(level, telemetryEvent, 0, (string) null, overrideBnetRegion);
  }

  public void Report_Telemetry(Telemetry.Level level, BIReport.TelemetryEvent telemetryEvent, string message)
  {
    this.Report_Telemetry(level, telemetryEvent, 0, message, constants.BnetRegion.REGION_UNINITIALIZED);
  }

  public void Report_Telemetry(Telemetry.Level level, BIReport.TelemetryEvent telemetryEvent, int errorCode, string message)
  {
    this.Report_Telemetry(level, telemetryEvent, errorCode, message, constants.BnetRegion.REGION_UNINITIALIZED);
  }

  private void Report_Telemetry(Telemetry.Level level, BIReport.TelemetryEvent telemetryEvent, int errorCode, string message, constants.BnetRegion overrideBnetRegion)
  {
    Telemetry telemetryProto = this.GenerateTelemetryProto(telemetryEvent);
    telemetryProto.Level_ = level;
    if (overrideBnetRegion != constants.BnetRegion.REGION_UNINITIALIZED)
      telemetryProto.BnetRegion_ = (Telemetry.BnetRegion) overrideBnetRegion;
    telemetryProto.ErrorCode_ = (long) errorCode;
    if (message != null)
      telemetryProto.Message_ = message;
    Log.BIReport.Print("Report: " + this.TelemetryDataToString(telemetryProto));
    this.StartCoroutine(this.Report(ProtobufUtil.ToByteArray((IProtoBuf) telemetryProto), "WTCG.BI.ClientTelemetry"));
  }

  public void Report_PresenceChange(PresenceStatus status, IEnumerable<int> parameters)
  {
    NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
    if (netObject == null || !netObject.SendTelemetryPresence)
      return;
    this.StartCoroutine(this.Report(ProtobufUtil.ToByteArray((IProtoBuf) this.GenerateTelemetryProto_PresenceChange(status, parameters)), "WTCG.BI.ClientTelemetry"));
  }

  public void Report_AppQuit()
  {
    Telemetry protoPresenceChange = this.GenerateTelemetryProto_PresenceChange(PresenceStatus.UNKNOWN, (IEnumerable<int>) null);
    BIReport.s_lastPresenceChangeStatus = new PresenceStatus?();
    BIReport.s_lastPresenceChangeParameters = (IEnumerable<int>) null;
    BIReport.s_lastPresenceChangeTimeLocal = new DateTime?();
    DateTime now = DateTime.Now;
    IEnumerator enumerator = this.Report(ProtobufUtil.ToByteArray((IProtoBuf) protoPresenceChange), "WTCG.BI.ClientTelemetry");
    do
      ;
    while (enumerator.MoveNext() && !((WWW) enumerator.Current).isDone && (DateTime.Now - now).TotalMilliseconds < 2000.0);
  }

  private Telemetry GenerateTelemetryProto(BIReport.TelemetryEvent telemetryEvent)
  {
    if (BIReport.s_sessionId == null)
      Log.BIReport.Print("ERROR: Sending report while s_sessionId == NULL");
    Telemetry.Locale locale = Telemetry.Locale.LOCALE_UNKNOWN;
    switch (Localization.GetLocale())
    {
      case Locale.enUS:
        locale = Telemetry.Locale.LOCALE_ENUS;
        break;
      case Locale.enGB:
        locale = Telemetry.Locale.LOCALE_ENGB;
        break;
      case Locale.frFR:
        locale = Telemetry.Locale.LOCALE_FRFR;
        break;
      case Locale.deDE:
        locale = Telemetry.Locale.LOCALE_DEDE;
        break;
      case Locale.koKR:
        locale = Telemetry.Locale.LOCALE_KOKR;
        break;
      case Locale.esES:
        locale = Telemetry.Locale.LOCALE_ESES;
        break;
      case Locale.esMX:
        locale = Telemetry.Locale.LOCALE_ESMX;
        break;
      case Locale.ruRU:
        locale = Telemetry.Locale.LOCALE_RURU;
        break;
      case Locale.zhTW:
        locale = Telemetry.Locale.LOCALE_ZHTW;
        break;
      case Locale.zhCN:
        locale = Telemetry.Locale.LOCALE_ZHCN;
        break;
      case Locale.itIT:
        locale = Telemetry.Locale.LOCALE_ITIT;
        break;
      case Locale.ptBR:
        locale = Telemetry.Locale.LOCALE_PTBR;
        break;
      case Locale.plPL:
        locale = Telemetry.Locale.LOCALE_PLPL;
        break;
      case Locale.jaJP:
        locale = Telemetry.Locale.LOCALE_15;
        break;
      case Locale.thTH:
        locale = Telemetry.Locale.LOCALE_16;
        break;
    }
    Telemetry.Platform platform = Telemetry.Platform.PLATFORM_UNKNOWN;
    switch (Application.platform)
    {
      case RuntimePlatform.OSXEditor:
        platform = Telemetry.Platform.PLATFORM_MAC;
        break;
      case RuntimePlatform.OSXPlayer:
        platform = Telemetry.Platform.PLATFORM_MAC;
        break;
      case RuntimePlatform.WindowsPlayer:
        platform = Telemetry.Platform.PLATFORM_PC;
        break;
      case RuntimePlatform.WindowsEditor:
        platform = Telemetry.Platform.PLATFORM_PC;
        break;
      case RuntimePlatform.IPhonePlayer:
        platform = Telemetry.Platform.PLATFORM_IOS;
        break;
      case RuntimePlatform.Android:
        platform = Telemetry.Platform.PLATFORM_ANDROID;
        break;
    }
    Telemetry.ScreenUI screenUi = Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android ? (!(bool) UniversalInputManager.UsePhoneUI ? Telemetry.ScreenUI.SCREENUI_TABLET : Telemetry.ScreenUI.SCREENUI_PHONE) : Telemetry.ScreenUI.SCREENUI_DESKTOP;
    Telemetry telemetry = new Telemetry();
    telemetry.Time_ = (long) BIReport.ConvertDateTimeToUnixEpoch(DateTime.Now);
    telemetry.Level_ = Telemetry.Level.LEVEL_INFO;
    telemetry.Version_ = string.Format("{0}.{1}", (object) "7.0", (object) 15590);
    telemetry.Locale_ = locale;
    telemetry.Platform_ = platform;
    telemetry.Os_ = SystemInfo.operatingSystem;
    telemetry.ScreenUI_ = screenUi;
    telemetry.Store_ = Telemetry.Store.STORE_BLIZZARD;
    telemetry.SessionId_ = BIReport.s_sessionId;
    telemetry.DeviceUniqueIdentifier_ = SystemInfo.deviceUniqueIdentifier;
    telemetry.Event_ = (ulong) telemetryEvent;
    if (BattleNet.IsInitialized() && BattleNet.GetCurrentRegion() != constants.BnetRegion.REGION_UNINITIALIZED)
    {
      telemetry.BnetRegion_ = (Telemetry.BnetRegion) BattleNet.GetCurrentRegion();
      EntityId myAccoundId = BattleNet.GetMyAccoundId();
      EntityId myGameAccountId = BattleNet.GetMyGameAccountId();
      telemetry.GameAccountId_ = myGameAccountId.lo;
      telemetry.PlayerIdentity = new PlayerIdentity();
      telemetry.PlayerIdentity.Account = new BnetId();
      telemetry.PlayerIdentity.Account.Hi = myAccoundId.hi;
      telemetry.PlayerIdentity.Account.Lo = myAccoundId.lo;
      telemetry.PlayerIdentity.GameAccount = new BnetId();
      telemetry.PlayerIdentity.GameAccount.Hi = myGameAccountId.hi;
      telemetry.PlayerIdentity.GameAccount.Lo = myGameAccountId.lo;
    }
    return telemetry;
  }

  public Telemetry GenerateTelemetryProto_PresenceChange(PresenceStatus status, IEnumerable<int> parameters = null)
  {
    Telemetry telemetryProto = this.GenerateTelemetryProto(BIReport.TelemetryEvent.EVENT_PRESENCE_CHANGE);
    telemetryProto.PresenceChange = new PresenceChange();
    telemetryProto.PresenceChange.NewStatus = (int) status;
    if (parameters != null)
      telemetryProto.PresenceChange.NewStatusParameters.AddRange(parameters);
    DateTime now = DateTime.Now;
    if (BIReport.s_lastPresenceChangeTimeLocal.HasValue)
    {
      DateTime? presenceChangeTimeLocal = BIReport.s_lastPresenceChangeTimeLocal;
      if ((!presenceChangeTimeLocal.HasValue ? 0 : (now >= presenceChangeTimeLocal.Value ? 1 : 0)) != 0)
      {
        telemetryProto.PresenceChange.MillisecondsSincePreviousStatus = (long) (now - BIReport.s_lastPresenceChangeTimeLocal.Value).TotalMilliseconds;
        goto label_6;
      }
    }
    telemetryProto.PresenceChange.MillisecondsSincePreviousStatus = 0L;
label_6:
    if (BIReport.s_lastPresenceChangeStatus.HasValue)
      telemetryProto.PresenceChange.PreviousStatus = (int) BIReport.s_lastPresenceChangeStatus.Value;
    if (BIReport.s_lastPresenceChangeParameters != null)
      telemetryProto.PresenceChange.PreviousStatusParameters.AddRange(BIReport.s_lastPresenceChangeParameters);
    BIReport.s_lastPresenceChangeStatus = new PresenceStatus?(status);
    BIReport.s_lastPresenceChangeParameters = parameters;
    BIReport.s_lastPresenceChangeTimeLocal = new DateTime?(now);
    return telemetryProto;
  }

  [DebuggerHidden]
  private IEnumerator Report(byte[] data, string protoMessageType)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BIReport.\u003CReport\u003Ec__Iterator2E0() { protoMessageType = protoMessageType, data = data, \u003C\u0024\u003EprotoMessageType = protoMessageType, \u003C\u0024\u003Edata = data };
  }

  private void GenerateSessionID()
  {
    if (BIReport.s_sessionId != null)
      Log.BIReport.Print("WARNING: Replacing session ID [" + BIReport.s_sessionId + "]");
    string message = SystemInfo.deviceUniqueIdentifier + DateTime.Now.ToFileTimeUtc().ToString();
    Log.BIReport.Print("rawSessionId = " + message);
    BIReport.s_sessionId = Crypto.SHA1.Calc(message);
    Log.BIReport.Print("s_sessionId = " + BIReport.s_sessionId);
  }

  private string TelemetryDataToString(Telemetry data)
  {
    return "Event_ = " + (object) (BIReport.TelemetryEvent) data.Event_ + " Time = " + (object) data.Time_ + " Level = " + (object) data.Level_ + " Version = " + data.Version_ + " Locale = " + (object) data.Locale_ + " Platform = " + (object) data.Platform_ + " OS = " + data.Os_ + " ScreenUI = " + (object) data.ScreenUI_ + " Store = " + (object) data.Store_ + " SessionId = " + data.SessionId_ + " DeviceUniqueIdentifier = " + data.DeviceUniqueIdentifier_ + " BnetRegion_ = " + (object) data.BnetRegion_ + " GameAccountId_ = " + (object) data.GameAccountId_ + " ErrorCode_ = " + (object) data.ErrorCode_ + " Message = " + data.Message_;
  }

  public static double ConvertDateTimeToUnixEpoch(DateTime time)
  {
    return (time - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
  }

  public enum TelemetryEvent
  {
    EVENT_GAMEPLAY_STUCK_DISCONNECT = 100,
    EVENT_PRESENCE_CHANGE = 101,
    EVENT_WEB_LOGIN_TOKEN_PROVIDED = 300,
    EVENT_WEB_LOGIN_ERROR = 410,
    EVENT_IGNORABLE_BNET_ERROR = 500,
    EVENT_FATAL_BNET_ERROR = 600,
    EVENT_ON_RESET = 700,
    EVENT_ON_RESET_WITH_LOGIN = 710,
    EVENT_THIRD_PARTY_PURCHASE_REQUEST = 800,
    EVENT_THIRD_PARTY_PURCHASE_SUCCESS = 810,
    EVENT_THIRD_PARTY_PURCHASE_SUCCESS_MALFORMED = 820,
    EVENT_THIRD_PARTY_PURCHASE_FAILED = 830,
    EVENT_THIRD_PARTY_PURCHASE_DEFERRED = 840,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_SIZE = 850,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_RECEIVED = 860,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_SUBMITTED = 870,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_SUBMITTED_FAILED = 871,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_SUBMITTED_RESPONSE = 872,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_SUBMITTED_DANGLING = 880,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_SUBMITTED_DANGLING_FAILED = 881,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_REQUEST = 890,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_REQUEST_NOT_FOUND = 891,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_REQUEST_FOUND = 892,
    EVENT_THIRD_PARTY_PURCHASE_AUTO_CANCEL = 893,
    EVENT_THIRD_PARTY_PURCHASE_CANCEL_RESPONSE = 894,
    EVENT_THIRD_PARTY_PURCHASE_RECEIPT_CONSUMED = 899,
    EVENT_ERROR_NETWORK_UNAVAILABLE = 900,
    EVENT_ERROR_UNKNOWN_ERROR = 1000,
    EVENT_RETURNING_PLAYER_DECK_NOT_CREATED_TOO_MANY_DECKS = 1100,
  }
}
