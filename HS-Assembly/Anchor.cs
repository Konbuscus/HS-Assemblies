﻿// Decompiled with JetBrains decompiler
// Type: Anchor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum Anchor
{
  TOP_LEFT,
  TOP,
  TOP_RIGHT,
  LEFT,
  CENTER,
  RIGHT,
  BOTTOM_LEFT,
  BOTTOM,
  BOTTOM_RIGHT,
  FRONT,
  BACK,
  TOP_LEFT_XZ,
  TOP_XZ,
  TOP_RIGHT_XZ,
  LEFT_XZ,
  CENTER_XZ,
  RIGHT_XZ,
  BOTTOM_LEFT_XZ,
  BOTTOM_XZ,
  BOTTOM_RIGHT_XZ,
  FRONT_XZ,
  BACK_XZ,
}
