﻿// Decompiled with JetBrains decompiler
// Type: AdventureWingProgressDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class AdventureWingProgressDisplay : MonoBehaviour
{
  public virtual void UpdateProgress(WingDbId wingDbId, bool normalComplete)
  {
  }

  public virtual bool HasProgressAnimationToPlay()
  {
    return false;
  }

  public virtual void PlayProgressAnimation(AdventureWingProgressDisplay.OnAnimationComplete onAnimComplete = null)
  {
    if (onAnimComplete == null)
      return;
    onAnimComplete();
  }

  public delegate void OnAnimationComplete();
}
