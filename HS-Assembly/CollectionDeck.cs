﻿// Decompiled with JetBrains decompiler
// Type: CollectionDeck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class CollectionDeck
{
  private List<CollectionDeckSlot> m_slots = new List<CollectionDeckSlot>();
  public DeckType Type = DeckType.NORMAL_DECK;
  public string HeroCardID = string.Empty;
  private string m_name;
  private bool m_netContentsLoaded;
  private bool m_isSavingContentChanges;
  private bool m_isSavingNameChanges;
  private bool m_isBeingDeleted;
  public long ID;
  public TAG_PREMIUM HeroPremium;
  public bool HeroOverridden;
  public int CardBackID;
  public bool CardBackOverridden;
  public int SeasonId;
  public bool NeedsName;
  public long SortOrder;
  public ulong CreateDate;
  public bool Locked;
  public DeckSourceType SourceType;
  private bool m_isWild;

  public string Name
  {
    get
    {
      return this.m_name;
    }
    set
    {
      if (value == null)
      {
        Debug.LogError((object) string.Format("CollectionDeck.SetName() - null name given for deck {0}", (object) this));
      }
      else
      {
        if (value.Equals(this.m_name, StringComparison.InvariantCultureIgnoreCase))
          return;
        this.m_name = value;
      }
    }
  }

  public bool IsWild
  {
    get
    {
      if (!GameUtils.IsAnythingRotated())
        return false;
      return this.m_isWild;
    }
    set
    {
      this.m_isWild = value;
    }
  }

  public bool IsTourneyValid
  {
    get
    {
      if (!this.m_netContentsLoaded)
        return false;
      return this.GetRuleset().IsDeckValid(this);
    }
  }

  public override string ToString()
  {
    return string.Format("Deck [id={0} name=\"{1}\" heroCardId={2} heroCardFlair={3} cardBackId={4} cardBackOverridden={5} heroOverridden={6} slotCount={7} needsName={8} sortOrder={9}]", (object) this.ID, (object) this.Name, (object) this.HeroCardID, (object) this.HeroPremium, (object) this.CardBackID, (object) this.CardBackOverridden, (object) this.HeroOverridden, (object) this.GetSlotCount(), (object) this.NeedsName, (object) this.SortOrder);
  }

  public void MarkNetworkContentsLoaded()
  {
    this.m_netContentsLoaded = true;
  }

  public bool NetworkContentsLoaded()
  {
    return this.m_netContentsLoaded;
  }

  public void MarkBeingDeleted()
  {
    this.m_isBeingDeleted = true;
  }

  public bool IsBeingDeleted()
  {
    return this.m_isBeingDeleted;
  }

  public bool IsSavingChanges()
  {
    if (!this.m_isSavingNameChanges)
      return this.m_isSavingContentChanges;
    return true;
  }

  public bool IsBasicDeck()
  {
    if (this.IsWild || this.SourceType != DeckSourceType.DECK_SOURCE_TYPE_BASIC_DECK)
      return false;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (DefLoader.Get().GetEntityDef(enumerator.Current.CardID).GetCardSet() != TAG_CARD_SET.CORE)
          return false;
      }
    }
    return true;
  }

  public bool IsInnkeeperDeck()
  {
    return this.SortOrder == -500L;
  }

  public int GetTotalCardCount()
  {
    int num = 0;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        num += current.Count;
      }
    }
    return num;
  }

  public int GetTotalOwnedCardCount()
  {
    if (!this.ShouldSplitSlotsByValidity())
      return this.GetTotalCardCount();
    int num = 0;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (current.Owned)
          num += current.Count;
      }
    }
    return num;
  }

  public int GetTotalValidCardCount()
  {
    int num = 0;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (this.IsValidSlot(current))
          num += current.Count;
      }
    }
    return num;
  }

  public int GetTotalInvalidCardCount()
  {
    int num = 0;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (!this.IsValidSlot(current))
          num += current.Count;
      }
    }
    return num;
  }

  public int GetTotalUnownedCardCount()
  {
    int num = 0;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (!current.Owned)
          num += current.Count;
      }
    }
    return num;
  }

  public List<CollectionDeckSlot> GetSlots()
  {
    return this.m_slots;
  }

  public int GetSlotCount()
  {
    return this.m_slots.Count;
  }

  public bool IsValidSlot(CollectionDeckSlot slot)
  {
    if (this.Locked)
      return true;
    if (this.SlotMatchesFormat(slot))
      return slot.Owned;
    return false;
  }

  public bool SlotMatchesFormat(CollectionDeckSlot slot)
  {
    if (this.IsWild)
      return true;
    return !GameUtils.IsCardRotated(slot.CardID);
  }

  public bool HasReplaceableSlot()
  {
    for (int index = 0; index < this.m_slots.Count; ++index)
    {
      if (!this.IsValidSlot(this.m_slots[index]))
        return true;
    }
    return false;
  }

  public CollectionDeckSlot GetSlotByIndex(int slotIndex)
  {
    if (slotIndex < 0 || slotIndex >= this.GetSlotCount())
      return (CollectionDeckSlot) null;
    return this.m_slots[slotIndex];
  }

  public CollectionDeckSlot GetSlotByUID(long uid)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_slots.Find(new Predicate<CollectionDeckSlot>(new CollectionDeck.\u003CGetSlotByUID\u003Ec__AnonStorey37C()
    {
      uid = uid,
      \u003C\u003Ef__this = this
    }.\u003C\u003Em__AB));
  }

  public DeckRuleset GetRuleset()
  {
    DeckRuleset deckRuleset = (DeckRuleset) null;
    switch (this.Type)
    {
      case DeckType.NORMAL_DECK:
      case DeckType.PRECON_DECK:
        deckRuleset = !this.IsWild ? DeckRuleset.GetStandardRuleset() : DeckRuleset.GetWildRuleset();
        break;
      case DeckType.TAVERN_BRAWL_DECK:
        deckRuleset = TavernBrawlManager.Get().GetDeckRuleset();
        break;
    }
    if (deckRuleset == null)
      deckRuleset = DeckRuleset.GetWildRuleset();
    return deckRuleset;
  }

  public bool IsValidForFormat(bool isFormatWild)
  {
    if (this.IsWild)
      return isFormatWild;
    return true;
  }

  public bool InsertSlotByDefaultSort(CollectionDeckSlot slot)
  {
    return this.InsertSlot(this.GetInsertionIdxByDefaultSort(slot), slot);
  }

  public void CopyFrom(CollectionDeck otherDeck)
  {
    this.ID = otherDeck.ID;
    this.Type = otherDeck.Type;
    this.m_name = otherDeck.m_name;
    this.HeroCardID = otherDeck.HeroCardID;
    this.HeroPremium = otherDeck.HeroPremium;
    this.HeroOverridden = otherDeck.HeroOverridden;
    this.CardBackID = otherDeck.CardBackID;
    this.CardBackOverridden = otherDeck.CardBackOverridden;
    this.NeedsName = otherDeck.NeedsName;
    this.SeasonId = otherDeck.SeasonId;
    this.IsWild = otherDeck.IsWild;
    this.SortOrder = otherDeck.SortOrder;
    this.SourceType = otherDeck.SourceType;
    this.m_slots.Clear();
    for (int slotIndex = 0; slotIndex < otherDeck.GetSlotCount(); ++slotIndex)
    {
      CollectionDeckSlot slotByIndex = otherDeck.GetSlotByIndex(slotIndex);
      CollectionDeckSlot collectionDeckSlot = new CollectionDeckSlot();
      collectionDeckSlot.CopyFrom(slotByIndex);
      this.m_slots.Add(collectionDeckSlot);
    }
  }

  public void CopyContents(CollectionDeck otherDeck)
  {
    this.HeroCardID = otherDeck.HeroCardID;
    this.HeroPremium = otherDeck.HeroPremium;
    this.m_slots.Clear();
    for (int slotIndex = 0; slotIndex < otherDeck.GetSlotCount(); ++slotIndex)
    {
      CollectionDeckSlot slotByIndex = otherDeck.GetSlotByIndex(slotIndex);
      for (int index = 0; index < slotByIndex.Count; ++index)
        this.AddCard(slotByIndex.CardID, slotByIndex.Premium, false);
    }
  }

  public void FillFromTemplateDeck(CollectionManager.TemplateDeck tplDeck)
  {
    this.ClearSlotContents();
    this.Name = tplDeck.m_title;
    using (Map<string, int>.Enumerator enumerator = tplDeck.m_cardIds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        int golden = 0;
        int standard = 0;
        CollectionManager.Get().GetOwnedCardCount(current.Key, out standard, out golden);
        int b = current.Value;
        int num1 = Mathf.Min(golden, b);
        int num2 = b - num1;
        for (int index = 0; index < num1; ++index)
          this.AddCard(current.Key, TAG_PREMIUM.GOLDEN, false);
        for (int index = 0; index < num2; ++index)
          this.AddCard(current.Key, TAG_PREMIUM.NORMAL, false);
      }
    }
  }

  public int ReconcileUnownedCards()
  {
    Map<long, CollectionDeckSlot> map = new Map<long, CollectionDeckSlot>();
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (!map.ContainsKey(current.UID))
          map.Add(current.UID, current);
      }
    }
    List<KeyValuePair<CollectionDeckSlot, int>> keyValuePairList = new List<KeyValuePair<CollectionDeckSlot, int>>();
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        CollectibleCard card = CollectionManager.Get().GetCard(current.CardID, current.Premium);
        CollectionDeckSlot collectionDeckSlot = (CollectionDeckSlot) null;
        map.TryGetValue(current.UID, out collectionDeckSlot);
        if (!current.Owned)
        {
          int num1 = collectionDeckSlot == null || !collectionDeckSlot.Owned ? card.OwnedCount : card.OwnedCount - collectionDeckSlot.Count;
          int num2 = num1 < current.Count ? num1 : current.Count;
          if (num2 > 0)
            keyValuePairList.Add(new KeyValuePair<CollectionDeckSlot, int>(current, num2));
        }
      }
    }
    using (List<KeyValuePair<CollectionDeckSlot, int>>.Enumerator enumerator = keyValuePairList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<CollectionDeckSlot, int> current = enumerator.Current;
        CollectionDeckSlot key = current.Key;
        int num = current.Value;
        string cardId = key.CardID;
        TAG_PREMIUM premium = key.Premium;
        Log.DeckTray.Print("Removing unowned ghostSlot {0} {1}", new object[2]
        {
          (object) key,
          (object) num
        });
        if (num >= key.Count)
          this.RemoveSlot(key);
        else
          key.Count -= num;
        for (int index = 0; index < num; ++index)
        {
          Log.DeckTray.Print("adding card to replace unowned slot: {0} {1}", new object[2]
          {
            (object) cardId,
            (object) premium
          });
          this.AddCard(cardId, premium, false);
        }
      }
    }
    return keyValuePairList.Count;
  }

  public int GetUnownedCardIdCount(string cardID)
  {
    int num = 0;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (current.CardID.Equals(cardID) && !current.Owned)
          num += current.Count;
      }
    }
    return num;
  }

  public int GetInvalidCardIdCount(string cardID)
  {
    int num = 0;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (current.CardID.Equals(cardID) && !this.IsValidSlot(current))
          num += current.Count;
      }
    }
    return num;
  }

  public int GetCardIdCount(string cardID, bool includeUnowned = true)
  {
    int num = 0;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (current.CardID.Equals(cardID) && (includeUnowned || current.Owned))
          num += current.Count;
      }
    }
    return num;
  }

  public int GetCardCountFirstMatchingSlot(string cardID, TAG_PREMIUM type)
  {
    CollectionDeckSlot slotByCardId = this.FindSlotByCardId(cardID, type);
    if (slotByCardId == null)
      return 0;
    return slotByCardId.Count;
  }

  public int GetCardCountAllMatchingSlots(string cardID, TAG_PREMIUM premium)
  {
    int num = 0;
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (current.CardID.Equals(cardID) && current.Premium == premium)
          num += current.Count;
      }
    }
    return num;
  }

  public int GetValidCardCount(string cardID, TAG_PREMIUM type, bool valid = true)
  {
    CollectionDeckSlot validSlotByCardId = this.FindValidSlotByCardId(cardID, type, valid);
    if (validSlotByCardId == null)
      return 0;
    return validSlotByCardId.Count;
  }

  public int GetOwnedCardCount(string cardID, TAG_PREMIUM type, bool owned = true)
  {
    CollectionDeckSlot ownedSlotByCardId = this.FindOwnedSlotByCardId(cardID, type, owned);
    if (ownedSlotByCardId == null)
      return 0;
    return ownedSlotByCardId.Count;
  }

  public int GetCardCountInSet(HashSet<string> set, bool isNot)
  {
    int num = 0;
    for (int index = 0; index < this.m_slots.Count; ++index)
    {
      CollectionDeckSlot slot = this.m_slots[index];
      if (set.Contains(slot.CardID) == !isNot)
        num += slot.Count;
    }
    return num;
  }

  public int CountFilterMisses(DeckRuleset deckRuleset)
  {
    int num = 0;
    for (int index = 0; index < this.m_slots.Count; ++index)
    {
      CollectionDeckSlot slot = this.m_slots[index];
      EntityDef entityDef = DefLoader.Get().GetEntityDef(slot.CardID);
      if (!deckRuleset.Filter(entityDef))
        num += slot.Count;
    }
    return num;
  }

  public List<CollectionDeckSlot> GetFilterMisses(DeckRuleset deckRuleset)
  {
    List<CollectionDeckSlot> collectionDeckSlotList = new List<CollectionDeckSlot>();
    for (int index = 0; index < this.m_slots.Count; ++index)
    {
      CollectionDeckSlot slot = this.m_slots[index];
      EntityDef entityDef = DefLoader.Get().GetEntityDef(slot.CardID);
      if (!deckRuleset.Filter(entityDef))
        collectionDeckSlotList.Add(slot);
    }
    return collectionDeckSlotList;
  }

  public void ClearSlotContents()
  {
    this.m_slots.Clear();
  }

  public bool CanAddOwnedCard(string cardID, TAG_PREMIUM premium)
  {
    int standard = 0;
    int golden = 0;
    CollectionManager.Get().GetOwnedCardCount(cardID, out standard, out golden);
    return (premium != TAG_PREMIUM.NORMAL ? golden : standard) > this.GetOwnedCardCount(cardID, premium, true);
  }

  public bool AddCard(EntityDef cardEntityDef, TAG_PREMIUM premium, bool exceedMax = false)
  {
    return this.AddCard(cardEntityDef.GetCardId(), premium, exceedMax);
  }

  public bool AddCard(string cardID, TAG_PREMIUM premium, bool exceedMax = false)
  {
    if (!exceedMax && !this.CanInsertCard(cardID, premium))
      return false;
    bool owned = this.CanAddOwnedCard(cardID, premium);
    CollectionDeckSlot collectionDeckSlot = owned ? this.FindValidSlotByCardId(cardID, premium, true) : this.FindOwnedSlotByCardId(cardID, premium, owned);
    if (collectionDeckSlot != null)
    {
      ++collectionDeckSlot.Count;
      return true;
    }
    return this.InsertSlotByDefaultSort(new CollectionDeckSlot()
    {
      CardID = cardID,
      Count = 1,
      Premium = premium,
      Owned = owned
    });
  }

  public void AddCard_IgnoreValidity(string cardID, TAG_PREMIUM premium, int countToAdd)
  {
    if (countToAdd <= 0)
      return;
    int num1 = countToAdd;
    int standard = 0;
    int golden = 0;
    CollectionManager.Get().GetOwnedCardCount(cardID, out standard, out golden);
    int num2 = premium != TAG_PREMIUM.NORMAL ? golden : standard;
    if (num2 > 0)
    {
      CollectionDeckSlot slot = this.FindOwnedSlotByCardId(cardID, premium, true);
      if (slot == null)
      {
        slot = new CollectionDeckSlot()
        {
          CardID = cardID,
          Count = 0,
          Premium = premium,
          Owned = true
        };
        this.InsertSlotByDefaultSort(slot);
      }
      int b = !this.ShouldSplitSlotsByValidity() ? slot.Count + num1 : num2;
      int num3 = Mathf.Min(slot.Count + num1, b);
      int num4 = num3 - slot.Count;
      slot.Count = num3;
      num1 -= num4;
    }
    if (num1 <= 0)
      return;
    CollectionDeckSlot ownedSlotByCardId = this.FindOwnedSlotByCardId(cardID, premium, false);
    if (ownedSlotByCardId == null)
      this.InsertSlotByDefaultSort(new CollectionDeckSlot()
      {
        CardID = cardID,
        Count = num1,
        Premium = premium,
        Owned = false
      });
    else
      ownedSlotByCardId.Count += num1;
  }

  public bool RemoveCard(string cardID, TAG_PREMIUM premium, bool valid = true, bool removeAllCopies = false)
  {
    CollectionDeckSlot validSlotByCardId = this.FindValidSlotByCardId(cardID, premium, valid);
    if (validSlotByCardId == null)
      return false;
    if (removeAllCopies)
      validSlotByCardId.Count = 0;
    else
      --validSlotByCardId.Count;
    return true;
  }

  public void OnContentChangesComplete()
  {
    this.m_isSavingContentChanges = false;
  }

  public void OnNameChangeComplete()
  {
    this.m_isSavingNameChanges = false;
  }

  public void SendChanges()
  {
    CollectionDeck baseDeck = CollectionManager.Get().GetBaseDeck(this.ID);
    if (this == baseDeck)
    {
      Debug.LogError((object) string.Format("CollectionDeck.Send() - {0} is a base deck. You cannot send a base deck to the network.", (object) baseDeck));
    }
    else
    {
      string deckName;
      this.GenerateNameDiff(baseDeck, out deckName);
      List<Network.CardUserData> contentChanges = this.GenerateContentChanges(baseDeck);
      int heroAssetID;
      TAG_PREMIUM heroCardPremium;
      bool heroDiff = this.GenerateHeroDiff(baseDeck, out heroAssetID, out heroCardPremium);
      int cardBackID;
      bool cardBackDiff = this.GenerateCardBackDiff(baseDeck, out cardBackID);
      bool flag = baseDeck.IsWild != this.IsWild;
      Network network = Network.Get();
      if (deckName != null)
      {
        this.m_isSavingNameChanges = true;
        network.RenameDeck(this.ID, deckName);
      }
      if (flag)
        this.SortOrder = TimeUtils.GetEpochTime();
      if (contentChanges.Count <= 0 && !heroDiff && (!cardBackDiff && !flag))
        return;
      this.m_isSavingContentChanges = true;
      Network.SendDeckData(this.ID, contentChanges, heroAssetID, heroCardPremium, cardBackID, this.IsWild, this.SortOrder);
    }
  }

  private bool CanInsertCard(string cardID, TAG_PREMIUM premium)
  {
    if (this.Type == DeckType.DRAFT_DECK)
      return true;
    DeckRuleset deckRuleset = CollectionManager.Get().GetDeckRuleset();
    EntityDef entityDef = DefLoader.Get().GetEntityDef(cardID);
    if (deckRuleset == null)
      return true;
    RuleInvalidReason reason;
    DeckRule brokenRule;
    return deckRuleset.CanAddToDeck(entityDef, premium, this, out reason, out brokenRule, new DeckRule.RuleType[1]
    {
      DeckRule.RuleType.PLAYER_OWNS_EACH_COPY
    });
  }

  private bool InsertSlot(int slotIndex, CollectionDeckSlot slot)
  {
    if (slotIndex < 0 || slotIndex > this.GetSlotCount())
    {
      Log.Rachelle.Print(string.Format("CollectionDeck.InsertSlot(): inserting slot {0} failed; invalid slot index {1}.", (object) slot, (object) slotIndex));
      return false;
    }
    long uid = slot.GetUID(this);
    CollectionDeckSlot slotByUid = this.GetSlotByUID(uid);
    if (slotByUid != null)
    {
      Debug.LogWarningFormat("CollectionDeck.InsertSlot: slot with uid={0} already exists in deckId={1} cardId={2} cardDbId={3} premium={4} owned={5} existingCount={6} slotIndex={7}", (object) uid, (object) this.ID, (object) slot.CardID, (object) GameUtils.TranslateCardIdToDbId(slot.CardID), (object) slot.Premium, (object) slot.Owned, (object) slotByUid.Count, (object) slotIndex);
      return false;
    }
    slot.OnSlotEmptied += new CollectionDeckSlot.DelOnSlotEmptied(this.OnSlotEmptied);
    slot.Index = slotIndex;
    this.m_slots.Insert(slotIndex, slot);
    this.UpdateSlotIndices(slotIndex, this.GetSlotCount() - 1);
    return true;
  }

  public void ForceRemoveSlot(CollectionDeckSlot slot)
  {
    this.RemoveSlot(slot);
  }

  private void RemoveSlot(CollectionDeckSlot slot)
  {
    slot.OnSlotEmptied -= new CollectionDeckSlot.DelOnSlotEmptied(this.OnSlotEmptied);
    int index = slot.Index;
    this.m_slots.RemoveAt(index);
    this.UpdateSlotIndices(index, this.GetSlotCount() - 1);
  }

  private void OnSlotEmptied(CollectionDeckSlot slot)
  {
    if (this.GetSlotByUID(slot.GetUID(this)) == null)
      Log.Rachelle.Print(string.Format("CollectionDeck.OnSlotCountUpdated(): Trying to remove slot {0}, but it does not exist in deck {1}", (object) slot, (object) this));
    else
      this.RemoveSlot(slot);
  }

  private void UpdateSlotIndices(int indexA, int indexB)
  {
    if (this.GetSlotCount() == 0)
      return;
    int val2;
    int val1;
    if (indexA < indexB)
    {
      val2 = indexA;
      val1 = indexB;
    }
    else
    {
      val2 = indexB;
      val1 = indexA;
    }
    int num1 = Math.Max(0, val2);
    int num2 = Math.Min(val1, this.GetSlotCount() - 1);
    for (int slotIndex = num1; slotIndex <= num2; ++slotIndex)
      this.GetSlotByIndex(slotIndex).Index = slotIndex;
  }

  public CollectionDeckSlot FindSlotByCardId(string cardID, TAG_PREMIUM premium)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_slots.Find(new Predicate<CollectionDeckSlot>(new CollectionDeck.\u003CFindSlotByCardId\u003Ec__AnonStorey37D()
    {
      cardID = cardID,
      premium = premium
    }.\u003C\u003Em__AC));
  }

  private CollectionDeckSlot FindOwnedSlotByCardId(string cardID, TAG_PREMIUM premium, bool owned)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionDeck.\u003CFindOwnedSlotByCardId\u003Ec__AnonStorey37E idCAnonStorey37E = new CollectionDeck.\u003CFindOwnedSlotByCardId\u003Ec__AnonStorey37E();
    // ISSUE: reference to a compiler-generated field
    idCAnonStorey37E.cardID = cardID;
    // ISSUE: reference to a compiler-generated field
    idCAnonStorey37E.premium = premium;
    // ISSUE: reference to a compiler-generated field
    idCAnonStorey37E.owned = owned;
    if (!this.ShouldSplitSlotsByValidity())
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      return this.FindSlotByCardId(idCAnonStorey37E.cardID, idCAnonStorey37E.premium);
    }
    // ISSUE: reference to a compiler-generated method
    return this.m_slots.Find(new Predicate<CollectionDeckSlot>(idCAnonStorey37E.\u003C\u003Em__AD));
  }

  private CollectionDeckSlot FindValidSlotByCardId(string cardID, TAG_PREMIUM premium, bool valid)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_slots.Find(new Predicate<CollectionDeckSlot>(new CollectionDeck.\u003CFindValidSlotByCardId\u003Ec__AnonStorey37F()
    {
      cardID = cardID,
      premium = premium,
      valid = valid,
      \u003C\u003Ef__this = this
    }.\u003C\u003Em__AE));
  }

  private void GenerateNameDiff(CollectionDeck baseDeck, out string deckName)
  {
    deckName = (string) null;
    if (this.Name.Equals(baseDeck.Name))
      return;
    deckName = this.Name;
  }

  private bool GenerateHeroDiff(CollectionDeck baseDeck, out int heroAssetID, out TAG_PREMIUM heroCardPremium)
  {
    heroAssetID = -1;
    heroCardPremium = TAG_PREMIUM.NORMAL;
    if (!this.HeroOverridden)
      return false;
    bool flag = this.HeroCardID == baseDeck.HeroCardID && this.HeroPremium == baseDeck.HeroPremium;
    if (baseDeck.HeroOverridden && flag)
      return false;
    heroAssetID = GameUtils.TranslateCardIdToDbId(this.HeroCardID);
    heroCardPremium = this.HeroPremium;
    return true;
  }

  private bool GenerateCardBackDiff(CollectionDeck baseDeck, out int cardBackID)
  {
    cardBackID = -1;
    if (!this.CardBackOverridden)
      return false;
    bool flag = this.CardBackID == baseDeck.CardBackID;
    if (baseDeck.CardBackOverridden && flag)
      return false;
    cardBackID = this.CardBackID;
    return true;
  }

  private Network.CardUserData CardUserDataFromSlot(CollectionDeckSlot deckSlot, bool deleted)
  {
    return new Network.CardUserData()
    {
      DbId = GameUtils.TranslateCardIdToDbId(deckSlot.CardID),
      Count = !deleted ? deckSlot.Count : 0,
      Premium = deckSlot.Premium
    };
  }

  private List<Network.CardUserData> GenerateContentChanges(CollectionDeck baseDeck)
  {
    SortedDictionary<long, CollectionDeckSlot> sortedDictionary1 = new SortedDictionary<long, CollectionDeckSlot>();
    using (List<CollectionDeckSlot>.Enumerator enumerator = baseDeck.GetSlots().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        CollectionDeckSlot collectionDeckSlot1 = (CollectionDeckSlot) null;
        if (sortedDictionary1.TryGetValue(current.UID, out collectionDeckSlot1))
        {
          collectionDeckSlot1.Count += current.Count;
        }
        else
        {
          CollectionDeckSlot collectionDeckSlot2 = new CollectionDeckSlot();
          collectionDeckSlot2.CopyFrom(current);
          sortedDictionary1.Add(collectionDeckSlot2.UID, collectionDeckSlot2);
        }
      }
    }
    SortedDictionary<long, CollectionDeckSlot> sortedDictionary2 = new SortedDictionary<long, CollectionDeckSlot>();
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.GetSlots().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        CollectionDeckSlot collectionDeckSlot1 = (CollectionDeckSlot) null;
        if (sortedDictionary2.TryGetValue(current.UID, out collectionDeckSlot1))
        {
          collectionDeckSlot1.Count += current.Count;
        }
        else
        {
          CollectionDeckSlot collectionDeckSlot2 = new CollectionDeckSlot();
          collectionDeckSlot2.CopyFrom(current);
          sortedDictionary2.Add(collectionDeckSlot2.UID, collectionDeckSlot2);
        }
      }
    }
    SortedDictionary<long, CollectionDeckSlot>.Enumerator enumerator1 = sortedDictionary1.GetEnumerator();
    SortedDictionary<long, CollectionDeckSlot>.Enumerator enumerator2 = sortedDictionary2.GetEnumerator();
    List<Network.CardUserData> cardUserDataList = new List<Network.CardUserData>();
    bool flag1 = enumerator1.MoveNext();
    bool flag2 = enumerator2.MoveNext();
    while (flag1 && flag2)
    {
      CollectionDeckSlot deckSlot1 = enumerator1.Current.Value;
      CollectionDeckSlot deckSlot2 = enumerator2.Current.Value;
      if (deckSlot1.GetUID(this) == deckSlot2.GetUID(this))
      {
        if (deckSlot1.Count != deckSlot2.Count)
          cardUserDataList.Add(this.CardUserDataFromSlot(deckSlot2, 0 == deckSlot2.Count));
        flag1 = enumerator1.MoveNext();
        flag2 = enumerator2.MoveNext();
      }
      else if (deckSlot1.GetUID(this) < deckSlot2.GetUID(this))
      {
        cardUserDataList.Add(this.CardUserDataFromSlot(deckSlot1, true));
        flag1 = enumerator1.MoveNext();
      }
      else
      {
        cardUserDataList.Add(this.CardUserDataFromSlot(deckSlot2, false));
        flag2 = enumerator2.MoveNext();
      }
    }
    for (; flag1; flag1 = enumerator1.MoveNext())
    {
      CollectionDeckSlot deckSlot = enumerator1.Current.Value;
      cardUserDataList.Add(this.CardUserDataFromSlot(deckSlot, true));
    }
    for (; flag2; flag2 = enumerator2.MoveNext())
    {
      CollectionDeckSlot deckSlot = enumerator2.Current.Value;
      cardUserDataList.Add(this.CardUserDataFromSlot(deckSlot, false));
    }
    return cardUserDataList;
  }

  private int GetInsertionIdxByDefaultSort(CollectionDeckSlot slot)
  {
    EntityDef entityDef1 = DefLoader.Get().GetEntityDef(slot.CardID);
    if (entityDef1 == null)
    {
      Log.Rachelle.Print(string.Format("CollectionDeck.GetInsertionIdxByDefaultSort(): could not get entity def for {0}", (object) slot.CardID));
      return -1;
    }
    int slotIndex;
    for (slotIndex = 0; slotIndex < this.GetSlotCount(); ++slotIndex)
    {
      CollectionDeckSlot slotByIndex = this.GetSlotByIndex(slotIndex);
      EntityDef entityDef2 = DefLoader.Get().GetEntityDef(slotByIndex.CardID);
      if (entityDef2 == null)
      {
        Log.Rachelle.Print(string.Format("CollectionDeck.GetInsertionIdxByDefaultSort(): entityDef is null at slot index {0}", (object) slotIndex));
        break;
      }
      int num = CollectionManager.Get().EntityDefSortComparison(entityDef1, entityDef2);
      if (num < 0 || num <= 0 && slot.Premium <= slotByIndex.Premium && (!this.ShouldSplitSlotsByValidity() || slot.Owned == slotByIndex.Owned))
        break;
    }
    return slotIndex;
  }

  public TAG_CLASS GetClass()
  {
    return DefLoader.Get().GetEntityDef(this.HeroCardID).GetClass();
  }

  public string ToDeckString()
  {
    StringBuilder stringBuilder1 = new StringBuilder();
    StringBuilder stringBuilder2 = new StringBuilder();
    EntityDef entityDef1 = DefLoader.Get().GetEntityDef(this.HeroCardID);
    string name = BnetPresenceMgr.Get().GetMyPlayer().GetBattleTag().GetName();
    string lower = entityDef1.GetClass().ToString().ToLower();
    string str = lower.Substring(0, 1).ToUpper() + lower.Substring(1, lower.Length - 1);
    stringBuilder1.Append(string.Format("# Hearthstone {0} Deck: \"{1}\" saved by {2}\n", (object) str, (object) this.Name, (object) name));
    stringBuilder1.Append("#\n");
    using (List<CollectionDeckSlot>.Enumerator enumerator = this.m_slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        EntityDef entityDef2 = DefLoader.Get().GetEntityDef(current.CardID);
        stringBuilder1.Append(string.Format("# {0}x ({1}) {2}\n", (object) current.Count, (object) entityDef2.GetCost(), (object) entityDef2.GetName()));
        stringBuilder2.Append(string.Format("{0},{1};", (object) current.Count, (object) current.CardID));
      }
    }
    stringBuilder1.Append("#\n");
    stringBuilder1.Append(stringBuilder2.ToString());
    return stringBuilder1.ToString();
  }

  public List<DeckMaker.DeckFill> GetDeckFillFromString(string deckString)
  {
    List<DeckMaker.DeckFill> deckFillList = new List<DeckMaker.DeckFill>();
    string str1 = deckString;
    char[] chArray1 = new char[1]{ '\n' };
    foreach (string str2 in str1.Split(chArray1))
    {
      string str3 = str2.Trim();
      if (!str3.StartsWith("#"))
      {
        try
        {
          string str4 = str3;
          char[] chArray2 = new char[1]{ ';' };
          foreach (string str5 in str4.Split(chArray2))
          {
            try
            {
              string[] strArray = str5.Split(',');
              int result;
              if (int.TryParse(strArray[0], out result))
              {
                if (result >= 0)
                {
                  if (result <= 10)
                  {
                    EntityDef entityDef = DefLoader.Get().GetEntityDef(strArray[1]);
                    if (entityDef != null)
                    {
                      for (int index = 0; index < result; ++index)
                        deckFillList.Add(new DeckMaker.DeckFill()
                        {
                          m_addCard = entityDef
                        });
                    }
                  }
                }
              }
            }
            catch
            {
            }
          }
        }
        catch
        {
        }
      }
    }
    return deckFillList;
  }

  public bool ShouldSplitSlotsByValidity()
  {
    if (this.Locked)
      return false;
    switch (this.Type)
    {
      case DeckType.DRAFT_DECK:
        return false;
      case DeckType.TAVERN_BRAWL_DECK:
        return TavernBrawlManager.Get().IsTavernBrawlActive && TavernBrawlManager.Get().GetDeckRuleset() != null && TavernBrawlManager.Get().GetDeckRuleset().HasOwnershipOrRotatedRule();
      default:
        return true;
    }
  }
}
