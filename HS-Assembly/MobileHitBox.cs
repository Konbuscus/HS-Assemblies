﻿// Decompiled with JetBrains decompiler
// Type: MobileHitBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MobileHitBox : MonoBehaviour
{
  public float m_scaleX = 1f;
  public float m_scaleY = 1f;
  private PlatformDependentValue<bool> m_isMobile = new PlatformDependentValue<bool>(PlatformCategory.Screen) { Tablet = true, MiniTablet = true, Phone = true, PC = false };
  public BoxCollider m_boxCollider;
  public float m_scaleZ;
  public Vector3 m_offset;
  public bool m_phoneOnly;
  private bool m_hasExecuted;

  private void Start()
  {
    if (!((Object) this.m_boxCollider != (Object) null) || !(bool) this.m_isMobile || this.m_phoneOnly && !(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_boxCollider.size = new Vector3()
    {
      x = this.m_boxCollider.size.x * this.m_scaleX,
      y = this.m_boxCollider.size.y * this.m_scaleY,
      z = (double) this.m_scaleZ != 0.0 ? this.m_boxCollider.size.z * this.m_scaleZ : this.m_boxCollider.size.z * this.m_scaleY
    };
    this.m_boxCollider.center += this.m_offset;
    this.m_hasExecuted = true;
  }

  public bool HasExecuted()
  {
    return this.m_hasExecuted;
  }
}
