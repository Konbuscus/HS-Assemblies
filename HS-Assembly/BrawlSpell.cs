﻿// Decompiled with JetBrains decompiler
// Type: BrawlSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BrawlSpell : Spell
{
  public float m_MinJumpHeight = 1.5f;
  public float m_MaxJumpHeight = 2.5f;
  public float m_MinJumpInDelay = 0.1f;
  public float m_MaxJumpInDelay = 0.2f;
  public float m_JumpInDuration = 1.5f;
  public iTween.EaseType m_JumpInEaseType = iTween.EaseType.linear;
  public float m_HoldTime = 0.1f;
  public float m_MinJumpOutDelay = 0.1f;
  public float m_MaxJumpOutDelay = 0.2f;
  public float m_JumpOutDuration = 1.5f;
  public iTween.EaseType m_JumpOutEaseType = iTween.EaseType.easeOutBounce;
  public float m_SurvivorHoldDuration = 0.5f;
  public List<GameObject> m_LeftJumpOutBones;
  public List<GameObject> m_RightJumpOutBones;
  public AudioSource m_JumpInSoundPrefab;
  public float m_JumpInSoundDelay;
  public AudioSource m_JumpOutSoundPrefab;
  public float m_JumpOutSoundDelay;
  public AudioSource m_LandSoundPrefab;
  public float m_LandSoundDelay;
  private int m_jumpsPending;
  private Card m_survivorCard;

  protected override void OnAction(SpellStateType prevStateType)
  {
    if (this.m_targets.Count > 0)
    {
      this.m_survivorCard = this.FindSurvivor();
      this.StartJumpIns();
    }
    else
    {
      this.OnSpellFinished();
      this.OnStateFinished();
    }
  }

  private Card FindSurvivor()
  {
    using (List<GameObject>.Enumerator enumerator1 = this.m_targets.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        GameObject current = enumerator1.Current;
        bool flag = true;
        Card component = current.GetComponent<Card>();
        using (List<PowerTask>.Enumerator enumerator2 = this.m_taskList.GetTaskList().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Network.PowerHistory power = enumerator2.Current.GetPower();
            if (power.Type == Network.PowerType.TAG_CHANGE)
            {
              Network.HistTagChange histTagChange = power as Network.HistTagChange;
              if (histTagChange.Tag == 360 && histTagChange.Value == 1)
              {
                Entity entity = GameState.Get().GetEntity(histTagChange.Entity);
                if (entity == null)
                  UnityEngine.Debug.LogWarning((object) string.Format("{0}.FindSurvivor() - WARNING trying to get entity with id {1} but there is no entity with that id", (object) this, (object) histTagChange.Entity));
                else if ((Object) component == (Object) entity.GetCard())
                {
                  flag = false;
                  break;
                }
              }
            }
          }
        }
        if (flag)
          return component;
      }
    }
    return (Card) null;
  }

  private void StartJumpIns()
  {
    this.m_jumpsPending = this.m_targets.Count;
    List<Card> cardList = new List<Card>(this.m_jumpsPending);
    using (List<GameObject>.Enumerator enumerator = this.m_targets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card component = enumerator.Current.GetComponent<Card>();
        cardList.Add(component);
      }
    }
    float startSec = 0.0f;
    while (cardList.Count > 0)
    {
      int index = Random.Range(0, cardList.Count);
      Card card = cardList[index];
      cardList.RemoveAt(index);
      this.StartJumpIn(card, ref startSec);
    }
  }

  private void StartJumpIn(Card card, ref float startSec)
  {
    float num = Random.Range(this.m_MinJumpInDelay, this.m_MaxJumpInDelay);
    this.StartCoroutine(this.JumpIn(card, startSec + num));
    startSec = startSec + num;
  }

  [DebuggerHidden]
  private IEnumerator JumpIn(Card card, float delaySec)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BrawlSpell.\u003CJumpIn\u003Ec__Iterator255() { delaySec = delaySec, card = card, \u003C\u0024\u003EdelaySec = delaySec, \u003C\u0024\u003Ecard = card, \u003C\u003Ef__this = this };
  }

  private void OnJumpInComplete(Card targetCard)
  {
    targetCard.HideCard();
    --this.m_jumpsPending;
    if (this.m_jumpsPending > 0)
      return;
    this.StartCoroutine(this.Hold());
  }

  [DebuggerHidden]
  private IEnumerator Hold()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BrawlSpell.\u003CHold\u003Ec__Iterator256() { \u003C\u003Ef__this = this };
  }

  private void StartJumpOuts()
  {
    this.m_jumpsPending = this.m_targets.Count - 1;
    List<int> usedBoneIndexes1 = new List<int>();
    List<int> usedBoneIndexes2 = new List<int>();
    float num1 = 0.0f;
    bool flag = true;
    for (int index = 0; index < this.m_targets.Count; ++index)
    {
      Card component = this.m_targets[index].GetComponent<Card>();
      if (!((Object) component == (Object) this.m_survivorCard))
      {
        GameObject freeBone;
        if (flag)
        {
          freeBone = this.GetFreeBone(this.m_LeftJumpOutBones, usedBoneIndexes1);
          if ((Object) freeBone == (Object) null)
          {
            usedBoneIndexes1.Clear();
            freeBone = this.GetFreeBone(this.m_LeftJumpOutBones, usedBoneIndexes1);
          }
        }
        else
        {
          freeBone = this.GetFreeBone(this.m_RightJumpOutBones, usedBoneIndexes2);
          if ((Object) freeBone == (Object) null)
          {
            usedBoneIndexes2.Clear();
            freeBone = this.GetFreeBone(this.m_RightJumpOutBones, usedBoneIndexes2);
          }
        }
        float num2 = Random.Range(this.m_MinJumpOutDelay, this.m_MaxJumpOutDelay);
        this.StartCoroutine(this.JumpOut(component, num1 + num2, freeBone.transform.position));
        num1 += num2;
        flag = !flag;
      }
    }
  }

  private GameObject GetFreeBone(List<GameObject> boneList, List<int> usedBoneIndexes)
  {
    List<int> intList = new List<int>();
    for (int index = 0; index < boneList.Count; ++index)
    {
      if (!usedBoneIndexes.Contains(index))
        intList.Add(index);
    }
    if (intList.Count == 0)
      return (GameObject) null;
    int index1 = Random.Range(0, intList.Count - 1);
    int index2 = intList[index1];
    usedBoneIndexes.Add(index2);
    return boneList[index2];
  }

  [DebuggerHidden]
  private IEnumerator JumpOut(Card card, float delaySec, Vector3 destPos)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BrawlSpell.\u003CJumpOut\u003Ec__Iterator257() { delaySec = delaySec, card = card, destPos = destPos, \u003C\u0024\u003EdelaySec = delaySec, \u003C\u0024\u003Ecard = card, \u003C\u0024\u003EdestPos = destPos, \u003C\u003Ef__this = this };
  }

  private void OnJumpOutComplete(Card targetCard)
  {
    --this.m_jumpsPending;
    if (this.m_jumpsPending > 0)
      return;
    this.ActivateState(SpellStateType.DEATH);
    this.StartCoroutine(this.SurvivorHold());
  }

  [DebuggerHidden]
  private IEnumerator SurvivorHold()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BrawlSpell.\u003CSurvivorHold\u003Ec__Iterator258() { \u003C\u003Ef__this = this };
  }

  private bool IsSurvivorAlone()
  {
    Zone zone = this.m_survivorCard.GetZone();
    using (List<GameObject>.Enumerator enumerator = this.m_targets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card component = enumerator.Current.GetComponent<Card>();
        if (!((Object) component == (Object) this.m_survivorCard) && (Object) component.GetZone() == (Object) zone)
          return false;
      }
    }
    return true;
  }

  [DebuggerHidden]
  private IEnumerator LoadAndPlaySound(AudioSource prefab, float delaySec)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BrawlSpell.\u003CLoadAndPlaySound\u003Ec__Iterator259() { prefab = prefab, delaySec = delaySec, \u003C\u0024\u003Eprefab = prefab, \u003C\u0024\u003EdelaySec = delaySec, \u003C\u003Ef__this = this };
  }
}
