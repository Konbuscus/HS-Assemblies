﻿// Decompiled with JetBrains decompiler
// Type: AdventureMissionDeckPickerDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class AdventureMissionDeckPickerDisplay : MonoBehaviour
{
  public GameObject m_deckPickerTrayContainer;
  private DeckPickerTrayDisplay m_deckPickerTray;

  private void Awake()
  {
    GameObject gameObject = AssetLoader.Get().LoadActor(!(bool) UniversalInputManager.UsePhoneUI ? "DeckPickerTray" : "DeckPickerTray_phone", false, false);
    if ((Object) gameObject == (Object) null)
    {
      Debug.LogError((object) "Unable to load DeckPickerTray.");
    }
    else
    {
      this.m_deckPickerTray = gameObject.GetComponent<DeckPickerTrayDisplay>();
      if ((Object) this.m_deckPickerTray == (Object) null)
      {
        Debug.LogError((object) "DeckPickerTrayDisplay component not found in DeckPickerTray object.");
      }
      else
      {
        if ((Object) this.m_deckPickerTrayContainer != (Object) null)
          GameUtils.SetParent((Component) this.m_deckPickerTray, this.m_deckPickerTrayContainer, false);
        this.m_deckPickerTray.AddDeckTrayLoadedListener(new DeckPickerTrayDisplay.DeckTrayLoaded(this.OnTrayLoaded));
        this.m_deckPickerTray.Init();
        this.m_deckPickerTray.SetPlayButtonText(GameStrings.Get("GLOBAL_PLAY"));
        AdventureConfig adventureConfig = AdventureConfig.Get();
        this.m_deckPickerTray.SetHeaderText((string) GameUtils.GetAdventureDataRecord((int) adventureConfig.GetSelectedAdventure(), (int) adventureConfig.GetSelectedMode()).Name);
      }
    }
  }

  private void OnTrayLoaded()
  {
    AdventureSubScene component = this.GetComponent<AdventureSubScene>();
    if (!((Object) component != (Object) null))
      return;
    component.SetIsLoaded(true);
  }
}
