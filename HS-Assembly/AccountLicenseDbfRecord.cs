﻿// Decompiled with JetBrains decompiler
// Type: AccountLicenseDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AccountLicenseDbfRecord : DbfRecord
{
  [SerializeField]
  private long m_LicenseId;

  [DbfField("LICENSE_ID", "the unique id of the license")]
  public long LicenseId
  {
    get
    {
      return this.m_LicenseId;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    AccountLicenseDbfAsset accountLicenseDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (AccountLicenseDbfAsset)) as AccountLicenseDbfAsset;
    if ((UnityEngine.Object) accountLicenseDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("AccountLicenseDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < accountLicenseDbfAsset.Records.Count; ++index)
      accountLicenseDbfAsset.Records[index].StripUnusedLocales();
    records = (object) accountLicenseDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetLicenseId(long v)
  {
    this.m_LicenseId = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AccountLicenseDbfRecord.\u003C\u003Ef__switch\u0024map2 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AccountLicenseDbfRecord.\u003C\u003Ef__switch\u0024map2 = new Dictionary<string, int>(2)
        {
          {
            "ID",
            0
          },
          {
            "LICENSE_ID",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AccountLicenseDbfRecord.\u003C\u003Ef__switch\u0024map2.TryGetValue(key, out num))
      {
        if (num == 0)
          return (object) this.ID;
        if (num == 1)
          return (object) this.LicenseId;
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (AccountLicenseDbfRecord.\u003C\u003Ef__switch\u0024map3 == null)
    {
      // ISSUE: reference to a compiler-generated field
      AccountLicenseDbfRecord.\u003C\u003Ef__switch\u0024map3 = new Dictionary<string, int>(2)
      {
        {
          "ID",
          0
        },
        {
          "LICENSE_ID",
          1
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!AccountLicenseDbfRecord.\u003C\u003Ef__switch\u0024map3.TryGetValue(key, out num))
      return;
    if (num != 0)
    {
      if (num != 1)
        return;
      this.SetLicenseId((long) val);
    }
    else
      this.SetID((int) val);
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AccountLicenseDbfRecord.\u003C\u003Ef__switch\u0024map4 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AccountLicenseDbfRecord.\u003C\u003Ef__switch\u0024map4 = new Dictionary<string, int>(2)
        {
          {
            "ID",
            0
          },
          {
            "LICENSE_ID",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AccountLicenseDbfRecord.\u003C\u003Ef__switch\u0024map4.TryGetValue(key, out num))
      {
        if (num == 0)
          return typeof (int);
        if (num == 1)
          return typeof (long);
      }
    }
    return (System.Type) null;
  }
}
