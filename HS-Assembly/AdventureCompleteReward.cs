﻿// Decompiled with JetBrains decompiler
// Type: AdventureCompleteReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class AdventureCompleteReward : Reward
{
  private const string s_EventShowHurt = "ShowHurt";
  private const string s_EventShowBadlyHurt = "ShowBadlyHurt";
  private const string s_EventHide = "Hide";
  [CustomEditField(Sections = "State Event Table")]
  public StateEventTable m_StateTable;
  [CustomEditField(Sections = "Banner")]
  public UberText m_BannerTextObject;
  [CustomEditField(Sections = "Banner")]
  public GameObject m_BannerObject;
  [CustomEditField(Sections = "Banner")]
  public Vector3_MobileOverride m_BannerScaleOverride;

  protected override void InitData()
  {
    this.SetData((RewardData) new AdventureCompleteRewardData(), false);
  }

  protected override void ShowReward(bool updateCacheValues)
  {
    if (this.IsShown)
      return;
    AdventureCompleteRewardData data = this.Data as AdventureCompleteRewardData;
    if ((Object) this.m_StateTable != (Object) null)
      this.m_StateTable.TriggerState(data.ModeId != AdventureModeDbId.HEROIC || !this.m_StateTable.HasState("ShowBadlyHurt") ? "ShowHurt" : "ShowBadlyHurt", true, (string) null);
    if ((Object) this.m_BannerTextObject != (Object) null)
      this.m_BannerTextObject.Text = data.BannerText;
    if ((Object) this.m_BannerObject != (Object) null && this.m_BannerScaleOverride != null)
    {
      Vector3 bannerScaleOverride = (Vector3) ((MobileOverrideValue<Vector3>) this.m_BannerScaleOverride);
      if (bannerScaleOverride != Vector3.zero)
        this.m_BannerObject.transform.localScale = bannerScaleOverride;
    }
    this.FadeFullscreenEffectsIn();
  }

  protected override void PlayShowSounds()
  {
  }

  protected override void HideReward()
  {
    if (!this.IsShown)
      return;
    base.HideReward();
    if ((Object) this.m_StateTable != (Object) null)
      this.m_StateTable.TriggerState("Hide", true, (string) null);
    this.FadeFullscreenEffectsOut();
  }

  private void FadeFullscreenEffectsIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((Object) fullScreenFxMgr == (Object) null)
    {
      Debug.LogWarning((object) "AdventureCompleteReward: FullScreenFXMgr.Get() returned null!");
    }
    else
    {
      fullScreenFxMgr.SetBlurBrightness(0.85f);
      fullScreenFxMgr.SetBlurDesaturation(0.0f);
      fullScreenFxMgr.Vignette(0.4f, 0.5f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      fullScreenFxMgr.Blur(1f, 0.5f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
  }

  private void FadeFullscreenEffectsOut()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((Object) fullScreenFxMgr == (Object) null)
    {
      Debug.LogWarning((object) "AdventureCompleteReward: FullScreenFXMgr.Get() returned null!");
    }
    else
    {
      fullScreenFxMgr.StopVignette(1f, iTween.EaseType.easeOutCirc, new FullScreenFXMgr.EffectListener(this.DestroyThis));
      fullScreenFxMgr.StopBlur(1f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
  }

  protected override void OnDataSet(bool updateVisuals)
  {
    if (!updateVisuals)
      return;
    if (!(this.Data is AdventureCompleteRewardData))
    {
      Debug.LogWarning((object) string.Format("AdventureCompleteReward.OnDataSet() - Data {0} is not AdventureCompleteRewardData", (object) this.Data));
    }
    else
    {
      this.EnableClickCatcher(true);
      this.RegisterClickListener((Reward.OnClickedCallback) ((reward, userData) => this.HideReward()));
      this.SetReady(true);
    }
  }

  private void DestroyThis()
  {
    Object.DestroyImmediate((Object) this.gameObject);
  }
}
