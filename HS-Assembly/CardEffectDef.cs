﻿// Decompiled with JetBrains decompiler
// Type: CardEffectDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[Serializable]
public class CardEffectDef
{
  [CustomEditField(T = EditType.CARD_SOUND_SPELL)]
  public List<string> m_SoundSpellPaths = new List<string>();
  [CustomEditField(T = EditType.SPELL)]
  public string m_SpellPath;
}
