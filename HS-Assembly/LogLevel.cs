﻿// Decompiled with JetBrains decompiler
// Type: LogLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.ComponentModel;

public enum LogLevel
{
  [Description("None")] None,
  [Description("Debug")] Debug,
  [Description("Info")] Info,
  [Description("Warning")] Warning,
  [Description("Error")] Error,
}
