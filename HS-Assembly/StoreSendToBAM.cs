﻿// Decompiled with JetBrains decompiler
// Type: StoreSendToBAM
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class StoreSendToBAM : UIBPopup
{
  private static readonly string SEND_TO_BAM_THEN_HIDE_COROUTINE = "SendToBAMThenHide";
  private static readonly string FMT_URL_RESET_PASSWORD = "https://nydus.battle.net/WTCG/{0}/client/password-reset?targetRegion={1}";
  private static readonly PlatformDependentValue<string> FMT_URL_PAYMENT_INFO = new PlatformDependentValue<string>(PlatformCategory.OS) { PC = "https://nydus.battle.net/WTCG/{0}/client/support/purchase?targetRegion={1}", iOS = "https://nydus.battle.net/WTCG/{0}/client/support/purchase?targetRegion={1}&targetDevice=ipad", Android = "https://nydus.battle.net/WTCG/{0}/client/support/purchase?targetRegion={1}&targetDevice=android" };
  private static readonly PlatformDependentValue<string> GLUE_STORE_PAYMENT_INFO_DETAILS = new PlatformDependentValue<string>(PlatformCategory.OS) { PC = "GLUE_STORE_PAYMENT_INFO_DETAILS", iOS = "GLUE_MOBILE_STORE_PAYMENT_INFO_DETAILS_APPLE", Android = "GLUE_MOBILE_STORE_PAYMENT_INFO_DETAILS_ANDROID" };
  private static readonly PlatformDependentValue<string> GLUE_STORE_PAYMENT_INFO_URL_DETAILS = new PlatformDependentValue<string>(PlatformCategory.OS) { PC = "GLUE_STORE_PAYMENT_INFO_URL_DETAILS", iOS = "GLUE_MOBILE_STORE_PAYMENT_INFO_URL_DETAILS", Android = "GLUE_MOBILE_STORE_PAYMENT_INFO_URL_DETAILS" };
  private static readonly Vector3 SHOW_MINI_SUMMARY_SCALE_PHONE = new Vector3(80f, 80f, 80f);
  private Vector3 m_originalShowScale = Vector3.zero;
  private List<StoreSendToBAM.DelOKListener> m_okayListeners = new List<StoreSendToBAM.DelOKListener>();
  private List<StoreSendToBAM.DelCancelListener> m_cancelListeners = new List<StoreSendToBAM.DelCancelListener>();
  private string m_errorCode = string.Empty;
  private const string FMT_URL_NO_PAYMENT_METHOD = "https://nydus.battle.net/WTCG/{0}/client/add-payment?targetRegion={1}&flowId=1";
  private const string FMT_URL_PAYMENT_EXPIRED = "https://nydus.battle.net/WTCG/{0}/client/add-payment?targetRegion={1}&flowId=5";
  private const string FMT_URL_GENERIC_PAYMENT_FAIL = "https://nydus.battle.net/WTCG/{0}/client/add-payment?targetRegion={1}&flowId=4";
  private const string FMT_URL_EULA_AND_TOS = "https://nydus.battle.net/WTCG/{0}/client/legal/terms-of-sale?targetRegion={1}";
  private const string FMT_URL_PURCHASE_UNIQUENESS_VIOLATED = "https://nydus.battle.net/WTCG/{0}/client/support/already-owned?targetRegion={1}";
  public UIBButton m_okayButton;
  public UIBButton m_cancelButton;
  public UberText m_headlineText;
  public UberText m_messageText;
  public MultiSliceElement m_allSections;
  public GameObject m_midSection;
  public GameObject m_sendToBAMRoot;
  public Transform m_sendToBAMRootWithSummaryBone;
  public StoreMiniSummary m_miniSummary;
  public PegUIElement m_offClickCatcher;
  private StoreSendToBAM.BAMReason m_sendToBAMReason;
  private MoneyOrGTAPPTransaction m_moneyOrGTAPPTransaction;
  private static Map<StoreSendToBAM.BAMReason, StoreSendToBAM.SendToBAMText> s_bamTextMap;

  private void Awake()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_scaleMode = CanvasScaleMode.WIDTH;
    if ((Object) this.m_offClickCatcher != (Object) null)
      this.m_offClickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCancelPressed));
    StoreSendToBAM.s_bamTextMap = new Map<StoreSendToBAM.BAMReason, StoreSendToBAM.SendToBAMText>()
    {
      {
        StoreSendToBAM.BAMReason.PAYMENT_INFO,
        new StoreSendToBAM.SendToBAMText("GLUE_STORE_PAYMENT_INFO_HEADLINE", (string) StoreSendToBAM.GLUE_STORE_PAYMENT_INFO_DETAILS, (string) StoreSendToBAM.GLUE_STORE_PAYMENT_INFO_URL_DETAILS, (string) StoreSendToBAM.FMT_URL_PAYMENT_INFO, StoreURL.Param.LOCALE, StoreURL.Param.REGION)
      },
      {
        StoreSendToBAM.BAMReason.NEED_PASSWORD_RESET,
        new StoreSendToBAM.SendToBAMText("GLUE_STORE_FORGOT_PWD_HEADLINE", "GLUE_STORE_FORGOT_PWD_DETAILS", "GLUE_STORE_FORGOT_PWD_URL_DETAILS", StoreSendToBAM.FMT_URL_RESET_PASSWORD, StoreURL.Param.LOCALE, StoreURL.Param.REGION)
      },
      {
        StoreSendToBAM.BAMReason.NO_VALID_PAYMENT_METHOD,
        new StoreSendToBAM.SendToBAMText("GLUE_STORE_NO_PAYMENT_HEADLINE", "GLUE_STORE_NO_PAYMENT_DETAILS", "GLUE_STORE_NO_PAYMENT_URL_DETAILS", "https://nydus.battle.net/WTCG/{0}/client/add-payment?targetRegion={1}&flowId=1", StoreURL.Param.LOCALE, StoreURL.Param.REGION)
      },
      {
        StoreSendToBAM.BAMReason.CREDIT_CARD_EXPIRED,
        new StoreSendToBAM.SendToBAMText("GLUE_STORE_GENERIC_BP_FAIL_HEADLINE", "GLUE_STORE_CC_EXPIRY_DETAILS", "GLUE_STORE_GENERIC_BP_FAIL_URL_DETAILS", "https://nydus.battle.net/WTCG/{0}/client/add-payment?targetRegion={1}&flowId=5", StoreURL.Param.LOCALE, StoreURL.Param.REGION)
      },
      {
        StoreSendToBAM.BAMReason.GENERIC_PAYMENT_FAIL,
        new StoreSendToBAM.SendToBAMText("GLUE_STORE_GENERIC_BP_FAIL_HEADLINE", "GLUE_STORE_GENERIC_BP_FAIL_DETAILS", "GLUE_STORE_GENERIC_BP_FAIL_URL_DETAILS", "https://nydus.battle.net/WTCG/{0}/client/add-payment?targetRegion={1}&flowId=4", StoreURL.Param.LOCALE, StoreURL.Param.REGION)
      },
      {
        StoreSendToBAM.BAMReason.EULA_AND_TOS,
        new StoreSendToBAM.SendToBAMText("GLUE_STORE_EULA_AND_TOS_HEADLINE", "GLUE_STORE_EULA_AND_TOS_DETAILS", "GLUE_STORE_EULA_AND_TOS_URL_DETAILS", "https://nydus.battle.net/WTCG/{0}/client/legal/terms-of-sale?targetRegion={1}", StoreURL.Param.LOCALE, StoreURL.Param.REGION)
      },
      {
        StoreSendToBAM.BAMReason.PRODUCT_UNIQUENESS_VIOLATED,
        new StoreSendToBAM.SendToBAMText("GLUE_STORE_PURCHASE_LOCK_HEADER", "GLUE_STORE_FAIL_PRODUCT_UNIQUENESS_VIOLATED", "GLUE_STORE_FAIL_PRODUCT_UNIQUENESS_VIOLATED_URL", "https://nydus.battle.net/WTCG/{0}/client/support/already-owned?targetRegion={1}", StoreURL.Param.LOCALE, StoreURL.Param.REGION)
      },
      {
        StoreSendToBAM.BAMReason.GENERIC_PURCHASE_FAIL_RETRY_CONTACT_CS_IF_PERSISTS,
        new StoreSendToBAM.SendToBAMText("GLUE_STORE_GENERIC_BP_FAIL_HEADLINE", "GLUE_STORE_GENERIC_BP_FAIL_RETRY_CONTACT_CS_IF_PERSISTS_DETAILS", "GLUE_STORE_GENERIC_BP_FAIL_RETRY_CONTACT_CS_IF_PERSISTS_URL_DETAILS", "https://nydus.battle.net/WTCG/{0}/client/add-payment?targetRegion={1}&flowId=4", StoreURL.Param.LOCALE, StoreURL.Param.REGION)
      }
    };
    this.m_okayButton.SetText(GameStrings.Get("GLOBAL_MORE"));
    this.m_cancelButton.SetText(GameStrings.Get("GLOBAL_CANCEL"));
    this.m_okayButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnOkayPressed));
    this.m_cancelButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCancelPressed));
  }

  public void Show(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, StoreSendToBAM.BAMReason reason, string errorCode, bool fromPreviousPurchase)
  {
    this.m_moneyOrGTAPPTransaction = moneyOrGTAPPTransaction;
    this.m_sendToBAMReason = reason;
    this.m_errorCode = errorCode;
    this.UpdateText();
    if (moneyOrGTAPPTransaction != null && (fromPreviousPurchase || moneyOrGTAPPTransaction.ShouldShowMiniSummary()))
    {
      this.m_sendToBAMRoot.transform.position = this.m_sendToBAMRootWithSummaryBone.position;
      this.m_miniSummary.SetDetails(this.m_moneyOrGTAPPTransaction.ProductID, 1);
      this.m_miniSummary.gameObject.SetActive(true);
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        this.m_originalShowScale = this.m_showScale;
        this.m_showScale = StoreSendToBAM.SHOW_MINI_SUMMARY_SCALE_PHONE;
      }
    }
    else
    {
      this.m_sendToBAMRoot.transform.localPosition = Vector3.zero;
      this.m_miniSummary.gameObject.SetActive(false);
      if ((bool) UniversalInputManager.UsePhoneUI && this.m_originalShowScale != Vector3.zero)
      {
        this.m_showScale = this.m_originalShowScale;
        this.m_originalShowScale = Vector3.zero;
      }
    }
    if (this.m_shown)
      return;
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnCancel));
    this.m_shown = true;
    this.m_headlineText.UpdateNow();
    this.LayoutMessageText();
    this.DoShowAnimation((UIBPopup.OnAnimationComplete) null);
  }

  public void RegisterOkayListener(StoreSendToBAM.DelOKListener listener)
  {
    if (this.m_okayListeners.Contains(listener))
      return;
    this.m_okayListeners.Add(listener);
  }

  public void RemoveOkayListener(StoreSendToBAM.DelOKListener listener)
  {
    this.m_okayListeners.Remove(listener);
  }

  public void RegisterCancelListener(StoreSendToBAM.DelCancelListener listener)
  {
    if (this.m_cancelListeners.Contains(listener))
      return;
    this.m_cancelListeners.Add(listener);
  }

  public void RemoveCancelListener(StoreSendToBAM.DelCancelListener listener)
  {
    this.m_cancelListeners.Remove(listener);
  }

  protected override void OnHidden()
  {
    this.m_okayButton.SetEnabled(true);
  }

  private void OnOkayPressed(UIEvent e)
  {
    this.StopCoroutine(StoreSendToBAM.SEND_TO_BAM_THEN_HIDE_COROUTINE);
    this.StartCoroutine(StoreSendToBAM.SEND_TO_BAM_THEN_HIDE_COROUTINE);
  }

  [DebuggerHidden]
  private IEnumerator SendToBAMThenHide()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StoreSendToBAM.\u003CSendToBAMThenHide\u003Ec__Iterator277() { \u003C\u003Ef__this = this };
  }

  private bool OnCancel()
  {
    this.StopCoroutine(StoreSendToBAM.SEND_TO_BAM_THEN_HIDE_COROUTINE);
    this.Hide(true);
    foreach (StoreSendToBAM.DelCancelListener delCancelListener in this.m_cancelListeners.ToArray())
      delCancelListener(this.m_moneyOrGTAPPTransaction);
    return true;
  }

  private void OnCancelPressed(UIEvent e)
  {
    Navigation.GoBack();
  }

  private void UpdateText()
  {
    StoreSendToBAM.SendToBAMText bamText = StoreSendToBAM.s_bamTextMap[this.m_sendToBAMReason];
    if (bamText == null)
    {
      UnityEngine.Debug.LogError((object) string.Format("StoreSendToBAM.UpdateText(): don't know how to update text for BAM reason {0}", (object) this.m_sendToBAMReason));
      this.m_headlineText.Text = string.Empty;
      this.m_messageText.Text = string.Empty;
    }
    else
    {
      string str1 = bamText.GetDetails();
      if (!string.IsNullOrEmpty(this.m_errorCode))
        str1 = str1 + " " + GameStrings.Format("GLUE_STORE_FAIL_DETAILS_ERROR_CODE", (object) this.m_errorCode);
      string str2 = str1 + "\n\n" + bamText.GetGoToURLDetails(this.m_okayButton.m_ButtonText.Text);
      this.m_headlineText.Text = bamText.GetHeadline();
      this.m_messageText.Text = str2;
    }
  }

  private void LayoutMessageText()
  {
    this.m_messageText.UpdateNow();
    TransformUtil.SetLocalScaleZ(this.m_midSection, 1f);
    TransformUtil.SetLocalScaleZ(this.m_midSection, this.m_messageText.GetTextWorldSpaceBounds().size.z / (TransformUtil.ComputeOrientedWorldBounds(this.m_midSection, true).Extents[2].magnitude * 2f));
    this.m_allSections.UpdateSlices();
  }

  public enum BAMReason
  {
    PAYMENT_INFO,
    NEED_PASSWORD_RESET,
    NO_VALID_PAYMENT_METHOD,
    CREDIT_CARD_EXPIRED,
    GENERIC_PAYMENT_FAIL,
    EULA_AND_TOS,
    PRODUCT_UNIQUENESS_VIOLATED,
    GENERIC_PURCHASE_FAIL_RETRY_CONTACT_CS_IF_PERSISTS,
  }

  private class SendToBAMText
  {
    private string m_headlineKey;
    private string m_detailsKey;
    private string m_goToURLKey;
    private StoreURL m_url;

    public SendToBAMText(string headlineKey, string detailsKey, string goToURLKey, string urlFmt, StoreURL.Param urlParam1, StoreURL.Param urlParam2)
    {
      this.m_headlineKey = headlineKey;
      this.m_detailsKey = detailsKey;
      this.m_goToURLKey = goToURLKey;
      this.m_url = new StoreURL(urlFmt, urlParam1, urlParam2);
    }

    public string GetHeadline()
    {
      return GameStrings.Get(this.m_headlineKey);
    }

    public string GetDetails()
    {
      return GameStrings.Get(this.m_detailsKey);
    }

    public string GetGoToURLDetails(string buttonName)
    {
      return GameStrings.Format(this.m_goToURLKey, (object) buttonName);
    }

    public string GetURL()
    {
      return this.m_url.GetURL();
    }
  }

  public delegate void DelOKListener(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, StoreSendToBAM.BAMReason reason);

  public delegate void DelCancelListener(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction);
}
