﻿// Decompiled with JetBrains decompiler
// Type: GeneralStoreAdventureContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class GeneralStoreAdventureContent : GeneralStoreContent
{
  public static readonly bool REQUIRE_REAL_MONEY_BUNDLE_OPTION = true;
  [CustomEditField(Sections = "Animation")]
  public float m_backgroundFlipAnimTime = 0.5f;
  private Map<string, Actor> m_loadedPreviewCards = new Map<string, Actor>();
  private Map<int, StoreAdventureDef> m_storeAdvDefs = new Map<int, StoreAdventureDef>();
  private int m_currentDisplay = -1;
  [CustomEditField(Sections = "General Store")]
  public GeneralStoreAdventureContentDisplay m_adventureDisplay;
  [CustomEditField(Sections = "Animation/Preorder")]
  public GeneralStoreRewardsCardBack m_preorderCardBackReward;
  [CustomEditField(Sections = "General Store")]
  public GameObject m_adventureEmptyDisplay;
  [CustomEditField(Sections = "Rewards")]
  public GameObject m_adventureCardPreviewPanel;
  [CustomEditField(Sections = "Rewards")]
  public UberText m_adventureCardPreviewText;
  [CustomEditField(Sections = "Rewards")]
  public List<GameObject> m_adventureCardPreviewBones;
  [CustomEditField(Sections = "Rewards")]
  public PegUIElement m_adventureCardPreviewOffClicker;
  [CustomEditField(Sections = "General Store/Buttons")]
  public GameObject m_adventureRadioButtonContainer;
  [CustomEditField(Sections = "General Store/Buttons")]
  public UberText m_adventureRadioButtonText;
  [CustomEditField(Sections = "General Store/Buttons")]
  public UberText m_adventureRadioButtonCostText;
  [CustomEditField(Sections = "General Store/Buttons")]
  public RadioButton m_adventureRadioButton;
  [CustomEditField(Sections = "General Store/Buttons")]
  public GameObject m_adventureOwnedCheckmark;
  [CustomEditField(Sections = "Sounds & Music", T = EditType.SOUND_PREFAB)]
  public string m_backgroundFlipSound;
  private bool m_showPreviewCards;
  private AdventureDbId m_selectedAdventureId;
  private GeneralStoreAdventureContentDisplay m_adventureDisplay1;
  private GeneralStoreAdventureContentDisplay m_adventureDisplay2;

  private void Awake()
  {
    this.m_adventureDisplay1 = this.m_adventureDisplay;
    this.m_adventureDisplay2 = UnityEngine.Object.Instantiate<GeneralStoreAdventureContentDisplay>(this.m_adventureDisplay);
    this.m_adventureDisplay2.transform.parent = this.m_adventureDisplay1.transform.parent;
    this.m_adventureDisplay2.transform.localPosition = this.m_adventureDisplay1.transform.localPosition;
    this.m_adventureDisplay2.transform.localScale = this.m_adventureDisplay1.transform.localScale;
    this.m_adventureDisplay2.transform.localRotation = this.m_adventureDisplay1.transform.localRotation;
    this.m_adventureDisplay2.gameObject.SetActive(false);
    if ((UnityEngine.Object) this.m_adventureDisplay1.m_rewardChest != (UnityEngine.Object) null)
    {
      this.m_adventureDisplay1.m_rewardChest.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnAdventuresShowPreviewCard));
      this.m_adventureDisplay2.m_rewardChest.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnAdventuresShowPreviewCard));
      if (!(bool) UniversalInputManager.UsePhoneUI)
      {
        this.m_adventureDisplay1.m_rewardChest.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnAdventuresHidePreviewCard));
        this.m_adventureDisplay2.m_rewardChest.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnAdventuresHidePreviewCard));
      }
    }
    AdventureProgressMgr.Get().RegisterProgressUpdatedListener(new AdventureProgressMgr.AdventureProgressUpdatedCallback(this.OnAdventureProgressUpdated));
    this.m_adventureCardPreviewPanel.SetActive(false);
    this.m_parentStore.SetChooseDescription(GameStrings.Get("GLUE_STORE_CHOOSE_ADVENTURE"));
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_adventureCardPreviewOffClicker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnAdventuresHidePreviewCard));
    using (List<AdventureDbfRecord>.Enumerator enumerator = GameUtils.GetSortedAdventureRecordsWithStorePrefab().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureDbfRecord current = enumerator.Current;
        string storePrefab = current.StorePrefab;
        GameObject gameObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(storePrefab), true, false);
        if (!((UnityEngine.Object) gameObject == (UnityEngine.Object) null))
        {
          StoreAdventureDef component = gameObject.GetComponent<StoreAdventureDef>();
          if ((UnityEngine.Object) component == (UnityEngine.Object) null)
            Debug.LogError((object) string.Format("StoreAdventureDef not found in object: {0}", (object) storePrefab));
          else
            this.m_storeAdvDefs.Add(current.ID, component);
        }
      }
    }
  }

  private void OnDestroy()
  {
    AdventureProgressMgr.Get().RemoveProgressUpdatedListener(new AdventureProgressMgr.AdventureProgressUpdatedCallback(this.OnAdventureProgressUpdated));
  }

  public void SetAdventureId(AdventureDbId adventureId, bool forceImmediate = false)
  {
    if (this.m_selectedAdventureId == adventureId)
      return;
    this.m_selectedAdventureId = adventureId;
    Network.Bundle bundle = (Network.Bundle) null;
    StoreManager.Get().GetAvailableAdventureBundle(this.m_selectedAdventureId, GeneralStoreAdventureContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, out bundle);
    this.SetCurrentMoneyBundle(bundle, false);
    this.AnimateAndUpdateDisplay((int) adventureId, forceImmediate);
    this.AnimateAdventureRadioButtonBar();
    this.UpdateAdventureDescription(bundle);
    this.UpdateAdventureTypeMusic();
    this.UpdateRadioButtonText(bundle);
  }

  public AdventureDbId GetAdventureId()
  {
    return this.m_selectedAdventureId;
  }

  public StoreAdventureDef GetStoreAdventureDef(int advId)
  {
    StoreAdventureDef storeAdventureDef = (StoreAdventureDef) null;
    this.m_storeAdvDefs.TryGetValue(advId, out storeAdventureDef);
    return storeAdventureDef;
  }

  public Map<int, StoreAdventureDef> GetStoreAdventureDefs()
  {
    return this.m_storeAdvDefs;
  }

  public override void PostStoreFlipIn(bool animateIn)
  {
    this.UpdateAdventureTypeMusic();
    if (!((UnityEngine.Object) this.m_preorderCardBackReward != (UnityEngine.Object) null) || !this.IsPreOrder())
      return;
    this.m_preorderCardBackReward.ShowCardBackReward();
  }

  public override void PreStoreFlipIn()
  {
    if (!((UnityEngine.Object) this.m_preorderCardBackReward != (UnityEngine.Object) null))
      return;
    this.m_preorderCardBackReward.HideCardBackReward();
  }

  public override void PreStoreFlipOut()
  {
    if (!((UnityEngine.Object) this.m_preorderCardBackReward != (UnityEngine.Object) null))
      return;
    this.m_preorderCardBackReward.HideCardBackReward();
  }

  public override bool AnimateEntranceEnd()
  {
    this.m_adventureRadioButton.gameObject.SetActive(true);
    return true;
  }

  public override bool AnimateExitStart()
  {
    this.m_adventureRadioButton.gameObject.SetActive(false);
    return true;
  }

  public override bool AnimateExitEnd()
  {
    return true;
  }

  public override void TryBuyWithMoney(Network.Bundle bundle, GeneralStoreContent.BuyEvent successBuyCB, GeneralStoreContent.BuyEvent failedBuyCB)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStoreAdventureContent.\u003CTryBuyWithMoney\u003Ec__AnonStorey3E6 moneyCAnonStorey3E6 = new GeneralStoreAdventureContent.\u003CTryBuyWithMoney\u003Ec__AnonStorey3E6();
    // ISSUE: reference to a compiler-generated field
    moneyCAnonStorey3E6.failedBuyCB = failedBuyCB;
    // ISSUE: reference to a compiler-generated field
    moneyCAnonStorey3E6.successBuyCB = successBuyCB;
    // ISSUE: reference to a compiler-generated field
    moneyCAnonStorey3E6.\u003C\u003Ef__this = this;
    if (this.IsContentActive())
    {
      if (!AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
      {
        AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
        info.m_headerText = GameStrings.Get("GLUE_STORE_ADVENTURE_LOCKED_HEROES_WARNING_TITLE");
        info.m_text = GameStrings.Get("GLUE_STORE_ADVENTURE_LOCKED_HEROES_WARNING_TEXT");
        info.m_showAlertIcon = true;
        info.m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL;
        // ISSUE: reference to a compiler-generated method
        info.m_responseCallback = new AlertPopup.ResponseCallback(moneyCAnonStorey3E6.\u003C\u003Em__1AE);
        this.m_parentStore.ActivateCover(true);
        DialogManager.Get().ShowPopup(info);
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        if (moneyCAnonStorey3E6.successBuyCB == null)
          return;
        // ISSUE: reference to a compiler-generated field
        moneyCAnonStorey3E6.successBuyCB();
      }
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      if (moneyCAnonStorey3E6.failedBuyCB == null)
        return;
      // ISSUE: reference to a compiler-generated field
      moneyCAnonStorey3E6.failedBuyCB();
    }
  }

  public override void TryBuyWithGold(GeneralStoreContent.BuyEvent successBuyCB = null, GeneralStoreContent.BuyEvent failedBuyCB = null)
  {
    if (successBuyCB == null)
      return;
    successBuyCB();
  }

  protected override void OnRefresh()
  {
    Network.Bundle bundle = (Network.Bundle) null;
    StoreManager.Get().GetAvailableAdventureBundle(this.m_selectedAdventureId, GeneralStoreAdventureContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, out bundle);
    this.SetCurrentMoneyBundle(bundle, false);
    this.UpdateRadioButtonText(bundle);
    this.UpdateAdventureDescription(bundle);
  }

  protected override void OnBundleChanged(NoGTAPPTransactionData goldBundle, Network.Bundle moneyBundle)
  {
    this.UpdateRadioButtonText(moneyBundle);
    this.UpdateAdventureDescription(moneyBundle);
  }

  public override void StoreShown(bool isCurrent)
  {
    if (!isCurrent)
      return;
    this.UpdateAdventureTypeMusic();
  }

  public override void StoreHidden(bool isCurrent)
  {
    using (Map<string, Actor>.Enumerator enumerator = this.m_loadedPreviewCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.Value.gameObject);
    }
    this.m_loadedPreviewCards.Clear();
    if (!isCurrent)
      return;
    this.HidePreviewCardPanel();
  }

  public override bool IsPurchaseDisabled()
  {
    return this.m_selectedAdventureId == AdventureDbId.INVALID;
  }

  public override string GetMoneyDisplayOwnedText()
  {
    return GameStrings.Get("GLUE_STORE_DUNGEON_BUTTON_COST_OWNED_TEXT");
  }

  private GameObject GetCurrentDisplayContainer()
  {
    return this.GetCurrentDisplay().gameObject;
  }

  private GameObject GetNextDisplayContainer()
  {
    if ((this.m_currentDisplay + 1) % 2 == 0)
      return this.m_adventureDisplay1.gameObject;
    return this.m_adventureDisplay2.gameObject;
  }

  private GeneralStoreAdventureContentDisplay GetCurrentDisplay()
  {
    if (this.m_currentDisplay == 0)
      return this.m_adventureDisplay1;
    return this.m_adventureDisplay2;
  }

  private void OnAdventuresShowPreviewCard(UIEvent e)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStoreAdventureContent.\u003COnAdventuresShowPreviewCard\u003Ec__AnonStorey3E8 cardCAnonStorey3E8 = new GeneralStoreAdventureContent.\u003COnAdventuresShowPreviewCard\u003Ec__AnonStorey3E8();
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey3E8.\u003C\u003Ef__this = this;
    StoreAdventureDef storeAdventureDef = this.GetStoreAdventureDef((int) this.m_selectedAdventureId);
    if ((UnityEngine.Object) storeAdventureDef == (UnityEngine.Object) null)
    {
      Debug.LogError((object) string.Format("Unable to find preview cards for {0} adventure.", (object) this.m_selectedAdventureId));
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      cardCAnonStorey3E8.previewCards = storeAdventureDef.m_previewCards.ToArray();
      // ISSUE: reference to a compiler-generated field
      if (cardCAnonStorey3E8.previewCards.Length == 0)
      {
        Debug.LogError((object) string.Format("No preview cards defined for {0} adventure.", (object) this.m_selectedAdventureId));
      }
      else
      {
        this.m_showPreviewCards = true;
        SoundManager.Get().LoadAndPlay("collection_manager_card_mouse_over");
        using (Map<string, Actor>.Enumerator enumerator = this.m_loadedPreviewCards.GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.Value.gameObject.SetActive(false);
        }
        // ISSUE: reference to a compiler-generated field
        cardCAnonStorey3E8.loadedPreviewCards = 0;
        int num = 0;
        // ISSUE: reference to a compiler-generated field
        foreach (string previewCard in cardCAnonStorey3E8.previewCards)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: reference to a compiler-generated method
          this.LoadAdventurePreviewCard(previewCard, new GeneralStoreAdventureContent.DelOnAdventurePreviewCardLoaded(new GeneralStoreAdventureContent.\u003COnAdventuresShowPreviewCard\u003Ec__AnonStorey3E7()
          {
            \u003C\u003Ef__ref\u00241000 = cardCAnonStorey3E8,
            \u003C\u003Ef__this = this,
            cardIndex = num
          }.\u003C\u003Em__1AF));
          ++num;
        }
      }
    }
  }

  private void LoadAdventurePreviewCard(string previewCard, GeneralStoreAdventureContent.DelOnAdventurePreviewCardLoaded onLoadComplete)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStoreAdventureContent.\u003CLoadAdventurePreviewCard\u003Ec__AnonStorey3E9 cardCAnonStorey3E9 = new GeneralStoreAdventureContent.\u003CLoadAdventurePreviewCard\u003Ec__AnonStorey3E9();
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey3E9.onLoadComplete = onLoadComplete;
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey3E9.previewCard = previewCard;
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey3E9.\u003C\u003Ef__this = this;
    Actor previewCard1 = (Actor) null;
    // ISSUE: reference to a compiler-generated field
    if (this.m_loadedPreviewCards.TryGetValue(cardCAnonStorey3E9.previewCard, out previewCard1))
    {
      // ISSUE: reference to a compiler-generated field
      cardCAnonStorey3E9.onLoadComplete(previewCard1);
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      DefLoader.Get().LoadFullDef(cardCAnonStorey3E9.previewCard, new DefLoader.LoadDefCallback<FullDef>(cardCAnonStorey3E9.\u003C\u003Em__1B0));
    }
  }

  private void OnAdventuresHidePreviewCard(UIEvent e)
  {
    this.m_showPreviewCards = false;
    SoundManager.Get().LoadAndPlay("card_shrink");
    this.HidePreviewCardPanel();
  }

  private void ShowPreviewCardPanel()
  {
    this.m_adventureCardPreviewPanel.SetActive(true);
    this.m_adventureCardPreviewPanel.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    iTween.StopByName(this.m_adventureCardPreviewPanel, "PreviewCardPanelScale");
    iTween.ScaleTo(this.m_adventureCardPreviewPanel, iTween.Hash((object) "scale", (object) Vector3.one, (object) "time", (object) 0.1f, (object) "name", (object) "PreviewCardPanelScale", (object) "easetype", (object) iTween.EaseType.linear));
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_parentStore.ActivateCover(true);
  }

  private void HidePreviewCardPanel()
  {
    iTween.StopByName(this.m_adventureCardPreviewPanel, "PreviewCardPanelScale");
    iTween.ScaleTo(this.m_adventureCardPreviewPanel, iTween.Hash((object) "scale", (object) new Vector3(0.02f, 0.02f, 0.02f), (object) "time", (object) 0.1f, (object) "name", (object) "PreviewCardPanelScale", (object) "oncomplete", (object) (Action<object>) (o =>
    {
      this.m_adventureCardPreviewPanel.SetActive(false);
      if (!(bool) UniversalInputManager.UsePhoneUI)
        return;
      this.m_parentStore.ActivateCover(false);
    }), (object) "easetype", (object) iTween.EaseType.linear));
  }

  private void UpdateRadioButtonText(Network.Bundle moneyBundle)
  {
    this.m_adventureRadioButton.SetSelected(true);
    if (moneyBundle == null)
    {
      this.m_adventureRadioButtonText.Text = GameStrings.Get("GLUE_STORE_DUNGEON_BUTTON_TEXT_PURCHASED");
      this.m_adventureRadioButtonText.Anchor = UberText.AnchorOptions.Middle;
      this.m_adventureRadioButtonCostText.Text = string.Empty;
    }
    else
    {
      string key;
      if (this.IsPreOrder())
      {
        AdventureDbfRecord record = GameDbf.Adventure.GetRecord((int) this.m_selectedAdventureId);
        key = record == null || string.IsNullOrEmpty((string) record.StorePreorderRadioText) ? "GLUE_STORE_DUNGEON_BUTTON_PREORDER_TEXT" : (string) record.StorePreorderRadioText;
      }
      else
        key = "GLUE_STORE_DUNGEON_BUTTON_TEXT";
      this.m_adventureRadioButtonText.Text = GameStrings.Get(key);
      this.m_adventureRadioButtonText.Anchor = UberText.AnchorOptions.Upper;
      string str = StoreManager.Get().FormatCostBundle(moneyBundle);
      this.m_adventureRadioButtonCostText.Text = GameStrings.Format("GLUE_STORE_DUNGEON_BUTTON_COST_TEXT", (object) StoreManager.Get().GetWingItemCount(moneyBundle.Items), (object) str);
    }
    if (!((UnityEngine.Object) this.m_adventureOwnedCheckmark != (UnityEngine.Object) null))
      return;
    this.m_adventureOwnedCheckmark.SetActive(moneyBundle == null);
  }

  private void UpdateAdventureDescription(Network.Bundle bundle)
  {
    if (this.m_selectedAdventureId != AdventureDbId.INVALID)
    {
      string title = string.Empty;
      string desc = string.Empty;
      string empty = string.Empty;
      AdventureDbfRecord record = GameDbf.Adventure.GetRecord((int) this.m_selectedAdventureId);
      if (record == null)
        Debug.LogError((object) string.Format("Unable to find adventure record ID: {0}", (object) this.m_selectedAdventureId));
      else if (bundle == null)
      {
        title = (string) record.StoreOwnedHeadline;
        desc = (string) record.StoreOwnedDesc;
      }
      else if (this.IsPreOrder())
      {
        title = (string) record.StorePreorderHeadline;
        int wingItemCount = StoreManager.Get().GetWingItemCount(bundle.Items);
        DbfLocValue var = record.GetVar(string.Format("STORE_PREORDER_WINGS_{0}_DESC", (object) wingItemCount)) as DbfLocValue;
        desc = var != null ? var.GetString(true) : string.Empty;
      }
      else
      {
        int wingItemCount = StoreManager.Get().GetWingItemCount(bundle.Items);
        DbfLocValue var1 = record.GetVar(string.Format("STORE_BUY_WINGS_{0}_HEADLINE", (object) wingItemCount)) as DbfLocValue;
        DbfLocValue var2 = record.GetVar(string.Format("STORE_BUY_WINGS_{0}_DESC", (object) wingItemCount)) as DbfLocValue;
        title = var1 != null ? var1.GetString(true) : string.Empty;
        desc = var2 != null ? var2.GetString(true) : string.Empty;
      }
      if (StoreManager.Get().IsKoreanCustomer())
        empty = GameStrings.Get("GLUE_STORE_KOREAN_PRODUCT_DETAILS_ADVENTURE");
      if ((UnityEngine.Object) this.m_adventureCardPreviewText != (UnityEngine.Object) null)
        this.m_adventureCardPreviewText.Text = (string) record.StorePreviewRewardsText;
      this.m_parentStore.SetDescription(title, desc, empty);
      StoreAdventureDef storeAdventureDef = this.GetStoreAdventureDef((int) this.m_selectedAdventureId);
      if (!((UnityEngine.Object) storeAdventureDef != (UnityEngine.Object) null))
        return;
      Texture texture = (Texture) null;
      if (!string.IsNullOrEmpty(storeAdventureDef.m_accentTextureName))
        texture = AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(storeAdventureDef.m_accentTextureName), false);
      this.m_parentStore.SetAccentTexture(texture);
    }
    else
    {
      this.m_parentStore.HideAccentTexture();
      this.m_parentStore.SetChooseDescription(GameStrings.Get("GLUE_STORE_CHOOSE_ADVENTURE"));
    }
  }

  private void UpdateAdventureTypeMusic()
  {
    if (this.m_parentStore.GetMode() == GeneralStoreMode.NONE)
      return;
    StoreAdventureDef storeAdventureDef = this.GetStoreAdventureDef((int) this.m_selectedAdventureId);
    if (!((UnityEngine.Object) storeAdventureDef == (UnityEngine.Object) null) && storeAdventureDef.m_playlist != MusicPlaylistType.Invalid && MusicManager.Get().StartPlaylist(storeAdventureDef.m_playlist))
      return;
    this.m_parentStore.ResumePreviousMusicPlaylist();
  }

  private void AnimateAndUpdateDisplay(int id, bool forceImmediate)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStoreAdventureContent.\u003CAnimateAndUpdateDisplay\u003Ec__AnonStorey3EB displayCAnonStorey3Eb = new GeneralStoreAdventureContent.\u003CAnimateAndUpdateDisplay\u003Ec__AnonStorey3EB();
    if ((UnityEngine.Object) this.m_preorderCardBackReward != (UnityEngine.Object) null)
      this.m_preorderCardBackReward.HideCardBackReward();
    // ISSUE: reference to a compiler-generated field
    displayCAnonStorey3Eb.currDisplay = (GameObject) null;
    if (this.m_currentDisplay == -1)
    {
      this.m_currentDisplay = 1;
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3Eb.currDisplay = this.m_adventureEmptyDisplay;
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3Eb.currDisplay = this.GetCurrentDisplayContainer();
    }
    GameObject displayContainer = this.GetNextDisplayContainer();
    this.m_currentDisplay = (this.m_currentDisplay + 1) % 2;
    displayContainer.SetActive(true);
    if (!forceImmediate)
    {
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3Eb.currDisplay.transform.localRotation = Quaternion.identity;
      displayContainer.transform.localEulerAngles = new Vector3(180f, 0.0f, 0.0f);
      // ISSUE: reference to a compiler-generated field
      iTween.StopByName(displayCAnonStorey3Eb.currDisplay, "ROTATION_TWEEN");
      iTween.StopByName(displayContainer, "ROTATION_TWEEN");
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      iTween.RotateBy(displayCAnonStorey3Eb.currDisplay, iTween.Hash((object) "amount", (object) new Vector3(0.5f, 0.0f, 0.0f), (object) "time", (object) 0.5f, (object) "name", (object) "ROTATION_TWEEN", (object) "oncomplete", (object) new Action<object>(displayCAnonStorey3Eb.\u003C\u003Em__1B2)));
      iTween.RotateBy(displayContainer, iTween.Hash((object) "amount", (object) new Vector3(0.5f, 0.0f, 0.0f), (object) "time", (object) 0.5f, (object) "name", (object) "ROTATION_TWEEN"));
      if (!string.IsNullOrEmpty(this.m_backgroundFlipSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_backgroundFlipSound));
    }
    else
    {
      displayContainer.transform.localRotation = Quaternion.identity;
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3Eb.currDisplay.transform.localEulerAngles = new Vector3(180f, 0.0f, 0.0f);
      // ISSUE: reference to a compiler-generated field
      displayCAnonStorey3Eb.currDisplay.SetActive(false);
    }
    AdventureDbfRecord record = GameDbf.Adventure.GetRecord(id);
    bool preorder = this.IsPreOrder();
    StoreAdventureDef storeAdventureDef = this.GetStoreAdventureDef(id);
    GeneralStoreAdventureContentDisplay currentDisplay = this.GetCurrentDisplay();
    currentDisplay.UpdateAdventureType(storeAdventureDef, record);
    currentDisplay.SetPreOrder(preorder);
    if (!((UnityEngine.Object) this.m_preorderCardBackReward != (UnityEngine.Object) null) || !preorder)
      return;
    this.m_preorderCardBackReward.SetCardBack(storeAdventureDef.m_preorderCardBackId);
    this.m_preorderCardBackReward.SetPreorderText(storeAdventureDef.m_preorderCardBackTextName);
    this.m_preorderCardBackReward.ShowCardBackReward();
  }

  private void AnimateAdventureRadioButtonBar()
  {
    if ((UnityEngine.Object) this.m_adventureRadioButtonContainer == (UnityEngine.Object) null)
      return;
    this.m_adventureRadioButtonContainer.SetActive(false);
    if (this.m_selectedAdventureId == AdventureDbId.INVALID)
      return;
    iTween.Stop(this.m_adventureRadioButtonContainer);
    this.m_adventureRadioButtonContainer.transform.localRotation = Quaternion.identity;
    this.m_adventureRadioButtonContainer.SetActive(true);
    this.m_adventureRadioButton.SetSelected(true);
    iTween.RotateBy(this.m_adventureRadioButtonContainer, iTween.Hash((object) "amount", (object) new Vector3(-1f, 0.0f, 0.0f), (object) "time", (object) this.m_backgroundFlipAnimTime, (object) "delay", (object) (1f / 1000f)));
  }

  private void OnAdventureProgressUpdated(bool isStartupAction, AdventureMission.WingProgress oldProgress, AdventureMission.WingProgress newProgress, object userData)
  {
    if (newProgress == null || oldProgress != null && oldProgress.IsOwned() || !newProgress.IsOwned())
      return;
    WingDbfRecord record = GameDbf.Wing.GetRecord(newProgress.Wing);
    if (record == null || (AdventureDbId) record.AdventureId != this.m_selectedAdventureId)
      return;
    Network.Bundle bundle = (Network.Bundle) null;
    StoreManager.Get().GetAvailableAdventureBundle(this.m_selectedAdventureId, GeneralStoreAdventureContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, out bundle);
    this.SetCurrentMoneyBundle(bundle, false);
    if (!((UnityEngine.Object) this.m_parentStore != (UnityEngine.Object) null))
      return;
    this.m_parentStore.RefreshContent();
  }

  private bool IsPreOrder()
  {
    Network.Bundle currentMoneyBundle = this.GetCurrentMoneyBundle();
    if (currentMoneyBundle != null)
      return StoreManager.Get().IsProductPrePurchase(currentMoneyBundle);
    return false;
  }

  public delegate void DelOnAdventurePreviewCardLoaded(Actor previewCard);
}
