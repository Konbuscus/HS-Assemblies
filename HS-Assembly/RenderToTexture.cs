﻿// Decompiled with JetBrains decompiler
// Type: RenderToTexture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RenderToTexture : MonoBehaviour
{
  private static Vector3 s_offset = new Vector3(-4000f, -4000f, -4000f);
  private readonly Vector3 ALPHA_OBJECT_OFFSET = new Vector3(0.0f, 1000f, 0.0f);
  private readonly Vector2[] PLANE_UVS = new Vector2[4]{ new Vector2(0.0f, 0.0f), new Vector2(1f, 0.0f), new Vector2(0.0f, 1f), new Vector2(1f, 1f) };
  private readonly Vector3[] PLANE_NORMALS = new Vector3[4]{ Vector3.up, Vector3.up, Vector3.up, Vector3.up };
  private readonly int[] PLANE_TRIANGLES = new int[6]{ 3, 1, 2, 2, 1, 0 };
  public bool m_HideRenderObject = true;
  public bool m_CreateRenderPlane = true;
  public string m_ShaderTextureName = string.Empty;
  public int m_Resolution = 128;
  public float m_Width = 1f;
  public float m_Height = 1f;
  public float m_NearClip = -0.1f;
  public float m_FarClip = 0.5f;
  public float m_BloomThreshold = 0.7f;
  public float m_BloomBlur = 0.3f;
  public float m_BloomResolutionRatio = 0.5f;
  public float m_BloomAlphaIntensity = 1f;
  public Color m_BloomColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
  public float m_AlphaClip = 15f;
  public float m_AlphaClipIntensity = 1.5f;
  public float m_AlphaClipAlphaIntensity = 1f;
  public Color m_TintColor = Color.white;
  public int m_RenderQueueOffset = 3000;
  public Color m_ClearColor = Color.clear;
  public RenderTextureFormat m_RenderTextureFormat = RenderTextureFormat.Default;
  public Vector3 m_PositionOffset = Vector3.zero;
  public LayerMask m_LayerMask = (LayerMask) -1;
  public bool m_RenderOnStart = true;
  public bool m_RenderOnEnable = true;
  private bool m_renderEnabled = true;
  private Vector3 m_ObjectToRenderOrgPosition = Vector3.zero;
  private Vector3 m_OriginalRenderPosition = Vector3.zero;
  private Vector3 m_ObjectToRenderOffset = Vector3.zero;
  private Vector3 m_AlphaObjectToRenderOffset = Vector3.zero;
  private Vector3 m_Offset = Vector3.zero;
  private const string BLUR_SHADER_NAME = "Hidden/R2TBlur";
  private const string BLUR_ALPHA_SHADER_NAME = "Hidden/R2TAlphaBlur";
  private const string ALPHA_BLEND_SHADER_NAME = "Hidden/R2TColorAlphaCombine";
  private const string ALPHA_FILL_SHADER_NAME = "Custom/AlphaFillOpaque";
  private const string BLOOM_SHADER_NAME = "Hidden/R2TBloom";
  private const string BLOOM_ALPHA_SHADER_NAME = "Hidden/R2TBloomAlpha";
  private const string ADDITIVE_SHADER_NAME = "Hidden/R2TAdditive";
  private const string TRANSPARENT_SHADER_NAME = "Hidden/R2TTransparent";
  private const string ALPHA_CLIP_SHADER_NAME = "Hidden/R2TAlphaClip";
  private const string ALPHA_CLIP_BLOOM_SHADER_NAME = "Hidden/R2TAlphaClipBloom";
  private const string ALPHA_CLIP_GRADIENT_SHADER_NAME = "Hidden/R2TAlphaClipGradient";
  private const RenderTextureFormat ALPHA_TEXTURE_FORMAT = RenderTextureFormat.Default;
  private const float OFFSET_DISTANCE = 300f;
  private const float MIN_OFFSET_DISTANCE = -4000f;
  private const float MAX_OFFSET_DISTANCE = -90000f;
  private const float RENDER_SIZE_QUALITY_LOW = 0.75f;
  private const float RENDER_SIZE_QUALITY_MEDIUM = 1f;
  private const float RENDER_SIZE_QUALITY_HIGH = 1.5f;
  public GameObject m_ObjectToRender;
  public GameObject m_AlphaObjectToRender;
  public bool m_RealtimeRender;
  public bool m_RealtimeTranslation;
  public RenderToTexture.RenderToTextureMaterial m_RenderMaterial;
  public Material m_Material;
  public GameObject m_RenderToObject;
  public bool m_RenderMeshAsAlpha;
  public float m_BloomIntensity;
  public RenderToTexture.BloomRenderType m_BloomRenderType;
  public RenderToTexture.BloomBlendType m_BloomBlend;
  public RenderToTexture.AlphaClipShader m_AlphaClipRenderStyle;
  public Texture2D m_AlphaClipGradientMap;
  public float m_BlurAmount;
  public bool m_BlurAlphaOnly;
  public int m_RenderQueue;
  public Shader m_ReplacmentShader;
  public string m_ReplacmentTag;
  public string m_AlphaReplacementTag;
  public bool m_UniformWorldScale;
  public float m_OverrideCameraSize;
  public bool m_LateUpdate;
  private bool m_init;
  private float m_WorldWidth;
  private float m_WorldHeight;
  private Vector3 m_WorldScale;
  private GameObject m_OffscreenGameObject;
  private GameObject m_CameraGO;
  private Camera m_Camera;
  private GameObject m_AlphaCameraGO;
  private Camera m_AlphaCamera;
  private GameObject m_BloomCaptureCameraGO;
  private Camera m_BloomCaptureCamera;
  private RenderTexture m_RenderTexture;
  private RenderTexture m_AlphaRenderTexture;
  private RenderTexture m_BloomRenderTexture;
  private RenderTexture m_BloomRenderBuffer1;
  private RenderTexture m_BloomRenderBuffer2;
  private GameObject m_PlaneGameObject;
  private GameObject m_BloomPlaneGameObject;
  private GameObject m_BloomCapturePlaneGameObject;
  private bool m_ObjectToRenderOrgPositionStored;
  private Transform m_ObjectToRenderOrgParent;
  private bool m_isDirty;
  private Shader m_AlphaFillShader;
  private Vector3 m_OffscreenPos;
  private RenderToTexture.RenderToTextureMaterial m_PreviousRenderMaterial;
  private Shader m_AlphaBlendShader;
  private Material m_AlphaBlendMaterial;
  private Shader m_AdditiveShader;
  private Material m_AdditiveMaterial;
  private Shader m_BloomShader;
  private Material m_BloomMaterial;
  private Shader m_BloomShaderAlpha;
  private Material m_BloomMaterialAlpha;
  private Shader m_BlurShader;
  private Material m_BlurMaterial;
  private Shader m_AlphaBlurShader;
  private Material m_AlphaBlurMaterial;
  private Shader m_TransparentShader;
  private Material m_TransparentMaterial;
  private Shader m_AlphaClipShader;
  private Material m_AlphaClipMaterial;
  private Shader m_AlphaClipBloomShader;
  private Material m_AlphaClipBloomMaterial;
  private Shader m_AlphaClipGradientShader;
  private Material m_AlphaClipGradientMaterial;

  protected Vector3 Offset
  {
    get
    {
      if (this.m_Offset == Vector3.zero)
      {
        RenderToTexture.s_offset.x -= 300f;
        if ((double) RenderToTexture.s_offset.x < -90000.0)
        {
          RenderToTexture.s_offset.x = -4000f;
          RenderToTexture.s_offset.y -= 300f;
          if ((double) RenderToTexture.s_offset.y < -90000.0)
          {
            RenderToTexture.s_offset.y = -4000f;
            RenderToTexture.s_offset.z -= 300f;
            if ((double) RenderToTexture.s_offset.z < -90000.0)
              RenderToTexture.s_offset.z = -4000f;
          }
        }
        this.m_Offset = RenderToTexture.s_offset;
      }
      return this.m_Offset;
    }
  }

  protected Material AlphaBlendMaterial
  {
    get
    {
      if ((Object) this.m_AlphaBlendMaterial == (Object) null)
      {
        if ((Object) this.m_AlphaBlendShader == (Object) null)
        {
          this.m_AlphaBlendShader = ShaderUtils.FindShader("Hidden/R2TColorAlphaCombine");
          if (!(bool) ((Object) this.m_AlphaBlendShader))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TColorAlphaCombine");
        }
        this.m_AlphaBlendMaterial = new Material(this.m_AlphaBlendShader);
        SceneUtils.SetHideFlags((Object) this.m_AlphaBlendMaterial, HideFlags.DontSave);
      }
      return this.m_AlphaBlendMaterial;
    }
  }

  protected Material AdditiveMaterial
  {
    get
    {
      if ((Object) this.m_AdditiveMaterial == (Object) null)
      {
        if ((Object) this.m_AdditiveShader == (Object) null)
        {
          this.m_AdditiveShader = ShaderUtils.FindShader("Hidden/R2TAdditive");
          if (!(bool) ((Object) this.m_AdditiveShader))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TAdditive");
        }
        this.m_AdditiveMaterial = new Material(this.m_AdditiveShader);
        SceneUtils.SetHideFlags((Object) this.m_AdditiveMaterial, HideFlags.DontSave);
      }
      return this.m_AdditiveMaterial;
    }
  }

  protected Material BloomMaterial
  {
    get
    {
      if ((Object) this.m_BloomMaterial == (Object) null)
      {
        if ((Object) this.m_BloomShader == (Object) null)
        {
          this.m_BloomShader = ShaderUtils.FindShader("Hidden/R2TBloom");
          if (!(bool) ((Object) this.m_BloomShader))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TBloom");
        }
        this.m_BloomMaterial = new Material(this.m_BloomShader);
        SceneUtils.SetHideFlags((Object) this.m_BloomMaterial, HideFlags.DontSave);
      }
      return this.m_BloomMaterial;
    }
  }

  protected Material BloomMaterialAlpha
  {
    get
    {
      if ((Object) this.m_BloomMaterialAlpha == (Object) null)
      {
        if ((Object) this.m_BloomShaderAlpha == (Object) null)
        {
          this.m_BloomShaderAlpha = ShaderUtils.FindShader("Hidden/R2TBloomAlpha");
          if (!(bool) ((Object) this.m_BloomShaderAlpha))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TBloomAlpha");
        }
        this.m_BloomMaterialAlpha = new Material(this.m_BloomShaderAlpha);
        SceneUtils.SetHideFlags((Object) this.m_BloomMaterialAlpha, HideFlags.DontSave);
      }
      return this.m_BloomMaterialAlpha;
    }
  }

  protected Material BlurMaterial
  {
    get
    {
      if ((Object) this.m_BlurMaterial == (Object) null)
      {
        if ((Object) this.m_BlurShader == (Object) null)
        {
          this.m_BlurShader = ShaderUtils.FindShader("Hidden/R2TBlur");
          if (!(bool) ((Object) this.m_BlurShader))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TBlur");
        }
        this.m_BlurMaterial = new Material(this.m_BlurShader);
        SceneUtils.SetHideFlags((Object) this.m_BlurMaterial, HideFlags.DontSave);
      }
      return this.m_BlurMaterial;
    }
  }

  protected Material AlphaBlurMaterial
  {
    get
    {
      if ((Object) this.m_AlphaBlurMaterial == (Object) null)
      {
        if ((Object) this.m_AlphaBlurShader == (Object) null)
        {
          this.m_AlphaBlurShader = ShaderUtils.FindShader("Hidden/R2TAlphaBlur");
          if (!(bool) ((Object) this.m_AlphaBlurShader))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TAlphaBlur");
        }
        this.m_AlphaBlurMaterial = new Material(this.m_AlphaBlurShader);
        SceneUtils.SetHideFlags((Object) this.m_AlphaBlurMaterial, HideFlags.DontSave);
      }
      return this.m_AlphaBlurMaterial;
    }
  }

  protected Material TransparentMaterial
  {
    get
    {
      if ((Object) this.m_TransparentMaterial == (Object) null)
      {
        if ((Object) this.m_TransparentShader == (Object) null)
        {
          this.m_TransparentShader = ShaderUtils.FindShader("Hidden/R2TTransparent");
          if (!(bool) ((Object) this.m_TransparentShader))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TTransparent");
        }
        this.m_TransparentMaterial = new Material(this.m_TransparentShader);
        SceneUtils.SetHideFlags((Object) this.m_TransparentMaterial, HideFlags.DontSave);
      }
      return this.m_TransparentMaterial;
    }
  }

  protected Material AlphaClipMaterial
  {
    get
    {
      if ((Object) this.m_AlphaClipMaterial == (Object) null)
      {
        if ((Object) this.m_AlphaClipShader == (Object) null)
        {
          this.m_AlphaClipShader = ShaderUtils.FindShader("Hidden/R2TAlphaClip");
          if (!(bool) ((Object) this.m_AlphaClipShader))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TAlphaClip");
        }
        this.m_AlphaClipMaterial = new Material(this.m_AlphaClipShader);
        SceneUtils.SetHideFlags((Object) this.m_AlphaClipMaterial, HideFlags.DontSave);
      }
      return this.m_AlphaClipMaterial;
    }
  }

  protected Material AlphaClipBloomMaterial
  {
    get
    {
      if ((Object) this.m_AlphaClipBloomMaterial == (Object) null)
      {
        if ((Object) this.m_AlphaClipBloomShader == (Object) null)
        {
          this.m_AlphaClipBloomShader = ShaderUtils.FindShader("Hidden/R2TAlphaClipBloom");
          if (!(bool) ((Object) this.m_AlphaClipBloomShader))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TAlphaClipBloom");
        }
        this.m_AlphaClipBloomMaterial = new Material(this.m_AlphaClipBloomShader);
        SceneUtils.SetHideFlags((Object) this.m_AlphaClipBloomMaterial, HideFlags.DontSave);
      }
      return this.m_AlphaClipBloomMaterial;
    }
  }

  protected Material AlphaClipGradientMaterial
  {
    get
    {
      if ((Object) this.m_AlphaClipGradientMaterial == (Object) null)
      {
        if ((Object) this.m_AlphaClipGradientShader == (Object) null)
        {
          this.m_AlphaClipGradientShader = ShaderUtils.FindShader("Hidden/R2TAlphaClipGradient");
          if (!(bool) ((Object) this.m_AlphaClipGradientShader))
            Debug.LogError((object) "Failed to load RenderToTexture Shader: Hidden/R2TAlphaClipGradient");
        }
        this.m_AlphaClipGradientMaterial = new Material(this.m_AlphaClipGradientShader);
        SceneUtils.SetHideFlags((Object) this.m_AlphaClipGradientMaterial, HideFlags.DontSave);
      }
      return this.m_AlphaClipGradientMaterial;
    }
  }

  private void Awake()
  {
    this.m_AlphaFillShader = ShaderUtils.FindShader("Custom/AlphaFillOpaque");
    if (!(bool) ((Object) this.m_AlphaFillShader))
      Debug.LogError((object) "Failed to load RenderToTexture Shader: Custom/AlphaFillOpaque");
    this.m_OffscreenPos = this.Offset;
    if (!((Object) this.m_Material != (Object) null))
      return;
    this.m_Material = Object.Instantiate<Material>(this.m_Material);
  }

  private void Start()
  {
    if (this.m_RenderOnStart)
      this.m_isDirty = true;
    this.Init();
  }

  private void Update()
  {
    if (!this.m_renderEnabled)
      return;
    if ((bool) ((Object) this.m_RenderTexture) && !this.m_RenderTexture.IsCreated())
    {
      Log.Kyle.Print("RenderToTexture Texture lost. Render Called");
      this.m_isDirty = true;
      this.RenderTex();
    }
    else
    {
      if (this.m_LateUpdate)
        return;
      if (this.m_HideRenderObject && (bool) ((Object) this.m_ObjectToRender))
        this.PositionHiddenObjectsAndCameras();
      if (!this.m_RealtimeRender && !this.m_isDirty)
        return;
      this.RenderTex();
    }
  }

  private void LateUpdate()
  {
    if (!this.m_renderEnabled)
      return;
    if (this.m_LateUpdate)
    {
      if (this.m_HideRenderObject && (bool) ((Object) this.m_ObjectToRender))
        this.PositionHiddenObjectsAndCameras();
      if (this.m_RealtimeRender || this.m_isDirty)
        this.RenderTex();
      this.m_RenderTexture.DiscardContents();
    }
    else if (this.m_RenderMaterial == RenderToTexture.RenderToTextureMaterial.AlphaClipBloom || this.m_RenderMaterial == RenderToTexture.RenderToTextureMaterial.Bloom)
    {
      this.RenderBloom();
    }
    else
    {
      if (!(bool) ((Object) this.m_BloomPlaneGameObject))
        return;
      Object.DestroyImmediate((Object) this.m_BloomPlaneGameObject);
    }
  }

  private void OnApplicationFocus(bool state)
  {
    if (!(bool) ((Object) this.m_RenderTexture) || !state)
      return;
    this.m_isDirty = true;
    this.RenderTex();
  }

  private void OnDrawGizmos()
  {
    if (!this.enabled)
      return;
    if ((double) this.m_FarClip < 0.0)
      this.m_FarClip = 0.0f;
    if ((double) this.m_NearClip > 0.0)
      this.m_NearClip = 0.0f;
    Gizmos.matrix = this.transform.localToWorldMatrix;
    Vector3 vector3 = new Vector3(0.0f, (float) (-(double) this.m_NearClip * 0.5), 0.0f);
    Gizmos.color = new Color(0.1f, 0.5f, 0.7f, 0.8f);
    Gizmos.DrawWireCube(vector3 + this.m_PositionOffset, new Vector3(this.m_Width, -this.m_NearClip, this.m_Height));
    Gizmos.color = new Color(0.2f, 0.2f, 0.9f, 0.8f);
    Gizmos.DrawWireCube(new Vector3(0.0f, (float) (-(double) this.m_FarClip * 0.5), 0.0f) + this.m_PositionOffset, new Vector3(this.m_Width, -this.m_FarClip, this.m_Height));
    Gizmos.color = new Color(0.8f, 0.8f, 1f, 1f);
    Gizmos.DrawWireCube(this.m_PositionOffset, new Vector3(this.m_Width, 0.0f, this.m_Height));
    Gizmos.matrix = Matrix4x4.identity;
  }

  private void OnDisable()
  {
    this.RestoreAfterRender();
    if ((bool) ((Object) this.m_ObjectToRender))
    {
      if ((Object) this.m_ObjectToRenderOrgParent != (Object) null)
        this.m_ObjectToRender.transform.parent = this.m_ObjectToRenderOrgParent;
      this.m_ObjectToRender.transform.localPosition = this.m_ObjectToRenderOrgPosition;
    }
    if ((bool) ((Object) this.m_PlaneGameObject))
      Object.Destroy((Object) this.m_PlaneGameObject);
    if ((bool) ((Object) this.m_BloomPlaneGameObject))
      Object.Destroy((Object) this.m_BloomPlaneGameObject);
    if ((bool) ((Object) this.m_BloomCapturePlaneGameObject))
      Object.Destroy((Object) this.m_BloomCapturePlaneGameObject);
    if ((bool) ((Object) this.m_BloomCaptureCameraGO))
      Object.Destroy((Object) this.m_BloomCaptureCameraGO);
    this.ReleaseTexture();
    if ((bool) ((Object) this.m_Camera))
      this.m_Camera.enabled = false;
    if ((bool) ((Object) this.m_AlphaCamera))
      this.m_AlphaCamera.enabled = false;
    this.m_init = false;
    this.m_isDirty = true;
  }

  private void OnDestroy()
  {
    this.CleanUp();
  }

  private void OnEnable()
  {
    if (!this.m_RenderOnEnable)
      return;
    this.RenderTex();
  }

  public RenderTexture Render()
  {
    this.m_isDirty = true;
    return this.m_RenderTexture;
  }

  public RenderTexture RenderNow()
  {
    this.RenderTex();
    return this.m_RenderTexture;
  }

  public void ForceTextureRebuild()
  {
    if (!this.enabled)
      return;
    this.ReleaseTexture();
    this.m_isDirty = true;
    this.RenderTex();
  }

  public void Show()
  {
    this.Show(false);
  }

  public void Show(bool render)
  {
    this.m_renderEnabled = true;
    if ((bool) ((Object) this.m_RenderToObject))
      this.m_RenderToObject.GetComponent<Renderer>().enabled = true;
    else if ((bool) ((Object) this.m_PlaneGameObject))
    {
      this.m_PlaneGameObject.GetComponent<Renderer>().enabled = true;
      if ((bool) ((Object) this.m_BloomPlaneGameObject))
        this.m_BloomPlaneGameObject.GetComponent<Renderer>().enabled = true;
    }
    if (!render)
      return;
    this.Render();
  }

  public void Hide()
  {
    this.m_renderEnabled = false;
    if ((bool) ((Object) this.m_RenderToObject))
    {
      this.m_RenderToObject.GetComponent<Renderer>().enabled = false;
    }
    else
    {
      if (!(bool) ((Object) this.m_PlaneGameObject))
        return;
      this.m_PlaneGameObject.GetComponent<Renderer>().enabled = false;
      if (!(bool) ((Object) this.m_BloomPlaneGameObject))
        return;
      this.m_BloomPlaneGameObject.GetComponent<Renderer>().enabled = false;
    }
  }

  public void SetDirty()
  {
    this.m_init = false;
    this.m_isDirty = true;
  }

  public Material GetRenderMaterial()
  {
    if ((bool) ((Object) this.m_RenderToObject))
      return this.m_RenderToObject.GetComponent<Renderer>().material;
    if ((bool) ((Object) this.m_PlaneGameObject))
      return this.m_PlaneGameObject.GetComponent<Renderer>().material;
    return this.m_Material;
  }

  public GameObject GetRenderToObject()
  {
    if ((bool) ((Object) this.m_RenderToObject))
      return this.m_RenderToObject;
    return this.m_PlaneGameObject;
  }

  public RenderTexture GetRenderTexture()
  {
    return this.m_RenderTexture;
  }

  public Vector3 GetOffscreenPosition()
  {
    return this.m_OffscreenPos;
  }

  public Vector3 GetOffscreenPositionOffset()
  {
    return this.m_OffscreenPos - this.transform.position;
  }

  private void Init()
  {
    if (this.m_init)
      return;
    if (this.m_RealtimeTranslation)
    {
      this.m_OffscreenGameObject = new GameObject();
      this.m_OffscreenGameObject.name = string.Format("R2TOffsetRenderRoot_{0}", (object) this.name);
      this.m_OffscreenGameObject.transform.position = this.transform.position;
    }
    if ((bool) ((Object) this.m_ObjectToRender))
    {
      if (!this.m_ObjectToRenderOrgPositionStored)
      {
        this.m_ObjectToRenderOrgParent = this.m_ObjectToRender.transform.parent;
        this.m_ObjectToRenderOrgPosition = this.m_ObjectToRender.transform.localPosition;
        this.m_ObjectToRenderOrgPositionStored = true;
      }
      if (this.m_HideRenderObject)
      {
        if (this.m_RealtimeTranslation)
        {
          this.m_ObjectToRender.transform.parent = this.m_OffscreenGameObject.transform;
          if ((bool) ((Object) this.m_AlphaObjectToRender))
            this.m_AlphaObjectToRender.transform.parent = this.m_OffscreenGameObject.transform;
        }
        this.m_OriginalRenderPosition = !(bool) ((Object) this.m_RenderToObject) ? this.transform.position : this.m_RenderToObject.transform.position;
        if ((bool) ((Object) this.m_ObjectToRender) && this.m_ObjectToRenderOffset == Vector3.zero)
          this.m_ObjectToRenderOffset = this.transform.position - this.m_ObjectToRender.transform.position;
        if ((bool) ((Object) this.m_AlphaObjectToRender) && this.m_AlphaObjectToRenderOffset == Vector3.zero)
          this.m_AlphaObjectToRenderOffset = this.transform.position - this.m_AlphaObjectToRender.transform.position;
      }
    }
    else if (!this.m_ObjectToRenderOrgPositionStored)
    {
      this.m_ObjectToRenderOrgPosition = this.transform.localPosition;
      if ((Object) this.m_OffscreenGameObject != (Object) null)
        this.m_OffscreenGameObject.transform.position = this.transform.position;
      this.m_ObjectToRenderOrgPositionStored = true;
    }
    if (this.m_HideRenderObject)
    {
      if (this.m_RealtimeTranslation)
      {
        if ((Object) this.m_OffscreenGameObject != (Object) null)
          this.m_OffscreenGameObject.transform.position = this.m_OffscreenPos;
      }
      else if ((bool) ((Object) this.m_ObjectToRender))
        this.m_ObjectToRender.transform.position = this.m_OffscreenPos;
      else
        this.transform.position = this.m_OffscreenPos;
    }
    if ((Object) this.m_ObjectToRender == (Object) null)
      this.m_ObjectToRender = this.gameObject;
    this.CalcWorldWidthHeightScale();
    this.CreateTexture();
    this.CreateCamera();
    if (this.m_RenderMeshAsAlpha || (Object) this.m_AlphaObjectToRender != (Object) null)
      this.CreateAlphaCamera();
    if (!(bool) ((Object) this.m_RenderToObject) && this.m_CreateRenderPlane)
      this.CreateRenderPlane();
    if ((bool) ((Object) this.m_RenderToObject))
      this.m_RenderToObject.GetComponent<Renderer>().material.renderQueue = this.m_RenderQueueOffset + this.m_RenderQueue;
    this.SetupMaterial();
    this.m_init = true;
  }

  private void RenderTex()
  {
    if (!this.m_renderEnabled)
      return;
    this.Init();
    if (!this.m_init || (Object) this.m_Camera == (Object) null)
      return;
    this.SetupForRender();
    if (this.m_RenderMaterial != this.m_PreviousRenderMaterial)
      this.SetupMaterial();
    if (this.m_HideRenderObject && (bool) ((Object) this.m_ObjectToRender))
      this.PositionHiddenObjectsAndCameras();
    if (this.m_RenderMeshAsAlpha || (Object) this.m_AlphaObjectToRender != (Object) null)
    {
      RenderTexture temporary1 = RenderTexture.GetTemporary(this.m_RenderTexture.width, this.m_RenderTexture.height, this.m_RenderTexture.depth, this.m_RenderTexture.format);
      this.m_Camera.targetTexture = temporary1;
      this.CameraRender();
      RenderTexture temporary2 = RenderTexture.GetTemporary(this.m_RenderTexture.width, this.m_RenderTexture.height, this.m_RenderTexture.depth, RenderTextureFormat.Default);
      this.m_AlphaCamera.targetTexture = temporary2;
      this.AlphaCameraRender();
      this.AlphaBlendMaterial.SetTexture("_AlphaTex", (Texture) temporary2);
      if ((double) this.m_BlurAmount > 0.0)
      {
        RenderTexture temporary3 = RenderTexture.GetTemporary(this.m_RenderTexture.width, this.m_RenderTexture.height, this.m_RenderTexture.depth, this.m_RenderTexture.format);
        Graphics.Blit((Texture) temporary1, temporary3, this.AlphaBlendMaterial);
        this.CameraRender();
        Material sampleMat = this.BlurMaterial;
        if (this.m_BlurAlphaOnly)
          sampleMat = this.AlphaBlurMaterial;
        this.m_RenderTexture.DiscardContents();
        this.Sample(temporary3, this.m_RenderTexture, sampleMat, this.m_BlurAmount);
        RenderTexture.ReleaseTemporary(temporary3);
      }
      else
      {
        this.m_RenderTexture.DiscardContents();
        Graphics.Blit((Texture) temporary1, this.m_RenderTexture, this.AlphaBlendMaterial);
      }
      RenderTexture.ReleaseTemporary(temporary1);
      RenderTexture.ReleaseTemporary(temporary2);
    }
    else if ((double) this.m_BlurAmount > 0.0)
    {
      RenderTexture temporary = RenderTexture.GetTemporary(this.m_RenderTexture.width, this.m_RenderTexture.height, this.m_RenderTexture.depth, this.m_RenderTexture.format);
      this.m_Camera.targetTexture = temporary;
      this.CameraRender();
      Material sampleMat = this.BlurMaterial;
      if (this.m_BlurAlphaOnly)
        sampleMat = this.m_AlphaBlurMaterial;
      this.m_RenderTexture.DiscardContents();
      this.Sample(temporary, this.m_RenderTexture, sampleMat, this.m_BlurAmount);
      RenderTexture.ReleaseTemporary(temporary);
    }
    else
    {
      this.m_Camera.targetTexture = this.m_RenderTexture;
      this.CameraRender();
    }
    if ((bool) ((Object) this.m_RenderToObject))
    {
      Renderer renderer = this.m_RenderToObject.GetComponent<Renderer>();
      if ((Object) renderer == (Object) null)
        renderer = this.m_RenderToObject.GetComponentInChildren<Renderer>();
      if (this.m_ShaderTextureName != string.Empty)
        renderer.material.SetTexture(this.m_ShaderTextureName, (Texture) this.m_RenderTexture);
      else
        renderer.material.mainTexture = (Texture) this.m_RenderTexture;
    }
    else if ((bool) ((Object) this.m_PlaneGameObject))
    {
      if (this.m_ShaderTextureName != string.Empty)
        this.m_PlaneGameObject.GetComponent<Renderer>().material.SetTexture(this.m_ShaderTextureName, (Texture) this.m_RenderTexture);
      else
        this.m_PlaneGameObject.GetComponent<Renderer>().material.mainTexture = (Texture) this.m_RenderTexture;
    }
    if (this.m_RenderMaterial == RenderToTexture.RenderToTextureMaterial.AlphaClip || this.m_RenderMaterial == RenderToTexture.RenderToTextureMaterial.AlphaClipBloom)
    {
      GameObject gameObject = this.m_PlaneGameObject;
      if ((bool) ((Object) this.m_RenderToObject))
        gameObject = this.m_RenderToObject;
      Material material = gameObject.GetComponent<Renderer>().material;
      material.SetFloat("_Cutoff", this.m_AlphaClip);
      material.SetFloat("_Intensity", this.m_AlphaClipIntensity);
      material.SetFloat("_AlphaIntensity", this.m_AlphaClipAlphaIntensity);
      if (this.m_AlphaClipRenderStyle == RenderToTexture.AlphaClipShader.ColorGradient)
        material.SetTexture("_GradientTex", (Texture) this.m_AlphaClipGradientMap);
    }
    if (!this.m_RealtimeRender)
      this.RestoreAfterRender();
    this.m_isDirty = false;
  }

  private void RenderBloom()
  {
    if ((double) this.m_BloomIntensity == 0.0)
    {
      if (!(bool) ((Object) this.m_BloomPlaneGameObject))
        return;
      Object.DestroyImmediate((Object) this.m_BloomPlaneGameObject);
    }
    else if ((double) this.m_BloomIntensity == 0.0)
    {
      if (!(bool) ((Object) this.m_BloomPlaneGameObject))
        return;
      Object.DestroyImmediate((Object) this.m_BloomPlaneGameObject);
    }
    else
    {
      int width = (int) ((double) this.m_RenderTexture.width * (double) Mathf.Clamp01(this.m_BloomResolutionRatio));
      int height = (int) ((double) this.m_RenderTexture.height * (double) Mathf.Clamp01(this.m_BloomResolutionRatio));
      RenderTexture dest = this.m_RenderTexture;
      if (this.m_RenderMaterial == RenderToTexture.RenderToTextureMaterial.AlphaClipBloom)
      {
        if (!(bool) ((Object) this.m_BloomPlaneGameObject))
          this.CreateBloomPlane();
        if (!(bool) ((Object) this.m_BloomRenderTexture))
          this.m_BloomRenderTexture = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);
      }
      if (!(bool) ((Object) this.m_BloomRenderBuffer1))
        this.m_BloomRenderBuffer1 = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);
      if (!(bool) ((Object) this.m_BloomRenderBuffer2))
        this.m_BloomRenderBuffer2 = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);
      Material mat = this.BloomMaterial;
      if (this.m_RenderMaterial == RenderToTexture.RenderToTextureMaterial.AlphaClipBloom)
      {
        mat = this.AlphaClipBloomMaterial;
        dest = this.m_BloomRenderTexture;
        if (!(bool) ((Object) this.m_BloomCaptureCameraGO))
          this.CreateBloomCaptureCamera();
        this.m_BloomCaptureCamera.targetTexture = this.m_BloomRenderTexture;
        mat.SetFloat("_Cutoff", this.m_AlphaClip);
        mat.SetFloat("_Intensity", this.m_AlphaClipIntensity);
        mat.SetFloat("_AlphaIntensity", this.m_AlphaClipAlphaIntensity);
        this.m_BloomCaptureCamera.Render();
      }
      if (this.m_BloomRenderType == RenderToTexture.BloomRenderType.Alpha)
      {
        mat = this.BloomMaterialAlpha;
        mat.SetFloat("_AlphaIntensity", this.m_BloomAlphaIntensity);
      }
      float num1 = 1f / (float) width;
      float num2 = 1f / (float) height;
      mat.SetFloat("_Threshold", this.m_BloomThreshold);
      mat.SetFloat("_Intensity", this.m_BloomIntensity / (1f - this.m_BloomThreshold));
      mat.SetVector("_OffsetA", new Vector4(1.5f * num1, 1.5f * num2, -1.5f * num1, 1.5f * num2));
      mat.SetVector("_OffsetB", new Vector4(-1.5f * num1, -1.5f * num2, 1.5f * num1, -1.5f * num2));
      this.m_BloomRenderBuffer2.DiscardContents();
      Graphics.Blit((Texture) dest, this.m_BloomRenderBuffer2, mat, 1);
      float num3 = num1 * (4f * this.m_BloomBlur);
      float num4 = num2 * (4f * this.m_BloomBlur);
      mat.SetVector("_OffsetA", new Vector4(1.5f * num3, 0.0f, -1.5f * num3, 0.0f));
      mat.SetVector("_OffsetB", new Vector4(0.5f * num3, 0.0f, -0.5f * num3, 0.0f));
      this.m_BloomRenderBuffer1.DiscardContents();
      Graphics.Blit((Texture) this.m_BloomRenderBuffer2, this.m_BloomRenderBuffer1, mat, 2);
      mat.SetVector("_OffsetA", new Vector4(0.0f, 1.5f * num4, 0.0f, -1.5f * num4));
      mat.SetVector("_OffsetB", new Vector4(0.0f, 0.5f * num4, 0.0f, -0.5f * num4));
      dest.DiscardContents();
      Graphics.Blit((Texture) this.m_BloomRenderBuffer1, dest, mat, 2);
      if (this.m_RenderMaterial == RenderToTexture.RenderToTextureMaterial.AlphaClipBloom)
      {
        this.m_BloomPlaneGameObject.GetComponent<Renderer>().material.color = this.m_BloomColor;
        this.m_BloomPlaneGameObject.GetComponent<Renderer>().material.mainTexture = (Texture) dest;
        if (!(bool) ((Object) this.m_PlaneGameObject))
          return;
        this.m_BloomPlaneGameObject.GetComponent<Renderer>().material.renderQueue = this.m_PlaneGameObject.GetComponent<Renderer>().material.renderQueue + 1;
      }
      else if ((bool) ((Object) this.m_RenderToObject))
      {
        this.m_RenderToObject.GetComponent<Renderer>().material.color = this.m_BloomColor;
        this.m_RenderToObject.GetComponent<Renderer>().material.mainTexture = (Texture) dest;
      }
      else
      {
        this.m_PlaneGameObject.GetComponent<Renderer>().material.color = this.m_BloomColor;
        this.m_PlaneGameObject.GetComponent<Renderer>().material.mainTexture = (Texture) dest;
      }
    }
  }

  private void SetupForRender()
  {
    this.CalcWorldWidthHeightScale();
    if (!(bool) ((Object) this.m_RenderTexture))
      this.CreateTexture();
    if (!this.m_HideRenderObject)
      return;
    if ((bool) ((Object) this.m_PlaneGameObject))
    {
      this.m_PlaneGameObject.transform.localPosition = this.m_PositionOffset;
      this.m_PlaneGameObject.layer = this.gameObject.layer;
    }
    if (!((Object) this.m_Camera != (Object) null))
      return;
    this.m_Camera.backgroundColor = this.m_ClearColor;
  }

  private void SetupMaterial()
  {
    GameObject gameObject = this.m_PlaneGameObject;
    if ((bool) ((Object) this.m_RenderToObject))
    {
      gameObject = this.m_RenderToObject;
      if (this.m_RenderMaterial == RenderToTexture.RenderToTextureMaterial.Custom)
        return;
    }
    if ((Object) gameObject == (Object) null)
      return;
    switch (this.m_RenderMaterial)
    {
      case RenderToTexture.RenderToTextureMaterial.Transparent:
        gameObject.GetComponent<Renderer>().material = this.TransparentMaterial;
        break;
      case RenderToTexture.RenderToTextureMaterial.Additive:
        gameObject.GetComponent<Renderer>().material = this.AdditiveMaterial;
        break;
      case RenderToTexture.RenderToTextureMaterial.Bloom:
        if (this.m_BloomBlend == RenderToTexture.BloomBlendType.Additive)
        {
          gameObject.GetComponent<Renderer>().material = this.AdditiveMaterial;
          break;
        }
        if (this.m_BloomBlend == RenderToTexture.BloomBlendType.Transparent)
        {
          gameObject.GetComponent<Renderer>().material = this.TransparentMaterial;
          break;
        }
        break;
      case RenderToTexture.RenderToTextureMaterial.AlphaClip:
        Material material1 = this.m_AlphaClipRenderStyle != RenderToTexture.AlphaClipShader.Standard ? this.AlphaClipGradientMaterial : this.AlphaClipMaterial;
        gameObject.GetComponent<Renderer>().material = material1;
        material1.SetFloat("_Cutoff", this.m_AlphaClip);
        material1.SetFloat("_Intensity", this.m_AlphaClipIntensity);
        material1.SetFloat("_AlphaIntensity", this.m_AlphaClipAlphaIntensity);
        if (this.m_AlphaClipRenderStyle == RenderToTexture.AlphaClipShader.ColorGradient)
        {
          material1.SetTexture("_GradientTex", (Texture) this.m_AlphaClipGradientMap);
          break;
        }
        break;
      case RenderToTexture.RenderToTextureMaterial.AlphaClipBloom:
        Material material2 = this.m_AlphaClipRenderStyle != RenderToTexture.AlphaClipShader.Standard ? this.AlphaClipGradientMaterial : this.AlphaClipMaterial;
        gameObject.GetComponent<Renderer>().material = material2;
        material2.SetFloat("_Cutoff", this.m_AlphaClip);
        material2.SetFloat("_Intensity", this.m_AlphaClipIntensity);
        material2.SetFloat("_AlphaIntensity", this.m_AlphaClipAlphaIntensity);
        if (this.m_AlphaClipRenderStyle == RenderToTexture.AlphaClipShader.ColorGradient)
        {
          material2.SetTexture("_GradientTex", (Texture) this.m_AlphaClipGradientMap);
          break;
        }
        break;
      default:
        if ((Object) this.m_Material != (Object) null)
        {
          gameObject.GetComponent<Renderer>().material = this.m_Material;
          break;
        }
        break;
    }
    if ((Object) gameObject.GetComponent<Renderer>().material != (Object) null)
      gameObject.GetComponent<Renderer>().material.color *= this.m_TintColor;
    if ((double) this.m_BloomIntensity > 0.0 && (bool) ((Object) this.m_BloomPlaneGameObject))
      this.m_BloomPlaneGameObject.GetComponent<Renderer>().material.color = this.m_BloomColor;
    gameObject.GetComponent<Renderer>().material.renderQueue = this.m_RenderQueueOffset + this.m_RenderQueue;
    if ((bool) ((Object) this.m_BloomPlaneGameObject))
      this.m_BloomPlaneGameObject.GetComponent<Renderer>().material.renderQueue = this.m_RenderQueueOffset + this.m_RenderQueue + 1;
    this.m_PreviousRenderMaterial = this.m_RenderMaterial;
  }

  private void PositionHiddenObjectsAndCameras()
  {
    Vector3 zero = Vector3.zero;
    Vector3 vector3 = !(bool) ((Object) this.m_RenderToObject) ? this.transform.position - this.m_OriginalRenderPosition : this.m_RenderToObject.transform.position - this.m_OriginalRenderPosition;
    if (this.m_RealtimeTranslation)
    {
      this.m_OffscreenGameObject.transform.position = this.m_OffscreenPos + vector3;
      this.m_OffscreenGameObject.transform.rotation = this.transform.rotation;
      if (!(bool) ((Object) this.m_AlphaObjectToRender))
        return;
      this.m_AlphaObjectToRender.transform.position = this.m_OffscreenPos - this.ALPHA_OBJECT_OFFSET - this.m_AlphaObjectToRenderOffset + vector3;
      this.m_AlphaObjectToRender.transform.rotation = this.transform.rotation;
    }
    else
    {
      if ((bool) ((Object) this.m_ObjectToRender))
      {
        this.m_ObjectToRender.transform.rotation = Quaternion.identity;
        this.m_ObjectToRender.transform.position = this.m_OffscreenPos - this.m_ObjectToRenderOffset + this.m_PositionOffset;
        this.m_ObjectToRender.transform.rotation = this.transform.rotation;
      }
      if ((bool) ((Object) this.m_AlphaObjectToRender))
      {
        this.m_AlphaObjectToRender.transform.position = this.m_OffscreenPos - this.ALPHA_OBJECT_OFFSET;
        this.m_AlphaObjectToRender.transform.rotation = this.transform.rotation;
      }
      if ((Object) this.m_CameraGO == (Object) null)
        return;
      this.m_CameraGO.transform.rotation = Quaternion.identity;
      this.m_CameraGO.transform.position = !(bool) ((Object) this.m_ObjectToRender) ? this.m_OffscreenPos + this.m_PositionOffset : this.m_ObjectToRender.transform.position;
      this.m_CameraGO.transform.rotation = this.m_ObjectToRender.transform.rotation;
      this.m_CameraGO.transform.Rotate(90f, 0.0f, 0.0f);
    }
  }

  private void RestoreAfterRender()
  {
    if (this.m_HideRenderObject)
      return;
    if ((bool) ((Object) this.m_ObjectToRender))
    {
      if ((Object) this.m_ObjectToRenderOrgParent != (Object) null)
        this.m_ObjectToRender.transform.parent = this.m_ObjectToRenderOrgParent;
      this.m_ObjectToRender.transform.localPosition = this.m_ObjectToRenderOrgPosition;
    }
    else
      this.transform.localPosition = this.m_ObjectToRenderOrgPosition;
  }

  private void CreateTexture()
  {
    if ((Object) this.m_RenderTexture != (Object) null)
      return;
    Vector2 vector2 = this.CalcTextureSize();
    GraphicsManager graphicsManager = GraphicsManager.Get();
    if ((bool) ((Object) graphicsManager))
    {
      if (graphicsManager.RenderQualityLevel == GraphicsQuality.Low)
        vector2 *= 0.75f;
      else if (graphicsManager.RenderQualityLevel == GraphicsQuality.Medium)
        vector2 *= 1f;
      else if (graphicsManager.RenderQualityLevel == GraphicsQuality.High)
        vector2 *= 1.5f;
    }
    this.m_RenderTexture = new RenderTexture((int) vector2.x, (int) vector2.y, 32, this.m_RenderTextureFormat);
    this.m_RenderTexture.Create();
    if (this.m_RenderMeshAsAlpha)
    {
      this.m_AlphaRenderTexture = new RenderTexture((int) vector2.x, (int) vector2.y, 32, this.m_RenderTextureFormat);
      this.m_AlphaRenderTexture.Create();
    }
    if ((bool) ((Object) this.m_Camera))
      this.m_Camera.targetTexture = this.m_RenderTexture;
    if (!(bool) ((Object) this.m_AlphaCamera))
      return;
    this.m_AlphaCamera.targetTexture = this.m_AlphaRenderTexture;
  }

  private void ReleaseTexture()
  {
    if ((Object) RenderTexture.active == (Object) this.m_RenderTexture)
      RenderTexture.active = (RenderTexture) null;
    if ((Object) RenderTexture.active == (Object) this.m_AlphaRenderTexture)
      RenderTexture.active = (RenderTexture) null;
    if ((Object) RenderTexture.active == (Object) this.m_BloomRenderTexture)
      RenderTexture.active = (RenderTexture) null;
    if ((Object) this.m_RenderTexture != (Object) null)
    {
      if ((bool) ((Object) this.m_Camera))
        this.m_Camera.targetTexture = (RenderTexture) null;
      Object.Destroy((Object) this.m_RenderTexture);
      this.m_RenderTexture = (RenderTexture) null;
    }
    if ((Object) this.m_AlphaRenderTexture != (Object) null)
    {
      if ((bool) ((Object) this.m_AlphaCamera))
        this.m_AlphaCamera.targetTexture = (RenderTexture) null;
      Object.Destroy((Object) this.m_AlphaRenderTexture);
      this.m_AlphaRenderTexture = (RenderTexture) null;
    }
    if ((Object) this.m_BloomRenderTexture != (Object) null)
    {
      Object.Destroy((Object) this.m_BloomRenderTexture);
      this.m_BloomRenderTexture = (RenderTexture) null;
    }
    if ((Object) this.m_BloomRenderBuffer1 != (Object) null)
    {
      Object.Destroy((Object) this.m_BloomRenderBuffer1);
      this.m_BloomRenderBuffer1 = (RenderTexture) null;
    }
    if ((Object) this.m_BloomRenderBuffer2 != (Object) null)
      Object.Destroy((Object) this.m_BloomRenderBuffer2);
    this.m_BloomRenderBuffer2 = (RenderTexture) null;
  }

  private void CreateCamera()
  {
    if ((Object) this.m_Camera != (Object) null)
      return;
    this.m_CameraGO = new GameObject();
    this.m_Camera = this.m_CameraGO.AddComponent<Camera>();
    this.m_CameraGO.name = this.name + "_R2TRenderCamera";
    this.m_Camera.orthographic = true;
    if (this.m_HideRenderObject)
    {
      if (this.m_RealtimeTranslation)
      {
        this.m_OffscreenGameObject.transform.position = this.transform.position;
        this.m_CameraGO.transform.parent = this.m_OffscreenGameObject.transform;
        this.m_CameraGO.transform.localPosition = Vector3.zero + this.m_PositionOffset;
        this.m_CameraGO.transform.rotation = this.transform.rotation;
        this.m_OffscreenGameObject.transform.position = this.m_OffscreenPos;
      }
      else
      {
        this.m_CameraGO.transform.parent = (Transform) null;
        this.m_CameraGO.transform.position = this.m_OffscreenPos + this.m_PositionOffset;
        this.m_CameraGO.transform.rotation = this.transform.rotation;
      }
    }
    else
    {
      this.m_CameraGO.transform.parent = this.transform;
      this.m_CameraGO.transform.position = this.transform.position + this.m_PositionOffset;
      this.m_CameraGO.transform.rotation = this.transform.rotation;
    }
    this.m_CameraGO.transform.Rotate(90f, 0.0f, 0.0f);
    if ((double) this.m_FarClip < 0.0)
      this.m_FarClip = 0.0f;
    if ((double) this.m_NearClip > 0.0)
      this.m_NearClip = 0.0f;
    this.m_Camera.nearClipPlane = this.m_NearClip * this.m_WorldScale.y;
    this.m_Camera.farClipPlane = this.m_FarClip * this.m_WorldScale.y;
    Camera main = Camera.main;
    if ((Object) main != (Object) null)
      this.m_Camera.depth = main.depth - 2f;
    this.m_Camera.clearFlags = CameraClearFlags.Color;
    this.m_Camera.backgroundColor = this.m_ClearColor;
    this.m_Camera.depthTextureMode = DepthTextureMode.None;
    this.m_Camera.renderingPath = RenderingPath.Forward;
    this.m_Camera.cullingMask = (int) this.m_LayerMask;
    this.m_Camera.targetTexture = this.m_RenderTexture;
    this.m_Camera.enabled = false;
  }

  private float OrthoSize()
  {
    if ((double) this.m_OverrideCameraSize > 0.0)
      return this.m_OverrideCameraSize;
    return (double) this.m_WorldWidth <= (double) this.m_WorldHeight ? this.m_WorldHeight * 0.5f : Mathf.Min(this.m_WorldWidth * 0.5f, this.m_WorldHeight * 0.5f);
  }

  private void CameraRender()
  {
    this.m_Camera.orthographicSize = this.OrthoSize();
    this.m_Camera.farClipPlane = this.m_FarClip * this.m_WorldScale.z;
    this.m_Camera.nearClipPlane = this.m_NearClip * this.m_WorldScale.z;
    if ((bool) ((Object) this.m_PlaneGameObject) && !this.m_HideRenderObject)
    {
      this.m_PlaneGameObject.GetComponent<Renderer>().enabled = false;
      if ((bool) ((Object) this.m_BloomPlaneGameObject))
        this.m_BloomPlaneGameObject.GetComponent<Renderer>().enabled = false;
    }
    if ((bool) ((Object) this.m_ReplacmentShader))
      this.m_Camera.RenderWithShader(this.m_ReplacmentShader, this.m_ReplacmentTag);
    else
      this.m_Camera.Render();
    if (!(bool) ((Object) this.m_PlaneGameObject) || this.m_HideRenderObject)
      return;
    this.m_PlaneGameObject.GetComponent<Renderer>().enabled = true;
    if (!(bool) ((Object) this.m_BloomPlaneGameObject))
      return;
    this.m_BloomPlaneGameObject.GetComponent<Renderer>().enabled = true;
  }

  private void CreateAlphaCamera()
  {
    if ((Object) this.m_AlphaCamera != (Object) null)
      return;
    this.m_AlphaCameraGO = new GameObject();
    this.m_AlphaCamera = this.m_AlphaCameraGO.AddComponent<Camera>();
    this.m_AlphaCameraGO.name = this.name + "_R2TAlphaRenderCamera";
    this.m_AlphaCamera.CopyFrom(this.m_Camera);
    this.m_AlphaCamera.enabled = false;
    this.m_AlphaCamera.backgroundColor = Color.clear;
    this.m_AlphaCameraGO.transform.parent = this.m_CameraGO.transform;
    this.m_AlphaCameraGO.transform.position = !(bool) ((Object) this.m_AlphaObjectToRender) ? this.m_CameraGO.transform.position : this.m_CameraGO.transform.position - this.ALPHA_OBJECT_OFFSET;
    this.m_AlphaCameraGO.transform.localRotation = Quaternion.identity;
  }

  private void AlphaCameraRender()
  {
    this.m_AlphaCamera.orthographicSize = this.OrthoSize();
    this.m_AlphaCamera.farClipPlane = this.m_FarClip * this.m_WorldScale.z;
    this.m_AlphaCamera.nearClipPlane = this.m_NearClip * this.m_WorldScale.z;
    if ((bool) ((Object) this.m_PlaneGameObject) && !this.m_HideRenderObject)
    {
      this.m_PlaneGameObject.GetComponent<Renderer>().enabled = false;
      if ((bool) ((Object) this.m_BloomPlaneGameObject))
        this.m_BloomPlaneGameObject.GetComponent<Renderer>().enabled = false;
    }
    if ((Object) this.m_AlphaObjectToRender == (Object) null)
    {
      string replacementTag = this.m_AlphaReplacementTag;
      if (replacementTag == string.Empty)
        replacementTag = this.m_ReplacmentTag;
      this.m_AlphaCamera.RenderWithShader(this.m_AlphaFillShader, replacementTag);
    }
    else
      this.m_AlphaCamera.Render();
    if (!(bool) ((Object) this.m_PlaneGameObject) || this.m_HideRenderObject)
      return;
    this.m_PlaneGameObject.GetComponent<Renderer>().enabled = true;
    if (!(bool) ((Object) this.m_BloomPlaneGameObject))
      return;
    this.m_BloomPlaneGameObject.GetComponent<Renderer>().enabled = true;
  }

  private void CreateBloomCaptureCamera()
  {
    if ((Object) this.m_BloomCaptureCamera != (Object) null)
      return;
    this.m_BloomCaptureCameraGO = new GameObject();
    this.m_BloomCaptureCamera = this.m_BloomCaptureCameraGO.AddComponent<Camera>();
    this.m_BloomCaptureCameraGO.name = this.name + "_R2TBloomRenderCamera";
    this.m_BloomCaptureCamera.CopyFrom(this.m_Camera);
    this.m_BloomCaptureCamera.enabled = false;
    this.m_BloomCaptureCamera.depth = this.m_Camera.depth + 1f;
    this.m_BloomCaptureCameraGO.transform.parent = this.m_Camera.transform;
    this.m_BloomCaptureCameraGO.transform.localPosition = Vector3.zero;
    this.m_BloomCaptureCameraGO.transform.localRotation = Quaternion.identity;
  }

  private Vector2 CalcTextureSize()
  {
    Vector2 vector2 = new Vector2((float) this.m_Resolution, (float) this.m_Resolution);
    if ((double) this.m_WorldWidth > (double) this.m_WorldHeight)
    {
      vector2.x = (float) this.m_Resolution;
      vector2.y = (float) this.m_Resolution * (this.m_WorldHeight / this.m_WorldWidth);
    }
    else
    {
      vector2.x = (float) this.m_Resolution * (this.m_WorldWidth / this.m_WorldHeight);
      vector2.y = (float) this.m_Resolution;
    }
    return vector2;
  }

  private void CreateRenderPlane()
  {
    if ((Object) this.m_PlaneGameObject != (Object) null)
      Object.DestroyImmediate((Object) this.m_PlaneGameObject);
    this.m_PlaneGameObject = this.CreateMeshPlane(string.Format("{0}_RenderPlane", (object) this.name), this.m_Material);
    SceneUtils.SetHideFlags((Object) this.m_PlaneGameObject, HideFlags.DontSave);
  }

  private void CreateBloomPlane()
  {
    if ((Object) this.m_BloomPlaneGameObject != (Object) null)
      Object.DestroyImmediate((Object) this.m_BloomPlaneGameObject);
    Material material = this.AdditiveMaterial;
    if (this.m_BloomBlend == RenderToTexture.BloomBlendType.Transparent)
      material = this.TransparentMaterial;
    this.m_BloomPlaneGameObject = this.CreateMeshPlane(string.Format("{0}_BloomRenderPlane", (object) this.name), material);
    this.m_BloomPlaneGameObject.transform.parent = this.m_PlaneGameObject.transform;
    this.m_BloomPlaneGameObject.transform.localPosition = new Vector3(0.0f, 0.15f, 0.0f);
    this.m_BloomPlaneGameObject.transform.localRotation = Quaternion.identity;
    this.m_BloomPlaneGameObject.transform.localScale = Vector3.one;
    this.m_BloomPlaneGameObject.GetComponent<Renderer>().material.color = this.m_BloomColor;
  }

  private void CreateBloomCapturePlane()
  {
    if ((Object) this.m_BloomCapturePlaneGameObject != (Object) null)
      Object.DestroyImmediate((Object) this.m_BloomCapturePlaneGameObject);
    Material material = this.AdditiveMaterial;
    if (this.m_BloomBlend == RenderToTexture.BloomBlendType.Transparent)
      material = this.TransparentMaterial;
    this.m_BloomCapturePlaneGameObject = this.CreateMeshPlane(string.Format("{0}_BloomCaptureRenderPlane", (object) this.name), material);
    this.m_BloomCapturePlaneGameObject.transform.parent = this.m_BloomCaptureCameraGO.transform;
    this.m_BloomCapturePlaneGameObject.transform.localPosition = Vector3.zero;
    this.m_BloomCapturePlaneGameObject.transform.localRotation = Quaternion.identity;
    this.m_BloomCapturePlaneGameObject.transform.Rotate(-90f, 0.0f, 0.0f);
    this.m_BloomCapturePlaneGameObject.transform.localScale = this.m_WorldScale;
    if ((bool) ((Object) this.m_Material))
      this.m_BloomCapturePlaneGameObject.GetComponent<Renderer>().material = this.m_PlaneGameObject.GetComponent<Renderer>().material;
    if (!(bool) ((Object) this.m_RenderTexture))
      return;
    this.m_BloomCapturePlaneGameObject.GetComponent<Renderer>().material.mainTexture = (Texture) this.m_RenderTexture;
  }

  private GameObject CreateMeshPlane(string name, Material material)
  {
    GameObject gameObject = new GameObject();
    gameObject.name = name;
    gameObject.transform.parent = this.transform;
    gameObject.transform.localPosition = this.m_PositionOffset;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    gameObject.AddComponent<MeshFilter>();
    gameObject.AddComponent<MeshRenderer>();
    Mesh mesh1 = new Mesh();
    float x = this.m_Width * 0.5f;
    float z = this.m_Height * 0.5f;
    mesh1.vertices = new Vector3[4]
    {
      new Vector3(-x, 0.0f, -z),
      new Vector3(x, 0.0f, -z),
      new Vector3(-x, 0.0f, z),
      new Vector3(x, 0.0f, z)
    };
    mesh1.uv = this.PLANE_UVS;
    mesh1.normals = this.PLANE_NORMALS;
    mesh1.triangles = this.PLANE_TRIANGLES;
    Mesh mesh2 = mesh1;
    gameObject.GetComponent<MeshFilter>().mesh = mesh2;
    mesh2.RecalculateBounds();
    if ((bool) ((Object) material))
      gameObject.GetComponent<Renderer>().material = material;
    gameObject.GetComponent<Renderer>().material.renderQueue = this.m_RenderQueueOffset + this.m_RenderQueue;
    return gameObject;
  }

  private void Sample(RenderTexture source, RenderTexture dest, Material sampleMat, float offset)
  {
    Graphics.BlitMultiTap((Texture) source, dest, sampleMat, new Vector2(-offset, -offset), new Vector2(-offset, offset), new Vector2(offset, offset), new Vector2(offset, -offset));
  }

  private void CalcWorldWidthHeightScale()
  {
    Quaternion rotation = this.transform.rotation;
    Vector3 localScale = this.transform.localScale;
    Transform parent = this.transform.parent;
    this.transform.rotation = Quaternion.identity;
    bool flag = false;
    if ((double) this.transform.lossyScale.magnitude == 0.0)
    {
      this.transform.parent = (Transform) null;
      this.transform.localScale = Vector3.one;
      flag = true;
    }
    if (this.m_UniformWorldScale)
    {
      float num = Mathf.Max(this.transform.lossyScale.x, this.transform.lossyScale.y, this.transform.lossyScale.z);
      this.m_WorldScale = new Vector3(num, num, num);
    }
    else
      this.m_WorldScale = this.transform.lossyScale;
    this.m_WorldWidth = this.m_Width * this.m_WorldScale.x;
    this.m_WorldHeight = this.m_Height * this.m_WorldScale.y;
    if (flag)
    {
      this.transform.parent = parent;
      this.transform.localScale = localScale;
    }
    this.transform.rotation = rotation;
    if ((double) this.m_WorldWidth != 0.0 && (double) this.m_WorldHeight != 0.0)
      return;
    Debug.LogError((object) string.Format(" \"{0}\": RenderToTexture has a world scale of zero. \nm_WorldWidth: {1},   m_WorldHeight: {2}", (object) this.m_WorldWidth, (object) this.m_WorldHeight));
  }

  private void CleanUp()
  {
    this.ReleaseTexture();
    if ((bool) ((Object) this.m_CameraGO))
      Object.Destroy((Object) this.m_CameraGO);
    if ((bool) ((Object) this.m_AlphaCameraGO))
      Object.Destroy((Object) this.m_AlphaCameraGO);
    if ((bool) ((Object) this.m_PlaneGameObject))
      Object.Destroy((Object) this.m_PlaneGameObject);
    if ((bool) ((Object) this.m_BloomPlaneGameObject))
      Object.Destroy((Object) this.m_BloomPlaneGameObject);
    if ((bool) ((Object) this.m_BloomCaptureCameraGO))
      Object.Destroy((Object) this.m_BloomCaptureCameraGO);
    if ((bool) ((Object) this.m_BloomCapturePlaneGameObject))
      Object.Destroy((Object) this.m_BloomCapturePlaneGameObject);
    if ((bool) ((Object) this.m_OffscreenGameObject))
      Object.Destroy((Object) this.m_OffscreenGameObject);
    if ((bool) ((Object) this.m_BlurMaterial))
      Object.Destroy((Object) this.m_BlurMaterial);
    if ((bool) ((Object) this.m_AlphaBlurMaterial))
      Object.Destroy((Object) this.m_AlphaBlurMaterial);
    if ((bool) ((Object) this.m_AlphaBlendMaterial))
      Object.Destroy((Object) this.m_AlphaBlendMaterial);
    if ((Object) this.m_ObjectToRender != (Object) null)
    {
      if ((Object) this.m_ObjectToRenderOrgParent != (Object) null)
        this.m_ObjectToRender.transform.parent = this.m_ObjectToRenderOrgParent;
      this.m_ObjectToRender.transform.localPosition = this.m_ObjectToRenderOrgPosition;
    }
    this.m_init = false;
    this.m_isDirty = true;
  }

  public enum RenderToTextureMaterial
  {
    Custom,
    Transparent,
    Additive,
    Bloom,
    AlphaClip,
    AlphaClipBloom,
  }

  public enum AlphaClipShader
  {
    Standard,
    ColorGradient,
  }

  public enum BloomRenderType
  {
    Color,
    Alpha,
  }

  public enum BloomBlendType
  {
    Additive,
    Transparent,
  }
}
