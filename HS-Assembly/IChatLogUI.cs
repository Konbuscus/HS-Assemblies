﻿// Decompiled with JetBrains decompiler
// Type: IChatLogUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public interface IChatLogUI
{
  bool IsShowing { get; }

  GameObject GameObject { get; }

  BnetPlayer Receiver { get; }

  void ShowForPlayer(BnetPlayer player);

  void Hide();

  void GoBack();
}
