﻿// Decompiled with JetBrains decompiler
// Type: FBAppRequestsFilterGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public sealed class FBAppRequestsFilterGroup : Dictionary<string, object>
{
  public FBAppRequestsFilterGroup(string name, List<string> user_ids)
  {
    this["name"] = (object) name;
    this["user_ids"] = (object) user_ids;
  }
}
