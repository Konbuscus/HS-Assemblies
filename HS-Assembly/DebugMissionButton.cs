﻿// Decompiled with JetBrains decompiler
// Type: DebugMissionButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using UnityEngine;

public class DebugMissionButton : PegUIElement
{
  public int m_missionId;
  public GameObject m_heroImage;
  public UberText m_name;
  public string m_introline;
  public string m_characterPrefabName;
  private GameObject m_heroPowerObject;
  private bool m_mousedOver;
  private FullDef m_heroPowerDef;
  private Actor m_heroPowerActor;

  private void Start()
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(this.m_missionId);
    if (record == null)
    {
      Error.AddDevWarning("Error", "scenario {0} does not exist in the DBF", (object) this.m_missionId);
    }
    else
    {
      if ((Object) this.m_name != (Object) null)
        this.m_name.Text = (string) record.ShortName;
      int player2HeroCardId = record.ClientPlayer2HeroCardId;
      if (player2HeroCardId == 0)
        player2HeroCardId = record.Player2HeroCardId;
      string cardId = GameUtils.TranslateDbIdToCardId(player2HeroCardId);
      if (cardId == null)
        return;
      DefLoader.Get().LoadCardDef(cardId, new DefLoader.LoadDefCallback<CardDef>(this.OnCardDefLoaded), (object) null, (CardPortraitQuality) null);
    }
  }

  private void OnCardDefLoaded(string cardID, CardDef cardDef, object userData)
  {
    this.m_heroImage.GetComponent<Renderer>().material.mainTexture = cardDef.GetPortraitTexture();
  }

  protected override void OnRelease()
  {
    if (!string.IsNullOrEmpty(this.m_introline))
    {
      if (string.IsNullOrEmpty(this.m_characterPrefabName))
        NotificationManager.Get().CreateKTQuote(this.m_introline, this.m_introline, true);
      else
        NotificationManager.Get().CreateCharacterQuote(this.m_characterPrefabName, GameStrings.Get(this.m_introline), this.m_introline, true, 0.0f, CanvasAnchor.BOTTOM_LEFT);
    }
    base.OnRelease();
    GameMgr.Get().FindGame(GameType.GT_VS_AI, FormatType.FT_WILD, this.m_missionId, DeckPickerTrayDisplay.Get().GetSelectedDeckID(), 0L);
    Object.Destroy((Object) this.gameObject);
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    this.m_mousedOver = true;
    base.OnOver(oldState);
    if (string.IsNullOrEmpty(GameUtils.GetMissionHeroPowerCardId(this.m_missionId)))
      return;
    DefLoader.Get().LoadFullDef(GameUtils.GetMissionHeroPowerCardId(this.m_missionId), new DefLoader.LoadDefCallback<FullDef>(this.OnHeroPowerDefLoaded));
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_mousedOver = false;
    base.OnOut(oldState);
    if (!(bool) ((Object) this.m_heroPowerActor))
      return;
    Object.Destroy((Object) this.m_heroPowerActor.gameObject);
  }

  private void OnHeroPowerDefLoaded(string cardID, FullDef def, object userData)
  {
    this.m_heroPowerDef = def;
    if (!this.m_mousedOver)
      return;
    AssetLoader.Get().LoadActor("History_HeroPower_Opponent", new AssetLoader.GameObjectCallback(this.OnHeroPowerActorLoaded), (object) null, false);
  }

  private void OnHeroPowerActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if (!this.m_mousedOver)
      Object.Destroy((Object) actorObject);
    if ((bool) ((Object) this.m_heroPowerActor))
      Object.Destroy((Object) this.m_heroPowerActor.gameObject);
    if ((Object) this == (Object) null || (Object) this.gameObject == (Object) null)
      Object.Destroy((Object) actorObject);
    if ((Object) actorObject == (Object) null)
      return;
    this.m_heroPowerActor = actorObject.GetComponent<Actor>();
    actorObject.transform.parent = this.gameObject.transform;
    this.m_heroPowerActor.SetCardDef(this.m_heroPowerDef.GetCardDef());
    this.m_heroPowerActor.SetEntityDef(this.m_heroPowerDef.GetEntityDef());
    this.m_heroPowerActor.UpdateAllComponents();
    actorObject.transform.position = this.transform.position + new Vector3(15f, 0.0f, 0.0f);
    actorObject.transform.localScale = Vector3.one;
    iTween.ScaleTo(actorObject, new Vector3(7f, 7f, 7f), 0.5f);
    SceneUtils.SetLayer(actorObject, GameLayer.Tooltip);
  }
}
