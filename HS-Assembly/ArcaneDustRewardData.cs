﻿// Decompiled with JetBrains decompiler
// Type: ArcaneDustRewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class ArcaneDustRewardData : RewardData
{
  public int Amount { get; set; }

  public ArcaneDustRewardData()
    : this(0)
  {
  }

  public ArcaneDustRewardData(int amount)
    : base(Reward.Type.ARCANE_DUST)
  {
    this.Amount = amount;
  }

  public override string ToString()
  {
    return string.Format("[ArcaneDustRewardData: Amount={0} Origin={1} OriginData={2}]", (object) this.Amount, (object) this.Origin, (object) this.OriginData);
  }

  protected override string GetGameObjectName()
  {
    return "ArcaneDustReward";
  }
}
