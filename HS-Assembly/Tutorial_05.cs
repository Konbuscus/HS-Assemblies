﻿// Decompiled with JetBrains decompiler
// Type: Tutorial_05
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Tutorial_05 : TutorialEntity
{
  private bool heroPowerHasNotBeenUsed = true;
  private int weaponsPlayed;
  private int numTimesRemindedAboutGoal;
  private bool victory;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_12_12");
    this.PreloadSound("VO_TUTORIAL_04_JAINA_03_39");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_11_11");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_02_03");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_04_05");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_08_08");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_03_04");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_05_06");
    this.PreloadSound("VO_TUTORIAL_05_JAINA_02_46");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_06_07");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_09_09");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_10_10");
    this.PreloadSound("VO_TUTORIAL_05_JAINA_05_47");
    this.PreloadSound("VO_TUTORIAL_05_JAINA_06_48");
    this.PreloadSound("VO_TUTORIAL_05_ILLIDAN_01_02");
    this.PreloadSound("VO_TUTORIAL_05_JAINA_01_45");
    this.PreloadSound("VO_INNKEEPER_TUT_COMPLETE_05");
  }

  public override void NotifyOfGameOver(TAG_PLAYSTATE gameResult)
  {
    if (gameResult == TAG_PLAYSTATE.WON)
      this.victory = true;
    base.NotifyOfGameOver(gameResult);
    if (gameResult == TAG_PLAYSTATE.WON)
    {
      this.SetTutorialProgress(TutorialProgress.ILLIDAN_COMPLETE);
      FixedRewardsMgr.Get().CheckForTutorialComplete();
      if (Network.ShouldBeConnectedToAurora())
        BnetPresenceMgr.Get().SetGameField(15U, 1);
      this.ResetTutorialLostProgress();
      this.PlaySound("VO_TUTORIAL_05_ILLIDAN_12_12", 1f, true, false);
    }
    else if (gameResult == TAG_PLAYSTATE.TIED)
    {
      this.PlaySound("VO_TUTORIAL_05_ILLIDAN_12_12", 1f, true, false);
    }
    else
    {
      if (gameResult != TAG_PLAYSTATE.LOST)
        return;
      this.SetTutorialLostProgress(TutorialProgress.ILLIDAN_COMPLETE);
    }
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_05.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator21E() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_05.\u003CHandleMissionEventWithTiming\u003Ec__Iterator21F() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator Wait(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_05.\u003CWait\u003Ec__Iterator220() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds };
  }

  public override bool IsKeywordHelpDelayOverridden()
  {
    return true;
  }

  private void ShowEndTurnBouncingArrow()
  {
    if (EndTurnButton.Get().IsInWaitingState())
      return;
    Vector3 position = EndTurnButton.Get().transform.position;
    NotificationManager.Get().CreateBouncingArrow(UserAttentionBlocker.NONE, new Vector3(position.x - 2f, position.y, position.z), new Vector3(0.0f, -90f, 0.0f));
  }

  public override bool NotifyOfEndTurnButtonPushed()
  {
    NotificationManager.Get().DestroyAllArrows();
    return true;
  }

  public override bool NotifyOfTooltipDisplay(TooltipZone specificZone)
  {
    return false;
  }

  public override string[] NotifyOfKeywordHelpPanelDisplay(Entity entity)
  {
    if (!(entity.GetCardId() == "TU4e_004") && !(entity.GetCardId() == "TU4e_007"))
      return (string[]) null;
    return new string[2]{ GameStrings.Get("TUTORIAL05_WEAPON_HEADLINE"), GameStrings.Get("TUTORIAL05_WEAPON_DESC") };
  }

  public override List<RewardData> GetCustomRewards()
  {
    if (!this.victory)
      return (List<RewardData>) null;
    List<RewardData> rewardDataList = new List<RewardData>();
    CardRewardData cardRewardData = new CardRewardData("EX1_277", TAG_PREMIUM.NORMAL, 2);
    cardRewardData.MarkAsDummyReward();
    rewardDataList.Add((RewardData) cardRewardData);
    return rewardDataList;
  }
}
