﻿// Decompiled with JetBrains decompiler
// Type: GetRenderToTextureRenderObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using HutongGames.PlayMaker;
using UnityEngine;

[ActionCategory("Pegasus")]
[Tooltip("Get the object being rendered to from RenderToTexture")]
public class GetRenderToTextureRenderObject : FsmStateAction
{
  [CheckForComponent(typeof (RenderToTexture))]
  [RequiredField]
  public FsmOwnerDefault gameObject;
  [RequiredField]
  [UIHint(UIHint.Variable)]
  public FsmGameObject renderObject;

  [Tooltip("Get the object being rendered to from RenderToTexture. This is used to get the procedurally generated render plane object.")]
  public override void Reset()
  {
    this.gameObject = (FsmOwnerDefault) null;
    this.renderObject = (FsmGameObject) null;
  }

  public override void OnEnter()
  {
    this.DoGetObject();
    this.Finish();
  }

  private void DoGetObject()
  {
    GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
    if ((Object) ownerDefaultTarget == (Object) null)
      return;
    RenderToTexture component = ownerDefaultTarget.GetComponent<RenderToTexture>();
    if ((Object) component == (Object) null)
      this.LogError("Missing RenderToTexture component!");
    else
      this.renderObject.Value = component.GetRenderToObject();
  }
}
