﻿// Decompiled with JetBrains decompiler
// Type: ZoneHand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneHand : Zone
{
  private static int MAX_CARDS = 10;
  private static float[] CARD_PIXEL_WIDTHS_TABLET = new float[11]{ 0.0f, 0.08f, 0.08f, 0.08f, 0.08f, 0.074f, 0.069f, 0.064f, 0.06f, 0.056f, 0.054f };
  private static float[] CARD_PIXEL_WIDTHS_PHONE = new float[11]{ 0.0f, 0.148f, 0.148f, 0.148f, 0.148f, 0.148f, 0.148f, 0.143f, 0.125f, 0.111f, 0.1f };
  private static PlatformDependentValue<float[]> CARD_PIXEL_WIDTHS = new PlatformDependentValue<float[]>(PlatformCategory.Screen) { PC = ZoneHand.CARD_PIXEL_WIDTHS_TABLET, Tablet = ZoneHand.CARD_PIXEL_WIDTHS_TABLET, Phone = ZoneHand.CARD_PIXEL_WIDTHS_PHONE };
  private readonly PlatformDependentValue<float> BASELINE_ASPECT_RATIO = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 1.333333f, Tablet = 1.333333f, Phone = 1.775f };
  private int lastMousedOver = -1;
  private bool m_doNotUpdateLayout = true;
  public const float MOUSE_OVER_SCALE = 1.5f;
  public const float HAND_SCALE = 0.62f;
  public const float HAND_SCALE_Y = 0.225f;
  public const float HAND_SCALE_OPPONENT = 0.682f;
  public const float HAND_SCALE_OPPONENT_Y = 0.225f;
  private const float CARD_WIDTH = 2.049684f;
  private const float ANGLE_OF_CARDS = 40f;
  private const float DEFAULT_ANIMATE_TIME = 0.35f;
  private const float DRIFT_AMOUNT = 0.08f;
  private const float Z_ROTATION_ON_LEFT = 354.5f;
  private const float Z_ROTATION_ON_RIGHT = 3f;
  private const float RESISTANCE_BASE = 10f;
  public GameObject m_iPhoneCardPosition;
  public GameObject m_leftArrow;
  public GameObject m_rightArrow;
  public GameObject m_manaGemPosition;
  public ManaCrystalMgr m_manaGemMgr;
  public GameObject m_playCardButton;
  public GameObject m_iPhonePreviewBone;
  public Float_MobileOverride m_SelectCardOffsetZ;
  public Float_MobileOverride m_SelectCardScale;
  public Float_MobileOverride m_TouchDragResistanceFactorY;
  public Vector3 m_enlargedHandPosition;
  public Vector3 m_enlargedHandScale;
  public Vector3 m_enlargedHandCardScale;
  public float m_enlargedHandDefaultCardSpacing;
  public float m_enlargedHandCardMinX;
  public float m_enlargedHandCardMaxX;
  public float m_heroWidthInHand;
  public float m_handHidingDistance;
  public GameObject m_heroHitbox;
  private float m_maxWidth;
  private Vector3 centerOfHand;
  private bool enemyHand;
  private bool m_handEnlarged;
  private Vector3 m_startingPosition;
  private Vector3 m_startingScale;
  private bool m_handMoving;
  private bool m_targetingMode;
  private int m_touchedSlot;
  private CardStandIn m_hiddenStandIn;
  private List<CardStandIn> standIns;
  private bool m_flipHandCards;

  public CardStandIn CurrentStandIn
  {
    get
    {
      if (this.lastMousedOver < 0 || this.lastMousedOver >= this.m_cards.Count)
        return (CardStandIn) null;
      return this.GetStandIn(this.m_cards[this.lastMousedOver]);
    }
  }

  private void Awake()
  {
    this.enemyHand = this.m_Side == Player.Side.OPPOSING;
    this.m_startingPosition = this.gameObject.transform.localPosition;
    this.m_startingScale = this.gameObject.transform.localScale;
    this.UpdateCenterAndWidth();
  }

  public int GetLastMousedOverCard()
  {
    return this.lastMousedOver;
  }

  public bool IsHandScrunched()
  {
    int count = this.m_cards.Count;
    if (this.m_handEnlarged && count > 3)
      return true;
    float defaultCardSpacing = this.GetDefaultCardSpacing();
    if (!this.enemyHand)
      count -= TurnStartManager.Get().GetNumCardsToDraw();
    return (double) (defaultCardSpacing * (float) count) > (double) this.MaxHandWidth();
  }

  public void SetDoNotUpdateLayout(bool enable)
  {
    this.m_doNotUpdateLayout = enable;
  }

  public bool IsDoNotUpdateLayout()
  {
    return this.m_doNotUpdateLayout;
  }

  public void OnSpellPowerEntityEnteredPlay()
  {
    using (List<Card>.Enumerator enumerator = this.m_cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (this.CanPlaySpellPowerHint(current))
        {
          Spell actorSpell = current.GetActorSpell(SpellType.SPELL_POWER_HINT_BURST, true);
          if ((UnityEngine.Object) actorSpell != (UnityEngine.Object) null)
            actorSpell.Reactivate();
        }
      }
    }
  }

  public void OnSpellPowerEntityMousedOver()
  {
    if (TargetReticleManager.Get().IsActive())
      return;
    using (List<Card>.Enumerator enumerator = this.m_cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (this.CanPlaySpellPowerHint(current))
        {
          Spell actorSpell1 = current.GetActorSpell(SpellType.SPELL_POWER_HINT_BURST, true);
          if ((UnityEngine.Object) actorSpell1 != (UnityEngine.Object) null)
            actorSpell1.Reactivate();
          Spell actorSpell2 = current.GetActorSpell(SpellType.SPELL_POWER_HINT_IDLE, true);
          if ((UnityEngine.Object) actorSpell2 != (UnityEngine.Object) null)
            actorSpell2.ActivateState(SpellStateType.BIRTH);
        }
      }
    }
  }

  public void OnSpellPowerEntityMousedOut()
  {
    using (List<Card>.Enumerator enumerator = this.m_cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Spell actorSpell = enumerator.Current.GetActorSpell(SpellType.SPELL_POWER_HINT_IDLE, true);
        if (!((UnityEngine.Object) actorSpell == (UnityEngine.Object) null) && actorSpell.IsActive())
          actorSpell.ActivateState(SpellStateType.DEATH);
      }
    }
  }

  public float GetDefaultCardSpacing()
  {
    if ((bool) UniversalInputManager.UsePhoneUI && this.m_handEnlarged)
      return this.m_enlargedHandDefaultCardSpacing;
    return 1.270804f;
  }

  public override void UpdateLayout()
  {
    if (!GameState.Get().IsMulliganManagerActive() && !this.enemyHand)
    {
      this.BlowUpOldStandins();
      for (int index = 0; index < this.m_cards.Count; ++index)
        this.CreateCardStandIn(this.m_cards[index]);
    }
    this.UpdateLayout(-1, true, -1);
  }

  public void ForceStandInUpdate()
  {
    this.BlowUpOldStandins();
    for (int index = 0; index < this.m_cards.Count; ++index)
      this.CreateCardStandIn(this.m_cards[index]);
  }

  public void UpdateLayout(int slotMousedOver)
  {
    this.UpdateLayout(slotMousedOver, false, -1);
  }

  public void UpdateLayout(int slotMousedOver, bool forced)
  {
    this.UpdateLayout(slotMousedOver, forced, -1);
  }

  public void UpdateLayout(int slotMousedOver, bool forced, int overrideCardCount)
  {
    this.m_updatingLayout = true;
    if (this.IsBlockingLayout())
    {
      this.UpdateLayoutFinished();
    }
    else
    {
      for (int index = 0; index < this.m_cards.Count; ++index)
      {
        Card card = this.m_cards[index];
        if (!card.IsDoNotSort() && card.GetTransitionStyle() != ZoneTransitionStyle.VERY_SLOW && (!this.IsCardNotInEnemyHandAnymore(card) && !card.HasBeenGrabbedByEnemyActionHandler()))
        {
          Spell bestSummonSpell = card.GetBestSummonSpell();
          if (!((UnityEngine.Object) bestSummonSpell != (UnityEngine.Object) null) || !bestSummonSpell.IsActive())
            card.ShowCard();
        }
      }
      if (this.m_doNotUpdateLayout)
        this.UpdateLayoutFinished();
      else if (this.m_cards.Count == 0)
        this.UpdateLayoutFinished();
      else if (slotMousedOver >= this.m_cards.Count || slotMousedOver < -1)
        this.UpdateLayoutFinished();
      else if (!forced && slotMousedOver == this.lastMousedOver)
      {
        this.m_updatingLayout = false;
        this.UpdateKeywordPanelsPosition(slotMousedOver);
      }
      else
      {
        this.m_cards.Sort(new Comparison<Card>(ZoneHand.HandCardSortComparison));
        this.UpdateLayoutImpl(slotMousedOver, overrideCardCount);
      }
    }
  }

  public static int HandCardSortComparison(Card card1, Card card2)
  {
    int num1 = !card1.IsBeingDrawnByOpponent() ? card1.GetZonePosition() : int.MaxValue;
    int num2 = !card2.IsBeingDrawnByOpponent() ? card2.GetZonePosition() : int.MaxValue;
    if (num1 != num2)
      return num1 - num2;
    return card1.GetEntity().GetZonePosition() - card2.GetEntity().GetZonePosition();
  }

  public void HideCards()
  {
    using (List<Card>.Enumerator enumerator = this.m_cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.GetActor().gameObject.SetActive(false);
    }
  }

  public void ShowCards()
  {
    using (List<Card>.Enumerator enumerator = this.m_cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.GetActor().gameObject.SetActive(true);
    }
  }

  public float GetCardWidth(int nCards)
  {
    if (nCards < 0)
      return 0.0f;
    if (nCards > ZoneHand.MAX_CARDS)
      nCards = ZoneHand.MAX_CARDS;
    float num = (float) Screen.width / (float) Screen.height;
    return (float[]) ZoneHand.CARD_PIXEL_WIDTHS[nCards] * (float) Screen.width * (float) this.BASELINE_ASPECT_RATIO / num;
  }

  public bool TouchReceived()
  {
    RaycastHit hitInfo;
    if (!UniversalInputManager.Get().GetInputHitInfo((LayerMask) GameLayer.CardRaycast.LayerBit(), out hitInfo))
      this.m_touchedSlot = -1;
    CardStandIn componentInParents = SceneUtils.FindComponentInParents<CardStandIn>((Component) hitInfo.transform);
    if ((UnityEngine.Object) componentInParents != (UnityEngine.Object) null)
    {
      this.m_touchedSlot = this.GetCardSlot(componentInParents.linkedCard);
      return true;
    }
    this.m_touchedSlot = -1;
    return false;
  }

  public void HandleInput()
  {
    Card card = (Card) null;
    if ((UnityEngine.Object) RemoteActionHandler.Get() != (UnityEngine.Object) null && (UnityEngine.Object) RemoteActionHandler.Get().GetFriendlyHoverCard() != (UnityEngine.Object) null)
    {
      Card friendlyHoverCard = RemoteActionHandler.Get().GetFriendlyHoverCard();
      if (friendlyHoverCard.GetController().IsFriendlySide() && friendlyHoverCard.GetZone() is ZoneHand)
        card = friendlyHoverCard;
    }
    int slotMousedOver1 = -1;
    if ((UnityEngine.Object) card != (UnityEngine.Object) null)
      slotMousedOver1 = this.GetCardSlot(card);
    if (UniversalInputManager.Get().IsTouchMode())
    {
      if (!InputManager.Get().LeftMouseButtonDown || this.m_touchedSlot < 0)
      {
        this.m_touchedSlot = -1;
        if (slotMousedOver1 < 0)
          this.UpdateLayout(-1);
        else
          this.UpdateLayout(slotMousedOver1);
      }
      else
      {
        float num1 = UniversalInputManager.Get().GetMousePosition().x - InputManager.Get().LastMouseDownPosition.x;
        float num2 = Mathf.Max(0.0f, UniversalInputManager.Get().GetMousePosition().y - InputManager.Get().LastMouseDownPosition.y);
        float cardWidth = this.GetCardWidth(this.m_cards.Count);
        float a = (float) (this.lastMousedOver - this.m_touchedSlot) * cardWidth;
        float num3 = (float) (10.0 + (double) num2 * (double) (float) ((MobileOverrideValue<float>) this.m_TouchDragResistanceFactorY));
        this.UpdateLayout(this.m_touchedSlot + (int) Mathf.Round(((double) num1 >= (double) a ? Mathf.Max(a, num1 - num3) : Mathf.Min(a, num1 + num3)) / cardWidth));
      }
    }
    else
    {
      CardStandIn cardStandIn = (CardStandIn) null;
      int slotMousedOver2 = -1;
      RaycastHit hitInfo;
      if (!UniversalInputManager.Get().InputHitAnyObject(Camera.main, GameLayer.InvisibleHitBox1) || !UniversalInputManager.Get().GetInputHitInfo(Camera.main, GameLayer.CardRaycast, out hitInfo))
      {
        if (slotMousedOver1 < 0)
        {
          this.UpdateLayout(-1);
          return;
        }
      }
      else
        cardStandIn = SceneUtils.FindComponentInParents<CardStandIn>((Component) hitInfo.transform);
      if ((UnityEngine.Object) cardStandIn == (UnityEngine.Object) null)
      {
        if (slotMousedOver1 < 0)
        {
          this.UpdateLayout(-1);
          return;
        }
      }
      else
        slotMousedOver2 = this.GetCardSlot(cardStandIn.linkedCard);
      if (slotMousedOver2 == this.lastMousedOver)
      {
        this.UpdateKeywordPanelsPosition(slotMousedOver2);
      }
      else
      {
        bool flag = slotMousedOver2 != -1;
        if (flag || slotMousedOver1 < 0)
        {
          this.UpdateLayout(slotMousedOver2);
        }
        else
        {
          if (flag || slotMousedOver1 < 0)
            return;
          this.UpdateLayout(slotMousedOver1);
        }
      }
    }
  }

  private void UpdateKeywordPanelsPosition(int slotMousedOver)
  {
    if (slotMousedOver < 0 || slotMousedOver >= this.m_cards.Count)
      return;
    Card card = this.m_cards[slotMousedOver];
    bool showOnRight = this.ShouldShowCardTooltipOnRight(card);
    TooltipPanelManager.Get().UpdateKeywordPanelsPosition(card, showOnRight);
  }

  private bool ShouldShowCardTooltipOnRight(Card card)
  {
    return (double) card.GetActor().GetMeshRenderer().bounds.center.x < (double) this.GetComponent<BoxCollider>().bounds.center.x;
  }

  public void ShowManaGems()
  {
    Vector3 position = this.m_manaGemPosition.transform.position;
    position.x += -0.5f * this.m_manaGemMgr.GetWidth();
    this.m_manaGemMgr.gameObject.transform.position = position;
    this.m_manaGemMgr.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
  }

  public void HideManaGems()
  {
    this.m_manaGemMgr.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
  }

  public void SetHandEnlarged(bool enlarged)
  {
    this.m_handEnlarged = enlarged;
    if (enlarged)
    {
      this.gameObject.transform.localPosition = this.m_enlargedHandPosition;
      this.gameObject.transform.localScale = this.m_enlargedHandScale;
      ManaCrystalMgr.Get().ShowPhoneManaTray();
    }
    else
    {
      this.gameObject.transform.localPosition = this.m_startingPosition;
      this.gameObject.transform.localScale = this.m_startingScale;
      ManaCrystalMgr.Get().HidePhoneManaTray();
    }
    this.UpdateCenterAndWidth();
    this.m_handMoving = true;
    this.UpdateLayout(-1, true);
    this.m_handMoving = false;
  }

  public bool HandEnlarged()
  {
    return this.m_handEnlarged;
  }

  public void SetFriendlyHeroTargetingMode(bool enable)
  {
    if (!enable && (UnityEngine.Object) this.m_hiddenStandIn != (UnityEngine.Object) null)
      this.m_hiddenStandIn.gameObject.SetActive(true);
    if (this.m_targetingMode == enable)
      return;
    this.m_targetingMode = enable;
    this.m_heroHitbox.SetActive(enable);
    if (!this.m_handEnlarged)
      return;
    if (enable)
    {
      this.m_hiddenStandIn = this.CurrentStandIn;
      if ((UnityEngine.Object) this.m_hiddenStandIn != (UnityEngine.Object) null)
        this.m_hiddenStandIn.gameObject.SetActive(false);
      Vector3 enlargedHandPosition = this.m_enlargedHandPosition;
      enlargedHandPosition.z -= this.m_handHidingDistance;
      this.gameObject.transform.localPosition = enlargedHandPosition;
    }
    else
      this.gameObject.transform.localPosition = this.m_enlargedHandPosition;
    this.UpdateCenterAndWidth();
  }

  private void UpdateLayoutImpl(int slotMousedOver, int overrideCardCount)
  {
    int num1 = 0;
    if (this.lastMousedOver != slotMousedOver && this.lastMousedOver != -1 && (this.lastMousedOver < this.m_cards.Count && (UnityEngine.Object) this.m_cards[this.lastMousedOver] != (UnityEngine.Object) null) && this.CanAnimateCard(this.m_cards[this.lastMousedOver]))
    {
      Card card = this.m_cards[this.lastMousedOver];
      iTween.Stop(card.gameObject);
      if (!this.enemyHand)
      {
        Vector3 overCardPosition = this.GetMouseOverCardPosition(card);
        Vector3 cardPosition = this.GetCardPosition(card, overrideCardCount);
        card.transform.position = new Vector3(overCardPosition.x, this.centerOfHand.y, cardPosition.z + 0.5f);
        card.transform.localScale = this.GetCardScale(card);
        card.transform.localEulerAngles = this.GetCardRotation(card);
      }
      card.NotifyMousedOut();
      GameLayer layer = GameLayer.Default;
      if (this.m_Side == Player.Side.OPPOSING && SpectatorManager.Get().IsSpectatingOpposingSide())
        layer = GameLayer.CardRaycast;
      SceneUtils.SetLayer(card.gameObject, layer);
    }
    float delaySec = 0.0f;
    for (int index = 0; index < this.m_cards.Count; ++index)
    {
      Card card = this.m_cards[index];
      if (this.CanAnimateCard(card))
      {
        ++num1;
        float z = !this.m_flipHandCards ? 354.5f : 534.5f;
        card.transform.rotation = Quaternion.Euler(new Vector3(card.transform.localEulerAngles.x, card.transform.localEulerAngles.y, z));
        float num2 = 0.5f;
        if (this.m_handMoving)
          num2 = 0.25f;
        if (this.enemyHand)
          num2 = 1.5f;
        float num3 = 0.25f;
        iTween.EaseType easeType = iTween.EaseType.easeOutExpo;
        float transitionDelay = card.GetTransitionDelay();
        card.SetTransitionDelay(0.0f);
        ZoneTransitionStyle transitionStyle = card.GetTransitionStyle();
        card.SetTransitionStyle(ZoneTransitionStyle.NORMAL);
        if (transitionStyle != ZoneTransitionStyle.NORMAL)
        {
          switch (transitionStyle)
          {
            case ZoneTransitionStyle.SLOW:
              easeType = iTween.EaseType.easeInExpo;
              num3 = num2;
              break;
            case ZoneTransitionStyle.VERY_SLOW:
              easeType = iTween.EaseType.easeInOutCubic;
              num3 = 1f;
              num2 = 1f;
              break;
          }
          card.GetActor().TurnOnCollider();
        }
        Vector3 vector3_1 = this.GetCardPosition(card, overrideCardCount);
        Vector3 vector3_2 = this.GetCardRotation(card, overrideCardCount);
        Vector3 vector3_3 = this.GetCardScale(card);
        if (index == slotMousedOver)
        {
          easeType = iTween.EaseType.easeOutExpo;
          if (this.enemyHand)
          {
            num3 = 0.15f;
            float num4 = 0.3f;
            vector3_1 = new Vector3(vector3_1.x, vector3_1.y, vector3_1.z - num4);
          }
          else
          {
            float num4 = 0.5f * (float) index - (float) (0.5 * (double) this.m_cards.Count / 2.0);
            float selectCardScale1 = (float) ((MobileOverrideValue<float>) this.m_SelectCardScale);
            float selectCardScale2 = (float) ((MobileOverrideValue<float>) this.m_SelectCardScale);
            vector3_2 = new Vector3(0.0f, 0.0f, 0.0f);
            vector3_3 = new Vector3(selectCardScale1, vector3_3.y, selectCardScale2);
            card.transform.localScale = vector3_3;
            num2 = 4f;
            float num5 = 0.1f;
            vector3_1 = this.GetMouseOverCardPosition(card);
            float x = vector3_1.x;
            if (this.m_handEnlarged)
            {
              vector3_1.x = Mathf.Max(vector3_1.x, this.m_enlargedHandCardMinX);
              vector3_1.x = Mathf.Min(vector3_1.x, this.m_enlargedHandCardMaxX);
            }
            card.transform.position = new Vector3((double) x == (double) vector3_1.x ? card.transform.position.x : vector3_1.x, vector3_1.y, vector3_1.z - num5);
            card.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
            iTween.Stop(card.gameObject);
            easeType = iTween.EaseType.easeOutExpo;
            if ((bool) ((UnityEngine.Object) CardTypeBanner.Get()))
              CardTypeBanner.Get().Show(card.GetActor());
            InputManager.Get().SetMousedOverCard(card);
            bool showOnRight = this.ShouldShowCardTooltipOnRight(card);
            TooltipPanelManager.Get().UpdateKeywordHelp(card, card.GetActor(), showOnRight, new float?(), new Vector3?());
            SceneUtils.SetLayer(card.gameObject, GameLayer.Tooltip);
          }
        }
        else if ((UnityEngine.Object) this.GetStandIn(card) != (UnityEngine.Object) null)
        {
          CardStandIn standIn = this.GetStandIn(card);
          iTween.Stop(standIn.gameObject);
          standIn.transform.position = vector3_1;
          if (!card.CardStandInIsInteractive())
            standIn.DisableStandIn();
          else
            standIn.EnableStandIn();
        }
        card.EnableTransitioningZones(true);
        string tweenName = ZoneMgr.Get().GetTweenName<ZoneHand>();
        Hashtable args1 = iTween.Hash((object) "scale", (object) vector3_3, (object) "delay", (object) transitionDelay, (object) "time", (object) num3, (object) "easeType", (object) easeType, (object) "name", (object) tweenName);
        iTween.ScaleTo(card.gameObject, args1);
        Hashtable args2 = iTween.Hash((object) "rotation", (object) vector3_2, (object) "delay", (object) transitionDelay, (object) "time", (object) num3, (object) "easeType", (object) easeType, (object) "name", (object) tweenName);
        iTween.RotateTo(card.gameObject, args2);
        Hashtable args3 = iTween.Hash((object) "position", (object) vector3_1, (object) "delay", (object) transitionDelay, (object) "time", (object) num2, (object) "easeType", (object) easeType, (object) "name", (object) tweenName);
        iTween.MoveTo(card.gameObject, args3);
        delaySec = Mathf.Max(delaySec, (float) ((double) transitionDelay + (double) num2), (float) ((double) transitionDelay + (double) num3));
      }
    }
    this.lastMousedOver = slotMousedOver;
    if (num1 > 0)
      this.StartFinishLayoutTimer(delaySec);
    else
      this.UpdateLayoutFinished();
  }

  private void CreateCardStandIn(Card card)
  {
    Actor actor = card.GetActor();
    if ((UnityEngine.Object) actor != (UnityEngine.Object) null)
      actor.GetMeshRenderer().gameObject.layer = 0;
    GameObject gameObject = AssetLoader.Get().LoadActor("Card_Collider_Standin", false, false);
    gameObject.transform.localEulerAngles = this.GetCardRotation(card);
    gameObject.transform.position = this.GetCardPosition(card);
    gameObject.transform.localScale = this.GetCardScale(card);
    CardStandIn component = gameObject.GetComponent<CardStandIn>();
    component.linkedCard = card;
    this.standIns.Add(component);
    if (component.linkedCard.CardStandInIsInteractive())
      return;
    component.DisableStandIn();
  }

  private CardStandIn GetStandIn(Card card)
  {
    if (this.standIns == null)
      return (CardStandIn) null;
    using (List<CardStandIn>.Enumerator enumerator = this.standIns.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardStandIn current = enumerator.Current;
        if (!((UnityEngine.Object) current == (UnityEngine.Object) null) && (UnityEngine.Object) current.linkedCard == (UnityEngine.Object) card)
          return current;
      }
    }
    return (CardStandIn) null;
  }

  public void MakeStandInInteractive(Card card)
  {
    if ((UnityEngine.Object) this.GetStandIn(card) == (UnityEngine.Object) null)
      return;
    this.GetStandIn(card).EnableStandIn();
  }

  private void BlowUpOldStandins()
  {
    if (this.standIns == null)
    {
      this.standIns = new List<CardStandIn>();
    }
    else
    {
      using (List<CardStandIn>.Enumerator enumerator = this.standIns.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CardStandIn current = enumerator.Current;
          if (!((UnityEngine.Object) current == (UnityEngine.Object) null))
            UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
      this.standIns = new List<CardStandIn>();
    }
  }

  public int GetCardSlot(Card card)
  {
    return this.m_cards.IndexOf(card);
  }

  public Vector3 GetCardPosition(Card card)
  {
    return this.GetCardPosition(card, -1);
  }

  public Vector3 GetCardPosition(Card card, int overrideCardCount)
  {
    return this.GetCardPosition(this.GetCardSlot(card), overrideCardCount);
  }

  public Vector3 GetCardPosition(int slot, int overrideCardCount)
  {
    float num1 = 0.0f;
    float num2 = 0.0f;
    float num3 = 0.0f;
    int num4 = this.m_cards.Count;
    if (overrideCardCount >= 0 && overrideCardCount < this.m_cards.Count)
      num4 = overrideCardCount;
    if (!this.enemyHand)
      num4 -= TurnStartManager.Get().GetNumCardsToDraw();
    if (this.IsHandScrunched())
    {
      num3 = 1f;
      float num5 = 40f;
      if (!this.enemyHand)
        num5 += (float) (num4 * 2);
      num1 = num5 / (float) num4;
      num2 = (float) (-(double) num5 / 2.0);
    }
    float f = !this.enemyHand ? 0.0f + (num1 * (float) slot + num2) : (float) (0.0 - (double) num1 * ((double) slot + 0.5)) - num2;
    float num6 = 0.0f;
    if (this.enemyHand && (double) f < 0.0 || !this.enemyHand && (double) f > 0.0)
      num6 = (float) ((double) Mathf.Sin((float) ((double) Mathf.Abs(f) * 3.14159274101257 / 180.0)) * (double) this.GetCardSpacing() / 2.0);
    float x = this.centerOfHand.x - this.GetCardSpacing() / 2f * (float) (num4 - 1 - slot * 2);
    if (this.m_handEnlarged && this.m_targetingMode)
    {
      if (num4 % 2 > 0)
      {
        if (slot < (num4 + 1) / 2)
          x -= this.m_heroWidthInHand;
      }
      else if (slot < num4 / 2)
        x -= this.m_heroWidthInHand / 2f;
      else
        x += this.m_heroWidthInHand / 2f;
    }
    float y = this.centerOfHand.y;
    float z = this.centerOfHand.z;
    if (num4 > 1)
    {
      if (this.enemyHand)
        z += Mathf.Pow((float) Mathf.Abs(slot - num4 / 2), 2f) / (float) (4 * num4) * num3 + num6;
      else
        z = this.centerOfHand.z - Mathf.Pow((float) Mathf.Abs(slot - num4 / 2), 2f) / (float) (4 * num4) * num3 - num6;
    }
    if (this.enemyHand && SpectatorManager.Get().IsSpectatingOpposingSide())
      z -= 0.2f;
    return new Vector3(x, y, z);
  }

  public Vector3 GetCardRotation(Card card)
  {
    return this.GetCardRotation(card, -1);
  }

  public Vector3 GetCardRotation(Card card, int overrideCardCount)
  {
    int cardSlot = this.GetCardSlot(card);
    float num1 = 0.0f;
    float num2 = 0.0f;
    int num3 = this.m_cards.Count;
    if (overrideCardCount >= 0 && overrideCardCount < this.m_cards.Count)
      num3 = overrideCardCount;
    if (!this.enemyHand)
      num3 -= TurnStartManager.Get().GetNumCardsToDraw();
    if (this.IsHandScrunched())
    {
      float num4 = 40f;
      if (!this.enemyHand)
        num4 += (float) (num3 * 2);
      num1 = num4 / (float) num3;
      num2 = (float) (-(double) num4 / 2.0);
    }
    float y = !this.enemyHand ? 0.0f + (num1 * (float) cardSlot + num2) : (float) (0.0 - (double) num1 * ((double) cardSlot + 0.5)) - num2;
    if (this.enemyHand && SpectatorManager.Get().IsSpectatingOpposingSide())
      y += 180f;
    float z = !this.m_flipHandCards ? 354.5f : 534.5f;
    return new Vector3(0.0f, y, z);
  }

  public Vector3 GetCardScale(Card card)
  {
    if (this.enemyHand)
      return new Vector3(0.682f, 0.225f, 0.682f);
    if ((bool) UniversalInputManager.UsePhoneUI && this.m_handEnlarged)
      return this.m_enlargedHandCardScale;
    return new Vector3(0.62f, 0.225f, 0.62f);
  }

  private Vector3 GetMouseOverCardPosition(Card card)
  {
    return new Vector3(this.GetCardPosition(card).x, this.centerOfHand.y + 1f, this.transform.FindChild("MouseOverCardHeight").position.z + (float) ((MobileOverrideValue<float>) this.m_SelectCardOffsetZ));
  }

  private float GetCardSpacing()
  {
    float num1 = this.GetDefaultCardSpacing();
    int count = this.m_cards.Count;
    if (!this.enemyHand)
      count -= TurnStartManager.Get().GetNumCardsToDraw();
    float num2 = num1 * (float) count;
    float num3 = this.MaxHandWidth();
    if ((double) num2 > (double) num3)
      num1 = num3 / (float) count;
    return num1;
  }

  private float MaxHandWidth()
  {
    float maxWidth = this.m_maxWidth;
    if (this.m_handEnlarged && this.m_targetingMode)
      maxWidth -= this.m_heroWidthInHand;
    return maxWidth;
  }

  protected bool CanAnimateCard(Card card)
  {
    bool flag = this.enemyHand && card.GetPrevZone() is ZonePlay;
    if (card.IsDoNotSort())
    {
      if (flag)
        Log.FaceDownCard.Print("ZoneHand.CanAnimateCard() - card={0} FAILED card.IsDoNotSort()", (object) card);
      return false;
    }
    if (!card.IsActorReady())
    {
      if (flag)
        Log.FaceDownCard.Print("ZoneHand.CanAnimateCard() - card={0} FAILED !card.IsActorReady()", (object) card);
      return false;
    }
    if (this.m_controller.IsFriendlySide() && (bool) ((UnityEngine.Object) TurnStartManager.Get()) && TurnStartManager.Get().IsCardDrawHandled(card))
      return false;
    if (this.IsCardNotInEnemyHandAnymore(card))
    {
      if (flag)
        Log.FaceDownCard.Print("ZoneHand.CanAnimateCard() - card={0} FAILED IsCardNotInEnemyHandAnymore()", (object) card);
      return false;
    }
    if (!card.HasBeenGrabbedByEnemyActionHandler())
      return true;
    if (flag)
      Log.FaceDownCard.Print("ZoneHand.CanAnimateCard() - card={0} FAILED card.HasBeenGrabbedByEnemyActionHandler()", (object) card);
    return false;
  }

  private bool IsCardNotInEnemyHandAnymore(Card card)
  {
    if (card.GetEntity().GetZone() != TAG_ZONE.HAND)
      return this.enemyHand;
    return false;
  }

  private void UpdateCenterAndWidth()
  {
    this.centerOfHand = this.GetComponent<Collider>().bounds.center;
    this.m_maxWidth = this.GetComponent<Collider>().bounds.size.x;
  }

  private bool CanPlaySpellPowerHint(Card card)
  {
    if (!card.IsShown() || !card.GetActor().IsShown())
      return false;
    Entity entity = card.GetEntity();
    if (entity.IsAffectedBySpellPower())
      return true;
    return TextUtils.HasBonusDamage(entity.GetCardTextInHand());
  }

  public override bool AddCard(Card card)
  {
    return base.AddCard(card);
  }
}
