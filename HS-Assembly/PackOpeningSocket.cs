﻿// Decompiled with JetBrains decompiler
// Type: PackOpeningSocket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class PackOpeningSocket : PegUIElement
{
  private Spell m_alertSpell;

  protected override void Awake()
  {
    base.Awake();
    this.m_alertSpell = this.GetComponent<Spell>();
  }

  public void OnPackHeld()
  {
    this.m_alertSpell.ActivateState(SpellStateType.BIRTH);
  }

  public void OnPackReleased()
  {
    this.m_alertSpell.ActivateState(SpellStateType.DEATH);
  }
}
