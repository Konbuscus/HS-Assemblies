﻿// Decompiled with JetBrains decompiler
// Type: ResizableTooltipPanel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ResizableTooltipPanel : TooltipPanel
{
  public override void Initialize(string keywordName, string keywordText)
  {
    if ((Object) this.m_background.GetComponent<NewThreeSliceElement>() == (Object) null)
      Error.AddDevFatal("Prefab expecting m_background to have a NewThreeSliceElement!");
    base.Initialize(keywordName, keywordText);
    float height = this.m_name.Height;
    float num1 = this.m_body.GetTextBounds().size.y;
    if (keywordText == string.Empty)
      num1 = 0.0f;
    float num2 = 1f;
    if ((double) this.m_initialBackgroundHeight == 0.0 || this.m_initialBackgroundScale == Vector3.zero)
    {
      this.m_initialBackgroundHeight = this.m_background.GetComponent<NewThreeSliceElement>().m_middle.GetComponent<Renderer>().bounds.size.z;
      this.m_initialBackgroundScale = this.m_background.GetComponent<NewThreeSliceElement>().m_middle.transform.localScale;
    }
    this.m_background.GetComponent<NewThreeSliceElement>().SetSize(new Vector3(this.m_initialBackgroundScale.x, this.m_initialBackgroundScale.y * ((height + num1) * num2) / this.m_initialBackgroundHeight, this.m_initialBackgroundScale.z));
  }

  public override float GetHeight()
  {
    return this.m_background.GetComponent<NewThreeSliceElement>().m_leftOrTop.GetComponent<Renderer>().bounds.size.z + this.m_background.GetComponent<NewThreeSliceElement>().m_middle.GetComponent<Renderer>().bounds.size.z + this.m_background.GetComponent<NewThreeSliceElement>().m_rightOrBottom.GetComponent<Renderer>().bounds.size.z;
  }

  public override float GetWidth()
  {
    return this.m_background.GetComponent<NewThreeSliceElement>().m_leftOrTop.GetComponent<Renderer>().bounds.size.x;
  }
}
