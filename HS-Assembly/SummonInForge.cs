﻿// Decompiled with JetBrains decompiler
// Type: SummonInForge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class SummonInForge : SpellImpl
{
  public float m_burnInAnimationSpeed = 1f;
  public GameObject m_burnIn;
  public GameObject m_blackBits;
  public GameObject m_smokePuff;
  public bool m_isHeroActor;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    this.StartCoroutine(this.BirthState());
  }

  [DebuggerHidden]
  private IEnumerator BirthState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SummonInForge.\u003CBirthState\u003Ec__Iterator2D3() { \u003C\u003Ef__this = this };
  }
}
