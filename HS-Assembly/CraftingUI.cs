﻿// Decompiled with JetBrains decompiler
// Type: CraftingUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class CraftingUI : MonoBehaviour
{
  private List<GameObject> m_thingsToDestroy = new List<GameObject>();
  public UberText m_bankAmountText;
  public CreateButton m_buttonCreate;
  public DisenchantButton m_buttonDisenchant;
  public GameObject m_soulboundNotification;
  public UberText m_soulboundTitle;
  public UberText m_soulboundDesc;
  public UberText m_disenchantValue;
  public UberText m_craftValue;
  public GameObject m_wildTheming;
  public float m_disenchantDelayBeforeCardExplodes;
  public float m_disenchantDelayBeforeCardFlips;
  public float m_disenchantDelayBeforeBallsComeOut;
  public float m_craftDelayBeforeConstructSpell;
  public float m_craftDelayBeforeGhostDeath;
  public GameObject m_glowballs;
  public SoundDef m_craftingSound;
  public SoundDef m_disenchantSound;
  public Collider m_mouseOverCollider;
  private Actor m_explodingActor;
  private Actor m_constructingActor;
  private bool m_isAnimating;
  private GameObject m_activeObject;
  private bool m_enabled;
  private bool m_mousedOver;
  private Notification m_craftNotification;
  private bool m_initializedPositions;

  private void Update()
  {
    if (!this.m_enabled)
      return;
    if (this.m_isAnimating)
    {
      this.m_mousedOver = false;
    }
    else
    {
      Ray ray = Camera.main.ScreenPointToRay(UniversalInputManager.Get().GetMousePosition());
      LayerMask layerMask = (LayerMask) 512;
      RaycastHit hitInfo;
      if (!Physics.Raycast(ray, out hitInfo, Camera.main.farClipPlane, (int) layerMask))
        return;
      if ((Object) hitInfo.collider == (Object) this.m_mouseOverCollider)
        this.NotifyOfMouseOver();
      else
        this.NotifyOfMouseOut();
    }
  }

  private void OnDisable()
  {
    this.StopCurrentAnim(true);
  }

  public void UpdateWildTheming()
  {
    if ((Object) this.m_wildTheming == (Object) null)
      return;
    EntityDef entityDef;
    TAG_PREMIUM premium;
    if (!CraftingManager.Get().GetShownCardInfo(out entityDef, out premium))
      this.m_wildTheming.SetActive(false);
    else
      this.m_wildTheming.SetActive(GameUtils.IsCardRotated(entityDef));
  }

  public void UpdateText()
  {
    this.UpdateBankText();
    EntityDef entityDef;
    TAG_PREMIUM premium;
    if (!CraftingManager.Get().GetShownCardInfo(out entityDef, out premium))
    {
      this.m_buttonDisenchant.DisableButton();
      this.m_buttonCreate.DisableButton();
    }
    else
    {
      NetCache.CardDefinition cardDefinition = new NetCache.CardDefinition()
      {
        Name = entityDef.GetCardId(),
        Premium = premium
      };
      int ownedIncludePending = CraftingManager.Get().GetNumOwnedIncludePending();
      string empty1 = string.Empty;
      string empty2 = string.Empty;
      TAG_CARD_SET cardSet = entityDef.GetCardSet();
      string cardSetName = GameStrings.GetCardSetName(cardSet);
      NetCache.CardValue cardValue = CraftingManager.Get().GetCardValue(cardDefinition.Name, cardDefinition.Premium);
      string str1 = GameStrings.Get("GLUE_CRAFTING_SOULBOUND");
      string str2;
      if (ownedIncludePending <= 0)
      {
        str1 = cardSetName;
        str2 = entityDef.GetHowToEarnText(cardDefinition.Premium);
      }
      else
        str2 = cardSet != TAG_CARD_SET.CORE ? (cardSet == TAG_CARD_SET.REWARD || cardSet == TAG_CARD_SET.PROMO ? GameStrings.Get("GLUE_CRAFTING_SOULBOUND_REWARD_DESC") : GameStrings.Get("GLUE_CRAFTING_SOULBOUND_DESC")) : GameStrings.Get("GLUE_CRAFTING_SOULBOUND_BASIC_DESC");
      bool flag;
      if (cardValue == null)
        flag = false;
      else if (this.IsCraftingEventForCardActive(cardDefinition.Name))
      {
        int clientTransactions = CraftingManager.Get().GetNumClientTransactions();
        int num1 = cardValue.Buy;
        if (clientTransactions < 0)
          num1 = cardValue.Sell;
        int num2 = cardValue.Sell;
        if (clientTransactions > 0)
          num2 = cardValue.Buy;
        this.m_disenchantValue.Text = "+" + num2.ToString();
        this.m_craftValue.Text = "-" + num1.ToString();
        flag = true;
      }
      else
      {
        str1 = GameStrings.Get("GLUE_CRAFTING_EVENT_NOT_ACTIVE_TITLE");
        str2 = GameStrings.Format("GLUE_CRAFTING_EVENT_NOT_ACTIVE_DESCRIPTION", (object) cardSetName);
        flag = false;
      }
      this.m_soulboundTitle.Text = str1;
      this.m_soulboundDesc.Text = str2;
      if (!flag)
      {
        this.m_buttonDisenchant.DisableButton();
        this.m_buttonCreate.DisableButton();
        this.m_soulboundNotification.SetActive(true);
        this.m_activeObject = this.m_soulboundNotification;
      }
      else if (!FixedRewardsMgr.Get().CanCraftCard(cardDefinition.Name, cardDefinition.Premium))
      {
        this.m_buttonDisenchant.DisableButton();
        this.m_buttonCreate.DisableButton();
        this.m_soulboundNotification.SetActive(true);
        this.m_activeObject = this.m_soulboundNotification;
      }
      else
      {
        this.m_soulboundNotification.SetActive(false);
        this.m_activeObject = this.gameObject;
        if (ownedIncludePending <= 0)
          this.m_buttonDisenchant.DisableButton();
        else
          this.m_buttonDisenchant.EnableButton();
        int num = !entityDef.IsElite() ? 2 : 1;
        long arcaneDustBalance = CraftingManager.Get().GetLocalArcaneDustBalance();
        if (ownedIncludePending >= num || arcaneDustBalance < (long) this.GetCardBuyValue(cardDefinition.Name, cardDefinition.Premium))
          this.m_buttonCreate.DisableButton();
        else
          this.m_buttonCreate.EnableButton();
      }
    }
  }

  public void DoDisenchant()
  {
    this.UpdateTips();
    Options.Get().SetBool(Option.HAS_DISENCHANTED, true);
    CraftingManager.Get().AdjustLocalArcaneDustBalance(this.GetCardSellValue(CraftingManager.Get().GetShownActor().GetEntityDef().GetCardId(), CraftingManager.Get().GetShownActor().GetPremium()));
    CraftingManager.Get().NotifyOfTransaction(-1);
    this.UpdateText();
    if (this.m_isAnimating)
      CraftingManager.Get().FinishFlipCurrentActorEarly();
    this.StopCurrentAnim(false);
    this.StartCoroutine(this.DoDisenchantAnims());
    CraftingManager.Get().StartCoroutine(this.StartCraftCooldown());
  }

  public void CleanUpEffects()
  {
    if ((Object) this.m_explodingActor != (Object) null)
    {
      Spell spell = this.m_explodingActor.GetSpell(SpellType.DECONSTRUCT);
      if ((Object) spell != (Object) null && spell.GetActiveState() != SpellStateType.NONE)
      {
        this.m_explodingActor.GetSpell(SpellType.DECONSTRUCT).GetComponent<PlayMakerFSM>().SendEvent("Cancel");
        this.m_explodingActor.Hide();
      }
    }
    if ((Object) this.m_constructingActor != (Object) null)
    {
      Spell spell = this.m_constructingActor.GetSpell(SpellType.CONSTRUCT);
      if ((Object) spell != (Object) null && spell.GetActiveState() != SpellStateType.NONE)
      {
        this.m_constructingActor.GetSpell(SpellType.CONSTRUCT).GetComponent<PlayMakerFSM>().SendEvent("Cancel");
        this.m_constructingActor.Hide();
      }
    }
    this.GetComponent<PlayMakerFSM>().SendEvent("Cancel");
    this.m_isAnimating = false;
  }

  public void DoCreate()
  {
    this.UpdateTips();
    if (!Options.Get().GetBool(Option.HAS_CRAFTED))
      Options.Get().SetBool(Option.HAS_CRAFTED, true);
    CraftingManager.Get().AdjustLocalArcaneDustBalance(-this.GetCardBuyValue(CraftingManager.Get().GetShownActor().GetEntityDef().GetCardId(), CraftingManager.Get().GetShownActor().GetPremium()));
    CraftingManager.Get().NotifyOfTransaction(1);
    if (CraftingManager.Get().GetNumOwnedIncludePending() > 1)
      CraftingManager.Get().ForceNonGhostFlagOn();
    this.UpdateText();
    this.StopCurrentAnim(false);
    this.StartCoroutine(this.DoCreateAnims());
    CraftingManager.Get().StartCoroutine(this.StartDisenchantCooldown());
  }

  public void UpdateBankText()
  {
    this.m_bankAmountText.Text = CraftingManager.Get().GetLocalArcaneDustBalance().ToString();
    BnetBar.Get().m_currencyFrame.RefreshContents();
    if (!(bool) UniversalInputManager.UsePhoneUI || !((Object) CraftingTray.Get() != (Object) null))
      return;
    ArcaneDustAmount.Get().UpdateCurrentDustAmount();
  }

  public void Disable(Vector3 hidePosition)
  {
    this.m_enabled = false;
    iTween.MoveTo(this.m_activeObject, iTween.Hash((object) "time", (object) 0.4f, (object) "position", (object) hidePosition));
    this.HideTips();
  }

  public bool IsEnabled()
  {
    return this.m_enabled;
  }

  public void Enable(Vector3 showPosition, Vector3 hidePosition)
  {
    if (!this.m_initializedPositions)
    {
      this.transform.position = hidePosition;
      this.m_soulboundNotification.transform.position = this.transform.position;
      this.m_soulboundTitle.Text = GameStrings.Get("GLUE_CRAFTING_SOULBOUND");
      this.m_soulboundDesc.Text = GameStrings.Get("GLUE_CRAFTING_SOULBOUND_DESC");
      this.m_activeObject = this.gameObject;
      this.m_initializedPositions = true;
    }
    this.m_enabled = true;
    this.UpdateText();
    this.UpdateWildTheming();
    this.m_activeObject.SetActive(true);
    iTween.MoveTo(this.m_activeObject, iTween.Hash((object) "time", (object) 0.5f, (object) "position", (object) showPosition));
    this.ShowFirstTimeTips();
  }

  public void SetStartingActive()
  {
    this.m_soulboundNotification.SetActive(false);
    this.gameObject.SetActive(false);
  }

  private void ShowFirstTimeTips()
  {
    if ((Object) this.m_activeObject == (Object) this.m_soulboundNotification || Options.Get().GetBool(Option.HAS_CRAFTED) || !UserAttentionManager.CanShowAttentionGrabber("CraftingUI.ShowFirstTimeTips"))
      return;
    this.CreateDisenchantNotification();
    this.CreateCraftNotification();
  }

  private void CreateDisenchantNotification()
  {
    if (!this.m_buttonDisenchant.IsButtonEnabled())
      ;
  }

  private void CreateCraftNotification()
  {
    if (!this.m_buttonCreate.IsButtonEnabled())
      return;
    Vector3 position;
    Notification.PopUpArrowDirection direction;
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      position = new Vector3(73.3f, 1f, 55.4f);
      direction = Notification.PopUpArrowDirection.Down;
    }
    else
    {
      position = new Vector3(55f, 1f, -56f);
      direction = Notification.PopUpArrowDirection.Left;
    }
    this.m_craftNotification = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, position, 16f * Vector3.one, GameStrings.Get("GLUE_COLLECTION_TUTORIAL06"), false);
    if (!((Object) this.m_craftNotification != (Object) null))
      return;
    this.m_craftNotification.ShowPopUpArrow(direction);
  }

  private void UpdateTips()
  {
    if (Options.Get().GetBool(Option.HAS_CRAFTED) || !UserAttentionManager.CanShowAttentionGrabber("CraftingUI.UpdateTips"))
      this.HideTips();
    else if ((Object) this.m_craftNotification == (Object) null)
    {
      this.CreateCraftNotification();
    }
    else
    {
      if (this.m_buttonCreate.IsButtonEnabled())
        return;
      NotificationManager.Get().DestroyNotification(this.m_craftNotification, 0.0f);
    }
  }

  private void HideTips()
  {
    if (!((Object) this.m_craftNotification != (Object) null))
      return;
    NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.m_craftNotification);
  }

  private void NotifyOfMouseOver()
  {
    if (this.m_mousedOver)
      return;
    this.m_mousedOver = true;
    this.GetComponent<PlayMakerFSM>().SendEvent("Idle");
  }

  private void NotifyOfMouseOut()
  {
    if (!this.m_mousedOver)
      return;
    this.m_mousedOver = false;
    this.GetComponent<PlayMakerFSM>().SendEvent("IdleCancel");
  }

  private int GetCardBuyValue(string cardID, TAG_PREMIUM premium)
  {
    NetCache.CardValue cardValue = CraftingManager.Get().GetCardValue(cardID, premium);
    if (CraftingManager.Get().GetNumClientTransactions() >= 0)
      return cardValue.Buy;
    return cardValue.Sell;
  }

  private int GetCardSellValue(string cardID, TAG_PREMIUM premium)
  {
    NetCache.CardValue cardValue = CraftingManager.Get().GetCardValue(cardID, premium);
    if (CraftingManager.Get().GetNumClientTransactions() <= 0)
      return cardValue.Sell;
    return cardValue.Buy;
  }

  private bool IsCraftingEventForCardActive(string cardID)
  {
    CardDbfRecord cardRecord = GameUtils.GetCardRecord(cardID);
    if (cardRecord != null)
      return SpecialEventManager.Get().IsEventActive(cardRecord.CraftingEvent, true);
    UnityEngine.Debug.LogWarning((object) string.Format("CraftingUI.IsCraftingEventForCardActive could not find DBF record for card {0}, assuming it cannot be crafted or disenchanted", (object) cardID));
    return false;
  }

  private void StopCurrentAnim(bool forceCleanup = false)
  {
    if (!this.m_isAnimating && !forceCleanup)
      return;
    this.StopAllCoroutines();
    this.CleanUpEffects();
    using (List<GameObject>.Enumerator enumerator = this.m_thingsToDestroy.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          Log.JMac.Print("StopCurrentAnim: Destroying GameObject {0}", (object) current);
          Object.Destroy((Object) current);
        }
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator StartDisenchantCooldown()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingUI.\u003CStartDisenchantCooldown\u003Ec__Iterator50()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator StartCraftCooldown()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingUI.\u003CStartCraftCooldown\u003Ec__Iterator51()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator DoDisenchantAnims()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingUI.\u003CDoDisenchantAnims\u003Ec__Iterator52()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator DoCreateAnims()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingUI.\u003CDoCreateAnims\u003Ec__Iterator53()
    {
      \u003C\u003Ef__this = this
    };
  }
}
