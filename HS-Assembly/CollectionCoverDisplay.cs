﻿// Decompiled with JetBrains decompiler
// Type: CollectionCoverDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CollectionCoverDisplay : PegUIElement
{
  private readonly string CRACK_LATCH_OPEN_ANIM_COROUTINE = "AnimateLatchCrackOpen";
  private readonly string LATCH_OPEN_ANIM_NAME = "CollectionManagerCoverV2_Lock_edit";
  private readonly float LATCH_OPEN_ANIM_SPEED = 4f;
  private readonly float LATCH_FADE_TIME = 0.1f;
  private readonly float LATCH_FADE_DELAY = 0.15f;
  private readonly float BOOK_COVER_FULLY_OPEN_Z_ROTATION = 280f;
  private readonly float BOOK_COVER_FULL_ANIM_TIME = 0.75f;
  public GameObject m_bookCoverLatch;
  public GameObject m_bookCoverLatchJoint;
  public GameObject m_bookCover;
  public Material m_latchFadeMaterial;
  public Material m_latchOpaqueMaterial;
  private readonly float BOOK_COVER_FULLY_CLOSED_Z_ROTATION;
  private bool m_isAnimating;
  private BoxCollider m_boxCollider;

  protected override void Awake()
  {
    base.Awake();
    this.m_boxCollider = this.transform.GetComponent<BoxCollider>();
  }

  private void Start()
  {
  }

  private void Update()
  {
  }

  public bool IsAnimating()
  {
    return this.m_isAnimating;
  }

  public void Open(CollectionCoverDisplay.DelOnOpened callback)
  {
    if ((double) this.m_bookCover.transform.localEulerAngles.z == (double) this.BOOK_COVER_FULLY_OPEN_Z_ROTATION)
      return;
    this.EnableCollider(false);
    this.SetIsAnimating(true);
    this.AnimateLatchOpening();
    this.AnimateCoverOpening(callback);
    SoundManager.Get().LoadAndPlay("collection_manager_book_open");
  }

  public void SetOpenState()
  {
    if (!this.m_bookCover.activeSelf)
      return;
    this.EnableCollider(false);
    this.SetIsAnimating(false);
    this.m_bookCover.SetActive(false);
    this.m_bookCoverLatchJoint.GetComponent<Renderer>().enabled = false;
  }

  public void Close()
  {
    this.m_bookCover.SetActive(true);
    if ((double) this.m_bookCover.transform.localEulerAngles.z == (double) this.BOOK_COVER_FULLY_CLOSED_Z_ROTATION)
      return;
    this.SetIsAnimating(true);
    this.AnimateCoverClosing();
    SoundManager.Get().LoadAndPlay("collection_manager_book_close");
  }

  private void SetIsAnimating(bool animating)
  {
    this.m_isAnimating = animating;
  }

  private void EnableCollider(bool enabled)
  {
    this.SetEnabled(enabled);
    this.m_boxCollider.enabled = enabled;
  }

  private void AnimateLatchOpening()
  {
    this.m_bookCoverLatch.GetComponent<Animation>()[this.LATCH_OPEN_ANIM_NAME].speed = this.LATCH_OPEN_ANIM_SPEED;
    if (this.m_bookCoverLatch.GetComponent<Animation>().IsPlaying(this.LATCH_OPEN_ANIM_NAME))
    {
      this.StopCoroutine(this.CRACK_LATCH_OPEN_ANIM_COROUTINE);
    }
    else
    {
      this.m_bookCoverLatch.GetComponent<Animation>()[this.LATCH_OPEN_ANIM_NAME].time = 0.0f;
      this.m_bookCoverLatch.GetComponent<Animation>().Play(this.LATCH_OPEN_ANIM_NAME);
    }
    iTween.FadeTo(this.m_bookCoverLatchJoint, iTween.Hash((object) "amount", (object) 0, (object) "delay", (object) this.LATCH_FADE_DELAY, (object) "time", (object) this.LATCH_FADE_TIME, (object) "easeType", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "OnLatchOpened", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void AnimateCoverOpening(CollectionCoverDisplay.DelOnOpened callback)
  {
    this.m_bookCoverLatchJoint.GetComponent<Renderer>().material = this.m_latchFadeMaterial;
    Vector3 localEulerAngles = this.m_bookCover.transform.localEulerAngles;
    localEulerAngles.z = this.BOOK_COVER_FULLY_OPEN_Z_ROTATION;
    Hashtable args = iTween.Hash((object) "rotation", (object) localEulerAngles, (object) "isLocal", (object) true, (object) "time", (object) this.BOOK_COVER_FULL_ANIM_TIME, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) "OnCoverOpened", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) callback, (object) "name", (object) "rotation");
    iTween.StopByName(this.m_bookCover.gameObject, "rotation");
    iTween.RotateTo(this.m_bookCover.gameObject, args);
  }

  private void AnimateCoverClosing()
  {
    Vector3 localEulerAngles = this.m_bookCover.transform.localEulerAngles;
    localEulerAngles.z = this.BOOK_COVER_FULLY_CLOSED_Z_ROTATION;
    Hashtable args = iTween.Hash((object) "rotation", (object) localEulerAngles, (object) "isLocal", (object) true, (object) "time", (object) this.BOOK_COVER_FULL_ANIM_TIME, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) "AnimateLatchClosing", (object) "oncompletetarget", (object) this.gameObject, (object) "name", (object) "rotation");
    iTween.StopByName(this.m_bookCover.gameObject, "rotation");
    iTween.RotateTo(this.m_bookCover.gameObject, args);
  }

  private void AnimateLatchClosing()
  {
    this.m_bookCoverLatchJoint.GetComponent<Renderer>().enabled = true;
    this.m_bookCoverLatchJoint.GetComponent<Renderer>().material = this.m_latchFadeMaterial;
    this.m_bookCoverLatch.GetComponent<Animation>()[this.LATCH_OPEN_ANIM_NAME].time = this.m_bookCoverLatch.GetComponent<Animation>()[this.LATCH_OPEN_ANIM_NAME].length;
    this.m_bookCoverLatch.GetComponent<Animation>()[this.LATCH_OPEN_ANIM_NAME].speed = (float) (-(double) this.LATCH_OPEN_ANIM_SPEED * 2.0);
    Hashtable args = iTween.Hash((object) "amount", (object) 1, (object) "time", (object) this.LATCH_FADE_TIME, (object) "easeType", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "OnLatchClosed", (object) "oncompletetarget", (object) this.gameObject);
    this.m_bookCoverLatch.GetComponent<Animation>().Play(this.LATCH_OPEN_ANIM_NAME);
    iTween.FadeTo(this.m_bookCoverLatchJoint, args);
  }

  private void OnCoverOpened(CollectionCoverDisplay.DelOnOpened callback)
  {
    this.m_bookCover.SetActive(false);
    this.SetIsAnimating(false);
    if (callback == null)
      return;
    callback();
  }

  private void OnLatchOpened()
  {
    this.m_bookCoverLatchJoint.GetComponent<Renderer>().enabled = false;
  }

  private void OnLatchClosed()
  {
    this.EnableCollider(true);
    this.SetIsAnimating(false);
  }

  private void CrackOpen()
  {
    if (this.IsAnimating())
      return;
    this.StopCoroutine(this.CRACK_LATCH_OPEN_ANIM_COROUTINE);
    this.StartCoroutine(this.CRACK_LATCH_OPEN_ANIM_COROUTINE);
  }

  [DebuggerHidden]
  private IEnumerator AnimateLatchCrackOpen()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionCoverDisplay.\u003CAnimateLatchCrackOpen\u003Ec__Iterator2E()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void CrackClose()
  {
    if (this.IsAnimating() || !this.m_bookCoverLatch.GetComponent<Animation>().IsPlaying(this.LATCH_OPEN_ANIM_NAME))
      return;
    this.StopCoroutine(this.CRACK_LATCH_OPEN_ANIM_COROUTINE);
    this.m_bookCoverLatch.GetComponent<Animation>()[this.LATCH_OPEN_ANIM_NAME].speed = -this.LATCH_OPEN_ANIM_SPEED;
  }

  public delegate void DelOnOpened();
}
