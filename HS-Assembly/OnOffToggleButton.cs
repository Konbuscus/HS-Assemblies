﻿// Decompiled with JetBrains decompiler
// Type: OnOffToggleButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class OnOffToggleButton : CheckBox
{
  public string m_onLabel = "GLOBAL_ON";
  public string m_offLabel = "GLOBAL_OFF";

  public override void SetChecked(bool isChecked)
  {
    base.SetChecked(isChecked);
    this.SetButtonText(GameStrings.Get(!isChecked ? this.m_offLabel : this.m_onLabel));
  }
}
