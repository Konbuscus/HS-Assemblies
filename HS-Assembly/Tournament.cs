﻿// Decompiled with JetBrains decompiler
// Type: Tournament
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Tournament
{
  private static Tournament s_instance;

  public static void Init()
  {
    if (Tournament.s_instance != null)
      return;
    Tournament.s_instance = new Tournament();
  }

  public static Tournament Get()
  {
    if (Tournament.s_instance == null)
      Debug.LogError((object) "Trying to retrieve the Tournament without calling Tournament.Init()!");
    return Tournament.s_instance;
  }

  public void NotifyOfBoxTransitionStart()
  {
    Box.Get().AddTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
  }

  public void OnBoxTransitionFinished(object userData)
  {
    Box.Get().RemoveTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    if (Options.Get().GetBool(Option.HAS_SEEN_TOURNAMENT, false))
      return;
    Options.Get().SetBool(Option.HAS_SEEN_TOURNAMENT, true);
  }
}
