﻿// Decompiled with JetBrains decompiler
// Type: ShakeObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ShakeObject : MonoBehaviour
{
  public float amount = 1f;
  private Vector3 orgPos;

  private void Start()
  {
    this.orgPos = this.transform.position;
  }

  private void Update()
  {
    this.transform.position = this.orgPos + new Vector3(Random.value * this.amount * this.amount, Random.value * this.amount * this.amount, Random.value * this.amount * this.amount);
  }
}
