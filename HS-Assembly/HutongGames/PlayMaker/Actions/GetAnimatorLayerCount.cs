﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorLayerCount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Returns the Animator controller layer count")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorLayerCount : FsmStateAction
  {
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The Target. An Animator component is required")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The Animator controller layer count")]
    [ActionSection("Results")]
    public FsmInt layerCount;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.layerCount = (FsmInt) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoGetLayerCount();
          this.Finish();
        }
      }
    }

    private void DoGetLayerCount()
    {
      if ((Object) this._animator == (Object) null)
        return;
      this.layerCount.Value = this._animator.layerCount;
    }
  }
}
