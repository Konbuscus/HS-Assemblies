﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Returns true if the specified layer is in a transition. Can also send events")]
  public class GetAnimatorIsLayerInTransition : FsmStateActionAnimatorBase
  {
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The target. An Animator component is required")]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The layer's index")]
    public FsmInt layerIndex;
    [ActionSection("Results")]
    [UIHint(UIHint.Variable)]
    [Tooltip("True if automatic matching is active")]
    public FsmBool isInTransition;
    [Tooltip("Event send if automatic matching is active")]
    public FsmEvent isInTransitionEvent;
    [Tooltip("Event send if automatic matching is not active")]
    public FsmEvent isNotInTransitionEvent;
    private Animator _animator;

    public override void Reset()
    {
      base.Reset();
      this.gameObject = (FsmOwnerDefault) null;
      this.isInTransition = (FsmBool) null;
      this.isInTransitionEvent = (FsmEvent) null;
      this.isNotInTransitionEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoCheckIsInTransition();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnActionUpdate()
    {
      this.DoCheckIsInTransition();
    }

    private void DoCheckIsInTransition()
    {
      if ((Object) this._animator == (Object) null)
        return;
      bool flag = this._animator.IsInTransition(this.layerIndex.Value);
      if (!this.isInTransition.IsNone)
        this.isInTransition.Value = flag;
      if (flag)
        this.Fsm.Event(this.isInTransitionEvent);
      else
        this.Fsm.Event(this.isNotInTransitionEvent);
    }
  }
}
