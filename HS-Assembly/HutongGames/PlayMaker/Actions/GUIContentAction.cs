﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUIContentAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("GUI base action - don't use!")]
  public abstract class GUIContentAction : GUIAction
  {
    public FsmTexture image;
    public FsmString text;
    public FsmString tooltip;
    public FsmString style;
    internal GUIContent content;

    public override void Reset()
    {
      base.Reset();
      this.image = (FsmTexture) null;
      this.text = (FsmString) string.Empty;
      this.tooltip = (FsmString) string.Empty;
      this.style = (FsmString) string.Empty;
    }

    public override void OnGUI()
    {
      base.OnGUI();
      this.content = new GUIContent(this.text.Value, this.image.Value, this.tooltip.Value);
    }
  }
}
