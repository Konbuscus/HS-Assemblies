﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get the right foot bottom height.")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorRightFootBottomHeight : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The Target. An Animator component is required")]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The right foot bottom height.")]
    [UIHint(UIHint.Variable)]
    [ActionSection("Result")]
    public FsmFloat rightFootHeight;
    [Tooltip("Repeat every frame during LateUpdate. Useful when value is subject to change over time.")]
    public bool everyFrame;
    private Animator _animator;

    public override void Reset()
    {
      base.Reset();
      this.gameObject = (FsmOwnerDefault) null;
      this.rightFootHeight = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this._getRightFootBottonHeight();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnLateUpdate()
    {
      this._getRightFootBottonHeight();
    }

    private void _getRightFootBottonHeight()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      this.rightFootHeight.Value = this._animator.rightFeetBottomHeight;
    }
  }
}
