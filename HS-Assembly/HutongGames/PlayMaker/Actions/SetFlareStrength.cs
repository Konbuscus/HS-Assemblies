﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetFlareStrength
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.RenderSettings)]
  [Tooltip("Sets the intensity of all Flares in the scene.")]
  public class SetFlareStrength : FsmStateAction
  {
    [RequiredField]
    public FsmFloat flareStrength;
    public bool everyFrame;

    public override void Reset()
    {
      this.flareStrength = (FsmFloat) 0.2f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetFlareStrength();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetFlareStrength();
    }

    private void DoSetFlareStrength()
    {
      RenderSettings.flareStrength = this.flareStrength.Value;
    }
  }
}
