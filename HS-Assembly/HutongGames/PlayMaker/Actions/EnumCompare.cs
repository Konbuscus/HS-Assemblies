﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.EnumCompare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Compares 2 Enum values and sends Events based on the result.")]
  [ActionCategory(ActionCategory.Logic)]
  public class EnumCompare : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmEnum enumVariable;
    [MatchFieldType("enumVariable")]
    public FsmEnum compareTo;
    public FsmEvent equalEvent;
    public FsmEvent notEqualEvent;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the true/false result in a bool variable.")]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame. Useful if the enum is changing over time.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.enumVariable = (FsmEnum) null;
      this.compareTo = (FsmEnum) null;
      this.equalEvent = (FsmEvent) null;
      this.notEqualEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoEnumCompare();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoEnumCompare();
    }

    private void DoEnumCompare()
    {
      if (this.enumVariable == null || this.compareTo == null)
        return;
      bool flag = object.Equals((object) this.enumVariable.get_Value(), (object) this.compareTo.get_Value());
      if (this.storeResult != null)
        this.storeResult.Value = flag;
      if (flag && this.equalEvent != null)
      {
        this.Fsm.Event(this.equalEvent);
      }
      else
      {
        if (flag || this.notEqualEvent == null)
          return;
        this.Fsm.Event(this.notEqualEvent);
      }
    }
  }
}
