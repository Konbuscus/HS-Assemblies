﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.iTweenStop
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("iTween")]
  [Tooltip("Stop an iTween action.")]
  public class iTweenStop : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault gameObject;
    public FsmString id;
    public iTweenFSMType iTweenType;
    public bool includeChildren;
    public bool inScene;

    public override void Reset()
    {
      FsmString fsmString = new FsmString();
      fsmString.UseVariable = true;
      this.id = fsmString;
      this.iTweenType = iTweenFSMType.all;
      this.includeChildren = false;
      this.inScene = false;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      this.DoiTween();
      this.Finish();
    }

    private void DoiTween()
    {
      if (this.id.IsNone)
      {
        if (this.iTweenType == iTweenFSMType.all)
          iTween.Stop();
        else if (this.inScene)
        {
          iTween.Stop(Enum.GetName(typeof (iTweenFSMType), (object) this.iTweenType));
        }
        else
        {
          GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
          if ((UnityEngine.Object) ownerDefaultTarget == (UnityEngine.Object) null)
            return;
          iTween.Stop(ownerDefaultTarget, Enum.GetName(typeof (iTweenFSMType), (object) this.iTweenType), this.includeChildren);
        }
      }
      else
        iTween.StopByName(this.id.Value);
    }
  }
}
