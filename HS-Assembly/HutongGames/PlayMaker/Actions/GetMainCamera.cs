﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMainCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the GameObject tagged MainCamera from the scene")]
  [ActionCategory(ActionCategory.Camera)]
  [ActionTarget(typeof (Camera), "storeGameObject", false)]
  public class GetMainCamera : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmGameObject storeGameObject;

    public override void Reset()
    {
      this.storeGameObject = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      this.storeGameObject.Value = !((Object) Camera.main != (Object) null) ? (GameObject) null : Camera.main.gameObject;
      this.Finish();
    }
  }
}
