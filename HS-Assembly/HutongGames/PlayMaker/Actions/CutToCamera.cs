﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.CutToCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Activates a Camera in the scene.")]
  [ActionCategory(ActionCategory.Camera)]
  public class CutToCamera : FsmStateAction
  {
    [RequiredField]
    public Camera camera;
    public bool makeMainCamera;
    public bool cutBackOnExit;
    private Camera oldCamera;

    public override void Reset()
    {
      this.camera = (Camera) null;
      this.makeMainCamera = true;
      this.cutBackOnExit = false;
    }

    public override void OnEnter()
    {
      if ((Object) this.camera == (Object) null)
      {
        this.LogError("Missing camera!");
      }
      else
      {
        this.oldCamera = Camera.main;
        CutToCamera.SwitchCamera(Camera.main, this.camera);
        if (this.makeMainCamera)
          this.camera.tag = "MainCamera";
        this.Finish();
      }
    }

    public override void OnExit()
    {
      if (!this.cutBackOnExit)
        return;
      CutToCamera.SwitchCamera(this.camera, this.oldCamera);
    }

    private static void SwitchCamera(Camera camera1, Camera camera2)
    {
      if ((Object) camera1 != (Object) null)
        camera1.enabled = false;
      if (!((Object) camera2 != (Object) null))
        return;
      camera2.enabled = true;
    }
  }
}
