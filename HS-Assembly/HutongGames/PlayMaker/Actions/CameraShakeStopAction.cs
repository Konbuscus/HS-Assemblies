﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.CameraShakeStopAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Stops a shaking camera, over time or immediately.")]
  [ActionCategory("Pegasus")]
  public class CameraShakeStopAction : CameraAction
  {
    public CameraAction.WhichCamera m_WhichCamera;
    [CheckForComponent(typeof (Camera))]
    public FsmGameObject m_SpecificCamera;
    public FsmString m_NamedCamera;
    public FsmFloat m_Time;
    public FsmFloat m_Delay;
    public FsmEvent m_FinishedEvent;
    private float m_timerSec;
    private bool m_shakeStopped;

    public override void Reset()
    {
      this.m_WhichCamera = CameraAction.WhichCamera.MAIN;
      FsmGameObject fsmGameObject = new FsmGameObject();
      fsmGameObject.UseVariable = true;
      this.m_SpecificCamera = fsmGameObject;
      FsmString fsmString = new FsmString();
      fsmString.UseVariable = false;
      this.m_NamedCamera = fsmString;
      this.m_Time = (FsmFloat) 0.0f;
      this.m_Delay = (FsmFloat) 0.0f;
      this.m_FinishedEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      this.m_timerSec = 0.0f;
      this.m_shakeStopped = false;
    }

    public override void OnUpdate()
    {
      Camera camera = this.GetCamera(this.m_WhichCamera, this.m_SpecificCamera, this.m_NamedCamera);
      if (!(bool) ((Object) camera))
      {
        Error.AddDevFatal("CameraShakeStopAction.OnUpdate() - Failed to get a camera. Owner={0}", (object) this.Owner);
        this.Finish();
      }
      this.m_timerSec += Time.deltaTime;
      if ((double) this.m_timerSec < (!this.m_Delay.IsNone ? (double) this.m_Delay.Value : 0.0))
        return;
      if (!this.m_shakeStopped)
        this.StopShake(camera);
      if (CameraShakeMgr.IsShaking(camera))
        return;
      this.Fsm.Event(this.m_FinishedEvent);
      this.Finish();
    }

    private void StopShake(Camera camera)
    {
      float time = !this.m_Time.IsNone ? this.m_Time.Value : 0.0f;
      CameraShakeMgr.Stop(camera, time);
      this.m_shakeStopped = true;
    }
  }
}
