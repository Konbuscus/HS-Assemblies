﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetSpeed2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the 2d Speed of a Game Object and stores it in a Float Variable. NOTE: The Game Object must have a rigid body 2D.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class GetSpeed2d : ComponentAction<Rigidbody2D>
  {
    [Tooltip("The GameObject with the Rigidbody2D attached")]
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody2D))]
    public FsmOwnerDefault gameObject;
    [Tooltip("The speed, or in technical terms: velocity magnitude")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.storeResult = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetSpeed();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetSpeed();
    }

    private void DoGetSpeed()
    {
      if (this.storeResult.IsNone || !this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.storeResult.Value = this.rigidbody2d.velocity.magnitude;
    }
  }
}
