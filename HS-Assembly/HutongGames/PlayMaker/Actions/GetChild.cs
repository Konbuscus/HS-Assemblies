﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetChild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Finds the Child of a GameObject by Name and/or Tag. Use this to find attach points etc. NOTE: This action will search recursively through all children and return the first match; To find a specific child use Find Child.")]
  public class GetChild : FsmStateAction
  {
    [Tooltip("The GameObject to search.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("The name of the child to search for.")]
    public FsmString childName;
    [UIHint(UIHint.Tag)]
    [Tooltip("The Tag to search for. If Child Name is set, both name and Tag need to match.")]
    public FsmString withTag;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result in a GameObject variable.")]
    [RequiredField]
    public FsmGameObject storeResult;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.childName = (FsmString) string.Empty;
      this.withTag = (FsmString) "Untagged";
      this.storeResult = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      this.storeResult.Value = GetChild.DoGetChildByName(this.Fsm.GetOwnerDefaultTarget(this.gameObject), this.childName.Value, this.withTag.Value);
      this.Finish();
    }

    private static GameObject DoGetChildByName(GameObject root, string name, string tag)
    {
      if ((UnityEngine.Object) root == (UnityEngine.Object) null)
        return (GameObject) null;
      IEnumerator enumerator = root.transform.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          Transform current = (Transform) enumerator.Current;
          if (!string.IsNullOrEmpty(name))
          {
            if (current.name == name)
            {
              if (string.IsNullOrEmpty(tag))
                return current.gameObject;
              if (current.tag.Equals(tag))
                return current.gameObject;
            }
          }
          else if (!string.IsNullOrEmpty(tag) && current.tag == tag)
            return current.gameObject;
          GameObject childByName = GetChild.DoGetChildByName(current.gameObject, name, tag);
          if ((UnityEngine.Object) childByName != (UnityEngine.Object) null)
            return childByName;
        }
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
      return (GameObject) null;
    }

    public override string ErrorCheck()
    {
      if (string.IsNullOrEmpty(this.childName.Value) && string.IsNullOrEmpty(this.withTag.Value))
        return "Specify Child Name, Tag, or both.";
      return (string) null;
    }
  }
}
