﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellGetCardAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Put a Spell's Source or Target Card into a GameObject variable.")]
  public class SpellGetCardAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    public SpellAction.Which m_WhichCard;
    public FsmGameObject m_GameObject;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      this.m_SpellObject = (FsmOwnerDefault) null;
      this.m_WhichCard = SpellAction.Which.SOURCE;
      this.m_GameObject = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      Card card = this.GetCard(this.m_WhichCard);
      if ((Object) card == (Object) null)
      {
        Error.AddDevFatal("SpellGetCardAction.OnEnter() - Card not found!");
        this.Finish();
      }
      else
      {
        if (!this.m_GameObject.IsNone)
          this.m_GameObject.Value = card.gameObject;
        this.Finish();
      }
    }
  }
}
