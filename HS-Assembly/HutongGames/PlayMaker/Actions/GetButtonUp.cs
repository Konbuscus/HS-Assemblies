﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetButtonUp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sends an Event when a Button is released.")]
  [ActionCategory(ActionCategory.Input)]
  public class GetButtonUp : FsmStateAction
  {
    [Tooltip("The name of the button. Set in the Unity Input Manager.")]
    [RequiredField]
    public FsmString buttonName;
    [Tooltip("Event to send if the button is released.")]
    public FsmEvent sendEvent;
    [Tooltip("Set to True if the button is released.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;

    public override void Reset()
    {
      this.buttonName = (FsmString) "Fire1";
      this.sendEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
    }

    public override void OnUpdate()
    {
      bool buttonUp = Input.GetButtonUp(this.buttonName.Value);
      if (buttonUp)
        this.Fsm.Event(this.sendEvent);
      this.storeResult.Value = buttonUp;
    }
  }
}
