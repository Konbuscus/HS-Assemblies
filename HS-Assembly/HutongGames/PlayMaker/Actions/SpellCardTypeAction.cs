﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellCardTypeAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Send an event based on a Spell's Card's Type.")]
  public class SpellCardTypeAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    public SpellAction.Which m_WhichCard;
    public FsmEvent m_MinionEvent;
    public FsmEvent m_HeroEvent;
    public FsmEvent m_HeroPowerEvent;
    public FsmEvent m_WeaponEvent;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      this.m_SpellObject = (FsmOwnerDefault) null;
      this.m_WhichCard = SpellAction.Which.SOURCE;
      this.m_MinionEvent = (FsmEvent) null;
      this.m_HeroEvent = (FsmEvent) null;
      this.m_HeroPowerEvent = (FsmEvent) null;
      this.m_WeaponEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      Card card = this.GetCard(this.m_WhichCard);
      if ((Object) card == (Object) null)
      {
        Error.AddDevFatal("SpellCardTypeAction.OnEnter() - Card not found!");
        this.Finish();
      }
      else
      {
        TAG_CARDTYPE cardType = card.GetEntity().GetCardType();
        switch (cardType)
        {
          case TAG_CARDTYPE.HERO:
            this.Fsm.Event(this.m_HeroEvent);
            break;
          case TAG_CARDTYPE.MINION:
            this.Fsm.Event(this.m_MinionEvent);
            break;
          case TAG_CARDTYPE.WEAPON:
            this.Fsm.Event(this.m_WeaponEvent);
            break;
          case TAG_CARDTYPE.HERO_POWER:
            this.Fsm.Event(this.m_HeroPowerEvent);
            break;
          default:
            Error.AddDevFatal("SpellCardTypeAction.OnEnter() - unknown type {0} on {1}", (object) cardType, (object) card);
            break;
        }
        this.Finish();
      }
    }
  }
}
