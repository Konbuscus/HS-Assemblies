﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ScaleTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Scales time: 1 = normal, 0.5 = half speed, 2 = double speed.")]
  [ActionCategory(ActionCategory.Time)]
  public class ScaleTime : FsmStateAction
  {
    [HasFloatSlider(0.0f, 4f)]
    [Tooltip("Scales time: 1 = normal, 0.5 = half speed, 2 = double speed.")]
    [RequiredField]
    public FsmFloat timeScale;
    [Tooltip("Adjust the fixed physics time step to match the time scale.")]
    public FsmBool adjustFixedDeltaTime;
    [Tooltip("Repeat every frame. Useful when animating the value.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.timeScale = (FsmFloat) 1f;
      this.adjustFixedDeltaTime = (FsmBool) true;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoTimeScale();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoTimeScale();
    }

    private void DoTimeScale()
    {
      Time.timeScale = this.timeScale.Value;
      Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }
  }
}
