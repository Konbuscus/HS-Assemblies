﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellGetLocationAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Get position from the bones in the board")]
  public class SpellGetLocationAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    [Tooltip("Choose a location from the spell to get the position from")]
    public SpellLocation m_Location;
    [Tooltip("Choose a bone, usually used with board location")]
    public FsmString m_Bone;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the position")]
    public FsmVector3 m_StorePosition;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_spell == (Object) null)
        return;
      GameObject spellLocationObject = SpellUtils.GetSpellLocationObject(this.m_spell, this.m_Location, this.m_Bone.Value);
      if ((Object) spellLocationObject != (Object) null)
        this.m_StorePosition.Value = spellLocationObject.transform.position;
      this.Finish();
    }
  }
}
