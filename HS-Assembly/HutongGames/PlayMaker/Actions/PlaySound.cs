﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PlaySound
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Audio)]
  [Tooltip("Plays an Audio Clip at a position defined by a Game Object or Vector3. If a position is defined, it takes priority over the game object. This action doesn't require an Audio Source component, but offers less control than Audio actions.")]
  public class PlaySound : FsmStateAction
  {
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat volume = (FsmFloat) 1f;
    public FsmOwnerDefault gameObject;
    public FsmVector3 position;
    [Title("Audio Clip")]
    [RequiredField]
    [ObjectType(typeof (AudioClip))]
    public FsmObject clip;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.position = fsmVector3;
      this.clip = (FsmObject) null;
      this.volume = (FsmFloat) 1f;
    }

    public override void OnEnter()
    {
      this.DoPlaySound();
      this.Finish();
    }

    private void DoPlaySound()
    {
      AudioClip clip = this.clip.Value as AudioClip;
      if ((Object) clip == (Object) null)
        this.LogWarning("Missing Audio Clip!");
      else if (!this.position.IsNone)
      {
        AudioSource.PlayClipAtPoint(clip, this.position.Value, this.volume.Value);
      }
      else
      {
        GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
        if ((Object) ownerDefaultTarget == (Object) null)
          return;
        AudioSource.PlayClipAtPoint(clip, ownerDefaultTarget.transform.position, this.volume.Value);
      }
    }
  }
}
