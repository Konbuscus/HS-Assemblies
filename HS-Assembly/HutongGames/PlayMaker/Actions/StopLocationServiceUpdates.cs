﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.StopLocationServiceUpdates
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Stops location service updates. This could be useful for saving battery life.")]
  [ActionCategory(ActionCategory.Device)]
  public class StopLocationServiceUpdates : FsmStateAction
  {
    public override void Reset()
    {
    }

    public override void OnEnter()
    {
      this.Finish();
    }
  }
}
