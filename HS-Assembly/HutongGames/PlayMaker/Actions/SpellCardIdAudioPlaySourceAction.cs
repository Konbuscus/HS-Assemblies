﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellCardIdAudioPlaySourceAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Plays an audio source dependent on a Spell's Card's ID.")]
  [ActionCategory("Pegasus Audio")]
  public class SpellCardIdAudioPlaySourceAction : SpellCardIdAudioAction
  {
    public FsmOwnerDefault m_SpellObject;
    [Tooltip("Which Card to check on the Spell.")]
    public SpellAction.Which m_WhichCard;
    [CompoundArray("Sources", "Card Id", "Source")]
    [RequiredField]
    public string[] m_CardIds;
    [CheckForComponent(typeof (AudioSource))]
    public FsmGameObject[] m_Sources;
    [CheckForComponent(typeof (AudioSource))]
    public FsmGameObject m_DefaultSource;
    [Tooltip("Optional. The source that gets picked will be put into this variable so you can track it.")]
    [CheckForComponent(typeof (AudioSource))]
    public FsmGameObject m_PickedSource;
    [Tooltip("Scales the volume of the AudioSource just for this Play call.")]
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_VolumeScale;
    [Tooltip("Wait for the Audio Source to finish playing before moving on.")]
    public FsmBool m_WaitForFinish;
    private AudioSource m_source;
    private float? m_originalVolume;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      this.m_SpellObject = (FsmOwnerDefault) null;
      this.m_WhichCard = SpellAction.Which.SOURCE;
      this.m_CardIds = new string[2];
      this.m_Sources = new FsmGameObject[2];
      this.m_DefaultSource = (FsmGameObject) null;
      FsmGameObject fsmGameObject = new FsmGameObject();
      fsmGameObject.UseVariable = true;
      this.m_PickedSource = fsmGameObject;
      this.m_VolumeScale = (FsmFloat) 1f;
      this.m_WaitForFinish = (FsmBool) false;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      this.m_source = this.GetSourceMatchingCardId(this.m_WhichCard, this.m_CardIds, this.m_Sources, this.m_DefaultSource);
      if ((Object) this.m_source == (Object) null)
      {
        this.Finish();
      }
      else
      {
        if (!this.m_PickedSource.IsNone)
          this.m_PickedSource.Value = this.m_source.gameObject;
        if (!this.m_VolumeScale.IsNone)
        {
          SoundManager.Get().SetVolume(this.m_source, this.m_VolumeScale.Value);
          this.m_originalVolume = new float?(this.m_VolumeScale.Value);
        }
        SoundManager.Get().Play(this.m_source, true);
        if (this.m_WaitForFinish.Value && SoundManager.Get().IsActive(this.m_source))
          return;
        this.Finish();
      }
    }

    public override void OnExit()
    {
      if (this.m_VolumeScale.IsNone || !this.m_originalVolume.HasValue)
        return;
      SoundManager.Get().SetVolume(this.m_source, this.m_originalVolume.Value);
    }

    public override void OnUpdate()
    {
      if (this.m_WaitForFinish.Value && SoundManager.Get().IsActive(this.m_source))
        return;
      this.Finish();
    }
  }
}
