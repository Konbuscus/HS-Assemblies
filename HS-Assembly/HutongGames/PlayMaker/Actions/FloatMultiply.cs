﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FloatMultiply
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Math)]
  [Tooltip("Multiplies one Float by another.")]
  public class FloatMultiply : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The float variable to multiply.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat floatVariable;
    [Tooltip("Multiply the float variable by this value.")]
    [RequiredField]
    public FsmFloat multiplyBy;
    [Tooltip("Repeat every frame. Useful if the variables are changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.floatVariable = (FsmFloat) null;
      this.multiplyBy = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.floatVariable.Value *= this.multiplyBy.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.floatVariable.Value *= this.multiplyBy.Value;
    }
  }
}
