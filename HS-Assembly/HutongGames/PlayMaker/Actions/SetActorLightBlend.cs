﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetActorLightBlend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Show or Hide an Actor without messing up the game.")]
  [ActionCategory("Pegasus")]
  public class SetActorLightBlend : FsmStateAction
  {
    public FsmGameObject m_ActorObject;
    [Tooltip("Light Blend Value")]
    public FsmFloat m_BlendValue;
    [Tooltip("Update Every Frame")]
    public bool m_EveryFrame;
    protected float m_initialLightBlendValue;
    protected Actor m_actor;

    public override void Reset()
    {
      this.m_ActorObject = (FsmGameObject) null;
      this.m_BlendValue = (FsmFloat) 1f;
      this.m_EveryFrame = false;
      this.m_actor = (Actor) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_actor == (Object) null)
        this.FindActor();
      this.m_actor.SetLightBlend(this.m_BlendValue.Value);
      if (this.m_EveryFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      if ((Object) this.m_actor == (Object) null)
        this.FindActor();
      if (!((Object) this.m_actor != (Object) null))
        return;
      this.m_actor.SetLightBlend(this.m_BlendValue.Value);
    }

    private void FindActor()
    {
      if (this.m_ActorObject.IsNone)
        return;
      GameObject start = this.m_ActorObject.Value;
      if (!((Object) start != (Object) null))
        return;
      this.m_actor = start.GetComponentInChildren<Actor>();
      if (!((Object) this.m_actor == (Object) null))
        return;
      this.m_actor = SceneUtils.FindComponentInThisOrParents<Actor>(start);
    }
  }
}
