﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetCosine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get the cosine. You can use degrees, simply check on the DegToRad conversion")]
  [ActionCategory(ActionCategory.Trigonometry)]
  public class GetCosine : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The angle. Note: You can use degrees, simply check DegtoRad if the angle is expressed in degrees.")]
    public FsmFloat angle;
    [Tooltip("Check on if the angle is expressed in degrees.")]
    public FsmBool DegToRad;
    [UIHint(UIHint.Variable)]
    [Tooltip("The angle cosinus")]
    [RequiredField]
    public FsmFloat result;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.angle = (FsmFloat) null;
      this.DegToRad = (FsmBool) true;
      this.everyFrame = false;
      this.result = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      this.DoCosine();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoCosine();
    }

    private void DoCosine()
    {
      float f = this.angle.Value;
      if (this.DegToRad.Value)
        f *= (float) Math.PI / 180f;
      this.result.Value = Mathf.Cos(f);
    }
  }
}
