﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActorRarityEventAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Send an event based on an Actor's Card's rarity.")]
  public class ActorRarityEventAction : ActorAction
  {
    public FsmOwnerDefault m_ActorObject;
    public FsmEvent m_FreeEvent;
    public FsmEvent m_CommonEvent;
    public FsmEvent m_RareEvent;
    public FsmEvent m_EpicEvent;
    public FsmEvent m_LegendaryEvent;

    protected override GameObject GetActorOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_ActorObject);
    }

    public override void Reset()
    {
      this.m_ActorObject = (FsmOwnerDefault) null;
      this.m_FreeEvent = (FsmEvent) null;
      this.m_CommonEvent = (FsmEvent) null;
      this.m_RareEvent = (FsmEvent) null;
      this.m_EpicEvent = (FsmEvent) null;
      this.m_LegendaryEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_actor == (Object) null)
      {
        this.Finish();
      }
      else
      {
        TAG_RARITY rarity = this.m_actor.GetRarity();
        switch (rarity)
        {
          case TAG_RARITY.COMMON:
            this.Fsm.Event(this.m_CommonEvent);
            break;
          case TAG_RARITY.FREE:
            this.Fsm.Event(this.m_FreeEvent);
            break;
          case TAG_RARITY.RARE:
            this.Fsm.Event(this.m_RareEvent);
            break;
          case TAG_RARITY.EPIC:
            this.Fsm.Event(this.m_EpicEvent);
            break;
          case TAG_RARITY.LEGENDARY:
            this.Fsm.Event(this.m_LegendaryEvent);
            break;
          default:
            Debug.LogError((object) string.Format("ActorRarityEventAction.OnEnter() - unknown rarity {0} on actor {1}", (object) rarity, (object) this.m_actor));
            break;
        }
        this.Finish();
      }
    }
  }
}
