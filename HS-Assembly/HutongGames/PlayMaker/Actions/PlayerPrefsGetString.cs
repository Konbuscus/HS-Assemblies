﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PlayerPrefsGetString
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("PlayerPrefs")]
  [Tooltip("Returns the value corresponding to key in the preference file if it exists.")]
  public class PlayerPrefsGetString : FsmStateAction
  {
    [Tooltip("Case sensitive key.")]
    [CompoundArray("Count", "Key", "Variable")]
    public FsmString[] keys;
    [UIHint(UIHint.Variable)]
    public FsmString[] variables;

    public override void Reset()
    {
      this.keys = new FsmString[1];
      this.variables = new FsmString[1];
    }

    public override void OnEnter()
    {
      for (int index = 0; index < this.keys.Length; ++index)
      {
        if (!this.keys[index].IsNone || !this.keys[index].Value.Equals(string.Empty))
          this.variables[index].Value = PlayerPrefs.GetString(this.keys[index].Value, !this.variables[index].IsNone ? this.variables[index].Value : string.Empty);
      }
      this.Finish();
    }
  }
}
