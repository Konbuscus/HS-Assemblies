﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.TouchEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sends events based on Touch Phases. Optionally filter by a fingerID.")]
  [ActionCategory(ActionCategory.Device)]
  public class TouchEvent : FsmStateAction
  {
    public FsmInt fingerId;
    public TouchPhase touchPhase;
    public FsmEvent sendEvent;
    [UIHint(UIHint.Variable)]
    public FsmInt storeFingerId;

    public override void Reset()
    {
      FsmInt fsmInt = new FsmInt();
      fsmInt.UseVariable = true;
      this.fingerId = fsmInt;
      this.storeFingerId = (FsmInt) null;
    }

    public override void OnUpdate()
    {
      if (Input.touchCount <= 0)
        return;
      foreach (Touch touch in Input.touches)
      {
        if ((this.fingerId.IsNone || touch.fingerId == this.fingerId.Value) && touch.phase == this.touchPhase)
        {
          this.storeFingerId.Value = touch.fingerId;
          this.Fsm.Event(this.sendEvent);
        }
      }
    }
  }
}
