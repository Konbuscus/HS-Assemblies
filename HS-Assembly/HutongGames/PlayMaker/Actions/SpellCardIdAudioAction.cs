﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellCardIdAudioAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("INTERNAL USE ONLY. Do not put this on your FSMs.")]
  public abstract class SpellCardIdAudioAction : SpellAction
  {
    protected AudioSource GetAudioSource(FsmOwnerDefault ownerDefault)
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(ownerDefault);
      if ((Object) ownerDefaultTarget == (Object) null)
        return (AudioSource) null;
      return ownerDefaultTarget.GetComponent<AudioSource>();
    }

    protected AudioClip GetClipMatchingCardId(SpellAction.Which whichCard, string[] cardIds, AudioClip[] clips, AudioClip defaultClip)
    {
      Card card = this.GetCard(whichCard);
      if ((Object) card == (Object) null)
      {
        Debug.LogWarningFormat("SpellCardIdAudioAction.GetClipMatchingCardId() - could not find {0} card", (object) whichCard);
        return (AudioClip) null;
      }
      int indexMatchingCardId = this.GetIndexMatchingCardId(card.GetEntity().GetCardId(), cardIds);
      if (indexMatchingCardId < 0)
        return defaultClip;
      return clips[indexMatchingCardId];
    }

    protected AudioSource GetSourceMatchingCardId(SpellAction.Which whichCard, string[] cardIds, FsmGameObject[] sources, FsmGameObject defaultSource)
    {
      Card card = this.GetCard(whichCard);
      if ((Object) card == (Object) null)
      {
        Debug.LogWarningFormat("SpellCardIdAudioAction.GetSourceMatchingCardId() - could not find {0} card", (object) whichCard);
        return (AudioSource) null;
      }
      int indexMatchingCardId = this.GetIndexMatchingCardId(card.GetEntity().GetCardId(), cardIds);
      if (indexMatchingCardId < 0)
        return defaultSource.Value.GetComponent<AudioSource>();
      return sources[indexMatchingCardId].Value.GetComponent<AudioSource>();
    }
  }
}
