﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUIBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("GUI Box.")]
  public class GUIBox : GUIContentAction
  {
    public override void OnGUI()
    {
      base.OnGUI();
      if (string.IsNullOrEmpty(this.style.Value))
        GUI.Box(this.rect, this.content);
      else
        GUI.Box(this.rect, this.content, (GUIStyle) this.style.Value);
    }
  }
}
