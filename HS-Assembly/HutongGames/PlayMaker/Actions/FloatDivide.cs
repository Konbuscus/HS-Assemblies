﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FloatDivide
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Divides one Float by another.")]
  [ActionCategory(ActionCategory.Math)]
  public class FloatDivide : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The float variable to divide.")]
    public FsmFloat floatVariable;
    [RequiredField]
    [Tooltip("Divide the float variable by this value.")]
    public FsmFloat divideBy;
    [Tooltip("Repeate every frame. Useful if the variables are changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.floatVariable = (FsmFloat) null;
      this.divideBy = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.floatVariable.Value /= this.divideBy.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.floatVariable.Value /= this.divideBy.Value;
    }
  }
}
