﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetTransform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets a Game Object's Transform and stores it in an Object Variable.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class GetTransform : FsmStateAction
  {
    [RequiredField]
    public FsmGameObject gameObject;
    [RequiredField]
    [ObjectType(typeof (Transform))]
    [UIHint(UIHint.Variable)]
    public FsmObject storeTransform;
    public bool everyFrame;

    public override void Reset()
    {
      FsmGameObject fsmGameObject = new FsmGameObject();
      fsmGameObject.UseVariable = true;
      this.gameObject = fsmGameObject;
      this.storeTransform = (FsmObject) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetGameObjectName();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetGameObjectName();
    }

    private void DoGetGameObjectName()
    {
      GameObject gameObject = this.gameObject.Value;
      this.storeTransform.Value = !((Object) gameObject != (Object) null) ? (Object) null : (Object) gameObject.transform;
    }
  }
}
