﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.CameraShakerAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Shakes a camera over time.")]
  [ActionCategory("Pegasus")]
  public class CameraShakerAction : CameraAction
  {
    public CameraAction.WhichCamera m_WhichCamera;
    [CheckForComponent(typeof (Camera))]
    public FsmGameObject m_SpecificCamera;
    public FsmString m_NamedCamera;
    public FsmVector3 m_Amount;
    [RequiredField]
    public FsmAnimationCurve m_IntensityCurve;
    public FsmFloat m_Delay;
    [Tooltip("[Optional] Hold the shake forever once the shake passes this time.")]
    public FsmFloat m_HoldAtTime;
    public FsmEvent m_FinishedEvent;
    private float m_timerSec;
    private bool m_shakeFired;

    public override void Reset()
    {
      this.m_WhichCamera = CameraAction.WhichCamera.MAIN;
      FsmGameObject fsmGameObject = new FsmGameObject();
      fsmGameObject.UseVariable = true;
      this.m_SpecificCamera = fsmGameObject;
      FsmString fsmString = new FsmString();
      fsmString.UseVariable = false;
      this.m_NamedCamera = fsmString;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = false;
      this.m_Amount = fsmVector3;
      this.m_IntensityCurve = (FsmAnimationCurve) null;
      this.m_Delay = (FsmFloat) 0.0f;
      FsmFloat fsmFloat = new FsmFloat();
      fsmFloat.UseVariable = true;
      this.m_HoldAtTime = fsmFloat;
      this.m_FinishedEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      this.m_timerSec = 0.0f;
      this.m_shakeFired = false;
    }

    public override void OnUpdate()
    {
      Camera camera = this.GetCamera(this.m_WhichCamera, this.m_SpecificCamera, this.m_NamedCamera);
      if (!(bool) ((Object) camera))
      {
        Error.AddDevFatal("CameraShakerAction.OnUpdate() - Failed to get a camera. Owner={0}", (object) this.Owner);
        this.Finish();
      }
      this.m_timerSec += Time.deltaTime;
      if ((double) this.m_timerSec < (!this.m_Delay.IsNone ? (double) this.m_Delay.Value : 0.0))
        return;
      if (!this.m_shakeFired)
      {
        if (this.m_IntensityCurve == null || this.m_IntensityCurve.curve == null)
        {
          this.Fsm.Event(this.m_FinishedEvent);
          this.Finish();
          return;
        }
        this.Shake(camera);
      }
      if (CameraShakeMgr.IsShaking(camera))
        return;
      this.Fsm.Event(this.m_FinishedEvent);
      this.Finish();
    }

    private void Shake(Camera camera)
    {
      Vector3 amount = !this.m_Amount.IsNone ? this.m_Amount.Value : Vector3.zero;
      AnimationCurve curve = this.m_IntensityCurve.curve;
      float? holdAtTime = new float?();
      if (!this.m_HoldAtTime.IsNone)
        holdAtTime = new float?(this.m_HoldAtTime.Value);
      CameraShakeMgr.Shake(camera, amount, curve, holdAtTime);
      this.m_shakeFired = true;
    }
  }
}
