﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActorEliteEventAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Send an event based on an Actor's Card's elite flag.")]
  [ActionCategory("Pegasus")]
  public class ActorEliteEventAction : ActorAction
  {
    public FsmOwnerDefault m_ActorObject;
    public FsmEvent m_EliteEvent;
    public FsmEvent m_NonEliteEvent;

    protected override GameObject GetActorOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_ActorObject);
    }

    public override void Reset()
    {
      this.m_ActorObject = (FsmOwnerDefault) null;
      this.m_EliteEvent = (FsmEvent) null;
      this.m_NonEliteEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_actor == (Object) null)
      {
        this.Finish();
      }
      else
      {
        if (this.m_actor.IsElite())
          this.Fsm.Event(this.m_EliteEvent);
        else
          this.Fsm.Event(this.m_NonEliteEvent);
        this.Finish();
      }
    }
  }
}
