﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGUITextureColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the Color of the GUITexture attached to a Game Object.")]
  [ActionCategory(ActionCategory.GUIElement)]
  public class SetGUITextureColor : ComponentAction<GUITexture>
  {
    [RequiredField]
    [CheckForComponent(typeof (GUITexture))]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    public FsmColor color;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.color = (FsmColor) Color.white;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetGUITextureColor();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetGUITextureColor();
    }

    private void DoSetGUITextureColor()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.guiTexture.color = this.color.Value;
    }
  }
}
