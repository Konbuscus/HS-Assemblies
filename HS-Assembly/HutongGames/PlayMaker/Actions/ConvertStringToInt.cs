﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ConvertStringToInt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Converts an String value to an Int value.")]
  [ActionCategory(ActionCategory.Convert)]
  public class ConvertStringToInt : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The String variable to convert to an integer.")]
    public FsmString stringVariable;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result in an Int variable.")]
    public FsmInt intVariable;
    [Tooltip("Repeat every frame. Useful if the String variable is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.intVariable = (FsmInt) null;
      this.stringVariable = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoConvertStringToInt();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoConvertStringToInt();
    }

    private void DoConvertStringToInt()
    {
      this.intVariable.Value = int.Parse(this.stringVariable.Value);
    }
  }
}
