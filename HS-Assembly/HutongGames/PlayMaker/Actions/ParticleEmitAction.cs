﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ParticleEmitAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Emit particles in a Particle System immediately.\nIf the particle system is not playing it will start playing.")]
  [ActionCategory("Pegasus")]
  public class ParticleEmitAction : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault m_GameObject;
    [Tooltip("The number of particles to emit.")]
    public FsmInt m_Count;
    [Tooltip("Run this action on all child objects' Particle Systems.")]
    public FsmBool m_IncludeChildren;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_Count = (FsmInt) 10;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((UnityEngine.Object) ownerDefaultTarget == (UnityEngine.Object) null)
      {
        this.Finish();
      }
      else
      {
        if (this.m_IncludeChildren.Value)
          this.EmitParticlesRecurse(ownerDefaultTarget);
        else
          this.EmitParticles(ownerDefaultTarget);
        this.Finish();
      }
    }

    private void EmitParticles(GameObject go)
    {
      ParticleSystem component = go.GetComponent<ParticleSystem>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        Debug.LogWarning((object) string.Format("ParticleEmitAction.OnEnter() - GameObject {0} has no ParticleSystem component", (object) go));
      else
        component.Emit(this.m_Count.Value);
    }

    private void EmitParticlesRecurse(GameObject go)
    {
      ParticleSystem component = go.GetComponent<ParticleSystem>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.Emit(this.m_Count.Value);
      IEnumerator enumerator = go.transform.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
          this.EmitParticlesRecurse(((Component) enumerator.Current).gameObject);
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
    }
  }
}
