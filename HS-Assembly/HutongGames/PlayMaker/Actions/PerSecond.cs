﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PerSecond
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Time)]
  [Tooltip("Multiplies a Float by Time.deltaTime to use in frame-rate independent operations. E.g., 10 becomes 10 units per second.")]
  public class PerSecond : FsmStateAction
  {
    [RequiredField]
    public FsmFloat floatValue;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmFloat storeResult;
    public bool everyFrame;

    public override void Reset()
    {
      this.floatValue = (FsmFloat) null;
      this.storeResult = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoPerSecond();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoPerSecond();
    }

    private void DoPerSecond()
    {
      if (this.storeResult == null)
        return;
      this.storeResult.Value = this.floatValue.Value * Time.deltaTime;
    }
  }
}
