﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the pressed state of the specified Button and stores it in a Bool Variable. See Unity Input Manager docs.")]
  [ActionCategory(ActionCategory.Input)]
  public class GetButton : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The name of the button. Set in the Unity Input Manager.")]
    public FsmString buttonName;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result in a bool variable.")]
    [RequiredField]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.buttonName = (FsmString) "Fire1";
      this.storeResult = (FsmBool) null;
      this.everyFrame = true;
    }

    public override void OnEnter()
    {
      this.DoGetButton();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetButton();
    }

    private void DoGetButton()
    {
      this.storeResult.Value = Input.GetButton(this.buttonName.Value);
    }
  }
}
