﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.RectOverlaps
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if 2 Rects overlap.")]
  [ActionCategory(ActionCategory.Rect)]
  public class RectOverlaps : FsmStateAction
  {
    [RequiredField]
    [Tooltip("First Rectangle.")]
    public FsmRect rect1;
    [RequiredField]
    [Tooltip("Second Rectangle.")]
    public FsmRect rect2;
    [Tooltip("Event to send if the Rects overlap.")]
    public FsmEvent trueEvent;
    [Tooltip("Event to send if the Rects do not overlap.")]
    public FsmEvent falseEvent;
    [Tooltip("Store the result in a variable.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      FsmRect fsmRect1 = new FsmRect();
      fsmRect1.UseVariable = true;
      this.rect1 = fsmRect1;
      FsmRect fsmRect2 = new FsmRect();
      fsmRect2.UseVariable = true;
      this.rect2 = fsmRect2;
      this.storeResult = (FsmBool) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoRectOverlap();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoRectOverlap();
    }

    private void DoRectOverlap()
    {
      if (this.rect1.IsNone || this.rect2.IsNone)
        return;
      bool flag = RectOverlaps.Intersect(this.rect1.Value, this.rect2.Value);
      this.storeResult.Value = flag;
      this.Fsm.Event(!flag ? this.falseEvent : this.trueEvent);
    }

    public static bool Intersect(Rect a, Rect b)
    {
      RectOverlaps.FlipNegative(ref a);
      RectOverlaps.FlipNegative(ref b);
      bool flag1 = (double) a.xMin < (double) b.xMax;
      bool flag2 = (double) a.xMax > (double) b.xMin;
      bool flag3 = (double) a.yMin < (double) b.yMax;
      bool flag4 = (double) a.yMax > (double) b.yMin;
      if (flag1 && flag2 && flag3)
        return flag4;
      return false;
    }

    public static void FlipNegative(ref Rect r)
    {
      if ((double) r.width < 0.0)
        r.x -= (r.width *= -1f);
      if ((double) r.height >= 0.0)
        return;
      r.y -= (r.height *= -1f);
    }
  }
}
