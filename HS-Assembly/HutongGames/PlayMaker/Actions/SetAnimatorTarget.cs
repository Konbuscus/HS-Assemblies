﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAnimatorTarget
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Sets an AvatarTarget and a targetNormalizedTime for the current state")]
  public class SetAnimatorTarget : FsmStateAction
  {
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The target.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("The avatar target")]
    public AvatarTarget avatarTarget;
    [Tooltip("The current state Time that is queried")]
    public FsmFloat targetNormalizedTime;
    [Tooltip("Repeat every frame during OnAnimatorMove. Useful when changing over time.")]
    public bool everyFrame;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.avatarTarget = AvatarTarget.Body;
      this.targetNormalizedTime = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.SetTarget();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void DoAnimatorMove()
    {
      this.SetTarget();
    }

    private void SetTarget()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      this._animator.SetTarget(this.avatarTarget, this.targetNormalizedTime.Value);
    }
  }
}
