﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetCameraCullingMask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the Culling Mask used by the Camera.")]
  [ActionCategory(ActionCategory.Camera)]
  public class SetCameraCullingMask : ComponentAction<Camera>
  {
    [CheckForComponent(typeof (Camera))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Cull these layers.")]
    [UIHint(UIHint.Layer)]
    public FsmInt[] cullingMask;
    [Tooltip("Invert the mask, so you cull all layers except those defined above.")]
    public FsmBool invertMask;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.cullingMask = new FsmInt[0];
      this.invertMask = (FsmBool) false;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetCameraCullingMask();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetCameraCullingMask();
    }

    private void DoSetCameraCullingMask()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.camera.cullingMask = ActionHelpers.LayerArrayToLayerMask(this.cullingMask, this.invertMask.Value);
    }
  }
}
