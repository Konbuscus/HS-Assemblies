﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetVector2XY
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Vector2)]
  [Tooltip("Get the XY channels of a Vector2 Variable and store them in Float Variables.")]
  public class GetVector2XY : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("The vector2 source")]
    public FsmVector2 vector2Variable;
    [Tooltip("The x component")]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeX;
    [Tooltip("The y component")]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeY;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.vector2Variable = (FsmVector2) null;
      this.storeX = (FsmFloat) null;
      this.storeY = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetVector2XYZ();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetVector2XYZ();
    }

    private void DoGetVector2XYZ()
    {
      if (this.vector2Variable == null)
        return;
      if (this.storeX != null)
        this.storeX.Value = this.vector2Variable.Value.x;
      if (this.storeY == null)
        return;
      this.storeY.Value = this.vector2Variable.Value.y;
    }
  }
}
