﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellStateAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Handles communication between a Spell and the SpellStates in an FSM.")]
  [ActionCategory("Pegasus")]
  public class SpellStateAction : SpellAction
  {
    private bool m_stateDirty = true;
    public FsmOwnerDefault m_SpellObject;
    private SpellStateType m_spellStateType;
    private bool m_stateInvalid;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_spell == (Object) null)
        return;
      this.DiscoverSpellStateType();
      if (this.m_stateInvalid)
        return;
      this.m_spell.OnFsmStateStarted(this.State, this.m_spellStateType);
      this.Finish();
    }

    private void DiscoverSpellStateType()
    {
      if (!this.m_stateDirty)
        return;
      string name = this.State.Name;
      foreach (FsmTransition globalTransition in this.Fsm.GlobalTransitions)
      {
        if (name.Equals(globalTransition.ToState))
        {
          this.m_spellStateType = EnumUtils.GetEnum<SpellStateType>(globalTransition.EventName);
          this.m_stateDirty = false;
          return;
        }
      }
      this.m_stateInvalid = true;
    }
  }
}
