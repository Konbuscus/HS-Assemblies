﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ControllerSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Modify various character controller settings.\n'None' leaves the setting unchanged.")]
  [ActionCategory(ActionCategory.Character)]
  public class ControllerSettings : FsmStateAction
  {
    [CheckForComponent(typeof (CharacterController))]
    [RequiredField]
    [Tooltip("The GameObject that owns the CharacterController.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The height of the character's capsule.")]
    public FsmFloat height;
    [Tooltip("The radius of the character's capsule.")]
    public FsmFloat radius;
    [Tooltip("The character controllers slope limit in degrees.")]
    public FsmFloat slopeLimit;
    [Tooltip("The character controllers step offset in meters.")]
    public FsmFloat stepOffset;
    [Tooltip("The center of the character's capsule relative to the transform's position")]
    public FsmVector3 center;
    [Tooltip("Should other rigidbodies or character controllers collide with this character controller (By default always enabled).")]
    public FsmBool detectCollisions;
    [Tooltip("Repeat every frame while the state is active.")]
    public bool everyFrame;
    private GameObject previousGo;
    private CharacterController controller;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      FsmFloat fsmFloat1 = new FsmFloat();
      fsmFloat1.UseVariable = true;
      this.height = fsmFloat1;
      FsmFloat fsmFloat2 = new FsmFloat();
      fsmFloat2.UseVariable = true;
      this.radius = fsmFloat2;
      FsmFloat fsmFloat3 = new FsmFloat();
      fsmFloat3.UseVariable = true;
      this.slopeLimit = fsmFloat3;
      FsmFloat fsmFloat4 = new FsmFloat();
      fsmFloat4.UseVariable = true;
      this.stepOffset = fsmFloat4;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.center = fsmVector3;
      FsmBool fsmBool = new FsmBool();
      fsmBool.UseVariable = true;
      this.detectCollisions = fsmBool;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoControllerSettings();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoControllerSettings();
    }

    private void DoControllerSettings()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((Object) ownerDefaultTarget != (Object) this.previousGo)
      {
        this.controller = ownerDefaultTarget.GetComponent<CharacterController>();
        this.previousGo = ownerDefaultTarget;
      }
      if (!((Object) this.controller != (Object) null))
        return;
      if (!this.height.IsNone)
        this.controller.height = this.height.Value;
      if (!this.radius.IsNone)
        this.controller.radius = this.radius.Value;
      if (!this.slopeLimit.IsNone)
        this.controller.slopeLimit = this.slopeLimit.Value;
      if (!this.stepOffset.IsNone)
        this.controller.stepOffset = this.stepOffset.Value;
      if (!this.center.IsNone)
        this.controller.center = this.center.Value;
      if (this.detectCollisions.IsNone)
        return;
      this.controller.detectCollisions = this.detectCollisions.Value;
    }
  }
}
