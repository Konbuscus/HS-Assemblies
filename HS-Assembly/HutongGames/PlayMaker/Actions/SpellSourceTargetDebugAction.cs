﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellSourceTargetDebugAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("[DEBUG] Setup a Spell to go from a source to a target.")]
  [ActionCategory("Pegasus")]
  public class SpellSourceTargetDebugAction : SpellAction
  {
    public FsmGameObject m_SpellObject;
    public FsmGameObject m_SourceObject;
    public FsmGameObject m_TargetObject;

    protected override GameObject GetSpellOwner()
    {
      return this.m_SpellObject.Value;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_spell == (Object) null)
        return;
      this.m_spell.RemoveAllTargets();
      this.m_spell.SetSource(this.m_SourceObject.Value);
      if ((Object) this.m_TargetObject.Value != (Object) null)
        this.m_spell.AddTarget(this.m_TargetObject.Value);
      this.Finish();
    }
  }
}
