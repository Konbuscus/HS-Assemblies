﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetNextChild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Each time this action is called it gets the next child of a GameObject. This lets you quickly loop through all the children of an object to perform actions on them. NOTE: To find a specific child use Find Child.")]
  public class GetNextChild : FsmStateAction
  {
    [Tooltip("The parent GameObject. Note, if GameObject changes, this action will reset and start again at the first child.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Store the next child in a GameObject variable.")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmGameObject storeNextChild;
    [Tooltip("Event to send to get the next child.")]
    public FsmEvent loopEvent;
    [Tooltip("Event to send when there are no more children.")]
    public FsmEvent finishedEvent;
    private GameObject go;
    private int nextChildIndex;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.storeNextChild = (FsmGameObject) null;
      this.loopEvent = (FsmEvent) null;
      this.finishedEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      this.DoGetNextChild(this.Fsm.GetOwnerDefaultTarget(this.gameObject));
      this.Finish();
    }

    private void DoGetNextChild(GameObject parent)
    {
      if ((Object) parent == (Object) null)
        return;
      if ((Object) this.go != (Object) parent)
      {
        this.go = parent;
        this.nextChildIndex = 0;
      }
      if (this.nextChildIndex >= this.go.transform.childCount)
      {
        this.nextChildIndex = 0;
        this.Fsm.Event(this.finishedEvent);
      }
      else
      {
        this.storeNextChild.Value = parent.transform.GetChild(this.nextChildIndex).gameObject;
        if (this.nextChildIndex >= this.go.transform.childCount)
        {
          this.nextChildIndex = 0;
          this.Fsm.Event(this.finishedEvent);
        }
        else
        {
          ++this.nextChildIndex;
          if (this.loopEvent == null)
            return;
          this.Fsm.Event(this.loopEvent);
        }
      }
    }
  }
}
