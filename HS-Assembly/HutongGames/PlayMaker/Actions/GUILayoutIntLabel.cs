﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutIntLabel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("GUILayout Label for an Int Variable.")]
  [ActionCategory(ActionCategory.GUILayout)]
  public class GUILayoutIntLabel : GUILayoutAction
  {
    [Tooltip("Text to put before the int variable.")]
    public FsmString prefix;
    [RequiredField]
    [Tooltip("Int variable to display.")]
    [UIHint(UIHint.Variable)]
    public FsmInt intVariable;
    [Tooltip("Optional GUIStyle in the active GUISKin.")]
    public FsmString style;

    public override void Reset()
    {
      base.Reset();
      this.prefix = (FsmString) string.Empty;
      this.style = (FsmString) string.Empty;
      this.intVariable = (FsmInt) null;
    }

    public override void OnGUI()
    {
      if (string.IsNullOrEmpty(this.style.Value))
        GUILayout.Label(new GUIContent(this.prefix.Value + (object) this.intVariable.Value), this.LayoutOptions);
      else
        GUILayout.Label(new GUIContent(this.prefix.Value + (object) this.intVariable.Value), (GUIStyle) this.style.Value, this.LayoutOptions);
    }
  }
}
