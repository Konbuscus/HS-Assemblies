﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SendEventBasedOnEntityTagAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Fires an event based on the tag value of a specified entity")]
  [ActionCategory("Pegasus")]
  public class SendEventBasedOnEntityTagAction : SpellAction
  {
    public FsmOwnerDefault m_spellObject;
    public SendEventBasedOnEntityTagAction.ValueRangeEvent[] m_rangeEventArray;
    public SpellAction.Which m_whichEntity;
    public GAME_TAG m_tagToCheck;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_spellObject);
    }

    public override void OnEnter()
    {
      base.OnEnter();
      Entity entity = this.GetEntity(this.m_whichEntity);
      if (entity == null)
      {
        Log.All.PrintError("{0}.OnEnter() - FAILED to find relevant entity: \"{1}\"", new object[2]
        {
          (object) this,
          (object) this.m_whichEntity
        });
        this.Finish();
      }
      else
      {
        FsmEvent eventToSend = this.DetermineEventToSend(entity.GetTag(this.m_tagToCheck));
        if (eventToSend != null)
          this.Fsm.Event(eventToSend);
        else
          Log.All.PrintError("{0}.OnEnter() - FAILED to find any event that could be sent, did you set up ranges?", (object) this);
        this.Finish();
      }
    }

    private FsmEvent DetermineEventToSend(int tagValue)
    {
      SendEventBasedOnEntityTagAction.ValueRangeEvent accordingToRanges = SpellUtils.GetAppropriateElementAccordingToRanges<SendEventBasedOnEntityTagAction.ValueRangeEvent>(this.m_rangeEventArray, (Func<SendEventBasedOnEntityTagAction.ValueRangeEvent, ValueRange>) (x => x.m_range), tagValue);
      if (accordingToRanges != null)
        return accordingToRanges.m_event;
      return (FsmEvent) null;
    }

    public class ValueRangeEvent
    {
      public ValueRange m_range;
      public FsmEvent m_event;
    }
  }
}
