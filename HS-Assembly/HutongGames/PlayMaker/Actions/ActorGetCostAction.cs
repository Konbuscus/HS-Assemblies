﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActorGetCostAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Put an Actor's cost data into variables.")]
  [ActionCategory("Pegasus")]
  public class ActorGetCostAction : ActorAction
  {
    public FsmOwnerDefault m_ActorObject;
    public FsmGameObject m_ManaGem;
    public FsmGameObject m_UberText;
    public FsmInt m_Cost;

    protected override GameObject GetActorOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_ActorObject);
    }

    public override void Reset()
    {
      this.m_ActorObject = (FsmOwnerDefault) null;
      FsmGameObject fsmGameObject1 = new FsmGameObject();
      fsmGameObject1.UseVariable = true;
      this.m_ManaGem = fsmGameObject1;
      FsmGameObject fsmGameObject2 = new FsmGameObject();
      fsmGameObject2.UseVariable = true;
      this.m_UberText = fsmGameObject2;
      FsmInt fsmInt = new FsmInt();
      fsmInt.UseVariable = true;
      this.m_Cost = fsmInt;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_actor == (Object) null)
      {
        this.Finish();
      }
      else
      {
        if (!this.m_ManaGem.IsNone)
          this.m_ManaGem.Value = this.m_actor.m_manaObject;
        if (!this.m_UberText.IsNone)
          this.m_UberText.Value = this.m_actor.GetCostTextObject();
        if (!this.m_Cost.IsNone)
        {
          Entity entity = this.m_actor.GetEntity();
          if (entity != null)
          {
            this.m_Cost.Value = entity.GetCost();
          }
          else
          {
            EntityDef entityDef = this.m_actor.GetEntityDef();
            if (entityDef != null)
              this.m_Cost.Value = entityDef.GetCost();
          }
        }
        this.Finish();
      }
    }
  }
}
