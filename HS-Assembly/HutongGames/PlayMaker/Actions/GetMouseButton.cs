﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMouseButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the pressed state of the specified Mouse Button and stores it in a Bool Variable. See Unity Input Manager doc.")]
  [ActionCategory(ActionCategory.Input)]
  public class GetMouseButton : FsmStateAction
  {
    [Tooltip("The mouse button to test.")]
    [RequiredField]
    public MouseButton button;
    [Tooltip("Store the pressed state in a Bool Variable.")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;

    public override void Reset()
    {
      this.button = MouseButton.Left;
      this.storeResult = (FsmBool) null;
    }

    public override void OnEnter()
    {
      this.storeResult.Value = Input.GetMouseButton((int) this.button);
    }

    public override void OnUpdate()
    {
      this.storeResult.Value = Input.GetMouseButton((int) this.button);
    }
  }
}
