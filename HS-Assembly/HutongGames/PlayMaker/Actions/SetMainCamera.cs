﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetMainCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Camera)]
  [Tooltip("Sets the Main Camera.")]
  public class SetMainCamera : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (Camera))]
    [Tooltip("The GameObject to set as the main camera (should have a Camera component).")]
    public FsmGameObject gameObject;

    public override void Reset()
    {
      this.gameObject = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      if ((Object) this.gameObject.Value != (Object) null)
      {
        if ((Object) Camera.main != (Object) null)
          Camera.main.gameObject.tag = "Untagged";
        this.gameObject.Value.tag = "MainCamera";
      }
      this.Finish();
    }
  }
}
