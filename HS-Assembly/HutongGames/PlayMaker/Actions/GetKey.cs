﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the pressed state of a Key.")]
  [ActionCategory(ActionCategory.Input)]
  public class GetKey : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The key to test.")]
    public KeyCode key;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store if the key is down (True) or up (False).")]
    [RequiredField]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame. Useful if you're waiting for a key press/release.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.key = KeyCode.None;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetKey();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetKey();
    }

    private void DoGetKey()
    {
      this.storeResult.Value = Input.GetKey(this.key);
    }
  }
}
