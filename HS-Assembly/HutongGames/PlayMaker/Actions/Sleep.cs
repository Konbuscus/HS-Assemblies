﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Sleep
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Forces a Game Object's Rigid Body to Sleep at least one frame.")]
  public class Sleep : ComponentAction<Rigidbody>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody))]
    public FsmOwnerDefault gameObject;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      this.DoSleep();
      this.Finish();
    }

    private void DoSleep()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.rigidbody.Sleep();
    }
  }
}
