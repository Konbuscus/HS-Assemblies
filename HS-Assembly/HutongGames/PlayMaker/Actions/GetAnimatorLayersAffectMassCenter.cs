﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Returns if additionnal layers affects the mass center")]
  public class GetAnimatorLayersAffectMassCenter : FsmStateAction
  {
    [Tooltip("The Target. An Animator component is required")]
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [Tooltip("If true, additionnal layers affects the mass center")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [ActionSection("Results")]
    public FsmBool affectMassCenter;
    [Tooltip("Event send if additionnal layers affects the mass center")]
    public FsmEvent affectMassCenterEvent;
    [Tooltip("Event send if additionnal layers do no affects the mass center")]
    public FsmEvent doNotAffectMassCenterEvent;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.affectMassCenter = (FsmBool) null;
      this.affectMassCenterEvent = (FsmEvent) null;
      this.doNotAffectMassCenterEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.CheckAffectMassCenter();
          this.Finish();
        }
      }
    }

    private void CheckAffectMassCenter()
    {
      if ((Object) this._animator == (Object) null)
        return;
      bool affectMassCenter = this._animator.layersAffectMassCenter;
      this.affectMassCenter.Value = affectMassCenter;
      if (affectMassCenter)
        this.Fsm.Event(this.affectMassCenterEvent);
      else
        this.Fsm.Event(this.doNotAffectMassCenterEvent);
    }
  }
}
