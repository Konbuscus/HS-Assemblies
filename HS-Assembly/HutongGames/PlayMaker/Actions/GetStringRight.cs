﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetStringRight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the Right n characters from a String.")]
  [ActionCategory(ActionCategory.String)]
  public class GetStringRight : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmString stringVariable;
    [Tooltip("Number of characters to get.")]
    public FsmInt charCount;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmString storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.stringVariable = (FsmString) null;
      this.charCount = (FsmInt) 0;
      this.storeResult = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetStringRight();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetStringRight();
    }

    private void DoGetStringRight()
    {
      if (this.stringVariable.IsNone || this.storeResult.IsNone)
        return;
      string str = this.stringVariable.Value;
      int length = Mathf.Clamp(this.charCount.Value, 0, str.Length);
      this.storeResult.Value = str.Substring(str.Length - length, length);
    }
  }
}
