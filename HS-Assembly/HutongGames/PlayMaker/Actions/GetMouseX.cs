﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMouseX
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Input)]
  [Tooltip("Gets the X Position of the mouse and stores it in a Float Variable.")]
  public class GetMouseX : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmFloat storeResult;
    public bool normalize;

    public override void Reset()
    {
      this.storeResult = (FsmFloat) null;
      this.normalize = true;
    }

    public override void OnEnter()
    {
      this.DoGetMouseX();
    }

    public override void OnUpdate()
    {
      this.DoGetMouseX();
    }

    private void DoGetMouseX()
    {
      if (this.storeResult == null)
        return;
      float x = Input.mousePosition.x;
      if (this.normalize)
        x /= (float) Screen.width;
      this.storeResult.Value = x;
    }
  }
}
