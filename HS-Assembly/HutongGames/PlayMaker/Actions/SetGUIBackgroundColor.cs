﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGUIBackgroundColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("Sets the Tinting Color for all background elements rendered by the GUI. By default only effects GUI rendered by this FSM, check Apply Globally to effect all GUI controls.")]
  public class SetGUIBackgroundColor : FsmStateAction
  {
    [RequiredField]
    public FsmColor backgroundColor;
    public FsmBool applyGlobally;

    public override void Reset()
    {
      this.backgroundColor = (FsmColor) Color.white;
    }

    public override void OnGUI()
    {
      GUI.backgroundColor = this.backgroundColor.Value;
      if (!this.applyGlobally.Value)
        return;
      PlayMakerGUI.GUIBackgroundColor = GUI.backgroundColor;
    }
  }
}
