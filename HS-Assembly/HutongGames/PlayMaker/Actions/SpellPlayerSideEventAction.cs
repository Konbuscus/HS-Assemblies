﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellPlayerSideEventAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Send an event based on the player side of a Spell.")]
  [ActionCategory("Pegasus")]
  public class SpellPlayerSideEventAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    public SpellAction.Which m_WhichCard;
    public FsmEvent m_FriendlyEvent;
    public FsmEvent m_OpponentEvent;
    public FsmEvent m_NeutralEvent;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      this.m_SpellObject = (FsmOwnerDefault) null;
      this.m_WhichCard = SpellAction.Which.SOURCE;
      this.m_FriendlyEvent = (FsmEvent) null;
      this.m_OpponentEvent = (FsmEvent) null;
      this.m_NeutralEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      Card card = this.GetCard(this.m_WhichCard);
      if ((Object) card == (Object) null)
      {
        Error.AddDevFatal("SpellPlayerSideEventAction.OnEnter() - Card not found for spell {0}", (object) this.GetSpell());
        this.Finish();
      }
      else
      {
        switch (card.GetEntity().GetControllerSide())
        {
          case Player.Side.FRIENDLY:
            this.Fsm.Event(this.m_FriendlyEvent);
            break;
          case Player.Side.OPPOSING:
            this.Fsm.Event(this.m_OpponentEvent);
            break;
          default:
            this.Fsm.Event(this.m_NeutralEvent);
            break;
        }
        this.Finish();
      }
    }
  }
}
