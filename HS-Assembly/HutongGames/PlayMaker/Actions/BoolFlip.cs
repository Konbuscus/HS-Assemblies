﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.BoolFlip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Math)]
  [Tooltip("Flips the value of a Bool Variable.")]
  public class BoolFlip : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("Bool variable to flip.")]
    [RequiredField]
    public FsmBool boolVariable;

    public override void Reset()
    {
      this.boolVariable = (FsmBool) null;
    }

    public override void OnEnter()
    {
      this.boolVariable.Value = !this.boolVariable.Value;
      this.Finish();
    }
  }
}
