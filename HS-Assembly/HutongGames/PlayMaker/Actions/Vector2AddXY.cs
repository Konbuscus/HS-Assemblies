﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector2AddXY
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Vector2)]
  [Tooltip("Adds a XY values to Vector2 Variable.")]
  public class Vector2AddXY : FsmStateAction
  {
    [Tooltip("The vector2 target")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmVector2 vector2Variable;
    [Tooltip("The x component to add")]
    public FsmFloat addX;
    [Tooltip("The y component to add")]
    public FsmFloat addY;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;
    [Tooltip("Add the value on a per second bases.")]
    public bool perSecond;

    public override void Reset()
    {
      this.vector2Variable = (FsmVector2) null;
      this.addX = (FsmFloat) 0.0f;
      this.addY = (FsmFloat) 0.0f;
      this.everyFrame = false;
      this.perSecond = false;
    }

    public override void OnEnter()
    {
      this.DoVector2AddXYZ();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoVector2AddXYZ();
    }

    private void DoVector2AddXYZ()
    {
      Vector2 vector2 = new Vector2(this.addX.Value, this.addY.Value);
      if (this.perSecond)
        this.vector2Variable.Value += vector2 * Time.deltaTime;
      else
        this.vector2Variable.Value += vector2;
    }
  }
}
