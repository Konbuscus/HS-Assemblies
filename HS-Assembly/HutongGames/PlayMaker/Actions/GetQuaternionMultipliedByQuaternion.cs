﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Quaternion)]
  [Tooltip("Get the quaternion from a quaternion multiplied by a quaternion.")]
  public class GetQuaternionMultipliedByQuaternion : QuaternionBaseAction
  {
    [Tooltip("The first quaternion to multiply")]
    [RequiredField]
    public FsmQuaternion quaternionA;
    [RequiredField]
    [Tooltip("The second quaternion to multiply")]
    public FsmQuaternion quaternionB;
    [UIHint(UIHint.Variable)]
    [Tooltip("The resulting quaternion")]
    [RequiredField]
    public FsmQuaternion result;

    public override void Reset()
    {
      this.quaternionA = (FsmQuaternion) null;
      this.quaternionB = (FsmQuaternion) null;
      this.result = (FsmQuaternion) null;
      this.everyFrame = false;
      this.everyFrameOption = QuaternionBaseAction.everyFrameOptions.Update;
    }

    public override void OnEnter()
    {
      this.DoQuatMult();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.Update)
        return;
      this.DoQuatMult();
    }

    public override void OnLateUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.LateUpdate)
        return;
      this.DoQuatMult();
    }

    public override void OnFixedUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.FixedUpdate)
        return;
      this.DoQuatMult();
    }

    private void DoQuatMult()
    {
      this.result.Value = this.quaternionA.Value * this.quaternionB.Value;
    }
  }
}
