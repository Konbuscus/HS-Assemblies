﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PlatformEventAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Event based on current platform")]
  public class PlatformEventAction : FsmStateAction
  {
    public FsmEvent m_DefaultEvent;
    public FsmEvent m_PhoneEvent;

    public override void Reset()
    {
      this.m_PhoneEvent = (FsmEvent) null;
      this.m_DefaultEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      if ((bool) UniversalInputManager.UsePhoneUI && this.m_PhoneEvent != null)
        this.Fsm.Event(this.m_PhoneEvent);
      else
        this.Fsm.Event(this.m_DefaultEvent);
    }

    public override string ErrorCheck()
    {
      return string.Empty;
    }
  }
}
