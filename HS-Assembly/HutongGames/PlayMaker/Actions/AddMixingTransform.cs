﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AddMixingTransform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animation)]
  [Tooltip("Play an animation on a subset of the hierarchy. E.g., A waving animation on the upper body.")]
  public class AddMixingTransform : BaseAnimationAction
  {
    [CheckForComponent(typeof (Animation))]
    [Tooltip("The GameObject playing the animation.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The name of the animation to mix. NOTE: The animation should already be added to the Animation Component on the GameObject.")]
    public FsmString animationName;
    [RequiredField]
    [Tooltip("The mixing transform. E.g., root/upper_body/left_shoulder")]
    public FsmString transform;
    [Tooltip("If recursive is true all children of the mix transform will also be animated.")]
    public FsmBool recursive;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.animationName = (FsmString) string.Empty;
      this.transform = (FsmString) string.Empty;
      this.recursive = (FsmBool) true;
    }

    public override void OnEnter()
    {
      this.DoAddMixingTransform();
      this.Finish();
    }

    private void DoAddMixingTransform()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if (!this.UpdateCache(ownerDefaultTarget))
        return;
      AnimationState animationState = this.animation[this.animationName.Value];
      if ((TrackedReference) animationState == (TrackedReference) null)
        return;
      Transform mix = ownerDefaultTarget.transform.Find(this.transform.Value);
      animationState.AddMixingTransform(mix, this.recursive.Value);
    }
  }
}
