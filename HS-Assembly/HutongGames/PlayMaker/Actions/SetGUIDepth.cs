﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGUIDepth
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("Sets the sorting depth of subsequent GUI elements.")]
  public class SetGUIDepth : FsmStateAction
  {
    [RequiredField]
    public FsmInt depth;

    public override void Reset()
    {
      this.depth = (FsmInt) 0;
    }

    public override void OnPreprocess()
    {
      this.Fsm.HandleOnGUI = true;
    }

    public override void OnGUI()
    {
      GUI.depth = this.depth.Value;
    }
  }
}
