﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutFloatLabel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUILayout)]
  [Tooltip("GUILayout Label for a Float Variable.")]
  public class GUILayoutFloatLabel : GUILayoutAction
  {
    [Tooltip("Text to put before the float variable.")]
    public FsmString prefix;
    [UIHint(UIHint.Variable)]
    [Tooltip("Float variable to display.")]
    [RequiredField]
    public FsmFloat floatVariable;
    [Tooltip("Optional GUIStyle in the active GUISKin.")]
    public FsmString style;

    public override void Reset()
    {
      base.Reset();
      this.prefix = (FsmString) string.Empty;
      this.style = (FsmString) string.Empty;
      this.floatVariable = (FsmFloat) null;
    }

    public override void OnGUI()
    {
      if (string.IsNullOrEmpty(this.style.Value))
        GUILayout.Label(new GUIContent(this.prefix.Value + (object) this.floatVariable.Value), this.LayoutOptions);
      else
        GUILayout.Label(new GUIContent(this.prefix.Value + (object) this.floatVariable.Value), (GUIStyle) this.style.Value, this.LayoutOptions);
    }
  }
}
