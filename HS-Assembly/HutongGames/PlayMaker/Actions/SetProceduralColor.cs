﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetProceduralColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set a named color property in a Substance material. NOTE: Use Rebuild Textures after setting Substance properties.")]
  [ActionCategory("Substance")]
  public class SetProceduralColor : FsmStateAction
  {
    [RequiredField]
    public FsmMaterial substanceMaterial;
    [RequiredField]
    public FsmString colorProperty;
    [RequiredField]
    public FsmColor colorValue;
    [Tooltip("NOTE: Updating procedural materials every frame can be very slow!")]
    public bool everyFrame;

    public override void Reset()
    {
      this.substanceMaterial = (FsmMaterial) null;
      this.colorProperty = (FsmString) string.Empty;
      this.colorValue = (FsmColor) Color.white;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetProceduralFloat();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetProceduralFloat();
    }

    private void DoSetProceduralFloat()
    {
      ProceduralMaterial proceduralMaterial = this.substanceMaterial.Value as ProceduralMaterial;
      if ((Object) proceduralMaterial == (Object) null)
        this.LogError("Not a substance material!");
      else
        proceduralMaterial.SetProceduralColor(this.colorProperty.Value, this.colorValue.Value);
    }
  }
}
