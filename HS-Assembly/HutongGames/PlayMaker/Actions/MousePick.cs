﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.MousePick
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Perform a Mouse Pick on the scene and stores the results. Use Ray Distance to set how close the camera must be to pick the object.")]
  [ActionCategory(ActionCategory.Input)]
  public class MousePick : FsmStateAction
  {
    [RequiredField]
    public FsmFloat rayDistance = (FsmFloat) 100f;
    [UIHint(UIHint.Variable)]
    public FsmBool storeDidPickObject;
    [UIHint(UIHint.Variable)]
    public FsmGameObject storeGameObject;
    [UIHint(UIHint.Variable)]
    public FsmVector3 storePoint;
    [UIHint(UIHint.Variable)]
    public FsmVector3 storeNormal;
    [UIHint(UIHint.Variable)]
    public FsmFloat storeDistance;
    [Tooltip("Pick only from these layers.")]
    [UIHint(UIHint.Layer)]
    public FsmInt[] layerMask;
    [Tooltip("Invert the mask, so you pick from all layers except those defined above.")]
    public FsmBool invertMask;
    public bool everyFrame;

    public override void Reset()
    {
      this.rayDistance = (FsmFloat) 100f;
      this.storeDidPickObject = (FsmBool) null;
      this.storeGameObject = (FsmGameObject) null;
      this.storePoint = (FsmVector3) null;
      this.storeNormal = (FsmVector3) null;
      this.storeDistance = (FsmFloat) null;
      this.layerMask = new FsmInt[0];
      this.invertMask = (FsmBool) false;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoMousePick();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoMousePick();
    }

    private void DoMousePick()
    {
      RaycastHit raycastHit = ActionHelpers.MousePick(this.rayDistance.Value, ActionHelpers.LayerArrayToLayerMask(this.layerMask, this.invertMask.Value));
      bool flag = (Object) raycastHit.collider != (Object) null;
      this.storeDidPickObject.Value = flag;
      if (flag)
      {
        this.storeGameObject.Value = raycastHit.collider.gameObject;
        this.storeDistance.Value = raycastHit.distance;
        this.storePoint.Value = raycastHit.point;
        this.storeNormal.Value = raycastHit.normal;
      }
      else
      {
        this.storeGameObject.Value = (GameObject) null;
        this.storeDistance.Value = float.PositiveInfinity;
        this.storePoint.Value = Vector3.zero;
        this.storeNormal.Value = Vector3.zero;
      }
    }
  }
}
