﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.RandomWait
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Time)]
  [Tooltip("Delays a State from finishing by a random time. NOTE: Other actions continue, but FINISHED can't happen before Time.")]
  public class RandomWait : FsmStateAction
  {
    [Tooltip("Minimum amount of time to wait.")]
    [RequiredField]
    public FsmFloat min;
    [RequiredField]
    [Tooltip("Maximum amount of time to wait.")]
    public FsmFloat max;
    [Tooltip("Event to send when timer is finished.")]
    public FsmEvent finishEvent;
    [Tooltip("Ignore time scale.")]
    public bool realTime;
    private float startTime;
    private float timer;
    private float time;

    public override void Reset()
    {
      this.min = (FsmFloat) 0.0f;
      this.max = (FsmFloat) 1f;
      this.finishEvent = (FsmEvent) null;
      this.realTime = false;
    }

    public override void OnEnter()
    {
      this.time = Random.Range(this.min.Value, this.max.Value);
      if ((double) this.time <= 0.0)
      {
        this.Fsm.Event(this.finishEvent);
        this.Finish();
      }
      else
      {
        this.startTime = FsmTime.RealtimeSinceStartup;
        this.timer = 0.0f;
      }
    }

    public override void OnUpdate()
    {
      if (this.realTime)
        this.timer = FsmTime.RealtimeSinceStartup - this.startTime;
      else
        this.timer += Time.deltaTime;
      if ((double) this.timer < (double) this.time)
        return;
      this.Finish();
      if (this.finishEvent == null)
        return;
      this.Fsm.Event(this.finishEvent);
    }
  }
}
