﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetIsFixedAngle2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics2D)]
  [Tooltip("Controls whether the rigidbody 2D should be prevented from rotating")]
  [Obsolete("This action is obsolete; use Constraints instead.")]
  public class SetIsFixedAngle2d : ComponentAction<Rigidbody2D>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody2D))]
    [Tooltip("The GameObject with the Rigidbody2D attached")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The flag value")]
    [RequiredField]
    public FsmBool isFixedAngle;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.isFixedAngle = (FsmBool) false;
    }

    public override void OnEnter()
    {
      this.DoSetIsFixedAngle();
      this.Finish();
    }

    private void DoSetIsFixedAngle()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.rigidbody2d.fixedAngle = this.isFixedAngle.Value;
    }
  }
}
