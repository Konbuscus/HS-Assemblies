﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetMass
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Sets the Mass of a Game Object's Rigid Body.")]
  public class SetMass : ComponentAction<Rigidbody>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody))]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [HasFloatSlider(0.1f, 10f)]
    public FsmFloat mass;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.mass = (FsmFloat) 1f;
    }

    public override void OnEnter()
    {
      this.DoSetMass();
      this.Finish();
    }

    private void DoSetMass()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.rigidbody.mass = this.mass.Value;
    }
  }
}
