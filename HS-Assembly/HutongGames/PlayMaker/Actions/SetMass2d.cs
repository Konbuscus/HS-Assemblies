﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetMass2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the Mass of a Game Object's Rigid Body 2D.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class SetMass2d : ComponentAction<Rigidbody2D>
  {
    [CheckForComponent(typeof (Rigidbody2D))]
    [Tooltip("The GameObject with the Rigidbody2D attached")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [HasFloatSlider(0.1f, 10f)]
    [Tooltip("The Mass")]
    public FsmFloat mass;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.mass = (FsmFloat) 1f;
    }

    public override void OnEnter()
    {
      this.DoSetMass();
      this.Finish();
    }

    private void DoSetMass()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.rigidbody2d.mass = this.mass.Value;
    }
  }
}
