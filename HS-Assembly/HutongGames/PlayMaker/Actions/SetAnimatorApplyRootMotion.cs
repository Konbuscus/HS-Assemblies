﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set Apply Root Motion: If true, Root is controlled by animations")]
  [ActionCategory(ActionCategory.Animator)]
  public class SetAnimatorApplyRootMotion : FsmStateAction
  {
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    [Tooltip("The Target. An Animator component is required")]
    public FsmOwnerDefault gameObject;
    [Tooltip("If true, Root is controlled by animations")]
    public FsmBool applyRootMotion;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.applyRootMotion = (FsmBool) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoApplyRootMotion();
          this.Finish();
        }
      }
    }

    private void DoApplyRootMotion()
    {
      if ((Object) this._animator == (Object) null)
        return;
      this._animator.applyRootMotion = this.applyRootMotion.Value;
    }
  }
}
