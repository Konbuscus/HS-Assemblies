﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.IsKinematic2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if a Game Object's Rigid Body 2D is Kinematic.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class IsKinematic2d : ComponentAction<Rigidbody2D>
  {
    [CheckForComponent(typeof (Rigidbody2D))]
    [Tooltip("the GameObject with a Rigidbody2D attached")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Event Sent if Kinematic")]
    public FsmEvent trueEvent;
    [Tooltip("Event sent if not Kinematic")]
    public FsmEvent falseEvent;
    [Tooltip("Store the Kinematic state")]
    [UIHint(UIHint.Variable)]
    public FsmBool store;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.store = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoIsKinematic();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoIsKinematic();
    }

    private void DoIsKinematic()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      bool isKinematic = this.rigidbody2d.isKinematic;
      this.store.Value = isKinematic;
      this.Fsm.Event(!isKinematic ? this.falseEvent : this.trueEvent);
    }
  }
}
