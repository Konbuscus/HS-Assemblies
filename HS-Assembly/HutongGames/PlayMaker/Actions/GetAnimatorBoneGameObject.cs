﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the GameObject mapped to this human bone id")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorBoneGameObject : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The target. An Animator component is required")]
    public FsmOwnerDefault gameObject;
    [ObjectType(typeof (HumanBodyBones))]
    [Tooltip("The bone reference")]
    public FsmEnum bone;
    [ActionSection("Results")]
    [UIHint(UIHint.Variable)]
    [Tooltip("The Bone's GameObject")]
    public FsmGameObject boneGameObject;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.bone = FsmEnum.op_Implicit((Enum) HumanBodyBones.Hips);
      this.boneGameObject = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((UnityEngine.Object) ownerDefaultTarget == (UnityEngine.Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((UnityEngine.Object) this._animator == (UnityEngine.Object) null)
        {
          this.Finish();
        }
        else
        {
          this.GetBoneTransform();
          this.Finish();
        }
      }
    }

    private void GetBoneTransform()
    {
      this.boneGameObject.Value = this._animator.GetBoneTransform((HumanBodyBones) this.bone.get_Value()).gameObject;
    }
  }
}
