﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetTagsOnChildren
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Set the Tag on all children of a GameObject. Optionally filter by component.")]
  public class SetTagsOnChildren : FsmStateAction
  {
    [Tooltip("GameObject Parent")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Set Tag To...")]
    [RequiredField]
    [UIHint(UIHint.Tag)]
    public FsmString tag;
    [Tooltip("Only set the Tag on children with this component.")]
    [UIHint(UIHint.ScriptComponent)]
    public FsmString filterByComponent;
    private System.Type componentFilter;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.tag = (FsmString) null;
      this.filterByComponent = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.SetTag(this.Fsm.GetOwnerDefaultTarget(this.gameObject));
      this.Finish();
    }

    private void SetTag(GameObject parent)
    {
      if ((UnityEngine.Object) parent == (UnityEngine.Object) null)
        return;
      if (string.IsNullOrEmpty(this.filterByComponent.Value))
      {
        IEnumerator enumerator = parent.transform.GetEnumerator();
        try
        {
          while (enumerator.MoveNext())
            ((Component) enumerator.Current).gameObject.tag = this.tag.Value;
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            disposable.Dispose();
        }
      }
      else
      {
        this.UpdateComponentFilter();
        if (this.componentFilter != null)
        {
          foreach (Component componentsInChild in parent.GetComponentsInChildren(this.componentFilter))
            componentsInChild.gameObject.tag = this.tag.Value;
        }
      }
      this.Finish();
    }

    private void UpdateComponentFilter()
    {
      this.componentFilter = ReflectionUtils.GetGlobalType(this.filterByComponent.Value);
      if (this.componentFilter == null)
        this.componentFilter = ReflectionUtils.GetGlobalType("UnityEngine." + this.filterByComponent.Value);
      if (this.componentFilter != null)
        return;
      Debug.LogWarning((object) ("Couldn't get type: " + this.filterByComponent.Value));
    }
  }
}
