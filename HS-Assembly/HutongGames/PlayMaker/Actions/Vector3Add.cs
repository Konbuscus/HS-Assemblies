﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector3Add
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Adds a value to Vector3 Variable.")]
  [ActionCategory(ActionCategory.Vector3)]
  public class Vector3Add : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmVector3 vector3Variable;
    [RequiredField]
    public FsmVector3 addVector;
    public bool everyFrame;
    public bool perSecond;

    public override void Reset()
    {
      this.vector3Variable = (FsmVector3) null;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.addVector = fsmVector3;
      this.everyFrame = false;
      this.perSecond = false;
    }

    public override void OnEnter()
    {
      this.DoVector3Add();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoVector3Add();
    }

    private void DoVector3Add()
    {
      if (this.perSecond)
        this.vector3Variable.Value = this.vector3Variable.Value + this.addVector.Value * Time.deltaTime;
      else
        this.vector3Variable.Value = this.vector3Variable.Value + this.addVector.Value;
    }
  }
}
