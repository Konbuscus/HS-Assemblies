﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioPlayRandomClipAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus Audio")]
  [Tooltip("Plays a random AudioClip. An AudioSource for the clip is created automatically based on the parameters.")]
  public class AudioPlayRandomClipAction : FsmStateAction
  {
    [Tooltip("Optional. If specified, the generated Audio Source will use the same transform as this object.")]
    public FsmOwnerDefault m_ParentObject;
    [RequiredField]
    [CompoundArray("Audio Clips", "Audio Clip", "Weight")]
    public AudioClip[] m_Clips;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat[] m_Weights;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_MinVolume;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_MaxVolume;
    [HasFloatSlider(-3f, 3f)]
    public FsmFloat m_MinPitch;
    [HasFloatSlider(-3f, 3f)]
    public FsmFloat m_MaxPitch;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_SpatialBlend;
    public SoundCategory m_Category;
    [Tooltip("If specified, this Audio Source will be used as a template for the generated Audio Source, otherwise the one in the SoundConfig will be the template.")]
    public AudioSource m_TemplateSource;

    public override void Reset()
    {
      this.m_ParentObject = (FsmOwnerDefault) null;
      this.m_Clips = new AudioClip[3];
      this.m_Weights = new FsmFloat[3]
      {
        (FsmFloat) 1f,
        (FsmFloat) 1f,
        (FsmFloat) 1f
      };
      this.m_MinVolume = (FsmFloat) 1f;
      this.m_MaxVolume = (FsmFloat) 1f;
      this.m_MinPitch = (FsmFloat) 1f;
      this.m_MaxPitch = (FsmFloat) 1f;
      this.m_SpatialBlend = (FsmFloat) 1f;
      this.m_Category = SoundCategory.FX;
      this.m_TemplateSource = (AudioSource) null;
    }

    public override void OnEnter()
    {
      if (this.m_Clips == null || this.m_Clips.Length == 0)
      {
        this.Finish();
      }
      else
      {
        SoundPlayClipArgs args = new SoundPlayClipArgs();
        args.m_templateSource = this.m_TemplateSource;
        args.m_clip = this.ChooseClip();
        args.m_volume = new float?(this.ChooseVolume());
        args.m_pitch = new float?(this.ChoosePitch());
        if (!this.m_SpatialBlend.IsNone)
          args.m_spatialBlend = new float?(this.m_SpatialBlend.Value);
        if (this.m_Category != SoundCategory.NONE)
          args.m_category = new SoundCategory?(this.m_Category);
        args.m_parentObject = this.Fsm.GetOwnerDefaultTarget(this.m_ParentObject);
        SoundManager.Get().PlayClip(args);
        this.Finish();
      }
    }

    private AudioClip ChooseClip()
    {
      if (this.m_Weights == null || this.m_Weights.Length == 0)
        return this.m_Clips[0];
      return this.m_Clips[ActionHelpers.GetRandomWeightedIndex(this.m_Weights)];
    }

    private float ChooseVolume()
    {
      float num1 = !this.m_MinVolume.IsNone ? this.m_MinVolume.Value : 1f;
      float num2 = !this.m_MaxVolume.IsNone ? this.m_MaxVolume.Value : 1f;
      if (Mathf.Approximately(num1, num2))
        return num1;
      return Random.Range(num1, num2);
    }

    private float ChoosePitch()
    {
      float num1 = !this.m_MinPitch.IsNone ? this.m_MinPitch.Value : 1f;
      float num2 = !this.m_MaxPitch.IsNone ? this.m_MaxPitch.Value : 1f;
      if (Mathf.Approximately(num1, num2))
        return num1;
      return Random.Range(num1, num2);
    }
  }
}
