﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutSpace
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUILayout)]
  [Tooltip("Inserts a space in the current layout group.")]
  public class GUILayoutSpace : FsmStateAction
  {
    public FsmFloat space;

    public override void Reset()
    {
      this.space = (FsmFloat) 10f;
    }

    public override void OnGUI()
    {
      GUILayout.Space(this.space.Value);
    }
  }
}
