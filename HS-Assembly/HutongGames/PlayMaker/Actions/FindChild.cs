﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FindChild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Finds the Child of a GameObject by Name.\nNote, you can specify a path to the child, e.g., LeftShoulder/Arm/Hand/Finger. If you need to specify a tag, use GetChild.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class FindChild : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject to search.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The name of the child. Note, you can specify a path to the child, e.g., LeftShoulder/Arm/Hand/Finger")]
    [RequiredField]
    public FsmString childName;
    [RequiredField]
    [Tooltip("Store the child in a GameObject variable.")]
    [UIHint(UIHint.Variable)]
    public FsmGameObject storeResult;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.childName = (FsmString) string.Empty;
      this.storeResult = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      this.DoFindChild();
      this.Finish();
    }

    private void DoFindChild()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      Transform child = ownerDefaultTarget.transform.FindChild(this.childName.Value);
      this.storeResult.Value = !((Object) child != (Object) null) ? (GameObject) null : child.gameObject;
    }
  }
}
