﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioSetRandomPitchAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus Audio")]
  [Tooltip("Randomly sets the pitch of an AudioSource on a Game Object.")]
  public class AudioSetRandomPitchAction : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (AudioSource))]
    public FsmOwnerDefault m_GameObject;
    [HasFloatSlider(-3f, 3f)]
    public FsmFloat m_MinPitch;
    [HasFloatSlider(-3f, 3f)]
    public FsmFloat m_MaxPitch;
    public bool m_EveryFrame;
    private float m_pitch;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_MinPitch = (FsmFloat) 1f;
      this.m_MaxPitch = (FsmFloat) 1f;
      this.m_EveryFrame = false;
    }

    public override void OnEnter()
    {
      this.ChoosePitch();
      this.UpdatePitch();
      if (this.m_EveryFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.UpdatePitch();
    }

    private void ChoosePitch()
    {
      this.m_pitch = Random.Range(!this.m_MinPitch.IsNone ? this.m_MinPitch.Value : 1f, !this.m_MaxPitch.IsNone ? this.m_MaxPitch.Value : 1f);
    }

    private void UpdatePitch()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      AudioSource component = ownerDefaultTarget.GetComponent<AudioSource>();
      if ((Object) component == (Object) null)
        return;
      SoundManager.Get().SetPitch(component, this.m_pitch);
    }
  }
}
