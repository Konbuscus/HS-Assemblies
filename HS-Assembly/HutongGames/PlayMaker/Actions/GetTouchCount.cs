﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetTouchCount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the number of Touches.")]
  [ActionCategory(ActionCategory.Device)]
  public class GetTouchCount : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmInt storeCount;
    public bool everyFrame;

    public override void Reset()
    {
      this.storeCount = (FsmInt) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetTouchCount();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetTouchCount();
    }

    private void DoGetTouchCount()
    {
      this.storeCount.Value = Input.touchCount;
    }
  }
}
