﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayReverse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Reverse the order of items in an Array.")]
  [ActionCategory(ActionCategory.Array)]
  public class ArrayReverse : FsmStateAction
  {
    [Tooltip("The Array to reverse.")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmArray array;

    public override void Reset()
    {
      this.array = (FsmArray) null;
    }

    public override void OnEnter()
    {
      List<object> objectList = new List<object>((IEnumerable<object>) this.array.Values);
      objectList.Reverse();
      this.array.Values = objectList.ToArray();
      this.Finish();
    }
  }
}
