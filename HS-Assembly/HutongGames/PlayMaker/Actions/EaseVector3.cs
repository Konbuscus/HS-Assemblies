﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.EaseVector3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Easing Animation - Vector3")]
  [ActionCategory(ActionCategory.AnimateVariables)]
  public class EaseVector3 : EaseFsmAction
  {
    [RequiredField]
    public FsmVector3 fromValue;
    [RequiredField]
    public FsmVector3 toValue;
    [UIHint(UIHint.Variable)]
    public FsmVector3 vector3Variable;
    private bool finishInNextStep;

    public override void Reset()
    {
      base.Reset();
      this.vector3Variable = (FsmVector3) null;
      this.fromValue = (FsmVector3) null;
      this.toValue = (FsmVector3) null;
      this.finishInNextStep = false;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      this.fromFloats = new float[3];
      this.fromFloats[0] = this.fromValue.Value.x;
      this.fromFloats[1] = this.fromValue.Value.y;
      this.fromFloats[2] = this.fromValue.Value.z;
      this.toFloats = new float[3];
      this.toFloats[0] = this.toValue.Value.x;
      this.toFloats[1] = this.toValue.Value.y;
      this.toFloats[2] = this.toValue.Value.z;
      this.resultFloats = new float[3];
      this.finishInNextStep = false;
      this.vector3Variable.Value = this.fromValue.Value;
    }

    public override void OnExit()
    {
      base.OnExit();
    }

    public override void OnUpdate()
    {
      base.OnUpdate();
      if (!this.vector3Variable.IsNone && this.isRunning)
        this.vector3Variable.Value = new Vector3(this.resultFloats[0], this.resultFloats[1], this.resultFloats[2]);
      if (this.finishInNextStep)
      {
        this.Finish();
        if (this.finishEvent != null)
          this.Fsm.Event(this.finishEvent);
      }
      if (!this.finishAction || this.finishInNextStep)
        return;
      if (!this.vector3Variable.IsNone)
        this.vector3Variable.Value = new Vector3(!this.reverse.IsNone ? (!this.reverse.Value ? this.toValue.Value.x : this.fromValue.Value.x) : this.toValue.Value.x, !this.reverse.IsNone ? (!this.reverse.Value ? this.toValue.Value.y : this.fromValue.Value.y) : this.toValue.Value.y, !this.reverse.IsNone ? (!this.reverse.Value ? this.toValue.Value.z : this.fromValue.Value.z) : this.toValue.Value.z);
      this.finishInNextStep = true;
    }
  }
}
