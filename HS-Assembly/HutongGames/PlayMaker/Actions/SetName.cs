﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets a Game Object's Name.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class SetName : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    public FsmString name;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.name = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.DoSetLayer();
      this.Finish();
    }

    private void DoSetLayer()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      ownerDefaultTarget.name = this.name.Value;
    }
  }
}
