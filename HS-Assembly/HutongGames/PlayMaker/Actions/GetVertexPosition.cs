﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetVertexPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the position of a vertex in a GameObject's mesh. Hint: Use GetVertexCount to get the number of vertices in a mesh.")]
  [ActionCategory("Mesh")]
  public class GetVertexPosition : FsmStateAction
  {
    [Tooltip("The GameObject to check.")]
    [RequiredField]
    [CheckForComponent(typeof (MeshFilter))]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The index of the vertex.")]
    public FsmInt vertexIndex;
    [Tooltip("Coordinate system to use.")]
    public Space space;
    [Tooltip("Store the vertex position in a variable.")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmVector3 storePosition;
    [Tooltip("Repeat every frame. Useful if the mesh is animated.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.space = Space.World;
      this.storePosition = (FsmVector3) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetVertexPosition();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetVertexPosition();
    }

    private void DoGetVertexPosition()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if (!((Object) ownerDefaultTarget != (Object) null))
        return;
      MeshFilter component = ownerDefaultTarget.GetComponent<MeshFilter>();
      if ((Object) component == (Object) null)
      {
        this.LogError("Missing MeshFilter!");
      }
      else
      {
        switch (this.space)
        {
          case Space.World:
            Vector3 vertex = component.mesh.vertices[this.vertexIndex.Value];
            this.storePosition.Value = ownerDefaultTarget.transform.TransformPoint(vertex);
            break;
          case Space.Self:
            this.storePosition.Value = component.mesh.vertices[this.vertexIndex.Value];
            break;
        }
      }
    }
  }
}
