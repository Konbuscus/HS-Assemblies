﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUIHorizontalSlider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("GUI Horizontal Slider connected to a Float Variable.")]
  public class GUIHorizontalSlider : GUIAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmFloat floatVariable;
    [RequiredField]
    public FsmFloat leftValue;
    [RequiredField]
    public FsmFloat rightValue;
    public FsmString sliderStyle;
    public FsmString thumbStyle;

    public override void Reset()
    {
      base.Reset();
      this.floatVariable = (FsmFloat) null;
      this.leftValue = (FsmFloat) 0.0f;
      this.rightValue = (FsmFloat) 100f;
      this.sliderStyle = (FsmString) "horizontalslider";
      this.thumbStyle = (FsmString) "horizontalsliderthumb";
    }

    public override void OnGUI()
    {
      base.OnGUI();
      if (this.floatVariable == null)
        return;
      this.floatVariable.Value = GUI.HorizontalSlider(this.rect, this.floatVariable.Value, this.leftValue.Value, this.rightValue.Value, (GUIStyle) (!(this.sliderStyle.Value != string.Empty) ? "horizontalslider" : this.sliderStyle.Value), (GUIStyle) (!(this.thumbStyle.Value != string.Empty) ? "horizontalsliderthumb" : this.thumbStyle.Value));
    }
  }
}
