﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioPauseAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Pauses the Audio Source of a Game Object.")]
  [ActionCategory("Pegasus Audio")]
  public class AudioPauseAction : FsmStateAction
  {
    [CheckForComponent(typeof (AudioSource))]
    [RequiredField]
    public FsmOwnerDefault m_GameObject;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget != (Object) null)
      {
        AudioSource component = ownerDefaultTarget.GetComponent<AudioSource>();
        if ((Object) component != (Object) null)
          SoundManager.Get().Pause(component);
      }
      this.Finish();
    }
  }
}
