﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetMouseCursor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Controls the appearance of Mouse Cursor.")]
  [ActionCategory(ActionCategory.GUI)]
  public class SetMouseCursor : FsmStateAction
  {
    public FsmTexture cursorTexture;
    public FsmBool hideCursor;
    public FsmBool lockCursor;

    public override void Reset()
    {
      this.cursorTexture = (FsmTexture) null;
      this.hideCursor = (FsmBool) false;
      this.lockCursor = (FsmBool) false;
    }

    public override void OnEnter()
    {
      PlayMakerGUI.LockCursor = this.lockCursor.Value;
      PlayMakerGUI.HideCursor = this.hideCursor.Value;
      PlayMakerGUI.MouseCursor = this.cursorTexture.Value;
      this.Finish();
    }
  }
}
