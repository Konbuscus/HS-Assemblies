﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUIVerticalSlider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("GUI Vertical Slider connected to a Float Variable.")]
  public class GUIVerticalSlider : GUIAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmFloat floatVariable;
    [RequiredField]
    public FsmFloat topValue;
    [RequiredField]
    public FsmFloat bottomValue;
    public FsmString sliderStyle;
    public FsmString thumbStyle;

    public override void Reset()
    {
      base.Reset();
      this.floatVariable = (FsmFloat) null;
      this.topValue = (FsmFloat) 100f;
      this.bottomValue = (FsmFloat) 0.0f;
      this.sliderStyle = (FsmString) "verticalslider";
      this.thumbStyle = (FsmString) "verticalsliderthumb";
      this.width = (FsmFloat) 0.1f;
    }

    public override void OnGUI()
    {
      base.OnGUI();
      if (this.floatVariable == null)
        return;
      this.floatVariable.Value = GUI.VerticalSlider(this.rect, this.floatVariable.Value, this.topValue.Value, this.bottomValue.Value, (GUIStyle) (!(this.sliderStyle.Value != string.Empty) ? "verticalslider" : this.sliderStyle.Value), (GUIStyle) (!(this.thumbStyle.Value != string.Empty) ? "verticalsliderthumb" : this.thumbStyle.Value));
    }
  }
}
