﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.KillDelayedEvents
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Note("Kill all queued delayed events.")]
  [ActionCategory(ActionCategory.StateMachine)]
  [Tooltip("Kill all queued delayed events. Normally delayed events are automatically killed when the active state is exited, but you can override this behaviour in FSM settings. If you choose to keep delayed events you can use this action to kill them when needed.")]
  public class KillDelayedEvents : FsmStateAction
  {
    public override void OnEnter()
    {
      this.Fsm.KillDelayedEvents();
      this.Finish();
    }
  }
}
