﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetVectorLength
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get Vector3 Length.")]
  [ActionCategory(ActionCategory.Vector3)]
  public class GetVectorLength : FsmStateAction
  {
    public FsmVector3 vector3;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmFloat storeLength;

    public override void Reset()
    {
      this.vector3 = (FsmVector3) null;
      this.storeLength = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      this.DoVectorLength();
      this.Finish();
    }

    private void DoVectorLength()
    {
      if (this.vector3 == null || this.storeLength == null)
        return;
      this.storeLength.Value = this.vector3.Value.magnitude;
    }
  }
}
