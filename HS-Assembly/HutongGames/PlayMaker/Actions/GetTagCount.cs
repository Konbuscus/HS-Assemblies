﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetTagCount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Gets the number of Game Objects in the scene with the specified Tag.")]
  public class GetTagCount : FsmStateAction
  {
    [UIHint(UIHint.Tag)]
    public FsmString tag;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmInt storeResult;

    public override void Reset()
    {
      this.tag = (FsmString) "Untagged";
      this.storeResult = (FsmInt) null;
    }

    public override void OnEnter()
    {
      GameObject[] gameObjectsWithTag = GameObject.FindGameObjectsWithTag(this.tag.Value);
      if (this.storeResult != null)
        this.storeResult.Value = gameObjectsWithTag == null ? 0 : gameObjectsWithTag.Length;
      this.Finish();
    }
  }
}
