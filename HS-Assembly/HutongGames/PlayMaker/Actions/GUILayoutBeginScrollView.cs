﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Begins a ScrollView. Use GUILayoutEndScrollView at the end of the block.")]
  [ActionCategory(ActionCategory.GUILayout)]
  public class GUILayoutBeginScrollView : GUILayoutAction
  {
    [RequiredField]
    [Tooltip("Assign a Vector2 variable to store the scroll position of this view.")]
    [UIHint(UIHint.Variable)]
    public FsmVector2 scrollPosition;
    [Tooltip("Always show the horizontal scrollbars.")]
    public FsmBool horizontalScrollbar;
    [Tooltip("Always show the vertical scrollbars.")]
    public FsmBool verticalScrollbar;
    [Tooltip("Define custom styles below. NOTE: You have to define all the styles if you check this option.")]
    public FsmBool useCustomStyle;
    [Tooltip("Named style in the active GUISkin for the horizontal scrollbars.")]
    public FsmString horizontalStyle;
    [Tooltip("Named style in the active GUISkin for the vertical scrollbars.")]
    public FsmString verticalStyle;
    [Tooltip("Named style in the active GUISkin for the background.")]
    public FsmString backgroundStyle;

    public override void Reset()
    {
      base.Reset();
      this.scrollPosition = (FsmVector2) null;
      this.horizontalScrollbar = (FsmBool) null;
      this.verticalScrollbar = (FsmBool) null;
      this.useCustomStyle = (FsmBool) null;
      this.horizontalStyle = (FsmString) null;
      this.verticalStyle = (FsmString) null;
      this.backgroundStyle = (FsmString) null;
    }

    public override void OnGUI()
    {
      if (this.useCustomStyle.Value)
        this.scrollPosition.Value = GUILayout.BeginScrollView(this.scrollPosition.Value, this.horizontalScrollbar.Value, this.verticalScrollbar.Value, (GUIStyle) this.horizontalStyle.Value, (GUIStyle) this.verticalStyle.Value, (GUIStyle) this.backgroundStyle.Value, this.LayoutOptions);
      else
        this.scrollPosition.Value = GUILayout.BeginScrollView(this.scrollPosition.Value, this.horizontalScrollbar.Value, this.verticalScrollbar.Value, this.LayoutOptions);
    }
  }
}
