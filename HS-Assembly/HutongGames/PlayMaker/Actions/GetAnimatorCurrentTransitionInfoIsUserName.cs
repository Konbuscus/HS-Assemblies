﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Check the active Transition user-specified name on a specified layer.")]
  public class GetAnimatorCurrentTransitionInfoIsUserName : FsmStateActionAnimatorBase
  {
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The target. An Animator component is required")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The layer's index")]
    public FsmInt layerIndex;
    [Tooltip("The user-specified name to check the transition against.")]
    public FsmString userName;
    [ActionSection("Results")]
    [UIHint(UIHint.Variable)]
    [Tooltip("True if name matches")]
    public FsmBool nameMatch;
    [Tooltip("Event send if name matches")]
    public FsmEvent nameMatchEvent;
    [Tooltip("Event send if name doesn't match")]
    public FsmEvent nameDoNotMatchEvent;
    private Animator _animator;

    public override void Reset()
    {
      base.Reset();
      this.gameObject = (FsmOwnerDefault) null;
      this.layerIndex = (FsmInt) null;
      this.userName = (FsmString) null;
      this.nameMatch = (FsmBool) null;
      this.nameMatchEvent = (FsmEvent) null;
      this.nameDoNotMatchEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.IsName();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnActionUpdate()
    {
      this.IsName();
    }

    private void IsName()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      bool flag = this._animator.GetAnimatorTransitionInfo(this.layerIndex.Value).IsUserName(this.userName.Value);
      if (!this.nameMatch.IsNone)
        this.nameMatch.Value = flag;
      if (flag)
        this.Fsm.Event(this.nameMatchEvent);
      else
        this.Fsm.Event(this.nameDoNotMatchEvent);
    }
  }
}
