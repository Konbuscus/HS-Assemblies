﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.HasComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Checks if an Object has a Component. Optionally remove the Component on exiting the state.")]
  public class HasComponent : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [UIHint(UIHint.ScriptComponent)]
    public FsmString component;
    public FsmBool removeOnExit;
    public FsmEvent trueEvent;
    public FsmEvent falseEvent;
    [UIHint(UIHint.Variable)]
    public FsmBool store;
    public bool everyFrame;
    private Component aComponent;

    public override void Reset()
    {
      this.aComponent = (Component) null;
      this.gameObject = (FsmOwnerDefault) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.component = (FsmString) null;
      this.store = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoHasComponent(this.gameObject.OwnerOption != OwnerDefaultOption.UseOwner ? this.gameObject.GameObject.Value : this.Owner);
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoHasComponent(this.gameObject.OwnerOption != OwnerDefaultOption.UseOwner ? this.gameObject.GameObject.Value : this.Owner);
    }

    public override void OnExit()
    {
      if (!this.removeOnExit.Value || !((Object) this.aComponent != (Object) null))
        return;
      Object.Destroy((Object) this.aComponent);
    }

    private void DoHasComponent(GameObject go)
    {
      this.aComponent = go.GetComponent(ReflectionUtils.GetGlobalType(this.component.Value));
      this.Fsm.Event(!((Object) this.aComponent != (Object) null) ? this.falseEvent : this.trueEvent);
    }
  }
}
