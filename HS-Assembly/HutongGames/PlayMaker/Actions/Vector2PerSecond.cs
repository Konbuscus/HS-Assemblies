﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector2PerSecond
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Vector2)]
  [Tooltip("Multiplies a Vector2 variable by Time.deltaTime. Useful for frame rate independent motion.")]
  public class Vector2PerSecond : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The Vector2")]
    public FsmVector2 vector2Variable;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.vector2Variable = (FsmVector2) null;
      this.everyFrame = true;
    }

    public override void OnEnter()
    {
      this.vector2Variable.Value = this.vector2Variable.Value * Time.deltaTime;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.vector2Variable.Value = this.vector2Variable.Value * Time.deltaTime;
    }
  }
}
