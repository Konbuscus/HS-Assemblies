﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAmbientLight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.RenderSettings)]
  [Tooltip("Sets the Ambient Light Color for the scene.")]
  public class SetAmbientLight : FsmStateAction
  {
    [RequiredField]
    public FsmColor ambientColor;
    public bool everyFrame;

    public override void Reset()
    {
      this.ambientColor = (FsmColor) Color.gray;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetAmbientColor();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetAmbientColor();
    }

    private void DoSetAmbientColor()
    {
      RenderSettings.ambientLight = this.ambientColor.Value;
    }
  }
}
