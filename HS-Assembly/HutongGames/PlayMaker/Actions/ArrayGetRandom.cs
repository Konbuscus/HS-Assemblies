﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayGetRandom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get a Random item from an Array.")]
  [ActionCategory(ActionCategory.Array)]
  public class ArrayGetRandom : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("The Array to use.")]
    [RequiredField]
    public FsmArray array;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the value in a variable.")]
    [MatchElementType("array")]
    [RequiredField]
    public FsmVar storeValue;
    [Tooltip("Repeat every frame while the state is active.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.array = (FsmArray) null;
      this.storeValue = (FsmVar) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetRandomValue();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetRandomValue();
    }

    private void DoGetRandomValue()
    {
      if (this.storeValue.IsNone)
        return;
      this.storeValue.SetValue(this.array.Get(Random.Range(0, this.array.Length)));
    }
  }
}
