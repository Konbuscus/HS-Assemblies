﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.iTweenResume
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("iTween")]
  [Tooltip("Resume an iTween action.")]
  public class iTweenResume : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault gameObject;
    public iTweenFSMType iTweenType;
    public bool includeChildren;
    public bool inScene;

    public override void Reset()
    {
      this.iTweenType = iTweenFSMType.all;
      this.includeChildren = false;
      this.inScene = false;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      this.DoiTween();
      this.Finish();
    }

    private void DoiTween()
    {
      if (this.iTweenType == iTweenFSMType.all)
        iTween.Resume();
      else if (this.inScene)
      {
        iTween.Resume(Enum.GetName(typeof (iTweenFSMType), (object) this.iTweenType));
      }
      else
      {
        GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
        if ((UnityEngine.Object) ownerDefaultTarget == (UnityEngine.Object) null)
          return;
        iTween.Resume(ownerDefaultTarget, Enum.GetName(typeof (iTweenFSMType), (object) this.iTweenType), this.includeChildren);
      }
    }
  }
}
