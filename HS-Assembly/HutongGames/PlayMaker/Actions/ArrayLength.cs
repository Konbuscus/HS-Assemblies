﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayLength
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Array)]
  [Tooltip("Gets the number of items in an Array.")]
  public class ArrayLength : FsmStateAction
  {
    [Tooltip("The Array Variable.")]
    [UIHint(UIHint.Variable)]
    public FsmArray array;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the length in an Int Variable.")]
    public FsmInt length;
    [Tooltip("Repeat every frame. Useful if the array is changing and you're waiting for a particular length.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.array = (FsmArray) null;
      this.length = (FsmInt) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.length.Value = this.array.Length;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.length.Value = this.array.Length;
    }
  }
}
