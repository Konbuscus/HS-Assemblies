﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DebugBool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Debug)]
  [Tooltip("Logs the value of a Bool Variable in the PlayMaker Log Window.")]
  public class DebugBool : BaseLogAction
  {
    [Tooltip("Info, Warning, or Error.")]
    public LogLevel logLevel;
    [Tooltip("The Bool variable to debug.")]
    [UIHint(UIHint.Variable)]
    public FsmBool boolVariable;

    public override void Reset()
    {
      this.logLevel = LogLevel.Info;
      this.boolVariable = (FsmBool) null;
      base.Reset();
    }

    public override void OnEnter()
    {
      string text = "None";
      if (!this.boolVariable.IsNone)
        text = this.boolVariable.Name + ": " + (object) this.boolVariable.Value;
      ActionHelpers.DebugLog(this.Fsm, this.logLevel, text, this.sendToUnityLog);
      this.Finish();
    }
  }
}
