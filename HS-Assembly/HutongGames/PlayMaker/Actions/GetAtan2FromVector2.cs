﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAtan2FromVector2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get the Arc Tangent 2 as in atan2(y,x) from a vector 2. You can get the result in degrees, simply check on the RadToDeg conversion")]
  [ActionCategory(ActionCategory.Trigonometry)]
  public class GetAtan2FromVector2 : FsmStateAction
  {
    [Tooltip("The vector2 of the tan")]
    [RequiredField]
    public FsmVector2 vector2;
    [UIHint(UIHint.Variable)]
    [Tooltip("The resulting angle. Note:If you want degrees, simply check RadToDeg")]
    [RequiredField]
    public FsmFloat angle;
    [Tooltip("Check on if you want the angle expressed in degrees.")]
    public FsmBool RadToDeg;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.vector2 = (FsmVector2) null;
      this.RadToDeg = (FsmBool) true;
      this.everyFrame = false;
      this.angle = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      this.DoATan();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoATan();
    }

    private void DoATan()
    {
      float num = Mathf.Atan2(this.vector2.Value.y, this.vector2.Value.x);
      if (this.RadToDeg.Value)
        num *= 57.29578f;
      this.angle.Value = num;
    }
  }
}
