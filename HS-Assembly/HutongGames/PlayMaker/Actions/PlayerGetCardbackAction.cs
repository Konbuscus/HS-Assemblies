﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PlayerGetCardbackAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Store a player's cardback materials based on Player side.")]
  public class PlayerGetCardbackAction : FsmStateAction
  {
    public Player.Side m_PlayerSide;
    public FsmMaterial m_CardbackMaterial;
    public FsmTexture m_CardbackTextureFlat;

    public override void Reset()
    {
      this.m_PlayerSide = Player.Side.FRIENDLY;
      this.m_CardbackMaterial = (FsmMaterial) null;
      this.m_CardbackTextureFlat = (FsmTexture) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      CardBack cardBack = this.m_PlayerSide == Player.Side.OPPOSING ? CardBackManager.Get().GetOpponentCardBack() : CardBackManager.Get().GetFriendlyCardBack();
      if ((Object) cardBack != (Object) null)
      {
        this.m_CardbackMaterial.Value = cardBack.m_CardBackMaterial;
        this.m_CardbackTextureFlat.Value = (Texture) cardBack.m_CardBackTexture;
      }
      this.Finish();
    }
  }
}
