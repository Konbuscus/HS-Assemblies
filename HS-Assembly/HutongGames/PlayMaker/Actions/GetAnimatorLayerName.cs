﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorLayerName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Returns the name of a layer from its index")]
  public class GetAnimatorLayerName : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The Target. An Animator component is required")]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [Tooltip("The layer index")]
    [RequiredField]
    public FsmInt layerIndex;
    [ActionSection("Results")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The layer name")]
    public FsmString layerName;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.layerIndex = (FsmInt) null;
      this.layerName = (FsmString) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoGetLayerName();
          this.Finish();
        }
      }
    }

    private void DoGetLayerName()
    {
      if ((Object) this._animator == (Object) null)
        return;
      this.layerName.Value = this._animator.GetLayerName(this.layerIndex.Value);
    }
  }
}
