﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DeviceOrientationEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sends an Event based on the Orientation of the mobile device.")]
  [ActionCategory(ActionCategory.Device)]
  public class DeviceOrientationEvent : FsmStateAction
  {
    [Tooltip("Note: If device is physically situated between discrete positions, as when (for example) rotated diagonally, system will report Unknown orientation.")]
    public DeviceOrientation orientation;
    [Tooltip("The event to send if the device orientation matches Orientation.")]
    public FsmEvent sendEvent;
    [Tooltip("Repeat every frame. Useful if you want to wait for the orientation to be true.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.orientation = DeviceOrientation.Portrait;
      this.sendEvent = (FsmEvent) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoDetectDeviceOrientation();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoDetectDeviceOrientation();
    }

    private void DoDetectDeviceOrientation()
    {
      if (Input.deviceOrientation != this.orientation)
        return;
      this.Fsm.Event(this.sendEvent);
    }
  }
}
