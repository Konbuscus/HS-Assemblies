﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetFsmArrayItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set an item in an Array Variable in another FSM.")]
  [ActionTarget(typeof (PlayMakerFSM), "gameObject,fsmName", false)]
  public class SetFsmArrayItem : BaseFsmVariableIndexAction
  {
    [Tooltip("The GameObject that owns the FSM.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Optional name of FSM on Game Object.")]
    [UIHint(UIHint.FsmName)]
    public FsmString fsmName;
    [RequiredField]
    [Tooltip("The name of the FSM variable.")]
    [UIHint(UIHint.FsmArray)]
    public FsmString variableName;
    [Tooltip("The index into the array.")]
    public FsmInt index;
    [RequiredField]
    [Tooltip("Set the value of the array at the specified index.")]
    public FsmVar value;
    [Tooltip("Repeat every frame. Useful if the value is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.fsmName = (FsmString) string.Empty;
      this.value = (FsmVar) null;
    }

    public override void OnEnter()
    {
      this.DoSetFsmArray();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    private void DoSetFsmArray()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject), this.fsmName.Value))
        return;
      FsmArray fsmArray = this.fsm.FsmVariables.GetFsmArray(this.variableName.Value);
      if (fsmArray != null)
      {
        if (this.index.Value < 0 || this.index.Value >= fsmArray.Length)
        {
          this.Fsm.Event(this.indexOutOfRange);
          this.Finish();
        }
        else if (fsmArray.ElementType == this.value.NamedVar.VariableType)
          fsmArray.Set(this.index.Value, this.value.GetValue());
        else
          this.LogWarning("Incompatible variable type: " + this.variableName.Value);
      }
      else
        this.DoVariableNotFound(this.variableName.Value);
    }

    public override void OnUpdate()
    {
      this.DoSetFsmArray();
    }
  }
}
