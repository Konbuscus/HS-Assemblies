﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAlphaRecursiveAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the alpha on a game object and its children.")]
  [ActionCategory("Pegasus")]
  public class SetAlphaRecursiveAction : FsmStateAction
  {
    public FsmOwnerDefault m_GameObject;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_Alpha;
    public bool m_EveryFrame;
    public bool m_IncludeChildren;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_Alpha = (FsmFloat) 0.0f;
      this.m_EveryFrame = false;
      this.m_IncludeChildren = false;
    }

    public override void OnEnter()
    {
      if ((Object) this.Fsm.GetOwnerDefaultTarget(this.m_GameObject) == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this.UpdateAlpha();
        if (this.m_EveryFrame)
          return;
        this.Finish();
      }
    }

    public override void OnUpdate()
    {
      this.UpdateAlpha();
    }

    private void UpdateAlpha()
    {
      if (this.m_Alpha.IsNone)
        return;
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      RenderUtils.SetAlpha(ownerDefaultTarget, this.m_Alpha.Value);
    }
  }
}
