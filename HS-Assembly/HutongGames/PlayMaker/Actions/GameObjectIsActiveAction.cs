﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GameObjectIsActiveAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Tests if a Game Object is active.")]
  public class GameObjectIsActiveAction : FsmStateAction
  {
    [Tooltip("The GameObject to test.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Event to send if the GameObject is active.")]
    public FsmEvent trueEvent;
    [Tooltip("Event to send if the GameObject is NOT active.")]
    public FsmEvent falseEvent;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result in a bool variable.")]
    public FsmBool storeResult;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoIsActive();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoIsActive();
    }

    private void DoIsActive()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget != (Object) null)
      {
        bool activeInHierarchy = ownerDefaultTarget.activeInHierarchy;
        this.storeResult.Value = activeInHierarchy;
        this.Fsm.Event(!activeInHierarchy ? this.falseEvent : this.trueEvent);
      }
      else
        Debug.LogError((object) "FSM GameObjectIsActive Error: GameObject is Null!");
    }
  }
}
