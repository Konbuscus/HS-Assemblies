﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.QuaternionAngleAxis
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Creates a rotation which rotates angle degrees around axis.")]
  [ActionCategory(ActionCategory.Quaternion)]
  public class QuaternionAngleAxis : QuaternionBaseAction
  {
    [Tooltip("The angle.")]
    [RequiredField]
    public FsmFloat angle;
    [RequiredField]
    [Tooltip("The rotation axis.")]
    public FsmVector3 axis;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("Store the rotation of this quaternion variable.")]
    public FsmQuaternion result;

    public override void Reset()
    {
      this.angle = (FsmFloat) null;
      this.axis = (FsmVector3) null;
      this.result = (FsmQuaternion) null;
      this.everyFrame = true;
      this.everyFrameOption = QuaternionBaseAction.everyFrameOptions.Update;
    }

    public override void OnEnter()
    {
      this.DoQuatAngleAxis();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.Update)
        return;
      this.DoQuatAngleAxis();
    }

    public override void OnLateUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.LateUpdate)
        return;
      this.DoQuatAngleAxis();
    }

    public override void OnFixedUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.FixedUpdate)
        return;
      this.DoQuatAngleAxis();
    }

    private void DoQuatAngleAxis()
    {
      this.result.Value = Quaternion.AngleAxis(this.angle.Value, this.axis.Value);
    }
  }
}
