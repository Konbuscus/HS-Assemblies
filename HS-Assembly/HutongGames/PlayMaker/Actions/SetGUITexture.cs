﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGUITexture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUIElement)]
  [Tooltip("Sets the Texture used by the GUITexture attached to a Game Object.")]
  public class SetGUITexture : ComponentAction<GUITexture>
  {
    [Tooltip("The GameObject that owns the GUITexture.")]
    [CheckForComponent(typeof (GUITexture))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Texture to apply.")]
    public FsmTexture texture;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.texture = (FsmTexture) null;
    }

    public override void OnEnter()
    {
      if (this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        this.guiTexture.texture = this.texture.Value;
      this.Finish();
    }
  }
}
