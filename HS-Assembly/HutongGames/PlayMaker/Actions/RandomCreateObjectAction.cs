﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.RandomCreateObjectAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Randomly picks an object from a list and creates it. THE CREATED OBJECT MUST BE DESTROYED!")]
  [ActionCategory("Pegasus")]
  public class RandomCreateObjectAction : FsmStateAction
  {
    [Tooltip("The created object gets stored in this variable.")]
    [UIHint(UIHint.Variable)]
    public FsmGameObject m_CreatedObject;
    [CompoundArray("Random Objects", "Object", "Weight")]
    public FsmGameObject[] m_Objects;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat[] m_Weights;

    public override void Reset()
    {
      FsmGameObject fsmGameObject = new FsmGameObject();
      fsmGameObject.UseVariable = true;
      this.m_CreatedObject = fsmGameObject;
      this.m_Objects = new FsmGameObject[3];
      this.m_Weights = new FsmFloat[3]
      {
        (FsmFloat) 1f,
        (FsmFloat) 1f,
        (FsmFloat) 1f
      };
    }

    public override void OnEnter()
    {
      if (this.m_Objects == null || this.m_Objects.Length == 0)
      {
        this.Finish();
      }
      else
      {
        GameObject destination = (GameObject) null;
        FsmGameObject fsmGameObject = this.m_Objects[ActionHelpers.GetRandomWeightedIndex(this.m_Weights)];
        if (fsmGameObject != null && !fsmGameObject.IsNone && (bool) ((Object) fsmGameObject.Value))
        {
          destination = Object.Instantiate<GameObject>(fsmGameObject.Value);
          TransformUtil.CopyWorld(destination, fsmGameObject.Value);
        }
        if (this.m_CreatedObject != null)
          this.m_CreatedObject.Value = destination;
        this.Finish();
      }
    }
  }
}
