﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetChildCount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Gets the number of children that a GameObject has.")]
  public class GetChildCount : FsmStateAction
  {
    [Tooltip("The GameObject to test.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the number of children in an int variable.")]
    [RequiredField]
    public FsmInt storeResult;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.storeResult = (FsmInt) null;
    }

    public override void OnEnter()
    {
      this.DoGetChildCount();
      this.Finish();
    }

    private void DoGetChildCount()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      this.storeResult.Value = ownerDefaultTarget.transform.childCount;
    }
  }
}
