﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellFinishAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Tells the game that a Spell is finished, allowing the game to progress.")]
  public class SpellFinishAction : SpellAction
  {
    public FsmFloat m_Delay = (FsmFloat) 0.0f;
    public FsmOwnerDefault m_SpellObject;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_spell == (Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("{0}.OnEnter() - FAILED to find Spell component on Owner \"{1}\"", (object) this, (object) this.Owner));
      }
      else
      {
        if ((double) this.m_Delay.Value > 0.0)
          this.m_spell.StartCoroutine(this.DelaySpellFinished());
        else
          this.m_spell.OnSpellFinished();
        this.Finish();
      }
    }

    [DebuggerHidden]
    private IEnumerator DelaySpellFinished()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SpellFinishAction.\u003CDelaySpellFinished\u003Ec__Iterator298() { \u003C\u003Ef__this = this };
    }
  }
}
