﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("INTERNAL USE ONLY. Do not put this on your FSMs.")]
  [ActionCategory("Pegasus")]
  public abstract class SpellAction : FsmStateAction
  {
    protected Spell m_spell;

    public Spell GetSpell()
    {
      if ((UnityEngine.Object) this.m_spell == (UnityEngine.Object) null)
      {
        GameObject spellOwner = this.GetSpellOwner();
        if ((UnityEngine.Object) spellOwner != (UnityEngine.Object) null)
          this.m_spell = SceneUtils.FindComponentInThisOrParents<Spell>(spellOwner);
      }
      return this.m_spell;
    }

    public Card GetCard(SpellAction.Which which)
    {
      Spell spell = this.GetSpell();
      if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
        return (Card) null;
      if (which == SpellAction.Which.TARGET)
        return spell.GetTargetCard();
      Card sourceCard = spell.GetSourceCard();
      if (which == SpellAction.Which.SOURCE_HERO && (UnityEngine.Object) sourceCard != (UnityEngine.Object) null)
        return sourceCard.GetHeroCard();
      if (which == SpellAction.Which.CHOSEN_TARGET)
      {
        Card powerTargetCard = spell.GetPowerTargetCard();
        if ((UnityEngine.Object) powerTargetCard != (UnityEngine.Object) null)
          return powerTargetCard;
      }
      if (which == SpellAction.Which.SOURCE_PLAYER)
      {
        Log.All.PrintError("{0} cannot get card for source player: players are not cards. Did you mean to choose SOURCE_HERO?", (object) this);
        if ((UnityEngine.Object) sourceCard != (UnityEngine.Object) null)
          return sourceCard.GetHeroCard();
      }
      return sourceCard;
    }

    public Entity GetEntity(SpellAction.Which which)
    {
      Spell spell = this.GetSpell();
      if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
        return (Entity) null;
      if (which == SpellAction.Which.TARGET)
        return spell.GetTargetCard().GetEntity();
      Card sourceCard = spell.GetSourceCard();
      if (which == SpellAction.Which.SOURCE_HERO && (UnityEngine.Object) sourceCard != (UnityEngine.Object) null)
        return sourceCard.GetHero();
      if (which == SpellAction.Which.SOURCE_PLAYER && (UnityEngine.Object) sourceCard != (UnityEngine.Object) null)
        return (Entity) sourceCard.GetController();
      if (which == SpellAction.Which.CHOSEN_TARGET)
      {
        Card powerTargetCard = spell.GetPowerTargetCard();
        if ((UnityEngine.Object) powerTargetCard != (UnityEngine.Object) null)
          return powerTargetCard.GetEntity();
      }
      return sourceCard.GetEntity();
    }

    public Actor GetActor(SpellAction.Which which)
    {
      Card card = this.GetCard(which);
      if ((UnityEngine.Object) card == (UnityEngine.Object) null)
        return (Actor) null;
      return card.GetActor();
    }

    public int GetIndexMatchingCardId(string cardId, string[] cardIds)
    {
      if (cardIds == null || cardIds.Length == 0)
        return -1;
      for (int index = 0; index < cardIds.Length; ++index)
      {
        string str = cardIds[index].Trim();
        if (cardId.Equals(str, StringComparison.OrdinalIgnoreCase))
          return index;
      }
      return -1;
    }

    protected abstract GameObject GetSpellOwner();

    public override void OnEnter()
    {
      this.GetSpell();
      if (!((UnityEngine.Object) this.m_spell == (UnityEngine.Object) null))
        return;
      Debug.LogError((object) string.Format("{0}.OnEnter() - FAILED to find Spell component on Owner \"{1}\"", (object) this, (object) this.Owner));
    }

    public enum Which
    {
      SOURCE,
      TARGET,
      SOURCE_HERO,
      CHOSEN_TARGET,
      SOURCE_PLAYER,
    }
  }
}
