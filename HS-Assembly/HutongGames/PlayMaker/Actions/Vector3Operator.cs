﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector3Operator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Vector3)]
  [Tooltip("Performs most possible operations on 2 Vector3: Dot product, Cross product, Distance, Angle, Project, Reflect, Add, Subtract, Multiply, Divide, Min, Max")]
  public class Vector3Operator : FsmStateAction
  {
    public Vector3Operator.Vector3Operation operation = Vector3Operator.Vector3Operation.Add;
    [RequiredField]
    public FsmVector3 vector1;
    [RequiredField]
    public FsmVector3 vector2;
    [UIHint(UIHint.Variable)]
    public FsmVector3 storeVector3Result;
    [UIHint(UIHint.Variable)]
    public FsmFloat storeFloatResult;
    public bool everyFrame;

    public override void Reset()
    {
      this.vector1 = (FsmVector3) null;
      this.vector2 = (FsmVector3) null;
      this.operation = Vector3Operator.Vector3Operation.Add;
      this.storeVector3Result = (FsmVector3) null;
      this.storeFloatResult = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoVector3Operator();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoVector3Operator();
    }

    private void DoVector3Operator()
    {
      Vector3 vector3_1 = this.vector1.Value;
      Vector3 vector3_2 = this.vector2.Value;
      switch (this.operation)
      {
        case Vector3Operator.Vector3Operation.DotProduct:
          this.storeFloatResult.Value = Vector3.Dot(vector3_1, vector3_2);
          break;
        case Vector3Operator.Vector3Operation.CrossProduct:
          this.storeVector3Result.Value = Vector3.Cross(vector3_1, vector3_2);
          break;
        case Vector3Operator.Vector3Operation.Distance:
          this.storeFloatResult.Value = Vector3.Distance(vector3_1, vector3_2);
          break;
        case Vector3Operator.Vector3Operation.Angle:
          this.storeFloatResult.Value = Vector3.Angle(vector3_1, vector3_2);
          break;
        case Vector3Operator.Vector3Operation.Project:
          this.storeVector3Result.Value = Vector3.Project(vector3_1, vector3_2);
          break;
        case Vector3Operator.Vector3Operation.Reflect:
          this.storeVector3Result.Value = Vector3.Reflect(vector3_1, vector3_2);
          break;
        case Vector3Operator.Vector3Operation.Add:
          this.storeVector3Result.Value = vector3_1 + vector3_2;
          break;
        case Vector3Operator.Vector3Operation.Subtract:
          this.storeVector3Result.Value = vector3_1 - vector3_2;
          break;
        case Vector3Operator.Vector3Operation.Multiply:
          Vector3 zero1 = Vector3.zero;
          zero1.x = vector3_1.x * vector3_2.x;
          zero1.y = vector3_1.y * vector3_2.y;
          zero1.z = vector3_1.z * vector3_2.z;
          this.storeVector3Result.Value = zero1;
          break;
        case Vector3Operator.Vector3Operation.Divide:
          Vector3 zero2 = Vector3.zero;
          zero2.x = vector3_1.x / vector3_2.x;
          zero2.y = vector3_1.y / vector3_2.y;
          zero2.z = vector3_1.z / vector3_2.z;
          this.storeVector3Result.Value = zero2;
          break;
        case Vector3Operator.Vector3Operation.Min:
          this.storeVector3Result.Value = Vector3.Min(vector3_1, vector3_2);
          break;
        case Vector3Operator.Vector3Operation.Max:
          this.storeVector3Result.Value = Vector3.Max(vector3_1, vector3_2);
          break;
      }
    }

    public enum Vector3Operation
    {
      DotProduct,
      CrossProduct,
      Distance,
      Angle,
      Project,
      Reflect,
      Add,
      Subtract,
      Multiply,
      Divide,
      Min,
      Max,
    }
  }
}
