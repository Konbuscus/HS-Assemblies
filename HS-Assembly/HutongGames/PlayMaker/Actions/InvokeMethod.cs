﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.InvokeMethod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.ScriptControl)]
  [Tooltip("Invokes a Method in a Behaviour attached to a Game Object. See Unity InvokeMethod docs.")]
  public class InvokeMethod : FsmStateAction
  {
    [Tooltip("The game object that owns the behaviour.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Script)]
    [RequiredField]
    [Tooltip("The behaviour that contains the method.")]
    public FsmString behaviour;
    [UIHint(UIHint.Method)]
    [RequiredField]
    [Tooltip("The name of the method to invoke.")]
    public FsmString methodName;
    [Tooltip("Optional time delay in seconds.")]
    [HasFloatSlider(0.0f, 10f)]
    public FsmFloat delay;
    [Tooltip("Call the method repeatedly.")]
    public FsmBool repeating;
    [HasFloatSlider(0.0f, 10f)]
    [Tooltip("Delay between repeated calls in seconds.")]
    public FsmFloat repeatDelay;
    [Tooltip("Stop calling the method when the state is exited.")]
    public FsmBool cancelOnExit;
    private MonoBehaviour component;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.behaviour = (FsmString) null;
      this.methodName = (FsmString) string.Empty;
      this.delay = (FsmFloat) null;
      this.repeating = (FsmBool) false;
      this.repeatDelay = (FsmFloat) 1f;
      this.cancelOnExit = (FsmBool) false;
    }

    public override void OnEnter()
    {
      this.DoInvokeMethod(this.Fsm.GetOwnerDefaultTarget(this.gameObject));
      this.Finish();
    }

    private void DoInvokeMethod(GameObject go)
    {
      if ((Object) go == (Object) null)
        return;
      this.component = go.GetComponent(ReflectionUtils.GetGlobalType(this.behaviour.Value)) as MonoBehaviour;
      if ((Object) this.component == (Object) null)
        this.LogWarning("InvokeMethod: " + go.name + " missing behaviour: " + this.behaviour.Value);
      else if (this.repeating.Value)
        this.component.InvokeRepeating(this.methodName.Value, this.delay.Value, this.repeatDelay.Value);
      else
        this.component.Invoke(this.methodName.Value, this.delay.Value);
    }

    public override void OnExit()
    {
      if ((Object) this.component == (Object) null || !this.cancelOnExit.Value)
        return;
      this.component.CancelInvoke(this.methodName.Value);
    }
  }
}
