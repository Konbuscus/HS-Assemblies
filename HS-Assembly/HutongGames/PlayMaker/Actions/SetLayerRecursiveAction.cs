﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetLayerRecursiveAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Sets the layer on a game object and its children.")]
  public class SetLayerRecursiveAction : FsmStateAction
  {
    private Map<GameObject, GameLayer> m_initialLayer = new Map<GameObject, GameLayer>();
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Layer number")]
    public GameLayer layer;
    [Tooltip("Resets to the initial layer once\nit leaves the state")]
    public bool resetOnExit;
    public bool includeChildren;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.layer = GameLayer.Default;
      this.resetOnExit = true;
      this.includeChildren = false;
      this.m_initialLayer.Clear();
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        if (this.includeChildren)
        {
          Transform[] componentsInChildren = ownerDefaultTarget.GetComponentsInChildren<Transform>();
          if (componentsInChildren != null)
          {
            foreach (Transform transform in componentsInChildren)
            {
              this.m_initialLayer[transform.gameObject] = (GameLayer) transform.gameObject.layer;
              transform.gameObject.layer = (int) this.layer;
            }
          }
        }
        else
        {
          Transform component = ownerDefaultTarget.GetComponent<Transform>();
          if ((Object) component != (Object) null)
          {
            this.m_initialLayer[component.gameObject] = (GameLayer) component.gameObject.layer;
            component.gameObject.layer = (int) this.layer;
          }
        }
        this.Finish();
      }
    }

    public override void OnExit()
    {
      if (!this.resetOnExit)
        return;
      using (Map<GameObject, GameLayer>.Enumerator enumerator = this.m_initialLayer.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<GameObject, GameLayer> current = enumerator.Current;
          GameObject key = current.Key;
          if (!((Object) key == (Object) null))
            key.layer = (int) current.Value;
        }
      }
    }
  }
}
