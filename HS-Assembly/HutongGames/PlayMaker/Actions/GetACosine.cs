﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetACosine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Trigonometry)]
  [Tooltip("Get the Arc Cosine. You can get the result in degrees, simply check on the RadToDeg conversion")]
  public class GetACosine : FsmStateAction
  {
    [Tooltip("The value of the cosine")]
    [RequiredField]
    public FsmFloat Value;
    [Tooltip("The resulting angle. Note:If you want degrees, simply check RadToDeg")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmFloat angle;
    [Tooltip("Check on if you want the angle expressed in degrees.")]
    public FsmBool RadToDeg;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.angle = (FsmFloat) null;
      this.RadToDeg = (FsmBool) true;
      this.everyFrame = false;
      this.Value = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      this.DoACosine();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoACosine();
    }

    private void DoACosine()
    {
      float num = Mathf.Acos(this.Value.Value);
      if (this.RadToDeg.Value)
        num *= 57.29578f;
      this.angle.Value = num;
    }
  }
}
