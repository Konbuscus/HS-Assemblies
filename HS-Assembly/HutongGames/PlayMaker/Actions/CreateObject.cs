﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.CreateObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Creates a Game Object, usually using a Prefab.")]
  [ActionTarget(typeof (GameObject), "gameObject", true)]
  public class CreateObject : FsmStateAction
  {
    [RequiredField]
    [Tooltip("GameObject to create. Usually a Prefab.")]
    public FsmGameObject gameObject;
    [Tooltip("Optional Spawn Point.")]
    public FsmGameObject spawnPoint;
    [Tooltip("Position. If a Spawn Point is defined, this is used as a local offset from the Spawn Point position.")]
    public FsmVector3 position;
    [Tooltip("Rotation. NOTE: Overrides the rotation of the Spawn Point.")]
    public FsmVector3 rotation;
    [UIHint(UIHint.Variable)]
    [Tooltip("Optionally store the created object.")]
    public FsmGameObject storeObject;
    [Tooltip("Use Network.Instantiate to create a Game Object on all clients in a networked game.")]
    public FsmBool networkInstantiate;
    [Tooltip("Usually 0. The group number allows you to group together network messages which allows you to filter them if so desired.")]
    public FsmInt networkGroup;

    public override void Reset()
    {
      this.gameObject = (FsmGameObject) null;
      this.spawnPoint = (FsmGameObject) null;
      FsmVector3 fsmVector3_1 = new FsmVector3();
      fsmVector3_1.UseVariable = true;
      this.position = fsmVector3_1;
      FsmVector3 fsmVector3_2 = new FsmVector3();
      fsmVector3_2.UseVariable = true;
      this.rotation = fsmVector3_2;
      this.storeObject = (FsmGameObject) null;
      this.networkInstantiate = (FsmBool) false;
      this.networkGroup = (FsmInt) 0;
    }

    public override void OnEnter()
    {
      GameObject gameObject = this.gameObject.Value;
      if ((Object) gameObject != (Object) null)
      {
        Vector3 position = Vector3.zero;
        Vector3 euler = Vector3.zero;
        if ((Object) this.spawnPoint.Value != (Object) null)
        {
          position = this.spawnPoint.Value.transform.position;
          if (!this.position.IsNone)
            position += this.position.Value;
          euler = this.rotation.IsNone ? this.spawnPoint.Value.transform.eulerAngles : this.rotation.Value;
        }
        else
        {
          if (!this.position.IsNone)
            position = this.position.Value;
          if (!this.rotation.IsNone)
            euler = this.rotation.Value;
        }
        this.storeObject.Value = (GameObject) Object.Instantiate((Object) gameObject, position, Quaternion.Euler(euler));
      }
      this.Finish();
    }
  }
}
