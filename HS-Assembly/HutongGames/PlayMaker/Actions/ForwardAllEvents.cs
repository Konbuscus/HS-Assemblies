﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ForwardAllEvents
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.StateMachine)]
  [Tooltip("Forwards all event received by this FSM to another target. Optionally specify a list of events to ignore.")]
  public class ForwardAllEvents : FsmStateAction
  {
    [Tooltip("Forward to this target.")]
    public FsmEventTarget forwardTo;
    [Tooltip("Don't forward these events.")]
    public FsmEvent[] exceptThese;
    [Tooltip("Should this action eat the events or pass them on.")]
    public bool eatEvents;

    public override void Reset()
    {
      this.forwardTo = new FsmEventTarget()
      {
        target = FsmEventTarget.EventTarget.FSMComponent
      };
      this.exceptThese = new FsmEvent[1]
      {
        FsmEvent.Finished
      };
      this.eatEvents = true;
    }

    public override bool Event(FsmEvent fsmEvent)
    {
      if (this.exceptThese != null)
      {
        foreach (FsmEvent fsmEvent1 in this.exceptThese)
        {
          if (fsmEvent1 == fsmEvent)
            return false;
        }
      }
      this.Fsm.Event(this.forwardTo, fsmEvent);
      return this.eatEvents;
    }
  }
}
