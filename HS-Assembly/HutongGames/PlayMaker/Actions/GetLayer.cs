﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetLayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Gets a Game Object's Layer and stores it in an Int Variable.")]
  public class GetLayer : FsmStateAction
  {
    [RequiredField]
    public FsmGameObject gameObject;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmInt storeResult;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmGameObject) null;
      this.storeResult = (FsmInt) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetLayer();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetLayer();
    }

    private void DoGetLayer()
    {
      if ((Object) this.gameObject.Value == (Object) null)
        return;
      this.storeResult.Value = this.gameObject.Value.layer;
    }
  }
}
