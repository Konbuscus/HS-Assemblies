﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.BaseAnimationAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  public abstract class BaseAnimationAction : ComponentAction<Animation>
  {
    public override void OnActionTargetInvoked(object targetObject)
    {
      AnimationClip clip = targetObject as AnimationClip;
      if ((Object) clip == (Object) null)
        return;
      Animation component = this.Owner.GetComponent<Animation>();
      if (!((Object) component != (Object) null))
        return;
      component.AddClip(clip, clip.name);
    }
  }
}
