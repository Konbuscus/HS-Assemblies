﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DestroyComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Destroys a Component of an Object.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class DestroyComponent : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject that owns the Component.")]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.ScriptComponent)]
    [Tooltip("The name of the Component to destroy.")]
    [RequiredField]
    public FsmString component;
    private Component aComponent;

    public override void Reset()
    {
      this.aComponent = (Component) null;
      this.gameObject = (FsmOwnerDefault) null;
      this.component = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.DoDestroyComponent(this.gameObject.OwnerOption != OwnerDefaultOption.UseOwner ? this.gameObject.GameObject.Value : this.Owner);
      this.Finish();
    }

    private void DoDestroyComponent(GameObject go)
    {
      this.aComponent = go.GetComponent(ReflectionUtils.GetGlobalType(this.component.Value));
      if ((Object) this.aComponent == (Object) null)
        this.LogError("No such component: " + this.component.Value);
      else
        Object.Destroy((Object) this.aComponent);
    }
  }
}
