﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetLightCookie
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Lights)]
  [Tooltip("Sets the Texture projected by a Light.")]
  public class SetLightCookie : ComponentAction<Light>
  {
    [CheckForComponent(typeof (Light))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    public FsmTexture lightCookie;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.lightCookie = (FsmTexture) null;
    }

    public override void OnEnter()
    {
      this.DoSetLightCookie();
      this.Finish();
    }

    private void DoSetLightCookie()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.light.cookie = this.lightCookie.Value;
    }
  }
}
