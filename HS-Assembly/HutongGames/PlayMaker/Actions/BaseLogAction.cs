﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.BaseLogAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  public abstract class BaseLogAction : FsmStateAction
  {
    public bool sendToUnityLog;

    public override void Reset()
    {
      this.sendToUnityLog = false;
    }
  }
}
