﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the playback speed of the Animator. 1 is normal playback speed")]
  [ActionCategory(ActionCategory.Animator)]
  public class SetAnimatorPlayBackSpeed : FsmStateAction
  {
    [Tooltip("The Target. An Animator component is required")]
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("If true, automaticly stabilize feet during transition and blending")]
    public FsmFloat playBackSpeed;
    [Tooltip("Repeat every frame. Useful for changing over time.")]
    public bool everyFrame;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.playBackSpeed = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoPlayBackSpeed();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnUpdate()
    {
      this.DoPlayBackSpeed();
    }

    private void DoPlayBackSpeed()
    {
      if ((Object) this._animator == (Object) null)
        return;
      this._animator.speed = this.playBackSpeed.Value;
    }
  }
}
