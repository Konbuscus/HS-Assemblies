﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.TGTGrandStandAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Used to control TGT Grand Stands")]
  public class TGTGrandStandAction : FsmStateAction
  {
    [RequiredField]
    public TGTGrandStandAction.EMOTE m_emote;
    protected Actor m_actor;

    public void PlayEmote(TGTGrandStandAction.EMOTE emote)
    {
      TGTGrandStand tgtGrandStand = TGTGrandStand.Get();
      if ((Object) tgtGrandStand == (Object) null)
        return;
      switch (emote)
      {
        case TGTGrandStandAction.EMOTE.Cheer:
          tgtGrandStand.PlayCheerAnimation();
          break;
        case TGTGrandStandAction.EMOTE.OhNo:
          tgtGrandStand.PlayOhNoAnimation();
          break;
      }
    }

    public override void Reset()
    {
      this.m_emote = TGTGrandStandAction.EMOTE.Cheer;
    }

    public override void OnEnter()
    {
      this.PlayEmote(this.m_emote);
    }

    public enum EMOTE
    {
      Cheer,
      OhNo,
    }
  }
}
