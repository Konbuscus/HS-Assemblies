﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.LookAt2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Transform)]
  [Tooltip("Rotates a 2d Game Object on it's z axis so its forward vector points at a 2d or 3d position.")]
  public class LookAt2d : FsmStateAction
  {
    [Tooltip("Repeat every frame.")]
    public bool everyFrame = true;
    [RequiredField]
    [Tooltip("The GameObject to rotate.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The 2d position to Look At.")]
    public FsmVector2 vector2Target;
    [Tooltip("The 3d position to Look At. If not set to none, will be added to the 2d target")]
    public FsmVector3 vector3Target;
    [Tooltip("Set the GameObject starting offset. In degrees. 0 if your object is facing right, 180 if facing left etc...")]
    public FsmFloat rotationOffset;
    [Title("Draw Debug Line")]
    [Tooltip("Draw a debug line from the GameObject to the Target.")]
    public FsmBool debug;
    [Tooltip("Color to use for the debug line.")]
    public FsmColor debugLineColor;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.vector2Target = (FsmVector2) null;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.vector3Target = fsmVector3;
      this.debug = (FsmBool) false;
      this.debugLineColor = (FsmColor) Color.green;
      this.everyFrame = true;
    }

    public override void OnEnter()
    {
      this.DoLookAt();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoLookAt();
    }

    private void DoLookAt()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      Vector3 end = new Vector3(this.vector2Target.Value.x, this.vector2Target.Value.y, 0.0f);
      if (!this.vector3Target.IsNone)
        end += this.vector3Target.Value;
      Vector3 vector3 = end - ownerDefaultTarget.transform.position;
      vector3.Normalize();
      float num = Mathf.Atan2(vector3.y, vector3.x) * 57.29578f;
      ownerDefaultTarget.transform.rotation = Quaternion.Euler(0.0f, 0.0f, num - this.rotationOffset.Value);
      if (!this.debug.Value)
        return;
      Debug.DrawLine(ownerDefaultTarget.transform.position, end, this.debugLineColor.Value);
    }
  }
}
