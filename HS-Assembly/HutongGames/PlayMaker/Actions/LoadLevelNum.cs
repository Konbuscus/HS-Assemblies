﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.LoadLevelNum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.SceneManagement;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Loads a Level by Index number. Before you can load a level, you have to add it to the list of levels defined in File->Build Settings...")]
  [ActionCategory(ActionCategory.Level)]
  public class LoadLevelNum : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The level index in File->Build Settings")]
    public FsmInt levelIndex;
    [Tooltip("Load the level additively, keeping the current scene.")]
    public bool additive;
    [Tooltip("Event to send after the level is loaded.")]
    public FsmEvent loadedEvent;
    [Tooltip("Keep this GameObject in the new level. NOTE: The GameObject and components is disabled then enabled on load; uncheck Reset On Disable to keep the active state.")]
    public FsmBool dontDestroyOnLoad;

    public override void Reset()
    {
      this.levelIndex = (FsmInt) null;
      this.additive = false;
      this.loadedEvent = (FsmEvent) null;
      this.dontDestroyOnLoad = (FsmBool) false;
    }

    public override void OnEnter()
    {
      if (this.dontDestroyOnLoad.Value)
        Object.DontDestroyOnLoad((Object) this.Owner.transform.root.gameObject);
      if (this.additive)
        SceneManager.LoadScene(this.levelIndex.Value, LoadSceneMode.Additive);
      else
        SceneManager.LoadScene(this.levelIndex.Value, LoadSceneMode.Single);
      this.Fsm.Event(this.loadedEvent);
      this.Finish();
    }
  }
}
