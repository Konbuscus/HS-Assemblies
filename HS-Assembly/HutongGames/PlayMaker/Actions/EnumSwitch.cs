﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.EnumSwitch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sends an Event based on the value of an Enum Variable.")]
  [ActionCategory(ActionCategory.Logic)]
  public class EnumSwitch : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmEnum enumVariable;
    [CompoundArray("Enum Switches", "Compare Enum Values", "Send")]
    [MatchFieldType("enumVariable")]
    public FsmEnum[] compareTo;
    public FsmEvent[] sendEvent;
    public bool everyFrame;

    public override void Reset()
    {
      this.enumVariable = (FsmEnum) null;
      this.compareTo = new FsmEnum[0];
      this.sendEvent = new FsmEvent[0];
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoEnumSwitch();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoEnumSwitch();
    }

    private void DoEnumSwitch()
    {
      if (this.enumVariable.IsNone)
        return;
      for (int index = 0; index < this.compareTo.Length; ++index)
      {
        if (object.Equals((object) this.enumVariable.get_Value(), (object) this.compareTo[index].get_Value()))
        {
          this.Fsm.Event(this.sendEvent[index]);
          break;
        }
      }
    }
  }
}
