﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetVisibilityRecursiveAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the visibility on a game object and its children.")]
  [ActionCategory("Pegasus")]
  public class SetVisibilityRecursiveAction : FsmStateAction
  {
    private Dictionary<Renderer, bool> m_initialVisibility = new Dictionary<Renderer, bool>();
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Should the objects be set to visible or invisible?")]
    public FsmBool visible;
    [Tooltip("Resets to the initial visibility once\nit leaves the state")]
    public bool resetOnExit;
    public bool includeChildren;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.visible = (FsmBool) false;
      this.resetOnExit = true;
      this.includeChildren = false;
      this.m_initialVisibility.Clear();
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        if (this.includeChildren)
        {
          Renderer[] componentsInChildren = ownerDefaultTarget.GetComponentsInChildren<Renderer>();
          if (componentsInChildren != null)
          {
            foreach (Renderer index in componentsInChildren)
            {
              this.m_initialVisibility[index] = index.enabled;
              index.enabled = this.visible.Value;
            }
          }
        }
        else
        {
          Renderer component = ownerDefaultTarget.GetComponent<Renderer>();
          if ((Object) component != (Object) null)
          {
            this.m_initialVisibility[component] = component.enabled;
            component.enabled = this.visible.Value;
          }
        }
        this.Finish();
      }
    }

    public override void OnExit()
    {
      if (!this.resetOnExit)
        return;
      using (Dictionary<Renderer, bool>.Enumerator enumerator = this.m_initialVisibility.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<Renderer, bool> current = enumerator.Current;
          Renderer key = current.Key;
          if (!((Object) key == (Object) null))
            key.enabled = current.Value;
        }
      }
    }
  }
}
