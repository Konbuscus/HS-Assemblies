﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetRectValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the value of a Rect Variable.")]
  [ActionCategory(ActionCategory.Rect)]
  public class SetRectValue : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmRect rectVariable;
    [RequiredField]
    public FsmRect rectValue;
    public bool everyFrame;

    public override void Reset()
    {
      this.rectVariable = (FsmRect) null;
      this.rectValue = (FsmRect) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.rectVariable.Value = this.rectValue.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.rectVariable.Value = this.rectValue.Value;
    }
  }
}
