﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector3RotateTowards
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Rotates a Vector3 direction from Current towards Target.")]
  [ActionCategory(ActionCategory.Vector3)]
  public class Vector3RotateTowards : FsmStateAction
  {
    [RequiredField]
    public FsmVector3 currentDirection;
    [RequiredField]
    public FsmVector3 targetDirection;
    [RequiredField]
    [Tooltip("Rotation speed in degrees per second")]
    public FsmFloat rotateSpeed;
    [Tooltip("Max Magnitude per second")]
    [RequiredField]
    public FsmFloat maxMagnitude;

    public override void Reset()
    {
      FsmVector3 fsmVector3_1 = new FsmVector3();
      fsmVector3_1.UseVariable = true;
      this.currentDirection = fsmVector3_1;
      FsmVector3 fsmVector3_2 = new FsmVector3();
      fsmVector3_2.UseVariable = true;
      this.targetDirection = fsmVector3_2;
      this.rotateSpeed = (FsmFloat) 360f;
      this.maxMagnitude = (FsmFloat) 1f;
    }

    public override void OnUpdate()
    {
      this.currentDirection.Value = Vector3.RotateTowards(this.currentDirection.Value, this.targetDirection.Value, this.rotateSpeed.Value * ((float) Math.PI / 180f) * Time.deltaTime, this.maxMagnitude.Value);
    }
  }
}
