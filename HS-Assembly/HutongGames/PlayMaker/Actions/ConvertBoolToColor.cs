﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ConvertBoolToColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Convert)]
  [Tooltip("Converts a Bool value to a Color.")]
  public class ConvertBoolToColor : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("The Bool variable to test.")]
    public FsmBool boolVariable;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("The Color variable to set based on the bool variable value.")]
    public FsmColor colorVariable;
    [Tooltip("Color if Bool variable is false.")]
    public FsmColor falseColor;
    [Tooltip("Color if Bool variable is true.")]
    public FsmColor trueColor;
    [Tooltip("Repeat every frame. Useful if the Bool variable is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.boolVariable = (FsmBool) null;
      this.colorVariable = (FsmColor) null;
      this.falseColor = (FsmColor) Color.black;
      this.trueColor = (FsmColor) Color.white;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoConvertBoolToColor();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoConvertBoolToColor();
    }

    private void DoConvertBoolToColor()
    {
      this.colorVariable.Value = !this.boolVariable.Value ? this.falseColor.Value : this.trueColor.Value;
    }
  }
}
