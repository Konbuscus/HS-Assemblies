﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PlayMovieTexture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Plays a Movie Texture. Use the Movie Texture in a Material, or in the GUI.")]
  [ActionCategory(ActionCategory.Movie)]
  public class PlayMovieTexture : FsmStateAction
  {
    [RequiredField]
    [ObjectType(typeof (MovieTexture))]
    public FsmObject movieTexture;
    public FsmBool loop;

    public override void Reset()
    {
      this.movieTexture = (FsmObject) null;
      this.loop = (FsmBool) false;
    }

    public override void OnEnter()
    {
      MovieTexture movieTexture = this.movieTexture.Value as MovieTexture;
      if ((Object) movieTexture != (Object) null)
      {
        movieTexture.loop = this.loop.Value;
        movieTexture.Play();
      }
      this.Finish();
    }
  }
}
