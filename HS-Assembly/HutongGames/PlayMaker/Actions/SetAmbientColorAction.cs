﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAmbientColorAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set scene ambient color")]
  [ActionCategory("Pegasus")]
  public class SetAmbientColorAction : FsmStateAction
  {
    public FsmColor m_Color;
    public bool m_EveryFrame;

    public override void Reset()
    {
      this.m_Color = (FsmColor) null;
      this.m_EveryFrame = false;
    }

    public override void OnEnter()
    {
      RenderSettings.ambientLight = this.m_Color.Value;
      if (this.m_EveryFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      RenderSettings.ambientLight = this.m_Color.Value;
    }
  }
}
