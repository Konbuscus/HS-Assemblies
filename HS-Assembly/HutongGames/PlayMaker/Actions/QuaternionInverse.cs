﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.QuaternionInverse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Inverse a quaternion")]
  [ActionCategory(ActionCategory.Quaternion)]
  public class QuaternionInverse : QuaternionBaseAction
  {
    [Tooltip("the rotation")]
    [RequiredField]
    public FsmQuaternion rotation;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("Store the inverse of the rotation variable.")]
    public FsmQuaternion result;

    public override void Reset()
    {
      this.rotation = (FsmQuaternion) null;
      this.result = (FsmQuaternion) null;
      this.everyFrame = true;
      this.everyFrameOption = QuaternionBaseAction.everyFrameOptions.Update;
    }

    public override void OnEnter()
    {
      this.DoQuatInverse();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.Update)
        return;
      this.DoQuatInverse();
    }

    public override void OnLateUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.LateUpdate)
        return;
      this.DoQuatInverse();
    }

    public override void OnFixedUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.FixedUpdate)
        return;
      this.DoQuatInverse();
    }

    private void DoQuatInverse()
    {
      this.result.Value = Quaternion.Inverse(this.rotation.Value);
    }
  }
}
