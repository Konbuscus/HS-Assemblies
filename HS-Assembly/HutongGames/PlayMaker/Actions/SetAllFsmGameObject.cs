﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAllFsmGameObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set the value of a Game Object Variable in another All FSM. Accept null reference")]
  [ActionCategory(ActionCategory.StateMachine)]
  public class SetAllFsmGameObject : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault gameObject;
    public bool everyFrame;

    public override void Reset()
    {
    }

    public override void OnEnter()
    {
      if (this.everyFrame)
        return;
      this.Finish();
    }

    private void DoSetFsmGameObject()
    {
    }
  }
}
