﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ControllerSimpleMove
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Moves a Game Object with a Character Controller. Velocity along the y-axis is ignored. Speed is in meters/s. Gravity is automatically applied.")]
  [ActionCategory(ActionCategory.Character)]
  public class ControllerSimpleMove : FsmStateAction
  {
    [Tooltip("The GameObject to move.")]
    [RequiredField]
    [CheckForComponent(typeof (CharacterController))]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The movement vector.")]
    public FsmVector3 moveVector;
    [Tooltip("Multiply the movement vector by a speed factor.")]
    public FsmFloat speed;
    [Tooltip("Move in local or word space.")]
    public Space space;
    private GameObject previousGo;
    private CharacterController controller;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.moveVector = fsmVector3;
      this.speed = (FsmFloat) 1f;
      this.space = Space.World;
    }

    public override void OnUpdate()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((Object) ownerDefaultTarget != (Object) this.previousGo)
      {
        this.controller = ownerDefaultTarget.GetComponent<CharacterController>();
        this.previousGo = ownerDefaultTarget;
      }
      if (!((Object) this.controller != (Object) null))
        return;
      this.controller.SimpleMove((this.space != Space.World ? ownerDefaultTarget.transform.TransformDirection(this.moveVector.Value) : this.moveVector.Value) * this.speed.Value);
    }
  }
}
