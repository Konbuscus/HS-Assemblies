﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.IsFixedAngle2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Obsolete("This action is obsolete; use Constraints instead.")]
  [ActionCategory(ActionCategory.Physics2D)]
  [Tooltip("Should the rigidbody2D be prevented from rotating?")]
  public class IsFixedAngle2d : ComponentAction<Rigidbody2D>
  {
    [Tooltip("The GameObject with the Rigidbody2D attached")]
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody2D))]
    public FsmOwnerDefault gameObject;
    [Tooltip("Event sent if the Rigidbody2D does have fixed angle")]
    public FsmEvent trueEvent;
    [Tooltip("Event sent if the Rigidbody2D doesn't have fixed angle")]
    public FsmEvent falseEvent;
    [Tooltip("Store the fixedAngle flag")]
    [UIHint(UIHint.Variable)]
    public FsmBool store;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.store = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoIsFixedAngle();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoIsFixedAngle();
    }

    private void DoIsFixedAngle()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      bool fixedAngle = this.rigidbody2d.fixedAngle;
      this.store.Value = fixedAngle;
      this.Fsm.Event(!fixedAngle ? this.falseEvent : this.trueEvent);
    }
  }
}
