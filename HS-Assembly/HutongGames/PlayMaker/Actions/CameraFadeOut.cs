﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.CameraFadeOut
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Camera)]
  [Tooltip("Fade to a fullscreen Color. NOTE: Uses OnGUI so requires a PlayMakerGUI component in the scene.")]
  public class CameraFadeOut : FsmStateAction
  {
    [RequiredField]
    [Tooltip("Color to fade to. E.g., Fade to black.")]
    public FsmColor color;
    [Tooltip("Fade out time in seconds.")]
    [HasFloatSlider(0.0f, 10f)]
    [RequiredField]
    public FsmFloat time;
    [Tooltip("Event to send when finished.")]
    public FsmEvent finishEvent;
    [Tooltip("Ignore TimeScale. Useful if the game is paused.")]
    public bool realTime;
    private float startTime;
    private float currentTime;
    private Color colorLerp;

    public override void Reset()
    {
      this.color = (FsmColor) Color.black;
      this.time = (FsmFloat) 1f;
      this.finishEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      this.startTime = FsmTime.RealtimeSinceStartup;
      this.currentTime = 0.0f;
      this.colorLerp = Color.clear;
    }

    public override void OnUpdate()
    {
      if (this.realTime)
        this.currentTime = FsmTime.RealtimeSinceStartup - this.startTime;
      else
        this.currentTime += Time.deltaTime;
      this.colorLerp = Color.Lerp(Color.clear, this.color.Value, this.currentTime / this.time.Value);
      if ((double) this.currentTime <= (double) this.time.Value || this.finishEvent == null)
        return;
      this.Fsm.Event(this.finishEvent);
    }

    public override void OnGUI()
    {
      Color color = GUI.color;
      GUI.color = this.colorLerp;
      GUI.DrawTexture(new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height), (Texture) ActionHelpers.WhiteTexture);
      GUI.color = color;
    }
  }
}
