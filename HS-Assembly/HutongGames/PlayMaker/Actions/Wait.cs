﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Wait
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Delays a State from finishing by the specified time. NOTE: Other actions continue, but FINISHED can't happen before Time.")]
  [ActionCategory(ActionCategory.Time)]
  public class Wait : FsmStateAction
  {
    [RequiredField]
    public FsmFloat time;
    public FsmEvent finishEvent;
    public bool realTime;
    private float startTime;
    private float timer;

    public override void Reset()
    {
      this.time = (FsmFloat) 1f;
      this.finishEvent = (FsmEvent) null;
      this.realTime = false;
    }

    public override void OnEnter()
    {
      if ((double) this.time.Value <= 0.0)
      {
        this.Fsm.Event(this.finishEvent);
        this.Finish();
      }
      else
      {
        this.startTime = FsmTime.RealtimeSinceStartup;
        this.timer = 0.0f;
      }
    }

    public override void OnUpdate()
    {
      if (this.realTime)
        this.timer = FsmTime.RealtimeSinceStartup - this.startTime;
      else
        this.timer += Time.deltaTime;
      if ((double) this.timer < (double) this.time.Value)
        return;
      this.Finish();
      if (this.finishEvent == null)
        return;
      this.Fsm.Event(this.finishEvent);
    }
  }
}
