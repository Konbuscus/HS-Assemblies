﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetOwner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Gets the Game Object that owns the FSM and stores it in a game object variable.")]
  public class GetOwner : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmGameObject storeGameObject;

    public override void Reset()
    {
      this.storeGameObject = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      this.storeGameObject.Value = this.Owner;
      this.Finish();
    }
  }
}
