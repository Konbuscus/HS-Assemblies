﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("If true, automaticaly stabilize feet during transition and blending")]
  [ActionCategory(ActionCategory.Animator)]
  public class SetAnimatorStabilizeFeet : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The Target. An Animator component is required")]
    public FsmOwnerDefault gameObject;
    [Tooltip("If true, automaticaly stabilize feet during transition and blending")]
    public FsmBool stabilizeFeet;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.stabilizeFeet = (FsmBool) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoStabilizeFeet();
          this.Finish();
        }
      }
    }

    private void DoStabilizeFeet()
    {
      if ((Object) this._animator == (Object) null)
        return;
      this._animator.stabilizeFeet = this.stabilizeFeet.Value;
    }
  }
}
