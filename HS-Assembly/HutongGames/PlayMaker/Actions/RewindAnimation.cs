﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.RewindAnimation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Rewinds the named animation.")]
  [ActionCategory(ActionCategory.Animation)]
  public class RewindAnimation : BaseAnimationAction
  {
    [RequiredField]
    [CheckForComponent(typeof (Animation))]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Animation)]
    public FsmString animName;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.animName = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.DoRewindAnimation();
      this.Finish();
    }

    private void DoRewindAnimation()
    {
      if (string.IsNullOrEmpty(this.animName.Value) || !this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.animation.Rewind(this.animName.Value);
    }
  }
}
