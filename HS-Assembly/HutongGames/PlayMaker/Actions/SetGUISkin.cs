﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGUISkin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("Sets the GUISkin used by GUI elements.")]
  public class SetGUISkin : FsmStateAction
  {
    [RequiredField]
    public GUISkin skin;
    public FsmBool applyGlobally;

    public override void Reset()
    {
      this.skin = (GUISkin) null;
      this.applyGlobally = (FsmBool) true;
    }

    public override void OnGUI()
    {
      if ((Object) this.skin != (Object) null)
        GUI.skin = this.skin;
      if (!this.applyGlobally.Value)
        return;
      PlayMakerGUI.GUISkin = this.skin;
      this.Finish();
    }
  }
}
