﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGUIText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the Text used by the GUIText Component attached to a Game Object.")]
  [ActionCategory(ActionCategory.GUIElement)]
  public class SetGUIText : ComponentAction<GUIText>
  {
    [CheckForComponent(typeof (GUIText))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.TextArea)]
    public FsmString text;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.text = (FsmString) string.Empty;
    }

    public override void OnEnter()
    {
      this.DoSetGUIText();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetGUIText();
    }

    private void DoSetGUIText()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.guiText.text = this.text.Value;
    }
  }
}
