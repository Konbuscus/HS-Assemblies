﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorHumanScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Returns the scale of the current Avatar for a humanoid rig, (1 by default if the rig is generic).\n The scale is relative to Unity's Default Avatar")]
  public class GetAnimatorHumanScale : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The Target. An Animator component is required")]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [Tooltip("the scale of the current Avatar")]
    [ActionSection("Result")]
    [UIHint(UIHint.Variable)]
    public FsmFloat humanScale;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.humanScale = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoGetHumanScale();
          this.Finish();
        }
      }
    }

    private void DoGetHumanScale()
    {
      if ((Object) this._animator == (Object) null)
        return;
      this.humanScale.Value = this._animator.humanScale;
    }
  }
}
