﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorIsControlled
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Animator")]
  [Tooltip("Returns true if a transform is controlled by the Animator. Can also send events")]
  [Obsolete("This action is obsolete. Use mask and layers to control subset of transforms in a skeleton")]
  public class GetAnimatorIsControlled : FsmStateAction
  {
  }
}
