﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGUIColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("Sets the Tinting Color for the GUI. By default only effects GUI rendered by this FSM, check Apply Globally to effect all GUI controls.")]
  public class SetGUIColor : FsmStateAction
  {
    [RequiredField]
    public FsmColor color;
    public FsmBool applyGlobally;

    public override void Reset()
    {
      this.color = (FsmColor) Color.white;
    }

    public override void OnGUI()
    {
      GUI.color = this.color.Value;
      if (!this.applyGlobally.Value)
        return;
      PlayMakerGUI.GUIColor = GUI.color;
    }
  }
}
