﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorIsHuman
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Returns true if the current rig is humanoid, false if it is generic. Can also sends events")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorIsHuman : FsmStateAction
  {
    [Tooltip("The Target. An Animator component is required")]
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [Tooltip("True if the current rig is humanoid, False if it is generic")]
    [ActionSection("Results")]
    public FsmBool isHuman;
    [Tooltip("Event send if rig is humanoid")]
    public FsmEvent isHumanEvent;
    [Tooltip("Event send if rig is generic")]
    public FsmEvent isGenericEvent;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.isHuman = (FsmBool) null;
      this.isHumanEvent = (FsmEvent) null;
      this.isGenericEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoCheckIsHuman();
          this.Finish();
        }
      }
    }

    private void DoCheckIsHuman()
    {
      if ((Object) this._animator == (Object) null)
        return;
      bool isHuman = this._animator.isHuman;
      if (!this.isHuman.IsNone)
        this.isHuman.Value = isHuman;
      if (isHuman)
        this.Fsm.Event(this.isHumanEvent);
      else
        this.Fsm.Event(this.isGenericEvent);
    }
  }
}
