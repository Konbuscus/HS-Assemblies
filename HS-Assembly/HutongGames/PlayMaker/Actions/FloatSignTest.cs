﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FloatSignTest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sends Events based on the sign of a Float.")]
  [ActionCategory(ActionCategory.Logic)]
  public class FloatSignTest : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The float variable to test.")]
    public FsmFloat floatValue;
    [Tooltip("Event to send if the float variable is positive.")]
    public FsmEvent isPositive;
    [Tooltip("Event to send if the float variable is negative.")]
    public FsmEvent isNegative;
    [Tooltip("Repeat every frame. Useful if the variable is changing and you're waiting for a particular result.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.floatValue = (FsmFloat) 0.0f;
      this.isPositive = (FsmEvent) null;
      this.isNegative = (FsmEvent) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSignTest();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSignTest();
    }

    private void DoSignTest()
    {
      if (this.floatValue == null)
        return;
      this.Fsm.Event((double) this.floatValue.Value >= 0.0 ? this.isPositive : this.isNegative);
    }

    public override string ErrorCheck()
    {
      if (FsmEvent.IsNullOrEmpty(this.isPositive) && FsmEvent.IsNullOrEmpty(this.isNegative))
        return "Action sends no events!";
      return string.Empty;
    }
  }
}
