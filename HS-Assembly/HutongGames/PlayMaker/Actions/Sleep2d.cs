﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Sleep2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Forces a Game Object's Rigid Body 2D to Sleep at least one frame.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class Sleep2d : ComponentAction<Rigidbody2D>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody2D))]
    [Tooltip("The GameObject with a Rigidbody2d attached")]
    public FsmOwnerDefault gameObject;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      this.DoSleep();
      this.Finish();
    }

    private void DoSleep()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.rigidbody2d.Sleep();
    }
  }
}
