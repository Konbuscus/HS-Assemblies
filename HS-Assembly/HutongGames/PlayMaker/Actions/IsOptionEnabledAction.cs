﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.IsOptionEnabledAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sends Events based on whether or not an option is enabled by the player.")]
  [ActionCategory("Pegasus")]
  public class IsOptionEnabledAction : FsmStateAction
  {
    [Tooltip("The option to check.")]
    public Option m_Option;
    [Tooltip("Event sent if the option is on.")]
    public FsmEvent m_TrueEvent;
    [Tooltip("Event sent if the option is off.")]
    public FsmEvent m_FalseEvent;
    [Tooltip("Check if the option is enabled every frame.")]
    public bool m_EveryFrame;

    public override void Reset()
    {
      this.m_Option = Option.INVALID;
      this.m_TrueEvent = (FsmEvent) null;
      this.m_FalseEvent = (FsmEvent) null;
      this.m_EveryFrame = false;
    }

    public override void OnEnter()
    {
      this.FireEvents();
      if (this.m_EveryFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.FireEvents();
    }

    public override string ErrorCheck()
    {
      if (!FsmEvent.IsNullOrEmpty(this.m_TrueEvent) || !FsmEvent.IsNullOrEmpty(this.m_FalseEvent))
        return string.Empty;
      return "Action sends no events!";
    }

    private void FireEvents()
    {
      if (Options.Get().GetBool(this.m_Option))
        this.Fsm.Event(this.m_TrueEvent);
      else
        this.Fsm.Event(this.m_FalseEvent);
    }
  }
}
