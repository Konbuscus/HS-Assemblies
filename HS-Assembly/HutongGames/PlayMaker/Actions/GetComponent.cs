﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.UnityObject)]
  [Tooltip("Gets a Component attached to a GameObject and stores it in an Object variable. NOTE: Set the Object variable's Object Type to get a component of that type. E.g., set Object Type to UnityEngine.AudioListener to get the AudioListener component on the camera.")]
  public class GetComponent : FsmStateAction
  {
    [Tooltip("The GameObject that owns the component.")]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("Store the component in an Object variable.\nNOTE: Set theObject variable's Object Type to get a component of that type. E.g., set Object Type to UnityEngine.AudioListener to get the AudioListener component on the camera.")]
    public FsmObject storeComponent;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.storeComponent = (FsmObject) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetComponent();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetComponent();
    }

    private void DoGetComponent()
    {
      if (this.storeComponent == null)
        return;
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null || this.storeComponent.IsNone)
        return;
      this.storeComponent.Value = (Object) ownerDefaultTarget.GetComponent(this.storeComponent.get_ObjectType());
    }
  }
}
