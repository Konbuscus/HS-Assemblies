﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Returns true if a parameter is controlled by an additional curve on an animation")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorIsParameterControlledByCurve : FsmStateAction
  {
    [Tooltip("The target. An Animator component is required")]
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [Tooltip("The parameter's name")]
    public FsmString parameterName;
    [ActionSection("Results")]
    [UIHint(UIHint.Variable)]
    [Tooltip("True if controlled by curve")]
    public FsmBool isControlledByCurve;
    [Tooltip("Event send if controlled by curve")]
    public FsmEvent isControlledByCurveEvent;
    [Tooltip("Event send if not controlled by curve")]
    public FsmEvent isNotControlledByCurveEvent;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.parameterName = (FsmString) null;
      this.isControlledByCurve = (FsmBool) null;
      this.isControlledByCurveEvent = (FsmEvent) null;
      this.isNotControlledByCurveEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoCheckIsParameterControlledByCurve();
          this.Finish();
        }
      }
    }

    private void DoCheckIsParameterControlledByCurve()
    {
      if ((Object) this._animator == (Object) null)
        return;
      bool flag = this._animator.IsParameterControlledByCurve(this.parameterName.Value);
      this.isControlledByCurve.Value = flag;
      if (flag)
        this.Fsm.Event(this.isControlledByCurveEvent);
      else
        this.Fsm.Event(this.isNotControlledByCurveEvent);
    }
  }
}
