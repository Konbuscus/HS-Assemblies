﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ApplicationQuit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Application)]
  [Tooltip("Quits the player application.")]
  public class ApplicationQuit : FsmStateAction
  {
    public override void Reset()
    {
    }

    public override void OnEnter()
    {
      Application.Quit();
      this.Finish();
    }
  }
}
