﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector3Lerp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Vector3)]
  [Tooltip("Linearly interpolates between 2 vectors.")]
  public class Vector3Lerp : FsmStateAction
  {
    [RequiredField]
    [Tooltip("First Vector.")]
    public FsmVector3 fromVector;
    [Tooltip("Second Vector.")]
    [RequiredField]
    public FsmVector3 toVector;
    [RequiredField]
    [Tooltip("Interpolate between From Vector and ToVector by this amount. Value is clamped to 0-1 range. 0 = From Vector; 1 = To Vector; 0.5 = half way between.")]
    public FsmFloat amount;
    [Tooltip("Store the result in this vector variable.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmVector3 storeResult;
    [Tooltip("Repeat every frame. Useful if any of the values are changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      FsmVector3 fsmVector3_1 = new FsmVector3();
      fsmVector3_1.UseVariable = true;
      this.fromVector = fsmVector3_1;
      FsmVector3 fsmVector3_2 = new FsmVector3();
      fsmVector3_2.UseVariable = true;
      this.toVector = fsmVector3_2;
      this.storeResult = (FsmVector3) null;
      this.everyFrame = true;
    }

    public override void OnEnter()
    {
      this.DoVector3Lerp();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoVector3Lerp();
    }

    private void DoVector3Lerp()
    {
      this.storeResult.Value = Vector3.Lerp(this.fromVector.Value, this.toVector.Value, this.amount.Value);
    }
  }
}
