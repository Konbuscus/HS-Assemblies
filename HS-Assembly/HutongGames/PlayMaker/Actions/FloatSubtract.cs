﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FloatSubtract
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Subtracts a value from a Float Variable.")]
  [ActionCategory(ActionCategory.Math)]
  public class FloatSubtract : FsmStateAction
  {
    [Tooltip("The float variable to subtract from.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmFloat floatVariable;
    [Tooltip("Value to subtract from the float variable.")]
    [RequiredField]
    public FsmFloat subtract;
    [Tooltip("Repeat every frame while the state is active.")]
    public bool everyFrame;
    [Tooltip("Used with Every Frame. Adds the value over one second to make the operation frame rate independent.")]
    public bool perSecond;

    public override void Reset()
    {
      this.floatVariable = (FsmFloat) null;
      this.subtract = (FsmFloat) null;
      this.everyFrame = false;
      this.perSecond = false;
    }

    public override void OnEnter()
    {
      this.DoFloatSubtract();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoFloatSubtract();
    }

    private void DoFloatSubtract()
    {
      if (!this.perSecond)
        this.floatVariable.Value -= this.subtract.Value;
      else
        this.floatVariable.Value -= this.subtract.Value * Time.deltaTime;
    }
  }
}
