﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FsmArraySet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set an item in an Array Variable in another FSM.")]
  [ActionCategory(ActionCategory.Array)]
  [ActionTarget(typeof (PlayMakerFSM), "gameObject,fsmName", false)]
  [Obsolete("This action was wip and accidentally released.")]
  public class FsmArraySet : FsmStateAction
  {
    [Tooltip("The GameObject that owns the FSM.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.FsmName)]
    [Tooltip("Optional name of FSM on Game Object.")]
    public FsmString fsmName;
    [RequiredField]
    [Tooltip("The name of the FSM variable.")]
    public FsmString variableName;
    [Tooltip("Set the value of the variable.")]
    public FsmString setValue;
    [Tooltip("Repeat every frame. Useful if the value is changing.")]
    public bool everyFrame;
    private GameObject goLastFrame;
    private PlayMakerFSM fsm;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.fsmName = (FsmString) string.Empty;
      this.setValue = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.DoSetFsmString();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    private void DoSetFsmString()
    {
      if (this.setValue == null)
        return;
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((UnityEngine.Object) ownerDefaultTarget == (UnityEngine.Object) null)
        return;
      if ((UnityEngine.Object) ownerDefaultTarget != (UnityEngine.Object) this.goLastFrame)
      {
        this.goLastFrame = ownerDefaultTarget;
        this.fsm = ActionHelpers.GetGameObjectFsm(ownerDefaultTarget, this.fsmName.Value);
      }
      if ((UnityEngine.Object) this.fsm == (UnityEngine.Object) null)
      {
        this.LogWarning("Could not find FSM: " + this.fsmName.Value);
      }
      else
      {
        FsmString fsmString = this.fsm.FsmVariables.GetFsmString(this.variableName.Value);
        if (fsmString != null)
          fsmString.Value = this.setValue.Value;
        else
          this.LogWarning("Could not find variable: " + this.variableName.Value);
      }
    }

    public override void OnUpdate()
    {
      this.DoSetFsmString();
    }
  }
}
