﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellCustomActorVariable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Initialize a spell state, setting a variable that references one of the Actor's game objects by name.")]
  public class SpellCustomActorVariable : FsmStateAction
  {
    public FsmString m_objectName;
    public FsmGameObject m_actorObject;

    public override void Reset()
    {
      this.m_objectName = (FsmString) string.Empty;
      this.m_actorObject = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      if (!this.m_actorObject.IsNone)
      {
        Actor componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<Actor>(this.Owner);
        if ((Object) componentInThisOrParents != (Object) null)
        {
          GameObject childBySubstring = SceneUtils.FindChildBySubstring(componentInThisOrParents.gameObject, this.m_objectName.Value);
          if ((Object) childBySubstring == (Object) null)
            Debug.LogWarning((object) ("Could not find object of name " + (object) this.m_objectName + " in actor"));
          else
            this.m_actorObject.Value = childBySubstring;
        }
      }
      this.Finish();
    }

    public override void OnUpdate()
    {
    }
  }
}
