﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("GUILayout Box.")]
  [ActionCategory(ActionCategory.GUILayout)]
  public class GUILayoutBox : GUILayoutAction
  {
    [Tooltip("Image to display in the Box.")]
    public FsmTexture image;
    [Tooltip("Text to display in the Box.")]
    public FsmString text;
    [Tooltip("Optional Tooltip string.")]
    public FsmString tooltip;
    [Tooltip("Optional GUIStyle in the active GUISkin.")]
    public FsmString style;

    public override void Reset()
    {
      base.Reset();
      this.text = (FsmString) string.Empty;
      this.image = (FsmTexture) null;
      this.tooltip = (FsmString) string.Empty;
      this.style = (FsmString) string.Empty;
    }

    public override void OnGUI()
    {
      if (string.IsNullOrEmpty(this.style.Value))
        GUILayout.Box(new GUIContent(this.text.Value, this.image.Value, this.tooltip.Value), this.LayoutOptions);
      else
        GUILayout.Box(new GUIContent(this.text.Value, this.image.Value, this.tooltip.Value), (GUIStyle) this.style.Value, this.LayoutOptions);
    }
  }
}
