﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.RandomWaitAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Delays a State from finishing by a random time. NOTE: Other actions continue, but FINISHED can't happen before the delay.")]
  public class RandomWaitAction : FsmStateAction
  {
    public FsmFloat m_MinTime;
    public FsmFloat m_MaxTime;
    public FsmEvent m_FinishEvent;
    public bool m_RealTime;
    private float m_startTime;
    private float m_waitTime;
    private float m_updateTime;

    public override void Reset()
    {
      this.m_MinTime = (FsmFloat) 1f;
      this.m_MaxTime = (FsmFloat) 1f;
      this.m_FinishEvent = (FsmEvent) null;
      this.m_RealTime = false;
    }

    public override void OnEnter()
    {
      if ((double) this.m_MinTime.Value <= 0.0 && (double) this.m_MaxTime.Value <= 0.0)
      {
        this.Finish();
        if (this.m_FinishEvent == null)
          return;
        this.Fsm.Event(this.m_FinishEvent);
      }
      else
      {
        this.m_startTime = FsmTime.RealtimeSinceStartup;
        this.m_waitTime = Random.Range(this.m_MinTime.Value, this.m_MaxTime.Value);
        this.m_updateTime = 0.0f;
      }
    }

    public override void OnUpdate()
    {
      if (this.m_RealTime)
        this.m_updateTime = FsmTime.RealtimeSinceStartup - this.m_startTime;
      else
        this.m_updateTime += Time.deltaTime;
      if ((double) this.m_updateTime <= (double) this.m_waitTime)
        return;
      this.Finish();
      if (this.m_FinishEvent == null)
        return;
      this.Fsm.Event(this.m_FinishEvent);
    }
  }
}
