﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.StringJoin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.String)]
  [Tooltip("Join an array of strings into a single string.")]
  public class StringJoin : FsmStateAction
  {
    [ArrayEditor(VariableType.String, "", 0, 0, 65536)]
    [Tooltip("Array of string to join into a single string.")]
    [UIHint(UIHint.Variable)]
    public FsmArray stringArray;
    [Tooltip("Seperator to add between each string.")]
    public FsmString separator;
    [Tooltip("Store the joined string in string variable.")]
    [UIHint(UIHint.Variable)]
    public FsmString storeResult;

    public override void OnEnter()
    {
      if (!this.stringArray.IsNone && !this.storeResult.IsNone)
        this.storeResult.Value = string.Join(this.separator.Value, this.stringArray.stringValues);
      this.Finish();
    }
  }
}
