﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellCardIdAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Send an event based on a Spell's Card's ID.")]
  [ActionCategory("Pegasus")]
  public class SpellCardIdAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    [Tooltip("Which Card to check on the Spell.")]
    public SpellAction.Which m_WhichCard;
    [CompoundArray("Events", "Event", "Card Id")]
    [RequiredField]
    public FsmEvent[] m_Events;
    public string[] m_CardIds;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      this.m_SpellObject = (FsmOwnerDefault) null;
      this.m_WhichCard = SpellAction.Which.SOURCE;
      this.m_Events = new FsmEvent[2];
      this.m_CardIds = new string[2];
    }

    public override void OnEnter()
    {
      base.OnEnter();
      Card card = this.GetCard(this.m_WhichCard);
      if ((Object) card == (Object) null)
      {
        Error.AddDevFatal("SpellCardIdAction.OnEnter() - Card not found!");
        this.Finish();
      }
      else
      {
        int indexMatchingCardId = this.GetIndexMatchingCardId(card.GetEntity().GetCardId(), this.m_CardIds);
        if (indexMatchingCardId >= 0 && this.m_Events[indexMatchingCardId] != null)
          this.Fsm.Event(this.m_Events[indexMatchingCardId]);
        this.Finish();
      }
    }
  }
}
