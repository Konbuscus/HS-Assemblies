﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Gets the name of a Game Object and stores it in a String Variable.")]
  public class GetName : FsmStateAction
  {
    [RequiredField]
    public FsmGameObject gameObject;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmString storeName;
    public bool everyFrame;

    public override void Reset()
    {
      FsmGameObject fsmGameObject = new FsmGameObject();
      fsmGameObject.UseVariable = true;
      this.gameObject = fsmGameObject;
      this.storeName = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetGameObjectName();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetGameObjectName();
    }

    private void DoGetGameObjectName()
    {
      GameObject gameObject = this.gameObject.Value;
      this.storeName.Value = !((Object) gameObject != (Object) null) ? string.Empty : gameObject.name;
    }
  }
}
