﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Returns true if automatic matching is active. Can also send events")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorIsMatchingTarget : FsmStateActionAnimatorBase
  {
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The target. An Animator component and a PlayMakerAnimatorProxy component are required")]
    public FsmOwnerDefault gameObject;
    [Tooltip("True if automatic matching is active")]
    [UIHint(UIHint.Variable)]
    [ActionSection("Results")]
    public FsmBool isMatchingActive;
    [Tooltip("Event send if automatic matching is active")]
    public FsmEvent matchingActivatedEvent;
    [Tooltip("Event send if automatic matching is not active")]
    public FsmEvent matchingDeactivedEvent;
    private Animator _animator;

    public override void Reset()
    {
      base.Reset();
      this.gameObject = (FsmOwnerDefault) null;
      this.isMatchingActive = (FsmBool) null;
      this.matchingActivatedEvent = (FsmEvent) null;
      this.matchingDeactivedEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoCheckIsMatchingActive();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnActionUpdate()
    {
      this.DoCheckIsMatchingActive();
    }

    private void DoCheckIsMatchingActive()
    {
      if ((Object) this._animator == (Object) null)
        return;
      bool isMatchingTarget = this._animator.isMatchingTarget;
      this.isMatchingActive.Value = isMatchingTarget;
      if (isMatchingTarget)
        this.Fsm.Event(this.matchingActivatedEvent);
      else
        this.Fsm.Event(this.matchingDeactivedEvent);
    }
  }
}
