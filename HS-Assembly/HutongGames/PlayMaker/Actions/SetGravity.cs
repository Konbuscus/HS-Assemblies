﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGravity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Sets the gravity vector, or individual axis.")]
  public class SetGravity : FsmStateAction
  {
    public FsmVector3 vector;
    public FsmFloat x;
    public FsmFloat y;
    public FsmFloat z;
    public bool everyFrame;

    public override void Reset()
    {
      this.vector = (FsmVector3) null;
      FsmFloat fsmFloat1 = new FsmFloat();
      fsmFloat1.UseVariable = true;
      this.x = fsmFloat1;
      FsmFloat fsmFloat2 = new FsmFloat();
      fsmFloat2.UseVariable = true;
      this.y = fsmFloat2;
      FsmFloat fsmFloat3 = new FsmFloat();
      fsmFloat3.UseVariable = true;
      this.z = fsmFloat3;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetGravity();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetGravity();
    }

    private void DoSetGravity()
    {
      Vector3 vector3 = this.vector.Value;
      if (!this.x.IsNone)
        vector3.x = this.x.Value;
      if (!this.y.IsNone)
        vector3.y = this.y.Value;
      if (!this.z.IsNone)
        vector3.z = this.z.Value;
      Physics.gravity = vector3;
    }
  }
}
