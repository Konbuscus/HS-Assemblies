﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.LookAt2dGameObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Rotates a 2d Game Object on it's z axis so its forward vector points at a Target.")]
  [ActionCategory(ActionCategory.Transform)]
  public class LookAt2dGameObject : FsmStateAction
  {
    [Tooltip("Repeat every frame.")]
    public bool everyFrame = true;
    [Tooltip("The GameObject to rotate.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("The GameObject to Look At.")]
    public FsmGameObject targetObject;
    [Tooltip("Set the GameObject starting offset. In degrees. 0 if your object is facing right, 180 if facing left etc...")]
    public FsmFloat rotationOffset;
    [Title("Draw Debug Line")]
    [Tooltip("Draw a debug line from the GameObject to the Target.")]
    public FsmBool debug;
    [Tooltip("Color to use for the debug line.")]
    public FsmColor debugLineColor;
    private GameObject go;
    private GameObject goTarget;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.targetObject = (FsmGameObject) null;
      this.debug = (FsmBool) false;
      this.debugLineColor = (FsmColor) Color.green;
      this.everyFrame = true;
    }

    public override void OnEnter()
    {
      this.DoLookAt();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoLookAt();
    }

    private void DoLookAt()
    {
      this.go = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      this.goTarget = this.targetObject.Value;
      if ((Object) this.go == (Object) null || this.targetObject == null)
        return;
      Vector3 vector3 = this.goTarget.transform.position - this.go.transform.position;
      vector3.Normalize();
      this.go.transform.rotation = Quaternion.Euler(0.0f, 0.0f, Mathf.Atan2(vector3.y, vector3.x) * 57.29578f - this.rotationOffset.Value);
      if (!this.debug.Value)
        return;
      Debug.DrawLine(this.go.transform.position, this.goTarget.transform.position, this.debugLineColor.Value);
    }
  }
}
