﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetFsmObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.StateMachine)]
  [ActionTarget(typeof (PlayMakerFSM), "gameObject,fsmName", false)]
  [Tooltip("Get the value of an Object Variable from another FSM.")]
  public class GetFsmObject : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject that owns the FSM.")]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.FsmName)]
    [Tooltip("Optional name of FSM on Game Object")]
    public FsmString fsmName;
    [RequiredField]
    [UIHint(UIHint.FsmObject)]
    public FsmString variableName;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmObject storeValue;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;
    private GameObject goLastFrame;
    protected PlayMakerFSM fsm;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.fsmName = (FsmString) string.Empty;
      this.variableName = (FsmString) string.Empty;
      this.storeValue = (FsmObject) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetFsmVariable();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetFsmVariable();
    }

    private void DoGetFsmVariable()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((Object) ownerDefaultTarget != (Object) this.goLastFrame)
      {
        this.goLastFrame = ownerDefaultTarget;
        this.fsm = ActionHelpers.GetGameObjectFsm(ownerDefaultTarget, this.fsmName.Value);
      }
      if ((Object) this.fsm == (Object) null || this.storeValue == null)
        return;
      FsmObject fsmObject = this.fsm.FsmVariables.GetFsmObject(this.variableName.Value);
      if (fsmObject == null)
        return;
      this.storeValue.Value = fsmObject.Value;
    }
  }
}
