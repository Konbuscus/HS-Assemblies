﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ControllerMove
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Character)]
  [Tooltip("Moves a Game Object with a Character Controller. See also Controller Simple Move. NOTE: It is recommended that you make only one call to Move or SimpleMove per frame.")]
  public class ControllerMove : FsmStateAction
  {
    [Tooltip("The GameObject to move.")]
    [CheckForComponent(typeof (CharacterController))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("The movement vector.")]
    [RequiredField]
    public FsmVector3 moveVector;
    [Tooltip("Move in local or word space.")]
    public Space space;
    [Tooltip("Movement vector is defined in units per second. Makes movement frame rate independent.")]
    public FsmBool perSecond;
    private GameObject previousGo;
    private CharacterController controller;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.moveVector = fsmVector3;
      this.space = Space.World;
      this.perSecond = (FsmBool) true;
    }

    public override void OnUpdate()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((Object) ownerDefaultTarget != (Object) this.previousGo)
      {
        this.controller = ownerDefaultTarget.GetComponent<CharacterController>();
        this.previousGo = ownerDefaultTarget;
      }
      if (!((Object) this.controller != (Object) null))
        return;
      Vector3 motion = this.space != Space.World ? ownerDefaultTarget.transform.TransformDirection(this.moveVector.Value) : this.moveVector.Value;
      if (this.perSecond.Value)
      {
        int num1 = (int) this.controller.Move(motion * Time.deltaTime);
      }
      else
      {
        int num2 = (int) this.controller.Move(motion);
      }
    }
  }
}
