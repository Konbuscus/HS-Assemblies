﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.StringCompare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Logic)]
  [Tooltip("Compares 2 Strings and sends Events based on the result.")]
  public class StringCompare : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmString stringVariable;
    public FsmString compareTo;
    public FsmEvent equalEvent;
    public FsmEvent notEqualEvent;
    [Tooltip("Store the true/false result in a bool variable.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame. Useful if any of the strings are changing over time.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.stringVariable = (FsmString) null;
      this.compareTo = (FsmString) string.Empty;
      this.equalEvent = (FsmEvent) null;
      this.notEqualEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoStringCompare();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoStringCompare();
    }

    private void DoStringCompare()
    {
      if (this.stringVariable == null || this.compareTo == null)
        return;
      bool flag = this.stringVariable.Value == this.compareTo.Value;
      if (this.storeResult != null)
        this.storeResult.Value = flag;
      if (flag && this.equalEvent != null)
      {
        this.Fsm.Event(this.equalEvent);
      }
      else
      {
        if (flag || this.notEqualEvent == null)
          return;
        this.Fsm.Event(this.notEqualEvent);
      }
    }
  }
}
