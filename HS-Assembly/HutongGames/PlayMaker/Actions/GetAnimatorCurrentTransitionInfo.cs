﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the current transition information on a specified layer. Only valid when during a transition.")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorCurrentTransitionInfo : FsmStateActionAnimatorBase
  {
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The target. An Animator component is required")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The layer's index")]
    [RequiredField]
    public FsmInt layerIndex;
    [Tooltip("The unique name of the Transition")]
    [UIHint(UIHint.Variable)]
    [ActionSection("Results")]
    public FsmString name;
    [Tooltip("The unique name of the Transition")]
    [UIHint(UIHint.Variable)]
    public FsmInt nameHash;
    [Tooltip("The user-specidied name of the Transition")]
    [UIHint(UIHint.Variable)]
    public FsmInt userNameHash;
    [UIHint(UIHint.Variable)]
    [Tooltip("Normalized time of the Transition")]
    public FsmFloat normalizedTime;
    private Animator _animator;

    public override void Reset()
    {
      base.Reset();
      this.gameObject = (FsmOwnerDefault) null;
      this.layerIndex = (FsmInt) null;
      this.name = (FsmString) null;
      this.nameHash = (FsmInt) null;
      this.userNameHash = (FsmInt) null;
      this.normalizedTime = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.GetTransitionInfo();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnActionUpdate()
    {
      this.GetTransitionInfo();
    }

    private void GetTransitionInfo()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      AnimatorTransitionInfo animatorTransitionInfo = this._animator.GetAnimatorTransitionInfo(this.layerIndex.Value);
      if (!this.name.IsNone)
        this.name.Value = this._animator.GetLayerName(this.layerIndex.Value);
      if (!this.nameHash.IsNone)
        this.nameHash.Value = animatorTransitionInfo.nameHash;
      if (!this.userNameHash.IsNone)
        this.userNameHash.Value = animatorTransitionInfo.userNameHash;
      if (this.normalizedTime.IsNone)
        return;
      this.normalizedTime.Value = animatorTransitionInfo.normalizedTime;
    }
  }
}
