﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetRaycastHitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Gets info on the last Raycast and store in variables.")]
  public class GetRaycastHitInfo : FsmStateAction
  {
    [Tooltip("Get the GameObject hit by the last Raycast and store it in a variable.")]
    [UIHint(UIHint.Variable)]
    public FsmGameObject gameObjectHit;
    [Tooltip("Get the world position of the ray hit point and store it in a variable.")]
    [UIHint(UIHint.Variable)]
    [Title("Hit Point")]
    public FsmVector3 point;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the normal at the hit point and store it in a variable.")]
    public FsmVector3 normal;
    [Tooltip("Get the distance along the ray to the hit point and store it in a variable.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat distance;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObjectHit = (FsmGameObject) null;
      this.point = (FsmVector3) null;
      this.normal = (FsmVector3) null;
      this.distance = (FsmFloat) null;
      this.everyFrame = false;
    }

    private void StoreRaycastInfo()
    {
      if (!((Object) this.Fsm.RaycastHitInfo.collider != (Object) null))
        return;
      this.gameObjectHit.Value = this.Fsm.RaycastHitInfo.collider.gameObject;
      this.point.Value = this.Fsm.RaycastHitInfo.point;
      this.normal.Value = this.Fsm.RaycastHitInfo.normal;
      this.distance.Value = this.Fsm.RaycastHitInfo.distance;
    }

    public override void OnEnter()
    {
      this.StoreRaycastInfo();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.StoreRaycastInfo();
    }
  }
}
