﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMaterial
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Material)]
  [Tooltip("Get a material at index on a gameObject and store it in a variable")]
  public class GetMaterial : ComponentAction<Renderer>
  {
    [RequiredField]
    [CheckForComponent(typeof (Renderer))]
    [Tooltip("The GameObject the Material is applied to.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The index of the Material in the Materials array.")]
    public FsmInt materialIndex;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the material in a variable.")]
    [RequiredField]
    public FsmMaterial material;
    [Tooltip("Get the shared material of this object. NOTE: Modifying the shared material will change the appearance of all objects using this material, and change material settings that are stored in the project too.")]
    public bool getSharedMaterial;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.material = (FsmMaterial) null;
      this.materialIndex = (FsmInt) 0;
      this.getSharedMaterial = false;
    }

    public override void OnEnter()
    {
      this.DoGetMaterial();
      this.Finish();
    }

    private void DoGetMaterial()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      if (this.materialIndex.Value == 0 && !this.getSharedMaterial)
        this.material.Value = this.renderer.material;
      else if (this.materialIndex.Value == 0 && this.getSharedMaterial)
        this.material.Value = this.renderer.sharedMaterial;
      else if (this.renderer.materials.Length > this.materialIndex.Value && !this.getSharedMaterial)
      {
        Material[] materials = this.renderer.materials;
        this.material.Value = materials[this.materialIndex.Value];
        this.renderer.materials = materials;
      }
      else
      {
        if (this.renderer.materials.Length <= this.materialIndex.Value || !this.getSharedMaterial)
          return;
        Material[] sharedMaterials = this.renderer.sharedMaterials;
        this.material.Value = sharedMaterials[this.materialIndex.Value];
        this.renderer.sharedMaterials = sharedMaterials;
      }
    }
  }
}
