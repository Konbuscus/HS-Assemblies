﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUITooltip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("Gets the Tooltip of the control the mouse is currently over and store it in a String Variable.")]
  public class GUITooltip : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    public FsmString storeTooltip;

    public override void Reset()
    {
      this.storeTooltip = (FsmString) null;
    }

    public override void OnGUI()
    {
      this.storeTooltip.Value = GUI.tooltip;
    }
  }
}
