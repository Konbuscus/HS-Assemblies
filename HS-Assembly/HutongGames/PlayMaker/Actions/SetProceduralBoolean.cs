﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetProceduralBoolean
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set a named bool property in a Substance material. NOTE: Use Rebuild Textures after setting Substance properties.")]
  [ActionCategory("Substance")]
  public class SetProceduralBoolean : FsmStateAction
  {
    [RequiredField]
    public FsmMaterial substanceMaterial;
    [RequiredField]
    public FsmString boolProperty;
    [RequiredField]
    public FsmBool boolValue;
    [Tooltip("NOTE: Updating procedural materials every frame can be very slow!")]
    public bool everyFrame;

    public override void Reset()
    {
      this.substanceMaterial = (FsmMaterial) null;
      this.boolProperty = (FsmString) string.Empty;
      this.boolValue = (FsmBool) false;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetProceduralFloat();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetProceduralFloat();
    }

    private void DoSetProceduralFloat()
    {
      ProceduralMaterial proceduralMaterial = this.substanceMaterial.Value as ProceduralMaterial;
      if ((Object) proceduralMaterial == (Object) null)
        this.LogError("Not a substance material!");
      else
        proceduralMaterial.SetProceduralBoolean(this.boolProperty.Value, this.boolValue.Value);
    }
  }
}
