﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorDelta
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the avatar delta position and rotation for the last evaluated frame.")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorDelta : FsmStateActionAnimatorBase
  {
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    [Tooltip("The target. An Animator component is required")]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [Tooltip("The avatar delta position for the last evaluated frame")]
    public FsmVector3 deltaPosition;
    [UIHint(UIHint.Variable)]
    [Tooltip("The avatar delta position for the last evaluated frame")]
    public FsmQuaternion deltaRotation;
    private Transform _transform;
    private Animator _animator;

    public override void Reset()
    {
      base.Reset();
      this.gameObject = (FsmOwnerDefault) null;
      this.deltaPosition = (FsmVector3) null;
      this.deltaRotation = (FsmQuaternion) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoGetDeltaPosition();
          this.Finish();
        }
      }
    }

    public override void OnActionUpdate()
    {
      this.DoGetDeltaPosition();
    }

    private void DoGetDeltaPosition()
    {
      if ((Object) this._animator == (Object) null)
        return;
      this.deltaPosition.Value = this._animator.deltaPosition;
      this.deltaRotation.Value = this._animator.deltaRotation;
    }
  }
}
