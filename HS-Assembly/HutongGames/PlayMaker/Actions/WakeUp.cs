﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.WakeUp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Forces a Game Object's Rigid Body to wake up.")]
  public class WakeUp : ComponentAction<Rigidbody>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody))]
    public FsmOwnerDefault gameObject;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      this.DoWakeUp();
      this.Finish();
    }

    private void DoWakeUp()
    {
      if (!this.UpdateCache(this.gameObject.OwnerOption != OwnerDefaultOption.UseOwner ? this.gameObject.GameObject.Value : this.Owner))
        return;
      this.rigidbody.WakeUp();
    }
  }
}
