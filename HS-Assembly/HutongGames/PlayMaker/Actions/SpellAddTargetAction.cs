﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellAddTargetAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Adds a target to a Spell.")]
  [ActionCategory("Pegasus")]
  public class SpellAddTargetAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    public FsmGameObject m_TargetObject;
    public FsmBool m_AllowDuplicates;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      base.Reset();
      this.m_AllowDuplicates = (FsmBool) false;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_spell == (Object) null)
        return;
      GameObject go = this.m_TargetObject.Value;
      if ((Object) go == (Object) null)
        return;
      if (!this.m_spell.IsTarget(go) || this.m_AllowDuplicates.Value)
        this.m_spell.AddTarget(go);
      this.Finish();
    }
  }
}
