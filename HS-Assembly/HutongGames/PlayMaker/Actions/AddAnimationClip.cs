﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AddAnimationClip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Adds a named Animation Clip to a Game Object. Optionally trims the Animation.")]
  [ActionCategory(ActionCategory.Animation)]
  public class AddAnimationClip : FsmStateAction
  {
    [Tooltip("The GameObject to add the Animation Clip to.")]
    [RequiredField]
    [CheckForComponent(typeof (Animation))]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [ObjectType(typeof (AnimationClip))]
    [Tooltip("The animation clip to add. NOTE: Make sure the clip is compatible with the object's hierarchy.")]
    public FsmObject animationClip;
    [Tooltip("Name the animation. Used by other actions to reference this animation.")]
    [RequiredField]
    public FsmString animationName;
    [Tooltip("Optionally trim the animation by specifying a first and last frame.")]
    public FsmInt firstFrame;
    [Tooltip("Optionally trim the animation by specifying a first and last frame.")]
    public FsmInt lastFrame;
    [Tooltip("Add an extra looping frame that matches the first frame.")]
    public FsmBool addLoopFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.animationClip = (FsmObject) null;
      this.animationName = (FsmString) string.Empty;
      this.firstFrame = (FsmInt) 0;
      this.lastFrame = (FsmInt) 0;
      this.addLoopFrame = (FsmBool) false;
    }

    public override void OnEnter()
    {
      this.DoAddAnimationClip();
      this.Finish();
    }

    private void DoAddAnimationClip()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      AnimationClip clip = this.animationClip.Value as AnimationClip;
      if ((Object) clip == (Object) null)
        return;
      Animation component = ownerDefaultTarget.GetComponent<Animation>();
      if (this.firstFrame.Value == 0 && this.lastFrame.Value == 0)
        component.AddClip(clip, this.animationName.Value);
      else
        component.AddClip(clip, this.animationName.Value, this.firstFrame.Value, this.lastFrame.Value, this.addLoopFrame.Value);
    }
  }
}
