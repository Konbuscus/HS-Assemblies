﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set the isTrigger option of a Collider2D. Optionally set all collider2D found on the gameobject Target.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class SetCollider2dIsTrigger : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (Collider2D))]
    [Tooltip("The GameObject with the Collider2D attached")]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The flag value")]
    public FsmBool isTrigger;
    [Tooltip("Set all Colliders on the GameObject target")]
    public bool setAllColliders;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.isTrigger = (FsmBool) false;
      this.setAllColliders = false;
    }

    public override void OnEnter()
    {
      this.DoSetIsTrigger();
      this.Finish();
    }

    private void DoSetIsTrigger()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if (this.setAllColliders)
      {
        foreach (Collider2D component in ownerDefaultTarget.GetComponents<Collider2D>())
          component.isTrigger = this.isTrigger.Value;
      }
      else
      {
        if (!((Object) ownerDefaultTarget.GetComponent<Collider2D>() != (Object) null))
          return;
        ownerDefaultTarget.GetComponent<Collider2D>().isTrigger = this.isTrigger.Value;
      }
    }
  }
}
