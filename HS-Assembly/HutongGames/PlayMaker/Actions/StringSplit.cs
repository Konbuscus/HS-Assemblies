﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.StringSplit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Splits a string into substrings using separator characters.")]
  [ActionCategory(ActionCategory.String)]
  public class StringSplit : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("String to split.")]
    public FsmString stringToSplit;
    [Tooltip("Characters used to split the string.\nUse '\\n' for newline\nUse '\\t' for tab")]
    public FsmString separators;
    [Tooltip("Remove all leading and trailing white-space characters from each seperated string.")]
    public FsmBool trimStrings;
    [Tooltip("Optional characters used to trim each seperated string.")]
    public FsmString trimChars;
    [Tooltip("Store the split strings in a String Array.")]
    [ArrayEditor(VariableType.String, "", 0, 0, 65536)]
    [UIHint(UIHint.Variable)]
    public FsmArray stringArray;

    public override void Reset()
    {
      this.stringToSplit = (FsmString) null;
      this.separators = (FsmString) null;
      this.trimStrings = (FsmBool) false;
      this.trimChars = (FsmString) null;
      this.stringArray = (FsmArray) null;
    }

    public override void OnEnter()
    {
      char[] charArray = this.trimChars.Value.ToCharArray();
      if (!this.stringToSplit.IsNone && !this.stringArray.IsNone)
      {
        this.stringArray.Values = (object[]) this.stringToSplit.Value.Split(this.separators.Value.ToCharArray());
        if (this.trimStrings.Value)
        {
          for (int index = 0; index < this.stringArray.Values.Length; ++index)
          {
            string str = this.stringArray.Values[index] as string;
            if (str != null)
            {
              if (!this.trimChars.IsNone && charArray.Length > 0)
                this.stringArray.Set(index, (object) str.Trim(charArray));
              else
                this.stringArray.Set(index, (object) str.Trim());
            }
          }
        }
        this.stringArray.SaveChanges();
      }
      this.Finish();
    }
  }
}
