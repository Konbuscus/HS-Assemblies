﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.WWWObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets data from a url and store it in variables. See Unity WWW docs for more details.")]
  [ActionCategory("Web Player")]
  public class WWWObject : FsmStateAction
  {
    [RequiredField]
    [Tooltip("Url to download data from.")]
    public FsmString url;
    [UIHint(UIHint.Variable)]
    [Tooltip("Gets text from the url.")]
    [ActionSection("Results")]
    public FsmString storeText;
    [Tooltip("Gets a Texture from the url.")]
    [UIHint(UIHint.Variable)]
    public FsmTexture storeTexture;
    [UIHint(UIHint.Variable)]
    [ObjectType(typeof (MovieTexture))]
    [Tooltip("Gets a Texture from the url.")]
    public FsmObject storeMovieTexture;
    [Tooltip("Error message if there was an error during the download.")]
    [UIHint(UIHint.Variable)]
    public FsmString errorString;
    [Tooltip("How far the download progressed (0-1).")]
    [UIHint(UIHint.Variable)]
    public FsmFloat progress;
    [ActionSection("Events")]
    [Tooltip("Event to send when the data has finished loading (progress = 1).")]
    public FsmEvent isDone;
    [Tooltip("Event to send if there was an error.")]
    public FsmEvent isError;
    private WWW wwwObject;

    public override void Reset()
    {
      this.url = (FsmString) null;
      this.storeText = (FsmString) null;
      this.storeTexture = (FsmTexture) null;
      this.errorString = (FsmString) null;
      this.progress = (FsmFloat) null;
      this.isDone = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      if (string.IsNullOrEmpty(this.url.Value))
        this.Finish();
      else
        this.wwwObject = new WWW(this.url.Value);
    }

    public override void OnUpdate()
    {
      if (this.wwwObject == null)
      {
        this.errorString.Value = "WWW Object is Null!";
        this.Finish();
      }
      else
      {
        this.errorString.Value = this.wwwObject.error;
        if (!string.IsNullOrEmpty(this.wwwObject.error))
        {
          this.Finish();
          this.Fsm.Event(this.isError);
        }
        else
        {
          this.progress.Value = this.wwwObject.progress;
          if (!this.progress.Value.Equals(1f))
            return;
          this.storeText.Value = this.wwwObject.text;
          this.storeTexture.Value = (Texture) this.wwwObject.texture;
          this.storeMovieTexture.Value = (Object) this.wwwObject.movie;
          this.errorString.Value = this.wwwObject.error;
          this.Fsm.Event(!string.IsNullOrEmpty(this.errorString.Value) ? this.isError : this.isDone);
          this.Finish();
        }
      }
    }
  }
}
