﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAmbientColorAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Get scene ambient color")]
  public class GetAmbientColorAction : FsmStateAction
  {
    public FsmColor m_Color;
    public bool m_EveryFrame;

    public override void Reset()
    {
      this.m_Color = (FsmColor) Color.white;
      this.m_EveryFrame = false;
    }

    public override void OnEnter()
    {
      this.m_Color.Value = RenderSettings.ambientLight;
      if (this.m_EveryFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.m_Color.Value = RenderSettings.ambientLight;
    }
  }
}
