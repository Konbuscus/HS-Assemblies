﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SampleCurve
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Math)]
  [Tooltip("Gets the value of a curve at a given time and stores it in a Float Variable. NOTE: This can be used for more than just animation! It's a general way to transform an input number into an output number using a curve (e.g., linear input -> bell curve).")]
  public class SampleCurve : FsmStateAction
  {
    [RequiredField]
    public FsmAnimationCurve curve;
    [RequiredField]
    public FsmFloat sampleAt;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmFloat storeValue;
    public bool everyFrame;

    public override void Reset()
    {
      this.curve = (FsmAnimationCurve) null;
      this.sampleAt = (FsmFloat) null;
      this.storeValue = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSampleCurve();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSampleCurve();
    }

    private void DoSampleCurve()
    {
      if (this.curve == null || this.curve.curve == null || this.storeValue == null)
        return;
      this.storeValue.Value = this.curve.curve.Evaluate(this.sampleAt.Value);
    }
  }
}
