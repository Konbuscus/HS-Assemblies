﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.MainCameraShakerAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("[DEPRECATED - Use CameraShakerAction instead.] Shakes the Main Camera a specified amount over time.")]
  [ActionCategory("Pegasus")]
  public class MainCameraShakerAction : FsmStateAction
  {
    public FsmVector3 m_Amount;
    public FsmFloat m_Time;
    public FsmFloat m_Delay;
    public FsmEvent m_FinishedEvent;
    private float m_timerSec;
    private bool m_shakeFired;

    public override void Reset()
    {
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = false;
      this.m_Amount = fsmVector3;
      this.m_Time = (FsmFloat) 1f;
      this.m_Delay = (FsmFloat) 0.0f;
      this.m_FinishedEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      this.m_timerSec = 0.0f;
      this.m_shakeFired = false;
    }

    public override void OnUpdate()
    {
      this.m_timerSec += Time.deltaTime;
      if ((double) this.m_timerSec >= (double) this.m_Delay.Value && !this.m_shakeFired)
        this.Shake();
      if ((double) this.m_timerSec < (double) this.m_Delay.Value + (double) this.m_Time.Value)
        return;
      this.Fsm.Event(this.m_FinishedEvent);
      this.Finish();
    }

    private void Shake()
    {
      CameraShakeMgr.Shake(Camera.main, this.m_Amount.Value, this.m_Time.Value);
      this.m_shakeFired = true;
    }
  }
}
