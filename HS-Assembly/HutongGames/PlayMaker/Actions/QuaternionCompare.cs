﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.QuaternionCompare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Quaternion)]
  [Tooltip("Check if two quaternions are equals or not. Takes in account inversed representations of quaternions")]
  public class QuaternionCompare : QuaternionBaseAction
  {
    [RequiredField]
    [Tooltip("First Quaternion")]
    public FsmQuaternion Quaternion1;
    [RequiredField]
    [Tooltip("Second Quaternion")]
    public FsmQuaternion Quaternion2;
    [Tooltip("true if Quaternions are equal")]
    public FsmBool equal;
    [Tooltip("Event sent if Quaternions are equal")]
    public FsmEvent equalEvent;
    [Tooltip("Event sent if Quaternions are not equal")]
    public FsmEvent notEqualEvent;

    public override void Reset()
    {
      FsmQuaternion fsmQuaternion1 = new FsmQuaternion();
      fsmQuaternion1.UseVariable = true;
      this.Quaternion1 = fsmQuaternion1;
      FsmQuaternion fsmQuaternion2 = new FsmQuaternion();
      fsmQuaternion2.UseVariable = true;
      this.Quaternion2 = fsmQuaternion2;
      this.equal = (FsmBool) null;
      this.equalEvent = (FsmEvent) null;
      this.notEqualEvent = (FsmEvent) null;
      this.everyFrameOption = QuaternionBaseAction.everyFrameOptions.Update;
    }

    public override void OnEnter()
    {
      this.DoQuatCompare();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.Update)
        return;
      this.DoQuatCompare();
    }

    public override void OnLateUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.LateUpdate)
        return;
      this.DoQuatCompare();
    }

    public override void OnFixedUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.FixedUpdate)
        return;
      this.DoQuatCompare();
    }

    private void DoQuatCompare()
    {
      bool flag = (double) Mathf.Abs(Quaternion.Dot(this.Quaternion1.Value, this.Quaternion2.Value)) > 0.999998986721039;
      this.equal.Value = flag;
      if (flag)
        this.Fsm.Event(this.equalEvent);
      else
        this.Fsm.Event(this.notEqualEvent);
    }
  }
}
