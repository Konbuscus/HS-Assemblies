﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActivatePlaySoundSpellsAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Cause the Play Sound Spells to fire on the selected card")]
  public class ActivatePlaySoundSpellsAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    public SpellAction.Which m_WhichCard;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void OnEnter()
    {
      base.OnEnter();
      Card card = this.GetCard(this.m_WhichCard);
      if ((Object) card == (Object) null)
      {
        Error.AddDevFatal("ActivatePlaySoundSpellsAction.OnEnter() - Card not found!");
        this.Finish();
      }
      else
      {
        if ((Object) card != (Object) null)
          card.ActivateSoundSpellList(card.GetPlaySoundSpells(true));
        this.Finish();
      }
    }
  }
}
