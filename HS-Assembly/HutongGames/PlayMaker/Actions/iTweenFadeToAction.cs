﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.iTweenFadeToAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Changes a GameObject's alpha over time, if it supports alpha changes.")]
  public class iTweenFadeToAction : iTweenFsmAction
  {
    [Tooltip("The shape of the easing curve applied to the animation.")]
    public iTween.EaseType easeType = iTween.EaseType.linear;
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("iTween ID. If set you can use iTween Stop action to stop it by its id.")]
    public FsmString id;
    [Tooltip("An alpha value the GameObject will animate To.")]
    public FsmFloat m_Alpha;
    [Tooltip("The time in seconds the animation will take to complete.")]
    public FsmFloat time;
    [Tooltip("The time in seconds the animation will wait before beginning.")]
    public FsmFloat delay;
    [Tooltip("The type of loop to apply once the animation has completed.")]
    public iTween.LoopType loopType;
    [Tooltip("Run this action on all child objects.")]
    public FsmBool m_IncludeChildren;

    public override void Reset()
    {
      base.Reset();
      FsmString fsmString = new FsmString();
      fsmString.UseVariable = true;
      this.id = fsmString;
      this.m_Alpha = (FsmFloat) 0.0f;
      this.time = (FsmFloat) 1f;
      this.delay = (FsmFloat) 0.0f;
      this.easeType = iTween.EaseType.linear;
      this.loopType = iTween.LoopType.none;
    }

    public override void OnEnter()
    {
      this.OnEnteriTween(this.gameObject);
      if (this.loopType != iTween.LoopType.none)
        this.IsLoop(true);
      this.DoiTween();
    }

    public override void OnExit()
    {
      this.OnExitiTween(this.gameObject);
    }

    private void DoiTween()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((UnityEngine.Object) ownerDefaultTarget == (UnityEngine.Object) null)
        return;
      this.itweenType = "color";
      if (this.m_IncludeChildren.Value)
      {
        IEnumerator enumerator = ownerDefaultTarget.transform.GetEnumerator();
        try
        {
          while (enumerator.MoveNext())
            this.DoiTweenOnChild(((Component) enumerator.Current).gameObject);
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            disposable.Dispose();
        }
      }
      this.DoiTweenOnParent(ownerDefaultTarget);
    }

    private void DoiTweenOnChild(GameObject go)
    {
      Hashtable args = this.InitTweenArgTable();
      if ((double) this.time.Value <= 0.0)
      {
        args.Add((object) "time", (object) 0.0f);
        iTween.FadeUpdate(go, args);
      }
      else
      {
        args[(object) "time"] = (object) (float) (!this.time.IsNone ? (double) this.time.Value : 1.0);
        iTween.FadeTo(go, args);
      }
    }

    private void DoiTweenOnParent(GameObject go)
    {
      Hashtable args = this.InitTweenArgTable();
      if ((double) this.time.Value <= 0.0)
      {
        args.Add((object) "time", (object) 0.0f);
        iTween.FadeUpdate(go, args);
        this.Fsm.Event(this.startEvent);
        this.Fsm.Event(this.finishEvent);
        this.Finish();
      }
      else
      {
        args[(object) "time"] = (object) (float) (!this.time.IsNone ? (double) this.time.Value : 1.0);
        args.Add((object) "oncomplete", (object) "iTweenOnComplete");
        args.Add((object) "oncompleteparams", (object) this.itweenID);
        args.Add((object) "onstart", (object) "iTweenOnStart");
        args.Add((object) "onstartparams", (object) this.itweenID);
        iTween.FadeTo(go, args);
      }
    }

    private Hashtable InitTweenArgTable()
    {
      return new Hashtable() { { (object) "alpha", (object) this.m_Alpha.Value }, { (object) "name", !this.id.IsNone ? (object) this.id.Value : (object) string.Empty }, { (object) "delay", (object) (float) (!this.delay.IsNone ? (double) this.delay.Value : 0.0) }, { (object) "easetype", (object) this.easeType }, { (object) "looptype", (object) this.loopType }, { (object) "ignoretimescale", (object) (bool) (!this.realTime.IsNone ? (this.realTime.Value ? 1 : 0) : 0) } };
    }
  }
}
