﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.UseGravity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Sets whether a Game Object's Rigidy Body is affected by Gravity.")]
  public class UseGravity : ComponentAction<Rigidbody>
  {
    [CheckForComponent(typeof (Rigidbody))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    public FsmBool useGravity;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.useGravity = (FsmBool) true;
    }

    public override void OnEnter()
    {
      this.DoUseGravity();
      this.Finish();
    }

    private void DoUseGravity()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.rigidbody.useGravity = this.useGravity.Value;
    }
  }
}
