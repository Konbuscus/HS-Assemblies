﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetKeyUp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sends an Event when a Key is released.")]
  [ActionCategory(ActionCategory.Input)]
  public class GetKeyUp : FsmStateAction
  {
    [RequiredField]
    public KeyCode key;
    public FsmEvent sendEvent;
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;

    public override void Reset()
    {
      this.sendEvent = (FsmEvent) null;
      this.key = KeyCode.None;
      this.storeResult = (FsmBool) null;
    }

    public override void OnUpdate()
    {
      bool keyUp = Input.GetKeyUp(this.key);
      if (keyUp)
        this.Fsm.Event(this.sendEvent);
      this.storeResult.Value = keyUp;
    }
  }
}
