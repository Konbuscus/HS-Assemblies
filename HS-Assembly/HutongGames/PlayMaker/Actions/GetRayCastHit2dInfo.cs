﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics2D)]
  [Tooltip("Gets info on the last 2d Raycast or LineCast and store in variables.")]
  public class GetRayCastHit2dInfo : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the GameObject hit by the last Raycast and store it in a variable.")]
    public FsmGameObject gameObjectHit;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the world position of the ray hit point and store it in a variable.")]
    [Title("Hit Point")]
    public FsmVector2 point;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the normal at the hit point and store it in a variable.")]
    public FsmVector3 normal;
    [Tooltip("Get the distance along the ray to the hit point and store it in a variable.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat distance;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObjectHit = (FsmGameObject) null;
      this.point = (FsmVector2) null;
      this.normal = (FsmVector3) null;
      this.distance = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.StoreRaycastInfo();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.StoreRaycastInfo();
    }

    private void StoreRaycastInfo()
    {
      RaycastHit2D raycastHit2Dinfo = Fsm.GetLastRaycastHit2DInfo(this.Fsm);
      if (!((Object) raycastHit2Dinfo.collider != (Object) null))
        return;
      this.gameObjectHit.Value = raycastHit2Dinfo.collider.gameObject;
      this.point.Value = raycastHit2Dinfo.point;
      this.normal.Value = (Vector3) raycastHit2Dinfo.normal;
      this.distance.Value = raycastHit2Dinfo.fraction;
    }
  }
}
