﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.EnableAnimation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animation)]
  [Tooltip("Enables/Disables an Animation on a GameObject.\nAnimation time is paused while disabled. Animation must also have a non zero weight to play.")]
  public class EnableAnimation : BaseAnimationAction
  {
    [RequiredField]
    [CheckForComponent(typeof (Animation))]
    [Tooltip("The GameObject playing the animation.")]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [UIHint(UIHint.Animation)]
    [Tooltip("The name of the animation to enable/disable.")]
    public FsmString animName;
    [Tooltip("Set to True to enable, False to disable.")]
    [RequiredField]
    public FsmBool enable;
    [Tooltip("Reset the initial enabled state when exiting the state.")]
    public FsmBool resetOnExit;
    private AnimationState anim;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.animName = (FsmString) null;
      this.enable = (FsmBool) true;
      this.resetOnExit = (FsmBool) false;
    }

    public override void OnEnter()
    {
      this.DoEnableAnimation(this.Fsm.GetOwnerDefaultTarget(this.gameObject));
      this.Finish();
    }

    private void DoEnableAnimation(GameObject go)
    {
      if (!this.UpdateCache(go))
        return;
      this.anim = this.animation[this.animName.Value];
      if (!((TrackedReference) this.anim != (TrackedReference) null))
        return;
      this.anim.enabled = this.enable.Value;
    }

    public override void OnExit()
    {
      if (!this.resetOnExit.Value || !((TrackedReference) this.anim != (TrackedReference) null))
        return;
      this.anim.enabled = !this.enable.Value;
    }
  }
}
