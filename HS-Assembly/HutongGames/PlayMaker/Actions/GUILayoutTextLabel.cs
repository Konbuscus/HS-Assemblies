﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutTextLabel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("GUILayout Label for simple text.")]
  [ActionCategory(ActionCategory.GUILayout)]
  public class GUILayoutTextLabel : GUILayoutAction
  {
    [Tooltip("Text to display.")]
    public FsmString text;
    [Tooltip("Optional GUIStyle in the active GUISkin.")]
    public FsmString style;

    public override void Reset()
    {
      base.Reset();
      this.text = (FsmString) string.Empty;
      this.style = (FsmString) string.Empty;
    }

    public override void OnGUI()
    {
      if (string.IsNullOrEmpty(this.style.Value))
        GUILayout.Label(new GUIContent(this.text.Value), this.LayoutOptions);
      else
        GUILayout.Label(new GUIContent(this.text.Value), (GUIStyle) this.style.Value, this.LayoutOptions);
    }
  }
}
