﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetLastEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the event that caused the transition to the current state, and stores it in a String Variable.")]
  [ActionCategory(ActionCategory.StateMachine)]
  public class GetLastEvent : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    public FsmString storeEvent;

    public override void Reset()
    {
      this.storeEvent = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.storeEvent.Value = this.Fsm.LastTransition != null ? this.Fsm.LastTransition.EventName : "START";
      this.Finish();
    }
  }
}
