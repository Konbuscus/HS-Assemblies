﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetRoot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the top most parent of the Game Object.\nIf the game object has no parent, returns itself.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class GetRoot : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmGameObject storeRoot;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.storeRoot = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      this.DoGetRoot();
      this.Finish();
    }

    private void DoGetRoot()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      this.storeRoot.Value = ownerDefaultTarget.transform.root.gameObject;
    }
  }
}
