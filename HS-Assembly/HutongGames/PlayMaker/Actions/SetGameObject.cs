﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGameObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the value of a Game Object Variable.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class SetGameObject : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmGameObject variable;
    public FsmGameObject gameObject;
    public bool everyFrame;

    public override void Reset()
    {
      this.variable = (FsmGameObject) null;
      this.gameObject = (FsmGameObject) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.variable.Value = this.gameObject.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.variable.Value = this.gameObject.Value;
    }
  }
}
