﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetTriggerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Gets info on the last Trigger event and store in variables.")]
  public class GetTriggerInfo : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    public FsmGameObject gameObjectHit;
    [UIHint(UIHint.Variable)]
    [Tooltip("Useful for triggering different effects. Audio, particles...")]
    public FsmString physicsMaterialName;

    public override void Reset()
    {
      this.gameObjectHit = (FsmGameObject) null;
      this.physicsMaterialName = (FsmString) null;
    }

    private void StoreTriggerInfo()
    {
      if ((Object) this.Fsm.TriggerCollider == (Object) null)
        return;
      this.gameObjectHit.Value = this.Fsm.TriggerCollider.gameObject;
      this.physicsMaterialName.Value = this.Fsm.TriggerCollider.material.name;
    }

    public override void OnEnter()
    {
      this.StoreTriggerInfo();
      this.Finish();
    }
  }
}
