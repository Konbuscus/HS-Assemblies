﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetShadowColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Get shadow color")]
  public class GetShadowColor : FsmStateAction
  {
    public FsmColor m_Color;

    public override void Reset()
    {
      this.m_Color = (FsmColor) Color.black;
    }

    public override void OnEnter()
    {
      this.m_Color.Value = Board.Get().m_ShadowColor;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.m_Color.Value = Board.Get().m_ShadowColor;
    }
  }
}
