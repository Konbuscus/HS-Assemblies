﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.EaseRect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Easing Animation - Rect.")]
  [ActionCategory("AnimateVariables")]
  public class EaseRect : EaseFsmAction
  {
    [RequiredField]
    public FsmRect fromValue;
    [RequiredField]
    public FsmRect toValue;
    [UIHint(UIHint.Variable)]
    public FsmRect rectVariable;
    private bool finishInNextStep;

    public override void Reset()
    {
      base.Reset();
      this.rectVariable = (FsmRect) null;
      this.fromValue = (FsmRect) null;
      this.toValue = (FsmRect) null;
      this.finishInNextStep = false;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      this.fromFloats = new float[4];
      this.fromFloats[0] = this.fromValue.Value.x;
      this.fromFloats[1] = this.fromValue.Value.y;
      this.fromFloats[2] = this.fromValue.Value.width;
      this.fromFloats[3] = this.fromValue.Value.height;
      this.toFloats = new float[4];
      this.toFloats[0] = this.toValue.Value.x;
      this.toFloats[1] = this.toValue.Value.y;
      this.toFloats[2] = this.toValue.Value.width;
      this.toFloats[3] = this.toValue.Value.height;
      this.resultFloats = new float[4];
      this.finishInNextStep = false;
      this.rectVariable.Value = this.fromValue.Value;
    }

    public override void OnExit()
    {
      base.OnExit();
    }

    public override void OnUpdate()
    {
      base.OnUpdate();
      if (!this.rectVariable.IsNone && this.isRunning)
        this.rectVariable.Value = new Rect(this.resultFloats[0], this.resultFloats[1], this.resultFloats[2], this.resultFloats[3]);
      if (this.finishInNextStep)
      {
        this.Finish();
        if (this.finishEvent != null)
          this.Fsm.Event(this.finishEvent);
      }
      if (!this.finishAction || this.finishInNextStep)
        return;
      if (!this.rectVariable.IsNone)
        this.rectVariable.Value = new Rect(!this.reverse.IsNone ? (!this.reverse.Value ? this.toValue.Value.x : this.fromValue.Value.x) : this.toValue.Value.x, !this.reverse.IsNone ? (!this.reverse.Value ? this.toValue.Value.y : this.fromValue.Value.y) : this.toValue.Value.y, !this.reverse.IsNone ? (!this.reverse.Value ? this.toValue.Value.width : this.fromValue.Value.width) : this.toValue.Value.width, !this.reverse.IsNone ? (!this.reverse.Value ? this.toValue.Value.height : this.fromValue.Value.height) : this.toValue.Value.height);
      this.finishInNextStep = true;
    }
  }
}
