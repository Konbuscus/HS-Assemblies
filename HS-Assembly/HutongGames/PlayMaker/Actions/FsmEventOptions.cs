﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FsmEventOptions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.StateMachine)]
  [Tooltip("Sets how subsequent events sent in this state are handled.")]
  public class FsmEventOptions : FsmStateAction
  {
    public PlayMakerFSM sendToFsmComponent;
    public FsmGameObject sendToGameObject;
    public FsmString fsmName;
    public FsmBool sendToChildren;
    public FsmBool broadcastToAll;

    public override void Reset()
    {
      this.sendToFsmComponent = (PlayMakerFSM) null;
      this.sendToGameObject = (FsmGameObject) null;
      this.fsmName = (FsmString) string.Empty;
      this.sendToChildren = (FsmBool) false;
      this.broadcastToAll = (FsmBool) false;
    }

    public override void OnUpdate()
    {
    }
  }
}
