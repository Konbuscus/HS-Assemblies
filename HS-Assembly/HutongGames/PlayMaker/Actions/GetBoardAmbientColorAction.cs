﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetBoardAmbientColorAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get scene ambient color")]
  [ActionCategory("Pegasus")]
  public class GetBoardAmbientColorAction : FsmStateAction
  {
    public FsmColor m_Color;

    public override void Reset()
    {
      this.m_Color = (FsmColor) Color.white;
    }

    public override void OnEnter()
    {
      this.m_Color.Value = RenderSettings.ambientLight;
      Board board = Board.Get();
      if ((Object) board != (Object) null)
        this.m_Color.Value = board.m_AmbientColor;
      this.Finish();
    }
  }
}
