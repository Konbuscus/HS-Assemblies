﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetRandomObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Gets a Random Game Object from the scene.\nOptionally filter by Tag.")]
  public class GetRandomObject : FsmStateAction
  {
    [UIHint(UIHint.Tag)]
    public FsmString withTag;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmGameObject storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.withTag = (FsmString) "Untagged";
      this.storeResult = (FsmGameObject) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetRandomObject();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetRandomObject();
    }

    private void DoGetRandomObject()
    {
      GameObject[] gameObjectArray = !(this.withTag.Value != "Untagged") ? (GameObject[]) Object.FindObjectsOfType(typeof (GameObject)) : GameObject.FindGameObjectsWithTag(this.withTag.Value);
      if (gameObjectArray.Length > 0)
        this.storeResult.Value = gameObjectArray[Random.Range(0, gameObjectArray.Length)];
      else
        this.storeResult.Value = (GameObject) null;
    }
  }
}
