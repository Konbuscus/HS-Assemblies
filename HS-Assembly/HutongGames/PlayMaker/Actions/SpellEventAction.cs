﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellEventAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Tells the game that a Spell is finished, allowing the game to progress.")]
  public class SpellEventAction : SpellAction
  {
    public FsmFloat m_Delay = (FsmFloat) 0.0f;
    public FsmString m_EventName = (FsmString) string.Empty;
    public FsmOwnerDefault m_SpellObject;
    public FsmObject m_EventData;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      this.m_SpellObject = (FsmOwnerDefault) null;
      this.m_Delay = (FsmFloat) 0.0f;
      this.m_EventName = (FsmString) string.Empty;
      this.m_EventData = (FsmObject) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_spell == (Object) null)
      {
        UnityEngine.Debug.LogErrorFormat("{0}.OnEnter() - FAILED to find Spell component on Owner \"{1}\"", new object[2]
        {
          (object) this,
          (object) this.Owner
        });
      }
      else
      {
        if ((double) this.m_Delay.Value > 0.0)
          this.m_spell.StartCoroutine(this.DelaySpellEvent());
        else
          this.m_spell.OnSpellEvent(this.m_EventName.Value, (object) this.m_EventData.Value);
        this.Finish();
      }
    }

    [DebuggerHidden]
    private IEnumerator DelaySpellEvent()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SpellEventAction.\u003CDelaySpellEvent\u003Ec__Iterator297() { \u003C\u003Ef__this = this };
    }
  }
}
