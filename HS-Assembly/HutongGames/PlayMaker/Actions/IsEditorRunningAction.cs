﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.IsEditorRunningAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Is Editor Running sends Events based on the result.")]
  [ActionCategory("Pegasus")]
  public class IsEditorRunningAction : FsmStateAction
  {
    [Tooltip("Event to use if Editor is running.")]
    [RequiredField]
    public FsmEvent trueEvent;
    [Tooltip("Event to use if Editor is NOT running.")]
    public FsmEvent falseEvent;
    [Tooltip("Store the true/false result in a bool variable.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;

    public override void Reset()
    {
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
    }

    public override void OnEnter()
    {
      this.IsEditorRunning();
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.IsEditorRunning();
    }

    private void IsEditorRunning()
    {
      this.storeResult.Value = GeneralUtils.IsEditorPlaying();
      if (this.storeResult.Value)
        this.Fsm.Event(this.trueEvent);
      else
        this.Fsm.Event(this.falseEvent);
    }
  }
}
