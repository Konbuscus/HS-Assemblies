﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetVertexCount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the number of vertices in a GameObject's mesh. Useful in conjunction with GetVertexPosition.")]
  [ActionCategory("Mesh")]
  public class GetVertexCount : FsmStateAction
  {
    [CheckForComponent(typeof (MeshFilter))]
    [RequiredField]
    [Tooltip("The GameObject to check.")]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("Store the vertex count in a variable.")]
    [UIHint(UIHint.Variable)]
    public FsmInt storeCount;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.storeCount = (FsmInt) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetVertexCount();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetVertexCount();
    }

    private void DoGetVertexCount()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if (!((Object) ownerDefaultTarget != (Object) null))
        return;
      MeshFilter component = ownerDefaultTarget.GetComponent<MeshFilter>();
      if ((Object) component == (Object) null)
        this.LogError("Missing MeshFilter!");
      else
        this.storeCount.Value = component.mesh.vertexCount;
    }
  }
}
