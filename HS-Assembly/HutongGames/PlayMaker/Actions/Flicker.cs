﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Flicker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Randomly flickers a Game Object on/off.")]
  [ActionCategory(ActionCategory.Effects)]
  public class Flicker : ComponentAction<Renderer>
  {
    [Tooltip("The GameObject to flicker.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("The frequency of the flicker in seconds.")]
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat frequency;
    [HasFloatSlider(0.0f, 1f)]
    [Tooltip("Amount of time flicker is On (0-1). E.g. Use 0.95 for an occasional flicker.")]
    public FsmFloat amountOn;
    [Tooltip("Only effect the renderer, leaving other components active.")]
    public bool rendererOnly;
    [Tooltip("Ignore time scale. Useful if flickering UI when the game is paused.")]
    public bool realTime;
    private float startTime;
    private float timer;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.frequency = (FsmFloat) 0.1f;
      this.amountOn = (FsmFloat) 0.5f;
      this.rendererOnly = true;
      this.realTime = false;
    }

    public override void OnEnter()
    {
      this.startTime = FsmTime.RealtimeSinceStartup;
      this.timer = 0.0f;
    }

    public override void OnUpdate()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if (this.realTime)
        this.timer = FsmTime.RealtimeSinceStartup - this.startTime;
      else
        this.timer += Time.deltaTime;
      if ((double) this.timer <= (double) this.frequency.Value)
        return;
      bool flag = (double) Random.Range(0.0f, 1f) < (double) this.amountOn.Value;
      if (this.rendererOnly)
      {
        if (this.UpdateCache(ownerDefaultTarget))
          this.renderer.enabled = flag;
      }
      else
        ownerDefaultTarget.SetActive(flag);
      this.startTime = this.timer;
      this.timer = 0.0f;
    }
  }
}
