﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetJointConnectedBody
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Connect a joint to a game object.")]
  public class SetJointConnectedBody : FsmStateAction
  {
    [CheckForComponent(typeof (Joint))]
    [RequiredField]
    [Tooltip("The joint to connect. Requires a Joint component.")]
    public FsmOwnerDefault joint;
    [CheckForComponent(typeof (Rigidbody))]
    [Tooltip("The game object to connect to the Joint. Set to none to connect the Joint to the world.")]
    public FsmGameObject rigidBody;

    public override void Reset()
    {
      this.joint = (FsmOwnerDefault) null;
      this.rigidBody = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.joint);
      if ((Object) ownerDefaultTarget != (Object) null)
      {
        Joint component = ownerDefaultTarget.GetComponent<Joint>();
        if ((Object) component != (Object) null)
          component.connectedBody = !((Object) this.rigidBody.Value == (Object) null) ? this.rigidBody.Value.GetComponent<Rigidbody>() : (Rigidbody) null;
      }
      this.Finish();
    }
  }
}
