﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetSkybox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the global Skybox.")]
  [ActionCategory(ActionCategory.RenderSettings)]
  public class SetSkybox : FsmStateAction
  {
    public FsmMaterial skybox;
    [Tooltip("Repeat every frame. Useful if the Skybox is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.skybox = (FsmMaterial) null;
    }

    public override void OnEnter()
    {
      RenderSettings.skybox = this.skybox.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      RenderSettings.skybox = this.skybox.Value;
    }
  }
}
