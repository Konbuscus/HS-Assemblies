﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.StopAnimation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Stops all playing Animations on a Game Object. Optionally, specify a single Animation to Stop.")]
  [ActionCategory(ActionCategory.Animation)]
  public class StopAnimation : BaseAnimationAction
  {
    [RequiredField]
    [CheckForComponent(typeof (Animation))]
    public FsmOwnerDefault gameObject;
    [Tooltip("Leave empty to stop all playing animations.")]
    [UIHint(UIHint.Animation)]
    public FsmString animName;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.animName = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.DoStopAnimation();
      this.Finish();
    }

    private void DoStopAnimation()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      if (FsmString.IsNullOrEmpty(this.animName))
        this.animation.Stop();
      else
        this.animation.Stop(this.animName.Value);
    }
  }
}
