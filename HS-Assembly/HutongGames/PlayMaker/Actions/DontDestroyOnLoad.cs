﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DontDestroyOnLoad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Makes the Game Object not be destroyed automatically when loading a new scene.")]
  [ActionCategory(ActionCategory.Level)]
  public class DontDestroyOnLoad : FsmStateAction
  {
    [RequiredField]
    [Tooltip("GameObject to mark as DontDestroyOnLoad.")]
    public FsmOwnerDefault gameObject;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      Object.DontDestroyOnLoad((Object) this.Owner.transform.root.gameObject);
      this.Finish();
    }
  }
}
