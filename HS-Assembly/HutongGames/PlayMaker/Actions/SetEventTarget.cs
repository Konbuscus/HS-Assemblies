﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetEventTarget
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the target FSM for all subsequent events sent by this state. The default 'Self' sends events to this FSM.")]
  [ActionCategory(ActionCategory.StateMachine)]
  public class SetEventTarget : FsmStateAction
  {
    public FsmEventTarget eventTarget;

    public override void Reset()
    {
    }

    public override void OnEnter()
    {
      this.Fsm.EventTarget = this.eventTarget;
      this.Finish();
    }
  }
}
