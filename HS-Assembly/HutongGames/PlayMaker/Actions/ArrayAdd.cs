﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayAdd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Array)]
  [Tooltip("Add an item to the end of an Array.")]
  public class ArrayAdd : FsmStateAction
  {
    [Tooltip("The Array Variable to use.")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmArray array;
    [Tooltip("Item to add.")]
    [RequiredField]
    [MatchElementType("array")]
    public FsmVar value;

    public override void Reset()
    {
      this.array = (FsmArray) null;
      this.value = (FsmVar) null;
    }

    public override void OnEnter()
    {
      this.DoAddValue();
      this.Finish();
    }

    private void DoAddValue()
    {
      this.array.Resize(this.array.Length + 1);
      this.value.UpdateValue();
      this.array.Set(this.array.Length - 1, this.value.GetValue());
    }
  }
}
