﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellMultiTargetDebugAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("[DEBUG] Setup a Spell to affect multiple targets.")]
  [ActionCategory("Pegasus")]
  public class SpellMultiTargetDebugAction : SpellAction
  {
    public FsmGameObject m_SpellObject;
    public FsmGameObject m_SourceObject;
    public FsmGameObject[] m_TargetObjects;

    protected override GameObject GetSpellOwner()
    {
      return this.m_SpellObject.Value;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_spell == (Object) null)
        return;
      this.m_spell.SetSource(this.m_SourceObject.Value);
      this.m_spell.RemoveAllTargets();
      for (int index = 0; index < this.m_TargetObjects.Length; ++index)
      {
        FsmGameObject targetObject = this.m_TargetObjects[index];
        if (!((Object) targetObject.Value == (Object) null) && !this.m_spell.IsTarget(targetObject.Value))
          this.m_spell.AddTarget(targetObject.Value);
      }
      this.Finish();
    }
  }
}
