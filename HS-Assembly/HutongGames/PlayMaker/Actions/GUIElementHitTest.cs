﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUIElementHitTest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUIElement)]
  [Tooltip("Performs a Hit Test on a Game Object with a GUITexture or GUIText component.")]
  public class GUIElementHitTest : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject that has a GUITexture or GUIText component.")]
    [CheckForComponent(typeof (GUIElement))]
    public FsmOwnerDefault gameObject;
    [Tooltip("Specify camera or use MainCamera as default.")]
    public Camera camera;
    [Tooltip("A vector position on screen. Usually stored by actions like GetTouchInfo, or World To Screen Point.")]
    public FsmVector3 screenPoint;
    [Tooltip("Specify screen X coordinate.")]
    public FsmFloat screenX;
    [Tooltip("Specify screen Y coordinate.")]
    public FsmFloat screenY;
    [Tooltip("Whether the specified screen coordinates are normalized (0-1).")]
    public FsmBool normalized;
    [Tooltip("Event to send if the Hit Test is true.")]
    public FsmEvent hitEvent;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result of the Hit Test in a bool variable (true/false).")]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame. Useful if you want to wait for the hit test to return true.")]
    public FsmBool everyFrame;
    private GUIElement guiElement;
    private GameObject gameObjectCached;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.camera = (Camera) null;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.screenPoint = fsmVector3;
      FsmFloat fsmFloat1 = new FsmFloat();
      fsmFloat1.UseVariable = true;
      this.screenX = fsmFloat1;
      FsmFloat fsmFloat2 = new FsmFloat();
      fsmFloat2.UseVariable = true;
      this.screenY = fsmFloat2;
      this.normalized = (FsmBool) true;
      this.hitEvent = (FsmEvent) null;
      this.everyFrame = (FsmBool) true;
    }

    public override void OnEnter()
    {
      this.DoHitTest();
      if (this.everyFrame.Value)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoHitTest();
    }

    private void DoHitTest()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((Object) ownerDefaultTarget != (Object) this.gameObjectCached)
      {
        this.guiElement = (GUIElement) ownerDefaultTarget.GetComponent<GUITexture>() ?? (GUIElement) ownerDefaultTarget.GetComponent<GUIText>();
        this.gameObjectCached = ownerDefaultTarget;
      }
      if ((Object) this.guiElement == (Object) null)
      {
        this.Finish();
      }
      else
      {
        Vector3 screenPosition = !this.screenPoint.IsNone ? this.screenPoint.Value : new Vector3(0.0f, 0.0f);
        if (!this.screenX.IsNone)
          screenPosition.x = this.screenX.Value;
        if (!this.screenY.IsNone)
          screenPosition.y = this.screenY.Value;
        if (this.normalized.Value)
        {
          screenPosition.x *= (float) Screen.width;
          screenPosition.y *= (float) Screen.height;
        }
        if (this.guiElement.HitTest(screenPosition, this.camera))
        {
          this.storeResult.Value = true;
          this.Fsm.Event(this.hitEvent);
        }
        else
          this.storeResult.Value = false;
      }
    }
  }
}
