﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DrawFullscreenColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Fills the screen with a Color. NOTE: Uses OnGUI so you need a PlayMakerGUI component in the scene.")]
  [ActionCategory(ActionCategory.GUI)]
  public class DrawFullscreenColor : FsmStateAction
  {
    [RequiredField]
    [Tooltip("Color. NOTE: Uses OnGUI so you need a PlayMakerGUI component in the scene.")]
    public FsmColor color;

    public override void Reset()
    {
      this.color = (FsmColor) Color.white;
    }

    public override void OnGUI()
    {
      Color color = GUI.color;
      GUI.color = this.color.Value;
      GUI.DrawTexture(new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height), (Texture) ActionHelpers.WhiteTexture);
      GUI.color = color;
    }
  }
}
