﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActorAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("INTERNAL USE ONLY. Do not put this on your FSMs.")]
  [ActionCategory("Pegasus")]
  public abstract class ActorAction : FsmStateAction
  {
    protected Actor m_actor;

    public Actor GetActor()
    {
      if ((Object) this.m_actor == (Object) null)
      {
        GameObject actorOwner = this.GetActorOwner();
        if ((Object) actorOwner != (Object) null)
          this.m_actor = SceneUtils.FindComponentInThisOrParents<Actor>(actorOwner);
      }
      return this.m_actor;
    }

    protected abstract GameObject GetActorOwner();

    public override void Reset()
    {
    }

    public override void OnEnter()
    {
      this.GetActor();
      if (!((Object) this.m_actor == (Object) null))
        return;
      Debug.LogError((object) string.Format("{0}.OnEnter() - FAILED to find Actor component on Owner \"{1}\"", (object) this, (object) this.Owner));
    }
  }
}
