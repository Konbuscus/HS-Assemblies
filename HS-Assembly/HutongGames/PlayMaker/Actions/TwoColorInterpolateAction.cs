﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.TwoColorInterpolateAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Interpolate 2 Colors over a specified amount of Time.")]
  [ActionCategory("Pegasus")]
  public class TwoColorInterpolateAction : FsmStateAction
  {
    [RequiredField]
    [Tooltip("Color 1")]
    public FsmColor color1;
    [RequiredField]
    [Tooltip("Color 2")]
    public FsmColor color2;
    [Tooltip("Interpolation time.")]
    [RequiredField]
    public FsmFloat time;
    [Tooltip("Store the interpolated color in a Color variable.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmColor storeColor;
    [Tooltip("Event to send when the interpolation finishes.")]
    public FsmEvent finishEvent;
    [Tooltip("Ignore TimeScale")]
    public bool realTime;
    private float startTime;
    private float currentTime;

    public override void Reset()
    {
      this.color1 = new FsmColor();
      this.color2 = new FsmColor();
      this.color1.Value = Color.black;
      this.color2.Value = Color.white;
      this.time = (FsmFloat) 1f;
      this.storeColor = (FsmColor) null;
      this.finishEvent = (FsmEvent) null;
      this.realTime = false;
    }

    public override void OnEnter()
    {
      this.startTime = FsmTime.RealtimeSinceStartup;
      this.currentTime = 0.0f;
      this.storeColor.Value = this.color1.Value;
    }

    public override void OnUpdate()
    {
      if (this.realTime)
        this.currentTime = FsmTime.RealtimeSinceStartup - this.startTime;
      else
        this.currentTime += Time.deltaTime;
      if ((double) this.currentTime > (double) this.time.Value)
      {
        this.Finish();
        this.storeColor.Value = this.color2.Value;
        if (this.finishEvent == null)
          return;
        this.Fsm.Event(this.finishEvent);
      }
      else
      {
        float t = this.currentTime / this.time.Value;
        this.storeColor.Value = !t.Equals(0.0f) ? ((double) t < 1.0 ? Color.Lerp(this.color1.Value, this.color2.Value, t) : this.color2.Value) : this.color1.Value;
      }
    }
  }
}
