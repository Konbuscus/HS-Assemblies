﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ApplicationRunInBackground
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Application)]
  [Tooltip("Sets if the Application should play in the background. Useful for servers or testing network games on one machine.")]
  public class ApplicationRunInBackground : FsmStateAction
  {
    public FsmBool runInBackground;

    public override void Reset()
    {
      this.runInBackground = (FsmBool) true;
    }

    public override void OnEnter()
    {
      Application.runInBackground = this.runInBackground.Value;
      this.Finish();
    }
  }
}
