﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimatorCrossFade
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Create a dynamic transition between the current state and the destination state.Both state as to be on the same layer. note: You cannot change the current state on a synchronized layer, you need to change it on the referenced layer.")]
  [ActionCategory(ActionCategory.Animator)]
  public class AnimatorCrossFade : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The target. An Animator component is required")]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [Tooltip("The name of the state that will be played.")]
    public FsmString stateName;
    [Tooltip("The duration of the transition. Value is in source state normalized time.")]
    public FsmFloat transitionDuration;
    [Tooltip("Layer index containing the destination state. Leave to none to ignore")]
    public FsmInt layer;
    [Tooltip("Start time of the current destination state. Value is in source state normalized time, should be between 0 and 1.")]
    public FsmFloat normalizedTime;
    private Animator _animator;
    private int _paramID;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.stateName = (FsmString) null;
      this.transitionDuration = (FsmFloat) 1f;
      FsmInt fsmInt = new FsmInt();
      fsmInt.UseVariable = true;
      this.layer = fsmInt;
      FsmFloat fsmFloat = new FsmFloat();
      fsmFloat.UseVariable = true;
      this.normalizedTime = fsmFloat;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator != (Object) null)
          this._animator.CrossFade(this.stateName.Value, this.transitionDuration.Value, !this.layer.IsNone ? this.layer.Value : -1, !this.normalizedTime.IsNone ? this.normalizedTime.Value : float.NegativeInfinity);
        this.Finish();
      }
    }
  }
}
