﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimatorFollow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Follow a target")]
  [HelpUrl("https://hutonggames.fogbugz.com/default.asp?W1033")]
  [ActionCategory("Animator")]
  public class AnimatorFollow : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The GameObject. An Animator component and a PlayMakerAnimatorProxy component are required")]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The Game Object to target.")]
    public FsmGameObject target;
    [Tooltip("The minimum distance to follow.")]
    public FsmFloat minimumDistance;
    [Tooltip("The damping for following up.")]
    public FsmFloat speedDampTime;
    [Tooltip("The damping for turning.")]
    public FsmFloat directionDampTime;
    private GameObject _go;
    private PlayMakerAnimatorMoveProxy _animatorProxy;
    private Animator avatar;
    private CharacterController controller;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.target = (FsmGameObject) null;
      this.speedDampTime = (FsmFloat) 0.25f;
      this.directionDampTime = (FsmFloat) 0.25f;
      this.minimumDistance = (FsmFloat) 1f;
    }

    public override void OnEnter()
    {
      this._go = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((UnityEngine.Object) this._go == (UnityEngine.Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animatorProxy = this._go.GetComponent<PlayMakerAnimatorMoveProxy>();
        if ((UnityEngine.Object) this._animatorProxy != (UnityEngine.Object) null)
          this._animatorProxy.OnAnimatorMoveEvent += new Action(this.OnAnimatorMoveEvent);
        this.avatar = this._go.GetComponent<Animator>();
        this.controller = this._go.GetComponent<CharacterController>();
        this.avatar.speed = 1f + UnityEngine.Random.Range(-0.4f, 0.4f);
      }
    }

    public override void OnUpdate()
    {
      GameObject gameObject = this.target.Value;
      float dampTime1 = this.speedDampTime.Value;
      float dampTime2 = this.directionDampTime.Value;
      float num = this.minimumDistance.Value;
      if (!(bool) ((UnityEngine.Object) this.avatar) || !(bool) ((UnityEngine.Object) gameObject))
        return;
      if ((double) Vector3.Distance(gameObject.transform.position, this.avatar.rootPosition) > (double) num)
      {
        this.avatar.SetFloat("Speed", 1f, dampTime1, Time.deltaTime);
        Vector3 lhs = this.avatar.rootRotation * Vector3.forward;
        Vector3 normalized = (gameObject.transform.position - this.avatar.rootPosition).normalized;
        if ((double) Vector3.Dot(lhs, normalized) > 0.0)
          this.avatar.SetFloat("Direction", Vector3.Cross(lhs, normalized).y, dampTime2, Time.deltaTime);
        else
          this.avatar.SetFloat("Direction", (double) Vector3.Cross(lhs, normalized).y <= 0.0 ? -1f : 1f, dampTime2, Time.deltaTime);
      }
      else
        this.avatar.SetFloat("Speed", 0.0f, dampTime1, Time.deltaTime);
      if (!((UnityEngine.Object) this._animatorProxy == (UnityEngine.Object) null))
        return;
      this.OnAnimatorMoveEvent();
    }

    public override void OnExit()
    {
      if (!((UnityEngine.Object) this._animatorProxy != (UnityEngine.Object) null))
        return;
      this._animatorProxy.OnAnimatorMoveEvent -= new Action(this.OnAnimatorMoveEvent);
    }

    public void OnAnimatorMoveEvent()
    {
      int num = (int) this.controller.Move(this.avatar.deltaPosition);
      this._go.transform.rotation = this.avatar.rootRotation;
    }
  }
}
