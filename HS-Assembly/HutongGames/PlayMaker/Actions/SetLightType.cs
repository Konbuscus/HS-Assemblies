﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetLightType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set Spot, Directional, or Point Light type.")]
  [ActionCategory(ActionCategory.Lights)]
  public class SetLightType : ComponentAction<Light>
  {
    [CheckForComponent(typeof (Light))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [ObjectType(typeof (LightType))]
    public FsmEnum lightType;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.lightType = FsmEnum.op_Implicit((Enum) LightType.Point);
    }

    public override void OnEnter()
    {
      this.DoSetLightType();
      this.Finish();
    }

    private void DoSetLightType()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.light.type = (LightType) this.lightType.get_Value();
    }
  }
}
