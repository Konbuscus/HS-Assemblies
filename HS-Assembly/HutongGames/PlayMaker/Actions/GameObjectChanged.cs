﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GameObjectChanged
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if the value of a GameObject variable changed. Use this to send an event on change, or store a bool that can be used in other operations.")]
  [ActionCategory(ActionCategory.Logic)]
  public class GameObjectChanged : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("The GameObject variable to watch for a change.")]
    public FsmGameObject gameObjectVariable;
    [Tooltip("Event to send if the variable changes.")]
    public FsmEvent changedEvent;
    [Tooltip("Set to True if the variable changes.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    private GameObject previousValue;

    public override void Reset()
    {
      this.gameObjectVariable = (FsmGameObject) null;
      this.changedEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
    }

    public override void OnEnter()
    {
      if (this.gameObjectVariable.IsNone)
        this.Finish();
      else
        this.previousValue = this.gameObjectVariable.Value;
    }

    public override void OnUpdate()
    {
      this.storeResult.Value = false;
      if (!((Object) this.gameObjectVariable.Value != (Object) this.previousValue))
        return;
      this.storeResult.Value = true;
      this.Fsm.Event(this.changedEvent);
    }
  }
}
