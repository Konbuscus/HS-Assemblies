﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DeviceVibrate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Causes the device to vibrate for half a second.")]
  [ActionCategory(ActionCategory.Device)]
  public class DeviceVibrate : FsmStateAction
  {
    public override void Reset()
    {
    }

    public override void OnEnter()
    {
      this.Finish();
    }
  }
}
