﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetScreenHeight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the Height of the Screen in pixels.")]
  [ActionCategory(ActionCategory.Application)]
  public class GetScreenHeight : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmFloat storeScreenHeight;

    public override void Reset()
    {
      this.storeScreenHeight = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      this.storeScreenHeight.Value = (float) Screen.height;
      this.Finish();
    }
  }
}
