﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector2Normalize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Normalizes a Vector2 Variable.")]
  [ActionCategory(ActionCategory.Vector2)]
  public class Vector2Normalize : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The vector to normalize")]
    public FsmVector2 vector2Variable;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.vector2Variable = (FsmVector2) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.vector2Variable.Value = this.vector2Variable.Value.normalized;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.vector2Variable.Value = this.vector2Variable.Value.normalized;
    }
  }
}
