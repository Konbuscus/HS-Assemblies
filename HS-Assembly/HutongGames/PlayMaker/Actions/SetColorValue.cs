﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetColorValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the value of a Color Variable.")]
  [ActionCategory(ActionCategory.Color)]
  public class SetColorValue : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmColor colorVariable;
    [RequiredField]
    public FsmColor color;
    public bool everyFrame;

    public override void Reset()
    {
      this.colorVariable = (FsmColor) null;
      this.color = (FsmColor) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetColorValue();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetColorValue();
    }

    private void DoSetColorValue()
    {
      if (this.colorVariable == null)
        return;
      this.colorVariable.Value = this.color.Value;
    }
  }
}
