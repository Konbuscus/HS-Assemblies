﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetMaterialFloat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets a named float in a game object's material.")]
  [ActionCategory(ActionCategory.Material)]
  public class SetMaterialFloat : ComponentAction<Renderer>
  {
    [Tooltip("The GameObject that the material is applied to.")]
    [CheckForComponent(typeof (Renderer))]
    public FsmOwnerDefault gameObject;
    [Tooltip("GameObjects can have multiple materials. Specify an index to target a specific material.")]
    public FsmInt materialIndex;
    [Tooltip("Alternatively specify a Material instead of a GameObject and Index.")]
    public FsmMaterial material;
    [Tooltip("A named float parameter in the shader.")]
    [RequiredField]
    public FsmString namedFloat;
    [Tooltip("Set the parameter value.")]
    [RequiredField]
    public FsmFloat floatValue;
    [Tooltip("Repeat every frame. Useful if the value is animated.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.materialIndex = (FsmInt) 0;
      this.material = (FsmMaterial) null;
      this.namedFloat = (FsmString) string.Empty;
      this.floatValue = (FsmFloat) 0.0f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetMaterialFloat();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetMaterialFloat();
    }

    private void DoSetMaterialFloat()
    {
      if ((Object) this.material.Value != (Object) null)
      {
        this.material.Value.SetFloat(this.namedFloat.Value, this.floatValue.Value);
      }
      else
      {
        if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
          return;
        if ((Object) this.renderer.material == (Object) null)
          this.LogError("Missing Material!");
        else if (this.materialIndex.Value == 0)
        {
          this.renderer.material.SetFloat(this.namedFloat.Value, this.floatValue.Value);
        }
        else
        {
          if (this.renderer.materials.Length <= this.materialIndex.Value)
            return;
          Material[] materials = this.renderer.materials;
          materials[this.materialIndex.Value].SetFloat(this.namedFloat.Value, this.floatValue.Value);
          this.renderer.materials = materials;
        }
      }
    }
  }
}
