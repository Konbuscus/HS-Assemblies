﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetCameraFOV
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets Field of View used by the Camera.")]
  [ActionCategory(ActionCategory.Camera)]
  public class SetCameraFOV : ComponentAction<Camera>
  {
    [CheckForComponent(typeof (Camera))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    public FsmFloat fieldOfView;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.fieldOfView = (FsmFloat) 50f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetCameraFOV();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetCameraFOV();
    }

    private void DoSetCameraFOV()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.camera.fieldOfView = this.fieldOfView.Value;
    }
  }
}
