﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Sets a Game Object's Tag.")]
  public class SetTag : FsmStateAction
  {
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Tag)]
    public FsmString tag;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.tag = (FsmString) "Untagged";
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget != (Object) null)
        ownerDefaultTarget.tag = this.tag.Value;
      this.Finish();
    }
  }
}
