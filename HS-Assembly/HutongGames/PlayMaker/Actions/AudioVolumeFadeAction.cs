﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioVolumeFadeAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus Audio")]
  [Tooltip("Fades an Audio Source component's volume towards a target value.")]
  public class AudioVolumeFadeAction : FsmStateAction
  {
    [CheckForComponent(typeof (AudioSource))]
    [RequiredField]
    public FsmOwnerDefault m_GameObject;
    [RequiredField]
    public FsmFloat m_FadeTime;
    [RequiredField]
    public FsmFloat m_TargetVolume;
    [Tooltip("Stop the audio source when the target volume is reached.")]
    public FsmBool m_StopWhenDone;
    [Tooltip("Use real time. Useful if time is scaled and you don't want this action to scale.")]
    public FsmBool m_RealTime;
    private AudioSource m_audio;
    private float m_startVolume;
    private float m_startTime;
    private float m_currentTime;
    private float m_endTime;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_FadeTime = (FsmFloat) 1f;
      this.m_TargetVolume = (FsmFloat) 0.0f;
      this.m_StopWhenDone = (FsmBool) true;
      this.m_RealTime = (FsmBool) false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this.m_audio = ownerDefaultTarget.GetComponent<AudioSource>();
        if ((Object) this.m_audio == (Object) null)
          this.Finish();
        else if ((double) this.m_FadeTime.Value <= (double) Mathf.Epsilon)
        {
          this.Finish();
        }
        else
        {
          this.m_startVolume = SoundManager.Get().GetVolume(this.m_audio);
          this.m_startTime = FsmTime.RealtimeSinceStartup;
          this.m_currentTime = this.m_startTime;
          this.m_endTime = this.m_startTime + this.m_FadeTime.Value;
          SoundManager.Get().SetVolume(this.m_audio, this.m_startVolume);
        }
      }
    }

    public override void OnUpdate()
    {
      if (this.m_RealTime.Value)
        this.m_currentTime = FsmTime.RealtimeSinceStartup - this.m_startTime;
      else
        this.m_currentTime += Time.deltaTime;
      if ((double) this.m_currentTime < (double) this.m_endTime)
      {
        SoundManager.Get().SetVolume(this.m_audio, Mathf.Lerp(this.m_startVolume, this.m_TargetVolume.Value, (this.m_currentTime - this.m_startTime) / this.m_FadeTime.Value));
      }
      else
      {
        SoundManager.Get().SetVolume(this.m_audio, this.m_TargetVolume.Value);
        if (this.m_StopWhenDone.Value)
          SoundManager.Get().Stop(this.m_audio);
        this.Finish();
      }
    }
  }
}
