﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.EnableBehaviour
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Enables/Disables a Behaviour on a GameObject. Optionally reset the Behaviour on exit - useful if you want the Behaviour to be active only while this state is active.")]
  [ActionCategory(ActionCategory.ScriptControl)]
  public class EnableBehaviour : FsmStateAction
  {
    [Tooltip("The GameObject that owns the Behaviour.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Behaviour)]
    [Tooltip("The name of the Behaviour to enable/disable.")]
    public FsmString behaviour;
    [Tooltip("Optionally drag a component directly into this field (behavior name will be ignored).")]
    public Component component;
    [Tooltip("Set to True to enable, False to disable.")]
    [RequiredField]
    public FsmBool enable;
    public FsmBool resetOnExit;
    private Behaviour componentTarget;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.behaviour = (FsmString) null;
      this.component = (Component) null;
      this.enable = (FsmBool) true;
      this.resetOnExit = (FsmBool) true;
    }

    public override void OnEnter()
    {
      this.DoEnableBehaviour(this.Fsm.GetOwnerDefaultTarget(this.gameObject));
      this.Finish();
    }

    private void DoEnableBehaviour(GameObject go)
    {
      if ((Object) go == (Object) null)
        return;
      this.componentTarget = !((Object) this.component != (Object) null) ? go.GetComponent(ReflectionUtils.GetGlobalType(this.behaviour.Value)) as Behaviour : this.component as Behaviour;
      if ((Object) this.componentTarget == (Object) null)
        this.LogWarning(" " + go.name + " missing behaviour: " + this.behaviour.Value);
      else
        this.componentTarget.enabled = this.enable.Value;
    }

    public override void OnExit()
    {
      if ((Object) this.componentTarget == (Object) null || !this.resetOnExit.Value)
        return;
      this.componentTarget.enabled = !this.enable.Value;
    }

    public override string ErrorCheck()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null || (Object) this.component != (Object) null || (this.behaviour.IsNone || string.IsNullOrEmpty(this.behaviour.Value)))
        return (string) null;
      if ((Object) (ownerDefaultTarget.GetComponent(ReflectionUtils.GetGlobalType(this.behaviour.Value)) as Behaviour) != (Object) null)
        return (string) null;
      return "Behaviour missing";
    }
  }
}
