﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.CopyTransformAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Copies a game object's transform to another game object.")]
  [ActionCategory("Pegasus")]
  public class CopyTransformAction : FsmStateAction
  {
    [RequiredField]
    public FsmGameObject m_SourceObject;
    [RequiredField]
    public FsmGameObject m_TargetObject;
    public FsmBool m_Position;
    public FsmBool m_Rotation;
    public FsmBool m_Scale;
    [Tooltip("Copies the transform in world space if checked, otherwise copies in local space.")]
    public FsmBool m_WorldSpace;

    public override void Reset()
    {
      FsmGameObject fsmGameObject1 = new FsmGameObject();
      fsmGameObject1.UseVariable = true;
      this.m_SourceObject = fsmGameObject1;
      FsmGameObject fsmGameObject2 = new FsmGameObject();
      fsmGameObject2.UseVariable = true;
      this.m_TargetObject = fsmGameObject2;
      this.m_Position = (FsmBool) true;
      this.m_Rotation = (FsmBool) true;
      this.m_Scale = (FsmBool) true;
      this.m_WorldSpace = (FsmBool) true;
    }

    public override void OnEnter()
    {
      if (this.m_SourceObject == null || this.m_SourceObject.IsNone || (!(bool) ((Object) this.m_SourceObject.Value) || this.m_TargetObject == null) || (this.m_TargetObject.IsNone || !(bool) ((Object) this.m_TargetObject.Value)))
      {
        this.Finish();
      }
      else
      {
        Transform transform1 = this.m_SourceObject.Value.transform;
        Transform transform2 = this.m_TargetObject.Value.transform;
        if (this.m_WorldSpace.Value)
        {
          if (this.m_Position.Value)
            transform2.position = transform1.position;
          if (this.m_Rotation.Value)
            transform2.rotation = transform1.rotation;
          if (this.m_Scale.Value)
            TransformUtil.CopyWorldScale((Component) transform2, (Component) transform1);
        }
        else
        {
          if (this.m_Position.Value)
            transform2.localPosition = transform1.localPosition;
          if (this.m_Rotation.Value)
            transform2.localRotation = transform1.localRotation;
          if (this.m_Scale.Value)
            transform2.localScale = transform1.localScale;
        }
        this.Finish();
      }
    }
  }
}
