﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PlayerPrefsDeleteKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("PlayerPrefs")]
  [Tooltip("Removes key and its corresponding value from the preferences.")]
  public class PlayerPrefsDeleteKey : FsmStateAction
  {
    public FsmString key;

    public override void Reset()
    {
      this.key = (FsmString) string.Empty;
    }

    public override void OnEnter()
    {
      if (!this.key.IsNone && !this.key.Value.Equals(string.Empty))
        PlayerPrefs.DeleteKey(this.key.Value);
      this.Finish();
    }
  }
}
