﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetFogDensity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the density of the Fog in the scene.")]
  [ActionCategory(ActionCategory.RenderSettings)]
  public class SetFogDensity : FsmStateAction
  {
    [RequiredField]
    public FsmFloat fogDensity;
    public bool everyFrame;

    public override void Reset()
    {
      this.fogDensity = (FsmFloat) 0.5f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetFogDensity();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetFogDensity();
    }

    private void DoSetFogDensity()
    {
      RenderSettings.fogDensity = this.fogDensity.Value;
    }
  }
}
