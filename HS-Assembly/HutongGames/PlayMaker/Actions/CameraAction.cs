﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.CameraAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("INTERNAL USE ONLY. Do not put this on your FSMs.")]
  public abstract class CameraAction : FsmStateAction
  {
    protected Camera m_namedCamera;

    protected Camera GetCamera(CameraAction.WhichCamera which, FsmGameObject specificCamera, FsmString namedCamera)
    {
      switch (which)
      {
        case CameraAction.WhichCamera.SPECIFIC:
          if (!specificCamera.IsNone)
            return specificCamera.Value.GetComponent<Camera>();
          break;
        case CameraAction.WhichCamera.NAMED:
          string name = !namedCamera.IsNone ? namedCamera.Value : (string) null;
          if (!string.IsNullOrEmpty(name))
          {
            if ((bool) ((Object) this.m_namedCamera))
            {
              if (this.m_namedCamera.name == name)
                return this.m_namedCamera;
              this.m_namedCamera = (Camera) null;
            }
            Camera camera = (Camera) null;
            GameObject gameObject = GameObject.Find(name);
            if ((bool) ((Object) gameObject))
              camera = gameObject.GetComponent<Camera>();
            if ((bool) ((Object) camera))
            {
              this.m_namedCamera = camera;
              return this.m_namedCamera;
            }
            break;
          }
          break;
      }
      return Camera.main;
    }

    public enum WhichCamera
    {
      MAIN,
      SPECIFIC,
      NAMED,
    }
  }
}
