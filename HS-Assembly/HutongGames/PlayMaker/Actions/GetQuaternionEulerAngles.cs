﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets a quaternion as euler angles.")]
  [ActionCategory(ActionCategory.Quaternion)]
  public class GetQuaternionEulerAngles : QuaternionBaseAction
  {
    [Tooltip("The rotation")]
    [RequiredField]
    public FsmQuaternion quaternion;
    [Tooltip("The euler angles of the quaternion.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmVector3 eulerAngles;

    public override void Reset()
    {
      this.quaternion = (FsmQuaternion) null;
      this.eulerAngles = (FsmVector3) null;
      this.everyFrame = true;
      this.everyFrameOption = QuaternionBaseAction.everyFrameOptions.Update;
    }

    public override void OnEnter()
    {
      this.GetQuatEuler();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.Update)
        return;
      this.GetQuatEuler();
    }

    public override void OnLateUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.LateUpdate)
        return;
      this.GetQuatEuler();
    }

    public override void OnFixedUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.FixedUpdate)
        return;
      this.GetQuatEuler();
    }

    private void GetQuatEuler()
    {
      this.eulerAngles.Value = this.quaternion.Value.eulerAngles;
    }
  }
}
