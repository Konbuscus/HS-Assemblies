﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ConvertFloatToInt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Convert)]
  [Tooltip("Converts a Float value to an Integer value.")]
  public class ConvertFloatToInt : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("The Float variable to convert to an integer.")]
    public FsmFloat floatVariable;
    [Tooltip("Store the result in an Integer variable.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmInt intVariable;
    public ConvertFloatToInt.FloatRounding rounding;
    public bool everyFrame;

    public override void Reset()
    {
      this.floatVariable = (FsmFloat) null;
      this.intVariable = (FsmInt) null;
      this.rounding = ConvertFloatToInt.FloatRounding.Nearest;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoConvertFloatToInt();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoConvertFloatToInt();
    }

    private void DoConvertFloatToInt()
    {
      switch (this.rounding)
      {
        case ConvertFloatToInt.FloatRounding.RoundDown:
          this.intVariable.Value = Mathf.FloorToInt(this.floatVariable.Value);
          break;
        case ConvertFloatToInt.FloatRounding.RoundUp:
          this.intVariable.Value = Mathf.CeilToInt(this.floatVariable.Value);
          break;
        case ConvertFloatToInt.FloatRounding.Nearest:
          this.intVariable.Value = Mathf.RoundToInt(this.floatVariable.Value);
          break;
      }
    }

    public enum FloatRounding
    {
      RoundDown,
      RoundUp,
      Nearest,
    }
  }
}
