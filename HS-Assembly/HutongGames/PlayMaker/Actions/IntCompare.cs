﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.IntCompare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Logic)]
  [Tooltip("Sends Events based on the comparison of 2 Integers.")]
  public class IntCompare : FsmStateAction
  {
    [RequiredField]
    public FsmInt integer1;
    [RequiredField]
    public FsmInt integer2;
    [Tooltip("Event sent if Int 1 equals Int 2")]
    public FsmEvent equal;
    [Tooltip("Event sent if Int 1 is less than Int 2")]
    public FsmEvent lessThan;
    [Tooltip("Event sent if Int 1 is greater than Int 2")]
    public FsmEvent greaterThan;
    public bool everyFrame;

    public override void Reset()
    {
      this.integer1 = (FsmInt) 0;
      this.integer2 = (FsmInt) 0;
      this.equal = (FsmEvent) null;
      this.lessThan = (FsmEvent) null;
      this.greaterThan = (FsmEvent) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoIntCompare();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoIntCompare();
    }

    private void DoIntCompare()
    {
      if (this.integer1.Value == this.integer2.Value)
        this.Fsm.Event(this.equal);
      else if (this.integer1.Value < this.integer2.Value)
      {
        this.Fsm.Event(this.lessThan);
      }
      else
      {
        if (this.integer1.Value <= this.integer2.Value)
          return;
        this.Fsm.Event(this.greaterThan);
      }
    }

    public override string ErrorCheck()
    {
      if (FsmEvent.IsNullOrEmpty(this.equal) && FsmEvent.IsNullOrEmpty(this.lessThan) && FsmEvent.IsNullOrEmpty(this.greaterThan))
        return "Action sends no events!";
      return string.Empty;
    }
  }
}
