﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetCollisionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Gets info on the last collision event and store in variables. See Unity Physics docs.")]
  public class GetCollisionInfo : FsmStateAction
  {
    [Tooltip("Get the GameObject hit.")]
    [UIHint(UIHint.Variable)]
    public FsmGameObject gameObjectHit;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the relative velocity of the collision.")]
    public FsmVector3 relativeVelocity;
    [Tooltip("Get the relative speed of the collision. Useful for controlling reactions. E.g., selecting an appropriate sound fx.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat relativeSpeed;
    [Tooltip("Get the world position of the collision contact. Useful for spawning effects etc.")]
    [UIHint(UIHint.Variable)]
    public FsmVector3 contactPoint;
    [Tooltip("Get the collision normal vector. Useful for aligning spawned effects etc.")]
    [UIHint(UIHint.Variable)]
    public FsmVector3 contactNormal;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the name of the physics material of the colliding GameObject. Useful for triggering different effects. Audio, particles...")]
    public FsmString physicsMaterialName;

    public override void Reset()
    {
      this.gameObjectHit = (FsmGameObject) null;
      this.relativeVelocity = (FsmVector3) null;
      this.relativeSpeed = (FsmFloat) null;
      this.contactPoint = (FsmVector3) null;
      this.contactNormal = (FsmVector3) null;
      this.physicsMaterialName = (FsmString) null;
    }

    private void StoreCollisionInfo()
    {
      if (this.Fsm.CollisionInfo == null)
        return;
      this.gameObjectHit.Value = this.Fsm.CollisionInfo.gameObject;
      this.relativeSpeed.Value = this.Fsm.CollisionInfo.relativeVelocity.magnitude;
      this.relativeVelocity.Value = this.Fsm.CollisionInfo.relativeVelocity;
      this.physicsMaterialName.Value = this.Fsm.CollisionInfo.collider.material.name;
      if (this.Fsm.CollisionInfo.contacts == null || this.Fsm.CollisionInfo.contacts.Length <= 0)
        return;
      this.contactPoint.Value = this.Fsm.CollisionInfo.contacts[0].point;
      this.contactNormal.Value = this.Fsm.CollisionInfo.contacts[0].normal;
    }

    public override void OnEnter()
    {
      this.StoreCollisionInfo();
      this.Finish();
    }
  }
}
