﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActorPlaySpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Play a spell on the given Actor")]
  public class ActorPlaySpell : FsmStateAction
  {
    public SpellType m_Spell;
    public FsmGameObject m_actorObject;

    public override void Reset()
    {
      this.m_Spell = SpellType.NONE;
      this.m_actorObject = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      if (!this.m_actorObject.IsNone)
      {
        Actor componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<Actor>(this.m_actorObject.Value);
        if ((Object) componentInThisOrParents != (Object) null && this.m_Spell != SpellType.NONE)
          componentInThisOrParents.ActivateSpellBirthState(this.m_Spell);
      }
      this.Finish();
    }

    public override void OnUpdate()
    {
    }
  }
}
