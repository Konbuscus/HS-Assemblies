﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.EnableFog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.RenderSettings)]
  [Tooltip("Enables/Disables Fog in the scene.")]
  public class EnableFog : FsmStateAction
  {
    [Tooltip("Set to True to enable, False to disable.")]
    public FsmBool enableFog;
    [Tooltip("Repeat every frame. Useful if the Enable Fog setting is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.enableFog = (FsmBool) true;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      RenderSettings.fog = this.enableFog.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      RenderSettings.fog = this.enableFog.Value;
    }
  }
}
