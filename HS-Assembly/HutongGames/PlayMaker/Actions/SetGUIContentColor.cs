﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGUIContentColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the Tinting Color for all text rendered by the GUI. By default only effects GUI rendered by this FSM, check Apply Globally to effect all GUI controls.")]
  [ActionCategory(ActionCategory.GUI)]
  public class SetGUIContentColor : FsmStateAction
  {
    [RequiredField]
    public FsmColor contentColor;
    public FsmBool applyGlobally;

    public override void Reset()
    {
      this.contentColor = (FsmColor) Color.white;
    }

    public override void OnGUI()
    {
      GUI.contentColor = this.contentColor.Value;
      if (!this.applyGlobally.Value)
        return;
      PlayMakerGUI.GUIContentColor = GUI.contentColor;
    }
  }
}
