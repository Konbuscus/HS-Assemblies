﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAnimatorIKGoal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Sets the position, rotation and weights of an IK goal. A GameObject can be set to control the position and rotation, or it can be manually expressed.")]
  public class SetAnimatorIKGoal : FsmStateAction
  {
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    [Tooltip("The target.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The IK goal")]
    public AvatarIKGoal iKGoal;
    [Tooltip("The gameObject target of the ik goal")]
    public FsmGameObject goal;
    [Tooltip("The position of the ik goal. If Goal GameObject set, position is used as an offset from Goal")]
    public FsmVector3 position;
    [Tooltip("The rotation of the ik goal.If Goal GameObject set, rotation is used as an offset from Goal")]
    public FsmQuaternion rotation;
    [Tooltip("The translative weight of an IK goal (0 = at the original animation before IK, 1 = at the goal)")]
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat positionWeight;
    [HasFloatSlider(0.0f, 1f)]
    [Tooltip("Sets the rotational weight of an IK goal (0 = rotation before IK, 1 = rotation at the IK goal)")]
    public FsmFloat rotationWeight;
    [Tooltip("Repeat every frame. Useful when changing over time.")]
    public bool everyFrame;
    private Animator _animator;
    private Transform _transform;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.goal = (FsmGameObject) null;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.position = fsmVector3;
      FsmQuaternion fsmQuaternion = new FsmQuaternion();
      fsmQuaternion.UseVariable = true;
      this.rotation = fsmQuaternion;
      this.positionWeight = (FsmFloat) 1f;
      this.rotationWeight = (FsmFloat) 1f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          GameObject gameObject = this.goal.Value;
          if ((Object) gameObject != (Object) null)
            this._transform = gameObject.transform;
          this.DoSetIKGoal();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void DoAnimatorIK(int layerIndex)
    {
      this.DoSetIKGoal();
    }

    private void DoSetIKGoal()
    {
      if ((Object) this._animator == (Object) null)
        return;
      if ((Object) this._transform != (Object) null)
      {
        if (this.position.IsNone)
          this._animator.SetIKPosition(this.iKGoal, this._transform.position);
        else
          this._animator.SetIKPosition(this.iKGoal, this._transform.position + this.position.Value);
        if (this.rotation.IsNone)
          this._animator.SetIKRotation(this.iKGoal, this._transform.rotation);
        else
          this._animator.SetIKRotation(this.iKGoal, this._transform.rotation * this.rotation.Value);
      }
      else
      {
        if (!this.position.IsNone)
          this._animator.SetIKPosition(this.iKGoal, this.position.Value);
        if (!this.rotation.IsNone)
          this._animator.SetIKRotation(this.iKGoal, this.rotation.Value);
      }
      if (!this.positionWeight.IsNone)
        this._animator.SetIKPositionWeight(this.iKGoal, this.positionWeight.Value);
      if (this.rotationWeight.IsNone)
        return;
      this._animator.SetIKRotationWeight(this.iKGoal, this.rotationWeight.Value);
    }
  }
}
