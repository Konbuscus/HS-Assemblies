﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ComponentAction`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  public abstract class ComponentAction<T> : FsmStateAction where T : Component
  {
    private GameObject cachedGameObject;
    private T component;

    protected Rigidbody rigidbody
    {
      get
      {
        return (object) this.component as Rigidbody;
      }
    }

    protected Rigidbody2D rigidbody2d
    {
      get
      {
        return (object) this.component as Rigidbody2D;
      }
    }

    protected Renderer renderer
    {
      get
      {
        return (object) this.component as Renderer;
      }
    }

    protected Animation animation
    {
      get
      {
        return (object) this.component as Animation;
      }
    }

    protected AudioSource audio
    {
      get
      {
        return (object) this.component as AudioSource;
      }
    }

    protected Camera camera
    {
      get
      {
        return (object) this.component as Camera;
      }
    }

    protected GUIText guiText
    {
      get
      {
        return (object) this.component as GUIText;
      }
    }

    protected GUITexture guiTexture
    {
      get
      {
        return (object) this.component as GUITexture;
      }
    }

    protected Light light
    {
      get
      {
        return (object) this.component as Light;
      }
    }

    protected NetworkView networkView
    {
      get
      {
        return (object) this.component as NetworkView;
      }
    }

    protected bool UpdateCache(GameObject go)
    {
      if ((Object) go == (Object) null)
        return false;
      if ((Object) this.component == (Object) null || (Object) this.cachedGameObject != (Object) go)
      {
        this.component = go.GetComponent<T>();
        this.cachedGameObject = go;
        if ((Object) this.component == (Object) null)
          this.LogWarning("Missing component: " + typeof (T).FullName + " on: " + go.name);
      }
      return (Object) this.component != (Object) null;
    }
  }
}
