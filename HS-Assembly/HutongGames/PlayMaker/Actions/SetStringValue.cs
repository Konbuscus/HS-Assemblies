﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetStringValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.String)]
  [Tooltip("Sets the value of a String Variable.")]
  public class SetStringValue : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmString stringVariable;
    [UIHint(UIHint.TextArea)]
    public FsmString stringValue;
    public bool everyFrame;

    public override void Reset()
    {
      this.stringVariable = (FsmString) null;
      this.stringValue = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetStringValue();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetStringValue();
    }

    private void DoSetStringValue()
    {
      if (this.stringVariable == null || this.stringValue == null)
        return;
      this.stringVariable.Value = this.stringValue.Value;
    }
  }
}
