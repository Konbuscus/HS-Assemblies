﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetFsmVariables
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.StateMachine)]
  [ActionTarget(typeof (PlayMakerFSM), "gameObject,fsmName", false)]
  [Tooltip("Get the values of multiple variables in another FSM and store in variables of the same name in this FSM.")]
  public class GetFsmVariables : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject that owns the FSM")]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.FsmName)]
    [Tooltip("Optional name of FSM on Game Object")]
    public FsmString fsmName;
    [Tooltip("Store the values of the FsmVariables")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [HideTypeFilter]
    public FsmVar[] getVariables;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;
    private GameObject cachedGO;
    private PlayMakerFSM sourceFsm;
    private INamedVariable[] sourceVariables;
    private NamedVariable[] targetVariables;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.fsmName = (FsmString) string.Empty;
      this.getVariables = (FsmVar[]) null;
    }

    private void InitFsmVars()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null || !((Object) ownerDefaultTarget != (Object) this.cachedGO))
        return;
      this.sourceVariables = new INamedVariable[this.getVariables.Length];
      this.targetVariables = new NamedVariable[this.getVariables.Length];
      for (int index = 0; index < this.getVariables.Length; ++index)
      {
        string variableName = this.getVariables[index].variableName;
        this.sourceFsm = ActionHelpers.GetGameObjectFsm(ownerDefaultTarget, this.fsmName.Value);
        this.sourceVariables[index] = (INamedVariable) this.sourceFsm.FsmVariables.GetVariable(variableName);
        this.targetVariables[index] = this.Fsm.Variables.GetVariable(variableName);
        this.getVariables[index].Type = this.targetVariables[index].VariableType;
        if (!string.IsNullOrEmpty(variableName) && this.sourceVariables[index] == null)
          this.LogWarning("Missing Variable: " + variableName);
        this.cachedGO = ownerDefaultTarget;
      }
    }

    public override void OnEnter()
    {
      this.InitFsmVars();
      this.DoGetFsmVariables();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetFsmVariables();
    }

    private void DoGetFsmVariables()
    {
      this.InitFsmVars();
      for (int index = 0; index < this.getVariables.Length; ++index)
      {
        this.getVariables[index].GetValueFrom(this.sourceVariables[index]);
        this.getVariables[index].ApplyValueTo((INamedVariable) this.targetVariables[index]);
      }
    }
  }
}
