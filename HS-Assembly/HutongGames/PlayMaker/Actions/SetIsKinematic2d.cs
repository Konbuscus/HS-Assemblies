﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetIsKinematic2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Controls whether 2D physics affects the Game Object.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class SetIsKinematic2d : ComponentAction<Rigidbody2D>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody2D))]
    [Tooltip("The GameObject with the Rigidbody2D attached")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The isKinematic value")]
    [RequiredField]
    public FsmBool isKinematic;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.isKinematic = (FsmBool) false;
    }

    public override void OnEnter()
    {
      this.DoSetIsKinematic();
      this.Finish();
    }

    private void DoSetIsKinematic()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.rigidbody2d.isKinematic = this.isKinematic.Value;
    }
  }
}
