﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SendEvent2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.StateMachine)]
  [Tooltip("Sends an Event after an optional delay. NOTE: To send events between FSMs they must be marked as Global in the Events Browser.")]
  public class SendEvent2 : FsmStateAction
  {
    public FsmEventTarget eventTarget;
    [RequiredField]
    public FsmString sendEvent;
    [HasFloatSlider(0.0f, 10f)]
    public FsmFloat delay;
    private DelayedEvent delayedEvent;

    public override void Reset()
    {
      this.sendEvent = (FsmString) null;
      this.delay = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      if ((double) this.delay.Value == 0.0)
      {
        this.Fsm.Event(this.eventTarget, this.sendEvent.Value);
        this.Finish();
      }
      else
        this.delayedEvent = new DelayedEvent(this.Fsm, this.eventTarget, this.sendEvent.Value, this.delay.Value);
    }

    public override void OnUpdate()
    {
      this.delayedEvent.Update();
      if (!this.delayedEvent.Finished)
        return;
      this.Finish();
    }
  }
}
