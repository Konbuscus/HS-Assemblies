﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PauseMovieTexture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Pauses a Movie Texture.")]
  [ActionCategory(ActionCategory.Movie)]
  public class PauseMovieTexture : FsmStateAction
  {
    [ObjectType(typeof (MovieTexture))]
    [RequiredField]
    public FsmObject movieTexture;

    public override void Reset()
    {
      this.movieTexture = (FsmObject) null;
    }

    public override void OnEnter()
    {
      MovieTexture movieTexture = this.movieTexture.Value as MovieTexture;
      if ((Object) movieTexture != (Object) null)
        movieTexture.Pause();
      this.Finish();
    }
  }
}
