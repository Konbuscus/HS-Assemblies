﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DebugObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Logs the value of an Object Variable in the PlayMaker Log Window.")]
  [ActionCategory(ActionCategory.Debug)]
  public class DebugObject : BaseLogAction
  {
    [Tooltip("Info, Warning, or Error.")]
    public LogLevel logLevel;
    [UIHint(UIHint.Variable)]
    [Tooltip("The Object variable to debug.")]
    public FsmObject fsmObject;

    public override void Reset()
    {
      this.logLevel = LogLevel.Info;
      this.fsmObject = (FsmObject) null;
      base.Reset();
    }

    public override void OnEnter()
    {
      string text = "None";
      if (!this.fsmObject.IsNone)
        text = this.fsmObject.Name + ": " + (object) this.fsmObject;
      ActionHelpers.DebugLog(this.Fsm, this.logLevel, text, this.sendToUnityLog);
      this.Finish();
    }
  }
}
