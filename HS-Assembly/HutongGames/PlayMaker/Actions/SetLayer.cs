﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetLayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Sets a Game Object's Layer.")]
  public class SetLayer : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Layer)]
    public int layer;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.layer = 0;
    }

    public override void OnEnter()
    {
      this.DoSetLayer();
      this.Finish();
    }

    private void DoSetLayer()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      ownerDefaultTarget.layer = this.layer;
    }
  }
}
