﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.BuildString
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.String)]
  [Tooltip("Builds a String from other Strings.")]
  public class BuildString : FsmStateAction
  {
    [RequiredField]
    [Tooltip("Array of Strings to combine.")]
    public FsmString[] stringParts;
    [Tooltip("Separator to insert between each String. E.g. space character.")]
    public FsmString separator;
    [Tooltip("Add Separator to end of built string.")]
    public FsmBool addToEnd;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the final String in a variable.")]
    public FsmString storeResult;
    [Tooltip("Repeat every frame while the state is active.")]
    public bool everyFrame;
    private string result;

    public override void Reset()
    {
      this.stringParts = new FsmString[3];
      this.separator = (FsmString) null;
      this.addToEnd = (FsmBool) true;
      this.storeResult = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoBuildString();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoBuildString();
    }

    private void DoBuildString()
    {
      if (this.storeResult == null)
        return;
      this.result = string.Empty;
      for (int index = 0; index < this.stringParts.Length - 1; ++index)
      {
        this.result += (string) (object) this.stringParts[index];
        this.result += this.separator.Value;
      }
      this.result += (string) (object) this.stringParts[this.stringParts.Length - 1];
      if (this.addToEnd.Value)
        this.result += this.separator.Value;
      this.storeResult.Value = this.result;
    }
  }
}
