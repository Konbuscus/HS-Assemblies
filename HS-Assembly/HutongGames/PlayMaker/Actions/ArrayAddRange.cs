﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayAddRange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Array)]
  [Tooltip("Add values to an array.")]
  public class ArrayAddRange : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The Array Variable to use.")]
    [UIHint(UIHint.Variable)]
    public FsmArray array;
    [RequiredField]
    [Tooltip("The variables to add.")]
    [MatchElementType("array")]
    public FsmVar[] variables;

    public override void Reset()
    {
      this.array = (FsmArray) null;
      this.variables = new FsmVar[2];
    }

    public override void OnEnter()
    {
      this.DoAddRange();
      this.Finish();
    }

    private void DoAddRange()
    {
      int length = this.variables.Length;
      if (length <= 0)
        return;
      this.array.Resize(this.array.Length + length);
      foreach (FsmVar variable in this.variables)
      {
        this.array.Set(this.array.Length - length, variable.GetValue());
        --length;
      }
    }
  }
}
