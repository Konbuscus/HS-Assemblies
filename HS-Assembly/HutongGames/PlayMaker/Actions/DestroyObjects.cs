﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DestroyObjects
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Destroys GameObjects in an array.")]
  public class DestroyObjects : FsmStateAction
  {
    [RequiredField]
    [ArrayEditor(VariableType.GameObject, "", 0, 0, 65536)]
    [Tooltip("The GameObjects to destroy.")]
    public FsmArray gameObjects;
    [Tooltip("Optional delay before destroying the Game Objects.")]
    [HasFloatSlider(0.0f, 5f)]
    public FsmFloat delay;
    [Tooltip("Detach children before destroying the Game Objects.")]
    public FsmBool detachChildren;

    public override void Reset()
    {
      this.gameObjects = (FsmArray) null;
      this.delay = (FsmFloat) 0.0f;
    }

    public override void OnEnter()
    {
      if (this.gameObjects.Values != null)
      {
        foreach (GameObject gameObject in this.gameObjects.Values)
        {
          if ((Object) gameObject != (Object) null)
          {
            if ((double) this.delay.Value <= 0.0)
              Object.Destroy((Object) gameObject);
            else
              Object.Destroy((Object) gameObject, this.delay.Value);
            if (this.detachChildren.Value)
              gameObject.transform.DetachChildren();
          }
        }
      }
      this.Finish();
    }
  }
}
