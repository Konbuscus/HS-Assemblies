﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FloatClamp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Math)]
  [Tooltip("Clamps the value of Float Variable to a Min/Max range.")]
  public class FloatClamp : FsmStateAction
  {
    [Tooltip("Float variable to clamp.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmFloat floatVariable;
    [Tooltip("The minimum value.")]
    [RequiredField]
    public FsmFloat minValue;
    [RequiredField]
    [Tooltip("The maximum value.")]
    public FsmFloat maxValue;
    [Tooltip("Repeate every frame. Useful if the float variable is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.floatVariable = (FsmFloat) null;
      this.minValue = (FsmFloat) null;
      this.maxValue = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoClamp();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoClamp();
    }

    private void DoClamp()
    {
      this.floatVariable.Value = Mathf.Clamp(this.floatVariable.Value, this.minValue.Value, this.maxValue.Value);
    }
  }
}
