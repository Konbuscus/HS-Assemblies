﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DebugDrawShape
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Draw Gizmos in the Scene View.")]
  [ActionCategory(ActionCategory.Debug)]
  public class DebugDrawShape : FsmStateAction
  {
    [Tooltip("Draw the Gizmo at a GameObject's position.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("The type of Gizmo to draw:\nSphere, Cube, WireSphere, or WireCube.")]
    public DebugDrawShape.ShapeType shape;
    [Tooltip("The color to use.")]
    public FsmColor color;
    [Tooltip("Use this for sphere gizmos")]
    public FsmFloat radius;
    [Tooltip("Use this for cube gizmos")]
    public FsmVector3 size;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.shape = DebugDrawShape.ShapeType.Sphere;
      this.color = (FsmColor) Color.grey;
      this.radius = (FsmFloat) 1f;
      this.size = (FsmVector3) new Vector3(1f, 1f, 1f);
    }

    public override void OnDrawActionGizmos()
    {
      Transform transform = this.Fsm.GetOwnerDefaultTarget(this.gameObject).transform;
      if ((Object) transform == (Object) null)
        return;
      Gizmos.color = this.color.Value;
      switch (this.shape)
      {
        case DebugDrawShape.ShapeType.Sphere:
          Gizmos.DrawSphere(transform.position, this.radius.Value);
          break;
        case DebugDrawShape.ShapeType.Cube:
          Gizmos.DrawCube(transform.position, this.size.Value);
          break;
        case DebugDrawShape.ShapeType.WireSphere:
          Gizmos.DrawWireSphere(transform.position, this.radius.Value);
          break;
        case DebugDrawShape.ShapeType.WireCube:
          Gizmos.DrawWireCube(transform.position, this.size.Value);
          break;
      }
    }

    public enum ShapeType
    {
      Sphere,
      Cube,
      WireSphere,
      WireCube,
    }
  }
}
