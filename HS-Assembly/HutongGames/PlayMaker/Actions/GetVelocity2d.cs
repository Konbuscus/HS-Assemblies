﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetVelocity2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the 2d Velocity of a Game Object and stores it in a Vector2 Variable or each Axis in a Float Variable. NOTE: The Game Object must have a Rigid Body 2D.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class GetVelocity2d : ComponentAction<Rigidbody2D>
  {
    [RequiredField]
    [Tooltip("The GameObject with the Rigidbody2D attached")]
    [CheckForComponent(typeof (Rigidbody2D))]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [Tooltip("The velocity")]
    public FsmVector2 vector;
    [UIHint(UIHint.Variable)]
    [Tooltip("The x value of the velocity")]
    public FsmFloat x;
    [UIHint(UIHint.Variable)]
    [Tooltip("The y value of the velocity")]
    public FsmFloat y;
    [Tooltip("The space reference to express the velocity")]
    public Space space;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.vector = (FsmVector2) null;
      this.x = (FsmFloat) null;
      this.y = (FsmFloat) null;
      this.space = Space.World;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetVelocity();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetVelocity();
    }

    private void DoGetVelocity()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      Vector2 vector2 = this.rigidbody2d.velocity;
      if (this.space == Space.Self)
        vector2 = (Vector2) this.rigidbody2d.transform.InverseTransformDirection((Vector3) vector2);
      this.vector.Value = vector2;
      this.x.Value = vector2.x;
      this.y.Value = vector2.y;
    }
  }
}
