﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ConvertFloatToString
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Convert)]
  [Tooltip("Converts a Float value to a String value with optional format.")]
  public class ConvertFloatToString : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The float variable to convert.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat floatVariable;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("A string variable to store the converted value.")]
    public FsmString stringVariable;
    [Tooltip("Optional Format, allows for leading zeroes. E.g., 0000")]
    public FsmString format;
    [Tooltip("Repeat every frame. Useful if the float variable is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.floatVariable = (FsmFloat) null;
      this.stringVariable = (FsmString) null;
      this.everyFrame = false;
      this.format = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.DoConvertFloatToString();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoConvertFloatToString();
    }

    private void DoConvertFloatToString()
    {
      if (this.format.IsNone || string.IsNullOrEmpty(this.format.Value))
        this.stringVariable.Value = this.floatVariable.Value.ToString();
      else
        this.stringVariable.Value = this.floatVariable.Value.ToString(this.format.Value);
    }
  }
}
