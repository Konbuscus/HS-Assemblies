﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetVector2Length
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get Vector2 Length.")]
  [ActionCategory(ActionCategory.Vector2)]
  public class GetVector2Length : FsmStateAction
  {
    [Tooltip("The Vector2 to get the length from")]
    public FsmVector2 vector2;
    [UIHint(UIHint.Variable)]
    [Tooltip("The Vector2 the length")]
    [RequiredField]
    public FsmFloat storeLength;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.vector2 = (FsmVector2) null;
      this.storeLength = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoVectorLength();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoVectorLength();
    }

    private void DoVectorLength()
    {
      if (this.vector2 == null || this.storeLength == null)
        return;
      this.storeLength.Value = this.vector2.Value.magnitude;
    }
  }
}
