﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMouseY
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the Y Position of the mouse and stores it in a Float Variable.")]
  [ActionCategory(ActionCategory.Input)]
  public class GetMouseY : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeResult;
    public bool normalize;

    public override void Reset()
    {
      this.storeResult = (FsmFloat) null;
      this.normalize = true;
    }

    public override void OnEnter()
    {
      this.DoGetMouseY();
    }

    public override void OnUpdate()
    {
      this.DoGetMouseY();
    }

    private void DoGetMouseY()
    {
      if (this.storeResult == null)
        return;
      float y = Input.mousePosition.y;
      if (this.normalize)
        y /= (float) Screen.height;
      this.storeResult.Value = y;
    }
  }
}
