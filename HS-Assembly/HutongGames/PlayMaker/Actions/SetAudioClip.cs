﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAudioClip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the Audio Clip played by the AudioSource component on a Game Object.")]
  [ActionCategory(ActionCategory.Audio)]
  public class SetAudioClip : ComponentAction<AudioSource>
  {
    [CheckForComponent(typeof (AudioSource))]
    [RequiredField]
    [Tooltip("The GameObject with the AudioSource component.")]
    public FsmOwnerDefault gameObject;
    [ObjectType(typeof (AudioClip))]
    [Tooltip("The AudioClip to set.")]
    public FsmObject audioClip;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.audioClip = (FsmObject) null;
    }

    public override void OnEnter()
    {
      if (this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        this.audio.clip = this.audioClip.Value as AudioClip;
      this.Finish();
    }
  }
}
