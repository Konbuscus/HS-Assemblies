﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetColorRGBA
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Color)]
  [Tooltip("Get the RGBA channels of a Color Variable and store them in Float Variables.")]
  public class GetColorRGBA : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("The Color variable.")]
    [RequiredField]
    public FsmColor color;
    [Tooltip("Store the red channel in a float variable.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeRed;
    [Tooltip("Store the green channel in a float variable.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeGreen;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the blue channel in a float variable.")]
    public FsmFloat storeBlue;
    [Tooltip("Store the alpha channel in a float variable.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeAlpha;
    [Tooltip("Repeat every frame. Useful if the color variable is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.color = (FsmColor) null;
      this.storeRed = (FsmFloat) null;
      this.storeGreen = (FsmFloat) null;
      this.storeBlue = (FsmFloat) null;
      this.storeAlpha = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetColorRGBA();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetColorRGBA();
    }

    private void DoGetColorRGBA()
    {
      if (this.color.IsNone)
        return;
      this.storeRed.Value = this.color.Value.r;
      this.storeGreen.Value = this.color.Value.g;
      this.storeBlue.Value = this.color.Value.b;
      this.storeAlpha.Value = this.color.Value.a;
    }
  }
}
