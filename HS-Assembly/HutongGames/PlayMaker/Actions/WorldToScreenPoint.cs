﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.WorldToScreenPoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Camera)]
  [Tooltip("Transforms position from world space into screen space. NOTE: Uses the MainCamera!")]
  public class WorldToScreenPoint : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("World position to transform into screen coordinates.")]
    public FsmVector3 worldPosition;
    [Tooltip("World X position.")]
    public FsmFloat worldX;
    [Tooltip("World Y position.")]
    public FsmFloat worldY;
    [Tooltip("World Z position.")]
    public FsmFloat worldZ;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the screen position in a Vector3 Variable. Z will equal zero.")]
    public FsmVector3 storeScreenPoint;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the screen X position in a Float Variable.")]
    public FsmFloat storeScreenX;
    [Tooltip("Store the screen Y position in a Float Variable.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeScreenY;
    [Tooltip("Normalize screen coordinates (0-1). Otherwise coordinates are in pixels.")]
    public FsmBool normalize;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.worldPosition = (FsmVector3) null;
      FsmFloat fsmFloat1 = new FsmFloat();
      fsmFloat1.UseVariable = true;
      this.worldX = fsmFloat1;
      FsmFloat fsmFloat2 = new FsmFloat();
      fsmFloat2.UseVariable = true;
      this.worldY = fsmFloat2;
      FsmFloat fsmFloat3 = new FsmFloat();
      fsmFloat3.UseVariable = true;
      this.worldZ = fsmFloat3;
      this.storeScreenPoint = (FsmVector3) null;
      this.storeScreenX = (FsmFloat) null;
      this.storeScreenY = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoWorldToScreenPoint();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoWorldToScreenPoint();
    }

    private void DoWorldToScreenPoint()
    {
      if ((Object) Camera.main == (Object) null)
      {
        this.LogError("No MainCamera defined!");
        this.Finish();
      }
      else
      {
        Vector3 zero = Vector3.zero;
        if (!this.worldPosition.IsNone)
          zero = this.worldPosition.Value;
        if (!this.worldX.IsNone)
          zero.x = this.worldX.Value;
        if (!this.worldY.IsNone)
          zero.y = this.worldY.Value;
        if (!this.worldZ.IsNone)
          zero.z = this.worldZ.Value;
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(zero);
        if (this.normalize.Value)
        {
          screenPoint.x /= (float) Screen.width;
          screenPoint.y /= (float) Screen.height;
        }
        this.storeScreenPoint.Value = screenPoint;
        this.storeScreenX.Value = screenPoint.x;
        this.storeScreenY.Value = screenPoint.y;
      }
    }
  }
}
