﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AddComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Adds a Component to a Game Object. Use this to change the behaviour of objects on the fly. Optionally remove the Component on exiting the state.")]
  public class AddComponent : FsmStateAction
  {
    [Tooltip("The GameObject to add the Component to.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Title("Component Type")]
    [Tooltip("The type of Component to add to the Game Object.")]
    [UIHint(UIHint.ScriptComponent)]
    [RequiredField]
    public FsmString component;
    [ObjectType(typeof (Component))]
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the component in an Object variable. E.g., to use with Set Property.")]
    public FsmObject storeComponent;
    [Tooltip("Remove the Component when this State is exited.")]
    public FsmBool removeOnExit;
    private Component addedComponent;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.component = (FsmString) null;
      this.storeComponent = (FsmObject) null;
    }

    public override void OnEnter()
    {
      this.DoAddComponent();
      this.Finish();
    }

    public override void OnExit()
    {
      if (!this.removeOnExit.Value || !((Object) this.addedComponent != (Object) null))
        return;
      Object.Destroy((Object) this.addedComponent);
    }

    private void DoAddComponent()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      this.addedComponent = ownerDefaultTarget.AddComponent(ReflectionUtils.GetGlobalType(this.component.Value));
      this.storeComponent.Value = (Object) this.addedComponent;
      if (!((Object) this.addedComponent == (Object) null))
        return;
      this.LogError("Can't add component: " + this.component.Value);
    }
  }
}
