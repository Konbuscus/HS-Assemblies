﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PlayNotificationAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Plays a notification.")]
  [ActionCategory("Pegasus Audio")]
  public class PlayNotificationAction : FsmStateAction
  {
    [Tooltip("Notification quote prefab to use.")]
    public FsmString m_NotificationPrefab;
    [Tooltip("The VO line to play (+loc string).")]
    public FsmString m_NotificationVO;
    [Tooltip("Notification popup position")]
    public FsmVector3 m_NotificationPosition;

    public override void Reset()
    {
      this.m_NotificationPrefab = (FsmString) string.Empty;
      this.m_NotificationVO = (FsmString) string.Empty;
    }

    public override void OnEnter()
    {
      Vector3 defaultCharacterPos = NotificationManager.DEFAULT_CHARACTER_POS;
      if (!this.m_NotificationPosition.IsNone)
        defaultCharacterPos = this.m_NotificationPosition.Value;
      NotificationManager.Get().CreateCharacterQuote(FileUtils.GameAssetPathToName(this.m_NotificationPrefab.Value), defaultCharacterPos, GameStrings.Get(this.m_NotificationVO.Value), this.m_NotificationVO.Value, true, 0.0f, (Action) null, CanvasAnchor.BOTTOM_LEFT);
    }
  }
}
