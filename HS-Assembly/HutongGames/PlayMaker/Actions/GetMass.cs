﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMass
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Gets the Mass of a Game Object's Rigid Body.")]
  public class GetMass : ComponentAction<Rigidbody>
  {
    [Tooltip("The GameObject that owns the Rigidbody")]
    [CheckForComponent(typeof (Rigidbody))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the mass in a float variable.")]
    public FsmFloat storeResult;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.storeResult = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      this.DoGetMass();
      this.Finish();
    }

    private void DoGetMass()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.storeResult.Value = this.rigidbody.mass;
    }
  }
}
