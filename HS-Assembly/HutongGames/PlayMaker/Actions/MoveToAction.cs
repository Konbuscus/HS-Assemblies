﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.MoveToAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Instantly moves an object to a destination object's position or to a specified position vector.")]
  public class MoveToAction : FsmStateAction
  {
    public FsmOwnerDefault m_GameObject;
    [Tooltip("Move to a destination object's position.")]
    public FsmGameObject m_DestinationObject;
    [Tooltip("Move to a specific position vector. If Destination Object is defined, this is used as an offset.")]
    public FsmVector3 m_VectorPosition;
    [Tooltip("Whether Vector Position is in local or world space.")]
    public Space m_Space;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      FsmGameObject fsmGameObject = new FsmGameObject();
      fsmGameObject.UseVariable = true;
      this.m_DestinationObject = fsmGameObject;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.m_VectorPosition = fsmVector3;
      this.m_Space = Space.World;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget != (Object) null)
        this.SetPosition(ownerDefaultTarget.transform);
      this.Finish();
    }

    private void SetPosition(Transform source)
    {
      Vector3 vector3 = !this.m_VectorPosition.IsNone ? this.m_VectorPosition.Value : Vector3.zero;
      if (!this.m_DestinationObject.IsNone && (Object) this.m_DestinationObject.Value != (Object) null)
      {
        Transform transform = this.m_DestinationObject.Value.transform;
        source.position = transform.position;
        if (this.m_Space == Space.World)
          source.position = source.position + vector3;
        else
          source.localPosition = source.localPosition + vector3;
      }
      else if (this.m_Space == Space.World)
        source.position = vector3;
      else
        source.localPosition = vector3;
    }
  }
}
