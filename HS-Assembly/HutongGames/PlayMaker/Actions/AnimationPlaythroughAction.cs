﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimationPlaythroughAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Plays an Animation on a Game Object. Does not wait for the animation to finish.")]
  [ActionCategory("Pegasus")]
  public class AnimationPlaythroughAction : FsmStateAction
  {
    [Tooltip("Game Object to play the animation on.")]
    [RequiredField]
    [CheckForComponent(typeof (Animation))]
    public FsmOwnerDefault m_GameObject;
    [Tooltip("The name of the animation to play.")]
    [UIHint(UIHint.Animation)]
    public FsmString m_AnimName;
    public FsmString m_PhoneAnimName;
    [Tooltip("How to treat previously playing animations.")]
    public PlayMode m_PlayMode;
    [Tooltip("Time taken to cross fade to this animation.")]
    [HasFloatSlider(0.0f, 5f)]
    public FsmFloat m_CrossFadeSec;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_AnimName = (FsmString) null;
      this.m_PhoneAnimName = (FsmString) null;
      this.m_PlayMode = PlayMode.StopAll;
      this.m_CrossFadeSec = (FsmFloat) 0.3f;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        Debug.LogWarning((object) "AnimationPlaythroughAction GameObject is null!");
        this.Finish();
      }
      else
      {
        string animName = this.m_AnimName.Value;
        if ((bool) UniversalInputManager.UsePhoneUI && !string.IsNullOrEmpty(this.m_PhoneAnimName.Value))
          animName = this.m_PhoneAnimName.Value;
        if (string.IsNullOrEmpty(animName))
        {
          this.Finish();
        }
        else
        {
          this.StartAnimation(ownerDefaultTarget, animName);
          this.Finish();
        }
      }
    }

    private void StartAnimation(GameObject go, string animName)
    {
      float fadeLength = this.m_CrossFadeSec.Value;
      if ((double) fadeLength <= (double) Mathf.Epsilon)
        go.GetComponent<Animation>().Play(animName, this.m_PlayMode);
      else
        go.GetComponent<Animation>().CrossFade(animName, fadeLength, this.m_PlayMode);
    }
  }
}
