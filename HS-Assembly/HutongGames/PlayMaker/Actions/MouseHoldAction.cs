﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.MouseHoldAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Fires events based on how long you hold the left mouse button over a GameObject.")]
  [ActionCategory("Pegasus")]
  public class MouseHoldAction : FsmStateAction
  {
    [CheckForComponent(typeof (Collider))]
    [RequiredField]
    public FsmOwnerDefault m_GameObject;
    [Tooltip("Whether or not to fire the Hold Event after the Hold Time.")]
    public FsmBool m_UseHoldTime;
    [Tooltip("How many seconds to wait before firing the Hold Event.")]
    public FsmFloat m_HoldTime;
    [Tooltip("Fired after the Hold Time passes if Use Hold Time is enabled.")]
    public FsmEvent m_HoldEvent;
    [Tooltip("Fired if the player mouses off OR releases the left mouse button before the Hold Time.")]
    public FsmEvent m_CancelEvent;
    private float m_holdingSec;
    private bool m_suppressHoldEvent;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_HoldTime = (FsmFloat) 0.0f;
      this.m_HoldEvent = (FsmEvent) null;
      this.m_CancelEvent = (FsmEvent) null;
      this.m_holdingSec = 0.0f;
      this.m_suppressHoldEvent = false;
    }

    public override void OnEnter()
    {
      this.m_holdingSec = 0.0f;
      this.m_suppressHoldEvent = false;
      this.CheckHold(false);
    }

    public override void OnUpdate()
    {
      this.CheckHold(true);
    }

    public override string ErrorCheck()
    {
      if (this.m_UseHoldTime.Value && FsmEvent.IsNullOrEmpty(this.m_HoldEvent))
        return "Use Hold Time is checked but there is no Hold Event";
      if ((double) this.m_HoldTime.Value < 0.0)
        return "Hold Time cannot be less than 0";
      return string.Empty;
    }

    private void CheckHold(bool updating)
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if (!InputUtil.IsPlayMakerMouseInputAllowed(ownerDefaultTarget))
        return;
      bool flag = UniversalInputManager.Get().InputIsOver(ownerDefaultTarget);
      bool mouseButton = UniversalInputManager.Get().GetMouseButton(0);
      if (updating)
      {
        if (flag && mouseButton)
          this.HandleHold();
        else
          this.HandleCancel();
      }
      if (!flag || !mouseButton)
        return;
      this.m_holdingSec += Time.deltaTime;
    }

    private void HandleHold()
    {
      if (this.m_suppressHoldEvent || !this.m_UseHoldTime.Value || (double) this.m_holdingSec < (double) this.m_HoldTime.Value)
        return;
      this.m_suppressHoldEvent = true;
      this.Fsm.Event(this.m_HoldEvent);
    }

    private void HandleCancel()
    {
      float holdingSec = this.m_holdingSec;
      this.m_holdingSec = 0.0f;
      if (this.m_suppressHoldEvent)
        this.m_suppressHoldEvent = false;
      else if (this.m_UseHoldTime.Value && (double) holdingSec >= (double) this.m_HoldTime.Value)
        this.Fsm.Event(this.m_HoldEvent);
      else
        this.Fsm.Event(this.m_CancelEvent);
    }
  }
}
