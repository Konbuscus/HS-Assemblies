﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ResetAmbientColorAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set scene ambient color")]
  [ActionCategory("Pegasus")]
  public class ResetAmbientColorAction : FsmStateAction
  {
    private SetRenderSettings m_renderSettings;

    public override void Reset()
    {
    }

    public override void OnEnter()
    {
      Board board = Board.Get();
      if ((Object) board != (Object) null)
        board.ResetAmbientColor();
      this.Finish();
    }
  }
}
