﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ConvertBoolToString
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Converts a Bool value to a String value.")]
  [ActionCategory(ActionCategory.Convert)]
  public class ConvertBoolToString : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("The Bool variable to test.")]
    public FsmBool boolVariable;
    [Tooltip("The String variable to set based on the Bool variable value.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmString stringVariable;
    [Tooltip("String value if Bool variable is false.")]
    public FsmString falseString;
    [Tooltip("String value if Bool variable is true.")]
    public FsmString trueString;
    [Tooltip("Repeat every frame. Useful if the Bool variable is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.boolVariable = (FsmBool) null;
      this.stringVariable = (FsmString) null;
      this.falseString = (FsmString) "False";
      this.trueString = (FsmString) "True";
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoConvertBoolToString();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoConvertBoolToString();
    }

    private void DoConvertBoolToString()
    {
      this.stringVariable.Value = !this.boolVariable.Value ? this.falseString.Value : this.trueString.Value;
    }
  }
}
