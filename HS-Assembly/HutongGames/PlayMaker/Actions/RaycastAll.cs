﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.RaycastAll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Casts a Ray against all Colliders in the scene. Use either a GameObject or Vector3 world position as the origin of the ray. Use GetRaycastAllInfo to get more detailed info.")]
  [ActionCategory(ActionCategory.Physics)]
  public class RaycastAll : FsmStateAction
  {
    public static RaycastHit[] RaycastAllHitInfo;
    [Tooltip("Start ray at game object position. \nOr use From Position parameter.")]
    public FsmOwnerDefault fromGameObject;
    [Tooltip("Start ray at a vector3 world position. \nOr use Game Object parameter.")]
    public FsmVector3 fromPosition;
    [Tooltip("A vector3 direction vector")]
    public FsmVector3 direction;
    [Tooltip("Cast the ray in world or local space. Note if no Game Object is specfied, the direction is in world space.")]
    public Space space;
    [Tooltip("The length of the ray. Set to -1 for infinity.")]
    public FsmFloat distance;
    [UIHint(UIHint.Variable)]
    [Tooltip("Event to send if the ray hits an object.")]
    [ActionSection("Result")]
    public FsmEvent hitEvent;
    [Tooltip("Set a bool variable to true if hit something, otherwise false.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeDidHit;
    [Tooltip("Store the GameObjects hit in an array variable.")]
    [ArrayEditor(VariableType.GameObject, "", 0, 0, 65536)]
    [UIHint(UIHint.Variable)]
    public FsmArray storeHitObjects;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the world position of the ray hit point and store it in a variable.")]
    public FsmVector3 storeHitPoint;
    [Tooltip("Get the normal at the hit point and store it in a variable.")]
    [UIHint(UIHint.Variable)]
    public FsmVector3 storeHitNormal;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the distance along the ray to the hit point and store it in a variable.")]
    public FsmFloat storeHitDistance;
    [Tooltip("Set how often to cast a ray. 0 = once, don't repeat; 1 = everyFrame; 2 = every other frame... \nSince raycasts can get expensive use the highest repeat interval you can get away with.")]
    [ActionSection("Filter")]
    public FsmInt repeatInterval;
    [UIHint(UIHint.Layer)]
    [Tooltip("Pick only from these layers.")]
    public FsmInt[] layerMask;
    [Tooltip("Invert the mask, so you pick from all layers except those defined above.")]
    public FsmBool invertMask;
    [Tooltip("The color to use for the debug line.")]
    [ActionSection("Debug")]
    public FsmColor debugColor;
    [Tooltip("Draw a debug line. Note: Check Gizmos in the Game View to see it in game.")]
    public FsmBool debug;
    private int repeat;

    public override void Reset()
    {
      this.fromGameObject = (FsmOwnerDefault) null;
      FsmVector3 fsmVector3_1 = new FsmVector3();
      fsmVector3_1.UseVariable = true;
      this.fromPosition = fsmVector3_1;
      FsmVector3 fsmVector3_2 = new FsmVector3();
      fsmVector3_2.UseVariable = true;
      this.direction = fsmVector3_2;
      this.space = Space.Self;
      this.distance = (FsmFloat) 100f;
      this.hitEvent = (FsmEvent) null;
      this.storeDidHit = (FsmBool) null;
      this.storeHitObjects = (FsmArray) null;
      this.storeHitPoint = (FsmVector3) null;
      this.storeHitNormal = (FsmVector3) null;
      this.storeHitDistance = (FsmFloat) null;
      this.repeatInterval = (FsmInt) 1;
      this.layerMask = new FsmInt[0];
      this.invertMask = (FsmBool) false;
      this.debugColor = (FsmColor) Color.yellow;
      this.debug = (FsmBool) false;
    }

    public override void OnEnter()
    {
      this.DoRaycast();
      if (this.repeatInterval.Value != 0)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      --this.repeat;
      if (this.repeat != 0)
        return;
      this.DoRaycast();
    }

    private void DoRaycast()
    {
      this.repeat = this.repeatInterval.Value;
      if ((double) this.distance.Value == 0.0)
        return;
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.fromGameObject);
      Vector3 vector3 = !((Object) ownerDefaultTarget != (Object) null) ? this.fromPosition.Value : ownerDefaultTarget.transform.position;
      float num1 = float.PositiveInfinity;
      if ((double) this.distance.Value > 0.0)
        num1 = this.distance.Value;
      Vector3 direction = this.direction.Value;
      if ((Object) ownerDefaultTarget != (Object) null && this.space == Space.Self)
        direction = ownerDefaultTarget.transform.TransformDirection(this.direction.Value);
      RaycastAll.RaycastAllHitInfo = Physics.RaycastAll(vector3, direction, num1, ActionHelpers.LayerArrayToLayerMask(this.layerMask, this.invertMask.Value));
      bool flag = RaycastAll.RaycastAllHitInfo.Length > 0;
      this.storeDidHit.Value = flag;
      if (flag)
      {
        GameObject[] gameObjectArray = new GameObject[RaycastAll.RaycastAllHitInfo.Length];
        for (int index = 0; index < RaycastAll.RaycastAllHitInfo.Length; ++index)
        {
          RaycastHit raycastHit = RaycastAll.RaycastAllHitInfo[index];
          gameObjectArray[index] = raycastHit.collider.gameObject;
        }
        this.storeHitObjects.Values = (object[]) gameObjectArray;
        this.storeHitPoint.Value = this.Fsm.RaycastHitInfo.point;
        this.storeHitNormal.Value = this.Fsm.RaycastHitInfo.normal;
        this.storeHitDistance.Value = this.Fsm.RaycastHitInfo.distance;
        this.Fsm.Event(this.hitEvent);
      }
      if (!this.debug.Value)
        return;
      float num2 = Mathf.Min(num1, 1000f);
      Debug.DrawLine(vector3, vector3 + direction * num2, this.debugColor.Value);
    }
  }
}
