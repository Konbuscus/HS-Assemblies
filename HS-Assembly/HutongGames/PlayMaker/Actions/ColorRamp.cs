﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ColorRamp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Color)]
  [Tooltip("Samples a Color on a continuous Colors gradient.")]
  public class ColorRamp : FsmStateAction
  {
    [Tooltip("Array of colors to defining the gradient.")]
    [RequiredField]
    public FsmColor[] colors;
    [RequiredField]
    [Tooltip("Point on the gradient to sample. Should be between 0 and the number of colors in the gradient.")]
    public FsmFloat sampleAt;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the sampled color in a Color variable.")]
    public FsmColor storeColor;
    [Tooltip("Repeat every frame while the state is active.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.colors = new FsmColor[3];
      this.sampleAt = (FsmFloat) 0.0f;
      this.storeColor = (FsmColor) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoColorRamp();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoColorRamp();
    }

    private void DoColorRamp()
    {
      if (this.colors == null || this.colors.Length == 0 || (this.sampleAt == null || this.storeColor == null))
        return;
      float f = Mathf.Clamp(this.sampleAt.Value, 0.0f, (float) (this.colors.Length - 1));
      this.storeColor.Value = (double) f != 0.0 ? ((double) f != (double) this.colors.Length ? Color.Lerp(this.colors[Mathf.FloorToInt(f)].Value, this.colors[Mathf.CeilToInt(f)].Value, f - Mathf.Floor(f)) : this.colors[this.colors.Length - 1].Value) : this.colors[0].Value;
    }

    public override string ErrorCheck()
    {
      if (this.colors.Length < 2)
        return "Define at least 2 colors to make a gradient.";
      return (string) null;
    }
  }
}
