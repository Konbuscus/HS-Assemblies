﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.RenderToTextureRender
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Triggers a render to texture to render.")]
  [ActionCategory("Pegasus")]
  public class RenderToTextureRender : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault r2tObject;
    public bool now;

    public override void Reset()
    {
      this.r2tObject = (FsmOwnerDefault) null;
      this.now = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.r2tObject);
      if ((Object) ownerDefaultTarget != (Object) null)
      {
        RenderToTexture component = ownerDefaultTarget.GetComponent<RenderToTexture>();
        if ((Object) component != (Object) null)
        {
          if (this.now)
            component.RenderNow();
          else
            component.Render();
        }
      }
      this.Finish();
    }
  }
}
