﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.BoolChanged
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if the value of a Bool Variable has changed. Use this to send an event on change, or store a bool that can be used in other operations.")]
  [ActionCategory(ActionCategory.Logic)]
  public class BoolChanged : FsmStateAction
  {
    [Tooltip("The Bool variable to watch for changes.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmBool boolVariable;
    [Tooltip("Event to send if the variable changes.")]
    public FsmEvent changedEvent;
    [UIHint(UIHint.Variable)]
    [Tooltip("Set to True if changed.")]
    public FsmBool storeResult;
    private bool previousValue;

    public override void Reset()
    {
      this.boolVariable = (FsmBool) null;
      this.changedEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
    }

    public override void OnEnter()
    {
      if (this.boolVariable.IsNone)
        this.Finish();
      else
        this.previousValue = this.boolVariable.Value;
    }

    public override void OnUpdate()
    {
      this.storeResult.Value = false;
      if (this.boolVariable.Value == this.previousValue)
        return;
      this.storeResult.Value = true;
      this.Fsm.Event(this.changedEvent);
    }
  }
}
