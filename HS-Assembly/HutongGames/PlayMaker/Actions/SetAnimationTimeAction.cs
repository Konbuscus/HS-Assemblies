﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAnimationTimeAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the speed of an Animation.")]
  [ActionCategory("Pegasus")]
  public class SetAnimationTimeAction : FsmStateAction
  {
    [Tooltip("Game Object to play the animation on.")]
    [RequiredField]
    [CheckForComponent(typeof (Animation))]
    public FsmOwnerDefault m_GameObject;
    [Tooltip("The name of the animation to play.")]
    [UIHint(UIHint.Animation)]
    [RequiredField]
    public FsmString m_AnimName;
    public FsmString m_PhoneAnimName;
    public FsmFloat m_Time;
    public bool m_EveryFrame;
    private AnimationState m_animState;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_AnimName = (FsmString) null;
      this.m_PhoneAnimName = (FsmString) null;
      this.m_Time = (FsmFloat) 0.0f;
      this.m_EveryFrame = false;
    }

    public override void OnEnter()
    {
      if (!this.CacheAnim())
      {
        this.Finish();
      }
      else
      {
        this.UpdateTime();
        if (this.m_EveryFrame)
          return;
        this.Finish();
      }
    }

    public override void OnUpdate()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        this.Finish();
      else if ((Object) ownerDefaultTarget.GetComponent<Animation>() == (Object) null)
      {
        Debug.LogWarning((object) string.Format("SetAnimationSpeedAction.OnUpdate() - GameObject {0} is missing an animation component", (object) ownerDefaultTarget));
        this.Finish();
      }
      else
        this.UpdateTime();
    }

    private bool CacheAnim()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return false;
      string index = this.m_AnimName.Value;
      if ((bool) UniversalInputManager.UsePhoneUI && !string.IsNullOrEmpty(this.m_PhoneAnimName.Value))
        index = this.m_PhoneAnimName.Value;
      this.m_animState = ownerDefaultTarget.GetComponent<Animation>()[index];
      return true;
    }

    private void UpdateTime()
    {
      this.m_animState.time = this.m_Time.Value;
    }
  }
}
