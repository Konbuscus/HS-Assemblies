﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioSetPitchAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the pitch of an AudioSource on a Game Object.")]
  [ActionCategory("Pegasus Audio")]
  public class AudioSetPitchAction : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (AudioSource))]
    public FsmOwnerDefault m_GameObject;
    [HasFloatSlider(-3f, 3f)]
    public FsmFloat m_Pitch;
    public bool m_EveryFrame;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_Pitch = (FsmFloat) 1f;
      this.m_EveryFrame = false;
    }

    public override void OnEnter()
    {
      this.UpdatePitch();
      if (this.m_EveryFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.UpdatePitch();
    }

    private void UpdatePitch()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      AudioSource component = ownerDefaultTarget.GetComponent<AudioSource>();
      if ((Object) component == (Object) null || this.m_Pitch.IsNone)
        return;
      SoundManager.Get().SetPitch(component, this.m_Pitch.Value);
    }
  }
}
