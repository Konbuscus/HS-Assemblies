﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimatorStopRecording
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Stops the animator record mode. It will lock the recording buffer's contents in its current state. The data get saved for subsequent playback with StartPlayback.")]
  [ActionCategory(ActionCategory.Animator)]
  public class AnimatorStopRecording : FsmStateAction
  {
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The target. An Animator component and a PlayMakerAnimatorProxy component are required")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [Tooltip("The recorder StartTime")]
    [ActionSection("Results")]
    public FsmFloat recorderStartTime;
    [Tooltip("The recorder StopTime")]
    [UIHint(UIHint.Variable)]
    public FsmFloat recorderStopTime;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.recorderStartTime = (FsmFloat) null;
      this.recorderStopTime = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        Animator component = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) component != (Object) null)
        {
          component.StopRecording();
          this.recorderStartTime.Value = component.recorderStartTime;
          this.recorderStopTime.Value = component.recorderStopTime;
        }
        this.Finish();
      }
    }
  }
}
