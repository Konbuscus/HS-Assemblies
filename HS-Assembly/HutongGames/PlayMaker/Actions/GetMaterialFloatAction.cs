﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMaterialFloatAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets a named float value from a game object's material.")]
  [ActionCategory("Pegasus")]
  public class GetMaterialFloatAction : FsmStateAction
  {
    [Tooltip("The GameObject that the material is applied to.")]
    [CheckForComponent(typeof (Renderer))]
    public FsmOwnerDefault gameObject;
    [Tooltip("GameObjects can have multiple materials. Specify an index to target a specific material.")]
    public FsmInt materialIndex;
    [Tooltip("Alternatively specify a Material instead of a GameObject and Index.")]
    public FsmMaterial material;
    [Tooltip("The named float parameter in the shader.")]
    [UIHint(UIHint.FsmFloat)]
    public FsmString namedFloat;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the parameter value.")]
    public FsmFloat floatValue;
    public FsmEvent fail;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.materialIndex = (FsmInt) 0;
      this.material = (FsmMaterial) null;
      this.namedFloat = (FsmString) "_Intensity";
      this.floatValue = (FsmFloat) null;
      this.fail = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      this.DoGetMaterialfloatValue();
      this.Finish();
    }

    private void DoGetMaterialfloatValue()
    {
      if (this.floatValue.IsNone)
        return;
      string propertyName = this.namedFloat.Value;
      if (propertyName == string.Empty)
        propertyName = "_Intensity";
      if ((Object) this.material.Value != (Object) null)
      {
        if (!this.material.Value.HasProperty(propertyName))
          this.Fsm.Event(this.fail);
        else
          this.floatValue.Value = this.material.Value.GetFloat(propertyName);
      }
      else
      {
        GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
        if ((Object) ownerDefaultTarget == (Object) null)
          return;
        if ((Object) ownerDefaultTarget.GetComponent<Renderer>() == (Object) null)
          this.LogError("Missing Renderer!");
        else if ((Object) ownerDefaultTarget.GetComponent<Renderer>().material == (Object) null)
          this.LogError("Missing Material!");
        else if (this.materialIndex.Value == 0)
        {
          if (!ownerDefaultTarget.GetComponent<Renderer>().material.HasProperty(propertyName))
            this.Fsm.Event(this.fail);
          else
            this.floatValue.Value = ownerDefaultTarget.GetComponent<Renderer>().material.GetFloat(propertyName);
        }
        else
        {
          if (ownerDefaultTarget.GetComponent<Renderer>().materials.Length <= this.materialIndex.Value)
            return;
          Material[] materials = ownerDefaultTarget.GetComponent<Renderer>().materials;
          if (!materials[this.materialIndex.Value].HasProperty(propertyName))
            this.Fsm.Event(this.fail);
          else
            this.floatValue.Value = materials[this.materialIndex.Value].GetFloat(propertyName);
        }
      }
    }
  }
}
