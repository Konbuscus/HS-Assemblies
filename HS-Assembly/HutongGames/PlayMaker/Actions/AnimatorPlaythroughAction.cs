﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimatorPlaythroughAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Enables an Animator and plays one of its states and waits for it to complete.")]
  public class AnimatorPlaythroughAction : FsmStateAction
  {
    private int m_checkLayer = -1;
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    [Tooltip("Game Object to play the animation on.")]
    public FsmOwnerDefault m_GameObject;
    public FsmString m_StateName;
    public FsmString m_LayerName;
    [Tooltip("Percent of time into the animation at which to start playing.")]
    [HasFloatSlider(0.0f, 100f)]
    public FsmFloat m_StartTimePercent;
    private AnimatorStateInfo m_currentAnimationState;
    private Animator m_checkComplete;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_StateName = (FsmString) null;
      FsmString fsmString = new FsmString();
      fsmString.UseVariable = true;
      this.m_LayerName = fsmString;
      this.m_StartTimePercent = (FsmFloat) 0.0f;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if (!(bool) ((Object) ownerDefaultTarget))
      {
        this.Finish();
      }
      else
      {
        Animator component = ownerDefaultTarget.GetComponent<Animator>();
        if ((bool) ((Object) component))
        {
          int layer = -1;
          if (!this.m_LayerName.IsNone)
            layer = AnimationUtil.GetLayerIndexFromName(component, this.m_LayerName.Value);
          float normalizedTime = float.NegativeInfinity;
          if (!this.m_StartTimePercent.IsNone)
            normalizedTime = 0.01f * this.m_StartTimePercent.Value;
          component.enabled = true;
          component.Play(this.m_StateName.Value, layer, normalizedTime);
          this.m_checkComplete = component;
          this.m_checkLayer = layer != -1 ? layer : 0;
        }
        else
          this.Finish();
      }
    }

    public override void OnUpdate()
    {
      if ((Object) this.m_checkComplete == (Object) null || (double) this.m_checkComplete.GetCurrentAnimatorStateInfo(this.m_checkLayer).normalizedTime <= 1.0)
        return;
      this.m_checkComplete = (Animator) null;
      this.Finish();
    }
  }
}
