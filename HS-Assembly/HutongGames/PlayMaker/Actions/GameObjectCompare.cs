﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GameObjectCompare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Compares 2 Game Objects and sends Events based on the result.")]
  [ActionCategory(ActionCategory.Logic)]
  public class GameObjectCompare : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Title("Game Object")]
    [Tooltip("A Game Object variable to compare.")]
    public FsmOwnerDefault gameObjectVariable;
    [RequiredField]
    [Tooltip("Compare the variable with this Game Object")]
    public FsmGameObject compareTo;
    [Tooltip("Send this event if Game Objects are equal")]
    public FsmEvent equalEvent;
    [Tooltip("Send this event if Game Objects are not equal")]
    public FsmEvent notEqualEvent;
    [Tooltip("Store the result of the check in a Bool Variable. (True if equal, false if not equal).")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame. Useful if you're waiting for a true or false result.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObjectVariable = (FsmOwnerDefault) null;
      this.compareTo = (FsmGameObject) null;
      this.equalEvent = (FsmEvent) null;
      this.notEqualEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGameObjectCompare();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGameObjectCompare();
    }

    private void DoGameObjectCompare()
    {
      bool flag = (Object) this.Fsm.GetOwnerDefaultTarget(this.gameObjectVariable) == (Object) this.compareTo.Value;
      this.storeResult.Value = flag;
      if (flag && this.equalEvent != null)
      {
        this.Fsm.Event(this.equalEvent);
      }
      else
      {
        if (flag || this.notEqualEvent == null)
          return;
        this.Fsm.Event(this.notEqualEvent);
      }
    }
  }
}
