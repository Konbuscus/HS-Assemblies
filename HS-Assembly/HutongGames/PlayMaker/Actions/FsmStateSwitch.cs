﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FsmStateSwitch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Logic)]
  [ActionTarget(typeof (PlayMakerFSM), "gameObject,fsmName", false)]
  [Tooltip("Sends Events based on the current State of an FSM.")]
  public class FsmStateSwitch : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject that owns the FSM.")]
    public FsmGameObject gameObject;
    [UIHint(UIHint.FsmName)]
    [Tooltip("Optional name of Fsm on GameObject. Useful if there is more than one FSM on the GameObject.")]
    public FsmString fsmName;
    [CompoundArray("State Switches", "Compare State", "Send Event")]
    public FsmString[] compareTo;
    public FsmEvent[] sendEvent;
    [Tooltip("Repeat every frame. Useful if you're waiting for a particular result.")]
    public bool everyFrame;
    private GameObject previousGo;
    private PlayMakerFSM fsm;

    public override void Reset()
    {
      this.gameObject = (FsmGameObject) null;
      this.fsmName = (FsmString) null;
      this.compareTo = new FsmString[1];
      this.sendEvent = new FsmEvent[1];
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoFsmStateSwitch();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoFsmStateSwitch();
    }

    private void DoFsmStateSwitch()
    {
      GameObject go = this.gameObject.Value;
      if ((Object) go == (Object) null)
        return;
      if ((Object) go != (Object) this.previousGo)
      {
        this.fsm = ActionHelpers.GetGameObjectFsm(go, this.fsmName.Value);
        this.previousGo = go;
      }
      if ((Object) this.fsm == (Object) null)
        return;
      string activeStateName = this.fsm.ActiveStateName;
      for (int index = 0; index < this.compareTo.Length; ++index)
      {
        if (activeStateName == this.compareTo[index].Value)
        {
          this.Fsm.Event(this.sendEvent[index]);
          break;
        }
      }
    }
  }
}
