﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ConvertMaterialToObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Convert)]
  [Tooltip("Converts a Material variable to an Object variable. Useful if you want to use Set Property (which only works on Object variables).")]
  public class ConvertMaterialToObject : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("The Material variable to convert to an Object.")]
    [RequiredField]
    public FsmMaterial materialVariable;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result in an Object variable.")]
    [RequiredField]
    public FsmObject objectVariable;
    [Tooltip("Repeat every frame. Useful if the Material variable is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.materialVariable = (FsmMaterial) null;
      this.objectVariable = (FsmObject) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoConvertMaterialToObject();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoConvertMaterialToObject();
    }

    private void DoConvertMaterialToObject()
    {
      this.objectVariable.Value = (Object) this.materialVariable.Value;
    }
  }
}
