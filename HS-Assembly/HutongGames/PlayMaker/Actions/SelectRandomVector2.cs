﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SelectRandomVector2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Select a Random Vector2 from a Vector2 array.")]
  [ActionCategory(ActionCategory.Vector2)]
  public class SelectRandomVector2 : FsmStateAction
  {
    [Tooltip("The array of Vectors and respective weights")]
    [CompoundArray("Vectors", "Vector", "Weight")]
    public FsmVector2[] vector2Array;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat[] weights;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("The picked vector2")]
    public FsmVector2 storeVector2;

    public override void Reset()
    {
      this.vector2Array = new FsmVector2[3];
      this.weights = new FsmFloat[3]
      {
        (FsmFloat) 1f,
        (FsmFloat) 1f,
        (FsmFloat) 1f
      };
      this.storeVector2 = (FsmVector2) null;
    }

    public override void OnEnter()
    {
      this.DoSelectRandomColor();
      this.Finish();
    }

    private void DoSelectRandomColor()
    {
      if (this.vector2Array == null || this.vector2Array.Length == 0 || this.storeVector2 == null)
        return;
      int randomWeightedIndex = ActionHelpers.GetRandomWeightedIndex(this.weights);
      if (randomWeightedIndex == -1)
        return;
      this.storeVector2.Value = this.vector2Array[randomWeightedIndex].Value;
    }
  }
}
