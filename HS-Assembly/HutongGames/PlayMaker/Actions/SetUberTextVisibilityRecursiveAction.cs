﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetUberTextVisibilityRecursiveAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Sets the visibility on UberText objects and its UberText children.")]
  public class SetUberTextVisibilityRecursiveAction : FsmStateAction
  {
    private Map<UberText, bool> m_initialVisibility = new Map<UberText, bool>();
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Should the objects be set to visible or invisible?")]
    public FsmBool visible;
    [Tooltip("Resets to the initial visibility once\nit leaves the state")]
    public bool resetOnExit;
    public bool includeChildren;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.visible = (FsmBool) false;
      this.resetOnExit = false;
      this.includeChildren = false;
      this.m_initialVisibility.Clear();
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        if (this.includeChildren)
        {
          UberText[] componentsInChildren = ownerDefaultTarget.GetComponentsInChildren<UberText>();
          if (componentsInChildren != null)
          {
            foreach (UberText index in componentsInChildren)
            {
              this.m_initialVisibility[index] = !index.isHidden();
              if (this.visible.Value)
                index.Show();
              else
                index.Hide();
            }
          }
        }
        else
        {
          UberText component = ownerDefaultTarget.GetComponent<UberText>();
          if ((Object) component != (Object) null)
          {
            this.m_initialVisibility[component] = !component.isHidden();
            if (this.visible.Value)
              component.Show();
            else
              component.Hide();
          }
        }
        this.Finish();
      }
    }

    public override void OnExit()
    {
      if (!this.resetOnExit)
        return;
      using (Map<UberText, bool>.Enumerator enumerator = this.m_initialVisibility.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<UberText, bool> current = enumerator.Current;
          UberText key = current.Key;
          if (!((Object) key == (Object) null))
          {
            if (current.Value)
              key.Show();
            else
              key.Hide();
          }
        }
      }
    }
  }
}
