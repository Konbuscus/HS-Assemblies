﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.StringReplace
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Replace a substring with a new String.")]
  [ActionCategory(ActionCategory.String)]
  public class StringReplace : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmString stringVariable;
    public FsmString replace;
    public FsmString with;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmString storeResult;
    public bool everyFrame;

    public override void Reset()
    {
      this.stringVariable = (FsmString) null;
      this.replace = (FsmString) string.Empty;
      this.with = (FsmString) string.Empty;
      this.storeResult = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoReplace();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoReplace();
    }

    private void DoReplace()
    {
      if (this.stringVariable == null || this.storeResult == null)
        return;
      this.storeResult.Value = this.stringVariable.Value.Replace(this.replace.Value, this.with.Value);
    }
  }
}
