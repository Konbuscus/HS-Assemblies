﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellCardIdAudioPlayClipAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Generates an AudioSource based on a template, then plays an audio clip dependent on a Spell's Card's ID.")]
  [ActionCategory("Pegasus Audio")]
  public class SpellCardIdAudioPlayClipAction : SpellCardIdAudioAction
  {
    public FsmOwnerDefault m_SpellObject;
    [Tooltip("Which Card to check on the Spell.")]
    public SpellAction.Which m_WhichCard;
    [Tooltip("If specified, this Audio Source will be used as a template for the generated Audio Source, otherwise the one in the SoundConfig will be the template.")]
    public AudioSource m_TemplateSource;
    [CompoundArray("Clips", "Card Id", "Clip")]
    public string[] m_CardIds;
    public AudioClip[] m_Clips;
    public AudioClip m_DefaultClip;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_Volume;
    [HasFloatSlider(-3f, 3f)]
    public FsmFloat m_Pitch;
    [Tooltip("If you want the template Category the Category, change this so that it's not NONE.")]
    public SoundCategory m_Category;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      this.m_SpellObject = (FsmOwnerDefault) null;
      this.m_WhichCard = SpellAction.Which.SOURCE;
      this.m_TemplateSource = (AudioSource) null;
      this.m_CardIds = new string[2];
      this.m_Clips = new AudioClip[2];
      this.m_DefaultClip = (AudioClip) null;
      this.m_Volume = (FsmFloat) 1f;
      this.m_Pitch = (FsmFloat) 1f;
      this.m_Category = SoundCategory.FX;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      AudioClip clipMatchingCardId = this.GetClipMatchingCardId(this.m_WhichCard, this.m_CardIds, this.m_Clips, this.m_DefaultClip);
      if ((Object) clipMatchingCardId == (Object) null)
      {
        this.Finish();
      }
      else
      {
        SoundPlayClipArgs args = new SoundPlayClipArgs();
        args.m_templateSource = this.m_TemplateSource;
        args.m_clip = clipMatchingCardId;
        if (!this.m_Volume.IsNone)
          args.m_volume = new float?(this.m_Volume.Value);
        if (!this.m_Pitch.IsNone)
          args.m_pitch = new float?(this.m_Pitch.Value);
        if (this.m_Category != SoundCategory.NONE)
          args.m_category = new SoundCategory?(this.m_Category);
        args.m_parentObject = this.Owner;
        SoundManager.Get().PlayClip(args);
        this.Finish();
      }
    }
  }
}
