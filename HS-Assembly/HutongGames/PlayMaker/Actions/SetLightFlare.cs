﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetLightFlare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Lights)]
  [Tooltip("Sets the Flare effect used by a Light.")]
  public class SetLightFlare : ComponentAction<Light>
  {
    [RequiredField]
    [CheckForComponent(typeof (Light))]
    public FsmOwnerDefault gameObject;
    public Flare lightFlare;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.lightFlare = (Flare) null;
    }

    public override void OnEnter()
    {
      this.DoSetLightRange();
      this.Finish();
    }

    private void DoSetLightRange()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.light.flare = this.lightFlare;
    }
  }
}
