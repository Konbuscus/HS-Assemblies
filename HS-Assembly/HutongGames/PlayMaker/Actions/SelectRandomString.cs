﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SelectRandomString
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Select a Random String from an array of Strings.")]
  [ActionCategory(ActionCategory.String)]
  public class SelectRandomString : FsmStateAction
  {
    [CompoundArray("Strings", "String", "Weight")]
    public FsmString[] strings;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat[] weights;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmString storeString;

    public override void Reset()
    {
      this.strings = new FsmString[3];
      this.weights = new FsmFloat[3]
      {
        (FsmFloat) 1f,
        (FsmFloat) 1f,
        (FsmFloat) 1f
      };
      this.storeString = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.DoSelectRandomString();
      this.Finish();
    }

    private void DoSelectRandomString()
    {
      if (this.strings == null || this.strings.Length == 0 || this.storeString == null)
        return;
      int randomWeightedIndex = ActionHelpers.GetRandomWeightedIndex(this.weights);
      if (randomWeightedIndex == -1)
        return;
      this.storeString.Value = this.strings[randomWeightedIndex].Value;
    }
  }
}
