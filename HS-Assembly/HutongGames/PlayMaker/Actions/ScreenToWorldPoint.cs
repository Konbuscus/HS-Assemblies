﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ScreenToWorldPoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Transforms position from screen space into world space. NOTE: Uses the MainCamera!")]
  [ActionCategory(ActionCategory.Camera)]
  public class ScreenToWorldPoint : FsmStateAction
  {
    [Tooltip("Screen position as a vector.")]
    [UIHint(UIHint.Variable)]
    public FsmVector3 screenVector;
    [Tooltip("Screen X position in pixels or normalized. See Normalized.")]
    public FsmFloat screenX;
    [Tooltip("Screen X position in pixels or normalized. See Normalized.")]
    public FsmFloat screenY;
    [Tooltip("Distance into the screen in world units.")]
    public FsmFloat screenZ;
    [Tooltip("If true, X/Y coordinates are considered normalized (0-1), otherwise they are expected to be in pixels")]
    public FsmBool normalized;
    [Tooltip("Store the world position in a vector3 variable.")]
    [UIHint(UIHint.Variable)]
    public FsmVector3 storeWorldVector;
    [Tooltip("Store the world X position in a float variable.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeWorldX;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the world Y position in a float variable.")]
    public FsmFloat storeWorldY;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the world Z position in a float variable.")]
    public FsmFloat storeWorldZ;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.screenVector = (FsmVector3) null;
      FsmFloat fsmFloat1 = new FsmFloat();
      fsmFloat1.UseVariable = true;
      this.screenX = fsmFloat1;
      FsmFloat fsmFloat2 = new FsmFloat();
      fsmFloat2.UseVariable = true;
      this.screenY = fsmFloat2;
      this.screenZ = (FsmFloat) 1f;
      this.normalized = (FsmBool) false;
      this.storeWorldVector = (FsmVector3) null;
      this.storeWorldX = (FsmFloat) null;
      this.storeWorldY = (FsmFloat) null;
      this.storeWorldZ = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoScreenToWorldPoint();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoScreenToWorldPoint();
    }

    private void DoScreenToWorldPoint()
    {
      if ((Object) Camera.main == (Object) null)
      {
        this.LogError("No MainCamera defined!");
        this.Finish();
      }
      else
      {
        Vector3 zero = Vector3.zero;
        if (!this.screenVector.IsNone)
          zero = this.screenVector.Value;
        if (!this.screenX.IsNone)
          zero.x = this.screenX.Value;
        if (!this.screenY.IsNone)
          zero.y = this.screenY.Value;
        if (!this.screenZ.IsNone)
          zero.z = this.screenZ.Value;
        if (this.normalized.Value)
        {
          zero.x *= (float) Screen.width;
          zero.y *= (float) Screen.height;
        }
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(zero);
        this.storeWorldVector.Value = worldPoint;
        this.storeWorldX.Value = worldPoint.x;
        this.storeWorldY.Value = worldPoint.y;
        this.storeWorldZ.Value = worldPoint.z;
      }
    }
  }
}
