﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetMaterialTexture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets a named texture in a game object's material.")]
  [ActionCategory(ActionCategory.Material)]
  public class SetMaterialTexture : ComponentAction<Renderer>
  {
    [CheckForComponent(typeof (Renderer))]
    [Tooltip("The GameObject that the material is applied to.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("GameObjects can have multiple materials. Specify an index to target a specific material.")]
    public FsmInt materialIndex;
    [Tooltip("Alternatively specify a Material instead of a GameObject and Index.")]
    public FsmMaterial material;
    [UIHint(UIHint.NamedTexture)]
    [Tooltip("A named parameter in the shader.")]
    public FsmString namedTexture;
    public FsmTexture texture;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.materialIndex = (FsmInt) 0;
      this.material = (FsmMaterial) null;
      this.namedTexture = (FsmString) "_MainTex";
      this.texture = (FsmTexture) null;
    }

    public override void OnEnter()
    {
      this.DoSetMaterialTexture();
      this.Finish();
    }

    private void DoSetMaterialTexture()
    {
      string propertyName = this.namedTexture.Value;
      if (propertyName == string.Empty)
        propertyName = "_MainTex";
      if ((Object) this.material.Value != (Object) null)
      {
        this.material.Value.SetTexture(propertyName, this.texture.Value);
      }
      else
      {
        if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
          return;
        if ((Object) this.renderer.material == (Object) null)
          this.LogError("Missing Material!");
        else if (this.materialIndex.Value == 0)
        {
          this.renderer.material.SetTexture(propertyName, this.texture.Value);
        }
        else
        {
          if (this.renderer.materials.Length <= this.materialIndex.Value)
            return;
          Material[] materials = this.renderer.materials;
          materials[this.materialIndex.Value].SetTexture(propertyName, this.texture.Value);
          this.renderer.materials = materials;
        }
      }
    }
  }
}
