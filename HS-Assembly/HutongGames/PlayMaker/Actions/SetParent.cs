﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetParent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Sets the Parent of a Game Object.")]
  public class SetParent : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The Game Object to parent.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The new parent for the Game Object.")]
    public FsmGameObject parent;
    [Tooltip("Set the local position to 0,0,0 after parenting.")]
    public FsmBool resetLocalPosition;
    [Tooltip("Set the local rotation to 0,0,0 after parenting.")]
    public FsmBool resetLocalRotation;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.parent = (FsmGameObject) null;
      this.resetLocalPosition = (FsmBool) null;
      this.resetLocalRotation = (FsmBool) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget != (Object) null)
      {
        ownerDefaultTarget.transform.parent = !((Object) this.parent.Value == (Object) null) ? this.parent.Value.transform : (Transform) null;
        if (this.resetLocalPosition.Value)
          ownerDefaultTarget.transform.localPosition = Vector3.zero;
        if (this.resetLocalRotation.Value)
          ownerDefaultTarget.transform.localRotation = Quaternion.identity;
      }
      this.Finish();
    }
  }
}
