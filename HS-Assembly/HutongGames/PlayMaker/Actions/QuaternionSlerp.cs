﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.QuaternionSlerp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Spherically interpolates between from and to by t.")]
  [ActionCategory(ActionCategory.Quaternion)]
  public class QuaternionSlerp : QuaternionBaseAction
  {
    [Tooltip("From Quaternion.")]
    [RequiredField]
    public FsmQuaternion fromQuaternion;
    [Tooltip("To Quaternion.")]
    [RequiredField]
    public FsmQuaternion toQuaternion;
    [RequiredField]
    [Tooltip("Interpolate between fromQuaternion and toQuaternion by this amount. Value is clamped to 0-1 range. 0 = fromQuaternion; 1 = toQuaternion; 0.5 = half way between.")]
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat amount;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result in this quaternion variable.")]
    public FsmQuaternion storeResult;

    public override void Reset()
    {
      FsmQuaternion fsmQuaternion1 = new FsmQuaternion();
      fsmQuaternion1.UseVariable = true;
      this.fromQuaternion = fsmQuaternion1;
      FsmQuaternion fsmQuaternion2 = new FsmQuaternion();
      fsmQuaternion2.UseVariable = true;
      this.toQuaternion = fsmQuaternion2;
      this.amount = (FsmFloat) 0.1f;
      this.storeResult = (FsmQuaternion) null;
      this.everyFrame = true;
      this.everyFrameOption = QuaternionBaseAction.everyFrameOptions.Update;
    }

    public override void OnEnter()
    {
      this.DoQuatSlerp();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.Update)
        return;
      this.DoQuatSlerp();
    }

    public override void OnLateUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.LateUpdate)
        return;
      this.DoQuatSlerp();
    }

    public override void OnFixedUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.FixedUpdate)
        return;
      this.DoQuatSlerp();
    }

    private void DoQuatSlerp()
    {
      this.storeResult.Value = Quaternion.Slerp(this.fromQuaternion.Value, this.toQuaternion.Value, this.amount.Value);
    }
  }
}
