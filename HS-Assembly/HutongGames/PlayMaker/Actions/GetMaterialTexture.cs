﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMaterialTexture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Material)]
  [Tooltip("Get a texture from a material on a GameObject")]
  public class GetMaterialTexture : ComponentAction<Renderer>
  {
    [RequiredField]
    [CheckForComponent(typeof (Renderer))]
    [Tooltip("The GameObject the Material is applied to.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The index of the Material in the Materials array.")]
    public FsmInt materialIndex;
    [UIHint(UIHint.NamedTexture)]
    [Tooltip("The texture to get. See Unity Shader docs for names.")]
    public FsmString namedTexture;
    [UIHint(UIHint.Variable)]
    [Title("StoreTexture")]
    [Tooltip("Store the texture in a variable.")]
    [RequiredField]
    public FsmTexture storedTexture;
    [Tooltip("Get the shared version of the texture.")]
    public bool getFromSharedMaterial;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.materialIndex = (FsmInt) 0;
      this.namedTexture = (FsmString) "_MainTex";
      this.storedTexture = (FsmTexture) null;
      this.getFromSharedMaterial = false;
    }

    public override void OnEnter()
    {
      this.DoGetMaterialTexture();
      this.Finish();
    }

    private void DoGetMaterialTexture()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      string propertyName = this.namedTexture.Value;
      if (propertyName == string.Empty)
        propertyName = "_MainTex";
      if (this.materialIndex.Value == 0 && !this.getFromSharedMaterial)
        this.storedTexture.Value = this.renderer.material.GetTexture(propertyName);
      else if (this.materialIndex.Value == 0 && this.getFromSharedMaterial)
        this.storedTexture.Value = this.renderer.sharedMaterial.GetTexture(propertyName);
      else if (this.renderer.materials.Length > this.materialIndex.Value && !this.getFromSharedMaterial)
      {
        Material[] materials = this.renderer.materials;
        this.storedTexture.Value = this.renderer.materials[this.materialIndex.Value].GetTexture(propertyName);
        this.renderer.materials = materials;
      }
      else
      {
        if (this.renderer.materials.Length <= this.materialIndex.Value || !this.getFromSharedMaterial)
          return;
        Material[] sharedMaterials = this.renderer.sharedMaterials;
        this.storedTexture.Value = this.renderer.sharedMaterials[this.materialIndex.Value].GetTexture(propertyName);
        this.renderer.materials = sharedMaterials;
      }
    }
  }
}
