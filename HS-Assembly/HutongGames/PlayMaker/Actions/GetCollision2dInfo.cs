﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetCollision2dInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics2D)]
  [Tooltip("Gets info on the last collision 2D event and store in variables. See Unity and PlayMaker docs on Unity 2D physics.")]
  public class GetCollision2dInfo : FsmStateAction
  {
    [Tooltip("Get the GameObject hit.")]
    [UIHint(UIHint.Variable)]
    public FsmGameObject gameObjectHit;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the relative velocity of the collision.")]
    public FsmVector3 relativeVelocity;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the relative speed of the collision. Useful for controlling reactions. E.g., selecting an appropriate sound fx.")]
    public FsmFloat relativeSpeed;
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the world position of the collision contact. Useful for spawning effects etc.")]
    public FsmVector3 contactPoint;
    [Tooltip("Get the collision normal vector. Useful for aligning spawned effects etc.")]
    [UIHint(UIHint.Variable)]
    public FsmVector3 contactNormal;
    [UIHint(UIHint.Variable)]
    [Tooltip("The number of separate shaped regions in the collider.")]
    public FsmInt shapeCount;
    [Tooltip("Get the name of the physics 2D material of the colliding GameObject. Useful for triggering different effects. Audio, particles...")]
    [UIHint(UIHint.Variable)]
    public FsmString physics2dMaterialName;

    public override void Reset()
    {
      this.gameObjectHit = (FsmGameObject) null;
      this.relativeVelocity = (FsmVector3) null;
      this.relativeSpeed = (FsmFloat) null;
      this.contactPoint = (FsmVector3) null;
      this.contactNormal = (FsmVector3) null;
      this.shapeCount = (FsmInt) null;
      this.physics2dMaterialName = (FsmString) null;
    }

    private void StoreCollisionInfo()
    {
      if (this.Fsm.Collision2DInfo == null)
        return;
      this.gameObjectHit.Value = this.Fsm.Collision2DInfo.gameObject;
      this.relativeSpeed.Value = this.Fsm.Collision2DInfo.relativeVelocity.magnitude;
      this.relativeVelocity.Value = (Vector3) this.Fsm.Collision2DInfo.relativeVelocity;
      this.physics2dMaterialName.Value = !((Object) this.Fsm.Collision2DInfo.collider.sharedMaterial != (Object) null) ? string.Empty : this.Fsm.Collision2DInfo.collider.sharedMaterial.name;
      this.shapeCount.Value = this.Fsm.Collision2DInfo.collider.shapeCount;
      if (this.Fsm.Collision2DInfo.contacts == null || this.Fsm.Collision2DInfo.contacts.Length <= 0)
        return;
      this.contactPoint.Value = (Vector3) this.Fsm.Collision2DInfo.contacts[0].point;
      this.contactNormal.Value = (Vector3) this.Fsm.Collision2DInfo.contacts[0].normal;
    }

    public override void OnEnter()
    {
      this.StoreCollisionInfo();
      this.Finish();
    }
  }
}
