﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.MousePick2dEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Input)]
  [Tooltip("Sends Events based on mouse interactions with a 2d Game Object: MouseOver, MouseDown, MouseUp, MouseOff.")]
  public class MousePick2dEvent : FsmStateAction
  {
    [Tooltip("The GameObject with a Collider2D attached.")]
    [CheckForComponent(typeof (Collider2D))]
    public FsmOwnerDefault GameObject;
    [Tooltip("Event to send when the mouse is over the GameObject.")]
    public FsmEvent mouseOver;
    [Tooltip("Event to send when the mouse is pressed while over the GameObject.")]
    public FsmEvent mouseDown;
    [Tooltip("Event to send when the mouse is released while over the GameObject.")]
    public FsmEvent mouseUp;
    [Tooltip("Event to send when the mouse moves off the GameObject.")]
    public FsmEvent mouseOff;
    [UIHint(UIHint.Layer)]
    [Tooltip("Pick only from these layers.")]
    public FsmInt[] layerMask;
    [Tooltip("Invert the mask, so you pick from all layers except those defined above.")]
    public FsmBool invertMask;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.GameObject = (FsmOwnerDefault) null;
      this.mouseOver = (FsmEvent) null;
      this.mouseDown = (FsmEvent) null;
      this.mouseUp = (FsmEvent) null;
      this.mouseOff = (FsmEvent) null;
      this.layerMask = new FsmInt[0];
      this.invertMask = (FsmBool) false;
      this.everyFrame = true;
    }

    public override void OnEnter()
    {
      this.DoMousePickEvent();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoMousePickEvent();
    }

    private void DoMousePickEvent()
    {
      if (this.DoRaycast())
      {
        if (this.mouseDown != null && Input.GetMouseButtonDown(0))
          this.Fsm.Event(this.mouseDown);
        if (this.mouseOver != null)
          this.Fsm.Event(this.mouseOver);
        if (this.mouseUp == null || !Input.GetMouseButtonUp(0))
          return;
        this.Fsm.Event(this.mouseUp);
      }
      else
      {
        if (this.mouseOff == null)
          return;
        this.Fsm.Event(this.mouseOff);
      }
    }

    private bool DoRaycast()
    {
      UnityEngine.GameObject gameObject = this.GameObject.OwnerOption != OwnerDefaultOption.UseOwner ? this.GameObject.GameObject.Value : this.Owner;
      RaycastHit2D rayIntersection = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition), float.PositiveInfinity, ActionHelpers.LayerArrayToLayerMask(this.layerMask, this.invertMask.Value));
      Fsm.RecordLastRaycastHit2DInfo(this.Fsm, rayIntersection);
      return (Object) rayIntersection.transform != (Object) null && (Object) rayIntersection.transform.gameObject == (Object) gameObject;
    }
  }
}
