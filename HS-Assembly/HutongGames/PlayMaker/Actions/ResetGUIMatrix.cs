﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ResetGUIMatrix
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("Resets the GUI matrix. Useful if you've rotated or scaled the GUI and now want to reset it.")]
  public class ResetGUIMatrix : FsmStateAction
  {
    public override void OnGUI()
    {
      Matrix4x4 identity = Matrix4x4.identity;
      GUI.matrix = identity;
      PlayMakerGUI.GUIMatrix = identity;
    }
  }
}
