﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetKeyDown
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sends an Event when a Key is pressed.")]
  [ActionCategory(ActionCategory.Input)]
  public class GetKeyDown : FsmStateAction
  {
    [RequiredField]
    public KeyCode key;
    public FsmEvent sendEvent;
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;

    public override void Reset()
    {
      this.sendEvent = (FsmEvent) null;
      this.key = KeyCode.None;
      this.storeResult = (FsmBool) null;
    }

    public override void OnUpdate()
    {
      bool keyDown = Input.GetKeyDown(this.key);
      if (keyDown)
        this.Fsm.Event(this.sendEvent);
      this.storeResult.Value = keyDown;
    }
  }
}
