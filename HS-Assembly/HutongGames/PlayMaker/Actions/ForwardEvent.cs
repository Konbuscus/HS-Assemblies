﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ForwardEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Forward an event received by this FSM to another target.")]
  [ActionCategory(ActionCategory.StateMachine)]
  public class ForwardEvent : FsmStateAction
  {
    [Tooltip("Forward to this target.")]
    public FsmEventTarget forwardTo;
    [Tooltip("The events to forward.")]
    public FsmEvent[] eventsToForward;
    [Tooltip("Should this action eat the events or pass them on.")]
    public bool eatEvents;

    public override void Reset()
    {
      this.forwardTo = new FsmEventTarget()
      {
        target = FsmEventTarget.EventTarget.FSMComponent
      };
      this.eventsToForward = (FsmEvent[]) null;
      this.eatEvents = true;
    }

    public override bool Event(FsmEvent fsmEvent)
    {
      if (this.eventsToForward != null)
      {
        foreach (FsmEvent fsmEvent1 in this.eventsToForward)
        {
          if (fsmEvent1 == fsmEvent)
          {
            this.Fsm.Event(this.forwardTo, fsmEvent);
            return this.eatEvents;
          }
        }
      }
      return false;
    }
  }
}
