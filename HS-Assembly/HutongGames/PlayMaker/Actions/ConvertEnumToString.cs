﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ConvertEnumToString
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Convert)]
  [Tooltip("Converts an Enum value to a String value.")]
  public class ConvertEnumToString : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The Enum variable to convert.")]
    public FsmEnum enumVariable;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The String variable to store the converted value.")]
    public FsmString stringVariable;
    [Tooltip("Repeat every frame. Useful if the Enum variable is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.enumVariable = (FsmEnum) null;
      this.stringVariable = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoConvertEnumToString();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoConvertEnumToString();
    }

    private void DoConvertEnumToString()
    {
      this.stringVariable.Value = this.enumVariable.get_Value() == null ? string.Empty : this.enumVariable.get_Value().ToString();
    }
  }
}
