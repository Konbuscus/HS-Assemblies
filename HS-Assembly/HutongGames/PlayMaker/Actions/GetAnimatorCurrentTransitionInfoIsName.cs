﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Check the active Transition name on a specified layer. Format is 'CURRENT_STATE -> NEXT_STATE'.")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorCurrentTransitionInfoIsName : FsmStateActionAnimatorBase
  {
    [Tooltip("The target. An Animator component is required")]
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The layer's index")]
    public FsmInt layerIndex;
    [Tooltip("The name to check the transition against.")]
    public FsmString name;
    [UIHint(UIHint.Variable)]
    [ActionSection("Results")]
    [Tooltip("True if name matches")]
    public FsmBool nameMatch;
    [Tooltip("Event send if name matches")]
    public FsmEvent nameMatchEvent;
    [Tooltip("Event send if name doesn't match")]
    public FsmEvent nameDoNotMatchEvent;
    private Animator _animator;

    public override void Reset()
    {
      base.Reset();
      this.gameObject = (FsmOwnerDefault) null;
      this.layerIndex = (FsmInt) null;
      this.name = (FsmString) null;
      this.nameMatch = (FsmBool) null;
      this.nameMatchEvent = (FsmEvent) null;
      this.nameDoNotMatchEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.IsName();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnActionUpdate()
    {
      this.IsName();
    }

    private void IsName()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      if (this._animator.GetAnimatorTransitionInfo(this.layerIndex.Value).IsName(this.name.Value))
      {
        this.nameMatch.Value = true;
        this.Fsm.Event(this.nameMatchEvent);
      }
      else
      {
        this.nameMatch.Value = false;
        this.Fsm.Event(this.nameDoNotMatchEvent);
      }
    }
  }
}
