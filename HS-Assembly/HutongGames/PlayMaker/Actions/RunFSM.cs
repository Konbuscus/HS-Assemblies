﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.RunFSM
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Creates an FSM from a saved FSM Template.")]
  [ActionCategory(ActionCategory.StateMachine)]
  public class RunFSM : RunFSMAction
  {
    public FsmTemplateControl fsmTemplateControl = new FsmTemplateControl();
    [UIHint(UIHint.Variable)]
    public FsmInt storeID;
    [Tooltip("Event to send when the FSM has finished (usually because it ran a Finish FSM action).")]
    public FsmEvent finishEvent;

    public override void Reset()
    {
      this.fsmTemplateControl = new FsmTemplateControl();
      this.storeID = (FsmInt) null;
      this.runFsm = (Fsm) null;
    }

    public override void Awake()
    {
      if (!((Object) this.fsmTemplateControl.fsmTemplate != (Object) null) || !Application.isPlaying)
        return;
      this.runFsm = this.Fsm.CreateSubFsm(this.fsmTemplateControl);
    }

    public override void OnEnter()
    {
      if (this.runFsm == null)
      {
        this.Finish();
      }
      else
      {
        this.fsmTemplateControl.UpdateValues();
        this.fsmTemplateControl.ApplyOverrides(this.runFsm);
        this.runFsm.OnEnable();
        if (!this.runFsm.Started)
          this.runFsm.Start();
        this.storeID.Value = this.fsmTemplateControl.ID;
        this.CheckIfFinished();
      }
    }

    protected override void CheckIfFinished()
    {
      if (this.runFsm != null && !this.runFsm.Finished)
        return;
      this.Finish();
      this.Fsm.Event(this.finishEvent);
    }
  }
}
