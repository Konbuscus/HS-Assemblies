﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DebugLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Debug)]
  [Tooltip("Sends a log message to the PlayMaker Log Window.")]
  public class DebugLog : BaseLogAction
  {
    [Tooltip("Info, Warning, or Error.")]
    public LogLevel logLevel;
    [Tooltip("Text to send to the log.")]
    public FsmString text;

    public override void Reset()
    {
      this.logLevel = LogLevel.Info;
      this.text = (FsmString) string.Empty;
      base.Reset();
    }

    public override void OnEnter()
    {
      if (!string.IsNullOrEmpty(this.text.Value))
        ActionHelpers.DebugLog(this.Fsm, this.logLevel, this.text.Value, this.sendToUnityLog);
      this.Finish();
    }
  }
}
