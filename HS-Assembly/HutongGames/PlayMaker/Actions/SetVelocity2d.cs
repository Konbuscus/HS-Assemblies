﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetVelocity2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics2D)]
  [Tooltip("Sets the 2d Velocity of a Game Object. To leave any axis unchanged, set variable to 'None'. NOTE: Game object must have a rigidbody 2D.")]
  public class SetVelocity2d : ComponentAction<Rigidbody2D>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody2D))]
    [Tooltip("The GameObject with the Rigidbody2D attached")]
    public FsmOwnerDefault gameObject;
    [Tooltip("A Vector2 value for the velocity")]
    public FsmVector2 vector;
    [Tooltip("The y value of the velocity. Overrides 'Vector' x value if set")]
    public FsmFloat x;
    [Tooltip("The y value of the velocity. Overrides 'Vector' y value if set")]
    public FsmFloat y;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.vector = (FsmVector2) null;
      FsmFloat fsmFloat1 = new FsmFloat();
      fsmFloat1.UseVariable = true;
      this.x = fsmFloat1;
      FsmFloat fsmFloat2 = new FsmFloat();
      fsmFloat2.UseVariable = true;
      this.y = fsmFloat2;
      this.everyFrame = false;
    }

    public override void Awake()
    {
      this.Fsm.HandleFixedUpdate = true;
    }

    public override void OnEnter()
    {
      this.DoSetVelocity();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnFixedUpdate()
    {
      this.DoSetVelocity();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    private void DoSetVelocity()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      Vector2 vector2 = !this.vector.IsNone ? this.vector.Value : this.rigidbody2d.velocity;
      if (!this.x.IsNone)
        vector2.x = this.x.Value;
      if (!this.y.IsNone)
        vector2.y = this.y.Value;
      this.rigidbody2d.velocity = vector2;
    }
  }
}
