﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GameObjectHasChildren
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Logic)]
  [Tooltip("Tests if a GameObject has children.")]
  public class GameObjectHasChildren : FsmStateAction
  {
    [Tooltip("The GameObject to test.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Event to send if the GameObject has children.")]
    public FsmEvent trueEvent;
    [Tooltip("Event to send if the GameObject does not have children.")]
    public FsmEvent falseEvent;
    [Tooltip("Store the result in a bool variable.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoHasChildren();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoHasChildren();
    }

    private void DoHasChildren()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      bool flag = ownerDefaultTarget.transform.childCount > 0;
      this.storeResult.Value = flag;
      this.Fsm.Event(!flag ? this.falseEvent : this.trueEvent);
    }
  }
}
