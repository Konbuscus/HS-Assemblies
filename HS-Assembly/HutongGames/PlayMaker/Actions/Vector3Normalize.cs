﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector3Normalize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Vector3)]
  [Tooltip("Normalizes a Vector3 Variable.")]
  public class Vector3Normalize : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmVector3 vector3Variable;
    public bool everyFrame;

    public override void Reset()
    {
      this.vector3Variable = (FsmVector3) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.vector3Variable.Value = this.vector3Variable.Value.normalized;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.vector3Variable.Value = this.vector3Variable.Value.normalized;
    }
  }
}
