﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FindClosest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Finds the closest object to the specified Game Object.\nOptionally filter by Tag and Visibility.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class FindClosest : FsmStateAction
  {
    [Tooltip("The GameObject to measure from.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Only consider objects with this Tag. NOTE: It's generally a lot quicker to find objects with a Tag!")]
    [UIHint(UIHint.Tag)]
    [RequiredField]
    public FsmString withTag;
    [Tooltip("If checked, ignores the object that owns this FSM.")]
    public FsmBool ignoreOwner;
    [Tooltip("Only consider objects visible to the camera.")]
    public FsmBool mustBeVisible;
    [Tooltip("Store the closest object.")]
    [UIHint(UIHint.Variable)]
    public FsmGameObject storeObject;
    [Tooltip("Store the distance to the closest object.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeDistance;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.withTag = (FsmString) "Untagged";
      this.ignoreOwner = (FsmBool) true;
      this.mustBeVisible = (FsmBool) false;
      this.storeObject = (FsmGameObject) null;
      this.storeDistance = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoFindClosest();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoFindClosest();
    }

    private void DoFindClosest()
    {
      GameObject gameObject1 = this.gameObject.OwnerOption != OwnerDefaultOption.UseOwner ? this.gameObject.GameObject.Value : this.Owner;
      GameObject[] gameObjectArray = string.IsNullOrEmpty(this.withTag.Value) || this.withTag.Value == "Untagged" ? (GameObject[]) Object.FindObjectsOfType(typeof (GameObject)) : GameObject.FindGameObjectsWithTag(this.withTag.Value);
      GameObject gameObject2 = (GameObject) null;
      float f = float.PositiveInfinity;
      foreach (GameObject go in gameObjectArray)
      {
        if ((!this.ignoreOwner.Value || !((Object) go == (Object) this.Owner)) && (!this.mustBeVisible.Value || ActionHelpers.IsVisible(go)))
        {
          float sqrMagnitude = (gameObject1.transform.position - go.transform.position).sqrMagnitude;
          if ((double) sqrMagnitude < (double) f)
          {
            f = sqrMagnitude;
            gameObject2 = go;
          }
        }
      }
      this.storeObject.Value = gameObject2;
      if (this.storeDistance.IsNone)
        return;
      this.storeDistance.Value = Mathf.Sqrt(f);
    }
  }
}
