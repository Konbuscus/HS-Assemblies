﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FloatChanged
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if the value of a Float variable changed. Use this to send an event on change, or store a bool that can be used in other operations.")]
  [ActionCategory(ActionCategory.Logic)]
  public class FloatChanged : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The Float variable to watch for a change.")]
    public FsmFloat floatVariable;
    [Tooltip("Event to send if the float variable changes.")]
    public FsmEvent changedEvent;
    [Tooltip("Set to True if the float variable changes.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    private float previousValue;

    public override void Reset()
    {
      this.floatVariable = (FsmFloat) null;
      this.changedEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
    }

    public override void OnEnter()
    {
      if (this.floatVariable.IsNone)
        this.Finish();
      else
        this.previousValue = this.floatVariable.Value;
    }

    public override void OnUpdate()
    {
      this.storeResult.Value = false;
      if ((double) this.floatVariable.Value == (double) this.previousValue)
        return;
      this.previousValue = this.floatVariable.Value;
      this.storeResult.Value = true;
      this.Fsm.Event(this.changedEvent);
    }
  }
}
