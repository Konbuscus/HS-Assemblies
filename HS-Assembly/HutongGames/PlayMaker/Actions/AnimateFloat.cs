﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimateFloat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.AnimateVariables)]
  [Tooltip("Animates the value of a Float Variable using an Animation Curve.")]
  public class AnimateFloat : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The animation curve to use.")]
    public FsmAnimationCurve animCurve;
    [Tooltip("The float variable to set.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmFloat floatVariable;
    [Tooltip("Optionally send an Event when the animation finishes.")]
    public FsmEvent finishEvent;
    [Tooltip("Ignore TimeScale. Useful if the game is paused.")]
    public bool realTime;
    private float startTime;
    private float currentTime;
    private float endTime;
    private bool looping;

    public override void Reset()
    {
      this.animCurve = (FsmAnimationCurve) null;
      this.floatVariable = (FsmFloat) null;
      this.finishEvent = (FsmEvent) null;
      this.realTime = false;
    }

    public override void OnEnter()
    {
      this.startTime = FsmTime.RealtimeSinceStartup;
      this.currentTime = 0.0f;
      if (this.animCurve != null && this.animCurve.curve != null && this.animCurve.curve.keys.Length > 0)
      {
        this.endTime = this.animCurve.curve.keys[this.animCurve.curve.length - 1].time;
        this.looping = ActionHelpers.IsLoopingWrapMode(this.animCurve.curve.postWrapMode);
        this.floatVariable.Value = this.animCurve.curve.Evaluate(0.0f);
      }
      else
        this.Finish();
    }

    public override void OnUpdate()
    {
      if (this.realTime)
        this.currentTime = FsmTime.RealtimeSinceStartup - this.startTime;
      else
        this.currentTime += Time.deltaTime;
      if (this.animCurve != null && this.animCurve.curve != null && this.floatVariable != null)
        this.floatVariable.Value = this.animCurve.curve.Evaluate(this.currentTime);
      if ((double) this.currentTime < (double) this.endTime)
        return;
      if (!this.looping)
        this.Finish();
      if (this.finishEvent == null)
        return;
      this.Fsm.Event(this.finishEvent);
    }
  }
}
