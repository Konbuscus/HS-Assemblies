﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioMute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Mute/unmute the Audio Clip played by an Audio Source component on a Game Object.")]
  [ActionCategory(ActionCategory.Audio)]
  public class AudioMute : FsmStateAction
  {
    [Tooltip("The GameObject with an Audio Source component.")]
    [RequiredField]
    [CheckForComponent(typeof (AudioSource))]
    public FsmOwnerDefault gameObject;
    [Tooltip("Check to mute, uncheck to unmute.")]
    [RequiredField]
    public FsmBool mute;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.mute = (FsmBool) false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget != (Object) null)
      {
        AudioSource component = ownerDefaultTarget.GetComponent<AudioSource>();
        if ((Object) component != (Object) null)
          component.mute = this.mute.Value;
      }
      this.Finish();
    }
  }
}
