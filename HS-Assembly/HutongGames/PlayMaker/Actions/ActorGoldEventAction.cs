﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActorGoldEventAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Send an event based on an Actor's Card's Golden state.")]
  public class ActorGoldEventAction : ActorAction
  {
    public FsmOwnerDefault m_ActorObject;
    public FsmEvent m_GoldenCardEvent;
    public FsmEvent m_StandardCardEvent;

    protected override GameObject GetActorOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_ActorObject);
    }

    public override void Reset()
    {
      this.m_ActorObject = (FsmOwnerDefault) null;
      this.m_GoldenCardEvent = (FsmEvent) null;
      this.m_StandardCardEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_actor == (Object) null)
      {
        this.Finish();
      }
      else
      {
        if (this.m_actor.GetPremium() == TAG_PREMIUM.GOLDEN)
          this.Fsm.Event(this.m_GoldenCardEvent);
        else
          this.Fsm.Event(this.m_StandardCardEvent);
        this.Finish();
      }
    }
  }
}
