﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DetachChildren
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Unparents all children from the Game Object.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class DetachChildren : FsmStateAction
  {
    [Tooltip("GameObject to unparent children from.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      DetachChildren.DoDetachChildren(this.Fsm.GetOwnerDefaultTarget(this.gameObject));
      this.Finish();
    }

    private static void DoDetachChildren(GameObject go)
    {
      if (!((Object) go != (Object) null))
        return;
      go.transform.DetachChildren();
    }
  }
}
