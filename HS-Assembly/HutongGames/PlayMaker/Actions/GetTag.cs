﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Gets a Game Object's Tag and stores it in a String Variable.")]
  public class GetTag : FsmStateAction
  {
    [RequiredField]
    public FsmGameObject gameObject;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmString storeResult;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmGameObject) null;
      this.storeResult = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetTag();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetTag();
    }

    private void DoGetTag()
    {
      if ((Object) this.gameObject.Value == (Object) null)
        return;
      this.storeResult.Value = this.gameObject.Value.tag;
    }
  }
}
