﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActivateGameObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Activates/deactivates a Game Object. Use this to hide/show areas, or enable/disable many Behaviours at once.")]
  public class ActivateGameObject : FsmStateAction
  {
    [Tooltip("The GameObject to activate/deactivate.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Check to activate, uncheck to deactivate Game Object.")]
    [RequiredField]
    public FsmBool activate;
    [Tooltip("Recursively activate/deactivate all children.")]
    public FsmBool recursive;
    [Tooltip("Reset the game objects when exiting this state. Useful if you want an object to be active only while this state is active.\nNote: Only applies to the last Game Object activated/deactivated (won't work if Game Object changes).")]
    public bool resetOnExit;
    [Tooltip("Repeat this action every frame. Useful if Activate changes over time.")]
    public bool everyFrame;
    private GameObject activatedGameObject;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.activate = (FsmBool) true;
      this.recursive = (FsmBool) true;
      this.resetOnExit = false;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoActivateGameObject();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoActivateGameObject();
    }

    public override void OnExit()
    {
      if ((UnityEngine.Object) this.activatedGameObject == (UnityEngine.Object) null || !this.resetOnExit)
        return;
      if (this.recursive.Value)
        this.SetActiveRecursively(this.activatedGameObject, !this.activate.Value);
      else
        this.activatedGameObject.SetActive(!this.activate.Value);
    }

    private void DoActivateGameObject()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((UnityEngine.Object) ownerDefaultTarget == (UnityEngine.Object) null)
        return;
      if (this.recursive.Value)
        this.SetActiveRecursively(ownerDefaultTarget, this.activate.Value);
      else
        ownerDefaultTarget.SetActive(this.activate.Value);
      this.activatedGameObject = ownerDefaultTarget;
    }

    public void SetActiveRecursively(GameObject go, bool state)
    {
      go.SetActive(state);
      IEnumerator enumerator = go.transform.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
          this.SetActiveRecursively(((Component) enumerator.Current).gameObject, state);
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
    }
  }
}
