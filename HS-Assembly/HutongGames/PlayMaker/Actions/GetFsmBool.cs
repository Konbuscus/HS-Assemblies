﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetFsmBool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionTarget(typeof (PlayMakerFSM), "gameObject,fsmName", false)]
  [ActionCategory(ActionCategory.StateMachine)]
  [Tooltip("Get the value of a Bool Variable from another FSM.")]
  public class GetFsmBool : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.FsmName)]
    [Tooltip("Optional name of FSM on Game Object")]
    public FsmString fsmName;
    [RequiredField]
    [UIHint(UIHint.FsmBool)]
    public FsmString variableName;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmBool storeValue;
    public bool everyFrame;
    private GameObject goLastFrame;
    private PlayMakerFSM fsm;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.fsmName = (FsmString) string.Empty;
      this.storeValue = (FsmBool) null;
    }

    public override void OnEnter()
    {
      this.DoGetFsmBool();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetFsmBool();
    }

    private void DoGetFsmBool()
    {
      if (this.storeValue == null)
        return;
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((Object) ownerDefaultTarget != (Object) this.goLastFrame)
      {
        this.goLastFrame = ownerDefaultTarget;
        this.fsm = ActionHelpers.GetGameObjectFsm(ownerDefaultTarget, this.fsmName.Value);
      }
      if ((Object) this.fsm == (Object) null)
        return;
      FsmBool fsmBool = this.fsm.FsmVariables.GetFsmBool(this.variableName.Value);
      if (fsmBool == null)
        return;
      this.storeValue.Value = fsmBool.Value;
    }
  }
}
