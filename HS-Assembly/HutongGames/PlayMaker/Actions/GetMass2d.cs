﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMass2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the Mass of a Game Object's Rigid Body 2D.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class GetMass2d : ComponentAction<Rigidbody2D>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody2D))]
    [Tooltip("The GameObject with a Rigidbody2D attached.")]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("Store the mass of gameObject.")]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeResult;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.storeResult = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      this.DoGetMass();
      this.Finish();
    }

    private void DoGetMass()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.storeResult.Value = this.rigidbody2d.mass;
    }
  }
}
