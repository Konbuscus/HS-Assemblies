﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetMaterialFloatAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets a named float in a game object's material.")]
  [ActionCategory("Pegasus")]
  public class SetMaterialFloatAction : FsmStateAction
  {
    [Tooltip("The GameObject that the material is applied to.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("GameObjects can have multiple materials. Specify an index to target a specific material.")]
    public FsmInt materialIndex;
    [Tooltip("Alternatively specify a Material instead of a GameObject and Index.")]
    public FsmMaterial material;
    [RequiredField]
    [Tooltip("A named float parameter in the shader.")]
    public FsmString namedFloat;
    [RequiredField]
    [Tooltip("Set the parameter value.")]
    public FsmFloat floatValue;
    [Tooltip("Repeat every frame. Useful if the value is animated.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.materialIndex = (FsmInt) 0;
      this.material = (FsmMaterial) null;
      this.namedFloat = (FsmString) string.Empty;
      this.floatValue = (FsmFloat) 0.0f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetMaterialFloat();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetMaterialFloat();
    }

    private void DoSetMaterialFloat()
    {
      if ((Object) this.material.Value != (Object) null)
      {
        this.material.Value.SetFloat(this.namedFloat.Value, this.floatValue.Value);
      }
      else
      {
        GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
        if ((Object) ownerDefaultTarget == (Object) null)
          return;
        if ((Object) ownerDefaultTarget.GetComponent<Renderer>() == (Object) null && (Object) ownerDefaultTarget.GetComponent<SkinnedMeshRenderer>() == (Object) null)
          this.LogError("Missing Renderer!");
        else if (this.materialIndex.Value == 0)
        {
          if ((Object) ownerDefaultTarget.GetComponent<Renderer>() == (Object) null)
          {
            SkinnedMeshRenderer component = ownerDefaultTarget.GetComponent<SkinnedMeshRenderer>();
            if ((Object) component == (Object) null)
              this.LogError("Missing Renderer!");
            else
              component.material.SetFloat(this.namedFloat.Value, this.floatValue.Value);
          }
          else
            ownerDefaultTarget.GetComponent<Renderer>().material.SetFloat(this.namedFloat.Value, this.floatValue.Value);
        }
        else
        {
          if (ownerDefaultTarget.GetComponent<Renderer>().materials.Length <= this.materialIndex.Value)
            return;
          if ((Object) ownerDefaultTarget.GetComponent<Renderer>() == (Object) null)
          {
            SkinnedMeshRenderer component = ownerDefaultTarget.GetComponent<SkinnedMeshRenderer>();
            if ((Object) component == (Object) null)
            {
              this.LogError("Missing Renderer!");
            }
            else
            {
              Material[] materials = component.materials;
              materials[this.materialIndex.Value].SetFloat(this.namedFloat.Value, this.floatValue.Value);
              component.materials = materials;
            }
          }
          else
          {
            Material[] materials = ownerDefaultTarget.GetComponent<Renderer>().materials;
            materials[this.materialIndex.Value].SetFloat(this.namedFloat.Value, this.floatValue.Value);
            ownerDefaultTarget.GetComponent<Renderer>().materials = materials;
          }
        }
      }
    }
  }
}
