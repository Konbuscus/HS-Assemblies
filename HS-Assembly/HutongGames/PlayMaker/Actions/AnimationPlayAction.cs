﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimationPlayAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Plays an Animation on a Game Object and waits for the animation to finish.")]
  [ActionCategory("Pegasus")]
  public class AnimationPlayAction : FsmStateAction
  {
    [CheckForComponent(typeof (Animation))]
    [RequiredField]
    [Tooltip("Game Object to play the animation on.")]
    public FsmOwnerDefault m_GameObject;
    [Tooltip("The name of the animation to play.")]
    [UIHint(UIHint.Animation)]
    public FsmString m_AnimName;
    public FsmString m_PhoneAnimName;
    [Tooltip("How to treat previously playing animations.")]
    public PlayMode m_PlayMode;
    [HasFloatSlider(0.0f, 5f)]
    [Tooltip("Time taken to cross fade to this animation.")]
    public FsmFloat m_CrossFadeSec;
    [Tooltip("Event to send when the animation is finished playing. NOTE: Not sent with Loop or PingPong wrap modes!")]
    public FsmEvent m_FinishEvent;
    [Tooltip("Event to send when the animation loops. If you want to send this event to another FSM use Set Event Target. NOTE: This event is only sent with Loop and PingPong wrap modes.")]
    public FsmEvent m_LoopEvent;
    [Tooltip("Stop playing the animation when this state is exited.")]
    public bool m_StopOnExit;
    private string m_animName;
    private AnimationState m_animState;
    private float m_prevAnimTime;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_AnimName = (FsmString) null;
      this.m_PhoneAnimName = (FsmString) null;
      this.m_PlayMode = PlayMode.StopAll;
      this.m_CrossFadeSec = (FsmFloat) 0.3f;
      this.m_FinishEvent = (FsmEvent) null;
      this.m_LoopEvent = (FsmEvent) null;
      this.m_StopOnExit = false;
    }

    public override void OnEnter()
    {
      if (!this.CacheAnim())
        this.Finish();
      else
        this.StartAnimation();
    }

    public override void OnUpdate()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        this.Finish();
      else if ((Object) ownerDefaultTarget.GetComponent<Animation>() == (Object) null)
      {
        Debug.LogWarning((object) string.Format("AnimationPlayAction.OnUpdate() - GameObject {0} is missing an animation component", (object) ownerDefaultTarget));
        this.Finish();
      }
      else
      {
        if (!this.m_animState.enabled || this.m_animState.wrapMode == WrapMode.ClampForever && (double) this.m_animState.time > (double) this.m_animState.length)
        {
          this.Fsm.Event(this.m_FinishEvent);
          this.Finish();
        }
        if (this.m_animState.wrapMode == WrapMode.ClampForever || (double) this.m_animState.time <= (double) this.m_animState.length || (double) this.m_prevAnimTime >= (double) this.m_animState.length)
          return;
        this.Fsm.Event(this.m_LoopEvent);
      }
    }

    public override void OnExit()
    {
      if (!this.m_StopOnExit)
        return;
      this.StopAnimation();
    }

    private bool CacheAnim()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      this.m_animName = this.m_AnimName.Value;
      if ((bool) UniversalInputManager.UsePhoneUI && !string.IsNullOrEmpty(this.m_PhoneAnimName.Value))
        this.m_animName = this.m_PhoneAnimName.Value;
      this.m_animState = ownerDefaultTarget.GetComponent<Animation>()[this.m_animName];
      return true;
    }

    private void StartAnimation()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      float fadeLength = this.m_CrossFadeSec.Value;
      if ((double) fadeLength <= (double) Mathf.Epsilon)
        ownerDefaultTarget.GetComponent<Animation>().Play(this.m_animName, this.m_PlayMode);
      else
        ownerDefaultTarget.GetComponent<Animation>().CrossFade(this.m_animName, fadeLength, this.m_PlayMode);
      this.m_prevAnimTime = this.m_animState.time;
    }

    private void StopAnimation()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null || (Object) ownerDefaultTarget.GetComponent<Animation>() == (Object) null)
        return;
      ownerDefaultTarget.GetComponent<Animation>().Stop(this.m_animName);
    }
  }
}
