﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAnimatorBool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Sets the value of a bool parameter")]
  public class SetAnimatorBool : FsmStateActionAnimatorBase
  {
    [Tooltip("The target")]
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.AnimatorBool)]
    [RequiredField]
    [Tooltip("The animator parameter")]
    public FsmString parameter;
    [Tooltip("The Bool value to assign to the animator parameter")]
    public FsmBool Value;
    private Animator _animator;
    private int _paramID;

    public override void Reset()
    {
      base.Reset();
      this.gameObject = (FsmOwnerDefault) null;
      this.parameter = (FsmString) null;
      this.Value = (FsmBool) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this._paramID = Animator.StringToHash(this.parameter.Value);
          this.SetParameter();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnActionUpdate()
    {
      this.SetParameter();
    }

    private void SetParameter()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      this._animator.SetBool(this._paramID, this.Value.Value);
    }
  }
}
