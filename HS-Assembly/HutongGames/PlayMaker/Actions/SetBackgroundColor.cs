﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetBackgroundColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the Background Color used by the Camera.")]
  [ActionCategory(ActionCategory.Camera)]
  public class SetBackgroundColor : ComponentAction<Camera>
  {
    [CheckForComponent(typeof (Camera))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    public FsmColor backgroundColor;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.backgroundColor = (FsmColor) Color.black;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetBackgroundColor();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetBackgroundColor();
    }

    private void DoSetBackgroundColor()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.camera.backgroundColor = this.backgroundColor.Value;
    }
  }
}
