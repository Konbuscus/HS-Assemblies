﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DebugGameObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Logs the value of a Game Object Variable in the PlayMaker Log Window.")]
  [ActionCategory(ActionCategory.Debug)]
  public class DebugGameObject : BaseLogAction
  {
    [Tooltip("Info, Warning, or Error.")]
    public LogLevel logLevel;
    [Tooltip("The GameObject variable to debug.")]
    [UIHint(UIHint.Variable)]
    public FsmGameObject gameObject;

    public override void Reset()
    {
      this.logLevel = LogLevel.Info;
      this.gameObject = (FsmGameObject) null;
      base.Reset();
    }

    public override void OnEnter()
    {
      string text = "None";
      if (!this.gameObject.IsNone)
        text = this.gameObject.Name + ": " + (object) this.gameObject;
      ActionHelpers.DebugLog(this.Fsm, this.logLevel, text, this.sendToUnityLog);
      this.Finish();
    }
  }
}
