﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.particleEmitterOnOff
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Turns a Particle Emitter on and off with optional delay.")]
  public class particleEmitterOnOff : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Set to True to turn it on and False to turn it off.")]
    public FsmBool emitOnOff;
    [Tooltip("If 0 it just acts like a switch. Values cause it to Toggle value after delay time (sec).")]
    public FsmFloat delay;
    public FsmEvent finishEvent;
    public bool realTime;
    private float startTime;
    private float timer;
    private GameObject go;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.emitOnOff = (FsmBool) false;
      this.delay = (FsmFloat) 0.0f;
      this.finishEvent = (FsmEvent) null;
      this.realTime = false;
      this.go = (GameObject) null;
    }

    public override void OnEnter()
    {
      this.go = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      this.go.GetComponent<ParticleEmitter>().emit = this.emitOnOff.Value;
      if ((double) this.delay.Value <= 0.0)
      {
        this.Finish();
      }
      else
      {
        this.startTime = Time.realtimeSinceStartup;
        this.timer = 0.0f;
      }
    }

    public override void OnUpdate()
    {
      if (this.realTime)
        this.timer = Time.realtimeSinceStartup - this.startTime;
      else
        this.timer += Time.deltaTime;
      if ((double) this.timer <= (double) this.delay.Value)
        return;
      this.go.GetComponent<ParticleEmitter>().emit = !this.emitOnOff.Value;
      this.Finish();
      if (this.finishEvent == null)
        return;
      this.Fsm.Event(this.finishEvent);
    }
  }
}
