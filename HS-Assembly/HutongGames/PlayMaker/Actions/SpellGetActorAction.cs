﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellGetActorAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Put a Spell's Source or Target Actor into a GameObject variable.")]
  [ActionCategory("Pegasus")]
  public class SpellGetActorAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    public SpellAction.Which m_WhichActor;
    public FsmGameObject m_GameObject;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      this.m_SpellObject = (FsmOwnerDefault) null;
      this.m_WhichActor = SpellAction.Which.SOURCE;
      this.m_GameObject = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      Actor actor = this.GetActor(this.m_WhichActor);
      if ((Object) actor == (Object) null)
      {
        Error.AddDevFatal("SpellGetActorAction.OnEnter() - Actor not found!");
        this.Finish();
      }
      else
      {
        if (!this.m_GameObject.IsNone)
          this.m_GameObject.Value = actor.gameObject;
        this.Finish();
      }
    }
  }
}
