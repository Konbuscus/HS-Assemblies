﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.WakeAllRigidBodies
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Rigid bodies start sleeping when they come to rest. This action wakes up all rigid bodies in the scene. E.g., if you Set Gravity and want objects at rest to respond.")]
  [ActionCategory(ActionCategory.Physics)]
  public class WakeAllRigidBodies : FsmStateAction
  {
    public bool everyFrame;
    private Rigidbody[] bodies;

    public override void Reset()
    {
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.bodies = Object.FindObjectsOfType(typeof (Rigidbody)) as Rigidbody[];
      this.DoWakeAll();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoWakeAll();
    }

    private void DoWakeAll()
    {
      this.bodies = Object.FindObjectsOfType(typeof (Rigidbody)) as Rigidbody[];
      if (this.bodies == null)
        return;
      foreach (Rigidbody body in this.bodies)
        body.WakeUp();
    }
  }
}
