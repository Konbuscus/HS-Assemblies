﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.MainCameraShakeStopAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("[DEPRECATED - Use CameraShakeStopAction instead.] Stops shaking the Main Camera over time.")]
  public class MainCameraShakeStopAction : FsmStateAction
  {
    public FsmFloat m_Time;
    public FsmFloat m_Delay;
    public FsmEvent m_FinishedEvent;
    private float m_timerSec;
    private bool m_shakeStopped;

    public override void Reset()
    {
      this.m_Time = (FsmFloat) 0.5f;
      this.m_Delay = (FsmFloat) 0.0f;
      this.m_FinishedEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      this.m_timerSec = 0.0f;
      this.m_shakeStopped = false;
    }

    public override void OnUpdate()
    {
      this.m_timerSec += Time.deltaTime;
      if ((double) this.m_timerSec >= (double) this.m_Delay.Value && !this.m_shakeStopped)
        this.StopShake();
      if ((double) this.m_timerSec < (double) this.m_Delay.Value + (double) this.m_Time.Value)
        return;
      this.Fsm.Event(this.m_FinishedEvent);
      this.Finish();
    }

    private void StopShake()
    {
      CameraShakeMgr.Stop(Camera.main, this.m_Time.Value);
      this.m_shakeStopped = true;
    }
  }
}
