﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ParticleStopAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Stop a Particle System.")]
  [ActionCategory("Pegasus")]
  public class ParticleStopAction : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault m_GameObject;
    [Tooltip("Run this action on all child objects' Particle Systems.")]
    public FsmBool m_IncludeChildren;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_IncludeChildren = (FsmBool) false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        ParticleSystem component = ownerDefaultTarget.GetComponent<ParticleSystem>();
        if ((Object) component == (Object) null)
        {
          Debug.LogWarning((object) string.Format("ParticleStopAction.OnEnter() - GameObject {0} has no ParticleSystem component", (object) ownerDefaultTarget));
          this.Finish();
        }
        else
        {
          component.Stop(this.m_IncludeChildren.Value);
          this.Finish();
        }
      }
    }
  }
}
