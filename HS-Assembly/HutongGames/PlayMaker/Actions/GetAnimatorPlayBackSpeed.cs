﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Gets the playback speed of the Animator. 1 is normal playback speed")]
  public class GetAnimatorPlayBackSpeed : FsmStateAction
  {
    [Tooltip("The Target. An Animator component is required")]
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The playBack speed of the animator. 1 is normal playback speed")]
    public FsmFloat playBackSpeed;
    [Tooltip("Repeat every frame. Useful when value is subject to change over time.")]
    public bool everyFrame;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.playBackSpeed = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.GetPlayBackSpeed();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnUpdate()
    {
      this.GetPlayBackSpeed();
    }

    private void GetPlayBackSpeed()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      this.playBackSpeed.Value = this._animator.speed;
    }
  }
}
