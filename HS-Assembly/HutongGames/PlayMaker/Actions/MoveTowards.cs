﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.MoveTowards
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Moves a Game Object towards a Target. Optionally sends an event when successful. The Target can be specified as a Game Object or a world Position. If you specify both, then the Position is used as a local offset from the Object's Position.")]
  [ActionCategory(ActionCategory.Transform)]
  public class MoveTowards : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject to Move")]
    public FsmOwnerDefault gameObject;
    [Tooltip("A target GameObject to move towards. Or use a world Target Position below.")]
    public FsmGameObject targetObject;
    [Tooltip("A world position if no Target Object. Otherwise used as a local offset from the Target Object.")]
    public FsmVector3 targetPosition;
    [Tooltip("Ignore any height difference in the target.")]
    public FsmBool ignoreVertical;
    [HasFloatSlider(0.0f, 20f)]
    [Tooltip("The maximum movement speed. HINT: You can make this a variable to change it over time.")]
    public FsmFloat maxSpeed;
    [HasFloatSlider(0.0f, 5f)]
    [Tooltip("Distance at which the move is considered finished, and the Finish Event is sent.")]
    public FsmFloat finishDistance;
    [Tooltip("Event to send when the Finish Distance is reached.")]
    public FsmEvent finishEvent;
    private GameObject go;
    private GameObject goTarget;
    private Vector3 targetPos;
    private Vector3 targetPosWithVertical;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.targetObject = (FsmGameObject) null;
      this.maxSpeed = (FsmFloat) 10f;
      this.finishDistance = (FsmFloat) 1f;
      this.finishEvent = (FsmEvent) null;
    }

    public override void OnUpdate()
    {
      this.DoMoveTowards();
    }

    private void DoMoveTowards()
    {
      if (!this.UpdateTargetPos())
        return;
      this.go.transform.position = Vector3.MoveTowards(this.go.transform.position, this.targetPos, this.maxSpeed.Value * Time.deltaTime);
      if ((double) (this.go.transform.position - this.targetPos).magnitude >= (double) this.finishDistance.Value)
        return;
      this.Fsm.Event(this.finishEvent);
      this.Finish();
    }

    public bool UpdateTargetPos()
    {
      this.go = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) this.go == (Object) null)
        return false;
      this.goTarget = this.targetObject.Value;
      if ((Object) this.goTarget == (Object) null && this.targetPosition.IsNone)
        return false;
      this.targetPos = !((Object) this.goTarget != (Object) null) ? this.targetPosition.Value : (this.targetPosition.IsNone ? this.goTarget.transform.position : this.goTarget.transform.TransformPoint(this.targetPosition.Value));
      this.targetPosWithVertical = this.targetPos;
      if (this.ignoreVertical.Value)
        this.targetPos.y = this.go.transform.position.y;
      return true;
    }

    public Vector3 GetTargetPos()
    {
      return this.targetPos;
    }

    public Vector3 GetTargetPosWithVertical()
    {
      return this.targetPosWithVertical;
    }
  }
}
