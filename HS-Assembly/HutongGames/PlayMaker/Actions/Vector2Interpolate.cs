﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector2Interpolate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Vector2)]
  [Tooltip("Interpolates between 2 Vector2 values over a specified Time.")]
  public class Vector2Interpolate : FsmStateAction
  {
    [Tooltip("The interpolation type")]
    public InterpolationType mode;
    [Tooltip("The vector to interpolate from")]
    [RequiredField]
    public FsmVector2 fromVector;
    [RequiredField]
    [Tooltip("The vector to interpolate to")]
    public FsmVector2 toVector;
    [RequiredField]
    [Tooltip("the interpolate time")]
    public FsmFloat time;
    [UIHint(UIHint.Variable)]
    [Tooltip("the interpolated result")]
    [RequiredField]
    public FsmVector2 storeResult;
    [Tooltip("This event is fired when the interpolation is done.")]
    public FsmEvent finishEvent;
    [Tooltip("Ignore TimeScale")]
    public bool realTime;
    private float startTime;
    private float currentTime;

    public override void Reset()
    {
      this.mode = InterpolationType.Linear;
      FsmVector2 fsmVector2_1 = new FsmVector2();
      fsmVector2_1.UseVariable = true;
      this.fromVector = fsmVector2_1;
      FsmVector2 fsmVector2_2 = new FsmVector2();
      fsmVector2_2.UseVariable = true;
      this.toVector = fsmVector2_2;
      this.time = (FsmFloat) 1f;
      this.storeResult = (FsmVector2) null;
      this.finishEvent = (FsmEvent) null;
      this.realTime = false;
    }

    public override void OnEnter()
    {
      this.startTime = FsmTime.RealtimeSinceStartup;
      this.currentTime = 0.0f;
      if (this.storeResult == null)
        this.Finish();
      else
        this.storeResult.Value = this.fromVector.Value;
    }

    public override void OnUpdate()
    {
      if (this.realTime)
        this.currentTime = FsmTime.RealtimeSinceStartup - this.startTime;
      else
        this.currentTime += Time.deltaTime;
      float t = this.currentTime / this.time.Value;
      switch (this.mode)
      {
        case InterpolationType.EaseInOut:
          t = Mathf.SmoothStep(0.0f, 1f, t);
          break;
      }
      this.storeResult.Value = Vector2.Lerp(this.fromVector.Value, this.toVector.Value, t);
      if ((double) t <= 1.0)
        return;
      if (this.finishEvent != null)
        this.Fsm.Event(this.finishEvent);
      this.Finish();
    }
  }
}
