﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.IsKinematic
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Tests if a Game Object's Rigid Body is Kinematic.")]
  public class IsKinematic : ComponentAction<Rigidbody>
  {
    [CheckForComponent(typeof (Rigidbody))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    public FsmEvent trueEvent;
    public FsmEvent falseEvent;
    [UIHint(UIHint.Variable)]
    public FsmBool store;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.store = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoIsKinematic();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoIsKinematic();
    }

    private void DoIsKinematic()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      bool isKinematic = this.rigidbody.isKinematic;
      this.store.Value = isKinematic;
      this.Fsm.Event(!isKinematic ? this.falseEvent : this.trueEvent);
    }
  }
}
