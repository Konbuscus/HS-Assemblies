﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActorSetVisibilityRecursiveAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the visibility on a game object and its children. Will properly Show/Hide Actors in the hierarchy.")]
  [ActionCategory("Pegasus")]
  public class ActorSetVisibilityRecursiveAction : FsmStateAction
  {
    private Map<GameObject, bool> m_initialVisibility = new Map<GameObject, bool>();
    public FsmOwnerDefault m_GameObject;
    [Tooltip("Should objects be set to visible or invisible?")]
    public FsmBool m_Visible;
    [Tooltip("Don't touch the Actor's SpellTable when setting visibility")]
    public FsmBool m_IgnoreSpells;
    [Tooltip("Resets to the initial visibility once it leaves the state")]
    public bool m_ResetOnExit;
    [Tooltip("Should children of the Game Object be affected?")]
    public bool m_IncludeChildren;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_Visible = (FsmBool) false;
      this.m_IgnoreSpells = (FsmBool) false;
      this.m_ResetOnExit = false;
      this.m_IncludeChildren = true;
      this.m_initialVisibility.Clear();
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((UnityEngine.Object) ownerDefaultTarget != (UnityEngine.Object) null)
      {
        if (this.m_ResetOnExit)
          this.SaveInitialVisibility(ownerDefaultTarget);
        this.SetVisibility(ownerDefaultTarget);
      }
      this.Finish();
    }

    public override void OnExit()
    {
      if (!this.m_ResetOnExit)
        return;
      this.RestoreInitialVisibility();
    }

    private void SaveInitialVisibility(GameObject go)
    {
      Actor component1 = go.GetComponent<Actor>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
      {
        this.m_initialVisibility[go] = component1.IsShown();
      }
      else
      {
        Renderer component2 = go.GetComponent<Renderer>();
        if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
          this.m_initialVisibility[go] = component2.enabled;
        if (!this.m_IncludeChildren)
          return;
        IEnumerator enumerator = go.transform.GetEnumerator();
        try
        {
          while (enumerator.MoveNext())
            this.SaveInitialVisibility(((Component) enumerator.Current).gameObject);
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            disposable.Dispose();
        }
      }
    }

    private void RestoreInitialVisibility()
    {
      using (Map<GameObject, bool>.Enumerator enumerator = this.m_initialVisibility.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<GameObject, bool> current = enumerator.Current;
          GameObject key = current.Key;
          bool flag = current.Value;
          Actor component = key.GetComponent<Actor>();
          if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          {
            if (flag)
              this.ShowActor(component);
            else
              this.HideActor(component);
          }
          else
            key.GetComponent<Renderer>().enabled = flag;
        }
      }
    }

    private void SetVisibility(GameObject go)
    {
      Actor component1 = go.GetComponent<Actor>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
      {
        if (this.m_Visible.Value)
          this.ShowActor(component1);
        else
          this.HideActor(component1);
      }
      else
      {
        Renderer component2 = go.GetComponent<Renderer>();
        if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
          component2.enabled = this.m_Visible.Value;
        if (!this.m_IncludeChildren)
          return;
        IEnumerator enumerator = go.transform.GetEnumerator();
        try
        {
          while (enumerator.MoveNext())
            this.SetVisibility(((Component) enumerator.Current).gameObject);
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            disposable.Dispose();
        }
      }
    }

    private void ShowActor(Actor actor)
    {
      actor.Show(this.m_IgnoreSpells.Value);
    }

    private void HideActor(Actor actor)
    {
      actor.Hide(this.m_IgnoreSpells.Value);
    }
  }
}
