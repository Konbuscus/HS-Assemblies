﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.RandomBool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets a Bool Variable to True or False randomly.")]
  [ActionCategory(ActionCategory.Math)]
  public class RandomBool : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;

    public override void Reset()
    {
      this.storeResult = (FsmBool) null;
    }

    public override void OnEnter()
    {
      this.storeResult.Value = Random.Range(0, 100) < 50;
      this.Finish();
    }
  }
}
