﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayCompare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if 2 Array Variables have the same values.")]
  [ActionCategory(ActionCategory.Logic)]
  public class ArrayCompare : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The first Array Variable to test.")]
    [UIHint(UIHint.Variable)]
    public FsmArray array1;
    [RequiredField]
    [Tooltip("The second Array Variable to test.")]
    [UIHint(UIHint.Variable)]
    public FsmArray array2;
    [Tooltip("Event to send if the 2 arrays have the same values.")]
    public FsmEvent SequenceEqual;
    [Tooltip("Event to send if the 2 arrays have different values.")]
    public FsmEvent SequenceNotEqual;
    [Tooltip("Store the result in a Bool variable.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.array1 = (FsmArray) null;
      this.array2 = (FsmArray) null;
      this.SequenceEqual = (FsmEvent) null;
      this.SequenceNotEqual = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      this.DoSequenceEqual();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    private void DoSequenceEqual()
    {
      if (this.array1.Values == null || this.array2.Values == null)
        return;
      this.storeResult.Value = ((IEnumerable<object>) this.array1.Values).SequenceEqual<object>((IEnumerable<object>) this.array2.Values);
      this.Fsm.Event(!this.storeResult.Value ? this.SequenceNotEqual : this.SequenceEqual);
    }
  }
}
