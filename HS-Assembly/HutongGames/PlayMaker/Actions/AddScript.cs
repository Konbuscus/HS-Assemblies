﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AddScript
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Adds a Script to a Game Object. Use this to change the behaviour of objects on the fly. Optionally remove the Script on exiting the state.")]
  [ActionCategory(ActionCategory.ScriptControl)]
  public class AddScript : FsmStateAction
  {
    [Tooltip("The GameObject to add the script to.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [UIHint(UIHint.ScriptComponent)]
    [Tooltip("The Script to add to the GameObject.")]
    public FsmString script;
    [Tooltip("Remove the script from the GameObject when this State is exited.")]
    public FsmBool removeOnExit;
    private Component addedComponent;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.script = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.DoAddComponent(this.gameObject.OwnerOption != OwnerDefaultOption.UseOwner ? this.gameObject.GameObject.Value : this.Owner);
      this.Finish();
    }

    public override void OnExit()
    {
      if (!this.removeOnExit.Value || !((Object) this.addedComponent != (Object) null))
        return;
      Object.Destroy((Object) this.addedComponent);
    }

    private void DoAddComponent(GameObject go)
    {
      this.addedComponent = go.AddComponent(ReflectionUtils.GetGlobalType(this.script.Value));
      if (!((Object) this.addedComponent == (Object) null))
        return;
      this.LogError("Can't add script: " + this.script.Value);
    }
  }
}
