﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGravity2dScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics2D)]
  [Tooltip("Sets The degree to which this object is affected by gravity.  NOTE: Game object must have a rigidbody 2D.")]
  public class SetGravity2dScale : ComponentAction<Rigidbody2D>
  {
    [CheckForComponent(typeof (Rigidbody2D))]
    [RequiredField]
    [Tooltip("The GameObject with a Rigidbody 2d attached")]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The gravity scale effect")]
    public FsmFloat gravityScale;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.gravityScale = (FsmFloat) 1f;
    }

    public override void OnEnter()
    {
      this.DoSetGravityScale();
      this.Finish();
    }

    private void DoSetGravityScale()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.rigidbody2d.gravityScale = this.gravityScale.Value;
    }
  }
}
