﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorCullingMode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Returns the culling of this Animator component. Optionnaly sends events.\nIf true ('AlwaysAnimate'): always animate the entire character. Object is animated even when offscreen.\nIf False ('BasedOnRenderers') animation is disabled when renderers are not visible.")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorCullingMode : FsmStateAction
  {
    [Tooltip("The Target. An Animator component is required")]
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.Variable)]
    [Tooltip("If true, always animate the entire character, else animation is disabled when renderers are not visible")]
    [ActionSection("Results")]
    [RequiredField]
    public FsmBool alwaysAnimate;
    [Tooltip("Event send if culling mode is 'AlwaysAnimate'")]
    public FsmEvent alwaysAnimateEvent;
    [Tooltip("Event send if culling mode is 'BasedOnRenders'")]
    public FsmEvent basedOnRenderersEvent;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.alwaysAnimate = (FsmBool) null;
      this.alwaysAnimateEvent = (FsmEvent) null;
      this.basedOnRenderersEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoCheckCulling();
          this.Finish();
        }
      }
    }

    private void DoCheckCulling()
    {
      if ((Object) this._animator == (Object) null)
        return;
      bool flag = this._animator.cullingMode == AnimatorCullingMode.AlwaysAnimate;
      this.alwaysAnimate.Value = flag;
      if (flag)
        this.Fsm.Event(this.alwaysAnimateEvent);
      else
        this.Fsm.Event(this.basedOnRenderersEvent);
    }
  }
}
