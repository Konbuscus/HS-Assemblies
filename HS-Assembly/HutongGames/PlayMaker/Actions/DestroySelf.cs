﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DestroySelf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Destroys the Owner of the Fsm! Useful for spawned Prefabs that need to kill themselves, e.g., a projectile that explodes on impact.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class DestroySelf : FsmStateAction
  {
    [Tooltip("Detach children before destroying the Owner.")]
    public FsmBool detachChildren;

    public override void Reset()
    {
      this.detachChildren = (FsmBool) false;
    }

    public override void OnEnter()
    {
      if ((Object) this.Owner != (Object) null)
      {
        if (this.detachChildren.Value)
          this.Owner.transform.DetachChildren();
        Object.Destroy((Object) this.Owner);
      }
      this.Finish();
    }
  }
}
