﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.QuaternionEuler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Returns a rotation that rotates z degrees around the z axis, x degrees around the x axis, and y degrees around the y axis (in that order).")]
  [ActionCategory(ActionCategory.Quaternion)]
  public class QuaternionEuler : QuaternionBaseAction
  {
    [Tooltip("The Euler angles.")]
    [RequiredField]
    public FsmVector3 eulerAngles;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the euler angles of this quaternion variable.")]
    [RequiredField]
    public FsmQuaternion result;

    public override void Reset()
    {
      this.eulerAngles = (FsmVector3) null;
      this.result = (FsmQuaternion) null;
      this.everyFrame = true;
      this.everyFrameOption = QuaternionBaseAction.everyFrameOptions.Update;
    }

    public override void OnEnter()
    {
      this.DoQuatEuler();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.Update)
        return;
      this.DoQuatEuler();
    }

    public override void OnLateUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.LateUpdate)
        return;
      this.DoQuatEuler();
    }

    public override void OnFixedUpdate()
    {
      if (this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.FixedUpdate)
        return;
      this.DoQuatEuler();
    }

    private void DoQuatEuler()
    {
      this.result.Value = Quaternion.Euler(this.eulerAngles.Value);
    }
  }
}
