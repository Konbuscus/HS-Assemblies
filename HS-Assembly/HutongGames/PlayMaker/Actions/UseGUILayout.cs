﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.UseGUILayout
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Turn GUILayout on/off. If you don't use GUILayout actions you can get some performace back by turning GUILayout off. This can make a difference on iOS platforms.")]
  [ActionCategory(ActionCategory.GUILayout)]
  public class UseGUILayout : FsmStateAction
  {
    [RequiredField]
    public bool turnOffGUIlayout;

    public override void Reset()
    {
      this.turnOffGUIlayout = true;
    }

    public override void OnEnter()
    {
      this.Fsm.Owner.useGUILayout = !this.turnOffGUIlayout;
      this.Finish();
    }
  }
}
