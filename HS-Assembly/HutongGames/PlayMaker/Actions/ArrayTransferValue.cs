﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayTransferValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
  [NoActionTargets]
  [ActionCategory(ActionCategory.Array)]
  [Tooltip("Transfer a value from one array to another, basically a copy/cut paste action on steroids.")]
  public class ArrayTransferValue : FsmStateAction
  {
    [Tooltip("The Array Variable source.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmArray arraySource;
    [RequiredField]
    [Tooltip("The Array Variable target.")]
    [UIHint(UIHint.Variable)]
    public FsmArray arrayTarget;
    [MatchFieldType("array")]
    [Tooltip("The index to transfer.")]
    public FsmInt indexToTransfer;
    [ActionSection("Transfer Options")]
    [ObjectType(typeof (ArrayTransferValue.ArrayTransferType))]
    public FsmEnum copyType;
    [ObjectType(typeof (ArrayTransferValue.ArrayPasteType))]
    public FsmEnum pasteType;
    [ActionSection("Result")]
    [Tooltip("Event sent if this array source does not contains that element (described below)")]
    public FsmEvent indexOutOfRange;

    public override void Reset()
    {
      this.arraySource = (FsmArray) null;
      this.arrayTarget = (FsmArray) null;
      this.indexToTransfer = (FsmInt) null;
      this.copyType = FsmEnum.op_Implicit((Enum) ArrayTransferValue.ArrayTransferType.Copy);
      this.pasteType = FsmEnum.op_Implicit((Enum) ArrayTransferValue.ArrayPasteType.AsLastItem);
    }

    public override void OnEnter()
    {
      this.DoTransferValue();
      this.Finish();
    }

    private void DoTransferValue()
    {
      if (this.arraySource.IsNone || this.arrayTarget.IsNone)
        return;
      int index = this.indexToTransfer.Value;
      if (index < 0 || index >= this.arraySource.Length)
      {
        this.Fsm.Event(this.indexOutOfRange);
      }
      else
      {
        object obj = this.arraySource.Values[index];
        if ((ArrayTransferValue.ArrayTransferType) this.copyType.get_Value() == ArrayTransferValue.ArrayTransferType.Cut)
        {
          List<object> objectList = new List<object>((IEnumerable<object>) this.arraySource.Values);
          objectList.RemoveAt(index);
          this.arraySource.Values = objectList.ToArray();
        }
        else if ((ArrayTransferValue.ArrayTransferType) this.copyType.get_Value() == ArrayTransferValue.ArrayTransferType.nullify)
          this.arraySource.Values.SetValue((object) null, index);
        if ((ArrayTransferValue.ArrayPasteType) this.pasteType.get_Value() == ArrayTransferValue.ArrayPasteType.AsFirstItem)
        {
          List<object> objectList = new List<object>((IEnumerable<object>) this.arrayTarget.Values);
          objectList.Insert(0, obj);
          this.arrayTarget.Values = objectList.ToArray();
        }
        else if ((ArrayTransferValue.ArrayPasteType) this.pasteType.get_Value() == ArrayTransferValue.ArrayPasteType.AsLastItem)
        {
          this.arrayTarget.Resize(this.arrayTarget.Length + 1);
          this.arrayTarget.Set(this.arrayTarget.Length - 1, obj);
        }
        else if ((ArrayTransferValue.ArrayPasteType) this.pasteType.get_Value() == ArrayTransferValue.ArrayPasteType.InsertAtSameIndex)
        {
          if (index >= this.arrayTarget.Length)
            this.Fsm.Event(this.indexOutOfRange);
          List<object> objectList = new List<object>((IEnumerable<object>) this.arrayTarget.Values);
          objectList.Insert(index, obj);
          this.arrayTarget.Values = objectList.ToArray();
        }
        else
        {
          if ((ArrayTransferValue.ArrayPasteType) this.pasteType.get_Value() != ArrayTransferValue.ArrayPasteType.ReplaceAtSameIndex)
            return;
          if (index >= this.arrayTarget.Length)
            this.Fsm.Event(this.indexOutOfRange);
          else
            this.arrayTarget.Set(index, obj);
        }
      }
    }

    public enum ArrayTransferType
    {
      Copy,
      Cut,
      nullify,
    }

    public enum ArrayPasteType
    {
      AsFirstItem,
      AsLastItem,
      InsertAtSameIndex,
      ReplaceAtSameIndex,
    }
  }
}
