﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutEmailField
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUILayout)]
  [Tooltip("GUILayout Password Field. Optionally send an event if the text has been edited.")]
  public class GUILayoutEmailField : GUILayoutAction
  {
    [Tooltip("The email Text")]
    [UIHint(UIHint.Variable)]
    public FsmString text;
    [Tooltip("The Maximum Length of the field")]
    public FsmInt maxLength;
    [Tooltip("The Style of the Field")]
    public FsmString style;
    [Tooltip("Event sent when field content changed")]
    public FsmEvent changedEvent;
    [Tooltip("Email valid format flag")]
    public FsmBool valid;

    public override void Reset()
    {
      this.text = (FsmString) null;
      this.maxLength = (FsmInt) 25;
      this.style = (FsmString) "TextField";
      this.valid = (FsmBool) true;
      this.changedEvent = (FsmEvent) null;
    }

    public override void OnGUI()
    {
      bool changed = GUI.changed;
      GUI.changed = false;
      this.text.Value = GUILayout.TextField(this.text.Value, (GUIStyle) this.style.Value, this.LayoutOptions);
      if (GUI.changed)
      {
        this.Fsm.Event(this.changedEvent);
        GUIUtility.ExitGUI();
      }
      else
        GUI.changed = changed;
    }
  }
}
