﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Captures the current pose of a hierarchy as an animation clip.\n\nUseful to blend from an arbitrary pose (e.g. a ragdoll death) back to a known animation (e.g. idle).")]
  [ActionCategory(ActionCategory.Animation)]
  public class CapturePoseAsAnimationClip : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (Animation))]
    [Tooltip("The GameObject root of the hierarchy to capture.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("Capture position keys.")]
    public FsmBool position;
    [Tooltip("Capture rotation keys.")]
    public FsmBool rotation;
    [Tooltip("Capture scale keys.")]
    public FsmBool scale;
    [Tooltip("Store the result in an Object variable of type AnimationClip.")]
    [RequiredField]
    [ObjectType(typeof (AnimationClip))]
    [UIHint(UIHint.Variable)]
    public FsmObject storeAnimationClip;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.position = (FsmBool) false;
      this.rotation = (FsmBool) true;
      this.scale = (FsmBool) false;
      this.storeAnimationClip = (FsmObject) null;
    }

    public override void OnEnter()
    {
      this.DoCaptureAnimationClip();
      this.Finish();
    }

    private void DoCaptureAnimationClip()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((UnityEngine.Object) ownerDefaultTarget == (UnityEngine.Object) null)
        return;
      AnimationClip clip = new AnimationClip();
      IEnumerator enumerator = ownerDefaultTarget.transform.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
          this.CaptureTransform((Transform) enumerator.Current, string.Empty, clip);
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
      this.storeAnimationClip.Value = (UnityEngine.Object) clip;
    }

    private void CaptureTransform(Transform transform, string path, AnimationClip clip)
    {
      path += transform.name;
      if (this.position.Value)
        this.CapturePosition(transform, path, clip);
      if (this.rotation.Value)
        this.CaptureRotation(transform, path, clip);
      if (this.scale.Value)
        this.CaptureScale(transform, path, clip);
      IEnumerator enumerator = transform.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
          this.CaptureTransform((Transform) enumerator.Current, path + "/", clip);
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
    }

    private void CapturePosition(Transform transform, string path, AnimationClip clip)
    {
      this.SetConstantCurve(clip, path, "localPosition.x", transform.localPosition.x);
      this.SetConstantCurve(clip, path, "localPosition.y", transform.localPosition.y);
      this.SetConstantCurve(clip, path, "localPosition.z", transform.localPosition.z);
    }

    private void CaptureRotation(Transform transform, string path, AnimationClip clip)
    {
      this.SetConstantCurve(clip, path, "localRotation.x", transform.localRotation.x);
      this.SetConstantCurve(clip, path, "localRotation.y", transform.localRotation.y);
      this.SetConstantCurve(clip, path, "localRotation.z", transform.localRotation.z);
      this.SetConstantCurve(clip, path, "localRotation.w", transform.localRotation.w);
    }

    private void CaptureScale(Transform transform, string path, AnimationClip clip)
    {
      this.SetConstantCurve(clip, path, "localScale.x", transform.localScale.x);
      this.SetConstantCurve(clip, path, "localScale.y", transform.localScale.y);
      this.SetConstantCurve(clip, path, "localScale.z", transform.localScale.z);
    }

    private void SetConstantCurve(AnimationClip clip, string childPath, string propertyPath, float value)
    {
      AnimationCurve animationCurve = AnimationCurve.Linear(0.0f, value, 100f, value);
      animationCurve.postWrapMode = WrapMode.Loop;
      clip.SetCurve(childPath, typeof (Transform), propertyPath, animationCurve);
    }
  }
}
