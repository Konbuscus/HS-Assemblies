﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetMaterial
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Material)]
  [Tooltip("Sets the material on a game object.")]
  public class SetMaterial : ComponentAction<Renderer>
  {
    [RequiredField]
    [CheckForComponent(typeof (Renderer))]
    public FsmOwnerDefault gameObject;
    public FsmInt materialIndex;
    [RequiredField]
    public FsmMaterial material;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.material = (FsmMaterial) null;
      this.materialIndex = (FsmInt) 0;
    }

    public override void OnEnter()
    {
      this.DoSetMaterial();
      this.Finish();
    }

    private void DoSetMaterial()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      if (this.materialIndex.Value == 0)
      {
        this.renderer.material = this.material.Value;
      }
      else
      {
        if (this.renderer.materials.Length <= this.materialIndex.Value)
          return;
        Material[] materials = this.renderer.materials;
        materials[this.materialIndex.Value] = this.material.Value;
        this.renderer.materials = materials;
      }
    }
  }
}
