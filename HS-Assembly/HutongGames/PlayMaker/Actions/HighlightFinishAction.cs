﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.HighlightFinishAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tells the Highlight system when the state is finished.")]
  [ActionCategory("Pegasus")]
  public class HighlightFinishAction : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault m_GameObject;
    protected HighlightState m_HighlightState;

    public HighlightState CacheHighlightState()
    {
      if ((Object) this.m_HighlightState == (Object) null)
      {
        GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
        if ((Object) ownerDefaultTarget != (Object) null)
          this.m_HighlightState = SceneUtils.FindComponentInThisOrParents<HighlightState>(ownerDefaultTarget);
      }
      return this.m_HighlightState;
    }

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      this.CacheHighlightState();
      if ((Object) this.m_HighlightState == (Object) null)
      {
        Debug.LogError((object) string.Format("{0}.OnEnter() - FAILED to find {1} in hierarchy", (object) this, (object) typeof (HighlightState)));
        this.Finish();
      }
      else
      {
        this.m_HighlightState.OnActionFinished();
        this.Finish();
      }
    }
  }
}
