﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get the left foot bottom height.")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorLeftFootBottomHeight : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The Target. An Animator component is required")]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("the left foot bottom height.")]
    [UIHint(UIHint.Variable)]
    [ActionSection("Result")]
    public FsmFloat leftFootHeight;
    [Tooltip("Repeat every frame. Useful when value is subject to change over time.")]
    public bool everyFrame;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.leftFootHeight = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this._getLeftFootBottonHeight();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnLateUpdate()
    {
      this._getLeftFootBottonHeight();
    }

    private void _getLeftFootBottonHeight()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      this.leftFootHeight.Value = this._animator.leftFeetBottomHeight;
    }
  }
}
