﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetASine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get the Arc sine. You can get the result in degrees, simply check on the RadToDeg conversion")]
  [ActionCategory(ActionCategory.Trigonometry)]
  public class GetASine : FsmStateAction
  {
    [Tooltip("The value of the sine")]
    [RequiredField]
    public FsmFloat Value;
    [UIHint(UIHint.Variable)]
    [Tooltip("The resulting angle. Note:If you want degrees, simply check RadToDeg")]
    [RequiredField]
    public FsmFloat angle;
    [Tooltip("Check on if you want the angle expressed in degrees.")]
    public FsmBool RadToDeg;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.angle = (FsmFloat) null;
      this.RadToDeg = (FsmBool) true;
      this.everyFrame = false;
      this.Value = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      this.DoASine();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoASine();
    }

    private void DoASine()
    {
      float num = Mathf.Asin(this.Value.Value);
      if (this.RadToDeg.Value)
        num *= 57.29578f;
      this.angle.Value = num;
    }
  }
}
