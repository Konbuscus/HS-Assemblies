﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGUIAlpha
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUI)]
  [Tooltip("Sets the global Alpha for the GUI. Useful for fading GUI up/down. By default only effects GUI rendered by this FSM, check Apply Globally to effect all GUI controls.")]
  public class SetGUIAlpha : FsmStateAction
  {
    [RequiredField]
    public FsmFloat alpha;
    public FsmBool applyGlobally;

    public override void Reset()
    {
      this.alpha = (FsmFloat) 1f;
    }

    public override void OnGUI()
    {
      GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, this.alpha.Value);
      if (!this.applyGlobally.Value)
        return;
      PlayMakerGUI.GUIColor = GUI.color;
    }
  }
}
