﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioLoadAndPlayAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Loads and Plays a Sound Prefab.")]
  [ActionCategory("Pegasus Audio")]
  public class AudioLoadAndPlayAction : FsmStateAction
  {
    [Tooltip("Optional. If specified, the generated Audio Source will be attached to this object.")]
    public FsmOwnerDefault m_ParentObject;
    [RequiredField]
    public FsmString m_PrefabName;
    [HasFloatSlider(0.0f, 1f)]
    [Tooltip("Optional. Scales the volume of the loaded sound.")]
    public FsmFloat m_VolumeScale;

    public override void Reset()
    {
      this.m_ParentObject = (FsmOwnerDefault) null;
      this.m_PrefabName = (FsmString) null;
      FsmFloat fsmFloat = new FsmFloat();
      fsmFloat.UseVariable = true;
      this.m_VolumeScale = fsmFloat;
    }

    public override void OnEnter()
    {
      if (this.m_PrefabName == null)
      {
        this.Finish();
      }
      else
      {
        GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_ParentObject);
        string soundName = this.m_PrefabName.Value;
        if (this.m_VolumeScale.IsNone)
          SoundManager.Get().LoadAndPlay(soundName, ownerDefaultTarget);
        else
          SoundManager.Get().LoadAndPlay(soundName, ownerDefaultTarget, this.m_VolumeScale.Value);
        this.Finish();
      }
    }
  }
}
