﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellInitActorVariables
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Initialize a spell state, setting variables that reference the parent actor and its contents.")]
  public class SpellInitActorVariables : FsmStateAction
  {
    public FsmGameObject m_actorObject;
    public FsmGameObject m_rootObject;
    public FsmGameObject m_rootObjectMesh;

    public override void Reset()
    {
    }

    public override void OnEnter()
    {
      Actor componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<Actor>(this.Owner);
      if ((Object) componentInThisOrParents == (Object) null)
      {
        this.Finish();
      }
      else
      {
        GameObject gameObject = componentInThisOrParents.gameObject;
        if (!this.m_actorObject.IsNone)
          this.m_actorObject.Value = gameObject;
        if (!this.m_rootObject.IsNone)
          this.m_rootObject.Value = componentInThisOrParents.GetRootObject();
        if (!this.m_rootObjectMesh.IsNone)
          this.m_rootObjectMesh.Value = componentInThisOrParents.GetMeshRenderer().gameObject;
        this.Finish();
      }
    }
  }
}
