﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector2Add
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Adds a value to Vector2 Variable.")]
  [ActionCategory(ActionCategory.Vector2)]
  public class Vector2Add : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("The vector2 target")]
    [RequiredField]
    public FsmVector2 vector2Variable;
    [Tooltip("The vector2 to add")]
    [RequiredField]
    public FsmVector2 addVector;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;
    [Tooltip("Add the value on a per second bases.")]
    public bool perSecond;

    public override void Reset()
    {
      this.vector2Variable = (FsmVector2) null;
      FsmVector2 fsmVector2 = new FsmVector2();
      fsmVector2.UseVariable = true;
      this.addVector = fsmVector2;
      this.everyFrame = false;
      this.perSecond = false;
    }

    public override void OnEnter()
    {
      this.DoVector2Add();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoVector2Add();
    }

    private void DoVector2Add()
    {
      if (this.perSecond)
        this.vector2Variable.Value = this.vector2Variable.Value + this.addVector.Value * Time.deltaTime;
      else
        this.vector2Variable.Value = this.vector2Variable.Value + this.addVector.Value;
    }
  }
}
