﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetFogColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the color of the Fog in the scene.")]
  [ActionCategory(ActionCategory.RenderSettings)]
  public class SetFogColor : FsmStateAction
  {
    [RequiredField]
    public FsmColor fogColor;
    public bool everyFrame;

    public override void Reset()
    {
      this.fogColor = (FsmColor) Color.white;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetFogColor();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetFogColor();
    }

    private void DoSetFogColor()
    {
      RenderSettings.fogColor = this.fogColor.Value;
    }
  }
}
