﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.iTweenFsmAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("iTween base action - don't use!")]
  public abstract class iTweenFsmAction : FsmStateAction
  {
    protected string itweenType = string.Empty;
    protected int itweenID = -1;
    [ActionSection("Events")]
    public FsmEvent startEvent;
    public FsmEvent finishEvent;
    [Tooltip("Setting this to true will allow the animation to continue independent of the current time which is helpful for animating menus after a game has been paused by setting Time.timeScale=0.")]
    public FsmBool realTime;
    public FsmBool stopOnExit;
    public FsmBool loopDontFinish;
    internal iTweenFSMEvents itweenEvents;

    public override void Reset()
    {
      this.startEvent = (FsmEvent) null;
      this.finishEvent = (FsmEvent) null;
      this.realTime = new FsmBool() { Value = false };
      this.stopOnExit = new FsmBool() { Value = true };
      this.loopDontFinish = new FsmBool() { Value = true };
      this.itweenType = string.Empty;
    }

    protected void OnEnteriTween(FsmOwnerDefault anOwner)
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(anOwner);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      this.itweenEvents = ownerDefaultTarget.AddComponent<iTweenFSMEvents>();
      this.itweenEvents.itweenFSMAction = this;
      ++iTweenFSMEvents.itweenIDCount;
      this.itweenID = iTweenFSMEvents.itweenIDCount;
      this.itweenEvents.itweenID = iTweenFSMEvents.itweenIDCount;
      this.itweenEvents.donotfinish = !this.loopDontFinish.IsNone && this.loopDontFinish.Value;
    }

    protected void IsLoop(bool aValue)
    {
      if (!((Object) this.itweenEvents != (Object) null))
        return;
      this.itweenEvents.islooping = aValue;
    }

    protected void OnExitiTween(FsmOwnerDefault anOwner)
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(anOwner);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((bool) ((Object) this.itweenEvents))
        Object.Destroy((Object) this.itweenEvents);
      if (this.stopOnExit.IsNone)
      {
        iTween.Stop(ownerDefaultTarget, this.itweenType);
      }
      else
      {
        if (!this.stopOnExit.Value)
          return;
        iTween.Stop(ownerDefaultTarget, this.itweenType);
      }
    }

    public enum AxisRestriction
    {
      none,
      x,
      y,
      z,
      xy,
      xz,
      yz,
    }
  }
}
