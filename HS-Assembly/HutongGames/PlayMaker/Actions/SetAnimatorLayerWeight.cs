﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the layer's current weight")]
  [ActionCategory(ActionCategory.Animator)]
  public class SetAnimatorLayerWeight : FsmStateAction
  {
    [Tooltip("The Target. An Animator component is required")]
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("The layer's index")]
    public FsmInt layerIndex;
    [RequiredField]
    [Tooltip("Sets the layer's current weight")]
    public FsmFloat layerWeight;
    [Tooltip("Repeat every frame. Useful for changing over time.")]
    public bool everyFrame;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.layerIndex = (FsmInt) null;
      this.layerWeight = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.DoLayerWeight();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnUpdate()
    {
      this.DoLayerWeight();
    }

    private void DoLayerWeight()
    {
      if ((Object) this._animator == (Object) null)
        return;
      this._animator.SetLayerWeight(this.layerIndex.Value, this.layerWeight.Value);
    }
  }
}
