﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SendEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionTarget(typeof (PlayMakerFSM), "eventTarget", false)]
  [ActionTarget(typeof (GameObject), "eventTarget", false)]
  [Tooltip("Sends an Event after an optional delay. NOTE: To send events between FSMs they must be marked as Global in the Events Browser.")]
  [ActionCategory(ActionCategory.StateMachine)]
  public class SendEvent : FsmStateAction
  {
    [Tooltip("Where to send the event.")]
    public FsmEventTarget eventTarget;
    [RequiredField]
    [Tooltip("The event to send. NOTE: Events must be marked Global to send between FSMs.")]
    public FsmEvent sendEvent;
    [Tooltip("Optional delay in seconds.")]
    [HasFloatSlider(0.0f, 10f)]
    public FsmFloat delay;
    [Tooltip("Repeat every frame. Rarely needed, but can be useful when sending events to other FSMs.")]
    public bool everyFrame;
    private DelayedEvent delayedEvent;

    public override void Reset()
    {
      this.eventTarget = (FsmEventTarget) null;
      this.sendEvent = (FsmEvent) null;
      this.delay = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      if ((double) this.delay.Value < 1.0 / 1000.0)
      {
        this.Fsm.Event(this.eventTarget, this.sendEvent);
        if (this.everyFrame)
          return;
        this.Finish();
      }
      else
        this.delayedEvent = this.Fsm.DelayedEvent(this.eventTarget, this.sendEvent, this.delay.Value);
    }

    public override void OnUpdate()
    {
      if (!this.everyFrame)
      {
        if (!DelayedEvent.WasSent(this.delayedEvent))
          return;
        this.Finish();
      }
      else
        this.Fsm.Event(this.eventTarget, this.sendEvent);
    }
  }
}
