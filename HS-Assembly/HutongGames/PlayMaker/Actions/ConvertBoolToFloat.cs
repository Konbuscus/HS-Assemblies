﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ConvertBoolToFloat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Converts a Bool value to a Float value.")]
  [ActionCategory(ActionCategory.Convert)]
  public class ConvertBoolToFloat : FsmStateAction
  {
    [Tooltip("The Bool variable to test.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmBool boolVariable;
    [Tooltip("The Float variable to set based on the Bool variable value.")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmFloat floatVariable;
    [Tooltip("Float value if Bool variable is false.")]
    public FsmFloat falseValue;
    [Tooltip("Float value if Bool variable is true.")]
    public FsmFloat trueValue;
    [Tooltip("Repeat every frame. Useful if the Bool variable is changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.boolVariable = (FsmBool) null;
      this.floatVariable = (FsmFloat) null;
      this.falseValue = (FsmFloat) 0.0f;
      this.trueValue = (FsmFloat) 1f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoConvertBoolToFloat();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoConvertBoolToFloat();
    }

    private void DoConvertBoolToFloat()
    {
      this.floatVariable.Value = !this.boolVariable.Value ? this.falseValue.Value : this.trueValue.Value;
    }
  }
}
