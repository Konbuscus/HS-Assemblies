﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GameObjectIsVisible
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if a Game Object is visible.")]
  [ActionTarget(typeof (GameObject), "gameObject", false)]
  [ActionCategory(ActionCategory.Logic)]
  public class GameObjectIsVisible : ComponentAction<Renderer>
  {
    [CheckForComponent(typeof (Renderer))]
    [Tooltip("The GameObject to test.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Event to send if the GameObject is visible.")]
    public FsmEvent trueEvent;
    [Tooltip("Event to send if the GameObject is NOT visible.")]
    public FsmEvent falseEvent;
    [Tooltip("Store the result in a bool variable.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoIsVisible();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoIsVisible();
    }

    private void DoIsVisible()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      bool isVisible = this.renderer.isVisible;
      this.storeResult.Value = isVisible;
      this.Fsm.Event(!isVisible ? this.falseEvent : this.trueEvent);
    }
  }
}
