﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.iTweenMoveActorTo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Move an object's actor.  Used for spells that are dynamically loaded.")]
  [ActionCategory("Pegasus")]
  public class iTweenMoveActorTo : iTweenFsmAction
  {
    [Tooltip("The shape of the easing curve applied to the animation.")]
    public iTween.EaseType easeType = iTween.EaseType.linear;
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("iTween ID. If set you can use iTween Stop action to stop it by its id.")]
    public FsmString id;
    [Tooltip("Position the GameObject will animate to.")]
    public FsmVector3 vectorPosition;
    [Tooltip("The time in seconds the animation will take to complete.")]
    public FsmFloat time;
    [Tooltip("The time in seconds the animation will wait before beginning.")]
    public FsmFloat delay;
    [Tooltip("The type of loop to apply once the animation has completed.")]
    public iTween.LoopType loopType;

    public override void Reset()
    {
      base.Reset();
      FsmString fsmString = new FsmString();
      fsmString.UseVariable = true;
      this.id = fsmString;
      FsmVector3 fsmVector3 = new FsmVector3();
      fsmVector3.UseVariable = true;
      this.vectorPosition = fsmVector3;
      this.time = (FsmFloat) 1f;
      this.delay = (FsmFloat) 0.0f;
      this.easeType = iTween.EaseType.linear;
      this.loopType = iTween.LoopType.none;
    }

    public override void OnEnter()
    {
      this.OnEnteriTween(this.gameObject);
      if (this.loopType != iTween.LoopType.none)
        this.IsLoop(true);
      this.DoiTween();
    }

    public override void OnExit()
    {
      this.OnExitiTween(this.gameObject);
    }

    private void DoiTween()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      Actor componentInParents = SceneUtils.FindComponentInParents<Actor>(ownerDefaultTarget);
      if ((Object) componentInParents == (Object) null)
        return;
      GameObject gameObject = componentInParents.gameObject;
      if ((Object) gameObject == (Object) null)
        return;
      this.itweenType = "move";
      Hashtable args = new Hashtable();
      args.Add((object) "position", (object) this.vectorPosition);
      args.Add((object) "name", !this.id.IsNone ? (object) this.id.Value : (object) string.Empty);
      args.Add((object) "delay", (object) (float) (!this.delay.IsNone ? (double) this.delay.Value : 0.0));
      args.Add((object) "easetype", (object) this.easeType);
      args.Add((object) "looptype", (object) this.loopType);
      args.Add((object) "ignoretimescale", (object) (bool) (!this.realTime.IsNone ? (this.realTime.Value ? 1 : 0) : 0));
      if ((double) this.time.Value <= 0.0)
      {
        args.Add((object) "time", (object) 0.0f);
        iTween.FadeUpdate(gameObject, args);
        this.Fsm.Event(this.startEvent);
        this.Fsm.Event(this.finishEvent);
        this.Finish();
      }
      else
      {
        args[(object) "time"] = (object) (float) (!this.time.IsNone ? (double) this.time.Value : 1.0);
        args.Add((object) "oncomplete", (object) "iTweenOnComplete");
        args.Add((object) "oncompleteparams", (object) this.itweenID);
        args.Add((object) "onstart", (object) "iTweenOnStart");
        args.Add((object) "onstartparams", (object) this.itweenID);
        args.Add((object) "oncompletetarget", (object) ownerDefaultTarget);
        iTween.MoveTo(gameObject, args);
      }
    }
  }
}
