﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ResetInputAxes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Input)]
  [Tooltip("Resets all Input. After ResetInputAxes all axes return to 0 and all buttons return to 0 for one frame")]
  public class ResetInputAxes : FsmStateAction
  {
    public override void Reset()
    {
    }

    public override void OnEnter()
    {
      Input.ResetInputAxes();
      this.Finish();
    }
  }
}
