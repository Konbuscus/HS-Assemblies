﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Rigid bodies 2D start sleeping when they come to rest. This action wakes up all rigid bodies 2D in the scene. E.g., if you Set Gravity 2D and want objects at rest to respond.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class WakeAllRigidBodies2d : FsmStateAction
  {
    [Tooltip("Repeat every frame. Note: This would be very expensive!")]
    public bool everyFrame;

    public override void Reset()
    {
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoWakeAll();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoWakeAll();
    }

    private void DoWakeAll()
    {
      Rigidbody2D[] objectsOfType = Object.FindObjectsOfType(typeof (Rigidbody2D)) as Rigidbody2D[];
      if (objectsOfType == null)
        return;
      foreach (Rigidbody2D rigidbody2D in objectsOfType)
        rigidbody2D.WakeUp();
    }
  }
}
