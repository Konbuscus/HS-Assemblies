﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.MousePickRandomAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Raycast from camera and send events. Mouse Down can have a rendom chance to send event")]
  public class MousePickRandomAction : FsmStateAction
  {
    [Tooltip("Min number of clicks before random gate open")]
    public FsmInt RandomGateClicksMin = (FsmInt) 0;
    [Tooltip("Max number of clicks before random gate open")]
    public FsmInt RandomGateClicksMax = (FsmInt) 0;
    [Tooltip("Resets count to 0 once triggered")]
    public FsmBool ResetOnOpen = (FsmBool) false;
    [Tooltip("Click Count")]
    public FsmInt ClickCount = (FsmInt) 0;
    private bool m_opened = true;
    [CheckForComponent(typeof (Collider))]
    public FsmOwnerDefault GameObject;
    [Tooltip("Additional Colliders for mouse pick")]
    public FsmGameObject[] additionalColliders;
    [Tooltip("Mouse Down event. Random Gate open (true)")]
    public FsmEvent mouseDownGateOpen;
    [Tooltip("Mouse Down event. Random Gate is closed (false)")]
    public FsmEvent mouseDownGateClosed;
    [Tooltip("Mouse Over event")]
    public FsmEvent mouseOver;
    [Tooltip("Mouse Up event")]
    public FsmEvent mouseUp;
    [Tooltip("Mouse Off event")]
    public FsmEvent mouseOff;
    [Tooltip("Check for clicks as soon as the state machine enters this state.")]
    public bool checkFirstFrame;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;
    [Tooltip("Stop processing after an event is triggered.")]
    public bool oneShot;
    private int m_RandomValue;

    public override void Reset()
    {
      this.GameObject = (FsmOwnerDefault) null;
      this.additionalColliders = new FsmGameObject[0];
      this.RandomGateClicksMin = (FsmInt) 0;
      this.RandomGateClicksMax = (FsmInt) 0;
      this.ResetOnOpen = (FsmBool) false;
      this.mouseDownGateOpen = (FsmEvent) null;
      this.mouseDownGateClosed = (FsmEvent) null;
      this.mouseOver = (FsmEvent) null;
      this.mouseUp = (FsmEvent) null;
      this.mouseOff = (FsmEvent) null;
      this.checkFirstFrame = true;
      this.everyFrame = true;
      this.oneShot = false;
      this.ClickCount = (FsmInt) 0;
    }

    public override void OnEnter()
    {
      if (this.RandomGateClicksMin.Value > this.RandomGateClicksMax.Value)
        this.RandomGateClicksMin = this.RandomGateClicksMax;
      if (this.m_opened)
      {
        this.m_RandomValue = Random.Range(this.RandomGateClicksMin.Value, this.RandomGateClicksMax.Value);
        this.m_opened = false;
      }
      if (this.checkFirstFrame)
        this.DoMousePickEvent();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoMousePickEvent();
    }

    private void DoMousePickEvent()
    {
      if (this.mouseOver == null && this.mouseOff == null && (this.mouseUp == null || !Input.GetMouseButtonUp(0)) && (this.mouseDownGateOpen == null && this.mouseDownGateClosed == null || !Input.GetMouseButtonDown(0)))
        return;
      UnityEngine.GameObject go = this.GameObject.OwnerOption != OwnerDefaultOption.UseOwner ? this.GameObject.GameObject.Value : this.Owner;
      if (!InputUtil.IsPlayMakerMouseInputAllowed(go))
        return;
      bool flag = UniversalInputManager.Get().InputIsOver(go.gameObject);
      if (!flag && this.additionalColliders.Length > 0)
      {
        for (int index = 0; index < this.additionalColliders.Length; ++index)
        {
          UnityEngine.GameObject gameObj = this.additionalColliders[index].Value;
          if (!((Object) gameObj == (Object) null))
          {
            flag = UniversalInputManager.Get().InputIsOver(gameObj);
            if (flag)
              break;
          }
        }
      }
      if (flag)
      {
        if (UniversalInputManager.Get().GetMouseButtonDown(0))
        {
          this.ClickCount.Value = this.ClickCount.Value + 1;
          if (this.ClickCount.Value >= this.m_RandomValue)
          {
            this.m_opened = true;
            if (this.ResetOnOpen.Value)
              this.ClickCount.Value = 0;
            if (this.mouseDownGateOpen != null)
              this.Fsm.Event(this.mouseDownGateOpen);
          }
          else if (this.mouseDownGateClosed != null)
            this.Fsm.Event(this.mouseDownGateClosed);
          if (this.oneShot)
            this.Finish();
        }
        if (this.mouseOver != null)
          this.Fsm.Event(this.mouseOver);
        if (this.mouseUp == null || !UniversalInputManager.Get().GetMouseButtonUp(0))
          return;
        this.Fsm.Event(this.mouseUp);
        if (!this.oneShot)
          return;
        this.Finish();
      }
      else
      {
        if (this.mouseOff == null)
          return;
        this.Fsm.Event(this.mouseOff);
      }
    }

    public override string ErrorCheck()
    {
      return string.Empty;
    }
  }
}
