﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.IsSleeping2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics2D)]
  [Tooltip("Tests if a Game Object's Rigidbody 2D is sleeping.")]
  public class IsSleeping2d : ComponentAction<Rigidbody2D>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody2D))]
    [Tooltip("The GameObject with the Rigidbody2D attached")]
    public FsmOwnerDefault gameObject;
    [Tooltip("Event sent if sleeping")]
    public FsmEvent trueEvent;
    [Tooltip("Event sent if not sleeping")]
    public FsmEvent falseEvent;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the value in a Boolean variable")]
    public FsmBool store;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.store = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoIsSleeping();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoIsSleeping();
    }

    private void DoIsSleeping()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      bool flag = this.rigidbody2d.IsSleeping();
      this.store.Value = flag;
      this.Fsm.Event(!flag ? this.falseEvent : this.trueEvent);
    }
  }
}
