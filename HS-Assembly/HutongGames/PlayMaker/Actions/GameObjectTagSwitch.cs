﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GameObjectTagSwitch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Logic)]
  [Tooltip("Sends an Event based on a Game Object's Tag.")]
  public class GameObjectTagSwitch : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("The GameObject to test.")]
    [RequiredField]
    public FsmGameObject gameObject;
    [UIHint(UIHint.Tag)]
    [CompoundArray("Tag Switches", "Compare Tag", "Send Event")]
    public FsmString[] compareTo;
    public FsmEvent[] sendEvent;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmGameObject) null;
      this.compareTo = new FsmString[1];
      this.sendEvent = new FsmEvent[1];
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoTagSwitch();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoTagSwitch();
    }

    private void DoTagSwitch()
    {
      GameObject gameObject = this.gameObject.Value;
      if ((Object) gameObject == (Object) null)
        return;
      for (int index = 0; index < this.compareTo.Length; ++index)
      {
        if (gameObject.tag == this.compareTo[index].Value)
        {
          this.Fsm.Event(this.sendEvent[index]);
          break;
        }
      }
    }
  }
}
