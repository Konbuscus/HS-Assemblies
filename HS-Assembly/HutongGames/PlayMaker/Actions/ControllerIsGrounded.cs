﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ControllerIsGrounded
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if a Character Controller on a Game Object was touching the ground during the last move.")]
  [ActionCategory(ActionCategory.Character)]
  public class ControllerIsGrounded : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject to check.")]
    [CheckForComponent(typeof (CharacterController))]
    public FsmOwnerDefault gameObject;
    [Tooltip("Event to send if touching the ground.")]
    public FsmEvent trueEvent;
    [Tooltip("Event to send if not touching the ground.")]
    public FsmEvent falseEvent;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result in a bool variable.")]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame while the state is active.")]
    public bool everyFrame;
    private GameObject previousGo;
    private CharacterController controller;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoControllerIsGrounded();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoControllerIsGrounded();
    }

    private void DoControllerIsGrounded()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((Object) ownerDefaultTarget != (Object) this.previousGo)
      {
        this.controller = ownerDefaultTarget.GetComponent<CharacterController>();
        this.previousGo = ownerDefaultTarget;
      }
      if ((Object) this.controller == (Object) null)
        return;
      bool isGrounded = this.controller.isGrounded;
      this.storeResult.Value = isGrounded;
      this.Fsm.Event(!isGrounded ? this.falseEvent : this.trueEvent);
    }
  }
}
