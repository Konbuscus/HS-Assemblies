﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetSystemDateTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets system date and time info and stores it in a string variable. An optional format string gives you a lot of control over the formatting (see online docs for format syntax).")]
  [ActionCategory(ActionCategory.Time)]
  public class GetSystemDateTime : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("Store System DateTime as a string.")]
    public FsmString storeString;
    [Tooltip("Optional format string. E.g., MM/dd/yyyy HH:mm")]
    public FsmString format;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.storeString = (FsmString) null;
      this.format = (FsmString) "MM/dd/yyyy HH:mm";
    }

    public override void OnEnter()
    {
      this.storeString.Value = DateTime.Now.ToString(this.format.Value);
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.storeString.Value = DateTime.Now.ToString(this.format.Value);
    }
  }
}
