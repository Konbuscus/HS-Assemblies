﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimatorPlay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Plays a state. This could be used to synchronize your animation with audio or synchronize an Animator over the network.")]
  [ActionCategory(ActionCategory.Animator)]
  public class AnimatorPlay : FsmStateAction
  {
    [Tooltip("The target. An Animator component is required")]
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [Tooltip("The name of the state that will be played.")]
    public FsmString stateName;
    [Tooltip("The layer where the state is.")]
    public FsmInt layer;
    [Tooltip("The normalized time at which the state will play")]
    public FsmFloat normalizedTime;
    [Tooltip("Repeat every frame. Useful when using normalizedTime to manually control the animation.")]
    public bool everyFrame;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.stateName = (FsmString) null;
      FsmInt fsmInt = new FsmInt();
      fsmInt.UseVariable = true;
      this.layer = fsmInt;
      FsmFloat fsmFloat = new FsmFloat();
      fsmFloat.UseVariable = true;
      this.normalizedTime = fsmFloat;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        this.DoAnimatorPlay();
        if (this.everyFrame)
          return;
        this.Finish();
      }
    }

    public override void OnUpdate()
    {
      this.DoAnimatorPlay();
    }

    private void DoAnimatorPlay()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      this._animator.Play(this.stateName.Value, !this.layer.IsNone ? this.layer.Value : -1, !this.normalizedTime.IsNone ? this.normalizedTime.Value : float.NegativeInfinity);
    }
  }
}
