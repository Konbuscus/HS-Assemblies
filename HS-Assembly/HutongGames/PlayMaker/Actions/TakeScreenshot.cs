﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.TakeScreenshot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Saves a Screenshot to the users MyPictures folder. TIP: Can be useful for automated testing and debugging.")]
  [ActionCategory(ActionCategory.Application)]
  public class TakeScreenshot : FsmStateAction
  {
    [RequiredField]
    public FsmString filename;
    public bool autoNumber;
    private int screenshotCount;

    public override void Reset()
    {
      this.filename = (FsmString) string.Empty;
      this.autoNumber = false;
    }

    public override void OnEnter()
    {
      if (string.IsNullOrEmpty(this.filename.Value))
        return;
      string str1 = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "/";
      string str2 = str1 + this.filename.Value + ".png";
      object[] objArray;
      if (this.autoNumber)
      {
        for (; File.Exists(str2); str2 = string.Concat(objArray))
        {
          ++this.screenshotCount;
          objArray = new object[4]
          {
            (object) str1,
            (object) this.filename.Value,
            (object) this.screenshotCount,
            (object) ".png"
          };
        }
      }
      Application.CaptureScreenshot(str2);
      this.Finish();
    }
  }
}
