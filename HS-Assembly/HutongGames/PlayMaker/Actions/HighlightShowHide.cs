﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.HighlightShowHide
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Used to Show and Hide card Highlights")]
  [ActionCategory("Pegasus")]
  public class HighlightShowHide : FsmStateAction
  {
    [RequiredField]
    [Tooltip("Show or Hide")]
    public FsmBool m_Show = (FsmBool) true;
    [Tooltip("GameObject to send highlight states to")]
    [RequiredField]
    public FsmOwnerDefault m_gameObj;
    private DelayedEvent delayedEvent;

    public override void Reset()
    {
      this.m_gameObj = (FsmOwnerDefault) null;
      this.m_Show = (FsmBool) true;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_gameObj);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        HighlightState[] componentsInChildren = ownerDefaultTarget.GetComponentsInChildren<HighlightState>();
        if (componentsInChildren == null)
        {
          this.Finish();
        }
        else
        {
          foreach (HighlightState highlightState in componentsInChildren)
          {
            if (this.m_Show.Value)
              highlightState.Show();
            else
              highlightState.Hide();
          }
          this.Finish();
        }
      }
    }
  }
}
