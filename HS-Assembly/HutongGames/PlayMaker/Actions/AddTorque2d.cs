﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AddTorque2d
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Adds a 2d torque (rotational force) to a Game Object.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class AddTorque2d : ComponentAction<Rigidbody2D>
  {
    [CheckForComponent(typeof (Rigidbody2D))]
    [RequiredField]
    [Tooltip("The GameObject to add torque to.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("Option for applying the force")]
    public ForceMode2D forceMode;
    [Tooltip("Torque")]
    public FsmFloat torque;
    [Tooltip("Repeat every frame while the state is active.")]
    public bool everyFrame;

    public override void OnPreprocess()
    {
      this.Fsm.HandleFixedUpdate = true;
    }

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.torque = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoAddTorque();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnFixedUpdate()
    {
      this.DoAddTorque();
    }

    private void DoAddTorque()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.rigidbody2d.AddTorque(this.torque.Value, this.forceMode);
    }
  }
}
