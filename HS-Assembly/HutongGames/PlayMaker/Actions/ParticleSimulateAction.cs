﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ParticleSimulateAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Simulates a Particle System at a variable speed.")]
  public class ParticleSimulateAction : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault m_GameObject;
    [Tooltip("Time at which this particle displays. This leave the system in a paused state.")]
    public FsmFloat m_TimeToFastForwardTo;
    [Tooltip("Run this action on all child objects' Particle Systems.")]
    public FsmBool m_IncludeChildren;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_TimeToFastForwardTo = (FsmFloat) 0.0f;
      this.m_IncludeChildren = (FsmBool) false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      ParticleSystem component = ownerDefaultTarget.GetComponent<ParticleSystem>();
      if ((Object) component == (Object) null)
        Debug.LogWarning((object) string.Format("ParticleSimulateAction.OnEnter() - GameObject {0} has no ParticleSystem component", (object) ownerDefaultTarget));
      else
        component.Simulate(this.m_TimeToFastForwardTo.Value, this.m_IncludeChildren.Value);
    }
  }
}
