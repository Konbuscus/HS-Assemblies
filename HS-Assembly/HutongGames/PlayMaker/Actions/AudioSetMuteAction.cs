﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioSetMuteAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Mute/unmute the Audio Source on a Game Object.")]
  [ActionCategory("Pegasus Audio")]
  public class AudioSetMuteAction : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (AudioSource))]
    public FsmOwnerDefault m_GameObject;
    public FsmBool m_Mute;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_Mute = (FsmBool) false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget != (Object) null)
      {
        AudioSource component = ownerDefaultTarget.GetComponent<AudioSource>();
        if ((Object) component != (Object) null)
          component.mute = this.m_Mute.Value;
      }
      this.Finish();
    }
  }
}
