﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutEndArea
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Close a GUILayout group started with BeginArea.")]
  [ActionCategory(ActionCategory.GUILayout)]
  public class GUILayoutEndArea : FsmStateAction
  {
    public override void Reset()
    {
    }

    public override void OnGUI()
    {
      GUILayout.EndArea();
    }
  }
}
