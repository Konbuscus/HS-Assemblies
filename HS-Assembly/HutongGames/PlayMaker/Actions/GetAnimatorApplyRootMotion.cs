﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Gets the value of ApplyRootMotion of an avatar. If true, root is controlled by animations")]
  public class GetAnimatorApplyRootMotion : FsmStateAction
  {
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The Target. An Animator component is required")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [ActionSection("Results")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Is the rootMotionapplied. If true, root is controlled by animations")]
    public FsmBool rootMotionApplied;
    [Tooltip("Event send if the root motion is applied")]
    public FsmEvent rootMotionIsAppliedEvent;
    [Tooltip("Event send if the root motion is not applied")]
    public FsmEvent rootMotionIsNotAppliedEvent;
    private Animator _animator;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.rootMotionApplied = (FsmBool) null;
      this.rootMotionIsAppliedEvent = (FsmEvent) null;
      this.rootMotionIsNotAppliedEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.GetApplyMotionRoot();
          this.Finish();
        }
      }
    }

    private void GetApplyMotionRoot()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      bool applyRootMotion = this._animator.applyRootMotion;
      this.rootMotionApplied.Value = applyRootMotion;
      if (applyRootMotion)
        this.Fsm.Event(this.rootMotionIsAppliedEvent);
      else
        this.Fsm.Event(this.rootMotionIsNotAppliedEvent);
    }
  }
}
