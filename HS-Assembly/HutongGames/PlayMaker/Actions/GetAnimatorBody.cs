﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorBody
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the avatar body mass center position and rotation. Optionally accept a GameObject to get the body transform. \nThe position and rotation are local to the gameobject")]
  [ActionCategory(ActionCategory.Animator)]
  public class GetAnimatorBody : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The target. An Animator component and a PlayMakerAnimatorProxy component are required")]
    [CheckForComponent(typeof (Animator))]
    public FsmOwnerDefault gameObject;
    [Tooltip("Repeat every frame. Useful when changing over time.")]
    public bool everyFrame;
    [Tooltip("The avatar body mass center")]
    [ActionSection("Results")]
    [UIHint(UIHint.Variable)]
    public FsmVector3 bodyPosition;
    [Tooltip("The avatar body mass center")]
    [UIHint(UIHint.Variable)]
    public FsmQuaternion bodyRotation;
    [Tooltip("If set, apply the body mass center position and rotation to this gameObject")]
    public FsmGameObject bodyGameObject;
    private Animator _animator;
    private Transform _transform;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.bodyPosition = (FsmVector3) null;
      this.bodyRotation = (FsmQuaternion) null;
      this.bodyGameObject = (FsmGameObject) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          GameObject gameObject = this.bodyGameObject.Value;
          if ((Object) gameObject != (Object) null)
            this._transform = gameObject.transform;
          this.DoGetBodyPosition();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void DoAnimatorMove()
    {
      this.DoGetBodyPosition();
    }

    private void DoGetBodyPosition()
    {
      if ((Object) this._animator == (Object) null)
        return;
      this.bodyPosition.Value = this._animator.bodyPosition;
      this.bodyRotation.Value = this._animator.bodyRotation;
      if (!((Object) this._transform != (Object) null))
        return;
      this._transform.position = this._animator.bodyPosition;
      this._transform.rotation = this._animator.bodyRotation;
    }
  }
}
