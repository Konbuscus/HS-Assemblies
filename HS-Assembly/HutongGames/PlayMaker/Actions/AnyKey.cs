﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnyKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Input)]
  [Tooltip("Sends an Event when the user hits any Key or Mouse Button.")]
  public class AnyKey : FsmStateAction
  {
    [RequiredField]
    [Tooltip("Event to send when any Key or Mouse Button is pressed.")]
    public FsmEvent sendEvent;

    public override void Reset()
    {
      this.sendEvent = (FsmEvent) null;
    }

    public override void OnUpdate()
    {
      if (!Input.anyKeyDown)
        return;
      this.Fsm.Event(this.sendEvent);
    }
  }
}
