﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimatorStopAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Disables an Animator.")]
  public class AnimatorStopAction : FsmStateAction
  {
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    [Tooltip("Game Object to play the animation on.")]
    public FsmOwnerDefault m_GameObject;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if (!(bool) ((Object) ownerDefaultTarget))
      {
        this.Finish();
      }
      else
      {
        Animator component = ownerDefaultTarget.GetComponent<Animator>();
        if ((bool) ((Object) component))
          component.enabled = false;
        this.Finish();
      }
    }
  }
}
