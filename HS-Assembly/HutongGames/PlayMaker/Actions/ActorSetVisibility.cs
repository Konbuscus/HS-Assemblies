﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ActorSetVisibility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Show or Hide an Actor without messing up the game.")]
  [ActionCategory("Pegasus")]
  public class ActorSetVisibility : ActorAction
  {
    public FsmGameObject m_ActorObject;
    [Tooltip("Should the Actor be set to visible or invisible?")]
    public FsmBool m_Visible;
    [Tooltip("Don't touch the Actor's SpellTable when setting visibility")]
    public FsmBool m_IgnoreSpells;
    [Tooltip("Resets to the initial visibility once\nit leaves the state")]
    public bool m_ResetOnExit;
    protected bool m_initialVisibility;

    protected override GameObject GetActorOwner()
    {
      return this.m_ActorObject.Value;
    }

    public override void Reset()
    {
      this.m_ActorObject = (FsmGameObject) null;
      this.m_Visible = (FsmBool) false;
      this.m_IgnoreSpells = (FsmBool) false;
      this.m_ResetOnExit = false;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_actor == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this.m_initialVisibility = this.m_actor.IsShown();
        if (this.m_Visible.Value)
          this.ShowActor();
        else
          this.HideActor();
        this.Finish();
      }
    }

    public override void OnExit()
    {
      if (!this.m_ResetOnExit)
        return;
      if (this.m_initialVisibility)
        this.ShowActor();
      else
        this.HideActor();
    }

    public void ShowActor()
    {
      this.m_actor.Show(this.m_IgnoreSpells.Value);
    }

    public void HideActor()
    {
      this.m_actor.Hide(this.m_IgnoreSpells.Value);
    }
  }
}
