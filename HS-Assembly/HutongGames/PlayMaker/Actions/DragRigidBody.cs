﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DragRigidBody
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Drag a Rigid body with the mouse. If draggingPlaneTransform is defined, it will use the UP axis of this gameObject as the dragging plane normal \nThat is select the ground Plane, if you want to drag object on the ground instead of from the camera point of view.")]
  public class DragRigidBody : FsmStateAction
  {
    [Tooltip("the springness of the drag")]
    public FsmFloat spring;
    [Tooltip("the damping of the drag")]
    public FsmFloat damper;
    [Tooltip("the drag during dragging")]
    public FsmFloat drag;
    [Tooltip("the angular drag during dragging")]
    public FsmFloat angularDrag;
    [Tooltip("The Max Distance between the dragging target and the RigidBody being dragged")]
    public FsmFloat distance;
    [Tooltip("If TRUE, dragging will have close to no effect on the Rigidbody rotation ( except if it hits other bodies as you drag it)")]
    public FsmBool attachToCenterOfMass;
    [Tooltip("Move th object forward and back or up and down")]
    public FsmBool moveUp;
    [Tooltip("If Defined. Use this transform Up axis as the dragging plane normal. Typically, set it to the ground plane if you want to drag objects around on the floor..")]
    public FsmOwnerDefault draggingPlaneTransform;
    private SpringJoint springJoint;
    private bool isDragging;
    private float oldDrag;
    private float oldAngularDrag;
    private Camera _cam;
    private GameObject _goPlane;
    private Vector3 _dragStartPos;
    private float dragDistance;

    public override void Reset()
    {
      this.spring = (FsmFloat) 50f;
      this.damper = (FsmFloat) 5f;
      this.drag = (FsmFloat) 10f;
      this.angularDrag = (FsmFloat) 5f;
      this.distance = (FsmFloat) 0.2f;
      this.attachToCenterOfMass = (FsmBool) false;
      this.draggingPlaneTransform = (FsmOwnerDefault) null;
      this.moveUp = (FsmBool) true;
    }

    public override void OnEnter()
    {
      this._cam = Camera.main;
      this._goPlane = this.Fsm.GetOwnerDefaultTarget(this.draggingPlaneTransform);
    }

    public override void OnUpdate()
    {
      if (!this.isDragging && UniversalInputManager.Get().GetMouseButtonDown(0))
      {
        RaycastHit hitInfo;
        if (!Physics.Raycast(this._cam.ScreenPointToRay(UniversalInputManager.Get().GetMousePosition()), out hitInfo, 100f) || !(bool) ((Object) hitInfo.rigidbody) || hitInfo.rigidbody.isKinematic)
          return;
        this.StartDragging(hitInfo);
      }
      if (!this.isDragging)
        return;
      this.Drag();
    }

    private void StartDragging(RaycastHit hit)
    {
      this.isDragging = true;
      if (!(bool) ((Object) this.springJoint))
      {
        GameObject gameObject = new GameObject("__Rigidbody dragger__");
        Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
        this.springJoint = gameObject.AddComponent<SpringJoint>();
        rigidbody.isKinematic = true;
      }
      this.springJoint.transform.position = hit.point;
      if (this.attachToCenterOfMass.Value)
        this.springJoint.anchor = this.springJoint.transform.InverseTransformPoint(this._cam.transform.TransformDirection(hit.rigidbody.centerOfMass) + hit.rigidbody.transform.position);
      else
        this.springJoint.anchor = Vector3.zero;
      this._dragStartPos = hit.point;
      this.springJoint.spring = this.spring.Value;
      this.springJoint.damper = this.damper.Value;
      this.springJoint.maxDistance = this.distance.Value;
      this.springJoint.connectedBody = hit.rigidbody;
      this.oldDrag = this.springJoint.connectedBody.drag;
      this.oldAngularDrag = this.springJoint.connectedBody.angularDrag;
      this.springJoint.connectedBody.drag = this.drag.Value;
      this.springJoint.connectedBody.angularDrag = this.angularDrag.Value;
      this.dragDistance = hit.distance;
    }

    private void Drag()
    {
      if (!UniversalInputManager.Get().GetMouseButton(0))
      {
        this.StopDragging();
      }
      else
      {
        Ray ray = this._cam.ScreenPointToRay(UniversalInputManager.Get().GetMousePosition());
        if ((Object) this._goPlane != (Object) null)
        {
          Plane plane = new Plane();
          plane = !this.moveUp.Value ? new Plane(this._goPlane.transform.up, this._dragStartPos) : new Plane(this._goPlane.transform.forward, this._dragStartPos);
          float enter;
          if (!plane.Raycast(ray, out enter))
            return;
          this.springJoint.transform.position = ray.GetPoint(enter);
        }
        else
          this.springJoint.transform.position = ray.GetPoint(this.dragDistance);
      }
    }

    private void StopDragging()
    {
      this.isDragging = false;
      if ((Object) this.springJoint == (Object) null || !(bool) ((Object) this.springJoint.connectedBody))
        return;
      this.springJoint.connectedBody.drag = this.oldDrag;
      this.springJoint.connectedBody.angularDrag = this.oldAngularDrag;
      this.springJoint.connectedBody = (Rigidbody) null;
    }

    public override void OnExit()
    {
      this.StopDragging();
    }
  }
}
