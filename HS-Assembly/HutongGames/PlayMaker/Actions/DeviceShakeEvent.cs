﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DeviceShakeEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Device)]
  [Tooltip("Sends an Event when the mobile device is shaken.")]
  public class DeviceShakeEvent : FsmStateAction
  {
    [Tooltip("Amount of acceleration required to trigger the event. Higher numbers require a harder shake.")]
    [RequiredField]
    public FsmFloat shakeThreshold;
    [RequiredField]
    [Tooltip("Event to send when Shake Threshold is exceded.")]
    public FsmEvent sendEvent;

    public override void Reset()
    {
      this.shakeThreshold = (FsmFloat) 3f;
      this.sendEvent = (FsmEvent) null;
    }

    public override void OnUpdate()
    {
      if ((double) Input.acceleration.sqrMagnitude <= (double) this.shakeThreshold.Value * (double) this.shakeThreshold.Value)
        return;
      this.Fsm.Event(this.sendEvent);
    }
  }
}
