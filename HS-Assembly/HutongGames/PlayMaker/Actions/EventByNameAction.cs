﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.EventByNameAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Trigger Event by name after an optional delay. NOTE: Use this over Send Event if you store events as string variables.")]
  public class EventByNameAction : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The event to send. NOTE: Events must be marked Global to send between FSMs.")]
    public FsmString sendEvent;
    [Tooltip("Optional delay in seconds.")]
    [HasFloatSlider(0.0f, 10f)]
    public FsmFloat delay;
    [RequiredField]
    [Tooltip("The event to send if the send event is not found. NOTE: Events must be marked Global to send between FSMs.")]
    public FsmString fallbackEvent;
    [Tooltip("Repeat every frame. Rarely needed.")]
    public bool everyFrame;
    private DelayedEvent delayedEvent;

    public override void Reset()
    {
      this.sendEvent = (FsmString) null;
      this.delay = (FsmFloat) null;
      this.fallbackEvent = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      string str = "FINISHED";
      if (this.fallbackEvent != null)
        str = this.fallbackEvent.Value;
      FsmState state = this.State;
      if (state != null)
      {
        foreach (FsmTransition transition in state.Transitions)
        {
          if (transition.EventName == this.sendEvent.Value)
            str = this.sendEvent.Value;
        }
      }
      if ((double) this.delay.Value < 1.0 / 1000.0)
      {
        this.Fsm.Event(str);
        this.Finish();
      }
      else
        this.delayedEvent = this.Fsm.DelayedEvent(FsmEvent.GetFsmEvent(str), this.delay.Value);
    }

    public override void OnUpdate()
    {
      if (this.everyFrame || !DelayedEvent.WasSent(this.delayedEvent))
        return;
      this.Finish();
    }
  }
}
