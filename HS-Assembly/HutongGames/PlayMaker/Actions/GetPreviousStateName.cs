﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetPreviousStateName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.StateMachine)]
  [Tooltip("Gets the name of the previously active state and stores it in a String Variable.")]
  public class GetPreviousStateName : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    public FsmString storeName;

    public override void Reset()
    {
      this.storeName = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.storeName.Value = this.Fsm.PreviousActiveState != null ? this.Fsm.PreviousActiveState.Name : (string) null;
      this.Finish();
    }
  }
}
