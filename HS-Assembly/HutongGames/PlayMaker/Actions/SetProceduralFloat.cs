﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetProceduralFloat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Substance")]
  [Tooltip("Set a named float property in a Substance material. NOTE: Use Rebuild Textures after setting Substance properties.")]
  public class SetProceduralFloat : FsmStateAction
  {
    [RequiredField]
    public FsmMaterial substanceMaterial;
    [RequiredField]
    public FsmString floatProperty;
    [RequiredField]
    public FsmFloat floatValue;
    [Tooltip("NOTE: Updating procedural materials every frame can be very slow!")]
    public bool everyFrame;

    public override void Reset()
    {
      this.substanceMaterial = (FsmMaterial) null;
      this.floatProperty = (FsmString) string.Empty;
      this.floatValue = (FsmFloat) 0.0f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetProceduralFloat();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetProceduralFloat();
    }

    private void DoSetProceduralFloat()
    {
      ProceduralMaterial proceduralMaterial = this.substanceMaterial.Value as ProceduralMaterial;
      if ((Object) proceduralMaterial == (Object) null)
        this.LogError("Not a substance material!");
      else
        proceduralMaterial.SetProceduralFloat(this.floatProperty.Value, this.floatValue.Value);
    }
  }
}
