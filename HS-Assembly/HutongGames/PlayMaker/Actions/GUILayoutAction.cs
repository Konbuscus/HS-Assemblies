﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("GUILayout base action - don't use!")]
  public abstract class GUILayoutAction : FsmStateAction
  {
    public LayoutOption[] layoutOptions;
    private GUILayoutOption[] options;

    public GUILayoutOption[] LayoutOptions
    {
      get
      {
        if (this.options == null)
        {
          this.options = new GUILayoutOption[this.layoutOptions.Length];
          for (int index = 0; index < this.layoutOptions.Length; ++index)
            this.options[index] = this.layoutOptions[index].GetGUILayoutOption();
        }
        return this.options;
      }
    }

    public override void Reset()
    {
      this.layoutOptions = new LayoutOption[0];
    }
  }
}
