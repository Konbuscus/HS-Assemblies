﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetControllerHitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets info on the last Character Controller collision and store in variables.")]
  [ActionCategory(ActionCategory.Character)]
  public class GetControllerHitInfo : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    public FsmGameObject gameObjectHit;
    [UIHint(UIHint.Variable)]
    public FsmVector3 contactPoint;
    [UIHint(UIHint.Variable)]
    public FsmVector3 contactNormal;
    [UIHint(UIHint.Variable)]
    public FsmVector3 moveDirection;
    [UIHint(UIHint.Variable)]
    public FsmFloat moveLength;
    [Tooltip("Useful for triggering different effects. Audio, particles...")]
    [UIHint(UIHint.Variable)]
    public FsmString physicsMaterialName;

    public override void Reset()
    {
      this.gameObjectHit = (FsmGameObject) null;
      this.contactPoint = (FsmVector3) null;
      this.contactNormal = (FsmVector3) null;
      this.moveDirection = (FsmVector3) null;
      this.moveLength = (FsmFloat) null;
      this.physicsMaterialName = (FsmString) null;
    }

    private void StoreTriggerInfo()
    {
      if (this.Fsm.ControllerCollider == null)
        return;
      this.gameObjectHit.Value = this.Fsm.ControllerCollider.gameObject;
      this.contactPoint.Value = this.Fsm.ControllerCollider.point;
      this.contactNormal.Value = this.Fsm.ControllerCollider.normal;
      this.moveDirection.Value = this.Fsm.ControllerCollider.moveDirection;
      this.moveLength.Value = this.Fsm.ControllerCollider.moveLength;
      this.physicsMaterialName.Value = this.Fsm.ControllerCollider.collider.material.name;
    }

    public override void OnEnter()
    {
      this.StoreTriggerInfo();
      this.Finish();
    }

    public override string ErrorCheck()
    {
      return ActionHelpers.CheckOwnerPhysicsSetup(this.Owner);
    }
  }
}
