﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetIPhoneSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Get various iPhone settings.")]
  [ActionCategory(ActionCategory.Device)]
  public class GetIPhoneSettings : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("Allows device to fall into 'sleep' state with screen being dim if no touches occurred. Default value is true.")]
    public FsmBool getScreenCanDarken;
    [UIHint(UIHint.Variable)]
    [Tooltip("A unique device identifier string. It is guaranteed to be unique for every device (Read Only).")]
    public FsmString getUniqueIdentifier;
    [Tooltip("The user defined name of the device (Read Only).")]
    [UIHint(UIHint.Variable)]
    public FsmString getName;
    [Tooltip("The model of the device (Read Only).")]
    [UIHint(UIHint.Variable)]
    public FsmString getModel;
    [UIHint(UIHint.Variable)]
    [Tooltip("The name of the operating system running on the device (Read Only).")]
    public FsmString getSystemName;
    [UIHint(UIHint.Variable)]
    [Tooltip("The generation of the device (Read Only).")]
    public FsmString getGeneration;

    public override void Reset()
    {
      this.getScreenCanDarken = (FsmBool) null;
      this.getUniqueIdentifier = (FsmString) null;
      this.getName = (FsmString) null;
      this.getModel = (FsmString) null;
      this.getSystemName = (FsmString) null;
      this.getGeneration = (FsmString) null;
    }

    public override void OnEnter()
    {
      this.Finish();
    }
  }
}
