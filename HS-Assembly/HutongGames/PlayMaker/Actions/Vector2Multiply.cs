﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector2Multiply
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Multiplies a Vector2 variable by a Float.")]
  [ActionCategory(ActionCategory.Vector2)]
  public class Vector2Multiply : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The vector to Multiply")]
    [UIHint(UIHint.Variable)]
    public FsmVector2 vector2Variable;
    [Tooltip("The multiplication factor")]
    [RequiredField]
    public FsmFloat multiplyBy;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.vector2Variable = (FsmVector2) null;
      this.multiplyBy = (FsmFloat) 1f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.vector2Variable.Value = this.vector2Variable.Value * this.multiplyBy.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.vector2Variable.Value = this.vector2Variable.Value * this.multiplyBy.Value;
    }
  }
}
