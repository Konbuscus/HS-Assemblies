﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetObjectValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the value of an Object Variable.")]
  [ActionCategory(ActionCategory.UnityObject)]
  public class SetObjectValue : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmObject objectVariable;
    [RequiredField]
    public FsmObject objectValue;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.objectVariable = (FsmObject) null;
      this.objectValue = (FsmObject) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.objectVariable.Value = this.objectValue.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.objectVariable.Value = this.objectValue.Value;
    }
  }
}
