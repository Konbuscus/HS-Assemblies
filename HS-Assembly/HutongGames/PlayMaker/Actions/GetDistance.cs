﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetDistance
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Measures the Distance betweens 2 Game Objects and stores the result in a Float Variable.")]
  public class GetDistance : FsmStateAction
  {
    [RequiredField]
    [Tooltip("Measure distance from this GameObject.")]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [Tooltip("Target GameObject.")]
    public FsmGameObject target;
    [Tooltip("Store the distance in a float variable.")]
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmFloat storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.target = (FsmGameObject) null;
      this.storeResult = (FsmFloat) null;
      this.everyFrame = true;
    }

    public override void OnEnter()
    {
      this.DoGetDistance();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetDistance();
    }

    private void DoGetDistance()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null || (Object) this.target.Value == (Object) null || this.storeResult == null)
        return;
      this.storeResult.Value = Vector3.Distance(ownerDefaultTarget.transform.position, this.target.Value.transform.position);
    }
  }
}
