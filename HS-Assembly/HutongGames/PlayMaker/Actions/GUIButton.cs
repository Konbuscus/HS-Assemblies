﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUIButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("GUI button. Sends an Event when pressed. Optionally store the button state in a Bool Variable.")]
  [ActionCategory(ActionCategory.GUI)]
  public class GUIButton : GUIContentAction
  {
    public FsmEvent sendEvent;
    [UIHint(UIHint.Variable)]
    public FsmBool storeButtonState;

    public override void Reset()
    {
      base.Reset();
      this.sendEvent = (FsmEvent) null;
      this.storeButtonState = (FsmBool) null;
      this.style = (FsmString) "Button";
    }

    public override void OnGUI()
    {
      base.OnGUI();
      bool flag = false;
      if (GUI.Button(this.rect, this.content, (GUIStyle) this.style.Value))
      {
        this.Fsm.Event(this.sendEvent);
        flag = true;
      }
      if (this.storeButtonState == null)
        return;
      this.storeButtonState.Value = flag;
    }
  }
}
