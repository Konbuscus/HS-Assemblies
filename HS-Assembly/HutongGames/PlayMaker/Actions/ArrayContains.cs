﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayContains
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Check if an Array contains a value. Optionally get its index.")]
  [ActionCategory(ActionCategory.Array)]
  public class ArrayContains : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("The Array Variable to use.")]
    public FsmArray array;
    [MatchElementType("array")]
    [Tooltip("The value to check against in the array.")]
    [RequiredField]
    public FsmVar value;
    [Tooltip("The index of the value in the array.")]
    [ActionSection("Result")]
    [UIHint(UIHint.Variable)]
    public FsmInt index;
    [Tooltip("Store in a bool whether it contains that element or not (described below)")]
    [UIHint(UIHint.Variable)]
    public FsmBool isContained;
    [Tooltip("Event sent if the array contains that element (described below)")]
    public FsmEvent isContainedEvent;
    [Tooltip("Event sent if the array does not contains that element (described below)")]
    public FsmEvent isNotContainedEvent;

    public override void Reset()
    {
      this.array = (FsmArray) null;
      this.value = (FsmVar) null;
      this.index = (FsmInt) null;
      this.isContained = (FsmBool) null;
      this.isContainedEvent = (FsmEvent) null;
      this.isNotContainedEvent = (FsmEvent) null;
    }

    public override void OnEnter()
    {
      this.DoCheckContainsValue();
      this.Finish();
    }

    private void DoCheckContainsValue()
    {
      this.value.UpdateValue();
      int num = Array.IndexOf<object>(this.array.Values, this.value.GetValue());
      bool flag = num != -1;
      this.isContained.Value = flag;
      this.index.Value = num;
      if (flag)
        this.Fsm.Event(this.isContainedEvent);
      else
        this.Fsm.Event(this.isNotContainedEvent);
    }
  }
}
