﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioPause
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Audio)]
  [Tooltip("Pauses playing the Audio Clip played by an Audio Source component on a Game Object.")]
  public class AudioPause : FsmStateAction
  {
    [CheckForComponent(typeof (AudioSource))]
    [RequiredField]
    [Tooltip("The GameObject with an Audio Source component.")]
    public FsmOwnerDefault gameObject;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget != (Object) null)
      {
        AudioSource component = ownerDefaultTarget.GetComponent<AudioSource>();
        if ((Object) component != (Object) null)
          component.Pause();
      }
      this.Finish();
    }
  }
}
