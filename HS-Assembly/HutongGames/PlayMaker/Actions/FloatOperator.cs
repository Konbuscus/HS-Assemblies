﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FloatOperator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Performs math operations on 2 Floats: Add, Subtract, Multiply, Divide, Min, Max.")]
  [ActionCategory(ActionCategory.Math)]
  public class FloatOperator : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The first float.")]
    public FsmFloat float1;
    [RequiredField]
    [Tooltip("The second float.")]
    public FsmFloat float2;
    [Tooltip("The math operation to perform on the floats.")]
    public FloatOperator.Operation operation;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result of the operation in a float variable.")]
    public FsmFloat storeResult;
    [Tooltip("Repeat every frame. Useful if the variables are changing.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.float1 = (FsmFloat) null;
      this.float2 = (FsmFloat) null;
      this.operation = FloatOperator.Operation.Add;
      this.storeResult = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoFloatOperator();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoFloatOperator();
    }

    private void DoFloatOperator()
    {
      float a = this.float1.Value;
      float b = this.float2.Value;
      switch (this.operation)
      {
        case FloatOperator.Operation.Add:
          this.storeResult.Value = a + b;
          break;
        case FloatOperator.Operation.Subtract:
          this.storeResult.Value = a - b;
          break;
        case FloatOperator.Operation.Multiply:
          this.storeResult.Value = a * b;
          break;
        case FloatOperator.Operation.Divide:
          this.storeResult.Value = a / b;
          break;
        case FloatOperator.Operation.Min:
          this.storeResult.Value = Mathf.Min(a, b);
          break;
        case FloatOperator.Operation.Max:
          this.storeResult.Value = Mathf.Max(a, b);
          break;
      }
    }

    public enum Operation
    {
      Add,
      Subtract,
      Multiply,
      Divide,
      Min,
      Max,
    }
  }
}
