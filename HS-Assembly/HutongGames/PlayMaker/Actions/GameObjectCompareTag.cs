﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GameObjectCompareTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if a Game Object has a tag.")]
  [ActionCategory(ActionCategory.Logic)]
  public class GameObjectCompareTag : FsmStateAction
  {
    [Tooltip("The GameObject to test.")]
    [RequiredField]
    public FsmGameObject gameObject;
    [UIHint(UIHint.Tag)]
    [Tooltip("The Tag to check for.")]
    [RequiredField]
    public FsmString tag;
    [Tooltip("Event to send if the GameObject has the Tag.")]
    public FsmEvent trueEvent;
    [Tooltip("Event to send if the GameObject does not have the Tag.")]
    public FsmEvent falseEvent;
    [Tooltip("Store the result in a Bool variable.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmGameObject) null;
      this.tag = (FsmString) "Untagged";
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoCompareTag();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoCompareTag();
    }

    private void DoCompareTag()
    {
      bool flag = false;
      if ((Object) this.gameObject.Value != (Object) null)
        flag = this.gameObject.Value.CompareTag(this.tag.Value);
      this.storeResult.Value = flag;
      this.Fsm.Event(!flag ? this.falseEvent : this.trueEvent);
    }
  }
}
