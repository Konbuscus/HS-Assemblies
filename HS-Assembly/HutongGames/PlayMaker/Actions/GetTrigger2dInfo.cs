﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetTrigger2dInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets info on the last Trigger 2d event and store in variables.  See Unity and PlayMaker docs on Unity 2D physics.")]
  [ActionCategory(ActionCategory.Physics2D)]
  public class GetTrigger2dInfo : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("Get the GameObject hit.")]
    public FsmGameObject gameObjectHit;
    [UIHint(UIHint.Variable)]
    [Tooltip("The number of separate shaped regions in the collider.")]
    public FsmInt shapeCount;
    [UIHint(UIHint.Variable)]
    [Tooltip("Useful for triggering different effects. Audio, particles...")]
    public FsmString physics2dMaterialName;

    public override void Reset()
    {
      this.gameObjectHit = (FsmGameObject) null;
      this.shapeCount = (FsmInt) null;
      this.physics2dMaterialName = (FsmString) null;
    }

    private void StoreTriggerInfo()
    {
      if ((Object) this.Fsm.TriggerCollider2D == (Object) null)
        return;
      this.gameObjectHit.Value = this.Fsm.TriggerCollider2D.gameObject;
      this.shapeCount.Value = this.Fsm.TriggerCollider2D.shapeCount;
      this.physics2dMaterialName.Value = !((Object) this.Fsm.TriggerCollider2D.sharedMaterial != (Object) null) ? string.Empty : this.Fsm.TriggerCollider2D.sharedMaterial.name;
    }

    public override void OnEnter()
    {
      this.StoreTriggerInfo();
      this.Finish();
    }
  }
}
