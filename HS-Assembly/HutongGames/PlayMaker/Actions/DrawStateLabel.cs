﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DrawStateLabel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Draws a state label for this FSM in the Game View. The label is drawn on the game object that owns the FSM. Use this to override the global setting in the PlayMaker Debug menu.")]
  [ActionCategory(ActionCategory.Debug)]
  public class DrawStateLabel : FsmStateAction
  {
    [Tooltip("Set to True to show State labels, or Fals to hide them.")]
    [RequiredField]
    public FsmBool showLabel;

    public override void Reset()
    {
      this.showLabel = (FsmBool) true;
    }

    public override void OnEnter()
    {
      this.Fsm.ShowStateLabel = this.showLabel.Value;
      this.Finish();
    }
  }
}
