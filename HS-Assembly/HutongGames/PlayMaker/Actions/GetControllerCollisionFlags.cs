﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetControllerCollisionFlags
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the Collision Flags from a Character Controller on a Game Object. Collision flags give you a broad overview of where the character collided with any other object.")]
  [ActionCategory(ActionCategory.Character)]
  public class GetControllerCollisionFlags : FsmStateAction
  {
    [CheckForComponent(typeof (CharacterController))]
    [Tooltip("The GameObject with a Character Controller component.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("True if the Character Controller capsule is on the ground")]
    [UIHint(UIHint.Variable)]
    public FsmBool isGrounded;
    [UIHint(UIHint.Variable)]
    [Tooltip("True if no collisions in last move.")]
    public FsmBool none;
    [Tooltip("True if the Character Controller capsule was hit on the sides.")]
    [UIHint(UIHint.Variable)]
    public FsmBool sides;
    [Tooltip("True if the Character Controller capsule was hit from above.")]
    [UIHint(UIHint.Variable)]
    public FsmBool above;
    [Tooltip("True if the Character Controller capsule was hit from below.")]
    [UIHint(UIHint.Variable)]
    public FsmBool below;
    private GameObject previousGo;
    private CharacterController controller;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.isGrounded = (FsmBool) null;
      this.none = (FsmBool) null;
      this.sides = (FsmBool) null;
      this.above = (FsmBool) null;
      this.below = (FsmBool) null;
    }

    public override void OnUpdate()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((Object) ownerDefaultTarget != (Object) this.previousGo)
      {
        this.controller = ownerDefaultTarget.GetComponent<CharacterController>();
        this.previousGo = ownerDefaultTarget;
      }
      if (!((Object) this.controller != (Object) null))
        return;
      this.isGrounded.Value = this.controller.isGrounded;
      FsmBool none = this.none;
      int collisionFlags = (int) this.controller.collisionFlags;
      int num = 0;
      none.Value = num != 0;
      this.sides.Value = (this.controller.collisionFlags & CollisionFlags.Sides) != CollisionFlags.None;
      this.above.Value = (this.controller.collisionFlags & CollisionFlags.Above) != CollisionFlags.None;
      this.below.Value = (this.controller.collisionFlags & CollisionFlags.Below) != CollisionFlags.None;
    }
  }
}
