﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GameObjectIsChildOf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Tests if a GameObject is a Child of another GameObject.")]
  [ActionCategory(ActionCategory.Logic)]
  public class GameObjectIsChildOf : FsmStateAction
  {
    [RequiredField]
    [Tooltip("GameObject to test.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("Is it a child of this GameObject?")]
    [RequiredField]
    public FsmGameObject isChildOf;
    [Tooltip("Event to send if GameObject is a child.")]
    public FsmEvent trueEvent;
    [Tooltip("Event to send if GameObject is NOT a child.")]
    public FsmEvent falseEvent;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Store result in a bool variable")]
    public FsmBool storeResult;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.isChildOf = (FsmGameObject) null;
      this.trueEvent = (FsmEvent) null;
      this.falseEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
    }

    public override void OnEnter()
    {
      this.DoIsChildOf(this.Fsm.GetOwnerDefaultTarget(this.gameObject));
      this.Finish();
    }

    private void DoIsChildOf(GameObject go)
    {
      if ((Object) go == (Object) null || this.isChildOf == null)
        return;
      bool flag = go.transform.IsChildOf(this.isChildOf.Value.transform);
      this.storeResult.Value = flag;
      this.Fsm.Event(!flag ? this.falseEvent : this.trueEvent);
    }
  }
}
