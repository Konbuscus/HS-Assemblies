﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetHeroPowerAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Use the spell to find the hero power")]
  public class GetHeroPowerAction : FsmStateAction
  {
    public FsmGameObject m_HeroPowerGameObject;

    public override void Reset()
    {
      this.m_HeroPowerGameObject = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      Spell spell = this.Owner.gameObject.GetComponentInChildren<Spell>();
      if ((Object) spell == (Object) null)
      {
        spell = SceneUtils.FindComponentInThisOrParents<Spell>(this.Owner);
        if ((Object) spell == (Object) null)
        {
          this.Finish();
          return;
        }
      }
      if ((Object) spell == (Object) null)
      {
        Debug.LogWarning((object) "GetHeroPowerAction: spell is null!");
      }
      else
      {
        Card card = spell.GetSourceCard();
        if ((Object) card == (Object) null)
        {
          card = SceneUtils.FindComponentInThisOrParents<Actor>(this.Owner).GetCard();
          if ((Object) card == (Object) null)
          {
            Debug.LogWarning((object) "GetHeroPowerAction: card is null!");
            return;
          }
        }
        Card heroPowerCard = card.GetHeroPowerCard();
        if ((Object) heroPowerCard == (Object) null)
        {
          Debug.LogWarning((object) "GetHeroPowerAction: heroPowerCard is null!");
        }
        else
        {
          Actor actor = heroPowerCard.GetActor();
          if ((Object) actor == (Object) null)
          {
            Debug.LogWarning((object) "GetHeroPowerAction: heroPowerCardActor is null!");
          }
          else
          {
            GameObject gameObject = actor.gameObject;
            if (!this.m_HeroPowerGameObject.IsNone)
              this.m_HeroPowerGameObject.Value = gameObject;
            this.Finish();
          }
        }
      }
    }
  }
}
