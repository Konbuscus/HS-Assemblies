﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetTimeScaleAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Sets the global time scale.")]
  public class SetTimeScaleAction : FsmStateAction
  {
    public FsmFloat m_Scale;
    public bool m_EveryFrame;

    public override void Reset()
    {
      this.m_Scale = (FsmFloat) 1f;
      this.m_EveryFrame = false;
    }

    public override void OnEnter()
    {
      this.UpdateScale();
      if (this.m_EveryFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.UpdateScale();
    }

    private void UpdateScale()
    {
      if (this.m_Scale.IsNone)
        return;
      Time.timeScale = this.m_Scale.Value;
    }
  }
}
