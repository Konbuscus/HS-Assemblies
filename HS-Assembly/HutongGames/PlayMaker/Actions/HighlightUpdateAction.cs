﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.HighlightUpdateAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Used to control the state of the Pegasus Highlight system")]
  [ActionCategory("Pegasus")]
  public class HighlightUpdateAction : FsmStateAction
  {
    [Tooltip("GameObject to send highlight states to")]
    [RequiredField]
    public FsmOwnerDefault m_gameObj;
    private DelayedEvent delayedEvent;

    public override void Reset()
    {
      this.m_gameObj = (FsmOwnerDefault) null;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_gameObj);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        HighlightState[] componentsInChildren = ownerDefaultTarget.GetComponentsInChildren<HighlightState>();
        if (componentsInChildren == null)
        {
          this.Finish();
        }
        else
        {
          foreach (HighlightState highlightState in componentsInChildren)
            highlightState.ForceUpdate();
          this.Finish();
        }
      }
    }
  }
}
