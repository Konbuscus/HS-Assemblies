﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetMouseButtonDown
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Input)]
  [Tooltip("Sends an Event when the specified Mouse Button is pressed. Optionally store the button state in a bool variable.")]
  public class GetMouseButtonDown : FsmStateAction
  {
    [Tooltip("The mouse button to test.")]
    [RequiredField]
    public MouseButton button;
    [Tooltip("Event to send if the mouse button is down.")]
    public FsmEvent sendEvent;
    [Tooltip("Store the button state in a Bool Variable.")]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    [Tooltip("Uncheck to run when entering the state.")]
    public bool inUpdateOnly;

    public override void Reset()
    {
      this.button = MouseButton.Left;
      this.sendEvent = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.inUpdateOnly = true;
    }

    public override void OnEnter()
    {
      if (this.inUpdateOnly)
        return;
      this.DoGetMouseButtonDown();
    }

    public override void OnUpdate()
    {
      this.DoGetMouseButtonDown();
    }

    private void DoGetMouseButtonDown()
    {
      bool mouseButtonDown = Input.GetMouseButtonDown((int) this.button);
      if (mouseButtonDown)
        this.Fsm.Event(this.sendEvent);
      this.storeResult.Value = mouseButtonDown;
    }
  }
}
