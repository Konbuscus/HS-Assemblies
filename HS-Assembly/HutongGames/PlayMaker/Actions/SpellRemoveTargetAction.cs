﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellRemoveTargetAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Removes a target from a Spell.")]
  public class SpellRemoveTargetAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    public FsmGameObject m_TargetObject;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_spell == (Object) null)
        return;
      GameObject go = this.m_TargetObject.Value;
      if ((Object) go == (Object) null)
        return;
      this.m_spell.RemoveTarget(go);
      this.Finish();
    }
  }
}
