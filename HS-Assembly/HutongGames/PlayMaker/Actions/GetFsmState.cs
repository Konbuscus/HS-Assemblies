﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetFsmState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.StateMachine)]
  [ActionTarget(typeof (PlayMakerFSM), "fsmComponent", false)]
  [Tooltip("Gets the name of the specified FSMs current state. Either reference the fsm component directly, or find it on a game object.")]
  public class GetFsmState : FsmStateAction
  {
    [Tooltip("Drag a PlayMakerFSM component here.")]
    public PlayMakerFSM fsmComponent;
    [Tooltip("If not specifyng the component above, specify the GameObject that owns the FSM")]
    public FsmOwnerDefault gameObject;
    [UIHint(UIHint.FsmName)]
    [Tooltip("Optional name of Fsm on Game Object. If left blank it will find the first PlayMakerFSM on the GameObject.")]
    public FsmString fsmName;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("Store the state name in a string variable.")]
    public FsmString storeResult;
    [Tooltip("Repeat every frame. E.g.,  useful if you're waiting for the state to change.")]
    public bool everyFrame;
    private PlayMakerFSM fsm;

    public override void Reset()
    {
      this.fsmComponent = (PlayMakerFSM) null;
      this.gameObject = (FsmOwnerDefault) null;
      this.fsmName = (FsmString) string.Empty;
      this.storeResult = (FsmString) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetFsmState();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetFsmState();
    }

    private void DoGetFsmState()
    {
      if ((Object) this.fsm == (Object) null)
      {
        if ((Object) this.fsmComponent != (Object) null)
        {
          this.fsm = this.fsmComponent;
        }
        else
        {
          GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
          if ((Object) ownerDefaultTarget != (Object) null)
            this.fsm = ActionHelpers.GetGameObjectFsm(ownerDefaultTarget, this.fsmName.Value);
        }
        if ((Object) this.fsm == (Object) null)
        {
          this.storeResult.Value = string.Empty;
          return;
        }
      }
      this.storeResult.Value = this.fsm.ActiveStateName;
    }
  }
}
