﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetAudioLoop
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Audio)]
  [Tooltip("Sets looping on the AudioSource component on a Game Object.")]
  public class SetAudioLoop : ComponentAction<AudioSource>
  {
    [RequiredField]
    [CheckForComponent(typeof (AudioSource))]
    public FsmOwnerDefault gameObject;
    public FsmBool loop;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.loop = (FsmBool) false;
    }

    public override void OnEnter()
    {
      if (this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        this.audio.loop = this.loop.Value;
      this.Finish();
    }
  }
}
