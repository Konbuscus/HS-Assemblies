﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Comment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Debug)]
  [Tooltip("Adds a text area to the action list. NOTE: Doesn't do anything, just for notes...")]
  public class Comment : FsmStateAction
  {
    [UIHint(UIHint.Comment)]
    public string comment;

    public override void Reset()
    {
      this.comment = string.Empty;
    }

    public override void OnEnter()
    {
      this.Finish();
    }
  }
}
