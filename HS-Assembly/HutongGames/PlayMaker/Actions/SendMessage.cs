﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SendMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.ScriptControl)]
  [Tooltip("Sends a Message to a Game Object. See Unity docs for SendMessage.")]
  public class SendMessage : FsmStateAction
  {
    [Tooltip("GameObject that sends the message.")]
    [RequiredField]
    public FsmOwnerDefault gameObject;
    [Tooltip("Where to send the message.\nSee Unity docs.")]
    public SendMessage.MessageType delivery;
    [Tooltip("Send options.\nSee Unity docs.")]
    public SendMessageOptions options;
    [RequiredField]
    public FunctionCall functionCall;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.delivery = SendMessage.MessageType.SendMessage;
      this.options = SendMessageOptions.DontRequireReceiver;
      this.functionCall = (FunctionCall) null;
    }

    public override void OnEnter()
    {
      this.DoSendMessage();
      this.Finish();
    }

    private void DoSendMessage()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      object parameter = (object) null;
      string parameterType = this.functionCall.ParameterType;
      if (parameterType != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (SendMessage.\u003C\u003Ef__switch\u0024mapB4 == null)
        {
          // ISSUE: reference to a compiler-generated field
          SendMessage.\u003C\u003Ef__switch\u0024mapB4 = new Dictionary<string, int>(16)
          {
            {
              "None",
              0
            },
            {
              "bool",
              1
            },
            {
              "int",
              2
            },
            {
              "float",
              3
            },
            {
              "string",
              4
            },
            {
              "Vector2",
              5
            },
            {
              "Vector3",
              6
            },
            {
              "Rect",
              7
            },
            {
              "GameObject",
              8
            },
            {
              "Material",
              9
            },
            {
              "Texture",
              10
            },
            {
              "Color",
              11
            },
            {
              "Quaternion",
              12
            },
            {
              "Object",
              13
            },
            {
              "Enum",
              14
            },
            {
              "Array",
              15
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (SendMessage.\u003C\u003Ef__switch\u0024mapB4.TryGetValue(parameterType, out num))
        {
          switch (num)
          {
            case 1:
              parameter = (object) this.functionCall.BoolParameter.Value;
              break;
            case 2:
              parameter = (object) this.functionCall.IntParameter.Value;
              break;
            case 3:
              parameter = (object) this.functionCall.FloatParameter.Value;
              break;
            case 4:
              parameter = (object) this.functionCall.StringParameter.Value;
              break;
            case 5:
              parameter = (object) this.functionCall.Vector2Parameter.Value;
              break;
            case 6:
              parameter = (object) this.functionCall.Vector3Parameter.Value;
              break;
            case 7:
              parameter = (object) this.functionCall.RectParamater.Value;
              break;
            case 8:
              parameter = (object) this.functionCall.GameObjectParameter.Value;
              break;
            case 9:
              parameter = (object) this.functionCall.MaterialParameter.Value;
              break;
            case 10:
              parameter = (object) this.functionCall.TextureParameter.Value;
              break;
            case 11:
              parameter = (object) this.functionCall.ColorParameter.Value;
              break;
            case 12:
              parameter = (object) this.functionCall.QuaternionParameter.Value;
              break;
            case 13:
              parameter = (object) this.functionCall.ObjectParameter.Value;
              break;
            case 14:
              parameter = (object) this.functionCall.EnumParameter.get_Value();
              break;
            case 15:
              parameter = (object) this.functionCall.ArrayParameter.Values;
              break;
          }
        }
      }
      switch (this.delivery)
      {
        case SendMessage.MessageType.SendMessage:
          ownerDefaultTarget.SendMessage(this.functionCall.FunctionName, parameter, this.options);
          break;
        case SendMessage.MessageType.SendMessageUpwards:
          ownerDefaultTarget.SendMessageUpwards(this.functionCall.FunctionName, parameter, this.options);
          break;
        case SendMessage.MessageType.BroadcastMessage:
          ownerDefaultTarget.BroadcastMessage(this.functionCall.FunctionName, parameter, this.options);
          break;
      }
    }

    public enum MessageType
    {
      SendMessage,
      SendMessageUpwards,
      BroadcastMessage,
    }
  }
}
