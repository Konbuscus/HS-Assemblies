﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.QuaternionBaseAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  public abstract class QuaternionBaseAction : FsmStateAction
  {
    [Tooltip("Repeat every frame. Useful if any of the values are changing.")]
    public bool everyFrame;
    [Tooltip("Defines how to perform the action when 'every Frame' is enabled.")]
    public QuaternionBaseAction.everyFrameOptions everyFrameOption;

    public override void Awake()
    {
      if (!this.everyFrame || this.everyFrameOption != QuaternionBaseAction.everyFrameOptions.FixedUpdate)
        return;
      this.Fsm.HandleFixedUpdate = true;
    }

    public enum everyFrameOptions
    {
      Update,
      FixedUpdate,
      LateUpdate,
    }
  }
}
