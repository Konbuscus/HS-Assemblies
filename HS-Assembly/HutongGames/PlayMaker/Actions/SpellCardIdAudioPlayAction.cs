﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellCardIdAudioPlayAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Plays an audio clip dependent on a Spell's Card's ID.")]
  [ActionCategory("Pegasus Audio")]
  public class SpellCardIdAudioPlayAction : SpellCardIdAudioAction
  {
    public FsmOwnerDefault m_SpellObject;
    [Tooltip("Which Card to check on the Spell.")]
    public SpellAction.Which m_WhichCard;
    [CheckForComponent(typeof (AudioSource))]
    [Tooltip("The GameObject with the AudioSource component.")]
    public FsmOwnerDefault m_AudioSourceObject;
    [RequiredField]
    [CompoundArray("Clips", "Card Id", "Clip")]
    public string[] m_CardIds;
    public AudioClip[] m_Clips;
    public AudioClip m_DefaultClip;
    [HasFloatSlider(0.0f, 1f)]
    [Tooltip("Scales the volume of the AudioSource just for this Play call.")]
    public FsmFloat m_VolumeScale;
    [Tooltip("Wait for the Audio Source to finish playing before moving on.")]
    public FsmBool m_WaitForFinish;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void Reset()
    {
      this.m_SpellObject = (FsmOwnerDefault) null;
      this.m_WhichCard = SpellAction.Which.SOURCE;
      this.m_AudioSourceObject = (FsmOwnerDefault) null;
      this.m_CardIds = new string[2];
      this.m_Clips = new AudioClip[2];
      this.m_DefaultClip = (AudioClip) null;
      this.m_VolumeScale = (FsmFloat) 1f;
      this.m_WaitForFinish = (FsmBool) false;
    }

    public override void OnEnter()
    {
      base.OnEnter();
      AudioSource audioSource = this.GetAudioSource(this.m_AudioSourceObject);
      if ((Object) audioSource == (Object) null)
      {
        this.Finish();
      }
      else
      {
        AudioClip clipMatchingCardId = this.GetClipMatchingCardId(this.m_WhichCard, this.m_CardIds, this.m_Clips, this.m_DefaultClip);
        if ((Object) clipMatchingCardId == (Object) null)
        {
          this.Finish();
        }
        else
        {
          if (this.m_VolumeScale.IsNone)
            SoundManager.Get().PlayOneShot(audioSource, clipMatchingCardId, 1f, true);
          else
            SoundManager.Get().PlayOneShot(audioSource, clipMatchingCardId, this.m_VolumeScale.Value, true);
          if (this.m_WaitForFinish.Value && SoundManager.Get().IsActive(audioSource))
            return;
          this.Finish();
        }
      }
    }

    public override void OnUpdate()
    {
      if (this.m_WaitForFinish.Value && SoundManager.Get().IsActive(this.GetAudioSource(this.m_AudioSourceObject)))
        return;
      this.Finish();
    }
  }
}
