﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayResize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Resize an array.")]
  [ActionCategory(ActionCategory.Array)]
  public class ArrayResize : FsmStateAction
  {
    [Tooltip("The Array Variable to resize")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmArray array;
    [Tooltip("The new size of the array.")]
    public FsmInt newSize;
    [Tooltip("The event to trigger if the new size is out of range")]
    public FsmEvent sizeOutOfRangeEvent;

    public override void OnEnter()
    {
      if (this.newSize.Value >= 0)
      {
        this.array.Resize(this.newSize.Value);
      }
      else
      {
        this.LogError("Size out of range: " + (object) this.newSize.Value);
        this.Fsm.Event(this.sizeOutOfRangeEvent);
      }
      this.Finish();
    }
  }
}
