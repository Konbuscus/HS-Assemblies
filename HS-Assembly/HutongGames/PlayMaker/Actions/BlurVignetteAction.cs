﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.BlurVignetteAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Trigger blur+vignette effects action.")]
  [ActionCategory("Pegasus")]
  public class BlurVignetteAction : FsmStateAction
  {
    public FsmFloat m_vignetteVal = (FsmFloat) 0.8f;
    [Tooltip("Blur or Vignette time that it takes to get to full value")]
    public FsmFloat m_blurTime;
    public FsmBool m_isBlurred;
    public BlurVignetteAction.ActionType m_actionType;

    public override void OnEnter()
    {
      if (this.m_isBlurred.Value)
      {
        switch (this.m_actionType)
        {
          case BlurVignetteAction.ActionType.Start:
            FullScreenFXMgr.Get().StartStandardBlurVignette(this.m_blurTime.Value);
            break;
          case BlurVignetteAction.ActionType.End:
            FullScreenFXMgr.Get().EndStandardBlurVignette(this.m_blurTime.Value, (FullScreenFXMgr.EffectListener) null);
            break;
        }
      }
      else
      {
        switch (this.m_actionType)
        {
          case BlurVignetteAction.ActionType.Start:
            FullScreenFXMgr.Get().Vignette(this.m_vignetteVal.Value, this.m_blurTime.Value, iTween.EaseType.easeInOutCubic, (FullScreenFXMgr.EffectListener) null);
            break;
          case BlurVignetteAction.ActionType.End:
            FullScreenFXMgr.Get().StopVignette(this.m_blurTime.Value, iTween.EaseType.easeInOutCubic, (FullScreenFXMgr.EffectListener) null);
            break;
        }
      }
      this.Finish();
    }

    public enum ActionType
    {
      Start,
      End,
    }
  }
}
