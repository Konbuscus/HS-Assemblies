﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FindGameObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Finds a Game Object by Name and/or Tag.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class FindGameObject : FsmStateAction
  {
    [Tooltip("The name of the GameObject to find. You can leave this empty if you specify a Tag.")]
    public FsmString objectName;
    [Tooltip("Find a GameObject with this tag. If Object Name is specified then both name and Tag must match.")]
    [UIHint(UIHint.Tag)]
    public FsmString withTag;
    [UIHint(UIHint.Variable)]
    [RequiredField]
    [Tooltip("Store the result in a GameObject variable.")]
    public FsmGameObject store;

    public override void Reset()
    {
      this.objectName = (FsmString) string.Empty;
      this.withTag = (FsmString) "Untagged";
      this.store = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      this.Find();
      this.Finish();
    }

    private void Find()
    {
      if (this.withTag.Value != "Untagged")
      {
        if (!string.IsNullOrEmpty(this.objectName.Value))
        {
          foreach (GameObject gameObject in GameObject.FindGameObjectsWithTag(this.withTag.Value))
          {
            if (gameObject.name == this.objectName.Value)
            {
              this.store.Value = gameObject;
              return;
            }
          }
          this.store.Value = (GameObject) null;
        }
        else
          this.store.Value = GameObject.FindGameObjectWithTag(this.withTag.Value);
      }
      else
        this.store.Value = GameObject.Find(this.objectName.Value);
    }

    public override string ErrorCheck()
    {
      if (string.IsNullOrEmpty(this.objectName.Value) && string.IsNullOrEmpty(this.withTag.Value))
        return "Specify Name, Tag, or both.";
      return (string) null;
    }
  }
}
