﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioPlayClipAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Generates an AudioSource based on a template, then plays that source.")]
  [ActionCategory("Pegasus Audio")]
  public class AudioPlayClipAction : FsmStateAction
  {
    [Tooltip("Optional. If specified, the generated Audio Source will be placed at the same location as this object.")]
    public FsmOwnerDefault m_ParentObject;
    [ObjectType(typeof (AudioClip))]
    [RequiredField]
    public FsmObject m_Clip;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_Volume;
    [HasFloatSlider(-3f, 3f)]
    public FsmFloat m_Pitch;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_SpatialBlend;
    public SoundCategory m_Category;
    [Tooltip("If specified, this Audio Source will be used as a template for the generated Audio Source, otherwise the one in the SoundConfig will be the template.")]
    public AudioSource m_TemplateSource;

    public override void Reset()
    {
      this.m_ParentObject = (FsmOwnerDefault) null;
      this.m_Clip = (FsmObject) null;
      this.m_Volume = (FsmFloat) 1f;
      this.m_Pitch = (FsmFloat) 1f;
      this.m_SpatialBlend = (FsmFloat) 1f;
      this.m_Category = SoundCategory.FX;
      this.m_TemplateSource = (AudioSource) null;
    }

    public override void OnEnter()
    {
      if (this.m_Clip == null)
      {
        this.Finish();
      }
      else
      {
        SoundPlayClipArgs args = new SoundPlayClipArgs();
        args.m_templateSource = this.m_TemplateSource;
        args.m_clip = this.m_Clip.Value as AudioClip;
        if (!this.m_Volume.IsNone)
          args.m_volume = new float?(this.m_Volume.Value);
        if (!this.m_Pitch.IsNone)
          args.m_pitch = new float?(this.m_Pitch.Value);
        if (!this.m_SpatialBlend.IsNone)
          args.m_spatialBlend = new float?(this.m_SpatialBlend.Value);
        if (this.m_Category != SoundCategory.NONE)
          args.m_category = new SoundCategory?(this.m_Category);
        args.m_parentObject = this.Fsm.GetOwnerDefaultTarget(this.m_ParentObject);
        SoundManager.Get().PlayClip(args);
        this.Finish();
      }
    }
  }
}
