﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ArrayClear
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets all items in an Array to their default value: 0, empty string, false, or null depending on their type. Optionally defines a reset value to use.")]
  [ActionCategory(ActionCategory.Array)]
  public class ArrayClear : FsmStateAction
  {
    [Tooltip("The Array Variable to clear.")]
    [UIHint(UIHint.Variable)]
    public FsmArray array;
    [Tooltip("Optional reset value. Leave as None for default value.")]
    [MatchElementType("array")]
    public FsmVar resetValue;

    public override void Reset()
    {
      this.array = (FsmArray) null;
      this.resetValue = new FsmVar() { useVariable = true };
    }

    public override void OnEnter()
    {
      int length = this.array.Length;
      this.array.Reset();
      this.array.Resize(length);
      if (!this.resetValue.IsNone)
      {
        object obj = this.resetValue.GetValue();
        for (int index = 0; index < length; ++index)
          this.array.Set(index, obj);
      }
      this.Finish();
    }
  }
}
