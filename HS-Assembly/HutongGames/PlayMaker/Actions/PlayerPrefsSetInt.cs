﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.PlayerPrefsSetInt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the value of the preference identified by key.")]
  [ActionCategory("PlayerPrefs")]
  public class PlayerPrefsSetInt : FsmStateAction
  {
    [CompoundArray("Count", "Key", "Value")]
    [Tooltip("Case sensitive key.")]
    public FsmString[] keys;
    public FsmInt[] values;

    public override void Reset()
    {
      this.keys = new FsmString[1];
      this.values = new FsmInt[1];
    }

    public override void OnEnter()
    {
      for (int index = 0; index < this.keys.Length; ++index)
      {
        if (!this.keys[index].IsNone || !this.keys[index].Value.Equals(string.Empty))
          PlayerPrefs.SetInt(this.keys[index].Value, !this.values[index].IsNone ? this.values[index].Value : 0);
      }
      this.Finish();
    }
  }
}
