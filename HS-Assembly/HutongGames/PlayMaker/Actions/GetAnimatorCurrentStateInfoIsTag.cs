﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Does tag match the tag of the active state in the statemachine")]
  public class GetAnimatorCurrentStateInfoIsTag : FsmStateActionAnimatorBase
  {
    [RequiredField]
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The target. An Animator component is required")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The layer's index")]
    [RequiredField]
    public FsmInt layerIndex;
    [Tooltip("The tag to check the layer against.")]
    public FsmString tag;
    [ActionSection("Results")]
    [UIHint(UIHint.Variable)]
    [Tooltip("True if tag matches")]
    public FsmBool tagMatch;
    [Tooltip("Event send if tag matches")]
    public FsmEvent tagMatchEvent;
    [Tooltip("Event send if tag matches")]
    public FsmEvent tagDoNotMatchEvent;
    private Animator _animator;

    public override void Reset()
    {
      base.Reset();
      this.gameObject = (FsmOwnerDefault) null;
      this.layerIndex = (FsmInt) null;
      this.tag = (FsmString) null;
      this.tagMatch = (FsmBool) null;
      this.tagMatchEvent = (FsmEvent) null;
      this.tagDoNotMatchEvent = (FsmEvent) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
        {
          this.Finish();
        }
        else
        {
          this.IsTag();
          if (this.everyFrame)
            return;
          this.Finish();
        }
      }
    }

    public override void OnActionUpdate()
    {
      this.IsTag();
    }

    private void IsTag()
    {
      if (!((Object) this._animator != (Object) null))
        return;
      if (this._animator.GetCurrentAnimatorStateInfo(this.layerIndex.Value).IsTag(this.tag.Value))
      {
        this.tagMatch.Value = true;
        this.Fsm.Event(this.tagMatchEvent);
      }
      else
      {
        this.tagMatch.Value = false;
        this.Fsm.Event(this.tagDoNotMatchEvent);
      }
    }
  }
}
