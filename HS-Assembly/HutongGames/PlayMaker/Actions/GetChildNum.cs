﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetChildNum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GameObject)]
  [Tooltip("Gets the Child of a GameObject by Index.\nE.g., O to get the first child. HINT: Use this with an integer variable to iterate through children.")]
  public class GetChildNum : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject to search.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("The index of the child to find.")]
    [RequiredField]
    public FsmInt childIndex;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the child in a GameObject variable.")]
    [RequiredField]
    public FsmGameObject store;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.childIndex = (FsmInt) 0;
      this.store = (FsmGameObject) null;
    }

    public override void OnEnter()
    {
      this.store.Value = this.DoGetChildNum(this.Fsm.GetOwnerDefaultTarget(this.gameObject));
      this.Finish();
    }

    private GameObject DoGetChildNum(GameObject go)
    {
      if ((Object) go == (Object) null)
        return (GameObject) null;
      return go.transform.GetChild(this.childIndex.Value % go.transform.childCount).gameObject;
    }
  }
}
