﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.Vector2ClampMagnitude
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Clamps the Magnitude of Vector2 Variable.")]
  [ActionCategory(ActionCategory.Vector2)]
  public class Vector2ClampMagnitude : FsmStateAction
  {
    [Tooltip("The Vector2")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmVector2 vector2Variable;
    [Tooltip("The maximum Magnitude")]
    [RequiredField]
    public FsmFloat maxLength;
    [Tooltip("Repeat every frame")]
    public bool everyFrame;

    public override void Reset()
    {
      this.vector2Variable = (FsmVector2) null;
      this.maxLength = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoVector2ClampMagnitude();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoVector2ClampMagnitude();
    }

    private void DoVector2ClampMagnitude()
    {
      this.vector2Variable.Value = Vector2.ClampMagnitude(this.vector2Variable.Value, this.maxLength.Value);
    }
  }
}
