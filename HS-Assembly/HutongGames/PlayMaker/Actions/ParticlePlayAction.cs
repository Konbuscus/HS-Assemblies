﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.ParticlePlayAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Play a Particle System. mschweitzer: I think this is equivalent to Simulate with a 1.0 speed.")]
  public class ParticlePlayAction : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault m_GameObject;
    [Tooltip("Run this action on all child objects' Particle Systems.")]
    public FsmBool m_IncludeChildren;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_IncludeChildren = (FsmBool) false;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        ParticleSystem component = ownerDefaultTarget.GetComponent<ParticleSystem>();
        if ((Object) component == (Object) null && !this.m_IncludeChildren.Value)
        {
          Debug.LogWarning((object) string.Format("ParticlePlayAction.OnEnter() - {0} has no ParticleSystem component. Owner={1}", (object) ownerDefaultTarget, (object) this.Owner));
          this.Finish();
        }
        else if ((Object) component == (Object) null && this.m_IncludeChildren.Value)
        {
          foreach (ParticleSystem componentsInChild in ownerDefaultTarget.GetComponentsInChildren<ParticleSystem>())
            componentsInChild.Play(this.m_IncludeChildren.Value);
          this.Finish();
        }
        else
        {
          component.Play(this.m_IncludeChildren.Value);
          this.Finish();
        }
      }
    }
  }
}
