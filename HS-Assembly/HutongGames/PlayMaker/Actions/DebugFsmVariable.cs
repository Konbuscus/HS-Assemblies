﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DebugFsmVariable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Debug)]
  [Tooltip("Print the value of any FSM Variable in the PlayMaker Log Window.")]
  public class DebugFsmVariable : BaseLogAction
  {
    [Tooltip("Info, Warning, or Error.")]
    public LogLevel logLevel;
    [UIHint(UIHint.Variable)]
    [Tooltip("The variable to debug.")]
    [HideTypeFilter]
    public FsmVar variable;

    public override void Reset()
    {
      this.logLevel = LogLevel.Info;
      this.variable = (FsmVar) null;
      base.Reset();
    }

    public override void OnEnter()
    {
      ActionHelpers.DebugLog(this.Fsm, this.logLevel, this.variable.DebugString(), this.sendToUnityLog);
      this.Finish();
    }
  }
}
