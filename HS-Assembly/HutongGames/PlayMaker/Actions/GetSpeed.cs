﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetSpeed
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Physics)]
  [Tooltip("Gets the Speed of a Game Object and stores it in a Float Variable. NOTE: The Game Object must have a rigid body.")]
  public class GetSpeed : ComponentAction<Rigidbody>
  {
    [RequiredField]
    [CheckForComponent(typeof (Rigidbody))]
    [Tooltip("The GameObject with a Rigidbody.")]
    public FsmOwnerDefault gameObject;
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the speed in a float variable.")]
    public FsmFloat storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.storeResult = (FsmFloat) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoGetSpeed();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoGetSpeed();
    }

    private void DoGetSpeed()
    {
      if (this.storeResult == null || !this.UpdateCache(this.gameObject.OwnerOption != OwnerDefaultOption.UseOwner ? this.gameObject.GameObject.Value : this.Owner))
        return;
      this.storeResult.Value = this.rigidbody.velocity.magnitude;
    }
  }
}
