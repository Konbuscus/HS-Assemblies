﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetHaloStrength
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the size of light halos.")]
  [ActionCategory(ActionCategory.RenderSettings)]
  public class SetHaloStrength : FsmStateAction
  {
    [RequiredField]
    public FsmFloat haloStrength;
    public bool everyFrame;

    public override void Reset()
    {
      this.haloStrength = (FsmFloat) 0.5f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetHaloStrength();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetHaloStrength();
    }

    private void DoSetHaloStrength()
    {
      RenderSettings.haloStrength = this.haloStrength.Value;
    }
  }
}
