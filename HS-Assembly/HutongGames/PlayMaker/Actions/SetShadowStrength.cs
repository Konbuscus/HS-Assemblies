﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetShadowStrength
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Lights)]
  [Tooltip("Sets the strength of the shadows cast by a Light.")]
  public class SetShadowStrength : ComponentAction<Light>
  {
    [RequiredField]
    [CheckForComponent(typeof (Light))]
    public FsmOwnerDefault gameObject;
    public FsmFloat shadowStrength;
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.shadowStrength = (FsmFloat) 0.8f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetShadowStrength();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetShadowStrength();
    }

    private void DoSetShadowStrength()
    {
      if (!this.UpdateCache(this.Fsm.GetOwnerDefaultTarget(this.gameObject)))
        return;
      this.light.shadowStrength = this.shadowStrength.Value;
    }
  }
}
