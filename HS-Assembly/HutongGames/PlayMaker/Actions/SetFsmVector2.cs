﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetFsmVector2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Set the value of a Vector2 Variable in another FSM.")]
  [ActionTarget(typeof (PlayMakerFSM), "gameObject,fsmName", false)]
  [ActionCategory(ActionCategory.StateMachine)]
  public class SetFsmVector2 : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject that owns the FSM.")]
    public FsmOwnerDefault gameObject;
    [Tooltip("Optional name of FSM on Game Object")]
    [UIHint(UIHint.FsmName)]
    public FsmString fsmName;
    [RequiredField]
    [Tooltip("The name of the FSM variable.")]
    [UIHint(UIHint.FsmVector2)]
    public FsmString variableName;
    [Tooltip("Set the value of the variable.")]
    [RequiredField]
    public FsmVector2 setValue;
    [Tooltip("Repeat every frame. Useful if the value is changing.")]
    public bool everyFrame;
    private GameObject goLastFrame;
    private string fsmNameLastFrame;
    private PlayMakerFSM fsm;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
      this.fsmName = (FsmString) string.Empty;
      this.setValue = (FsmVector2) null;
    }

    public override void OnEnter()
    {
      this.DoSetFsmVector2();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    private void DoSetFsmVector2()
    {
      if (this.setValue == null)
        return;
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      if ((Object) ownerDefaultTarget != (Object) this.goLastFrame || this.fsmName.Value != this.fsmNameLastFrame)
      {
        this.goLastFrame = ownerDefaultTarget;
        this.fsmNameLastFrame = this.fsmName.Value;
        this.fsm = ActionHelpers.GetGameObjectFsm(ownerDefaultTarget, this.fsmName.Value);
      }
      if ((Object) this.fsm == (Object) null)
      {
        this.LogWarning("Could not find FSM: " + this.fsmName.Value);
      }
      else
      {
        FsmVector2 fsmVector2 = this.fsm.FsmVariables.GetFsmVector2(this.variableName.Value);
        if (fsmVector2 != null)
          fsmVector2.Value = this.setValue.Value;
        else
          this.LogWarning("Could not find variable: " + this.variableName.Value);
      }
    }

    public override void OnUpdate()
    {
      this.DoSetFsmVector2();
    }
  }
}
