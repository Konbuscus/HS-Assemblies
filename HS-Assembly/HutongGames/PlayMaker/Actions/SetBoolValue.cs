﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetBoolValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Math)]
  [Tooltip("Sets the value of a Bool Variable.")]
  public class SetBoolValue : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [RequiredField]
    public FsmBool boolVariable;
    [RequiredField]
    public FsmBool boolValue;
    public bool everyFrame;

    public override void Reset()
    {
      this.boolVariable = (FsmBool) null;
      this.boolValue = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.boolVariable.Value = this.boolValue.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.boolVariable.Value = this.boolValue.Value;
    }
  }
}
