﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AnimatorPlayAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Enables an Animator and plays one of its states.")]
  public class AnimatorPlayAction : FsmStateAction
  {
    [CheckForComponent(typeof (Animator))]
    [RequiredField]
    [Tooltip("Game Object to play the animation on.")]
    public FsmOwnerDefault m_GameObject;
    public FsmString m_StateName;
    public FsmString m_LayerName;
    [HasFloatSlider(0.0f, 100f)]
    [Tooltip("Percent of time into the animation at which to start playing.")]
    public FsmFloat m_StartTimePercent;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_StateName = (FsmString) null;
      FsmString fsmString = new FsmString();
      fsmString.UseVariable = true;
      this.m_LayerName = fsmString;
      this.m_StartTimePercent = (FsmFloat) 0.0f;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if (!(bool) ((Object) ownerDefaultTarget))
      {
        this.Finish();
      }
      else
      {
        Animator component = ownerDefaultTarget.GetComponent<Animator>();
        if ((bool) ((Object) component))
        {
          int layer = -1;
          if (!this.m_LayerName.IsNone)
            layer = AnimationUtil.GetLayerIndexFromName(component, this.m_LayerName.Value);
          float normalizedTime = float.NegativeInfinity;
          if (!this.m_StartTimePercent.IsNone)
            normalizedTime = 0.01f * this.m_StartTimePercent.Value;
          component.enabled = true;
          component.Play(this.m_StateName.Value, layer, normalizedTime);
        }
        this.Finish();
      }
    }
  }
}
