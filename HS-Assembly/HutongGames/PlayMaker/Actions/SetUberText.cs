﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetUberText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus")]
  [Tooltip("Sets the text on an UberText object.")]
  public class SetUberText : FsmStateAction
  {
    [RequiredField]
    public FsmOwnerDefault uberTextObject;
    public string text;

    public override void Reset()
    {
      this.uberTextObject = (FsmOwnerDefault) null;
      this.text = string.Empty;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.uberTextObject);
      if ((Object) ownerDefaultTarget != (Object) null)
      {
        UberText component = ownerDefaultTarget.GetComponent<UberText>();
        if ((Object) component != (Object) null)
          component.Text = this.text;
      }
      this.Finish();
    }
  }
}
