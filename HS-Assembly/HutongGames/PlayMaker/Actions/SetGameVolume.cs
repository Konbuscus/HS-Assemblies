﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetGameVolume
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the global sound volume.")]
  [ActionCategory(ActionCategory.Audio)]
  public class SetGameVolume : FsmStateAction
  {
    [HasFloatSlider(0.0f, 1f)]
    [RequiredField]
    public FsmFloat volume;
    public bool everyFrame;

    public override void Reset()
    {
      this.volume = (FsmFloat) 1f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      AudioListener.volume = this.volume.Value;
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      AudioListener.volume = this.volume.Value;
    }
  }
}
