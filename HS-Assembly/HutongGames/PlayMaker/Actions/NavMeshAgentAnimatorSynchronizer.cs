﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Animator)]
  [Tooltip("Synchronize a NavMesh Agent velocity and rotation with the animator process.")]
  public class NavMeshAgentAnimatorSynchronizer : FsmStateAction
  {
    [RequiredField]
    [CheckForComponent(typeof (NavMeshAgent))]
    [CheckForComponent(typeof (Animator))]
    [Tooltip("The Agent target. An Animator component and a NavMeshAgent component are required")]
    public FsmOwnerDefault gameObject;
    private Animator _animator;
    private NavMeshAgent _agent;
    private Transform _trans;

    public override void Reset()
    {
      this.gameObject = (FsmOwnerDefault) null;
    }

    public override void OnPreprocess()
    {
      this.Fsm.HandleAnimatorMove = true;
    }

    public override void OnEnter()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.gameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
      {
        this.Finish();
      }
      else
      {
        this._agent = ownerDefaultTarget.GetComponent<NavMeshAgent>();
        this._animator = ownerDefaultTarget.GetComponent<Animator>();
        if ((Object) this._animator == (Object) null)
          this.Finish();
        else
          this._trans = ownerDefaultTarget.transform;
      }
    }

    public override void DoAnimatorMove()
    {
      this._agent.velocity = this._animator.deltaPosition / Time.deltaTime;
      this._trans.rotation = this._animator.rootRotation;
    }
  }
}
