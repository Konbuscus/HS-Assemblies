﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioSetRandomVolumeAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory("Pegasus Audio")]
  [Tooltip("Randomly sets the volume of an AudioSource on a Game Object.")]
  public class AudioSetRandomVolumeAction : FsmStateAction
  {
    [CheckForComponent(typeof (AudioSource))]
    [RequiredField]
    public FsmOwnerDefault m_GameObject;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_MinVolume;
    [HasFloatSlider(0.0f, 1f)]
    public FsmFloat m_MaxVolume;
    public bool m_EveryFrame;
    private float m_volume;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_MinVolume = (FsmFloat) 1f;
      this.m_MaxVolume = (FsmFloat) 1f;
      this.m_EveryFrame = false;
    }

    public override void OnEnter()
    {
      this.UpdateVolume();
      if (this.m_EveryFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.UpdateVolume();
    }

    private void ChooseVolume()
    {
      this.m_volume = Random.Range(!this.m_MinVolume.IsNone ? this.m_MinVolume.Value : 1f, !this.m_MaxVolume.IsNone ? this.m_MaxVolume.Value : 1f);
    }

    private void UpdateVolume()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return;
      AudioSource component = ownerDefaultTarget.GetComponent<AudioSource>();
      if ((Object) component == (Object) null)
        return;
      SoundManager.Get().SetVolume(component, this.m_volume);
    }
  }
}
