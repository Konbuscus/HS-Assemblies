﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetEnumValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Enum)]
  [Tooltip("Sets the value of an Enum Variable.")]
  public class SetEnumValue : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("The Enum Variable to set.")]
    public FsmEnum enumVariable;
    [Tooltip("The Enum value to set the variable to.")]
    [MatchFieldType("enumVariable")]
    public FsmEnum enumValue;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.enumVariable = (FsmEnum) null;
      this.enumValue = (FsmEnum) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetEnumValue();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetEnumValue();
    }

    private void DoSetEnumValue()
    {
      this.enumVariable.set_Value(this.enumValue.get_Value());
    }
  }
}
