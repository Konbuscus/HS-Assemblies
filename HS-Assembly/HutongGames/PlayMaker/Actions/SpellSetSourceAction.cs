﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SpellSetSourceAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets the source for a Spell.")]
  [ActionCategory("Pegasus")]
  public class SpellSetSourceAction : SpellAction
  {
    public FsmOwnerDefault m_SpellObject;
    public FsmGameObject m_SourceObject;

    protected override GameObject GetSpellOwner()
    {
      return this.Fsm.GetOwnerDefaultTarget(this.m_SpellObject);
    }

    public override void OnEnter()
    {
      base.OnEnter();
      if ((Object) this.m_spell == (Object) null)
        return;
      this.m_spell.SetSource(this.m_SourceObject.Value);
      this.Finish();
    }
  }
}
