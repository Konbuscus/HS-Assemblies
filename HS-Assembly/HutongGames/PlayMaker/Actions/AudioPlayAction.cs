﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.AudioPlayAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Plays the Audio Source of a Game Object or plays a one shot clip. Waits for the audio to finish.")]
  [ActionCategory("Pegasus Audio")]
  public class AudioPlayAction : FsmStateAction
  {
    [CheckForComponent(typeof (AudioSource))]
    [RequiredField]
    [Tooltip("The GameObject with the AudioSource component.")]
    public FsmOwnerDefault m_GameObject;
    [HasFloatSlider(0.0f, 1f)]
    [Tooltip("Scales the volume of the AudioSource just for this Play call.")]
    public FsmFloat m_VolumeScale;
    [Tooltip("Optionally play a One Shot AudioClip.")]
    [ObjectType(typeof (AudioClip))]
    public FsmObject m_OneShotClip;
    [Tooltip("Event to send when the AudioSource finishes playing.")]
    public FsmEvent m_FinishedEvent;

    public override void Reset()
    {
      this.m_GameObject = (FsmOwnerDefault) null;
      this.m_VolumeScale = (FsmFloat) 1f;
      this.m_OneShotClip = (FsmObject) null;
    }

    public override void OnEnter()
    {
      AudioSource source = this.GetSource();
      if ((Object) source == (Object) null)
      {
        this.Fsm.Event(this.m_FinishedEvent);
        this.Finish();
      }
      else
      {
        AudioClip audioClip = this.m_OneShotClip.Value as AudioClip;
        if ((Object) audioClip == (Object) null)
        {
          if (!this.m_VolumeScale.IsNone)
            SoundManager.Get().SetVolume(source, this.m_VolumeScale.Value);
          SoundManager.Get().Play(source, true);
        }
        else
        {
          SoundPlayClipArgs args = new SoundPlayClipArgs();
          args.m_templateSource = source;
          args.m_clip = audioClip;
          if (!this.m_VolumeScale.IsNone)
            args.m_volume = new float?(this.m_VolumeScale.Value);
          args.m_parentObject = source.gameObject;
          SoundManager.Get().PlayClip(args);
        }
        if (SoundManager.Get().IsActive(source))
          return;
        this.Fsm.Event(this.m_FinishedEvent);
        this.Finish();
      }
    }

    public override void OnUpdate()
    {
      if (SoundManager.Get().IsActive(this.GetSource()))
        return;
      this.Fsm.Event(this.m_FinishedEvent);
      this.Finish();
    }

    private AudioSource GetSource()
    {
      GameObject ownerDefaultTarget = this.Fsm.GetOwnerDefaultTarget(this.m_GameObject);
      if ((Object) ownerDefaultTarget == (Object) null)
        return (AudioSource) null;
      return ownerDefaultTarget.GetComponent<AudioSource>();
    }
  }
}
