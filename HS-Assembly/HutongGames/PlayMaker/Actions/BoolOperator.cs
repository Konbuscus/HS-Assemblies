﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.BoolOperator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Performs boolean operations on 2 Bool Variables.")]
  [ActionCategory(ActionCategory.Math)]
  public class BoolOperator : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The first Bool variable.")]
    public FsmBool bool1;
    [Tooltip("The second Bool variable.")]
    [RequiredField]
    public FsmBool bool2;
    [Tooltip("Boolean Operation.")]
    public BoolOperator.Operation operation;
    [Tooltip("Store the result in a Bool Variable.")]
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame while the state is active.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.bool1 = (FsmBool) false;
      this.bool2 = (FsmBool) false;
      this.operation = BoolOperator.Operation.AND;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoBoolOperator();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoBoolOperator();
    }

    private void DoBoolOperator()
    {
      bool flag1 = this.bool1.Value;
      bool flag2 = this.bool2.Value;
      switch (this.operation)
      {
        case BoolOperator.Operation.AND:
          this.storeResult.Value = flag1 && flag2;
          break;
        case BoolOperator.Operation.NAND:
          this.storeResult.Value = (!flag1 ? 0 : (flag2 ? 1 : 0)) == 0;
          break;
        case BoolOperator.Operation.OR:
          this.storeResult.Value = flag1 || flag2;
          break;
        case BoolOperator.Operation.XOR:
          this.storeResult.Value = flag1 ^ flag2;
          break;
      }
    }

    public enum Operation
    {
      AND,
      NAND,
      OR,
      XOR,
    }
  }
}
