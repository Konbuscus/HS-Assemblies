﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.SetMaterialFloatMultiAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Sets a named float in multiple game object's material.")]
  [ActionCategory("Pegasus")]
  public class SetMaterialFloatMultiAction : ComponentAction<Renderer>
  {
    [CheckForComponent(typeof (Renderer))]
    [Tooltip("The GameObjects that the material is applied to.")]
    public FsmGameObject[] gameObjectList;
    [RequiredField]
    [Tooltip("A named float parameter in the shader.")]
    public FsmString namedFloat;
    [RequiredField]
    [Tooltip("Set the parameter value.")]
    public FsmFloat floatValue;
    [Tooltip("Repeat every frame. Useful if the value is animated.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObjectList = (FsmGameObject[]) null;
      this.namedFloat = (FsmString) string.Empty;
      this.floatValue = (FsmFloat) 0.0f;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoSetMaterialFloat();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoSetMaterialFloat();
    }

    private void DoSetMaterialFloat()
    {
      for (int index = 0; index < this.gameObjectList.Length; ++index)
        this.gameObjectList[index].Value.GetComponent<Renderer>().material.SetFloat(this.namedFloat.Value, this.floatValue.Value);
    }
  }
}
