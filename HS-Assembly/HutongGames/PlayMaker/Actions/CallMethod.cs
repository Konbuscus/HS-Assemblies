﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.CallMethod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Call a method in a behaviour.")]
  [ActionCategory(ActionCategory.ScriptControl)]
  public class CallMethod : FsmStateAction
  {
    [Tooltip("Store the component in an Object variable.\nNOTE: Set theObject variable's Object Type to get a component of that type. E.g., set Object Type to UnityEngine.AudioListener to get the AudioListener component on the camera.")]
    [ObjectType(typeof (MonoBehaviour))]
    public FsmObject behaviour;
    [Tooltip("Name of the method to call on the component")]
    public FsmString methodName;
    [Tooltip("Method paramters. NOTE: these must match the method's signature!")]
    public FsmVar[] parameters;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result of the method call.")]
    [ActionSection("Store Result")]
    public FsmVar storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;
    private FsmObject cachedBehaviour;
    private FsmString cachedMethodName;
    private System.Type cachedType;
    private MethodInfo cachedMethodInfo;
    private ParameterInfo[] cachedParameterInfo;
    private object[] parametersArray;
    private string errorString;

    public override void Reset()
    {
      this.behaviour = (FsmObject) null;
      this.methodName = (FsmString) null;
      this.parameters = (FsmVar[]) null;
      this.storeResult = (FsmVar) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.parametersArray = new object[this.parameters.Length];
      this.DoMethodCall();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoMethodCall();
    }

    private void DoMethodCall()
    {
      if (this.behaviour.Value == (UnityEngine.Object) null)
        this.Finish();
      else if (this.NeedToUpdateCache() && !this.DoCache())
      {
        Debug.LogError((object) this.errorString);
        this.Finish();
      }
      else
      {
        object obj;
        if (this.cachedParameterInfo.Length == 0)
        {
          obj = this.cachedMethodInfo.Invoke((object) this.cachedBehaviour.Value, (object[]) null);
        }
        else
        {
          for (int index = 0; index < this.parameters.Length; ++index)
          {
            FsmVar parameter = this.parameters[index];
            parameter.UpdateValue();
            this.parametersArray[index] = parameter.GetValue();
          }
          obj = this.cachedMethodInfo.Invoke((object) this.cachedBehaviour.Value, this.parametersArray);
        }
        if (this.storeResult.IsNone)
          return;
        this.storeResult.SetValue(obj);
      }
    }

    private bool NeedToUpdateCache()
    {
      if (this.cachedBehaviour != null && this.cachedMethodName != null && (!(this.cachedBehaviour.Value != this.behaviour.Value) && !(this.cachedBehaviour.Name != this.behaviour.Name)) && !(this.cachedMethodName.Value != this.methodName.Value))
        return this.cachedMethodName.Name != this.methodName.Name;
      return true;
    }

    private bool DoCache()
    {
      this.errorString = string.Empty;
      this.cachedBehaviour = new FsmObject(this.behaviour);
      this.cachedMethodName = new FsmString(this.methodName);
      if (this.cachedBehaviour.Value == (UnityEngine.Object) null)
      {
        if (!this.behaviour.UsesVariable || Application.isPlaying)
          this.errorString += "Behaviour is invalid!\n";
        this.Finish();
        return false;
      }
      this.cachedType = ((object) this.behaviour.Value).GetType();
      List<System.Type> typeList = new List<System.Type>(this.parameters.Length);
      foreach (FsmVar parameter in this.parameters)
        typeList.Add(parameter.get_RealType());
      this.cachedMethodInfo = this.cachedType.GetMethod(this.methodName.Value, typeList.ToArray());
      if (this.cachedMethodInfo == null)
      {
        CallMethod callMethod = this;
        string str = callMethod.errorString + "Invalid Method Name or Parameters: " + this.methodName.Value + "\n";
        callMethod.errorString = str;
        this.Finish();
        return false;
      }
      this.cachedParameterInfo = this.cachedMethodInfo.GetParameters();
      return true;
    }

    public override string ErrorCheck()
    {
      if (Application.isPlaying)
        return this.errorString;
      this.errorString = string.Empty;
      if (!this.DoCache())
        return this.errorString;
      if (this.parameters.Length != this.cachedParameterInfo.Length)
        return "Parameter count does not match method.\nMethod has " + (object) this.cachedParameterInfo.Length + " parameters.\nYou specified " + (object) this.parameters.Length + " paramaters.";
      for (int index = 0; index < this.parameters.Length; ++index)
      {
        System.Type realType = this.parameters[index].get_RealType();
        System.Type parameterType = this.cachedParameterInfo[index].ParameterType;
        if (!object.ReferenceEquals((object) realType, (object) parameterType))
          return "Parameters do not match method signature.\nParameter " + (object) (index + 1) + " (" + (object) realType + ") should be of type: " + (object) parameterType;
      }
      if (object.ReferenceEquals((object) this.cachedMethodInfo.ReturnType, (object) typeof (void)))
      {
        if (!string.IsNullOrEmpty(this.storeResult.variableName))
          return "Method does not have return.\nSpecify 'none' in Store Result.";
      }
      else if (!object.ReferenceEquals((object) this.cachedMethodInfo.ReturnType, (object) this.storeResult.get_RealType()))
        return "Store Result is of the wrong type.\nIt should be of type: " + (object) this.cachedMethodInfo.ReturnType;
      return string.Empty;
    }
  }
}
