﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.FloatAdd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Math)]
  [Tooltip("Adds a value to a Float Variable.")]
  public class FloatAdd : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    [Tooltip("The Float variable to add to.")]
    public FsmFloat floatVariable;
    [Tooltip("Amount to add.")]
    [RequiredField]
    public FsmFloat add;
    [Tooltip("Repeat every frame while the state is active.")]
    public bool everyFrame;
    [Tooltip("Used with Every Frame. Adds the value over one second to make the operation frame rate independent.")]
    public bool perSecond;

    public override void Reset()
    {
      this.floatVariable = (FsmFloat) null;
      this.add = (FsmFloat) null;
      this.everyFrame = false;
      this.perSecond = false;
    }

    public override void OnEnter()
    {
      this.DoFloatAdd();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoFloatAdd();
    }

    private void DoFloatAdd()
    {
      if (!this.perSecond)
        this.floatVariable.Value += this.add.Value;
      else
        this.floatVariable.Value += this.add.Value * Time.deltaTime;
    }
  }
}
