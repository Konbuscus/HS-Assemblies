﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GetScreenWidth
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Gets the Width of the Screen in pixels.")]
  [ActionCategory(ActionCategory.Application)]
  public class GetScreenWidth : FsmStateAction
  {
    [RequiredField]
    [UIHint(UIHint.Variable)]
    public FsmFloat storeScreenWidth;

    public override void Reset()
    {
      this.storeScreenWidth = (FsmFloat) null;
    }

    public override void OnEnter()
    {
      this.storeScreenWidth.Value = (float) Screen.width;
      this.Finish();
    }
  }
}
