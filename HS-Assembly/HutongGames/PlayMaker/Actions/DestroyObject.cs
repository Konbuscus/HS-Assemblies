﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.DestroyObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [Tooltip("Destroys a Game Object.")]
  [ActionCategory(ActionCategory.GameObject)]
  public class DestroyObject : FsmStateAction
  {
    [RequiredField]
    [Tooltip("The GameObject to destroy.")]
    public FsmGameObject gameObject;
    [Tooltip("Optional delay before destroying the Game Object.")]
    [HasFloatSlider(0.0f, 5f)]
    public FsmFloat delay;
    [Tooltip("Detach children before destroying the Game Object.")]
    public FsmBool detachChildren;

    public override void Reset()
    {
      this.gameObject = (FsmGameObject) null;
      this.delay = (FsmFloat) 0.0f;
    }

    public override void OnEnter()
    {
      GameObject gameObject = this.gameObject.Value;
      if ((Object) gameObject != (Object) null)
      {
        if ((double) this.delay.Value <= 0.0)
          Object.Destroy((Object) gameObject);
        else
          Object.Destroy((Object) gameObject, this.delay.Value);
        if (this.detachChildren.Value)
          gameObject.transform.DetachChildren();
      }
      this.Finish();
    }

    public override void OnUpdate()
    {
    }
  }
}
