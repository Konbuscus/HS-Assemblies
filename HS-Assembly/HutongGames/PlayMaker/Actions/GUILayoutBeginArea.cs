﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GUILayoutBeginArea
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.GUILayout)]
  [Tooltip("Begin a GUILayout block of GUI controls in a fixed screen area. NOTE: Block must end with a corresponding GUILayoutEndArea.")]
  public class GUILayoutBeginArea : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    public FsmRect screenRect;
    public FsmFloat left;
    public FsmFloat top;
    public FsmFloat width;
    public FsmFloat height;
    public FsmBool normalized;
    public FsmString style;
    private Rect rect;

    public override void Reset()
    {
      this.screenRect = (FsmRect) null;
      this.left = (FsmFloat) 0.0f;
      this.top = (FsmFloat) 0.0f;
      this.width = (FsmFloat) 1f;
      this.height = (FsmFloat) 1f;
      this.normalized = (FsmBool) true;
      this.style = (FsmString) string.Empty;
    }

    public override void OnGUI()
    {
      this.rect = this.screenRect.IsNone ? new Rect() : this.screenRect.Value;
      if (!this.left.IsNone)
        this.rect.x = this.left.Value;
      if (!this.top.IsNone)
        this.rect.y = this.top.Value;
      if (!this.width.IsNone)
        this.rect.width = this.width.Value;
      if (!this.height.IsNone)
        this.rect.height = this.height.Value;
      if (this.normalized.Value)
      {
        this.rect.x *= (float) Screen.width;
        this.rect.width *= (float) Screen.width;
        this.rect.y *= (float) Screen.height;
        this.rect.height *= (float) Screen.height;
      }
      GUILayout.BeginArea(this.rect, GUIContent.none, (GUIStyle) this.style.Value);
    }
  }
}
