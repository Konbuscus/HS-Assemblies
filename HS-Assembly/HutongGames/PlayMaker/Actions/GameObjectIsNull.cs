﻿// Decompiled with JetBrains decompiler
// Type: HutongGames.PlayMaker.Actions.GameObjectIsNull
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
  [ActionCategory(ActionCategory.Logic)]
  [Tooltip("Tests if a GameObject Variable has a null value. E.g., If the FindGameObject action failed to find an object.")]
  public class GameObjectIsNull : FsmStateAction
  {
    [UIHint(UIHint.Variable)]
    [Tooltip("The GameObject variable to test.")]
    [RequiredField]
    public FsmGameObject gameObject;
    [Tooltip("Event to send if the GamObject is null.")]
    public FsmEvent isNull;
    [Tooltip("Event to send if the GamObject is NOT null.")]
    public FsmEvent isNotNull;
    [UIHint(UIHint.Variable)]
    [Tooltip("Store the result in a bool variable.")]
    public FsmBool storeResult;
    [Tooltip("Repeat every frame.")]
    public bool everyFrame;

    public override void Reset()
    {
      this.gameObject = (FsmGameObject) null;
      this.isNull = (FsmEvent) null;
      this.isNotNull = (FsmEvent) null;
      this.storeResult = (FsmBool) null;
      this.everyFrame = false;
    }

    public override void OnEnter()
    {
      this.DoIsGameObjectNull();
      if (this.everyFrame)
        return;
      this.Finish();
    }

    public override void OnUpdate()
    {
      this.DoIsGameObjectNull();
    }

    private void DoIsGameObjectNull()
    {
      bool flag = (Object) this.gameObject.Value == (Object) null;
      if (this.storeResult != null)
        this.storeResult.Value = flag;
      this.Fsm.Event(!flag ? this.isNotNull : this.isNull);
    }
  }
}
