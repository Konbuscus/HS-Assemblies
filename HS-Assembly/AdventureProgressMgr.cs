﻿// Decompiled with JetBrains decompiler
// Type: AdventureProgressMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System;
using System.Collections.Generic;
using UnityEngine;

public class AdventureProgressMgr
{
  private Map<int, AdventureMission.WingProgress> m_wingProgress = new Map<int, AdventureMission.WingProgress>();
  private Map<int, int> m_wingAckState = new Map<int, int>();
  private Map<int, AdventureMission> m_missions = new Map<int, AdventureMission>();
  private List<AdventureProgressMgr.AdventureProgressUpdatedListener> m_progressUpdatedListeners = new List<AdventureProgressMgr.AdventureProgressUpdatedListener>();
  private static AdventureProgressMgr s_instance;

  private AdventureProgressMgr()
  {
    this.LoadAdventureMissionsFromDBF();
  }

  public static AdventureProgressMgr Get()
  {
    return AdventureProgressMgr.s_instance;
  }

  public static void Init()
  {
    if (AdventureProgressMgr.s_instance != null)
      return;
    AdventureProgressMgr.s_instance = new AdventureProgressMgr();
    Network.Get().RegisterNetHandler((object) AdventureProgressResponse.PacketID.ID, new Network.NetHandler(AdventureProgressMgr.s_instance.OnAdventureProgress), (Network.TimeoutHandler) null);
    NetCache.Get().RegisterNewNoticesListener(new NetCache.DelNewNoticesListener(AdventureProgressMgr.s_instance.OnNewNotices));
    ApplicationMgr.Get().WillReset += new Action(AdventureProgressMgr.s_instance.WillReset);
  }

  public static void InitRequests()
  {
    Network.RequestAdventureProgress();
  }

  public bool RegisterProgressUpdatedListener(AdventureProgressMgr.AdventureProgressUpdatedCallback callback)
  {
    return this.RegisterProgressUpdatedListener(callback, (object) null);
  }

  public bool RegisterProgressUpdatedListener(AdventureProgressMgr.AdventureProgressUpdatedCallback callback, object userData)
  {
    if (callback == null)
      return false;
    AdventureProgressMgr.AdventureProgressUpdatedListener progressUpdatedListener = new AdventureProgressMgr.AdventureProgressUpdatedListener();
    progressUpdatedListener.SetCallback(callback);
    progressUpdatedListener.SetUserData(userData);
    if (this.m_progressUpdatedListeners.Contains(progressUpdatedListener))
      return false;
    this.m_progressUpdatedListeners.Add(progressUpdatedListener);
    return true;
  }

  public bool RemoveProgressUpdatedListener(AdventureProgressMgr.AdventureProgressUpdatedCallback callback)
  {
    return this.RemoveProgressUpdatedListener(callback, (object) null);
  }

  public bool RemoveProgressUpdatedListener(AdventureProgressMgr.AdventureProgressUpdatedCallback callback, object userData)
  {
    if (callback == null)
      return false;
    AdventureProgressMgr.AdventureProgressUpdatedListener progressUpdatedListener = new AdventureProgressMgr.AdventureProgressUpdatedListener();
    progressUpdatedListener.SetCallback(callback);
    progressUpdatedListener.SetUserData(userData);
    if (!this.m_progressUpdatedListeners.Contains(progressUpdatedListener))
      return false;
    this.m_progressUpdatedListeners.Remove(progressUpdatedListener);
    return true;
  }

  public List<AdventureMission.WingProgress> GetAllProgress()
  {
    return new List<AdventureMission.WingProgress>((IEnumerable<AdventureMission.WingProgress>) this.m_wingProgress.Values);
  }

  public AdventureMission.WingProgress GetProgress(int wing)
  {
    if (!this.m_wingProgress.ContainsKey(wing))
      return (AdventureMission.WingProgress) null;
    return this.m_wingProgress[wing];
  }

  public bool OwnsOneOrMoreAdventureWings(AdventureDbId adventureID)
  {
    using (List<WingDbfRecord>.Enumerator enumerator = GameDbf.Wing.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WingDbfRecord current = enumerator.Current;
        if ((AdventureDbId) current.AdventureId == adventureID && this.OwnsWing(current.ID))
          return true;
      }
    }
    return false;
  }

  public bool OwnsWing(int wing)
  {
    if (!this.m_wingProgress.ContainsKey(wing))
      return false;
    return this.m_wingProgress[wing].IsOwned();
  }

  public bool IsWingComplete(AdventureDbId adventureID, AdventureModeDbId modeID, WingDbId wingId)
  {
    using (List<ScenarioDbfRecord>.Enumerator enumerator = GameDbf.Scenario.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ScenarioDbfRecord current = enumerator.Current;
        if ((AdventureDbId) current.AdventureId == adventureID && (AdventureModeDbId) current.ModeId == modeID && ((WingDbId) current.WingId == wingId && !this.HasDefeatedScenario(current.ID)))
          return false;
      }
    }
    return true;
  }

  public bool IsAdventureModeComplete(AdventureDbId adventureID, AdventureModeDbId modeID)
  {
    using (List<ScenarioDbfRecord>.Enumerator enumerator = GameDbf.Scenario.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ScenarioDbfRecord current = enumerator.Current;
        if ((AdventureDbId) current.AdventureId == adventureID && (AdventureModeDbId) current.ModeId == modeID && !this.HasDefeatedScenario(current.ID))
          return false;
      }
    }
    return true;
  }

  public bool IsWingLocked(AdventureWingDef wingDef)
  {
    if (wingDef.GetWingId() == WingDbId.LOE_HALL_OF_EXPLORERS)
      return (!this.IsWingComplete(AdventureDbId.LOE, AdventureModeDbId.NORMAL, WingDbId.LOE_TEMPLE_OF_ORSIS) || !this.IsWingComplete(AdventureDbId.LOE, AdventureModeDbId.NORMAL, WingDbId.LOE_ULDAMAN) ? 0 : (this.IsWingComplete(AdventureDbId.LOE, AdventureModeDbId.NORMAL, WingDbId.LOE_RUINED_CITY) ? 1 : 0)) == 0;
    if (wingDef.GetOpenPrereqId() == WingDbId.INVALID)
      return false;
    int ack;
    AdventureProgressMgr.Get().GetWingAck((int) wingDef.GetOpenPrereqId(), out ack);
    return ack <= 0;
  }

  public int GetPlayableAdventureScenarios(AdventureDbId adventureID, AdventureModeDbId modeID)
  {
    List<WingDbfRecord> records1 = GameDbf.Wing.GetRecords();
    List<ScenarioDbfRecord> records2 = GameDbf.Scenario.GetRecords();
    int num = 0;
    using (List<WingDbfRecord>.Enumerator enumerator1 = records1.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        WingDbfRecord current1 = enumerator1.Current;
        int id = current1.ID;
        if ((AdventureDbId) current1.AdventureId == adventureID && this.OwnsWing(id) && this.IsWingOpen(id))
        {
          using (List<ScenarioDbfRecord>.Enumerator enumerator2 = records2.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              ScenarioDbfRecord current2 = enumerator2.Current;
              if (current1.ID == current2.WingId && (AdventureDbId) current2.AdventureId == adventureID && ((AdventureModeDbId) current2.ModeId == modeID && !AdventureProgressMgr.Get().HasDefeatedScenario(current2.ID)) && (current2.ModeId != 3 || AdventureProgressMgr.Get().CanPlayScenario(current2.ID)))
                ++num;
            }
          }
        }
      }
    }
    return num;
  }

  public int GetPlayableClassChallenges(AdventureDbId adventureID, AdventureModeDbId modeID)
  {
    int num = 0;
    using (List<ScenarioDbfRecord>.Enumerator enumerator = GameDbf.Scenario.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ScenarioDbfRecord current = enumerator.Current;
        if ((AdventureDbId) current.AdventureId == adventureID && (AdventureModeDbId) current.ModeId == modeID && (this.CanPlayScenario(current.ID) && !this.HasDefeatedScenario(current.ID)))
          ++num;
      }
    }
    return num;
  }

  public List<CardRewardData> GetCardRewardsForWing(int wing, HashSet<RewardVisualTiming> rewardTimings)
  {
    List<RewardData> forAdventureWing = AchieveManager.Get().GetRewardsForAdventureWing(wing, rewardTimings);
    List<CardRewardData> cardRewardDataList = new List<CardRewardData>();
    using (List<RewardData>.Enumerator enumerator = forAdventureWing.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RewardData current = enumerator.Current;
        if (current.RewardType == Reward.Type.CARD)
          cardRewardDataList.Add(current as CardRewardData);
      }
    }
    return cardRewardDataList;
  }

  public SpecialEventType GetWingOpenEvent(int wing)
  {
    WingDbfRecord record = GameDbf.Wing.GetRecord(wing);
    if (record == null)
    {
      Debug.LogWarning((object) string.Format("AdventureProgressMgr.GetWingOpenEvent could not find DBF record for wing {0}, assuming it is has no open event", (object) wing));
      return SpecialEventType.IGNORE;
    }
    string requiredEvent = record.RequiredEvent;
    SpecialEventType outVal;
    if (EnumUtils.TryGetEnum<SpecialEventType>(requiredEvent, out outVal))
      return outVal;
    Debug.LogWarning((object) string.Format("AdventureProgressMgr.GetWingOpenEvent wing={0} could not find SpecialEventType record for event '{1}'", (object) wing, (object) requiredEvent));
    return SpecialEventType.IGNORE;
  }

  public string GetWingName(int wing)
  {
    WingDbfRecord record = GameDbf.Wing.GetRecord(wing);
    if (record != null)
      return (string) record.Name;
    Debug.LogWarning((object) string.Format("AdventureProgressMgr.GetWingName could not find DBF record for wing {0}", (object) wing));
    return string.Empty;
  }

  public bool IsWingOpen(int wing)
  {
    return SpecialEventManager.Get().IsEventActive(this.GetWingOpenEvent(wing), false);
  }

  public bool CanPlayScenario(int scenarioID)
  {
    if (DemoMgr.Get().GetMode() == DemoMode.BLIZZCON_2015 && scenarioID != 1061)
      return false;
    if (!this.m_missions.ContainsKey(scenarioID))
      return true;
    AdventureMission mission = this.m_missions[scenarioID];
    if (!mission.HasRequiredProgress())
      return true;
    AdventureMission.WingProgress progress = this.GetProgress(mission.RequiredProgress.Wing);
    if (progress == null)
      return false;
    return progress.MeetsProgressAndFlagsRequirements(mission.RequiredProgress);
  }

  public bool HasDefeatedScenario(int scenarioID)
  {
    AdventureMission adventureMission;
    if (!this.m_missions.TryGetValue(scenarioID, out adventureMission) || adventureMission.RequiredProgress == null)
      return false;
    AdventureMission.WingProgress progress = this.GetProgress(adventureMission.RequiredProgress.Wing);
    if (progress == null)
      return false;
    return !GameUtils.IsHeroicAdventureMission(scenarioID) ? (!GameUtils.IsClassChallengeMission(scenarioID) ? progress.MeetsProgressRequirement(adventureMission.GrantedProgress.Progress) : progress.MeetsFlagsRequirement(adventureMission.GrantedProgress.Flags)) : progress.MeetsFlagsRequirement(adventureMission.GrantedProgress.Flags);
  }

  public List<CardRewardData> GetImmediateCardRewardsForDefeatingScenario(int scenarioID)
  {
    HashSet<RewardVisualTiming> rewardTimings = new HashSet<RewardVisualTiming>()
    {
      RewardVisualTiming.IMMEDIATE
    };
    return this.GetCardRewardsForDefeatingScenario(scenarioID, rewardTimings);
  }

  public List<CardRewardData> GetCardRewardsForDefeatingScenario(int scenarioID, HashSet<RewardVisualTiming> rewardTimings)
  {
    AdventureMission adventureMission;
    if (!this.m_missions.TryGetValue(scenarioID, out adventureMission))
      return new List<CardRewardData>();
    List<RewardData> rewardDataList = (List<RewardData>) null;
    if (GameUtils.IsHeroicAdventureMission(scenarioID))
      rewardDataList = AchieveManager.Get().GetRewardsForAdventureScenario(adventureMission.GrantedProgress.Wing, scenarioID, rewardTimings);
    else if (GameUtils.IsClassChallengeMission(scenarioID))
      rewardDataList = AchieveManager.Get().GetRewardsForAdventureScenario(adventureMission.GrantedProgress.Wing, scenarioID, rewardTimings);
    else if (adventureMission.GrantedProgress != null)
      rewardDataList = AchieveManager.Get().GetRewardsForAdventureScenario(adventureMission.GrantedProgress.Wing, scenarioID, rewardTimings);
    List<CardRewardData> cardRewardDataList = new List<CardRewardData>();
    if (rewardDataList != null)
    {
      using (List<RewardData>.Enumerator enumerator = rewardDataList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          RewardData current = enumerator.Current;
          if (current.RewardType == Reward.Type.CARD)
            cardRewardDataList.Add(current as CardRewardData);
        }
      }
    }
    return cardRewardDataList;
  }

  public bool SetWingAck(int wing, int ackId)
  {
    int num;
    if (this.m_wingAckState.TryGetValue(wing, out num))
    {
      if (ackId < num)
        return false;
      if (ackId == num)
        return true;
    }
    this.m_wingAckState[wing] = ackId;
    Network.AckWingProgress(wing, ackId);
    return true;
  }

  public bool GetWingAck(int wing, out int ack)
  {
    return this.m_wingAckState.TryGetValue(wing, out ack);
  }

  private void LoadAdventureMissionsFromDBF()
  {
    using (List<AdventureMissionDbfRecord>.Enumerator enumerator = GameDbf.AdventureMission.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureMissionDbfRecord current = enumerator.Current;
        int scenarioId = current.ScenarioId;
        if (this.m_missions.ContainsKey(scenarioId))
        {
          Debug.LogWarning((object) string.Format("AdventureProgressMgr.LoadAdventureMissionsFromDBF(): duplicate entry found for scenario ID {0}", (object) scenarioId));
        }
        else
        {
          string noteDesc = current.NoteDesc;
          AdventureMission.WingProgress requiredProgress = new AdventureMission.WingProgress(current.ReqWingId, current.ReqProgress, current.ReqFlags);
          AdventureMission.WingProgress grantedProgress = new AdventureMission.WingProgress(current.GrantsWingId, current.GrantsProgress, current.GrantsFlags);
          this.m_missions[scenarioId] = new AdventureMission(scenarioId, noteDesc, requiredProgress, grantedProgress);
        }
      }
    }
  }

  private void WillReset()
  {
    this.m_wingProgress.Clear();
    this.m_wingAckState.Clear();
    this.m_progressUpdatedListeners.Clear();
  }

  private void OnAdventureProgress()
  {
    using (List<Network.AdventureProgress>.Enumerator enumerator = Network.GetAdventureProgressResponse().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.AdventureProgress current = enumerator.Current;
        this.CreateOrUpdateProgress(true, current.Wing, current.Progress);
        this.CreateOrUpdateWingFlags(true, current.Wing, current.Flags);
        this.CreateOrUpdateWingAck(current.Wing, current.Ack);
      }
    }
  }

  private void OnNewNotices(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList)
  {
    List<long> longList = new List<long>();
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = newNotices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.ProfileNotice current = enumerator.Current;
        if (current.Type == NetCache.ProfileNotice.NoticeType.ADVENTURE_PROGRESS)
        {
          NetCache.ProfileNoticeAdventureProgress adventureProgress = current as NetCache.ProfileNoticeAdventureProgress;
          if (adventureProgress.Progress.HasValue)
            this.CreateOrUpdateProgress(false, adventureProgress.Wing, adventureProgress.Progress.Value);
          if (adventureProgress.Flags.HasValue)
            this.CreateOrUpdateWingFlags(false, adventureProgress.Wing, adventureProgress.Flags.Value);
          longList.Add(current.NoticeID);
        }
      }
    }
    using (List<long>.Enumerator enumerator = longList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Network.AckNotice(enumerator.Current);
    }
  }

  private void FireProgressUpdate(bool isStartupAction, AdventureMission.WingProgress oldProgress, AdventureMission.WingProgress newProgress)
  {
    foreach (AdventureProgressMgr.AdventureProgressUpdatedListener progressUpdatedListener in this.m_progressUpdatedListeners.ToArray())
      progressUpdatedListener.Fire(isStartupAction, oldProgress, newProgress);
  }

  private void CreateOrUpdateProgress(bool isStartupAction, int wing, int progress)
  {
    if (!this.m_wingProgress.ContainsKey(wing))
    {
      this.m_wingProgress[wing] = new AdventureMission.WingProgress(wing, progress, 0UL);
      Log.Rachelle.Print("AdventureProgressMgr.CreateOrUpdateProgress: creating wing {0} : PROGRESS {1}", new object[2]
      {
        (object) wing,
        (object) this.m_wingProgress[wing]
      });
      this.FireProgressUpdate(isStartupAction, (AdventureMission.WingProgress) null, this.m_wingProgress[wing]);
    }
    else
    {
      AdventureMission.WingProgress oldProgress = this.m_wingProgress[wing].Clone();
      this.m_wingProgress[wing].SetProgress(progress);
      Log.Rachelle.Print("AdventureProgressMgr.CreateOrUpdateProgress: updating wing {0} : PROGRESS {1} (former progress {2})", new object[3]
      {
        (object) wing,
        (object) this.m_wingProgress[wing],
        (object) oldProgress
      });
      this.FireProgressUpdate(isStartupAction, oldProgress, this.m_wingProgress[wing]);
    }
  }

  private void CreateOrUpdateWingFlags(bool isStartupAction, int wing, ulong flags)
  {
    if (!this.m_wingProgress.ContainsKey(wing))
    {
      this.m_wingProgress[wing] = new AdventureMission.WingProgress(wing, 0, flags);
      Log.Rachelle.Print("AdventureProgressMgr.CreateOrUpdateWingFlags: creating wing {0} : PROGRESS {1}", new object[2]
      {
        (object) wing,
        (object) this.m_wingProgress[wing]
      });
      this.FireProgressUpdate(isStartupAction, (AdventureMission.WingProgress) null, this.m_wingProgress[wing]);
    }
    else
    {
      AdventureMission.WingProgress oldProgress = this.m_wingProgress[wing].Clone();
      this.m_wingProgress[wing].SetFlags(flags);
      Log.Rachelle.Print("AdventureProgressMgr.CreateOrUpdateWingFlags: updating wing {0} : PROGRESS {1} (former flags {2})", new object[3]
      {
        (object) wing,
        (object) this.m_wingProgress[wing],
        (object) oldProgress
      });
      this.FireProgressUpdate(isStartupAction, oldProgress, this.m_wingProgress[wing]);
    }
  }

  private void CreateOrUpdateWingAck(int wing, int ack)
  {
    this.m_wingAckState[wing] = ack;
  }

  private class AdventureProgressUpdatedListener : EventListener<AdventureProgressMgr.AdventureProgressUpdatedCallback>
  {
    public void Fire(bool isStartupAction, AdventureMission.WingProgress oldProgress, AdventureMission.WingProgress newProgress)
    {
      this.m_callback(isStartupAction, oldProgress, newProgress, this.m_userData);
    }
  }

  public delegate void AdventureProgressUpdatedCallback(bool isStartupAction, AdventureMission.WingProgress oldProgress, AdventureMission.WingProgress newProgress, object userData);
}
