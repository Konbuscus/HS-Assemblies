﻿// Decompiled with JetBrains decompiler
// Type: CollectibleCardClassFilter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;

public class CollectibleCardClassFilter : CollectibleCardFilter
{
  private int m_cardsPerPage = 8;
  private Map<TAG_CLASS, List<CollectibleCard>> m_currentResultsByClass = new Map<TAG_CLASS, List<CollectibleCard>>();
  private TAG_CLASS[] m_classTabOrder;

  public void Init(TAG_CLASS[] classTabOrder, int cardsPerPage)
  {
    this.m_classTabOrder = classTabOrder;
    this.m_cardsPerPage = cardsPerPage;
    for (int index = 0; index < classTabOrder.Length; ++index)
      this.m_currentResultsByClass[classTabOrder[index]] = new List<CollectibleCard>();
  }

  public void UpdateResults()
  {
    List<CollectibleCard> list = this.GenerateList();
    using (Map<TAG_CLASS, List<CollectibleCard>>.Enumerator enumerator = this.m_currentResultsByClass.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.Clear();
    }
    using (List<CollectibleCard>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectibleCard current = enumerator.Current;
        if (!this.m_currentResultsByClass.ContainsKey(current.Class))
        {
          Error.AddDevFatal("Card: {0} ({1}) has an invalid class: {2}. Cannot render page.", (object) current.Name, (object) current.CardId, (object) current.Class);
          break;
        }
        this.m_currentResultsByClass[current.Class].Add(current);
      }
    }
  }

  public int GetNumPagesForClass(TAG_CLASS cardClass)
  {
    int count = this.m_currentResultsByClass[cardClass].Count;
    return count / this.m_cardsPerPage + (count % this.m_cardsPerPage <= 0 ? 0 : 1);
  }

  public int GetNumNewCardsForClass(TAG_CLASS cardClass)
  {
    return this.m_currentResultsByClass[cardClass].Where<CollectibleCard>((Func<CollectibleCard, bool>) (c => c.IsNewCard)).Count<CollectibleCard>();
  }

  public int GetTotalNumPages()
  {
    int num = 0;
    foreach (TAG_CLASS cardClass in this.m_classTabOrder)
      num += this.GetNumPagesForClass(cardClass);
    return num;
  }

  public List<CollectibleCard> GetPageContents(int page)
  {
    if (page < 0 || page > this.GetTotalNumPages())
      return new List<CollectibleCard>();
    int num1 = 0;
    for (int index = 0; index < this.m_classTabOrder.Length; ++index)
    {
      int num2 = num1;
      TAG_CLASS tagClass = this.m_classTabOrder[index];
      num1 += this.GetNumPagesForClass(tagClass);
      if (page <= num1)
      {
        int pageWithinClass = page - num2;
        int collectionPage;
        return this.GetPageContentsForClass(tagClass, pageWithinClass, false, out collectionPage);
      }
    }
    return new List<CollectibleCard>();
  }

  public List<CollectibleCard> GetFirstNonEmptyPage(out int collectionPage)
  {
    collectionPage = 0;
    TAG_CLASS pageClass = TAG_CLASS.NEUTRAL;
    for (int index = 0; index < this.m_classTabOrder.Length; ++index)
    {
      if (this.m_currentResultsByClass[this.m_classTabOrder[index]].Count > 0)
      {
        pageClass = this.m_classTabOrder[index];
        break;
      }
    }
    return this.GetPageContentsForClass(pageClass, 1, true, out collectionPage);
  }

  public List<CollectibleCard> GetPageContentsForClass(TAG_CLASS pageClass, int pageWithinClass, bool calculateCollectionPage, out int collectionPage)
  {
    collectionPage = 0;
    if (pageWithinClass <= 0 || pageWithinClass > this.GetNumPagesForClass(pageClass))
      return new List<CollectibleCard>();
    if (calculateCollectionPage)
    {
      for (int index = 0; index < this.m_classTabOrder.Length; ++index)
      {
        TAG_CLASS cardClass = this.m_classTabOrder[index];
        if (cardClass != pageClass)
          collectionPage = collectionPage + this.GetNumPagesForClass(cardClass);
        else
          break;
      }
      collectionPage = collectionPage + pageWithinClass;
    }
    List<CollectibleCard> source = this.m_currentResultsByClass[pageClass];
    if (source == null)
      return new List<CollectibleCard>();
    return source.Skip<CollectibleCard>(this.m_cardsPerPage * (pageWithinClass - 1)).Take<CollectibleCard>(this.m_cardsPerPage).ToList<CollectibleCard>();
  }

  public List<CollectibleCard> GetPageContentsForCard(string cardID, TAG_PREMIUM premiumType, out int collectionPage)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectibleCardClassFilter.\u003CGetPageContentsForCard\u003Ec__AnonStorey37B cardCAnonStorey37B = new CollectibleCardClassFilter.\u003CGetPageContentsForCard\u003Ec__AnonStorey37B();
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey37B.cardID = cardID;
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey37B.premiumType = premiumType;
    // ISSUE: reference to a compiler-generated field
    TAG_CLASS pageClass = DefLoader.Get().GetEntityDef(cardCAnonStorey37B.cardID).GetClass();
    collectionPage = 0;
    // ISSUE: reference to a compiler-generated method
    int index = this.m_currentResultsByClass[pageClass].FindIndex(new Predicate<CollectibleCard>(cardCAnonStorey37B.\u003C\u003Em__A6));
    if (index < 0)
      return new List<CollectibleCard>();
    int num = index + 1;
    int pageWithinClass = num / this.m_cardsPerPage + (num % this.m_cardsPerPage <= 0 ? 0 : 1);
    return this.GetPageContentsForClass(pageClass, pageWithinClass, true, out collectionPage);
  }
}
