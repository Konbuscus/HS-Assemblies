﻿// Decompiled with JetBrains decompiler
// Type: Entity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class Entity : EntityBase
{
  private EntityDef m_entityDef = new EntityDef();
  private List<Entity> m_attachments = new List<Entity>();
  private List<int> m_subCardIDs = new List<int>();
  private int m_realTimeCost = -1;
  private Card m_card;
  private string m_cardId;
  private Entity.LoadState m_loadState;
  private int m_cardAssetLoadCount;
  private bool m_useBattlecryPower;
  private bool m_duplicateForHistory;
  private CardTextHistoryData m_cardTextHistoryData;
  private int m_realTimeAttack;
  private int m_realTimeHealth;
  private int m_realTimeDamage;
  private int m_realTimeArmor;
  private bool m_realTimePoweredUp;
  private bool m_realTimeDivineShield;
  private TAG_CARDTYPE m_realTimeCardType;

  public override string ToString()
  {
    return this.GetDebugName();
  }

  public virtual void OnRealTimeFullEntity(Network.HistFullEntity fullEntity)
  {
    this.SetTags(fullEntity.Entity.Tags);
    this.InitRealTimeValues(fullEntity.Entity);
    if (!this.IsEnchantment())
      this.InitCard();
    this.LoadEntityDef(fullEntity.Entity.CardID);
  }

  public void OnFullEntity(Network.HistFullEntity fullEntity)
  {
    this.m_loadState = Entity.LoadState.PENDING;
    this.LoadCard(fullEntity.Entity.CardID);
    int tag1 = this.GetTag(GAME_TAG.ATTACHED);
    if (tag1 != 0)
      GameState.Get().GetEntity(tag1).AddAttachment(this);
    int tag2 = this.GetTag(GAME_TAG.PARENT_CARD);
    if (tag2 != 0)
      GameState.Get().GetEntity(tag2).AddSubCard(this);
    if (this.GetZone() != TAG_ZONE.PLAY)
      return;
    if (this.IsHero())
    {
      this.GetController().SetHero(this);
    }
    else
    {
      if (!this.IsHeroPower())
        return;
      this.GetController().SetHeroPower(this);
    }
  }

  public virtual void OnRealTimeShowEntity(Network.HistShowEntity showEntity)
  {
    this.HandleRealTimeEntityChange(showEntity.Entity);
  }

  public void OnShowEntity(Network.HistShowEntity showEntity)
  {
    this.HandleEntityChange(showEntity.Entity);
  }

  public virtual void OnRealTimeChangeEntity(Network.HistChangeEntity changeEntity)
  {
    this.HandleRealTimeEntityChange(changeEntity.Entity);
  }

  public void OnChangeEntity(Network.HistChangeEntity changeEntity)
  {
    this.m_subCardIDs.Clear();
    this.HandleEntityChange(changeEntity.Entity);
  }

  public virtual void OnRealTimeTagChanged(Network.HistTagChange change)
  {
    GAME_TAG tag = (GAME_TAG) change.Tag;
    switch (tag)
    {
      case GAME_TAG.DAMAGE:
        this.SetRealTimeDamage(change.Value);
        break;
      case GAME_TAG.HEALTH:
        this.SetRealTimeHealth(change.Value);
        break;
      case GAME_TAG.ATK:
        this.SetRealTimeAttack(change.Value);
        break;
      case GAME_TAG.COST:
        this.SetRealTimeCost(change.Value);
        break;
      default:
        if (tag != GAME_TAG.DURABILITY)
        {
          if (tag != GAME_TAG.DIVINE_SHIELD)
          {
            if (tag != GAME_TAG.CARDTYPE)
            {
              if (tag != GAME_TAG.ARMOR)
              {
                if (tag != GAME_TAG.POWERED_UP)
                  break;
                this.SetRealTimePoweredUp(change.Value);
                break;
              }
              this.SetRealTimeArmor(change.Value);
              break;
            }
            this.SetRealTimeCardType((TAG_CARDTYPE) change.Value);
            break;
          }
          this.SetRealTimeDivineShield(change.Value);
          break;
        }
        goto case GAME_TAG.HEALTH;
    }
  }

  public virtual void OnTagsChanged(TagDeltaList changeList)
  {
    bool flag = false;
    for (int index = 0; index < changeList.Count; ++index)
    {
      TagDelta change = changeList[index];
      if (this.IsNameChange(change))
        flag = true;
      this.HandleTagChange(change);
    }
    if ((UnityEngine.Object) this.m_card == (UnityEngine.Object) null)
      return;
    if (flag)
      this.UpdateCardName();
    this.m_card.OnTagsChanged(changeList);
  }

  public virtual void OnTagChanged(TagDelta change)
  {
    this.HandleTagChange(change);
    if ((UnityEngine.Object) this.m_card == (UnityEngine.Object) null)
      return;
    if (this.IsNameChange(change))
      this.UpdateCardName();
    this.m_card.OnTagChanged(change);
  }

  public virtual void OnMetaData(Network.HistMetaData metaData)
  {
    if ((UnityEngine.Object) this.m_card == (UnityEngine.Object) null)
      return;
    this.m_card.OnMetaData(metaData);
  }

  private void HandleRealTimeEntityChange(Network.Entity netEntity)
  {
    this.InitRealTimeValues(netEntity);
    if (!this.IsEnchantment() || !(bool) ((UnityEngine.Object) this.m_card))
      return;
    this.m_card.Destroy();
  }

  private void HandleEntityChange(Network.Entity netEntity)
  {
    TagDeltaList deltas = this.m_tags.CreateDeltas(netEntity.Tags);
    this.SetTags(netEntity.Tags);
    this.LoadCard(netEntity.CardID);
    this.OnTagsChanged(deltas);
  }

  private void HandleTagChange(TagDelta change)
  {
    switch ((GAME_TAG) change.tag)
    {
      case GAME_TAG.ATTACHED:
        Entity entity1 = GameState.Get().GetEntity(change.oldValue);
        if (entity1 != null)
          entity1.RemoveAttachment(this);
        Entity entity2 = GameState.Get().GetEntity(change.newValue);
        if (entity2 == null)
          break;
        entity2.AddAttachment(this);
        break;
      case GAME_TAG.ZONE:
        this.UpdateUseBattlecryFlag(false);
        if (GameState.Get().IsTurnStartManagerActive() && change.oldValue == 2 && change.newValue == 3)
        {
          PowerTaskList currentTaskList = GameState.Get().GetPowerProcessor().GetCurrentTaskList();
          if (currentTaskList != null && currentTaskList.GetSourceEntity() == GameState.Get().GetFriendlySidePlayer())
            TurnStartManager.Get().NotifyOfCardDrawn(this);
        }
        if (change.newValue == 1)
        {
          if (this.IsHero())
            this.GetController().SetHero(this);
          else if (this.IsHeroPower())
            this.GetController().SetHeroPower(this);
        }
        this.CheckZoneChangeForEnchantment(change);
        break;
      case GAME_TAG.PARENT_CARD:
        Entity entity3 = GameState.Get().GetEntity(change.oldValue);
        if (entity3 != null)
          entity3.RemoveSubCard(this);
        Entity entity4 = GameState.Get().GetEntity(change.newValue);
        if (entity4 == null)
          break;
        entity4.AddSubCard(this);
        break;
    }
  }

  private void CheckZoneChangeForEnchantment(TagDelta change)
  {
    if (change.tag != 49 || !this.IsEnchantment() || change.oldValue == change.newValue || change.newValue != 5 && change.newValue != 4)
      return;
    Entity entity = GameState.Get().GetEntity(this.GetAttached());
    if (entity == null)
      return;
    entity.RemoveAttachment(this);
  }

  private bool IsNameChange(TagDelta change)
  {
    switch ((GAME_TAG) change.tag)
    {
      case GAME_TAG.ZONE:
      case GAME_TAG.ENTITY_ID:
      case GAME_TAG.ZONE_POSITION:
        return true;
      default:
        return false;
    }
  }

  public EntityDef GetEntityDef()
  {
    return this.m_entityDef;
  }

  public Card InitCard()
  {
    this.m_card = new GameObject().AddComponent<Card>();
    this.m_card.SetEntity(this);
    this.UpdateCardName();
    return this.m_card;
  }

  public CardDef GetCardDef()
  {
    if ((UnityEngine.Object) this.m_card != (UnityEngine.Object) null)
      return this.m_card.GetCardDef();
    if (!string.IsNullOrEmpty(this.m_cardId))
      return DefLoader.Get().GetCardDef(this.m_cardId, (CardPortraitQuality) null);
    return (CardDef) null;
  }

  public Card GetCard()
  {
    return this.m_card;
  }

  public void SetCard(Card card)
  {
    this.m_card = card;
  }

  public string GetCardId()
  {
    return this.m_cardId;
  }

  public void SetCardId(string cardId)
  {
    this.m_cardId = cardId;
  }

  public void Destroy()
  {
    if (!((UnityEngine.Object) this.m_card != (UnityEngine.Object) null))
      return;
    this.m_card.Destroy();
  }

  public Entity.LoadState GetLoadState()
  {
    return this.m_loadState;
  }

  public bool IsLoadingAssets()
  {
    return this.m_loadState == Entity.LoadState.LOADING;
  }

  public bool IsBusy()
  {
    return this.IsLoadingAssets() || (UnityEngine.Object) this.m_card != (UnityEngine.Object) null && !this.m_card.IsActorReady();
  }

  public bool IsHidden()
  {
    return string.IsNullOrEmpty(this.m_cardId);
  }

  public void ClearTags()
  {
    this.m_tags.Clear();
  }

  public void SetTagAndHandleChange<TagEnum>(GAME_TAG tag, TagEnum tagValue)
  {
    this.SetTagAndHandleChange((int) tag, Convert.ToInt32((object) tagValue));
  }

  public TagDelta SetTagAndHandleChange(int tag, int tagValue)
  {
    int tag1 = this.m_tags.GetTag(tag);
    this.SetTag(tag, tagValue);
    TagDelta change = new TagDelta();
    change.tag = tag;
    change.oldValue = tag1;
    change.newValue = tagValue;
    this.OnTagChanged(change);
    return change;
  }

  public override int GetReferencedTag(int tag)
  {
    return this.m_entityDef.GetReferencedTag(tag);
  }

  public int GetDefCost()
  {
    return this.m_entityDef.GetCost();
  }

  public int GetDefATK()
  {
    return this.m_entityDef.GetATK();
  }

  public int GetDefHealth()
  {
    return this.m_entityDef.GetHealth();
  }

  public int GetDefDurability()
  {
    return this.m_entityDef.GetDurability();
  }

  public TAG_RACE GetRace()
  {
    return this.m_entityDef.GetRace();
  }

  public override TAG_CLASS GetClass()
  {
    if (this.IsSecret())
      return base.GetClass();
    return this.m_entityDef.GetClass();
  }

  public override IEnumerable<TAG_CLASS> GetClasses(Comparison<TAG_CLASS> classSorter = null)
  {
    if (this.IsSecret())
      return base.GetClasses((Comparison<TAG_CLASS>) null);
    return this.m_entityDef.GetClasses((Comparison<TAG_CLASS>) null);
  }

  public TAG_ENCHANTMENT_VISUAL GetEnchantmentBirthVisual()
  {
    return this.m_entityDef.GetEnchantmentBirthVisual();
  }

  public TAG_ENCHANTMENT_VISUAL GetEnchantmentIdleVisual()
  {
    return this.m_entityDef.GetEnchantmentIdleVisual();
  }

  public TAG_RARITY GetRarity()
  {
    return this.m_entityDef.GetRarity();
  }

  public new TAG_CARD_SET GetCardSet()
  {
    return this.m_entityDef.GetCardSet();
  }

  public TAG_PREMIUM GetPremiumType()
  {
    return (TAG_PREMIUM) this.GetTag(GAME_TAG.PREMIUM);
  }

  public bool CanBeDamagedRealTime()
  {
    if (!this.GetRealTimeDivineShield())
      return !this.IsImmune();
    return false;
  }

  public int GetCurrentHealth()
  {
    return this.GetTag(GAME_TAG.HEALTH) - this.GetTag(GAME_TAG.DAMAGE) - this.GetTag(GAME_TAG.PREDAMAGE);
  }

  public int GetCurrentDurability()
  {
    return this.GetTag(GAME_TAG.DURABILITY) - this.GetTag(GAME_TAG.DAMAGE) - this.GetTag(GAME_TAG.PREDAMAGE);
  }

  public int GetCurrentDefense()
  {
    return this.GetCurrentHealth() + this.GetArmor();
  }

  public int GetCurrentVitality()
  {
    if (this.IsCharacter())
      return this.GetCurrentDefense();
    if (this.IsWeapon())
      return this.GetCurrentDurability();
    Error.AddDevFatal("Entity.GetCurrentVitality() should not be called on {0}. This entity is neither a character nor a weapon.", (object) this);
    return 0;
  }

  public Player GetController()
  {
    return GameState.Get().GetPlayer(this.GetControllerId());
  }

  public Player.Side GetControllerSide()
  {
    Player controller = this.GetController();
    if (controller == null)
      return Player.Side.NEUTRAL;
    return controller.GetSide();
  }

  public bool IsControlledByLocalUser()
  {
    return this.GetController().IsLocalUser();
  }

  public bool IsControlledByFriendlySidePlayer()
  {
    return this.GetController().IsFriendlySide();
  }

  public bool IsControlledByOpposingSidePlayer()
  {
    return this.GetController().IsOpposingSide();
  }

  public bool IsControlledByRevealedPlayer()
  {
    return this.GetController().IsRevealed();
  }

  public bool IsControlledByConcealedPlayer()
  {
    return !this.IsControlledByRevealedPlayer();
  }

  public Entity GetCreator()
  {
    return GameState.Get().GetEntity(this.GetCreatorId());
  }

  public Entity GetDisplayedCreator()
  {
    return GameState.Get().GetEntity(this.GetDisplayedCreatorId());
  }

  public virtual Entity GetHero()
  {
    if (this.IsHero())
      return this;
    Player controller = this.GetController();
    if (controller == null)
      return (Entity) null;
    return controller.GetHero();
  }

  public virtual Card GetHeroCard()
  {
    if (this.IsHero())
      return this.GetCard();
    Player controller = this.GetController();
    if (controller == null)
      return (Card) null;
    return controller.GetHeroCard();
  }

  public virtual Entity GetHeroPower()
  {
    if (this.IsHeroPower())
      return this;
    Player controller = this.GetController();
    if (controller == null)
      return (Entity) null;
    return controller.GetHeroPower();
  }

  public virtual Card GetHeroPowerCard()
  {
    if (this.IsHeroPower())
      return this.GetCard();
    Player controller = this.GetController();
    if (controller == null)
      return (Card) null;
    return controller.GetHeroPowerCard();
  }

  public virtual Card GetWeaponCard()
  {
    if (this.IsWeapon())
      return this.GetCard();
    Player controller = this.GetController();
    if (controller == null)
      return (Card) null;
    return controller.GetWeaponCard();
  }

  public virtual string GetName()
  {
    return this.m_entityDef.GetName() ?? this.GetDebugName();
  }

  public virtual string GetDebugName()
  {
    string name = this.m_entityDef.GetName();
    if (name != null)
      return string.Format("[name={0} id={1} zone={2} zonePos={3} cardId={4} player={5}]", (object) name, (object) this.GetEntityId(), (object) this.GetZone(), (object) this.GetZonePosition(), (object) this.m_cardId, (object) this.GetControllerId());
    if (this.m_cardId != null)
      return string.Format("[id={0} cardId={1} type={2} zone={3} zonePos={4} player={5}]", (object) this.GetEntityId(), (object) this.m_cardId, (object) this.GetCardType(), (object) this.GetZone(), (object) this.GetZonePosition(), (object) this.GetControllerId());
    return string.Format("UNKNOWN ENTITY [id={0} type={1} zone={2} zonePos={3}]", (object) this.GetEntityId(), (object) this.GetCardType(), (object) this.GetZone(), (object) this.GetZonePosition());
  }

  public void UpdateCardName()
  {
    if ((UnityEngine.Object) this.m_card == (UnityEngine.Object) null)
      return;
    string name = this.m_entityDef.GetName();
    if (name != null)
    {
      if (string.IsNullOrEmpty(this.m_cardId))
        this.m_card.gameObject.name = string.Format("{0} [id={1} zone={2} zonePos={3}]", (object) name, (object) this.GetEntityId(), (object) this.GetZone(), (object) this.GetZonePosition());
      else
        this.m_card.gameObject.name = string.Format("{0} [id={1} cardId={2} zone={3} zonePos={4}]", (object) name, (object) this.GetEntityId(), (object) this.GetCardId(), (object) this.GetZone(), (object) this.GetZonePosition());
    }
    else
      this.m_card.gameObject.name = string.Format("Hidden Entity [id={0} zone={1} zonePos={2}]", (object) this.GetEntityId(), (object) this.GetZone(), (object) this.GetZonePosition());
  }

  public string GetCardTextInHand()
  {
    CardDef cardDef = this.GetCardDef();
    if (!((UnityEngine.Object) cardDef == (UnityEngine.Object) null))
      return cardDef.GetCardTextBuilder().BuildCardTextInHand(this);
    Log.All.PrintError("Entity.GetCardTextInHand(): entity {0} does not have a CardDef", (object) this.GetEntityId());
    return string.Empty;
  }

  public string GetCardTextInHistory()
  {
    CardDef cardDef = this.GetCardDef();
    if (!((UnityEngine.Object) cardDef == (UnityEngine.Object) null))
      return cardDef.GetCardTextBuilder().BuildCardTextInHistory(this);
    Log.All.PrintError("Entity.GetCardTextInHand(): entity {0} does not have a CardDef", (object) this.GetEntityId());
    return string.Empty;
  }

  public string GetTargetingArrowText()
  {
    CardDbfRecord cardRecord = GameDbf.GetIndex().GetCardRecord(this.m_cardId);
    if (cardRecord == null || cardRecord.TargetArrowText == null)
      return string.Empty;
    return TextUtils.TransformCardText(this, cardRecord.TargetArrowText.GetString(true));
  }

  public string GetRaceText()
  {
    return this.m_entityDef.GetRaceText();
  }

  public void AddAttachment(Entity entity)
  {
    int count = this.m_attachments.Count;
    if (this.m_attachments.Contains(entity))
    {
      Log.Mike.Print(string.Format("Entity.AddAttachment() - {0} is already an attachment of {1}", (object) entity, (object) this));
    }
    else
    {
      this.m_attachments.Add(entity);
      if ((UnityEngine.Object) this.m_card == (UnityEngine.Object) null)
        return;
      this.m_card.OnEnchantmentAdded(count, entity);
    }
  }

  public void RemoveAttachment(Entity entity)
  {
    int count = this.m_attachments.Count;
    if (!this.m_attachments.Remove(entity))
    {
      Log.Mike.Print(string.Format("Entity.RemoveAttachment() - {0} is not an attachment of {1}", (object) entity, (object) this));
    }
    else
    {
      if ((UnityEngine.Object) this.m_card == (UnityEngine.Object) null)
        return;
      this.m_card.OnEnchantmentRemoved(count, entity);
    }
  }

  public void AddSubCard(Entity entity)
  {
    if (this.m_subCardIDs.Contains(entity.GetEntityId()))
      return;
    this.m_subCardIDs.Add(entity.GetEntityId());
  }

  public void RemoveSubCard(Entity entity)
  {
    this.m_subCardIDs.Remove(entity.GetEntityId());
  }

  public List<Entity> GetAttachments()
  {
    return this.m_attachments;
  }

  public bool DoEnchantmentsHaveTriggerVisuals()
  {
    using (List<Entity>.Enumerator enumerator = this.m_attachments.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.HasTriggerVisual())
          return true;
      }
    }
    return false;
  }

  public bool IsEnchanted()
  {
    return this.m_attachments.Count > 0;
  }

  public bool IsEnchantment()
  {
    return this.GetRealTimeCardType() == TAG_CARDTYPE.ENCHANTMENT;
  }

  public bool IsDarkWandererSecret()
  {
    if (this.IsSecret())
      return this.GetClass() == TAG_CLASS.WARRIOR;
    return false;
  }

  public List<Entity> GetEnchantments()
  {
    return this.GetAttachments();
  }

  public bool HasSubCards()
  {
    if (this.m_subCardIDs != null)
      return this.m_subCardIDs.Count > 0;
    return false;
  }

  public List<int> GetSubCardIDs()
  {
    return this.m_subCardIDs;
  }

  public int GetSubCardIndex(Entity entity)
  {
    if (entity == null)
      return -1;
    int entityId = entity.GetEntityId();
    for (int index = 0; index < this.m_subCardIDs.Count; ++index)
    {
      if (this.m_subCardIDs[index] == entityId)
        return index;
    }
    return -1;
  }

  public Entity GetParentEntity()
  {
    return GameState.Get().GetEntity(this.GetTag(GAME_TAG.PARENT_CARD));
  }

  public Power GetMasterPower()
  {
    return this.m_entityDef.GetMasterPower();
  }

  public Power GetAttackPower()
  {
    return this.m_entityDef.GetAttackPower();
  }

  public Entity CloneForZoneMgr()
  {
    Entity entity = new Entity();
    entity.m_entityDef = this.m_entityDef;
    entity.m_card = this.m_card;
    entity.m_cardId = this.m_cardId;
    entity.ReplaceTags(this.m_tags);
    entity.m_loadState = this.m_loadState;
    return entity;
  }

  public Entity CloneForHistory(HistoryInfo historyInfo)
  {
    Entity entity = new Entity();
    entity.m_duplicateForHistory = true;
    entity.m_entityDef = this.m_entityDef;
    entity.m_card = this.m_card;
    entity.m_cardId = this.m_cardId;
    entity.ReplaceTags(this.m_tags);
    entity.m_cardTextHistoryData = this.m_card.GetCardDef().GetCardTextBuilder().CreateCardTextHistoryData();
    entity.m_cardTextHistoryData.SetHistoryData(this, historyInfo);
    entity.SetTag<TAG_ZONE>(GAME_TAG.ZONE, TAG_ZONE.HAND);
    entity.m_loadState = this.m_loadState;
    return entity;
  }

  public bool IsHistoryDupe()
  {
    return this.m_duplicateForHistory;
  }

  public int GetJadeGolem()
  {
    return Mathf.Min(this.GetController().GetTag(GAME_TAG.JADE_GOLEM) + 1, 30);
  }

  public int GetDamageBonus()
  {
    Player controller = this.GetController();
    if (controller == null)
      return 0;
    if (this.IsSpell())
    {
      int tag = controller.GetTag(GAME_TAG.CURRENT_SPELLPOWER);
      if (this.HasTag(GAME_TAG.RECEIVES_DOUBLE_SPELLDAMAGE_BONUS))
        tag *= 2;
      return tag;
    }
    if (this.IsHeroPower())
      return controller.GetTag(GAME_TAG.CURRENT_HEROPOWER_DAMAGE_BONUS);
    return 0;
  }

  public int GetDamageBonusDouble()
  {
    Player controller = this.GetController();
    if (controller == null)
      return 0;
    if (this.IsSpell())
      return controller.GetTag(GAME_TAG.SPELLPOWER_DOUBLE);
    if (this.IsHeroPower())
      return controller.GetTag(GAME_TAG.HERO_POWER_DOUBLE);
    return 0;
  }

  public int GetHealingDouble()
  {
    Player controller = this.GetController();
    if (controller == null)
      return 0;
    if (this.IsSpell())
      return controller.GetTag(GAME_TAG.HEALING_DOUBLE);
    if (this.IsHeroPower())
      return controller.GetTag(GAME_TAG.HERO_POWER_DOUBLE);
    return 0;
  }

  public void ClearBattlecryFlag()
  {
    this.m_useBattlecryPower = false;
  }

  public bool ShouldUseBattlecryPower()
  {
    return this.m_useBattlecryPower;
  }

  public void UpdateUseBattlecryFlag(bool fromGameState)
  {
    if (!this.IsMinion() || (this.GetZone() != TAG_ZONE.HAND || !fromGameState && !GameState.Get().EntityHasTargets(this)))
      return;
    this.m_useBattlecryPower = true;
  }

  public void InitRealTimeValues(Network.Entity netEntity)
  {
    this.SetRealTimeCost(this.GetCost());
    this.SetRealTimeAttack(this.GetATK());
    this.SetRealTimeHealth(this.GetHealth());
    this.SetRealTimeDamage(this.GetDamage());
    this.SetRealTimeArmor(this.GetArmor());
    this.SetRealTimePoweredUp(this.GetTag(GAME_TAG.POWERED_UP));
    this.SetRealTimeDivineShield(this.GetTag(GAME_TAG.DIVINE_SHIELD));
    this.SetRealTimeCardType((TAG_CARDTYPE) this.GetTag(GAME_TAG.CARDTYPE));
    using (List<Network.Entity.Tag>.Enumerator enumerator = netEntity.Tags.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        GAME_TAG name = (GAME_TAG) current.Name;
        switch (name)
        {
          case GAME_TAG.DAMAGE:
            this.SetRealTimeDamage(current.Value);
            continue;
          case GAME_TAG.HEALTH:
            this.SetRealTimeHealth(current.Value);
            continue;
          case GAME_TAG.ATK:
            this.SetRealTimeAttack(current.Value);
            continue;
          case GAME_TAG.COST:
            this.SetRealTimeCost(current.Value);
            continue;
          default:
            if (name != GAME_TAG.DURABILITY)
            {
              if (name != GAME_TAG.DIVINE_SHIELD)
              {
                if (name != GAME_TAG.CARDTYPE)
                {
                  if (name != GAME_TAG.ARMOR)
                  {
                    if (name == GAME_TAG.POWERED_UP)
                    {
                      this.SetRealTimePoweredUp(current.Value);
                      continue;
                    }
                    continue;
                  }
                  this.SetRealTimeArmor(current.Value);
                  continue;
                }
                this.SetRealTimeCardType((TAG_CARDTYPE) current.Value);
                continue;
              }
              this.SetRealTimeDivineShield(current.Value);
              continue;
            }
            goto case GAME_TAG.HEALTH;
        }
      }
    }
  }

  public void SetRealTimeCost(int newCost)
  {
    this.m_realTimeCost = newCost;
  }

  public int GetRealTimeCost()
  {
    if (this.m_realTimeCost == -1)
      return this.GetCost();
    return this.m_realTimeCost;
  }

  public void SetRealTimeAttack(int newAttack)
  {
    this.m_realTimeAttack = newAttack;
  }

  public int GetRealTimeAttack()
  {
    return this.m_realTimeAttack;
  }

  public void SetRealTimeHealth(int newHealth)
  {
    this.m_realTimeHealth = newHealth;
  }

  public void SetRealTimeDamage(int newDamage)
  {
    this.m_realTimeDamage = newDamage;
  }

  public void SetRealTimeArmor(int newArmor)
  {
    this.m_realTimeArmor = newArmor;
  }

  public int GetRealTimeRemainingHP()
  {
    return this.m_realTimeHealth + this.m_realTimeArmor - this.m_realTimeDamage;
  }

  public void SetRealTimePoweredUp(int poweredUp)
  {
    this.m_realTimePoweredUp = poweredUp > 0;
  }

  public bool GetRealTimePoweredUp()
  {
    return this.m_realTimePoweredUp;
  }

  public void SetRealTimeDivineShield(int divineShield)
  {
    this.m_realTimeDivineShield = divineShield > 0;
  }

  public bool GetRealTimeDivineShield()
  {
    return this.m_realTimeDivineShield;
  }

  public void SetRealTimeCardType(TAG_CARDTYPE cardType)
  {
    this.m_realTimeCardType = cardType;
  }

  public TAG_CARDTYPE GetRealTimeCardType()
  {
    return this.m_realTimeCardType;
  }

  public CardTextHistoryData GetCardTextHistoryData()
  {
    return this.m_cardTextHistoryData;
  }

  public PlayErrors.TargetEntityInfo ConvertToTargetInfo()
  {
    return new PlayErrors.TargetEntityInfo() { id = this.GetEntityId(), owningPlayerID = this.GetControllerId(), damage = this.GetDamage(), attack = this.GetATK(), cost = this.GetCost(), race = (int) this.m_entityDef.GetRace(), rarity = (int) this.m_entityDef.GetRarity(), isImmune = this.IsImmune(), canBeAttacked = this.CanBeAttacked(), canBeTargetedByOpponents = this.CanBeTargetedByOpponents(), canBeTargetedBySpells = this.CanBeTargetedBySpells(), canBeTargetedByHeroPowers = this.CanBeTargetedByHeroPowers(), canBeTargetedByBattlecries = this.CanBeTargetedByBattlecries(), cardType = this.GetCardType(), isFrozen = this.IsFrozen(), isEnchanted = this.IsEnchanted(), isStealthed = this.IsStealthed(), isTaunter = this.HasTaunt(), isMagnet = this.IsMagnet(), hasCharge = this.HasCharge(), hasAttackedThisTurn = this.GetNumAttacksThisTurn() > 0, hasBattlecry = this.HasBattlecry(), hasDeathrattle = this.HasDeathrattle() };
  }

  public bool HasAdditionalPlayRequirements()
  {
    if (!this.HasTag(GAME_TAG.ADDITIONAL_PLAY_REQS_1))
      return this.HasTag(GAME_TAG.ADDITIONAL_PLAY_REQS_2);
    return true;
  }

  private void AddAdditionalPlayRequirements(PlayErrors.SourceEntityInfo sourceInfo)
  {
    if (!sourceInfo.isMasterPower || !this.HasAdditionalPlayRequirements())
      return;
    List<PlayErrors.PlayRequirementInfo> playRequirementInfoList = new List<PlayErrors.PlayRequirementInfo>();
    if (this.HasTag(GAME_TAG.ADDITIONAL_PLAY_REQS_1))
    {
      EntityDef entityDef = DefLoader.Get().GetEntityDef(this.GetTag(GAME_TAG.ADDITIONAL_PLAY_REQS_1));
      if (entityDef != null)
        playRequirementInfoList.Add(entityDef.GetMasterPower().GetPlayRequirementInfo());
    }
    if (this.HasTag(GAME_TAG.ADDITIONAL_PLAY_REQS_2))
    {
      EntityDef entityDef = DefLoader.Get().GetEntityDef(this.GetTag(GAME_TAG.ADDITIONAL_PLAY_REQS_2));
      if (entityDef != null)
        playRequirementInfoList.Add(entityDef.GetMasterPower().GetPlayRequirementInfo());
    }
    using (List<PlayErrors.PlayRequirementInfo>.Enumerator enumerator = playRequirementInfoList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayErrors.PlayRequirementInfo current = enumerator.Current;
        sourceInfo.requirementsMap |= current.requirementsMap;
        sourceInfo.minAttackRequirement = Math.Max(sourceInfo.minAttackRequirement, current.paramMinAtk);
        sourceInfo.maxAttackRequirement = Math.Min(sourceInfo.maxAttackRequirement, current.paramMaxAtk);
        sourceInfo.numMinionSlotsRequirement = Math.Max(sourceInfo.numMinionSlotsRequirement, current.paramNumMinionSlots);
        sourceInfo.numMinionSlotsWithTargetRequirement = Math.Max(sourceInfo.numMinionSlotsWithTargetRequirement, current.paramNumMinionSlotsWithTarget);
        sourceInfo.minTotalMinionsRequirement = Math.Max(sourceInfo.minTotalMinionsRequirement, current.paramMinNumTotalMinions);
        sourceInfo.minFriendlyMinionsRequirement = Math.Max(sourceInfo.minFriendlyMinionsRequirement, current.paramMinNumFriendlyMinions);
        sourceInfo.minEnemyMinionsRequirement = Math.Max(sourceInfo.minEnemyMinionsRequirement, current.paramMinNumEnemyMinions);
        sourceInfo.minFriendlySecretsRequirement = Math.Max(sourceInfo.minFriendlySecretsRequirement, current.paramMinNumFriendlySecrets);
        if (current.paramExactCost != 0)
          sourceInfo.exactCostRequirement = sourceInfo.exactCostRequirement == 0 || sourceInfo.exactCostRequirement == current.paramExactCost ? current.paramExactCost : int.MaxValue;
        if (current.paramRace != 0)
          sourceInfo.raceRequirement = sourceInfo.raceRequirement == 0 || sourceInfo.raceRequirement == current.paramRace ? current.paramRace : int.MaxValue;
      }
    }
  }

  public PlayErrors.SourceEntityInfo ConvertToSourceInfo(PlayErrors.PlayRequirementInfo playRequirementInfo, Entity parent)
  {
    PlayErrors.SourceEntityInfo sourceInfoImpl = this.ConvertToSourceInfoImpl(playRequirementInfo, parent);
    this.AddAdditionalPlayRequirements(sourceInfoImpl);
    return sourceInfoImpl;
  }

  private PlayErrors.SourceEntityInfo ConvertToSourceInfoImpl(PlayErrors.PlayRequirementInfo playRequirementInfo, Entity parent)
  {
    List<string> entourageCardIds = this.GetEntityDef().GetEntourageCardIDs();
    List<string> stringList = new List<string>();
    int num = 0;
    ZoneMgr zoneMgr = ZoneMgr.Get();
    if ((UnityEngine.Object) zoneMgr != (UnityEngine.Object) null)
    {
      ZonePlay zoneOfType1 = zoneMgr.FindZoneOfType<ZonePlay>(Player.Side.FRIENDLY);
      if ((UnityEngine.Object) zoneOfType1 != (UnityEngine.Object) null)
      {
        using (List<Card>.Enumerator enumerator = zoneOfType1.GetCards().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Entity entity = enumerator.Current.GetEntity();
            if (entity != null)
              stringList.Add(entity.GetCardId());
          }
        }
      }
      ZonePlay zoneOfType2 = zoneMgr.FindZoneOfType<ZonePlay>(Player.Side.OPPOSING);
      if ((UnityEngine.Object) zoneOfType2 != (UnityEngine.Object) null)
      {
        using (List<Card>.Enumerator enumerator = zoneOfType2.GetCards().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Entity entity = enumerator.Current.GetEntity();
            if (entity != null && entity.IsMinion())
              ++num;
          }
        }
      }
    }
    PlayErrors.SourceEntityInfo sourceEntityInfo = new PlayErrors.SourceEntityInfo();
    sourceEntityInfo.requirementsMap = playRequirementInfo.requirementsMap;
    sourceEntityInfo.id = this.GetEntityId();
    sourceEntityInfo.cost = this.GetCost();
    sourceEntityInfo.attack = this.GetATK();
    sourceEntityInfo.minAttackRequirement = playRequirementInfo.paramMinAtk;
    sourceEntityInfo.maxAttackRequirement = playRequirementInfo.paramMaxAtk;
    sourceEntityInfo.exactCostRequirement = playRequirementInfo.paramExactCost;
    sourceEntityInfo.raceRequirement = playRequirementInfo.paramRace;
    sourceEntityInfo.numMinionSlotsRequirement = playRequirementInfo.paramNumMinionSlots;
    sourceEntityInfo.numMinionSlotsWithTargetRequirement = playRequirementInfo.paramNumMinionSlotsWithTarget;
    sourceEntityInfo.minTotalMinionsRequirement = playRequirementInfo.paramMinNumTotalMinions;
    sourceEntityInfo.minFriendlyMinionsRequirement = playRequirementInfo.paramMinNumFriendlyMinions;
    sourceEntityInfo.minEnemyMinionsRequirement = playRequirementInfo.paramMinNumEnemyMinions;
    sourceEntityInfo.numTurnsInPlay = this.GetNumTurnsInPlay();
    sourceEntityInfo.numAttacksThisTurn = this.GetNumAttacksThisTurn();
    sourceEntityInfo.numAttacksAllowedThisTurn = 1 + this.GetWindfury() + this.GetExtraAttacksThisTurn();
    sourceEntityInfo.minFriendlySecretsRequirement = playRequirementInfo.paramMinNumFriendlySecrets;
    sourceEntityInfo.cardType = this.GetCardType();
    sourceEntityInfo.zone = this.GetZone();
    sourceEntityInfo.isSecret = this.IsSecret();
    sourceEntityInfo.isDuplicateSecret = false;
    if (sourceEntityInfo.isSecret)
    {
      Player player = GameState.Get().GetPlayer(this.GetControllerId());
      if (player != null)
      {
        using (List<string>.Enumerator enumerator = player.GetSecretDefinitions().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            if (this.GetCardId().Equals(enumerator.Current, StringComparison.OrdinalIgnoreCase))
            {
              sourceEntityInfo.isDuplicateSecret = true;
              break;
            }
          }
        }
      }
    }
    sourceEntityInfo.isExhausted = this.IsExhausted();
    sourceEntityInfo.isMasterPower = this.GetZone() == TAG_ZONE.HAND || this.IsHeroPower();
    sourceEntityInfo.isActionPower = TAG_ZONE.HAND == this.GetZone();
    sourceEntityInfo.isActivatePower = this.GetZone() == TAG_ZONE.PLAY || this.IsHeroPower();
    sourceEntityInfo.isAttackPower = this.IsHero() || !this.IsHeroPower() && TAG_ZONE.PLAY == this.GetZone();
    sourceEntityInfo.isFrozen = this.IsFrozen();
    sourceEntityInfo.hasBattlecry = this.HasBattlecry();
    sourceEntityInfo.canAttack = this.CanAttack();
    sourceEntityInfo.entireEntourageInPlay = false;
    if (entourageCardIds.Count > 0)
    {
      sourceEntityInfo.entireEntourageInPlay = stringList.Count > 0;
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      Entity.\u003CConvertToSourceInfoImpl\u003Ec__AnonStorey3B5 implCAnonStorey3B5 = new Entity.\u003CConvertToSourceInfoImpl\u003Ec__AnonStorey3B5();
      using (List<string>.Enumerator enumerator = entourageCardIds.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          // ISSUE: reference to a compiler-generated field
          implCAnonStorey3B5.entourageCardID = enumerator.Current;
          // ISSUE: reference to a compiler-generated method
          if (stringList.Find(new Predicate<string>(implCAnonStorey3B5.\u003C\u003Em__13B)) == null)
          {
            sourceEntityInfo.entireEntourageInPlay = false;
            break;
          }
        }
      }
    }
    sourceEntityInfo.hasCharge = this.HasCharge();
    sourceEntityInfo.isChoiceMinion = parent != null && parent.IsMinion();
    sourceEntityInfo.cannotAttackHeroes = this.CannotAttackHeroes();
    sourceEntityInfo.costsHealth = this.HasTag(GAME_TAG.CARD_COSTS_HEALTH);
    return sourceEntityInfo;
  }

  private void LoadEntityDef(string cardId)
  {
    if (this.m_cardId != cardId)
      this.m_cardId = cardId;
    if (string.IsNullOrEmpty(cardId))
      return;
    this.m_entityDef = DefLoader.Get().GetEntityDef(cardId);
    if (this.m_entityDef == null)
      Error.AddDevFatal("Failed to load a card xml for {0}", (object) cardId);
    else
      this.UpdateCardName();
  }

  public void LoadCard(string cardId)
  {
    this.LoadEntityDef(cardId);
    this.m_loadState = Entity.LoadState.LOADING;
    if (string.IsNullOrEmpty(cardId))
      DefLoader.Get().LoadCardDef("HiddenCard", new DefLoader.LoadDefCallback<CardDef>(this.OnCardDefLoaded), (object) null, (CardPortraitQuality) null);
    else
      DefLoader.Get().LoadCardDef(cardId, new DefLoader.LoadDefCallback<CardDef>(this.OnCardDefLoaded), (object) null, (CardPortraitQuality) null);
  }

  private void OnCardDefLoaded(string cardId, CardDef cardDef, object callbackData)
  {
    if ((UnityEngine.Object) cardDef == (UnityEngine.Object) null)
    {
      Debug.LogErrorFormat("Entity.OnCardDefLoaded() - {0} does not have an asset!", (object) cardId);
      this.m_loadState = Entity.LoadState.DONE;
    }
    else
    {
      if ((UnityEngine.Object) this.m_card != (UnityEngine.Object) null)
        this.m_card.LoadCardDef(cardDef);
      this.UpdateUseBattlecryFlag(false);
      this.m_loadState = Entity.LoadState.DONE;
    }
  }

  public enum LoadState
  {
    INVALID,
    PENDING,
    LOADING,
    DONE,
  }
}
