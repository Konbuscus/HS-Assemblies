﻿// Decompiled with JetBrains decompiler
// Type: TurnTimerUpdate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class TurnTimerUpdate
{
  private float m_secondsRemaining;
  private float m_endTimestamp;
  private bool m_show;

  public float GetSecondsRemaining()
  {
    return this.m_secondsRemaining;
  }

  public void SetSecondsRemaining(float sec)
  {
    this.m_secondsRemaining = sec;
  }

  public float GetEndTimestamp()
  {
    return this.m_endTimestamp;
  }

  public void SetEndTimestamp(float timestamp)
  {
    this.m_endTimestamp = timestamp;
  }

  public bool ShouldShow()
  {
    return this.m_show;
  }

  public void SetShow(bool show)
  {
    this.m_show = show;
  }
}
