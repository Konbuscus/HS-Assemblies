﻿// Decompiled with JetBrains decompiler
// Type: EchoingOozeSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class EchoingOozeSpell : Spell
{
  public Spell m_CustomSpawnSpell;
  public float m_PostSpawnDelayMin;
  public float m_PostSpawnDelayMax;

  protected override Card GetTargetCardFromPowerTask(int index, PowerTask task)
  {
    Network.HistFullEntity power = task.GetPower() as Network.HistFullEntity;
    if (power == null)
      return (Card) null;
    Network.Entity entity1 = power.Entity;
    Entity entity2 = GameState.Get().GetEntity(entity1.ID);
    if (entity2 != null)
      return entity2.GetCard();
    UnityEngine.Debug.LogWarning((object) string.Format("{0}.GetTargetCardFromPowerTask() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) entity1.ID));
    return (Card) null;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    Card targetCard = this.GetTargetCard();
    if ((Object) targetCard == (Object) null)
      this.OnStateFinished();
    else
      this.DoEffect(targetCard);
  }

  private void DoEffect(Card targetCard)
  {
    Spell spell = Object.Instantiate<Spell>(this.m_CustomSpawnSpell);
    targetCard.OverrideCustomSpawnSpell(spell);
    this.DoTasksUntilSpawn(targetCard);
    this.StartCoroutine(this.WaitThenFinish());
  }

  private void DoTasksUntilSpawn(Card targetCard)
  {
    int entityId = targetCard.GetEntity().GetEntityId();
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    int num = 0;
    for (int index = 0; index < taskList.Count; ++index)
    {
      Network.HistFullEntity power = taskList[index].GetPower() as Network.HistFullEntity;
      if (power != null && power.Entity.ID == entityId)
      {
        num = index;
        break;
      }
    }
    this.m_taskList.DoTasks(0, num + 1);
  }

  [DebuggerHidden]
  private IEnumerator WaitThenFinish()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EchoingOozeSpell.\u003CWaitThenFinish\u003Ec__Iterator2B6() { \u003C\u003Ef__this = this };
  }
}
