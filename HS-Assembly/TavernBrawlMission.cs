﻿// Decompiled with JetBrains decompiler
// Type: TavernBrawlMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;

public class TavernBrawlMission
{
  public int missionId = -1;
  public FormatType formatType = FormatType.FT_WILD;
  public RewardType rewardType = RewardType.REWARD_NONE;
  public RewardTrigger rewardTrigger = RewardTrigger.REWARD_TRIGGER_NONE;
  public int seasonId;
  public DateTime? endDateLocal;
  public DateTime? closedToNewSessionsDateLocal;
  public bool canCreateDeck;
  public bool canEditDeck;
  public bool canSelectHeroForDeck;
  public int ticketType;
  public long RewardData1;
  public long RewardData2;
  public int maxWins;
  public int maxLosses;
  public int maxSessions;
  public int SeasonEndSecondsSpreadCount;
  public bool friendlyChallengeDisabled;
  public DeckRuleset deckRuleset;

  public bool IsSessionBased
  {
    get
    {
      if (this.maxWins <= 0)
        return this.maxLosses > 0;
      return true;
    }
  }
}
