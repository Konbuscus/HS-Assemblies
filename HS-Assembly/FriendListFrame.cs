﻿// Decompiled with JetBrains decompiler
// Type: FriendListFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FriendListFrame : MonoBehaviour
{
  private static readonly PlatformDependentValue<bool> ALLOW_ITEM_SELECTION = new PlatformDependentValue<bool>(PlatformCategory.Screen)
  {
    PC = true,
    Tablet = true,
    Phone = false
  };
  private List<FriendListFrame.NearbyPlayerUpdate> m_nearbyPlayerUpdates = new List<FriendListFrame.NearbyPlayerUpdate>();
  private BnetPlayerChangelist m_playersChangeList = new BnetPlayerChangelist();
  private List<GameObject> m_ignoreObjectsForWorldBound = new List<GameObject>();
  private bool m_isRAFButtonEnabled = true;
  private List<FriendListFrame.FriendListItem> m_allItems = new List<FriendListFrame.FriendListItem>();
  private Dictionary<MobileFriendListItem.TypeFlags, FriendListItemHeader> m_headers = new Dictionary<MobileFriendListItem.TypeFlags, FriendListItemHeader>();
  private const float NEARBY_PLAYERS_UPDATE_TIME = 10f;
  public FriendListFrame.Me me;
  public FriendListFrame.RecentOpponent recentOpponent;
  public FriendListFrame.Prefabs prefabs;
  public FriendListFrame.ListInfo listInfo;
  public TouchList items;
  public FriendListButton addFriendButton;
  public FriendListButton removeFriendButton;
  public PegUIElement rafButton;
  public HighlightState rafButtonButtonGlow;
  public GameObject rafButtonGlowBone;
  public GameObject rafButtonDisabled;
  public TouchListScrollbar scrollbar;
  public NineSliceElement window;
  public GameObject portraitBackground;
  public Material unrankedBackground;
  public Material rankedBackground;
  public GameObject innerShadow;
  public GameObject outerShadow;
  private PlayerPortrait myPortrait;
  private AddFriendFrame m_addFriendFrame;
  private AlertPopup m_removeFriendPopup;
  private Camera m_itemsCamera;
  private bool m_editFriendsMode;
  private BnetPlayer m_friendToRemove;
  private float m_lastNearbyPlayersUpdate;
  private bool m_nearbyPlayersNeedUpdate;
  private FriendListFrame.VirtualizedFriendsListBehavior m_longListBehavior;

  public BnetPlayer SelectedPlayer
  {
    get
    {
      if (this.items.SelectedItem == null)
        return (BnetPlayer) null;
      FriendListBaseFriendFrame component1 = this.items.SelectedItem.GetComponent<FriendListBaseFriendFrame>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
        return component1.GetFriend();
      FriendListNearbyPlayerFrame component2 = this.items.SelectedItem.GetComponent<FriendListNearbyPlayerFrame>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
        return component2.GetNearbyPlayer();
      return (BnetPlayer) null;
    }
    set
    {
      this.items.SelectedIndex = this.items.FindIndex(new Predicate<ITouchListItem>(new FriendListFrame.\u003C\u003Ec__AnonStorey369()
      {
        value = value
      }.\u003C\u003Em__57));
      this.UpdateItems();
    }
  }

  public bool ShowingAddFriendFrame
  {
    get
    {
      return (UnityEngine.Object) this.m_addFriendFrame != (UnityEngine.Object) null;
    }
  }

  public bool IsInEditMode
  {
    get
    {
      return this.m_editFriendsMode;
    }
  }

  public event Action AddFriendFrameOpened;

  public event Action AddFriendFrameClosed;

  public event Action RemoveFriendPopupOpened;

  public event Action RemoveFriendPopupClosed;

  private void Awake()
  {
    this.myPortrait = this.me.portraitRef.Spawn<PlayerPortrait>();
    this.recentOpponent.button.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRecentOpponentButtonReleased));
    this.InitButtons();
    this.RegisterFriendEvents();
    this.CreateItemsCamera();
    this.UpdateBackgroundCollider();
    bool flag = (!UniversalInputManager.Get().IsTouchMode() ? 0 : (PlatformSettings.OS != OSCategory.PC ? 1 : 0)) == 0;
    if ((UnityEngine.Object) this.scrollbar != (UnityEngine.Object) null)
      this.scrollbar.gameObject.SetActive(flag);
    this.me.m_Medal.m_legendIndex.RenderToTexture = false;
    this.me.m_Medal.m_legendIndex.TextColor = new Color(0.97f, 0.98f, 0.7f, 1f);
    if (!BnetFriendMgr.Get().HasOnlineFriends() && !BnetNearbyPlayerMgr.Get().HasNearbyStrangers())
      return;
    CollectionManager.Get().RequestDeckContentsForDecksWithoutContentsLoaded((CollectionManager.DelOnAllDeckContents) null);
  }

  private void Start()
  {
    this.UpdateMyself();
    this.UpdateRecentOpponent();
    this.InitItems();
    this.UpdateRAFState();
  }

  private void OnDestroy()
  {
    this.UnregisterFriendEvents();
    this.CloseAddFriendFrame();
    if (this.m_longListBehavior != null && this.m_longListBehavior.FreeList != null)
    {
      using (List<MobileFriendListItem>.Enumerator enumerator = this.m_longListBehavior.FreeList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MobileFriendListItem current = enumerator.Current;
          if ((UnityEngine.Object) current != (UnityEngine.Object) null)
            UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    using (Dictionary<MobileFriendListItem.TypeFlags, FriendListItemHeader>.ValueCollection.Enumerator enumerator = this.m_headers.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FriendListItemHeader current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
  }

  private void Update()
  {
    this.HandleKeyboardInput();
    if (this.m_nearbyPlayersNeedUpdate && (double) Time.realtimeSinceStartup >= (double) this.m_lastNearbyPlayersUpdate + 10.0)
      this.HandleNearbyPlayersChanged();
    this.UpdateButtonGlows();
  }

  private void UpdateButtonGlows()
  {
    this.removeFriendButton.ShowActiveGlow(this.IsInEditMode);
  }

  private void OnEnable()
  {
    if (this.m_nearbyPlayersNeedUpdate)
      this.HandleNearbyPlayersChanged();
    if (this.m_playersChangeList.GetChanges().Count > 0)
    {
      this.DoPlayersChanged(this.m_playersChangeList);
      this.m_playersChangeList.GetChanges().Clear();
    }
    if (this.items.IsInitialized)
      this.ResumeItemsLayout();
    this.UpdateMyself();
    this.items.ResetState();
    this.m_editFriendsMode = false;
    this.m_friendToRemove = (BnetPlayer) null;
  }

  public void SetWorldRect(float x, float y, float width, float height)
  {
    bool activeSelf = this.gameObject.activeSelf;
    this.gameObject.SetActive(true);
    this.window.SetEntireSize(width, height);
    Vector3 worldPoint = TransformUtil.ComputeWorldPoint(TransformUtil.ComputeSetPointBounds((Component) this.window), new Vector3(0.0f, 1f, 0.0f));
    this.transform.Translate(new Vector3(x, y, worldPoint.z) - worldPoint);
    this.UpdateItemsList();
    this.UpdateItemsCamera();
    this.UpdateBackgroundCollider();
    this.UpdateDropShadow();
    this.UpdateRAFGlowLayout();
    this.gameObject.SetActive(activeSelf);
  }

  public void SetWorldPosition(float x, float y)
  {
    this.SetWorldPosition(new Vector3(x, y));
  }

  public void SetWorldPosition(Vector3 pos)
  {
    bool activeSelf = this.gameObject.activeSelf;
    this.gameObject.SetActive(true);
    this.transform.position = pos;
    this.UpdateItemsList();
    this.UpdateItemsCamera();
    this.UpdateBackgroundCollider();
    this.UpdateRAFGlowLayout();
    this.gameObject.SetActive(activeSelf);
  }

  public void SetWorldHeight(float height)
  {
    bool activeSelf = this.gameObject.activeSelf;
    this.gameObject.SetActive(true);
    this.window.SetEntireHeight(height);
    this.UpdateItemsList();
    this.UpdateItemsCamera();
    this.UpdateBackgroundCollider();
    this.UpdateDropShadow();
    this.UpdateRAFGlowLayout();
    this.gameObject.SetActive(activeSelf);
  }

  public void ShowAddFriendFrame(BnetPlayer player = null)
  {
    this.m_addFriendFrame = UnityEngine.Object.Instantiate<AddFriendFrame>(this.prefabs.addFriendFrame);
    this.m_addFriendFrame.Closed += new Action(this.CloseAddFriendFrame);
    if (player == null)
      return;
    this.m_addFriendFrame.SetPlayer(player);
  }

  public void CloseAddFriendFrame()
  {
    if ((UnityEngine.Object) this.m_addFriendFrame == (UnityEngine.Object) null)
      return;
    this.m_addFriendFrame.Close();
    if (this.AddFriendFrameClosed != null)
      this.AddFriendFrameClosed();
    this.m_addFriendFrame = (AddFriendFrame) null;
  }

  public void ShowRemoveFriendPopup(BnetPlayer friend)
  {
    this.m_friendToRemove = friend;
    if (this.m_friendToRemove == null)
      return;
    string uniqueName = FriendUtils.GetUniqueName(this.m_friendToRemove);
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_text = GameStrings.Format("GLOBAL_FRIENDLIST_REMOVE_FRIEND_ALERT_MESSAGE", (object) uniqueName),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_responseCallback = new AlertPopup.ResponseCallback(this.OnRemoveFriendPopupResponse),
      m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
    }, new DialogManager.DialogProcessCallback(this.OnRemoveFriendDialogShown), (object) this.m_friendToRemove);
    if (this.RemoveFriendPopupOpened == null)
      return;
    this.RemoveFriendPopupOpened();
  }

  public void UpdateRAFButtonGlow()
  {
    this.rafButtonButtonGlow.ChangeState(Options.Get().GetBool(Option.HAS_SEEN_RAF) || !this.m_isRAFButtonEnabled ? ActorStateType.NONE : ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }

  public List<GameObject> GetIgnoreListForWorldBounds()
  {
    return this.m_ignoreObjectsForWorldBound;
  }

  public void SetRAFButtonEnabled(bool enabled)
  {
    if (this.m_isRAFButtonEnabled == enabled)
      return;
    this.m_isRAFButtonEnabled = enabled;
    this.rafButtonDisabled.SetActive(!this.m_isRAFButtonEnabled);
    this.rafButton.GetComponent<UIBHighlight>().EnableResponse = this.m_isRAFButtonEnabled;
    if (this.m_isRAFButtonEnabled)
      this.rafButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRAFButtonReleased));
    else
      this.rafButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRAFButtonReleased));
    this.UpdateRAFButtonGlow();
  }

  private void CreateItemsCamera()
  {
    this.m_itemsCamera = new GameObject("ItemsCamera")
    {
      transform = {
        parent = this.items.transform,
        localPosition = new Vector3(0.0f, 0.0f, -100f)
      }
    }.AddComponent<Camera>();
    this.m_itemsCamera.orthographic = true;
    this.m_itemsCamera.depth = (float) (BnetBar.CameraDepth + 1);
    this.m_itemsCamera.clearFlags = CameraClearFlags.Depth;
    this.m_itemsCamera.cullingMask = GameLayer.BattleNetFriendList.LayerBit();
    this.UpdateItemsCamera();
  }

  private void UpdateItemsList()
  {
    Transform bottomRightBone = this.GetBottomRightBone();
    this.items.transform.position = (this.listInfo.topLeft.position + bottomRightBone.position) / 2f;
    Vector3 vector3_1 = bottomRightBone.position - this.listInfo.topLeft.position;
    this.items.ClipSize = new Vector2(vector3_1.x, Math.Abs(vector3_1.y));
    if (!((UnityEngine.Object) this.innerShadow != (UnityEngine.Object) null))
      return;
    this.innerShadow.transform.position = this.items.transform.position;
    Vector3 vector3_2 = this.GetBottomRightBone().position - this.listInfo.topLeft.position;
    TransformUtil.SetLocalScaleToWorldDimension(this.innerShadow, new WorldDimensionIndex[2]
    {
      new WorldDimensionIndex(Mathf.Abs(vector3_2.x), 0),
      new WorldDimensionIndex(Mathf.Abs(vector3_2.y), 2)
    });
  }

  private void UpdateItemsCamera()
  {
    Camera bnetCamera = BaseUI.Get().GetBnetCamera();
    Transform bottomRightBone = this.GetBottomRightBone();
    Vector3 screenPoint1 = bnetCamera.WorldToScreenPoint(this.listInfo.topLeft.position);
    Vector3 screenPoint2 = bnetCamera.WorldToScreenPoint(bottomRightBone.position);
    GeneralUtils.Swap<float>(ref screenPoint1.y, ref screenPoint2.y);
    this.m_itemsCamera.pixelRect = new Rect(screenPoint1.x, screenPoint1.y, screenPoint2.x - screenPoint1.x, screenPoint2.y - screenPoint1.y);
    this.m_itemsCamera.orthographicSize = this.m_itemsCamera.rect.height * bnetCamera.orthographicSize;
  }

  private void UpdateBackgroundCollider()
  {
    Bounds bounds = ((IEnumerable<Renderer>) this.window.GetComponentsInChildren<Renderer>()).Aggregate<Renderer, Bounds>(new Bounds(this.transform.position, Vector3.zero), (Func<Bounds, Renderer, Bounds>) ((aggregate, renderer) =>
    {
      if ((double) renderer.bounds.size.x != 0.0 && (double) renderer.bounds.size.y != 0.0 && (double) renderer.bounds.size.z != 0.0)
        aggregate.Encapsulate(renderer.bounds);
      return aggregate;
    }));
    Vector3 vector3_1 = this.transform.InverseTransformPoint(bounds.min);
    Vector3 vector3_2 = this.transform.InverseTransformPoint(bounds.max);
    BoxCollider boxCollider = this.GetComponent<BoxCollider>();
    if ((UnityEngine.Object) boxCollider == (UnityEngine.Object) null)
      boxCollider = this.gameObject.AddComponent<BoxCollider>();
    boxCollider.center = (vector3_1 + vector3_2) / 2f + Vector3.forward;
    boxCollider.size = vector3_2 - vector3_1;
  }

  private void UpdateDropShadow()
  {
    if ((UnityEngine.Object) this.outerShadow == (UnityEngine.Object) null)
      return;
    this.outerShadow.SetActive(!UniversalInputManager.Get().IsTouchMode());
  }

  private void UpdateMyself()
  {
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    if (myPlayer != null && myPlayer.IsDisplayable())
    {
      BnetBattleTag battleTag = myPlayer.GetBattleTag();
      this.myPortrait.SetProgramId(BnetProgramId.HEARTHSTONE);
      this.me.nameText.Text = battleTag.GetName();
      this.me.numberText.Text = string.Format("#{0}", (object) battleTag.GetNumber().ToString());
      this.me.statusText.Text = GameStrings.Get("GLOBAL_FRIENDLIST_MYSTATUS");
      TransformUtil.SetPoint((Component) this.me.numberText, Anchor.LEFT, (Component) this.me.nameText, Anchor.RIGHT, 6f * Vector3.right);
      NetCache.NetCacheMedalInfo netObject = NetCache.Get().GetNetObject<NetCache.NetCacheMedalInfo>();
      MedalInfoTranslator medal = netObject != null ? new MedalInfoTranslator(netObject) : (MedalInfoTranslator) null;
      if (medal == null || medal.GetCurrentMedal(medal.IsBestCurrentRankWild()).rank == 25)
      {
        this.me.m_MedalPatch.SetActive(false);
        this.myPortrait.gameObject.SetActive(true);
        if (!((UnityEngine.Object) this.portraitBackground != (UnityEngine.Object) null))
          return;
        Material[] materials = this.portraitBackground.GetComponent<Renderer>().materials;
        materials[0] = this.unrankedBackground;
        this.portraitBackground.GetComponent<Renderer>().materials = materials;
      }
      else
      {
        this.myPortrait.gameObject.SetActive(false);
        this.me.m_Medal.SetEnabled(false);
        this.me.m_Medal.SetMedal(medal, false);
        this.me.m_Medal.SetFormat(medal.IsBestCurrentRankWild());
        this.me.m_MedalPatch.SetActive(true);
        if (!((UnityEngine.Object) this.portraitBackground != (UnityEngine.Object) null))
          return;
        Material[] materials = this.portraitBackground.GetComponent<Renderer>().materials;
        materials[0] = this.rankedBackground;
        this.portraitBackground.GetComponent<Renderer>().materials = materials;
      }
    }
    else
    {
      this.me.nameText.Text = string.Empty;
      this.me.numberText.Text = string.Empty;
      this.me.statusText.Text = string.Empty;
    }
  }

  private void UpdateRecentOpponent()
  {
    BnetPlayer recentOpponent = FriendMgr.Get().GetRecentOpponent();
    if (recentOpponent == null)
    {
      this.recentOpponent.button.gameObject.SetActive(false);
    }
    else
    {
      this.recentOpponent.button.gameObject.SetActive(true);
      this.recentOpponent.nameText.Text = FriendUtils.GetUniqueNameWithColor(recentOpponent);
    }
  }

  private void InitItems()
  {
    BnetFriendMgr bnetFriendMgr = BnetFriendMgr.Get();
    BnetNearbyPlayerMgr bnetNearbyPlayerMgr = BnetNearbyPlayerMgr.Get();
    this.items.SelectionEnabled = true;
    this.items.SelectedIndexChanging += (TouchList.SelectedIndexChangingEvent) (index => index != -1);
    this.SuspendItemsLayout();
    this.UpdateRequests(bnetFriendMgr.GetReceivedInvites(), (List<BnetInvitation>) null);
    this.UpdateAllFriends(bnetFriendMgr.GetFriends(), (List<BnetPlayer>) null);
    this.UpdateAllNearbyPlayers(bnetNearbyPlayerMgr.GetNearbyStrangers(), (List<BnetPlayer>) null);
    this.UpdateAllNearbyFriends(bnetNearbyPlayerMgr.GetNearbyFriends(), (List<BnetPlayer>) null);
    this.UpdateAllHeaders();
    this.ResumeItemsLayout();
    this.UpdateAllHeaderBackgrounds();
    this.UpdateSelectedItem();
    this.UpdateRAFButtonGlow();
  }

  private void UpdateItems()
  {
    foreach (FriendListRequestFrame listRequestFrame in this.GetItems<FriendListRequestFrame>())
      listRequestFrame.UpdateInvite();
    this.UpdateFriendItems();
  }

  private void UpdateFriendItems()
  {
    foreach (FriendListCurrentGameFrame currentGameFrame in this.GetItems<FriendListCurrentGameFrame>())
      currentGameFrame.UpdateFriend();
    foreach (FriendListFriendFrame friendListFriendFrame in this.GetItems<FriendListFriendFrame>())
      friendListFriendFrame.UpdateFriend();
  }

  private void UpdateNearbyPlayerItems()
  {
    foreach (FriendListNearbyPlayerFrame nearbyPlayerFrame in this.GetItems<FriendListNearbyPlayerFrame>())
      nearbyPlayerFrame.UpdateNearbyPlayer();
  }

  private void UpdateRequests(List<BnetInvitation> addedList, List<BnetInvitation> removedList)
  {
    if (removedList == null && addedList == null)
      return;
    if (removedList != null)
    {
      using (List<BnetInvitation>.Enumerator enumerator = removedList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.RemoveItem(false, MobileFriendListItem.TypeFlags.Request, (object) enumerator.Current);
      }
    }
    foreach (FriendListRequestFrame listRequestFrame in this.GetItems<FriendListRequestFrame>())
      listRequestFrame.UpdateInvite();
    if (addedList == null)
      return;
    using (List<BnetInvitation>.Enumerator enumerator = addedList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.m_allItems.Add(new FriendListFrame.FriendListItem(false, MobileFriendListItem.TypeFlags.Request, (object) enumerator.Current));
    }
  }

  private void UpdateAllFriends(List<BnetPlayer> addedList, List<BnetPlayer> removedList)
  {
    if (removedList == null && addedList == null)
      return;
    if (removedList != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = removedList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          BnetPlayer current = enumerator.Current;
          if (this.RemoveItem(false, MobileFriendListItem.TypeFlags.Friend, (object) current) || !this.RemoveItem(false, MobileFriendListItem.TypeFlags.CurrentGame, (object) current))
            ;
        }
      }
    }
    this.UpdateFriendItems();
    if (addedList != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = addedList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          BnetPlayer current = enumerator.Current;
          this.m_allItems.Add(current.GetPersistentGameId() != 0L ? new FriendListFrame.FriendListItem(false, MobileFriendListItem.TypeFlags.CurrentGame, (object) current) : new FriendListFrame.FriendListItem(false, MobileFriendListItem.TypeFlags.Friend, (object) current));
        }
      }
    }
    this.SortAndRefreshTouchList();
  }

  private void UpdateAllNearbyPlayers(List<BnetPlayer> addedList, List<BnetPlayer> removedList)
  {
    if (removedList != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = removedList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.RemoveItem(false, MobileFriendListItem.TypeFlags.NearbyPlayer, (object) enumerator.Current);
      }
    }
    this.UpdateNearbyPlayerItems();
    if (addedList != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = addedList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.m_allItems.Add(new FriendListFrame.FriendListItem(false, MobileFriendListItem.TypeFlags.NearbyPlayer, (object) enumerator.Current));
      }
    }
    this.SortAndRefreshTouchList();
  }

  private void UpdateAllNearbyFriends(List<BnetPlayer> addedList, List<BnetPlayer> removedList)
  {
    if (removedList != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = removedList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.RemoveItem(false, MobileFriendListItem.TypeFlags.NearbyPlayer, (object) enumerator.Current);
      }
    }
    this.UpdateNearbyPlayerItems();
    if (addedList != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = addedList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.m_allItems.Add(new FriendListFrame.FriendListItem(false, MobileFriendListItem.TypeFlags.NearbyPlayer, (object) enumerator.Current));
      }
    }
    this.SortAndRefreshTouchList();
  }

  private FriendListBaseFriendFrame FindBaseFriendFrame(BnetPlayer friend)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.FindFirstItem<FriendListBaseFriendFrame>(new Predicate<FriendListBaseFriendFrame>(new FriendListFrame.\u003CFindBaseFriendFrame\u003Ec__AnonStorey36A()
    {
      friend = friend
    }.\u003C\u003Em__5A));
  }

  private FriendListCurrentGameFrame FindCurrentGameFrame(BnetPlayer friend)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.FindFirstItem<FriendListCurrentGameFrame>(new Predicate<FriendListCurrentGameFrame>(new FriendListFrame.\u003CFindCurrentGameFrame\u003Ec__AnonStorey36B()
    {
      friend = friend
    }.\u003C\u003Em__5B));
  }

  private FriendListFriendFrame FindFriendFrame(BnetPlayer friend)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.FindFirstItem<FriendListFriendFrame>(new Predicate<FriendListFriendFrame>(new FriendListFrame.\u003CFindFriendFrame\u003Ec__AnonStorey36C()
    {
      friend = friend
    }.\u003C\u003Em__5C));
  }

  private FriendListFriendFrame FindFriendFrame(BnetAccountId id)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.FindFirstItem<FriendListFriendFrame>(new Predicate<FriendListFriendFrame>(new FriendListFrame.\u003CFindFriendFrame\u003Ec__AnonStorey36D()
    {
      id = id
    }.\u003C\u003Em__5D));
  }

  private MobileFriendListItem CreateFriendFrame(BnetPlayer friend)
  {
    FriendListFriendFrame friendListFriendFrame = UnityEngine.Object.Instantiate<FriendListFriendFrame>(this.prefabs.friendItem);
    UberText[] objs = UberText.EnableAllTextInObject(friendListFriendFrame.gameObject, false);
    friendListFriendFrame.SetFriend(friend);
    friendListFriendFrame.GetComponent<FriendListUIElement>().AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBaseFriendFrameReleased));
    MobileFriendListItem visualItem = this.FinishCreateVisualItem<FriendListFriendFrame>(friendListFriendFrame, MobileFriendListItem.TypeFlags.Friend, (ITouchListItem) this.FindHeader(MobileFriendListItem.TypeFlags.Friend), friendListFriendFrame.gameObject);
    UberText.EnableAllTextObjects(objs, true);
    return visualItem;
  }

  private MobileFriendListItem CreateNearbyPlayerFrame(BnetPlayer friend)
  {
    FriendListNearbyPlayerFrame nearbyPlayerFrame = UnityEngine.Object.Instantiate<FriendListNearbyPlayerFrame>(this.prefabs.nearbyPlayerItem);
    UberText[] objs = UberText.EnableAllTextInObject(nearbyPlayerFrame.gameObject, false);
    nearbyPlayerFrame.SetNearbyPlayer(friend);
    nearbyPlayerFrame.GetComponent<FriendListUIElement>().AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnNearbyPlayerFrameReleased));
    MobileFriendListItem visualItem = this.FinishCreateVisualItem<FriendListNearbyPlayerFrame>(nearbyPlayerFrame, MobileFriendListItem.TypeFlags.NearbyPlayer, (ITouchListItem) this.FindHeader(MobileFriendListItem.TypeFlags.NearbyPlayer), nearbyPlayerFrame.gameObject);
    UberText.EnableAllTextObjects(objs, true);
    return visualItem;
  }

  private MobileFriendListItem CreateCurrentGameFrame(BnetPlayer friend)
  {
    FriendListCurrentGameFrame currentGameFrame = UnityEngine.Object.Instantiate<FriendListCurrentGameFrame>(this.prefabs.currentGameItem);
    UberText[] objs = UberText.EnableAllTextInObject(currentGameFrame.gameObject, false);
    currentGameFrame.SetFriend(friend);
    currentGameFrame.GetComponent<FriendListUIElement>().AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBaseFriendFrameReleased));
    MobileFriendListItem visualItem = this.FinishCreateVisualItem<FriendListCurrentGameFrame>(currentGameFrame, MobileFriendListItem.TypeFlags.CurrentGame, (ITouchListItem) this.FindHeader(MobileFriendListItem.TypeFlags.CurrentGame), currentGameFrame.gameObject);
    UberText.EnableAllTextObjects(objs, true);
    return visualItem;
  }

  private MobileFriendListItem CreateRequestFrame(BnetInvitation invite)
  {
    FriendListRequestFrame listRequestFrame = UnityEngine.Object.Instantiate<FriendListRequestFrame>(this.prefabs.requestItem);
    UberText[] objs = UberText.EnableAllTextInObject(listRequestFrame.gameObject, false);
    listRequestFrame.SetInvite(invite);
    MobileFriendListItem visualItem = this.FinishCreateVisualItem<FriendListRequestFrame>(listRequestFrame, MobileFriendListItem.TypeFlags.Request, (ITouchListItem) this.FindHeader(MobileFriendListItem.TypeFlags.Request), listRequestFrame.gameObject);
    UberText.EnableAllTextObjects(objs, true);
    return visualItem;
  }

  private void UpdateAllHeaders()
  {
    this.UpdateRequestsHeader((FriendListItemHeader) null);
    this.UpdateCurrentGamesHeader();
    this.UpdateNearbyPlayersHeader((FriendListItemHeader) null);
    this.UpdateFriendsHeader((FriendListItemHeader) null);
  }

  private void UpdateAllHeaderBackgrounds()
  {
    this.UpdateHeaderBackground(this.FindHeader(MobileFriendListItem.TypeFlags.Request));
    this.UpdateHeaderBackground(this.FindHeader(MobileFriendListItem.TypeFlags.CurrentGame));
  }

  private void UpdateRequestsHeader(FriendListItemHeader header = null)
  {
    int num = this.m_allItems.Count<FriendListFrame.FriendListItem>((Func<FriendListFrame.FriendListItem, bool>) (i => i.ItemMainType == MobileFriendListItem.TypeFlags.Request));
    if (num > 0)
    {
      string text = GameStrings.Format("GLOBAL_FRIENDLIST_REQUESTS_HEADER", (object) num);
      if ((UnityEngine.Object) header == (UnityEngine.Object) null)
      {
        header = this.FindOrAddHeader(MobileFriendListItem.TypeFlags.Request);
        if (!this.m_allItems.Any<FriendListFrame.FriendListItem>((Func<FriendListFrame.FriendListItem, bool>) (item =>
        {
          if (item.IsHeader)
            return item.SubType == MobileFriendListItem.TypeFlags.Request;
          return false;
        })))
          this.m_allItems.Add(new FriendListFrame.FriendListItem(true, MobileFriendListItem.TypeFlags.Request, (object) null));
      }
      header.SetText(text);
    }
    else
    {
      if (!((UnityEngine.Object) header == (UnityEngine.Object) null))
        return;
      this.RemoveItem(true, MobileFriendListItem.TypeFlags.Request, (object) null);
    }
  }

  private void UpdateCurrentGamesHeader()
  {
    int num = this.m_allItems.Count<FriendListFrame.FriendListItem>((Func<FriendListFrame.FriendListItem, bool>) (i => i.ItemMainType == MobileFriendListItem.TypeFlags.CurrentGame));
    if (num > 0)
    {
      string text = GameStrings.Format("GLOBAL_FRIENDLIST_CURRENT_GAMES_HEADER", (object) num);
      FriendListItemHeader orAddHeader = this.FindOrAddHeader(MobileFriendListItem.TypeFlags.CurrentGame);
      if (!this.m_allItems.Any<FriendListFrame.FriendListItem>((Func<FriendListFrame.FriendListItem, bool>) (item =>
      {
        if (item.IsHeader)
          return item.SubType == MobileFriendListItem.TypeFlags.CurrentGame;
        return false;
      })))
        this.m_allItems.Add(new FriendListFrame.FriendListItem(true, MobileFriendListItem.TypeFlags.CurrentGame, (object) null));
      orAddHeader.SetText(text);
    }
    else
      this.RemoveItem(true, MobileFriendListItem.TypeFlags.CurrentGame, (object) null);
  }

  private void UpdateNearbyPlayersHeader(FriendListItemHeader header = null)
  {
    int num = this.m_allItems.Count<FriendListFrame.FriendListItem>((Func<FriendListFrame.FriendListItem, bool>) (i => i.ItemMainType == MobileFriendListItem.TypeFlags.NearbyPlayer));
    if (num > 0)
    {
      string text = GameStrings.Format("GLOBAL_FRIENDLIST_NEARBY_PLAYERS_HEADER", (object) num);
      if ((UnityEngine.Object) header == (UnityEngine.Object) null)
      {
        header = this.FindOrAddHeader(MobileFriendListItem.TypeFlags.NearbyPlayer);
        if (!this.m_allItems.Any<FriendListFrame.FriendListItem>((Func<FriendListFrame.FriendListItem, bool>) (item =>
        {
          if (item.IsHeader)
            return item.SubType == MobileFriendListItem.TypeFlags.NearbyPlayer;
          return false;
        })))
          this.m_allItems.Add(new FriendListFrame.FriendListItem(true, MobileFriendListItem.TypeFlags.NearbyPlayer, (object) null));
      }
      header.SetText(text);
    }
    else
    {
      if (!((UnityEngine.Object) header == (UnityEngine.Object) null))
        return;
      this.RemoveItem(true, MobileFriendListItem.TypeFlags.NearbyPlayer, (object) null);
    }
  }

  private void UpdateFriendsHeader(FriendListItemHeader header = null)
  {
    IEnumerable<FriendListFrame.FriendListItem> source = this.m_allItems.Where<FriendListFrame.FriendListItem>((Func<FriendListFrame.FriendListItem, bool>) (i => i.ItemMainType == MobileFriendListItem.TypeFlags.Friend));
    int num1 = source.Count<FriendListFrame.FriendListItem>((Func<FriendListFrame.FriendListItem, bool>) (i => i.GetFriend().IsOnline()));
    int num2 = source.Count<FriendListFrame.FriendListItem>();
    string text;
    if (num1 == num2)
      text = GameStrings.Format("GLOBAL_FRIENDLIST_FRIENDS_HEADER_ALL_ONLINE", (object) num1);
    else
      text = GameStrings.Format("GLOBAL_FRIENDLIST_FRIENDS_HEADER", (object) num1, (object) num2);
    if ((UnityEngine.Object) header == (UnityEngine.Object) null)
    {
      header = this.FindOrAddHeader(MobileFriendListItem.TypeFlags.Friend);
      if (!this.m_allItems.Any<FriendListFrame.FriendListItem>((Func<FriendListFrame.FriendListItem, bool>) (item =>
      {
        if (item.IsHeader)
          return item.SubType == MobileFriendListItem.TypeFlags.Friend;
        return false;
      })))
        this.m_allItems.Add(new FriendListFrame.FriendListItem(true, MobileFriendListItem.TypeFlags.Friend, (object) null));
    }
    header.SetText(text);
  }

  private void UpdateHeaderBackground(FriendListItemHeader itemHeader)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    FriendListFrame.\u003CUpdateHeaderBackground\u003Ec__AnonStorey36E backgroundCAnonStorey36E = new FriendListFrame.\u003CUpdateHeaderBackground\u003Ec__AnonStorey36E();
    if ((UnityEngine.Object) itemHeader == (UnityEngine.Object) null)
      return;
    MobileFriendListItem component = itemHeader.GetComponent<MobileFriendListItem>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || (component.Type & MobileFriendListItem.TypeFlags.Request) == (MobileFriendListItem.TypeFlags) 0 && (component.Type & MobileFriendListItem.TypeFlags.CurrentGame) == (MobileFriendListItem.TypeFlags) 0)
      return;
    TiledBackground tiledBackground;
    if ((UnityEngine.Object) itemHeader.Background == (UnityEngine.Object) null)
    {
      GameObject go = new GameObject("ItemsBackground");
      go.transform.parent = component.transform;
      TransformUtil.Identity(go);
      go.layer = 24;
      FriendListFrame.HeaderBackgroundInfo headerBackgroundInfo = (component.Type & MobileFriendListItem.TypeFlags.Request) == (MobileFriendListItem.TypeFlags) 0 ? this.listInfo.currentGameBackgroundInfo : this.listInfo.requestBackgroundInfo;
      go.AddComponent<MeshFilter>().mesh = headerBackgroundInfo.mesh;
      go.AddComponent<MeshRenderer>().material = headerBackgroundInfo.material;
      tiledBackground = go.AddComponent<TiledBackground>();
      itemHeader.Background = go;
    }
    else
      tiledBackground = itemHeader.Background.GetComponent<TiledBackground>();
    tiledBackground.transform.parent = (Transform) null;
    // ISSUE: reference to a compiler-generated field
    backgroundCAnonStorey36E.type = component.Type ^ MobileFriendListItem.TypeFlags.Header;
    // ISSUE: reference to a compiler-generated method
    Bounds bounds = this.items.Aggregate<ITouchListItem, Bounds>(new Bounds(component.transform.position, Vector3.zero), new Func<Bounds, ITouchListItem, Bounds>(backgroundCAnonStorey36E.\u003C\u003Em__67));
    tiledBackground.transform.parent = component.transform;
    bounds.center = component.transform.InverseTransformPoint(bounds.center);
    tiledBackground.SetBounds(bounds);
    TransformUtil.SetPosZ((Component) tiledBackground.transform, 2f);
    tiledBackground.gameObject.SetActive(itemHeader.IsShowingContents);
  }

  private FriendListItemHeader FindHeader(MobileFriendListItem.TypeFlags type)
  {
    type |= MobileFriendListItem.TypeFlags.Header;
    FriendListItemHeader friendListItemHeader;
    this.m_headers.TryGetValue(type, out friendListItemHeader);
    return friendListItemHeader;
  }

  private FriendListItemHeader FindOrAddHeader(MobileFriendListItem.TypeFlags type)
  {
    type |= MobileFriendListItem.TypeFlags.Header;
    FriendListItemHeader friendListItemHeader = this.FindHeader(type);
    if ((UnityEngine.Object) friendListItemHeader == (UnityEngine.Object) null)
    {
      FriendListFrame.FriendListItem friendListItem = new FriendListFrame.FriendListItem(true, type, (object) null);
      friendListItemHeader = UnityEngine.Object.Instantiate<FriendListItemHeader>(this.prefabs.headerItem);
      this.m_headers[type] = friendListItemHeader;
      Option setoption = Option.FRIENDS_LIST_FRIEND_SECTION_HIDE;
      switch (friendListItem.SubType)
      {
        case MobileFriendListItem.TypeFlags.Friend:
          setoption = Option.FRIENDS_LIST_FRIEND_SECTION_HIDE;
          break;
        case MobileFriendListItem.TypeFlags.CurrentGame:
          setoption = Option.FRIENDS_LIST_CURRENTGAME_SECTION_HIDE;
          break;
        case MobileFriendListItem.TypeFlags.NearbyPlayer:
          setoption = Option.FRIENDS_LIST_NEARBYPLAYER_SECTION_HIDE;
          break;
        case MobileFriendListItem.TypeFlags.Request:
          setoption = Option.FRIENDS_LIST_REQUEST_SECTION_HIDE;
          break;
      }
      friendListItemHeader.SubType = friendListItem.SubType;
      friendListItemHeader.Option = setoption;
      bool showHeaderSection = this.GetShowHeaderSection(setoption);
      friendListItemHeader.SetInitialShowContents(showHeaderSection);
      friendListItemHeader.ClearToggleListeners();
      friendListItemHeader.AddToggleListener(new FriendListItemHeader.ToggleContentsFunc(this.OnHeaderSectionToggle), (object) friendListItemHeader);
      UberText[] objs = UberText.EnableAllTextInObject(friendListItemHeader.gameObject, false);
      this.FinishCreateVisualItem<FriendListItemHeader>(friendListItemHeader, type, (ITouchListItem) null, (GameObject) null);
      UberText.EnableAllTextObjects(objs, true);
    }
    return friendListItemHeader;
  }

  private void OnHeaderSectionToggle(bool show, object userdata)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    FriendListFrame.\u003COnHeaderSectionToggle\u003Ec__AnonStorey36F toggleCAnonStorey36F = new FriendListFrame.\u003COnHeaderSectionToggle\u003Ec__AnonStorey36F();
    // ISSUE: reference to a compiler-generated field
    toggleCAnonStorey36F.header = (FriendListItemHeader) userdata;
    // ISSUE: reference to a compiler-generated field
    this.SetShowHeaderSection(toggleCAnonStorey36F.header.Option, show);
    // ISSUE: reference to a compiler-generated method
    this.items.RefreshList(this.m_allItems.FindIndex(new Predicate<FriendListFrame.FriendListItem>(toggleCAnonStorey36F.\u003C\u003Em__68)), true);
    // ISSUE: reference to a compiler-generated field
    this.UpdateHeaderBackground(toggleCAnonStorey36F.header);
  }

  private T FindFirstItem<T>(Predicate<T> predicate) where T : MonoBehaviour
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    ITouchListItem touchListItem = this.items.FirstOrDefault<ITouchListItem>(new Func<ITouchListItem, bool>(new FriendListFrame.\u003CFindFirstItem\u003Ec__AnonStorey370<T>()
    {
      predicate = predicate
    }.\u003C\u003Em__69));
    if (touchListItem != null)
      return touchListItem.GetComponent<T>();
    return (T) null;
  }

  private IEnumerable<T> GetItems<T>() where T : MonoBehaviour
  {
    // ISSUE: object of a compiler-generated type is created
    return this.items.Select<ITouchListItem, \u003C\u003E__AnonType0<ITouchListItem, T>>((Func<ITouchListItem, \u003C\u003E__AnonType0<ITouchListItem, T>>) (i => new \u003C\u003E__AnonType0<ITouchListItem, T>(i, i.GetComponent<T>()))).Where<\u003C\u003E__AnonType0<ITouchListItem, T>>((Func<\u003C\u003E__AnonType0<ITouchListItem, T>, bool>) (param0 => (UnityEngine.Object) param0.c != (UnityEngine.Object) null)).Select<\u003C\u003E__AnonType0<ITouchListItem, T>, T>((Func<\u003C\u003E__AnonType0<ITouchListItem, T>, T>) (param0 => param0.c));
  }

  private MobileFriendListItem FinishCreateVisualItem<T>(T obj, MobileFriendListItem.TypeFlags type, ITouchListItem parent, GameObject showObj) where T : MonoBehaviour
  {
    MobileFriendListItem mobileFriendListItem = obj.gameObject.GetComponent<MobileFriendListItem>();
    if ((UnityEngine.Object) mobileFriendListItem == (UnityEngine.Object) null)
    {
      mobileFriendListItem = obj.gameObject.AddComponent<MobileFriendListItem>();
      BoxCollider component = mobileFriendListItem.GetComponent<BoxCollider>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.size = new Vector3(component.size.x, component.size.y + this.items.elementSpacing, component.size.z);
    }
    mobileFriendListItem.Type = type;
    mobileFriendListItem.SetShowObject(showObj);
    mobileFriendListItem.SetParent(parent);
    if (mobileFriendListItem.Selectable)
    {
      BnetPlayer selectedFriend = FriendMgr.Get().GetSelectedFriend();
      if (selectedFriend != null)
      {
        BnetPlayer bnetPlayer = (BnetPlayer) null;
        if ((object) obj is FriendListFriendFrame)
          bnetPlayer = ((FriendListBaseFriendFrame) (object) obj).GetFriend();
        else if ((object) obj is FriendListNearbyPlayerFrame)
          bnetPlayer = ((FriendListNearbyPlayerFrame) (object) obj).GetNearbyPlayer();
        if (bnetPlayer != null && selectedFriend == bnetPlayer)
          mobileFriendListItem.Selected();
      }
    }
    return mobileFriendListItem;
  }

  private bool RemoveItem(bool isHeader, MobileFriendListItem.TypeFlags type, object itemToRemove)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    int index = this.m_allItems.FindIndex(new Predicate<FriendListFrame.FriendListItem>(new FriendListFrame.\u003CRemoveItem\u003Ec__AnonStorey371()
    {
      isHeader = isHeader,
      type = type,
      itemToRemove = itemToRemove
    }.\u003C\u003Em__6D));
    if (index < 0)
      return false;
    this.m_allItems.RemoveAt(index);
    return true;
  }

  private void SuspendItemsLayout()
  {
    this.items.SuspendLayout();
  }

  private void ResumeItemsLayout()
  {
    this.items.ResumeLayout(false);
    this.SortAndRefreshTouchList();
  }

  private void ToggleEditFriendsMode()
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_editFriendsMode = !this.m_editFriendsMode;
    this.UpdateFriendItems();
  }

  private void SortAndRefreshTouchList()
  {
    if (this.items.IsLayoutSuspended)
      return;
    this.m_allItems.Sort(new Comparison<FriendListFrame.FriendListItem>(this.ItemsSortCompare));
    if (this.m_longListBehavior == null)
    {
      this.m_longListBehavior = new FriendListFrame.VirtualizedFriendsListBehavior(this);
      this.items.LongListBehavior = (TouchList.ILongListBehavior) this.m_longListBehavior;
    }
    else
      this.items.RefreshList(0, true);
  }

  private int ItemsSortCompare(FriendListFrame.FriendListItem item1, FriendListFrame.FriendListItem item2)
  {
    int num1 = item2.ItemFlags.CompareTo((object) item1.ItemFlags);
    if (num1 != 0)
      return num1;
    switch (item1.ItemFlags)
    {
      case MobileFriendListItem.TypeFlags.Friend:
        return FriendUtils.FriendSortCompare(item1.GetFriend(), item2.GetFriend());
      case MobileFriendListItem.TypeFlags.NearbyPlayer:
        return FriendUtils.FriendSortCompare(item1.GetNearbyPlayer(), item2.GetNearbyPlayer());
      case MobileFriendListItem.TypeFlags.CurrentGame:
        return FriendUtils.FriendSortCompare(item1.GetCurrentGame(), item2.GetCurrentGame());
      case MobileFriendListItem.TypeFlags.Request:
        BnetInvitation invite1 = item1.GetInvite();
        BnetInvitation invite2 = item2.GetInvite();
        int num2 = string.Compare(invite1.GetInviterName(), invite2.GetInviterName(), true);
        if (num2 != 0)
          return num2;
        return (int) ((long) invite1.GetInviterId().GetLo() - (long) invite2.GetInviterId().GetLo());
      default:
        return 0;
    }
  }

  private void RegisterFriendEvents()
  {
    BnetFriendMgr.Get().AddChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    FriendChallengeMgr.Get().AddChangedListener(new FriendChallengeMgr.ChangedCallback(this.OnFriendChallengeChanged));
    BnetNearbyPlayerMgr.Get().AddChangeListener(new BnetNearbyPlayerMgr.ChangeCallback(this.OnNearbyPlayersChanged));
    FriendMgr.Get().AddRecentOpponentListener(new FriendMgr.RecentOpponentCallback(this.OnRecentOpponent));
    SceneMgr.Get().RegisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
    SpectatorManager.Get().OnInviteReceived += new SpectatorManager.InviteReceivedHandler(this.SpectatorManager_OnInviteReceivedOrSent);
    SpectatorManager.Get().OnInviteSent += new SpectatorManager.InviteSentHandler(this.SpectatorManager_OnInviteReceivedOrSent);
  }

  private void UnregisterFriendEvents()
  {
    BnetFriendMgr.Get().RemoveChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    FriendChallengeMgr.Get().RemoveChangedListener(new FriendChallengeMgr.ChangedCallback(this.OnFriendChallengeChanged));
    BnetNearbyPlayerMgr.Get().RemoveChangeListener(new BnetNearbyPlayerMgr.ChangeCallback(this.OnNearbyPlayersChanged));
    FriendMgr.Get().RemoveRecentOpponentListener(new FriendMgr.RecentOpponentCallback(this.OnRecentOpponent));
    if ((UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null)
      SceneMgr.Get().UnregisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
    SpectatorManager.Get().OnInviteReceived -= new SpectatorManager.InviteReceivedHandler(this.SpectatorManager_OnInviteReceivedOrSent);
    SpectatorManager.Get().OnInviteSent -= new SpectatorManager.InviteSentHandler(this.SpectatorManager_OnInviteReceivedOrSent);
  }

  private void OnFriendsChanged(BnetFriendChangelist changelist, object userData)
  {
    this.SuspendItemsLayout();
    this.UpdateRequests(changelist.GetAddedReceivedInvites(), changelist.GetRemovedReceivedInvites());
    this.UpdateAllFriends(changelist.GetAddedFriends(), changelist.GetRemovedFriends());
    this.UpdateAllHeaders();
    this.ResumeItemsLayout();
    this.UpdateAllHeaderBackgrounds();
    this.UpdateSelectedItem();
  }

  private void OnNearbyPlayersChanged(BnetNearbyPlayerChangelist changelist, object userData)
  {
    this.m_nearbyPlayersNeedUpdate = true;
    if (changelist.GetAddedStrangers() != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = changelist.GetAddedStrangers().GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.m_nearbyPlayerUpdates.Add(new FriendListFrame.NearbyPlayerUpdate(FriendListFrame.NearbyPlayerUpdate.ChangeType.Added, enumerator.Current));
      }
    }
    if (changelist.GetRemovedStrangers() != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = changelist.GetRemovedStrangers().GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.m_nearbyPlayerUpdates.Add(new FriendListFrame.NearbyPlayerUpdate(FriendListFrame.NearbyPlayerUpdate.ChangeType.Removed, enumerator.Current));
      }
    }
    if (changelist.GetAddedFriends() != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = changelist.GetAddedFriends().GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.m_nearbyPlayerUpdates.Add(new FriendListFrame.NearbyPlayerUpdate(FriendListFrame.NearbyPlayerUpdate.ChangeType.Added, enumerator.Current));
      }
    }
    if (changelist.GetRemovedFriends() != null)
    {
      using (List<BnetPlayer>.Enumerator enumerator = changelist.GetRemovedFriends().GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.m_nearbyPlayerUpdates.Add(new FriendListFrame.NearbyPlayerUpdate(FriendListFrame.NearbyPlayerUpdate.ChangeType.Removed, enumerator.Current));
      }
    }
    if (!this.gameObject.activeInHierarchy || (double) Time.realtimeSinceStartup < (double) this.m_lastNearbyPlayersUpdate + 10.0)
      return;
    this.HandleNearbyPlayersChanged();
  }

  private void HandleNearbyPlayersChanged()
  {
    if (!this.m_nearbyPlayersNeedUpdate)
      return;
    this.UpdateNearbyPlayerItems();
    if (this.m_nearbyPlayerUpdates.Count > 0)
    {
      this.SuspendItemsLayout();
      using (List<FriendListFrame.NearbyPlayerUpdate>.Enumerator enumerator = this.m_nearbyPlayerUpdates.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          FriendListFrame.NearbyPlayerUpdate current = enumerator.Current;
          if (current.Change == FriendListFrame.NearbyPlayerUpdate.ChangeType.Added)
            this.m_allItems.Add(new FriendListFrame.FriendListItem(false, MobileFriendListItem.TypeFlags.NearbyPlayer, (object) current.Player));
          else
            this.RemoveItem(false, MobileFriendListItem.TypeFlags.NearbyPlayer, (object) current.Player);
        }
      }
      this.m_nearbyPlayerUpdates.Clear();
      this.UpdateAllHeaders();
      this.ResumeItemsLayout();
      this.UpdateAllHeaderBackgrounds();
      this.UpdateSelectedItem();
    }
    this.m_nearbyPlayersNeedUpdate = false;
    this.m_lastNearbyPlayersUpdate = Time.realtimeSinceStartup;
  }

  private void DoPlayersChanged(BnetPlayerChangelist changelist)
  {
    this.SuspendItemsLayout();
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    bool flag1 = false;
    bool flag2 = false;
    using (List<BnetPlayerChange>.Enumerator enumerator = changelist.GetChanges().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayerChange current = enumerator.Current;
        BnetPlayer oldPlayer = current.GetOldPlayer();
        BnetPlayer newPlayer = current.GetNewPlayer();
        if (newPlayer == FriendMgr.Get().GetRecentOpponent())
          this.UpdateRecentOpponent();
        if (newPlayer == myPlayer)
        {
          this.UpdateMyself();
          BnetGameAccount hearthstoneGameAccount = newPlayer.GetHearthstoneGameAccount();
          flag1 = oldPlayer == null || oldPlayer.GetHearthstoneGameAccount() == (BnetGameAccount) null ? hearthstoneGameAccount.CanBeInvitedToGame() : oldPlayer.GetHearthstoneGameAccount().CanBeInvitedToGame() != hearthstoneGameAccount.CanBeInvitedToGame();
        }
        else
        {
          if (oldPlayer == null || oldPlayer.GetBestName() != newPlayer.GetBestName())
            flag2 = true;
          long persistentGameId = newPlayer.GetPersistentGameId();
          FriendListFriendFrame friendFrame = this.FindFriendFrame(newPlayer);
          if ((UnityEngine.Object) friendFrame != (UnityEngine.Object) null)
          {
            if (persistentGameId != 0L)
            {
              this.RemoveItem(false, MobileFriendListItem.TypeFlags.Friend, (object) newPlayer);
              this.m_allItems.Add(new FriendListFrame.FriendListItem(false, MobileFriendListItem.TypeFlags.CurrentGame, (object) newPlayer));
              this.UpdateCurrentGamesHeader();
              this.UpdateFriendsHeader((FriendListItemHeader) null);
            }
            else
              friendFrame.UpdateFriend();
          }
          else
          {
            FriendListCurrentGameFrame currentGameFrame = this.FindCurrentGameFrame(newPlayer);
            if ((UnityEngine.Object) currentGameFrame != (UnityEngine.Object) null)
            {
              if (persistentGameId == 0L)
              {
                this.RemoveItem(false, MobileFriendListItem.TypeFlags.CurrentGame, (object) null);
                this.m_allItems.Add(new FriendListFrame.FriendListItem(false, MobileFriendListItem.TypeFlags.Friend, (object) newPlayer));
                this.UpdateCurrentGamesHeader();
                this.UpdateFriendsHeader((FriendListItemHeader) null);
              }
              else
                currentGameFrame.UpdateFriend();
            }
          }
        }
      }
    }
    if (flag1)
      this.UpdateItems();
    else if (flag2)
      this.UpdateFriendItems();
    this.UpdateAllHeaders();
    this.UpdateAllHeaderBackgrounds();
    this.ResumeItemsLayout();
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    if (this.gameObject.activeInHierarchy)
      this.DoPlayersChanged(changelist);
    else
      this.m_playersChangeList.GetChanges().AddRange((IEnumerable<BnetPlayerChange>) changelist.GetChanges());
  }

  private void OnRecentOpponent(BnetPlayer recentOpponent, object userData)
  {
    this.UpdateRecentOpponent();
  }

  private void OnScenePreUnload(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    switch (SceneMgr.Get().GetMode())
    {
      case SceneMgr.Mode.FRIENDLY:
      case SceneMgr.Mode.FATAL_ERROR:
        if ((UnityEngine.Object) ChatMgr.Get() != (UnityEngine.Object) null)
        {
          ChatMgr.Get().CloseFriendsList();
          break;
        }
        UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
        break;
    }
  }

  private void SpectatorManager_OnInviteReceivedOrSent(OnlineEventType evt, BnetPlayer inviter)
  {
    FriendListFriendFrame friendFrame = this.FindFriendFrame(inviter);
    if (!((UnityEngine.Object) friendFrame != (UnityEngine.Object) null))
      return;
    friendFrame.UpdateFriend();
  }

  private void OnFriendChallengeChanged(FriendChallengeEvent challengeEvent, BnetPlayer player, object userData)
  {
    if (player == BnetPresenceMgr.Get().GetMyPlayer())
    {
      this.UpdateFriendItems();
    }
    else
    {
      FriendListBaseFriendFrame baseFriendFrame = this.FindBaseFriendFrame(player);
      if (!((UnityEngine.Object) baseFriendFrame != (UnityEngine.Object) null))
        return;
      baseFriendFrame.UpdateFriend();
    }
  }

  private bool HandleKeyboardInput()
  {
    return FatalErrorMgr.Get().HasError() ? false : false;
  }

  private void OnAddFriendButtonReleased(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("Small_Click");
    if ((UnityEngine.Object) this.m_addFriendFrame != (UnityEngine.Object) null)
    {
      this.CloseAddFriendFrame();
    }
    else
    {
      if (this.AddFriendFrameOpened != null)
        this.AddFriendFrameOpened();
      this.ShowAddFriendFrame(FriendMgr.Get().GetSelectedFriend());
    }
  }

  private void OnEditFriendsButtonReleased(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("Small_Click");
    this.ToggleEditFriendsMode();
  }

  private void OnRemoveFriendButtonReleased(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("Small_Click");
    if (this.items.SelectedItem == null)
      return;
    this.ShowRemoveFriendPopup(FriendMgr.Get().GetSelectedFriend());
  }

  private void OnRAFButtonReleased(UIEvent e)
  {
    if (!this.m_isRAFButtonEnabled)
      return;
    SoundManager.Get().LoadAndPlay("Small_Click");
    RAFManager.Get().ShowRAFFrame();
  }

  private void OnRAFButtonOver(UIEvent e)
  {
    TooltipZone component = this.rafButton.GetComponent<TooltipZone>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    string bodytext = GameUtils.GetNextTutorial() == 0 ? GameStrings.Get("GLUE_RAF_TOOLTIP_DESC") : GameStrings.Get("GLUE_RAF_TOOLTIP_LOCKED_DESC");
    component.ShowSocialTooltip((Component) this.rafButton, GameStrings.Get("GLUE_RAF_TOOLTIP_HEADLINE"), bodytext, 75f, GameLayer.BattleNetDialog);
  }

  private void OnRAFButtonOut(UIEvent e)
  {
    TooltipZone component = this.rafButton.GetComponent<TooltipZone>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.HideTooltip();
  }

  private bool OnRemoveFriendDialogShown(DialogBase dialog, object userData)
  {
    if (!BnetFriendMgr.Get().IsFriend((BnetPlayer) userData))
      return false;
    this.m_removeFriendPopup = (AlertPopup) dialog;
    return true;
  }

  private void OnRemoveFriendPopupResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CONFIRM && this.m_friendToRemove != null)
      BnetFriendMgr.Get().RemoveFriend(this.m_friendToRemove);
    this.m_friendToRemove = (BnetPlayer) null;
    this.m_removeFriendPopup = (AlertPopup) null;
    if (this.RemoveFriendPopupClosed == null)
      return;
    this.RemoveFriendPopupClosed();
  }

  private void OnRecentOpponentButtonReleased(UIEvent e)
  {
    if (string.IsNullOrEmpty(this.recentOpponent.nameText.Text))
      return;
    this.ShowAddFriendFrame(FriendMgr.Get().GetRecentOpponent());
  }

  private void OnBaseFriendFrameReleased(UIEvent e)
  {
    if (this.IsInEditMode)
      return;
    BnetPlayer friend = e.GetElement().GetComponent<FriendListBaseFriendFrame>().GetFriend();
    FriendMgr.Get().SetSelectedFriend(friend);
    if ((bool) FriendListFrame.ALLOW_ITEM_SELECTION)
      this.SelectedPlayer = friend;
    ChatMgr.Get().OnFriendListFriendSelected(friend);
  }

  private void OnNearbyPlayerFrameReleased(UIEvent e)
  {
    BnetPlayer nearbyPlayer = e.GetElement().GetComponent<FriendListNearbyPlayerFrame>().GetNearbyPlayer();
    FriendMgr.Get().SetSelectedFriend(nearbyPlayer);
    if (!(bool) FriendListFrame.ALLOW_ITEM_SELECTION)
      return;
    this.SelectedPlayer = nearbyPlayer;
  }

  private void UpdateSelectedItem()
  {
    FriendListBaseFriendFrame baseFriendFrame = this.FindBaseFriendFrame(FriendMgr.Get().GetSelectedFriend());
    if ((UnityEngine.Object) baseFriendFrame == (UnityEngine.Object) null)
    {
      if (this.items.SelectedIndex == -1)
        return;
      this.items.SelectedIndex = -1;
      if (!((UnityEngine.Object) this.m_removeFriendPopup != (UnityEngine.Object) null))
        return;
      this.m_removeFriendPopup.Hide();
      this.m_removeFriendPopup = (AlertPopup) null;
      if (this.RemoveFriendPopupClosed == null)
        return;
      this.RemoveFriendPopupClosed();
    }
    else
      this.items.SelectedIndex = this.items.IndexOf((ITouchListItem) baseFriendFrame.GetComponent<MobileFriendListItem>());
  }

  private void UpdateRAFState()
  {
    if (GameUtils.ShouldShowSetRotationIntro() || SceneMgr.Get().GetMode() == SceneMgr.Mode.LOGIN || ((UnityEngine.Object) WelcomeQuests.Get() != (UnityEngine.Object) null || GameUtils.GetNextTutorial() != 0))
      this.SetRAFButtonEnabled(false);
    this.UpdateRAFGlowLayout();
  }

  private void UpdateRAFGlowLayout()
  {
    if ((UnityEngine.Object) this.rafButtonGlowBone != (UnityEngine.Object) null)
      this.rafButtonButtonGlow.transform.parent = this.rafButtonGlowBone.transform;
    else
      this.rafButtonButtonGlow.transform.position = this.rafButton.transform.position;
  }

  private void InitButtons()
  {
    this.addFriendButton.SetText(GameStrings.Get("GLOBAL_FRIENDLIST_ADD_FRIEND_BUTTON"));
    this.addFriendButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnAddFriendButtonReleased));
    this.removeFriendButton.SetText(GameStrings.Get("GLOBAL_FRIENDLIST_REMOVE_FRIEND_BUTTON"));
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.removeFriendButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnEditFriendsButtonReleased));
    else
      this.removeFriendButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRemoveFriendButtonReleased));
    this.rafButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRAFButtonReleased));
    this.rafButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnRAFButtonOver));
    this.rafButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnRAFButtonOut));
    this.m_ignoreObjectsForWorldBound.Add(this.rafButtonButtonGlow.gameObject);
  }

  private bool GetShowHeaderSection(Option setoption)
  {
    return !(bool) Options.Get().GetOption(setoption, (object) false);
  }

  private void SetShowHeaderSection(Option sectionoption, bool show)
  {
    if (this.GetShowHeaderSection(sectionoption) == show)
      return;
    Options.Get().SetOption(sectionoption, (object) !show);
  }

  private Transform GetBottomRightBone()
  {
    if ((UnityEngine.Object) this.scrollbar != (UnityEngine.Object) null && this.scrollbar.gameObject.activeSelf)
      return this.listInfo.bottomRightWithScrollbar;
    return this.listInfo.bottomRight;
  }

  private class NearbyPlayerUpdate
  {
    public FriendListFrame.NearbyPlayerUpdate.ChangeType Change;
    public BnetPlayer Player;

    public NearbyPlayerUpdate(FriendListFrame.NearbyPlayerUpdate.ChangeType c, BnetPlayer p)
    {
      this.Change = c;
      this.Player = p;
    }

    public enum ChangeType
    {
      Added,
      Removed,
    }
  }

  [Serializable]
  public class Me
  {
    public Spawner portraitRef;
    public UberText nameText;
    public UberText numberText;
    public UberText statusText;
    public GameObject m_MedalPatch;
    public TournamentMedal m_Medal;
  }

  [Serializable]
  public class RecentOpponent
  {
    public PegUIElement button;
    public UberText nameText;
  }

  [Serializable]
  public class Prefabs
  {
    public FriendListItemHeader headerItem;
    public FriendListRequestFrame requestItem;
    public FriendListCurrentGameFrame currentGameItem;
    public FriendListFriendFrame friendItem;
    public FriendListNearbyPlayerFrame nearbyPlayerItem;
    public AddFriendFrame addFriendFrame;
  }

  [Serializable]
  public class ListInfo
  {
    public Transform topLeft;
    public Transform bottomRight;
    public Transform bottomRightWithScrollbar;
    public FriendListFrame.HeaderBackgroundInfo requestBackgroundInfo;
    public FriendListFrame.HeaderBackgroundInfo currentGameBackgroundInfo;
  }

  [Serializable]
  public class HeaderBackgroundInfo
  {
    public Mesh mesh;
    public Material material;
  }

  public struct FriendListItem
  {
    private object m_item;

    public MobileFriendListItem.TypeFlags ItemFlags { get; private set; }

    public bool IsHeader
    {
      get
      {
        return (this.ItemFlags & MobileFriendListItem.TypeFlags.Header) != (MobileFriendListItem.TypeFlags) 0;
      }
    }

    public MobileFriendListItem.TypeFlags ItemMainType
    {
      get
      {
        if (this.IsHeader)
          return MobileFriendListItem.TypeFlags.Header;
        return this.SubType;
      }
    }

    public MobileFriendListItem.TypeFlags SubType
    {
      get
      {
        return this.ItemFlags & ~MobileFriendListItem.TypeFlags.Header;
      }
    }

    public FriendListItem(bool isHeader, MobileFriendListItem.TypeFlags itemType, object itemData)
    {
      if (!isHeader && itemData == null)
        Log.Henry.Print("FriendListItem: itemData is null! itemType=" + (object) itemType);
      this.m_item = itemData;
      this.ItemFlags = itemType;
      if (isHeader)
        this.ItemFlags |= MobileFriendListItem.TypeFlags.Header;
      else
        this.ItemFlags = this.ItemFlags & ~MobileFriendListItem.TypeFlags.Header;
    }

    public BnetPlayer GetFriend()
    {
      if ((this.ItemFlags & MobileFriendListItem.TypeFlags.Friend) == (MobileFriendListItem.TypeFlags) 0)
        return (BnetPlayer) null;
      return (BnetPlayer) this.m_item;
    }

    public BnetPlayer GetCurrentGame()
    {
      if ((this.ItemFlags & MobileFriendListItem.TypeFlags.CurrentGame) == (MobileFriendListItem.TypeFlags) 0)
        return (BnetPlayer) null;
      return (BnetPlayer) this.m_item;
    }

    public BnetPlayer GetNearbyPlayer()
    {
      if ((this.ItemFlags & MobileFriendListItem.TypeFlags.NearbyPlayer) == (MobileFriendListItem.TypeFlags) 0)
        return (BnetPlayer) null;
      return (BnetPlayer) this.m_item;
    }

    public BnetInvitation GetInvite()
    {
      if ((this.ItemFlags & MobileFriendListItem.TypeFlags.Request) == (MobileFriendListItem.TypeFlags) 0)
        return (BnetInvitation) null;
      return (BnetInvitation) this.m_item;
    }

    public override string ToString()
    {
      if (this.IsHeader)
        return string.Format("[{0}]Header", (object) this.SubType);
      return string.Format("[{0}]{1}", (object) this.ItemMainType, this.m_item);
    }

    public System.Type GetFrameType()
    {
      switch (this.ItemMainType)
      {
        case MobileFriendListItem.TypeFlags.Header:
          return typeof (FriendListItemHeader);
        case MobileFriendListItem.TypeFlags.Friend:
          return typeof (FriendListFriendFrame);
        case MobileFriendListItem.TypeFlags.CurrentGame:
          return typeof (FriendListCurrentGameFrame);
        case MobileFriendListItem.TypeFlags.NearbyPlayer:
          return typeof (FriendListNearbyPlayerFrame);
        case MobileFriendListItem.TypeFlags.Request:
          return typeof (FriendListRequestFrame);
        default:
          throw new Exception("Unknown ItemType: " + (object) this.ItemFlags + " (" + (object) this.ItemFlags + ")");
      }
    }
  }

  private class VirtualizedFriendsListBehavior : TouchList.ILongListBehavior
  {
    private int m_cachedMaxVisibleItems = -1;
    private HashSet<MobileFriendListItem> m_acquiredItems = new HashSet<MobileFriendListItem>();
    private const int MAX_FREELIST_ITEMS = 20;
    private FriendListFrame m_friendList;
    private List<MobileFriendListItem> m_freelist;
    private Bounds[] m_boundsByType;

    public List<MobileFriendListItem> FreeList
    {
      get
      {
        return this.m_freelist;
      }
    }

    public int AllItemsCount
    {
      get
      {
        return this.m_friendList.m_allItems.Count;
      }
    }

    public int MaxVisibleItems
    {
      get
      {
        if (this.m_cachedMaxVisibleItems >= 0)
          return this.m_cachedMaxVisibleItems;
        this.m_cachedMaxVisibleItems = 0;
        Vector2 clipSize = this.m_friendList.items.ClipSize;
        Bounds prefabBounds1 = FriendListFrame.VirtualizedFriendsListBehavior.GetPrefabBounds(this.m_friendList.prefabs.requestItem.gameObject);
        Bounds prefabBounds2 = FriendListFrame.VirtualizedFriendsListBehavior.GetPrefabBounds(this.m_friendList.prefabs.friendItem.gameObject);
        Bounds prefabBounds3 = FriendListFrame.VirtualizedFriendsListBehavior.GetPrefabBounds(this.m_friendList.prefabs.nearbyPlayerItem.gameObject);
        float num = Mathf.Min(prefabBounds1.max.y - prefabBounds1.min.y, prefabBounds2.max.y - prefabBounds2.min.y, prefabBounds3.max.y - prefabBounds3.min.y);
        if ((double) num > 0.0)
          this.m_cachedMaxVisibleItems = Mathf.CeilToInt(clipSize.y / num) + 3;
        return this.m_cachedMaxVisibleItems;
      }
    }

    public int MinBuffer
    {
      get
      {
        return 2;
      }
    }

    public int MaxAcquiredItems
    {
      get
      {
        return this.MaxVisibleItems + 2 * this.MinBuffer;
      }
    }

    private bool HasCollapsedHeaders
    {
      get
      {
        return this.m_friendList.m_headers.Any<KeyValuePair<MobileFriendListItem.TypeFlags, FriendListItemHeader>>((Func<KeyValuePair<MobileFriendListItem.TypeFlags, FriendListItemHeader>, bool>) (kv => !kv.Value.IsShowingContents));
      }
    }

    public VirtualizedFriendsListBehavior(FriendListFrame friendList)
    {
      this.m_friendList = friendList;
    }

    private static Bounds GetPrefabBounds(GameObject prefabGameObject)
    {
      GameObject go = UnityEngine.Object.Instantiate<GameObject>(prefabGameObject);
      go.SetActive(true);
      Bounds setPointBounds = TransformUtil.ComputeSetPointBounds(go);
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
      return setPointBounds;
    }

    public bool IsItemShowable(int allItemsIndex)
    {
      if (allItemsIndex < 0 || allItemsIndex >= this.AllItemsCount)
        return false;
      FriendListFrame.FriendListItem allItem = this.m_friendList.m_allItems[allItemsIndex];
      if (allItem.IsHeader)
        return true;
      FriendListItemHeader header = this.m_friendList.FindHeader(allItem.SubType);
      if ((UnityEngine.Object) header != (UnityEngine.Object) null)
        return header.IsShowingContents;
      return false;
    }

    public Vector3 GetItemSize(int allItemsIndex)
    {
      if (allItemsIndex < 0 || allItemsIndex >= this.AllItemsCount)
        return Vector3.zero;
      FriendListFrame.FriendListItem allItem = this.m_friendList.m_allItems[allItemsIndex];
      if (this.m_boundsByType == null)
        this.InitializeBoundsByTypeArray();
      return this.m_boundsByType[this.GetBoundsByTypeIndex(allItem.ItemMainType)].size;
    }

    public void ReleaseAllItems()
    {
      if (this.m_acquiredItems.Count == 0)
        return;
      if (this.m_freelist == null)
        this.m_freelist = new List<MobileFriendListItem>();
      using (HashSet<MobileFriendListItem>.Enumerator enumerator = this.m_acquiredItems.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MobileFriendListItem current = enumerator.Current;
          if (current.IsHeader)
            current.gameObject.SetActive(false);
          else if (this.m_freelist.Count >= 20)
          {
            UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
          }
          else
          {
            this.m_freelist.Add(current);
            current.gameObject.SetActive(false);
          }
          current.Unselected();
        }
      }
      this.m_acquiredItems.Clear();
    }

    public void ReleaseItem(ITouchListItem item)
    {
      MobileFriendListItem mobileFriendListItem = item as MobileFriendListItem;
      if ((UnityEngine.Object) mobileFriendListItem == (UnityEngine.Object) null)
        throw new ArgumentException("given item is not MobileFriendListItem: " + (object) item);
      if (this.m_freelist == null)
        this.m_freelist = new List<MobileFriendListItem>();
      if (mobileFriendListItem.IsHeader)
        mobileFriendListItem.gameObject.SetActive(false);
      else if (this.m_freelist.Count >= 20)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) item.gameObject);
      }
      else
      {
        this.m_freelist.Add(mobileFriendListItem);
        mobileFriendListItem.gameObject.SetActive(false);
      }
      if (!this.m_acquiredItems.Remove(mobileFriendListItem))
        Log.Henry.Print("VirtualizedFriendsListBehavior.ReleaseItem item not found in m_acquiredItems: {0}", (object) mobileFriendListItem);
      mobileFriendListItem.Unselected();
    }

    public ITouchListItem AcquireItem(int index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      FriendListFrame.VirtualizedFriendsListBehavior.\u003CAcquireItem\u003Ec__AnonStorey372 itemCAnonStorey372 = new FriendListFrame.VirtualizedFriendsListBehavior.\u003CAcquireItem\u003Ec__AnonStorey372();
      if (this.m_acquiredItems.Count >= this.MaxAcquiredItems)
        throw new Exception("Bug in ILongListBehavior? there are too many acquired items! index=" + (object) index + " max=" + (object) this.MaxAcquiredItems + " maxVisible=" + (object) this.MaxVisibleItems + " minBuffer=" + (object) this.MinBuffer + " acquiredItems.Count=" + (object) this.m_acquiredItems.Count + " hasCollapsedHeaders=" + (object) this.HasCollapsedHeaders);
      if (index < 0 || index >= this.m_friendList.m_allItems.Count)
        throw new IndexOutOfRangeException(string.Format("Invalid index, {0} has {1} elements.", (object) DebugUtils.GetHierarchyPathAndType((UnityEngine.Object) this.m_friendList, '.'), (object) this.m_friendList.m_allItems.Count));
      // ISSUE: reference to a compiler-generated field
      itemCAnonStorey372.item = this.m_friendList.m_allItems[index];
      // ISSUE: reference to a compiler-generated field
      MobileFriendListItem.TypeFlags itemMainType = itemCAnonStorey372.item.ItemMainType;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      itemCAnonStorey372.frameType = itemCAnonStorey372.item.GetFrameType();
      // ISSUE: reference to a compiler-generated field
      if (this.m_freelist != null && !itemCAnonStorey372.item.IsHeader)
      {
        // ISSUE: reference to a compiler-generated method
        int lastIndex = this.m_freelist.FindLastIndex(new Predicate<MobileFriendListItem>(itemCAnonStorey372.\u003C\u003Em__71));
        if (lastIndex >= 0 && (UnityEngine.Object) this.m_freelist[lastIndex] == (UnityEngine.Object) null)
        {
          for (int index1 = 0; index1 < this.m_freelist.Count; ++index1)
          {
            if ((UnityEngine.Object) this.m_freelist[index1] == (UnityEngine.Object) null)
            {
              this.m_freelist.RemoveAt(index1);
              --index1;
            }
          }
          // ISSUE: reference to a compiler-generated method
          lastIndex = this.m_freelist.FindLastIndex(new Predicate<MobileFriendListItem>(itemCAnonStorey372.\u003C\u003Em__72));
        }
        if (lastIndex >= 0)
        {
          MobileFriendListItem mobileFriendListItem = this.m_freelist[lastIndex];
          this.m_freelist.RemoveAt(lastIndex);
          switch (itemMainType)
          {
            case MobileFriendListItem.TypeFlags.Friend:
              FriendListFriendFrame component1 = mobileFriendListItem.GetComponent<FriendListFriendFrame>();
              // ISSUE: reference to a compiler-generated field
              component1.SetFriend(itemCAnonStorey372.item.GetFriend());
              this.m_friendList.FinishCreateVisualItem<FriendListFriendFrame>(component1, itemMainType, (ITouchListItem) this.m_friendList.FindHeader(itemMainType), component1.gameObject);
              bool activeSelf1 = component1.gameObject.activeSelf;
              component1.gameObject.SetActive(true);
              component1.UpdateFriend();
              if (!activeSelf1)
              {
                component1.gameObject.SetActive(activeSelf1);
                break;
              }
              break;
            case MobileFriendListItem.TypeFlags.NearbyPlayer:
              FriendListNearbyPlayerFrame component2 = mobileFriendListItem.GetComponent<FriendListNearbyPlayerFrame>();
              // ISSUE: reference to a compiler-generated field
              component2.SetNearbyPlayer(itemCAnonStorey372.item.GetNearbyPlayer());
              this.m_friendList.FinishCreateVisualItem<FriendListNearbyPlayerFrame>(component2, itemMainType, (ITouchListItem) this.m_friendList.FindHeader(itemMainType), component2.gameObject);
              bool activeSelf2 = component2.gameObject.activeSelf;
              component2.gameObject.SetActive(true);
              component2.UpdateNearbyPlayer();
              if (!activeSelf2)
              {
                component2.gameObject.SetActive(activeSelf2);
                break;
              }
              break;
            case MobileFriendListItem.TypeFlags.Request:
              FriendListRequestFrame component3 = mobileFriendListItem.GetComponent<FriendListRequestFrame>();
              // ISSUE: reference to a compiler-generated field
              component3.SetInvite(itemCAnonStorey372.item.GetInvite());
              this.m_friendList.FinishCreateVisualItem<FriendListRequestFrame>(component3, itemMainType, (ITouchListItem) this.m_friendList.FindHeader(itemMainType), component3.gameObject);
              bool activeSelf3 = component3.gameObject.activeSelf;
              component3.gameObject.SetActive(true);
              component3.UpdateInvite();
              if (!activeSelf3)
              {
                component3.gameObject.SetActive(activeSelf3);
                break;
              }
              break;
            default:
              // ISSUE: reference to a compiler-generated field
              throw new NotImplementedException("VirtualizedFriendsListBehavior.AcquireItem[reuse] frameType=" + itemCAnonStorey372.frameType.FullName + " itemType=" + (object) itemMainType);
          }
          mobileFriendListItem.gameObject.SetActive(true);
          this.m_acquiredItems.Add(mobileFriendListItem);
          return (ITouchListItem) mobileFriendListItem;
        }
      }
      MobileFriendListItem mobileFriendListItem1;
      switch (itemMainType)
      {
        case MobileFriendListItem.TypeFlags.Header:
          // ISSUE: reference to a compiler-generated field
          mobileFriendListItem1 = this.m_friendList.FindHeader(itemCAnonStorey372.item.SubType).GetComponent<MobileFriendListItem>();
          break;
        case MobileFriendListItem.TypeFlags.Friend:
          // ISSUE: reference to a compiler-generated field
          mobileFriendListItem1 = this.m_friendList.CreateFriendFrame(itemCAnonStorey372.item.GetFriend());
          break;
        case MobileFriendListItem.TypeFlags.NearbyPlayer:
          // ISSUE: reference to a compiler-generated field
          mobileFriendListItem1 = this.m_friendList.CreateNearbyPlayerFrame(itemCAnonStorey372.item.GetNearbyPlayer());
          break;
        case MobileFriendListItem.TypeFlags.Request:
          // ISSUE: reference to a compiler-generated field
          mobileFriendListItem1 = this.m_friendList.CreateRequestFrame(itemCAnonStorey372.item.GetInvite());
          break;
        default:
          // ISSUE: reference to a compiler-generated field
          throw new NotImplementedException("VirtualizedFriendsListBehavior.AcquireItem[new] type=" + itemCAnonStorey372.frameType.FullName);
      }
      this.m_acquiredItems.Add(mobileFriendListItem1);
      return (ITouchListItem) mobileFriendListItem1;
    }

    private void InitializeBoundsByTypeArray()
    {
      Array values = Enum.GetValues(typeof (MobileFriendListItem.TypeFlags));
      this.m_boundsByType = new Bounds[values.Length];
      for (int index = 0; index < values.Length; ++index)
      {
        MobileFriendListItem.TypeFlags itemType = (MobileFriendListItem.TypeFlags) values.GetValue(index);
        Component prefab = this.GetPrefab(itemType);
        this.m_boundsByType[this.GetBoundsByTypeIndex(itemType)] = !((UnityEngine.Object) prefab == (UnityEngine.Object) null) ? FriendListFrame.VirtualizedFriendsListBehavior.GetPrefabBounds(prefab.gameObject) : new Bounds();
      }
    }

    private int GetBoundsByTypeIndex(MobileFriendListItem.TypeFlags itemType)
    {
      switch (itemType)
      {
        case MobileFriendListItem.TypeFlags.Header:
          return 0;
        case MobileFriendListItem.TypeFlags.Friend:
          return 4;
        case MobileFriendListItem.TypeFlags.CurrentGame:
          return 3;
        case MobileFriendListItem.TypeFlags.NearbyPlayer:
          return 2;
        case MobileFriendListItem.TypeFlags.Request:
          return 1;
        default:
          throw new Exception("Unknown ItemType: " + (object) itemType + " (" + (object) itemType + ")");
      }
    }

    private Component GetPrefab(MobileFriendListItem.TypeFlags itemType)
    {
      switch (itemType)
      {
        case MobileFriendListItem.TypeFlags.Header:
          return (Component) this.m_friendList.prefabs.headerItem;
        case MobileFriendListItem.TypeFlags.Friend:
          return (Component) this.m_friendList.prefabs.friendItem;
        case MobileFriendListItem.TypeFlags.CurrentGame:
          return (Component) this.m_friendList.prefabs.currentGameItem;
        case MobileFriendListItem.TypeFlags.NearbyPlayer:
          return (Component) this.m_friendList.prefabs.nearbyPlayerItem;
        case MobileFriendListItem.TypeFlags.Request:
          return (Component) this.m_friendList.prefabs.requestItem;
        default:
          throw new Exception("Unknown ItemType: " + (object) itemType + " (" + (object) itemType + ")");
      }
    }
  }
}
