﻿// Decompiled with JetBrains decompiler
// Type: BouncingBlade
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BouncingBlade : SuperSpell
{
  public float m_BladeSpinningMaxVol = 1f;
  public float m_BladeSpinningRampTime = 0.3f;
  private List<BouncingBlade.Target> m_TargetQueue = new List<BouncingBlade.Target>();
  private const float DAMAGE_SPLAT_DELAY = 0.0f;
  private const float BLADE_ANIMATION_SPEED = 50f;
  private const float BLADE_BIRTH_TIME = 0.3f;
  private const int OFFSCREEN_HIT_PERCENT = 5;
  public GameObject m_BladeRoot;
  public GameObject m_Blade;
  public GameObject m_Trail;
  public GameObject m_HitBonesRoot;
  public List<ParticleSystem> m_SparkParticles;
  public ParticleSystem m_EndSparkParticles;
  public ParticleSystem m_EndBigSparkParticles;
  public List<BouncingBlade.HitBonesType> m_HitBones;
  public AudioSource m_BladeSpinning;
  public AudioSource m_BladeSpinningContinuous;
  public AudioSource m_BladeHitMinion;
  public AudioSource m_BladeHitBoardCorner;
  public AudioSource m_BladeHitOffScreen;
  public AudioSource m_StartSound;
  public AudioSource m_EndSound;
  public float m_BladeSpinningMinVol;
  private bool m_Running;
  private Vector3? m_NextPosition;
  private bool m_Animating;
  private bool m_isDone;
  private BouncingBlade.HitBonesType m_PreviousHitBone;
  private Vector3 m_OrgBladeScale;

  protected override void Awake()
  {
    base.Awake();
    Log.Kyle.Print("Awake()");
    this.m_BladeRoot.SetActive(false);
    this.m_PreviousHitBone = this.m_HitBones[this.m_HitBones.Count - 1];
    this.m_OrgBladeScale = this.m_BladeRoot.transform.localScale;
    this.m_BladeRoot.transform.localScale = Vector3.zero;
  }

  protected override void Start()
  {
    base.Start();
    Log.Kyle.Print("Start()");
    this.SetupBounceLocations();
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    if (this.m_targets.Count == 0)
    {
      this.m_isDone = true;
      this.m_BladeRoot.SetActive(false);
      --this.m_effectsPendingFinish;
      this.FinishIfPossible();
    }
    else
    {
      if (!this.m_Running)
      {
        this.m_BladeRoot.SetActive(true);
        this.m_Blade.SetActive(false);
        this.m_Trail.SetActive(false);
        this.m_Running = true;
        this.StartCoroutine(this.BladeRunner());
      }
      this.m_BladeRoot.transform.localScale = this.m_OrgBladeScale;
      this.m_isDone = false;
      bool flag = this.IsHandlingLastTaskList();
      for (int visualTargetIndex = 0; visualTargetIndex < this.m_targets.Count; ++visualTargetIndex)
      {
        GameObject target1 = this.m_targets[visualTargetIndex];
        int dataIndexForTarget = this.GetMetaDataIndexForTarget(visualTargetIndex);
        BouncingBlade.Target target2 = new BouncingBlade.Target();
        target2.VisualTarget = target1;
        target2.TargetPosition = target1.transform.position;
        target2.MetaDataIdx = dataIndexForTarget;
        target2.isMinion = true;
        if (visualTargetIndex == this.m_targets.Count - 1)
          target2.LastTarget = true;
        if (flag)
          target2.LastBlock = true;
        this.m_TargetQueue.Add(target2);
        if (!target2.LastTarget)
        {
          BouncingBlade.Target target3 = new BouncingBlade.Target();
          target3.TargetPosition = this.AcquireRandomBoardTarget(out target3.Offscreen);
          target3.isMinion = false;
          target3.LastTarget = false;
          this.m_TargetQueue.Add(target3);
        }
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator BladeRunner()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BouncingBlade.\u003CBladeRunner\u003Ec__Iterator2A9() { \u003C\u003Ef__this = this };
  }

  private void SetupBounceLocations()
  {
    Vector3 position = Board.Get().FindBone("CenterPointBone").transform.position;
    Vector3 localPosition = this.m_HitBonesRoot.transform.localPosition;
    this.m_HitBonesRoot.transform.position = position;
    using (List<BouncingBlade.HitBonesType>.Enumerator enumerator = this.m_HitBones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BouncingBlade.HitBonesType current = enumerator.Current;
        current.SetPosition(current.Bone.transform.position);
      }
    }
    this.m_HitBonesRoot.transform.localPosition = localPosition;
  }

  private void AnimateToNextTarget(BouncingBlade.Target target)
  {
    this.m_Animating = true;
    iTween.MoveTo(this.m_BladeRoot, iTween.Hash((object) "position", (object) target.TargetPosition, (object) "speed", (object) 50f, (object) "orienttopath", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "AnimationComplete", (object) "oncompleteparams", (object) target));
  }

  private void RampBladeVolume()
  {
    iTween.StopByName(this.m_BladeSpinning.gameObject, "BladeSpinningSound");
    SoundManager.Get().SetVolume(this.m_BladeSpinning, this.m_BladeSpinningMinVol);
    iTween.ValueTo(this.m_BladeSpinning.gameObject, iTween.Hash((object) "name", (object) "BladeSpinningSound", (object) "from", (object) this.m_BladeSpinningMinVol, (object) "to", (object) this.m_BladeSpinningMaxVol, (object) "time", (object) this.m_BladeSpinningRampTime, (object) "easetype", (object) iTween.EaseType.linear, (object) "onupdate", (object) (Action<object>) (amount => SoundManager.Get().SetVolume(this.m_BladeSpinning, (float) amount)), (object) "onupdatetarget", (object) this.m_BladeSpinning.gameObject));
  }

  private void AnimationComplete(BouncingBlade.Target target)
  {
    this.m_Animating = false;
    this.AnimateSparks();
    if (!target.LastBlock && !target.LastTarget)
      this.RampBladeVolume();
    AudioSource source = !target.isMinion ? (!target.Offscreen ? this.m_BladeHitBoardCorner : this.m_BladeHitOffScreen) : this.m_BladeHitMinion;
    if (!((UnityEngine.Object) source != (UnityEngine.Object) null))
      return;
    source.gameObject.transform.position = target.TargetPosition;
    SoundManager.Get().Play(source, true);
  }

  private void AnimateSparks()
  {
    using (List<ParticleSystem>.Enumerator enumerator = this.m_SparkParticles.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Play();
    }
  }

  private Vector3 AcquireRandomBoardTarget(out bool offscreen)
  {
    offscreen = false;
    if (UnityEngine.Random.Range(1, 100) < 5)
      offscreen = true;
    List<BouncingBlade.HitBonesType> hitBonesTypeList = new List<BouncingBlade.HitBonesType>();
    if (offscreen)
    {
      using (List<BouncingBlade.HitBonesType>.Enumerator enumerator = this.m_HitBones.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          BouncingBlade.HitBonesType current = enumerator.Current;
          if (current.Direction != BouncingBlade.HIT_DIRECTIONS.E && current.Direction != BouncingBlade.HIT_DIRECTIONS.NE && (current.Direction != BouncingBlade.HIT_DIRECTIONS.NW && current.Direction != BouncingBlade.HIT_DIRECTIONS.SE) && (current.Direction != BouncingBlade.HIT_DIRECTIONS.SW && current.Direction != this.m_PreviousHitBone.Direction))
            hitBonesTypeList.Add(current);
        }
      }
    }
    else
    {
      using (List<BouncingBlade.HitBonesType>.Enumerator enumerator = this.m_HitBones.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          BouncingBlade.HitBonesType current = enumerator.Current;
          if (current.Direction != BouncingBlade.HIT_DIRECTIONS.E_OFFSCREEN && current.Direction != BouncingBlade.HIT_DIRECTIONS.N_OFFSCREEN && (current.Direction != BouncingBlade.HIT_DIRECTIONS.S_OFFSCREEN && current.Direction != BouncingBlade.HIT_DIRECTIONS.W_OFFSCREEN) && current.Direction != this.m_PreviousHitBone.Direction)
            hitBonesTypeList.Add(current);
        }
      }
    }
    int index = UnityEngine.Random.Range(0, hitBonesTypeList.Count);
    this.m_PreviousHitBone = hitBonesTypeList[index];
    return hitBonesTypeList[index].GetPosition();
  }

  public enum HIT_DIRECTIONS
  {
    NW,
    NE,
    E,
    SW,
    SE,
    N_OFFSCREEN,
    E_OFFSCREEN,
    W_OFFSCREEN,
    S_OFFSCREEN,
  }

  [Serializable]
  public class HitBonesType
  {
    public BouncingBlade.HIT_DIRECTIONS Direction;
    public GameObject Bone;
    private Vector3 m_Position;

    public void SetPosition(Vector3 pos)
    {
      this.m_Position = pos;
    }

    public Vector3 GetPosition()
    {
      return this.m_Position;
    }
  }

  [Serializable]
  public class Target
  {
    public GameObject VisualTarget;
    public Vector3 TargetPosition;
    public bool isMinion;
    public int MetaDataIdx;
    public bool LastTarget;
    public bool LastBlock;
    public bool Offscreen;
  }
}
