﻿// Decompiled with JetBrains decompiler
// Type: SpellTargetInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class SpellTargetInfo
{
  public int m_RandomTargetCountMin = 8;
  public int m_RandomTargetCountMax = 10;
  public SpellTargetBehavior m_Behavior;
  public bool m_SuppressPlaySounds;
}
