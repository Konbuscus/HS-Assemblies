﻿// Decompiled with JetBrains decompiler
// Type: HeroSkinInfoManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class HeroSkinInfoManager : MonoBehaviour
{
  public float m_animationTime = 0.5f;
  public MusicPlaylistType m_defaultHeroMusic = MusicPlaylistType.UI_CMHeroSkinPreview;
  public GameObject m_previewPane;
  public GameObject m_vanillaHeroFrame;
  public MeshRenderer m_vanillaHeroPreviewQuad;
  public UberText m_vanillaHeroTitle;
  public UberText m_vanillaHeroDescription;
  public UIBButton m_vanillaHeroFavoriteButton;
  public GameObject m_newHeroFrame;
  public MeshRenderer m_newHeroPreviewQuad;
  public UberText m_newHeroTitle;
  public UberText m_newHeroDescription;
  public UIBButton m_newHeroFavoriteButton;
  public PegUIElement m_offClicker;
  public Material m_defaultPreviewMaterial;
  public Material m_vanillaHeroNonPremiumMaterial;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_enterPreviewSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_exitPreviewSound;
  private string m_currentCardId;
  private CollectionHeroDef m_currentHeroDef;
  private CardHeroDbfRecord m_currentHeroRecord;
  private EntityDef m_currentEntityDef;
  private TAG_PREMIUM m_currentPremium;
  private static HeroSkinInfoManager s_instance;
  private bool m_animating;
  private bool m_hasEnteredHeroSkinPreview;
  private MusicPlaylistType m_prevPlaylist;

  public static HeroSkinInfoManager Get()
  {
    if ((UnityEngine.Object) HeroSkinInfoManager.s_instance == (UnityEngine.Object) null)
      HeroSkinInfoManager.s_instance = AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "HeroSkinInfoManager" : "HeroSkinInfoManager_phone", true, false).GetComponent<HeroSkinInfoManager>();
    return HeroSkinInfoManager.s_instance;
  }

  private void Awake()
  {
    this.m_previewPane.SetActive(false);
    this.SetupUI();
  }

  private void OnDestroy()
  {
    this.CancelPreview();
    HeroSkinInfoManager.s_instance = (HeroSkinInfoManager) null;
  }

  private static bool OnNavigateBack()
  {
    if ((UnityEngine.Object) HeroSkinInfoManager.Get() == (UnityEngine.Object) null)
      return true;
    HeroSkinInfoManager.Get().CancelPreview();
    return true;
  }

  public void EnterPreview(CollectionCardVisual cardVisual)
  {
    if (this.m_animating)
      return;
    this.m_currentEntityDef = cardVisual.GetActor().GetEntityDef();
    this.m_currentPremium = cardVisual.GetActor().GetPremium();
    Navigation.PushUnique(new Navigation.NavigateBackHandler(HeroSkinInfoManager.OnNavigateBack));
    if (this.LoadHeroDef(this.m_currentEntityDef.GetCardId()))
    {
      if (this.m_currentEntityDef.GetCardSet() == TAG_CARD_SET.CORE)
      {
        this.m_vanillaHeroTitle.Text = this.m_currentEntityDef.GetName();
        this.m_vanillaHeroDescription.Text = (string) this.m_currentHeroRecord.Description;
        Texture portraitTexture = (Texture) null;
        Material material;
        if (this.m_currentPremium == TAG_PREMIUM.NORMAL)
        {
          material = this.m_vanillaHeroNonPremiumMaterial;
          portraitTexture = cardVisual.GetActor().GetCardDef().GetPortraitTexture();
        }
        else
          material = cardVisual.GetActor().GetCardDef().GetPremiumPortraitMaterial();
        this.m_newHeroFrame.SetActive(false);
        this.m_vanillaHeroFrame.SetActive(true);
        this.AssignVanillaHeroPreviewMaterial(material, portraitTexture);
      }
      else
      {
        this.m_newHeroTitle.Text = this.m_currentEntityDef.GetName();
        this.m_newHeroDescription.Text = (string) this.m_currentHeroRecord.Description;
        this.m_newHeroFrame.SetActive(true);
        this.m_vanillaHeroFrame.SetActive(false);
        this.AssignNewHeroPreviewMaterial(this.m_currentHeroDef.m_previewTexture, cardVisual.GetActor().GetPortraitTexture());
      }
      if ((UnityEngine.Object) this.m_currentHeroDef != (UnityEngine.Object) null && this.m_currentHeroDef.m_collectionManagerPreviewEmote != EmoteType.INVALID)
        GameUtils.LoadCardDefEmoteSound(cardVisual.GetActor().GetCardDef(), this.m_currentHeroDef.m_collectionManagerPreviewEmote, (GameUtils.EmoteSoundLoaded) (cardSpell =>
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          HeroSkinInfoManager.\u003CEnterPreview\u003Ec__AnonStorey3A3 previewCAnonStorey3A3 = new HeroSkinInfoManager.\u003CEnterPreview\u003Ec__AnonStorey3A3();
          // ISSUE: reference to a compiler-generated field
          previewCAnonStorey3A3.cardSpell = cardSpell;
          // ISSUE: reference to a compiler-generated field
          if (!((UnityEngine.Object) previewCAnonStorey3A3.cardSpell != (UnityEngine.Object) null))
            return;
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          previewCAnonStorey3A3.cardSpell.AddFinishedCallback(new Spell.FinishedCallback(previewCAnonStorey3A3.\u003C\u003Em__FF));
          // ISSUE: reference to a compiler-generated field
          previewCAnonStorey3A3.cardSpell.Reactivate();
        }));
      this.m_hasEnteredHeroSkinPreview = true;
    }
    else
    {
      Debug.LogError((object) "Could not load entity def for hero skin, preview will not be shown");
      this.m_newHeroFrame.SetActive(false);
      this.m_vanillaHeroFrame.SetActive(false);
    }
    this.m_previewPane.SetActive(true);
    this.m_offClicker.gameObject.SetActive(true);
    this.m_animating = true;
    iTween.ScaleFrom(this.m_previewPane, iTween.Hash((object) "scale", (object) new Vector3(0.01f, 0.01f, 0.01f), (object) "time", (object) this.m_animationTime, (object) "easeType", (object) iTween.EaseType.easeOutCirc, (object) "oncomplete", (object) (Action<object>) (e => this.m_animating = false)));
    bool flag = false;
    if (!CollectionManager.Get().IsInEditMode())
    {
      TAG_CLASS heroClass = this.m_currentEntityDef.GetClass();
      NetCache.CardDefinition favoriteHero = CollectionManager.Get().GetFavoriteHero(heroClass);
      List<CollectibleCard> bestHeroesIown = CollectionManager.Get().GetBestHeroesIOwn(heroClass);
      flag = favoriteHero != null && favoriteHero.Name != this.m_currentCardId && bestHeroesIown.Count > 1;
    }
    if (this.m_currentEntityDef.GetCardSet() == TAG_CARD_SET.CORE)
    {
      this.m_vanillaHeroFavoriteButton.SetEnabled(flag);
      this.m_vanillaHeroFavoriteButton.Flip(flag);
    }
    else
    {
      this.m_newHeroFavoriteButton.SetEnabled(flag);
      this.m_newHeroFavoriteButton.Flip(flag);
    }
    if (!string.IsNullOrEmpty(this.m_enterPreviewSound))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_enterPreviewSound));
    this.PlayHeroMusic();
    FullScreenFXMgr.Get().StartStandardBlurVignette(this.m_animationTime);
  }

  public void CancelPreview()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    HeroSkinInfoManager.\u003CCancelPreview\u003Ec__AnonStorey3A4 previewCAnonStorey3A4 = new HeroSkinInfoManager.\u003CCancelPreview\u003Ec__AnonStorey3A4();
    // ISSUE: reference to a compiler-generated field
    previewCAnonStorey3A4.\u003C\u003Ef__this = this;
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(HeroSkinInfoManager.OnNavigateBack));
    if (this.m_animating || !this.m_hasEnteredHeroSkinPreview)
      return;
    this.m_hasEnteredHeroSkinPreview = false;
    // ISSUE: reference to a compiler-generated field
    previewCAnonStorey3A4.origScale = this.m_previewPane.transform.localScale;
    this.m_animating = true;
    // ISSUE: reference to a compiler-generated method
    iTween.ScaleTo(this.m_previewPane, iTween.Hash((object) "scale", (object) new Vector3(0.01f, 0.01f, 0.01f), (object) "time", (object) this.m_animationTime, (object) "easeType", (object) iTween.EaseType.easeOutCirc, (object) "oncomplete", (object) new Action<object>(previewCAnonStorey3A4.\u003C\u003Em__F9)));
    if (!string.IsNullOrEmpty(this.m_exitPreviewSound))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_exitPreviewSound));
    this.StopHeroMusic();
    FullScreenFXMgr.Get().EndStandardBlurVignette(this.m_animationTime, (FullScreenFXMgr.EffectListener) null);
  }

  private bool LoadHeroDef(string cardId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    HeroSkinInfoManager.\u003CLoadHeroDef\u003Ec__AnonStorey3A5 defCAnonStorey3A5 = new HeroSkinInfoManager.\u003CLoadHeroDef\u003Ec__AnonStorey3A5();
    // ISSUE: reference to a compiler-generated field
    defCAnonStorey3A5.cardId = cardId;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (this.m_currentCardId == defCAnonStorey3A5.cardId && string.IsNullOrEmpty(defCAnonStorey3A5.cardId))
      return true;
    // ISSUE: reference to a compiler-generated method
    CardHeroDbfRecord cardHeroDbfRecord = GameDbf.CardHero.GetRecords().Find(new Predicate<CardHeroDbfRecord>(defCAnonStorey3A5.\u003C\u003Em__FA));
    if (cardHeroDbfRecord == null)
    {
      // ISSUE: reference to a compiler-generated field
      Debug.LogWarning((object) string.Format("Unable to find hero with ID: {0} in HERO.xml", (object) defCAnonStorey3A5.cardId));
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    CardDef cardDef = DefLoader.Get().GetCardDef(defCAnonStorey3A5.cardId, (CardPortraitQuality) null);
    CollectionHeroDef collectionHeroDef = GameUtils.LoadGameObjectWithComponent<CollectionHeroDef>(cardDef.m_CollectionHeroDefPath);
    if ((UnityEngine.Object) collectionHeroDef == (UnityEngine.Object) null)
    {
      Debug.LogWarning((object) string.Format("Hero def does not exist on object: {0}", (object) cardDef.m_CollectionHeroDefPath));
      return false;
    }
    if ((UnityEngine.Object) this.m_currentHeroDef != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_currentHeroDef.gameObject);
    // ISSUE: reference to a compiler-generated field
    this.m_currentCardId = defCAnonStorey3A5.cardId;
    this.m_currentHeroDef = collectionHeroDef;
    this.m_currentHeroRecord = cardHeroDbfRecord;
    return true;
  }

  private void SetupUI()
  {
    this.m_newHeroFavoriteButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e =>
    {
      this.SetFavoriteHero();
      this.CancelPreview();
    }));
    if ((UnityEngine.Object) this.m_vanillaHeroFavoriteButton != (UnityEngine.Object) null && (UnityEngine.Object) this.m_vanillaHeroFavoriteButton != (UnityEngine.Object) this.m_newHeroFavoriteButton)
      this.m_vanillaHeroFavoriteButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e =>
      {
        this.SetFavoriteHero();
        this.CancelPreview();
      }));
    this.m_offClicker.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.CancelPreview()));
    this.m_offClicker.AddEventListener(UIEventType.RIGHTCLICK, (UIEvent.Handler) (e => this.CancelPreview()));
  }

  private void SetFavoriteHero()
  {
    Network.SetFavoriteHero(this.m_currentEntityDef.GetClass(), new NetCache.CardDefinition()
    {
      Name = this.m_currentEntityDef.GetCardId(),
      Premium = this.m_currentPremium
    });
  }

  private void AssignVanillaHeroPreviewMaterial(Material material, Texture portraitTexture)
  {
    if ((UnityEngine.Object) portraitTexture != (UnityEngine.Object) null)
    {
      Material[] materials = this.m_vanillaHeroPreviewQuad.GetComponent<Renderer>().materials;
      materials[1] = material;
      materials[1].SetTexture("_MainTex", portraitTexture);
      this.m_vanillaHeroPreviewQuad.GetComponent<Renderer>().materials = materials;
    }
    else
      RenderUtils.SetMaterial((Renderer) this.m_vanillaHeroPreviewQuad, 1, material);
  }

  private void AssignNewHeroPreviewMaterial(Material material, Texture portraitTexture)
  {
    if ((UnityEngine.Object) material == (UnityEngine.Object) null)
    {
      this.m_newHeroPreviewQuad.GetComponent<Renderer>().material = this.m_defaultPreviewMaterial;
      this.m_newHeroPreviewQuad.GetComponent<Renderer>().material.mainTexture = portraitTexture;
    }
    else
      this.m_newHeroPreviewQuad.GetComponent<Renderer>().material = material;
  }

  private void PlayHeroMusic()
  {
    MusicPlaylistType type = (UnityEngine.Object) this.m_currentHeroDef == (UnityEngine.Object) null || this.m_currentHeroDef.m_heroPlaylist == MusicPlaylistType.Invalid ? this.m_defaultHeroMusic : this.m_currentHeroDef.m_heroPlaylist;
    if (type == MusicPlaylistType.Invalid)
      return;
    this.m_prevPlaylist = MusicManager.Get().GetCurrentPlaylist();
    MusicManager.Get().StartPlaylist(type);
  }

  private void StopHeroMusic()
  {
    MusicManager.Get().StartPlaylist(this.m_prevPlaylist);
  }
}
