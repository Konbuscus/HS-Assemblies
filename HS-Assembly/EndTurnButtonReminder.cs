﻿// Decompiled with JetBrains decompiler
// Type: EndTurnButtonReminder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class EndTurnButtonReminder : MonoBehaviour
{
  public float m_MaxDelaySec = 0.3f;
  private List<Card> m_cardsWaitingToRemind = new List<Card>();

  public bool ShowFriendlySidePlayerTurnReminder()
  {
    GameState state = GameState.Get();
    if (state.IsMulliganManagerActive())
      return false;
    Player friendlySidePlayer = state.GetFriendlySidePlayer();
    if (friendlySidePlayer == null || !friendlySidePlayer.IsCurrentPlayer())
      return false;
    ZoneMgr zoneMgr = ZoneMgr.Get();
    if ((Object) zoneMgr == (Object) null)
      return false;
    ZonePlay zoneOfType = zoneMgr.FindZoneOfType<ZonePlay>(Player.Side.FRIENDLY);
    if ((Object) zoneOfType == (Object) null)
      return false;
    List<Card> cardsToRemindList = this.GenerateCardsToRemindList(state, zoneOfType.GetCards());
    if (cardsToRemindList.Count == 0)
      return true;
    this.PlayReminders(cardsToRemindList);
    return true;
  }

  private List<Card> GenerateCardsToRemindList(GameState state, List<Card> originalList)
  {
    List<Card> cardList = new List<Card>();
    for (int index = 0; index < originalList.Count; ++index)
    {
      Card original = originalList[index];
      if (state.HasResponse(original.GetEntity()))
        cardList.Add(original);
    }
    return cardList;
  }

  private void PlayReminders(List<Card> cards)
  {
    int index1;
    do
    {
      index1 = Random.Range(0, cards.Count);
    }
    while (this.m_cardsWaitingToRemind.Contains(cards[index1]));
    for (int index2 = 0; index2 < cards.Count; ++index2)
    {
      Card card = cards[index2];
      Spell actorSpell = card.GetActorSpell(SpellType.WIGGLE, true);
      if (!((Object) actorSpell == (Object) null) && actorSpell.GetActiveState() == SpellStateType.NONE && !this.m_cardsWaitingToRemind.Contains(card))
      {
        if (index2 == index1)
        {
          actorSpell.Activate();
        }
        else
        {
          float num = Random.Range(0.0f, this.m_MaxDelaySec);
          if (Mathf.Approximately(num, 0.0f))
          {
            actorSpell.Activate();
          }
          else
          {
            this.m_cardsWaitingToRemind.Add(card);
            this.StartCoroutine(this.WaitAndPlayReminder(card, actorSpell, num));
          }
        }
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitAndPlayReminder(Card card, Spell reminderSpell, float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EndTurnButtonReminder.\u003CWaitAndPlayReminder\u003Ec__IteratorAD() { delay = delay, card = card, reminderSpell = reminderSpell, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003Ecard = card, \u003C\u0024\u003EreminderSpell = reminderSpell, \u003C\u003Ef__this = this };
  }
}
