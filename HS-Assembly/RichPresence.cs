﻿// Decompiled with JetBrains decompiler
// Type: RichPresence
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class RichPresence
{
  public static readonly FourCC STATUS_STREAMID = new FourCC("stat");
  public static readonly FourCC TUTORIAL_STREAMID = new FourCC("tut");
  public static readonly FourCC SCENARIOS_STREAMID = new FourCC("scen");
  public static readonly Map<System.Type, FourCC> s_streamIds = new Map<System.Type, FourCC>() { { typeof (PresenceStatus), RichPresence.STATUS_STREAMID }, { typeof (PresenceTutorial), RichPresence.TUTORIAL_STREAMID }, { typeof (ScenarioDbId), RichPresence.SCENARIOS_STREAMID } };
  public const uint FIELD_INDEX_START = 458752;
}
