﻿// Decompiled with JetBrains decompiler
// Type: NAX14_Sapphiron
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class NAX14_Sapphiron : NAX_MissionEntity
{
  private bool m_cardKtLinePlayed;
  private int m_numTimesFrostBreathMisses;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_NAX14_01_EMOTE_01");
    this.PreloadSound("VO_NAX14_01_EMOTE_02");
    this.PreloadSound("VO_NAX14_01_EMOTE_03");
    this.PreloadSound("VO_NAX14_01_CARD_01");
    this.PreloadSound("VO_NAX14_01_HP_01");
    this.PreloadSound("VO_KT_SAPPHIRON2_84");
    this.PreloadSound("VO_KT_SAPPHIRON3_85");
    this.PreloadSound("VO_KT_SAPPHIRON4_ALT_87");
    this.PreloadSound("VO_KT_SAPPHIRON5_88");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX14_01_EMOTE_01",
            m_stringTag = "VO_NAX14_01_EMOTE_01"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX14_01_EMOTE_02",
            m_stringTag = "VO_NAX14_01_EMOTE_02"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX14_01_EMOTE_03",
            m_stringTag = "VO_NAX14_01_EMOTE_03"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX14_Sapphiron.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1E5() { turn = turn, \u003C\u0024\u003Eturn = turn };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX14_Sapphiron.\u003CHandleGameOverWithTiming\u003Ec__Iterator1E6() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX14_Sapphiron.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1E7() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX14_Sapphiron.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1E8() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }
}
