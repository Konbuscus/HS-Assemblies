﻿// Decompiled with JetBrains decompiler
// Type: CreateButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CreateButton : CraftingButton
{
  protected override void OnRelease()
  {
    if (CraftingManager.Get().GetPendingServerTransaction() != null)
      return;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.GetComponent<Animation>().Play("CardExchange_ButtonPress2_phone");
    else
      this.GetComponent<Animation>().Play("CardExchange_ButtonPress2");
    string cardId = CraftingManager.Get().GetShownActor().GetEntityDef().GetCardId();
    DeckRuleset deckRuleset = CollectionManager.Get().GetDeckRuleset();
    bool flag;
    if (deckRuleset != null)
    {
      flag = !deckRuleset.Filter(DefLoader.Get().GetEntityDef(cardId));
    }
    else
    {
      TAG_PREMIUM premium = CraftingManager.Get().GetShownActor().GetPremium();
      flag = GameUtils.IsSetRotated(CollectionManager.Get().GetCard(cardId, premium).Set);
    }
    if (CraftingManager.Get().GetNumClientTransactions() != 0)
      flag = false;
    if (flag)
    {
      AlertPopup.PopupInfo info = new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_CRAFTING_WILD_CARD_HEADER"),
        m_cancelText = GameStrings.Get("GLUE_CRAFTING_WILD_CARD_WARNING_CANCEL"),
        m_confirmText = GameStrings.Get("GLUE_CRAFTING_WILD_CARD_WARNING_CONFIRM"),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
        m_responseCallback = new AlertPopup.ResponseCallback(this.OnConfirmCreateResponse)
      };
      if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
      {
        info.m_headerText = GameStrings.Get("GLUE_CRAFTING_WILD_CARD_TAVERN_BRAWL_HEADER");
        info.m_text = GameStrings.Get("GLUE_CRAFTING_WILD_CARD_TAVERN_BRAWL_DESC");
      }
      else
        info.m_text = !CollectionManager.Get().AccountEverHadWildCards() ? GameStrings.Get("GLUE_CRAFTING_WILD_CARD_FIRST_WILD_DESC") : GameStrings.Get("GLUE_CRAFTING_WILD_CARD_DESC");
      DialogManager.Get().ShowPopup(info);
    }
    else
      this.DoCreate();
  }

  private void OnConfirmCreateResponse(AlertPopup.Response response, object userData)
  {
    if (response != AlertPopup.Response.CONFIRM)
      return;
    if (!CollectionManager.Get().AccountEverHadWildCards())
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_CRAFTING_WILD_CARD_HEADER"),
        m_text = GameStrings.Get("GLUE_CRAFTING_WILD_CARD_INTRO_DESC"),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_responseCallback = (AlertPopup.ResponseCallback) ((r, data) =>
        {
          this.DoCreate();
          Options.Get().SetBool(Option.HAS_SEEN_STANDARD_MODE_TUTORIAL, true);
          Options.Get().SetInt(Option.SET_ROTATION_INTRO_PROGRESS, 1);
          UserAttentionManager.StopBlocking(UserAttentionBlocker.SET_ROTATION_INTRO);
          Options.Get().SetBool(Option.SHOW_SWITCH_TO_WILD_ON_PLAY_SCREEN, true);
          Options.Get().SetBool(Option.SHOW_SWITCH_TO_WILD_ON_CREATE_DECK, true);
        })
      });
    else
      this.DoCreate();
  }

  public override void EnableButton()
  {
    if (CraftingManager.Get().GetNumClientTransactions() < 0)
    {
      this.EnterUndoMode();
    }
    else
    {
      this.labelText.Text = GameStrings.Get("GLUE_CRAFTING_CREATE");
      base.EnableButton();
    }
  }

  private void DoCreate()
  {
    CraftingManager.Get().CreateButtonPressed();
  }
}
