﻿// Decompiled with JetBrains decompiler
// Type: AdventureRewardsPreview
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class AdventureRewardsPreview : MonoBehaviour
{
  [SerializeField]
  private float m_CardWidth = 30f;
  [SerializeField]
  private float m_CardSpacing = 5f;
  [SerializeField]
  private float m_CardClumpAngleIncrement = 10f;
  [SerializeField]
  private Vector3 m_CardClumpSpacing = Vector3.zero;
  [CustomEditField(Sections = "Cards Preview")]
  public float m_ShowHideAnimationTime = 0.15f;
  private List<List<Actor>> m_CardBatches = new List<List<Actor>>();
  private List<AdventureRewardsPreview.OnHide> m_OnHideListeners = new List<AdventureRewardsPreview.OnHide>();
  [CustomEditField(Sections = "Cards Preview")]
  public GameObject m_CardsContainer;
  [CustomEditField(Sections = "Cards Preview")]
  public UberText m_HeaderTextObject;
  [CustomEditField(Sections = "Cards Preview")]
  public PegUIElement m_BackButton;
  [CustomEditField(Sections = "Cards Preview")]
  public GameObject m_ClickBlocker;
  [CustomEditField(Sections = "Cards Preview")]
  public UIBScrollable m_DisableScrollbar;
  [CustomEditField(Sections = "Cards Preview")]
  public bool m_PreviewCardsExpandable;
  [CustomEditField(Sections = "Cards Preview/Hidden Cards")]
  public GameObject m_HiddenCardsLabelObject;
  [CustomEditField(Sections = "Cards Preview/Hidden Cards")]
  public UberText m_HiddenCardsLabel;
  [CustomEditField(Parent = "m_PreviewCardsExpandable", Sections = "Cards Preview")]
  public AdventureRewardsDisplayArea m_CardsPreviewDisplay;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_PreviewAppearSound;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_PreviewShrinkSound;
  private int m_HiddenCardCount;

  [CustomEditField(Sections = "Cards Preview")]
  public float CardWidth
  {
    get
    {
      return this.m_CardWidth;
    }
    set
    {
      this.m_CardWidth = value;
      this.UpdateCardPositions();
    }
  }

  [CustomEditField(Sections = "Cards Preview")]
  public float CardSpacing
  {
    get
    {
      return this.m_CardSpacing;
    }
    set
    {
      this.m_CardSpacing = value;
      this.UpdateCardPositions();
    }
  }

  [CustomEditField(Sections = "Cards Preview")]
  public float CardClumpAngleIncrement
  {
    get
    {
      return this.m_CardClumpAngleIncrement;
    }
    set
    {
      this.m_CardClumpAngleIncrement = value;
      this.UpdateCardPositions();
    }
  }

  [CustomEditField(Sections = "Cards Preview")]
  public Vector3 CardClumpSpacing
  {
    get
    {
      return this.m_CardClumpSpacing;
    }
    set
    {
      this.m_CardClumpSpacing = value;
      this.UpdateCardPositions();
    }
  }

  private void Awake()
  {
    if (!((UnityEngine.Object) this.m_BackButton != (UnityEngine.Object) null))
      return;
    this.m_BackButton.AddEventListener(UIEventType.PRESS, (UIEvent.Handler) (e => Navigation.GoBack()));
  }

  public void AddHideListener(AdventureRewardsPreview.OnHide dlg)
  {
    this.m_OnHideListeners.Add(dlg);
  }

  public void RemoveHideListener(AdventureRewardsPreview.OnHide dlg)
  {
    this.m_OnHideListeners.Remove(dlg);
  }

  private bool OnNavigateBack()
  {
    this.Show(false);
    return true;
  }

  public void SetHeaderText(string text)
  {
    this.m_HeaderTextObject.Text = GameStrings.Format("GLUE_ADVENTURE_REWARDS_PREVIEW_HEADER", (object) text);
  }

  public void AddSpecificCards(List<string> cardIds)
  {
    using (List<string>.Enumerator enumerator = cardIds.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.AddCardBatch(new List<string>()
        {
          enumerator.Current
        });
    }
  }

  public void AddCardBatch(int scenarioId)
  {
    this.AddCardBatch(AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario(scenarioId));
  }

  public void AddCardBatch(List<CardRewardData> rewards)
  {
    List<string> cardIds = new List<string>();
    using (List<CardRewardData>.Enumerator enumerator = rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardRewardData current = enumerator.Current;
        cardIds.Add(current.CardID);
      }
    }
    this.AddCardBatch(cardIds);
  }

  public void AddCardBatch(List<string> cardIds)
  {
    List<Actor> cardBatch = new List<Actor>();
    this.m_CardBatches.Add(cardBatch);
    this.AddCardBatch(cardIds, cardBatch);
  }

  public void SetHiddenCardCount(int hiddenCardCount)
  {
    this.m_HiddenCardCount = hiddenCardCount;
  }

  public void Reset()
  {
    using (List<List<Actor>>.Enumerator enumerator1 = this.m_CardBatches.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Actor>.Enumerator enumerator2 = enumerator1.Current.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Actor current = enumerator2.Current;
            if ((UnityEngine.Object) current != (UnityEngine.Object) null)
              UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
          }
        }
      }
    }
    this.m_HiddenCardCount = 0;
    this.m_CardBatches.Clear();
  }

  public void Show(bool show)
  {
    if ((UnityEngine.Object) this.m_ClickBlocker != (UnityEngine.Object) null)
      this.m_ClickBlocker.SetActive(show);
    if ((UnityEngine.Object) this.m_DisableScrollbar != (UnityEngine.Object) null)
      this.m_DisableScrollbar.Enable(!show);
    if (show)
    {
      this.UpdateCardPositions();
      FullScreenFXMgr.Get().StartStandardBlurVignette(this.m_ShowHideAnimationTime);
      this.gameObject.SetActive(true);
      iTween.ScaleFrom(this.gameObject, iTween.Hash((object) "scale", (object) (Vector3.one * 0.05f), (object) "time", (object) this.m_ShowHideAnimationTime));
      if (!string.IsNullOrEmpty(this.m_PreviewAppearSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_PreviewAppearSound));
      Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    }
    else
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) (Vector3.one * 0.05f), (object) "time", (object) this.m_ShowHideAnimationTime, (object) "oncomplete", (object) new Action<object>(new AdventureRewardsPreview.\u003CShow\u003Ec__AnonStorey358()
      {
        \u003C\u003Ef__this = this,
        origScale = this.transform.localScale
      }.\u003C\u003Em__2F)));
      if (!string.IsNullOrEmpty(this.m_PreviewShrinkSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_PreviewShrinkSound));
      FullScreenFXMgr.Get().EndStandardBlurVignette(this.m_ShowHideAnimationTime, (FullScreenFXMgr.EffectListener) null);
    }
  }

  private void AddCardBatch(List<string> cardIds, List<Actor> cardBatch)
  {
    if (cardIds == null || cardIds.Count == 0)
      return;
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureRewardsPreview.\u003CAddCardBatch\u003Ec__AnonStorey359 batchCAnonStorey359 = new AdventureRewardsPreview.\u003CAddCardBatch\u003Ec__AnonStorey359();
    // ISSUE: reference to a compiler-generated field
    batchCAnonStorey359.\u003C\u003Ef__this = this;
    using (List<string>.Enumerator enumerator = cardIds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        batchCAnonStorey359.cardId = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        AdventureRewardsPreview.\u003CAddCardBatch\u003Ec__AnonStorey35A batchCAnonStorey35A = new AdventureRewardsPreview.\u003CAddCardBatch\u003Ec__AnonStorey35A();
        // ISSUE: reference to a compiler-generated field
        batchCAnonStorey35A.\u003C\u003Ef__ref\u0024857 = batchCAnonStorey359;
        // ISSUE: reference to a compiler-generated field
        batchCAnonStorey35A.\u003C\u003Ef__this = this;
        // ISSUE: reference to a compiler-generated field
        FullDef fullDef = DefLoader.Get().GetFullDef(batchCAnonStorey359.cardId, (CardPortraitQuality) null);
        GameObject gameObject = AssetLoader.Get().LoadActor(ActorNames.GetHandActor(fullDef.GetEntityDef(), TAG_PREMIUM.NORMAL), false, false);
        // ISSUE: reference to a compiler-generated field
        batchCAnonStorey35A.actor = gameObject.GetComponent<Actor>();
        // ISSUE: reference to a compiler-generated field
        batchCAnonStorey35A.actor.SetCardDef(fullDef.GetCardDef());
        // ISSUE: reference to a compiler-generated field
        batchCAnonStorey35A.actor.SetEntityDef(fullDef.GetEntityDef());
        // ISSUE: reference to a compiler-generated field
        GameUtils.SetParent((Component) batchCAnonStorey35A.actor, this.m_CardsContainer, false);
        // ISSUE: reference to a compiler-generated field
        SceneUtils.SetLayer((Component) batchCAnonStorey35A.actor, this.m_CardsContainer.gameObject.layer);
        // ISSUE: reference to a compiler-generated field
        cardBatch.Add(batchCAnonStorey35A.actor);
        if (this.m_PreviewCardsExpandable && (UnityEngine.Object) this.m_CardsPreviewDisplay != (UnityEngine.Object) null)
        {
          // ISSUE: reference to a compiler-generated field
          PegUIElement pegUiElement = batchCAnonStorey35A.actor.m_cardMesh.gameObject.AddComponent<PegUIElement>();
          pegUiElement.GetComponent<Collider>().enabled = true;
          // ISSUE: reference to a compiler-generated method
          pegUiElement.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(batchCAnonStorey35A.\u003C\u003Em__30));
        }
      }
    }
  }

  private void UpdateCardPositions()
  {
    int count = this.m_CardBatches.Count;
    bool flag1 = this.m_HiddenCardCount > 0;
    bool flag2 = (UnityEngine.Object) this.m_HiddenCardsLabelObject != (UnityEngine.Object) null;
    if (flag1 && flag2)
      ++count;
    float num1 = (float) (((double) (count - 1) * (double) this.m_CardSpacing + (double) count * (double) this.m_CardWidth) * 0.5 - (double) this.m_CardWidth * 0.5);
    int num2 = 0;
    using (List<List<Actor>>.Enumerator enumerator1 = this.m_CardBatches.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        List<Actor> current1 = enumerator1.Current;
        if (current1.Count != 0)
        {
          int num3 = 0;
          using (List<Actor>.Enumerator enumerator2 = current1.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Actor current2 = enumerator2.Current;
              if (!((UnityEngine.Object) current2 == (UnityEngine.Object) null))
              {
                Vector3 vector3 = this.m_CardClumpSpacing * (float) num3;
                vector3.x += (float) num2 * (this.m_CardSpacing + this.m_CardWidth) - num1;
                current2.transform.localScale = Vector3.one * 5f;
                current2.transform.localRotation = Quaternion.identity;
                current2.transform.Rotate(new Vector3(0.0f, 1f, 0.0f), (float) num3 * this.m_CardClumpAngleIncrement);
                current2.transform.localPosition = vector3;
                current2.SetUnlit();
                current2.ContactShadow(true);
                current2.UpdateAllComponents();
                current2.Show();
                ++num3;
              }
            }
          }
          ++num2;
        }
      }
    }
    if (flag1 && flag2)
    {
      Vector3 zero = Vector3.zero;
      zero.x += (float) num2 * (this.m_CardSpacing + this.m_CardWidth) - num1;
      this.m_HiddenCardsLabelObject.transform.localPosition = zero;
      this.m_HiddenCardsLabel.Text = string.Format("+{0}", (object) this.m_HiddenCardCount);
    }
    if (!flag2)
      return;
    this.m_HiddenCardsLabelObject.SetActive(flag1);
  }

  private void FireHideEvent()
  {
    foreach (AdventureRewardsPreview.OnHide onHide in this.m_OnHideListeners.ToArray())
      onHide();
  }

  public delegate void OnHide();
}
