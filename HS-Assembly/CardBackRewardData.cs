﻿// Decompiled with JetBrains decompiler
// Type: CardBackRewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class CardBackRewardData : RewardData
{
  public int CardBackID { get; set; }

  public CardBackRewardData()
    : this(0)
  {
  }

  public CardBackRewardData(int cardBackID)
    : base(Reward.Type.CARD_BACK)
  {
    this.CardBackID = cardBackID;
  }

  public override string ToString()
  {
    return string.Format("[CardBackRewardData: CardBackID={0} Origin={1} OriginData={2}]", (object) this.CardBackID, (object) this.Origin, (object) this.OriginData);
  }

  protected override string GetGameObjectName()
  {
    return "CardBackReward";
  }
}
