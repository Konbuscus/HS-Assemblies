﻿// Decompiled with JetBrains decompiler
// Type: RankedRewardChest2D
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class RankedRewardChest2D : PegUIElement
{
  public List<GameObject> m_rewardChests;
  public GameObject m_emptyRewardChest;
  public UberText m_rewardChestDescriptionText;
  public UberText m_rewardChestRankText;
  public GameObject m_legendaryGem;
  private int m_rank;

  protected override void Awake()
  {
    base.Awake();
    using (List<GameObject>.Enumerator enumerator = this.m_rewardChests.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetActive(false);
    }
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.ChestOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.ChestOut));
  }

  public void ChestOver(UIEvent e)
  {
    string headline;
    string bodytext;
    if (RankedRewardChest.GetChestIndexFromRank(this.m_rank) >= 0)
    {
      headline = RankedRewardChest.GetChestNameFromRank(this.m_rank);
      bodytext = GameStrings.Format("GLUE_QUEST_LOG_CHEST_TOOLTIP_BODY");
    }
    else
    {
      headline = GameStrings.Format("GLUE_QUEST_LOG_NO_CHEST");
      bodytext = GameStrings.Format("GLUE_QUEST_LOG_CHEST_TOOLTIP_BODY_NO_CHEST");
    }
    this.gameObject.GetComponent<TooltipZone>().ShowLayerTooltip(headline, bodytext);
  }

  private void ChestOut(UIEvent e)
  {
    this.gameObject.GetComponent<TooltipZone>().HideTooltip();
  }

  public void SetRank(int rank)
  {
    this.m_rank = rank;
    int chestIndexFromRank = RankedRewardChest.GetChestIndexFromRank(rank);
    if (chestIndexFromRank >= 0)
    {
      this.m_rewardChests[chestIndexFromRank].SetActive(true);
      this.m_emptyRewardChest.SetActive(false);
      this.m_rewardChestRankText.Text = rank.ToString();
    }
    else
    {
      this.m_emptyRewardChest.SetActive(true);
      this.m_rewardChestRankText.TextAlpha = 0.2f;
      this.m_rewardChestRankText.Text = 20.ToString();
    }
    bool flag = this.m_rank == 0;
    this.m_legendaryGem.SetActive(flag);
    this.m_rewardChestRankText.gameObject.SetActive(!flag);
  }
}
