﻿// Decompiled with JetBrains decompiler
// Type: Box
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Box : MonoBehaviour
{
  private Box.State m_state = Box.State.STARTUP;
  private Queue<Box.State> m_stateQueue = new Queue<Box.State>();
  private List<Box.TransitionFinishedListener> m_transitionFinishedListeners = new List<Box.TransitionFinishedListener>();
  private List<Box.ButtonPressListener> m_buttonPressListeners = new List<Box.ButtonPressListener>();
  private const string SHOW_LOG_COROUTINE = "ShowQuestLogWhenReady";
  public GameObject m_rootObject;
  public BoxStateInfoList m_StateInfoList;
  public BoxLogo m_Logo;
  public BoxStartButton m_StartButton;
  public BoxDoor m_LeftDoor;
  public BoxDoor m_RightDoor;
  public BoxDisk m_Disk;
  public GameObject m_DiskCenter;
  public BoxSpinner m_TopSpinner;
  public BoxSpinner m_BottomSpinner;
  public BoxDrawer m_Drawer;
  public BoxCamera m_Camera;
  public Camera m_NoFxCamera;
  public AudioListener m_AudioListener;
  public BoxLightMgr m_LightMgr;
  public BoxEventMgr m_EventMgr;
  public BoxMenuButton m_TournamentButton;
  public BoxMenuButton m_SoloAdventuresButton;
  public BoxMenuButton m_ForgeButton;
  public TavernBrawlMenuButton m_TavernBrawlButton;
  public GameObject m_EmptyFourthButton;
  public GameObject m_TavernBrawlButtonVisual;
  public GameObject m_TavernBrawlButtonActivateFX;
  public GameObject m_TavernBrawlButtonDeactivateFX;
  public string m_tavernBrawlActivateSound;
  public string m_tavernBrawlDeactivateSound;
  public string m_tavernBrawlPopupSound;
  public string m_tavernBrawlPopdownSound;
  public PackOpeningButton m_OpenPacksButton;
  public BoxMenuButton m_CollectionButton;
  public StoreButton m_StoreButton;
  public QuestLogButton m_QuestLogButton;
  public Color m_EnabledMaterial;
  public Color m_DisabledMaterial;
  public Color m_EnabledDrawerMaterial;
  public Color m_DisabledDrawerMaterial;
  public GameObject m_OuterFrame;
  public Texture2D m_textureCompressionTest;
  public RibbonButtonsUI m_ribbonButtons;
  public GameObject m_letterboxingContainer;
  public GameObject m_tableTop;
  private static Box s_instance;
  private Box.BoxStateConfig[] m_stateConfigs;
  private int m_pendingEffects;
  private bool m_transitioningToSceneMode;
  private Box.ButtonType? m_queuedButtonFire;
  private bool m_waitingForNetData;
  private GameLayer m_originalLeftDoorLayer;
  private GameLayer m_originalRightDoorLayer;
  private GameLayer m_originalDrawerLayer;
  private bool m_waitingForSceneLoad;
  private bool m_showRibbonButtons;
  private bool m_questLogLoading;
  private Box.DataState m_questLogNetCacheDataState;
  private QuestLog m_questLog;
  private GameObject m_tempInputBlocker;
  private GameObject m_setRotationDisk;
  private BoxMenuButton m_setRotationButton;

  public bool IsTavernBrawlButtonDeactivated { get; private set; }

  private void Awake()
  {
    Log.LoadingScreen.Print("Box.Awake()");
    Box.s_instance = this;
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
    this.InitializeStateConfigs();
    if ((UnityEngine.Object) LoadingScreen.Get() != (UnityEngine.Object) null)
      LoadingScreen.Get().NotifyMainSceneObjectAwoke(this.gameObject);
    this.m_originalLeftDoorLayer = (GameLayer) this.m_LeftDoor.gameObject.layer;
    this.m_originalRightDoorLayer = (GameLayer) this.m_RightDoor.gameObject.layer;
    this.m_originalDrawerLayer = (GameLayer) this.m_Drawer.gameObject.layer;
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    if ((double) TransformUtil.PhoneAspectRatioScale() < 0.990000009536743)
      GameUtils.InstantiateGameObject("Letterboxing", this.m_letterboxingContainer, false);
    GameObject child = AssetLoader.Get().LoadGameObject("RibbonButtons_Phone", true, false);
    this.m_ribbonButtons = child.GetComponent<RibbonButtonsUI>();
    this.m_ribbonButtons.Toggle(false);
    GameUtils.SetParent(child, this.m_rootObject, false);
    AssetLoader.Get().LoadTexture("TheBox_Top_phone", new AssetLoader.ObjectCallback(this.OnBoxTopPhoneTextureLoaded), (object) null, false);
  }

  private void Start()
  {
    this.InitializeNet();
    this.InitializeComponents();
    this.InitializeState();
    if (GameUtils.ShouldShowSetRotationIntro())
      this.m_DiskCenter.SetActive(false);
    else
      this.m_DiskCenter.SetActive(true);
    this.InitializeUI();
    if (DemoMgr.Get().IsExpoDemo())
    {
      this.m_StoreButton.gameObject.SetActive(false);
      this.m_Drawer.gameObject.SetActive(false);
      this.m_QuestLogButton.gameObject.SetActive(false);
    }
    TavernBrawlManager tavernBrawlManager = TavernBrawlManager.Get();
    tavernBrawlManager.OnTavernBrawlUpdated += new Action(this.TavernBrawl_UpdateUI);
    if (tavernBrawlManager.IsTavernBrawlInfoReady)
      this.DoTavernBrawlButtonInitialization();
    else
      tavernBrawlManager.OnTavernBrawlUpdated += new Action(this.DoTavernBrawlButtonInitialization);
  }

  private void OnDestroy()
  {
    Log.LoadingScreen.Print("Box.OnDestroy()");
    TavernBrawlManager.Get().OnTavernBrawlUpdated -= new Action(this.DoTavernBrawlButtonInitialization);
    TavernBrawlManager.Get().OnTavernBrawlUpdated -= new Action(this.TavernBrawl_UpdateUI);
    NetCache.Get().RemoveUpdatedListener(typeof (NetCache.NetCacheFeatures), new Action(this.DoTavernBrawlButtonInitialization));
    this.ShutdownState();
    this.ShutdownNet();
    Box.s_instance = (Box) null;
  }

  public static Box Get()
  {
    return Box.s_instance;
  }

  public Camera GetCamera()
  {
    return this.m_Camera.GetComponent<Camera>();
  }

  public BoxCamera GetBoxCamera()
  {
    return this.m_Camera;
  }

  public Camera GetNoFxCamera()
  {
    return this.m_NoFxCamera;
  }

  public AudioListener GetAudioListener()
  {
    return this.m_AudioListener;
  }

  public Box.State GetState()
  {
    return this.m_state;
  }

  public Texture2D GetTextureCompressionTestTexture()
  {
    return this.m_textureCompressionTest;
  }

  public bool ChangeState(Box.State state)
  {
    if (state == Box.State.INVALID || this.m_state == state)
      return false;
    if (this.HasPendingEffects())
      this.QueueStateChange(state);
    else
      this.ChangeStateNow(state);
    return true;
  }

  public void UpdateState()
  {
    if (this.m_state == Box.State.STARTUP)
      this.UpdateState_Startup();
    else if (this.m_state == Box.State.PRESS_START)
      this.UpdateState_PressStart();
    else if (this.m_state == Box.State.LOADING_HUB)
      this.UpdateState_LoadingHub();
    else if (this.m_state == Box.State.LOADING)
      this.UpdateState_Loading();
    else if (this.m_state == Box.State.HUB)
      this.UpdateState_Hub();
    else if (this.m_state == Box.State.HUB_WITH_DRAWER)
      this.UpdateState_HubWithDrawer();
    else if (this.m_state == Box.State.OPEN)
      this.UpdateState_Open();
    else if (this.m_state == Box.State.CLOSED)
      this.UpdateState_Closed();
    else if (this.m_state == Box.State.ERROR)
      this.UpdateState_Error();
    else if (this.m_state == Box.State.SET_ROTATION_LOADING)
      this.UpdateState_SetRotation();
    else if (this.m_state == Box.State.SET_ROTATION)
      this.UpdateState_SetRotation();
    else if (this.m_state == Box.State.SET_ROTATION_OPEN)
      this.UpdateState_SetRotationOpen();
    else
      UnityEngine.Debug.LogError((object) string.Format("Box.UpdateState() - unhandled state {0}", (object) this.m_state));
  }

  public BoxLightMgr GetLightMgr()
  {
    return this.m_LightMgr;
  }

  public BoxLightStateType GetLightState()
  {
    return this.m_LightMgr.GetActiveState();
  }

  public void ChangeLightState(BoxLightStateType stateType)
  {
    this.m_LightMgr.ChangeState(stateType);
  }

  public void SetLightState(BoxLightStateType stateType)
  {
    this.m_LightMgr.SetState(stateType);
  }

  public BoxEventMgr GetEventMgr()
  {
    return this.m_EventMgr;
  }

  public Spell GetEventSpell(BoxEventType eventType)
  {
    return this.m_EventMgr.GetEventSpell(eventType);
  }

  public bool HasPendingEffects()
  {
    return this.m_pendingEffects > 0;
  }

  public bool IsBusy()
  {
    if (!this.HasPendingEffects())
      return this.m_stateQueue.Count > 0;
    return true;
  }

  public bool IsTransitioningToSceneMode()
  {
    return this.m_transitioningToSceneMode;
  }

  public void OnAnimStarted()
  {
    ++this.m_pendingEffects;
  }

  public void OnAnimFinished()
  {
    --this.m_pendingEffects;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_OuterFrame.SetActive(false);
    if (this.HasPendingEffects())
      return;
    if (this.m_stateQueue.Count == 0)
    {
      this.UpdateUIEvents();
      if (!this.m_transitioningToSceneMode)
        return;
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        bool show = this.m_state == Box.State.HUB_WITH_DRAWER;
        if (show != this.m_showRibbonButtons)
          this.ToggleRibbonUI(show);
      }
      this.FireTransitionFinishedEvent();
      this.m_transitioningToSceneMode = false;
    }
    else
      this.ChangeStateQueued();
  }

  public void OnLoggedIn()
  {
    this.InitializeNet();
  }

  public void AddTransitionFinishedListener(Box.TransitionFinishedCallback callback)
  {
    this.AddTransitionFinishedListener(callback, (object) null);
  }

  public void AddTransitionFinishedListener(Box.TransitionFinishedCallback callback, object userData)
  {
    Box.TransitionFinishedListener finishedListener = new Box.TransitionFinishedListener();
    finishedListener.SetCallback(callback);
    finishedListener.SetUserData(userData);
    if (this.m_transitionFinishedListeners.Contains(finishedListener))
      return;
    this.m_transitionFinishedListeners.Add(finishedListener);
  }

  public bool RemoveTransitionFinishedListener(Box.TransitionFinishedCallback callback)
  {
    return this.RemoveTransitionFinishedListener(callback, (object) null);
  }

  public bool RemoveTransitionFinishedListener(Box.TransitionFinishedCallback callback, object userData)
  {
    Box.TransitionFinishedListener finishedListener = new Box.TransitionFinishedListener();
    finishedListener.SetCallback(callback);
    finishedListener.SetUserData(userData);
    return this.m_transitionFinishedListeners.Remove(finishedListener);
  }

  public void AddButtonPressListener(Box.ButtonPressCallback callback)
  {
    this.AddButtonPressListener(callback, (object) null);
  }

  public void AddButtonPressListener(Box.ButtonPressCallback callback, object userData)
  {
    Box.ButtonPressListener buttonPressListener = new Box.ButtonPressListener();
    buttonPressListener.SetCallback(callback);
    buttonPressListener.SetUserData(userData);
    if (this.m_buttonPressListeners.Contains(buttonPressListener))
      return;
    this.m_buttonPressListeners.Add(buttonPressListener);
  }

  public bool RemoveButtonPressListener(Box.ButtonPressCallback callback)
  {
    return this.RemoveButtonPressListener(callback, (object) null);
  }

  public bool RemoveButtonPressListener(Box.ButtonPressCallback callback, object userData)
  {
    Box.ButtonPressListener buttonPressListener = new Box.ButtonPressListener();
    buttonPressListener.SetCallback(callback);
    buttonPressListener.SetUserData(userData);
    return this.m_buttonPressListeners.Remove(buttonPressListener);
  }

  public void SetToIgnoreFullScreenEffects(bool ignoreEffects)
  {
    if (ignoreEffects)
    {
      SceneUtils.ReplaceLayer(this.m_LeftDoor.gameObject, GameLayer.IgnoreFullScreenEffects, this.m_originalLeftDoorLayer);
      SceneUtils.ReplaceLayer(this.m_RightDoor.gameObject, GameLayer.IgnoreFullScreenEffects, this.m_originalRightDoorLayer);
      SceneUtils.ReplaceLayer(this.m_Drawer.gameObject, GameLayer.IgnoreFullScreenEffects, this.m_originalDrawerLayer);
    }
    else
    {
      SceneUtils.ReplaceLayer(this.m_LeftDoor.gameObject, this.m_originalLeftDoorLayer, GameLayer.IgnoreFullScreenEffects);
      SceneUtils.ReplaceLayer(this.m_RightDoor.gameObject, this.m_originalRightDoorLayer, GameLayer.IgnoreFullScreenEffects);
      SceneUtils.ReplaceLayer(this.m_Drawer.gameObject, this.m_originalDrawerLayer, GameLayer.IgnoreFullScreenEffects);
    }
  }

  private void InitializeStateConfigs()
  {
    this.m_stateConfigs = new Box.BoxStateConfig[Enum.GetValues(typeof (Box.State)).Length];
    this.m_stateConfigs[1] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.LOADING
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED
      },
      m_camState = {
        m_state = BoxCamera.State.CLOSED
      },
      m_fullScreenBlackState = {
        m_state = true
      }
    };
    this.m_stateConfigs[2] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.SHOWN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.SHOWN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.LOADING
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED
      },
      m_camState = {
        m_state = BoxCamera.State.CLOSED
      },
      m_fullScreenBlackState = {
        m_ignore = true
      }
    };
    this.m_stateConfigs[4] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.LOADING
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED
      },
      m_camState = {
        m_state = BoxCamera.State.CLOSED
      },
      m_fullScreenBlackState = {
        m_ignore = true
      }
    };
    this.m_stateConfigs[3] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.LOADING
      },
      m_drawerState = {
        m_ignore = true
      },
      m_camState = {
        m_ignore = true
      },
      m_fullScreenBlackState = {
        m_ignore = true
      }
    };
    this.m_stateConfigs[5] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.MAINMENU
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED
      },
      m_camState = {
        m_state = BoxCamera.State.CLOSED
      },
      m_fullScreenBlackState = {
        m_ignore = true
      }
    };
    this.m_stateConfigs[6] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.MAINMENU
      },
      m_drawerState = {
        m_state = BoxDrawer.State.OPENED
      },
      m_camState = {
        m_state = BoxCamera.State.CLOSED_WITH_DRAWER
      },
      m_fullScreenBlackState = {
        m_ignore = true
      }
    };
    this.m_stateConfigs[7] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.OPENED
      },
      m_diskState = {
        m_state = BoxDisk.State.LOADING
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED_BOX_OPENED
      },
      m_camState = {
        m_state = BoxCamera.State.OPENED
      },
      m_fullScreenBlackState = {
        m_ignore = true
      }
    };
    this.m_stateConfigs[8] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.LOADING
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED
      },
      m_camState = {
        m_state = BoxCamera.State.CLOSED
      },
      m_fullScreenBlackState = {
        m_state = false
      }
    };
    this.m_stateConfigs[9] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.LOADING
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED
      },
      m_camState = {
        m_state = BoxCamera.State.CLOSED
      },
      m_fullScreenBlackState = {
        m_state = false
      }
    };
    this.m_stateConfigs[10] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.LOADING
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED
      },
      m_camState = {
        m_state = BoxCamera.State.CLOSED
      },
      m_fullScreenBlackState = {
        m_ignore = true
      }
    };
    this.m_stateConfigs[11] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.CLOSED
      },
      m_diskState = {
        m_state = BoxDisk.State.MAINMENU
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED
      },
      m_camState = {
        m_state = BoxCamera.State.CLOSED
      },
      m_fullScreenBlackState = {
        m_ignore = true
      }
    };
    this.m_stateConfigs[12] = new Box.BoxStateConfig()
    {
      m_logoState = {
        m_state = BoxLogo.State.HIDDEN
      },
      m_startButtonState = {
        m_state = BoxStartButton.State.HIDDEN
      },
      m_doorState = {
        m_state = BoxDoor.State.OPENED
      },
      m_diskState = {
        m_state = BoxDisk.State.LOADING
      },
      m_drawerState = {
        m_state = BoxDrawer.State.CLOSED
      },
      m_camState = {
        m_state = BoxCamera.State.OPENED
      },
      m_fullScreenBlackState = {
        m_ignore = false
      }
    };
  }

  private void InitializeState()
  {
    this.m_state = Box.State.STARTUP;
    bool flag = GameMgr.Get().WasTutorial() && !GameMgr.Get().WasSpectator();
    SceneMgr sceneMgr = SceneMgr.Get();
    if ((UnityEngine.Object) sceneMgr != (UnityEngine.Object) null)
    {
      if (flag)
      {
        this.m_state = Box.State.LOADING;
      }
      else
      {
        sceneMgr.RegisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
        sceneMgr.RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
        this.m_state = this.TranslateSceneModeToBoxState(sceneMgr.GetMode());
      }
    }
    this.UpdateState();
    this.m_TopSpinner.Spin();
    this.m_BottomSpinner.Spin();
    if (flag)
      LoadingScreen.Get().RegisterPreviousSceneDestroyedListener(new LoadingScreen.PreviousSceneDestroyedCallback(this.OnTutorialSceneDestroyed));
    if (this.m_state != Box.State.HUB_WITH_DRAWER)
      return;
    this.ToggleRibbonUI(true);
  }

  private void OnTutorialSceneDestroyed(object userData)
  {
    LoadingScreen.Get().UnregisterPreviousSceneDestroyedListener(new LoadingScreen.PreviousSceneDestroyedCallback(this.OnTutorialSceneDestroyed));
    Spell eventSpell = this.GetEventSpell(BoxEventType.TUTORIAL_PLAY);
    eventSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnTutorialPlaySpellStateFinished));
    eventSpell.ActivateState(SpellStateType.DEATH);
  }

  private void OnTutorialPlaySpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    SceneMgr sceneMgr = SceneMgr.Get();
    sceneMgr.RegisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
    sceneMgr.RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    this.ChangeStateToReflectSceneMode(SceneMgr.Get().GetMode(), false);
  }

  private void ShutdownState()
  {
    if ((UnityEngine.Object) this.m_StoreButton != (UnityEngine.Object) null)
      this.m_StoreButton.Unload();
    this.UnloadQuestLog();
    SceneMgr sceneMgr = SceneMgr.Get();
    if (!((UnityEngine.Object) sceneMgr != (UnityEngine.Object) null))
      return;
    sceneMgr.UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    sceneMgr.UnregisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
  }

  private void QueueStateChange(Box.State state)
  {
    this.m_stateQueue.Enqueue(state);
  }

  private void ChangeStateQueued()
  {
    this.ChangeStateNow(this.m_stateQueue.Dequeue());
  }

  private void ChangeStateNow(Box.State state)
  {
    bool flag = GameUtils.ShouldShowSetRotationIntro();
    if (!flag)
    {
      this.m_DiskCenter.SetActive(true);
      if ((UnityEngine.Object) this.m_setRotationDisk != (UnityEngine.Object) null)
        this.m_setRotationDisk.SetActive(false);
    }
    if (state == Box.State.OPEN && flag)
      state = Box.State.SET_ROTATION_OPEN;
    this.m_state = state;
    if (state == Box.State.STARTUP)
      this.ChangeState_Startup();
    else if (state == Box.State.PRESS_START)
      this.ChangeState_PressStart();
    else if (state == Box.State.LOADING_HUB)
      this.ChangeState_LoadingHub();
    else if (state == Box.State.LOADING)
      this.ChangeState_Loading();
    else if (state == Box.State.HUB)
      this.ChangeState_Hub();
    else if (state == Box.State.HUB_WITH_DRAWER)
      this.ChangeState_HubWithDrawer();
    else if (state == Box.State.OPEN)
      this.ChangeState_Open();
    else if (state == Box.State.CLOSED)
      this.ChangeState_Closed();
    else if (state == Box.State.ERROR)
      this.ChangeState_Error();
    else if (state == Box.State.SET_ROTATION_LOADING)
      this.ChangeState_SetRotationLoading();
    else if (state == Box.State.SET_ROTATION)
      this.ChangeState_SetRotation();
    else if (state == Box.State.SET_ROTATION_OPEN)
      this.ChangeState_SetRotationOpen();
    else
      UnityEngine.Debug.LogError((object) string.Format("Box.ChangeStateNow() - unhandled state {0}", (object) state));
    this.UpdateUIEvents();
  }

  private void ChangeStateToReflectSceneMode(SceneMgr.Mode mode, bool isSceneActuallyLoaded)
  {
    Box.State boxState = this.TranslateSceneModeToBoxState(mode);
    bool flag = GameUtils.ShouldShowSetRotationIntro();
    if (mode == SceneMgr.Mode.HUB && flag)
    {
      this.ChangeState(Box.State.SET_ROTATION_LOADING);
      if (isSceneActuallyLoaded)
        this.StartCoroutine(this.SetRotation_StartSetRotationIntro());
    }
    else if (mode == SceneMgr.Mode.TOURNAMENT && flag)
    {
      this.ChangeState(Box.State.SET_ROTATION_OPEN);
      this.m_transitioningToSceneMode = true;
    }
    else if (this.ChangeState(boxState))
      this.m_transitioningToSceneMode = true;
    this.m_LightMgr.ChangeState(this.TranslateSceneModeToLightState(mode));
  }

  private Box.State TranslateSceneModeToBoxState(SceneMgr.Mode mode)
  {
    if (mode == SceneMgr.Mode.STARTUP)
      return Box.State.STARTUP;
    if (mode == SceneMgr.Mode.LOGIN)
      return Box.State.INVALID;
    if (mode == SceneMgr.Mode.HUB)
      return Box.State.HUB_WITH_DRAWER;
    if (mode == SceneMgr.Mode.TOURNAMENT || mode == SceneMgr.Mode.ADVENTURE || (mode == SceneMgr.Mode.FRIENDLY || mode == SceneMgr.Mode.DRAFT) || (mode == SceneMgr.Mode.COLLECTIONMANAGER || mode == SceneMgr.Mode.TAVERN_BRAWL || mode == SceneMgr.Mode.PACKOPENING))
      return Box.State.OPEN;
    if (mode == SceneMgr.Mode.GAMEPLAY)
      return Box.State.INVALID;
    return mode == SceneMgr.Mode.FATAL_ERROR ? Box.State.ERROR : Box.State.OPEN;
  }

  private BoxLightStateType TranslateSceneModeToLightState(SceneMgr.Mode mode)
  {
    if (mode == SceneMgr.Mode.LOGIN)
      return BoxLightStateType.INVALID;
    if (mode == SceneMgr.Mode.TOURNAMENT)
      return BoxLightStateType.TOURNAMENT;
    if (mode == SceneMgr.Mode.COLLECTIONMANAGER || mode == SceneMgr.Mode.TAVERN_BRAWL)
      return BoxLightStateType.COLLECTION;
    if (mode == SceneMgr.Mode.PACKOPENING)
      return BoxLightStateType.PACK_OPENING;
    if (mode == SceneMgr.Mode.GAMEPLAY)
      return BoxLightStateType.INVALID;
    if (mode == SceneMgr.Mode.DRAFT)
      return BoxLightStateType.FORGE;
    return mode == SceneMgr.Mode.ADVENTURE || mode == SceneMgr.Mode.FRIENDLY ? BoxLightStateType.ADVENTURE : BoxLightStateType.DEFAULT;
  }

  private void OnScenePreUnload(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    switch (mode)
    {
      case SceneMgr.Mode.GAMEPLAY:
        break;
      case SceneMgr.Mode.STARTUP:
        break;
      case SceneMgr.Mode.RESET:
        break;
      default:
        if (prevMode == SceneMgr.Mode.HUB)
        {
          this.ChangeState(Box.State.LOADING);
          this.m_StoreButton.Unload();
          this.UnloadQuestLog();
        }
        else if (mode == SceneMgr.Mode.HUB)
        {
          this.ChangeStateToReflectSceneMode(mode, false);
          this.m_waitingForSceneLoad = true;
        }
        else if (this.ShouldUseLoadingHubState(mode, prevMode))
          this.ChangeState(Box.State.LOADING_HUB);
        else
          this.ChangeState(Box.State.LOADING);
        this.ClearQueuedButtonFireEvent();
        this.UpdateUIEvents();
        break;
    }
  }

  private bool ShouldUseLoadingHubState(SceneMgr.Mode mode, SceneMgr.Mode prevMode)
  {
    return mode == SceneMgr.Mode.FRIENDLY && prevMode != SceneMgr.Mode.HUB || prevMode == SceneMgr.Mode.COLLECTIONMANAGER && (mode == SceneMgr.Mode.ADVENTURE || mode == SceneMgr.Mode.TOURNAMENT) || mode == SceneMgr.Mode.COLLECTIONMANAGER && (prevMode == SceneMgr.Mode.ADVENTURE || prevMode == SceneMgr.Mode.TOURNAMENT);
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (!TavernBrawlManager.Get().IsTavernBrawlActive && SceneMgr.Get().GetPrevMode() != SceneMgr.Mode.STARTUP)
      this.PlayTavernBrawlButtonActivation(false, true);
    this.ChangeStateToReflectSceneMode(mode, true);
    if (!this.m_waitingForSceneLoad)
      return;
    this.m_waitingForSceneLoad = false;
    if (!this.m_queuedButtonFire.HasValue)
      return;
    this.FireButtonPressEvent(this.m_queuedButtonFire.Value);
    this.m_queuedButtonFire = new Box.ButtonType?();
  }

  private void ChangeState_Startup()
  {
    this.m_state = Box.State.STARTUP;
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_PressStart()
  {
    this.m_state = Box.State.PRESS_START;
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_SetRotationLoading()
  {
    this.m_state = Box.State.SET_ROTATION_LOADING;
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_SetRotation()
  {
    this.m_state = Box.State.SET_ROTATION;
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_SetRotationOpen()
  {
    this.m_state = Box.State.SET_ROTATION_OPEN;
    this.StartCoroutine(this.SetRotationOpen_ChangeState());
  }

  private void ChangeState_LoadingHub()
  {
    this.m_state = Box.State.LOADING_HUB;
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_Loading()
  {
    this.m_state = Box.State.LOADING;
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_Hub()
  {
    this.m_state = Box.State.HUB;
    this.HackRequestNetFeatures();
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_HubWithDrawer()
  {
    this.m_state = Box.State.HUB_WITH_DRAWER;
    this.m_Camera.EnableAccelerometer();
    this.HackRequestNetData();
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_Open()
  {
    this.m_state = Box.State.OPEN;
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_Closed()
  {
    this.m_state = Box.State.CLOSED;
    this.ChangeStateUsingConfig();
  }

  private void ChangeState_Error()
  {
    this.m_state = Box.State.ERROR;
    this.ChangeStateUsingConfig();
  }

  private void UpdateState_Startup()
  {
    this.m_state = Box.State.STARTUP;
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_PressStart()
  {
    this.m_state = Box.State.PRESS_START;
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_SetRotationLoading()
  {
    this.m_state = Box.State.SET_ROTATION_LOADING;
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_SetRotation()
  {
    this.m_state = Box.State.SET_ROTATION;
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_SetRotationOpen()
  {
    this.m_state = Box.State.SET_ROTATION_OPEN;
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_LoadingHub()
  {
    this.m_state = Box.State.LOADING_HUB;
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_Loading()
  {
    this.m_state = Box.State.LOADING;
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_Hub()
  {
    this.m_state = Box.State.HUB;
    this.HackRequestNetFeatures();
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_HubWithDrawer()
  {
    this.m_state = Box.State.HUB_WITH_DRAWER;
    this.m_Camera.EnableAccelerometer();
    this.HackRequestNetData();
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_Open()
  {
    this.m_state = Box.State.OPEN;
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_Closed()
  {
    this.m_state = Box.State.CLOSED;
    this.UpdateStateUsingConfig();
  }

  private void UpdateState_Error()
  {
    this.m_state = Box.State.ERROR;
    this.UpdateStateUsingConfig();
  }

  private void ChangeStateUsingConfig()
  {
    Box.BoxStateConfig stateConfig = this.m_stateConfigs[(int) this.m_state];
    if (!stateConfig.m_logoState.m_ignore)
      this.m_Logo.ChangeState(stateConfig.m_logoState.m_state);
    if (!stateConfig.m_startButtonState.m_ignore)
      this.m_StartButton.ChangeState(stateConfig.m_startButtonState.m_state);
    if (!stateConfig.m_doorState.m_ignore)
    {
      this.m_LeftDoor.ChangeState(stateConfig.m_doorState.m_state);
      this.m_RightDoor.ChangeState(stateConfig.m_doorState.m_state);
    }
    if (!stateConfig.m_diskState.m_ignore)
      this.m_Disk.ChangeState(stateConfig.m_diskState.m_state);
    if (!stateConfig.m_drawerState.m_ignore)
    {
      if (!(bool) UniversalInputManager.UsePhoneUI)
      {
        this.m_Drawer.ChangeState(stateConfig.m_drawerState.m_state);
      }
      else
      {
        bool show = this.m_state == Box.State.HUB_WITH_DRAWER;
        if (!show && show != this.m_showRibbonButtons)
          this.ToggleRibbonUI(show);
      }
    }
    if (!stateConfig.m_camState.m_ignore)
      this.m_Camera.ChangeState(stateConfig.m_camState.m_state);
    if (stateConfig.m_fullScreenBlackState.m_ignore)
      return;
    this.FullScreenBlack_ChangeState(stateConfig.m_fullScreenBlackState.m_state);
  }

  private void ToggleRibbonUI(bool show)
  {
    if ((UnityEngine.Object) this.m_ribbonButtons == (UnityEngine.Object) null)
      return;
    this.m_ribbonButtons.Toggle(show);
    this.m_showRibbonButtons = show;
  }

  private void UpdateStateUsingConfig()
  {
    Box.BoxStateConfig stateConfig = this.m_stateConfigs[(int) this.m_state];
    if (!stateConfig.m_logoState.m_ignore)
      this.m_Logo.UpdateState(stateConfig.m_logoState.m_state);
    if (!stateConfig.m_startButtonState.m_ignore)
      this.m_StartButton.UpdateState(stateConfig.m_startButtonState.m_state);
    if (!stateConfig.m_doorState.m_ignore)
    {
      this.m_LeftDoor.ChangeState(stateConfig.m_doorState.m_state);
      this.m_RightDoor.ChangeState(stateConfig.m_doorState.m_state);
    }
    if (!stateConfig.m_diskState.m_ignore)
      this.m_Disk.UpdateState(stateConfig.m_diskState.m_state);
    this.m_TopSpinner.Reset();
    this.m_BottomSpinner.Reset();
    if (!stateConfig.m_drawerState.m_ignore)
      this.m_Drawer.UpdateState(stateConfig.m_drawerState.m_state);
    if (!stateConfig.m_camState.m_ignore)
      this.m_Camera.UpdateState(stateConfig.m_camState.m_state);
    if (stateConfig.m_fullScreenBlackState.m_ignore)
      return;
    this.FullScreenBlack_UpdateState(stateConfig.m_fullScreenBlackState.m_state);
  }

  private void FullScreenBlack_ChangeState(bool enable)
  {
    this.FullScreenBlack_UpdateState(enable);
  }

  private void FullScreenBlack_UpdateState(bool enable)
  {
    FullScreenEffects component = this.m_Camera.GetComponent<FullScreenEffects>();
    component.BlendToColorEnable = enable;
    if (!enable)
      return;
    component.BlendToColor = Color.black;
    component.BlendToColorAmount = 1f;
  }

  private void FireTransitionFinishedEvent()
  {
    foreach (Box.TransitionFinishedListener finishedListener in this.m_transitionFinishedListeners.ToArray())
      finishedListener.Fire();
  }

  private void InitializeUI()
  {
    PegUI.Get().SetInputCamera(this.m_Camera.GetComponent<Camera>());
    this.m_StartButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnStartButtonPressed));
    switch (InputUtil.GetInputScheme())
    {
      case InputScheme.TOUCH:
        this.m_StartButton.SetText(GameStrings.Get("GLUE_START_TOUCH"));
        break;
      case InputScheme.GAMEPAD:
        this.m_StartButton.SetText(GameStrings.Get("GLUE_START_PRESS"));
        break;
      case InputScheme.KEYBOARD_MOUSE:
        this.m_StartButton.SetText(GameStrings.Get("GLUE_START_CLICK"));
        break;
    }
    this.m_TournamentButton.SetText(GameStrings.Get("GLUE_TOURNAMENT"));
    this.m_SoloAdventuresButton.SetText(GameStrings.Get("GLUE_ADVENTURE"));
    this.m_ForgeButton.SetText(GameStrings.Get("GLUE_FORGE"));
    this.m_TavernBrawlButton.SetText(GameStrings.Get("GLOBAL_TAVERN_BRAWL"));
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_Drawer.gameObject.SetActive(false);
      this.m_ribbonButtons.m_collectionManagerRibbon.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCollectionButtonPressed));
      this.m_ribbonButtons.m_packOpeningRibbon.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnOpenPacksButtonPressed));
      this.m_ribbonButtons.m_questLogRibbon.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnQuestButtonPressed));
      this.m_ribbonButtons.m_storeRibbon.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnStoreButtonReleased));
    }
    else
    {
      this.m_OpenPacksButton.SetText(GameStrings.Get("GLUE_OPEN_PACKS"));
      this.m_CollectionButton.SetText(GameStrings.Get("GLUE_MY_COLLECTION"));
      this.m_QuestLogButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnQuestButtonPressed));
      this.m_StoreButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnStoreButtonReleased));
    }
    this.RegisterButtonEvents((PegUIElement) this.m_TournamentButton);
    this.RegisterButtonEvents((PegUIElement) this.m_SoloAdventuresButton);
    this.RegisterButtonEvents((PegUIElement) this.m_ForgeButton);
    this.RegisterButtonEvents((PegUIElement) this.m_TavernBrawlButton);
    this.RegisterButtonEvents((PegUIElement) this.m_OpenPacksButton);
    this.RegisterButtonEvents((PegUIElement) this.m_CollectionButton);
    this.UpdateUI(true);
  }

  public void UpdateUI(bool isInitialization = false)
  {
    this.UpdateUIState(isInitialization);
    this.UpdateUIEvents();
  }

  private void TavernBrawl_UpdateUI()
  {
    this.UpdateUI(false);
  }

  private void UpdateUIState(bool isInitialization)
  {
    if (this.m_waitingForNetData)
    {
      this.SetPackCount(-1);
      this.HighlightButton((BoxMenuButton) this.m_OpenPacksButton, false);
      this.HighlightButton(this.m_TournamentButton, false);
      this.HighlightButton(this.m_SoloAdventuresButton, false);
      this.HighlightButton(this.m_CollectionButton, false);
      this.HighlightButton(this.m_ForgeButton, false);
      this.HighlightButton((BoxMenuButton) this.m_TavernBrawlButton, false);
    }
    else
    {
      NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
      if (DemoMgr.Get().GetMode() == DemoMode.BLIZZCON_2013)
      {
        netObject.Games.Practice = false;
        netObject.Games.Tournament = false;
      }
      int boosterCount = this.ComputeBoosterCount();
      this.SetPackCount(boosterCount);
      this.HighlightButton((BoxMenuButton) this.m_OpenPacksButton, boosterCount > 0 && !Options.Get().GetBool(Option.HAS_SEEN_PACK_OPENING, false));
      bool highlightOn1 = false;
      bool highlightOn2 = false;
      if (netObject.Games.Practice && Options.Get().GetBool(Option.BUNDLE_JUST_PURCHASE_IN_HUB, false))
        highlightOn1 = true;
      else if (netObject.Games.Tournament && Options.Get().GetBool(Option.SHOW_INNKEEPER_DECK_DIALOGUE))
        highlightOn2 = true;
      else if (netObject.Games.Practice && !Options.Get().GetBool(Option.HAS_SEEN_PRACTICE_MODE, false))
        highlightOn1 = true;
      else if (netObject.Games.Tournament && AchieveManager.Get() != null && AchieveManager.Get().HasActiveQuestId(AchievementDbId.FIRST_BLOOD))
        highlightOn2 = true;
      this.HighlightButton(this.m_TournamentButton, highlightOn2);
      this.ToggleButtonTextureState(netObject.Games.Tournament, this.m_TournamentButton);
      this.HighlightButton(this.m_SoloAdventuresButton, highlightOn1);
      this.ToggleButtonTextureState(netObject.Games.Practice, this.m_SoloAdventuresButton);
      this.HighlightButton(this.m_CollectionButton, !highlightOn1 && netObject.Collection.Manager && !Options.Get().GetBool(Option.HAS_SEEN_COLLECTIONMANAGER_AFTER_PRACTICE, false));
      this.ToggleDrawerButtonState(netObject.Collection.Manager, this.m_CollectionButton);
      bool enabled = netObject.Games.Forge && AchieveManager.Get() != null && AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.FORGE);
      if (enabled)
        enabled = HealthyGamingMgr.Get().isArenaEnabled();
      this.HighlightButton(this.m_ForgeButton, !highlightOn2 && !highlightOn1 && enabled && !Options.Get().GetBool(Option.HAS_SEEN_FORGE, false));
      this.ToggleButtonTextureState(enabled, this.m_ForgeButton);
      this.UpdateTavernBrawlButtonState(true);
      if (SpecialEventVisualMgr.GetActiveEventType() != SpecialEventType.SPECIAL_EVENT_PRE_TAVERN_BRAWL)
        return;
      this.m_TavernBrawlButton.gameObject.SetActive(false);
      this.m_EmptyFourthButton.SetActive(true);
    }
  }

  private void DoTavernBrawlButtonInitialization()
  {
    TavernBrawlManager tavernBrawlManager = TavernBrawlManager.Get();
    NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
    if (netObject == null)
    {
      NetCache.Get().RegisterUpdatedListener(typeof (NetCache.NetCacheFeatures), new Action(this.DoTavernBrawlButtonInitialization));
    }
    else
    {
      NetCache.Get().RemoveUpdatedListener(typeof (NetCache.NetCacheFeatures), new Action(this.DoTavernBrawlButtonInitialization));
      if (netObject == null || !netObject.Games.TavernBrawl || !tavernBrawlManager.HasUnlockedTavernBrawl || !tavernBrawlManager.IsTavernBrawlInfoReady)
        return;
      tavernBrawlManager.OnTavernBrawlUpdated -= new Action(this.DoTavernBrawlButtonInitialization);
      if (tavernBrawlManager.IsTavernBrawlActive && !tavernBrawlManager.IsFirstTimeSeeingCurrentSeason)
        return;
      this.PlayTavernBrawlButtonActivation(false, true);
    }
  }

  public void PlayTavernBrawlButtonActivation(bool activate, bool isInitialization = false)
  {
    Animator component = this.m_TavernBrawlButtonVisual.GetComponent<Animator>();
    component.StopPlayback();
    if (activate)
    {
      component.Play("TavernBrawl_ButtonActivate");
      if (!isInitialization)
        this.m_TavernBrawlButtonActivateFX.GetComponent<ParticleSystem>().Play();
    }
    else
    {
      if (!isInitialization)
        this.m_TavernBrawlButton.ClearHighlightAndTooltip();
      component.Play("TavernBrawl_ButtonDeactivate");
      if (!isInitialization)
        this.m_TavernBrawlButtonDeactivateFX.GetComponent<ParticleSystem>().Play();
    }
    this.IsTavernBrawlButtonDeactivated = !activate;
  }

  public bool UpdateTavernBrawlButtonState(bool highlightAllowed)
  {
    NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
    TavernBrawlManager tavernBrawlManager = TavernBrawlManager.Get();
    bool enabled = netObject != null && netObject.Games.TavernBrawl && tavernBrawlManager.HasUnlockedTavernBrawl;
    this.HighlightButton((BoxMenuButton) this.m_TavernBrawlButton, highlightAllowed && enabled && (tavernBrawlManager.IsFirstTimeSeeingCurrentSeason && !this.IsTavernBrawlButtonDeactivated) && (!this.IsButtonHighlighted(this.m_TournamentButton) && !this.IsButtonHighlighted(this.m_SoloAdventuresButton)) && !this.IsButtonHighlighted(this.m_ForgeButton));
    this.ToggleButtonTextureState(enabled, (BoxMenuButton) this.m_TavernBrawlButton);
    return enabled;
  }

  private void UpdateUIEvents()
  {
    bool flag = GameUtils.ShouldShowSetRotationIntro();
    if (this.CanEnableUIEvents() && this.m_state == Box.State.PRESS_START)
      this.EnableButton((PegUIElement) this.m_StartButton);
    else
      this.DisableButton((PegUIElement) this.m_StartButton);
    if (!flag && this.m_state != Box.State.PRESS_START && (this.m_state != Box.State.LOADING_HUB && this.CanEnableUIEvents()))
    {
      this.m_StoreButton.gameObject.SetActive(true);
      this.m_QuestLogButton.gameObject.SetActive(true);
    }
    if (this.CanEnableUIEvents() && (this.m_state == Box.State.HUB || this.m_state == Box.State.HUB_WITH_DRAWER))
    {
      if (this.m_waitingForNetData)
      {
        this.DisableButton((PegUIElement) this.m_TournamentButton);
        this.DisableButton((PegUIElement) this.m_SoloAdventuresButton);
        this.DisableButton((PegUIElement) this.m_ForgeButton);
        this.DisableButton((PegUIElement) this.m_TavernBrawlButton);
        this.DisableButton((PegUIElement) this.m_QuestLogButton);
        this.DisableButton((PegUIElement) this.m_StoreButton);
      }
      else
      {
        this.EnableButton((PegUIElement) this.m_TournamentButton);
        this.EnableButton((PegUIElement) this.m_SoloAdventuresButton);
        this.EnableButton((PegUIElement) this.m_ForgeButton);
        this.EnableButton((PegUIElement) this.m_TavernBrawlButton);
        this.EnableButton((PegUIElement) this.m_QuestLogButton);
        this.EnableButton((PegUIElement) this.m_StoreButton);
      }
      if (this.m_state == Box.State.HUB_WITH_DRAWER)
      {
        if (this.m_waitingForNetData)
        {
          this.DisableButton((PegUIElement) this.m_OpenPacksButton);
          this.DisableButton((PegUIElement) this.m_CollectionButton);
        }
        else
        {
          this.EnableButton((PegUIElement) this.m_OpenPacksButton);
          this.EnableButton((PegUIElement) this.m_CollectionButton);
        }
      }
      else
      {
        this.DisableButton((PegUIElement) this.m_OpenPacksButton);
        this.DisableButton((PegUIElement) this.m_CollectionButton);
      }
    }
    else
    {
      this.DisableButton((PegUIElement) this.m_TournamentButton);
      this.DisableButton((PegUIElement) this.m_SoloAdventuresButton);
      this.DisableButton((PegUIElement) this.m_ForgeButton);
      this.DisableButton((PegUIElement) this.m_TavernBrawlButton);
      this.DisableButton((PegUIElement) this.m_OpenPacksButton);
      this.DisableButton((PegUIElement) this.m_CollectionButton);
      this.DisableButton((PegUIElement) this.m_QuestLogButton);
      this.DisableButton((PegUIElement) this.m_StoreButton);
    }
    if (DemoMgr.Get().GetMode() == DemoMode.BLIZZCON_2013)
    {
      this.DisableButton((PegUIElement) this.m_TournamentButton);
      this.DisableButton((PegUIElement) this.m_SoloAdventuresButton);
      this.DisableButton((PegUIElement) this.m_OpenPacksButton);
      this.DisableButton((PegUIElement) this.m_CollectionButton);
      this.DisableButton((PegUIElement) this.m_QuestLogButton);
      this.DisableButton((PegUIElement) this.m_StoreButton);
    }
    else if (DemoMgr.Get().GetMode() == DemoMode.BLIZZCON_2014)
    {
      this.DisableButton((PegUIElement) this.m_SoloAdventuresButton);
      this.DisableButton((PegUIElement) this.m_ForgeButton);
      this.DisableButton((PegUIElement) this.m_TavernBrawlButton);
      this.DisableButton((PegUIElement) this.m_OpenPacksButton);
      this.DisableButton((PegUIElement) this.m_CollectionButton);
      this.DisableButton((PegUIElement) this.m_QuestLogButton);
      this.DisableButton((PegUIElement) this.m_StoreButton);
    }
    else if (DemoMgr.Get().GetMode() == DemoMode.BLIZZCON_2015)
    {
      this.DisableButton((PegUIElement) this.m_TournamentButton);
      this.DisableButton((PegUIElement) this.m_ForgeButton);
      this.DisableButton((PegUIElement) this.m_TavernBrawlButton);
      this.DisableButton((PegUIElement) this.m_OpenPacksButton);
      this.DisableButton((PegUIElement) this.m_CollectionButton);
      this.DisableButton((PegUIElement) this.m_QuestLogButton);
      this.DisableButton((PegUIElement) this.m_StoreButton);
    }
    else if (DemoMgr.Get().GetMode() == DemoMode.ANNOUNCEMENT_5_0 || DemoMgr.Get().GetMode() == DemoMode.BLIZZCON_2016)
    {
      this.DisableButton((PegUIElement) this.m_TournamentButton);
      this.DisableButton((PegUIElement) this.m_SoloAdventuresButton);
      this.DisableButton((PegUIElement) this.m_ForgeButton);
      this.DisableButton((PegUIElement) this.m_TavernBrawlButton);
      this.DisableButton((PegUIElement) this.m_OpenPacksButton);
      this.DisableButton((PegUIElement) this.m_CollectionButton);
      this.DisableButton((PegUIElement) this.m_QuestLogButton);
      this.DisableButton((PegUIElement) this.m_StoreButton);
    }
    if (!flag && this.m_state != Box.State.LOADING_HUB)
      return;
    this.m_StoreButton.gameObject.SetActive(false);
    this.m_QuestLogButton.gameObject.SetActive(false);
  }

  private bool CanEnableUIEvents()
  {
    return !this.HasPendingEffects() && this.m_stateQueue.Count <= 0 && (this.m_state != Box.State.INVALID && this.m_state != Box.State.STARTUP) && (this.m_state != Box.State.LOADING && this.m_state != Box.State.LOADING_HUB && this.m_state != Box.State.OPEN);
  }

  private void RegisterButtonEvents(PegUIElement button)
  {
    button.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnButtonPressed));
    button.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnButtonMouseOver));
    button.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnButtonMouseOut));
  }

  private void ToggleButtonTextureState(bool enabled, BoxMenuButton button)
  {
    if (enabled)
      button.m_TextMesh.TextColor = this.m_EnabledMaterial;
    else
      button.m_TextMesh.TextColor = this.m_DisabledMaterial;
  }

  private void ToggleDrawerButtonState(bool enabled, BoxMenuButton button)
  {
    if (enabled)
      button.m_TextMesh.TextColor = this.m_EnabledDrawerMaterial;
    else
      button.m_TextMesh.TextColor = this.m_DisabledDrawerMaterial;
  }

  private void HighlightButton(BoxMenuButton button, bool highlightOn)
  {
    if ((UnityEngine.Object) button.m_HighlightState == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("Box.HighlighButton {0} - highlight state is null", (object) button));
    }
    else
    {
      ActorStateType stateType = !highlightOn ? ActorStateType.HIGHLIGHT_OFF : ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE;
      button.m_HighlightState.ChangeState(stateType);
    }
  }

  private bool IsButtonHighlighted(BoxMenuButton button)
  {
    return button.m_HighlightState.CurrentState == ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE;
  }

  private void SetPackCount(int n)
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_ribbonButtons.SetPackCount(n);
    else
      this.m_OpenPacksButton.SetPackCount(n);
  }

  public void EnableButton(PegUIElement button)
  {
    button.SetEnabled(true);
  }

  public void DisableButton(PegUIElement button)
  {
    button.SetEnabled(false);
    TooltipZone component = button.GetComponent<TooltipZone>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.HideTooltip();
  }

  private void OnButtonPressed(UIEvent e)
  {
    PegUIElement element = e.GetElement();
    NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
    bool tournament = netObject.Games.Tournament;
    bool practice = netObject.Games.Practice;
    bool flag1 = netObject.Games.Forge && AchieveManager.Get() != null && AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.FORGE);
    if (flag1)
      flag1 = HealthyGamingMgr.Get().isArenaEnabled();
    bool manager = netObject.Collection.Manager;
    bool flag2 = netObject.Games.TavernBrawl && TavernBrawlManager.Get().HasUnlockedTavernBrawl;
    BoxMenuButton boxMenuButton = (BoxMenuButton) element;
    if ((UnityEngine.Object) boxMenuButton == (UnityEngine.Object) this.m_StartButton)
      this.OnStartButtonPressed(e);
    else if ((UnityEngine.Object) boxMenuButton == (UnityEngine.Object) this.m_TournamentButton && tournament)
      this.OnTournamentButtonPressed(e);
    else if ((UnityEngine.Object) boxMenuButton == (UnityEngine.Object) this.m_SoloAdventuresButton && practice)
      this.OnSoloAdventuresButtonPressed(e);
    else if ((UnityEngine.Object) boxMenuButton == (UnityEngine.Object) this.m_ForgeButton && flag1)
      this.OnForgeButtonPressed(e);
    else if ((UnityEngine.Object) boxMenuButton == (UnityEngine.Object) this.m_TavernBrawlButton && flag2)
      this.OnTavernBrawlButtonPressed(e);
    else if ((UnityEngine.Object) boxMenuButton == (UnityEngine.Object) this.m_OpenPacksButton)
      this.OnOpenPacksButtonPressed(e);
    else if ((UnityEngine.Object) boxMenuButton == (UnityEngine.Object) this.m_CollectionButton && manager)
    {
      this.OnCollectionButtonPressed(e);
    }
    else
    {
      if (!((UnityEngine.Object) boxMenuButton == (UnityEngine.Object) this.m_setRotationButton))
        return;
      this.OnSetRotationButtonPressed(e);
    }
  }

  private void FireButtonPressEvent(Box.ButtonType buttonType)
  {
    if (this.m_waitingForSceneLoad)
    {
      this.m_queuedButtonFire = new Box.ButtonType?(buttonType);
    }
    else
    {
      foreach (Box.ButtonPressListener buttonPressListener in this.m_buttonPressListeners.ToArray())
        buttonPressListener.Fire(buttonType);
    }
  }

  private void ClearQueuedButtonFireEvent()
  {
    this.m_queuedButtonFire = new Box.ButtonType?();
  }

  private void OnStartButtonPressed(UIEvent e)
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
      this.ChangeState(Box.State.HUB);
    else
      this.FireButtonPressEvent(Box.ButtonType.START);
  }

  private void OnTournamentButtonPressed(UIEvent e)
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
    {
      this.ChangeState(Box.State.OPEN);
    }
    else
    {
      if (!Options.Get().HasOption(Option.HAS_CLICKED_TOURNAMENT))
        Options.Get().SetBool(Option.HAS_CLICKED_TOURNAMENT, true);
      AchieveManager.Get().NotifyOfClick(Achievement.ClickTriggerType.BUTTON_PLAY);
      this.FireButtonPressEvent(Box.ButtonType.TOURNAMENT);
    }
  }

  private void OnSetRotationButtonPressed(UIEvent e)
  {
    Log.Kyle.Print("Set Rotation Button Pressed!");
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
    {
      this.ChangeState(Box.State.SET_ROTATION_OPEN);
    }
    else
    {
      if (!Options.Get().HasOption(Option.HAS_CLICKED_TOURNAMENT))
        Options.Get().SetBool(Option.HAS_CLICKED_TOURNAMENT, true);
      AchieveManager.Get().NotifyOfClick(Achievement.ClickTriggerType.BUTTON_PLAY);
      this.FireButtonPressEvent(Box.ButtonType.SET_ROTATION);
    }
  }

  private void OnSoloAdventuresButtonPressed(UIEvent e)
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
    {
      this.ChangeState(Box.State.OPEN);
    }
    else
    {
      if (AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
        AchieveManager.Get().NotifyOfClick(Achievement.ClickTriggerType.BUTTON_ADVENTURE);
      this.FireButtonPressEvent(Box.ButtonType.ADVENTURE);
    }
  }

  private void OnForgeButtonPressed(UIEvent e)
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
    {
      this.ChangeState(Box.State.OPEN);
    }
    else
    {
      AchieveManager.Get().NotifyOfClick(Achievement.ClickTriggerType.BUTTON_ARENA);
      this.FireButtonPressEvent(Box.ButtonType.FORGE);
    }
  }

  private void OnTavernBrawlButtonPressed(UIEvent e)
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
      this.ChangeState(Box.State.OPEN);
    else
      this.FireButtonPressEvent(Box.ButtonType.TAVERN_BRAWL);
  }

  private void OnOpenPacksButtonPressed(UIEvent e)
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
      this.ChangeState(Box.State.OPEN);
    else
      this.FireButtonPressEvent(Box.ButtonType.OPEN_PACKS);
  }

  public void OnCollectionButtonPressed(UIEvent e)
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
      this.ChangeState(Box.State.OPEN);
    else
      this.FireButtonPressEvent(Box.ButtonType.COLLECTION);
  }

  public void OnQuestButtonPressed(UIEvent e)
  {
    if (ShownUIMgr.Get().HasShownUI())
      return;
    ShownUIMgr.Get().SetShownUI(ShownUIMgr.UI_WINDOW.QUEST_LOG);
    SoundManager.Get().LoadAndPlay("Small_Click", this.gameObject);
    this.m_tempInputBlocker = CameraUtils.CreateInputBlocker(Box.Get().GetCamera(), "QuestLogInputBlocker", (Component) null, 30f);
    SceneUtils.SetLayer(this.m_tempInputBlocker, GameLayer.IgnoreFullScreenEffects);
    this.m_tempInputBlocker.AddComponent<PegUIElement>();
    this.StopCoroutine("ShowQuestLogWhenReady");
    this.StartCoroutine("ShowQuestLogWhenReady");
  }

  private void OnButtonMouseOver(UIEvent e)
  {
    TooltipZone component = e.GetElement().gameObject.GetComponent<TooltipZone>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    bool flag1 = AchieveManager.Get() != null && AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.FORGE);
    string str = GameStrings.Get("GLUE_TOOLTIP_BUTTON_DISABLED_DESC");
    string bodytext = str;
    NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
    bool tournament = netObject.Games.Tournament;
    bool practice = netObject.Games.Practice;
    bool flag2 = netObject.Games.Forge && flag1;
    if (flag2)
      flag2 = HealthyGamingMgr.Get().isArenaEnabled();
    bool manager = netObject.Collection.Manager;
    if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_TournamentButton.gameObject && tournament)
      bodytext = GameStrings.Get("GLUE_TOOLTIP_BUTTON_TOURNAMENT_DESC");
    else if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_SoloAdventuresButton.gameObject && practice)
      bodytext = GameStrings.Get("GLUE_TOOLTIP_BUTTON_ADVENTURE_DESC");
    else if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_ForgeButton.gameObject)
    {
      if (flag2)
        bodytext = GameStrings.Get("GLUE_TOOLTIP_BUTTON_FORGE_DESC");
      else if (!flag1)
        bodytext = GameStrings.Format("GLUE_TOOLTIP_BUTTON_FORGE_NOT_UNLOCKED", (object) 20);
    }
    else if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_TavernBrawlButton.gameObject)
      bodytext = !netObject.Games.TavernBrawl ? str : (!TavernBrawlManager.Get().HasUnlockedTavernBrawl ? GameStrings.Get("GLUE_TOOLTIP_BUTTON_TAVERN_BRAWL_NOT_UNLOCKED") : GameStrings.Get("GLUE_TOOLTIP_BUTTON_TAVERN_BRAWL_DESC"));
    else if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_OpenPacksButton.gameObject)
      bodytext = GameStrings.Get("GLUE_TOOLTIP_BUTTON_PACKOPEN_DESC");
    else if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_CollectionButton.gameObject && manager)
      bodytext = GameStrings.Get("GLUE_TOOLTIP_BUTTON_COLLECTION_DESC");
    if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_TournamentButton.gameObject)
      component.ShowBoxTooltip(GameStrings.Get("GLUE_TOOLTIP_BUTTON_TOURNAMENT_HEADLINE"), bodytext);
    else if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_SoloAdventuresButton.gameObject)
      component.ShowBoxTooltip(GameStrings.Get("GLUE_TOOLTIP_BUTTON_ADVENTURE_HEADLINE"), bodytext);
    else if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_ForgeButton.gameObject)
      component.ShowBoxTooltip(GameStrings.Get("GLUE_TOOLTIP_BUTTON_FORGE_HEADLINE"), bodytext);
    else if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_TavernBrawlButton.gameObject)
      component.ShowBoxTooltip(GameStrings.Get("GLUE_TOOLTIP_BUTTON_TAVERN_BRAWL_HEADLINE"), bodytext);
    else if ((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_OpenPacksButton.gameObject)
    {
      component.ShowBoxTooltip(GameStrings.Get("GLUE_TOOLTIP_BUTTON_PACKOPEN_HEADLINE"), bodytext);
    }
    else
    {
      if (!((UnityEngine.Object) component.targetObject == (UnityEngine.Object) this.m_CollectionButton.gameObject))
        return;
      component.ShowBoxTooltip(GameStrings.Get("GLUE_TOOLTIP_BUTTON_COLLECTION_HEADLINE"), bodytext);
    }
  }

  private void OnButtonMouseOut(UIEvent e)
  {
    TooltipZone component = e.GetElement().gameObject.GetComponent<TooltipZone>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    component.HideTooltip();
  }

  public void InitializeNet()
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null)
      return;
    this.m_waitingForNetData = true;
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.STARTUP)
      return;
    NetCache.Get().RegisterScreenBox(new NetCache.NetCacheCallback(this.OnNetCacheReady));
  }

  private void ShutdownNet()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
  }

  private void HackRequestNetData()
  {
    this.m_waitingForNetData = true;
    this.UpdateUI(false);
    NetCache.Get().ReloadNetObject<NetCache.NetCacheBoosters>();
    NetCache.Get().ReloadNetObject<NetCache.NetCacheFeatures>();
  }

  private void HackRequestNetFeatures()
  {
    this.m_waitingForNetData = true;
    this.UpdateUI(false);
    NetCache.Get().ReloadNetObject<NetCache.NetCacheFeatures>();
  }

  private void OnNetCacheReady()
  {
    this.m_waitingForNetData = false;
    if (Network.ShouldBeConnectedToAurora())
      RankMgr.Get().SetRankPresenceField(NetCache.Get().GetNetObject<NetCache.NetCacheMedalInfo>());
    this.UpdateUI(false);
  }

  private int ComputeBoosterCount()
  {
    return NetCache.Get().GetNetObject<NetCache.NetCacheBoosters>().GetTotalNumBoosters();
  }

  public void UnloadQuestLog()
  {
    this.m_questLogNetCacheDataState = Box.DataState.UNLOADING;
    this.DestroyQuestLog();
  }

  [DebuggerHidden]
  private IEnumerator ShowQuestLogWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Box.\u003CShowQuestLogWhenReady\u003Ec__Iterator1E()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void DestroyQuestLog()
  {
    this.StopCoroutine("ShowQuestLogWhenReady");
    if ((UnityEngine.Object) this.m_questLog == (UnityEngine.Object) null)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_questLog.gameObject);
  }

  private void OnQuestLogLoaded(string name, GameObject go, object callbackData)
  {
    this.m_questLogLoading = false;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("QuestLogButton.OnQuestLogLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      this.m_questLog = go.GetComponent<QuestLog>();
      if ((UnityEngine.Object) this.m_questLog != (UnityEngine.Object) null)
        return;
      UnityEngine.Debug.LogError((object) string.Format("QuestLogButton.OnQuestLogLoaded() - ERROR \"{0}\" has no {1} component", (object) name, (object) typeof (QuestLog)));
    }
  }

  private void OnQuestLogNetCacheReady()
  {
    if (this.m_questLogNetCacheDataState == Box.DataState.UNLOADING)
      return;
    this.m_questLogNetCacheDataState = Box.DataState.RECEIVED;
  }

  private bool ShouldRequestData(Box.DataState state)
  {
    return state == Box.DataState.NONE || state == Box.DataState.UNLOADING;
  }

  private void OnStoreButtonReleased(UIEvent e)
  {
    if (FriendChallengeMgr.Get().HasChallenge() || this.m_StoreButton.IsVisualClosed())
      return;
    if (!StoreManager.Get().IsOpen())
    {
      SoundManager.Get().LoadAndPlay("Store_closed_button_click", this.gameObject);
    }
    else
    {
      FriendChallengeMgr.Get().OnStoreOpened();
      SoundManager.Get().LoadAndPlay("Small_Click", this.gameObject);
      StoreManager.Get().RegisterStoreShownListener(new StoreManager.StoreShownCallback(this.OnStoreShown));
      StoreManager.Get().StartGeneralTransaction();
    }
  }

  private void OnStoreShown(object userData)
  {
    StoreManager.Get().RemoveStoreShownListener(new StoreManager.StoreShownCallback(this.OnStoreShown));
  }

  private void SetRotation_ShowRotationDisk()
  {
    this.m_DiskCenter.SetActive(false);
    if ((UnityEngine.Object) this.m_setRotationDisk != (UnityEngine.Object) null)
    {
      this.m_setRotationDisk.SetActive(true);
    }
    else
    {
      this.m_setRotationDisk = AssetLoader.Get().LoadGameObject("TheBox_CenterDisk_SetRotation", true, false);
      this.m_setRotationDisk.SetActive(true);
      this.m_setRotationDisk.transform.parent = this.m_Disk.transform;
      this.m_setRotationDisk.transform.localPosition = Vector3.zero;
      this.m_setRotationDisk.transform.localRotation = Quaternion.identity;
      this.m_setRotationButton = this.m_setRotationDisk.GetComponentInChildren<BoxMenuButton>();
      this.m_StoreButton.gameObject.SetActive(false);
      this.m_QuestLogButton.gameObject.SetActive(false);
      HighlightState componentInChildren = this.m_setRotationButton.GetComponentInChildren<HighlightState>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
        componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
      this.RegisterButtonEvents((PegUIElement) this.m_setRotationButton);
    }
  }

  [DebuggerHidden]
  private IEnumerator SetRotationOpen_ChangeState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Box.\u003CSetRotationOpen_ChangeState\u003Ec__Iterator1F()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator SetRotation_StartSetRotationIntro()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Box.\u003CSetRotation_StartSetRotationIntro\u003Ec__Iterator20()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void SetRotation_ShowNerfedCards_DialogHidden(DialogBase dialog, object userData)
  {
    this.SetRotation_FinishShowingRewards();
  }

  private void SetRotation_FinishShowingRewards()
  {
    this.ChangeState(Box.State.SET_ROTATION);
    this.SetRotation_ShowRotationDisk();
  }

  private void InitializeComponents()
  {
    this.m_Logo.SetParent(this);
    this.m_Logo.SetInfo(this.m_StateInfoList.m_LogoInfo);
    this.m_StartButton.SetParent(this);
    this.m_StartButton.SetInfo(this.m_StateInfoList.m_StartButtonInfo);
    this.m_LeftDoor.SetParent(this);
    this.m_LeftDoor.SetInfo(this.m_StateInfoList.m_LeftDoorInfo);
    this.m_RightDoor.SetParent(this);
    this.m_RightDoor.SetInfo(this.m_StateInfoList.m_RightDoorInfo);
    this.m_RightDoor.EnableMaster(true);
    this.m_Disk.SetParent(this);
    this.m_Disk.SetInfo(this.m_StateInfoList.m_DiskInfo);
    this.m_TopSpinner.SetParent(this);
    this.m_TopSpinner.SetInfo(this.m_StateInfoList.m_SpinnerInfo);
    this.m_BottomSpinner.SetParent(this);
    this.m_BottomSpinner.SetInfo(this.m_StateInfoList.m_SpinnerInfo);
    this.m_Drawer.SetParent(this);
    this.m_Drawer.SetInfo(this.m_StateInfoList.m_DrawerInfo);
    this.m_Camera.SetParent(this);
    this.m_Camera.SetInfo(this.m_StateInfoList.m_CameraInfo);
    this.m_Camera.GetComponent<FullScreenEffects>().BlendToColorEnable = false;
  }

  private void OnBoxTopPhoneTextureLoaded(string name, UnityEngine.Object obj, object callbackData)
  {
    Texture texture = obj as Texture;
    foreach (MeshRenderer componentsInChild in this.gameObject.GetComponentsInChildren<MeshRenderer>())
    {
      Material sharedMaterial = componentsInChild.sharedMaterial;
      if ((UnityEngine.Object) sharedMaterial != (UnityEngine.Object) null)
      {
        Texture mainTexture = sharedMaterial.mainTexture;
        if ((UnityEngine.Object) mainTexture != (UnityEngine.Object) null && mainTexture.name.Equals("TheBox_Top"))
          componentsInChild.material.mainTexture = texture;
      }
    }
  }

  public enum State
  {
    INVALID,
    STARTUP,
    PRESS_START,
    LOADING,
    LOADING_HUB,
    HUB,
    HUB_WITH_DRAWER,
    OPEN,
    CLOSED,
    ERROR,
    SET_ROTATION_LOADING,
    SET_ROTATION,
    SET_ROTATION_OPEN,
  }

  public enum ButtonType
  {
    START,
    TOURNAMENT,
    ADVENTURE,
    FORGE,
    OPEN_PACKS,
    COLLECTION,
    TAVERN_BRAWL,
    SET_ROTATION,
  }

  private class TransitionFinishedListener : EventListener<Box.TransitionFinishedCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  private class ButtonPressListener : EventListener<Box.ButtonPressCallback>
  {
    public void Fire(Box.ButtonType buttonType)
    {
      this.m_callback(buttonType, this.m_userData);
    }
  }

  private class BoxStateConfig
  {
    public Box.BoxStateConfig.Part<BoxLogo.State> m_logoState = new Box.BoxStateConfig.Part<BoxLogo.State>();
    public Box.BoxStateConfig.Part<BoxStartButton.State> m_startButtonState = new Box.BoxStateConfig.Part<BoxStartButton.State>();
    public Box.BoxStateConfig.Part<BoxDoor.State> m_doorState = new Box.BoxStateConfig.Part<BoxDoor.State>();
    public Box.BoxStateConfig.Part<BoxDisk.State> m_diskState = new Box.BoxStateConfig.Part<BoxDisk.State>();
    public Box.BoxStateConfig.Part<BoxDrawer.State> m_drawerState = new Box.BoxStateConfig.Part<BoxDrawer.State>();
    public Box.BoxStateConfig.Part<BoxCamera.State> m_camState = new Box.BoxStateConfig.Part<BoxCamera.State>();
    public Box.BoxStateConfig.Part<bool> m_fullScreenBlackState = new Box.BoxStateConfig.Part<bool>();

    public class Part<T>
    {
      public bool m_ignore;
      public T m_state;
    }
  }

  private enum DataState
  {
    NONE,
    REQUEST_SENT,
    RECEIVED,
    UNLOADING,
  }

  public delegate void TransitionFinishedCallback(object userData);

  public delegate void ButtonPressCallback(Box.ButtonType buttonType, object userData);
}
