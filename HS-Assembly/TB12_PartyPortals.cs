﻿// Decompiled with JetBrains decompiler
// Type: TB12_PartyPortals
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;

public class TB12_PartyPortals : MissionEntity
{
  private Card m_bossCard;
  private Card m_mediva;
  private int times_emoted;
  private bool[] last_time_emoted;

  private void GetMediva()
  {
    Entity entity = GameState.Get().GetEntity(GameState.Get().GetGameEntity().GetTag(GAME_TAG.TAG_SCRIPT_DATA_ENT_1));
    if (entity == null)
      return;
    this.m_mediva = entity.GetCard();
  }

  public override void PreloadAssets()
  {
    this.last_time_emoted = new bool[100];
    this.PreloadSound("VO_Mediva_01_Female_Blood Elf_AranHeroPower_01");
    this.PreloadSound("VO_Mediva_01_Female_Blood Elf_ChessEmoteOops_03");
    this.PreloadSound("VO_Mediva_01_Female_Blood Elf_CroneTurn1_03");
    this.PreloadSound("VO_Mediva_01_Female_Blood Elf_CuratorBeasts_09");
    this.PreloadSound("VO_Mediva_01_Female_Blood Elf_CuratorBeasts_04");
    this.PreloadSound("VO_Mediva_01_Female_Blood Elf_IllhoofWounded_03");
    this.PreloadSound("VO_Mediva_01_Female_Blood Elf_JulianneDeadlyPoison_03");
    this.PreloadSound("VO_Mediva_01_Female_Blood Elf_NetherspiteTurn1_02");
    this.PreloadSound("VO_Mediva_01_Female_Blood Elf_PrologueTurn5_02");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_AranHeroPower_03");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_AranSpell_01");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_AranSpell_10");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_ChessEmoteOops_04");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_CroneTurn1_05");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_CuratorBeasts_05");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_IllhoofWounded_02");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_NetherspiteTurn1_04");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_CuratorBeasts_08");
    this.PreloadSound("VO_Mediva_02_Female_Draenei_PrologueTurn5_02");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_AranHeroPower_04");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_AranSpell_06");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_AranSpell_01");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_ChessEmoteOops_01");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_CuratorBeasts_01");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_IllhoofWounded_01");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_IllhoofWounded_04");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_JulianneDeadlyPoison_02");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_PrologueTurn5_03");
    this.PreloadSound("VO_Mediva_03_Male_Night Elf_PrologueTurn5_02");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB12_PartyPortals.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1F8() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
