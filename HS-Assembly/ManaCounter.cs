﻿// Decompiled with JetBrains decompiler
// Type: ManaCounter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ManaCounter : MonoBehaviour
{
  public Player.Side m_Side;
  public GameObject m_phoneGemContainer;
  public UberText m_availableManaPhone;
  public UberText m_permanentManaPhone;
  private Player m_player;
  private UberText m_textMesh;
  private GameObject m_phoneGem;

  private void Awake()
  {
    this.m_textMesh = this.GetComponent<UberText>();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_phoneGem = AssetLoader.Get().LoadActor("Resource_Large_phone", false, false);
    GameUtils.SetParent(this.m_phoneGem, this.m_phoneGemContainer, true);
  }

  private void Start()
  {
    this.m_textMesh.Text = GameStrings.Format("GAMEPLAY_MANA_COUNTER", (object) "0", (object) "0");
  }

  public void SetPlayer(Player player)
  {
    this.m_player = player;
  }

  public Player GetPlayer()
  {
    return this.m_player;
  }

  public GameObject GetPhoneGem()
  {
    return this.m_phoneGem;
  }

  public void UpdateText()
  {
    int tag = this.m_player.GetTag(GAME_TAG.RESOURCES);
    if (!this.gameObject.activeInHierarchy)
      this.gameObject.SetActive(true);
    int availableResources = this.m_player.GetNumAvailableResources();
    string str;
    if ((bool) UniversalInputManager.UsePhoneUI && tag >= 10)
      str = availableResources.ToString();
    else
      str = GameStrings.Format("GAMEPLAY_MANA_COUNTER", (object) availableResources, (object) tag);
    this.m_textMesh.Text = str;
    if (!(bool) UniversalInputManager.UsePhoneUI || !((Object) this.m_availableManaPhone != (Object) null))
      return;
    this.m_availableManaPhone.Text = availableResources.ToString();
    this.m_permanentManaPhone.Text = tag.ToString();
  }
}
