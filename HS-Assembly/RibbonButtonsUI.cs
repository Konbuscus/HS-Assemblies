﻿// Decompiled with JetBrains decompiler
// Type: RibbonButtonsUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RibbonButtonsUI : MonoBehaviour
{
  public float m_EaseInTime = 1f;
  public float m_EaseOutTime = 0.4f;
  public float m_minAspectRatioAdjustment = 0.24f;
  private bool m_shown = true;
  public List<RibbonButtonsUI.RibbonButtonObject> m_Ribbons;
  public Transform m_LeftBones;
  public Transform m_RightBones;
  public GameObject m_rootObject;
  public PegUIElement m_collectionManagerRibbon;
  public PegUIElement m_questLogRibbon;
  public PegUIElement m_packOpeningRibbon;
  public PegUIElement m_storeRibbon;
  public UberText m_packCount;
  public GameObject m_packCountFrame;

  public void Awake()
  {
    this.m_rootObject.SetActive(false);
    float num = (1f - TransformUtil.PhoneAspectRatioScale()) * this.m_minAspectRatioAdjustment;
    TransformUtil.SetLocalPosX((Component) this.m_LeftBones, this.m_LeftBones.localPosition.x + num);
    TransformUtil.SetLocalPosX((Component) this.m_RightBones, this.m_RightBones.localPosition.x - num);
  }

  public void Toggle(bool show)
  {
    this.m_shown = show;
    if (show)
      this.StartCoroutine(this.ShowRibbons());
    else
      this.StartCoroutine(this.HideRibbons());
  }

  [DebuggerHidden]
  private IEnumerator ShowRibbons()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RibbonButtonsUI.\u003CShowRibbons\u003Ec__Iterator65()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator HideRibbons()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RibbonButtonsUI.\u003CHideRibbons\u003Ec__Iterator66()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void SetPackCount(int packs)
  {
    if (packs <= 0)
    {
      this.m_packCount.Text = string.Empty;
      this.m_packCountFrame.SetActive(false);
    }
    else
    {
      this.m_packCount.Text = GameStrings.Format("GLUE_PACK_OPENING_BOOSTER_COUNT", (object) packs);
      this.m_packCountFrame.SetActive(true);
    }
  }

  [Serializable]
  public class RibbonButtonObject
  {
    public PegUIElement m_Ribbon;
    public Transform m_HiddenBone;
    public Transform m_ShownBone;
    public bool m_LeftSide;
    public float m_AnimateInDelay;
  }
}
