﻿// Decompiled with JetBrains decompiler
// Type: DraftInputManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DraftInputManager : MonoBehaviour
{
  private static DraftInputManager s_instance;

  private void Awake()
  {
    DraftInputManager.s_instance = this;
  }

  private void OnDestroy()
  {
    DraftInputManager.s_instance = (DraftInputManager) null;
  }

  public static DraftInputManager Get()
  {
    return DraftInputManager.s_instance;
  }

  public void Unload()
  {
  }

  public bool HandleKeyboardInput()
  {
    if ((Object) DraftDisplay.Get() == (Object) null)
      return false;
    if (Input.GetKeyUp(KeyCode.Escape) && DraftDisplay.Get().IsInHeroSelectMode())
    {
      DraftDisplay.Get().DoHeroCancelAnimation();
      return true;
    }
    if (!ApplicationMgr.IsInternal())
      return false;
    List<DraftCardVisual> cardVisuals = DraftDisplay.Get().GetCardVisuals();
    if (cardVisuals == null || cardVisuals.Count == 0)
      return false;
    int index = -1;
    if (Input.GetKeyUp(KeyCode.Alpha1))
      index = 0;
    else if (Input.GetKeyUp(KeyCode.Alpha2))
      index = 1;
    else if (Input.GetKeyUp(KeyCode.Alpha3))
      index = 2;
    if (index == -1 || cardVisuals.Count < index + 1)
      return false;
    DraftCardVisual draftCardVisual = cardVisuals[index];
    if ((Object) draftCardVisual == (Object) null)
      return false;
    draftCardVisual.ChooseThisCard();
    return true;
  }
}
