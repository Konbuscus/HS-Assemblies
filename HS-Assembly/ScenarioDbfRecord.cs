﻿// Decompiled with JetBrains decompiler
// Type: ScenarioDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ScenarioDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private int m_Players;
  [SerializeField]
  private int m_Player1HeroCardId;
  [SerializeField]
  private int m_Player2HeroCardId;
  [SerializeField]
  private bool m_IsTutorial;
  [SerializeField]
  private bool m_IsExpert;
  [SerializeField]
  private bool m_IsCoop;
  [SerializeField]
  private int m_AdventureId;
  [SerializeField]
  private int m_WingId;
  [SerializeField]
  private int m_SortOrder;
  [SerializeField]
  private int m_ModeId;
  [SerializeField]
  private int m_ClientPlayer2HeroCardId;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private DbfLocValue m_ShortName;
  [SerializeField]
  private DbfLocValue m_Description;
  [SerializeField]
  private DbfLocValue m_OpponentName;
  [SerializeField]
  private DbfLocValue m_CompletedDescription;
  [SerializeField]
  private int m_Player1DeckId;
  [SerializeField]
  private int m_DeckRulesetId;
  [SerializeField]
  private string m_TbTexture;
  [SerializeField]
  private string m_TbTexturePhone;
  [SerializeField]
  private float m_TbTexturePhoneOffsetY;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("PLAYERS", "number of players we need to start this template")]
  public int Players
  {
    get
    {
      return this.m_Players;
    }
  }

  [DbfField("PLAYER1_HERO_CARD_ID", "if not null then the specific ASSET.CARD.ID to be used")]
  public int Player1HeroCardId
  {
    get
    {
      return this.m_Player1HeroCardId;
    }
  }

  [DbfField("PLAYER2_HERO_CARD_ID", "if not null then the specific ASSET.CARD.ID to be used")]
  public int Player2HeroCardId
  {
    get
    {
      return this.m_Player2HeroCardId;
    }
  }

  [DbfField("IS_TUTORIAL", "")]
  public bool IsTutorial
  {
    get
    {
      return this.m_IsTutorial;
    }
  }

  [DbfField("IS_EXPERT", "expert or normal AI?")]
  public bool IsExpert
  {
    get
    {
      return this.m_IsExpert;
    }
  }

  [DbfField("IS_COOP", "flags this scenario as a cooperative scenario, meaning players share the same win/loss conditions")]
  public bool IsCoop
  {
    get
    {
      return this.m_IsCoop;
    }
  }

  [DbfField("ADVENTURE_ID", "ASSET.ADVENTURE.ID")]
  public int AdventureId
  {
    get
    {
      return this.m_AdventureId;
    }
  }

  [DbfField("WING_ID", "ASSET.WING.ID")]
  public int WingId
  {
    get
    {
      return this.m_WingId;
    }
  }

  [DbfField("SORT_ORDER", "sort order of this scenario in its wing (or adventure)")]
  public int SortOrder
  {
    get
    {
      return this.m_SortOrder;
    }
  }

  [DbfField("MODE_ID", "ASSET.ADVENTURE_MODE.ID")]
  public int ModeId
  {
    get
    {
      return this.m_ModeId;
    }
  }

  [DbfField("CLIENT_PLAYER2_HERO_CARD_ID", "client-only: if not 0 then overrides PLAYER2_HERO_CARD_ID column for what the client displays as the boss hero")]
  public int ClientPlayer2HeroCardId
  {
    get
    {
      return this.m_ClientPlayer2HeroCardId;
    }
  }

  [DbfField("NAME", "FK to LOC_STR.ID for the user-facing name of this scenario")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("SHORT_NAME", "FK to LOC_STR.ID for the user-facing name of this scenario in places that the name won't fit.")]
  public DbfLocValue ShortName
  {
    get
    {
      return this.m_ShortName;
    }
  }

  [DbfField("DESCRIPTION", "FK to LOC_STR.ID for the user-facing description of this scenario.")]
  public DbfLocValue Description
  {
    get
    {
      return this.m_Description;
    }
  }

  [DbfField("OPPONENT_NAME", "FK to LOC_STR.ID for the user-facing name of the player's opponent.")]
  public DbfLocValue OpponentName
  {
    get
    {
      return this.m_OpponentName;
    }
  }

  [DbfField("COMPLETED_DESCRIPTION", "FK to LOC_STR.ID for the user-facing mesage displayed after beating this scenario.")]
  public DbfLocValue CompletedDescription
  {
    get
    {
      return this.m_CompletedDescription;
    }
  }

  [DbfField("PLAYER1_DECK_ID", "")]
  public int Player1DeckId
  {
    get
    {
      return this.m_Player1DeckId;
    }
  }

  [DbfField("DECK_RULESET_ID", "the DECK_RULESET.ID used to create/validate decks for this scenario.")]
  public int DeckRulesetId
  {
    get
    {
      return this.m_DeckRulesetId;
    }
  }

  [DbfField("TB_TEXTURE", "chalkboard texture that shows in the main TB screen (non-phone).")]
  public string TbTexture
  {
    get
    {
      return this.m_TbTexture;
    }
  }

  [DbfField("TB_TEXTURE_PHONE", "chalkboard texture that shows in the main TB screen (phones only).")]
  public string TbTexturePhone
  {
    get
    {
      return this.m_TbTexturePhone;
    }
  }

  [DbfField("TB_TEXTURE_PHONE_OFFSET_Y", "offset into the TB_TEXTURE_PHONE - used by client.")]
  public float TbTexturePhoneOffsetY
  {
    get
    {
      return this.m_TbTexturePhoneOffsetY;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    ScenarioDbfAsset scenarioDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (ScenarioDbfAsset)) as ScenarioDbfAsset;
    if ((UnityEngine.Object) scenarioDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("ScenarioDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < scenarioDbfAsset.Records.Count; ++index)
      scenarioDbfAsset.Records[index].StripUnusedLocales();
    records = (object) scenarioDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Name.StripUnusedLocales();
    this.m_ShortName.StripUnusedLocales();
    this.m_Description.StripUnusedLocales();
    this.m_OpponentName.StripUnusedLocales();
    this.m_CompletedDescription.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetPlayers(int v)
  {
    this.m_Players = v;
  }

  public void SetPlayer1HeroCardId(int v)
  {
    this.m_Player1HeroCardId = v;
  }

  public void SetPlayer2HeroCardId(int v)
  {
    this.m_Player2HeroCardId = v;
  }

  public void SetIsTutorial(bool v)
  {
    this.m_IsTutorial = v;
  }

  public void SetIsExpert(bool v)
  {
    this.m_IsExpert = v;
  }

  public void SetIsCoop(bool v)
  {
    this.m_IsCoop = v;
  }

  public void SetAdventureId(int v)
  {
    this.m_AdventureId = v;
  }

  public void SetWingId(int v)
  {
    this.m_WingId = v;
  }

  public void SetSortOrder(int v)
  {
    this.m_SortOrder = v;
  }

  public void SetModeId(int v)
  {
    this.m_ModeId = v;
  }

  public void SetClientPlayer2HeroCardId(int v)
  {
    this.m_ClientPlayer2HeroCardId = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetShortName(DbfLocValue v)
  {
    this.m_ShortName = v;
    v.SetDebugInfo(this.ID, "SHORT_NAME");
  }

  public void SetDescription(DbfLocValue v)
  {
    this.m_Description = v;
    v.SetDebugInfo(this.ID, "DESCRIPTION");
  }

  public void SetOpponentName(DbfLocValue v)
  {
    this.m_OpponentName = v;
    v.SetDebugInfo(this.ID, "OPPONENT_NAME");
  }

  public void SetCompletedDescription(DbfLocValue v)
  {
    this.m_CompletedDescription = v;
    v.SetDebugInfo(this.ID, "COMPLETED_DESCRIPTION");
  }

  public void SetPlayer1DeckId(int v)
  {
    this.m_Player1DeckId = v;
  }

  public void SetDeckRulesetId(int v)
  {
    this.m_DeckRulesetId = v;
  }

  public void SetTbTexture(string v)
  {
    this.m_TbTexture = v;
  }

  public void SetTbTexturePhone(string v)
  {
    this.m_TbTexturePhone = v;
  }

  public void SetTbTexturePhoneOffsetY(float v)
  {
    this.m_TbTexturePhoneOffsetY = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ScenarioDbfRecord.\u003C\u003Ef__switch\u0024map5F == null)
      {
        // ISSUE: reference to a compiler-generated field
        ScenarioDbfRecord.\u003C\u003Ef__switch\u0024map5F = new Dictionary<string, int>(23)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "PLAYERS",
            2
          },
          {
            "PLAYER1_HERO_CARD_ID",
            3
          },
          {
            "PLAYER2_HERO_CARD_ID",
            4
          },
          {
            "IS_TUTORIAL",
            5
          },
          {
            "IS_EXPERT",
            6
          },
          {
            "IS_COOP",
            7
          },
          {
            "ADVENTURE_ID",
            8
          },
          {
            "WING_ID",
            9
          },
          {
            "SORT_ORDER",
            10
          },
          {
            "MODE_ID",
            11
          },
          {
            "CLIENT_PLAYER2_HERO_CARD_ID",
            12
          },
          {
            "NAME",
            13
          },
          {
            "SHORT_NAME",
            14
          },
          {
            "DESCRIPTION",
            15
          },
          {
            "OPPONENT_NAME",
            16
          },
          {
            "COMPLETED_DESCRIPTION",
            17
          },
          {
            "PLAYER1_DECK_ID",
            18
          },
          {
            "DECK_RULESET_ID",
            19
          },
          {
            "TB_TEXTURE",
            20
          },
          {
            "TB_TEXTURE_PHONE",
            21
          },
          {
            "TB_TEXTURE_PHONE_OFFSET_Y",
            22
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ScenarioDbfRecord.\u003C\u003Ef__switch\u0024map5F.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Players;
          case 3:
            return (object) this.Player1HeroCardId;
          case 4:
            return (object) this.Player2HeroCardId;
          case 5:
            return (object) this.IsTutorial;
          case 6:
            return (object) this.IsExpert;
          case 7:
            return (object) this.IsCoop;
          case 8:
            return (object) this.AdventureId;
          case 9:
            return (object) this.WingId;
          case 10:
            return (object) this.SortOrder;
          case 11:
            return (object) this.ModeId;
          case 12:
            return (object) this.ClientPlayer2HeroCardId;
          case 13:
            return (object) this.Name;
          case 14:
            return (object) this.ShortName;
          case 15:
            return (object) this.Description;
          case 16:
            return (object) this.OpponentName;
          case 17:
            return (object) this.CompletedDescription;
          case 18:
            return (object) this.Player1DeckId;
          case 19:
            return (object) this.DeckRulesetId;
          case 20:
            return (object) this.TbTexture;
          case 21:
            return (object) this.TbTexturePhone;
          case 22:
            return (object) this.TbTexturePhoneOffsetY;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (ScenarioDbfRecord.\u003C\u003Ef__switch\u0024map60 == null)
    {
      // ISSUE: reference to a compiler-generated field
      ScenarioDbfRecord.\u003C\u003Ef__switch\u0024map60 = new Dictionary<string, int>(23)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "PLAYERS",
          2
        },
        {
          "PLAYER1_HERO_CARD_ID",
          3
        },
        {
          "PLAYER2_HERO_CARD_ID",
          4
        },
        {
          "IS_TUTORIAL",
          5
        },
        {
          "IS_EXPERT",
          6
        },
        {
          "IS_COOP",
          7
        },
        {
          "ADVENTURE_ID",
          8
        },
        {
          "WING_ID",
          9
        },
        {
          "SORT_ORDER",
          10
        },
        {
          "MODE_ID",
          11
        },
        {
          "CLIENT_PLAYER2_HERO_CARD_ID",
          12
        },
        {
          "NAME",
          13
        },
        {
          "SHORT_NAME",
          14
        },
        {
          "DESCRIPTION",
          15
        },
        {
          "OPPONENT_NAME",
          16
        },
        {
          "COMPLETED_DESCRIPTION",
          17
        },
        {
          "PLAYER1_DECK_ID",
          18
        },
        {
          "DECK_RULESET_ID",
          19
        },
        {
          "TB_TEXTURE",
          20
        },
        {
          "TB_TEXTURE_PHONE",
          21
        },
        {
          "TB_TEXTURE_PHONE_OFFSET_Y",
          22
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!ScenarioDbfRecord.\u003C\u003Ef__switch\u0024map60.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetPlayers((int) val);
        break;
      case 3:
        this.SetPlayer1HeroCardId((int) val);
        break;
      case 4:
        this.SetPlayer2HeroCardId((int) val);
        break;
      case 5:
        this.SetIsTutorial((bool) val);
        break;
      case 6:
        this.SetIsExpert((bool) val);
        break;
      case 7:
        this.SetIsCoop((bool) val);
        break;
      case 8:
        this.SetAdventureId((int) val);
        break;
      case 9:
        this.SetWingId((int) val);
        break;
      case 10:
        this.SetSortOrder((int) val);
        break;
      case 11:
        this.SetModeId((int) val);
        break;
      case 12:
        this.SetClientPlayer2HeroCardId((int) val);
        break;
      case 13:
        this.SetName((DbfLocValue) val);
        break;
      case 14:
        this.SetShortName((DbfLocValue) val);
        break;
      case 15:
        this.SetDescription((DbfLocValue) val);
        break;
      case 16:
        this.SetOpponentName((DbfLocValue) val);
        break;
      case 17:
        this.SetCompletedDescription((DbfLocValue) val);
        break;
      case 18:
        this.SetPlayer1DeckId((int) val);
        break;
      case 19:
        this.SetDeckRulesetId((int) val);
        break;
      case 20:
        this.SetTbTexture((string) val);
        break;
      case 21:
        this.SetTbTexturePhone((string) val);
        break;
      case 22:
        this.SetTbTexturePhoneOffsetY((float) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ScenarioDbfRecord.\u003C\u003Ef__switch\u0024map61 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ScenarioDbfRecord.\u003C\u003Ef__switch\u0024map61 = new Dictionary<string, int>(23)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "PLAYERS",
            2
          },
          {
            "PLAYER1_HERO_CARD_ID",
            3
          },
          {
            "PLAYER2_HERO_CARD_ID",
            4
          },
          {
            "IS_TUTORIAL",
            5
          },
          {
            "IS_EXPERT",
            6
          },
          {
            "IS_COOP",
            7
          },
          {
            "ADVENTURE_ID",
            8
          },
          {
            "WING_ID",
            9
          },
          {
            "SORT_ORDER",
            10
          },
          {
            "MODE_ID",
            11
          },
          {
            "CLIENT_PLAYER2_HERO_CARD_ID",
            12
          },
          {
            "NAME",
            13
          },
          {
            "SHORT_NAME",
            14
          },
          {
            "DESCRIPTION",
            15
          },
          {
            "OPPONENT_NAME",
            16
          },
          {
            "COMPLETED_DESCRIPTION",
            17
          },
          {
            "PLAYER1_DECK_ID",
            18
          },
          {
            "DECK_RULESET_ID",
            19
          },
          {
            "TB_TEXTURE",
            20
          },
          {
            "TB_TEXTURE_PHONE",
            21
          },
          {
            "TB_TEXTURE_PHONE_OFFSET_Y",
            22
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ScenarioDbfRecord.\u003C\u003Ef__switch\u0024map61.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (int);
          case 3:
            return typeof (int);
          case 4:
            return typeof (int);
          case 5:
            return typeof (bool);
          case 6:
            return typeof (bool);
          case 7:
            return typeof (bool);
          case 8:
            return typeof (int);
          case 9:
            return typeof (int);
          case 10:
            return typeof (int);
          case 11:
            return typeof (int);
          case 12:
            return typeof (int);
          case 13:
            return typeof (DbfLocValue);
          case 14:
            return typeof (DbfLocValue);
          case 15:
            return typeof (DbfLocValue);
          case 16:
            return typeof (DbfLocValue);
          case 17:
            return typeof (DbfLocValue);
          case 18:
            return typeof (int);
          case 19:
            return typeof (int);
          case 20:
            return typeof (string);
          case 21:
            return typeof (string);
          case 22:
            return typeof (float);
        }
      }
    }
    return (System.Type) null;
  }
}
