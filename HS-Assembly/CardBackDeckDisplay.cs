﻿// Decompiled with JetBrains decompiler
// Type: CardBackDeckDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardBackDeckDisplay : MonoBehaviour
{
  public bool m_FriendlyDeck = true;
  private CardBackManager m_CardBackManager;

  private void Start()
  {
    this.m_CardBackManager = CardBackManager.Get();
    if ((Object) this.m_CardBackManager == (Object) null)
    {
      if ((Object) ApplicationMgr.Get() != (Object) null)
        Debug.LogError((object) "Failed to get CardBackManager!");
      this.enabled = false;
    }
    this.UpdateDeckCardBacks();
  }

  public void UpdateDeckCardBacks()
  {
    if ((Object) this.m_CardBackManager == (Object) null)
      return;
    this.m_CardBackManager.UpdateDeck(this.gameObject, this.m_FriendlyDeck);
  }
}
