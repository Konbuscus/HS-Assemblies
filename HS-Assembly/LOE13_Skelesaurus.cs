﻿// Decompiled with JetBrains decompiler
// Type: LOE13_Skelesaurus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LOE13_Skelesaurus : LOE_MissionEntity
{
  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOE_13_TURN_1");
    this.PreloadSound("VO_LOE_13_TURN_1_2");
    this.PreloadSound("VO_LOE_13_TURN_5");
    this.PreloadSound("VO_LOE_13_TURN_5_2");
    this.PreloadSound("VO_LOE_13_TURN_9");
    this.PreloadSound("VO_LOE_13_TURN_9_2");
    this.PreloadSound("VO_LOE_13_WIN");
    this.PreloadSound("LOEA13_1_SkelesaurusHex_EmoteResponse");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "LOEA13_1_SkelesaurusHex_EmoteResponse",
            m_stringTag = "VO_LOE_13_RESPONSE"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE13_Skelesaurus.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator17A() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE13_Skelesaurus.\u003CHandleGameOverWithTiming\u003Ec__Iterator17B() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
