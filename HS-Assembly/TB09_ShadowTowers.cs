﻿// Decompiled with JetBrains decompiler
// Type: TB09_ShadowTowers
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TB09_ShadowTowers : MissionEntity
{
  private static readonly Dictionary<int, string> minionMsgs = new Dictionary<int, string>() { { 1, "TB_SHADOWTOWERS_SHADOWSPAWNED" }, { 2, "TB_SHADOWTOWERS_SHADOWSPAWNED" }, { 3, "TB_SHADOWTOWERS_ADJACENTMINIONS" }, { 4, "TB_SHADOWTOWERS_SHADOWSPAWNEDNEXT" } };
  private HashSet<int> seen = new HashSet<int>();
  private Notification ShadowTowerPopup;
  private Notification MinionPopup;
  private Vector3 popUpPos;
  private string textID;
  private bool doPopup;
  private bool doLeftArrow;
  private bool doUpArrow;
  private bool doDownArrow;
  private float delayTime;
  private float popupDuration;

  public override void PreloadAssets()
  {
    this.PreloadSound("tutorial_mission_hero_coin_mouse_away");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB09_ShadowTowers.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1F4() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
