﻿// Decompiled with JetBrains decompiler
// Type: SpellTargetBehavior
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum SpellTargetBehavior
{
  DEFAULT,
  FRIENDLY_PLAY_ZONE_CENTER,
  FRIENDLY_PLAY_ZONE_RANDOM,
  OPPONENT_PLAY_ZONE_CENTER,
  OPPONENT_PLAY_ZONE_RANDOM,
  BOARD_CENTER,
  UNTARGETED,
  CHOSEN_TARGET_ONLY,
  BOARD_RANDOM,
  TARGET_ZONE_CENTER,
  NEW_CREATED_CARDS,
}
