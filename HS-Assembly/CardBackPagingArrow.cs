﻿// Decompiled with JetBrains decompiler
// Type: CardBackPagingArrow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class CardBackPagingArrow : CardBackPagingArrowBase
{
  public ArrowModeButton button;

  public override void EnablePaging(bool enable)
  {
    this.button.Activate(enable);
  }

  public override void AddEventListener(UIEventType eventType, UIEvent.Handler handler)
  {
    this.button.AddEventListener(eventType, handler);
  }
}
