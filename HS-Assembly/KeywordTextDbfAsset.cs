﻿// Decompiled with JetBrains decompiler
// Type: KeywordTextDbfAsset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class KeywordTextDbfAsset : ScriptableObject
{
  public List<KeywordTextDbfRecord> Records = new List<KeywordTextDbfRecord>();
}
