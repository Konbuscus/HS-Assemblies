﻿// Decompiled with JetBrains decompiler
// Type: ZoneChangeList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class ZoneChangeList
{
  private List<ZoneChange> m_changes = new List<ZoneChange>();
  private HashSet<Zone> m_dirtyZones = new HashSet<Zone>();
  private int m_id;
  private int m_predictedPosition;
  private bool m_ignoreCardZoneChanges;
  private bool m_canceledChangeList;
  private PowerTaskList m_taskList;
  private bool m_complete;
  private ZoneMgr.ChangeCompleteCallback m_completeCallback;
  private object m_completeCallbackUserData;

  public int GetId()
  {
    return this.m_id;
  }

  public void SetId(int id)
  {
    this.m_id = id;
  }

  public bool IsLocal()
  {
    return this.m_taskList == null;
  }

  public int GetPredictedPosition()
  {
    return this.m_predictedPosition;
  }

  public void SetPredictedPosition(int pos)
  {
    this.m_predictedPosition = pos;
  }

  public bool DoesIgnoreCardZoneChanges()
  {
    return this.m_ignoreCardZoneChanges;
  }

  public void SetIgnoreCardZoneChanges(bool ignore)
  {
    this.m_ignoreCardZoneChanges = ignore;
  }

  public bool IsCanceledChangeList()
  {
    return this.m_canceledChangeList;
  }

  public void SetCanceledChangeList(bool canceledChangeList)
  {
    this.m_canceledChangeList = canceledChangeList;
  }

  public void SetZoneInputBlocking(bool block)
  {
    for (int index = 0; index < this.m_changes.Count; ++index)
    {
      ZoneChange change = this.m_changes[index];
      Zone sourceZone = change.GetSourceZone();
      if ((Object) sourceZone != (Object) null)
        sourceZone.BlockInput(block);
      Zone destinationZone = change.GetDestinationZone();
      if ((Object) destinationZone != (Object) null)
        destinationZone.BlockInput(block);
    }
  }

  public bool IsComplete()
  {
    return this.m_complete;
  }

  public ZoneMgr.ChangeCompleteCallback GetCompleteCallback()
  {
    return this.m_completeCallback;
  }

  public void SetCompleteCallback(ZoneMgr.ChangeCompleteCallback callback)
  {
    this.m_completeCallback = callback;
  }

  public object GetCompleteCallbackUserData()
  {
    return this.m_completeCallbackUserData;
  }

  public void SetCompleteCallbackUserData(object userData)
  {
    this.m_completeCallbackUserData = userData;
  }

  public void FireCompleteCallback()
  {
    this.DebugPrint("ZoneChangeList.FireCompleteCallback() - m_id={0} m_taskList={1} m_changes.Count={2} m_complete={3} m_completeCallback={4}", (object) this.m_id, (object) (this.m_taskList != null ? this.m_taskList.GetId().ToString() : "(null)"), (object) this.m_changes.Count, (object) this.m_complete, (object) (this.m_completeCallback != null ? "(not null)" : "(null)"));
    if (this.m_completeCallback == null)
      return;
    this.m_completeCallback(this, this.m_completeCallbackUserData);
  }

  public PowerTaskList GetTaskList()
  {
    return this.m_taskList;
  }

  public void SetTaskList(PowerTaskList taskList)
  {
    this.m_taskList = taskList;
  }

  public List<ZoneChange> GetChanges()
  {
    return this.m_changes;
  }

  public ZoneChange GetChange(int index)
  {
    return this.m_changes[index];
  }

  public ZoneChange GetLocalTriggerChange()
  {
    if (!this.IsLocal())
      return (ZoneChange) null;
    if (this.m_changes.Count > 0)
      return this.m_changes[0];
    return (ZoneChange) null;
  }

  public Card GetLocalTriggerCard()
  {
    ZoneChange localTriggerChange = this.GetLocalTriggerChange();
    if (localTriggerChange == null)
      return (Card) null;
    return localTriggerChange.GetEntity().GetCard();
  }

  public void AddChange(ZoneChange change)
  {
    this.m_changes.Add(change);
  }

  public void InsertChange(int index, ZoneChange change)
  {
    this.m_changes.Insert(index, change);
  }

  public void ClearChanges()
  {
    this.m_changes.Clear();
  }

  [DebuggerHidden]
  public IEnumerator ProcessChanges()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ZoneChangeList.\u003CProcessChanges\u003Ec__IteratorE9() { \u003C\u003Ef__this = this };
  }

  public override string ToString()
  {
    return string.Format("id={0} changes={1} complete={2} local={3} localTrigger=[{4}]", (object) this.m_id, (object) this.m_changes.Count, (object) this.m_complete, (object) this.IsLocal(), (object) this.GetLocalTriggerChange());
  }

  private bool IsDisplayableDyingSecret(Entity entity, Card card, Zone srcZone, Zone dstZone)
  {
    return entity.IsSecret() && srcZone is ZoneSecret && dstZone is ZoneGraveyard;
  }

  private void ProcessDyingSecrets(Map<Player, DyingSecretGroup> dyingSecretMap)
  {
    if (dyingSecretMap == null)
      return;
    Map<Player, DeadSecretGroup> deadSecretMap = (Map<Player, DeadSecretGroup>) null;
    using (Map<Player, DyingSecretGroup>.Enumerator enumerator = dyingSecretMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<Player, DyingSecretGroup> current = enumerator.Current;
        Player key = current.Key;
        DyingSecretGroup dyingSecretGroup = current.Value;
        Card mainCard = dyingSecretGroup.GetMainCard();
        List<Card> cards = dyingSecretGroup.GetCards();
        List<Actor> actors = dyingSecretGroup.GetActors();
        for (int index = 0; index < cards.Count; ++index)
        {
          Card card = cards[index];
          Actor oldActor = actors[index];
          if (card.WasSecretTriggered())
          {
            oldActor.Destroy();
          }
          else
          {
            if ((Object) card == (Object) mainCard && card.CanShowSecretDeath())
              card.ShowSecretDeath(oldActor);
            else
              oldActor.Destroy();
            if (deadSecretMap == null)
              deadSecretMap = new Map<Player, DeadSecretGroup>();
            DeadSecretGroup deadSecretGroup;
            if (!deadSecretMap.TryGetValue(key, out deadSecretGroup))
            {
              deadSecretGroup = new DeadSecretGroup();
              deadSecretGroup.SetMainCard(mainCard);
              deadSecretMap.Add(key, deadSecretGroup);
            }
            deadSecretGroup.AddCard(card);
          }
        }
      }
    }
    BigCard.Get().ShowSecretDeaths(deadSecretMap);
  }

  private bool MustWaitForChoices()
  {
    if (!ChoiceCardMgr.Get().HasChoices())
      return false;
    PowerProcessor powerProcessor = GameState.Get().GetPowerProcessor();
    if (powerProcessor.HasGameOverTaskList())
      return false;
    using (Map<int, Player>.KeyCollection.Enumerator enumerator = GameState.Get().GetPlayerMap().Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PowerTaskList preChoiceTaskList = ChoiceCardMgr.Get().GetPreChoiceTaskList(enumerator.Current);
        if (preChoiceTaskList != null && !powerProcessor.HasTaskList(preChoiceTaskList))
          return true;
      }
    }
    return false;
  }

  [DebuggerHidden]
  private IEnumerator WaitForAndRemoveLoadingEntity(HashSet<Entity> loadingEntities, Entity entity, Card card)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ZoneChangeList.\u003CWaitForAndRemoveLoadingEntity\u003Ec__IteratorEA() { entity = entity, card = card, loadingEntities = loadingEntities, \u003C\u0024\u003Eentity = entity, \u003C\u0024\u003Ecard = card, \u003C\u0024\u003EloadingEntities = loadingEntities, \u003C\u003Ef__this = this };
  }

  private bool IsEntityLoading(Entity entity, Card card)
  {
    return entity.IsLoadingAssets() || (Object) card != (Object) null && card.IsActorLoading();
  }

  private bool ShouldForceUpdateActor(ZoneChange change)
  {
    return change.GetEntity().GetCardType() == TAG_CARDTYPE.SPELL && (Object) change.GetSourceZone() == (Object) null && change.GetDestinationZoneTag() == TAG_ZONE.PLAY;
  }

  [DebuggerHidden]
  private IEnumerator UpdateDirtyZones(HashSet<Entity> loadingEntities)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ZoneChangeList.\u003CUpdateDirtyZones\u003Ec__IteratorEB() { loadingEntities = loadingEntities, \u003C\u0024\u003EloadingEntities = loadingEntities, \u003C\u003Ef__this = this };
  }

  private bool IsDeathBlock()
  {
    if (this.m_taskList == null)
      return false;
    return this.m_taskList.IsDeathBlock();
  }

  [DebuggerHidden]
  private IEnumerator ZoneHand_UpdateLayout(ZoneHand zoneHand)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ZoneChangeList.\u003CZoneHand_UpdateLayout\u003Ec__IteratorEC() { zoneHand = zoneHand, \u003C\u0024\u003EzoneHand = zoneHand, \u003C\u003Ef__this = this };
  }

  private void OnUpdateLayoutComplete(Zone zone, object userData)
  {
    this.DebugPrint("ZoneChangeList.OnUpdateLayoutComplete() - m_id={0} END waiting for zone {1}", (object) this.m_id, (object) zone);
    this.m_dirtyZones.Remove(zone);
  }

  [DebuggerHidden]
  private IEnumerator FinishWhenPossible()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ZoneChangeList.\u003CFinishWhenPossible\u003Ec__IteratorED() { \u003C\u003Ef__this = this };
  }

  private void Finish()
  {
    this.m_complete = true;
    Log.Zone.Print("ZoneChangeList.Finish() - {0}", (object) this);
  }

  [Conditional("ZONE_CHANGE_DEBUG")]
  private void DebugPrint(string format, params object[] args)
  {
    string format1 = string.Format(format, args);
    Log.Zone.Print(format1);
  }
}
