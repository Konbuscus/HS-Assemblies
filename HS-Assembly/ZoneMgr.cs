﻿// Decompiled with JetBrains decompiler
// Type: ZoneMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusGame;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ZoneMgr : MonoBehaviour
{
  private Map<System.Type, string> m_tweenNames = new Map<System.Type, string>() { { typeof (ZoneHand), "ZoneHandUpdateLayout" }, { typeof (ZonePlay), "ZonePlayUpdateLayout" }, { typeof (ZoneWeapon), "ZoneWeaponUpdateLayout" } };
  private List<Zone> m_zones = new List<Zone>();
  private int m_nextLocalChangeListId = 1;
  private int m_nextServerChangeListId = 1;
  private Queue<ZoneChangeList> m_pendingServerChangeLists = new Queue<ZoneChangeList>();
  private Map<int, Entity> m_tempEntityMap = new Map<int, Entity>();
  private Map<Zone, ZoneMgr.TempZone> m_tempZoneMap = new Map<Zone, ZoneMgr.TempZone>();
  private List<ZoneChangeList> m_activeLocalChangeLists = new List<ZoneChangeList>();
  private List<ZoneChangeList> m_pendingLocalChangeLists = new List<ZoneChangeList>();
  private QueueList<ZoneChangeList> m_localChangeListHistory = new QueueList<ZoneChangeList>();
  private static ZoneMgr s_instance;
  private ZoneChangeList m_activeServerChangeList;
  private float m_nextDeathBlockLayoutDelaySec;

  private void Awake()
  {
    ZoneMgr.s_instance = this;
    foreach (Zone componentsInChild in this.gameObject.GetComponentsInChildren<Zone>())
      this.m_zones.Add(componentsInChild);
    if (GameState.Get() == null)
      return;
    GameState.Get().RegisterCurrentPlayerChangedListener(new GameState.CurrentPlayerChangedCallback(this.OnCurrentPlayerChanged));
    GameState.Get().RegisterOptionRejectedListener(new GameState.OptionRejectedCallback(this.OnOptionRejected), (object) null);
  }

  private void Start()
  {
    InputManager inputManager = InputManager.Get();
    if (!((UnityEngine.Object) inputManager != (UnityEngine.Object) null))
      return;
    inputManager.StartWatchingForInput();
  }

  private void Update()
  {
    this.UpdateLocalChangeLists();
    this.UpdateServerChangeLists();
  }

  private void OnDestroy()
  {
    if (GameState.Get() != null)
    {
      GameState.Get().UnregisterCurrentPlayerChangedListener(new GameState.CurrentPlayerChangedCallback(this.OnCurrentPlayerChanged));
      GameState.Get().UnregisterOptionRejectedListener(new GameState.OptionRejectedCallback(this.OnOptionRejected), (object) null);
    }
    ZoneMgr.s_instance = (ZoneMgr) null;
  }

  public static ZoneMgr Get()
  {
    return ZoneMgr.s_instance;
  }

  public List<Zone> GetZones()
  {
    return this.m_zones;
  }

  public Zone FindZoneForTags(int controllerId, TAG_ZONE zoneTag, TAG_CARDTYPE cardType)
  {
    if (controllerId == 0)
      return (Zone) null;
    if (zoneTag == TAG_ZONE.INVALID)
      return (Zone) null;
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current.CanAcceptTags(controllerId, zoneTag, cardType))
          return current;
      }
    }
    return (Zone) null;
  }

  public Zone FindZoneForEntity(Entity entity)
  {
    if (entity.GetZone() == TAG_ZONE.INVALID)
      return (Zone) null;
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current.CanAcceptTags(entity.GetControllerId(), entity.GetZone(), entity.GetCardType()))
          return current;
      }
    }
    return (Zone) null;
  }

  public Zone FindZoneForEntityAndZoneTag(Entity entity, TAG_ZONE zoneTag)
  {
    if (zoneTag == TAG_ZONE.INVALID)
      return (Zone) null;
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current.CanAcceptTags(entity.GetControllerId(), zoneTag, entity.GetCardType()))
          return current;
      }
    }
    return (Zone) null;
  }

  public Zone FindZoneForEntityAndController(Entity entity, int controllerId)
  {
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current.CanAcceptTags(controllerId, entity.GetZone(), entity.GetCardType()))
          return current;
      }
    }
    return (Zone) null;
  }

  public Zone FindZoneForFullEntity(Network.HistFullEntity fullEntity)
  {
    int controllerId = 0;
    TAG_ZONE zoneTag = TAG_ZONE.INVALID;
    TAG_CARDTYPE cardType = TAG_CARDTYPE.INVALID;
    using (List<Network.Entity.Tag>.Enumerator enumerator = fullEntity.Entity.Tags.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        switch ((GAME_TAG) current.Name)
        {
          case GAME_TAG.ZONE:
            zoneTag = (TAG_ZONE) current.Value;
            continue;
          case GAME_TAG.CONTROLLER:
            controllerId = current.Value;
            continue;
          case GAME_TAG.CARDTYPE:
            cardType = (TAG_CARDTYPE) current.Value;
            continue;
          default:
            continue;
        }
      }
    }
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current.CanAcceptTags(controllerId, zoneTag, cardType))
          return current;
      }
    }
    return (Zone) null;
  }

  public Zone FindZoneForShowEntity(Entity entity, Network.HistShowEntity showEntity)
  {
    int controllerId = entity.GetControllerId();
    TAG_ZONE zone = entity.GetZone();
    TAG_CARDTYPE cardType = entity.GetCardType();
    using (List<Network.Entity.Tag>.Enumerator enumerator = showEntity.Entity.Tags.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        switch ((GAME_TAG) current.Name)
        {
          case GAME_TAG.ZONE:
            zone = (TAG_ZONE) current.Value;
            continue;
          case GAME_TAG.CONTROLLER:
            controllerId = current.Value;
            continue;
          case GAME_TAG.CARDTYPE:
            cardType = (TAG_CARDTYPE) current.Value;
            continue;
          default:
            continue;
        }
      }
    }
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current.CanAcceptTags(controllerId, zone, cardType))
          return current;
      }
    }
    return (Zone) null;
  }

  public T FindZoneOfType<T>(Player.Side side) where T : Zone
  {
    System.Type type = typeof (T);
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (((object) current).GetType() == type && current.m_Side == side)
          return (T) current;
      }
    }
    return (T) null;
  }

  public List<T> FindZonesOfType<T>() where T : Zone
  {
    return this.FindZonesOfType<T, T>();
  }

  public List<ReturnType> FindZonesOfType<ReturnType, ArgType>() where ReturnType : Zone where ArgType : Zone
  {
    List<ReturnType> returnTypeList = new List<ReturnType>();
    System.Type type = typeof (ArgType);
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (((object) current).GetType() == type)
          returnTypeList.Add((ReturnType) current);
      }
    }
    return returnTypeList;
  }

  public List<T> FindZonesOfType<T>(Player.Side side) where T : Zone
  {
    return this.FindZonesOfType<T, T>(side);
  }

  public List<ReturnType> FindZonesOfType<ReturnType, ArgType>(Player.Side side) where ReturnType : Zone where ArgType : Zone
  {
    List<ReturnType> returnTypeList = new List<ReturnType>();
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current is ArgType && current.m_Side == side)
          returnTypeList.Add((ReturnType) current);
      }
    }
    return returnTypeList;
  }

  public List<Zone> FindZonesForTag(TAG_ZONE zoneTag)
  {
    List<Zone> zoneList = new List<Zone>();
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current.m_ServerTag == zoneTag)
          zoneList.Add(current);
      }
    }
    return zoneList;
  }

  public Map<System.Type, string> GetTweenNames()
  {
    return this.m_tweenNames;
  }

  public string GetTweenName<T>() where T : Zone
  {
    System.Type key = typeof (T);
    string empty = string.Empty;
    this.m_tweenNames.TryGetValue(key, out empty);
    return empty;
  }

  public void RequestNextDeathBlockLayoutDelaySec(float sec)
  {
    this.m_nextDeathBlockLayoutDelaySec = Mathf.Max(this.m_nextDeathBlockLayoutDelaySec, sec);
  }

  public float RemoveNextDeathBlockLayoutDelaySec()
  {
    float blockLayoutDelaySec = this.m_nextDeathBlockLayoutDelaySec;
    this.m_nextDeathBlockLayoutDelaySec = 0.0f;
    return blockLayoutDelaySec;
  }

  public int PredictZonePosition(Zone zone, int pos)
  {
    ZoneMgr.TempZone tempZone = this.BuildTempZone(zone);
    this.PredictZoneFromPowerProcessor(tempZone);
    int insertionPosition = this.FindBestInsertionPosition(tempZone, pos - 1, pos);
    this.m_tempZoneMap.Clear();
    this.m_tempEntityMap.Clear();
    return insertionPosition;
  }

  public bool HasPredictedCards()
  {
    return this.HasPredictedPositions() || this.HasPredictedWeapons() || this.HasPredictedSecrets();
  }

  public bool HasPredictedPositions()
  {
    using (List<Zone>.Enumerator enumerator1 = this.FindZonesOfType<Zone>(Player.Side.FRIENDLY).GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Card>.Enumerator enumerator2 = enumerator1.Current.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            if (enumerator2.Current.GetPredictedZonePosition() != 0)
              return true;
          }
        }
      }
    }
    return false;
  }

  public bool HasPredictedWeapons()
  {
    using (List<ZoneWeapon>.Enumerator enumerator1 = this.FindZonesOfType<ZoneWeapon>(Player.Side.FRIENDLY).GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Card>.Enumerator enumerator2 = enumerator1.Current.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            if (enumerator2.Current.GetEntity().GetZone() != TAG_ZONE.PLAY)
              return true;
          }
        }
      }
    }
    return false;
  }

  public bool HasPredictedSecrets()
  {
    using (List<ZoneSecret>.Enumerator enumerator1 = this.FindZonesOfType<ZoneSecret>(Player.Side.FRIENDLY).GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Card>.Enumerator enumerator2 = enumerator1.Current.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            if (enumerator2.Current.GetEntity().GetZone() != TAG_ZONE.SECRET)
              return true;
          }
        }
      }
    }
    return false;
  }

  public bool HasActiveLocalChange()
  {
    return this.m_activeLocalChangeLists.Count > 0;
  }

  public bool HasPendingLocalChange()
  {
    return this.m_pendingLocalChangeLists.Count > 0;
  }

  public bool HasTriggeredActiveLocalChange(Entity entity)
  {
    return this.HasTriggeredActiveLocalChange(entity.GetCard());
  }

  public bool HasTriggeredActiveLocalChange(Card card)
  {
    return this.FindTriggeredActiveLocalChangeIndex(card) >= 0;
  }

  public ZoneChangeList FindTriggeredActiveLocalChange(Entity entity)
  {
    return this.FindTriggeredActiveLocalChange(entity.GetCard());
  }

  public ZoneChangeList FindTriggeredActiveLocalChange(Card card)
  {
    int localChangeIndex = this.FindTriggeredActiveLocalChangeIndex(card);
    if (localChangeIndex < 0)
      return (ZoneChangeList) null;
    return this.m_pendingLocalChangeLists[localChangeIndex];
  }

  public bool HasTriggeredPendingLocalChange(Entity entity)
  {
    return this.HasTriggeredPendingLocalChange(entity.GetCard());
  }

  public bool HasTriggeredPendingLocalChange(Card card)
  {
    return this.FindTriggeredPendingLocalChangeIndex(card) >= 0;
  }

  public ZoneChangeList FindTriggeredPendingLocalChange(Entity entity)
  {
    return this.FindTriggeredPendingLocalChange(entity.GetCard());
  }

  public ZoneChangeList FindTriggeredPendingLocalChange(Card card)
  {
    int localChangeIndex = this.FindTriggeredPendingLocalChangeIndex(card);
    if (localChangeIndex < 0)
      return (ZoneChangeList) null;
    return this.m_pendingLocalChangeLists[localChangeIndex];
  }

  public bool IsChangeInLocalHistory(ZoneChangeList changeList)
  {
    return this.m_localChangeListHistory.GetList().Contains(changeList);
  }

  public ZoneChangeList AddLocalZoneChange(Card triggerCard, TAG_ZONE zoneTag)
  {
    Zone entityAndZoneTag = this.FindZoneForEntityAndZoneTag(triggerCard.GetEntity(), zoneTag);
    return this.AddLocalZoneChange(triggerCard, entityAndZoneTag, zoneTag, 0, (ZoneMgr.ChangeCompleteCallback) null, (object) null);
  }

  public ZoneChangeList AddLocalZoneChange(Card triggerCard, TAG_ZONE zoneTag, int destinationPos)
  {
    Zone entityAndZoneTag = this.FindZoneForEntityAndZoneTag(triggerCard.GetEntity(), zoneTag);
    return this.AddLocalZoneChange(triggerCard, entityAndZoneTag, zoneTag, destinationPos, (ZoneMgr.ChangeCompleteCallback) null, (object) null);
  }

  public ZoneChangeList AddLocalZoneChange(Card triggerCard, TAG_ZONE zoneTag, int destinationPos, ZoneMgr.ChangeCompleteCallback callback, object userData)
  {
    Zone entityAndZoneTag = this.FindZoneForEntityAndZoneTag(triggerCard.GetEntity(), zoneTag);
    return this.AddLocalZoneChange(triggerCard, entityAndZoneTag, zoneTag, destinationPos, callback, userData);
  }

  public ZoneChangeList AddLocalZoneChange(Card triggerCard, Zone destinationZone, int destinationPos)
  {
    if (!((UnityEngine.Object) destinationZone == (UnityEngine.Object) null))
      return this.AddLocalZoneChange(triggerCard, destinationZone, destinationZone.m_ServerTag, destinationPos, (ZoneMgr.ChangeCompleteCallback) null, (object) null);
    Debug.LogWarning((object) string.Format("ZoneMgr.AddLocalZoneChange() - illegal zone change to null zone for card {0}", (object) triggerCard));
    return (ZoneChangeList) null;
  }

  public ZoneChangeList AddLocalZoneChange(Card triggerCard, Zone destinationZone, int destinationPos, ZoneMgr.ChangeCompleteCallback callback, object userData)
  {
    if (!((UnityEngine.Object) destinationZone == (UnityEngine.Object) null))
      return this.AddLocalZoneChange(triggerCard, destinationZone, destinationZone.m_ServerTag, destinationPos, callback, userData);
    Debug.LogWarning((object) string.Format("ZoneMgr.AddLocalZoneChange() - illegal zone change to null zone for card {0}", (object) triggerCard));
    return (ZoneChangeList) null;
  }

  public ZoneChangeList AddLocalZoneChange(Card triggerCard, Zone destinationZone, TAG_ZONE destinationZoneTag, int destinationPos, ZoneMgr.ChangeCompleteCallback callback, object userData)
  {
    if (destinationZoneTag == TAG_ZONE.INVALID)
    {
      Debug.LogWarning((object) string.Format("ZoneMgr.AddLocalZoneChange() - illegal zone change to {0} for card {1}", (object) destinationZoneTag, (object) triggerCard));
      return (ZoneChangeList) null;
    }
    if ((destinationZone is ZonePlay || destinationZone is ZoneHand) && destinationPos <= 0)
    {
      Debug.LogWarning((object) string.Format("ZoneMgr.AddLocalZoneChange() - destinationPos {0} is too small for zone {1}, min is 1", (object) destinationPos, (object) destinationZone));
      return (ZoneChangeList) null;
    }
    ZoneChangeList localChangeList = this.CreateLocalChangeList(triggerCard, destinationZone, destinationZoneTag, destinationPos, callback, userData);
    this.ProcessOrEnqueueLocalChangeList(localChangeList);
    this.m_localChangeListHistory.Enqueue(localChangeList);
    return localChangeList;
  }

  public ZoneChangeList AddPredictedLocalZoneChange(Card triggerCard, Zone destinationZone, int destinationPos, int predictedPos)
  {
    if ((UnityEngine.Object) triggerCard == (UnityEngine.Object) null)
    {
      Debug.LogWarning((object) string.Format("ZoneMgr.AddPredictedLocalZoneChange() - triggerCard is null"));
      return (ZoneChangeList) null;
    }
    ZoneChangeList zoneChangeList = this.AddLocalZoneChange(triggerCard, destinationZone, destinationPos);
    if (zoneChangeList == null)
      return (ZoneChangeList) null;
    triggerCard.SetPredictedZonePosition(predictedPos);
    zoneChangeList.SetPredictedPosition(predictedPos);
    return zoneChangeList;
  }

  public ZoneChangeList CancelLocalZoneChange(ZoneChangeList changeList, ZoneMgr.ChangeCompleteCallback callback = null, object userData = null)
  {
    if (changeList == null)
    {
      Debug.LogWarning((object) string.Format("ZoneMgr.CancelLocalZoneChange() - changeList is null"));
      return (ZoneChangeList) null;
    }
    if (!this.m_localChangeListHistory.Remove(changeList))
    {
      Debug.LogWarning((object) string.Format("ZoneMgr.CancelLocalZoneChange() - changeList {0} is not in history", (object) changeList.GetId()));
      return (ZoneChangeList) null;
    }
    ZoneChange localTriggerChange = changeList.GetLocalTriggerChange();
    Card card = localTriggerChange.GetEntity().GetCard();
    Zone sourceZone = localTriggerChange.GetSourceZone();
    int sourcePosition = localTriggerChange.GetSourcePosition();
    ZoneChangeList localChangeList = this.CreateLocalChangeList(card, sourceZone, sourceZone.m_ServerTag, sourcePosition, callback, userData);
    localChangeList.SetCanceledChangeList(true);
    localChangeList.SetZoneInputBlocking(true);
    this.ProcessOrEnqueueLocalChangeList(localChangeList);
    return localChangeList;
  }

  public static bool IsHandledPower(Network.PowerHistory power)
  {
    switch (power.Type)
    {
      case Network.PowerType.FULL_ENTITY:
        Network.HistFullEntity histFullEntity = power as Network.HistFullEntity;
        bool flag = false;
        using (List<Network.Entity.Tag>.Enumerator enumerator = histFullEntity.Entity.Tags.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Network.Entity.Tag current = enumerator.Current;
            if (current.Name == 202)
            {
              if (current.Value == 1 || current.Value == 2)
                return false;
            }
            else if (current.Name == 49 || current.Name == 263 || current.Name == 50)
              flag = true;
          }
        }
        return flag;
      case Network.PowerType.SHOW_ENTITY:
        return true;
      case Network.PowerType.HIDE_ENTITY:
        return true;
      case Network.PowerType.TAG_CHANGE:
        Network.HistTagChange histTagChange = power as Network.HistTagChange;
        if (histTagChange.Tag != 49 && histTagChange.Tag != 263 && histTagChange.Tag != 50)
          return false;
        Entity entity = GameState.Get().GetEntity(histTagChange.Entity);
        return entity == null || !entity.IsPlayer() && !entity.IsGame();
      default:
        return false;
    }
  }

  public bool HasActiveServerChange()
  {
    return this.m_activeServerChangeList != null;
  }

  public bool HasPendingServerChange()
  {
    return this.m_pendingServerChangeLists.Count > 0;
  }

  public ZoneChangeList AddServerZoneChanges(PowerTaskList taskList, int taskStartIndex, int taskEndIndex, ZoneMgr.ChangeCompleteCallback callback, object userData)
  {
    int serverChangeListId = this.GetNextServerChangeListId();
    ZoneChangeList parentList = new ZoneChangeList();
    parentList.SetId(serverChangeListId);
    parentList.SetTaskList(taskList);
    parentList.SetCompleteCallback(callback);
    parentList.SetCompleteCallbackUserData(userData);
    Log.Zone.Print("ZoneMgr.AddServerZoneChanges() - taskListId={0} changeListId={1} taskStart={2} taskEnd={3}", (object) taskList.GetId(), (object) serverChangeListId, (object) taskStartIndex, (object) taskEndIndex);
    List<PowerTask> taskList1 = taskList.GetTaskList();
    for (int index = taskStartIndex; index <= taskEndIndex; ++index)
    {
      PowerTask powerTask = taskList1[index];
      Network.PowerHistory power = powerTask.GetPower();
      Network.PowerType type = power.Type;
      ZoneChange change;
      switch (type)
      {
        case Network.PowerType.FULL_ENTITY:
          change = this.CreateZoneChangeFromFullEntity((Network.HistFullEntity) power);
          break;
        case Network.PowerType.SHOW_ENTITY:
          change = this.CreateZoneChangeFromEntity(((Network.HistShowEntity) power).Entity);
          break;
        case Network.PowerType.HIDE_ENTITY:
          change = this.CreateZoneChangeFromHideEntity((Network.HistHideEntity) power);
          break;
        case Network.PowerType.TAG_CHANGE:
          change = this.CreateZoneChangeFromTagChange((Network.HistTagChange) power);
          break;
        case Network.PowerType.CREATE_GAME:
          change = this.CreateZoneChangeFromCreateGame((Network.HistCreateGame) power);
          break;
        case Network.PowerType.META_DATA:
          change = this.CreateZoneChangeFromMetaData((Network.HistMetaData) power);
          break;
        case Network.PowerType.CHANGE_ENTITY:
          change = this.CreateZoneChangeFromEntity(((Network.HistChangeEntity) power).Entity);
          break;
        default:
          Debug.LogError((object) string.Format("ZoneMgr.AddServerZoneChanges() - id={0} received unhandled power of type {1}", (object) parentList.GetId(), (object) type));
          return (ZoneChangeList) null;
      }
      if (change != null)
      {
        change.SetParentList(parentList);
        change.SetPowerTask(powerTask);
        parentList.AddChange(change);
      }
    }
    this.m_tempEntityMap.Clear();
    this.m_pendingServerChangeLists.Enqueue(parentList);
    return parentList;
  }

  private void UpdateLocalChangeLists()
  {
    List<ZoneChangeList> zoneChangeListList = (List<ZoneChangeList>) null;
    int index1 = 0;
    while (index1 < this.m_activeLocalChangeLists.Count)
    {
      ZoneChangeList activeLocalChangeList = this.m_activeLocalChangeLists[index1];
      if (!activeLocalChangeList.IsComplete())
      {
        ++index1;
      }
      else
      {
        activeLocalChangeList.FireCompleteCallback();
        this.m_activeLocalChangeLists.RemoveAt(index1);
        if (zoneChangeListList == null)
          zoneChangeListList = new List<ZoneChangeList>();
        zoneChangeListList.Add(activeLocalChangeList);
      }
    }
    if (zoneChangeListList == null)
      return;
    bool flag = false;
    for (int index2 = 0; index2 < zoneChangeListList.Count; ++index2)
    {
      ZoneChangeList zoneChangeList = zoneChangeListList[index2];
      Entity entity = zoneChangeList.GetLocalTriggerChange().GetEntity();
      Card card = entity.GetCard();
      if (zoneChangeList.IsCanceledChangeList())
      {
        flag = true;
        card.SetPredictedZonePosition(0);
      }
      int localChangeIndex = this.FindTriggeredPendingLocalChangeIndex(entity);
      if (localChangeIndex >= 0)
      {
        ZoneChangeList pendingLocalChangeList = this.m_pendingLocalChangeLists[localChangeIndex];
        this.m_pendingLocalChangeLists.RemoveAt(localChangeIndex);
        this.CreateLocalChangesFromTrigger(pendingLocalChangeList, pendingLocalChangeList.GetLocalTriggerChange());
        this.ProcessLocalChangeList(pendingLocalChangeList);
      }
    }
    if (!flag)
      return;
    this.AutoCorrectZonesAfterLocalChange();
  }

  private void UpdateServerChangeLists()
  {
    if (this.m_activeServerChangeList != null && this.m_activeServerChangeList.IsComplete())
    {
      this.m_activeServerChangeList.FireCompleteCallback();
      this.m_activeServerChangeList = (ZoneChangeList) null;
      this.AutoCorrectZonesAfterServerChange();
    }
    if (!this.HasPendingServerChange() || this.HasActiveServerChange())
      return;
    this.m_activeServerChangeList = this.m_pendingServerChangeLists.Dequeue();
    this.PostProcessServerChangeList(this.m_activeServerChangeList);
    this.StartCoroutine(this.m_activeServerChangeList.ProcessChanges());
  }

  private bool HasLocalChangeExitingZone(Entity entity, Zone zone)
  {
    return this.HasLocalChangeExitingZone(entity, zone, this.m_activeLocalChangeLists) || this.HasLocalChangeExitingZone(entity, zone, this.m_pendingLocalChangeLists);
  }

  private bool HasLocalChangeExitingZone(Entity entity, Zone zone, List<ZoneChangeList> changeLists)
  {
    TAG_ZONE serverTag = zone.m_ServerTag;
    using (List<ZoneChangeList>.Enumerator enumerator1 = changeLists.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<ZoneChange>.Enumerator enumerator2 = enumerator1.Current.GetChanges().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            ZoneChange current = enumerator2.Current;
            if (entity == current.GetEntity() && serverTag == current.GetSourceZoneTag() && serverTag != current.GetDestinationZoneTag())
              return true;
          }
        }
      }
    }
    return false;
  }

  private void PredictZoneFromPowerProcessor(ZoneMgr.TempZone tempZone)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ZoneMgr.\u003CPredictZoneFromPowerProcessor\u003Ec__AnonStorey3BF processorCAnonStorey3Bf = new ZoneMgr.\u003CPredictZoneFromPowerProcessor\u003Ec__AnonStorey3BF();
    // ISSUE: reference to a compiler-generated field
    processorCAnonStorey3Bf.tempZone = tempZone;
    // ISSUE: reference to a compiler-generated field
    processorCAnonStorey3Bf.\u003C\u003Ef__this = this;
    PowerProcessor powerProcessor = GameState.Get().GetPowerProcessor();
    // ISSUE: reference to a compiler-generated field
    processorCAnonStorey3Bf.tempZone.PreprocessChanges();
    // ISSUE: reference to a compiler-generated method
    powerProcessor.ForEachTaskList(new Action<int, PowerTaskList>(processorCAnonStorey3Bf.\u003C\u003Em__159));
    // ISSUE: reference to a compiler-generated field
    processorCAnonStorey3Bf.tempZone.Sort();
    // ISSUE: reference to a compiler-generated field
    processorCAnonStorey3Bf.tempZone.PostprocessChanges();
  }

  private void PredictZoneFromPowerTaskList(ZoneMgr.TempZone tempZone, PowerTaskList taskList)
  {
    List<PowerTask> taskList1 = taskList.GetTaskList();
    for (int index = 0; index < taskList1.Count; ++index)
    {
      Network.PowerHistory power = taskList1[index].GetPower();
      this.PredictZoneFromPower(tempZone, power);
    }
  }

  private void PredictZoneFromPower(ZoneMgr.TempZone tempZone, Network.PowerHistory power)
  {
    switch (power.Type)
    {
      case Network.PowerType.FULL_ENTITY:
        this.PredictZoneFromFullEntity(tempZone, (Network.HistFullEntity) power);
        break;
      case Network.PowerType.SHOW_ENTITY:
        this.PredictZoneFromShowEntity(tempZone, (Network.HistShowEntity) power);
        break;
      case Network.PowerType.HIDE_ENTITY:
        this.PredictZoneFromHideEntity(tempZone, (Network.HistHideEntity) power);
        break;
      case Network.PowerType.TAG_CHANGE:
        this.PredictZoneFromTagChange(tempZone, (Network.HistTagChange) power);
        break;
    }
  }

  private void PredictZoneFromFullEntity(ZoneMgr.TempZone tempZone, Network.HistFullEntity fullEntity)
  {
    Entity entity = this.RegisterTempEntity(fullEntity.Entity);
    Zone zone = tempZone.GetZone();
    if (entity.GetZone() != zone.m_ServerTag || entity.GetControllerId() != zone.GetControllerId())
      return;
    tempZone.AddEntity(entity);
  }

  private void PredictZoneFromShowEntity(ZoneMgr.TempZone tempZone, Network.HistShowEntity showEntity)
  {
    Entity tempEntity = this.RegisterTempEntity(showEntity.Entity);
    using (List<Network.Entity.Tag>.Enumerator enumerator = showEntity.Entity.Tags.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        this.PredictZoneByApplyingTag(tempZone, tempEntity, (GAME_TAG) current.Name, current.Value);
      }
    }
  }

  private void PredictZoneFromHideEntity(ZoneMgr.TempZone tempZone, Network.HistHideEntity hideEntity)
  {
    Entity tempEntity = this.RegisterTempEntity(hideEntity.Entity);
    this.PredictZoneByApplyingTag(tempZone, tempEntity, GAME_TAG.ZONE, hideEntity.Zone);
  }

  private void PredictZoneFromTagChange(ZoneMgr.TempZone tempZone, Network.HistTagChange tagChange)
  {
    Entity tempEntity = this.RegisterTempEntity(tagChange.Entity);
    this.PredictZoneByApplyingTag(tempZone, tempEntity, (GAME_TAG) tagChange.Tag, tagChange.Value);
  }

  private void PredictZoneByApplyingTag(ZoneMgr.TempZone tempZone, Entity tempEntity, GAME_TAG tag, int val)
  {
    if (tag != GAME_TAG.ZONE && tag != GAME_TAG.CONTROLLER)
    {
      tempEntity.SetTag(tag, val);
    }
    else
    {
      Zone zone = tempZone.GetZone();
      if (tempEntity.GetZone() == zone.m_ServerTag && tempEntity.GetControllerId() == zone.GetControllerId())
        tempZone.RemoveEntity(tempEntity);
      tempEntity.SetTag(tag, val);
      if (tempEntity.GetZone() != zone.m_ServerTag || tempEntity.GetControllerId() != zone.GetControllerId())
        return;
      tempZone.AddEntity(tempEntity);
    }
  }

  private ZoneChangeList CreateLocalChangeList(Card triggerCard, Zone destinationZone, TAG_ZONE destinationZoneTag, int destinationPos, ZoneMgr.ChangeCompleteCallback callback, object userData)
  {
    int localChangeListId = this.GetNextLocalChangeListId();
    Log.Zone.Print("ZoneMgr.CreateLocalChangeList() - changeListId={0}", (object) localChangeListId);
    ZoneChangeList parentList = new ZoneChangeList();
    parentList.SetId(localChangeListId);
    parentList.SetCompleteCallback(callback);
    parentList.SetCompleteCallbackUserData(userData);
    Entity entity = triggerCard.GetEntity();
    Zone zone = triggerCard.GetZone();
    TAG_ZONE tag = !((UnityEngine.Object) zone == (UnityEngine.Object) null) ? zone.m_ServerTag : TAG_ZONE.INVALID;
    int zonePosition = triggerCard.GetZonePosition();
    ZoneChange change = new ZoneChange();
    change.SetParentList(parentList);
    change.SetEntity(entity);
    change.SetSourceZone(zone);
    change.SetSourceZoneTag(tag);
    change.SetSourcePosition(zonePosition);
    change.SetDestinationZone(destinationZone);
    change.SetDestinationZoneTag(destinationZoneTag);
    change.SetDestinationPosition(destinationPos);
    parentList.AddChange(change);
    return parentList;
  }

  private void ProcessOrEnqueueLocalChangeList(ZoneChangeList changeList)
  {
    ZoneChange localTriggerChange = changeList.GetLocalTriggerChange();
    if (this.HasTriggeredActiveLocalChange(localTriggerChange.GetEntity().GetCard()))
    {
      this.m_pendingLocalChangeLists.Add(changeList);
    }
    else
    {
      this.CreateLocalChangesFromTrigger(changeList, localTriggerChange);
      this.ProcessLocalChangeList(changeList);
    }
  }

  private void CreateLocalChangesFromTrigger(ZoneChangeList changeList, ZoneChange triggerChange)
  {
    Log.Zone.Print("ZoneMgr.CreateLocalChangesFromTrigger() - {0}", (object) changeList);
    Entity entity = triggerChange.GetEntity();
    Zone sourceZone = triggerChange.GetSourceZone();
    TAG_ZONE sourceZoneTag = triggerChange.GetSourceZoneTag();
    int sourcePosition = triggerChange.GetSourcePosition();
    Zone destinationZone = triggerChange.GetDestinationZone();
    TAG_ZONE destinationZoneTag = triggerChange.GetDestinationZoneTag();
    int destinationPosition = triggerChange.GetDestinationPosition();
    if (sourceZoneTag != destinationZoneTag)
    {
      this.CreateLocalChangesFromTrigger(changeList, entity, sourceZone, sourceZoneTag, sourcePosition, destinationZone, destinationZoneTag, destinationPosition);
    }
    else
    {
      if (sourcePosition == destinationPosition)
        return;
      this.CreateLocalPosOnlyChangesFromTrigger(changeList, entity, sourceZone, sourcePosition, destinationPosition);
    }
  }

  private void CreateLocalChangesFromTrigger(ZoneChangeList changeList, Entity triggerEntity, Zone sourceZone, TAG_ZONE sourceZoneTag, int sourcePos, Zone destinationZone, TAG_ZONE destinationZoneTag, int destinationPos)
  {
    Log.Zone.Print("ZoneMgr.CreateLocalChangesFromTrigger() - triggerEntity={0} srcZone={1} srcPos={2} dstZone={3} dstPos={4}", (object) triggerEntity, (object) sourceZoneTag, (object) sourcePos, (object) destinationZoneTag, (object) destinationPos);
    if (sourcePos != destinationPos)
      Log.Zone.Print("ZoneMgr.CreateLocalChangesFromTrigger() - srcPos={0} destPos={1}", new object[2]
      {
        (object) sourcePos,
        (object) destinationPos
      });
    if ((UnityEngine.Object) sourceZone != (UnityEngine.Object) null)
    {
      List<Card> cards = sourceZone.GetCards();
      for (int index = sourcePos; index < cards.Count; ++index)
      {
        Card card = cards[index];
        Entity entity = card.GetEntity();
        ZoneChange change = new ZoneChange();
        change.SetParentList(changeList);
        change.SetEntity(entity);
        int pos = index;
        change.SetSourcePosition(card.GetZonePosition());
        change.SetDestinationPosition(pos);
        Log.Zone.Print("ZoneMgr.CreateLocalChangesFromTrigger() - srcZone card {0} zonePos {1} -> {2}", new object[3]
        {
          (object) card,
          (object) card.GetZonePosition(),
          (object) pos
        });
        changeList.AddChange(change);
      }
    }
    if (!((UnityEngine.Object) destinationZone != (UnityEngine.Object) null) || destinationZone is ZoneSecret)
      return;
    if (destinationZone is ZoneWeapon)
    {
      List<Card> cards = destinationZone.GetCards();
      if (cards.Count <= 0)
        return;
      Entity entity = cards[0].GetEntity();
      ZoneChange change = new ZoneChange();
      change.SetParentList(changeList);
      change.SetEntity(entity);
      change.SetDestinationZone((Zone) this.FindZoneOfType<ZoneGraveyard>(destinationZone.m_Side));
      change.SetDestinationZoneTag(TAG_ZONE.GRAVEYARD);
      changeList.AddChange(change);
    }
    else if (destinationZone is ZonePlay || destinationZone is ZoneHand)
    {
      List<Card> cards = destinationZone.GetCards();
      for (int index = destinationPos - 1; index < cards.Count; ++index)
      {
        Card card = cards[index];
        Entity entity = card.GetEntity();
        int pos = index + 2;
        ZoneChange change = new ZoneChange();
        change.SetParentList(changeList);
        change.SetEntity(entity);
        change.SetDestinationPosition(pos);
        Log.Zone.Print("ZoneMgr.CreateLocalChangesFromTrigger() - dstZone card {0} zonePos {1} -> {2}", new object[3]
        {
          (object) card,
          (object) entity.GetZonePosition(),
          (object) pos
        });
        changeList.AddChange(change);
      }
    }
    else
      Debug.LogError((object) string.Format("ZoneMgr.CreateLocalChangesFromTrigger() - don't know how to predict zone position changes for zone {0}", (object) destinationZone));
  }

  private void CreateLocalPosOnlyChangesFromTrigger(ZoneChangeList changeList, Entity triggerEntity, Zone sourceZone, int sourcePos, int destinationPos)
  {
    List<Card> cards = sourceZone.GetCards();
    if (sourcePos < destinationPos)
    {
      for (int index = sourcePos; index < destinationPos && index < cards.Count; ++index)
      {
        Card card = cards[index];
        Entity entity = card.GetEntity();
        ZoneChange change = new ZoneChange();
        change.SetParentList(changeList);
        change.SetEntity(entity);
        int pos = index;
        change.SetSourcePosition(card.GetZonePosition());
        change.SetDestinationPosition(pos);
        changeList.AddChange(change);
      }
    }
    else
    {
      for (int index = destinationPos - 1; index < sourcePos - 1; ++index)
      {
        Card card = cards[index];
        Entity entity = card.GetEntity();
        ZoneChange change = new ZoneChange();
        change.SetParentList(changeList);
        change.SetEntity(entity);
        int pos = index + 2;
        change.SetSourcePosition(card.GetZonePosition());
        change.SetDestinationPosition(pos);
        changeList.AddChange(change);
      }
    }
  }

  private void ProcessLocalChangeList(ZoneChangeList changeList)
  {
    Log.Zone.Print("ZoneMgr.ProcessLocalChangeList() - [{0}]", (object) changeList);
    this.m_activeLocalChangeLists.Add(changeList);
    this.StartCoroutine(changeList.ProcessChanges());
  }

  private void OnCurrentPlayerChanged(Player player, object userData)
  {
    if (!player.IsLocalUser())
      return;
    this.m_localChangeListHistory.Clear();
  }

  private void OnOptionRejected(Network.Options.Option option, object userData)
  {
    if (option.Type != Network.Options.Option.OptionType.POWER)
      return;
    Entity entity = GameState.Get().GetEntity(option.Main.ID);
    ZoneChangeList rejectedLocalZoneChange = this.FindRejectedLocalZoneChange(entity);
    if (rejectedLocalZoneChange == null)
    {
      Log.Zone.Print("ZoneMgr.RejectLocalZoneChange() - did not find a zone change to reject for {0}", (object) entity);
    }
    else
    {
      entity.GetCard().SetPredictedZonePosition(0);
      this.CancelLocalZoneChange(rejectedLocalZoneChange, (ZoneMgr.ChangeCompleteCallback) null, (object) null);
    }
  }

  private ZoneChangeList FindRejectedLocalZoneChange(Entity triggerEntity)
  {
    List<ZoneChangeList> list = this.m_localChangeListHistory.GetList();
    for (int index1 = 0; index1 < list.Count; ++index1)
    {
      ZoneChangeList zoneChangeList = list[index1];
      List<ZoneChange> changes = zoneChangeList.GetChanges();
      for (int index2 = 0; index2 < changes.Count; ++index2)
      {
        ZoneChange zoneChange = changes[index2];
        if (zoneChange.GetEntity() == triggerEntity && zoneChange.GetDestinationZoneTag() == TAG_ZONE.PLAY)
          return zoneChangeList;
      }
    }
    return (ZoneChangeList) null;
  }

  private ZoneChange CreateZoneChangeFromCreateGame(Network.HistCreateGame createGame)
  {
    ZoneChange zoneChange = new ZoneChange();
    zoneChange.SetEntity((Entity) GameState.Get().GetGameEntity());
    return zoneChange;
  }

  private ZoneChange CreateZoneChangeFromFullEntity(Network.HistFullEntity fullEntity)
  {
    Network.Entity entity1 = fullEntity.Entity;
    Entity entity2 = GameState.Get().GetEntity(entity1.ID);
    if (entity2 == null)
    {
      Debug.LogWarning((object) string.Format("ZoneMgr.CreateZoneChangeFromFullEntity() - WARNING entity {0} DOES NOT EXIST!", (object) entity1.ID));
      return (ZoneChange) null;
    }
    ZoneChange zoneChange = new ZoneChange();
    zoneChange.SetEntity(entity2);
    if ((UnityEngine.Object) entity2.GetCard() == (UnityEngine.Object) null)
      return zoneChange;
    bool flag1 = false;
    bool flag2 = false;
    bool flag3 = false;
    using (List<Network.Entity.Tag>.Enumerator enumerator = entity1.Tags.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        if (current.Name == 49)
        {
          zoneChange.SetDestinationZoneTag((TAG_ZONE) current.Value);
          flag1 = true;
          if (flag2)
          {
            if (flag3)
              break;
          }
        }
        else if (current.Name == 263)
        {
          zoneChange.SetDestinationPosition(current.Value);
          flag2 = true;
          if (flag1)
          {
            if (flag3)
              break;
          }
        }
        else if (current.Name == 50)
        {
          zoneChange.SetDestinationControllerId(current.Value);
          flag3 = true;
          if (flag1)
          {
            if (flag2)
              break;
          }
        }
      }
    }
    if (flag1 || flag3)
      zoneChange.SetDestinationZone(this.FindZoneForEntity(entity2));
    return zoneChange;
  }

  private ZoneChange CreateZoneChangeFromEntity(Network.Entity netEnt)
  {
    Entity entity1 = GameState.Get().GetEntity(netEnt.ID);
    if (entity1 == null)
    {
      Debug.LogWarning((object) string.Format("ZoneMgr.CreateZoneChangeFromEntity() - WARNING entity {0} DOES NOT EXIST!", (object) netEnt.ID));
      return (ZoneChange) null;
    }
    ZoneChange zoneChange = new ZoneChange();
    zoneChange.SetEntity(entity1);
    if ((UnityEngine.Object) entity1.GetCard() == (UnityEngine.Object) null)
      return zoneChange;
    Entity entity2 = this.RegisterTempEntity(netEnt.ID, entity1);
    using (List<Network.Entity.Tag>.Enumerator enumerator = netEnt.Tags.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        entity2.SetTag(current.Name, current.Value);
      }
    }
    bool flag1 = false;
    bool flag2 = false;
    bool flag3 = false;
    using (List<Network.Entity.Tag>.Enumerator enumerator = netEnt.Tags.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        entity2.SetTag(current.Name, current.Value);
        if (current.Name == 49)
        {
          zoneChange.SetDestinationZoneTag((TAG_ZONE) current.Value);
          flag1 = true;
          if (flag2)
          {
            if (flag3)
              break;
          }
        }
        else if (current.Name == 263)
        {
          zoneChange.SetDestinationPosition(current.Value);
          flag2 = true;
          if (flag1)
          {
            if (flag3)
              break;
          }
        }
        else if (current.Name == 50)
        {
          zoneChange.SetDestinationControllerId(current.Value);
          flag3 = true;
          if (flag1)
          {
            if (flag2)
              break;
          }
        }
      }
    }
    if (flag1 || flag3)
      zoneChange.SetDestinationZone(this.FindZoneForEntity(entity2));
    return zoneChange;
  }

  private ZoneChange CreateZoneChangeFromHideEntity(Network.HistHideEntity hideEntity)
  {
    Entity entity1 = GameState.Get().GetEntity(hideEntity.Entity);
    if (entity1 == null)
    {
      Debug.LogWarning((object) string.Format("ZoneMgr.CreateZoneChangeFromHideEntity() - WARNING entity {0} DOES NOT EXIST! zone={1}", (object) hideEntity.Entity, (object) hideEntity.Zone));
      return (ZoneChange) null;
    }
    ZoneChange zoneChange = new ZoneChange();
    zoneChange.SetEntity(entity1);
    if ((UnityEngine.Object) entity1.GetCard() == (UnityEngine.Object) null)
      return zoneChange;
    Entity entity2 = this.RegisterTempEntity(hideEntity.Entity, entity1);
    entity2.SetTag(GAME_TAG.ZONE, hideEntity.Zone);
    TAG_ZONE zone = (TAG_ZONE) hideEntity.Zone;
    zoneChange.SetDestinationZoneTag(zone);
    zoneChange.SetDestinationZone(this.FindZoneForEntity(entity2));
    return zoneChange;
  }

  private ZoneChange CreateZoneChangeFromTagChange(Network.HistTagChange tagChange)
  {
    Entity entity1 = GameState.Get().GetEntity(tagChange.Entity);
    if (entity1 == null)
    {
      Debug.LogError((object) string.Format("ZoneMgr.CreateZoneChangeFromTagChange() - Entity {0} does not exist", (object) tagChange.Entity));
      return (ZoneChange) null;
    }
    ZoneChange zoneChange = new ZoneChange();
    zoneChange.SetEntity(entity1);
    Card card = entity1.GetCard();
    if ((UnityEngine.Object) card == (UnityEngine.Object) null)
      return zoneChange;
    Entity entity2 = this.RegisterTempEntity(tagChange.Entity, entity1);
    entity2.SetTag(tagChange.Tag, tagChange.Value);
    if (tagChange.Tag == 49)
    {
      if ((UnityEngine.Object) card == (UnityEngine.Object) null)
      {
        Debug.LogError((object) string.Format("ZoneMgr.CreateZoneChangeFromTagChange() - {0} does not have a card visual", (object) entity1));
        return (ZoneChange) null;
      }
      TAG_ZONE tag = (TAG_ZONE) tagChange.Value;
      zoneChange.SetDestinationZoneTag(tag);
      zoneChange.SetDestinationZone(this.FindZoneForEntity(entity2));
    }
    else if (tagChange.Tag == 263)
    {
      if ((UnityEngine.Object) card == (UnityEngine.Object) null)
      {
        Debug.LogError((object) string.Format("ZoneMgr.CreateZoneChangeFromTagChange() - {0} does not have a card visual", (object) entity1));
        return (ZoneChange) null;
      }
      zoneChange.SetDestinationPosition(tagChange.Value);
    }
    else if (tagChange.Tag == 50)
    {
      if ((UnityEngine.Object) card == (UnityEngine.Object) null)
      {
        Debug.LogError((object) string.Format("ZoneMgr.CreateZoneChangeFromTagChange() - {0} does not have a card visual", (object) entity1));
        return (ZoneChange) null;
      }
      int controllerId = tagChange.Value;
      zoneChange.SetDestinationControllerId(controllerId);
      zoneChange.SetDestinationZone(this.FindZoneForEntity(entity2));
    }
    return zoneChange;
  }

  private ZoneChange CreateZoneChangeFromMetaData(Network.HistMetaData metaData)
  {
    if (metaData.Info.Count <= 0)
      return (ZoneChange) null;
    Entity entity = GameState.Get().GetEntity(metaData.Info[0]);
    if (entity == null)
    {
      Debug.LogError((object) string.Format("ZoneMgr.CreateZoneChangeFromMetaData() - Entity {0} does not exist", (object) metaData.Info[0]));
      return (ZoneChange) null;
    }
    ZoneChange zoneChange = new ZoneChange();
    zoneChange.SetEntity(entity);
    return zoneChange;
  }

  private Entity RegisterTempEntity(int id)
  {
    Entity entity = GameState.Get().GetEntity(id);
    return this.RegisterTempEntity(id, entity);
  }

  private Entity RegisterTempEntity(Network.Entity netEnt)
  {
    Entity entity = GameState.Get().GetEntity(netEnt.ID);
    return this.RegisterTempEntity(netEnt.ID, entity);
  }

  private Entity RegisterTempEntity(Entity entity)
  {
    return this.RegisterTempEntity(entity.GetEntityId(), entity);
  }

  private Entity RegisterTempEntity(int id, Entity entity)
  {
    Entity entity1;
    if (!this.m_tempEntityMap.TryGetValue(id, out entity1))
    {
      entity1 = entity.CloneForZoneMgr();
      this.m_tempEntityMap.Add(id, entity1);
    }
    return entity1;
  }

  private void PostProcessServerChangeList(ZoneChangeList serverChangeList)
  {
    if (!this.ShouldPostProcessServerChangeList(serverChangeList) || this.CheckAndIgnoreServerChangeList(serverChangeList) || this.ReplaceRemoteWeaponInServerChangeList(serverChangeList))
      return;
    this.MergeServerChangeList(serverChangeList);
  }

  private bool ShouldPostProcessServerChangeList(ZoneChangeList changeList)
  {
    List<ZoneChange> changes = changeList.GetChanges();
    for (int index = 0; index < changes.Count; ++index)
    {
      if (changes[index].HasDestinationData())
        return true;
    }
    return false;
  }

  private bool CheckAndIgnoreServerChangeList(ZoneChangeList serverChangeList)
  {
    Network.HistBlockStart blockStart = serverChangeList.GetTaskList().GetBlockStart();
    if (blockStart == null || blockStart.BlockType != HistoryBlock.Type.PLAY)
      return false;
    ZoneChangeList serverChangeList1 = this.FindLocalChangeListMatchingServerChangeList(serverChangeList);
    if (serverChangeList1 == null)
      return false;
    serverChangeList.SetIgnoreCardZoneChanges(true);
    while (this.m_localChangeListHistory.Count > 0)
    {
      ZoneChangeList zoneChangeList = this.m_localChangeListHistory.Dequeue();
      if (serverChangeList1 == zoneChangeList)
      {
        serverChangeList1.GetLocalTriggerCard().SetPredictedZonePosition(0);
        break;
      }
    }
    return true;
  }

  private ZoneChangeList FindLocalChangeListMatchingServerChangeList(ZoneChangeList serverChangeList)
  {
    foreach (ZoneChangeList zoneChangeList in this.m_localChangeListHistory)
    {
      int predictedPosition = zoneChangeList.GetPredictedPosition();
      using (List<ZoneChange>.Enumerator enumerator = zoneChangeList.GetChanges().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ZoneChange current = enumerator.Current;
          Entity entity1 = current.GetEntity();
          TAG_ZONE destinationZoneTag1 = current.GetDestinationZoneTag();
          if (destinationZoneTag1 != TAG_ZONE.INVALID)
          {
            List<ZoneChange> changes = serverChangeList.GetChanges();
            for (int index = 0; index < changes.Count; ++index)
            {
              ZoneChange zoneChange = changes[index];
              Entity entity2 = zoneChange.GetEntity();
              if (entity1 == entity2)
              {
                TAG_ZONE destinationZoneTag2 = zoneChange.GetDestinationZoneTag();
                if (destinationZoneTag1 == destinationZoneTag2)
                {
                  ZoneChange nextDstPosChange = this.FindNextDstPosChange(serverChangeList, index, entity2);
                  int num = nextDstPosChange != null ? nextDstPosChange.GetDestinationPosition() : entity2.GetZonePosition();
                  if (predictedPosition == num)
                    return zoneChangeList;
                }
              }
            }
          }
        }
      }
    }
    return (ZoneChangeList) null;
  }

  private ZoneChange FindNextDstPosChange(ZoneChangeList changeList, int index, Entity entity)
  {
    List<ZoneChange> changes = changeList.GetChanges();
    for (int index1 = index; index1 < changes.Count; ++index1)
    {
      ZoneChange zoneChange = changes[index1];
      if (zoneChange.HasDestinationZoneChange() && index1 != index)
        return (ZoneChange) null;
      if (zoneChange.HasDestinationPosition())
      {
        if (zoneChange.GetEntity() != entity)
          return (ZoneChange) null;
        return zoneChange;
      }
    }
    return (ZoneChange) null;
  }

  private bool ReplaceRemoteWeaponInServerChangeList(ZoneChangeList serverChangeList)
  {
    ZoneChange zoneChange = serverChangeList.GetChanges().Find((Predicate<ZoneChange>) (change =>
    {
      Zone destinationZone = change.GetDestinationZone();
      return destinationZone is ZoneWeapon && !destinationZone.GetController().IsFriendlySide();
    }));
    if (zoneChange == null)
      return false;
    Zone destinationZone1 = zoneChange.GetDestinationZone();
    if (destinationZone1.GetCardCount() == 0)
      return false;
    Entity entity = destinationZone1.GetCardAtIndex(0).GetEntity();
    Zone zoneForTags = this.FindZoneForTags(entity.GetControllerId(), TAG_ZONE.GRAVEYARD, TAG_CARDTYPE.WEAPON);
    ZoneChange change1 = new ZoneChange();
    change1.SetEntity(entity);
    change1.SetDestinationZone(zoneForTags);
    change1.SetDestinationZoneTag(TAG_ZONE.GRAVEYARD);
    change1.SetDestinationPosition(0);
    serverChangeList.AddChange(change1);
    return true;
  }

  private bool MergeServerChangeList(ZoneChangeList serverChangeList)
  {
    using (List<Zone>.Enumerator enumerator = this.m_zones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (this.IsZoneInLocalHistory(current))
        {
          ZoneMgr.TempZone tempZone = this.BuildTempZone(current);
          this.m_tempZoneMap[current] = tempZone;
          tempZone.PreprocessChanges();
        }
      }
    }
    List<ZoneChange> changes = serverChangeList.GetChanges();
    for (int index = 0; index < changes.Count; ++index)
      this.TempApplyZoneChange(changes[index]);
    bool flag = false;
    using (Map<Zone, ZoneMgr.TempZone>.ValueCollection.Enumerator enumerator = this.m_tempZoneMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ZoneMgr.TempZone current = enumerator.Current;
        current.Sort();
        current.PostprocessChanges();
        Zone zone = current.GetZone();
        for (int pos = 1; pos < zone.GetLastPos(); ++pos)
        {
          Card cardAtPos = zone.GetCardAtPos(pos);
          Entity entity = cardAtPos.GetEntity();
          if (cardAtPos.GetPredictedZonePosition() != 0)
          {
            int insertionPosition = this.FindBestInsertionPosition(current, pos - 1, pos + 1);
            current.InsertEntityAtPos(insertionPosition, entity);
          }
        }
        if (current.IsModified())
        {
          flag = true;
          for (int pos = 1; pos < current.GetLastPos(); ++pos)
          {
            Entity entity = current.GetEntityAtPos(pos).GetCard().GetEntity();
            ZoneChange change = new ZoneChange();
            change.SetEntity(entity);
            change.SetDestinationZone(zone);
            change.SetDestinationZoneTag(zone.m_ServerTag);
            change.SetDestinationPosition(pos);
            serverChangeList.AddChange(change);
          }
        }
      }
    }
    this.m_tempZoneMap.Clear();
    this.m_tempEntityMap.Clear();
    return flag;
  }

  private bool IsZoneInLocalHistory(Zone zone)
  {
    foreach (ZoneChangeList zoneChangeList in this.m_localChangeListHistory)
    {
      using (List<ZoneChange>.Enumerator enumerator = zoneChangeList.GetChanges().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ZoneChange current = enumerator.Current;
          Zone sourceZone = current.GetSourceZone();
          Zone destinationZone = current.GetDestinationZone();
          if ((UnityEngine.Object) zone == (UnityEngine.Object) sourceZone || (UnityEngine.Object) zone == (UnityEngine.Object) destinationZone)
            return true;
        }
      }
    }
    return false;
  }

  private void TempApplyZoneChange(ZoneChange change)
  {
    Network.PowerHistory power = change.GetPowerTask().GetPower();
    Entity entity = this.RegisterTempEntity(change.GetEntity());
    if (!change.HasDestinationZoneChange())
    {
      GameUtils.ApplyPower(entity, power);
    }
    else
    {
      ZoneMgr.TempZone tempZoneForZone1 = this.FindTempZoneForZone(this.FindZoneForEntity(entity));
      if (tempZoneForZone1 != null)
        tempZoneForZone1.RemoveEntity(entity);
      GameUtils.ApplyPower(entity, power);
      ZoneMgr.TempZone tempZoneForZone2 = this.FindTempZoneForZone(change.GetDestinationZone());
      if (tempZoneForZone2 == null)
        return;
      tempZoneForZone2.AddEntity(entity);
    }
  }

  private ZoneMgr.TempZone BuildTempZone(Zone zone)
  {
    ZoneMgr.TempZone tempZone = new ZoneMgr.TempZone();
    tempZone.SetZone(zone);
    List<Card> cards = zone.GetCards();
    for (int index = 0; index < cards.Count; ++index)
    {
      Card card = cards[index];
      if (card.GetPredictedZonePosition() == 0)
      {
        Entity entity = this.RegisterTempEntity(card.GetEntity());
        tempZone.AddInitialEntity(entity);
      }
    }
    return tempZone;
  }

  private ZoneMgr.TempZone FindTempZoneForZone(Zone zone)
  {
    if ((UnityEngine.Object) zone == (UnityEngine.Object) null)
      return (ZoneMgr.TempZone) null;
    ZoneMgr.TempZone tempZone = (ZoneMgr.TempZone) null;
    this.m_tempZoneMap.TryGetValue(zone, out tempZone);
    return tempZone;
  }

  private int FindBestInsertionPosition(ZoneMgr.TempZone tempZone, int leftPos, int rightPos)
  {
    Zone zone = tempZone.GetZone();
    int pos1 = 0;
    for (int index = leftPos - 1; index >= 0; --index)
    {
      Entity entity = zone.GetCardAtIndex(index).GetEntity();
      pos1 = tempZone.FindEntityPosWithReplacements(entity.GetEntityId());
      if (pos1 != 0)
        break;
    }
    int pos2;
    if (pos1 == 0)
    {
      pos2 = 1;
    }
    else
    {
      int entityId = tempZone.GetEntityAtPos(pos1).GetEntityId();
      for (pos2 = pos1 + 1; pos2 < tempZone.GetLastPos(); ++pos2)
      {
        Entity entityAtPos = tempZone.GetEntityAtPos(pos2);
        if (entityAtPos.GetCreatorId() != entityId || zone.ContainsCard(entityAtPos.GetCard()))
          break;
      }
    }
    int pos3 = 0;
    for (int index = rightPos - 1; index < zone.GetCardCount(); ++index)
    {
      Entity entity = zone.GetCardAtIndex(index).GetEntity();
      pos3 = tempZone.FindEntityPosWithReplacements(entity.GetEntityId());
      if (pos3 != 0)
        break;
    }
    int num;
    if (pos3 == 0)
    {
      num = tempZone.GetLastPos();
    }
    else
    {
      int entityId = tempZone.GetEntityAtPos(pos3).GetEntityId();
      int pos4;
      for (pos4 = pos3 - 1; pos4 > 0; --pos4)
      {
        Entity entityAtPos = tempZone.GetEntityAtPos(pos4);
        if (entityAtPos.GetCreatorId() != entityId || zone.ContainsCard(entityAtPos.GetCard()))
          break;
      }
      num = pos4 + 1;
    }
    return Mathf.CeilToInt(0.5f * (float) (pos2 + num));
  }

  private int GetNextLocalChangeListId()
  {
    int localChangeListId = this.m_nextLocalChangeListId;
    this.m_nextLocalChangeListId = this.m_nextLocalChangeListId != int.MaxValue ? this.m_nextLocalChangeListId + 1 : 1;
    return localChangeListId;
  }

  private int GetNextServerChangeListId()
  {
    int serverChangeListId = this.m_nextServerChangeListId;
    this.m_nextServerChangeListId = this.m_nextServerChangeListId != int.MaxValue ? this.m_nextServerChangeListId + 1 : 1;
    return serverChangeListId;
  }

  private int FindTriggeredActiveLocalChangeIndex(Entity entity)
  {
    return this.FindTriggeredActiveLocalChangeIndex(entity.GetCard());
  }

  private int FindTriggeredActiveLocalChangeIndex(Card card)
  {
    for (int index = 0; index < this.m_activeLocalChangeLists.Count; ++index)
    {
      if ((UnityEngine.Object) this.m_activeLocalChangeLists[index].GetLocalTriggerCard() == (UnityEngine.Object) card)
        return index;
    }
    return -1;
  }

  private int FindTriggeredPendingLocalChangeIndex(Entity entity)
  {
    return this.FindTriggeredPendingLocalChangeIndex(entity.GetCard());
  }

  private int FindTriggeredPendingLocalChangeIndex(Card card)
  {
    for (int index = 0; index < this.m_pendingLocalChangeLists.Count; ++index)
    {
      if ((UnityEngine.Object) this.m_pendingLocalChangeLists[index].GetLocalTriggerCard() == (UnityEngine.Object) card)
        return index;
    }
    return -1;
  }

  private void AutoCorrectZonesAfterServerChange()
  {
    if (this.HasActiveLocalChange() || this.HasPendingLocalChange() || (this.HasActiveServerChange() || this.HasPendingServerChange()) || this.HasPredictedCards())
      return;
    this.AutoCorrectZones();
  }

  private void AutoCorrectZonesAfterLocalChange()
  {
    if (this.HasActiveLocalChange() || this.HasPendingLocalChange() || (this.HasActiveServerChange() || this.HasPendingServerChange()) || (this.HasPredictedSecrets() || this.HasPredictedWeapons() || (UnityEngine.Object) InputManager.Get().GetBattlecrySourceCard() != (UnityEngine.Object) null))
      return;
    this.AutoCorrectZones();
  }

  private void AutoCorrectZones()
  {
    ZoneChangeList changeList = (ZoneChangeList) null;
    using (List<Zone>.Enumerator enumerator1 = this.FindZonesOfType<Zone>(Player.Side.FRIENDLY).GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Zone current1 = enumerator1.Current;
        using (List<Card>.Enumerator enumerator2 = current1.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Card current2 = enumerator2.Current;
            Entity entity = current2.GetEntity();
            TAG_ZONE zone = entity.GetZone();
            int controllerId1 = entity.GetControllerId();
            int zonePosition1 = entity.GetZonePosition();
            TAG_ZONE serverTag = current1.m_ServerTag;
            int controllerId2 = current1.GetControllerId();
            int zonePosition2 = current2.GetZonePosition();
            if (zone != serverTag || controllerId1 != controllerId2 || zonePosition1 != 0 && zonePosition1 != zonePosition2)
            {
              if (changeList == null)
              {
                int localChangeListId = this.GetNextLocalChangeListId();
                changeList = new ZoneChangeList();
                changeList.SetId(localChangeListId);
              }
              ZoneChange change = new ZoneChange();
              change.SetEntity(entity);
              change.SetDestinationZoneTag(zone);
              change.SetDestinationZone(this.FindZoneForEntity(entity));
              change.SetDestinationControllerId(controllerId1);
              change.SetDestinationPosition(zonePosition1);
              changeList.AddChange(change);
            }
          }
        }
      }
    }
    if (changeList == null)
      return;
    this.ProcessLocalChangeList(changeList);
  }

  private class TempZone
  {
    private List<Entity> m_prevEntities = new List<Entity>();
    private List<Entity> m_entities = new List<Entity>();
    private Map<int, int> m_replacedEntities = new Map<int, int>();
    private Zone m_zone;
    private bool m_modified;

    public Zone GetZone()
    {
      return this.m_zone;
    }

    public void SetZone(Zone zone)
    {
      this.m_zone = zone;
    }

    public bool IsModified()
    {
      return this.m_modified;
    }

    public int GetEntityCount()
    {
      return this.m_entities.Count;
    }

    public List<Entity> GetEntities()
    {
      return this.m_entities;
    }

    public Entity GetEntityAtIndex(int index)
    {
      if (index < 0)
        return (Entity) null;
      if (index >= this.m_entities.Count)
        return (Entity) null;
      return this.m_entities[index];
    }

    public Entity GetEntityAtPos(int pos)
    {
      return this.GetEntityAtIndex(pos - 1);
    }

    public void ClearEntities()
    {
      this.m_entities.Clear();
    }

    public void AddInitialEntity(Entity entity)
    {
      this.m_entities.Add(entity);
    }

    public bool CanAcceptEntity(Entity entity)
    {
      return (UnityEngine.Object) ZoneMgr.Get().FindZoneForEntityAndZoneTag(entity, this.m_zone.m_ServerTag) == (UnityEngine.Object) this.m_zone;
    }

    public void AddEntity(Entity entity)
    {
      if (!this.CanAcceptEntity(entity) || this.m_entities.Contains(entity))
        return;
      this.m_entities.Add(entity);
      this.m_modified = true;
    }

    public void InsertEntityAtIndex(int index, Entity entity)
    {
      if (!this.CanAcceptEntity(entity) || index < 0 || index > this.m_entities.Count || index < this.m_entities.Count && this.m_entities[index] == entity)
        return;
      this.m_entities.Insert(index, entity);
      this.m_modified = true;
    }

    public void InsertEntityAtPos(int pos, Entity entity)
    {
      this.InsertEntityAtIndex(pos - 1, entity);
    }

    public bool RemoveEntity(Entity entity)
    {
      if (!this.m_entities.Remove(entity))
        return false;
      this.m_modified = true;
      return true;
    }

    public int GetLastPos()
    {
      return this.m_entities.Count + 1;
    }

    public int FindEntityPos(Entity entity)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      return 1 + this.m_entities.FindIndex(new Predicate<Entity>(new ZoneMgr.TempZone.\u003CFindEntityPos\u003Ec__AnonStorey3C0() { entity = entity }.\u003C\u003Em__15B));
    }

    public bool ContainsEntity(Entity entity)
    {
      return this.FindEntityPos(entity) > 0;
    }

    public int FindEntityPos(int entityId)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      return 1 + this.m_entities.FindIndex(new Predicate<Entity>(new ZoneMgr.TempZone.\u003CFindEntityPos\u003Ec__AnonStorey3C1() { entityId = entityId }.\u003C\u003Em__15C));
    }

    public bool ContainsEntity(int entityId)
    {
      return this.FindEntityPos(entityId) > 0;
    }

    public int FindEntityPosWithReplacements(int entityId)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      ZoneMgr.TempZone.\u003CFindEntityPosWithReplacements\u003Ec__AnonStorey3C2 replacementsCAnonStorey3C2 = new ZoneMgr.TempZone.\u003CFindEntityPosWithReplacements\u003Ec__AnonStorey3C2();
      // ISSUE: reference to a compiler-generated field
      replacementsCAnonStorey3C2.entityId = entityId;
      // ISSUE: reference to a compiler-generated field
      while (replacementsCAnonStorey3C2.entityId != 0)
      {
        // ISSUE: reference to a compiler-generated method
        int num = 1 + this.m_entities.FindIndex(new Predicate<Entity>(replacementsCAnonStorey3C2.\u003C\u003Em__15D));
        if (num > 0)
          return num;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        this.m_replacedEntities.TryGetValue(replacementsCAnonStorey3C2.entityId, out replacementsCAnonStorey3C2.entityId);
      }
      return 0;
    }

    public void Sort()
    {
      if (this.m_modified)
      {
        this.m_entities.Sort(new Comparison<Entity>(this.SortComparison));
      }
      else
      {
        Entity[] array = this.m_entities.ToArray();
        this.m_entities.Sort(new Comparison<Entity>(this.SortComparison));
        for (int index = 0; index < this.m_entities.Count; ++index)
        {
          if (array[index] != this.m_entities[index])
          {
            this.m_modified = true;
            break;
          }
        }
      }
    }

    public void PreprocessChanges()
    {
      this.m_prevEntities.Clear();
      for (int index = 0; index < this.m_entities.Count; ++index)
        this.m_prevEntities.Add(this.m_entities[index]);
    }

    public void PostprocessChanges()
    {
      for (int index = 0; index < this.m_prevEntities.Count; ++index)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        ZoneMgr.TempZone.\u003CPostprocessChanges\u003Ec__AnonStorey3C3 changesCAnonStorey3C3 = new ZoneMgr.TempZone.\u003CPostprocessChanges\u003Ec__AnonStorey3C3();
        if (index >= this.m_entities.Count)
          break;
        // ISSUE: reference to a compiler-generated field
        changesCAnonStorey3C3.prevEntity = this.m_prevEntities[index];
        // ISSUE: reference to a compiler-generated method
        if (this.m_entities.FindIndex(new Predicate<Entity>(changesCAnonStorey3C3.\u003C\u003Em__15E)) < 0)
        {
          Entity entity = this.m_entities[index];
          if (!this.m_prevEntities.Contains(entity))
          {
            // ISSUE: reference to a compiler-generated field
            this.m_replacedEntities[changesCAnonStorey3C3.prevEntity.GetEntityId()] = entity.GetEntityId();
          }
        }
      }
    }

    public override string ToString()
    {
      return string.Format("{0} ({1} entities)", (object) this.m_zone, (object) this.m_entities.Count);
    }

    private int SortComparison(Entity entity1, Entity entity2)
    {
      return entity1.GetZonePosition() - entity2.GetZonePosition();
    }
  }

  public delegate void ChangeCompleteCallback(ZoneChangeList changeList, object userData);
}
