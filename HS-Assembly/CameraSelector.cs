﻿// Decompiled with JetBrains decompiler
// Type: CameraSelector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CameraSelector : MonoBehaviour
{
  public Vector3 cameraPosition;
  public Vector3 cameraRotation;
  public bool activateOnStart;

  private void Start()
  {
    if (!this.activateOnStart)
      return;
    Camera.main.transform.rotation = Quaternion.Euler(this.cameraRotation);
    Camera.main.transform.position = this.cameraPosition;
  }

  private void OnMouseDown()
  {
    Camera.main.transform.rotation = Quaternion.Euler(this.cameraRotation);
    Camera.main.transform.position = this.cameraPosition;
  }
}
