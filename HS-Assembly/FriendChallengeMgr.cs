﻿// Decompiled with JetBrains decompiler
// Type: FriendChallengeMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using PegasusShared;
using SpectatorProto;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FriendChallengeMgr
{
  private int m_scenarioId = 2;
  private List<FriendChallengeMgr.ChangedListener> m_changedListeners = new List<FriendChallengeMgr.ChangedListener>();
  private const int DEFAULT_SCENARIO_ID = 2;
  private const bool FRIENDLY_CHALLENGE_PARTY_INVITES_USE_RESERVATIONS = true;
  private const string ATTRIBUTE_STATE_PLAYER1 = "s1";
  private const string ATTRIBUTE_STATE_PLAYER2 = "s2";
  private const string ATTRIBUTE_DECK_PLAYER1 = "d1";
  private const string ATTRIBUTE_DECK_PLAYER2 = "d2";
  private const string ATTRIBUTE_LEFT = "left";
  private const string ATTRIBUTE_ERROR = "error";
  private const string ATTRIBUTE_QUEST_INFO = "quests";
  private const string ATTRIBUTE_VALUE_STATE_WAIT = "wait";
  private const string ATTRIBUTE_VALUE_STATE_DECK = "deck";
  private const string ATTRIBUTE_VALUE_STATE_READY = "ready";
  private const string ATTRIBUTE_VALUE_STATE_GAME = "game";
  private const string ATTRIBUTE_VALUE_STATE_GOTO = "goto";
  private static FriendChallengeMgr s_instance;
  private bool m_netCacheReady;
  private bool m_myPlayerReady;
  private BnetEntityId m_partyId;
  private BnetEntityId m_challengeePartyId;
  private BnetGameAccountId m_challengerId;
  private bool m_challengerPending;
  private bool m_isChallengeTavernBrawl;
  private FormatType m_challengeFormatType;
  private BnetPlayer m_challenger;
  private long m_challengerDeckId;
  private bool m_challengerDeckSelected;
  private BnetPlayer m_challengee;
  private long m_challengeeDeckId;
  private bool m_challengeeAccepted;
  private bool m_challengeeDeckSelected;
  private bool m_challengerInGameState;
  private bool m_challengeeInGameState;
  private DialogBase m_challengeDialog;
  private bool m_hasSeenDeclinedReason;
  private bool m_updatePartyQuestInfoOnGameplaySceneUnload;

  public static FriendChallengeMgr Get()
  {
    if (FriendChallengeMgr.s_instance == null)
    {
      FriendChallengeMgr.s_instance = new FriendChallengeMgr();
      ApplicationMgr.Get().WillReset += new System.Action(FriendChallengeMgr.s_instance.WillReset);
      AchieveManager.Get().RegisterAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(FriendChallengeMgr.s_instance.AchieveManager_OnAchievesUpdated), (object) null);
      BnetParty.OnJoined += new BnetParty.JoinedHandler(FriendChallengeMgr.s_instance.BnetParty_OnJoined);
      BnetParty.OnReceivedInvite += new BnetParty.ReceivedInviteHandler(FriendChallengeMgr.s_instance.BnetParty_OnReceivedInvite);
      BnetParty.OnPartyAttributeChanged += new BnetParty.PartyAttributeChangedHandler(FriendChallengeMgr.s_instance.BnetParty_OnPartyAttributeChanged);
      BnetParty.OnMemberEvent += new BnetParty.MemberEventHandler(FriendChallengeMgr.s_instance.BnetParty_OnMemberEvent);
    }
    return FriendChallengeMgr.s_instance;
  }

  public void OnLoggedIn()
  {
    NetCache.Get().RegisterFriendChallenge(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    SceneMgr.Get().RegisterSceneUnloadedEvent(new SceneMgr.SceneUnloadedCallback(this.OnSceneUnloaded));
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    BnetFriendMgr.Get().AddChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
    BnetNearbyPlayerMgr.Get().AddChangeListener(new BnetNearbyPlayerMgr.ChangeCallback(this.OnNearbyPlayersChanged));
    BnetEventMgr.Get().AddChangeListener(new BnetEventMgr.ChangeCallback(this.OnBnetEventOccurred));
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    this.AddChangedListener(new FriendChallengeMgr.ChangedCallback(this.OnChallengeChanged));
    BnetPresenceMgr.Get().SetGameField(19U, BattleNet.GetVersion());
    BnetPresenceMgr.Get().SetGameField(20U, BattleNet.GetEnvironment());
  }

  private void BnetParty_OnJoined(OnlineEventType evt, PartyInfo party, LeaveReason? reason)
  {
    if (party.Type != PartyType.FRIENDLY_CHALLENGE)
      return;
    if (evt == OnlineEventType.ADDED)
    {
      long? partyAttributeLong1 = BnetParty.GetPartyAttributeLong(party.Id, "WTCG.Game.ScenarioId");
      if (partyAttributeLong1.HasValue)
      {
        this.m_scenarioId = (int) partyAttributeLong1.Value;
        TavernBrawlMission tavernBrawlMission = TavernBrawlManager.Get().CurrentMission();
        this.m_isChallengeTavernBrawl = tavernBrawlMission != null && this.m_scenarioId == tavernBrawlMission.missionId;
      }
      long? partyAttributeLong2 = BnetParty.GetPartyAttributeLong(party.Id, "WTCG.Format.Type");
      this.m_challengeFormatType = !partyAttributeLong2.HasValue ? FormatType.FT_UNKNOWN : (FormatType) partyAttributeLong2.Value;
      string attributeKey = !this.DidSendChallenge() ? "s2" : "s1";
      BnetParty.SetPartyAttributeString(party.Id, attributeKey, "wait");
      if (this.DidSendChallenge())
      {
        BnetParty.SendInvite(party.Id, this.m_challengee.GetHearthstoneGameAccountId(), true);
      }
      else
      {
        foreach (bnet.protocol.attribute.Attribute attribute in BnetParty.GetAllPartyAttributesVariant(party.Id))
          this.BnetParty_OnPartyAttributeChanged(party, attribute.Name, attribute.Value);
      }
    }
    else
    {
      if (evt != OnlineEventType.REMOVED)
        return;
      if (!((IEnumerable<PartyInfo>) BnetParty.GetJoinedParties()).Any<PartyInfo>((Func<PartyInfo, bool>) (i => i.Type == PartyType.FRIENDLY_CHALLENGE)))
        this.m_scenarioId = 2;
      string data = !reason.HasValue ? "NO_SUPPLIED_REASON" : ((int) reason.Value).ToString();
      this.PushPartyEvent(party.Id.ToBnetEntityId(), "left", data, (BnetGameAccountId) null);
    }
  }

  private void BnetParty_OnReceivedInvite(OnlineEventType evt, PartyInfo party, ulong inviteId, InviteRemoveReason? reason)
  {
    if (party.Type != PartyType.FRIENDLY_CHALLENGE || evt != OnlineEventType.ADDED)
      return;
    if (BnetParty.IsInParty((PartyId) this.m_partyId))
      BnetParty.DeclineReceivedInvite(inviteId);
    else
      BnetParty.AcceptReceivedInvite(inviteId);
  }

  private void BnetParty_OnPartyAttributeChanged(PartyInfo party, string attributeKey, bnet.protocol.attribute.Variant value)
  {
    if (party.Type != PartyType.FRIENDLY_CHALLENGE)
      return;
    string key = attributeKey;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (FriendChallengeMgr.\u003C\u003Ef__switch\u0024map1 == null)
      {
        // ISSUE: reference to a compiler-generated field
        FriendChallengeMgr.\u003C\u003Ef__switch\u0024map1 = new Dictionary<string, int>(4)
        {
          {
            "WTCG.Friendly.DeclineReason",
            0
          },
          {
            "error",
            1
          },
          {
            "d1",
            2
          },
          {
            "d2",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (FriendChallengeMgr.\u003C\u003Ef__switch\u0024map1.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            this.BnetParty_OnPartyAttributeChanged_DeclineReason(party, attributeKey, value);
            break;
          case 1:
            this.BnetParty_OnPartyAttributeChanged_Error(party, attributeKey, value);
            break;
          case 2:
            this.m_challengerDeckId = !value.HasIntValue ? 0L : value.IntValue;
            break;
          case 3:
            this.m_challengeeDeckId = !value.HasIntValue ? 0L : value.IntValue;
            break;
        }
      }
    }
    if (!value.HasStringValue)
      return;
    BnetGameAccountId otherPlayerGameAccountId = (BnetGameAccountId) null;
    if (this.DidSendChallenge())
    {
      if (this.m_challengee != null)
        otherPlayerGameAccountId = this.m_challengee.GetHearthstoneGameAccountId();
    }
    else if (this.m_challenger != null)
      otherPlayerGameAccountId = this.m_challenger.GetHearthstoneGameAccountId();
    if ((BnetEntityId) otherPlayerGameAccountId == (BnetEntityId) null)
    {
      BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
      foreach (bgs.PartyMember member in BnetParty.GetMembers(party.Id))
      {
        if ((BnetEntityId) member.GameAccountId != (BnetEntityId) myGameAccountId)
        {
          otherPlayerGameAccountId = member.GameAccountId;
          break;
        }
      }
    }
    this.PushPartyEvent(party.Id.ToBnetEntityId(), attributeKey, value.StringValue, otherPlayerGameAccountId);
  }

  private void BnetParty_OnPartyAttributeChanged_DeclineReason(PartyInfo party, string attributeKey, bnet.protocol.attribute.Variant value)
  {
    if (party.Type != PartyType.FRIENDLY_CHALLENGE || !this.DidSendChallenge() || !value.HasIntValue)
      return;
    FriendChallengeMgr.DeclineReason intValue = (FriendChallengeMgr.DeclineReason) value.IntValue;
    string key = (string) null;
    switch (intValue)
    {
      case FriendChallengeMgr.DeclineReason.NoValidDeck:
        key = "GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGEE_NO_DECK";
        break;
      case FriendChallengeMgr.DeclineReason.StandardNoValidDeck:
        key = "GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGEE_NO_STANDARD_DECK";
        break;
      case FriendChallengeMgr.DeclineReason.TavernBrawlNoValidDeck:
        key = "GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGEE_NO_TAVERN_BRAWL_DECK";
        break;
      case FriendChallengeMgr.DeclineReason.TavernBrawlNotUnlocked:
        key = "GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGEE_TAVERN_BRAWL_LOCKED";
        break;
      case FriendChallengeMgr.DeclineReason.UserIsBusy:
        key = "GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGEE_USER_IS_BUSY";
        break;
      case FriendChallengeMgr.DeclineReason.NotSeenWild:
        key = "GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGEE_NOT_SEEN_WILD";
        break;
    }
    if (key == null)
      return;
    if ((UnityEngine.Object) this.m_challengeDialog != (UnityEngine.Object) null)
    {
      this.m_challengeDialog.Hide();
      this.m_challengeDialog = (DialogBase) null;
    }
    this.m_hasSeenDeclinedReason = true;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
      m_text = GameStrings.Get(key),
      m_responseDisplay = AlertPopup.ResponseDisplay.OK,
      m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
    });
  }

  private void BnetParty_OnPartyAttributeChanged_Error(PartyInfo party, string attributeKey, bnet.protocol.attribute.Variant value)
  {
    if (party.Type != PartyType.FRIENDLY_CHALLENGE)
      return;
    if (this.DidReceiveChallenge() && value.HasIntValue)
    {
      Log.Party.Print(LogLevel.Error, "BnetParty_OnPartyAttributeChanged_Error - code={0}", new object[1]
      {
        (object) value.IntValue
      });
      GameMgr.Get().OnBnetError(new BnetErrorInfo(BnetFeature.Games, BnetFeatureEvent.Games_OnCreated, (BattleNetErrors) value.IntValue), (object) null);
    }
    if (!BnetParty.IsLeader(party.Id) || value.IsNone())
      return;
    BnetParty.ClearPartyAttribute(party.Id, attributeKey);
  }

  private void BnetParty_OnMemberEvent(OnlineEventType evt, PartyInfo party, BnetGameAccountId memberId, bool isRolesUpdate, LeaveReason? reason)
  {
    if (party.Type != PartyType.FRIENDLY_CHALLENGE || evt != OnlineEventType.REMOVED || !BnetParty.IsInParty(party.Id))
      return;
    BnetParty.DissolveParty(party.Id);
  }

  private void AchieveManager_OnAchievesUpdated(List<Achievement> updatedAchieves, List<Achievement> completedAchievements, object userData)
  {
    if (!completedAchievements.Any<Achievement>((Func<Achievement, bool>) (a => a.IsFriendlyChallengeQuest)))
      return;
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY)
      this.m_updatePartyQuestInfoOnGameplaySceneUnload = true;
    else
      this.UpdatePartyQuestInfo();
  }

  private void UpdatePartyQuestInfo()
  {
    if (!this.DidSendChallenge() || !BnetParty.IsInParty((PartyId) this.m_partyId))
      return;
    byte[] numArray = (byte[]) null;
    IEnumerable<Achievement> source = AchieveManager.Get().GetActiveQuests(false).Where<Achievement>((Func<Achievement, bool>) (q => q.IsFriendlyChallengeQuest));
    if (source.Any<Achievement>())
    {
      PartyQuestInfo partyQuestInfo = new PartyQuestInfo();
      partyQuestInfo.QuestIds.AddRange(source.Select<Achievement, int>((Func<Achievement, int>) (q => q.ID)));
      numArray = ProtobufUtil.ToByteArray((IProtoBuf) partyQuestInfo);
    }
    BnetParty.SetPartyAttributeBlob((PartyId) this.m_partyId, "quests", numArray);
  }

  public void OnStoreOpened()
  {
    this.UpdateMyAvailability();
  }

  public void OnStoreClosed()
  {
    this.UpdateMyAvailability();
  }

  public BnetPlayer GetChallengee()
  {
    return this.m_challengee;
  }

  public BnetPlayer GetChallenger()
  {
    return this.m_challenger;
  }

  public bool DidReceiveChallenge()
  {
    if (this.m_challengerPending)
      return true;
    if (this.m_challenger != null)
      return this.m_challengee == BnetPresenceMgr.Get().GetMyPlayer();
    return false;
  }

  public bool DidSendChallenge()
  {
    if (this.m_challengee != null)
      return this.m_challenger == BnetPresenceMgr.Get().GetMyPlayer();
    return false;
  }

  public bool HasChallenge()
  {
    if (!this.DidSendChallenge())
      return this.DidReceiveChallenge();
    return true;
  }

  public bool AmIInGameState()
  {
    if (this.DidSendChallenge())
      return this.m_challengerInGameState;
    return this.m_challengeeInGameState;
  }

  public BnetPlayer GetOpponent(BnetPlayer player)
  {
    if (player == this.m_challenger)
      return this.m_challengee;
    if (player == this.m_challengee)
      return this.m_challenger;
    return (BnetPlayer) null;
  }

  public BnetPlayer GetMyOpponent()
  {
    return this.GetOpponent(BnetPresenceMgr.Get().GetMyPlayer());
  }

  public bool CanChallenge(BnetPlayer player)
  {
    if (player == null)
      return false;
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    if (player == myPlayer || !this.AmIAvailable())
      return false;
    BnetGameAccount hearthstoneGameAccount1 = player.GetHearthstoneGameAccount();
    if (hearthstoneGameAccount1 == (BnetGameAccount) null || !hearthstoneGameAccount1.IsOnline() || !hearthstoneGameAccount1.CanBeInvitedToGame())
      return false;
    if (ApplicationMgr.IsPublic())
    {
      BnetGameAccount hearthstoneGameAccount2 = myPlayer.GetHearthstoneGameAccount();
      if (string.Compare(hearthstoneGameAccount1.GetClientVersion(), hearthstoneGameAccount2.GetClientVersion()) != 0 || string.Compare(hearthstoneGameAccount1.GetClientEnv(), hearthstoneGameAccount2.GetClientEnv()) != 0)
        return false;
    }
    return true;
  }

  public bool AmIAvailable()
  {
    if (!this.m_netCacheReady || !this.m_myPlayerReady || SpectatorManager.Get().IsInSpectatorMode())
      return false;
    BnetGameAccount hearthstoneGameAccount = BnetPresenceMgr.Get().GetMyPlayer().GetHearthstoneGameAccount();
    if (hearthstoneGameAccount == (BnetGameAccount) null)
      return false;
    return hearthstoneGameAccount.CanBeInvitedToGame();
  }

  public bool DidISelectDeck()
  {
    if (this.DidSendChallenge())
      return this.m_challengerDeckSelected;
    if (this.DidReceiveChallenge())
      return this.m_challengeeDeckSelected;
    return true;
  }

  public bool DidOpponentSelectDeck()
  {
    if (this.DidSendChallenge())
      return this.m_challengeeDeckSelected;
    if (this.DidReceiveChallenge())
      return this.m_challengerDeckSelected;
    return true;
  }

  public static void ShowChallengerNeedsToCreateTavernBrawlDeckAlert()
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
      m_text = GameStrings.Format("GLOBAL_FRIENDLIST_CHALLENGE_CHALLENGER_NO_TAVERN_BRAWL_DECK"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK,
      m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
    });
  }

  public void SendChallenge(BnetPlayer player, FormatType formatType)
  {
    if (!this.CanChallenge(player))
      return;
    this.SendChallenge_Internal(player, formatType, false);
  }

  public void SendTavernBrawlChallenge(BnetPlayer player)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    FriendChallengeMgr.\u003CSendTavernBrawlChallenge\u003Ec__AnonStorey368 challengeCAnonStorey368 = new FriendChallengeMgr.\u003CSendTavernBrawlChallenge\u003Ec__AnonStorey368();
    // ISSUE: reference to a compiler-generated field
    challengeCAnonStorey368.player = player;
    // ISSUE: reference to a compiler-generated field
    challengeCAnonStorey368.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    if (!this.CanChallenge(challengeCAnonStorey368.player))
      return;
    // ISSUE: reference to a compiler-generated method
    TavernBrawlManager.Get().EnsureAllDataReady(new TavernBrawlManager.CallbackEnsureServerDataReady(challengeCAnonStorey368.\u003C\u003Em__53));
  }

  private void TavernBrawl_SendChallenge_OnEnsureServerDataReady(BnetPlayer player)
  {
    TavernBrawlManager tavernBrawlManager = TavernBrawlManager.Get();
    if (!this.CanChallenge(player) || !tavernBrawlManager.IsTavernBrawlActive || this.HasChallenge())
      return;
    if (!tavernBrawlManager.CanChallengeToTavernBrawl)
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
        m_text = GameStrings.Format("GLOBAL_FRIENDLIST_CHALLENGE_TOOLTIP_TAVERN_BRAWL_NOT_CHALLENGEABLE"),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
      });
    else if (tavernBrawlManager.CurrentMission().canCreateDeck && !tavernBrawlManager.HasValidDeck())
      FriendChallengeMgr.ShowChallengerNeedsToCreateTavernBrawlDeckAlert();
    else
      this.SendChallenge_Internal(player, FormatType.FT_UNKNOWN, true);
  }

  private void SendChallenge_Internal(BnetPlayer player, FormatType formatType, bool isTavernBrawl)
  {
    this.m_challenger = BnetPresenceMgr.Get().GetMyPlayer();
    this.m_challengerId = this.m_challenger.GetHearthstoneGameAccount().GetId();
    this.m_challengee = player;
    this.m_hasSeenDeclinedReason = false;
    this.m_scenarioId = 2;
    this.m_isChallengeTavernBrawl = false;
    this.m_challengeFormatType = formatType;
    if (isTavernBrawl)
    {
      TavernBrawlMission tavernBrawlMission = TavernBrawlManager.Get().CurrentMission();
      this.m_scenarioId = tavernBrawlMission.missionId;
      this.m_challengeFormatType = tavernBrawlMission.formatType;
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TAVERN_BRAWL_FRIENDLY_WAITING);
      this.m_isChallengeTavernBrawl = true;
    }
    bnet.protocol.attribute.Attribute attribute = (bnet.protocol.attribute.Attribute) null;
    IEnumerable<Achievement> source = AchieveManager.Get().GetActiveQuests(false).Where<Achievement>((Func<Achievement, bool>) (q => q.IsFriendlyChallengeQuest));
    if (source.Any<Achievement>())
    {
      PartyQuestInfo partyQuestInfo = new PartyQuestInfo();
      partyQuestInfo.QuestIds.AddRange(source.Select<Achievement, int>((Func<Achievement, int>) (q => q.ID)));
      attribute = ProtocolHelper.CreateAttribute("quests", ProtobufUtil.ToByteArray((IProtoBuf) partyQuestInfo));
    }
    BnetParty.CreateParty(PartyType.FRIENDLY_CHALLENGE, PrivacyLevel.OPEN_INVITATION, (BnetParty.CreateSuccessCallback) null, ProtocolHelper.CreateAttribute("WTCG.Game.ScenarioId", (long) this.m_scenarioId), ProtocolHelper.CreateAttribute("WTCG.Format.Type", (long) this.m_challengeFormatType), attribute);
    this.UpdateMyAvailability();
    this.FireChangedEvent(FriendChallengeEvent.I_SENT_CHALLENGE, player);
  }

  public void CancelChallenge()
  {
    if (!this.HasChallenge())
      return;
    if (this.DidSendChallenge())
    {
      this.RescindChallenge();
    }
    else
    {
      if (!this.DidReceiveChallenge())
        return;
      this.DeclineChallenge();
    }
  }

  public void AcceptChallenge()
  {
    if (!this.DidReceiveChallenge())
      return;
    this.m_challengeeAccepted = true;
    BnetParty.SetPartyAttributeString((PartyId) this.m_partyId, !this.DidSendChallenge() ? "s2" : "s1", "deck");
    this.FireChangedEvent(FriendChallengeEvent.I_ACCEPTED_CHALLENGE, this.m_challenger);
  }

  public void DeclineChallenge()
  {
    if (!this.DidReceiveChallenge())
      return;
    this.RevertTavernBrawlStatus();
    this.DeclineFriendChallenge_Internal(this.m_partyId);
    BnetPlayer challenger = this.m_challenger;
    this.CleanUpChallengeData(true);
    this.FireChangedEvent(FriendChallengeEvent.I_DECLINED_CHALLENGE, challenger);
  }

  private void DeclineFriendChallenge_Internal(BnetEntityId partyId)
  {
    if (!BnetParty.IsInParty((PartyId) partyId))
      return;
    BnetParty.DissolveParty((PartyId) partyId);
  }

  private void PushPartyEvent(BnetEntityId partyId, string type, string data, BnetGameAccountId otherPlayerGameAccountId = null)
  {
    if ((BnetEntityId) otherPlayerGameAccountId == (BnetEntityId) null)
    {
      BnetPlayer bnetPlayer = !this.DidSendChallenge() ? this.m_challengee : this.m_challenger;
      otherPlayerGameAccountId = bnetPlayer != null ? bnetPlayer.GetHearthstoneGameAccountId() : (BnetGameAccountId) null;
    }
    PartyEvent partyEvent = new PartyEvent();
    partyEvent.partyId.hi = partyId.GetHi();
    partyEvent.partyId.lo = partyId.GetLo();
    partyEvent.eventName = type;
    partyEvent.eventData = data;
    if ((BnetEntityId) otherPlayerGameAccountId != (BnetEntityId) null)
    {
      partyEvent.otherMemberId.hi = otherPlayerGameAccountId.GetHi();
      partyEvent.otherMemberId.lo = otherPlayerGameAccountId.GetLo();
    }
    this.OnPartyUpdate(new PartyEvent[1]{ partyEvent });
  }

  public void RescindChallenge()
  {
    if (!this.DidSendChallenge())
      return;
    this.RevertTavernBrawlStatus();
    if (BnetParty.IsInParty((PartyId) this.m_partyId))
      BnetParty.DissolveParty((PartyId) this.m_partyId);
    BnetPlayer challengee = this.m_challengee;
    this.CleanUpChallengeData(true);
    this.FireChangedEvent(FriendChallengeEvent.I_RESCINDED_CHALLENGE, challengee);
  }

  public bool IsChallengeStandardDuel()
  {
    if (!this.HasChallenge() || this.m_isChallengeTavernBrawl)
      return false;
    return this.m_challengeFormatType == FormatType.FT_STANDARD;
  }

  public bool IsChallengeWildDuel()
  {
    if (!this.HasChallenge() || this.m_isChallengeTavernBrawl)
      return false;
    return this.m_challengeFormatType == FormatType.FT_WILD;
  }

  public bool IsChallengeTavernBrawl()
  {
    if (!this.HasChallenge())
      return false;
    return this.m_isChallengeTavernBrawl;
  }

  public void SkipDeckSelection()
  {
    this.SelectDeck(1L);
  }

  public void SelectDeck(long deckId)
  {
    if (this.DidSendChallenge())
    {
      this.m_challengerDeckSelected = true;
    }
    else
    {
      if (!this.DidReceiveChallenge())
        return;
      this.m_challengeeDeckSelected = true;
    }
    this.SelectMyDeck_InternalParty(deckId);
    this.FireChangedEvent(FriendChallengeEvent.SELECTED_DECK, BnetPresenceMgr.Get().GetMyPlayer());
  }

  public void DeselectDeck()
  {
    if (this.DidSendChallenge() && this.m_challengerDeckSelected)
    {
      this.m_challengerDeckSelected = false;
      this.m_challengerInGameState = false;
    }
    else
    {
      if (!this.DidReceiveChallenge() || !this.m_challengeeDeckSelected)
        return;
      this.m_challengeeDeckSelected = false;
      this.m_challengeeInGameState = false;
    }
    this.SelectMyDeck_InternalParty(0L);
    this.FireChangedEvent(FriendChallengeEvent.DESELECTED_DECK, BnetPresenceMgr.Get().GetMyPlayer());
  }

  private void SelectMyDeck_InternalParty(long deckId)
  {
    string val = deckId != 0L ? "ready" : "deck";
    bnet.protocol.attribute.Attribute[] attributeArray;
    if (this.DidSendChallenge())
    {
      this.m_challengerDeckId = deckId;
      attributeArray = new bnet.protocol.attribute.Attribute[2]
      {
        ProtocolHelper.CreateAttribute("s1", val),
        ProtocolHelper.CreateAttribute("d1", deckId)
      };
    }
    else
    {
      this.m_challengeeDeckId = deckId;
      attributeArray = new bnet.protocol.attribute.Attribute[2]
      {
        ProtocolHelper.CreateAttribute("s2", val),
        ProtocolHelper.CreateAttribute("d2", deckId)
      };
    }
    BnetParty.SetPartyAttributes((PartyId) this.m_partyId, attributeArray);
  }

  public int GetScenarioId()
  {
    return this.m_scenarioId;
  }

  public FormatType GetFormatType()
  {
    return this.m_challengeFormatType;
  }

  public PartyQuestInfo GetPartyQuestInfo()
  {
    PartyQuestInfo partyQuestInfo = (PartyQuestInfo) null;
    byte[] partyAttributeBlob = BnetParty.GetPartyAttributeBlob((PartyId) this.m_partyId, "quests");
    if (partyAttributeBlob != null && partyAttributeBlob.Length > 0)
      partyQuestInfo = ProtobufUtil.ParseFrom<PartyQuestInfo>(partyAttributeBlob, 0, -1);
    return partyQuestInfo;
  }

  public bool AddChangedListener(FriendChallengeMgr.ChangedCallback callback)
  {
    return this.AddChangedListener(callback, (object) null);
  }

  public bool AddChangedListener(FriendChallengeMgr.ChangedCallback callback, object userData)
  {
    FriendChallengeMgr.ChangedListener changedListener = new FriendChallengeMgr.ChangedListener();
    changedListener.SetCallback(callback);
    changedListener.SetUserData(userData);
    if (this.m_changedListeners.Contains(changedListener))
      return false;
    this.m_changedListeners.Add(changedListener);
    return true;
  }

  public bool RemoveChangedListener(FriendChallengeMgr.ChangedCallback callback)
  {
    return this.RemoveChangedListener(callback, (object) null);
  }

  public bool RemoveChangedListener(FriendChallengeMgr.ChangedCallback callback, object userData)
  {
    FriendChallengeMgr.ChangedListener changedListener = new FriendChallengeMgr.ChangedListener();
    changedListener.SetCallback(callback);
    changedListener.SetUserData(userData);
    return this.m_changedListeners.Remove(changedListener);
  }

  private void OnPartyUpdate(PartyEvent[] updates)
  {
    for (int index = 0; index < updates.Length; ++index)
    {
      PartyEvent update = updates[index];
      BnetEntityId fromEntityId1 = BnetEntityId.CreateFromEntityId(update.partyId);
      BnetGameAccountId fromEntityId2 = BnetGameAccountId.CreateFromEntityId(update.otherMemberId);
      if (update.eventName == "s1")
      {
        if (update.eventData == "wait")
          this.OnPartyUpdate_CreatedParty(fromEntityId1, fromEntityId2);
        else if (update.eventData == "deck")
        {
          if (this.DidReceiveChallenge() && this.m_challengerDeckSelected)
          {
            this.m_challengerDeckSelected = false;
            this.m_challengerInGameState = false;
            this.FireChangedEvent(FriendChallengeEvent.DESELECTED_DECK, this.m_challenger);
          }
        }
        else if (update.eventData == "ready")
        {
          if (this.DidReceiveChallenge())
          {
            this.m_challengerDeckSelected = true;
            this.FireChangedEvent(FriendChallengeEvent.SELECTED_DECK, this.m_challenger);
            this.SetIAmInGameState();
          }
        }
        else if (update.eventData == "game")
        {
          if (this.DidReceiveChallenge())
          {
            this.m_challengerInGameState = true;
            this.SetIAmInGameState();
            FriendlyChallengeHelper.Get().WaitForFriendChallengeToStart();
            this.StartFriendlyChallengeGameIfReady();
          }
        }
        else if (update.eventData == "goto")
        {
          this.m_challengerDeckSelected = false;
          this.m_challengerInGameState = false;
        }
      }
      else if (update.eventName == "s2")
      {
        if (update.eventData == "wait")
          this.OnPartyUpdate_JoinedParty(fromEntityId1, fromEntityId2);
        else if (update.eventData == "deck")
        {
          if (this.DidSendChallenge())
          {
            if (this.m_challengeeAccepted)
            {
              this.m_challengeeDeckSelected = false;
              this.m_challengeeInGameState = false;
              this.FireChangedEvent(FriendChallengeEvent.DESELECTED_DECK, this.m_challengee);
            }
            else
            {
              this.m_challengeeAccepted = true;
              this.FireChangedEvent(FriendChallengeEvent.OPPONENT_ACCEPTED_CHALLENGE, this.m_challengee);
            }
          }
        }
        else if (update.eventData == "ready")
        {
          if (this.DidSendChallenge())
          {
            this.m_challengeeDeckSelected = true;
            this.FireChangedEvent(FriendChallengeEvent.SELECTED_DECK, this.m_challengee);
            this.SetIAmInGameState();
          }
        }
        else if (update.eventData == "game")
        {
          if (this.DidSendChallenge())
          {
            this.m_challengeeInGameState = true;
            this.SetIAmInGameState();
            FriendlyChallengeHelper.Get().WaitForFriendChallengeToStart();
            this.StartFriendlyChallengeGameIfReady();
          }
        }
        else if (update.eventData == "goto")
        {
          this.m_challengeeDeckSelected = false;
          this.m_challengeeInGameState = false;
        }
      }
      else if (update.eventName == "left")
      {
        if (this.DidSendChallenge())
        {
          BnetPlayer challengee = this.m_challengee;
          bool challengeeAccepted = this.m_challengeeAccepted;
          this.RevertTavernBrawlStatus();
          this.CleanUpChallengeData(true);
          if (challengeeAccepted)
            this.FireChangedEvent(FriendChallengeEvent.OPPONENT_CANCELED_CHALLENGE, challengee);
          else
            this.FireChangedEvent(FriendChallengeEvent.OPPONENT_DECLINED_CHALLENGE, challengee);
        }
        else if (this.DidReceiveChallenge())
        {
          BnetPlayer challenger = this.m_challenger;
          bool challengeeAccepted = this.m_challengeeAccepted;
          this.RevertTavernBrawlStatus();
          this.CleanUpChallengeData(true);
          if (challenger != null)
          {
            if (challengeeAccepted)
              this.FireChangedEvent(FriendChallengeEvent.OPPONENT_CANCELED_CHALLENGE, challenger);
            else
              this.FireChangedEvent(FriendChallengeEvent.OPPONENT_RESCINDED_CHALLENGE, challenger);
          }
        }
      }
    }
  }

  private void OnPartyUpdate_CreatedParty(BnetEntityId partyId, BnetGameAccountId otherMemberId)
  {
    this.m_partyId = partyId;
    if (this.m_challengeePartyId != (BnetEntityId) null && this.m_challengeePartyId != this.m_partyId)
    {
      this.ResolveChallengeConflict();
    }
    else
    {
      this.m_challengeePartyId = this.m_partyId;
      this.UpdateChallengeSentDialog();
    }
  }

  private void OnPartyUpdate_JoinedParty(BnetEntityId partyId, BnetGameAccountId otherMemberId)
  {
    if (this.DidSendChallenge() && (BnetEntityId) this.m_challengee.GetHearthstoneGameAccount().GetId() == (BnetEntityId) otherMemberId)
    {
      if (!(partyId != this.m_partyId))
        return;
      this.m_challengeePartyId = partyId;
      if (this.m_partyId == (BnetEntityId) null)
        return;
      this.ResolveChallengeConflict();
    }
    else if (!BnetUtils.CanReceiveChallengeFrom(otherMemberId))
      this.DeclineFriendChallenge_Internal(partyId);
    else if (!this.AmIAvailable())
      this.DeclineFriendChallenge_Internal(partyId);
    else
      this.HandleJoinedParty(partyId, otherMemberId);
  }

  private void StartFriendlyChallengeGameIfReady()
  {
    if (!this.DidSendChallenge() || !BnetParty.IsInParty((PartyId) this.m_partyId) || (this.m_challengerDeckId == 0L || this.m_challengeeDeckId == 0L) || (!this.m_challengerInGameState || !this.m_challengeeInGameState))
      return;
    BnetParty.SetPartyAttributes((PartyId) this.m_partyId, ProtocolHelper.CreateAttribute("s1", "goto"), ProtocolHelper.CreateAttribute("s2", "goto"));
    GameMgr.Get().EnterFriendlyChallengeGame(this.GetFormatType(), this.m_scenarioId, this.m_challengerDeckId, this.m_challengeeDeckId, this.m_challengee.GetHearthstoneGameAccountId());
    this.m_challengerDeckId = this.m_challengeeDeckId = 0L;
  }

  private void SetIAmInGameState()
  {
    if (!BnetParty.IsInParty((PartyId) this.m_partyId) || !this.m_challengerDeckSelected || (!this.m_challengeeDeckSelected || this.AmIInGameState()))
      return;
    if (this.DidSendChallenge())
    {
      this.m_challengerInGameState = true;
      BnetParty.SetPartyAttributeString((PartyId) this.m_partyId, "s1", "game");
    }
    else
    {
      this.m_challengeeInGameState = true;
      BnetParty.SetPartyAttributeString((PartyId) this.m_partyId, "s2", "game");
    }
  }

  private void OnNetCacheReady()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    this.m_netCacheReady = true;
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.FATAL_ERROR)
      return;
    this.UpdateMyAvailability();
  }

  private void OnSceneUnloaded(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    if (Network.IsLoggedIn() && prevMode != SceneMgr.Mode.GAMEPLAY)
      this.UpdateMyAvailability();
    if (!this.m_updatePartyQuestInfoOnGameplaySceneUnload || prevMode != SceneMgr.Mode.GAMEPLAY)
      return;
    this.m_updatePartyQuestInfoOnGameplaySceneUnload = false;
    this.UpdatePartyQuestInfo();
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (SceneMgr.Get().GetPrevMode() != SceneMgr.Mode.GAMEPLAY || mode == SceneMgr.Mode.GAMEPLAY || mode == SceneMgr.Mode.FATAL_ERROR)
      return;
    this.m_netCacheReady = false;
    if (mode == SceneMgr.Mode.FRIENDLY || TavernBrawlManager.IsInTavernBrawlFriendlyChallenge())
      this.UpdateMyAvailability();
    else
      this.CleanUpChallengeData(true);
    NetCache.Get().RegisterFriendChallenge(new NetCache.NetCacheCallback(this.OnNetCacheReady));
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    if (changelist.FindChange(myPlayer) != null)
    {
      BnetGameAccount hearthstoneGameAccount = myPlayer.GetHearthstoneGameAccount();
      if (hearthstoneGameAccount != (BnetGameAccount) null && !this.m_myPlayerReady && (hearthstoneGameAccount.HasGameField(20U) && hearthstoneGameAccount.HasGameField(19U)))
      {
        this.m_myPlayerReady = true;
        this.UpdateMyAvailability();
      }
      if (!this.AmIAvailable() && this.m_challengerPending)
      {
        this.DeclineFriendChallenge_Internal(this.m_partyId);
        this.CleanUpChallengeData(true);
      }
    }
    if (!this.m_challengerPending)
      return;
    BnetPlayerChange change = changelist.FindChange(this.m_challengerId);
    if (change == null)
      return;
    BnetPlayer player = change.GetPlayer();
    if (!player.IsDisplayable())
      return;
    this.m_challenger = player;
    this.m_challengerPending = false;
    this.FireChangedEvent(FriendChallengeEvent.I_RECEIVED_CHALLENGE, this.m_challenger);
  }

  private void OnFriendsChanged(BnetFriendChangelist changelist, object userData)
  {
    if (!this.HasChallenge())
      return;
    List<BnetPlayer> removedFriends = changelist.GetRemovedFriends();
    if (removedFriends == null)
      return;
    BnetPlayer opponent = this.GetOpponent(BnetPresenceMgr.Get().GetMyPlayer());
    if (opponent == null)
      return;
    using (List<BnetPlayer>.Enumerator enumerator = removedFriends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current == opponent)
        {
          this.RevertTavernBrawlStatus();
          this.CleanUpChallengeData(true);
          this.FireChangedEvent(FriendChallengeEvent.OPPONENT_REMOVED_FROM_FRIENDS, opponent);
          break;
        }
      }
    }
  }

  private void OnNearbyPlayersChanged(BnetNearbyPlayerChangelist changelist, object userData)
  {
    if (!this.HasChallenge())
      return;
    List<BnetPlayer> removedPlayers = changelist.GetRemovedPlayers();
    if (removedPlayers == null)
      return;
    BnetPlayer opponent = this.GetOpponent(BnetPresenceMgr.Get().GetMyPlayer());
    if (opponent == null)
      return;
    using (List<BnetPlayer>.Enumerator enumerator = removedPlayers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current == opponent)
        {
          this.CleanUpChallengeData(true);
          this.FireChangedEvent(FriendChallengeEvent.OPPONENT_CANCELED_CHALLENGE, opponent);
          break;
        }
      }
    }
  }

  private void OnBnetEventOccurred(BattleNet.BnetEvent bnetEvent, object userData)
  {
    if (bnetEvent != BattleNet.BnetEvent.Disconnected)
      return;
    if ((UnityEngine.Object) this.m_challengeDialog != (UnityEngine.Object) null)
    {
      this.m_challengeDialog.Hide();
      this.m_challengeDialog = (DialogBase) null;
    }
    this.CleanUpChallengeData(true);
  }

  private void OnChallengeChanged(FriendChallengeEvent challengeEvent, BnetPlayer player, object userData)
  {
    switch (challengeEvent)
    {
      case FriendChallengeEvent.I_SENT_CHALLENGE:
        this.ShowISentChallengeDialog(player);
        break;
      case FriendChallengeEvent.OPPONENT_ACCEPTED_CHALLENGE:
        this.StartChallengeProcess();
        break;
      case FriendChallengeEvent.OPPONENT_DECLINED_CHALLENGE:
        this.ShowOpponentDeclinedChallengeDialog(player);
        break;
      case FriendChallengeEvent.I_RECEIVED_CHALLENGE:
        if (!this.CanPromptReceivedChallenge())
          break;
        if (this.IsChallengeTavernBrawl())
          PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TAVERN_BRAWL_FRIENDLY_WAITING);
        this.ShowIReceivedChallengeDialog(player);
        break;
      case FriendChallengeEvent.I_ACCEPTED_CHALLENGE:
        this.StartChallengeProcess();
        break;
      case FriendChallengeEvent.OPPONENT_RESCINDED_CHALLENGE:
        this.ShowOpponentCanceledChallengeDialog(player);
        break;
      case FriendChallengeEvent.OPPONENT_CANCELED_CHALLENGE:
        this.ShowOpponentCanceledChallengeDialog(player);
        break;
      case FriendChallengeEvent.OPPONENT_REMOVED_FROM_FRIENDS:
        this.ShowOpponentRemovedFromFriendsDialog(player);
        break;
    }
  }

  private bool CanPromptReceivedChallenge()
  {
    bool flag = !UserAttentionManager.CanShowAttentionGrabber("FriendlyChallengeMgr.CanPromptReceivedChallenge");
    if (!flag)
    {
      if (GameMgr.Get().IsFindingGame())
        flag = true;
      else if (RankMgr.Get().AmILegendRankPlayer)
        flag = SceneMgr.Get().IsModeRequested(SceneMgr.Mode.TOURNAMENT);
    }
    if (flag)
    {
      BnetParty.SetPartyAttributeLong((PartyId) this.m_partyId, "WTCG.Friendly.DeclineReason", 6L);
      this.DeclineChallenge();
      return false;
    }
    if (this.IsChallengeTavernBrawl())
    {
      if (!TavernBrawlManager.Get().HasUnlockedTavernBrawl)
      {
        BnetParty.SetPartyAttributeLong((PartyId) this.m_partyId, "WTCG.Friendly.DeclineReason", 5L);
        this.DeclineChallenge();
        return false;
      }
      TavernBrawlManager.Get().EnsureAllDataReady(new TavernBrawlManager.CallbackEnsureServerDataReady(this.TavernBrawl_ReceivedChallenge_OnEnsureServerDataReady));
      return false;
    }
    if (!CollectionManager.Get().AreAllDeckContentsReady())
    {
      CollectionManager.Get().RequestDeckContentsForDecksWithoutContentsLoaded(new CollectionManager.DelOnAllDeckContents(this.CanPromptReceivedChallenge_OnDeckContentsLoaded));
      return false;
    }
    if (this.IsChallengeStandardDuel() && !CollectionManager.Get().AccountHasValidStandardDeck())
    {
      BnetParty.SetPartyAttributeLong((PartyId) this.m_partyId, "WTCG.Friendly.DeclineReason", 3L);
      this.DeclineChallenge();
      return false;
    }
    if (this.IsChallengeWildDuel())
    {
      if (!CollectionManager.Get().ShouldAccountSeeStandardWild())
      {
        BnetParty.SetPartyAttributeLong((PartyId) this.m_partyId, "WTCG.Friendly.DeclineReason", 7L);
        this.DeclineChallenge();
        return false;
      }
      if (!CollectionManager.Get().AccountHasAnyValidDeck())
      {
        BnetParty.SetPartyAttributeLong((PartyId) this.m_partyId, "WTCG.Friendly.DeclineReason", 2L);
        this.DeclineChallenge();
        return false;
      }
    }
    return true;
  }

  private void CanPromptReceivedChallenge_OnDeckContentsLoaded()
  {
    if (!this.DidReceiveChallenge() || !this.CanPromptReceivedChallenge())
      return;
    this.ShowIReceivedChallengeDialog(this.m_challenger);
  }

  private void TavernBrawl_ReceivedChallenge_OnEnsureServerDataReady()
  {
    TavernBrawlMission tavernBrawlMission = TavernBrawlManager.Get().CurrentMission();
    FriendChallengeMgr.DeclineReason? nullable = new FriendChallengeMgr.DeclineReason?();
    if (tavernBrawlMission == null)
      nullable = new FriendChallengeMgr.DeclineReason?(FriendChallengeMgr.DeclineReason.None);
    if (tavernBrawlMission != null && tavernBrawlMission.canCreateDeck && !TavernBrawlManager.Get().HasValidDeck())
      nullable = new FriendChallengeMgr.DeclineReason?(FriendChallengeMgr.DeclineReason.TavernBrawlNoValidDeck);
    if (nullable.HasValue)
    {
      BnetParty.SetPartyAttributeLong((PartyId) this.m_partyId, "WTCG.Friendly.DeclineReason", (long) nullable.Value);
      this.DeclineChallenge();
    }
    else
    {
      if (this.IsChallengeTavernBrawl())
        PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TAVERN_BRAWL_FRIENDLY_WAITING);
      this.ShowIReceivedChallengeDialog(this.m_challenger);
    }
  }

  private bool RevertTavernBrawlStatus()
  {
    if (!this.IsChallengeTavernBrawl() || PresenceMgr.Get().CurrentStatus != PresenceStatus.TAVERN_BRAWL_FRIENDLY_WAITING)
      return false;
    PresenceMgr.Get().SetPrevStatus();
    return true;
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    this.UpdateMyAvailability();
    switch (eventData.m_state)
    {
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
        if (this.DidSendChallenge())
        {
          BnetParty.SetPartyAttributeLong((PartyId) this.m_partyId, "error", (long) GameMgr.Get().GetLastEnterGameError());
          BnetParty.SetPartyAttributeString((PartyId) this.m_partyId, "s1", "deck");
        }
        else if (this.DidReceiveChallenge())
          BnetParty.SetPartyAttributeString((PartyId) this.m_partyId, "s2", "deck");
        switch (SceneMgr.Get().GetMode())
        {
          case SceneMgr.Mode.FRIENDLY:
          case SceneMgr.Mode.TAVERN_BRAWL:
            break;
          default:
            this.CancelChallenge();
            break;
        }
    }
    return false;
  }

  private void WillReset()
  {
    this.CleanUpChallengeData(false);
    if (!((UnityEngine.Object) this.m_challengeDialog != (UnityEngine.Object) null))
      return;
    this.m_challengeDialog.Hide();
    this.m_challengeDialog = (DialogBase) null;
  }

  private void ShowISentChallengeDialog(BnetPlayer challengee)
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
      m_text = GameStrings.Format("GLOBAL_FRIEND_CHALLENGE_OPPONENT_WAITING_RESPONSE", (object) FriendUtils.GetUniqueName(challengee)),
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.NONE,
      m_responseCallback = new AlertPopup.ResponseCallback(this.OnChallengeSentDialogResponse),
      m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI)
    }, new DialogManager.DialogProcessCallback(this.OnChallengeSentDialogProcessed));
  }

  private void ShowOpponentDeclinedChallengeDialog(BnetPlayer challengee)
  {
    if ((UnityEngine.Object) this.m_challengeDialog != (UnityEngine.Object) null)
    {
      this.m_challengeDialog.Hide();
      this.m_challengeDialog = (DialogBase) null;
    }
    if (this.m_hasSeenDeclinedReason)
      return;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
      m_text = GameStrings.Format("GLOBAL_FRIEND_CHALLENGE_OPPONENT_DECLINED", (object) FriendUtils.GetUniqueName(challengee)),
      m_alertTextAlignment = UberText.AlignmentOptions.Center,
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  private void ShowIReceivedChallengeDialog(BnetPlayer challenger)
  {
    if ((UnityEngine.Object) this.m_challengeDialog != (UnityEngine.Object) null)
    {
      this.m_challengeDialog.Hide();
      this.m_challengeDialog = (DialogBase) null;
    }
    DialogManager.Get().ShowFriendlyChallenge(this.m_challengeFormatType, challenger, this.IsChallengeTavernBrawl(), new FriendlyChallengeDialog.ResponseCallback(this.OnChallengeReceivedDialogResponse), new DialogManager.DialogProcessCallback(this.OnChallengeReceivedDialogProcessed));
  }

  private void ShowOpponentCanceledChallengeDialog(BnetPlayer otherPlayer)
  {
    if ((UnityEngine.Object) this.m_challengeDialog != (UnityEngine.Object) null)
    {
      this.m_challengeDialog.Hide();
      this.m_challengeDialog = (DialogBase) null;
    }
    if ((UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null && SceneMgr.Get().IsInGame() && (GameState.Get() != null && !GameState.Get().IsGameOverNowOrPending()) && (GameState.Get().GetOpposingSidePlayer() != null && GameState.Get().GetOpposingSidePlayer().GetBnetPlayer() != null && (otherPlayer != null && (BnetEntityId) otherPlayer.GetHearthstoneGameAccountId() == (BnetEntityId) GameState.Get().GetOpposingSidePlayer().GetBnetPlayer().GetHearthstoneGameAccountId())))
      return;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
      m_text = GameStrings.Format("GLOBAL_FRIEND_CHALLENGE_OPPONENT_CANCELED", (object) FriendUtils.GetUniqueName(otherPlayer)),
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  private void ShowOpponentRemovedFromFriendsDialog(BnetPlayer otherPlayer)
  {
    if ((UnityEngine.Object) this.m_challengeDialog != (UnityEngine.Object) null)
    {
      this.m_challengeDialog.Hide();
      this.m_challengeDialog = (DialogBase) null;
    }
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_FRIEND_CHALLENGE_HEADER"),
      m_text = GameStrings.Format("GLOBAL_FRIEND_CHALLENGE_OPPONENT_FRIEND_REMOVED", (object) FriendUtils.GetUniqueName(otherPlayer)),
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  private bool OnChallengeSentDialogProcessed(DialogBase dialog, object userData)
  {
    if (!this.DidSendChallenge() || this.m_challengeeAccepted)
      return false;
    this.m_challengeDialog = dialog;
    this.UpdateChallengeSentDialog();
    return true;
  }

  private void UpdateChallengeSentDialog()
  {
    if (this.m_partyId == (BnetEntityId) null || (UnityEngine.Object) this.m_challengeDialog == (UnityEngine.Object) null)
      return;
    AlertPopup challengeDialog = (AlertPopup) this.m_challengeDialog;
    AlertPopup.PopupInfo info = challengeDialog.GetInfo();
    if (info == null)
      return;
    info.m_responseDisplay = AlertPopup.ResponseDisplay.CANCEL;
    challengeDialog.UpdateInfo(info);
  }

  private void OnChallengeSentDialogResponse(AlertPopup.Response response, object userData)
  {
    this.m_challengeDialog = (DialogBase) null;
    this.RescindChallenge();
  }

  private bool OnChallengeReceivedDialogProcessed(DialogBase dialog, object userData)
  {
    if (!this.DidReceiveChallenge())
      return false;
    this.m_challengeDialog = dialog;
    PartyQuestInfo partyQuestInfo = this.GetPartyQuestInfo();
    if (partyQuestInfo != null)
      ((FriendlyChallengeDialog) dialog).SetQuestInfo(partyQuestInfo);
    return true;
  }

  private void OnChallengeReceivedDialogResponse(bool accept)
  {
    this.m_challengeDialog = (DialogBase) null;
    if (accept)
      this.AcceptChallenge();
    else
      this.DeclineChallenge();
  }

  private void HandleJoinedParty(BnetEntityId partyId, BnetGameAccountId otherMemberId)
  {
    this.m_partyId = partyId;
    this.m_challengeePartyId = partyId;
    this.m_challengerId = otherMemberId;
    this.m_challenger = BnetUtils.GetPlayer(this.m_challengerId);
    this.m_challengee = BnetPresenceMgr.Get().GetMyPlayer();
    this.m_hasSeenDeclinedReason = false;
    if (this.m_challenger == null || !this.m_challenger.IsDisplayable())
    {
      this.m_challengerPending = true;
      this.UpdateMyAvailability();
    }
    else
    {
      this.UpdateMyAvailability();
      this.FireChangedEvent(FriendChallengeEvent.I_RECEIVED_CHALLENGE, this.m_challenger);
    }
  }

  private void ResolveChallengeConflict()
  {
    if (this.m_partyId.GetLo() < this.m_challengeePartyId.GetLo())
    {
      this.DeclineFriendChallenge_Internal(this.m_challengeePartyId);
      this.m_challengeePartyId = this.m_partyId;
    }
    else
    {
      if (this.m_challengee == null)
        return;
      this.HandleJoinedParty(this.m_challengeePartyId, this.m_challengee.GetHearthstoneGameAccount().GetId());
    }
  }

  public void UpdateMyAvailability()
  {
    bool flag = !this.HasAvailabilityBlocker();
    BnetPresenceMgr.Get().SetGameField(1U, flag);
    BnetNearbyPlayerMgr.Get().SetAvailability(flag);
  }

  private bool HasAvailabilityBlocker()
  {
    return this.GetAvailabilityBlockerReason() != FriendChallengeMgr.FriendListAvailabilityBlockerReasons.NONE;
  }

  private FriendChallengeMgr.FriendListAvailabilityBlockerReasons GetAvailabilityBlockerReason()
  {
    if (!this.m_netCacheReady)
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.NETCACHE_NOT_READY;
    if (!this.m_myPlayerReady)
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.MY_PLAYER_NOT_READY;
    if (this.HasChallenge())
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.HAS_EXISTING_CHALLENGE;
    if (SpectatorManager.Get().IsInSpectatorMode())
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.SPECTATING_GAME;
    if (GameMgr.Get().IsFindingGame() && (GameMgr.Get().GetNextGameType() != GameType.GT_RANKED || !RankMgr.Get().AmILegendRank(GameMgr.Get().GetNextFormatType() == FormatType.FT_WILD)))
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.FINDING_GAME;
    if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.FATAL_ERROR))
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.HAS_FATAL_ERROR;
    if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.LOGIN))
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.LOGGING_IN;
    if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.STARTUP))
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.STARTING_UP;
    if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.GAMEPLAY))
    {
      if (GameMgr.Get().IsSpectator() || GameMgr.Get().IsNextSpectator())
        return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.SPECTATING_GAME;
      return GameMgr.Get().IsAI() || GameMgr.Get().IsNextAI() ? FriendChallengeMgr.FriendListAvailabilityBlockerReasons.PLAYING_AI_GAME : FriendChallengeMgr.FriendListAvailabilityBlockerReasons.PLAYING_NON_AI_GAME;
    }
    if (!GameUtils.AreAllTutorialsComplete())
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.TUTORIALS_INCOMPLETE;
    if (ShownUIMgr.Get().GetShownUI() == ShownUIMgr.UI_WINDOW.GENERAL_STORE || ShownUIMgr.Get().GetShownUI() == ShownUIMgr.UI_WINDOW.ARENA_STORE)
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.STORE_IS_SHOWN;
    if ((UnityEngine.Object) TavernBrawlDisplay.Get() != (UnityEngine.Object) null && TavernBrawlDisplay.Get().IsInDeckEditMode())
      return FriendChallengeMgr.FriendListAvailabilityBlockerReasons.EDITING_TAVERN_BRAWL;
    return ReturningPlayerMgr.Get().IsInReturningPlayerModeAndOnRails ? FriendChallengeMgr.FriendListAvailabilityBlockerReasons.IN_RETURNING_PLAYER_EXPERIENCE : FriendChallengeMgr.FriendListAvailabilityBlockerReasons.NONE;
  }

  private void FireChangedEvent(FriendChallengeEvent challengeEvent, BnetPlayer player)
  {
    foreach (FriendChallengeMgr.ChangedListener changedListener in this.m_changedListeners.ToArray())
      changedListener.Fire(challengeEvent, player);
  }

  private void CleanUpChallengeData(bool updateAvailability = true)
  {
    this.m_partyId = (BnetEntityId) null;
    this.m_challengeePartyId = (BnetEntityId) null;
    this.m_challengerId = (BnetGameAccountId) null;
    this.m_challengerPending = false;
    this.m_challenger = (BnetPlayer) null;
    this.m_challengerDeckSelected = false;
    this.m_challengerInGameState = false;
    this.m_challengee = (BnetPlayer) null;
    this.m_challengeeAccepted = false;
    this.m_challengeeDeckSelected = false;
    this.m_challengeeInGameState = false;
    this.m_scenarioId = 2;
    this.m_isChallengeTavernBrawl = false;
    this.m_challengeFormatType = FormatType.FT_UNKNOWN;
    this.m_updatePartyQuestInfoOnGameplaySceneUnload = false;
    if (!updateAvailability)
      return;
    this.UpdateMyAvailability();
  }

  private void StartChallengeProcess()
  {
    if ((UnityEngine.Object) this.m_challengeDialog != (UnityEngine.Object) null)
    {
      this.m_challengeDialog.Hide();
      this.m_challengeDialog = (DialogBase) null;
    }
    GameMgr.Get().SetPendingAutoConcede(true);
    if (CollectionManager.Get().IsInEditMode())
    {
      CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
      if (taggedDeck != null)
        taggedDeck.SendChanges();
    }
    if (this.IsChallengeTavernBrawl() && !TavernBrawlManager.Get().SelectHeroBeforeMission())
    {
      if (TavernBrawlManager.Get().CurrentMission().canCreateDeck)
      {
        if (TavernBrawlManager.Get().HasValidDeck())
          this.SelectDeck(TavernBrawlManager.Get().CurrentDeck().ID);
        else
          Debug.LogError((object) "Attempting to start a Tavern Brawl challenge without a valid deck!  How did this happen?");
      }
      else
        this.SkipDeckSelection();
    }
    else
    {
      if (!this.m_isChallengeTavernBrawl)
        Options.Get().SetBool(Option.IN_WILD_MODE, this.m_challengeFormatType == FormatType.FT_WILD);
      Navigation.Clear();
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.FRIENDLY);
    }
  }

  private enum FriendListAvailabilityBlockerReasons
  {
    NONE,
    NETCACHE_NOT_READY,
    MY_PLAYER_NOT_READY,
    HAS_EXISTING_CHALLENGE,
    FINDING_GAME,
    HAS_FATAL_ERROR,
    LOGGING_IN,
    STARTING_UP,
    PLAYING_NON_AI_GAME,
    TUTORIALS_INCOMPLETE,
    STORE_IS_SHOWN,
    PLAYING_AI_GAME,
    SPECTATING_GAME,
    EDITING_TAVERN_BRAWL,
    IN_RETURNING_PLAYER_EXPERIENCE,
  }

  public enum DeclineReason
  {
    None,
    UserDeclined,
    NoValidDeck,
    StandardNoValidDeck,
    TavernBrawlNoValidDeck,
    TavernBrawlNotUnlocked,
    UserIsBusy,
    NotSeenWild,
  }

  private class ChangedListener : EventListener<FriendChallengeMgr.ChangedCallback>
  {
    public void Fire(FriendChallengeEvent challengeEvent, BnetPlayer player)
    {
      this.m_callback(challengeEvent, player, this.m_userData);
    }
  }

  public delegate void ChangedCallback(FriendChallengeEvent challengeEvent, BnetPlayer player, object userData);
}
