﻿// Decompiled with JetBrains decompiler
// Type: TwistingNetherDrainInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class TwistingNetherDrainInfo
{
  public float m_DurationMin = 1.5f;
  public float m_DurationMax = 2f;
  public iTween.EaseType m_EaseType = iTween.EaseType.easeInOutCubic;
  public float m_DelayMin;
  public float m_DelayMax;
}
