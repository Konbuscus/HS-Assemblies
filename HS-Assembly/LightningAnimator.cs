﻿// Decompiled with JetBrains decompiler
// Type: LightningAnimator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class LightningAnimator : MonoBehaviour
{
  public bool m_SetAlphaToZeroOnStart = true;
  public string m_MatFrameProperty = "_Frame";
  public float m_FrameTime = 0.01f;
  public Vector3 m_SourceMinRotation = new Vector3(0.0f, -10f, 0.0f);
  public Vector3 m_SourceMaxRotation = new Vector3(0.0f, 10f, 0.0f);
  public Vector3 m_TargetMinRotation = new Vector3(0.0f, -20f, 0.0f);
  public Vector3 m_TargetMaxRotation = new Vector3(0.0f, 20f, 0.0f);
  public bool m_StartOnEnable;
  public float m_StartDelayMin;
  public float m_StartDelayMax;
  public List<int> m_FrameList;
  public Transform m_SourceJount;
  public Transform m_TargetJoint;
  private Material m_material;
  private float m_matGlowIntensity;

  private void Start()
  {
    this.m_material = this.GetComponent<Renderer>().material;
    if ((Object) this.m_material == (Object) null)
      this.enabled = false;
    if (this.m_SetAlphaToZeroOnStart)
    {
      Color color = this.m_material.color;
      color.a = 0.0f;
      this.m_material.color = color;
    }
    if (!this.m_material.HasProperty("_GlowIntensity"))
      return;
    this.m_matGlowIntensity = this.m_material.GetFloat("_GlowIntensity");
  }

  private void OnEnable()
  {
    if (!this.m_StartOnEnable)
      return;
    this.StartAnimation();
  }

  public void StartAnimation()
  {
    this.StartCoroutine(this.AnimateMaterial());
  }

  [DebuggerHidden]
  private IEnumerator AnimateMaterial()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LightningAnimator.\u003CAnimateMaterial\u003Ec__Iterator336() { \u003C\u003Ef__this = this };
  }

  private void RandomJointRotation()
  {
    if ((Object) this.m_SourceJount != (Object) null)
      this.m_SourceJount.Rotate(Vector3.Lerp(this.m_SourceMinRotation, this.m_SourceMaxRotation, Random.value));
    if (!((Object) this.m_TargetJoint != (Object) null))
      return;
    this.m_TargetJoint.Rotate(Vector3.Lerp(this.m_TargetMinRotation, this.m_TargetMaxRotation, Random.value));
  }
}
