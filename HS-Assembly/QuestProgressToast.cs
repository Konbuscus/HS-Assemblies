﻿// Decompiled with JetBrains decompiler
// Type: QuestProgressToast
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class QuestProgressToast : GameToast
{
  public UberText m_questTitle;
  public UberText m_questDescription;
  public UberText m_questProgressCount;
  public GameObject m_questProgressCountBg;
  public GameObject m_background;

  private void Awake()
  {
    this.m_intensityMaterials.Add(this.m_questProgressCountBg.GetComponent<Renderer>().material);
    this.m_intensityMaterials.Add(this.m_background.GetComponent<Renderer>().material);
  }

  public void UpdateDisplay(string title, string description, int progress, int maxProgress)
  {
    if (maxProgress > 1)
    {
      this.m_questProgressCountBg.SetActive(true);
      this.m_questProgressCount.Text = GameStrings.Format("GLOBAL_QUEST_PROGRESS_COUNT", (object) progress, (object) maxProgress);
    }
    else
      this.m_questProgressCountBg.SetActive(false);
    this.m_questTitle.Text = title;
    this.m_questDescription.Text = description;
  }
}
