﻿// Decompiled with JetBrains decompiler
// Type: AchieveManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using PegasusUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AchieveManager
{
  private static readonly long TIMED_ACHIEVE_VALIDATION_DELAY_TICKS = 600000000;
  private static readonly long CHECK_LICENSE_ADDED_ACHIEVE_DELAY_TICKS = 3000000000;
  private static readonly long TIMED_AND_LICENSE_ACHIEVE_CHECK_DELAY_TICKS = Math.Min(AchieveManager.TIMED_ACHIEVE_VALIDATION_DELAY_TICKS, AchieveManager.CHECK_LICENSE_ADDED_ACHIEVE_DELAY_TICKS);
  private static AchieveManager s_instance = (AchieveManager) null;
  private Map<int, Achievement> m_achievements = new Map<int, Achievement>();
  private HashSet<int> m_achieveValidationsToRequest = new HashSet<int>();
  private HashSet<int> m_achieveValidationsRequested = new HashSet<int>();
  private Map<int, long> m_lastEventTimingValidationByAchieve = new Map<int, long>();
  private Map<int, long> m_lastCheckLicenseAddedByAchieve = new Map<int, long>();
  private List<int> m_achieveNotificationsToQueue = new List<int>();
  private List<AchievementNotification> m_blockedAchievementNotifications = new List<AchievementNotification>();
  private List<AchieveManager.AchieveCanceledListener> m_achieveCanceledListeners = new List<AchieveManager.AchieveCanceledListener>();
  private List<AchieveManager.AchievesUpdatedListener> m_achievesUpdatedListeners = new List<AchieveManager.AchievesUpdatedListener>();
  private List<AchieveManager.LicenseAddedAchievesUpdatedListener> m_licenseAddedAchievesUpdatedListeners = new List<AchieveManager.LicenseAddedAchievesUpdatedListener>();
  private bool m_allNetAchievesReceived;
  private int m_numEventResponsesNeeded;
  private bool m_disableCancelButtonUntilServerReturns;
  private long m_lastEventTimingAndLicenseAchieveCheck;
  private bool m_queueNotifications;

  private AchieveManager()
  {
    this.LoadAchievesFromDBF();
  }

  public static AchieveManager Get()
  {
    return AchieveManager.s_instance;
  }

  public static void Init()
  {
    if (AchieveManager.s_instance != null)
      return;
    AchieveManager.s_instance = new AchieveManager();
    Network network = Network.Get();
    network.RegisterNetHandler((object) Achieves.PacketID.ID, new Network.NetHandler(AchieveManager.s_instance.OnAchieves), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) CancelQuestResponse.PacketID.ID, new Network.NetHandler(AchieveManager.s_instance.OnQuestCanceled), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) ValidateAchieveResponse.PacketID.ID, new Network.NetHandler(AchieveManager.s_instance.OnAchieveValidated), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) TriggerEventResponse.PacketID.ID, new Network.NetHandler(AchieveManager.s_instance.OnEventTriggered), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.AccountLicenseAchieveResponse.PacketID.ID, new Network.NetHandler(AchieveManager.s_instance.OnAccountLicenseAchieveResponse), (Network.TimeoutHandler) null);
    NetCache.Get().RegisterNewNoticesListener(new NetCache.DelNewNoticesListener(AchieveManager.s_instance.OnNewNotices));
    ApplicationMgr.Get().WillReset += new Action(AchieveManager.s_instance.WillReset);
  }

  public static void InitRequests()
  {
    Network.RequestAchieves();
  }

  public static bool IsPredicateTrue(Achievement.Predicate predicate)
  {
    return predicate == Achievement.Predicate.CAN_SEE_WILD && CollectionManager.Get().ShouldAccountSeeStandardWild();
  }

  public void InitAchieveManager()
  {
    AchieveManager.s_instance.WillReset();
  }

  public bool IsReady()
  {
    return this.m_allNetAchievesReceived && this.m_numEventResponsesNeeded <= 0 && (this.m_achieveValidationsToRequest.Count <= 0 && this.m_achieveValidationsRequested.Count <= 0) && NetCache.Get().GetNetObject<NetCache.NetCacheProfileNotices>() != null;
  }

  public bool RegisterAchievesUpdatedListener(AchieveManager.AchievesUpdatedCallback callback, object userData = null)
  {
    if (callback == null)
      return false;
    AchieveManager.AchievesUpdatedListener achievesUpdatedListener = new AchieveManager.AchievesUpdatedListener();
    achievesUpdatedListener.SetCallback(callback);
    achievesUpdatedListener.SetUserData(userData);
    if (this.m_achievesUpdatedListeners.Contains(achievesUpdatedListener))
      return false;
    this.m_achievesUpdatedListeners.Add(achievesUpdatedListener);
    return true;
  }

  public bool RemoveAchievesUpdatedListener(AchieveManager.AchievesUpdatedCallback callback)
  {
    return this.RemoveAchievesUpdatedListener(callback, (object) null);
  }

  public bool RemoveAchievesUpdatedListener(AchieveManager.AchievesUpdatedCallback callback, object userData)
  {
    if (callback == null)
      return false;
    AchieveManager.AchievesUpdatedListener achievesUpdatedListener = new AchieveManager.AchievesUpdatedListener();
    achievesUpdatedListener.SetCallback(callback);
    achievesUpdatedListener.SetUserData(userData);
    if (!this.m_achievesUpdatedListeners.Contains(achievesUpdatedListener))
      return false;
    this.m_achievesUpdatedListeners.Remove(achievesUpdatedListener);
    return true;
  }

  public List<Achievement> GetNewCompletedAchieves()
  {
    return this.m_achievements.Values.ToList<Achievement>().FindAll((Predicate<Achievement>) (obj =>
    {
      if (!obj.IsCompleted() || obj.IsInternal() || obj.RewardTiming == RewardVisualTiming.NEVER)
        return false;
      switch (obj.AchieveType)
      {
        case Achievement.AchType.UNLOCK_HERO:
          return false;
        case Achievement.AchType.UNLOCK_GOLDEN_HERO:
          return false;
        case Achievement.AchType.DAILY_REPEATABLE:
          return false;
        default:
          return obj.AcknowledgedProgress < obj.Progress;
      }
    }));
  }

  private static bool IsActiveQuest(Achievement obj, bool onlyNewlyActive)
  {
    if (!obj.Active || !obj.CanShowInQuestLog)
      return false;
    if (onlyNewlyActive)
      return obj.IsNewlyActive();
    return true;
  }

  private static bool IsAutoDestroyQuest(Achievement obj)
  {
    if (!obj.CanShowInQuestLog)
      return false;
    return obj.AutoDestroy;
  }

  private static bool IsDialogQuest(Achievement obj)
  {
    if (!obj.CanShowInQuestLog)
      return false;
    return obj.QuestDialogId != 0;
  }

  public List<Achievement> GetActiveQuests(bool onlyNewlyActive = false)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_achievements.Values.Where<Achievement>(new Func<Achievement, bool>(new AchieveManager.\u003CGetActiveQuests\u003Ec__AnonStorey40A() { onlyNewlyActive = onlyNewlyActive }.\u003C\u003Em__23A)).ToList<Achievement>();
  }

  public bool HasQuestsToShow(bool onlyNewlyActive = false)
  {
    if (!this.HasActiveQuests(onlyNewlyActive))
      return this.HasActiveAutoDestroyQuests();
    return true;
  }

  public bool HasActiveQuests(bool onlyNewlyActive = false)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_achievements.Any<KeyValuePair<int, Achievement>>(new Func<KeyValuePair<int, Achievement>, bool>(new AchieveManager.\u003CHasActiveQuests\u003Ec__AnonStorey40B() { onlyNewlyActive = onlyNewlyActive }.\u003C\u003Em__23B));
  }

  public bool HasActiveAutoDestroyQuests()
  {
    return this.m_achievements.Any<KeyValuePair<int, Achievement>>((Func<KeyValuePair<int, Achievement>, bool>) (kv =>
    {
      if (AchieveManager.IsActiveQuest(kv.Value, false))
        return AchieveManager.IsAutoDestroyQuest(kv.Value);
      return false;
    }));
  }

  public bool HasActiveDialogQuests()
  {
    return this.m_achievements.Any<KeyValuePair<int, Achievement>>((Func<KeyValuePair<int, Achievement>, bool>) (kv =>
    {
      if (AchieveManager.IsActiveQuest(kv.Value, false))
        return AchieveManager.IsDialogQuest(kv.Value);
      return false;
    }));
  }

  public bool HasActiveQuestId(AchievementDbId id)
  {
    using (List<Achievement>.Enumerator enumerator = this.GetActiveQuests(false).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if ((AchievementDbId) enumerator.Current.ID == id)
          return true;
      }
    }
    return false;
  }

  public List<Achievement> GetNewlyProgressedQuests()
  {
    return AchieveManager.Get().GetActiveQuests(false).FindAll((Predicate<Achievement>) (obj => obj.Progress > obj.AcknowledgedProgress));
  }

  public bool HasUnlockedFeature(Achievement.UnlockableFeature feature)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AchieveManager.\u003CHasUnlockedFeature\u003Ec__AnonStorey40C featureCAnonStorey40C = new AchieveManager.\u003CHasUnlockedFeature\u003Ec__AnonStorey40C();
    // ISSUE: reference to a compiler-generated field
    featureCAnonStorey40C.feature = feature;
    // ISSUE: reference to a compiler-generated field
    if (DemoMgr.Get().ArenaIs1WinMode() && featureCAnonStorey40C.feature == Achievement.UnlockableFeature.FORGE)
      return true;
    // ISSUE: reference to a compiler-generated method
    Achievement achievement = this.m_achievements.Values.ToList<Achievement>().Find(new Predicate<Achievement>(featureCAnonStorey40C.\u003C\u003Em__23F));
    if (achievement != null)
      return achievement.IsCompleted();
    // ISSUE: reference to a compiler-generated field
    Debug.LogWarning((object) string.Format("AchieveManager.HasUnlockedFeature(): could not find achieve that unlocks feature {0}", (object) featureCAnonStorey40C.feature));
    return false;
  }

  public Achievement GetAchievement(int achieveID)
  {
    if (!this.m_achievements.ContainsKey(achieveID))
      return (Achievement) null;
    return this.m_achievements[achieveID];
  }

  public int GetNumAchievesInGroup(Achievement.AchType achieveGroup)
  {
    return this.GetAchievesInGroup(achieveGroup).Count;
  }

  public List<Achievement> GetCompletedAchieves()
  {
    return new List<Achievement>((IEnumerable<Achievement>) this.m_achievements.Values).FindAll((Predicate<Achievement>) (obj => obj.IsCompleted()));
  }

  public List<Achievement> GetAchievesInGroup(Achievement.AchType achieveGroup)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return new List<Achievement>((IEnumerable<Achievement>) this.m_achievements.Values).FindAll(new Predicate<Achievement>(new AchieveManager.\u003CGetAchievesInGroup\u003Ec__AnonStorey40D() { achieveGroup = achieveGroup }.\u003C\u003Em__241));
  }

  public List<Achievement> GetAchievesInGroup(Achievement.AchType achieveGroup, bool isComplete)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.GetAchievesInGroup(achieveGroup).FindAll(new Predicate<Achievement>(new AchieveManager.\u003CGetAchievesInGroup\u003Ec__AnonStorey40E() { isComplete = isComplete }.\u003C\u003Em__242));
  }

  public List<Achievement> GetAchievesForAdventureWing(int wingID)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return new List<Achievement>((IEnumerable<Achievement>) this.m_achievements.Values).FindAll(new Predicate<Achievement>(new AchieveManager.\u003CGetAchievesForAdventureWing\u003Ec__AnonStorey40F() { wingID = wingID }.\u003C\u003Em__243));
  }

  public Achievement GetUnlockGoldenHeroAchievement(string heroCardID, TAG_PREMIUM premium)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.GetAchievesInGroup(Achievement.AchType.UNLOCK_GOLDEN_HERO).Find(new Predicate<Achievement>(new AchieveManager.\u003CGetUnlockGoldenHeroAchievement\u003Ec__AnonStorey410() { heroCardID = heroCardID, premium = premium }.\u003C\u003Em__244));
  }

  public bool HasActiveAchievesForEvent(SpecialEventType eventTrigger)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AchieveManager.\u003CHasActiveAchievesForEvent\u003Ec__AnonStorey411 eventCAnonStorey411 = new AchieveManager.\u003CHasActiveAchievesForEvent\u003Ec__AnonStorey411();
    // ISSUE: reference to a compiler-generated field
    eventCAnonStorey411.eventTrigger = eventTrigger;
    // ISSUE: reference to a compiler-generated field
    if (eventCAnonStorey411.eventTrigger == SpecialEventType.IGNORE)
      return false;
    // ISSUE: reference to a compiler-generated method
    return this.m_achievements.Values.ToList<Achievement>().FindAll(new Predicate<Achievement>(eventCAnonStorey411.\u003C\u003Em__245)).Count > 0;
  }

  public bool CanCancelQuest(int achieveID)
  {
    if (this.m_disableCancelButtonUntilServerReturns || !this.CanCancelQuestNow())
      return false;
    Achievement achievement = this.GetAchievement(achieveID);
    if (achievement == null || achievement.AchieveType != Achievement.AchType.DAILY_QUEST)
      return false;
    return achievement.Active;
  }

  public bool RegisterQuestCanceledListener(AchieveManager.AchieveCanceledCallback callback)
  {
    return this.RegisterQuestCanceledListener(callback, (object) null);
  }

  public bool RegisterQuestCanceledListener(AchieveManager.AchieveCanceledCallback callback, object userData)
  {
    AchieveManager.AchieveCanceledListener canceledListener = new AchieveManager.AchieveCanceledListener();
    canceledListener.SetCallback(callback);
    canceledListener.SetUserData(userData);
    if (this.m_achieveCanceledListeners.Contains(canceledListener))
      return false;
    this.m_achieveCanceledListeners.Add(canceledListener);
    return true;
  }

  public bool RemoveQuestCanceledListener(AchieveManager.AchieveCanceledCallback callback)
  {
    return this.RemoveQuestCanceledListener(callback, (object) null);
  }

  public bool RemoveQuestCanceledListener(AchieveManager.AchieveCanceledCallback callback, object userData)
  {
    AchieveManager.AchieveCanceledListener canceledListener = new AchieveManager.AchieveCanceledListener();
    canceledListener.SetCallback(callback);
    canceledListener.SetUserData(userData);
    return this.m_achieveCanceledListeners.Remove(canceledListener);
  }

  public void CancelQuest(int achieveID)
  {
    if (!this.CanCancelQuest(achieveID))
    {
      this.FireAchieveCanceledEvent(achieveID, false);
    }
    else
    {
      this.BlockAllNotifications();
      this.m_disableCancelButtonUntilServerReturns = true;
      Network.RequestCancelQuest(achieveID);
    }
  }

  public bool RegisterLicenseAddedAchievesUpdatedListener(AchieveManager.LicenseAddedAchievesUpdatedCallback callback)
  {
    return this.RegisterLicenseAddedAchievesUpdatedListener(callback, (object) null);
  }

  public bool RegisterLicenseAddedAchievesUpdatedListener(AchieveManager.LicenseAddedAchievesUpdatedCallback callback, object userData)
  {
    AchieveManager.LicenseAddedAchievesUpdatedListener achievesUpdatedListener = new AchieveManager.LicenseAddedAchievesUpdatedListener();
    achievesUpdatedListener.SetCallback(callback);
    achievesUpdatedListener.SetUserData(userData);
    if (this.m_licenseAddedAchievesUpdatedListeners.Contains(achievesUpdatedListener))
      return false;
    this.m_licenseAddedAchievesUpdatedListeners.Add(achievesUpdatedListener);
    return true;
  }

  public bool RemoveLicenseAddedAchievesUpdatedListener(AchieveManager.LicenseAddedAchievesUpdatedCallback callback)
  {
    return this.RemoveLicenseAddedAchievesUpdatedListener(callback, (object) null);
  }

  public bool RemoveLicenseAddedAchievesUpdatedListener(AchieveManager.LicenseAddedAchievesUpdatedCallback callback, object userData)
  {
    AchieveManager.LicenseAddedAchievesUpdatedListener achievesUpdatedListener = new AchieveManager.LicenseAddedAchievesUpdatedListener();
    achievesUpdatedListener.SetCallback(callback);
    achievesUpdatedListener.SetUserData(userData);
    return this.m_licenseAddedAchievesUpdatedListeners.Remove(achievesUpdatedListener);
  }

  public bool HasActiveLicenseAddedAchieves()
  {
    return this.GetActiveLicenseAddedAchieves().Count > 0;
  }

  public bool HasIncompletePurchaseAchieves()
  {
    return this.m_achievements.Values.ToList<Achievement>().FindAll((Predicate<Achievement>) (obj =>
    {
      if (obj.IsCompleted() || !obj.Enabled)
        return false;
      return obj.AchieveTrigger == Achievement.Trigger.PURCHASE;
    })).Count > 0;
  }

  public bool HasIncompleteDisenchantAchieves()
  {
    return this.m_achievements.Values.ToList<Achievement>().FindAll((Predicate<Achievement>) (obj =>
    {
      if (obj.IsCompleted() || !obj.Enabled)
        return false;
      return obj.AchieveTrigger == Achievement.Trigger.DISENCHANT;
    })).Count > 0;
  }

  public void NotifyOfClick(Achievement.ClickTriggerType clickType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    using (List<Achievement>.Enumerator enumerator = this.m_achievements.Values.ToList<Achievement>().FindAll(new Predicate<Achievement>(new AchieveManager.\u003CNotifyOfClick\u003Ec__AnonStorey412() { clickType = clickType }.\u003C\u003Em__248)).GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.m_achieveValidationsToRequest.Add(enumerator.Current.ID);
    }
    this.ValidateAchievesNow();
  }

  public void CompleteAutoDestroyAchieve(int achieveId)
  {
    using (List<Achievement>.Enumerator enumerator = this.m_achievements.Values.ToList<Achievement>().FindAll((Predicate<Achievement>) (obj =>
    {
      if (obj.IsCompleted() || !obj.Enabled || !obj.Active)
        return false;
      return obj.AchieveTrigger == Achievement.Trigger.DESTROYED;
    })).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        if (current.ID == achieveId)
          this.m_achieveValidationsToRequest.Add(current.ID);
      }
    }
    this.ValidateAchievesNow();
  }

  public void NotifyOfCardsGained(List<EntityDef> entityDefs, bool hasGolden)
  {
    HashSet<TAG_CARD_SET> tagCardSetSet = new HashSet<TAG_CARD_SET>();
    HashSet<TAG_RACE> tagRaceSet = new HashSet<TAG_RACE>();
    HashSet<Achievement> source1 = new HashSet<Achievement>();
    HashSet<Achievement> source2 = new HashSet<Achievement>();
    using (List<EntityDef>.Enumerator enumerator = entityDefs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EntityDef current = enumerator.Current;
        tagCardSetSet.Add(current.GetCardSet());
        tagRaceSet.Add(current.GetRace());
      }
    }
    using (Map<int, Achievement>.ValueCollection.Enumerator enumerator = this.m_achievements.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        if (!source1.Contains(current) && !source2.Contains(current) && !current.IsCompleted())
        {
          if ((current.AchieveTrigger == Achievement.Trigger.GAIN_CARD || hasGolden && current.AchieveTrigger == Achievement.Trigger.GAIN_GOLDEN_CARD) && current.RaceRequirement.HasValue && tagRaceSet.Contains(current.RaceRequirement.Value))
            source1.Add(current);
          else if (current.AchieveTrigger == Achievement.Trigger.COMPLETE_CARD_SET && tagCardSetSet.Contains(current.CardSetRequirement.Value))
            source2.Add(current);
        }
      }
    }
    this.AddAchievesToValidate(source1.ToList<Achievement>(), source2.ToList<Achievement>());
  }

  public void NotifyOfCardGained(EntityDef entityDef, TAG_PREMIUM premium, int totalCount)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AchieveManager.\u003CNotifyOfCardGained\u003Ec__AnonStorey413 gainedCAnonStorey413 = new AchieveManager.\u003CNotifyOfCardGained\u003Ec__AnonStorey413();
    // ISSUE: reference to a compiler-generated field
    gainedCAnonStorey413.premium = premium;
    // ISSUE: reference to a compiler-generated field
    gainedCAnonStorey413.entityDef = entityDef;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    Log.Achievements.Print("NotifyOfCardGained: " + (object) gainedCAnonStorey413.entityDef + " " + (object) gainedCAnonStorey413.premium + " " + (object) totalCount);
    // ISSUE: reference to a compiler-generated method
    // ISSUE: reference to a compiler-generated method
    this.AddAchievesToValidate(this.m_achievements.Values.ToList<Achievement>().FindAll(new Predicate<Achievement>(gainedCAnonStorey413.\u003C\u003Em__24A)), this.m_achievements.Values.ToList<Achievement>().FindAll(new Predicate<Achievement>(gainedCAnonStorey413.\u003C\u003Em__24B)));
  }

  public void NotifyOfPacksReadyToOpen(UnopenedPack unopenedPack)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    List<Achievement> all = this.m_achievements.Values.ToList<Achievement>().FindAll(new Predicate<Achievement>(new AchieveManager.\u003CNotifyOfPacksReadyToOpen\u003Ec__AnonStorey414() { unopenedPack = unopenedPack }.\u003C\u003Em__24C));
    if (all.Count <= 0)
      return;
    using (List<Achievement>.Enumerator enumerator = all.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.m_achieveValidationsToRequest.Add(enumerator.Current.ID);
    }
    this.ValidateAchievesNow();
  }

  public void Heartbeat()
  {
    this.CheckTimedEventsAndLicenses(DateTime.UtcNow);
  }

  public void ValidateAchievesNow()
  {
    if (this.m_achieveValidationsToRequest.Count == 0)
      return;
    this.m_achieveValidationsRequested.Union<int>((IEnumerable<int>) this.m_achieveValidationsToRequest);
    using (HashSet<int>.Enumerator enumerator = this.m_achieveValidationsToRequest.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Network.ValidateAchieve(enumerator.Current);
    }
    this.m_achieveValidationsToRequest.Clear();
  }

  public void TriggerLaunchDayEvent()
  {
    if (!this.HasActiveAchievesForEvent(SpecialEventType.LAUNCH_DAY))
      return;
    Player opposingSidePlayer = GameState.Get().GetOpposingSidePlayer();
    if (opposingSidePlayer == null)
      return;
    BnetPlayer nearbyPlayer = BnetNearbyPlayerMgr.Get().FindNearbyPlayer(opposingSidePlayer.GetGameAccountId());
    if (nearbyPlayer == null)
      return;
    BnetAccountId accountId1 = nearbyPlayer.GetAccountId();
    if ((BnetEntityId) accountId1 == (BnetEntityId) null)
      return;
    List<BnetPlayer> nearbyPlayers = BnetNearbyPlayerMgr.Get().GetNearbyPlayers();
    BnetPlayer bnetPlayer = (BnetPlayer) null;
    using (List<BnetPlayer>.Enumerator enumerator = nearbyPlayers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer current = enumerator.Current;
        BnetAccountId accountId2 = current.GetAccountId();
        if (!((BnetEntityId) accountId2 == (BnetEntityId) null) && !accountId2.Equals((BnetEntityId) accountId1))
        {
          bnetPlayer = current;
          break;
        }
      }
    }
    ulong sessionStartTime1;
    ulong sessionStartTime2;
    if (bnetPlayer == null || !BnetNearbyPlayerMgr.Get().GetNearbySessionStartTime(nearbyPlayer, out sessionStartTime1) || !BnetNearbyPlayerMgr.Get().GetNearbySessionStartTime(bnetPlayer, out sessionStartTime2))
      return;
    BnetGameAccountId hearthstoneGameAccountId1 = nearbyPlayer.GetHearthstoneGameAccountId();
    if ((BnetEntityId) hearthstoneGameAccountId1 == (BnetEntityId) null)
      return;
    BnetGameAccountId hearthstoneGameAccountId2 = bnetPlayer.GetHearthstoneGameAccountId();
    if ((BnetEntityId) hearthstoneGameAccountId2 == (BnetEntityId) null)
      return;
    ++this.m_numEventResponsesNeeded;
    Network.TriggerLaunchEvent(hearthstoneGameAccountId1, sessionStartTime1, hearthstoneGameAccountId2, sessionStartTime2);
  }

  private void LoadAchievesFromDBF()
  {
    List<AchieveDbfRecord> records1 = GameDbf.Achieve.GetRecords();
    List<QuestDialogDbfRecord> records2 = GameDbf.QuestDialog.GetRecords();
    List<QuestDialogOnReceivedDbfRecord> records3 = GameDbf.QuestDialogOnReceived.GetRecords();
    List<QuestDialogOnCompleteDbfRecord> records4 = GameDbf.QuestDialogOnComplete.GetRecords();
    List<QuestDialogOnProgress1DbfRecord> records5 = GameDbf.QuestDialogOnProgress1.GetRecords();
    List<QuestDialogOnProgress2DbfRecord> records6 = GameDbf.QuestDialogOnProgress2.GetRecords();
    Map<int, int> map = new Map<int, int>();
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AchieveManager.\u003CLoadAchievesFromDBF\u003Ec__AnonStorey415 dbfCAnonStorey415 = new AchieveManager.\u003CLoadAchievesFromDBF\u003Ec__AnonStorey415();
    using (List<AchieveDbfRecord>.Enumerator enumerator = records1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        dbfCAnonStorey415.achieveRecord = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        AchieveManager.\u003CLoadAchievesFromDBF\u003Ec__AnonStorey416 dbfCAnonStorey416 = new AchieveManager.\u003CLoadAchievesFromDBF\u003Ec__AnonStorey416();
        // ISSUE: reference to a compiler-generated field
        dbfCAnonStorey416.\u003C\u003Ef__ref\u00241045 = dbfCAnonStorey415;
        // ISSUE: reference to a compiler-generated field
        int id1 = dbfCAnonStorey415.achieveRecord.ID;
        // ISSUE: reference to a compiler-generated field
        bool enabled = dbfCAnonStorey415.achieveRecord.Enabled;
        Achievement.AchType outVal1;
        // ISSUE: reference to a compiler-generated field
        if (EnumUtils.TryGetEnum<Achievement.AchType>(dbfCAnonStorey415.achieveRecord.AchType, out outVal1))
        {
          // ISSUE: reference to a compiler-generated field
          int achQuota = dbfCAnonStorey415.achieveRecord.AchQuota;
          // ISSUE: reference to a compiler-generated field
          int race = dbfCAnonStorey415.achieveRecord.Race;
          TAG_RACE? raceReq = new TAG_RACE?();
          if (race != 0)
            raceReq = new TAG_RACE?((TAG_RACE) race);
          // ISSUE: reference to a compiler-generated field
          int cardSet = dbfCAnonStorey415.achieveRecord.CardSet;
          TAG_CARD_SET? cardSetReq = new TAG_CARD_SET?();
          if (cardSet != 0)
            cardSetReq = new TAG_CARD_SET?((TAG_CARD_SET) cardSet);
          // ISSUE: reference to a compiler-generated field
          string reward = dbfCAnonStorey415.achieveRecord.Reward;
          // ISSUE: reference to a compiler-generated field
          long rewardData1 = dbfCAnonStorey415.achieveRecord.RewardData1;
          // ISSUE: reference to a compiler-generated field
          long rewardData2 = dbfCAnonStorey415.achieveRecord.RewardData2;
          List<RewardData> rewards = new List<RewardData>();
          TAG_CLASS? classReq = new TAG_CLASS?();
          string key = reward;
          if (key != null)
          {
            // ISSUE: reference to a compiler-generated field
            if (AchieveManager.\u003C\u003Ef__switch\u0024mapB7 == null)
            {
              // ISSUE: reference to a compiler-generated field
              AchieveManager.\u003C\u003Ef__switch\u0024mapB7 = new Dictionary<string, int>(13)
              {
                {
                  "basic",
                  0
                },
                {
                  "card",
                  1
                },
                {
                  "card2x",
                  2
                },
                {
                  "cardback",
                  3
                },
                {
                  "cardset",
                  4
                },
                {
                  "craftable_golden",
                  5
                },
                {
                  "dust",
                  6
                },
                {
                  "forge",
                  7
                },
                {
                  "gold",
                  8
                },
                {
                  "goldhero",
                  9
                },
                {
                  "hero",
                  10
                },
                {
                  "mount",
                  11
                },
                {
                  "pack",
                  12
                }
              };
            }
            int num;
            // ISSUE: reference to a compiler-generated field
            if (AchieveManager.\u003C\u003Ef__switch\u0024mapB7.TryGetValue(key, out num))
            {
              switch (num)
              {
                case 0:
                  Debug.LogWarning((object) string.Format("AchieveManager.LoadAchievesFromFile(): unable to define reward {0} for achieve {1}", (object) reward, (object) id1));
                  break;
                case 1:
                  string cardId1 = GameUtils.TranslateDbIdToCardId((int) rewardData1);
                  TAG_PREMIUM premium1 = (TAG_PREMIUM) rewardData2;
                  rewards.Add((RewardData) new CardRewardData(cardId1, premium1, 1));
                  break;
                case 2:
                  string cardId2 = GameUtils.TranslateDbIdToCardId((int) rewardData1);
                  TAG_PREMIUM premium2 = (TAG_PREMIUM) rewardData2;
                  rewards.Add((RewardData) new CardRewardData(cardId2, premium2, 2));
                  break;
                case 3:
                  rewards.Add((RewardData) new CardBackRewardData((int) rewardData1));
                  break;
                case 6:
                  rewards.Add((RewardData) new ArcaneDustRewardData((int) rewardData1));
                  break;
                case 7:
                  rewards.Add((RewardData) new ForgeTicketRewardData((int) rewardData1));
                  break;
                case 8:
                  rewards.Add((RewardData) new GoldRewardData((long) (int) rewardData1));
                  break;
                case 9:
                  string cardId3 = GameUtils.TranslateDbIdToCardId((int) rewardData1);
                  TAG_PREMIUM premium3 = (TAG_PREMIUM) rewardData2;
                  rewards.Add((RewardData) new CardRewardData(cardId3, premium3, 1));
                  break;
                case 10:
                  classReq = new TAG_CLASS?((TAG_CLASS) rewardData2);
                  string heroCardIdFromClass = GameUtils.GetBasicHeroCardIdFromClass(classReq.Value);
                  if (!string.IsNullOrEmpty(heroCardIdFromClass))
                  {
                    rewards.Add((RewardData) new CardRewardData(heroCardIdFromClass, TAG_PREMIUM.NORMAL, 1));
                    break;
                  }
                  break;
                case 11:
                  rewards.Add((RewardData) new MountRewardData((MountRewardData.MountType) rewardData1));
                  break;
                case 12:
                  int id2 = rewardData2 <= 0L ? 1 : (int) rewardData2;
                  rewards.Add((RewardData) new BoosterPackRewardData(id2, (int) rewardData1));
                  break;
              }
            }
          }
          RewardVisualTiming outVal2;
          // ISSUE: reference to a compiler-generated field
          if (EnumUtils.TryGetEnum<RewardVisualTiming>(dbfCAnonStorey415.achieveRecord.RewardTiming, out outVal2))
          {
            // ISSUE: reference to a compiler-generated field
            string unlocks = dbfCAnonStorey415.achieveRecord.Unlocks;
            Achievement.UnlockableFeature? unlockedFeature = new Achievement.UnlockableFeature?();
            Achievement.UnlockableFeature outVal3;
            if (!string.IsNullOrEmpty(unlocks) && EnumUtils.TryGetEnum<Achievement.UnlockableFeature>(unlocks, out outVal3))
              unlockedFeature = new Achievement.UnlockableFeature?(outVal3);
            // ISSUE: reference to a compiler-generated method
            AchieveDbfRecord achieveDbfRecord1 = records1.Find(new Predicate<AchieveDbfRecord>(dbfCAnonStorey416.\u003C\u003Em__24D));
            int num = achieveDbfRecord1 != null ? achieveDbfRecord1.ID : 0;
            map[id1] = num;
            // ISSUE: reference to a compiler-generated method
            AchieveDbfRecord achieveDbfRecord2 = records1.Find(new Predicate<AchieveDbfRecord>(dbfCAnonStorey416.\u003C\u003Em__24E));
            int linkToId = achieveDbfRecord2 != null ? achieveDbfRecord2.ID : 0;
            Achievement.Trigger outVal4;
            // ISSUE: reference to a compiler-generated field
            if (!EnumUtils.TryGetEnum<Achievement.Trigger>(dbfCAnonStorey415.achieveRecord.Triggered, out outVal4))
              outVal4 = Achievement.Trigger.IGNORE;
            SpecialEventType eventTrigger = SpecialEventType.IGNORE;
            Achievement.ClickTriggerType? clickType = new Achievement.ClickTriggerType?();
            switch (outVal4)
            {
              case Achievement.Trigger.CLICK:
                clickType = new Achievement.ClickTriggerType?((Achievement.ClickTriggerType) rewardData1);
                break;
              case Achievement.Trigger.EVENT:
              case Achievement.Trigger.EVENT_TIMING_ONLY:
                // ISSUE: reference to a compiler-generated field
                eventTrigger = EnumUtils.GetEnum<SpecialEventType>(dbfCAnonStorey415.achieveRecord.Event);
                break;
            }
            Achievement.GameMode outVal5 = Achievement.GameMode.ANY;
            // ISSUE: reference to a compiler-generated field
            if (dbfCAnonStorey415.achieveRecord.GameMode != null)
            {
              // ISSUE: reference to a compiler-generated field
              EnumUtils.TryGetEnum<Achievement.GameMode>(dbfCAnonStorey415.achieveRecord.GameMode, out outVal5);
            }
            // ISSUE: reference to a compiler-generated field
            int scenarioId = dbfCAnonStorey415.achieveRecord.ScenarioId;
            // ISSUE: reference to a compiler-generated field
            int adventureWingId = dbfCAnonStorey415.achieveRecord.AdventureWingId;
            // ISSUE: reference to a compiler-generated field
            int booster = dbfCAnonStorey415.achieveRecord.Booster;
            // ISSUE: reference to a compiler-generated field
            bool genericRewardVisual = dbfCAnonStorey415.achieveRecord.UseGenericRewardVisual;
            // ISSUE: reference to a compiler-generated field
            bool toReturningPlayer = dbfCAnonStorey415.achieveRecord.ShowToReturningPlayer;
            // ISSUE: reference to a compiler-generated method
            QuestDialogDbfRecord questDialogDbfRecord = records2.Find(new Predicate<QuestDialogDbfRecord>(dbfCAnonStorey416.\u003C\u003Em__24F));
            // ISSUE: reference to a compiler-generated field
            dbfCAnonStorey416.questDialogId = questDialogDbfRecord != null ? questDialogDbfRecord.ID : 0;
            List<Achievement.QuestDialog> onReceivedDialog = (List<Achievement.QuestDialog>) null;
            List<Achievement.QuestDialog> onCompleteDialog = (List<Achievement.QuestDialog>) null;
            List<Achievement.QuestDialog> onProgress1Dialog = (List<Achievement.QuestDialog>) null;
            List<Achievement.QuestDialog> onProgress2Dialog = (List<Achievement.QuestDialog>) null;
            if (questDialogDbfRecord != null)
            {
              // ISSUE: reference to a compiler-generated method
              List<QuestDialogOnReceivedDbfRecord> all1 = records3.FindAll(new Predicate<QuestDialogOnReceivedDbfRecord>(dbfCAnonStorey416.\u003C\u003Em__250));
              all1.Sort((Comparison<QuestDialogOnReceivedDbfRecord>) ((a, b) =>
              {
                if (a.PlayOrder < b.PlayOrder)
                  return -1;
                return a.PlayOrder > b.PlayOrder ? 1 : 0;
              }));
              onReceivedDialog = this.LoadQuestDialog(all1);
              // ISSUE: reference to a compiler-generated method
              List<QuestDialogOnCompleteDbfRecord> all2 = records4.FindAll(new Predicate<QuestDialogOnCompleteDbfRecord>(dbfCAnonStorey416.\u003C\u003Em__252));
              all2.Sort((Comparison<QuestDialogOnCompleteDbfRecord>) ((a, b) =>
              {
                if (a.PlayOrder < b.PlayOrder)
                  return -1;
                return a.PlayOrder > b.PlayOrder ? 1 : 0;
              }));
              onCompleteDialog = this.LoadQuestDialog(all2);
              // ISSUE: reference to a compiler-generated method
              List<QuestDialogOnProgress1DbfRecord> all3 = records5.FindAll(new Predicate<QuestDialogOnProgress1DbfRecord>(dbfCAnonStorey416.\u003C\u003Em__254));
              all3.Sort((Comparison<QuestDialogOnProgress1DbfRecord>) ((a, b) =>
              {
                if (a.PlayOrder < b.PlayOrder)
                  return -1;
                return a.PlayOrder > b.PlayOrder ? 1 : 0;
              }));
              onProgress1Dialog = this.LoadQuestDialog(all3);
              // ISSUE: reference to a compiler-generated method
              List<QuestDialogOnProgress2DbfRecord> all4 = records6.FindAll(new Predicate<QuestDialogOnProgress2DbfRecord>(dbfCAnonStorey416.\u003C\u003Em__256));
              all4.Sort((Comparison<QuestDialogOnProgress2DbfRecord>) ((a, b) =>
              {
                if (a.PlayOrder < b.PlayOrder)
                  return -1;
                return a.PlayOrder > b.PlayOrder ? 1 : 0;
              }));
              onProgress2Dialog = this.LoadQuestDialog(all4);
            }
            // ISSUE: reference to a compiler-generated field
            bool autoDestroy = dbfCAnonStorey415.achieveRecord.AutoDestroy;
            // ISSUE: reference to a compiler-generated field
            string questTilePrefab = dbfCAnonStorey415.achieveRecord.QuestTilePrefab;
            int onCompleteQuestDialogBannerId = questDialogDbfRecord != null ? questDialogDbfRecord.OnCompleteBannerId : 0;
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            Achievement achievement = new Achievement(dbfCAnonStorey415.achieveRecord, id1, enabled, outVal1, achQuota, linkToId, outVal4, outVal5, raceReq, classReq, cardSetReq, clickType, eventTrigger, unlockedFeature, rewards, scenarioId, adventureWingId, outVal2, booster, genericRewardVisual, toReturningPlayer, dbfCAnonStorey416.questDialogId, autoDestroy, questTilePrefab, onCompleteQuestDialogBannerId, onReceivedDialog, onCompleteDialog, onProgress1Dialog, onProgress2Dialog);
            // ISSUE: reference to a compiler-generated field
            achievement.SetClientFlags((Achievement.ClientFlags) dbfCAnonStorey415.achieveRecord.ClientFlags);
            // ISSUE: reference to a compiler-generated field
            achievement.SetAltTextPredicate(dbfCAnonStorey415.achieveRecord.AltTextPredicate);
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            achievement.SetName((string) dbfCAnonStorey415.achieveRecord.Name, (string) dbfCAnonStorey415.achieveRecord.AltName);
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            achievement.SetDescription((string) dbfCAnonStorey415.achieveRecord.Description, (string) dbfCAnonStorey415.achieveRecord.AltDescription);
            this.InitAchievement(achievement);
          }
        }
      }
    }
    using (List<Achievement>.Enumerator enumerator = this.m_achievements.Values.ToList<Achievement>().FindAll((Predicate<Achievement>) (obj =>
    {
      if (!obj.IsInternal())
        return false;
      return obj.Rewards.Count > 0;
    })).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        Achievement achievement1 = this.GetAchievement(map[current.ID]);
        while (achievement1 != null && achievement1.IsInternal())
          achievement1 = this.GetAchievement(map[achievement1.ID]);
        if (achievement1 == null)
        {
          Debug.LogWarning((object) string.Format("AchieveManager.LoadAchievesFromDBF(): found internal achievement with reward but could not find non-internal parent: {0}", (object) current));
        }
        else
        {
          Achievement achievement2 = this.GetAchievement(achievement1.ID);
          if (achievement2 == null)
            Debug.LogWarning((object) string.Format("AchieveManager.LoadAchievesFromDBF(): parentAchieve with id {0} for internalRewardAchieve {1} is null!", (object) achievement1, (object) current));
          else
            achievement2.AddChildRewards(current.Rewards);
        }
      }
    }
  }

  private List<Achievement.QuestDialog> LoadQuestDialog(List<QuestDialogOnReceivedDbfRecord> onReceivedRecords)
  {
    List<Achievement.QuestDialog> questDialogList = new List<Achievement.QuestDialog>();
    using (List<QuestDialogOnReceivedDbfRecord>.Enumerator enumerator = onReceivedRecords.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        QuestDialogOnReceivedDbfRecord current = enumerator.Current;
        Achievement.QuestDialog questDialog;
        questDialog.playOrder = current.PlayOrder;
        questDialog.prefabName = current.PrefabName;
        questDialog.audioName = current.AudioName;
        questDialog.useAltSpeechBubble = current.AltBubblePosition;
        questDialog.waitBefore = current.WaitBefore;
        questDialog.waitAfter = current.WaitAfter;
        questDialog.persistPrefab = current.PersistPrefab;
        questDialogList.Add(questDialog);
      }
    }
    return questDialogList;
  }

  private List<Achievement.QuestDialog> LoadQuestDialog(List<QuestDialogOnCompleteDbfRecord> onCompleteRecords)
  {
    List<Achievement.QuestDialog> questDialogList = new List<Achievement.QuestDialog>();
    using (List<QuestDialogOnCompleteDbfRecord>.Enumerator enumerator = onCompleteRecords.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        QuestDialogOnCompleteDbfRecord current = enumerator.Current;
        Achievement.QuestDialog questDialog;
        questDialog.playOrder = current.PlayOrder;
        questDialog.prefabName = current.PrefabName;
        questDialog.audioName = current.AudioName;
        questDialog.useAltSpeechBubble = current.AltBubblePosition;
        questDialog.waitBefore = current.WaitBefore;
        questDialog.waitAfter = current.WaitAfter;
        questDialog.persistPrefab = current.PersistPrefab;
        questDialogList.Add(questDialog);
      }
    }
    return questDialogList;
  }

  private List<Achievement.QuestDialog> LoadQuestDialog(List<QuestDialogOnProgress1DbfRecord> onProgress1Records)
  {
    List<Achievement.QuestDialog> questDialogList = new List<Achievement.QuestDialog>();
    using (List<QuestDialogOnProgress1DbfRecord>.Enumerator enumerator = onProgress1Records.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        QuestDialogOnProgress1DbfRecord current = enumerator.Current;
        Achievement.QuestDialog questDialog;
        questDialog.playOrder = current.PlayOrder;
        questDialog.prefabName = current.PrefabName;
        questDialog.audioName = current.AudioName;
        questDialog.useAltSpeechBubble = current.AltBubblePosition;
        questDialog.waitBefore = current.WaitBefore;
        questDialog.waitAfter = current.WaitAfter;
        questDialog.persistPrefab = current.PersistPrefab;
        questDialogList.Add(questDialog);
      }
    }
    return questDialogList;
  }

  private List<Achievement.QuestDialog> LoadQuestDialog(List<QuestDialogOnProgress2DbfRecord> onProgress2Records)
  {
    List<Achievement.QuestDialog> questDialogList = new List<Achievement.QuestDialog>();
    using (List<QuestDialogOnProgress2DbfRecord>.Enumerator enumerator = onProgress2Records.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        QuestDialogOnProgress2DbfRecord current = enumerator.Current;
        Achievement.QuestDialog questDialog;
        questDialog.playOrder = current.PlayOrder;
        questDialog.prefabName = current.PrefabName;
        questDialog.audioName = current.AudioName;
        questDialog.useAltSpeechBubble = current.AltBubblePosition;
        questDialog.waitBefore = current.WaitBefore;
        questDialog.waitAfter = current.WaitAfter;
        questDialog.persistPrefab = current.PersistPrefab;
        questDialogList.Add(questDialog);
      }
    }
    return questDialogList;
  }

  private void InitAchievement(Achievement achievement)
  {
    if (this.m_achievements.ContainsKey(achievement.ID))
      Debug.LogWarning((object) string.Format("AchieveManager.InitAchievement() - already registered achievement with ID {0}", (object) achievement.ID));
    else
      this.m_achievements.Add(achievement.ID, achievement);
  }

  private void WillReset()
  {
    this.m_allNetAchievesReceived = false;
    this.m_achieveValidationsToRequest.Clear();
    this.m_achieveValidationsRequested.Clear();
    this.m_achievesUpdatedListeners.Clear();
    this.m_lastEventTimingValidationByAchieve.Clear();
    this.m_lastCheckLicenseAddedByAchieve.Clear();
    this.m_licenseAddedAchievesUpdatedListeners.Clear();
    this.m_achievements.Clear();
    this.LoadAchievesFromDBF();
  }

  private void OnAchieves()
  {
    Achieves allAchievesList = Network.Achieves();
    if (!this.m_allNetAchievesReceived)
      this.OnAllAchieves(allAchievesList);
    else
      Debug.LogError((object) string.Format("OnAchieves() - was executed / called again when we already had our achievements"));
  }

  private void OnAllAchieves(Achieves allAchievesList)
  {
    using (List<Achieve>.Enumerator enumerator = allAchievesList.List.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achieve current = enumerator.Current;
        Achievement achievement = this.GetAchievement(current.Id);
        if (achievement != null)
          achievement.OnAchieveData(current);
      }
    }
    NetCache.Get().RegisterNotices((NetCache.NetCacheCallback) null, (NetCache.ErrorCallback) null);
    this.CheckAllCardGainAchieves();
    this.m_allNetAchievesReceived = true;
    this.UnblockAllNotifications();
  }

  public void OnAchievementNotifications(List<AchievementNotification> achievementNotifications)
  {
    List<Achievement> completedAchieves = new List<Achievement>();
    List<Achievement> updatedAchieves = new List<Achievement>();
    bool flag = false;
    using (List<AchievementNotification>.Enumerator enumerator = achievementNotifications.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AchievementNotification current = enumerator.Current;
        if (this.m_queueNotifications || !this.m_allNetAchievesReceived || this.m_achieveNotificationsToQueue.Contains((int) current.AchievementId))
        {
          Log.Achievements.Print("Blocking AchievementNotification: ID={0}", (object) current.AchievementId);
          this.m_blockedAchievementNotifications.Add(current);
        }
        else
        {
          Achievement achievement = this.GetAchievement((int) current.AchievementId);
          if (achievement != null)
          {
            if (achievement.AchieveTrigger == Achievement.Trigger.ACCOUNT_LICENSE_ADDED || achievement.AchieveTrigger == Achievement.Trigger.EVENT_TIMING_ONLY)
              flag = true;
            achievement.OnAchieveNotification(current);
            if (!achievement.Active)
              completedAchieves.Add(achievement);
            else
              updatedAchieves.Add(achievement);
            Log.Achievements.Print("OnAchievementNotification: Achievement={0}", (object) achievement);
          }
        }
      }
    }
    if (flag)
      this.m_lastEventTimingAndLicenseAchieveCheck = 0L;
    foreach (AchieveManager.AchievesUpdatedListener achievesUpdatedListener in this.m_achievesUpdatedListeners.ToArray())
      achievesUpdatedListener.Fire(updatedAchieves, completedAchieves);
  }

  private void BlockNotification(int achieveIdToQueue)
  {
    this.BlockNotifications(new List<int>()
    {
      achieveIdToQueue
    });
  }

  private void BlockNotifications(List<int> achieveIdsToQueue)
  {
    for (int index = 0; index < achieveIdsToQueue.Count; ++index)
      this.m_achieveNotificationsToQueue.Add(achieveIdsToQueue[index]);
  }

  private void UnblockNotification(int achieveIdToUnblock)
  {
    this.UnblockNotifications(new List<int>()
    {
      achieveIdToUnblock
    });
  }

  private void UnblockNotifications(List<int> achieveIdsToUnblock)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AchieveManager.\u003CUnblockNotifications\u003Ec__AnonStorey417 notificationsCAnonStorey417 = new AchieveManager.\u003CUnblockNotifications\u003Ec__AnonStorey417();
    // ISSUE: reference to a compiler-generated field
    notificationsCAnonStorey417.achieveIdsToUnblock = achieveIdsToUnblock;
    // ISSUE: reference to a compiler-generated method
    List<AchievementNotification> list = this.m_blockedAchievementNotifications.Where<AchievementNotification>(new Func<AchievementNotification, bool>(notificationsCAnonStorey417.\u003C\u003Em__259)).ToList<AchievementNotification>();
    // ISSUE: reference to a compiler-generated method
    this.m_blockedAchievementNotifications.RemoveAll(new Predicate<AchievementNotification>(notificationsCAnonStorey417.\u003C\u003Em__25A));
    // ISSUE: reference to a compiler-generated method
    this.m_achieveNotificationsToQueue.RemoveAll(new Predicate<int>(notificationsCAnonStorey417.\u003C\u003Em__25B));
    if (list.Count <= 0)
      return;
    this.OnAchievementNotifications(list);
  }

  public void BlockAllNotifications()
  {
    this.m_queueNotifications = true;
  }

  public void UnblockAllNotifications()
  {
    this.m_queueNotifications = false;
    if (this.m_blockedAchievementNotifications.Count <= 0)
      return;
    this.OnAchievementNotifications(this.m_blockedAchievementNotifications);
    this.m_blockedAchievementNotifications.Clear();
  }

  private void OnQuestCanceled()
  {
    Network.CanceledQuest canceledQuest = Network.GetCanceledQuest();
    Log.Achievements.Print("OnQuestCanceled: CanceledQuest={0}", (object) canceledQuest);
    this.m_disableCancelButtonUntilServerReturns = false;
    if (canceledQuest.Canceled)
    {
      this.GetAchievement(canceledQuest.AchieveID).OnCancelSuccess();
      NetCache.NetCacheRewardProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>();
      if (netObject != null)
        netObject.NextQuestCancelDate = canceledQuest.NextQuestCancelDate;
    }
    this.FireAchieveCanceledEvent(canceledQuest.AchieveID, canceledQuest.Canceled);
    this.UnblockAllNotifications();
  }

  private void OnAchieveValidated()
  {
    this.m_achieveValidationsRequested.Remove(Network.GetValidatedAchieve().Achieve);
  }

  private void OnEventTriggered()
  {
    Network.GetTriggerEventResponse();
    --this.m_numEventResponsesNeeded;
  }

  private void OnAccountLicenseAchieveResponse()
  {
    Network.AccountLicenseAchieveResponse licenseAchieveResponse = Network.GetAccountLicenseAchieveResponse();
    if (licenseAchieveResponse.Result != Network.AccountLicenseAchieveResponse.AchieveResult.COMPLETE)
    {
      this.FireLicenseAddedAchievesUpdatedEvent();
    }
    else
    {
      Log.Rachelle.Print("AchieveManager.OnAccountLicenseAchieveResponse(): achieve {0} is now complete, refreshing achieves", (object) licenseAchieveResponse.Achieve);
      this.OnAccountLicenseAchievesUpdated((object) licenseAchieveResponse.Achieve);
    }
  }

  private void OnAccountLicenseAchievesUpdated(object userData)
  {
    int num = (int) userData;
    Log.Rachelle.Print("AchieveManager.OnAccountLicenseAchievesUpdated(): refreshing achieves complete, triggered by achieve {0}", (object) num);
    this.FireLicenseAddedAchievesUpdatedEvent();
  }

  private void FireLicenseAddedAchievesUpdatedEvent()
  {
    List<Achievement> licenseAddedAchieves = this.GetActiveLicenseAddedAchieves();
    foreach (AchieveManager.LicenseAddedAchievesUpdatedListener achievesUpdatedListener in this.m_licenseAddedAchievesUpdatedListeners.ToArray())
      achievesUpdatedListener.Fire(licenseAddedAchieves);
  }

  private void OnNewNotices(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList)
  {
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = newNotices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.ProfileNotice current = enumerator.Current;
        if (current.Origin == NetCache.ProfileNotice.NoticeOrigin.ACHIEVEMENT)
        {
          Achievement achievement = this.GetAchievement((int) current.OriginData);
          if (achievement != null)
            achievement.AddRewardNoticeID(current.NoticeID);
        }
      }
    }
  }

  private bool CanCancelQuestNow()
  {
    if (Vars.Key("Quests.CanCancelManyTimes").GetBool(false))
      return true;
    NetCache.NetCacheRewardProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>();
    if (netObject == null)
      return false;
    long fileTimeUtc = DateTime.Now.ToFileTimeUtc();
    return netObject.NextQuestCancelDate <= fileTimeUtc;
  }

  private void FireAchieveCanceledEvent(int achieveID, bool success)
  {
    foreach (AchieveManager.AchieveCanceledListener canceledListener in this.m_achieveCanceledListeners.ToArray())
      canceledListener.Fire(achieveID, success);
  }

  private void AddAchievesToValidate(List<Achievement> possibleRaceAchieves, List<Achievement> possibleCardSetAchieves)
  {
    using (List<Achievement>.Enumerator enumerator = possibleRaceAchieves.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        TAG_PREMIUM? premium = new TAG_PREMIUM?();
        if (current.AchieveTrigger == Achievement.Trigger.GAIN_GOLDEN_CARD)
          premium = new TAG_PREMIUM?(TAG_PREMIUM.GOLDEN);
        if (CollectionManager.Get().AllCardsInSetOwned(new TAG_CARD_SET?(TAG_CARD_SET.CORE), new TAG_CLASS?(), new TAG_RARITY?(), new TAG_RACE?(current.RaceRequirement.Value), premium) && CollectionManager.Get().AllCardsInSetOwned(new TAG_CARD_SET?(TAG_CARD_SET.EXPERT1), new TAG_CLASS?(), new TAG_RARITY?(), new TAG_RACE?(current.RaceRequirement.Value), premium))
          this.m_achieveValidationsToRequest.Add(current.ID);
      }
    }
    using (List<Achievement>.Enumerator enumerator = possibleCardSetAchieves.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        if (CollectionManager.Get().AllCardsInSetOwned(new TAG_CARD_SET?(current.CardSetRequirement.Value), new TAG_CLASS?(), new TAG_RARITY?(), new TAG_RACE?(), new TAG_PREMIUM?()))
          this.m_achieveValidationsToRequest.Add(current.ID);
      }
    }
  }

  private void CheckAllCardGainAchieves()
  {
    this.AddAchievesToValidate(this.m_achievements.Values.ToList<Achievement>().FindAll((Predicate<Achievement>) (obj =>
    {
      if (obj.IsCompleted())
        return false;
      switch (obj.AchieveTrigger)
      {
        case Achievement.Trigger.GAIN_CARD:
        case Achievement.Trigger.GAIN_GOLDEN_CARD:
          return obj.RaceRequirement.HasValue;
        default:
          return false;
      }
    })), this.m_achievements.Values.ToList<Achievement>().FindAll((Predicate<Achievement>) (obj =>
    {
      if (obj.IsCompleted() || obj.AchieveTrigger != Achievement.Trigger.COMPLETE_CARD_SET)
        return false;
      return obj.CardSetRequirement.HasValue;
    })));
    this.ValidateAchievesNow();
  }

  private void CheckTimedEventsAndLicenses(DateTime utcNow)
  {
    if (!this.m_allNetAchievesReceived)
      return;
    DateTime localTime = utcNow.ToLocalTime();
    if (localTime.Ticks - this.m_lastEventTimingAndLicenseAchieveCheck < AchieveManager.TIMED_AND_LICENSE_ACHIEVE_CHECK_DELAY_TICKS)
      return;
    this.m_lastEventTimingAndLicenseAchieveCheck = localTime.Ticks;
    int num = 0;
    using (Map<int, Achievement>.ValueCollection.Enumerator enumerator = this.m_achievements.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        if (current.Enabled && !current.IsCompleted() && (current.Active && current.AchieveTrigger == Achievement.Trigger.EVENT_TIMING_ONLY) && (SpecialEventManager.Get().IsEventActive(current.EventTrigger, false) && (!this.m_lastEventTimingValidationByAchieve.ContainsKey(current.ID) || localTime.Ticks - this.m_lastEventTimingValidationByAchieve[current.ID] >= AchieveManager.TIMED_ACHIEVE_VALIDATION_DELAY_TICKS)))
        {
          Log.Rachelle.Print("AchieveManager.CheckTimedEventsAndLicenses(): checking on timed event achieve {0} time {1}", new object[2]
          {
            (object) current.ID,
            (object) localTime
          });
          this.m_lastEventTimingValidationByAchieve[current.ID] = localTime.Ticks;
          this.m_achieveValidationsToRequest.Add(current.ID);
          ++num;
        }
        if (current.IsActiveLicenseAddedAchieve() && (!this.m_lastCheckLicenseAddedByAchieve.ContainsKey(current.ID) || utcNow.Ticks - this.m_lastCheckLicenseAddedByAchieve[current.ID] >= AchieveManager.CHECK_LICENSE_ADDED_ACHIEVE_DELAY_TICKS))
        {
          Log.Rachelle.Print("AchieveManager.CheckTimedEventsAndLicenses(): checking on license added achieve {0} time {1}", new object[2]
          {
            (object) current.ID,
            (object) localTime
          });
          this.m_lastCheckLicenseAddedByAchieve[current.ID] = utcNow.Ticks;
          Network.CheckAccountLicenseAchieve(current.ID);
        }
      }
    }
    if (num == 0)
      return;
    this.ValidateAchievesNow();
  }

  private List<Achievement> GetActiveLicenseAddedAchieves()
  {
    return this.m_achievements.Values.ToList<Achievement>().FindAll((Predicate<Achievement>) (obj => obj.IsActiveLicenseAddedAchieve()));
  }

  public List<RewardData> GetRewardsForAdventureWing(int wingID, HashSet<RewardVisualTiming> rewardTimings)
  {
    List<RewardData> rewardDataList = new List<RewardData>();
    using (List<Achievement>.Enumerator enumerator = this.GetAchievesForAdventureWing(wingID).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        rewardDataList.AddRange((IEnumerable<RewardData>) this.GetRewardsForAchieve(current.ID, rewardTimings));
      }
    }
    return rewardDataList;
  }

  public List<RewardData> GetRewardsForAdventureScenario(int wingID, int scenarioID, HashSet<RewardVisualTiming> rewardTimings)
  {
    List<RewardData> rewardDataList = new List<RewardData>();
    using (List<Achievement>.Enumerator enumerator = this.GetAchievesForAdventureWing(wingID).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        if (current.ScenarioID == scenarioID)
          rewardDataList.AddRange((IEnumerable<RewardData>) this.GetRewardsForAchieve(current.ID, rewardTimings));
      }
    }
    return rewardDataList;
  }

  private List<RewardData> GetRewardsForAchieve(int achieveID, HashSet<RewardVisualTiming> rewardTimings)
  {
    List<RewardData> rewardDataList = new List<RewardData>();
    Achievement achievement = this.GetAchievement(achieveID);
    List<RewardData> rewards = achievement.Rewards;
    if (rewardTimings.Contains(achievement.RewardTiming))
    {
      using (List<RewardData>.Enumerator enumerator = rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          RewardData current = enumerator.Current;
          rewardDataList.Add(current);
        }
      }
    }
    return rewardDataList;
  }

  private class AchieveCanceledListener : EventListener<AchieveManager.AchieveCanceledCallback>
  {
    public void Fire(int achieveID, bool success)
    {
      this.m_callback(achieveID, success, this.m_userData);
    }
  }

  private class AchievesUpdatedListener : EventListener<AchieveManager.AchievesUpdatedCallback>
  {
    public void Fire(List<Achievement> updatedAchieves, List<Achievement> completedAchieves)
    {
      this.m_callback(updatedAchieves, completedAchieves, this.m_userData);
    }
  }

  private class LicenseAddedAchievesUpdatedListener : EventListener<AchieveManager.LicenseAddedAchievesUpdatedCallback>
  {
    public void Fire(List<Achievement> activeLicenseAddedAchieves)
    {
      this.m_callback(activeLicenseAddedAchieves, this.m_userData);
    }
  }

  public delegate void AchieveCanceledCallback(int achieveID, bool success, object userData);

  public delegate void AchievesUpdatedCallback(List<Achievement> updatedAchieves, List<Achievement> completedAchieves, object userData);

  public delegate void LicenseAddedAchievesUpdatedCallback(List<Achievement> activeLicenseAddedAchieves, object userData);
}
