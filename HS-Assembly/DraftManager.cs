﻿// Decompiled with JetBrains decompiler
// Type: DraftManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusClient;
using PegasusShared;
using PegasusUtil;
using System;
using System.Collections.Generic;
using UnityEngine;

public class DraftManager
{
  private int m_maxWins = int.MaxValue;
  private List<DraftManager.DraftDeckSet> m_draftDeckSetListeners = new List<DraftManager.DraftDeckSet>();
  private static DraftManager s_instance;
  private CollectionDeck m_draftDeck;
  private bool m_hasReceivedSessionWinsLosses;
  private int m_currentSlot;
  private int m_validSlot;
  private int m_losses;
  private int m_wins;
  private bool m_isNewKey;
  private bool m_deckActiveDuringSession;
  private Network.RewardChest m_chest;

  public bool CanShowWinsLosses
  {
    get
    {
      return this.m_hasReceivedSessionWinsLosses;
    }
  }

  public static DraftManager Get()
  {
    if (DraftManager.s_instance == null)
    {
      DraftManager.s_instance = new DraftManager();
      ApplicationMgr.Get().WillReset += new Action(DraftManager.s_instance.DraftManager_WillReset);
      GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(DraftManager.s_instance.OnFindGameEvent));
      Network.Get().RegisterNetHandler((object) ArenaSessionResponse.PacketID.ID, new Network.NetHandler(DraftManager.s_instance.OnArenaSessionResponse), (Network.TimeoutHandler) null);
    }
    return DraftManager.s_instance;
  }

  public void OnLoggedIn()
  {
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
  }

  public void RegisterDisplayHandlers()
  {
    Network network = Network.Get();
    network.RegisterNetHandler((object) DraftBeginning.PacketID.ID, new Network.NetHandler(this.OnBegin), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.DraftRetired.PacketID.ID, new Network.NetHandler(this.OnRetire), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) DraftRewardsAcked.PacketID.ID, new Network.NetHandler(this.OnAckRewards), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.DraftChoicesAndContents.PacketID.ID, new Network.NetHandler(this.OnChoicesAndContents), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.DraftChosen.PacketID.ID, new Network.NetHandler(this.OnChosen), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.DraftError.PacketID.ID, new Network.NetHandler(this.OnError), (Network.TimeoutHandler) null);
    StoreManager.Get().RegisterSuccessfulPurchaseAckListener(new StoreManager.SuccessfulPurchaseAckCallback(this.OnDraftPurchaseAck));
    if (!DemoMgr.Get().ArenaIs1WinMode())
      return;
    StoreManager.Get().RegisterSuccessfulPurchaseListener(new StoreManager.SuccessfulPurchaseCallback(this.OnDraftPurchaseAck));
  }

  public void UnregisterDisplayHandlers()
  {
    Network network = Network.Get();
    network.RemoveNetHandler((object) DraftBeginning.PacketID.ID, new Network.NetHandler(this.OnBegin));
    network.RemoveNetHandler((object) PegasusUtil.DraftRetired.PacketID.ID, new Network.NetHandler(this.OnRetire));
    network.RemoveNetHandler((object) DraftRewardsAcked.PacketID.ID, new Network.NetHandler(this.OnAckRewards));
    network.RemoveNetHandler((object) PegasusUtil.DraftChoicesAndContents.PacketID.ID, new Network.NetHandler(this.OnChoicesAndContents));
    network.RemoveNetHandler((object) PegasusUtil.DraftChosen.PacketID.ID, new Network.NetHandler(this.OnChosen));
    network.RemoveNetHandler((object) PegasusUtil.DraftError.PacketID.ID, new Network.NetHandler(this.OnError));
    StoreManager.Get().RemoveSuccessfulPurchaseAckListener(new StoreManager.SuccessfulPurchaseAckCallback(this.OnDraftPurchaseAck));
    if (!DemoMgr.Get().ArenaIs1WinMode())
      return;
    StoreManager.Get().RemoveSuccessfulPurchaseListener(new StoreManager.SuccessfulPurchaseCallback(this.OnDraftPurchaseAck));
  }

  public void RegisterDraftDeckSetListener(DraftManager.DraftDeckSet dlg)
  {
    this.m_draftDeckSetListeners.Add(dlg);
  }

  public void RemoveDraftDeckSetListener(DraftManager.DraftDeckSet dlg)
  {
    this.m_draftDeckSetListeners.Remove(dlg);
  }

  public CollectionDeck GetDraftDeck()
  {
    return this.m_draftDeck;
  }

  public int GetSlot()
  {
    return this.m_currentSlot;
  }

  public int GetLosses()
  {
    return this.m_losses;
  }

  public int GetWins()
  {
    return this.m_wins;
  }

  public int GetMaxWins()
  {
    return this.m_maxWins;
  }

  public bool GetIsNewKey()
  {
    return this.m_isNewKey;
  }

  public bool DeckWasActiveDuringSession()
  {
    return this.m_deckActiveDuringSession;
  }

  public List<RewardData> GetRewards()
  {
    if (this.m_chest != null)
      return this.m_chest.Rewards;
    return new List<RewardData>();
  }

  public void MakeChoice(int choiceNum)
  {
    if (this.m_draftDeck == null)
    {
      Debug.LogWarning((object) "DraftManager.MakeChoice(): Trying to make a draft choice while the draft deck is null");
    }
    else
    {
      if (this.m_validSlot != this.m_currentSlot)
        return;
      ++this.m_validSlot;
      Network.MakeDraftChoice(this.m_draftDeck.ID, this.m_currentSlot, choiceNum);
    }
  }

  public void NotifyOfFinalGame(bool wonFinalGame)
  {
    if (wonFinalGame)
      ++this.m_wins;
    else
      ++this.m_losses;
  }

  public void FindGame()
  {
    GameMgr.Get().FindGame(GameType.GT_ARENA, FormatType.FT_WILD, 2, 0L, 0L);
  }

  private void ClearDeckInfo()
  {
    this.m_draftDeck = (CollectionDeck) null;
    this.m_hasReceivedSessionWinsLosses = false;
    this.m_losses = 0;
    this.m_wins = 0;
    this.m_maxWins = int.MaxValue;
    this.m_isNewKey = false;
    this.m_chest = (Network.RewardChest) null;
    this.m_deckActiveDuringSession = false;
  }

  private void DraftManager_WillReset()
  {
    this.ClearDeckInfo();
  }

  private void OnBegin()
  {
    this.m_hasReceivedSessionWinsLosses = true;
    this.m_losses = 0;
    this.m_wins = 0;
    BnetPresenceMgr.Get().SetGameFieldBlob(22U, (IProtoBuf) new SessionRecord()
    {
      Wins = 0U,
      Losses = 0U,
      RunFinished = false,
      SessionRecordType = SessionRecordType.ARENA
    });
    Network.BeginDraft newDraftDeckId = Network.GetNewDraftDeckID();
    this.m_draftDeck = new CollectionDeck()
    {
      ID = newDraftDeckId.DeckID,
      Type = DeckType.DRAFT_DECK,
      IsWild = true
    };
    this.m_currentSlot = 0;
    this.m_validSlot = 0;
    Log.Arena.Print(string.Format("DraftManager.OnBegin - Got new draft deck with ID: {0}", (object) this.m_draftDeck.ID));
    this.InformDraftDisplayOfChoices(newDraftDeckId.Heroes);
    this.FireDraftDeckSetEvent();
  }

  private void OnRetire()
  {
    Network.DraftRetired retiredDraft = Network.GetRetiredDraft();
    Log.Arena.Print(string.Format("DraftManager.OnRetire deckID={0}", (object) retiredDraft.Deck));
    this.m_chest = retiredDraft.Chest;
    this.InformDraftDisplayOfChoices(new List<NetCache.CardDefinition>());
  }

  private void OnAckRewards()
  {
    BnetPresenceMgr.Get().SetGameFieldBlob(22U, (IProtoBuf) new SessionRecord()
    {
      Wins = (uint) this.m_wins,
      Losses = (uint) this.m_losses,
      RunFinished = true,
      SessionRecordType = SessionRecordType.ARENA
    });
    if (!Options.Get().GetBool(Option.HAS_ACKED_ARENA_REWARDS, false) && UserAttentionManager.CanShowAttentionGrabber("DraftManager.OnAckRewards:" + (object) Option.HAS_ACKED_ARENA_REWARDS))
    {
      NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, new Vector3(155.3f, NotificationManager.DEPTH, 34.5f), GameStrings.Get("VO_INNKEEPER_ARENA_1ST_REWARD"), "VO_INNKEEPER_ARENA_1ST_REWARD", 0.0f, (Action) null, false);
      Options.Get().SetBool(Option.HAS_ACKED_ARENA_REWARDS, true);
    }
    Network.GetRewardsAckDraftID();
    this.ClearDeckInfo();
  }

  private void OnChoicesAndContents()
  {
    Network.DraftChoicesAndContents choicesAndContents = Network.GetDraftChoicesAndContents();
    this.m_hasReceivedSessionWinsLosses = true;
    this.m_currentSlot = choicesAndContents.Slot;
    this.m_validSlot = choicesAndContents.Slot;
    this.m_draftDeck = new CollectionDeck()
    {
      ID = choicesAndContents.DeckInfo.Deck,
      Type = DeckType.DRAFT_DECK,
      HeroCardID = choicesAndContents.Hero.Name,
      HeroPremium = choicesAndContents.Hero.Premium,
      IsWild = true
    };
    Log.Arena.Print(string.Format("DraftManager.OnChoicesAndContents - Draft Deck ID: {0}, Hero Card = {1}", (object) this.m_draftDeck.ID, (object) this.m_draftDeck.HeroCardID));
    using (List<Network.CardUserData>.Enumerator enumerator = choicesAndContents.DeckInfo.Cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.CardUserData current = enumerator.Current;
        string cardID = current.DbId != 0 ? GameUtils.TranslateDbIdToCardId(current.DbId) : string.Empty;
        Log.Arena.Print(string.Format("DraftManager.OnChoicesAndContents - Draft deck contains card {0}", (object) cardID));
        for (int index = 0; index < current.Count; ++index)
        {
          if (!this.m_draftDeck.AddCard(cardID, current.Premium, false))
            Debug.LogWarning((object) string.Format("DraftManager.OnChoicesAndContents() - Card {0} could not be added to draft deck", (object) cardID));
        }
      }
    }
    this.m_losses = choicesAndContents.Losses;
    this.m_isNewKey = choicesAndContents.Wins > this.m_wins;
    this.m_wins = choicesAndContents.Wins;
    this.m_maxWins = choicesAndContents.MaxWins;
    this.m_chest = choicesAndContents.Chest;
    if (this.m_losses > 0 && DemoMgr.Get().ArenaIs1WinMode())
    {
      Network.RetireDraftDeck(this.GetDraftDeck().ID, this.GetSlot());
    }
    else
    {
      if (this.m_wins == 5 && DemoMgr.Get().GetMode() == DemoMode.BLIZZCON_2013)
        DemoMgr.Get().CreateDemoText(GameStrings.Get("GLUE_BLIZZCON2013_ARENA_5_WINS"), false, false);
      else if (this.m_losses == 3 && !Options.Get().GetBool(Option.HAS_LOST_IN_ARENA, false) && UserAttentionManager.CanShowAttentionGrabber("DraftManager.OnChoicesAndContents:" + (object) Option.HAS_LOST_IN_ARENA))
      {
        NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, new Vector3(155.3f, NotificationManager.DEPTH, 34.5f), GameStrings.Get("VO_INNKEEPER_ARENA_3RD_LOSS"), "VO_INNKEEPER_ARENA_3RD_LOSS", 0.0f, (Action) null, false);
        Options.Get().SetBool(Option.HAS_LOST_IN_ARENA, true);
      }
      this.InformDraftDisplayOfChoices(choicesAndContents.Choices);
    }
  }

  private void InformDraftDisplayOfChoices(List<NetCache.CardDefinition> choices)
  {
    DraftDisplay draftDisplay = DraftDisplay.Get();
    if ((UnityEngine.Object) draftDisplay == (UnityEngine.Object) null)
      return;
    if (choices.Count == 0)
    {
      DraftDisplay.DraftMode mode;
      if (this.m_chest == null)
      {
        mode = DraftDisplay.DraftMode.ACTIVE_DRAFT_DECK;
        this.m_deckActiveDuringSession = true;
      }
      else
        mode = DraftDisplay.DraftMode.IN_REWARDS;
      draftDisplay.SetDraftMode(mode);
    }
    else
    {
      draftDisplay.SetDraftMode(DraftDisplay.DraftMode.DRAFTING);
      draftDisplay.AcceptNewChoices(choices);
    }
  }

  private void OnChosen()
  {
    Network.DraftChosen chosenAndNext = Network.GetChosenAndNext();
    if (this.m_currentSlot == 0)
    {
      Log.Arena.Print(string.Format("DraftManager.OnChosen(): hero={0} premium={1}", (object) chosenAndNext.ChosenCard.Name, (object) chosenAndNext.ChosenCard.Premium));
      this.m_draftDeck.HeroCardID = chosenAndNext.ChosenCard.Name;
      this.m_draftDeck.HeroPremium = chosenAndNext.ChosenCard.Premium;
    }
    else
      this.m_draftDeck.AddCard(chosenAndNext.ChosenCard.Name, chosenAndNext.ChosenCard.Premium, false);
    ++this.m_currentSlot;
    if (this.m_currentSlot > 30 && (UnityEngine.Object) DraftDisplay.Get() != (UnityEngine.Object) null)
      DraftDisplay.Get().DoDeckCompleteAnims();
    this.InformDraftDisplayOfChoices(chosenAndNext.NextChoices);
  }

  private void OnError()
  {
    if (!SceneMgr.Get().IsModeRequested(SceneMgr.Mode.DRAFT))
      return;
    Network.DraftError draftError = Network.GetDraftError();
    DraftDisplay draftDisplay = DraftDisplay.Get();
    switch (draftError)
    {
      case Network.DraftError.DE_UNKNOWN:
        Debug.LogError((object) "DraftManager.OnError - UNKNOWN EXCEPTION - Talk to Brode or Fitch.");
        break;
      case Network.DraftError.DE_NO_LICENSE:
        Debug.LogWarning((object) "DraftManager.OnError - No License.  What does this mean???");
        break;
      case Network.DraftError.DE_RETIRE_FIRST:
        Debug.LogError((object) "DraftManager.OnError - You cannot start a new draft while one is in progress.");
        break;
      case Network.DraftError.DE_NOT_IN_DRAFT:
        if (!((UnityEngine.Object) draftDisplay != (UnityEngine.Object) null))
          break;
        draftDisplay.SetDraftMode(DraftDisplay.DraftMode.NO_ACTIVE_DRAFT);
        break;
      case Network.DraftError.DE_NOT_IN_DRAFT_BUT_COULD_BE:
        if (Options.Get().GetBool(Option.HAS_SEEN_FORGE, false))
        {
          this.RequestDraftStart();
          break;
        }
        DraftDisplay.Get().SetDraftMode(DraftDisplay.DraftMode.NO_ACTIVE_DRAFT);
        break;
      case Network.DraftError.DE_FEATURE_DISABLED:
        Debug.LogError((object) "DraftManager.OnError - The Arena is currently disabled. Returning to the hub.");
        if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
          break;
        SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
        Error.AddWarningLoc("GLOBAL_FEATURE_DISABLED_TITLE", "GLOBAL_FEATURE_DISABLED_MESSAGE_FORGE");
        break;
      default:
        Debug.LogError((object) ("DraftManager.onError - UNHANDLED ERROR - please send this to Brode. ERROR: " + draftError.ToString()));
        break;
    }
  }

  private void OnArenaSessionResponse()
  {
    ArenaSessionResponse arenaSessionResponse = Network.GetArenaSessionResponse();
    if (arenaSessionResponse == null || arenaSessionResponse.ErrorCode != PegasusShared.ErrorCode.ERROR_OK || !arenaSessionResponse.HasSession)
      return;
    this.m_hasReceivedSessionWinsLosses = true;
    this.m_wins = arenaSessionResponse.Session.Wins;
    this.m_losses = arenaSessionResponse.Session.Losses;
    if (!GameMgr.Get().IsArena() && !GameMgr.Get().IsNextArena())
      return;
    BnetPresenceMgr.Get().SetGameFieldBlob(22U, (IProtoBuf) new SessionRecord()
    {
      Wins = (uint) this.m_wins,
      Losses = (uint) this.m_losses,
      RunFinished = false,
      SessionRecordType = SessionRecordType.ARENA
    });
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_CANCELED:
        if ((UnityEngine.Object) DraftDisplay.Get() != (UnityEngine.Object) null)
        {
          DraftDisplay.Get().HandleGameStartupFailure();
          break;
        }
        break;
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
      case FindGameState.SERVER_GAME_CANCELED:
        if ((UnityEngine.Object) DraftDisplay.Get() != (UnityEngine.Object) null)
        {
          DraftDisplay.Get().HandleGameStartupFailure();
          break;
        }
        break;
      case FindGameState.SERVER_GAME_CONNECTING:
        if (GameMgr.Get().IsNextArena() && !this.m_hasReceivedSessionWinsLosses)
        {
          Network.SendArenaSessionRequest();
          break;
        }
        break;
    }
    return false;
  }

  private void OnDraftPurchaseAck(Network.Bundle bundle, PaymentMethod paymentMethod, object userData)
  {
    if (this.m_draftDeck != null)
      StoreManager.Get().HideStore(StoreType.ARENA_STORE);
    else
      this.RequestDraftStart();
  }

  public void RequestDraftStart()
  {
    Network.StartANewDraft();
  }

  private void FireDraftDeckSetEvent()
  {
    foreach (DraftManager.DraftDeckSet draftDeckSet in this.m_draftDeckSetListeners.ToArray())
      draftDeckSet(this.m_draftDeck);
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (!GameMgr.Get().IsArena() || mode != SceneMgr.Mode.GAMEPLAY)
      return;
    GameState.Get().RegisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
  }

  private void OnGameOver(TAG_PLAYSTATE playState, object userData)
  {
    switch (playState)
    {
      case TAG_PLAYSTATE.WON:
        if (this.GetWins() != 11)
          break;
        this.NotifyOfFinalGame(true);
        break;
      case TAG_PLAYSTATE.LOST:
      case TAG_PLAYSTATE.TIED:
        if (this.GetLosses() != 2)
          break;
        this.NotifyOfFinalGame(false);
        break;
    }
  }

  public delegate void DraftDeckSet(CollectionDeck deck);
}
