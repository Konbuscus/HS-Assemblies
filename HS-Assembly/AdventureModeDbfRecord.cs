﻿// Decompiled with JetBrains decompiler
// Type: AdventureModeDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AdventureModeDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    AdventureModeDbfAsset adventureModeDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (AdventureModeDbfAsset)) as AdventureModeDbfAsset;
    if ((UnityEngine.Object) adventureModeDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("AdventureModeDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < adventureModeDbfAsset.Records.Count; ++index)
      adventureModeDbfAsset.Records[index].StripUnusedLocales();
    records = (object) adventureModeDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureModeDbfRecord.\u003C\u003Ef__switch\u0024map11 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureModeDbfRecord.\u003C\u003Ef__switch\u0024map11 = new Dictionary<string, int>(2)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureModeDbfRecord.\u003C\u003Ef__switch\u0024map11.TryGetValue(key, out num))
      {
        if (num == 0)
          return (object) this.ID;
        if (num == 1)
          return (object) this.NoteDesc;
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (AdventureModeDbfRecord.\u003C\u003Ef__switch\u0024map12 == null)
    {
      // ISSUE: reference to a compiler-generated field
      AdventureModeDbfRecord.\u003C\u003Ef__switch\u0024map12 = new Dictionary<string, int>(2)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!AdventureModeDbfRecord.\u003C\u003Ef__switch\u0024map12.TryGetValue(key, out num))
      return;
    if (num != 0)
    {
      if (num != 1)
        return;
      this.SetNoteDesc((string) val);
    }
    else
      this.SetID((int) val);
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureModeDbfRecord.\u003C\u003Ef__switch\u0024map13 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureModeDbfRecord.\u003C\u003Ef__switch\u0024map13 = new Dictionary<string, int>(2)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureModeDbfRecord.\u003C\u003Ef__switch\u0024map13.TryGetValue(key, out num))
      {
        if (num == 0)
          return typeof (int);
        if (num == 1)
          return typeof (string);
      }
    }
    return (System.Type) null;
  }
}
