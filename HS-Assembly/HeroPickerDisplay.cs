﻿// Decompiled with JetBrains decompiler
// Type: HeroPickerDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HeroPickerDisplay : MonoBehaviour
{
  private static readonly PlatformDependentValue<Vector3> HERO_PICKER_START_POSITION = new PlatformDependentValue<Vector3>(PlatformCategory.Screen)
  {
    PC = new Vector3(-57.36467f, 2.4869f, -28.6f),
    Phone = new Vector3(-66.4f, 2.4869f, -28.6f)
  };
  private static readonly Vector3 HERO_PICKER_END_POSITION = new Vector3(40.6f, 2.4869f, -28.6f);
  public GameObject m_deckPickerBone;
  private static HeroPickerDisplay s_instance;
  private DeckPickerTrayDisplay m_deckPickerTray;

  private void Awake()
  {
    this.transform.localPosition = (Vector3) HeroPickerDisplay.HERO_PICKER_START_POSITION;
    AssetLoader.Get().LoadActor(!(bool) UniversalInputManager.UsePhoneUI ? "DeckPickerTray" : "DeckPickerTray_phone", new AssetLoader.GameObjectCallback(this.DeckPickerTrayLoaded), (object) null, false);
    if ((Object) HeroPickerDisplay.s_instance != (Object) null)
      Debug.LogWarning((object) "HeroPickerDisplay is supposed to be a singleton, but a second instance of it is being created!");
    HeroPickerDisplay.s_instance = this;
    SoundManager.Get().Load("hero_panel_slide_on");
    SoundManager.Get().Load("hero_panel_slide_off");
  }

  private void OnDestroy()
  {
    HeroPickerDisplay.s_instance = (HeroPickerDisplay) null;
  }

  public static HeroPickerDisplay Get()
  {
    return HeroPickerDisplay.s_instance;
  }

  public bool IsShown()
  {
    return this.transform.localPosition == HeroPickerDisplay.HERO_PICKER_END_POSITION;
  }

  public bool IsHidden()
  {
    return this.transform.localPosition == (Vector3) HeroPickerDisplay.HERO_PICKER_START_POSITION;
  }

  public void ShowTray()
  {
    SoundManager.Get().LoadAndPlay("hero_panel_slide_on");
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) HeroPickerDisplay.HERO_PICKER_END_POSITION, (object) "time", (object) 0.5f, (object) "isLocal", (object) true, (object) "easeType", (object) iTween.EaseType.easeOutBounce));
  }

  private void DeckPickerTrayLoaded(string name, GameObject go, object callbackData)
  {
    this.m_deckPickerTray = go.GetComponent<DeckPickerTrayDisplay>();
    this.m_deckPickerTray.UpdateCreateDeckText();
    this.m_deckPickerTray.transform.parent = this.transform;
    this.m_deckPickerTray.transform.localScale = this.m_deckPickerBone.transform.localScale;
    this.m_deckPickerTray.transform.localPosition = this.m_deckPickerBone.transform.localPosition;
    this.m_deckPickerTray.Init();
    this.ShowTray();
  }

  public void HideTray(float delay = 0.0f)
  {
    SoundManager.Get().LoadAndPlay("hero_panel_slide_off");
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) (Vector3) HeroPickerDisplay.HERO_PICKER_START_POSITION, (object) "time", (object) 0.5f, (object) "isLocal", (object) true, (object) "oncomplete", (object) "KillHeroPicker", (object) "oncompletetarget", (object) this.gameObject, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "delay", (object) delay));
  }

  private void KillHeroPicker()
  {
    this.m_deckPickerTray.Unload();
    Object.DestroyImmediate((Object) this.gameObject);
  }
}
