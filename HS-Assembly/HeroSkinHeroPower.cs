﻿// Decompiled with JetBrains decompiler
// Type: HeroSkinHeroPower
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class HeroSkinHeroPower : MonoBehaviour
{
  public Actor m_Actor;
  public Texture m_OriginalFrontTexture;
  public Texture m_OriginalBackTexture;

  private void Start()
  {
    if (!SceneMgr.Get().IsInGame())
      return;
    this.StartCoroutine(this.HeroSkinCustomHeroPowerTextures());
  }

  public void RestoreOriginalTextures()
  {
    this.SetFrontTexture(this.m_OriginalFrontTexture);
    this.SetBackTexture(this.m_OriginalBackTexture);
  }

  public void SetFrontTextureFromPath(string path)
  {
    if (string.IsNullOrEmpty(path))
      return;
    AssetLoader.Get().LoadTexture(path, new AssetLoader.ObjectCallback(this.OnFrontTextureLoaded), (object) null, false);
  }

  public void SetBackTextureFromPath(string path)
  {
    if (string.IsNullOrEmpty(path))
      return;
    AssetLoader.Get().LoadTexture(path, new AssetLoader.ObjectCallback(this.OnBackTextureLoaded), (object) null, false);
  }

  public Texture GetFrontTexture()
  {
    return this.GetComponent<Renderer>().materials[0].mainTexture;
  }

  public Texture GetBackTexture()
  {
    return this.GetComponent<Renderer>().materials[2].mainTexture;
  }

  public void SetFrontTexture(Texture tex)
  {
    Renderer component = this.GetComponent<Renderer>();
    Material[] materials = component.materials;
    materials[0].mainTexture = tex;
    component.materials = materials;
  }

  public void SetBackTexture(Texture tex)
  {
    Renderer component = this.GetComponent<Renderer>();
    Material[] materials = component.materials;
    materials[1].SetTexture("_SecondTex", tex);
    materials[2].mainTexture = tex;
    component.materials = materials;
  }

  [DebuggerHidden]
  private IEnumerator HeroSkinCustomHeroPowerTextures()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroSkinHeroPower.\u003CHeroSkinCustomHeroPowerTextures\u003Ec__Iterator30D() { \u003C\u003Ef__this = this };
  }

  private void OnFrontTextureLoaded(string name, Object obj, object callbackData)
  {
    this.SetFrontTexture((Texture) (obj as Texture2D));
  }

  private void OnBackTextureLoaded(string name, Object obj, object callbackData)
  {
    this.SetBackTexture((Texture) (obj as Texture2D));
  }
}
