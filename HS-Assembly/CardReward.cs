﻿// Decompiled with JetBrains decompiler
// Type: CardReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class CardReward : Reward
{
  private static readonly Map<TAG_CARDTYPE, Vector3> CARD_SCALE = new Map<TAG_CARDTYPE, Vector3>() { { TAG_CARDTYPE.SPELL, new Vector3(1f, 1f, 1f) }, { TAG_CARDTYPE.MINION, new Vector3(1f, 1f, 1f) }, { TAG_CARDTYPE.WEAPON, new Vector3(1f, 0.5f, 1f) } };
  public bool m_showCardCount = true;
  public bool m_RotateIn = true;
  private List<Actor> m_actors = new List<Actor>();
  public GameObject m_nonHeroCardsRoot;
  public GameObject m_heroCardRoot;
  public GameObject m_cardParent;
  public GameObject m_duplicateCardParent;
  public CardRewardCount m_cardCount;
  private GameObject m_goToRotate;
  private CardSoundSpell m_emote;

  public void MakeActorsUnlit()
  {
    using (List<Actor>.Enumerator enumerator = this.m_actors.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetUnlit();
    }
  }

  protected override void InitData()
  {
    this.SetData((RewardData) new CardRewardData(), false);
  }

  protected override void OnDataSet(bool updateVisuals)
  {
    if (!updateVisuals)
      return;
    CardRewardData data = this.Data as CardRewardData;
    if (data == null)
      Debug.LogWarning((object) string.Format("CardReward.SetData() - data {0} is not CardRewardData", (object) this.Data));
    else if (string.IsNullOrEmpty(data.CardID))
    {
      Debug.LogWarning((object) string.Format("CardReward.SetData() - data {0} has invalid cardID", (object) data));
    }
    else
    {
      this.SetReady(false);
      EntityDef entityDef = DefLoader.Get().GetEntityDef(data.CardID);
      if (entityDef.IsHero())
      {
        AssetLoader.Get().LoadActor("Card_Play_Hero", new AssetLoader.GameObjectCallback(this.OnHeroActorLoaded), (object) entityDef, false);
        this.m_goToRotate = this.m_heroCardRoot;
        this.m_cardCount.Hide();
        if (data.Premium == TAG_PREMIUM.GOLDEN)
          this.SetUpGoldenHeroAchieves();
        else
          this.SetupHeroAchieves();
      }
      else
      {
        if ((bool) UniversalInputManager.UsePhoneUI || !this.m_showCardCount)
          this.m_cardCount.Hide();
        AssetLoader.Get().LoadActor(ActorNames.GetHandActor(entityDef, data.Premium), new AssetLoader.GameObjectCallback(this.OnActorLoaded), (object) entityDef, false);
        this.m_goToRotate = this.m_nonHeroCardsRoot;
      }
    }
  }

  protected override void ShowReward(bool updateCacheValues)
  {
    CardRewardData data = this.Data as CardRewardData;
    if (!data.IsDummyReward && updateCacheValues)
      CollectionManager.Get().AddCardReward(data, true);
    this.InitRewardText();
    if (data.FixedReward != null && data.FixedReward.UseQuestToast && (DefLoader.Get().GetEntityDef(data.CardID).IsHero() && (Object) this.m_rewardBanner != (Object) null))
      this.m_rewardBanner.gameObject.SetActive(false);
    if (!this.m_showCardCount)
      this.m_cardCount.Hide();
    this.m_root.SetActive(true);
    if (this.m_RotateIn)
    {
      this.m_goToRotate.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 180f);
      iTween.RotateAdd(this.m_goToRotate.gameObject, iTween.Hash((object) "amount", (object) new Vector3(0.0f, 0.0f, 540f), (object) "time", (object) 1.5f, (object) "easeType", (object) iTween.EaseType.easeOutElastic, (object) "space", (object) Space.Self));
    }
    SoundManager.Get().LoadAndPlay("game_end_reward");
    this.PlayHeroEmote();
  }

  protected override void HideReward()
  {
    base.HideReward();
    this.m_root.SetActive(false);
  }

  private void OnCardDefLoaded(string cardID, CardDef cardDef, object callbackData)
  {
    if (DefLoader.Get().GetEntityDef(cardID) == null)
    {
      Debug.LogWarning((object) string.Format("OnCardDefLoaded() - entityDef for CardID {0} is null", (object) cardID));
    }
    else
    {
      using (List<Actor>.Enumerator enumerator = this.m_actors.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.FinishSettingUpActor(enumerator.Current, cardDef);
      }
      using (List<EmoteEntryDef>.Enumerator enumerator = cardDef.m_EmoteDefs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          EmoteEntryDef current = enumerator.Current;
          if (current.m_emoteType == EmoteType.START)
            AssetLoader.Get().LoadSpell(current.m_emoteSoundSpellPath, new AssetLoader.GameObjectCallback(this.OnStartEmoteLoaded), (object) null, false);
        }
      }
      this.SetReady(true);
    }
  }

  private void OnStartEmoteLoaded(string name, GameObject go, object callbackData)
  {
    if ((Object) go == (Object) null)
      return;
    CardSoundSpell component = go.GetComponent<CardSoundSpell>();
    if ((Object) component == (Object) null)
      return;
    this.m_emote = component;
  }

  private void PlayHeroEmote()
  {
    if ((Object) this.m_emote == (Object) null)
      return;
    this.m_emote.Reactivate();
  }

  private void OnHeroActorLoaded(string name, GameObject go, object callbackData)
  {
    EntityDef entityDef = (EntityDef) callbackData;
    Actor component = go.GetComponent<Actor>();
    component.SetEntityDef(entityDef);
    component.transform.parent = this.m_heroCardRoot.transform;
    component.transform.localScale = Vector3.one;
    component.transform.localPosition = Vector3.zero;
    component.transform.localRotation = Quaternion.identity;
    component.TurnOffCollider();
    component.m_healthObject.SetActive(false);
    CardRewardData data = this.Data as CardRewardData;
    if (data.FixedReward != null && data.FixedReward.UseQuestToast)
    {
      PlatformDependentValue<Vector3> platformDependentValue1 = new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(2f, 2f, 2f), Phone = new Vector3(1.3f, 1.3f, 1.3f) };
      PlatformDependentValue<Vector3> platformDependentValue2 = new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(0.0f, 0.0f, -0.07f), Phone = new Vector3(0.0f, 0.0f, -0.3f) };
      component.transform.localScale = (Vector3) platformDependentValue1;
      component.transform.localPosition = (Vector3) platformDependentValue2;
    }
    SceneUtils.SetLayer(component.gameObject, GameLayer.IgnoreFullScreenEffects);
    this.m_actors.Add(component);
    DefLoader.Get().LoadCardDef(entityDef.GetCardId(), new DefLoader.LoadDefCallback<CardDef>(this.OnCardDefLoaded), (object) new CardPortraitQuality(3, true), (CardPortraitQuality) null);
  }

  private void OnActorLoaded(string name, GameObject go, object callbackData)
  {
    EntityDef entityDef = (EntityDef) callbackData;
    Actor component = go.GetComponent<Actor>();
    this.StartSettingUpNonHeroActor(component, entityDef, this.m_cardParent.transform);
    CardRewardData data = this.Data as CardRewardData;
    this.m_cardCount.SetCount(data.Count);
    if (data.Count > 1)
      this.StartSettingUpNonHeroActor(Object.Instantiate<Actor>(component), entityDef, this.m_duplicateCardParent.transform);
    DefLoader.Get().LoadCardDef(entityDef.GetCardId(), new DefLoader.LoadDefCallback<CardDef>(this.OnCardDefLoaded), (object) entityDef, new CardPortraitQuality(3, true));
  }

  private void StartSettingUpNonHeroActor(Actor actor, EntityDef entityDef, Transform parentTransform)
  {
    actor.SetEntityDef(entityDef);
    actor.transform.parent = parentTransform;
    actor.transform.localScale = CardReward.CARD_SCALE[entityDef.GetCardType()];
    actor.transform.localPosition = Vector3.zero;
    actor.transform.localRotation = Quaternion.identity;
    actor.TurnOffCollider();
    if (this.Data.Origin != NetCache.ProfileNotice.NoticeOrigin.ACHIEVEMENT)
      SceneUtils.SetLayer(actor.gameObject, GameLayer.IgnoreFullScreenEffects);
    this.m_actors.Add(actor);
  }

  private void FinishSettingUpActor(Actor actor, CardDef cardDef)
  {
    CardRewardData data = this.Data as CardRewardData;
    actor.SetCardDef(cardDef);
    actor.SetPremium(data.Premium);
    actor.UpdateAllComponents();
  }

  private void SetupHeroAchieves()
  {
    List<Achievement> achievesInGroup1 = AchieveManager.Get().GetAchievesInGroup(Achievement.AchType.UNLOCK_HERO);
    List<Achievement> achievesInGroup2 = AchieveManager.Get().GetAchievesInGroup(Achievement.AchType.UNLOCK_HERO, true);
    int count1 = achievesInGroup1.Count;
    int count2 = achievesInGroup2.Count;
    string className = GameStrings.GetClassName(DefLoader.Get().GetEntityDef((this.Data as CardRewardData).CardID).GetClass());
    this.SetRewardText(GameStrings.Format("GLOBAL_REWARD_HERO_HEADLINE", (object) className), GameStrings.Format("GLOBAL_REWARD_HERO_DETAILS", (object) count2, (object) count1), GameStrings.Format("GLOBAL_REWARD_HERO_SOURCE", (object) className));
  }

  private void SetUpGoldenHeroAchieves()
  {
    this.SetRewardText(GameStrings.Get("GLOBAL_REWARD_GOLDEN_HERO_HEADLINE"), string.Empty, string.Empty);
  }

  private void InitRewardText()
  {
    CardRewardData data = this.Data as CardRewardData;
    EntityDef entityDef = DefLoader.Get().GetEntityDef(data.CardID);
    if (entityDef.IsHero())
      return;
    string headline = GameStrings.Get("GLOBAL_REWARD_CARD_HEADLINE");
    string details = string.Empty;
    string empty = string.Empty;
    TAG_CARD_SET cardSet = entityDef.GetCardSet();
    TAG_CLASS tagClass = entityDef.GetClass();
    string className = GameStrings.GetClassName(tagClass);
    if (GameMgr.Get().IsTutorial())
      details = GameUtils.GetCurrentTutorialCardRewardDetails();
    else if (cardSet == TAG_CARD_SET.CORE)
    {
      int num = 20;
      int basicCardsIown = CollectionManager.Get().GetBasicCardsIOwn(tagClass);
      if (data.Premium == TAG_PREMIUM.GOLDEN)
      {
        details = string.Empty;
      }
      else
      {
        if (num == basicCardsIown)
          data.InnKeeperLine = CardRewardData.InnKeeperTrigger.CORE_CLASS_SET_COMPLETE;
        else if (basicCardsIown == 4)
          data.InnKeeperLine = CardRewardData.InnKeeperTrigger.SECOND_REWARD_EVER;
        details = GameStrings.Format("GLOBAL_REWARD_CORE_CARD_DETAILS", (object) basicCardsIown, (object) num, (object) className);
      }
    }
    string source;
    if (this.Data.Origin == NetCache.ProfileNotice.NoticeOrigin.LEVEL_UP)
    {
      TAG_CLASS originData = (TAG_CLASS) this.Data.OriginData;
      source = GameStrings.Format("GLOBAL_REWARD_CARD_LEVEL_UP", (object) GameUtils.GetHeroLevel(originData).CurrentLevel.Level.ToString(), (object) GameStrings.GetClassName(originData));
    }
    else
      source = string.Empty;
    this.SetRewardText(headline, details, source);
  }
}
