﻿// Decompiled with JetBrains decompiler
// Type: BnetPlayerChangelist
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;

public class BnetPlayerChangelist
{
  private List<BnetPlayerChange> m_changes = new List<BnetPlayerChange>();

  public List<BnetPlayerChange> GetChanges()
  {
    return this.m_changes;
  }

  public void AddChange(BnetPlayerChange change)
  {
    this.m_changes.Add(change);
  }

  public bool HasChange(BnetGameAccountId id)
  {
    return this.HasChange(BnetPresenceMgr.Get().GetPlayer(id));
  }

  public bool HasChange(BnetAccountId id)
  {
    return this.HasChange(BnetPresenceMgr.Get().GetPlayer(id));
  }

  public bool HasChange(BnetPlayer player)
  {
    return this.FindChange(player) != null;
  }

  public BnetPlayerChange FindChange(BnetGameAccountId id)
  {
    return this.FindChange(BnetPresenceMgr.Get().GetPlayer(id));
  }

  public BnetPlayerChange FindChange(BnetAccountId id)
  {
    return this.FindChange(BnetPresenceMgr.Get().GetPlayer(id));
  }

  public BnetPlayerChange FindChange(BnetPlayer player)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BnetPlayerChangelist.\u003CFindChange\u003Ec__AnonStorey467 changeCAnonStorey467 = new BnetPlayerChangelist.\u003CFindChange\u003Ec__AnonStorey467();
    // ISSUE: reference to a compiler-generated field
    changeCAnonStorey467.player = player;
    // ISSUE: reference to a compiler-generated field
    if (changeCAnonStorey467.player == null)
      return (BnetPlayerChange) null;
    // ISSUE: reference to a compiler-generated method
    return this.m_changes.Find(new Predicate<BnetPlayerChange>(changeCAnonStorey467.\u003C\u003Em__34B));
  }
}
