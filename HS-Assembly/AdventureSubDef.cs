﻿// Decompiled with JetBrains decompiler
// Type: AdventureSubDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class AdventureSubDef : MonoBehaviour
{
  [CustomEditField(Sections = "Chooser")]
  public Vector2 m_TextureTiling = Vector2.one;
  [CustomEditField(Sections = "Chooser")]
  public Vector2 m_TextureOffset = Vector2.zero;
  [CustomEditField(Sections = "Mission Display", T = EditType.TEXTURE)]
  public string m_WatermarkTexture;
  [CustomEditField(Sections = "Chooser", T = EditType.GAME_OBJECT)]
  public String_MobileOverride m_ChooserDescriptionPrefab;
  [CustomEditField(Sections = "Chooser", T = EditType.TEXTURE)]
  public string m_Texture;
  private AdventureModeDbId m_AdventureModeId;
  private int m_SortOrder;
  private string m_ShortName;
  private string m_Description;
  private string m_RequirementsDescription;
  private string m_CompleteBannerText;

  public void Init(AdventureDataDbfRecord advDataRecord)
  {
    this.m_AdventureModeId = (AdventureModeDbId) advDataRecord.ModeId;
    this.m_SortOrder = advDataRecord.SortOrder;
    this.m_ShortName = (string) advDataRecord.ShortName;
    this.m_Description = (string) (!(bool) UniversalInputManager.UsePhoneUI ? advDataRecord.Description : advDataRecord.ShortDescription);
    this.m_RequirementsDescription = (string) advDataRecord.RequirementsDescription;
    this.m_CompleteBannerText = (string) advDataRecord.CompleteBannerText;
  }

  public AdventureModeDbId GetAdventureModeId()
  {
    return this.m_AdventureModeId;
  }

  public int GetSortOrder()
  {
    return this.m_SortOrder;
  }

  public string GetShortName()
  {
    return this.m_ShortName;
  }

  public string GetDescription()
  {
    return this.m_Description;
  }

  public string GetRequirementsDescription()
  {
    return this.m_RequirementsDescription;
  }

  public string GetCompleteBannerText()
  {
    return this.m_CompleteBannerText;
  }
}
