﻿// Decompiled with JetBrains decompiler
// Type: BackBehavior
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BackBehavior : MonoBehaviour
{
  public void Awake()
  {
    PegUIElement component = this.gameObject.GetComponent<PegUIElement>();
    if (!((Object) component != (Object) null))
      return;
    component.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OnRelease()));
  }

  public void OnRelease()
  {
    Navigation.GoBack();
  }
}
