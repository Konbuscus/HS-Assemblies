﻿// Decompiled with JetBrains decompiler
// Type: UberShuriken
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UberShuriken : MonoBehaviour
{
  public float m_FollowCurvePositionAttraction = 0.5f;
  public float m_FollowCurvePositionIntensity = 1.7f;
  public AnimationCurve m_FollowCurvePositionOverLifetime = new AnimationCurve(new Keyframe[2]{ new Keyframe(1f, 1f), new Keyframe(1f, 1f) });
  public float m_CurlNoisePower = 1f;
  public AnimationCurve m_CurlNoiseOverLifetime = new AnimationCurve(new Keyframe[2]{ new Keyframe(0.0f, 0.0f), new Keyframe(1f, 1f) });
  public float m_CurlNoiseScale = 1f;
  public Vector3 m_CurlNoiseAnimation = Vector3.zero;
  public float m_TwinkleRate = 1f;
  public AnimationCurve m_TwinkleOverLifetime = new AnimationCurve(new Keyframe[2]{ new Keyframe(0.0f, 0.0f), new Keyframe(1f, 1f) });
  private List<ParticleSystem> m_particleSystems = new List<ParticleSystem>();
  private int m_curlNoiseIntervalIndex = 1;
  private const int FOLLOW_CURVE_INVERVAL = 3;
  private const int CURL_NOISE_INVERVAL = 3;
  public bool m_IncludeChildren;
  public UberCurve m_UberCurve;
  public bool m_FollowCurveDirection;
  public bool m_FollowCurvePosition;
  public bool m_CurlNoise;
  public bool m_Twinkle;
  [Range(-1f, 1f)]
  public float m_TwinkleBias;
  private float m_time;
  private int m_followCurveIntervalIndex;

  private void Awake()
  {
    if ((Object) this.m_UberCurve == (Object) null)
      this.m_UberCurve = this.GetComponent<UberCurve>();
    this.UpdateParticleSystemList();
  }

  private void Update()
  {
    this.m_time = Time.time;
    this.UpdateParticles();
  }

  private void UpdateParticles()
  {
    this.m_followCurveIntervalIndex = this.m_followCurveIntervalIndex + 1 <= 3 ? this.m_followCurveIntervalIndex + 1 : (this.m_followCurveIntervalIndex = 0);
    this.m_curlNoiseIntervalIndex = this.m_curlNoiseIntervalIndex + 1 <= 3 ? this.m_curlNoiseIntervalIndex + 1 : 0;
    using (List<ParticleSystem>.Enumerator enumerator = this.m_particleSystems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ParticleSystem current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          int particleCount = current.particleCount;
          if (particleCount == 0)
            break;
          ParticleSystem.Particle[] particles = new ParticleSystem.Particle[particleCount];
          current.GetParticles(particles);
          if (this.m_FollowCurveDirection || this.m_FollowCurvePosition)
            this.FollowCurveOverLife(current, particles, particleCount);
          if (this.m_CurlNoise)
            this.ParticleCurlNoise(current, particles, particleCount);
          if (this.m_Twinkle)
            this.ParticleTwinkle(current, particles, particleCount);
          current.SetParticles(particles, particleCount);
        }
      }
    }
  }

  private void UpdateParticleSystemList()
  {
    this.m_particleSystems.Clear();
    if (this.m_IncludeChildren)
    {
      ParticleSystem[] componentsInChildren = this.GetComponentsInChildren<ParticleSystem>();
      if ((Object) this.GetComponent<ParticleSystem>() == (Object) null || componentsInChildren.Length == 0)
        Debug.LogError((object) "Failed to find a ParticleSystem");
      foreach (ParticleSystem particleSystem in componentsInChildren)
        this.m_particleSystems.Add(particleSystem);
    }
    else
    {
      ParticleSystem component = this.GetComponent<ParticleSystem>();
      if ((Object) component == (Object) null)
        Debug.LogError((object) "Failed to find a ParticleSystem");
      this.m_particleSystems.Add(component);
    }
  }

  private void FollowCurveOverLife(ParticleSystem particleSystem, ParticleSystem.Particle[] particles, int particleCount)
  {
    if ((Object) this.m_UberCurve == (Object) null)
      this.CreateCurve();
    int curveIntervalIndex = this.m_followCurveIntervalIndex;
    while (curveIntervalIndex < particleCount)
    {
      float position = (float) (1.0 - (double) particles[curveIntervalIndex].lifetime / (double) particles[curveIntervalIndex].startLifetime);
      if (this.m_FollowCurvePosition)
      {
        Vector3 zero = Vector3.zero;
        Vector3 b = (particleSystem.simulationSpace != ParticleSystemSimulationSpace.World ? this.m_UberCurve.CatmullRomEvaluateLocalPosition(position) : this.m_UberCurve.CatmullRomEvaluateWorldPosition(position)) - particles[curveIntervalIndex].position;
        Vector3 vector3 = Vector3.Lerp(particles[curveIntervalIndex].velocity, b, this.m_FollowCurvePositionAttraction);
        particles[curveIntervalIndex].velocity = vector3 * this.m_FollowCurvePositionIntensity;
      }
      if (this.m_FollowCurveDirection)
      {
        Vector3 vector3 = this.m_UberCurve.CatmullRomEvaluateDirection(position).normalized * particles[curveIntervalIndex].velocity.magnitude;
        particles[curveIntervalIndex].velocity = vector3;
      }
      curveIntervalIndex += 3;
    }
  }

  private void CreateCurve()
  {
    if ((Object) this.m_UberCurve != (Object) null)
      return;
    this.m_UberCurve = this.GetComponent<UberCurve>();
    if ((Object) this.m_UberCurve != (Object) null)
      return;
    this.m_UberCurve = this.gameObject.AddComponent<UberCurve>();
  }

  private void ParticleCurlNoise(ParticleSystem particleSystem, ParticleSystem.Particle[] particles, int particleCount)
  {
    float time = this.m_time;
    float num1 = this.m_CurlNoiseAnimation.x * time;
    float num2 = this.m_CurlNoiseAnimation.y * time;
    float num3 = this.m_CurlNoiseAnimation.z * time;
    int noiseIntervalIndex = this.m_curlNoiseIntervalIndex;
    while (noiseIntervalIndex < particleCount)
    {
      float num4 = this.m_CurlNoiseOverLifetime.Evaluate((float) (1.0 - (double) particles[noiseIntervalIndex].lifetime / (double) particles[noiseIntervalIndex].startLifetime)) * this.m_CurlNoisePower;
      Vector3 velocity = particles[noiseIntervalIndex].velocity;
      Vector3 vector3_1 = particles[noiseIntervalIndex].position * this.m_CurlNoiseScale * 0.1f;
      velocity.x += UberMath.SimplexNoise(vector3_1.x + num1, vector3_1.y + num2, vector3_1.z + num3) * num4;
      velocity.y += UberMath.SimplexNoise(vector3_1.y + num1, vector3_1.z + num2, vector3_1.x + num3) * num4;
      velocity.z += UberMath.SimplexNoise(vector3_1.z + num1, vector3_1.x + num2, vector3_1.y + num3) * num4;
      Vector3 vector3_2 = velocity.normalized * particles[noiseIntervalIndex].velocity.magnitude;
      particles[noiseIntervalIndex].velocity = vector3_2;
      noiseIntervalIndex += 3;
    }
  }

  private void ParticleTwinkle(ParticleSystem particleSystem, ParticleSystem.Particle[] particles, int particleCount)
  {
    for (int index = 0; index < particleCount; ++index)
    {
      float time = particles[index].lifetime / particles[index].startLifetime;
      Vector3 position = particles[index].position;
      Color startColor = (Color) particles[index].startColor;
      startColor.a = Mathf.Clamp01((float) ((double) UberMath.SimplexNoise((float) ((double) position.x + (double) position.y + (double) position.z - (double) time - (double) index * 3.32999992370605) * this.m_TwinkleRate, 0.5f) + (double) this.m_TwinkleBias + (double) time * (double) this.m_TwinkleOverLifetime.Evaluate(time)));
      particles[index].startColor = (Color32) startColor;
    }
  }
}
