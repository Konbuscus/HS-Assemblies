﻿// Decompiled with JetBrains decompiler
// Type: ArenaStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System.Collections.Generic;
using UnityEngine;

public class ArenaStore : Store
{
  private static readonly int NUM_BUNDLE_ITEMS_REQUIRED = 1;
  public UIBButton m_backButton;
  public GameObject m_storeClosed;
  private NoGTAPPTransactionData m_goldTransactionData;
  private Network.Bundle m_bundle;
  private static ArenaStore s_instance;

  protected override void Start()
  {
    base.Start();
    this.m_backButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBackPressed));
  }

  protected override void Awake()
  {
    ArenaStore.s_instance = this;
    this.m_destroyOnSceneLoad = false;
    base.Awake();
    this.m_backButton.SetText(GameStrings.Get("GLOBAL_BACK"));
  }

  protected override void OnDestroy()
  {
    ArenaStore.s_instance = (ArenaStore) null;
  }

  public static ArenaStore Get()
  {
    return ArenaStore.s_instance;
  }

  public override void Hide()
  {
    if ((Object) ShownUIMgr.Get() != (Object) null)
      ShownUIMgr.Get().ClearShownUI();
    FriendChallengeMgr.Get().OnStoreClosed();
    StoreManager.Get().RemoveAuthorizationExitListener(new StoreManager.AuthorizationExitCallback(this.OnAuthExit));
    this.EnableFullScreenEffects(false);
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      BnetBar.Get().SetCurrencyType(new CurrencyFrame.CurrencyType?());
      BnetBar.Get().UpdateForPhone();
    }
    base.Hide();
  }

  public override void OnMoneySpent()
  {
    this.UpdateMoneyButtonState();
  }

  public override void OnGoldBalanceChanged(NetCache.NetCacheGoldBalance balance)
  {
    this.UpdateGoldButtonState(balance);
  }

  protected override void ShowImpl(Store.DelOnStoreShown onStoreShownCB, bool isTotallyFake)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ArenaStore.\u003CShowImpl\u003Ec__AnonStorey3DE implCAnonStorey3De = new ArenaStore.\u003CShowImpl\u003Ec__AnonStorey3DE();
    // ISSUE: reference to a compiler-generated field
    implCAnonStorey3De.isTotallyFake = isTotallyFake;
    // ISSUE: reference to a compiler-generated field
    implCAnonStorey3De.onStoreShownCB = onStoreShownCB;
    // ISSUE: reference to a compiler-generated field
    implCAnonStorey3De.\u003C\u003Ef__this = this;
    this.m_shown = true;
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    StoreManager.Get().RegisterAuthorizationExitListener(new StoreManager.AuthorizationExitCallback(this.OnAuthExit));
    this.EnableFullScreenEffects(true);
    this.SetUpBuyButtons();
    ShownUIMgr.Get().SetShownUI(ShownUIMgr.UI_WINDOW.ARENA_STORE);
    FriendChallengeMgr.Get().OnStoreOpened();
    // ISSUE: reference to a compiler-generated method
    this.DoShowAnimation(new UIBPopup.OnAnimationComplete(implCAnonStorey3De.\u003C\u003Em__19F));
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    BnetBar.Get().SetCurrencyType(new CurrencyFrame.CurrencyType?(CurrencyFrame.CurrencyType.GOLD));
    BnetBar.Get().UpdateForPhone();
  }

  protected override void BuyWithGold(UIEvent e)
  {
    if (this.m_goldTransactionData == null)
      return;
    this.FireBuyWithGoldEventNoGTAPP(this.m_goldTransactionData);
  }

  protected override void BuyWithMoney(UIEvent e)
  {
    if (this.m_bundle == null)
      return;
    this.FireBuyWithMoneyEvent(this.m_bundle.ProductID, 1);
  }

  private void OnAuthExit(object userData)
  {
    Navigation.Pop();
    this.ExitForgeStore(true);
  }

  private void OnBackPressed(UIEvent e)
  {
    Navigation.GoBack();
  }

  private bool OnNavigateBack()
  {
    this.ExitForgeStore(false);
    return true;
  }

  private void ExitForgeStore(bool authorizationBackButtonPressed)
  {
    this.ActivateCover(false);
    SceneUtils.SetLayer(this.gameObject, GameLayer.Default);
    this.EnableFullScreenEffects(false);
    StoreManager.Get().RemoveAuthorizationExitListener(new StoreManager.AuthorizationExitCallback(this.OnAuthExit));
    this.FireExitEvent(authorizationBackButtonPressed);
  }

  private void UpdateMoneyButtonState()
  {
    Store.BuyButtonState state = Store.BuyButtonState.ENABLED;
    if (this.m_bundle == null || !StoreManager.Get().IsOpen())
    {
      state = Store.BuyButtonState.DISABLED;
      this.m_storeClosed.SetActive(true);
    }
    else if (!StoreManager.Get().IsBattlePayFeatureEnabled())
      state = Store.BuyButtonState.DISABLED_FEATURE;
    else if (StoreManager.Get().IsPromptShowing())
    {
      state = Store.BuyButtonState.DISABLED;
      this.SetGoldButtonState(state);
    }
    else
      this.m_storeClosed.SetActive(false);
    this.SetMoneyButtonState(state);
  }

  private void UpdateGoldButtonState(NetCache.NetCacheGoldBalance balance)
  {
    Store.BuyButtonState state = Store.BuyButtonState.ENABLED;
    if (this.m_goldTransactionData == null)
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsOpen())
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsBuyWithGoldFeatureEnabled())
      state = Store.BuyButtonState.DISABLED_FEATURE;
    else if (balance == null)
    {
      state = Store.BuyButtonState.DISABLED;
    }
    else
    {
      long cost;
      if (!StoreManager.Get().GetGoldCostNoGTAPP(this.m_goldTransactionData, out cost))
        state = Store.BuyButtonState.DISABLED_NO_TOOLTIP;
      else if (balance.GetTotal() < cost)
        state = Store.BuyButtonState.DISABLED_NOT_ENOUGH_GOLD;
      else if (StoreManager.Get().IsPromptShowing())
      {
        state = Store.BuyButtonState.DISABLED;
        this.SetMoneyButtonState(state);
      }
    }
    this.SetGoldButtonState(state);
  }

  private void SetUpBuyButtons()
  {
    this.SetUpBuyWithGoldButton();
    this.SetUpBuyWithMoneyButton();
  }

  private void SetUpBuyWithGoldButton()
  {
    string empty = string.Empty;
    NoGTAPPTransactionData noGTAPPTransactionData = new NoGTAPPTransactionData() { Product = ProductType.PRODUCT_TYPE_DRAFT, ProductData = 0, Quantity = 1 };
    long cost;
    string text;
    if (StoreManager.Get().GetGoldCostNoGTAPP(noGTAPPTransactionData, out cost))
    {
      this.m_goldTransactionData = noGTAPPTransactionData;
      text = cost.ToString();
      this.UpdateGoldButtonState(NetCache.Get().GetNetObject<NetCache.NetCacheGoldBalance>());
    }
    else
    {
      Debug.LogWarning((object) "ForgeStore.SetUpBuyWithGoldButton(): no gold price for purchase Arena without GTAPP");
      text = GameStrings.Get("GLUE_STORE_PRODUCT_PRICE_NA");
      this.SetGoldButtonState(Store.BuyButtonState.DISABLED);
    }
    this.m_buyWithGoldButton.SetText(text);
  }

  private void SetUpBuyWithMoneyButton()
  {
    bool productTypeExists = false;
    List<Network.Bundle> bundlesForProduct = StoreManager.Get().GetAvailableBundlesForProduct(ProductType.PRODUCT_TYPE_DRAFT, true, false, out productTypeExists, 0, ArenaStore.NUM_BUNDLE_ITEMS_REQUIRED);
    string empty = string.Empty;
    string text;
    if (bundlesForProduct.Count == 1)
    {
      this.m_bundle = bundlesForProduct[0];
      text = StoreManager.Get().FormatCostBundle(this.m_bundle);
      this.UpdateMoneyButtonState();
    }
    else
    {
      Debug.LogWarning((object) string.Format("ForgeStore.SetUpBuyWithMoneyButton(): expecting 1 bundle for purchasing Forge, found {0}", (object) bundlesForProduct.Count));
      text = GameStrings.Get("GLUE_STORE_PRODUCT_PRICE_NA");
      this.SetMoneyButtonState(Store.BuyButtonState.DISABLED);
    }
    this.m_buyWithMoneyButton.SetText(text);
  }
}
