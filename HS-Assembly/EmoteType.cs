﻿// Decompiled with JetBrains decompiler
// Type: EmoteType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum EmoteType
{
  INVALID,
  GREETINGS,
  WELL_PLAYED,
  OOPS,
  THREATEN,
  THANKS,
  SORRY,
  CONCEDE,
  START,
  TIMER,
  THINK1,
  THINK2,
  THINK3,
  GOOD_GAME,
  LOW_CARDS,
  NO_CARDS,
  ERROR_NEED_WEAPON,
  ERROR_NEED_MANA,
  ERROR_MINION_ATTACKED,
  ERROR_I_ATTACKED,
  ERROR_JUST_PLAYED,
  ERROR_HAND_FULL,
  ERROR_FULL_MINIONS,
  ERROR_STEALTH,
  ERROR_PLAY,
  ERROR_TARGET,
  ERROR_TAUNT,
  ERROR_GENERIC,
  PICKED,
  DEATH_LINE,
  EVENT_LUNAR_NEW_YEAR,
  MIRROR_START,
  EVENT_WINTER_VEIL,
  WOW,
}
