﻿// Decompiled with JetBrains decompiler
// Type: DeckRule_CountCopiesOfEachCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DeckRule_CountCopiesOfEachCard : DeckRule
{
  public DeckRule_CountCopiesOfEachCard(int subset, int min, int max)
  {
    this.m_ruleType = DeckRule.RuleType.COUNT_COPIES_OF_EACH_CARD;
    this.m_appliesToSubsetId = subset;
    this.m_minValue = min;
    this.m_maxValue = max;
    if (this.m_appliesToSubsetId == 0)
      return;
    this.m_appliesToSubset = GameDbf.GetIndex().GetSubsetById(this.m_appliesToSubsetId);
  }

  public DeckRule_CountCopiesOfEachCard(DeckRulesetRuleDbfRecord record)
    : base(DeckRule.RuleType.COUNT_COPIES_OF_EACH_CARD, record)
  {
  }

  public override bool CanAddToDeck(EntityDef def, TAG_PREMIUM premium, CollectionDeck deck, out RuleInvalidReason reason)
  {
    reason = (RuleInvalidReason) null;
    if (!this.AppliesTo(def.GetCardId()))
      return true;
    bool flag = deck.GetCardIdCount(def.GetCardId(), true) >= this.m_maxValue;
    if (flag)
      reason = new RuleInvalidReason(GameStrings.Format("GLUE_COLLECTION_LOCK_MAX_DECK_COPIES", (object) this.m_maxValue), this.m_maxValue, false);
    return this.GetResult(!flag);
  }

  public bool GetMaxCopies(EntityDef def, out int maxCopies)
  {
    maxCopies = int.MaxValue;
    if (!this.AppliesTo(def.GetCardId()))
      return false;
    maxCopies = this.m_maxValue;
    return true;
  }

  public override bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason)
  {
    reason = (RuleInvalidReason) null;
    bool val = true;
    List<CollectionDeckSlot> slots = deck.GetSlots();
    int countParam = 0;
    bool isMinimum = false;
    using (List<CollectionDeckSlot>.Enumerator enumerator = slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string cardId = enumerator.Current.CardID;
        if (this.AppliesTo(cardId))
        {
          int cardIdCount = deck.GetCardIdCount(cardId, true);
          if (cardIdCount < this.m_minValue)
          {
            val = false;
            countParam = this.m_minValue - cardIdCount;
            isMinimum = true;
            break;
          }
          if (cardIdCount > this.m_maxValue)
          {
            val = false;
            int maxValue;
            countParam = maxValue = this.m_maxValue;
            break;
          }
        }
      }
    }
    bool result = this.GetResult(val);
    if (!result)
      reason = new RuleInvalidReason(this.m_errorString, countParam, isMinimum);
    return result;
  }
}
