﻿// Decompiled with JetBrains decompiler
// Type: AdventureGenericButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[CustomEditClass]
public class AdventureGenericButton : PegUIElement
{
  [CustomEditField(Sections = "Portrait Settings")]
  public AdventureGenericButton.MaterialProperties m_MaterialProperties = new AdventureGenericButton.MaterialProperties();
  [CustomEditField(Sections = "Border Settings")]
  public AdventureGenericButton.MaterialProperties m_BorderMaterialProperties = new AdventureGenericButton.MaterialProperties();
  [CustomEditField(Sections = "Text Settings")]
  public Color m_NormalTextColor = new Color();
  public Color m_DisabledTextColor = new Color();
  private bool m_PortraitLoaded = true;
  private const string s_DefaultPortraitMaterialTextureName = "_MainTex";
  private const int s_DefaultPortraitMaterialIndex = 1;
  [CustomEditField(Sections = "Portrait Settings")]
  public MeshRenderer m_PortraitRenderer;
  [CustomEditField(Sections = "Border Settings")]
  public MeshRenderer m_BorderRenderer;
  [CustomEditField(Sections = "Text Settings")]
  public UberText m_ButtonTextObject;

  public bool IsPortraitLoaded()
  {
    return this.m_PortraitLoaded;
  }

  public void SetDesaturate(bool desaturate)
  {
    if (!this.CheckValidMaterialProperties(this.m_MaterialProperties) || !this.CheckValidMaterialProperties(this.m_BorderMaterialProperties))
      return;
    this.m_PortraitRenderer.materials[this.m_MaterialProperties.m_MaterialIndex].SetFloat("_Desaturate", !desaturate ? 0.0f : 1f);
    this.m_BorderRenderer.materials[this.m_BorderMaterialProperties.m_MaterialIndex].SetFloat("_Desaturate", !desaturate ? 0.0f : 1f);
    this.m_ButtonTextObject.TextColor = !desaturate ? this.m_NormalTextColor : this.m_DisabledTextColor;
  }

  public void SetContrast(float contrast)
  {
    if (!this.CheckValidMaterialProperties(this.m_MaterialProperties) || !this.CheckValidMaterialProperties(this.m_BorderMaterialProperties))
      return;
    this.m_PortraitRenderer.materials[this.m_MaterialProperties.m_MaterialIndex].SetFloat("_Contrast", contrast);
    this.m_BorderRenderer.materials[this.m_BorderMaterialProperties.m_MaterialIndex].SetFloat("_Contrast", contrast);
  }

  public void SetButtonText(string str)
  {
    if ((UnityEngine.Object) this.m_ButtonTextObject == (UnityEngine.Object) null)
      return;
    this.m_ButtonTextObject.Text = str;
  }

  public void SetPortraitTexture(string texturename)
  {
    this.SetPortraitTexture(texturename, this.m_MaterialProperties.m_MaterialIndex, this.m_MaterialProperties.m_MaterialPropertyName);
  }

  public void SetPortraitTexture(string texturename, int index, string mattexprop)
  {
    if (texturename == null || texturename.Length == 0)
      return;
    texturename = FileUtils.GameAssetPathToName(texturename);
    AdventureGenericButton.MaterialProperties matprop = new AdventureGenericButton.MaterialProperties()
    {
      m_MaterialIndex = index,
      m_MaterialPropertyName = mattexprop
    };
    if (!this.CheckValidMaterialProperties(matprop))
      return;
    this.m_PortraitLoaded = false;
    AssetLoader.Get().LoadTexture(texturename, new AssetLoader.ObjectCallback(this.ApplyPortraitTexture), (object) matprop, false);
  }

  public void SetPortraitTiling(Vector2 tiling)
  {
    this.SetPortraitTiling(tiling, this.m_MaterialProperties.m_MaterialIndex, this.m_MaterialProperties.m_MaterialPropertyName);
  }

  public void SetPortraitTiling(Vector2 tiling, int index, string mattexprop)
  {
    AdventureGenericButton.MaterialProperties matprop = new AdventureGenericButton.MaterialProperties()
    {
      m_MaterialIndex = index,
      m_MaterialPropertyName = mattexprop
    };
    if (!this.CheckValidMaterialProperties(matprop))
      return;
    this.m_PortraitRenderer.materials[matprop.m_MaterialIndex].SetTextureScale(matprop.m_MaterialPropertyName, tiling);
  }

  public void SetPortraitOffset(Vector2 offset)
  {
    this.SetPortraitOffset(offset, this.m_MaterialProperties.m_MaterialIndex, this.m_MaterialProperties.m_MaterialPropertyName);
  }

  public void SetPortraitOffset(Vector2 offset, int index, string mattexprop)
  {
    AdventureGenericButton.MaterialProperties matprop = new AdventureGenericButton.MaterialProperties()
    {
      m_MaterialIndex = index,
      m_MaterialPropertyName = mattexprop
    };
    if (!this.CheckValidMaterialProperties(matprop))
      return;
    this.m_PortraitRenderer.materials[matprop.m_MaterialIndex].SetTextureOffset(matprop.m_MaterialPropertyName, offset);
  }

  private void ApplyPortraitTexture(string name, UnityEngine.Object obj, object userdata)
  {
    this.m_PortraitLoaded = true;
    AdventureGenericButton.MaterialProperties materialProperties = userdata as AdventureGenericButton.MaterialProperties;
    Texture texture = obj as Texture;
    if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
      Debug.LogError((object) string.Format("Unable to load portrait texture {0}.", (object) name), obj);
    else
      this.m_PortraitRenderer.materials[materialProperties.m_MaterialIndex].SetTexture(materialProperties.m_MaterialPropertyName, texture);
  }

  private bool CheckValidMaterialProperties(AdventureGenericButton.MaterialProperties matprop)
  {
    if ((UnityEngine.Object) this.m_PortraitRenderer == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "No portrait mesh renderer set.");
      return false;
    }
    if (matprop.m_MaterialIndex < this.m_PortraitRenderer.materials.Length)
      return true;
    Debug.LogError((object) string.Format("Unable to find material index {0}", (object) matprop.m_MaterialIndex));
    return false;
  }

  [Serializable]
  public class MaterialProperties
  {
    public int m_MaterialIndex = 1;
    public string m_MaterialPropertyName = "_MainTex";
  }
}
