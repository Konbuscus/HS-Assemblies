﻿// Decompiled with JetBrains decompiler
// Type: ArtVerificationManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class ArtVerificationManager : MonoBehaviour
{
  private const float START_DELAY_SEC = 1f;
  private int m_cardsToLoad;

  private void Start()
  {
    this.StartCoroutine(this.StartVerification());
  }

  [DebuggerHidden]
  private IEnumerator StartVerification()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ArtVerificationManager.\u003CStartVerification\u003Ec__Iterator34A() { \u003C\u003Ef__this = this };
  }

  private void LoadCards()
  {
    GameDbf.Load(false);
    List<string> allCardIds = GameUtils.GetAllCardIds();
    this.m_cardsToLoad = allCardIds.Count;
    using (List<string>.Enumerator enumerator = allCardIds.GetEnumerator())
    {
      while (enumerator.MoveNext())
        DefLoader.Get().LoadCardDef(enumerator.Current, new DefLoader.LoadDefCallback<CardDef>(this.OnCardDefLoaded), (object) null, new CardPortraitQuality(3, TAG_PREMIUM.GOLDEN));
    }
  }

  private void OnCardDefLoaded(string cardID, CardDef def, object userData)
  {
    --this.m_cardsToLoad;
    this.CleanUpCard(def);
    if (this.m_cardsToLoad > 0)
      return;
    this.FinishVerification();
  }

  private void CleanUpCard(CardDef def)
  {
    if (!(bool) ((Object) def))
      return;
    Object.Destroy((Object) def);
  }

  private void FinishVerification()
  {
    UnityEngine.Debug.Log((object) "Finished");
    GeneralUtils.ExitApplication();
  }
}
