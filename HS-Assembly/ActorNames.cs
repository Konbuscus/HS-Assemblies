﻿// Decompiled with JetBrains decompiler
// Type: ActorNames
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ActorNames
{
  public const string INVISIBLE = "Card_Invisible";
  public const string HIDDEN = "Card_Hidden";
  public const string HAND_MINION = "Card_Hand_Ally";
  public const string HAND_SPELL = "Card_Hand_Ability";
  public const string HAND_WEAPON = "Card_Hand_Weapon";
  public const string HAND_FATIGUE = "Card_Hand_Fatigue";
  public const string PLAY_MINION = "Card_Play_Ally";
  public const string PLAY_WEAPON = "Card_Play_Weapon";
  public const string PLAY_HERO = "Card_Play_Hero";
  public const string PLAY_HERO_POWER = "Card_Play_HeroPower";
  public const string PLAY_ENCHANTMENT = "Card_Play_Enchantment";
  public const string PLAY_OBFUSCATED = "Card_Play_Obfuscated";
  public const string SECRET = "Card_Play_Secret_Mage";
  public const string SECRET_HUNTER = "Card_Play_Secret_Hunter";
  public const string SECRET_MAGE = "Card_Play_Secret_Mage";
  public const string SECRET_PALADIN = "Card_Play_Secret_Paladin";
  public const string SECRET_WANDERER = "Card_Play_Secret_Wanderer";
  public const string TOOLTIP = "CardTooltip";
  public const string HISTORY_HERO = "History_Hero";
  public const string HISTORY_HERO_POWER = "History_HeroPower";
  public const string HISTORY_HERO_POWER_OPPONENT = "History_HeroPower_Opponent";
  public const string HISTORY_SECRET_HUNTER = "History_Secret_Hunter";
  public const string HISTORY_SECRET_MAGE = "History_Secret_Mage";
  public const string HISTORY_SECRET_PALADIN = "History_Secret_Paladin";
  public const string HISTORY_SECRET_WANDERER = "History_Secret_Wanderer";
  public const string HISTORY_OBFUSCATED = "History_Obfuscated";
  public const string HEROPICKER_HERO = "HeroPicker_Hero";
  public const string COLLECTION_CARD_BACK = "Collection_Card_Back";
  public const string COLLECTION_DECK_TILE = "DeckCardBar";
  public const string COLLECTION_DECK_TILE_PHONE = "DeckCardBar_phone";
  public const string HERO_SKIN = "Card_Hero_Skin";

  public static string GetZoneActor(TAG_CARDTYPE cardType, TAG_CLASS classTag, TAG_ZONE zoneTag, Player controller, TAG_PREMIUM premium)
  {
    switch (zoneTag)
    {
      case TAG_ZONE.PLAY:
        if (cardType == TAG_CARDTYPE.MINION)
          return ActorNames.GetNameWithPremiumType("Card_Play_Ally", premium);
        if (cardType == TAG_CARDTYPE.WEAPON)
          return ActorNames.GetNameWithPremiumType("Card_Play_Weapon", premium);
        if (cardType == TAG_CARDTYPE.SPELL)
          return "Card_Invisible";
        if (cardType == TAG_CARDTYPE.HERO)
          return "Card_Play_Hero";
        if (cardType == TAG_CARDTYPE.HERO_POWER)
          return ActorNames.GetNameWithPremiumType("Card_Play_HeroPower", premium);
        if (cardType == TAG_CARDTYPE.ENCHANTMENT)
          return "Card_Play_Enchantment";
        break;
      case TAG_ZONE.DECK:
      case TAG_ZONE.REMOVEDFROMGAME:
      case TAG_ZONE.SETASIDE:
        return "Card_Invisible";
      case TAG_ZONE.HAND:
        if (controller == null || !controller.IsFriendlySide() && !controller.HasTag(GAME_TAG.HAND_REVEALED) && !SpectatorManager.Get().IsSpectatingOpposingSide())
          return "Card_Hidden";
        if (cardType == TAG_CARDTYPE.MINION)
          return ActorNames.GetNameWithPremiumType("Card_Hand_Ally", premium);
        if (cardType == TAG_CARDTYPE.WEAPON)
          return ActorNames.GetNameWithPremiumType("Card_Hand_Weapon", premium);
        if (cardType == TAG_CARDTYPE.SPELL)
          return ActorNames.GetNameWithPremiumType("Card_Hand_Ability", premium);
        if (cardType == TAG_CARDTYPE.HERO_POWER)
          return ActorNames.GetNameWithPremiumType("History_HeroPower", premium);
        break;
      case TAG_ZONE.GRAVEYARD:
        if (cardType == TAG_CARDTYPE.MINION)
          return ActorNames.GetNameWithPremiumType("Card_Hand_Ally", premium);
        if (cardType == TAG_CARDTYPE.WEAPON)
          return ActorNames.GetNameWithPremiumType("Card_Hand_Weapon", premium);
        if (cardType == TAG_CARDTYPE.SPELL)
          return ActorNames.GetNameWithPremiumType("Card_Hand_Ability", premium);
        if (cardType == TAG_CARDTYPE.HERO)
          return "Card_Play_Hero";
        break;
      case TAG_ZONE.SECRET:
        if (classTag == TAG_CLASS.HUNTER)
          return "Card_Play_Secret_Hunter";
        if (classTag == TAG_CLASS.MAGE)
          return "Card_Play_Secret_Mage";
        if (classTag == TAG_CLASS.PALADIN)
          return "Card_Play_Secret_Paladin";
        return classTag == TAG_CLASS.WARRIOR ? "Card_Play_Secret_Wanderer" : "Card_Play_Secret_Mage";
    }
    Debug.LogWarningFormat("ActorNames.GetZoneActor() - Can't determine actor for {0}. Returning {1} instead.", new object[2]
    {
      (object) cardType,
      (object) "Card_Hidden"
    });
    return "Card_Hidden";
  }

  private static bool ShouldObfuscate(Entity entity)
  {
    return entity.GetController() != null && !entity.GetController().IsFriendlySide() && entity.IsObfuscated();
  }

  public static string GetZoneActor(Entity entity, TAG_ZONE zoneTag)
  {
    if (ActorNames.ShouldObfuscate(entity) && zoneTag == TAG_ZONE.PLAY)
      return "Card_Play_Obfuscated";
    return ActorNames.GetZoneActor(entity.GetCardType(), entity.GetClass(), zoneTag, entity.GetController(), entity.GetPremiumType());
  }

  public static string GetZoneActor(EntityDef entityDef, TAG_ZONE zoneTag)
  {
    return ActorNames.GetZoneActor(entityDef.GetCardType(), entityDef.GetClass(), zoneTag, (Player) null, TAG_PREMIUM.NORMAL);
  }

  public static string GetZoneActor(EntityDef entityDef, TAG_ZONE zoneTag, TAG_PREMIUM premium)
  {
    return ActorNames.GetZoneActor(entityDef.GetCardType(), entityDef.GetClass(), zoneTag, (Player) null, premium);
  }

  public static string GetHandActor(TAG_CARDTYPE cardType, TAG_PREMIUM premiumType)
  {
    switch (cardType)
    {
      case TAG_CARDTYPE.HERO:
        return "History_Hero";
      case TAG_CARDTYPE.MINION:
        return ActorNames.GetNameWithPremiumType("Card_Hand_Ally", premiumType);
      case TAG_CARDTYPE.SPELL:
        return ActorNames.GetNameWithPremiumType("Card_Hand_Ability", premiumType);
      case TAG_CARDTYPE.WEAPON:
        return ActorNames.GetNameWithPremiumType("Card_Hand_Weapon", premiumType);
      case TAG_CARDTYPE.HERO_POWER:
        return "History_HeroPower";
      default:
        return "Card_Hidden";
    }
  }

  public static string GetHandActor(TAG_CARDTYPE cardType)
  {
    return ActorNames.GetHandActor(cardType, TAG_PREMIUM.NORMAL);
  }

  public static string GetHandActor(Entity entity)
  {
    return ActorNames.GetHandActor(entity.GetCardType(), entity.GetPremiumType());
  }

  public static string GetHandActor(EntityDef entityDef)
  {
    return ActorNames.GetHandActor(entityDef.GetCardType(), TAG_PREMIUM.NORMAL);
  }

  public static string GetHandActor(EntityDef entityDef, TAG_PREMIUM premiumType)
  {
    return ActorNames.GetHandActor(entityDef.GetCardType(), premiumType);
  }

  public static string GetHeroSkinOrHandActor(TAG_CARDTYPE type, TAG_PREMIUM premium)
  {
    if (type == TAG_CARDTYPE.HERO)
      return "Card_Hero_Skin";
    return ActorNames.GetHandActor(type, premium);
  }

  public static string GetBigCardActor(Entity entity)
  {
    return ActorNames.GetHistoryActor(entity);
  }

  public static string GetHistoryActor(Entity entity)
  {
    if (entity.IsSecret() && entity.IsHidden())
      return ActorNames.GetHistorySecretActor(entity);
    if (ActorNames.ShouldObfuscate(entity))
      return "History_Obfuscated";
    TAG_CARDTYPE cardType = entity.GetCardType();
    TAG_PREMIUM premiumType = entity.GetPremiumType();
    switch (cardType)
    {
      case TAG_CARDTYPE.HERO:
        return "History_Hero";
      case TAG_CARDTYPE.HERO_POWER:
        if (entity.GetController().IsFriendlySide())
          return ActorNames.GetNameWithPremiumType("History_HeroPower", premiumType);
        return ActorNames.GetNameWithPremiumType("History_HeroPower_Opponent", premiumType);
      default:
        return ActorNames.GetHandActor(entity);
    }
  }

  public static string GetHistorySecretActor(Entity entity)
  {
    TAG_CLASS tagClass = entity.GetClass();
    switch (tagClass)
    {
      case TAG_CLASS.HUNTER:
        return "History_Secret_Hunter";
      case TAG_CLASS.MAGE:
        return "History_Secret_Mage";
      case TAG_CLASS.PALADIN:
        return "History_Secret_Paladin";
      default:
        if (entity.IsDarkWandererSecret())
          return "History_Secret_Wanderer";
        Debug.LogWarning((object) string.Format("ActorNames.GetHistorySecretActor() - No actor for class {0}. Returning {1} instead.", (object) tagClass, (object) "History_Secret_Mage"));
        return "History_Secret_Mage";
    }
  }

  public static string GetNameWithPremiumType(string actorName, TAG_PREMIUM premiumType)
  {
    if (premiumType == TAG_PREMIUM.GOLDEN)
      return string.Format("{0}_Premium", (object) actorName);
    return actorName;
  }
}
