﻿// Decompiled with JetBrains decompiler
// Type: PowerSpellController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PowerSpellController : SpellController
{
  private List<CardSoundSpell> m_powerSoundSpells = new List<CardSoundSpell>();
  private Spell m_powerSpell;
  private int m_cardEffectsBlockingFinish;
  private int m_cardEffectsBlockingTaskListFinish;

  protected override bool AddPowerSourceAndTargets(PowerTaskList taskList)
  {
    if (!this.HasSourceCard(taskList))
      return false;
    Entity sourceEntity = taskList.GetSourceEntity();
    Card card = sourceEntity.GetCard();
    CardEffect effect = this.InitEffect(card);
    if (effect == null)
      return false;
    if (sourceEntity.IsMinion())
    {
      if (!this.InitPowerSpell(effect, card))
      {
        if (!SpellUtils.CanAddPowerTargets(taskList))
        {
          this.Reset();
          return false;
        }
        if ((Object) this.GetActorBattlecrySpell(card) == (Object) null)
        {
          this.Reset();
          return false;
        }
      }
    }
    else
    {
      this.InitPowerSpell(effect, card);
      this.InitPowerSounds(effect, card);
      if ((Object) this.m_powerSpell == (Object) null && this.m_powerSoundSpells.Count == 0)
      {
        this.Reset();
        return false;
      }
    }
    this.SetSource(card);
    return true;
  }

  protected override void OnProcessTaskList()
  {
    if (this.ActivateActorBattlecrySpell() || this.ActivateCardEffects())
      return;
    base.OnProcessTaskList();
  }

  protected override void OnFinished()
  {
    if (this.m_processingTaskList)
      this.m_pendingFinish = true;
    else
      this.StartCoroutine(this.WaitThenFinish());
  }

  private void Reset()
  {
    SpellUtils.PurgeSpell(this.m_powerSpell);
    SpellUtils.PurgeSpells<CardSoundSpell>(this.m_powerSoundSpells);
    this.m_powerSpell = (Spell) null;
    this.m_powerSoundSpells.Clear();
    this.m_cardEffectsBlockingFinish = 0;
    this.m_cardEffectsBlockingTaskListFinish = 0;
  }

  [DebuggerHidden]
  private IEnumerator WaitThenFinish()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PowerSpellController.\u003CWaitThenFinish\u003Ec__Iterator24F() { \u003C\u003Ef__this = this };
  }

  private Spell GetActorBattlecrySpell(Card card)
  {
    Spell actorSpell = card.GetActorSpell(SpellType.BATTLECRY, true);
    if ((Object) actorSpell == (Object) null)
      return (Spell) null;
    if (!actorSpell.HasUsableState(SpellStateType.ACTION))
      return (Spell) null;
    return actorSpell;
  }

  private bool ActivateActorBattlecrySpell()
  {
    Card source = this.GetSource();
    if (!this.CanActivateActorBattlecrySpell(source.GetEntity()))
      return false;
    Spell actorBattlecrySpell = this.GetActorBattlecrySpell(source);
    if ((Object) actorBattlecrySpell == (Object) null)
      return false;
    this.StartCoroutine(this.WaitThenActivateActorBattlecrySpell(actorBattlecrySpell));
    return true;
  }

  private bool CanActivateActorBattlecrySpell(Entity entity)
  {
    return this.m_taskList.IsOrigin() && (entity.HasBattlecry() || entity.HasCombo() && entity.GetController().IsComboActive());
  }

  [DebuggerHidden]
  private IEnumerator WaitThenActivateActorBattlecrySpell(Spell actorBattlecrySpell)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PowerSpellController.\u003CWaitThenActivateActorBattlecrySpell\u003Ec__Iterator250() { actorBattlecrySpell = actorBattlecrySpell, \u003C\u0024\u003EactorBattlecrySpell = actorBattlecrySpell, \u003C\u003Ef__this = this };
  }

  private CardEffect InitEffect(Card card)
  {
    if ((Object) card == (Object) null)
      return (CardEffect) null;
    Network.HistBlockStart blockStart = this.m_taskList.GetBlockStart();
    string effectCardId = blockStart.EffectCardId;
    int effectIndex = blockStart.EffectIndex;
    CardEffect cardEffect;
    if (string.IsNullOrEmpty(effectCardId))
    {
      cardEffect = effectIndex < 0 ? card.GetPlayEffect() : card.GetSubOptionEffect(effectIndex);
    }
    else
    {
      CardDef cardDef = DefLoader.Get().GetCardDef(effectCardId, (CardPortraitQuality) null);
      CardEffectDef proxyEffectDef;
      if (effectIndex >= 0)
      {
        if (cardDef.m_SubOptionEffectDefs == null)
          return (CardEffect) null;
        if (effectIndex >= cardDef.m_SubOptionEffectDefs.Count)
          return (CardEffect) null;
        proxyEffectDef = cardDef.m_SubOptionEffectDefs[effectIndex];
      }
      else
        proxyEffectDef = cardDef.m_PlayEffectDef;
      cardEffect = card.GetOrCreateProxyEffect(blockStart, proxyEffectDef);
    }
    return cardEffect;
  }

  private bool ActivateCardEffects()
  {
    bool flag1 = this.ActivatePowerSpell();
    bool flag2 = this.ActivatePowerSounds();
    if (!flag1)
      return flag2;
    return true;
  }

  private void OnCardSpellFinished(Spell spell, object userData)
  {
    this.CardSpellFinished();
  }

  private void OnCardSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    this.CardSpellNoneStateEntered();
  }

  private void CardSpellFinished()
  {
    --this.m_cardEffectsBlockingTaskListFinish;
    if (this.m_cardEffectsBlockingTaskListFinish > 0)
      return;
    this.OnFinishedTaskList();
  }

  private void CardSpellNoneStateEntered()
  {
    --this.m_cardEffectsBlockingFinish;
    if (this.m_cardEffectsBlockingFinish > 0)
      return;
    this.OnFinished();
  }

  private bool InitPowerSpell(CardEffect effect, Card card)
  {
    Spell spell = effect.GetSpell(true);
    if ((Object) spell == (Object) null)
      return false;
    if (!spell.HasUsableState(SpellStateType.ACTION))
    {
      Log.Power.PrintWarning("{0}.InitPowerSpell() - spell {1} for Card {2} has no {3} state", (object) this.name, (object) spell, (object) card, (object) SpellStateType.ACTION);
      return false;
    }
    if (!spell.AttachPowerTaskList(this.m_taskList))
    {
      Log.Power.Print("{0}.InitPowerSpell() - FAILED to attach task list to spell {1} for Card {2}", new object[3]
      {
        (object) this.name,
        (object) spell,
        (object) card
      });
      return false;
    }
    if (spell.GetActiveState() != SpellStateType.NONE)
      spell.ActivateState(SpellStateType.NONE);
    this.m_powerSpell = spell;
    ++this.m_cardEffectsBlockingFinish;
    ++this.m_cardEffectsBlockingTaskListFinish;
    return true;
  }

  private bool ActivatePowerSpell()
  {
    if ((Object) this.m_powerSpell == (Object) null)
      return false;
    this.m_powerSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnCardSpellFinished));
    this.m_powerSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnCardSpellStateFinished));
    this.m_powerSpell.ActivateState(SpellStateType.ACTION);
    return true;
  }

  private bool InitPowerSounds(CardEffect effect, Card card)
  {
    List<CardSoundSpell> soundSpells = effect.GetSoundSpells(true);
    if (soundSpells == null || soundSpells.Count == 0)
      return false;
    using (List<CardSoundSpell>.Enumerator enumerator = soundSpells.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardSoundSpell current = enumerator.Current;
        if ((bool) ((Object) current))
        {
          if (!current.AttachPowerTaskList(this.m_taskList))
            Log.Power.Print("{0}.InitPowerSounds() - FAILED to attach task list to PowerSoundSpell {1} for Card {2}", new object[3]
            {
              (object) this.name,
              (object) current,
              (object) card
            });
          else
            this.m_powerSoundSpells.Add(current);
        }
      }
    }
    if (this.m_powerSoundSpells.Count == 0)
      return false;
    ++this.m_cardEffectsBlockingFinish;
    ++this.m_cardEffectsBlockingTaskListFinish;
    return true;
  }

  private bool ActivatePowerSounds()
  {
    if (this.m_powerSoundSpells.Count == 0)
      return false;
    Card source = this.GetSource();
    using (List<CardSoundSpell>.Enumerator enumerator = this.m_powerSoundSpells.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardSoundSpell current = enumerator.Current;
        if ((bool) ((Object) current))
          source.ActivateSoundSpell(current);
      }
    }
    this.CardSpellFinished();
    this.CardSpellNoneStateEntered();
    return true;
  }
}
