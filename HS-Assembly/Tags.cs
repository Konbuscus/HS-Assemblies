﻿// Decompiled with JetBrains decompiler
// Type: Tags
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;

public class Tags
{
  public static string DebugTag(int tag, int val)
  {
    string str1 = tag.ToString();
    try
    {
      str1 = ((GAME_TAG) tag).ToString();
    }
    catch (Exception ex)
    {
    }
    string str2 = val.ToString();
    GAME_TAG gameTag = (GAME_TAG) tag;
    switch (gameTag)
    {
      case GAME_TAG.NEXT_STEP:
      case GAME_TAG.STEP:
        try
        {
          str2 = ((TAG_STEP) val).ToString();
          break;
        }
        catch (Exception ex)
        {
          break;
        }
      case GAME_TAG.CLASS:
        try
        {
          str2 = ((TAG_CLASS) val).ToString();
          break;
        }
        catch (Exception ex)
        {
          break;
        }
      case GAME_TAG.CARDRACE:
        try
        {
          str2 = ((TAG_RACE) val).ToString();
          break;
        }
        catch (Exception ex)
        {
          break;
        }
      case GAME_TAG.FACTION:
        try
        {
          str2 = ((TAG_FACTION) val).ToString();
          break;
        }
        catch (Exception ex)
        {
          break;
        }
      case GAME_TAG.CARDTYPE:
        try
        {
          str2 = ((TAG_CARDTYPE) val).ToString();
          break;
        }
        catch (Exception ex)
        {
          break;
        }
      case GAME_TAG.RARITY:
        try
        {
          str2 = ((TAG_RARITY) val).ToString();
          break;
        }
        catch (Exception ex)
        {
          break;
        }
      case GAME_TAG.STATE:
        try
        {
          str2 = ((TAG_STATE) val).ToString();
          break;
        }
        catch (Exception ex)
        {
          break;
        }
      case GAME_TAG.PLAYSTATE:
        try
        {
          str2 = ((TAG_PLAYSTATE) val).ToString();
          break;
        }
        catch (Exception ex)
        {
          break;
        }
      default:
        if (gameTag != GAME_TAG.ENCHANTMENT_BIRTH_VISUAL)
        {
          if (gameTag != GAME_TAG.ENCHANTMENT_IDLE_VISUAL)
          {
            if (gameTag != GAME_TAG.ZONE)
            {
              if (gameTag != GAME_TAG.CARD_SET)
              {
                if (gameTag == GAME_TAG.MULLIGAN_STATE)
                {
                  try
                  {
                    str2 = ((TAG_MULLIGAN) val).ToString();
                    break;
                  }
                  catch (Exception ex)
                  {
                    break;
                  }
                }
                else
                  break;
              }
              else
              {
                try
                {
                  str2 = ((TAG_CARD_SET) val).ToString();
                  break;
                }
                catch (Exception ex)
                {
                  break;
                }
              }
            }
            else
            {
              try
              {
                str2 = ((TAG_ZONE) val).ToString();
                break;
              }
              catch (Exception ex)
              {
                break;
              }
            }
          }
        }
        try
        {
          str2 = ((TAG_ENCHANTMENT_VISUAL) val).ToString();
          break;
        }
        catch (Exception ex)
        {
          break;
        }
    }
    return string.Format("tag={0} value={1}", (object) str1, (object) str2);
  }

  public static void DebugDump(EntityBase entity, params GAME_TAG[] specificTagsToDump)
  {
    Log.Henry.Print(LogLevel.Debug, string.Format("Tags.DebugDump: entity={0}", (object) entity), new object[0]);
    Map<int, int> map = entity.GetTags().GetMap();
    foreach (int index in specificTagsToDump == null || specificTagsToDump.Length <= 0 ? map.Keys.ToArray<int>() : ((IEnumerable<GAME_TAG>) specificTagsToDump).Select<GAME_TAG, int>((Func<GAME_TAG, int>) (t => (int) t)).ToArray<int>())
    {
      string empty = string.Empty;
      string str;
      if (map.ContainsKey(index))
      {
        int val = map[index];
        str = Tags.DebugTag(index, val);
      }
      else
        str = string.Format("tag={0} value=(NULL)", (object) ((GAME_TAG) index).ToString());
      Log.Henry.Print(LogLevel.Debug, string.Format("Tags.DebugDump:           {0}", (object) str), new object[0]);
    }
  }
}
