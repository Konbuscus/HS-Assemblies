﻿// Decompiled with JetBrains decompiler
// Type: InputManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusGame;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class InputManager : MonoBehaviour
{
  public float m_MouseOverDelay = 0.4f;
  public DragRotatorInfo m_DragRotatorInfo = new DragRotatorInfo() { m_PitchInfo = new DragRotatorAxisInfo() { m_ForceMultiplier = 25f, m_MinDegrees = -40f, m_MaxDegrees = 40f, m_RestSeconds = 2f }, m_RollInfo = new DragRotatorAxisInfo() { m_ForceMultiplier = 25f, m_MinDegrees = -45f, m_MaxDegrees = 45f, m_RestSeconds = 2f } };
  private readonly PlatformDependentValue<float> MIN_GRAB_Y = new PlatformDependentValue<float>(PlatformCategory.Screen) { Tablet = 80f, Phone = 80f };
  private List<Card> m_cancelingBattlecryCards = new List<Card>();
  private List<Entity> m_entitiesThatPredictedMana = new List<Entity>();
  private List<Actor> m_mobileTargettingEffectActors = new List<Actor>();
  private List<InputManager.PhoneHandShownListener> m_phoneHandShownListener = new List<InputManager.PhoneHandShownListener>();
  private List<InputManager.PhoneHandHiddenListener> m_phoneHandHiddenListener = new List<InputManager.PhoneHandHiddenListener>();
  private const float MOBILE_TARGETTING_Y_OFFSET = 0.8f;
  private const float MOBILE_TARGETTING_XY_SCALE = 1.08f;
  private static InputManager s_instance;
  private ZoneHand m_myHandZone;
  private ZonePlay m_myPlayZone;
  private ZoneWeapon m_myWeaponZone;
  private Card m_heldCard;
  private bool m_checkForInput;
  private GameObject m_lastObjectMousedDown;
  private GameObject m_lastObjectRightMousedDown;
  private Vector3 m_lastMouseDownPosition;
  private bool m_leftMouseButtonIsDown;
  private bool m_dragging;
  private Card m_mousedOverCard;
  private HistoryCard m_mousedOverHistoryCard;
  private GameObject m_mousedOverObject;
  private float m_mousedOverTimer;
  private ZoneChangeList m_lastZoneChangeList;
  private Card m_battlecrySourceCard;
  private bool m_cardWasInsideHandLastFrame;
  private bool m_isInBattleCryEffect;
  private uint m_spectatorNotifyCurrentToken;
  private Card m_lastPreviewedCard;
  private bool m_touchDraggingCard;
  private bool m_useHandEnlarge;
  private bool m_hideHandAfterPlayingCard;
  private bool m_targettingHeroPower;
  private bool m_touchedDownOnSmallHand;

  public bool LeftMouseButtonDown
  {
    get
    {
      return this.m_leftMouseButtonIsDown;
    }
  }

  public Vector3 LastMouseDownPosition
  {
    get
    {
      return this.m_lastMouseDownPosition;
    }
  }

  private void Awake()
  {
    InputManager.s_instance = this;
    this.m_useHandEnlarge = (bool) UniversalInputManager.UsePhoneUI;
    if (GameState.Get() == null)
      return;
    GameState.Get().RegisterOptionsReceivedListener(new GameState.OptionsReceivedCallback(this.OnOptionsReceived));
    GameState.Get().RegisterOptionRejectedListener(new GameState.OptionRejectedCallback(this.OnOptionRejected), (object) null);
    GameState.Get().RegisterTurnTimerUpdateListener(new GameState.TurnTimerUpdateCallback(this.OnTurnTimerUpdate));
    GameState.Get().RegisterSpectatorNotifyListener(new GameState.SpectatorNotifyEventCallback(this.OnSpectatorNotifyEvent), (object) null);
    GameState.Get().RegisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
  }

  private void OnDestroy()
  {
    if (GameState.Get() != null)
    {
      GameState.Get().UnregisterOptionsReceivedListener(new GameState.OptionsReceivedCallback(this.OnOptionsReceived));
      GameState.Get().UnregisterOptionRejectedListener(new GameState.OptionRejectedCallback(this.OnOptionRejected), (object) null);
      GameState.Get().UnregisterTurnTimerUpdateListener(new GameState.TurnTimerUpdateCallback(this.OnTurnTimerUpdate));
      GameState.Get().UnregisterSpectatorNotifyListener(new GameState.SpectatorNotifyEventCallback(this.OnSpectatorNotifyEvent), (object) null);
      GameState.Get().UnregisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
    }
    InputManager.s_instance = (InputManager) null;
  }

  private void Update()
  {
    if (!this.m_checkForInput)
      return;
    if (UniversalInputManager.Get().GetMouseButtonDown(0))
      this.HandleLeftMouseDown();
    if (UniversalInputManager.Get().GetMouseButtonUp(0))
    {
      this.m_touchDraggingCard = false;
      this.HandleLeftMouseUp();
    }
    if (UniversalInputManager.Get().GetMouseButtonDown(1))
      this.HandleRightMouseDown();
    if (UniversalInputManager.Get().GetMouseButtonUp(1))
      this.HandleRightMouseUp();
    this.HandleMouseMove();
    if (this.m_leftMouseButtonIsDown && (Object) this.m_heldCard == (Object) null)
    {
      this.HandleUpdateWhileLeftMouseButtonIsDown();
      if (UniversalInputManager.Get().IsTouchMode() && !this.m_touchDraggingCard)
        this.HandleUpdateWhileNotHoldingCard();
    }
    else if ((Object) this.m_heldCard == (Object) null)
      this.HandleUpdateWhileNotHoldingCard();
    if (GameState.Get() == null || GameState.Get().GetFriendlySidePlayer() == null || GameState.Get().GetFriendlySidePlayer().IsLocalUser())
    {
      bool hitBattlefield = UniversalInputManager.Get().InputHitAnyObject(Camera.main, GameLayer.InvisibleHitBox2);
      if ((bool) ((Object) TargetReticleManager.Get()) && TargetReticleManager.Get().IsActive())
      {
        if (!hitBattlefield && (Object) this.GetBattlecrySourceCard() == (Object) null && (Object) ChoiceCardMgr.Get().GetSubOptionParentCard() == (Object) null && (!(bool) UniversalInputManager.UsePhoneUI || !this.m_targettingHeroPower && !GameState.Get().IsSelectedOptionFriendlyHero()))
        {
          this.CancelOption();
          if (this.m_useHandEnlarge)
            this.m_myHandZone.SetFriendlyHeroTargetingMode(false);
          if ((Object) this.m_heldCard != (Object) null)
            this.PositionHeldCard();
        }
        else
          TargetReticleManager.Get().UpdateArrowPosition();
      }
      else if ((bool) ((Object) this.m_heldCard))
        this.HandleUpdateWhileHoldingCard(hitBattlefield);
    }
    if ((Object) EmoteHandler.Get() != (Object) null && EmoteHandler.Get().AreEmotesActive())
      EmoteHandler.Get().HandleInput();
    if ((Object) EnemyEmoteHandler.Get() != (Object) null && EnemyEmoteHandler.Get().AreEmotesActive())
      EnemyEmoteHandler.Get().HandleInput();
    this.ShowTooltipIfNecessary();
  }

  public static InputManager Get()
  {
    return InputManager.s_instance;
  }

  public bool HandleKeyboardInput()
  {
    if (this.HandleUniversalHotkeys())
      return true;
    if (GameState.Get() != null && GameState.Get().IsMulliganManagerActive())
      return this.HandleMulliganHotkeys();
    return this.HandleGameHotkeys();
  }

  public Card GetMousedOverCard()
  {
    return this.m_mousedOverCard;
  }

  public void SetMousedOverCard(Card card)
  {
    if ((Object) this.m_mousedOverCard == (Object) card)
      return;
    if ((Object) this.m_mousedOverCard != (Object) null && !(this.m_mousedOverCard.GetZone() is ZoneHand))
      this.HandleMouseOffCard();
    if (!card.IsInputEnabled())
      return;
    this.m_mousedOverCard = card;
    card.NotifyMousedOver();
  }

  public Card GetBattlecrySourceCard()
  {
    return this.m_battlecrySourceCard;
  }

  public void StartWatchingForInput()
  {
    if (this.m_checkForInput)
      return;
    this.m_checkForInput = true;
    using (List<Zone>.Enumerator enumerator = ZoneMgr.Get().GetZones().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current.m_Side == Player.Side.FRIENDLY)
        {
          if (current is ZoneHand)
            this.m_myHandZone = (ZoneHand) current;
          else if (current is ZonePlay)
            this.m_myPlayZone = (ZonePlay) current;
          else if (current is ZoneWeapon)
            this.m_myWeaponZone = (ZoneWeapon) current;
        }
      }
    }
  }

  public void DisableInput()
  {
    this.m_checkForInput = false;
    this.HandleMouseOff();
    if (!(bool) ((Object) TargetReticleManager.Get()))
      return;
    TargetReticleManager.Get().DestroyFriendlyTargetArrow(false);
  }

  public Card GetHeldCard()
  {
    return this.m_heldCard;
  }

  public void EnableInput()
  {
    this.m_checkForInput = true;
  }

  public void OnMulliganEnded()
  {
    if (!(bool) ((Object) this.m_mousedOverCard))
      return;
    this.SetShouldShowTooltip();
  }

  private void SetShouldShowTooltip()
  {
    this.m_mousedOverTimer = 0.0f;
    this.m_mousedOverCard.SetShouldShowTooltip();
  }

  public ZoneHand GetFriendlyHand()
  {
    return this.m_myHandZone;
  }

  public bool UseHandEnlarge()
  {
    return this.m_useHandEnlarge;
  }

  public void SetHandEnlarge(bool set)
  {
    this.m_useHandEnlarge = set;
  }

  public bool DoesHideHandAfterPlayingCard()
  {
    return this.m_hideHandAfterPlayingCard;
  }

  public void SetHideHandAfterPlayingCard(bool set)
  {
    this.m_hideHandAfterPlayingCard = set;
  }

  public bool DropHeldCard()
  {
    return this.DropHeldCard(false);
  }

  private void HandleLeftMouseDown()
  {
    this.m_touchedDownOnSmallHand = false;
    GameObject gameObject = (GameObject) null;
    RaycastHit hitInfo;
    if (UniversalInputManager.Get().GetInputHitInfo(GameLayer.CardRaycast, out hitInfo))
    {
      gameObject = hitInfo.collider.gameObject;
      if ((Object) gameObject.GetComponent<EndTurnButtonReminder>() != (Object) null)
        return;
      CardStandIn componentInParents1 = SceneUtils.FindComponentInParents<CardStandIn>((Component) hitInfo.transform);
      if ((Object) componentInParents1 != (Object) null && GameState.Get() != null && !GameState.Get().IsMulliganManagerActive())
      {
        if (this.IsCancelingBattlecryCard(componentInParents1.linkedCard))
          return;
        if (this.m_useHandEnlarge && !this.m_myHandZone.HandEnlarged())
        {
          this.m_leftMouseButtonIsDown = true;
          this.m_touchedDownOnSmallHand = true;
          return;
        }
        this.m_lastObjectMousedDown = componentInParents1.gameObject;
        this.m_lastMouseDownPosition = UniversalInputManager.Get().GetMousePosition();
        this.m_leftMouseButtonIsDown = true;
        if (UniversalInputManager.Get().IsTouchMode())
        {
          this.m_touchDraggingCard = this.m_myHandZone.TouchReceived();
          this.m_lastPreviewedCard = componentInParents1.linkedCard;
        }
        this.m_myHandZone.HandleInput();
        return;
      }
      if ((Object) gameObject.GetComponent<EndTurnButton>() != (Object) null && !GameMgr.Get().IsSpectator())
      {
        EndTurnButton.Get().PlayPushDownAnimation();
        this.m_lastObjectMousedDown = hitInfo.collider.gameObject;
        return;
      }
      if ((Object) gameObject.GetComponent<GameOpenPack>() != (Object) null)
      {
        this.m_lastObjectMousedDown = hitInfo.collider.gameObject;
        return;
      }
      Actor componentInParents2 = SceneUtils.FindComponentInParents<Actor>((Component) hitInfo.transform);
      if ((Object) componentInParents2 == (Object) null)
        return;
      Card card = componentInParents2.GetCard();
      if (UniversalInputManager.Get().IsTouchMode() && (Object) this.m_battlecrySourceCard != (Object) null && (Object) card == (Object) this.m_battlecrySourceCard)
      {
        this.m_dragging = true;
        TargetReticleManager.Get().ShowArrow(true);
        return;
      }
      if (this.IsCancelingBattlecryCard(card))
        return;
      if ((Object) card != (Object) null)
        this.m_lastObjectMousedDown = card.gameObject;
      else if ((Object) componentInParents2.GetHistoryCard() != (Object) null)
        this.m_lastObjectMousedDown = componentInParents2.transform.parent.gameObject;
      else
        UnityEngine.Debug.LogWarning((object) "You clicked on something that is not being handled by InputManager.  Alert The Brode!");
      this.m_lastMouseDownPosition = UniversalInputManager.Get().GetMousePosition();
      this.m_leftMouseButtonIsDown = true;
    }
    if (this.m_useHandEnlarge && this.m_myHandZone.HandEnlarged() && ((Object) ChoiceCardMgr.Get().GetSubOptionParentCard() == (Object) null && (Object) gameObject == (Object) null))
      this.HidePhoneHand();
    this.HandleMemberClick();
  }

  private void ShowPhoneHand()
  {
    if (GameState.Get().IsMulliganPhaseNowOrPending() || !this.m_useHandEnlarge || this.m_myHandZone.HandEnlarged())
      return;
    this.m_myHandZone.AddUpdateLayoutCompleteCallback(new Zone.UpdateLayoutCompleteCallback(this.OnHandEnlargeComplete));
    this.m_myHandZone.SetHandEnlarged(true);
    foreach (InputManager.PhoneHandShownListener handShownListener in this.m_phoneHandShownListener.ToArray())
      handShownListener.Fire();
  }

  private void HidePhoneHand()
  {
    if (!this.m_useHandEnlarge || !((Object) this.m_myHandZone != (Object) null) || !this.m_myHandZone.HandEnlarged())
      return;
    this.m_myHandZone.SetHandEnlarged(false);
    foreach (InputManager.PhoneHandHiddenListener handHiddenListener in this.m_phoneHandHiddenListener.ToArray())
      handHiddenListener.Fire();
  }

  private void OnHandEnlargeComplete(Zone zone, object userData)
  {
    zone.RemoveUpdateLayoutCompleteCallback(new Zone.UpdateLayoutCompleteCallback(this.OnHandEnlargeComplete));
    if (!this.m_leftMouseButtonIsDown || !UniversalInputManager.Get().InputHitAnyObject(GameLayer.CardRaycast))
      return;
    this.HandleLeftMouseDown();
  }

  private void HidePhoneHandIfOutOfServerPlays()
  {
    if (GameState.Get().HasHandPlays())
      return;
    this.HidePhoneHand();
  }

  private bool HasLocalHandPlays()
  {
    List<Card> cards = this.m_myHandZone.GetCards();
    if (cards.Count == 0)
      return false;
    int spendableManaCrystals = ManaCrystalMgr.Get().GetSpendableManaCrystals();
    using (List<Card>.Enumerator enumerator = cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.GetEntity().GetRealTimeCost() <= spendableManaCrystals)
          return true;
      }
    }
    return false;
  }

  private void HandleLeftMouseUp()
  {
    PegCursor.Get().SetMode(PegCursor.Mode.UP);
    bool dragging = this.m_dragging;
    this.m_dragging = false;
    this.m_leftMouseButtonIsDown = false;
    this.m_targettingHeroPower = false;
    GameObject objectMousedDown = this.m_lastObjectMousedDown;
    this.m_lastObjectMousedDown = (GameObject) null;
    if (UniversalInputManager.Get().WasTouchCanceled())
    {
      this.CancelOption();
      this.m_heldCard = (Card) null;
    }
    else if ((Object) this.m_heldCard != (Object) null && (GameState.Get().GetResponseMode() == GameState.ResponseMode.OPTION || GameState.Get().GetResponseMode() == GameState.ResponseMode.NONE))
    {
      this.DropHeldCard();
    }
    else
    {
      bool flag1 = UniversalInputManager.Get().IsTouchMode() && GameState.Get().IsInTargetMode();
      bool flag2 = (Object) ChoiceCardMgr.Get().GetSubOptionParentCard() != (Object) null;
      RaycastHit hitInfo;
      if (UniversalInputManager.Get().GetInputHitInfo(GameLayer.CardRaycast, out hitInfo))
      {
        GameObject gameObject = hitInfo.collider.gameObject;
        if ((Object) gameObject.GetComponent<EndTurnButtonReminder>() != (Object) null)
          return;
        if ((Object) gameObject.GetComponent<EndTurnButton>() != (Object) null && (Object) gameObject == (Object) objectMousedDown && !GameMgr.Get().IsSpectator())
        {
          EndTurnButton.Get().PlayButtonUpAnimation();
          this.DoEndTurnButton();
        }
        else if ((Object) gameObject.GetComponent<GameOpenPack>() != (Object) null && (Object) gameObject == (Object) objectMousedDown)
        {
          gameObject.GetComponent<GameOpenPack>().HandleClick();
        }
        else
        {
          Actor componentInParents1 = SceneUtils.FindComponentInParents<Actor>((Component) hitInfo.transform);
          if ((Object) componentInParents1 != (Object) null)
          {
            Card card = componentInParents1.GetCard();
            if ((Object) card != (Object) null)
            {
              if (((Object) card.gameObject == (Object) objectMousedDown || dragging) && !this.IsCancelingBattlecryCard(card))
                this.HandleClickOnCard(componentInParents1.GetCard().gameObject, (Object) card.gameObject == (Object) objectMousedDown);
            }
            else if ((Object) componentInParents1.GetHistoryCard() != (Object) null)
              HistoryManager.Get().HandleClickOnBigCard(componentInParents1.GetHistoryCard());
          }
          CardStandIn componentInParents2 = SceneUtils.FindComponentInParents<CardStandIn>((Component) hitInfo.transform);
          if ((Object) componentInParents2 != (Object) null)
          {
            if (this.m_useHandEnlarge && this.m_touchedDownOnSmallHand)
              this.ShowPhoneHand();
            if ((Object) objectMousedDown == (Object) componentInParents2.gameObject && GameState.Get() != null && (!GameState.Get().IsMulliganManagerActive() && !this.IsCancelingBattlecryCard(componentInParents2.linkedCard)))
              this.HandleClickOnCard(componentInParents2.linkedCard.gameObject, true);
          }
          if (UniversalInputManager.Get().IsTouchMode() && (Object) componentInParents1 != (Object) null && (Object) ChoiceCardMgr.Get().GetSubOptionParentCard() != (Object) null)
          {
            using (List<Card>.Enumerator enumerator = ChoiceCardMgr.Get().GetFriendlyCards().GetEnumerator())
            {
              while (enumerator.MoveNext())
              {
                if ((Object) enumerator.Current == (Object) componentInParents1.GetCard())
                {
                  flag2 = false;
                  break;
                }
              }
            }
          }
        }
      }
      if (flag1)
        this.CancelOption();
      if (!UniversalInputManager.Get().IsTouchMode() || !flag2 || !((Object) ChoiceCardMgr.Get().GetSubOptionParentCard() != (Object) null))
        return;
      this.CancelSubOptionMode();
    }
  }

  private void HandleRightMouseDown()
  {
    RaycastHit hitInfo;
    if (!UniversalInputManager.Get().GetInputHitInfo(GameLayer.CardRaycast, out hitInfo))
      return;
    GameObject gameObject = hitInfo.collider.gameObject;
    if ((Object) gameObject.GetComponent<EndTurnButtonReminder>() != (Object) null || (Object) gameObject.GetComponent<EndTurnButton>() != (Object) null)
      return;
    Actor componentInParents = SceneUtils.FindComponentInParents<Actor>((Component) hitInfo.transform);
    if ((Object) componentInParents == (Object) null)
      return;
    if ((Object) componentInParents.GetCard() != (Object) null)
      this.m_lastObjectRightMousedDown = componentInParents.GetCard().gameObject;
    else if ((Object) componentInParents.GetHistoryCard() != (Object) null)
      this.m_lastObjectRightMousedDown = componentInParents.transform.parent.gameObject;
    else
      UnityEngine.Debug.LogWarning((object) "You clicked on something that is not being handled by InputManager.  Alert The Brode!");
  }

  private void HandleRightMouseUp()
  {
    PegCursor.Get().SetMode(PegCursor.Mode.UP);
    GameObject objectRightMousedDown = this.m_lastObjectRightMousedDown;
    this.m_lastObjectRightMousedDown = (GameObject) null;
    this.m_lastObjectMousedDown = (GameObject) null;
    this.m_leftMouseButtonIsDown = false;
    this.m_dragging = false;
    RaycastHit hitInfo;
    if (UniversalInputManager.Get().GetInputHitInfo(GameLayer.CardRaycast, out hitInfo))
    {
      Actor componentInParents = SceneUtils.FindComponentInParents<Actor>((Component) hitInfo.transform);
      if ((Object) componentInParents == (Object) null || (Object) componentInParents.GetCard() == (Object) null)
        this.HandleRightClick();
      else if ((Object) componentInParents.GetCard().gameObject == (Object) objectRightMousedDown)
        this.HandleRightClickOnCard(componentInParents.GetCard());
      else
        this.HandleRightClick();
    }
    else
      this.HandleRightClick();
  }

  private void HandleRightClick()
  {
    if (this.CancelOption())
      return;
    if ((Object) EmoteHandler.Get() != (Object) null && EmoteHandler.Get().AreEmotesActive())
      EmoteHandler.Get().HideEmotes();
    if (!((Object) EnemyEmoteHandler.Get() != (Object) null) || !EnemyEmoteHandler.Get().AreEmotesActive())
      return;
    EnemyEmoteHandler.Get().HideEmotes();
  }

  private bool CancelOption()
  {
    bool flag = false;
    GameState gameState = GameState.Get();
    if (gameState.IsInMainOptionMode())
      gameState.CancelCurrentOptionMode();
    if (this.CancelTargetMode())
      flag = true;
    if (this.CancelSubOptionMode())
      flag = true;
    if (this.DropHeldCard(true))
      flag = true;
    if ((bool) ((Object) this.m_mousedOverCard))
      this.m_mousedOverCard.UpdateProposedManaUsage();
    return flag;
  }

  private bool CancelTargetMode()
  {
    if (!GameState.Get().IsInTargetMode())
      return false;
    SoundManager.Get().LoadAndPlay("CancelAttack");
    if ((bool) ((Object) this.m_mousedOverCard))
      this.DisableSkullIfNeeded(this.m_mousedOverCard);
    if ((bool) ((Object) TargetReticleManager.Get()))
      TargetReticleManager.Get().DestroyFriendlyTargetArrow(true);
    this.ResetBattlecrySourceCard();
    this.CancelSubOptions();
    GameState.Get().CancelCurrentOptionMode();
    return true;
  }

  private bool CancelSubOptionMode()
  {
    if (!GameState.Get().IsInSubOptionMode())
      return false;
    this.CancelSubOptions();
    GameState.Get().CancelCurrentOptionMode();
    return true;
  }

  private void PositionHeldCard()
  {
    Card heldCard = this.m_heldCard;
    Entity entity = heldCard.GetEntity();
    RaycastHit hitInfo1;
    if (UniversalInputManager.Get().GetInputHitInfo(Camera.main, GameLayer.InvisibleHitBox2, out hitInfo1))
    {
      if (!heldCard.IsOverPlayfield())
      {
        if (GameState.Get().HasResponse(entity))
        {
          heldCard.NotifyOverPlayfield();
        }
        else
        {
          this.m_leftMouseButtonIsDown = false;
          this.m_lastObjectMousedDown = (GameObject) null;
          this.m_dragging = false;
          this.DropHeldCard();
          return;
        }
      }
      if (entity.IsMinion())
      {
        int slot = this.PlayZoneSlotMousedOver(heldCard);
        if (slot != this.m_myPlayZone.GetSlotMousedOver())
          this.m_myPlayZone.SortWithSpotForHeldCard(slot);
      }
    }
    else if (heldCard.IsOverPlayfield())
    {
      heldCard.NotifyLeftPlayfield();
      this.m_myPlayZone.SortWithSpotForHeldCard(-1);
    }
    RaycastHit hitInfo2;
    if (!UniversalInputManager.Get().GetInputHitInfo(Camera.main, GameLayer.DragPlane, out hitInfo2))
      return;
    heldCard.transform.position = hitInfo2.point;
  }

  private int PlayZoneSlotMousedOver(Card card)
  {
    int num1 = 0;
    RaycastHit hitInfo;
    if (UniversalInputManager.Get().GetInputHitInfo(Camera.main, GameLayer.InvisibleHitBox2, out hitInfo))
    {
      float slotWidth = this.m_myPlayZone.GetSlotWidth();
      float num2 = this.m_myPlayZone.transform.position.x - (float) ((double) (this.m_myPlayZone.GetCards().Count + 1) * (double) slotWidth / 2.0);
      num1 = (int) Mathf.Ceil((float) (((double) hitInfo.point.x - (double) num2) / (double) slotWidth - (double) slotWidth / 2.0));
      int count = this.m_myPlayZone.GetCards().Count;
      if (num1 < 0 || num1 > count)
        num1 = (double) card.transform.position.x >= (double) this.m_myPlayZone.transform.position.x ? count : 0;
    }
    return num1;
  }

  private void HandleUpdateWhileLeftMouseButtonIsDown()
  {
    if (UniversalInputManager.Get().IsTouchMode() && (Object) this.m_heldCard == (Object) null)
    {
      if ((Object) this.GetBattlecrySourceCard() == (Object) null)
        this.m_myHandZone.HandleInput();
      Card card = !((Object) this.m_myHandZone.CurrentStandIn != (Object) null) ? (Card) null : this.m_myHandZone.CurrentStandIn.linkedCard;
      if ((Object) card != (Object) this.m_lastPreviewedCard)
      {
        if ((Object) card != (Object) null)
          this.m_lastMouseDownPosition.y = UniversalInputManager.Get().GetMousePosition().y;
        this.m_lastPreviewedCard = card;
      }
    }
    if (this.m_dragging || (Object) this.m_lastObjectMousedDown == (Object) null)
      return;
    if ((bool) ((Object) this.m_lastObjectMousedDown.GetComponent<HistoryCard>()))
    {
      this.m_lastObjectMousedDown = (GameObject) null;
      this.m_leftMouseButtonIsDown = false;
    }
    else
    {
      float num1 = UniversalInputManager.Get().GetMousePosition().y - this.m_lastMouseDownPosition.y;
      float num2 = UniversalInputManager.Get().GetMousePosition().x - this.m_lastMouseDownPosition.x;
      if ((double) num2 > -20.0 && (double) num2 < 20.0 && ((double) num1 > -20.0 && (double) num1 < 20.0))
        return;
      bool flag = !UniversalInputManager.Get().IsTouchMode() || (double) num1 > (double) (float) this.MIN_GRAB_Y;
      CardStandIn cardStandIn = this.m_lastObjectMousedDown.GetComponent<CardStandIn>();
      if ((Object) cardStandIn != (Object) null && GameState.Get() != null && !GameState.Get().IsMulliganManagerActive())
      {
        if (UniversalInputManager.Get().IsTouchMode())
        {
          if (!flag)
            return;
          cardStandIn = this.m_myHandZone.CurrentStandIn;
          if ((Object) cardStandIn == (Object) null)
            return;
        }
        if (ChoiceCardMgr.Get().IsFriendlyShown() || !((Object) this.GetBattlecrySourceCard() == (Object) null) || !this.IsInZone(cardStandIn.linkedCard, TAG_ZONE.HAND))
          return;
        this.m_dragging = true;
        this.GrabCard(cardStandIn.linkedCard.gameObject);
      }
      else
      {
        if (GameState.Get().IsMulliganManagerActive() || GameState.Get().IsInTargetMode())
          return;
        Card component = this.m_lastObjectMousedDown.GetComponent<Card>();
        Entity entity = component.GetEntity();
        if (!entity.IsControlledByLocalUser())
          return;
        if (this.IsInZone(component, TAG_ZONE.HAND))
        {
          if (!flag || UniversalInputManager.Get().IsTouchMode() && !GameState.Get().HasResponse(entity) || (!ChoiceCardMgr.Get().IsFriendlyShown() || !((Object) this.GetBattlecrySourceCard() == (Object) null)))
            return;
          this.m_dragging = true;
          this.GrabCard(this.m_lastObjectMousedDown);
        }
        else
        {
          if (!this.IsInZone(component, TAG_ZONE.PLAY) || entity.IsHeroPower() && (!entity.IsHeroPower() || !GameState.Get().EntityHasTargets(entity)))
            return;
          this.m_dragging = true;
          this.HandleClickOnCardInBattlefield(entity);
        }
      }
    }
  }

  private void HandleUpdateWhileHoldingCard(bool hitBattlefield)
  {
    PegCursor.Get().SetMode(PegCursor.Mode.DRAG);
    Card heldCard = this.m_heldCard;
    if (!heldCard.IsInputEnabled())
    {
      this.DropHeldCard();
    }
    else
    {
      Entity entity = heldCard.GetEntity();
      if (hitBattlefield && (bool) ((Object) TargetReticleManager.Get()) && (!TargetReticleManager.Get().IsActive() && GameState.Get().EntityHasTargets(entity)) && entity.GetCardType() != TAG_CARDTYPE.MINION)
      {
        if (!this.DoNetworkResponse(entity, true))
        {
          this.PositionHeldCard();
          return;
        }
        DragCardSoundEffects component = heldCard.GetComponent<DragCardSoundEffects>();
        if ((bool) ((Object) component))
          component.Disable();
        RemoteActionHandler.Get().NotifyOpponentOfCardPickedUp(heldCard);
        RemoteActionHandler.Get().NotifyOpponentOfTargetModeBegin(heldCard);
        TargetReticleManager.Get().CreateFriendlyTargetArrow(entity.GetHero(), entity, true, true, (string) null, false);
        this.ActivatePowerUpSpell(heldCard);
        this.ActivatePlaySpell(heldCard);
      }
      else
      {
        if (hitBattlefield && this.m_cardWasInsideHandLastFrame)
        {
          RemoteActionHandler.Get().NotifyOpponentOfCardPickedUp(heldCard);
          this.m_cardWasInsideHandLastFrame = false;
        }
        else if (!hitBattlefield)
          this.m_cardWasInsideHandLastFrame = true;
        this.PositionHeldCard();
        if (GameState.Get().GetResponseMode() == GameState.ResponseMode.SUB_OPTION)
          this.CancelSubOptionMode();
      }
      if (!UniversalInputManager.Get().IsTouchMode() || hitBattlefield || !((Object) this.m_heldCard != (Object) null) || (double) UniversalInputManager.Get().GetMousePosition().y - (double) this.m_lastMouseDownPosition.y >= (double) (float) this.MIN_GRAB_Y)
        return;
      PegCursor.Get().SetMode(PegCursor.Mode.STOPDRAG);
      this.ReturnHeldCardToHand();
    }
  }

  private void ActivatePowerUpSpell(Card card)
  {
    if (card.GetEntity().IsSpell())
    {
      Spell actorSpell = card.GetActorSpell(SpellType.POWER_UP, true);
      if ((Object) actorSpell != (Object) null)
        actorSpell.ActivateState(SpellStateType.BIRTH);
    }
    card.DeactivateHandStateSpells();
  }

  private void ActivatePlaySpell(Card card)
  {
    Entity entity = card.GetEntity();
    Entity parentEntity = entity.GetParentEntity();
    Spell spell = parentEntity != null ? parentEntity.GetCard().GetSubOptionSpell(parentEntity.GetSubCardIndex(entity), true) : card.GetPlaySpell(true);
    if (!((Object) spell != (Object) null))
      return;
    spell.ActivateState(SpellStateType.BIRTH);
  }

  private void HandleMouseMove()
  {
    if (!GameState.Get().IsInTargetMode())
      return;
    this.HandleUpdateWhileNotHoldingCard();
  }

  private void HandleUpdateWhileNotHoldingCard()
  {
    if (!UniversalInputManager.Get().IsTouchMode() || !TargetReticleManager.Get().IsLocalArrowActive())
      this.m_myHandZone.HandleInput();
    RaycastHit hitInfo;
    if ((!UniversalInputManager.Get().IsTouchMode() || UniversalInputManager.Get().GetMouseButton(0)) && UniversalInputManager.Get().GetInputHitInfo(GameLayer.CardRaycast, out hitInfo))
    {
      CardStandIn componentInParents1 = SceneUtils.FindComponentInParents<CardStandIn>((Component) hitInfo.transform);
      Actor componentInParents2 = SceneUtils.FindComponentInParents<Actor>((Component) hitInfo.transform);
      if ((Object) componentInParents2 == (Object) null && (Object) componentInParents1 == (Object) null)
      {
        this.HandleMouseOverObjectWhileNotHoldingCard(hitInfo);
      }
      else
      {
        if ((Object) this.m_mousedOverObject != (Object) null)
          this.HandleMouseOffLastObject();
        Card card = (Card) null;
        if ((Object) componentInParents2 != (Object) null)
          card = componentInParents2.GetCard();
        if ((Object) card == (Object) null)
        {
          if ((Object) componentInParents1 == (Object) null || GameState.Get() == null || GameState.Get().IsMulliganManagerActive())
            return;
          card = componentInParents1.linkedCard;
        }
        if (this.IsCancelingBattlecryCard(card))
          return;
        if ((Object) card != (Object) this.m_mousedOverCard && ((Object) card.GetZone() != (Object) this.m_myHandZone || GameState.Get().IsMulliganManagerActive()))
        {
          if ((Object) this.m_mousedOverCard != (Object) null)
            this.HandleMouseOffCard();
          this.HandleMouseOverCard(card);
        }
        PegCursor.Get().SetMode(PegCursor.Mode.OVER);
      }
    }
    else
    {
      if ((bool) ((Object) this.m_mousedOverCard))
        this.HandleMouseOffCard();
      this.HandleMouseOff();
    }
  }

  private void HandleMouseOverObjectWhileNotHoldingCard(RaycastHit hitInfo)
  {
    GameObject gameObject = hitInfo.collider.gameObject;
    if ((Object) this.m_mousedOverCard != (Object) null)
      this.HandleMouseOffCard();
    if (UniversalInputManager.Get().IsTouchMode() && !UniversalInputManager.Get().GetMouseButton(0))
    {
      if (!((Object) this.m_mousedOverObject != (Object) null))
        return;
      this.HandleMouseOffLastObject();
    }
    else
    {
      bool flag = (Object) TargetReticleManager.Get() != (Object) null && TargetReticleManager.Get().IsLocalArrowActive();
      if (GameMgr.Get() != null && GameMgr.Get().IsSpectator())
        flag = false;
      if ((Object) gameObject.GetComponent<HistoryManager>() != (Object) null && !flag)
      {
        this.m_mousedOverObject = gameObject;
        HistoryManager.Get().NotifyOfInput(hitInfo.point.z);
      }
      else
      {
        if ((Object) this.m_mousedOverObject == (Object) gameObject)
          return;
        if ((Object) this.m_mousedOverObject != (Object) null)
          this.HandleMouseOffLastObject();
        if ((bool) ((Object) EndTurnButton.Get()) && !GameMgr.Get().IsSpectator())
        {
          if ((Object) gameObject.GetComponent<EndTurnButton>() != (Object) null)
          {
            this.m_mousedOverObject = gameObject;
            EndTurnButton.Get().HandleMouseOver();
          }
          else if ((Object) gameObject.GetComponent<EndTurnButtonReminder>() != (Object) null && gameObject.GetComponent<EndTurnButtonReminder>().ShowFriendlySidePlayerTurnReminder())
            this.m_mousedOverObject = gameObject;
        }
        TooltipZone component1 = gameObject.GetComponent<TooltipZone>();
        if ((Object) component1 != (Object) null)
        {
          this.m_mousedOverObject = gameObject;
          this.ShowTooltipZone(gameObject, component1);
        }
        GameOpenPack component2 = gameObject.GetComponent<GameOpenPack>();
        if ((Object) component2 != (Object) null)
        {
          this.m_mousedOverObject = gameObject;
          component2.NotifyOfMouseOver();
        }
        if ((Object) this.GetBattlecrySourceCard() != (Object) null || !UniversalInputManager.Get().InputHitAnyObject(Camera.main, GameLayer.InvisibleHitBox1) || !ChoiceCardMgr.Get().HasSubOption())
          return;
        this.CancelSubOptionMode();
      }
    }
  }

  private void HandleMouseOff()
  {
    if ((bool) ((Object) this.m_mousedOverCard))
      this.HandleMouseOffCard();
    if (!(bool) ((Object) this.m_mousedOverObject))
      return;
    this.HandleMouseOffLastObject();
  }

  private void HandleMouseOffLastObject()
  {
    if ((bool) ((Object) this.m_mousedOverObject.GetComponent<EndTurnButton>()))
    {
      this.m_mousedOverObject.GetComponent<EndTurnButton>().HandleMouseOut();
      this.m_lastObjectMousedDown = (GameObject) null;
    }
    else if ((bool) ((Object) this.m_mousedOverObject.GetComponent<EndTurnButtonReminder>()))
      this.m_lastObjectMousedDown = (GameObject) null;
    else if ((Object) this.m_mousedOverObject.GetComponent<TooltipZone>() != (Object) null)
    {
      this.m_mousedOverObject.GetComponent<TooltipZone>().HideTooltip();
      this.m_lastObjectMousedDown = (GameObject) null;
    }
    else if ((Object) this.m_mousedOverObject.GetComponent<HistoryManager>() != (Object) null)
      HistoryManager.Get().NotifyOfMouseOff();
    else if ((Object) this.m_mousedOverObject.GetComponent<GameOpenPack>() != (Object) null)
    {
      this.m_mousedOverObject.GetComponent<GameOpenPack>().NotifyOfMouseOff();
      this.m_lastObjectMousedDown = (GameObject) null;
    }
    this.m_mousedOverObject = (GameObject) null;
    this.HideBigViewCardBacks();
  }

  private void GrabCard(GameObject cardObject)
  {
    if (GameMgr.Get().IsSpectator())
      return;
    Card component = cardObject.GetComponent<Card>();
    if (!component.IsInputEnabled() || !GameState.Get().GetGameEntity().ShouldAllowCardGrab(component.GetEntity()))
      return;
    Zone zone = component.GetZone();
    if (!zone.IsInputEnabled())
      return;
    component.SetDoNotSort(true);
    if (zone is ZoneHand && !UniversalInputManager.Get().IsTouchMode())
      ((ZoneHand) zone).UpdateLayout(-1);
    this.m_heldCard = component;
    SoundManager.Get().LoadAndPlay("FX_MinionSummon01_DrawFromHand_01", cardObject);
    DragCardSoundEffects cardSoundEffects = this.m_heldCard.GetComponent<DragCardSoundEffects>();
    if ((bool) ((Object) cardSoundEffects))
      cardSoundEffects.enabled = true;
    else
      cardSoundEffects = cardObject.AddComponent<DragCardSoundEffects>();
    cardSoundEffects.Restart();
    cardObject.AddComponent<DragRotator>().SetInfo(this.m_DragRotatorInfo);
    ProjectedShadow componentInChildren = component.GetActor().GetComponentInChildren<ProjectedShadow>();
    if ((Object) componentInChildren != (Object) null)
      componentInChildren.EnableShadow(0.15f);
    iTween.Stop(cardObject);
    float num = 0.7f;
    iTween.ScaleTo(cardObject, new Vector3(num, num, num), 0.2f);
    TooltipPanelManager.Get().HideKeywordHelp();
    if ((bool) ((Object) CardTypeBanner.Get()))
      CardTypeBanner.Get().Hide();
    component.NotifyPickedUp();
    GameState.Get().GetGameEntity().NotifyOfCardGrabbed(component.GetEntity());
    SceneUtils.SetLayer((Component) component, GameLayer.Default);
  }

  private void DropCanceledHeldCard(Card card)
  {
    this.m_heldCard = (Card) null;
    RemoteActionHandler.Get().NotifyOpponentOfCardDropped();
    this.m_myHandZone.UpdateLayout(-1, true);
    this.m_myPlayZone.SortWithSpotForHeldCard(-1);
  }

  public void ReturnHeldCardToHand()
  {
    if ((Object) this.m_heldCard == (Object) null)
      return;
    Log.Hand.Print("ReturnHeldCardToHand()");
    Card heldCard = this.m_heldCard;
    heldCard.SetDoNotSort(false);
    iTween.Stop(this.m_heldCard.gameObject);
    Entity entity = heldCard.GetEntity();
    heldCard.NotifyLeftPlayfield();
    GameState.Get().GetGameEntity().NotifyOfCardDropped(entity);
    DragCardSoundEffects component = heldCard.GetComponent<DragCardSoundEffects>();
    if ((bool) ((Object) component))
      component.Disable();
    Object.Destroy((Object) this.m_heldCard.GetComponent<DragRotator>());
    ProjectedShadow componentInChildren = heldCard.GetActor().GetComponentInChildren<ProjectedShadow>();
    if ((Object) componentInChildren != (Object) null)
      componentInChildren.DisableShadow();
    RemoteActionHandler.Get().NotifyOpponentOfCardDropped();
    if (this.m_useHandEnlarge)
      this.m_myHandZone.SetFriendlyHeroTargetingMode(false);
    this.m_myHandZone.UpdateLayout(this.m_myHandZone.GetLastMousedOverCard(), true);
    this.m_dragging = false;
    this.m_heldCard = (Card) null;
  }

  private bool DropHeldCard(bool wasCancelled)
  {
    Log.Hand.Print("DropHeldCard - cancelled? " + (object) wasCancelled);
    PegCursor.Get().SetMode(PegCursor.Mode.STOPDRAG);
    if (this.m_useHandEnlarge)
    {
      this.m_myHandZone.SetFriendlyHeroTargetingMode(false);
      if (this.m_hideHandAfterPlayingCard)
        this.HidePhoneHand();
      else
        this.m_myHandZone.UpdateLayout(-1, true);
    }
    if ((Object) this.m_heldCard == (Object) null)
      return false;
    Card heldCard = this.m_heldCard;
    heldCard.SetDoNotSort(false);
    iTween.Stop(this.m_heldCard.gameObject);
    Entity entity = heldCard.GetEntity();
    heldCard.NotifyLeftPlayfield();
    GameState.Get().GetGameEntity().NotifyOfCardDropped(entity);
    DragCardSoundEffects component = heldCard.GetComponent<DragCardSoundEffects>();
    if ((bool) ((Object) component))
      component.Disable();
    Object.Destroy((Object) this.m_heldCard.GetComponent<DragRotator>());
    this.m_heldCard = (Card) null;
    ProjectedShadow componentInChildren = heldCard.GetActor().GetComponentInChildren<ProjectedShadow>();
    if ((Object) componentInChildren != (Object) null)
      componentInChildren.DisableShadow();
    if (wasCancelled)
    {
      this.DropCanceledHeldCard(heldCard);
      return true;
    }
    bool flag1 = false;
    if (this.IsInZone(heldCard, TAG_ZONE.HAND))
    {
      bool flag2 = entity.IsMinion();
      bool flag3 = entity.IsWeapon();
      if (flag2 || flag3)
      {
        RaycastHit hitInfo;
        if (UniversalInputManager.Get().GetInputHitInfo(Camera.main, GameLayer.InvisibleHitBox2, out hitInfo))
        {
          Zone zone = !flag3 ? (Zone) this.m_myPlayZone : (Zone) this.m_myWeaponZone;
          if ((bool) ((Object) zone))
          {
            GameState gameState = GameState.Get();
            int num1 = 0;
            int num2 = 0;
            if (flag2)
            {
              num1 = this.PlayZoneSlotMousedOver(heldCard) + 1;
              num2 = ZoneMgr.Get().PredictZonePosition(zone, num1);
              gameState.SetSelectedOptionPosition(num2);
            }
            if (this.DoNetworkResponse(entity, true))
            {
              this.m_lastZoneChangeList = ZoneMgr.Get().AddPredictedLocalZoneChange(heldCard, zone, num1, num2);
              this.PredictSpentMana(entity);
              if (flag2 && gameState.EntityHasTargets(entity))
              {
                flag1 = true;
                bool showArrow = !UniversalInputManager.Get().IsTouchMode();
                if ((bool) ((Object) TargetReticleManager.Get()))
                  TargetReticleManager.Get().CreateFriendlyTargetArrow(entity, entity, true, showArrow, (string) null, false);
                this.m_battlecrySourceCard = heldCard;
                if (UniversalInputManager.Get().IsTouchMode())
                  this.StartBattleCryEffect(entity);
              }
            }
            else
              gameState.SetSelectedOptionPosition(0);
          }
        }
      }
      else if (entity.IsSpell())
      {
        if (GameState.Get().EntityHasTargets(entity))
        {
          this.DropCanceledHeldCard(entity.GetCard());
          return true;
        }
        RaycastHit hitInfo;
        if (UniversalInputManager.Get().GetInputHitInfo(Camera.main, GameLayer.InvisibleHitBox2, out hitInfo))
        {
          if (!GameState.Get().HasResponse(entity))
          {
            PlayErrors.DisplayPlayError(PlayErrors.GetPlayEntityError(entity), entity);
          }
          else
          {
            this.DoNetworkResponse(entity, true);
            this.m_lastZoneChangeList = ZoneMgr.Get().AddLocalZoneChange(heldCard, TAG_ZONE.PLAY);
            this.PredictSpentMana(entity);
            if (GameState.Get().HasSubOptions(entity))
            {
              heldCard.DeactivateHandStateSpells();
            }
            else
            {
              this.ActivatePowerUpSpell(heldCard);
              this.ActivatePlaySpell(heldCard);
            }
          }
        }
      }
      this.m_myHandZone.UpdateLayout(-1, true);
      this.m_myPlayZone.SortWithSpotForHeldCard(-1);
    }
    if (flag1)
    {
      if ((bool) ((Object) RemoteActionHandler.Get()))
        RemoteActionHandler.Get().NotifyOpponentOfTargetModeBegin(heldCard);
    }
    else if (GameState.Get().GetResponseMode() != GameState.ResponseMode.SUB_OPTION)
      RemoteActionHandler.Get().NotifyOpponentOfCardDropped();
    return true;
  }

  private void HandleRightClickOnCard(Card card)
  {
    if (GameState.Get().IsInTargetMode() || GameState.Get().IsInSubOptionMode() || (Object) this.m_heldCard != (Object) null)
    {
      this.HandleRightClick();
    }
    else
    {
      if (!card.GetEntity().IsHero())
        return;
      if (card.GetEntity().IsControlledByLocalUser())
      {
        if (!((Object) EmoteHandler.Get() != (Object) null))
          return;
        if (EmoteHandler.Get().AreEmotesActive())
          EmoteHandler.Get().HideEmotes();
        else
          EmoteHandler.Get().ShowEmotes();
      }
      else
      {
        bool flag = (Object) EnemyEmoteHandler.Get() != (Object) null;
        if (GameMgr.Get().IsSpectator() && card.GetEntity().GetControllerSide() != Player.Side.OPPOSING)
          flag = false;
        if (!flag)
          return;
        if (EnemyEmoteHandler.Get().AreEmotesActive())
          EnemyEmoteHandler.Get().HideEmotes();
        else
          EnemyEmoteHandler.Get().ShowEmotes();
      }
    }
  }

  private void HandleClickOnCard(GameObject upClickedCard, bool wasMouseDownTarget)
  {
    if ((Object) EmoteHandler.Get() != (Object) null)
    {
      if (EmoteHandler.Get().IsMouseOverEmoteOption())
        return;
      EmoteHandler.Get().HideEmotes();
    }
    if ((Object) EnemyEmoteHandler.Get() != (Object) null)
    {
      if (EnemyEmoteHandler.Get().IsMouseOverEmoteOption())
        return;
      EnemyEmoteHandler.Get().HideEmotes();
    }
    Card component = upClickedCard.GetComponent<Card>();
    Entity entity = component.GetEntity();
    Log.Hand.Print("HandleClickOnCard - Card zone: " + (object) component.GetZone());
    if (UniversalInputManager.Get().IsTouchMode() && entity.IsHero() && (!GameState.Get().IsInTargetMode() && wasMouseDownTarget))
    {
      if (entity.IsControlledByLocalUser())
      {
        if (!((Object) EmoteHandler.Get() != (Object) null))
          return;
        EmoteHandler.Get().ShowEmotes();
        return;
      }
      if (!GameMgr.Get().IsSpectator() && (Object) EnemyEmoteHandler.Get() != (Object) null)
      {
        EnemyEmoteHandler.Get().ShowEmotes();
        return;
      }
    }
    if ((Object) component == (Object) ChoiceCardMgr.Get().GetSubOptionParentCard())
    {
      this.CancelOption();
    }
    else
    {
      GameState.ResponseMode responseMode = GameState.Get().GetResponseMode();
      if (this.IsInZone(component, TAG_ZONE.HAND))
      {
        if (GameState.Get().IsMulliganManagerActive())
        {
          if (GameMgr.Get().IsSpectator())
            return;
          MulliganManager.Get().ToggleHoldState(component);
        }
        else
        {
          if (component.IsAttacking() || UniversalInputManager.Get().IsTouchMode() || (ChoiceCardMgr.Get().IsFriendlyShown() || !((Object) this.GetBattlecrySourceCard() == (Object) null)))
            return;
          this.GrabCard(upClickedCard);
        }
      }
      else if (responseMode == GameState.ResponseMode.SUB_OPTION)
        this.HandleClickOnSubOption(entity, false);
      else if (responseMode == GameState.ResponseMode.CHOICE)
      {
        this.HandleClickOnChoice(entity);
      }
      else
      {
        if (!this.IsInZone(component, TAG_ZONE.PLAY))
          return;
        this.HandleClickOnCardInBattlefield(entity);
      }
    }
  }

  private void HandleClickOnCardInBattlefield(Entity clickedEntity)
  {
    if (GameMgr.Get().IsSpectator())
      return;
    PegCursor.Get().SetMode(PegCursor.Mode.STOPDRAG);
    GameState gameState = GameState.Get();
    Card card = clickedEntity.GetCard();
    if (UniversalInputManager.Get().IsTouchMode() && clickedEntity.IsHeroPower() && (double) this.m_mousedOverTimer > (double) this.m_MouseOverDelay || !gameState.GetGameEntity().NotifyOfBattlefieldCardClicked(clickedEntity, gameState.IsInTargetMode()))
      return;
    if (gameState.IsInTargetMode())
    {
      this.DisableSkullIfNeeded(card);
      if (gameState.GetSelectedNetworkSubOption().ID == clickedEntity.GetEntityId())
      {
        this.CancelOption();
      }
      else
      {
        if (!this.DoNetworkResponse(clickedEntity, true) || !((Object) this.m_heldCard != (Object) null))
          return;
        Card heldCard = this.m_heldCard;
        this.m_heldCard = (Card) null;
        heldCard.SetDoNotSort(false);
        this.m_lastZoneChangeList = ZoneMgr.Get().AddLocalZoneChange(heldCard, TAG_ZONE.PLAY);
      }
    }
    else if (UniversalInputManager.Get().IsTouchMode() && UniversalInputManager.Get().GetMouseButtonUp(0) && gameState.EntityHasTargets(clickedEntity))
    {
      if (card.IsShowingTooltip() || !gameState.IsFriendlySidePlayerTurn())
        return;
      PlayErrors.DisplayPlayError(PlayErrors.ErrorType.REQ_DRAG_TO_PLAY, clickedEntity);
    }
    else if (clickedEntity.IsWeapon() && clickedEntity.IsControlledByLocalUser())
    {
      this.HandleClickOnCardInBattlefield(gameState.GetFriendlySidePlayer().GetHero());
    }
    else
    {
      if (!this.DoNetworkResponse(clickedEntity, true))
        return;
      if (!gameState.IsInTargetMode())
      {
        if (!clickedEntity.IsHeroPower())
          return;
        this.ActivatePlaySpell(card);
        clickedEntity.SetTagAndHandleChange<int>(GAME_TAG.EXHAUSTED, 1);
        this.PredictSpentMana(clickedEntity);
      }
      else
      {
        RemoteActionHandler.Get().NotifyOpponentOfTargetModeBegin(card);
        if ((bool) ((Object) TargetReticleManager.Get()))
          TargetReticleManager.Get().CreateFriendlyTargetArrow(clickedEntity, clickedEntity, false, true, (string) null, false);
        if (clickedEntity.IsHeroPower())
        {
          this.m_targettingHeroPower = true;
          this.ActivatePlaySpell(card);
        }
        else
        {
          if (!clickedEntity.IsCharacter())
            return;
          card.ActivateCharacterAttackEffects();
          gameState.ShowEnemyTauntCharacters();
          if (card.IsAttacking())
            return;
          Spell attackSpellForInput = card.GetActorAttackSpellForInput();
          if (!((Object) attackSpellForInput != (Object) null))
            return;
          if (clickedEntity.HasTag(GAME_TAG.IMMUNE_WHILE_ATTACKING))
            card.GetActor().ActivateSpellBirthState(SpellType.IMMUNE);
          attackSpellForInput.ActivateState(SpellStateType.BIRTH);
        }
      }
    }
  }

  private void HandleClickOnSubOption(Entity entity, bool isSimulated = false)
  {
    if (isSimulated || GameState.Get().HasResponse(entity))
    {
      bool flag = false;
      Card optionParentCard = ChoiceCardMgr.Get().GetSubOptionParentCard();
      if (!isSimulated)
      {
        flag = GameState.Get().SubEntityHasTargets(entity);
        if (flag)
        {
          RemoteActionHandler.Get().NotifyOpponentOfTargetModeBegin(optionParentCard);
          TargetReticleManager.Get().CreateFriendlyTargetArrow(entity.GetHero(), optionParentCard.GetEntity(), true, !UniversalInputManager.Get().IsTouchMode(), entity.GetCardTextInHand(), false);
        }
      }
      Card card = entity.GetCard();
      if (!isSimulated)
        this.DoNetworkResponse(entity, true);
      this.ActivatePowerUpSpell(card);
      this.ActivatePlaySpell(card);
      if (entity.IsMinion())
        card.HideCard();
      ChoiceCardMgr.Get().OnSubOptionClicked(entity);
      if (!isSimulated && !flag)
        this.FinishSubOptions();
      if (!UniversalInputManager.Get().IsTouchMode() || isSimulated || !flag)
        return;
      this.StartMobileTargetingEffect(GameState.Get().GetSelectedNetworkSubOption().Targets);
    }
    else
      PlayErrors.DisplayPlayError(PlayErrors.GetPlayEntityError(entity), entity);
  }

  private void HandleClickOnChoice(Entity entity)
  {
    if (GameMgr.Get().IsSpectator())
      return;
    if (this.DoNetworkResponse(entity, true))
      SoundManager.Get().LoadAndPlay("HeroDropItem1");
    else
      PlayErrors.DisplayPlayError(PlayErrors.GetPlayEntityError(entity), entity);
  }

  public void ResetBattlecrySourceCard()
  {
    if ((Object) this.m_battlecrySourceCard == (Object) null)
      return;
    if (UniversalInputManager.Get().IsTouchMode())
      GameplayErrorManager.Get().DisplayMessage(!this.m_battlecrySourceCard.GetEntity().HasTag(GAME_TAG.BATTLECRY) ? GameStrings.Get("GAMEPLAY_MOBILE_TARGETING_CANCELED") : GameStrings.Get("GAMEPLAY_MOBILE_BATTLECRY_CANCELED"));
    this.m_cancelingBattlecryCards.Add(this.m_battlecrySourceCard);
    Entity entity = this.m_battlecrySourceCard.GetEntity();
    Spell actorSpell = this.m_battlecrySourceCard.GetActorSpell(SpellType.BATTLECRY, true);
    if ((bool) ((Object) actorSpell))
      actorSpell.ActivateState(SpellStateType.CANCEL);
    Spell playSpell = this.m_battlecrySourceCard.GetPlaySpell(true);
    if ((bool) ((Object) playSpell))
      playSpell.ActivateState(SpellStateType.CANCEL);
    Spell customSummonSpell = this.m_battlecrySourceCard.GetCustomSummonSpell();
    if ((bool) ((Object) customSummonSpell))
      customSummonSpell.ActivateState(SpellStateType.CANCEL);
    ZoneMgr.Get().CancelLocalZoneChange(this.m_lastZoneChangeList, (ZoneMgr.ChangeCompleteCallback) ((changeList, userData) => this.m_cancelingBattlecryCards.Remove((Card) userData)), (object) this.m_battlecrySourceCard);
    this.m_lastZoneChangeList = (ZoneChangeList) null;
    this.RollbackSpentMana(entity);
    this.ClearBattlecrySourceCard();
  }

  private bool IsCancelingBattlecryCard(Card card)
  {
    return this.m_cancelingBattlecryCards.Contains(card);
  }

  public void DoEndTurnButton()
  {
    if (GameMgr.Get().IsSpectator())
      return;
    GameState gameState = GameState.Get();
    if (gameState.IsResponsePacketBlocked() || EndTurnButton.Get().IsInputBlocked())
      return;
    switch (gameState.GetResponseMode())
    {
      case GameState.ResponseMode.OPTION:
        Network.Options optionsPacket = gameState.GetOptionsPacket();
        for (int index = 0; index < optionsPacket.List.Count; ++index)
        {
          Network.Options.Option option = optionsPacket.List[index];
          if (option.Type == Network.Options.Option.OptionType.END_TURN || option.Type == Network.Options.Option.OptionType.PASS)
          {
            if (!gameState.GetGameEntity().NotifyOfEndTurnButtonPushed())
              break;
            gameState.SetSelectedOption(index);
            gameState.SendOption();
            this.HidePhoneHand();
            this.DoEndTurnButton_Option_OnEndTurnRequested();
            break;
          }
        }
        break;
      case GameState.ResponseMode.CHOICE:
        Network.EntityChoices friendlyEntityChoices = gameState.GetFriendlyEntityChoices();
        List<Entity> chosenEntities = gameState.GetChosenEntities();
        if (chosenEntities.Count < friendlyEntityChoices.CountMin)
          break;
        ChoiceCardMgr.Get().OnSendChoices(friendlyEntityChoices, chosenEntities);
        gameState.SendChoices();
        break;
    }
  }

  private void DoEndTurnButton_Option_OnEndTurnRequested()
  {
    if ((Object) TurnTimer.Get() != (Object) null)
      TurnTimer.Get().OnEndTurnRequested();
    EndTurnButton.Get().OnEndTurnRequested();
  }

  public bool DoNetworkResponse(Entity entity, bool checkValidInput = true)
  {
    if ((Object) ThinkEmoteManager.Get() != (Object) null)
      ThinkEmoteManager.Get().NotifyOfActivity();
    GameState gameState = GameState.Get();
    if (checkValidInput && !gameState.IsEntityInputEnabled(entity))
      return false;
    GameState.ResponseMode responseMode = gameState.GetResponseMode();
    bool flag = false;
    switch (responseMode)
    {
      case GameState.ResponseMode.OPTION:
        flag = this.DoNetworkOptions(entity);
        break;
      case GameState.ResponseMode.SUB_OPTION:
        flag = this.DoNetworkSubOptions(entity);
        break;
      case GameState.ResponseMode.OPTION_TARGET:
        flag = this.DoNetworkOptionTarget(entity, (Entity) null);
        break;
      case GameState.ResponseMode.CHOICE:
        flag = this.DoNetworkChoice(entity);
        break;
    }
    if (flag)
      entity.GetCard().UpdateActorState();
    return flag;
  }

  private void OnOptionsReceived(object userData)
  {
    if ((bool) ((Object) this.m_mousedOverCard))
      this.m_mousedOverCard.UpdateProposedManaUsage();
    this.HidePhoneHandIfOutOfServerPlays();
  }

  private void OnCurrentPlayerChanged(Player player, object userData)
  {
    if (!player.IsLocalUser())
      return;
    this.m_entitiesThatPredictedMana.Clear();
  }

  private void OnOptionRejected(Network.Options.Option option, object userData)
  {
    if (option.Type == Network.Options.Option.OptionType.POWER)
    {
      Entity entity = GameState.Get().GetEntity(option.Main.ID);
      entity.GetCard().NotifyTargetingCanceled();
      if (entity.IsHeroPower())
        entity.SetTagAndHandleChange<int>(GAME_TAG.EXHAUSTED, 0);
      this.RollbackSpentMana(entity);
    }
    GameplayErrorManager.Get().DisplayMessage(GameStrings.Get("GAMEPLAY_ERROR_PLAY_REJECTED"));
  }

  private void OnTurnTimerUpdate(TurnTimerUpdate update, object userData)
  {
    if ((double) update.GetSecondsRemaining() > (double) Mathf.Epsilon)
      return;
    this.CancelOption();
  }

  private void OnGameOver(TAG_PLAYSTATE playState, object userData)
  {
    this.CancelOption();
  }

  private void OnSpectatorNotifyEvent(SpectatorNotify notify, object userData)
  {
    if (!GameMgr.Get().IsSpectator())
      return;
    GameState gameState = GameState.Get();
    if (gameState == null || !gameState.IsGameCreatedOrCreating() || (gameState.IsGameOverNowOrPending() || !notify.HasChooseOption))
      return;
    bool flag = true;
    ChooseOption chooseOption = notify.ChooseOption;
    if (notify.PlayerId != gameState.GetCurrentPlayer().GetPlayerId())
    {
      Log.Power.Print("Spectator received ChooseOption for wrong player turn receivedPlayerId={0} receivedId={1} currentTurnPlayerId={2}", new object[3]
      {
        (object) notify.PlayerId,
        (object) chooseOption.Id,
        (object) gameState.GetCurrentPlayer().GetPlayerId()
      });
    }
    else
    {
      Network.Options optionsPacket = gameState.GetOptionsPacket();
      if (optionsPacket == null)
      {
        string format = string.Format("Spectator received SpectatorNotify while options is null receivedPlayerId={0} receivedId={1} currentTurnPlayerId={2}", (object) notify.PlayerId, (object) chooseOption.Id, (object) gameState.GetCurrentPlayer().GetPlayerId());
        Log.Power.Print(format);
        UnityEngine.Debug.LogError((object) format);
      }
      else
      {
        Network.Options.Option chosenOption = (Network.Options.Option) null;
        if (chooseOption.Index >= 0 && chooseOption.Index < optionsPacket.List.Count)
          chosenOption = optionsPacket.List[chooseOption.Index];
        if (optionsPacket == null || optionsPacket.ID != chooseOption.Id || chosenOption == null)
        {
          Log.Power.Print("Spectator received unexpected ChooseOption playerId={0} receivedId={1} receivedIndex={2} availId={3} availCount={4}", (object) notify.PlayerId, (object) chooseOption.Id, (object) chooseOption.Index, (object) (optionsPacket != null ? optionsPacket.ID.ToString() : "NULL"), (object) (optionsPacket != null ? optionsPacket.List.Count.ToString() : "NULL"));
        }
        else
        {
          gameState.SetSelectedOption(chooseOption);
          Entity entity1 = gameState.GetEntity(chosenOption.Main.ID);
          if (chosenOption.Type == Network.Options.Option.OptionType.END_TURN || chosenOption.Type == Network.Options.Option.OptionType.PASS)
          {
            gameState.GetGameEntity().NotifyOfEndTurnButtonPushed();
            GameState.Get().ClearResponseMode();
            this.OnSpectatorNotifyEvent_UpdateHighlights();
            if ((Object) this.m_mousedOverCard != (Object) null)
              GameState.Get().GetFriendlySidePlayer().CancelAllProposedMana(this.m_mousedOverCard.GetEntity());
            this.DoEndTurnButton_Option_OnEndTurnRequested();
          }
          else
          {
            if (entity1 == null)
              Log.Power.Print("Spectator received unknown entity in ChooseOption playerId={0} receivedId={1} entityId={2}", new object[3]
              {
                (object) notify.PlayerId,
                (object) chooseOption.Id,
                (object) chosenOption.Main.ID
              });
            RemoteActionHandler remoteActionHandler = RemoteActionHandler.Get();
            if ((Object) remoteActionHandler != (Object) null && (Object) remoteActionHandler.GetFriendlyHoverCard() != (Object) null && (Object) remoteActionHandler.GetFriendlyHoverCard() == (Object) entity1.GetCard())
              remoteActionHandler.GetFriendlyHoverCard().NotifyMousedOut();
            if (chooseOption.HasSubOption && chooseOption.SubOption >= 0)
            {
              if (chosenOption.Subs == null || chooseOption.SubOption >= chosenOption.Subs.Count)
              {
                Log.Power.Print("Spectator received unexpected ChooseOption SubOption playerId={0} receivedId={1} option={2} subOption={3} availSubOptions={4}", (object) notify.PlayerId, (object) chooseOption.Id, (object) chooseOption.Index, (object) chooseOption.SubOption, (object) (chosenOption.Subs != null ? chosenOption.Subs.Count : 0));
                this.OnSpectatorNotifyEvent_UpdateHighlights();
                return;
              }
              this.DoNetworkResponse(entity1, false);
              Network.Options.Option.SubOption sub = chosenOption.Subs[chooseOption.SubOption];
              Entity entity2 = gameState.GetEntity(sub.ID);
              if (entity2 == null)
              {
                Log.Power.Print("Spectator received unknown entity in ChooseOption SubOption playerId={0} receivedId={1} mainEntityId={2} subEntityId={3}", (object) notify.PlayerId, (object) chooseOption.Id, (object) chosenOption.Main.ID, (object) sub.ID);
                this.OnSpectatorNotifyEvent_UpdateHighlights();
                return;
              }
              ++this.m_spectatorNotifyCurrentToken;
              this.StartCoroutine(this.FinishSpectatorNotify_SubOption(this.m_spectatorNotifyCurrentToken, notify, chooseOption, chosenOption, entity2));
              flag = false;
            }
            else if (chooseOption.Target > 0)
            {
              Entity entity2 = gameState.GetEntity(chooseOption.Target);
              if (entity2 == null)
              {
                Log.Power.Print("Spectator received unknown target entity in ChooseOption playerId={0} receivedId={1} mainEntityId={2} targetEntityId={3}", (object) notify.PlayerId, (object) chooseOption.Id, (object) chosenOption.Main.ID, (object) chooseOption.Target);
                this.OnSpectatorNotifyEvent_UpdateHighlights();
                return;
              }
              this.DoNetworkOptionTarget(entity2, entity1);
            }
            if (!flag)
              return;
            this.OnSpectatorNotifyEvent_UpdateHighlights();
          }
        }
      }
    }
  }

  private void OnSpectatorNotifyEvent_UpdateHighlights()
  {
    GameState.Get().UpdateOptionHighlights(GameState.Get().GetOptionsPacket() ?? GameState.Get().GetLastOptions());
  }

  [DebuggerHidden]
  private IEnumerator FinishSpectatorNotify_SubOption(uint myToken, SpectatorNotify notify, ChooseOption echoPacket, Network.Options.Option chosenOption, Entity subEntity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new InputManager.\u003CFinishSpectatorNotify_SubOption\u003Ec__IteratorBE() { myToken = myToken, notify = notify, echoPacket = echoPacket, chosenOption = chosenOption, subEntity = subEntity, \u003C\u0024\u003EmyToken = myToken, \u003C\u0024\u003Enotify = notify, \u003C\u0024\u003EechoPacket = echoPacket, \u003C\u0024\u003EchosenOption = chosenOption, \u003C\u0024\u003EsubEntity = subEntity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator FinishSpectatorNotify_SubOptionImpl(uint myToken, SpectatorNotify notify, ChooseOption echoPacket, Network.Options.Option chosenOption, Entity subEntity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new InputManager.\u003CFinishSpectatorNotify_SubOptionImpl\u003Ec__IteratorBF() { echoPacket = echoPacket, myToken = myToken, notify = notify, chosenOption = chosenOption, subEntity = subEntity, \u003C\u0024\u003EechoPacket = echoPacket, \u003C\u0024\u003EmyToken = myToken, \u003C\u0024\u003Enotify = notify, \u003C\u0024\u003EchosenOption = chosenOption, \u003C\u0024\u003EsubEntity = subEntity, \u003C\u003Ef__this = this };
  }

  private bool DoNetworkChoice(Entity entity)
  {
    GameState gameState = GameState.Get();
    if (!gameState.IsChoosableEntity(entity))
      return false;
    if (gameState.RemoveChosenEntity(entity))
      return true;
    gameState.AddChosenEntity(entity);
    Network.EntityChoices friendlyEntityChoices = gameState.GetFriendlyEntityChoices();
    if (friendlyEntityChoices.CountMax == 1)
    {
      List<Entity> chosenEntities = gameState.GetChosenEntities();
      ChoiceCardMgr.Get().OnSendChoices(friendlyEntityChoices, chosenEntities);
      gameState.SendChoices();
    }
    return true;
  }

  private bool DoNetworkOptions(Entity entity)
  {
    int entityId = entity.GetEntityId();
    GameState gameState = GameState.Get();
    Network.Options optionsPacket = gameState.GetOptionsPacket();
    for (int index = 0; index < optionsPacket.List.Count; ++index)
    {
      Network.Options.Option option = optionsPacket.List[index];
      if (option.Type == Network.Options.Option.OptionType.POWER && option.Main.ID == entityId)
      {
        gameState.SetSelectedOption(index);
        if (option.Subs.Count == 0)
        {
          if (option.Main.Targets == null || option.Main.Targets.Count == 0)
            gameState.SendOption();
          else
            this.EnterOptionTargetMode();
        }
        else
        {
          gameState.EnterSubOptionMode();
          ChoiceCardMgr.Get().ShowSubOptions(entity.GetCard());
        }
        return true;
      }
    }
    if (!UniversalInputManager.Get().IsTouchMode() || !entity.GetCard().IsShowingTooltip())
      PlayErrors.DisplayPlayError(PlayErrors.GetPlayEntityError(entity), entity);
    return false;
  }

  private bool DoNetworkSubOptions(Entity entity)
  {
    int entityId = entity.GetEntityId();
    GameState gameState = GameState.Get();
    Network.Options.Option selectedNetworkOption = gameState.GetSelectedNetworkOption();
    for (int index = 0; index < selectedNetworkOption.Subs.Count; ++index)
    {
      Network.Options.Option.SubOption sub = selectedNetworkOption.Subs[index];
      if (sub.ID == entityId)
      {
        gameState.SetSelectedSubOption(index);
        if (sub.Targets == null || sub.Targets.Count == 0)
          gameState.SendOption();
        else
          this.EnterOptionTargetMode();
        return true;
      }
    }
    return false;
  }

  private bool DoNetworkOptionTarget(Entity entity, Entity simulatedSourceEntity = null)
  {
    bool flag = simulatedSourceEntity == null;
    int entityId = entity.GetEntityId();
    GameState gameState = GameState.Get();
    Network.Options.Option.SubOption networkSubOption = gameState.GetSelectedNetworkSubOption();
    Entity entity1 = !flag ? simulatedSourceEntity : gameState.GetEntity(networkSubOption.ID);
    if (flag && !networkSubOption.Targets.Contains(entityId))
    {
      Entity entity2 = gameState.GetEntity(entityId);
      PlayErrors.DisplayPlayError(PlayErrors.GetTargetEntityError(entity1, entity2), entity1);
      return false;
    }
    if ((bool) ((Object) TargetReticleManager.Get()))
      TargetReticleManager.Get().DestroyFriendlyTargetArrow(false);
    this.FinishBattlecrySourceCard();
    this.FinishSubOptions();
    if (entity1.IsHeroPower())
    {
      entity1.SetTagAndHandleChange<int>(GAME_TAG.EXHAUSTED, 1);
      this.PredictSpentMana(entity1);
    }
    gameState.SetSelectedOptionTarget(entityId);
    gameState.SendOption();
    return true;
  }

  private void EnterOptionTargetMode()
  {
    GameState gameState = GameState.Get();
    gameState.EnterOptionTargetMode();
    if (!this.m_useHandEnlarge)
      return;
    this.m_myHandZone.SetFriendlyHeroTargetingMode(gameState.FriendlyHeroIsTargetable());
    this.m_myHandZone.UpdateLayout(-1, true);
  }

  private void FinishBattlecrySourceCard()
  {
    if ((Object) this.m_battlecrySourceCard == (Object) null)
      return;
    this.ClearBattlecrySourceCard();
  }

  private void ClearBattlecrySourceCard()
  {
    if (this.m_isInBattleCryEffect && (Object) this.m_battlecrySourceCard != (Object) null)
      this.EndBattleCryEffect();
    this.m_battlecrySourceCard = (Card) null;
    RemoteActionHandler.Get().NotifyOpponentOfCardDropped();
    if (!this.m_useHandEnlarge)
      return;
    this.m_myHandZone.SetFriendlyHeroTargetingMode(false);
    this.m_myHandZone.UpdateLayout(-1, true);
  }

  private void CancelSubOptions()
  {
    Card optionParentCard = ChoiceCardMgr.Get().GetSubOptionParentCard();
    if ((Object) optionParentCard == (Object) null)
      return;
    ChoiceCardMgr.Get().CancelSubOptions();
    Entity entity = optionParentCard.GetEntity();
    if (!entity.IsHeroPower())
    {
      ZoneMgr.Get().CancelLocalZoneChange(this.m_lastZoneChangeList, (ZoneMgr.ChangeCompleteCallback) null, (object) null);
      this.m_lastZoneChangeList = (ZoneChangeList) null;
    }
    this.RollbackSpentMana(entity);
    this.DropSubOptionParentCard();
  }

  private void FinishSubOptions()
  {
    if ((Object) ChoiceCardMgr.Get().GetSubOptionParentCard() == (Object) null)
      return;
    this.DropSubOptionParentCard();
  }

  public void DropSubOptionParentCard()
  {
    Log.Hand.Print("DropSubOptionParentCard()");
    ChoiceCardMgr.Get().ClearSubOptions();
    RemoteActionHandler.Get().NotifyOpponentOfCardDropped();
    if (this.m_useHandEnlarge)
    {
      this.m_myHandZone.SetFriendlyHeroTargetingMode(false);
      this.m_myHandZone.UpdateLayout(-1, true);
    }
    if (!UniversalInputManager.Get().IsTouchMode())
      return;
    this.EndMobileTargetingEffect();
  }

  private void StartMobileTargetingEffect(List<int> targets)
  {
    if (targets == null || targets.Count == 0)
      return;
    this.m_mobileTargettingEffectActors.Clear();
    using (List<int>.Enumerator enumerator = targets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity = GameState.Get().GetEntity(enumerator.Current);
        if ((Object) entity.GetCard() != (Object) null)
        {
          Actor actor = entity.GetCard().GetActor();
          this.m_mobileTargettingEffectActors.Add(actor);
          this.ApplyMobileTargettingEffectToActor(actor);
        }
      }
    }
    FullScreenFXMgr.Get().Desaturate(0.9f, 0.4f, iTween.EaseType.easeInOutQuad, (FullScreenFXMgr.EffectListener) null);
  }

  private void EndMobileTargetingEffect()
  {
    using (List<Actor>.Enumerator enumerator = this.m_mobileTargettingEffectActors.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.RemoveMobileTargettingEffectFromActor(enumerator.Current);
    }
    FullScreenFXMgr.Get().StopDesaturate(0.4f, iTween.EaseType.easeInOutQuad, (FullScreenFXMgr.EffectListener) null);
  }

  private void StartBattleCryEffect(Entity entity)
  {
    this.m_isInBattleCryEffect = true;
    Network.Options.Option selectedNetworkOption = GameState.Get().GetSelectedNetworkOption();
    if (selectedNetworkOption == null)
    {
      UnityEngine.Debug.LogError((object) "No targets for BattleCry.");
    }
    else
    {
      this.StartMobileTargetingEffect(selectedNetworkOption.Main.Targets);
      this.m_battlecrySourceCard.SetBattleCrySource(true);
    }
  }

  private void EndBattleCryEffect()
  {
    this.m_isInBattleCryEffect = false;
    this.EndMobileTargetingEffect();
    this.m_battlecrySourceCard.SetBattleCrySource(false);
  }

  private void ApplyMobileTargettingEffectToActor(Actor actor)
  {
    if ((Object) actor == (Object) null || (Object) actor.gameObject == (Object) null)
      return;
    SceneUtils.SetLayer(actor.gameObject, GameLayer.IgnoreFullScreenEffects);
    Hashtable args1 = iTween.Hash((object) "y", (object) 0.8f, (object) "time", (object) 0.4f, (object) "easeType", (object) iTween.EaseType.easeOutQuad, (object) "name", (object) "position", (object) "isLocal", (object) true);
    Hashtable args2 = iTween.Hash((object) "x", (object) 1.08f, (object) "z", (object) 1.08f, (object) "time", (object) 0.4f, (object) "easeType", (object) iTween.EaseType.easeOutQuad, (object) "name", (object) "scale");
    iTween.StopByName(actor.gameObject, "position");
    iTween.StopByName(actor.gameObject, "scale");
    iTween.MoveTo(actor.gameObject, args1);
    iTween.ScaleTo(actor.gameObject, args2);
  }

  private void RemoveMobileTargettingEffectFromActor(Actor actor)
  {
    if ((Object) actor == (Object) null || (Object) actor.gameObject == (Object) null)
      return;
    SceneUtils.SetLayer(actor.gameObject, GameLayer.Default);
    SceneUtils.SetLayer(actor.GetMeshRenderer().gameObject, GameLayer.CardRaycast);
    Hashtable args1 = iTween.Hash((object) "x", (object) 0.0f, (object) "y", (object) 0.0f, (object) "z", (object) 0.0f, (object) "time", (object) 0.5f, (object) "easeType", (object) iTween.EaseType.easeOutQuad, (object) "name", (object) "position", (object) "isLocal", (object) true);
    Hashtable args2 = iTween.Hash((object) "x", (object) 1f, (object) "z", (object) 1f, (object) "time", (object) 0.4f, (object) "easeType", (object) iTween.EaseType.easeOutQuad, (object) "name", (object) "scale");
    iTween.StopByName(actor.gameObject, "position");
    iTween.StopByName(actor.gameObject, "scale");
    iTween.MoveTo(actor.gameObject, args1);
    iTween.ScaleTo(actor.gameObject, args2);
  }

  private bool HandleMulliganHotkeys()
  {
    if ((Object) MulliganManager.Get() == (Object) null || !ApplicationMgr.IsInternal() || (!Input.GetKeyUp(KeyCode.Escape) || GameMgr.Get().IsTutorial()) || PlatformSettings.IsMobile())
      return false;
    MulliganManager.Get().SetAllMulliganCardsToHold();
    this.DoEndTurnButton();
    TurnStartManager.Get().BeginListeningForTurnEvents();
    MulliganManager.Get().SkipMulliganForDev();
    return true;
  }

  private bool HandleUniversalHotkeys()
  {
    return false;
  }

  private bool HandleGameHotkeys()
  {
    if (GameState.Get() != null && GameState.Get().IsMulliganManagerActive() || !Input.GetKeyUp(KeyCode.Escape))
      return false;
    return this.CancelOption();
  }

  private void ShowBullseyeIfNeeded()
  {
    if ((Object) TargetReticleManager.Get() == (Object) null || !TargetReticleManager.Get().IsActive())
      return;
    TargetReticleManager.Get().ShowBullseye((Object) this.m_mousedOverCard != (Object) null && GameState.Get().IsValidOptionTarget(this.m_mousedOverCard.GetEntity()));
  }

  private void ShowSkullIfNeeded()
  {
    if ((Object) this.GetBattlecrySourceCard() != (Object) null)
      return;
    Network.Options.Option.SubOption networkSubOption = GameState.Get().GetSelectedNetworkSubOption();
    if (networkSubOption == null)
      return;
    Entity entity1 = GameState.Get().GetEntity(networkSubOption.ID);
    if (entity1 == null || !entity1.IsMinion() && !entity1.IsHero())
      return;
    Entity entity2 = this.m_mousedOverCard.GetEntity();
    if (!entity2.IsMinion() && !entity2.IsHero() || (!GameState.Get().IsValidOptionTarget(entity2) || entity2.IsObfuscated()))
      return;
    if (entity2.CanBeDamagedRealTime() && entity1.GetRealTimeAttack() >= entity2.GetRealTimeRemainingHP() || entity1.IsPoisonous() && entity2.IsMinion())
    {
      Spell spell = this.m_mousedOverCard.ActivateActorSpell(SpellType.SKULL);
      if ((Object) spell != (Object) null)
      {
        spell.transform.localScale = Vector3.zero;
        iTween.ScaleTo(spell.gameObject, iTween.Hash((object) "scale", (object) Vector3.one, (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.easeOutElastic));
      }
    }
    if ((!entity1.CanBeDamagedRealTime() || entity2.GetRealTimeAttack() < entity1.GetRealTimeRemainingHP()) && (!entity2.IsPoisonous() || !entity1.IsMinion()))
      return;
    Spell spell1 = entity1.GetCard().ActivateActorSpell(SpellType.SKULL);
    if (!((Object) spell1 != (Object) null))
      return;
    spell1.transform.localScale = Vector3.zero;
    iTween.ScaleTo(spell1.gameObject, iTween.Hash((object) "scale", (object) Vector3.one, (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.easeOutElastic));
  }

  private void DisableSkullIfNeeded(Card mousedOverCard)
  {
    Spell actorSpell1 = mousedOverCard.GetActorSpell(SpellType.SKULL, true);
    if ((Object) actorSpell1 != (Object) null)
    {
      iTween.Stop(actorSpell1.gameObject);
      actorSpell1.transform.localScale = Vector3.zero;
      actorSpell1.Deactivate();
    }
    Network.Options.Option.SubOption networkSubOption = GameState.Get().GetSelectedNetworkSubOption();
    if (networkSubOption == null)
      return;
    Entity entity = GameState.Get().GetEntity(networkSubOption.ID);
    if (entity == null)
      return;
    Card card = entity.GetCard();
    if ((Object) card == (Object) null)
      return;
    Spell actorSpell2 = card.GetActorSpell(SpellType.SKULL, true);
    if (!((Object) actorSpell2 != (Object) null))
      return;
    iTween.Stop(actorSpell2.gameObject);
    actorSpell2.transform.localScale = Vector3.zero;
    actorSpell2.Deactivate();
  }

  private void HandleMouseOverCard(Card card)
  {
    if (!card.IsInputEnabled())
      return;
    GameState gameState = GameState.Get();
    this.m_mousedOverCard = card;
    bool flag = gameState.IsFriendlySidePlayerTurn() && (bool) ((Object) TargetReticleManager.Get()) && TargetReticleManager.Get().IsActive();
    if (GameMgr.Get() != null && GameMgr.Get().IsSpectator())
      flag = false;
    if (gameState.IsMainPhase() && (Object) this.m_heldCard == (Object) null && (!ChoiceCardMgr.Get().HasSubOption() && !flag) && (!UniversalInputManager.Get().IsTouchMode() || (Object) card.gameObject == (Object) this.m_lastObjectMousedDown))
      this.SetShouldShowTooltip();
    card.NotifyMousedOver();
    if (gameState.IsMulliganManagerActive() && card.GetEntity().IsControlledByFriendlySidePlayer() && !(bool) UniversalInputManager.UsePhoneUI)
      TooltipPanelManager.Get().UpdateKeywordHelpForMulliganCard(card.GetEntity(), card.GetActor());
    this.ShowBullseyeIfNeeded();
    this.ShowSkullIfNeeded();
  }

  private void HandleMouseOffCard()
  {
    PegCursor.Get().SetMode(PegCursor.Mode.UP);
    Card mousedOverCard = this.m_mousedOverCard;
    this.m_mousedOverCard = (Card) null;
    mousedOverCard.HideTooltip();
    mousedOverCard.NotifyMousedOut();
    this.ShowBullseyeIfNeeded();
    this.DisableSkullIfNeeded(mousedOverCard);
  }

  public void HandleMemberClick()
  {
    if (!((Object) this.m_mousedOverObject == (Object) null))
      return;
    RaycastHit hitInfo1;
    if (UniversalInputManager.Get().GetInputHitInfo(Camera.main, GameLayer.PlayAreaCollision, out hitInfo1))
    {
      RaycastHit hitInfo2;
      if (GameState.Get() == null || GameState.Get().IsMulliganManagerActive() || UniversalInputManager.Get().GetInputHitInfo(GameLayer.CardRaycast, out hitInfo2))
        return;
      GameObject dustEffectPrefab = Board.Get().GetMouseClickDustEffectPrefab();
      if ((Object) dustEffectPrefab == (Object) null)
        return;
      GameObject parent = Object.Instantiate<GameObject>(dustEffectPrefab);
      parent.transform.position = hitInfo1.point;
      ParticleSystem[] componentsInChildren = parent.GetComponentsInChildren<ParticleSystem>();
      if (componentsInChildren == null)
        return;
      Vector3 euler = new Vector3(Input.GetAxis("Mouse Y") * 40f, Input.GetAxis("Mouse X") * 40f, 0.0f);
      foreach (ParticleSystem particleSystem in componentsInChildren)
      {
        if (particleSystem.name == "Rocks")
          particleSystem.transform.localRotation = Quaternion.Euler(euler);
        particleSystem.Play();
      }
      switch (Random.Range(1, 6))
      {
        case 1:
          SoundManager.Get().LoadAndPlay("board_common_dirt_poke_1", parent);
          break;
        case 2:
          SoundManager.Get().LoadAndPlay("board_common_dirt_poke_2", parent);
          break;
        case 3:
          SoundManager.Get().LoadAndPlay("board_common_dirt_poke_3", parent);
          break;
        case 4:
          SoundManager.Get().LoadAndPlay("board_common_dirt_poke_4", parent);
          break;
        case 5:
          SoundManager.Get().LoadAndPlay("board_common_dirt_poke_5", parent);
          break;
      }
    }
    else
    {
      if (!((Object) Gameplay.Get() != (Object) null))
        return;
      SoundManager.Get().LoadAndPlay("UI_MouseClick_01");
    }
  }

  public bool MouseIsMoving(float tolerance)
  {
    return (double) Mathf.Abs(Input.GetAxis("Mouse X")) > (double) tolerance || (double) Mathf.Abs(Input.GetAxis("Mouse Y")) > (double) tolerance;
  }

  public bool MouseIsMoving()
  {
    return this.MouseIsMoving(0.0f);
  }

  private void ShowTooltipIfNecessary()
  {
    if ((Object) this.m_mousedOverCard == (Object) null || !this.m_mousedOverCard.GetShouldShowTooltip())
      return;
    this.m_mousedOverTimer += Time.unscaledDeltaTime;
    if (!this.m_mousedOverCard.IsActorReady())
      return;
    if (GameState.Get().GetGameEntity().IsMouseOverDelayOverriden())
      this.m_mousedOverCard.ShowTooltip();
    else if (this.m_mousedOverCard.GetZone() is ZoneHand)
    {
      this.m_mousedOverCard.ShowTooltip();
    }
    else
    {
      if ((double) this.m_mousedOverTimer < (double) this.m_MouseOverDelay)
        return;
      this.m_mousedOverCard.ShowTooltip();
    }
  }

  private void ShowTooltipZone(GameObject hitObject, TooltipZone tooltip)
  {
    this.HideBigViewCardBacks();
    GameState gameState = GameState.Get();
    if (gameState.IsMulliganManagerActive())
      return;
    GameEntity gameEntity = gameState.GetGameEntity();
    if (gameEntity == null || gameEntity.AreTooltipsDisabled() || gameEntity.NotifyOfTooltipDisplay(tooltip))
      return;
    if ((Object) tooltip.targetObject.GetComponent<ManaCrystalMgr>() != (Object) null)
    {
      if (ManaCrystalMgr.Get().ShouldShowOverloadTooltip())
        this.ShowTooltipZone(tooltip, GameStrings.Get("GAMEPLAY_TOOLTIP_MANA_OVERLOAD_HEADLINE"), GameStrings.Get("GAMEPLAY_TOOLTIP_MANA_OVERLOAD_DESCRIPTION"));
      else
        this.ShowTooltipZone(tooltip, GameStrings.Get("GAMEPLAY_TOOLTIP_MANA_HEADLINE"), GameStrings.Get("GAMEPLAY_TOOLTIP_MANA_DESCRIPTION"));
    }
    else
    {
      ZoneDeck component1 = tooltip.targetObject.GetComponent<ZoneDeck>();
      if ((Object) component1 != (Object) null)
      {
        if (component1.m_Side == Player.Side.FRIENDLY)
        {
          if (component1.IsFatigued())
            this.ShowTooltipZone(tooltip, GameStrings.Get("GAMEPLAY_TOOLTIP_FATIGUE_DECK_HEADLINE"), GameStrings.Get("GAMEPLAY_TOOLTIP_FATIGUE_DECK_DESCRIPTION"));
          else
            this.ShowTooltipZone(tooltip, GameStrings.Get("GAMEPLAY_TOOLTIP_DECK_HEADLINE"), GameStrings.Format("GAMEPLAY_TOOLTIP_DECK_DESCRIPTION", (object) component1.GetCards().Count));
        }
        else
        {
          if (component1.m_Side != Player.Side.OPPOSING)
            return;
          if (component1.IsFatigued())
            this.ShowTooltipZone(tooltip, GameStrings.Get("GAMEPLAY_TOOLTIP_FATIGUE_ENEMYDECK_HEADLINE"), GameStrings.Get("GAMEPLAY_TOOLTIP_FATIGUE_ENEMYDECK_DESCRIPTION"));
          else
            this.ShowTooltipZone(tooltip, GameStrings.Get("GAMEPLAY_TOOLTIP_ENEMYDECK_HEADLINE"), GameStrings.Format("GAMEPLAY_TOOLTIP_ENEMYDECK_DESC", (object) component1.GetCards().Count));
        }
      }
      else
      {
        ZoneHand component2 = tooltip.targetObject.GetComponent<ZoneHand>();
        if (!((Object) component2 != (Object) null) || component2.m_Side != Player.Side.OPPOSING)
          return;
        if (GameMgr.Get().IsTutorial())
        {
          this.ShowTooltipZone(tooltip, GameStrings.Get("GAMEPLAY_TOOLTIP_ENEMYHAND_HEADLINE"), GameStrings.Get("GAMEPLAY_TOOLTIP_ENEMYHAND_DESC_TUT"));
        }
        else
        {
          int cardCount = component2.GetCardCount();
          if (cardCount == 1)
            this.ShowTooltipZone(tooltip, GameStrings.Get("GAMEPLAY_TOOLTIP_ENEMYHAND_HEADLINE"), GameStrings.Format("GAMEPLAY_TOOLTIP_ENEMYHAND_DESC_SINGLE", (object) cardCount));
          else
            this.ShowTooltipZone(tooltip, GameStrings.Get("GAMEPLAY_TOOLTIP_ENEMYHAND_HEADLINE"), GameStrings.Format("GAMEPLAY_TOOLTIP_ENEMYHAND_DESC", (object) cardCount));
        }
      }
    }
  }

  private void ShowTooltipZone(TooltipZone tooltip, string headline, string description)
  {
    GameState.Get().GetGameEntity().NotifyOfTooltipZoneMouseOver(tooltip);
    if (UniversalInputManager.Get().IsTouchMode())
      tooltip.ShowGameplayTooltipLarge(headline, description);
    else
      tooltip.ShowGameplayTooltip(headline, description);
  }

  private void HideBigViewCardBacks()
  {
  }

  private void PredictSpentMana(Entity entity)
  {
    Player friendlySidePlayer = GameState.Get().GetFriendlySidePlayer();
    if (friendlySidePlayer.HasTag(GAME_TAG.SPELLS_COST_HEALTH) && entity.IsSpell() || entity.HasTag(GAME_TAG.CARD_COSTS_HEALTH))
      return;
    int num1 = entity.GetRealTimeCost() - friendlySidePlayer.GetRealTimeTempMana();
    if (friendlySidePlayer.GetRealTimeTempMana() > 0)
    {
      int num2 = Mathf.Clamp(entity.GetRealTimeCost(), 0, friendlySidePlayer.GetRealTimeTempMana());
      friendlySidePlayer.NotifyOfUsedTempMana(num2);
      ManaCrystalMgr.Get().DestroyTempManaCrystals(num2);
    }
    if (num1 > 0)
    {
      friendlySidePlayer.NotifyOfSpentMana(num1);
      ManaCrystalMgr.Get().UpdateSpentMana(num1);
    }
    friendlySidePlayer.UpdateManaCounter();
    this.m_entitiesThatPredictedMana.Add(entity);
  }

  private void RollbackSpentMana(Entity entity)
  {
    int index = this.m_entitiesThatPredictedMana.IndexOf(entity);
    if (index < 0)
      return;
    this.m_entitiesThatPredictedMana.RemoveAt(index);
    Player friendlySidePlayer = GameState.Get().GetFriendlySidePlayer();
    int num = -entity.GetRealTimeCost() + friendlySidePlayer.GetRealTimeTempMana();
    if (friendlySidePlayer.GetRealTimeTempMana() > 0)
    {
      int numCrystals = Mathf.Clamp(entity.GetRealTimeCost(), 0, friendlySidePlayer.GetRealTimeTempMana());
      friendlySidePlayer.NotifyOfUsedTempMana(-numCrystals);
      ManaCrystalMgr.Get().AddTempManaCrystals(numCrystals);
    }
    if (num < 0)
    {
      friendlySidePlayer.NotifyOfSpentMana(num);
      ManaCrystalMgr.Get().UpdateSpentMana(num);
    }
    friendlySidePlayer.UpdateManaCounter();
  }

  public void OnManaCrystalMgrManaSpent()
  {
    if (!(bool) ((Object) this.m_mousedOverCard))
      return;
    this.m_mousedOverCard.UpdateProposedManaUsage();
  }

  private bool IsInZone(Entity entity, TAG_ZONE zoneTag)
  {
    return this.IsInZone(entity.GetCard(), zoneTag);
  }

  private bool IsInZone(Card card, TAG_ZONE zoneTag)
  {
    if ((Object) card.GetZone() == (Object) null)
      return false;
    return GameUtils.GetFinalZoneForEntity(card.GetEntity()) == zoneTag;
  }

  public bool RegisterPhoneHandShownListener(InputManager.PhoneHandShownCallback callback)
  {
    return this.RegisterPhoneHandShownListener(callback, (object) null);
  }

  public bool RegisterPhoneHandShownListener(InputManager.PhoneHandShownCallback callback, object userData)
  {
    InputManager.PhoneHandShownListener handShownListener = new InputManager.PhoneHandShownListener();
    handShownListener.SetCallback(callback);
    handShownListener.SetUserData(userData);
    if (this.m_phoneHandShownListener.Contains(handShownListener))
      return false;
    this.m_phoneHandShownListener.Add(handShownListener);
    return true;
  }

  public bool RemovePhoneHandShownListener(InputManager.PhoneHandShownCallback callback)
  {
    return this.RemovePhoneHandShownListener(callback, (object) null);
  }

  public bool RemovePhoneHandShownListener(InputManager.PhoneHandShownCallback callback, object userData)
  {
    InputManager.PhoneHandShownListener handShownListener = new InputManager.PhoneHandShownListener();
    handShownListener.SetCallback(callback);
    handShownListener.SetUserData(userData);
    return this.m_phoneHandShownListener.Remove(handShownListener);
  }

  public bool RegisterPhoneHandHiddenListener(InputManager.PhoneHandHiddenCallback callback)
  {
    return this.RegisterPhoneHandHiddenListener(callback, (object) null);
  }

  public bool RegisterPhoneHandHiddenListener(InputManager.PhoneHandHiddenCallback callback, object userData)
  {
    InputManager.PhoneHandHiddenListener handHiddenListener = new InputManager.PhoneHandHiddenListener();
    handHiddenListener.SetCallback(callback);
    handHiddenListener.SetUserData(userData);
    if (this.m_phoneHandHiddenListener.Contains(handHiddenListener))
      return false;
    this.m_phoneHandHiddenListener.Add(handHiddenListener);
    return true;
  }

  public bool RemovePhoneHandHiddenListener(InputManager.PhoneHandHiddenCallback callback)
  {
    return this.RemovePhoneHandHiddenListener(callback, (object) null);
  }

  public bool RemovePhoneHandHiddenListener(InputManager.PhoneHandHiddenCallback callback, object userData)
  {
    InputManager.PhoneHandHiddenListener handHiddenListener = new InputManager.PhoneHandHiddenListener();
    handHiddenListener.SetCallback(callback);
    handHiddenListener.SetUserData(userData);
    return this.m_phoneHandHiddenListener.Remove(handHiddenListener);
  }

  private class PhoneHandShownListener : EventListener<InputManager.PhoneHandShownCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  private class PhoneHandHiddenListener : EventListener<InputManager.PhoneHandHiddenCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  public delegate void PhoneHandShownCallback(object userData);

  public delegate void PhoneHandHiddenCallback(object userData);
}
