﻿// Decompiled with JetBrains decompiler
// Type: GeneralStoreAdventureContentDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GeneralStoreAdventureContentDisplay : MonoBehaviour
{
  public PegUIElement m_rewardChest;
  public GameObject m_rewardsFrame;
  public GameObject m_preorderFrame;
  public GameObject m_leavingSoonBanner;
  public UIBButton m_leavingSoonButton;
  public MeshRenderer m_logo;
  public MeshRenderer m_keyArt;
  private string m_leavingSoonInfoText;

  private void Awake()
  {
    if (!((Object) this.m_leavingSoonButton != (Object) null))
      return;
    this.m_leavingSoonButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OnLeavingSoonButtonClicked()));
  }

  public void UpdateAdventureType(StoreAdventureDef advDef, AdventureDbfRecord advRecord)
  {
    if ((Object) advDef == (Object) null)
      return;
    AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(advDef.m_logoTextureName), (AssetLoader.ObjectCallback) ((name, obj, data) =>
    {
      Texture texture = obj as Texture;
      if ((Object) texture == (Object) null)
        Debug.LogError((object) string.Format("Failed to load texture {0}!", (object) name));
      else
        this.m_logo.material.mainTexture = texture;
    }), (object) null, false);
    this.m_keyArt.material = advDef.m_keyArt;
    if (!((Object) this.m_leavingSoonBanner != (Object) null))
      return;
    this.m_leavingSoonBanner.SetActive(advRecord.LeavingSoon);
    if (!advRecord.LeavingSoon)
      return;
    this.m_leavingSoonInfoText = (string) advRecord.LeavingSoonText;
  }

  public void SetPreOrder(bool preorder)
  {
    if ((Object) this.m_rewardChest != (Object) null && !(bool) UniversalInputManager.UsePhoneUI)
      this.m_rewardChest.gameObject.SetActive(!preorder);
    if ((Object) this.m_rewardsFrame != (Object) null)
      this.m_rewardsFrame.SetActive(!preorder);
    if (!((Object) this.m_preorderFrame != (Object) null))
      return;
    this.m_preorderFrame.SetActive(preorder);
  }

  private void OnLeavingSoonButtonClicked()
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_STORE_ADVENTURE_LEAVING_SOON"),
      m_text = this.m_leavingSoonInfoText,
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }
}
