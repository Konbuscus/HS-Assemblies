﻿// Decompiled with JetBrains decompiler
// Type: NetherspiteBeamSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[RequireComponent(typeof (LineRenderer))]
[RequireComponent(typeof (UberCurve))]
public class NetherspiteBeamSpell : Spell
{
  [Range(1f, 1000f)]
  public int m_fullPathPolys = 50;
  [Range(1f, 1000f)]
  public int m_blockedPathPolys = 5;
  public bool m_targetMinionToRight = true;
  public string m_beamFadeInMaterialVar = string.Empty;
  public float m_beamFadeInTime = 1f;
  private List<GameObject> m_visualizers = new List<GameObject>();
  public List<Vector3> m_sourceCardOffsets;
  public List<Vector3> m_destCardOffsets;
  public List<Vector3> m_fullPathPoints;
  public bool m_visualizeControlPoints;
  private int m_beamFadeInPropertyID;
  public Spell m_beamSourceSpell;
  public Spell m_beamTargetMinionSpell;
  public Spell m_beamTargetHeroSpell;
  public ParticleSystem m_fullPathParticles;
  public ParticleSystem m_blockedPathParticles;
  private bool m_usingFullPath;
  private Actor m_targetActor;
  private Spell m_beamTargetSpellInstance;
  private Spell m_beamSourceSpellInstance;
  private UberCurve m_uberCurve;
  private LineRenderer m_lineRenderer;
  private Material m_beamMaterial;

  protected override void Awake()
  {
    base.Awake();
    this.m_uberCurve = this.GetComponent<UberCurve>();
    this.m_lineRenderer = this.GetComponent<LineRenderer>();
    this.m_beamMaterial = this.m_lineRenderer.material;
    if (string.IsNullOrEmpty(this.m_beamFadeInMaterialVar))
      return;
    this.m_beamFadeInPropertyID = Shader.PropertyToID(this.m_beamFadeInMaterialVar);
  }

  protected override void OnBirth(SpellStateType prevStateType)
  {
    base.OnBirth(prevStateType);
    if ((Object) this.m_beamSourceSpell != (Object) null)
    {
      this.m_beamSourceSpellInstance = Object.Instantiate<Spell>(this.m_beamSourceSpell);
      this.m_beamSourceSpellInstance.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnSpellStateFinished));
      this.m_beamSourceSpellInstance.transform.parent = this.GetSourceCard().GetActor().transform;
      TransformUtil.Identity((Component) this.m_beamSourceSpellInstance);
      this.m_beamSourceSpellInstance.Activate();
    }
    if ((Object) this.m_fullPathParticles != (Object) null)
    {
      this.m_fullPathParticles.transform.parent = this.GetSourceCard().GetActor().transform;
      TransformUtil.Identity((Component) this.m_fullPathParticles);
    }
    if ((Object) this.m_blockedPathParticles != (Object) null)
    {
      this.m_blockedPathParticles.transform.parent = this.GetSourceCard().GetActor().transform;
      TransformUtil.Identity((Component) this.m_blockedPathParticles);
    }
    this.StartCoroutine("DoUpdate");
  }

  protected override void OnDeath(SpellStateType prevStateType)
  {
    base.OnDeath(prevStateType);
    if ((Object) this.m_beamSourceSpellInstance != (Object) null)
      this.m_beamSourceSpellInstance.ActivateState(SpellStateType.DEATH);
    if ((Object) this.m_fullPathParticles != (Object) null)
    {
      this.m_fullPathParticles.Stop();
      this.m_fullPathParticles.Clear();
    }
    if ((Object) this.m_blockedPathParticles != (Object) null)
    {
      this.m_blockedPathParticles.Stop();
      this.m_blockedPathParticles.Clear();
    }
    this.StopCoroutine("DoUpdate");
  }

  [DebuggerHidden]
  private IEnumerator DoUpdate()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NetherspiteBeamSpell.\u003CDoUpdate\u003Ec__Iterator2C6() { \u003C\u003Ef__this = this };
  }

  private void UpdateBeamFade(float fadeValue)
  {
    Color color = this.m_beamMaterial.GetColor(this.m_beamFadeInPropertyID);
    color.a = fadeValue;
    this.m_beamMaterial.SetColor(this.m_beamFadeInPropertyID, color);
  }

  private void UpdateBlockedPathControlPoints(Actor minionToRight)
  {
    int num = this.m_sourceCardOffsets.Count + this.m_destCardOffsets.Count;
    if (this.m_uberCurve.m_controlPoints.Count != num)
    {
      this.m_uberCurve.m_controlPoints.Clear();
      for (int index = 0; index < num; ++index)
        this.m_uberCurve.m_controlPoints.Add(new UberCurve.UberCurveControlPoint());
    }
    int index1 = 0;
    Card sourceCard = this.GetSourceCard();
    int index2 = 0;
    while (index2 < this.m_sourceCardOffsets.Count)
    {
      this.m_uberCurve.m_controlPoints[index1].position = sourceCard.transform.position + this.m_sourceCardOffsets[index2];
      ++index2;
      ++index1;
    }
    int index3 = 0;
    while (index3 < this.m_destCardOffsets.Count)
    {
      this.m_uberCurve.m_controlPoints[index1].position = minionToRight.transform.position + this.m_destCardOffsets[index3];
      ++index3;
      ++index1;
    }
  }

  private void UpdateFullPathControlPoints()
  {
    int num = this.m_sourceCardOffsets.Count + this.m_fullPathPoints.Count;
    if (this.m_uberCurve.m_controlPoints.Count != num)
    {
      this.m_uberCurve.m_controlPoints.Clear();
      for (int index = 0; index < num; ++index)
        this.m_uberCurve.m_controlPoints.Add(new UberCurve.UberCurveControlPoint());
    }
    int index1 = 0;
    Card sourceCard = this.GetSourceCard();
    int index2 = 0;
    while (index2 < this.m_sourceCardOffsets.Count)
    {
      this.m_uberCurve.m_controlPoints[index1].position = sourceCard.transform.position + this.m_sourceCardOffsets[index2];
      ++index2;
      ++index1;
    }
    int index3 = 0;
    while (index3 < this.m_fullPathPoints.Count)
    {
      this.m_uberCurve.m_controlPoints[index1].position = this.m_fullPathPoints[index3];
      ++index3;
      ++index1;
    }
  }

  private Actor GetTargetMinion()
  {
    int zonePosition = this.GetSourceCard().GetZonePosition();
    ZonePlay battlefieldZone = this.GetSourceCard().GetController().GetBattlefieldZone();
    int pos = !this.m_targetMinionToRight ? zonePosition - 1 : zonePosition + 1;
    while (pos > 0 && pos <= battlefieldZone.GetCardCount())
    {
      Card cardAtPos = battlefieldZone.GetCardAtPos(pos);
      if (cardAtPos.IsActorReady())
        return cardAtPos.GetActor();
      pos += !this.m_targetMinionToRight ? -1 : 1;
    }
    return (Actor) null;
  }

  private void VisualizeControlPoints()
  {
    if (!this.m_visualizeControlPoints)
    {
      using (List<GameObject>.Enumerator enumerator = this.m_visualizers.GetEnumerator())
      {
        while (enumerator.MoveNext())
          Object.Destroy((Object) enumerator.Current);
      }
      this.m_visualizers.Clear();
    }
    else if (this.m_visualizers.Count != this.m_uberCurve.m_controlPoints.Count)
    {
      using (List<GameObject>.Enumerator enumerator = this.m_visualizers.GetEnumerator())
      {
        while (enumerator.MoveNext())
          Object.Destroy((Object) enumerator.Current);
      }
      this.m_visualizers.Clear();
      for (int index = 0; index < this.m_uberCurve.m_controlPoints.Count; ++index)
      {
        GameObject primitive = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        primitive.transform.position = this.m_uberCurve.m_controlPoints[index].position;
        primitive.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        this.m_visualizers.Add(primitive);
      }
    }
    else
    {
      for (int index = 0; index < this.m_uberCurve.m_controlPoints.Count; ++index)
        this.m_visualizers[index].transform.position = this.m_uberCurve.transform.TransformPoint(this.m_uberCurve.m_controlPoints[index].position);
    }
  }

  private void OnSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    Object.Destroy((Object) spell.gameObject);
  }
}
