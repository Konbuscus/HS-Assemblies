﻿// Decompiled with JetBrains decompiler
// Type: Tutorial_03
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Tutorial_03 : TutorialEntity
{
  private int numTauntGorillasPlayed;
  private bool enemyPlayedBigBrother;
  private bool needATaunterVOPlayed;
  private bool overrideMouseOver;
  private bool monkeyLinePlayedOnce;
  private bool defenselessVoPlayed;
  private bool seenTheBrother;
  private bool warnedAgainstAttackingGorilla;
  private bool victory;
  private Notification thatsABadPlayPopup;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_TUTORIAL_03_JAINA_17_33");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_18_34");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_01_24");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_05_25");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_07_26");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_12_28");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_13_29");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_16_32");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_14_30");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_15_31");
    this.PreloadSound("VO_TUTORIAL_03_JAINA_20_36");
    this.PreloadSound("VO_TUTORIAL_03_MUKLA_01_01");
    this.PreloadSound("VO_TUTORIAL_03_MUKLA_03_03");
    this.PreloadSound("VO_TUTORIAL_03_MUKLA_04_04");
    this.PreloadSound("VO_TUTORIAL_03_MUKLA_05_05");
    this.PreloadSound("VO_TUTORIAL_03_MUKLA_06_06");
    this.PreloadSound("VO_TUTORIAL_03_MUKLA_07_07");
  }

  public override void NotifyOfGameOver(TAG_PLAYSTATE gameResult)
  {
    if (gameResult == TAG_PLAYSTATE.WON)
      this.victory = true;
    base.NotifyOfGameOver(gameResult);
    if (gameResult == TAG_PLAYSTATE.WON)
    {
      this.SetTutorialProgress(TutorialProgress.MUKLA_COMPLETE);
      this.PlaySound("VO_TUTORIAL_03_MUKLA_07_07", 1f, true, false);
    }
    else if (gameResult == TAG_PLAYSTATE.TIED)
    {
      this.PlaySound("VO_TUTORIAL_03_MUKLA_07_07", 1f, true, false);
    }
    else
    {
      if (gameResult != TAG_PLAYSTATE.LOST)
        return;
      this.SetTutorialLostProgress(TutorialProgress.MUKLA_COMPLETE);
    }
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_03.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator214() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator GetReadyForBro()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_03.\u003CGetReadyForBro\u003Ec__Iterator215() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_03.\u003CHandleMissionEventWithTiming\u003Ec__Iterator216() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator ShowTauntPopup()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_03.\u003CShowTauntPopup\u003Ec__Iterator217() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator AdjustBigBrotherTransform()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_03.\u003CAdjustBigBrotherTransform\u003Ec__Iterator218() { \u003C\u003Ef__this = this };
  }

  private Card FindCardInEnemyBattlefield(string cardId)
  {
    ZonePlay battlefieldZone = GameState.Get().GetOpposingSidePlayer().GetBattlefieldZone();
    for (int index = 0; index < battlefieldZone.GetCardCount(); ++index)
    {
      Card cardAtIndex = battlefieldZone.GetCardAtIndex(index);
      if (!(cardAtIndex.GetEntity().GetCardId() != cardId))
        return cardAtIndex;
    }
    return (Card) null;
  }

  public override bool IsKeywordHelpDelayOverridden()
  {
    return true;
  }

  public override void NotifyOfCardMousedOff(Entity mousedOffEntity)
  {
    this.overrideMouseOver = false;
  }

  public override void NotifyOfCardMousedOver(Entity mousedOverEntity)
  {
    if (!mousedOverEntity.HasTaunt())
      return;
    this.overrideMouseOver = true;
  }

  public override bool NotifyOfBattlefieldCardClicked(Entity clickedEntity, bool wasInTargetMode)
  {
    if (!base.NotifyOfBattlefieldCardClicked(clickedEntity, wasInTargetMode))
      return false;
    if (!wasInTargetMode || !(clickedEntity.GetCardId() == "TU4c_007") || this.warnedAgainstAttackingGorilla)
      return true;
    this.warnedAgainstAttackingGorilla = true;
    this.HandleMissionEvent(11);
    return false;
  }

  private void ShowDontPolymorphYourselfPopup(Vector3 origin)
  {
    if ((Object) this.thatsABadPlayPopup != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.thatsABadPlayPopup);
    this.thatsABadPlayPopup = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(origin.x - 3f, origin.y, origin.z), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("TUTORIAL01_HELP_07"), true);
    NotificationManager.Get().DestroyNotification(this.thatsABadPlayPopup, 2.5f);
  }

  private void ShowDontFireballYourselfPopup(Vector3 origin)
  {
    if ((Object) this.thatsABadPlayPopup != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.thatsABadPlayPopup);
    this.thatsABadPlayPopup = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(origin.x - 3f, origin.y, origin.z), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("TUTORIAL01_HELP_07"), true);
    NotificationManager.Get().DestroyNotification(this.thatsABadPlayPopup, 2.5f);
  }

  public override bool IsMouseOverDelayOverriden()
  {
    return this.overrideMouseOver;
  }

  public override bool ShouldShowCrazyKeywordTooltip()
  {
    return true;
  }

  public override void NotifyOfDefeatCoinAnimation()
  {
    if (!this.victory)
      return;
    this.PlaySound("VO_TUTORIAL_03_JAINA_20_36", 1f, true, false);
  }

  public override List<RewardData> GetCustomRewards()
  {
    if (!this.victory)
      return (List<RewardData>) null;
    List<RewardData> rewardDataList = new List<RewardData>();
    CardRewardData cardRewardData = new CardRewardData("CS2_022", TAG_PREMIUM.NORMAL, 2);
    cardRewardData.MarkAsDummyReward();
    rewardDataList.Add((RewardData) cardRewardData);
    return rewardDataList;
  }
}
