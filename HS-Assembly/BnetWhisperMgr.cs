﻿// Decompiled with JetBrains decompiler
// Type: BnetWhisperMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;

public class BnetWhisperMgr
{
  private List<BnetWhisper> m_whispers = new List<BnetWhisper>();
  private Map<BnetGameAccountId, List<BnetWhisper>> m_whisperMap = new Map<BnetGameAccountId, List<BnetWhisper>>();
  private int m_firstPendingWhisperIndex = -1;
  private List<BnetWhisperMgr.WhisperListener> m_whisperListeners = new List<BnetWhisperMgr.WhisperListener>();
  private const int MAX_WHISPERS_PER_PLAYER = 100;
  private static BnetWhisperMgr s_instance;

  public static BnetWhisperMgr Get()
  {
    if (BnetWhisperMgr.s_instance == null)
    {
      BnetWhisperMgr.s_instance = new BnetWhisperMgr();
      ApplicationMgr.Get().WillReset += (Action) (() =>
      {
        BnetWhisperMgr.s_instance.m_whispers.Clear();
        BnetWhisperMgr.s_instance.m_whisperMap.Clear();
        BnetWhisperMgr.s_instance.m_firstPendingWhisperIndex = -1;
        BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(BnetWhisperMgr.Get().OnPlayersChanged));
      });
    }
    return BnetWhisperMgr.s_instance;
  }

  public void Initialize()
  {
    Network.Get().SetWhisperHandler(new Network.WhisperHandler(this.OnWhispers));
    Network.Get().AddBnetErrorListener(BnetFeature.Whisper, new Network.BnetErrorCallback(this.OnBnetError));
  }

  public void Shutdown()
  {
    Network.Get().RemoveBnetErrorListener(BnetFeature.Whisper, new Network.BnetErrorCallback(this.OnBnetError));
    Network.Get().SetWhisperHandler((Network.WhisperHandler) null);
  }

  public List<BnetWhisper> GetWhispersWithPlayer(BnetPlayer player)
  {
    if (player == null)
      return (List<BnetWhisper>) null;
    List<BnetWhisper> bnetWhisperList1 = new List<BnetWhisper>();
    using (Map<BnetGameAccountId, BnetGameAccount>.KeyCollection.Enumerator enumerator = player.GetGameAccounts().Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        List<BnetWhisper> bnetWhisperList2;
        if (this.m_whisperMap.TryGetValue(enumerator.Current, out bnetWhisperList2))
          bnetWhisperList1.AddRange((IEnumerable<BnetWhisper>) bnetWhisperList2);
      }
    }
    if (bnetWhisperList1.Count == 0)
      return (List<BnetWhisper>) null;
    bnetWhisperList1.Sort((Comparison<BnetWhisper>) ((a, b) =>
    {
      ulong timestampMicrosec1 = a.GetTimestampMicrosec();
      ulong timestampMicrosec2 = b.GetTimestampMicrosec();
      if (timestampMicrosec1 < timestampMicrosec2)
        return -1;
      return timestampMicrosec1 > timestampMicrosec2 ? 1 : 0;
    }));
    return bnetWhisperList1;
  }

  public bool SendWhisper(BnetPlayer player, string message)
  {
    if (player == null)
      return false;
    BnetGameAccount bestGameAccount = player.GetBestGameAccount();
    if (bestGameAccount == (BnetGameAccount) null)
      return false;
    Network.SendWhisper(bestGameAccount.GetId(), message);
    return true;
  }

  public bool HavePendingWhispers()
  {
    return this.m_firstPendingWhisperIndex >= 0;
  }

  public bool AddWhisperListener(BnetWhisperMgr.WhisperCallback callback)
  {
    return this.AddWhisperListener(callback, (object) null);
  }

  public bool AddWhisperListener(BnetWhisperMgr.WhisperCallback callback, object userData)
  {
    BnetWhisperMgr.WhisperListener whisperListener = new BnetWhisperMgr.WhisperListener();
    whisperListener.SetCallback(callback);
    whisperListener.SetUserData(userData);
    if (this.m_whisperListeners.Contains(whisperListener))
      return false;
    this.m_whisperListeners.Add(whisperListener);
    return true;
  }

  public bool RemoveWhisperListener(BnetWhisperMgr.WhisperCallback callback)
  {
    return this.RemoveWhisperListener(callback, (object) null);
  }

  public bool RemoveWhisperListener(BnetWhisperMgr.WhisperCallback callback, object userData)
  {
    BnetWhisperMgr.WhisperListener whisperListener = new BnetWhisperMgr.WhisperListener();
    whisperListener.SetCallback(callback);
    whisperListener.SetUserData(userData);
    return this.m_whisperListeners.Remove(whisperListener);
  }

  private void OnWhispers(BnetWhisper[] whispers)
  {
    for (int index = 0; index < whispers.Length; ++index)
    {
      BnetWhisper whisper = whispers[index];
      this.m_whispers.Add(whisper);
      if (!this.HavePendingWhispers())
      {
        if (WhisperUtil.IsDisplayable(whisper))
        {
          this.ProcessWhisper(this.m_whispers.Count - 1);
        }
        else
        {
          this.m_firstPendingWhisperIndex = index;
          BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
        }
      }
    }
  }

  private bool OnBnetError(BnetErrorInfo info, object userData)
  {
    Log.Mike.Print("BnetWhisperMgr.OnBnetError() - event={0} error={1}", new object[2]
    {
      (object) info.GetFeatureEvent(),
      (object) info.GetError()
    });
    return true;
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    if (!this.CanProcessPendingWhispers())
      return;
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    this.ProcessPendingWhispers();
  }

  private void FireWhisperEvent(BnetWhisper whisper)
  {
    foreach (BnetWhisperMgr.WhisperListener whisperListener in this.m_whisperListeners.ToArray())
      whisperListener.Fire(whisper);
  }

  private bool CanProcessPendingWhispers()
  {
    if (this.m_firstPendingWhisperIndex < 0)
      return true;
    for (int pendingWhisperIndex = this.m_firstPendingWhisperIndex; pendingWhisperIndex < this.m_whispers.Count; ++pendingWhisperIndex)
    {
      if (!WhisperUtil.IsDisplayable(this.m_whispers[pendingWhisperIndex]))
        return false;
    }
    return true;
  }

  private void ProcessPendingWhispers()
  {
    if (this.m_firstPendingWhisperIndex < 0)
      return;
    for (int pendingWhisperIndex = this.m_firstPendingWhisperIndex; pendingWhisperIndex < this.m_whispers.Count; ++pendingWhisperIndex)
      this.ProcessWhisper(pendingWhisperIndex);
    this.m_firstPendingWhisperIndex = -1;
  }

  private void ProcessWhisper(int index)
  {
    BnetWhisper whisper = this.m_whispers[index];
    BnetGameAccountId theirGameAccountId = WhisperUtil.GetTheirGameAccountId(whisper);
    if (!BnetUtils.CanReceiveWhisperFrom(theirGameAccountId))
    {
      this.m_whispers.RemoveAt(index);
    }
    else
    {
      List<BnetWhisper> whispers;
      if (!this.m_whisperMap.TryGetValue(theirGameAccountId, out whispers))
      {
        whispers = new List<BnetWhisper>();
        this.m_whisperMap.Add(theirGameAccountId, whispers);
      }
      else if (whispers.Count == 100)
        this.RemoveOldestWhisper(whispers);
      whispers.Add(whisper);
      this.FireWhisperEvent(whisper);
    }
  }

  private void RemoveOldestWhisper(List<BnetWhisper> whispers)
  {
    BnetWhisper whisper = whispers[0];
    whispers.RemoveAt(0);
    this.m_whispers.Remove(whisper);
  }

  private class WhisperListener : EventListener<BnetWhisperMgr.WhisperCallback>
  {
    public void Fire(BnetWhisper whisper)
    {
      this.m_callback(whisper, this.m_userData);
    }
  }

  public delegate void WhisperCallback(BnetWhisper whisper, object userData);
}
