﻿// Decompiled with JetBrains decompiler
// Type: FriendlyScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class FriendlyScene : PlayGameScene
{
  private static FriendlyScene s_instance;

  protected override void Awake()
  {
    base.Awake();
    FriendlyScene.s_instance = this;
  }

  private void OnDestroy()
  {
    FriendlyScene.s_instance = (FriendlyScene) null;
  }

  public static FriendlyScene Get()
  {
    return FriendlyScene.s_instance;
  }

  public override string GetScreenName()
  {
    return "Friendly";
  }

  public override void Unload()
  {
    base.Unload();
    FriendlyDisplay.Get().Unload();
  }
}
