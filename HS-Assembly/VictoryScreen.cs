﻿// Decompiled with JetBrains decompiler
// Type: VictoryScreen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class VictoryScreen : EndGameScreen
{
  private const string NO_GOLDEN_HERO = "none";
  public GamesWonIndicator m_gamesWonIndicator;
  public Transform m_goldenHeroEventBone;
  private bool m_showWinProgress;
  private bool m_showGoldenHeroEvent;
  private bool m_hasParsedCompletedQuests;
  private bool m_goldenHeroCardDefReady;
  private GoldenHeroEvent m_goldenHeroEvent;
  private CardDef m_goldenHeroCardDef;
  private Achievement m_goldenHeroAchievement;

  protected override void Awake()
  {
    base.Awake();
    this.m_gamesWonIndicator.Hide();
    if (!this.ShouldMakeUtilRequests())
      return;
    if (GameMgr.Get().IsTutorial() && !GameMgr.Get().IsSpectator())
      NetCache.Get().RegisterTutorialEndGameScreen(new NetCache.NetCacheCallback(((EndGameScreen) this).OnNetCacheReady));
    else
      NetCache.Get().RegisterScreenEndOfGame(new NetCache.NetCacheCallback(((EndGameScreen) this).OnNetCacheReady));
  }

  protected override void ShowStandardFlow()
  {
    base.ShowStandardFlow();
    if (!GameMgr.Get().IsTutorial() || GameMgr.Get().IsSpectator())
      this.m_hitbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(((EndGameScreen) this).ContinueButtonPress_PrevMode));
    else if (GameUtils.AreAllTutorialsComplete())
    {
      LoadingScreen.Get().SetFadeColor(Color.white);
      this.m_hitbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ContinueButtonPress_FirstTimeHub));
    }
    else if (DemoMgr.Get().GetMode() == DemoMode.APPLE_STORE && GameUtils.GetNextTutorial() == 0)
      this.StartCoroutine(DemoMgr.Get().CompleteAppleStoreDemo());
    else
      this.m_hitbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(((EndGameScreen) this).ContinueButtonPress_TutorialProgress));
  }

  protected override void LoadGoldenHeroEvent()
  {
    AssetLoader.Get().LoadGameObject("Hero2GoldHero", new AssetLoader.GameObjectCallback(this.OnGoldenHeroEventLoaded), (object) null, false);
  }

  protected override bool ShowGoldenHeroEvent()
  {
    if (!this.JustEarnedGoldenHero())
      return false;
    if (this.m_goldenHeroEvent.gameObject.activeInHierarchy)
    {
      this.m_goldenHeroEvent.Hide();
      this.m_showGoldenHeroEvent = false;
      return false;
    }
    this.m_goldenHeroAchievement.AckCurrentProgressAndRewardNotices();
    this.SetPlayingBlockingAnim(true);
    this.m_goldenHeroEvent.RegisterAnimationDoneListener(new GoldenHeroEvent.AnimationDoneListener(this.NotifyOfGoldenHeroAnimComplete));
    this.m_twoScoop.StopAnimating();
    this.m_goldenHeroEvent.Show();
    this.m_twoScoop.m_heroActor.transform.parent = this.m_goldenHeroEvent.m_heroBone;
    this.m_twoScoop.m_heroActor.transform.localPosition = Vector3.zero;
    this.m_twoScoop.m_heroActor.transform.localScale = new Vector3(1.375f, 1.375f, 1.375f);
    return true;
  }

  protected override bool JustEarnedGoldenHero()
  {
    if (this.m_hasParsedCompletedQuests)
      return this.m_showGoldenHeroEvent;
    string goldenHeroCardId = this.GetGoldenHeroCardID();
    if (goldenHeroCardId != "none")
    {
      CardPortraitQuality quality = new CardPortraitQuality(3, TAG_PREMIUM.GOLDEN);
      DefLoader.Get().LoadCardDef(goldenHeroCardId, new DefLoader.LoadDefCallback<CardDef>(this.OnGoldenHeroCardDefLoaded), new object(), quality);
    }
    this.m_hasParsedCompletedQuests = true;
    this.m_showGoldenHeroEvent = goldenHeroCardId != "none";
    return this.m_showGoldenHeroEvent;
  }

  protected void ContinueButtonPress_FirstTimeHub(UIEvent e)
  {
    if (!this.HasShownScoops())
      return;
    this.HideTwoScoop();
    if (this.ShowNextReward())
    {
      SoundManager.Get().LoadAndPlay("VO_INNKEEPER_TUT_COMPLETE_05");
    }
    else
    {
      if (this.ShowNextCompletedQuest())
        return;
      this.ContinueButtonPress_Common();
      this.m_hitbox.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ContinueButtonPress_FirstTimeHub));
      if (Network.ShouldBeConnectedToAurora())
      {
        this.BackToMode(SceneMgr.Mode.HUB);
      }
      else
      {
        NotificationManager.Get().CreateTutorialDialog("GLOBAL_MEDAL_REWARD_CONGRATULATIONS", "TUTORIAL_MOBILE_COMPLETE_CONGRATS", "GLOBAL_OKAY", new UIEvent.Handler(this.UserPressedStartButton), new Vector2(0.5f, 0.0f), true);
        this.m_hitbox.gameObject.SetActive(false);
        this.m_continueText.gameObject.SetActive(false);
      }
    }
  }

  protected void UserPressedStartButton(UIEvent e)
  {
    WebAuth.ClearLoginData();
    this.BackToMode(SceneMgr.Mode.RESET);
  }

  protected override void OnTwoScoopShown()
  {
    if ((Object) BnetBar.Get() != (Object) null)
      BnetBar.Get().SuppressLoginTooltip(true);
    if (!this.m_showWinProgress)
      return;
    this.m_gamesWonIndicator.Show();
  }

  protected override void OnTwoScoopHidden()
  {
    if (!this.m_showWinProgress)
      return;
    this.m_gamesWonIndicator.Hide();
  }

  private void OnGoldenHeroEventLoaded(string name, GameObject go, object callbackData)
  {
    this.StartCoroutine(this.WaitUntilTwoScoopLoaded(name, go, callbackData));
  }

  public void NotifyOfGoldenHeroAnimComplete()
  {
    this.SetPlayingBlockingAnim(false);
    this.m_goldenHeroEvent.RemoveAnimationDoneListener(new GoldenHeroEvent.AnimationDoneListener(this.NotifyOfGoldenHeroAnimComplete));
  }

  [DebuggerHidden]
  private IEnumerator WaitUntilTwoScoopLoaded(string name, GameObject go, object callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new VictoryScreen.\u003CWaitUntilTwoScoopLoaded\u003Ec__Iterator9E() { go = go, \u003C\u0024\u003Ego = go, \u003C\u003Ef__this = this };
  }

  protected override void InitGoldRewardUI()
  {
    NetCache.NetCacheRewardProgress netObject1 = NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>();
    string str = EndGameScreen.GetFriendlyChallengeRewardText();
    if (string.IsNullOrEmpty(str))
    {
      TAG_GOLD_REWARD_STATE tag = GameState.Get().GetFriendlySidePlayer().GetTag<TAG_GOLD_REWARD_STATE>(GAME_TAG.GOLD_REWARD_STATE);
      if (tag != TAG_GOLD_REWARD_STATE.ELIGIBLE)
      {
        Log.Rachelle.Print(string.Format("VictoryScreen.InitGoldRewardUI(): goldRewardState = {0}", (object) tag));
        switch (tag - 3)
        {
          case TAG_GOLD_REWARD_STATE.INVALID:
            str = GameStrings.Format("GLOBAL_GOLD_REWARD_ALREADY_CAPPED", (object) netObject1.MaxGoldPerDay);
            break;
          case TAG_GOLD_REWARD_STATE.ELIGIBLE:
            str = GameStrings.Get("GLOBAL_GOLD_REWARD_BAD_RATING");
            break;
          case TAG_GOLD_REWARD_STATE.WRONG_GAME_TYPE:
            str = GameStrings.Get("GLOBAL_GOLD_REWARD_SHORT_GAME_BY_TIME");
            break;
          case TAG_GOLD_REWARD_STATE.ALREADY_CAPPED:
            str = GameStrings.Get("GLOBAL_GOLD_REWARD_OVER_CAIS");
            break;
          default:
            return;
        }
      }
    }
    if (!string.IsNullOrEmpty(str))
    {
      this.m_noGoldRewardText.gameObject.SetActive(true);
      this.m_noGoldRewardText.Text = str;
    }
    else
    {
      NetCache.NetCacheGamesPlayed netObject2 = NetCache.Get().GetNetObject<NetCache.NetCacheGamesPlayed>();
      Log.Rachelle.Print(string.Format("EndGameTwoScoop.UpdateData(): {0}/{1} wins towards {2} gold", (object) netObject2.FreeRewardProgress, (object) netObject1.WinsPerGold, (object) netObject1.GoldPerReward));
      this.m_showWinProgress = true;
      this.m_gamesWonIndicator.Init(Reward.Type.GOLD, netObject1.GoldPerReward, netObject1.WinsPerGold, netObject2.FreeRewardProgress, GamesWonIndicator.InnKeeperTrigger.NONE);
    }
  }

  private string GetGoldenHeroCardID()
  {
    int index = 0;
    using (List<Achievement>.Enumerator enumerator1 = this.m_completedQuests.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Achievement current1 = enumerator1.Current;
        if (current1.AchieveType == Achievement.AchType.UNLOCK_GOLDEN_HERO)
        {
          this.m_goldenHeroAchievement = current1;
          using (List<RewardData>.Enumerator enumerator2 = current1.Rewards.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              RewardData current2 = enumerator2.Current;
              if (current2.RewardType != Reward.Type.CARD)
              {
                ++index;
              }
              else
              {
                CardRewardData cardReward = current2 as CardRewardData;
                CollectionManager.Get().AddCardReward(cardReward, false);
                this.m_completedQuests.RemoveAt(index);
                return cardReward.CardID;
              }
            }
          }
        }
      }
    }
    return "none";
  }

  private void OnGoldenHeroCardDefLoaded(string cardId, CardDef def, object userData)
  {
    this.m_goldenHeroCardDef = def;
    this.m_goldenHeroCardDefReady = true;
  }
}
