﻿// Decompiled with JetBrains decompiler
// Type: TAG_TYPE
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum TAG_TYPE
{
  UNKNOWN,
  BOOL,
  NUMBER,
  COUNTER,
  ENTITY,
  PLAYER,
  TEAM,
  ENTITY_DEFINITION,
  STRING,
}
