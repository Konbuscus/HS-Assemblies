﻿// Decompiled with JetBrains decompiler
// Type: QuestLogButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class QuestLogButton : PegUIElement
{
  public HighlightState m_highlight;

  private void Start()
  {
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnButtonOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnButtonOut));
    SoundManager.Get().Load("quest_log_button_mouse_over");
  }

  private void OnButtonOver(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("quest_log_button_mouse_over", this.gameObject);
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
    TooltipZone component = this.GetComponent<TooltipZone>();
    if ((Object) component == (Object) null)
      return;
    component.ShowBoxTooltip(GameStrings.Get("GLUE_TOOLTIP_BUTTON_QUESTLOG_HEADLINE"), GameStrings.Get("GLUE_TOOLTIP_BUTTON_QUESTLOG_DESC"));
  }

  private void OnButtonOut(UIEvent e)
  {
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
    TooltipZone component = this.GetComponent<TooltipZone>();
    if (!((Object) component != (Object) null))
      return;
    component.HideTooltip();
  }
}
