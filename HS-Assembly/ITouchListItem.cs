﻿// Decompiled with JetBrains decompiler
// Type: ITouchListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public interface ITouchListItem
{
  Bounds LocalBounds { get; }

  bool IsHeader { get; }

  bool Visible { get; set; }

  GameObject gameObject { get; }

  Transform transform { get; }

  T GetComponent<T>() where T : Component;
}
