﻿// Decompiled with JetBrains decompiler
// Type: GvG_Promotion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GvG_Promotion : MonoBehaviour
{
  private GameObject m_arenaObj;
  private bool m_AnimExisted;

  private void Start()
  {
    this.m_arenaObj = Box.Get().m_ForgeButton.gameObject;
    this.transform.parent = this.m_arenaObj.transform;
    Animation component = this.m_arenaObj.GetComponent<Animation>();
    this.m_AnimExisted = true;
    if ((Object) component == (Object) null)
    {
      this.m_arenaObj.AddComponent<Animation>();
      this.m_AnimExisted = false;
    }
    this.GetComponent<Spell>().Activate();
  }

  private void OnDestroy()
  {
    this.m_arenaObj.transform.localPosition = new Vector3(-0.004992113f, 1.260711f, 0.4331615f);
    this.m_arenaObj.transform.localScale = new Vector3(1f, 1f, 1f);
    this.m_arenaObj.transform.localRotation = new Quaternion(0.0f, -180f, 0.0f, 0.0f);
    if (this.m_AnimExisted)
      return;
    Animation component = this.m_arenaObj.GetComponent<Animation>();
    if (!((Object) component != (Object) null))
      return;
    Object.Destroy((Object) component);
  }
}
