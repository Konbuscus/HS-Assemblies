﻿// Decompiled with JetBrains decompiler
// Type: CardAreaCircles
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class CardAreaCircles : MonoBehaviour
{
  public GameObject m_attackGem;
  public GameObject m_description;
  public GameObject m_healthGem;
  public GameObject m_manaGem;
  public GameObject m_minionPortrait;
  public GameObject m_minionRace;
  public GameObject m_nameBanner;
  public GameObject m_rarityGem;

  private void Awake()
  {
    this.HideAll();
  }

  public void SetCirclesForCard(CollectibleCard card)
  {
    this.HideAll();
    if (card.IsAttackChanged)
      this.m_attackGem.SetActive(true);
    if (card.IsCardInHandTextChanged)
      this.m_description.SetActive(true);
    if (card.IsHealthChanged)
      this.m_healthGem.SetActive(true);
    if (!card.IsManaCostChanged)
      return;
    this.m_manaGem.SetActive(true);
  }

  private void HideAll()
  {
    IEnumerator enumerator = this.transform.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
        ((Component) enumerator.Current).gameObject.SetActive(false);
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
  }
}
