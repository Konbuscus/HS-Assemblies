﻿// Decompiled with JetBrains decompiler
// Type: ScrollingUVs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ScrollingUVs : MonoBehaviour
{
  public Vector2 uvAnimationRate = new Vector2(1f, 1f);
  private Vector2 m_offset = Vector2.zero;
  public int materialIndex;
  private Material m_material;

  private void Start()
  {
    this.m_material = this.GetComponent<Renderer>().materials[this.materialIndex];
  }

  private void LateUpdate()
  {
    if (!this.GetComponent<Renderer>().enabled)
      return;
    if ((Object) this.m_material == (Object) null)
      this.m_material = this.GetComponent<Renderer>().materials[this.materialIndex];
    this.m_offset += this.uvAnimationRate * Time.deltaTime;
    this.m_material.SetTextureOffset("_MainTex", this.m_offset);
  }
}
