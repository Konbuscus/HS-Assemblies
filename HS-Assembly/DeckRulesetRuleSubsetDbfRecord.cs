﻿// Decompiled with JetBrains decompiler
// Type: DeckRulesetRuleSubsetDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DeckRulesetRuleSubsetDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_DeckRulesetRuleId;
  [SerializeField]
  private int m_SubsetId;

  [DbfField("DECK_RULESET_RULE_ID", "which DECK_RULESET_RULE.ID")]
  public int DeckRulesetRuleId
  {
    get
    {
      return this.m_DeckRulesetRuleId;
    }
  }

  [DbfField("SUBSET_ID", "a SUBSET.ID parameter to the rule")]
  public int SubsetId
  {
    get
    {
      return this.m_SubsetId;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    DeckRulesetRuleSubsetDbfAsset ruleSubsetDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (DeckRulesetRuleSubsetDbfAsset)) as DeckRulesetRuleSubsetDbfAsset;
    if ((UnityEngine.Object) ruleSubsetDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("DeckRulesetRuleSubsetDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < ruleSubsetDbfAsset.Records.Count; ++index)
      ruleSubsetDbfAsset.Records[index].StripUnusedLocales();
    records = (object) ruleSubsetDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetDeckRulesetRuleId(int v)
  {
    this.m_DeckRulesetRuleId = v;
  }

  public void SetSubsetId(int v)
  {
    this.m_SubsetId = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckRulesetRuleSubsetDbfRecord.\u003C\u003Ef__switch\u0024map35 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckRulesetRuleSubsetDbfRecord.\u003C\u003Ef__switch\u0024map35 = new Dictionary<string, int>(2)
        {
          {
            "DECK_RULESET_RULE_ID",
            0
          },
          {
            "SUBSET_ID",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckRulesetRuleSubsetDbfRecord.\u003C\u003Ef__switch\u0024map35.TryGetValue(key, out num))
      {
        if (num == 0)
          return (object) this.DeckRulesetRuleId;
        if (num == 1)
          return (object) this.SubsetId;
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (DeckRulesetRuleSubsetDbfRecord.\u003C\u003Ef__switch\u0024map36 == null)
    {
      // ISSUE: reference to a compiler-generated field
      DeckRulesetRuleSubsetDbfRecord.\u003C\u003Ef__switch\u0024map36 = new Dictionary<string, int>(2)
      {
        {
          "DECK_RULESET_RULE_ID",
          0
        },
        {
          "SUBSET_ID",
          1
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!DeckRulesetRuleSubsetDbfRecord.\u003C\u003Ef__switch\u0024map36.TryGetValue(key, out num))
      return;
    if (num != 0)
    {
      if (num != 1)
        return;
      this.SetSubsetId((int) val);
    }
    else
      this.SetDeckRulesetRuleId((int) val);
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckRulesetRuleSubsetDbfRecord.\u003C\u003Ef__switch\u0024map37 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckRulesetRuleSubsetDbfRecord.\u003C\u003Ef__switch\u0024map37 = new Dictionary<string, int>(2)
        {
          {
            "DECK_RULESET_RULE_ID",
            0
          },
          {
            "SUBSET_ID",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckRulesetRuleSubsetDbfRecord.\u003C\u003Ef__switch\u0024map37.TryGetValue(key, out num))
      {
        if (num == 0)
          return typeof (int);
        if (num == 1)
          return typeof (int);
      }
    }
    return (System.Type) null;
  }
}
