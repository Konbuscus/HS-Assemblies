﻿// Decompiled with JetBrains decompiler
// Type: AssetFamily
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum AssetFamily
{
  Actor,
  AudioClip,
  Board,
  CardBack,
  CardPrefab,
  CardPremium,
  CardTexture,
  CardXML,
  FontDef,
  GameObject,
  Movie,
  Screen,
  Sound,
  Spell,
  Texture,
}
