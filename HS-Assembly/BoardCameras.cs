﻿// Decompiled with JetBrains decompiler
// Type: BoardCameras
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoardCameras : MonoBehaviour
{
  public AudioListener m_AudioListener;
  private static BoardCameras s_instance;

  private void Awake()
  {
    BoardCameras.s_instance = this;
    if (!((Object) LoadingScreen.Get() != (Object) null))
      return;
    LoadingScreen.Get().NotifyMainSceneObjectAwoke(this.gameObject);
  }

  private void OnDestroy()
  {
    BoardCameras.s_instance = (BoardCameras) null;
  }

  public static BoardCameras Get()
  {
    return BoardCameras.s_instance;
  }

  public AudioListener GetAudioListener()
  {
    return this.m_AudioListener;
  }
}
