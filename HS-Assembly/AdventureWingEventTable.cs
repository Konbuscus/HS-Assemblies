﻿// Decompiled with JetBrains decompiler
// Type: AdventureWingEventTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

[CustomEditClass]
public class AdventureWingEventTable : StateEventTable
{
  private const string s_EventPlateActivate = "PlateActivate";
  private const string s_EventPlateDeactivate = "PlateDeactivate";
  private const string s_EventPlateInitialText = "PlateInitialText";
  private const string s_EventPlateBuy = "PlateBuy";
  private const string s_EventPlateInitialBuy = "PlateInitialBuy";
  private const string s_EventPlateKey = "PlateKey";
  private const string s_EventPlateKeyNotRecommended = "PlateKeyNotRecommended";
  private const string s_EventPlateInitialKey = "PlateInitialKey";
  private const string s_EventPlateInitialKeyNotRecommended = "PlateInitialKeyNotRecommended";
  private const string s_EventPlateOpen = "PlateOpen";
  private const string s_EventBigChestShow = "BigChestShow";
  private const string s_EventBigChestStayOpen = "BigChestStayOpen";
  private const string s_EventBigChestOpen = "BigChestOpen";
  private const string s_EventBigChestCover = "BigChestCover";
  private const string s_EventPlateCoverPreviewChest = "PlateCoverPreviewChest";

  public bool IsPlateKey()
  {
    string lastState = this.GetLastState();
    if (!(lastState == "PlateKey") && !(lastState == "PlateInitialKey") && !(lastState == "PlateKeyNotRecommended"))
      return lastState == "PlateInitialKeyNotRecommended";
    return true;
  }

  public bool IsPlateBuy()
  {
    string lastState = this.GetLastState();
    if (!(lastState == "PlateBuy"))
      return lastState == "PlateInitialBuy";
    return true;
  }

  public bool IsPlateInitialText()
  {
    return this.GetLastState() == "PlateInitialText";
  }

  public bool IsPlateInOrGoingToAnActiveState()
  {
    string lastState = this.GetLastState();
    if (lastState != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureWingEventTable.\u003C\u003Ef__switch\u0024map0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureWingEventTable.\u003C\u003Ef__switch\u0024map0 = new Dictionary<string, int>(8)
        {
          {
            "PlateActivate",
            0
          },
          {
            "PlateInitialText",
            0
          },
          {
            "PlateBuy",
            0
          },
          {
            "PlateInitialBuy",
            0
          },
          {
            "PlateKey",
            0
          },
          {
            "PlateKeyNotRecommended",
            0
          },
          {
            "PlateInitialKey",
            0
          },
          {
            "PlateInitialKeyNotRecommended",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureWingEventTable.\u003C\u003Ef__switch\u0024map0.TryGetValue(lastState, out num) && num == 0)
        return true;
    }
    return false;
  }

  public void PlateActivate()
  {
    this.TriggerState("PlateActivate", true, (string) null);
  }

  public void PlateDeactivate()
  {
    this.TriggerState("PlateDeactivate", true, (string) null);
  }

  public void PlateBuy(bool initial = false)
  {
    if (this.IsPlateBuy())
      return;
    this.TriggerState(!initial ? "PlateBuy" : "PlateInitialBuy", true, (string) null);
  }

  public void PlateInitialText()
  {
    this.TriggerState("PlateInitialText", true, (string) null);
  }

  public void PlateKey(bool isRecommended, bool initial)
  {
    if (!isRecommended)
    {
      string eventName = !initial ? "PlateKeyNotRecommended" : "PlateInitialKeyNotRecommended";
      if (this.GetStateEvent(eventName) != null)
      {
        this.TriggerState(eventName, true, (string) null);
        return;
      }
    }
    this.TriggerState(!initial ? "PlateKey" : "PlateInitialKey", true, (string) null);
  }

  public void PlateOpen(float delay = 0.0f)
  {
    this.SetFloatVar("PlateOpen", "PostAnimationDelay", delay);
    this.TriggerState("PlateOpen", true, (string) null);
  }

  public void PlateCoverPreviewChest()
  {
    this.TriggerState("PlateCoverPreviewChest", false, (string) null);
  }

  public void BigChestShow()
  {
    this.TriggerState("BigChestShow", true, (string) null);
  }

  public void BigChestStayOpen()
  {
    this.TriggerState("BigChestStayOpen", true, (string) null);
  }

  public void BigChestOpen()
  {
    this.TriggerState("BigChestOpen", true, (string) null);
  }

  public void BigChestCover()
  {
    this.TriggerState("BigChestCover", true, (string) null);
  }

  public void AddOpenPlateStartEventListener(StateEventTable.StateEventTrigger dlg, bool once = false)
  {
    this.AddStateEventStartListener("PlateOpen", dlg, once);
  }

  public void RemoveOpenPlateStartEventListener(StateEventTable.StateEventTrigger dlg)
  {
    this.RemoveStateEventStartListener("PlateOpen", dlg);
  }

  public void AddOpenPlateEndEventListener(StateEventTable.StateEventTrigger dlg, bool once = false)
  {
    this.AddStateEventEndListener("PlateOpen", dlg, once);
  }

  public void RemoveOpenPlateEndEventListener(StateEventTable.StateEventTrigger dlg)
  {
    this.RemoveStateEventEndListener("PlateOpen", dlg);
  }

  public void AddOpenChestStartEventListener(StateEventTable.StateEventTrigger dlg, bool once = false)
  {
    this.AddStateEventStartListener("BigChestOpen", dlg, once);
  }

  public void RemoveOpenChestStartEventListener(StateEventTable.StateEventTrigger dlg)
  {
    this.RemoveStateEventStartListener("BigChestOpen", dlg);
  }

  public void AddOpenChestEndEventListener(StateEventTable.StateEventTrigger dlg, bool once = false)
  {
    this.AddStateEventEndListener("BigChestOpen", dlg, once);
  }

  public void RemoveOpenChestEndEventListener(StateEventTable.StateEventTrigger dlg)
  {
    this.RemoveStateEventEndListener("BigChestOpen", dlg);
  }
}
