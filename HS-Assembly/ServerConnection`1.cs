﻿// Decompiled with JetBrains decompiler
// Type: ServerConnection`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class ServerConnection<PacketType> where PacketType : PacketFormat, new()
{
  private object m_lock = new object();
  private Socket m_socket;
  private int m_port;
  private ClientConnection<PacketType> m_currentConnection;
  private bool m_listening;

  ~ServerConnection()
  {
    this.Disconnect();
  }

  public IPEndPoint GetLocalEndPoint()
  {
    if (this.m_socket == null)
      return (IPEndPoint) null;
    return this.m_socket.LocalEndPoint as IPEndPoint;
  }

  public bool Open(int port)
  {
    if (this.m_socket != null)
      return false;
    IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, port);
    try
    {
      this.m_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      this.m_socket.Bind((EndPoint) ipEndPoint);
      this.m_socket.Listen(16);
    }
    catch (Exception ex)
    {
      Debug.LogWarning((object) ("SeverConnection: error opening inbound connection: " + ex.Message + " (this probably occurred because you have multiple game instances running)"));
      this.m_socket = (Socket) null;
      return false;
    }
    return this.Listen();
  }

  public void Disconnect()
  {
    if (this.m_socket == null || !this.m_socket.Connected)
      return;
    this.m_socket.Shutdown(SocketShutdown.Both);
    this.m_socket.Close();
  }

  public bool Listen()
  {
    lock (this.m_lock)
    {
      if (this.m_listening)
        return true;
      this.m_listening = true;
    }
    if (this.m_socket == null)
      return false;
    try
    {
      this.m_socket.BeginAccept(new AsyncCallback(ServerConnection<PacketType>.OnAccept), (object) this);
    }
    catch (Exception ex)
    {
      lock (this.m_lock)
        this.m_listening = false;
      Debug.LogError((object) ("error listening for incoming connections: " + ex.Message));
      this.m_socket = (Socket) null;
      return false;
    }
    return true;
  }

  private static void OnAccept(IAsyncResult ar)
  {
    ServerConnection<PacketType> asyncState = (ServerConnection<PacketType>) ar.AsyncState;
    if (asyncState == null)
      return;
    if (asyncState.m_socket == null)
      return;
    try
    {
      Socket socket = asyncState.m_socket.EndAccept(ar);
      asyncState.m_currentConnection = new ClientConnection<PacketType>(socket);
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("error accepting connection: " + ex.Message));
    }
    asyncState.m_listening = false;
  }

  public ClientConnection<PacketType> GetNextAcceptedConnection()
  {
    if (this.m_currentConnection != null)
    {
      ClientConnection<PacketType> currentConnection = this.m_currentConnection;
      this.m_currentConnection = (ClientConnection<PacketType>) null;
      return currentConnection;
    }
    this.Listen();
    return (ClientConnection<PacketType>) null;
  }
}
