﻿// Decompiled with JetBrains decompiler
// Type: SpellFacing
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum SpellFacing
{
  NONE,
  SAME_AS_SOURCE_HERO,
  TOWARDS_TARGET,
  TOWARDS_TARGET_HERO,
  TOWARDS_SOURCE_HERO,
  TOWARDS_SOURCE,
  TOWARDS_SOURCE_AUTO,
  SAME_AS_SOURCE,
  SAME_AS_SOURCE_AUTO,
  TOWARDS_CHOSEN_TARGET,
  OPPOSITE_OF_SOURCE,
  OPPOSITE_OF_SOURCE_AUTO,
  OPPOSITE_OF_SOURCE_HERO,
  TOWARDS_OPPONENT_HERO,
}
