﻿// Decompiled with JetBrains decompiler
// Type: OverrideCustomDeathSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class OverrideCustomDeathSpell : SuperSpell
{
  public bool m_SuppressKeywordDeaths = true;
  public float m_KeywordDeathDelay = 0.6f;
  public Spell m_CustomDeathSpell;

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    using (List<GameObject>.Enumerator enumerator = this.GetVisualTargets().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          Card component = current.GetComponent<Card>();
          component.OverrideCustomDeathSpell(Object.Instantiate<Spell>(this.m_CustomDeathSpell));
          component.SuppressKeywordDeaths(this.m_SuppressKeywordDeaths);
          component.SetKeywordDeathDelaySec(this.m_KeywordDeathDelay);
        }
      }
    }
    --this.m_effectsPendingFinish;
    this.FinishIfPossible();
  }
}
