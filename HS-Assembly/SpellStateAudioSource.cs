﻿// Decompiled with JetBrains decompiler
// Type: SpellStateAudioSource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

[Serializable]
public class SpellStateAudioSource
{
  public bool m_Enabled = true;
  public AudioSource m_AudioSource;
  public float m_StartDelaySec;
  public bool m_PlayGlobally;
  public bool m_StopOnStateChange;
  public string m_Comment;

  public void Init()
  {
    if ((UnityEngine.Object) this.m_AudioSource == (UnityEngine.Object) null)
      return;
    this.m_AudioSource.playOnAwake = false;
  }

  public void Play(SpellState parent)
  {
    if (!this.m_Enabled)
      return;
    if (Mathf.Approximately(this.m_StartDelaySec, 0.0f))
      this.PlayNow();
    else
      parent.StartCoroutine(this.DelayedPlay());
  }

  public void Stop()
  {
    if (!this.m_Enabled || (UnityEngine.Object) this.m_AudioSource == (UnityEngine.Object) null || (this.m_PlayGlobally || !this.m_StopOnStateChange))
      return;
    this.m_AudioSource.Stop();
  }

  [DebuggerHidden]
  private IEnumerator DelayedPlay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpellStateAudioSource.\u003CDelayedPlay\u003Ec__Iterator31D() { \u003C\u003Ef__this = this };
  }

  private void PlayNow()
  {
    if ((UnityEngine.Object) this.m_AudioSource == (UnityEngine.Object) null)
      return;
    if (this.m_PlayGlobally)
      SoundManager.Get().PlayClip(new SoundPlayClipArgs()
      {
        m_clip = this.m_AudioSource.clip,
        m_volume = new float?(this.m_AudioSource.volume),
        m_pitch = new float?(this.m_AudioSource.pitch),
        m_category = new SoundCategory?(SoundManager.Get().GetCategory(this.m_AudioSource)),
        m_parentObject = this.m_AudioSource.gameObject
      });
    else
      SoundManager.Get().Play(this.m_AudioSource, true);
  }
}
