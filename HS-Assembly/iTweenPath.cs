﻿// Decompiled with JetBrains decompiler
// Type: iTweenPath
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Pixelplacement/iTweenPath")]
public class iTweenPath : MonoBehaviour
{
  public static Map<string, iTweenPath> paths = new Map<string, iTweenPath>();
  public string pathName = string.Empty;
  public Color pathColor = Color.cyan;
  public List<Vector3> nodes = new List<Vector3>() { Vector3.zero, Vector3.zero };
  public string initialName = string.Empty;
  public bool pathVisible = true;
  public bool initialized;

  private void OnEnable()
  {
    string key = iTweenPath.FixupPathName(this.pathName);
    if (iTweenPath.paths.ContainsKey(key))
      return;
    iTweenPath.paths.Add(key, this);
  }

  private void OnDisable()
  {
    iTweenPath.paths.Remove(iTweenPath.FixupPathName(this.pathName));
  }

  private void OnDrawGizmosSelected()
  {
    if (!this.pathVisible || this.nodes.Count <= 0)
      return;
    iTween.DrawPath(this.nodes.ToArray(), this.pathColor);
  }

  public static Vector3[] GetPath(string requestedName)
  {
    requestedName = iTweenPath.FixupPathName(requestedName);
    if (iTweenPath.paths.ContainsKey(requestedName))
      return iTweenPath.paths[requestedName].nodes.ToArray();
    Debug.Log((object) ("No path with that name (" + requestedName + ") exists! Are you sure you wrote it correctly?"));
    return (Vector3[]) null;
  }

  public static Vector3[] GetPathReversed(string requestedName)
  {
    requestedName = iTweenPath.FixupPathName(requestedName);
    if (iTweenPath.paths.ContainsKey(requestedName))
    {
      List<Vector3> range = iTweenPath.paths[requestedName].nodes.GetRange(0, iTweenPath.paths[requestedName].nodes.Count);
      range.Reverse();
      return range.ToArray();
    }
    Debug.Log((object) ("No path with that name (" + requestedName + ") exists! Are you sure you wrote it correctly?"));
    return (Vector3[]) null;
  }

  public static string FixupPathName(string name)
  {
    return name.ToLower();
  }
}
