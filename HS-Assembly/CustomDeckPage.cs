﻿// Decompiled with JetBrains decompiler
// Type: CustomDeckPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System.Collections.Generic;
using UnityEngine;

public class CustomDeckPage : MonoBehaviour
{
  private List<GameObject> m_deckCovers = new List<GameObject>();
  private List<CollectionDeckBoxVisual> m_customDecks = new List<CollectionDeckBoxVisual>();
  public const int MAX_CUSTOM_DECKS_TO_DISPLAY = 9;
  public Vector3 m_customDeckStart;
  public Vector3 m_customDeckScale;
  public float m_customDeckHorizontalSpacing;
  public float m_customDeckVerticalSpacing;
  public CollectionDeckBoxVisual m_deckboxPrefab;
  public Vector3 m_deckCoverOffset;
  public GameObject m_deckboxCoverPrefab;
  public PlayMakerFSM m_vineGlowBurst;
  public GameObject[] m_customVineGlowToggle;
  private List<CollectionDeck> m_collectionDecks;
  private Texture m_customTrayStandardTexture;
  private Texture m_customTrayWildTexture;
  private int m_numCustomDecks;
  private CustomDeckPage.DeckButtonCallback m_deckButtonCallback;

  public void Show()
  {
    this.GetComponent<Renderer>().enabled = true;
    for (int index = 0; index < this.m_numCustomDecks; ++index)
    {
      if (index < this.m_customDecks.Count)
        this.m_customDecks[index].Show();
    }
  }

  public void Hide()
  {
    this.GetComponent<Renderer>().enabled = false;
    for (int index = 0; index < this.m_numCustomDecks; ++index)
    {
      if (index < this.m_customDecks.Count)
        this.m_customDecks[index].Hide();
    }
  }

  public bool PageReady()
  {
    if ((Object) this.m_customTrayStandardTexture != (Object) null && (Object) this.m_customTrayWildTexture != (Object) null)
      return this.AreAllCustomDecksReady();
    return false;
  }

  public CollectionDeckBoxVisual GetDeckboxWithDeckID(long deckID)
  {
    if (deckID <= 0L)
      return (CollectionDeckBoxVisual) null;
    using (List<CollectionDeckBoxVisual>.Enumerator enumerator = this.m_customDecks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckBoxVisual current = enumerator.Current;
        if (current.GetDeckID() == deckID)
          return current;
      }
    }
    return (CollectionDeckBoxVisual) null;
  }

  public void UpdateTrayTransitionValue(float transitionValue)
  {
    this.GetComponent<Renderer>().material.SetFloat("_Transistion", transitionValue);
    using (List<GameObject>.Enumerator enumerator = this.m_deckCovers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Renderer componentInChildren = enumerator.Current.GetComponentInChildren<Renderer>();
        if ((Object) componentInChildren != (Object) null)
          componentInChildren.material.SetFloat("_Transistion", transitionValue);
      }
    }
  }

  public void PlayVineGlowBurst(bool useFX, bool hasValidStandardDeck)
  {
    if (!((Object) this.m_vineGlowBurst != (Object) null))
      return;
    string eventName = !useFX ? (!hasValidStandardDeck ? "GlowVinesCustomNoFX" : "GlowVinesNoFX") : (!hasValidStandardDeck ? "GlowVinesCustom" : "GlowVines");
    if (string.IsNullOrEmpty(eventName))
      return;
    this.m_vineGlowBurst.SendEvent(eventName);
  }

  public void SetTrayTextures(Texture standardTexture, Texture wildTexture)
  {
    Material material = this.GetComponent<Renderer>().material;
    material.mainTexture = standardTexture;
    material.SetTexture("_MainTex2", wildTexture);
    this.m_customTrayStandardTexture = standardTexture;
    this.m_customTrayWildTexture = wildTexture;
    this.InitCustomDecks();
  }

  public void SetDecks(List<CollectionDeck> decks)
  {
    this.m_collectionDecks = decks;
  }

  public void SetDeckButtonCallback(CustomDeckPage.DeckButtonCallback callback)
  {
    this.m_deckButtonCallback = callback;
  }

  public void EnableDeckButtons(bool enable)
  {
    using (List<CollectionDeckBoxVisual>.Enumerator enumerator = this.m_customDecks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckBoxVisual current = enumerator.Current;
        current.SetEnabled(enable);
        if (!enable)
          current.SetHighlightState(ActorStateType.HIGHLIGHT_OFF);
      }
    }
  }

  public void TransitionWildDecks()
  {
    int index = 0;
    using (List<CollectionDeck>.Enumerator enumerator = this.m_collectionDecks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeck current = enumerator.Current;
        if (current.Type == DeckType.NORMAL_DECK)
        {
          CollectionDeckBoxVisual customDeck = this.m_customDecks[index];
          if (current.IsWild)
            customDeck.TransitionFromStandardToWild();
          ++index;
        }
      }
    }
  }

  public void UpdateDeckVisuals(bool isWild, bool checkFormat, bool forceStandardVisuals)
  {
    int index = 0;
    this.m_numCustomDecks = 0;
    using (List<CollectionDeck>.Enumerator enumerator = this.m_collectionDecks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeck current = enumerator.Current;
        if (current.Type == DeckType.NORMAL_DECK)
        {
          ++this.m_numCustomDecks;
          bool isValid = current.IsTourneyValid;
          if (checkFormat)
            isValid = isValid && (current.IsValidForFormat(isWild) || forceStandardVisuals);
          CollectionDeckBoxVisual customDeck = this.m_customDecks[index];
          customDeck.SetDeckName(current.Name);
          customDeck.SetDeckID(current.ID);
          customDeck.SetHeroCardID(current.HeroCardID);
          customDeck.SetIsMissingCards(!current.IsTourneyValid);
          customDeck.SetIsValid(isValid);
          customDeck.SetFormat(current.IsWild && !forceStandardVisuals);
          Log.Kyle.Print("InitCustomDecks - is Hero Skin: {0}", (object) (GameUtils.GetCardSetFromCardID(current.HeroCardID) == TAG_CARD_SET.HERO_SKINS));
          customDeck.Show();
          this.m_deckCovers[index].SetActive(false);
          ++index;
          if (index >= this.m_customDecks.Count)
            break;
        }
      }
    }
    for (; index < this.m_customDecks.Count; ++index)
    {
      this.m_customDecks[index].Hide();
      this.m_deckCovers[index].SetActive(true);
    }
  }

  public bool HasWildDeck()
  {
    using (List<CollectionDeck>.Enumerator enumerator = this.m_collectionDecks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.IsWild)
          return true;
      }
    }
    return false;
  }

  private bool AreAllCustomDecksReady()
  {
    using (List<CollectionDeckBoxVisual>.Enumerator enumerator = this.m_customDecks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckBoxVisual current = enumerator.Current;
        if (current.GetFullDef() == null && current.GetDeckID() > 0L)
          return false;
      }
    }
    return true;
  }

  private void InitCustomDecks()
  {
    int num = 0;
    Vector3 customDeckStart = this.m_customDeckStart;
    float horizontalSpacing = this.m_customDeckHorizontalSpacing;
    float deckVerticalSpacing = this.m_customDeckVerticalSpacing;
    for (; num < 9; ++num)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      CustomDeckPage.\u003CInitCustomDecks\u003Ec__AnonStorey3AE decksCAnonStorey3Ae = new CustomDeckPage.\u003CInitCustomDecks\u003Ec__AnonStorey3AE();
      // ISSUE: reference to a compiler-generated field
      decksCAnonStorey3Ae.\u003C\u003Ef__this = this;
      GameObject gameObject1 = new GameObject();
      gameObject1.name = "DeckParent" + (object) num;
      gameObject1.transform.parent = this.gameObject.transform;
      if (num == 0)
      {
        gameObject1.transform.localPosition = customDeckStart;
      }
      else
      {
        float x = customDeckStart.x - (float) (num % 3) * horizontalSpacing;
        float z = (float) Mathf.CeilToInt((float) (num / 3)) * deckVerticalSpacing + customDeckStart.z;
        gameObject1.transform.localPosition = new Vector3(x, customDeckStart.y, z);
      }
      // ISSUE: reference to a compiler-generated field
      decksCAnonStorey3Ae.deckBox = Object.Instantiate<CollectionDeckBoxVisual>(this.m_deckboxPrefab);
      // ISSUE: reference to a compiler-generated field
      CollectionDeckBoxVisual deckBox = decksCAnonStorey3Ae.deckBox;
      string str = deckBox.name + " - " + (object) num;
      deckBox.name = str;
      // ISSUE: reference to a compiler-generated field
      decksCAnonStorey3Ae.deckBox.transform.parent = gameObject1.transform;
      // ISSUE: reference to a compiler-generated field
      decksCAnonStorey3Ae.deckBox.transform.localPosition = Vector3.zero;
      // ISSUE: reference to a compiler-generated field
      decksCAnonStorey3Ae.deckBox.SetOriginalButtonPosition();
      gameObject1.transform.localScale = this.m_customDeckScale;
      if (this.m_deckButtonCallback == null)
      {
        Debug.LogError((object) "SetDeckButtonCallback() not called in CustomDeckPage!");
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        decksCAnonStorey3Ae.deckBox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(decksCAnonStorey3Ae.\u003C\u003Em__118));
      }
      // ISSUE: reference to a compiler-generated field
      decksCAnonStorey3Ae.deckBox.SetEnabled(!DeckPickerTrayDisplay.Get().IsMissingStandardDeckTrayShown());
      // ISSUE: reference to a compiler-generated field
      this.m_customDecks.Add(decksCAnonStorey3Ae.deckBox);
      GameObject gameObject2 = Object.Instantiate<GameObject>(this.m_deckboxCoverPrefab);
      gameObject2.transform.parent = this.gameObject.transform;
      gameObject2.transform.localScale = this.m_customDeckScale;
      gameObject2.transform.position = gameObject1.transform.position + this.m_deckCoverOffset;
      Material material = gameObject2.GetComponentInChildren<Renderer>().material;
      material.mainTexture = this.m_customTrayStandardTexture;
      material.SetTexture("_MainTex2", this.m_customTrayWildTexture);
      this.m_deckCovers.Add(gameObject2);
    }
    if (this.m_collectionDecks == null)
      Debug.LogErrorFormat("m_collectionDecks not set in CustomDeckPage! Ensure that SetDecks() is called before this method!");
    else
      this.UpdateDeckVisuals(Options.Get().GetBool(Option.IN_WILD_MODE), SceneMgr.Get().GetMode() == SceneMgr.Mode.TOURNAMENT || SceneMgr.Get().GetMode() == SceneMgr.Mode.FRIENDLY, false);
  }

  private void OnSelectCustomDeck(CollectionDeckBoxVisual deckbox)
  {
    bool flag = Options.Get().GetBool(Option.IN_WILD_MODE);
    if (deckbox.IsWild() && !flag)
      DeckPickerTrayDisplay.Get().ShowClickedWildDeckInStandardPopup();
    this.m_deckButtonCallback(deckbox, true);
  }

  public delegate void DeckButtonCallback(CollectionDeckBoxVisual deckbox, bool showTrayForPhone = true);
}
