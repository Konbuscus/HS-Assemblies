﻿// Decompiled with JetBrains decompiler
// Type: Hub
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class Hub : Scene
{
  public static bool s_hasAlreadyShownTBAnimation;
  private Notification m_PracticeNotification;

  private void Start()
  {
    if (CollectionManager.Get().GetPreconDeck(TAG_CLASS.MAGE) == null)
    {
      Error.AddFatalLoc("GLOBAL_ERROR_NO_MAGE_PRECON");
    }
    else
    {
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.HUB);
      Box box = Box.Get();
      box.AddButtonPressListener(new Box.ButtonPressCallback(this.OnBoxButtonPressed));
      box.m_QuestLogButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.HideTooltipNotification));
      box.m_StoreButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.HideTooltipNotification));
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        box.m_ribbonButtons.m_questLogRibbon.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.HideTooltipNotification));
        box.m_ribbonButtons.m_storeRibbon.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.HideTooltipNotification));
      }
      SceneMgr.Get().NotifySceneLoaded();
      if (SceneMgr.Get().GetPrevMode() != SceneMgr.Mode.LOGIN)
        MusicManager.Get().StartPlaylist(MusicPlaylistType.UI_MainTitle);
      if (AchieveManager.Get().HasQuestsToShow(true))
        WelcomeQuests.Show(UserAttentionBlocker.NONE, false, (WelcomeQuests.DelOnWelcomeQuestsClosed) null, false);
      PopupDisplayManager.Get().ShowAnyOutstandingPopups();
      if (!Options.Get().GetBool(Option.HAS_SEEN_HUB, false) && UserAttentionManager.CanShowAttentionGrabber("Hub.Start:" + (object) Option.HAS_SEEN_HUB))
        this.StartCoroutine(this.DoFirstTimeHubWelcome());
      else if (!Options.Get().GetBool(Option.HAS_SEEN_100g_REMINDER, false))
      {
        if (NetCache.Get().GetNetObject<NetCache.NetCacheGoldBalance>().GetTotal() >= 100L && UserAttentionManager.CanShowAttentionGrabber("Hub.Start:" + (object) Option.HAS_SEEN_100g_REMINDER))
        {
          NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_FIRST_100_GOLD"), "VO_INNKEEPER_FIRST_100_GOLD", 0.0f, (Action) null, false);
          Options.Get().SetBool(Option.HAS_SEEN_100g_REMINDER, true);
        }
      }
      else if (TavernBrawlManager.Get().IsFirstTimeSeeingThisFeature)
        Hub.DoTavernBrawlIntroVO();
      StoreManager.Get().RegisterSuccessfulPurchaseListener(new StoreManager.SuccessfulPurchaseCallback(this.OnAdventureBundlePurchase));
      SpecialEventType activeEventType = SpecialEventVisualMgr.GetActiveEventType();
      if (activeEventType != SpecialEventType.IGNORE && AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.FORGE))
      {
        SpecialEventVisualMgr.Get().LoadEvent(activeEventType);
        SceneMgr.Get().RegisterSceneUnloadedEvent(new SceneMgr.SceneUnloadedCallback(this.OnSceneUnloaded));
      }
      if (TavernBrawlManager.Get().IsFirstTimeSeeingCurrentSeason && UserAttentionManager.CanShowAttentionGrabber("Hub.TavernBrawl.IsFirstTimeSeeingCurrentSeason") && !Hub.s_hasAlreadyShownTBAnimation)
      {
        Hub.s_hasAlreadyShownTBAnimation = true;
        this.StartCoroutine(this.DoTavernBrawlAnims());
      }
      TavernBrawlManager.Get().OnTavernBrawlUpdated += new Action(this.DoTavernBrawlAnimsCB);
      TavernBrawlManager.Get().OnSessionLimitRaised += new TavernBrawlManager.TavernBrawlSessionLimitRaisedCallback(this.MaybeDoTavernBrawlLimitRaisedAlert);
      NetCache.Get().RegisterUpdatedListener(typeof (NetCache.NetCacheFeatures), new Action(this.DoTavernBrawlAnimsCB));
      NetCache.Get().RegisterUpdatedListener(typeof (NetCache.NetCacheHeroLevels), new Action(this.DoTavernBrawlAnimsCB));
    }
  }

  private void OnDestroy()
  {
    TavernBrawlManager.Get().OnTavernBrawlUpdated -= new Action(this.DoTavernBrawlAnimsCB);
    NetCache.Get().RemoveUpdatedListener(typeof (NetCache.NetCacheFeatures), new Action(this.DoTavernBrawlAnimsCB));
    NetCache.Get().RemoveUpdatedListener(typeof (NetCache.NetCacheHeroLevels), new Action(this.DoTavernBrawlAnimsCB));
  }

  private static void DoTavernBrawlIntroVO()
  {
    if (NotificationManager.Get().HasSoundPlayedThisSession("VO_INNKEEPER_TAVERNBRAWL_PUSH_32"))
      return;
    NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_TAVERNBRAWL_PUSH_32"), "VO_INNKEEPER_TAVERNBRAWL_PUSH_32", (Action) (() => NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_TAVERNBRAWL_DESC1_29"), "VO_INNKEEPER_TAVERNBRAWL_DESC1_29", 0.0f, (Action) null, false)), false);
    NotificationManager.Get().ForceAddSoundToPlayedList("VO_INNKEEPER_TAVERNBRAWL_PUSH_32");
  }

  private void DoTavernBrawlAnimsCB()
  {
    this.StartCoroutine(this.DoTavernBrawlAnims());
  }

  [DebuggerHidden]
  private IEnumerator DoTavernBrawlAnims()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Hub.\u003CDoTavernBrawlAnims\u003Ec__IteratorEF animsCIteratorEf = new Hub.\u003CDoTavernBrawlAnims\u003Ec__IteratorEF();
    return (IEnumerator) animsCIteratorEf;
  }

  private void MaybeDoTavernBrawlLimitRaisedAlert(int lastSeenLimit, int newLimit)
  {
    int availableForPurchase = TavernBrawlManager.Get().NumSessionsAvailableForPurchase;
    if (availableForPurchase != newLimit - lastSeenLimit)
      return;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_responseDisplay = AlertPopup.ResponseDisplay.OK,
      m_alertTextAlignment = UberText.AlignmentOptions.Center,
      m_headerText = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_LIMIT_ALERT_TITLE"),
      m_text = GameStrings.Format("GLUE_HEROIC_BRAWL_SESSION_LIMIT_ALERT_LIMIT_RAISED", (object) availableForPurchase)
    });
  }

  private void Update()
  {
    Network.Get().ProcessNetwork();
  }

  public override void Unload()
  {
    StoreManager.Get().RemoveSuccessfulPurchaseListener(new StoreManager.SuccessfulPurchaseCallback(this.OnAdventureBundlePurchase));
    this.HideTooltipNotification((UIEvent) null);
    Box box = Box.Get();
    box.m_QuestLogButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.HideTooltipNotification));
    box.m_StoreButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.HideTooltipNotification));
    box.RemoveButtonPressListener(new Box.ButtonPressCallback(this.OnBoxButtonPressed));
  }

  private void OnSceneUnloaded(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    SpecialEventType activeEventType = SpecialEventVisualMgr.GetActiveEventType();
    if (activeEventType != SpecialEventType.IGNORE)
      SpecialEventVisualMgr.Get().UnloadEvent(activeEventType);
    SceneMgr.Get().UnregisterSceneUnloadedEvent(new SceneMgr.SceneUnloadedCallback(this.OnSceneUnloaded));
  }

  private void OnBoxButtonPressed(Box.ButtonType buttonType, object userData)
  {
    if (buttonType == Box.ButtonType.TOURNAMENT)
    {
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.TOURNAMENT);
      Tournament.Get().NotifyOfBoxTransitionStart();
    }
    else if (buttonType == Box.ButtonType.FORGE)
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.DRAFT);
    else if (buttonType == Box.ButtonType.ADVENTURE)
    {
      AdventureConfig.Get().ResetSubScene();
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.ADVENTURE);
    }
    else if (buttonType == Box.ButtonType.COLLECTION)
    {
      CollectionManager.Get().NotifyOfBoxTransitionStart();
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.COLLECTIONMANAGER);
    }
    else if (buttonType == Box.ButtonType.OPEN_PACKS)
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.PACKOPENING);
    else if (buttonType == Box.ButtonType.TAVERN_BRAWL)
    {
      this.HandleTavernBrawlButtonPressed();
    }
    else
    {
      if (buttonType != Box.ButtonType.SET_ROTATION)
        return;
      Log.Kyle.Print("OnBoxButtonPressed  Box.ButtonType.SET_ROTATION");
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.TOURNAMENT);
      Tournament.Get().NotifyOfBoxTransitionStart();
    }
  }

  private void HandleTavernBrawlButtonPressed()
  {
    if (TavernBrawlManager.Get().IsCurrentTavernBrawlSeasonClosedToPlayer)
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_HEROIC_BRAWL_SIGNUPS_CLOSED_TITLE"),
        m_text = GameStrings.Get("GLUE_HEROIC_BRAWL_SIGNUPS_CLOSED"),
        m_alertTextAlignmentAnchor = UberText.AnchorOptions.Middle,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK
      });
    else if (TavernBrawlManager.Get().IsPlayerAtSessionMaxForCurrentTavernBrawl)
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLOBAL_HEROIC_BRAWL"),
        m_text = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_LIMIT_ALERT_LIMIT_HIT"),
        m_alertTextAlignmentAnchor = UberText.AnchorOptions.Middle,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK
      });
    else
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.TAVERN_BRAWL);
  }

  [DebuggerHidden]
  private IEnumerator DoFirstTimeHubWelcome()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Hub.\u003CDoFirstTimeHubWelcome\u003Ec__IteratorF0() { \u003C\u003Ef__this = this };
  }

  private void OnAdventureBundlePurchase(Network.Bundle bundle, PaymentMethod purchaseMethod, object userData)
  {
    if (bundle == null || bundle.Items == null)
      return;
    using (List<Network.BundleItem>.Enumerator enumerator = bundle.Items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Product == ProductType.PRODUCT_TYPE_NAXX)
        {
          Options.Get().SetBool(Option.BUNDLE_JUST_PURCHASE_IN_HUB, true);
          AdventureConfig.Get().SetSelectedAdventureMode(AdventureDbId.NAXXRAMAS, AdventureModeDbId.NORMAL);
          break;
        }
      }
    }
  }

  private void HideTooltipNotification(UIEvent e)
  {
    if (!((UnityEngine.Object) this.m_PracticeNotification != (UnityEngine.Object) null))
      return;
    NotificationManager.Get().DestroyNotification(this.m_PracticeNotification, 0.0f);
  }
}
