﻿// Decompiled with JetBrains decompiler
// Type: CardTextHistoryData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class CardTextHistoryData
{
  public int m_damageBonus;
  public int m_damageBonusDouble;
  public int m_healingDouble;

  public virtual void SetHistoryData(Entity entity, HistoryInfo historyInfo)
  {
    this.m_damageBonus = entity.GetDamageBonus();
    this.m_damageBonusDouble = entity.GetDamageBonusDouble();
    this.m_healingDouble = entity.GetHealingDouble();
  }
}
