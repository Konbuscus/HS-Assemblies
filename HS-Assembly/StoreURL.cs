﻿// Decompiled with JetBrains decompiler
// Type: StoreURL
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using UnityEngine;

public class StoreURL
{
  private string m_format;
  private StoreURL.Param m_param1;
  private StoreURL.Param m_param2;

  public StoreURL(string format, StoreURL.Param param1, StoreURL.Param param2)
  {
    this.m_format = format;
    this.m_param1 = param1;
    this.m_param2 = param2;
  }

  public string GetURL()
  {
    return string.Format(this.m_format, (object) this.GetParamString(this.m_param1), (object) this.GetParamString(this.m_param2));
  }

  private string GetParamString(StoreURL.Param paramType)
  {
    switch (paramType)
    {
      case StoreURL.Param.LOCALE:
        return Localization.GetLocale().ToString();
      case StoreURL.Param.REGION:
        constants.BnetRegion accountRegion = BattleNet.GetAccountRegion();
        string str = NydusLink.RegionToStr(accountRegion);
        if (str != null)
          return str;
        Debug.LogError((object) string.Format("StoreURL unrecognized region {0}", (object) accountRegion));
        return NydusLink.RegionToStr(constants.BnetRegion.REGION_US);
      default:
        return string.Empty;
    }
  }

  public enum Param
  {
    NONE,
    LOCALE,
    REGION,
  }
}
