﻿// Decompiled with JetBrains decompiler
// Type: DeckTemplatePhoneTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DeckTemplatePhoneTray : MonoBehaviour
{
  public DeckTrayCardListContent m_cardsContent;
  public UIBScrollable m_scrollbar;
  public TooltipZone m_deckHeaderTooltip;
  public DeckBigCard m_deckBigCard;
  public UberText m_countLabelText;
  public UberText m_countText;
  public GameObject m_headerLabel;
  public PlayMakerFSM m_deckTemplateChosenGlow;
  private static DeckTemplatePhoneTray s_instance;

  private void Awake()
  {
    DeckTemplatePhoneTray.s_instance = this;
    if ((Object) this.m_scrollbar != (Object) null)
    {
      this.m_scrollbar.Enable(false);
      this.m_scrollbar.AddTouchScrollStartedListener(new UIBScrollable.OnTouchScrollStarted(this.OnTouchScrollStarted));
    }
    this.m_cardsContent.RegisterCardTilePressListener(new DeckTrayCardListContent.CardTilePress(this.OnCardTilePress));
    this.m_cardsContent.RegisterCardTileOverListener(new DeckTrayCardListContent.CardTileOver(this.OnCardTileOver));
    this.m_cardsContent.RegisterCardTileOutListener(new DeckTrayCardListContent.CardTileOut(this.OnCardTileOut));
    this.m_cardsContent.RegisterCardTileReleaseListener(new DeckTrayCardListContent.CardTileRelease(this.OnCardTileRelease));
    this.m_cardsContent.ShowFakeDeck(true);
  }

  private void OnDestroy()
  {
    DeckTemplatePhoneTray.s_instance = (DeckTemplatePhoneTray) null;
  }

  public static DeckTemplatePhoneTray Get()
  {
    return DeckTemplatePhoneTray.s_instance;
  }

  public bool MouseIsOver()
  {
    return UniversalInputManager.Get().InputIsOver(this.gameObject);
  }

  public DeckTrayCardListContent GetCardsContent()
  {
    return this.m_cardsContent;
  }

  public TooltipZone GetTooltipZone()
  {
    return this.m_deckHeaderTooltip;
  }

  private void OnCardCountUpdated(int cardCount)
  {
    string empty = string.Empty;
    string count = string.Empty;
    if (cardCount > 0)
    {
      if ((Object) this.m_headerLabel != (Object) null)
        this.m_headerLabel.SetActive(true);
      if (cardCount < CollectionManager.Get().GetDeckSize())
      {
        empty = GameStrings.Get("GLUE_DECK_TRAY_CARD_COUNT_LABEL");
        count = GameStrings.Format("GLUE_DECK_TRAY_COUNT", (object) cardCount, (object) CollectionManager.Get().GetDeckSize());
      }
    }
    this.m_countLabelText.Text = empty;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.StartCoroutine(this.DelayCardCountUpdate(count));
    else
      this.m_countText.Text = count;
  }

  [DebuggerHidden]
  private IEnumerator DelayCardCountUpdate(string count)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTemplatePhoneTray.\u003CDelayCardCountUpdate\u003Ec__Iterator81()
    {
      count = count,
      \u003C\u0024\u003Ecount = count,
      \u003C\u003Ef__this = this
    };
  }

  private void ShowDeckBigCard(DeckTrayDeckTileVisual cardTile, float delay = 0)
  {
    CollectionDeckTileActor actor = cardTile.GetActor();
    if ((Object) this.m_deckBigCard == (Object) null)
      return;
    EntityDef entityDef = actor.GetEntityDef();
    CardDef cardDef = DefLoader.Get().GetCardDef(entityDef.GetCardId(), new CardPortraitQuality(3, actor.GetPremium()));
    GhostCard.Type ghostTypeFromSlot = GhostCard.GetGhostTypeFromSlot(this.m_cardsContent.GetEditingDeck(), cardTile.GetSlot());
    this.m_deckBigCard.Show(entityDef, actor.GetPremium(), cardDef, actor.gameObject.transform.position, ghostTypeFromSlot, delay);
    if (!UniversalInputManager.Get().IsTouchMode())
      return;
    cardTile.SetHighlight(true);
  }

  private void HideDeckBigCard(DeckTrayDeckTileVisual cardTile, bool force = false)
  {
    CollectionDeckTileActor actor = cardTile.GetActor();
    if (!((Object) this.m_deckBigCard != (Object) null))
      return;
    if (force)
      this.m_deckBigCard.ForceHide();
    else
      this.m_deckBigCard.Hide(actor.GetEntityDef(), actor.GetPremium());
    if (!UniversalInputManager.Get().IsTouchMode())
      return;
    cardTile.SetHighlight(false);
  }

  private void OnTouchScrollStarted()
  {
    if (!((Object) this.m_deckBigCard != (Object) null))
      return;
    this.m_deckBigCard.ForceHide();
  }

  private void OnCardTilePress(DeckTrayDeckTileVisual cardTile)
  {
    if (UniversalInputManager.Get().IsTouchMode())
    {
      this.ShowDeckBigCard(cardTile, 0.2f);
    }
    else
    {
      if (!((Object) CollectionInputMgr.Get() != (Object) null))
        return;
      this.HideDeckBigCard(cardTile, false);
    }
  }

  private void OnCardTileOver(DeckTrayDeckTileVisual cardTile)
  {
    if (UniversalInputManager.Get().IsTouchMode())
      return;
    this.ShowDeckBigCard(cardTile, 0.0f);
  }

  private void OnCardTileOut(DeckTrayDeckTileVisual cardTile)
  {
    this.HideDeckBigCard(cardTile, false);
  }

  private void OnCardTileRelease(DeckTrayDeckTileVisual cardTile)
  {
    if (!UniversalInputManager.Get().IsTouchMode())
      return;
    this.HideDeckBigCard(cardTile, false);
  }

  public void FlashDeckTemplateHighlight()
  {
    if (!((Object) this.m_deckTemplateChosenGlow != (Object) null))
      return;
    this.m_deckTemplateChosenGlow.SendEvent("Flash");
  }
}
