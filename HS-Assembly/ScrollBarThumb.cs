﻿// Decompiled with JetBrains decompiler
// Type: ScrollBarThumb
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class ScrollBarThumb : PegUIElement
{
  private bool m_isDragging;

  private void Update()
  {
    if (!this.IsDragging() || !UniversalInputManager.Get().GetMouseButtonUp(0))
      return;
    this.StopDragging();
  }

  public bool IsDragging()
  {
    return this.m_isDragging;
  }

  public void StartDragging()
  {
    this.m_isDragging = true;
  }

  public void StopDragging()
  {
    this.m_isDragging = false;
  }

  protected override void OnHold()
  {
    this.StartDragging();
  }
}
