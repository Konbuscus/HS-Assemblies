﻿// Decompiled with JetBrains decompiler
// Type: Tutorial_06
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Tutorial_06 : TutorialEntity
{
  private Notification endTurnNotifier;
  private bool victory;
  private bool m_choSpeaking;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_TUTORIAL_06_CHO_15_15");
    this.PreloadSound("VO_TUTORIAL_06_CHO_09_13");
    this.PreloadSound("VO_TUTORIAL_06_CHO_17_16");
    this.PreloadSound("VO_TUTORIAL_06_CHO_05_09");
    this.PreloadSound("VO_TUTORIAL_06_JAINA_03_51");
    this.PreloadSound("VO_TUTORIAL_06_CHO_06_10");
    this.PreloadSound("VO_TUTORIAL_06_CHO_21_18");
    this.PreloadSound("VO_TUTORIAL_06_CHO_20_17");
    this.PreloadSound("VO_TUTORIAL_06_CHO_07_11");
    this.PreloadSound("VO_TUTORIAL_06_JAINA_04_52");
    this.PreloadSound("VO_TUTORIAL_06_CHO_04_08");
    this.PreloadSound("VO_TUTORIAL_06_CHO_12_14");
    this.PreloadSound("VO_TUTORIAL_06_CHO_01_05");
    this.PreloadSound("VO_TUTORIAL_06_JAINA_01_49");
    this.PreloadSound("VO_TUTORIAL_06_CHO_02_06");
    this.PreloadSound("VO_TUTORIAL_06_JAINA_02_50");
    this.PreloadSound("VO_TUTORIAL_06_CHO_03_07");
    this.PreloadSound("VO_TUTORIAL_06_CHO_22_19");
    this.PreloadSound("VO_TUTORIAL_06_JAINA_05_53");
  }

  public override void NotifyOfGameOver(TAG_PLAYSTATE gameResult)
  {
    if (gameResult == TAG_PLAYSTATE.WON)
      this.victory = true;
    base.NotifyOfGameOver(gameResult);
    if (gameResult == TAG_PLAYSTATE.WON)
    {
      this.SetTutorialProgress(TutorialProgress.CHO_COMPLETE);
      this.PlaySound("VO_TUTORIAL_06_CHO_22_19", 1f, true, false);
    }
    else if (gameResult == TAG_PLAYSTATE.TIED)
    {
      this.PlaySound("VO_TUTORIAL_06_CHO_22_19", 1f, true, false);
    }
    else
    {
      if (gameResult != TAG_PLAYSTATE.LOST)
        return;
      this.SetTutorialLostProgress(TutorialProgress.CHO_COMPLETE);
    }
  }

  protected override Spell BlowUpHero(Card card, SpellType spellType)
  {
    if (card.GetEntity().GetCardId() != "TU4f_001")
      return base.BlowUpHero(card, spellType);
    Spell spell = card.ActivateActorSpell(SpellType.CHODEATH);
    Gameplay.Get().StartCoroutine(this.HideOtherElements(card));
    return spell;
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_06.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator221() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_06.\u003CHandleMissionEventWithTiming\u003Ec__Iterator222() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  private Card FindVoodooDoctorInOpposingSide()
  {
    using (List<Card>.Enumerator enumerator = GameState.Get().GetOpposingSidePlayer().GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (current.GetEntity().GetCardId() == "EX1_011")
          return current;
      }
    }
    return (Card) null;
  }

  [DebuggerHidden]
  private IEnumerator Wait(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_06.\u003CWait\u003Ec__Iterator223() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds };
  }

  public override float GetAdditionalTimeToWaitForSpells()
  {
    return 1.5f;
  }

  public override bool IsKeywordHelpDelayOverridden()
  {
    return true;
  }

  public override bool NotifyOfEndTurnButtonPushed()
  {
    Network.Options optionsPacket = GameState.Get().GetOptionsPacket();
    if (optionsPacket == null || optionsPacket.List == null || optionsPacket.List.Count == 1)
      return true;
    for (int index = 0; index < optionsPacket.List.Count; ++index)
    {
      Network.Options.Option option = optionsPacket.List[index];
      if (option.Type == Network.Options.Option.OptionType.POWER && GameState.Get().GetEntity(option.Main.ID).GetZone() == TAG_ZONE.PLAY)
      {
        if ((Object) this.endTurnNotifier != (Object) null)
          NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.endTurnNotifier);
        Vector3 position = EndTurnButton.Get().transform.position;
        this.endTurnNotifier = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(position.x - 3f, position.y, position.z), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("TUTORIAL_NO_ENDTURN_ATK"), true);
        NotificationManager.Get().DestroyNotification(this.endTurnNotifier, 2.5f);
        return false;
      }
    }
    return true;
  }

  public override void NotifyOfDefeatCoinAnimation()
  {
    if (!this.victory)
      return;
    this.PlaySound("VO_TUTORIAL_06_JAINA_05_53", 1f, true, false);
  }

  public override List<RewardData> GetCustomRewards()
  {
    if (!this.victory)
      return (List<RewardData>) null;
    List<RewardData> rewardDataList = new List<RewardData>();
    CardRewardData cardRewardData = new CardRewardData("CS2_124", TAG_PREMIUM.NORMAL, 2);
    cardRewardData.MarkAsDummyReward();
    rewardDataList.Add((RewardData) cardRewardData);
    return rewardDataList;
  }
}
