﻿// Decompiled with JetBrains decompiler
// Type: FindGameState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum FindGameState
{
  INVALID,
  CLIENT_STARTED,
  CLIENT_CANCELED,
  CLIENT_ERROR,
  BNET_QUEUE_ENTERED,
  BNET_QUEUE_DELAYED,
  BNET_QUEUE_UPDATED,
  BNET_QUEUE_CANCELED,
  BNET_ERROR,
  SERVER_GAME_CONNECTING,
  SERVER_GAME_STARTED,
  SERVER_GAME_CANCELED,
}
