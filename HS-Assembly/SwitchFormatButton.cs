﻿// Decompiled with JetBrains decompiler
// Type: SwitchFormatButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class SwitchFormatButton : UIBButton
{
  private static readonly int TIMES_MOUSED_OVER_THRESHOLD = 5;
  private static readonly float SHOW_POP_UP_DELAY_SEC = 0.5f;
  private static readonly float WILD_FLIP_DELAY_SEC = 1.5f;
  public MeshRenderer m_buttonRenderer;
  public HighlightState m_highlight;
  public GameObject m_coverObject;
  public UIBHighlight m_uibHighlight;
  public GameObject m_popUpPrefab;
  public Transform m_popUpBone;
  public Transform m_popUpBonePhone;
  private bool m_isHighlightEnabled;
  private bool m_cachedHighlightEnabled;
  private bool m_isWild;
  private bool m_isRotating;
  private bool m_isCovered;
  private GameObject m_popUpObject;

  protected override void Awake()
  {
    base.Awake();
  }

  public void SetFormat(bool isWild, bool doAnimation)
  {
    if (this.m_isWild == isWild)
      return;
    this.m_isWild = isWild;
    GameObject gameObject = this.m_buttonRenderer.gameObject;
    iTween.Stop(gameObject);
    if (this.m_isRotating)
    {
      this.m_isRotating = false;
      this.EnableHighlightImpl(this.m_cachedHighlightEnabled);
    }
    if (doAnimation)
    {
      this.m_isRotating = true;
      this.m_cachedHighlightEnabled = this.m_isHighlightEnabled;
      this.EnableHighlightImpl(false);
      this.HidePopUp();
      Vector3 eulerAngles = gameObject.transform.localRotation.eulerAngles;
      eulerAngles.z = !this.m_isWild ? 180f : 0.0f;
      gameObject.transform.localRotation = Quaternion.Euler(eulerAngles);
      Hashtable args = iTween.Hash((object) "z", (object) -180, (object) "time", (object) 0.5f, (object) "oncomplete", (object) "OnRotateComplete", (object) "oncompletetarget", (object) this.gameObject);
      iTween.RotateAdd(gameObject, args);
    }
    else
    {
      Vector3 eulerAngles = gameObject.transform.rotation.eulerAngles;
      eulerAngles.z = !this.m_isWild ? 0.0f : 180f;
      gameObject.transform.rotation = Quaternion.Euler(eulerAngles);
    }
  }

  private void OnRotateComplete()
  {
    this.m_isRotating = false;
    this.EnableHighlightImpl(this.m_cachedHighlightEnabled);
  }

  public void Disable()
  {
    this.m_uibHighlight.Reset();
    this.HidePopUp();
    this.SetEnabled(false);
  }

  public void Enable()
  {
    this.SetEnabled(true);
  }

  public bool IsRotating()
  {
    return this.m_isRotating;
  }

  public void Cover()
  {
    this.m_isCovered = true;
    if (!((Object) this.m_coverObject != (Object) null))
      return;
    this.m_coverObject.SetActive(true);
  }

  public void Uncover()
  {
    this.m_isCovered = false;
    if (!((Object) this.m_coverObject != (Object) null))
      return;
    this.m_coverObject.SetActive(false);
  }

  public bool IsCovered()
  {
    return this.m_isCovered;
  }

  public void ShowPopUp()
  {
    if ((Object) this.m_popUpObject == (Object) null)
    {
      this.m_popUpObject = Object.Instantiate<GameObject>(this.m_popUpPrefab);
      Transform transform = !(bool) UniversalInputManager.UsePhoneUI ? this.m_popUpBone : this.m_popUpBonePhone;
      Vector3 relativePosition = OverlayUI.Get().GetRelativePosition(transform.position, Box.Get().m_Camera.GetComponent<Camera>(), OverlayUI.Get().m_heightScale.m_Center, 1f);
      this.m_popUpObject.transform.parent = OverlayUI.Get().m_heightScale.m_Center;
      this.m_popUpObject.transform.localPosition = relativePosition;
      this.m_popUpObject.transform.localScale = transform.localScale;
      this.m_popUpObject.SetActive(false);
    }
    int num = Options.Get().GetInt(Option.TIMES_MOUSED_OVER_SWITCH_FORMAT_BUTTON);
    if (num < SwitchFormatButton.TIMES_MOUSED_OVER_THRESHOLD)
    {
      this.m_popUpObject.SetActive(true);
      Options.Get().SetInt(Option.TIMES_MOUSED_OVER_SWITCH_FORMAT_BUTTON, num + 1);
    }
    else
      this.StartCoroutine("ShowPopUpAfterDelay");
  }

  [DebuggerHidden]
  private IEnumerator ShowPopUpAfterDelay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SwitchFormatButton.\u003CShowPopUpAfterDelay\u003Ec__Iterator7E()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void HidePopUp()
  {
    if (!((Object) this.m_popUpObject != (Object) null))
      return;
    this.StopCoroutine("ShowPopUpAfterDelay");
    this.m_popUpObject.SetActive(false);
  }

  public void EnableHighlight(bool enabled)
  {
    if (!this.m_isRotating)
      this.EnableHighlightImpl(enabled);
    else
      this.m_cachedHighlightEnabled = enabled;
  }

  public void DoWildFlip()
  {
    this.StopCoroutine("DoWildFlipImpl");
    this.StartCoroutine("DoWildFlipImpl");
  }

  [DebuggerHidden]
  private IEnumerator DoWildFlipImpl()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SwitchFormatButton.\u003CDoWildFlipImpl\u003Ec__Iterator7F()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void EnableHighlightImpl(bool enabled)
  {
    this.m_isHighlightEnabled = enabled;
    if (enabled)
      this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    else
      this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }
}
