﻿// Decompiled with JetBrains decompiler
// Type: LOE04_Scarvash
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LOE04_Scarvash : LOE_MissionEntity
{
  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOEA04_BRANN_TURN_1");
    this.PreloadSound("VO_LOE_04_SCARVASH_TURN_2");
    this.PreloadSound("VO_BRANN_MITHRIL_ALT_02");
    this.PreloadSound("VO_LOE_SCARVASH_TURN_6_CARTOGRAPHER");
    this.PreloadSound("VO_LOE_04_RESPONSE");
    this.PreloadSound("VO_LOE_04_WIN");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_LOE_04_RESPONSE",
            m_stringTag = "VO_LOE_04_RESPONSE"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE04_Scarvash.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator166() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE04_Scarvash.\u003CHandleGameOverWithTiming\u003Ec__Iterator167() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
