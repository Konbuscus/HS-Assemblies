﻿// Decompiled with JetBrains decompiler
// Type: RemoteActionHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteActionHandler : MonoBehaviour
{
  private RemoteActionHandler.UserUI myCurrentUI = new RemoteActionHandler.UserUI();
  private RemoteActionHandler.UserUI myLastUI = new RemoteActionHandler.UserUI();
  private RemoteActionHandler.UserUI enemyWantedUI = new RemoteActionHandler.UserUI();
  private RemoteActionHandler.UserUI enemyActualUI = new RemoteActionHandler.UserUI();
  private RemoteActionHandler.UserUI friendlyWantedUI = new RemoteActionHandler.UserUI();
  private RemoteActionHandler.UserUI friendlyActualUI = new RemoteActionHandler.UserUI();
  private float m_lastSendTime = Time.realtimeSinceStartup;
  public const string TWEEN_NAME = "RemoteActionHandler";
  private const float DRIFT_TIME = 10f;
  private const float LOW_FREQ_SEND_TIME = 0.35f;
  private const float HIGH_FREQ_SEND_TIME = 0.25f;
  private static RemoteActionHandler s_instance;

  private void Awake()
  {
    RemoteActionHandler.s_instance = this;
    GameState.Get().RegisterTurnChangedListener(new GameState.TurnChangedCallback(this.OnTurnChanged));
  }

  private void OnDestroy()
  {
    RemoteActionHandler.s_instance = (RemoteActionHandler) null;
  }

  private void Update()
  {
    if ((UnityEngine.Object) TargetReticleManager.Get() != (UnityEngine.Object) null)
      TargetReticleManager.Get().UpdateArrowPosition();
    if (this.myCurrentUI.SameAs(this.myLastUI) || !this.CanSendUI())
      return;
    Network.Get().SendUserUI(this.myCurrentUI.over.ID, this.myCurrentUI.held.ID, this.myCurrentUI.origin.ID, 0, 0);
    this.myLastUI.CopyFrom(this.myCurrentUI);
  }

  public static RemoteActionHandler Get()
  {
    return RemoteActionHandler.s_instance;
  }

  public Card GetOpponentHeldCard()
  {
    return this.enemyActualUI.held.card;
  }

  public Card GetFriendlyHoverCard()
  {
    return this.friendlyActualUI.over.card;
  }

  public Card GetFriendlyHeldCard()
  {
    return this.friendlyActualUI.held.card;
  }

  public void NotifyOpponentOfMouseOverEntity(Card card)
  {
    this.myCurrentUI.over.card = card;
  }

  public void NotifyOpponentOfMouseOut()
  {
    this.myCurrentUI.over.card = (Card) null;
  }

  public void NotifyOpponentOfTargetModeBegin(Card card)
  {
    this.myCurrentUI.origin.card = card;
  }

  public void NotifyOpponentOfTargetEnd()
  {
    this.myCurrentUI.origin.card = (Card) null;
  }

  public void NotifyOpponentOfCardPickedUp(Card card)
  {
    this.myCurrentUI.held.card = card;
  }

  public void NotifyOpponentOfCardDropped()
  {
    this.myCurrentUI.held.card = (Card) null;
  }

  public void HandleAction(Network.UserUI newData)
  {
    bool flag = false;
    if (newData.playerId.HasValue)
    {
      Player friendlySidePlayer = GameState.Get().GetFriendlySidePlayer();
      flag = friendlySidePlayer != null && friendlySidePlayer.GetPlayerId() == newData.playerId.Value;
    }
    if (newData.mouseInfo != null)
    {
      if (flag)
      {
        this.friendlyWantedUI.held.ID = newData.mouseInfo.HeldCardID;
        this.friendlyWantedUI.over.ID = newData.mouseInfo.OverCardID;
        this.friendlyWantedUI.origin.ID = newData.mouseInfo.ArrowOriginID;
      }
      else
      {
        this.enemyWantedUI.held.ID = newData.mouseInfo.HeldCardID;
        this.enemyWantedUI.over.ID = newData.mouseInfo.OverCardID;
        this.enemyWantedUI.origin.ID = newData.mouseInfo.ArrowOriginID;
      }
      this.UpdateCardOver();
      this.UpdateCardHeld();
      this.MaybeDestroyArrow();
      this.MaybeCreateArrow();
      this.UpdateTargetArrow();
    }
    else
    {
      if (newData.emoteInfo == null)
        return;
      EmoteType emote = (EmoteType) newData.emoteInfo.Emote;
      if (flag)
      {
        GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(emote);
      }
      else
      {
        if (!this.CanReceiveEnemyEmote(emote))
          return;
        GameState.Get().GetOpposingSidePlayer().GetHeroCard().PlayEmote(emote);
      }
    }
  }

  private bool CanSendUI()
  {
    if (GameMgr.Get() == null || GameMgr.Get().IsSpectator() || GameMgr.Get().IsAI() && !SpectatorManager.Get().MyGameHasSpectators() && SpectatorManager.Get().MyGameHasSpectators())
      return false;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    float num = realtimeSinceStartup - this.m_lastSendTime;
    if (this.IsSendingTargetingArrow() && (double) num > 0.25)
    {
      this.m_lastSendTime = realtimeSinceStartup;
      return true;
    }
    if ((double) num < 0.349999994039536)
      return false;
    this.m_lastSendTime = realtimeSinceStartup;
    return true;
  }

  private bool IsSendingTargetingArrow()
  {
    return !((UnityEngine.Object) this.myCurrentUI.origin.card == (UnityEngine.Object) null) && !((UnityEngine.Object) this.myCurrentUI.over.card == (UnityEngine.Object) null) && !((UnityEngine.Object) this.myCurrentUI.over.card == (UnityEngine.Object) this.myCurrentUI.origin.card) && ((UnityEngine.Object) this.myCurrentUI.origin.card != (UnityEngine.Object) this.myLastUI.origin.card || (UnityEngine.Object) this.myCurrentUI.over.card != (UnityEngine.Object) this.myLastUI.over.card);
  }

  private int GetOpponentHandHoverSlot()
  {
    Entity entity = this.enemyActualUI.over.entity;
    if (entity == null || entity.GetZone() != TAG_ZONE.HAND || entity.GetController().IsFriendlySide())
      return -1;
    return entity.GetTag(GAME_TAG.ZONE_POSITION) - 1;
  }

  private void UpdateCardOver()
  {
    Card card1 = this.enemyActualUI.over.card;
    Card card2 = this.enemyWantedUI.over.card;
    if ((UnityEngine.Object) card1 != (UnityEngine.Object) card2)
    {
      this.enemyActualUI.over.card = card2;
      if ((UnityEngine.Object) card1 != (UnityEngine.Object) null)
        card1.NotifyOpponentMousedOffThisCard();
      if ((UnityEngine.Object) card2 != (UnityEngine.Object) null)
        card2.NotifyOpponentMousedOverThisCard();
      ZoneMgr.Get().FindZoneOfType<ZoneHand>(Player.Side.OPPOSING).UpdateLayout(this.GetOpponentHandHoverSlot());
    }
    if (!GameMgr.Get().IsSpectator())
      return;
    Card card3 = this.friendlyActualUI.over.card;
    Card card4 = this.friendlyWantedUI.over.card;
    if (!((UnityEngine.Object) card3 != (UnityEngine.Object) card4))
      return;
    this.friendlyActualUI.over.card = card4;
    if ((UnityEngine.Object) card3 != (UnityEngine.Object) null)
    {
      ZoneHand zone = card3.GetZone() as ZoneHand;
      if ((UnityEngine.Object) zone != (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) zone.CurrentStandIn == (UnityEngine.Object) null)
          zone.UpdateLayout(-1);
      }
      else
        card3.NotifyMousedOut();
    }
    if (!((UnityEngine.Object) card4 != (UnityEngine.Object) null))
      return;
    ZoneHand zone1 = card4.GetZone() as ZoneHand;
    if ((UnityEngine.Object) zone1 != (UnityEngine.Object) null)
    {
      if (!((UnityEngine.Object) zone1.CurrentStandIn == (UnityEngine.Object) null))
        return;
      int cardPos = zone1.FindCardPos(card4);
      if (cardPos < 1)
        return;
      zone1.UpdateLayout(cardPos - 1);
    }
    else
      card4.NotifyMousedOver();
  }

  private void UpdateCardHeld()
  {
    Card card1 = this.enemyActualUI.held.card;
    Card card2 = this.enemyWantedUI.held.card;
    if ((UnityEngine.Object) card1 != (UnityEngine.Object) card2)
    {
      this.enemyActualUI.held.card = card2;
      if ((UnityEngine.Object) card1 != (UnityEngine.Object) null)
        card1.MarkAsGrabbedByEnemyActionHandler(false);
      if (this.IsCardInHand(card1))
        card1.GetZone().UpdateLayout();
      if (this.CanAnimateHeldCard(card2))
      {
        card2.MarkAsGrabbedByEnemyActionHandler(true);
        if (SpectatorManager.Get().IsSpectatingOpposingSide())
          this.StandUpright(false);
        Hashtable args = iTween.Hash((object) "name", (object) "RemoteActionHandler", (object) "position", (object) Board.Get().FindBone("OpponentCardPlayingSpot").position, (object) "time", (object) 1f, (object) "oncomplete", (object) (Action<object>) (o => this.StartDrift(false)), (object) "oncompletetarget", (object) this.gameObject);
        iTween.MoveTo(card2.gameObject, args);
      }
    }
    if (!GameMgr.Get().IsSpectator())
      return;
    Card card3 = this.friendlyActualUI.held.card;
    Card card4 = this.friendlyWantedUI.held.card;
    if (!((UnityEngine.Object) card3 != (UnityEngine.Object) card4))
      return;
    this.friendlyActualUI.held.card = card4;
    if ((UnityEngine.Object) card3 != (UnityEngine.Object) null)
      card3.MarkAsGrabbedByEnemyActionHandler(false);
    if (this.IsCardInHand(card3))
      card3.GetZone().UpdateLayout();
    if (!this.CanAnimateHeldCard(card4))
      return;
    card4.MarkAsGrabbedByEnemyActionHandler(true);
    ZoneHand zone = card4.GetZone() as ZoneHand;
    if ((UnityEngine.Object) zone != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) zone.CurrentStandIn == (UnityEngine.Object) null || (UnityEngine.Object) zone.CurrentStandIn.linkedCard == (UnityEngine.Object) card4)
        card4.NotifyMousedOut();
      Hashtable args = iTween.Hash((object) "scale", (object) zone.GetCardScale(card4), (object) "time", (object) 0.15f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "name", (object) "RemoteActionHandler");
      iTween.ScaleTo(card4.gameObject, args);
    }
    Hashtable args1 = iTween.Hash((object) "name", (object) "RemoteActionHandler", (object) "position", (object) Board.Get().FindBone("FriendlyCardPlayingSpot").position, (object) "time", (object) 1f, (object) "oncomplete", (object) (Action<object>) (o => this.StartDrift(true)), (object) "oncompletetarget", (object) this.gameObject);
    iTween.MoveTo(card4.gameObject, args1);
    SceneUtils.SetLayer((Component) card4, GameLayer.Default);
  }

  private void StartDrift(bool isFriendlySide)
  {
    if (isFriendlySide || !SpectatorManager.Get().IsSpectatingOpposingSide())
      this.StandUpright(isFriendlySide);
    this.DriftLeftAndRight(isFriendlySide);
  }

  private void DriftLeftAndRight(bool isFriendlySide)
  {
    Card card = !isFriendlySide ? this.enemyActualUI.held.card : this.friendlyActualUI.held.card;
    if (!this.CanAnimateHeldCard(card))
      return;
    Vector3[] vector3Array;
    if (isFriendlySide)
    {
      iTweenPath iTweenPath;
      if (!iTweenPath.paths.TryGetValue(iTweenPath.FixupPathName("driftPath1_friendly"), out iTweenPath))
      {
        Transform bone1 = Board.Get().FindBone("OpponentCardPlayingSpot");
        Transform bone2 = Board.Get().FindBone("FriendlyCardPlayingSpot");
        Vector3 vector3 = bone2.position - bone1.position;
        iTweenPath path = iTweenPath.paths[iTweenPath.FixupPathName("driftPath1")];
        iTweenPath = bone2.gameObject.AddComponent<iTweenPath>();
        iTweenPath.pathVisible = true;
        iTweenPath.pathName = "driftPath1_friendly";
        iTweenPath.pathColor = path.pathColor;
        iTweenPath.nodes = new List<Vector3>((IEnumerable<Vector3>) path.nodes);
        for (int index = 0; index < iTweenPath.nodes.Count; ++index)
          iTweenPath.nodes[index] = path.nodes[index] + vector3;
        iTweenPath.enabled = false;
        iTweenPath.enabled = true;
      }
      vector3Array = iTweenPath.nodes.ToArray();
    }
    else
      vector3Array = iTweenPath.GetPath("driftPath1");
    Hashtable args = iTween.Hash((object) "name", (object) "RemoteActionHandler", (object) "path", (object) vector3Array, (object) "time", (object) 10f, (object) "easetype", (object) iTween.EaseType.linear, (object) "looptype", (object) iTween.LoopType.pingPong);
    iTween.MoveTo(card.gameObject, args);
  }

  private void StandUpright(bool isFriendlySide)
  {
    Card card = !isFriendlySide ? this.enemyActualUI.held.card : this.friendlyActualUI.held.card;
    if (!this.CanAnimateHeldCard(card))
      return;
    float num = 5f;
    if (!isFriendlySide && SpectatorManager.Get().IsSpectatingOpposingSide())
      num = 0.3f;
    Hashtable args = iTween.Hash((object) "name", (object) "RemoteActionHandler", (object) "rotation", (object) Vector3.zero, (object) "time", (object) num, (object) "easetype", (object) iTween.EaseType.easeInOutSine);
    iTween.RotateTo(card.gameObject, args);
  }

  private void MaybeDestroyArrow()
  {
    if ((UnityEngine.Object) TargetReticleManager.Get() == (UnityEngine.Object) null || !TargetReticleManager.Get().IsActive())
      return;
    bool flag = GameState.Get() != null && GameState.Get().IsFriendlySidePlayerTurn();
    RemoteActionHandler.UserUI userUi1 = !flag ? this.enemyWantedUI : this.friendlyWantedUI;
    RemoteActionHandler.UserUI userUi2 = !flag ? this.enemyActualUI : this.friendlyActualUI;
    if ((UnityEngine.Object) userUi1.origin.card == (UnityEngine.Object) userUi2.origin.card)
      return;
    if (userUi2.origin.entity.HasTag(GAME_TAG.IMMUNE_WHILE_ATTACKING) && !userUi2.origin.card.ShouldShowImmuneVisuals())
      userUi2.origin.card.GetActor().ActivateSpellDeathState(SpellType.IMMUNE);
    userUi2.origin.card = (Card) null;
    if (flag)
      TargetReticleManager.Get().DestroyFriendlyTargetArrow(false);
    else
      TargetReticleManager.Get().DestroyEnemyTargetArrow();
  }

  private void MaybeCreateArrow()
  {
    if ((UnityEngine.Object) TargetReticleManager.Get() == (UnityEngine.Object) null || TargetReticleManager.Get().IsActive())
      return;
    bool flag = GameState.Get() != null && GameState.Get().IsFriendlySidePlayerTurn();
    RemoteActionHandler.UserUI userUi1 = !flag ? this.enemyWantedUI : this.friendlyWantedUI;
    RemoteActionHandler.UserUI userUi2 = !flag ? this.enemyActualUI : this.friendlyActualUI;
    if ((UnityEngine.Object) userUi1.origin.card == (UnityEngine.Object) null || (UnityEngine.Object) userUi2.over.card == (UnityEngine.Object) null || ((UnityEngine.Object) userUi2.over.card.GetActor() == (UnityEngine.Object) null || !userUi2.over.card.GetActor().IsShown()) || (UnityEngine.Object) userUi2.over.card == (UnityEngine.Object) userUi1.origin.card)
      return;
    Player currentPlayer = GameState.Get().GetCurrentPlayer();
    if (currentPlayer == null || currentPlayer.IsLocalUser())
      return;
    userUi2.origin.card = userUi1.origin.card;
    if (flag)
      TargetReticleManager.Get().CreateFriendlyTargetArrow(userUi2.origin.entity, userUi2.origin.entity, false, true, (string) null, false);
    else
      TargetReticleManager.Get().CreateEnemyTargetArrow(userUi2.origin.entity);
    if (userUi2.origin.entity.HasTag(GAME_TAG.IMMUNE_WHILE_ATTACKING))
      userUi2.origin.card.ActivateActorSpell(SpellType.IMMUNE);
    this.SetArrowTarget();
  }

  private void UpdateTargetArrow()
  {
    if ((UnityEngine.Object) TargetReticleManager.Get() == (UnityEngine.Object) null || !TargetReticleManager.Get().IsActive())
      return;
    this.SetArrowTarget();
  }

  private void SetArrowTarget()
  {
    bool flag = GameState.Get() != null && GameState.Get().IsFriendlySidePlayerTurn();
    RemoteActionHandler.UserUI userUi1 = !flag ? this.enemyWantedUI : this.friendlyWantedUI;
    RemoteActionHandler.UserUI userUi2 = !flag ? this.enemyActualUI : this.friendlyActualUI;
    if ((UnityEngine.Object) userUi2.over.card == (UnityEngine.Object) null || (UnityEngine.Object) userUi2.over.card.GetActor() == (UnityEngine.Object) null || (!userUi2.over.card.GetActor().IsShown() || (UnityEngine.Object) userUi2.over.card == (UnityEngine.Object) userUi1.origin.card))
      return;
    Vector3 position1 = Camera.main.transform.position;
    Vector3 position2 = userUi2.over.card.transform.position;
    RaycastHit hitInfo;
    if (!Physics.Raycast(new Ray(position1, position2 - position1), out hitInfo, Camera.main.farClipPlane, GameLayer.DragPlane.LayerBit()))
      return;
    TargetReticleManager.Get().SetRemotePlayerArrowPosition(hitInfo.point);
  }

  private bool IsCardInHand(Card card)
  {
    return !((UnityEngine.Object) card == (UnityEngine.Object) null) && card.GetZone() is ZoneHand && card.GetEntity().GetZone() == TAG_ZONE.HAND;
  }

  private bool CanAnimateHeldCard(Card card)
  {
    if (!this.IsCardInHand(card))
      return false;
    string tweenName = ZoneMgr.Get().GetTweenName<ZoneHand>();
    return !iTween.HasNameNotInList(card.gameObject, "RemoteActionHandler", tweenName);
  }

  private void OnTurnChanged(int oldTurn, int newTurn, object userData)
  {
    Player currentPlayer = GameState.Get().GetCurrentPlayer();
    if (currentPlayer != null && !currentPlayer.IsLocalUser() && !GameMgr.Get().IsSpectator() || (UnityEngine.Object) TargetReticleManager.Get() == (UnityEngine.Object) null)
      return;
    RemoteActionHandler.UserUI userUi;
    if (currentPlayer.IsFriendlySide())
    {
      userUi = this.friendlyActualUI;
      if (TargetReticleManager.Get().IsEnemyArrowActive())
        TargetReticleManager.Get().DestroyEnemyTargetArrow();
    }
    else
    {
      userUi = this.enemyActualUI;
      if (TargetReticleManager.Get().IsLocalArrowActive())
        TargetReticleManager.Get().DestroyFriendlyTargetArrow(false);
    }
    if (userUi.origin == null || userUi.origin.entity == null || (!((UnityEngine.Object) userUi.origin.card != (UnityEngine.Object) null) || !userUi.origin.entity.HasTag(GAME_TAG.IMMUNE_WHILE_ATTACKING)) || userUi.origin.card.ShouldShowImmuneVisuals())
      return;
    userUi.origin.card.GetActor().ActivateSpellDeathState(SpellType.IMMUNE);
  }

  private bool CanReceiveEnemyEmote(EmoteType emoteType)
  {
    if ((UnityEngine.Object) EnemyEmoteHandler.Get() == (UnityEngine.Object) null || EnemyEmoteHandler.Get().IsSquelched() || (UnityEngine.Object) EmoteHandler.Get() == (UnityEngine.Object) null)
      return false;
    List<EmoteOption> emotes = EmoteHandler.Get().m_Emotes;
    if (emotes == null)
      return false;
    for (int index = 0; index < emotes.Count; ++index)
    {
      if (emotes[index].m_EmoteType == emoteType)
        return true;
    }
    return false;
  }

  private class CardAndID
  {
    private int m_ID;
    private Entity m_entity;
    private Card m_card;

    public Card card
    {
      get
      {
        return this.m_card;
      }
      set
      {
        if ((UnityEngine.Object) value == (UnityEngine.Object) this.m_card)
          return;
        if ((UnityEngine.Object) value == (UnityEngine.Object) null)
        {
          this.Clear();
        }
        else
        {
          this.m_card = value;
          this.m_entity = value.GetEntity();
          if (this.m_entity == null)
          {
            Debug.LogWarning((object) "RemoteActionHandler--card has no entity");
            this.Clear();
          }
          else
          {
            this.m_ID = this.m_entity.GetEntityId();
            if (this.m_ID >= 1)
              return;
            Debug.LogWarning((object) "RemoteActionHandler--invalid entity ID");
            this.Clear();
          }
        }
      }
    }

    public int ID
    {
      get
      {
        return this.m_ID;
      }
      set
      {
        if (value == this.m_ID)
          return;
        if (value == 0)
        {
          this.Clear();
        }
        else
        {
          this.m_ID = value;
          this.m_entity = GameState.Get().GetEntity(value);
          if (this.m_entity == null)
          {
            Debug.LogWarning((object) "RemoteActionHandler--no entity found for ID");
            this.Clear();
          }
          else
          {
            this.m_card = this.m_entity.GetCard();
            if (!((UnityEngine.Object) this.m_card == (UnityEngine.Object) null))
              return;
            Debug.LogWarning((object) "RemoteActionHandler--entity has no card");
            this.Clear();
          }
        }
      }
    }

    public Entity entity
    {
      get
      {
        return this.m_entity;
      }
    }

    private void Clear()
    {
      this.m_ID = 0;
      this.m_entity = (Entity) null;
      this.m_card = (Card) null;
    }
  }

  private class UserUI
  {
    public RemoteActionHandler.CardAndID over = new RemoteActionHandler.CardAndID();
    public RemoteActionHandler.CardAndID held = new RemoteActionHandler.CardAndID();
    public RemoteActionHandler.CardAndID origin = new RemoteActionHandler.CardAndID();

    public bool SameAs(RemoteActionHandler.UserUI compare)
    {
      return !((UnityEngine.Object) this.held.card != (UnityEngine.Object) compare.held.card) && !((UnityEngine.Object) this.over.card != (UnityEngine.Object) compare.over.card) && !((UnityEngine.Object) this.origin.card != (UnityEngine.Object) compare.origin.card);
    }

    public void CopyFrom(RemoteActionHandler.UserUI source)
    {
      this.held.ID = source.held.ID;
      this.over.ID = source.over.ID;
      this.origin.ID = source.origin.ID;
    }
  }
}
