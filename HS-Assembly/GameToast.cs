﻿// Decompiled with JetBrains decompiler
// Type: GameToast
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GameToast : MonoBehaviour
{
  public List<Material> m_intensityMaterials = new List<Material>();

  private void Start()
  {
    this.UpdateIntensity(16f);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "time", (object) 0.5f, (object) "from", (object) 16f, (object) "to", (object) 1f, (object) "delay", (object) 0.25f, (object) "easetype", (object) iTween.EaseType.easeOutCubic, (object) "onupdate", (object) "UpdateIntensity"));
  }

  private void UpdateIntensity(float intensity)
  {
    using (List<Material>.Enumerator enumerator = this.m_intensityMaterials.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetFloat("_Intensity", intensity);
    }
  }
}
