﻿// Decompiled with JetBrains decompiler
// Type: NetCache
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs.types;
using BobNetProto;
using PegasusShared;
using PegasusUtil;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using WTCG.BI;

public class NetCache
{
  private static readonly Map<System.Type, GetAccountInfo.Request> m_getAccountInfoTypeMap = new Map<System.Type, GetAccountInfo.Request>() { { typeof (NetCache.NetCacheDecks), GetAccountInfo.Request.DECK_LIST }, { typeof (NetCache.NetCacheCollection), GetAccountInfo.Request.COLLECTION }, { typeof (NetCache.NetCacheMedalInfo), GetAccountInfo.Request.MEDAL_INFO }, { typeof (NetCache.NetCacheBoosters), GetAccountInfo.Request.BOOSTERS }, { typeof (NetCache.NetCacheCardBacks), GetAccountInfo.Request.CARD_BACKS }, { typeof (NetCache.NetCachePlayerRecords), GetAccountInfo.Request.PLAYER_RECORD }, { typeof (NetCache.NetCacheGamesPlayed), GetAccountInfo.Request.GAMES_PLAYED }, { typeof (NetCache.NetCacheProfileProgress), GetAccountInfo.Request.CAMPAIGN_INFO }, { typeof (NetCache.NetCacheProfileNotices), GetAccountInfo.Request.NOTICES }, { typeof (NetCache.NetCacheClientOptions), GetAccountInfo.Request.CLIENT_OPTIONS }, { typeof (NetCache.NetCacheCardValues), GetAccountInfo.Request.CARD_VALUES }, { typeof (NetCache.NetCacheDisconnectedGame), GetAccountInfo.Request.DISCONNECTED }, { typeof (NetCache.NetCacheArcaneDustBalance), GetAccountInfo.Request.ARCANE_DUST_BALANCE }, { typeof (NetCache.NetCacheFeatures), GetAccountInfo.Request.FEATURES }, { typeof (NetCache.NetCacheRewardProgress), GetAccountInfo.Request.REWARD_PROGRESS }, { typeof (NetCache.NetCacheGoldBalance), GetAccountInfo.Request.GOLD_BALANCE }, { typeof (NetCache.NetCacheHeroLevels), GetAccountInfo.Request.HERO_XP }, { typeof (NetCache.NetCachePlayQueue), GetAccountInfo.Request.PVP_QUEUE }, { typeof (NetCache.NetCacheNotSoMassiveLogin), GetAccountInfo.Request.NOT_SO_MASSIVE_LOGIN }, { typeof (NetCache.NetCacheTavernBrawlInfo), GetAccountInfo.Request.TAVERN_BRAWL_INFO }, { typeof (NetCache.NetCacheTavernBrawlRecord), GetAccountInfo.Request.TAVERN_BRAWL_RECORD }, { typeof (NetCache.NetCacheFavoriteHeroes), GetAccountInfo.Request.FAVORITE_HEROES }, { typeof (NetCache.NetCacheAccountLicenses), GetAccountInfo.Request.ACCOUNT_LICENSES } };
  private static readonly Map<System.Type, int> m_genericRequestTypeMap = new Map<System.Type, int>() { { typeof (ClientStaticAssetsResponse), 340 } };
  private static readonly Map<GetAccountInfo.Request, System.Type> m_requestTypeMap = NetCache.GetInvertTypeMap();
  private static bool m_fatalErrorCodeSet = false;
  private static NetCache s_instance = new NetCache();
  private Map<System.Type, object> m_netCache = new Map<System.Type, object>();
  private List<NetCache.DelNewNoticesListener> m_newNoticesListeners = new List<NetCache.DelNewNoticesListener>();
  private List<NetCache.DelGoldBalanceListener> m_goldBalanceListeners = new List<NetCache.DelGoldBalanceListener>();
  private Map<System.Type, List<System.Action>> m_updatedListeners = new Map<System.Type, List<System.Action>>();
  private Map<System.Type, int> m_changeRequests = new Map<System.Type, int>();
  private HashSet<long> m_ackedNotices = new HashSet<long>();
  private List<NetCache.ProfileNotice> m_queuedProfileNotices = new List<NetCache.ProfileNotice>();
  private List<NetCache.NetCacheBatchRequest> m_cacheRequests = new List<NetCache.NetCacheBatchRequest>();
  private List<System.Type> m_inTransitRequests = new List<System.Type>();
  private NetCache.NetCacheHeroLevels m_prevHeroLevels;
  private NetCache.NetCacheMedalInfo m_previousMedalInfo;
  private bool m_receivedInitialProfileNotices;
  private long m_lastForceCheckedSeason;

  private static Map<GetAccountInfo.Request, System.Type> GetInvertTypeMap()
  {
    Map<GetAccountInfo.Request, System.Type> map = new Map<GetAccountInfo.Request, System.Type>();
    using (Map<System.Type, GetAccountInfo.Request>.Enumerator enumerator = NetCache.m_getAccountInfoTypeMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<System.Type, GetAccountInfo.Request> current = enumerator.Current;
        map[current.Value] = current.Key;
      }
    }
    return map;
  }

  public T GetNetObject<T>()
  {
    object testData = this.GetTestData(typeof (T));
    if (testData != null)
      return (T) testData;
    if (this.m_netCache.TryGetValue(typeof (T), out testData) && testData is T)
      return (T) testData;
    return default (T);
  }

  public bool IsNetObjectAvailable<T>()
  {
    return (object) this.GetNetObject<T>() != null;
  }

  private object GetTestData(System.Type type)
  {
    if (type != typeof (NetCache.NetCacheBoosters) || !GameUtils.IsFakePackOpeningEnabled())
      return (object) null;
    NetCache.NetCacheBoosters netCacheBoosters = new NetCache.NetCacheBoosters();
    int fakePackCount = GameUtils.GetFakePackCount();
    NetCache.BoosterStack boosterStack = new NetCache.BoosterStack() { Id = 1, Count = fakePackCount };
    netCacheBoosters.BoosterStacks.Add(boosterStack);
    return (object) netCacheBoosters;
  }

  public void UnloadNetObject<T>()
  {
    this.m_netCache[typeof (T)] = (object) null;
  }

  public void ReloadNetObject<T>()
  {
    this.NetCacheReload_Internal((NetCache.NetCacheBatchRequest) null, typeof (T));
  }

  public void RefreshNetObject<T>()
  {
    this.RequestNetCacheObject(typeof (T));
  }

  private bool GetOption<T>(ServerOption type, out T ret) where T : NetCache.ClientOptionBase
  {
    ret = (T) null;
    NetCache.NetCacheClientOptions netObject = NetCache.Get().GetNetObject<NetCache.NetCacheClientOptions>();
    if (!this.ClientOptionExists(type))
      return false;
    T obj = netObject.ClientState[type] as T;
    if ((object) obj == null)
      return false;
    ret = obj;
    return true;
  }

  public int GetIntOption(ServerOption type)
  {
    NetCache.ClientOptionInt ret = (NetCache.ClientOptionInt) null;
    if (!this.GetOption<NetCache.ClientOptionInt>(type, out ret))
      return 0;
    return ret.OptionValue;
  }

  public bool GetIntOption(ServerOption type, out int ret)
  {
    ret = 0;
    NetCache.ClientOptionInt ret1 = (NetCache.ClientOptionInt) null;
    if (!this.GetOption<NetCache.ClientOptionInt>(type, out ret1))
      return false;
    ret = ret1.OptionValue;
    return true;
  }

  public long GetLongOption(ServerOption type)
  {
    NetCache.ClientOptionLong ret = (NetCache.ClientOptionLong) null;
    if (!this.GetOption<NetCache.ClientOptionLong>(type, out ret))
      return 0;
    return ret.OptionValue;
  }

  public bool GetLongOption(ServerOption type, out long ret)
  {
    ret = 0L;
    NetCache.ClientOptionLong ret1 = (NetCache.ClientOptionLong) null;
    if (!this.GetOption<NetCache.ClientOptionLong>(type, out ret1))
      return false;
    ret = ret1.OptionValue;
    return true;
  }

  public float GetFloatOption(ServerOption type)
  {
    NetCache.ClientOptionFloat ret = (NetCache.ClientOptionFloat) null;
    if (!this.GetOption<NetCache.ClientOptionFloat>(type, out ret))
      return 0.0f;
    return ret.OptionValue;
  }

  public bool GetFloatOption(ServerOption type, out float ret)
  {
    ret = 0.0f;
    NetCache.ClientOptionFloat ret1 = (NetCache.ClientOptionFloat) null;
    if (!this.GetOption<NetCache.ClientOptionFloat>(type, out ret1))
      return false;
    ret = ret1.OptionValue;
    return true;
  }

  public ulong GetULongOption(ServerOption type)
  {
    NetCache.ClientOptionULong ret = (NetCache.ClientOptionULong) null;
    if (!this.GetOption<NetCache.ClientOptionULong>(type, out ret))
      return 0;
    return ret.OptionValue;
  }

  public bool GetULongOption(ServerOption type, out ulong ret)
  {
    ret = 0UL;
    NetCache.ClientOptionULong ret1 = (NetCache.ClientOptionULong) null;
    if (!this.GetOption<NetCache.ClientOptionULong>(type, out ret1))
      return false;
    ret = ret1.OptionValue;
    return true;
  }

  public void RegisterUpdatedListener(System.Type type, System.Action listener)
  {
    List<System.Action> actionList;
    if (!this.m_updatedListeners.TryGetValue(type, out actionList))
    {
      actionList = new List<System.Action>();
      this.m_updatedListeners[type] = actionList;
    }
    actionList.Add(listener);
  }

  public void RemoveUpdatedListener(System.Type type, System.Action listener)
  {
    List<System.Action> actionList;
    if (!this.m_updatedListeners.TryGetValue(type, out actionList))
      return;
    actionList.Remove(listener);
  }

  public void RegisterNewNoticesListener(NetCache.DelNewNoticesListener listener)
  {
    if (this.m_newNoticesListeners.Contains(listener))
      return;
    this.m_newNoticesListeners.Add(listener);
  }

  public void RemoveNewNoticesListener(NetCache.DelNewNoticesListener listener)
  {
    this.m_newNoticesListeners.Remove(listener);
  }

  public bool RemoveNotice(long ID)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    NetCache.\u003CRemoveNotice\u003Ec__AnonStorey3C7 noticeCAnonStorey3C7 = new NetCache.\u003CRemoveNotice\u003Ec__AnonStorey3C7();
    // ISSUE: reference to a compiler-generated field
    noticeCAnonStorey3C7.ID = ID;
    NetCache.NetCacheProfileNotices cacheProfileNotices = this.m_netCache[typeof (NetCache.NetCacheProfileNotices)] as NetCache.NetCacheProfileNotices;
    if (cacheProfileNotices == null)
    {
      // ISSUE: reference to a compiler-generated field
      Debug.LogWarning((object) string.Format("NetCache.RemoveNotice({0}) - profileNotices is null", (object) noticeCAnonStorey3C7.ID));
      return false;
    }
    if (cacheProfileNotices.Notices == null)
    {
      // ISSUE: reference to a compiler-generated field
      Debug.LogWarning((object) string.Format("NetCache.RemoveNotice({0}) - profileNotices.Notices is null", (object) noticeCAnonStorey3C7.ID));
      return false;
    }
    // ISSUE: reference to a compiler-generated method
    NetCache.ProfileNotice profileNotice = cacheProfileNotices.Notices.Find(new Predicate<NetCache.ProfileNotice>(noticeCAnonStorey3C7.\u003C\u003Em__16A));
    if (profileNotice == null)
      return false;
    cacheProfileNotices.Notices.Remove(profileNotice);
    this.m_ackedNotices.Add(profileNotice.NoticeID);
    return true;
  }

  public void NetCacheChanged<T>()
  {
    System.Type key = typeof (T);
    int num1 = 0;
    this.m_changeRequests.TryGetValue(key, out num1);
    int num2 = num1 + 1;
    this.m_changeRequests[key] = num2;
    if (num2 > 1)
      return;
    while (this.m_changeRequests[key] > 0)
    {
      this.NetCacheChangedImpl<T>();
      this.m_changeRequests[key] = this.m_changeRequests[key] - 1;
    }
  }

  private void NetCacheChangedImpl<T>()
  {
    foreach (NetCache.NetCacheBatchRequest request in this.m_cacheRequests.ToArray())
    {
      using (Map<System.Type, NetCache.Request>.Enumerator enumerator = request.m_requests.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Key == typeof (T))
          {
            this.NetCacheCheckRequest(request);
            break;
          }
        }
      }
    }
  }

  public void CheckSeasonForRoll()
  {
    if (this.GetNetObject<NetCache.NetCacheProfileNotices>() == null)
      return;
    NetCache.NetCacheRewardProgress netObject = this.GetNetObject<NetCache.NetCacheRewardProgress>();
    if (netObject == null)
      return;
    DateTime utcNow = DateTime.UtcNow;
    DateTime dateTime = DateTime.FromFileTimeUtc(netObject.SeasonEndDate);
    if (dateTime >= utcNow || this.m_lastForceCheckedSeason == (long) netObject.Season)
      return;
    this.m_lastForceCheckedSeason = (long) netObject.Season;
    Log.Rachelle.Print("NetCache.CheckSeasonForRoll oldSeason = {0} season end = {1} utc now = {2}", new object[3]
    {
      (object) this.m_lastForceCheckedSeason,
      (object) dateTime,
      (object) utcNow
    });
  }

  public void OnArcaneDustBalanceChanged(long balanceChange)
  {
    this.GetNetObject<NetCache.NetCacheArcaneDustBalance>().Balance += balanceChange;
    this.NetCacheChanged<NetCache.NetCacheArcaneDustBalance>();
  }

  public void RegisterGoldBalanceListener(NetCache.DelGoldBalanceListener listener)
  {
    if (this.m_goldBalanceListeners.Contains(listener))
      return;
    this.m_goldBalanceListeners.Add(listener);
  }

  public void RemoveGoldBalanceListener(NetCache.DelGoldBalanceListener listener)
  {
    this.m_goldBalanceListeners.Remove(listener);
  }

  public static void DefaultErrorHandler(NetCache.ErrorInfo info)
  {
    if (info.Error == NetCache.ErrorCode.TIMEOUT)
    {
      if (BreakingNews.SHOWS_BREAKING_NEWS)
        Network.Get().ShowBreakingNewsOrError("GLOBAL_ERROR_NETWORK_UTIL_TIMEOUT", 0.0f);
      else
        NetCache.ShowError(info, "GLOBAL_ERROR_NETWORK_UTIL_TIMEOUT");
    }
    else
      NetCache.ShowError(info, "GLOBAL_ERROR_NETWORK_GENERIC");
  }

  public static void ShowError(NetCache.ErrorInfo info, string localizationKey, params object[] localizationArgs)
  {
    Error.AddFatalLoc(localizationKey, localizationArgs);
    Debug.LogError((object) NetCache.GetInternalErrorMessage(info, true));
  }

  public static string GetInternalErrorMessage(NetCache.ErrorInfo info, bool includeStackTrace = true)
  {
    Map<System.Type, object> netCache = NetCache.Get().m_netCache;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.AppendFormat("NetCache Error: {0}", (object) info.Error);
    stringBuilder.AppendFormat("\nFrom: {0}", (object) info.RequestingFunction.Method.Name);
    stringBuilder.AppendFormat("\nRequested Data ({0}):", (object) info.RequestedTypes.Count);
    using (Map<System.Type, NetCache.Request>.Enumerator enumerator = info.RequestedTypes.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<System.Type, NetCache.Request> current = enumerator.Current;
        object obj = (object) null;
        netCache.TryGetValue(current.Key, out obj);
        if (obj == null)
          stringBuilder.AppendFormat("\n[{0}] MISSING", (object) current.Key);
        else
          stringBuilder.AppendFormat("\n[{0}]", (object) current.Key);
      }
    }
    if (includeStackTrace)
      stringBuilder.AppendFormat("\nStack Trace:\n{0}", (object) info.RequestStackTrace);
    return stringBuilder.ToString();
  }

  private void NetCacheMakeBatchRequest(NetCache.NetCacheBatchRequest batchRequest)
  {
    List<GetAccountInfo.Request> requestList = new List<GetAccountInfo.Request>();
    List<GenericRequest> genericRequests = (List<GenericRequest>) null;
    using (Map<System.Type, NetCache.Request>.Enumerator enumerator = batchRequest.m_requests.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.Request request1 = enumerator.Current.Value;
        if (request1 == null)
        {
          Debug.LogError((object) string.Format("NetUseBatchRequest Null request for {0}...SKIP", (object) request1.m_type.Name));
        }
        else
        {
          if (request1.m_reload)
            this.m_netCache[request1.m_type] = (object) null;
          if ((!this.m_netCache.ContainsKey(request1.m_type) || this.m_netCache[request1.m_type] == null) && !this.m_inTransitRequests.Contains(request1.m_type))
          {
            request1.m_result = NetCache.RequestResult.PENDING;
            this.m_inTransitRequests.Add(request1.m_type);
            GetAccountInfo.Request request2;
            if (NetCache.m_getAccountInfoTypeMap.TryGetValue(request1.m_type, out request2))
            {
              requestList.Add(request2);
            }
            else
            {
              int num;
              if (NetCache.m_genericRequestTypeMap.TryGetValue(request1.m_type, out num))
              {
                if (genericRequests == null)
                  genericRequests = new List<GenericRequest>();
                genericRequests.Add(new GenericRequest()
                {
                  RequestId = num
                });
              }
              else
                Log.Net.Print("NetCache: Unable to make request for type={0}", (object) request1.m_type.FullName);
            }
          }
        }
      }
    }
    if (requestList.Count > 0 || genericRequests != null)
      Network.RequestNetCacheObjectList(requestList, genericRequests);
    this.m_cacheRequests.Add(batchRequest);
    this.NetCacheCheckRequest(batchRequest);
  }

  private void NetCacheUse_Internal(NetCache.NetCacheBatchRequest request, System.Type type)
  {
    if (request != null && request.m_requests.ContainsKey(type))
      Log.Bob.Print(string.Format("NetCache ...SKIP {0}", (object) type.Name));
    else if (this.m_netCache.ContainsKey(type) && this.m_netCache[type] != null)
    {
      Log.Bob.Print(string.Format("NetCache ...USE {0}", (object) type.Name));
    }
    else
    {
      Log.Bob.Print(string.Format("NetCache <<<GET {0}", (object) type.Name));
      this.RequestNetCacheObject(type);
    }
  }

  private void RequestNetCacheObject(System.Type type)
  {
    if (this.m_inTransitRequests.Contains(type))
      return;
    this.m_inTransitRequests.Add(type);
    Network.RequestNetCacheObject(NetCache.m_getAccountInfoTypeMap[type]);
  }

  private void NetCacheReload_Internal(NetCache.NetCacheBatchRequest request, System.Type type)
  {
    this.m_netCache[type] = (object) null;
    if (type == typeof (NetCache.NetCacheProfileNotices))
      Debug.LogError((object) "NetCacheReload_Internal - tried to issue request with type NetCacheProfileNotices - this is no longer allowed!");
    else
      this.NetCacheUse_Internal(request, type);
  }

  private void NetCacheCheckRequest(NetCache.NetCacheBatchRequest request)
  {
    using (Map<System.Type, NetCache.Request>.Enumerator enumerator = request.m_requests.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<System.Type, NetCache.Request> current = enumerator.Current;
        if (!this.m_netCache.ContainsKey(current.Key) || this.m_netCache[current.Key] == null)
          return;
      }
    }
    request.m_canTimeout = false;
    if (request.m_callback == null)
      return;
    request.m_callback();
  }

  private void UpdateRequestNeedState(System.Type type, NetCache.RequestResult result)
  {
    using (List<NetCache.NetCacheBatchRequest>.Enumerator enumerator = this.m_cacheRequests.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.NetCacheBatchRequest current = enumerator.Current;
        if (current.m_requests.ContainsKey(type))
          current.m_requests[type].m_result = result;
      }
    }
  }

  private void OnNetCacheObjReceived<T>(T netCacheObject)
  {
    System.Type index1 = typeof (T);
    Log.Bob.Print(string.Format("OnNetCacheObjReceived SAVE --> {0}", (object) index1.Name));
    this.UpdateRequestNeedState(index1, NetCache.RequestResult.DATA_COMPLETE);
    this.m_netCache[index1] = (object) netCacheObject;
    this.m_inTransitRequests.Remove(index1);
    this.NetCacheChanged<T>();
    List<System.Action> actionList;
    if (!this.m_updatedListeners.TryGetValue(index1, out actionList))
      return;
    for (int index2 = 0; index2 < actionList.Count; ++index2)
      actionList[index2]();
  }

  public void Clear()
  {
    this.m_netCache.Clear();
    this.m_prevHeroLevels = (NetCache.NetCacheHeroLevels) null;
    this.m_changeRequests.Clear();
    this.m_cacheRequests.Clear();
    this.m_inTransitRequests.Clear();
  }

  public void UnregisterNetCacheHandler(NetCache.NetCacheCallback handler)
  {
    using (List<NetCache.NetCacheBatchRequest>.Enumerator enumerator = this.m_cacheRequests.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.NetCacheBatchRequest current = enumerator.Current;
        if (!((MulticastDelegate) current.m_callback != (MulticastDelegate) handler))
        {
          this.m_cacheRequests.Remove(current);
          break;
        }
      }
    }
  }

  public void Heartbeat()
  {
    NetCache.NetCacheBatchRequest[] array = this.m_cacheRequests.ToArray();
    DateTime now = DateTime.Now;
    foreach (NetCache.NetCacheBatchRequest cacheBatchRequest in array)
    {
      if (cacheBatchRequest.m_canTimeout && !(now - cacheBatchRequest.m_timeAdded < Network.GetMaxDeferredWait()) && !Network.Get().HaveUnhandledPackets())
      {
        cacheBatchRequest.m_canTimeout = false;
        if (!NetCache.m_fatalErrorCodeSet)
        {
          NetCache.ErrorInfo info = new NetCache.ErrorInfo();
          info.Error = NetCache.ErrorCode.TIMEOUT;
          info.RequestingFunction = cacheBatchRequest.m_requestFunc;
          info.RequestedTypes = new Map<System.Type, NetCache.Request>((IEnumerable<KeyValuePair<System.Type, NetCache.Request>>) cacheBatchRequest.m_requests);
          info.RequestStackTrace = cacheBatchRequest.m_requestStackTrace;
          string errorSubset1 = "CT";
          int num = 0;
          using (Map<System.Type, NetCache.Request>.Enumerator enumerator = cacheBatchRequest.m_requests.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              KeyValuePair<System.Type, NetCache.Request> current = enumerator.Current;
              switch (current.Value.m_result)
              {
                case NetCache.RequestResult.GENERIC_COMPLETE:
                case NetCache.RequestResult.DATA_COMPLETE:
                  if (num < 3)
                    continue;
                  goto label_12;
                default:
                  string[] strArray = current.Value.m_type.ToString().Split('+');
                  if (strArray.GetLength(0) != 0)
                  {
                    string str = strArray[strArray.GetLength(0) - 1];
                    errorSubset1 = errorSubset1 + ";" + str + "=" + (object) current.Value.m_result;
                    ++num;
                    goto case NetCache.RequestResult.GENERIC_COMPLETE;
                  }
                  else
                    goto case NetCache.RequestResult.GENERIC_COMPLETE;
              }
            }
          }
label_12:
          FatalErrorMgr.Get().SetErrorCode("HS", errorSubset1, (string) null, (string) null);
          NetCache.m_fatalErrorCodeSet = true;
          cacheBatchRequest.m_errorCallback(info);
        }
      }
    }
  }

  private void OnGenericResponse()
  {
    Network.GenericResponse genericResponse = Network.GetGenericResponse();
    if (genericResponse == null)
    {
      Debug.LogError((object) string.Format("NetCache - GenericResponse parse error"));
    }
    else
    {
      if ((long) genericResponse.RequestId != 201L)
        return;
      System.Type key;
      if (!NetCache.m_requestTypeMap.TryGetValue((GetAccountInfo.Request) genericResponse.RequestSubId, out key))
      {
        Debug.LogError((object) string.Format("NetCache - Ignoring unexpected requestId={0}:{1}", (object) genericResponse.RequestId, (object) genericResponse.RequestSubId));
      }
      else
      {
        foreach (NetCache.NetCacheBatchRequest cacheBatchRequest in this.m_cacheRequests.ToArray())
        {
          if (cacheBatchRequest.m_requests.ContainsKey(key))
          {
            switch (genericResponse.ResultCode)
            {
              case Network.GenericResponse.Result.REQUEST_IN_PROCESS:
                if (cacheBatchRequest.m_requests[key].m_result == NetCache.RequestResult.PENDING)
                {
                  cacheBatchRequest.m_requests[key].m_result = NetCache.RequestResult.IN_PROCESS;
                  continue;
                }
                continue;
              case Network.GenericResponse.Result.REQUEST_COMPLETE:
                cacheBatchRequest.m_requests[key].m_result = NetCache.RequestResult.GENERIC_COMPLETE;
                Debug.LogWarning((object) string.Format("GenericResponse Success for requestId={0}:{1}", (object) genericResponse.RequestId, (object) genericResponse.RequestSubId));
                continue;
              default:
                Debug.LogError((object) string.Format("Unhandled failure code={0} {1} for requestId={2}:{3}", (object) genericResponse.ResultCode, (object) genericResponse.ResultCode.ToString(), (object) genericResponse.RequestId, (object) genericResponse.RequestSubId));
                cacheBatchRequest.m_requests[key].m_result = NetCache.RequestResult.ERROR;
                NetCache.ErrorInfo info = new NetCache.ErrorInfo();
                info.Error = NetCache.ErrorCode.SERVER;
                info.ServerError = (uint) genericResponse.ResultCode;
                info.RequestingFunction = cacheBatchRequest.m_requestFunc;
                info.RequestedTypes = new Map<System.Type, NetCache.Request>((IEnumerable<KeyValuePair<System.Type, NetCache.Request>>) cacheBatchRequest.m_requests);
                info.RequestStackTrace = cacheBatchRequest.m_requestStackTrace;
                FatalErrorMgr.Get().SetErrorCode("HS", "CG" + genericResponse.ResultCode.ToString(), genericResponse.RequestId.ToString(), genericResponse.RequestSubId.ToString());
                cacheBatchRequest.m_errorCallback(info);
                continue;
            }
          }
        }
      }
    }
  }

  private void OnDBAction()
  {
    Network.DBAction dbAction = Network.GetDbAction();
    if (dbAction.Result == Network.DBAction.ResultType.SUCCESS)
      return;
    Debug.LogError((object) string.Format("Unhandled dbAction {0} with error {1}", (object) dbAction.Action, (object) dbAction.Result));
  }

  private void OnBoosters()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheBoosters>(Network.GetBoosters());
  }

  private void OnCollection()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheCollection>(Network.GetCollectionCardStacks());
  }

  private void OnDecks()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheDecks>(Network.GetDeckHeaders());
  }

  private void OnCardValues()
  {
    NetCache.NetCacheCardValues netCacheObject = new NetCache.NetCacheCardValues();
    CardValues cardValues = Network.GetCardValues();
    if (cardValues != null || Network.ShouldBeConnectedToAurora())
    {
      netCacheObject.CardNerfIndex = cardValues.CardNerfIndex;
      using (List<PegasusUtil.CardValue>.Enumerator enumerator = cardValues.Cards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          PegasusUtil.CardValue current = enumerator.Current;
          string cardId = GameUtils.TranslateDbIdToCardId(current.Card.Asset);
          if (cardId == null)
            Debug.LogError((object) string.Format("NetCache.OnCardValues(): Cannot find card '{0}' in card manifest.  Confirm your card manifest matches your game server's database.", (object) current.Card.Asset));
          else
            netCacheObject.Values.Add(new NetCache.CardDefinition()
            {
              Name = cardId,
              Premium = (TAG_PREMIUM) current.Card.Premium
            }, new NetCache.CardValue()
            {
              Buy = current.Buy,
              Sell = current.Sell,
              Nerfed = current.Nerfed
            });
        }
      }
    }
    this.OnNetCacheObjReceived<NetCache.NetCacheCardValues>(netCacheObject);
  }

  private void OnMedalInfo()
  {
    NetCache.NetCacheMedalInfo medalInfo = Network.GetMedalInfo();
    if (this.m_previousMedalInfo != null)
      medalInfo.PreviousMedalInfo = this.m_previousMedalInfo.Clone();
    this.m_previousMedalInfo = medalInfo;
    this.OnNetCacheObjReceived<NetCache.NetCacheMedalInfo>(medalInfo);
  }

  private void OnFeaturesChanged()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheFeatures>(Network.GetFeatures());
  }

  private void OnArcaneDustBalance()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheArcaneDustBalance>(new NetCache.NetCacheArcaneDustBalance()
    {
      Balance = Network.GetArcaneDustBalance()
    });
  }

  private void OnGoldBalance()
  {
    NetCache.NetCacheGoldBalance goldBalance = Network.GetGoldBalance();
    this.OnNetCacheObjReceived<NetCache.NetCacheGoldBalance>(goldBalance);
    foreach (NetCache.DelGoldBalanceListener goldBalanceListener in this.m_goldBalanceListeners.ToArray())
      goldBalanceListener(goldBalance);
  }

  private void OnGamesInfo()
  {
    NetCache.NetCacheGamesPlayed gamesInfo = Network.GetGamesInfo();
    if (gamesInfo == null)
      Debug.LogWarning((object) "error getting games info");
    else
      this.OnNetCacheObjReceived<NetCache.NetCacheGamesPlayed>(gamesInfo);
  }

  private void OnProfileProgress()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheProfileProgress>(Network.GetProfileProgress());
  }

  private void OnHearthstoneUnavailableGame()
  {
    this.OnHearthstoneUnavailable(true);
  }

  private void OnHearthstoneUnavailableUtil()
  {
    this.OnHearthstoneUnavailable(false);
  }

  private void OnProfileNoticeNotifications()
  {
    this.HandleIncomingProfileNotices(Network.GetProfileNoticeNotifications(), false);
  }

  private void OnHearthstoneUnavailable(bool gamePacket)
  {
    Network.UnavailableReason hearthstoneUnavailable = Network.GetHearthstoneUnavailable(gamePacket);
    Debug.Log((object) ("Hearthstone Unavailable!  Reason: " + hearthstoneUnavailable.mainReason));
    string mainReason = hearthstoneUnavailable.mainReason;
    if (mainReason != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (NetCache.\u003C\u003Ef__switch\u0024mapB0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        NetCache.\u003C\u003Ef__switch\u0024mapB0 = new Dictionary<string, int>(2)
        {
          {
            "VERSION",
            0
          },
          {
            "OFFLINE",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (NetCache.\u003C\u003Ef__switch\u0024mapB0.TryGetValue(mainReason, out num))
      {
        if (num != 0)
        {
          if (num == 1)
          {
            Network.Get().ShowConnectionFailureError("GLOBAL_ERROR_NETWORK_UNAVAILABLE_OFFLINE");
            return;
          }
        }
        else
        {
          ErrorParams parms = new ErrorParams();
          parms.m_message = GameStrings.Format("GLOBAL_ERROR_NETWORK_UNAVAILABLE_UPGRADE");
          if ((bool) Error.HAS_APP_STORE)
            parms.m_redirectToStore = true;
          Error.AddFatal(parms);
          return;
        }
      }
    }
    BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_ERROR_NETWORK_UNAVAILABLE, 1, hearthstoneUnavailable.mainReason);
    Network.Get().ShowConnectionFailureError("GLOBAL_ERROR_NETWORK_UNAVAILABLE_UNKNOWN");
  }

  private void OnPlayQueue()
  {
    this.OnNetCacheObjReceived<NetCache.NetCachePlayQueue>(Network.GetPlayQueue());
    Log.Bob.Print("play queue {0}", (object) NetCache.Get().GetNetObject<NetCache.NetCachePlayQueue>().GameType);
  }

  private void OnCardBacks()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheCardBacks>(Network.GetCardBacks());
  }

  private void OnPlayerRecords()
  {
    this.OnNetCacheObjReceived<NetCache.NetCachePlayerRecords>(Network.GetPlayerRecords());
  }

  private void OnRewardProgress()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheRewardProgress>(Network.GetRewardProgress());
  }

  private void OnAllHeroXP()
  {
    NetCache.NetCacheHeroLevels allHeroXp = Network.GetAllHeroXP();
    if (this.m_prevHeroLevels != null)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      NetCache.\u003COnAllHeroXP\u003Ec__AnonStorey3C8 xpCAnonStorey3C8 = new NetCache.\u003COnAllHeroXP\u003Ec__AnonStorey3C8();
      using (List<NetCache.HeroLevel>.Enumerator enumerator = allHeroXp.Levels.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          // ISSUE: reference to a compiler-generated field
          xpCAnonStorey3C8.newHeroLevel = enumerator.Current;
          // ISSUE: reference to a compiler-generated method
          NetCache.HeroLevel heroLevel = this.m_prevHeroLevels.Levels.Find(new Predicate<NetCache.HeroLevel>(xpCAnonStorey3C8.\u003C\u003Em__16B));
          if (heroLevel != null)
          {
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            if (xpCAnonStorey3C8.newHeroLevel != null && xpCAnonStorey3C8.newHeroLevel.CurrentLevel != null && xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level != heroLevel.CurrentLevel.Level && (xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level == 20 || xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level == 30 || (xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level == 40 || xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level == 50) || xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level == 60))
            {
              // ISSUE: reference to a compiler-generated field
              if (xpCAnonStorey3C8.newHeroLevel.Class == TAG_CLASS.DRUID)
              {
                // ISSUE: reference to a compiler-generated field
                BnetPresenceMgr.Get().SetGameField(5U, xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level);
              }
              else
              {
                // ISSUE: reference to a compiler-generated field
                if (xpCAnonStorey3C8.newHeroLevel.Class == TAG_CLASS.HUNTER)
                {
                  // ISSUE: reference to a compiler-generated field
                  BnetPresenceMgr.Get().SetGameField(6U, xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level);
                }
                else
                {
                  // ISSUE: reference to a compiler-generated field
                  if (xpCAnonStorey3C8.newHeroLevel.Class == TAG_CLASS.MAGE)
                  {
                    // ISSUE: reference to a compiler-generated field
                    BnetPresenceMgr.Get().SetGameField(7U, xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level);
                  }
                  else
                  {
                    // ISSUE: reference to a compiler-generated field
                    if (xpCAnonStorey3C8.newHeroLevel.Class == TAG_CLASS.PALADIN)
                    {
                      // ISSUE: reference to a compiler-generated field
                      BnetPresenceMgr.Get().SetGameField(8U, xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level);
                    }
                    else
                    {
                      // ISSUE: reference to a compiler-generated field
                      if (xpCAnonStorey3C8.newHeroLevel.Class == TAG_CLASS.PRIEST)
                      {
                        // ISSUE: reference to a compiler-generated field
                        BnetPresenceMgr.Get().SetGameField(9U, xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level);
                      }
                      else
                      {
                        // ISSUE: reference to a compiler-generated field
                        if (xpCAnonStorey3C8.newHeroLevel.Class == TAG_CLASS.ROGUE)
                        {
                          // ISSUE: reference to a compiler-generated field
                          BnetPresenceMgr.Get().SetGameField(10U, xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level);
                        }
                        else
                        {
                          // ISSUE: reference to a compiler-generated field
                          if (xpCAnonStorey3C8.newHeroLevel.Class == TAG_CLASS.SHAMAN)
                          {
                            // ISSUE: reference to a compiler-generated field
                            BnetPresenceMgr.Get().SetGameField(11U, xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level);
                          }
                          else
                          {
                            // ISSUE: reference to a compiler-generated field
                            if (xpCAnonStorey3C8.newHeroLevel.Class == TAG_CLASS.WARLOCK)
                            {
                              // ISSUE: reference to a compiler-generated field
                              BnetPresenceMgr.Get().SetGameField(12U, xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level);
                            }
                            else
                            {
                              // ISSUE: reference to a compiler-generated field
                              if (xpCAnonStorey3C8.newHeroLevel.Class == TAG_CLASS.WARRIOR)
                              {
                                // ISSUE: reference to a compiler-generated field
                                BnetPresenceMgr.Get().SetGameField(13U, xpCAnonStorey3C8.newHeroLevel.CurrentLevel.Level);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            // ISSUE: reference to a compiler-generated field
            xpCAnonStorey3C8.newHeroLevel.PrevLevel = heroLevel.CurrentLevel;
          }
        }
      }
    }
    this.m_prevHeroLevels = allHeroXp;
    this.OnNetCacheObjReceived<NetCache.NetCacheHeroLevels>(allHeroXp);
  }

  private void OnProfileNotices()
  {
    List<NetCache.ProfileNotice> profileNotices = Network.GetProfileNotices();
    this.m_receivedInitialProfileNotices = true;
    this.HandleIncomingProfileNotices(profileNotices, true);
    this.HandleIncomingProfileNotices(this.m_queuedProfileNotices, true);
    this.m_queuedProfileNotices.Clear();
  }

  private void HandleIncomingProfileNotices(List<NetCache.ProfileNotice> receivedNotices, bool isInitialNoticeList)
  {
    if (!this.m_receivedInitialProfileNotices)
    {
      this.m_queuedProfileNotices.AddRange((IEnumerable<NetCache.ProfileNotice>) receivedNotices);
    }
    else
    {
      if (receivedNotices.Find((Predicate<NetCache.ProfileNotice>) (obj => obj.Type == NetCache.ProfileNotice.NoticeType.GAINED_MEDAL)) != null)
      {
        this.m_previousMedalInfo = (NetCache.NetCacheMedalInfo) null;
        NetCache.NetCacheMedalInfo netObject = this.GetNetObject<NetCache.NetCacheMedalInfo>();
        if (netObject != null)
          netObject.PreviousMedalInfo = (NetCache.NetCacheMedalInfo) null;
      }
      List<NetCache.ProfileNotice> newNotices = this.FindNewNotices(receivedNotices);
      NetCache.NetCacheProfileNotices netCacheObject = this.GetNetObject<NetCache.NetCacheProfileNotices>() ?? new NetCache.NetCacheProfileNotices();
      HashSet<long> longSet = new HashSet<long>();
      for (int index = 0; index < netCacheObject.Notices.Count; ++index)
        longSet.Add(netCacheObject.Notices[index].NoticeID);
      for (int index = 0; index < receivedNotices.Count; ++index)
      {
        if (!this.m_ackedNotices.Contains(receivedNotices[index].NoticeID) && !longSet.Contains(receivedNotices[index].NoticeID))
          netCacheObject.Notices.Add(receivedNotices[index]);
      }
      this.OnNetCacheObjReceived<NetCache.NetCacheProfileNotices>(netCacheObject);
      NetCache.DelNewNoticesListener[] array = this.m_newNoticesListeners.ToArray();
      using (List<NetCache.ProfileNotice>.Enumerator enumerator = newNotices.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          NetCache.ProfileNotice current = enumerator.Current;
          Log.Achievements.Print("NetCache.OnProfileNotices() sending {0} to {1} listeners", new object[2]
          {
            (object) current,
            (object) array.Length
          });
        }
      }
      foreach (NetCache.DelNewNoticesListener newNoticesListener in array)
      {
        Log.Achievements.Print("NetCache.OnProfileNotices(): sending notices to {0}::{1}", new object[2]
        {
          (object) newNoticesListener.Method.ReflectedType.Name,
          (object) newNoticesListener.Method.Name
        });
        newNoticesListener(newNotices, isInitialNoticeList);
      }
    }
  }

  private List<NetCache.ProfileNotice> FindNewNotices(List<NetCache.ProfileNotice> receivedNotices)
  {
    List<NetCache.ProfileNotice> profileNoticeList = new List<NetCache.ProfileNotice>();
    NetCache.NetCacheProfileNotices netObject = this.GetNetObject<NetCache.NetCacheProfileNotices>();
    if (netObject == null)
    {
      profileNoticeList.AddRange((IEnumerable<NetCache.ProfileNotice>) receivedNotices);
    }
    else
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      NetCache.\u003CFindNewNotices\u003Ec__AnonStorey3C9 noticesCAnonStorey3C9 = new NetCache.\u003CFindNewNotices\u003Ec__AnonStorey3C9();
      using (List<NetCache.ProfileNotice>.Enumerator enumerator = receivedNotices.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          // ISSUE: reference to a compiler-generated field
          noticesCAnonStorey3C9.receivedNotice = enumerator.Current;
          // ISSUE: reference to a compiler-generated method
          if (netObject.Notices.Find(new Predicate<NetCache.ProfileNotice>(noticesCAnonStorey3C9.\u003C\u003Em__16D)) == null)
          {
            // ISSUE: reference to a compiler-generated field
            profileNoticeList.Add(noticesCAnonStorey3C9.receivedNotice);
          }
        }
      }
    }
    return profileNoticeList;
  }

  private void OnClientOptions()
  {
    NetCache.NetCacheClientOptions netCacheObject = this.GetNetObject<NetCache.NetCacheClientOptions>();
    bool flag = netCacheObject == null;
    if (flag)
      netCacheObject = new NetCache.NetCacheClientOptions();
    Network.ReadClientOptions(netCacheObject.ClientState);
    netCacheObject.UpdateServerState();
    this.OnNetCacheObjReceived<NetCache.NetCacheClientOptions>(netCacheObject);
    if (flag)
      OptionsMigration.UpgradeServerOptions();
    netCacheObject.RemoveInvalidOptions();
  }

  private void SetClientOption(ServerOption type, NetCache.ClientOptionBase newVal)
  {
    NetCache.NetCacheClientOptions cacheClientOptions = (NetCache.NetCacheClientOptions) this.m_netCache[typeof (NetCache.NetCacheClientOptions)];
    cacheClientOptions.ClientState[type] = newVal;
    NetCache.NetCacheFeatures netObject = NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>();
    if (netObject != null && netObject.Misc != null && netObject.Misc.ClientOptionsUpdateIntervalSeconds > 0)
      ApplicationMgr.Get().ScheduleCallback((float) netObject.Misc.ClientOptionsUpdateIntervalSeconds, true, new ApplicationMgr.ScheduledCallback(cacheClientOptions.OnUpdateIntervalElasped), (object) null);
    this.NetCacheChanged<NetCache.NetCacheClientOptions>();
  }

  public void SetIntOption(ServerOption type, int val)
  {
    this.SetClientOption(type, (NetCache.ClientOptionBase) new NetCache.ClientOptionInt(val));
  }

  public void SetLongOption(ServerOption type, long val)
  {
    this.SetClientOption(type, (NetCache.ClientOptionBase) new NetCache.ClientOptionLong(val));
  }

  public void SetFloatOption(ServerOption type, float val)
  {
    this.SetClientOption(type, (NetCache.ClientOptionBase) new NetCache.ClientOptionFloat(val));
  }

  public void SetULongOption(ServerOption type, ulong val)
  {
    this.SetClientOption(type, (NetCache.ClientOptionBase) new NetCache.ClientOptionULong(val));
  }

  public void DeleteClientOption(ServerOption type)
  {
    this.SetClientOption(type, (NetCache.ClientOptionBase) null);
  }

  public bool ClientOptionExists(ServerOption type)
  {
    NetCache.NetCacheClientOptions netObject = this.GetNetObject<NetCache.NetCacheClientOptions>();
    if (netObject == null || !netObject.ClientState.ContainsKey(type))
      return false;
    return netObject.ClientState[type] != null;
  }

  private void OnLastGameInfo()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheDisconnectedGame>(Network.GetDisconnectedGameInfo());
  }

  private void OnNotSoMassiveLoginReply()
  {
    NotSoMassiveLoginReply massiveLoginReply = Network.GetNotSoMassiveLoginReply();
    if (massiveLoginReply != null)
      SpecialEventManager.Get().InitEventTiming((IList<SpecialEventTiming>) massiveLoginReply.SpecialEventTiming);
    NetCache.NetCacheNotSoMassiveLogin netCacheObject = new NetCache.NetCacheNotSoMassiveLogin();
    netCacheObject.Packet = massiveLoginReply;
    if (massiveLoginReply.HasTavernBrawls)
    {
      this.OnNetCacheObjReceived<NetCache.NetCacheTavernBrawlInfo>(new NetCache.NetCacheTavernBrawlInfo(massiveLoginReply.TavernBrawls));
      if (massiveLoginReply.TavernBrawls.HasCurrentTavernBrawl && massiveLoginReply.TavernBrawls.CurrentTavernBrawl.MyRecord != null)
        this.OnNetCacheObjReceived<NetCache.NetCacheTavernBrawlRecord>(new NetCache.NetCacheTavernBrawlRecord(massiveLoginReply.TavernBrawls.CurrentTavernBrawl.MyRecord));
    }
    this.OnNetCacheObjReceived<NetCache.NetCacheNotSoMassiveLogin>(netCacheObject);
  }

  private void OnTavernBrawlInfoResponse()
  {
    TavernBrawlInfo tavernBrawlInfo = Network.GetTavernBrawlInfo();
    this.OnNetCacheObjReceived<NetCache.NetCacheTavernBrawlInfo>(new NetCache.NetCacheTavernBrawlInfo(tavernBrawlInfo));
    if (!tavernBrawlInfo.HasCurrentTavernBrawl || tavernBrawlInfo.CurrentTavernBrawl.MyRecord == null)
      return;
    this.OnNetCacheObjReceived<NetCache.NetCacheTavernBrawlRecord>(new NetCache.NetCacheTavernBrawlRecord(tavernBrawlInfo.CurrentTavernBrawl.MyRecord));
  }

  private void OnTavernBrawlRecordResponse()
  {
    this.OnNetCacheObjReceived<NetCache.NetCacheTavernBrawlRecord>(new NetCache.NetCacheTavernBrawlRecord(Network.GetTavernBrawlRecord()));
  }

  private void OnTavernBrawlRequestSessionBeginResponse()
  {
    TavernBrawlRequestSessionBeginResponse brawlSessionBegin = Network.GetTavernBrawlSessionBegin();
    if (!brawlSessionBegin.HasPlayerRecord)
      return;
    this.OnNetCacheObjReceived<NetCache.NetCacheTavernBrawlRecord>(new NetCache.NetCacheTavernBrawlRecord(brawlSessionBegin.PlayerRecord));
  }

  private void OnTavernBrawlRequestSessionRetireResponse()
  {
    TavernBrawlRequestSessionRetireResponse brawlSessionRetired = Network.GetTavernBrawlSessionRetired();
    if (brawlSessionRetired.PlayerRecord == null)
      return;
    this.OnNetCacheObjReceived<NetCache.NetCacheTavernBrawlRecord>(new NetCache.NetCacheTavernBrawlRecord(brawlSessionRetired.PlayerRecord));
  }

  private void OnFavoriteHeroesResponse()
  {
    FavoriteHeroesResponse favoriteHeroesResponse = Network.GetFavoriteHeroesResponse();
    NetCache.NetCacheFavoriteHeroes netCacheObject = new NetCache.NetCacheFavoriteHeroes();
    using (List<FavoriteHero>.Enumerator enumerator = favoriteHeroesResponse.FavoriteHeroes.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FavoriteHero current = enumerator.Current;
        TAG_CLASS outVal1;
        if (!EnumUtils.TryCast<TAG_CLASS>((object) current.ClassId, out outVal1))
        {
          Debug.LogWarning((object) string.Format("NetCache.OnFavoriteHeroesResponse() unrecognized hero class {0}", (object) current.ClassId));
        }
        else
        {
          TAG_PREMIUM outVal2;
          if (!EnumUtils.TryCast<TAG_PREMIUM>((object) current.Hero.Premium, out outVal2))
          {
            Debug.LogWarning((object) string.Format("NetCache.OnFavoriteHeroesResponse() unrecognized hero premium {0} for hero class {1}", (object) current.Hero.Premium, (object) outVal1));
          }
          else
          {
            NetCache.CardDefinition cardDefinition = new NetCache.CardDefinition() { Name = GameUtils.TranslateDbIdToCardId(current.Hero.Asset), Premium = outVal2 };
            netCacheObject.FavoriteHeroes[outVal1] = cardDefinition;
          }
        }
      }
    }
    this.OnNetCacheObjReceived<NetCache.NetCacheFavoriteHeroes>(netCacheObject);
  }

  private void OnAccountLicensesInfoResponse()
  {
    AccountLicensesInfoResponse licensesInfoResponse = Network.GetAccountLicensesInfoResponse();
    NetCache.NetCacheAccountLicenses netCacheObject = new NetCache.NetCacheAccountLicenses();
    using (List<AccountLicenseInfo>.Enumerator enumerator = licensesInfoResponse.List.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AccountLicenseInfo current = enumerator.Current;
        netCacheObject.AccountLicenses[current.License] = current;
      }
    }
    this.OnNetCacheObjReceived<NetCache.NetCacheAccountLicenses>(netCacheObject);
  }

  private void OnClientStaticAssetsResponse()
  {
    ClientStaticAssetsResponse staticAssetsResponse = Network.GetClientStaticAssetsResponse();
    if (staticAssetsResponse == null)
      return;
    this.OnNetCacheObjReceived<ClientStaticAssetsResponse>(staticAssetsResponse);
  }

  private void RegisterNetCacheHandlers()
  {
    Network network = Network.Get();
    network.RegisterNetHandler((object) PegasusUtil.DBAction.PacketID.ID, new Network.NetHandler(this.OnDBAction), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.GenericResponse.PacketID.ID, new Network.NetHandler(this.OnGenericResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) BoosterList.PacketID.ID, new Network.NetHandler(this.OnBoosters), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) Collection.PacketID.ID, new Network.NetHandler(this.OnCollection), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) DeckList.PacketID.ID, new Network.NetHandler(this.OnDecks), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) MedalInfo.PacketID.ID, new Network.NetHandler(this.OnMedalInfo), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) ProfileProgress.PacketID.ID, new Network.NetHandler(this.OnProfileProgress), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) GamesInfo.PacketID.ID, new Network.NetHandler(this.OnGamesInfo), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusUtil.ProfileNotices.PacketID.ID, new Network.NetHandler(this.OnProfileNotices), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) ClientOptions.PacketID.ID, new Network.NetHandler(this.OnClientOptions), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) Disconnected.PacketID.ID, new Network.NetHandler(this.OnLastGameInfo), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) CardValues.PacketID.ID, new Network.NetHandler(this.OnCardValues), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) ArcaneDustBalance.PacketID.ID, new Network.NetHandler(this.OnArcaneDustBalance), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) GoldBalance.PacketID.ID, new Network.NetHandler(this.OnGoldBalance), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) GuardianVars.PacketID.ID, new Network.NetHandler(this.OnFeaturesChanged), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PlayerRecords.PacketID.ID, new Network.NetHandler(this.OnPlayerRecords), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) RewardProgress.PacketID.ID, new Network.NetHandler(this.OnRewardProgress), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) HeroXP.PacketID.ID, new Network.NetHandler(this.OnAllHeroXP), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PlayQueue.PacketID.ID, new Network.NetHandler(this.OnPlayQueue), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) CardBacks.PacketID.ID, new Network.NetHandler(this.OnCardBacks), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) NotSoMassiveLoginReply.PacketID.ID, new Network.NetHandler(this.OnNotSoMassiveLoginReply), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) TavernBrawlInfo.PacketID.ID, new Network.NetHandler(this.OnTavernBrawlInfoResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) TavernBrawlPlayerRecordResponse.PacketID.ID, new Network.NetHandler(this.OnTavernBrawlRecordResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) FavoriteHeroesResponse.PacketID.ID, new Network.NetHandler(this.OnFavoriteHeroesResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) AccountLicensesInfoResponse.PacketID.ID, new Network.NetHandler(this.OnAccountLicensesInfoResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) Deadend.PacketID.ID, new Network.NetHandler(this.OnHearthstoneUnavailableGame), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) DeadendUtil.PacketID.ID, new Network.NetHandler(this.OnHearthstoneUnavailableUtil), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) NoticeNotifications.PacketID.ID, new Network.NetHandler(this.OnProfileNoticeNotifications), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) ClientStaticAssetsResponse.PacketID.ID, new Network.NetHandler(this.OnClientStaticAssetsResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) TavernBrawlRequestSessionBeginResponse.PacketID.ID, new Network.NetHandler(this.OnTavernBrawlRequestSessionBeginResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) TavernBrawlRequestSessionRetireResponse.PacketID.ID, new Network.NetHandler(this.OnTavernBrawlRequestSessionRetireResponse), (Network.TimeoutHandler) null);
  }

  public void InitNetCache()
  {
    ApplicationMgr.Get().WillReset += new System.Action(NetCache.NetCache_WillReset);
    Network.RegisterThrottledPacketListener(new Network.ThrottledPacketListener(this.OnPacketThrottled));
    this.RegisterNetCacheHandlers();
  }

  public static NetCache Get()
  {
    if (NetCache.s_instance == null)
      Debug.LogError((object) "no NetCache object");
    return NetCache.s_instance;
  }

  private static void NetCache_WillReset()
  {
    NetCache.NetCacheClientOptions netObject = NetCache.Get().GetNetObject<NetCache.NetCacheClientOptions>();
    if (netObject == null)
      return;
    netObject.DispatchClientOptionsToServer();
  }

  public void RegisterCollectionManager(NetCache.NetCacheCallback callback)
  {
    this.RegisterCollectionManager(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterCollectionManager(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterCollectionManager---");
    NetCache.NetCacheBatchRequest request = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterCollectionManager));
    this.AddCollectionManagerToRequest(ref request);
    this.NetCacheMakeBatchRequest(request);
  }

  public void RegisterScreenCollectionManager(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenCollectionManager(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenCollectionManager(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenCollectionManager---");
    NetCache.NetCacheBatchRequest request = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenCollectionManager));
    this.AddCollectionManagerToRequest(ref request);
    this.AddRandomDeckMakerToRequest(ref request);
    request.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheFeatures), false),
      new NetCache.Request(typeof (NetCache.NetCacheHeroLevels), false)
    });
    this.NetCacheMakeBatchRequest(request);
  }

  public void RegisterScreenForge(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenForge(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenForge(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenForge---");
    NetCache.NetCacheBatchRequest request = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenForge));
    this.AddCollectionManagerToRequest(ref request);
    request.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheFeatures), false),
      new NetCache.Request(typeof (NetCache.NetCacheHeroLevels), false)
    });
    this.NetCacheMakeBatchRequest(request);
  }

  public void RegisterScreenTourneys(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenTourneys(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenTourneys(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenTourneys---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenTourneys));
    batchRequest.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheMedalInfo), true),
      new NetCache.Request(typeof (NetCache.NetCachePlayerRecords), false),
      new NetCache.Request(typeof (NetCache.NetCacheDecks), false),
      new NetCache.Request(typeof (NetCache.NetCacheFeatures), false),
      new NetCache.Request(typeof (NetCache.NetCacheHeroLevels), false),
      new NetCache.Request(typeof (NetCache.NetCachePlayQueue), true)
    });
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterScreenFriendly(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenFriendly(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenFriendly(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenFriendly---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenFriendly));
    batchRequest.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheDecks), false),
      new NetCache.Request(typeof (NetCache.NetCacheHeroLevels), false)
    });
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterScreenPractice(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenPractice(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenPractice(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenPractice---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenPractice));
    batchRequest.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheDecks), false),
      new NetCache.Request(typeof (NetCache.NetCacheFeatures), false),
      new NetCache.Request(typeof (NetCache.NetCacheHeroLevels), false),
      new NetCache.Request(typeof (NetCache.NetCacheRewardProgress), false)
    });
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterScreenEndOfGame(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenEndOfGame(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenEndOfGame(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenEndOfGame---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenEndOfGame));
    batchRequest.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheRewardProgress), false),
      new NetCache.Request(typeof (NetCache.NetCacheMedalInfo), true),
      new NetCache.Request(typeof (NetCache.NetCacheGamesPlayed), true),
      new NetCache.Request(typeof (NetCache.NetCachePlayerRecords), true),
      new NetCache.Request(typeof (NetCache.NetCacheHeroLevels), true)
    });
    if (GameMgr.Get() != null && GameMgr.Get().GetGameType() == GameType.GT_TAVERNBRAWL)
      batchRequest.AddRequest(new NetCache.Request(typeof (NetCache.NetCacheTavernBrawlRecord), true));
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterScreenPackOpening(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenPackOpening(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenPackOpening(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenPackOpening---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenPackOpening));
    batchRequest.AddRequest(new NetCache.Request(typeof (NetCache.NetCacheBoosters), false));
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterScreenBox(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenBox(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenBox(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenBox---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenBox));
    batchRequest.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheBoosters), false),
      new NetCache.Request(typeof (NetCache.NetCacheClientOptions), false),
      new NetCache.Request(typeof (NetCache.NetCacheProfileProgress), false),
      new NetCache.Request(typeof (NetCache.NetCacheFeatures), false),
      new NetCache.Request(typeof (NetCache.NetCacheMedalInfo), false)
    });
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterScreenQuestLog(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenQuestLog(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenQuestLog(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenQuestLog---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenQuestLog));
    batchRequest.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheMedalInfo), false),
      new NetCache.Request(typeof (NetCache.NetCacheHeroLevels), false),
      new NetCache.Request(typeof (NetCache.NetCachePlayerRecords), false),
      new NetCache.Request(typeof (NetCache.NetCacheProfileProgress), true)
    });
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterFeatures(NetCache.NetCacheCallback callback)
  {
    this.RegisterFeatures(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterFeatures(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterFeatures---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterFeatures));
    batchRequest.AddRequest(new NetCache.Request(typeof (NetCache.NetCacheFeatures), false));
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterScreenLogin(NetCache.NetCacheCallback callback)
  {
    this.RegisterScreenLogin(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterScreenLogin(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterScreenLogin---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterScreenLogin));
    batchRequest.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheNotSoMassiveLogin), false),
      new NetCache.Request(typeof (NetCache.NetCacheProfileProgress), false),
      new NetCache.Request(typeof (NetCache.NetCacheRewardProgress), false),
      new NetCache.Request(typeof (NetCache.NetCachePlayerRecords), false),
      new NetCache.Request(typeof (NetCache.NetCacheGoldBalance), false),
      new NetCache.Request(typeof (NetCache.NetCacheHeroLevels), false),
      new NetCache.Request(typeof (NetCache.NetCacheCardBacks), false),
      new NetCache.Request(typeof (NetCache.NetCacheFavoriteHeroes), false),
      new NetCache.Request(typeof (NetCache.NetCacheAccountLicenses), false),
      new NetCache.Request(typeof (ClientStaticAssetsResponse), false),
      new NetCache.Request(typeof (NetCache.NetCacheClientOptions), true)
    });
    this.NetCacheMakeBatchRequest(batchRequest);
    TavernBrawlManager.Get().IsRefreshingTavernBrawlInfo = true;
  }

  public void RegisterReconnectMgr(NetCache.NetCacheCallback callback)
  {
    this.RegisterReconnectMgr(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterReconnectMgr(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterReconnectMgr---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterReconnectMgr));
    batchRequest.AddRequest(new NetCache.Request(typeof (NetCache.NetCacheDisconnectedGame), false));
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterTutorialEndGameScreen(NetCache.NetCacheCallback callback)
  {
    this.RegisterTutorialEndGameScreen(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterTutorialEndGameScreen(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterTutorialEndGameScreen---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterTutorialEndGameScreen));
    batchRequest.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheProfileProgress), false)
    });
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterFriendChallenge(NetCache.NetCacheCallback callback)
  {
    this.RegisterFriendChallenge(callback, new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  public void RegisterFriendChallenge(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterFriendChallenge---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterFriendChallenge));
    batchRequest.AddRequest(new NetCache.Request(typeof (NetCache.NetCacheProfileProgress), false));
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  public void RegisterNotices(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback)
  {
    Log.Bob.Print("---RegisterFeatures---");
    NetCache.NetCacheBatchRequest batchRequest = new NetCache.NetCacheBatchRequest(callback, errorCallback, new NetCache.RequestFunc(this.RegisterNotices));
    batchRequest.AddRequest(new NetCache.Request(typeof (NetCache.NetCacheProfileNotices), false));
    this.NetCacheMakeBatchRequest(batchRequest);
  }

  private void AddCollectionManagerToRequest(ref NetCache.NetCacheBatchRequest request)
  {
    request.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheDecks), false),
      new NetCache.Request(typeof (NetCache.NetCacheCollection), false),
      new NetCache.Request(typeof (NetCache.NetCacheCardValues), false),
      new NetCache.Request(typeof (NetCache.NetCacheArcaneDustBalance), false),
      new NetCache.Request(typeof (NetCache.NetCacheClientOptions), false)
    });
  }

  private void AddRandomDeckMakerToRequest(ref NetCache.NetCacheBatchRequest request)
  {
    request.AddRequests(new List<NetCache.Request>()
    {
      new NetCache.Request(typeof (NetCache.NetCacheCollection), false)
    });
  }

  private void OnPacketThrottled(int packetID, long retryMillis)
  {
    if (packetID != 201)
      return;
    DateTime dateTime = DateTime.Now;
    dateTime = dateTime.AddMilliseconds((double) retryMillis);
    using (List<NetCache.NetCacheBatchRequest>.Enumerator enumerator = this.m_cacheRequests.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.m_timeAdded = dateTime;
    }
  }

  public void Cheat_AddNotice(NetCache.ProfileNotice notice)
  {
    if (!ApplicationMgr.IsInternal())
      return;
    this.UnloadNetObject<NetCache.NetCacheProfileNotices>();
    PopupDisplayManager.Get().ClearSeenNotices();
    notice.NoticeID = 9999L;
    this.m_ackedNotices.Remove(notice.NoticeID);
    this.HandleIncomingProfileNotices(new List<NetCache.ProfileNotice>()
    {
      notice
    }, false);
  }

  public class NetCacheGamesPlayed
  {
    public int GamesStarted { get; set; }

    public int GamesWon { get; set; }

    public int GamesLost { get; set; }

    public int FreeRewardProgress { get; set; }
  }

  public class NetCacheFeatures
  {
    public bool CaisEnabledNonMobile;
    public bool CaisEnabledMobileChina;
    public bool CaisEnabledMobileSouthKorea;
    public bool SendTelemetryPresence;

    public NetCache.NetCacheFeatures.CacheMisc Misc { get; set; }

    public NetCache.NetCacheFeatures.CacheGames Games { get; set; }

    public NetCache.NetCacheFeatures.CacheCollection Collection { get; set; }

    public NetCache.NetCacheFeatures.CacheStore Store { get; set; }

    public NetCache.NetCacheFeatures.CacheHeroes Heroes { get; set; }

    public NetCacheFeatures()
    {
      this.Misc = new NetCache.NetCacheFeatures.CacheMisc();
      this.Games = new NetCache.NetCacheFeatures.CacheGames();
      this.Collection = new NetCache.NetCacheFeatures.CacheCollection();
      this.Store = new NetCache.NetCacheFeatures.CacheStore();
      this.Heroes = new NetCache.NetCacheFeatures.CacheHeroes();
    }

    public class CacheMisc
    {
      public int ClientOptionsUpdateIntervalSeconds { get; set; }
    }

    public class CacheGames
    {
      public bool Tournament { get; set; }

      public bool Practice { get; set; }

      public bool Casual { get; set; }

      public bool Forge { get; set; }

      public bool Friendly { get; set; }

      public bool TavernBrawl { get; set; }

      public int ShowUserUI { get; set; }
    }

    public class CacheCollection
    {
      public bool Manager { get; set; }

      public bool Crafting { get; set; }
    }

    public class CacheStore
    {
      public bool Store { get; set; }

      public bool BattlePay { get; set; }

      public bool BuyWithGold { get; set; }
    }

    public class CacheHeroes
    {
      public bool Hunter { get; set; }

      public bool Mage { get; set; }

      public bool Paladin { get; set; }

      public bool Priest { get; set; }

      public bool Rogue { get; set; }

      public bool Shaman { get; set; }

      public bool Warlock { get; set; }

      public bool Warrior { get; set; }
    }
  }

  public class NetCacheArcaneDustBalance
  {
    public long Balance { get; set; }
  }

  public class NetCacheGoldBalance
  {
    public long CappedBalance { get; set; }

    public long BonusBalance { get; set; }

    public long Cap { get; set; }

    public long CapWarning { get; set; }

    public long GetTotal()
    {
      return this.CappedBalance + this.BonusBalance;
    }
  }

  public class NetCacheNotSoMassiveLogin
  {
    public NotSoMassiveLoginReply Packet { get; set; }
  }

  public class NetCacheSubscribeResponse
  {
    public ulong FeaturesSupported;
    public ulong Route;
    public ulong KeepAliveDelay;
    public ulong RequestMaxWaitSecs;
  }

  public class NetCachePlayQueue
  {
    public BnetGameType GameType { get; set; }
  }

  public class HeroLevel
  {
    public TAG_CLASS Class { get; set; }

    public NetCache.HeroLevel.LevelInfo PrevLevel { get; set; }

    public NetCache.HeroLevel.LevelInfo CurrentLevel { get; set; }

    public NetCache.HeroLevel.NextLevelReward NextReward { get; set; }

    public HeroLevel()
    {
      this.Class = TAG_CLASS.INVALID;
      this.PrevLevel = (NetCache.HeroLevel.LevelInfo) null;
      this.CurrentLevel = new NetCache.HeroLevel.LevelInfo();
      this.NextReward = (NetCache.HeroLevel.NextLevelReward) null;
    }

    public override string ToString()
    {
      return string.Format("[HeroLevel: Class={0}, PrevLevel={1}, CurrentLevel={2}, NextReward={3}]", (object) this.Class, (object) this.PrevLevel, (object) this.CurrentLevel, (object) this.NextReward);
    }

    public class NextLevelReward
    {
      public int Level { get; set; }

      public RewardData Reward { get; set; }

      public NextLevelReward()
      {
        this.Level = 0;
        this.Reward = (RewardData) null;
      }

      public override string ToString()
      {
        return string.Format("[NextLevelReward: Level={0}, Reward={1}]", (object) this.Level, (object) this.Reward);
      }
    }

    public class LevelInfo
    {
      public int Level { get; set; }

      public int MaxLevel { get; set; }

      public long XP { get; set; }

      public long MaxXP { get; set; }

      public LevelInfo()
      {
        this.Level = 0;
        this.MaxLevel = 60;
        this.XP = 0L;
        this.MaxXP = 0L;
      }

      public bool IsMaxLevel()
      {
        return this.Level == this.MaxLevel;
      }

      public override string ToString()
      {
        return string.Format("[LevelInfo: Level={0}, XP={1}, MaxXP={2}]", (object) this.Level, (object) this.XP, (object) this.MaxXP);
      }
    }
  }

  public class NetCacheHeroLevels
  {
    public List<NetCache.HeroLevel> Levels { get; set; }

    public NetCacheHeroLevels()
    {
      this.Levels = new List<NetCache.HeroLevel>();
    }

    public override string ToString()
    {
      string str = "[START NetCacheHeroLevels]\n";
      using (List<NetCache.HeroLevel>.Enumerator enumerator = this.Levels.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          NetCache.HeroLevel current = enumerator.Current;
          str += string.Format("{0}\n", (object) current);
        }
      }
      return str + "[END NetCacheHeroLevels]";
    }
  }

  public class NetCacheProfileProgress
  {
    public TutorialProgress CampaignProgress { get; set; }

    public int BestForgeWins { get; set; }

    public long LastForgeDate { get; set; }

    public int DisplayBanner { get; set; }
  }

  public class NetCacheCardBacks
  {
    public int DefaultCardBack { get; set; }

    public HashSet<int> CardBacks { get; set; }

    public NetCacheCardBacks()
    {
      this.CardBacks = new HashSet<int>();
      this.CardBacks.Add(0);
    }
  }

  public class BoosterStack
  {
    public int Id { get; set; }

    public int Count { get; set; }
  }

  public class NetCacheBoosters
  {
    public List<NetCache.BoosterStack> BoosterStacks { get; set; }

    public NetCacheBoosters()
    {
      this.BoosterStacks = new List<NetCache.BoosterStack>();
    }

    public NetCache.BoosterStack GetBoosterStack(int id)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      return this.BoosterStacks.Find(new Predicate<NetCache.BoosterStack>(new NetCache.NetCacheBoosters.\u003CGetBoosterStack\u003Ec__AnonStorey3CA() { id = id }.\u003C\u003Em__16E));
    }

    public int GetTotalNumBoosters()
    {
      int num = 0;
      using (List<NetCache.BoosterStack>.Enumerator enumerator = this.BoosterStacks.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          NetCache.BoosterStack current = enumerator.Current;
          num += current.Count;
        }
      }
      return num;
    }
  }

  public class DeckHeader
  {
    public long ID { get; set; }

    public string Name { get; set; }

    public int CardBack { get; set; }

    public string Hero { get; set; }

    public TAG_PREMIUM HeroPremium { get; set; }

    public string HeroPower { get; set; }

    public DeckType Type { get; set; }

    public bool CardBackOverridden { get; set; }

    public bool HeroOverridden { get; set; }

    public int SeasonId { get; set; }

    public bool NeedsName { get; set; }

    public long SortOrder { get; set; }

    public bool IsWild { get; set; }

    public bool Locked { get; set; }

    public DeckSourceType SourceType { get; set; }

    public DateTime? CreateDate { get; set; }

    public DateTime? LastModified { get; set; }

    public override string ToString()
    {
      return string.Format("[DeckHeader: ID={0} Name={1} Hero={2} HeroPremium={3} HeroPower={4} DeckType={5} CardBack={6} CardBackOverridden={7} HeroOverridden={8} NeedsName={9} SortOrder={10} SourceType={11} LastModified={12} CreateDate={13}]", (object) this.ID, (object) this.Name, (object) this.Hero, (object) this.HeroPremium, (object) this.HeroPower, (object) this.Type, (object) this.CardBack, (object) this.CardBackOverridden, (object) this.HeroOverridden, (object) this.NeedsName, (object) this.SortOrder, (object) this.SourceType, (object) this.CreateDate, (object) this.LastModified);
    }
  }

  public class NetCacheDecks
  {
    public List<NetCache.DeckHeader> Decks { get; set; }

    public NetCacheDecks()
    {
      this.Decks = new List<NetCache.DeckHeader>();
    }
  }

  public class CardDefinition
  {
    public string Name { get; set; }

    public TAG_PREMIUM Premium { get; set; }

    public override bool Equals(object obj)
    {
      NetCache.CardDefinition cardDefinition = obj as NetCache.CardDefinition;
      if (cardDefinition != null && this.Premium == cardDefinition.Premium)
        return this.Name.Equals(cardDefinition.Name);
      return false;
    }

    public override int GetHashCode()
    {
      return (int) (this.Name.GetHashCode() + this.Premium);
    }

    public override string ToString()
    {
      return string.Format("[CardDefinition: Name={0}, Premium={1}]", (object) this.Name, (object) this.Premium);
    }
  }

  public class CardValue
  {
    public int Buy { get; set; }

    public int Sell { get; set; }

    public bool Nerfed { get; set; }
  }

  public class NetCacheCardValues
  {
    public int CardNerfIndex { get; set; }

    public Map<NetCache.CardDefinition, NetCache.CardValue> Values { get; set; }

    public NetCacheCardValues()
    {
      this.CardNerfIndex = 0;
      this.Values = new Map<NetCache.CardDefinition, NetCache.CardValue>();
    }
  }

  public class NetCacheDisconnectedGame
  {
    public GameServerInfo ServerInfo { get; set; }
  }

  public class BoosterCard
  {
    public NetCache.CardDefinition Def { get; set; }

    public long Date { get; set; }

    public BoosterCard()
    {
      this.Def = new NetCache.CardDefinition();
    }
  }

  public class CardStack
  {
    public NetCache.CardDefinition Def { get; set; }

    public long Date { get; set; }

    public int Count { get; set; }

    public int NumSeen { get; set; }

    public CardStack()
    {
      this.Def = new NetCache.CardDefinition();
    }
  }

  public class NetCacheCollection
  {
    public Map<TAG_CLASS, int> BasicCardsUnlockedPerClass = new Map<TAG_CLASS, int>();
    public int TotalCardsOwned;

    public List<NetCache.CardStack> Stacks { get; set; }

    public NetCacheCollection()
    {
      this.Stacks = new List<NetCache.CardStack>();
      foreach (int num in Enum.GetValues(typeof (TAG_CLASS)))
        this.BasicCardsUnlockedPerClass[(TAG_CLASS) num] = 0;
    }
  }

  public class PlayerRecord
  {
    public GameType RecordType { get; set; }

    public int Data { get; set; }

    public int Wins { get; set; }

    public int Losses { get; set; }

    public int Ties { get; set; }
  }

  public class NetCachePlayerRecords
  {
    public List<NetCache.PlayerRecord> Records { get; set; }

    public NetCachePlayerRecords()
    {
      this.Records = new List<NetCache.PlayerRecord>();
    }
  }

  public class NetCacheRewardProgress
  {
    public int Season { get; set; }

    public long SeasonEndDate { get; set; }

    public int WinsPerGold { get; set; }

    public int GoldPerReward { get; set; }

    public int MaxGoldPerDay { get; set; }

    public int PackRewardId { get; set; }

    public int XPSoloLimit { get; set; }

    public int MaxHeroLevel { get; set; }

    public long NextQuestCancelDate { get; set; }

    public float SpecialEventTimingMod { get; set; }

    public int FriendWeekConcederMaxDefense { get; set; }
  }

  public class NetCacheMedalInfo
  {
    public MedalInfoData Standard { get; set; }

    public MedalInfoData Wild { get; set; }

    public NetCache.NetCacheMedalInfo PreviousMedalInfo { get; set; }

    public NetCache.NetCacheMedalInfo Clone()
    {
      return new NetCache.NetCacheMedalInfo() { Standard = NetCache.NetCacheMedalInfo.CloneMedalInfoData(this.Standard), Wild = NetCache.NetCacheMedalInfo.CloneMedalInfoData(this.Wild) };
    }

    public static MedalInfoData CloneMedalInfoData(MedalInfoData original)
    {
      return new MedalInfoData() { SeasonWins = original.SeasonWins, Stars = original.Stars, Streak = original.Streak, StarLevel = original.StarLevel, LevelStart = original.LevelStart, LevelEnd = original.LevelEnd, CanLoseLevel = original.CanLoseLevel, HasLegendRank = original.HasLegendRank, LegendRank = original.LegendRank, HasBestStarLevel = original.HasBestStarLevel, BestStarLevel = original.BestStarLevel, HasCanLoseStars = original.HasCanLoseStars, CanLoseStars = original.CanLoseStars };
    }

    public override string ToString()
    {
      return string.Format("[NetCacheMedalInfo] \n Standard={0} \n Wild={1}", (object) this.Standard, (object) this.Wild);
    }
  }

  public abstract class ProfileNotice
  {
    private NetCache.ProfileNotice.NoticeType m_type;

    public long NoticeID { get; set; }

    public NetCache.ProfileNotice.NoticeType Type
    {
      get
      {
        return this.m_type;
      }
    }

    public NetCache.ProfileNotice.NoticeOrigin Origin { get; set; }

    public long OriginData { get; set; }

    public long Date { get; set; }

    protected ProfileNotice(NetCache.ProfileNotice.NoticeType init)
    {
      this.m_type = init;
      this.NoticeID = 0L;
      this.Origin = NetCache.ProfileNotice.NoticeOrigin.UNKNOWN;
      this.OriginData = 0L;
      this.Date = 0L;
    }

    public override string ToString()
    {
      return string.Format("[{0}: NoticeID={1}, Type={2}, Origin={3}, OriginData={4}, Date={5}]", (object) this.GetType(), (object) this.NoticeID, (object) this.Type, (object) this.Origin, (object) this.OriginData, (object) this.Date);
    }

    public enum NoticeType
    {
      GAINED_MEDAL = 1,
      REWARD_BOOSTER = 2,
      REWARD_CARD = 3,
      DISCONNECTED_GAME = 4,
      PRECON_DECK = 5,
      REWARD_DUST = 6,
      REWARD_MOUNT = 7,
      REWARD_FORGE = 8,
      REWARD_GOLD = 9,
      PURCHASE = 10,
      REWARD_CARD_BACK = 11,
      BONUS_STARS = 12,
      ADVENTURE_PROGRESS = 14,
      HERO_LEVEL_UP = 15,
      ACCOUNT_LICENSE = 16,
      TAVERN_BRAWL_REWARDS = 17,
      TAVERN_BRAWL_TICKET = 18,
    }

    public enum NoticeOrigin
    {
      UNKNOWN = -1,
      SEASON = 1,
      BETA_REIMBURSE = 2,
      FORGE = 3,
      TOURNEY = 4,
      PRECON_DECK = 5,
      ACK = 6,
      ACHIEVEMENT = 7,
      LEVEL_UP = 8,
      PURCHASE_COMPLETE = 10,
      PURCHASE_FAILED = 11,
      PURCHASE_CANCELED = 12,
      BLIZZCON = 13,
      EVENT = 14,
      DISCONNECTED_GAME = 15,
      OUT_OF_BAND_LICENSE = 16,
      IGR = 17,
      ADVENTURE_PROGRESS = 18,
      ADVENTURE_FLAGS = 19,
      TAVERN_BRAWL_REWARD = 20,
      ACCOUNT_LICENSE_FLAGS = 21,
      FROM_PURCHASE = 22,
    }
  }

  public class ProfileNoticeMedal : NetCache.ProfileNotice
  {
    public int StarLevel { get; set; }

    public int LegendRank { get; set; }

    public int BestStarLevel { get; set; }

    public bool IsWild { get; set; }

    public Network.RewardChest Chest { get; set; }

    public ProfileNoticeMedal()
      : base(NetCache.ProfileNotice.NoticeType.GAINED_MEDAL)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [StarLevel={1}, LegendRank={2}, BestStarLevel={3}, IsWild={4}, Chest={5}]", (object) base.ToString(), (object) this.StarLevel, (object) this.LegendRank, (object) this.BestStarLevel, (object) this.IsWild, (object) this.Chest);
    }
  }

  public class ProfileNoticeRewardBooster : NetCache.ProfileNotice
  {
    public int Id { get; set; }

    public int Count { get; set; }

    public ProfileNoticeRewardBooster()
      : base(NetCache.ProfileNotice.NoticeType.REWARD_BOOSTER)
    {
      this.Id = 0;
      this.Count = 0;
    }

    public override string ToString()
    {
      return string.Format("{0} [Id={1}, Count={2}]", (object) base.ToString(), (object) this.Id, (object) this.Count);
    }
  }

  public class ProfileNoticeRewardCard : NetCache.ProfileNotice
  {
    public string CardID { get; set; }

    public TAG_PREMIUM Premium { get; set; }

    public int Quantity { get; set; }

    public ProfileNoticeRewardCard()
      : base(NetCache.ProfileNotice.NoticeType.REWARD_CARD)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [CardID={1}, Premium={2}, Quantity={3}]", (object) base.ToString(), (object) this.CardID, (object) this.Premium, (object) this.Quantity);
    }
  }

  public class ProfileNoticePreconDeck : NetCache.ProfileNotice
  {
    public long DeckID { get; set; }

    public int HeroAsset { get; set; }

    public ProfileNoticePreconDeck()
      : base(NetCache.ProfileNotice.NoticeType.PRECON_DECK)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [DeckID={1}, HeroAsset={2}]", (object) base.ToString(), (object) this.DeckID, (object) this.HeroAsset);
    }
  }

  public class ProfileNoticeRewardDust : NetCache.ProfileNotice
  {
    public int Amount { get; set; }

    public ProfileNoticeRewardDust()
      : base(NetCache.ProfileNotice.NoticeType.REWARD_DUST)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [Amount={1}]", (object) base.ToString(), (object) this.Amount);
    }
  }

  public class ProfileNoticeRewardMount : NetCache.ProfileNotice
  {
    public int MountID { get; set; }

    public ProfileNoticeRewardMount()
      : base(NetCache.ProfileNotice.NoticeType.REWARD_MOUNT)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [MountID={1}]", (object) base.ToString(), (object) this.MountID);
    }
  }

  public class ProfileNoticeRewardForge : NetCache.ProfileNotice
  {
    public int Quantity { get; set; }

    public ProfileNoticeRewardForge()
      : base(NetCache.ProfileNotice.NoticeType.REWARD_FORGE)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [Quantity={1}]", (object) base.ToString(), (object) this.Quantity);
    }
  }

  public class ProfileNoticeRewardGold : NetCache.ProfileNotice
  {
    public int Amount { get; set; }

    public ProfileNoticeRewardGold()
      : base(NetCache.ProfileNotice.NoticeType.REWARD_GOLD)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [Amount={1}]", (object) base.ToString(), (object) this.Amount);
    }
  }

  public class ProfileNoticePurchase : NetCache.ProfileNotice
  {
    public string ProductID { get; set; }

    public Currency CurrencyType { get; set; }

    public long Data { get; set; }

    public ProfileNoticePurchase()
      : base(NetCache.ProfileNotice.NoticeType.PURCHASE)
    {
    }

    public override string ToString()
    {
      return string.Format("[ProfileNoticePurchase: NoticeID={0}, Type={1}, Origin={2}, OriginData={3}, Date={4} ProductID='{5}', Data={6} Currency={7}]", (object) this.NoticeID, (object) this.Type, (object) this.Origin, (object) this.OriginData, (object) this.Date, (object) this.ProductID, (object) this.Data, (object) this.CurrencyType);
    }
  }

  public class ProfileNoticeRewardCardBack : NetCache.ProfileNotice
  {
    public int CardBackID { get; set; }

    public ProfileNoticeRewardCardBack()
      : base(NetCache.ProfileNotice.NoticeType.REWARD_CARD_BACK)
    {
    }

    public override string ToString()
    {
      return string.Format("[ProfileNoticePurchase: NoticeID={0}, Type={1}, Origin={2}, OriginData={3}, Date={4} CardBackID={5}]", (object) this.NoticeID, (object) this.Type, (object) this.Origin, (object) this.OriginData, (object) this.Date, (object) this.CardBackID);
    }
  }

  public class ProfileNoticeBonusStars : NetCache.ProfileNotice
  {
    public int StarLevel { get; set; }

    public int Stars { get; set; }

    public ProfileNoticeBonusStars()
      : base(NetCache.ProfileNotice.NoticeType.BONUS_STARS)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [StarLevel={1}, Stars={2}]", (object) base.ToString(), (object) this.StarLevel, (object) this.Stars);
    }
  }

  public class ProfileNoticeDisconnectedGame : NetCache.ProfileNotice
  {
    public GameType GameType { get; set; }

    public FormatType FormatType { get; set; }

    public int MissionId { get; set; }

    public ProfileNoticeDisconnectedGameResult.GameResult GameResult { get; set; }

    public ProfileNoticeDisconnectedGameResult.PlayerResult YourResult { get; set; }

    public ProfileNoticeDisconnectedGameResult.PlayerResult OpponentResult { get; set; }

    public int PlayerIndex { get; set; }

    public ProfileNoticeDisconnectedGame()
      : base(NetCache.ProfileNotice.NoticeType.DISCONNECTED_GAME)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [GameType={1}, FormatType={2}, MissionId={3} GameResult={4}, YourResult={5}, OpponentResult={6}, PlayerIndex={7}]", (object) base.ToString(), (object) this.GameType, (object) this.FormatType, (object) this.MissionId, (object) this.GameResult, (object) this.YourResult, (object) this.OpponentResult, (object) this.PlayerIndex);
    }
  }

  public class ProfileNoticeAdventureProgress : NetCache.ProfileNotice
  {
    public int Wing { get; set; }

    public int? Progress { get; set; }

    public ulong? Flags { get; set; }

    public ProfileNoticeAdventureProgress()
      : base(NetCache.ProfileNotice.NoticeType.ADVENTURE_PROGRESS)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [Wing={1}, Progress={2}, Flags={3}]", (object) base.ToString(), (object) this.Wing, (object) this.Progress, (object) this.Flags);
    }
  }

  public class ProfileNoticeLevelUp : NetCache.ProfileNotice
  {
    public int HeroClass { get; set; }

    public int NewLevel { get; set; }

    public ProfileNoticeLevelUp()
      : base(NetCache.ProfileNotice.NoticeType.HERO_LEVEL_UP)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [HeroClass={1}, NewLevel={2}", (object) base.ToString(), (object) this.HeroClass, (object) this.NewLevel);
    }
  }

  public class ProfileNoticeAcccountLicense : NetCache.ProfileNotice
  {
    public long License { get; set; }

    public long CasID { get; set; }

    public ProfileNoticeAcccountLicense()
      : base(NetCache.ProfileNotice.NoticeType.ACCOUNT_LICENSE)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [License={1}, CasID={2}", (object) base.ToString(), (object) this.License, (object) this.CasID);
    }
  }

  public class ProfileNoticeTavernBrawlRewards : NetCache.ProfileNotice
  {
    public PegasusShared.RewardChest Chest { get; set; }

    public int Wins { get; set; }

    public ProfileNoticeTavernBrawlRewards()
      : base(NetCache.ProfileNotice.NoticeType.TAVERN_BRAWL_REWARDS)
    {
    }

    public override string ToString()
    {
      return string.Format("{0} [Chest={1}, Wins={2}", (object) base.ToString(), (object) this.Chest, (object) this.Wins);
    }
  }

  public class ProfileNoticeTavernBrawlTicket : NetCache.ProfileNotice
  {
    public int TicketType { get; set; }

    public int Quantity { get; set; }

    public ProfileNoticeTavernBrawlTicket()
      : base(NetCache.ProfileNotice.NoticeType.TAVERN_BRAWL_TICKET)
    {
    }
  }

  public class NetCacheProfileNotices
  {
    public List<NetCache.ProfileNotice> Notices { get; set; }

    public NetCacheProfileNotices()
    {
      this.Notices = new List<NetCache.ProfileNotice>();
    }
  }

  public abstract class ClientOptionBase : ICloneable
  {
    public abstract void PopulateIntoPacket(ServerOption type, SetOptions packet);

    public override bool Equals(object other)
    {
      return other != null && other.GetType() == this.GetType();
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public object Clone()
    {
      return this.MemberwiseClone();
    }
  }

  public class ClientOptionInt : NetCache.ClientOptionBase
  {
    public int OptionValue { get; set; }

    public ClientOptionInt(int val)
    {
      this.OptionValue = val;
    }

    public override void PopulateIntoPacket(ServerOption type, SetOptions packet)
    {
      packet.Options.Add(new PegasusUtil.ClientOption()
      {
        Index = (int) type,
        AsInt32 = this.OptionValue
      });
    }

    public override bool Equals(object other)
    {
      return base.Equals(other) && ((NetCache.ClientOptionInt) other).OptionValue == this.OptionValue;
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }

  public class ClientOptionLong : NetCache.ClientOptionBase
  {
    public long OptionValue { get; set; }

    public ClientOptionLong(long val)
    {
      this.OptionValue = val;
    }

    public override void PopulateIntoPacket(ServerOption type, SetOptions packet)
    {
      packet.Options.Add(new PegasusUtil.ClientOption()
      {
        Index = (int) type,
        AsInt64 = this.OptionValue
      });
    }

    public override bool Equals(object other)
    {
      return base.Equals(other) && ((NetCache.ClientOptionLong) other).OptionValue == this.OptionValue;
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }

  public class ClientOptionFloat : NetCache.ClientOptionBase
  {
    public float OptionValue { get; set; }

    public ClientOptionFloat(float val)
    {
      this.OptionValue = val;
    }

    public override void PopulateIntoPacket(ServerOption type, SetOptions packet)
    {
      packet.Options.Add(new PegasusUtil.ClientOption()
      {
        Index = (int) type,
        AsFloat = this.OptionValue
      });
    }

    public override bool Equals(object other)
    {
      return base.Equals(other) && (double) ((NetCache.ClientOptionFloat) other).OptionValue == (double) this.OptionValue;
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }

  public class ClientOptionULong : NetCache.ClientOptionBase
  {
    public ulong OptionValue { get; set; }

    public ClientOptionULong(ulong val)
    {
      this.OptionValue = val;
    }

    public override void PopulateIntoPacket(ServerOption type, SetOptions packet)
    {
      packet.Options.Add(new PegasusUtil.ClientOption()
      {
        Index = (int) type,
        AsUint64 = this.OptionValue
      });
    }

    public override bool Equals(object other)
    {
      return base.Equals(other) && (long) ((NetCache.ClientOptionULong) other).OptionValue == (long) this.OptionValue;
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }

  public class NetCacheClientOptions
  {
    public Map<ServerOption, NetCache.ClientOptionBase> ClientState { get; set; }

    public Map<ServerOption, NetCache.ClientOptionBase> ServerState { get; set; }

    public NetCacheClientOptions()
    {
      this.ClientState = new Map<ServerOption, NetCache.ClientOptionBase>();
      this.ServerState = new Map<ServerOption, NetCache.ClientOptionBase>();
    }

    public void UpdateServerState()
    {
      using (Map<ServerOption, NetCache.ClientOptionBase>.Enumerator enumerator = this.ClientState.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ServerOption, NetCache.ClientOptionBase> current = enumerator.Current;
          this.ServerState[current.Key] = current.Value == null ? (NetCache.ClientOptionBase) null : (NetCache.ClientOptionBase) current.Value.Clone();
        }
      }
    }

    public void OnUpdateIntervalElasped(object userData)
    {
      ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.OnUpdateIntervalElasped), (object) null);
      this.DispatchClientOptionsToServer();
    }

    public void DispatchClientOptionsToServer()
    {
      bool flag = false;
      SetOptions packet = new SetOptions();
      using (Map<ServerOption, NetCache.ClientOptionBase>.Enumerator enumerator = this.ClientState.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ServerOption, NetCache.ClientOptionBase> current = enumerator.Current;
          NetCache.ClientOptionBase clientOptionBase;
          if (this.ServerState.TryGetValue(current.Key, out clientOptionBase))
          {
            if (current.Value != null || clientOptionBase != null)
            {
              if (current.Value == null && clientOptionBase != null || current.Value != null && clientOptionBase == null)
              {
                flag = true;
                break;
              }
              if (!clientOptionBase.Equals((object) current.Value))
              {
                flag = true;
                break;
              }
            }
          }
          else
          {
            flag = true;
            break;
          }
        }
      }
      if (!flag)
        return;
      using (Map<ServerOption, NetCache.ClientOptionBase>.Enumerator enumerator = this.ClientState.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ServerOption, NetCache.ClientOptionBase> current = enumerator.Current;
          if (current.Value != null)
            current.Value.PopulateIntoPacket(current.Key, packet);
        }
      }
      Network.SetClientOptions(packet);
      this.UpdateServerState();
    }

    public void RemoveInvalidOptions()
    {
      List<ServerOption> serverOptionList = new List<ServerOption>();
      using (Map<ServerOption, NetCache.ClientOptionBase>.Enumerator enumerator = this.ClientState.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ServerOption, NetCache.ClientOptionBase> current = enumerator.Current;
          ServerOption key = current.Key;
          NetCache.ClientOptionBase clientOptionBase = current.Value;
          System.Type serverOptionType = Options.Get().GetServerOptionType(key);
          System.Type type = clientOptionBase.GetType();
          if (serverOptionType == typeof (int))
          {
            if (type == typeof (NetCache.ClientOptionInt))
              continue;
          }
          else if (serverOptionType == typeof (long))
          {
            if (type == typeof (NetCache.ClientOptionLong))
              continue;
          }
          else if (serverOptionType == typeof (float))
          {
            if (type == typeof (NetCache.ClientOptionFloat))
              continue;
          }
          else if (serverOptionType == typeof (ulong) && type == typeof (NetCache.ClientOptionULong))
            continue;
          if (serverOptionType == null)
            Log.Net.Print("NetCacheClientOptions.RemoveInvalidOptions() - Option {0} has type {1}, but value is type {2}. Removing it.", new object[3]
            {
              (object) key,
              (object) serverOptionType,
              (object) type
            });
          else
            Log.Net.Print("NetCacheClientOptions.RemoveInvalidOptions() - Option {0} has type {1}, but value is type {2}. Removing it.", new object[3]
            {
              (object) EnumUtils.GetString<ServerOption>(key),
              (object) serverOptionType,
              (object) type
            });
          serverOptionList.Add(key);
        }
      }
      using (List<ServerOption>.Enumerator enumerator = serverOptionList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ServerOption current = enumerator.Current;
          this.ClientState.Remove(current);
          this.ServerState.Remove(current);
        }
      }
    }
  }

  public class NetCacheTavernBrawlInfo
  {
    public TavernBrawlInfo Info { get; set; }

    public NetCacheTavernBrawlInfo(TavernBrawlInfo info)
    {
      this.Info = info;
    }
  }

  public class NetCacheTavernBrawlRecord
  {
    public TavernBrawlPlayerRecord Record { get; set; }

    public NetCacheTavernBrawlRecord(TavernBrawlPlayerRecord record)
    {
      this.Record = record;
    }
  }

  public class NetCacheFavoriteHeroes
  {
    public Map<TAG_CLASS, NetCache.CardDefinition> FavoriteHeroes { get; set; }

    public NetCacheFavoriteHeroes()
    {
      this.FavoriteHeroes = new Map<TAG_CLASS, NetCache.CardDefinition>();
    }
  }

  public class NetCacheAccountLicenses
  {
    public Map<long, AccountLicenseInfo> AccountLicenses { get; set; }

    public NetCacheAccountLicenses()
    {
      this.AccountLicenses = new Map<long, AccountLicenseInfo>();
    }
  }

  public enum ErrorCode
  {
    NONE,
    TIMEOUT,
    SERVER,
  }

  public class ErrorInfo
  {
    public NetCache.ErrorCode Error { get; set; }

    public uint ServerError { get; set; }

    public NetCache.RequestFunc RequestingFunction { get; set; }

    public Map<System.Type, NetCache.Request> RequestedTypes { get; set; }

    public string RequestStackTrace { get; set; }
  }

  public enum RequestResult
  {
    UNKNOWN,
    PENDING,
    IN_PROCESS,
    GENERIC_COMPLETE,
    DATA_COMPLETE,
    ERROR,
  }

  public class Request
  {
    public const bool RELOAD = true;
    public System.Type m_type;
    public bool m_reload;
    public NetCache.RequestResult m_result;

    public Request(System.Type rt, bool rl = false)
    {
      this.m_type = rt;
      this.m_reload = rl;
      this.m_result = NetCache.RequestResult.UNKNOWN;
    }
  }

  private class NetCacheBatchRequest
  {
    public Map<System.Type, NetCache.Request> m_requests = new Map<System.Type, NetCache.Request>();
    public bool m_canTimeout = true;
    public DateTime m_timeAdded = DateTime.Now;
    public NetCache.NetCacheCallback m_callback;
    public NetCache.ErrorCallback m_errorCallback;
    public NetCache.RequestFunc m_requestFunc;
    public string m_requestStackTrace;

    public NetCacheBatchRequest(NetCache.NetCacheCallback reply, NetCache.ErrorCallback errorCallback, NetCache.RequestFunc requestFunc)
    {
      this.m_callback = reply;
      this.m_errorCallback = errorCallback;
      this.m_requestFunc = requestFunc;
      this.m_requestStackTrace = Environment.StackTrace;
    }

    public void AddRequests(List<NetCache.Request> requests)
    {
      using (List<NetCache.Request>.Enumerator enumerator = requests.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.AddRequest(enumerator.Current);
      }
    }

    public void AddRequest(NetCache.Request r)
    {
      if (this.m_requests.ContainsKey(r.m_type))
        return;
      this.m_requests.Add(r.m_type, r);
    }
  }

  public delegate void DelNewNoticesListener(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList);

  public delegate void DelGoldBalanceListener(NetCache.NetCacheGoldBalance balance);

  public delegate void ErrorCallback(NetCache.ErrorInfo info);

  public delegate void NetCacheCallback();

  public delegate void RequestFunc(NetCache.NetCacheCallback callback, NetCache.ErrorCallback errorCallback);
}
