﻿// Decompiled with JetBrains decompiler
// Type: ScoreLabelDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ScoreLabelDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private DbfLocValue m_Text;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("TEXT", "")]
  public DbfLocValue Text
  {
    get
    {
      return this.m_Text;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    ScoreLabelDbfAsset scoreLabelDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (ScoreLabelDbfAsset)) as ScoreLabelDbfAsset;
    if ((UnityEngine.Object) scoreLabelDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("ScoreLabelDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < scoreLabelDbfAsset.Records.Count; ++index)
      scoreLabelDbfAsset.Records[index].StripUnusedLocales();
    records = (object) scoreLabelDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Text.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetText(DbfLocValue v)
  {
    this.m_Text = v;
    v.SetDebugInfo(this.ID, "TEXT");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ScoreLabelDbfRecord.\u003C\u003Ef__switch\u0024map62 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ScoreLabelDbfRecord.\u003C\u003Ef__switch\u0024map62 = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TEXT",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ScoreLabelDbfRecord.\u003C\u003Ef__switch\u0024map62.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Text;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (ScoreLabelDbfRecord.\u003C\u003Ef__switch\u0024map63 == null)
    {
      // ISSUE: reference to a compiler-generated field
      ScoreLabelDbfRecord.\u003C\u003Ef__switch\u0024map63 = new Dictionary<string, int>(3)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "TEXT",
          2
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!ScoreLabelDbfRecord.\u003C\u003Ef__switch\u0024map63.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetText((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ScoreLabelDbfRecord.\u003C\u003Ef__switch\u0024map64 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ScoreLabelDbfRecord.\u003C\u003Ef__switch\u0024map64 = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TEXT",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ScoreLabelDbfRecord.\u003C\u003Ef__switch\u0024map64.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
