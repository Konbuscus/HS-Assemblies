﻿// Decompiled with JetBrains decompiler
// Type: YoggSaronSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class YoggSaronSpell : Spell
{
  public Spell m_MistSpellPrefab;
  private Spell m_mistSpellInstance;

  public override bool CanPurge()
  {
    return (Object) this.m_mistSpellInstance == (Object) null;
  }

  public override bool AddPowerTargets()
  {
    return !(bool) ((Object) this.m_mistSpellInstance) || this.m_taskList.IsEndOfBlock();
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    this.StartCoroutine(this.DoEffectsWithTiming());
  }

  [DebuggerHidden]
  private IEnumerator DoEffectsWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new YoggSaronSpell.\u003CDoEffectsWithTiming\u003Ec__Iterator2D6() { \u003C\u003Ef__this = this };
  }
}
