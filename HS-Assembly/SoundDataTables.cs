﻿// Decompiled with JetBrains decompiler
// Type: SoundDataTables
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class SoundDataTables
{
  public static readonly Map<SoundCategory, Option> s_categoryEnabledOptionMap = new Map<SoundCategory, Option>() { { SoundCategory.MUSIC, Option.MUSIC }, { SoundCategory.SPECIAL_MUSIC, Option.MUSIC }, { SoundCategory.HERO_MUSIC, Option.MUSIC } };
  public static readonly Map<SoundCategory, Option> s_categoryVolumeOptionMap = new Map<SoundCategory, Option>() { { SoundCategory.MUSIC, Option.MUSIC_VOLUME }, { SoundCategory.SPECIAL_MUSIC, Option.MUSIC_VOLUME }, { SoundCategory.HERO_MUSIC, Option.MUSIC_VOLUME } };
}
