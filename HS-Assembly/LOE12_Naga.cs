﻿// Decompiled with JetBrains decompiler
// Type: LOE12_Naga
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LOE12_Naga : LOE_MissionEntity
{
  private bool m_pearlLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOE_12_RESPONSE");
    this.PreloadSound("VO_LOE_12_NAZJAR_TURN_1");
    this.PreloadSound("VO_LOE_12_NAZJAR_TURN_1_FINLEY");
    this.PreloadSound("VO_LOE_12_NAZJAR_TURN_3_FINLEY");
    this.PreloadSound("VO_LOE_NAZJAR_TURN_3_CARTOGRAPHER");
    this.PreloadSound("VO_LOE_12_NAZJAR_TURN_5");
    this.PreloadSound("VO_LOE_12_NAZJAR_TURN_5_FINLEY");
    this.PreloadSound("VO_LOE_12_WIN");
    this.PreloadSound("VO_LOE_12_WIN_2");
    this.PreloadSound("VO_LOE_12_NAZJAR_PEARL");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_LOE_12_RESPONSE",
            m_stringTag = "VO_LOE_12_RESPONSE"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE12_Naga.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator176() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE12_Naga.\u003CHandleMissionEventWithTiming\u003Ec__Iterator177() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE12_Naga.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator178() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE12_Naga.\u003CHandleGameOverWithTiming\u003Ec__Iterator179() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
