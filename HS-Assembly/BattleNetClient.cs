﻿// Decompiled with JetBrains decompiler
// Type: BattleNetClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Diagnostics;
using System.IO;

public class BattleNetClient
{
  public static bool needsToRun
  {
    get
    {
      if (BattleNetClient.usedOnThisPlatform)
        return !BattleNetClient.launchedHearthstone;
      return false;
    }
  }

  private static bool usedOnThisPlatform
  {
    get
    {
      return true;
    }
  }

  private static bool launchedHearthstone
  {
    get
    {
      foreach (string commandLineArg in Environment.GetCommandLineArgs())
      {
        if (commandLineArg.Equals("-launch", StringComparison.OrdinalIgnoreCase))
          return true;
      }
      return false;
    }
  }

  private static FileInfo bootstrapper
  {
    get
    {
      return new FileInfo("Hearthstone Beta Launcher.exe");
    }
  }

  public static void quitHearthstoneAndRun()
  {
    Log.All.PrintWarning("Hearthstone was not run from Battle.net Client");
    if (!BattleNetClient.bootstrapper.Exists)
    {
      Log.All.PrintWarning("Hearthstone could not find Battle.net client");
      Error.AddFatalLoc("GLUE_CANNOT_FIND_BATTLENET_CLIENT");
    }
    else
    {
      try
      {
        new Process()
        {
          StartInfo = {
            UseShellExecute = false,
            FileName = BattleNetClient.bootstrapper.FullName,
            Arguments = "-uid hs_beta"
          },
          EnableRaisingEvents = true
        }.Start();
        Log.All.PrintWarning("Hearthstone ran Battle.net Client.  Exiting.");
        ApplicationMgr.Get().Exit();
      }
      catch (Exception ex)
      {
        Error.AddFatalLoc("GLUE_CANNOT_RUN_BATTLENET_CLIENT");
        Log.All.PrintWarning("Hearthstone could not launch Battle.net client: {0}", (object) ex.Message);
      }
    }
  }
}
