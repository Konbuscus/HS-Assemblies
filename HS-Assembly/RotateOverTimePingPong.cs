﻿// Decompiled with JetBrains decompiler
// Type: RotateOverTimePingPong
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RotateOverTimePingPong : MonoBehaviour
{
  public bool RandomStartX = true;
  public bool RandomStartY = true;
  public bool RandomStartZ = true;
  public float RotateRangeXmax = 10f;
  public float RotateRangeYmax = 10f;
  public float RotateRangeZmax = 10f;
  public float RotateSpeedX;
  public float RotateSpeedY;
  public float RotateSpeedZ;
  public float RotateRangeXmin;
  public float RotateRangeYmin;
  public float RotateRangeZmin;

  private void Start()
  {
    if (this.RandomStartX)
      this.transform.Rotate(Vector3.left, Random.Range(this.RotateRangeXmin, this.RotateRangeXmax));
    if (this.RandomStartY)
      this.transform.Rotate(Vector3.up, Random.Range(this.RotateRangeYmin, this.RotateRangeYmax));
    if (!this.RandomStartZ)
      return;
    this.transform.Rotate(Vector3.forward, Random.Range(this.RotateRangeZmin, this.RotateRangeZmax));
  }

  private void Update()
  {
    iTween.RotateUpdate(this.gameObject, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, this.gameObject.transform.localRotation.y, Mathf.Sin(Time.time) * this.RotateRangeZmax), (object) "isLocal", (object) true, (object) "time", (object) 0));
  }
}
