﻿// Decompiled with JetBrains decompiler
// Type: PlayParticlesList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlayParticlesList : MonoBehaviour
{
  public GameObject[] m_objects;

  public void PlayParticle(int theIndex)
  {
    if (theIndex < 0 || theIndex > this.m_objects.Length)
      Debug.LogWarning((object) "The index is out of range");
    else if ((Object) this.m_objects[theIndex] == (Object) null)
      Debug.LogWarningFormat("{0} PlayParticlesList object is null", (object) this.gameObject.name);
    else
      this.m_objects[theIndex].GetComponent<ParticleSystem>().Play();
  }
}
