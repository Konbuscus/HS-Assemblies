﻿// Decompiled with JetBrains decompiler
// Type: HeroLabel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class HeroLabel : MonoBehaviour
{
  public UberText m_nameText;
  public UberText m_classText;

  public void UpdateText(string nameText, string classText)
  {
    this.m_nameText.Text = nameText;
    this.m_classText.Text = classText;
  }

  public void SetFade(float fade)
  {
    this.m_nameText.TextAlpha = fade;
    this.m_classText.TextAlpha = fade;
  }

  public void FadeIn()
  {
    iTween.Stop(this.m_nameText.gameObject);
    iTween.Stop(this.m_classText.gameObject);
    iTween.FadeTo(this.m_nameText.gameObject, 1f, 0.5f);
    iTween.FadeTo(this.m_classText.gameObject, 1f, 0.5f);
  }

  public void FadeOut()
  {
    iTween.Stop(this.m_nameText.gameObject);
    iTween.Stop(this.m_classText.gameObject);
    iTween.FadeTo(this.m_nameText.gameObject, 0.0f, 0.5f);
    iTween.FadeTo(this.m_classText.gameObject, 0.0f, 0.5f);
    this.StartCoroutine(this.FinishFade());
  }

  [DebuggerHidden]
  private IEnumerator FinishFade()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroLabel.\u003CFinishFade\u003Ec__Iterator30C() { \u003C\u003Ef__this = this };
  }
}
