﻿// Decompiled with JetBrains decompiler
// Type: DrawWireframe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DrawWireframe : MonoBehaviour
{
  private void OnPreRender()
  {
    GL.wireframe = true;
  }

  private void OnPostRender()
  {
    GL.wireframe = false;
  }
}
