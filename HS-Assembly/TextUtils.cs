﻿// Decompiled with JetBrains decompiler
// Type: TextUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;

public static class TextUtils
{
  private const int DEFAULT_STRING_BUILDER_CAPACITY_FUDGE = 10;

  public static string DecodeWhitespaces(string text)
  {
    text = text.Replace("\\n", "\n");
    text = text.Replace("\\t", "\t");
    return text;
  }

  public static string EncodeWhitespaces(string text)
  {
    text = text.Replace("\n", "\\n");
    text = text.Replace("\t", "\\t");
    return text;
  }

  public static string ComposeLineItemString(List<string> lines)
  {
    if (lines.Count == 0)
      return string.Empty;
    StringBuilder stringBuilder = new StringBuilder();
    using (List<string>.Enumerator enumerator = lines.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        stringBuilder.AppendLine(current);
      }
    }
    stringBuilder.Remove(stringBuilder.Length - 1, 1);
    return stringBuilder.ToString();
  }

  public static int CountCharInString(string s, char c)
  {
    int num = 0;
    for (int index = 0; index < s.Length; ++index)
    {
      if ((int) s[index] == (int) c)
        ++num;
    }
    return num;
  }

  public static string Slice(this string str, int start, int end)
  {
    int length1 = str.Length;
    if (start < 0)
      start = length1 + start;
    if (end < 0)
      end = length1 + end;
    int length2 = end - start;
    if (length2 <= 0)
      return string.Empty;
    int num = length1 - start;
    if (length2 > num)
      length2 = num;
    return str.Substring(start, length2);
  }

  public static string Slice(this string str, int start)
  {
    return str.Slice(start, str.Length);
  }

  public static string Slice<T>(this string str)
  {
    return str.Slice(0, str.Length);
  }

  public static string TransformCardText(Entity entity, string text)
  {
    return TextUtils.TransformCardText(entity.GetDamageBonus(), entity.GetDamageBonusDouble(), entity.GetHealingDouble(), text);
  }

  public static string TransformCardText(string text)
  {
    return TextUtils.TransformCardText(0, 0, 0, text);
  }

  public static string TransformCardText(int damageBonus, int damageBonusDouble, int healingDouble, string powersText)
  {
    return GameStrings.ParseLanguageRules(TextUtils.TransformCardTextImpl(damageBonus, damageBonusDouble, healingDouble, powersText));
  }

  public static string ToHexString(this byte[] bytes)
  {
    char[] chArray = new char[bytes.Length * 2];
    for (int index = 0; index < bytes.Length; ++index)
    {
      int num1 = (int) bytes[index] >> 4;
      chArray[index * 2] = (char) (55 + num1 + (num1 - 10 >> 31 & -7));
      int num2 = (int) bytes[index] & 15;
      chArray[index * 2 + 1] = (char) (55 + num2 + (num2 - 10 >> 31 & -7));
    }
    return new string(chArray);
  }

  public static string ToHexString(string str)
  {
    return Encoding.UTF8.GetBytes(str).ToHexString();
  }

  public static string FromHexString(string str)
  {
    if (str.Length % 2 == 1)
      throw new Exception("Hex string must have an even number of digits");
    byte[] bytes = new byte[str.Length >> 1];
    for (int index = 0; index < str.Length >> 1; ++index)
      bytes[index] = (byte) ((TextUtils.GetHexValue(str[index << 1]) << 4) + TextUtils.GetHexValue(str[(index << 1) + 1]));
    return Encoding.UTF8.GetString(bytes);
  }

  private static int GetHexValue(char hex)
  {
    int num = (int) hex;
    return num - (num >= 58 ? 55 : 48);
  }

  public static string HexDumpFromBytes(byte[] bytes, string separator = "\n")
  {
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < bytes.Length; ++index)
    {
      stringBuilder.Append(string.Format("{0:X2}", (object) bytes[index]));
      if ((index + 1) % 4 == 0 && index < bytes.Length)
        stringBuilder.Append(separator);
    }
    return stringBuilder.ToString();
  }

  public static bool HasBonusDamage(string powersText)
  {
    if (powersText == null)
      return false;
    for (int index1 = 0; index1 < powersText.Length; ++index1)
    {
      if ((int) powersText[index1] == 36)
      {
        int index2;
        for (index2 = ++index1; index2 < powersText.Length; ++index2)
        {
          char ch = powersText[index2];
          if ((int) ch < 48 || (int) ch > 57)
            break;
        }
        if (index2 != index1)
          return true;
      }
    }
    return false;
  }

  private static string TransformCardTextImpl(int damageBonus, int damageBonusDouble, int healingDouble, string powersText)
  {
    if (powersText == null || powersText == string.Empty)
      return string.Empty;
    StringBuilder stringBuilder = new StringBuilder();
    bool flag1 = damageBonus > 0 || damageBonusDouble > 0;
    bool flag2 = healingDouble > 0;
    for (int startIndex = 0; startIndex < powersText.Length; ++startIndex)
    {
      char ch1 = powersText[startIndex];
      switch (ch1)
      {
        case '$':
        case '#':
          int index1;
          for (index1 = ++startIndex; index1 < powersText.Length; ++index1)
          {
            char ch2 = powersText[index1];
            if ((int) ch2 < 48 || (int) ch2 > 57)
              break;
          }
          if (index1 != startIndex)
          {
            int int32 = Convert.ToInt32(powersText.Substring(startIndex, index1 - startIndex));
            if ((int) ch1 == 36)
            {
              int32 += damageBonus;
              for (int index2 = 0; index2 < damageBonusDouble; ++index2)
                int32 *= 2;
            }
            else if ((int) ch1 == 35)
            {
              for (int index2 = 0; index2 < healingDouble; ++index2)
                int32 *= 2;
            }
            if (flag1 && (int) ch1 == 36 || flag2 && (int) ch1 == 35)
            {
              stringBuilder.Append('*');
              stringBuilder.Append(int32);
              stringBuilder.Append('*');
            }
            else
              stringBuilder.Append(int32);
            startIndex = index1 - 1;
            break;
          }
          break;
        default:
          stringBuilder.Append(ch1);
          break;
      }
    }
    return stringBuilder.ToString();
  }
}
