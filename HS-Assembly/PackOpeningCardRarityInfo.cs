﻿// Decompiled with JetBrains decompiler
// Type: PackOpeningCardRarityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PackOpeningCardRarityInfo : MonoBehaviour
{
  public PackOpeningRarity m_RarityType;
  public GameObject m_RarityObject;
  public GameObject m_HiddenCardObject;
  public GameObject m_RevealedCardObject;

  private void Start()
  {
  }
}
