﻿// Decompiled with JetBrains decompiler
// Type: DeckMaker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class DeckMaker
{
  private static readonly DeckMaker.CardRequirements[] s_OrderedCardRequirements = new DeckMaker.CardRequirements[6]{ new DeckMaker.CardRequirements(8, (DeckMaker.CardRequirementsCondition) (e =>
  {
    if (DeckMaker.IsMinion(e) && DeckMaker.HasMinCost(e, 1))
      return DeckMaker.HasMaxCost(e, 2);
    return false;
  }), "GLUE_RDM_LOW_COST"), new DeckMaker.CardRequirements(5, (DeckMaker.CardRequirementsCondition) (e =>
  {
    if (DeckMaker.IsMinion(e) && DeckMaker.HasMinCost(e, 3))
      return DeckMaker.HasMaxCost(e, 4);
    return false;
  }), "GLUE_RDM_MEDIUM_COST"), new DeckMaker.CardRequirements(4, (DeckMaker.CardRequirementsCondition) (e =>
  {
    if (DeckMaker.IsMinion(e))
      return DeckMaker.HasMinCost(e, 5);
    return false;
  }), "GLUE_RDM_HIGH_COST"), new DeckMaker.CardRequirements(7, (DeckMaker.CardRequirementsCondition) (e => DeckMaker.IsSpell(e)), "GLUE_RDM_MORE_SPELLS"), new DeckMaker.CardRequirements(2, (DeckMaker.CardRequirementsCondition) (e => DeckMaker.IsWeapon(e)), "GLUE_RDM_MORE_WEAPONS"), new DeckMaker.CardRequirements(int.MaxValue, (DeckMaker.CardRequirementsCondition) (e => DeckMaker.IsMinion(e)), "GLUE_RDM_NO_SPECIFICS") };
  private const int s_randomChoicePoolSize = 8;
  private const int s_priorityWeightDifference = 100;

  private static bool IsMinion(EntityDef e)
  {
    return e.GetCardType() == TAG_CARDTYPE.MINION;
  }

  private static bool IsSpell(EntityDef e)
  {
    return e.GetCardType() == TAG_CARDTYPE.SPELL;
  }

  private static bool IsWeapon(EntityDef e)
  {
    return e.GetCardType() == TAG_CARDTYPE.WEAPON;
  }

  private static bool HasMinCost(EntityDef e, int minCost)
  {
    return e.GetCost() >= minCost;
  }

  private static bool HasMaxCost(EntityDef e, int maxCost)
  {
    return e.GetCost() <= maxCost;
  }

  [DebuggerHidden]
  public static IEnumerable<DeckMaker.DeckFill> GetFillCards(CollectionDeck deck, DeckRuleset deckRuleset)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckMaker.\u003CGetFillCards\u003Ec__Iterator2FE cardsCIterator2Fe = new DeckMaker.\u003CGetFillCards\u003Ec__Iterator2FE() { deck = deck, deckRuleset = deckRuleset, \u003C\u0024\u003Edeck = deck, \u003C\u0024\u003EdeckRuleset = deckRuleset };
    // ISSUE: reference to a compiler-generated field
    cardsCIterator2Fe.\u0024PC = -2;
    return (IEnumerable<DeckMaker.DeckFill>) cardsCIterator2Fe;
  }

  public static DeckMaker.DeckChoiceFill GetFillCardChoices(CollectionDeck deck, EntityDef referenceCard, int choices, DeckRuleset deckRuleset = null)
  {
    if (deckRuleset == null)
      deckRuleset = deck.GetRuleset();
    List<EntityDef> currentDeckCards;
    List<EntityDef> currentInvalidCards;
    List<EntityDef> distinctCardsICanAddToDeck;
    DeckMaker.InitFromDeck(deck, deckRuleset, out currentDeckCards, out currentInvalidCards, out distinctCardsICanAddToDeck);
    return DeckMaker.GetFillCard(referenceCard, distinctCardsICanAddToDeck, currentDeckCards, currentInvalidCards, choices);
  }

  private static void InitFromDeck(CollectionDeck deck, DeckRuleset deckRuleset, out List<EntityDef> currentDeckCards, out List<EntityDef> currentInvalidCards, out List<EntityDef> distinctCardsICanAddToDeck)
  {
    CollectionManager collectionManager = CollectionManager.Get();
    List<DeckMaker.SortableEntityDef> sortableEntityDefList = new List<DeckMaker.SortableEntityDef>();
    currentDeckCards = new List<EntityDef>();
    currentInvalidCards = new List<EntityDef>();
    using (List<CollectionDeckSlot>.Enumerator enumerator = deck.GetSlots().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        CollectibleCard card = CollectionManager.Get().GetCard(current.CardID, current.Premium);
        if (card != null)
        {
          EntityDef entityDef = card.GetEntityDef();
          for (int index = 0; index < current.Count; ++index)
          {
            if (deck.IsValidSlot(current))
              currentDeckCards.Add(entityDef);
            else
              currentInvalidCards.Add(entityDef);
          }
        }
      }
    }
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckMaker.\u003CInitFromDeck\u003Ec__AnonStorey42A deckCAnonStorey42A = new DeckMaker.\u003CInitFromDeck\u003Ec__AnonStorey42A();
    using (Map<string, EntityDef>.Enumerator enumerator = DefLoader.Get().GetAllEntityDefs().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        deckCAnonStorey42A.kvpair = enumerator.Current;
        // ISSUE: reference to a compiler-generated field
        CollectibleCard card1 = collectionManager.GetCard(deckCAnonStorey42A.kvpair.Key, TAG_PREMIUM.NORMAL);
        // ISSUE: reference to a compiler-generated field
        if (card1 != null && !card1.IsHero && (card1.Class == deck.GetClass() || card1.Class == TAG_CLASS.NEUTRAL) && (deckRuleset == null || deckRuleset.Filter(deckCAnonStorey42A.kvpair.Value)))
        {
          // ISSUE: reference to a compiler-generated field
          int a = Mathf.Min(2, deckRuleset.GetMaxCopiesOfCardAllowed(deckCAnonStorey42A.kvpair.Value));
          int ownedCount = card1.OwnedCount;
          // ISSUE: reference to a compiler-generated field
          CollectibleCard card2 = collectionManager.GetCard(deckCAnonStorey42A.kvpair.Key, TAG_PREMIUM.GOLDEN);
          if (card2 != null)
            ownedCount += card2.OwnedCount;
          // ISSUE: reference to a compiler-generated method
          int count = currentDeckCards.FindAll(new Predicate<EntityDef>(deckCAnonStorey42A.\u003C\u003Em__2AA)).Count;
          int num = Mathf.Min(a, ownedCount) - count;
          for (int index = 0; index < num; ++index)
          {
            // ISSUE: reference to a compiler-generated field
            sortableEntityDefList.Add(new DeckMaker.SortableEntityDef()
            {
              m_entityDef = deckCAnonStorey42A.kvpair.Value,
              m_suggestWeight = card1.SuggestWeight
            });
          }
        }
      }
    }
    sortableEntityDefList.Sort((Comparison<DeckMaker.SortableEntityDef>) ((lhs, rhs) =>
    {
      int num = rhs.m_suggestWeight - lhs.m_suggestWeight;
      if (num != 0)
        return num;
      return UnityEngine.Random.Range(-1, 2);
    }));
    distinctCardsICanAddToDeck = new List<EntityDef>();
    using (List<DeckMaker.SortableEntityDef>.Enumerator enumerator = sortableEntityDefList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckMaker.SortableEntityDef current = enumerator.Current;
        distinctCardsICanAddToDeck.Add(current.m_entityDef);
      }
    }
  }

  [DebuggerHidden]
  private static IEnumerable<DeckMaker.DeckFill> GetInvalidFillCards(List<EntityDef> cardsICanAddToDeck, List<EntityDef> currentDeckCards, List<EntityDef> currentInvalidCards)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckMaker.\u003CGetInvalidFillCards\u003Ec__Iterator2FF cardsCIterator2Ff = new DeckMaker.\u003CGetInvalidFillCards\u003Ec__Iterator2FF() { currentInvalidCards = currentInvalidCards, cardsICanAddToDeck = cardsICanAddToDeck, currentDeckCards = currentDeckCards, \u003C\u0024\u003EcurrentInvalidCards = currentInvalidCards, \u003C\u0024\u003EcardsICanAddToDeck = cardsICanAddToDeck, \u003C\u0024\u003EcurrentDeckCards = currentDeckCards };
    // ISSUE: reference to a compiler-generated field
    cardsCIterator2Ff.\u0024PC = -2;
    return (IEnumerable<DeckMaker.DeckFill>) cardsCIterator2Ff;
  }

  private static bool ReplaceInvalidCard(DeckMaker.DeckFill choice, List<EntityDef> cardsICanAddToDeck, List<EntityDef> currentDeckCards, List<EntityDef> currentInvalidCards)
  {
    if (choice == null || !currentInvalidCards.Remove(choice.m_removeTemplate))
      return false;
    cardsICanAddToDeck.Remove(choice.m_addCard);
    currentDeckCards.Add(choice.m_addCard);
    return true;
  }

  private static DeckMaker.DeckChoiceFill GetFillCard(EntityDef referenceCard, List<EntityDef> cardsICanAddToDeck, List<EntityDef> currentDeckCards, List<EntityDef> currentInvalidCards, int totalNumChoices = 3)
  {
    if (referenceCard == null && currentInvalidCards != null && currentInvalidCards.Count > 0)
      referenceCard = currentInvalidCards.First<EntityDef>();
    int requirementsStartIndex = DeckMaker.GetCardRequirementsStartIndex(referenceCard, currentDeckCards);
    DeckMaker.DeckChoiceFill deckChoiceFill1 = new DeckMaker.DeckChoiceFill(referenceCard, new EntityDef[0]);
    for (int index = requirementsStartIndex; index < DeckMaker.s_OrderedCardRequirements.Length; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      DeckMaker.\u003CGetFillCard\u003Ec__AnonStorey42B cardCAnonStorey42B = new DeckMaker.\u003CGetFillCard\u003Ec__AnonStorey42B();
      if (totalNumChoices > 0)
      {
        DeckMaker.CardRequirements orderedCardRequirement = DeckMaker.s_OrderedCardRequirements[index];
        // ISSUE: reference to a compiler-generated field
        cardCAnonStorey42B.condition = orderedCardRequirement.m_condition;
        // ISSUE: reference to a compiler-generated method
        List<EntityDef> all = cardsICanAddToDeck.FindAll(new Predicate<EntityDef>(cardCAnonStorey42B.\u003C\u003Em__2AC));
        if (all.Count > 0)
        {
          int num = 8;
          List<EntityDef> entityDefList1 = new List<EntityDef>();
          List<EntityDef> entityDefList2 = new List<EntityDef>();
          int a = int.MinValue;
          foreach (EntityDef entityDef in all.Distinct<EntityDef>())
          {
            CollectibleCard card = CollectionManager.Get().GetCard(entityDef.GetCardId(), TAG_PREMIUM.NORMAL);
            a = Mathf.Max(a, card.SuggestWeight);
          }
          foreach (EntityDef entityDef in all.Distinct<EntityDef>())
          {
            if (num > 0)
            {
              CollectibleCard card = CollectionManager.Get().GetCard(entityDef.GetCardId(), TAG_PREMIUM.NORMAL);
              if (a - card.SuggestWeight > 100)
                entityDefList2.Add(entityDef);
              else
                entityDefList1.Add(entityDef);
              --num;
            }
            else
              break;
          }
          GeneralUtils.Shuffle<EntityDef>((IList<EntityDef>) entityDefList1);
          GeneralUtils.Shuffle<EntityDef>((IList<EntityDef>) entityDefList2);
          int count1 = Mathf.Min(entityDefList1.Count, totalNumChoices);
          int count2 = Mathf.Min(entityDefList2.Count, totalNumChoices - count1);
          if (count1 > 0)
            deckChoiceFill1.m_addChoices.AddRange((IEnumerable<EntityDef>) entityDefList1.GetRange(0, count1));
          if (count2 > 0)
            deckChoiceFill1.m_addChoices.AddRange((IEnumerable<EntityDef>) entityDefList2.GetRange(0, count2));
          totalNumChoices -= count1 + count2;
          DeckMaker.DeckChoiceFill deckChoiceFill2 = deckChoiceFill1;
          string str;
          if (referenceCard == null)
            str = orderedCardRequirement.GetRequirementReason();
          else
            str = GameStrings.Format("GLUE_RDM_TEMPLATE_REPLACE", (object) referenceCard.GetName());
          deckChoiceFill2.m_reason = str;
        }
      }
      else
        break;
    }
    return deckChoiceFill1;
  }

  private static int GetCardRequirementsStartIndex(EntityDef referenceCard, List<EntityDef> currentDeckCards)
  {
    if (referenceCard != null)
    {
      for (int index = 0; index < DeckMaker.s_OrderedCardRequirements.Length; ++index)
      {
        if (DeckMaker.s_OrderedCardRequirements[index].m_condition(referenceCard))
          return index;
      }
    }
    else if (currentDeckCards != null)
    {
      for (int index = 0; index < DeckMaker.s_OrderedCardRequirements.Length; ++index)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        DeckMaker.\u003CGetCardRequirementsStartIndex\u003Ec__AnonStorey42C indexCAnonStorey42C = new DeckMaker.\u003CGetCardRequirementsStartIndex\u003Ec__AnonStorey42C();
        DeckMaker.CardRequirements orderedCardRequirement = DeckMaker.s_OrderedCardRequirements[index];
        // ISSUE: reference to a compiler-generated field
        indexCAnonStorey42C.condition = orderedCardRequirement.m_condition;
        // ISSUE: reference to a compiler-generated method
        if (currentDeckCards.FindAll(new Predicate<EntityDef>(indexCAnonStorey42C.\u003C\u003Em__2AD)).Count < orderedCardRequirement.m_requiredCount)
          return index;
      }
    }
    return 0;
  }

  public class DeckChoiceFill
  {
    public List<EntityDef> m_addChoices = new List<EntityDef>();
    public EntityDef m_removeTemplate;
    public string m_reason;

    public DeckChoiceFill(EntityDef remove, params EntityDef[] addChoices)
    {
      this.m_removeTemplate = remove;
      if (addChoices == null || addChoices.Length <= 0)
        return;
      this.m_addChoices = new List<EntityDef>((IEnumerable<EntityDef>) addChoices);
    }

    public DeckMaker.DeckFill GetDeckFillChoice(int idx)
    {
      if (idx >= this.m_addChoices.Count)
        return (DeckMaker.DeckFill) null;
      return new DeckMaker.DeckFill() { m_removeTemplate = this.m_removeTemplate, m_addCard = this.m_addChoices[idx], m_reason = this.m_reason };
    }
  }

  public class DeckFill
  {
    public EntityDef m_removeTemplate;
    public EntityDef m_addCard;
    public string m_reason;
  }

  private class CardRequirements
  {
    public int m_requiredCount;
    public DeckMaker.CardRequirementsCondition m_condition;
    private string m_reason;

    public CardRequirements(int requiredCount, DeckMaker.CardRequirementsCondition condition, string reason = "")
    {
      this.m_requiredCount = requiredCount;
      this.m_condition = condition;
      this.m_reason = reason;
    }

    public string GetRequirementReason()
    {
      if (string.IsNullOrEmpty(this.m_reason))
        return "No reason!";
      return GameStrings.Get(this.m_reason);
    }
  }

  private class SortableEntityDef
  {
    public EntityDef m_entityDef;
    public int m_suggestWeight;
  }

  public delegate bool CardRequirementsCondition(EntityDef entityDef);
}
