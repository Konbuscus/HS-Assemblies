﻿// Decompiled with JetBrains decompiler
// Type: CollectionCardCount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CollectionCardCount : MonoBehaviour
{
  private int m_count = 1;
  public UberText m_countText;
  public GameObject m_border;
  public GameObject m_wideBorder;
  public Color m_standardTextColor;
  public Color m_wildTextColor;
  public Material m_standardBorderMaterial;
  public Material m_wildBorderMaterial;

  private void Start()
  {
  }

  private void Update()
  {
  }

  private void Awake()
  {
  }

  public void SetCount(int cardCount)
  {
    this.m_count = cardCount;
    this.UpdateVisibility();
  }

  public int GetCount()
  {
    return this.m_count;
  }

  public void Show()
  {
    this.UpdateVisibility();
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  private void UpdateVisibility()
  {
    if (this.m_count <= 1)
    {
      this.Hide();
    }
    else
    {
      this.gameObject.SetActive(true);
      this.m_countText.TextColor = !CollectionManager.Get().IsShowingWildTheming((CollectionDeck) null) ? this.m_standardTextColor : this.m_wildTextColor;
      Material material = !CollectionManager.Get().IsShowingWildTheming((CollectionDeck) null) ? this.m_standardBorderMaterial : this.m_wildBorderMaterial;
      this.m_wideBorder.GetComponent<Renderer>().material = material;
      this.m_border.GetComponent<Renderer>().material = material;
      GameObject gameObject1;
      GameObject gameObject2;
      if (this.m_count < 10)
      {
        gameObject1 = this.m_border;
        gameObject2 = this.m_wideBorder;
        this.m_countText.Text = GameStrings.Format("GLUE_COLLECTION_CARD_COUNT", (object) this.m_count);
      }
      else
      {
        gameObject1 = this.m_wideBorder;
        gameObject2 = this.m_border;
        this.m_countText.Text = GameStrings.Get("GLUE_COLLECTION_CARD_COUNT_LARGE");
      }
      gameObject1.SetActive(true);
      gameObject2.SetActive(false);
    }
  }
}
