﻿// Decompiled with JetBrains decompiler
// Type: DeckRule_IsInAnySubset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DeckRule_IsInAnySubset : DeckRule
{
  public DeckRule_IsInAnySubset(DeckRulesetRuleDbfRecord record)
    : base(DeckRule.RuleType.IS_IN_ANY_SUBSET, record)
  {
  }

  public DeckRule_IsInAnySubset(int subsetId)
  {
    this.m_subsets = new List<HashSet<string>>();
    this.m_subsets.Add(GameDbf.GetIndex().GetSubsetById(subsetId));
  }

  public override bool Filter(EntityDef def)
  {
    string cardId = def.GetCardId();
    if (!this.AppliesTo(cardId))
      return true;
    using (List<HashSet<string>>.Enumerator enumerator = this.m_subsets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Contains(cardId))
          return this.GetResult(true);
      }
    }
    return this.GetResult(false);
  }

  public override bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason)
  {
    return this.DefaultYes(out reason);
  }
}
