﻿// Decompiled with JetBrains decompiler
// Type: LogInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class LogInfo
{
  public LogLevel m_minLevel = LogLevel.Debug;
  public LogLevel m_defaultLevel = LogLevel.Debug;
  public bool m_alwaysPrintErrors = true;
  public string m_name;
  public bool m_consolePrinting;
  public bool m_screenPrinting;
  public bool m_filePrinting;
  public bool m_verbose;
}
