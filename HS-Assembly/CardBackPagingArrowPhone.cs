﻿// Decompiled with JetBrains decompiler
// Type: CardBackPagingArrowPhone
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class CardBackPagingArrowPhone : CardBackPagingArrowBase
{
  public PegUIElement button;

  private void Start()
  {
    this.button.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnButtonReleased));
  }

  private void OnButtonReleased(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("deck_select_button_press");
  }

  public override void EnablePaging(bool enable)
  {
    this.button.gameObject.SetActive(enable);
  }

  public override void AddEventListener(UIEventType eventType, UIEvent.Handler handler)
  {
    this.button.AddEventListener(eventType, handler);
  }
}
