﻿// Decompiled with JetBrains decompiler
// Type: MATSDK.MATPreloadData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace MATSDK
{
  public struct MATPreloadData
  {
    public string advertiserSubAd;
    public string advertiserSubAdgroup;
    public string advertiserSubCampaign;
    public string advertiserSubKeyword;
    public string advertiserSubPublisher;
    public string advertiserSubSite;
    public string agencyId;
    public string offerId;
    public string publisherId;
    public string publisherReferenceId;
    public string publisherSub1;
    public string publisherSub2;
    public string publisherSub3;
    public string publisherSub4;
    public string publisherSub5;
    public string publisherSubAd;
    public string publisherSubAdgroup;
    public string publisherSubCampaign;
    public string publisherSubKeyword;
    public string publisherSubPublisher;
    public string publisherSubSite;

    public MATPreloadData(string publisherId)
    {
      this.advertiserSubAd = (string) null;
      this.advertiserSubAdgroup = (string) null;
      this.advertiserSubCampaign = (string) null;
      this.advertiserSubKeyword = (string) null;
      this.advertiserSubPublisher = (string) null;
      this.advertiserSubSite = (string) null;
      this.agencyId = (string) null;
      this.offerId = (string) null;
      this.publisherId = publisherId;
      this.publisherReferenceId = (string) null;
      this.publisherSub1 = (string) null;
      this.publisherSub2 = (string) null;
      this.publisherSub3 = (string) null;
      this.publisherSub4 = (string) null;
      this.publisherSub5 = (string) null;
      this.publisherSubAd = (string) null;
      this.publisherSubAdgroup = (string) null;
      this.publisherSubCampaign = (string) null;
      this.publisherSubKeyword = (string) null;
      this.publisherSubPublisher = (string) null;
      this.publisherSubSite = (string) null;
    }
  }
}
