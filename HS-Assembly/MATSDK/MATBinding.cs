﻿// Decompiled with JetBrains decompiler
// Type: MATSDK.MATBinding
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace MATSDK
{
  public class MATBinding : MonoBehaviour
  {
    public static void Init(string advertiserId, string conversionKey)
    {
      if (!Application.isEditor)
        ;
    }

    public static void CheckForDeferredDeeplinkWithTimeout(double timeoutMillis)
    {
      if (!Application.isEditor)
        ;
    }

    public static void AutomateIapEventMeasurement(bool automate)
    {
      if (!Application.isEditor)
        ;
    }

    public static void MeasureEvent(string eventName)
    {
      if (!Application.isEditor)
        ;
    }

    public static void MeasureEvent(MATEvent matEvent)
    {
      if (!Application.isEditor)
        ;
    }

    public static void MeasureSession()
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAge(int age)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAllowDuplicates(bool allow)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAppAdTracking(bool adTrackingEnabled)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetDebugMode(bool debug)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventAttribute1(string eventAttribute)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventAttribute2(string eventAttribute)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventAttribute3(string eventAttribute)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventAttribute4(string eventAttribute)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventAttribute5(string eventAttribute)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventContentId(string eventContentId)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventContentType(string eventContentType)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventDate1(DateTime eventDate)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventDate2(DateTime eventDate)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventLevel(int eventLevel)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventQuantity(int eventQuantity)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventRating(float eventRating)
    {
      if (!Application.isEditor)
        ;
    }

    private static void SetEventSearchString(string eventSearchString)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetExistingUser(bool isExistingUser)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetFacebookEventLogging(bool enable, bool limit)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetFacebookUserId(string fbUserId)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetGender(int gender)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetGoogleUserId(string googleUserId)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetLocation(double latitude, double longitude, double altitude)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetPackageName(string packageName)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetPayingUser(bool isPayingUser)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetPhoneNumber(string phoneNumber)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetTwitterUserId(string twitterUserId)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetUserEmail(string userEmail)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetUserId(string userId)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetUserName(string userName)
    {
      if (!Application.isEditor)
        ;
    }

    public static bool GetIsPayingUser()
    {
      if (Application.isEditor)
        ;
      return true;
    }

    public static string GetMATId()
    {
      if (Application.isEditor)
        ;
      return string.Empty;
    }

    public static string GetOpenLogId()
    {
      if (Application.isEditor)
        ;
      return string.Empty;
    }

    public static void SetAppleAdvertisingIdentifier(string advertiserIdentifier, bool trackingEnabled)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAppleVendorIdentifier(string vendorIdentifier)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetJailbroken(bool isJailbroken)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetShouldAutoDetectJailbroken(bool isAutoDetectJailbroken)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetShouldAutoGenerateVendorIdentifier(bool shouldAutoGenerate)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetUseCookieTracking(bool useCookieTracking)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAndroidId(string androidId)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAndroidIdMd5(string androidIdMd5)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAndroidIdSha1(string androidIdSha1)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAndroidIdSha256(string androidIdSha256)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetDeviceId(string deviceId)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetEmailCollection(bool collectEmail)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetMacAddress(string macAddress)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetGoogleAdvertisingId(string adId, bool isLATEnabled)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetPreloadedApp(MATPreloadData preloadData)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAppName(string appName)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetAppVersion(string appVersion)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetLastOpenLogId(string lastOpenLogId)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetOSVersion(string osVersion)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetCurrencyCode(string currencyCode)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetDelegate(bool enable)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetSiteId(string siteId)
    {
      if (!Application.isEditor)
        ;
    }

    public static void SetTRUSTeId(string tpid)
    {
      if (!Application.isEditor)
        ;
    }
  }
}
