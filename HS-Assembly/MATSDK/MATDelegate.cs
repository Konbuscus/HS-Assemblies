﻿// Decompiled with JetBrains decompiler
// Type: MATSDK.MATDelegate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using UnityEngine;

namespace MATSDK
{
  public class MATDelegate : MonoBehaviour
  {
    public void trackerDidSucceed(string data)
    {
    }

    public void trackerDidFail(string error)
    {
      MonoBehaviour.print((object) ("MATDelegate trackerDidFail: " + error));
    }

    public void trackerDidEnqueueRequest(string refId)
    {
      MonoBehaviour.print((object) ("MATDelegate trackerDidEnqueueRequest: " + refId));
    }

    public void trackerDidReceiveDeepLink(string url)
    {
      MonoBehaviour.print((object) ("MATDelegate trackerDidReceiveDeepLink: " + url));
    }

    public static string DecodeFrom64(string encodedString)
    {
      MonoBehaviour.print((object) "MATDelegate.DecodeFrom64(string)");
      return Encoding.UTF8.GetString(Convert.FromBase64String(encodedString));
    }
  }
}
