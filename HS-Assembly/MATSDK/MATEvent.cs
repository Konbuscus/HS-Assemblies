﻿// Decompiled with JetBrains decompiler
// Type: MATSDK.MATEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

namespace MATSDK
{
  public struct MATEvent
  {
    public string name;
    public int? id;
    public double? revenue;
    public string currencyCode;
    public string advertiserRefId;
    public MATItem[] eventItems;
    public int? transactionState;
    public string receipt;
    public string receiptSignature;
    public string contentType;
    public string contentId;
    public int? level;
    public int? quantity;
    public string searchString;
    public double? rating;
    public DateTime? date1;
    public DateTime? date2;
    public string attribute1;
    public string attribute2;
    public string attribute3;
    public string attribute4;
    public string attribute5;

    private MATEvent(int dummy1, int dummy2)
    {
      this.name = (string) null;
      this.id = new int?();
      this.revenue = new double?();
      this.currencyCode = (string) null;
      this.advertiserRefId = (string) null;
      this.eventItems = (MATItem[]) null;
      this.transactionState = new int?();
      this.receipt = (string) null;
      this.receiptSignature = (string) null;
      this.contentType = (string) null;
      this.contentId = (string) null;
      this.level = new int?();
      this.quantity = new int?();
      this.searchString = (string) null;
      this.rating = new double?();
      this.date1 = new DateTime?();
      this.date2 = new DateTime?();
      this.attribute1 = (string) null;
      this.attribute2 = (string) null;
      this.attribute3 = (string) null;
      this.attribute4 = (string) null;
      this.attribute5 = (string) null;
    }

    public MATEvent(string name)
    {
      this = new MATEvent(0, 0);
      this.name = name;
    }

    public MATEvent(int id)
    {
      this = new MATEvent(0, 0);
      this.id = new int?(id);
    }
  }
}
