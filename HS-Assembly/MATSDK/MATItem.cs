﻿// Decompiled with JetBrains decompiler
// Type: MATSDK.MATItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

namespace MATSDK
{
  public struct MATItem
  {
    public string name;
    public double? unitPrice;
    public int? quantity;
    public double? revenue;
    public string attribute1;
    public string attribute2;
    public string attribute3;
    public string attribute4;
    public string attribute5;

    public MATItem(string name)
    {
      this.name = name;
      this.unitPrice = new double?();
      this.quantity = new int?();
      this.revenue = new double?();
      this.attribute1 = (string) null;
      this.attribute2 = (string) null;
      this.attribute3 = (string) null;
      this.attribute4 = (string) null;
      this.attribute5 = (string) null;
    }
  }
}
