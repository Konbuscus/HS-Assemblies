﻿// Decompiled with JetBrains decompiler
// Type: MATSDK.MATEventIos
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

namespace MATSDK
{
  internal struct MATEventIos
  {
    public string name;
    public string eventId;
    public string revenue;
    public string currencyCode;
    public string advertiserRefId;
    public string transactionState;
    public string contentType;
    public string contentId;
    public string level;
    public string quantity;
    public string searchString;
    public string rating;
    public string date1;
    public string date2;
    public string attribute1;
    public string attribute2;
    public string attribute3;
    public string attribute4;
    public string attribute5;

    private MATEventIos(int dummy1, int dummy2)
    {
      this.eventId = (string) null;
      this.name = (string) null;
      this.revenue = (string) null;
      this.currencyCode = (string) null;
      this.advertiserRefId = (string) null;
      this.transactionState = (string) null;
      this.contentType = (string) null;
      this.contentId = (string) null;
      this.level = (string) null;
      this.quantity = (string) null;
      this.searchString = (string) null;
      this.rating = (string) null;
      this.date1 = (string) null;
      this.date2 = (string) null;
      this.attribute1 = (string) null;
      this.attribute2 = (string) null;
      this.attribute3 = (string) null;
      this.attribute4 = (string) null;
      this.attribute5 = (string) null;
    }

    public MATEventIos(string name)
    {
      this = new MATEventIos(0, 0);
      this.name = name;
    }

    public MATEventIos(int id)
    {
      this = new MATEventIos(0, 0);
      this.eventId = id.ToString();
    }

    public MATEventIos(MATEvent matEvent)
    {
      this.name = matEvent.name;
      this.eventId = matEvent.name != null ? (string) null : matEvent.id.ToString();
      this.advertiserRefId = matEvent.advertiserRefId;
      this.attribute1 = matEvent.attribute1;
      this.attribute2 = matEvent.attribute2;
      this.attribute3 = matEvent.attribute3;
      this.attribute4 = matEvent.attribute4;
      this.attribute5 = matEvent.attribute5;
      this.contentId = matEvent.contentId != null ? matEvent.contentId.ToString() : (string) null;
      this.contentType = matEvent.contentType;
      this.currencyCode = matEvent.currencyCode;
      this.level = matEvent.level.HasValue ? matEvent.level.ToString() : (string) null;
      this.quantity = matEvent.quantity.HasValue ? matEvent.quantity.ToString() : (string) null;
      this.rating = matEvent.rating.HasValue ? matEvent.rating.ToString() : (string) null;
      this.revenue = matEvent.revenue.HasValue ? matEvent.revenue.ToString() : (string) null;
      this.searchString = matEvent.searchString;
      this.transactionState = matEvent.transactionState.HasValue ? matEvent.transactionState.ToString() : (string) null;
      this.date1 = (string) null;
      this.date2 = (string) null;
      DateTime dateTime = new DateTime(1970, 1, 1);
      if (matEvent.date1.HasValue)
        this.date1 = (new TimeSpan(matEvent.date1.Value.Ticks).TotalMilliseconds - new TimeSpan(dateTime.Ticks).TotalMilliseconds).ToString();
      if (!matEvent.date2.HasValue)
        return;
      this.date2 = (new TimeSpan(matEvent.date2.Value.Ticks).TotalMilliseconds - new TimeSpan(dateTime.Ticks).TotalMilliseconds).ToString();
    }
  }
}
