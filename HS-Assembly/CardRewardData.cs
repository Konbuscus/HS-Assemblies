﻿// Decompiled with JetBrains decompiler
// Type: CardRewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class CardRewardData : RewardData
{
  public string CardID { get; set; }

  public TAG_PREMIUM Premium { get; set; }

  public int Count { get; set; }

  public CardRewardData.InnKeeperTrigger InnKeeperLine { get; set; }

  public FixedRewardMapDbfRecord FixedReward { get; set; }

  public CardRewardData()
    : this(string.Empty, TAG_PREMIUM.NORMAL, 0)
  {
  }

  public CardRewardData(string cardID, TAG_PREMIUM premium, int count)
    : base(Reward.Type.CARD)
  {
    this.CardID = cardID;
    this.Count = count;
    this.Premium = premium;
    this.InnKeeperLine = CardRewardData.InnKeeperTrigger.NONE;
  }

  public override string ToString()
  {
    EntityDef entityDef = DefLoader.Get().GetEntityDef(this.CardID);
    return string.Format("[CardRewardData: cardName={0} CardID={1}, Premium={2} Count={3} Origin={4} OriginData={5}]", (object) (entityDef != null ? entityDef.GetName() : "[UNKNOWN]"), (object) this.CardID, (object) this.Premium, (object) this.Count, (object) this.Origin, (object) this.OriginData);
  }

  public bool Merge(CardRewardData other)
  {
    if (!this.CardID.Equals(other.CardID) || !this.Premium.Equals((object) other.Premium))
      return false;
    this.Count += other.Count;
    using (List<long>.Enumerator enumerator = other.m_noticeIDs.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.AddNoticeID(enumerator.Current);
    }
    return true;
  }

  protected override string GetGameObjectName()
  {
    return "CardReward";
  }

  public enum InnKeeperTrigger
  {
    NONE,
    CORE_CLASS_SET_COMPLETE,
    SECOND_REWARD_EVER,
  }
}
