﻿// Decompiled with JetBrains decompiler
// Type: CraftingTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CraftingTray : MonoBehaviour
{
  private static PlatformDependentValue<int> MASS_DISENCHANT_MATERIAL_TO_SWITCH = new PlatformDependentValue<int>(PlatformCategory.Screen)
  {
    PC = 0,
    Phone = 1
  };
  public UIBButton m_doneButton;
  public PegUIElement m_massDisenchantButton;
  public UberText m_potentialDustAmount;
  public UberText m_massDisenchantText;
  public CheckBox m_showGoldenCheckbox;
  public CheckBox m_showSoulboundCheckbox;
  public HighlightState m_highlight;
  public GameObject m_massDisenchantMesh;
  public Material m_massDisenchantMaterial;
  public Material m_massDisenchantDisabledMaterial;
  private int m_dustAmount;
  private bool m_shown;
  private static CraftingTray s_instance;

  private void Awake()
  {
    CraftingTray.s_instance = this;
  }

  private void Start()
  {
    this.m_doneButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDoneButtonReleased));
    this.m_massDisenchantButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnMassDisenchantButtonReleased));
    this.m_massDisenchantButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnMassDisenchantButtonOver));
    this.m_massDisenchantButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnMassDisenchantButtonOut));
    this.m_showGoldenCheckbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ToggleShowGolden));
    this.m_showGoldenCheckbox.SetChecked(false);
    this.m_showSoulboundCheckbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ToggleShowSoulbound));
    this.m_showSoulboundCheckbox.SetChecked(false);
    this.SetMassDisenchantAmount();
  }

  private void OnDestroy()
  {
    CraftingTray.s_instance = (CraftingTray) null;
  }

  public static CraftingTray Get()
  {
    return CraftingTray.s_instance;
  }

  public void UpdateMassDisenchantAmount()
  {
    if (this.m_dustAmount > 0)
    {
      Material[] materials = this.m_massDisenchantMesh.GetComponent<Renderer>().materials;
      materials[(int) CraftingTray.MASS_DISENCHANT_MATERIAL_TO_SWITCH] = this.m_massDisenchantMaterial;
      this.m_massDisenchantMesh.GetComponent<Renderer>().materials = materials;
      this.m_highlight.gameObject.SetActive(true);
      this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
      this.m_massDisenchantButton.SetEnabled(true);
      this.m_massDisenchantText.gameObject.SetActive(true);
      this.m_potentialDustAmount.gameObject.SetActive(true);
    }
    else
    {
      Material[] materials = this.m_massDisenchantMesh.GetComponent<Renderer>().materials;
      materials[(int) CraftingTray.MASS_DISENCHANT_MATERIAL_TO_SWITCH] = this.m_massDisenchantDisabledMaterial;
      this.m_massDisenchantMesh.GetComponent<Renderer>().materials = materials;
      this.m_highlight.gameObject.SetActive(false);
      this.m_massDisenchantButton.SetEnabled(false);
      this.m_massDisenchantText.gameObject.SetActive(false);
      this.m_potentialDustAmount.gameObject.SetActive(false);
    }
  }

  public void SetMassDisenchantAmount()
  {
    if (!this.gameObject.activeSelf)
      return;
    this.StartCoroutine(this.SetMassDisenchantAmountWhenReady());
  }

  [DebuggerHidden]
  private IEnumerator SetMassDisenchantAmountWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CraftingTray.\u003CSetMassDisenchantAmountWhenReady\u003Ec__Iterator4F()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void Show(bool? overrideShowSoulbound = null, bool? overrideShowGolden = null, bool updatePage = true)
  {
    this.m_shown = true;
    PresenceMgr.Get().SetStatus((Enum) PresenceStatus.CRAFTING);
    if (overrideShowSoulbound.HasValue)
      this.m_showSoulboundCheckbox.SetChecked(overrideShowSoulbound.Value);
    if (overrideShowGolden.HasValue)
      this.m_showGoldenCheckbox.SetChecked(overrideShowGolden.Value);
    this.SetMassDisenchantAmount();
    CollectionManagerDisplay.Get().m_pageManager.ShowCraftingModeCards(!this.m_showSoulboundCheckbox.IsChecked(), this.m_showGoldenCheckbox.IsChecked(), updatePage, false);
  }

  public void Hide()
  {
    this.m_shown = false;
    PresenceMgr.Get().SetPrevStatus();
    CollectionManagerDisplay.Get().HideCraftingTray();
    CollectionManagerDisplay.Get().m_pageManager.HideMassDisenchant();
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  private void OnDoneButtonReleased(UIEvent e)
  {
    this.Hide();
  }

  private void OnMassDisenchantButtonReleased(UIEvent e)
  {
    if (CollectionManagerDisplay.Get().m_pageManager.ArePagesTurning())
      return;
    if (CollectionManagerDisplay.Get().m_pageManager.IsShowingMassDisenchant())
    {
      CollectionManagerDisplay.Get().m_pageManager.HideMassDisenchant();
      this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
    }
    else
    {
      CollectionManagerDisplay.Get().m_pageManager.ShowMassDisenchant();
      this.StartCoroutine(MassDisenchant.Get().StartHighlight());
    }
    SoundManager.Get().LoadAndPlay("Hub_Click");
  }

  private void OnMassDisenchantButtonOver(UIEvent e)
  {
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
    SoundManager.Get().LoadAndPlay("Hub_Mouseover");
  }

  private void OnMassDisenchantButtonOut(UIEvent e)
  {
    if (CollectionManagerDisplay.Get().m_pageManager.IsShowingMassDisenchant())
      return;
    int num = 0;
    try
    {
      num = int.Parse(this.m_potentialDustAmount.Text);
    }
    catch (Exception ex)
    {
      Log.All.PrintWarning("Exception when attempting to parse CraftingTray's m_potentialDustAmount! Exception: {0}", (object) ex);
    }
    if (num > 0)
      this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    else
      this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }

  private void ToggleShowGolden(UIEvent e)
  {
    bool showGolden = this.m_showGoldenCheckbox.IsChecked();
    CollectionManagerDisplay.Get().m_pageManager.ShowCraftingModeCards(!this.m_showSoulboundCheckbox.IsChecked(), showGolden, true, true);
    if (showGolden)
      SoundManager.Get().LoadAndPlay("checkbox_toggle_on", this.gameObject);
    else
      SoundManager.Get().LoadAndPlay("checkbox_toggle_off", this.gameObject);
  }

  private void ToggleShowSoulbound(UIEvent e)
  {
    bool flag = this.m_showSoulboundCheckbox.IsChecked();
    CollectionManagerDisplay.Get().m_pageManager.ShowCraftingModeCards(!flag, this.m_showGoldenCheckbox.IsChecked(), true, true);
    if (flag)
      SoundManager.Get().LoadAndPlay("checkbox_toggle_on", this.gameObject);
    else
      SoundManager.Get().LoadAndPlay("checkbox_toggle_off", this.gameObject);
  }
}
