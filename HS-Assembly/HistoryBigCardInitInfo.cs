﻿// Decompiled with JetBrains decompiler
// Type: HistoryBigCardInitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class HistoryBigCardInitInfo : HistoryItemInitInfo
{
  public HistoryManager.BigCardFinishedCallback m_finishedCallback;
  public bool m_countered;
  public bool m_waitForSecretSpell;
  public bool m_fromMetaData;
  public Entity m_postTransformedEntity;
}
