﻿// Decompiled with JetBrains decompiler
// Type: FixedRewardsMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class FixedRewardsMgr
{
  private HashSet<NetCache.CardDefinition> m_craftableCardRewards = new HashSet<NetCache.CardDefinition>();
  private Map<int, FixedRewardsMgr.FixedMetaActionReward> m_earnedMetaActionRewards = new Map<int, FixedRewardsMgr.FixedMetaActionReward>();
  private Map<RewardVisualTiming, HashSet<FixedRewardsMgr.RewardMapIDToShow>> m_rewardMapIDsToShow = new Map<RewardVisualTiming, HashSet<FixedRewardsMgr.RewardMapIDToShow>>();
  private HashSet<int> m_rewardMapIDsAwarded = new HashSet<int>();
  private bool m_isInitialization = true;
  private static FixedRewardsMgr s_instance;
  private bool m_registeredForAdventureProgressUpdates;
  private bool m_registeredForProfileNotices;
  private bool m_registeredForCompletedAchieves;
  private bool m_isStartupFinished;

  public static void Initialize()
  {
    FixedRewardsMgr.Get();
    AccountLicenseMgr.Get().RegisterAccountLicensesChangedListener(new AccountLicenseMgr.AccountLicensesChangedCallback(FixedRewardsMgr.s_instance.OnAccountLicensesUpdate));
  }

  public static FixedRewardsMgr Get()
  {
    if (FixedRewardsMgr.s_instance == null)
    {
      FixedRewardsMgr.s_instance = new FixedRewardsMgr();
      ApplicationMgr.Get().WillReset += new Action(FixedRewardsMgr.s_instance.WillReset);
    }
    if (!FixedRewardsMgr.s_instance.m_registeredForAdventureProgressUpdates)
      FixedRewardsMgr.s_instance.m_registeredForAdventureProgressUpdates = AdventureProgressMgr.Get().RegisterProgressUpdatedListener(new AdventureProgressMgr.AdventureProgressUpdatedCallback(FixedRewardsMgr.s_instance.OnAdventureProgressUpdate));
    if (!FixedRewardsMgr.s_instance.m_registeredForProfileNotices)
    {
      NetCache.Get().RegisterNewNoticesListener(new NetCache.DelNewNoticesListener(FixedRewardsMgr.s_instance.OnNewNotices));
      FixedRewardsMgr.s_instance.m_registeredForProfileNotices = true;
    }
    if (!FixedRewardsMgr.s_instance.m_registeredForCompletedAchieves)
    {
      AchieveManager.Get().RegisterAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(FixedRewardsMgr.s_instance.OnAchievesUpdated), (object) null);
      FixedRewardsMgr.s_instance.m_registeredForCompletedAchieves = true;
    }
    return FixedRewardsMgr.s_instance;
  }

  public void CheckForTutorialComplete()
  {
    List<CardRewardData> cardRewards = new List<CardRewardData>();
    this.CheckForTutorialComplete(cardRewards);
    CollectionManager.Get().AddCardRewards(cardRewards, false);
  }

  public void CheckForTutorialComplete(List<CardRewardData> cardRewards)
  {
    NetCache.NetCacheProfileProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>();
    if (netObject == null)
      UnityEngine.Debug.LogWarning((object) string.Format("FixedRewardsMgr.CheckForTutorialComplete(): null == NetCache.NetCacheProfileProgress"));
    else
      this.TriggerTutorialProgressAction(FixedRewardsMgr.ShowVisualOption.DO_NOT_SHOW, (int) netObject.CampaignProgress, cardRewards);
  }

  public bool IsStartupFinished()
  {
    return this.m_isStartupFinished;
  }

  public void InitStartupFixedRewards()
  {
    List<CardRewardData> cardRewards = new List<CardRewardData>();
    List<AdventureMission.WingProgress> allProgress = AdventureProgressMgr.Get().GetAllProgress();
    this.m_isInitialization = true;
    using (List<AdventureMission.WingProgress>.Enumerator enumerator = allProgress.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureMission.WingProgress current = enumerator.Current;
        if (current.MeetsFlagsRequirement(1UL))
        {
          this.TriggerWingProgressAction(FixedRewardsMgr.ShowVisualOption.DO_NOT_SHOW, current.Wing, current.Progress, cardRewards);
          this.TriggerWingFlagsAction(FixedRewardsMgr.ShowVisualOption.DO_NOT_SHOW, current.Wing, current.Flags, cardRewards);
        }
      }
    }
    this.GrantAchieveRewards(cardRewards);
    this.m_isInitialization = false;
    this.CheckForTutorialComplete(cardRewards);
    this.GrantHeroLevelRewards(cardRewards);
    using (List<AccountLicenseInfo>.Enumerator enumerator = AccountLicenseMgr.Get().GetAllOwnedAccountLicenseInfo().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AccountLicenseInfo current = enumerator.Current;
        this.TriggerAccountLicenseFlagsAction(FixedRewardsMgr.ShowVisualOption.DO_NOT_SHOW, current.License, current.Flags_, cardRewards);
      }
    }
    CollectionManager.Get().AddCardRewards(cardRewards, false);
    this.m_isStartupFinished = true;
    CollectionManager.Get().FixedRewardsStartupComplete();
  }

  public bool ShowFixedRewards(UserAttentionBlocker blocker, HashSet<RewardVisualTiming> rewardVisualTimings, FixedRewardsMgr.DelOnAllFixedRewardsShown allRewardsShownCallback, FixedRewardsMgr.DelPositionNonToastReward positionNonToastRewardCallback, Vector3 rewardPunchScale, Vector3 rewardScale)
  {
    return this.ShowFixedRewards(blocker, rewardVisualTimings, allRewardsShownCallback, positionNonToastRewardCallback, rewardPunchScale, rewardScale, (object) null);
  }

  public bool HasRewardsToShow(RewardVisualTiming rewardVisualTiming)
  {
    if (this.m_rewardMapIDsToShow.ContainsKey(rewardVisualTiming))
      return this.m_rewardMapIDsToShow[rewardVisualTiming].Count > 0;
    return false;
  }

  public bool ShowFixedRewards(UserAttentionBlocker blocker, HashSet<RewardVisualTiming> rewardVisualTimings, FixedRewardsMgr.DelOnAllFixedRewardsShown allRewardsShownCallback, FixedRewardsMgr.DelPositionNonToastReward positionNonToastRewardCallback, Vector3 rewardPunchScale, Vector3 rewardScale, object userData)
  {
    if (!UserAttentionManager.CanShowAttentionGrabber(blocker, "FixedRewardsMgr.ShowFixedRewards:" + (object) blocker))
      return false;
    FixedRewardsMgr.OnAllFixedRewardsShownCallbackInfo callbackInfo = new FixedRewardsMgr.OnAllFixedRewardsShownCallbackInfo() { m_rewardMapIDsToShow = new List<FixedRewardsMgr.RewardMapIDToShow>(), m_onAllRewardsShownCallback = allRewardsShownCallback, m_positionNonToastRewardCallback = positionNonToastRewardCallback, m_rewardPunchScale = rewardPunchScale, m_rewardScale = rewardScale, m_userData = userData, m_showingCheatRewards = false };
    using (HashSet<RewardVisualTiming>.Enumerator enumerator = rewardVisualTimings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RewardVisualTiming current = enumerator.Current;
        if (this.m_rewardMapIDsToShow.ContainsKey(current))
        {
          callbackInfo.m_rewardMapIDsToShow.AddRange((IEnumerable<FixedRewardsMgr.RewardMapIDToShow>) this.m_rewardMapIDsToShow[current]);
          this.m_rewardMapIDsToShow[current].Clear();
        }
      }
    }
    if (callbackInfo.m_rewardMapIDsToShow.Count == 0)
      return false;
    callbackInfo.m_rewardMapIDsToShow.Sort((Comparison<FixedRewardsMgr.RewardMapIDToShow>) ((a, b) =>
    {
      if (a.SortOrder < b.SortOrder)
        return -1;
      return a.SortOrder > b.SortOrder ? 1 : 0;
    }));
    this.ShowFixedRewards_Internal(blocker, callbackInfo);
    return true;
  }

  public bool Cheat_ShowFixedReward(int fixedRewardMapID, FixedRewardsMgr.DelPositionNonToastReward positionNonToastRewardCallback, Vector3 rewardPunchScale, Vector3 rewardScale)
  {
    if (!ApplicationMgr.IsInternal())
      return false;
    FixedRewardMapDbfRecord record = GameDbf.FixedRewardMap.GetRecord(fixedRewardMapID);
    int sortOrder = record != null ? record.SortOrder : 0;
    this.ShowFixedRewards_Internal(UserAttentionBlocker.NONE, new FixedRewardsMgr.OnAllFixedRewardsShownCallbackInfo()
    {
      m_rewardMapIDsToShow = new List<FixedRewardsMgr.RewardMapIDToShow>()
      {
        new FixedRewardsMgr.RewardMapIDToShow(fixedRewardMapID, FixedRewardsMgr.RewardMapIDToShow.NO_ACHIEVE_ID, sortOrder)
      },
      m_onAllRewardsShownCallback = (FixedRewardsMgr.DelOnAllFixedRewardsShown) null,
      m_positionNonToastRewardCallback = positionNonToastRewardCallback,
      m_rewardPunchScale = rewardPunchScale,
      m_rewardScale = rewardScale,
      m_userData = (object) null,
      m_showingCheatRewards = true
    });
    return true;
  }

  public bool CanCraftCard(string cardID, TAG_PREMIUM premium)
  {
    if (GameUtils.GetFixedRewardForCard(cardID, premium) == null)
      return true;
    NetCache.CardDefinition cardDefinition = new NetCache.CardDefinition() { Name = cardID, Premium = premium };
    if (GameUtils.IsCardRotated(cardID))
      return true;
    return this.m_craftableCardRewards.Contains(cardDefinition);
  }

  public List<RewardData> GetRewardsForWing(int wingID, HashSet<RewardVisualTiming> rewardTimings)
  {
    List<RewardData> rewardDataList = new List<RewardData>();
    List<FixedRewardActionDbfRecord> rewardActionDbfRecordList = new List<FixedRewardActionDbfRecord>();
    rewardActionDbfRecordList.AddRange((IEnumerable<FixedRewardActionDbfRecord>) GameUtils.GetFixedActionRecords(FixedActionType.WING_PROGRESS));
    rewardActionDbfRecordList.AddRange((IEnumerable<FixedRewardActionDbfRecord>) GameUtils.GetFixedActionRecords(FixedActionType.WING_FLAGS));
    using (List<FixedRewardActionDbfRecord>.Enumerator enumerator = rewardActionDbfRecordList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardActionDbfRecord current = enumerator.Current;
        if (current.WingId == wingID)
          rewardDataList.AddRange((IEnumerable<RewardData>) this.GetRewardsForAction(current.ID, rewardTimings));
      }
    }
    return rewardDataList;
  }

  public List<RewardData> GetRewardsForWingProgress(int wingID, int progress, HashSet<RewardVisualTiming> rewardTimings)
  {
    List<RewardData> rewardDataList = new List<RewardData>();
    using (List<FixedRewardActionDbfRecord>.Enumerator enumerator = GameUtils.GetFixedActionRecords(FixedActionType.WING_PROGRESS).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardActionDbfRecord current = enumerator.Current;
        if (current.WingId == wingID && current.WingProgress == progress)
          rewardDataList.AddRange((IEnumerable<RewardData>) this.GetRewardsForAction(current.ID, rewardTimings));
      }
    }
    return rewardDataList;
  }

  public List<RewardData> GetRewardsForWingFlags(int wingID, ulong flags, HashSet<RewardVisualTiming> rewardTimings)
  {
    List<RewardData> rewardDataList = new List<RewardData>();
    using (List<FixedRewardActionDbfRecord>.Enumerator enumerator = GameUtils.GetFixedActionRecords(FixedActionType.WING_FLAGS).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardActionDbfRecord current = enumerator.Current;
        if (current.WingId == wingID && (long) current.WingFlags == (long) flags)
          rewardDataList.AddRange((IEnumerable<RewardData>) this.GetRewardsForAction(current.ID, rewardTimings));
      }
    }
    return rewardDataList;
  }

  private void WillReset()
  {
    this.m_craftableCardRewards.Clear();
    this.m_earnedMetaActionRewards.Clear();
    this.m_rewardMapIDsToShow.Clear();
    this.m_rewardMapIDsAwarded.Clear();
    AdventureProgressMgr.Get().RemoveProgressUpdatedListener(new AdventureProgressMgr.AdventureProgressUpdatedCallback(this.OnAdventureProgressUpdate));
    this.m_registeredForAdventureProgressUpdates = false;
    NetCache.Get().RemoveNewNoticesListener(new NetCache.DelNewNoticesListener(this.OnNewNotices));
    this.m_registeredForProfileNotices = false;
    AchieveManager.Get().RemoveAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(this.OnAchievesUpdated));
    this.m_registeredForCompletedAchieves = false;
    this.m_isStartupFinished = false;
  }

  private void OnAdventureProgressUpdate(bool isStartupAction, AdventureMission.WingProgress oldProgress, AdventureMission.WingProgress newProgress, object userData)
  {
    List<CardRewardData> cardRewards = new List<CardRewardData>();
    if (isStartupAction || newProgress == null || !newProgress.IsOwned())
      return;
    if (oldProgress == null)
    {
      this.TriggerWingProgressAction(FixedRewardsMgr.ShowVisualOption.SHOW, newProgress.Wing, newProgress.Progress, cardRewards);
      this.TriggerWingFlagsAction(FixedRewardsMgr.ShowVisualOption.SHOW, newProgress.Wing, newProgress.Flags, cardRewards);
    }
    else
    {
      bool flag = !oldProgress.IsOwned() && newProgress.IsOwned();
      if (flag || oldProgress.Progress != newProgress.Progress)
        this.TriggerWingProgressAction(!flag ? FixedRewardsMgr.ShowVisualOption.SHOW : FixedRewardsMgr.ShowVisualOption.DO_NOT_SHOW, newProgress.Wing, newProgress.Progress, cardRewards);
      if ((long) oldProgress.Flags != (long) newProgress.Flags)
        this.TriggerWingFlagsAction(FixedRewardsMgr.ShowVisualOption.SHOW, newProgress.Wing, newProgress.Flags, cardRewards);
    }
    CollectionManager.Get().AddCardRewards(cardRewards, false);
  }

  private void OnNewNotices(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList)
  {
    List<CardRewardData> cardRewards = new List<CardRewardData>();
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = newNotices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.ProfileNotice current = enumerator.Current;
        if (current.Type == NetCache.ProfileNotice.NoticeType.HERO_LEVEL_UP)
        {
          NetCache.ProfileNoticeLevelUp profileNoticeLevelUp = current as NetCache.ProfileNoticeLevelUp;
          FixedRewardsMgr.Get().TriggerHeroLevelAction(FixedRewardsMgr.ShowVisualOption.SHOW, profileNoticeLevelUp.HeroClass, profileNoticeLevelUp.NewLevel, cardRewards);
          Network.AckNotice(current.NoticeID);
        }
      }
    }
    if (CollectionManager.Get() == null)
      return;
    CollectionManager.Get().AddCardRewards(cardRewards, false);
  }

  private void OnAchievesUpdated(List<Achievement> updatedAchieves, List<Achievement> achieves, object userData)
  {
    List<CardRewardData> cardRewards = new List<CardRewardData>();
    using (List<Achievement>.Enumerator enumerator = achieves.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.TriggerAchieveAction(FixedRewardsMgr.ShowVisualOption.SHOW, enumerator.Current.ID, cardRewards);
    }
    if (CollectionManager.Get() == null)
      return;
    CollectionManager.Get().AddCardRewards(cardRewards, false);
  }

  private void OnAccountLicensesUpdate(List<AccountLicenseInfo> changedAccountLicenses, object userData)
  {
    ApplicationMgr.Get().StartCoroutine(this.OnAccountLicensesUpdate_DoWork(changedAccountLicenses));
  }

  [DebuggerHidden]
  private IEnumerator OnAccountLicensesUpdate_DoWork(List<AccountLicenseInfo> changedAccountLicenses)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FixedRewardsMgr.\u003COnAccountLicensesUpdate_DoWork\u003Ec__Iterator309() { changedAccountLicenses = changedAccountLicenses, \u003C\u0024\u003EchangedAccountLicenses = changedAccountLicenses, \u003C\u003Ef__this = this };
  }

  private FixedRewardsMgr.FixedMetaActionReward GetEarnedMetaActionReward(int metaActionID)
  {
    if (!this.m_earnedMetaActionRewards.ContainsKey(metaActionID))
      this.m_earnedMetaActionRewards[metaActionID] = new FixedRewardsMgr.FixedMetaActionReward(metaActionID);
    return this.m_earnedMetaActionRewards[metaActionID];
  }

  private void UpdateEarnedMetaActionFlags(int metaActionID, ulong addFlags, ulong removeFlags)
  {
    this.GetEarnedMetaActionReward(metaActionID).UpdateFlags(addFlags, removeFlags);
  }

  private NetCache.CardDefinition GetCardDefinition(FixedRewardDbfRecord dbfRecReward)
  {
    string cardId = GameUtils.TranslateDbIdToCardId(dbfRecReward.CardId);
    if (cardId == null)
      return (NetCache.CardDefinition) null;
    TAG_PREMIUM outVal;
    if (!EnumUtils.TryCast<TAG_PREMIUM>((object) dbfRecReward.CardPremium, out outVal))
      return (NetCache.CardDefinition) null;
    return new NetCache.CardDefinition() { Name = cardId, Premium = outVal };
  }

  private FixedRewardsMgr.FixedReward GetFixedReward(FixedRewardMapDbfRecord fixedRewardMapRecord)
  {
    FixedRewardsMgr.FixedReward fixedReward = new FixedRewardsMgr.FixedReward();
    FixedRewardDbfRecord record = GameDbf.FixedReward.GetRecord(fixedRewardMapRecord.RewardId);
    FixedRewardType outVal;
    if (record == null || !EnumUtils.TryGetEnum<FixedRewardType>(record.Type, out outVal))
      return fixedReward;
    fixedReward.Type = outVal;
    switch (outVal)
    {
      case FixedRewardType.VIRTUAL_CARD:
      case FixedRewardType.REAL_CARD:
        NetCache.CardDefinition cardDefinition1 = this.GetCardDefinition(record);
        if (cardDefinition1 != null)
        {
          fixedReward.FixedCardRewardData = new CardRewardData(cardDefinition1.Name, cardDefinition1.Premium, fixedRewardMapRecord.RewardCount);
          fixedReward.FixedCardRewardData.FixedReward = fixedRewardMapRecord;
          break;
        }
        break;
      case FixedRewardType.CARD_BACK:
        int cardBackId = record.CardBackId;
        fixedReward.FixedCardBackRewardData = new CardBackRewardData(cardBackId);
        break;
      case FixedRewardType.CRAFTABLE_CARD:
        NetCache.CardDefinition cardDefinition2 = this.GetCardDefinition(record);
        if (cardDefinition2 != null)
        {
          fixedReward.FixedCraftableCardRewardData = cardDefinition2;
          break;
        }
        break;
      case FixedRewardType.META_ACTION_FLAGS:
        int metaActionId = record.MetaActionId;
        ulong metaActionFlags = record.MetaActionFlags;
        fixedReward.FixedMetaActionRewardData = new FixedRewardsMgr.FixedMetaActionReward(metaActionId);
        fixedReward.FixedMetaActionRewardData.UpdateFlags(metaActionFlags, 0UL);
        break;
    }
    return fixedReward;
  }

  private bool QueueRewardVisual(FixedRewardMapDbfRecord fixedRewardMapRecord, int achieveID)
  {
    int id = fixedRewardMapRecord.ID;
    RewardVisualTiming outVal;
    if (!EnumUtils.TryGetEnum<RewardVisualTiming>(fixedRewardMapRecord.RewardTiming, out outVal))
    {
      UnityEngine.Debug.LogWarning((object) string.Format("QueueRewardVisual rewardMapID={0} no enum value for reward visual timing {1}, check fixed rewards map", (object) id, (object) outVal));
      outVal = RewardVisualTiming.IMMEDIATE;
    }
    Log.Achievements.Print("QueueRewardVisual achieveID=" + (object) achieveID + " fixedRewardMapId=" + (object) fixedRewardMapRecord.ID + " " + fixedRewardMapRecord.NoteDesc + " " + (object) outVal);
    if (outVal == RewardVisualTiming.NEVER)
      return false;
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    if (outVal == RewardVisualTiming.OUT_OF_BAND && mode != SceneMgr.Mode.LOGIN)
      return false;
    if (!this.m_rewardMapIDsToShow.ContainsKey(outVal))
      this.m_rewardMapIDsToShow[outVal] = new HashSet<FixedRewardsMgr.RewardMapIDToShow>();
    int sortOrder = fixedRewardMapRecord.SortOrder;
    this.m_rewardMapIDsToShow[outVal].Add(new FixedRewardsMgr.RewardMapIDToShow(fixedRewardMapRecord.ID, achieveID, sortOrder));
    return true;
  }

  private void TriggerRewardsForAction(int actionID, FixedRewardsMgr.ShowVisualOption showRewardVisual, List<CardRewardData> cardRewards)
  {
    this.TriggerRewardsForAction(actionID, showRewardVisual, cardRewards, FixedRewardsMgr.RewardMapIDToShow.NO_ACHIEVE_ID);
  }

  private void TriggerRewardsForAction(int actionID, FixedRewardsMgr.ShowVisualOption showRewardVisual, List<CardRewardData> cardRewards, int achieveID)
  {
    using (List<FixedRewardMapDbfRecord>.Enumerator enumerator = GameUtils.GetFixedRewardMapRecordsForAction(actionID).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardMapDbfRecord current = enumerator.Current;
        int id = current.ID;
        if (this.m_rewardMapIDsAwarded.Contains(id))
        {
          if (showRewardVisual != FixedRewardsMgr.ShowVisualOption.FORCE_SHOW)
            continue;
        }
        else
          this.m_rewardMapIDsAwarded.Add(id);
        if (current.RewardCount > 0)
        {
          bool flag1 = showRewardVisual != FixedRewardsMgr.ShowVisualOption.DO_NOT_SHOW;
          FixedRewardsMgr.FixedReward fixedReward = this.GetFixedReward(current);
          if (fixedReward.FixedCardRewardData != null)
          {
            bool flag2;
            if (this.m_isInitialization)
            {
              flag1 = false;
              flag2 = fixedReward.Type == FixedRewardType.VIRTUAL_CARD;
            }
            else
              flag2 = true;
            if ((!flag1 || !this.QueueRewardVisual(current, achieveID)) && flag2)
              cardRewards.Add(fixedReward.FixedCardRewardData);
          }
          if (fixedReward.FixedCardBackRewardData != null && (!flag1 || !this.QueueRewardVisual(current, achieveID)))
            CardBackManager.Get().AddNewCardBack(fixedReward.FixedCardBackRewardData.CardBackID);
          if (fixedReward.FixedCraftableCardRewardData != null)
            this.m_craftableCardRewards.Add(fixedReward.FixedCraftableCardRewardData);
          if (fixedReward.FixedMetaActionRewardData != null)
          {
            this.UpdateEarnedMetaActionFlags(fixedReward.FixedMetaActionRewardData.MetaActionID, fixedReward.FixedMetaActionRewardData.MetaActionFlags, 0UL);
            this.TriggerMetaActionFlagsAction(showRewardVisual, fixedReward.FixedMetaActionRewardData.MetaActionID, cardRewards);
          }
        }
      }
    }
  }

  private void TriggerWingProgressAction(FixedRewardsMgr.ShowVisualOption showRewardVisual, int wingID, int progress, List<CardRewardData> cardRewards)
  {
    using (List<FixedRewardActionDbfRecord>.Enumerator enumerator = GameUtils.GetFixedActionRecords(FixedActionType.WING_PROGRESS).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardActionDbfRecord current = enumerator.Current;
        if (current.WingId == wingID && current.WingProgress <= progress)
          this.TriggerRewardsForAction(current.ID, showRewardVisual, cardRewards);
      }
    }
  }

  private void TriggerWingFlagsAction(FixedRewardsMgr.ShowVisualOption showRewardVisual, int wingID, ulong flags, List<CardRewardData> cardRewards)
  {
    using (List<FixedRewardActionDbfRecord>.Enumerator enumerator = GameUtils.GetFixedActionRecords(FixedActionType.WING_FLAGS).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardActionDbfRecord current = enumerator.Current;
        if (current.WingId == wingID)
        {
          ulong wingFlags = current.WingFlags;
          if (((long) wingFlags & (long) flags) == (long) wingFlags)
            this.TriggerRewardsForAction(current.ID, showRewardVisual, cardRewards);
        }
      }
    }
  }

  private void TriggerAchieveAction(FixedRewardsMgr.ShowVisualOption showRewardVisual, int achieveId, List<CardRewardData> cardRewards)
  {
    using (List<FixedRewardActionDbfRecord>.Enumerator enumerator = GameUtils.GetFixedActionRecords(FixedActionType.ACHIEVE).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardActionDbfRecord current = enumerator.Current;
        if (current.AchieveId == achieveId)
          this.TriggerRewardsForAction(current.ID, showRewardVisual, cardRewards, achieveId);
      }
    }
  }

  private void TriggerHeroLevelAction(FixedRewardsMgr.ShowVisualOption showRewardVisual, int classID, int heroLevel, List<CardRewardData> cardRewards)
  {
    using (List<FixedRewardActionDbfRecord>.Enumerator enumerator = GameUtils.GetFixedActionRecords(FixedActionType.HERO_LEVEL).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardActionDbfRecord current = enumerator.Current;
        if (current.ClassId == classID && current.HeroLevel <= heroLevel)
          this.TriggerRewardsForAction(current.ID, showRewardVisual, cardRewards);
      }
    }
  }

  private void TriggerTutorialProgressAction(FixedRewardsMgr.ShowVisualOption showRewardVisual, int tutorialProgress, List<CardRewardData> cardRewards)
  {
    using (List<FixedRewardActionDbfRecord>.Enumerator enumerator = GameUtils.GetFixedActionRecords(FixedActionType.TUTORIAL_PROGRESS).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardActionDbfRecord current = enumerator.Current;
        if (current.TutorialProgress <= tutorialProgress)
          this.TriggerRewardsForAction(current.ID, showRewardVisual, cardRewards);
      }
    }
  }

  private void TriggerAccountLicenseFlagsAction(FixedRewardsMgr.ShowVisualOption showRewardVisual, long license, ulong flags, List<CardRewardData> cardRewards)
  {
    using (List<FixedRewardActionDbfRecord>.Enumerator enumerator = GameUtils.GetFixedActionRecords(FixedActionType.ACCOUNT_LICENSE_FLAGS).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardActionDbfRecord current = enumerator.Current;
        if (current.AccountLicenseId == license)
        {
          ulong accountLicenseFlags = current.AccountLicenseFlags;
          if (((long) accountLicenseFlags & (long) flags) == (long) accountLicenseFlags)
            this.TriggerRewardsForAction(current.ID, showRewardVisual, cardRewards);
        }
      }
    }
  }

  private void TriggerMetaActionFlagsAction(FixedRewardsMgr.ShowVisualOption showRewardVisual, int metaActionID, List<CardRewardData> cardRewards)
  {
    FixedRewardActionDbfRecord record = GameDbf.FixedRewardAction.GetRecord(metaActionID);
    if (record == null)
      return;
    ulong metaActionFlags = record.MetaActionFlags;
    if (!this.GetEarnedMetaActionReward(metaActionID).HasAllRequiredFlags(metaActionFlags))
      return;
    this.TriggerRewardsForAction(metaActionID, showRewardVisual, cardRewards);
  }

  private void ShowFixedRewards_Internal(UserAttentionBlocker blocker, FixedRewardsMgr.OnAllFixedRewardsShownCallbackInfo callbackInfo)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    FixedRewardsMgr.\u003CShowFixedRewards_Internal\u003Ec__AnonStorey43F internalCAnonStorey43F = new FixedRewardsMgr.\u003CShowFixedRewards_Internal\u003Ec__AnonStorey43F();
    // ISSUE: reference to a compiler-generated field
    internalCAnonStorey43F.blocker = blocker;
    // ISSUE: reference to a compiler-generated field
    internalCAnonStorey43F.callbackInfo = callbackInfo;
    // ISSUE: reference to a compiler-generated field
    internalCAnonStorey43F.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    if (internalCAnonStorey43F.callbackInfo.m_rewardMapIDsToShow.Count == 0)
    {
      // ISSUE: reference to a compiler-generated field
      if (internalCAnonStorey43F.callbackInfo.m_onAllRewardsShownCallback == null)
        return;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      internalCAnonStorey43F.callbackInfo.m_onAllRewardsShownCallback(internalCAnonStorey43F.callbackInfo.m_userData);
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      FixedRewardsMgr.RewardMapIDToShow rewardMapIdToShow = internalCAnonStorey43F.callbackInfo.m_rewardMapIDsToShow[0];
      // ISSUE: reference to a compiler-generated field
      internalCAnonStorey43F.callbackInfo.m_rewardMapIDsToShow.RemoveAt(0);
      FixedRewardMapDbfRecord record = GameDbf.FixedRewardMap.GetRecord(rewardMapIdToShow.RewardMapID);
      FixedRewardsMgr.FixedReward fixedReward = this.GetFixedReward(record);
      RewardData rewardData = (RewardData) null;
      if (fixedReward.FixedCardRewardData != null)
        rewardData = (RewardData) fixedReward.FixedCardRewardData;
      else if (fixedReward.FixedCardBackRewardData != null)
        rewardData = (RewardData) fixedReward.FixedCardBackRewardData;
      Log.Achievements.Print("Showing Fixed Reward: " + (object) rewardMapIdToShow.AchieveID);
      if (rewardData == null)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        this.ShowFixedRewards_Internal(internalCAnonStorey43F.blocker, internalCAnonStorey43F.callbackInfo);
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        if (internalCAnonStorey43F.callbackInfo.m_showingCheatRewards)
          rewardData.MarkAsDummyReward();
        if (rewardMapIdToShow.AchieveID != FixedRewardsMgr.RewardMapIDToShow.NO_ACHIEVE_ID)
        {
          Achievement achievement = AchieveManager.Get().GetAchievement(rewardMapIdToShow.AchieveID);
          if (achievement != null)
            achievement.AckCurrentProgressAndRewardNotices();
        }
        if (record.UseQuestToast)
        {
          string toastName = (string) record.ToastName;
          string toastDescription = (string) record.ToastDescription;
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          QuestToast.ShowFixedRewardQuestToast(internalCAnonStorey43F.blocker, new QuestToast.DelOnCloseQuestToast(internalCAnonStorey43F.\u003C\u003Em__2E9), rewardData, toastName, toastDescription);
        }
        else
        {
          // ISSUE: reference to a compiler-generated method
          rewardData.LoadRewardObject(new Reward.DelOnRewardLoaded(internalCAnonStorey43F.\u003C\u003Em__2EA));
        }
      }
    }
  }

  private void OnNonToastRewardClicked(Reward reward, object userData)
  {
    FixedRewardsMgr.OnAllFixedRewardsShownCallbackInfo callbackInfo = userData as FixedRewardsMgr.OnAllFixedRewardsShownCallbackInfo;
    reward.RemoveClickListener(new Reward.OnClickedCallback(this.OnNonToastRewardClicked), (object) callbackInfo);
    reward.Hide(true);
    this.ShowFixedRewards_Internal(UserAttentionBlocker.NONE, callbackInfo);
  }

  private List<RewardData> GetRewardsForAction(int actionID, HashSet<RewardVisualTiming> rewardTimings)
  {
    List<RewardData> rewardDataList = new List<RewardData>();
    using (List<FixedRewardMapDbfRecord>.Enumerator enumerator = GameUtils.GetFixedRewardMapRecordsForAction(actionID).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardMapDbfRecord current = enumerator.Current;
        RewardVisualTiming outVal;
        if (current.RewardCount > 0 && EnumUtils.TryGetEnum<RewardVisualTiming>(current.RewardTiming, out outVal) && rewardTimings.Contains(outVal))
        {
          FixedRewardsMgr.FixedReward fixedReward = this.GetFixedReward(current);
          if (fixedReward.FixedCardRewardData != null)
            rewardDataList.Add((RewardData) fixedReward.FixedCardRewardData);
          else if (fixedReward.FixedCardBackRewardData != null)
            rewardDataList.Add((RewardData) fixedReward.FixedCardBackRewardData);
        }
      }
    }
    return rewardDataList;
  }

  private void GrantAchieveRewards(List<CardRewardData> cardRewards)
  {
    AchieveManager achieveManager = AchieveManager.Get();
    if (achieveManager == null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("FixedRewardsMgr.GrantAchieveRewards(): null == AchieveManager.Get()"));
    }
    else
    {
      using (List<Achievement>.Enumerator enumerator = achieveManager.GetCompletedAchieves().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Achievement current = enumerator.Current;
          this.TriggerAchieveAction(current.AcknowledgedProgress >= current.Progress ? FixedRewardsMgr.ShowVisualOption.DO_NOT_SHOW : FixedRewardsMgr.ShowVisualOption.SHOW, current.ID, cardRewards);
        }
      }
    }
  }

  private void GrantHeroLevelRewards(List<CardRewardData> cardRewards)
  {
    NetCache.NetCacheHeroLevels netObject = NetCache.Get().GetNetObject<NetCache.NetCacheHeroLevels>();
    if (netObject == null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("FixedRewardsMgr.GrantHeroUnlockRewards(): null == NetCache.NetCacheHeroLevels"));
    }
    else
    {
      using (List<NetCache.HeroLevel>.Enumerator enumerator = netObject.Levels.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          NetCache.HeroLevel current = enumerator.Current;
          this.TriggerHeroLevelAction(FixedRewardsMgr.ShowVisualOption.DO_NOT_SHOW, (int) current.Class, current.CurrentLevel.Level, cardRewards);
        }
      }
    }
  }

  private class FixedMetaActionReward
  {
    public int MetaActionID { get; private set; }

    public ulong MetaActionFlags { get; private set; }

    public FixedMetaActionReward(int metaActionID)
    {
      this.MetaActionID = metaActionID;
      this.MetaActionFlags = 0UL;
    }

    public void UpdateFlags(ulong addFlags, ulong removeFlags)
    {
      this.MetaActionFlags |= addFlags;
      this.MetaActionFlags = this.MetaActionFlags & ~removeFlags;
    }

    public bool HasAllRequiredFlags(ulong requiredFlags)
    {
      return ((long) this.MetaActionFlags & (long) requiredFlags) == (long) requiredFlags;
    }
  }

  private class FixedReward
  {
    public FixedRewardType Type;
    public CardRewardData FixedCardRewardData;
    public CardBackRewardData FixedCardBackRewardData;
    public NetCache.CardDefinition FixedCraftableCardRewardData;
    public FixedRewardsMgr.FixedMetaActionReward FixedMetaActionRewardData;

    public FixedReward()
    {
      this.Type = FixedRewardType.UNKNOWN;
      this.FixedCardRewardData = (CardRewardData) null;
      this.FixedCardBackRewardData = (CardBackRewardData) null;
      this.FixedCraftableCardRewardData = (NetCache.CardDefinition) null;
      this.FixedMetaActionRewardData = (FixedRewardsMgr.FixedMetaActionReward) null;
    }
  }

  private class OnAllFixedRewardsShownCallbackInfo
  {
    public List<FixedRewardsMgr.RewardMapIDToShow> m_rewardMapIDsToShow;
    public FixedRewardsMgr.DelOnAllFixedRewardsShown m_onAllRewardsShownCallback;
    public FixedRewardsMgr.DelPositionNonToastReward m_positionNonToastRewardCallback;
    public Vector3 m_rewardPunchScale;
    public Vector3 m_rewardScale;
    public object m_userData;
    public bool m_showingCheatRewards;
  }

  private class RewardMapIDToShow
  {
    public static readonly int NO_ACHIEVE_ID;
    public int RewardMapID;
    public int AchieveID;
    public int SortOrder;

    public RewardMapIDToShow(int rewardMapID, int achieveID, int sortOrder)
    {
      this.RewardMapID = rewardMapID;
      this.AchieveID = achieveID;
      this.SortOrder = sortOrder;
    }

    public override bool Equals(object obj)
    {
      FixedRewardsMgr.RewardMapIDToShow rewardMapIdToShow = obj as FixedRewardsMgr.RewardMapIDToShow;
      if (rewardMapIdToShow == null)
        return false;
      return this.RewardMapID == rewardMapIdToShow.RewardMapID;
    }

    public override int GetHashCode()
    {
      return this.RewardMapID.GetHashCode();
    }
  }

  private enum ShowVisualOption
  {
    DO_NOT_SHOW,
    SHOW,
    FORCE_SHOW,
  }

  public delegate void DelOnAllFixedRewardsShown(object userData);

  public delegate void DelPositionNonToastReward(Reward reward);
}
