﻿// Decompiled with JetBrains decompiler
// Type: UIReleaseAllEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class UIReleaseAllEvent : UIEvent
{
  private bool m_mouseIsOver;

  public UIReleaseAllEvent(bool mouseIsOver, PegUIElement element)
    : base(UIEventType.RELEASEALL, element)
  {
    this.m_mouseIsOver = mouseIsOver;
  }

  public bool GetMouseIsOver()
  {
    return this.m_mouseIsOver;
  }
}
