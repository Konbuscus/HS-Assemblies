﻿// Decompiled with JetBrains decompiler
// Type: SoundPlayClipArgs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SoundPlayClipArgs
{
  public AudioSource m_templateSource;
  public AudioClip m_clip;
  public float? m_volume;
  public float? m_pitch;
  public float? m_spatialBlend;
  public SoundCategory? m_category;
  public GameObject m_parentObject;
  public bool? m_localizeAudio;
}
