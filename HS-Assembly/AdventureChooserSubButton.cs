﻿// Decompiled with JetBrains decompiler
// Type: AdventureChooserSubButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class AdventureChooserSubButton : AdventureGenericButton
{
  private const string s_EventFlash = "Flash";
  [CustomEditField(Sections = "Progress UI")]
  public GameObject m_progressCounter;
  [CustomEditField(Sections = "Progress UI")]
  public UberText m_progressCounterText;
  [CustomEditField(Sections = "Progress UI")]
  public GameObject m_heroicSkull;
  [CustomEditField(Sections = "Event Table")]
  public StateEventTable m_StateTable;
  private AdventureDbId m_TargetAdventure;
  private AdventureModeDbId m_TargetMode;
  private bool m_Glow;

  public void SetAdventure(AdventureDbId id, AdventureModeDbId mode)
  {
    this.m_TargetAdventure = id;
    this.m_TargetMode = mode;
    this.ShowRemainingProgressCount();
  }

  public AdventureDbId GetAdventure()
  {
    return this.m_TargetAdventure;
  }

  public AdventureModeDbId GetMode()
  {
    return this.m_TargetMode;
  }

  public void SetHighlight(bool enable)
  {
    UIBHighlightStateControl component1 = this.GetComponent<UIBHighlightStateControl>();
    if ((Object) component1 != (Object) null)
    {
      if (this.m_Glow)
        component1.Select(true, true);
      else
        component1.Select(enable, false);
    }
    UIBHighlight component2 = this.GetComponent<UIBHighlight>();
    if (!((Object) component2 != (Object) null))
      return;
    component2.AlwaysOver = enable;
  }

  public void SetNewGlow(bool enable)
  {
    this.m_Glow = enable;
    UIBHighlightStateControl component = this.GetComponent<UIBHighlightStateControl>();
    if (!((Object) component != (Object) null))
      return;
    component.Select(enable, true);
  }

  public void Flash()
  {
    this.m_StateTable.TriggerState("Flash", true, (string) null);
  }

  public bool IsReady()
  {
    UIBHighlightStateControl component = this.GetComponent<UIBHighlightStateControl>();
    if ((Object) component != (Object) null)
      return component.IsReady();
    return false;
  }

  public void ShowRemainingProgressCount()
  {
    int num = 0;
    if (this.m_TargetMode == AdventureModeDbId.CLASS_CHALLENGE)
      num = AdventureProgressMgr.Get().GetPlayableClassChallenges(this.m_TargetAdventure, this.m_TargetMode);
    if (this.m_TargetMode == AdventureModeDbId.NORMAL || this.m_TargetMode == AdventureModeDbId.HEROIC)
      num = AdventureProgressMgr.Get().GetPlayableAdventureScenarios(this.m_TargetAdventure, this.m_TargetMode);
    if (this.m_TargetMode == AdventureModeDbId.HEROIC)
    {
      if (num > 0)
        this.m_heroicSkull.SetActive(true);
      else
        this.m_heroicSkull.SetActive(false);
      this.m_progressCounter.SetActive(false);
    }
    else
    {
      this.m_heroicSkull.SetActive(false);
      if (num > 0)
      {
        this.m_progressCounter.SetActive(true);
        this.m_progressCounterText.Text = num.ToString();
      }
      else
        this.m_progressCounter.SetActive(false);
    }
  }
}
