﻿// Decompiled with JetBrains decompiler
// Type: AssignActorPortraitToChildren
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AssignActorPortraitToChildren : MonoBehaviour
{
  private Actor m_Actor;

  private void Start()
  {
    this.m_Actor = SceneUtils.FindComponentInThisOrParents<Actor>(this.gameObject);
  }

  public void AssignPortraitToAllChildren()
  {
    if (!(bool) ((Object) this.m_Actor) || (Object) this.m_Actor.m_portraitMesh == (Object) null)
      return;
    Material[] materials = this.m_Actor.m_portraitMesh.GetComponent<Renderer>().materials;
    if (materials.Length == 0 || this.m_Actor.m_portraitMatIdx < 0)
      return;
    Texture mainTexture = materials[this.m_Actor.m_portraitMatIdx].mainTexture;
    foreach (Renderer componentsInChild in this.GetComponentsInChildren<Renderer>())
    {
      foreach (Material material in componentsInChild.materials)
      {
        if (material.name.Contains("portrait"))
          material.mainTexture = mainTexture;
      }
    }
  }
}
