﻿// Decompiled with JetBrains decompiler
// Type: AttackInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class AttackInfo
{
  public Entity m_attacker;
  public Entity m_defender;
  public Entity m_proposedAttacker;
  public Entity m_proposedDefender;
  public int? m_attackerTagValue;
  public int? m_defenderTagValue;
  public int? m_proposedAttackerTagValue;
  public int? m_proposedDefenderTagValue;
}
