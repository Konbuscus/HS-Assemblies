﻿// Decompiled with JetBrains decompiler
// Type: BnetUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using PegasusShared;

public static class BnetUtils
{
  public static BnetPlayer GetPlayer(BnetGameAccountId id)
  {
    if ((BnetEntityId) id == (BnetEntityId) null)
      return (BnetPlayer) null;
    return BnetNearbyPlayerMgr.Get().FindNearbyStranger(id) ?? BnetPresenceMgr.Get().GetPlayer(id);
  }

  public static string GetPlayerBestName(BnetGameAccountId id)
  {
    BnetPlayer player = BnetUtils.GetPlayer(id);
    string str = player != null ? player.GetBestName() : (string) null;
    if (string.IsNullOrEmpty(str))
      str = GameStrings.Get("GLOBAL_PLAYER_PLAYER");
    return str;
  }

  public static bool HasPlayerBestNamePresence(BnetGameAccountId id)
  {
    BnetPlayer player = BnetUtils.GetPlayer(id);
    return !string.IsNullOrEmpty(player != null ? player.GetBestName() : (string) null);
  }

  public static string GetInviterBestName(PartyInvite invite)
  {
    if (invite != null && !string.IsNullOrEmpty(invite.InviterName))
      return invite.InviterName;
    BnetPlayer bnetPlayer = invite != null ? BnetUtils.GetPlayer(invite.InviterId) : (BnetPlayer) null;
    string str = bnetPlayer != null ? bnetPlayer.GetBestName() : (string) null;
    if (string.IsNullOrEmpty(str))
      str = GameStrings.Get("GLOBAL_PLAYER_PLAYER");
    return str;
  }

  public static bool CanReceiveChallengeFrom(BnetGameAccountId id)
  {
    return BnetFriendMgr.Get().IsFriend(id) || BnetNearbyPlayerMgr.Get().IsNearbyStranger(id);
  }

  public static bool CanReceiveWhisperFrom(BnetGameAccountId id)
  {
    return !BnetPresenceMgr.Get().GetMyPlayer().IsBusy() && BnetFriendMgr.Get().IsFriend(id);
  }

  public static BnetGameAccountId CreateGameAccountId(BnetId src)
  {
    BnetGameAccountId bnetGameAccountId = new BnetGameAccountId();
    bnetGameAccountId.SetHi(src.Hi);
    bnetGameAccountId.SetLo(src.Lo);
    return bnetGameAccountId;
  }

  public static PartyId CreatePartyId(BnetId protoEntityId)
  {
    return new PartyId(protoEntityId.Hi, protoEntityId.Lo);
  }

  public static BnetId CreatePegasusBnetId(PartyId partyId)
  {
    return new BnetId() { Hi = partyId.Hi, Lo = partyId.Lo };
  }

  public static BnetId CreatePegasusBnetId(BnetEntityId src)
  {
    return new BnetId() { Hi = src.GetHi(), Lo = src.GetLo() };
  }

  public static string GetNameForProgramId(BnetProgramId programId)
  {
    string nameTag = BnetProgramId.GetNameTag(programId);
    if (nameTag != null)
      return GameStrings.Get(nameTag);
    return (string) null;
  }
}
