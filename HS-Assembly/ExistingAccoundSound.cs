﻿// Decompiled with JetBrains decompiler
// Type: ExistingAccoundSound
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class ExistingAccoundSound
{
  public string m_buttonClick = "Small_Click";
  public string m_popupShow = "pop_up_dialogue_window_open";
  public string m_popupHide = "pop_up_dialogue_window_close";
  public string m_innkeeperWelcome = "VO_INNKEEPER_WELCOME5_20";
}
