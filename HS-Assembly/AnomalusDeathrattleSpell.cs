﻿// Decompiled with JetBrains decompiler
// Type: AnomalusDeathrattleSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class AnomalusDeathrattleSpell : Spell
{
  public float m_DelayBeforeStart = 1f;
  public float m_DelayDistanceModifier = 1f;
  public float m_RiseTime = 0.5f;
  public float m_HangTime = 1f;
  public float m_LiftHeightMin = 2f;
  public float m_LiftHeightMax = 3f;
  public float m_LiftRotMin = -15f;
  public float m_LiftRotMax = 15f;
  public float m_SlamTime = 0.15f;
  public float m_Bounceness = 0.2f;
  public float m_DelayAfterSpellFinish = 3f;
  public Spell m_CustomDeathSpell;
  private GameObject[] m_TargetActorGameObjects;
  private Actor[] m_TargetActors;

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    List<Card> cardList = new List<Card>();
    List<Entity> targetEntities = new List<Entity>();
    using (List<GameObject>.Enumerator enumerator = this.GetVisualTargets().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((UnityEngine.Object) current == (UnityEngine.Object) null))
        {
          Card component = current.GetComponent<Card>();
          cardList.Add(component);
          targetEntities.Add(component.GetEntity());
        }
      }
    }
    List<Entity> sourceAmongstTargets = GameUtils.GetEntitiesKilledBySourceAmongstTargets(this.GetSourceCard().GetEntity().GetEntityId(), targetEntities);
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AnomalusDeathrattleSpell.\u003COnAction\u003Ec__AnonStorey406 actionCAnonStorey406 = new AnomalusDeathrattleSpell.\u003COnAction\u003Ec__AnonStorey406();
    using (List<Card>.Enumerator enumerator = cardList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        actionCAnonStorey406.targetCard = enumerator.Current;
        // ISSUE: reference to a compiler-generated method
        if (sourceAmongstTargets.Exists(new Predicate<Entity>(actionCAnonStorey406.\u003C\u003Em__20D)))
        {
          // ISSUE: reference to a compiler-generated field
          actionCAnonStorey406.targetCard.OverrideCustomDeathSpell(UnityEngine.Object.Instantiate<Spell>(this.m_CustomDeathSpell));
        }
      }
    }
    this.StartCoroutine(this.AnimateMinions());
  }

  [DebuggerHidden]
  private IEnumerator AnimateMinions()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AnomalusDeathrattleSpell.\u003CAnimateMinions\u003Ec__Iterator2A7() { \u003C\u003Ef__this = this };
  }
}
