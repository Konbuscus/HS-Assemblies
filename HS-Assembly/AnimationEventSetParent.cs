﻿// Decompiled with JetBrains decompiler
// Type: AnimationEventSetParent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnimationEventSetParent : MonoBehaviour
{
  public GameObject m_Parent;

  private void Start()
  {
    if ((bool) ((Object) this.m_Parent))
      return;
    Debug.LogError((object) "Animation Event Set Parent is null!");
    this.enabled = false;
  }

  public void SetParent()
  {
    if (!(bool) ((Object) this.m_Parent))
      return;
    this.transform.parent = this.m_Parent.transform;
  }
}
