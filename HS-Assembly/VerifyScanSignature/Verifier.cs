﻿// Decompiled with JetBrains decompiler
// Type: VerifyScanSignature.Verifier
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace VerifyScanSignature
{
  public class Verifier
  {
    private static byte[] s_magic = Encoding.UTF8.GetBytes("NGIS");
    private static byte[] s_modulus = new byte[256]{ (byte) 71, (byte) 147, (byte) 189, (byte) 90, (byte) 135, (byte) 248, (byte) 118, (byte) 158, (byte) 9, (byte) 35, (byte) 81, (byte) 227, (byte) 220, (byte) 190, (byte) 70, (byte) 67, (byte) 131, (byte) 87, (byte) 138, (byte) 136, (byte) 32, (byte) 95, (byte) 228, (byte) 80, (byte) 60, (byte) 61, (byte) 203, (byte) 23, (byte) 67, (byte) 213, (byte) 44, (byte) 40, (byte) 101, (byte) 167, (byte) 34, (byte) 67, (byte) 50, (byte) 88, (byte) 7, (byte) 55, (byte) 202, (byte) 163, (byte) 63, (byte) 214, (byte) 194, (byte) 117, (byte) 161, (byte) 34, (byte) 91, (byte) 196, (byte) 172, (byte) 134, (byte) 21, (byte) 98, (byte) 196, (byte) 41, (byte) 156, (byte) 246, (byte) 223, (byte) 49, (byte) 186, (byte) 224, (byte) 19, (byte) 41, (byte) 73, (byte) 44, (byte) 186, (byte) 3, (byte) 227, (byte) 167, (byte) 254, (byte) 36, (byte) 60, (byte) 183, (byte) 180, (byte) 102, (byte) 225, (byte) 65, (byte) 184, (byte) 159, (byte) 132, (byte) 9, (byte) 209, (byte) 125, (byte) 62, (byte) 9, (byte) 248, (byte) 172, (byte) 44, (byte) 88, (byte) 247, (byte) 75, (byte) 144, (byte) 147, (byte) 59, (byte) 207, (byte) 190, (byte) 37, (byte) 227, (byte) 163, (byte) 49, (byte) 252, (byte) 46, (byte) 114, (byte) 51, (byte) 214, (byte) 24, (byte) 224, (byte) 228, (byte) 215, (byte) 91, (byte) 168, (byte) 117, (byte) 41, (byte) 170, (byte) 140, (byte) 185, (byte) 72, (byte) 152, (byte) 206, (byte) 153, (byte) 90, (byte) 158, (byte) 222, (byte) 139, (byte) 166, (byte) 82, (byte) 91, (byte) 28, (byte) 4, (byte) 231, (byte) 28, (byte) 98, (byte) 37, (byte) 167, (byte) 66, (byte) 13, (byte) 32, (byte) 41, (byte) 233, (byte) 79, (byte) 235, (byte) 215, (byte) 29, (byte) 14, (byte) 86, (byte) 107, (byte) 118, (byte) 30, (byte) 161, (byte) 7, (byte) 153, (byte) 56, (byte) 252, (byte) 114, (byte) 50, (byte) 103, (byte) 106, (byte) 93, (byte) 148, (byte) 5, (byte) 85, (byte) 70, (byte) 93, (byte) 125, (byte) 56, (byte) 77, (byte) 142, (byte) 82, (byte) 48, (byte) 180, (byte) 15, (byte) 74, (byte) 94, (byte) 78, (byte) 183, (byte) 45, (byte) 227, (byte) 155, (byte) 135, (byte) 37, (byte) 243, (byte) 25, (byte) 43, (byte) 68, (byte) 194, (byte) 54, (byte) 131, (byte) 205, (byte) 254, (byte) 118, (byte) 193, (byte) 50, (byte) 233, (byte) 180, (byte) 29, (byte) 241, (byte) 114, (byte) 192, (byte) 180, (byte) 157, (byte) 162, (byte) 140, (byte) 69, (byte) 105, (byte) 112, (byte) 107, (byte) 96, (byte) 120, (byte) 192, (byte) 219, (byte) 142, (byte) 47, (byte) 11, (byte) 171, (byte) 209, (byte) 62, (byte) 83, (byte) 63, (byte) 151, (byte) 78, (byte) 78, (byte) 76, (byte) 39, (byte) 43, (byte) 52, (byte) 51, (byte) 85, (byte) 208, (byte) 39, (byte) 186, (byte) 163, (byte) 186, (byte) 199, (byte) 75, (byte) 99, (byte) 233, (byte) 91, (byte) 167, (byte) 58, (byte) 139, (byte) 100, (byte) 134, (byte) 177, (byte) 152, (byte) 177, (byte) 131, (byte) 140, (byte) 232, (byte) 10, (byte) 59, (byte) 40, (byte) 62, (byte) 42, (byte) 150, (byte) 168 };
    private static uint s_publicExponent = 65537;
    private static byte[] s_hashDER = new byte[19]{ (byte) 48, (byte) 49, (byte) 48, (byte) 13, (byte) 6, (byte) 9, (byte) 96, (byte) 134, (byte) 72, (byte) 1, (byte) 101, (byte) 3, (byte) 4, (byte) 2, (byte) 1, (byte) 5, (byte) 0, (byte) 4, (byte) 32 };
    private static int s_reservedLength = Verifier.s_magic.Length + Verifier.s_modulus.Length;
    private SHA256 m_hash = SHA256.Create();
    private byte[] m_reserved = new byte[Verifier.s_reservedLength];
    private int m_reservedCount;

    private static bool AreByteArraysEqual(byte[] left, int leftOffset, byte[] right, int rightOffset, int length)
    {
      for (int index = 0; index < length; ++index)
      {
        if ((int) left[leftOffset + index] != (int) right[rightOffset + index])
          return false;
      }
      return true;
    }

    private static bool AreByteArraysEqual(byte[] left, byte[] right)
    {
      if (left.Length != right.Length)
        return false;
      return Verifier.AreByteArraysEqual(left, 0, right, 0, left.Length);
    }

    private static byte[] MakePKCS1Padding(byte[] hash)
    {
      byte[] numArray = new byte[Verifier.s_modulus.Length];
      numArray[0] = (byte) 0;
      numArray[1] = (byte) 1;
      int destinationIndex1 = Verifier.s_modulus.Length - hash.Length;
      Array.Copy((Array) hash, 0, (Array) numArray, destinationIndex1, hash.Length);
      int destinationIndex2 = destinationIndex1 - Verifier.s_hashDER.Length;
      Array.Copy((Array) Verifier.s_hashDER, 0, (Array) numArray, destinationIndex2, Verifier.s_hashDER.Length);
      int index = destinationIndex2 - 1;
      numArray[index] = (byte) 0;
      while (index > 2)
      {
        --index;
        numArray[index] = byte.MaxValue;
      }
      Array.Reverse((Array) numArray);
      return numArray;
    }

    private static BigInteger ByteArrayToBigInteger(byte[] data, int offset, int length)
    {
      byte[] array = new byte[checked (length + 1)];
      Array.Copy((Array) data, offset, (Array) array, 0, length);
      array[length] = (byte) 0;
      Array.Reverse((Array) array);
      return new BigInteger(array);
    }

    private static BigInteger ByteArrayToBigInteger(byte[] data)
    {
      if (data.Length == 0)
        return new BigInteger(0L);
      if ((int) data[data.Length - 1] >= 128)
        return Verifier.ByteArrayToBigInteger(data, 0, data.Length);
      return new BigInteger(data);
    }

    private static bool RSAEncrypt(byte[] data, int offset, int length, out BigInteger output)
    {
      output = (BigInteger) null;
      if (length != Verifier.s_modulus.Length)
        return false;
      BigInteger bigInteger1 = Verifier.ByteArrayToBigInteger(Verifier.s_modulus);
      BigInteger exp = new BigInteger((long) Verifier.s_publicExponent);
      BigInteger bigInteger2 = Verifier.ByteArrayToBigInteger(data, offset, length);
      if (bigInteger2 < (BigInteger) 0 || bigInteger2 >= bigInteger1)
        return false;
      output = BigInteger.PowMod(bigInteger2, exp, bigInteger1);
      return true;
    }

    public void Process(byte[] data, int offset, int length)
    {
      if (length == 0)
        return;
      if (this.m_reservedCount < Verifier.s_reservedLength)
      {
        int length1 = Verifier.s_reservedLength - this.m_reservedCount;
        if (length1 > length)
          length1 = length;
        Array.Copy((Array) data, offset, (Array) this.m_reserved, this.m_reservedCount, length1);
        this.m_reservedCount += length1;
        offset += length1;
        length -= length1;
      }
      if (length == 0)
        return;
      if (length >= Verifier.s_reservedLength)
      {
        this.m_hash.TransformBlock(this.m_reserved, 0, Verifier.s_reservedLength, this.m_reserved, 0);
        this.m_hash.TransformBlock(data, offset, length - Verifier.s_reservedLength, data, offset);
        Array.Copy((Array) data, offset + (length - Verifier.s_reservedLength), (Array) this.m_reserved, 0, Verifier.s_reservedLength);
      }
      else
      {
        this.m_hash.TransformBlock(this.m_reserved, 0, length, this.m_reserved, 0);
        Array.Copy((Array) this.m_reserved, length, (Array) this.m_reserved, 0, Verifier.s_reservedLength - length);
        Array.Copy((Array) data, offset, (Array) this.m_reserved, Verifier.s_reservedLength - length, length);
      }
    }

    public bool Finish(string tag)
    {
      if (this.m_reservedCount < Verifier.s_reservedLength || !Verifier.AreByteArraysEqual(this.m_reserved, 0, Verifier.s_magic, 0, Verifier.s_magic.Length))
        return false;
      byte[] bytes = Encoding.UTF8.GetBytes(tag);
      this.m_hash.TransformFinalBlock(bytes, 0, bytes.Length);
      byte[] numArray = Verifier.MakePKCS1Padding(this.m_hash.Hash);
      BigInteger output = (BigInteger) null;
      if (!Verifier.RSAEncrypt(this.m_reserved, Verifier.s_magic.Length, this.m_reserved.Length - Verifier.s_magic.Length, out output))
        return false;
      byte[] array = new byte[Verifier.s_magic.Length + numArray.Length];
      Array.Copy((Array) Verifier.s_magic, (Array) array, Verifier.s_magic.Length);
      Array.Copy((Array) numArray, (Array) array, numArray.Length);
      Array.Reverse((Array) array);
      return new BigInteger(array).CompareTo(output) == 0;
    }

    public static bool VerifyStreamSignature(Stream stream, string tag, int bufferSize = 16384)
    {
      Verifier verifier = new Verifier();
      byte[] numArray = new byte[bufferSize];
      int length;
      do
      {
        length = stream.Read(numArray, 0, numArray.Length);
        verifier.Process(numArray, 0, length);
      }
      while (length > 0);
      return verifier.Finish(tag);
    }
  }
}
