﻿// Decompiled with JetBrains decompiler
// Type: TwistingNetherFloatInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class TwistingNetherFloatInfo
{
  public Vector3 m_OffsetMin = new Vector3(-1.5f, -1.5f, -1.5f);
  public Vector3 m_OffsetMax = new Vector3(1.5f, 1.5f, 1.5f);
  public Vector2 m_RotationXZMin = new Vector2(-10f, -10f);
  public Vector2 m_RotationXZMax = new Vector2(10f, 10f);
  public float m_DurationMin = 1.5f;
  public float m_DurationMax = 2f;
}
