﻿// Decompiled with JetBrains decompiler
// Type: TransformOverride
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TransformOverride : MonoBehaviour
{
  public List<ScreenCategory> m_screenCategory = new List<ScreenCategory>();
  public List<Vector3> m_localPosition = new List<Vector3>();
  public List<Vector3> m_localScale = new List<Vector3>();
  public List<Quaternion> m_localRotation = new List<Quaternion>();
  public float testVal;

  public void Awake()
  {
    if (!Application.isPlaying)
      return;
    this.UpdateObject();
  }

  public void AddCategory(ScreenCategory screen)
  {
    if (Application.isPlaying)
      return;
    this.m_screenCategory.Add(screen);
    this.m_localPosition.Add(this.transform.localPosition);
    this.m_localScale.Add(this.transform.localScale);
    this.m_localRotation.Add(this.transform.localRotation);
  }

  public void AddCategory()
  {
    this.AddCategory(PlatformSettings.Screen);
  }

  public void UpdateObject()
  {
    int bestScreenMatch = PlatformSettings.GetBestScreenMatch(this.m_screenCategory);
    this.transform.localPosition = this.m_localPosition[bestScreenMatch];
    this.transform.localScale = this.m_localScale[bestScreenMatch];
    this.transform.localRotation = this.m_localRotation[bestScreenMatch];
  }
}
