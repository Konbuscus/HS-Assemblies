﻿// Decompiled with JetBrains decompiler
// Type: DraftPhoneDeckTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DraftPhoneDeckTray : BasePhoneDeckTray
{
  private static DraftPhoneDeckTray s_instance;

  protected override void Awake()
  {
    base.Awake();
    DraftPhoneDeckTray.s_instance = this;
    DraftManager.Get().RegisterDraftDeckSetListener(new DraftManager.DraftDeckSet(this.OnDraftDeckInitialized));
  }

  private void OnDestroy()
  {
    DraftManager.Get().RemoveDraftDeckSetListener(new DraftManager.DraftDeckSet(this.OnDraftDeckInitialized));
    CollectionManager.Get().ClearTaggedDeck(CollectionManager.DeckTag.Arena);
    DraftPhoneDeckTray.s_instance = (DraftPhoneDeckTray) null;
  }

  public static DraftPhoneDeckTray Get()
  {
    return DraftPhoneDeckTray.s_instance;
  }

  public override void Initialize()
  {
    CollectionDeck draftDeck = DraftManager.Get().GetDraftDeck();
    if (draftDeck == null)
      return;
    this.OnDraftDeckInitialized(draftDeck);
  }

  private void OnDraftDeckInitialized(CollectionDeck draftDeck)
  {
    if (draftDeck == null)
    {
      Debug.LogError((object) "Draft deck is null.");
    }
    else
    {
      CollectionManager.Get().SetTaggedDeck(CollectionManager.DeckTag.Arena, draftDeck, (object) null);
      this.OnCardCountUpdated(draftDeck.GetTotalCardCount());
      this.m_cardsContent.UpdateCardList(true, (Actor) null);
    }
  }
}
