﻿// Decompiled with JetBrains decompiler
// Type: ManaCrystalMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class ManaCrystalMgr : MonoBehaviour
{
  private readonly string GEM_FLIP_ANIM_NAME = "Resource_Large_phone_Flip";
  private int m_proposedManaSourceEntID = -1;
  private const float ANIMATE_TIME = 0.6f;
  private const float SECS_BETW_MANA_SPAWNS = 0.2f;
  private const float SECS_BETW_MANA_READIES = 0.2f;
  private const float SECS_BETW_MANA_SPENDS = 0.2f;
  private const float GEM_FLIP_TEXT_FADE_TIME = 0.1f;
  public Material tempManaCrystalMaterial;
  public Material tempManaCrystalProposedQuadMaterial;
  public Texture redCrystalTexture;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public String_MobileOverride manaLockPrefab;
  public ManaCrystalEventSpells m_eventSpells;
  public SlidingTray manaTrayPhone;
  public Transform manaGemBone;
  public GameObject friendlyManaCounter;
  private static ManaCrystalMgr s_instance;
  private List<ManaCrystal> m_permanentCrystals;
  private List<ManaCrystal> m_temporaryCrystals;
  private int m_numCrystalsLoading;
  private int m_numQueuedToSpawn;
  private int m_numQueuedToReady;
  private int m_numQueuedToSpend;
  private int m_additionalOverloadedCrystalsOwedNextTurn;
  private int m_additionalOverloadedCrystalsOwedThisTurn;
  private bool m_overloadLocksAreShowing;
  private float m_manaCrystalWidth;
  private GameObject m_friendlyManaGem;
  private UberText m_friendlyManaText;
  private Texture friendlyManaGemTexture;

  private void Awake()
  {
    ManaCrystalMgr.s_instance = this;
    if (!((UnityEngine.Object) this.gameObject.GetComponent<AudioSource>() == (UnityEngine.Object) null))
      return;
    this.gameObject.AddComponent<AudioSource>();
  }

  private void OnDestroy()
  {
    ManaCrystalMgr.s_instance = (ManaCrystalMgr) null;
  }

  private void Start()
  {
    this.m_permanentCrystals = new List<ManaCrystal>();
    this.m_temporaryCrystals = new List<ManaCrystal>();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_friendlyManaText = this.friendlyManaCounter.GetComponent<UberText>();
    this.m_friendlyManaGem = this.friendlyManaCounter.GetComponent<ManaCounter>().GetPhoneGem();
    if (!((UnityEngine.Object) this.friendlyManaGemTexture != (UnityEngine.Object) null))
      return;
    this.SetFriendlyManaGemTexture(this.friendlyManaGemTexture);
    this.friendlyManaGemTexture = (Texture) null;
  }

  public static ManaCrystalMgr Get()
  {
    return ManaCrystalMgr.s_instance;
  }

  public Vector3 GetManaCrystalSpawnPosition()
  {
    return this.transform.position;
  }

  public void AddManaCrystals(int numCrystals, bool isTurnStart)
  {
    for (int index = 0; index < numCrystals; ++index)
    {
      GameState.Get().GetGameEntity().NotifyOfManaCrystalSpawned();
      this.StartCoroutine(this.WaitThenAddManaCrystal(false, isTurnStart));
    }
  }

  public void AddTempManaCrystals(int numCrystals)
  {
    for (int index = 0; index < numCrystals; ++index)
      this.StartCoroutine(this.WaitThenAddManaCrystal(true, false));
  }

  public void DestroyManaCrystals(int numCrystals)
  {
    this.StartCoroutine(this.WaitThenDestroyManaCrystals(false, numCrystals));
  }

  public void DestroyTempManaCrystals(int numCrystals)
  {
    this.StartCoroutine(this.WaitThenDestroyManaCrystals(true, numCrystals));
  }

  public void UpdateSpentMana(int shownChangeAmount)
  {
    if (shownChangeAmount > 0)
      this.SpendManaCrystals(shownChangeAmount);
    else if (GameState.Get().IsTurnStartManagerActive())
      TurnStartManager.Get().NotifyOfManaCrystalFilled(-shownChangeAmount);
    else
      this.ReadyManaCrystals(-shownChangeAmount);
  }

  public void SpendManaCrystals(int numCrystals)
  {
    SoundManager.Get().LoadAndPlay("mana_crystal_expend", this.gameObject);
    for (int index = 0; index < numCrystals; ++index)
      this.SpendManaCrystal();
  }

  public void ReadyManaCrystals(int numCrystals)
  {
    for (int index = 0; index < numCrystals; ++index)
      this.ReadyManaCrystal();
  }

  public int GetSpendableManaCrystals()
  {
    int num = 0;
    for (int index = 0; index < this.m_temporaryCrystals.Count; ++index)
    {
      if (this.m_temporaryCrystals[index].state == ManaCrystal.State.READY)
        ++num;
    }
    for (int index = 0; index < this.m_permanentCrystals.Count; ++index)
    {
      ManaCrystal permanentCrystal = this.m_permanentCrystals[index];
      if (permanentCrystal.state == ManaCrystal.State.READY && !permanentCrystal.IsOverloaded())
        ++num;
    }
    return num;
  }

  public void CancelAllProposedMana(Entity entity)
  {
    if (entity == null || this.m_proposedManaSourceEntID != entity.GetEntityId())
      return;
    this.m_proposedManaSourceEntID = -1;
    this.m_eventSpells.m_proposeUsageSpell.ActivateState(SpellStateType.DEATH);
    for (int index = 0; index < this.m_temporaryCrystals.Count; ++index)
    {
      if (this.m_temporaryCrystals[index].state == ManaCrystal.State.PROPOSED)
        this.m_temporaryCrystals[index].state = ManaCrystal.State.READY;
    }
    for (int index = this.m_permanentCrystals.Count - 1; index >= 0; --index)
    {
      if (this.m_permanentCrystals[index].state == ManaCrystal.State.PROPOSED)
        this.m_permanentCrystals[index].state = ManaCrystal.State.READY;
    }
  }

  public void ProposeManaCrystalUsage(Entity entity)
  {
    if (entity == null)
      return;
    this.m_proposedManaSourceEntID = entity.GetEntityId();
    int cost = entity.GetCost();
    this.m_eventSpells.m_proposeUsageSpell.ActivateState(SpellStateType.BIRTH);
    int num = 0;
    for (int index = this.m_temporaryCrystals.Count - 1; index >= 0; --index)
    {
      if (this.m_temporaryCrystals[index].state == ManaCrystal.State.USED)
        Log.Rachelle.Print("Found a SPENT temporary mana crystal... this shouldn't happen!");
      else if (num < cost)
      {
        this.m_temporaryCrystals[index].state = ManaCrystal.State.PROPOSED;
        ++num;
      }
      else
        this.m_temporaryCrystals[index].state = ManaCrystal.State.READY;
    }
    for (int index = 0; index < this.m_permanentCrystals.Count; ++index)
    {
      if (this.m_permanentCrystals[index].state != ManaCrystal.State.USED && !this.m_permanentCrystals[index].IsOverloaded())
      {
        if (num < cost)
        {
          this.m_permanentCrystals[index].state = ManaCrystal.State.PROPOSED;
          ++num;
        }
        else
          this.m_permanentCrystals[index].state = ManaCrystal.State.READY;
      }
    }
  }

  public void HandleSameTurnOverloadChanged(int crystalsChanged)
  {
    if (crystalsChanged > 0)
    {
      this.MarkCrystalsOwedForOverload(crystalsChanged);
    }
    else
    {
      if (crystalsChanged >= 0)
        return;
      this.ReclaimCrystalsOwedForOverload(-crystalsChanged);
    }
  }

  public void MarkCrystalsOwedForOverload(int numCrystals)
  {
    if (numCrystals > 0)
      this.m_overloadLocksAreShowing = true;
    int num = 0;
    int index = 0;
    while (numCrystals != num)
    {
      if (index == this.m_permanentCrystals.Count)
      {
        this.m_additionalOverloadedCrystalsOwedNextTurn += numCrystals - num;
        break;
      }
      ManaCrystal permanentCrystal = this.m_permanentCrystals[index];
      if (!permanentCrystal.IsOwedForOverload())
      {
        permanentCrystal.MarkAsOwedForOverload();
        ++num;
      }
      ++index;
    }
  }

  public void ReclaimCrystalsOwedForOverload(int numCrystals)
  {
    int num = 0;
    int lastIndex;
    for (lastIndex = this.m_permanentCrystals.FindLastIndex((Predicate<ManaCrystal>) (crystal => crystal.IsOwedForOverload())); num < numCrystals && lastIndex >= 0; ++num)
    {
      this.m_permanentCrystals[lastIndex].ReclaimOverload();
      --lastIndex;
    }
    this.m_additionalOverloadedCrystalsOwedNextTurn -= numCrystals - num;
    this.m_overloadLocksAreShowing = lastIndex >= 0 || this.m_additionalOverloadedCrystalsOwedNextTurn > 0;
  }

  public void UnlockCrystals(int numCrystals)
  {
    int num = 0;
    int lastIndex;
    for (lastIndex = this.m_permanentCrystals.FindLastIndex((Predicate<ManaCrystal>) (crystal => crystal.IsOverloaded())); num < numCrystals && lastIndex >= 0; ++num)
    {
      this.m_permanentCrystals[lastIndex].UnlockOverload();
      --lastIndex;
    }
    this.m_additionalOverloadedCrystalsOwedThisTurn -= numCrystals - num;
    this.m_overloadLocksAreShowing = lastIndex >= 0 || this.m_additionalOverloadedCrystalsOwedThisTurn > 0;
  }

  public void TurnCrystalsRed(int previous, int current)
  {
    for (int index = previous; index < current && index < this.m_permanentCrystals.Count; ++index)
      this.m_permanentCrystals[index].gem.gameObject.GetComponent<Renderer>().material.mainTexture = this.redCrystalTexture;
  }

  public void OnCurrentPlayerChanged()
  {
    this.m_additionalOverloadedCrystalsOwedThisTurn = this.m_additionalOverloadedCrystalsOwedNextTurn;
    this.m_additionalOverloadedCrystalsOwedNextTurn = 0;
    this.m_overloadLocksAreShowing = this.m_additionalOverloadedCrystalsOwedThisTurn > 0;
    for (int index = 0; index < this.m_permanentCrystals.Count; ++index)
    {
      ManaCrystal permanentCrystal = this.m_permanentCrystals[index];
      if (permanentCrystal.IsOverloaded())
        permanentCrystal.UnlockOverload();
      if (permanentCrystal.IsOwedForOverload())
      {
        this.m_overloadLocksAreShowing = true;
        permanentCrystal.PayOverload();
      }
      else if (this.m_additionalOverloadedCrystalsOwedThisTurn > 0)
      {
        permanentCrystal.PayOverload();
        --this.m_additionalOverloadedCrystalsOwedThisTurn;
      }
    }
  }

  public bool ShouldShowOverloadTooltip()
  {
    return this.m_overloadLocksAreShowing;
  }

  public void SetFriendlyManaGemTexture(Texture texture)
  {
    if ((UnityEngine.Object) this.m_friendlyManaGem == (UnityEngine.Object) null)
      this.friendlyManaGemTexture = texture;
    else
      this.m_friendlyManaGem.GetComponentInChildren<MeshRenderer>().material.mainTexture = texture;
  }

  public void SetFriendlyManaGemTint(Color tint)
  {
    if ((UnityEngine.Object) this.m_friendlyManaGem == (UnityEngine.Object) null)
      return;
    this.m_friendlyManaGem.GetComponentInChildren<MeshRenderer>().material.SetColor("_TintColor", tint);
  }

  public void ShowPhoneManaTray()
  {
    this.m_friendlyManaGem.GetComponent<Animation>()[this.GEM_FLIP_ANIM_NAME].speed = 1f;
    this.m_friendlyManaGem.GetComponent<Animation>().Play(this.GEM_FLIP_ANIM_NAME);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) this.m_friendlyManaText.TextAlpha, (object) "to", (object) 0.0f, (object) "time", (object) 0.1f, (object) "onupdate", (object) (Action<object>) (newVal => this.m_friendlyManaText.TextAlpha = (float) newVal)));
    this.manaTrayPhone.ToggleTraySlider(true, (Transform) null, true);
  }

  public void HidePhoneManaTray()
  {
    this.m_friendlyManaGem.GetComponent<Animation>()[this.GEM_FLIP_ANIM_NAME].speed = -1f;
    if ((double) this.m_friendlyManaGem.GetComponent<Animation>()[this.GEM_FLIP_ANIM_NAME].time == 0.0)
      this.m_friendlyManaGem.GetComponent<Animation>()[this.GEM_FLIP_ANIM_NAME].time = this.m_friendlyManaGem.GetComponent<Animation>()[this.GEM_FLIP_ANIM_NAME].length;
    this.m_friendlyManaGem.GetComponent<Animation>().Play(this.GEM_FLIP_ANIM_NAME);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) this.m_friendlyManaText.TextAlpha, (object) "to", (object) 1f, (object) "time", (object) 0.1f, (object) "onupdate", (object) (Action<object>) (newVal => this.m_friendlyManaText.TextAlpha = (float) newVal)));
    this.manaTrayPhone.ToggleTraySlider(false, (Transform) null, true);
  }

  private void UpdateLayout()
  {
    Vector3 position = this.transform.position;
    if ((bool) UniversalInputManager.UsePhoneUI)
      position = this.manaGemBone.transform.position;
    for (int index = this.m_permanentCrystals.Count - 1; index >= 0; --index)
    {
      this.m_permanentCrystals[index].transform.position = position;
      if ((bool) UniversalInputManager.UsePhoneUI)
        position.z += this.m_manaCrystalWidth;
      else
        position.x += this.m_manaCrystalWidth;
    }
    for (int index = 0; index < this.m_temporaryCrystals.Count; ++index)
    {
      this.m_temporaryCrystals[index].transform.position = position;
      if ((bool) UniversalInputManager.UsePhoneUI)
        position.z += this.m_manaCrystalWidth;
      else
        position.x += this.m_manaCrystalWidth;
    }
  }

  [DebuggerHidden]
  private IEnumerator UpdatePermanentCrystalStates()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ManaCrystalMgr.\u003CUpdatePermanentCrystalStates\u003Ec__IteratorC2() { \u003C\u003Ef__this = this };
  }

  private void LoadCrystalCallback(string actorName, GameObject actorObject, object callbackData)
  {
    --this.m_numCrystalsLoading;
    if ((double) this.m_manaCrystalWidth <= 0.0)
      this.m_manaCrystalWidth = !(bool) UniversalInputManager.UsePhoneUI ? actorObject.transform.FindChild("Gem_Mana").GetComponent<Renderer>().bounds.size.x : 0.33f;
    ManaCrystalMgr.LoadCrystalCallbackData crystalCallbackData = callbackData as ManaCrystalMgr.LoadCrystalCallbackData;
    ManaCrystal component = actorObject.GetComponent<ManaCrystal>();
    if (crystalCallbackData.IsTempCrystal)
    {
      component.MarkAsTemp();
      this.m_temporaryCrystals.Add(component);
    }
    else
    {
      this.m_permanentCrystals.Add(component);
      if (crystalCallbackData.IsTurnStart)
      {
        if (this.m_additionalOverloadedCrystalsOwedThisTurn > 0)
        {
          component.PayOverload();
          --this.m_additionalOverloadedCrystalsOwedThisTurn;
        }
      }
      else if (this.m_additionalOverloadedCrystalsOwedNextTurn > 0)
      {
        component.state = ManaCrystal.State.USED;
        component.MarkAsOwedForOverload();
        --this.m_additionalOverloadedCrystalsOwedNextTurn;
      }
      this.StartCoroutine(this.UpdatePermanentCrystalStates());
    }
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      component.transform.parent = this.manaGemBone.transform.parent;
      component.transform.localRotation = this.manaGemBone.transform.localRotation;
      component.transform.localScale = this.manaGemBone.transform.localScale;
    }
    else
      component.transform.parent = this.transform;
    component.transform.localPosition = Vector3.zero;
    component.PlayCreateAnimation();
    SoundManager.Get().LoadAndPlay("mana_crystal_add", this.gameObject);
    this.UpdateLayout();
  }

  public float GetWidth()
  {
    if (this.m_permanentCrystals.Count == 0)
      return 0.0f;
    return this.m_permanentCrystals[0].transform.FindChild("Gem_Mana").GetComponent<Renderer>().bounds.size.x * (float) this.m_permanentCrystals.Count * (float) this.m_temporaryCrystals.Count;
  }

  [DebuggerHidden]
  private IEnumerator WaitThenAddManaCrystal(bool isTemp, bool isTurnStart)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ManaCrystalMgr.\u003CWaitThenAddManaCrystal\u003Ec__IteratorC3() { isTemp = isTemp, isTurnStart = isTurnStart, \u003C\u0024\u003EisTemp = isTemp, \u003C\u0024\u003EisTurnStart = isTurnStart, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitThenDestroyManaCrystals(bool isTemp, int numCrystals)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ManaCrystalMgr.\u003CWaitThenDestroyManaCrystals\u003Ec__IteratorC4() { numCrystals = numCrystals, isTemp = isTemp, \u003C\u0024\u003EnumCrystals = numCrystals, \u003C\u0024\u003EisTemp = isTemp, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitThenReadyManaCrystal()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ManaCrystalMgr.\u003CWaitThenReadyManaCrystal\u003Ec__IteratorC5() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitThenSpendManaCrystal()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ManaCrystalMgr.\u003CWaitThenSpendManaCrystal\u003Ec__IteratorC6() { \u003C\u003Ef__this = this };
  }

  private void DestroyManaCrystal()
  {
    if (this.m_permanentCrystals.Count <= 0)
      return;
    int index = 0;
    ManaCrystal permanentCrystal = this.m_permanentCrystals[index];
    this.m_permanentCrystals.RemoveAt(index);
    permanentCrystal.GetComponent<ManaCrystal>().Destroy();
    this.UpdateLayout();
    this.StartCoroutine(this.UpdatePermanentCrystalStates());
  }

  private void DestroyTempManaCrystal()
  {
    if (this.m_temporaryCrystals.Count <= 0)
      return;
    int index = this.m_temporaryCrystals.Count - 1;
    ManaCrystal temporaryCrystal = this.m_temporaryCrystals[index];
    this.m_temporaryCrystals.RemoveAt(index);
    temporaryCrystal.GetComponent<ManaCrystal>().Destroy();
    this.UpdateLayout();
  }

  private void SpendManaCrystal()
  {
    this.StartCoroutine(this.WaitThenSpendManaCrystal());
  }

  private void ReadyManaCrystal()
  {
    this.StartCoroutine(this.WaitThenReadyManaCrystal());
  }

  private class LoadCrystalCallbackData
  {
    private bool m_isTempCrystal;
    private bool m_isTurnStart;

    public bool IsTempCrystal
    {
      get
      {
        return this.m_isTempCrystal;
      }
    }

    public bool IsTurnStart
    {
      get
      {
        return this.m_isTurnStart;
      }
    }

    public LoadCrystalCallbackData(bool isTempCrystal, bool isTurnStart)
    {
      this.m_isTempCrystal = isTempCrystal;
      this.m_isTurnStart = isTurnStart;
    }
  }
}
