﻿// Decompiled with JetBrains decompiler
// Type: TGTTargetDummy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class TGTTargetDummy : MonoBehaviour
{
  public float m_BodyHitIntensity = 25f;
  public float m_ShieldHitIntensity = 25f;
  public float m_SwordHitIntensity = -25f;
  private const int SPIN_PERCENT = 5;
  public GameObject m_Body;
  public GameObject m_Shield;
  public GameObject m_Sword;
  public GameObject m_BodyRotX;
  public GameObject m_BodyRotY;
  public GameObject m_BodyMesh;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_HitBodySoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_HitShieldSoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_HitSwordSoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_HitSpinSoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_SqueakSoundPrefab;
  private static TGTTargetDummy s_instance;
  private float m_squeakSoundVelocity;
  private float m_lastSqueakSoundVol;
  private Quaternion m_lastFrameSqueakAngle;
  private AudioSource m_squeakSound;

  private void Awake()
  {
    TGTTargetDummy.s_instance = this;
  }

  private void Start()
  {
    this.StartCoroutine(this.RegisterBoardEventLargeShake());
    this.m_BodyRotX.GetComponent<Rigidbody>().maxAngularVelocity = this.m_BodyHitIntensity;
    this.m_BodyRotY.GetComponent<Rigidbody>().maxAngularVelocity = Mathf.Max(this.m_SwordHitIntensity, this.m_ShieldHitIntensity);
  }

  private void Update()
  {
    this.HandleHits();
  }

  public static TGTTargetDummy Get()
  {
    return TGTTargetDummy.s_instance;
  }

  public void ArrowHit()
  {
    this.m_BodyRotX.GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(this.m_BodyHitIntensity * 0.25f, this.m_BodyHitIntensity * 0.5f), 0.0f, 0.0f);
  }

  public void BodyHit()
  {
    this.PlaySqueakSound();
    if (!string.IsNullOrEmpty(this.m_HitBodySoundPrefab))
    {
      string name = FileUtils.GameAssetPathToName(this.m_HitBodySoundPrefab);
      if (!string.IsNullOrEmpty(name))
        SoundManager.Get().LoadAndPlay(name, this.m_Body);
    }
    this.m_BodyRotX.GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(this.m_BodyHitIntensity * 0.75f, this.m_BodyHitIntensity), 0.0f, 0.0f);
    this.m_BodyRotY.GetComponent<Rigidbody>().angularVelocity = new Vector3(0.0f, Random.Range(-5f, 5f), 0.0f);
  }

  public void ShieldHit()
  {
    this.PlaySqueakSound();
    if (Random.Range(0, 100) < 5)
    {
      this.Spin(false);
    }
    else
    {
      if (!string.IsNullOrEmpty(this.m_HitShieldSoundPrefab))
      {
        string name = FileUtils.GameAssetPathToName(this.m_HitShieldSoundPrefab);
        if (!string.IsNullOrEmpty(name))
          SoundManager.Get().LoadAndPlay(name, this.m_Body);
      }
      this.m_BodyRotY.GetComponent<Rigidbody>().angularVelocity = new Vector3(0.0f, Random.Range(this.m_ShieldHitIntensity * 0.7f, this.m_ShieldHitIntensity), 0.0f);
      this.m_BodyRotX.GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(-5f, -10f), 0.0f, 0.0f);
    }
  }

  public void SwordHit()
  {
    this.PlaySqueakSound();
    if (Random.Range(0, 100) < 5)
    {
      this.Spin(true);
    }
    else
    {
      if (!string.IsNullOrEmpty(this.m_HitSwordSoundPrefab))
      {
        string name = FileUtils.GameAssetPathToName(this.m_HitSwordSoundPrefab);
        if (!string.IsNullOrEmpty(name))
          SoundManager.Get().LoadAndPlay(name, this.m_Body);
      }
      this.m_BodyRotY.GetComponent<Rigidbody>().angularVelocity = new Vector3(0.0f, Random.Range(this.m_SwordHitIntensity * 0.7f, this.m_SwordHitIntensity), 0.0f);
      this.m_BodyRotX.GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(-5f, -10f), 0.0f, 0.0f);
    }
  }

  [DebuggerHidden]
  private IEnumerator RegisterBoardEventLargeShake()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTTargetDummy.\u003CRegisterBoardEventLargeShake\u003Ec__Iterator1C()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void HandleHits()
  {
    if (UniversalInputManager.Get().GetMouseButtonDown(0) && this.IsOver(this.m_Body))
      this.BodyHit();
    if (UniversalInputManager.Get().GetMouseButtonDown(0) && this.IsOver(this.m_Shield))
      this.ShieldHit();
    if (!UniversalInputManager.Get().GetMouseButtonDown(0) || !this.IsOver(this.m_Sword))
      return;
    this.SwordHit();
  }

  private void Spin(bool reverse)
  {
    float num = 1080f;
    if (reverse)
      num = -1080f;
    if (!string.IsNullOrEmpty(this.m_HitSpinSoundPrefab))
    {
      string name = FileUtils.GameAssetPathToName(this.m_HitSpinSoundPrefab);
      if (!string.IsNullOrEmpty(name))
        SoundManager.Get().LoadAndPlay(name, this.m_Body);
    }
    this.m_BodyMesh.transform.localEulerAngles = Vector3.zero;
    iTween.RotateTo(this.m_BodyMesh, iTween.Hash((object) "rotation", (object) new Vector3(0.0f, this.m_BodyMesh.transform.localEulerAngles.y + num, 0.0f), (object) "isLocal", (object) true, (object) "time", (object) 3f, (object) "easetype", (object) iTween.EaseType.easeOutElastic));
  }

  private void PlaySqueakSound()
  {
    this.StopCoroutine("SqueakSound");
    this.m_lastSqueakSoundVol = 0.0f;
    this.StartCoroutine("SqueakSound");
  }

  [DebuggerHidden]
  private IEnumerator SqueakSound()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTTargetDummy.\u003CSqueakSound\u003Ec__Iterator1D() { \u003C\u003Ef__this = this };
  }

  private bool IsOver(GameObject go)
  {
    return (bool) ((Object) go) && InputUtil.IsPlayMakerMouseInputAllowed(go) && UniversalInputManager.Get().InputIsOver(go);
  }
}
