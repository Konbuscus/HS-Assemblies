﻿// Decompiled with JetBrains decompiler
// Type: WorldDimensionIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public struct WorldDimensionIndex
{
  public float Dimension;
  public int Index;

  public WorldDimensionIndex(float dimension, int index)
  {
    this.Dimension = dimension;
    this.Index = index;
  }
}
