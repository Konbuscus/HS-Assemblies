﻿// Decompiled with JetBrains decompiler
// Type: RewardCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RewardCard : MonoBehaviour
{
  public string m_CardID = string.Empty;
  private GameLayer m_layer = GameLayer.IgnoreFullScreenEffects;
  private bool m_Ready;
  private TAG_PREMIUM m_premium;
  private EntityDef m_entityDef;
  private CardDef m_cardDef;
  private Actor m_actor;

  private void OnDestroy()
  {
    this.m_Ready = false;
  }

  public bool IsReady()
  {
    return this.m_Ready;
  }

  public void LoadCard(CardRewardData cardData, GameLayer layer = GameLayer.IgnoreFullScreenEffects)
  {
    this.m_layer = layer;
    this.m_CardID = cardData.CardID;
    this.m_premium = cardData.Premium;
    DefLoader.Get().LoadFullDef(this.m_CardID, new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
  }

  public void Death()
  {
    this.m_actor.ActivateSpellBirthState(SpellType.DEATH);
  }

  private void OnFullDefLoaded(string cardId, FullDef fullDef, object userData)
  {
    if (fullDef == null)
    {
      Debug.LogWarning((object) string.Format("RewardCard.OnFullDefLoaded() - FAILED to load \"{0}\"", (object) cardId));
    }
    else
    {
      this.m_entityDef = fullDef.GetEntityDef();
      this.m_cardDef = fullDef.GetCardDef();
      AssetLoader.Get().LoadActor(ActorNames.GetHandActor(this.m_entityDef, this.m_premium), new AssetLoader.GameObjectCallback(this.OnActorLoaded), (object) null, false);
    }
  }

  private void OnActorLoaded(string name, GameObject actorObject, object userData)
  {
    if ((Object) actorObject == (Object) null)
    {
      Debug.LogWarning((object) string.Format("RewardCard.OnActorLoaded() - FAILED to load actor \"{0}\"", (object) name));
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((Object) component == (Object) null)
      {
        Debug.LogWarning((object) string.Format("RewardCard.OnActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) name));
      }
      else
      {
        this.m_actor = component;
        this.m_actor.TurnOffCollider();
        SceneUtils.SetLayer(component.gameObject, this.m_layer);
        this.m_actor.SetEntityDef(this.m_entityDef);
        this.m_actor.SetCardDef(this.m_cardDef);
        this.m_actor.SetPremium(this.m_premium);
        this.m_actor.UpdateAllComponents();
        this.m_actor.transform.parent = this.transform;
        this.m_actor.transform.localPosition = Vector3.zero;
        this.m_actor.transform.localEulerAngles = new Vector3(270f, 0.0f, 0.0f);
        this.m_actor.transform.localScale = Vector3.one;
        this.m_actor.Show();
        this.m_Ready = true;
      }
    }
  }
}
