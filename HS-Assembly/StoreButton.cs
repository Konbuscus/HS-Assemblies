﻿// Decompiled with JetBrains decompiler
// Type: StoreButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StoreButton : PegUIElement
{
  public GameObject m_storeClosed;
  public UberText m_storeClosedText;
  public UberText m_storeText;
  public HighlightState m_highlightState;
  public GameObject m_highlight;

  protected override void Awake()
  {
    base.Awake();
    this.m_storeText.Text = GameStrings.Get("GLUE_STORE_OPEN_BUTTON_TEXT");
    this.m_storeClosedText.Text = GameStrings.Get("GLUE_STORE_CLOSED_BUTTON_TEXT");
  }

  private void Start()
  {
    this.m_storeClosed.SetActive(!StoreManager.Get().IsOpen());
    StoreManager.Get().RegisterStatusChangedListener(new StoreManager.StatusChangedCallback(this.OnStoreStatusChanged));
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnButtonOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnButtonOut));
    SoundManager.Get().Load("store_button_mouse_over");
    SoundManager.Get().Load("Store_window_shrink");
  }

  public void Unload()
  {
    this.SetEnabled(false);
    StoreManager.Get().RemoveStatusChangedListener(new StoreManager.StatusChangedCallback(this.OnStoreStatusChanged));
  }

  public bool IsVisualClosed()
  {
    if ((Object) this.m_storeClosed != (Object) null)
      return this.m_storeClosed.activeInHierarchy;
    return false;
  }

  private void OnButtonOver(UIEvent e)
  {
    if (this.IsVisualClosed())
      SoundManager.Get().LoadAndPlay("Store_window_shrink", this.gameObject);
    else
      SoundManager.Get().LoadAndPlay("store_button_mouse_over", this.gameObject);
    if ((Object) this.m_highlightState != (Object) null)
      this.m_highlightState.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
    if ((Object) this.m_highlight != (Object) null)
      this.m_highlight.SetActive(true);
    TooltipZone component = this.GetComponent<TooltipZone>();
    if ((Object) component == (Object) null)
      return;
    component.ShowBoxTooltip(GameStrings.Get("GLUE_TOOLTIP_BUTTON_STORE_HEADLINE"), GameStrings.Get("GLUE_TOOLTIP_BUTTON_STORE_DESC"));
  }

  private void OnButtonOut(UIEvent e)
  {
    if ((Object) this.m_highlightState != (Object) null)
      this.m_highlightState.ChangeState(ActorStateType.HIGHLIGHT_OFF);
    if ((Object) this.m_highlight != (Object) null)
      this.m_highlight.SetActive(false);
    TooltipZone component = this.GetComponent<TooltipZone>();
    if (!((Object) component != (Object) null))
      return;
    component.HideTooltip();
  }

  private void OnStoreStatusChanged(bool isOpen, object userData)
  {
    if (!((Object) this.m_storeClosed != (Object) null))
      return;
    this.m_storeClosed.SetActive(!isOpen);
  }
}
