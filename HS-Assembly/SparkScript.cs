﻿// Decompiled with JetBrains decompiler
// Type: SparkScript
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SparkScript : MonoBehaviour
{
  public AudioClip clip1;
  public AudioClip clip2;

  private void Awake()
  {
    AudioSource component = this.GetComponent<AudioSource>();
    if ((double) Random.value >= 0.5)
      component.clip = this.clip1;
    else
      component.clip = this.clip2;
  }
}
