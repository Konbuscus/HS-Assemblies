﻿// Decompiled with JetBrains decompiler
// Type: TB14_DPromo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TB14_DPromo : MissionEntity
{
  private static readonly Dictionary<int, string> minionMsgs = new Dictionary<int, string>() { { 10, "TB_DPROMO_2NDHEROPOPUP" }, { 11, "TB_DPROMO_2NDHEROPOPUP" } };
  private float popupDuration = 2.5f;
  private float popupScale = 2.5f;
  private HashSet<int> seen = new HashSet<int>();
  private Notification MyPopup;
  private Vector3 popUpPos;
  private string textID;
  private bool doPopup;
  private bool doLeftArrow;
  private bool doUpArrow;
  private bool doDownArrow;
  private float delayTime;

  public override void PreloadAssets()
  {
    this.PreloadSound("tutorial_mission_hero_coin_mouse_away");
    this.PreloadSound("CowKing_TB_SPT_DPromo_Hero2_Play");
    this.PreloadSound("CowKing_TB_SPT_DPromo_Hero2_Death");
    this.PreloadSound("HellBovine_TB_SPT_DPromoMinion_Attack");
    this.PreloadSound("HellBovine_TB_SPT_DPromoMinion_Death");
    this.PreloadSound("HellBovine_TB_SPT_DPromoMinion_Play");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB14_DPromo.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1FA() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
