﻿// Decompiled with JetBrains decompiler
// Type: BnetPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;

public class BnetPlayer
{
  private Map<BnetGameAccountId, BnetGameAccount> m_gameAccounts = new Map<BnetGameAccountId, BnetGameAccount>();
  private BnetAccountId m_accountId;
  private BnetAccount m_account;
  private BnetGameAccount m_hsGameAccount;
  private BnetGameAccount m_bestGameAccount;

  public BnetPlayer Clone()
  {
    BnetPlayer bnetPlayer = (BnetPlayer) this.MemberwiseClone();
    if ((BnetEntityId) this.m_accountId != (BnetEntityId) null)
      bnetPlayer.m_accountId = this.m_accountId.Clone();
    if (this.m_account != (BnetAccount) null)
      bnetPlayer.m_account = this.m_account.Clone();
    if (this.m_hsGameAccount != (BnetGameAccount) null)
      bnetPlayer.m_hsGameAccount = this.m_hsGameAccount.Clone();
    if (this.m_bestGameAccount != (BnetGameAccount) null)
      bnetPlayer.m_bestGameAccount = this.m_bestGameAccount.Clone();
    bnetPlayer.m_gameAccounts = new Map<BnetGameAccountId, BnetGameAccount>();
    using (Map<BnetGameAccountId, BnetGameAccount>.Enumerator enumerator = this.m_gameAccounts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<BnetGameAccountId, BnetGameAccount> current = enumerator.Current;
        bnetPlayer.m_gameAccounts.Add(current.Key.Clone(), current.Value.Clone());
      }
    }
    return bnetPlayer;
  }

  public BnetAccountId GetAccountId()
  {
    if ((BnetEntityId) this.m_accountId != (BnetEntityId) null)
      return this.m_accountId;
    BnetGameAccount firstGameAccount = this.GetFirstGameAccount();
    if (firstGameAccount != (BnetGameAccount) null)
      return firstGameAccount.GetOwnerId();
    return (BnetAccountId) null;
  }

  public void SetAccountId(BnetAccountId accountId)
  {
    this.m_accountId = accountId;
  }

  public BnetAccount GetAccount()
  {
    return this.m_account;
  }

  public void SetAccount(BnetAccount account)
  {
    this.m_account = account;
    this.m_accountId = account.GetId();
  }

  public string GetFullName()
  {
    if (this.m_account == (BnetAccount) null)
      return (string) null;
    return this.m_account.GetFullName();
  }

  public BnetBattleTag GetBattleTag()
  {
    if (this.m_account != (BnetAccount) null && this.m_account.GetBattleTag() != (BnetBattleTag) null)
      return this.m_account.GetBattleTag();
    BnetGameAccount firstGameAccount = this.GetFirstGameAccount();
    if (firstGameAccount != (BnetGameAccount) null)
      return firstGameAccount.GetBattleTag();
    return (BnetBattleTag) null;
  }

  public BnetGameAccount GetGameAccount(BnetGameAccountId id)
  {
    BnetGameAccount bnetGameAccount = (BnetGameAccount) null;
    this.m_gameAccounts.TryGetValue(id, out bnetGameAccount);
    return bnetGameAccount;
  }

  public Map<BnetGameAccountId, BnetGameAccount> GetGameAccounts()
  {
    return this.m_gameAccounts;
  }

  public bool HasGameAccount(BnetGameAccountId id)
  {
    return this.m_gameAccounts.ContainsKey(id);
  }

  public void AddGameAccount(BnetGameAccount gameAccount)
  {
    BnetGameAccountId id = gameAccount.GetId();
    if (this.m_gameAccounts.ContainsKey(id))
      return;
    this.m_gameAccounts.Add(id, gameAccount);
    this.CacheSpecialGameAccounts();
  }

  public bool RemoveGameAccount(BnetGameAccountId id)
  {
    if (!this.m_gameAccounts.Remove(id))
      return false;
    this.CacheSpecialGameAccounts();
    return true;
  }

  public BnetGameAccount GetHearthstoneGameAccount()
  {
    return this.m_hsGameAccount;
  }

  public BnetGameAccountId GetHearthstoneGameAccountId()
  {
    if (this.m_hsGameAccount == (BnetGameAccount) null)
      return (BnetGameAccountId) null;
    return this.m_hsGameAccount.GetId();
  }

  public BnetGameAccount GetBestGameAccount()
  {
    return this.m_bestGameAccount;
  }

  public BnetGameAccountId GetBestGameAccountId()
  {
    if (this.m_bestGameAccount == (BnetGameAccount) null)
      return (BnetGameAccountId) null;
    return this.m_bestGameAccount.GetId();
  }

  public bool IsDisplayable()
  {
    return this.GetBestName() != null;
  }

  public BnetGameAccount GetFirstGameAccount()
  {
    using (Map<BnetGameAccountId, BnetGameAccount>.ValueCollection.Enumerator enumerator = this.m_gameAccounts.Values.GetEnumerator())
    {
      if (enumerator.MoveNext())
        return enumerator.Current;
    }
    return (BnetGameAccount) null;
  }

  public long GetPersistentGameId()
  {
    return 0;
  }

  public string GetBestName()
  {
    if (this == BnetPresenceMgr.Get().GetMyPlayer())
    {
      if (this.m_hsGameAccount == (BnetGameAccount) null)
        return (string) null;
      if (this.m_hsGameAccount.GetBattleTag() == (BnetBattleTag) null)
        return (string) null;
      return this.m_hsGameAccount.GetBattleTag().GetName();
    }
    if (this.m_account != (BnetAccount) null)
    {
      string fullName = this.m_account.GetFullName();
      if (fullName != null)
        return fullName;
      if (this.m_account.GetBattleTag() != (BnetBattleTag) null)
        return this.m_account.GetBattleTag().GetName();
    }
    using (Map<BnetGameAccountId, BnetGameAccount>.Enumerator enumerator = this.m_gameAccounts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<BnetGameAccountId, BnetGameAccount> current = enumerator.Current;
        if (current.Value.GetBattleTag() != (BnetBattleTag) null)
          return current.Value.GetBattleTag().GetName();
      }
    }
    return (string) null;
  }

  public BnetProgramId GetBestProgramId()
  {
    if (this.m_bestGameAccount == (BnetGameAccount) null)
      return (BnetProgramId) null;
    return this.m_bestGameAccount.GetProgramId();
  }

  public string GetBestRichPresence()
  {
    if (this.m_bestGameAccount == (BnetGameAccount) null)
      return (string) null;
    return this.m_bestGameAccount.GetRichPresence();
  }

  public bool IsOnline()
  {
    using (Map<BnetGameAccountId, BnetGameAccount>.ValueCollection.Enumerator enumerator = this.m_gameAccounts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.IsOnline())
          return true;
      }
    }
    return false;
  }

  public bool IsAway()
  {
    return this.m_account != (BnetAccount) null && this.m_account.IsAway() || this.m_bestGameAccount != (BnetGameAccount) null && this.m_bestGameAccount.IsAway();
  }

  public bool IsBusy()
  {
    return this.m_account != (BnetAccount) null && this.m_account.IsBusy() || this.m_bestGameAccount != (BnetGameAccount) null && this.m_bestGameAccount.IsBusy();
  }

  public ulong GetBestAwayTimeMicrosec()
  {
    ulong num = 0;
    if (this.m_account != (BnetAccount) null && this.m_account.IsAway())
    {
      num = Math.Max(this.m_account.GetAwayTimeMicrosec(), this.m_account.GetLastOnlineMicrosec());
      if ((long) num != 0L)
        return num;
    }
    if (this.m_bestGameAccount != (BnetGameAccount) null && this.m_bestGameAccount.IsAway())
    {
      num = Math.Max(this.m_bestGameAccount.GetAwayTimeMicrosec(), this.m_bestGameAccount.GetLastOnlineMicrosec());
      if ((long) num != 0L)
        return num;
    }
    return num;
  }

  public ulong GetBestLastOnlineMicrosec()
  {
    ulong num = 0;
    if (this.m_account != (BnetAccount) null)
    {
      num = this.m_account.GetLastOnlineMicrosec();
      if ((long) num != 0L)
        return num;
    }
    if (this.m_bestGameAccount != (BnetGameAccount) null)
    {
      num = this.m_bestGameAccount.GetLastOnlineMicrosec();
      if ((long) num != 0L)
        return num;
    }
    return num;
  }

  public bool HasMultipleOnlineGameAccounts()
  {
    bool flag = false;
    using (Map<BnetGameAccountId, BnetGameAccount>.ValueCollection.Enumerator enumerator = this.m_gameAccounts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.IsOnline())
        {
          if (flag)
            return true;
          flag = true;
        }
      }
    }
    return false;
  }

  public int GetNumOnlineGameAccounts()
  {
    int num = 0;
    using (Map<BnetGameAccountId, BnetGameAccount>.ValueCollection.Enumerator enumerator = this.m_gameAccounts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.IsOnline())
          ++num;
      }
    }
    return num;
  }

  public List<BnetGameAccount> GetOnlineGameAccounts()
  {
    List<BnetGameAccount> bnetGameAccountList = new List<BnetGameAccount>();
    using (Map<BnetGameAccountId, BnetGameAccount>.ValueCollection.Enumerator enumerator = this.m_gameAccounts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetGameAccount current = enumerator.Current;
        if (current.IsOnline())
          bnetGameAccountList.Add(current);
      }
    }
    return bnetGameAccountList;
  }

  public bool HasAccount(BnetEntityId id)
  {
    if (id == (BnetEntityId) null)
      return false;
    if ((BnetEntityId) this.m_accountId == id)
      return true;
    using (Map<BnetGameAccountId, BnetGameAccount>.KeyCollection.Enumerator enumerator = this.m_gameAccounts.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if ((BnetEntityId) enumerator.Current == id)
          return true;
      }
    }
    return false;
  }

  public void OnGameAccountChanged(uint fieldId)
  {
    if ((int) fieldId != 3 && (int) fieldId != 1 && (int) fieldId != 4)
      return;
    this.CacheSpecialGameAccounts();
  }

  public override string ToString()
  {
    BnetAccountId accountId = this.GetAccountId();
    BnetBattleTag battleTag = this.GetBattleTag();
    if ((BnetEntityId) accountId == (BnetEntityId) null && battleTag == (BnetBattleTag) null)
      return "UNKNOWN PLAYER";
    return string.Format("[account={0} battleTag={1} numGameAccounts={2}]", (object) accountId, (object) battleTag, (object) this.m_gameAccounts.Count);
  }

  private void CacheSpecialGameAccounts()
  {
    this.m_hsGameAccount = (BnetGameAccount) null;
    this.m_bestGameAccount = (BnetGameAccount) null;
    ulong num = 0;
    using (Map<BnetGameAccountId, BnetGameAccount>.ValueCollection.Enumerator enumerator = this.m_gameAccounts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetGameAccount current = enumerator.Current;
        BnetProgramId programId1 = current.GetProgramId();
        if (!((bgs.FourCC) programId1 == (bgs.FourCC) null))
        {
          if ((bgs.FourCC) programId1 == (bgs.FourCC) BnetProgramId.HEARTHSTONE)
          {
            this.m_hsGameAccount = current;
            if (!current.IsOnline() && BnetFriendMgr.Get().IsFriend(current.GetId()))
              break;
            this.m_bestGameAccount = current;
            break;
          }
          if (this.m_bestGameAccount == (BnetGameAccount) null)
          {
            this.m_bestGameAccount = current;
            num = this.m_bestGameAccount.GetLastOnlineMicrosec();
          }
          else if (!this.m_bestGameAccount.IsOnline() && current.IsOnline())
          {
            this.m_bestGameAccount = current;
            num = this.m_bestGameAccount.GetLastOnlineMicrosec();
          }
          else
          {
            BnetProgramId programId2 = this.m_bestGameAccount.GetProgramId();
            if (current.IsOnline())
            {
              if (programId1.IsGame() && !programId2.IsGame())
              {
                this.m_bestGameAccount = current;
                num = this.m_bestGameAccount.GetLastOnlineMicrosec();
              }
              else if (programId1.IsGame() && programId2.IsGame())
              {
                ulong lastOnlineMicrosec = current.GetLastOnlineMicrosec();
                if (lastOnlineMicrosec > num)
                {
                  this.m_bestGameAccount = current;
                  num = lastOnlineMicrosec;
                }
              }
            }
            else if (!this.m_bestGameAccount.IsOnline())
            {
              ulong lastOnlineMicrosec = current.GetLastOnlineMicrosec();
              if (lastOnlineMicrosec > num)
              {
                this.m_bestGameAccount = current;
                num = lastOnlineMicrosec;
              }
            }
          }
        }
      }
    }
  }
}
