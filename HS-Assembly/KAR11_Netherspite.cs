﻿// Decompiled with JetBrains decompiler
// Type: KAR11_Netherspite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR11_Netherspite : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Netherspite_Male_Dragon_NetherspiteEmoteResponse_01");
    this.PreloadSound("VO_Netherspite_Male_Dragon_NetherspiteTurn3_02");
    this.PreloadSound("VO_Netherspite_Male_Dragon_NetherspiteTurn5_01");
    this.PreloadSound("VO_Netherspite_Male_Dragon_NetherspiteTurn7_01");
    this.PreloadSound("VO_Netherspite_Male_Dragon_NetherspiteEmpowerment_01");
    this.PreloadSound("VO_Netherspite_Male_Dragon_NetherspiteShadowBreath_01");
    this.PreloadSound("VO_Netherspite_Male_Dragon_NetherspiteUnstablePortal_02");
    this.PreloadSound("VO_Netherspite_Male_Dragon_NetherspiteAngryChicken_01");
    this.PreloadSound("VO_Moroes_Male_Human_NetherspiteWin_01");
    this.PreloadSound("VO_Moroes_Male_Human_NetherspiteTurn1_01");
    this.PreloadSound("VO_Moroes_Male_Human_NetherspiteTurn7_01");
    this.PreloadSound("VO_Moroes_Male_Human_NetherspiteTurn5_01");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Netherspite_Male_Dragon_NetherspiteEmoteResponse_01",
            m_stringTag = "VO_Netherspite_Male_Dragon_NetherspiteEmoteResponse_01"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR11_Netherspite.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1B6() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR11_Netherspite.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1B7() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR11_Netherspite.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1B8() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR11_Netherspite.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator1B9() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR11_Netherspite.\u003CHandleGameOverWithTiming\u003Ec__Iterator1BA() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
