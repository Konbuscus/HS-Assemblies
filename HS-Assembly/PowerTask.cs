﻿// Decompiled with JetBrains decompiler
// Type: PowerTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class PowerTask
{
  private Network.PowerHistory m_power;
  private bool m_completed;

  public Network.PowerHistory GetPower()
  {
    return this.m_power;
  }

  public void SetPower(Network.PowerHistory power)
  {
    this.m_power = power;
  }

  public bool IsCompleted()
  {
    return this.m_completed;
  }

  public void SetCompleted(bool complete)
  {
    this.m_completed = complete;
  }

  public void DoRealTimeTask(List<Network.PowerHistory> powerList, int index)
  {
    GameState gameState = GameState.Get();
    switch (this.m_power.Type)
    {
      case Network.PowerType.FULL_ENTITY:
        Network.HistFullEntity power1 = (Network.HistFullEntity) this.m_power;
        gameState.OnRealTimeFullEntity(power1);
        break;
      case Network.PowerType.SHOW_ENTITY:
        Network.HistShowEntity power2 = (Network.HistShowEntity) this.m_power;
        gameState.OnRealTimeShowEntity(power2);
        break;
      case Network.PowerType.TAG_CHANGE:
        Network.HistTagChange power3 = (Network.HistTagChange) this.m_power;
        gameState.OnRealTimeTagChange(power3);
        break;
      case Network.PowerType.CREATE_GAME:
        Network.HistCreateGame power4 = (Network.HistCreateGame) this.m_power;
        gameState.OnRealTimeCreateGame(powerList, index, power4);
        break;
      case Network.PowerType.CHANGE_ENTITY:
        Network.HistChangeEntity power5 = (Network.HistChangeEntity) this.m_power;
        gameState.OnRealTimeChangeEntity(power5);
        break;
    }
  }

  public void DoTask()
  {
    if (this.m_completed)
      return;
    GameState gameState = GameState.Get();
    switch (this.m_power.Type)
    {
      case Network.PowerType.FULL_ENTITY:
        Network.HistFullEntity power1 = (Network.HistFullEntity) this.m_power;
        gameState.OnFullEntity(power1);
        break;
      case Network.PowerType.SHOW_ENTITY:
        Network.HistShowEntity power2 = (Network.HistShowEntity) this.m_power;
        gameState.OnShowEntity(power2);
        break;
      case Network.PowerType.HIDE_ENTITY:
        Network.HistHideEntity power3 = (Network.HistHideEntity) this.m_power;
        gameState.OnHideEntity(power3);
        break;
      case Network.PowerType.TAG_CHANGE:
        Network.HistTagChange power4 = (Network.HistTagChange) this.m_power;
        gameState.OnTagChange(power4);
        break;
      case Network.PowerType.META_DATA:
        Network.HistMetaData power5 = (Network.HistMetaData) this.m_power;
        gameState.OnMetaData(power5);
        break;
      case Network.PowerType.CHANGE_ENTITY:
        Network.HistChangeEntity power6 = (Network.HistChangeEntity) this.m_power;
        gameState.OnChangeEntity(power6);
        break;
    }
    this.m_completed = true;
  }

  public void DoEarlyConcedeTask()
  {
    if (this.m_completed)
      return;
    GameState gameState = GameState.Get();
    switch (this.m_power.Type)
    {
      case Network.PowerType.SHOW_ENTITY:
        Network.HistShowEntity power1 = (Network.HistShowEntity) this.m_power;
        gameState.OnEarlyConcedeShowEntity(power1);
        break;
      case Network.PowerType.HIDE_ENTITY:
        Network.HistHideEntity power2 = (Network.HistHideEntity) this.m_power;
        gameState.OnEarlyConcedeHideEntity(power2);
        break;
      case Network.PowerType.TAG_CHANGE:
        Network.HistTagChange power3 = (Network.HistTagChange) this.m_power;
        gameState.OnEarlyConcedeTagChange(power3);
        break;
      case Network.PowerType.CHANGE_ENTITY:
        Network.HistChangeEntity power4 = (Network.HistChangeEntity) this.m_power;
        gameState.OnEarlyConcedeChangeEntity(power4);
        break;
    }
    this.m_completed = true;
  }

  public override string ToString()
  {
    string str = "null";
    if (this.m_power != null)
    {
      switch (this.m_power.Type)
      {
        case Network.PowerType.FULL_ENTITY:
          Network.HistFullEntity power1 = (Network.HistFullEntity) this.m_power;
          str = string.Format("type={0} entity={1} tags={2}", (object) this.m_power.Type, (object) this.GetPrintableEntity(power1.Entity), (object) power1.Entity.Tags);
          break;
        case Network.PowerType.SHOW_ENTITY:
          Network.HistShowEntity power2 = (Network.HistShowEntity) this.m_power;
          str = string.Format("type={0} entity={1} tags={2}", (object) this.m_power.Type, (object) this.GetPrintableEntity(power2.Entity), (object) power2.Entity.Tags);
          break;
        case Network.PowerType.HIDE_ENTITY:
          Network.HistHideEntity power3 = (Network.HistHideEntity) this.m_power;
          str = string.Format("type={0} entity={1} zone={2}", (object) this.m_power.Type, (object) this.GetPrintableEntity(power3.Entity), (object) power3.Zone);
          break;
        case Network.PowerType.TAG_CHANGE:
          Network.HistTagChange power4 = (Network.HistTagChange) this.m_power;
          str = string.Format("type={0} entity={1} {2}", (object) this.m_power.Type, (object) this.GetPrintableEntity(power4.Entity), (object) Tags.DebugTag(power4.Tag, power4.Value));
          break;
        case Network.PowerType.CREATE_GAME:
          str = ((Network.HistCreateGame) this.m_power).ToString();
          break;
        case Network.PowerType.META_DATA:
          str = ((Network.HistMetaData) this.m_power).ToString();
          break;
        case Network.PowerType.CHANGE_ENTITY:
          Network.HistChangeEntity power5 = (Network.HistChangeEntity) this.m_power;
          str = string.Format("type={0} entity={1} tags={2}", (object) this.m_power.Type, (object) this.GetPrintableEntity(power5.Entity), (object) power5.Entity.Tags);
          break;
      }
    }
    return string.Format("power=[{0}] complete={1}", (object) str, (object) this.m_completed);
  }

  private string GetPrintableEntity(int entityId)
  {
    Entity entity = GameState.Get().GetEntity(entityId);
    if (entity == null)
      return entityId.ToString();
    string name = entity.GetName();
    if (name == null)
      return string.Format("[id={0} cardId={1}]", (object) entityId, (object) entity.GetCardId());
    return string.Format("[id={0} cardId={1} name={2}]", (object) entityId, (object) entity.GetCardId(), (object) name);
  }

  private string GetPrintableEntity(Network.Entity netEntity)
  {
    Entity entity = GameState.Get().GetEntity(netEntity.ID);
    string str = entity != null ? entity.GetName() : (string) null;
    if (str == null)
      return string.Format("[id={0} cardId={2}]", (object) netEntity.ID, (object) netEntity.CardID);
    return string.Format("[id={0} cardId={1} name={2}]", (object) netEntity.ID, (object) netEntity.CardID, (object) str);
  }
}
