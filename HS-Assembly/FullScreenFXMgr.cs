﻿// Decompiled with JetBrains decompiler
// Type: FullScreenFXMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class FullScreenFXMgr : MonoBehaviour
{
  private static FullScreenFXMgr s_instance;
  private FullScreenFXMgr.EffectListener m_vignetteListener;
  private FullScreenFXMgr.EffectListener m_desatListener;
  private FullScreenFXMgr.EffectListener m_blurListener;
  private FullScreenFXMgr.EffectListener m_blendToColorListener;
  private int m_ActiveEffectsCount;
  private FullScreenEffects m_FullScreenEffects;
  private int m_StdBlurVignetteCount;

  private void Awake()
  {
    if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
      ApplicationMgr.Get().WillReset += new Action(this.WillReset);
    FullScreenFXMgr.s_instance = this;
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
      ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
    FullScreenFXMgr.s_instance = (FullScreenFXMgr) null;
  }

  public void WillReset()
  {
    this.m_ActiveEffectsCount = 0;
    Camera main = Camera.main;
    if ((UnityEngine.Object) main == (UnityEngine.Object) null)
      return;
    FullScreenEffects component = main.GetComponent<FullScreenEffects>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    component.BlurEnabled = false;
    component.VignettingEnable = false;
    component.DesaturationEnabled = false;
    component.BlendToColorEnable = false;
    this.m_StdBlurVignetteCount = 0;
  }

  public static FullScreenFXMgr Get()
  {
    return FullScreenFXMgr.s_instance;
  }

  public bool isFullScreenEffectActive()
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return false;
    return this.m_FullScreenEffects.isActive();
  }

  private void BeginEffect(string name, string onUpdate, string onComplete, float start, float end, float time, iTween.EaseType easeType)
  {
    Log.FullScreenFX.Print("BeginEffect " + name + " " + (object) start + " => " + (object) end);
    Hashtable args = new Hashtable();
    args[(object) "name"] = (object) name;
    args[(object) "onupdate"] = (object) onUpdate;
    args[(object) "onupdatetarget"] = (object) this.gameObject;
    args[(object) "from"] = (object) start;
    if (!string.IsNullOrEmpty(onComplete))
    {
      args[(object) "oncomplete"] = (object) onComplete;
      args[(object) "oncompletetarget"] = (object) this.gameObject;
    }
    args[(object) "to"] = (object) end;
    args[(object) "time"] = (object) time;
    args[(object) "easetype"] = (object) easeType;
    iTween.StopByName(this.gameObject, name);
    iTween.ValueTo(this.gameObject, args);
  }

  public void StopAllEffects(float delay = 0.0f)
  {
    this.GetCurrEffects();
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null || !this.m_FullScreenEffects.isActive())
      return;
    Log.FullScreenFX.Print("StopAllEffects");
    this.StartCoroutine(this.StopAllEffectsCoroutine(this.m_FullScreenEffects, delay));
  }

  [DebuggerHidden]
  private IEnumerator StopAllEffectsCoroutine(FullScreenEffects effects, float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FullScreenFXMgr.\u003CStopAllEffectsCoroutine\u003Ec__Iterator331() { delay = delay, effects = effects, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003Eeffects = effects, \u003C\u003Ef__this = this };
  }

  public void Vignette(float endVal, float time, iTween.EaseType easeType, FullScreenFXMgr.EffectListener listener = null)
  {
    this.GetCurrEffects();
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.VignettingEnable = true;
    this.m_vignetteListener = listener;
    this.BeginEffect("vignette", "OnVignette", "OnVignetteComplete", 0.0f, endVal, time, easeType);
  }

  public void StopVignette(float time, iTween.EaseType easeType, FullScreenFXMgr.EffectListener listener = null)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null || !this.m_FullScreenEffects.isActive())
      return;
    this.m_FullScreenEffects.VignettingEnable = true;
    this.m_vignetteListener = listener;
    this.BeginEffect("vignette", "OnVignette", "OnVignetteClear", this.m_FullScreenEffects.VignettingIntensity, 0.0f, time, easeType);
  }

  public void OnVignette(float val)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.VignettingIntensity = val;
  }

  public void OnVignetteComplete()
  {
    if (this.m_vignetteListener == null)
      return;
    this.m_vignetteListener();
  }

  public void OnVignetteClear()
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.VignettingEnable = false;
    this.OnVignetteComplete();
  }

  public void Desaturate(float endVal, float time, iTween.EaseType easeType, FullScreenFXMgr.EffectListener listener = null)
  {
    this.GetCurrEffects();
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.DesaturationEnabled = true;
    this.m_desatListener = listener;
    this.BeginEffect("desat", "OnDesat", "OnDesatComplete", this.m_FullScreenEffects.Desaturation, endVal, time, easeType);
  }

  public void StopDesaturate(float time, iTween.EaseType easeType, FullScreenFXMgr.EffectListener listener = null)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null || !this.m_FullScreenEffects.isActive())
      return;
    this.m_FullScreenEffects.DesaturationEnabled = true;
    this.m_desatListener = listener;
    this.BeginEffect("desat", "OnDesat", "OnDesatClear", this.m_FullScreenEffects.Desaturation, 0.0f, time, easeType);
  }

  public void OnDesat(float val)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.Desaturation = val;
  }

  public void OnDesatComplete()
  {
    if (this.m_desatListener == null)
      return;
    this.m_desatListener();
  }

  public void OnDesatClear()
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.DesaturationEnabled = false;
    this.OnDesatComplete();
  }

  public void SetBlurAmount(float val)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.BlurAmount = val;
  }

  public void SetBlurBrightness(float val)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.BlurBrightness = val;
  }

  public void SetBlurDesaturation(float val)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.BlurDesaturation = val;
  }

  public void Blur(float blurVal, float time, iTween.EaseType easeType, FullScreenFXMgr.EffectListener listener = null)
  {
    ++this.m_ActiveEffectsCount;
    this.GetCurrEffects();
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.BlurEnabled = true;
    this.m_blurListener = listener;
    this.BeginEffect("blur", "OnBlur", "OnBlurComplete", this.m_FullScreenEffects.BlurBlend, blurVal, time, easeType);
  }

  public void StopBlur(float time, iTween.EaseType easeType, FullScreenFXMgr.EffectListener listener = null)
  {
    --this.m_ActiveEffectsCount;
    if (this.m_ActiveEffectsCount > 0 || (UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null || !this.m_FullScreenEffects.isActive())
      return;
    this.m_FullScreenEffects.BlurEnabled = true;
    this.m_blurListener = listener;
    this.BeginEffect("blur", "OnBlur", "OnBlurClear", this.m_FullScreenEffects.BlurBlend, 0.0f, time, easeType);
  }

  public void DisableBlur()
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.BlurEnabled = false;
    this.m_FullScreenEffects.BlurBlend = 0.0f;
    this.m_FullScreenEffects.BlurAmount = 0.0f;
  }

  public void OnBlur(float val)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.BlurBlend = val;
  }

  public void OnBlurComplete()
  {
    if (this.m_blurListener == null)
      return;
    this.m_blurListener();
  }

  public void OnBlurClear()
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.BlurEnabled = false;
    this.OnBlurComplete();
  }

  public void BlendToColor(Color blendColor, float endVal, float time, iTween.EaseType easeType, FullScreenFXMgr.EffectListener listener = null)
  {
    this.GetCurrEffects();
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.enabled = true;
    this.m_FullScreenEffects.BlendToColorEnable = true;
    this.m_FullScreenEffects.BlendToColor = blendColor;
    this.m_blendToColorListener = listener;
    this.BeginEffect("blendtocolor", "OnBlendToColor", "OnBlendToColorComplete", 0.0f, endVal, time, easeType);
  }

  public void StopBlendToColor(float time, iTween.EaseType easeType, FullScreenFXMgr.EffectListener listener = null)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null || !this.m_FullScreenEffects.isActive())
      return;
    this.m_FullScreenEffects.BlendToColorEnable = true;
    this.m_blendToColorListener = listener;
    this.BeginEffect("blendtocolor", "OnBlendToColor", "OnBlendToColorClear", this.m_FullScreenEffects.BlendToColorAmount, 0.0f, time, easeType);
  }

  public void OnBlendToColor(float val)
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.BlendToColorAmount = val;
  }

  public void OnBlendToColorComplete()
  {
    if (this.m_blendToColorListener == null)
      return;
    this.m_blendToColorListener();
  }

  public void OnBlendToColorClear()
  {
    if ((UnityEngine.Object) this.m_FullScreenEffects == (UnityEngine.Object) null)
      return;
    this.m_FullScreenEffects.BlendToColorEnable = false;
    this.OnBlendToColorComplete();
  }

  public void StartStandardBlurVignette(float time)
  {
    if (this.m_StdBlurVignetteCount == 0)
    {
      this.SetBlurBrightness(1f);
      this.SetBlurDesaturation(0.0f);
      this.Vignette(0.4f, time, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      this.Blur(1f, time, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
    ++this.m_StdBlurVignetteCount;
  }

  public void EndStandardBlurVignette(float time, FullScreenFXMgr.EffectListener listener = null)
  {
    if (this.m_StdBlurVignetteCount == 0)
      return;
    --this.m_StdBlurVignetteCount;
    if (this.m_StdBlurVignetteCount != 0)
      return;
    this.StopBlur(time, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    this.StopVignette(time, iTween.EaseType.easeOutCirc, listener);
  }

  private FullScreenEffects GetCurrEffects()
  {
    if ((UnityEngine.Object) Camera.main == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Camera.main is null!");
      return (FullScreenEffects) null;
    }
    FullScreenEffects component = Camera.main.GetComponent<FullScreenEffects>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "fullScreenEffects is nulll!");
      return (FullScreenEffects) null;
    }
    this.m_FullScreenEffects = component;
    return component;
  }

  public delegate void EffectListener();
}
