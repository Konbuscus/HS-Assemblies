﻿// Decompiled with JetBrains decompiler
// Type: BoosterDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BoosterDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private int m_SortOrder;
  [SerializeField]
  private string m_OpenPackEvent;
  [SerializeField]
  private string m_BuyWithGoldEvent;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private string m_PackOpeningPrefab;
  [SerializeField]
  private string m_PackOpeningFxPrefab;
  [SerializeField]
  private string m_StorePrefab;
  [SerializeField]
  private string m_ArenaPrefab;
  [SerializeField]
  private bool m_LeavingSoon;
  [SerializeField]
  private DbfLocValue m_LeavingSoonText;

  [DbfField("NOTE_DESC", "designer name of booster contents")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("SORT_ORDER", "client uses this column to determine order of this booster in a displayable list; also used by server to determine the 'current pack', which is defined as the pack with the greatest SORT_ORDER value (see sRandomizeRewardBoosterPack).")]
  public int SortOrder
  {
    get
    {
      return this.m_SortOrder;
    }
  }

  [DbfField("OPEN_PACK_EVENT", "EVENT_TIMING.EVENT that indicates when players are allowed to open packs of this type")]
  public string OpenPackEvent
  {
    get
    {
      return this.m_OpenPackEvent;
    }
  }

  [DbfField("BUY_WITH_GOLD_EVENT", "EVENT_TIMING.EVENT that indicates when players are allowed to buy this pack type with gold (if GOLD_COST > 0)")]
  public string BuyWithGoldEvent
  {
    get
    {
      return this.m_BuyWithGoldEvent;
    }
  }

  [DbfField("NAME", "")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("PACK_OPENING_PREFAB", "")]
  public string PackOpeningPrefab
  {
    get
    {
      return this.m_PackOpeningPrefab;
    }
  }

  [DbfField("PACK_OPENING_FX_PREFAB", "")]
  public string PackOpeningFxPrefab
  {
    get
    {
      return this.m_PackOpeningFxPrefab;
    }
  }

  [DbfField("STORE_PREFAB", "")]
  public string StorePrefab
  {
    get
    {
      return this.m_StorePrefab;
    }
  }

  [DbfField("ARENA_PREFAB", "")]
  public string ArenaPrefab
  {
    get
    {
      return this.m_ArenaPrefab;
    }
  }

  [DbfField("LEAVING_SOON", "this booster will be removed from the in-game store soon")]
  public bool LeavingSoon
  {
    get
    {
      return this.m_LeavingSoon;
    }
  }

  [DbfField("LEAVING_SOON_TEXT", "localized text id explaining why this booster will be removed from the in-game store soon")]
  public DbfLocValue LeavingSoonText
  {
    get
    {
      return this.m_LeavingSoonText;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    BoosterDbfAsset boosterDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (BoosterDbfAsset)) as BoosterDbfAsset;
    if ((UnityEngine.Object) boosterDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("BoosterDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < boosterDbfAsset.Records.Count; ++index)
      boosterDbfAsset.Records[index].StripUnusedLocales();
    records = (object) boosterDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Name.StripUnusedLocales();
    this.m_LeavingSoonText.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetSortOrder(int v)
  {
    this.m_SortOrder = v;
  }

  public void SetOpenPackEvent(string v)
  {
    this.m_OpenPackEvent = v;
  }

  public void SetBuyWithGoldEvent(string v)
  {
    this.m_BuyWithGoldEvent = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetPackOpeningPrefab(string v)
  {
    this.m_PackOpeningPrefab = v;
  }

  public void SetPackOpeningFxPrefab(string v)
  {
    this.m_PackOpeningFxPrefab = v;
  }

  public void SetStorePrefab(string v)
  {
    this.m_StorePrefab = v;
  }

  public void SetArenaPrefab(string v)
  {
    this.m_ArenaPrefab = v;
  }

  public void SetLeavingSoon(bool v)
  {
    this.m_LeavingSoon = v;
  }

  public void SetLeavingSoonText(DbfLocValue v)
  {
    this.m_LeavingSoonText = v;
    v.SetDebugInfo(this.ID, "LEAVING_SOON_TEXT");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (BoosterDbfRecord.\u003C\u003Ef__switch\u0024map1A == null)
      {
        // ISSUE: reference to a compiler-generated field
        BoosterDbfRecord.\u003C\u003Ef__switch\u0024map1A = new Dictionary<string, int>(12)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "SORT_ORDER",
            2
          },
          {
            "OPEN_PACK_EVENT",
            3
          },
          {
            "BUY_WITH_GOLD_EVENT",
            4
          },
          {
            "NAME",
            5
          },
          {
            "PACK_OPENING_PREFAB",
            6
          },
          {
            "PACK_OPENING_FX_PREFAB",
            7
          },
          {
            "STORE_PREFAB",
            8
          },
          {
            "ARENA_PREFAB",
            9
          },
          {
            "LEAVING_SOON",
            10
          },
          {
            "LEAVING_SOON_TEXT",
            11
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (BoosterDbfRecord.\u003C\u003Ef__switch\u0024map1A.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.SortOrder;
          case 3:
            return (object) this.OpenPackEvent;
          case 4:
            return (object) this.BuyWithGoldEvent;
          case 5:
            return (object) this.Name;
          case 6:
            return (object) this.PackOpeningPrefab;
          case 7:
            return (object) this.PackOpeningFxPrefab;
          case 8:
            return (object) this.StorePrefab;
          case 9:
            return (object) this.ArenaPrefab;
          case 10:
            return (object) this.LeavingSoon;
          case 11:
            return (object) this.LeavingSoonText;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (BoosterDbfRecord.\u003C\u003Ef__switch\u0024map1B == null)
    {
      // ISSUE: reference to a compiler-generated field
      BoosterDbfRecord.\u003C\u003Ef__switch\u0024map1B = new Dictionary<string, int>(12)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "SORT_ORDER",
          2
        },
        {
          "OPEN_PACK_EVENT",
          3
        },
        {
          "BUY_WITH_GOLD_EVENT",
          4
        },
        {
          "NAME",
          5
        },
        {
          "PACK_OPENING_PREFAB",
          6
        },
        {
          "PACK_OPENING_FX_PREFAB",
          7
        },
        {
          "STORE_PREFAB",
          8
        },
        {
          "ARENA_PREFAB",
          9
        },
        {
          "LEAVING_SOON",
          10
        },
        {
          "LEAVING_SOON_TEXT",
          11
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!BoosterDbfRecord.\u003C\u003Ef__switch\u0024map1B.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetSortOrder((int) val);
        break;
      case 3:
        this.SetOpenPackEvent((string) val);
        break;
      case 4:
        this.SetBuyWithGoldEvent((string) val);
        break;
      case 5:
        this.SetName((DbfLocValue) val);
        break;
      case 6:
        this.SetPackOpeningPrefab((string) val);
        break;
      case 7:
        this.SetPackOpeningFxPrefab((string) val);
        break;
      case 8:
        this.SetStorePrefab((string) val);
        break;
      case 9:
        this.SetArenaPrefab((string) val);
        break;
      case 10:
        this.SetLeavingSoon((bool) val);
        break;
      case 11:
        this.SetLeavingSoonText((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (BoosterDbfRecord.\u003C\u003Ef__switch\u0024map1C == null)
      {
        // ISSUE: reference to a compiler-generated field
        BoosterDbfRecord.\u003C\u003Ef__switch\u0024map1C = new Dictionary<string, int>(12)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "SORT_ORDER",
            2
          },
          {
            "OPEN_PACK_EVENT",
            3
          },
          {
            "BUY_WITH_GOLD_EVENT",
            4
          },
          {
            "NAME",
            5
          },
          {
            "PACK_OPENING_PREFAB",
            6
          },
          {
            "PACK_OPENING_FX_PREFAB",
            7
          },
          {
            "STORE_PREFAB",
            8
          },
          {
            "ARENA_PREFAB",
            9
          },
          {
            "LEAVING_SOON",
            10
          },
          {
            "LEAVING_SOON_TEXT",
            11
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (BoosterDbfRecord.\u003C\u003Ef__switch\u0024map1C.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (int);
          case 3:
            return typeof (string);
          case 4:
            return typeof (string);
          case 5:
            return typeof (DbfLocValue);
          case 6:
            return typeof (string);
          case 7:
            return typeof (string);
          case 8:
            return typeof (string);
          case 9:
            return typeof (string);
          case 10:
            return typeof (bool);
          case 11:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
