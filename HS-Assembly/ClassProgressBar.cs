﻿// Decompiled with JetBrains decompiler
// Type: ClassProgressBar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ClassProgressBar : PegUIElement
{
  public TAG_CLASS m_class;
  public UberText m_classNameText;
  public UberText m_levelText;
  public GameObject m_classLockedGO;
  public UberText m_lockedText;
  public ProgressBar m_progressBar;
  public GameObject m_classIcon;
  private NetCache.HeroLevel.NextLevelReward m_nextLevelReward;
  private string m_rewardText;

  protected override void Awake()
  {
    base.Awake();
    this.m_lockedText.Text = GameStrings.Get("GLUE_QUEST_LOG_CLASS_LOCKED");
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnProgressBarOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnProgressBarOut));
  }

  public void Init()
  {
    this.m_classNameText.Text = GameStrings.GetClassName(this.m_class);
  }

  public void SetNextReward(NetCache.HeroLevel.NextLevelReward nextLevelReward)
  {
    this.m_nextLevelReward = nextLevelReward;
  }

  private void ShowTooltip()
  {
    TooltipPanel tooltipPanel = this.gameObject.GetComponent<TooltipZone>().ShowLayerTooltip(GameStrings.Format("GLOBAL_HERO_LEVEL_NEXT_REWARD_TITLE", (object) this.m_nextLevelReward.Level), this.m_rewardText);
    tooltipPanel.m_name.WordWrap = false;
    tooltipPanel.m_name.UpdateNow();
  }

  private void OnProgressBarOver(UIEvent e)
  {
    if (this.m_rewardText != null)
    {
      this.ShowTooltip();
    }
    else
    {
      if (this.m_nextLevelReward == null)
        return;
      RewardData reward = this.m_nextLevelReward.Reward;
      if (reward == null)
        return;
      this.m_rewardText = RewardUtils.GetRewardText(reward);
      this.ShowTooltip();
    }
  }

  private void OnProgressBarOut(UIEvent e)
  {
    this.gameObject.GetComponent<TooltipZone>().HideTooltip();
  }
}
