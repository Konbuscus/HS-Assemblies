﻿// Decompiled with JetBrains decompiler
// Type: Cinematic
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class Cinematic : MonoBehaviour
{
  private const string CINEMATIC_FILE_NAME = "Cinematic";
  private const float MOVIE_LOAD_TIMEOUT = 10f;
  private bool m_isPlaying;
  private bool m_isMovieLoaded;
  private bool m_isMovieAudioLoaded;
  private Cinematic.PlayOptions m_options;
  private MovieTexture m_MovieTexture;
  private AudioClip m_MovieAudio;
  private SoundDucker m_soundDucker;
  private int m_previousTargetFrameRate;

  private void Awake()
  {
    this.m_soundDucker = this.gameObject.AddComponent<SoundDucker>();
    this.m_soundDucker.m_GlobalDuckDef = new SoundDuckedCategoryDef();
    this.m_soundDucker.m_GlobalDuckDef.m_Volume = 0.0f;
    this.m_soundDucker.m_GlobalDuckDef.m_RestoreSec = 1.5f;
    this.m_soundDucker.m_GlobalDuckDef.m_BeginSec = 1.5f;
  }

  public void Play(Cinematic.MovieCallback callback)
  {
    this.m_options = new Cinematic.PlayOptions();
    this.m_options.callback = callback;
    string empty = string.Empty;
    string methodName = "PlayPC";
    if (methodName == string.Empty)
      return;
    this.StartCoroutine(methodName);
  }

  public bool isPlaying()
  {
    return this.m_isPlaying;
  }

  private void StartPlaying()
  {
    this.m_isPlaying = true;
    this.m_previousTargetFrameRate = Application.targetFrameRate;
    Application.targetFrameRate = 0;
    Options.Get().SetBool(Option.HAS_SEEN_CINEMATIC, true);
    this.m_soundDucker.StartDucking();
  }

  private void StopPlaying()
  {
    Application.targetFrameRate = this.m_previousTargetFrameRate;
    this.m_isPlaying = false;
    if (this.m_options != null)
      this.m_options.callback();
    this.m_soundDucker.StopDucking();
  }

  private static void PlayCinematic(string gameObjectName, string localeName)
  {
  }

  private void OnGUI()
  {
    if (!this.m_isPlaying)
      return;
    GUI.DrawTexture(new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height), (Texture) this.m_MovieTexture, ScaleMode.ScaleToFit, false, 0.0f);
  }

  [DebuggerHidden]
  private IEnumerator PlayPC()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Cinematic.\u003CPlayPC\u003Ec__Iterator325() { \u003C\u003Ef__this = this };
  }

  private void MovieLoaded(string name, Object obj, object callbackData)
  {
    if (obj == (Object) null)
    {
      UnityEngine.Debug.LogError((object) "Failed to load Cinematic movie!");
    }
    else
    {
      this.m_isMovieLoaded = true;
      this.m_MovieTexture = obj as MovieTexture;
    }
  }

  private void AudioLoaded(string name, GameObject obj, object callbackData)
  {
    if ((Object) obj == (Object) null)
    {
      UnityEngine.Debug.LogError((object) "Failed to load Cinematic Audio Track!");
    }
    else
    {
      this.m_isMovieAudioLoaded = true;
      this.m_MovieAudio = obj.GetComponent<AudioSource>().clip;
    }
  }

  private class PlayOptions
  {
    public Cinematic.MovieCallback callback;
  }

  public delegate void MovieCallback();
}
