﻿// Decompiled with JetBrains decompiler
// Type: MobileChatLogMessageFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MobileChatLogMessageFrame : MonoBehaviour, ITouchListItem
{
  public UberText text;
  public GameObject m_Background;
  private Bounds localBounds;

  public string Message
  {
    get
    {
      return this.text.Text;
    }
    set
    {
      this.text.Text = value;
      this.text.UpdateNow();
      this.UpdateLocalBounds();
    }
  }

  public bool IsHeader
  {
    get
    {
      return false;
    }
  }

  public bool Visible
  {
    get
    {
      return true;
    }
    set
    {
    }
  }

  public Color Color
  {
    get
    {
      return this.text.TextColor;
    }
    set
    {
      this.text.TextColor = value;
    }
  }

  public float Width
  {
    get
    {
      return this.text.Width;
    }
    set
    {
      this.text.Width = value;
      if (!((Object) this.m_Background != (Object) null))
        return;
      float x = this.m_Background.GetComponent<MeshFilter>().mesh.bounds.size.x;
      this.m_Background.transform.localScale = new Vector3(value / x, this.m_Background.transform.localScale.y, 1f);
      this.m_Background.transform.localPosition = new Vector3((float) (-(double) value / (0.5 * (double) x)), 0.0f, 0.0f);
    }
  }

  public Bounds LocalBounds
  {
    get
    {
      return this.localBounds;
    }
  }

  public new T GetComponent<T>() where T : Component
  {
    return base.GetComponent<T>();
  }

  private void UpdateLocalBounds()
  {
    Bounds textBounds = this.text.GetTextBounds();
    Vector3 size = textBounds.size;
    this.localBounds.center = this.transform.InverseTransformPoint(textBounds.center) + 10f * Vector3.up;
    this.localBounds.size = size;
  }

  GameObject ITouchListItem.get_gameObject()
  {
    return this.gameObject;
  }

  Transform ITouchListItem.get_transform()
  {
    return this.transform;
  }
}
