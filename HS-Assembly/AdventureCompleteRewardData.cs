﻿// Decompiled with JetBrains decompiler
// Type: AdventureCompleteRewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class AdventureCompleteRewardData : RewardData
{
  private const string s_DefaultRewardObject = "AdventureCompleteReward_Naxxramas";

  public AdventureModeDbId ModeId { get; set; }

  public string RewardObjectName { get; set; }

  public string BannerText { get; set; }

  public AdventureCompleteRewardData()
    : this(AdventureModeDbId.INVALID, "AdventureCompleteReward_Naxxramas", string.Empty)
  {
  }

  public AdventureCompleteRewardData(AdventureModeDbId modeId, string rewardObjectName, string bannerText)
    : base(Reward.Type.CLASS_CHALLENGE)
  {
    this.ModeId = modeId;
    this.RewardObjectName = rewardObjectName;
    this.BannerText = bannerText;
  }

  public override string ToString()
  {
    return string.Format("[AdventureCompleteRewardData: RewardObjectName={0} Origin={1} OriginData={2}]", (object) this.RewardObjectName, (object) this.Origin, (object) this.OriginData);
  }

  protected override string GetGameObjectName()
  {
    return this.RewardObjectName;
  }
}
