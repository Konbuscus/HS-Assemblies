﻿// Decompiled with JetBrains decompiler
// Type: BaseUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class BaseUI : MonoBehaviour
{
  public BaseUIBones m_Bones;
  public BaseUIPrefabs m_Prefabs;
  public Camera m_BnetCamera;
  public Camera m_BnetDialogCamera;
  public BnetBar m_BnetBar;
  public ExistingAccountPopup m_ExistingAccountPopup;
  private static BaseUI s_instance;

  private void Awake()
  {
    BaseUI.s_instance = this;
    ((Component) UnityEngine.Object.Instantiate((UnityEngine.Object) this.m_Prefabs.m_ChatMgrPrefab, this.transform.position, Quaternion.identity)).transform.parent = this.transform;
    this.m_BnetCamera.GetComponent<ScreenResizeDetector>().AddSizeChangedListener(new ScreenResizeDetector.SizeChangedCallback(this.OnScreenSizeChanged));
  }

  private void OnDestroy()
  {
    BaseUI.s_instance = (BaseUI) null;
  }

  private void Start()
  {
    this.UpdateLayout();
    InnKeepersSpecial.Init();
  }

  private void OnGUI()
  {
    if (Event.current.type != UnityEngine.EventType.KeyUp)
      return;
    switch (Event.current.keyCode)
    {
      case KeyCode.Print:
      case KeyCode.SysReq:
      case KeyCode.F13:
        this.TakeScreenshot();
        break;
    }
  }

  public static BaseUI Get()
  {
    return BaseUI.s_instance;
  }

  public void OnLoggedIn()
  {
    this.m_BnetBar.OnLoggedIn();
  }

  public Camera GetBnetCamera()
  {
    return this.m_BnetCamera;
  }

  public Camera GetBnetDialogCamera()
  {
    return this.m_BnetDialogCamera;
  }

  public Transform GetAddFriendBone()
  {
    if (!UniversalInputManager.Get().IsTouchMode())
      return this.m_Bones.m_AddFriend;
    if ((bool) UniversalInputManager.UsePhoneUI)
      return this.m_Bones.m_AddFriendPhoneKeyboard;
    return this.m_Bones.m_AddFriendVirtualKeyboard;
  }

  public Transform GetRecruitAFriendBone()
  {
    return this.m_Bones.m_RecruitAFriend;
  }

  public Transform GetChatBubbleBone()
  {
    return this.m_Bones.m_ChatBubble;
  }

  public Transform GetGameMenuBone(bool withRatings = false)
  {
    if (SceneMgr.Get().IsInGame())
      return this.m_Bones.m_InGameMenu;
    if (withRatings)
      return this.m_Bones.m_BoxMenuWithRatings;
    return this.m_Bones.m_BoxMenu;
  }

  public Transform GetOptionsMenuBone()
  {
    return this.m_Bones.m_OptionsMenu;
  }

  public Transform GetQuickChatBone()
  {
    if (UniversalInputManager.Get().IsTouchMode() && W8Touch.s_isWindows8OrGreater || W8Touch.Get().IsVirtualKeyboardVisible())
      return this.m_Bones.m_QuickChatVirtualKeyboard;
    return this.m_Bones.m_QuickChat;
  }

  public bool HandleKeyboardInput()
  {
    if ((UnityEngine.Object) this.m_BnetBar != (UnityEngine.Object) null && this.m_BnetBar.HandleKeyboardInput())
      return true;
    if (!Input.GetKeyUp(BackButton.backKey) || PlatformSettings.OS != OSCategory.Android)
      return false;
    UnityEngine.Debug.Log((object) "Going to home screen on device!");
    return true;
  }

  private void OnScreenSizeChanged(object userData)
  {
    this.UpdateLayout();
  }

  private void UpdateLayout()
  {
    this.m_BnetBar.UpdateLayout();
    if ((UnityEngine.Object) ChatMgr.Get() != (UnityEngine.Object) null)
      ChatMgr.Get().UpdateLayout();
    if (!((UnityEngine.Object) SplashScreen.Get() != (UnityEngine.Object) null))
      return;
    SplashScreen.Get().UpdateLayout();
  }

  private void TakeScreenshot()
  {
    string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
    DateTime now = DateTime.Now;
    string str = string.Format("{0}/Hearthstone Screenshot {1:MM-dd-yy HH.mm.ss}.png", (object) folderPath, (object) now);
    int num = 1;
    while (File.Exists(str))
      str = string.Format("{0}/Hearthstone Screenshot {1:MM-dd-yy HH.mm.ss} {2}.png", (object) folderPath, (object) now, (object) num++);
    UIStatus.Get().HideIfScreenshotMessage();
    Application.CaptureScreenshot(str);
    this.StartCoroutine(this.NotifyOfScreenshotComplete());
    UnityEngine.Debug.Log((object) string.Format("screenshot saved to {0}", (object) str));
  }

  [DebuggerHidden]
  private IEnumerator NotifyOfScreenshotComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BaseUI.\u003CNotifyOfScreenshotComplete\u003Ec__Iterator345 completeCIterator345 = new BaseUI.\u003CNotifyOfScreenshotComplete\u003Ec__Iterator345();
    return (IEnumerator) completeCIterator345;
  }
}
