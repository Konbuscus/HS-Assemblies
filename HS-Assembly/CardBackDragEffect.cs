﻿// Decompiled with JetBrains decompiler
// Type: CardBackDragEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardBackDragEffect : MonoBehaviour
{
  private float m_Min = 2f;
  private float m_Max = 30f;
  private const float MIN_VELOCITY = 2f;
  private const float MAX_VELOCITY = 30f;
  public Actor m_Actor;
  public GameObject m_EffectsRoot;
  private CardBackManager m_CardBackManager;
  private Vector3 m_LastPosition;
  private float m_Speed;
  private bool m_Active;

  private void Awake()
  {
  }

  private void Start()
  {
    if (!(bool) ((Object) SceneMgr.Get()) || SceneMgr.Get().GetMode() != SceneMgr.Mode.GAMEPLAY)
    {
      this.enabled = false;
    }
    else
    {
      this.m_LastPosition = this.transform.position;
      if ((Object) this.m_CardBackManager == (Object) null)
      {
        this.m_CardBackManager = CardBackManager.Get();
        if ((Object) this.m_CardBackManager == (Object) null)
        {
          Debug.LogError((object) "Failed to get CardBackManager!");
          this.enabled = false;
        }
      }
      this.SetEffect();
    }
  }

  private void FixedUpdate()
  {
  }

  private void Update()
  {
    if (!((Object) this.m_EffectsRoot != (Object) null))
      return;
    if (!this.GetComponent<Renderer>().enabled)
    {
      this.ShowParticles(false);
      if (!this.m_EffectsRoot.activeSelf)
        return;
      this.m_EffectsRoot.SetActive(false);
    }
    else
    {
      this.m_Speed = (float) ((double) (this.transform.position - this.m_LastPosition).magnitude / (double) Time.deltaTime * 3.59999990463257);
      this.UpdateDragEffect();
      this.m_LastPosition = this.transform.position;
    }
  }

  private void OnDisable()
  {
    if (!((Object) this.m_EffectsRoot != (Object) null))
      return;
    this.ShowParticles(false);
  }

  private void OnDestroy()
  {
  }

  private void OnEnable()
  {
  }

  public void SetEffect()
  {
    if ((Object) this.m_CardBackManager == (Object) null)
    {
      this.m_CardBackManager = CardBackManager.Get();
      if ((Object) this.m_CardBackManager == (Object) null)
      {
        Debug.LogError((object) "Failed to get CardBackManager!");
        this.enabled = false;
        return;
      }
    }
    bool friendlySide = true;
    Entity entity = this.m_Actor.GetEntity();
    if (entity != null)
    {
      Player controller = entity.GetController();
      if (controller != null && controller.GetSide() == Player.Side.OPPOSING)
        friendlySide = false;
    }
    this.m_CardBackManager.UpdateDragEffect(this.gameObject, friendlySide);
    CardBack cardBack = this.m_CardBackManager.GetCardBack(this.m_Actor);
    if (!((Object) cardBack != (Object) null))
      return;
    this.m_Min = cardBack.m_EffectMinVelocity;
    this.m_Max = cardBack.m_EffectMaxVelocity;
  }

  private void UpdateDragEffect()
  {
    if ((double) this.m_Speed > (double) this.m_Min && (double) this.m_Speed < (double) this.m_Max)
    {
      if (this.m_Active)
        return;
      this.m_Active = true;
      this.ShowParticles(true);
    }
    else
    {
      if (!this.m_Active)
        return;
      this.m_Active = false;
      this.ShowParticles(false);
    }
  }

  private void ShowParticles(bool show)
  {
    if (show)
    {
      foreach (ParticleSystem componentsInChild in this.GetComponentsInChildren<ParticleSystem>())
      {
        if (!((Object) componentsInChild == (Object) null))
          componentsInChild.Play();
      }
    }
    else
    {
      foreach (ParticleSystem componentsInChild in this.GetComponentsInChildren<ParticleSystem>())
      {
        if ((Object) componentsInChild == (Object) null)
          break;
        componentsInChild.Stop();
      }
    }
  }
}
