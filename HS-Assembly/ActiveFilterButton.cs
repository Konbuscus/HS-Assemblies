﻿// Decompiled with JetBrains decompiler
// Type: ActiveFilterButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ActiveFilterButton : MonoBehaviour
{
  private string m_manaFilterValue = string.Empty;
  private string m_searchFilterValue = string.Empty;
  public SlidingTray m_manaFilterTray;
  public SlidingTray m_setFilterTray;
  public UberText m_searchText;
  public GameObject m_manaFilterIcon;
  public UberText m_manaFilterText;
  public PegUIElement m_activeFilterButton;
  public PegUIElement m_inactiveFilterButton;
  public ManaFilterTabManager m_manaFilter;
  public SetFilterTray m_setFilter;
  public NestedPrefab m_setFilterContainer;
  public CollectionSearch m_search;
  public PegUIElement m_offClickCatcher;
  public UIBButton m_doneButton;
  public Material m_enabledMaterial;
  public Material m_disabledMaterial;
  public MeshRenderer m_inactiveFilterButtonRenderer;
  public GameObject m_inactiveFilterButtonText;
  public Transform m_manaFilterIconCenterBone;
  public Transform m_setFilterIconCenterBone;
  private bool m_filtersShown;
  private bool m_manaFilterActive;
  private bool m_searchFilterActive;
  private Vector3 m_manaFilterIconDefaultPos;
  private Vector3 m_setFilterIconDefaultPos;

  protected void Awake()
  {
    if ((Object) this.m_inactiveFilterButton != (Object) null)
      this.m_inactiveFilterButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ShowFilters()));
    if ((Object) this.m_activeFilterButton != (Object) null)
      this.m_activeFilterButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ClearFilters()));
    if ((Object) this.m_doneButton != (Object) null)
      this.m_doneButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OffClickPressed()));
    CollectionManagerDisplay.Get().RegisterManaFilterListener(new CollectionManagerDisplay.FilterStateListener(this.ManaFilterUpdate));
    CollectionManagerDisplay.Get().RegisterSearchFilterListener(new CollectionManagerDisplay.FilterStateListener(this.SearchFilterUpdate));
  }

  protected void Start()
  {
    if ((Object) this.m_setFilterContainer != (Object) null)
    {
      this.m_setFilter = this.m_setFilterContainer.PrefabGameObject(false).GetComponent<SetFilterTray>();
      this.m_setFilter.m_toggleButton.transform.parent = this.transform;
      this.m_setFilterIconDefaultPos = this.m_setFilter.m_toggleButton.transform.localPosition;
    }
    this.m_manaFilterIconDefaultPos = this.m_manaFilterIcon.transform.localPosition;
    this.FiltersUpdated();
  }

  public void OnDestroy()
  {
    CollectionManagerDisplay collectionManagerDisplay = CollectionManagerDisplay.Get();
    if (!((Object) collectionManagerDisplay != (Object) null))
      return;
    collectionManagerDisplay.UnregisterManaFilterListener(new CollectionManagerDisplay.FilterStateListener(this.ManaFilterUpdate));
    collectionManagerDisplay.UnregisterSearchFilterListener(new CollectionManagerDisplay.FilterStateListener(this.SearchFilterUpdate));
  }

  public void ShowFilters()
  {
    CollectionManagerDisplay.Get().HideDeckHelpPopup();
    Navigation.Push(new Navigation.NavigateBackHandler(this.HideFilters));
    this.m_manaFilterTray.ToggleTraySlider(true, (Transform) null, true);
    this.m_setFilterTray.ToggleTraySlider(true, (Transform) null, true);
    this.m_setFilter.Show(true);
    this.m_manaFilter.m_manaCrystalContainer.UpdateSlices();
  }

  public bool HideFilters()
  {
    this.m_manaFilterTray.ToggleTraySlider(false, (Transform) null, true);
    this.m_setFilterTray.ToggleTraySlider(false, (Transform) null, true);
    CollectionManagerDisplay.Get().m_search.Deactivate();
    this.m_setFilter.Show(false);
    return true;
  }

  public void OffClickPressed()
  {
    Navigation.GoBack();
    this.FiltersUpdated();
  }

  public void ClearFilters()
  {
    this.m_manaFilter.ClearFilter();
    this.m_setFilter.ClearFilter();
    this.m_search.ClearFilter(true);
  }

  public void SetEnabled(bool enabled)
  {
    this.m_inactiveFilterButton.SetEnabled(enabled);
    this.m_inactiveFilterButtonText.SetActive(enabled);
    this.m_inactiveFilterButtonRenderer.sharedMaterial = !enabled ? this.m_disabledMaterial : this.m_enabledMaterial;
  }

  public void ManaFilterUpdate(bool state, object description)
  {
    this.m_manaFilterActive = state;
    this.m_manaFilterValue = description != null ? (string) description : string.Empty;
    this.FiltersUpdated();
  }

  public void SearchFilterUpdate(bool state, object description)
  {
    this.m_searchFilterActive = state;
    this.m_searchFilterValue = description != null ? (string) description : string.Empty;
    this.FiltersUpdated();
  }

  private void FiltersUpdated()
  {
    bool flag = this.m_manaFilterActive || this.m_searchFilterActive || this.m_setFilter.HasActiveFilter();
    if ((Object) this.m_inactiveFilterButton != (Object) null)
    {
      this.m_activeFilterButton.gameObject.SetActive(flag);
      this.m_inactiveFilterButton.gameObject.SetActive(!flag);
    }
    else
    {
      if (this.m_filtersShown != flag)
      {
        Vector3 euler = !flag ? new Vector3(0.0f, 0.0f, 0.0f) : new Vector3(180f, 0.0f, 0.0f);
        float num = !flag ? -0.5f : 0.5f;
        iTween.Stop(this.m_activeFilterButton.gameObject);
        this.m_activeFilterButton.gameObject.transform.localRotation = Quaternion.Euler(euler);
        iTween.RotateBy(this.m_activeFilterButton.gameObject, iTween.Hash((object) "x", (object) num, (object) "time", (object) 0.25f, (object) "easetype", (object) iTween.EaseType.easeInOutExpo));
      }
      this.m_filtersShown = flag;
    }
    this.m_searchText.Text = !this.m_searchFilterActive ? string.Empty : this.m_searchFilterValue;
    this.m_manaFilterIcon.SetActive(this.m_manaFilterActive && !this.m_searchFilterActive);
    this.m_manaFilterText.Text = this.m_manaFilterValue;
    bool isShown = this.m_setFilter.HasActiveFilter() && !this.m_searchFilterActive;
    this.m_setFilter.SetButtonShown(isShown);
    if (this.m_manaFilterIcon.activeSelf && !isShown)
      this.m_manaFilterIcon.transform.localPosition = this.m_manaFilterIconCenterBone.localPosition;
    else if (!this.m_manaFilterIcon.activeSelf && isShown)
    {
      this.m_setFilter.m_toggleButton.gameObject.transform.localPosition = this.m_setFilterIconCenterBone.localPosition;
    }
    else
    {
      this.m_manaFilterIcon.transform.localPosition = this.m_manaFilterIconDefaultPos;
      this.m_setFilter.m_toggleButton.gameObject.transform.localPosition = this.m_setFilterIconDefaultPos;
    }
  }
}
