﻿// Decompiled with JetBrains decompiler
// Type: DyingSecretGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class DyingSecretGroup
{
  private List<Card> m_cards = new List<Card>();
  private List<Actor> m_actors = new List<Actor>();
  private Card m_mainCard;

  public Card GetMainCard()
  {
    return this.m_mainCard;
  }

  public List<Actor> GetActors()
  {
    return this.m_actors;
  }

  public List<Card> GetCards()
  {
    return this.m_cards;
  }

  public void AddCard(Card card)
  {
    if ((UnityEngine.Object) this.m_mainCard == (UnityEngine.Object) null)
      this.m_mainCard = card.GetZone().GetCards().Find((Predicate<Card>) (currCard => currCard.IsShown()));
    this.m_cards.Add(card);
    this.m_actors.Add(card.GetActor());
  }
}
