﻿// Decompiled with JetBrains decompiler
// Type: BnetBarKeyboard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class BnetBarKeyboard : PegUIElement
{
  private List<OnKeyboardPressed> m_keyboardPressedListeners = new List<OnKeyboardPressed>();
  public Color m_highlight;
  public Color m_origColor;

  public void ShowHighlight(bool show)
  {
    if (show)
      this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", this.m_highlight);
    else
      this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", this.m_origColor);
  }

  protected override void OnPress()
  {
    W8Touch.Get().ShowKeyboard();
    foreach (OnKeyboardPressed onKeyboardPressed in this.m_keyboardPressedListeners.ToArray())
      onKeyboardPressed();
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    this.ShowHighlight(true);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.ShowHighlight(false);
  }

  public void RegisterKeyboardPressedListener(OnKeyboardPressed listener)
  {
    if (this.m_keyboardPressedListeners.Contains(listener))
      return;
    this.m_keyboardPressedListeners.Add(listener);
  }

  public void UnregisterKeyboardPressedListener(OnKeyboardPressed listener)
  {
    this.m_keyboardPressedListeners.Remove(listener);
  }
}
