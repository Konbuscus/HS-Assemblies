﻿// Decompiled with JetBrains decompiler
// Type: TutorialNotification
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TutorialNotification : Notification
{
  public UIBButton m_ButtonStart;
  public UberText m_WantedText;

  public void SetWantedText(string txt)
  {
    if (!((Object) this.m_WantedText != (Object) null))
      return;
    this.m_WantedText.Text = txt;
    this.m_WantedText.gameObject.SetActive(true);
  }
}
