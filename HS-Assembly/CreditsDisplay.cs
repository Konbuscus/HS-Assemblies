﻿// Decompiled with JetBrains decompiler
// Type: CreditsDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class CreditsDisplay : MonoBehaviour
{
  private bool m_displayingLatestYear = true;
  private int m_lastCard = 1;
  private const float CREDITS_SCROLL_SPEED = 2.5f;
  private const int MAX_LINES_PER_CHUNK = 70;
  private const string START_CREDITS_COROUTINE = "StartCredits";
  private const string SHOW_NEW_CARD_COROUTINE = "ShowNewCard";
  public GameObject m_creditsRoot;
  public UberText m_creditsText1;
  public UberText m_creditsText2;
  private UberText m_currentText;
  public Transform m_offscreenCardBone;
  public Transform m_cardBone;
  public UIBButton m_doneButton;
  public UIBButton m_yearButton;
  public Transform m_flopPoint;
  public GameObject m_doneArrowInButton;
  private static CreditsDisplay s_instance;
  private string[] m_creditLines;
  private int m_currentLine;
  private List<Actor> m_fakeCards;
  private List<FullDef> m_creditsDefs;
  private bool started;
  private bool m_creditsTextLoaded;
  private bool m_creditsTextLoadSucceeded;
  private bool m_creditsDone;
  private Actor m_shownCreditsCard;
  private Vector3 creditsRootStartLocalPosition;
  private Vector3 creditsText1StartLocalPosition;
  private Vector3 creditsText2StartLocalPosition;

  public static CreditsDisplay Get()
  {
    return CreditsDisplay.s_instance;
  }

  private void Awake()
  {
    CreditsDisplay.s_instance = this;
    this.m_fakeCards = new List<Actor>();
    this.m_creditsDefs = new List<FullDef>();
    this.creditsRootStartLocalPosition = this.m_creditsRoot.transform.localPosition;
    this.creditsText1StartLocalPosition = this.m_creditsText1.transform.localPosition;
    this.creditsText2StartLocalPosition = this.m_creditsText2.transform.localPosition;
    this.m_doneButton.SetText(GameStrings.Get("GLOBAL_BACK"));
    this.m_doneButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDonePressed));
    this.m_yearButton.SetText(!this.m_displayingLatestYear ? "2015" : "2014");
    this.m_yearButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnYearPressed));
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      Box.Get().m_tableTop.SetActive(false);
      Box.Get().m_letterboxingContainer.SetActive(false);
      this.m_doneButton.SetText(string.Empty);
      this.m_doneArrowInButton.SetActive(true);
    }
    AssetLoader.Get().LoadActor("Card_Hand_Ally", new AssetLoader.GameObjectCallback(this.ActorLoadedCallback), (object) null, false);
    AssetLoader.Get().LoadActor("Card_Hand_Ally", new AssetLoader.GameObjectCallback(this.ActorLoadedCallback), (object) null, false);
    this.LoadAllCreditsCards();
    this.LoadCreditsText();
  }

  private void OnDestoy()
  {
    CreditsDisplay.s_instance = (CreditsDisplay) null;
  }

  private void LoadAllCreditsCards()
  {
    if (this.m_displayingLatestYear)
    {
      DefLoader.Get().LoadFullDef("CRED_01", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_02", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_03", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_04", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_05", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_06", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_07", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_08", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_09", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_10", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_11", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_12", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_14", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_15", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_16", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_18", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_19", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_20", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_21", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_22", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_23", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_24", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_25", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_26", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_27", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_28", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_29", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_30", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_31", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_32", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_33", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_34", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_35", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_36", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_37", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_38", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_39", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_40", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_41", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_42", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_43", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_44", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_45", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_46", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
    }
    else
    {
      DefLoader.Get().LoadFullDef("CRED_01", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_02", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_03", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_04", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_05", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_06", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_07", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_08", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_09", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_10", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_11", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_12", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_13", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_14", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_15", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_16", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
      DefLoader.Get().LoadFullDef("CRED_17", new DefLoader.LoadDefCallback<FullDef>(this.OnFullDefLoaded));
    }
  }

  private void LoadCreditsText()
  {
    this.m_creditsTextLoadSucceeded = false;
    string filePath = this.GetFilePath();
    if (filePath == null)
    {
      Error.AddDevWarning("Credits Error", "CreditsDisplay.LoadCreditsText() - Failed to find file for CREDITS.");
      this.m_creditsTextLoaded = true;
    }
    else
    {
      try
      {
        this.m_creditLines = File.ReadAllLines(filePath);
        this.m_creditsTextLoadSucceeded = true;
      }
      catch (Exception ex)
      {
        Error.AddDevWarning("Credits Error", "CreditsDisplay.LoadCreditsText() - Failed to read \"{0}\".\n\nException: {1}", (object) filePath, (object) ex.Message);
      }
      this.m_creditsTextLoaded = true;
    }
  }

  private string GetFilePath()
  {
    foreach (Locale locale in Localization.GetLoadOrder(false))
    {
      string fileName = "CREDITS_" + (!this.m_displayingLatestYear ? "2014" : "2015") + ".txt";
      string assetPath = GameStrings.GetAssetPath(locale, fileName);
      if (File.Exists(assetPath))
        return assetPath;
    }
    return (string) null;
  }

  private void FlopCredits()
  {
    this.m_currentText = !((UnityEngine.Object) this.m_currentText == (UnityEngine.Object) this.m_creditsText1) ? this.m_creditsText1 : this.m_creditsText2;
    this.m_currentText.Text = this.GetNextCreditsChunk();
    this.DropText();
  }

  private void DropText()
  {
    UberText uberText = this.m_creditsText1;
    if ((UnityEngine.Object) this.m_currentText == (UnityEngine.Object) this.m_creditsText1)
      uberText = this.m_creditsText2;
    float z = 1.8649f;
    TransformUtil.SetPoint(this.m_currentText.gameObject, Anchor.FRONT, uberText.gameObject, Anchor.BACK, new Vector3(0.0f, 0.0f, z));
  }

  private string GetNextCreditsChunk()
  {
    string str = string.Empty;
    int currentLine = this.m_currentLine;
    int num = 70;
    for (int index = 0; index < num; ++index)
    {
      if (this.m_creditLines.Length < index + currentLine + 1)
      {
        this.m_creditsDone = true;
        this.StartEndCreditsTimer();
        return str;
      }
      string creditLine = this.m_creditLines[index + currentLine];
      if (creditLine.Length > 38)
      {
        num -= Mathf.CeilToInt((float) (creditLine.Length / 38));
        if (index > num && index > 60)
          break;
      }
      str = str + creditLine + Environment.NewLine;
      ++this.m_currentLine;
    }
    return str;
  }

  private void ActorLoadedCallback(string name, GameObject go, object callbackData)
  {
    this.m_fakeCards.Add(go.GetComponent<Actor>());
  }

  private void Start()
  {
    Navigation.Push(new Navigation.NavigateBackHandler(this.EndCredits));
    this.StartCoroutine(this.NotifySceneLoadedWhenReady());
  }

  [DebuggerHidden]
  private IEnumerator NotifySceneLoadedWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CreditsDisplay.\u003CNotifySceneLoadedWhenReady\u003Ec__Iterator69()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnBoxOpened(object userData)
  {
    Box.Get().RemoveTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxOpened));
    if (!this.m_creditsTextLoadSucceeded)
    {
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
    }
    else
    {
      MusicManager.Get().StartPlaylist(MusicPlaylistType.UI_Credits);
      this.StartCoroutine("StartCredits");
    }
  }

  [DebuggerHidden]
  private IEnumerator StartCredits()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CreditsDisplay.\u003CStartCredits\u003Ec__Iterator6A()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void NewCard()
  {
    this.StartCoroutine("ShowNewCard");
  }

  [DebuggerHidden]
  private IEnumerator ShowNewCard()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CreditsDisplay.\u003CShowNewCard\u003Ec__Iterator6B()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void Update()
  {
    Network.Get().ProcessNetwork();
    if (!this.started)
      return;
    this.m_creditsRoot.transform.localPosition += new Vector3(0.0f, 0.0f, 2.5f * Time.deltaTime);
    if (this.m_creditsDone || (UnityEngine.Object) this.m_currentText == (UnityEngine.Object) null || (double) this.GetTopOfCurrentCredits() <= (double) this.m_flopPoint.position.z)
      return;
    this.FlopCredits();
  }

  private float GetTopOfCurrentCredits()
  {
    Bounds worldSpaceBounds = this.m_currentText.GetTextWorldSpaceBounds();
    return worldSpaceBounds.center.z + worldSpaceBounds.extents.z;
  }

  private void OnFullDefLoaded(string cardID, FullDef def, object userData)
  {
    this.m_creditsDefs.Add(def);
  }

  private void OnDonePressed(UIEvent e)
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      Box.Get().m_letterboxingContainer.SetActive(true);
    Navigation.GoBack();
  }

  private void OnYearPressed(UIEvent e)
  {
    this.StopCoroutine("StartCredits");
    this.StopCoroutine("ShowNewCard");
    if ((UnityEngine.Object) this.m_shownCreditsCard != (UnityEngine.Object) null)
    {
      this.m_shownCreditsCard.ActivateSpellBirthState(SpellType.BURN);
      SoundManager.Get().LoadAndPlay("credits_card_embers_" + UnityEngine.Random.Range(1, 3).ToString());
      this.m_shownCreditsCard = (Actor) null;
    }
    this.m_displayingLatestYear = !this.m_displayingLatestYear;
    this.StartCoroutine(this.ResetCredits());
  }

  [DebuggerHidden]
  private IEnumerator ResetCredits()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CreditsDisplay.\u003CResetCredits\u003Ec__Iterator6C()
    {
      \u003C\u003Ef__this = this
    };
  }

  private bool EndCredits()
  {
    iTween.FadeTo(this.m_creditsText1.gameObject, 0.0f, 0.1f);
    iTween.FadeTo(this.m_creditsText2.gameObject, 0.0f, 0.1f);
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
    return true;
  }

  private void StartEndCreditsTimer()
  {
    this.StartCoroutine(this.EndCreditsTimer());
  }

  [DebuggerHidden]
  private IEnumerator EndCreditsTimer()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CreditsDisplay.\u003CEndCreditsTimer\u003Ec__Iterator6D timerCIterator6D = new CreditsDisplay.\u003CEndCreditsTimer\u003Ec__Iterator6D();
    return (IEnumerator) timerCIterator6D;
  }
}
