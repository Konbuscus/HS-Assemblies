﻿// Decompiled with JetBrains decompiler
// Type: GameColors
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GameColors
{
  public static readonly Color BLIZZARD_BLUE = new Color(0.3686275f, 0.7921569f, 0.9411765f, 1f);
  public static readonly Color PLAYER_NAME = GameColors.BLIZZARD_BLUE;
  public static readonly Color PLAYER_NAME_ONLINE = GameColors.PLAYER_NAME;
  public static readonly Color PLAYER_NAME_OFFLINE = new Color(0.6f, 0.6f, 0.6f, 1f);
  public static readonly Color PLAYER_NUMBER = new Color(0.6313726f, 0.6313726f, 0.6313726f, 1f);
  public static readonly Color SERVER_SHUTDOWN = new Color(0.9647059f, 0.1215686f, 0.1215686f, 1f);
  public const string BLIZZARD_BLUE_STR = "5ecaf0ff";
  public const string PLAYER_NAME_STR = "5ecaf0ff";
  public const string PLAYER_NAME_ONLINE_STR = "5ecaf0ff";
  public const string PLAYER_NAME_OFFLINE_STR = "999999ff";
  public const string PLAYER_NUMBER_STR = "a1a1a1ff";
  public const string GOLD_STR = "ffd200";
  public const string ORANGE_STR = "ff9c00";
  public const string SERVER_SHUTDOWN_STR = "f61f1fff";
}
