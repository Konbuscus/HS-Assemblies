﻿// Decompiled with JetBrains decompiler
// Type: EntityBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;

public abstract class EntityBase
{
  protected TagMap m_tags = new TagMap();

  public bool HasTag(GAME_TAG tag)
  {
    return this.GetTag(tag) > 0;
  }

  public TagMap GetTags()
  {
    return this.m_tags;
  }

  public int GetTag(int tag)
  {
    return this.m_tags.GetTag(tag);
  }

  public int GetTag(GAME_TAG enumTag)
  {
    return this.m_tags.GetTag((int) enumTag);
  }

  public TagEnum GetTag<TagEnum>(GAME_TAG enumTag)
  {
    return (TagEnum) Enum.ToObject(typeof (TagEnum), this.GetTag(enumTag));
  }

  public void SetTag(int tag, int tagValue)
  {
    this.m_tags.SetTag(tag, tagValue);
  }

  public void SetTag(GAME_TAG tag, int tagValue)
  {
    this.SetTag((int) tag, tagValue);
  }

  public void SetTag<TagEnum>(GAME_TAG tag, TagEnum tagValue)
  {
    this.SetTag((int) tag, Convert.ToInt32((object) tagValue));
  }

  public void SetTags(Map<GAME_TAG, int> tagMap)
  {
    this.m_tags.SetTags(tagMap);
  }

  public void SetTags(List<Network.Entity.Tag> tags)
  {
    this.m_tags.SetTags(tags);
  }

  public void ReplaceTags(TagMap tags)
  {
    this.m_tags.Replace(tags);
  }

  public void ReplaceTags(List<Network.Entity.Tag> tags)
  {
    this.m_tags.Replace(tags);
  }

  public bool HasReferencedTag(GAME_TAG enumTag)
  {
    return this.GetReferencedTag(enumTag) > 0;
  }

  public bool HasReferencedTag(int tag)
  {
    return this.GetReferencedTag(tag) > 0;
  }

  public int GetReferencedTag(GAME_TAG enumTag)
  {
    return this.GetReferencedTag((int) enumTag);
  }

  public abstract int GetReferencedTag(int tag);

  public bool HasCharge()
  {
    return this.HasTag(GAME_TAG.CHARGE);
  }

  public bool ReferencesCharge()
  {
    return this.HasReferencedTag(GAME_TAG.CHARGE);
  }

  public bool HasBattlecry()
  {
    return this.HasTag(GAME_TAG.BATTLECRY);
  }

  public bool ReferencesBattlecry()
  {
    return this.HasReferencedTag(GAME_TAG.BATTLECRY);
  }

  public bool CanBeTargetedBySpells()
  {
    if (!this.HasTag(GAME_TAG.CANT_BE_TARGETED_BY_SPELLS))
      return !this.HasTag(GAME_TAG.UNTOUCHABLE);
    return false;
  }

  public bool CanBeTargetedByHeroPowers()
  {
    if (!this.HasTag(GAME_TAG.CANT_BE_TARGETED_BY_HERO_POWERS))
      return !this.HasTag(GAME_TAG.UNTOUCHABLE);
    return false;
  }

  public bool CanBeTargetedByBattlecries()
  {
    if (!this.HasTag(GAME_TAG.CANT_BE_TARGETED_BY_BATTLECRIES))
      return !this.HasTag(GAME_TAG.UNTOUCHABLE);
    return false;
  }

  public bool HasTriggerVisual()
  {
    return this.HasTag(GAME_TAG.TRIGGER_VISUAL);
  }

  public bool HasInspire()
  {
    return this.HasTag(GAME_TAG.INSPIRE);
  }

  public bool IsImmune()
  {
    if (!this.HasTag(GAME_TAG.IMMUNE))
      return this.HasTag(GAME_TAG.UNTOUCHABLE);
    return true;
  }

  public bool DontShowImmune()
  {
    return this.HasTag(GAME_TAG.DONT_SHOW_IMMUNE);
  }

  public bool IsPoisonous()
  {
    return this.HasTag(GAME_TAG.POISONOUS);
  }

  public bool HasAura()
  {
    return this.HasTag(GAME_TAG.AURA);
  }

  public bool HasHealthMin()
  {
    return this.GetTag(GAME_TAG.HEALTH_MINIMUM) > 0;
  }

  public bool ReferencesImmune()
  {
    return this.HasReferencedTag(GAME_TAG.IMMUNE);
  }

  public bool IsEnraged()
  {
    if (this.HasTag(GAME_TAG.ENRAGED))
      return this.GetDamage() > 0;
    return false;
  }

  public bool IsFreeze()
  {
    return this.HasTag(GAME_TAG.FREEZE);
  }

  public int GetDamage()
  {
    return this.GetTag(GAME_TAG.DAMAGE);
  }

  public bool IsFrozen()
  {
    return this.HasTag(GAME_TAG.FROZEN);
  }

  public bool IsAsleep()
  {
    if (this.GetNumTurnsInPlay() == 0 && !this.HasCharge())
      return !this.ReferencesAutoAttack();
    return false;
  }

  public bool IsStealthed()
  {
    return this.HasTag(GAME_TAG.STEALTH);
  }

  public bool ReferencesStealth()
  {
    return this.HasReferencedTag(GAME_TAG.STEALTH);
  }

  public bool HasTaunt()
  {
    return this.HasTag(GAME_TAG.TAUNT);
  }

  public bool ReferencesTaunt()
  {
    return this.HasReferencedTag(GAME_TAG.TAUNT);
  }

  public bool HasDivineShield()
  {
    return this.HasTag(GAME_TAG.DIVINE_SHIELD);
  }

  public bool ReferencesDivineShield()
  {
    return this.HasReferencedTag(GAME_TAG.DIVINE_SHIELD);
  }

  public bool ReferencesAutoAttack()
  {
    return this.HasReferencedTag(GAME_TAG.AUTOATTACK);
  }

  public bool IsHero()
  {
    return this.GetTag(GAME_TAG.CARDTYPE) == 3;
  }

  public bool IsHeroPower()
  {
    return this.GetTag(GAME_TAG.CARDTYPE) == 10;
  }

  public bool IsMinion()
  {
    return this.GetTag(GAME_TAG.CARDTYPE) == 4;
  }

  public bool IsSpell()
  {
    return this.GetTag(GAME_TAG.CARDTYPE) == 5;
  }

  public bool IsWeapon()
  {
    return this.GetTag(GAME_TAG.CARDTYPE) == 7;
  }

  public bool IsElite()
  {
    return this.GetTag(GAME_TAG.ELITE) > 0;
  }

  public TAG_CARD_SET GetCardSet()
  {
    return (TAG_CARD_SET) this.GetTag(GAME_TAG.CARD_SET);
  }

  public TAG_CARDTYPE GetCardType()
  {
    return (TAG_CARDTYPE) this.GetTag(GAME_TAG.CARDTYPE);
  }

  public bool IsGame()
  {
    return this.GetTag(GAME_TAG.CARDTYPE) == 1;
  }

  public bool IsPlayer()
  {
    return this.GetTag(GAME_TAG.CARDTYPE) == 2;
  }

  public bool IsExhausted()
  {
    return this.HasTag(GAME_TAG.EXHAUSTED);
  }

  public bool IsAttached()
  {
    return this.HasTag(GAME_TAG.ATTACHED);
  }

  public bool IsRecentlyArrived()
  {
    return this.HasTag(GAME_TAG.RECENTLY_ARRIVED);
  }

  public bool IsObfuscated()
  {
    return this.HasTag(GAME_TAG.OBFUSCATED);
  }

  public bool IsSecret()
  {
    return this.HasTag(GAME_TAG.SECRET);
  }

  public bool ReferencesSecret()
  {
    return this.HasReferencedTag(GAME_TAG.SECRET);
  }

  public bool CanAttack()
  {
    if (!this.HasTag(GAME_TAG.CANT_ATTACK))
      return !this.HasTag(GAME_TAG.UNTOUCHABLE);
    return false;
  }

  public bool CannotAttackHeroes()
  {
    return this.HasTag(GAME_TAG.CANNOT_ATTACK_HEROES);
  }

  public bool CanBeAttacked()
  {
    if (!this.HasTag(GAME_TAG.CANT_BE_ATTACKED))
      return !this.HasTag(GAME_TAG.UNTOUCHABLE);
    return false;
  }

  public bool CanBeTargetedByOpponents()
  {
    if (!this.HasTag(GAME_TAG.CANT_BE_TARGETED_BY_OPPONENTS))
      return !this.HasTag(GAME_TAG.UNTOUCHABLE);
    return false;
  }

  public bool IsMagnet()
  {
    return this.HasTag(GAME_TAG.MAGNET);
  }

  public int GetNumTurnsInPlay()
  {
    return this.GetTag(GAME_TAG.NUM_TURNS_IN_PLAY);
  }

  public int GetNumAttacksThisTurn()
  {
    return this.GetTag(GAME_TAG.NUM_ATTACKS_THIS_TURN);
  }

  public int GetSpellPower()
  {
    return this.GetTag(GAME_TAG.SPELLPOWER);
  }

  public bool HasSpellPower()
  {
    return this.HasTag(GAME_TAG.SPELLPOWER);
  }

  public bool HasHeroPowerDamage()
  {
    return this.HasTag(GAME_TAG.HEROPOWER_DAMAGE);
  }

  public bool IsAffectedBySpellPower()
  {
    return this.HasTag(GAME_TAG.AFFECTED_BY_SPELL_POWER);
  }

  public bool HasSpellPowerDouble()
  {
    return this.HasTag(GAME_TAG.SPELLPOWER_DOUBLE);
  }

  public bool ReferencesSpellPower()
  {
    return this.HasReferencedTag(GAME_TAG.SPELLPOWER);
  }

  public int GetCost()
  {
    return this.GetTag(GAME_TAG.COST);
  }

  public int GetATK()
  {
    return this.GetTag(GAME_TAG.ATK);
  }

  public int GetHealth()
  {
    return this.GetTag(GAME_TAG.HEALTH);
  }

  public int GetDurability()
  {
    return this.GetTag(GAME_TAG.DURABILITY);
  }

  public int GetArmor()
  {
    return this.GetTag(GAME_TAG.ARMOR);
  }

  public int GetAttached()
  {
    return this.GetTag(GAME_TAG.ATTACHED);
  }

  public TAG_ZONE GetZone()
  {
    return (TAG_ZONE) this.GetTag(GAME_TAG.ZONE);
  }

  public int GetZonePosition()
  {
    return this.GetTag(GAME_TAG.ZONE_POSITION);
  }

  public int GetCreatorId()
  {
    return this.GetTag(GAME_TAG.CREATOR);
  }

  public int GetControllerId()
  {
    return this.GetTag(GAME_TAG.CONTROLLER);
  }

  public int GetFatigue()
  {
    return this.GetTag(GAME_TAG.FATIGUE);
  }

  public int GetWindfury()
  {
    return this.GetTag(GAME_TAG.WINDFURY);
  }

  public bool HasWindfury()
  {
    return this.GetTag(GAME_TAG.WINDFURY) > 0;
  }

  public bool ReferencesWindfury()
  {
    return this.HasReferencedTag(GAME_TAG.WINDFURY);
  }

  public int GetExtraAttacksThisTurn()
  {
    return this.GetTag(GAME_TAG.EXTRA_ATTACKS_THIS_TURN);
  }

  public bool HasCombo()
  {
    return this.HasTag(GAME_TAG.COMBO);
  }

  public bool HasOverload()
  {
    return this.HasTag(GAME_TAG.OVERLOAD);
  }

  public bool HasDeathrattle()
  {
    return this.HasTag(GAME_TAG.DEATHRATTLE);
  }

  public bool ReferencesDeathrattle()
  {
    return this.HasReferencedTag(GAME_TAG.DEATHRATTLE);
  }

  public bool IsSilenced()
  {
    return this.HasTag(GAME_TAG.SILENCED);
  }

  public int GetEntityId()
  {
    return this.GetTag(GAME_TAG.ENTITY_ID);
  }

  public bool IsCharacter()
  {
    if (!this.IsHero())
      return this.IsMinion();
    return true;
  }

  public bool IsItem()
  {
    return this.GetTag(GAME_TAG.CARDTYPE) == 8;
  }

  public bool IsToken()
  {
    return this.GetTag(GAME_TAG.CARDTYPE) == 9;
  }

  public bool HasCustomKeywordEffect()
  {
    return this.HasTag(GAME_TAG.CUSTOM_KEYWORD_EFFECT);
  }

  public int GetDisplayedCreatorId()
  {
    return this.GetTag(GAME_TAG.DISPLAYED_CREATOR);
  }

  public bool IsBasicCardUnlock()
  {
    if (this.IsHero())
      return false;
    return this.GetTag<TAG_CARD_SET>(GAME_TAG.CARD_SET) == TAG_CARD_SET.CORE;
  }

  public virtual TAG_CLASS GetClass()
  {
    IEnumerable<TAG_CLASS> classes = this.GetClasses((Comparison<TAG_CLASS>) null);
    if (classes.Count<TAG_CLASS>() == 0)
      return TAG_CLASS.INVALID;
    if (classes.Count<TAG_CLASS>() == 1)
      return classes.First<TAG_CLASS>();
    return TAG_CLASS.NEUTRAL;
  }

  public virtual IEnumerable<TAG_CLASS> GetClasses(Comparison<TAG_CLASS> classSorter = null)
  {
    List<TAG_CLASS> tagClassList = new List<TAG_CLASS>();
    uint tag1 = (uint) this.GetTag(GAME_TAG.MULTIPLE_CLASSES);
    if ((int) tag1 == 0)
    {
      TAG_CLASS tag2 = this.GetTag<TAG_CLASS>(GAME_TAG.CLASS);
      tagClassList.Add(tag2);
    }
    else
    {
      int num = 1;
      while ((int) tag1 != 0)
      {
        TAG_CLASS outVal;
        if (((int) tag1 & 1) == 1 && EnumUtils.TryCast<TAG_CLASS>((object) num, out outVal))
          tagClassList.Add(outVal);
        tag1 >>= 1;
        ++num;
      }
    }
    if (classSorter != null)
      tagClassList.Sort(classSorter);
    return (IEnumerable<TAG_CLASS>) tagClassList;
  }

  public bool IsMultiClass()
  {
    return this.GetClasses((Comparison<TAG_CLASS>) null).Count<TAG_CLASS>() > 1;
  }

  public bool IsAllowedInClass(TAG_CLASS classID)
  {
    return this.GetClasses((Comparison<TAG_CLASS>) null).Contains<TAG_CLASS>(classID);
  }

  public TAG_MULTI_CLASS_GROUP GetMultiClassGroup()
  {
    return (TAG_MULTI_CLASS_GROUP) this.GetTag(GAME_TAG.MULTI_CLASS_GROUP);
  }
}
