﻿// Decompiled with JetBrains decompiler
// Type: AdventureChooserTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class AdventureChooserTray : MonoBehaviour
{
  [SerializeField]
  private float m_ButtonOffset = -2.5f;
  private List<AdventureChooserButton> m_AdventureButtons = new List<AdventureChooserButton>();
  private Map<AdventureDbId, Map<AdventureModeDbId, AdventureChooserDescription>> m_Descriptions = new Map<AdventureDbId, Map<AdventureModeDbId, AdventureChooserDescription>>();
  private const string s_DefaultPortraitMaterialTextureName = "_MainTex";
  private const int s_DefaultPortraitMaterialIndex = 0;
  [CustomEditField(Sections = "Sub Scene")]
  [SerializeField]
  public AdventureSubScene m_ParentSubScene;
  [CustomEditField(Sections = "Description")]
  public UberText m_DescriptionTitleObject;
  [CustomEditField(Sections = "Description")]
  public GameObject m_DescriptionContainer;
  [SerializeField]
  [CustomEditField(Sections = "Choose Frame")]
  public PlayButton m_ChooseButton;
  [SerializeField]
  [CustomEditField(Sections = "Choose Frame")]
  public UIBButton m_BackButton;
  [CustomEditField(Sections = "Choose Frame")]
  [SerializeField]
  public GameObject m_ChooseElementsContainer;
  [SerializeField]
  [CustomEditField(Sections = "Choose Frame", T = EditType.GAME_OBJECT)]
  public string m_DefaultChooserButtonPrefab;
  [CustomEditField(Sections = "Choose Frame", T = EditType.GAME_OBJECT)]
  [SerializeField]
  public string m_DefaultChooserSubButtonPrefab;
  [CustomEditField(Sections = "Choose Frame")]
  public UIBScrollable m_ChooseFrameScroller;
  [CustomEditField(Sections = "Behavior Settings")]
  public bool m_OnlyOneExpands;
  private AdventureChooserSubButton m_SelectedSubButton;
  private AdventureChooserDescription m_CurrentChooserDescription;
  private bool m_AttemptedLoad;
  private bool m_isStarted;

  [CustomEditField(Sections = "Behavior Settings")]
  public float ButtonOffset
  {
    get
    {
      return this.m_ButtonOffset;
    }
    set
    {
      this.m_ButtonOffset = value;
      this.OnButtonVisualUpdated();
    }
  }

  private void Awake()
  {
    this.m_ChooseButton.Disable();
    this.m_BackButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OnBackButton()));
    this.m_ChooseButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ChangeSubScene()));
    AdventureConfig.Get().AddSelectedModeChangeListener(new AdventureConfig.SelectedModeChange(this.OnSelectedModeChange));
    AdventureProgressMgr.Get().RegisterProgressUpdatedListener(new AdventureProgressMgr.AdventureProgressUpdatedCallback(this.OnAdventureProgressUpdated));
    if ((Object) this.m_ChooseElementsContainer == (Object) null)
    {
      UnityEngine.Debug.LogError((object) "m_ChooseElementsContainer cannot be null. Unable to create button.", (Object) this);
    }
    else
    {
      using (List<AdventureDef>.Enumerator enumerator = AdventureScene.Get().GetSortedAdventureDefs().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AdventureDef current = enumerator.Current;
          if (this.ShowAdventureOnTray(current.GetAdventureId()))
            this.CreateAdventureChooserButton(current);
        }
      }
      if ((Object) this.m_ParentSubScene != (Object) null)
        this.m_ParentSubScene.SetIsLoaded(true);
      this.OnButtonVisualUpdated();
      Box.Get().AddTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    }
  }

  private void Start()
  {
    Navigation.PushUnique(new Navigation.NavigateBackHandler(AdventureChooserTray.OnNavigateBack));
    this.m_isStarted = true;
  }

  private void OnDestroy()
  {
    if ((Object) AdventureConfig.Get() != (Object) null)
      AdventureConfig.Get().RemoveSelectedModeChangeListener(new AdventureConfig.SelectedModeChange(this.OnSelectedModeChange));
    if ((Object) Box.Get() != (Object) null)
      Box.Get().RemoveTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    if (AdventureProgressMgr.Get() != null)
      AdventureProgressMgr.Get().RemoveProgressUpdatedListener(new AdventureProgressMgr.AdventureProgressUpdatedCallback(this.OnAdventureProgressUpdated));
    this.CancelInvoke("ShowDisabledAdventureModeRequirementsWarning");
  }

  private void OnButtonVisualUpdated()
  {
    float num = 0.0f;
    AdventureChooserButton[] array = this.m_AdventureButtons.ToArray();
    for (int index = 0; index < array.Length; ++index)
    {
      TransformUtil.SetLocalPosZ((Component) array[index].transform, -num);
      num += array[index].GetFullButtonHeight() + this.m_ButtonOffset;
    }
  }

  private void OnAdventureButtonToggled(AdventureChooserButton btn, bool toggled, int index)
  {
    btn.SetSelectSubButtonOnToggle(this.m_OnlyOneExpands);
    if (this.m_OnlyOneExpands)
    {
      if (!toggled)
        return;
      this.ToggleScrollable(false);
      AdventureChooserButton[] array = this.m_AdventureButtons.ToArray();
      for (int index1 = 0; index1 < array.Length; ++index1)
      {
        if (index1 != index)
          array[index1].Toggle = false;
      }
    }
    else
    {
      if (!((Object) this.m_SelectedSubButton != (Object) null))
        return;
      btn = this.m_AdventureButtons[index];
      if (!btn.ContainsSubButton(this.m_SelectedSubButton))
        return;
      this.m_SelectedSubButton.SetHighlight(toggled);
      if (!toggled)
      {
        this.m_ChooseButton.Disable();
      }
      else
      {
        if (this.m_AttemptedLoad)
          return;
        this.m_ChooseButton.Enable();
      }
    }
  }

  private bool ShowAdventureOnTray(AdventureDbId adventureID)
  {
    return (!GameUtils.IsAdventureRotated(adventureID) || AdventureProgressMgr.Get().OwnsOneOrMoreAdventureWings(adventureID)) && (AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES) || AdventureProgressMgr.Get().OwnsOneOrMoreAdventureWings(adventureID) || adventureID == AdventureDbId.PRACTICE) && AdventureScene.Get().IsAdventureOpen(adventureID);
  }

  private void CreateAdventureChooserButton(AdventureDef advDef)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureChooserTray.\u003CCreateAdventureChooserButton\u003Ec__AnonStorey34F buttonCAnonStorey34F = new AdventureChooserTray.\u003CCreateAdventureChooserButton\u003Ec__AnonStorey34F();
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.\u003C\u003Ef__this = this;
    string chooserButtonPrefab = this.m_DefaultChooserButtonPrefab;
    if (!string.IsNullOrEmpty(advDef.m_ChooserButtonPrefab))
      chooserButtonPrefab = advDef.m_ChooserButtonPrefab;
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.newbutton = GameUtils.LoadGameObjectWithComponent<AdventureChooserButton>(chooserButtonPrefab);
    // ISSUE: reference to a compiler-generated field
    if ((Object) buttonCAnonStorey34F.newbutton == (Object) null)
      return;
    // ISSUE: reference to a compiler-generated field
    GameUtils.SetParent((Component) buttonCAnonStorey34F.newbutton, this.m_ChooseElementsContainer, false);
    AdventureDbId adventureId = advDef.GetAdventureId();
    // ISSUE: reference to a compiler-generated field
    GameUtils.SetAutomationName(buttonCAnonStorey34F.newbutton.gameObject, (object) adventureId);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.newbutton.SetAdventure(adventureId);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.newbutton.SetButtonText(advDef.GetAdventureName());
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.newbutton.SetPortraitTexture(advDef.m_Texture);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.newbutton.SetPortraitTiling(advDef.m_TextureTiling);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.newbutton.SetPortraitOffset(advDef.m_TextureOffset);
    AdventureDbId selectedAdventure = AdventureConfig.Get().GetSelectedAdventure();
    AdventureModeDbId chooserAdventureMode = AdventureConfig.Get().GetClientChooserAdventureMode(adventureId);
    if (selectedAdventure == adventureId)
    {
      // ISSUE: reference to a compiler-generated field
      buttonCAnonStorey34F.newbutton.Toggle = true;
    }
    List<AdventureSubDef> sortedSubDefs = advDef.GetSortedSubDefs();
    string chooserSubButtonPrefab = this.m_DefaultChooserSubButtonPrefab;
    if (!string.IsNullOrEmpty(advDef.m_ChooserSubButtonPrefab))
      chooserSubButtonPrefab = advDef.m_ChooserSubButtonPrefab;
    using (List<AdventureSubDef>.Enumerator enumerator = sortedSubDefs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureSubDef current = enumerator.Current;
        AdventureModeDbId adventureModeId = current.GetAdventureModeId();
        // ISSUE: reference to a compiler-generated field
        AdventureChooserSubButton subButton = buttonCAnonStorey34F.newbutton.CreateSubButton(adventureModeId, current, chooserSubButtonPrefab, chooserAdventureMode == adventureModeId);
        if (!((Object) subButton == (Object) null))
        {
          // ISSUE: reference to a compiler-generated field
          bool active = buttonCAnonStorey34F.newbutton.Toggle && chooserAdventureMode == adventureModeId;
          if (active)
          {
            subButton.SetHighlight(true);
            this.UpdateChooseButton(adventureId, adventureModeId);
            this.SetTitleText(adventureId, adventureModeId);
          }
          else if (AdventureConfig.Get().IsFeaturedMode(adventureId, adventureModeId))
            subButton.SetNewGlow(true);
          bool flag = AdventureConfig.Get().CanPlayMode(adventureId, adventureModeId);
          subButton.SetDesaturate(!flag);
          if (selectedAdventure == AdventureDbId.PRACTICE && adventureModeId == AdventureModeDbId.EXPERT && !flag)
            subButton.SetContrast(0.3f);
          this.CreateAdventureChooserDescriptionFromPrefab(adventureId, current, active);
        }
      }
    }
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.newbutton.AddVisualUpdatedListener(new AdventureChooserButton.VisualUpdated(this.OnButtonVisualUpdated));
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.index = this.m_AdventureButtons.Count;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    buttonCAnonStorey34F.newbutton.AddToggleListener(new AdventureChooserButton.Toggled(buttonCAnonStorey34F.\u003C\u003Em__6));
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.newbutton.AddModeSelectionListener(new AdventureChooserButton.ModeSelection(this.ButtonModeSelected));
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey34F.newbutton.AddExpandedListener(new AdventureChooserButton.Expanded(this.ButtonExpanded));
    // ISSUE: reference to a compiler-generated field
    this.m_AdventureButtons.Add(buttonCAnonStorey34F.newbutton);
  }

  private void CreateAdventureChooserDescriptionFromPrefab(AdventureDbId adventureId, AdventureSubDef subDef, bool active)
  {
    if (string.IsNullOrEmpty((string) ((MobileOverrideValue<string>) subDef.m_ChooserDescriptionPrefab)))
      return;
    Map<AdventureModeDbId, AdventureChooserDescription> map;
    if (!this.m_Descriptions.TryGetValue(adventureId, out map))
    {
      map = new Map<AdventureModeDbId, AdventureChooserDescription>();
      this.m_Descriptions[adventureId] = map;
    }
    string description = subDef.GetDescription();
    string requiredText = (string) null;
    if (!AdventureConfig.Get().CanPlayMode(adventureId, subDef.GetAdventureModeId()))
      requiredText = subDef.GetRequirementsDescription();
    AdventureChooserDescription chooserDescription = GameUtils.LoadGameObjectWithComponent<AdventureChooserDescription>((string) ((MobileOverrideValue<string>) subDef.m_ChooserDescriptionPrefab));
    if ((Object) chooserDescription == (Object) null)
      return;
    GameUtils.SetParent((Component) chooserDescription, this.m_DescriptionContainer, false);
    chooserDescription.SetText(requiredText, description);
    chooserDescription.gameObject.SetActive(active);
    map[subDef.GetAdventureModeId()] = chooserDescription;
    if (!active)
      return;
    this.m_CurrentChooserDescription = chooserDescription;
  }

  private AdventureChooserDescription GetAdventureChooserDescription(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    Map<AdventureModeDbId, AdventureChooserDescription> map;
    if (!this.m_Descriptions.TryGetValue(adventureId, out map))
      return (AdventureChooserDescription) null;
    AdventureChooserDescription chooserDescription;
    if (!map.TryGetValue(modeId, out chooserDescription))
      return (AdventureChooserDescription) null;
    return chooserDescription;
  }

  private void ButtonModeSelected(AdventureChooserSubButton btn)
  {
    using (List<AdventureChooserButton>.Enumerator enumerator = this.m_AdventureButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.DisableSubButtonHighlights();
    }
    this.m_SelectedSubButton = btn;
    if (AdventureConfig.Get().MarkFeaturedMode(btn.GetAdventure(), btn.GetMode()))
      btn.SetNewGlow(false);
    AdventureConfig.Get().SetSelectedAdventureMode(btn.GetAdventure(), btn.GetMode());
    this.SetTitleText(btn.GetAdventure(), btn.GetMode());
  }

  private void ButtonExpanded(AdventureChooserButton button, bool expand)
  {
    if (!expand)
      return;
    this.ToggleScrollable(true);
    AdventureConfig adventureConfig = AdventureConfig.Get();
    foreach (AdventureChooserSubButton subButton in button.GetSubButtons())
    {
      if (adventureConfig.IsFeaturedMode(button.GetAdventure(), subButton.GetMode()))
        subButton.Flash();
    }
  }

  private void SetTitleText(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    this.m_DescriptionTitleObject.Text = (string) GameUtils.GetAdventureDataRecord((int) adventureId, (int) modeId).Name;
  }

  private void OnSelectedModeChange(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    AdventureChooserDescription chooserDescription = this.GetAdventureChooserDescription(adventureId, modeId);
    if ((Object) this.m_CurrentChooserDescription != (Object) null)
      this.m_CurrentChooserDescription.gameObject.SetActive(false);
    this.m_CurrentChooserDescription = chooserDescription;
    if ((Object) this.m_CurrentChooserDescription != (Object) null)
      this.m_CurrentChooserDescription.gameObject.SetActive(true);
    this.UpdateChooseButton(adventureId, modeId);
    if (this.m_ChooseButton.IsEnabled())
    {
      PlayMakerFSM component = this.m_ChooseButton.GetComponent<PlayMakerFSM>();
      if ((Object) component != (Object) null)
        component.SendEvent("Burst");
    }
    if (AdventureConfig.Get().CanPlayMode(adventureId, modeId))
      return;
    if (!this.m_isStarted)
      this.Invoke("ShowDisabledAdventureModeRequirementsWarning", 0.0f);
    else
      this.ShowDisabledAdventureModeRequirementsWarning();
  }

  private void ShowDisabledAdventureModeRequirementsWarning()
  {
    this.CancelInvoke("ShowDisabledAdventureModeRequirementsWarning");
    if (!this.m_isStarted || SceneMgr.Get().GetMode() != SceneMgr.Mode.ADVENTURE || (!((Object) this.m_ChooseButton != (Object) null) || this.m_ChooseButton.IsEnabled()))
      return;
    AdventureDbId selectedAdventure = AdventureConfig.Get().GetSelectedAdventure();
    AdventureModeDbId selectedMode = AdventureConfig.Get().GetSelectedMode();
    if (AdventureConfig.Get().CanPlayMode(selectedAdventure, selectedMode))
      return;
    AdventureDataDbfRecord adventureDataRecord = GameUtils.GetAdventureDataRecord((int) selectedAdventure, (int) selectedMode);
    string requirementsDescription = (string) adventureDataRecord.RequirementsDescription;
    if (string.IsNullOrEmpty(requirementsDescription))
      return;
    Error.AddWarning((string) adventureDataRecord.Name, requirementsDescription);
  }

  private void UpdateChooseButton(AdventureDbId adventureId, AdventureModeDbId modeId)
  {
    if (!this.m_AttemptedLoad && AdventureConfig.Get().CanPlayMode(adventureId, modeId))
    {
      this.m_ChooseButton.SetText(GameStrings.Get("GLOBAL_ADVENTURE_CHOOSE_BUTTON_TEXT"));
      if (this.m_ChooseButton.IsEnabled())
        return;
      this.m_ChooseButton.Enable();
    }
    else
    {
      this.m_ChooseButton.SetText(GameStrings.Get("GLUE_QUEST_LOG_CLASS_LOCKED"));
      this.m_ChooseButton.Disable();
    }
  }

  private void OnBoxTransitionFinished(object userData)
  {
    if (!this.m_isStarted || SceneMgr.Get().GetMode() != SceneMgr.Mode.ADVENTURE)
      return;
    if (this.m_ChooseButton.IsEnabled())
    {
      PlayMakerFSM component = this.m_ChooseButton.GetComponent<PlayMakerFSM>();
      if (!((Object) component != (Object) null))
        return;
      component.SendEvent("Burst");
    }
    else
      this.ShowDisabledAdventureModeRequirementsWarning();
  }

  private void ChangeSubScene()
  {
    this.m_AttemptedLoad = true;
    this.m_ChooseButton.SetText(GameStrings.Get("GLUE_LOADING"));
    this.m_ChooseButton.Disable();
    this.m_BackButton.SetEnabled(false);
    this.StartCoroutine(this.WaitThenChangeSubScene());
  }

  [DebuggerHidden]
  private IEnumerator WaitThenChangeSubScene()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureChooserTray.\u003CWaitThenChangeSubScene\u003Ec__Iterator0 subSceneCIterator0 = new AdventureChooserTray.\u003CWaitThenChangeSubScene\u003Ec__Iterator0();
    return (IEnumerator) subSceneCIterator0;
  }

  private void OnBackButton()
  {
    Navigation.GoBack();
  }

  private static bool OnNavigateBack()
  {
    AdventureChooserTray.BackToMainMenu();
    return true;
  }

  private static void BackToMainMenu()
  {
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
  }

  private void ToggleScrollable(bool enable)
  {
    if (!((Object) this.m_ChooseFrameScroller != (Object) null) || this.m_ChooseFrameScroller.enabled == enable)
      return;
    Log.JMac.Print("AdventureChooserTray.ToggleScrollable: " + (object) enable);
    this.m_ChooseFrameScroller.enabled = enable;
  }

  private void OnAdventureProgressUpdated(bool isStartupAction, AdventureMission.WingProgress oldProgress, AdventureMission.WingProgress newProgress, object userData)
  {
    if (newProgress == null || oldProgress != null && oldProgress.IsOwned() || (!newProgress.IsOwned() || GameDbf.Wing.GetRecord(newProgress.Wing) == null))
      return;
    using (List<AdventureChooserButton>.Enumerator enumerator = this.m_AdventureButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        foreach (AdventureChooserSubButton subButton in enumerator.Current.GetSubButtons())
          subButton.ShowRemainingProgressCount();
      }
    }
  }
}
