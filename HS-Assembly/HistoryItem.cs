﻿// Decompiled with JetBrains decompiler
// Type: HistoryItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HistoryItem : MonoBehaviour
{
  public Actor m_mainCardActor;
  protected bool m_dead;
  protected int m_splatAmount;
  protected Entity m_entity;
  protected Texture m_portraitTexture;
  protected Material m_portraitGoldenMaterial;
  protected bool m_mainCardActorInitialized;
  protected bool m_fatigue;

  public Entity GetEntity()
  {
    return this.m_entity;
  }

  public Texture GetPortraitTexture()
  {
    return this.m_portraitTexture;
  }

  public Material GetPortraitGoldenMaterial()
  {
    return this.m_portraitGoldenMaterial;
  }

  public bool IsMainCardActorInitialized()
  {
    return this.m_mainCardActorInitialized;
  }

  public void InitializeMainCardActor()
  {
    if (this.m_mainCardActorInitialized)
      return;
    this.m_mainCardActor.TurnOffCollider();
    this.m_mainCardActor.SetActorState(ActorStateType.CARD_HISTORY);
    this.m_mainCardActorInitialized = true;
  }

  public void DisplaySpells()
  {
    if (this.m_fatigue || !this.m_entity.IsCharacter() && !this.m_entity.IsWeapon())
      return;
    if (this.m_dead || this.m_splatAmount >= this.m_entity.GetCurrentVitality())
    {
      this.DisplaySkullOnActor(this.m_mainCardActor);
    }
    else
    {
      if (this.m_splatAmount == 0)
        return;
      this.DisplaySplatOnActor(this.m_mainCardActor, this.m_splatAmount);
    }
  }

  private void DisplaySplatOnActor(Actor actor, int damage)
  {
    Spell spell = actor.GetSpell(SpellType.DAMAGE);
    if ((Object) spell == (Object) null)
      return;
    DamageSplatSpell damageSplatSpell = (DamageSplatSpell) spell;
    damageSplatSpell.SetDamage(damage);
    damageSplatSpell.ActivateState(SpellStateType.IDLE);
  }

  private void DisplaySkullOnActor(Actor actor)
  {
    Spell spell = actor.GetSpell(SpellType.SKULL);
    if ((Object) spell == (Object) null)
      return;
    spell.ActivateState(SpellStateType.IDLE);
  }
}
