﻿// Decompiled with JetBrains decompiler
// Type: BoxStateInfoList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class BoxStateInfoList
{
  public BoxCameraStateInfo m_CameraInfo;
  public BoxDiskStateInfo m_DiskInfo;
  public BoxDoorStateInfo m_LeftDoorInfo;
  public BoxDoorStateInfo m_RightDoorInfo;
  public BoxDrawerStateInfo m_DrawerInfo;
  public BoxLogoStateInfo m_LogoInfo;
  public BoxSpinnerStateInfo m_SpinnerInfo;
  public BoxStartButtonStateInfo m_StartButtonInfo;
}
