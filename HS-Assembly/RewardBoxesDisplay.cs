﻿// Decompiled with JetBrains decompiler
// Type: RewardBoxesDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class RewardBoxesDisplay : MonoBehaviour
{
  public bool m_playBoxFlyoutSound = true;
  private GameLayer m_layer = GameLayer.IgnoreFullScreenEffects;
  private bool m_addRewardsToCacheValues = true;
  public const string DEFAULT_PREFAB = "RewardBoxes";
  public GameObject m_Root;
  public GameObject m_ClickCatcher;
  [CustomEditField(Sections = "Reward Panel")]
  public NormalButton m_DoneButton;
  public RewardBoxesDisplay.RewardSet m_RewardSets;
  private List<Action> m_doneCallbacks;
  private List<GameObject> m_InstancedObjects;
  private GameObject[] m_RewardObjects;
  private List<RewardBoxesDisplay.RewardPackageData> m_RewardPackages;
  private bool m_useDarkeningClickCatcher;
  private bool m_doneButtonFinishedShown;
  private bool m_destroyed;
  private List<RewardData> m_Rewards;
  private static RewardBoxesDisplay s_Instance;

  public bool IsClosing { get; private set; }

  private void Awake()
  {
    RewardBoxesDisplay.s_Instance = this;
    this.m_addRewardsToCacheValues = !Login.IsLoginSceneActive();
    this.m_InstancedObjects = new List<GameObject>();
    this.m_doneCallbacks = new List<Action>();
    RenderUtils.SetAlpha(this.m_ClickCatcher, 0.0f);
  }

  private void Start()
  {
    if (!((UnityEngine.Object) this.m_RewardSets.m_RewardPackage != (UnityEngine.Object) null))
      return;
    this.m_RewardSets.m_RewardPackage.SetActive(false);
  }

  private void OnDisable()
  {
  }

  private void OnDestroy()
  {
    this.CleanUp();
    this.m_destroyed = true;
  }

  private void OnEnable()
  {
  }

  public static RewardBoxesDisplay Get()
  {
    return RewardBoxesDisplay.s_Instance;
  }

  public void SetRewards(List<RewardData> rewards)
  {
    this.m_Rewards = rewards;
  }

  public void UseDarkeningClickCatcher(bool value)
  {
    this.m_useDarkeningClickCatcher = value;
  }

  public void RegisterDoneCallback(Action action)
  {
    this.m_doneCallbacks.Add(action);
  }

  public List<RewardBoxesDisplay.RewardPackageData> GetPackageData(int rewardCount)
  {
    for (int index = 0; index < this.m_RewardSets.m_RewardData.Count; ++index)
    {
      if (this.m_RewardSets.m_RewardData[index].m_PackageData.Count == rewardCount)
        return this.m_RewardSets.m_RewardData[index].m_PackageData;
    }
    UnityEngine.Debug.LogError((object) ("RewardBoxesDisplay: GetPackageData - no package data found with a reward count of " + (object) rewardCount));
    return (List<RewardBoxesDisplay.RewardPackageData>) null;
  }

  public void SetLayer(GameLayer layer)
  {
    this.m_layer = layer;
    SceneUtils.SetLayer(this.gameObject, this.m_layer);
  }

  public void ShowAlreadyOpenedRewards()
  {
    this.m_RewardPackages = this.GetPackageData(this.m_Rewards.Count);
    this.m_RewardObjects = new GameObject[this.m_Rewards.Count];
    this.FadeFullscreenEffectsIn();
    this.ShowOpenedRewards();
    this.AllDone();
  }

  public void ShowOpenedRewards()
  {
    for (int rewardIndex = 0; rewardIndex < this.m_RewardPackages.Count; ++rewardIndex)
    {
      RewardBoxesDisplay.RewardPackageData rewardPackage = this.m_RewardPackages[rewardIndex];
      if ((UnityEngine.Object) rewardPackage.m_TargetBone == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: AnimateRewards package target bone is null!");
        break;
      }
      if (rewardIndex >= this.m_RewardObjects.Length)
      {
        UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: AnimateRewards reward index exceeded!");
        break;
      }
      this.m_RewardObjects[rewardIndex] = this.CreateRewardInstance(rewardIndex, rewardPackage.m_TargetBone.position, true);
    }
  }

  public void AnimateRewards()
  {
    int count = this.m_Rewards.Count;
    this.m_RewardPackages = this.GetPackageData(count);
    this.m_RewardObjects = new GameObject[count];
    for (int rewardIndex = 0; rewardIndex < this.m_RewardPackages.Count; ++rewardIndex)
    {
      RewardBoxesDisplay.RewardPackageData rewardPackage = this.m_RewardPackages[rewardIndex];
      if ((UnityEngine.Object) rewardPackage.m_TargetBone == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: AnimateRewards package target bone is null!");
        return;
      }
      if (rewardIndex >= this.m_RewardObjects.Length)
      {
        UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: AnimateRewards reward index exceeded!");
        return;
      }
      this.m_RewardObjects[rewardIndex] = this.CreateRewardInstance(rewardIndex, rewardPackage.m_TargetBone.position, false);
    }
    this.RewardPackageAnimation();
  }

  public void OpenReward(int rewardIndex, Vector3 rewardPos)
  {
    if (rewardIndex >= this.m_RewardObjects.Length)
    {
      UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: OpenReward reward index exceeded!");
    }
    else
    {
      GameObject rewardObject = this.m_RewardObjects[rewardIndex];
      if ((UnityEngine.Object) rewardObject == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: OpenReward object is null!");
      }
      else
      {
        if (!rewardObject.activeSelf)
          rewardObject.SetActive(true);
        if (!this.CheckAllRewardsActive())
          return;
        this.AllDone();
      }
    }
  }

  private void RewardPackageAnimation()
  {
    if ((UnityEngine.Object) this.m_RewardSets.m_RewardPackage == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: missing Reward Package!");
    }
    else
    {
      this.FadeFullscreenEffectsIn();
      for (int index = 0; index < this.m_RewardPackages.Count; ++index)
      {
        RewardBoxesDisplay.RewardPackageData rewardPackage = this.m_RewardPackages[index];
        if ((UnityEngine.Object) rewardPackage.m_TargetBone == (UnityEngine.Object) null || (UnityEngine.Object) rewardPackage.m_StartBone == (UnityEngine.Object) null)
        {
          UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: missing reward target bone!");
        }
        else
        {
          GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_RewardSets.m_RewardPackage);
          TransformUtil.AttachAndPreserveLocalTransform(gameObject.transform, this.m_Root.transform);
          gameObject.transform.position = rewardPackage.m_StartBone.position;
          gameObject.SetActive(true);
          this.m_InstancedObjects.Add(gameObject);
          Vector3 localScale = gameObject.transform.localScale;
          gameObject.transform.localScale = Vector3.zero;
          SceneUtils.EnableColliders(gameObject, false);
          iTween.ScaleTo(gameObject, iTween.Hash((object) "scale", (object) localScale, (object) "time", (object) this.m_RewardSets.m_AnimationTime, (object) "delay", (object) rewardPackage.m_StartDelay, (object) "easetype", (object) iTween.EaseType.linear));
          PlayMakerFSM component1 = gameObject.GetComponent<PlayMakerFSM>();
          if ((UnityEngine.Object) component1 == (UnityEngine.Object) null)
          {
            UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: missing reward Playmaker FSM!");
          }
          else
          {
            if (!this.m_playBoxFlyoutSound)
              component1.FsmVariables.FindFsmBool("PlayFlyoutSound").Value = false;
            RewardPackage component2 = gameObject.GetComponent<RewardPackage>();
            component2.m_RewardIndex = index;
            RewardBoxesDisplay.RewardBoxData rewardBoxData = new RewardBoxesDisplay.RewardBoxData();
            rewardBoxData.m_GameObject = gameObject;
            rewardBoxData.m_RewardPackage = component2;
            rewardBoxData.m_FSM = component1;
            rewardBoxData.m_Index = index;
            iTween.MoveTo(gameObject, iTween.Hash((object) "position", (object) rewardPackage.m_TargetBone.transform.position, (object) "time", (object) this.m_RewardSets.m_AnimationTime, (object) "delay", (object) rewardPackage.m_StartDelay, (object) "easetype", (object) iTween.EaseType.linear, (object) "onstarttarget", (object) this.gameObject, (object) "onstart", (object) "RewardPackageOnStart", (object) "onstartparams", (object) rewardBoxData, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "RewardPackageOnComplete", (object) "oncompleteparams", (object) rewardBoxData));
          }
        }
      }
    }
  }

  private void RewardPackageOnStart(RewardBoxesDisplay.RewardBoxData boxData)
  {
    boxData.m_FSM.SendEvent("Birth");
  }

  private void RewardPackageOnComplete(RewardBoxesDisplay.RewardBoxData boxData)
  {
    this.StartCoroutine(this.RewardPackageActivate(boxData));
  }

  [DebuggerHidden]
  private IEnumerator RewardPackageActivate(RewardBoxesDisplay.RewardBoxData boxData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardBoxesDisplay.\u003CRewardPackageActivate\u003Ec__Iterator8C()
    {
      boxData = boxData,
      \u003C\u0024\u003EboxData = boxData,
      \u003C\u003Ef__this = this
    };
  }

  private void RewardPackagePressed(UIEvent e)
  {
    Log.Kyle.Print("box clicked!");
  }

  private GameObject CreateRewardInstance(int rewardIndex, Vector3 rewardPos, bool activeOnStart)
  {
    RewardData reward = this.m_Rewards[rewardIndex];
    GameObject go = (GameObject) null;
    switch (reward.RewardType)
    {
      case Reward.Type.ARCANE_DUST:
        go = UnityEngine.Object.Instantiate<GameObject>(this.m_RewardSets.m_RewardDust);
        TransformUtil.AttachAndPreserveLocalTransform(go.transform, this.m_Root.transform);
        go.transform.position = rewardPos;
        go.SetActive(true);
        go.GetComponentInChildren<UberText>().Text = ((ArcaneDustRewardData) reward).Amount.ToString();
        go.SetActive(activeOnStart);
        break;
      case Reward.Type.BOOSTER_PACK:
        BoosterPackRewardData boosterPackRewardData = reward as BoosterPackRewardData;
        int id = boosterPackRewardData.Id;
        if (id == 0)
        {
          id = 1;
          UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay - booster reward is not valid. ID = 0");
        }
        Log.Kyle.Print(string.Format("Booster DB ID: {0}", (object) id));
        string arenaPrefab = GameDbf.Booster.GetRecord(id).ArenaPrefab;
        if (string.IsNullOrEmpty(arenaPrefab))
        {
          UnityEngine.Debug.LogError((object) string.Format("RewardBoxesDisplay - no prefab found for booster {0}!", (object) boosterPackRewardData.Id));
          break;
        }
        go = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(arenaPrefab), true, false);
        TransformUtil.AttachAndPreserveLocalTransform(go.transform, this.m_Root.transform);
        go.transform.position = rewardPos;
        go.SetActive(activeOnStart);
        break;
      case Reward.Type.CARD:
        go = UnityEngine.Object.Instantiate<GameObject>(this.m_RewardSets.m_RewardCard);
        TransformUtil.AttachAndPreserveLocalTransform(go.transform, this.m_Root.transform);
        go.transform.position = rewardPos;
        go.SetActive(true);
        CardRewardData cardData = (CardRewardData) reward;
        go.GetComponentInChildren<RewardCard>().LoadCard(cardData, this.m_layer);
        go.SetActive(activeOnStart);
        break;
      case Reward.Type.CARD_BACK:
        go = UnityEngine.Object.Instantiate<GameObject>(this.m_RewardSets.m_RewardCardBack);
        TransformUtil.AttachAndPreserveLocalTransform(go.transform, this.m_Root.transform);
        go.transform.position = rewardPos;
        go.SetActive(true);
        CardBackRewardData cardbackData = (CardBackRewardData) reward;
        go.GetComponentInChildren<RewardCardBack>().LoadCardBack(cardbackData, this.m_layer);
        go.SetActive(activeOnStart);
        break;
      case Reward.Type.GOLD:
        go = UnityEngine.Object.Instantiate<GameObject>(this.m_RewardSets.m_RewardGold);
        TransformUtil.AttachAndPreserveLocalTransform(go.transform, this.m_Root.transform);
        go.transform.position = rewardPos;
        go.SetActive(true);
        go.GetComponentInChildren<UberText>().Text = ((GoldRewardData) reward).Amount.ToString();
        go.SetActive(activeOnStart);
        break;
    }
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: Unable to create reward, object null!");
      return (GameObject) null;
    }
    if (rewardIndex >= this.m_RewardObjects.Length)
    {
      UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: CreateRewardInstance reward index exceeded!");
      return (GameObject) null;
    }
    SceneUtils.SetLayer(go, this.m_layer);
    this.m_RewardObjects[rewardIndex] = go;
    this.m_InstancedObjects.Add(go);
    return go;
  }

  private void AllDone()
  {
    Vector3 zero = Vector3.zero;
    for (int index = 0; index < this.m_RewardPackages.Count; ++index)
    {
      RewardBoxesDisplay.RewardPackageData rewardPackage = this.m_RewardPackages[index];
      zero += rewardPackage.m_TargetBone.position;
    }
    this.m_DoneButton.transform.position = zero / (float) this.m_RewardPackages.Count;
    this.m_DoneButton.gameObject.SetActive(true);
    this.m_DoneButton.SetText(GameStrings.Get("GLOBAL_DONE"));
    Spell component = this.m_DoneButton.m_button.GetComponent<Spell>();
    component.AddFinishedCallback(new Spell.FinishedCallback(this.OnDoneButtonShown));
    component.ActivateState(SpellStateType.BIRTH);
  }

  private void OnDoneButtonShown(Spell spell, object userData)
  {
    this.m_doneButtonFinishedShown = true;
    this.m_DoneButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDoneButtonPressed));
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  private void OnDoneButtonPressed(UIEvent e)
  {
    Navigation.GoBack();
  }

  public void Close()
  {
    this.IsClosing = true;
    if (this.m_doneButtonFinishedShown)
      this.OnNavigateBack();
    else
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  private bool OnNavigateBack()
  {
    UnityEngine.Debug.Log((object) "navigating back!");
    if (!this.m_DoneButton.m_button.activeSelf)
      return false;
    foreach (GameObject rewardObject in this.m_RewardObjects)
    {
      if (!((UnityEngine.Object) rewardObject == (UnityEngine.Object) null))
      {
        PlayMakerFSM component = rewardObject.GetComponent<PlayMakerFSM>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          component.SendEvent("Death");
        foreach (Component componentsInChild in rewardObject.GetComponentsInChildren<UberText>())
          iTween.FadeTo(componentsInChild.gameObject, iTween.Hash((object) "alpha", (object) 0.0f, (object) "time", (object) 0.8f, (object) "includechildren", (object) true, (object) "easetype", (object) iTween.EaseType.easeInOutCubic));
        RewardCard componentInChildren = rewardObject.GetComponentInChildren<RewardCard>();
        if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
          componentInChildren.Death();
      }
    }
    SceneUtils.EnableColliders(this.m_DoneButton.gameObject, false);
    this.m_DoneButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDoneButtonPressed));
    Spell component1 = this.m_DoneButton.m_button.GetComponent<Spell>();
    component1.AddFinishedCallback(new Spell.FinishedCallback(this.OnDoneButtonHidden));
    component1.ActivateState(SpellStateType.DEATH);
    if (this.m_addRewardsToCacheValues)
      this.AddRewardsToCacheValues();
    return true;
  }

  private void AddRewardsToCacheValues()
  {
    bool flag = false;
    using (List<RewardData>.Enumerator enumerator = this.m_Rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RewardData current = enumerator.Current;
        switch (current.RewardType)
        {
          case Reward.Type.ARCANE_DUST:
            NetCache.Get().OnArcaneDustBalanceChanged((long) ((ArcaneDustRewardData) current).Amount);
            continue;
          case Reward.Type.CARD:
            CardRewardData cardRewardData = (CardRewardData) current;
            CollectionManager.Get().OnCardRewardOpened(cardRewardData.CardID, cardRewardData.Premium, cardRewardData.Count);
            continue;
          case Reward.Type.GOLD:
            flag = true;
            continue;
          default:
            continue;
        }
      }
    }
    if (!flag)
      return;
    NetCache.Get().RefreshNetObject<NetCache.NetCacheGoldBalance>();
  }

  private void OnDoneButtonHidden(Spell spell, object userData)
  {
    this.FadeFullscreenEffectsOut();
  }

  private void FadeFullscreenEffectsIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((UnityEngine.Object) fullScreenFxMgr == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: FullScreenFXMgr.Get() returned null!");
    }
    else
    {
      fullScreenFxMgr.SetBlurBrightness(0.85f);
      fullScreenFxMgr.SetBlurDesaturation(0.0f);
      fullScreenFxMgr.Vignette(0.4f, 0.5f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      fullScreenFxMgr.Blur(1f, 0.5f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      if (!this.m_useDarkeningClickCatcher)
        return;
      iTween.FadeTo(this.m_ClickCatcher, 0.75f, 0.5f);
    }
  }

  private void FadeFullscreenEffectsOut()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((UnityEngine.Object) fullScreenFxMgr == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: FullScreenFXMgr.Get() returned null!");
    }
    else
    {
      fullScreenFxMgr.StopVignette(2f, iTween.EaseType.easeOutCirc, new FullScreenFXMgr.EffectListener(this.FadeFullscreenEffectsOutFinished));
      fullScreenFxMgr.StopBlur(2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      if (!this.m_useDarkeningClickCatcher)
        return;
      iTween.FadeTo(this.m_ClickCatcher, 0.0f, 0.5f);
    }
  }

  private void FadeVignetteIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((UnityEngine.Object) fullScreenFxMgr == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "RewardBoxesDisplay: FullScreenFXMgr.Get() returned null!");
    }
    else
    {
      fullScreenFxMgr.DisableBlur();
      fullScreenFxMgr.Vignette(1.4f, 1.5f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
  }

  private void FadeFullscreenEffectsOutFinished()
  {
    using (List<Action>.Enumerator enumerator = this.m_doneCallbacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Action current = enumerator.Current;
        if (current != null)
          current();
      }
    }
    this.m_doneCallbacks.Clear();
    if (this.m_destroyed)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  private bool CheckAllRewardsActive()
  {
    foreach (GameObject rewardObject in this.m_RewardObjects)
    {
      if (!rewardObject.activeSelf)
        return false;
    }
    return true;
  }

  private void CleanUp()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_InstancedObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.FadeFullscreenEffectsOut();
    RewardBoxesDisplay.s_Instance = (RewardBoxesDisplay) null;
  }

  public void DebugLogRewards()
  {
    UnityEngine.Debug.Log((object) "BOX REWARDS:");
    for (int index = 0; index < this.m_Rewards.Count; ++index)
    {
      RewardData reward = this.m_Rewards[index];
      UnityEngine.Debug.Log((object) string.Format("  reward {0}={1}", (object) index, (object) reward));
    }
  }

  [Serializable]
  public class RewardPackageData
  {
    public Transform m_StartBone;
    public Transform m_TargetBone;
    public float m_StartDelay;
  }

  [Serializable]
  public class RewardSet
  {
    public float m_AnimationTime = 1f;
    public GameObject m_RewardPackage;
    public GameObject m_RewardCard;
    public GameObject m_RewardCardBack;
    public GameObject m_RewardGold;
    public GameObject m_RewardDust;
    public List<RewardBoxesDisplay.BoxRewardData> m_RewardData;
  }

  [Serializable]
  public class BoxRewardData
  {
    public List<RewardBoxesDisplay.RewardPackageData> m_PackageData;
  }

  public class RewardBoxData
  {
    public GameObject m_GameObject;
    public RewardPackage m_RewardPackage;
    public PlayMakerFSM m_FSM;
    public int m_Index;
  }

  public class RewardCardLoadData
  {
    public EntityDef m_EntityDef;
    public Transform m_ParentTransform;
    public CardRewardData m_CardRewardData;
  }

  private enum Events
  {
    GVG_PROMOTION,
  }
}
