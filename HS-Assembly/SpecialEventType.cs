﻿// Decompiled with JetBrains decompiler
// Type: SpecialEventType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.ComponentModel;

public enum SpecialEventType
{
  UNKNOWN = -1,
  [Description("always")] ALWAYS = 0,
  [Description("none")] IGNORE = 0,
  [Description("launch")] LAUNCH_DAY = 1,
  [Description("naxx_1")] NAXX_1_OPENS = 2,
  [Description("naxx_2")] NAXX_2_OPENS = 3,
  [Description("naxx_3")] NAXX_3_OPENS = 4,
  [Description("naxx_4")] NAXX_4_OPENS = 5,
  [Description("naxx_5")] NAXX_5_OPENS = 6,
  [Description("gvg_promote")] GVG_PROMOTION = 7,
  [Description("gvg_begin")] GVG_LAUNCH_PERIOD = 8,
  [Description("gvg_arena")] GVG_ARENA_PLAY = 9,
  [Description("lunar_new_year")] LUNAR_NEW_YEAR = 11,
  [Description("brm_1")] BRM_1_OPENS = 12,
  [Description("brm_2")] BRM_2_OPENS = 13,
  [Description("brm_3")] BRM_3_OPENS = 14,
  [Description("brm_4")] BRM_4_OPENS = 15,
  [Description("brm_5")] BRM_5_OPENS = 16,
  [Description("brm_pre_sale")] BRM_PRE_SALE = 17,
  [Description("brm_normal_sale")] BRM_NORMAL_SALE = 18,
  [Description("tb_pre_event")] SPECIAL_EVENT_PRE_TAVERN_BRAWL = 19,
  [Description("tgt_pre_sale")] SPECIAL_EVENT_TGT_PRE_SALE = 29,
  [Description("tgt_normal_sale")] SPECIAL_EVENT_TGT_NORMAL_SALE = 30,
  [Description("samsung_galaxy_gifts")] SPECIAL_EVENT_SAMSUNG_GALAXY_GIFTS = 31,
  [Description("tgt_arena_draftable")] SPECIAL_EVENT_TGT_ARENA_DRAFTABLE = 32,
  [Description("loe_1")] SPECIAL_EVENT_LOE_WING_1_OPEN = 56,
  [Description("loe_2")] SPECIAL_EVENT_LOE_WING_2_OPEN = 57,
  [Description("loe_3")] SPECIAL_EVENT_LOE_WING_3_OPEN = 58,
  [Description("loe_4")] SPECIAL_EVENT_LOE_WING_4_OPEN = 59,
  [Description("set_rotation_2016")] SPECIAL_EVENT_SET_ROTATION_2016 = 65,
  [Description("set_rotation_2017")] SPECIAL_EVENT_SET_ROTATION_2017 = 66,
  [Description("feast_of_winter_veil")] FEAST_OF_WINTER_VEIL = 85,
  [Description("og_pre_purchase")] SPECIAL_EVENT_OLD_GODS_PRE_PURCHASE = 105,
  [Description("og_normal_sale")] SPECIAL_EVENT_OLD_GODS_NORMAL_SALE = 106,
  [Description("apple_charity_promo_2016")] SPECIAL_EVENT_APPLE_CHARITY_PROMO_2016 = 108,
  [Description("naxx_gvg_real_money_sale")] SPECIAL_EVENT_NAXX_GVG_REAL_MONEY_SALE = 160,
  [Description("set_rotation_2016_freepacks")] SPECIAL_EVENT_SET_ROTATION_2016_FREEPACKS = 161,
  [Description("set_rotation_2016_questline")] SPECIAL_EVENT_SET_ROTATION_2016_QUESTLINE = 162,
  [Description("friend_week")] FRIEND_WEEK = 166,
  [Description("kara_1")] SPECIAL_EVENT_KARA_WING_1_OPEN = 167,
  [Description("kara_2")] SPECIAL_EVENT_KARA_WING_2_OPEN = 168,
  [Description("kara_3")] SPECIAL_EVENT_KARA_WING_3_OPEN = 169,
  [Description("kara_4")] SPECIAL_EVENT_KARA_WING_4_OPEN = 170,
  [Description("kara_launch_sale")] SPECIAL_EVENT_KARA_LAUNCH_SALE = 171,
  [Description("kara_post_launch_sale")] SPECIAL_EVENT_KARA_POST_LAUNCH_SALE = 172,
  [Description("kara_prologue_grantable")] SPECIAL_EVENT_KARA_PROLOGUE_GRANTABLE = 177,
  [Description("msg_normal_sale")] SPECIAL_EVENT_MSG_NORMAL_SALE = 178,
  [Description("msg_intro_questline")] SPECIAL_EVENT_MSG_INTRO_QUESTLINE = 179,
}
