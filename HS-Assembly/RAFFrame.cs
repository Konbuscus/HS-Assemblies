﻿// Decompiled with JetBrains decompiler
// Type: RAFFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RAFFrame : UIBPopup
{
  private static readonly Vector3 SHOW_SCALE_PHONE = new Vector3(18f, 18f, 18f);
  private bool m_isHeroKeyArtShowing = true;
  private static RAFFrame s_Instance;
  public GeneralStoreHeroesContentDisplay m_heroDisplay;
  public UIBButton m_recruitFriendsButton;
  public HighlightState m_recruitFriendsButtonGlow;
  public UIBButton m_infoButton;
  public GameObject m_frame;
  public GameObject m_heroFrame;
  public GameObject m_progressFrame;
  public RAFLinkFrame m_linkFrame;
  public RAFInfo m_infoFrame;
  public List<RAFRecruitBar> m_recruitContainerList;
  public GameObject m_recruitCount;
  public UberText m_recruitCountText;
  public List<RAFChest> m_chestList;
  public GameObject m_heroRewardChestTooltip;
  public UberText m_heroRewardChestTooltipText;
  public Transform m_heroRewardChestTooltipHeroBone;
  public Transform m_heroRewardChestTooltipHeroPowerBone;
  public GameObject m_packRewardChestTooltip;
  public UberText m_packRewardChestTooltipText;
  public GameObject m_packRewardContainer;
  public UnopenedPack m_packReward;
  public GameObject m_totalResultLabel;
  public GameObject m_totalResult;
  public GameObject m_inputBlockerRenderer;
  private RAFChest m_heroChest;
  private Actor m_heroActor;
  private Actor m_heroPowerActor;
  private bool m_showHeroRewardChestTooltip;
  private PegUIElement m_inputBlockerPegUIElement;
  private bool m_isHeroDisplaySetup;
  private CollectionHeroDef m_collectionHeroDef;
  private MusicPlaylistType m_prevMusicPlaylist;
  private RAFFrame.Display m_shownDisplay;

  private void Awake()
  {
    RAFFrame.s_Instance = this;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_showScale = RAFFrame.SHOW_SCALE_PHONE;
    this.m_recruitFriendsButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRecruitFriendsButtonReleased));
    this.m_infoButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnInfoButtonReleased));
    this.m_heroDisplay.m_previewToggle.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnHeroPreviewToggle));
  }

  private void Start()
  {
    this.m_heroChest = this.m_chestList[0];
    this.m_heroChest.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.ShowHeroRewardTooltip));
    this.m_heroChest.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.HideHeroRewardTooltip));
    for (int index = 1; index < this.m_chestList.Count; ++index)
    {
      this.m_chestList[index].AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.ShowPackRewardTooltip));
      this.m_chestList[index].AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.HidePackRewardTooltip));
    }
    this.m_packReward.AddBooster();
    if (this.m_shownDisplay == RAFFrame.Display.NONE)
      this.ShowHeroFrame();
    this.UpdateRecruitFriendsButtonGlow();
  }

  private void OnDestroy()
  {
    this.Hide(true);
    RAFFrame.s_Instance = (RAFFrame) null;
  }

  public static RAFFrame Get()
  {
    return RAFFrame.s_Instance;
  }

  public override void Show()
  {
    if (this.IsShown())
      return;
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    FullScreenFXMgr.Get().StartStandardBlurVignette(0.1f);
    if ((bool) UniversalInputManager.UsePhoneUI)
      BnetBar.Get().m_currencyFrame.HideTemporarily();
    base.Show();
    if ((Object) this.m_inputBlockerPegUIElement != (Object) null)
    {
      Object.Destroy((Object) this.m_inputBlockerPegUIElement.gameObject);
      this.m_inputBlockerPegUIElement = (PegUIElement) null;
    }
    GameObject inputBlocker = CameraUtils.CreateInputBlocker(CameraUtils.FindFirstByLayer(this.gameObject.layer), "RAFInputBlocker");
    SceneUtils.SetLayer(inputBlocker, this.gameObject.layer);
    this.m_inputBlockerPegUIElement = inputBlocker.AddComponent<PegUIElement>();
    this.m_inputBlockerPegUIElement.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnInputBlockerRelease));
    TransformUtil.SetPosY((Component) this.m_inputBlockerPegUIElement, this.gameObject.transform.position.y - 15f);
    Options.Get().SetBool(Option.HAS_SEEN_RAF, true);
    FriendListFrame friendListFrame = ChatMgr.Get().FriendListFrame;
    if (!((Object) friendListFrame != (Object) null))
      return;
    friendListFrame.UpdateRAFButtonGlow();
  }

  protected override void Hide(bool animate)
  {
    if (!this.IsShown())
      return;
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    if ((Object) this.m_inputBlockerPegUIElement != (Object) null)
    {
      Object.Destroy((Object) this.m_inputBlockerPegUIElement.gameObject);
      this.m_inputBlockerPegUIElement = (PegUIElement) null;
    }
    FullScreenFXMgr.Get().EndStandardBlurVignette(0.1f, (FullScreenFXMgr.EffectListener) null);
    this.m_heroDisplay.ResetPreview();
    this.m_isHeroKeyArtShowing = true;
    this.StopHeroMusic();
    if ((bool) UniversalInputManager.UsePhoneUI)
      BnetBar.Get().m_currencyFrame.RefreshContents();
    base.Hide(animate);
  }

  public void ShowProgressFrame()
  {
    this.m_heroFrame.SetActive(false);
    this.m_progressFrame.SetActive(true);
    if ((Object) this.m_heroActor == (Object) null)
      AssetLoader.Get().LoadActor("Card_Play_Hero", new AssetLoader.GameObjectCallback(this.OnHeroActorLoaded), (object) null, false);
    if ((Object) this.m_heroPowerActor == (Object) null)
      AssetLoader.Get().LoadActor("Card_Play_HeroPower", new AssetLoader.GameObjectCallback(this.OnHeroPowerActorLoaded), (object) null, false);
    this.m_shownDisplay = RAFFrame.Display.PROGRESS;
  }

  public void ShowHeroFrame()
  {
    this.m_heroFrame.SetActive(true);
    if (!this.m_isHeroDisplaySetup)
    {
      this.m_heroDisplay.SetKeyArtRenderer(this.m_heroDisplay.m_parentLite.m_renderQuad);
      this.m_heroDisplay.m_parentLite.m_renderToTexture.GetComponent<RenderToTexture>().m_RenderToObject = this.m_heroDisplay.m_renderArtQuad;
      CardHeroDbfRecord record = GameDbf.CardHero.GetRecord(17);
      this.m_collectionHeroDef = GameUtils.LoadGameObjectWithComponent<CollectionHeroDef>(DefLoader.Get().GetCardDef(record.CardId).m_CollectionHeroDefPath);
      this.m_heroDisplay.UpdateFrame(record, 0, this.m_collectionHeroDef);
      this.m_isHeroDisplaySetup = true;
    }
    else
      this.m_heroDisplay.ResetPreview();
    this.m_progressFrame.SetActive(false);
    this.m_shownDisplay = RAFFrame.Display.HERO;
  }

  public void ResetProgressFrame()
  {
    using (List<RAFRecruitBar>.Enumerator enumerator = this.m_recruitContainerList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetLocked(true);
    }
    this.m_recruitCount.SetActive(false);
    this.m_totalResultLabel.SetActive(false);
    this.m_totalResult.SetActive(false);
    using (List<RAFChest>.Enumerator enumerator = this.m_chestList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetOpen(false);
    }
  }

  public void UpdateRecruitFriendsButtonGlow()
  {
    this.m_recruitFriendsButtonGlow.ChangeState(!Options.Get().GetBool(Option.HAS_SEEN_RAF_RECRUIT_URL) ? ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE : ActorStateType.NONE);
  }

  public void SetProgress(int numRecruits)
  {
    this.ResetProgressFrame();
    for (int index = 0; index < numRecruits && index < 5; ++index)
    {
      RAFRecruitBar recruitContainer = this.m_recruitContainerList[index];
      recruitContainer.SetLocked(false);
      recruitContainer.SetBattleTag("GoodKnight#1234");
      recruitContainer.SetLevel(20);
      this.m_chestList[index].SetOpen(true);
    }
    if (numRecruits <= 5)
      return;
    this.m_recruitCount.gameObject.SetActive(true);
    int num = numRecruits - 5;
    this.m_recruitCountText.Text = GameStrings.Format("GLUE_RAF_PROGRESS_FRAME_RECRUIT_COUNT", (object) num, (object) num);
  }

  public void SetProgressData(uint totalRecruitCount, List<RAFManager.RecruitData> topRecruits)
  {
    this.ResetProgressFrame();
    if ((int) totalRecruitCount == 0)
    {
      Log.RAF.PrintError("SetProgressData() - totalRecruitCount is 0!");
      this.ShowHeroFrame();
    }
    else if (topRecruits == null)
    {
      Log.RAF.PrintError("SetProgressData() - topRecruits is NULL!");
      this.ShowHeroFrame();
    }
    else
    {
      for (int index = 0; index < topRecruits.Count; ++index)
      {
        RAFRecruitBar recruitContainer = this.m_recruitContainerList[index];
        recruitContainer.SetLocked(false);
        RAFManager.RecruitData topRecruit = topRecruits[index];
        string battleTag = topRecruit.m_recruitBattleTag != null ? topRecruit.m_recruitBattleTag : GameStrings.Get("GAMEPLAY_UNKNOWN_OPPONENT_NAME");
        int progress = (int) topRecruit.m_recruit.Progress;
        recruitContainer.SetGameAccountId(topRecruit.m_recruit.GameAccountId);
        recruitContainer.SetBattleTag(battleTag);
        recruitContainer.SetLevel(progress);
        if (progress >= 20)
          this.m_chestList[index].SetOpen(true);
      }
      if (totalRecruitCount <= 5U)
        return;
      this.m_recruitCount.gameObject.SetActive(true);
      int num = (int) totalRecruitCount - 5;
      this.m_recruitCountText.Text = GameStrings.Format("GLUE_RAF_PROGRESS_FRAME_RECRUIT_COUNT", (object) num, (object) num);
    }
  }

  public void UpdateBattleTag(BnetId gameAccountId, string battleTag)
  {
    using (List<RAFRecruitBar>.Enumerator enumerator = this.m_recruitContainerList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RAFRecruitBar current = enumerator.Current;
        if (current.GetGameAccountId() == gameAccountId)
        {
          current.SetBattleTag(battleTag);
          break;
        }
      }
    }
  }

  public void ShowLinkFrame(string displayURL, string fullURL)
  {
    Options.Get().SetBool(Option.HAS_SEEN_RAF_RECRUIT_URL, true);
    this.UpdateRecruitFriendsButtonGlow();
    this.m_linkFrame.SetURL(displayURL, fullURL);
    this.m_linkFrame.Show();
  }

  public void DarkenInputBlocker(GameObject inputBlockerObject, float alpha)
  {
    inputBlockerObject.AddComponent<MeshRenderer>().material = this.m_inputBlockerRenderer.GetComponent<MeshRenderer>().material;
    inputBlockerObject.AddComponent<MeshFilter>().mesh = this.m_inputBlockerRenderer.GetComponent<MeshFilter>().mesh;
    BoxCollider component = inputBlockerObject.GetComponent<BoxCollider>();
    TransformUtil.SetLocalScaleXY(inputBlockerObject, component.size.x, component.size.y);
    component.size = new Vector3(1f, 1f, 0.0f);
    TransformUtil.SetLocalEulerAngleX(inputBlockerObject, 90f);
    RenderUtils.SetAlpha(inputBlockerObject, alpha);
  }

  private bool OnNavigateBack()
  {
    this.Hide(true);
    return true;
  }

  private void OnInputBlockerRelease(UIEvent e)
  {
    this.Hide(true);
  }

  private void OnRecruitFriendsButtonReleased(UIEvent e)
  {
    string recruitDisplayUrl = RAFManager.Get().GetRecruitDisplayURL();
    if (recruitDisplayUrl == null)
      return;
    string recruitFullUrl = RAFManager.Get().GetRecruitFullURL();
    this.ShowLinkFrame(recruitDisplayUrl, recruitFullUrl);
  }

  private void OnInfoButtonReleased(UIEvent e)
  {
    this.m_infoFrame.Show();
  }

  private void OnHeroPreviewToggle(UIEvent e)
  {
    this.m_isHeroKeyArtShowing = !this.m_isHeroKeyArtShowing;
    if (this.m_isHeroKeyArtShowing)
      this.StopHeroMusic();
    else
      this.PlayHeroMusic();
  }

  private void PlayHeroMusic()
  {
    if ((Object) this.m_collectionHeroDef == (Object) null)
    {
      Log.RAF.PrintWarning("RAFFrame.PlayHeroMusic - m_collectionHeroDef is NULL!");
    }
    else
    {
      MusicPlaylistType heroPlaylist = this.m_collectionHeroDef.m_heroPlaylist;
      if (heroPlaylist == MusicPlaylistType.Invalid)
        return;
      this.m_prevMusicPlaylist = MusicManager.Get().GetCurrentPlaylist();
      MusicManager.Get().StartPlaylist(heroPlaylist);
    }
  }

  private void StopHeroMusic()
  {
    if (this.m_prevMusicPlaylist == MusicPlaylistType.Invalid)
      return;
    MusicManager.Get().StartPlaylist(this.m_prevMusicPlaylist);
    this.m_prevMusicPlaylist = MusicPlaylistType.Invalid;
  }

  private void ShowHeroRewardTooltip(UIEvent e)
  {
    this.m_showHeroRewardChestTooltip = true;
    this.m_heroRewardChestTooltipText.Text = GameStrings.Get(!this.m_heroChest.IsOpen() ? "GLUE_RAF_HERO_TOOLTIP_TITLE" : "GLUE_RAF_HERO_TOOLTIP_REDEEMED_TITLE");
    this.StartCoroutine(this.ShowHeroRewardTooltipWhenReady());
  }

  [DebuggerHidden]
  private IEnumerator ShowHeroRewardTooltipWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RAFFrame.\u003CShowHeroRewardTooltipWhenReady\u003Ec__Iterator238() { \u003C\u003Ef__this = this };
  }

  private void HideHeroRewardTooltip(UIEvent e)
  {
    this.m_showHeroRewardChestTooltip = false;
    this.m_heroRewardChestTooltip.SetActive(false);
  }

  private void ShowPackRewardTooltip(UIEvent e)
  {
    RAFChest element = e.GetElement() as RAFChest;
    this.m_packRewardChestTooltipText.Text = GameStrings.Get(!element.IsOpen() ? "GLUE_RAF_PACK_TOOLTIP_TITLE" : "GLUE_RAF_PACK_TOOLTIP_REDEEMED_TITLE");
    this.m_packRewardChestTooltip.transform.position = element.m_tooltipBone.transform.position;
    this.m_packRewardChestTooltip.SetActive(true);
  }

  private void HidePackRewardTooltip(UIEvent e)
  {
    this.m_packRewardChestTooltip.SetActive(false);
  }

  private void OnHeroActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if ((Object) actorObject == (Object) null)
    {
      Log.RAF.PrintWarning(string.Format("RAFFrame.OnHeroActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      this.m_heroActor = actorObject.GetComponent<Actor>();
      if ((Object) this.m_heroActor == (Object) null)
      {
        Log.RAF.PrintWarning(string.Format("RAFFrame.OnHeroActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        actorObject.transform.parent = this.m_heroRewardChestTooltip.transform;
        actorObject.transform.localScale = this.m_heroRewardChestTooltipHeroBone.localScale;
        actorObject.transform.localPosition = this.m_heroRewardChestTooltipHeroBone.localPosition;
        this.m_heroActor.SetUnlit();
        SceneUtils.SetLayer(this.m_heroActor.gameObject, this.gameObject.layer);
        Object.Destroy((Object) this.m_heroActor.m_healthObject);
        Object.Destroy((Object) this.m_heroActor.m_attackObject);
        this.m_heroActor.Hide();
        DefLoader.Get().LoadFullDef(GameUtils.TranslateDbIdToCardId(GameDbf.CardHero.GetRecord(17).CardId), new DefLoader.LoadDefCallback<FullDef>(this.OnHeroFullDefLoaded));
      }
    }
  }

  private void OnHeroPowerActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if ((Object) actorObject == (Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("RAFFrame.OnHeroActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      this.m_heroPowerActor = actorObject.GetComponent<Actor>();
      if ((Object) this.m_heroPowerActor == (Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("RAFFrame.OnHeroActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        actorObject.transform.parent = this.m_heroRewardChestTooltip.transform;
        actorObject.transform.localScale = this.m_heroRewardChestTooltipHeroPowerBone.localScale;
        actorObject.transform.localPosition = this.m_heroRewardChestTooltipHeroPowerBone.localPosition;
        this.m_heroPowerActor.SetUnlit();
        SceneUtils.SetLayer(this.m_heroPowerActor.gameObject, this.gameObject.layer);
        this.m_heroPowerActor.Hide();
        DefLoader.Get().LoadFullDef(GameUtils.GetHeroPowerCardIdFromHero(GameDbf.CardHero.GetRecord(17).CardId), new DefLoader.LoadDefCallback<FullDef>(this.OnHeroPowerFullDefLoaded));
      }
    }
  }

  private void OnHeroFullDefLoaded(string cardId, FullDef fullDef, object userData)
  {
    EntityDef entityDef = fullDef.GetEntityDef();
    this.m_heroActor.SetPremium(TAG_PREMIUM.GOLDEN);
    this.m_heroActor.SetEntityDef(entityDef);
    this.m_heroActor.SetCardDef(fullDef.GetCardDef());
    this.m_heroActor.UpdateAllComponents();
    this.m_heroActor.SetUnlit();
    this.m_heroActor.Show();
  }

  private void OnHeroPowerFullDefLoaded(string cardId, FullDef def, object userData)
  {
    this.m_heroPowerActor.SetCardDef(def.GetCardDef());
    this.m_heroPowerActor.SetEntityDef(def.GetEntityDef());
    this.m_heroPowerActor.UpdateAllComponents();
    this.m_heroPowerActor.SetUnlit();
    this.m_heroPowerActor.GetCardDef().m_AlwaysRenderPremiumPortrait = false;
    this.m_heroPowerActor.UpdateMaterials();
    this.m_heroPowerActor.Show();
    this.StartCoroutine(this.UpdateHeroSkinHeroPower());
  }

  [DebuggerHidden]
  private IEnumerator UpdateHeroSkinHeroPower()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RAFFrame.\u003CUpdateHeroSkinHeroPower\u003Ec__Iterator239() { \u003C\u003Ef__this = this };
  }

  private enum Display
  {
    NONE,
    HERO,
    PROGRESS,
  }
}
