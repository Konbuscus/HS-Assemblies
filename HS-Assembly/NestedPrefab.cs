﻿// Decompiled with JetBrains decompiler
// Type: NestedPrefab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[CustomEditClass]
public class NestedPrefab : MonoBehaviour
{
  private List<NestedPrefab.EditorMesh> m_EditorMeshes = new List<NestedPrefab.EditorMesh>();
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_Prefab;
  private string m_lastPrefab;
  private GameObject m_PrefabGameObject;

  public GameObject PrefabGameObject(bool instantiateIfNeeded = false)
  {
    if ((Object) this.m_PrefabGameObject == (Object) null && instantiateIfNeeded)
      this.UpdateMesh();
    return this.m_PrefabGameObject;
  }

  private void OnEnable()
  {
    if (!((Object) this.m_PrefabGameObject == (Object) null))
      return;
    this.UpdateMesh();
  }

  private void UpdateMesh()
  {
    this.LoadPrefab();
    this.m_EditorMeshes.Clear();
    if (!this.enabled || !((Object) this.m_PrefabGameObject != (Object) null))
      return;
    this.SetupEditorMesh(this.m_PrefabGameObject, Matrix4x4.identity);
  }

  private void SetupEditorMesh(GameObject go, Matrix4x4 goMtx)
  {
    if (!(bool) ((Object) go))
      return;
    Vector3 pos = go.transform.position * -1f;
    Matrix4x4 matrix4x4 = goMtx * Matrix4x4.TRS(pos, Quaternion.identity, Vector3.one);
    foreach (Renderer componentsInChild in go.GetComponentsInChildren(typeof (Renderer), true))
    {
      MeshFilter component = componentsInChild.GetComponent<MeshFilter>();
      if (!((Object) component == (Object) null) && componentsInChild.sharedMaterials != null && componentsInChild.sharedMaterials.Length != 0)
        this.m_EditorMeshes.Add(new NestedPrefab.EditorMesh()
        {
          mesh = component.sharedMesh,
          matrix = matrix4x4 * componentsInChild.transform.localToWorldMatrix,
          materials = new List<Material>((IEnumerable<Material>) componentsInChild.sharedMaterials)
        });
    }
    foreach (NestedPrefab componentsInChild in go.GetComponentsInChildren(typeof (NestedPrefab), true))
    {
      if (componentsInChild.enabled && componentsInChild.gameObject.activeSelf)
        this.SetupEditorMesh(componentsInChild.m_PrefabGameObject, matrix4x4 * componentsInChild.transform.localToWorldMatrix);
    }
  }

  private void LoadPrefab()
  {
    this.m_PrefabGameObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(this.m_Prefab), true, false);
    Quaternion localRotation = this.m_PrefabGameObject.transform.localRotation;
    Vector3 localScale = this.m_PrefabGameObject.transform.localScale;
    this.m_PrefabGameObject.transform.parent = this.transform;
    this.m_PrefabGameObject.transform.localPosition = Vector3.zero;
    this.m_PrefabGameObject.transform.localRotation = localRotation;
    this.m_PrefabGameObject.transform.localScale = localScale;
  }

  private struct EditorMesh
  {
    public Mesh mesh;
    public Matrix4x4 matrix;
    public List<Material> materials;
  }
}
