﻿// Decompiled with JetBrains decompiler
// Type: TGTGrandStand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class TGTGrandStand : MonoBehaviour
{
  private readonly string[] ANIMATION_CHEER = new string[3]{ "Cheer01", "Cheer02", "Cheer03" };
  private readonly string[] ANIMATION_OHNO = new string[2]{ "OhNo01", "OhNo02" };
  private const string ANIMATION_IDLE = "Idle";
  private const string ANIMATION_SCORE_CARD = "ScoreCard";
  private const float MIN_RANDOM_TIME_FACTOR = 0.05f;
  private const float MAX_RANDOM_TIME_FACTOR = 0.2f;
  private const float CHEER_ANIMATION_PLAY_TIME = 4f;
  private const float OHNO_ANIMATION_PLAY_TIME = 3.5f;
  private const float FRIENDLY_HERO_DAMAGE_WEIGHT_TRGGER = 7f;
  private const float OPPONENT_HERO_DAMAGE_WEIGHT_TRGGER = 10f;
  private const float FRIENDLY_LEGENDARY_SPAWN_MIN_COST_TRGGER = 6f;
  private const float OPPONENT_LEGENDARY_SPAWN_MIN_COST_TRGGER = 9f;
  private const float FRIENDLY_LEGENDARY_DEATH_MIN_COST_TRGGER = 6f;
  private const float OPPONENT_LEGENDARY_DEATH_MIN_COST_TRGGER = 9f;
  private const float FRIENDLY_MINION_DAMAGE_WEIGHT = 15f;
  private const float OPPONENT_MINION_DAMAGE_WEIGHT = 15f;
  private const float FRIENDLY_MINION_DEATH_WEIGHT = 15f;
  private const float OPPONENT_MINION_DEATH_WEIGHT = 15f;
  private const float FRIENDLY_MINION_SPAWN_WEIGHT = 10f;
  private const float OPPONENT_MINION_SPAWN_WEIGHT = 10f;
  private const float OPPONENT_HERO_DAMAGE_SCORE_CARD_TRIGGER = 15f;
  private const float OPPONENT_HERO_DAMAGE_SCORE_CARD_10S_TRIGGER = 20f;
  public GameObject m_HumanRoot;
  public GameObject m_OrcRoot;
  public GameObject m_KnightRoot;
  public Animator m_HumanAnimator;
  public Animator m_OrcAnimator;
  public Animator m_KnightAnimator;
  public GameObject m_HumanScoreCard;
  public GameObject m_OrcScoreCard;
  public GameObject m_KnightScoreCard;
  public UberText m_HumanScoreUberText;
  public UberText m_OrcScoreUberText;
  public UberText m_KnightScoreUberText;
  [CustomEditField(Sections = "Human Sounds", T = EditType.SOUND_PREFAB)]
  public string m_ClickHumanSound;
  [CustomEditField(Sections = "Human Sounds", T = EditType.SOUND_PREFAB)]
  public List<string> m_CheerHumanSounds;
  [CustomEditField(Sections = "Human Sounds", T = EditType.SOUND_PREFAB)]
  public List<string> m_OhNoHumanSounds;
  [CustomEditField(Sections = "Orc Sounds", T = EditType.SOUND_PREFAB)]
  public string m_ClickOrcSound;
  [CustomEditField(Sections = "Orc Sounds", T = EditType.SOUND_PREFAB)]
  public List<string> m_CheerOrcSounds;
  [CustomEditField(Sections = "Orc Sounds", T = EditType.SOUND_PREFAB)]
  public List<string> m_OhNoOrcSounds;
  [CustomEditField(Sections = "Knight Sounds", T = EditType.SOUND_PREFAB)]
  public string m_ClickKnightSound;
  [CustomEditField(Sections = "Knight Sounds", T = EditType.SOUND_PREFAB)]
  public List<string> m_CheerKnightSounds;
  [CustomEditField(Sections = "Knight Sounds", T = EditType.SOUND_PREFAB)]
  public List<string> m_OhNoKnightSounds;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_ScoreCardSound;
  private BoardEvents m_boardEvents;
  private bool m_isAnimating;
  private static TGTGrandStand s_instance;

  private void Awake()
  {
    TGTGrandStand.s_instance = this;
  }

  private void Start()
  {
    this.StartCoroutine(this.RegisterBoardEvents());
  }

  private void Update()
  {
    this.HandleClicks();
  }

  private void OnDestroy()
  {
    TGTGrandStand.s_instance = (TGTGrandStand) null;
  }

  public static TGTGrandStand Get()
  {
    return TGTGrandStand.s_instance;
  }

  private void HandleClicks()
  {
    if (UniversalInputManager.Get().GetMouseButtonDown(0) && this.IsOver(this.m_HumanRoot))
      this.HumanClick();
    if (UniversalInputManager.Get().GetMouseButtonDown(0) && this.IsOver(this.m_OrcRoot))
      this.OrcClick();
    if (!UniversalInputManager.Get().GetMouseButtonDown(0) || !this.IsOver(this.m_KnightRoot))
      return;
    this.KnightClick();
  }

  private void HumanClick()
  {
    this.m_HumanRoot.GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 0.0f, -30f);
    if (string.IsNullOrEmpty(this.m_ClickHumanSound))
      return;
    string name = FileUtils.GameAssetPathToName(this.m_ClickHumanSound);
    if (string.IsNullOrEmpty(name))
      return;
    SoundManager.Get().LoadAndPlay(name, this.m_HumanRoot);
  }

  private void OrcClick()
  {
    this.m_OrcRoot.GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 0.0f, -30f);
    if (string.IsNullOrEmpty(this.m_ClickOrcSound))
      return;
    string name = FileUtils.GameAssetPathToName(this.m_ClickOrcSound);
    if (string.IsNullOrEmpty(name))
      return;
    SoundManager.Get().LoadAndPlay(name, this.m_OrcRoot);
  }

  private void KnightClick()
  {
    this.m_KnightRoot.GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 0.0f, -30f);
    if (string.IsNullOrEmpty(this.m_ClickKnightSound))
      return;
    string name = FileUtils.GameAssetPathToName(this.m_ClickKnightSound);
    if (string.IsNullOrEmpty(name))
      return;
    SoundManager.Get().LoadAndPlay(name, this.m_KnightRoot);
  }

  [DebuggerHidden]
  private IEnumerator TestAnimations()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTGrandStand.\u003CTestAnimations\u003Ec__Iterator15() { \u003C\u003Ef__this = this };
  }

  public void PlayCheerAnimation()
  {
    int index1 = Random.Range(0, this.ANIMATION_CHEER.Length);
    this.PlayAnimation(this.m_HumanAnimator, this.ANIMATION_CHEER[index1], 4f);
    this.PlaySoundFromList(this.m_CheerHumanSounds, index1);
    int index2 = Random.Range(0, this.ANIMATION_CHEER.Length);
    this.PlayAnimation(this.m_OrcAnimator, this.ANIMATION_CHEER[index2], 4f);
    this.PlaySoundFromList(this.m_CheerOrcSounds, index2);
    int index3 = Random.Range(0, this.ANIMATION_CHEER.Length);
    this.PlayAnimation(this.m_KnightAnimator, this.ANIMATION_CHEER[index3], 4f);
    this.PlaySoundFromList(this.m_CheerKnightSounds, index3);
  }

  public void PlayOhNoAnimation()
  {
    int index1 = Random.Range(0, this.ANIMATION_OHNO.Length);
    this.PlayAnimation(this.m_HumanAnimator, this.ANIMATION_OHNO[index1], 3.5f);
    this.PlaySoundFromList(this.m_OhNoHumanSounds, index1);
    int index2 = Random.Range(0, this.ANIMATION_OHNO.Length);
    this.PlayAnimation(this.m_OrcAnimator, this.ANIMATION_OHNO[index2], 3.5f);
    this.PlaySoundFromList(this.m_OhNoOrcSounds, index2);
    int index3 = Random.Range(0, this.ANIMATION_OHNO.Length);
    this.PlayAnimation(this.m_KnightAnimator, this.ANIMATION_OHNO[index3], 3.5f);
    this.PlaySoundFromList(this.m_OhNoKnightSounds, index3);
  }

  public void PlayScoreCard(string humanScore, string orcScore, string knightScore)
  {
    this.m_HumanScoreUberText.Text = humanScore;
    this.m_OrcScoreUberText.Text = orcScore;
    this.m_KnightScoreUberText.Text = knightScore;
    this.m_HumanAnimator.SetTrigger("ScoreCard");
    this.m_OrcAnimator.SetTrigger("ScoreCard");
    this.m_KnightAnimator.SetTrigger("ScoreCard");
    this.PlaySound(this.m_ScoreCardSound);
  }

  private void PlaySoundFromList(List<string> soundList, int index)
  {
    if (soundList == null || soundList.Count == 0)
      return;
    if (index > soundList.Count)
      index = 0;
    this.PlaySound(soundList[index]);
  }

  private void PlaySound(string soundString)
  {
    if (string.IsNullOrEmpty(soundString))
      return;
    string name = FileUtils.GameAssetPathToName(soundString);
    if (string.IsNullOrEmpty(name))
      return;
    SoundManager.Get().LoadAndPlay(name, this.m_OrcRoot);
  }

  private void PlayAnimation(Animator animator, string animName, float time)
  {
    this.m_isAnimating = true;
    this.m_HumanScoreCard.SetActive(false);
    this.m_OrcScoreCard.SetActive(false);
    this.m_KnightScoreCard.SetActive(false);
    this.StartCoroutine(this.PlayAnimationRandomStart(animator, animName, time));
  }

  [DebuggerHidden]
  private IEnumerator PlayAnimationRandomStart(Animator animator, string animName, float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTGrandStand.\u003CPlayAnimationRandomStart\u003Ec__Iterator16() { animator = animator, animName = animName, time = time, \u003C\u0024\u003Eanimator = animator, \u003C\u0024\u003EanimName = animName, \u003C\u0024\u003Etime = time, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator ReturnToIdleAnimation(Animator animator, float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTGrandStand.\u003CReturnToIdleAnimation\u003Ec__Iterator17() { time = time, animator = animator, \u003C\u0024\u003Etime = time, \u003C\u0024\u003Eanimator = animator, \u003C\u003Ef__this = this };
  }

  private void Shake()
  {
    if (this.m_isAnimating)
      return;
    this.StartCoroutine(this.ShakeHuman());
    this.StartCoroutine(this.ShakeOrc());
    this.StartCoroutine(this.ShakeKnight());
  }

  [DebuggerHidden]
  private IEnumerator ShakeHuman()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTGrandStand.\u003CShakeHuman\u003Ec__Iterator18() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator ShakeOrc()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTGrandStand.\u003CShakeOrc\u003Ec__Iterator19() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator ShakeKnight()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTGrandStand.\u003CShakeKnight\u003Ec__Iterator1A() { \u003C\u003Ef__this = this };
  }

  private bool IsOver(GameObject go)
  {
    return (bool) ((Object) go) && InputUtil.IsPlayMakerMouseInputAllowed(go) && UniversalInputManager.Get().InputIsOver(go);
  }

  [DebuggerHidden]
  private IEnumerator RegisterBoardEvents()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TGTGrandStand.\u003CRegisterBoardEvents\u003Ec__Iterator1B() { \u003C\u003Ef__this = this };
  }

  private void FriendlyHeroDamage(float weight)
  {
    this.PlayOhNoAnimation();
  }

  private void OpponentHeroDamage(float weight)
  {
    if ((double) weight > 15.0)
    {
      if ((double) weight > 20.0)
        this.PlayScoreCard("10", "10", "10");
      else
        this.PlayScoreCard("10", Random.Range(7, 9).ToString(), Random.Range(8, 10).ToString());
    }
    else
      this.PlayCheerAnimation();
  }

  private void FriendlyLegendarySpawn(float weight)
  {
    this.PlayCheerAnimation();
  }

  private void OpponentLegendarySpawn(float weight)
  {
    this.PlayOhNoAnimation();
  }

  private void FriendlyLegendaryDeath(float weight)
  {
    this.PlayOhNoAnimation();
  }

  private void OpponentLegendaryDeath(float weight)
  {
    this.PlayCheerAnimation();
  }

  private void FriendlyMinionDamage(float weight)
  {
    this.PlayOhNoAnimation();
  }

  private void OpponentMinionDamage(float weight)
  {
    this.PlayCheerAnimation();
  }

  private void FriendlyMinionDeath(float weight)
  {
    this.PlayOhNoAnimation();
  }

  private void OpponentMinionDeath(float weight)
  {
    this.PlayCheerAnimation();
  }

  private void FriendlyMinionSpawn(float weight)
  {
    this.PlayCheerAnimation();
  }

  private void OpponentMinionSpawn(float weight)
  {
    this.PlayOhNoAnimation();
  }
}
