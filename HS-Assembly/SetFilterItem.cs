﻿// Decompiled with JetBrains decompiler
// Type: SetFilterItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SetFilterItem : PegUIElement
{
  public UberText m_uberText;
  public GameObject m_selectedGlow;
  public MeshRenderer m_icon;
  public GameObject m_mouseOverGlow;
  public GameObject m_pressedShadow;
  private bool m_isHeader;
  private bool m_isWild;
  private bool m_isAllStandard;
  private List<TAG_CARD_SET> m_cardSets;
  private float m_height;
  private SetFilterItem.ItemSelectedCallback m_callback;
  private bool m_isSelected;

  public bool IsHeader
  {
    get
    {
      return this.m_isHeader;
    }
    set
    {
      this.m_isHeader = value;
    }
  }

  public string Text
  {
    get
    {
      return this.m_uberText.Text;
    }
    set
    {
      this.m_uberText.Text = value;
    }
  }

  public bool IsWild
  {
    get
    {
      return this.m_isWild;
    }
    set
    {
      this.m_isWild = value;
    }
  }

  public bool IsAllStandard
  {
    get
    {
      return this.m_isAllStandard;
    }
    set
    {
      this.m_isAllStandard = value;
    }
  }

  public List<TAG_CARD_SET> CardSets
  {
    get
    {
      return this.m_cardSets;
    }
    set
    {
      this.m_cardSets = value;
    }
  }

  public float Height
  {
    get
    {
      return this.m_height;
    }
    set
    {
      this.m_height = value;
    }
  }

  public SetFilterItem.ItemSelectedCallback Callback
  {
    get
    {
      return this.m_callback;
    }
    set
    {
      this.m_callback = value;
    }
  }

  public Vector2? IconOffset
  {
    get
    {
      return new Vector2?(this.m_icon.material.GetTextureOffset("_MainTex"));
    }
    set
    {
      if (!value.HasValue)
      {
        this.m_icon.gameObject.SetActive(false);
      }
      else
      {
        this.m_icon.gameObject.SetActive(true);
        this.m_icon.material.SetTextureOffset("_MainTex", value.Value);
      }
    }
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    if (!((Object) this.m_mouseOverGlow != (Object) null))
      return;
    this.m_mouseOverGlow.SetActive(true);
    SoundManager.Get().LoadAndPlay("Small_Mouseover", this.gameObject);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    if ((Object) this.m_mouseOverGlow != (Object) null)
      this.m_mouseOverGlow.SetActive(false);
    if (this.m_isSelected || !((Object) this.m_pressedShadow != (Object) null))
      return;
    this.m_pressedShadow.SetActive(false);
  }

  public void SetSelected(bool selected)
  {
    this.m_selectedGlow.SetActive(selected);
    if ((Object) this.m_pressedShadow != (Object) null)
      this.m_pressedShadow.SetActive(selected);
    this.m_isSelected = selected;
  }

  protected override void OnPress()
  {
    if ((Object) this.m_pressedShadow != (Object) null)
      this.m_pressedShadow.SetActive(true);
    SoundManager.Get().LoadAndPlay("Small_Click");
  }

  protected override void OnRelease()
  {
    if (this.m_isSelected || !((Object) this.m_pressedShadow != (Object) null))
      return;
    this.m_pressedShadow.SetActive(false);
  }

  public delegate void ItemSelectedCallback(object data, bool isWild);
}
