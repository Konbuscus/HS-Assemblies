﻿// Decompiled with JetBrains decompiler
// Type: CollectionClassTab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class CollectionClassTab : PegUIElement
{
  private static readonly string CLASS_ICONS_TEXTURE_NAME = "ClassIcons";
  public static readonly float SELECT_TAB_ANIM_TIME = 0.2f;
  public Vector3 m_DeselectedLocalScale = new Vector3(0.44f, 0.44f, 0.44f);
  public Vector3 m_SelectedLocalScale = new Vector3(0.66f, 0.66f, 0.66f);
  public float m_SelectedLocalYPos = 0.1259841f;
  private bool m_shouldBeVisible = true;
  private bool m_isVisible = true;
  public GameObject m_glowMesh;
  public GameObject m_newItemCount;
  public UberText m_newItemCountText;
  public CollectionManagerDisplay.ViewMode m_tabViewMode;
  public float m_DeselectedLocalYPos;
  private TAG_CLASS m_classTag;
  private int m_numNewItems;
  private bool m_selected;
  private Vector3 m_targetLocalPos;
  private bool m_showLargeTab;

  public void Init(TAG_CLASS classTag)
  {
    this.m_classTag = classTag;
    this.SetClassIconsTextureOffset(this.gameObject.GetComponent<Renderer>());
    if ((Object) this.m_glowMesh != (Object) null)
      this.SetClassIconsTextureOffset(this.m_glowMesh.GetComponent<Renderer>());
    this.SetGlowActive(false);
    this.UpdateNewItemCount(0);
  }

  public TAG_CLASS GetClass()
  {
    return this.m_classTag;
  }

  public void SetGlowActive(bool active)
  {
    if (this.m_selected)
      active = true;
    if (!((Object) this.m_glowMesh != (Object) null))
      return;
    this.m_glowMesh.SetActive(active);
  }

  public void SetSelected(bool selected)
  {
    if (this.m_selected == selected)
      return;
    this.m_selected = selected;
    this.SetGlowActive(this.m_selected);
  }

  public void UpdateNewItemCount(int numNewItems)
  {
    this.m_numNewItems = numNewItems;
    this.UpdateNewItemCountVisuals();
  }

  public void SetTargetLocalPosition(Vector3 targetLocalPos)
  {
    this.m_targetLocalPos = targetLocalPos;
  }

  public void SetIsVisible(bool isVisible)
  {
    this.m_isVisible = isVisible;
    this.SetEnabled(this.m_isVisible);
  }

  public bool IsVisible()
  {
    return this.m_isVisible;
  }

  public void SetTargetVisibility(bool visible)
  {
    this.m_shouldBeVisible = visible;
  }

  public bool ShouldBeVisible()
  {
    return this.m_shouldBeVisible;
  }

  public bool WillSlide()
  {
    return (double) Mathf.Abs(this.m_targetLocalPos.x - this.transform.localPosition.x) > 0.0500000007450581;
  }

  public void AnimateToTargetPosition(float animationTime, iTween.EaseType easeType)
  {
    Hashtable args = iTween.Hash((object) "position", (object) this.m_targetLocalPos, (object) "isLocal", (object) true, (object) "time", (object) animationTime, (object) "easetype", (object) easeType, (object) "name", (object) "position", (object) "oncomplete", (object) "OnMovedToTargetPos", (object) "onompletetarget", (object) this.gameObject);
    iTween.StopByName(this.gameObject, "position");
    iTween.MoveTo(this.gameObject, args);
  }

  public void SetLargeTab(bool large)
  {
    if (large == this.m_showLargeTab)
      return;
    if (large)
    {
      Vector3 localPosition = this.transform.localPosition;
      localPosition.y = this.m_SelectedLocalYPos;
      this.transform.localPosition = localPosition;
      iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) this.m_SelectedLocalScale, (object) "time", (object) CollectionClassTab.SELECT_TAB_ANIM_TIME, (object) "name", (object) "scale"));
      SoundManager.Get().LoadAndPlay("class_tab_click", this.gameObject);
    }
    else
    {
      Vector3 localPosition = this.transform.localPosition;
      localPosition.y = this.m_DeselectedLocalYPos;
      this.transform.localPosition = localPosition;
      iTween.StopByName(this.gameObject, "scale");
      this.transform.localScale = this.m_DeselectedLocalScale;
    }
    this.m_showLargeTab = large;
  }

  private static Vector2 GetTextureOffset(TAG_CLASS classTag)
  {
    if (CollectionPageManager.s_classTextureOffsets.ContainsKey(classTag))
      return CollectionPageManager.s_classTextureOffsets[classTag];
    Debug.LogWarning((object) string.Format("CollectionClassTab.GetTextureOffset(): No class texture offsets exist for class {0}", (object) classTag));
    return Vector2.zero;
  }

  private void SetClassIconsTextureOffset(Renderer renderer)
  {
    if ((Object) renderer == (Object) null)
      return;
    Vector2 textureOffset = CollectionClassTab.GetTextureOffset(this.m_classTag);
    foreach (Material material in renderer.materials)
    {
      if (!((Object) material.mainTexture == (Object) null) && material.mainTexture.name.Contains(CollectionClassTab.CLASS_ICONS_TEXTURE_NAME))
        material.mainTextureOffset = textureOffset;
    }
  }

  private void UpdateNewItemCountVisuals()
  {
    if ((Object) this.m_newItemCountText != (Object) null)
      this.m_newItemCountText.Text = GameStrings.Format("GLUE_COLLECTION_NEW_CARD_CALLOUT", (object) this.m_numNewItems);
    if (!((Object) this.m_newItemCount != (Object) null))
      return;
    this.m_newItemCount.SetActive(this.m_numNewItems > 0);
  }

  private void OnMovedToTargetPos()
  {
    if (this.m_showLargeTab)
      return;
    Vector3 localPosition = this.transform.localPosition;
    localPosition.y = this.m_DeselectedLocalYPos;
    this.transform.localPosition = localPosition;
  }
}
