﻿// Decompiled with JetBrains decompiler
// Type: TournamentScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class TournamentScene : PlayGameScene
{
  private static TournamentScene s_instance;

  protected override void Awake()
  {
    base.Awake();
    TournamentScene.s_instance = this;
  }

  private void OnDestroy()
  {
    TournamentScene.s_instance = (TournamentScene) null;
  }

  public static TournamentScene Get()
  {
    return TournamentScene.s_instance;
  }

  public override string GetScreenName()
  {
    return "Tournament";
  }

  public override void Unload()
  {
    base.Unload();
    TournamentDisplay.Get().Unload();
  }
}
