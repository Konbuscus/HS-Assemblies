﻿// Decompiled with JetBrains decompiler
// Type: KeywordTextDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class KeywordTextDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private int m_Tag;
  [SerializeField]
  private string m_Name;
  [SerializeField]
  private string m_Text;
  [SerializeField]
  private string m_RefText;
  [SerializeField]
  private string m_CollectionText;

  [DbfField("NOTE_DESC", "name of keyword's tag")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("TAG", "ID if keyword's tag")]
  public int Tag
  {
    get
    {
      return this.m_Tag;
    }
  }

  [DbfField("NAME", "Game string for keyword's name")]
  public string Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("TEXT", "Game string for keyword's tooltip description")]
  public string Text
  {
    get
    {
      return this.m_Text;
    }
  }

  [DbfField("REF_TEXT", "Game string for reference keyword's tooltip description")]
  public string RefText
  {
    get
    {
      return this.m_RefText;
    }
  }

  [DbfField("COLLECTION_TEXT", "Override game string for keyword's tooltip description in Collection Manager")]
  public string CollectionText
  {
    get
    {
      return this.m_CollectionText;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    KeywordTextDbfAsset keywordTextDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (KeywordTextDbfAsset)) as KeywordTextDbfAsset;
    if ((UnityEngine.Object) keywordTextDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("KeywordTextDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < keywordTextDbfAsset.Records.Count; ++index)
      keywordTextDbfAsset.Records[index].StripUnusedLocales();
    records = (object) keywordTextDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetTag(int v)
  {
    this.m_Tag = v;
  }

  public void SetName(string v)
  {
    this.m_Name = v;
  }

  public void SetText(string v)
  {
    this.m_Text = v;
  }

  public void SetRefText(string v)
  {
    this.m_RefText = v;
  }

  public void SetCollectionText(string v)
  {
    this.m_CollectionText = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (KeywordTextDbfRecord.\u003C\u003Ef__switch\u0024map47 == null)
      {
        // ISSUE: reference to a compiler-generated field
        KeywordTextDbfRecord.\u003C\u003Ef__switch\u0024map47 = new Dictionary<string, int>(7)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TAG",
            2
          },
          {
            "NAME",
            3
          },
          {
            "TEXT",
            4
          },
          {
            "REF_TEXT",
            5
          },
          {
            "COLLECTION_TEXT",
            6
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (KeywordTextDbfRecord.\u003C\u003Ef__switch\u0024map47.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Tag;
          case 3:
            return (object) this.Name;
          case 4:
            return (object) this.Text;
          case 5:
            return (object) this.RefText;
          case 6:
            return (object) this.CollectionText;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (KeywordTextDbfRecord.\u003C\u003Ef__switch\u0024map48 == null)
    {
      // ISSUE: reference to a compiler-generated field
      KeywordTextDbfRecord.\u003C\u003Ef__switch\u0024map48 = new Dictionary<string, int>(7)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "TAG",
          2
        },
        {
          "NAME",
          3
        },
        {
          "TEXT",
          4
        },
        {
          "REF_TEXT",
          5
        },
        {
          "COLLECTION_TEXT",
          6
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!KeywordTextDbfRecord.\u003C\u003Ef__switch\u0024map48.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetTag((int) val);
        break;
      case 3:
        this.SetName((string) val);
        break;
      case 4:
        this.SetText((string) val);
        break;
      case 5:
        this.SetRefText((string) val);
        break;
      case 6:
        this.SetCollectionText((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (KeywordTextDbfRecord.\u003C\u003Ef__switch\u0024map49 == null)
      {
        // ISSUE: reference to a compiler-generated field
        KeywordTextDbfRecord.\u003C\u003Ef__switch\u0024map49 = new Dictionary<string, int>(7)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "TAG",
            2
          },
          {
            "NAME",
            3
          },
          {
            "TEXT",
            4
          },
          {
            "REF_TEXT",
            5
          },
          {
            "COLLECTION_TEXT",
            6
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (KeywordTextDbfRecord.\u003C\u003Ef__switch\u0024map49.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (int);
          case 3:
            return typeof (string);
          case 4:
            return typeof (string);
          case 5:
            return typeof (string);
          case 6:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
