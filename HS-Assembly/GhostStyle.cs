﻿// Decompiled with JetBrains decompiler
// Type: GhostStyle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class GhostStyle
{
  public Material m_GhostMaterial;
  public Material m_GhostCardMaterial;
  public Material m_GhostCardCollectionMaterial;
  public Material m_GhostBigCardMaterial;
  public Material m_GhostMaterialTransparent;
  public Material m_GhostMaterialMod2x;
  public Material m_GhostMaterialGlowPlane;
  public Material m_GhostMaterialAbilityGlowPlane;
}
