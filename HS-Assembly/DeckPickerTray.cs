﻿// Decompiled with JetBrains decompiler
// Type: DeckPickerTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class DeckPickerTray
{
  private static DeckPickerTray s_instance;
  private bool m_registeredHandlers;

  public static DeckPickerTray Get()
  {
    if (DeckPickerTray.s_instance == null)
      DeckPickerTray.s_instance = new DeckPickerTray();
    return DeckPickerTray.s_instance;
  }

  public void RegisterHandlers()
  {
    if (this.m_registeredHandlers)
      return;
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    this.m_registeredHandlers = true;
  }

  public void UnregisterHandlers()
  {
    if (!this.m_registeredHandlers)
      return;
    GameMgr.Get().UnregisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    this.m_registeredHandlers = false;
  }

  public void Unload()
  {
    this.UnregisterHandlers();
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_CANCELED:
        DeckPickerTrayDisplay.Get().HandleGameStartupFailure();
        break;
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
        DeckPickerTrayDisplay.Get().HandleGameStartupFailure();
        break;
      case FindGameState.SERVER_GAME_STARTED:
        DeckPickerTrayDisplay.Get().OnServerGameStarted();
        break;
      case FindGameState.SERVER_GAME_CANCELED:
        DeckPickerTrayDisplay.Get().OnServerGameCanceled();
        break;
    }
    return false;
  }
}
