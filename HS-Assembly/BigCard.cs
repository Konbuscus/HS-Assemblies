﻿// Decompiled with JetBrains decompiler
// Type: BigCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class BigCard : MonoBehaviour
{
  private static readonly Vector3 INVISIBLE_SCALE = new Vector3(0.0001f, 0.0001f, 0.0001f);
  public int m_RenderQueueEnchantmentBanner = 1;
  public int m_RenderQueueEnchantmentPanel = 2;
  private Pool<BigCardEnchantmentPanel> m_enchantmentPool = new Pool<BigCardEnchantmentPanel>();
  public BigCardEnchantmentPanel m_EnchantmentPanelPrefab;
  public GameObject m_EnchantmentBanner;
  public GameObject m_EnchantmentBannerBottom;
  public BigCard.LayoutData m_LayoutData;
  public BigCard.SecretLayoutData m_SecretLayoutData;
  private static BigCard s_instance;
  private Card m_card;
  private Actor m_bigCardActor;
  private List<Actor> m_phoneSecretActors;
  private float m_initialBannerHeight;
  private Vector3 m_initialBannerScale;
  private Vector3 m_initialBannerBottomScale;
  private readonly PlatformDependentValue<float> PLATFORM_SCALING_FACTOR;
  private readonly PlatformDependentValue<float> ENCHANTMENT_SCALING_FACTOR;

  public BigCard()
  {
    this.PLATFORM_SCALING_FACTOR = new PlatformDependentValue<float>(PlatformCategory.Screen)
    {
      PC = 1f,
      Tablet = 1f,
      Phone = 1.3f,
      MiniTablet = 1f
    };
    this.ENCHANTMENT_SCALING_FACTOR = new PlatformDependentValue<float>(PlatformCategory.Screen)
    {
      PC = 1f,
      Tablet = 1f,
      Phone = 1.5f,
      MiniTablet = 1f
    };
  }

  private void Awake()
  {
    BigCard.s_instance = this;
    this.m_initialBannerHeight = this.m_EnchantmentBanner.GetComponent<Renderer>().bounds.size.z;
    this.m_initialBannerScale = this.m_EnchantmentBanner.transform.localScale;
    this.m_initialBannerBottomScale = this.m_EnchantmentBannerBottom.transform.localScale;
    this.m_enchantmentPool.SetCreateItemCallback(new Pool<BigCardEnchantmentPanel>.CreateItemCallback(this.CreateEnchantmentPanel));
    this.m_enchantmentPool.SetDestroyItemCallback(new Pool<BigCardEnchantmentPanel>.DestroyItemCallback(this.DestroyEnchantmentPanel));
    this.m_enchantmentPool.SetExtensionCount(1);
    this.m_enchantmentPool.SetMaxReleasedItemCount(2);
    this.ResetEnchantments();
  }

  private void OnDestroy()
  {
    BigCard.s_instance = (BigCard) null;
  }

  public static BigCard Get()
  {
    return BigCard.s_instance;
  }

  public Card GetCard()
  {
    return this.m_card;
  }

  public void Show(Card card)
  {
    this.m_card = card;
    if (GameState.Get() != null && !GameState.Get().GetGameEntity().NotifyOfCardTooltipDisplayShow(card))
      return;
    Zone zone = card.GetZone();
    if ((bool) UniversalInputManager.UsePhoneUI && zone is ZoneSecret)
      this.LoadAndDisplayTooltipPhoneSecrets();
    else
      this.LoadAndDisplayBigCard();
  }

  public void Hide()
  {
    if (GameState.Get() != null)
      GameState.Get().GetGameEntity().NotifyOfCardTooltipDisplayHide(this.m_card);
    this.HideBigCard();
    this.HideTooltipPhoneSecrets();
    this.m_card = (Card) null;
  }

  public bool Hide(Card card)
  {
    if ((UnityEngine.Object) this.m_card != (UnityEngine.Object) card)
      return false;
    this.Hide();
    return true;
  }

  public void ShowSecretDeaths(Map<Player, DeadSecretGroup> deadSecretMap)
  {
    if (deadSecretMap == null || deadSecretMap.Count == 0)
      return;
    int num = 0;
    using (Map<Player, DeadSecretGroup>.ValueCollection.Enumerator enumerator = deadSecretMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeadSecretGroup current = enumerator.Current;
        Card mainCard = current.GetMainCard();
        List<Card> cards = current.GetCards();
        List<Actor> actors = new List<Actor>();
        for (int index = 0; index < cards.Count; ++index)
        {
          Actor actor = this.LoadPhoneSecret(cards[index]);
          actors.Add(actor);
        }
        this.DisplayPhoneSecrets(mainCard, actors, true);
        ++num;
      }
    }
  }

  private void LoadAndDisplayBigCard()
  {
    if ((bool) ((UnityEngine.Object) this.m_bigCardActor))
      this.m_bigCardActor.Destroy();
    this.m_bigCardActor = AssetLoader.Get().LoadActor(ActorNames.GetBigCardActor(this.m_card.GetEntity()), false, false).GetComponent<Actor>();
    this.SetupActor(this.m_card, this.m_bigCardActor);
    this.DisplayBigCard();
  }

  private void HideBigCard()
  {
    if (!(bool) ((UnityEngine.Object) this.m_bigCardActor))
      return;
    this.ResetEnchantments();
    iTween.Stop(this.m_bigCardActor.gameObject);
    this.m_bigCardActor.Destroy();
    this.m_bigCardActor = (Actor) null;
    TooltipPanelManager.Get().HideKeywordHelp();
  }

  private void DisplayBigCard()
  {
    Entity entity = this.m_card.GetEntity();
    bool flag = entity.GetController().IsFriendlySide();
    Zone zone = this.m_card.GetZone();
    Bounds bounds = this.m_bigCardActor.GetMeshRenderer().bounds;
    Vector3 position1 = this.m_card.GetActor().transform.position;
    Vector3 vector3_1 = new Vector3(0.0f, 0.0f, 0.0f);
    Vector3 scale = new Vector3(1.1f, 1.1f, 1.1f);
    float? overrideScale = new float?();
    if (zone is ZoneHero)
      vector3_1 = !flag ? new Vector3(0.0f, 4f, (float) (-(double) bounds.size.z * 0.699999988079071)) : new Vector3(0.0f, 4f, 0.0f);
    else if (zone is ZoneHeroPower)
    {
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        scale = new Vector3(1.3f, 1f, 1.3f);
        vector3_1 = !flag ? new Vector3(-3.5f, 8f, -3.35f) : new Vector3(-3.5f, 8f, 3.5f);
      }
      else
        vector3_1 = !flag ? new Vector3(0.0f, 4f, -2.6f) : new Vector3(0.0f, 4f, 2.69f);
      overrideScale = new float?(0.6f);
    }
    else if (zone is ZoneWeapon)
    {
      scale = new Vector3(1.65f, 1.65f, 1.65f);
      vector3_1 = !flag ? new Vector3(-1.57f, 0.0f, -1f) : new Vector3(0.0f, 0.0f, bounds.size.z * 0.9f);
      scale *= (float) this.PLATFORM_SCALING_FACTOR;
    }
    else if (zone is ZoneSecret)
    {
      scale = new Vector3(1.65f, 1.65f, 1.65f);
      vector3_1 = new Vector3(bounds.size.x + 0.3f, 0.0f, 0.0f);
    }
    else if (zone is ZoneHand)
    {
      vector3_1 = new Vector3(bounds.size.x * 0.7f, 4f, (float) (-(double) bounds.size.z * 0.800000011920929));
      scale = new Vector3(1.65f, 1.65f, 1.65f);
    }
    else
    {
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        scale = new Vector3(2f, 2f, 2f);
        vector3_1 = !this.ShowBigCardOnRight() ? new Vector3((float) (-(double) bounds.size.x - 2.20000004768372), 0.0f, 0.0f) : new Vector3(bounds.size.x + 2.2f, 0.0f, 0.0f);
      }
      else
      {
        scale = new Vector3(1.65f, 1.65f, 1.65f);
        vector3_1 = !this.ShowBigCardOnRight() ? new Vector3((float) (-(double) bounds.size.x - 0.699999988079071), 0.0f, 0.0f) : new Vector3(bounds.size.x + 0.7f, 0.0f, 0.0f);
      }
      if (zone is ZonePlay)
      {
        vector3_1 += new Vector3(0.0f, 0.1f, 0.0f);
        scale *= (float) this.PLATFORM_SCALING_FACTOR;
      }
    }
    Vector3 vector3_2 = new Vector3(0.02f, 0.02f, 0.02f);
    Vector3 vector3_3 = position1 + vector3_1 + vector3_2;
    Vector3 vector3_4 = new Vector3(1f, 1f, 1f);
    Transform parent = this.m_bigCardActor.transform.parent;
    this.m_bigCardActor.transform.localScale = scale;
    this.m_bigCardActor.transform.position = vector3_3;
    this.m_bigCardActor.transform.parent = (Transform) null;
    if (zone is ZoneHand)
    {
      this.m_bigCardActor.SetEntity(entity);
      this.m_bigCardActor.UpdateTextComponents(entity);
    }
    else
    {
      this.UpdateEnchantments();
      if ((bool) UniversalInputManager.UsePhoneUI && this.m_EnchantmentBanner.activeInHierarchy)
      {
        float num = this.m_enchantmentPool.GetActiveList().Count <= 1 ? 0.85f : 0.75f;
        scale *= num;
        this.m_bigCardActor.transform.localScale = scale;
      }
    }
    this.FitInsideScreen();
    this.m_bigCardActor.transform.parent = parent;
    this.m_bigCardActor.transform.localScale = vector3_4;
    Vector3 position2 = this.m_bigCardActor.transform.position;
    this.m_bigCardActor.transform.position -= vector3_2;
    BigCard.KeywordArgs keywordArgs1 = new BigCard.KeywordArgs();
    keywordArgs1.card = this.m_card;
    keywordArgs1.actor = this.m_bigCardActor;
    keywordArgs1.showOnRight = this.ShowBigCardOnRight();
    if (zone is ZoneHand)
    {
      iTween.ScaleTo(this.m_bigCardActor.gameObject, iTween.Hash((object) "scale", (object) scale, (object) "time", (object) this.m_LayoutData.m_ScaleSec, (object) "oncompleteparams", (object) keywordArgs1, (object) "oncomplete", (object) (Action<object>) (obj =>
      {
        BigCard.KeywordArgs keywordArgs2 = (BigCard.KeywordArgs) obj;
        TooltipPanelManager.Get().UpdateKeywordHelp(keywordArgs2.card, keywordArgs2.actor, keywordArgs2.showOnRight, new float?(), new Vector3?());
      })));
    }
    else
    {
      iTween.ScaleTo(this.m_bigCardActor.gameObject, scale, this.m_LayoutData.m_ScaleSec);
      TooltipPanelManager.Get().UpdateKeywordHelp(keywordArgs1.card, keywordArgs1.actor, keywordArgs1.showOnRight, overrideScale, new Vector3?());
    }
    iTween.MoveTo(this.m_bigCardActor.gameObject, position2, this.m_LayoutData.m_DriftSec);
    this.m_bigCardActor.transform.rotation = Quaternion.identity;
    this.m_bigCardActor.Show();
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      Vector3? overrideOffset = new Vector3?();
      if (this.m_card.GetEntity().IsHeroPower())
        overrideOffset = new Vector3?(new Vector3(0.6f, 0.0f, -1.3f));
      TooltipPanelManager.Get().UpdateKeywordHelp(this.m_card, this.m_bigCardActor, this.ShowKeywordOnRight(), overrideScale, overrideOffset);
    }
    if (!entity.IsSilenced())
      return;
    this.m_bigCardActor.ActivateSpellBirthState(SpellType.SILENCE);
  }

  private bool ShowBigCardOnRight()
  {
    if (UniversalInputManager.Get().IsTouchMode())
      return this.ShowBigCardOnRightTouch();
    return this.ShowBigCardOnRightMouse();
  }

  private bool ShowBigCardOnRightMouse()
  {
    if (this.m_card.GetEntity().IsHero() || this.m_card.GetEntity().IsHeroPower() || this.m_card.GetEntity().IsSecret())
      return true;
    if (this.m_card.GetEntity().GetCardId() == "TU4c_007")
      return false;
    ZonePlay zone = this.m_card.GetZone() as ZonePlay;
    if ((UnityEngine.Object) zone != (UnityEngine.Object) null)
      return (double) this.m_card.GetActor().GetMeshRenderer().bounds.center.x < (double) zone.GetComponent<BoxCollider>().bounds.center.x + 2.5;
    return true;
  }

  private bool ShowBigCardOnRightTouch()
  {
    if (this.m_card.GetEntity().IsHero() || this.m_card.GetEntity().IsHeroPower() || (this.m_card.GetEntity().IsSecret() || this.m_card.GetEntity().GetCardId() == "TU4c_007"))
      return false;
    ZonePlay zone = this.m_card.GetZone() as ZonePlay;
    if (!((UnityEngine.Object) zone != (UnityEngine.Object) null))
      return false;
    float num = !(bool) UniversalInputManager.UsePhoneUI ? -2.5f : 0.0f;
    return (double) this.m_card.GetActor().GetMeshRenderer().bounds.center.x < (double) zone.GetComponent<BoxCollider>().bounds.center.x + (double) num;
  }

  private bool ShowKeywordOnRight()
  {
    if (this.m_card.GetEntity().IsHeroPower() || this.m_card.GetEntity().IsWeapon())
      return true;
    if (this.m_card.GetEntity().IsHero() || this.m_card.GetEntity().IsSecret() || this.m_card.GetEntity().GetCardId() == "TU4c_007")
      return false;
    ZonePlay zone = this.m_card.GetZone() as ZonePlay;
    if (!((UnityEngine.Object) zone != (UnityEngine.Object) null))
      return false;
    if ((bool) UniversalInputManager.UsePhoneUI)
      return (double) this.m_card.GetActor().GetMeshRenderer().bounds.center.x > (double) zone.GetComponent<BoxCollider>().bounds.center.x;
    return (double) this.m_card.GetActor().GetMeshRenderer().bounds.center.x < (double) zone.GetComponent<BoxCollider>().bounds.center.x + 0.0299999993294477;
  }

  private void UpdateEnchantments()
  {
    this.ResetEnchantments();
    GameObject bone = this.m_bigCardActor.FindBone("EnchantmentTooltip");
    if ((UnityEngine.Object) bone == (UnityEngine.Object) null)
      return;
    List<Entity> enchantments = this.m_card.GetEntity().GetEnchantments();
    List<BigCardEnchantmentPanel> activeList = this.m_enchantmentPool.GetActiveList();
    int count1 = enchantments.Count;
    int count2 = activeList.Count;
    int count3 = count1 - count2;
    if (count3 > 0)
      this.m_enchantmentPool.AcquireBatch(count3);
    else if (count3 < 0)
      this.m_enchantmentPool.ReleaseBatch(count1, -count3);
    if (count1 == 0)
      return;
    for (int index = 0; index < activeList.Count; ++index)
      activeList[index].SetEnchantment(enchantments[index]);
    this.LayoutEnchantments(bone);
    SceneUtils.SetLayer(bone, GameLayer.Tooltip);
  }

  private void ResetEnchantments()
  {
    this.m_EnchantmentBanner.SetActive(false);
    this.m_EnchantmentBannerBottom.SetActive(false);
    this.m_EnchantmentBanner.transform.parent = this.transform;
    this.m_EnchantmentBannerBottom.transform.parent = this.transform;
    using (List<BigCardEnchantmentPanel>.Enumerator enumerator = this.m_enchantmentPool.GetActiveList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BigCardEnchantmentPanel current = enumerator.Current;
        current.transform.parent = this.transform;
        current.ResetScale();
        current.Hide();
      }
    }
  }

  private void LayoutEnchantments(GameObject bone)
  {
    float num = 0.1f;
    List<BigCardEnchantmentPanel> activeList = this.m_enchantmentPool.GetActiveList();
    BigCardEnchantmentPanel enchantmentPanel1 = (BigCardEnchantmentPanel) null;
    for (int index = 0; index < activeList.Count; ++index)
    {
      BigCardEnchantmentPanel enchantmentPanel2 = activeList[index];
      enchantmentPanel2.Show();
      enchantmentPanel2.transform.localScale *= (float) this.PLATFORM_SCALING_FACTOR * (float) this.ENCHANTMENT_SCALING_FACTOR;
      if (index == 0)
        TransformUtil.SetPoint(enchantmentPanel2.gameObject, new Vector3(0.5f, 0.0f, 1f), this.m_bigCardActor.GetMeshRenderer().gameObject, new Vector3(0.5f, 0.0f, 0.0f), new Vector3(0.01f, 0.01f, 0.0f));
      else
        TransformUtil.SetPoint(enchantmentPanel2.gameObject, new Vector3(0.0f, 0.0f, 1f), enchantmentPanel1.gameObject, new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f));
      enchantmentPanel1 = enchantmentPanel2;
      enchantmentPanel2.transform.parent = bone.transform;
      float height = enchantmentPanel2.GetHeight();
      num += height;
    }
    this.m_EnchantmentBanner.SetActive(true);
    this.m_EnchantmentBannerBottom.SetActive(true);
    this.m_EnchantmentBannerBottom.transform.localScale = this.m_initialBannerBottomScale * (float) this.PLATFORM_SCALING_FACTOR * (float) this.ENCHANTMENT_SCALING_FACTOR;
    this.m_EnchantmentBanner.transform.localEulerAngles = new Vector3(0.0f, 180f, 0.0f);
    this.m_EnchantmentBannerBottom.transform.localEulerAngles = new Vector3(0.0f, 180f, 0.0f);
    TransformUtil.SetPoint(this.m_EnchantmentBanner, new Vector3(0.5f, 0.0f, 1f), this.m_bigCardActor.GetMeshRenderer().gameObject, new Vector3(0.5f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.2f));
    this.m_EnchantmentBanner.transform.localScale = this.m_initialBannerScale * (float) this.PLATFORM_SCALING_FACTOR * (float) this.ENCHANTMENT_SCALING_FACTOR;
    TransformUtil.SetLocalScaleZ(this.m_EnchantmentBanner.gameObject, num / this.m_initialBannerHeight / this.m_initialBannerScale.z);
    this.m_EnchantmentBanner.transform.parent = bone.transform;
    TransformUtil.SetPoint(this.m_EnchantmentBannerBottom, Anchor.FRONT, this.m_EnchantmentBanner, Anchor.BACK);
    this.m_EnchantmentBannerBottom.transform.parent = bone.transform;
  }

  private void FitInsideScreen()
  {
    this.FitInsideScreenBottom();
    this.FitInsideScreenTop();
  }

  private bool FitInsideScreenBottom()
  {
    Bounds bounds = !this.m_EnchantmentBanner.activeInHierarchy ? this.m_bigCardActor.GetMeshRenderer().bounds : this.m_EnchantmentBannerBottom.GetComponent<Renderer>().bounds;
    Vector3 center = bounds.center;
    if ((bool) UniversalInputManager.UsePhoneUI)
      center.z -= 0.4f;
    Vector3 origin = new Vector3(center.x, center.y, center.z - bounds.extents.z);
    Ray ray = new Ray(origin, origin - center);
    Plane bottomPlane = CameraUtils.CreateBottomPlane(CameraUtils.FindFirstByLayer(GameLayer.Tooltip));
    float enter = 0.0f;
    if (bottomPlane.Raycast(ray, out enter) || Mathf.Approximately(enter, 0.0f))
      return false;
    TransformUtil.SetPosZ(this.m_bigCardActor.gameObject, this.m_bigCardActor.transform.position.z - enter);
    return true;
  }

  private bool FitInsideScreenTop()
  {
    Bounds bounds = this.m_bigCardActor.GetMeshRenderer().bounds;
    Vector3 center = bounds.center;
    if ((bool) UniversalInputManager.UsePhoneUI && !(this.m_card.GetZone() is ZoneHeroPower))
      ++center.z;
    Vector3 origin = new Vector3(center.x, center.y, center.z + bounds.extents.z);
    Ray ray = new Ray(origin, origin - center);
    Plane topPlane = CameraUtils.CreateTopPlane(CameraUtils.FindFirstByLayer(GameLayer.Tooltip));
    float enter = 0.0f;
    if (topPlane.Raycast(ray, out enter) || Mathf.Approximately(enter, 0.0f))
      return false;
    TransformUtil.SetPosZ(this.m_bigCardActor.gameObject, this.m_bigCardActor.transform.position.z + enter);
    return true;
  }

  private BigCardEnchantmentPanel CreateEnchantmentPanel(int index)
  {
    BigCardEnchantmentPanel enchantmentPanel = UnityEngine.Object.Instantiate<BigCardEnchantmentPanel>(this.m_EnchantmentPanelPrefab);
    enchantmentPanel.name = string.Format("{0}{1}", (object) typeof (BigCardEnchantmentPanel).ToString(), (object) index);
    SceneUtils.SetRenderQueue(enchantmentPanel.gameObject, this.m_RenderQueueEnchantmentPanel, false);
    return enchantmentPanel;
  }

  private void DestroyEnchantmentPanel(BigCardEnchantmentPanel panel)
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) panel.gameObject);
  }

  private void LoadAndDisplayTooltipPhoneSecrets()
  {
    if (this.m_phoneSecretActors == null)
    {
      this.m_phoneSecretActors = new List<Actor>();
    }
    else
    {
      using (List<Actor>.Enumerator enumerator = this.m_phoneSecretActors.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Destroy();
      }
      this.m_phoneSecretActors.Clear();
    }
    List<Card> cards = this.m_card.GetZone().GetCards();
    for (int index = 0; index < cards.Count; ++index)
      this.m_phoneSecretActors.Add(this.LoadPhoneSecret(cards[index]));
    this.DisplayPhoneSecrets(this.m_card, this.m_phoneSecretActors, false);
  }

  private void HideTooltipPhoneSecrets()
  {
    if (this.m_phoneSecretActors == null)
      return;
    using (List<Actor>.Enumerator enumerator = this.m_phoneSecretActors.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.HidePhoneSecret(enumerator.Current);
    }
    this.m_phoneSecretActors.Clear();
  }

  private Actor LoadPhoneSecret(Card card)
  {
    Actor component = AssetLoader.Get().LoadActor(ActorNames.GetBigCardActor(card.GetEntity()), false, false).GetComponent<Actor>();
    this.SetupActor(card, component);
    return component;
  }

  private void DisplayPhoneSecrets(Card mainCard, List<Actor> actors, bool showDeath)
  {
    Vector3 initialOffset;
    Vector3 spacing;
    Vector3 drift;
    this.DetermineSecretLayoutOffsets(mainCard, actors, out initialOffset, out spacing, out drift);
    bool flag1 = GeneralUtils.IsOdd(actors.Count);
    Actor actor1 = mainCard.GetActor();
    Vector3 vector3_1 = actor1.transform.position + initialOffset;
    for (int n = 0; n < actors.Count; ++n)
    {
      Actor actor2 = actors[n];
      Vector3 vector3_2;
      if (n == 0 && flag1)
      {
        vector3_2 = vector3_1;
      }
      else
      {
        bool flag2 = GeneralUtils.IsOdd(n);
        bool flag3 = flag1 == flag2;
        float num1 = !flag1 ? Mathf.Floor(0.5f * (float) n) : Mathf.Ceil(0.5f * (float) n);
        float num2 = num1 * spacing.x;
        if (!flag1)
          num2 += 0.5f * spacing.x;
        if (flag3)
          num2 = -num2;
        float num3 = num1 * spacing.z;
        vector3_2 = new Vector3(vector3_1.x + num2, vector3_1.y, vector3_1.z + num3);
      }
      actor2.transform.position = actor1.transform.position;
      actor2.transform.rotation = actor1.transform.rotation;
      actor2.transform.localScale = BigCard.INVISIBLE_SCALE;
      float time = !showDeath ? this.m_SecretLayoutData.m_ShowAnimTime : this.m_SecretLayoutData.m_DeathShowAnimTime;
      Hashtable args1 = iTween.Hash((object) "position", (object) (vector3_2 - drift), (object) "time", (object) time, (object) "easeType", (object) iTween.EaseType.easeOutExpo);
      iTween.MoveTo(actor2.gameObject, args1);
      Hashtable args2 = iTween.Hash((object) "position", (object) vector3_2, (object) "delay", (object) time, (object) "time", (object) this.m_SecretLayoutData.m_DriftSec, (object) "easeType", (object) iTween.EaseType.easeOutExpo);
      iTween.MoveTo(actor2.gameObject, args2);
      iTween.ScaleTo(actor2.gameObject, this.transform.localScale, time);
      if (showDeath)
        this.ShowPhoneSecretDeath(actor2);
    }
  }

  private void DetermineSecretLayoutOffsets(Card mainCard, List<Actor> actors, out Vector3 initialOffset, out Vector3 spacing, out Vector3 drift)
  {
    Player controller = mainCard.GetController();
    bool flag1 = controller.IsFriendlySide();
    bool flag2 = controller.IsRevealed();
    int minCardThreshold = this.m_SecretLayoutData.m_MinCardThreshold;
    int maxCardThreshold = this.m_SecretLayoutData.m_MaxCardThreshold;
    BigCard.SecretLayoutOffsets minCardOffsets = this.m_SecretLayoutData.m_MinCardOffsets;
    BigCard.SecretLayoutOffsets maxCardOffsets = this.m_SecretLayoutData.m_MaxCardOffsets;
    float t = Mathf.InverseLerp((float) minCardThreshold, (float) maxCardThreshold, (float) actors.Count);
    if (flag2)
    {
      initialOffset = !flag1 ? Vector3.Lerp(minCardOffsets.m_OpponentInitialOffset, maxCardOffsets.m_OpponentInitialOffset, t) : Vector3.Lerp(minCardOffsets.m_InitialOffset, maxCardOffsets.m_InitialOffset, t);
      spacing = this.m_SecretLayoutData.m_Spacing;
    }
    else
    {
      initialOffset = !flag1 ? Vector3.Lerp(minCardOffsets.m_HiddenOpponentInitialOffset, maxCardOffsets.m_HiddenOpponentInitialOffset, t) : Vector3.Lerp(minCardOffsets.m_HiddenInitialOffset, maxCardOffsets.m_HiddenInitialOffset, t);
      spacing = this.m_SecretLayoutData.m_HiddenSpacing;
    }
    if (flag1)
    {
      spacing.z = -spacing.z;
      drift = this.m_SecretLayoutData.m_DriftOffset;
    }
    else
      drift = -this.m_SecretLayoutData.m_DriftOffset;
  }

  private void ShowPhoneSecretDeath(Actor actor)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BigCard.\u003CShowPhoneSecretDeath\u003Ec__AnonStorey3B2 deathCAnonStorey3B2 = new BigCard.\u003CShowPhoneSecretDeath\u003Ec__AnonStorey3B2();
    // ISSUE: reference to a compiler-generated field
    deathCAnonStorey3B2.actor = actor;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    deathCAnonStorey3B2.deathSpellStateFinished = new Spell.StateFinishedCallback(deathCAnonStorey3B2.\u003C\u003Em__135);
    // ISSUE: reference to a compiler-generated method
    Hashtable args = iTween.Hash((object) "time", (object) this.m_SecretLayoutData.m_TimeUntilDeathSpell, (object) "oncomplete", (object) new Action<object>(deathCAnonStorey3B2.\u003C\u003Em__136));
    // ISSUE: reference to a compiler-generated field
    iTween.Timer(deathCAnonStorey3B2.actor.gameObject, args);
  }

  private void HidePhoneSecret(Actor actor)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BigCard.\u003CHidePhoneSecret\u003Ec__AnonStorey3B3 secretCAnonStorey3B3 = new BigCard.\u003CHidePhoneSecret\u003Ec__AnonStorey3B3();
    // ISSUE: reference to a compiler-generated field
    secretCAnonStorey3B3.actor = actor;
    Actor actor1 = this.m_card.GetActor();
    // ISSUE: reference to a compiler-generated field
    iTween.MoveTo(secretCAnonStorey3B3.actor.gameObject, actor1.transform.position, this.m_SecretLayoutData.m_HideAnimTime);
    // ISSUE: reference to a compiler-generated field
    iTween.ScaleTo(secretCAnonStorey3B3.actor.gameObject, BigCard.INVISIBLE_SCALE, this.m_SecretLayoutData.m_HideAnimTime);
    // ISSUE: reference to a compiler-generated method
    Hashtable args = iTween.Hash((object) "time", (object) this.m_SecretLayoutData.m_HideAnimTime, (object) "oncomplete", (object) new Action<object>(secretCAnonStorey3B3.\u003C\u003Em__137));
    // ISSUE: reference to a compiler-generated field
    iTween.Timer(secretCAnonStorey3B3.actor.gameObject, args);
  }

  private void SetupActor(Card card, Actor actor)
  {
    Entity entity = card.GetEntity();
    if (this.ShouldActorUseEntity(entity))
      actor.SetEntity(entity);
    if (this.ShouldActorUseEntityDef(entity))
      actor.SetEntityDef(entity.GetEntityDef());
    actor.SetPremium(entity.GetPremiumType());
    actor.SetCard(card);
    actor.SetCardDef(card.GetCardDef());
    actor.UpdateAllComponents();
    actor.GetComponentInChildren<BoxCollider>().enabled = false;
    actor.name = "BigCard_" + actor.name;
    SceneUtils.SetLayer((Component) actor, GameLayer.Tooltip);
  }

  private bool ShouldActorUseEntity(Entity entity)
  {
    return entity.IsHidden() || entity.GetZone() == TAG_ZONE.PLAY && (UnityEngine.Object) entity.GetCardDef() != (UnityEngine.Object) null && entity.GetCardDef().GetCardTextBuilder().ShouldUseEntityForTextInPlay() || (entity.IsHeroPower() || GameMgr.Get().IsSpectator() && entity.GetZone() == TAG_ZONE.HAND && entity.GetController().IsOpposingSide());
  }

  private bool ShouldActorUseEntityDef(Entity entity)
  {
    return !entity.IsHidden() && !entity.IsHeroPower() && (!GameMgr.Get().IsSpectator() || entity.GetZone() != TAG_ZONE.HAND || !entity.GetController().IsOpposingSide());
  }

  [Serializable]
  public class LayoutData
  {
    public float m_ScaleSec = 0.15f;
    public float m_DriftSec = 10f;
  }

  [Serializable]
  public class SecretLayoutOffsets
  {
    public Vector3 m_InitialOffset = new Vector3(0.1f, 5f, 3.3f);
    public Vector3 m_OpponentInitialOffset = new Vector3(0.1f, 5f, -3.3f);
    public Vector3 m_HiddenInitialOffset = new Vector3(0.0f, 4f, 4f);
    public Vector3 m_HiddenOpponentInitialOffset = new Vector3(0.0f, 4f, -4f);
  }

  [Serializable]
  public class SecretLayoutData
  {
    public float m_ShowAnimTime = 0.15f;
    public float m_HideAnimTime = 0.15f;
    public float m_DeathShowAnimTime = 1f;
    public float m_TimeUntilDeathSpell = 1.5f;
    public float m_DriftSec = 5f;
    public Vector3 m_DriftOffset = new Vector3(0.0f, 0.0f, 0.05f);
    public Vector3 m_Spacing = new Vector3(2.1f, 0.0f, 0.7f);
    public Vector3 m_HiddenSpacing = new Vector3(2.4f, 0.0f, 0.7f);
    public int m_MinCardThreshold = 1;
    public int m_MaxCardThreshold = 5;
    public BigCard.SecretLayoutOffsets m_MinCardOffsets = new BigCard.SecretLayoutOffsets();
    public BigCard.SecretLayoutOffsets m_MaxCardOffsets = new BigCard.SecretLayoutOffsets();
  }

  private struct KeywordArgs
  {
    public Card card;
    public Actor actor;
    public bool showOnRight;
  }
}
