﻿// Decompiled with JetBrains decompiler
// Type: PackOpeningScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PackOpeningScene : Scene
{
  private PackOpening m_packOpening;

  protected override void Awake()
  {
    base.Awake();
    AssetLoader.Get().LoadUIScreen(!(bool) UniversalInputManager.UsePhoneUI ? "PackOpening" : "PackOpening_phone", new AssetLoader.GameObjectCallback(this.OnUIScreenLoaded), (object) null, false);
  }

  private void Update()
  {
    Network.Get().ProcessNetwork();
  }

  private void OnUIScreenLoaded(string name, GameObject screen, object callbackData)
  {
    if ((Object) screen == (Object) null)
    {
      Debug.LogError((object) string.Format("PackOpeningScene.OnPackOpeningLoaded() - failed to load {0}", (object) name));
    }
    else
    {
      this.m_packOpening = screen.GetComponent<PackOpening>();
      if (!((Object) this.m_packOpening == (Object) null))
        return;
      Debug.LogError((object) string.Format("PackOpeningScene.OnPackOpeningLoaded() - {0} did not have a {1} component", (object) name, (object) typeof (PackOpening)));
    }
  }
}
