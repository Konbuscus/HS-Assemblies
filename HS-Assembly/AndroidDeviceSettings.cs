﻿// Decompiled with JetBrains decompiler
// Type: AndroidDeviceSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using UnityEngine;

public class AndroidDeviceSettings
{
  public int densityDpi = 300;
  public bool isExtraLarge = true;
  public const int SCREENLAYOUT_SIZE_XLARGE = 4;
  private static AndroidDeviceSettings s_instance;
  public float heightPixels;
  public float widthPixels;
  public float xdpi;
  public float ydpi;
  public float widthInches;
  public float heightInches;
  public float diagonalInches;
  public float aspectRatio;
  public bool isOnTabletWhitelist;
  public bool isOnPhoneWhitelist;
  public string applicationStorageFolder;
  public int screenLayout;

  private AndroidDeviceSettings()
  {
  }

  public static bool IsCurrentTextureFormatSupported()
  {
    bool flag = SystemInfo.SupportsTextureFormat(new Map<string, TextureFormat>() { { string.Empty, TextureFormat.ARGB32 }, { "etc1", TextureFormat.ETC_RGB4 }, { "etc2", TextureFormat.ETC2_RGBA8 }, { "astc", TextureFormat.ASTC_RGBA_10x10 }, { "atc", TextureFormat.ATC_RGBA8 }, { "dxt", TextureFormat.DXT5 }, { "pvrtc", TextureFormat.PVRTC_RGBA4 } }[string.Empty]);
    Debug.Log((object) ("Checking whether texture format of build () is supported? " + (object) flag));
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.Append("All supported texture formats: ");
    foreach (int num in Enum.GetValues(typeof (TextureFormat)))
    {
      TextureFormat format = (TextureFormat) num;
      try
      {
        if (SystemInfo.SupportsTextureFormat(format))
          stringBuilder.Append(((int) format).ToString() + ", ");
      }
      catch (ArgumentException ex)
      {
      }
    }
    Log.Graphics.Print(stringBuilder.ToString());
    return flag;
  }

  public bool IsMusicPlaying()
  {
    return false;
  }

  public static AndroidDeviceSettings Get()
  {
    if (AndroidDeviceSettings.s_instance == null)
      AndroidDeviceSettings.s_instance = new AndroidDeviceSettings();
    return AndroidDeviceSettings.s_instance;
  }
}
