﻿// Decompiled with JetBrains decompiler
// Type: SpawnToDeckSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class SpawnToDeckSpell : SuperSpell
{
  public float m_RevealStartScale = 0.1f;
  public float m_RevealYOffsetMin = 5f;
  public float m_RevealYOffsetMax = 5f;
  public SpawnToDeckSpell.StackData m_StackData = new SpawnToDeckSpell.StackData();
  public SpawnToDeckSpell.SequenceData m_SequenceData = new SpawnToDeckSpell.SequenceData();
  private const float PHONE_HAND_OFFSET = 1.5f;
  public SpawnToDeckSpell.HandActorSource m_HandActorSource;
  public string m_OverrideCardId;
  public float m_RevealFriendlySideZOffset;
  public float m_RevealOpponentSideZOffset;
  public SpawnToDeckSpell.SpreadType m_SpreadType;
  public Spell m_customRevealSpell;
  private CardDef m_overrideCardDef;

  public override bool AddPowerTargets()
  {
    this.m_visualToTargetIndexMap.Clear();
    this.m_targetToMetaDataMap.Clear();
    this.m_targets.Clear();
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    for (int index = 0; index < taskList.Count; ++index)
    {
      PowerTask task = taskList[index];
      Card cardFromPowerTask = this.GetTargetCardFromPowerTask(index, task);
      if (!((UnityEngine.Object) cardFromPowerTask == (UnityEngine.Object) null) && this.IsValidSpellTarget(cardFromPowerTask.GetEntity()))
        this.AddTarget(cardFromPowerTask.gameObject);
    }
    return this.m_targets.Count > 0;
  }

  protected override Card GetTargetCardFromPowerTask(int index, PowerTask task)
  {
    Network.PowerHistory power = task.GetPower();
    if (power.Type != Network.PowerType.FULL_ENTITY)
      return (Card) null;
    Network.Entity entity1 = (power as Network.HistFullEntity).Entity;
    Entity entity2 = GameState.Get().GetEntity(entity1.ID);
    if (entity2 == null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("{0}.GetTargetCardFromPowerTask() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) entity1.ID));
      return (Card) null;
    }
    if (entity2.GetZone() != TAG_ZONE.DECK)
      return (Card) null;
    return entity2.GetCard();
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    this.StartCoroutine(this.DoActionWithTiming());
  }

  [DebuggerHidden]
  private IEnumerator DoActionWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpawnToDeckSpell.\u003CDoActionWithTiming\u003Ec__Iterator2CD() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator LoadAssets(List<Actor> actors)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpawnToDeckSpell.\u003CLoadAssets\u003Ec__Iterator2CE() { actors = actors, \u003C\u0024\u003Eactors = actors, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator DoEffects(List<Actor> actors)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpawnToDeckSpell.\u003CDoEffects\u003Ec__Iterator2CF() { actors = actors, \u003C\u0024\u003Eactors = actors, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator AnimateSpread(List<Actor> actors)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpawnToDeckSpell.\u003CAnimateSpread\u003Ec__Iterator2D0() { actors = actors, \u003C\u0024\u003Eactors = actors, \u003C\u003Ef__this = this };
  }

  private void AnimateActorUsingSpell(List<Actor> actors, int index)
  {
    Actor actor = actors[index];
    Card component = this.m_targets[index].GetComponent<Card>();
    actor.transform.localScale = new Vector3(this.m_RevealStartScale, this.m_RevealStartScale, this.m_RevealStartScale);
    actor.transform.rotation = this.transform.rotation;
    actor.transform.position = this.transform.position;
    actor.Show();
    SpawnToDeckSpell.RevealSpellFinishedCallbackData finishedCallbackData = new SpawnToDeckSpell.RevealSpellFinishedCallbackData();
    finishedCallbackData.actor = actor;
    finishedCallbackData.card = component;
    if ((UnityEngine.Object) this.m_customRevealSpell == (UnityEngine.Object) null)
    {
      Log.Spells.PrintError("SpawnToDeckSpell.AnimateSpread(): m_SpreadType is set to CUSTOM_SPELL, but m_customRevealSpell is null!");
      this.OnRevealSpellFinished((Spell) null, (object) finishedCallbackData);
    }
    else
    {
      Spell spell = UnityEngine.Object.Instantiate<Spell>(this.m_customRevealSpell);
      SpellUtils.SetCustomSpellParent(spell, (Component) actor);
      spell.AddFinishedCallback(new Spell.FinishedCallback(this.OnRevealSpellFinished), (object) finishedCallbackData);
      spell.Activate();
    }
  }

  public void OnRevealSpellFinished(Spell spell, object userData)
  {
    SpawnToDeckSpell.RevealSpellFinishedCallbackData finishedCallbackData = (SpawnToDeckSpell.RevealSpellFinishedCallbackData) userData;
    Actor actor = finishedCallbackData.actor;
    Card card = finishedCallbackData.card;
    Entity entity = card.GetEntity();
    ZoneDeck deckZone = entity.GetController().GetDeckZone();
    bool hideBackSide = this.GetEntityDef(entity).GetCardType() == TAG_CARDTYPE.INVALID;
    this.StartCoroutine(this.AnimateRevealedCardToDeck(actor, card, deckZone, hideBackSide));
  }

  [DebuggerHidden]
  public IEnumerator AnimateRevealedCardToDeck(Actor actor, Card card, ZoneDeck deck, bool hideBackSide)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpawnToDeckSpell.\u003CAnimateRevealedCardToDeck\u003Ec__Iterator2D1() { card = card, actor = actor, deck = deck, hideBackSide = hideBackSide, \u003C\u0024\u003Ecard = card, \u003C\u0024\u003Eactor = actor, \u003C\u0024\u003Edeck = deck, \u003C\u0024\u003EhideBackSide = hideBackSide, \u003C\u003Ef__this = this };
  }

  private Vector3 ComputeRevealPosition(Vector3 offset)
  {
    Vector3 position = this.transform.position;
    float num = UnityEngine.Random.Range(this.m_RevealYOffsetMin, this.m_RevealYOffsetMax);
    position.y += num;
    switch (this.GetSourceCard().GetControllerSide())
    {
      case Player.Side.FRIENDLY:
        position.z += this.m_RevealFriendlySideZOffset;
        break;
      case Player.Side.OPPOSING:
        position.z += this.m_RevealOpponentSideZOffset;
        break;
    }
    return position + offset;
  }

  private void PreventHandOverlapPhone(List<Actor> actors, List<Vector3> revealPositions)
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    Entity powerTarget = this.GetPowerTarget();
    if (powerTarget != null)
    {
      if (powerTarget.GetControllerSide() == Player.Side.OPPOSING)
        return;
    }
    else
    {
      Card sourceCard = this.GetSourceCard();
      if ((UnityEngine.Object) sourceCard != (UnityEngine.Object) null && sourceCard.GetControllerSide() == Player.Side.OPPOSING)
        return;
    }
    for (int index = 0; index < revealPositions.Count; ++index)
    {
      Vector3 vector3 = revealPositions[index];
      vector3 = new Vector3(vector3.x, vector3.y, vector3.z + 1.5f);
      revealPositions[index] = vector3;
    }
  }

  private void PreventDeckOverlap(List<Actor> actors, List<Vector3> revealPositions)
  {
    float screenDist = 0.0f;
    for (int index = 0; index < revealPositions.Count; ++index)
    {
      ZoneDeck deckZone = this.m_targets[index].GetComponent<Card>().GetEntity().GetController().GetDeckZone();
      float num1 = 0.0f;
      GameObject activeThickness = deckZone.GetActiveThickness();
      if ((bool) ((UnityEngine.Object) activeThickness))
        num1 = activeThickness.GetComponent<Renderer>().bounds.extents.x;
      Vector3 position = deckZone.transform.position;
      position.x -= num1;
      Vector3 revealPosition = revealPositions[index];
      revealPosition.x += actors[index].GetMeshRenderer().bounds.extents.x;
      Vector3 screenPoint = Camera.main.WorldToScreenPoint(position);
      float num2 = Camera.main.WorldToScreenPoint(revealPosition).x - screenPoint.x;
      if ((double) num2 > (double) screenDist)
        screenDist = num2;
    }
    if ((double) screenDist <= 0.0)
      return;
    for (int index = 0; index < revealPositions.Count; ++index)
    {
      ZoneDeck deckZone = this.m_targets[index].GetComponent<Card>().GetEntity().GetController().GetDeckZone();
      float worldDist = CameraUtils.ScreenToWorldDist(Camera.main, screenDist, deckZone.transform.position);
      Vector3 vector3 = revealPositions[index];
      vector3 = new Vector3(vector3.x - worldDist, vector3.y, vector3.z);
      revealPositions[index] = vector3;
    }
  }

  private List<float> RandomizeRevealTimes(int count, float revealSec, float nextRevealSecMin, float nextRevealSecMax)
  {
    List<float> floatList = new List<float>(count);
    List<int> intList = new List<int>(count);
    for (int index = 0; index < count; ++index)
    {
      floatList.Add(0.0f);
      intList.Add(index);
    }
    float num1 = revealSec;
    for (int index1 = 0; index1 < count; ++index1)
    {
      int index2 = UnityEngine.Random.Range(0, intList.Count);
      int index3 = intList[index2];
      intList.RemoveAt(index2);
      floatList[index3] = num1;
      float num2 = UnityEngine.Random.Range(nextRevealSecMin, nextRevealSecMax);
      num1 += num2;
    }
    return floatList;
  }

  [DebuggerHidden]
  private IEnumerator AnimateActor(List<Actor> actors, int index, float revealSec, Vector3 revealPos, float waitSec)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpawnToDeckSpell.\u003CAnimateActor\u003Ec__Iterator2D2() { actors = actors, index = index, revealPos = revealPos, revealSec = revealSec, waitSec = waitSec, \u003C\u0024\u003Eactors = actors, \u003C\u0024\u003Eindex = index, \u003C\u0024\u003ErevealPos = revealPos, \u003C\u0024\u003ErevealSec = revealSec, \u003C\u0024\u003EwaitSec = waitSec, \u003C\u003Ef__this = this };
  }

  private TAG_PREMIUM GetPremium(Entity entity)
  {
    TAG_PREMIUM premiumType = this.GetSourceCard().GetEntity().GetPremiumType();
    if (premiumType == TAG_PREMIUM.GOLDEN)
      return TAG_PREMIUM.GOLDEN;
    switch (this.m_HandActorSource)
    {
      case SpawnToDeckSpell.HandActorSource.CHOSEN_TARGET:
        return this.GetPowerTarget().GetPremiumType();
      case SpawnToDeckSpell.HandActorSource.OVERRIDE:
        return premiumType;
      default:
        return entity.GetPremiumType();
    }
  }

  private EntityDef GetEntityDef(Entity entity)
  {
    switch (this.m_HandActorSource)
    {
      case SpawnToDeckSpell.HandActorSource.CHOSEN_TARGET:
        return this.GetPowerTarget().GetEntityDef();
      case SpawnToDeckSpell.HandActorSource.OVERRIDE:
        return DefLoader.Get().GetEntityDef(this.m_OverrideCardId);
      default:
        return entity.GetEntityDef();
    }
  }

  private CardDef GetCardDef(Card card)
  {
    switch (this.m_HandActorSource)
    {
      case SpawnToDeckSpell.HandActorSource.CHOSEN_TARGET:
        return this.GetPowerTargetCard().GetCardDef();
      case SpawnToDeckSpell.HandActorSource.OVERRIDE:
        return this.m_overrideCardDef;
      default:
        return card.GetCardDef();
    }
  }

  public enum HandActorSource
  {
    CHOSEN_TARGET,
    OVERRIDE,
    SPELL_TARGET,
  }

  public enum SpreadType
  {
    STACK,
    SEQUENCE,
    CUSTOM_SPELL,
  }

  [Serializable]
  public class StackData
  {
    public float m_RevealTime = 1f;
    public float m_StaggerTime = 1.2f;
  }

  [Serializable]
  public class SequenceData
  {
    public float m_Spacing = 2.1f;
    public float m_RevealTime = 0.6f;
    public float m_NextCardRevealTimeMin = 0.1f;
    public float m_NextCardRevealTimeMax = 0.2f;
    public float m_HoldTime = 0.3f;
    public float m_NextCardHoldTime = 0.4f;
  }

  private struct RevealSpellFinishedCallbackData
  {
    public Actor actor;
    public Card card;
  }
}
