﻿// Decompiled with JetBrains decompiler
// Type: Zone
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour
{
  protected List<Card> m_cards = new List<Card>();
  protected bool m_layoutDirty = true;
  protected List<Zone.UpdateLayoutCompleteListener> m_completeListeners = new List<Zone.UpdateLayoutCompleteListener>();
  public const float TRANSITION_SEC = 1f;
  public TAG_ZONE m_ServerTag;
  public Player.Side m_Side;
  protected Player m_controller;
  protected bool m_updatingLayout;
  protected int m_inputBlockerCount;
  protected int m_layoutBlockerCount;

  public override string ToString()
  {
    return string.Format("{1} {0}", (object) this.m_ServerTag, (object) this.m_Side);
  }

  public Player GetController()
  {
    return this.m_controller;
  }

  public int GetControllerId()
  {
    if (this.m_controller == null)
      return 0;
    return this.m_controller.GetPlayerId();
  }

  public void SetController(Player controller)
  {
    this.m_controller = controller;
  }

  public List<Card> GetCards()
  {
    return this.m_cards;
  }

  public int GetCardCount()
  {
    return this.m_cards.Count;
  }

  public Card GetFirstCard()
  {
    if (this.m_cards.Count > 0)
      return this.m_cards[0];
    return (Card) null;
  }

  public Card GetLastCard()
  {
    if (this.m_cards.Count > 0)
      return this.m_cards[this.m_cards.Count - 1];
    return (Card) null;
  }

  public Card GetCardAtIndex(int index)
  {
    if (index < 0)
      return (Card) null;
    if (index >= this.m_cards.Count)
      return (Card) null;
    return this.m_cards[index];
  }

  public Card GetCardAtPos(int pos)
  {
    return this.GetCardAtIndex(pos - 1);
  }

  public int GetLastPos()
  {
    return this.m_cards.Count + 1;
  }

  public int FindCardPos(Card card)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return 1 + this.m_cards.FindIndex(new Predicate<Card>(new Zone.\u003CFindCardPos\u003Ec__AnonStorey3BE() { card = card }.\u003C\u003Em__156));
  }

  public bool ContainsCard(Card card)
  {
    return this.FindCardPos(card) > 0;
  }

  public bool IsOnlyCard(Card card)
  {
    if (this.m_cards.Count != 1)
      return false;
    return (UnityEngine.Object) this.m_cards[0] == (UnityEngine.Object) card;
  }

  public void DirtyLayout()
  {
    this.m_layoutDirty = true;
  }

  public bool IsLayoutDirty()
  {
    return this.m_layoutDirty;
  }

  public bool IsUpdatingLayout()
  {
    return this.m_updatingLayout;
  }

  public bool IsInputEnabled()
  {
    return this.m_inputBlockerCount <= 0;
  }

  public int GetInputBlockerCount()
  {
    return this.m_inputBlockerCount;
  }

  public void AddInputBlocker()
  {
    this.AddInputBlocker(1);
  }

  public void RemoveInputBlocker()
  {
    this.AddInputBlocker(-1);
  }

  public void BlockInput(bool block)
  {
    this.AddInputBlocker(!block ? -1 : 1);
  }

  public void AddInputBlocker(int count)
  {
    int inputBlockerCount = this.m_inputBlockerCount;
    this.m_inputBlockerCount += count;
    if (inputBlockerCount == this.m_inputBlockerCount || inputBlockerCount * this.m_inputBlockerCount != 0)
      return;
    this.UpdateInput();
  }

  public bool IsBlockingLayout()
  {
    return this.m_layoutBlockerCount > 0;
  }

  public int GetLayoutBlockerCount()
  {
    return this.m_layoutBlockerCount;
  }

  public void AddLayoutBlocker()
  {
    ++this.m_layoutBlockerCount;
  }

  public void RemoveLayoutBlocker()
  {
    --this.m_layoutBlockerCount;
  }

  public bool AddUpdateLayoutCompleteCallback(Zone.UpdateLayoutCompleteCallback callback)
  {
    return this.AddUpdateLayoutCompleteCallback(callback, (object) null);
  }

  public bool AddUpdateLayoutCompleteCallback(Zone.UpdateLayoutCompleteCallback callback, object userData)
  {
    Zone.UpdateLayoutCompleteListener completeListener = new Zone.UpdateLayoutCompleteListener();
    completeListener.SetCallback(callback);
    completeListener.SetUserData(userData);
    if (this.m_completeListeners.Contains(completeListener))
      return false;
    this.m_completeListeners.Add(completeListener);
    return true;
  }

  public bool RemoveUpdateLayoutCompleteCallback(Zone.UpdateLayoutCompleteCallback callback)
  {
    return this.RemoveUpdateLayoutCompleteCallback(callback, (object) null);
  }

  public bool RemoveUpdateLayoutCompleteCallback(Zone.UpdateLayoutCompleteCallback callback, object userData)
  {
    Zone.UpdateLayoutCompleteListener completeListener = new Zone.UpdateLayoutCompleteListener();
    completeListener.SetCallback(callback);
    completeListener.SetUserData(userData);
    return this.m_completeListeners.Remove(completeListener);
  }

  public virtual bool CanAcceptTags(int controllerId, TAG_ZONE zoneTag, TAG_CARDTYPE cardType)
  {
    return this.m_ServerTag == zoneTag && (this.m_controller == null || this.m_controller.GetPlayerId() == controllerId) && cardType != TAG_CARDTYPE.ENCHANTMENT;
  }

  public virtual bool AddCard(Card card)
  {
    this.m_cards.Add(card);
    this.DirtyLayout();
    return true;
  }

  public virtual bool InsertCard(int index, Card card)
  {
    this.m_cards.Insert(index, card);
    this.DirtyLayout();
    return true;
  }

  public virtual int RemoveCard(Card card)
  {
    for (int index = 0; index < this.m_cards.Count; ++index)
    {
      if ((UnityEngine.Object) this.m_cards[index] == (UnityEngine.Object) card)
      {
        this.m_cards.RemoveAt(index);
        this.DirtyLayout();
        return index;
      }
    }
    Debug.LogWarning((object) string.Format("{0}.RemoveCard() - FAILED: {1} tried to remove {2}", (object) this, (object) this.m_controller, (object) card));
    return -1;
  }

  public virtual void UpdateLayout()
  {
    if (this.m_cards.Count == 0)
      this.UpdateLayoutFinished();
    else if (GameState.Get().IsMulliganManagerActive())
    {
      this.UpdateLayoutFinished();
    }
    else
    {
      this.m_updatingLayout = true;
      if (this.IsBlockingLayout())
      {
        this.UpdateLayoutFinished();
      }
      else
      {
        for (int index = 0; index < this.m_cards.Count; ++index)
        {
          Card card = this.m_cards[index];
          if (!card.IsDoNotSort())
          {
            card.ShowCard();
            card.EnableTransitioningZones(true);
            iTween.MoveTo(card.gameObject, this.transform.position, 1f);
            iTween.RotateTo(card.gameObject, this.transform.localEulerAngles, 1f);
            iTween.ScaleTo(card.gameObject, this.transform.localScale, 1f);
          }
        }
        this.StartFinishLayoutTimer(1f);
      }
    }
  }

  public static int CardSortComparison(Card card1, Card card2)
  {
    int zonePosition1 = card1.GetZonePosition();
    int zonePosition2 = card2.GetZonePosition();
    if (zonePosition1 != zonePosition2)
      return zonePosition1 - zonePosition2;
    return card1.GetEntity().GetZonePosition() - card2.GetEntity().GetZonePosition();
  }

  protected void UpdateInput()
  {
    bool enabled = this.IsInputEnabled();
    using (List<Card>.Enumerator enumerator = this.m_cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        Actor actor = current.GetActor();
        if (!((UnityEngine.Object) actor == (UnityEngine.Object) null))
        {
          actor.ToggleForceIdle(!enabled);
          actor.ToggleCollider(enabled);
          current.UpdateActorState();
        }
      }
    }
    Card mousedOverCard = InputManager.Get().GetMousedOverCard();
    if (!enabled || !this.m_cards.Contains(mousedOverCard))
      return;
    mousedOverCard.UpdateProposedManaUsage();
  }

  protected void StartFinishLayoutTimer(float delaySec)
  {
    if ((double) delaySec <= (double) Mathf.Epsilon)
      this.UpdateLayoutFinished();
    else if ((UnityEngine.Object) this.m_cards.Find((Predicate<Card>) (card => card.IsTransitioningZones())) == (UnityEngine.Object) null)
      this.UpdateLayoutFinished();
    else
      iTween.Timer(this.gameObject, iTween.Hash((object) "time", (object) delaySec, (object) "oncomplete", (object) "UpdateLayoutFinished", (object) "oncompletetarget", (object) this.gameObject));
  }

  protected void UpdateLayoutFinished()
  {
    for (int index = 0; index < this.m_cards.Count; ++index)
      this.m_cards[index].EnableTransitioningZones(false);
    this.m_updatingLayout = false;
    this.m_layoutDirty = false;
    this.FireUpdateLayoutCompleteCallbacks();
  }

  protected void FireUpdateLayoutCompleteCallbacks()
  {
    if (this.m_completeListeners.Count == 0)
      return;
    Zone.UpdateLayoutCompleteListener[] array = this.m_completeListeners.ToArray();
    this.m_completeListeners.Clear();
    for (int index = 0; index < array.Length; ++index)
      array[index].Fire(this);
  }

  protected class UpdateLayoutCompleteListener : EventListener<Zone.UpdateLayoutCompleteCallback>
  {
    public void Fire(Zone zone)
    {
      this.m_callback(zone, this.m_userData);
    }
  }

  public delegate void UpdateLayoutCompleteCallback(Zone zone, object userData);
}
