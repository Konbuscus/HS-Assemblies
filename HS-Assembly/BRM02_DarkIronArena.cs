﻿// Decompiled with JetBrains decompiler
// Type: BRM02_DarkIronArena
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class BRM02_DarkIronArena : BRM_MissionEntity
{
  private HashSet<string> m_linesPlayed = new HashSet<string>();
  private const float PLAY_CARD_DELAY = 0.7f;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BRMA02_1_RESPONSE_04");
    this.PreloadSound("VO_BRMA02_1_HERO_POWER_05");
    this.PreloadSound("VO_BRMA02_1_TURN1_02");
    this.PreloadSound("VO_BRMA02_1_TURN1_PT2_03");
    this.PreloadSound("VO_BRMA02_1_ALAKIR_34");
    this.PreloadSound("VO_BRMA02_1_ALEXSTRAZA_32");
    this.PreloadSound("VO_BRMA02_1_BEAST_22");
    this.PreloadSound("VO_BRMA02_1_BOOM_28");
    this.PreloadSound("VO_BRMA02_1_CAIRNE_20");
    this.PreloadSound("VO_BRMA02_1_CHO_07");
    this.PreloadSound("VO_BRMA02_1_DEATHWING_35");
    this.PreloadSound("VO_BRMA02_1_ETC_18");
    this.PreloadSound("VO_BRMA02_1_FEUGEN_15");
    this.PreloadSound("VO_BRMA02_1_FOEREAPER_29");
    this.PreloadSound("VO_BRMA02_1_GEDDON_13");
    this.PreloadSound("VO_BRMA02_1_GELBIN_21");
    this.PreloadSound("VO_BRMA02_1_GRUUL_31");
    this.PreloadSound("VO_BRMA02_1_HOGGER_27");
    this.PreloadSound("VO_BRMA02_1_ILLIDAN_23");
    this.PreloadSound("VO_BRMA02_1_LEVIATHAN_12");
    this.PreloadSound("VO_BRMA02_1_LOATHEB_16");
    this.PreloadSound("VO_BRMA02_1_MAEXXNA_24");
    this.PreloadSound("VO_BRMA02_1_MILLHOUSE_09");
    this.PreloadSound("VO_BRMA02_1_MOGOR_25");
    this.PreloadSound("VO_BRMA02_1_MUKLA_10");
    this.PreloadSound("VO_BRMA02_1_NOZDORMU_36");
    this.PreloadSound("VO_BRMA02_1_ONYXIA_33");
    this.PreloadSound("VO_BRMA02_1_PAGLE_08");
    this.PreloadSound("VO_BRMA02_1_SNEED_30");
    this.PreloadSound("VO_BRMA02_1_STALAGG_14");
    this.PreloadSound("VO_BRMA02_1_SYLVANAS_19");
    this.PreloadSound("VO_BRMA02_1_THALNOS_06");
    this.PreloadSound("VO_BRMA02_1_THAURISSAN_37");
    this.PreloadSound("VO_BRMA02_1_TINKMASTER_11");
    this.PreloadSound("VO_BRMA02_1_TOSHLEY_26");
    this.PreloadSound("VO_BRMA02_1_VOLJIN_17");
    this.PreloadSound("VO_NEFARIAN_GRIMSTONE_DEAD1_30");
    this.PreloadSound("VO_RAGNAROS_GRIMSTONE_DEAD2_66");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA02_1_RESPONSE_04",
            m_stringTag = "VO_BRMA02_1_RESPONSE_04"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToWillPlayCardWithTiming(string cardId)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM02_DarkIronArena.\u003CRespondToWillPlayCardWithTiming\u003Ec__Iterator127() { cardId = cardId, \u003C\u0024\u003EcardId = cardId, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM02_DarkIronArena.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator128() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM02_DarkIronArena.\u003CHandleGameOverWithTiming\u003Ec__Iterator129() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
