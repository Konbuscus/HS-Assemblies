﻿// Decompiled with JetBrains decompiler
// Type: AlertPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class AlertPopup : DialogBase
{
  public List<GameObject> m_buttonIconsSet1 = new List<GameObject>();
  public List<GameObject> m_buttonIconsSet2 = new List<GameObject>();
  private const float BUTTON_FRAME_WIDTH = 80f;
  public AlertPopup.Header m_header;
  public NineSliceElement m_body;
  public GameObject m_alertIcon;
  public MultiSliceElement m_buttonContainer;
  public UIBButton m_okayButton;
  public UIBButton m_confirmButton;
  public UIBButton m_cancelButton;
  public GameObject m_clickCatcher;
  public UberText m_alertText;
  public Vector3 m_alertIconOffset;
  public float m_padding;
  public Vector3 m_loadPosition;
  public Vector3 m_showPosition;
  private AlertPopup.PopupInfo m_popupInfo;
  private AlertPopup.PopupInfo m_updateInfo;
  private float m_alertTextInitialWidth;

  public string BodyText
  {
    get
    {
      return this.m_alertText.Text;
    }
    set
    {
      this.m_alertText.Text = value;
      if (this.m_popupInfo == null)
        return;
      this.UpdateLayout();
    }
  }

  protected override void Awake()
  {
    this.m_alertTextInitialWidth = this.m_alertText.Width;
    base.Awake();
    this.m_okayButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ButtonPress(AlertPopup.Response.OK)));
    this.m_confirmButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ButtonPress(AlertPopup.Response.CONFIRM)));
    this.m_cancelButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.ButtonPress(AlertPopup.Response.CANCEL)));
  }

  private void Start()
  {
    this.m_alertText.Text = GameStrings.Get("GLOBAL_OKAY");
  }

  private void OnDestroy()
  {
    if (!((UnityEngine.Object) UniversalInputManager.Get() != (UnityEngine.Object) null))
      return;
    UniversalInputManager.Get().SetSystemDialogActive(false);
  }

  public override bool HandleKeyboardInput()
  {
    if (!Input.GetKeyUp(KeyCode.Escape) || this.m_popupInfo == null || (!this.m_popupInfo.m_keyboardEscIsCancel || !this.m_cancelButton.enabled) || !this.m_cancelButton.gameObject.activeSelf)
      return false;
    this.GoBack();
    return true;
  }

  public override void GoBack()
  {
    this.ButtonPress(AlertPopup.Response.CANCEL);
  }

  public void SetInfo(AlertPopup.PopupInfo info)
  {
    this.m_popupInfo = info;
  }

  public AlertPopup.PopupInfo GetInfo()
  {
    return this.m_popupInfo;
  }

  public override void Show()
  {
    base.Show();
    this.InitInfo();
    this.UpdateAll(this.m_popupInfo);
    this.transform.localPosition += this.m_popupInfo.m_offset;
    if (this.m_popupInfo.m_layerToUse.HasValue)
      SceneUtils.SetLayer((Component) this, this.m_popupInfo.m_layerToUse.Value);
    if (this.m_popupInfo.m_disableBlocker)
      this.m_clickCatcher.SetActive(false);
    this.DoShowAnimation();
    UniversalInputManager.Get().SetSystemDialogActive(this.m_popupInfo == null || !this.m_popupInfo.m_layerToUse.HasValue || this.m_popupInfo.m_layerToUse.Value == GameLayer.UI || this.m_popupInfo.m_layerToUse.Value == GameLayer.HighPriorityUI);
  }

  public void UpdateInfo(AlertPopup.PopupInfo info)
  {
    this.m_updateInfo = info;
    this.UpdateButtons(this.m_updateInfo.m_responseDisplay);
    if (this.m_showAnimState == DialogBase.ShowAnimState.IN_PROGRESS)
      return;
    this.UpdateInfoAfterAnim();
  }

  protected override void OnHideAnimFinished()
  {
    UniversalInputManager.Get().SetSystemDialogActive(false);
    base.OnHideAnimFinished();
  }

  protected override void OnShowAnimFinished()
  {
    base.OnShowAnimFinished();
    if (this.m_updateInfo == null)
      return;
    this.UpdateInfoAfterAnim();
  }

  private void InitInfo()
  {
    if (this.m_popupInfo == null)
      this.m_popupInfo = new AlertPopup.PopupInfo();
    if (this.m_popupInfo.m_headerText != null)
      return;
    this.m_popupInfo.m_headerText = GameStrings.Get("GLOBAL_DEFAULT_ALERT_HEADER");
  }

  private void UpdateButtons(AlertPopup.ResponseDisplay displayType)
  {
    this.m_confirmButton.gameObject.SetActive(false);
    this.m_cancelButton.gameObject.SetActive(false);
    this.m_okayButton.gameObject.SetActive(false);
    switch (displayType)
    {
      case AlertPopup.ResponseDisplay.OK:
        this.m_okayButton.gameObject.SetActive(true);
        break;
      case AlertPopup.ResponseDisplay.CONFIRM:
        this.m_confirmButton.gameObject.SetActive(true);
        break;
      case AlertPopup.ResponseDisplay.CANCEL:
        this.m_cancelButton.gameObject.SetActive(true);
        break;
      case AlertPopup.ResponseDisplay.CONFIRM_CANCEL:
        this.m_confirmButton.gameObject.SetActive(true);
        this.m_cancelButton.gameObject.SetActive(true);
        break;
    }
    this.m_buttonContainer.UpdateSlices();
  }

  private void UpdateTexts(AlertPopup.PopupInfo popupInfo)
  {
    this.m_alertText.RichText = this.m_popupInfo.m_richTextEnabled;
    this.m_alertText.Alignment = this.m_popupInfo.m_alertTextAlignment;
    this.m_alertText.Anchor = this.m_popupInfo.m_alertTextAlignmentAnchor;
    if (popupInfo.m_headerText == null)
      popupInfo.m_headerText = GameStrings.Get("GLOBAL_DEFAULT_ALERT_HEADER");
    this.m_alertText.Text = popupInfo.m_text;
    this.m_okayButton.SetText(popupInfo.m_okText != null ? popupInfo.m_okText : GameStrings.Get("GLOBAL_OKAY"));
    this.m_confirmButton.SetText(popupInfo.m_confirmText != null ? popupInfo.m_confirmText : GameStrings.Get("GLOBAL_CONFIRM"));
    this.m_cancelButton.SetText(popupInfo.m_cancelText != null ? popupInfo.m_cancelText : GameStrings.Get("GLOBAL_CANCEL"));
  }

  private void UpdateInfoAfterAnim()
  {
    this.m_popupInfo = this.m_updateInfo;
    this.m_updateInfo = (AlertPopup.PopupInfo) null;
    this.UpdateAll(this.m_popupInfo);
  }

  private void UpdateAll(AlertPopup.PopupInfo popupInfo)
  {
    this.m_alertIcon.SetActive(popupInfo.m_showAlertIcon);
    bool flag1 = popupInfo.m_iconSet == AlertPopup.PopupInfo.IconSet.Default;
    bool flag2 = popupInfo.m_iconSet == AlertPopup.PopupInfo.IconSet.Alternate;
    for (int index = 0; index < this.m_buttonIconsSet1.Count; ++index)
      this.m_buttonIconsSet1[index].SetActive(flag1);
    for (int index = 0; index < this.m_buttonIconsSet2.Count; ++index)
      this.m_buttonIconsSet2[index].SetActive(flag2);
    this.UpdateHeaderText(popupInfo.m_headerText);
    this.UpdateTexts(popupInfo);
    this.UpdateLayout();
  }

  private void UpdateLayout()
  {
    bool activeSelf = this.m_alertIcon.activeSelf;
    Bounds textBounds = this.m_alertText.GetTextBounds();
    float x = textBounds.size.x;
    float a = textBounds.size.y + this.m_padding + this.m_popupInfo.m_padding;
    float num = 0.0f;
    float b = 0.0f;
    if (activeSelf)
    {
      OrientedBounds orientedWorldBounds = TransformUtil.ComputeOrientedWorldBounds(this.m_alertIcon, true);
      num = orientedWorldBounds.Extents[0].magnitude * 2f;
      b = orientedWorldBounds.Extents[2].magnitude * 2f;
    }
    this.UpdateButtons(this.m_popupInfo.m_responseDisplay);
    this.m_body.SetSize(Mathf.Max(TransformUtil.GetBoundsOfChildren((Component) this.m_confirmButton).size.x * 2f, x) + num, Mathf.Max(a, b));
    Vector3 offset = new Vector3(0.0f, 0.01f, 0.0f);
    TransformUtil.SetPoint(this.m_alertIcon, Anchor.TOP_LEFT_XZ, (GameObject) this.m_body.m_middle, Anchor.TOP_LEFT_XZ, offset);
    this.m_alertIcon.transform.localPosition += this.m_alertIconOffset;
    Anchor anchor = Anchor.TOP_LEFT_XZ;
    if (this.m_popupInfo.m_alertTextAlignment == UberText.AlignmentOptions.Center)
    {
      anchor = Anchor.TOP_XZ;
      if (this.m_popupInfo.m_showAlertIcon)
        anchor = Anchor.TOP_LEFT_XZ;
    }
    if (this.m_alertText.Anchor == UberText.AnchorOptions.Middle)
    {
      switch (anchor)
      {
        case Anchor.TOP_LEFT_XZ:
          anchor = Anchor.LEFT_XZ;
          break;
        case Anchor.TOP_XZ:
          anchor = Anchor.CENTER_XZ;
          break;
        case Anchor.TOP_RIGHT_XZ:
          anchor = Anchor.RIGHT_XZ;
          break;
      }
    }
    TransformUtil.SetPoint((Component) this.m_alertText, anchor, (GameObject) this.m_body.m_middle, anchor, offset);
    Vector3 position = this.m_alertText.transform.position;
    position.x += num + this.m_alertIconOffset.x;
    ++position.y;
    this.m_alertText.transform.position = position;
    if (this.m_popupInfo.m_alertTextAlignment == UberText.AlignmentOptions.Center)
      this.m_alertText.Width = this.m_alertTextInitialWidth - num * this.m_alertText.transform.localScale.x;
    this.m_header.m_container.transform.position = this.m_body.m_top.m_slice.transform.position;
    this.m_buttonContainer.transform.position = this.m_body.m_bottom.m_slice.transform.position;
  }

  private void ButtonPress(AlertPopup.Response response)
  {
    if (this.m_popupInfo.m_responseCallback != null)
      this.m_popupInfo.m_responseCallback(response, this.m_popupInfo.m_responseUserData);
    this.Hide();
  }

  private void UpdateHeaderText(string text)
  {
    bool flag = string.IsNullOrEmpty(text);
    this.m_header.m_container.gameObject.SetActive(!flag);
    if (flag)
      return;
    this.m_header.m_text.ResizeToFit = false;
    this.m_header.m_text.Text = text;
    this.m_header.m_text.UpdateNow();
    MeshRenderer component = this.m_body.m_middle.m_slice.GetComponent<MeshRenderer>();
    float x1 = this.m_header.m_text.GetTextBounds().size.x;
    float x2 = this.m_header.m_text.transform.worldToLocalMatrix.MultiplyVector(this.m_header.m_text.GetTextBounds().size).x;
    float num = 0.8f * this.m_header.m_text.transform.worldToLocalMatrix.MultiplyVector(component.GetComponent<Renderer>().bounds.size).x;
    if ((double) x2 > (double) num)
    {
      this.m_header.m_text.Width = num;
      this.m_header.m_text.ResizeToFit = true;
      this.m_header.m_text.UpdateNow();
      x1 = this.m_header.m_text.GetTextBounds().size.x;
    }
    else
      this.m_header.m_text.Width = x2;
    TransformUtil.SetLocalScaleToWorldDimension(this.m_header.m_middle, new WorldDimensionIndex(x1, 0));
    this.m_header.m_container.UpdateSlices();
  }

  [Serializable]
  public class Header
  {
    public MultiSliceElement m_container;
    public GameObject m_middle;
    public UberText m_text;
  }

  public enum Response
  {
    OK,
    CONFIRM,
    CANCEL,
  }

  public enum ResponseDisplay
  {
    NONE,
    OK,
    CONFIRM,
    CANCEL,
    CONFIRM_CANCEL,
  }

  public class PopupInfo
  {
    public UserAttentionBlocker m_attentionCategory = UserAttentionBlocker.ALL_EXCEPT_FATAL_ERROR_SCENE;
    public bool m_showAlertIcon = true;
    public AlertPopup.ResponseDisplay m_responseDisplay = AlertPopup.ResponseDisplay.OK;
    public Vector3 m_offset = Vector3.zero;
    public bool m_richTextEnabled = true;
    public bool m_keyboardEscIsCancel = true;
    public string m_id;
    public string m_headerText;
    public string m_text;
    public string m_okText;
    public string m_confirmText;
    public string m_cancelText;
    public AlertPopup.ResponseCallback m_responseCallback;
    public object m_responseUserData;
    public float m_padding;
    public Vector3? m_scaleOverride;
    public bool m_disableBlocker;
    public AlertPopup.PopupInfo.IconSet m_iconSet;
    public UberText.AlignmentOptions m_alertTextAlignment;
    public UberText.AnchorOptions m_alertTextAlignmentAnchor;
    public GameLayer? m_layerToUse;

    public enum IconSet
    {
      Default,
      Alternate,
      None,
    }
  }

  public delegate void ResponseCallback(AlertPopup.Response response, object userData);
}
