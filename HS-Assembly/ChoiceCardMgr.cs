﻿// Decompiled with JetBrains decompiler
// Type: ChoiceCardMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class ChoiceCardMgr : MonoBehaviour
{
  private static readonly Vector3 INVISIBLE_SCALE = new Vector3(0.0001f, 0.0001f, 0.0001f);
  public ChoiceCardMgr.CommonData m_CommonData = new ChoiceCardMgr.CommonData();
  public ChoiceCardMgr.ChoiceData m_ChoiceData = new ChoiceCardMgr.ChoiceData();
  public ChoiceCardMgr.SubOptionData m_SubOptionData = new ChoiceCardMgr.SubOptionData();
  public ChoiceCardMgr.ChoiceEffectData m_ChoiceEffectData = new ChoiceCardMgr.ChoiceEffectData();
  private Map<int, ChoiceCardMgr.ChoiceState> m_choiceStateMap = new Map<int, ChoiceCardMgr.ChoiceState>();
  private static ChoiceCardMgr s_instance;
  private ChoiceCardMgr.SubOptionState m_subOptionState;
  private Banner m_choiceBanner;
  private NormalButton m_choiceButton;
  private bool m_friendlyChoicesShown;
  private List<Card> m_lastShownChoices;
  private bool m_lastHideChosen;

  private void Awake()
  {
    ChoiceCardMgr.s_instance = this;
  }

  private void OnDestroy()
  {
    ChoiceCardMgr.s_instance = (ChoiceCardMgr) null;
  }

  private void Start()
  {
    GameState.Get().RegisterEntityChoicesReceivedListener(new GameState.EntityChoicesReceivedCallback(this.OnEntityChoicesReceived));
    GameState.Get().RegisterEntitiesChosenReceivedListener(new GameState.EntitiesChosenReceivedCallback(this.OnEntitiesChosenReceived));
    GameState.Get().RegisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
  }

  public static ChoiceCardMgr Get()
  {
    return ChoiceCardMgr.s_instance;
  }

  public List<Card> GetFriendlyCards()
  {
    if (this.m_subOptionState != null)
      return this.m_subOptionState.m_cards;
    ChoiceCardMgr.ChoiceState choiceState;
    if (this.m_choiceStateMap.TryGetValue(GameState.Get().GetFriendlyPlayerId(), out choiceState))
      return choiceState.m_cards;
    return (List<Card>) null;
  }

  public bool IsShown()
  {
    return this.m_subOptionState != null || this.m_choiceStateMap.Count > 0;
  }

  public bool IsFriendlyShown()
  {
    return this.m_subOptionState != null || this.m_choiceStateMap.ContainsKey(GameState.Get().GetFriendlyPlayerId());
  }

  public bool HasSubOption()
  {
    return this.m_subOptionState != null;
  }

  public Card GetSubOptionParentCard()
  {
    if (this.m_subOptionState == null)
      return (Card) null;
    return this.m_subOptionState.m_parentCard;
  }

  public void ClearSubOptions()
  {
    this.m_subOptionState = (ChoiceCardMgr.SubOptionState) null;
  }

  public void ShowSubOptions(Card parentCard)
  {
    this.m_subOptionState = new ChoiceCardMgr.SubOptionState();
    this.m_subOptionState.m_parentCard = parentCard;
    this.StartCoroutine(this.WaitThenShowSubOptions());
  }

  public bool IsWaitingToShowSubOptions()
  {
    return this.HasSubOption() && this.m_subOptionState.m_parentCard.GetEntity().IsMinion() && (UnityEngine.Object) this.m_subOptionState.m_parentCard.GetZone() != (UnityEngine.Object) GameState.Get().GetFriendlySidePlayer().GetBattlefieldZone();
  }

  public void CancelSubOptions()
  {
    if (!this.HasSubOption())
      return;
    Entity entity = this.m_subOptionState.m_parentCard.GetEntity();
    Card card = entity.GetCard();
    for (int index = 0; index < this.m_subOptionState.m_cards.Count; ++index)
    {
      Spell subOptionSpell = card.GetSubOptionSpell(index, false);
      if ((bool) ((UnityEngine.Object) subOptionSpell))
      {
        switch (subOptionSpell.GetActiveState())
        {
          case SpellStateType.NONE:
          case SpellStateType.CANCEL:
            continue;
          default:
            subOptionSpell.ActivateState(SpellStateType.CANCEL);
            continue;
        }
      }
    }
    card.ActivateHandStateSpells();
    if (entity.IsHeroPower())
      entity.SetTagAndHandleChange<int>(GAME_TAG.EXHAUSTED, 0);
    this.HideSubOptions((Entity) null);
  }

  public void OnSubOptionClicked(Entity chosenEntity)
  {
    if (!this.HasSubOption())
      return;
    this.HideSubOptions(chosenEntity);
  }

  public bool HasChoices()
  {
    return this.m_choiceStateMap.Count > 0;
  }

  public bool HasChoices(int playerId)
  {
    return this.m_choiceStateMap.ContainsKey(playerId);
  }

  public bool HasFriendlyChoices()
  {
    return this.HasChoices(GameState.Get().GetFriendlyPlayerId());
  }

  public PowerTaskList GetPreChoiceTaskList(int playerId)
  {
    ChoiceCardMgr.ChoiceState choiceState;
    if (this.m_choiceStateMap.TryGetValue(playerId, out choiceState))
      return choiceState.m_preTaskList;
    return (PowerTaskList) null;
  }

  public PowerTaskList GetFriendlyPreChoiceTaskList()
  {
    return this.GetPreChoiceTaskList(GameState.Get().GetFriendlyPlayerId());
  }

  public bool IsWaitingToShowChoices(int playerId)
  {
    ChoiceCardMgr.ChoiceState choiceState;
    if (this.m_choiceStateMap.TryGetValue(playerId, out choiceState))
      return choiceState.m_waitingToShow;
    return false;
  }

  public bool IsFriendlyWaitingToShowChoices()
  {
    return this.IsWaitingToShowChoices(GameState.Get().GetFriendlyPlayerId());
  }

  public NormalButton GetChoiceButton()
  {
    return this.m_choiceButton;
  }

  public void OnSendChoices(Network.EntityChoices choicePacket, List<Entity> chosenEntities)
  {
    if (choicePacket.ChoiceType != CHOICE_TYPE.GENERAL)
      return;
    int friendlyPlayerId = GameState.Get().GetFriendlyPlayerId();
    ChoiceCardMgr.ChoiceState state;
    if (!this.m_choiceStateMap.TryGetValue(friendlyPlayerId, out state))
      Error.AddDevFatal("ChoiceCardMgr.OnSendChoices() - there is no ChoiceState for friendly player {0}", (object) friendlyPlayerId);
    this.HideChoicesFromInput(friendlyPlayerId, state, chosenEntities);
  }

  private void OnEntityChoicesReceived(Network.EntityChoices choices, PowerTaskList preChoiceTaskList, object userData)
  {
    if (choices.ChoiceType != CHOICE_TYPE.GENERAL)
      return;
    this.StartCoroutine(this.WaitThenShowChoices(choices, preChoiceTaskList));
  }

  private bool OnEntitiesChosenReceived(Network.EntitiesChosen chosen, object userData)
  {
    if (chosen.ChoiceType != CHOICE_TYPE.GENERAL)
      return false;
    this.StartCoroutine(this.WaitThenHideChoices(chosen));
    return true;
  }

  private void OnGameOver(TAG_PLAYSTATE playState, object userData)
  {
    this.StopAllCoroutines();
    this.CancelSubOptions();
    this.CancelChoices();
  }

  [DebuggerHidden]
  private IEnumerator WaitThenShowChoices(Network.EntityChoices choices, PowerTaskList preChoiceTaskList)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ChoiceCardMgr.\u003CWaitThenShowChoices\u003Ec__IteratorA6() { choices = choices, preChoiceTaskList = preChoiceTaskList, \u003C\u0024\u003Echoices = choices, \u003C\u0024\u003EpreChoiceTaskList = preChoiceTaskList, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator LoadChoiceCardActors(Entity entity, Card card)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ChoiceCardMgr.\u003CLoadChoiceCardActors\u003Ec__IteratorA7() { entity = entity, card = card, \u003C\u0024\u003Eentity = entity, \u003C\u0024\u003Ecard = card, \u003C\u003Ef__this = this };
  }

  private bool IsChoiceCardReady(Card card)
  {
    return this.IsEntityReady(card.GetEntity()) && this.IsCardReady(card) && this.IsCardActorReady(card);
  }

  private void ShowChoices(ChoiceCardMgr.ChoiceState state, bool friendly)
  {
    List<Card> cards = state.m_cards;
    this.m_lastHideChosen = state.m_hideChosen;
    if (friendly)
    {
      this.m_lastShownChoices = cards;
      this.ShowChoiceUi(cards);
    }
    int count = cards.Count;
    string name = !friendly ? this.m_ChoiceData.m_OpponentBoneName : this.m_ChoiceData.m_FriendlyBoneName;
    if ((bool) UniversalInputManager.UsePhoneUI)
      name += "_phone";
    Vector3 position = Board.Get().FindBone(name).position;
    float num1 = this.m_ChoiceData.m_HorizontalPadding;
    if (friendly && (bool) UniversalInputManager.UsePhoneUI && count > this.m_CommonData.m_PhoneMaxCardsBeforeAdjusting)
      num1 = this.GetPhonePaddingForCardCount(count);
    float num2 = !friendly ? this.m_CommonData.m_OpponentCardWidth : this.m_CommonData.m_FriendlyCardWidth;
    if (friendly && (bool) UniversalInputManager.UsePhoneUI && count > this.m_CommonData.m_PhoneMaxCardsBeforeAdjusting)
    {
      float scaleForCardCount = this.GetPhoneScaleForCardCount(count);
      num2 *= scaleForCardCount;
    }
    float num3 = 0.5f * num2;
    float num4 = 0.5f * (float) ((double) num2 * (double) count + (double) num1 * (double) (count - 1));
    float num5 = position.x - num4 + num3;
    for (int index = 0; index < count; ++index)
    {
      cards[index].transform.position = new Vector3()
      {
        x = num5,
        y = position.y,
        z = position.z
      };
      num5 += num2 + num1;
    }
    this.ShowChoiceCards(state, friendly, true);
  }

  private float GetPhoneScaleForCardCount(int cardCount)
  {
    if (cardCount <= this.m_CommonData.m_PhoneMaxCardsBeforeAdjusting)
      return 1f;
    if (cardCount == 4)
      return this.m_CommonData.m_PhoneFourCardScale;
    if (cardCount == 5)
      return this.m_CommonData.m_PhoneFiveCardScale;
    return this.m_CommonData.m_PhoneSixPlusCardScale;
  }

  private float GetPhonePaddingForCardCount(int cardCount)
  {
    if (cardCount <= this.m_CommonData.m_PhoneMaxCardsBeforeAdjusting)
      return this.m_ChoiceData.m_HorizontalPadding;
    if (cardCount == 4)
      return this.m_ChoiceData.m_PhoneHorizontalPaddingFourCards;
    if (cardCount == 5)
      return this.m_ChoiceData.m_PhoneHorizontalPaddingFiveCards;
    return this.m_ChoiceData.m_PhoneHorizontalPaddingSixPlusCards;
  }

  private void ShowChoiceCards(ChoiceCardMgr.ChoiceState state, bool friendly, bool playEffects)
  {
    string name = !friendly ? this.m_ChoiceData.m_OpponentBoneName : this.m_ChoiceData.m_FriendlyBoneName;
    if ((bool) UniversalInputManager.UsePhoneUI)
      name += "_phone";
    int count = state.m_cards.Count;
    Transform bone = Board.Get().FindBone(name);
    Vector3 eulerAngles = bone.rotation.eulerAngles;
    Vector3 localScale = bone.localScale;
    if (friendly && (bool) UniversalInputManager.UsePhoneUI && count > this.m_CommonData.m_PhoneMaxCardsBeforeAdjusting)
    {
      float scaleForCardCount = this.GetPhoneScaleForCardCount(count);
      localScale.x *= scaleForCardCount;
      localScale.z *= scaleForCardCount;
    }
    for (int index = 0; index < count; ++index)
    {
      Card card = state.m_cards[index];
      card.ShowCard();
      card.transform.localScale = ChoiceCardMgr.INVISIBLE_SCALE;
      iTween.Stop(card.gameObject);
      iTween.RotateTo(card.gameObject, eulerAngles, this.m_ChoiceData.m_CardShowTime);
      iTween.ScaleTo(card.gameObject, localScale, this.m_ChoiceData.m_CardShowTime);
    }
    if (!playEffects)
      return;
    this.PlayChoiceEffects(state, friendly);
  }

  private void PlayChoiceEffects(ChoiceCardMgr.ChoiceState state, bool friendly)
  {
    if (!friendly)
      return;
    GameState gameState = GameState.Get();
    Entity entity = gameState.GetEntity(gameState.GetFriendlyEntityChoices().Source);
    if (entity == null)
      return;
    using (List<Card>.Enumerator enumerator = state.m_cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        Spell choiceEffectForCard = this.GetChoiceEffectForCard(entity, current);
        if ((UnityEngine.Object) choiceEffectForCard != (UnityEngine.Object) null)
        {
          Spell spell1 = UnityEngine.Object.Instantiate<Spell>(choiceEffectForCard);
          TransformUtil.AttachAndPreserveLocalTransform(spell1.transform, current.GetActor().transform);
          spell1.AddStateFinishedCallback((Spell.StateFinishedCallback) ((spell, prevStateType, userData) =>
          {
            if (spell.GetActiveState() != SpellStateType.NONE)
              return;
            UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
          }));
          spell1.Activate();
        }
      }
    }
  }

  private Spell GetChoiceEffectForCard(Entity sourceEntity, Card card)
  {
    if (sourceEntity.HasReferencedTag(GAME_TAG.DISCOVER))
      return this.m_ChoiceEffectData.m_DiscoverCardEffect;
    return (Spell) null;
  }

  [DebuggerHidden]
  private IEnumerator WaitThenHideChoices(Network.EntitiesChosen chosen)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ChoiceCardMgr.\u003CWaitThenHideChoices\u003Ec__IteratorA8() { chosen = chosen, \u003C\u0024\u003Echosen = chosen, \u003C\u003Ef__this = this };
  }

  private void HideChoicesFromPacket(int playerId, List<Card> choices, Network.EntitiesChosen chosen, bool hideChosen)
  {
    for (int index = 0; index < choices.Count; ++index)
    {
      Card choice = choices[index];
      if (hideChosen || !this.WasCardChosen(choice, chosen.Entities))
        choice.HideCard();
    }
    this.DoCommonHideChoicesWork(playerId);
    GameState.Get().OnEntitiesChosenProcessed(chosen);
  }

  private bool WasCardChosen(Card card, List<int> chosenEntityIds)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ChoiceCardMgr.\u003CWasCardChosen\u003Ec__AnonStorey3B4 chosenCAnonStorey3B4 = new ChoiceCardMgr.\u003CWasCardChosen\u003Ec__AnonStorey3B4();
    Entity entity = card.GetEntity();
    // ISSUE: reference to a compiler-generated field
    chosenCAnonStorey3B4.entityId = entity.GetEntityId();
    // ISSUE: reference to a compiler-generated method
    return chosenEntityIds.FindIndex(new Predicate<int>(chosenCAnonStorey3B4.\u003C\u003Em__139)) >= 0;
  }

  private void HideChoicesFromInput(int playerId, ChoiceCardMgr.ChoiceState state, List<Entity> chosenEntities)
  {
    for (int index = 0; index < state.m_cards.Count; ++index)
    {
      Card card = state.m_cards[index];
      Entity entity = card.GetEntity();
      if (state.m_hideChosen || !chosenEntities.Contains(entity))
        card.HideCard();
    }
    this.DoCommonHideChoicesWork(playerId);
  }

  private void DoCommonHideChoicesWork(int playerId)
  {
    if (playerId == GameState.Get().GetFriendlyPlayerId())
      this.HideChoiceUi();
    this.m_choiceStateMap.Remove(playerId);
  }

  private void HideChoiceCards(ChoiceCardMgr.ChoiceState state)
  {
    for (int index = 0; index < state.m_cards.Count; ++index)
      this.HideChoiceCard(state.m_cards[index]);
  }

  private void HideChoiceCard(Card card)
  {
    Action<object> action = (Action<object>) (userData => ((Card) userData).HideCard());
    iTween.Stop(card.gameObject);
    Hashtable args = iTween.Hash((object) "scale", (object) ChoiceCardMgr.INVISIBLE_SCALE, (object) "time", (object) this.m_ChoiceData.m_CardHideTime, (object) "oncomplete", (object) action, (object) "oncompleteparams", (object) card, (object) "oncompletetarget", (object) this.gameObject);
    iTween.ScaleTo(card.gameObject, args);
  }

  private void ShowChoiceUi(List<Card> cards)
  {
    this.ShowChoiceBanner(cards);
    this.ShowChoiceButton();
  }

  private void HideChoiceUi()
  {
    this.HideChoiceBanner();
    this.HideChoiceButton();
  }

  private void ShowChoiceBanner(List<Card> cards)
  {
    this.HideChoiceBanner();
    Network.EntityChoices friendlyEntityChoices = GameState.Get().GetFriendlyEntityChoices();
    Transform bone = Board.Get().FindBone(this.m_ChoiceData.m_BannerBoneName);
    this.m_choiceBanner = (Banner) UnityEngine.Object.Instantiate((UnityEngine.Object) this.m_ChoiceData.m_BannerPrefab, bone.position, bone.rotation);
    string headline = GameState.Get().GetGameEntity().CustomChoiceBannerText();
    if (headline == null)
    {
      if (friendlyEntityChoices.CountMax == 1)
      {
        headline = GameStrings.Get("GAMEPLAY_CHOOSE_ONE");
        using (List<Card>.Enumerator enumerator = cards.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Card current = enumerator.Current;
            if ((UnityEngine.Object) null != (UnityEngine.Object) current && current.GetEntity().IsHeroPower())
            {
              headline = GameStrings.Get("GAMEPLAY_CHOOSE_ONE_HERO_POWER");
              break;
            }
          }
        }
      }
      else
        headline = string.Format("[PH] Choose {0} to {1}", (object) friendlyEntityChoices.CountMin, (object) friendlyEntityChoices.CountMax);
    }
    this.m_choiceBanner.SetText(headline);
    Vector3 localScale = this.m_choiceBanner.transform.localScale;
    this.m_choiceBanner.transform.localScale = ChoiceCardMgr.INVISIBLE_SCALE;
    iTween.ScaleTo(this.m_choiceBanner.gameObject, iTween.Hash((object) "scale", (object) localScale, (object) "time", (object) this.m_ChoiceData.m_UiShowTime));
  }

  private void HideChoiceBanner()
  {
    if (!(bool) ((UnityEngine.Object) this.m_choiceBanner))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_choiceBanner.gameObject);
  }

  private void ShowChoiceButton()
  {
    this.HideChoiceButton();
    this.m_choiceButton = AssetLoader.Get().LoadActor(FileUtils.GameAssetPathToName(this.m_ChoiceData.m_ButtonPrefab), false, false).GetComponent<NormalButton>();
    this.m_choiceButton.GetButtonUberText().TextAlpha = 1f;
    string buttonBoneName = this.m_ChoiceData.m_ButtonBoneName;
    if ((bool) UniversalInputManager.UsePhoneUI)
      buttonBoneName += "_phone";
    TransformUtil.CopyWorld((Component) this.m_choiceButton, (Component) Board.Get().FindBone(buttonBoneName));
    this.m_friendlyChoicesShown = true;
    this.m_choiceButton.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.ChoiceButton_OnPress));
    this.m_choiceButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ChoiceButton_OnRelease));
    this.m_choiceButton.SetText(GameStrings.Get("GLOBAL_HIDE"));
    this.m_choiceButton.m_button.GetComponent<Spell>().ActivateState(SpellStateType.BIRTH);
  }

  private void HideChoiceButton()
  {
    if (!(bool) ((UnityEngine.Object) this.m_choiceButton))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_choiceButton.gameObject);
  }

  private void ChoiceButton_OnPress(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("UI_MouseClick_01");
  }

  private void ChoiceButton_OnRelease(UIEvent e)
  {
    ChoiceCardMgr.ChoiceState choiceState = this.m_choiceStateMap[GameState.Get().GetFriendlyPlayerId()];
    if (this.m_friendlyChoicesShown)
    {
      this.m_choiceButton.SetText(GameStrings.Get("GLOBAL_SHOW"));
      this.HideChoiceCards(choiceState);
      this.m_friendlyChoicesShown = false;
    }
    else
    {
      this.m_choiceButton.SetText(GameStrings.Get("GLOBAL_HIDE"));
      this.ShowChoiceCards(choiceState, true, this.m_ChoiceEffectData.m_AlwaysPlayChoiceEffects);
      this.m_friendlyChoicesShown = true;
    }
  }

  private void CancelChoices()
  {
    this.HideChoiceUi();
    using (Map<int, ChoiceCardMgr.ChoiceState>.ValueCollection.Enumerator enumerator = this.m_choiceStateMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ChoiceCardMgr.ChoiceState current = enumerator.Current;
        for (int index = 0; index < current.m_cards.Count; ++index)
          current.m_cards[index].HideCard();
      }
    }
    this.m_choiceStateMap.Clear();
  }

  [DebuggerHidden]
  private IEnumerator WaitThenShowSubOptions()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ChoiceCardMgr.\u003CWaitThenShowSubOptions\u003Ec__IteratorA9() { \u003C\u003Ef__this = this };
  }

  private void ShowSubOptions()
  {
    GameState gameState = GameState.Get();
    Card parentCard = this.m_subOptionState.m_parentCard;
    Entity entity = this.m_subOptionState.m_parentCard.GetEntity();
    string boneName = this.m_SubOptionData.m_BoneName;
    if ((bool) UniversalInputManager.UsePhoneUI)
      boneName += "_phone";
    Transform bone = Board.Get().FindBone(boneName);
    float friendlyCardWidth = this.m_CommonData.m_FriendlyCardWidth;
    float x1 = bone.position.x;
    ZonePlay battlefieldZone = GameState.Get().GetFriendlySidePlayer().GetBattlefieldZone();
    List<int> subCardIds = entity.GetSubCardIDs();
    float num1;
    float num2;
    if (entity.IsMinion() && !(bool) UniversalInputManager.UsePhoneUI)
    {
      int zonePosition = parentCard.GetZonePosition();
      float x2 = battlefieldZone.GetCardPosition(parentCard).x;
      if (zonePosition > 5)
      {
        num1 = friendlyCardWidth + this.m_SubOptionData.m_AdjacentCardXOffset;
        num2 = x2 - (this.m_CommonData.m_FriendlyCardWidth * 1.5f + this.m_SubOptionData.m_AdjacentCardXOffset + this.m_SubOptionData.m_MinionParentXOffset);
      }
      else if (zonePosition == 1 && battlefieldZone.GetCards().Count > 6)
      {
        num1 = friendlyCardWidth + this.m_SubOptionData.m_AdjacentCardXOffset;
        num2 = x2 + (this.m_CommonData.m_FriendlyCardWidth / 2f + this.m_SubOptionData.m_MinionParentXOffset);
      }
      else
      {
        num1 = friendlyCardWidth + this.m_SubOptionData.m_MinionParentXOffset * 2f;
        num2 = x2 - (this.m_CommonData.m_FriendlyCardWidth / 2f + this.m_SubOptionData.m_MinionParentXOffset);
      }
    }
    else
    {
      int count = subCardIds.Count;
      num1 = friendlyCardWidth + (count <= this.m_CommonData.m_PhoneMaxCardsBeforeAdjusting ? this.m_SubOptionData.m_AdjacentCardXOffset : this.m_SubOptionData.m_PhoneMaxAdjacentCardXOffset);
      num2 = x1 - num1 / 2f * (float) (count - 1);
    }
    for (int index = 0; index < subCardIds.Count; ++index)
    {
      int id = subCardIds[index];
      Card card = gameState.GetEntity(id).GetCard();
      if (!((UnityEngine.Object) card == (UnityEngine.Object) null))
      {
        this.m_subOptionState.m_cards.Add(card);
        card.ForceLoadHandActor();
        card.transform.position = parentCard.transform.position;
        card.transform.localScale = ChoiceCardMgr.INVISIBLE_SCALE;
        iTween.MoveTo(card.gameObject, new Vector3()
        {
          x = num2 + (float) index * num1,
          y = bone.position.y,
          z = bone.position.z
        }, this.m_SubOptionData.m_CardShowTime);
        Vector3 localScale = bone.localScale;
        if ((bool) UniversalInputManager.UsePhoneUI && subCardIds.Count > this.m_CommonData.m_PhoneMaxCardsBeforeAdjusting)
        {
          float scaleForCardCount = this.GetPhoneScaleForCardCount(subCardIds.Count);
          localScale.x *= scaleForCardCount;
          localScale.z *= scaleForCardCount;
        }
        iTween.ScaleTo(card.gameObject, localScale, this.m_SubOptionData.m_CardShowTime);
        card.ActivateHandStateSpells();
      }
    }
  }

  private void HideSubOptions(Entity chosenEntity = null)
  {
    for (int index = 0; index < this.m_subOptionState.m_cards.Count; ++index)
    {
      Card card = this.m_subOptionState.m_cards[index];
      card.DeactivateHandStateSpells();
      if (card.GetEntity() != chosenEntity)
        card.HideCard();
    }
  }

  private bool IsEntityReady(Entity entity)
  {
    return entity.GetZone() == TAG_ZONE.SETASIDE && !entity.IsBusy();
  }

  private bool IsCardReady(Card card)
  {
    return !((UnityEngine.Object) card.GetCardDef() == (UnityEngine.Object) null);
  }

  private bool IsCardActorReady(Card card)
  {
    return card.IsActorReady();
  }

  [Serializable]
  public class CommonData
  {
    public float m_FriendlyCardWidth = 2.85f;
    public float m_OpponentCardWidth = 1.5f;
    public int m_PhoneMaxCardsBeforeAdjusting = 3;
    public float m_PhoneFourCardScale = 0.85f;
    public float m_PhoneFiveCardScale = 0.65f;
    public float m_PhoneSixPlusCardScale = 0.55f;
  }

  [Serializable]
  public class ChoiceData
  {
    public string m_FriendlyBoneName = "FriendlyChoice";
    public string m_OpponentBoneName = "OpponentChoice";
    public string m_BannerBoneName = "ChoiceBanner";
    public string m_ButtonBoneName = "ChoiceButton";
    public float m_MinShowTime = 1f;
    public float m_CardShowTime = 0.2f;
    public float m_CardHideTime = 0.2f;
    public float m_UiShowTime = 0.5f;
    public float m_HorizontalPadding = 0.75f;
    public float m_PhoneHorizontalPaddingFourCards = 0.5f;
    public float m_PhoneHorizontalPaddingFiveCards = 0.4f;
    public float m_PhoneHorizontalPaddingSixPlusCards = 0.3f;
    public Banner m_BannerPrefab;
    [CustomEditField(T = EditType.GAME_OBJECT)]
    public string m_ButtonPrefab;
  }

  [Serializable]
  public class SubOptionData
  {
    public string m_BoneName = "SubOption";
    public float m_AdjacentCardXOffset = 0.75f;
    public float m_PhoneMaxAdjacentCardXOffset = 0.1f;
    public float m_MinionParentXOffset = 0.9f;
    public float m_CardShowTime = 0.2f;
  }

  [Serializable]
  public class ChoiceEffectData
  {
    public bool m_AlwaysPlayChoiceEffects;
    public Spell m_DiscoverCardEffect;
  }

  private class SubOptionState
  {
    public List<Card> m_cards = new List<Card>();
    public Card m_parentCard;
  }

  private class ChoiceState
  {
    public List<Card> m_cards = new List<Card>();
    public bool m_waitingToShow;
    public bool m_hideChosen;
    public PowerTaskList m_preTaskList;
  }
}
