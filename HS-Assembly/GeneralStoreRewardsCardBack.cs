﻿// Decompiled with JetBrains decompiler
// Type: GeneralStoreRewardsCardBack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class GeneralStoreRewardsCardBack : MonoBehaviour
{
  public float m_cardBackAppearDelay = 0.5f;
  public float m_cardBackAppearTime = 0.5f;
  public float m_driftRadius = 0.1f;
  public float m_driftTime = 10f;
  private int m_cardBackId = -1;
  public GameObject m_cardBackContainer;
  public Animation m_cardBackAppearAnimation;
  public UberText m_cardBackText;
  public string m_cardBackAppearAnimationName;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_cardBackAppearSound;
  private GameObject m_cardBackObject;
  private bool m_cardBackObjectLoading;
  private Vector3 m_cardBackTextOrigScale;

  public void SetCardBack(int id)
  {
    if (id == -1 || id == this.m_cardBackId)
      return;
    this.LoadCardBackWithId(id);
  }

  public void SetPreorderText(string text)
  {
    this.m_cardBackText.Text = text;
  }

  public void ShowCardBackReward()
  {
    this.HideCardBackReward();
    if (this.m_cardBackId == -1 || (Object) this.m_cardBackAppearAnimation == (Object) null || (string.IsNullOrEmpty(this.m_cardBackAppearAnimationName) || !this.gameObject.activeInHierarchy))
      return;
    this.StartCoroutine("AnimateCardBackIn");
  }

  public void HideCardBackReward()
  {
    this.StopCoroutine("AnimateCardBackIn");
    if (!((Object) this.m_cardBackContainer != (Object) null))
      return;
    this.m_cardBackContainer.SetActive(false);
  }

  private void Awake()
  {
    this.m_cardBackTextOrigScale = this.m_cardBackText.transform.localScale;
  }

  private void LoadCardBackWithId(int cardBackId)
  {
    if ((Object) this.m_cardBackObject != (Object) null)
      Object.Destroy((Object) this.m_cardBackObject);
    if (cardBackId < 0)
    {
      UnityEngine.Debug.LogError((object) "Card back ID must be a positive number");
    }
    else
    {
      this.m_cardBackId = cardBackId;
      this.m_cardBackObjectLoading = CardBackManager.Get().LoadCardBackByIndex(this.m_cardBackId, (CardBackManager.LoadCardBackData.LoadCardBackCallback) (cardBackData =>
      {
        GameObject gameObject = cardBackData.m_GameObject;
        gameObject.transform.parent = this.transform;
        gameObject.name = "CARD_BACK_" + (object) cardBackData.m_CardBackIndex;
        Actor component = gameObject.GetComponent<Actor>();
        if ((Object) component != (Object) null)
        {
          GameObject cardMesh = component.m_cardMesh;
          component.SetCardbackUpdateIgnore(true);
          component.SetUnlit();
          if ((Object) cardMesh != (Object) null)
          {
            Material material = cardMesh.GetComponent<Renderer>().material;
            if (material.HasProperty("_SpecularIntensity"))
              material.SetFloat("_SpecularIntensity", 0.0f);
          }
        }
        this.m_cardBackObject = gameObject;
        SceneUtils.SetLayer(this.m_cardBackObject, this.m_cardBackContainer.gameObject.layer);
        GameUtils.SetParent(this.m_cardBackObject, this.m_cardBackContainer, false);
        this.m_cardBackObject.transform.localPosition = Vector3.zero;
        this.m_cardBackObject.transform.localScale = Vector3.one;
        this.m_cardBackObject.transform.localRotation = Quaternion.identity;
        AnimationUtil.FloatyPosition(this.m_cardBackContainer, this.m_driftRadius, this.m_driftTime);
        if ((Object) this.m_cardBackContainer != (Object) null)
          this.m_cardBackContainer.SetActive(false);
        this.m_cardBackObjectLoading = false;
      }), "Card_Hidden");
    }
  }

  [DebuggerHidden]
  private IEnumerator AnimateCardBackIn()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStoreRewardsCardBack.\u003CAnimateCardBackIn\u003Ec__Iterator272() { \u003C\u003Ef__this = this };
  }
}
