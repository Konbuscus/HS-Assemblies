﻿// Decompiled with JetBrains decompiler
// Type: AdventureBossCoin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class AdventureBossCoin : PegUIElement
{
  public int m_PortraitMaterialIndex = 1;
  private const string s_EventCoinFlip = "Flip";
  public GameObject m_Coin;
  public MeshRenderer m_PortraitRenderer;
  public GameObject m_Connector;
  public StateEventTable m_CoinStateTable;
  public PegUIElement m_DisabledCollider;
  private bool m_Enabled;

  public void SetPortraitMaterial(Material mat)
  {
    if (!((Object) this.m_PortraitRenderer != (Object) null) || this.m_PortraitMaterialIndex >= this.m_PortraitRenderer.materials.Length)
      return;
    Material[] materials = this.m_PortraitRenderer.materials;
    materials[this.m_PortraitMaterialIndex] = mat;
    this.m_PortraitRenderer.materials = materials;
  }

  public void ShowConnector(bool show)
  {
    if (!((Object) this.m_Connector != (Object) null))
      return;
    this.m_Connector.SetActive(show);
  }

  public void Enable(bool flag, bool animate = true)
  {
    this.GetComponent<Collider>().enabled = flag;
    if ((Object) this.m_DisabledCollider != (Object) null)
      this.m_DisabledCollider.gameObject.SetActive(!flag);
    if (this.m_Enabled == flag)
      return;
    this.m_Enabled = flag;
    if (animate && flag)
    {
      this.ShowCoin(false);
      this.m_CoinStateTable.TriggerState("Flip", true, (string) null);
    }
    else
      this.ShowCoin(flag);
  }

  public void Select(bool selected)
  {
    UIBHighlight component = this.GetComponent<UIBHighlight>();
    if ((Object) component == (Object) null)
      return;
    component.AlwaysOver = selected;
    if (!selected)
      return;
    this.EnableFancyHighlight(false);
  }

  public void HighlightOnce()
  {
    UIBHighlight component = this.GetComponent<UIBHighlight>();
    if ((Object) component == (Object) null)
      return;
    component.HighlightOnce();
  }

  public void ShowNewLookGlow()
  {
    this.EnableFancyHighlight(true);
  }

  private void EnableFancyHighlight(bool enable)
  {
    UIBHighlightStateControl component = this.GetComponent<UIBHighlightStateControl>();
    if ((Object) component == (Object) null)
      return;
    component.Select(enable, false);
  }

  private void ShowCoin(bool show)
  {
    if ((Object) this.m_Coin == (Object) null)
      return;
    TransformUtil.SetEulerAngleZ(this.m_Coin, !show ? -180f : 0.0f);
  }
}
