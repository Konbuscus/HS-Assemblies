﻿// Decompiled with JetBrains decompiler
// Type: RainOfFire
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class RainOfFire : SuperSpell
{
  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    --this.m_effectsPendingFinish;
    this.FinishIfPossible();
  }

  protected override void UpdateVisualTargets()
  {
    int num = this.NumberOfCardsInOpponentsHand();
    this.m_TargetInfo.m_RandomTargetCountMin = num;
    this.m_TargetInfo.m_RandomTargetCountMax = num;
    this.GenerateRandomPlayZoneVisualTargets(SpellUtils.FindOpponentPlayZone((Spell) this));
    for (int index = 0; index < this.m_targets.Count; ++index)
    {
      if (index < this.m_visualTargets.Count)
        this.m_visualTargets[index] = this.m_targets[index];
      else
        this.AddVisualTarget(this.m_targets[index]);
    }
  }

  private int NumberOfCardsInOpponentsHand()
  {
    return GameState.Get().GetFirstOpponentPlayer(this.GetSourceCard().GetController()).GetHandZone().GetCardCount();
  }
}
