﻿// Decompiled with JetBrains decompiler
// Type: DropdownMenuItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DropdownMenuItem : PegUIElement
{
  public GameObject m_selected;
  public UberText m_text;
  private object m_value;

  public object GetValue()
  {
    return this.m_value;
  }

  public void SetValue(object val, string text)
  {
    this.m_value = val;
    this.m_text.Text = text;
  }

  public void SetSelected(bool selected)
  {
    if ((Object) this.m_selected == (Object) null)
      return;
    this.m_selected.SetActive(selected);
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    this.m_text.TextColor = Color.white;
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_text.TextColor = Color.black;
  }
}
