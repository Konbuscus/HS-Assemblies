﻿// Decompiled with JetBrains decompiler
// Type: FriendListFriendFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using UnityEngine;

public class FriendListFriendFrame : FriendListBaseFriendFrame
{
  private const float REFRESH_FRIENDS_SECONDS = 30f;
  public FriendListChallengeButton m_ChallengeButton;
  public FriendListFriendFrameBones m_Bones;
  public FriendListFriendFrameOffsets m_Offsets;
  private Component[] m_rightComponentOrder;

  protected override void Awake()
  {
    base.Awake();
    this.m_ChallengeButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnChallengeButtonReleased));
    this.m_rightComponentOrder = new Component[2]
    {
      (Component) this.m_ChatIcon,
      (Component) this.m_ChallengeButton
    };
    iTween.Timer(this.gameObject, iTween.Hash((object) "time", (object) 30f, (object) "looptype", (object) iTween.LoopType.loop, (object) "oncomplete", (object) "UpdateFriend", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void OnEnable()
  {
    this.UpdateFriend();
  }

  public override bool SetFriend(BnetPlayer player)
  {
    this.m_ChallengeButton.SetPlayer(player);
    return base.SetFriend(player);
  }

  public override void UpdateFriend()
  {
    if (!this.gameObject.activeSelf || this.m_player == null)
      return;
    base.UpdateFriend();
    this.m_PlayerIcon.UpdateIcon();
    if (this.m_player.IsOnline())
    {
      this.m_PlayerNameText.Text = FriendUtils.GetFriendListName(this.m_player, true);
      this.UpdateOnlineStatus();
    }
    else
    {
      this.m_PlayerNameText.Text = FriendUtils.GetFriendListName(this.m_player, true);
      this.UpdateOfflineStatus();
    }
    this.m_ChallengeButton.UpdateButton();
    this.UpdateLayout();
  }

  private void OnChallengeButtonReleased(UIEvent e)
  {
    if (this.m_ChallengeButton.CanChallenge() && !ChatMgr.Get().FriendListFrame.IsInEditMode)
      return;
    FriendListFriendFrame.OnPlayerChallengeButtonPressed(this.m_ChallengeButton, this.m_player);
  }

  public static void OnPlayerChallengeButtonPressed(FriendListChallengeButton challengeButton, BnetPlayer player)
  {
    SoundManager.Get().LoadAndPlay("Small_Click");
    if (ChatMgr.Get().FriendListFrame.IsInEditMode)
    {
      ChatMgr.Get().FriendListFrame.ShowRemoveFriendPopup(player);
    }
    else
    {
      BnetGameAccountId hearthstoneGameAccountId = player.GetHearthstoneGameAccountId();
      SpectatorManager spectatorManager = SpectatorManager.Get();
      if (spectatorManager.CanSpectate(player))
        spectatorManager.SpectatePlayer(player);
      else if (spectatorManager.IsSpectatingMe(hearthstoneGameAccountId))
        DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
        {
          m_headerText = GameStrings.Get("GLOBAL_SPECTATOR_KICK_PROMPT_HEADER"),
          m_text = GameStrings.Format("GLOBAL_SPECTATOR_KICK_PROMPT_TEXT", (object) FriendUtils.GetUniqueName(player)),
          m_showAlertIcon = true,
          m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
          m_responseCallback = new AlertPopup.ResponseCallback(FriendListFriendFrame.OnKickSpectatorDialogResponse),
          m_responseUserData = (object) player
        });
      else if (spectatorManager.CanInviteToSpectateMyGame(hearthstoneGameAccountId))
        spectatorManager.InviteToSpectateMe(player);
      else if (spectatorManager.IsSpectatingPlayer(hearthstoneGameAccountId))
      {
        if (GameMgr.Get().IsFindingGame() || SceneMgr.Get().IsTransitioning() || GameMgr.Get().IsTransitionPopupShown())
          return;
        DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
        {
          m_headerText = GameStrings.Get("GLOBAL_SPECTATOR_LEAVE_PROMPT_HEADER"),
          m_text = GameStrings.Get("GLOBAL_SPECTATOR_LEAVE_PROMPT_TEXT"),
          m_showAlertIcon = true,
          m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
          m_responseCallback = new AlertPopup.ResponseCallback(FriendListFriendFrame.OnLeaveSpectatingDialogResponse)
        });
      }
      else
      {
        if (spectatorManager.IsInvitedToSpectateMyGame(hearthstoneGameAccountId))
          ;
        return;
      }
      challengeButton.UpdateButton();
      ChatMgr.Get().CloseChatUI();
    }
  }

  private static void OnLeaveSpectatingDialogResponse(AlertPopup.Response response, object userData)
  {
    if (response != AlertPopup.Response.CONFIRM)
      return;
    SpectatorManager.Get().LeaveSpectatorMode();
  }

  private static void OnKickSpectatorDialogResponse(AlertPopup.Response response, object userData)
  {
    BnetPlayer player = (BnetPlayer) userData;
    if (response != AlertPopup.Response.CONFIRM)
      return;
    SpectatorManager.Get().KickSpectator(player, true);
  }

  private void UpdateLayout()
  {
    Component component = (Component) this.m_Bones.m_RightComponent;
    for (int index = this.m_rightComponentOrder.Length - 1; index >= 0; --index)
    {
      Component src = this.m_rightComponentOrder[index];
      if (src.gameObject.activeSelf)
      {
        TransformUtil.SetPoint(src, Anchor.RIGHT, component, Anchor.LEFT, this.m_Offsets.m_RightComponent);
        component = src;
      }
    }
    Vector3 vector3 = new Vector3(0.0f, 0.0f, 0.0f) + this.AddWidth((Component) this.m_PlayerIcon) + this.AddWidth((Component) this.m_rankMedal);
    this.LayoutLeftText(this.m_PlayerNameText, this.m_Bones.m_PlayerNameText, this.m_Offsets.m_PlayerNameText + vector3, component);
    this.LayoutLeftText(this.m_StatusText, this.m_Bones.m_StatusText, this.m_Offsets.m_StatusText + vector3, component);
    this.m_rankMedal.transform.position = this.m_Bones.m_Medal.transform.position;
  }

  private Vector3 AddWidth(Component component)
  {
    Vector3 vector3 = new Vector3();
    if (component.gameObject.activeInHierarchy)
    {
      Bounds setPointBounds = TransformUtil.ComputeSetPointBounds(component, true);
      vector3.x += setPointBounds.max.x - setPointBounds.min.x;
    }
    return vector3;
  }

  private void LayoutLeftText(UberText text, Transform bone, Vector3 offset, Component rightComponent)
  {
    if (!text.gameObject.activeInHierarchy)
      return;
    text.Width = this.ComputeLeftComponentWidth(bone, offset, rightComponent);
    TransformUtil.SetPoint((Component) text, Anchor.LEFT, (Component) bone, Anchor.RIGHT, offset);
  }

  private float ComputeLeftComponentWidth(Transform bone, Vector3 offset, Component rightComponent)
  {
    Vector3 vector3 = bone.position + offset;
    Bounds setPointBounds = TransformUtil.ComputeSetPointBounds(rightComponent);
    return setPointBounds.center.x - setPointBounds.extents.x + this.m_Offsets.m_RightComponent.x - vector3.x;
  }
}
