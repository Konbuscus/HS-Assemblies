﻿// Decompiled with JetBrains decompiler
// Type: ChatUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;

public static class ChatUtils
{
  public const int MAX_INPUT_CHARACTERS = 512;
  public const int MAX_RECENT_WHISPER_RECEIVERS = 10;
  public const float FRIENDLIST_CHATICON_INACTIVE_SEC = 10f;

  public static string GetMessage(BnetWhisper whisper)
  {
    return ChatUtils.GetMessage(whisper.GetMessage());
  }

  public static string GetMessage(string message)
  {
    if (Localization.GetLocale() == Locale.zhCN)
      return BattleNet.FilterProfanity(message);
    return message;
  }
}
