﻿// Decompiled with JetBrains decompiler
// Type: JoustSpellController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusGame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class JoustSpellController : SpellController
{
  public float m_RandomSecMin = 0.1f;
  public float m_RandomSecMax = 0.25f;
  public float m_ShowTime = 1.2f;
  public float m_DriftCycleTime = 10f;
  public float m_RevealTime = 0.5f;
  public iTween.EaseType m_RevealEaseType = iTween.EaseType.easeOutBack;
  public float m_HoldTime = 1.2f;
  public float m_HideTime = 0.8f;
  public string m_FriendlyBoneName = "FriendlyJoust";
  public string m_OpponentBoneName = "OpponentJoust";
  public Spell m_WinnerSpellPrefab;
  public Spell m_LoserSpellPrefab;
  public Spell m_NoJousterSpellPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_ShowSoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_DrawStingerPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_HideSoundPrefab;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_HideStingerPrefab;
  private int m_joustTaskIndex;
  private JoustSpellController.Jouster m_friendlyJouster;
  private JoustSpellController.Jouster m_opponentJouster;
  private JoustSpellController.Jouster m_winningJouster;
  private JoustSpellController.Jouster m_sourceJouster;

  protected override bool AddPowerSourceAndTargets(PowerTaskList taskList)
  {
    if (!this.HasSourceCard(taskList))
      return false;
    this.m_joustTaskIndex = -1;
    List<PowerTask> taskList1 = taskList.GetTaskList();
    for (int index = 0; index < taskList1.Count; ++index)
    {
      Network.HistMetaData power = taskList1[index].GetPower() as Network.HistMetaData;
      if (power != null && power.MetaType == HistoryMeta.Type.JOUST)
        this.m_joustTaskIndex = index;
    }
    if (this.m_joustTaskIndex < 0)
      return false;
    this.SetSource(taskList.GetSourceEntity().GetCard());
    return true;
  }

  protected override void OnProcessTaskList()
  {
    this.StartCoroutine(this.DoEffectWithTiming());
  }

  [DebuggerHidden]
  private IEnumerator DoEffectWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JoustSpellController.\u003CDoEffectWithTiming\u003Ec__Iterator24A() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitForShowEntities()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JoustSpellController.\u003CWaitForShowEntities\u003Ec__Iterator24B() { \u003C\u003Ef__this = this };
  }

  private void CreateJousters()
  {
    Network.HistMetaData power = (Network.HistMetaData) this.m_taskList.GetTaskList()[this.m_joustTaskIndex].GetPower();
    Player friendlySidePlayer = GameState.Get().GetFriendlySidePlayer();
    Player opposingSidePlayer = GameState.Get().GetOpposingSidePlayer();
    this.m_friendlyJouster = this.CreateJouster(friendlySidePlayer, power);
    this.m_opponentJouster = this.CreateJouster(opposingSidePlayer, power);
    this.DetermineWinner(power);
    this.DetermineSourceJouster();
  }

  private JoustSpellController.Jouster CreateJouster(Player player, Network.HistMetaData metaData)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    JoustSpellController.\u003CCreateJouster\u003Ec__AnonStorey3D6 jousterCAnonStorey3D6 = new JoustSpellController.\u003CCreateJouster\u003Ec__AnonStorey3D6();
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D6.entity = (Entity) null;
    using (List<int>.Enumerator enumerator = metaData.Info.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity = GameState.Get().GetEntity(enumerator.Current);
        if (entity != null && entity.GetController() == player)
        {
          // ISSUE: reference to a compiler-generated field
          jousterCAnonStorey3D6.entity = entity;
          break;
        }
      }
    }
    // ISSUE: reference to a compiler-generated field
    if (jousterCAnonStorey3D6.entity == null)
      return (JoustSpellController.Jouster) null;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D6.card = jousterCAnonStorey3D6.entity.GetCard();
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D6.cardDef = jousterCAnonStorey3D6.card.GetCardDef();
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D6.card.SetInputEnabled(false);
    GameObject gameObject1 = AssetLoader.Get().LoadActor("Card_Hidden", false, false);
    // ISSUE: reference to a compiler-generated field
    GameObject gameObject2 = AssetLoader.Get().LoadActor(ActorNames.GetHandActor(jousterCAnonStorey3D6.entity), false, false);
    JoustSpellController.Jouster jouster = new JoustSpellController.Jouster();
    jouster.m_player = player;
    // ISSUE: reference to a compiler-generated field
    jouster.m_card = jousterCAnonStorey3D6.card;
    jouster.m_initialActor = gameObject1.GetComponent<Actor>();
    jouster.m_revealedActor = gameObject2.GetComponent<Actor>();
    // ISSUE: reference to a compiler-generated method
    Action<Actor> action = new Action<Actor>(jousterCAnonStorey3D6.\u003C\u003Em__191);
    action(jouster.m_initialActor);
    action(jouster.m_revealedActor);
    return jouster;
  }

  private void DetermineWinner(Network.HistMetaData metaData)
  {
    Card joustWinner = GameUtils.GetJoustWinner(metaData);
    if (!(bool) ((UnityEngine.Object) joustWinner))
      return;
    if (joustWinner.GetController().IsFriendlySide())
      this.m_winningJouster = this.m_friendlyJouster;
    else
      this.m_winningJouster = this.m_opponentJouster;
  }

  private void DetermineSourceJouster()
  {
    Player controller = this.GetSource().GetController();
    if (this.m_friendlyJouster != null && this.m_friendlyJouster.m_card.GetController() == controller)
    {
      this.m_sourceJouster = this.m_friendlyJouster;
    }
    else
    {
      if (this.m_opponentJouster == null || this.m_opponentJouster.m_card.GetController() != controller)
        return;
      this.m_sourceJouster = this.m_opponentJouster;
    }
  }

  [DebuggerHidden]
  private IEnumerator ShowJousters()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JoustSpellController.\u003CShowJousters\u003Ec__Iterator24C() { \u003C\u003Ef__this = this };
  }

  private void ShowJouster(JoustSpellController.Jouster jouster, Vector3 localScale, Quaternion rotation, Vector3 position, float delaySec, float showSec)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    JoustSpellController.\u003CShowJouster\u003Ec__AnonStorey3D7 jousterCAnonStorey3D7 = new JoustSpellController.\u003CShowJouster\u003Ec__AnonStorey3D7();
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D7.jouster = jouster;
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D7.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    ++jousterCAnonStorey3D7.jouster.m_effectsPendingFinish;
    // ISSUE: reference to a compiler-generated field
    Card card = jousterCAnonStorey3D7.jouster.m_card;
    // ISSUE: reference to a compiler-generated field
    ZoneDeck deckZone = jousterCAnonStorey3D7.jouster.m_player.GetDeckZone();
    GameObject thicknessForLayout = deckZone.GetThicknessForLayout();
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D7.jouster.m_deckIndex = deckZone.RemoveCard(card);
    deckZone.SetSuppressEmotes(true);
    deckZone.UpdateLayout();
    float num = 0.5f * showSec;
    Vector3 vector3_1 = thicknessForLayout.GetComponent<Renderer>().bounds.center + Card.IN_DECK_OFFSET;
    Vector3 vector3_2 = vector3_1 + Card.ABOVE_DECK_OFFSET;
    Vector3 vector3_3 = position;
    Vector3 eulerAngles = rotation.eulerAngles;
    Vector3 vector3_4 = localScale;
    Vector3[] vector3Array = new Vector3[3]{ vector3_1, vector3_2, vector3_3 };
    card.ShowCard();
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D7.jouster.m_initialActor.Show();
    card.transform.position = vector3_1;
    card.transform.rotation = Card.IN_DECK_HIDDEN_ROTATION;
    card.transform.localScale = Card.IN_DECK_SCALE;
    iTween.MoveTo(card.gameObject, iTween.Hash((object) "path", (object) vector3Array, (object) "delay", (object) delaySec, (object) "time", (object) showSec, (object) "easetype", (object) iTween.EaseType.easeInOutQuart));
    iTween.RotateTo(card.gameObject, iTween.Hash((object) "rotation", (object) eulerAngles, (object) "delay", (object) (float) ((double) delaySec + (double) num), (object) "time", (object) num, (object) "easetype", (object) iTween.EaseType.easeInOutCubic));
    iTween.ScaleTo(card.gameObject, iTween.Hash((object) "scale", (object) vector3_4, (object) "delay", (object) (float) ((double) delaySec + (double) num), (object) "time", (object) num, (object) "easetype", (object) iTween.EaseType.easeInOutQuint));
    if (!string.IsNullOrEmpty(this.m_ShowSoundPrefab))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_ShowSoundPrefab));
    // ISSUE: reference to a compiler-generated method
    Action<object> action = new Action<object>(jousterCAnonStorey3D7.\u003C\u003Em__192);
    iTween.Timer(card.gameObject, iTween.Hash((object) "delay", (object) delaySec, (object) "time", (object) showSec, (object) "oncomplete", (object) action));
  }

  private void PlayNoJousterSpell(Player player)
  {
    ZoneDeck deckZone = player.GetDeckZone();
    Spell spell1 = UnityEngine.Object.Instantiate<Spell>(this.m_NoJousterSpellPrefab);
    spell1.SetPosition(deckZone.transform.position);
    spell1.AddStateFinishedCallback((Spell.StateFinishedCallback) ((spell, prevStateType, userData) =>
    {
      if (spell.GetActiveState() != SpellStateType.NONE)
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
    }));
    spell1.Activate();
  }

  private void DriftJouster(JoustSpellController.Jouster jouster)
  {
    Card card = jouster.m_card;
    Vector3 position = card.transform.position;
    float num1 = 0.02f * jouster.m_initialActor.GetMeshRenderer().bounds.size.z;
    Vector3 vector3_1 = GeneralUtils.RandomSign() * num1 * card.transform.up;
    Vector3 vector3_2 = -vector3_1;
    Vector3 vector3_3 = GeneralUtils.RandomSign() * num1 * card.transform.right;
    Vector3 vector3_4 = -vector3_3;
    List<Vector3> vector3List = new List<Vector3>();
    vector3List.Add(position + vector3_1 + vector3_3);
    vector3List.Add(position + vector3_2 + vector3_3);
    vector3List.Add(position);
    vector3List.Add(position + vector3_1 + vector3_4);
    vector3List.Add(position + vector3_2 + vector3_4);
    vector3List.Add(position);
    float num2 = this.m_DriftCycleTime + this.GetRandomSec();
    Hashtable args = iTween.Hash((object) "path", (object) vector3List.ToArray(), (object) "time", (object) num2, (object) "easetype", (object) iTween.EaseType.linear, (object) "looptype", (object) iTween.LoopType.loop);
    iTween.MoveTo(card.gameObject, args);
  }

  [DebuggerHidden]
  private IEnumerator Joust()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JoustSpellController.\u003CJoust\u003Ec__Iterator24D() { \u003C\u003Ef__this = this };
  }

  private void RevealJouster(JoustSpellController.Jouster jouster, float revealSec)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    JoustSpellController.\u003CRevealJouster\u003Ec__AnonStorey3D8 jousterCAnonStorey3D8 = new JoustSpellController.\u003CRevealJouster\u003Ec__AnonStorey3D8();
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D8.jouster = jouster;
    // ISSUE: reference to a compiler-generated field
    ++jousterCAnonStorey3D8.jouster.m_effectsPendingFinish;
    // ISSUE: reference to a compiler-generated field
    Card card = jousterCAnonStorey3D8.jouster.m_card;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D8.hiddenActor = jousterCAnonStorey3D8.jouster.m_initialActor;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D8.revealedActor = jousterCAnonStorey3D8.jouster.m_revealedActor;
    // ISSUE: reference to a compiler-generated field
    TransformUtil.SetEulerAngleZ(jousterCAnonStorey3D8.revealedActor.gameObject, -180f);
    // ISSUE: reference to a compiler-generated field
    iTween.RotateAdd(jousterCAnonStorey3D8.hiddenActor.gameObject, iTween.Hash((object) "z", (object) 180f, (object) "time", (object) revealSec, (object) "easetype", (object) this.m_RevealEaseType));
    // ISSUE: reference to a compiler-generated field
    iTween.RotateAdd(jousterCAnonStorey3D8.revealedActor.gameObject, iTween.Hash((object) "z", (object) 180f, (object) "time", (object) revealSec, (object) "easetype", (object) this.m_RevealEaseType));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D8.startAngleZ = jousterCAnonStorey3D8.revealedActor.transform.rotation.eulerAngles.z;
    // ISSUE: reference to a compiler-generated method
    Action<object> action1 = new Action<object>(jousterCAnonStorey3D8.\u003C\u003Em__194);
    // ISSUE: reference to a compiler-generated method
    Action<object> action2 = new Action<object>(jousterCAnonStorey3D8.\u003C\u003Em__195);
    iTween.Timer(card.gameObject, iTween.Hash((object) "time", (object) revealSec, (object) "onupdate", (object) action1, (object) "oncomplete", (object) action2));
  }

  [DebuggerHidden]
  private IEnumerator HideJousters()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JoustSpellController.\u003CHideJousters\u003Ec__Iterator24E() { \u003C\u003Ef__this = this };
  }

  private void HideJouster(JoustSpellController.Jouster jouster, float delaySec, float hideSec)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    JoustSpellController.\u003CHideJouster\u003Ec__AnonStorey3D9 jousterCAnonStorey3D9 = new JoustSpellController.\u003CHideJouster\u003Ec__AnonStorey3D9();
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D9.jouster = jouster;
    // ISSUE: reference to a compiler-generated field
    ++jousterCAnonStorey3D9.jouster.m_effectsPendingFinish;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D9.card = jousterCAnonStorey3D9.jouster.m_card;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    jousterCAnonStorey3D9.deck = jousterCAnonStorey3D9.jouster.m_player.GetDeckZone();
    // ISSUE: reference to a compiler-generated field
    Vector3 center = jousterCAnonStorey3D9.deck.GetThicknessForLayout().GetComponent<Renderer>().bounds.center;
    float num = 0.5f * hideSec;
    // ISSUE: reference to a compiler-generated field
    Vector3 position = jousterCAnonStorey3D9.card.transform.position;
    Vector3 vector3_1 = center + Card.ABOVE_DECK_OFFSET;
    Vector3 vector3_2 = center + Card.IN_DECK_OFFSET;
    Vector3 inDeckAngles = Card.IN_DECK_ANGLES;
    Vector3 inDeckScale = Card.IN_DECK_SCALE;
    Vector3[] vector3Array = new Vector3[3]{ position, vector3_1, vector3_2 };
    // ISSUE: reference to a compiler-generated field
    iTween.MoveTo(jousterCAnonStorey3D9.card.gameObject, iTween.Hash((object) "path", (object) vector3Array, (object) "delay", (object) delaySec, (object) "time", (object) hideSec, (object) "easetype", (object) iTween.EaseType.easeInOutQuad));
    // ISSUE: reference to a compiler-generated field
    iTween.RotateTo(jousterCAnonStorey3D9.card.gameObject, iTween.Hash((object) "rotation", (object) inDeckAngles, (object) "delay", (object) delaySec, (object) "time", (object) num, (object) "easetype", (object) iTween.EaseType.easeInOutCubic));
    // ISSUE: reference to a compiler-generated field
    iTween.ScaleTo(jousterCAnonStorey3D9.card.gameObject, iTween.Hash((object) "scale", (object) inDeckScale, (object) "delay", (object) (float) ((double) delaySec + (double) num), (object) "time", (object) num, (object) "easetype", (object) iTween.EaseType.easeInOutQuint));
    if (!string.IsNullOrEmpty(this.m_HideSoundPrefab))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_HideSoundPrefab));
    // ISSUE: reference to a compiler-generated method
    Action<object> action = new Action<object>(jousterCAnonStorey3D9.\u003C\u003Em__196);
    // ISSUE: reference to a compiler-generated field
    iTween.Timer(jousterCAnonStorey3D9.card.gameObject, iTween.Hash((object) "delay", (object) delaySec, (object) "time", (object) hideSec, (object) "oncomplete", (object) action));
  }

  private void DestroyJousters()
  {
    if (this.m_friendlyJouster != null)
    {
      this.DestroyJouster(this.m_friendlyJouster);
      this.m_friendlyJouster = (JoustSpellController.Jouster) null;
    }
    if (this.m_opponentJouster == null)
      return;
    this.DestroyJouster(this.m_opponentJouster);
    this.m_opponentJouster = (JoustSpellController.Jouster) null;
  }

  private void DestroyJouster(JoustSpellController.Jouster jouster)
  {
    if (jouster == null)
      return;
    jouster.m_card.SetInputEnabled(true);
    jouster.m_initialActor.Destroy();
    jouster.m_revealedActor.Destroy();
  }

  private float GetRandomSec()
  {
    return UnityEngine.Random.Range(this.m_RandomSecMin, this.m_RandomSecMax);
  }

  private bool PlaySpellOnActor(JoustSpellController.Jouster jouster, Actor actor, Spell spellPrefab)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    JoustSpellController.\u003CPlaySpellOnActor\u003Ec__AnonStorey3DA actorCAnonStorey3Da = new JoustSpellController.\u003CPlaySpellOnActor\u003Ec__AnonStorey3DA();
    // ISSUE: reference to a compiler-generated field
    actorCAnonStorey3Da.jouster = jouster;
    if (!(bool) ((UnityEngine.Object) spellPrefab))
      return false;
    // ISSUE: reference to a compiler-generated field
    ++actorCAnonStorey3Da.jouster.m_effectsPendingFinish;
    Card card = actor.GetCard();
    Spell spell1 = UnityEngine.Object.Instantiate<Spell>(spellPrefab);
    spell1.transform.parent = actor.transform;
    // ISSUE: reference to a compiler-generated method
    spell1.AddFinishedCallback(new Spell.FinishedCallback(actorCAnonStorey3Da.\u003C\u003Em__197));
    spell1.AddStateFinishedCallback((Spell.StateFinishedCallback) ((spell, prevStateType, userData) =>
    {
      if (spell.GetActiveState() != SpellStateType.NONE)
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
    }));
    spell1.SetSource(card.gameObject);
    spell1.Activate();
    return true;
  }

  private bool IsJousterBusy(JoustSpellController.Jouster jouster)
  {
    if (jouster == null)
      return false;
    return jouster.m_effectsPendingFinish > 0;
  }

  private class Jouster
  {
    public Player m_player;
    public Card m_card;
    public int m_deckIndex;
    public Actor m_initialActor;
    public Actor m_revealedActor;
    public int m_effectsPendingFinish;
  }
}
