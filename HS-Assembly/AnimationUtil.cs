﻿// Decompiled with JetBrains decompiler
// Type: AnimationUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class AnimationUtil : MonoBehaviour
{
  public static void ShowWithPunch(GameObject go, Vector3 startScale, Vector3 punchScale, Vector3 afterPunchScale, string callbackName = "", bool noFade = false, GameObject callbackGO = null, object callbackData = null, AnimationUtil.DelOnShownWithPunch onShowPunchCallback = null)
  {
    if (!noFade)
      iTween.FadeTo(go, 1f, 0.25f);
    go.transform.localScale = startScale;
    iTween.ScaleTo(go, iTween.Hash((object) "scale", (object) punchScale, (object) "time", (object) 0.25f));
    iTween.MoveTo(go, iTween.Hash((object) "position", (object) (go.transform.position + new Vector3(0.02f, 0.02f, 0.02f)), (object) "time", (object) 1.5f));
    AnimationUtil.PunchData callbackData1 = new AnimationUtil.PunchData() { m_gameObject = go, m_scale = afterPunchScale, m_callbackName = callbackName, m_callbackGameObject = callbackGO, m_callbackData = callbackData, m_onShowPunchCallback = onShowPunchCallback };
    go.GetComponent<MonoBehaviour>().StartCoroutine(AnimationUtil.ShowPunchRoutine(callbackData1));
  }

  [DebuggerHidden]
  private static IEnumerator ShowPunchRoutine(AnimationUtil.PunchData callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AnimationUtil.\u003CShowPunchRoutine\u003Ec__Iterator31F() { callbackData = callbackData, \u003C\u0024\u003EcallbackData = callbackData };
  }

  public static void ShowPunch(GameObject go, Vector3 scale, string callbackName = "", GameObject callbackGO = null, object callbackData = null)
  {
    if (string.IsNullOrEmpty(callbackName))
    {
      iTween.ScaleTo(go, scale, 0.15f);
    }
    else
    {
      if ((UnityEngine.Object) callbackGO == (UnityEngine.Object) null)
        callbackGO = go;
      if (callbackData == null)
        callbackData = new object();
      Hashtable args = iTween.Hash((object) "scale", (object) scale, (object) "time", (object) 0.15f, (object) "oncomplete", (object) callbackName, (object) "oncompletetarget", (object) callbackGO, (object) "oncompleteparams", callbackData);
      iTween.ScaleTo(go, args);
    }
  }

  public static void GrowThenDrift(GameObject go, Vector3 origin, Vector3 driftOffset)
  {
    iTween.ScaleFrom(go, iTween.Hash((object) "scale", (object) (Vector3.one * 0.05f), (object) "time", (object) 0.15f, (object) "easeType", (object) iTween.EaseType.easeOutQuart));
    iTween.MoveFrom(go, iTween.Hash((object) "position", (object) origin, (object) "time", (object) 0.15f, (object) "easeType", (object) iTween.EaseType.easeOutQuart));
    go.GetComponent<MonoBehaviour>().StartCoroutine(AnimationUtil.DriftAfterTween(go, 0.15f, driftOffset));
  }

  [DebuggerHidden]
  private static IEnumerator DriftAfterTween(GameObject go, float delayTime, Vector3 driftOffset)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AnimationUtil.\u003CDriftAfterTween\u003Ec__Iterator320() { delayTime = delayTime, go = go, driftOffset = driftOffset, \u003C\u0024\u003EdelayTime = delayTime, \u003C\u0024\u003Ego = go, \u003C\u0024\u003EdriftOffset = driftOffset };
  }

  public static void FloatyPosition(GameObject go, Vector3 startPos, float localRadius, float loopTime)
  {
    Vector3[] vector3Array = new Vector3[5]{ startPos, startPos + new Vector3(localRadius, 0.0f, localRadius), startPos + new Vector3(localRadius * 2f, 0.0f, 0.0f), startPos + new Vector3(localRadius, 0.0f, -localRadius), startPos + Vector3.zero };
    iTween.StopByName("DriftingTween");
    iTween.MoveTo(go, iTween.Hash((object) "name", (object) "DriftingTween", (object) "path", (object) vector3Array, (object) "time", (object) loopTime, (object) "islocal", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "looptype", (object) iTween.LoopType.loop, (object) "movetopath", (object) false));
  }

  public static void FloatyPosition(GameObject go, float radius, float loopTime)
  {
    AnimationUtil.FloatyPosition(go, go.transform.localPosition, radius, loopTime);
  }

  public static void ScaleFade(GameObject go, Vector3 scale)
  {
    AnimationUtil.ScaleFade(go, scale, (string) null);
  }

  public static void ScaleFade(GameObject go, Vector3 scale, string callbackName)
  {
    iTween.FadeTo(go, 0.0f, 0.25f);
    Hashtable args;
    if (string.IsNullOrEmpty(callbackName))
      args = iTween.Hash((object) "scale", (object) scale, (object) "time", (object) 0.25f);
    else
      args = iTween.Hash((object) "scale", (object) scale, (object) "time", (object) 0.25f, (object) "oncomplete", (object) callbackName, (object) "oncompletetarget", (object) go);
    iTween.ScaleTo(go, args);
  }

  public static int GetLayerIndexFromName(Animator animator, string layerName)
  {
    if (layerName == null)
      return -1;
    layerName = layerName.Trim();
    for (int layerIndex = 0; layerIndex < animator.layerCount; ++layerIndex)
    {
      string layerName1 = animator.GetLayerName(layerIndex);
      if (layerName1 != null && layerName1.Trim().Equals(layerName, StringComparison.OrdinalIgnoreCase))
        return layerIndex;
    }
    return -1;
  }

  public static void DriftObject(GameObject go, Vector3 driftOffset)
  {
    iTween.StopByName(go, "DRIFT_MOVE_OBJECT_ITWEEN");
    iTween.MoveBy(go, iTween.Hash((object) "amount", (object) driftOffset, (object) "time", (object) 10f, (object) "name", (object) "DRIFT_MOVE_OBJECT_ITWEEN", (object) "easeType", (object) iTween.EaseType.easeOutQuart));
  }

  public static void FadeTexture(MeshRenderer mesh, float fromAlpha, float toAlpha, float fadeTime, float delay, AnimationUtil.DelOnFade onCompleteCallback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AnimationUtil.\u003CFadeTexture\u003Ec__AnonStorey45B textureCAnonStorey45B = new AnimationUtil.\u003CFadeTexture\u003Ec__AnonStorey45B();
    // ISSUE: reference to a compiler-generated field
    textureCAnonStorey45B.onCompleteCallback = onCompleteCallback;
    iTween.StopByName(mesh.gameObject, "FADE_TEXTURE");
    // ISSUE: reference to a compiler-generated field
    textureCAnonStorey45B.logoMaterial = mesh.materials[0];
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    textureCAnonStorey45B.currentColor = textureCAnonStorey45B.logoMaterial.GetColor("_Color");
    // ISSUE: reference to a compiler-generated field
    textureCAnonStorey45B.currentColor.a = fromAlpha;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    textureCAnonStorey45B.logoMaterial.SetColor("_Color", textureCAnonStorey45B.currentColor);
    // ISSUE: reference to a compiler-generated method
    Hashtable args = iTween.Hash((object) "from", (object) fromAlpha, (object) "to", (object) toAlpha, (object) "time", (object) fadeTime, (object) "onupdate", (object) new Action<object>(textureCAnonStorey45B.\u003C\u003Em__32A), (object) "name", (object) "FADE_TEXTURE");
    if ((double) delay > 0.0)
      args.Add((object) "delay", (object) delay);
    // ISSUE: reference to a compiler-generated field
    if (textureCAnonStorey45B.onCompleteCallback != null)
    {
      // ISSUE: reference to a compiler-generated method
      args.Add((object) "oncomplete", (object) new Action<object>(textureCAnonStorey45B.\u003C\u003Em__32B));
    }
    iTween.ValueTo(mesh.gameObject, args);
  }

  public static void DelayedActivate(GameObject go, float time, bool activate)
  {
    go.GetComponent<MonoBehaviour>().StartCoroutine(AnimationUtil.DelayedActivation(go, time, activate));
  }

  [DebuggerHidden]
  private static IEnumerator DelayedActivation(GameObject go, float time, bool activate)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AnimationUtil.\u003CDelayedActivation\u003Ec__Iterator321() { time = time, go = go, activate = activate, \u003C\u0024\u003Etime = time, \u003C\u0024\u003Ego = go, \u003C\u0024\u003Eactivate = activate };
  }

  private class PunchData
  {
    public GameObject m_gameObject;
    public Vector3 m_scale;
    public string m_callbackName;
    public GameObject m_callbackGameObject;
    public object m_callbackData;
    public AnimationUtil.DelOnShownWithPunch m_onShowPunchCallback;
  }

  public delegate void DelOnShownWithPunch(object callbackData);

  public delegate void DelOnFade();
}
