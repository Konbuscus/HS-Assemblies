﻿// Decompiled with JetBrains decompiler
// Type: GameOpenPack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class GameOpenPack : MonoBehaviour
{
  public PlayMakerFSM m_playMakerFSM;
  private bool clickedOnPack;
  private bool fullyLoaded;

  public void Finish()
  {
    if (GameState.Get() == null)
      return;
    GameState.Get().GetGameEntity().NotifyOfCustomIntroFinished();
  }

  public void PlayJainaLine()
  {
    GameState.Get().GetGameEntity().SendCustomEvent(66);
  }

  public void PlayHoggerLine()
  {
    if ((Object) MulliganManager.Get() == (Object) null)
      ;
  }

  [DebuggerHidden]
  private IEnumerator PlayHoggerAfterVersus()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameOpenPack.\u003CPlayHoggerAfterVersus\u003Ec__Iterator203 versusCIterator203 = new GameOpenPack.\u003CPlayHoggerAfterVersus\u003Ec__Iterator203();
    return (IEnumerator) versusCIterator203;
  }

  public void RaiseBoardLights()
  {
    Board.Get().RaiseTheLights();
  }

  public void Begin()
  {
    if (GameState.Get() == null)
      return;
    GameState.Get().GetGameEntity().NotifyOfGamePackOpened();
  }

  public void NotifyOfFullyLoaded()
  {
    this.fullyLoaded = true;
  }

  public void NotifyOfMouseOver()
  {
    if (!this.fullyLoaded || this.clickedOnPack)
      return;
    this.m_playMakerFSM.SendEvent("Birth");
  }

  public void NotifyOfMouseOff()
  {
    if (!this.fullyLoaded || this.clickedOnPack)
      return;
    this.m_playMakerFSM.SendEvent("Cancel");
  }

  public void HandleClick()
  {
    if (!this.fullyLoaded || this.clickedOnPack || !SceneMgr.Get().IsSceneLoaded() || (Object) LoadingScreen.Get() != (Object) null && LoadingScreen.Get().IsTransitioning())
      return;
    MusicManager.Get().StartPlaylist(MusicPlaylistType.Misc_Tutorial01PackOpen);
    this.clickedOnPack = true;
    this.m_playMakerFSM.SendEvent("Action");
  }
}
