﻿// Decompiled with JetBrains decompiler
// Type: InactivePlayerKicker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class InactivePlayerKicker : MonoBehaviour
{
  private bool m_shouldCheckForInactivity = true;
  private float m_kickSec = 1800f;
  private const float DEFAULT_KICK_SEC = 1800f;
  private static InactivePlayerKicker s_instance;
  private bool m_checkingForInactivity;
  private bool m_activityDetected;
  private float m_inactivityStartTimestamp;

  private void Awake()
  {
    InactivePlayerKicker.s_instance = this;
    ApplicationMgr.Get().WillReset += new Action(InactivePlayerKicker.s_instance.WillReset);
  }

  private void Start()
  {
    SceneMgr.Get().RegisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
    if (!ApplicationMgr.IsInternal())
      return;
    Options.Get().RegisterChangedListener(Option.IDLE_KICK_TIME, new Options.ChangedCallback(this.OnOptionChanged));
    Options.Get().RegisterChangedListener(Option.IDLE_KICKER, new Options.ChangedCallback(this.OnOptionChanged));
  }

  private void OnDestroy()
  {
    ApplicationMgr.Get().WillReset -= new Action(InactivePlayerKicker.s_instance.WillReset);
    if ((UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null)
      SceneMgr.Get().UnregisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
    if (ApplicationMgr.IsInternal())
    {
      Options.Get().UnregisterChangedListener(Option.IDLE_KICK_TIME, new Options.ChangedCallback(this.OnOptionChanged));
      Options.Get().UnregisterChangedListener(Option.IDLE_KICKER, new Options.ChangedCallback(this.OnOptionChanged));
    }
    InactivePlayerKicker.s_instance = (InactivePlayerKicker) null;
  }

  private void Update()
  {
    this.CheckInactivity();
  }

  private void OnGUI()
  {
    this.CheckActivity();
  }

  private void WillReset()
  {
    this.SetShouldCheckForInactivity(true);
  }

  public static InactivePlayerKicker Get()
  {
    return InactivePlayerKicker.s_instance;
  }

  public void OnLoggedIn()
  {
    this.UpdateIdleKickTimeOption();
    this.UpdateCheckForInactivity();
  }

  public bool IsCheckingForInactivity()
  {
    return this.m_checkingForInactivity;
  }

  public bool ShouldCheckForInactivity()
  {
    return this.m_shouldCheckForInactivity;
  }

  public void SetShouldCheckForInactivity(bool check)
  {
    if (this.m_shouldCheckForInactivity == check)
      return;
    this.m_shouldCheckForInactivity = check;
    this.UpdateCheckForInactivity();
  }

  public float GetKickSec()
  {
    return this.m_kickSec;
  }

  public void SetKickSec(float sec)
  {
    this.m_kickSec = sec;
  }

  public bool SetKickTimeStr(string timeStr)
  {
    float sec;
    if (!TimeUtils.TryParseDevSecFromElapsedTimeString(timeStr, out sec))
      return false;
    this.SetKickSec(sec);
    return true;
  }

  private bool CanCheckForInactivity()
  {
    return !DemoMgr.Get().IsExpoDemo() && Network.IsLoggedIn() && this.m_shouldCheckForInactivity && (!ApplicationMgr.IsInternal() || Options.Get().GetBool(Option.IDLE_KICKER));
  }

  private void UpdateCheckForInactivity()
  {
    bool checkingForInactivity = this.m_checkingForInactivity;
    this.m_checkingForInactivity = this.CanCheckForInactivity();
    if (!this.m_checkingForInactivity || checkingForInactivity)
      return;
    this.StartCheckForInactivity();
  }

  private void StartCheckForInactivity()
  {
    this.m_activityDetected = false;
    this.m_inactivityStartTimestamp = Time.realtimeSinceStartup;
  }

  private void CheckActivity()
  {
    if (!this.IsCheckingForInactivity())
      return;
    switch (Event.current.type)
    {
      case EventType.MouseDown:
      case EventType.MouseUp:
      case EventType.MouseDrag:
      case EventType.KeyDown:
      case EventType.KeyUp:
      case EventType.ScrollWheel:
        this.m_activityDetected = true;
        break;
      default:
        if (GameMgr.Get() == null || !GameMgr.Get().IsSpectator())
          break;
        this.m_activityDetected = true;
        break;
    }
  }

  private void CheckInactivity()
  {
    if (!this.IsCheckingForInactivity())
      return;
    if (this.m_activityDetected)
    {
      this.m_inactivityStartTimestamp = Time.realtimeSinceStartup;
      this.m_activityDetected = false;
    }
    else
    {
      if ((double) (Time.realtimeSinceStartup - this.m_inactivityStartTimestamp) < (double) this.m_kickSec)
        return;
      Error.AddFatalLoc("GLOBAL_ERROR_INACTIVITY_KICK");
      if ((bool) ApplicationMgr.AllowResetFromFatalError)
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) this);
    }
  }

  private void OnScenePreUnload(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.FATAL_ERROR)
      return;
    if ((bool) ApplicationMgr.AllowResetFromFatalError)
      this.SetShouldCheckForInactivity(false);
    else
      UnityEngine.Object.Destroy((UnityEngine.Object) this);
  }

  private void UpdateIdleKickTimeOption()
  {
    if (!ApplicationMgr.IsInternal())
      return;
    this.SetKickTimeStr(Options.Get().GetString(Option.IDLE_KICK_TIME));
  }

  private void OnOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    switch (option)
    {
      case Option.IDLE_KICKER:
        this.UpdateCheckForInactivity();
        break;
      case Option.IDLE_KICK_TIME:
        this.UpdateIdleKickTimeOption();
        break;
      default:
        Error.AddDevFatal("InactivePlayerKicker.OnOptionChanged() - unhandled option {0}", (object) option);
        break;
    }
  }
}
