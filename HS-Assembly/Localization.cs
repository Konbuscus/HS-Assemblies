﻿// Decompiled with JetBrains decompiler
// Type: Localization
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;
using System.Globalization;

public class Localization
{
  public static readonly string DEFAULT_LOCALE_NAME = Locale.enUS.ToString();
  public static readonly Map<Locale, Locale[]> LOAD_ORDERS = new Map<Locale, Locale[]>() { { Locale.enUS, new Locale[1] }, { Locale.enGB, new Locale[2]{ Locale.enGB, Locale.enUS } }, { Locale.frFR, new Locale[1]{ Locale.frFR } }, { Locale.deDE, new Locale[1]{ Locale.deDE } }, { Locale.koKR, new Locale[1]{ Locale.koKR } }, { Locale.esES, new Locale[1]{ Locale.esES } }, { Locale.esMX, new Locale[1]{ Locale.esMX } }, { Locale.ruRU, new Locale[1]{ Locale.ruRU } }, { Locale.zhTW, new Locale[1]{ Locale.zhTW } }, { Locale.zhCN, new Locale[1]{ Locale.zhCN } }, { Locale.itIT, new Locale[1]{ Locale.itIT } }, { Locale.ptBR, new Locale[1]{ Locale.ptBR } }, { Locale.plPL, new Locale[1]{ Locale.plPL } }, { Locale.jaJP, new Locale[1]{ Locale.jaJP } }, { Locale.thTH, new Locale[1]{ Locale.thTH } } };
  private static Localization s_instance = new Localization();
  public static readonly PlatformDependentValue<bool> LOCALE_FROM_OPTIONS = new PlatformDependentValue<bool>(PlatformCategory.OS) { iOS = true, Android = true, PC = false, Mac = false };
  public const Locale DEFAULT_LOCALE = Locale.enUS;
  private Locale m_locale;
  private CultureInfo m_cultureInfo;
  private List<Locale> m_foreignLocales;
  private List<string> m_foreignLocaleNames;

  public static void Initialize()
  {
    Locale? nullable = new Locale?();
    Locale outVal1;
    if ((bool) Localization.LOCALE_FROM_OPTIONS && EnumUtils.TryGetEnum<Locale>(Options.Get().GetString(Option.LOCALE), out outVal1))
      nullable = new Locale?(outVal1);
    if (!nullable.HasValue)
    {
      string str1 = (string) null;
      if (ApplicationMgr.IsPublic())
        str1 = BattleNet.GetLaunchOption("LOCALE", false);
      if (string.IsNullOrEmpty(str1))
        str1 = Vars.Key("Localization.Locale").GetStr(Localization.DEFAULT_LOCALE_NAME);
      if (ApplicationMgr.IsInternal())
      {
        string str2 = Vars.Key("Localization.OverrideBnetLocale").GetStr(string.Empty);
        if (!string.IsNullOrEmpty(str2))
          str1 = str2;
      }
      Locale outVal2;
      nullable = !EnumUtils.TryGetEnum<Locale>(str1, out outVal2) ? new Locale?(Locale.enUS) : new Locale?(outVal2);
    }
    Localization.SetLocale(nullable.Value);
  }

  public static Locale GetLocale()
  {
    return Localization.s_instance.m_locale;
  }

  public static void SetLocale(Locale locale)
  {
    Localization.s_instance.SetPegLocale(locale);
  }

  public static bool IsIMELocale()
  {
    if (Localization.GetLocale() != Locale.zhCN && Localization.GetLocale() != Locale.zhTW)
      return Localization.GetLocale() == Locale.koKR;
    return true;
  }

  public static string GetLocaleName()
  {
    return Localization.s_instance.m_locale.ToString();
  }

  public static string GetBnetLocaleName()
  {
    string str = Localization.s_instance.m_locale.ToString();
    return string.Format("{0}-{1}", (object) str.Substring(0, 2), (object) str.Substring(2, 2));
  }

  public static bool SetLocaleName(string localeName)
  {
    if (!Localization.IsValidLocaleName(localeName))
      return false;
    Localization.s_instance.SetPegLocaleName(localeName);
    return true;
  }

  public static Locale[] GetLoadOrder(AssetFamily family)
  {
    return Localization.GetLoadOrder(family == AssetFamily.CardTexture || family == AssetFamily.CardPremium);
  }

  public static Locale[] GetLoadOrder(Locale locale, bool isCardTexture = false)
  {
    Locale[] array = Localization.LOAD_ORDERS[locale];
    if (Network.IsRunning() && BattleNet.GetAccountCountry() == "CHN" && isCardTexture)
    {
      Array.Resize<Locale>(ref array, array.Length + 1);
      Array.Copy((Array) array, 0, (Array) array, 1, array.Length - 1);
      array[0] = Locale.zhCN;
    }
    return array;
  }

  public static Locale[] GetLoadOrder(bool isCardTexture = false)
  {
    return Localization.GetLoadOrder(Localization.s_instance.m_locale, isCardTexture);
  }

  public static CultureInfo GetCultureInfo()
  {
    return Localization.s_instance.m_cultureInfo;
  }

  public static bool IsValidLocaleName(string localeName)
  {
    return Enum.IsDefined(typeof (Locale), (object) localeName);
  }

  public static bool IsValidLocaleName(string localeName, params Locale[] locales)
  {
    if (locales == null || locales.Length == 0)
      return false;
    for (int index = 0; index < locales.Length; ++index)
    {
      string str = locales[index].ToString();
      if (localeName == str)
        return true;
    }
    return false;
  }

  public static bool IsForeignLocale(Locale locale)
  {
    return locale != Locale.enUS;
  }

  public static bool IsForeignLocaleName(string localeName)
  {
    Locale locale;
    try
    {
      locale = EnumUtils.Parse<Locale>(localeName);
    }
    catch (Exception ex)
    {
      return false;
    }
    return Localization.IsForeignLocale(locale);
  }

  public static List<Locale> GetForeignLocales()
  {
    List<Locale> foreignLocales = Localization.s_instance.m_foreignLocales;
    if (foreignLocales != null)
      return foreignLocales;
    List<Locale> localeList = new List<Locale>();
    foreach (int num in Enum.GetValues(typeof (Locale)))
    {
      Locale locale = (Locale) num;
      switch (locale)
      {
        case Locale.UNKNOWN:
        case Locale.enUS:
          continue;
        default:
          localeList.Add(locale);
          continue;
      }
    }
    Localization.s_instance.m_foreignLocales = localeList;
    return localeList;
  }

  public static List<string> GetForeignLocaleNames()
  {
    List<string> foreignLocaleNames = Localization.s_instance.m_foreignLocaleNames;
    if (foreignLocaleNames != null)
      return foreignLocaleNames;
    List<string> stringList = new List<string>();
    foreach (string name in Enum.GetNames(typeof (Locale)))
    {
      if (!(name == Locale.UNKNOWN.ToString()) && !(name == Localization.DEFAULT_LOCALE_NAME))
        stringList.Add(name);
    }
    Localization.s_instance.m_foreignLocaleNames = stringList;
    return stringList;
  }

  public static string ConvertLocaleToDotNet(Locale locale)
  {
    return Localization.ConvertLocaleToDotNet(locale.ToString());
  }

  public static string ConvertLocaleToDotNet(string localeName)
  {
    return string.Format("{0}-{1}", (object) localeName.Substring(0, 2), (object) localeName.Substring(2, 2).ToUpper());
  }

  public static bool DoesLocaleUseDecimalPoint(Locale locale)
  {
    switch (locale)
    {
      case Locale.enUS:
      case Locale.enGB:
      case Locale.koKR:
      case Locale.esMX:
      case Locale.zhTW:
      case Locale.zhCN:
      case Locale.jaJP:
      case Locale.thTH:
        return true;
      case Locale.frFR:
      case Locale.deDE:
      case Locale.esES:
      case Locale.ruRU:
      case Locale.itIT:
      case Locale.ptBR:
      case Locale.plPL:
        return false;
      default:
        return true;
    }
  }

  private void SetPegLocale(Locale locale)
  {
    this.SetPegLocaleName(locale.ToString());
  }

  private void SetPegLocaleName(string localeName)
  {
    this.m_locale = EnumUtils.Parse<Locale>(localeName);
    this.m_cultureInfo = CultureInfo.CreateSpecificCulture(Localization.ConvertLocaleToDotNet(this.m_locale));
  }
}
