﻿// Decompiled with JetBrains decompiler
// Type: ShowFPS
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class ShowFPS : MonoBehaviour
{
  private float m_UpdateInterval = 0.5f;
  private GUIText m_GuiText;
  private double m_LastInterval;
  private int frames;
  private bool m_FrameCountActive;
  private float m_FrameCountTime;
  private float m_FrameCountLastTime;
  private int m_FrameCount;
  private bool m_verbose;
  private string m_fpsText;
  private static ShowFPS s_instance;

  private void Awake()
  {
    ShowFPS.s_instance = this;
    if (!ApplicationMgr.IsPublic())
      return;
    Object.DestroyImmediate((Object) this.gameObject);
  }

  private void OnDestroy()
  {
    ShowFPS.s_instance = (ShowFPS) null;
  }

  public static ShowFPS Get()
  {
    return ShowFPS.s_instance;
  }

  [ContextMenu("Start Frame Count")]
  public void StartFrameCount()
  {
    this.m_FrameCountLastTime = Time.realtimeSinceStartup;
    this.m_FrameCountTime = 0.0f;
    this.m_FrameCount = 0;
    this.m_FrameCountActive = true;
  }

  [ContextMenu("Stop Frame Count")]
  public void StopFrameCount()
  {
    this.m_FrameCountActive = false;
  }

  [ContextMenu("Clear Frame Count")]
  public void ClearFrameCount()
  {
    this.m_FrameCountLastTime = 0.0f;
    this.m_FrameCountTime = 0.0f;
    this.m_FrameCount = 0;
    this.m_FrameCountActive = false;
  }

  private void Start()
  {
    this.m_LastInterval = (double) Time.realtimeSinceStartup;
    this.frames = 0;
    this.UpdateEnabled();
    Options.Get().RegisterChangedListener(Option.HUD, new Options.ChangedCallback(this.OnHudOptionChanged));
  }

  private void OnDisable()
  {
    if ((bool) ((Object) this.m_GuiText))
      Object.DestroyImmediate((Object) this.m_GuiText.gameObject);
    Time.captureFramerate = 0;
  }

  private void Update()
  {
    bool flag = false;
    foreach (Component allCamera in Camera.allCameras)
    {
      FullScreenEffects component = allCamera.GetComponent<FullScreenEffects>();
      if (!((Object) component == (Object) null) && component.enabled)
        flag = true;
    }
    if (!(bool) ((Object) this.m_GuiText))
    {
      GameObject gameObject = new GameObject("FPS");
      gameObject.transform.position = Vector3.zero;
      this.m_GuiText = gameObject.AddComponent<GUIText>();
      SceneUtils.SetHideFlags((Object) gameObject, HideFlags.HideAndDontSave);
      this.m_GuiText.pixelOffset = new Vector2((float) Screen.width * 0.7f, 15f);
    }
    ++this.frames;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if ((double) realtimeSinceStartup > this.m_LastInterval + (double) this.m_UpdateInterval)
    {
      float num = (float) this.frames / (realtimeSinceStartup - (float) this.m_LastInterval);
      this.m_fpsText = !this.m_verbose ? string.Format("{0:f2}", (object) num) : string.Format("{0:f2} - {1} frames over {2}sec", (object) num, (object) this.frames, (object) this.m_UpdateInterval);
      this.frames = 0;
      this.m_LastInterval = (double) realtimeSinceStartup;
    }
    string str = this.m_fpsText;
    if (this.m_FrameCountActive || this.m_FrameCount > 0)
    {
      if (this.m_FrameCountActive)
      {
        this.m_FrameCountTime += (float) (((double) realtimeSinceStartup - (double) this.m_FrameCountLastTime) / 60.0) * Time.timeScale;
        if ((double) this.m_FrameCountLastTime == 0.0)
          this.m_FrameCountLastTime = realtimeSinceStartup;
        this.m_FrameCount = Mathf.CeilToInt(this.m_FrameCountTime * 60f);
      }
      str = string.Format("{0} - Frame Count: {1}", (object) str, (object) this.m_FrameCount);
    }
    if (flag)
      str = string.Format("{0} - FSE", (object) str);
    if ((Object) ScreenEffectsMgr.Get() != (Object) null)
    {
      int screenEffectsCount = ScreenEffectsMgr.Get().GetActiveScreenEffectsCount();
      if (screenEffectsCount > 0 && ScreenEffectsMgr.Get().gameObject.activeSelf)
        str = string.Format("{0} - ScreenEffects Active: {1}", (object) str, (object) screenEffectsCount);
    }
    this.m_GuiText.text = str;
  }

  private void OnHudOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    this.UpdateEnabled();
  }

  private void UpdateEnabled()
  {
    this.enabled = Options.Get().GetBool(Option.HUD);
  }
}
