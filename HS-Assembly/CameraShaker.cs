﻿// Decompiled with JetBrains decompiler
// Type: CameraShaker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CameraShaker : MonoBehaviour
{
  public Vector3 m_Amount;
  public AnimationCurve m_IntensityCurve;
  public bool m_Hold;
  public float m_HoldAtSec;

  public void StartShake()
  {
    float? holdAtTime = new float?();
    if (this.m_Hold)
      holdAtTime = new float?(this.m_HoldAtSec);
    CameraShakeMgr.Shake(Camera.main, this.m_Amount, this.m_IntensityCurve, holdAtTime);
  }
}
