﻿// Decompiled with JetBrains decompiler
// Type: DrawMorphedCardSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class DrawMorphedCardSpell : SuperSpell
{
  private int m_revealTaskIndex = -1;
  public float m_OldCardHoldTime;
  public float m_NewCardHoldTime;
  private Card m_oldCard;
  private Card m_newCard;

  public override bool AddPowerTargets()
  {
    this.m_revealTaskIndex = -1;
    if (!this.CanAddPowerTargets())
      return false;
    this.FindOldAndNewCards();
    return (bool) ((UnityEngine.Object) this.m_oldCard) && (bool) ((UnityEngine.Object) this.m_newCard);
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    this.m_oldCard.SetHoldingForLinkedCardSwitch(true);
    this.m_newCard.SetHoldingForLinkedCardSwitch(true);
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
  }

  protected override void DoActionNow()
  {
    if (this.m_revealTaskIndex < 0)
      this.BeginEffects();
    else
      this.m_taskList.DoTasks(0, this.m_revealTaskIndex + 1, new PowerTaskList.CompleteCallback(this.OnRevealTasksComplete));
  }

  private void FindOldAndNewCards()
  {
    int num = -1;
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    for (int index = 0; index < taskList.Count; ++index)
    {
      Network.PowerHistory power = taskList[index].GetPower();
      switch (power.Type)
      {
        case Network.PowerType.FULL_ENTITY:
          Entity entity1 = GameState.Get().GetEntity(((Network.HistFullEntity) power).Entity.ID);
          if (entity1 != null)
          {
            Card card = entity1.GetCard();
            if (!((UnityEngine.Object) card == (UnityEngine.Object) null) && this.IsValidSpellTarget(card.GetEntity()))
            {
              this.m_newCard = card;
              break;
            }
            break;
          }
          break;
        case Network.PowerType.SHOW_ENTITY:
          Network.HistShowEntity histShowEntity = (Network.HistShowEntity) power;
          Entity entity2 = GameState.Get().GetEntity(histShowEntity.Entity.ID);
          if (entity2 != null)
          {
            Card card = entity2.GetCard();
            if (!((UnityEngine.Object) card == (UnityEngine.Object) null) && this.IsValidSpellTarget(card.GetEntity()) && entity2.GetZone() == TAG_ZONE.DECK && histShowEntity.Entity.Tags.Find((Predicate<Network.Entity.Tag>) (tag => tag.Name == 49 && tag.Value == 3)) != null)
            {
              this.m_oldCard = card;
              num = index;
              break;
            }
            break;
          }
          break;
      }
    }
    if (!(bool) ((UnityEngine.Object) this.m_oldCard) || !(bool) ((UnityEngine.Object) this.m_newCard))
      return;
    this.m_revealTaskIndex = num;
    this.AddTarget(this.m_oldCard.gameObject);
  }

  private void OnRevealTasksComplete(PowerTaskList taskList, int startIndex, int count, object userData)
  {
    this.BeginEffects();
  }

  private void BeginEffects()
  {
    base.DoActionNow();
    this.StartCoroutine(this.HoldOldCard());
  }

  [DebuggerHidden]
  private IEnumerator HoldOldCard()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DrawMorphedCardSpell.\u003CHoldOldCard\u003Ec__Iterator2B4() { \u003C\u003Ef__this = this };
  }

  private void OnAllTasksComplete(PowerTaskList taskList, int startIndex, int count, object userData)
  {
    this.StartCoroutine(this.HoldNewCard());
  }

  [DebuggerHidden]
  private IEnumerator HoldNewCard()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DrawMorphedCardSpell.\u003CHoldNewCard\u003Ec__Iterator2B5() { \u003C\u003Ef__this = this };
  }
}
