﻿// Decompiled with JetBrains decompiler
// Type: ProgressBar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class ProgressBar : MonoBehaviour
{
  public float m_increaseAnimTime = 2f;
  public float m_decreaseAnimTime = 1f;
  public float m_coolDownAnimTime = 1f;
  public float m_barIntensity = 1.2f;
  public float m_barIntensityIncreaseMax = 3f;
  public float m_audioFadeInOut = 0.2f;
  public float m_increasePitchStart = 1f;
  public float m_increasePitchEnd = 1.2f;
  public float m_decreasePitchStart = 1f;
  public float m_decreasePitchEnd = 0.8f;
  public TextMesh m_label;
  public UberText m_uberLabel;
  private Material m_barMaterial;
  private float m_prevVal;
  private float m_currVal;
  private float m_factor;
  private float m_maxIntensity;
  private float m_Uadd;
  private float m_animationTime;

  public void Awake()
  {
    this.m_barMaterial = this.GetComponent<Renderer>().material;
  }

  public void SetMaterial(Material material)
  {
    this.m_barMaterial = material;
  }

  public void AnimateProgress(float prevVal, float currVal)
  {
    this.m_prevVal = prevVal;
    this.m_currVal = currVal;
    this.m_factor = (double) this.m_currVal <= (double) this.m_prevVal ? this.m_prevVal - this.m_currVal : this.m_currVal - this.m_prevVal;
    this.m_factor = Mathf.Abs(this.m_factor);
    if ((double) this.m_currVal > (double) this.m_prevVal)
      this.IncreaseProgress(this.m_currVal, this.m_prevVal);
    else
      this.DecreaseProgress(this.m_currVal, this.m_prevVal);
  }

  public void SetProgressBar(float progress)
  {
    if ((Object) this.m_barMaterial == (Object) null)
      this.m_barMaterial = this.GetComponent<Renderer>().material;
    this.m_barMaterial.SetFloat("_Intensity", this.m_barIntensity);
    this.m_barMaterial.SetFloat("_Percent", progress);
  }

  public float GetAnimationTime()
  {
    return this.m_animationTime;
  }

  public void SetLabel(string text)
  {
    if ((Object) this.m_uberLabel != (Object) null)
      this.m_uberLabel.Text = text;
    if (!((Object) this.m_label != (Object) null))
      return;
    this.m_label.text = text;
  }

  public void SetBarTexture(Texture texture)
  {
    if ((Object) this.m_barMaterial == (Object) null)
      this.m_barMaterial = this.GetComponent<Renderer>().material;
    this.m_barMaterial.SetTexture("_NoiseTex", texture);
  }

  private void IncreaseProgress(float currProgress, float prevProgress)
  {
    float num = this.m_increaseAnimTime * this.m_factor;
    this.m_animationTime = num;
    iTween.EaseType easeType = iTween.EaseType.easeOutQuad;
    Hashtable args1 = iTween.Hash((object) "from", (object) prevProgress, (object) "to", (object) currProgress, (object) "time", (object) num, (object) "easetype", (object) easeType, (object) "onupdate", (object) "Progress_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "IncreaseProgress");
    iTween.StopByName(this.gameObject, "IncreaseProgress");
    iTween.ValueTo(this.gameObject, args1);
    Hashtable args2 = iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 0.005f, (object) "time", (object) num, (object) "easetype", (object) iTween.EaseType.easeOutQuad, (object) "onupdate", (object) "ScrollSpeed_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "UVSpeed");
    iTween.StopByName(this.gameObject, "UVSpeed");
    iTween.ValueTo(this.gameObject, args2);
    this.m_maxIntensity = this.m_barIntensity + (this.m_barIntensityIncreaseMax - this.m_barIntensity) * this.m_factor;
    Hashtable args3 = iTween.Hash((object) "from", (object) this.m_barIntensity, (object) "to", (object) this.m_maxIntensity, (object) "time", (object) num, (object) "easetype", (object) easeType, (object) "onupdate", (object) "Intensity_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "Intensity", (object) "oncomplete", (object) "Intensity_OnComplete", (object) "oncompletetarget", (object) this.gameObject);
    iTween.StopByName(this.gameObject, "Intensity");
    iTween.ValueTo(this.gameObject, args3);
    if ((bool) ((Object) this.GetComponent<AudioSource>()))
    {
      SoundManager.Get().SetVolume(this.GetComponent<AudioSource>(), 0.0f);
      SoundManager.Get().SetPitch(this.GetComponent<AudioSource>(), this.m_increasePitchStart);
      SoundManager.Get().Play(this.GetComponent<AudioSource>(), true);
    }
    Hashtable args4 = iTween.Hash((object) "from", (object) 0, (object) "to", (object) 1, (object) "time", (object) (float) ((double) num * (double) this.m_audioFadeInOut), (object) "delay", (object) 0, (object) "easetype", (object) easeType, (object) "onupdate", (object) "AudioVolume_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "barVolumeStart");
    iTween.StopByName(this.gameObject, "barVolumeStart");
    iTween.ValueTo(this.gameObject, args4);
    Hashtable args5 = iTween.Hash((object) "from", (object) 1, (object) "to", (object) 0, (object) "time", (object) (float) ((double) num * (double) this.m_audioFadeInOut), (object) "delay", (object) (float) ((double) num * (1.0 - (double) this.m_audioFadeInOut)), (object) "easetype", (object) easeType, (object) "onupdate", (object) "AudioVolume_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "oncomplete", (object) "AudioVolume_OnComplete", (object) "name", (object) "barVolumeEnd");
    iTween.StopByName(this.gameObject, "barVolumeEnd");
    iTween.ValueTo(this.gameObject, args5);
    Hashtable args6 = iTween.Hash((object) "from", (object) this.m_increasePitchStart, (object) "to", (object) this.m_increasePitchEnd, (object) "time", (object) num, (object) "delay", (object) 0, (object) "easetype", (object) easeType, (object) "onupdate", (object) "AudioPitch_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "barPitch");
    iTween.StopByName(this.gameObject, "barPitch");
    iTween.ValueTo(this.gameObject, args6);
  }

  private void Progress_OnUpdate(float val)
  {
    if ((Object) this.m_barMaterial == (Object) null)
      this.m_barMaterial = this.GetComponent<Renderer>().material;
    this.m_barMaterial.SetFloat("_Percent", val);
  }

  private void Intensity_OnComplete()
  {
    iTween.StopByName(this.gameObject, "Increase");
    iTween.StopByName(this.gameObject, "Intensity");
    iTween.StopByName(this.gameObject, "UVSpeed");
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) this.m_maxIntensity, (object) "to", (object) this.m_barIntensity, (object) "time", (object) this.m_coolDownAnimTime, (object) "easetype", (object) iTween.EaseType.easeOutQuad, (object) "onupdate", (object) "Intensity_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "Intensity"));
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0.005f, (object) "to", (object) 0.0f, (object) "time", (object) this.m_coolDownAnimTime, (object) "easetype", (object) iTween.EaseType.easeOutQuad, (object) "onupdate", (object) "ScrollSpeed_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "UVSpeed"));
  }

  private void Intensity_OnUpdate(float val)
  {
    if ((Object) this.m_barMaterial == (Object) null)
      this.m_barMaterial = this.GetComponent<Renderer>().material;
    this.m_barMaterial.SetFloat("_Intensity", val);
  }

  private void ScrollSpeed_OnUpdate(float val)
  {
    if ((Object) this.m_barMaterial == (Object) null)
      this.m_barMaterial = this.GetComponent<Renderer>().material;
    this.m_Uadd += val;
    this.m_barMaterial.SetFloat("_Uadd", this.m_Uadd);
  }

  private void AudioVolume_OnUpdate(float val)
  {
    if (!(bool) ((Object) this.GetComponent<AudioSource>()))
      return;
    SoundManager.Get().SetVolume(this.GetComponent<AudioSource>(), val);
  }

  private void AudioVolume_OnComplete()
  {
    if (!(bool) ((Object) this.GetComponent<AudioSource>()))
      return;
    SoundManager.Get().Stop(this.GetComponent<AudioSource>());
  }

  private void AudioPitch_OnUpdate(float val)
  {
    if (!(bool) ((Object) this.GetComponent<AudioSource>()))
      return;
    SoundManager.Get().SetPitch(this.GetComponent<AudioSource>(), val);
  }

  private void DecreaseProgress(float currProgress, float prevProgress)
  {
    float num = this.m_decreaseAnimTime * this.m_factor;
    this.m_animationTime = num;
    iTween.EaseType easeType = iTween.EaseType.easeInOutCubic;
    Hashtable args1 = iTween.Hash((object) "from", (object) prevProgress, (object) "to", (object) currProgress, (object) "time", (object) num, (object) "easetype", (object) easeType, (object) "onupdate", (object) "Progress_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "Decrease");
    iTween.StopByName(this.gameObject, "Decrease");
    iTween.ValueTo(this.gameObject, args1);
    if ((bool) ((Object) this.GetComponent<AudioSource>()))
    {
      SoundManager.Get().SetVolume(this.GetComponent<AudioSource>(), 0.0f);
      SoundManager.Get().SetPitch(this.GetComponent<AudioSource>(), this.m_decreasePitchStart);
      SoundManager.Get().Play(this.GetComponent<AudioSource>(), true);
    }
    Hashtable args2 = iTween.Hash((object) "from", (object) 0, (object) "to", (object) 1, (object) "time", (object) (float) ((double) num * (double) this.m_audioFadeInOut), (object) "delay", (object) 0, (object) "easetype", (object) easeType, (object) "onupdate", (object) "AudioVolume_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "barVolumeStart");
    iTween.StopByName(this.gameObject, "barVolumeStart");
    iTween.ValueTo(this.gameObject, args2);
    Hashtable args3 = iTween.Hash((object) "from", (object) 1, (object) "to", (object) 0, (object) "time", (object) (float) ((double) num * (double) this.m_audioFadeInOut), (object) "delay", (object) (float) ((double) num * (1.0 - (double) this.m_audioFadeInOut)), (object) "easetype", (object) easeType, (object) "onupdate", (object) "AudioVolume_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "oncomplete", (object) "AudioVolume_OnComplete", (object) "name", (object) "barVolumeEnd");
    iTween.StopByName(this.gameObject, "barVolumeEnd");
    iTween.ValueTo(this.gameObject, args3);
    Hashtable args4 = iTween.Hash((object) "from", (object) this.m_decreasePitchStart, (object) "to", (object) this.m_decreasePitchEnd, (object) "time", (object) num, (object) "delay", (object) 0, (object) "easetype", (object) easeType, (object) "onupdate", (object) "AudioPitch_OnUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "barPitch");
    iTween.StopByName(this.gameObject, "barPitch");
    iTween.ValueTo(this.gameObject, args4);
  }
}
