﻿// Decompiled with JetBrains decompiler
// Type: ClassFilterHeaderButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ClassFilterHeaderButton : PegUIElement
{
  public SlidingTray m_classFilterTray;
  public UberText m_headerText;
  public Transform m_showTwoRowsBone;
  public ClassFilterButtonContainer m_container;
  private ClassFilterButton[] m_buttons;

  protected override void Awake()
  {
    this.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.HandleRelease()));
    base.Awake();
  }

  public void HandleRelease()
  {
    CollectionManagerDisplay.Get().HideDeckHelpPopup();
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    bool flag = taggedDeck != null;
    if (this.m_buttons == null)
      this.m_buttons = this.m_classFilterTray.GetComponentsInChildren<ClassFilterButton>();
    if (!flag)
      this.m_container.SetDefaults();
    else
      this.m_container.SetClass(taggedDeck.GetClass());
    this.m_classFilterTray.ToggleTraySlider(true, this.m_showTwoRowsBone, true);
    NotificationManager.Get().DestroyAllPopUps();
  }

  public void SetMode(CollectionManagerDisplay.ViewMode mode, TAG_CLASS? classTag)
  {
    if (mode == CollectionManagerDisplay.ViewMode.CARD_BACKS)
      this.m_headerText.Text = GameStrings.Get("GLUE_COLLECTION_MANAGER_CARD_BACKS_TITLE");
    else if (mode == CollectionManagerDisplay.ViewMode.HERO_SKINS)
      this.m_headerText.Text = GameStrings.Get("GLUE_COLLECTION_MANAGER_HERO_SKINS_TITLE");
    else if (classTag.HasValue)
      this.m_headerText.Text = GameStrings.GetClassName(classTag.Value);
    else
      this.m_headerText.Text = string.Empty;
  }
}
