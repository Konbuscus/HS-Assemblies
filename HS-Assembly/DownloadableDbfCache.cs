﻿// Decompiled with JetBrains decompiler
// Type: DownloadableDbfCache
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using PegasusUtil;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public static class DownloadableDbfCache
{
  private static Map<int, KeyValuePair<AssetRecordInfo, DownloadableDbfCache.LoadCachedAssetCallback>> s_assetRequests = new Map<int, KeyValuePair<AssetRecordInfo, DownloadableDbfCache.LoadCachedAssetCallback>>();
  private static HashSet<AssetKey> s_requiredClientStaticAssetsStillPending = new HashSet<AssetKey>();
  private static int s_nextCallbackToken = -1;
  private const int PRUNE_CACHED_ASSETS_MAX_AGE_DAYS = 124;

  public static bool IsRequiredClientStaticAssetsStillPending
  {
    get
    {
      if (NetCache.Get().GetNetObject<ClientStaticAssetsResponse>() == null)
        return true;
      return DownloadableDbfCache.s_requiredClientStaticAssetsStillPending.Count > 0;
    }
  }

  private static int NextCallbackToken
  {
    get
    {
      return ++DownloadableDbfCache.s_nextCallbackToken;
    }
  }

  public static void Initialize()
  {
    Network.Get().RegisterNetHandler((object) GetAssetResponse.PacketID.ID, new Network.NetHandler(DownloadableDbfCache.Network_OnGetAssetResponse), (Network.TimeoutHandler) null);
    NetCache.Get().RegisterUpdatedListener(typeof (ClientStaticAssetsResponse), new Action(DownloadableDbfCache.NetCache_OnClientStaticAssetsResponse));
  }

  public static bool IsAssetRequestInProgress(int assetId, AssetType assetType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return DownloadableDbfCache.s_assetRequests.Any<KeyValuePair<int, KeyValuePair<AssetRecordInfo, DownloadableDbfCache.LoadCachedAssetCallback>>>(new Func<KeyValuePair<int, KeyValuePair<AssetRecordInfo, DownloadableDbfCache.LoadCachedAssetCallback>>, bool>(new DownloadableDbfCache.\u003CIsAssetRequestInProgress\u003Ec__AnonStorey43B() { assetId = assetId, assetType = assetType }.\u003C\u003Em__2E4));
  }

  public static bool IsAssetRequestInProgress(AssetKey assetKey)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return DownloadableDbfCache.s_assetRequests.Any<KeyValuePair<int, KeyValuePair<AssetRecordInfo, DownloadableDbfCache.LoadCachedAssetCallback>>>(new Func<KeyValuePair<int, KeyValuePair<AssetRecordInfo, DownloadableDbfCache.LoadCachedAssetCallback>>, bool>(new DownloadableDbfCache.\u003CIsAssetRequestInProgress\u003Ec__AnonStorey43C() { assetKey = assetKey }.\u003C\u003Em__2E5));
  }

  public static bool LoadCachedAssets(bool canRequestFromServer, DownloadableDbfCache.LoadCachedAssetCallback cb, params AssetRecordInfo[] assets)
  {
    if (assets.Length == 0)
      return false;
    List<AssetKey> requestKeys = new List<AssetKey>();
    byte[] assetBytes = (byte[]) null;
    foreach (AssetRecordInfo asset in assets)
    {
      if (asset != null)
      {
        if (asset.RecordHash == null)
        {
          if ((int) asset.RecordByteSize == 0)
            requestKeys.Add(asset.Asset);
        }
        else
        {
          bool flag = false;
          string cachedAssetFilePath = DownloadableDbfCache.GetCachedAssetFilePath(asset.Asset.Type, asset.Asset.AssetId, asset.RecordHash);
          if (!File.Exists(cachedAssetFilePath))
          {
            flag = (int) asset.RecordByteSize != 0;
            if (!flag)
              DownloadableDbfCache.s_requiredClientStaticAssetsStillPending.Remove(asset.Asset);
            try
            {
              Directory.CreateDirectory(DownloadableDbfCache.GetCachedAssetFolder(asset.Asset.Type));
            }
            catch (Exception ex)
            {
              Error.AddDevFatal("Error creating cached asset folder {0}:\n{1}", (object) cachedAssetFilePath, (object) ex.ToString());
              return false;
            }
          }
          else
          {
            try
            {
              if (new FileInfo(cachedAssetFilePath).Length != (long) asset.RecordByteSize)
                flag = true;
              else if ((int) asset.RecordByteSize != 0)
              {
                byte[] numArray = File.ReadAllBytes(cachedAssetFilePath);
                if (GeneralUtils.AreArraysEqual<byte>(System.Security.Cryptography.SHA1.Create().ComputeHash(numArray, 0, numArray.Length), asset.RecordHash))
                {
                  Log.Downloader.Print("LoadCachedAsset: locally available=true {0} id={1} hash={2}", new object[3]
                  {
                    (object) asset.Asset.Type,
                    (object) asset.Asset.AssetId,
                    (object) (asset.RecordHash != null ? asset.RecordHash.ToHexString() : "<null>")
                  });
                  if (assetBytes == null)
                    assetBytes = numArray;
                  DownloadableDbfCache.SetCachedAssetIntoDbfSystem(asset.Asset.Type, numArray);
                  DownloadableDbfCache.s_requiredClientStaticAssetsStillPending.Remove(asset.Asset);
                }
                else
                  flag = true;
              }
            }
            catch (Exception ex)
            {
              Error.AddDevFatal("Error reading cached asset folder {0}:\n{1}", (object) cachedAssetFilePath, (object) ex.ToString());
              requestKeys.Add(asset.Asset);
            }
          }
          if (flag)
          {
            requestKeys.Add(asset.Asset);
            if (canRequestFromServer)
              Log.Downloader.Print("LoadCachedAsset: locally available=false, requesting from server {0} id={1} hash={2}", new object[3]
              {
                (object) asset.Asset.Type,
                (object) asset.Asset.AssetId,
                (object) (asset.RecordHash != null ? asset.RecordHash.ToHexString() : "<null>")
              });
            else
              Log.Downloader.Print("LoadCachedAsset: locally available=false, not requesting from server yet - {0} id={1} hash={2}", new object[3]
              {
                (object) asset.Asset.Type,
                (object) asset.Asset.AssetId,
                (object) (asset.RecordHash != null ? asset.RecordHash.ToHexString() : "<null>")
              });
          }
        }
      }
    }
    AssetRecordInfo asset1 = assets[0];
    if (requestKeys.Count > 0)
    {
      if (canRequestFromServer)
      {
        int nextCallbackToken = DownloadableDbfCache.NextCallbackToken;
        if (cb != null)
          DownloadableDbfCache.s_assetRequests[nextCallbackToken] = new KeyValuePair<AssetRecordInfo, DownloadableDbfCache.LoadCachedAssetCallback>(asset1, cb);
        Network.SendAssetRequest(nextCallbackToken, requestKeys);
      }
    }
    else if (asset1 != null && cb != null)
    {
      if (assetBytes == null)
        assetBytes = new byte[0];
      cb(asset1.Asset, PegasusShared.ErrorCode.ERROR_OK, assetBytes);
    }
    return requestKeys.Count == 0;
  }

  private static string GetCachedAssetFolder(AssetType assetType)
  {
    string str;
    switch (assetType)
    {
      case AssetType.ASSET_TYPE_SCENARIO:
        str = "Scenario";
        break;
      case AssetType.ASSET_TYPE_SUBSET_CARD:
        str = "Subset";
        break;
      case AssetType.ASSET_TYPE_DECK_RULESET:
        str = "DeckRuleset";
        break;
      default:
        str = "Other";
        break;
    }
    return string.Format("{0}/{1}", (object) FileUtils.CachePath, (object) str);
  }

  private static string GetCachedAssetFileExtension(AssetType assetType)
  {
    switch (assetType)
    {
      case AssetType.ASSET_TYPE_SCENARIO:
        return "scen";
      case AssetType.ASSET_TYPE_SUBSET_CARD:
        return "subset_card";
      case AssetType.ASSET_TYPE_DECK_RULESET:
        return "deck_ruleset";
      default:
        return assetType.ToString().Replace("ASSET_TYPE_", string.Empty).ToLower();
    }
  }

  private static string GetCachedAssetFilePath(AssetType assetType, int assetId, byte[] assetHash)
  {
    string cachedAssetFolder = DownloadableDbfCache.GetCachedAssetFolder(assetType);
    string assetFileExtension = DownloadableDbfCache.GetCachedAssetFileExtension(assetType);
    return string.Format("{0}/{1}_{2}.{3}", (object) cachedAssetFolder, (object) assetId, (object) assetHash.ToHexString(), (object) assetFileExtension);
  }

  private static void StoreReceivedAssetIntoLocalCache(AssetType assetType, int assetId, byte[] assetBytes, int assetBytesLength)
  {
    byte[] hash = System.Security.Cryptography.SHA1.Create().ComputeHash(assetBytes, 0, assetBytesLength);
    string cachedAssetFilePath = DownloadableDbfCache.GetCachedAssetFilePath(assetType, assetId, hash);
    try
    {
      using (FileStream fileStream = new FileStream(cachedAssetFilePath, FileMode.Create))
        fileStream.Write(assetBytes, 0, assetBytesLength);
    }
    catch (Exception ex)
    {
      Error.AddDevFatal("Error saving cached asset {0}:\n{1}", (object) cachedAssetFilePath, (object) ex.ToString());
    }
  }

  private static void SetCachedAssetIntoDbfSystem(AssetType assetType, byte[] assetBytes)
  {
    switch (assetType)
    {
      case AssetType.ASSET_TYPE_SCENARIO:
        DownloadableDbfCache.SetCachedAssetIntoDbfSystem_Scenario(ProtobufUtil.ParseFrom<ScenarioDbRecord>(assetBytes, 0, assetBytes.Length));
        break;
      case AssetType.ASSET_TYPE_SUBSET_CARD:
        DownloadableDbfCache.SetCachedAssetIntoDbfSystem_SubsetCard(ProtobufUtil.ParseFrom<SubsetCardListDbRecord>(assetBytes, 0, assetBytes.Length));
        break;
      case AssetType.ASSET_TYPE_DECK_RULESET:
        DownloadableDbfCache.SetCachedAssetIntoDbfSystem_DeckRuleset(ProtobufUtil.ParseFrom<DeckRulesetDbRecord>(assetBytes, 0, assetBytes.Length));
        break;
    }
  }

  private static void SetCachedAssetIntoDbfSystem_Scenario(ScenarioDbRecord protoScenario)
  {
    ScenarioDbfRecord record = DbfUtils.ConvertFromProtobuf(protoScenario);
    if (record == null)
      Log.Downloader.Print("DbfUtils.ConvertFromProtobuf(protoScenario) returned null:\n{0}", (object) (protoScenario != null ? protoScenario.ToString() : "(null)"));
    else
      GameDbf.Scenario.ReplaceRecordByRecordId(record);
  }

  private static void SetCachedAssetIntoDbfSystem_DeckRuleset(DeckRulesetDbRecord proto)
  {
    DeckRulesetDbfRecord record = DbfUtils.ConvertFromProtobuf(proto);
    if (record == null)
      Log.Downloader.Print("DbfUtils.ConvertFromProtobuf(proto) returned null:\n{0}", (object) (proto != null ? proto.ToString() : "(null)"));
    else
      GameDbf.DeckRuleset.ReplaceRecordByRecordId(record);
    using (List<DeckRulesetRuleDbRecord>.Enumerator enumerator = proto.Rules.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckRulesetRuleDbRecord current = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        DownloadableDbfCache.\u003CSetCachedAssetIntoDbfSystem_DeckRuleset\u003Ec__AnonStorey43D rulesetCAnonStorey43D = new DownloadableDbfCache.\u003CSetCachedAssetIntoDbfSystem_DeckRuleset\u003Ec__AnonStorey43D();
        List<int> outTargetSubsetIds;
        // ISSUE: reference to a compiler-generated field
        rulesetCAnonStorey43D.dbfRule = DbfUtils.ConvertFromProtobuf(current, out outTargetSubsetIds);
        // ISSUE: reference to a compiler-generated field
        GameDbf.DeckRulesetRule.ReplaceRecordByRecordId(rulesetCAnonStorey43D.dbfRule);
        // ISSUE: reference to a compiler-generated method
        GameDbf.DeckRulesetRuleSubset.RemoveRecordsWhere(new Predicate<DeckRulesetRuleSubsetDbfRecord>(rulesetCAnonStorey43D.\u003C\u003Em__2E6));
        if (outTargetSubsetIds != null)
        {
          for (int index = 0; index < outTargetSubsetIds.Count; ++index)
          {
            DeckRulesetRuleSubsetDbfRecord ruleSubsetDbfRecord = new DeckRulesetRuleSubsetDbfRecord();
            // ISSUE: reference to a compiler-generated field
            ruleSubsetDbfRecord.SetDeckRulesetRuleId(rulesetCAnonStorey43D.dbfRule.ID);
            ruleSubsetDbfRecord.SetSubsetId(outTargetSubsetIds[index]);
            GameDbf.DeckRulesetRuleSubset.AddRecord((DbfRecord) ruleSubsetDbfRecord);
          }
        }
      }
    }
  }

  private static void SetCachedAssetIntoDbfSystem_SubsetCard(SubsetCardListDbRecord proto)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DownloadableDbfCache.\u003CSetCachedAssetIntoDbfSystem_SubsetCard\u003Ec__AnonStorey43E cardCAnonStorey43E = new DownloadableDbfCache.\u003CSetCachedAssetIntoDbfSystem_SubsetCard\u003Ec__AnonStorey43E();
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey43E.dbf = GameDbf.Subset.GetRecord(proto.SubsetId);
    // ISSUE: reference to a compiler-generated field
    if (cardCAnonStorey43E.dbf == null)
    {
      // ISSUE: reference to a compiler-generated field
      cardCAnonStorey43E.dbf = new SubsetDbfRecord();
      // ISSUE: reference to a compiler-generated field
      cardCAnonStorey43E.dbf.SetID(proto.SubsetId);
      // ISSUE: reference to a compiler-generated field
      GameDbf.Subset.AddRecord((DbfRecord) cardCAnonStorey43E.dbf);
    }
    // ISSUE: reference to a compiler-generated method
    GameDbf.SubsetCard.RemoveRecordsWhere(new Predicate<SubsetCardDbfRecord>(cardCAnonStorey43E.\u003C\u003Em__2E7));
    using (List<int>.Enumerator enumerator = proto.CardIds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        SubsetCardDbfRecord subsetCardDbfRecord = new SubsetCardDbfRecord();
        // ISSUE: reference to a compiler-generated field
        subsetCardDbfRecord.SetSubsetId(cardCAnonStorey43E.dbf.ID);
        subsetCardDbfRecord.SetCardId(current);
        GameDbf.SubsetCard.AddRecord((DbfRecord) subsetCardDbfRecord);
      }
    }
  }

  private static void NetCache_OnClientStaticAssetsResponse()
  {
    ClientStaticAssetsResponse netObject = NetCache.Get().GetNetObject<ClientStaticAssetsResponse>();
    if (netObject == null)
      return;
    using (List<AssetRecordInfo>.Enumerator enumerator = netObject.AssetsToGet.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AssetRecordInfo current = enumerator.Current;
        DownloadableDbfCache.s_requiredClientStaticAssetsStillPending.Add(current.Asset);
      }
    }
    DownloadableDbfCache.LoadCachedAssets(true, (DownloadableDbfCache.LoadCachedAssetCallback) null, netObject.AssetsToGet.ToArray());
  }

  private static void Network_OnGetAssetResponse()
  {
    GetAssetResponse assetResponse = Network.GetAssetResponse();
    if (assetResponse == null)
      return;
    PegasusShared.ErrorCode code = PegasusShared.ErrorCode.ERROR_OK;
    Map<AssetKey, byte[]> map = new Map<AssetKey, byte[]>();
    for (int index = 0; index < assetResponse.Responses.Count; ++index)
    {
      AssetResponse response = assetResponse.Responses[index];
      if (response.ErrorCode == PegasusShared.ErrorCode.ERROR_OK)
      {
        DownloadableDbfCache.s_requiredClientStaticAssetsStillPending.Remove(response.RequestedKey);
      }
      else
      {
        Log.Downloader.Print("Network_OnGetAssetResponse: error={0}:{1} type={2}:{3} id={4}", (object) response.ErrorCode, (object) response.ErrorCode.ToString(), (object) response.RequestedKey.Type, (object) response.RequestedKey.Type.ToString(), (object) response.RequestedKey.AssetId);
        if (code == PegasusShared.ErrorCode.ERROR_OK)
          code = response.ErrorCode;
        if (DownloadableDbfCache.s_requiredClientStaticAssetsStillPending.Contains(response.RequestedKey))
        {
          Error.AddDevFatal(GameStrings.Get("GLUE_REQUIRED_CLIENT_STATIC_ASSETS_ERROR_MESSAGE"));
          return;
        }
      }
      AssetKey requestedKey = response.RequestedKey;
      byte[] assetBytes = (byte[]) null;
      if (response.HasScenarioAsset)
        assetBytes = ProtobufUtil.ToByteArray((IProtoBuf) response.ScenarioAsset);
      if (response.HasSubsetCardListAsset)
        assetBytes = ProtobufUtil.ToByteArray((IProtoBuf) response.SubsetCardListAsset);
      if (response.HasDeckRulesetAsset)
        assetBytes = ProtobufUtil.ToByteArray((IProtoBuf) response.DeckRulesetAsset);
      if (assetBytes != null)
      {
        map[requestedKey] = assetBytes;
        DownloadableDbfCache.StoreReceivedAssetIntoLocalCache(requestedKey.Type, requestedKey.AssetId, assetBytes, assetBytes.Length);
        DownloadableDbfCache.SetCachedAssetIntoDbfSystem(requestedKey.Type, assetBytes);
      }
    }
    ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(DownloadableDbfCache.PruneCachedAssetFiles), (object) null);
    ApplicationMgr.Get().ScheduleCallback(5f, true, new ApplicationMgr.ScheduledCallback(DownloadableDbfCache.PruneCachedAssetFiles), (object) null);
    KeyValuePair<AssetRecordInfo, DownloadableDbfCache.LoadCachedAssetCallback> keyValuePair;
    if (!DownloadableDbfCache.s_assetRequests.TryGetValue(assetResponse.ClientToken, out keyValuePair))
      return;
    AssetRecordInfo key = keyValuePair.Key;
    DownloadableDbfCache.LoadCachedAssetCallback cb = keyValuePair.Value;
    DownloadableDbfCache.s_assetRequests.Remove(assetResponse.ClientToken);
    byte[] assetBytes1;
    if (!map.TryGetValue(key.Asset, out assetBytes1))
    {
      if (DownloadableDbfCache.LoadCachedAssets(0 != 0, cb, key))
        return;
      assetBytes1 = new byte[0];
    }
    cb(key.Asset, code, assetBytes1);
  }

  private static void PruneCachedAssetFiles(object userData)
  {
    string cachePath = FileUtils.CachePath;
    string message = (string) null;
    string str = (string) null;
    try
    {
      DirectoryInfo directoryInfo = new DirectoryInfo(cachePath);
      if (!directoryInfo.Exists)
        return;
      foreach (DirectoryInfo directory in directoryInfo.GetDirectories())
      {
        message = directory.FullName;
        foreach (FileInfo file in directory.GetFiles())
        {
          str = file.Name;
          TimeSpan timeSpan = DateTime.Now - file.LastWriteTime;
          if (file.LastWriteTime < DateTime.Now && timeSpan.TotalDays > 124.0)
            file.Delete();
        }
      }
    }
    catch (Exception ex)
    {
      Error.AddDevWarning("Error pruning dir={0} file={1}:\n{2}", message, (object) str, (object) ex.ToString());
    }
  }

  public delegate void LoadCachedAssetCallback(AssetKey requestedKey, PegasusShared.ErrorCode code, byte[] assetBytes);
}
