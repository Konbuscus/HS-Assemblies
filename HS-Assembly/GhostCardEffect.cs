﻿// Decompiled with JetBrains decompiler
// Type: GhostCardEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class GhostCardEffect : Spell
{
  public GameObject m_Glow;
  public GameObject m_GlowUnique;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    if ((Object) this.m_Glow != (Object) null)
      this.m_Glow.GetComponent<Renderer>().enabled = false;
    if ((Object) this.m_GlowUnique != (Object) null)
      this.m_GlowUnique.GetComponent<Renderer>().enabled = false;
    this.StartCoroutine(this.GhostEffect(prevStateType));
  }

  protected override void OnDeath(SpellStateType prevStateType)
  {
    if ((Object) this.m_Glow != (Object) null)
      this.m_Glow.GetComponent<Renderer>().enabled = false;
    if ((Object) this.m_GlowUnique != (Object) null)
      this.m_GlowUnique.GetComponent<Renderer>().enabled = false;
    base.OnDeath(prevStateType);
    this.OnSpellFinished();
  }

  [DebuggerHidden]
  private IEnumerator GhostEffect(SpellStateType prevStateType)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GhostCardEffect.\u003CGhostEffect\u003Ec__Iterator2B9() { prevStateType = prevStateType, \u003C\u0024\u003EprevStateType = prevStateType, \u003C\u003Ef__this = this };
  }
}
