﻿// Decompiled with JetBrains decompiler
// Type: GameMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using PegasusClient;
using PegasusGame;
using PegasusShared;
using SpectatorProto;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GameMgr
{
  private static GameMgr s_instance = new GameMgr();
  private static Map<QueueEvent.Type, FindGameState?> s_bnetToFindGameResultMap = new Map<QueueEvent.Type, FindGameState?>() { { QueueEvent.Type.UNKNOWN, new FindGameState?() }, { QueueEvent.Type.QUEUE_ENTER, new FindGameState?(FindGameState.BNET_QUEUE_ENTERED) }, { QueueEvent.Type.QUEUE_LEAVE, new FindGameState?() }, { QueueEvent.Type.QUEUE_DELAY, new FindGameState?(FindGameState.BNET_QUEUE_DELAYED) }, { QueueEvent.Type.QUEUE_UPDATE, new FindGameState?(FindGameState.BNET_QUEUE_UPDATED) }, { QueueEvent.Type.QUEUE_DELAY_ERROR, new FindGameState?(FindGameState.BNET_ERROR) }, { QueueEvent.Type.QUEUE_AMM_ERROR, new FindGameState?(FindGameState.BNET_ERROR) }, { QueueEvent.Type.QUEUE_WAIT_END, new FindGameState?() }, { QueueEvent.Type.QUEUE_CANCEL, new FindGameState?(FindGameState.BNET_QUEUE_CANCELED) }, { QueueEvent.Type.QUEUE_GAME_STARTED, new FindGameState?(FindGameState.SERVER_GAME_CONNECTING) }, { QueueEvent.Type.ABORT_CLIENT_DROPPED, new FindGameState?(FindGameState.BNET_ERROR) } };
  private readonly Map<string, System.Type> s_transitionPopupNameToType = new Map<string, System.Type>() { { "MatchingPopup3D", typeof (MatchingPopupDisplay) }, { "MatchingPopup3D_phone", typeof (MatchingPopupDisplay) }, { "LoadingPopup", typeof (LoadingPopupDisplay) } };
  private List<GameMgr.FindGameListener> m_findGameListeners = new List<GameMgr.FindGameListener>();
  private Map<int, string> m_lastDisplayedPlayerNames = new Map<int, string>();
  private const string MATCHING_POPUP_PC_NAME = "MatchingPopup3D";
  private const string MATCHING_POPUP_PHONE_NAME = "MatchingPopup3D_phone";
  private const string LOADING_POPUP_NAME = "LoadingPopup";
  private const int MINIMUM_SECONDS_TIL_TB_END_TO_RETURN_TO_TB_SCENE = 10;
  private PlatformDependentValue<string> MATCHING_POPUP_NAME;
  private GameType m_gameType;
  private GameType m_prevGameType;
  private GameType m_nextGameType;
  private FormatType m_formatType;
  private FormatType m_prevFormatType;
  private FormatType m_nextFormatType;
  private int m_missionId;
  private int m_prevMissionId;
  private int m_nextMissionId;
  private ReconnectType m_reconnectType;
  private ReconnectType m_prevReconnectType;
  private ReconnectType m_nextReconnectType;
  private bool m_spectator;
  private bool m_prevSpectator;
  private bool m_nextSpectator;
  private long? m_lastDeckId;
  private long? m_lastAIDeckId;
  private uint m_lastEnterGameError;
  private bool m_pendingAutoConcede;
  private FindGameState m_findGameState;
  private TransitionPopup m_transitionPopup;
  private Vector3 m_initialTransitionPopupPos;
  private Network.GameSetup m_gameSetup;

  public static GameMgr Get()
  {
    return GameMgr.s_instance;
  }

  public void Initialize()
  {
    this.MATCHING_POPUP_NAME = new PlatformDependentValue<string>(PlatformCategory.Screen)
    {
      PC = "MatchingPopup3D",
      Phone = "MatchingPopup3D_phone"
    };
    ApplicationMgr.Get().WillReset += new System.Action(this.WillReset);
  }

  public void OnLoggedIn()
  {
    Network network = Network.Get();
    network.RegisterGameQueueHandler(new Network.GameQueueHandler(this.OnGameQueueEvent));
    network.RegisterNetHandler((object) PegasusGame.GameSetup.PacketID.ID, new Network.NetHandler(this.OnGameSetup), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) GameCanceled.PacketID.ID, new Network.NetHandler(this.OnGameCanceled), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) ServerResult.PacketID.ID, new Network.NetHandler(this.OnServerResult), (Network.TimeoutHandler) null);
    network.AddBnetErrorListener(BnetFeature.Games, new Network.BnetErrorCallback(this.OnBnetError));
    SceneMgr.Get().RegisterSceneUnloadedEvent(new SceneMgr.SceneUnloadedCallback(this.OnSceneUnloaded));
    SceneMgr.Get().RegisterScenePreLoadEvent(new SceneMgr.ScenePreLoadCallback(this.OnScenePreLoad));
    ReconnectMgr.Get().AddTimeoutListener(new ReconnectMgr.TimeoutCallback(this.OnReconnectTimeout));
  }

  public GameType GetGameType()
  {
    return this.m_gameType;
  }

  public GameType GetPreviousGameType()
  {
    return this.m_prevGameType;
  }

  public GameType GetNextGameType()
  {
    return this.m_nextGameType;
  }

  public FormatType GetFormatType()
  {
    return this.m_formatType;
  }

  public FormatType GetPreviousFormatType()
  {
    return this.m_prevFormatType;
  }

  public FormatType GetNextFormatType()
  {
    return this.m_nextFormatType;
  }

  public int GetMissionId()
  {
    return this.m_missionId;
  }

  public int GetPreviousMissionId()
  {
    return this.m_prevMissionId;
  }

  public int GetNextMissionId()
  {
    return this.m_nextMissionId;
  }

  public ReconnectType GetReconnectType()
  {
    return this.m_reconnectType;
  }

  public ReconnectType GetPreviousReconnectType()
  {
    return this.m_prevReconnectType;
  }

  public ReconnectType GetNextReconnectType()
  {
    return this.m_nextReconnectType;
  }

  public bool IsReconnect()
  {
    return this.m_reconnectType != ReconnectType.INVALID;
  }

  public bool IsPreviousReconnect()
  {
    return this.m_prevReconnectType != ReconnectType.INVALID;
  }

  public bool IsNextReconnect()
  {
    return this.m_nextReconnectType != ReconnectType.INVALID;
  }

  public bool IsSpectator()
  {
    return this.m_spectator;
  }

  public bool WasSpectator()
  {
    return this.m_prevSpectator;
  }

  public bool IsNextSpectator()
  {
    return this.m_nextSpectator;
  }

  public uint GetLastEnterGameError()
  {
    return this.m_lastEnterGameError;
  }

  public bool IsPendingAutoConcede()
  {
    return this.m_pendingAutoConcede;
  }

  public void SetPendingAutoConcede(bool pendingAutoConcede)
  {
    if (!Network.IsConnectedToGameServer())
      return;
    this.m_pendingAutoConcede = pendingAutoConcede;
  }

  public Network.GameSetup GetGameSetup()
  {
    return this.m_gameSetup;
  }

  public FindGameState GetFindGameState()
  {
    return this.m_findGameState;
  }

  public bool IsFindingGame()
  {
    return this.m_findGameState != FindGameState.INVALID;
  }

  public bool IsAboutToStopFindingGame()
  {
    switch (this.m_findGameState)
    {
      case FindGameState.CLIENT_CANCELED:
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
      case FindGameState.SERVER_GAME_STARTED:
      case FindGameState.SERVER_GAME_CANCELED:
        return true;
      default:
        return false;
    }
  }

  public void RegisterFindGameEvent(GameMgr.FindGameCallback callback)
  {
    this.RegisterFindGameEvent(callback, (object) null);
  }

  public void RegisterFindGameEvent(GameMgr.FindGameCallback callback, object userData)
  {
    GameMgr.FindGameListener findGameListener = new GameMgr.FindGameListener();
    findGameListener.SetCallback(callback);
    findGameListener.SetUserData(userData);
    if (this.m_findGameListeners.Contains(findGameListener))
      return;
    this.m_findGameListeners.Add(findGameListener);
  }

  public bool UnregisterFindGameEvent(GameMgr.FindGameCallback callback)
  {
    return this.UnregisterFindGameEvent(callback, (object) null);
  }

  public bool UnregisterFindGameEvent(GameMgr.FindGameCallback callback, object userData)
  {
    GameMgr.FindGameListener findGameListener = new GameMgr.FindGameListener();
    findGameListener.SetCallback(callback);
    findGameListener.SetUserData(userData);
    return this.m_findGameListeners.Remove(findGameListener);
  }

  private void FindGameInternal(GameType gameType, FormatType formatType, int missionId, long deckId, long aiDeckId)
  {
    this.m_lastEnterGameError = 0U;
    this.m_nextGameType = gameType;
    this.m_nextFormatType = formatType;
    this.m_nextMissionId = missionId;
    this.m_lastDeckId = new long?(deckId);
    this.m_lastAIDeckId = new long?(aiDeckId);
    this.ChangeFindGameState(FindGameState.CLIENT_STARTED);
    Network.Get().FindGame(gameType, formatType, missionId, deckId, aiDeckId);
    this.UpdateSessionPresence(gameType);
  }

  public void FindGame(GameType gameType, FormatType formatType, int missionId, long deckId = 0, long aiDeckId = 0)
  {
    this.FindGameInternal(gameType, formatType, missionId, deckId, aiDeckId);
    string popupForFindGame = this.DetermineTransitionPopupForFindGame(gameType, formatType, missionId);
    if (popupForFindGame == null)
      return;
    this.ShowTransitionPopup(popupForFindGame);
  }

  public void RestartGame()
  {
    int gameType = (int) this.m_gameType;
    int formatType = (int) this.m_formatType;
    int missionId = this.m_missionId;
    long? lastDeckId = this.m_lastDeckId;
    long deckId = !lastDeckId.HasValue ? 0L : lastDeckId.Value;
    long? lastAiDeckId = this.m_lastAIDeckId;
    long aiDeckId = !lastAiDeckId.HasValue ? 0L : lastAiDeckId.Value;
    this.FindGameInternal((GameType) gameType, (FormatType) formatType, missionId, deckId, aiDeckId);
  }

  public bool HasLastPlayedDeckId()
  {
    return this.m_lastDeckId.HasValue;
  }

  public void EnterFriendlyChallengeGame(FormatType formatType, int missionId, long player1DeckId, long player2DeckId, BnetGameAccountId player2GameAccountId)
  {
    Network.Get().EnterFriendlyChallengeGame(formatType, missionId, player1DeckId, player2DeckId, player2GameAccountId);
  }

  public void WaitForFriendChallengeToStart(FormatType formatType, int missionId)
  {
    this.m_nextGameType = GameType.GT_VS_FRIEND;
    this.m_nextFormatType = formatType;
    this.m_nextMissionId = missionId;
    this.m_lastEnterGameError = 0U;
    this.ChangeFindGameState(FindGameState.CLIENT_STARTED);
    this.ShowTransitionPopup("LoadingPopup");
  }

  public void SpectateGame(JoinInfo joinInfo)
  {
    bgs.types.GameServerInfo serverInfo = new bgs.types.GameServerInfo();
    serverInfo.Address = joinInfo.ServerIpAddress;
    serverInfo.Port = (int) joinInfo.ServerPort;
    serverInfo.GameHandle = joinInfo.GameHandle;
    serverInfo.SpectatorPassword = joinInfo.SecretKey;
    serverInfo.SpectatorMode = true;
    this.m_nextGameType = joinInfo.GameType;
    this.m_nextFormatType = joinInfo.FormatType;
    this.m_nextMissionId = joinInfo.MissionId;
    this.m_nextSpectator = true;
    this.m_lastEnterGameError = 0U;
    this.ChangeFindGameState(FindGameState.CLIENT_STARTED);
    this.ShowTransitionPopup("LoadingPopup");
    this.ChangeFindGameState(FindGameState.SERVER_GAME_CONNECTING, serverInfo);
    if (!((UnityEngine.Object) Gameplay.Get() == (UnityEngine.Object) null))
      return;
    Network.Get().SetGameServerDisconnectEventListener(new Network.GameServerDisconnectEvent(this.OnGameServerDisconnect));
  }

  private void OnGameServerDisconnect(BattleNetErrors error)
  {
    this.OnGameCanceled();
  }

  public void ReconnectGame(GameType gameType, FormatType formatType, ReconnectType reconnectType, bgs.types.GameServerInfo serverInfo)
  {
    this.m_nextGameType = gameType;
    this.m_nextFormatType = formatType;
    this.m_nextMissionId = serverInfo.Mission;
    this.m_nextReconnectType = reconnectType;
    this.m_nextSpectator = serverInfo.SpectatorMode;
    this.m_lastEnterGameError = 0U;
    this.ChangeFindGameState(FindGameState.CLIENT_STARTED);
    this.ChangeFindGameState(FindGameState.SERVER_GAME_CONNECTING, serverInfo);
  }

  public bool CancelFindGame()
  {
    if (!GameUtils.IsMatchmadeGameType(this.m_nextGameType) || !Network.Get().IsFindingGame())
      return false;
    Network.Get().CancelFindGame();
    this.ChangeFindGameState(FindGameState.CLIENT_CANCELED);
    return true;
  }

  public GameEntity CreateGameEntity()
  {
    ScenarioDbId missionId = (ScenarioDbId) this.m_missionId;
    switch (missionId)
    {
      case ScenarioDbId.TUTORIAL_ILLIDAN:
        return (GameEntity) new Tutorial_05();
      case ScenarioDbId.TUTORIAL_CHO:
        return (GameEntity) new Tutorial_06();
      case ScenarioDbId.NAXX_ANUBREKHAN:
      case ScenarioDbId.NAXX_HEROIC_ANUBREKHAN:
        return (GameEntity) new NAX01_AnubRekhan();
      case ScenarioDbId.NAXX_FAERLINA:
      case ScenarioDbId.NAXX_CHALLENGE_DRUID_V_FAERLINA:
      case ScenarioDbId.NAXX_HEROIC_FAERLINA:
        return (GameEntity) new NAX02_Faerlina();
      case ScenarioDbId.NAXX_NOTH:
      case ScenarioDbId.NAXX_HEROIC_NOTH:
        return (GameEntity) new NAX04_Noth();
      case ScenarioDbId.NAXX_HEIGAN:
      case ScenarioDbId.NAXX_CHALLENGE_MAGE_V_HEIGAN:
      case ScenarioDbId.NAXX_HEROIC_HEIGAN:
        return (GameEntity) new NAX05_Heigan();
      case ScenarioDbId.NAXX_LOATHEB:
      case ScenarioDbId.NAXX_CHALLENGE_HUNTER_V_LOATHEB:
      case ScenarioDbId.NAXX_HEROIC_LOATHEB:
        return (GameEntity) new NAX06_Loatheb();
      case ScenarioDbId.NAXX_MAEXXNA:
      case ScenarioDbId.NAXX_CHALLENGE_ROGUE_V_MAEXXNA:
      case ScenarioDbId.NAXX_HEROIC_MAEXXNA:
        return (GameEntity) new NAX03_Maexxna();
      case ScenarioDbId.NAXX_RAZUVIOUS:
      case ScenarioDbId.NAXX_HEROIC_RAZUVIOUS:
        return (GameEntity) new NAX07_Razuvious();
      case ScenarioDbId.NAXX_GOTHIK:
      case ScenarioDbId.NAXX_CHALLENGE_SHAMAN_V_GOTHIK:
      case ScenarioDbId.NAXX_HEROIC_GOTHIK:
        return (GameEntity) new NAX08_Gothik();
      case ScenarioDbId.NAXX_HORSEMEN:
      case ScenarioDbId.NAXX_CHALLENGE_WARLOCK_V_HORSEMEN:
      case ScenarioDbId.NAXX_HEROIC_HORSEMEN:
        return (GameEntity) new NAX09_Horsemen();
      case ScenarioDbId.NAXX_PATCHWERK:
      case ScenarioDbId.NAXX_HEROIC_PATCHWERK:
        return (GameEntity) new NAX10_Patchwerk();
      case ScenarioDbId.NAXX_GROBBULUS:
      case ScenarioDbId.NAXX_CHALLENGE_WARRIOR_V_GROBBULUS:
      case ScenarioDbId.NAXX_HEROIC_GROBBULUS:
        return (GameEntity) new NAX11_Grobbulus();
      case ScenarioDbId.NAXX_GLUTH:
      case ScenarioDbId.NAXX_HEROIC_GLUTH:
        return (GameEntity) new NAX12_Gluth();
      case ScenarioDbId.NAXX_THADDIUS:
      case ScenarioDbId.NAXX_CHALLENGE_PRIEST_V_THADDIUS:
      case ScenarioDbId.NAXX_HEROIC_THADDIUS:
        return (GameEntity) new NAX13_Thaddius();
      case ScenarioDbId.NAXX_KELTHUZAD:
      case ScenarioDbId.NAXX_CHALLENGE_PALADIN_V_KELTHUZAD:
      case ScenarioDbId.NAXX_HEROIC_KELTHUZAD:
        return (GameEntity) new NAX15_KelThuzad();
      case ScenarioDbId.NAXX_SAPPHIRON:
      case ScenarioDbId.NAXX_HEROIC_SAPPHIRON:
        return (GameEntity) new NAX14_Sapphiron();
      case ScenarioDbId.BRM_GRIM_GUZZLER:
      case ScenarioDbId.BRM_HEROIC_GRIM_GUZZLER:
      case ScenarioDbId.BRM_CHALLENGE_HUNTER_V_GUZZLER:
        return (GameEntity) new BRM01_GrimGuzzler();
      case ScenarioDbId.BRM_DARK_IRON_ARENA:
      case ScenarioDbId.BRM_HEROIC_DARK_IRON_ARENA:
      case ScenarioDbId.BRM_CHALLENGE_MAGE_V_DARK_IRON_ARENA:
        return (GameEntity) new BRM02_DarkIronArena();
      case ScenarioDbId.BRM_THAURISSAN:
      case ScenarioDbId.BRM_HEROIC_THAURISSAN:
        return (GameEntity) new BRM03_Thaurissan();
      case ScenarioDbId.BRM_GARR:
      case ScenarioDbId.BRM_HEROIC_GARR:
      case ScenarioDbId.BRM_CHALLENGE_WARRIOR_V_GARR:
        return (GameEntity) new BRM04_Garr();
      case ScenarioDbId.BRM_BARON_GEDDON:
      case ScenarioDbId.BRM_HEROIC_BARON_GEDDON:
      case ScenarioDbId.BRM_CHALLENGE_SHAMAN_V_GEDDON:
        return (GameEntity) new BRM05_BaronGeddon();
      case ScenarioDbId.BRM_MAJORDOMO:
      case ScenarioDbId.BRM_HEROIC_MAJORDOMO:
        return (GameEntity) new BRM06_Majordomo();
      case ScenarioDbId.BRM_OMOKK:
      case ScenarioDbId.BRM_HEROIC_OMOKK:
        return (GameEntity) new BRM07_Omokk();
      case ScenarioDbId.BRM_DRAKKISATH:
      case ScenarioDbId.BRM_CHALLENGE_PRIEST_V_DRAKKISATH:
      case ScenarioDbId.BRM_HEROIC_DRAKKISATH:
        return (GameEntity) new BRM08_Drakkisath();
      case ScenarioDbId.BRM_REND_BLACKHAND:
      case ScenarioDbId.BRM_CHALLENGE_DRUID_V_BLACKHAND:
      case ScenarioDbId.BRM_HEROIC_REND_BLACKHAND:
        return (GameEntity) new BRM09_RendBlackhand();
      case ScenarioDbId.BRM_RAZORGORE:
      case ScenarioDbId.BRM_HEROIC_RAZORGORE:
      case ScenarioDbId.BRM_CHALLENGE_WARLOCK_V_RAZORGORE:
        return (GameEntity) new BRM10_Razorgore();
      case ScenarioDbId.BRM_VAELASTRASZ:
      case ScenarioDbId.BRM_HEROIC_VAELASTRASZ:
      case ScenarioDbId.BRM_CHALLENGE_ROGUE_V_VAELASTRASZ:
        return (GameEntity) new BRM11_Vaelastrasz();
      case ScenarioDbId.BRM_CHROMAGGUS:
      case ScenarioDbId.BRM_HEROIC_CHROMAGGUS:
        return (GameEntity) new BRM12_Chromaggus();
      case ScenarioDbId.BRM_NEFARIAN:
      case ScenarioDbId.BRM_HEROIC_NEFARIAN:
        return (GameEntity) new BRM13_Nefarian();
      case ScenarioDbId.BRM_OMNOTRON:
      case ScenarioDbId.BRM_HEROIC_OMNOTRON:
      case ScenarioDbId.BRM_CHALLENGE_PALADIN_V_OMNOTRON:
        return (GameEntity) new BRM14_Omnotron();
      case ScenarioDbId.BRM_MALORIAK:
      case ScenarioDbId.BRM_HEROIC_MALORIAK:
        return (GameEntity) new BRM15_Maloriak();
      case ScenarioDbId.BRM_ATRAMEDES:
      case ScenarioDbId.BRM_HEROIC_ATRAMEDES:
        return (GameEntity) new BRM16_Atramedes();
      case ScenarioDbId.BRM_ZOMBIE_NEF:
      case ScenarioDbId.BRM_HEROIC_ZOMBIE_NEF:
        return (GameEntity) new BRM17_ZombieNef();
      case ScenarioDbId.TB_RAG_V_NEF:
        return (GameEntity) new TB01_RagVsNef();
      case ScenarioDbId.TB_DECKBUILDING:
label_70:
        return (GameEntity) new TB04_DeckBuilding();
      default:
        switch (missionId - 1624)
        {
          case ScenarioDbId.INVALID:
          case (ScenarioDbId) 36:
label_85:
            return (GameEntity) new LOE04_Scarvash();
          case (ScenarioDbId) 1:
          case (ScenarioDbId) 47:
label_87:
            return (GameEntity) new LOE14_Steel_Sentinel();
          case ScenarioDbId.TUTORIAL_MILLHOUSE:
          case (ScenarioDbId) 46:
label_93:
            return (GameEntity) new LOE13_Skelesaurus();
          case (ScenarioDbId) 9:
          case (ScenarioDbId) 55:
label_68:
            return (GameEntity) new TB02_CoOp();
          case (ScenarioDbId) 13:
          case (ScenarioDbId) 37:
label_81:
            return (GameEntity) new LOE10_Giantfin();
          case (ScenarioDbId) 14:
          case (ScenarioDbId) 26:
label_83:
            return (GameEntity) new LOE01_Zinaar();
          case (ScenarioDbId) 21:
          case (ScenarioDbId) 35:
label_82:
            return (GameEntity) new LOE09_LordSlitherspear();
          case (ScenarioDbId) 22:
          case (ScenarioDbId) 42:
label_90:
            return (GameEntity) new LOE12_Naga();
          case (ScenarioDbId) 27:
label_84:
            return (GameEntity) new LOE02_Sun_Raider_Phaerix();
          case (ScenarioDbId) 29:
label_88:
            return (GameEntity) new LOE03_AncientTemple();
          case (ScenarioDbId) 30:
            goto label_70;
          case (ScenarioDbId) 32:
          case (ScenarioDbId) 34:
            return (GameEntity) new TB05_GiftExchange();
          case (ScenarioDbId) 33:
label_86:
            return (GameEntity) new LOE08_Archaedas();
          case (ScenarioDbId) 44:
          case (ScenarioDbId) 50:
            return (GameEntity) new TB_ChooseYourFateBuildaround();
          case (ScenarioDbId) 45:
label_89:
            return (GameEntity) new LOE07_MineCart();
          case (ScenarioDbId) 48:
label_91:
            return (GameEntity) new LOE15_Boss1();
          case (ScenarioDbId) 49:
label_92:
            return (GameEntity) new LOE16_Boss2();
          case (ScenarioDbId) 51:
          case (ScenarioDbId) 52:
            return (GameEntity) new TB_ChooseYourFateRandom();
          default:
            switch (missionId - 1813)
            {
              case ScenarioDbId.INVALID:
label_74:
                return (GameEntity) new TB09_ShadowTowers();
              case ScenarioDbId.MULTIPLAYER_1v1:
              case (ScenarioDbId) 49:
label_106:
                return (GameEntity) new KAR11_Netherspite();
              case ScenarioDbId.TUTORIAL_HOGGER:
              case (ScenarioDbId) 41:
label_105:
                return (GameEntity) new KAR10_Aran();
              case ScenarioDbId.TUTORIAL_MILLHOUSE:
label_95:
                return (GameEntity) new KAR00_Prologue();
              case (ScenarioDbId) 5:
              case (ScenarioDbId) 48:
label_96:
                return (GameEntity) new KAR01_Pantry();
              case ScenarioDbId.MULTIPLAYER_1v1 | ScenarioDbId.TUTORIAL_MILLHOUSE:
              case (ScenarioDbId) 47:
label_97:
                return (GameEntity) new KAR02_Mirror();
              case ScenarioDbId.TUTORIAL_HOGGER | ScenarioDbId.TUTORIAL_MILLHOUSE:
label_98:
                return (GameEntity) new KAR03_Chess();
              case (ScenarioDbId) 18:
              case (ScenarioDbId) 44:
label_99:
                return (GameEntity) new KAR04_Julianne();
              case (ScenarioDbId) 19:
              case (ScenarioDbId) 40:
label_100:
                return (GameEntity) new KAR05_Wolf();
              case (ScenarioDbId) 20:
label_101:
                return (GameEntity) new KAR06_Crone();
              case (ScenarioDbId) 21:
              case (ScenarioDbId) 37:
label_102:
                return (GameEntity) new KAR07_Curator();
              case (ScenarioDbId) 22:
              case (ScenarioDbId) 50:
label_103:
                return (GameEntity) new KAR08_Nightbane();
              case (ScenarioDbId) 23:
              case (ScenarioDbId) 43:
label_104:
                return (GameEntity) new KAR09_Illhoof();
              case (ScenarioDbId) 24:
label_107:
                return (GameEntity) new KAR12_Portals();
              case (ScenarioDbId) 31:
              case (ScenarioDbId) 52:
                return (GameEntity) new TB11_CoOpv3();
              case (ScenarioDbId) 39:
              case (ScenarioDbId) 42:
                return (GameEntity) new TB10_DeckRecipe();
              default:
                switch (missionId - 1749)
                {
                  case ScenarioDbId.INVALID:
                    goto label_98;
                  case ScenarioDbId.TUTORIAL_MILLHOUSE:
                    goto label_99;
                  case ScenarioDbId.MULTIPLAYER_1v1 | ScenarioDbId.TUTORIAL_MILLHOUSE:
                    goto label_97;
                  case ScenarioDbId.TUTORIAL_HOGGER | ScenarioDbId.TUTORIAL_MILLHOUSE:
                    goto label_102;
                  case (ScenarioDbId) 9:
                    goto label_104;
                  case (ScenarioDbId) 10:
                    goto label_103;
                  case (ScenarioDbId) 11:
                    goto label_105;
                  case (ScenarioDbId) 12:
                    goto label_106;
                  case (ScenarioDbId) 16:
                    goto label_96;
                  case (ScenarioDbId) 17:
                    goto label_95;
                  case (ScenarioDbId) 18:
                    goto label_107;
                  default:
                    switch (missionId - 2122)
                    {
                      case ScenarioDbId.INVALID:
                      case ScenarioDbId.MULTIPLAYER_1v1:
                        return (GameEntity) new TB10_DeckRecipe();
                      case (ScenarioDbId) 1:
label_80:
                        return (GameEntity) new TB15_BossBattleRoyale();
                      case (ScenarioDbId) 5:
                        return (GameEntity) new TB14_DPromo();
                      default:
                        switch (missionId - 1143)
                        {
                          case ScenarioDbId.INVALID:
                            goto label_93;
                          case ScenarioDbId.TUTORIAL_HOGGER:
                            goto label_91;
                          case ScenarioDbId.TUTORIAL_MILLHOUSE:
                            goto label_68;
                          default:
                            switch (missionId - 1060)
                            {
                              case ScenarioDbId.INVALID:
                                goto label_85;
                              case (ScenarioDbId) 1:
                                goto label_88;
                              case ScenarioDbId.MULTIPLAYER_1v1:
                                goto label_89;
                              case ScenarioDbId.TUTORIAL_HOGGER:
                                return (GameEntity) new TB03_InspireVSJoust();
                              default:
                                switch (missionId - 1084)
                                {
                                  case ScenarioDbId.INVALID:
                                    goto label_86;
                                  case ScenarioDbId.MULTIPLAYER_1v1:
                                    goto label_90;
                                  default:
                                    switch (missionId - 1779)
                                    {
                                      case ScenarioDbId.INVALID:
                                      case ScenarioDbId.MULTIPLAYER_1v1:
                                        goto label_74;
                                      default:
                                        switch (missionId - 2112)
                                        {
                                          case ScenarioDbId.INVALID:
                                          case (ScenarioDbId) 1:
                                            return (GameEntity) new TB_Blizzcon_2016();
                                          case ScenarioDbId.MULTIPLAYER_1v1:
                                            return (GameEntity) new TB13_LethalPuzzles();
                                          default:
                                            if (missionId == ScenarioDbId.TUTORIAL_HOGGER)
                                              return (GameEntity) new Tutorial_01();
                                            if (missionId == ScenarioDbId.TUTORIAL_MILLHOUSE)
                                              return (GameEntity) new Tutorial_02();
                                            if (missionId != ScenarioDbId.LOE_SUN_RAIDER_PHAERIX)
                                            {
                                              if (missionId != ScenarioDbId.LOE_ZINAAR)
                                              {
                                                if (missionId != ScenarioDbId.KAR_CRONE)
                                                {
                                                  if (missionId != ScenarioDbId.KAR_WOLF)
                                                  {
                                                    if (missionId == ScenarioDbId.TB_KARAPORTALS_1P_TEST || missionId == ScenarioDbId.TB_KARAPORTALS)
                                                      return (GameEntity) new TB12_PartyPortals();
                                                    if (missionId == ScenarioDbId.TUTORIAL_MUKLA)
                                                      return (GameEntity) new Tutorial_03();
                                                    if (missionId == ScenarioDbId.TUTORIAL_NESINGWARY)
                                                      return (GameEntity) new Tutorial_04();
                                                    if (missionId != ScenarioDbId.TB_CO_OP_TEST && missionId != ScenarioDbId.TB_CO_OP && missionId != ScenarioDbId.TB_CO_OP_TEST2)
                                                    {
                                                      if (missionId != ScenarioDbId.LOE_GIANTFIN)
                                                      {
                                                        if (missionId != ScenarioDbId.LOE_RAFAAM_2)
                                                        {
                                                          if (missionId != ScenarioDbId.LOE_STEEL_SENTINEL)
                                                          {
                                                            if (missionId != ScenarioDbId.LOE_SLITHERSPEAR)
                                                            {
                                                              if (missionId != ScenarioDbId.LOE_CHALLENGE_PALADIN_V_ARCHAEDUS)
                                                              {
                                                                if (missionId != ScenarioDbId.LOE_CHALLENGE_WARLOCK_V_SUN_RAIDER)
                                                                {
                                                                  if (missionId == ScenarioDbId.TB_KELTHUZADRAFAAM)
                                                                    return (GameEntity) new TB_KelthuzadRafaam();
                                                                  if (missionId != ScenarioDbId.TB_BATTLEROYALE_1P_TEST)
                                                                    return (GameEntity) new StandardGameEntity();
                                                                  goto label_80;
                                                                }
                                                                else
                                                                  goto label_84;
                                                              }
                                                              else
                                                                goto label_86;
                                                            }
                                                            else
                                                              goto label_82;
                                                          }
                                                          else
                                                            goto label_87;
                                                        }
                                                        else
                                                          goto label_92;
                                                      }
                                                      else
                                                        goto label_81;
                                                    }
                                                    else
                                                      goto label_68;
                                                  }
                                                  else
                                                    goto label_100;
                                                }
                                                else
                                                  goto label_101;
                                              }
                                              else
                                                goto label_83;
                                            }
                                            else
                                              goto label_84;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
  }

  public bool IsAI()
  {
    return GameUtils.IsAIMission(this.m_missionId);
  }

  public bool WasAI()
  {
    return GameUtils.IsAIMission(this.m_prevMissionId);
  }

  public bool IsNextAI()
  {
    return GameUtils.IsAIMission(this.m_nextMissionId);
  }

  public bool IsTutorial()
  {
    return GameUtils.IsTutorialMission(this.m_missionId);
  }

  public bool WasTutorial()
  {
    return GameUtils.IsTutorialMission(this.m_prevMissionId);
  }

  public bool IsNextTutorial()
  {
    return GameUtils.IsTutorialMission(this.m_nextMissionId);
  }

  public bool IsPractice()
  {
    return GameUtils.IsPracticeMission(this.m_missionId);
  }

  public bool WasPractice()
  {
    return GameUtils.IsPracticeMission(this.m_prevMissionId);
  }

  public bool IsNextPractice()
  {
    return GameUtils.IsPracticeMission(this.m_nextMissionId);
  }

  public bool IsClassChallengeMission()
  {
    return GameUtils.IsClassChallengeMission(this.m_missionId);
  }

  public bool IsHeroicMission()
  {
    return GameUtils.IsHeroicAdventureMission(this.m_missionId);
  }

  public bool IsExpansionMission()
  {
    return GameUtils.IsExpansionMission(this.m_missionId);
  }

  public bool WasExpansionMission()
  {
    return GameUtils.IsExpansionMission(this.m_prevMissionId);
  }

  public bool IsNextExpansionMission()
  {
    return GameUtils.IsExpansionMission(this.m_nextMissionId);
  }

  public bool IsPlay()
  {
    if (!this.IsRankedPlay())
      return this.IsUnrankedPlay();
    return true;
  }

  public bool WasPlay()
  {
    if (!this.WasRankedPlay())
      return this.WasUnrankedPlay();
    return true;
  }

  public bool IsNextPlay()
  {
    if (!this.IsNextRankedPlay())
      return this.IsNextUnrankedPlay();
    return true;
  }

  public bool IsRankedPlay()
  {
    return this.m_gameType == GameType.GT_RANKED;
  }

  public bool WasRankedPlay()
  {
    return this.m_prevGameType == GameType.GT_RANKED;
  }

  public bool IsNextRankedPlay()
  {
    return this.m_nextGameType == GameType.GT_RANKED;
  }

  public bool IsUnrankedPlay()
  {
    return this.m_gameType == GameType.GT_CASUAL;
  }

  public bool WasUnrankedPlay()
  {
    return this.m_prevGameType == GameType.GT_CASUAL;
  }

  public bool IsNextUnrankedPlay()
  {
    return this.m_nextGameType == GameType.GT_CASUAL;
  }

  public bool IsArena()
  {
    return this.m_gameType == GameType.GT_ARENA;
  }

  public bool WasArena()
  {
    return this.m_prevGameType == GameType.GT_ARENA;
  }

  public bool IsNextArena()
  {
    return this.m_nextGameType == GameType.GT_ARENA;
  }

  public bool IsFriendly()
  {
    return this.m_gameType == GameType.GT_VS_FRIEND;
  }

  public bool WasFriendly()
  {
    return this.m_prevGameType == GameType.GT_VS_FRIEND;
  }

  public bool IsNextFriendly()
  {
    return this.m_nextGameType == GameType.GT_VS_FRIEND;
  }

  public bool WasTavernBrawl()
  {
    return this.m_prevGameType == GameType.GT_TAVERNBRAWL;
  }

  public bool IsTavernBrawl()
  {
    return this.m_gameType == GameType.GT_TAVERNBRAWL;
  }

  public bool IsNextTavernBrawl()
  {
    return this.m_nextGameType == GameType.GT_TAVERNBRAWL;
  }

  public bool IsWildFormatType()
  {
    return this.m_formatType == FormatType.FT_WILD;
  }

  public bool IsNextWildFormatType()
  {
    return this.m_nextFormatType == FormatType.FT_WILD;
  }

  public SceneMgr.Mode GetPostGameSceneMode()
  {
    if (this.IsSpectator())
      return GameUtils.AreAllTutorialsComplete() ? SceneMgr.Mode.HUB : SceneMgr.Mode.INVALID;
    SceneMgr.Mode mode = SceneMgr.Mode.HUB;
    GameType gameType = this.m_gameType;
    switch (gameType)
    {
      case GameType.GT_VS_AI:
        mode = !ReturningPlayerMgr.Get().IsInReturningPlayerMode || !GameUtils.IsReturningPlayerMission(this.m_missionId) || !AdventureProgressMgr.Get().IsAdventureModeComplete(AdventureDbId.RETURNING_PLAYER, AdventureModeDbId.CLASS_CHALLENGE) ? SceneMgr.Mode.ADVENTURE : SceneMgr.Mode.HUB;
        break;
      case GameType.GT_VS_FRIEND:
        if (FriendChallengeMgr.Get().HasChallenge())
        {
          mode = !FriendChallengeMgr.Get().IsChallengeTavernBrawl() ? SceneMgr.Mode.FRIENDLY : SceneMgr.Mode.TAVERN_BRAWL;
          break;
        }
        break;
      case GameType.GT_ARENA:
        mode = SceneMgr.Mode.DRAFT;
        break;
      case GameType.GT_RANKED:
      case GameType.GT_CASUAL:
        mode = SceneMgr.Mode.TOURNAMENT;
        break;
      default:
        if (gameType == GameType.GT_TAVERNBRAWL)
        {
          mode = SceneMgr.Mode.TAVERN_BRAWL;
          if (TavernBrawlManager.Get().CurrentTavernBrawlSeasonEndInSeconds < 10L)
          {
            mode = SceneMgr.Mode.HUB;
            break;
          }
          break;
        }
        break;
    }
    return mode;
  }

  public SceneMgr.Mode GetPostDisconnectSceneMode()
  {
    if (this.IsSpectator())
      return GameUtils.AreAllTutorialsComplete() ? SceneMgr.Mode.HUB : SceneMgr.Mode.INVALID;
    if (this.IsTutorial())
      return SceneMgr.Mode.INVALID;
    return this.GetPostGameSceneMode();
  }

  public void PreparePostGameSceneMode(SceneMgr.Mode mode)
  {
    if (mode != SceneMgr.Mode.ADVENTURE || AdventureConfig.Get().GetCurrentSubScene() != AdventureSubScenes.Chooser)
      return;
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(this.m_missionId);
    if (record == null)
      return;
    int adventureId = record.AdventureId;
    if (adventureId == 0)
      return;
    int modeId = record.ModeId;
    if (modeId == 0)
      return;
    AdventureConfig.Get().SetSelectedAdventureMode((AdventureDbId) adventureId, (AdventureModeDbId) modeId);
    AdventureConfig.Get().ChangeSubSceneToSelectedAdventure();
    AdventureConfig.Get().SetMission((ScenarioDbId) this.m_missionId, false);
  }

  public bool IsTransitionPopupShown()
  {
    if ((UnityEngine.Object) this.m_transitionPopup == (UnityEngine.Object) null)
      return false;
    return this.m_transitionPopup.IsShown();
  }

  public TransitionPopup GetTransitionPopup()
  {
    return this.m_transitionPopup;
  }

  public void UpdatePresence()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return;
    if (this.IsSpectator())
    {
      PresenceMgr presenceMgr = PresenceMgr.Get();
      if (this.IsTutorial())
        presenceMgr.SetStatus((Enum) PresenceStatus.SPECTATING_GAME_TUTORIAL);
      else if (this.IsPractice())
        presenceMgr.SetStatus((Enum) PresenceStatus.SPECTATING_GAME_PRACTICE);
      else if (this.IsPlay())
        presenceMgr.SetStatus((Enum) PresenceStatus.SPECTATING_GAME_PLAY);
      else if (this.IsArena())
        presenceMgr.SetStatus((Enum) PresenceStatus.SPECTATING_GAME_ARENA);
      else if (this.IsFriendly())
        presenceMgr.SetStatus((Enum) PresenceStatus.SPECTATING_GAME_FRIENDLY);
      else if (this.IsTavernBrawl())
        presenceMgr.SetStatus((Enum) PresenceStatus.SPECTATING_GAME_TAVERN_BRAWL);
      else if (this.IsExpansionMission() || GameUtils.IsMissionForAdventure(this.m_missionId, 245))
      {
        ScenarioDbId missionId = (ScenarioDbId) this.m_missionId;
        presenceMgr.SetStatus_SpectatingMission(missionId);
      }
      SpectatorManager.Get().UpdateMySpectatorInfo();
    }
    else
    {
      if (this.IsTutorial())
      {
        switch ((ScenarioDbId) this.m_missionId)
        {
          case ScenarioDbId.TUTORIAL_HOGGER:
            PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TUTORIAL_GAME, (Enum) PresenceTutorial.HOGGER);
            break;
          case ScenarioDbId.TUTORIAL_MILLHOUSE:
            PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TUTORIAL_GAME, (Enum) PresenceTutorial.MILLHOUSE);
            break;
          case ScenarioDbId.TUTORIAL_ILLIDAN:
            PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TUTORIAL_GAME, (Enum) PresenceTutorial.ILLIDAN);
            break;
          case ScenarioDbId.TUTORIAL_CHO:
            PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TUTORIAL_GAME, (Enum) PresenceTutorial.CHO);
            break;
          case ScenarioDbId.TUTORIAL_MUKLA:
            PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TUTORIAL_GAME, (Enum) PresenceTutorial.MUKLA);
            break;
          case ScenarioDbId.TUTORIAL_NESINGWARY:
            PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TUTORIAL_GAME, (Enum) PresenceTutorial.HEMET);
            break;
        }
      }
      else if (this.IsPractice())
        PresenceMgr.Get().SetStatus((Enum) PresenceStatus.PRACTICE_GAME);
      else if (this.IsPlay())
        PresenceMgr.Get().SetStatus((Enum) PresenceStatus.PLAY_GAME);
      else if (this.IsArena())
        PresenceMgr.Get().SetStatus((Enum) PresenceStatus.ARENA_GAME);
      else if (this.IsFriendly())
      {
        PresenceStatus presenceStatus = PresenceStatus.FRIENDLY_GAME;
        if (FriendChallengeMgr.Get().IsChallengeTavernBrawl())
          presenceStatus = PresenceStatus.TAVERN_BRAWL_FRIENDLY_GAME;
        PresenceMgr.Get().SetStatus((Enum) presenceStatus);
      }
      else if (this.IsTavernBrawl())
        PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TAVERN_BRAWL_GAME);
      else if (this.IsExpansionMission())
        PresenceMgr.Get().SetStatus_PlayingMission((ScenarioDbId) this.m_missionId);
      SpectatorManager.Get().UpdateMySpectatorInfo();
    }
  }

  public void UpdateSessionPresence(GameType gameType)
  {
    if (gameType == GameType.GT_ARENA)
    {
      int wins = DraftManager.Get().GetWins();
      int losses = DraftManager.Get().GetLosses();
      BnetPresenceMgr.Get().SetGameFieldBlob(22U, (IProtoBuf) new SessionRecord()
      {
        Wins = (uint) wins,
        Losses = (uint) losses,
        RunFinished = false,
        SessionRecordType = SessionRecordType.ARENA
      });
    }
    else
    {
      if (gameType != GameType.GT_TAVERNBRAWL || !TavernBrawlManager.Get().IsCurrentSeasonSessionBased)
        return;
      int gamesWon = TavernBrawlManager.Get().GamesWon;
      int gamesLost = TavernBrawlManager.Get().GamesLost;
      BnetPresenceMgr.Get().SetGameFieldBlob(22U, (IProtoBuf) new SessionRecord()
      {
        Wins = (uint) gamesWon,
        Losses = (uint) gamesLost,
        RunFinished = false,
        SessionRecordType = SessionRecordType.TAVERN_BRAWL
      });
    }
  }

  public void SetLastDisplayedPlayerName(int playerId, string name)
  {
    this.m_lastDisplayedPlayerNames[playerId] = name;
  }

  public string GetLastDisplayedPlayerName(int playerId)
  {
    string str;
    this.m_lastDisplayedPlayerNames.TryGetValue(playerId, out str);
    return str;
  }

  private void WillReset()
  {
    this.m_gameType = GameType.GT_UNKNOWN;
    this.m_prevGameType = GameType.GT_UNKNOWN;
    this.m_nextGameType = GameType.GT_UNKNOWN;
    this.m_formatType = FormatType.FT_UNKNOWN;
    this.m_prevFormatType = FormatType.FT_UNKNOWN;
    this.m_nextFormatType = FormatType.FT_UNKNOWN;
    this.m_missionId = 0;
    this.m_prevMissionId = 0;
    this.m_nextMissionId = 0;
    this.m_reconnectType = ReconnectType.INVALID;
    this.m_prevReconnectType = ReconnectType.INVALID;
    this.m_nextReconnectType = ReconnectType.INVALID;
    this.m_spectator = false;
    this.m_prevSpectator = false;
    this.m_nextSpectator = false;
    this.m_lastEnterGameError = 0U;
    this.m_findGameState = FindGameState.INVALID;
    this.m_gameSetup = (Network.GameSetup) null;
    this.m_lastDisplayedPlayerNames.Clear();
  }

  private void OnSceneUnloaded(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    if (prevMode != SceneMgr.Mode.GAMEPLAY || SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY)
      return;
    this.OnGameEnded();
  }

  private void OnScenePreLoad(SceneMgr.Mode prevMode, SceneMgr.Mode mode, object userData)
  {
    this.PreloadTransitionPopup();
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.HUB)
      this.DestroyTransitionPopup();
    if (mode == SceneMgr.Mode.GAMEPLAY && prevMode != SceneMgr.Mode.GAMEPLAY)
    {
      Screen.sleepTimeout = -1;
    }
    else
    {
      if (prevMode != SceneMgr.Mode.GAMEPLAY || mode == SceneMgr.Mode.GAMEPLAY || SpectatorManager.Get().IsInSpectatorMode())
        return;
      Screen.sleepTimeout = -2;
    }
  }

  private void OnServerResult()
  {
    if (!this.IsFindingGame())
      return;
    ServerResult serverResult = Network.GetServerResult();
    if (serverResult.ResultCode == 1)
    {
      float secondsToWait = Mathf.Max(!serverResult.HasRetryDelaySeconds ? 2f : serverResult.RetryDelaySeconds, 0.5f);
      ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.OnServerResult_Retry), (object) null);
      ApplicationMgr.Get().ScheduleCallback(secondsToWait, true, new ApplicationMgr.ScheduledCallback(this.OnServerResult_Retry), (object) null);
    }
    else
    {
      if (serverResult.ResultCode != 2)
        return;
      this.OnGameCanceled();
    }
  }

  private void OnServerResult_Retry(object userData)
  {
    Network.Get().RetryGotoGameServer();
  }

  private void ChangeBoardIfNecessary()
  {
    int num = this.m_gameSetup.Board;
    string board = Cheats.Get().GetBoard();
    bool flag = false;
    if (!string.IsNullOrEmpty(board))
    {
      num = GameUtils.GetBoardIdFromAssetName(board);
      flag = true;
    }
    if (!flag && DemoMgr.Get().IsExpoDemo())
    {
      string str = Vars.Key("Demo.ForceBoard").GetStr((string) null);
      if (str != null)
        num = GameUtils.GetBoardIdFromAssetName(str);
    }
    this.m_gameSetup.Board = num;
  }

  private void PreloadTransitionPopup()
  {
    switch (SceneMgr.Get().GetMode())
    {
      case SceneMgr.Mode.TOURNAMENT:
      case SceneMgr.Mode.DRAFT:
      case SceneMgr.Mode.TAVERN_BRAWL:
        this.LoadTransitionPopup((string) this.MATCHING_POPUP_NAME);
        break;
      case SceneMgr.Mode.FRIENDLY:
      case SceneMgr.Mode.ADVENTURE:
        this.LoadTransitionPopup("LoadingPopup");
        break;
    }
  }

  private string DetermineTransitionPopupForFindGame(GameType gameType, FormatType formatType, int missionId)
  {
    switch (gameType)
    {
      case GameType.GT_TAVERNBRAWL:
        switch (Network.TranslateGameTypeToBnet(gameType, formatType, missionId))
        {
          case BnetGameType.BGT_TAVERNBRAWL_PVP:
          case BnetGameType.BGT_TAVERNBRAWL_2P_COOP:
            return (string) this.MATCHING_POPUP_NAME;
          default:
            return "LoadingPopup";
        }
      case GameType.GT_TUTORIAL:
        return (string) null;
      case GameType.GT_ARENA:
      case GameType.GT_RANKED:
      case GameType.GT_CASUAL:
        return (string) this.MATCHING_POPUP_NAME;
      default:
        return "LoadingPopup";
    }
  }

  private void LoadTransitionPopup(string popupName)
  {
    GameObject gameObject = AssetLoader.Get().LoadActor(popupName, false, false);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
    {
      Error.AddDevFatal("GameMgr.LoadTransitionPopup() - Failed to load {0}", (object) popupName);
    }
    else
    {
      this.m_transitionPopup = gameObject.GetComponent<TransitionPopup>();
      this.m_initialTransitionPopupPos = this.m_transitionPopup.transform.position;
      this.m_transitionPopup.RegisterMatchCanceledEvent(new TransitionPopup.MatchCanceledEvent(this.OnTransitionPopupCanceled));
      SceneUtils.SetLayer((Component) this.m_transitionPopup, GameLayer.IgnoreFullScreenEffects);
    }
  }

  private void ShowTransitionPopup(string popupName)
  {
    if (!(bool) ((UnityEngine.Object) this.m_transitionPopup) || ((object) this.m_transitionPopup).GetType() != this.s_transitionPopupNameToType[popupName])
    {
      this.DestroyTransitionPopup();
      this.LoadTransitionPopup(popupName);
    }
    if (this.m_transitionPopup.IsShown())
      return;
    if ((UnityEngine.Object) Box.Get() != (UnityEngine.Object) null && Box.Get().GetState() != Box.State.OPEN)
      this.m_transitionPopup.transform.position = Box.Get().GetBoxCamera().transform.position - Box.Get().m_Camera.GetCameraPosition(BoxCamera.State.OPENED) - this.m_initialTransitionPopupPos;
    this.m_transitionPopup.SetAdventureId(GameUtils.GetAdventureId(this.m_nextMissionId));
    this.m_transitionPopup.SetFormatType(this.m_nextFormatType);
    this.m_transitionPopup.Show();
  }

  private void OnTransitionPopupCanceled()
  {
    bool flag = Network.Get().IsFindingGame();
    if (flag)
      Network.Get().CancelFindGame();
    this.ChangeFindGameState(FindGameState.CLIENT_CANCELED);
    if (flag)
      return;
    this.ChangeFindGameState(FindGameState.INVALID);
  }

  private void HideTransitionPopup()
  {
    if (!(bool) ((UnityEngine.Object) this.m_transitionPopup))
      return;
    this.m_transitionPopup.Hide();
  }

  private void DestroyTransitionPopup()
  {
    if (!(bool) ((UnityEngine.Object) this.m_transitionPopup))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_transitionPopup.gameObject);
  }

  private bool GetFriendlyErrorMessage(int errorCode, ref string headerKey, ref string messageKey, ref object[] messageParams)
  {
    switch (errorCode)
    {
      case 1000500:
        headerKey = "GLOBAL_ERROR_GENERIC_HEADER";
        messageKey = "GLOBAL_ERROR_FIND_GAME_SCENARIO_INCORRECT_NUM_PLAYERS";
        return true;
      case 1000501:
        headerKey = "GLOBAL_ERROR_GENERIC_HEADER";
        messageKey = "GLOBAL_ERROR_FIND_GAME_SCENARIO_NO_DECK_SPECIFIED";
        return true;
      case 1000502:
      case 1002008:
        headerKey = "GLOBAL_ERROR_GENERIC_HEADER";
        messageKey = "GLOBAL_ERROR_FIND_GAME_SCENARIO_MISCONFIGURED";
        return true;
      case 1001000:
        headerKey = "GLOBAL_TAVERN_BRAWL";
        messageKey = "GLOBAL_TAVERN_BRAWL_ERROR_SEASON_INCREMENTED";
        TavernBrawlManager.Get().RefreshServerData();
        return true;
      case 1001001:
        headerKey = "GLOBAL_TAVERN_BRAWL";
        messageKey = "GLOBAL_TAVERN_BRAWL_ERROR_NOT_ACTIVE";
        TavernBrawlManager.Get().RefreshServerData();
        return true;
      case 1002007:
      case 1002002:
        headerKey = "GLOBAL_ERROR_GENERIC_HEADER";
        messageKey = "GLUE_ERROR_DECK_RULESET_RULE_VIOLATION";
        return true;
      default:
        return false;
    }
  }

  private void OnGameQueueEvent(QueueEvent queueEvent)
  {
    FindGameState? nullable = new FindGameState?();
    GameMgr.s_bnetToFindGameResultMap.TryGetValue(queueEvent.EventType, out nullable);
    if (queueEvent.BnetError != 0)
      this.m_lastEnterGameError = (uint) queueEvent.BnetError;
    if (!nullable.HasValue)
      return;
    if (queueEvent.EventType == QueueEvent.Type.QUEUE_DELAY_ERROR)
    {
      if (queueEvent.BnetError == 25017)
        return;
      string empty = string.Empty;
      string messageKey = (string) null;
      object[] messageParams = new object[0];
      if (this.GetFriendlyErrorMessage(queueEvent.BnetError, ref empty, ref messageKey, ref messageParams))
      {
        Error.AddWarningLoc(empty, messageKey, messageParams);
        nullable = new FindGameState?(FindGameState.BNET_QUEUE_CANCELED);
        this.HandleGameCanceled();
      }
    }
    if (queueEvent.BnetError != 0)
    {
      string empty = string.Empty;
      if (Enum.IsDefined(typeof (BattleNetErrors), (object) (BattleNetErrors) queueEvent.BnetError))
        empty = ((BattleNetErrors) queueEvent.BnetError).ToString();
      else if (Enum.IsDefined(typeof (PegasusShared.ErrorCode), (object) (PegasusShared.ErrorCode) queueEvent.BnetError))
        empty = ((PegasusShared.ErrorCode) queueEvent.BnetError).ToString();
      string str = string.Format("OnGameQueueEvent error={0} {1}", (object) queueEvent.BnetError, (object) empty);
      if (ApplicationMgr.IsInternal())
        Error.AddDevWarning("OnGameQueueEvent", str);
      else
        Log.BattleNet.PrintWarning(str);
    }
    if (queueEvent.EventType == QueueEvent.Type.QUEUE_GAME_STARTED)
    {
      queueEvent.GameServer.Mission = this.m_nextMissionId;
      this.ChangeFindGameState(nullable.Value, queueEvent, queueEvent.GameServer, (Network.GameCancelInfo) null);
    }
    else
      this.ChangeFindGameState(nullable.Value, queueEvent);
  }

  private void OnGameSetup()
  {
    if (SpectatorManager.Get().IsSpectatingOpposingSide() && this.m_gameSetup != null)
      return;
    this.m_gameSetup = Network.GetGameSetupInfo();
    this.ChangeBoardIfNecessary();
    if (this.m_findGameState == FindGameState.INVALID && this.m_gameType == GameType.GT_UNKNOWN)
    {
      Debug.LogError((object) string.Format("GameMgr.OnGameStarting() - Received {0} packet even though we're not looking for a game.", (object) PegasusGame.GameSetup.PacketID.ID));
    }
    else
    {
      this.m_prevGameType = this.m_gameType;
      this.m_gameType = this.m_nextGameType;
      this.m_nextGameType = GameType.GT_UNKNOWN;
      this.m_prevFormatType = this.m_formatType;
      this.m_formatType = this.m_nextFormatType;
      this.m_nextFormatType = FormatType.FT_UNKNOWN;
      this.m_prevMissionId = this.m_missionId;
      this.m_missionId = this.m_nextMissionId;
      this.m_nextMissionId = 0;
      this.m_prevReconnectType = this.m_reconnectType;
      this.m_reconnectType = this.m_nextReconnectType;
      this.m_nextReconnectType = ReconnectType.INVALID;
      this.m_prevSpectator = this.m_spectator;
      this.m_spectator = this.m_nextSpectator;
      this.m_nextSpectator = false;
      this.ChangeFindGameState(FindGameState.SERVER_GAME_STARTED);
    }
  }

  private void OnGameCanceled()
  {
    this.HandleGameCanceled();
    this.ChangeFindGameState(FindGameState.SERVER_GAME_CANCELED, Network.GetGameCancelInfo());
  }

  public bool OnBnetError(BnetErrorInfo info, object userData)
  {
    if (info.GetFeature() == BnetFeature.Games)
    {
      BattleNetErrors error = info.GetError();
      this.m_lastEnterGameError = (uint) error;
      string str = (string) null;
      bool flag = false;
      FindGameState state = FindGameState.BNET_ERROR;
      switch (error)
      {
        case BattleNetErrors.ERROR_GAME_MASTER_INVALID_FACTORY:
        case BattleNetErrors.ERROR_GAME_MASTER_NO_GAME_SERVER:
        case BattleNetErrors.ERROR_GAME_MASTER_NO_FACTORY:
          str = error.ToString();
          flag = true;
          break;
      }
      if (!flag)
      {
        string empty = string.Empty;
        string messageKey = (string) null;
        object[] messageParams = new object[0];
        if (this.GetFriendlyErrorMessage((int) this.m_lastEnterGameError, ref empty, ref messageKey, ref messageParams))
        {
          Error.AddWarningLoc(empty, messageKey, messageParams);
          str = ((PegasusShared.ErrorCode) this.m_lastEnterGameError).ToString();
          state = FindGameState.BNET_QUEUE_CANCELED;
          flag = true;
        }
      }
      if (!flag && info.GetFeatureEvent() == BnetFeatureEvent.Games_OnFindGame)
        flag = true;
      if (flag)
      {
        string format = string.Format("GameMgr.OnBnetError() - received error {0} {1}", (object) this.m_lastEnterGameError, (object) str);
        Log.BattleNet.Print(LogLevel.Error, format, new object[0]);
        if (!Log.BattleNet.CanPrint(LogTarget.CONSOLE, LogLevel.Error, false))
          Debug.LogError((object) string.Format("[{0}] {1}", (object) "BattleNet", (object) format));
        this.HandleGameCanceled();
        this.ChangeFindGameState(state);
        return true;
      }
    }
    return false;
  }

  private void HandleGameCanceled()
  {
    this.m_nextGameType = GameType.GT_UNKNOWN;
    this.m_nextFormatType = FormatType.FT_UNKNOWN;
    this.m_nextMissionId = 0;
    this.m_nextReconnectType = ReconnectType.INVALID;
    this.m_nextSpectator = false;
    Network.Get().ClearLastGameServerJoined();
  }

  private bool OnReconnectTimeout(object userData)
  {
    this.HandleGameCanceled();
    this.ChangeFindGameState(FindGameState.CLIENT_CANCELED);
    this.ChangeFindGameState(FindGameState.INVALID);
    return false;
  }

  private bool ChangeFindGameState(FindGameState state)
  {
    return this.ChangeFindGameState(state, (QueueEvent) null, (bgs.types.GameServerInfo) null, (Network.GameCancelInfo) null);
  }

  private bool ChangeFindGameState(FindGameState state, QueueEvent queueEvent)
  {
    return this.ChangeFindGameState(state, queueEvent, (bgs.types.GameServerInfo) null, (Network.GameCancelInfo) null);
  }

  private bool ChangeFindGameState(FindGameState state, bgs.types.GameServerInfo serverInfo)
  {
    return this.ChangeFindGameState(state, (QueueEvent) null, serverInfo, (Network.GameCancelInfo) null);
  }

  private bool ChangeFindGameState(FindGameState state, Network.GameCancelInfo cancelInfo)
  {
    return this.ChangeFindGameState(state, (QueueEvent) null, (bgs.types.GameServerInfo) null, cancelInfo);
  }

  private bool ChangeFindGameState(FindGameState state, QueueEvent queueEvent, bgs.types.GameServerInfo serverInfo, Network.GameCancelInfo cancelInfo)
  {
    this.m_findGameState = state;
    FindGameEventData eventData = new FindGameEventData();
    eventData.m_state = state;
    eventData.m_gameServer = serverInfo;
    eventData.m_cancelInfo = cancelInfo;
    if (queueEvent != null)
    {
      eventData.m_queueMinSeconds = queueEvent.MinSeconds;
      eventData.m_queueMaxSeconds = queueEvent.MaxSeconds;
    }
    switch (state)
    {
      case FindGameState.CLIENT_CANCELED:
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
      case FindGameState.SERVER_GAME_STARTED:
      case FindGameState.SERVER_GAME_CANCELED:
        Network.Get().RemoveGameServerDisconnectEventListener(new Network.GameServerDisconnectEvent(this.OnGameServerDisconnect));
        break;
    }
    bool gameEvent = this.FireFindGameEvent(eventData);
    if (!gameEvent)
      this.DoDefaultFindGameEventBehavior(eventData);
    this.FinalizeState(eventData);
    return gameEvent;
  }

  private bool FireFindGameEvent(FindGameEventData eventData)
  {
    bool flag = false;
    foreach (GameMgr.FindGameListener findGameListener in this.m_findGameListeners.ToArray())
      flag = findGameListener.Fire(eventData) || flag;
    return flag;
  }

  private void DoDefaultFindGameEventBehavior(FindGameEventData eventData)
  {
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_ERROR:
        Error.AddWarningLoc("GLOBAL_ERROR_GENERIC_HEADER", "GLOBAL_ERROR_GAME_DENIED");
        this.HideTransitionPopup();
        break;
      case FindGameState.BNET_QUEUE_CANCELED:
        this.HideTransitionPopup();
        break;
      case FindGameState.SERVER_GAME_CONNECTING:
        Network.Get().GotoGameServer(eventData.m_gameServer, this.IsNextReconnect());
        break;
      case FindGameState.SERVER_GAME_STARTED:
        if ((UnityEngine.Object) Box.Get() != (UnityEngine.Object) null)
        {
          LoadingScreen.Get().SetFreezeFrameCamera(Box.Get().GetCamera());
          LoadingScreen.Get().SetTransitionAudioListener(Box.Get().GetAudioListener());
        }
        if (SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY)
        {
          if (SpectatorManager.Get().IsSpectatingOpposingSide())
            break;
          SceneMgr.Get().ReloadMode();
          break;
        }
        SceneMgr.Get().SetNextMode(SceneMgr.Mode.GAMEPLAY);
        break;
      case FindGameState.SERVER_GAME_CANCELED:
        if (eventData.m_cancelInfo != null)
        {
          if (eventData.m_cancelInfo.CancelReason == Network.GameCancelInfo.Reason.OPPONENT_TIMEOUT)
            Error.AddWarningLoc("GLOBAL_ERROR_GENERIC_HEADER", "GLOBAL_ERROR_GAME_OPPONENT_TIMEOUT");
          else
            Error.AddDevWarning("GAME ERROR", "The Game Server canceled the game. Error: {0}", (object) eventData.m_cancelInfo.CancelReason);
        }
        this.HideTransitionPopup();
        break;
    }
  }

  private void FinalizeState(FindGameEventData eventData)
  {
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
      case FindGameState.SERVER_GAME_STARTED:
      case FindGameState.SERVER_GAME_CANCELED:
        this.ChangeFindGameState(FindGameState.INVALID);
        break;
    }
  }

  private void OnGameEnded()
  {
    this.m_prevGameType = this.m_gameType;
    this.m_gameType = GameType.GT_UNKNOWN;
    this.m_prevFormatType = this.m_formatType;
    this.m_formatType = FormatType.FT_UNKNOWN;
    this.m_prevMissionId = this.m_missionId;
    this.m_missionId = 0;
    this.m_prevReconnectType = this.m_reconnectType;
    this.m_reconnectType = ReconnectType.INVALID;
    this.m_prevSpectator = this.m_spectator;
    this.m_spectator = false;
    this.m_lastEnterGameError = 0U;
    this.m_pendingAutoConcede = false;
    this.m_gameSetup = (Network.GameSetup) null;
    this.m_lastDisplayedPlayerNames.Clear();
  }

  private class FindGameListener : EventListener<GameMgr.FindGameCallback>
  {
    public bool Fire(FindGameEventData eventData)
    {
      return this.m_callback(eventData, this.m_userData);
    }
  }

  public delegate bool FindGameCallback(FindGameEventData eventData, object userData);
}
