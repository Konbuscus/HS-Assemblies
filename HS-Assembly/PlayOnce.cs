﻿// Decompiled with JetBrains decompiler
// Type: PlayOnce
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlayOnce : MonoBehaviour
{
  public string notes;
  public string notes2;
  public GameObject tester;
  public string testerAnim;
  public GameObject tester2;
  public string tester2Anim;
  public GameObject tester3;
  public string tester3Anim;

  private void Start()
  {
    if ((Object) this.tester != (Object) null)
      this.tester.SetActive(false);
    if ((Object) this.tester2 != (Object) null)
      this.tester2.SetActive(false);
    if (!((Object) this.tester3 != (Object) null))
      return;
    this.tester3.SetActive(false);
  }

  private void OnGUI()
  {
    if (!Event.current.isKey)
      return;
    if ((Object) this.tester != (Object) null)
    {
      this.tester.SetActive(true);
      this.tester.GetComponent<Animation>().Stop(this.testerAnim);
      this.tester.GetComponent<Animation>().Play(this.testerAnim);
    }
    else
      Debug.Log((object) "NO 'tester' object.");
    if ((Object) this.tester2 != (Object) null)
    {
      this.tester2.SetActive(true);
      this.tester2.GetComponent<Animation>().Stop(this.tester2Anim);
      this.tester2.GetComponent<Animation>().Play(this.tester2Anim);
    }
    else
      Debug.Log((object) "NO 'tester2' object.");
    if ((Object) this.tester3 != (Object) null)
    {
      this.tester3.SetActive(true);
      this.tester3.GetComponent<Animation>().Stop(this.tester3Anim);
      this.tester3.GetComponent<Animation>().Play(this.tester3Anim);
    }
    else
      Debug.Log((object) "NO 'tester3' object.");
  }

  private void Update()
  {
  }
}
