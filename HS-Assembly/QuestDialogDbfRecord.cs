﻿// Decompiled with JetBrains decompiler
// Type: QuestDialogDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QuestDialogDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_OnCompleteBannerId;
  [SerializeField]
  private string m_NoteDesc;

  [DbfField("ON_COMPLETE_BANNER_ID", "id of banner to be shown after on complete dialog plays, if one exists.")]
  public int OnCompleteBannerId
  {
    get
    {
      return this.m_OnCompleteBannerId;
    }
  }

  [DbfField("NOTE_DESC", "designer description of card vo")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    QuestDialogDbfAsset questDialogDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (QuestDialogDbfAsset)) as QuestDialogDbfAsset;
    if ((UnityEngine.Object) questDialogDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("QuestDialogDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < questDialogDbfAsset.Records.Count; ++index)
      questDialogDbfAsset.Records[index].StripUnusedLocales();
    records = (object) questDialogDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetOnCompleteBannerId(int v)
  {
    this.m_OnCompleteBannerId = v;
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (QuestDialogDbfRecord.\u003C\u003Ef__switch\u0024map4D == null)
      {
        // ISSUE: reference to a compiler-generated field
        QuestDialogDbfRecord.\u003C\u003Ef__switch\u0024map4D = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "ON_COMPLETE_BANNER_ID",
            1
          },
          {
            "NOTE_DESC",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (QuestDialogDbfRecord.\u003C\u003Ef__switch\u0024map4D.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.OnCompleteBannerId;
          case 2:
            return (object) this.NoteDesc;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (QuestDialogDbfRecord.\u003C\u003Ef__switch\u0024map4E == null)
    {
      // ISSUE: reference to a compiler-generated field
      QuestDialogDbfRecord.\u003C\u003Ef__switch\u0024map4E = new Dictionary<string, int>(3)
      {
        {
          "ID",
          0
        },
        {
          "ON_COMPLETE_BANNER_ID",
          1
        },
        {
          "NOTE_DESC",
          2
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!QuestDialogDbfRecord.\u003C\u003Ef__switch\u0024map4E.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetOnCompleteBannerId((int) val);
        break;
      case 2:
        this.SetNoteDesc((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (QuestDialogDbfRecord.\u003C\u003Ef__switch\u0024map4F == null)
      {
        // ISSUE: reference to a compiler-generated field
        QuestDialogDbfRecord.\u003C\u003Ef__switch\u0024map4F = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "ON_COMPLETE_BANNER_ID",
            1
          },
          {
            "NOTE_DESC",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (QuestDialogDbfRecord.\u003C\u003Ef__switch\u0024map4F.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
