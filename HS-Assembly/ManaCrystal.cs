﻿// Decompiled with JetBrains decompiler
// Type: ManaCrystal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class ManaCrystal : MonoBehaviour
{
  private readonly string ANIM_SPAWN_EFFECTS = "mana_spawn_edit";
  private readonly string ANIM_TEMP_SPAWN_EFFECTS = "mana_spawn_edit_temp";
  private readonly string ANIM_MANA_GEM_BIRTH = "ManaGemBirth";
  private readonly string ANIM_TEMP_MANA_GEM_BIRTH = "ManaGemBirth_Temp";
  private readonly string ANIM_READY_TO_USED = "ManaGemUsed";
  private readonly string ANIM_USED_TO_READY = "ManaGem_Restore";
  private readonly string ANIM_READY_TO_PROPOSED = "ManaGemProposed";
  private readonly string ANIM_TEMP_READY_TO_PROPOSED = "ManaGemProposed_Temp";
  private readonly string ANIM_PROPOSED_TO_READY = "ManaGemProposed_Cancel";
  private readonly string ANIM_TEMP_PROPOSED_TO_READY = "ManaGemProposed_Cancel_Temp";
  private readonly string ANIM_USED_TO_PROPOSED = "ManaGemUsed_Proposed";
  private readonly string ANIM_PROPOSED_TO_USED = "ManaGemProposed_Used";
  private bool m_isInGame = true;
  public GameObject gem;
  public GameObject spawnEffects;
  public GameObject gemDestroy;
  public GameObject tempSpawnEffects;
  public GameObject tempGemDestroy;
  private bool m_birthAnimationPlayed;
  private bool m_playingAnimation;
  private bool m_isTemp;
  private Spell m_overloadOwedSpell;
  private Spell m_overloadPaidSpell;
  private ManaCrystal.State m_state;
  private ManaCrystal.State m_visibleState;

  public ManaCrystal.State state
  {
    get
    {
      return this.m_state;
    }
    set
    {
      if (this.m_state == ManaCrystal.State.DESTROYED)
        return;
      if (value == ManaCrystal.State.DESTROYED)
        this.Destroy();
      else
        this.m_state = value;
    }
  }

  private void Start()
  {
  }

  private void Update()
  {
    ManaCrystal.State state = this.state;
    if (state == this.m_visibleState || state == ManaCrystal.State.DESTROYED)
      return;
    this.PlayGemAnimation(this.GetTransitionAnimName(this.m_visibleState, state), state);
  }

  public void MarkAsNotInGame()
  {
    this.m_isInGame = false;
  }

  public void MarkAsTemp()
  {
    this.m_isTemp = true;
    ManaCrystalMgr manaCrystalMgr = ManaCrystalMgr.Get();
    this.gem.GetComponentInChildren<MeshRenderer>().material = manaCrystalMgr.tempManaCrystalMaterial;
    this.gem.transform.FindChild("Proposed_Quad").gameObject.GetComponent<MeshRenderer>().material = manaCrystalMgr.tempManaCrystalProposedQuadMaterial;
  }

  public void PlayCreateAnimation()
  {
    this.spawnEffects.SetActive(!this.m_isTemp);
    this.tempSpawnEffects.SetActive(this.m_isTemp);
    if (this.m_isTemp)
    {
      this.tempSpawnEffects.GetComponent<Animation>().Play(this.ANIM_TEMP_SPAWN_EFFECTS);
      this.PlayGemAnimation(this.ANIM_TEMP_MANA_GEM_BIRTH, ManaCrystal.State.READY);
    }
    else
    {
      this.spawnEffects.GetComponent<Animation>().Play(this.ANIM_SPAWN_EFFECTS);
      this.PlayGemAnimation(this.ANIM_MANA_GEM_BIRTH, ManaCrystal.State.READY);
    }
  }

  public void Destroy()
  {
    this.m_state = ManaCrystal.State.DESTROYED;
    this.StartCoroutine(this.WaitThenDestroy());
  }

  public bool IsOverloaded()
  {
    return (Object) this.m_overloadPaidSpell != (Object) null;
  }

  public bool IsOwedForOverload()
  {
    return (Object) this.m_overloadOwedSpell != (Object) null;
  }

  public void MarkAsOwedForOverload()
  {
    this.MarkAsOwedForOverload(false);
  }

  public void ReclaimOverload()
  {
    if (!this.IsOwedForOverload())
      return;
    this.m_overloadOwedSpell.RemoveStateFinishedCallback(new Spell.StateFinishedCallback(this.OnOverloadBirthCompletePayOverload));
    this.m_overloadOwedSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnOverloadUnlockedAnimComplete));
    this.m_overloadOwedSpell.ActivateState(SpellStateType.DEATH);
    this.m_overloadOwedSpell = (Spell) null;
  }

  public void PayOverload()
  {
    if (!this.IsOwedForOverload())
    {
      this.state = ManaCrystal.State.USED;
      this.MarkAsOwedForOverload(true);
    }
    else
    {
      this.m_overloadPaidSpell = this.m_overloadOwedSpell;
      this.m_overloadOwedSpell = (Spell) null;
      this.m_overloadPaidSpell.ActivateState(SpellStateType.ACTION);
    }
  }

  public void UnlockOverload()
  {
    if (!this.IsOverloaded())
      return;
    this.m_overloadPaidSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnOverloadUnlockedAnimComplete));
    this.m_overloadPaidSpell.ActivateState(SpellStateType.DEATH);
    this.m_overloadPaidSpell = (Spell) null;
  }

  private void PlayGemAnimation(string animName, ManaCrystal.State newVisibleState)
  {
    if (this.m_isInGame && !this.m_birthAnimationPlayed)
    {
      if (!animName.Equals(this.ANIM_MANA_GEM_BIRTH) && !animName.Equals(this.ANIM_TEMP_MANA_GEM_BIRTH))
        return;
      this.m_birthAnimationPlayed = true;
    }
    if (!(bool) ((TrackedReference) this.gem.GetComponent<Animation>()[animName]))
    {
      UnityEngine.Debug.LogWarning((object) string.Format("Mana gem animation named '{0}' doesn't exist.", (object) animName));
    }
    else
    {
      if (this.state == ManaCrystal.State.DESTROYED || this.m_playingAnimation)
        return;
      this.m_playingAnimation = true;
      this.gem.GetComponent<Animation>()[animName].normalizedTime = 1f;
      this.gem.GetComponent<Animation>()[animName].time = 0.0f;
      this.gem.GetComponent<Animation>()[animName].speed = 1f;
      this.gem.GetComponent<Animation>().Play(animName);
      if (!this.gameObject.activeInHierarchy)
      {
        this.m_playingAnimation = false;
        this.m_visibleState = newVisibleState;
      }
      else
        this.StartCoroutine(this.WaitForAnimation(animName, newVisibleState));
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitForAnimation(string animName, ManaCrystal.State newVisibleState)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ManaCrystal.\u003CWaitForAnimation\u003Ec__IteratorC0() { animName = animName, newVisibleState = newVisibleState, \u003C\u0024\u003EanimName = animName, \u003C\u0024\u003EnewVisibleState = newVisibleState, \u003C\u003Ef__this = this };
  }

  private string GetTransitionAnimName(ManaCrystal.State oldState, ManaCrystal.State newState)
  {
    string str = string.Empty;
    switch (oldState)
    {
      case ManaCrystal.State.READY:
        if (newState == ManaCrystal.State.PROPOSED)
        {
          str = !this.m_isTemp ? this.ANIM_READY_TO_PROPOSED : this.ANIM_TEMP_READY_TO_PROPOSED;
          break;
        }
        if (newState == ManaCrystal.State.USED)
        {
          str = this.ANIM_READY_TO_USED;
          break;
        }
        break;
      case ManaCrystal.State.USED:
        if (newState == ManaCrystal.State.READY)
        {
          str = this.ANIM_USED_TO_READY;
          break;
        }
        if (newState == ManaCrystal.State.PROPOSED)
        {
          str = this.ANIM_USED_TO_PROPOSED;
          break;
        }
        break;
      case ManaCrystal.State.PROPOSED:
        if (newState == ManaCrystal.State.READY)
        {
          str = !this.m_isTemp ? this.ANIM_PROPOSED_TO_READY : this.ANIM_TEMP_PROPOSED_TO_READY;
          break;
        }
        if (newState == ManaCrystal.State.USED)
        {
          str = this.ANIM_PROPOSED_TO_USED;
          break;
        }
        break;
      case ManaCrystal.State.DESTROYED:
        Log.Rachelle.Print("Trying to get an anim name for a mana that's been destroyed!!!");
        break;
    }
    return str;
  }

  [DebuggerHidden]
  private IEnumerator WaitThenDestroy()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ManaCrystal.\u003CWaitThenDestroy\u003Ec__IteratorC1() { \u003C\u003Ef__this = this };
  }

  private void OnGemDestroyedAnimComplete(Spell spell, SpellStateType spellStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    Object.Destroy((Object) this.gameObject);
  }

  private void OnOverloadUnlockedAnimComplete(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    Object.Destroy((Object) spell.transform.parent.gameObject);
  }

  private void OnOverloadBirthCompletePayOverload(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.IDLE)
      return;
    spell.RemoveStateFinishedCallback(new Spell.StateFinishedCallback(this.OnOverloadBirthCompletePayOverload));
    this.PayOverload();
  }

  public void MarkAsOwedForOverload(bool immediatelyLockForOverload)
  {
    if (this.IsOwedForOverload())
    {
      if (!immediatelyLockForOverload)
        return;
      this.PayOverload();
    }
    else
    {
      GameObject gameObject = (GameObject) GameUtils.InstantiateGameObject((string) ((MobileOverrideValue<string>) ManaCrystalMgr.Get().manaLockPrefab), this.gameObject, false);
      if ((bool) UniversalInputManager.UsePhoneUI)
      {
        gameObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
        gameObject.transform.localPosition = new Vector3(0.0f, 0.1f, 0.0f);
        float num = 1.1f;
        gameObject.transform.localScale = new Vector3(num, num, num);
      }
      else
      {
        float num = 1f / this.transform.localScale.x;
        gameObject.transform.localScale = new Vector3(num, num, num);
      }
      this.m_overloadOwedSpell = gameObject.transform.FindChild("Lock_Mana").GetComponent<Spell>();
      this.m_overloadOwedSpell.RemoveStateFinishedCallback(new Spell.StateFinishedCallback(this.OnOverloadUnlockedAnimComplete));
      if (immediatelyLockForOverload)
        this.m_overloadOwedSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnOverloadBirthCompletePayOverload));
      this.m_overloadOwedSpell.ActivateState(SpellStateType.BIRTH);
    }
  }

  public enum State
  {
    READY,
    USED,
    PROPOSED,
    DESTROYED,
  }
}
