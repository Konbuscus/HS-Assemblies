﻿// Decompiled with JetBrains decompiler
// Type: AllyAttackDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class AllyAttackDef
{
  public float m_MoveToTargetDuration = 0.12f;
  public iTween.EaseType m_MoveToTargetEaseType = iTween.EaseType.linear;
}
