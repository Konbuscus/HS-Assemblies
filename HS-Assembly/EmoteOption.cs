﻿// Decompiled with JetBrains decompiler
// Type: EmoteOption
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EmoteOption : MonoBehaviour
{
  public EmoteType m_EmoteType;
  public string m_StringTag;
  public EmoteType m_FallbackEmoteType;
  public string m_FallbackStringTag;
  public MeshRenderer m_Backplate;
  public UberText m_Text;
  private EmoteType m_currentEmoteType;
  private string m_currentStringTag;
  private Vector3 m_startingScale;
  private bool m_textIsGrey;

  private void Awake()
  {
    this.UpdateEmoteType();
    if ((Object) this.m_Text != (Object) null)
      this.m_Text.gameObject.SetActive(false);
    if ((Object) this.m_Backplate != (Object) null)
      this.m_Backplate.enabled = false;
    this.m_startingScale = this.transform.localScale;
    this.transform.localScale = Vector3.zero;
  }

  private void Update()
  {
    if ((Object) this.m_Text == (Object) null)
      return;
    if (EmoteHandler.Get().EmoteSpamBlocked())
    {
      if (this.m_textIsGrey)
        return;
      this.m_textIsGrey = true;
      this.m_Text.TextColor = new Color(0.5372549f, 0.5372549f, 0.5372549f);
    }
    else
    {
      if (!this.m_textIsGrey)
        return;
      this.m_textIsGrey = false;
      this.m_Text.TextColor = new Color(0.0f, 0.0f, 0.0f);
    }
  }

  public void DoClick()
  {
    EmoteHandler.Get().ResetTimeSinceLastEmote();
    GameState.Get().GetFriendlySidePlayer().GetHeroCard().PlayEmote(this.m_currentEmoteType);
    Network.Get().SendEmote(this.m_EmoteType);
    EmoteHandler.Get().HideEmotes();
  }

  public void Enable()
  {
    this.m_Backplate.enabled = true;
    this.m_Text.gameObject.SetActive(true);
    this.GetComponent<Collider>().enabled = true;
    iTween.Stop(this.gameObject);
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) this.m_startingScale, (object) "time", (object) 0.5f, (object) "ignoretimescale", (object) true, (object) "easetype", (object) iTween.EaseType.easeOutElastic));
  }

  public void Disable()
  {
    this.GetComponent<Collider>().enabled = false;
    iTween.Stop(this.gameObject);
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) Vector3.zero, (object) "time", (object) 0.1f, (object) "ignoretimescale", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "FinishDisable"));
  }

  public void HandleMouseOut()
  {
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) this.m_startingScale, (object) "time", (object) 0.2f, (object) "ignoretimescale", (object) true));
  }

  public void HandleMouseOver()
  {
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) (this.m_startingScale * 1.1f), (object) "time", (object) 0.2f, (object) "ignoretimescale", (object) true));
  }

  public void UpdateEmoteType()
  {
    if (this.ShouldUseFallbackEmote())
    {
      this.m_currentEmoteType = this.m_FallbackEmoteType;
      this.m_currentStringTag = this.m_FallbackStringTag;
    }
    else
    {
      this.m_currentEmoteType = this.m_EmoteType;
      this.m_currentStringTag = this.m_StringTag;
    }
    if (!((Object) this.m_Text != (Object) null))
      return;
    this.m_Text.Text = GameStrings.Get(this.m_currentStringTag);
  }

  private bool ShouldUseFallbackEmote()
  {
    Player friendlySidePlayer = GameState.Get().GetFriendlySidePlayer();
    if (friendlySidePlayer == null)
      return false;
    Card heroCard = friendlySidePlayer.GetHeroCard();
    return !((Object) heroCard == (Object) null) && heroCard.GetEmoteEntry(this.m_EmoteType) == null && heroCard.GetEmoteEntry(this.m_FallbackEmoteType) != null;
  }

  private void FinishDisable()
  {
    if (this.GetComponent<Collider>().enabled)
      return;
    this.m_Backplate.enabled = false;
    this.m_Text.gameObject.SetActive(false);
  }
}
