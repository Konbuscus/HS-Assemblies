﻿// Decompiled with JetBrains decompiler
// Type: ArcaneDustAmount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArcaneDustAmount : MonoBehaviour
{
  public UberText m_dustCount;
  public GameObject m_dustJar;
  public GameObject m_dustFX;
  public GameObject m_explodeFX_Common;
  public GameObject m_explodeFX_Rare;
  public GameObject m_explodeFX_Epic;
  public GameObject m_explodeFX_Legendary;
  private static ArcaneDustAmount s_instance;

  private void Awake()
  {
    ArcaneDustAmount.s_instance = this;
  }

  private void Start()
  {
    this.UpdateCurrentDustAmount();
  }

  public static ArcaneDustAmount Get()
  {
    return ArcaneDustAmount.s_instance;
  }

  public void UpdateCurrentDustAmount()
  {
    this.m_dustCount.Text = CraftingManager.Get().GetLocalArcaneDustBalance().ToString();
  }
}
