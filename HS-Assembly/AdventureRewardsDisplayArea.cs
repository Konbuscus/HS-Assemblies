﻿// Decompiled with JetBrains decompiler
// Type: AdventureRewardsDisplayArea
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class AdventureRewardsDisplayArea : MonoBehaviour
{
  [CustomEditField(Sections = "UI")]
  public float m_RewardsCardSpacing = 10f;
  private List<Actor> m_CurrentCardRewards = new List<Actor>();
  [CustomEditField(Sections = "UI")]
  public GameObject m_RewardsCardArea;
  [CustomEditField(Sections = "UI")]
  public Vector3 m_RewardsCardOffset;
  [CustomEditField(Sections = "UI")]
  public float m_RewardsCardMouseOffset;
  [CustomEditField(Sections = "UI")]
  public Vector3 m_RewardsCardScale;
  [CustomEditField(Sections = "UI")]
  public Vector3 m_RewardsCardDriftAmount;
  [CustomEditField(Sections = "UI")]
  public bool m_EnableFullscreenMode;
  [CustomEditField(Parent = "m_EnableFullscreenMode", Sections = "UI")]
  public PegUIElement m_FullscreenModeOffClicker;
  [CustomEditField(Parent = "m_EnableFullscreenMode", Sections = "UI")]
  public UIBScrollable m_FullscreenDisableScrollBar;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_CardPreviewAppearSound;
  private bool m_FullscreenEnabled;
  private bool m_Showing;

  private void Awake()
  {
    if ((UnityEngine.Object) this.m_FullscreenModeOffClicker != (UnityEngine.Object) null)
      this.m_FullscreenModeOffClicker.AddEventListener(UIEventType.PRESS, (UIEvent.Handler) (e => this.HideCardRewards()));
    if (!((UnityEngine.Object) this.m_FullscreenDisableScrollBar != (UnityEngine.Object) null))
      return;
    this.m_FullscreenDisableScrollBar.AddTouchScrollStartedListener(new UIBScrollable.OnTouchScrollStarted(this.HideCardRewards));
  }

  private void OnDestroy()
  {
    this.DisableFullscreen();
  }

  public bool IsShowing()
  {
    return this.m_Showing;
  }

  public void ShowCardsNoFullscreen(List<CardRewardData> rewards, Vector3 finalPosition, Vector3? origin = null)
  {
    List<string> cardIds = new List<string>();
    using (List<CardRewardData>.Enumerator enumerator = rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardRewardData current = enumerator.Current;
        cardIds.Add(current.CardID);
      }
    }
    this.ShowCardsNoFullscreen(cardIds, finalPosition, origin);
  }

  public void ShowCards(List<CardRewardData> rewards, Vector3 finalPosition, Vector3? origin = null)
  {
    List<string> cardIds = new List<string>();
    using (List<CardRewardData>.Enumerator enumerator = rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardRewardData current = enumerator.Current;
        cardIds.Add(current.CardID);
      }
    }
    this.ShowCards(cardIds, finalPosition, origin);
  }

  public void ShowCardsNoFullscreen(List<string> cardIds, Vector3 finalPosition, Vector3? origin = null)
  {
    this.DoShowCardRewards(cardIds, new Vector3?(finalPosition), origin, true);
  }

  public void ShowCards(List<string> cardIds, Vector3 finalPosition, Vector3? origin = null)
  {
    if (this.m_Showing)
      return;
    this.m_Showing = true;
    if (this.m_EnableFullscreenMode)
      this.DoShowCardRewards(cardIds, new Vector3?(), origin, false);
    else
      this.DoShowCardRewards(cardIds, new Vector3?(finalPosition), origin, false);
  }

  public void HideCardRewards()
  {
    this.m_Showing = false;
    using (List<Actor>.Enumerator enumerator = this.m_CurrentCardRewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Actor current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_CurrentCardRewards.Clear();
    this.DisableFullscreen();
  }

  private void DoShowCardRewards(List<string> cardIds, Vector3? finalPosition, Vector3? origin, bool disableFullscreen)
  {
    int index = 0;
    int count = cardIds.Count;
    using (List<string>.Enumerator enumerator = cardIds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FullDef fullDef = DefLoader.Get().GetFullDef(enumerator.Current, (CardPortraitQuality) null);
        GameObject gameObject = AssetLoader.Get().LoadActor(ActorNames.GetHandActor(fullDef.GetEntityDef(), TAG_PREMIUM.NORMAL), false, false);
        Actor component1 = gameObject.GetComponent<Actor>();
        component1.SetCardDef(fullDef.GetCardDef());
        component1.SetEntityDef(fullDef.GetEntityDef());
        if ((UnityEngine.Object) component1.m_cardMesh != (UnityEngine.Object) null)
        {
          BoxCollider component2 = component1.m_cardMesh.GetComponent<BoxCollider>();
          if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
            component2.enabled = false;
        }
        this.m_CurrentCardRewards.Add(component1);
        GameUtils.SetParent((Component) component1, this.m_RewardsCardArea, false);
        this.ShowCardRewardsObject(gameObject, finalPosition, origin, index, count);
        ++index;
      }
    }
    this.EnableFullscreen(disableFullscreen);
  }

  private void ShowCardRewardsObject(GameObject obj, Vector3? finalPosition, Vector3? origin, int index, int totalCount)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureRewardsDisplayArea.\u003CShowCardRewardsObject\u003Ec__AnonStorey357 objectCAnonStorey357 = new AdventureRewardsDisplayArea.\u003CShowCardRewardsObject\u003Ec__AnonStorey357();
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey357.obj = obj;
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey357.\u003C\u003Ef__this = this;
    Vector3 vector3;
    if (finalPosition.HasValue)
    {
      Vector3 min = this.m_RewardsCardArea.GetComponent<Collider>().bounds.min;
      Vector3 max = this.m_RewardsCardArea.GetComponent<Collider>().bounds.max;
      vector3 = finalPosition.Value + this.m_RewardsCardOffset;
      float num = (float) index * this.m_RewardsCardSpacing;
      vector3.z = Mathf.Clamp(vector3.z, min.z, max.z);
      if ((double) vector3.x + (double) this.m_RewardsCardMouseOffset > (double) max.x)
        vector3.x -= this.m_RewardsCardMouseOffset + num;
      else
        vector3.x += this.m_RewardsCardMouseOffset + num;
    }
    else
    {
      vector3 = this.m_RewardsCardArea.transform.position + this.m_RewardsCardOffset;
      float num = (float) index * this.m_RewardsCardSpacing;
      vector3.x += num;
      vector3.x -= (float) ((double) (totalCount - 1) * (double) this.m_RewardsCardSpacing * 0.5);
    }
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey357.obj.transform.localScale = this.m_RewardsCardScale;
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey357.obj.transform.position = vector3;
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey357.obj.SetActive(true);
    if (this.m_EnableFullscreenMode)
    {
      // ISSUE: reference to a compiler-generated field
      SceneUtils.SetLayer(objectCAnonStorey357.obj, GameLayer.IgnoreFullScreenEffects);
      if ((UnityEngine.Object) this.m_FullscreenModeOffClicker != (UnityEngine.Object) null)
        SceneUtils.SetLayer((Component) this.m_FullscreenModeOffClicker, GameLayer.IgnoreFullScreenEffects);
    }
    // ISSUE: reference to a compiler-generated field
    iTween.StopByName(objectCAnonStorey357.obj, "REWARD_SCALE_UP");
    // ISSUE: reference to a compiler-generated field
    iTween.ScaleFrom(objectCAnonStorey357.obj, iTween.Hash((object) "scale", (object) (Vector3.one * 0.05f), (object) "time", (object) 0.15f, (object) "easeType", (object) iTween.EaseType.easeOutQuart, (object) "name", (object) "REWARD_SCALE_UP"));
    if (origin.HasValue)
    {
      // ISSUE: reference to a compiler-generated field
      iTween.StopByName(objectCAnonStorey357.obj, "REWARD_MOVE_FROM_ORIGIN");
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      iTween.MoveFrom(objectCAnonStorey357.obj, iTween.Hash((object) "position", (object) origin.Value, (object) "time", (object) 0.15f, (object) "easeType", (object) iTween.EaseType.easeOutQuart, (object) "name", (object) "REWARD_MOVE_FROM_ORIGIN", (object) "oncomplete", (object) new Action<object>(objectCAnonStorey357.\u003C\u003Em__2D)));
    }
    else if (this.m_RewardsCardDriftAmount != Vector3.zero)
    {
      // ISSUE: reference to a compiler-generated field
      AnimationUtil.DriftObject(objectCAnonStorey357.obj, this.m_RewardsCardDriftAmount);
    }
    if (string.IsNullOrEmpty(this.m_CardPreviewAppearSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_CardPreviewAppearSound));
  }

  private void EnableFullscreen(bool disableFullscreen)
  {
    if (!this.m_EnableFullscreenMode || disableFullscreen)
      return;
    FullScreenFXMgr.Get().StartStandardBlurVignette(0.25f);
    if ((UnityEngine.Object) this.m_FullscreenModeOffClicker != (UnityEngine.Object) null)
      this.m_FullscreenModeOffClicker.gameObject.SetActive(true);
    this.m_FullscreenEnabled = true;
  }

  private void DisableFullscreen()
  {
    if (!this.m_FullscreenEnabled)
      return;
    if ((UnityEngine.Object) FullScreenFXMgr.Get() != (UnityEngine.Object) null)
      FullScreenFXMgr.Get().EndStandardBlurVignette(0.25f, (FullScreenFXMgr.EffectListener) null);
    if ((UnityEngine.Object) this.m_FullscreenModeOffClicker != (UnityEngine.Object) null)
      this.m_FullscreenModeOffClicker.gameObject.SetActive(false);
    this.m_FullscreenEnabled = false;
  }
}
