﻿// Decompiled with JetBrains decompiler
// Type: LOE03_AncientTemple
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class LOE03_AncientTemple : LOE_MissionEntity
{
  private Notification m_turnCounter;
  private TempleArt m_templeArt;
  private int m_mostRecentMissionEvent;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOE_03_TURN_5_4");
    this.PreloadSound("VO_LOE_03_TURN_6");
    this.PreloadSound("VO_LOE_03_TURN_9");
    this.PreloadSound("VO_LOE_03_TURN_5");
    this.PreloadSound("VO_LOE_03_TURN_4_GOOD");
    this.PreloadSound("VO_LOE_03_TURN_4_BAD");
    this.PreloadSound("VO_LOE_03_TURN_6_2");
    this.PreloadSound("VO_LOE_03_TURN_4_NEITHER");
    this.PreloadSound("VO_LOE_03_TURN_3_WARNING");
    this.PreloadSound("VO_LOE_03_TURN_2");
    this.PreloadSound("VO_LOE_03_TURN_2_2");
    this.PreloadSound("VO_LOE_03_TURN_4");
    this.PreloadSound("VO_LOE_03_TURN_7");
    this.PreloadSound("VO_LOE_03_TURN_7_2");
    this.PreloadSound("VO_LOE_03_TURN_3_BOULDER");
    this.PreloadSound("VO_LOE_03_TURN_1");
    this.PreloadSound("VO_LOE_03_TURN_8");
    this.PreloadSound("VO_LOE_03_TURN_10");
    this.PreloadSound("VO_LOE_03_WIN");
    this.PreloadSound("VO_LOE_WING_1_WIN_2");
    this.PreloadSound("VO_LOE_03_RESPONSE");
  }

  public override bool ShouldHandleCoin()
  {
    return false;
  }

  public override void NotifyOfMulliganInitialized()
  {
    base.NotifyOfMulliganInitialized();
    this.m_mostRecentMissionEvent = this.GetTag(GAME_TAG.MISSION_EVENT);
    this.InitVisuals();
  }

  public override void NotifyOfMulliganEnded()
  {
    base.NotifyOfMulliganEnded();
    GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor().GetHealthObject().Hide();
  }

  public override void OnTagChanged(TagDelta change)
  {
    base.OnTagChanged(change);
    if (change.tag != 48)
      return;
    this.UpdateVisuals(change.newValue);
  }

  public override string CustomChoiceBannerText()
  {
    if (this.GetTag<TAG_STEP>(GAME_TAG.STEP) == TAG_STEP.MAIN_START)
    {
      string key = (string) null;
      switch (this.m_mostRecentMissionEvent)
      {
        case 10:
          key = "MISSION_GLOWING_POOL";
          break;
        case 11:
          key = "MISSION_PIT_OF_SPIKES";
          break;
        case 12:
          key = "MISSION_TAKE_THE_SHORTCUT";
          break;
        case 4:
          key = "MISSION_STATUES_EYE";
          break;
      }
      if (key != null)
        return GameStrings.Get(key);
    }
    return (string) null;
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE03_AncientTemple.\u003CHandleMissionEventWithTiming\u003Ec__Iterator163() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE03_AncientTemple.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator164() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  protected override void PlayEmoteResponse(EmoteType emoteType, CardSoundSpell emoteSpell)
  {
    if (!MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS.Contains(emoteType))
      return;
    Gameplay.Get().StartCoroutine(this.PlayBigCharacterQuoteAndWait("Reno_BigQuote", "VO_LOE_03_RESPONSE", 3f, 1f, true, false));
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE03_AncientTemple.\u003CHandleGameOverWithTiming\u003Ec__Iterator165() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }

  private void InitVisuals()
  {
    int cost = this.GetCost();
    int tag = this.GetTag(GAME_TAG.TURN);
    this.InitTempleArt(cost);
    if (tag < 1 || !GameState.Get().IsPastBeginPhase())
      return;
    this.InitTurnCounter(cost);
  }

  private void InitTempleArt(int cost)
  {
    this.m_templeArt = AssetLoader.Get().LoadGameObject("TempleArt", true, false).GetComponent<TempleArt>();
    this.UpdateTempleArt(cost);
  }

  private void InitTurnCounter(int cost)
  {
    this.m_turnCounter = AssetLoader.Get().LoadGameObject("LOE_Turn_Timer", true, false).GetComponent<Notification>();
    PlayMakerFSM component = this.m_turnCounter.GetComponent<PlayMakerFSM>();
    component.FsmVariables.GetFsmBool("RunningMan").Value = true;
    component.FsmVariables.GetFsmBool("MineCart").Value = false;
    component.SendEvent("Birth");
    this.m_turnCounter.transform.parent = GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor().gameObject.transform;
    this.m_turnCounter.transform.localPosition = new Vector3(-1.4f, 0.187f, -0.11f);
    this.m_turnCounter.transform.localScale = Vector3.one * 0.52f;
    this.UpdateTurnCounterText(cost);
  }

  private void UpdateVisuals(int cost)
  {
    this.UpdateTempleArt(cost);
    if (!(bool) ((Object) this.m_turnCounter))
      return;
    this.UpdateTurnCounter(cost);
  }

  private void UpdateTempleArt(int cost)
  {
    this.m_templeArt.DoPortraitSwap(GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor(), cost);
  }

  private void UpdateTurnCounter(int cost)
  {
    this.m_turnCounter.GetComponent<PlayMakerFSM>().SendEvent("Action");
    this.UpdateTurnCounterText(cost);
  }

  private void UpdateTurnCounterText(int cost)
  {
    this.m_turnCounter.ChangeDialogText(GameStrings.FormatPlurals("MISSION_DEFAULTCOUNTERNAME", new GameStrings.PluralNumber[1]
    {
      new GameStrings.PluralNumber()
      {
        m_index = 0,
        m_number = cost
      }
    }), cost.ToString(), string.Empty, string.Empty);
  }
}
