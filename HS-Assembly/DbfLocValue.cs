﻿// Decompiled with JetBrains decompiler
// Type: DbfLocValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DbfLocValue
{
  [SerializeField]
  private List<Locale> m_locales = new List<Locale>();
  [SerializeField]
  private List<string> m_locValues = new List<string>();
  private bool m_hideDebugInfo = true;
  [SerializeField]
  private int m_locId;
  private int m_recordId;
  private string m_recordColumn;

  public DbfLocValue()
  {
  }

  public DbfLocValue(bool hideDebugInfo)
  {
    this.m_hideDebugInfo = hideDebugInfo;
  }

  public static implicit operator string(DbfLocValue v)
  {
    if (v != null)
      return v.GetString(true);
    return string.Empty;
  }

  public string GetString(bool defaultToLoadOrder = true)
  {
    return this.GetString(Localization.GetLocale(), defaultToLoadOrder);
  }

  public string GetString(Locale loc, bool defaultToLoadOrder = true)
  {
    if (this.m_locales.Count > 0)
    {
      int index1 = this.m_locales.IndexOf(loc);
      if (index1 >= 0)
        return this.m_locValues[index1];
      foreach (Locale locale in Localization.GetLoadOrder(false))
      {
        int index2 = this.m_locales.IndexOf(locale);
        if (index2 >= 0)
          return this.m_locValues[index2];
      }
    }
    if (!this.m_hideDebugInfo)
      return string.Format("ID={0} COLUMN={1}", (object) this.m_recordId, (object) this.m_recordColumn);
    return string.Empty;
  }

  public void SetString(Locale loc, string value)
  {
    int index = this.m_locales.IndexOf(loc);
    if (index >= 0)
    {
      this.m_locValues[index] = value;
    }
    else
    {
      this.m_locales.Add(loc);
      this.m_locValues.Add(value);
    }
  }

  public void SetString(string value)
  {
    this.SetString(Localization.GetLocale(), value);
  }

  public void SetLocId(int locId)
  {
    this.m_locId = locId;
  }

  public int GetLocId()
  {
    return this.m_locId;
  }

  public void SetDebugInfo(int recordId, string recordColumn)
  {
    this.m_recordId = recordId;
    this.m_recordColumn = recordColumn;
  }

  public void StripUnusedLocales()
  {
    if (this.m_locales.Count <= 1)
      return;
    Locale[] loadOrder = Localization.GetLoadOrder(false);
    List<Locale> localeList = new List<Locale>();
    List<string> stringList = new List<string>();
    for (int index1 = 0; index1 < loadOrder.Length; ++index1)
    {
      int index2 = this.m_locales.IndexOf(loadOrder[index1]);
      if (index2 >= 0)
      {
        localeList.Add(this.m_locales[index2]);
        stringList.Add(this.m_locValues[index2]);
      }
    }
    this.m_locales = localeList;
    this.m_locValues = stringList;
  }
}
