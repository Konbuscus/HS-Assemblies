﻿// Decompiled with JetBrains decompiler
// Type: HistoryManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusGame;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class HistoryManager : MonoBehaviour
{
  private List<HistoryCard> m_historyTiles = new List<HistoryCard>();
  private List<HistoryManager.TileEntry> m_queuedEntries = new List<HistoryManager.TileEntry>();
  private const float BIG_CARD_POWER_PROCESSOR_DELAY_TIME = 1f;
  private const float BIG_CARD_SPELL_DISPLAY_TIME = 4f;
  private const float BIG_CARD_MINION_DISPLAY_TIME = 3f;
  private const float BIG_CARD_HERO_POWER_DISPLAY_TIME = 4f;
  private const float BIG_CARD_SECRET_DISPLAY_TIME = 4f;
  private const float BIG_CARD_POST_TRANSFORM_DISPLAY_TIME = 2f;
  private const float BIG_CARD_META_DATA_DISPLAY_TIME_SCALAR = 0.375f;
  private const float SPACE_BETWEEN_TILES = 0.15f;
  public Texture m_mageSecretTexture;
  public Texture m_paladinSecretTexture;
  public Texture m_hunterSecretTexture;
  public Texture m_wandererSecretTexture;
  public Texture m_FatigueTexture;
  public SoundDucker m_SoundDucker;
  public Spell m_TransformSpell;
  private static HistoryManager s_instance;
  private bool m_historyDisabled;
  private HistoryCard m_currentlyMousedOverTile;
  private bool m_animatingVignette;
  private bool m_animatingDesat;
  private Vector3[] m_bigCardPath;
  private HistoryManager.BigCardEntry m_pendingBigCardEntry;
  private HistoryCard m_currentBigCard;
  private bool m_showingBigCard;
  private bool m_bigCardWaitingForSecret;
  private HistoryManager.BigCardTransformState m_bigCardTransformState;
  private Spell m_bigCardTransformSpell;

  private void Awake()
  {
    HistoryManager.s_instance = this;
    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.15f, this.transform.position.z);
  }

  private void OnDestroy()
  {
    HistoryManager.s_instance = (HistoryManager) null;
  }

  private void Start()
  {
    this.StartCoroutine(this.WaitForBoardLoadedAndSetPaths());
  }

  public static HistoryManager Get()
  {
    return HistoryManager.s_instance;
  }

  public void DisableHistory()
  {
    this.m_historyDisabled = true;
  }

  private Entity CreatePreTransformedEntity(Entity entity)
  {
    int tag = entity.GetTag(GAME_TAG.TRANSFORMED_FROM_CARD);
    if (tag == 0)
      return (Entity) null;
    string cardId = GameUtils.TranslateDbIdToCardId(tag);
    if (string.IsNullOrEmpty(cardId))
      return (Entity) null;
    Entity entity1 = new Entity();
    EntityDef entityDef = DefLoader.Get().GetEntityDef(cardId);
    entity1.InitCard();
    entity1.ReplaceTags(entityDef.GetTags());
    entity1.LoadCard(cardId);
    entity1.SetTag(GAME_TAG.CONTROLLER, entity.GetControllerId());
    entity1.SetTag<TAG_ZONE>(GAME_TAG.ZONE, TAG_ZONE.HAND);
    entity1.SetTag<TAG_PREMIUM>(GAME_TAG.PREMIUM, entity.GetPremiumType());
    return entity1;
  }

  public void CreatePlayedTile(Entity playedEntity, Entity targetedEntity)
  {
    if (this.m_historyDisabled)
      return;
    HistoryManager.TileEntry tileEntry = new HistoryManager.TileEntry();
    this.m_queuedEntries.Add(tileEntry);
    tileEntry.SetCardPlayed(playedEntity);
    tileEntry.SetCardTargeted(targetedEntity);
    if (tileEntry.m_lastCardPlayed.GetDuplicatedEntity() != null)
      return;
    this.StartCoroutine("WaitForCardLoadedAndDuplicateInfo", (object) tileEntry.m_lastCardPlayed);
  }

  public void CreateTriggerTile(Entity triggeredEntity)
  {
    if (this.m_historyDisabled)
      return;
    HistoryManager.TileEntry tileEntry = new HistoryManager.TileEntry();
    this.m_queuedEntries.Add(tileEntry);
    tileEntry.SetCardTriggered(triggeredEntity);
  }

  public void CreateAttackTile(Entity attacker, Entity defender, PowerTaskList taskList)
  {
    if (this.m_historyDisabled)
      return;
    HistoryManager.TileEntry tileEntry = new HistoryManager.TileEntry();
    this.m_queuedEntries.Add(tileEntry);
    tileEntry.SetAttacker(attacker);
    tileEntry.SetDefender(defender);
    Entity duplicatedEntity1 = tileEntry.m_lastAttacker.GetDuplicatedEntity();
    Entity duplicatedEntity2 = tileEntry.m_lastDefender.GetDuplicatedEntity();
    int entityId1 = attacker.GetEntityId();
    int entityId2 = defender.GetEntityId();
    int num = -1;
    List<PowerTask> taskList1 = taskList.GetTaskList();
    for (int index = 0; index < taskList1.Count; ++index)
    {
      Network.PowerHistory power = taskList1[index].GetPower();
      if (power.Type == Network.PowerType.META_DATA)
      {
        Network.HistMetaData histMetaData = (Network.HistMetaData) power;
        if (histMetaData.MetaType == HistoryMeta.Type.DAMAGE && histMetaData.Info.Contains(entityId2))
        {
          num = index;
          break;
        }
      }
    }
    for (int index = 0; index < num; ++index)
    {
      Network.PowerHistory power = taskList1[index].GetPower();
      switch (power.Type)
      {
        case Network.PowerType.SHOW_ENTITY:
          Network.HistShowEntity showEntity = (Network.HistShowEntity) power;
          if (entityId1 == showEntity.Entity.ID)
            GameUtils.ApplyShowEntity(duplicatedEntity1, showEntity);
          if (entityId2 == showEntity.Entity.ID)
          {
            GameUtils.ApplyShowEntity(duplicatedEntity2, showEntity);
            break;
          }
          break;
        case Network.PowerType.HIDE_ENTITY:
          Network.HistHideEntity hideEntity = (Network.HistHideEntity) power;
          if (entityId1 == hideEntity.Entity)
            GameUtils.ApplyHideEntity(duplicatedEntity1, hideEntity);
          if (entityId2 == hideEntity.Entity)
          {
            GameUtils.ApplyHideEntity(duplicatedEntity2, hideEntity);
            break;
          }
          break;
        case Network.PowerType.TAG_CHANGE:
          Network.HistTagChange tagChange = (Network.HistTagChange) power;
          if (entityId1 == tagChange.Entity)
            GameUtils.ApplyTagChange(duplicatedEntity1, tagChange);
          if (entityId2 == tagChange.Entity)
          {
            GameUtils.ApplyTagChange(duplicatedEntity2, tagChange);
            break;
          }
          break;
      }
    }
  }

  public void CreateFatigueTile()
  {
    if (this.m_historyDisabled)
      return;
    HistoryManager.TileEntry tileEntry = new HistoryManager.TileEntry();
    this.m_queuedEntries.Add(tileEntry);
    tileEntry.SetFatigue();
  }

  public void MarkCurrentHistoryEntryAsCompleted()
  {
    if (this.m_historyDisabled)
      return;
    this.GetCurrentHistoryEntry().m_complete = true;
    this.LoadNextHistoryEntry();
  }

  public bool HasHistoryEntry()
  {
    return this.GetCurrentHistoryEntry() != null;
  }

  public void NotifyDamageChanged(Entity entity, int damage)
  {
    if (entity == null || this.m_historyDisabled)
      return;
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    int num = damage - entity.GetDamage();
    if (this.IsEntityTheLastCardPlayed(entity))
      currentHistoryEntry.m_lastCardPlayed.m_damageChangeAmount = num;
    else if (this.IsEntityTheLastAttacker(entity))
      currentHistoryEntry.m_lastAttacker.m_damageChangeAmount = num;
    else if (this.IsEntityTheLastDefender(entity))
      currentHistoryEntry.m_lastDefender.m_damageChangeAmount = num;
    else if (this.IsEntityTheLastCardTargeted(entity))
    {
      currentHistoryEntry.m_lastCardTargeted.m_damageChangeAmount = num;
    }
    else
    {
      for (int index = 0; index < currentHistoryEntry.m_affectedCards.Count; ++index)
      {
        if (this.IsEntityTheAffectedCard(entity, index))
        {
          currentHistoryEntry.m_affectedCards[index].m_damageChangeAmount = num;
          return;
        }
      }
      if (!this.NotifyEntityAffected(entity, false, false))
        return;
      this.NotifyDamageChanged(entity, damage);
    }
  }

  public void NotifyArmorChanged(Entity entity, int newArmor)
  {
    if (this.m_historyDisabled)
      return;
    int num = entity.GetArmor() - newArmor;
    if (num <= 0 || this.IsEntityTheLastCardPlayed(entity))
      return;
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    if (this.IsEntityTheLastAttacker(entity))
      currentHistoryEntry.m_lastAttacker.m_armorChangeAmount = num;
    else if (this.IsEntityTheLastDefender(entity))
      currentHistoryEntry.m_lastDefender.m_armorChangeAmount = num;
    else if (this.IsEntityTheLastCardTargeted(entity))
    {
      currentHistoryEntry.m_lastCardTargeted.m_armorChangeAmount = num;
    }
    else
    {
      for (int index = 0; index < currentHistoryEntry.m_affectedCards.Count; ++index)
      {
        if (this.IsEntityTheAffectedCard(entity, index))
        {
          currentHistoryEntry.m_affectedCards[index].m_armorChangeAmount = num;
          return;
        }
      }
      if (!this.NotifyEntityAffected(entity, false, false))
        return;
      this.NotifyArmorChanged(entity, newArmor);
    }
  }

  public void OverrideCurrentHistoryEntryWithMetaData()
  {
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    if (currentHistoryEntry == null || currentHistoryEntry.m_usingMetaDataOverride)
      return;
    currentHistoryEntry.m_usingMetaDataOverride = true;
    currentHistoryEntry.m_affectedCards.Clear();
  }

  public bool NotifyEntityAffected(int entityId, bool allowDuplicates, bool fromMetaData)
  {
    return this.NotifyEntityAffected(GameState.Get().GetEntity(entityId), allowDuplicates, fromMetaData);
  }

  public bool NotifyEntityAffected(Entity entity, bool allowDuplicates, bool fromMetaData)
  {
    if (this.m_historyDisabled || entity.IsEnchantment())
      return false;
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    if (!fromMetaData && currentHistoryEntry.m_usingMetaDataOverride)
      return false;
    if (!allowDuplicates)
    {
      if (this.IsEntityTheLastAttacker(entity) || this.IsEntityTheLastDefender(entity) || this.IsEntityTheLastCardTargeted(entity) || currentHistoryEntry.m_lastCardPlayed != null && entity == currentHistoryEntry.m_lastCardPlayed.GetOriginalEntity())
        return false;
      for (int index = 0; index < currentHistoryEntry.m_affectedCards.Count; ++index)
      {
        if (this.IsEntityTheAffectedCard(entity, index))
          return false;
      }
    }
    HistoryInfo historyInfo = new HistoryInfo();
    historyInfo.SetOriginalEntity(entity);
    currentHistoryEntry.m_affectedCards.Add(historyInfo);
    return true;
  }

  public void NotifyEntityDied(int entityId)
  {
    this.NotifyEntityDied(GameState.Get().GetEntity(entityId));
  }

  public void NotifyEntityDied(Entity entity)
  {
    if (this.m_historyDisabled || entity.IsEnchantment() || this.IsEntityTheLastCardPlayed(entity))
      return;
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    if (this.IsEntityTheLastAttacker(entity))
      currentHistoryEntry.m_lastAttacker.SetDied(true);
    else if (this.IsEntityTheLastDefender(entity))
      currentHistoryEntry.m_lastDefender.SetDied(true);
    else if (this.IsEntityTheLastCardTargeted(entity))
    {
      currentHistoryEntry.m_lastCardTargeted.SetDied(true);
    }
    else
    {
      for (int index = 0; index < currentHistoryEntry.m_affectedCards.Count; ++index)
      {
        if (this.IsEntityTheAffectedCard(entity, index))
        {
          currentHistoryEntry.m_affectedCards[index].SetDied(true);
          return;
        }
      }
      if (this.IsDeadInLaterHistoryEntry(entity) || !this.NotifyEntityAffected(entity, false, false))
        return;
      this.NotifyEntityDied(entity);
    }
  }

  public void NotifyOfInput(float zPosition)
  {
    if (this.m_historyTiles.Count == 0)
    {
      this.CheckForMouseOff();
    }
    else
    {
      float num1 = 1000f;
      float num2 = -1000f;
      float num3 = 1000f;
      HistoryCard historyCard = (HistoryCard) null;
      using (List<HistoryCard>.Enumerator enumerator = this.m_historyTiles.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          HistoryCard current = enumerator.Current;
          if (current.HasBeenShown())
          {
            Collider tileCollider = current.GetTileCollider();
            if (!((Object) tileCollider == (Object) null))
            {
              float num4 = tileCollider.bounds.center.z - tileCollider.bounds.extents.z;
              float num5 = tileCollider.bounds.center.z + tileCollider.bounds.extents.z;
              if ((double) num4 < (double) num1)
                num1 = num4;
              if ((double) num5 > (double) num2)
                num2 = num5;
              float num6 = Mathf.Abs(zPosition - num4);
              if ((double) num6 < (double) num3)
              {
                num3 = num6;
                historyCard = current;
              }
              float num7 = Mathf.Abs(zPosition - num5);
              if ((double) num7 < (double) num3)
              {
                num3 = num7;
                historyCard = current;
              }
            }
          }
        }
      }
      if ((double) zPosition < (double) num1 || (double) zPosition > (double) num2)
        this.CheckForMouseOff();
      else if ((Object) historyCard == (Object) null)
      {
        this.CheckForMouseOff();
      }
      else
      {
        this.m_SoundDucker.StartDucking();
        if ((Object) historyCard == (Object) this.m_currentlyMousedOverTile)
          return;
        if ((Object) this.m_currentlyMousedOverTile != (Object) null)
          this.m_currentlyMousedOverTile.NotifyMousedOut();
        else
          this.FadeVignetteIn();
        this.m_currentlyMousedOverTile = historyCard;
        historyCard.NotifyMousedOver();
      }
    }
  }

  public void NotifyOfMouseOff()
  {
    this.CheckForMouseOff();
  }

  public void UpdateLayout()
  {
    if (this.UserIsMousedOverAHistoryTile())
      return;
    float num1 = 0.0f;
    Vector3 topTilePosition = this.GetTopTilePosition();
    for (int index = this.m_historyTiles.Count - 1; index >= 0; --index)
    {
      int num2 = 0;
      if (this.m_historyTiles[index].IsHalfSize())
        num2 = 1;
      Collider tileCollider = this.m_historyTiles[index].GetTileCollider();
      float num3 = 0.0f;
      if ((Object) tileCollider != (Object) null)
        num3 = tileCollider.bounds.size.z / 2f;
      Vector3 position = new Vector3(topTilePosition.x, topTilePosition.y, (float) ((double) topTilePosition.z - (double) num1 + (double) num2 * (double) num3));
      this.m_historyTiles[index].MarkAsShown();
      iTween.MoveTo(this.m_historyTiles[index].gameObject, position, 1f);
      if ((Object) tileCollider != (Object) null)
        num1 += tileCollider.bounds.size.z + 0.15f;
    }
    this.DestroyHistoryTilesThatFallOffTheEnd();
  }

  public int GetNumHistoryTiles()
  {
    return this.m_historyTiles.Count;
  }

  public int GetIndexForTile(HistoryCard tile)
  {
    for (int index = 0; index < this.m_historyTiles.Count; ++index)
    {
      if ((Object) this.m_historyTiles[index] == (Object) tile)
        return index;
    }
    UnityEngine.Debug.LogWarning((object) "HistoryManager.GetIndexForTile() - that Tile doesn't exist!");
    return -1;
  }

  private void LoadNextHistoryEntry()
  {
    if (this.m_queuedEntries.Count == 0 || !this.m_queuedEntries[0].m_complete)
      return;
    this.StartCoroutine(this.LoadNextHistoryEntryWhenLoaded());
  }

  [DebuggerHidden]
  private IEnumerator LoadNextHistoryEntryWhenLoaded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HistoryManager.\u003CLoadNextHistoryEntryWhenLoaded\u003Ec__IteratorB5() { \u003C\u003Ef__this = this };
  }

  private void CreateTransformTile(HistoryInfo sourceInfo)
  {
    if (sourceInfo.m_infoType == HistoryInfoType.FATIGUE)
      return;
    Entity duplicatedEntity = sourceInfo.GetDuplicatedEntity();
    Entity originalEntity = sourceInfo.GetOriginalEntity();
    if (duplicatedEntity == null || originalEntity == null)
      return;
    int tag = duplicatedEntity.GetTag(GAME_TAG.TRANSFORMED_FROM_CARD);
    if (tag == 0 || string.IsNullOrEmpty(GameUtils.TranslateDbIdToCardId(tag)))
      return;
    Entity transformedEntity = this.CreatePreTransformedEntity(duplicatedEntity);
    HistoryInfo historyInfo1 = new HistoryInfo();
    historyInfo1.SetOriginalEntity(transformedEntity);
    HistoryInfo historyInfo2 = new HistoryInfo();
    historyInfo2.SetOriginalEntity(originalEntity);
    AssetLoader.Get().LoadActor("HistoryCard", new AssetLoader.GameObjectCallback(this.TileLoadedCallback), (object) new List<HistoryInfo>()
    {
      historyInfo1,
      historyInfo2
    }, false);
  }

  [DebuggerHidden]
  private IEnumerator WaitForCardLoadedAndDuplicateInfo(HistoryInfo info)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HistoryManager.\u003CWaitForCardLoadedAndDuplicateInfo\u003Ec__IteratorB6() { info = info, \u003C\u0024\u003Einfo = info };
  }

  private bool IsEntityTheLastCardTargeted(Entity entity)
  {
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    if (currentHistoryEntry.m_lastCardTargeted != null)
      return entity == currentHistoryEntry.m_lastCardTargeted.GetOriginalEntity();
    return false;
  }

  private bool IsEntityTheLastAttacker(Entity entity)
  {
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    if (currentHistoryEntry.m_lastAttacker != null)
      return entity == currentHistoryEntry.m_lastAttacker.GetOriginalEntity();
    return false;
  }

  private bool IsEntityTheLastCardPlayed(Entity entity)
  {
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    if (currentHistoryEntry.m_lastCardPlayed != null)
      return entity == currentHistoryEntry.m_lastCardPlayed.GetOriginalEntity();
    return false;
  }

  private bool IsEntityTheLastDefender(Entity entity)
  {
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    if (currentHistoryEntry.m_lastDefender != null)
      return entity == currentHistoryEntry.m_lastDefender.GetOriginalEntity();
    return false;
  }

  private bool IsEntityTheAffectedCard(Entity entity, int index)
  {
    HistoryManager.TileEntry currentHistoryEntry = this.GetCurrentHistoryEntry();
    if (currentHistoryEntry.m_affectedCards[index] != null)
      return entity == currentHistoryEntry.m_affectedCards[index].GetOriginalEntity();
    return false;
  }

  private HistoryManager.TileEntry GetCurrentHistoryEntry()
  {
    if (this.m_queuedEntries.Count == 0)
      return (HistoryManager.TileEntry) null;
    for (int index = this.m_queuedEntries.Count - 1; index >= 0; --index)
    {
      if (!this.m_queuedEntries[index].m_complete)
        return this.m_queuedEntries[index];
    }
    return (HistoryManager.TileEntry) null;
  }

  private bool IsDeadInLaterHistoryEntry(Entity entity)
  {
    bool flag = false;
    for (int index1 = this.m_queuedEntries.Count - 1; index1 >= 0; --index1)
    {
      HistoryManager.TileEntry queuedEntry = this.m_queuedEntries[index1];
      if (!queuedEntry.m_complete)
        return flag;
      for (int index2 = 0; index2 < queuedEntry.m_affectedCards.Count; ++index2)
      {
        HistoryInfo affectedCard = queuedEntry.m_affectedCards[index2];
        if (affectedCard.GetOriginalEntity() == entity && (affectedCard.HasDied() || affectedCard.GetSplatAmount() >= entity.GetCurrentVitality()))
          flag = true;
      }
    }
    return false;
  }

  private void TileLoadedCallback(string actorName, GameObject actorObject, object callbackData)
  {
    List<HistoryInfo> historyInfoList = (List<HistoryInfo>) callbackData;
    HistoryInfo historyInfo = historyInfoList[0];
    historyInfoList.RemoveAt(0);
    HistoryTileInitInfo info = new HistoryTileInitInfo();
    info.m_type = historyInfo.m_infoType;
    info.m_childInfos = historyInfoList;
    if (info.m_type == HistoryInfoType.FATIGUE)
    {
      info.m_fatigueTexture = this.m_FatigueTexture;
    }
    else
    {
      Entity duplicatedEntity = historyInfo.GetDuplicatedEntity();
      CardDef cardDef = duplicatedEntity.GetCard().GetCardDef();
      info.m_entity = duplicatedEntity;
      info.m_portraitTexture = this.DeterminePortraitTextureForTiles(duplicatedEntity, cardDef);
      info.m_portraitGoldenMaterial = cardDef.GetPremiumPortraitMaterial();
      info.m_fullTileMaterial = cardDef.GetHistoryTileFullPortrait();
      info.m_halfTileMaterial = cardDef.GetHistoryTileHalfPortrait();
      info.m_splatAmount = historyInfo.GetSplatAmount();
      info.m_dead = historyInfo.HasDied();
    }
    HistoryCard component = actorObject.GetComponent<HistoryCard>();
    this.m_historyTiles.Add(component);
    component.LoadTile(info);
    this.SetAsideTileAndTryToUpdate(component);
    this.LoadNextHistoryEntry();
  }

  private Texture DeterminePortraitTextureForTiles(Entity entity, CardDef cardDef)
  {
    return !entity.IsSecret() || !entity.IsHidden() || !entity.IsControlledByConcealedPlayer() ? (entity.GetController() == null || entity.GetController().IsFriendlySide() || !entity.IsObfuscated() ? cardDef.GetPortraitTexture() : this.m_paladinSecretTexture) : (entity.GetClass() != TAG_CLASS.PALADIN ? (entity.GetClass() != TAG_CLASS.HUNTER ? (!entity.IsDarkWandererSecret() ? this.m_mageSecretTexture : this.m_wandererSecretTexture) : this.m_hunterSecretTexture) : this.m_paladinSecretTexture);
  }

  private void CheckForMouseOff()
  {
    if ((Object) this.m_currentlyMousedOverTile == (Object) null)
      return;
    this.m_currentlyMousedOverTile.NotifyMousedOut();
    this.m_currentlyMousedOverTile = (HistoryCard) null;
    this.m_SoundDucker.StopDucking();
    this.FadeVignetteOut();
  }

  private void DestroyHistoryTilesThatFallOffTheEnd()
  {
    if (this.m_historyTiles.Count == 0)
      return;
    float num1 = 0.0f;
    float z = this.GetComponent<Collider>().bounds.size.z;
    for (int index = 0; index < this.m_historyTiles.Count; ++index)
      num1 += this.m_historyTiles[index].GetTileSize();
    float num2 = num1 + 0.15f * (float) (this.m_historyTiles.Count - 1);
    while ((double) num2 > (double) z)
    {
      num2 = num2 - this.m_historyTiles[0].GetTileSize() - 0.15f;
      Object.Destroy((Object) this.m_historyTiles[0].gameObject);
      this.m_historyTiles.RemoveAt(0);
    }
  }

  private void SetAsideTileAndTryToUpdate(HistoryCard tile)
  {
    Vector3 topTilePosition = this.GetTopTilePosition();
    tile.transform.position = new Vector3(topTilePosition.x - 20f, topTilePosition.y, topTilePosition.z);
    this.UpdateLayout();
  }

  private Vector3 GetTopTilePosition()
  {
    return new Vector3(this.transform.position.x, this.transform.position.y - 0.15f, this.transform.position.z);
  }

  private bool UserIsMousedOverAHistoryTile()
  {
    RaycastHit hitInfo;
    if (UniversalInputManager.Get().GetInputHitInfo((LayerMask) GameLayer.Default.LayerBit(), out hitInfo) && (Object) hitInfo.transform.GetComponentInChildren<HistoryManager>() == (Object) null && (Object) hitInfo.transform.GetComponentInChildren<HistoryCard>() == (Object) null)
      return false;
    float z = hitInfo.point.z;
    float num1 = 1000f;
    float num2 = -1000f;
    using (List<HistoryCard>.Enumerator enumerator = this.m_historyTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HistoryCard current = enumerator.Current;
        if (current.HasBeenShown())
        {
          Collider tileCollider = current.GetTileCollider();
          if (!((Object) tileCollider == (Object) null))
          {
            float num3 = tileCollider.bounds.center.z - tileCollider.bounds.extents.z;
            float num4 = tileCollider.bounds.center.z + tileCollider.bounds.extents.z;
            if ((double) num3 < (double) num1)
              num1 = num3;
            if ((double) num4 > (double) num2)
              num2 = num4;
          }
        }
      }
    }
    return (double) z >= (double) num1 && (double) z <= (double) num2;
  }

  private void FadeVignetteIn()
  {
    using (List<HistoryCard>.Enumerator enumerator = this.m_historyTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HistoryCard current = enumerator.Current;
        if (!((Object) current.m_tileActor == (Object) null))
          SceneUtils.SetLayer(current.m_tileActor.gameObject, GameLayer.IgnoreFullScreenEffects);
      }
    }
    SceneUtils.SetLayer(this.gameObject, GameLayer.IgnoreFullScreenEffects);
    FullScreenEffects component = Camera.main.GetComponent<FullScreenEffects>();
    component.VignettingEnable = true;
    component.DesaturationEnabled = true;
    this.AnimateVignetteIn();
  }

  private void FadeVignetteOut()
  {
    using (List<HistoryCard>.Enumerator enumerator = this.m_historyTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HistoryCard current = enumerator.Current;
        if (!((Object) current.m_tileActor == (Object) null))
          SceneUtils.SetLayer(current.GetTileCollider().gameObject, GameLayer.Default);
      }
    }
    SceneUtils.SetLayer(this.gameObject, GameLayer.CardRaycast);
    this.AnimateVignetteOut();
  }

  private void AnimateVignetteIn()
  {
    FullScreenEffects component = Camera.main.GetComponent<FullScreenEffects>();
    this.m_animatingVignette = component.VignettingEnable;
    if (this.m_animatingVignette)
    {
      Hashtable args = iTween.Hash((object) "from", (object) component.VignettingIntensity, (object) "to", (object) 0.6f, (object) "time", (object) 0.4f, (object) "easetype", (object) iTween.EaseType.easeInOutQuad, (object) "onupdate", (object) "OnUpdateVignetteVal", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "historyVig", (object) "oncomplete", (object) "OnVignetteInFinished", (object) "oncompletetarget", (object) this.gameObject);
      iTween.StopByName(Camera.main.gameObject, "historyVig");
      iTween.ValueTo(Camera.main.gameObject, args);
    }
    this.m_animatingDesat = component.DesaturationEnabled;
    if (!this.m_animatingDesat)
      return;
    Hashtable args1 = iTween.Hash((object) "from", (object) component.Desaturation, (object) "to", (object) 1f, (object) "time", (object) 0.4f, (object) "easetype", (object) iTween.EaseType.easeInOutQuad, (object) "onupdate", (object) "OnUpdateDesatVal", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "historyDesat", (object) "oncomplete", (object) "OnDesatInFinished", (object) "oncompletetarget", (object) this.gameObject);
    iTween.StopByName(Camera.main.gameObject, "historyDesat");
    iTween.ValueTo(Camera.main.gameObject, args1);
  }

  private void AnimateVignetteOut()
  {
    FullScreenEffects component = Camera.main.GetComponent<FullScreenEffects>();
    this.m_animatingVignette = component.VignettingEnable;
    if (this.m_animatingVignette)
    {
      Hashtable args = iTween.Hash((object) "from", (object) component.VignettingIntensity, (object) "to", (object) 0.0f, (object) "time", (object) 0.4f, (object) "easetype", (object) iTween.EaseType.easeInOutQuad, (object) "onupdate", (object) "OnUpdateVignetteVal", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "historyVig", (object) "oncomplete", (object) "OnVignetteOutFinished", (object) "oncompletetarget", (object) this.gameObject);
      iTween.StopByName(Camera.main.gameObject, "historyVig");
      iTween.ValueTo(Camera.main.gameObject, args);
    }
    this.m_animatingDesat = component.DesaturationEnabled;
    if (!this.m_animatingDesat)
      return;
    Hashtable args1 = iTween.Hash((object) "from", (object) component.Desaturation, (object) "to", (object) 0.0f, (object) "time", (object) 0.4f, (object) "easetype", (object) iTween.EaseType.easeInOutQuad, (object) "onupdate", (object) "OnUpdateDesatVal", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "historyDesat", (object) "oncomplete", (object) "OnDesatOutFinished", (object) "oncompletetarget", (object) this.gameObject);
    iTween.StopByName(Camera.main.gameObject, "historyDesat");
    iTween.ValueTo(Camera.main.gameObject, args1);
  }

  private void OnUpdateVignetteVal(float val)
  {
    Camera.main.GetComponent<FullScreenEffects>().VignettingIntensity = val;
  }

  private void OnUpdateDesatVal(float val)
  {
    Camera.main.GetComponent<FullScreenEffects>().Desaturation = val;
  }

  private void OnVignetteInFinished()
  {
    this.m_animatingVignette = false;
  }

  private void OnDesatInFinished()
  {
    this.m_animatingDesat = false;
  }

  private void OnVignetteOutFinished()
  {
    this.m_animatingVignette = false;
    Camera.main.GetComponent<FullScreenEffects>().VignettingEnable = false;
    this.OnFullScreenEffectOutFinished();
  }

  private void OnDesatOutFinished()
  {
    this.m_animatingDesat = false;
    Camera.main.GetComponent<FullScreenEffects>().DesaturationEnabled = false;
    this.OnFullScreenEffectOutFinished();
  }

  private void OnFullScreenEffectOutFinished()
  {
    if (this.m_animatingDesat || this.m_animatingVignette)
      return;
    Camera.main.GetComponent<FullScreenEffects>().Disable();
    using (List<HistoryCard>.Enumerator enumerator = this.m_historyTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HistoryCard current = enumerator.Current;
        if (!((Object) current.m_tileActor == (Object) null))
          SceneUtils.SetLayer(current.m_tileActor.gameObject, GameLayer.Default);
      }
    }
  }

  public bool IsShowingBigCard()
  {
    return this.m_showingBigCard;
  }

  public bool HasBigCard()
  {
    return (Object) this.m_currentBigCard != (Object) null;
  }

  public HistoryCard GetCurrentBigCard()
  {
    return this.m_currentBigCard;
  }

  public Entity GetPendingBigCardEntity()
  {
    if (this.m_pendingBigCardEntry == null)
      return (Entity) null;
    return this.m_pendingBigCardEntry.m_info.GetOriginalEntity();
  }

  public void CreatePlayedBigCard(Entity entity, HistoryManager.BigCardFinishedCallback callback, bool fromMetaData, bool countered)
  {
    if (!GameState.Get().GetGameEntity().ShouldShowBigCard())
    {
      callback();
    }
    else
    {
      this.m_showingBigCard = true;
      this.StopCoroutine("WaitForCardLoadedAndCreateBigCard");
      HistoryManager.BigCardEntry bigCardEntry = new HistoryManager.BigCardEntry();
      bigCardEntry.m_info = new HistoryInfo();
      bigCardEntry.m_info.SetOriginalEntity(entity);
      bigCardEntry.m_info.m_infoType = !entity.IsWeapon() ? HistoryInfoType.CARD_PLAYED : HistoryInfoType.WEAPON_PLAYED;
      bigCardEntry.m_finishedCallback = callback;
      bigCardEntry.m_fromMetaData = fromMetaData;
      bigCardEntry.m_countered = countered;
      this.StartCoroutine("WaitForCardLoadedAndCreateBigCard", (object) bigCardEntry);
    }
  }

  public void CreateTriggeredBigCard(Entity entity, HistoryManager.BigCardFinishedCallback callback, bool fromMetaData, bool isSecret)
  {
    if (!GameState.Get().GetGameEntity().ShouldShowBigCard())
    {
      callback();
    }
    else
    {
      this.m_showingBigCard = true;
      this.StopCoroutine("WaitForCardLoadedAndCreateBigCard");
      HistoryManager.BigCardEntry bigCardEntry = new HistoryManager.BigCardEntry();
      bigCardEntry.m_info = new HistoryInfo();
      bigCardEntry.m_info.SetOriginalEntity(entity);
      bigCardEntry.m_info.m_infoType = HistoryInfoType.TRIGGER;
      bigCardEntry.m_fromMetaData = fromMetaData;
      bigCardEntry.m_finishedCallback = callback;
      bigCardEntry.m_waitForSecretSpell = isSecret;
      this.StartCoroutine("WaitForCardLoadedAndCreateBigCard", (object) bigCardEntry);
    }
  }

  public void NotifyOfSecretSpellFinished()
  {
    this.m_bigCardWaitingForSecret = false;
  }

  public void HandleClickOnBigCard(HistoryCard card)
  {
    if ((bool) ((Object) this.m_currentBigCard) && (Object) this.m_currentBigCard != (Object) card)
      return;
    this.OnCurrentBigCardClicked();
  }

  [DebuggerHidden]
  private IEnumerator WaitForBoardLoadedAndSetPaths()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HistoryManager.\u003CWaitForBoardLoadedAndSetPaths\u003Ec__IteratorB7() { \u003C\u003Ef__this = this };
  }

  private Vector3 GetBigCardPosition()
  {
    return Board.Get().FindBone("BigCardPosition").position;
  }

  [DebuggerHidden]
  private IEnumerator WaitForCardLoadedAndCreateBigCard(HistoryManager.BigCardEntry bigCardEntry)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HistoryManager.\u003CWaitForCardLoadedAndCreateBigCard\u003Ec__IteratorB8() { bigCardEntry = bigCardEntry, \u003C\u0024\u003EbigCardEntry = bigCardEntry, \u003C\u003Ef__this = this };
  }

  private void BigCardLoadedCallback(string actorName, GameObject actorObject, object callbackData)
  {
    HistoryManager.BigCardEntry bigCardEntry = (HistoryManager.BigCardEntry) callbackData;
    Entity entity1 = bigCardEntry.m_info.GetDuplicatedEntity();
    Card card = entity1.GetCard();
    CardDef cardDef = card.GetCardDef();
    if (entity1.GetCardType() == TAG_CARDTYPE.SPELL || entity1.GetCardType() == TAG_CARDTYPE.HERO_POWER)
    {
      actorObject.transform.position = card.transform.position;
      actorObject.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
    }
    else
      actorObject.transform.position = this.GetBigCardPosition();
    Entity transformedEntity = this.CreatePreTransformedEntity(entity1);
    Entity entity2 = (Entity) null;
    if (transformedEntity != null)
    {
      entity2 = entity1;
      entity1 = transformedEntity;
      cardDef = entity1.GetCard().GetCardDef();
    }
    HistoryBigCardInitInfo info = new HistoryBigCardInitInfo();
    info.m_entity = entity1;
    info.m_portraitTexture = cardDef.GetPortraitTexture();
    info.m_portraitGoldenMaterial = cardDef.GetPremiumPortraitMaterial();
    info.m_finishedCallback = bigCardEntry.m_finishedCallback;
    info.m_countered = bigCardEntry.m_countered;
    info.m_waitForSecretSpell = bigCardEntry.m_waitForSecretSpell;
    info.m_fromMetaData = bigCardEntry.m_fromMetaData;
    info.m_postTransformedEntity = entity2;
    HistoryCard component = actorObject.GetComponent<HistoryCard>();
    component.LoadBigCard(info);
    if ((bool) ((Object) this.m_currentBigCard))
      this.InterruptCurrentBigCard();
    this.m_currentBigCard = component;
    this.StartCoroutine("WaitThenShowBigCard");
  }

  [DebuggerHidden]
  private IEnumerator WaitThenShowBigCard()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HistoryManager.\u003CWaitThenShowBigCard\u003Ec__IteratorB9() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitThenDestroyBigCard()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HistoryManager.\u003CWaitThenDestroyBigCard\u003Ec__IteratorBA() { \u003C\u003Ef__this = this };
  }

  private void DestroyBigCard()
  {
    if ((Object) this.m_currentBigCard == (Object) null)
      return;
    if ((Object) this.m_currentBigCard.m_mainCardActor == (Object) null)
      this.RunFinishedCallbackAndDestroyBigCard();
    else if (this.m_currentBigCard.WasBigCardCountered())
      this.PlayBigCardCounteredEffects();
    else if (this.m_currentBigCard.HasBigCardPostTransformedEntity())
      this.PlayBigCardTransformEffects();
    else
      this.RunFinishedCallbackAndDestroyBigCard();
  }

  private void RunFinishedCallbackAndDestroyBigCard()
  {
    if ((Object) this.m_currentBigCard == (Object) null)
      return;
    this.m_currentBigCard.RunBigCardFinishedCallback();
    this.m_showingBigCard = false;
    Object.Destroy((Object) this.m_currentBigCard.gameObject);
  }

  private void PlayBigCardCounteredEffects()
  {
    Spell.StateFinishedCallback callback = (Spell.StateFinishedCallback) ((s, prevStateType, userData) =>
    {
      if (s.GetActiveState() != SpellStateType.NONE)
        return;
      HistoryCard historyCard = (HistoryCard) userData;
      this.m_showingBigCard = false;
      Object.Destroy((Object) historyCard.gameObject);
    });
    Spell spell = this.m_currentBigCard.m_mainCardActor.GetSpell(SpellType.DEATH);
    if ((Object) spell == (Object) null)
    {
      this.RunFinishedCallbackAndDestroyBigCard();
    }
    else
    {
      spell.AddStateFinishedCallback(callback, (object) this.m_currentBigCard);
      this.m_currentBigCard.RunBigCardFinishedCallback();
      this.m_currentBigCard = (HistoryCard) null;
      spell.Activate();
    }
  }

  private void PlayBigCardTransformEffects()
  {
    this.StartCoroutine(this.PlayBigCardTransformEffectsWithTiming());
  }

  [DebuggerHidden]
  private IEnumerator PlayBigCardTransformEffectsWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HistoryManager.\u003CPlayBigCardTransformEffectsWithTiming\u003Ec__IteratorBB() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator PlayBigCardTransformSpell()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HistoryManager.\u003CPlayBigCardTransformSpell\u003Ec__IteratorBC() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitForBigCardPostTransform()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HistoryManager.\u003CWaitForBigCardPostTransform\u003Ec__IteratorBD() { \u003C\u003Ef__this = this };
  }

  private void OnCurrentBigCardClicked()
  {
    if (this.m_currentBigCard.HasBigCardPostTransformedEntity())
      this.ForceNextBigCardTransformState();
    else
      this.InterruptCurrentBigCard();
  }

  private void ForceNextBigCardTransformState()
  {
    switch (this.m_bigCardTransformState)
    {
      case HistoryManager.BigCardTransformState.PRE_TRANSFORM:
        this.m_bigCardTransformState = HistoryManager.BigCardTransformState.TRANSFORM;
        this.StopWaitingThenDestroyBigCard();
        break;
      case HistoryManager.BigCardTransformState.TRANSFORM:
        if (!(bool) ((Object) this.m_bigCardTransformSpell))
          break;
        Object.Destroy((Object) this.m_bigCardTransformSpell.gameObject);
        break;
      case HistoryManager.BigCardTransformState.POST_TRANSFORM:
        this.InterruptCurrentBigCard();
        break;
    }
  }

  private void StopWaitingThenDestroyBigCard()
  {
    this.StopCoroutine("WaitThenDestroyBigCard");
    this.DestroyBigCard();
  }

  private void InterruptCurrentBigCard()
  {
    this.StopCoroutine("WaitThenShowBigCard");
    if (this.m_currentBigCard.HasBigCardPostTransformedEntity())
      this.CutoffBigCardTransformEffects();
    else
      this.StopWaitingThenDestroyBigCard();
  }

  private void CutoffBigCardTransformEffects()
  {
    if ((bool) ((Object) this.m_bigCardTransformSpell))
      Object.Destroy((Object) this.m_bigCardTransformSpell.gameObject);
    this.StopCoroutine("PlayBigCardTransformEffectsWithTiming");
    this.m_bigCardTransformState = HistoryManager.BigCardTransformState.INVALID;
    this.RunFinishedCallbackAndDestroyBigCard();
  }

  private class BigCardEntry
  {
    public HistoryInfo m_info;
    public HistoryManager.BigCardFinishedCallback m_finishedCallback;
    public bool m_fromMetaData;
    public bool m_countered;
    public bool m_waitForSecretSpell;
  }

  private enum BigCardTransformState
  {
    INVALID,
    PRE_TRANSFORM,
    TRANSFORM,
    POST_TRANSFORM,
  }

  private class TileEntry
  {
    public List<HistoryInfo> m_affectedCards = new List<HistoryInfo>();
    public HistoryInfo m_lastAttacker;
    public HistoryInfo m_lastDefender;
    public HistoryInfo m_lastCardPlayed;
    public HistoryInfo m_lastCardTriggered;
    public HistoryInfo m_lastCardTargeted;
    public HistoryInfo m_fatigueInfo;
    public bool m_usingMetaDataOverride;
    public bool m_complete;

    public void SetAttacker(Entity attacker)
    {
      this.m_lastAttacker = new HistoryInfo();
      this.m_lastAttacker.m_infoType = HistoryInfoType.ATTACK;
      this.m_lastAttacker.SetOriginalEntity(attacker);
    }

    public void SetDefender(Entity defender)
    {
      this.m_lastDefender = new HistoryInfo();
      this.m_lastDefender.SetOriginalEntity(defender);
    }

    public void SetCardPlayed(Entity entity)
    {
      this.m_lastCardPlayed = new HistoryInfo();
      this.m_lastCardPlayed.m_infoType = !entity.IsWeapon() ? HistoryInfoType.CARD_PLAYED : HistoryInfoType.WEAPON_PLAYED;
      this.m_lastCardPlayed.SetOriginalEntity(entity);
    }

    public void SetCardTargeted(Entity entity)
    {
      if (entity == null)
        return;
      this.m_lastCardTargeted = new HistoryInfo();
      this.m_lastCardTargeted.SetOriginalEntity(entity);
    }

    public void SetCardTriggered(Entity entity)
    {
      if (entity.IsGame() || entity.IsPlayer() || entity.IsHero())
        return;
      this.m_lastCardTriggered = new HistoryInfo();
      this.m_lastCardTriggered.m_infoType = HistoryInfoType.TRIGGER;
      this.m_lastCardTriggered.SetOriginalEntity(entity);
    }

    public void SetFatigue()
    {
      this.m_fatigueInfo = new HistoryInfo();
      this.m_fatigueInfo.m_infoType = HistoryInfoType.FATIGUE;
    }

    public bool CanDuplicateAllEntities(bool duplicateHiddenNonSecrets)
    {
      HistoryInfo sourceInfo = this.GetSourceInfo();
      if (this.ShouldDuplicateEntity(sourceInfo) && !sourceInfo.CanDuplicateEntity(duplicateHiddenNonSecrets))
        return false;
      HistoryInfo targetInfo = this.GetTargetInfo();
      if (this.ShouldDuplicateEntity(targetInfo) && !targetInfo.CanDuplicateEntity(duplicateHiddenNonSecrets))
        return false;
      for (int index = 0; index < this.m_affectedCards.Count; ++index)
      {
        HistoryInfo affectedCard = this.m_affectedCards[index];
        if (this.ShouldDuplicateEntity(affectedCard) && !affectedCard.CanDuplicateEntity(duplicateHiddenNonSecrets))
          return false;
      }
      return true;
    }

    public void DuplicateAllEntities(bool duplicateHiddenNonSecrets)
    {
      HistoryInfo sourceInfo = this.GetSourceInfo();
      if (this.ShouldDuplicateEntity(sourceInfo))
        sourceInfo.DuplicateEntity(duplicateHiddenNonSecrets);
      HistoryInfo targetInfo = this.GetTargetInfo();
      if (this.ShouldDuplicateEntity(targetInfo))
        targetInfo.DuplicateEntity(duplicateHiddenNonSecrets);
      for (int index = 0; index < this.m_affectedCards.Count; ++index)
      {
        HistoryInfo affectedCard = this.m_affectedCards[index];
        if (this.ShouldDuplicateEntity(affectedCard))
          affectedCard.DuplicateEntity(duplicateHiddenNonSecrets);
      }
    }

    public bool ShouldDuplicateEntity(HistoryInfo info)
    {
      return info != null && info != this.m_fatigueInfo;
    }

    public HistoryInfo GetSourceInfo()
    {
      if (this.m_lastCardPlayed != null)
        return this.m_lastCardPlayed;
      if (this.m_lastAttacker != null)
        return this.m_lastAttacker;
      if (this.m_lastCardTriggered != null)
        return this.m_lastCardTriggered;
      if (this.m_fatigueInfo != null)
        return this.m_fatigueInfo;
      UnityEngine.Debug.LogError((object) "HistoryEntry.GetSourceInfo() - no source info");
      return (HistoryInfo) null;
    }

    public HistoryInfo GetTargetInfo()
    {
      if (this.m_lastCardPlayed != null && this.m_lastCardTargeted != null)
        return this.m_lastCardTargeted;
      if (this.m_lastAttacker != null && this.m_lastDefender != null)
        return this.m_lastDefender;
      return (HistoryInfo) null;
    }
  }

  public delegate void BigCardFinishedCallback();
}
