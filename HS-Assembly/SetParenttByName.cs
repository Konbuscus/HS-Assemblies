﻿// Decompiled with JetBrains decompiler
// Type: SetParenttByName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class SetParenttByName : MonoBehaviour
{
  [CustomEditField(T = EditType.SCENE_OBJECT)]
  public string m_ParentName;

  private void Start()
  {
    if (string.IsNullOrEmpty(this.m_ParentName))
      return;
    GameObject gameObject = this.FindGameObject(this.m_ParentName);
    if ((Object) gameObject == (Object) null)
      Log.Kyle.Print("SetParenttByName failed to locate parent object: {0}", (object) this.m_ParentName);
    else
      this.transform.parent = gameObject.transform;
  }

  private GameObject FindGameObject(string gameObjName)
  {
    if ((int) gameObjName[0] != 47)
      return GameObject.Find(gameObjName);
    string[] strArray = gameObjName.Split('/');
    return GameObject.Find(strArray[strArray.Length - 1]);
  }
}
