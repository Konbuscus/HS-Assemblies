﻿// Decompiled with JetBrains decompiler
// Type: SpectatorManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using PegasusGame;
using PegasusShared;
using SpectatorProto;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class SpectatorManager
{
  private static readonly PlatformDependentValue<float> WAITING_FOR_NEXT_GAME_AUTO_LEAVE_SECONDS = new PlatformDependentValue<float>(PlatformCategory.OS) { iOS = 300f, Android = 300f, PC = -1f, Mac = -1f };
  private static readonly PlatformDependentValue<bool> DISABLE_MENU_BUTTON_WHILE_WAITING = new PlatformDependentValue<bool>(PlatformCategory.OS) { iOS = true, Android = true, PC = false, Mac = false };
  private static SpectatorManager s_instance = (SpectatorManager) null;
  private Map<PartyId, BnetGameAccountId> m_knownPartyCreatorIds = new Map<PartyId, BnetGameAccountId>();
  private HashSet<BnetGameAccountId> m_gameServerKnownSpectators = new HashSet<BnetGameAccountId>();
  private Map<BnetGameAccountId, SpectatorManager.ReceivedInvite> m_receivedSpectateMeInvites = new Map<BnetGameAccountId, SpectatorManager.ReceivedInvite>();
  private Map<BnetGameAccountId, float> m_sentSpectateMeInvites = new Map<BnetGameAccountId, float>();
  public const int MAX_SPECTATORS_PER_SIDE = 10;
  private const float RECEIVED_INVITE_TIMEOUT_SECONDS = 300f;
  private const float SENT_INVITE_TIMEOUT_SECONDS = 30f;
  private const bool SPECTATOR_PARTY_INVITES_USE_RESERVATIONS = false;
  private const float REQUEST_INVITE_TIMEOUT_SECONDS = 5f;
  private const string ALERTPOPUPID_WAITINGFORNEXTGAME = "SPECTATOR_WAITING_FOR_NEXT_GAME";
  private const float ENDGAMESCREEN_AUTO_CLOSE_SECONDS = 5f;
  private const float KICKED_FROM_SPECTATING_BLACKOUT_DURATION_SECONDS = 10f;
  private bool m_initialized;
  private BnetGameAccountId m_spectateeFriendlySide;
  private BnetGameAccountId m_spectateeOpposingSide;
  private PartyId m_spectatorPartyIdMain;
  private PartyId m_spectatorPartyIdOpposingSide;
  private SpectatorManager.IntendedSpectateeParty m_requestedInvite;
  private AlertPopup m_waitingForNextGameDialog;
  private HashSet<PartyId> m_leavePartyIdsRequested;
  private SpectatorManager.PendingSpectatePlayer m_pendingSpectatePlayerAfterLeave;
  private HashSet<BnetGameAccountId> m_userInitiatedOutgoingInvites;
  private HashSet<BnetGameAccountId> m_kickedPlayers;
  private Map<BnetGameAccountId, float> m_kickedFromSpectatingList;
  private int? m_expectedDisconnectReason;
  private bool m_isExpectingArriveInGameplayAsSpectator;
  private bool m_isShowingRemovedAsSpectatorPopup;

  private static bool IsGameOver
  {
    get
    {
      return GameState.Get() != null && GameState.Get().IsGameOverNowOrPending();
    }
  }

  public event SpectatorManager.InviteReceivedHandler OnInviteReceived;

  public event SpectatorManager.InviteSentHandler OnInviteSent;

  public event SpectatorManager.SpectatorToMyGameHandler OnSpectatorToMyGame;

  public event SpectatorManager.SpectatorModeChangedHandler OnSpectatorModeChanged;

  private SpectatorManager()
  {
  }

  public static SpectatorManager Get()
  {
    if (SpectatorManager.s_instance == null)
      SpectatorManager.CreateInstance();
    return SpectatorManager.s_instance;
  }

  public static JoinInfo GetSpectatorJoinInfo(BnetGameAccount gameAccount)
  {
    byte[] gameFieldBytes = gameAccount.GetGameFieldBytes(21U);
    if (gameFieldBytes != null && gameFieldBytes.Length > 0)
      return ProtobufUtil.ParseFrom<JoinInfo>(gameFieldBytes, 0, -1);
    return (JoinInfo) null;
  }

  public static bool IsSpectatorSlotAvailable(JoinInfo info)
  {
    return info != null && (info.HasPartyId || info.HasServerIpAddress && info.HasSecretKey && !string.IsNullOrEmpty(info.SecretKey)) && ((!info.HasIsJoinable || info.IsJoinable) && (!info.HasMaxNumSpectators || !info.HasCurrentNumSpectators || info.CurrentNumSpectators < info.MaxNumSpectators));
  }

  public static bool IsSpectatorSlotAvailable(BnetGameAccount gameAccount)
  {
    return SpectatorManager.IsSpectatorSlotAvailable(SpectatorManager.GetSpectatorJoinInfo(gameAccount));
  }

  public void InitializeConnectedToBnet()
  {
    if (this.m_initialized)
      return;
    this.m_initialized = true;
    foreach (PartyInfo joinedParty in BnetParty.GetJoinedParties())
      this.BnetParty_OnJoined(OnlineEventType.ADDED, joinedParty, new LeaveReason?());
    foreach (PartyInvite receivedInvite in BnetParty.GetReceivedInvites())
      this.BnetParty_OnReceivedInvite(OnlineEventType.ADDED, new PartyInfo(receivedInvite.PartyId, receivedInvite.PartyType), receivedInvite.InviteId, new InviteRemoveReason?());
  }

  public bool CanSpectate(BnetPlayer player)
  {
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    if (player == myPlayer)
      return false;
    BnetGameAccountId hearthstoneGameAccountId = player.GetHearthstoneGameAccountId();
    if (this.IsSpectatingPlayer(hearthstoneGameAccountId) || (BnetEntityId) this.m_spectateeOpposingSide != (BnetEntityId) null || this.HasPreviouslyKickedMeFromGame(hearthstoneGameAccountId) && !this.HasInvitedMeToSpectate(hearthstoneGameAccountId) || (GameMgr.Get().IsFindingGame() || GameMgr.Get().IsNextSpectator() || !SpectatorManager.IsSpectatorSlotAvailable(player.GetBestGameAccount()) && !this.HasInvitedMeToSpectate(hearthstoneGameAccountId)))
      return false;
    if (GameMgr.Get().IsSpectator())
    {
      if (!this.IsPlayerInGame(hearthstoneGameAccountId))
        return false;
    }
    else if (SceneMgr.Get().IsInGame())
      return false;
    if (!GameUtils.AreAllTutorialsComplete())
      return false;
    if (ApplicationMgr.IsPublic())
    {
      BnetGameAccount hearthstoneGameAccount1 = player.GetHearthstoneGameAccount();
      BnetGameAccount hearthstoneGameAccount2 = myPlayer.GetHearthstoneGameAccount();
      if (string.Compare(hearthstoneGameAccount1.GetClientVersion(), hearthstoneGameAccount2.GetClientVersion()) != 0 || string.Compare(hearthstoneGameAccount1.GetClientEnv(), hearthstoneGameAccount2.GetClientEnv()) != 0)
        return false;
    }
    return SceneMgr.Get().GetMode() != SceneMgr.Mode.LOGIN && !ReturningPlayerMgr.Get().IsInReturningPlayerModeAndOnRails;
  }

  public bool IsInSpectatorMode()
  {
    return GameMgr.Get() != null && GameMgr.Get().IsSpectator() || !(this.m_spectatorPartyIdMain == (PartyId) null) && BnetParty.IsInParty(this.m_spectatorPartyIdMain) && (this.m_initialized && !((BnetEntityId) this.GetPartyCreator(this.m_spectatorPartyIdMain) == (BnetEntityId) null)) && !this.ShouldBePartyLeader(this.m_spectatorPartyIdMain);
  }

  public bool ShouldBeSpectatingInGame()
  {
    return !(this.m_spectatorPartyIdMain == (PartyId) null) && BnetParty.GetPartyAttributeBlob(this.m_spectatorPartyIdMain, "WTCG.Party.ServerInfo") != null;
  }

  public bool IsSpectatingPlayer(BnetGameAccountId gameAccountId)
  {
    return (BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null && (BnetEntityId) this.m_spectateeFriendlySide == (BnetEntityId) gameAccountId || (BnetEntityId) this.m_spectateeOpposingSide != (BnetEntityId) null && (BnetEntityId) this.m_spectateeOpposingSide == (BnetEntityId) gameAccountId;
  }

  public bool IsSpectatingMe(BnetGameAccountId gameAccountId)
  {
    return !this.IsInSpectatorMode() && (this.m_gameServerKnownSpectators.Contains(gameAccountId) || (BnetEntityId) gameAccountId != (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId() && BnetParty.IsMember(this.m_spectatorPartyIdMain, gameAccountId));
  }

  public int GetCountSpectatingMe()
  {
    if (this.m_spectatorPartyIdMain != (PartyId) null && !this.ShouldBePartyLeader(this.m_spectatorPartyIdMain))
      return 0;
    return Mathf.Max(BnetParty.CountMembers(this.m_spectatorPartyIdMain) - 1, this.m_gameServerKnownSpectators.Count);
  }

  public bool IsBeingSpectated()
  {
    return this.GetCountSpectatingMe() > 0;
  }

  public BnetGameAccountId[] GetSpectatorPartyMembers(bool friendlySide = true, bool includeSelf = false)
  {
    List<BnetGameAccountId> bnetGameAccountIdList = new List<BnetGameAccountId>((IEnumerable<BnetGameAccountId>) this.m_gameServerKnownSpectators);
    bgs.PartyMember[] members = BnetParty.GetMembers(!friendlySide ? this.m_spectatorPartyIdOpposingSide : this.m_spectatorPartyIdMain);
    BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
    foreach (bgs.PartyMember partyMember in members)
    {
      if ((includeSelf || (BnetEntityId) partyMember.GameAccountId != (BnetEntityId) myGameAccountId) && !bnetGameAccountIdList.Contains(partyMember.GameAccountId))
        bnetGameAccountIdList.Add(partyMember.GameAccountId);
    }
    return bnetGameAccountIdList.ToArray();
  }

  public bool IsInSpectatableGame()
  {
    return SceneMgr.Get().IsInGame() && !GameMgr.Get().IsSpectator() && !SpectatorManager.IsGameOver;
  }

  public bool CanAddSpectators()
  {
    if (GameMgr.Get() != null && GameMgr.Get().IsSpectator() || ((BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null || (BnetEntityId) this.m_spectateeOpposingSide != (BnetEntityId) null))
      return false;
    int countSpectatingMe = this.GetCountSpectatingMe();
    return (this.IsInSpectatableGame() || !(this.m_spectatorPartyIdMain == (PartyId) null) && countSpectatingMe > 0) && countSpectatingMe < 10;
  }

  public bool CanInviteToSpectateMyGame(BnetGameAccountId gameAccountId)
  {
    if (!this.CanAddSpectators())
      return false;
    BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
    if ((BnetEntityId) gameAccountId == (BnetEntityId) myGameAccountId || this.IsSpectatingMe(gameAccountId) || this.IsInvitedToSpectateMyGame(gameAccountId))
      return false;
    BnetGameAccount gameAccount = BnetPresenceMgr.Get().GetGameAccount(gameAccountId);
    if (gameAccount == (BnetGameAccount) null || !gameAccount.IsOnline() || !gameAccount.CanBeInvitedToGame() && !this.IsPlayerSpectatingMyGamesOpposingSide(gameAccountId))
      return false;
    if (ApplicationMgr.IsPublic())
    {
      BnetGameAccount hearthstoneGameAccount = BnetPresenceMgr.Get().GetMyPlayer().GetHearthstoneGameAccount();
      if (string.Compare(gameAccount.GetClientVersion(), hearthstoneGameAccount.GetClientVersion()) != 0 || string.Compare(gameAccount.GetClientEnv(), hearthstoneGameAccount.GetClientEnv()) != 0)
        return false;
    }
    return SceneMgr.Get().IsInGame();
  }

  public bool IsPlayerSpectatingMyGamesOpposingSide(BnetGameAccountId gameAccountId)
  {
    BnetGameAccount gameAccount = BnetPresenceMgr.Get().GetGameAccount(gameAccountId);
    if (gameAccount == (BnetGameAccount) null)
      return false;
    BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
    bool flag = false;
    if (BnetFriendMgr.Get() != null && BnetFriendMgr.Get().IsFriend(gameAccountId))
    {
      JoinInfo spectatorJoinInfo = SpectatorManager.GetSpectatorJoinInfo(gameAccount);
      Map<int, Player>.ValueCollection source = GameState.Get() != null ? GameState.Get().GetPlayerMap().Values : (Map<int, Player>.ValueCollection) null;
      if (spectatorJoinInfo != null && spectatorJoinInfo.SpectatedPlayers.Count > 0 && (source != null && source.Count > 0))
      {
        for (int index = 0; index < spectatorJoinInfo.SpectatedPlayers.Count; ++index)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          SpectatorManager.\u003CIsPlayerSpectatingMyGamesOpposingSide\u003Ec__AnonStorey44E sideCAnonStorey44E = new SpectatorManager.\u003CIsPlayerSpectatingMyGamesOpposingSide\u003Ec__AnonStorey44E();
          // ISSUE: reference to a compiler-generated field
          sideCAnonStorey44E.spectatedPlayerId = BnetUtils.CreateGameAccountId(spectatorJoinInfo.SpectatedPlayers[index]);
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          if ((BnetEntityId) sideCAnonStorey44E.spectatedPlayerId != (BnetEntityId) myGameAccountId && source.Any<Player>(new Func<Player, bool>(sideCAnonStorey44E.\u003C\u003Em__308)))
          {
            flag = true;
            break;
          }
        }
      }
    }
    return flag;
  }

  public bool IsInvitedToSpectateMyGame(BnetGameAccountId gameAccountId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return ((IEnumerable<PartyInvite>) BnetParty.GetSentInvites(this.m_spectatorPartyIdMain)).FirstOrDefault<PartyInvite>(new Func<PartyInvite, bool>(new SpectatorManager.\u003CIsInvitedToSpectateMyGame\u003Ec__AnonStorey44F() { gameAccountId = gameAccountId }.\u003C\u003Em__309)) != null;
  }

  public bool CanKickSpectator(BnetGameAccountId gameAccountId)
  {
    return this.IsSpectatingMe(gameAccountId);
  }

  public bool HasInvitedMeToSpectate(BnetGameAccountId gameAccountId)
  {
    return BnetParty.GetReceivedInviteFrom(gameAccountId, PartyType.SPECTATOR_PARTY) != null;
  }

  public bool HasAnyReceivedInvites()
  {
    return ((IEnumerable<PartyInvite>) BnetParty.GetReceivedInvites()).Where<PartyInvite>((Func<PartyInvite, bool>) (i => i.PartyType == PartyType.SPECTATOR_PARTY)).ToArray<PartyInvite>().Length > 0;
  }

  public bool MyGameHasSpectators()
  {
    if (!SceneMgr.Get().IsInGame())
      return false;
    return this.m_gameServerKnownSpectators.Count > 0;
  }

  public BnetGameAccountId GetSpectateeFriendlySide()
  {
    return this.m_spectateeFriendlySide;
  }

  public BnetGameAccountId GetSpectateeOpposingSide()
  {
    return this.m_spectateeOpposingSide;
  }

  public bool IsSpectatingOpposingSide()
  {
    return (BnetEntityId) this.m_spectateeOpposingSide != (BnetEntityId) null;
  }

  public bool HasPreviouslyKickedMeFromGame(BnetGameAccountId playerId)
  {
    float num;
    if (this.m_kickedFromSpectatingList == null || !this.m_kickedFromSpectatingList.TryGetValue(playerId, out num))
      return false;
    if ((double) Time.realtimeSinceStartup >= (double) num && (double) Time.realtimeSinceStartup - (double) num < 10.0)
      return true;
    this.m_kickedFromSpectatingList.Remove(playerId);
    if (this.m_kickedFromSpectatingList.Count == 0)
      this.m_kickedFromSpectatingList = (Map<BnetGameAccountId, float>) null;
    return false;
  }

  public void SpectatePlayer(BnetPlayer player)
  {
    if (this.m_pendingSpectatePlayerAfterLeave != null || !this.CanSpectate(player))
      return;
    BnetGameAccountId hearthstoneGameAccountId = player.GetHearthstoneGameAccountId();
    PartyInvite receivedInviteFrom = BnetParty.GetReceivedInviteFrom(hearthstoneGameAccountId, PartyType.SPECTATOR_PARTY);
    if (receivedInviteFrom != null)
    {
      if ((BnetEntityId) this.m_spectateeFriendlySide == (BnetEntityId) null || (BnetEntityId) this.m_spectateeOpposingSide == (BnetEntityId) null && this.IsPlayerInGame(hearthstoneGameAccountId))
      {
        this.CloseWaitingForNextGameDialog();
        if ((BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null && (BnetEntityId) hearthstoneGameAccountId != (BnetEntityId) this.m_spectateeFriendlySide)
          this.m_spectateeOpposingSide = hearthstoneGameAccountId;
        BnetParty.AcceptReceivedInvite(receivedInviteFrom.InviteId);
        this.HideShownUI();
      }
      else
      {
        this.LogInfoParty("SpectatePlayer: trying to accept an invite even though there is no room for another spectatee: player={0} spectatee1={1} spectatee2={2} isPlayerInGame={3} inviteId={4}", (object) (hearthstoneGameAccountId.ToString() + " (" + BnetUtils.GetPlayerBestName(hearthstoneGameAccountId) + ")"), (object) this.m_spectateeFriendlySide, (object) this.m_spectateeOpposingSide, (object) this.IsPlayerInGame(hearthstoneGameAccountId), (object) receivedInviteFrom.InviteId);
        BnetParty.DeclineReceivedInvite(receivedInviteFrom.InviteId);
      }
    }
    else
    {
      JoinInfo spectatorJoinInfo = SpectatorManager.GetSpectatorJoinInfo(player.GetHearthstoneGameAccount());
      if (spectatorJoinInfo == null)
        Error.AddWarningLoc("Bad Spectator Key", "Spectator key is blank!");
      else if (!spectatorJoinInfo.HasPartyId && string.IsNullOrEmpty(spectatorJoinInfo.SecretKey))
        Error.AddWarningLoc("No Party/Bad Spectator Key", "No party information and Spectator key is blank!");
      else if (spectatorJoinInfo.HasPartyId && this.m_requestedInvite != null)
      {
        this.LogInfoParty("SpectatePlayer: already requesting invite from {0}:party={1}, cannot request another from {2}:party={3}", (object) this.m_requestedInvite.SpectateeId, (object) this.m_requestedInvite.PartyId, (object) hearthstoneGameAccountId, (object) BnetUtils.CreatePartyId(spectatorJoinInfo.PartyId));
      }
      else
      {
        this.HideShownUI();
        if (!((BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null) || !((BnetEntityId) this.m_spectateeOpposingSide == (BnetEntityId) null) || GameMgr.Get() == null || !GameMgr.Get().IsSpectator())
        {
          if (this.m_spectatorPartyIdMain != (PartyId) null)
          {
            if (this.IsInSpectatorMode())
              this.EndSpectatorMode(true);
            else
              this.LeaveParty(this.m_spectatorPartyIdMain, this.ShouldBePartyLeader(this.m_spectatorPartyIdMain));
            this.m_pendingSpectatePlayerAfterLeave = new SpectatorManager.PendingSpectatePlayer(hearthstoneGameAccountId, spectatorJoinInfo);
            return;
          }
          if (this.m_spectatorPartyIdOpposingSide != (PartyId) null)
          {
            this.m_pendingSpectatePlayerAfterLeave = new SpectatorManager.PendingSpectatePlayer(hearthstoneGameAccountId, spectatorJoinInfo);
            this.LeaveParty(this.m_spectatorPartyIdOpposingSide, false);
            return;
          }
        }
        this.SpectatePlayer_Internal(hearthstoneGameAccountId, spectatorJoinInfo);
      }
    }
  }

  private void HideShownUI()
  {
    ShownUIMgr shownUiMgr = ShownUIMgr.Get();
    if (!((UnityEngine.Object) shownUiMgr != (UnityEngine.Object) null))
      return;
    switch (shownUiMgr.GetShownUI())
    {
      case ShownUIMgr.UI_WINDOW.GENERAL_STORE:
        if (!((UnityEngine.Object) GeneralStore.Get() != (UnityEngine.Object) null))
          break;
        GeneralStore.Get().Close(false);
        break;
      case ShownUIMgr.UI_WINDOW.ARENA_STORE:
        if (!((UnityEngine.Object) ArenaStore.Get() != (UnityEngine.Object) null))
          break;
        ArenaStore.Get().Hide();
        break;
      case ShownUIMgr.UI_WINDOW.TAVERN_BRAWL_STORE:
        if (!((UnityEngine.Object) TavernBrawlStore.Get() != (UnityEngine.Object) null))
          break;
        TavernBrawlStore.Get().Hide();
        break;
      case ShownUIMgr.UI_WINDOW.QUEST_LOG:
        if (!((UnityEngine.Object) QuestLog.Get() != (UnityEngine.Object) null))
          break;
        QuestLog.Get().Hide();
        break;
    }
  }

  private void FireSpectatorModeChanged(OnlineEventType evt, BnetPlayer spectatee)
  {
    if (FriendChallengeMgr.Get() != null)
      FriendChallengeMgr.Get().UpdateMyAvailability();
    if (this.OnSpectatorModeChanged != null)
      this.OnSpectatorModeChanged(evt, spectatee);
    if (evt == OnlineEventType.ADDED)
    {
      Screen.sleepTimeout = -1;
    }
    else
    {
      if (evt != OnlineEventType.REMOVED || SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY)
        return;
      Screen.sleepTimeout = -2;
    }
  }

  private void SpectatePlayer_Internal(BnetGameAccountId gameAccountId, JoinInfo joinInfo)
  {
    if (!this.m_initialized)
      this.LogInfoParty("ERROR: SpectatePlayer_Internal called before initialized; spectatee={0}", (object) gameAccountId);
    this.m_pendingSpectatePlayerAfterLeave = (SpectatorManager.PendingSpectatePlayer) null;
    if ((UnityEngine.Object) WelcomeQuests.Get() != (UnityEngine.Object) null)
      WelcomeQuests.Hide();
    PartyInvite receivedInviteFrom = BnetParty.GetReceivedInviteFrom(gameAccountId, PartyType.SPECTATOR_PARTY);
    if ((BnetEntityId) this.m_spectateeFriendlySide == (BnetEntityId) null)
    {
      this.LogInfoPower("================== Begin Spectating 1st player ==================");
      this.m_spectateeFriendlySide = gameAccountId;
      if (receivedInviteFrom != null)
      {
        this.CloseWaitingForNextGameDialog();
        BnetParty.AcceptReceivedInvite(receivedInviteFrom.InviteId);
      }
      else if (joinInfo.HasPartyId)
      {
        PartyId partyId = BnetUtils.CreatePartyId(joinInfo.PartyId);
        this.m_requestedInvite = new SpectatorManager.IntendedSpectateeParty(gameAccountId, partyId);
        BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
        BnetParty.RequestInvite(partyId, gameAccountId, myGameAccountId, PartyType.SPECTATOR_PARTY);
        ApplicationMgr.Get().ScheduleCallback(5f, true, new ApplicationMgr.ScheduledCallback(this.SpectatePlayer_RequestInvite_FriendlySide_Timeout), (object) null);
      }
      else
      {
        this.CloseWaitingForNextGameDialog();
        this.m_isExpectingArriveInGameplayAsSpectator = true;
        GameMgr.Get().SpectateGame(joinInfo);
      }
    }
    else if ((BnetEntityId) this.m_spectateeOpposingSide == (BnetEntityId) null)
    {
      if (!this.IsPlayerInGame(gameAccountId))
        Error.AddWarning("Error", "Cannot spectate two different games at same time.");
      else if ((BnetEntityId) this.m_spectateeFriendlySide == (BnetEntityId) gameAccountId)
      {
        this.LogInfoParty("SpectatePlayer: already spectating player {0}", (object) gameAccountId);
        if (receivedInviteFrom == null)
          return;
        BnetParty.AcceptReceivedInvite(receivedInviteFrom.InviteId);
      }
      else
      {
        this.LogInfoPower("================== Begin Spectating 2nd player ==================");
        this.m_spectateeOpposingSide = gameAccountId;
        if (receivedInviteFrom != null)
          BnetParty.AcceptReceivedInvite(receivedInviteFrom.InviteId);
        else if (joinInfo.HasPartyId)
        {
          PartyId partyId = BnetUtils.CreatePartyId(joinInfo.PartyId);
          this.m_requestedInvite = new SpectatorManager.IntendedSpectateeParty(gameAccountId, partyId);
          BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
          BnetParty.RequestInvite(partyId, gameAccountId, myGameAccountId, PartyType.SPECTATOR_PARTY);
          ApplicationMgr.Get().ScheduleCallback(5f, true, new ApplicationMgr.ScheduledCallback(this.SpectatePlayer_RequestInvite_OpposingSide_Timeout), (object) null);
        }
        else
          this.SpectateSecondPlayer_Network(joinInfo);
      }
    }
    else if ((BnetEntityId) this.m_spectateeFriendlySide == (BnetEntityId) gameAccountId || (BnetEntityId) this.m_spectateeOpposingSide == (BnetEntityId) gameAccountId)
      this.LogInfoParty("SpectatePlayer: already spectating player {0}", (object) gameAccountId);
    else
      Error.AddDevFatal("Cannot spectate more than 2 players.");
  }

  private void SpectatePlayer_RequestInvite_FriendlySide_Timeout(object userData)
  {
    if (this.m_requestedInvite == null)
      return;
    this.m_spectateeFriendlySide = (BnetGameAccountId) null;
    this.EndSpectatorMode(true);
    SpectatorManager.DisplayErrorDialog(GameStrings.Get("GLOBAL_SPECTATOR_SERVER_REJECTED_HEADER"), GameStrings.Get("GLOBAL_SPECTATOR_SERVER_REJECTED_TEXT"));
  }

  private void SpectatePlayer_RequestInvite_OpposingSide_Timeout(object userData)
  {
    if (this.m_requestedInvite == null)
      return;
    this.m_requestedInvite = (SpectatorManager.IntendedSpectateeParty) null;
    this.m_spectateeOpposingSide = (BnetGameAccountId) null;
    SpectatorManager.DisplayErrorDialog(GameStrings.Get("GLOBAL_SPECTATOR_SERVER_REJECTED_HEADER"), GameStrings.Get("GLOBAL_SPECTATOR_SERVER_REJECTED_TEXT"));
  }

  private static JoinInfo CreateJoinInfo(PartyServerInfo serverInfo)
  {
    JoinInfo joinInfo = new JoinInfo();
    joinInfo.ServerIpAddress = serverInfo.ServerIpAddress;
    joinInfo.ServerPort = serverInfo.ServerPort;
    joinInfo.GameHandle = serverInfo.GameHandle;
    joinInfo.SecretKey = serverInfo.SecretKey;
    if (serverInfo.HasGameType)
      joinInfo.GameType = serverInfo.GameType;
    if (serverInfo.HasFormatType)
      joinInfo.FormatType = serverInfo.FormatType;
    if (serverInfo.HasMissionId)
      joinInfo.MissionId = serverInfo.MissionId;
    return joinInfo;
  }

  private static bool IsSameGameAndServer(PartyServerInfo a, PartyServerInfo b)
  {
    if (a == null)
      return b == null;
    if (b == null || !(a.ServerIpAddress == b.ServerIpAddress))
      return false;
    return a.GameHandle == b.GameHandle;
  }

  private static bool IsSameGameAndServer(PartyServerInfo a, bgs.types.GameServerInfo b)
  {
    if (a == null)
      return b == null;
    if (b == null || !(a.ServerIpAddress == b.Address))
      return false;
    return a.GameHandle == b.GameHandle;
  }

  private void SpectateSecondPlayer_Network(JoinInfo joinInfo)
  {
    Network.Get().SpectateSecondPlayer(new bgs.types.GameServerInfo()
    {
      Address = joinInfo.ServerIpAddress,
      Port = (int) joinInfo.ServerPort,
      GameHandle = joinInfo.GameHandle,
      SpectatorPassword = joinInfo.SecretKey,
      SpectatorMode = true
    });
  }

  private void JoinPartyGame(PartyId partyId)
  {
    if (partyId == (PartyId) null)
      return;
    PartyInfo joinedParty = BnetParty.GetJoinedParty(partyId);
    if (joinedParty == null)
      return;
    this.BnetParty_OnPartyAttributeChanged_ServerInfo(joinedParty, "WTCG.Party.ServerInfo", BnetParty.GetPartyAttributeVariant(partyId, "WTCG.Party.ServerInfo"));
  }

  public void LeaveSpectatorMode()
  {
    if (GameMgr.Get().IsSpectator())
    {
      if (Network.IsConnectedToGameServer())
        Network.DisconnectFromGameServer();
      else
        this.LeaveGameScene();
    }
    if (this.m_spectatorPartyIdOpposingSide != (PartyId) null)
      this.LeaveParty(this.m_spectatorPartyIdOpposingSide, false);
    if (!(this.m_spectatorPartyIdMain != (PartyId) null))
      return;
    this.LeaveParty(this.m_spectatorPartyIdMain, false);
  }

  public void InviteToSpectateMe(BnetPlayer player)
  {
    if (player == null)
      return;
    BnetGameAccountId hearthstoneGameAccountId = player.GetHearthstoneGameAccountId();
    if (this.m_kickedPlayers != null && this.m_kickedPlayers.Contains(hearthstoneGameAccountId))
      this.m_kickedPlayers.Remove(hearthstoneGameAccountId);
    if (!this.CanInviteToSpectateMyGame(hearthstoneGameAccountId))
      return;
    if (this.m_userInitiatedOutgoingInvites == null)
      this.m_userInitiatedOutgoingInvites = new HashSet<BnetGameAccountId>();
    this.m_userInitiatedOutgoingInvites.Add(hearthstoneGameAccountId);
    if (this.m_spectatorPartyIdMain == (PartyId) null)
      BnetParty.CreateParty(PartyType.SPECTATOR_PARTY, PrivacyLevel.OPEN_INVITATION, ProtobufUtil.ToByteArray((IProtoBuf) BnetUtils.CreatePegasusBnetId((BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId())), (BnetParty.CreateSuccessCallback) null);
    else
      BnetParty.SendInvite(this.m_spectatorPartyIdMain, hearthstoneGameAccountId, false);
  }

  public void KickSpectator(BnetPlayer player, bool regenerateSpectatorPassword)
  {
    this.KickSpectator_Internal(player, regenerateSpectatorPassword, true);
  }

  private void KickSpectator_Internal(BnetPlayer player, bool regenerateSpectatorPassword, bool addToKickList)
  {
    if (player == null)
      return;
    BnetGameAccountId hearthstoneGameAccountId = player.GetHearthstoneGameAccountId();
    if (!this.CanKickSpectator(hearthstoneGameAccountId))
      return;
    if (addToKickList)
    {
      if (this.m_kickedPlayers == null)
        this.m_kickedPlayers = new HashSet<BnetGameAccountId>();
      this.m_kickedPlayers.Add(hearthstoneGameAccountId);
    }
    if (Network.IsConnectedToGameServer())
      Network.Get().SendRemoveSpectators((regenerateSpectatorPassword ? 1 : 0) != 0, hearthstoneGameAccountId);
    if (!(this.m_spectatorPartyIdMain != (PartyId) null) || !this.ShouldBePartyLeader(this.m_spectatorPartyIdMain) || !BnetParty.IsMember(this.m_spectatorPartyIdMain, hearthstoneGameAccountId))
      return;
    BnetParty.KickMember(this.m_spectatorPartyIdMain, hearthstoneGameAccountId);
  }

  public void UpdateMySpectatorInfo()
  {
    this.UpdateSpectatorPresence();
    this.UpdateSpectatorPartyServerInfo();
  }

  private JoinInfo GetMyGameJoinInfo()
  {
    JoinInfo joinInfo1 = (JoinInfo) null;
    JoinInfo joinInfo2 = new JoinInfo();
    if (this.IsInSpectatorMode())
    {
      if ((BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null)
      {
        BnetId pegasusBnetId = BnetUtils.CreatePegasusBnetId((BnetEntityId) this.m_spectateeFriendlySide);
        joinInfo2.SpectatedPlayers.Add(pegasusBnetId);
      }
      if ((BnetEntityId) this.m_spectateeOpposingSide != (BnetEntityId) null)
      {
        BnetId pegasusBnetId = BnetUtils.CreatePegasusBnetId((BnetEntityId) this.m_spectateeOpposingSide);
        joinInfo2.SpectatedPlayers.Add(pegasusBnetId);
      }
      if (joinInfo2.SpectatedPlayers.Count > 0)
        joinInfo1 = joinInfo2;
    }
    else if (SceneMgr.Get().IsInGame())
    {
      int countSpectatingMe = this.GetCountSpectatingMe();
      if (this.CanAddSpectators())
      {
        bgs.types.GameServerInfo gameServerJoined = Network.Get().GetLastGameServerJoined();
        if (this.m_spectatorPartyIdMain == (PartyId) null && gameServerJoined != null && (SceneMgr.Get().IsInGame() && !SpectatorManager.IsGameOver))
        {
          joinInfo2.ServerIpAddress = gameServerJoined.Address;
          joinInfo2.ServerPort = (uint) gameServerJoined.Port;
          joinInfo2.GameHandle = gameServerJoined.GameHandle;
          joinInfo2.SecretKey = gameServerJoined.SpectatorPassword ?? string.Empty;
        }
        if (this.m_spectatorPartyIdMain != (PartyId) null)
        {
          BnetId pegasusBnetId = BnetUtils.CreatePegasusBnetId(this.m_spectatorPartyIdMain);
          joinInfo2.PartyId = pegasusBnetId;
        }
      }
      joinInfo2.CurrentNumSpectators = countSpectatingMe;
      joinInfo2.MaxNumSpectators = 10;
      joinInfo2.IsJoinable = joinInfo2.CurrentNumSpectators < joinInfo2.MaxNumSpectators;
      joinInfo2.GameType = GameMgr.Get().GetGameType();
      joinInfo2.FormatType = GameMgr.Get().GetFormatType();
      joinInfo2.MissionId = GameMgr.Get().GetMissionId();
      joinInfo1 = joinInfo2;
    }
    return joinInfo1;
  }

  private static PartyServerInfo GetPartyServerInfo(PartyId partyId)
  {
    byte[] partyAttributeBlob = BnetParty.GetPartyAttributeBlob(partyId, "WTCG.Party.ServerInfo");
    return partyAttributeBlob != null ? ProtobufUtil.ParseFrom<PartyServerInfo>(partyAttributeBlob, 0, -1) : (PartyServerInfo) null;
  }

  public bool HandleDisconnectFromGameplay()
  {
    bool hasValue = this.m_expectedDisconnectReason.HasValue;
    this.EndCurrentSpectatedGame(false);
    if (hasValue)
    {
      if (GameMgr.Get().IsTransitionPopupShown())
        GameMgr.Get().GetTransitionPopup().Cancel();
      else
        this.LeaveGameScene();
    }
    return hasValue;
  }

  public void OnRealTimeGameOver()
  {
    this.UpdateMySpectatorInfo();
  }

  private void EndCurrentSpectatedGame(bool isLeavingGameplay)
  {
    if (isLeavingGameplay && this.IsInSpectatorMode())
      SoundManager.Get().LoadAndPlay("SpectatorMode_Exit");
    this.m_expectedDisconnectReason = new int?();
    this.m_isExpectingArriveInGameplayAsSpectator = false;
    this.ClearAllGameServerKnownSpectators();
    if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null && !ApplicationMgr.Get().IsResetting())
      this.UpdateSpectatorPresence();
    if (GameMgr.Get() == null || !((UnityEngine.Object) GameMgr.Get().GetTransitionPopup() != (UnityEngine.Object) null))
      return;
    GameMgr.Get().GetTransitionPopup().OnHidden -= new System.Action<TransitionPopup>(this.EnterSpectatorMode_OnTransitionPopupHide);
  }

  private void EndSpectatorMode(bool wasKnownSpectating = false)
  {
    bool gameplayAsSpectator = this.m_isExpectingArriveInGameplayAsSpectator;
    bool flag = wasKnownSpectating || (BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null || (BnetEntityId) this.m_spectateeOpposingSide != (BnetEntityId) null;
    this.LeaveSpectatorMode();
    this.EndCurrentSpectatedGame(false);
    this.m_spectateeFriendlySide = (BnetGameAccountId) null;
    this.m_spectateeOpposingSide = (BnetGameAccountId) null;
    this.m_requestedInvite = (SpectatorManager.IntendedSpectateeParty) null;
    this.CloseWaitingForNextGameDialog();
    this.m_pendingSpectatePlayerAfterLeave = (SpectatorManager.PendingSpectatePlayer) null;
    this.m_isExpectingArriveInGameplayAsSpectator = false;
    if (flag)
    {
      this.LogInfoPower("================== End Spectator Mode ==================");
      this.FireSpectatorModeChanged(OnlineEventType.REMOVED, BnetUtils.GetPlayer(this.m_spectateeFriendlySide));
    }
    if (!gameplayAsSpectator || ApplicationMgr.Get().IsResetting())
      return;
    SceneMgr.Mode mode = SceneMgr.Mode.HUB;
    if (!GameUtils.AreAllTutorialsComplete())
      Network.Get().ShowBreakingNewsOrError("GLOBAL_ERROR_NETWORK_LOST_GAME_CONNECTION", 0.0f);
    else if (!SceneMgr.Get().IsModeRequested(mode))
    {
      SceneMgr.Get().SetNextMode(mode);
    }
    else
    {
      if (SceneMgr.Get().GetMode() != mode)
        return;
      SceneMgr.Get().ReloadMode();
    }
  }

  private void ClearAllCacheForReset()
  {
    this.EndSpectatorMode(false);
    this.m_initialized = false;
    this.m_spectatorPartyIdMain = (PartyId) null;
    this.m_spectatorPartyIdOpposingSide = (PartyId) null;
    this.m_requestedInvite = (SpectatorManager.IntendedSpectateeParty) null;
    this.m_waitingForNextGameDialog = (AlertPopup) null;
    this.m_pendingSpectatePlayerAfterLeave = (SpectatorManager.PendingSpectatePlayer) null;
    this.m_userInitiatedOutgoingInvites = (HashSet<BnetGameAccountId>) null;
    this.m_kickedPlayers = (HashSet<BnetGameAccountId>) null;
    this.m_kickedFromSpectatingList = (Map<BnetGameAccountId, float>) null;
    this.m_expectedDisconnectReason = new int?();
    this.m_isExpectingArriveInGameplayAsSpectator = false;
    this.m_isShowingRemovedAsSpectatorPopup = false;
    this.m_gameServerKnownSpectators.Clear();
  }

  private void WillReset()
  {
    this.ClearAllCacheForReset();
    if (!((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null))
      return;
    ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.SpectatorManager_UpdatePresenceNextFrame), (object) null);
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_CANCELED:
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
        if (this.IsInSpectatorMode())
        {
          this.EndSpectatorMode(true);
          break;
        }
        break;
      case FindGameState.SERVER_GAME_CANCELED:
        if (this.IsInSpectatorMode())
        {
          SpectatorManager.DisplayErrorDialog(GameStrings.Get("GLOBAL_SPECTATOR_SERVER_REJECTED_HEADER"), GameStrings.Get("GLOBAL_SPECTATOR_SERVER_REJECTED_TEXT"));
          this.EndSpectatorMode(true);
          break;
        }
        break;
    }
    return false;
  }

  private void GameState_InitializedEvent(GameState instance, object userData)
  {
    if (!(this.m_spectatorPartyIdOpposingSide != (PartyId) null))
      return;
    GameState.Get().RegisterCreateGameListener(new GameState.CreateGameCallback(this.GameState_CreateGameEvent), (object) null);
  }

  private void GameState_CreateGameEvent(GameState.CreateGamePhase createGamePhase, object userData)
  {
    if (createGamePhase < GameState.CreateGamePhase.CREATED)
      return;
    GameState.Get().UnregisterCreateGameListener(new GameState.CreateGameCallback(this.GameState_CreateGameEvent));
    if (!(this.m_spectatorPartyIdOpposingSide != (PartyId) null))
      return;
    this.AutoSpectateOpposingSide();
  }

  private void AutoSpectateOpposingSide()
  {
    if (GameState.Get() == null)
      return;
    if (GameState.Get().GetCreateGamePhase() < GameState.CreateGamePhase.CREATED)
    {
      GameState.Get().RegisterCreateGameListener(new GameState.CreateGameCallback(this.GameState_CreateGameEvent), (object) null);
    }
    else
    {
      if (SceneMgr.Get().GetMode() != SceneMgr.Mode.GAMEPLAY)
        return;
      if ((UnityEngine.Object) GameMgr.Get().GetTransitionPopup() != (UnityEngine.Object) null && GameMgr.Get().GetTransitionPopup().IsShown())
      {
        GameMgr.Get().GetTransitionPopup().OnHidden += new System.Action<TransitionPopup>(this.EnterSpectatorMode_OnTransitionPopupHide);
      }
      else
      {
        if (!(this.m_spectatorPartyIdOpposingSide != (PartyId) null) || !((BnetEntityId) this.m_spectateeOpposingSide != (BnetEntityId) null) || !this.IsStillInParty(this.m_spectatorPartyIdOpposingSide))
          return;
        if (this.IsPlayerInGame(this.m_spectateeOpposingSide))
        {
          PartyServerInfo partyServerInfo = SpectatorManager.GetPartyServerInfo(this.m_spectatorPartyIdOpposingSide);
          JoinInfo joinInfo = partyServerInfo != null ? SpectatorManager.CreateJoinInfo(partyServerInfo) : (JoinInfo) null;
          if (joinInfo == null)
            return;
          this.SpectateSecondPlayer_Network(joinInfo);
        }
        else
        {
          this.LogInfoPower("================== End Spectating 2nd player ==================");
          this.LeaveParty(this.m_spectatorPartyIdOpposingSide, false);
        }
      }
    }
  }

  private void OnSceneUnloaded(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    if (mode == SceneMgr.Mode.GAMEPLAY && prevMode != SceneMgr.Mode.GAMEPLAY)
    {
      if ((BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null)
        BnetBar.Get().HideFriendList();
      if (GameMgr.Get().IsSpectator())
      {
        if ((UnityEngine.Object) GameMgr.Get().GetTransitionPopup() != (UnityEngine.Object) null)
          GameMgr.Get().GetTransitionPopup().OnHidden += new System.Action<TransitionPopup>(this.EnterSpectatorMode_OnTransitionPopupHide);
        this.FireSpectatorModeChanged(OnlineEventType.ADDED, BnetUtils.GetPlayer(this.m_spectateeOpposingSide ?? this.m_spectateeFriendlySide));
      }
      else
        this.m_kickedPlayers = (HashSet<BnetGameAccountId>) null;
      this.CloseWaitingForNextGameDialog();
      this.DeclineAllReceivedInvitations();
      this.UpdateMySpectatorInfo();
    }
    else
    {
      if (prevMode != SceneMgr.Mode.GAMEPLAY || mode == SceneMgr.Mode.GAMEPLAY)
        return;
      if (this.IsInSpectatorMode())
      {
        this.LogInfoPower("================== End Spectator Game ==================");
        Time.timeScale = !(bool) ((UnityEngine.Object) SceneDebugger.Get()) ? 1f : SceneDebugger.GetDevTimescale();
      }
      this.EndCurrentSpectatedGame(true);
      this.UpdateMySpectatorInfo();
      if (!(this.m_spectatorPartyIdMain != (PartyId) null) || !this.IsStillInParty(this.m_spectatorPartyIdMain) || this.ShouldBePartyLeader(this.m_spectatorPartyIdMain))
        return;
      PartyServerInfo partyServerInfo = SpectatorManager.GetPartyServerInfo(this.m_spectatorPartyIdMain);
      if (partyServerInfo == null)
      {
        this.ShowWaitingForNextGameDialog();
      }
      else
      {
        bgs.types.GameServerInfo gameServerJoined = Network.Get().GetLastGameServerJoined();
        if (!SpectatorManager.IsSameGameAndServer(partyServerInfo, gameServerJoined))
        {
          this.LogInfoPower("================== OnSceneUnloaded: auto-spectating game after leaving game ==================");
          this.BnetParty_OnPartyAttributeChanged_ServerInfo(new PartyInfo(this.m_spectatorPartyIdMain, PartyType.SPECTATOR_PARTY), "WTCG.Party.ServerInfo", BnetParty.GetPartyAttributeVariant(this.m_spectatorPartyIdMain, "WTCG.Party.ServerInfo"));
        }
        else
          this.ShowWaitingForNextGameDialog();
      }
    }
  }

  public void ShowWaitingForNextGameDialog()
  {
    if (!Network.IsLoggedIn())
      return;
    DialogManager.Get().ShowUniquePopup(new AlertPopup.PopupInfo()
    {
      m_id = "SPECTATOR_WAITING_FOR_NEXT_GAME",
      m_layerToUse = new GameLayer?(GameLayer.BackgroundUI),
      m_headerText = GameStrings.Get("GLOBAL_SPECTATOR_WAITING_FOR_NEXT_GAME_HEADER"),
      m_text = this.GetWaitingForNextGameDialogText(),
      m_responseDisplay = AlertPopup.ResponseDisplay.CANCEL,
      m_cancelText = GameStrings.Get("GLOBAL_LEAVE_SPECTATOR_MODE"),
      m_responseCallback = new AlertPopup.ResponseCallback(this.OnSceneUnloaded_AwaitingNextGame_LeaveSpectatorMode),
      m_keyboardEscIsCancel = false
    }, new DialogManager.DialogProcessCallback(this.OnSceneUnloaded_AwaitingNextGame_DialogProcessCallback));
    ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(SpectatorManager.WaitingForNextGame_AutoLeaveSpectatorMode), (object) null);
    if ((double) (float) SpectatorManager.WAITING_FOR_NEXT_GAME_AUTO_LEAVE_SECONDS < 0.0)
      return;
    ApplicationMgr.Get().ScheduleCallback((float) SpectatorManager.WAITING_FOR_NEXT_GAME_AUTO_LEAVE_SECONDS, true, new ApplicationMgr.ScheduledCallback(SpectatorManager.WaitingForNextGame_AutoLeaveSpectatorMode), (object) null);
  }

  private void CloseWaitingForNextGameDialog()
  {
    if ((bool) SpectatorManager.DISABLE_MENU_BUTTON_WHILE_WAITING)
      BnetBar.Get().m_menuButton.SetEnabled(true);
    if ((UnityEngine.Object) DialogManager.Get() != (UnityEngine.Object) null)
      DialogManager.Get().RemoveUniquePopupRequestFromQueue("SPECTATOR_WAITING_FOR_NEXT_GAME");
    if ((UnityEngine.Object) this.m_waitingForNextGameDialog != (UnityEngine.Object) null)
    {
      this.m_waitingForNextGameDialog.Hide();
      this.m_waitingForNextGameDialog = (AlertPopup) null;
    }
    ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(SpectatorManager.WaitingForNextGame_AutoLeaveSpectatorMode), (object) null);
  }

  private void UpdateWaitingForNextGameDialog()
  {
    if ((UnityEngine.Object) this.m_waitingForNextGameDialog == (UnityEngine.Object) null)
      return;
    this.m_waitingForNextGameDialog.BodyText = this.GetWaitingForNextGameDialogText();
  }

  private string GetWaitingForNextGameDialogText()
  {
    BnetPlayer player = BnetUtils.GetPlayer(this.m_spectateeFriendlySide);
    string playerBestName = BnetUtils.GetPlayerBestName(this.m_spectateeFriendlySide);
    string str = player == null || !player.IsOnline() ? GameStrings.Get("GLOBAL_OFFLINE") : PresenceMgr.Get().GetStatusText(player);
    if (str != null)
      str = str.Trim();
    string key = "GLOBAL_SPECTATOR_WAITING_FOR_NEXT_GAME_TEXT_OFFLINE";
    if (player != null && player.IsOnline() && !string.IsNullOrEmpty(str))
    {
      key = "GLOBAL_SPECTATOR_WAITING_FOR_NEXT_GAME_TEXT";
      Enum[] statusEnums = PresenceMgr.Get().GetStatusEnums(player);
      if (statusEnums.Length > 0 && (PresenceStatus) statusEnums[0] == PresenceStatus.ADVENTURE_SCENARIO_SELECT)
        key = "GLOBAL_SPECTATOR_WAITING_FOR_NEXT_GAME_TEXT_ENTERING";
      else if (statusEnums.Length > 0 && (PresenceStatus) statusEnums[0] == PresenceStatus.ADVENTURE_SCENARIO_PLAYING_GAME)
      {
        if (statusEnums.Length > 1 && GameUtils.IsHeroicAdventureMission((int) statusEnums[1]))
          key = "GLOBAL_SPECTATOR_WAITING_FOR_NEXT_GAME_TEXT_BATTLING";
        else if (statusEnums.Length > 1 && GameUtils.IsClassChallengeMission((int) statusEnums[1]))
          key = "GLOBAL_SPECTATOR_WAITING_FOR_NEXT_GAME_TEXT_PLAYING";
      }
    }
    return GameStrings.Format(key, (object) playerBestName, (object) str);
  }

  private bool OnSceneUnloaded_AwaitingNextGame_DialogProcessCallback(DialogBase dialog, object userData)
  {
    if (SceneMgr.Get().IsInGame() || GameMgr.Get() != null && GameMgr.Get().IsFindingGame())
      return false;
    this.m_waitingForNextGameDialog = (AlertPopup) dialog;
    this.UpdateWaitingForNextGameDialog();
    if ((bool) SpectatorManager.DISABLE_MENU_BUTTON_WHILE_WAITING)
      BnetBar.Get().m_menuButton.SetEnabled(false);
    return true;
  }

  private static void WaitingForNextGame_AutoLeaveSpectatorMode(object userData)
  {
    if (!SpectatorManager.Get().IsInSpectatorMode() || SceneMgr.Get().IsInGame())
      return;
    SpectatorManager.Get().LeaveSpectatorMode();
    SpectatorManager.DisplayErrorDialog(GameStrings.Get("GLOBAL_SPECTATOR_WAITING_FOR_NEXT_GAME_HEADER"), GameStrings.Format("GLOBAL_SPECTATOR_WAITING_FOR_NEXT_GAME_TIMEOUT"));
  }

  private void OnSceneUnloaded_AwaitingNextGame_LeaveSpectatorMode(AlertPopup.Response response, object userData)
  {
    this.LeaveSpectatorMode();
  }

  private void EnterSpectatorMode_OnTransitionPopupHide(TransitionPopup popup)
  {
    popup.OnHidden -= new System.Action<TransitionPopup>(this.EnterSpectatorMode_OnTransitionPopupHide);
    if ((UnityEngine.Object) SoundManager.Get() != (UnityEngine.Object) null)
      SoundManager.Get().LoadAndPlay("SpectatorMode_Enter");
    if (!((BnetEntityId) this.m_spectateeOpposingSide != (BnetEntityId) null))
      return;
    this.AutoSpectateOpposingSide();
  }

  private void OnSpectatorOpenJoinOptionChanged(Option option, object prevValue, bool existed, object userData)
  {
    bool flag = Options.Get().GetBool(Option.SPECTATOR_OPEN_JOIN);
    if (existed && (bool) prevValue == flag || !(bool) ((UnityEngine.Object) SceneMgr.Get()) || !SceneMgr.Get().IsInGame() || GameMgr.Get() != null && GameMgr.Get().IsSpectator())
      return;
    JoinInfo joinInfo = !flag ? (JoinInfo) null : this.GetMyGameJoinInfo();
    if (!Network.ShouldBeConnectedToAurora())
      return;
    BnetPresenceMgr.Get().SetGameFieldBlob(21U, (IProtoBuf) joinInfo);
  }

  private void Network_OnSpectatorInviteReceived(Invite protoInvite)
  {
    this.AddReceivedInvitation(BnetUtils.CreateGameAccountId(protoInvite.InviterGameAccountId), protoInvite.JoinInfo);
  }

  private void Network_OnSpectatorInviteReceived_ResponseCallback(AlertPopup.Response response, object userData)
  {
    BnetGameAccountId bnetGameAccountId = (BnetGameAccountId) userData;
    if (response == AlertPopup.Response.CANCEL)
    {
      this.RemoveReceivedInvitation(bnetGameAccountId);
    }
    else
    {
      BnetPlayer player = BnetUtils.GetPlayer(bnetGameAccountId);
      if (player == null)
        return;
      this.SpectatePlayer(player);
    }
  }

  private void Network_OnSpectatorNotifyEvent()
  {
    SpectatorNotify spectatorNotify = Network.GetSpectatorNotify();
    if (spectatorNotify.HasSpectatorPasswordUpdate && !string.IsNullOrEmpty(spectatorNotify.SpectatorPasswordUpdate))
    {
      bgs.types.GameServerInfo gameServerJoined = Network.Get().GetLastGameServerJoined();
      if (!spectatorNotify.SpectatorPasswordUpdate.Equals(gameServerJoined.SpectatorPassword))
      {
        gameServerJoined.SpectatorPassword = spectatorNotify.SpectatorPasswordUpdate;
        this.UpdateMySpectatorInfo();
        this.RevokeAllSentInvitations();
      }
    }
    if (spectatorNotify.HasSpectatorRemoved)
    {
      this.m_expectedDisconnectReason = new int?(spectatorNotify.SpectatorRemoved.ReasonCode);
      bool flag = GameMgr.Get().IsTransitionPopupShown();
      if (spectatorNotify.SpectatorRemoved.ReasonCode == 0)
      {
        if (spectatorNotify.SpectatorRemoved.HasRemovedBy)
        {
          if (this.m_kickedFromSpectatingList == null)
            this.m_kickedFromSpectatingList = new Map<BnetGameAccountId, float>();
          this.m_kickedFromSpectatingList[BnetUtils.CreateGameAccountId(spectatorNotify.SpectatorRemoved.RemovedBy)] = Time.realtimeSinceStartup;
        }
        if (!this.m_isShowingRemovedAsSpectatorPopup)
        {
          AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
          info.m_headerText = GameStrings.Get("GLOBAL_SPECTATOR_REMOVED_PROMPT_HEADER");
          info.m_text = GameStrings.Get("GLOBAL_SPECTATOR_REMOVED_PROMPT_TEXT");
          info.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
          info.m_responseCallback = !flag ? (AlertPopup.ResponseCallback) ((r, data) => SpectatorManager.Get().m_isShowingRemovedAsSpectatorPopup = false) : new AlertPopup.ResponseCallback(this.Network_OnSpectatorNotifyEvent_Removed_GoToNextMode);
          this.m_isShowingRemovedAsSpectatorPopup = true;
          DialogManager.Get().ShowPopup(info);
        }
      }
      else if (flag)
        this.Network_OnSpectatorNotifyEvent_Removed_GoToNextMode(AlertPopup.Response.OK, (object) null);
      SoundManager.Get().LoadAndPlay("SpectatorMode_Exit");
      this.EndSpectatorMode(true);
      this.m_expectedDisconnectReason = new int?(spectatorNotify.SpectatorRemoved.ReasonCode);
    }
    if (spectatorNotify == null || spectatorNotify.SpectatorChange.Count == 0 || GameMgr.Get() != null && GameMgr.Get().IsSpectator())
      return;
    using (List<SpectatorChange>.Enumerator enumerator = spectatorNotify.SpectatorChange.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpectatorChange current = enumerator.Current;
        BnetGameAccountId gameAccountId = BnetUtils.CreateGameAccountId(current.GameAccountId);
        if (current.IsRemoved)
        {
          this.RemoveKnownSpectator(gameAccountId);
        }
        else
        {
          this.AddKnownSpectator(gameAccountId);
          this.ReinviteKnownSpectatorsNotInParty();
        }
      }
    }
  }

  private void Network_OnSpectatorNotifyEvent_Removed_GoToNextMode(AlertPopup.Response response, object userData)
  {
    this.m_isShowingRemovedAsSpectatorPopup = false;
  }

  private void ReceivedInvitation_ExpireTimeout(object userData)
  {
    this.PruneOldInvites();
    if (this.m_receivedSpectateMeInvites.Count <= 0)
      return;
    ApplicationMgr.Get().ScheduleCallback(Mathf.Max(0.0f, this.m_receivedSpectateMeInvites.Min<KeyValuePair<BnetGameAccountId, SpectatorManager.ReceivedInvite>>((Func<KeyValuePair<BnetGameAccountId, SpectatorManager.ReceivedInvite>, float>) (kv => kv.Value.m_timestamp)) + 300f - Time.realtimeSinceStartup), true, new ApplicationMgr.ScheduledCallback(this.ReceivedInvitation_ExpireTimeout), (object) null);
  }

  private void Presence_OnGameAccountPresenceChange(PresenceUpdate[] updates)
  {
    foreach (PresenceUpdate update in updates)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      SpectatorManager.\u003CPresence_OnGameAccountPresenceChange\u003Ec__AnonStorey450 changeCAnonStorey450 = new SpectatorManager.\u003CPresence_OnGameAccountPresenceChange\u003Ec__AnonStorey450();
      // ISSUE: reference to a compiler-generated field
      changeCAnonStorey450.entityId = BnetGameAccountId.CreateFromEntityId(update.entityId);
      bool flag1 = (int) update.fieldId == 1 && update.programId == (bgs.FourCC) BnetProgramId.BNET;
      bool flag2 = update.programId == (bgs.FourCC) BnetProgramId.HEARTHSTONE && (int) update.fieldId == 17;
      // ISSUE: reference to a compiler-generated field
      if ((UnityEngine.Object) this.m_waitingForNextGameDialog != (UnityEngine.Object) null && (BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null && (flag1 || flag2) && (BnetEntityId) changeCAnonStorey450.entityId == (BnetEntityId) this.m_spectateeFriendlySide)
        this.UpdateWaitingForNextGameDialog();
      if (flag1 && update.boolVal)
      {
        foreach (PartyId joinedPartyId in BnetParty.GetJoinedPartyIds())
        {
          // ISSUE: reference to a compiler-generated field
          if (BnetParty.IsLeader(joinedPartyId) && !BnetParty.IsMember(joinedPartyId, changeCAnonStorey450.entityId))
          {
            BnetGameAccountId partyCreator = this.GetPartyCreator(joinedPartyId);
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated method
            if ((BnetEntityId) partyCreator != (BnetEntityId) null && (BnetEntityId) partyCreator == (BnetEntityId) changeCAnonStorey450.entityId && !((IEnumerable<PartyInvite>) BnetParty.GetSentInvites(joinedPartyId)).Any<PartyInvite>(new Func<PartyInvite, bool>(changeCAnonStorey450.\u003C\u003Em__30D)))
            {
              // ISSUE: reference to a compiler-generated field
              BnetParty.SendInvite(joinedPartyId, changeCAnonStorey450.entityId, false);
            }
          }
        }
      }
    }
  }

  private void BnetFriendMgr_OnFriendsChanged(BnetFriendChangelist changelist, object userData)
  {
    if (changelist == null || !this.IsBeingSpectated())
      return;
    List<BnetPlayer> removedFriends = changelist.GetRemovedFriends();
    if (removedFriends == null)
      return;
    using (List<BnetPlayer>.Enumerator enumerator = removedFriends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer current = enumerator.Current;
        if (this.IsSpectatingMe(current.GetHearthstoneGameAccountId()))
          this.KickSpectator_Internal(current, true, false);
      }
    }
  }

  private void EndGameScreen_OnTwoScoopsShown(bool shown, EndGameTwoScoop twoScoops)
  {
    if (!this.IsInSpectatorMode())
      return;
    if (shown)
      ApplicationMgr.Get().ScheduleCallback(5f, false, new ApplicationMgr.ScheduledCallback(this.EndGameScreen_OnTwoScoopsShown_AutoClose), (object) null);
    else
      ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.EndGameScreen_OnTwoScoopsShown_AutoClose), (object) null);
  }

  private void EndGameScreen_OnTwoScoopsShown_AutoClose(object userData)
  {
    if ((UnityEngine.Object) EndGameScreen.Get() == (UnityEngine.Object) null)
      return;
    if ((double) (float) SpectatorManager.WAITING_FOR_NEXT_GAME_AUTO_LEAVE_SECONDS >= 0.0)
    {
      do
        ;
      while (EndGameScreen.Get().ContinueEvents());
    }
    else
      EndGameScreen.Get().ContinueEvents();
  }

  private void BnetParty_OnError(PartyError error)
  {
    if (!error.IsOperationCallback)
      return;
    switch (error.FeatureEvent)
    {
      case BnetFeatureEvent.Party_Create_Callback:
        if (!(error.ErrorCode != BattleNetErrors.ERROR_OK))
          break;
        this.m_userInitiatedOutgoingInvites = (HashSet<BnetGameAccountId>) null;
        SpectatorManager.DisplayErrorDialog(GameStrings.Get("GLOBAL_ERROR_GENERIC_HEADER"), GameStrings.Format("GLOBAL_SPECTATOR_ERROR_CREATE_PARTY_TEXT"));
        break;
      case BnetFeatureEvent.Party_Leave_Callback:
      case BnetFeatureEvent.Party_Dissolve_Callback:
        if (this.m_leavePartyIdsRequested != null)
          this.m_leavePartyIdsRequested.Remove(error.PartyId);
        if (this.m_pendingSpectatePlayerAfterLeave == null || !(error.ErrorCode != BattleNetErrors.ERROR_OK))
          break;
        SpectatorManager.DisplayErrorDialog(GameStrings.Get("GLOBAL_ERROR_GENERIC_HEADER"), GameStrings.Format("GLOBAL_SPECTATOR_ERROR_LEAVE_FOR_SPECTATE_PLAYER_TEXT", (object) BnetUtils.GetPlayerBestName(this.m_pendingSpectatePlayerAfterLeave.SpectateeId)));
        this.m_pendingSpectatePlayerAfterLeave = (SpectatorManager.PendingSpectatePlayer) null;
        break;
    }
  }

  private static void DisplayErrorDialog(string header, string body)
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = header,
      m_text = body,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  private void BnetParty_OnJoined(OnlineEventType evt, PartyInfo party, LeaveReason? reason)
  {
    if (!this.m_initialized || party.Type != PartyType.SPECTATOR_PARTY)
      return;
    if (evt == OnlineEventType.REMOVED)
    {
      bool flag1 = false;
      if (this.m_leavePartyIdsRequested != null)
        flag1 = this.m_leavePartyIdsRequested.Remove(party.Id);
      this.LogInfoParty("SpectatorParty_OnLeft: left party={0} current={1} reason={2} wasRequested={3}", (object) party, (object) this.m_spectatorPartyIdMain, (object) (!reason.HasValue ? "null" : reason.Value.ToString()), (object) flag1);
      bool flag2 = false;
      if (party.Id == this.m_spectatorPartyIdOpposingSide)
      {
        this.m_spectatorPartyIdOpposingSide = (PartyId) null;
        flag2 = true;
      }
      else if ((BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null)
      {
        if (party.Id == this.m_spectatorPartyIdMain)
        {
          this.m_spectatorPartyIdMain = (PartyId) null;
          flag2 = true;
        }
      }
      else if ((BnetEntityId) this.m_spectateeFriendlySide == (BnetEntityId) null && (BnetEntityId) this.m_spectateeOpposingSide == (BnetEntityId) null)
      {
        if (party.Id != this.m_spectatorPartyIdMain)
        {
          this.CreatePartyIfNecessary();
          return;
        }
        this.m_userInitiatedOutgoingInvites = (HashSet<BnetGameAccountId>) null;
        this.m_spectatorPartyIdMain = (PartyId) null;
        this.UpdateSpectatorPresence();
        if (reason.HasValue && reason.Value != LeaveReason.MEMBER_LEFT && reason.Value != LeaveReason.DISSOLVED_BY_MEMBER)
          ApplicationMgr.Get().ScheduleCallback(1f, true, (ApplicationMgr.ScheduledCallback) (userData => this.CreatePartyIfNecessary()), (object) null);
      }
      if (this.m_pendingSpectatePlayerAfterLeave != null && this.m_spectatorPartyIdMain == (PartyId) null && this.m_spectatorPartyIdOpposingSide == (PartyId) null)
        this.SpectatePlayer_Internal(this.m_pendingSpectatePlayerAfterLeave.SpectateeId, this.m_pendingSpectatePlayerAfterLeave.JoinInfo);
      else if (flag2 && this.m_spectatorPartyIdMain == (PartyId) null)
      {
        if (flag1)
        {
          this.EndSpectatorMode(true);
        }
        else
        {
          bool flag3 = reason.HasValue && reason.Value == LeaveReason.MEMBER_KICKED;
          bool flag4 = this.m_expectedDisconnectReason.HasValue && this.m_expectedDisconnectReason.Value == 0;
          this.EndSpectatorMode(true);
          if (flag3 && !flag4)
          {
            if (flag3)
            {
              BnetGameAccountId index = this.GetPartyCreator(party.Id);
              if ((BnetEntityId) index == (BnetEntityId) null)
              {
                bgs.PartyMember leader = BnetParty.GetLeader(party.Id);
                index = leader != null ? leader.GameAccountId : (BnetGameAccountId) null;
              }
              if ((BnetEntityId) index != (BnetEntityId) null)
              {
                if (this.m_kickedFromSpectatingList == null)
                  this.m_kickedFromSpectatingList = new Map<BnetGameAccountId, float>();
                this.m_kickedFromSpectatingList[index] = Time.realtimeSinceStartup;
              }
            }
            if (!this.m_isShowingRemovedAsSpectatorPopup)
            {
              bool flag5 = GameMgr.Get().IsTransitionPopupShown();
              AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
              info.m_headerText = GameStrings.Get("GLOBAL_SPECTATOR_REMOVED_PROMPT_HEADER");
              info.m_text = GameStrings.Get("GLOBAL_SPECTATOR_REMOVED_PROMPT_TEXT");
              info.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
              info.m_responseCallback = !flag5 ? (AlertPopup.ResponseCallback) ((r, data) => SpectatorManager.Get().m_isShowingRemovedAsSpectatorPopup = false) : new AlertPopup.ResponseCallback(this.Network_OnSpectatorNotifyEvent_Removed_GoToNextMode);
              this.m_isShowingRemovedAsSpectatorPopup = true;
              DialogManager.Get().ShowPopup(info);
            }
          }
        }
      }
      if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
        ApplicationMgr.Get().ScheduleCallback(0.5f, false, new ApplicationMgr.ScheduledCallback(this.BnetParty_OnLostPartyReference_RemoveKnownCreator), (object) party.Id);
    }
    if (evt != OnlineEventType.ADDED)
      return;
    BnetGameAccountId partyCreator = this.GetPartyCreator(party.Id);
    if ((BnetEntityId) partyCreator == (BnetEntityId) null)
    {
      this.LogInfoParty("SpectatorParty_OnJoined: joined party={0} without creator.", (object) party.Id);
      this.LeaveParty(party.Id, BnetParty.IsLeader(party.Id));
    }
    else
    {
      if (this.m_requestedInvite != null && this.m_requestedInvite.PartyId == party.Id)
      {
        this.m_requestedInvite = (SpectatorManager.IntendedSpectateeParty) null;
        ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.SpectatePlayer_RequestInvite_FriendlySide_Timeout), (object) null);
        ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.SpectatePlayer_RequestInvite_OpposingSide_Timeout), (object) null);
      }
      bool flag1 = this.ShouldBePartyLeader(party.Id);
      bool flag2 = this.m_spectatorPartyIdMain == (PartyId) null;
      bool flag3 = flag2;
      if (this.m_spectatorPartyIdMain != (PartyId) null && this.m_spectatorPartyIdMain != party.Id && (flag1 || (BnetEntityId) partyCreator != (BnetEntityId) this.m_spectateeOpposingSide))
      {
        flag3 = true;
        this.LogInfoParty("SpectatorParty_OnJoined: joined party={0} when different current={1} (will be clobbered) joinedParties={2}", (object) party.Id, (object) this.m_spectatorPartyIdMain, (object) string.Join(", ", ((IEnumerable<PartyInfo>) BnetParty.GetJoinedParties()).Select<PartyInfo, string>((Func<PartyInfo, string>) (i => i.ToString())).ToArray<string>()));
      }
      if (flag1)
      {
        this.m_spectatorPartyIdMain = party.Id;
        if (flag3)
          this.UpdateSpectatorPresence();
        this.UpdateSpectatorPartyServerInfo();
        this.ReinviteKnownSpectatorsNotInParty();
        if (this.m_userInitiatedOutgoingInvites != null)
        {
          using (HashSet<BnetGameAccountId>.Enumerator enumerator = this.m_userInitiatedOutgoingInvites.GetEnumerator())
          {
            while (enumerator.MoveNext())
              BnetParty.SendInvite(this.m_spectatorPartyIdMain, enumerator.Current, false);
          }
        }
        if (!flag2 || this.OnSpectatorToMyGame == null)
          return;
        foreach (bgs.PartyMember member in BnetParty.GetMembers(this.m_spectatorPartyIdMain))
        {
          if (!((BnetEntityId) member.GameAccountId == (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId()))
          {
            ApplicationMgr.Get().StartCoroutine(this.WaitForPresenceThenToast(member.GameAccountId, SocialToastMgr.TOAST_TYPE.SPECTATOR_ADDED));
            this.OnSpectatorToMyGame(OnlineEventType.ADDED, BnetUtils.GetPlayer(member.GameAccountId));
          }
        }
      }
      else
      {
        bool flag4 = true;
        if ((BnetEntityId) this.m_spectateeFriendlySide == (BnetEntityId) null)
        {
          this.m_spectateeFriendlySide = partyCreator;
          this.m_spectatorPartyIdMain = party.Id;
          flag4 = false;
        }
        else if ((BnetEntityId) partyCreator == (BnetEntityId) this.m_spectateeFriendlySide)
          this.m_spectatorPartyIdMain = party.Id;
        else if ((BnetEntityId) partyCreator == (BnetEntityId) this.m_spectateeOpposingSide)
          this.m_spectatorPartyIdOpposingSide = party.Id;
        if (BnetParty.GetPartyAttributeBlob(party.Id, "WTCG.Party.ServerInfo") != null)
        {
          this.LogInfoParty("SpectatorParty_OnJoined: joined party={0} as spectator, begin spectating game.", (object) party.Id);
          if (!flag4)
          {
            if ((BnetEntityId) partyCreator == (BnetEntityId) this.m_spectateeOpposingSide)
              this.LogInfoPower("================== Begin Spectating 2nd player ==================");
            else
              this.LogInfoPower("================== Begin Spectating 1st player ==================");
          }
          this.JoinPartyGame(party.Id);
        }
        else
        {
          if (!SceneMgr.Get().IsInGame())
            this.ShowWaitingForNextGameDialog();
          this.FireSpectatorModeChanged(OnlineEventType.ADDED, BnetUtils.GetPlayer(partyCreator));
        }
      }
    }
  }

  private void BnetParty_OnLostPartyReference_RemoveKnownCreator(object userData)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SpectatorManager.\u003CBnetParty_OnLostPartyReference_RemoveKnownCreator\u003Ec__AnonStorey451 creatorCAnonStorey451 = new SpectatorManager.\u003CBnetParty_OnLostPartyReference_RemoveKnownCreator\u003Ec__AnonStorey451();
    // ISSUE: reference to a compiler-generated field
    creatorCAnonStorey451.partyId = userData as PartyId;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    if (!(creatorCAnonStorey451.partyId != (PartyId) null) || BnetParty.IsInParty(creatorCAnonStorey451.partyId) || ((IEnumerable<PartyInvite>) BnetParty.GetReceivedInvites()).Any<PartyInvite>(new Func<PartyInvite, bool>(creatorCAnonStorey451.\u003C\u003Em__311)))
      return;
    // ISSUE: reference to a compiler-generated field
    SpectatorManager.Get().m_knownPartyCreatorIds.Remove(creatorCAnonStorey451.partyId);
  }

  private void BnetParty_OnReceivedInvite(OnlineEventType evt, PartyInfo party, ulong inviteId, InviteRemoveReason? reason)
  {
    if (!this.m_initialized || party.Type != PartyType.SPECTATOR_PARTY)
      return;
    PartyInvite receivedInvite = BnetParty.GetReceivedInvite(inviteId);
    BnetPlayer inviter = receivedInvite != null ? BnetUtils.GetPlayer(receivedInvite.InviterId) : (BnetPlayer) null;
    bool flag1 = true;
    if (evt == OnlineEventType.ADDED)
    {
      if (receivedInvite == null)
        return;
      if (receivedInvite.IsRejoin || this.ShouldBePartyLeader(receivedInvite.PartyId))
      {
        BnetGameAccountId partyCreator = this.GetPartyCreator(receivedInvite.PartyId);
        this.LogInfoParty("Spectator_OnReceivedInvite rejoin={0} partyId={1} creatorId={2}", (object) receivedInvite.IsRejoin, (object) receivedInvite.PartyId, (object) partyCreator);
        bool flag2 = false;
        if (this.ShouldBePartyLeader(receivedInvite.PartyId))
        {
          BnetParty.AcceptReceivedInvite(inviteId);
          flag1 = false;
        }
        else if (this.m_spectatorPartyIdMain != (PartyId) null)
        {
          if (this.m_spectatorPartyIdMain == receivedInvite.PartyId)
          {
            BnetParty.AcceptReceivedInvite(inviteId);
            flag1 = false;
          }
          else
            flag2 = true;
        }
        else
        {
          flag2 = true;
          if ((BnetEntityId) partyCreator != (BnetEntityId) null && (BnetEntityId) this.m_spectateeFriendlySide == (BnetEntityId) null)
          {
            flag2 = false;
            this.m_spectateeFriendlySide = partyCreator;
            BnetParty.AcceptReceivedInvite(inviteId);
            flag1 = false;
          }
        }
        if (flag2)
          BnetParty.DeclineReceivedInvite(inviteId);
      }
      else if ((BnetEntityId) receivedInvite.InviterId == (BnetEntityId) this.m_spectateeFriendlySide || (BnetEntityId) receivedInvite.InviterId == (BnetEntityId) this.m_spectateeOpposingSide || this.m_requestedInvite != null && this.m_requestedInvite.PartyId == receivedInvite.PartyId)
      {
        BnetParty.AcceptReceivedInvite(inviteId);
        flag1 = false;
        if (this.m_requestedInvite != null)
        {
          this.m_requestedInvite = (SpectatorManager.IntendedSpectateeParty) null;
          ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.SpectatePlayer_RequestInvite_FriendlySide_Timeout), (object) null);
          ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.SpectatePlayer_RequestInvite_OpposingSide_Timeout), (object) null);
        }
      }
      else if (!UserAttentionManager.CanShowAttentionGrabber("SpectatorManager.BnetParty_OnReceivedInvite:" + (object) evt))
      {
        BnetParty.DeclineReceivedInvite(inviteId);
      }
      else
      {
        if (this.m_kickedFromSpectatingList != null)
          this.m_kickedFromSpectatingList.Remove(receivedInvite.InviterId);
        if ((UnityEngine.Object) SocialToastMgr.Get() != (UnityEngine.Object) null)
          SocialToastMgr.Get().AddToast(UserAttentionBlocker.NONE, BnetUtils.GetInviterBestName(receivedInvite), SocialToastMgr.TOAST_TYPE.SPECTATOR_INVITE_RECEIVED);
      }
    }
    else if (evt == OnlineEventType.REMOVED && (!reason.HasValue || reason.Value == InviteRemoveReason.ACCEPTED) && (UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
      ApplicationMgr.Get().ScheduleCallback(0.5f, false, new ApplicationMgr.ScheduledCallback(this.BnetParty_OnLostPartyReference_RemoveKnownCreator), (object) party.Id);
    if (!flag1 || this.OnInviteReceived == null)
      return;
    this.OnInviteReceived(evt, inviter);
  }

  private void BnetParty_OnSentInvite(OnlineEventType evt, PartyInfo party, ulong inviteId, bool senderIsMyself, InviteRemoveReason? reason)
  {
    if (party.Type != PartyType.SPECTATOR_PARTY || !senderIsMyself)
      return;
    PartyInvite sentInvite = BnetParty.GetSentInvite(party.Id, inviteId);
    BnetPlayer invitee = sentInvite != null ? BnetUtils.GetPlayer(sentInvite.InviteeId) : (BnetPlayer) null;
    if (evt == OnlineEventType.ADDED)
    {
      bool flag = false;
      if (this.m_userInitiatedOutgoingInvites != null && sentInvite != null)
        flag = this.m_userInitiatedOutgoingInvites.Remove(sentInvite.InviteeId);
      if (flag && sentInvite != null && (this.ShouldBePartyLeader(party.Id) && !this.m_gameServerKnownSpectators.Contains(sentInvite.InviteeId)) && (UnityEngine.Object) SocialToastMgr.Get() != (UnityEngine.Object) null)
        SocialToastMgr.Get().AddToast(UserAttentionBlocker.NONE, BnetUtils.GetPlayerBestName(sentInvite.InviteeId), SocialToastMgr.TOAST_TYPE.SPECTATOR_INVITE_SENT);
    }
    if (sentInvite == null || this.m_gameServerKnownSpectators.Contains(sentInvite.InviteeId) || this.OnInviteSent == null)
      return;
    this.OnInviteSent(evt, invitee);
  }

  private void BnetParty_OnReceivedInviteRequest(OnlineEventType evt, PartyInfo party, InviteRequest request, InviteRequestRemovedReason? reason)
  {
    if (party.Type != PartyType.SPECTATOR_PARTY || evt != OnlineEventType.ADDED)
      return;
    bool flag = false;
    if (party.Id != this.m_spectatorPartyIdMain)
      flag = true;
    if ((BnetEntityId) request.RequesterId != (BnetEntityId) null && (BnetEntityId) request.RequesterId == (BnetEntityId) request.TargetId && !Options.Get().GetBool(Option.SPECTATOR_OPEN_JOIN))
      flag = true;
    if (!BnetFriendMgr.Get().IsFriend(request.RequesterId))
      flag = true;
    if (!BnetFriendMgr.Get().IsFriend(request.TargetId))
      flag = true;
    if (this.m_kickedPlayers != null && (this.m_kickedPlayers.Contains(request.RequesterId) || this.m_kickedPlayers.Contains(request.TargetId)))
      flag = true;
    if (flag)
      BnetParty.IgnoreInviteRequest(party.Id, request.TargetId);
    else
      BnetParty.AcceptInviteRequest(party.Id, request.TargetId, false);
  }

  private void BnetParty_OnMemberEvent(OnlineEventType evt, PartyInfo party, BnetGameAccountId memberId, bool isRolesUpdate, LeaveReason? reason)
  {
    if (party.Id == (PartyId) null || party.Id != this.m_spectatorPartyIdMain && party.Id != this.m_spectatorPartyIdOpposingSide)
      return;
    if (evt == OnlineEventType.ADDED && BnetParty.IsLeader(party.Id))
    {
      BnetGameAccountId partyCreator = this.GetPartyCreator(party.Id);
      if ((BnetEntityId) partyCreator != (BnetEntityId) null && (BnetEntityId) partyCreator == (BnetEntityId) memberId)
        BnetParty.SetLeader(party.Id, memberId);
    }
    if (!this.m_initialized || evt == OnlineEventType.UPDATED || (!((BnetEntityId) memberId != (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId()) || !this.ShouldBePartyLeader(party.Id)) || SceneMgr.Get().IsInGame() && Network.IsConnectedToGameServer() && this.m_gameServerKnownSpectators.Contains(memberId))
      return;
    SocialToastMgr.TOAST_TYPE toastType = evt != OnlineEventType.ADDED ? SocialToastMgr.TOAST_TYPE.SPECTATOR_REMOVED : SocialToastMgr.TOAST_TYPE.SPECTATOR_ADDED;
    ApplicationMgr.Get().StartCoroutine(this.WaitForPresenceThenToast(memberId, toastType));
    if (this.OnSpectatorToMyGame == null)
      return;
    BnetPlayer player = BnetUtils.GetPlayer(memberId);
    this.OnSpectatorToMyGame(evt, player);
  }

  private void BnetParty_OnChatMessage(PartyInfo party, BnetGameAccountId speakerId, string chatMessage)
  {
  }

  private void BnetParty_OnPartyAttributeChanged_ServerInfo(PartyInfo party, string attributeKey, bnet.protocol.attribute.Variant value)
  {
    if (party.Type != PartyType.SPECTATOR_PARTY || value == null)
      return;
    byte[] bytes = !value.HasBlobValue ? (byte[]) null : value.BlobValue;
    if (bytes == null)
      return;
    PartyServerInfo from = ProtobufUtil.ParseFrom<PartyServerInfo>(bytes, 0, -1);
    if (from == null)
      return;
    if (!from.HasSecretKey || string.IsNullOrEmpty(from.SecretKey))
    {
      this.LogInfoParty("BnetParty_OnPartyAttributeChanged_ServerInfo: no secret key in serverInfo.");
    }
    else
    {
      bgs.types.GameServerInfo gameServerJoined = Network.Get().GetLastGameServerJoined();
      bool flag = Network.IsConnectedToGameServer() && SpectatorManager.IsSameGameAndServer(from, gameServerJoined);
      if (!flag && SceneMgr.Get().IsInGame())
      {
        this.LogInfoParty("BnetParty_OnPartyAttributeChanged_ServerInfo: cannot join game while in gameplay new={0} curr={1}.", (object) from.GameHandle, (object) gameServerJoined.GameHandle);
      }
      else
      {
        JoinInfo joinInfo = SpectatorManager.CreateJoinInfo(from);
        if (party.Id == this.m_spectatorPartyIdOpposingSide)
        {
          if (!((UnityEngine.Object) GameMgr.Get().GetTransitionPopup() == (UnityEngine.Object) null) || !GameMgr.Get().IsSpectator())
            return;
          this.SpectateSecondPlayer_Network(joinInfo);
        }
        else
        {
          if (flag || !(party.Id == this.m_spectatorPartyIdMain))
            return;
          this.LogInfoPower("================== Start Spectator Game ==================");
          this.m_isExpectingArriveInGameplayAsSpectator = true;
          GameMgr.Get().SpectateGame(joinInfo);
          this.CloseWaitingForNextGameDialog();
        }
      }
    }
  }

  private void LogInfoParty(string format, params object[] args)
  {
    Log.Party.Print(format, args);
  }

  private void LogInfoPower(string format, params object[] args)
  {
    Log.Party.Print(format, args);
    Log.Power.Print(format, args);
  }

  private bool IsPlayerInGame(BnetGameAccountId gameAccountId)
  {
    GameState gameState = GameState.Get();
    if (gameState == null)
      return false;
    using (Map<int, Player>.Enumerator enumerator = gameState.GetPlayerMap().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BnetPlayer bnetPlayer = enumerator.Current.Value.GetBnetPlayer();
        if (bnetPlayer != null && (BnetEntityId) bnetPlayer.GetHearthstoneGameAccountId() == (BnetEntityId) gameAccountId)
          return true;
      }
    }
    return false;
  }

  private bool IsStillInParty(PartyId partyId)
  {
    return BnetParty.IsInParty(partyId) && (this.m_leavePartyIdsRequested == null || !this.m_leavePartyIdsRequested.Contains(partyId));
  }

  private void PruneOldInvites()
  {
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    List<BnetGameAccountId> bnetGameAccountIdList = new List<BnetGameAccountId>();
    using (Map<BnetGameAccountId, SpectatorManager.ReceivedInvite>.Enumerator enumerator = this.m_receivedSpectateMeInvites.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<BnetGameAccountId, SpectatorManager.ReceivedInvite> current = enumerator.Current;
        float timestamp = current.Value.m_timestamp;
        if ((double) (realtimeSinceStartup - timestamp) > 300.0)
          bnetGameAccountIdList.Add(current.Key);
      }
    }
    using (List<BnetGameAccountId>.Enumerator enumerator = bnetGameAccountIdList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.RemoveReceivedInvitation(enumerator.Current);
    }
    bnetGameAccountIdList.Clear();
    using (Map<BnetGameAccountId, float>.Enumerator enumerator = this.m_sentSpectateMeInvites.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<BnetGameAccountId, float> current = enumerator.Current;
        float num = current.Value;
        if ((double) (realtimeSinceStartup - num) > 30.0)
          bnetGameAccountIdList.Add(current.Key);
      }
    }
    using (List<BnetGameAccountId>.Enumerator enumerator = bnetGameAccountIdList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.RemoveSentInvitation(enumerator.Current);
    }
  }

  private void AddReceivedInvitation(BnetGameAccountId inviterId, JoinInfo joinInfo)
  {
    bool flag = !this.m_receivedSpectateMeInvites.ContainsKey(inviterId);
    SpectatorManager.ReceivedInvite receivedInvite = new SpectatorManager.ReceivedInvite(joinInfo);
    this.m_receivedSpectateMeInvites[inviterId] = receivedInvite;
    if (flag)
    {
      BnetPlayer player = BnetUtils.GetPlayer(inviterId);
      if ((UnityEngine.Object) SocialToastMgr.Get() != (UnityEngine.Object) null)
        SocialToastMgr.Get().AddToast(UserAttentionBlocker.NONE, BnetUtils.GetPlayerBestName(inviterId), SocialToastMgr.TOAST_TYPE.SPECTATOR_INVITE_RECEIVED);
      if (this.OnInviteReceived != null)
        this.OnInviteReceived(OnlineEventType.ADDED, player);
    }
    float secondsToWait = Mathf.Max(0.0f, this.m_receivedSpectateMeInvites.Min<KeyValuePair<BnetGameAccountId, SpectatorManager.ReceivedInvite>>((Func<KeyValuePair<BnetGameAccountId, SpectatorManager.ReceivedInvite>, float>) (kv => kv.Value.m_timestamp)) + 300f - Time.realtimeSinceStartup);
    ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.ReceivedInvitation_ExpireTimeout), (object) null);
    ApplicationMgr.Get().ScheduleCallback(secondsToWait, true, new ApplicationMgr.ScheduledCallback(this.ReceivedInvitation_ExpireTimeout), (object) null);
  }

  private void RemoveReceivedInvitation(BnetGameAccountId inviterId)
  {
    if ((BnetEntityId) inviterId == (BnetEntityId) null || !this.m_receivedSpectateMeInvites.Remove(inviterId))
      return;
    BnetPlayer player = BnetUtils.GetPlayer(inviterId);
    if (this.OnInviteReceived == null)
      return;
    this.OnInviteReceived(OnlineEventType.REMOVED, player);
  }

  private void ClearAllReceivedInvitations()
  {
    BnetGameAccountId[] array = this.m_receivedSpectateMeInvites.Keys.ToArray<BnetGameAccountId>();
    this.m_receivedSpectateMeInvites.Clear();
    if (this.OnInviteReceived == null)
      return;
    foreach (BnetGameAccountId id in array)
      this.OnInviteReceived(OnlineEventType.REMOVED, BnetUtils.GetPlayer(id));
  }

  private void AddSentInvitation(BnetGameAccountId inviteeId)
  {
    if ((BnetEntityId) inviteeId == (BnetEntityId) null)
      return;
    bool flag = !this.m_sentSpectateMeInvites.ContainsKey(inviteeId);
    this.m_sentSpectateMeInvites[inviteeId] = Time.realtimeSinceStartup;
    if (!flag)
      return;
    BnetPlayer player = BnetUtils.GetPlayer(inviteeId);
    if (this.OnInviteSent == null)
      return;
    this.OnInviteSent(OnlineEventType.ADDED, player);
  }

  private void RemoveSentInvitation(BnetGameAccountId inviteeId)
  {
    if ((BnetEntityId) inviteeId == (BnetEntityId) null || !this.m_sentSpectateMeInvites.Remove(inviteeId))
      return;
    BnetPlayer player = BnetUtils.GetPlayer(inviteeId);
    if (this.OnInviteSent == null)
      return;
    this.OnInviteSent(OnlineEventType.REMOVED, player);
  }

  private void DeclineAllReceivedInvitations()
  {
    foreach (PartyInvite receivedInvite in BnetParty.GetReceivedInvites())
    {
      if (receivedInvite.PartyType == PartyType.SPECTATOR_PARTY)
        BnetParty.DeclineReceivedInvite(receivedInvite.InviteId);
    }
  }

  private void RevokeAllSentInvitations()
  {
    this.ClearAllSentInvitations();
    BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
    PartyId[] partyIdArray = new PartyId[2]{ this.m_spectatorPartyIdMain, this.m_spectatorPartyIdOpposingSide };
    foreach (PartyId partyId in partyIdArray)
    {
      if (!(partyId == (PartyId) null))
      {
        foreach (PartyInvite sentInvite in BnetParty.GetSentInvites(partyId))
        {
          if (!((BnetEntityId) sentInvite.InviterId != (BnetEntityId) myGameAccountId))
            BnetParty.RevokeSentInvite(partyId, sentInvite.InviteId);
        }
      }
    }
  }

  private void ClearAllSentInvitations()
  {
    BnetGameAccountId[] array = this.m_sentSpectateMeInvites.Keys.ToArray<BnetGameAccountId>();
    this.m_sentSpectateMeInvites.Clear();
    if (this.OnInviteSent == null)
      return;
    foreach (BnetGameAccountId id in array)
      this.OnInviteSent(OnlineEventType.REMOVED, BnetUtils.GetPlayer(id));
  }

  private void AddKnownSpectator(BnetGameAccountId gameAccountId)
  {
    if ((BnetEntityId) gameAccountId == (BnetEntityId) null)
      return;
    bool flag1 = this.m_gameServerKnownSpectators.Add(gameAccountId);
    this.CreatePartyIfNecessary();
    this.RemoveSentInvitation(gameAccountId);
    this.RemoveReceivedInvitation(gameAccountId);
    if (!flag1)
      return;
    if (SceneMgr.Get().IsInGame() && Network.IsConnectedToGameServer())
    {
      bool flag2 = BnetParty.IsMember(this.m_spectatorPartyIdMain, gameAccountId);
      BnetPlayer player = BnetUtils.GetPlayer(gameAccountId);
      if (!flag2)
        ApplicationMgr.Get().StartCoroutine(this.WaitForPresenceThenToast(gameAccountId, SocialToastMgr.TOAST_TYPE.SPECTATOR_ADDED));
      if (this.OnSpectatorToMyGame != null)
        this.OnSpectatorToMyGame(OnlineEventType.ADDED, player);
    }
    this.UpdateSpectatorPresence();
  }

  private void RemoveKnownSpectator(BnetGameAccountId gameAccountId)
  {
    if ((BnetEntityId) gameAccountId == (BnetEntityId) null || !this.m_gameServerKnownSpectators.Remove(gameAccountId))
      return;
    if (SceneMgr.Get().IsInGame() && Network.IsConnectedToGameServer())
    {
      bool flag = BnetParty.IsMember(this.m_spectatorPartyIdMain, gameAccountId);
      BnetPlayer player = BnetUtils.GetPlayer(gameAccountId);
      if (!flag)
        ApplicationMgr.Get().StartCoroutine(this.WaitForPresenceThenToast(gameAccountId, SocialToastMgr.TOAST_TYPE.SPECTATOR_REMOVED));
      if (this.OnSpectatorToMyGame != null)
        this.OnSpectatorToMyGame(OnlineEventType.REMOVED, player);
    }
    this.UpdateSpectatorPresence();
  }

  private void ClearAllGameServerKnownSpectators()
  {
    BnetGameAccountId[] array = this.m_gameServerKnownSpectators.ToArray<BnetGameAccountId>();
    this.m_gameServerKnownSpectators.Clear();
    if (this.OnSpectatorToMyGame != null && SceneMgr.Get().IsInGame() && Network.IsConnectedToGameServer())
    {
      foreach (BnetGameAccountId id in array)
        this.OnSpectatorToMyGame(OnlineEventType.REMOVED, BnetUtils.GetPlayer(id));
    }
    if (array.Length <= 0)
      return;
    this.UpdateSpectatorPresence();
  }

  private void UpdateSpectatorPresence()
  {
    if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
    {
      ApplicationMgr.Get().CancelScheduledCallback(new ApplicationMgr.ScheduledCallback(this.SpectatorManager_UpdatePresenceNextFrame), (object) null);
      ApplicationMgr.Get().ScheduleCallback(0.0f, true, new ApplicationMgr.ScheduledCallback(this.SpectatorManager_UpdatePresenceNextFrame), (object) null);
    }
    else
      this.SpectatorManager_UpdatePresenceNextFrame((object) null);
  }

  private void SpectatorManager_UpdatePresenceNextFrame(object userData)
  {
    JoinInfo joinInfo = (JoinInfo) null;
    if (Options.Get().GetBool(Option.SPECTATOR_OPEN_JOIN) || this.IsInSpectatorMode())
      joinInfo = this.GetMyGameJoinInfo();
    if (!Network.ShouldBeConnectedToAurora())
      return;
    BnetPresenceMgr.Get().SetGameFieldBlob(21U, (IProtoBuf) joinInfo);
  }

  private void UpdateSpectatorPartyServerInfo()
  {
    if (this.m_spectatorPartyIdMain == (PartyId) null)
      return;
    if (!this.ShouldBePartyLeader(this.m_spectatorPartyIdMain))
    {
      if (!BnetParty.IsLeader(this.m_spectatorPartyIdMain))
        return;
      BnetParty.ClearPartyAttribute(this.m_spectatorPartyIdMain, "WTCG.Party.ServerInfo");
    }
    else
    {
      byte[] partyAttributeBlob = BnetParty.GetPartyAttributeBlob(this.m_spectatorPartyIdMain, "WTCG.Party.ServerInfo");
      bgs.types.GameServerInfo gameServerJoined = Network.Get().GetLastGameServerJoined();
      if (SpectatorManager.IsGameOver || !SceneMgr.Get().IsInGame() || (!Network.IsConnectedToGameServer() || gameServerJoined == null) || string.IsNullOrEmpty(gameServerJoined.Address))
      {
        if (partyAttributeBlob == null)
          return;
        BnetParty.ClearPartyAttribute(this.m_spectatorPartyIdMain, "WTCG.Party.ServerInfo");
      }
      else
      {
        byte[] byteArray = ProtobufUtil.ToByteArray((IProtoBuf) new PartyServerInfo() { ServerIpAddress = gameServerJoined.Address, ServerPort = (uint) gameServerJoined.Port, GameHandle = gameServerJoined.GameHandle, SecretKey = (gameServerJoined.SpectatorPassword ?? string.Empty), GameType = GameMgr.Get().GetGameType(), FormatType = GameMgr.Get().GetFormatType(), MissionId = GameMgr.Get().GetMissionId() });
        if (GeneralUtils.AreArraysEqual<byte>(byteArray, partyAttributeBlob))
          return;
        BnetParty.SetPartyAttributeBlob(this.m_spectatorPartyIdMain, "WTCG.Party.ServerInfo", byteArray);
      }
    }
  }

  private bool ShouldBePartyLeader(PartyId partyId)
  {
    if (GameMgr.Get().IsSpectator() || (BnetEntityId) this.m_spectateeFriendlySide != (BnetEntityId) null || (BnetEntityId) this.m_spectateeOpposingSide != (BnetEntityId) null)
      return false;
    BnetGameAccountId partyCreator = this.GetPartyCreator(partyId);
    return !((BnetEntityId) partyCreator == (BnetEntityId) null) && !((BnetEntityId) partyCreator != (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId());
  }

  private BnetGameAccountId GetPartyCreator(PartyId partyId)
  {
    if (partyId == (PartyId) null)
      return (BnetGameAccountId) null;
    BnetGameAccountId bnetGameAccountId = (BnetGameAccountId) null;
    if (this.m_knownPartyCreatorIds.TryGetValue(partyId, out bnetGameAccountId) && (BnetEntityId) bnetGameAccountId != (BnetEntityId) null)
      return bnetGameAccountId;
    byte[] partyAttributeBlob = BnetParty.GetPartyAttributeBlob(partyId, "WTCG.Party.Creator");
    if (partyAttributeBlob == null)
      return (BnetGameAccountId) null;
    bnetGameAccountId = BnetUtils.CreateGameAccountId(ProtobufUtil.ParseFrom<BnetId>(partyAttributeBlob, 0, -1));
    if (bnetGameAccountId.IsValid())
      this.m_knownPartyCreatorIds[partyId] = bnetGameAccountId;
    return bnetGameAccountId;
  }

  private bool CreatePartyIfNecessary()
  {
    if (this.m_spectatorPartyIdMain != (PartyId) null)
    {
      if ((BnetEntityId) this.GetPartyCreator(this.m_spectatorPartyIdMain) != (BnetEntityId) null && !this.ShouldBePartyLeader(this.m_spectatorPartyIdMain))
        return false;
      PartyInfo[] joinedParties = BnetParty.GetJoinedParties();
      if (((IEnumerable<PartyInfo>) joinedParties).FirstOrDefault<PartyInfo>((Func<PartyInfo, bool>) (i =>
      {
        if (i.Id == this.m_spectatorPartyIdMain)
          return i.Type == PartyType.SPECTATOR_PARTY;
        return false;
      })) == null)
      {
        this.LogInfoParty("CreatePartyIfNecessary stored PartyId={0} is not in joined party list: {1}", (object) this.m_spectatorPartyIdMain, (object) string.Join(", ", ((IEnumerable<PartyInfo>) joinedParties).Select<PartyInfo, string>((Func<PartyInfo, string>) (i => i.ToString())).ToArray<string>()));
        this.m_spectatorPartyIdMain = (PartyId) null;
        this.UpdateSpectatorPresence();
      }
      PartyInfo partyInfo = ((IEnumerable<PartyInfo>) joinedParties).FirstOrDefault<PartyInfo>((Func<PartyInfo, bool>) (i => i.Type == PartyType.SPECTATOR_PARTY));
      if (partyInfo != null && this.m_spectatorPartyIdMain != partyInfo.Id)
      {
        this.LogInfoParty("CreatePartyIfNecessary repairing mismatching PartyIds current={0} new={1}", (object) this.m_spectatorPartyIdMain, (object) partyInfo.Id);
        this.m_spectatorPartyIdMain = partyInfo.Id;
        this.UpdateSpectatorPresence();
      }
      if (this.m_spectatorPartyIdMain != (PartyId) null)
        return false;
    }
    if (this.GetCountSpectatingMe() <= 0)
      return false;
    BnetParty.CreateParty(PartyType.SPECTATOR_PARTY, PrivacyLevel.OPEN_INVITATION, ProtobufUtil.ToByteArray((IProtoBuf) BnetUtils.CreatePegasusBnetId((BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId())), (BnetParty.CreateSuccessCallback) null);
    return true;
  }

  private void ReinviteKnownSpectatorsNotInParty()
  {
    if (this.m_spectatorPartyIdMain == (PartyId) null || !this.ShouldBePartyLeader(this.m_spectatorPartyIdMain))
      return;
    bgs.PartyMember[] members = BnetParty.GetMembers(this.m_spectatorPartyIdMain);
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SpectatorManager.\u003CReinviteKnownSpectatorsNotInParty\u003Ec__AnonStorey452 partyCAnonStorey452 = new SpectatorManager.\u003CReinviteKnownSpectatorsNotInParty\u003Ec__AnonStorey452();
    using (HashSet<BnetGameAccountId>.Enumerator enumerator = this.m_gameServerKnownSpectators.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        partyCAnonStorey452.knownSpectator = enumerator.Current;
        // ISSUE: reference to a compiler-generated method
        if (((IEnumerable<bgs.PartyMember>) members).FirstOrDefault<bgs.PartyMember>(new Func<bgs.PartyMember, bool>(partyCAnonStorey452.\u003C\u003Em__316)) == null)
        {
          // ISSUE: reference to a compiler-generated field
          BnetParty.SendInvite(this.m_spectatorPartyIdMain, partyCAnonStorey452.knownSpectator, false);
        }
      }
    }
  }

  private void LeaveParty(PartyId partyId, bool dissolve)
  {
    if (partyId == (PartyId) null)
      return;
    if (this.m_leavePartyIdsRequested == null)
      this.m_leavePartyIdsRequested = new HashSet<PartyId>();
    this.m_leavePartyIdsRequested.Add(partyId);
    if (dissolve)
      BnetParty.DissolveParty(partyId);
    else
      BnetParty.Leave(partyId);
  }

  private void LeaveGameScene()
  {
    if ((UnityEngine.Object) EndGameScreen.Get() != (UnityEngine.Object) null)
    {
      EndGameScreen.Get().m_hitbox.TriggerPress();
      EndGameScreen.Get().m_hitbox.TriggerRelease();
    }
    else
    {
      if (ApplicationMgr.Get().IsResetting())
        return;
      SceneMgr.Get().SetNextMode(GameMgr.Get().GetPostGameSceneMode());
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitForPresenceThenToast(BnetGameAccountId gameAccountId, SocialToastMgr.TOAST_TYPE toastType)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpectatorManager.\u003CWaitForPresenceThenToast\u003Ec__Iterator313() { gameAccountId = gameAccountId, toastType = toastType, \u003C\u0024\u003EgameAccountId = gameAccountId, \u003C\u0024\u003EtoastType = toastType };
  }

  private static SpectatorManager CreateInstance()
  {
    SpectatorManager.s_instance = new SpectatorManager();
    ApplicationMgr.Get().WillReset += new System.Action(SpectatorManager.s_instance.WillReset);
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(SpectatorManager.s_instance.OnFindGameEvent));
    SceneMgr.Get().RegisterSceneUnloadedEvent(new SceneMgr.SceneUnloadedCallback(SpectatorManager.s_instance.OnSceneUnloaded));
    GameState.RegisterGameStateInitializedListener(new GameState.GameStateInitializedCallback(SpectatorManager.s_instance.GameState_InitializedEvent), (object) null);
    Network.Get().SetSpectatorInviteReceivedHandler(new Network.SpectatorInviteReceivedHandler(SpectatorManager.s_instance.Network_OnSpectatorInviteReceived));
    Options.Get().RegisterChangedListener(Option.SPECTATOR_OPEN_JOIN, new Options.ChangedCallback(SpectatorManager.s_instance.OnSpectatorOpenJoinOptionChanged));
    BnetPresenceMgr.Get().OnGameAccountPresenceChange += new System.Action<PresenceUpdate[]>(SpectatorManager.s_instance.Presence_OnGameAccountPresenceChange);
    BnetFriendMgr.Get().AddChangeListener(new BnetFriendMgr.ChangeCallback(SpectatorManager.s_instance.BnetFriendMgr_OnFriendsChanged));
    EndGameScreen.OnTwoScoopsShown += new EndGameScreen.OnTwoScoopsShownHandler(SpectatorManager.s_instance.EndGameScreen_OnTwoScoopsShown);
    Network.Get().RegisterNetHandler((object) SpectatorNotify.PacketID.ID, new Network.NetHandler(SpectatorManager.s_instance.Network_OnSpectatorNotifyEvent), (Network.TimeoutHandler) null);
    BnetParty.OnError += new BnetParty.PartyErrorHandler(SpectatorManager.s_instance.BnetParty_OnError);
    BnetParty.OnJoined += new BnetParty.JoinedHandler(SpectatorManager.s_instance.BnetParty_OnJoined);
    BnetParty.OnReceivedInvite += new BnetParty.ReceivedInviteHandler(SpectatorManager.s_instance.BnetParty_OnReceivedInvite);
    BnetParty.OnSentInvite += new BnetParty.SentInviteHandler(SpectatorManager.s_instance.BnetParty_OnSentInvite);
    BnetParty.OnReceivedInviteRequest += new BnetParty.ReceivedInviteRequestHandler(SpectatorManager.s_instance.BnetParty_OnReceivedInviteRequest);
    BnetParty.OnMemberEvent += new BnetParty.MemberEventHandler(SpectatorManager.s_instance.BnetParty_OnMemberEvent);
    BnetParty.OnChatMessage += new BnetParty.ChatMessageHandler(SpectatorManager.s_instance.BnetParty_OnChatMessage);
    BnetParty.RegisterAttributeChangedHandler("WTCG.Party.ServerInfo", new BnetParty.PartyAttributeChangedHandler(SpectatorManager.s_instance.BnetParty_OnPartyAttributeChanged_ServerInfo));
    return SpectatorManager.s_instance;
  }

  private struct ReceivedInvite
  {
    public float m_timestamp;
    public JoinInfo m_joinInfo;

    public ReceivedInvite(JoinInfo joinInfo)
    {
      this.m_timestamp = Time.realtimeSinceStartup;
      this.m_joinInfo = joinInfo;
    }
  }

  private class IntendedSpectateeParty
  {
    public BnetGameAccountId SpectateeId;
    public PartyId PartyId;

    public IntendedSpectateeParty(BnetGameAccountId spectateeId, PartyId partyId)
    {
      this.SpectateeId = spectateeId;
      this.PartyId = partyId;
    }
  }

  private class PendingSpectatePlayer
  {
    public BnetGameAccountId SpectateeId;
    public JoinInfo JoinInfo;

    public PendingSpectatePlayer(BnetGameAccountId spectateeId, JoinInfo joinInfo)
    {
      this.SpectateeId = spectateeId;
      this.JoinInfo = joinInfo;
    }
  }

  public delegate void InviteReceivedHandler(OnlineEventType evt, BnetPlayer inviter);

  public delegate void InviteSentHandler(OnlineEventType evt, BnetPlayer invitee);

  public delegate void SpectatorToMyGameHandler(OnlineEventType evt, BnetPlayer spectator);

  public delegate void SpectatorModeChangedHandler(OnlineEventType evt, BnetPlayer spectatee);
}
