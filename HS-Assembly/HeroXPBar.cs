﻿// Decompiled with JetBrains decompiler
// Type: HeroXPBar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class HeroXPBar : PegUIElement
{
  public ProgressBar m_progressBar;
  public UberText m_heroLevelText;
  public UberText m_barText;
  public GameObject m_simpleFrame;
  public GameObject m_heroFrame;
  public bool m_isAnimated;
  public float m_delay;
  public bool m_isOnDeck;
  public NetCache.HeroLevel m_heroLevel;
  public int m_soloLevelLimit;
  public HeroXPBar.PlayLevelUpEffectCallback m_levelUpCallback;
  private string m_rewardText;

  protected override void Awake()
  {
    base.Awake();
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnProgressBarOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnProgressBarOut));
  }

  public void EmptyLevelUpFunction()
  {
  }

  public void UpdateDisplay()
  {
    if (this.m_isOnDeck)
    {
      this.m_simpleFrame.SetActive(true);
      this.m_heroFrame.SetActive(false);
    }
    else
    {
      this.m_simpleFrame.SetActive(false);
      this.m_heroFrame.SetActive(true);
    }
    this.SetBarText(string.Empty);
    if (this.m_isAnimated)
    {
      this.m_heroLevelText.Text = this.m_heroLevel.PrevLevel.Level.ToString();
      if (this.m_heroLevel.PrevLevel.IsMaxLevel())
      {
        this.SetBarValue(1f);
      }
      else
      {
        this.SetBarValue((float) this.m_heroLevel.PrevLevel.XP / (float) this.m_heroLevel.PrevLevel.MaxXP);
        this.StartCoroutine(this.DelayBarAnimation(this.m_heroLevel.PrevLevel, this.m_heroLevel.CurrentLevel));
      }
    }
    else
    {
      this.m_heroLevelText.Text = this.m_heroLevel.CurrentLevel.Level.ToString();
      if (this.m_heroLevel.CurrentLevel.IsMaxLevel())
        this.SetBarValue(1f);
      else
        this.SetBarValue((float) this.m_heroLevel.CurrentLevel.XP / (float) this.m_heroLevel.CurrentLevel.MaxXP);
    }
  }

  public void AnimateBar(NetCache.HeroLevel.LevelInfo previousLevelInfo, NetCache.HeroLevel.LevelInfo currentLevelInfo)
  {
    this.m_heroLevelText.Text = previousLevelInfo.Level.ToString();
    if (previousLevelInfo.Level < currentLevelInfo.Level)
    {
      this.m_progressBar.AnimateProgress((float) previousLevelInfo.XP / (float) previousLevelInfo.MaxXP, 1f);
      this.StartCoroutine(this.AnimatePostLevelUpXp(this.m_progressBar.GetAnimationTime(), currentLevelInfo));
    }
    else
    {
      float prevVal = (float) previousLevelInfo.XP / (float) previousLevelInfo.MaxXP;
      float currVal = (float) currentLevelInfo.XP / (float) currentLevelInfo.MaxXP;
      if (currentLevelInfo.IsMaxLevel())
        currVal = 1f;
      this.m_progressBar.AnimateProgress(prevVal, currVal);
    }
  }

  public void SetBarValue(float barValue)
  {
    this.m_progressBar.SetProgressBar(barValue);
  }

  public void SetBarText(string barText)
  {
    if (!((Object) this.m_barText != (Object) null))
      return;
    this.m_barText.Text = barText;
  }

  [DebuggerHidden]
  private IEnumerator AnimatePostLevelUpXp(float delayTime, NetCache.HeroLevel.LevelInfo currentLevelInfo)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroXPBar.\u003CAnimatePostLevelUpXp\u003Ec__Iterator30E() { delayTime = delayTime, currentLevelInfo = currentLevelInfo, \u003C\u0024\u003EdelayTime = delayTime, \u003C\u0024\u003EcurrentLevelInfo = currentLevelInfo, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator DelayBarAnimation(NetCache.HeroLevel.LevelInfo prevInfo, NetCache.HeroLevel.LevelInfo currInfo)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroXPBar.\u003CDelayBarAnimation\u003Ec__Iterator30F() { prevInfo = prevInfo, currInfo = currInfo, \u003C\u0024\u003EprevInfo = prevInfo, \u003C\u0024\u003EcurrInfo = currInfo, \u003C\u003Ef__this = this };
  }

  private void ShowTooltip()
  {
    TooltipZone component = this.gameObject.GetComponent<TooltipZone>();
    float scale = !SceneMgr.Get().IsInGame() ? (!(bool) UniversalInputManager.UsePhoneUI ? (float) TooltipPanel.COLLECTION_MANAGER_SCALE : (float) TooltipPanel.BOX_SCALE) : (float) TooltipPanel.MULLIGAN_SCALE;
    if ((bool) UniversalInputManager.UsePhoneUI)
      scale *= 1.1f;
    component.ShowTooltip(GameStrings.Format("GLOBAL_HERO_LEVEL_NEXT_REWARD_TITLE", (object) this.m_heroLevel.NextReward.Level), this.m_rewardText, scale, 1 != 0);
  }

  private void OnProgressBarOver(UIEvent e)
  {
    if (this.m_heroLevel == null || this.m_heroLevel.NextReward == null || this.m_heroLevel.NextReward.Reward == null)
      return;
    this.m_rewardText = RewardUtils.GetRewardText(this.m_heroLevel.NextReward.Reward);
    this.ShowTooltip();
  }

  private void OnProgressBarOut(UIEvent e)
  {
    this.gameObject.GetComponent<TooltipZone>().HideTooltip();
  }

  public delegate void PlayLevelUpEffectCallback();
}
