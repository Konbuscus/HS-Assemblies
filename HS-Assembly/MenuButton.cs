﻿// Decompiled with JetBrains decompiler
// Type: MenuButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MenuButton : PegUIElement
{
  public TextMesh m_label;

  public void SetText(string s)
  {
    this.m_label.text = s;
  }
}
