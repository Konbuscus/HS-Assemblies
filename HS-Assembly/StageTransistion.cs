﻿// Decompiled with JetBrains decompiler
// Type: StageTransistion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class StageTransistion : MonoBehaviour
{
  public float FxEmitterAKillTime = 1f;
  public float RayTime = 10f;
  public float fxATime = 1f;
  private bool fxmovefwd = true;
  public float FxEmitterAWaitTime = 1f;
  public float FxEmitterATimer = 2f;
  public GameObject hlBase;
  public GameObject hlEdge;
  public GameObject entireObj;
  public GameObject inplayObj;
  public GameObject rays;
  public GameObject flash;
  public GameObject fxEmitterA;
  public GameObject fxEmitterB;
  private Shader shaderBucket;
  private bool colorchange;
  private bool powerchange;
  private bool amountchange;
  private bool turnon;
  private bool rayschange;
  private bool flashchange;
  public Color endColor;
  public Color flashendColor;
  private int stage;
  private bool FxStartAnim;
  private bool FxStartStop;
  private bool fxEmitterAScale;
  private bool raysdone;

  private void Start()
  {
    this.rays.SetActive(false);
    this.flash.SetActive(false);
    this.entireObj.SetActive(true);
    this.inplayObj.SetActive(false);
    this.hlBase.GetComponent<Renderer>().material.SetFloat("_Amount", 0.0f);
    this.hlEdge.GetComponent<Renderer>().material.SetFloat("_Amount", 0.0f);
  }

  private void OnGUI()
  {
    if (!Event.current.isKey)
      return;
    this.amountchange = true;
  }

  private void OnMouseEnter()
  {
    if (this.FxStartAnim)
      return;
    this.StopCoroutine("fxOnExit");
    this.FxStartStop = false;
    this.FxStartAnim = true;
    this.fxmovefwd = true;
    this.StartCoroutine(this.fxStartEnd());
    this.fxEmitterA.GetComponent<ParticleEmitter>().emit = true;
    this.powerchange = true;
    this.fxEmitterAScale = true;
  }

  private void OnMouseExit()
  {
    if (this.FxStartStop)
      return;
    this.StopCoroutine("fxStartEnd");
    this.FxStartAnim = false;
    this.FxStartStop = true;
    this.fxmovefwd = false;
    this.fxEmitterA.GetComponent<ParticleEmitter>().emit = true;
    this.StartCoroutine(this.fxOnExit());
  }

  private void OnMouseDown()
  {
    switch (this.stage)
    {
      case 0:
        this.ManaUse();
        break;
      case 1:
        this.RaysOn();
        break;
    }
    ++this.stage;
  }

  private void RaysOn()
  {
    this.rays.SetActive(true);
    this.flash.SetActive(true);
    this.rayschange = true;
  }

  [DebuggerHidden]
  private IEnumerator destroyParticle()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StageTransistion.\u003CdestroyParticle\u003Ec__Iterator341() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator fxStartEnd()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StageTransistion.\u003CfxStartEnd\u003Ec__Iterator342() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator fxOnExit()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StageTransistion.\u003CfxOnExit\u003Ec__Iterator343() { \u003C\u003Ef__this = this };
  }

  private void OnSelected()
  {
    this.StartCoroutine(this.destroyParticle());
  }

  private void ManaUse()
  {
    this.colorchange = true;
  }

  private void Update()
  {
    if (this.amountchange)
    {
      float num1 = Time.deltaTime / 0.5f;
      float num2 = num1 * 0.6954f;
      float num3 = num1 * 0.6954f;
      UnityEngine.Debug.Log((object) ("amount edge " + (object) (this.hlEdge.GetComponent<Renderer>().material.GetFloat("_Amount") + num3)));
      this.hlBase.GetComponent<Renderer>().material.SetFloat("_Amount", this.hlBase.GetComponent<Renderer>().material.GetFloat("_Amount") + num2);
      if ((double) this.hlBase.GetComponent<Renderer>().material.GetFloat("_Amount") >= 0.69539999961853)
        this.amountchange = false;
      this.hlEdge.GetComponent<Renderer>().material.SetFloat("_Amount", this.hlEdge.GetComponent<Renderer>().material.GetFloat("_Amount") + num3);
    }
    if (this.colorchange)
      this.hlBase.GetComponent<Renderer>().material.color = Color.Lerp(this.hlBase.GetComponent<Renderer>().material.color, this.endColor, Time.deltaTime / 0.5f);
    if (this.powerchange)
    {
      float num1 = Time.deltaTime / 0.5f;
      float num2 = num1 * 18f;
      float num3 = num1 * 0.6954f;
      this.hlBase.GetComponent<Renderer>().material.SetFloat("_power", this.hlBase.GetComponent<Renderer>().material.GetFloat("_power") + num2);
      if ((double) this.hlBase.GetComponent<Renderer>().material.GetFloat("_power") >= 29.0)
        this.powerchange = false;
      this.hlBase.GetComponent<Renderer>().material.SetFloat("_Amount", this.hlBase.GetComponent<Renderer>().material.GetFloat("_Amount") + num3);
      if ((double) this.hlBase.GetComponent<Renderer>().material.GetFloat("_Amount") >= 1.12000000476837)
        this.amountchange = false;
    }
    if (this.rayschange)
    {
      this.rays.transform.localScale += new Vector3(0.0f, Time.deltaTime / 0.5f * this.RayTime, 0.0f);
      if (!this.raysdone && (double) this.rays.transform.localScale.y >= 20.0)
      {
        this.rays.SetActive(false);
        this.GetComponent<Renderer>().enabled = false;
        this.inplayObj.SetActive(true);
        this.inplayObj.GetComponent<Animation>().Play();
        this.fxEmitterB.GetComponent<ParticleEmitter>().emit = true;
        this.fxEmitterA.SetActive(false);
        this.raysdone = true;
      }
    }
    if (this.raysdone)
    {
      float num = this.flash.GetComponent<Renderer>().material.GetFloat("_InvFade") - Time.deltaTime;
      this.flash.GetComponent<Renderer>().material.SetFloat("_InvFade", num);
      UnityEngine.Debug.Log((object) ("InvFade " + (object) num));
      if ((double) num <= 0.00999999977648258)
        this.entireObj.SetActive(false);
    }
    if (this.FxStartAnim || this.FxStartStop)
    {
      if (this.fxmovefwd)
      {
        Particle[] particles = this.fxEmitterA.GetComponent<ParticleEmitter>().particles;
        for (int index = 0; index < particles.Length; ++index)
          particles[index].position += particles[index].position * Time.deltaTime / 0.2f;
        this.fxEmitterA.GetComponent<ParticleEmitter>().particles = particles;
      }
      else
      {
        Particle[] particles = this.fxEmitterA.GetComponent<ParticleEmitter>().particles;
        for (int index = 0; index < particles.Length; ++index)
          particles[index].position -= particles[index].position * Time.deltaTime / 0.2f;
        this.fxEmitterA.GetComponent<ParticleEmitter>().particles = particles;
      }
    }
    if (!this.fxEmitterAScale)
      return;
    float num4 = Time.deltaTime / 0.5f * this.fxATime;
    this.fxEmitterA.transform.localScale += new Vector3(num4, num4, num4);
  }
}
