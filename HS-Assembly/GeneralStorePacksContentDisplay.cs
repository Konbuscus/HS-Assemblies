﻿// Decompiled with JetBrains decompiler
// Type: GeneralStorePacksContentDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class GeneralStorePacksContentDisplay : MonoBehaviour
{
  private static readonly Vector3 PACK_SCALE = new Vector3(0.06f, 0.03f, 0.06f);
  private static readonly float PACK_X_VARIATION_MAG = 0.015f;
  private static readonly float PACK_Y_OFFSET = 0.02f;
  private static readonly float PACK_Z_VARIATION_MAG = 0.01f;
  private static readonly float PACK_FLY_OUT_X_DEG_VARIATION_MAG = 10f;
  private static readonly float PACK_FLY_OUT_Z_DEG_VARIATION_MAG = 10f;
  private static readonly float BOX_FLY_OUT_X_DEG_VARIATION_MAG = 0.0f;
  private static readonly float BOX_FLY_OUT_Z_DEG_VARIATION_MAG = 0.0f;
  private static readonly int PACK_STACK_SEED = 2;
  private static Map<int, AnimatedLowPolyPack> s_packTemplates = new Map<int, AnimatedLowPolyPack>();
  public List<GameObject> m_packStacks = new List<GameObject>();
  private List<AnimatedLowPolyPack> m_showingPacks = new List<AnimatedLowPolyPack>();
  private List<AnimatedLeavingSoonSign> m_showingLeavingSoonSigns = new List<AnimatedLeavingSoonSign>();
  public MeshRenderer m_background;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_leavingSoonBannerPrefab;
  private GeneralStorePacksContentDisplay.PACK_DISPLAY_TYPE m_packDisplayType;
  private GeneralStorePacksContent m_parent;
  private int m_lastVisiblePacks;

  public void SetParent(GeneralStorePacksContent parent)
  {
    this.m_parent = parent;
  }

  public int ShowPacks(int numVisiblePacks, float flyInTime, float flyOutTime, float flyInDelay, float flyOutDelay, bool forceImmediate = false)
  {
    if (this.m_lastVisiblePacks == numVisiblePacks)
      return 0;
    this.m_packDisplayType = GeneralStorePacksContentDisplay.PACK_DISPLAY_TYPE.PACK;
    bool flag = this.m_parent.IsContentActive();
    AnimatedLowPolyPack[] currentPacks = this.GetCurrentPacks(this.m_parent.GetBoosterId(), numVisiblePacks);
    int numPacksFlyingOut = 0;
    for (int index = currentPacks.Length - 1; index >= numVisiblePacks; --index)
    {
      AnimatedLowPolyPack animatedLowPolyPack = currentPacks[index];
      if (flag && !forceImmediate)
      {
        if (animatedLowPolyPack.FlyOut(flyOutTime, flyOutDelay * (float) numPacksFlyingOut))
          ++numPacksFlyingOut;
      }
      else
        animatedLowPolyPack.FlyOutImmediate();
    }
    int numPacksFlyingIn = 0;
    for (int index = 0; index < numVisiblePacks; ++index)
    {
      AnimatedLowPolyPack animatedLowPolyPack = currentPacks[index];
      if (flag && !forceImmediate)
      {
        if (animatedLowPolyPack.FlyIn(flyInTime, flyInDelay * (float) numPacksFlyingIn))
          ++numPacksFlyingIn;
      }
      else
        animatedLowPolyPack.FlyInImmediate();
    }
    this.FlyLeavingSoonBanner(numPacksFlyingIn, numPacksFlyingOut, flyInTime, flyOutTime, flyInDelay, flyOutDelay, numVisiblePacks, flag && !forceImmediate);
    this.m_lastVisiblePacks = numVisiblePacks;
    if (numPacksFlyingIn > numPacksFlyingOut)
      return numPacksFlyingIn;
    return -numPacksFlyingOut;
  }

  public int ShowBundleBox(float flyInTime, float flyOutTime, float flyInDelay, float flyOutDelay, float delay = 0.0f, bool forceImmediate = false)
  {
    Log.Kyle.Print("ShowBundleBox()");
    this.m_packDisplayType = GeneralStorePacksContentDisplay.PACK_DISPLAY_TYPE.BOX;
    bool animated = this.m_parent.IsContentActive();
    AnimatedLowPolyPack[] currentPacks = this.GetCurrentPacks(this.m_parent.GetBoosterId(), 1);
    int numPacksFlyingIn = 1;
    AnimatedLowPolyPack animatedLowPolyPack = currentPacks[0];
    if (!forceImmediate)
      animatedLowPolyPack.FlyIn(flyInTime, delay);
    else
      animatedLowPolyPack.FlyInImmediate();
    this.FlyLeavingSoonBanner(numPacksFlyingIn, 1, flyInTime, flyOutTime, flyInDelay, flyOutDelay, 1, animated);
    this.m_lastVisiblePacks = 0;
    return 0;
  }

  public void PurchaseBundleBox(string cardID)
  {
    AnimatedLowPolyPack[] currentPacks = this.GetCurrentPacks(this.m_parent.GetBoosterId(), 1);
    if (currentPacks == null || currentPacks.Length < 1)
    {
      Debug.LogWarningFormat("PurchaseBundleBox() didn't caontain any packs for cardID {0}", (object) cardID);
    }
    else
    {
      AnimatedLowPolyPack animatedLowPolyPack = currentPacks[0];
      if ((UnityEngine.Object) animatedLowPolyPack == (UnityEngine.Object) null)
      {
        Debug.LogWarningFormat("PurchaseBundleBox() failed to get AnimatedLowPolyPack for cardID {0}", (object) cardID);
      }
      else
      {
        FirstPurchaseBox firstPurchaseBox = animatedLowPolyPack.GetFirstPurchaseBox();
        if ((UnityEngine.Object) firstPurchaseBox == (UnityEngine.Object) null)
          Debug.LogWarningFormat("PurchaseBundleBox() failed to get FirstPurchaseBox for cardID {0}", (object) cardID);
        else
          firstPurchaseBox.PurchaseBundle(cardID);
      }
    }
  }

  public void UpdatePackType(StorePackDef packDef)
  {
    this.ClearPacks();
    if (!((UnityEngine.Object) this.m_background != (UnityEngine.Object) null) || !((UnityEngine.Object) packDef != (UnityEngine.Object) null))
      return;
    this.m_background.material = packDef.m_background;
  }

  public void ClearPacks()
  {
    using (List<AnimatedLowPolyPack>.Enumerator enumerator = this.m_showingPacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.m_showingPacks.Clear();
    using (List<AnimatedLeavingSoonSign>.Enumerator enumerator = this.m_showingLeavingSoonSigns.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.m_showingLeavingSoonSigns.Clear();
    this.m_lastVisiblePacks = 0;
  }

  private AnimatedLowPolyPack[] GetCurrentPacks(int id, int count)
  {
    if (count > this.m_showingPacks.Count)
    {
      AnimatedLowPolyPack original = (AnimatedLowPolyPack) null;
      if (!GeneralStorePacksContentDisplay.s_packTemplates.TryGetValue(id, out original))
      {
        original = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(this.m_parent.GetStorePackDef(id).m_lowPolyPrefab), true, false).GetComponent<AnimatedLowPolyPack>();
        GeneralStorePacksContentDisplay.s_packTemplates[id] = original;
        original.gameObject.SetActive(false);
      }
      for (int count1 = this.m_showingPacks.Count; count1 < count; ++count1)
      {
        AnimatedLowPolyPack pack = UnityEngine.Object.Instantiate<AnimatedLowPolyPack>(original);
        this.SetupLowPolyPack(pack, count1, false);
        this.m_showingPacks.Add(pack);
      }
    }
    return this.m_showingPacks.ToArray();
  }

  private void SetupLowPolyPack(AnimatedLowPolyPack pack, int i, bool useVisiblePacksOnly)
  {
    pack.gameObject.SetActive(true);
    int packColumn = this.DeterminePackColumn(i);
    GameUtils.SetParent((Component) pack, this.m_packStacks[packColumn], true);
    pack.transform.localScale = GeneralStorePacksContentDisplay.PACK_SCALE;
    pack.Init(packColumn, this.DeterminePackLocalPos(packColumn, this.m_showingPacks, useVisiblePacksOnly), new Vector3(0.0f, 3.5f, -0.1f), true, true);
    SceneUtils.SetLayer((Component) pack, this.m_packStacks[packColumn].layer);
    Log.Kyle.Print("SetupLowPolyPack pack display type: {0}", (object) this.m_packDisplayType);
    float y;
    float x;
    float z;
    if (this.m_packDisplayType == GeneralStorePacksContentDisplay.PACK_DISPLAY_TYPE.BOX)
    {
      y = UnityEngine.Random.Range(-this.m_parent.m_BoxYDegreeVariationMag, this.m_parent.m_BoxYDegreeVariationMag);
      x = UnityEngine.Random.Range(-GeneralStorePacksContentDisplay.BOX_FLY_OUT_X_DEG_VARIATION_MAG, GeneralStorePacksContentDisplay.BOX_FLY_OUT_X_DEG_VARIATION_MAG);
      z = UnityEngine.Random.Range(-GeneralStorePacksContentDisplay.BOX_FLY_OUT_Z_DEG_VARIATION_MAG, GeneralStorePacksContentDisplay.BOX_FLY_OUT_Z_DEG_VARIATION_MAG);
    }
    else
    {
      y = UnityEngine.Random.Range(-this.m_parent.m_PackYDegreeVariationMag, this.m_parent.m_PackYDegreeVariationMag);
      x = UnityEngine.Random.Range(-GeneralStorePacksContentDisplay.PACK_FLY_OUT_X_DEG_VARIATION_MAG, GeneralStorePacksContentDisplay.PACK_FLY_OUT_X_DEG_VARIATION_MAG);
      z = UnityEngine.Random.Range(-GeneralStorePacksContentDisplay.PACK_FLY_OUT_Z_DEG_VARIATION_MAG, GeneralStorePacksContentDisplay.PACK_FLY_OUT_Z_DEG_VARIATION_MAG);
    }
    Vector3 flyInLocalAngles = new Vector3(0.0f, y, 0.0f);
    Vector3 flyOutLocalAngles = new Vector3(x, 0.0f, z);
    pack.SetFlyingLocalRotations(flyInLocalAngles, flyOutLocalAngles);
  }

  private Vector3 DeterminePackLocalPos(int column, List<AnimatedLowPolyPack> packs, bool useVisiblePacksOnly)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStorePacksContentDisplay.\u003CDeterminePackLocalPos\u003Ec__AnonStorey3F7 posCAnonStorey3F7 = new GeneralStorePacksContentDisplay.\u003CDeterminePackLocalPos\u003Ec__AnonStorey3F7();
    // ISSUE: reference to a compiler-generated field
    posCAnonStorey3F7.column = column;
    // ISSUE: reference to a compiler-generated field
    posCAnonStorey3F7.useVisiblePacksOnly = useVisiblePacksOnly;
    // ISSUE: reference to a compiler-generated method
    List<AnimatedLowPolyPack> all = packs.FindAll(new Predicate<AnimatedLowPolyPack>(posCAnonStorey3F7.\u003C\u003Em__1DB));
    Vector3 zero = Vector3.zero;
    if (this.m_packDisplayType != GeneralStorePacksContentDisplay.PACK_DISPLAY_TYPE.BOX)
    {
      zero.x = UnityEngine.Random.Range(-GeneralStorePacksContentDisplay.PACK_X_VARIATION_MAG, GeneralStorePacksContentDisplay.PACK_X_VARIATION_MAG);
      zero.y = GeneralStorePacksContentDisplay.PACK_Y_OFFSET * (float) all.Count;
      zero.z = UnityEngine.Random.Range(-GeneralStorePacksContentDisplay.PACK_Z_VARIATION_MAG, GeneralStorePacksContentDisplay.PACK_Z_VARIATION_MAG);
    }
    // ISSUE: reference to a compiler-generated field
    if (posCAnonStorey3F7.column % 2 == 0)
      zero.y += 0.03f;
    return zero;
  }

  private int DeterminePackColumn(int packNumber)
  {
    double num1 = new System.Random(GeneralStorePacksContentDisplay.PACK_STACK_SEED + packNumber).NextDouble();
    double num2 = 0.0;
    float num3 = 1f / (float) this.m_packStacks.Count;
    int num4;
    for (num4 = 0; num4 < this.m_packStacks.Count - 1; ++num4)
    {
      num2 += (double) num3;
      if (num1 <= num2)
        break;
    }
    return num4;
  }

  private void FlyLeavingSoonBanner(int numPacksFlyingIn, int numPacksFlyingOut, float flyInTime, float flyOutTime, float flyInDelay, float flyOutDelay, int numVisiblePacks, bool animated)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStorePacksContentDisplay.\u003CFlyLeavingSoonBanner\u003Ec__AnonStorey3F8 bannerCAnonStorey3F8 = new GeneralStorePacksContentDisplay.\u003CFlyLeavingSoonBanner\u003Ec__AnonStorey3F8();
    // ISSUE: reference to a compiler-generated field
    bannerCAnonStorey3F8.\u003C\u003Ef__this = this;
    using (List<AnimatedLeavingSoonSign>.Enumerator enumerator = this.m_showingLeavingSoonSigns.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AnimatedLeavingSoonSign current = enumerator.Current;
        if (animated)
          current.FlyOut(flyOutTime, 0.0f);
        else
          current.FlyOutImmediate();
      }
    }
    using (List<AnimatedLeavingSoonSign>.Enumerator enumerator = this.m_showingLeavingSoonSigns.FindAll((Predicate<AnimatedLeavingSoonSign>) (l => l.GetState() == AnimatedLowPolyPack.State.HIDDEN)).GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.m_showingLeavingSoonSigns.RemoveAll((Predicate<AnimatedLeavingSoonSign>) (l => l.GetState() == AnimatedLowPolyPack.State.HIDDEN));
    if (string.IsNullOrEmpty(this.m_leavingSoonBannerPrefab))
      return;
    // ISSUE: reference to a compiler-generated field
    bannerCAnonStorey3F8.boosterRecord = GameDbf.Booster.GetRecord(this.m_parent.GetBoosterId());
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (bannerCAnonStorey3F8.boosterRecord == null || !bannerCAnonStorey3F8.boosterRecord.LeavingSoon)
      return;
    AnimatedLeavingSoonSign animatedLeavingSoonSign = GameUtils.LoadGameObjectWithComponent<AnimatedLeavingSoonSign>(FileUtils.GameAssetPathToName(this.m_leavingSoonBannerPrefab));
    if ((UnityEngine.Object) animatedLeavingSoonSign == (UnityEngine.Object) null)
      return;
    if ((UnityEngine.Object) animatedLeavingSoonSign.m_leavingSoonButton != (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated method
      animatedLeavingSoonSign.m_leavingSoonButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(bannerCAnonStorey3F8.\u003C\u003Em__1DE));
    }
    this.SetupLowPolyPack((AnimatedLowPolyPack) animatedLeavingSoonSign, numVisiblePacks, true);
    this.m_showingLeavingSoonSigns.Add(animatedLeavingSoonSign);
    if (animated)
      animatedLeavingSoonSign.FlyIn(flyInTime, flyInDelay * (float) numPacksFlyingIn);
    else
      animatedLeavingSoonSign.FlyInImmediate();
  }

  private void OnLeavingSoonButtonClicked(string leavingSoonText)
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_STORE_EXPANSION_LEAVING_SOON"),
      m_text = leavingSoonText,
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  public enum PACK_DISPLAY_TYPE
  {
    PACK,
    BOX,
  }
}
