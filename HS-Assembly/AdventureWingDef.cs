﻿// Decompiled with JetBrains decompiler
// Type: AdventureWingDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class AdventureWingDef : MonoBehaviour
{
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public String_MobileOverride m_WingPrefab;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_CoinPrefab;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_RewardsPrefab;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_UnlockSpellPrefab;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_AccentPrefab;
  [CustomEditField(Sections = "Opening Quote")]
  public string m_OpenQuotePrefab;
  [CustomEditField(Sections = "Opening Quote")]
  public string m_OpenQuoteVOLine;
  [CustomEditField(Sections = "Opening Quote")]
  public float m_OpenQuoteDelay;
  [CustomEditField(Sections = "Wing Open Popup", T = EditType.GAME_OBJECT)]
  public string m_WingOpenPopup;
  [CustomEditField(Sections = "Complete Quote")]
  public string m_CompleteQuotePrefab;
  [CustomEditField(Sections = "Complete Quote")]
  public string m_CompleteQuoteVOLine;
  [CustomEditField(Sections = "Rewards Preview")]
  public List<string> m_SpecificRewardsPreviewCards;
  [CustomEditField(Sections = "Rewards Preview")]
  public int m_HiddenRewardsPreviewCount;
  [CustomEditField(Sections = "Loc Strings")]
  public string m_LockedLocString;
  [CustomEditField(Sections = "Loc Strings")]
  public string m_LockedPurchaseLocString;
  private AdventureDbId m_AdventureId;
  private WingDbId m_WingId;
  private WingDbId m_OwnershipPrereq;
  private int m_SortOrder;
  private int m_UnlockOrder;
  private string m_WingName;
  private string m_ComingSoonLabel;
  private string m_RequiresLabel;
  private WingDbId m_OpenPrereq;
  private string m_OpeningDiscouragedLabel;
  private string m_OpeningDiscouragedWarning;

  public void Init(WingDbfRecord wingRecord)
  {
    this.m_AdventureId = (AdventureDbId) wingRecord.AdventureId;
    this.m_WingId = (WingDbId) wingRecord.ID;
    this.m_OwnershipPrereq = (WingDbId) wingRecord.OwnershipPrereqWingId;
    this.m_SortOrder = wingRecord.SortOrder;
    this.m_UnlockOrder = wingRecord.UnlockOrder;
    this.m_WingName = (string) wingRecord.Name;
    this.m_ComingSoonLabel = (string) wingRecord.ComingSoonLabel;
    this.m_RequiresLabel = (string) wingRecord.RequiresLabel;
    this.m_OpenPrereq = (WingDbId) wingRecord.OpenPrereqWingId;
    this.m_OpeningDiscouragedLabel = (string) wingRecord.OpenDiscouragedLabel;
    this.m_OpeningDiscouragedWarning = (string) wingRecord.OpenDiscouragedWarning;
  }

  public AdventureDbId GetAdventureId()
  {
    return this.m_AdventureId;
  }

  public WingDbId GetWingId()
  {
    return this.m_WingId;
  }

  public WingDbId GetOwnershipPrereqId()
  {
    return this.m_OwnershipPrereq;
  }

  public int GetSortOrder()
  {
    return this.m_SortOrder;
  }

  public int GetUnlockOrder()
  {
    return this.m_UnlockOrder;
  }

  public string GetWingName()
  {
    return this.m_WingName;
  }

  public string GetComingSoonLabel()
  {
    return this.m_ComingSoonLabel;
  }

  public string GetRequiresLabel()
  {
    return this.m_RequiresLabel;
  }

  public WingDbId GetOpenPrereqId()
  {
    return this.m_OpenPrereq;
  }

  public string GetOpeningNotRecommendedLabel()
  {
    return this.m_OpeningDiscouragedLabel;
  }

  public string GetOpeningNotRecommendedWarning()
  {
    return this.m_OpeningDiscouragedWarning;
  }
}
