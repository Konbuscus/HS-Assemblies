﻿// Decompiled with JetBrains decompiler
// Type: EntityDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

public class EntityDef : EntityBase
{
  protected TagMap m_referencedTags = new TagMap();
  private Map<string, Power> m_powers = new Map<string, Power>();
  private string m_masterPower = string.Empty;
  private List<PowerHistoryInfo> m_powerHistoryInfoList = new List<PowerHistoryInfo>();
  private List<string> m_entourageCardIDs = new List<string>();
  private string m_cardId;
  private string m_currLoadingPowerDefinition;
  private List<Power.PowerInfo> m_currLoadingPowerInfos;

  public override string ToString()
  {
    return this.GetDebugName();
  }

  public static XmlElement LoadCardXmlFromAsset(string cardId, UnityEngine.Object asset)
  {
    if (asset == (UnityEngine.Object) null)
    {
      Debug.LogWarning((object) string.Format("EntityDef.LoadCardXmlFromAsset() - null cardAsset given for cardId \"{0}\"", (object) cardId));
      return (XmlElement) null;
    }
    TextAsset xmlAsset = asset as TextAsset;
    return EntityDef.LoadCardXmlFromAsset(cardId, xmlAsset);
  }

  public static XmlElement LoadCardXmlFromAsset(string cardId, TextAsset xmlAsset)
  {
    if (!((UnityEngine.Object) xmlAsset == (UnityEngine.Object) null))
      return XmlUtils.LoadXmlDocFromTextAsset(xmlAsset)["Entity"];
    Debug.LogWarning((object) string.Format("EntityDef.LoadCardXmlFromAsset() - asset for cardId \"{0}\" was not a card xml", (object) cardId));
    return (XmlElement) null;
  }

  public static EntityDef LoadFromAsset(string cardId, TextAsset xmlAsset, bool overrideCardId = false)
  {
    if (!((UnityEngine.Object) xmlAsset == (UnityEngine.Object) null))
      return EntityDef.LoadFromString(cardId, xmlAsset.text, overrideCardId);
    Debug.LogWarning((object) string.Format("EntityDef.LoadFromAsset() - asset for cardId \"{0}\" was not a card xml", (object) cardId));
    return (EntityDef) null;
  }

  public static EntityDef LoadFromString(string cardId, string xmlText, bool overrideCardId = false)
  {
    EntityDef entityDef = new EntityDef();
    using (StringReader stringReader = new StringReader(xmlText))
    {
      using (XmlReader reader = XmlReader.Create((TextReader) stringReader))
        entityDef.LoadDataFromCardXml(reader);
      if (overrideCardId)
        entityDef.m_cardId = cardId;
    }
    return entityDef;
  }

  public EntityDef Clone()
  {
    EntityDef entityDef = new EntityDef();
    entityDef.m_cardId = this.m_cardId;
    entityDef.ReplaceTags(this.m_tags);
    entityDef.m_referencedTags.Replace(this.m_referencedTags);
    using (Map<string, Power>.Enumerator enumerator = this.m_powers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, Power> current = enumerator.Current;
        entityDef.m_powers.Add(current.Key, current.Value);
      }
    }
    entityDef.m_masterPower = this.m_masterPower;
    return entityDef;
  }

  public string GetCardId()
  {
    return this.m_cardId;
  }

  public void SetCardId(string cardId)
  {
    this.m_cardId = cardId;
  }

  public TagMap GetReferencedTags()
  {
    return this.m_referencedTags;
  }

  public override int GetReferencedTag(int tag)
  {
    return this.m_referencedTags.GetTag(tag);
  }

  public void SetReferencedTag(GAME_TAG enumTag, int val)
  {
    this.SetReferencedTag((int) enumTag, val);
  }

  public void SetReferencedTag(int tag, int val)
  {
    this.m_referencedTags.SetTag(tag, val);
  }

  public TAG_RACE GetRace()
  {
    return this.GetTag<TAG_RACE>(GAME_TAG.CARDRACE);
  }

  public TAG_ENCHANTMENT_VISUAL GetEnchantmentBirthVisual()
  {
    return this.GetTag<TAG_ENCHANTMENT_VISUAL>(GAME_TAG.ENCHANTMENT_BIRTH_VISUAL);
  }

  public TAG_ENCHANTMENT_VISUAL GetEnchantmentIdleVisual()
  {
    return this.GetTag<TAG_ENCHANTMENT_VISUAL>(GAME_TAG.ENCHANTMENT_IDLE_VISUAL);
  }

  public TAG_RARITY GetRarity()
  {
    return this.GetTag<TAG_RARITY>(GAME_TAG.RARITY);
  }

  public string GetName()
  {
    CardDbfRecord cardRecord = GameDbf.GetIndex().GetCardRecord(this.m_cardId);
    if (cardRecord != null && cardRecord.Name != null)
      return cardRecord.Name.GetString(true) ?? this.GetDebugName();
    return this.GetDebugName();
  }

  public string GetDebugName()
  {
    string str = (string) null;
    CardDbfRecord cardRecord = GameDbf.GetIndex().GetCardRecord(this.m_cardId);
    if (cardRecord != null && cardRecord.Name != null)
      str = cardRecord.Name.GetString(true);
    if (str != null)
      return string.Format("[name={0} cardId={1} type={2}]", (object) str, (object) this.m_cardId, (object) this.GetCardType());
    if (this.m_cardId != null)
      return string.Format("[cardId={0} type={1}]", (object) this.m_cardId, (object) this.GetCardType());
    return string.Format("UNKNOWN ENTITY [cardType={0}]", (object) this.GetCardType());
  }

  public string GetArtistName()
  {
    CardDbfRecord cardRecord = GameDbf.GetIndex().GetCardRecord(this.m_cardId);
    if (cardRecord == null)
      return "ERROR: NO ARTIST NAME";
    return cardRecord.ArtistName ?? string.Empty;
  }

  public string GetFlavorText()
  {
    CardDbfRecord cardRecord = GameDbf.GetIndex().GetCardRecord(this.m_cardId);
    if (cardRecord == null || cardRecord.FlavorText == null)
      return string.Empty;
    return cardRecord.FlavorText.GetString(true) ?? string.Empty;
  }

  public string GetHowToEarnText(TAG_PREMIUM premium)
  {
    CardDbfRecord cardRecord = GameDbf.GetIndex().GetCardRecord(this.m_cardId);
    if (cardRecord == null)
      return string.Empty;
    if (premium == TAG_PREMIUM.GOLDEN)
    {
      if (cardRecord.HowToGetGoldCard != null)
        return cardRecord.HowToGetGoldCard.GetString(true) ?? string.Empty;
      return string.Empty;
    }
    if (cardRecord.HowToGetCard != null)
      return cardRecord.HowToGetCard.GetString(true) ?? string.Empty;
    return string.Empty;
  }

  public string GetCardTextInHand()
  {
    CardDef cardDef = DefLoader.Get().GetCardDef(this.m_cardId, (CardPortraitQuality) null);
    if ((UnityEngine.Object) cardDef == (UnityEngine.Object) null)
      return CardTextBuilder.GetDefaultCardTextInHand(this);
    return cardDef.GetCardTextBuilder().BuildCardTextInHand(this);
  }

  public string GetRaceText()
  {
    if (!this.HasTag(GAME_TAG.CARDRACE))
      return string.Empty;
    return GameStrings.GetRaceName(this.GetRace());
  }

  public Power GetMasterPower()
  {
    if (this.m_masterPower.Equals(string.Empty) || !this.m_powers.ContainsKey(this.m_masterPower))
      return Power.GetDefaultMasterPower();
    return this.m_powers[this.m_masterPower];
  }

  public Power GetAttackPower()
  {
    return Power.GetDefaultAttackPower();
  }

  public PowerHistoryInfo GetPowerHistoryInfo(int effectIndex)
  {
    if (effectIndex < 0)
      return (PowerHistoryInfo) null;
    PowerHistoryInfo powerHistoryInfo = (PowerHistoryInfo) null;
    using (List<PowerHistoryInfo>.Enumerator enumerator = this.m_powerHistoryInfoList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PowerHistoryInfo current = enumerator.Current;
        if (current.GetEffectIndex() == effectIndex)
        {
          powerHistoryInfo = current;
          break;
        }
      }
    }
    return powerHistoryInfo;
  }

  public List<string> GetEntourageCardIDs()
  {
    return this.m_entourageCardIDs;
  }

  public bool LoadDataFromCardXml(XmlReader reader)
  {
    this.m_powerHistoryInfoList = new List<PowerHistoryInfo>();
    do
      ;
    while ((reader.NodeType != XmlNodeType.Element || reader.Name != "Entity") && reader.Read());
    if (reader.EOF)
      return false;
    int depth = reader.Depth;
    this.m_cardId = reader["CardID"];
    this.LoadTagFromDBF();
    while (reader.Read())
    {
      if (reader.NodeType == XmlNodeType.Element)
      {
        if (reader.Depth > depth && !reader.EOF)
        {
          if (this.m_currLoadingPowerDefinition != null && !reader.Name.Equals("PlayRequirement"))
            this.FlushPower();
          string name = reader.Name;
          if (name != null)
          {
            // ISSUE: reference to a compiler-generated field
            if (EntityDef.\u003C\u003Ef__switch\u0024mapC6 == null)
            {
              // ISSUE: reference to a compiler-generated field
              EntityDef.\u003C\u003Ef__switch\u0024mapC6 = new Dictionary<string, int>(5)
              {
                {
                  "MasterPower",
                  0
                },
                {
                  "Power",
                  1
                },
                {
                  "PlayRequirement",
                  2
                },
                {
                  "EntourageCard",
                  3
                },
                {
                  "TriggeredPowerHistoryInfo",
                  4
                }
              };
            }
            int num;
            // ISSUE: reference to a compiler-generated field
            if (EntityDef.\u003C\u003Ef__switch\u0024mapC6.TryGetValue(name, out num))
            {
              switch (num)
              {
                case 0:
                  this.ReadMasterPower(reader);
                  continue;
                case 1:
                  this.ReadPower(reader);
                  continue;
                case 2:
                  this.ReadPlayRequirement(reader);
                  continue;
                case 3:
                  this.ReadEntourage(reader);
                  continue;
                case 4:
                  this.ReadTriggeredPowerHistoryInfo(reader);
                  continue;
                default:
                  continue;
              }
            }
          }
        }
        else
          break;
      }
    }
    this.FlushPower();
    return true;
  }

  private void LoadTagFromDBF()
  {
    IEnumerable<CardTagDbfRecord> cardTagRecords = GameUtils.GetCardTagRecords(this.m_cardId);
    if (cardTagRecords == null)
    {
      Debug.LogError((object) string.Format("EntityDef.LoadDataFromCardXml() - No tags found for the card: {0}", (object) this.m_cardId));
    }
    else
    {
      foreach (CardTagDbfRecord cardTagDbfRecord in cardTagRecords)
      {
        if (cardTagDbfRecord.IsReferenceTag)
          this.SetReferencedTag(cardTagDbfRecord.TagId, cardTagDbfRecord.TagValue);
        else
          this.SetTag(cardTagDbfRecord.TagId, cardTagDbfRecord.TagValue);
      }
    }
  }

  private void ReadMasterPower(XmlReader reader)
  {
    if (!string.IsNullOrEmpty(this.m_masterPower))
      Debug.Log((object) string.Format("Error loading card xml {0}, multiple MasterPower definitions", (object) this.m_cardId));
    else
      this.m_masterPower = reader.ReadElementContentAsString();
  }

  private void FlushPower()
  {
    if (this.m_currLoadingPowerDefinition != null)
    {
      Power power = Power.Create(this.m_currLoadingPowerDefinition, this.m_currLoadingPowerInfos);
      if (this.m_powers.ContainsKey(power.GetDefinition()))
        Debug.LogError((object) string.Format("Error loading card xml {0}, already contains power definition {1}", (object) this.m_cardId, (object) power.GetDefinition()));
      else
        this.m_powers.Add(power.GetDefinition(), power);
    }
    this.m_currLoadingPowerDefinition = (string) null;
    this.m_currLoadingPowerInfos = (List<Power.PowerInfo>) null;
  }

  private void ReadPower(XmlReader reader)
  {
    this.m_currLoadingPowerDefinition = reader["definition"];
    this.m_currLoadingPowerInfos = new List<Power.PowerInfo>();
  }

  private void ReadPlayRequirement(XmlReader reader)
  {
    string str1 = reader["reqID"];
    string str2 = reader["param"];
    if (string.IsNullOrEmpty(str1))
    {
      Debug.LogError((object) "PlayRequirement is missing requirement ID");
    }
    else
    {
      int int32 = Convert.ToInt32(str1);
      int num = !string.IsNullOrEmpty(str2) ? Convert.ToInt32(str2) : 0;
      Power.PowerInfo powerInfo;
      powerInfo.reqId = (PlayErrors.ErrorType) int32;
      powerInfo.param = num;
      this.m_currLoadingPowerInfos.Add(powerInfo);
    }
  }

  private void ReadEntourage(XmlReader reader)
  {
    this.m_entourageCardIDs.Add(reader["cardID"]);
  }

  private void ReadTriggeredPowerHistoryInfo(XmlReader reader)
  {
    this.m_powerHistoryInfoList.Add(new PowerHistoryInfo(Convert.ToInt32(reader["effectIndex"]), Convert.ToBoolean(reader["showInHistory"])));
  }
}
