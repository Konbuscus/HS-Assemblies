﻿// Decompiled with JetBrains decompiler
// Type: InitialConversationLines
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public static class InitialConversationLines
{
  public static readonly string[][] BRM_INITIAL_CONVO_LINES = new string[5][]{ new string[2]{ "NormalNefarian_Quote", "VO_NEFARIAN_INTRO1_24" }, new string[2]{ "Ragnaros_Quote", "VO_RAGNAROS_INTRO2_64" }, new string[2]{ "NormalNefarian_Quote", "VO_NEFARIAN_INTRO3_25" }, new string[2]{ "NormalNefarian_Quote", "VO_NEFARIAN_INTRO4_26" }, new string[2]{ "Ragnaros_Quote", "VO_RAGNAROS_INTRO5_65" } };
  public static readonly string[][] LOE_INITIAL_CONVO_LINES = new string[2][]{ new string[2]{ "Cartographer_Quote", "VO_LOE_INTRO_1" }, new string[2]{ "Cartographer_Quote", "VO_LOE_INTRO_3" } };
  public static readonly string[][] KARA_INITIAL_CONVO_LINES = new string[2][]{ new string[2]{ "Medivh_Quote", "VO_Medivh_Male_Human_Intro_01" }, new string[2]{ "Medivh_Quote", "VO_Medivh_Male_Human_Intro_02" } };
}
