﻿// Decompiled with JetBrains decompiler
// Type: ForgeTicketRewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class ForgeTicketRewardData : RewardData
{
  public int Quantity { get; set; }

  public ForgeTicketRewardData()
    : this(0)
  {
  }

  public ForgeTicketRewardData(int quantity)
    : base(Reward.Type.FORGE_TICKET)
  {
    this.Quantity = quantity;
  }

  public override string ToString()
  {
    return string.Format("[ForgeTicketRewardData: Quantity={0} Origin={1} OriginData={2}]", (object) this.Quantity, (object) this.Origin, (object) this.OriginData);
  }

  protected override string GetGameObjectName()
  {
    return "ArenaTicketReward";
  }
}
