﻿// Decompiled with JetBrains decompiler
// Type: MageExplosion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MageExplosion : MonoBehaviour
{
  public GameObject m_ring;

  private void Start()
  {
  }

  private void Awake()
  {
  }

  public void Play()
  {
  }

  private void KillExplosion()
  {
    foreach (ParticleEmitter componentsInChild in this.transform.GetComponentsInChildren<ParticleEmitter>())
    {
      ParticleAnimator component = componentsInChild.transform.GetComponent<ParticleAnimator>();
      if ((Object) component != (Object) null)
        component.autodestruct = true;
      Particle[] particles = componentsInChild.particles;
      for (int index = 0; index < particles.Length; ++index)
        particles[index].energy = 0.1f;
      componentsInChild.particles = particles;
    }
    Object.Destroy((Object) this);
  }
}
