﻿// Decompiled with JetBrains decompiler
// Type: Networking.QueueDispatcher
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using PegasusUtil;
using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Networking
{
  public class QueueDispatcher : IDispatcher, IClientConnectionListener<PegasusPacket>
  {
    private const int MaxWaitTimeMS = 1000;
    private const int WaitTimeStep = 10;
    private readonly QueueClientConnection<PegasusPacket> m_gameConnection;
    private readonly Queue<PegasusPacket> m_gamePackets;
    private readonly IClientRequestManager m_utilConnection;
    private readonly IDebugConnectionManager m_debugConnectionManager;
    private readonly IPacketDecoderManager m_packetDecoderManager;

    public IDebugConnectionManager DebugConnectionManager
    {
      get
      {
        return this.m_debugConnectionManager;
      }
    }

    public GameStartState GameStartState { get; set; }

    public int PingsSinceLastPong { get; set; }

    public double TimeLastPingReceived { get; set; }

    public double TimeLastPingSent { get; set; }

    public QueueDispatcher(IDebugConnectionManager debugConnectionManager, IClientRequestManager clientRequestManager, IPacketDecoderManager packetDecoder)
    {
      this.m_utilConnection = clientRequestManager;
      this.m_gameConnection = new QueueClientConnection<PegasusPacket>();
      this.m_gameConnection.AddConnectHandler(new ConnectHandler(this.OnGameConnection));
      this.m_gameConnection.AddDisconnectHandler(new DisconnectHandler(this.OnGameDisconnect));
      this.m_gameConnection.AddListener((IClientConnectionListener<PegasusPacket>) this, (object) ServerType.GAME_SERVER);
      this.m_gamePackets = new Queue<PegasusPacket>();
      this.GameStartState = GameStartState.Invalid;
      this.m_debugConnectionManager = debugConnectionManager;
      this.m_debugConnectionManager.AddListener((IClientConnectionListener<PegasusPacket>) this);
      this.m_packetDecoderManager = packetDecoder;
    }

    public void Close()
    {
      this.m_utilConnection.Terminate();
      this.m_utilConnection.Update();
      if (this.m_gameConnection == null)
        return;
      this.m_gameConnection.Update();
      int millisecondsTimeout = 0;
      while (this.m_gameConnection.HasOutPacketsInFlight())
      {
        millisecondsTimeout += 10;
        Thread.Sleep(millisecondsTimeout);
        if (millisecondsTimeout > 1000)
          break;
      }
      this.m_gameConnection.Disconnect();
    }

    public PegasusPacket DecodePacket(PegasusPacket packet)
    {
      return this.m_packetDecoderManager.DecodePacket(packet);
    }

    public void PacketReceived(PegasusPacket packet, object state)
    {
      if (this.m_packetDecoderManager.CanDecodePacket(packet.Type))
      {
        PegasusPacket pegasusPacket = this.m_packetDecoderManager.DecodePacket(packet);
        switch ((int) state)
        {
          case 0:
            this.OnGamePacketReceived(packet, packet.Type);
            break;
          case 1:
            this.OnUtilPacketReceived(pegasusPacket, packet.Type);
            break;
          case 2:
            this.DebugConnectionManager.OnPacketReceived(pegasusPacket);
            break;
        }
      }
      else
        Debug.LogError((object) ("Could not find a packet decoder for a packet of type " + (object) packet.Type));
    }

    public void SetDisconnectedFromBattleNet()
    {
      this.m_utilConnection.SetDisconnectedFromBattleNet();
    }

    public bool ShouldIgnoreError(BnetErrorInfo errorInfo)
    {
      return this.m_utilConnection.ShouldIgnoreError(errorInfo);
    }

    public void AddGameServerConnectionListener(IClientConnectionListener<PegasusPacket> listener)
    {
      this.m_gameConnection.AddListener(listener, (object) ServerType.GAME_SERVER);
    }

    public bool ConnectToGameServer(string address, int port)
    {
      this.m_gameConnection.Connect(address, port);
      return this.IsConnectedToGameServer();
    }

    public void DisconnectFromGameServer()
    {
      this.m_gameConnection.Disconnect();
    }

    public int DropAllGamePackets()
    {
      int count = this.m_gamePackets.Count;
      this.m_gamePackets.Clear();
      return count;
    }

    public void DropGamePacket()
    {
      this.m_gamePackets.Dequeue();
    }

    public bool GameServerHasEvents()
    {
      return this.m_gameConnection.HasEvents();
    }

    public bool HasGamePackets()
    {
      return this.m_gamePackets.Count > 0;
    }

    public bool HasGameServerConnection()
    {
      return this.m_gameConnection != null;
    }

    public bool IsConnectedToGameServer()
    {
      if (this.m_gameConnection != null)
        return this.m_gameConnection.Active;
      return false;
    }

    public PegasusPacket NextGamePacket()
    {
      return this.m_gamePackets.Peek();
    }

    public int NextGameType()
    {
      return this.m_gamePackets.Peek().Type;
    }

    private void OnGameConnection(BattleNetErrors error)
    {
      Log.GameMgr.Print("Connecting to game server with error code " + (object) error);
      if (error == BattleNetErrors.ERROR_OK)
        return;
      GameStartState gameStartState = this.GameStartState;
      this.GameStartState = GameStartState.Invalid;
      if (Network.ShouldBeConnectedToAurora())
      {
        if (Network.Get().GetLastGameServerJoined() != null && gameStartState == GameStartState.Reconnecting)
          return;
        Network.Get().ShowBreakingNewsOrError("GLOBAL_ERROR_NETWORK_NO_GAME_SERVER", 0.0f);
        Debug.LogError((object) ("Failed to connect to game server with error " + (object) error));
      }
      else
      {
        Network.Get().ShowBreakingNewsOrError("GLOBAL_ERROR_NETWORK_NO_GAME_SERVER", 0.0f);
        Debug.LogError((object) ("Failed to connect to game server with error " + (object) error));
      }
    }

    private void OnGameDisconnect(BattleNetErrors error)
    {
      Log.GameMgr.Print("Disconnected from game server with error {0} {1}", new object[2]
      {
        (object) (int) error,
        (object) error.ToString()
      });
      bool flag = false;
      if (error != BattleNetErrors.ERROR_OK)
      {
        if (this.GameStartState == GameStartState.Reconnecting)
          flag = true;
        else if (this.GameStartState == GameStartState.InitialStart)
        {
          bgs.types.GameServerInfo gameServerJoined = Network.Get().GetLastGameServerJoined();
          if (gameServerJoined == null || !gameServerJoined.SpectatorMode)
          {
            Network.ConnectErrorParams errorParams = new Network.ConnectErrorParams();
            errorParams.m_message = GameStrings.Format(error != BattleNetErrors.ERROR_RPC_CONNECTION_TIMED_OUT ? "GLOBAL_ERROR_NETWORK_DISCONNECT_GAME_SERVER" : "GLOBAL_ERROR_NETWORK_CONNECTION_TIMEOUT");
            Network.AddErrorToList(errorParams);
            Debug.LogError((object) ("Disconnected from game server with error " + (object) error));
            flag = true;
          }
        }
        this.GameStartState = GameStartState.Invalid;
      }
      if (flag)
        return;
      Network.GameServerDisconnectEvents.Add(error);
    }

    public void OnGamePacketReceived(PegasusPacket decodedPacket, int packetTypeId)
    {
      switch (packetTypeId)
      {
        case 16:
          this.GameStartState = GameStartState.Invalid;
          break;
        case 116:
          this.TimeLastPingReceived = bgs.TimeUtils.GetElapsedTimeSinceEpoch(new DateTime?()).TotalSeconds;
          this.PingsSinceLastPong = 0;
          break;
      }
      if (decodedPacket == null)
        return;
      this.m_gamePackets.Enqueue(decodedPacket);
    }

    public void ProcessGamePackets()
    {
      this.m_gameConnection.Update();
    }

    public void SendGamePacket(int packetId, IProtoBuf body)
    {
      this.m_gameConnection.SendPacket(new PegasusPacket(packetId, 0, (object) body));
    }

    public int DropAllUtilPackets()
    {
      return -1;
    }

    public bool HasUtilErrors()
    {
      return this.m_utilConnection.HasErrors();
    }

    public void DropUtilPacket()
    {
      this.m_utilConnection.DropNextClientRequest();
    }

    public bool HasUtilPackets()
    {
      return this.m_utilConnection.HasPendingDeliveryPackets();
    }

    public PegasusPacket NextUtilPacket()
    {
      return this.m_utilConnection.GetNextClientRequest();
    }

    public int NextUtilType()
    {
      return this.m_utilConnection.PeekNetClientRequestType();
    }

    public void NotifyUtilResponseReceived(PegasusPacket packet)
    {
      this.m_utilConnection.NotifyResponseReceived(packet);
    }

    public void OnLoginComplete()
    {
      this.m_utilConnection.NotifyLoginSequenceCompleted();
    }

    public void OnStartupPacketSequenceComplete()
    {
      this.m_utilConnection.NotifyStartupSequenceComplete();
    }

    public void OnUtilPacketReceived(PegasusPacket decodedPacket, int packetTypeId)
    {
    }

    public void ProcessUtilPackets()
    {
      this.m_utilConnection.Update();
    }

    public void SendUtilPacket(int type, int system, IProtoBuf body, RequestPhase requestPhase = RequestPhase.RUNNING, int subId = 0)
    {
      if (!Network.ShouldBeConnectedToAurora())
      {
        FakeUtilHandler.FakeUtilOutbound(type, body, subId);
      }
      else
      {
        ClientRequestManager.ClientRequestConfig clientRequestConfig1;
        if (system != 0)
          clientRequestConfig1 = new ClientRequestManager.ClientRequestConfig()
          {
            ShouldRetryOnError = false,
            RequestedSystem = system
          };
        else
          clientRequestConfig1 = (ClientRequestManager.ClientRequestConfig) null;
        ClientRequestManager.ClientRequestConfig clientRequestConfig2 = clientRequestConfig1;
        if (!this.m_utilConnection.SendClientRequest(type, body, clientRequestConfig2, requestPhase, subId))
          return;
        if (type == 201)
        {
          GetAccountInfo getAccountInfo = (GetAccountInfo) body;
          Network.AddPendingRequestTimeout(type, (int) getAccountInfo.Request_);
        }
        else
          Network.AddPendingRequestTimeout(type, 0);
      }
    }
  }
}
