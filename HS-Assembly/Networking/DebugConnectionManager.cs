﻿// Decompiled with JetBrains decompiler
// Type: Networking.DebugConnectionManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Networking
{
  internal class DebugConnectionManager : IDebugConnectionManager
  {
    private const int DEBUG_CLIENT_TCP_PORT = 1226;
    private ClientConnection<PegasusPacket> m_debugConnection;
    private readonly Queue<PegasusPacket> m_debugPackets;
    private readonly ServerConnection<PegasusPacket> m_debugServerListener;
    private IClientConnectionListener<PegasusPacket> m_connectionListener;

    public DebugConnectionManager()
    {
      this.m_debugPackets = new Queue<PegasusPacket>();
      this.m_debugServerListener = new ServerConnection<PegasusPacket>();
      this.m_debugServerListener.Open(1226);
    }

    public bool TryConnectDebugConsole()
    {
      if (this.IsActive())
        return true;
      this.m_debugConnection = this.m_debugServerListener.GetNextAcceptedConnection();
      if (this.m_debugConnection == null)
        return false;
      if (this.m_connectionListener != null)
        this.m_debugConnection.AddListener(this.m_connectionListener, (object) ServerType.DEBUG_CONSOLE);
      this.m_debugConnection.StartReceiving();
      return true;
    }

    public bool AllowDebugConnections()
    {
      return true;
    }

    public void OnPacketReceived(PegasusPacket packet)
    {
      this.m_debugPackets.Enqueue(packet);
    }

    public bool ShouldBroadcastDebugConnections()
    {
      return false;
    }

    public void SendDebugPacket(int packetId, IProtoBuf body)
    {
      this.m_debugConnection.SendPacket(new PegasusPacket(packetId, 0, (object) body));
    }

    public bool HaveDebugPackets()
    {
      return this.m_debugPackets.Any<PegasusPacket>();
    }

    public int NextDebugConsoleType()
    {
      return this.m_debugPackets.Peek().Type;
    }

    public void Shutdown()
    {
      if (!this.IsActive())
        return;
      this.m_debugConnection.Disconnect();
      this.m_debugServerListener.Disconnect();
    }

    public bool IsActive()
    {
      if (this.m_debugServerListener != null && this.m_debugConnection != null)
        return this.m_debugConnection.Active;
      return false;
    }

    public void OnLoginStarted()
    {
      this.SetupBroadcast();
    }

    public void Setup()
    {
      this.SetupBroadcast();
    }

    public void Update()
    {
      this.m_debugConnection.Update();
    }

    public void DropPacket()
    {
      this.m_debugPackets.Dequeue();
    }

    public int DropAllPackets()
    {
      int count = this.m_debugPackets.Count;
      this.m_debugPackets.Clear();
      return count;
    }

    public void AddListener(IClientConnectionListener<PegasusPacket> listener)
    {
      this.m_connectionListener = listener;
    }

    public PegasusPacket NextDebugPacket()
    {
      return this.m_debugPackets.Peek();
    }

    public void SendDebugConsoleResponse(int responseType, string message)
    {
      if (message == null)
        return;
      if (!this.IsActive())
        Debug.LogWarning((object) ("Cannot send console response " + message + "; no debug console is active."));
      else
        this.SendDebugPacket(124, (IProtoBuf) new BobNetProto.DebugConsoleResponse()
        {
          ResponseType_ = (BobNetProto.DebugConsoleResponse.ResponseType) responseType,
          Response = message
        });
    }

    private void SetupBroadcast()
    {
    }
  }
}
