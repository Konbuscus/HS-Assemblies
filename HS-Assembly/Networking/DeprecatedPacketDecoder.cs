﻿// Decompiled with JetBrains decompiler
// Type: Networking.DeprecatedPacketDecoder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Networking
{
  internal class DeprecatedPacketDecoder : IPacketDecoder
  {
    public PegasusPacket DecodePacket(PegasusPacket packet)
    {
      Debug.LogWarning((object) ("Dropping deprecated packet of type: " + (object) packet.Type));
      return (PegasusPacket) null;
    }
  }
}
