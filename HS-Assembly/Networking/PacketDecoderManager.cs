﻿// Decompiled with JetBrains decompiler
// Type: Networking.PacketDecoderManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using BobNetProto;
using PegasusGame;
using PegasusUtil;

namespace Networking
{
  public class PacketDecoderManager : IPacketDecoderManager
  {
    private readonly Map<int, IPacketDecoder> packetDecoders;

    public PacketDecoderManager(Map<int, IPacketDecoder> decoders)
    {
      this.packetDecoders = decoders;
    }

    public PacketDecoderManager(bool registerDebugDecoders)
    {
      this.packetDecoders = new Map<int, IPacketDecoder>()
      {
        {
          116,
          (IPacketDecoder) new PongPacketDecoder()
        },
        {
          169,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<Deadend>()
        },
        {
          167,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<DeadendUtil>()
        },
        {
          14,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<AllOptions>()
        },
        {
          5,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<DebugMessage>()
        },
        {
          17,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusGame.EntityChoices>()
        },
        {
          13,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusGame.EntitiesChosen>()
        },
        {
          16,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusGame.GameSetup>()
        },
        {
          19,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusGame.PowerHistory>()
        },
        {
          15,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusGame.UserUI>()
        },
        {
          9,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusGame.TurnTimer>()
        },
        {
          10,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<NAckOption>()
        },
        {
          12,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<GameCanceled>()
        },
        {
          23,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<ServerResult>()
        },
        {
          24,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<SpectatorNotify>()
        },
        {
          289,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<Disconnected>()
        },
        {
          202,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<DeckList>()
        },
        {
          207,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<Collection>()
        },
        {
          215,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<GetDeckContentsResponse>()
        },
        {
          216,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.DBAction>()
        },
        {
          217,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<DeckCreated>()
        },
        {
          218,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<DeckDeleted>()
        },
        {
          219,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<DeckRenamed>()
        },
        {
          212,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.ProfileNotices>()
        },
        {
          224,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<BoosterList>()
        },
        {
          226,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<BoosterContent>()
        },
        {
          208,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<GamesInfo>()
        },
        {
          231,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<ProfileDeckLimit>()
        },
        {
          262,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<ArcaneDustBalance>()
        },
        {
          278,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<GoldBalance>()
        },
        {
          233,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<ProfileProgress>()
        },
        {
          270,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PlayerRecords>()
        },
        {
          271,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<RewardProgress>()
        },
        {
          232,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<MedalInfo>()
        },
        {
          241,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<ClientOptions>()
        },
        {
          246,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<DraftBeginning>()
        },
        {
          247,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.DraftRetired>()
        },
        {
          248,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.DraftChoicesAndContents>()
        },
        {
          249,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.DraftChosen>()
        },
        {
          288,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<DraftRewardsAcked>()
        },
        {
          251,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.DraftError>()
        },
        {
          252,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<Achieves>()
        },
        {
          285,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<ValidateAchieveResponse>()
        },
        {
          282,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<CancelQuestResponse>()
        },
        {
          264,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<GuardianVars>()
        },
        {
          260,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<CardValues>()
        },
        {
          258,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<BoughtSoldCard>()
        },
        {
          269,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.MassDisenchantResponse>()
        },
        {
          265,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<BattlePayStatusResponse>()
        },
        {
          295,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.ThirdPartyPurchaseStatusResponse>()
        },
        {
          272,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.PurchaseMethod>()
        },
        {
          275,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<CancelPurchaseResponse>()
        },
        {
          256,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.PurchaseResponse>()
        },
        {
          238,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<BattlePayConfigResponse>()
        },
        {
          280,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PurchaseWithGoldResponse>()
        },
        {
          283,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<HeroXP>()
        },
        {
          254,
          (IPacketDecoder) new NoOpPacketDecoder()
        },
        {
          286,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PlayQueue>()
        },
        {
          331,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<UpdateAccountLicensesResponse>()
        },
        {
          236,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<CardBacks>()
        },
        {
          292,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<SetCardBackResponse>()
        },
        {
          296,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<SetProgressResponse>()
        },
        {
          299,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<TriggerEventResponse>()
        },
        {
          300,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<NotSoMassiveLoginReply>()
        },
        {
          304,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<AssetsVersionResponse>()
        },
        {
          306,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<AdventureProgressResponse>()
        },
        {
          336,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<RecruitAFriendURLResponse>()
        },
        {
          338,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<RecruitAFriendDataResponse>()
        },
        {
          307,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<UpdateLoginComplete>()
        },
        {
          311,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.AccountLicenseAchieveResponse>()
        },
        {
          315,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<SubscribeResponse>()
        },
        {
          316,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<TavernBrawlInfo>()
        },
        {
          317,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<TavernBrawlPlayerRecordResponse>()
        },
        {
          318,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<FavoriteHeroesResponse>()
        },
        {
          320,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.SetFavoriteHeroResponse>()
        },
        {
          324,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<DebugCommandResponse>()
        },
        {
          325,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<AccountLicensesInfoResponse>()
        },
        {
          326,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<PegasusUtil.GenericResponse>()
        },
        {
          328,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<ClientRequestResponse>()
        },
        {
          322,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<GetAssetResponse>()
        },
        {
          341,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<ClientStaticAssetsResponse>()
        },
        {
          333,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<AchievementNotifications>()
        },
        {
          334,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<NoticeNotifications>()
        },
        {
          347,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<TavernBrawlRequestSessionBeginResponse>()
        },
        {
          348,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<TavernBrawlRequestSessionRetireResponse>()
        },
        {
          349,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<TavernBrawlSessionAckRewardsResponse>()
        },
        {
          351,
          (IPacketDecoder) new DefaultProtobufPacketDecoder<ArenaSessionResponse>()
        }
      };
      if (!registerDebugDecoders)
        return;
      this.packetDecoders.Add(123, (IPacketDecoder) new DefaultProtobufPacketDecoder<DebugConsoleCommand>());
      this.packetDecoders.Add(124, (IPacketDecoder) new DefaultProtobufPacketDecoder<BobNetProto.DebugConsoleResponse>());
    }

    public bool CanDecodePacket(int packetId)
    {
      return this.packetDecoders.ContainsKey(packetId);
    }

    public PegasusPacket DecodePacket(PegasusPacket packet)
    {
      IPacketDecoder packetDecoder;
      if (this.packetDecoders.TryGetValue(packet.Type, out packetDecoder))
        return packetDecoder.DecodePacket(packet);
      return (PegasusPacket) null;
    }
  }
}
