﻿// Decompiled with JetBrains decompiler
// Type: Networking.FakeUtilHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System;

namespace Networking
{
  internal static class FakeUtilHandler
  {
    internal static bool FakeUtilOutbound(int type, IProtoBuf body, int subId)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      FakeUtilHandler.\u003CFakeUtilOutbound\u003Ec__AnonStorey3C6 outboundCAnonStorey3C6 = new FakeUtilHandler.\u003CFakeUtilOutbound\u003Ec__AnonStorey3C6();
      // ISSUE: reference to a compiler-generated field
      outboundCAnonStorey3C6.body = body;
      // ISSUE: reference to a compiler-generated field
      outboundCAnonStorey3C6.success = true;
      switch (type)
      {
        case 239:
        case 276:
        case 284:
        case 305:
          // ISSUE: reference to a compiler-generated field
          return outboundCAnonStorey3C6.success;
        case 240:
        case 201:
        case 253:
          // ISSUE: reference to a compiler-generated field
          FakeUtilHandler.FakeProcessPacket(type, outboundCAnonStorey3C6.body, subId);
          goto case 239;
        case 327:
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          ((GenericRequestList) outboundCAnonStorey3C6.body).Requests.ForEach(new Action<GenericRequest>(outboundCAnonStorey3C6.\u003C\u003Em__169));
          goto case 239;
        default:
          // ISSUE: reference to a compiler-generated field
          outboundCAnonStorey3C6.success = false;
          goto case 239;
      }
    }

    private static bool FakeProcessPacket(int type, IProtoBuf body, int subId)
    {
      bool flag = true;
      switch (type)
      {
        case 201:
          flag = FakeUtilHandler.FakeUtilOutboundGetAccountInfo((GetAccountInfo.Request) subId);
          break;
        case 240:
          flag = Network.Get().FakeHandleType((Enum) ClientOptions.PacketID.ID);
          break;
        case 253:
          flag = Network.Get().FakeHandleType((Enum) Achieves.PacketID.ID);
          break;
        case 340:
          Network.Get().FakeHandleType((Enum) ClientStaticAssetsResponse.PacketID.ID);
          break;
        default:
          Log.Net.PrintWarning("FakeUtilOutbound: unable to simulate response for requestId={0} subId={1}", new object[2]
          {
            (object) type,
            (object) subId
          });
          break;
      }
      return flag;
    }

    private static bool FakeUtilOutboundGetAccountInfo(GetAccountInfo.Request request)
    {
      Enum enumId = (Enum) null;
      switch (request)
      {
        case GetAccountInfo.Request.DECK_LIST:
          enumId = (Enum) DeckList.PacketID.ID;
          break;
        case GetAccountInfo.Request.COLLECTION:
          enumId = (Enum) Collection.PacketID.ID;
          break;
        case GetAccountInfo.Request.MEDAL_INFO:
          enumId = (Enum) MedalInfo.PacketID.ID;
          break;
        case GetAccountInfo.Request.BOOSTERS:
          enumId = (Enum) BoosterList.PacketID.ID;
          break;
        case GetAccountInfo.Request.CARD_BACKS:
          enumId = (Enum) CardBacks.PacketID.ID;
          break;
        case GetAccountInfo.Request.PLAYER_RECORD:
          enumId = (Enum) PlayerRecords.PacketID.ID;
          break;
        case GetAccountInfo.Request.DECK_LIMIT:
          enumId = (Enum) ProfileDeckLimit.PacketID.ID;
          break;
        case GetAccountInfo.Request.CAMPAIGN_INFO:
          enumId = (Enum) ProfileProgress.PacketID.ID;
          break;
        case GetAccountInfo.Request.NOTICES:
          enumId = (Enum) PegasusUtil.ProfileNotices.PacketID.ID;
          break;
        case GetAccountInfo.Request.CLIENT_OPTIONS:
          enumId = (Enum) ClientOptions.PacketID.ID;
          break;
        case GetAccountInfo.Request.CARD_VALUES:
          enumId = (Enum) CardValues.PacketID.ID;
          break;
        case GetAccountInfo.Request.DISCONNECTED:
          enumId = (Enum) Disconnected.PacketID.ID;
          break;
        case GetAccountInfo.Request.ARCANE_DUST_BALANCE:
          enumId = (Enum) ArcaneDustBalance.PacketID.ID;
          break;
        case GetAccountInfo.Request.FEATURES:
          enumId = (Enum) GuardianVars.PacketID.ID;
          break;
        case GetAccountInfo.Request.REWARD_PROGRESS:
          enumId = (Enum) RewardProgress.PacketID.ID;
          break;
        case GetAccountInfo.Request.GOLD_BALANCE:
          enumId = (Enum) GoldBalance.PacketID.ID;
          break;
        case GetAccountInfo.Request.HERO_XP:
          enumId = (Enum) HeroXP.PacketID.ID;
          break;
        case GetAccountInfo.Request.NOT_SO_MASSIVE_LOGIN:
          enumId = (Enum) NotSoMassiveLoginReply.PacketID.ID;
          break;
        case GetAccountInfo.Request.TAVERN_BRAWL_INFO:
          enumId = (Enum) TavernBrawlInfo.PacketID.ID;
          break;
        case GetAccountInfo.Request.TAVERN_BRAWL_RECORD:
          enumId = (Enum) TavernBrawlPlayerRecordResponse.PacketID.ID;
          break;
        case GetAccountInfo.Request.FAVORITE_HEROES:
          enumId = (Enum) FavoriteHeroesResponse.PacketID.ID;
          break;
        case GetAccountInfo.Request.ACCOUNT_LICENSES:
          enumId = (Enum) AccountLicensesInfoResponse.PacketID.ID;
          break;
      }
      if (enumId != null)
        return Network.Get().FakeHandleType(enumId);
      return false;
    }
  }
}
