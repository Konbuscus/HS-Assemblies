﻿// Decompiled with JetBrains decompiler
// Type: Networking.IClientRequestManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;

namespace Networking
{
  public interface IClientRequestManager
  {
    bool SendClientRequest(int type, IProtoBuf body, ClientRequestManager.ClientRequestConfig clientRequestConfig, RequestPhase requestPhase, int subID);

    void NotifyResponseReceived(PegasusPacket packet);

    void NotifyStartupSequenceComplete();

    bool HasPendingDeliveryPackets();

    int PeekNetClientRequestType();

    PegasusPacket GetNextClientRequest();

    void DropNextClientRequest();

    void NotifyLoginSequenceCompleted();

    bool ShouldIgnoreError(BnetErrorInfo errorInfo);

    void ScheduleResubscribe();

    void Terminate();

    void SetDisconnectedFromBattleNet();

    void Update();

    bool HasErrors();
  }
}
