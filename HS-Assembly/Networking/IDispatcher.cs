﻿// Decompiled with JetBrains decompiler
// Type: Networking.IDispatcher
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;

namespace Networking
{
  public interface IDispatcher
  {
    IDebugConnectionManager DebugConnectionManager { get; }

    GameStartState GameStartState { get; set; }

    int PingsSinceLastPong { get; set; }

    double TimeLastPingReceived { get; set; }

    double TimeLastPingSent { get; set; }

    void Close();

    PegasusPacket DecodePacket(PegasusPacket packet);

    void SetDisconnectedFromBattleNet();

    bool ShouldIgnoreError(BnetErrorInfo errorInfo);

    bool ConnectToGameServer(string address, int port);

    void DisconnectFromGameServer();

    int DropAllGamePackets();

    void DropGamePacket();

    bool GameServerHasEvents();

    bool HasGamePackets();

    bool HasGameServerConnection();

    bool IsConnectedToGameServer();

    PegasusPacket NextGamePacket();

    int NextGameType();

    void ProcessGamePackets();

    void OnGamePacketReceived(PegasusPacket decodedPacket, int packetTypeId);

    void SendGamePacket(int packetId, IProtoBuf body);

    int DropAllUtilPackets();

    void DropUtilPacket();

    bool HasUtilErrors();

    bool HasUtilPackets();

    PegasusPacket NextUtilPacket();

    int NextUtilType();

    void NotifyUtilResponseReceived(PegasusPacket packet);

    void OnLoginComplete();

    void OnStartupPacketSequenceComplete();

    void OnUtilPacketReceived(PegasusPacket decodedPacket, int packetTypeId);

    void ProcessUtilPackets();

    void SendUtilPacket(int type, int system, IProtoBuf body, RequestPhase requestPhase, int subId);
  }
}
