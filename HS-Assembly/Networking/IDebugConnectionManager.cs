﻿// Decompiled with JetBrains decompiler
// Type: Networking.IDebugConnectionManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;

namespace Networking
{
  public interface IDebugConnectionManager
  {
    bool AllowDebugConnections();

    bool ShouldBroadcastDebugConnections();

    void SendDebugPacket(int packetId, IProtoBuf body);

    bool HaveDebugPackets();

    int NextDebugConsoleType();

    void Shutdown();

    bool IsActive();

    void Update();

    void OnLoginStarted();

    void DropPacket();

    int DropAllPackets();

    void AddListener(IClientConnectionListener<PegasusPacket> listener);

    PegasusPacket NextDebugPacket();

    bool TryConnectDebugConsole();

    void OnPacketReceived(PegasusPacket packet);

    void SendDebugConsoleResponse(int responseType, string message);
  }
}
