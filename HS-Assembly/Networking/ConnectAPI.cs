﻿// Decompiled with JetBrains decompiler
// Type: Networking.ConnectAPI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using BobNetProto;
using PegasusGame;
using PegasusShared;
using PegasusUtil;
using System.Collections.Generic;

namespace Networking
{
  public class ConnectAPI
  {
    private readonly IDispatcher dispatcherImpl;

    public ConnectAPI(IDispatcher dispatcher)
    {
      this.dispatcherImpl = dispatcher;
    }

    public void AbortBlizzardPurchase(string deviceId, bool isAutoCanceled, CancelPurchase.CancelReason? reason, string error)
    {
      CancelPurchase cancelPurchase = new CancelPurchase() { IsAutoCancel = isAutoCanceled, DeviceId = deviceId, ErrorMessage = error };
      if (reason.HasValue)
        cancelPurchase.Reason = reason.Value;
      this.SendUtilPacket(274, 1, (IProtoBuf) cancelPurchase, RequestPhase.RUNNING, 0);
    }

    public void AbortThirdPartyPurchase(string deviceId, CancelPurchase.CancelReason reason, string error)
    {
      this.SendUtilPacket(274, 1, (IProtoBuf) new CancelPurchase()
      {
        IsAutoCancel = false,
        Reason = reason,
        DeviceId = deviceId,
        ErrorMessage = error
      }, RequestPhase.RUNNING, 0);
    }

    public void AckAchieveProgress(int achievementId, int ackProgress)
    {
      this.SendUtilPacket(243, 0, (IProtoBuf) new AckAchieveProgress()
      {
        Id = achievementId,
        AckProgress = ackProgress
      }, RequestPhase.RUNNING, 0);
    }

    public void AckCardSeen(AckCardSeen ackCardSeenPacket)
    {
      this.SendUtilPacket(223, 0, (IProtoBuf) ackCardSeenPacket, RequestPhase.RUNNING, 0);
    }

    public void AckNotice(long noticeId)
    {
      this.SendUtilPacket(213, 0, (IProtoBuf) new AckNotice()
      {
        Entry = noticeId
      }, RequestPhase.RUNNING, 0);
    }

    public void AcknowledgeBanner(int bannerId)
    {
      this.SendUtilPacket(309, 0, (IProtoBuf) new AcknowledgeBanner()
      {
        Banner = bannerId
      }, RequestPhase.RUNNING, 0);
    }

    public void AckWingProgress(int wing, int ackProgress)
    {
      this.SendUtilPacket(308, 0, (IProtoBuf) new AckWingProgress()
      {
        Wing = wing,
        Ack = ackProgress
      }, RequestPhase.RUNNING, 0);
    }

    public void BeginThirdPartyPurchase(string deviceId, BattlePayProvider provider, string productId, int quantity)
    {
      this.SendUtilPacket(312, 1, (IProtoBuf) new StartThirdPartyPurchase()
      {
        Provider = provider,
        ProductId = productId,
        Quantity = quantity,
        DeviceId = deviceId
      }, RequestPhase.RUNNING, 0);
    }

    public void BeginThirdPartyPurchaseWithReceipt(string deviceId, BattlePayProvider provider, string productId, int quantity, string thirdPartyId, string base64Receipt, string thirdPartyUserId)
    {
      ThirdPartyReceiptData partyReceiptData = new ThirdPartyReceiptData() { ThirdPartyId = thirdPartyId, Receipt = base64Receipt, ThirdPartyUserId = thirdPartyUserId };
      this.SendUtilPacket(312, 1, (IProtoBuf) new StartThirdPartyPurchase()
      {
        Provider = provider,
        ProductId = productId,
        Quantity = quantity,
        DeviceId = deviceId,
        DanglingReceiptData = partyReceiptData
      }, RequestPhase.RUNNING, 0);
    }

    public void BuyCard(PegasusShared.CardDef cardDef, int count, int unitBuyPrice)
    {
      BuySellCard buySellCard = new BuySellCard() { Def = cardDef, Buying = true, UnitBuyPrice = unitBuyPrice };
      if (count != 1)
        buySellCard.Count = count;
      this.SendUtilPacket(257, 0, (IProtoBuf) buySellCard, RequestPhase.RUNNING, 0);
    }

    public void CheckAccountLicenseAchieve(int achieveId)
    {
      this.SendUtilPacket(297, 1, (IProtoBuf) new CheckAccountLicenseAchieve()
      {
        Achieve = achieveId
      }, RequestPhase.RUNNING, 0);
    }

    public void Close()
    {
      this.dispatcherImpl.Close();
      this.dispatcherImpl.DebugConnectionManager.Shutdown();
    }

    public void Concede()
    {
      this.SendGamePacket(11, (IProtoBuf) new Concede());
    }

    public void ConfirmPurchase()
    {
      this.SendUtilPacket(273, 1, (IProtoBuf) new DoPurchase(), RequestPhase.RUNNING, 0);
    }

    public void CreateDeck(DeckType deckType, string name, int heroId, TAG_PREMIUM heroIsPremium, bool isWild, long sortOrder, DeckSourceType sourceType)
    {
      this.SendUtilPacket(209, 0, (IProtoBuf) new CreateDeck()
      {
        Name = name,
        Hero = heroId,
        HeroPremium = (int) heroIsPremium,
        DeckType = deckType,
        TaggedStandard = !isWild,
        SortOrder = sortOrder,
        SourceType = sourceType
      }, RequestPhase.RUNNING, 0);
    }

    public DeckCreated DeckCreated()
    {
      return this.UnpackNextUtilPacket<DeckCreated>(217);
    }

    public DeckDeleted DeckDeleted()
    {
      return this.UnpackNextUtilPacket<DeckDeleted>(218);
    }

    public DeckRenamed DeckRenamed()
    {
      return this.UnpackNextUtilPacket<DeckRenamed>(219);
    }

    public void DecodeAndProcessPacket(PegasusPacket packet)
    {
      if (this.dispatcherImpl.DecodePacket(packet) == null)
        return;
      this.dispatcherImpl.NotifyUtilResponseReceived(packet);
    }

    public void DeleteDeck(long deckId)
    {
      this.SendUtilPacket(210, 0, (IProtoBuf) new DeleteDeck()
      {
        Deck = deckId
      }, RequestPhase.RUNNING, 0);
    }

    public void DisconnectFromGameServer()
    {
      this.dispatcherImpl.DisconnectFromGameServer();
    }

    public void DoLoginUpdate(string referralSource)
    {
      this.SendUtilPacket(205, 0, (IProtoBuf) new UpdateLogin()
      {
        Referral = referralSource
      }, RequestPhase.STARTUP, 0);
    }

    public void DraftAckRewards(long deckId, int slot)
    {
      this.SendUtilPacket(287, 0, (IProtoBuf) new DraftAckRewards()
      {
        DeckId = deckId,
        Slot = slot
      }, RequestPhase.RUNNING, 0);
    }

    public void DraftBegin()
    {
      this.SendUtilPacket(235, 0, (IProtoBuf) new DraftBegin(), RequestPhase.RUNNING, 0);
    }

    public PegasusUtil.DraftChosen DraftCardChosen()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.DraftChosen>(249);
    }

    public DraftBeginning DraftGetBeginning()
    {
      return this.UnpackNextUtilPacket<DraftBeginning>(246);
    }

    public PegasusUtil.DraftChoicesAndContents DraftGetChoicesAndContents()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.DraftChoicesAndContents>(248);
    }

    public PegasusUtil.DraftError DraftGetError()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.DraftError>(251);
    }

    public void DraftGetPicksAndContents()
    {
      this.SendUtilPacket(244, 0, (IProtoBuf) new DraftGetPicksAndContents(), RequestPhase.RUNNING, 0);
    }

    public void SendArenaSessionRequest()
    {
      this.SendUtilPacket(346, 0, (IProtoBuf) new ArenaSessionRequest(), RequestPhase.RUNNING, 0);
    }

    public ArenaSessionResponse GetArenaSessionResponse()
    {
      return this.UnpackNextUtilPacket<ArenaSessionResponse>(351);
    }

    public DraftRewardsAcked DraftRewardsAcked()
    {
      return this.UnpackNextUtilPacket<DraftRewardsAcked>(288);
    }

    public void DraftMakePick(long deckId, int slot, int index)
    {
      this.SendUtilPacket(245, 0, (IProtoBuf) new DraftMakePick()
      {
        DeckId = deckId,
        Slot = slot,
        Index = index
      }, RequestPhase.RUNNING, 0);
    }

    public void DraftRetire(long deckId, int slot)
    {
      this.SendUtilPacket(242, 0, (IProtoBuf) new DraftRetire()
      {
        DeckId = deckId,
        Slot = slot
      }, RequestPhase.RUNNING, 0);
    }

    public int DropAllDebugPackets()
    {
      return this.dispatcherImpl.DebugConnectionManager.DropAllPackets();
    }

    public int DropAllGamePackets()
    {
      return this.dispatcherImpl.DropAllGamePackets();
    }

    public int DropAllUtilPackets()
    {
      return this.dispatcherImpl.DropAllUtilPackets();
    }

    public void DropDebugPacket()
    {
      this.dispatcherImpl.DebugConnectionManager.DropPacket();
    }

    public void DropGamePacket()
    {
      this.dispatcherImpl.DropGamePacket();
    }

    public void DropUtilPacket()
    {
      this.dispatcherImpl.DropUtilPacket();
    }

    public bool GameServerHasEvents()
    {
      return this.dispatcherImpl.GameServerHasEvents();
    }

    public PegasusUtil.AccountLicenseAchieveResponse GetAccountLicenseAchieveResponse()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.AccountLicenseAchieveResponse>(311);
    }

    public AccountLicensesInfoResponse GetAccountLicensesInfoResponse()
    {
      return this.UnpackNextUtilPacket<AccountLicensesInfoResponse>(325);
    }

    public AchievementNotifications GetAchievementNotifications()
    {
      return this.UnpackNextUtilPacket<AchievementNotifications>(333);
    }

    public Achieves GetAchieves()
    {
      return this.UnpackNextUtilPacket<Achieves>(252);
    }

    public AdventureProgressResponse GetAdventureProgressResponse()
    {
      return this.UnpackNextUtilPacket<AdventureProgressResponse>(306);
    }

    public void GetAllClientOptions()
    {
      this.SendUtilPacket(240, 0, (IProtoBuf) new GetOptions(), RequestPhase.RUNNING, 0);
    }

    public HeroXP GetAllHeroXp()
    {
      return this.UnpackNextUtilPacket<HeroXP>(283);
    }

    public ArcaneDustBalance GetArcaneDustBalance()
    {
      return this.UnpackNextUtilPacket<ArcaneDustBalance>(262);
    }

    public GetAssetResponse GetAssetResponse()
    {
      return this.UnpackNextUtilPacket<GetAssetResponse>(322);
    }

    public AssetsVersionResponse GetAssetsVersionResponse()
    {
      return this.UnpackNextUtilPacket<AssetsVersionResponse>(304);
    }

    public BattlePayConfigResponse GetBattlePayConfigResponse()
    {
      return this.UnpackNextUtilPacket<BattlePayConfigResponse>(238);
    }

    public BattlePayStatusResponse GetBattlePayStatusResponse()
    {
      return this.UnpackNextUtilPacket<BattlePayStatusResponse>(265);
    }

    public BoosterList GetBoosters()
    {
      return this.UnpackNextUtilPacket<BoosterList>(224);
    }

    public CancelQuestResponse GetCanceledQuestResponse()
    {
      return this.UnpackNextUtilPacket<CancelQuestResponse>(282);
    }

    public SetCardBackResponse GetCardBackResponse()
    {
      return this.UnpackNextUtilPacket<SetCardBackResponse>(292);
    }

    public CardBacks GetCardBacks()
    {
      return this.UnpackNextUtilPacket<CardBacks>(236);
    }

    public BoughtSoldCard GetCardSaleResult()
    {
      return this.UnpackNextUtilPacket<BoughtSoldCard>(258);
    }

    public CardValues GetCardValues()
    {
      return this.UnpackNextUtilPacket<CardValues>(260);
    }

    public ClientStaticAssetsResponse GetClientStaticAssetsResponse()
    {
      return this.UnpackNextUtilPacket<ClientStaticAssetsResponse>(341);
    }

    public Collection GetCollectionCardStacks()
    {
      return this.UnpackNextUtilPacket<Collection>(207);
    }

    public PegasusUtil.DBAction GetDbAction()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.DBAction>(216);
    }

    public Deadend GetDeadendGame()
    {
      return this.UnpackNextGamePacket<Deadend>(169);
    }

    public DeadendUtil GetDeadendUtil()
    {
      return this.UnpackNextUtilPacket<DeadendUtil>(167);
    }

    public DebugCommandResponse GetDebugCommandResponse()
    {
      return this.UnpackNextUtilPacket<DebugCommandResponse>(324);
    }

    public DebugConsoleCommand GetDebugConsoleCommand()
    {
      if (!this.dispatcherImpl.DebugConnectionManager.AllowDebugConnections())
        return (DebugConsoleCommand) null;
      return this.UnpackNextDebugPacket<DebugConsoleCommand>(123);
    }

    public BobNetProto.DebugConsoleResponse GetDebugConsoleResponse()
    {
      if (!this.dispatcherImpl.DebugConnectionManager.AllowDebugConnections())
        return (BobNetProto.DebugConsoleResponse) null;
      return this.UnpackNextGamePacket<BobNetProto.DebugConsoleResponse>(124);
    }

    public GetDeckContentsResponse GetDeckContentsResponse()
    {
      return this.UnpackNextUtilPacket<GetDeckContentsResponse>(215);
    }

    public DeckList GetDeckHeaders()
    {
      return this.UnpackNextUtilPacket<DeckList>(202);
    }

    public ProfileDeckLimit GetDeckLimit()
    {
      return this.UnpackNextUtilPacket<ProfileDeckLimit>(231);
    }

    public Disconnected GetDisconnectedGameInfo()
    {
      return this.UnpackNextUtilPacket<Disconnected>(289);
    }

    public PegasusUtil.DraftRetired GetDraftRetired()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.DraftRetired>(247);
    }

    public PegasusGame.EntitiesChosen GetEntitiesChosen()
    {
      return this.UnpackNextGamePacket<PegasusGame.EntitiesChosen>(13);
    }

    public PegasusGame.EntityChoices GetEntityChoices()
    {
      return this.UnpackNextGamePacket<PegasusGame.EntityChoices>(17);
    }

    public FavoriteHeroesResponse GetFavoriteHeroesResponse()
    {
      return this.UnpackNextUtilPacket<FavoriteHeroesResponse>(318);
    }

    public GuardianVars GetGuardianVars()
    {
      return this.UnpackNextUtilPacket<GuardianVars>(264);
    }

    public GameCanceled GetGameCancelInfo()
    {
      return this.UnpackNextGamePacket<GameCanceled>(12);
    }

    public PegasusGame.GameSetup GetGameSetup()
    {
      return this.UnpackNextGamePacket<PegasusGame.GameSetup>(16);
    }

    public GamesInfo GetGamesInfo()
    {
      return this.UnpackNextUtilPacket<GamesInfo>(208);
    }

    public GameStartState GetGameStartState()
    {
      return this.dispatcherImpl.GameStartState;
    }

    public void SetGameStartState(GameStartState state)
    {
      this.dispatcherImpl.GameStartState = state;
    }

    public void GetGameState()
    {
      this.SendGamePacket(1, (IProtoBuf) new GetGameState());
    }

    public PegasusUtil.GenericResponse GetGenericResponse()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.GenericResponse>(326);
    }

    public GoldBalance GetGoldBalance()
    {
      return this.UnpackNextUtilPacket<GoldBalance>(278);
    }

    public PegasusUtil.MassDisenchantResponse GetMassDisenchantResponse()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.MassDisenchantResponse>(269);
    }

    public MedalInfo GetMedalInfo()
    {
      return this.UnpackNextUtilPacket<MedalInfo>(232);
    }

    public NAckOption GetNAckOption()
    {
      return this.UnpackNextGamePacket<NAckOption>(10);
    }

    public NoticeNotifications GetNoticeNotifications()
    {
      return this.UnpackNextUtilPacket<NoticeNotifications>(334);
    }

    public NotSoMassiveLoginReply GetNotSoMassiveLoginReply()
    {
      return this.UnpackNextUtilPacket<NotSoMassiveLoginReply>(300);
    }

    public BoosterContent GetOpenedBooster()
    {
      return this.UnpackNextUtilPacket<BoosterContent>(226);
    }

    public AllOptions GetAllOptions()
    {
      return this.UnpackNextGamePacket<AllOptions>(14);
    }

    public PlayerRecords GetPlayerRecords()
    {
      return this.UnpackNextUtilPacket<PlayerRecords>(270);
    }

    public PlayQueue GetPlayQueue()
    {
      return this.UnpackNextUtilPacket<PlayQueue>(286);
    }

    public PegasusGame.PowerHistory GetPowerHistory()
    {
      return this.UnpackNextGamePacket<PegasusGame.PowerHistory>(19);
    }

    public ProcessRecruitAFriendResponse GetProcessRecruitAFriendResponse()
    {
      return this.UnpackNextUtilPacket<ProcessRecruitAFriendResponse>(342);
    }

    public NoticeNotifications GetProfileNoticeNotifications()
    {
      return this.UnpackNextUtilPacket<NoticeNotifications>(334);
    }

    public PegasusUtil.ProfileNotices GetProfileNotices()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.ProfileNotices>(212);
    }

    public ProfileProgress GetProfileProgress()
    {
      return this.UnpackNextUtilPacket<ProfileProgress>(233);
    }

    public CancelPurchaseResponse GetCancelPurchaseResponse()
    {
      return this.UnpackNextUtilPacket<CancelPurchaseResponse>(275);
    }

    public PegasusUtil.PurchaseMethod GetPurchaseMethodResponse()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.PurchaseMethod>(272);
    }

    public PegasusUtil.PurchaseResponse GetPurchaseResponse()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.PurchaseResponse>(256);
    }

    public PurchaseWithGoldResponse GetPurchaseWithGoldResponse()
    {
      return this.UnpackNextUtilPacket<PurchaseWithGoldResponse>(280);
    }

    public RecruitAFriendDataResponse GetRecruitAFriendDataResponse()
    {
      return this.UnpackNextUtilPacket<RecruitAFriendDataResponse>(338);
    }

    public RecruitAFriendURLResponse GetRecruitAFriendUrlResponse()
    {
      return this.UnpackNextUtilPacket<RecruitAFriendURLResponse>(336);
    }

    public RewardProgress GetRewardProgress()
    {
      return this.UnpackNextUtilPacket<RewardProgress>(271);
    }

    public ServerResult GetServerResult()
    {
      return this.UnpackNextGamePacket<ServerResult>(23);
    }

    public PegasusUtil.SetFavoriteHeroResponse GetSetFavoriteHeroResponse()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.SetFavoriteHeroResponse>(320);
    }

    public SetProgressResponse GetSetProgressResponse()
    {
      return this.UnpackNextUtilPacket<SetProgressResponse>(296);
    }

    public SpectatorNotify GetSpectatorNotify()
    {
      return this.UnpackNextGamePacket<SpectatorNotify>(24);
    }

    public SubscribeResponse GetSubscribeResponse()
    {
      return this.UnpackNextUtilPacket<SubscribeResponse>(315);
    }

    public TavernBrawlInfo GetTavernBrawlInfo()
    {
      return this.UnpackNextUtilPacket<TavernBrawlInfo>(316);
    }

    public TavernBrawlPlayerRecordResponse GeTavernBrawlPlayerRecordResponse()
    {
      return this.UnpackNextUtilPacket<TavernBrawlPlayerRecordResponse>(317);
    }

    public void GetThirdPartyPurchaseStatus(string transactionId)
    {
      this.SendUtilPacket(294, 1, (IProtoBuf) new GetThirdPartyPurchaseStatus()
      {
        ThirdPartyId = transactionId
      }, RequestPhase.RUNNING, 0);
    }

    public PegasusUtil.ThirdPartyPurchaseStatusResponse GetThirdPartyPurchaseStatusResponse()
    {
      return this.UnpackNextUtilPacket<PegasusUtil.ThirdPartyPurchaseStatusResponse>(295);
    }

    public TriggerEventResponse GetTriggerEventResponse()
    {
      return this.UnpackNextUtilPacket<TriggerEventResponse>(299);
    }

    public PegasusGame.TurnTimer GetTurnTimerInfo()
    {
      return this.UnpackNextGamePacket<PegasusGame.TurnTimer>(9);
    }

    public UpdateAccountLicensesResponse GetUpdateAccountLicensesResponse()
    {
      return this.UnpackNextUtilPacket<UpdateAccountLicensesResponse>(331);
    }

    public UpdateLoginComplete GetUpdateLoginComplete()
    {
      return this.UnpackNextUtilPacket<UpdateLoginComplete>(307);
    }

    public PegasusGame.UserUI GetUserUi()
    {
      return this.UnpackNextGamePacket<PegasusGame.UserUI>(15);
    }

    public ValidateAchieveResponse GetValidateAchieveResponse()
    {
      return this.UnpackNextUtilPacket<ValidateAchieveResponse>(285);
    }

    public bool GotoGameServer(string address, int port)
    {
      return this.dispatcherImpl.ConnectToGameServer(address, port);
    }

    public void SendSpectatorGameHandshake(string version, Platform platform, bgs.types.GameServerInfo info, BnetId bnetId)
    {
      this.SendGamePacket(22, (IProtoBuf) new SpectatorHandshake()
      {
        GameHandle = info.GameHandle,
        Password = info.SpectatorPassword,
        Version = version,
        Platform = platform,
        GameAccountId = bnetId
      });
    }

    public void SendGameHandshake(bgs.types.GameServerInfo info, Platform platform)
    {
      this.SendGamePacket(168, (IProtoBuf) new Handshake()
      {
        Password = info.AuroraPassword,
        GameHandle = info.GameHandle,
        ClientHandle = (long) (int) info.ClientHandle,
        Mission = info.Mission,
        Version = info.Version,
        Platform = platform
      });
    }

    public bool HasErrors()
    {
      return this.dispatcherImpl.HasUtilErrors();
    }

    public bool HasDebugPackets()
    {
      return this.dispatcherImpl.DebugConnectionManager.HaveDebugPackets();
    }

    public bool HasGamePackets()
    {
      return this.dispatcherImpl.HasGamePackets();
    }

    public bool HasGameServerConnection()
    {
      return this.dispatcherImpl.HasGameServerConnection();
    }

    public bool HasUtilPackets()
    {
      return this.dispatcherImpl.HasUtilPackets();
    }

    public bool IsConnectedToGameServer()
    {
      return this.dispatcherImpl.IsConnectedToGameServer();
    }

    public void UpdateGameServerConnection()
    {
      this.dispatcherImpl.ProcessGamePackets();
    }

    public void ProcessUtilPackets()
    {
      this.dispatcherImpl.ProcessUtilPackets();
    }

    public bool TryConnectDebugConsole()
    {
      return this.dispatcherImpl.DebugConnectionManager.TryConnectDebugConsole();
    }

    public void UpdateDebugConsole()
    {
      this.dispatcherImpl.DebugConnectionManager.Update();
    }

    public void MassDisenchant()
    {
      this.SendUtilPacket(268, 0, (IProtoBuf) new MassDisenchantRequest(), RequestPhase.RUNNING, 0);
    }

    public PegasusPacket NextDebugPacket()
    {
      return this.dispatcherImpl.DebugConnectionManager.NextDebugPacket();
    }

    public int NextDebugPacketType()
    {
      return this.dispatcherImpl.DebugConnectionManager.NextDebugConsoleType();
    }

    public PegasusPacket NextGamePacket()
    {
      return this.dispatcherImpl.NextGamePacket();
    }

    public int NextGamePacketType()
    {
      return this.dispatcherImpl.NextGameType();
    }

    public PegasusPacket NextUtilPacket()
    {
      return this.dispatcherImpl.NextUtilPacket();
    }

    public int NextUtilPacketType()
    {
      return this.dispatcherImpl.NextUtilType();
    }

    public void OnDebugPacketReceived(PegasusPacket packet)
    {
      this.dispatcherImpl.DebugConnectionManager.OnPacketReceived(packet);
    }

    public void OnGamePacketReceived(PegasusPacket packet, int packetTypeId)
    {
      this.dispatcherImpl.OnGamePacketReceived(packet, packetTypeId);
    }

    public void OnLoginComplete()
    {
      this.dispatcherImpl.OnLoginComplete();
    }

    public void OnLoginStarted()
    {
      this.dispatcherImpl.DebugConnectionManager.OnLoginStarted();
    }

    public void OnUtilPacketReceived(PegasusPacket packet, int packetTypeId)
    {
      this.dispatcherImpl.OnUtilPacketReceived(packet, packetTypeId);
    }

    public void OnStartupPacketSequenceComplete()
    {
      this.dispatcherImpl.OnStartupPacketSequenceComplete();
    }

    public void OpenBooster(int boosterTypeId)
    {
      this.SendUtilPacket(225, 0, (IProtoBuf) new OpenBooster()
      {
        BoosterType = boosterTypeId
      }, RequestPhase.RUNNING, 0);
    }

    public void PurchaseViaGold(int quantity, ProductType product, int data)
    {
      this.SendUtilPacket(279, 0, (IProtoBuf) new PurchaseWithGold()
      {
        Product = product,
        Quantity = quantity,
        Data = data
      }, RequestPhase.RUNNING, 0);
    }

    public ClientOptions GetClientOptions()
    {
      return this.UnpackNextUtilPacket<ClientOptions>(241);
    }

    public void RenameDeck(long deckId, string name)
    {
      this.SendUtilPacket(211, 0, (IProtoBuf) new RenameDeck()
      {
        Deck = deckId,
        Name = name
      }, RequestPhase.RUNNING, 0);
    }

    public void RequestAccountLicensesUpdate()
    {
      this.SendUtilPacket(276, 1, (IProtoBuf) new UpdateAccountLicenses(), RequestPhase.RUNNING, 0);
    }

    public void RequestAchieves(string deviceModel)
    {
      GetAchieves getAchieves = new GetAchieves();
      if (deviceModel != null)
        getAchieves.DeviceModel = deviceModel;
      this.SendUtilPacket(253, 0, (IProtoBuf) getAchieves, RequestPhase.RUNNING, 0);
    }

    public void RequestAdventureProgress()
    {
      this.SendUtilPacket(305, 0, (IProtoBuf) new GetAdventureProgress(), RequestPhase.RUNNING, 0);
    }

    public void RequestAssetsVersion(Platform platform)
    {
      this.SendUtilPacket(303, 0, (IProtoBuf) new GetAssetsVersion()
      {
        Platform = platform
      }, RequestPhase.STARTUP, 0);
    }

    public void RequestBattlePayConfig()
    {
      this.SendUtilPacket(237, 1, (IProtoBuf) new GetBattlePayConfig(), RequestPhase.RUNNING, 0);
    }

    public void RequestBattlePayStatus()
    {
      this.SendUtilPacket((int) byte.MaxValue, 1, (IProtoBuf) new GetBattlePayStatus(), RequestPhase.RUNNING, 0);
    }

    public void RequestCancelQuest(int questId)
    {
      this.SendUtilPacket(281, 0, (IProtoBuf) new CancelQuest()
      {
        QuestId = questId
      }, RequestPhase.RUNNING, 0);
    }

    public void RequestDeckContents(long[] deckIds)
    {
      GetDeckContents getDeckContents = new GetDeckContents();
      getDeckContents.DeckId.AddRange((IEnumerable<long>) deckIds);
      this.SendUtilPacket(214, 0, (IProtoBuf) getDeckContents, RequestPhase.RUNNING, 0);
    }

    public void RequestAccountInfoNetCacheObject(GetAccountInfo.Request request)
    {
      this.SendUtilPacket(201, 0, (IProtoBuf) new GetAccountInfo()
      {
        Request_ = request
      }, RequestPhase.RUNNING, (int) request);
    }

    public void RequestNetCacheObjectList(List<GetAccountInfo.Request> requests, List<GenericRequest> genericRequests)
    {
      GenericRequestList genericRequestList = new GenericRequestList();
      using (List<GetAccountInfo.Request>.Enumerator enumerator = requests.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GetAccountInfo.Request current = enumerator.Current;
          genericRequestList.Requests.Add(new GenericRequest()
          {
            RequestId = 201,
            RequestSubId = (int) current
          });
        }
      }
      if (genericRequests != null)
      {
        using (List<GenericRequest>.Enumerator enumerator = genericRequests.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            GenericRequest current = enumerator.Current;
            genericRequestList.Requests.Add(current);
          }
        }
      }
      this.SendUtilPacket(327, 0, (IProtoBuf) genericRequestList, RequestPhase.RUNNING, 0);
    }

    public void RequestProcessRecruitAFriend()
    {
      this.SendUtilPacket(339, 2, (IProtoBuf) new ProcessRecruitAFriend(), RequestPhase.RUNNING, 0);
    }

    public void RequestPurchaseMethod(string productId, int quantity, int currency, string deviceId, Platform platform)
    {
      this.SendUtilPacket(250, 1, (IProtoBuf) new GetPurchaseMethod()
      {
        ProductId = productId,
        Quantity = quantity,
        Currency = currency,
        DeviceId = deviceId,
        Platform = platform
      }, RequestPhase.RUNNING, 0);
    }

    public void RequestRecruitAFriendData()
    {
      this.SendUtilPacket(337, 2, (IProtoBuf) new GetRecruitAFriendData(), RequestPhase.RUNNING, 0);
    }

    public void RequestRecruitAFriendUrl(Platform platform)
    {
      this.SendUtilPacket(335, 2, (IProtoBuf) new GetRecruitAFriendURL()
      {
        Platform = platform
      }, RequestPhase.RUNNING, 0);
    }

    public void SellCard(PegasusShared.CardDef cardDef, int count, int unitSellPrice, int currentCollectionCount)
    {
      BuySellCard buySellCard = new BuySellCard() { Def = cardDef, Buying = false, UnitSellPrice = unitSellPrice, CurrentCollectionCount = currentCollectionCount };
      if (count != 1)
        buySellCard.Count = count;
      this.SendUtilPacket(257, 0, (IProtoBuf) buySellCard, RequestPhase.RUNNING, 0);
    }

    public void SendAssetRequest(int clientToken, List<AssetKey> requestKeys)
    {
      this.SendUtilPacket(321, 0, (IProtoBuf) new GetAssetRequest()
      {
        ClientToken = clientToken,
        Requests = requestKeys
      }, RequestPhase.RUNNING, 0);
    }

    public void SendChoices(int choicesId, List<int> picks)
    {
      this.SendGamePacket(3, (IProtoBuf) new ChooseEntities()
      {
        Id = choicesId,
        Entities = picks
      });
    }

    public void SendDebugCommandRequest(DebugCommandRequest packet)
    {
      this.SendUtilPacket(322, 0, (IProtoBuf) packet, RequestPhase.RUNNING, 0);
    }

    public void SendDebugConsoleResponse(int responseType, string message)
    {
      this.dispatcherImpl.DebugConnectionManager.SendDebugConsoleResponse(responseType, message);
    }

    public void SendDeckData(DeckSetData packet)
    {
      this.SendUtilPacket(222, 0, (IProtoBuf) packet, RequestPhase.RUNNING, 0);
    }

    public void SendDeckTemplateSource(long deckId, int templateId)
    {
      this.SendUtilPacket(332, 0, (IProtoBuf) new DeckSetTemplateSource()
      {
        Deck = deckId,
        TemplateId = templateId
      }, RequestPhase.RUNNING, 0);
    }

    public void SendEmote(int emoteId)
    {
      this.SendGamePacket(15, (IProtoBuf) new PegasusGame.UserUI()
      {
        Emote = emoteId
      });
    }

    public bool AllowDebugConnections()
    {
      return this.dispatcherImpl.DebugConnectionManager.AllowDebugConnections();
    }

    public void SendDebugConsoleCommand(string command)
    {
      this.SendGamePacket(123, (IProtoBuf) new DebugConsoleCommand()
      {
        Command = command
      });
    }

    public void SendOption(int choiceId, int index, int target, int subOption, int position)
    {
      this.SendGamePacket(2, (IProtoBuf) new ChooseOption()
      {
        Id = choiceId,
        Index = index,
        Target = target,
        SubOption = subOption,
        Position = position
      });
    }

    public void SendPing()
    {
      this.SendGamePacket(115, (IProtoBuf) new Ping());
    }

    public void SendRemoveAllSpectators(bool regeneratePassword)
    {
      this.SendGamePacket(26, (IProtoBuf) new RemoveSpectators()
      {
        KickAllSpectators = true,
        RegenerateSpectatorPassword = regeneratePassword
      });
    }

    public void SendRemoveSpectators(bool regeneratePassword, List<BnetId> spectators)
    {
      this.SendGamePacket(26, (IProtoBuf) new RemoveSpectators()
      {
        RegenerateSpectatorPassword = regeneratePassword,
        TargetGameaccountIds = spectators
      });
    }

    public void SendSpectatorInvite(BnetId targetBnetId, BnetId targetGameAccountId)
    {
      this.SendGamePacket(25, (IProtoBuf) new InviteToSpectate()
      {
        TargetBnetAccountId = targetBnetId,
        TargetGameAccountId = targetGameAccountId
      });
    }

    public void SendUnsubscribeRequest(Unsubscribe packet, int systemChannel)
    {
      this.SendUtilPacket(329, systemChannel, (IProtoBuf) packet, RequestPhase.RUNNING, 0);
    }

    public void SendUserUi(int overCard, int heldCard, int arrowOrigin, int x, int y)
    {
      this.SendGamePacket(15, (IProtoBuf) new PegasusGame.UserUI()
      {
        MouseInfo = new PegasusGame.MouseInfo()
        {
          ArrowOrigin = arrowOrigin,
          OverCard = overCard,
          HeldCard = heldCard,
          X = x,
          Y = y
        }
      });
    }

    public void SetClientOptions(SetOptions packet)
    {
      this.SendUtilPacket(239, 0, (IProtoBuf) packet, RequestPhase.RUNNING, 0);
    }

    public void SetDeckCardBack(int cardBack, long deckId)
    {
      this.SendUtilPacket(291, 0, (IProtoBuf) new SetCardBack()
      {
        CardBack = cardBack,
        DeckId = deckId
      }, RequestPhase.RUNNING, 0);
    }

    public void SetDefaultCardBack(int cardBack)
    {
      this.SendUtilPacket(291, 0, (IProtoBuf) new SetCardBack()
      {
        CardBack = cardBack
      }, RequestPhase.RUNNING, 0);
    }

    public void SetDisconnectedFromBattleNet()
    {
      this.dispatcherImpl.SetDisconnectedFromBattleNet();
    }

    public void SetFavoriteHero(int classId, PegasusShared.CardDef heroCardDef)
    {
      this.SendUtilPacket(319, 0, (IProtoBuf) new SetFavoriteHero()
      {
        FavoriteHero = new FavoriteHero()
        {
          ClassId = classId,
          Hero = heroCardDef
        }
      }, RequestPhase.RUNNING, 0);
    }

    public void SetProgress(long value)
    {
      this.SendUtilPacket(230, 0, (IProtoBuf) new SetProgress()
      {
        Value = value
      }, RequestPhase.STARTUP, 0);
    }

    public bool ShouldIgnoreError(BnetErrorInfo errorInfo)
    {
      return this.dispatcherImpl.ShouldIgnoreError(errorInfo);
    }

    public void SubmitThirdPartyPurchaseReceipt(long bpayId, string thirdPartyId, string base64Receipt, string thirdPartyUserId)
    {
      ThirdPartyReceiptData partyReceiptData = new ThirdPartyReceiptData() { ThirdPartyId = thirdPartyId, Receipt = base64Receipt };
      if (!string.IsNullOrEmpty(thirdPartyUserId))
        partyReceiptData.ThirdPartyUserId = thirdPartyUserId;
      this.SendUtilPacket(293, 1, (IProtoBuf) new SubmitThirdPartyReceipt()
      {
        TransactionId = bpayId,
        ReceiptData = partyReceiptData
      }, RequestPhase.RUNNING, 0);
    }

    public double GetTimeLastPingReceieved()
    {
      return this.dispatcherImpl.TimeLastPingReceived;
    }

    public void SetTimeLastPingReceived(double time)
    {
      this.dispatcherImpl.TimeLastPingReceived = time;
    }

    public double GetTimeLastPingSent()
    {
      return this.dispatcherImpl.TimeLastPingSent;
    }

    public void SetTimeLastPingSent(double time)
    {
      this.dispatcherImpl.TimeLastPingSent = time;
    }

    public void TriggerLaunchEvent(ulong lastPlayedBnetHi, ulong lastPlayedBnetLo, ulong lastPlayedStartTime, ulong otherPlayerBnetHi, ulong otherPlayerBnetLo, ulong otherPlayerStartTime)
    {
      this.SendUtilPacket(298, 0, (IProtoBuf) new TriggerLaunchDayEvent()
      {
        LastPlayed = new PegasusUtil.NearbyPlayer()
        {
          BnetIdHi = lastPlayedBnetHi,
          BnetIdLo = lastPlayedBnetLo,
          SessionStartTime = lastPlayedStartTime
        },
        OtherPlayer = new PegasusUtil.NearbyPlayer()
        {
          BnetIdHi = otherPlayerBnetHi,
          BnetIdLo = otherPlayerBnetLo,
          SessionStartTime = otherPlayerStartTime
        }
      }, RequestPhase.RUNNING, 0);
    }

    public void SetPingsSinceLastPong(int value)
    {
      this.dispatcherImpl.PingsSinceLastPong = value;
    }

    public int GetPingsSinceLastPong()
    {
      return this.dispatcherImpl.PingsSinceLastPong;
    }

    public void ValidateAchieve(int achieveId)
    {
      this.SendUtilPacket(284, 0, (IProtoBuf) new ValidateAchieve()
      {
        Achieve = achieveId
      }, RequestPhase.RUNNING, 0);
    }

    public void RequestTavernBrawlSessionBegin()
    {
      this.SendUtilPacket(343, 0, (IProtoBuf) new TavernBrawlRequestSessionBegin(), RequestPhase.RUNNING, 0);
    }

    public void TavernBrawlRetire()
    {
      this.SendUtilPacket(344, 0, (IProtoBuf) new TavernBrawlRequestSessionRetire(), RequestPhase.RUNNING, 0);
    }

    public void AckTavernBrawlSessionRewards()
    {
      this.SendUtilPacket(345, 0, (IProtoBuf) new TavernBrawlAckSessionRewards(), RequestPhase.RUNNING, 0);
    }

    public TavernBrawlRequestSessionBeginResponse GetTavernBrawlSessionBeginResponse()
    {
      return this.UnpackNextUtilPacket<TavernBrawlRequestSessionBeginResponse>(347);
    }

    public TavernBrawlRequestSessionRetireResponse GetTavernBrawlSessionRetired()
    {
      return this.UnpackNextUtilPacket<TavernBrawlRequestSessionRetireResponse>(348);
    }

    private void SendGamePacket(int packetId, IProtoBuf body)
    {
      this.dispatcherImpl.SendGamePacket(packetId, body);
    }

    private void SendUtilPacket(int type, int system, IProtoBuf body, RequestPhase requestPhase = RequestPhase.RUNNING, int subId = 0)
    {
      this.dispatcherImpl.SendUtilPacket(type, system, body, requestPhase, subId);
    }

    private T UnpackNextDebugPacket<T>(int packetId) where T : IProtoBuf
    {
      return this.Unpack<T>(this.dispatcherImpl.DebugConnectionManager.NextDebugPacket(), packetId);
    }

    private T UnpackNextGamePacket<T>(int packetId) where T : IProtoBuf
    {
      return this.Unpack<T>(this.dispatcherImpl.NextGamePacket(), packetId);
    }

    private T UnpackNextUtilPacket<T>(int packetId) where T : IProtoBuf
    {
      return this.Unpack<T>(this.dispatcherImpl.NextUtilPacket(), packetId);
    }

    private T Unpack<T>(PegasusPacket p, int packetId) where T : IProtoBuf
    {
      if (p == null || p.Type != packetId || !(p.Body is T))
        return default (T);
      return (T) p.Body;
    }
  }
}
