﻿// Decompiled with JetBrains decompiler
// Type: Networking.DefaultProtobufPacketDecoder`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.IO;

namespace Networking
{
  internal class DefaultProtobufPacketDecoder<T> : IPacketDecoder where T : IProtoBuf, new()
  {
    public PegasusPacket DecodePacket(PegasusPacket packet)
    {
      byte[] body = (byte[]) packet.Body;
      T obj = new T();
      obj.Deserialize((Stream) new MemoryStream(body));
      packet.Body = (object) obj;
      return packet;
    }
  }
}
