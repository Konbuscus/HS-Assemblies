﻿// Decompiled with JetBrains decompiler
// Type: MemModule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using VerifyScanSignature;

public class MemModule
{
  private static MemModule.MemModuleFunc s_memModuleFunc = (MemModule.MemModuleFunc) null;
  private static IntPtr s_memModule32 = IntPtr.Zero;
  private static uint MAX_OUTPUT_SIZE = 4096;
  private static byte[] s_outputBuffer = new byte[(IntPtr) MemModule.MAX_OUTPUT_SIZE];
  private static List<AuthenticationAPI.MemModuleLoadRequest> s_memModuleRequests = new List<AuthenticationAPI.MemModuleLoadRequest>();
  private static List<KeyValuePair<AuthenticationAPI.MemModuleLoadRequest, byte[]>> s_memModuleResponses = new List<KeyValuePair<AuthenticationAPI.MemModuleLoadRequest, byte[]>>();
  private static Thread s_thread = (Thread) null;
  private const int WAIT_INTERVAL_MILLISECONDS = 500;

  public static bool Process()
  {
    lock (MemModule.s_memModuleRequests)
    {
      for (AuthenticationAPI.MemModuleLoadRequest local_1 = BattleNet.NextMemModuleRequest(); local_1 != null; local_1 = BattleNet.NextMemModuleRequest())
        MemModule.s_memModuleRequests.Add(local_1);
    }
    if (MemModule.s_memModuleRequests.Count > 0 && !MemModule.Init())
      return false;
    int num = 0;
    lock (MemModule.s_memModuleResponses)
    {
      for (int local_4 = 0; local_4 < MemModule.s_memModuleResponses.Count; ++local_4)
        BattleNet.SendMemModuleResponse(MemModule.s_memModuleResponses[local_4].Key, MemModule.s_memModuleResponses[local_4].Value);
      num = MemModule.s_memModuleResponses.Count;
      MemModule.s_memModuleResponses.Clear();
    }
    return num > 0;
  }

  private static bool Init()
  {
    if (MemModule.s_memModuleFunc != null)
      return true;
    string pluginPath = FileUtils.GetPluginPath("MemModule32.dll");
    if (!Verifier.VerifyStreamSignature((Stream) File.Open(pluginPath, FileMode.Open, FileAccess.Read, FileShare.Read | FileShare.Delete), "tt0253927", 16384))
    {
      Error.AddDevFatal("MemModule: Failed to validate signature for: \"{0}\"!", (object) pluginPath);
      return false;
    }
    MemModule.s_memModule32 = FileUtils.LoadPlugin("MemModule32", true);
    if (MemModule.s_memModule32 != IntPtr.Zero)
    {
      MemModule.s_memModuleFunc = (MemModule.MemModuleFunc) Marshal.GetDelegateForFunctionPointer(DLLUtils.GetProcAddress(MemModule.s_memModule32, new IntPtr(IntPtr.Zero.ToInt32() + 1)), typeof (MemModule.MemModuleFunc));
      if (MemModule.s_thread != null && MemModule.s_thread.ThreadState == ThreadState.Running)
        MemModule.s_thread.Abort();
      MemModule.s_thread = new Thread(new ThreadStart(MemModule.Work));
      MemModule.s_thread.Start();
      return MemModule.s_memModuleFunc != null;
    }
    Error.AddDevFatal("MemModule: Failed to load: \"{0}\"!", (object) pluginPath);
    return false;
  }

  private static void Work()
  {
    while (true)
    {
      AuthenticationAPI.MemModuleLoadRequest key = (AuthenticationAPI.MemModuleLoadRequest) null;
      lock (MemModule.s_memModuleRequests)
      {
        if (MemModule.s_memModuleRequests.Count > 0)
        {
          key = MemModule.s_memModuleRequests[0];
          MemModule.s_memModuleRequests.RemoveAt(0);
        }
      }
      if (key != null)
      {
        uint outputSize = 0;
        if (MemModule.s_memModuleFunc != null)
        {
          int num = MemModule.s_memModuleFunc(key.m_input, key.m_input, MemModule.s_outputBuffer, ref outputSize, ref MemModule.MAX_OUTPUT_SIZE) ? 1 : 0;
        }
        byte[] array = ((IEnumerable<byte>) MemModule.s_outputBuffer).Take<byte>((int) outputSize).ToArray<byte>();
        lock (MemModule.s_memModuleResponses)
          MemModule.s_memModuleResponses.Add(new KeyValuePair<AuthenticationAPI.MemModuleLoadRequest, byte[]>(key, array));
      }
      Thread.Sleep(500);
    }
  }

  public static void AppQuit()
  {
    if (MemModule.s_memModule32 != IntPtr.Zero)
      DLLUtils.FreeLibrary(MemModule.s_memModule32);
    if (MemModule.s_thread == null)
      return;
    MemModule.s_thread.Abort();
  }

  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  private delegate bool MemModuleFunc(byte[] input, byte[] unused, byte[] output, ref uint outputSize, ref uint maxOutputSize);
}
