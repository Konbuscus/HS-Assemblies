﻿// Decompiled with JetBrains decompiler
// Type: ArenaMedal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArenaMedal : PegUIElement
{
  public const string MEDAL_TEXTURE_PREFIX = "Medal_Key_";
  public const string MEDAL_NAME_PREFIX = "GLOBAL_ARENA_MEDAL_";
  public GameObject m_rankMedal;
  private int m_medal;

  protected override void Awake()
  {
    base.Awake();
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.MedalOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.MedalOut));
  }

  public void SetMedal(int medal)
  {
    this.m_medal = medal;
    AssetLoader.Get().LoadTexture("Medal_Key_" + (object) (medal + 1), new AssetLoader.ObjectCallback(this.OnTextureLoaded), (object) null, false);
  }

  private void MedalOver(UIEvent e)
  {
    string medalName;
    string bodytext;
    if (Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE) || SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
    {
      medalName = this.GetMedalName();
      bodytext = GameStrings.Format("GLOBAL_MEDAL_ARENA_TOOLTIP_BODY", (object) this.m_medal);
    }
    else
    {
      medalName = GameStrings.Get("GLUE_TOURNAMENT_UNRANKED_MODE");
      bodytext = GameStrings.Get("GLUE_TOURNAMENT_UNRANKED_DESC");
    }
    this.gameObject.GetComponent<TooltipZone>().ShowLayerTooltip(medalName, bodytext);
  }

  private void MedalOut(UIEvent e)
  {
    this.gameObject.GetComponent<TooltipZone>().HideTooltip();
  }

  private void OnTextureLoaded(string assetName, Object asset, object callbackData)
  {
    if (asset == (Object) null)
    {
      Debug.LogWarning((object) string.Format("ArenaMedal.OnTextureLoaded(): asset for {0} is null!", (object) assetName));
    }
    else
    {
      Texture texture = asset as Texture;
      if ((Object) texture == (Object) null)
        Debug.LogWarning((object) string.Format("ArenaMedal.OnTextureLoaded(): medalTexture for {0} is null (asset is not a texture)!", (object) assetName));
      else
        this.m_rankMedal.GetComponent<Renderer>().material.mainTexture = texture;
    }
  }

  private string GetMedalName()
  {
    return GameStrings.Get("GLOBAL_ARENA_MEDAL_" + (object) this.m_medal);
  }

  private string GetNextMedalName()
  {
    string key = "GLOBAL_ARENA_MEDAL_" + (object) (this.m_medal + 1);
    string str = GameStrings.Get(key);
    if (str == key)
      return string.Empty;
    return str;
  }
}
