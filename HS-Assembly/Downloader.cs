﻿// Decompiled with JetBrains decompiler
// Type: Downloader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Downloader : MonoBehaviour
{
  private static int m_remoteUriCNIndex = 1;
  private static Downloader s_instance;
  private List<string> m_bundlesToDownload;
  private static string m_remoteUri;
  private static string m_remoteUriCN;
  private bool m_isDownloading;
  private Map<string, AssetBundle> m_downloadableBundles;
  private float m_downloadStartTime;
  private bool m_hasShownInternalFailureAlert;

  private void Awake()
  {
    Downloader.s_instance = this;
    this.m_bundlesToDownload = new List<string>();
    this.m_downloadableBundles = new Map<string, AssetBundle>();
    if (ApplicationMgr.GetMobileEnvironment() == MobileEnv.PRODUCTION)
    {
      Downloader.m_remoteUri = "http://dist.blizzard.com/hs-pod/dlc/";
      Downloader.m_remoteUriCN = "http://client{0}.pdl.battlenet.com.cn/hs-pod/dlc/";
    }
    else
      Downloader.m_remoteUri = "http://streaming-t5.corp.blizzard.net/hearthstone/dlc/";
    Downloader.m_remoteUri += "win/";
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
  }

  private void OnDestroy()
  {
    Downloader.s_instance = (Downloader) null;
  }

  private void Start()
  {
    if (AssetLoader.DOWNLOADABLE_LANGUAGE_PACKS)
      this.DownloadLocalizedBundles();
    else
      Log.Downloader.Print("Not downloading language bundles (DOWNLOADABLE_LANGUAGE_PACKS=false)");
  }

  public static Downloader Get()
  {
    return Downloader.s_instance;
  }

  public bool AllLocalizedAudioBundlesDownloaded()
  {
    if (this.m_isDownloading)
      return true;
    return this.BundlesMissing().Count == 0;
  }

  public void DownloadLocalizedBundles()
  {
    this.DownloadLocalizedBundles(this.BundlesToDownloadForLocale());
  }

  private void DownloadLocalizedBundles(List<string> bundlesToDownload)
  {
    if (bundlesToDownload == null || bundlesToDownload.Count <= 0 || this.m_isDownloading)
      return;
    this.m_isDownloading = true;
    this.m_downloadStartTime = Time.realtimeSinceStartup;
    Log.Downloader.Print("Starting to load or download localized bundles at " + (object) this.m_downloadStartTime);
    this.m_bundlesToDownload = bundlesToDownload;
    this.DownloadNextFile();
  }

  public void DeleteLocalizedBundles()
  {
    using (Map<string, AssetBundle>.ValueCollection.Enumerator enumerator = this.m_downloadableBundles.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Unload(true);
    }
    Caching.CleanCache();
    this.m_downloadableBundles.Clear();
    UnityEngine.Debug.Log((object) "Cleared cache of downloadable bundles.");
  }

  public AssetBundle GetDownloadedBundle(string fileName)
  {
    AssetBundle assetBundle = (AssetBundle) null;
    this.m_downloadableBundles.TryGetValue(fileName, out assetBundle);
    if ((UnityEngine.Object) assetBundle == (UnityEngine.Object) null)
      UnityEngine.Debug.Log((object) string.Format("Attempted to load bundle {0} but not available yet.", (object) fileName));
    return assetBundle;
  }

  public static int GetTryCount()
  {
    return MobileDeviceLocale.GetCurrentRegionId() != constants.BnetRegion.REGION_CN ? 1 : 4;
  }

  public static string BundleURL(string fileName)
  {
    string str = Downloader.m_remoteUri;
    if (MobileDeviceLocale.GetCurrentRegionId() == constants.BnetRegion.REGION_CN && Downloader.m_remoteUriCNIndex != Downloader.GetTryCount() && Downloader.m_remoteUriCN != null)
      str = string.Format(Downloader.m_remoteUriCN, (object) ("0" + Downloader.m_remoteUriCNIndex.ToString()));
    return string.Format("{0}{1}/{2}", (object) str, (object) DownloadManifest.Get().HashForBundle(fileName), (object) fileName);
  }

  public static string NextBundleURL(string fileName)
  {
    if (MobileDeviceLocale.GetCurrentRegionId() == constants.BnetRegion.REGION_CN)
      Downloader.m_remoteUriCNIndex = Downloader.m_remoteUriCNIndex != Downloader.GetTryCount() ? Downloader.m_remoteUriCNIndex + 1 : 1;
    return Downloader.BundleURL(fileName);
  }

  private void WillReset()
  {
    this.StopAllCoroutines();
    this.m_isDownloading = false;
    this.m_hasShownInternalFailureAlert = false;
    using (Map<string, AssetBundle>.ValueCollection.Enumerator enumerator = this.m_downloadableBundles.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AssetBundle current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null)
          current.Unload(true);
      }
    }
    this.m_downloadableBundles.Clear();
    if (!AssetLoader.DOWNLOADABLE_LANGUAGE_PACKS)
      return;
    this.DownloadLocalizedBundles();
  }

  private List<string> BundlesToDownloadForLocale()
  {
    List<string> stringList = new List<string>();
    foreach (Locale enumVal in Localization.GetLoadOrder(false))
    {
      switch (enumVal)
      {
        case Locale.enUS:
        case Locale.enGB:
          continue;
        default:
          using (Map<AssetFamily, AssetFamilyBundleInfo>.ValueCollection.Enumerator enumerator = AssetBundleInfo.FamilyInfo.Values.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              AssetFamilyBundleInfo current = enumerator.Current;
              for (int index = 0; index < current.NumberOfDownloadableLocaleBundles; ++index)
              {
                string lower = EnumUtils.GetString<Locale>(enumVal).ToLower();
                string str = string.Format("{0}{1}_dlc_{2}.unity3d", (object) current.BundleName, (object) lower, (object) index);
                stringList.Add(str);
              }
            }
            continue;
          }
      }
    }
    return stringList;
  }

  private void DownloadNextFile()
  {
    if (this.m_bundlesToDownload.Count > 0)
    {
      string bundleName = this.m_bundlesToDownload[0];
      string fileName = DownloadManifest.Get().DownloadableBundleFileName(bundleName);
      if (fileName == null)
      {
        Error.AddDevFatal(string.Format("Downloader.DownloadNextFile() - Attempting to download bundle not listed in manifest.  No hash found for bundle {0}", (object) bundleName));
        this.m_bundlesToDownload.RemoveAt(0);
        this.DownloadNextFile();
      }
      else
        this.StartCoroutine(this.DownloadAndCache(fileName));
    }
    else
    {
      Log.Downloader.Print("Finished downloading or loading all bundles - duration: " + (object) (float) ((double) Time.realtimeSinceStartup - (double) this.m_downloadStartTime));
      AssetCache.ClearAllCachesFailedRequests();
      this.m_isDownloading = false;
    }
  }

  [DebuggerHidden]
  private IEnumerator DownloadAndCache(string fileName)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Downloader.\u003CDownloadAndCache\u003Ec__Iterator22A() { fileName = fileName, \u003C\u0024\u003EfileName = fileName, \u003C\u003Ef__this = this };
  }

  private List<string> BundlesMissing()
  {
    List<string> downloadForLocale = this.BundlesToDownloadForLocale();
    List<string> stringList = new List<string>();
    using (List<string>.Enumerator enumerator = downloadForLocale.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (!this.m_downloadableBundles.ContainsKey(current))
          stringList.Add(current);
      }
    }
    return stringList;
  }
}
