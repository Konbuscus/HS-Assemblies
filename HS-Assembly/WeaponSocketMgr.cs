﻿// Decompiled with JetBrains decompiler
// Type: WeaponSocketMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class WeaponSocketMgr : MonoBehaviour
{
  public List<WeaponSocketDecoration> m_Decorations;

  public void UpdateSockets()
  {
    if (this.m_Decorations == null)
      return;
    using (List<WeaponSocketDecoration>.Enumerator enumerator = this.m_Decorations.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateVisibility();
    }
  }

  public static bool ShouldSeeWeaponSocket(TAG_CLASS tagVal)
  {
    switch (tagVal)
    {
      case TAG_CLASS.DRUID:
      case TAG_CLASS.MAGE:
      case TAG_CLASS.PRIEST:
      case TAG_CLASS.WARLOCK:
        return false;
      default:
        return true;
    }
  }
}
