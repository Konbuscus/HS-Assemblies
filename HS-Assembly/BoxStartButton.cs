﻿// Decompiled with JetBrains decompiler
// Type: BoxStartButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class BoxStartButton : PegUIElement
{
  public UberText m_Text;
  private Box m_parent;
  private BoxStartButtonStateInfo m_info;
  private BoxStartButton.State m_state;

  public Box GetParent()
  {
    return this.m_parent;
  }

  public void SetParent(Box parent)
  {
    this.m_parent = parent;
  }

  public BoxStartButtonStateInfo GetInfo()
  {
    return this.m_info;
  }

  public void SetInfo(BoxStartButtonStateInfo info)
  {
    this.m_info = info;
  }

  public string GetText()
  {
    return this.m_Text.Text;
  }

  public void SetText(string text)
  {
    this.m_Text.Text = text;
  }

  public bool ChangeState(BoxStartButton.State state)
  {
    if (this.m_state == state)
      return false;
    this.m_state = state;
    if (state == BoxStartButton.State.SHOWN)
    {
      this.m_parent.OnAnimStarted();
      this.gameObject.SetActive(true);
      iTween.FadeTo(this.gameObject, iTween.Hash((object) "amount", (object) this.m_info.m_ShownAlpha, (object) "delay", (object) this.m_info.m_ShownDelaySec, (object) "time", (object) this.m_info.m_ShownFadeSec, (object) "easeType", (object) this.m_info.m_ShownFadeEaseType, (object) "oncomplete", (object) "OnShownAnimFinished", (object) "oncompletetarget", (object) this.gameObject));
    }
    else if (state == BoxStartButton.State.HIDDEN)
    {
      this.m_parent.OnAnimStarted();
      iTween.FadeTo(this.gameObject, iTween.Hash((object) "amount", (object) this.m_info.m_HiddenAlpha, (object) "delay", (object) this.m_info.m_HiddenDelaySec, (object) "time", (object) this.m_info.m_HiddenFadeSec, (object) "easeType", (object) this.m_info.m_HiddenFadeEaseType, (object) "oncomplete", (object) "OnHiddenAnimFinished", (object) "oncompletetarget", (object) this.gameObject));
    }
    return true;
  }

  public void UpdateState(BoxStartButton.State state)
  {
    this.m_state = state;
    if (state == BoxStartButton.State.SHOWN)
    {
      RenderUtils.SetAlpha(this.gameObject, this.m_info.m_ShownAlpha);
      this.gameObject.SetActive(true);
    }
    else
    {
      if (state != BoxStartButton.State.HIDDEN)
        return;
      RenderUtils.SetAlpha(this.gameObject, this.m_info.m_HiddenAlpha);
      this.gameObject.SetActive(false);
    }
  }

  private void OnShownAnimFinished()
  {
    this.m_parent.OnAnimFinished();
  }

  private void OnHiddenAnimFinished()
  {
    this.gameObject.SetActive(false);
    this.m_parent.OnAnimFinished();
  }

  public enum State
  {
    SHOWN,
    HIDDEN,
  }
}
