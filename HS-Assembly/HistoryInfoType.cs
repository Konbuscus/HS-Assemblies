﻿// Decompiled with JetBrains decompiler
// Type: HistoryInfoType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum HistoryInfoType
{
  NONE,
  WEAPON_PLAYED,
  CARD_PLAYED,
  ATTACK,
  TRIGGER,
  WEAPON_BREAK,
  FATIGUE,
}
