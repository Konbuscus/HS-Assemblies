﻿// Decompiled with JetBrains decompiler
// Type: TutorialProgressScreen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TutorialProgressScreen : MonoBehaviour
{
  private List<HeroCoin> m_heroCoins = new List<HeroCoin>();
  private readonly Map<TutorialProgress, ScenarioDbId> m_progressToNextMissionIdMap = new Map<TutorialProgress, ScenarioDbId>() { { TutorialProgress.NOTHING_COMPLETE, ScenarioDbId.TUTORIAL_HOGGER }, { TutorialProgress.HOGGER_COMPLETE, ScenarioDbId.TUTORIAL_MILLHOUSE }, { TutorialProgress.MILLHOUSE_COMPLETE, ScenarioDbId.TUTORIAL_CHO }, { TutorialProgress.CHO_COMPLETE, ScenarioDbId.TUTORIAL_MUKLA }, { TutorialProgress.MUKLA_COMPLETE, ScenarioDbId.TUTORIAL_NESINGWARY }, { TutorialProgress.NESINGWARY_COMPLETE, ScenarioDbId.TUTORIAL_ILLIDAN } };
  private readonly Map<ScenarioDbId, TutorialProgressScreen.LessonAsset> m_missionIdToLessonAssetMap = new Map<ScenarioDbId, TutorialProgressScreen.LessonAsset>() { { ScenarioDbId.TUTORIAL_HOGGER, (TutorialProgressScreen.LessonAsset) null }, { ScenarioDbId.TUTORIAL_MILLHOUSE, new TutorialProgressScreen.LessonAsset() { m_asset = "Tutorial_Lesson1" } }, { ScenarioDbId.TUTORIAL_CHO, new TutorialProgressScreen.LessonAsset() { m_asset = "Tutorial_Lesson2", m_phoneAsset = "Tutorial_Lesson2_phone" } }, { ScenarioDbId.TUTORIAL_MUKLA, new TutorialProgressScreen.LessonAsset() { m_asset = "Tutorial_Lesson3" } }, { ScenarioDbId.TUTORIAL_NESINGWARY, new TutorialProgressScreen.LessonAsset() { m_asset = "Tutorial_Lesson4" } }, { ScenarioDbId.TUTORIAL_ILLIDAN, new TutorialProgressScreen.LessonAsset() { m_asset = "Tutorial_Lesson5" } } };
  private List<ScenarioDbfRecord> m_sortedMissionRecords = new List<ScenarioDbfRecord>();
  private Vector3 START_SCALE = new Vector3(0.5f, 0.5f, 0.5f);
  private Vector3 FINAL_SCALE = (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(7f, 1f, 7f), Phone = new Vector3(6.1f, 1f, 6.1f) };
  private Vector3 FINAL_SCALE_OVER_BOX = (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(92.5f, 14f, 92.5f), Phone = new Vector3(106f, 16f, 106f) };
  private PlatformDependentValue<Vector3> FINAL_POS = new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(-8f, 5f, -5f), Phone = new Vector3(-8f, 5f, -4.58f) };
  private PlatformDependentValue<Vector3> FINAL_POS_OVER_BOX = new PlatformDependentValue<Vector3>(PlatformCategory.Screen) { PC = new Vector3(0.0f, 5f, -0.2f), Phone = new Vector3(0.0f, 5f, -2.06f) };
  private const float START_SCALE_VAL = 0.5f;
  private const float HERO_SPACING = -0.2f;
  public HeroCoin m_coinPrefab;
  public UberText m_lessonTitle;
  public UberText m_missionProgressTitle;
  public GameObject m_currentLessonBone;
  public PegUIElement m_exitButton;
  public UberText m_exitButtonLabel;
  private static TutorialProgressScreen s_instance;
  private HeroCoin.CoinPressCallback m_coinPressCallback;
  private bool m_showProgressSavedMessage;
  private Vector3 HERO_COIN_START;
  private bool IS_TESTING;

  private void Awake()
  {
    TutorialProgressScreen.s_instance = this;
    FullScreenFXMgr.Get().Vignette(1f, 0.5f, iTween.EaseType.easeInOutQuad, (FullScreenFXMgr.EffectListener) null);
    this.m_lessonTitle.Text = GameStrings.Get("TUTORIAL_PROGRESS_LESSON_TITLE");
    this.m_missionProgressTitle.Text = GameStrings.Get("TUTORIAL_PROGRESS_TITLE");
    this.m_exitButton.gameObject.SetActive(false);
    this.InitMissionRecords();
  }

  private void OnDestroy()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.UpdateProgress));
    TutorialProgressScreen.s_instance = (TutorialProgressScreen) null;
  }

  public static TutorialProgressScreen Get()
  {
    return TutorialProgressScreen.s_instance;
  }

  public void StartTutorialProgress()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY)
    {
      if (GameState.Get().GetFriendlySidePlayer().GetTag<TAG_PLAYSTATE>(GAME_TAG.PLAYSTATE) == TAG_PLAYSTATE.WON)
      {
        GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActorSpell(SpellType.ENDGAME_WIN, true).ActivateState(SpellStateType.DEATH);
        this.m_showProgressSavedMessage = true;
      }
      Gameplay.Get().RemoveGamePlayNameBannerPhone();
    }
    this.LoadAllTutorialHeroEntities();
  }

  public void SetCoinPressCallback(HeroCoin.CoinPressCallback callback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TutorialProgressScreen.\u003CSetCoinPressCallback\u003Ec__AnonStorey400 callbackCAnonStorey400 = new TutorialProgressScreen.\u003CSetCoinPressCallback\u003Ec__AnonStorey400();
    // ISSUE: reference to a compiler-generated field
    callbackCAnonStorey400.callback = callback;
    // ISSUE: reference to a compiler-generated field
    callbackCAnonStorey400.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    if (callbackCAnonStorey400.callback == null)
      return;
    // ISSUE: reference to a compiler-generated method
    this.m_coinPressCallback = new HeroCoin.CoinPressCallback(callbackCAnonStorey400.\u003C\u003Em__201);
  }

  private void InitMissionRecords()
  {
    using (List<ScenarioDbfRecord>.Enumerator enumerator = GameDbf.Scenario.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ScenarioDbfRecord current = enumerator.Current;
        if (current.AdventureId == 1 && Enum.IsDefined(typeof (ScenarioDbId), (object) current.ID))
          this.m_sortedMissionRecords.Add(current);
      }
    }
    this.m_sortedMissionRecords.Sort(new Comparison<ScenarioDbfRecord>(GameUtils.MissionSortComparison));
  }

  private void LoadAllTutorialHeroEntities()
  {
    for (int index = 0; index < this.m_sortedMissionRecords.Count; ++index)
    {
      string missionHeroCardId = GameUtils.GetMissionHeroCardId(this.m_sortedMissionRecords[index].ID);
      if (DefLoader.Get().GetEntityDef(missionHeroCardId) == null)
        UnityEngine.Debug.LogError((object) string.Format("TutorialProgress.OnTutorialHeroEntityDefLoaded() - failed to load {0}", (object) missionHeroCardId));
    }
    this.SetupCoins();
    this.Show();
  }

  private void SetupCoins()
  {
    this.HERO_COIN_START = new Vector3(0.5f, 0.1f, 0.32f);
    Vector3 vector3 = Vector3.zero;
    for (int index = 0; index < this.m_sortedMissionRecords.Count; ++index)
    {
      int id = this.m_sortedMissionRecords[index].ID;
      HeroCoin heroCoin = UnityEngine.Object.Instantiate<HeroCoin>(this.m_coinPrefab);
      heroCoin.transform.parent = this.transform;
      heroCoin.gameObject.SetActive(false);
      heroCoin.SetCoinPressCallback(this.m_coinPressCallback);
      Vector2 crackTexture;
      switch (UnityEngine.Random.Range(0, 3))
      {
        case 1:
          crackTexture = new Vector2(0.25f, -1f);
          break;
        case 2:
          crackTexture = new Vector2(0.5f, -1f);
          break;
        default:
          crackTexture = new Vector2(0.0f, -1f);
          break;
      }
      if (index == 0)
        heroCoin.transform.localPosition = this.HERO_COIN_START;
      else
        heroCoin.transform.localPosition = new Vector3(vector3.x - 0.2f, vector3.y, vector3.z);
      string lessonAssetName = (string) null;
      TutorialProgressScreen.LessonAsset lessonAsset;
      this.m_missionIdToLessonAssetMap.TryGetValue((ScenarioDbId) id, out lessonAsset);
      if (lessonAsset != null)
        lessonAssetName = !(bool) UniversalInputManager.UsePhoneUI || string.IsNullOrEmpty(lessonAsset.m_phoneAsset) ? lessonAsset.m_asset : lessonAsset.m_phoneAsset;
      if (!string.IsNullOrEmpty(lessonAssetName))
        heroCoin.SetLessonAssetName(lessonAssetName);
      this.m_heroCoins.Add(heroCoin);
      Vector2 goldTexture = Vector2.zero;
      Vector2 grayTexture = Vector2.zero;
      switch (id)
      {
        case 3:
          goldTexture = new Vector2(0.0f, -0.25f);
          grayTexture = new Vector2(0.25f, -0.25f);
          break;
        case 4:
          goldTexture = new Vector2(0.5f, 0.0f);
          grayTexture = new Vector2(0.75f, 0.0f);
          break;
        case 248:
          goldTexture = new Vector2(0.0f, -0.5f);
          grayTexture = new Vector2(0.25f, -0.5f);
          break;
        case 249:
          goldTexture = new Vector2(0.5f, -0.5f);
          grayTexture = new Vector2(0.75f, -0.5f);
          break;
        case 181:
          goldTexture = new Vector2(0.5f, -0.25f);
          grayTexture = new Vector2(0.75f, -0.25f);
          break;
        case 201:
          goldTexture = new Vector2(0.0f, 0.0f);
          grayTexture = new Vector2(0.25f, 0.0f);
          break;
      }
      heroCoin.SetCoinInfo(goldTexture, grayTexture, crackTexture, id);
      vector3 = heroCoin.transform.localPosition;
    }
    SceneUtils.SetLayer(this.gameObject, GameLayer.IgnoreFullScreenEffects);
  }

  private void Show()
  {
    iTween.FadeTo(this.gameObject, 1f, 0.25f);
    bool flag = SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY;
    this.transform.position = (Vector3) (!flag ? this.FINAL_POS_OVER_BOX : this.FINAL_POS);
    this.transform.localScale = this.START_SCALE;
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) (!flag ? this.FINAL_SCALE_OVER_BOX : this.FINAL_SCALE), (object) "time", (object) 0.5f, (object) "oncomplete", (object) "OnScaleAnimComplete", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void Hide()
  {
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) this.START_SCALE, (object) "time", (object) 0.5f, (object) "oncomplete", (object) "OnHideAnimComplete", (object) "oncompletetarget", (object) this.gameObject));
    iTween.FadeTo(this.gameObject, iTween.Hash((object) "alpha", (object) 0.0f, (object) "time", (object) 0.25f, (object) "delay", (object) 0.25f));
  }

  private void OnScaleAnimComplete()
  {
    if (this.IS_TESTING)
      this.UpdateProgress();
    else
      NetCache.Get().RegisterTutorialEndGameScreen(new NetCache.NetCacheCallback(this.UpdateProgress), new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
    using (List<HeroCoin>.Enumerator enumerator = this.m_heroCoins.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.FinishIntroScaling();
    }
  }

  private void OnHideAnimComplete()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  private void UpdateProgress()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TutorialProgressScreen.\u003CUpdateProgress\u003Ec__AnonStorey401 progressCAnonStorey401 = new TutorialProgressScreen.\u003CUpdateProgress\u003Ec__AnonStorey401();
    if (this.IS_TESTING)
    {
      // ISSUE: reference to a compiler-generated field
      progressCAnonStorey401.nextMissionId = this.m_progressToNextMissionIdMap[TutorialProgress.HOGGER_COMPLETE];
    }
    else
    {
      NetCache.NetCacheProfileProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>();
      // ISSUE: reference to a compiler-generated field
      progressCAnonStorey401.nextMissionId = this.m_progressToNextMissionIdMap[netObject.CampaignProgress];
    }
    // ISSUE: reference to a compiler-generated method
    int index1 = this.m_heroCoins.FindIndex(new Predicate<HeroCoin>(progressCAnonStorey401.\u003C\u003Em__202));
    for (int index2 = 0; index2 < this.m_heroCoins.Count; ++index2)
    {
      HeroCoin heroCoin = this.m_heroCoins[index2];
      if (index2 == index1 - 1)
        this.StartCoroutine(this.SetActiveToDefeated(heroCoin));
      else if (index2 < index1)
        heroCoin.SetProgress(HeroCoin.CoinStatus.DEFEATED);
      else if (index2 == index1)
      {
        this.StartCoroutine(this.SetUnrevealedToActive(heroCoin));
        string lessonAssetName = heroCoin.GetLessonAssetName();
        if (!string.IsNullOrEmpty(lessonAssetName))
          AssetLoader.Get().LoadGameObject(lessonAssetName, new AssetLoader.GameObjectCallback(this.OnTutorialImageLoaded), (object) null, false);
      }
      else
        heroCoin.SetProgress(HeroCoin.CoinStatus.UNREVEALED);
    }
    if (!this.m_showProgressSavedMessage)
      return;
    UIStatus.Get().AddInfo(GameStrings.Get("TUTORIAL_PROGRESS_SAVED"));
    this.m_showProgressSavedMessage = false;
  }

  private void OnTutorialImageLoaded(string name, GameObject go, object callbackData)
  {
    this.SetupTutorialImage(go);
  }

  private void SetupTutorialImage(GameObject go)
  {
    SceneUtils.SetLayer(go, GameLayer.IgnoreFullScreenEffects);
    go.transform.parent = this.m_currentLessonBone.transform;
    go.transform.localScale = Vector3.one;
    go.transform.localEulerAngles = Vector3.zero;
    go.transform.localPosition = Vector3.zero;
  }

  [DebuggerHidden]
  private IEnumerator SetActiveToDefeated(HeroCoin coin)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TutorialProgressScreen.\u003CSetActiveToDefeated\u003Ec__Iterator290() { coin = coin, \u003C\u0024\u003Ecoin = coin };
  }

  [DebuggerHidden]
  private IEnumerator SetUnrevealedToActive(HeroCoin coin)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TutorialProgressScreen.\u003CSetUnrevealedToActive\u003Ec__Iterator291() { coin = coin, \u003C\u0024\u003Ecoin = coin };
  }

  private void ExitButtonPress(UIEvent e)
  {
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
    FullScreenFXMgr.Get().Vignette(0.0f, 0.5f, iTween.EaseType.easeInOutQuad, (FullScreenFXMgr.EffectListener) null);
  }

  private class LessonAsset
  {
    public string m_asset;
    public string m_phoneAsset;
  }
}
