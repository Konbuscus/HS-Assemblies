﻿// Decompiled with JetBrains decompiler
// Type: UberMath
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class UberMath
{
  private static readonly int[,] grad3 = new int[12, 3]{ { 1, 1, 0 }, { -1, 1, 0 }, { 1, -1, 0 }, { -1, -1, 0 }, { 1, 0, 1 }, { -1, 0, 1 }, { 1, 0, -1 }, { -1, 0, -1 }, { 0, 1, 1 }, { 0, -1, 1 }, { 0, 1, -1 }, { 0, -1, -1 } };
  private static readonly int[,] grad4 = new int[32, 4]{ { 0, 1, 1, 1 }, { 0, 1, 1, -1 }, { 0, 1, -1, 1 }, { 0, 1, -1, -1 }, { 0, -1, 1, 1 }, { 0, -1, 1, -1 }, { 0, -1, -1, 1 }, { 0, -1, -1, -1 }, { 1, 0, 1, 1 }, { 1, 0, 1, -1 }, { 1, 0, -1, 1 }, { 1, 0, -1, -1 }, { -1, 0, 1, 1 }, { -1, 0, 1, -1 }, { -1, 0, -1, 1 }, { -1, 0, -1, -1 }, { 1, 1, 0, 1 }, { 1, 1, 0, -1 }, { 1, -1, 0, 1 }, { 1, -1, 0, -1 }, { -1, 1, 0, 1 }, { -1, 1, 0, -1 }, { -1, -1, 0, 1 }, { -1, -1, 0, -1 }, { 1, 1, 1, 0 }, { 1, 1, -1, 0 }, { 1, -1, 1, 0 }, { 1, -1, -1, 0 }, { -1, 1, 1, 0 }, { -1, 1, -1, 0 }, { -1, -1, 1, 0 }, { -1, -1, -1, 0 } };
  private static readonly int[,] simplex = new int[64, 4]{ { 0, 1, 2, 3 }, { 0, 1, 3, 2 }, { 0, 0, 0, 0 }, { 0, 2, 3, 1 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 1, 2, 3, 0 }, { 0, 2, 1, 3 }, { 0, 0, 0, 0 }, { 0, 3, 1, 2 }, { 0, 3, 2, 1 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 1, 3, 2, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 1, 2, 0, 3 }, { 0, 0, 0, 0 }, { 1, 3, 0, 2 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 2, 3, 0, 1 }, { 2, 3, 1, 0 }, { 1, 0, 2, 3 }, { 1, 0, 3, 2 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 2, 0, 3, 1 }, { 0, 0, 0, 0 }, { 2, 1, 3, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 2, 0, 1, 3 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 3, 0, 1, 2 }, { 3, 0, 2, 1 }, { 0, 0, 0, 0 }, { 3, 1, 2, 0 }, { 2, 1, 0, 3 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 3, 1, 0, 2 }, { 0, 0, 0, 0 }, { 3, 2, 0, 1 }, { 3, 2, 1, 0 } };
  private static int[] perm = new int[512];
  private const float ONE_THIRD = 0.3333333f;
  private const float ONE_SIXTH = 0.1666667f;
  private const float ONE_SIXTH_MUL3 = 0.5f;

  static UberMath()
  {
    for (int index = 0; index < 512; ++index)
      UberMath.perm[index] = Random.Range(5, 250);
  }

  private static int floor(float x)
  {
    if ((double) x > 0.0)
      return (int) x;
    return (int) x - 1;
  }

  private static float dot(int gx, int gy, float x, float y)
  {
    return (float) ((double) gx * (double) x + (double) gy * (double) y);
  }

  private static float dot(int gx, int gy, int gz, float x, float y, float z)
  {
    return (float) ((double) gx * (double) x + (double) gy * (double) y + (double) gz * (double) z);
  }

  private static float dot(int gx, int gy, int gz, int gw, float x, float y, float z, float w)
  {
    return (float) ((double) gx * (double) x + (double) gy * (double) y + (double) gz * (double) z + (double) gz * (double) w);
  }

  public static float SimplexNoise(float xin, float yin)
  {
    float num1 = 0.3660254f;
    float num2 = (xin + yin) * num1;
    int num3 = UberMath.floor(xin + num2);
    int num4 = UberMath.floor(yin + num2);
    float num5 = 0.2113249f;
    float num6 = (float) (num3 + num4) * num5;
    float num7 = (float) num4 - num6;
    float num8 = (float) num3 - num6;
    float y1 = yin - num7;
    float x1 = xin - num8;
    int num9;
    int num10;
    if ((double) x1 > (double) y1)
    {
      num9 = 1;
      num10 = 0;
    }
    else
    {
      num9 = 0;
      num10 = 1;
    }
    float x2 = x1 - (float) num9 + num5;
    float y2 = y1 - (float) num10 + num5;
    float x3 = (float) ((double) x1 - 1.0 + 2.0 * (double) num5);
    float y3 = (float) ((double) y1 - 1.0 + 2.0 * (double) num5);
    int num11 = num3 & (int) byte.MaxValue;
    int index1 = num4 & (int) byte.MaxValue;
    int index2 = UberMath.perm[num11 + UberMath.perm[index1]] % 12;
    int index3 = UberMath.perm[num11 + num9 + UberMath.perm[index1 + num10]] % 12;
    int index4 = UberMath.perm[num11 + 1 + UberMath.perm[index1 + 1]] % 12;
    float num12 = (float) (0.5 - (double) x1 * (double) x1 - (double) y1 * (double) y1);
    float num13;
    if ((double) num12 < 0.0)
    {
      num13 = 0.0f;
    }
    else
    {
      float num14 = num12 * num12;
      num13 = num14 * num14 * UberMath.dot(UberMath.grad3[index2, 0], UberMath.grad3[index2, 1], x1, y1);
    }
    float num15 = (float) (0.5 - (double) x2 * (double) x2 - (double) y2 * (double) y2);
    float num16;
    if ((double) num15 < 0.0)
    {
      num16 = 0.0f;
    }
    else
    {
      float num14 = num15 * num15;
      num16 = num14 * num14 * UberMath.dot(UberMath.grad3[index3, 0], UberMath.grad3[index3, 1], x2, y2);
    }
    float num17 = (float) (0.5 - (double) x3 * (double) x3 - (double) y3 * (double) y3);
    float num18;
    if ((double) num17 < 0.0)
    {
      num18 = 0.0f;
    }
    else
    {
      float num14 = num17 * num17;
      num18 = num14 * num14 * UberMath.dot(UberMath.grad3[index4, 0], UberMath.grad3[index4, 1], x3, y3);
    }
    return (float) (70.0 * ((double) num13 + (double) num16 + (double) num18));
  }

  public static float SimplexNoise(float xin, float yin, float zin)
  {
    float num1 = (float) (((double) xin + (double) yin + (double) zin) * 0.333333343267441);
    int num2 = UberMath.floor(xin + num1);
    int num3 = UberMath.floor(yin + num1);
    int num4 = UberMath.floor(zin + num1);
    float num5 = (float) (num2 + num3 + num4) * 0.1666667f;
    float num6 = (float) num2 - num5;
    float num7 = (float) num3 - num5;
    float num8 = (float) num4 - num5;
    float x1 = xin - num6;
    float y1 = yin - num7;
    float z1 = zin - num8;
    int num9;
    int num10;
    int num11;
    int num12;
    int num13;
    int num14;
    if ((double) x1 >= (double) y1)
    {
      if ((double) y1 >= (double) z1)
      {
        num9 = 1;
        num10 = 0;
        num11 = 0;
        num12 = 1;
        num13 = 1;
        num14 = 0;
      }
      else if ((double) x1 >= (double) z1)
      {
        num9 = 1;
        num10 = 0;
        num11 = 0;
        num12 = 1;
        num13 = 0;
        num14 = 1;
      }
      else
      {
        num9 = 0;
        num10 = 0;
        num11 = 1;
        num12 = 1;
        num13 = 0;
        num14 = 1;
      }
    }
    else if ((double) y1 < (double) z1)
    {
      num9 = 0;
      num10 = 0;
      num11 = 1;
      num12 = 0;
      num13 = 1;
      num14 = 1;
    }
    else if ((double) x1 < (double) z1)
    {
      num9 = 0;
      num10 = 1;
      num11 = 0;
      num12 = 0;
      num13 = 1;
      num14 = 1;
    }
    else
    {
      num9 = 0;
      num10 = 1;
      num11 = 0;
      num12 = 1;
      num13 = 1;
      num14 = 0;
    }
    float x2 = (float) ((double) x1 - (double) num9 + 0.16666667163372);
    float y2 = (float) ((double) y1 - (double) num10 + 0.16666667163372);
    float z2 = (float) ((double) z1 - (double) num11 + 0.16666667163372);
    float x3 = (float) ((double) x1 - (double) num12 + 0.333333343267441);
    float y3 = (float) ((double) y1 - (double) num13 + 0.333333343267441);
    float z3 = (float) ((double) z1 - (double) num14 + 0.333333343267441);
    float x4 = (float) ((double) x1 - 1.0 + 0.5);
    float y4 = (float) ((double) y1 - 1.0 + 0.5);
    float z4 = (float) ((double) z1 - 1.0 + 0.5);
    int num15 = num2 & (int) byte.MaxValue;
    int num16 = num3 & (int) byte.MaxValue;
    int index1 = num4 & (int) byte.MaxValue;
    int index2 = UberMath.perm[num15 + UberMath.perm[num16 + UberMath.perm[index1]]] % 12;
    int index3 = UberMath.perm[num15 + num9 + UberMath.perm[num16 + num10 + UberMath.perm[index1 + num11]]] % 12;
    int index4 = UberMath.perm[num15 + num12 + UberMath.perm[num16 + num13 + UberMath.perm[index1 + num14]]] % 12;
    int index5 = UberMath.perm[num15 + 1 + UberMath.perm[num16 + 1 + UberMath.perm[index1 + 1]]] % 12;
    float num17 = (float) (0.600000023841858 - (double) x1 * (double) x1 - (double) y1 * (double) y1 - (double) z1 * (double) z1);
    float num18;
    if ((double) num17 < 0.0)
    {
      num18 = 0.0f;
    }
    else
    {
      float num19 = num17 * num17;
      num18 = num19 * num19 * UberMath.dot(UberMath.grad3[index2, 0], UberMath.grad3[index2, 1], UberMath.grad3[index2, 2], x1, y1, z1);
    }
    float num20 = (float) (0.600000023841858 - (double) x2 * (double) x2 - (double) y2 * (double) y2 - (double) z2 * (double) z2);
    float num21;
    if ((double) num20 < 0.0)
    {
      num21 = 0.0f;
    }
    else
    {
      float num19 = num20 * num20;
      num21 = num19 * num19 * UberMath.dot(UberMath.grad3[index3, 0], UberMath.grad3[index3, 1], UberMath.grad3[index3, 2], x2, y2, z2);
    }
    float num22 = (float) (0.600000023841858 - (double) x3 * (double) x3 - (double) y3 * (double) y3 - (double) z3 * (double) z3);
    float num23;
    if ((double) num22 < 0.0)
    {
      num23 = 0.0f;
    }
    else
    {
      float num19 = num22 * num22;
      num23 = num19 * num19 * UberMath.dot(UberMath.grad3[index4, 0], UberMath.grad3[index4, 1], UberMath.grad3[index4, 2], x3, y3, z3);
    }
    float num24 = (float) (0.600000023841858 - (double) x4 * (double) x4 - (double) y4 * (double) y4 - (double) z4 * (double) z4);
    float num25;
    if ((double) num24 < 0.0)
    {
      num25 = 0.0f;
    }
    else
    {
      float num19 = num24 * num24;
      num25 = num19 * num19 * UberMath.dot(UberMath.grad3[index5, 0], UberMath.grad3[index5, 1], UberMath.grad3[index5, 2], x4, y4, z4);
    }
    return (float) (32.0 * ((double) num18 + (double) num21 + (double) num23 + (double) num25));
  }

  public static float SimplexNoise(float x, float y, float z, float w)
  {
    float num1 = 0.309017f;
    float num2 = 0.1381966f;
    float num3 = (x + y + z + w) * num1;
    int num4 = UberMath.floor(x + num3);
    int num5 = UberMath.floor(y + num3);
    int num6 = UberMath.floor(z + num3);
    int num7 = UberMath.floor(w + num3);
    float num8 = (float) (num4 + num5 + num6 + num7) * num2;
    float num9 = (float) num4 - num8;
    float num10 = (float) num5 - num8;
    float num11 = (float) num6 - num8;
    float num12 = (float) num7 - num8;
    float x1 = x - num9;
    float y1 = y - num10;
    float z1 = z - num11;
    float w1 = w - num12;
    int index1 = ((double) x1 <= (double) y1 ? 0 : 32) + ((double) x1 <= (double) z1 ? 0 : 16) + ((double) y1 <= (double) z1 ? 0 : 8) + ((double) x1 <= (double) w1 ? 0 : 4) + ((double) y1 <= (double) w1 ? 0 : 2) + ((double) z1 <= (double) w1 ? 0 : 1);
    int num13 = UberMath.simplex[index1, 0] < 3 ? 0 : 1;
    int num14 = UberMath.simplex[index1, 1] < 3 ? 0 : 1;
    int num15 = UberMath.simplex[index1, 2] < 3 ? 0 : 1;
    int num16 = UberMath.simplex[index1, 3] < 3 ? 0 : 1;
    int num17 = UberMath.simplex[index1, 0] < 2 ? 0 : 1;
    int num18 = UberMath.simplex[index1, 1] < 2 ? 0 : 1;
    int num19 = UberMath.simplex[index1, 2] < 2 ? 0 : 1;
    int num20 = UberMath.simplex[index1, 3] < 2 ? 0 : 1;
    int num21 = UberMath.simplex[index1, 0] < 1 ? 0 : 1;
    int num22 = UberMath.simplex[index1, 1] < 1 ? 0 : 1;
    int num23 = UberMath.simplex[index1, 2] < 1 ? 0 : 1;
    int num24 = UberMath.simplex[index1, 3] < 1 ? 0 : 1;
    float x2 = x1 - (float) num13 + num2;
    float y2 = y1 - (float) num14 + num2;
    float z2 = z1 - (float) num15 + num2;
    float w2 = w1 - (float) num16 + num2;
    float x3 = (float) ((double) x1 - (double) num17 + 2.0 * (double) num2);
    float y3 = (float) ((double) y1 - (double) num18 + 2.0 * (double) num2);
    float z3 = (float) ((double) z1 - (double) num19 + 2.0 * (double) num2);
    float w3 = (float) ((double) w1 - (double) num20 + 2.0 * (double) num2);
    float x4 = (float) ((double) x1 - (double) num21 + 3.0 * (double) num2);
    float y4 = (float) ((double) y1 - (double) num22 + 3.0 * (double) num2);
    float z4 = (float) ((double) z1 - (double) num23 + 3.0 * (double) num2);
    float w4 = (float) ((double) w1 - (double) num24 + 3.0 * (double) num2);
    float x5 = (float) ((double) x1 - 1.0 + 4.0 * (double) num2);
    float y5 = (float) ((double) y1 - 1.0 + 4.0 * (double) num2);
    float z5 = (float) ((double) z1 - 1.0 + 4.0 * (double) num2);
    float w5 = (float) ((double) w1 - 1.0 + 4.0 * (double) num2);
    int num25 = num4 & (int) byte.MaxValue;
    int num26 = num5 & (int) byte.MaxValue;
    int num27 = num6 & (int) byte.MaxValue;
    int index2 = num7 & (int) byte.MaxValue;
    int index3 = UberMath.perm[num25 + UberMath.perm[num26 + UberMath.perm[num27 + UberMath.perm[index2]]]] % 32;
    int index4 = UberMath.perm[num25 + num13 + UberMath.perm[num26 + num14 + UberMath.perm[num27 + num15 + UberMath.perm[index2 + num16]]]] % 32;
    int index5 = UberMath.perm[num25 + num17 + UberMath.perm[num26 + num18 + UberMath.perm[num27 + num19 + UberMath.perm[index2 + num20]]]] % 32;
    int index6 = UberMath.perm[num25 + num21 + UberMath.perm[num26 + num22 + UberMath.perm[num27 + num23 + UberMath.perm[index2 + num24]]]] % 32;
    int index7 = UberMath.perm[num25 + 1 + UberMath.perm[num26 + 1 + UberMath.perm[num27 + 1 + UberMath.perm[index2 + 1]]]] % 32;
    float num28 = (float) (0.600000023841858 - (double) x1 * (double) x1 - (double) y1 * (double) y1 - (double) z1 * (double) z1 - (double) w1 * (double) w1);
    float num29;
    if ((double) num28 < 0.0)
    {
      num29 = 0.0f;
    }
    else
    {
      float num30 = num28 * num28;
      num29 = num30 * num30 * UberMath.dot(UberMath.grad4[index3, 0], UberMath.grad4[index3, 1], UberMath.grad4[index3, 2], UberMath.grad4[index3, 3], x1, y1, z1, w1);
    }
    float num31 = (float) (0.600000023841858 - (double) x2 * (double) x2 - (double) y2 * (double) y2 - (double) z2 * (double) z2 - (double) w2 * (double) w2);
    float num32;
    if ((double) num31 < 0.0)
    {
      num32 = 0.0f;
    }
    else
    {
      float num30 = num31 * num31;
      num32 = num30 * num30 * UberMath.dot(UberMath.grad4[index4, 0], UberMath.grad4[index4, 1], UberMath.grad4[index4, 2], UberMath.grad4[index4, 3], x2, y2, z2, w2);
    }
    float num33 = (float) (0.600000023841858 - (double) x3 * (double) x3 - (double) y3 * (double) y3 - (double) z3 * (double) z3 - (double) w3 * (double) w3);
    float num34;
    if ((double) num33 < 0.0)
    {
      num34 = 0.0f;
    }
    else
    {
      float num30 = num33 * num33;
      num34 = num30 * num30 * UberMath.dot(UberMath.grad4[index5, 0], UberMath.grad4[index5, 1], UberMath.grad4[index5, 2], UberMath.grad4[index5, 3], x3, y3, z3, w3);
    }
    float num35 = (float) (0.600000023841858 - (double) x4 * (double) x4 - (double) y4 * (double) y4 - (double) z4 * (double) z4 - (double) w4 * (double) w4);
    float num36;
    if ((double) num35 < 0.0)
    {
      num36 = 0.0f;
    }
    else
    {
      float num30 = num35 * num35;
      num36 = num30 * num30 * UberMath.dot(UberMath.grad4[index6, 0], UberMath.grad4[index6, 1], UberMath.grad4[index6, 2], UberMath.grad4[index6, 3], x4, y4, z4, w4);
    }
    float num37 = (float) (0.600000023841858 - (double) x5 * (double) x5 - (double) y5 * (double) y5 - (double) z5 * (double) z5 - (double) w5 * (double) w5);
    float num38;
    if ((double) num37 < 0.0)
    {
      num38 = 0.0f;
    }
    else
    {
      float num30 = num37 * num37;
      num38 = num30 * num30 * UberMath.dot(UberMath.grad4[index7, 0], UberMath.grad4[index7, 1], UberMath.grad4[index7, 2], UberMath.grad4[index7, 3], x5, y5, z5, w5);
    }
    return (float) (27.0 * ((double) num29 + (double) num32 + (double) num34 + (double) num36 + (double) num38));
  }
}
