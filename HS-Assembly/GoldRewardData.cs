﻿// Decompiled with JetBrains decompiler
// Type: GoldRewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

public class GoldRewardData : RewardData
{
  public long Amount { get; set; }

  public DateTime? Date { get; set; }

  public GoldRewardData()
    : this(0L)
  {
  }

  public GoldRewardData(long amount)
    : this(amount, new DateTime?())
  {
  }

  public GoldRewardData(long amount, DateTime? date)
    : base(Reward.Type.GOLD)
  {
    this.Amount = amount;
    this.Date = date;
  }

  public override string ToString()
  {
    return string.Format("[GoldRewardData: Amount={0} Origin={1} OriginData={2}]", (object) this.Amount, (object) this.Origin, (object) this.OriginData);
  }

  protected override string GetGameObjectName()
  {
    return "GoldReward";
  }
}
