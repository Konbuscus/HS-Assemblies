﻿// Decompiled with JetBrains decompiler
// Type: KARRunes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class KARRunes : MonoBehaviour
{
  public float m_bookBooshDelay = 0.5f;
  public float m_floorGlowDelay = 2.5f;
  public GameObject m_BookPageR;
  public GameObject m_BookPageL;
  public GameObject m_FloorRune;
  public Flipbook m_BookPageR_TurnedMesh;
  public Flipbook m_BookPageR_NotTurnedMesh;
  public Flipbook m_BookPageR_StaticMesh;
  public Flipbook m_BookPageL_StaticMesh;
  public Flipbook m_BookPageR_Glow;
  public Flipbook m_BookPageL_Glow;
  public Flipbook m_FloorPage;
  public Animator m_BookBooshAnim;
  public string m_BookBooshAnimState;
  public Animator m_BookGlowRAnim;
  public string m_BookGlowAnimRState;
  public Animator m_BookGlowLAnim;
  public string m_BookGlowAnimLState;
  public ParticleSystem m_FloorParticles;
  public Animator m_FloorGlowAnim;
  public string m_FloorGlowAnimState;
  public Animator m_LibraryBook;
  public string m_PageFlipRightAnimState;
  public string m_PageFlipLeftAnimState;
  public string m_BookShakeAnimState;
  public string m_RuneMatchSound;
  public List<string> m_PageFlipSounds;
  private bool m_isAnimating;
  private int m_leftIdx;
  private int m_rightIdx;
  private int m_floorRuneIdx;

  private void Start()
  {
    this.FlipBookPages(true);
    this.m_floorRuneIdx = Random.Range(0, 15);
    this.m_FloorPage.SetIndex(this.m_floorRuneIdx);
  }

  private void Update()
  {
    this.HandleHits();
  }

  private void HandleHits()
  {
    if (UniversalInputManager.Get().GetMouseButtonUp(0) && this.IsOver(this.m_BookPageR) && !this.m_isAnimating)
      this.FlipBookPages(true);
    if (UniversalInputManager.Get().GetMouseButtonUp(0) && this.IsOver(this.m_BookPageL) && !this.m_isAnimating)
      this.FlipBookPages(false);
    if (!UniversalInputManager.Get().GetMouseButtonUp(0) || !this.IsOver(this.m_FloorRune) || this.m_isAnimating)
      return;
    this.StartCoroutine(this.CheckRuneMatches());
  }

  private void FlipBookPages(bool isRight = true)
  {
    this.m_isAnimating = true;
    this.m_LibraryBook.enabled = true;
    if (this.m_PageFlipSounds.Count > 0)
    {
      string pageFlipSound = this.m_PageFlipSounds[Random.Range(0, this.m_PageFlipSounds.Count - 1)];
      if (pageFlipSound != null)
        SoundManager.Get().LoadAndPlay(pageFlipSound);
    }
    if (isRight)
    {
      this.m_LibraryBook.Play(this.m_PageFlipRightAnimState, -1, 0.0f);
      this.m_BookPageL_StaticMesh.SetIndex(this.m_leftIdx);
      this.m_leftIdx = Random.Range(0, 15);
      this.m_BookPageR_TurnedMesh.SetIndex(this.m_leftIdx);
      this.m_BookPageL_Glow.SetIndex(this.m_leftIdx);
      this.m_BookPageR_NotTurnedMesh.SetIndex(this.m_rightIdx);
      this.m_rightIdx = Random.Range(0, 15);
      while (this.m_rightIdx == this.m_leftIdx)
        this.m_rightIdx = Random.Range(0, 15);
      this.m_BookPageR_StaticMesh.SetIndex(this.m_rightIdx);
      this.m_BookPageR_Glow.SetIndex(this.m_rightIdx);
    }
    else
    {
      this.m_LibraryBook.Play(this.m_PageFlipLeftAnimState, -1, 0.0f);
      this.m_BookPageR_StaticMesh.SetIndex(this.m_rightIdx);
      this.m_rightIdx = Random.Range(0, 15);
      this.m_BookPageR_NotTurnedMesh.SetIndex(this.m_rightIdx);
      this.m_BookPageR_Glow.SetIndex(this.m_rightIdx);
      this.m_BookPageR_TurnedMesh.SetIndex(this.m_leftIdx);
      this.m_leftIdx = Random.Range(0, 15);
      while (this.m_leftIdx == this.m_rightIdx)
        this.m_leftIdx = Random.Range(0, 15);
      this.m_BookPageL_StaticMesh.SetIndex(this.m_leftIdx);
      this.m_BookPageL_Glow.SetIndex(this.m_leftIdx);
    }
    this.m_isAnimating = false;
  }

  [DebuggerHidden]
  private IEnumerator CheckRuneMatches()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KARRunes.\u003CCheckRuneMatches\u003Ec__Iterator12()
    {
      \u003C\u003Ef__this = this
    };
  }

  private bool IsOver(GameObject go)
  {
    return (bool) ((Object) go) && InputUtil.IsPlayMakerMouseInputAllowed(go) && UniversalInputManager.Get().InputIsOver(go);
  }
}
