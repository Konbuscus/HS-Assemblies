﻿// Decompiled with JetBrains decompiler
// Type: QuestDialogDbfAsset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class QuestDialogDbfAsset : ScriptableObject
{
  public List<QuestDialogDbfRecord> Records = new List<QuestDialogDbfRecord>();
}
