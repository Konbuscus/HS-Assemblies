﻿// Decompiled with JetBrains decompiler
// Type: ClassSpecificVoSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ClassSpecificVoSpell : CardSoundSpell
{
  public ClassSpecificVoData m_ClassSpecificVoData = new ClassSpecificVoData();

  public override AudioSource DetermineBestAudioSource()
  {
    AudioSource audioSource = this.SearchForClassSpecificVo();
    if ((bool) ((Object) audioSource))
      return audioSource;
    return base.DetermineBestAudioSource();
  }

  private AudioSource SearchForClassSpecificVo()
  {
    using (List<SpellZoneTag>.Enumerator enumerator = this.m_ClassSpecificVoData.m_ZonesToSearch.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AudioSource audioSource = this.SearchForClassSpecificVo(SpellUtils.FindZonesFromTag((Spell) this, enumerator.Current, this.m_ClassSpecificVoData.m_SideToSearch));
        if ((bool) ((Object) audioSource))
          return audioSource;
      }
    }
    return (AudioSource) null;
  }

  private AudioSource SearchForClassSpecificVo(List<Zone> zones)
  {
    if (zones == null)
      return (AudioSource) null;
    using (List<Zone>.Enumerator enumerator1 = zones.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Card>.Enumerator enumerator2 = enumerator1.Current.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            SpellClassTag spellEnum = SpellUtils.ConvertClassTagToSpellEnum(enumerator2.Current.GetEntity().GetClass());
            if (spellEnum != SpellClassTag.NONE)
            {
              using (List<ClassSpecificVoLine>.Enumerator enumerator3 = this.m_ClassSpecificVoData.m_Lines.GetEnumerator())
              {
                while (enumerator3.MoveNext())
                {
                  ClassSpecificVoLine current = enumerator3.Current;
                  if (current.m_Class == spellEnum)
                    return current.m_AudioSource;
                }
              }
            }
          }
        }
      }
    }
    return (AudioSource) null;
  }
}
