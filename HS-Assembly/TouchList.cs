﻿// Decompiled with JetBrains decompiler
// Type: TouchList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof (BoxCollider))]
public class TouchList : PegUIElement, IEnumerable, IList<ITouchListItem>, ICollection<ITouchListItem>, IEnumerable<ITouchListItem>
{
  private static readonly float ScrollBoundsSpringB = Mathf.Sqrt(1600f);
  private static readonly Func<float, float> OutOfBoundsDistReducer = (Func<float, float>) (dist => (float) (30.0 * ((double) Mathf.Log(dist + 30f) - (double) Mathf.Log(30f))));
  public TouchList.Alignment alignment = TouchList.Alignment.Mid;
  public Vector2 padding = Vector2.zero;
  public int breadth = 1;
  public float scrollWheelIncrement = 30f;
  public Float_MobileOverride maxKineticScrollSpeed = new Float_MobileOverride();
  private List<ITouchListItem> items = new List<ITouchListItem>();
  private Map<ITouchListItem, TouchList.ItemInfo> itemInfos = new Map<ITouchListItem, TouchList.ItemInfo>();
  private Vector3 dragBeginContentPosition = Vector3.zero;
  private Vector3 lastTouchPosition = Vector3.zero;
  private bool allowModification = true;
  private const float ScrollDragThreshold = 0.05f;
  private const float ItemDragThreshold = 0.05f;
  private const float KineticScrollFriction = 10000f;
  private const float MinKineticScrollSpeed = 0.01f;
  private const float ScrollBoundsSpringK = 400f;
  private const float MinOutOfBoundsDistance = 0.05f;
  private const float CLIPSIZE_EPSILON = 0.0001f;
  public TouchList.Orientation orientation;
  public TouchList.LayoutPlane layoutPlane;
  public float elementSpacing;
  public float itemDragFinishDistance;
  public TiledBackground background;
  private GameObject content;
  private int layoutDimension1;
  private int layoutDimension2;
  private int layoutDimension3;
  private float contentSize;
  private float excessContentSize;
  private float m_fullListContentSize;
  private Vector2? touchBeginScreenPosition;
  private Vector3? dragBeginOffsetFromContent;
  private float lastContentPosition;
  private ITouchListItem touchBeginItem;
  private bool m_isHoveredOverTouchList;
  private PegUIElement m_hoveredOverItem;
  private TouchList.ILongListBehavior longListBehavior;
  private Vector3? dragItemBegin;
  private bool layoutSuspended;
  private int? selection;

  public int Count
  {
    get
    {
      this.EnforceInitialized();
      return this.items.Count;
    }
  }

  public bool IsReadOnly
  {
    get
    {
      this.EnforceInitialized();
      return false;
    }
  }

  public bool IsInitialized
  {
    get
    {
      return (UnityEngine.Object) this.content != (UnityEngine.Object) null;
    }
  }

  public TouchList.ILongListBehavior LongListBehavior
  {
    get
    {
      this.EnforceInitialized();
      return this.longListBehavior;
    }
    set
    {
      this.EnforceInitialized();
      if (value == this.longListBehavior)
        return;
      this.allowModification = true;
      this.Clear();
      if (this.longListBehavior != null)
        this.longListBehavior.ReleaseAllItems();
      this.longListBehavior = value;
      if (this.longListBehavior == null)
        return;
      this.RefreshList(0, false);
      this.allowModification = false;
    }
  }

  public float ScrollValue
  {
    get
    {
      this.EnforceInitialized();
      float scrollableAmount = this.ScrollableAmount;
      float num = (double) scrollableAmount <= 0.0 ? 0.0f : Mathf.Clamp01(-this.content.transform.localPosition[this.layoutDimension1] / scrollableAmount);
      if ((double) num == 0.0 || (double) num == 1.0)
        return -this.GetOutOfBoundsDist(this.content.transform.localPosition[this.layoutDimension1]) / Mathf.Max(this.contentSize, this.ClipSize[this.GetVector2Dimension(this.layoutDimension1)]) + num;
      return num;
    }
    set
    {
      this.EnforceInitialized();
      if (this.dragBeginOffsetFromContent.HasValue || Mathf.Approximately(this.ScrollValue, value))
        return;
      float scrollableAmount = this.ScrollableAmount;
      Vector3 localPosition = this.content.transform.localPosition;
      localPosition[this.layoutDimension1] = -Mathf.Clamp01(value) * scrollableAmount;
      this.content.transform.localPosition = localPosition;
      float num = localPosition[this.layoutDimension1] - this.lastContentPosition;
      if ((double) num != 0.0)
        this.PreBufferLongListItems((double) num < 0.0);
      this.lastContentPosition = localPosition[this.layoutDimension1];
      this.FixUpScrolling();
      this.OnScrolled();
    }
  }

  public float ScrollableAmount
  {
    get
    {
      if (this.longListBehavior == null)
        return this.excessContentSize;
      return Mathf.Max(0.0f, this.m_fullListContentSize - this.ClipSize[this.GetVector2Dimension(this.layoutDimension1)]);
    }
  }

  public bool CanScrollAhead
  {
    get
    {
      if ((double) this.ScrollValue < 1.0)
        return true;
      if (this.longListBehavior != null && this.items.Count > 0)
      {
        for (int allItemsIndex = this.itemInfos[this.items.Last<ITouchListItem>()].LongListIndex + 1; allItemsIndex < this.longListBehavior.AllItemsCount; ++allItemsIndex)
        {
          if (this.longListBehavior.IsItemShowable(allItemsIndex))
            return true;
        }
      }
      return false;
    }
  }

  public bool CanScrollBehind
  {
    get
    {
      if ((double) this.ScrollValue > 0.0)
        return true;
      if (this.longListBehavior != null && this.items.Count > 0)
      {
        TouchList.ItemInfo itemInfo = this.itemInfos[this.items.First<ITouchListItem>()];
        if (this.longListBehavior.AllItemsCount > 0)
        {
          for (int allItemsIndex = itemInfo.LongListIndex - 1; allItemsIndex >= 0; --allItemsIndex)
          {
            if (this.longListBehavior.IsItemShowable(allItemsIndex))
              return true;
          }
        }
      }
      return false;
    }
  }

  public float ViewWindowMinValue
  {
    get
    {
      return -this.content.transform.localPosition[this.layoutDimension1] / this.contentSize;
    }
    set
    {
      Vector3 localPosition = this.content.transform.localPosition;
      localPosition[this.layoutDimension1] = -Mathf.Clamp01(value) * this.contentSize;
      this.content.transform.localPosition = localPosition;
      float num = this.content.transform.localPosition[this.layoutDimension1] - this.lastContentPosition;
      if ((double) num != 0.0)
        this.PreBufferLongListItems((double) num < 0.0);
      this.lastContentPosition = localPosition[this.layoutDimension1];
      this.OnScrolled();
    }
  }

  public float ViewWindowMaxValue
  {
    get
    {
      return (-this.content.transform.localPosition[this.layoutDimension1] + this.ClipSize[this.GetVector2Dimension(this.layoutDimension1)]) / this.contentSize;
    }
    set
    {
      Vector3 localPosition = this.content.transform.localPosition;
      localPosition[this.layoutDimension1] = -Mathf.Clamp01(value) * this.contentSize + this.ClipSize[this.GetVector2Dimension(this.layoutDimension1)];
      this.content.transform.localPosition = localPosition;
      float num = this.content.transform.localPosition[this.layoutDimension1] - this.lastContentPosition;
      if ((double) num != 0.0)
        this.PreBufferLongListItems((double) num < 0.0);
      this.lastContentPosition = localPosition[this.layoutDimension1];
      this.OnScrolled();
    }
  }

  public Vector2 ClipSize
  {
    get
    {
      this.EnforceInitialized();
      BoxCollider component = this.GetComponent<Collider>() as BoxCollider;
      return new Vector2(component.size.x, this.layoutPlane != TouchList.LayoutPlane.XY ? component.size.z : component.size.y);
    }
    set
    {
      this.EnforceInitialized();
      BoxCollider component = this.GetComponent<Collider>() as BoxCollider;
      Vector3 vector3_1 = new Vector3(value.x, 0.0f, 0.0f);
      vector3_1[1] = this.layoutPlane != TouchList.LayoutPlane.XY ? component.size.y : value.y;
      vector3_1[2] = this.layoutPlane != TouchList.LayoutPlane.XZ ? component.size.z : value.y;
      Vector3 vector3_2 = VectorUtils.Abs(component.size - vector3_1);
      if ((double) vector3_2.x <= 9.99999974737875E-05 && (double) vector3_2.y <= 9.99999974737875E-05 && (double) vector3_2.z <= 9.99999974737875E-05)
        return;
      component.size = vector3_1;
      this.UpdateBackgroundBounds();
      if (this.longListBehavior == null)
        this.RepositionItems(0, new Vector3?());
      else
        this.RefreshList(0, true);
      if (this.ClipSizeChanged == null)
        return;
      this.ClipSizeChanged();
    }
  }

  public bool SelectionEnabled
  {
    get
    {
      this.EnforceInitialized();
      return this.selection.HasValue;
    }
    set
    {
      this.EnforceInitialized();
      if (value == this.SelectionEnabled)
        return;
      if (value)
        this.selection = new int?(-1);
      else
        this.selection = new int?();
    }
  }

  public int SelectedIndex
  {
    get
    {
      this.EnforceInitialized();
      if (this.selection.HasValue)
        return this.selection.Value;
      return -1;
    }
    set
    {
      this.EnforceInitialized();
      if (!this.SelectionEnabled)
        return;
      int num = value;
      int? selection1 = this.selection;
      int valueOrDefault = selection1.GetValueOrDefault();
      if ((num != valueOrDefault ? 0 : (selection1.HasValue ? 1 : 0)) != 0 || this.SelectedIndexChanging != null && !this.SelectedIndexChanging(value))
        return;
      ISelectableTouchListItem selectedItem = this.SelectedItem as ISelectableTouchListItem;
      ISelectableTouchListItem selectableTouchListItem = (value == -1 ? (ITouchListItem) null : this[value]) as ISelectableTouchListItem;
      if (value == -1 || selectableTouchListItem != null && selectableTouchListItem.Selectable)
        this.selection = new int?(value);
      if (selectedItem != null)
      {
        int? selection2 = this.selection;
        if ((selection2.GetValueOrDefault() != value ? 0 : (selection2.HasValue ? 1 : 0)) != 0)
          selectedItem.Unselected();
      }
      int? selection3 = this.selection;
      if ((selection3.GetValueOrDefault() != value ? 0 : (selection3.HasValue ? 1 : 0)) == 0 || selectableTouchListItem == null)
        return;
      selectableTouchListItem.Selected();
      this.ScrollToItem((ITouchListItem) selectableTouchListItem);
    }
  }

  public ITouchListItem SelectedItem
  {
    get
    {
      this.EnforceInitialized();
      if (this.selection.HasValue && this.selection.Value != -1)
        return this[this.selection.Value];
      return (ITouchListItem) null;
    }
    set
    {
      this.EnforceInitialized();
      int num = this.items.IndexOf(value);
      if (num == -1)
        return;
      this.SelectedIndex = num;
    }
  }

  public ITouchListItem this[int index]
  {
    get
    {
      return this.items[index];
    }
    set
    {
      if (!this.allowModification)
        return;
      this.items[index] = value;
    }
  }

  public bool IsLayoutSuspended
  {
    get
    {
      return this.layoutSuspended;
    }
  }

  public event Action Scrolled;

  public event TouchList.SelectedIndexChangingEvent SelectedIndexChanging;

  public event TouchList.ScrollingEnabledChangedEvent ScrollingEnabledChanged;

  public event Action ClipSizeChanged;

  public event TouchList.ItemDragEvent ItemDragStarted;

  public event TouchList.ItemDragEvent ItemDragged;

  public event TouchList.ItemDragEvent ItemDragFinished;

  IEnumerator IEnumerable.GetEnumerator()
  {
    this.EnforceInitialized();
    return (IEnumerator) this.items.GetEnumerator();
  }

  private void FixUpScrolling()
  {
    if (this.longListBehavior == null || this.items.Count <= 0)
      return;
    Bounds localClipBounds = this.CalculateLocalClipBounds();
    TouchList.ItemInfo itemInfo1 = this.itemInfos[this.items[0]];
    if (itemInfo1.LongListIndex == 0 && !this.CanScrollBehind)
    {
      float num = localClipBounds.min[this.layoutDimension1];
      Vector3 min = itemInfo1.Min;
      if ((double) min[this.layoutDimension1] == (double) num)
        return;
      Vector3 zero = Vector3.zero;
      zero[this.layoutDimension1] = num - min[this.layoutDimension1];
      for (int index = 0; index < this.items.Count; ++index)
        this.items[index].gameObject.transform.Translate(-zero);
    }
    else
    {
      if (this.items.Count <= 1 || this.CanScrollAhead)
        return;
      float num = localClipBounds.max[this.layoutDimension1];
      TouchList.ItemInfo itemInfo2 = this.itemInfos[this.items[this.items.Count - 1]];
      if (itemInfo2.LongListIndex < this.longListBehavior.AllItemsCount - 1)
        return;
      Vector3 max = itemInfo2.Max;
      if ((double) max[this.layoutDimension1] == (double) num)
        return;
      Vector3 zero = Vector3.zero;
      zero[this.layoutDimension1] = num - max[this.layoutDimension1];
      for (int index = 0; index < this.items.Count; ++index)
        this.items[index].gameObject.transform.Translate(-zero);
    }
  }

  public void Add(ITouchListItem item)
  {
    this.Add(item, true);
  }

  public void Add(ITouchListItem item, bool repositionItems)
  {
    this.EnforceInitialized();
    if (!this.allowModification)
      return;
    this.items.Add(item);
    Vector3 negatedScale = this.GetNegatedScale(item.transform.localScale);
    item.transform.parent = this.content.transform;
    item.transform.localPosition = Vector3.zero;
    item.transform.localRotation = Quaternion.identity;
    if (this.orientation == TouchList.Orientation.Vertical)
      item.transform.localScale = negatedScale;
    this.itemInfos[item] = new TouchList.ItemInfo(item, this.layoutPlane);
    item.gameObject.SetActive(false);
    int? selection = this.selection;
    if ((selection.GetValueOrDefault() != -1 ? 0 : (selection.HasValue ? 1 : 0)) != 0 && item is ISelectableTouchListItem && ((ISelectableTouchListItem) item).IsSelected())
      this.selection = new int?(this.items.Count - 1);
    if (!repositionItems)
      return;
    this.RepositionItems(this.items.Count - 1, new Vector3?());
    this.RecalculateLongListContentSize(true);
  }

  public void Clear()
  {
    this.EnforceInitialized();
    if (!this.allowModification)
      return;
    using (List<ITouchListItem>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ITouchListItem current = enumerator.Current;
        Vector3 negatedScale = this.GetNegatedScale(current.transform.localScale);
        current.transform.parent = (Transform) null;
        if (this.orientation == TouchList.Orientation.Vertical)
          current.transform.localScale = negatedScale;
      }
    }
    this.content.transform.localPosition = Vector3.zero;
    this.lastContentPosition = 0.0f;
    this.items.Clear();
    this.RecalculateSize();
    this.UpdateBackgroundScroll();
    this.RecalculateLongListContentSize(true);
    if (this.SelectionEnabled)
      this.SelectedIndex = -1;
    if (!((UnityEngine.Object) this.m_hoveredOverItem != (UnityEngine.Object) null))
      return;
    this.m_hoveredOverItem.TriggerOut();
    this.m_hoveredOverItem = (PegUIElement) null;
  }

  public bool Contains(ITouchListItem item)
  {
    this.EnforceInitialized();
    return this.items.Contains(item);
  }

  public void CopyTo(ITouchListItem[] array, int arrayIndex)
  {
    this.EnforceInitialized();
    this.items.CopyTo(array, arrayIndex);
  }

  private List<ITouchListItem> GetItemsInView()
  {
    this.EnforceInitialized();
    List<ITouchListItem> touchListItemList = new List<ITouchListItem>();
    float num = this.CalculateLocalClipBounds().max[this.layoutDimension1];
    for (int numItemsBehindView = this.GetNumItemsBehindView(); numItemsBehindView < this.items.Count && (double) (this.itemInfos[this.items[numItemsBehindView]].Min - this.content.transform.localPosition)[this.layoutDimension1] < (double) num; ++numItemsBehindView)
      touchListItemList.Add(this.items[numItemsBehindView]);
    return touchListItemList;
  }

  public void SetVisibilityOfAllItems()
  {
    if (this.layoutSuspended)
      return;
    this.EnforceInitialized();
    Bounds localClipBounds = this.CalculateLocalClipBounds();
    for (int visualizedListIndex = 0; visualizedListIndex < this.items.Count; ++visualizedListIndex)
    {
      ITouchListItem touchListItem = this.items[visualizedListIndex];
      bool flag = this.IsItemVisible_Internal(visualizedListIndex, ref localClipBounds);
      if (flag != touchListItem.gameObject.activeSelf)
        touchListItem.gameObject.SetActive(flag);
    }
  }

  private bool IsItemVisible_Internal(int visualizedListIndex, ref Bounds localClipBounds)
  {
    TouchList.ItemInfo itemInfo = this.itemInfos[this.items[visualizedListIndex]];
    return this.IsWithinClipBounds(itemInfo.Min, itemInfo.Max, ref localClipBounds);
  }

  private bool IsWithinClipBounds(Vector3 localBoundsMin, Vector3 localBoundsMax, ref Bounds localClipBounds)
  {
    float num1 = localClipBounds.min[this.layoutDimension1];
    float num2 = localClipBounds.max[this.layoutDimension1];
    return (double) localBoundsMax[this.layoutDimension1] >= (double) num1 && (double) localBoundsMin[this.layoutDimension1] <= (double) num2;
  }

  private bool IsItemVisible(int visualizedListIndex)
  {
    Bounds localClipBounds = this.CalculateLocalClipBounds();
    return this.IsItemVisible_Internal(visualizedListIndex, ref localClipBounds);
  }

  public IEnumerator<ITouchListItem> GetEnumerator()
  {
    this.EnforceInitialized();
    return (IEnumerator<ITouchListItem>) this.items.GetEnumerator();
  }

  public int IndexOf(ITouchListItem item)
  {
    this.EnforceInitialized();
    return this.items.IndexOf(item);
  }

  public void Insert(int index, ITouchListItem item)
  {
    this.Insert(index, item, true);
  }

  public void Insert(int index, ITouchListItem item, bool repositionItems)
  {
    this.EnforceInitialized();
    if (!this.allowModification)
      return;
    this.items.Insert(index, item);
    Vector3 negatedScale = this.GetNegatedScale(item.transform.localScale);
    item.transform.parent = this.content.transform;
    item.transform.localPosition = Vector3.zero;
    item.transform.localRotation = Quaternion.identity;
    if (this.orientation == TouchList.Orientation.Vertical)
      item.transform.localScale = negatedScale;
    this.itemInfos[item] = new TouchList.ItemInfo(item, this.layoutPlane);
    int? selection = this.selection;
    if ((selection.GetValueOrDefault() != -1 ? 0 : (selection.HasValue ? 1 : 0)) != 0 && item is ISelectableTouchListItem && ((ISelectableTouchListItem) item).IsSelected())
      this.selection = new int?(index);
    if (!repositionItems)
      return;
    this.RepositionItems(index, new Vector3?());
    this.RecalculateLongListContentSize(true);
  }

  public bool Remove(ITouchListItem item)
  {
    this.EnforceInitialized();
    if (!this.allowModification)
      return false;
    int index = this.items.IndexOf(item);
    if (index == -1)
      return false;
    this.RemoveAt(index, true);
    return true;
  }

  public void RemoveAt(int index)
  {
    this.RemoveAt(index, true);
  }

  public void RemoveAt(int index, bool repositionItems)
  {
    this.EnforceInitialized();
    if (!this.allowModification)
      return;
    Vector3 negatedScale = this.GetNegatedScale(this.items[index].transform.localScale);
    ITouchListItem touchListItem = this.items[index];
    touchListItem.transform.parent = this.transform;
    if (this.orientation == TouchList.Orientation.Vertical)
      this.items[index].transform.localScale = negatedScale;
    this.itemInfos.Remove(this.items[index]);
    this.items.RemoveAt(index);
    int num = index;
    int? selection1 = this.selection;
    int valueOrDefault = selection1.GetValueOrDefault();
    if ((num != valueOrDefault ? 0 : (selection1.HasValue ? 1 : 0)) != 0)
    {
      this.selection = new int?(-1);
    }
    else
    {
      int? selection2 = this.selection;
      if ((!selection2.HasValue ? 0 : (index < selection2.Value ? 1 : 0)) != 0)
      {
        int? selection3 = this.selection;
        if (selection3.HasValue)
          this.selection = new int?(selection3.Value - 1);
      }
    }
    if ((UnityEngine.Object) this.m_hoveredOverItem != (UnityEngine.Object) null && (UnityEngine.Object) touchListItem.GetComponent<PegUIElement>() == (UnityEngine.Object) this.m_hoveredOverItem)
    {
      this.m_hoveredOverItem.TriggerOut();
      this.m_hoveredOverItem = (PegUIElement) null;
    }
    if (!repositionItems)
      return;
    this.RepositionItems(index, new Vector3?());
    this.RecalculateLongListContentSize(true);
  }

  public int FindIndex(Predicate<ITouchListItem> match)
  {
    this.EnforceInitialized();
    return this.items.FindIndex(match);
  }

  public void Sort(Comparison<ITouchListItem> comparison)
  {
    this.EnforceInitialized();
    ITouchListItem selectedItem = this.SelectedItem;
    this.items.Sort(comparison);
    this.RepositionItems(0, new Vector3?());
    this.selection = new int?(this.items.IndexOf(selectedItem));
  }

  public void SuspendLayout()
  {
    this.EnforceInitialized();
    this.layoutSuspended = true;
  }

  public void ResumeLayout(bool repositionItems = true)
  {
    this.EnforceInitialized();
    this.layoutSuspended = false;
    if (!repositionItems)
      return;
    this.RepositionItems(0, new Vector3?());
  }

  public void ResetState()
  {
    this.touchBeginScreenPosition = new Vector2?();
    this.dragBeginOffsetFromContent = new Vector3?();
    this.dragBeginContentPosition = Vector3.zero;
    this.lastTouchPosition = Vector3.zero;
    this.lastContentPosition = 0.0f;
    this.dragItemBegin = new Vector3?();
    if (!((UnityEngine.Object) this.content != (UnityEngine.Object) null))
      return;
    this.content.transform.localPosition = Vector3.zero;
  }

  protected override void Awake()
  {
    base.Awake();
    this.content = new GameObject("Content");
    this.content.transform.parent = this.transform;
    TransformUtil.Identity((Component) this.content.transform);
    this.layoutDimension1 = 0;
    this.layoutDimension2 = this.layoutPlane != TouchList.LayoutPlane.XY ? 2 : 1;
    this.layoutDimension3 = 3 - this.layoutDimension2;
    if (this.orientation == TouchList.Orientation.Vertical)
    {
      GeneralUtils.Swap<int>(ref this.layoutDimension1, ref this.layoutDimension2);
      Vector3 one = Vector3.one;
      one[this.layoutDimension1] = -1f;
      this.transform.localScale = one;
    }
    if (!((UnityEngine.Object) this.background != (UnityEngine.Object) null))
      return;
    if (this.orientation == TouchList.Orientation.Vertical)
      this.background.transform.localScale = this.GetNegatedScale(this.background.transform.localScale);
    this.UpdateBackgroundBounds();
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    this.m_isHoveredOverTouchList = true;
    this.OnHover(true);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_isHoveredOverTouchList = false;
    if (!((UnityEngine.Object) this.m_hoveredOverItem != (UnityEngine.Object) null))
      return;
    this.m_hoveredOverItem.TriggerOut();
    this.m_hoveredOverItem = (PegUIElement) null;
  }

  private void OnHover(bool isKnownOver)
  {
    if (UniversalInputManager.Get().IsTouchMode())
      return;
    if (this.touchBeginItem != null || this.dragItemBegin.HasValue)
    {
      if (!((UnityEngine.Object) this.m_hoveredOverItem != (UnityEngine.Object) null))
        return;
      this.m_hoveredOverItem.TriggerOut();
      this.m_hoveredOverItem = (PegUIElement) null;
    }
    else
    {
      Camera firstByLayer = CameraUtils.FindFirstByLayer(this.gameObject.layer);
      if ((UnityEngine.Object) firstByLayer == (UnityEngine.Object) null)
      {
        if (!((UnityEngine.Object) this.m_hoveredOverItem != (UnityEngine.Object) null))
          return;
        this.m_hoveredOverItem.TriggerOut();
        this.m_hoveredOverItem = (PegUIElement) null;
      }
      else
      {
        RaycastHit hitInfo;
        if (!isKnownOver && (!UniversalInputManager.Get().GetInputHitInfo(firstByLayer, out hitInfo) || (UnityEngine.Object) hitInfo.transform != (UnityEngine.Object) this.transform) && (UnityEngine.Object) this.m_hoveredOverItem != (UnityEngine.Object) null)
        {
          this.m_hoveredOverItem.TriggerOut();
          this.m_hoveredOverItem = (PegUIElement) null;
        }
        this.GetComponent<Collider>().enabled = false;
        PegUIElement pegUiElement = (PegUIElement) null;
        if (UniversalInputManager.Get().GetInputHitInfo(firstByLayer, out hitInfo))
          pegUiElement = hitInfo.transform.GetComponent<PegUIElement>();
        this.GetComponent<Collider>().enabled = true;
        if (!((UnityEngine.Object) pegUiElement != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_hoveredOverItem != (UnityEngine.Object) pegUiElement))
          return;
        if ((UnityEngine.Object) this.m_hoveredOverItem != (UnityEngine.Object) null)
          this.m_hoveredOverItem.TriggerOut();
        pegUiElement.TriggerOver();
        this.m_hoveredOverItem = pegUiElement;
      }
    }
  }

  protected override void OnPress()
  {
    if ((UnityEngine.Object) CameraUtils.FindFirstByLayer(this.gameObject.layer) == (UnityEngine.Object) null)
      return;
    this.touchBeginScreenPosition = new Vector2?((Vector2) UniversalInputManager.Get().GetMousePosition());
    if ((double) this.lastContentPosition != (double) this.content.transform.localPosition[this.layoutDimension1])
      return;
    Vector3 vector3 = this.GetTouchPosition() - this.content.transform.localPosition;
    for (int index1 = 0; index1 < this.items.Count; ++index1)
    {
      ITouchListItem index2 = this.items[index1];
      if ((index2.IsHeader || index2.Visible) && this.itemInfos[index2].Contains((Vector2) vector3, this.layoutPlane))
      {
        this.touchBeginItem = index2;
        break;
      }
    }
  }

  protected override void OnRelease()
  {
    Camera firstByLayer = CameraUtils.FindFirstByLayer(this.gameObject.layer);
    if ((UnityEngine.Object) firstByLayer == (UnityEngine.Object) null || this.touchBeginItem == null || this.dragItemBegin.HasValue)
      return;
    this.touchBeginScreenPosition = new Vector2?();
    this.GetComponent<Collider>().enabled = false;
    PegUIElement pegUiElement = (PegUIElement) null;
    RaycastHit hitInfo;
    if (UniversalInputManager.Get().GetInputHitInfo(firstByLayer, out hitInfo))
      pegUiElement = hitInfo.transform.GetComponent<PegUIElement>();
    this.GetComponent<Collider>().enabled = true;
    if (!((UnityEngine.Object) pegUiElement != (UnityEngine.Object) null))
      return;
    pegUiElement.TriggerRelease();
    this.touchBeginItem = (ITouchListItem) null;
  }

  private void EnforceInitialized()
  {
    if (!this.IsInitialized)
      throw new InvalidOperationException("TouchList must be initialized before using it. Please wait for Awake to finish.");
  }

  private void Update()
  {
    if (UniversalInputManager.Get().IsTouchMode())
      this.UpdateTouchInput();
    else
      this.UpdateMouseInput();
    if (!this.m_isHoveredOverTouchList)
      return;
    this.OnHover(false);
  }

  private void UpdateTouchInput()
  {
    Vector3 touchPosition = this.GetTouchPosition();
    if (UniversalInputManager.Get().GetMouseButtonUp(0))
    {
      if (this.dragItemBegin.HasValue && this.ItemDragFinished != null)
      {
        int num = this.ItemDragFinished(this.touchBeginItem, this.GetItemDragDelta(touchPosition)) ? 1 : 0;
        this.dragItemBegin = new Vector3?();
      }
      this.touchBeginItem = (ITouchListItem) null;
      this.touchBeginScreenPosition = new Vector2?();
      this.dragBeginOffsetFromContent = new Vector3?();
    }
    if (this.touchBeginScreenPosition.HasValue)
    {
      Func<int, float, bool> func = (Func<int, float, bool>) ((dimension, inchThreshold) =>
      {
        int vector2Dimension = this.GetVector2Dimension(dimension);
        return (double) Mathf.Abs(this.touchBeginScreenPosition.Value[vector2Dimension] - UniversalInputManager.Get().GetMousePosition()[vector2Dimension]) > (double) (inchThreshold * ((double) Screen.dpi <= 0.0 ? 150f : Screen.dpi));
      });
      if (this.ItemDragStarted != null && func(this.layoutDimension2, 0.05f) && this.ItemDragStarted(this.touchBeginItem, this.GetItemDragDelta(touchPosition)))
      {
        this.dragItemBegin = new Vector3?(this.GetTouchPosition());
        this.touchBeginScreenPosition = new Vector2?();
      }
      else if (func(this.layoutDimension1, 0.05f))
      {
        this.dragBeginContentPosition = this.content.transform.localPosition;
        this.dragBeginOffsetFromContent = new Vector3?(this.dragBeginContentPosition - this.lastTouchPosition);
        this.touchBeginItem = (ITouchListItem) null;
        this.touchBeginScreenPosition = new Vector2?();
      }
    }
    if (this.dragItemBegin.HasValue)
    {
      if (!this.ItemDragged(this.touchBeginItem, this.GetItemDragDelta(touchPosition)))
      {
        this.dragItemBegin = new Vector3?();
        this.touchBeginItem = (ITouchListItem) null;
      }
    }
    else if (this.dragBeginOffsetFromContent.HasValue)
    {
      float contentPosition = touchPosition[this.layoutDimension1] + this.dragBeginOffsetFromContent.Value[this.layoutDimension1];
      float outOfBoundsDist = this.GetOutOfBoundsDist(contentPosition);
      if ((double) outOfBoundsDist != 0.0)
      {
        float num = TouchList.OutOfBoundsDistReducer(Mathf.Abs(outOfBoundsDist)) * Mathf.Sign(outOfBoundsDist);
        contentPosition = (double) num >= 0.0 ? num : -this.excessContentSize + num;
      }
      Vector3 localPosition = this.content.transform.localPosition;
      this.lastContentPosition = localPosition[this.layoutDimension1];
      localPosition[this.layoutDimension1] = contentPosition;
      this.content.transform.localPosition = localPosition;
      if ((double) this.lastContentPosition != (double) localPosition[this.layoutDimension1])
        this.OnScrolled();
    }
    else
    {
      float contentPosition = this.content.transform.localPosition[this.layoutDimension1];
      float outOfBoundsDist = this.GetOutOfBoundsDist(contentPosition);
      float num1 = (this.content.transform.localPosition[this.layoutDimension1] - this.lastContentPosition) / Time.fixedDeltaTime;
      if ((double) (float) ((MobileOverrideValue<float>) this.maxKineticScrollSpeed) > (double) Mathf.Epsilon)
        num1 = (double) num1 <= 0.0 ? Mathf.Max(num1, -(float) ((MobileOverrideValue<float>) this.maxKineticScrollSpeed)) : Mathf.Min(num1, (float) ((MobileOverrideValue<float>) this.maxKineticScrollSpeed));
      if ((double) outOfBoundsDist != 0.0)
      {
        Vector3 localPosition = this.content.transform.localPosition;
        this.lastContentPosition = contentPosition;
        float num2 = (float) (-400.0 * (double) outOfBoundsDist - (double) TouchList.ScrollBoundsSpringB * (double) num1);
        float num3 = num1 + num2 * Time.fixedDeltaTime;
        localPosition[this.layoutDimension1] += num3 * Time.fixedDeltaTime;
        if ((double) Mathf.Abs(this.GetOutOfBoundsDist(localPosition[this.layoutDimension1])) < 0.0500000007450581)
        {
          float num4 = (double) Mathf.Abs(localPosition[this.layoutDimension1] + this.excessContentSize) >= (double) Mathf.Abs(localPosition[this.layoutDimension1]) ? 0.0f : -this.excessContentSize;
          localPosition[this.layoutDimension1] = num4;
          this.lastContentPosition = num4;
        }
        this.content.transform.localPosition = localPosition;
        this.OnScrolled();
      }
      else if ((double) num1 != 0.0)
      {
        this.lastContentPosition = this.content.transform.localPosition[this.layoutDimension1];
        float num2 = (float) (-(double) Mathf.Sign(num1) * 10000.0);
        float f = num1 + num2 * Time.fixedDeltaTime;
        if ((double) Mathf.Abs(f) >= 0.00999999977648258 && (double) Mathf.Sign(f) == (double) Mathf.Sign(num1))
        {
          Vector3 localPosition = this.content.transform.localPosition;
          localPosition[this.layoutDimension1] += f * Time.fixedDeltaTime;
          this.content.transform.localPosition = localPosition;
          this.OnScrolled();
        }
      }
    }
    float num5 = this.content.transform.localPosition[this.layoutDimension1] - this.lastContentPosition;
    if ((double) num5 != 0.0)
      this.PreBufferLongListItems((double) num5 < 0.0);
    this.lastTouchPosition = touchPosition;
  }

  private void PreBufferLongListItems(bool scrolledAhead)
  {
    if (this.LongListBehavior == null)
      return;
    this.allowModification = true;
    if (scrolledAhead && this.GetNumItemsAheadOfView() < this.longListBehavior.MinBuffer)
    {
      bool flag = this.CanScrollAhead;
      if (this.items.Count > 0 && (double) this.itemInfos[this.items[this.items.Count - 1]].Max[this.layoutDimension1] < (double) this.CalculateLocalClipBounds().min[this.layoutDimension1])
      {
        this.RefreshList(0, true);
        flag = false;
      }
      if (flag)
        this.LoadAhead();
    }
    else if (!scrolledAhead && this.GetNumItemsBehindView() < this.longListBehavior.MinBuffer)
    {
      bool flag = this.CanScrollBehind;
      if (this.items.Count > 0 && (double) this.itemInfos[this.items[0]].Min[this.layoutDimension1] > (double) this.CalculateLocalClipBounds().max[this.layoutDimension1])
      {
        this.RefreshList(0, true);
        flag = false;
      }
      if (flag)
        this.LoadBehind();
    }
    this.allowModification = false;
  }

  private void UpdateMouseInput()
  {
    Camera firstByLayer = CameraUtils.FindFirstByLayer(this.gameObject.layer);
    RaycastHit hitInfo;
    if ((UnityEngine.Object) firstByLayer == (UnityEngine.Object) null || !this.GetComponent<Collider>().Raycast(firstByLayer.ScreenPointToRay(UniversalInputManager.Get().GetMousePosition()), out hitInfo, firstByLayer.farClipPlane))
      return;
    float f = 0.0f;
    if ((double) Input.GetAxis("Mouse ScrollWheel") < 0.0 && this.CanScrollAhead)
      f -= this.scrollWheelIncrement;
    if ((double) Input.GetAxis("Mouse ScrollWheel") > 0.0 && this.CanScrollBehind)
      f += this.scrollWheelIncrement;
    if ((double) Mathf.Abs(f) <= (double) Mathf.Epsilon)
      return;
    float num1 = this.content.transform.localPosition[this.layoutDimension1] + f;
    if ((double) num1 <= -(double) this.excessContentSize)
      num1 = -this.excessContentSize;
    else if ((double) num1 >= 0.0)
      num1 = 0.0f;
    Vector3 localPosition = this.content.transform.localPosition;
    this.lastContentPosition = localPosition[this.layoutDimension1];
    localPosition[this.layoutDimension1] = num1;
    this.content.transform.localPosition = localPosition;
    float num2 = this.content.transform.localPosition[this.layoutDimension1] - this.lastContentPosition;
    this.lastContentPosition = this.content.transform.localPosition[this.layoutDimension1];
    if ((double) num2 != 0.0)
      this.PreBufferLongListItems((double) num2 < 0.0);
    this.FixUpScrolling();
    this.OnScrolled();
  }

  private float GetOutOfBoundsDist(float contentPosition)
  {
    float num = 0.0f;
    if ((double) contentPosition < -(double) this.excessContentSize)
      num = contentPosition + this.excessContentSize;
    else if ((double) contentPosition > 0.0)
      num = contentPosition;
    return num;
  }

  private void ScrollToItem(ITouchListItem item)
  {
    Bounds localClipBounds = this.CalculateLocalClipBounds();
    TouchList.ItemInfo itemInfo = this.itemInfos[item];
    float num1 = itemInfo.Max[this.layoutDimension1] - localClipBounds.max[this.layoutDimension1];
    if ((double) num1 > 0.0)
    {
      Vector3 zero = Vector3.zero;
      zero[this.layoutDimension1] = num1;
      this.content.transform.Translate(zero);
      this.lastContentPosition = this.content.transform.localPosition[this.layoutDimension1];
      this.PreBufferLongListItems(true);
      this.OnScrolled();
    }
    float num2 = localClipBounds.min[this.layoutDimension1] - itemInfo.Min[this.layoutDimension1];
    if ((double) num2 <= 0.0)
      return;
    Vector3 zero1 = Vector3.zero;
    zero1[this.layoutDimension1] = -num2;
    this.content.transform.Translate(zero1);
    this.lastContentPosition = this.content.transform.localPosition[this.layoutDimension1];
    this.PreBufferLongListItems(false);
    this.OnScrolled();
  }

  private void OnScrolled()
  {
    this.UpdateBackgroundScroll();
    this.SetVisibilityOfAllItems();
    if (this.Scrolled == null)
      return;
    this.Scrolled();
  }

  private Vector3 GetTouchPosition()
  {
    Camera firstByLayer = CameraUtils.FindFirstByLayer(this.gameObject.layer);
    if ((UnityEngine.Object) firstByLayer == (UnityEngine.Object) null)
      return Vector3.zero;
    Vector3 inPoint = (double) Vector3.Distance(firstByLayer.transform.position, this.GetComponent<Collider>().bounds.min) >= (double) Vector3.Distance(firstByLayer.transform.position, this.GetComponent<Collider>().bounds.max) ? this.GetComponent<Collider>().bounds.max : this.GetComponent<Collider>().bounds.min;
    Plane plane = new Plane(-firstByLayer.transform.forward, inPoint);
    Ray ray = firstByLayer.ScreenPointToRay(UniversalInputManager.Get().GetMousePosition());
    float enter;
    plane.Raycast(ray, out enter);
    return this.transform.InverseTransformPoint(ray.GetPoint(enter));
  }

  private float GetItemDragDelta(Vector3 touchPosition)
  {
    if (this.dragItemBegin.HasValue)
      return touchPosition[this.layoutDimension2] - this.dragItemBegin.Value[this.layoutDimension2];
    return 0.0f;
  }

  private void LoadAhead()
  {
    bool allowModification = this.allowModification;
    bool layoutSuspended = this.layoutSuspended;
    this.allowModification = true;
    int startingIndex = -1;
    int num1 = 0;
    int numItemsBehindView = this.GetNumItemsBehindView();
    for (int index = 0; index < numItemsBehindView - this.longListBehavior.MinBuffer; ++index)
    {
      ITouchListItem touchListItem = this.items[0];
      this.RemoveAt(0, false);
      this.longListBehavior.ReleaseItem(touchListItem);
    }
    float num2 = this.CalculateLocalClipBounds().max[this.layoutDimension1];
    int num3 = 0;
    for (int index1 = this.items.Count != 0 ? this.itemInfos[this.items.Last<ITouchListItem>()].LongListIndex + 1 : 0; index1 < this.longListBehavior.AllItemsCount && this.items.Count < this.longListBehavior.MaxAcquiredItems && num3 < this.longListBehavior.MinBuffer; ++index1)
    {
      if (this.longListBehavior.IsItemShowable(index1))
      {
        if (startingIndex < 0)
          startingIndex = this.items.Count;
        ITouchListItem index2 = this.longListBehavior.AcquireItem(index1);
        this.Add(index2, false);
        TouchList.ItemInfo itemInfo = this.itemInfos[index2];
        itemInfo.LongListIndex = index1;
        ++num1;
        if ((double) itemInfo.Min[this.layoutDimension1] > (double) num2)
          ++num3;
      }
    }
    if (startingIndex >= 0)
    {
      this.layoutSuspended = false;
      this.RepositionItems(startingIndex, new Vector3?());
    }
    this.allowModification = allowModification;
    if (layoutSuspended == this.layoutSuspended)
      return;
    this.layoutSuspended = layoutSuspended;
  }

  private void LoadBehind()
  {
    bool allowModification = this.allowModification;
    this.allowModification = true;
    int num1 = 0;
    int itemsAheadOfView = this.GetNumItemsAheadOfView();
    for (int index = 0; index < itemsAheadOfView - this.longListBehavior.MinBuffer; ++index)
    {
      ITouchListItem touchListItem = this.items[this.items.Count - 1];
      this.RemoveAt(this.items.Count - 1, false);
      this.longListBehavior.ReleaseItem(touchListItem);
    }
    float num2 = this.CalculateLocalClipBounds().min[this.layoutDimension1];
    int num3 = 0;
    for (int index1 = this.items.Count != 0 ? this.itemInfos[this.items.First<ITouchListItem>()].LongListIndex - 1 : this.longListBehavior.AllItemsCount - 1; index1 >= 0 && this.items.Count < this.longListBehavior.MaxAcquiredItems && num3 < this.longListBehavior.MinBuffer; --index1)
    {
      if (this.longListBehavior.IsItemShowable(index1))
      {
        ITouchListItem index2 = this.longListBehavior.AcquireItem(index1);
        this.InsertAndPositionBehind(index2, index1);
        TouchList.ItemInfo itemInfo = this.itemInfos[index2];
        itemInfo.LongListIndex = index1;
        ++num1;
        if ((double) itemInfo.Max[this.layoutDimension1] < (double) num2)
          ++num3;
      }
    }
    this.allowModification = allowModification;
  }

  private int GetNumItemsBehindView()
  {
    float num = this.CalculateLocalClipBounds().min[this.layoutDimension1];
    for (int index = 0; index < this.items.Count; ++index)
    {
      if ((double) this.itemInfos[this.items[index]].Max[this.layoutDimension1] > (double) num)
        return index;
    }
    return this.items.Count;
  }

  private int GetNumItemsAheadOfView()
  {
    float num = this.CalculateLocalClipBounds().max[this.layoutDimension1];
    for (int index = this.items.Count - 1; index >= 0; --index)
    {
      if ((double) this.itemInfos[this.items[index]].Min[this.layoutDimension1] < (double) num)
        return this.items.Count - 1 - index;
    }
    return this.items.Count;
  }

  public void RefreshList(int startingLongListIndex, bool preserveScrolling)
  {
    if (this.longListBehavior == null)
      return;
    bool allowModification = this.allowModification;
    this.allowModification = true;
    int num1 = this.SelectedItem != null ? this.itemInfos[this.SelectedItem].LongListIndex : -1;
    int index1 = -2;
    int startingIndex = -1;
    if (startingLongListIndex > 0)
    {
      for (int index2 = 0; index2 < this.items.Count; ++index2)
      {
        if (this.itemInfos[this.items[index2]].LongListIndex < startingLongListIndex)
        {
          index1 = index2;
        }
        else
        {
          startingIndex = index2;
          break;
        }
      }
    }
    else
      startingIndex = 0;
    int num2 = startingIndex != -1 ? startingIndex : index1 + 1;
    Bounds bounds = this.GetComponent<Collider>().bounds;
    Vector3? initialItemPosition = new Vector3?();
    Vector3 point1 = Vector3.zero;
    int num3 = this.orientation != TouchList.Orientation.Vertical ? 1 : -1;
    if (preserveScrolling)
    {
      point1 = this.content.transform.position;
      // ISSUE: variable of a reference type
      Vector3& local;
      int layoutDimension1;
      // ISSUE: explicit reference operation
      // ISSUE: explicit reference operation
      double num4 = (double) (^(local = @point1))[layoutDimension1 = this.layoutDimension1] - (double) num3 * (double) bounds.extents[this.layoutDimension1];
      // ISSUE: explicit reference operation
      (^local)[layoutDimension1] = (float) num4;
      point1[this.layoutDimension1] += (float) num3 * this.padding[this.GetVector2Dimension(this.layoutDimension1)];
      point1[this.layoutDimension2] = bounds.center[this.layoutDimension2];
      point1[this.layoutDimension3] = bounds.center[this.layoutDimension3];
      Vector3 localPosition = this.content.transform.localPosition;
      this.content.transform.localPosition = Vector3.zero;
      Bounds localClipBounds = this.CalculateLocalClipBounds();
      Vector3 min = localClipBounds.min;
      min[this.layoutDimension1] = -localPosition[this.layoutDimension1] + localClipBounds.min[this.layoutDimension1];
      this.content.transform.localPosition = localPosition;
      initialItemPosition = new Vector3?(min);
      if (index1 >= 0)
      {
        ITouchListItem index2 = this.items[index1];
        TouchList.ItemInfo itemInfo1 = this.itemInfos[index2];
        point1 = index2.transform.position - itemInfo1.Offset;
        point1[this.layoutDimension1] += (float) num3 * this.elementSpacing;
        ITouchListItem index3 = this.items[0];
        TouchList.ItemInfo itemInfo2 = this.itemInfos[index3];
        initialItemPosition = new Vector3?(index3.transform.localPosition - itemInfo2.Offset);
      }
    }
    int num5 = 0;
    if (num2 >= 0)
    {
      for (int index2 = this.items.Count - 1; index2 >= num2; --index2)
      {
        ++num5;
        ITouchListItem touchListItem = this.items[index2];
        this.RemoveAt(index2, false);
        this.longListBehavior.ReleaseItem(touchListItem);
      }
    }
    if (startingIndex < 0)
    {
      startingIndex = index1 + 1;
      if (startingIndex < 0)
        startingIndex = 0;
    }
    int num6 = 0;
    for (int index2 = startingLongListIndex; index2 < this.longListBehavior.AllItemsCount && this.items.Count < this.longListBehavior.MaxAcquiredItems; ++index2)
    {
      if (this.longListBehavior.IsItemShowable(index2))
      {
        bool flag = true;
        if (preserveScrolling)
        {
          flag = false;
          Vector3 itemSize = this.longListBehavior.GetItemSize(index2);
          Vector3 point2 = point1;
          point2[this.layoutDimension1] += (float) num3 * itemSize[this.layoutDimension1];
          if (bounds.Contains(point1) || bounds.Contains(point2))
            flag = true;
          point1 = point2;
          point1[this.layoutDimension1] += (float) num3 * this.elementSpacing;
        }
        if (flag)
        {
          ++num6;
          ITouchListItem index3 = this.longListBehavior.AcquireItem(index2);
          this.Add(index3, false);
          this.itemInfos[index3].LongListIndex = index2;
        }
      }
    }
    this.RepositionItems(startingIndex, initialItemPosition);
    if (startingIndex == 0)
      this.LoadBehind();
    if (num2 >= 0)
      this.LoadAhead();
    bool flag1 = false;
    float outOfBoundsDist = this.GetOutOfBoundsDist(this.content.transform.localPosition[this.layoutDimension1]);
    if ((double) outOfBoundsDist != 0.0 && (double) this.excessContentSize > 0.0)
    {
      Vector3 localPosition = this.content.transform.localPosition;
      localPosition[this.layoutDimension1] -= outOfBoundsDist;
      float num4 = localPosition[this.layoutDimension1] - this.content.transform.localPosition[this.layoutDimension1];
      this.content.transform.localPosition = localPosition;
      this.lastContentPosition = this.content.transform.localPosition[this.layoutDimension1];
      if ((double) num4 < 0.0)
        this.LoadAhead();
      else
        this.LoadBehind();
      flag1 = true;
    }
    if (num1 >= 0 && this.items.Count > 0 && (num1 >= this.itemInfos[this.items.First<ITouchListItem>()].LongListIndex && num1 <= this.itemInfos[this.items.Last<ITouchListItem>()].LongListIndex))
    {
      for (int index2 = 0; index2 < this.items.Count; ++index2)
      {
        ISelectableTouchListItem selectableTouchListItem = this.items[index2] as ISelectableTouchListItem;
        if (selectableTouchListItem != null && this.itemInfos[(ITouchListItem) selectableTouchListItem].LongListIndex == num1)
        {
          this.selection = new int?(index2);
          selectableTouchListItem.Selected();
          break;
        }
      }
    }
    bool flag2 = this.RecalculateLongListContentSize(false) || flag1;
    this.allowModification = allowModification;
    if (!flag2)
      return;
    this.OnScrolled();
    this.OnScrollingEnabledChanged();
  }

  private void OnScrollingEnabledChanged()
  {
    if (this.ScrollingEnabledChanged == null)
      return;
    if (this.longListBehavior == null)
      this.ScrollingEnabledChanged((double) this.excessContentSize > 0.0);
    else
      this.ScrollingEnabledChanged((double) this.m_fullListContentSize > (double) this.ClipSize[this.GetVector2Dimension(this.layoutDimension1)]);
  }

  private void RepositionItems(int startingIndex, Vector3? initialItemPosition = null)
  {
    if (this.layoutSuspended)
      return;
    if (this.orientation == TouchList.Orientation.Vertical)
      this.transform.localScale = Vector3.one;
    Vector3 localPosition = this.content.transform.localPosition;
    this.content.transform.localPosition = Vector3.zero;
    Vector3 vector3_1 = this.CalculateLocalClipBounds().min;
    if (initialItemPosition.HasValue)
      vector3_1 = initialItemPosition.Value;
    vector3_1[this.layoutDimension1] += this.padding[this.GetVector2Dimension(this.layoutDimension1)];
    vector3_1[this.layoutDimension3] = 0.0f;
    this.content.transform.localPosition = localPosition;
    this.ValidateBreadth();
    startingIndex -= startingIndex % this.breadth;
    if (startingIndex > 0)
    {
      int num = startingIndex - this.breadth;
      float b = float.MinValue;
      for (int index = num; index < startingIndex && index < this.items.Count; ++index)
        b = Mathf.Max(this.itemInfos[this.items[index]].Max[this.layoutDimension1], b);
      vector3_1[this.layoutDimension1] = b + this.elementSpacing;
    }
    Vector3 zero = Vector3.zero;
    zero[this.layoutDimension1] = 1f;
    for (int itemIndex = startingIndex; itemIndex < this.items.Count; ++itemIndex)
    {
      ITouchListItem touchListItem = this.items[itemIndex];
      if (!touchListItem.IsHeader && !touchListItem.Visible)
      {
        this.items[itemIndex].Visible = false;
        this.items[itemIndex].gameObject.SetActive(false);
      }
      else
      {
        TouchList.ItemInfo itemInfo = this.itemInfos[this.items[itemIndex]];
        Vector3 vector3_2 = vector3_1 + itemInfo.Offset;
        vector3_2[this.layoutDimension2] = this.GetBreadthPosition(itemIndex) + itemInfo.Offset[this.layoutDimension2];
        this.items[itemIndex].transform.localPosition = vector3_2;
        if ((itemIndex + 1) % this.breadth == 0)
          vector3_1 = (itemInfo.Max[this.layoutDimension1] + this.elementSpacing) * zero;
      }
    }
    this.RecalculateSize();
    this.UpdateBackgroundScroll();
    if (this.orientation == TouchList.Orientation.Vertical)
      this.transform.localScale = this.GetNegatedScale(Vector3.one);
    this.SetVisibilityOfAllItems();
  }

  private void InsertAndPositionBehind(ITouchListItem item, int longListIndex)
  {
    if (this.items.Count == 0)
    {
      this.Add(item, true);
    }
    else
    {
      ITouchListItem index = this.items.FirstOrDefault<ITouchListItem>();
      if (index == null)
      {
        this.Insert(0, item, true);
      }
      else
      {
        if (this.orientation == TouchList.Orientation.Vertical)
          this.transform.localScale = Vector3.one;
        TouchList.ItemInfo itemInfo1 = this.itemInfos[index];
        Vector3 vector3_1 = index.transform.localPosition - itemInfo1.Offset;
        this.Insert(0, item, false);
        this.itemInfos[item].LongListIndex = longListIndex;
        TouchList.ItemInfo itemInfo2 = this.itemInfos[item];
        Vector3 vector3_2 = vector3_1;
        float num = itemInfo2.Size[this.layoutDimension1] + this.elementSpacing;
        vector3_2[this.layoutDimension1] = vector3_2[this.layoutDimension1] - num;
        Vector3 vector3_3 = vector3_2 + itemInfo2.Offset;
        item.transform.localPosition = vector3_3;
        int? selection = this.selection;
        if ((selection.GetValueOrDefault() != -1 ? 0 : (selection.HasValue ? 1 : 0)) != 0 && item is ISelectableTouchListItem && ((ISelectableTouchListItem) item).IsSelected())
          this.selection = new int?(0);
        this.RecalculateSize();
        this.UpdateBackgroundScroll();
        if (this.orientation == TouchList.Orientation.Vertical)
          this.transform.localScale = this.GetNegatedScale(Vector3.one);
        bool flag = this.IsItemVisible(0);
        item.gameObject.SetActive(flag);
      }
    }
  }

  private void RecalculateSize()
  {
    float num1 = Math.Abs((this.GetComponent<Collider>() as BoxCollider).size[this.layoutDimension1]);
    float num2 = (float) (-(double) num1 / 2.0);
    float val2 = num2;
    if (this.items.Any<ITouchListItem>())
    {
      this.ValidateBreadth();
      int num3 = this.items.Count - 1;
      int num4 = num3 - num3 % this.breadth;
      int num5 = Math.Min(num4 + this.breadth, this.items.Count);
      for (int index = num4; index < num5; ++index)
        val2 = Math.Max(this.itemInfos[this.items[index]].Max[this.layoutDimension1], val2);
      this.contentSize = val2 - num2 + this.padding[this.GetVector2Dimension(this.layoutDimension1)];
      this.excessContentSize = Math.Max(this.contentSize - num1, 0.0f);
    }
    else
    {
      this.contentSize = 0.0f;
      this.excessContentSize = 0.0f;
    }
    this.OnScrollingEnabledChanged();
  }

  public bool RecalculateLongListContentSize(bool fireOnScroll = true)
  {
    if (this.longListBehavior == null)
      return false;
    float fullListContentSize = this.m_fullListContentSize;
    this.m_fullListContentSize = 0.0f;
    bool flag1 = true;
    for (int allItemsIndex = 0; allItemsIndex < this.longListBehavior.AllItemsCount; ++allItemsIndex)
    {
      if (this.longListBehavior.IsItemShowable(allItemsIndex))
      {
        this.m_fullListContentSize += this.longListBehavior.GetItemSize(allItemsIndex)[this.layoutDimension1];
        if (flag1)
          flag1 = false;
        else
          this.m_fullListContentSize += this.elementSpacing;
      }
    }
    if ((double) this.m_fullListContentSize > 0.0)
      this.m_fullListContentSize += 2f * this.padding[this.GetVector2Dimension(this.layoutDimension1)];
    bool flag2 = (double) fullListContentSize != (double) this.m_fullListContentSize;
    if (flag2 && fireOnScroll)
    {
      this.OnScrolled();
      this.OnScrollingEnabledChanged();
    }
    return flag2;
  }

  private void UpdateBackgroundBounds()
  {
    if ((UnityEngine.Object) this.background == (UnityEngine.Object) null)
      return;
    Vector3 size = (this.GetComponent<Collider>() as BoxCollider).size;
    size[this.layoutDimension1] = Math.Abs(size[this.layoutDimension1]);
    size[this.layoutDimension3] = 0.0f;
    Camera firstByLayer = CameraUtils.FindFirstByLayer((GameLayer) this.gameObject.layer);
    if ((UnityEngine.Object) firstByLayer == (UnityEngine.Object) null)
      return;
    Vector3 position = (double) Vector3.Distance(firstByLayer.transform.position, this.GetComponent<Collider>().bounds.min) <= (double) Vector3.Distance(firstByLayer.transform.position, this.GetComponent<Collider>().bounds.max) ? this.GetComponent<Collider>().bounds.max : this.GetComponent<Collider>().bounds.min;
    Vector3 zero = Vector3.zero;
    zero[this.layoutDimension3] = this.content.transform.InverseTransformPoint(position)[this.layoutDimension3];
    this.background.SetBounds(new Bounds(zero, size));
    this.UpdateBackgroundScroll();
  }

  private void UpdateBackgroundScroll()
  {
    if ((UnityEngine.Object) this.background == (UnityEngine.Object) null)
      return;
    float num1 = Math.Abs((this.GetComponent<Collider>() as BoxCollider).size[this.layoutDimension1]);
    float num2 = this.content.transform.localPosition[this.layoutDimension1];
    if (this.orientation == TouchList.Orientation.Vertical)
      num2 *= -1f;
    Vector2 offset = this.background.Offset;
    offset[this.GetVector2Dimension(this.layoutDimension1)] = num2 / num1;
    this.background.Offset = offset;
  }

  private float GetBreadthPosition(int itemIndex)
  {
    float num1 = this.padding[this.GetVector2Dimension(this.layoutDimension2)];
    float num2 = 0.0f;
    int num3 = itemIndex - itemIndex % this.breadth;
    int num4 = Math.Min(num3 + this.breadth, this.items.Count);
    for (int index = num3; index < num4; ++index)
    {
      if (index == itemIndex)
        num2 = num1;
      num1 += this.itemInfos[this.items[index]].Size[this.layoutDimension2];
    }
    float num5 = num1 + this.padding[this.GetVector2Dimension(this.layoutDimension2)];
    float num6 = 0.0f;
    float num7 = (this.GetComponent<Collider>() as BoxCollider).size[this.layoutDimension2];
    TouchList.Alignment alignment = this.alignment;
    if (this.orientation == TouchList.Orientation.Horizontal && this.alignment != TouchList.Alignment.Mid)
      alignment = this.alignment ^ TouchList.Alignment.Max;
    switch (alignment)
    {
      case TouchList.Alignment.Min:
        num6 = (float) (-(double) num7 / 2.0);
        break;
      case TouchList.Alignment.Mid:
        num6 = (float) (-(double) num5 / 2.0);
        break;
      case TouchList.Alignment.Max:
        num6 = num7 / 2f - num5;
        break;
    }
    return num6 + num2;
  }

  private Vector3 GetNegatedScale(Vector3 scale)
  {
    // ISSUE: variable of a reference type
    Vector3& local;
    int index;
    // ISSUE: explicit reference operation
    // ISSUE: explicit reference operation
    double num = (double) (^(local = @scale))[index = this.layoutPlane != TouchList.LayoutPlane.XY ? 2 : 1] * -1.0;
    // ISSUE: explicit reference operation
    (^local)[index] = (float) num;
    return scale;
  }

  private int GetVector2Dimension(int vec3Dimension)
  {
    if (vec3Dimension == 0)
      return vec3Dimension;
    return 1;
  }

  private int GetVector3Dimension(int vec2Dimension)
  {
    if (vec2Dimension == 0 || this.layoutPlane == TouchList.LayoutPlane.XY)
      return vec2Dimension;
    return 2;
  }

  private void ValidateBreadth()
  {
    if (this.longListBehavior != null)
      this.breadth = 1;
    else
      this.breadth = Math.Max(this.breadth, 1);
  }

  private Bounds CalculateLocalClipBounds()
  {
    Vector3 vector3_1 = this.content.transform.InverseTransformPoint(this.GetComponent<Collider>().bounds.min);
    Vector3 vector3_2 = this.content.transform.InverseTransformPoint(this.GetComponent<Collider>().bounds.max);
    return new Bounds((vector3_2 + vector3_1) / 2f, VectorUtils.Abs(vector3_2 - vector3_1));
  }

  public enum Orientation
  {
    Horizontal,
    Vertical,
  }

  public enum Alignment
  {
    Min,
    Mid,
    Max,
  }

  public enum LayoutPlane
  {
    XY,
    XZ,
  }

  public interface ILongListBehavior
  {
    int AllItemsCount { get; }

    int MaxVisibleItems { get; }

    int MinBuffer { get; }

    int MaxAcquiredItems { get; }

    void ReleaseAllItems();

    void ReleaseItem(ITouchListItem item);

    ITouchListItem AcquireItem(int index);

    bool IsItemShowable(int allItemsIndex);

    Vector3 GetItemSize(int allItemsIndex);
  }

  private class ItemInfo
  {
    private readonly ITouchListItem item;

    public Vector3 Size { get; private set; }

    public Vector3 Offset { get; private set; }

    public int LongListIndex { get; set; }

    public Vector3 Min
    {
      get
      {
        return this.item.transform.localPosition + Vector3.Scale(this.item.LocalBounds.min, VectorUtils.Abs(this.item.transform.localScale));
      }
    }

    public Vector3 Max
    {
      get
      {
        return this.item.transform.localPosition + Vector3.Scale(this.item.LocalBounds.max, VectorUtils.Abs(this.item.transform.localScale));
      }
    }

    public ItemInfo(ITouchListItem item, TouchList.LayoutPlane layoutPlane)
    {
      this.item = item;
      Vector3 vector3_1 = Vector3.Scale(item.LocalBounds.min, VectorUtils.Abs(item.transform.localScale)) - item.transform.localPosition;
      Vector3 vector3_2 = Vector3.Scale(item.LocalBounds.max, VectorUtils.Abs(item.transform.localScale)) - item.transform.localPosition;
      this.Size = vector3_2 - vector3_1;
      Vector3 vector3_3 = vector3_1;
      if (layoutPlane == TouchList.LayoutPlane.XZ)
        vector3_3.y = vector3_2.y;
      this.Offset = item.transform.localPosition - vector3_3;
    }

    public bool Contains(Vector2 point, TouchList.LayoutPlane layoutPlane)
    {
      Vector3 min = this.Min;
      Vector3 max = this.Max;
      int index = layoutPlane != TouchList.LayoutPlane.XY ? 2 : 1;
      if ((double) point.x > (double) min.x && (double) point.y > (double) min[index] && (double) point.x < (double) max.x)
        return (double) point.y < (double) max[index];
      return false;
    }
  }

  public delegate bool SelectedIndexChangingEvent(int index);

  public delegate void ScrollingEnabledChangedEvent(bool canScroll);

  public delegate bool ItemDragEvent(ITouchListItem item, float dragAmount);
}
