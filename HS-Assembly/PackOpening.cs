﻿// Decompiled with JetBrains decompiler
// Type: PackOpening
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PackOpening : MonoBehaviour
{
  public bool m_OnePackCentered = true;
  private bool m_waitingForInitialNetData = true;
  private Map<int, UnopenedPack> m_unopenedPacks = new Map<int, UnopenedPack>();
  private Map<int, bool> m_unopenedPacksLoading = new Map<int, bool>();
  private const int MAX_OPENED_PACKS_BEFORE_CARD_CACHE_RESET = 10;
  public PackOpeningBones m_Bones;
  public PackOpeningDirector m_DirectorPrefab;
  public PackOpeningSocket m_Socket;
  public PackOpeningSocket m_SocketAccent;
  public UberText m_HeaderText;
  public UIBButton m_BackButton;
  public StoreButton m_StoreButton;
  public GameObject m_DragPlane;
  public GameObject m_InputBlocker;
  public UIBObjectSpacing m_UnopenedPackContainer;
  public UIBScrollable m_UnopenedPackScroller;
  public CameraMask m_PackTrayCameraMask;
  public Vector3 m_StoreButtonOffset;
  public float m_UnopenedPackPadding;
  private static PackOpening s_instance;
  private bool m_shown;
  private bool m_loadingUnopenedPack;
  private PackOpeningDirector m_director;
  private UnopenedPack m_draggedPack;
  private Notification m_hintArrow;
  private GameObject m_PackOpeningCardFX;
  private int m_openedPacksUntilCardCacheReset;
  private bool m_autoOpenPending;
  private int m_lastOpenedBoosterId;
  private bool m_enableBackButton;
  private bool m_openBoxTransitionFinished;
  private static bool m_hasAcknowledgedKoreanWarning;

  private void Awake()
  {
    PackOpening.s_instance = this;
    if ((bool) UniversalInputManager.UsePhoneUI)
      AssetLoader.Get().LoadGameObject("PackOpeningCardFX_Phone", new AssetLoader.GameObjectCallback(this.OnPackOpeningFXLoaded), (object) null, false);
    else
      AssetLoader.Get().LoadGameObject("PackOpeningCardFX", new AssetLoader.GameObjectCallback(this.OnPackOpeningFXLoaded), (object) null, false);
    this.InitializeNet();
    this.InitializeUI();
    Box.Get().AddTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    this.m_StoreButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnStoreButtonReleased));
  }

  private void Start()
  {
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  private void Update()
  {
    this.UpdateDraggedPack();
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) this.m_draggedPack != (UnityEngine.Object) null && (UnityEngine.Object) PegCursor.Get() != (UnityEngine.Object) null)
      PegCursor.Get().SetMode(PegCursor.Mode.STOPDRAG);
    this.ShutdownNet();
    PackOpening.s_instance = (PackOpening) null;
  }

  public static PackOpening Get()
  {
    return PackOpening.s_instance;
  }

  public GameObject GetPackOpeningCardEffects()
  {
    return this.m_PackOpeningCardFX;
  }

  public bool HandleKeyboardInput()
  {
    if (!Input.GetKeyUp(KeyCode.Space) || (UnityEngine.Object) this.m_director == (UnityEngine.Object) null)
      return false;
    if (this.CanOpenPackAutomatically())
    {
      this.m_autoOpenPending = true;
      this.m_director.FinishPackOpen();
      this.StartCoroutine(this.OpenNextPackWhenReady());
    }
    return true;
  }

  private void NotifySceneLoadedWhenReady()
  {
    if (!this.m_waitingForInitialNetData)
      return;
    this.m_waitingForInitialNetData = false;
    SceneMgr.Get().NotifySceneLoaded();
  }

  private void OnBoxTransitionFinished(object userData)
  {
    Box.Get().RemoveTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    this.Show();
    this.m_openBoxTransitionFinished = true;
  }

  private void Show()
  {
    if (this.m_shown)
      return;
    this.m_shown = true;
    PresenceMgr.Get().SetStatus((Enum) PresenceStatus.PACKOPENING);
    if (!Options.Get().GetBool(Option.HAS_SEEN_PACK_OPENING, false))
    {
      NetCache.NetCacheBoosters netObject = NetCache.Get().GetNetObject<NetCache.NetCacheBoosters>();
      if (netObject != null && netObject.GetTotalNumBoosters() > 0)
        Options.Get().SetBool(Option.HAS_SEEN_PACK_OPENING, true);
    }
    MusicManager.Get().StartPlaylist(MusicPlaylistType.UI_PackOpening);
    this.CreateDirector();
    BnetBar.Get().m_currencyFrame.RefreshContents();
    if (NetCache.Get().GetNetObject<NetCache.NetCacheBoosters>().BoosterStacks.Count < 2)
      this.ShowHintOnUnopenedPack();
    this.UpdateUIEvents();
    this.DisablePackTrayMask();
  }

  private void Hide()
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    this.DestroyHint();
    this.m_StoreButton.Unload();
    this.m_InputBlocker.SetActive(false);
    this.EnablePackTrayMask();
    this.UnregisterUIEvents();
    this.ShutdownNet();
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
  }

  private bool OnNavigateBack()
  {
    if (!this.m_enableBackButton || this.m_InputBlocker.activeSelf)
      return false;
    this.Hide();
    return true;
  }

  private void InitializeNet()
  {
    this.m_waitingForInitialNetData = true;
    NetCache.Get().RegisterScreenPackOpening(new NetCache.NetCacheCallback(this.OnNetDataReceived), new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
    Network.Get().RegisterNetHandler((object) BoosterContent.PacketID.ID, new Network.NetHandler(this.OnBoosterOpened), (Network.TimeoutHandler) null);
  }

  private void ShutdownNet()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetDataReceived));
    Network.Get().RemoveNetHandler((object) BoosterContent.PacketID.ID, new Network.NetHandler(this.OnBoosterOpened));
  }

  private void OnNetDataReceived()
  {
    this.NotifySceneLoadedWhenReady();
    this.UpdatePacks();
    this.UpdateUIEvents();
  }

  private void UpdatePacks()
  {
    NetCache.NetCacheBoosters netObject = NetCache.Get().GetNetObject<NetCache.NetCacheBoosters>();
    if (netObject == null)
    {
      UnityEngine.Debug.LogError((object) string.Format("PackOpening.UpdatePacks() - boosters are null"));
    }
    else
    {
      using (List<NetCache.BoosterStack>.Enumerator enumerator = netObject.BoosterStacks.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          NetCache.BoosterStack current = enumerator.Current;
          int id = current.Id;
          if (this.m_unopenedPacks.ContainsKey(id) && (UnityEngine.Object) this.m_unopenedPacks[id] != (UnityEngine.Object) null)
          {
            if (netObject.GetBoosterStack(id) == null)
            {
              UnityEngine.Object.Destroy((UnityEngine.Object) this.m_unopenedPacks[id]);
              this.m_unopenedPacks[id] = (UnopenedPack) null;
            }
            else
              this.UpdatePack(this.m_unopenedPacks[id], netObject.GetBoosterStack(id));
          }
          else if (netObject.GetBoosterStack(id) != null && (!this.m_unopenedPacksLoading.ContainsKey(id) || !this.m_unopenedPacksLoading[id]))
          {
            this.m_unopenedPacksLoading[id] = true;
            string name = FileUtils.GameAssetPathToName(GameDbf.Booster.GetRecord(id).PackOpeningPrefab);
            if (string.IsNullOrEmpty(name))
              UnityEngine.Debug.LogError((object) string.Format("PackOpening.UpdatePacks() - no prefab found for booster {0}!", (object) id));
            else
              AssetLoader.Get().LoadActor(name, new AssetLoader.GameObjectCallback(this.OnUnopenedPackLoaded), (object) current, false);
          }
        }
      }
      this.LayoutPacks(false);
    }
  }

  private void OnBoosterOpened()
  {
    this.m_director.OnBoosterOpened(Network.OpenedBooster());
  }

  private void CreateDirector()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_DirectorPrefab.gameObject);
    this.m_director = gameObject.GetComponent<PackOpeningDirector>();
    gameObject.transform.parent = this.transform;
    TransformUtil.CopyWorld((Component) this.m_director, (Component) this.m_Bones.m_Director);
  }

  private void PickUpBooster()
  {
    UnopenedPack creatorPack = this.m_draggedPack.GetCreatorPack();
    creatorPack.RemoveBooster();
    this.m_draggedPack.SetBoosterStack(new NetCache.BoosterStack()
    {
      Id = creatorPack.GetBoosterStack().Id,
      Count = 1
    });
  }

  private void OpenBooster(UnopenedPack pack)
  {
    int num = 1;
    if (!GameUtils.IsFakePackOpeningEnabled())
    {
      num = pack.GetBoosterStack().Id;
      Network.OpenBooster(num);
    }
    this.m_InputBlocker.SetActive(true);
    this.m_director.AddFinishedListener(new PackOpeningDirector.FinishedCallback(this.OnDirectorFinished));
    this.m_director.Play(num);
    this.m_lastOpenedBoosterId = num;
    BnetBar.Get().m_currencyFrame.HideTemporarily();
    if (GameUtils.IsFakePackOpeningEnabled())
      this.StartCoroutine(this.OnFakeBoosterOpened());
    this.m_UnopenedPackScroller.Pause(true);
  }

  [DebuggerHidden]
  private IEnumerator OnFakeBoosterOpened()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PackOpening.\u003COnFakeBoosterOpened\u003Ec__Iterator22B() { \u003C\u003Ef__this = this };
  }

  private void PutBackBooster()
  {
    UnopenedPack creatorPack = this.m_draggedPack.GetCreatorPack();
    this.m_draggedPack.RemoveBooster();
    creatorPack.AddBooster();
  }

  private void UpdatePack(UnopenedPack pack, NetCache.BoosterStack boosterStack)
  {
    pack.SetBoosterStack(boosterStack);
  }

  private void OnUnopenedPackLoaded(string name, GameObject go, object userData)
  {
    NetCache.BoosterStack boosterStack = (NetCache.BoosterStack) userData;
    int id = boosterStack.Id;
    this.m_unopenedPacksLoading[id] = false;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("PackOpening.OnUnopenedPackLoaded() - FAILED to load {0}", (object) name));
    }
    else
    {
      UnopenedPack component = go.GetComponent<UnopenedPack>();
      go.SetActive(false);
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("PackOpening.OnUnopenedPackLoaded() - asset {0} did not have a {1} script on it", (object) name, (object) typeof (UnopenedPack)));
      }
      else
      {
        this.m_unopenedPacks.Add(id, component);
        component.gameObject.SetActive(true);
        GameUtils.SetParent((Component) component, (Component) this.m_UnopenedPackContainer, false);
        component.transform.localScale = Vector3.one;
        component.AddEventListener(UIEventType.HOLD, new UIEvent.Handler(this.OnUnopenedPackHold));
        component.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnUnopenedPackRollover));
        component.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnUnopenedPackRollout));
        component.AddEventListener(UIEventType.RELEASEALL, new UIEvent.Handler(this.OnUnopenedPackReleaseAll));
        this.UpdatePack(component, boosterStack);
        AchieveManager.Get().NotifyOfPacksReadyToOpen(component);
        this.LayoutPacks(false);
        if (NetCache.Get().GetNetObject<NetCache.NetCacheBoosters>().BoosterStacks.Count < 2)
          this.ShowHintOnUnopenedPack();
        this.UpdateUIEvents();
      }
    }
  }

  private void LayoutPacks(bool animate = false)
  {
    IEnumerable<int> sortedPackIds = GameUtils.GetSortedPackIds(false);
    this.m_UnopenedPackContainer.ClearObjects();
    if (!this.m_openBoxTransitionFinished)
      this.DisablePackTrayMask();
    foreach (int key in sortedPackIds)
    {
      UnopenedPack unopenedPack;
      this.m_unopenedPacks.TryGetValue(key, out unopenedPack);
      if (!((UnityEngine.Object) unopenedPack == (UnityEngine.Object) null) && unopenedPack.GetBoosterStack().Count != 0)
      {
        unopenedPack.gameObject.SetActive(true);
        this.m_UnopenedPackContainer.AddObject((Component) unopenedPack, true);
      }
    }
    if (this.m_OnePackCentered && this.m_UnopenedPackContainer.m_Objects.Count <= 1)
      this.m_UnopenedPackContainer.AddSpace(0);
    this.m_UnopenedPackContainer.AddObject((Component) this.m_StoreButton, this.m_StoreButtonOffset, true);
    if (animate)
      this.m_UnopenedPackContainer.AnimateUpdatePositions(0.25f, iTween.EaseType.easeInOutQuad);
    else
      this.m_UnopenedPackContainer.UpdatePositions();
    if (this.m_openBoxTransitionFinished)
      return;
    this.EnablePackTrayMask();
  }

  private void CreateDraggedPack(UnopenedPack creatorPack)
  {
    this.m_draggedPack = creatorPack.AcquireDraggedPack();
    Vector3 vector3 = this.m_draggedPack.transform.position;
    RaycastHit hitInfo;
    if (UniversalInputManager.Get().GetInputHitInfo((LayerMask) GameLayer.DragPlane.LayerBit(), out hitInfo))
      vector3 = hitInfo.point;
    float f = Vector3.Dot(Camera.main.transform.forward, Vector3.up);
    float num = -f / Mathf.Abs(f);
    Bounds bounds = this.m_draggedPack.GetComponent<Collider>().bounds;
    vector3.y += num * bounds.extents.y * this.m_draggedPack.transform.lossyScale.y;
    this.m_draggedPack.transform.position = vector3;
  }

  private void DestroyDraggedPack()
  {
    this.m_draggedPack.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDraggedPackRelease));
    this.m_draggedPack.GetCreatorPack().ReleaseDraggedPack();
    this.m_draggedPack = (UnopenedPack) null;
  }

  private void UpdateDraggedPack()
  {
    if ((UnityEngine.Object) this.m_draggedPack == (UnityEngine.Object) null)
      return;
    Vector3 position = this.m_draggedPack.transform.position;
    RaycastHit hitInfo;
    if (UniversalInputManager.Get().GetInputHitInfo((LayerMask) GameLayer.DragPlane.LayerBit(), out hitInfo))
    {
      position.x = hitInfo.point.x;
      position.z = hitInfo.point.z;
      this.m_draggedPack.transform.position = position;
    }
    if (!UniversalInputManager.Get().GetMouseButtonUp(0))
      return;
    this.DropPack();
  }

  [DebuggerHidden]
  private IEnumerator HideAfterNoMorePacks()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PackOpening.\u003CHideAfterNoMorePacks\u003Ec__Iterator22C() { \u003C\u003Ef__this = this };
  }

  private void OnDirectorFinished(object userData)
  {
    this.m_UnopenedPackScroller.Pause(false);
    int num = 0;
    using (Map<int, UnopenedPack>.Enumerator enumerator = this.m_unopenedPacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, UnopenedPack> current = enumerator.Current;
        if (!((UnityEngine.Object) current.Value == (UnityEngine.Object) null))
        {
          int count = current.Value.GetBoosterStack().Count;
          num += count;
          current.Value.gameObject.SetActive(count > 0);
        }
      }
    }
    if (num == 0)
    {
      this.StartCoroutine(this.HideAfterNoMorePacks());
    }
    else
    {
      this.m_InputBlocker.SetActive(false);
      ++this.m_openedPacksUntilCardCacheReset;
      if (this.m_openedPacksUntilCardCacheReset == 10)
      {
        DefLoader.Get().ClearCardDefs();
        this.m_openedPacksUntilCardCacheReset = 0;
      }
      this.CreateDirector();
      this.LayoutPacks(true);
    }
    BnetBar.Get().m_currencyFrame.RefreshContents();
  }

  private void ShowHintOnUnopenedPack()
  {
    List<UnopenedPack> unopenedPackList = new List<UnopenedPack>();
    using (Map<int, UnopenedPack>.Enumerator enumerator = this.m_unopenedPacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, UnopenedPack> current = enumerator.Current;
        if (!((UnityEngine.Object) current.Value == (UnityEngine.Object) null) && current.Value.CanOpenPack() && current.Value.GetBoosterStack().Count > 0)
          unopenedPackList.Add(current.Value);
      }
    }
    if (unopenedPackList.Count < 1 || (UnityEngine.Object) unopenedPackList[0] == (UnityEngine.Object) null)
      return;
    unopenedPackList[0].PlayAlert();
    if (Options.Get().GetBool(Option.HAS_OPENED_BOOSTER, false) || !UserAttentionManager.CanShowAttentionGrabber("PackOpening.ShowHintOnUnopenedPack"))
      return;
    if ((UnityEngine.Object) this.m_hintArrow == (UnityEngine.Object) null)
      this.m_hintArrow = NotificationManager.Get().CreateBouncingArrow(UserAttentionBlocker.NONE, false);
    if (!((UnityEngine.Object) this.m_hintArrow != (UnityEngine.Object) null))
      return;
    this.FixArrowScale(unopenedPackList[0].transform);
    Bounds bounds1 = unopenedPackList[0].GetComponent<Collider>().bounds;
    Bounds bounds2 = this.m_hintArrow.bounceObject.GetComponent<Renderer>().bounds;
    Vector3 center = bounds1.center;
    center.z += bounds1.extents.z + bounds2.extents.z;
    this.m_hintArrow.transform.position = center;
  }

  private void ShowHintOnSlot()
  {
    if (Options.Get().GetBool(Option.HAS_OPENED_BOOSTER, false) || !UserAttentionManager.CanShowAttentionGrabber("PackOpening.ShowHintOnSlot"))
      return;
    if ((UnityEngine.Object) this.m_hintArrow == (UnityEngine.Object) null)
      this.m_hintArrow = NotificationManager.Get().CreateBouncingArrow(UserAttentionBlocker.NONE, false);
    if (!((UnityEngine.Object) this.m_hintArrow != (UnityEngine.Object) null))
      return;
    this.FixArrowScale(this.m_draggedPack.transform);
    Bounds bounds = this.m_hintArrow.bounceObject.GetComponent<Renderer>().bounds;
    Vector3 position = this.m_Bones.m_Hint.position;
    position.z += bounds.extents.z;
    this.m_hintArrow.transform.position = position;
  }

  private void FixArrowScale(Transform parent)
  {
    Transform parent1 = this.m_hintArrow.transform.parent;
    this.m_hintArrow.transform.parent = parent;
    this.m_hintArrow.transform.localScale = Vector3.one;
    this.m_hintArrow.transform.parent = parent1;
  }

  private void HideHint()
  {
    if ((UnityEngine.Object) this.m_hintArrow == (UnityEngine.Object) null)
      return;
    Options.Get().SetBool(Option.HAS_OPENED_BOOSTER, true);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_hintArrow.gameObject);
    this.m_hintArrow = (Notification) null;
  }

  private void DestroyHint()
  {
    if ((UnityEngine.Object) this.m_hintArrow == (UnityEngine.Object) null)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_hintArrow.gameObject);
    this.m_hintArrow = (Notification) null;
  }

  private void InitializeUI()
  {
    this.m_HeaderText.Text = GameStrings.Get("GLUE_PACK_OPENING_HEADER");
    this.m_BackButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBackButtonPressed));
    this.m_DragPlane.SetActive(false);
    this.m_InputBlocker.SetActive(false);
  }

  private void UpdateUIEvents()
  {
    if (!this.m_shown)
      this.UnregisterUIEvents();
    else if ((UnityEngine.Object) this.m_draggedPack != (UnityEngine.Object) null)
    {
      this.UnregisterUIEvents();
    }
    else
    {
      this.m_enableBackButton = true;
      this.m_BackButton.SetEnabled(true);
      this.m_StoreButton.SetEnabled(true);
      using (Map<int, UnopenedPack>.Enumerator enumerator = this.m_unopenedPacks.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, UnopenedPack> current = enumerator.Current;
          if ((UnityEngine.Object) current.Value != (UnityEngine.Object) null)
            current.Value.SetEnabled(true);
        }
      }
    }
  }

  private void UnregisterUIEvents()
  {
    this.m_enableBackButton = false;
    this.m_BackButton.SetEnabled(false);
    this.m_StoreButton.SetEnabled(false);
    using (Map<int, UnopenedPack>.Enumerator enumerator = this.m_unopenedPacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, UnopenedPack> current = enumerator.Current;
        if ((UnityEngine.Object) current.Value != (UnityEngine.Object) null)
          current.Value.SetEnabled(false);
      }
    }
  }

  private void OnBackButtonPressed(UIEvent e)
  {
    Navigation.GoBack();
  }

  private void HoldPack(UnopenedPack selectedPack)
  {
    if (!selectedPack.CanOpenPack())
      return;
    this.HideUnopenedPackTooltip();
    PegCursor.Get().SetMode(PegCursor.Mode.DRAG);
    this.m_DragPlane.SetActive(true);
    this.CreateDraggedPack(selectedPack);
    if ((UnityEngine.Object) this.m_draggedPack != (UnityEngine.Object) null)
    {
      TooltipPanel componentInChildren = this.m_draggedPack.GetComponentInChildren<TooltipPanel>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) componentInChildren.gameObject);
    }
    this.PickUpBooster();
    selectedPack.StopAlert();
    this.ShowHintOnSlot();
    this.m_Socket.OnPackHeld();
    this.m_SocketAccent.OnPackHeld();
    this.UpdateUIEvents();
  }

  private void DropPack()
  {
    PegCursor.Get().SetMode(PegCursor.Mode.STOPDRAG);
    this.m_Socket.OnPackReleased();
    this.m_SocketAccent.OnPackReleased();
    if (UniversalInputManager.Get().InputIsOver(this.m_Socket.gameObject))
    {
      if (BattleNet.GetAccountCountry() == "KOR")
        PackOpening.m_hasAcknowledgedKoreanWarning = true;
      this.OpenBooster(this.m_draggedPack);
      this.HideHint();
    }
    else
    {
      this.PutBackBooster();
      this.DestroyHint();
    }
    this.DestroyDraggedPack();
    this.UpdateUIEvents();
    this.m_DragPlane.SetActive(false);
  }

  private void AutomaticallyOpenPack()
  {
    if (!this.CanOpenPackAutomatically())
      return;
    this.HideUnopenedPackTooltip();
    UnopenedPack unopenedPack = (UnopenedPack) null;
    if (!this.m_unopenedPacks.TryGetValue(this.m_lastOpenedBoosterId, out unopenedPack) || unopenedPack.GetBoosterStack().Count == 0)
    {
      using (Map<int, UnopenedPack>.Enumerator enumerator = this.m_unopenedPacks.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, UnopenedPack> current = enumerator.Current;
          if (!((UnityEngine.Object) current.Value == (UnityEngine.Object) null) && current.Value.GetBoosterStack().Count > 0)
          {
            unopenedPack = current.Value;
            break;
          }
        }
      }
    }
    if ((UnityEngine.Object) unopenedPack == (UnityEngine.Object) null || !unopenedPack.CanOpenPack())
      return;
    this.m_draggedPack = unopenedPack.AcquireDraggedPack();
    this.PickUpBooster();
    unopenedPack.StopAlert();
    this.OpenBooster(this.m_draggedPack);
    this.DestroyDraggedPack();
    this.UpdateUIEvents();
    this.m_DragPlane.SetActive(false);
  }

  private void OnUnopenedPackHold(UIEvent e)
  {
    this.HoldPack(e.GetElement() as UnopenedPack);
  }

  private void OnUnopenedPackRollover(UIEvent e)
  {
    if (PackOpening.m_hasAcknowledgedKoreanWarning || BattleNet.GetAccountCountry() != "KOR")
      return;
    TooltipZone component = (e.GetElement() as UnopenedPack).GetComponent<TooltipZone>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    component.ShowTooltip(" ", GameStrings.Get("GLUE_PACK_OPENING_TOOLTIP"), 5f, true);
  }

  private void OnUnopenedPackRollout(UIEvent e)
  {
    this.HideUnopenedPackTooltip();
  }

  private void OnUnopenedPackReleaseAll(UIEvent e)
  {
    if ((UnityEngine.Object) this.m_draggedPack == (UnityEngine.Object) null)
    {
      if (UniversalInputManager.Get().IsTouchMode() || !((UIReleaseAllEvent) e).GetMouseIsOver())
        return;
      this.HoldPack(e.GetElement() as UnopenedPack);
    }
    else
      this.DropPack();
  }

  private void OnDraggedPackRelease(UIEvent e)
  {
    this.DropPack();
  }

  private void HideUnopenedPackTooltip()
  {
    using (Map<int, UnopenedPack>.Enumerator enumerator = this.m_unopenedPacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, UnopenedPack> current = enumerator.Current;
        if (!((UnityEngine.Object) current.Value == (UnityEngine.Object) null))
          current.Value.GetComponent<TooltipZone>().HideTooltip();
      }
    }
  }

  private bool CanOpenPackAutomatically()
  {
    return !this.m_autoOpenPending && this.m_shown && GameUtils.HaveBoosters() && ((!this.m_director.IsPlaying() || this.m_director.IsDoneButtonShown()) && (!this.m_DragPlane.activeSelf && !StoreManager.Get().IsShownOrWaitingToShow()));
  }

  [DebuggerHidden]
  private IEnumerator OpenNextPackWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PackOpening.\u003COpenNextPackWhenReady\u003Ec__Iterator22D() { \u003C\u003Ef__this = this };
  }

  private void OnPackOpeningFXLoaded(string name, GameObject gameObject, object callbackData)
  {
    this.m_PackOpeningCardFX = gameObject;
  }

  private void OnStoreButtonReleased(UIEvent e)
  {
    if (this.m_StoreButton.IsVisualClosed())
      return;
    StoreManager.Get().StartGeneralTransaction();
  }

  private void EnablePackTrayMask()
  {
    if ((UnityEngine.Object) this.m_PackTrayCameraMask == (UnityEngine.Object) null)
      return;
    this.m_PackTrayCameraMask.enabled = true;
  }

  private void DisablePackTrayMask()
  {
    if ((UnityEngine.Object) this.m_PackTrayCameraMask == (UnityEngine.Object) null)
      return;
    foreach (Component componentsInChild in this.m_PackTrayCameraMask.m_ClipObjects.GetComponentsInChildren<Transform>())
    {
      GameObject gameObject = componentsInChild.gameObject;
      if (!((UnityEngine.Object) gameObject == (UnityEngine.Object) null))
        SceneUtils.SetLayer(gameObject, GameLayer.Default);
    }
    this.m_PackTrayCameraMask.enabled = false;
  }
}
