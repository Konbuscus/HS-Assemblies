﻿// Decompiled with JetBrains decompiler
// Type: CameraFade
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CameraFade : MonoBehaviour
{
  public Color m_Color = Color.black;
  public float m_Fade = 1f;
  private float m_CameraDepth = 14f;
  public bool m_RenderOverAll;
  private Texture2D m_TempTexture;
  private GameObject m_PlaneGameObject;
  private Material m_Material;
  private Camera m_Camera;

  private void Awake()
  {
    this.m_TempTexture = new Texture2D(1, 1);
    this.m_TempTexture.SetPixel(0, 0, Color.white);
    this.m_TempTexture.Apply();
    this.m_Camera = this.GetComponent<Camera>();
    if ((Object) this.m_Camera == (Object) null)
    {
      Debug.LogError((object) "CameraFade faild to find camera component!");
      this.enabled = false;
    }
    this.m_CameraDepth = this.m_Camera.depth;
    this.SetupCamera();
  }

  private void Update()
  {
    if ((double) this.m_Fade <= 0.0)
    {
      if ((Object) this.GetComponent<Renderer>() != (Object) null && this.GetComponent<Renderer>().enabled)
        this.GetComponent<Renderer>().enabled = false;
      if (!this.m_Camera.enabled)
        return;
      this.m_Camera.enabled = false;
    }
    else
    {
      if ((Object) this.GetComponent<Renderer>() == (Object) null)
        this.CreateRenderPlane();
      if (!this.GetComponent<Renderer>().enabled)
        this.GetComponent<Renderer>().enabled = true;
      if (!this.m_Camera.enabled)
        this.m_Camera.enabled = true;
      if (this.m_RenderOverAll)
      {
        if ((double) this.m_Camera.depth < 100.0)
          this.m_Camera.depth = 100f;
      }
      else if ((double) this.m_Camera.depth != (double) this.m_CameraDepth)
        this.m_Camera.depth = this.m_CameraDepth;
      this.m_Material.color = new Color(this.m_Color.r, this.m_Color.g, this.m_Color.b, this.m_Fade);
    }
  }

  private void CreateRenderPlane()
  {
    this.gameObject.AddComponent<MeshFilter>();
    this.gameObject.AddComponent<MeshRenderer>();
    this.GetComponent<Renderer>().GetComponent<MeshFilter>().mesh = new Mesh()
    {
      vertices = new Vector3[4]
      {
        new Vector3(-10f, -10f, 0.0f),
        new Vector3(10f, -10f, 0.0f),
        new Vector3(-10f, 10f, 0.0f),
        new Vector3(10f, 10f, 0.0f)
      },
      colors = new Color[4]
      {
        Color.white,
        Color.white,
        Color.white,
        Color.white
      },
      uv = new Vector2[4]
      {
        new Vector2(0.0f, 0.0f),
        new Vector2(1f, 0.0f),
        new Vector2(0.0f, 1f),
        new Vector2(1f, 1f)
      },
      normals = new Vector3[4]
      {
        Vector3.up,
        Vector3.up,
        Vector3.up,
        Vector3.up
      },
      triangles = new int[6]{ 3, 1, 2, 2, 1, 0 }
    };
    this.m_Material = new Material(ShaderUtils.FindShader("Hidden/CameraFade"));
    this.GetComponent<Renderer>().sharedMaterial = this.m_Material;
  }

  private void SetupCamera()
  {
    this.m_Camera.farClipPlane = 1f;
    this.m_Camera.nearClipPlane = -1f;
    this.m_Camera.clearFlags = CameraClearFlags.Nothing;
    this.m_Camera.orthographicSize = 0.5f;
  }
}
