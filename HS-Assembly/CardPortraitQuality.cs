﻿// Decompiled with JetBrains decompiler
// Type: CardPortraitQuality
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardPortraitQuality
{
  public const int NOT_LOADED = 0;
  public const int LOW = 1;
  public const int MEDIUM = 2;
  public const int HIGH = 3;
  public int TextureQuality;
  public bool LoadPremium;

  public CardPortraitQuality(int quality, bool loadPremium)
  {
    this.TextureQuality = quality;
    this.LoadPremium = loadPremium;
  }

  public CardPortraitQuality(int quality, TAG_PREMIUM premiumType)
  {
    this.TextureQuality = quality;
    this.LoadPremium = premiumType == TAG_PREMIUM.GOLDEN;
  }

  public static bool operator >=(CardPortraitQuality left, CardPortraitQuality right)
  {
    if (left == null)
      return false;
    if (right == null)
      return true;
    if (left.TextureQuality < right.TextureQuality)
      return false;
    if (!left.LoadPremium)
      return !right.LoadPremium;
    return true;
  }

  public static bool operator <=(CardPortraitQuality left, CardPortraitQuality right)
  {
    if (left == null)
      return true;
    if (right == null || left.TextureQuality > right.TextureQuality)
      return false;
    if (left.LoadPremium)
      return right.LoadPremium;
    return true;
  }

  public static CardPortraitQuality GetUnloaded()
  {
    return new CardPortraitQuality(0, false);
  }

  public static CardPortraitQuality GetDefault()
  {
    return new CardPortraitQuality(3, true);
  }

  public static CardPortraitQuality GetFromDef(CardDef def)
  {
    if ((Object) def == (Object) null)
      return CardPortraitQuality.GetDefault();
    return def.GetPortraitQuality();
  }

  public override string ToString()
  {
    return "(" + (object) this.TextureQuality + ", " + (object) this.LoadPremium + ")";
  }
}
