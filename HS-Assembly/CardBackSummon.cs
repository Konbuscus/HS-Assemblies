﻿// Decompiled with JetBrains decompiler
// Type: CardBackSummon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardBackSummon : MonoBehaviour
{
  private CardBackManager m_CardBackManager;
  private Actor m_Actor;

  private void Start()
  {
    this.UpdateEffect();
  }

  public void UpdateEffect()
  {
    this.UpdateEchoTexture();
  }

  private void UpdateEchoTexture()
  {
    if ((Object) this.m_CardBackManager == (Object) null)
    {
      this.m_CardBackManager = CardBackManager.Get();
      if ((Object) this.m_CardBackManager == (Object) null)
      {
        Debug.LogError((object) "CardBackSummonIn failed to get CardBackManager!");
        this.enabled = false;
      }
    }
    if ((Object) this.m_Actor == (Object) null)
    {
      this.m_Actor = SceneUtils.FindComponentInParents<Actor>(this.gameObject);
      if ((Object) this.m_Actor == (Object) null)
        Debug.LogError((object) "CardBackSummonIn failed to get Actor!");
    }
    Texture texture = this.GetComponent<Renderer>().material.mainTexture;
    if (this.m_CardBackManager.IsActorFriendly(this.m_Actor))
    {
      CardBack friendlyCardBack = this.m_CardBackManager.GetFriendlyCardBack();
      if ((Object) friendlyCardBack != (Object) null)
        texture = (Texture) friendlyCardBack.m_HiddenCardEchoTexture;
    }
    else
    {
      CardBack opponentCardBack = this.m_CardBackManager.GetOpponentCardBack();
      if ((Object) opponentCardBack != (Object) null)
        texture = (Texture) opponentCardBack.m_HiddenCardEchoTexture;
    }
    if (!((Object) texture != (Object) null))
      return;
    this.GetComponent<Renderer>().material.mainTexture = texture;
  }
}
