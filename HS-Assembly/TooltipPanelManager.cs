﻿// Decompiled with JetBrains decompiler
// Type: TooltipPanelManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TooltipPanelManager : MonoBehaviour
{
  private Pool<TooltipPanel> m_tooltipPanelPool = new Pool<TooltipPanel>();
  private List<TooltipPanel> m_tooltipPanels = new List<TooltipPanel>();
  private float scaleToUse = (float) TooltipPanel.GAMEPLAY_SCALE;
  private const float FADE_IN_TIME = 0.125f;
  private const float DELAY_BEFORE_FADE_IN = 0.4f;
  public TooltipPanel m_tooltipPanelPrefab;
  private static TooltipPanelManager s_instance;
  private Actor m_actor;
  private Card m_card;

  private void Awake()
  {
    TooltipPanelManager.s_instance = this;
    this.m_tooltipPanelPool.SetCreateItemCallback(new Pool<TooltipPanel>.CreateItemCallback(this.CreateKeywordPanel));
    this.m_tooltipPanelPool.SetDestroyItemCallback(new Pool<TooltipPanel>.DestroyItemCallback(this.DestroyKeywordPanel));
    this.m_tooltipPanelPool.SetExtensionCount(1);
    if (!((Object) SceneMgr.Get() != (Object) null))
      return;
    SceneMgr.Get().RegisterSceneUnloadedEvent(new SceneMgr.SceneUnloadedCallback(this.OnSceneUnloaded));
  }

  private void OnDestroy()
  {
    TooltipPanelManager.s_instance = (TooltipPanelManager) null;
  }

  public static TooltipPanelManager Get()
  {
    return TooltipPanelManager.s_instance;
  }

  public void UpdateKeywordPanelsPosition(Card card, bool showOnRight)
  {
    Actor actor = card.GetActor();
    bool inHand = card.GetZone() is ZoneHand;
    this.StartCoroutine(this.PositionPanelsForGame(actor.GetMeshRenderer().gameObject, showOnRight, inHand, new Vector3?()));
  }

  public void UpdateKeywordHelp(Card card, Actor actor, bool showOnRight = true, float? overrideScale = null, Vector3? overrideOffset = null)
  {
    this.m_card = card;
    this.UpdateKeywordHelp(card.GetEntity(), actor, showOnRight, overrideScale, overrideOffset);
  }

  public void UpdateKeywordHelp(Entity entity, Actor actor, bool showOnRight, float? overrideScale = null, Vector3? overrideOffset = null)
  {
    this.m_card = entity.GetCard();
    if (GameState.Get().GetGameEntity().ShouldShowCrazyKeywordTooltip())
    {
      if (!((Object) TutorialKeywordManager.Get() != (Object) null))
        return;
      TutorialKeywordManager.Get().UpdateKeywordHelp(entity, actor, showOnRight, overrideScale);
    }
    else
    {
      bool inHand = this.m_card.GetZone() is ZoneHand;
      this.scaleToUse = !overrideScale.HasValue ? (!inHand ? (float) TooltipPanel.GAMEPLAY_SCALE : (float) TooltipPanel.HAND_SCALE) : overrideScale.Value;
      this.PrepareToUpdateKeywordHelp(actor);
      string[] strArray = GameState.Get().GetGameEntity().NotifyOfKeywordHelpPanelDisplay(entity);
      if (strArray != null)
        this.SetupTooltipPanel(strArray[0], strArray[1]);
      this.SetUpPanels((EntityBase) entity);
      this.StartCoroutine(this.PositionPanelsForGame(actor.GetMeshRenderer().gameObject, showOnRight, inHand, overrideOffset));
      GameState.Get().GetGameEntity().NotifyOfHelpPanelDisplay(this.m_tooltipPanels.Count);
    }
  }

  [DebuggerHidden]
  private IEnumerator PositionPanelsForGame(GameObject actorObject, bool showOnRight, bool inHand, Vector3? overrideOffset = null)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TooltipPanelManager.\u003CPositionPanelsForGame\u003Ec__IteratorE1() { actorObject = actorObject, overrideOffset = overrideOffset, showOnRight = showOnRight, inHand = inHand, \u003C\u0024\u003EactorObject = actorObject, \u003C\u0024\u003EoverrideOffset = overrideOffset, \u003C\u0024\u003EshowOnRight = showOnRight, \u003C\u0024\u003EinHand = inHand, \u003C\u003Ef__this = this };
  }

  public void UpdateKeywordHelpForHistoryCard(Entity entity, Actor actor, UberText createdByText)
  {
    this.m_card = entity.GetCard();
    this.scaleToUse = (float) TooltipPanel.HISTORY_SCALE;
    this.PrepareToUpdateKeywordHelp(actor);
    string[] strArray = GameState.Get().GetGameEntity().NotifyOfKeywordHelpPanelDisplay(entity);
    if (strArray != null)
      this.SetupTooltipPanel(strArray[0], strArray[1]);
    this.SetUpPanels((EntityBase) entity);
    this.StartCoroutine(this.PositionPanelsForHistory(actor, createdByText));
  }

  [DebuggerHidden]
  private IEnumerator PositionPanelsForHistory(Actor actor, UberText createdByText)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TooltipPanelManager.\u003CPositionPanelsForHistory\u003Ec__IteratorE2() { createdByText = createdByText, actor = actor, \u003C\u0024\u003EcreatedByText = createdByText, \u003C\u0024\u003Eactor = actor, \u003C\u003Ef__this = this };
  }

  public void UpdateKeywordHelpForCollectionManager(EntityDef entityDef, Actor actor, TooltipPanelManager.Orientation orientation)
  {
    this.scaleToUse = (float) TooltipPanel.COLLECTION_MANAGER_SCALE;
    this.PrepareToUpdateKeywordHelp(actor);
    this.SetUpPanels((EntityBase) entityDef);
    this.StartCoroutine(this.PositionPanelsForCM(actor, orientation));
  }

  [DebuggerHidden]
  private IEnumerator PositionPanelsForCM(Actor actor, TooltipPanelManager.Orientation orientation = TooltipPanelManager.Orientation.RightTop)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TooltipPanelManager.\u003CPositionPanelsForCM\u003Ec__IteratorE3() { actor = actor, orientation = orientation, \u003C\u0024\u003Eactor = actor, \u003C\u0024\u003Eorientation = orientation, \u003C\u003Ef__this = this };
  }

  public void UpdateGhostCardHelpForCollectionManager(Actor actor, GhostCard.Type ghostType, TooltipPanelManager.Orientation orientation)
  {
    this.scaleToUse = (float) TooltipPanel.COLLECTION_MANAGER_SCALE;
    this.PrepareToUpdateGhostCardHelp(actor);
    string str = !UniversalInputManager.Get().IsTouchMode() ? string.Empty : "_TOUCH";
    string headline;
    string description;
    if (ghostType == GhostCard.Type.NOT_VALID)
    {
      headline = GameStrings.Get("GLUE_GHOST_CARD_NOT_VALID_TITLE");
      description = GameStrings.Get("GLUE_GHOST_CARD_NOT_VALID_DESCRIPTION" + str);
    }
    else
    {
      if (ghostType != GhostCard.Type.MISSING && ghostType != GhostCard.Type.MISSING_UNCRAFTABLE)
        return;
      headline = GameStrings.Get("GLUE_GHOST_CARD_MISSING_TITLE");
      description = GameStrings.Get("GLUE_GHOST_CARD_MISSING_DESCRIPTION" + str);
    }
    this.SetupTooltipPanel(headline, description);
    this.StartCoroutine(this.PositionPanelsForCM(actor, orientation));
  }

  public void UpdateKeywordHelpForDeckHelper(EntityDef entityDef, Actor actor)
  {
    this.scaleToUse = 3.75f;
    this.PrepareToUpdateKeywordHelp(actor);
    this.SetUpPanels((EntityBase) entityDef);
    this.StartCoroutine(this.PositionPanelsForForge(actor.GetMeshRenderer().gameObject, 0));
  }

  public void UpdateKeywordHelpForForge(EntityDef entityDef, Actor actor, int cardChoice = 0)
  {
    this.scaleToUse = (float) TooltipPanel.FORGE_SCALE;
    this.PrepareToUpdateKeywordHelp(actor);
    this.SetUpPanels((EntityBase) entityDef);
    this.StartCoroutine(this.PositionPanelsForForge(actor.GetMeshRenderer().gameObject, cardChoice));
  }

  [DebuggerHidden]
  private IEnumerator PositionPanelsForForge(GameObject actorObject, int cardChoice = 0)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TooltipPanelManager.\u003CPositionPanelsForForge\u003Ec__IteratorE4() { actorObject = actorObject, cardChoice = cardChoice, \u003C\u0024\u003EactorObject = actorObject, \u003C\u0024\u003EcardChoice = cardChoice, \u003C\u003Ef__this = this };
  }

  public void UpdateKeywordHelpForPackOpening(EntityDef entityDef, Actor actor)
  {
    this.scaleToUse = 2.75f;
    this.PrepareToUpdateKeywordHelp(actor);
    this.SetUpPanels((EntityBase) entityDef);
    this.StartCoroutine(this.PositionPanelsForPackOpening(actor.GetMeshRenderer().gameObject));
  }

  [DebuggerHidden]
  private IEnumerator PositionPanelsForPackOpening(GameObject actorObject)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TooltipPanelManager.\u003CPositionPanelsForPackOpening\u003Ec__IteratorE5() { actorObject = actorObject, \u003C\u0024\u003EactorObject = actorObject, \u003C\u003Ef__this = this };
  }

  public void UpdateKeywordHelpForMulliganCard(Entity entity, Actor actor)
  {
    this.m_card = entity.GetCard();
    this.scaleToUse = (float) TooltipPanel.MULLIGAN_SCALE;
    this.PrepareToUpdateKeywordHelp(actor);
    string[] strArray = GameState.Get().GetGameEntity().NotifyOfKeywordHelpPanelDisplay(entity);
    if (strArray != null)
      this.SetupTooltipPanel(strArray[0], strArray[1]);
    this.SetUpPanels((EntityBase) entity);
    this.StartCoroutine(this.PositionPanelsForMulligan(actor.GetMeshRenderer().gameObject));
  }

  [DebuggerHidden]
  private IEnumerator PositionPanelsForMulligan(GameObject actorObject)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TooltipPanelManager.\u003CPositionPanelsForMulligan\u003Ec__IteratorE6() { actorObject = actorObject, \u003C\u0024\u003EactorObject = actorObject, \u003C\u003Ef__this = this };
  }

  private void PrepareToUpdateKeywordHelp(Actor actor)
  {
    this.HideKeywordHelp();
    this.m_actor = actor;
    this.m_tooltipPanels.Clear();
  }

  private void PrepareToUpdateGhostCardHelp(Actor actor)
  {
    this.HideTooltipPanels();
    this.m_actor = actor;
    this.m_tooltipPanels.Clear();
  }

  private void SetUpPanels(EntityBase entityInfo)
  {
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.SHIFTING);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.TAUNT);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.STEALTH);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.DIVINE_SHIELD);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.SPELLPOWER);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.ENRAGED);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.CHARGE);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.BATTLECRY);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.FROZEN);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.FREEZE);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.WINDFURY);
    if (entityInfo.GetZone() != TAG_ZONE.SECRET)
      this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.SECRET);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.DEATHRATTLE);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.OVERLOAD);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.COMBO);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.SILENCE);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.COUNTER);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.IMMUNE);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.SPARE_PART);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.INSPIRE);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.DISCOVER);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.CTHUN);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.AUTOATTACK);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.MINION_TYPE_REFERENCE);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.JADE_GOLEM);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.GRIMY_GOONS);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.JADE_LOTUS);
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.KABAL);
    if (!entityInfo.IsHeroPower())
      return;
    this.SetupKeywordPanelIfNecessary(entityInfo, GAME_TAG.AI_MUST_PLAY);
  }

  private bool SetupKeywordPanelIfNecessary(EntityBase entityInfo, GAME_TAG tag)
  {
    if (!entityInfo.HasTag(tag) && !entityInfo.HasReferencedTag(tag))
      return false;
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.COLLECTIONMANAGER && GameStrings.HasCollectionKeywordText(tag))
    {
      this.SetupCollectionKeywordPanel(tag);
      return true;
    }
    if (entityInfo.HasTag(tag) && GameStrings.HasKeywordText(tag))
    {
      if (tag == GAME_TAG.WINDFURY && entityInfo.GetTag(tag) > 1)
        return false;
      this.SetupKeywordPanel(tag);
      return true;
    }
    if (!entityInfo.HasReferencedTag(tag) || !GameStrings.HasRefKeywordText(tag))
      return false;
    this.SetupKeywordRefPanel(tag);
    return true;
  }

  private Vector3 GetPanelPosition(TooltipPanel panel)
  {
    Vector3 vector3 = new Vector3(0.0f, 0.0f, 0.0f);
    TooltipPanel tooltipPanel1 = (TooltipPanel) null;
    for (int index = 0; index < this.m_tooltipPanels.Count; ++index)
    {
      TooltipPanel tooltipPanel2 = this.m_tooltipPanels[index];
      float num1 = !this.m_card.GetEntity().IsHero() ? (this.m_card.GetEntity().GetZone() != TAG_ZONE.PLAY ? 0.85f : 1.05f) : 1.2f;
      if ((Object) this.m_actor.GetMeshRenderer() == (Object) null)
        return vector3;
      float num2 = -0.2f * this.m_actor.GetMeshRenderer().bounds.size.z;
      if ((Object) tooltipPanel2 == (Object) panel)
        vector3 = index != 0 ? tooltipPanel1.transform.position - new Vector3(0.0f, 0.0f, (float) ((double) tooltipPanel1.GetHeight() * 0.349999994039536 + (double) tooltipPanel2.GetHeight() * 0.349999994039536)) : this.m_actor.transform.position + new Vector3(this.m_actor.GetMeshRenderer().bounds.size.x * num1, 0.0f, this.m_actor.GetMeshRenderer().bounds.extents.z + num2);
      tooltipPanel1 = tooltipPanel2;
    }
    return vector3;
  }

  private void SetupCollectionKeywordPanel(GAME_TAG tag)
  {
    this.SetupTooltipPanel(GameStrings.GetKeywordName(tag), this.GetTextForTag(tag, GameStrings.GetCollectionKeywordTextKey(tag)));
  }

  private void SetupKeywordPanel(GAME_TAG tag)
  {
    this.SetupTooltipPanel(GameStrings.GetKeywordName(tag), this.GetTextForTag(tag, GameStrings.GetKeywordTextKey(tag)));
  }

  private void SetupKeywordRefPanel(GAME_TAG tag)
  {
    this.SetupTooltipPanel(GameStrings.GetKeywordName(tag), this.GetTextForTag(tag, GameStrings.GetRefKeywordTextKey(tag)));
  }

  private string GetTextForTag(GAME_TAG tag, string key)
  {
    if (tag != GAME_TAG.SPELLPOWER)
      return GameStrings.Get(key);
    int num = !((Object) this.m_card != (Object) null) ? (!((Object) this.m_actor != (Object) null) || this.m_actor.GetEntityDef() == null || !(this.m_actor.GetEntityDef().GetCardId() == "EX1_563") ? 1 : 5) : this.m_card.GetEntity().GetSpellPower();
    return GameStrings.Format(key, (object) num);
  }

  private void SetupTooltipPanel(string headline, string description)
  {
    TooltipPanel helpPanel = this.m_tooltipPanelPool.Acquire();
    if ((Object) helpPanel == (Object) null)
      return;
    helpPanel.Reset();
    helpPanel.Initialize(headline, description);
    helpPanel.SetScale(this.scaleToUse);
    this.m_tooltipPanels.Add(helpPanel);
    this.FadeInPanel(helpPanel);
  }

  private void FadeInPanel(TooltipPanel helpPanel)
  {
    this.CleanTweensOnPanel(helpPanel);
    float num = 0.4f;
    if (GameState.Get() != null && GameState.Get().GetGameEntity().IsKeywordHelpDelayOverridden())
      num = 0.0f;
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "onupdatetarget", (object) this.gameObject, (object) "onupdate", (object) "OnUberTextFadeUpdate", (object) "time", (object) 0.125f, (object) "delay", (object) num, (object) "to", (object) 1f, (object) "from", (object) 0.0f));
  }

  private void OnUberTextFadeUpdate(float newValue)
  {
    using (List<TooltipPanel>.Enumerator enumerator = this.m_tooltipPanels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        RenderUtils.SetAlpha(enumerator.Current.gameObject, newValue, true);
    }
  }

  private void CleanTweensOnPanel(TooltipPanel helpPanel)
  {
    iTween.Stop(this.gameObject);
    RenderUtils.SetAlpha(helpPanel.gameObject, 0.0f, true);
  }

  public void ShowKeywordHelp()
  {
    using (List<TooltipPanel>.Enumerator enumerator = this.m_tooltipPanels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.gameObject.SetActive(true);
    }
  }

  public void HideKeywordHelp()
  {
    GameState gameState = GameState.Get();
    if (gameState != null && gameState.GetGameEntity().ShouldShowCrazyKeywordTooltip() && (Object) TutorialKeywordManager.Get() != (Object) null)
      TutorialKeywordManager.Get().HideKeywordHelp();
    this.HideTooltipPanels();
  }

  public void HideTooltipPanels()
  {
    using (List<TooltipPanel>.Enumerator enumerator = this.m_tooltipPanels.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TooltipPanel current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          this.CleanTweensOnPanel(current);
          current.gameObject.SetActive(false);
          this.m_tooltipPanelPool.Release(current);
        }
      }
    }
  }

  public Card GetCard()
  {
    return this.m_card;
  }

  public Vector3 GetPositionOfTopPanel()
  {
    if (this.m_tooltipPanels.Count == 0)
      return new Vector3(0.0f, 0.0f, 0.0f);
    return this.m_tooltipPanels[0].transform.position;
  }

  public TooltipPanel CreateKeywordPanel(int i)
  {
    return Object.Instantiate<TooltipPanel>(this.m_tooltipPanelPrefab);
  }

  private void DestroyKeywordPanel(TooltipPanel panel)
  {
    Object.Destroy((Object) panel.gameObject);
  }

  private void OnSceneUnloaded(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    using (List<TooltipPanel>.Enumerator enumerator = this.m_tooltipPanels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current.gameObject);
    }
    this.m_tooltipPanels.Clear();
    this.m_tooltipPanelPool.Clear();
    Object.Destroy((Object) this.m_actor);
    this.m_actor = (Actor) null;
    Object.Destroy((Object) this.m_card);
    this.m_card = (Card) null;
  }

  public enum Orientation
  {
    RightTop,
    RightBottom,
    LeftMiddle,
  }
}
