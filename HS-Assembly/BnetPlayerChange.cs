﻿// Decompiled with JetBrains decompiler
// Type: BnetPlayerChange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class BnetPlayerChange
{
  private BnetPlayer m_oldPlayer;
  private BnetPlayer m_newPlayer;

  public BnetPlayer GetOldPlayer()
  {
    return this.m_oldPlayer;
  }

  public void SetOldPlayer(BnetPlayer player)
  {
    this.m_oldPlayer = player;
  }

  public BnetPlayer GetNewPlayer()
  {
    return this.m_newPlayer;
  }

  public void SetNewPlayer(BnetPlayer player)
  {
    this.m_newPlayer = player;
  }

  public BnetPlayer GetPlayer()
  {
    return this.m_newPlayer;
  }
}
