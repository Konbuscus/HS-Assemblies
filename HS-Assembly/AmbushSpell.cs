﻿// Decompiled with JetBrains decompiler
// Type: AmbushSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class AmbushSpell : OverrideCustomSpawnSpell
{
  public override bool AddPowerTargets()
  {
    if (!this.m_taskList.IsStartOfBlock())
      return false;
    this.AddMultiplePowerTargets_FromMetaData(this.m_taskList.GetTaskList());
    return true;
  }
}
