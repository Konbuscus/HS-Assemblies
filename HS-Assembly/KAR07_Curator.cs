﻿// Decompiled with JetBrains decompiler
// Type: KAR07_Curator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR07_Curator : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorEmoteResponse_01");
    this.PreloadSound("VO_Moroes_Male_Human_CuratorTurn3_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorTurn1_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorTurn5_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorTurn9_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorArcaneGolem_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorMedivhSkin_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorHarrison_02");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorReno_02");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorMurlocs_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorPirates_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorBeasts_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorDemons_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorMechs_01");
    this.PreloadSound("VO_Curator_Male_ArcaneGolem_CuratorDragons_01");
    this.PreloadSound("VO_Moroes_Male_Human_CuratorWin_01");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Curator_Male_ArcaneGolem_CuratorEmoteResponse_01",
            m_stringTag = "VO_Curator_Male_ArcaneGolem_CuratorEmoteResponse_01"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR07_Curator.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1A5() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR07_Curator.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator1A6() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR07_Curator.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1A7() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR07_Curator.\u003CHandleGameOverWithTiming\u003Ec__Iterator1A8() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
