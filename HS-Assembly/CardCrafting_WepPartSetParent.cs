﻿// Decompiled with JetBrains decompiler
// Type: CardCrafting_WepPartSetParent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardCrafting_WepPartSetParent : MonoBehaviour
{
  public GameObject m_Parent;
  public GameObject m_WepParts;

  private void Start()
  {
    if ((bool) ((Object) this.m_Parent))
      return;
    Debug.LogError((object) "Animation Event Set Parent is null!");
    this.enabled = false;
  }

  public void SetParentWepParts()
  {
    if (!(bool) ((Object) this.m_Parent))
      return;
    this.m_WepParts.transform.parent = this.m_Parent.transform;
  }
}
