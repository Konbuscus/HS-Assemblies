﻿// Decompiled with JetBrains decompiler
// Type: ParticleEffectsRepulser
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class ParticleEffectsRepulser
{
  public float m_Radius = 1f;
  public float m_Power = 1f;
  public Transform m_Transform;
}
