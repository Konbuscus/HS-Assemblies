﻿// Decompiled with JetBrains decompiler
// Type: Pool`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class Pool<T>
{
  private List<T> m_freeList = new List<T>();
  private List<T> m_activeList = new List<T>();
  private int m_extensionCount = 5;
  private int m_maxReleasedItemCount = 5;
  public const int DEFAULT_EXTENSION_COUNT = 5;
  public const int DEFAULT_MAX_RELEASED_ITEM_COUNT = 5;
  private Pool<T>.CreateItemCallback m_createItemCallback;
  private Pool<T>.DestroyItemCallback m_destroyItemCallback;

  public int GetExtensionCount()
  {
    return this.m_extensionCount;
  }

  public void SetExtensionCount(int count)
  {
    this.m_extensionCount = count;
  }

  public int GetMaxReleasedItemCount()
  {
    return this.m_maxReleasedItemCount;
  }

  public void SetMaxReleasedItemCount(int count)
  {
    this.m_maxReleasedItemCount = count;
  }

  public Pool<T>.CreateItemCallback GetCreateItemCallback()
  {
    return this.m_createItemCallback;
  }

  public void SetCreateItemCallback(Pool<T>.CreateItemCallback callback)
  {
    this.m_createItemCallback = callback;
  }

  public Pool<T>.DestroyItemCallback GetDestroyItemCallback()
  {
    return this.m_destroyItemCallback;
  }

  public void SetDestroyItemCallback(Pool<T>.DestroyItemCallback callback)
  {
    this.m_destroyItemCallback = callback;
  }

  public T Acquire()
  {
    if (this.m_freeList.Count == 0)
    {
      if (this.m_extensionCount == 0)
        return default (T);
      if (!this.AddFreeItems(this.m_extensionCount))
        return default (T);
    }
    int index = this.m_freeList.Count - 1;
    T free = this.m_freeList[index];
    this.m_freeList.RemoveAt(index);
    this.m_activeList.Add(free);
    return free;
  }

  public List<T> AcquireBatch(int count)
  {
    List<T> objList = new List<T>();
    for (int index = 0; index < count; ++index)
    {
      T obj = this.Acquire();
      objList.Add(obj);
    }
    return objList;
  }

  public bool Release(T item)
  {
    if (!this.m_activeList.Remove(item))
      return false;
    if (this.m_freeList.Count < this.m_maxReleasedItemCount)
    {
      this.m_freeList.Add(item);
      return true;
    }
    if (this.m_destroyItemCallback != null)
      return false;
    this.m_destroyItemCallback(item);
    return true;
  }

  public bool ReleaseBatch(int activeListStart, int count)
  {
    if (count <= 0)
      return true;
    if (activeListStart >= this.m_activeList.Count)
      return false;
    int num1 = this.m_activeList.Count - activeListStart;
    if (count > num1)
      count = num1;
    int count1 = count;
    int num2 = this.m_maxReleasedItemCount - this.m_freeList.Count;
    if (count1 > num2)
      count1 = num2;
    if (count1 > 0)
    {
      List<T> range = this.m_activeList.GetRange(activeListStart, count1);
      this.m_activeList.RemoveRange(activeListStart, count1);
      this.m_freeList.AddRange((IEnumerable<T>) range);
    }
    int num3 = count - count1;
    if (num3 > 0)
    {
      if (this.m_destroyItemCallback == null)
        return false;
      for (int index = 0; index < num3; ++index)
      {
        T active = this.m_activeList[activeListStart];
        this.m_activeList.RemoveAt(activeListStart);
        this.m_destroyItemCallback(active);
      }
    }
    return true;
  }

  public bool ReleaseAll()
  {
    return this.ReleaseBatch(0, this.m_activeList.Count);
  }

  public bool AddFreeItems(int count)
  {
    if (this.m_createItemCallback == null)
      return false;
    for (int index = 0; index < count; ++index)
      this.m_freeList.Add(this.m_createItemCallback(this.m_activeList.Count + this.m_freeList.Count + 1));
    return true;
  }

  public void Clear()
  {
    if (this.m_destroyItemCallback == null)
    {
      this.m_activeList.Clear();
      this.m_freeList.Clear();
    }
    else
    {
      for (int index = 0; index < this.m_activeList.Count; ++index)
        this.m_destroyItemCallback(this.m_activeList[index]);
      this.m_activeList.Clear();
      for (int index = 0; index < this.m_freeList.Count; ++index)
        this.m_destroyItemCallback(this.m_freeList[index]);
      this.m_freeList.Clear();
    }
  }

  public List<T> GetFreeList()
  {
    return this.m_freeList;
  }

  public List<T> GetActiveList()
  {
    return this.m_activeList;
  }

  public delegate T CreateItemCallback(int freeListIndex);

  public delegate void DestroyItemCallback(T item);
}
