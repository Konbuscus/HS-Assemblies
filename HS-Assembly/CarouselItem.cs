﻿// Decompiled with JetBrains decompiler
// Type: CarouselItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class CarouselItem
{
  public abstract void Show(Carousel parent);

  public abstract void Hide();

  public abstract void Clear();

  public abstract GameObject GetGameObject();

  public abstract bool IsLoaded();
}
