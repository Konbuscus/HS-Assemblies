﻿// Decompiled with JetBrains decompiler
// Type: ProjectedShadow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class ProjectedShadow : MonoBehaviour
{
  private static float s_offset = -12000f;
  private static Color s_ShadowColor = new Color(0.098f, 0.098f, 0.235f, 0.45f);
  private readonly Vector2[] PLANE_UVS = new Vector2[4]{ new Vector2(0.0f, 0.0f), new Vector2(1f, 0.0f), new Vector2(0.0f, 1f), new Vector2(1f, 1f) };
  private readonly Vector3[] PLANE_NORMALS = new Vector3[4]{ Vector3.up, Vector3.up, Vector3.up, Vector3.up };
  private readonly int[] PLANE_TRIANGLES = new int[6]{ 3, 1, 2, 2, 1, 0 };
  public float m_ShadowProjectorSize = 1.5f;
  public float m_ProjectionFarClip = 10f;
  public Vector3 m_ContactOffset = Vector3.zero;
  public bool m_isDirtyContactShadow = true;
  private float m_AdjustedShadowProjectorSize = 1.5f;
  private float m_BoardHeight = 0.2f;
  private const int RENDER_SIZE = 64;
  private const string SHADER_NAME = "Custom/ProjectedShadow";
  private const string CONTACT_SHADER_NAME = "Custom/ContactShadow";
  private const string SHADER_FALLOFF_RAMP = "Textures/ProjectedShadowRamp";
  private const string EDGE_FALLOFF_TEXTURE = "Textures/ProjectedShadowEdgeAlpha";
  private const string GAMEOBJECT_NAME_EXT = "ShadowProjector";
  private const string UNLIT_WHITE_SHADER_NAME = "Custom/Unlit/Color/White";
  private const string UNLIT_LIGHTGREY_SHADER_NAME = "Custom/Unlit/Color/LightGrey";
  private const string UNLIT_DARKGREY_SHADER_NAME = "Custom/Unlit/Color/DarkGrey";
  private const string MULTISAMPLE_SHADER_NAME = "Custom/Selection/HighlightMultiSample";
  private const float NEARCLIP_PLANE = 0.0f;
  private const float SHADOW_OFFSET_SCALE = 0.3f;
  private const float RENDERMASK_OFFSET = 0.11f;
  private const float RENDERMASK_BLUR = 0.6f;
  private const float RENDERMASK_BLUR2 = 0.8f;
  private const float CONTACT_SHADOW_SCALE = 0.98f;
  private const float CONTACT_SHADOW_FADE_IN_HEIGHT = 0.08f;
  private const float CONTACT_SHADOW_INTENSITY = 3.5f;
  private const int CONTACT_SHADOW_RESOLUTION = 80;
  public bool m_ShadowEnabled;
  public bool m_AutoBoardHeightDisable;
  public float m_AutoDisableHeight;
  public bool m_ContinuousRendering;
  public Vector3 m_ProjectionOffset;
  public bool m_ContactShadow;
  private GameObject m_RootObject;
  private GameObject m_ProjectorGameObject;
  private Transform m_ProjectorTransform;
  private Projector m_Projector;
  private Camera m_Camera;
  private RenderTexture m_ShadowTexture;
  private RenderTexture m_ContactShadowTexture;
  private Mesh m_PlaneMesh;
  private GameObject m_PlaneGameObject;
  private Texture2D m_ShadowFalloffRamp;
  private Texture2D m_EdgeFalloffTexture;
  private Shader m_ShadowShader;
  private Shader m_UnlitWhiteShader;
  private Shader m_UnlitDarkGreyShader;
  private Shader m_UnlitLightGreyShader;
  private Material m_ShadowMaterial;
  private Shader m_ContactShadowShader;
  private Material m_ContactShadowMaterial;
  private Shader m_MultiSampleShader;
  private Material m_MultiSampleMaterial;

  protected Material ShadowMaterial
  {
    get
    {
      if ((Object) this.m_ShadowMaterial == (Object) null)
      {
        this.m_ShadowMaterial = new Material(this.m_ShadowShader);
        SceneUtils.SetHideFlags((Object) this.m_ShadowMaterial, HideFlags.DontSave);
        this.m_ShadowMaterial.SetTexture("_Ramp", (Texture) this.m_ShadowFalloffRamp);
        this.m_ShadowMaterial.SetTexture("_MainTex", (Texture) this.m_ShadowTexture);
        this.m_ShadowMaterial.SetColor("_Color", ProjectedShadow.s_ShadowColor);
        this.m_ShadowMaterial.SetTexture("_Edge", (Texture) this.m_EdgeFalloffTexture);
      }
      return this.m_ShadowMaterial;
    }
  }

  protected Material ContactShadowMaterial
  {
    get
    {
      if ((Object) this.m_ContactShadowMaterial == (Object) null)
      {
        this.m_ContactShadowMaterial = new Material(this.m_ContactShadowShader);
        this.m_ContactShadowMaterial.SetFloat("_Intensity", 3.5f);
        this.m_ContactShadowMaterial.SetColor("_Color", ProjectedShadow.s_ShadowColor);
        SceneUtils.SetHideFlags((Object) this.m_ContactShadowMaterial, HideFlags.DontSave);
      }
      return this.m_ContactShadowMaterial;
    }
  }

  protected Material MultiSampleMaterial
  {
    get
    {
      if ((Object) this.m_MultiSampleMaterial == (Object) null)
      {
        this.m_MultiSampleMaterial = new Material(this.m_MultiSampleShader);
        SceneUtils.SetHideFlags((Object) this.m_MultiSampleMaterial, HideFlags.DontSave);
      }
      return this.m_MultiSampleMaterial;
    }
  }

  protected void Start()
  {
    GraphicsManager graphicsManager = GraphicsManager.Get();
    if ((bool) ((Object) graphicsManager) && graphicsManager.RealtimeShadows)
      this.enabled = false;
    if ((Object) this.m_ShadowShader == (Object) null)
      this.m_ShadowShader = ShaderUtils.FindShader("Custom/ProjectedShadow");
    if (!(bool) ((Object) this.m_ShadowShader))
    {
      UnityEngine.Debug.LogError((object) "Failed to load Projected Shadow Shader: Custom/ProjectedShadow");
      this.enabled = false;
    }
    if ((Object) this.m_ContactShadowShader == (Object) null)
      this.m_ContactShadowShader = ShaderUtils.FindShader("Custom/ContactShadow");
    if (!(bool) ((Object) this.m_ContactShadowShader))
    {
      UnityEngine.Debug.LogError((object) "Failed to load Projected Shadow Shader: Custom/ContactShadow");
      this.enabled = false;
    }
    if ((Object) this.m_ShadowFalloffRamp == (Object) null)
      this.m_ShadowFalloffRamp = Resources.Load("Textures/ProjectedShadowRamp") as Texture2D;
    if (!(bool) ((Object) this.m_ShadowFalloffRamp))
    {
      UnityEngine.Debug.LogError((object) "Failed to load Projected Shadow Ramp: Textures/ProjectedShadowRamp");
      this.enabled = false;
    }
    if ((Object) this.m_EdgeFalloffTexture == (Object) null)
      this.m_EdgeFalloffTexture = Resources.Load("Textures/ProjectedShadowEdgeAlpha") as Texture2D;
    if (!(bool) ((Object) this.m_EdgeFalloffTexture))
    {
      UnityEngine.Debug.LogError((object) "Failed to load Projected Shadow Edge Falloff Texture: Textures/ProjectedShadowEdgeAlpha");
      this.enabled = false;
    }
    if ((Object) this.m_MultiSampleShader == (Object) null)
      this.m_MultiSampleShader = ShaderUtils.FindShader("Custom/Selection/HighlightMultiSample");
    if (!(bool) ((Object) this.m_MultiSampleShader))
    {
      UnityEngine.Debug.LogError((object) "Failed to load Projected Shadow Shader: Custom/Selection/HighlightMultiSample");
      this.enabled = false;
    }
    this.m_UnlitWhiteShader = ShaderUtils.FindShader("Custom/Unlit/Color/White");
    if (!(bool) ((Object) this.m_UnlitWhiteShader))
      UnityEngine.Debug.LogError((object) "Failed to load Projected Shadow Shader: Custom/Unlit/Color/White");
    this.m_UnlitLightGreyShader = ShaderUtils.FindShader("Custom/Unlit/Color/LightGrey");
    if (!(bool) ((Object) this.m_UnlitLightGreyShader))
      UnityEngine.Debug.LogError((object) "Failed to load Projected Shadow Shader: Custom/Unlit/Color/LightGrey");
    this.m_UnlitDarkGreyShader = ShaderUtils.FindShader("Custom/Unlit/Color/DarkGrey");
    if (!(bool) ((Object) this.m_UnlitDarkGreyShader))
      UnityEngine.Debug.LogError((object) "Failed to load Projected Shadow Shader: Custom/Unlit/Color/DarkGrey");
    if ((Object) Board.Get() != (Object) null)
      this.StartCoroutine(this.AssignBoardHeight_WaitForBoardStandardGameLoaded());
    Actor component = this.GetComponent<Actor>();
    if ((Object) component != (Object) null)
    {
      this.m_RootObject = component.GetRootObject();
    }
    else
    {
      GameObject childBySubstring = SceneUtils.FindChildBySubstring(this.gameObject, "RootObject");
      this.m_RootObject = !((Object) childBySubstring != (Object) null) ? this.gameObject : childBySubstring;
    }
    this.m_ShadowMaterial = this.ShadowMaterial;
  }

  [DebuggerHidden]
  private IEnumerator AssignBoardHeight_WaitForBoardStandardGameLoaded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ProjectedShadow.\u003CAssignBoardHeight_WaitForBoardStandardGameLoaded\u003Ec__Iterator33A() { \u003C\u003Ef__this = this };
  }

  protected void Update()
  {
    GraphicsManager graphicsManager = GraphicsManager.Get();
    if ((bool) ((Object) graphicsManager) && graphicsManager.RealtimeShadows)
    {
      this.enabled = false;
    }
    else
    {
      this.Render();
      if (!this.m_ContactShadow)
        return;
      this.RenderContactShadow();
    }
  }

  private void OnDisable()
  {
    if ((Object) this.m_Projector != (Object) null)
      this.m_Projector.enabled = false;
    if ((bool) ((Object) this.m_PlaneGameObject))
      Object.DestroyImmediate((Object) this.m_PlaneGameObject);
    if ((Object) RenderTexture.active == (Object) this.m_ShadowTexture || (Object) RenderTexture.active == (Object) this.m_ContactShadowTexture)
      RenderTexture.active = (RenderTexture) null;
    if ((bool) ((Object) this.m_ShadowTexture))
      Object.DestroyImmediate((Object) this.m_ShadowTexture);
    this.m_ShadowTexture = (RenderTexture) null;
    if ((bool) ((Object) this.m_ContactShadowTexture))
      Object.DestroyImmediate((Object) this.m_ContactShadowTexture);
    this.m_ContactShadowTexture = (RenderTexture) null;
  }

  protected void OnDestroy()
  {
    if ((bool) ((Object) this.m_ShadowMaterial))
      Object.Destroy((Object) this.m_ShadowMaterial);
    if ((bool) ((Object) this.m_MultiSampleMaterial))
      Object.Destroy((Object) this.m_MultiSampleMaterial);
    if ((bool) ((Object) this.m_Camera))
      Object.Destroy((Object) this.m_Camera.gameObject);
    if (!(bool) ((Object) this.m_ProjectorGameObject))
      return;
    Object.Destroy((Object) this.m_ProjectorGameObject);
  }

  private void OnDrawGizmos()
  {
    float num = (float) ((double) this.m_ShadowProjectorSize * (double) TransformUtil.ComputeWorldScale((Component) this.transform).x * 2.0);
    Gizmos.matrix = this.transform.localToWorldMatrix;
    Gizmos.color = new Color(0.6f, 0.15f, 0.6f);
    if (this.m_ContactShadow)
      Gizmos.DrawWireCube(this.m_ContactOffset, new Vector3(num, 0.0f, num));
    else
      Gizmos.DrawWireCube(Vector3.zero, new Vector3(num, 0.0f, num));
    Gizmos.matrix = Matrix4x4.identity;
  }

  public void Render()
  {
    if (!this.m_ShadowEnabled || !this.m_RootObject.activeSelf)
    {
      if ((bool) ((Object) this.m_Projector) && this.m_Projector.enabled)
        this.m_Projector.enabled = false;
      if (!(bool) ((Object) this.m_PlaneGameObject))
        return;
      this.m_PlaneGameObject.SetActive(false);
    }
    else
    {
      this.m_AdjustedShadowProjectorSize = this.m_ShadowProjectorSize * TransformUtil.ComputeWorldScale((Component) this.transform).x;
      if ((Object) this.m_Projector == (Object) null)
        this.CreateProjector();
      if ((Object) this.m_Camera == (Object) null)
        this.CreateCamera();
      float num1 = (float) (((double) this.transform.position.y - (double) this.m_BoardHeight) * 0.300000011920929);
      this.m_AdjustedShadowProjectorSize += Mathf.Lerp(0.0f, 0.5f, num1 * 0.5f);
      if (this.m_ContactShadow)
      {
        float num2 = this.m_BoardHeight + 0.08f;
        if ((double) num1 < (double) num2)
        {
          if ((Object) this.m_PlaneGameObject == (Object) null)
            this.m_isDirtyContactShadow = true;
          else if (!this.m_PlaneGameObject.activeSelf)
            this.m_isDirtyContactShadow = true;
          float num3 = Mathf.Clamp((num2 - num1) / num2, 0.0f, 1f);
          if ((bool) ((Object) this.m_ContactShadowTexture))
          {
            this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial.mainTexture = (Texture) this.m_ContactShadowTexture;
            this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial.color = ProjectedShadow.s_ShadowColor;
            this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial.SetFloat("_Alpha", num3);
          }
        }
        else if ((Object) this.m_PlaneGameObject != (Object) null)
          this.m_PlaneGameObject.SetActive(false);
      }
      if ((double) num1 < (double) this.m_AutoDisableHeight && this.m_AutoBoardHeightDisable)
      {
        this.m_Projector.enabled = false;
        Object.DestroyImmediate((Object) this.m_ShadowTexture);
        this.m_ShadowTexture = (RenderTexture) null;
      }
      else
      {
        this.m_Projector.enabled = true;
        float num2 = 0.0f;
        if ((Object) this.transform.parent != (Object) null)
          num2 = Mathf.Lerp(-0.7f, 1.8f, (float) ((double) this.transform.parent.position.x / 17.0 * -1.0)) * num1;
        this.m_ProjectorTransform.position = new Vector3((float) ((double) this.transform.position.x - (double) num2 - (double) num1 * 0.25), this.transform.position.y, this.transform.position.z - num1 * 0.8f);
        this.m_ProjectorTransform.Translate(this.m_ProjectionOffset);
        if (!this.m_ContinuousRendering)
        {
          Quaternion rotation = this.transform.rotation;
          float num3 = (float) ((1.0 - (double) rotation.z) * 0.5 + 0.5);
          float num4 = rotation.x * 0.5f;
          this.m_Projector.aspectRatio = num3 - num4;
          this.m_Projector.orthographicSize = this.m_AdjustedShadowProjectorSize + num4;
          this.m_ProjectorTransform.rotation = Quaternion.identity;
          this.m_ProjectorTransform.Rotate(90f, rotation.eulerAngles.y, 0.0f);
        }
        else
        {
          this.m_ProjectorTransform.rotation = Quaternion.identity;
          this.m_ProjectorTransform.Rotate(90f, 0.0f, 0.0f);
          this.m_Projector.orthographicSize = this.m_AdjustedShadowProjectorSize;
        }
        int num5 = 64;
        if ((Object) this.m_ShadowTexture == (Object) null)
        {
          this.m_ShadowTexture = new RenderTexture(num5, num5, 32);
          this.m_ShadowTexture.wrapMode = TextureWrapMode.Clamp;
          this.RenderShadowMask();
        }
        else
        {
          if (!this.m_ContinuousRendering && this.m_ShadowTexture.IsCreated())
            return;
          this.RenderShadowMask();
        }
      }
    }
  }

  public static void SetShadowColor(Color color)
  {
    ProjectedShadow.s_ShadowColor = color;
  }

  public void EnableShadow()
  {
    this.m_ShadowEnabled = true;
  }

  public void EnableShadow(float FadeInTime)
  {
    this.m_ShadowEnabled = true;
    Hashtable args = iTween.Hash((object) "from", (object) 0, (object) "to", (object) 1, (object) "time", (object) FadeInTime, (object) "easetype", (object) iTween.EaseType.easeInCubic, (object) "onupdate", (object) "UpdateShadowColor", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "ProjectedShadowFade");
    iTween.StopByName(this.gameObject, "ProjectedShadowFade");
    iTween.ValueTo(this.gameObject, args);
  }

  public void DisableShadow()
  {
    this.DisableShadowProjector();
  }

  public void DisableShadow(float FadeOutTime)
  {
    if ((Object) this.m_Projector == (Object) null || !this.m_ShadowEnabled)
      return;
    Hashtable args = iTween.Hash((object) "from", (object) 1, (object) "to", (object) 0, (object) "time", (object) FadeOutTime, (object) "easetype", (object) iTween.EaseType.easeOutCubic, (object) "onupdate", (object) "UpdateShadowColor", (object) "onupdatetarget", (object) this.gameObject, (object) "name", (object) "ProjectedShadowFade", (object) "oncomplete", (object) "DisableShadowProjector");
    iTween.StopByName(this.gameObject, "ProjectedShadowFade");
    iTween.ValueTo(this.gameObject, args);
  }

  public void UpdateContactShadow(Spell spell, SpellStateType prevStateType, object userData)
  {
    this.UpdateContactShadow();
  }

  public void UpdateContactShadow(Spell spell, object userData)
  {
    this.UpdateContactShadow();
  }

  public void UpdateContactShadow(Spell spell)
  {
    this.UpdateContactShadow();
  }

  public void UpdateContactShadow()
  {
    if (!this.m_ContactShadow)
      return;
    this.m_isDirtyContactShadow = true;
  }

  private void DisableShadowProjector()
  {
    if ((Object) this.m_Projector != (Object) null)
      this.m_Projector.enabled = false;
    this.m_ShadowEnabled = false;
  }

  private void UpdateShadowColor(float val)
  {
    if ((Object) this.m_Projector == (Object) null || (Object) this.m_Projector.material == (Object) null)
      return;
    this.m_Projector.material.SetColor("_Color", Color.Lerp(new Color(0.5f, 0.5f, 0.5f, 0.5f), ProjectedShadow.s_ShadowColor, val));
  }

  private void RenderShadowMask()
  {
    this.m_ShadowTexture.DiscardContents();
    this.m_Camera.depth = Camera.main.depth - 3f;
    this.m_Camera.clearFlags = CameraClearFlags.Color;
    Vector3 position = this.transform.position;
    Vector3 localScale = this.transform.localScale;
    ProjectedShadow.s_offset -= 10f;
    if ((double) ProjectedShadow.s_offset < -19000.0)
      ProjectedShadow.s_offset = -12000f;
    Vector3 vector3 = Vector3.left * ProjectedShadow.s_offset;
    this.transform.position = vector3;
    this.m_Camera.transform.position = vector3;
    this.m_Camera.transform.rotation = Quaternion.identity;
    this.m_Camera.transform.Rotate(90f, 0.0f, 0.0f);
    RenderTexture temporary1 = RenderTexture.GetTemporary(80, 80);
    RenderTexture temporary2 = RenderTexture.GetTemporary(80, 80);
    this.m_Camera.targetTexture = temporary1;
    this.m_Camera.orthographicSize = (float) ((double) this.m_ShadowProjectorSize * (double) TransformUtil.ComputeWorldScale((Component) this.transform).x - 0.109999999403954 - 0.0500000007450581);
    this.m_Camera.RenderWithShader(this.m_UnlitWhiteShader, "Highlight");
    this.Sample(temporary1, temporary2, 0.6f);
    this.Sample(temporary2, this.m_ShadowTexture, 0.8f);
    this.ShadowMaterial.SetTexture("_MainTex", (Texture) this.m_ShadowTexture);
    this.ShadowMaterial.SetColor("_Color", ProjectedShadow.s_ShadowColor);
    this.transform.position = position;
    this.transform.localScale = localScale;
    RenderTexture.ReleaseTemporary(temporary1);
    RenderTexture.ReleaseTemporary(temporary2);
  }

  [DebuggerHidden]
  private IEnumerator DelayRenderContactShadow()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ProjectedShadow.\u003CDelayRenderContactShadow\u003Ec__Iterator33B() { \u003C\u003Ef__this = this };
  }

  private void RenderContactShadow()
  {
    GraphicsManager graphicsManager = GraphicsManager.Get();
    if ((bool) ((Object) graphicsManager) && graphicsManager.RealtimeShadows)
      this.enabled = false;
    if ((Object) this.m_ContactShadowTexture != (Object) null && !this.m_isDirtyContactShadow && this.m_ContactShadowTexture.IsCreated())
      return;
    this.m_AdjustedShadowProjectorSize = this.m_ShadowProjectorSize * TransformUtil.ComputeWorldScale((Component) this.transform).x;
    if ((Object) this.m_Camera == (Object) null)
      this.CreateCamera();
    if ((Object) this.m_PlaneGameObject == (Object) null)
      this.CreateRenderPlane();
    this.m_PlaneGameObject.SetActive(true);
    if ((Object) this.m_ContactShadowTexture == (Object) null)
      this.m_ContactShadowTexture = new RenderTexture(80, 80, 32);
    Quaternion localRotation = this.transform.localRotation;
    Vector3 position = this.transform.position;
    Vector3 localScale = this.transform.localScale;
    ProjectedShadow.s_offset -= 10f;
    if ((double) ProjectedShadow.s_offset < -19000.0)
      ProjectedShadow.s_offset = -12000f;
    Vector3 vector3 = Vector3.left * ProjectedShadow.s_offset;
    this.transform.position = vector3;
    this.transform.rotation = Quaternion.identity;
    this.SetWorldScale(this.transform, Vector3.one);
    this.m_Camera.transform.position = vector3;
    this.m_Camera.transform.rotation = Quaternion.identity;
    this.m_Camera.transform.Rotate(90f, 0.0f, 0.0f);
    RenderTexture temporary = RenderTexture.GetTemporary(80, 80);
    this.m_Camera.depth = Camera.main.depth - 3f;
    this.m_Camera.clearFlags = CameraClearFlags.Color;
    this.m_Camera.targetTexture = temporary;
    this.m_Camera.orthographicSize = (float) ((double) this.m_ShadowProjectorSize - 0.109999999403954 - 0.0500000007450581);
    this.m_Camera.RenderWithShader(this.m_UnlitDarkGreyShader, "Highlight");
    this.m_ContactShadowTexture.DiscardContents();
    this.Sample(temporary, this.m_ContactShadowTexture, 0.6f);
    this.transform.localRotation = localRotation;
    this.transform.position = position;
    this.transform.localScale = localScale;
    this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial.mainTexture = (Texture) this.m_ContactShadowTexture;
    this.m_isDirtyContactShadow = false;
    RenderTexture.ReleaseTemporary(temporary);
  }

  private void Sample(RenderTexture source, RenderTexture dest, float off)
  {
    Graphics.BlitMultiTap((Texture) source, dest, this.MultiSampleMaterial, new Vector2(-off, -off), new Vector2(-off, off), new Vector2(off, off), new Vector2(off, -off));
  }

  private void CreateProjector()
  {
    if ((Object) this.m_Projector != (Object) null)
    {
      Object.Destroy((Object) this.m_Projector);
      this.m_Projector = (Projector) null;
    }
    if ((Object) this.m_ProjectorGameObject != (Object) null)
    {
      Object.Destroy((Object) this.m_ProjectorGameObject);
      this.m_ProjectorGameObject = (GameObject) null;
      this.m_ProjectorTransform = (Transform) null;
    }
    this.m_ProjectorGameObject = new GameObject(string.Format("{0}_{1}", (object) this.name, (object) "ShadowProjector"));
    this.m_Projector = this.m_ProjectorGameObject.AddComponent<Projector>();
    this.m_ProjectorTransform = this.m_ProjectorGameObject.transform;
    this.m_ProjectorTransform.Rotate(90f, 0.0f, 0.0f);
    if ((Object) this.m_RootObject != (Object) null)
      this.m_ProjectorTransform.parent = this.m_RootObject.transform;
    this.m_Projector.nearClipPlane = 0.0f;
    this.m_Projector.farClipPlane = this.m_ProjectionFarClip;
    this.m_Projector.orthographic = true;
    this.m_Projector.orthographicSize = this.m_AdjustedShadowProjectorSize;
    SceneUtils.SetHideFlags((Object) this.m_Projector, HideFlags.HideAndDontSave);
    this.m_Projector.material = this.m_ShadowMaterial;
  }

  private void CreateCamera()
  {
    if ((Object) this.m_Camera != (Object) null)
      Object.Destroy((Object) this.m_Camera);
    GameObject gameObject = new GameObject();
    this.m_Camera = gameObject.AddComponent<Camera>();
    gameObject.name = this.name + "_ShadowCamera";
    SceneUtils.SetHideFlags((Object) gameObject, HideFlags.HideAndDontSave);
    this.m_Camera.orthographic = true;
    this.m_Camera.orthographicSize = this.m_AdjustedShadowProjectorSize;
    this.m_Camera.transform.position = this.transform.position;
    this.m_Camera.transform.rotation = this.transform.rotation;
    this.m_Camera.transform.Rotate(90f, 0.0f, 0.0f);
    if ((Object) this.m_RootObject != (Object) null)
      this.m_Camera.transform.parent = this.m_RootObject.transform;
    this.m_Camera.nearClipPlane = -3f;
    this.m_Camera.farClipPlane = 3f;
    this.m_Camera.depth = !((Object) Camera.main != (Object) null) ? -4f : Camera.main.depth - 5f;
    this.m_Camera.backgroundColor = Color.black;
    this.m_Camera.clearFlags = CameraClearFlags.Color;
    this.m_Camera.depthTextureMode = DepthTextureMode.None;
    this.m_Camera.renderingPath = RenderingPath.Forward;
    this.m_Camera.SetReplacementShader(this.m_UnlitWhiteShader, "Highlight");
    this.m_Camera.enabled = false;
  }

  private void CreateRenderPlane()
  {
    if ((Object) this.m_PlaneGameObject != (Object) null)
      Object.DestroyImmediate((Object) this.m_PlaneGameObject);
    this.m_PlaneGameObject = new GameObject();
    this.m_PlaneGameObject.name = this.name + "_ContactShadowRenderPlane";
    if ((Object) this.m_RootObject != (Object) null)
      this.m_PlaneGameObject.transform.parent = this.m_RootObject.transform;
    this.m_PlaneGameObject.transform.localPosition = this.m_ContactOffset;
    this.m_PlaneGameObject.transform.localRotation = Quaternion.identity;
    this.m_PlaneGameObject.transform.localScale = new Vector3(0.98f, 1f, 0.98f);
    this.m_PlaneGameObject.AddComponent<MeshFilter>();
    this.m_PlaneGameObject.AddComponent<MeshRenderer>();
    SceneUtils.SetHideFlags((Object) this.m_PlaneGameObject, HideFlags.HideAndDontSave);
    Mesh mesh1 = new Mesh();
    mesh1.name = "ContactShadowMeshPlane";
    float shadowProjectorSize1 = this.m_ShadowProjectorSize;
    float shadowProjectorSize2 = this.m_ShadowProjectorSize;
    mesh1.vertices = new Vector3[4]
    {
      new Vector3(-shadowProjectorSize1, 0.0f, -shadowProjectorSize2),
      new Vector3(shadowProjectorSize1, 0.0f, -shadowProjectorSize2),
      new Vector3(-shadowProjectorSize1, 0.0f, shadowProjectorSize2),
      new Vector3(shadowProjectorSize1, 0.0f, shadowProjectorSize2)
    };
    mesh1.uv = this.PLANE_UVS;
    mesh1.normals = this.PLANE_NORMALS;
    mesh1.triangles = this.PLANE_TRIANGLES;
    Mesh mesh2 = mesh1;
    this.m_PlaneGameObject.GetComponent<MeshFilter>().mesh = mesh2;
    this.m_PlaneMesh = mesh2;
    this.m_PlaneMesh.RecalculateBounds();
    this.m_ContactShadowMaterial = this.ContactShadowMaterial;
    this.m_ContactShadowMaterial.color = ProjectedShadow.s_ShadowColor;
    if (!(bool) ((Object) this.m_ContactShadowMaterial))
      return;
    this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial = this.m_ContactShadowMaterial;
  }

  public void SetWorldScale(Transform xform, Vector3 scale)
  {
    GameObject gameObject = new GameObject();
    Transform transform = gameObject.transform;
    transform.parent = (Transform) null;
    transform.localRotation = Quaternion.identity;
    transform.localScale = Vector3.one;
    Transform parent = xform.parent;
    xform.parent = transform;
    xform.localScale = scale;
    xform.parent = parent;
    Object.Destroy((Object) gameObject);
  }
}
