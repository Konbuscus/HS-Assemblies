﻿// Decompiled with JetBrains decompiler
// Type: UIBHighlightStateControl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class UIBHighlightStateControl : MonoBehaviour
{
  [CustomEditField(Sections = "Highlight State Type")]
  public ActorStateType m_MouseOverStateType = ActorStateType.HIGHLIGHT_MOUSE_OVER;
  [CustomEditField(Sections = "Highlight State Type")]
  public ActorStateType m_PrimarySelectedStateType = ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE;
  [CustomEditField(Sections = "Highlight State Type")]
  public ActorStateType m_SecondarySelectedStateType = ActorStateType.HIGHLIGHT_SECONDARY_ACTIVE;
  [CustomEditField(Sections = "Behavior Settings")]
  public bool m_EnableResponse = true;
  [CustomEditField(Sections = "Highlight State Reference")]
  public HighlightState m_HighlightState;
  [CustomEditField(Sections = "Behavior Settings")]
  public bool m_UseMouseOver;
  [CustomEditField(Sections = "Behavior Settings")]
  public bool m_AllowSelection;
  private PegUIElement m_PegUIElement;
  private bool m_MouseOver;

  private void Awake()
  {
    PegUIElement component = this.gameObject.GetComponent<PegUIElement>();
    if (!((Object) component != (Object) null))
      return;
    component.AddEventListener(UIEventType.ROLLOVER, (UIEvent.Handler) (e =>
    {
      if (!this.m_EnableResponse)
        return;
      this.OnRollOver();
    }));
    component.AddEventListener(UIEventType.ROLLOUT, (UIEvent.Handler) (e =>
    {
      if (!this.m_EnableResponse)
        return;
      this.OnRollOut();
    }));
    component.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e =>
    {
      if (!this.m_EnableResponse)
        return;
      this.OnRelease();
    }));
  }

  public void Select(bool selected, bool primary = false)
  {
    if (selected)
      this.m_HighlightState.ChangeState(!primary ? this.m_SecondarySelectedStateType : this.m_PrimarySelectedStateType);
    else if (this.m_MouseOver)
      this.m_HighlightState.ChangeState(this.m_MouseOverStateType);
    else
      this.m_HighlightState.ChangeState(ActorStateType.NONE);
  }

  public bool IsReady()
  {
    return this.m_HighlightState.IsReady();
  }

  private void OnRollOver()
  {
    if (!this.m_UseMouseOver)
      return;
    this.m_MouseOver = true;
    this.m_HighlightState.ChangeState(this.m_MouseOverStateType);
  }

  private void OnRollOut()
  {
    if (!this.m_UseMouseOver)
      return;
    this.m_MouseOver = false;
    if (this.m_AllowSelection)
      return;
    this.m_HighlightState.ChangeState(ActorStateType.NONE);
  }

  private void OnRelease()
  {
    if (this.m_AllowSelection)
      this.Select(true, false);
    else if (this.m_MouseOver)
      this.m_HighlightState.ChangeState(this.m_MouseOverStateType);
    else
      this.m_HighlightState.ChangeState(ActorStateType.NONE);
  }
}
