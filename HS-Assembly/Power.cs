﻿// Decompiled with JetBrains decompiler
// Type: Power
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;

public class Power
{
  private string mDefinition = string.Empty;
  private PlayErrors.PlayRequirementInfo mPlayRequirementInfo = new PlayErrors.PlayRequirementInfo();
  private static Power s_defaultAttackPower;
  private static Power s_defaultMasterPower;

  public string GetDefinition()
  {
    return this.mDefinition;
  }

  public PlayErrors.PlayRequirementInfo GetPlayRequirementInfo()
  {
    return this.mPlayRequirementInfo;
  }

  public static Power GetDefaultAttackPower()
  {
    if (Power.s_defaultAttackPower == null)
    {
      Power.s_defaultAttackPower = new Power();
      Power.s_defaultAttackPower.mPlayRequirementInfo.requirementsMap = PlayErrors.GetRequirementsMap(new List<PlayErrors.ErrorType>()
      {
        PlayErrors.ErrorType.REQ_TARGET_TO_PLAY,
        PlayErrors.ErrorType.REQ_ENEMY_TARGET
      });
    }
    return Power.s_defaultAttackPower;
  }

  public static Power GetDefaultMasterPower()
  {
    if (Power.s_defaultMasterPower == null)
    {
      Power.s_defaultMasterPower = new Power();
      List<PlayErrors.ErrorType> requirements = new List<PlayErrors.ErrorType>();
      Power.s_defaultMasterPower.mPlayRequirementInfo.requirementsMap = PlayErrors.GetRequirementsMap(requirements);
    }
    return Power.s_defaultMasterPower;
  }

  public static Power Create(string definition, List<Power.PowerInfo> infos)
  {
    Power power = new Power();
    power.mDefinition = definition;
    List<PlayErrors.ErrorType> requirements = new List<PlayErrors.ErrorType>();
    if (infos != null && infos.Count > 0)
    {
      using (List<Power.PowerInfo>.Enumerator enumerator = infos.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Power.PowerInfo current = enumerator.Current;
          PlayErrors.ErrorType reqId = current.reqId;
          int num = current.param;
          PlayErrors.ErrorType errorType = reqId;
          switch (errorType)
          {
            case PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_MINIONS:
              power.mPlayRequirementInfo.paramMinNumFriendlyMinions = num;
              break;
            case PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_SECRETS:
              power.mPlayRequirementInfo.paramMinNumFriendlySecrets = num;
              break;
            case PlayErrors.ErrorType.REQ_TARGET_EXACT_COST:
              power.mPlayRequirementInfo.paramExactCost = num;
              break;
            default:
              switch (errorType - 8)
              {
                case PlayErrors.ErrorType.NONE:
                  power.mPlayRequirementInfo.paramMaxAtk = num;
                  break;
                case PlayErrors.ErrorType.REQ_FRIENDLY_TARGET:
                  power.mPlayRequirementInfo.paramRace = num;
                  break;
                case PlayErrors.ErrorType.REQ_DAMAGED_TARGET:
                  power.mPlayRequirementInfo.paramNumMinionSlots = num;
                  break;
                default:
                  if (errorType != PlayErrors.ErrorType.REQ_MINION_CAP_IF_TARGET_AVAILABLE)
                  {
                    if (errorType != PlayErrors.ErrorType.REQ_MINIMUM_ENEMY_MINIONS)
                    {
                      if (errorType != PlayErrors.ErrorType.REQ_TARGET_MIN_ATTACK)
                      {
                        if (errorType == PlayErrors.ErrorType.REQ_MINIMUM_TOTAL_MINIONS)
                        {
                          power.mPlayRequirementInfo.paramMinNumTotalMinions = num;
                          break;
                        }
                        break;
                      }
                      power.mPlayRequirementInfo.paramMinAtk = num;
                      break;
                    }
                    power.mPlayRequirementInfo.paramMinNumEnemyMinions = num;
                    break;
                  }
                  power.mPlayRequirementInfo.paramNumMinionSlotsWithTarget = num;
                  break;
              }
          }
          requirements.Add(reqId);
        }
      }
    }
    power.mPlayRequirementInfo.requirementsMap = PlayErrors.GetRequirementsMap(requirements);
    return power;
  }

  public static Power LoadFromXml(XmlElement rootElement)
  {
    Power power = new Power();
    power.mDefinition = rootElement.GetAttribute("definition");
    XPathNavigator navigator = rootElement.CreateNavigator();
    XPathExpression expr = navigator.Compile("./PlayRequirement");
    XPathNodeIterator xpathNodeIterator = navigator.Select(expr);
    List<PlayErrors.ErrorType> requirements = new List<PlayErrors.ErrorType>();
    while (xpathNodeIterator.MoveNext())
    {
      XmlElement node = (XmlElement) ((IHasXmlNode) xpathNodeIterator.Current).GetNode();
      int result;
      if (int.TryParse(node.GetAttribute("reqID"), out result))
      {
        PlayErrors.ErrorType errorType = (PlayErrors.ErrorType) result;
        requirements.Add(errorType);
        if (errorType == PlayErrors.ErrorType.REQ_TARGET_MIN_ATTACK)
        {
          if (!int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramMinAtk))
            Log.Rachelle.Print(string.Format("Unable to read play requirement param minAtk for power {0}.", (object) power.GetDefinition()));
        }
        else if (errorType == PlayErrors.ErrorType.REQ_TARGET_MAX_ATTACK)
        {
          if (!int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramMaxAtk))
            Log.Rachelle.Print(string.Format("Unable to read play requirement param maxAtk for power {0}.", (object) power.GetDefinition()));
        }
        else if (errorType == PlayErrors.ErrorType.REQ_TARGET_EXACT_COST)
        {
          if (!int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramExactCost))
            Log.Rachelle.Print(string.Format("Unable to read play requirement param exactCost for power {0}.", (object) power.GetDefinition()));
        }
        else if (errorType == PlayErrors.ErrorType.REQ_TARGET_WITH_RACE)
        {
          if (!int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramRace))
            Log.Rachelle.Print(string.Format("Unable to read play requirement param race for power {0}.", (object) power.GetDefinition()));
        }
        else if (errorType == PlayErrors.ErrorType.REQ_NUM_MINION_SLOTS)
        {
          if (!int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramNumMinionSlots))
            Log.Rachelle.Print(string.Format("Unable to read play requirement param num minion slots for power {0}.", (object) power.GetDefinition()));
        }
        else if (errorType == PlayErrors.ErrorType.REQ_MINION_CAP_IF_TARGET_AVAILABLE)
        {
          if (!int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramNumMinionSlotsWithTarget))
            Log.Rachelle.Print(string.Format("Unable to read play requirement param num minion slots with target for power {0}.", (object) power.GetDefinition()));
        }
        else if (errorType == PlayErrors.ErrorType.REQ_MINIMUM_ENEMY_MINIONS)
        {
          if (!int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramMinNumEnemyMinions))
            Log.Rachelle.Print(string.Format("Unable to read play requirement param num enemy minions for power {0}.", (object) power.GetDefinition()));
        }
        else if (errorType == PlayErrors.ErrorType.REQ_MINIMUM_TOTAL_MINIONS)
        {
          if (!int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramMinNumTotalMinions))
            Log.Rachelle.Print(string.Format("Unable to read play requirement param num total minions for power {0}.", (object) power.GetDefinition()));
        }
        else if (errorType == PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_MINIONS)
        {
          if (!int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramMinNumFriendlyMinions))
            Log.Rachelle.Print(string.Format("Unable to read play requirement param num friendly minions for power {0}.", (object) power.GetDefinition()));
        }
        else if (errorType == PlayErrors.ErrorType.REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_SECRETS && !int.TryParse(node.GetAttribute("param"), out power.mPlayRequirementInfo.paramMinNumFriendlySecrets))
          Log.Rachelle.Print(string.Format("Unable to read play requirement param num friendly secrets for power {0}.", (object) power.GetDefinition()));
      }
    }
    power.mPlayRequirementInfo.requirementsMap = PlayErrors.GetRequirementsMap(requirements);
    return power;
  }

  public struct PowerInfo
  {
    public PlayErrors.ErrorType reqId;
    public int param;
  }
}
