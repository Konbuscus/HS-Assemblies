﻿// Decompiled with JetBrains decompiler
// Type: AccountLicenseMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class AccountLicenseMgr
{
  private List<AccountLicenseMgr.AccountLicensesChangedListener> m_accountLicensesChangedListeners = new List<AccountLicenseMgr.AccountLicensesChangedListener>();
  private static AccountLicenseMgr s_instance;
  private Map<long, long> m_seenLicenseNotices;
  private AccountLicenseMgr.LicenseUpdateState m_consumableLicensesUpdateState;
  private AccountLicenseMgr.LicenseUpdateState m_fixedLicensesUpdateState;

  public static AccountLicenseMgr Get()
  {
    return AccountLicenseMgr.s_instance;
  }

  public static void Init()
  {
    if (AccountLicenseMgr.s_instance != null)
      return;
    AccountLicenseMgr.s_instance = new AccountLicenseMgr();
    Network.Get().RegisterNetHandler((object) UpdateAccountLicensesResponse.PacketID.ID, new Network.NetHandler(AccountLicenseMgr.s_instance.OnAccountLicensesUpdatedResponse), (Network.TimeoutHandler) null);
    NetCache.Get().RegisterNewNoticesListener(new NetCache.DelNewNoticesListener(AccountLicenseMgr.s_instance.OnNewNotices));
    ApplicationMgr.Get().WillReset += new Action(AccountLicenseMgr.s_instance.WillReset);
  }

  public void InitRequests()
  {
    Network.RequestAccountLicensesUpdate();
  }

  public bool OwnsAccountLicense(long license)
  {
    NetCache.NetCacheAccountLicenses netObject = NetCache.Get().GetNetObject<NetCache.NetCacheAccountLicenses>();
    if (netObject == null || !netObject.AccountLicenses.ContainsKey(license))
      return false;
    return this.OwnsAccountLicense(netObject.AccountLicenses[license]);
  }

  public bool OwnsAccountLicense(AccountLicenseInfo accountLicenseInfo)
  {
    if (accountLicenseInfo == null)
      return false;
    return ((long) accountLicenseInfo.Flags_ & 1L) == 1L;
  }

  public List<AccountLicenseInfo> GetAllOwnedAccountLicenseInfo()
  {
    List<AccountLicenseInfo> accountLicenseInfoList = new List<AccountLicenseInfo>();
    NetCache.NetCacheAccountLicenses netObject = NetCache.Get().GetNetObject<NetCache.NetCacheAccountLicenses>();
    if (netObject != null)
    {
      using (Map<long, AccountLicenseInfo>.ValueCollection.Enumerator enumerator = netObject.AccountLicenses.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AccountLicenseInfo current = enumerator.Current;
          if (this.OwnsAccountLicense(current))
            accountLicenseInfoList.Add(current);
        }
      }
    }
    return accountLicenseInfoList;
  }

  public bool RegisterAccountLicensesChangedListener(AccountLicenseMgr.AccountLicensesChangedCallback callback)
  {
    return this.RegisterAccountLicensesChangedListener(callback, (object) null);
  }

  public bool RegisterAccountLicensesChangedListener(AccountLicenseMgr.AccountLicensesChangedCallback callback, object userData)
  {
    AccountLicenseMgr.AccountLicensesChangedListener licensesChangedListener = new AccountLicenseMgr.AccountLicensesChangedListener();
    licensesChangedListener.SetCallback(callback);
    licensesChangedListener.SetUserData(userData);
    if (this.m_accountLicensesChangedListeners.Contains(licensesChangedListener))
      return false;
    this.m_accountLicensesChangedListeners.Add(licensesChangedListener);
    return true;
  }

  public bool RemoveAccountLicensesChangedListener(AccountLicenseMgr.AccountLicensesChangedCallback callback)
  {
    return this.RemoveAccountLicensesChangedListener(callback, (object) null);
  }

  public bool RemoveAccountLicensesChangedListener(AccountLicenseMgr.AccountLicensesChangedCallback callback, object userData)
  {
    AccountLicenseMgr.AccountLicensesChangedListener licensesChangedListener = new AccountLicenseMgr.AccountLicensesChangedListener();
    licensesChangedListener.SetCallback(callback);
    licensesChangedListener.SetUserData(userData);
    return this.m_accountLicensesChangedListeners.Remove(licensesChangedListener);
  }

  private void WillReset()
  {
    if (this.m_seenLicenseNotices != null)
      this.m_seenLicenseNotices.Clear();
    this.m_consumableLicensesUpdateState = AccountLicenseMgr.LicenseUpdateState.UNKNOWN;
    this.m_fixedLicensesUpdateState = AccountLicenseMgr.LicenseUpdateState.UNKNOWN;
  }

  private void OnAccountLicensesUpdatedResponse()
  {
    UpdateAccountLicensesResponse licensesResponse = Network.GetUpdateAccountLicensesResponse();
    this.m_consumableLicensesUpdateState = !licensesResponse.ConsumableLicenseSuccess ? AccountLicenseMgr.LicenseUpdateState.FAIL : AccountLicenseMgr.LicenseUpdateState.SUCCESS;
    this.m_fixedLicensesUpdateState = !licensesResponse.FixedLicenseSuccess ? AccountLicenseMgr.LicenseUpdateState.FAIL : AccountLicenseMgr.LicenseUpdateState.SUCCESS;
    Log.Rachelle.Print("OnAccountLicensesUpdatedResponse consumableLicensesUpdateState={0} fixedLicensesUpdateState={1}", new object[2]
    {
      (object) this.m_consumableLicensesUpdateState,
      (object) this.m_fixedLicensesUpdateState
    });
    if (this.m_consumableLicensesUpdateState == AccountLicenseMgr.LicenseUpdateState.SUCCESS && this.m_fixedLicensesUpdateState == AccountLicenseMgr.LicenseUpdateState.SUCCESS)
      return;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_ERROR_GENERIC_HEADER"),
      m_text = GameStrings.Get("GLOBAL_ERROR_ACCOUNT_LICENSES"),
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  private void OnNewNotices(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList)
  {
    NetCache.NetCacheAccountLicenses netObject = NetCache.Get().GetNetObject<NetCache.NetCacheAccountLicenses>();
    if (netObject == null)
      ApplicationMgr.Get().StartCoroutine(this.OnNewNotices_WaitForNetCacheAccountLicenses(newNotices));
    else
      this.OnNewNotices_Internal(newNotices, netObject);
  }

  [DebuggerHidden]
  private IEnumerator OnNewNotices_WaitForNetCacheAccountLicenses(List<NetCache.ProfileNotice> newNotices)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AccountLicenseMgr.\u003COnNewNotices_WaitForNetCacheAccountLicenses\u003Ec__Iterator2D7() { newNotices = newNotices, \u003C\u0024\u003EnewNotices = newNotices, \u003C\u003Ef__this = this };
  }

  private void OnNewNotices_Internal(List<NetCache.ProfileNotice> newNotices, NetCache.NetCacheAccountLicenses netCacheAccountLicenses)
  {
    if (netCacheAccountLicenses == null)
      UnityEngine.Debug.LogWarning((object) "AccountLicenses.OnNewNotices netCacheAccountLicenses is null -- going to ack all ACCOUNT_LICENSE notices assuming NetCache is not yet loaded");
    HashSet<long> longSet = new HashSet<long>();
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = newNotices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.ProfileNotice current = enumerator.Current;
        if (current.Type == NetCache.ProfileNotice.NoticeType.ACCOUNT_LICENSE)
        {
          NetCache.ProfileNoticeAcccountLicense noticeAcccountLicense = current as NetCache.ProfileNoticeAcccountLicense;
          if (netCacheAccountLicenses != null)
          {
            if (!netCacheAccountLicenses.AccountLicenses.ContainsKey(noticeAcccountLicense.License))
              netCacheAccountLicenses.AccountLicenses[noticeAcccountLicense.License] = new AccountLicenseInfo()
              {
                License = noticeAcccountLicense.License,
                Flags_ = 0UL,
                CasId = 0L
              };
            if (noticeAcccountLicense.CasID >= netCacheAccountLicenses.AccountLicenses[noticeAcccountLicense.License].CasId)
            {
              netCacheAccountLicenses.AccountLicenses[noticeAcccountLicense.License].CasId = noticeAcccountLicense.CasID;
              if (current.Origin == NetCache.ProfileNotice.NoticeOrigin.ACCOUNT_LICENSE_FLAGS)
                netCacheAccountLicenses.AccountLicenses[noticeAcccountLicense.License].Flags_ = (ulong) current.OriginData;
              else
                UnityEngine.Debug.LogWarning((object) string.Format("AccountLicenses.OnNewNotices unexpected notice origin {0} (data={1}) for license {2} casID {3}", (object) current.Origin, (object) current.OriginData, (object) noticeAcccountLicense.License, (object) noticeAcccountLicense.CasID));
              long num = noticeAcccountLicense.CasID - 1L;
              if (this.m_seenLicenseNotices != null)
                this.m_seenLicenseNotices.TryGetValue(noticeAcccountLicense.License, out num);
              if (num < noticeAcccountLicense.CasID)
                longSet.Add(noticeAcccountLicense.License);
              if (this.m_seenLicenseNotices == null)
                this.m_seenLicenseNotices = new Map<long, long>();
              this.m_seenLicenseNotices[noticeAcccountLicense.License] = noticeAcccountLicense.CasID;
            }
          }
          Network.AckNotice(current.NoticeID);
        }
      }
    }
    if (netCacheAccountLicenses == null)
      return;
    List<AccountLicenseInfo> changedLicensesInfo = new List<AccountLicenseInfo>();
    using (HashSet<long>.Enumerator enumerator = longSet.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        long current = enumerator.Current;
        if (netCacheAccountLicenses.AccountLicenses.ContainsKey(current))
          changedLicensesInfo.Add(netCacheAccountLicenses.AccountLicenses[current]);
      }
    }
    if (changedLicensesInfo.Count == 0)
      return;
    foreach (AccountLicenseMgr.AccountLicensesChangedListener licensesChangedListener in this.m_accountLicensesChangedListeners.ToArray())
      licensesChangedListener.Fire(changedLicensesInfo);
  }

  private enum LicenseUpdateState
  {
    UNKNOWN,
    SUCCESS,
    FAIL,
  }

  private class AccountLicensesChangedListener : EventListener<AccountLicenseMgr.AccountLicensesChangedCallback>
  {
    public void Fire(List<AccountLicenseInfo> changedLicensesInfo)
    {
      this.m_callback(changedLicensesInfo, this.m_userData);
    }
  }

  public delegate void AccountLicensesChangedCallback(List<AccountLicenseInfo> changedLicensesInfo, object userData);
}
