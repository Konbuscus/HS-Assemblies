﻿// Decompiled with JetBrains decompiler
// Type: BnetInvitationId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class BnetInvitationId
{
  private ulong m_val;

  public BnetInvitationId(ulong val)
  {
    this.m_val = val;
  }

  public static bool operator ==(BnetInvitationId a, BnetInvitationId b)
  {
    if (object.ReferenceEquals((object) a, (object) b))
      return true;
    if ((object) a == null || (object) b == null)
      return false;
    return (long) a.m_val == (long) b.m_val;
  }

  public static bool operator !=(BnetInvitationId a, BnetInvitationId b)
  {
    return !(a == b);
  }

  public ulong GetVal()
  {
    return this.m_val;
  }

  public void SetVal(ulong val)
  {
    this.m_val = val;
  }

  public override bool Equals(object obj)
  {
    if (obj == null)
      return false;
    BnetInvitationId bnetInvitationId = obj as BnetInvitationId;
    if ((object) bnetInvitationId == null)
      return false;
    return (long) this.m_val == (long) bnetInvitationId.m_val;
  }

  public bool Equals(BnetInvitationId other)
  {
    if ((object) other == null)
      return false;
    return (long) this.m_val == (long) other.m_val;
  }

  public override int GetHashCode()
  {
    return this.m_val.GetHashCode();
  }

  public override string ToString()
  {
    return this.m_val.ToString();
  }
}
