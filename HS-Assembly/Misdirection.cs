﻿// Decompiled with JetBrains decompiler
// Type: Misdirection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[RequireComponent(typeof (Animation))]
public class Misdirection : Spell
{
  public float m_ReticleFadeInTime = 0.8f;
  public float m_ReticleFadeOutTime = 0.4f;
  public float m_ReticlePathTime = 3f;
  public float m_ReticleBlur = 0.005f;
  public float m_ReticleBlurFocusTime = 0.8f;
  public Color m_ReticleAttackColor = Color.red;
  public float m_ReticleAttackScale = 1.1f;
  public float m_ReticleAttackTime = 0.3f;
  public Vector3 m_ReticleAttackRotate = new Vector3(0.0f, 90f, 0.0f);
  public float m_ReticleAttackHold = 0.25f;
  public int m_ReticlePathDesiredMinimumTargets = 3;
  public int m_ReticlePathDesiredMaximumTargets = 4;
  public GameObject m_Reticle;
  public bool m_AllowTargetingInitialTarget;
  private GameObject m_ReticleInstance;
  private Card m_AttackingEntityCard;
  private Card m_InitialTargetCard;
  private Color m_OrgAmbient;

  public override bool AddPowerTargets()
  {
    if (!this.CanAddPowerTargets())
      return false;
    this.AddMultiplePowerTargets();
    return true;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    this.StartAnimation();
  }

  private void ResolveTargets()
  {
    List<GameObject> targets = this.GetTargets();
    this.m_AttackingEntityCard = targets[1].GetComponent<Card>();
    GameState gameState = GameState.Get();
    GameEntity gameEntity = gameState.GetGameEntity();
    Entity entity1 = gameState.GetEntity(gameEntity.GetTag(GAME_TAG.PROPOSED_DEFENDER));
    if (entity1 != null)
    {
      this.m_InitialTargetCard = entity1.GetCard();
    }
    else
    {
      Entity entity2 = gameState.GetEntity(this.m_AttackingEntityCard.GetEntity().GetTag(GAME_TAG.CARD_TARGET));
      if (entity2 != null)
        this.m_InitialTargetCard = entity2.GetCard();
      else
        this.m_InitialTargetCard = targets[2].GetComponent<Card>();
    }
  }

  private void StartAnimation()
  {
    this.ResolveTargets();
    this.m_ReticleInstance = (GameObject) Object.Instantiate((Object) this.m_Reticle, this.m_InitialTargetCard.transform.position, Quaternion.identity);
    Material renderMaterial = this.m_ReticleInstance.GetComponent<RenderToTexture>().GetRenderMaterial();
    renderMaterial.SetFloat("_Alpha", 0.0f);
    renderMaterial.SetFloat("_blur", this.m_ReticleBlur);
    this.StartCoroutine(this.ReticleFadeIn());
    this.StartCoroutine(this.AnimateReticle());
    AudioSource component = this.GetComponent<AudioSource>();
    if (!((Object) component != (Object) null))
      return;
    SoundManager.Get().Play(component, true);
  }

  [DebuggerHidden]
  private IEnumerator ReticleFadeIn()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Misdirection.\u003CReticleFadeIn\u003Ec__Iterator2C1() { \u003C\u003Ef__this = this };
  }

  private void SetReticleAlphaValue(float val)
  {
    this.m_ReticleInstance.GetComponent<RenderToTexture>().GetRenderMaterial().SetFloat("_Alpha", val);
  }

  [DebuggerHidden]
  private IEnumerator AnimateReticle()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Misdirection.\u003CAnimateReticle\u003Ec__Iterator2C2() { \u003C\u003Ef__this = this };
  }

  private void ReticleAnimationComplete()
  {
    this.StartCoroutine(this.ReticleAttackAnimation());
  }

  [DebuggerHidden]
  private IEnumerator ReticleAttackAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Misdirection.\u003CReticleAttackAnimation\u003Ec__Iterator2C3() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator ReticleFadeOut()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Misdirection.\u003CReticleFadeOut\u003Ec__Iterator2C4() { \u003C\u003Ef__this = this };
  }

  private Vector3[] BuildAnimationPath()
  {
    Card[] possibleTargetCards = this.FindPossibleTargetCards();
    int num = Random.Range(this.m_ReticlePathDesiredMinimumTargets, this.m_ReticlePathDesiredMaximumTargets);
    if (num >= possibleTargetCards.Length + 2)
      num = possibleTargetCards.Length + 2;
    if (possibleTargetCards.Length <= 1)
      return new Vector3[2]{ this.m_InitialTargetCard.transform.position, this.GetTarget().transform.position };
    List<Vector3> vector3List = new List<Vector3>();
    vector3List.Add(this.m_InitialTargetCard.transform.position);
    GameObject gameObject1 = this.m_InitialTargetCard.gameObject;
    for (int index = 1; index < num; ++index)
    {
      GameObject gameObject2 = possibleTargetCards[Random.Range(0, possibleTargetCards.Length - 1)].gameObject;
      if ((Object) gameObject2 == (Object) gameObject1)
      {
        gameObject2 = possibleTargetCards[Random.Range(0, possibleTargetCards.Length - 1)].gameObject;
        if ((Object) gameObject2 == (Object) gameObject1)
          gameObject2 = !((Object) gameObject2 == (Object) possibleTargetCards[possibleTargetCards.Length - 1]) ? possibleTargetCards[possibleTargetCards.Length - 1].gameObject : possibleTargetCards[0].gameObject;
      }
      if (index == num - 1 && (Object) gameObject2 == (Object) this.GetTarget() && (Object) gameObject2 == (Object) gameObject1)
        gameObject2 = !((Object) gameObject2 == (Object) possibleTargetCards[possibleTargetCards.Length - 1]) ? possibleTargetCards[possibleTargetCards.Length - 1].gameObject : possibleTargetCards[0].gameObject;
      vector3List.Add(gameObject2.transform.position);
    }
    vector3List.Add(this.GetTarget().transform.position);
    return vector3List.ToArray();
  }

  private Card[] FindPossibleTargetCards()
  {
    List<Card> cardList = new List<Card>();
    ZoneMgr zoneMgr = ZoneMgr.Get();
    if ((Object) zoneMgr == (Object) null)
      return cardList.ToArray();
    using (List<ZonePlay>.Enumerator enumerator1 = zoneMgr.FindZonesOfType<ZonePlay>().GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Card>.Enumerator enumerator2 = enumerator1.Current.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Card current = enumerator2.Current;
            if (!((Object) current == (Object) this.m_AttackingEntityCard) && (!((Object) current == (Object) this.m_InitialTargetCard) || this.m_AllowTargetingInitialTarget))
              cardList.Add(current);
          }
        }
      }
    }
    using (List<ZoneHero>.Enumerator enumerator1 = zoneMgr.FindZonesOfType<ZoneHero>().GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Card>.Enumerator enumerator2 = enumerator1.Current.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Card current = enumerator2.Current;
            if (!((Object) current == (Object) this.m_AttackingEntityCard) && (!((Object) current == (Object) this.m_InitialTargetCard) || this.m_AllowTargetingInitialTarget))
              cardList.Add(current);
          }
        }
      }
    }
    return cardList.ToArray();
  }

  private Card[] GetOpponentZoneMinions()
  {
    List<Card> cardList = new List<Card>();
    using (List<Card>.Enumerator enumerator = GameState.Get().GetFirstOpponentPlayer(this.GetSourceCard().GetController()).GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (!((Object) current == (Object) this.m_AttackingEntityCard))
          cardList.Add(current);
      }
    }
    return cardList.ToArray();
  }

  private Card GetCurrentPlayerHeroCard()
  {
    return this.GetSourceCard().GetController().GetHeroCard();
  }

  private Card GetOpponentHeroCard()
  {
    return GameState.Get().GetFirstOpponentPlayer(this.GetSourceCard().GetController()).GetHeroCard();
  }

  private enum TargetingMetadata
  {
    DestinationTarget,
    AttackingEntity,
    InitialTarget,
  }
}
