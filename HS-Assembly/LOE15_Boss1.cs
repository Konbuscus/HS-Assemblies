﻿// Decompiled with JetBrains decompiler
// Type: LOE15_Boss1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LOE15_Boss1 : LOE_MissionEntity
{
  private List<Zone> m_zonesToHide = new List<Zone>();
  private bool m_magmaRagerLinePlayed;
  private bool m_lowHealth;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOE_15_RESPONSE");
    this.PreloadSound("VO_LOEA15_1_LOW_HEALTH_10");
    this.PreloadSound("VO_LOEA15_1_TURN1_08");
    this.PreloadSound("VO_LOEA15_1_MAGMA_RAGER_09");
    this.PreloadSound("VO_LOEA15_1_LOSS_11");
    this.PreloadSound("VO_LOEA15_1_WIN_12");
    this.PreloadSound("VO_LOEA15_GOLDEN");
    this.PreloadSound("VO_LOEA15_1_START_07");
    this.PreloadSound("VO_LOE_15_SPARE");
    this.PreloadSound("VO_ELISE_WEIRD_DECK_05");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_LOE_15_RESPONSE",
            m_stringTag = "VO_LOE_15_RESPONSE"
          }
        }
      }
    };
  }

  public override bool DoAlternateMulliganIntro()
  {
    GameState.Get().GetOpposingSidePlayer().GetDeckZone().SetVisibility(false);
    this.m_zonesToHide.Clear();
    this.m_zonesToHide.AddRange((IEnumerable<Zone>) ZoneMgr.Get().FindZonesForTag(TAG_ZONE.HAND));
    this.m_zonesToHide.AddRange((IEnumerable<Zone>) ZoneMgr.Get().FindZonesForTag(TAG_ZONE.DECK));
    using (List<Zone>.Enumerator enumerator1 = this.m_zonesToHide.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Zone current1 = enumerator1.Current;
        Log.JMac.Print("Number of cards in zone " + (object) current1 + ": " + (object) current1.GetCards().Count);
        using (List<Card>.Enumerator enumerator2 = current1.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Card current2 = enumerator2.Current;
            current2.HideCard();
            current2.SetDoNotSort(true);
          }
        }
      }
    }
    return false;
  }

  [DebuggerHidden]
  public override IEnumerator DoActionsAfterIntroBeforeMulligan()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE15_Boss1.\u003CDoActionsAfterIntroBeforeMulligan\u003Ec__Iterator17E() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE15_Boss1.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator17F() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE15_Boss1.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator180() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE15_Boss1.\u003CHandleGameOverWithTiming\u003Ec__Iterator181() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
