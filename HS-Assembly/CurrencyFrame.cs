﻿// Decompiled with JetBrains decompiler
// Type: CurrencyFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class CurrencyFrame : MonoBehaviour
{
  private CurrencyFrame.State m_state = CurrencyFrame.State.SHOWN;
  public UberText m_amount;
  public GameObject m_dustJar;
  public GameObject m_dustFX;
  public GameObject m_explodeFX_Common;
  public GameObject m_explodeFX_Rare;
  public GameObject m_explodeFX_Epic;
  public GameObject m_explodeFX_Legendary;
  public GameObject m_goldCoin;
  public GameObject m_background;
  public PegUIElement m_mouseOverZone;
  private CurrencyFrame.CurrencyType m_showingCurrency;
  private bool m_backgroundFaded;
  private CurrencyFrame.CurrencyType? m_overrideCurrencyType;

  private void Awake()
  {
    NetCache.Get().RegisterGoldBalanceListener(new NetCache.DelGoldBalanceListener(this.OnGoldBalanceChanged));
    this.m_mouseOverZone.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnFrameMouseOver));
    this.m_mouseOverZone.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnFrameMouseOut));
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
  }

  private void OnDestroy()
  {
    if (NetCache.Get() != null)
      NetCache.Get().RemoveGoldBalanceListener(new NetCache.DelGoldBalanceListener(this.OnGoldBalanceChanged));
    if (!((Object) SceneMgr.Get() != (Object) null))
      return;
    SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
  }

  public void DeactivateCurrencyFrame()
  {
    this.gameObject.SetActive(false);
    this.m_state = CurrencyFrame.State.HIDDEN;
  }

  public void RefreshContents()
  {
    CurrencyFrame.CurrencyType currencyToShow = this.GetCurrencyToShow();
    this.UpdateAmount(currencyToShow);
    this.Show(currencyToShow);
  }

  public void HideTemporarily()
  {
    iTween.FadeTo(this.gameObject, iTween.Hash((object) "amount", (object) 0.0f, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutCubic));
    this.m_showingCurrency = CurrencyFrame.CurrencyType.NONE;
  }

  public GameObject GetTooltipObject()
  {
    TooltipZone component = this.GetComponent<TooltipZone>();
    if ((Object) component != (Object) null)
      return component.GetTooltipObject();
    return (GameObject) null;
  }

  public void SetCurrencyOverride(CurrencyFrame.CurrencyType? type)
  {
    this.m_overrideCurrencyType = type;
    this.RefreshContents();
    BnetBar.Get().UpdateLayout();
  }

  private void ShowImmediate(CurrencyFrame.CurrencyType currencyType)
  {
    bool flag = currencyType != CurrencyFrame.CurrencyType.NONE;
    this.m_showingCurrency = currencyType;
    this.gameObject.SetActive(flag);
    iTween.Stop(this.gameObject, true);
    Renderer[] componentsInChildren = this.gameObject.GetComponentsInChildren<Renderer>();
    float a1 = !flag ? 0.0f : 1f;
    foreach (Renderer renderer in componentsInChildren)
      renderer.material.color = new Color(1f, 1f, 1f, a1);
    float a2 = currencyType != CurrencyFrame.CurrencyType.GOLD ? 0.0f : 1f;
    float a3 = currencyType != CurrencyFrame.CurrencyType.ARCANE_DUST ? 0.0f : 1f;
    this.m_goldCoin.GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f, a2);
    this.m_dustJar.GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f, a3);
    this.m_state = !flag ? CurrencyFrame.State.HIDDEN : CurrencyFrame.State.SHOWN;
  }

  private void Show(CurrencyFrame.CurrencyType currencyType)
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.ShowImmediate(currencyType);
    }
    else
    {
      bool flag = currencyType != CurrencyFrame.CurrencyType.NONE;
      if (!DemoMgr.Get().IsCurrencyEnabled())
        flag = false;
      if (flag)
      {
        if (this.m_state == CurrencyFrame.State.SHOWN || this.m_state == CurrencyFrame.State.ANIMATE_IN)
        {
          this.ShowCurrencyType(currencyType);
        }
        else
        {
          this.m_state = CurrencyFrame.State.ANIMATE_IN;
          this.gameObject.SetActive(true);
          Hashtable args = iTween.Hash((object) "amount", (object) 1f, (object) "delay", (object) 0.0f, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutCubic, (object) "oncomplete", (object) "ActivateCurrencyFrame", (object) "oncompletetarget", (object) this.gameObject);
          iTween.Stop(this.gameObject);
          iTween.FadeTo(this.gameObject, args);
          this.ShowCurrencyType(currencyType);
        }
      }
      else if (this.m_state == CurrencyFrame.State.HIDDEN || this.m_state == CurrencyFrame.State.ANIMATE_OUT)
      {
        this.ShowCurrencyType(currencyType);
      }
      else
      {
        this.m_state = CurrencyFrame.State.ANIMATE_OUT;
        Hashtable args = iTween.Hash((object) "amount", (object) 0.0f, (object) "delay", (object) 0.0f, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutCubic, (object) "oncomplete", (object) "DeactivateCurrencyFrame", (object) "oncompletetarget", (object) this.gameObject);
        iTween.Stop(this.gameObject);
        iTween.FadeTo(this.gameObject, args);
        this.ShowCurrencyType(currencyType);
      }
    }
  }

  private CurrencyFrame.CurrencyType GetCurrencyToShow()
  {
    if (this.m_overrideCurrencyType.HasValue)
      return this.m_overrideCurrencyType.Value;
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    this.m_backgroundFaded = true;
    CurrencyFrame.CurrencyType currencyType;
    switch (mode)
    {
      case SceneMgr.Mode.HUB:
        currencyType = CurrencyFrame.CurrencyType.GOLD;
        this.m_backgroundFaded = false;
        break;
      case SceneMgr.Mode.COLLECTIONMANAGER:
        currencyType = !(bool) UniversalInputManager.UsePhoneUI ? CurrencyFrame.CurrencyType.ARCANE_DUST : CurrencyFrame.CurrencyType.NONE;
        break;
      case SceneMgr.Mode.PACKOPENING:
      case SceneMgr.Mode.TOURNAMENT:
      case SceneMgr.Mode.FRIENDLY:
      case SceneMgr.Mode.DRAFT:
      case SceneMgr.Mode.ADVENTURE:
        currencyType = !(bool) UniversalInputManager.UsePhoneUI ? CurrencyFrame.CurrencyType.GOLD : CurrencyFrame.CurrencyType.NONE;
        break;
      case SceneMgr.Mode.TAVERN_BRAWL:
        currencyType = !(bool) UniversalInputManager.UsePhoneUI ? (!((Object) TavernBrawlDisplay.Get() != (Object) null) || !TavernBrawlDisplay.Get().IsInDeckEditMode() ? CurrencyFrame.CurrencyType.GOLD : CurrencyFrame.CurrencyType.ARCANE_DUST) : CurrencyFrame.CurrencyType.NONE;
        break;
      default:
        currencyType = CurrencyFrame.CurrencyType.NONE;
        break;
    }
    if ((bool) UniversalInputManager.UsePhoneUI && currencyType == CurrencyFrame.CurrencyType.ARCANE_DUST)
      currencyType = CurrencyFrame.CurrencyType.NONE;
    return currencyType;
  }

  private void SetAmount(long amount)
  {
    this.m_amount.Text = amount.ToString();
  }

  private void ShowCurrencyType(CurrencyFrame.CurrencyType currencyType)
  {
    this.FadeBackground(this.m_backgroundFaded);
    if (this.m_showingCurrency == currencyType)
      return;
    this.m_showingCurrency = currencyType;
    iTween.FadeTo(this.m_amount.gameObject, 1f, 0.25f);
    switch (this.m_showingCurrency)
    {
      case CurrencyFrame.CurrencyType.GOLD:
        iTween.FadeTo(this.m_dustJar, 0.0f, 0.25f);
        iTween.FadeTo(this.m_goldCoin, 1f, 0.25f);
        break;
      case CurrencyFrame.CurrencyType.ARCANE_DUST:
        iTween.FadeTo(this.m_dustJar, 1f, 0.25f);
        iTween.FadeTo(this.m_goldCoin, 0.0f, 0.25f);
        break;
      default:
        iTween.FadeTo(this.m_dustJar, 0.0f, 0.25f);
        iTween.FadeTo(this.m_goldCoin, 0.0f, 0.25f);
        break;
    }
  }

  private void FadeBackground(bool isFaded)
  {
    Hashtable args;
    if (isFaded)
      args = iTween.Hash((object) "amount", (object) 0.5f, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutCubic);
    else
      args = iTween.Hash((object) "amount", (object) 1f, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutCubic);
    iTween.FadeTo(this.m_background, args);
  }

  private void ActivateCurrencyFrame()
  {
    this.m_state = CurrencyFrame.State.SHOWN;
  }

  private void OnFrameMouseOver(UIEvent e)
  {
    string key1 = string.Empty;
    string key2 = string.Empty;
    switch (this.m_showingCurrency)
    {
      case CurrencyFrame.CurrencyType.GOLD:
        key1 = "GLUE_TOOLTIP_GOLD_HEADER";
        key2 = "GLUE_TOOLTIP_GOLD_DESCRIPTION";
        break;
      case CurrencyFrame.CurrencyType.ARCANE_DUST:
        key1 = "GLUE_CRAFTING_ARCANEDUST";
        key2 = "GLUE_CRAFTING_ARCANEDUST_DESCRIPTION";
        break;
    }
    if (key1 == string.Empty)
      return;
    TooltipPanel tooltipPanel = this.GetComponent<TooltipZone>().ShowTooltip(GameStrings.Get(key1), GameStrings.Get(key2), 0.7f, true);
    SceneUtils.SetLayer(tooltipPanel.gameObject, GameLayer.BattleNet);
    tooltipPanel.transform.localEulerAngles = new Vector3(270f, 0.0f, 0.0f);
    tooltipPanel.transform.localScale = new Vector3(70f, 70f, 70f);
    if ((bool) UniversalInputManager.UsePhoneUI)
      TransformUtil.SetPoint((Component) tooltipPanel, Anchor.TOP, (Component) this.m_mouseOverZone, Anchor.BOTTOM, Vector3.zero);
    else
      TransformUtil.SetPoint((Component) tooltipPanel, Anchor.BOTTOM, (Component) this.m_mouseOverZone, Anchor.TOP, Vector3.zero);
  }

  private void OnFrameMouseOut(UIEvent e)
  {
    this.GetComponent<TooltipZone>().HideTooltip();
  }

  private void OnGoldBalanceChanged(NetCache.NetCacheGoldBalance balance)
  {
    if (this.m_showingCurrency != CurrencyFrame.CurrencyType.GOLD)
      return;
    this.SetAmount(balance.GetTotal());
  }

  private void UpdateAmount(CurrencyFrame.CurrencyType currencyType)
  {
    long amount = 0;
    switch (currencyType)
    {
      case CurrencyFrame.CurrencyType.GOLD:
        NetCache.NetCacheGoldBalance netObject1 = NetCache.Get().GetNetObject<NetCache.NetCacheGoldBalance>();
        if (netObject1 != null)
          amount = netObject1.GetTotal();
        this.SetAmount(amount);
        break;
      case CurrencyFrame.CurrencyType.ARCANE_DUST:
        if ((Object) CraftingManager.Get() != (Object) null)
        {
          amount = CraftingManager.Get().GetLocalArcaneDustBalance();
        }
        else
        {
          NetCache.NetCacheArcaneDustBalance netObject2 = NetCache.Get().GetNetObject<NetCache.NetCacheArcaneDustBalance>();
          if (netObject2 != null)
            amount = netObject2.Balance;
        }
        this.SetAmount(amount);
        break;
    }
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    this.m_overrideCurrencyType = new CurrencyFrame.CurrencyType?();
    this.m_amount.UpdateNow();
  }

  public enum CurrencyType
  {
    NONE,
    GOLD,
    ARCANE_DUST,
  }

  public enum State
  {
    ANIMATE_IN,
    ANIMATE_OUT,
    HIDDEN,
    SHOWN,
  }
}
