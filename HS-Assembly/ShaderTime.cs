﻿// Decompiled with JetBrains decompiler
// Type: ShaderTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ShaderTime : MonoBehaviour
{
  private float m_maxTime = 999f;
  private bool m_enabled = true;
  private float m_time;
  private static ShaderTime s_instance;

  public static ShaderTime Get()
  {
    return ShaderTime.s_instance;
  }

  private void Awake()
  {
    ShaderTime.s_instance = this;
  }

  private void Update()
  {
    this.UpdateShaderAnimationTime();
    this.UpdateGyro();
  }

  private void OnDestroy()
  {
    Shader.SetGlobalFloat("_ShaderTime", 0.0f);
  }

  public void DisableShaderAnimation()
  {
    this.m_enabled = false;
  }

  public void EnableShaderAnimation()
  {
    this.m_enabled = true;
  }

  private void UpdateShaderAnimationTime()
  {
    if (!this.m_enabled)
    {
      this.m_time = 1f;
      Shader.SetGlobalFloat("_ShaderTime", this.m_time);
    }
    this.m_time += Time.deltaTime / 20f;
    if ((double) this.m_time > (double) this.m_maxTime)
    {
      this.m_time = this.m_time - this.m_maxTime;
      if ((double) this.m_time <= 0.0)
        this.m_time = 0.0001f;
    }
    Shader.SetGlobalFloat("_ShaderTime", this.m_time);
  }

  private void UpdateGyro()
  {
    Shader.SetGlobalVector("_Gyroscope", (Vector4) Input.gyro.gravity);
  }
}
