﻿// Decompiled with JetBrains decompiler
// Type: DbfFieldAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[AttributeUsage(AttributeTargets.Property)]
public class DbfFieldAttribute : Attribute
{
  public string m_varName;
  public string m_comment;

  public DbfFieldAttribute(string varName, string comment)
  {
    this.m_varName = varName;
    this.m_comment = comment;
  }
}
