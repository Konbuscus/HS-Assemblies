﻿// Decompiled with JetBrains decompiler
// Type: AdventureDataDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AdventureDataDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private int m_AdventureId;
  [SerializeField]
  private int m_ModeId;
  [SerializeField]
  private int m_SortOrder;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private DbfLocValue m_ShortName;
  [SerializeField]
  private DbfLocValue m_Description;
  [SerializeField]
  private DbfLocValue m_ShortDescription;
  [SerializeField]
  private DbfLocValue m_RequirementsDescription;
  [SerializeField]
  private DbfLocValue m_CompleteBannerText;
  [SerializeField]
  private string m_SubscenePrefab;
  [SerializeField]
  private string m_AdventureSubDefPrefab;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("ADVENTURE_ID", "ASSET.ADVENTURE.ID")]
  public int AdventureId
  {
    get
    {
      return this.m_AdventureId;
    }
  }

  [DbfField("MODE_ID", "ASSET.ADVENTURE_MODE.ID")]
  public int ModeId
  {
    get
    {
      return this.m_ModeId;
    }
  }

  [DbfField("SORT_ORDER", "sort order of this adventure data in its adventure")]
  public int SortOrder
  {
    get
    {
      return this.m_SortOrder;
    }
  }

  [DbfField("NAME", "")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("SHORT_NAME", "")]
  public DbfLocValue ShortName
  {
    get
    {
      return this.m_ShortName;
    }
  }

  [DbfField("DESCRIPTION", "")]
  public DbfLocValue Description
  {
    get
    {
      return this.m_Description;
    }
  }

  [DbfField("SHORT_DESCRIPTION", "")]
  public DbfLocValue ShortDescription
  {
    get
    {
      return this.m_ShortDescription;
    }
  }

  [DbfField("REQUIREMENTS_DESCRIPTION", "")]
  public DbfLocValue RequirementsDescription
  {
    get
    {
      return this.m_RequirementsDescription;
    }
  }

  [DbfField("COMPLETE_BANNER_TEXT", "")]
  public DbfLocValue CompleteBannerText
  {
    get
    {
      return this.m_CompleteBannerText;
    }
  }

  [DbfField("SUBSCENE_PREFAB", "")]
  public string SubscenePrefab
  {
    get
    {
      return this.m_SubscenePrefab;
    }
  }

  [DbfField("ADVENTURE_SUB_DEF_PREFAB", "")]
  public string AdventureSubDefPrefab
  {
    get
    {
      return this.m_AdventureSubDefPrefab;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    AdventureDataDbfAsset adventureDataDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (AdventureDataDbfAsset)) as AdventureDataDbfAsset;
    if ((UnityEngine.Object) adventureDataDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("AdventureDataDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < adventureDataDbfAsset.Records.Count; ++index)
      adventureDataDbfAsset.Records[index].StripUnusedLocales();
    records = (object) adventureDataDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Name.StripUnusedLocales();
    this.m_ShortName.StripUnusedLocales();
    this.m_Description.StripUnusedLocales();
    this.m_ShortDescription.StripUnusedLocales();
    this.m_RequirementsDescription.StripUnusedLocales();
    this.m_CompleteBannerText.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetAdventureId(int v)
  {
    this.m_AdventureId = v;
  }

  public void SetModeId(int v)
  {
    this.m_ModeId = v;
  }

  public void SetSortOrder(int v)
  {
    this.m_SortOrder = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetShortName(DbfLocValue v)
  {
    this.m_ShortName = v;
    v.SetDebugInfo(this.ID, "SHORT_NAME");
  }

  public void SetDescription(DbfLocValue v)
  {
    this.m_Description = v;
    v.SetDebugInfo(this.ID, "DESCRIPTION");
  }

  public void SetShortDescription(DbfLocValue v)
  {
    this.m_ShortDescription = v;
    v.SetDebugInfo(this.ID, "SHORT_DESCRIPTION");
  }

  public void SetRequirementsDescription(DbfLocValue v)
  {
    this.m_RequirementsDescription = v;
    v.SetDebugInfo(this.ID, "REQUIREMENTS_DESCRIPTION");
  }

  public void SetCompleteBannerText(DbfLocValue v)
  {
    this.m_CompleteBannerText = v;
    v.SetDebugInfo(this.ID, "COMPLETE_BANNER_TEXT");
  }

  public void SetSubscenePrefab(string v)
  {
    this.m_SubscenePrefab = v;
  }

  public void SetAdventureSubDefPrefab(string v)
  {
    this.m_AdventureSubDefPrefab = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureDataDbfRecord.\u003C\u003Ef__switch\u0024map8 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureDataDbfRecord.\u003C\u003Ef__switch\u0024map8 = new Dictionary<string, int>(13)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "ADVENTURE_ID",
            2
          },
          {
            "MODE_ID",
            3
          },
          {
            "SORT_ORDER",
            4
          },
          {
            "NAME",
            5
          },
          {
            "SHORT_NAME",
            6
          },
          {
            "DESCRIPTION",
            7
          },
          {
            "SHORT_DESCRIPTION",
            8
          },
          {
            "REQUIREMENTS_DESCRIPTION",
            9
          },
          {
            "COMPLETE_BANNER_TEXT",
            10
          },
          {
            "SUBSCENE_PREFAB",
            11
          },
          {
            "ADVENTURE_SUB_DEF_PREFAB",
            12
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureDataDbfRecord.\u003C\u003Ef__switch\u0024map8.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.AdventureId;
          case 3:
            return (object) this.ModeId;
          case 4:
            return (object) this.SortOrder;
          case 5:
            return (object) this.Name;
          case 6:
            return (object) this.ShortName;
          case 7:
            return (object) this.Description;
          case 8:
            return (object) this.ShortDescription;
          case 9:
            return (object) this.RequirementsDescription;
          case 10:
            return (object) this.CompleteBannerText;
          case 11:
            return (object) this.SubscenePrefab;
          case 12:
            return (object) this.AdventureSubDefPrefab;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (AdventureDataDbfRecord.\u003C\u003Ef__switch\u0024map9 == null)
    {
      // ISSUE: reference to a compiler-generated field
      AdventureDataDbfRecord.\u003C\u003Ef__switch\u0024map9 = new Dictionary<string, int>(13)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "ADVENTURE_ID",
          2
        },
        {
          "MODE_ID",
          3
        },
        {
          "SORT_ORDER",
          4
        },
        {
          "NAME",
          5
        },
        {
          "SHORT_NAME",
          6
        },
        {
          "DESCRIPTION",
          7
        },
        {
          "SHORT_DESCRIPTION",
          8
        },
        {
          "REQUIREMENTS_DESCRIPTION",
          9
        },
        {
          "COMPLETE_BANNER_TEXT",
          10
        },
        {
          "SUBSCENE_PREFAB",
          11
        },
        {
          "ADVENTURE_SUB_DEF_PREFAB",
          12
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!AdventureDataDbfRecord.\u003C\u003Ef__switch\u0024map9.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetAdventureId((int) val);
        break;
      case 3:
        this.SetModeId((int) val);
        break;
      case 4:
        this.SetSortOrder((int) val);
        break;
      case 5:
        this.SetName((DbfLocValue) val);
        break;
      case 6:
        this.SetShortName((DbfLocValue) val);
        break;
      case 7:
        this.SetDescription((DbfLocValue) val);
        break;
      case 8:
        this.SetShortDescription((DbfLocValue) val);
        break;
      case 9:
        this.SetRequirementsDescription((DbfLocValue) val);
        break;
      case 10:
        this.SetCompleteBannerText((DbfLocValue) val);
        break;
      case 11:
        this.SetSubscenePrefab((string) val);
        break;
      case 12:
        this.SetAdventureSubDefPrefab((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureDataDbfRecord.\u003C\u003Ef__switch\u0024mapA == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureDataDbfRecord.\u003C\u003Ef__switch\u0024mapA = new Dictionary<string, int>(13)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "ADVENTURE_ID",
            2
          },
          {
            "MODE_ID",
            3
          },
          {
            "SORT_ORDER",
            4
          },
          {
            "NAME",
            5
          },
          {
            "SHORT_NAME",
            6
          },
          {
            "DESCRIPTION",
            7
          },
          {
            "SHORT_DESCRIPTION",
            8
          },
          {
            "REQUIREMENTS_DESCRIPTION",
            9
          },
          {
            "COMPLETE_BANNER_TEXT",
            10
          },
          {
            "SUBSCENE_PREFAB",
            11
          },
          {
            "ADVENTURE_SUB_DEF_PREFAB",
            12
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureDataDbfRecord.\u003C\u003Ef__switch\u0024mapA.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (int);
          case 3:
            return typeof (int);
          case 4:
            return typeof (int);
          case 5:
            return typeof (DbfLocValue);
          case 6:
            return typeof (DbfLocValue);
          case 7:
            return typeof (DbfLocValue);
          case 8:
            return typeof (DbfLocValue);
          case 9:
            return typeof (DbfLocValue);
          case 10:
            return typeof (DbfLocValue);
          case 11:
            return typeof (string);
          case 12:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
