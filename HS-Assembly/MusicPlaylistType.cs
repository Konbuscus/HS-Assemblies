﻿// Decompiled with JetBrains decompiler
// Type: MusicPlaylistType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public enum MusicPlaylistType
{
  Invalid = 0,
  UI_MainTitle = 100,
  UI_Tournament = 101,
  UI_Arena = 102,
  UI_Friendly = 103,
  UI_CollectionManager = 104,
  UI_PackOpening = 105,
  UI_Credits = 106,
  UI_EndGameScreen = 107,
  UI_TavernBrawl = 108,
  UI_CMHeroSkinPreview = 109,
  UI_HeroicBrawl = 110,
  InGame_Mulligan = 200,
  InGame_MulliganSoft = 201,
  InGame_Default = 202,
  InGame_GvGBoard = 203,
  InGame_NaxxramasAdventure = 204,
  InGame_BRMAdventure = 205,
  InGame_LOE1Adventure = 206,
  InGame_LOE2Adventure = 207,
  InGame_LOE_Minecart = 208,
  InGame_LOE_Wing3 = 209,
  InGame_LOE_Wing4Mission4 = 210,
  InGame_Karazhan = 211,
  InGame_KarazhanPrologue = 212,
  InGame_KarazhanFreeMedivh = 213,
  UISolo_Practice = 300,
  UISolo_Naxxramas = 301,
  UISolo_BRM = 302,
  UISolo_LOE_Select = 303,
  UISolo_LOE_Mission = 304,
  UISolo_Karazhan = 305,
  Store_PacksClassic = 400,
  Store_PacksGvG = 401,
  Store_PacksTGT = 402,
  Store_PacksOG = 403,
  Store_PacksMSG = 404,
  Store_AdvNaxxramas = 450,
  Store_AdvBRM = 451,
  Store_AdvLOE = 452,
  Store_AdvKarazhan = 453,
  Misc_Tutorial01 = 501,
  Misc_Tutorial01PackOpen = 502,
  Hero_Magni = 900,
  Hero_Alleria = 901,
  Hero_Medvih = 902,
  Hero_Liadrin = 903,
  Hero_Khadgar = 904,
  Hero_Morgl = 905,
  Hero_Tyrande = 906,
}
