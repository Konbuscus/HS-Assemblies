﻿// Decompiled with JetBrains decompiler
// Type: TB02_CoOp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TB02_CoOp : MissionEntity
{
  private Card m_bossCard;

  private void SetUpBossCard()
  {
    if (!((Object) this.m_bossCard == (Object) null))
      return;
    Entity entity = GameState.Get().GetEntity(GameState.Get().GetGameEntity().GetTag(GAME_TAG.TAG_SCRIPT_DATA_ENT_1));
    if (entity == null)
      return;
    this.m_bossCard = entity.GetCard();
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("FX_MinionSummon_Cast");
    this.PreloadSound("CleanMechSmall_Trigger_Underlay");
    this.PreloadSound("CleanMechLarge_Play_Underlay");
    this.PreloadSound("CleanMechLarge_Death_Underlay");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB02_CoOp.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1EC() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB02_CoOp.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1ED() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  public override bool ShouldPlayHeroBlowUpSpells(TAG_PLAYSTATE playState)
  {
    return playState != TAG_PLAYSTATE.WON;
  }
}
