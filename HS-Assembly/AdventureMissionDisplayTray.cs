﻿// Decompiled with JetBrains decompiler
// Type: AdventureMissionDisplayTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AdventureMissionDisplayTray : MonoBehaviour
{
  public SlidingTray m_slidingTray;
  public PegUIElement m_rewardsChest;
  public AdventureRewardsDisplayArea m_rewardsDisplay;
  public Transform m_rewardsDisplayBone;

  private void Awake()
  {
    AdventureConfig adventureConfig = AdventureConfig.Get();
    adventureConfig.AddAdventureMissionSetListener(new AdventureConfig.AdventureMissionSet(this.OnMissionSelected));
    adventureConfig.AddSubSceneChangeListener(new AdventureConfig.SubSceneChange(this.OnSubsceneChanged));
    if ((Object) this.m_rewardsChest != (Object) null)
    {
      this.m_rewardsChest.AddEventListener(UIEventType.ROLLOVER, (UIEvent.Handler) (e => this.ShowCardRewards()));
      this.m_rewardsChest.AddEventListener(UIEventType.ROLLOUT, (UIEvent.Handler) (e => this.HideCardRewards()));
    }
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
  }

  private void OnDestroy()
  {
    AdventureConfig adventureConfig = AdventureConfig.Get();
    if ((Object) adventureConfig != (Object) null)
    {
      adventureConfig.RemoveAdventureMissionSetListener(new AdventureConfig.AdventureMissionSet(this.OnMissionSelected));
      adventureConfig.RemoveSubSceneChangeListener(new AdventureConfig.SubSceneChange(this.OnSubsceneChanged));
    }
    GameMgr.Get().UnregisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
  }

  private void OnMissionSelected(ScenarioDbId mission, bool showDetails)
  {
    if (mission == ScenarioDbId.INVALID)
      return;
    if (showDetails)
      this.m_slidingTray.ToggleTraySlider(true, (Transform) null, true);
    this.ShowRewardsChest();
  }

  public void EnableRewardsChest(bool enabled)
  {
    if ((Object) this.m_rewardsChest == (Object) null)
      return;
    this.m_rewardsChest.SetEnabled(enabled);
  }

  public void ShowRewardsChest()
  {
    if ((Object) this.m_rewardsChest == (Object) null)
      return;
    ScenarioDbId mission = AdventureConfig.Get().GetMission();
    List<CardRewardData> defeatingScenario = AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario((int) mission);
    bool flag = AdventureProgressMgr.Get().HasDefeatedScenario((int) mission);
    this.m_rewardsChest.gameObject.SetActive(defeatingScenario != null && defeatingScenario.Count != 0 && !flag);
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_CANCELED:
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
      case FindGameState.SERVER_GAME_CANCELED:
        this.EnableRewardsChest(true);
        break;
    }
    return false;
  }

  private void ShowCardRewards()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      NotificationManager.Get().DestroyActiveQuote(0.2f, false);
    this.m_rewardsDisplay.ShowCardsNoFullscreen(AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario((int) AdventureConfig.Get().GetMission()), this.m_rewardsDisplayBone.position, new Vector3?(this.m_rewardsChest.transform.position));
  }

  private void HideCardRewards()
  {
    this.m_rewardsDisplay.HideCardRewards();
  }

  private void OnSubsceneChanged(AdventureSubScenes newscene, bool forward)
  {
    this.m_slidingTray.ToggleTraySlider(false, (Transform) null, true);
  }
}
