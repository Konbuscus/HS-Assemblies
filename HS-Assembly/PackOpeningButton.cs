﻿// Decompiled with JetBrains decompiler
// Type: PackOpeningButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PackOpeningButton : BoxMenuButton
{
  public UberText m_count;
  public GameObject m_countFrame;

  public string GetGetPackCount()
  {
    return this.m_count.Text;
  }

  public void SetPackCount(int packs)
  {
    if (packs < 0)
      this.m_count.Text = string.Empty;
    else
      this.m_count.Text = GameStrings.Format("GLUE_PACK_OPENING_BOOSTER_COUNT", (object) packs);
  }
}
