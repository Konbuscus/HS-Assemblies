﻿// Decompiled with JetBrains decompiler
// Type: AttackType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum AttackType
{
  INVALID,
  REGULAR,
  PROPOSED,
  CANCELED,
  ONLY_ATTACKER,
  ONLY_DEFENDER,
  ONLY_PROPOSED_ATTACKER,
  ONLY_PROPOSED_DEFENDER,
  WAITING_ON_PROPOSED_ATTACKER,
  WAITING_ON_PROPOSED_DEFENDER,
  WAITING_ON_ATTACKER,
  WAITING_ON_DEFENDER,
}
