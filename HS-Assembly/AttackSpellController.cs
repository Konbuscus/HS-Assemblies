﻿// Decompiled with JetBrains decompiler
// Type: AttackSpellController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class AttackSpellController : SpellController
{
  public float m_ImpactStagingPoint = 1f;
  public float m_SourceImpactOffset = -0.25f;
  private const float PROPOSED_ATTACK_IMPACT_POINT_SCALAR = 0.5f;
  private const float WINDFURY_REMINDER_WAIT_SEC = 1.2f;
  public HeroAttackDef m_HeroInfo;
  public AllyAttackDef m_AllyInfo;
  public SpellValueRange[] m_ImpactDefs;
  public Spell m_DefaultImpactSpellPrefab;
  private AttackType m_attackType;
  private Spell m_sourceAttackSpell;
  private Vector3 m_sourcePos;
  private Vector3 m_sourceToTarget;
  private Vector3 m_sourceFacing;

  protected override bool AddPowerSourceAndTargets(PowerTaskList taskList)
  {
    this.m_attackType = taskList.GetAttackType();
    if (this.m_attackType == AttackType.INVALID)
      return false;
    Entity attacker = taskList.GetAttacker();
    if (attacker != null)
      this.SetSource(attacker.GetCard());
    Entity defender = taskList.GetDefender();
    if (defender != null)
      this.AddTarget(defender.GetCard());
    return true;
  }

  protected override void OnProcessTaskList()
  {
    if (this.m_attackType == AttackType.ONLY_PROPOSED_ATTACKER || this.m_attackType == AttackType.ONLY_PROPOSED_DEFENDER || (this.m_attackType == AttackType.ONLY_ATTACKER || this.m_attackType == AttackType.ONLY_DEFENDER) || (this.m_attackType == AttackType.WAITING_ON_PROPOSED_ATTACKER || this.m_attackType == AttackType.WAITING_ON_PROPOSED_DEFENDER || (this.m_attackType == AttackType.WAITING_ON_ATTACKER || this.m_attackType == AttackType.WAITING_ON_DEFENDER)))
    {
      this.FinishEverything();
    }
    else
    {
      Card source = this.GetSource();
      Entity entity = source.GetEntity();
      Zone zone = source.GetZone();
      bool flag = zone.m_Side == Player.Side.FRIENDLY;
      this.m_sourceAttackSpell = !flag ? source.GetActorSpell(SpellType.OPPONENT_ATTACK, true) : source.GetActorSpell(SpellType.FRIENDLY_ATTACK, true);
      if (this.m_attackType == AttackType.CANCELED)
      {
        if ((UnityEngine.Object) this.m_sourceAttackSpell != (UnityEngine.Object) null)
        {
          if (entity.IsHero())
            this.m_sourceAttackSpell.ActivateState(SpellStateType.CANCEL);
          else
            this.m_sourceAttackSpell.ActivateState(SpellStateType.DEATH);
        }
        source.SetDoNotSort(false);
        zone.UpdateLayout();
        source.EnableAttacking(false);
        this.FinishEverything();
      }
      else
      {
        source.EnableAttacking(true);
        if (entity.HasTag(GAME_TAG.IMMUNE_WHILE_ATTACKING))
          source.ActivateActorSpell(SpellType.IMMUNE);
        this.m_sourceAttackSpell.AddStateStartedCallback(new Spell.StateStartedCallback(this.OnSourceAttackStateStarted));
        if (flag)
        {
          if (this.m_sourceAttackSpell.GetActiveState() != SpellStateType.IDLE)
            this.m_sourceAttackSpell.ActivateState(SpellStateType.BIRTH);
          else
            this.m_sourceAttackSpell.ActivateState(SpellStateType.ACTION);
        }
        else
          this.m_sourceAttackSpell.ActivateState(SpellStateType.BIRTH);
      }
    }
  }

  private void OnSourceAttackStateStarted(Spell spell, SpellStateType prevStateType, object userData)
  {
    switch (spell.GetActiveState())
    {
      case SpellStateType.IDLE:
        spell.ActivateState(SpellStateType.ACTION);
        break;
      case SpellStateType.ACTION:
        spell.RemoveStateStartedCallback(new Spell.StateStartedCallback(this.OnSourceAttackStateStarted));
        this.LaunchAttack();
        break;
    }
  }

  private void LaunchAttack()
  {
    Card source = this.GetSource();
    Entity entity = source.GetEntity();
    Card target = this.GetTarget();
    bool flag = this.m_attackType == AttackType.PROPOSED;
    if (flag && entity.IsHero())
    {
      this.m_sourceAttackSpell.ActivateState(SpellStateType.IDLE);
      this.FinishEverything();
    }
    else
    {
      this.m_sourcePos = source.transform.position;
      this.m_sourceToTarget = target.transform.position - this.m_sourcePos;
      Vector3 impactPos = this.ComputeImpactPos();
      source.SetDoNotSort(true);
      this.MoveSourceToTarget(source, entity, impactPos);
      if (entity.IsHero())
        this.OrientSourceHeroToTarget(source);
      if (flag)
        return;
      target.SetDoNotSort(true);
      this.MoveTargetToSource(target, entity, impactPos);
    }
  }

  private void OnMoveToTargetFinished()
  {
    Card source = this.GetSource();
    Entity entity = source.GetEntity();
    Card target = this.GetTarget();
    bool flag = this.m_attackType == AttackType.PROPOSED;
    this.DoTasks(source, target);
    if (!flag)
      this.ActivateImpactEffects(source, target);
    if (entity.IsHero())
    {
      this.MoveSourceHeroBack(source);
      this.OrientSourceHeroBack(source);
      target.SetDoNotSort(false);
      target.GetZone().UpdateLayout();
    }
    else if (flag)
    {
      this.FinishEverything();
    }
    else
    {
      source.SetDoNotSort(false);
      source.GetZone().UpdateLayout();
      target.SetDoNotSort(false);
      target.GetZone().UpdateLayout();
      if (entity.HasTag(GAME_TAG.FINISH_ATTACK_SPELL_ON_DAMAGE))
        this.FinishAttackSpellController();
      else
        this.m_sourceAttackSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnMinionSourceAttackStateFinished));
      this.m_sourceAttackSpell.ActivateState(SpellStateType.DEATH);
    }
  }

  private void DoTasks(Card sourceCard, Card targetCard)
  {
    GameUtils.DoDamageTasks(this.m_taskList, sourceCard, targetCard);
  }

  private void MoveSourceHeroBack(Card sourceCard)
  {
    Hashtable args = iTween.Hash((object) "position", (object) this.m_sourcePos, (object) "time", (object) this.m_HeroInfo.m_MoveBackDuration, (object) "easetype", (object) this.m_HeroInfo.m_MoveBackEaseType, (object) "oncomplete", (object) "OnHeroMoveBackFinished", (object) "oncompletetarget", (object) this.gameObject);
    iTween.MoveTo(sourceCard.gameObject, args);
  }

  private void OrientSourceHeroBack(Card sourceCard)
  {
    Hashtable args = iTween.Hash((object) "rotation", (object) Quaternion.LookRotation(this.m_sourceFacing).eulerAngles, (object) "time", (object) this.m_HeroInfo.m_OrientBackDuration, (object) "easetype", (object) this.m_HeroInfo.m_OrientBackEaseType);
    iTween.RotateTo(sourceCard.gameObject, args);
  }

  private void OnHeroMoveBackFinished()
  {
    Card source = this.GetSource();
    Entity entity = source.GetEntity();
    source.SetDoNotSort(false);
    source.EnableAttacking(false);
    if (entity.GetController().IsLocalUser() || this.m_sourceAttackSpell.GetActiveState() == SpellStateType.NONE)
    {
      this.PlayWindfuryReminderIfPossible(entity, source);
      this.FinishEverything();
    }
    else
      this.m_sourceAttackSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnHeroSourceAttackStateFinished));
  }

  private void OnHeroSourceAttackStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    spell.RemoveStateFinishedCallback(new Spell.StateFinishedCallback(this.OnHeroSourceAttackStateFinished));
    Card source = this.GetSource();
    this.PlayWindfuryReminderIfPossible(source.GetEntity(), source);
    this.FinishEverything();
  }

  private void OnMinionSourceAttackStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    spell.RemoveStateFinishedCallback(new Spell.StateFinishedCallback(this.OnMinionSourceAttackStateFinished));
    this.FinishAttackSpellController();
  }

  private void FinishAttackSpellController()
  {
    Card source = this.GetSource();
    Entity entity = source.GetEntity();
    source.EnableAttacking(false);
    if (!this.CanPlayWindfuryReminder(entity, source))
    {
      this.FinishEverything();
    }
    else
    {
      this.OnFinishedTaskList();
      this.StartCoroutine(this.WaitThenPlayWindfuryReminder(entity, source));
    }
  }

  private void FinishEverything()
  {
    Card source = this.GetSource();
    if (source.GetEntity().HasTag(GAME_TAG.IMMUNE_WHILE_ATTACKING) && !source.ShouldShowImmuneVisuals())
      source.GetActor().ActivateSpellDeathState(SpellType.IMMUNE);
    this.OnFinishedTaskList();
    this.OnFinished();
  }

  [DebuggerHidden]
  private IEnumerator WaitThenPlayWindfuryReminder(Entity entity, Card card)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AttackSpellController.\u003CWaitThenPlayWindfuryReminder\u003Ec__Iterator248() { entity = entity, card = card, \u003C\u0024\u003Eentity = entity, \u003C\u0024\u003Ecard = card, \u003C\u003Ef__this = this };
  }

  private bool CanPlayWindfuryReminder(Entity entity, Card card)
  {
    return entity.HasWindfury() && !entity.IsExhausted() && (entity.GetZone() == TAG_ZONE.PLAY && entity.GetController().IsCurrentPlayer()) && !((UnityEngine.Object) card.GetActorSpell(SpellType.WINDFURY_BURST, true) == (UnityEngine.Object) null);
  }

  private void PlayWindfuryReminderIfPossible(Entity entity, Card card)
  {
    if (!this.CanPlayWindfuryReminder(entity, card))
      return;
    card.ActivateActorSpell(SpellType.WINDFURY_BURST);
  }

  private void MoveSourceToTarget(Card sourceCard, Entity sourceEntity, Vector3 impactPos)
  {
    Vector3 impactOffset = this.ComputeImpactOffset(sourceCard, impactPos);
    Vector3 vector3 = impactPos + impactOffset;
    float toTargetDuration;
    iTween.EaseType toTargetEaseType;
    if (sourceEntity.IsHero())
    {
      toTargetDuration = this.m_HeroInfo.m_MoveToTargetDuration;
      toTargetEaseType = this.m_HeroInfo.m_MoveToTargetEaseType;
    }
    else
    {
      toTargetDuration = this.m_AllyInfo.m_MoveToTargetDuration;
      toTargetEaseType = this.m_AllyInfo.m_MoveToTargetEaseType;
    }
    Hashtable args = iTween.Hash((object) "position", (object) vector3, (object) "time", (object) toTargetDuration, (object) "easetype", (object) toTargetEaseType, (object) "oncomplete", (object) "OnMoveToTargetFinished", (object) "oncompletetarget", (object) this.gameObject);
    iTween.MoveTo(sourceCard.gameObject, args);
  }

  private void OrientSourceHeroToTarget(Card sourceCard)
  {
    this.m_sourceFacing = sourceCard.transform.forward;
    Hashtable args = iTween.Hash((object) "rotation", (object) (this.m_sourceAttackSpell.GetSpellType() != SpellType.OPPONENT_ATTACK ? Quaternion.LookRotation(this.m_sourceToTarget) : Quaternion.LookRotation(-this.m_sourceToTarget)).eulerAngles, (object) "time", (object) this.m_HeroInfo.m_OrientToTargetDuration, (object) "easetype", (object) this.m_HeroInfo.m_OrientToTargetEaseType);
    iTween.RotateTo(sourceCard.gameObject, args);
  }

  private void MoveTargetToSource(Card targetCard, Entity sourceEntity, Vector3 impactPos)
  {
    float toTargetDuration;
    iTween.EaseType toTargetEaseType;
    if (sourceEntity.IsHero())
    {
      toTargetDuration = this.m_HeroInfo.m_MoveToTargetDuration;
      toTargetEaseType = this.m_HeroInfo.m_MoveToTargetEaseType;
    }
    else
    {
      toTargetDuration = this.m_AllyInfo.m_MoveToTargetDuration;
      toTargetEaseType = this.m_AllyInfo.m_MoveToTargetEaseType;
    }
    Hashtable args = iTween.Hash((object) "position", (object) impactPos, (object) "time", (object) toTargetDuration, (object) "easetype", (object) toTargetEaseType);
    iTween.MoveTo(targetCard.gameObject, args);
  }

  private Vector3 ComputeImpactPos()
  {
    float num = 1f;
    if (this.m_attackType == AttackType.PROPOSED)
      num = 0.5f;
    return this.m_sourcePos + num * this.m_ImpactStagingPoint * this.m_sourceToTarget;
  }

  private Vector3 ComputeImpactOffset(Card sourceCard, Vector3 impactPos)
  {
    if (Mathf.Approximately(this.m_SourceImpactOffset, 0.5f))
      return Vector3.zero;
    Bounds bounds = sourceCard.GetActor().GetMeshRenderer().bounds;
    bounds.center = this.m_sourcePos;
    Ray ray = new Ray(impactPos, bounds.center - impactPos);
    float distance;
    if (!bounds.IntersectRay(ray, out distance))
      return Vector3.zero;
    Vector3 vector3_1 = ray.origin + distance * ray.direction;
    Vector3 vector3_2 = 2f * bounds.center - vector3_1 - vector3_1;
    return 0.5f * vector3_2 - this.m_SourceImpactOffset * vector3_2;
  }

  private void ActivateImpactEffects(Card sourceCard, Card targetCard)
  {
    Spell impactSpellPrefab = this.DetermineImpactSpellPrefab(sourceCard);
    if ((UnityEngine.Object) impactSpellPrefab == (UnityEngine.Object) null)
      return;
    Spell spell = UnityEngine.Object.Instantiate<Spell>(impactSpellPrefab);
    spell.SetSource(sourceCard.gameObject);
    spell.AddTarget(targetCard.gameObject);
    Vector3 position = targetCard.transform.position;
    spell.SetPosition(position);
    Quaternion orientation = Quaternion.LookRotation(this.m_sourceToTarget);
    spell.SetOrientation(orientation);
    spell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnImpactSpellStateFinished));
    spell.Activate();
  }

  private Spell DetermineImpactSpellPrefab(Card sourceCard)
  {
    int atk = sourceCard.GetEntity().GetATK();
    SpellValueRange accordingToRanges = SpellUtils.GetAppropriateElementAccordingToRanges<SpellValueRange>(this.m_ImpactDefs, (Func<SpellValueRange, ValueRange>) (x => x.m_range), atk);
    if (accordingToRanges != null && (UnityEngine.Object) accordingToRanges.m_spellPrefab != (UnityEngine.Object) null)
      return accordingToRanges.m_spellPrefab;
    return this.m_DefaultImpactSpellPrefab;
  }

  private void OnImpactSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
  }
}
