﻿// Decompiled with JetBrains decompiler
// Type: PrinceMalchezaarSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class PrinceMalchezaarSpell : SuperSpell
{
  private bool hasOpponentSeenSpell;
  public GameObject malchezaarInitialVO;
  public GameObject malchezaarResponseVO;

  public override bool AddPowerTargets()
  {
    Player controller = this.GetSourceCard().GetController();
    if (controller.HasSeenMalchezaarSpell())
      return false;
    bool flag = base.AddPowerTargets();
    if (flag)
      controller.SetHasSeenMalchezaarSpell(true);
    return flag;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    this.StartCoroutine(this.DoAction(prevStateType));
  }

  [DebuggerHidden]
  private IEnumerator DoAction(SpellStateType prevStateType)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PrinceMalchezaarSpell.\u003CDoAction\u003Ec__Iterator2CB() { prevStateType = prevStateType, \u003C\u0024\u003EprevStateType = prevStateType, \u003C\u003Ef__this = this };
  }
}
