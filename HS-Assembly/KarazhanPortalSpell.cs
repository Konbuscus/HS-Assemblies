﻿// Decompiled with JetBrains decompiler
// Type: KarazhanPortalSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class KarazhanPortalSpell : SuperSpell
{
  public float m_minTimePortalIsOpen = 1f;
  private bool m_isPortalOpenForMinTime = true;
  public Spell m_portalSpell;
  public Spell m_customSpawnSpell;
  private bool m_waitForSpawnSpell;
  private Spell m_portalSpellInstance;
  private Spell m_spawnSpellInstance;
  private Card m_spawnedMinion;
  private bool m_playSuperSpellVisuals;

  public override bool AddPowerTargets()
  {
    this.m_playSuperSpellVisuals = base.AddPowerTargets();
    return true;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    if ((Object) this.m_portalSpellInstance == (Object) null)
    {
      this.m_portalSpellInstance = Object.Instantiate<Spell>(this.m_portalSpell);
      SpellUtils.SetCustomSpellParent(this.m_portalSpellInstance, (Component) this);
      this.m_portalSpellInstance.SetSource(this.GetSource());
      this.m_portalSpellInstance.Activate();
      this.m_isPortalOpenForMinTime = false;
      this.StartCoroutine(this.DoPortalMinTimeOpenTimer());
    }
    bool flag = false;
    if ((Object) this.m_spawnedMinion == (Object) null)
    {
      this.m_spawnedMinion = this.GetSpawnedMinion();
      if ((Object) this.m_spawnedMinion != (Object) null)
      {
        this.m_waitForSpawnSpell = true;
        this.m_spawnSpellInstance = Object.Instantiate<Spell>(this.m_customSpawnSpell);
        this.m_spawnSpellInstance.AddSpellEventCallback(new Spell.SpellEventCallback(this.OnSpawnSpellEvent));
        this.m_spawnedMinion.OverrideCustomSpawnSpell(this.m_spawnSpellInstance);
        flag = true;
      }
    }
    if (this.m_playSuperSpellVisuals)
      base.OnAction(prevStateType);
    else
      this.OnStateFinished();
    if (!flag)
      return;
    this.SuppressDeathSoundsOnKilledTargets();
  }

  private void SuppressDeathSoundsOnKilledTargets()
  {
    List<Entity> targetEntities = new List<Entity>();
    using (List<GameObject>.Enumerator enumerator = this.GetVisualTargets().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          Card component = current.GetComponent<Card>();
          targetEntities.Add(component.GetEntity());
        }
      }
    }
    using (List<Entity>.Enumerator enumerator = GameUtils.GetEntitiesKilledBySourceAmongstTargets(this.GetSourceCard().GetEntity().GetEntityId(), targetEntities).GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.GetCard().SuppressDeathSounds(true);
    }
  }

  public override bool CanPurge()
  {
    return this.m_taskList.IsEndOfBlock() && (!((Object) this.m_portalSpellInstance != (Object) null) || !this.m_portalSpellInstance.IsActive());
  }

  public void OnSpawnSpellEvent(string eventName, object eventData, object userData)
  {
    if (!(eventName == "ClosePortal"))
      return;
    this.m_waitForSpawnSpell = false;
    this.TryPortalClose();
  }

  [DebuggerHidden]
  private IEnumerator DoPortalMinTimeOpenTimer()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KarazhanPortalSpell.\u003CDoPortalMinTimeOpenTimer\u003Ec__Iterator2BC() { \u003C\u003Ef__this = this };
  }

  public override void OnSpellFinished()
  {
    this.TryPortalClose();
    base.OnSpellFinished();
  }

  private void TryPortalClose()
  {
    if (this.m_taskList != null && !this.m_taskList.IsEndOfBlock())
    {
      for (PowerTaskList powerTaskList = this.m_taskList; powerTaskList != null; powerTaskList = powerTaskList.GetNext())
      {
        if (powerTaskList.HasTasks())
          return;
      }
    }
    if (this.m_waitForSpawnSpell || (Object) this.m_portalSpellInstance == (Object) null || (this.m_portalSpellInstance.GetActiveState() == SpellStateType.DEATH || this.m_portalSpellInstance.GetActiveState() == SpellStateType.NONE) || !this.m_isPortalOpenForMinTime)
      return;
    this.m_portalSpellInstance.ActivateState(SpellStateType.DEATH);
  }

  private Card GetSpawnedMinion()
  {
    for (int index = 0; index < this.m_taskList.GetTaskList().Count; ++index)
    {
      Network.PowerHistory power = this.m_taskList.GetTaskList()[index].GetPower();
      if (power.Type == Network.PowerType.FULL_ENTITY)
      {
        Entity entity = GameState.Get().GetEntity((power as Network.HistFullEntity).Entity.ID);
        if (entity.GetTag(GAME_TAG.ZONE) != 6 && entity != null)
        {
          Card card = entity.GetCard();
          if (!((Object) card == (Object) null))
            return card;
        }
      }
    }
    return (Card) null;
  }
}
