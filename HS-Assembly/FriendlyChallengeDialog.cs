﻿// Decompiled with JetBrains decompiler
// Type: FriendlyChallengeDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using SpectatorProto;
using UnityEngine;

public class FriendlyChallengeDialog : DialogBase
{
  private const float NAME_LINE_PADDING = 0.01f;
  public UberText m_challengeText;
  public UberText m_challengerName;
  public UIBButton m_acceptButton;
  public UIBButton m_denyButton;
  public UberText m_nearbyPlayerNote;
  public float m_friendQuestSliderSoundDelay;
  public string m_friendQuestSliderSound;
  public float m_friendQuestSliderSoundDelay2;
  public string m_friendQuestSliderSound2;
  public GameObject m_friendQuestContainer;
  public GameObject m_dropShadow;
  private FriendlyChallengeDialog.ResponseCallback m_responseCallback;
  private Achievement m_quest;
  private FriendlyChallengeQuestFrame m_friendlyQuestFrame;
  private PartyQuestInfo m_partyQuestInfo;

  private void Start()
  {
    this.m_acceptButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ConfirmButtonPress));
    this.m_denyButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.CancelButtonPress));
  }

  private void Update()
  {
    BnetBar.Get().RequestDisableButtons();
  }

  public override void Show()
  {
    base.Show();
    if ((bool) UniversalInputManager.UsePhoneUI && this.m_nearbyPlayerNote.gameObject.activeSelf)
      this.transform.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y + 50f, this.transform.localPosition.z);
    this.DoShowAnimation();
    UniversalInputManager.Get().SetSystemDialogActive(true);
    SoundManager.Get().LoadAndPlay("friendly_challenge");
    if (this.m_partyQuestInfo == null)
      return;
    ApplicationMgr.Get().ScheduleCallback(this.m_friendQuestSliderSoundDelay, false, (ApplicationMgr.ScheduledCallback) (u => SoundManager.Get().LoadAndPlay(this.m_friendQuestSliderSound)), (object) null);
    ApplicationMgr.Get().ScheduleCallback(this.m_friendQuestSliderSoundDelay2, false, (ApplicationMgr.ScheduledCallback) (u => SoundManager.Get().LoadAndPlay(this.m_friendQuestSliderSound2)), (object) null);
  }

  public override void Hide()
  {
    base.Hide();
    SoundManager.Get().LoadAndPlay("banner_shrink");
    iTween.FadeTo(this.m_dropShadow, iTween.Hash((object) "amount", (object) 0.0f, (object) "time", (object) 1f));
  }

  public override bool HandleKeyboardInput()
  {
    if (!Input.GetKeyUp(KeyCode.Escape))
      return false;
    this.CancelButtonPress((UIEvent) null);
    return true;
  }

  public void SetInfo(FriendlyChallengeDialog.Info info)
  {
    string key = "GLOBAL_FRIEND_CHALLENGE_BODY1";
    if (FriendChallengeMgr.Get().IsChallengeTavernBrawl())
      key = "GLOBAL_FRIEND_CHALLENGE_TAVERN_BRAWL_BODY1";
    else if (CollectionManager.Get().ShouldAccountSeeStandardWild())
    {
      if (info.m_formatType == FormatType.FT_STANDARD)
        key = "GLOBAL_FRIEND_CHALLENGE_BODY1_STANDARD";
      else if (info.m_formatType == FormatType.FT_WILD)
        key = "GLOBAL_FRIEND_CHALLENGE_BODY1_WILD";
    }
    this.m_challengeText.Text = GameStrings.Get(key);
    this.m_challengerName.Text = FriendUtils.GetUniqueName(info.m_challenger);
    this.m_responseCallback = info.m_callback;
    this.m_nearbyPlayerNote.gameObject.SetActive(BnetNearbyPlayerMgr.Get().IsNearbyStranger(info.m_challenger));
  }

  public void SetQuestInfo(PartyQuestInfo info)
  {
    if ((Object) this.m_friendQuestContainer == (Object) null)
      return;
    this.m_partyQuestInfo = info;
    if (info == null || info.QuestIds.Count == 0)
    {
      this.m_friendQuestContainer.gameObject.SetActive(false);
    }
    else
    {
      this.m_friendQuestContainer.gameObject.SetActive(true);
      SlidingTray component = this.m_friendQuestContainer.GetComponent<SlidingTray>();
      if ((Object) component != (Object) null)
        component.ShowTray();
      iTween.FadeTo(this.m_dropShadow, iTween.Hash((object) "amount", (object) 1f, (object) "time", (object) 1f));
      this.m_quest = AchieveManager.Get().GetAchievement(info.QuestIds[0]);
      this.m_friendlyQuestFrame = this.m_friendQuestContainer.GetComponentInChildren<FriendlyChallengeQuestFrame>();
      if (this.m_quest == null || !((Object) this.m_friendlyQuestFrame != (Object) null))
        return;
      this.m_friendlyQuestFrame.m_questName.Text = this.m_quest.Name;
      this.m_friendlyQuestFrame.m_questDesc.Text = this.m_quest.Description;
      RewardUtils.SetQuestTileNameLinePosition(this.m_friendlyQuestFrame.m_nameLine, this.m_friendlyQuestFrame.m_questName, 0.01f);
      RewardData rewardData = this.m_quest.Rewards.Count != 0 ? this.m_quest.Rewards[0] : (RewardData) null;
      if (rewardData == null || !((Object) this.m_friendlyQuestFrame.m_rewardBone != (Object) null))
        return;
      rewardData.LoadRewardObject(new Reward.DelOnRewardLoaded(this.SetQuestInfo_OnLoadRewardObject));
    }
  }

  private void SetQuestInfo_OnLoadRewardObject(Reward reward, object callbackData)
  {
    if ((Object) this.m_friendlyQuestFrame.m_rewardBone == (Object) null)
      return;
    reward.transform.SetParent(this.m_friendlyQuestFrame.m_rewardBone.transform);
    reward.transform.localPosition = Vector3.zero;
    float amountToScaleReward;
    RewardUtils.SetRewardMaterialOffset(this.m_quest.Rewards, this.m_friendlyQuestFrame.m_rewardMesh.material, this.m_friendlyQuestFrame.m_rewardAmountLabel, out amountToScaleReward);
    this.m_friendlyQuestFrame.m_rewardMesh.transform.localScale *= amountToScaleReward;
  }

  private void ConfirmButtonPress(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("Small_Click");
    if (this.m_responseCallback != null)
      this.m_responseCallback(true);
    this.Hide();
  }

  private void CancelButtonPress(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("Small_Click");
    if (this.m_responseCallback != null)
      this.m_responseCallback(false);
    this.Hide();
  }

  public class Info
  {
    public FormatType m_formatType;
    public BnetPlayer m_challenger;
    public FriendlyChallengeDialog.ResponseCallback m_callback;
  }

  public delegate void ResponseCallback(bool accept);
}
