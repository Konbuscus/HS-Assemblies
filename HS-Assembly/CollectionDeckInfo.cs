﻿// Decompiled with JetBrains decompiler
// Type: CollectionDeckInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class CollectionDeckInfo : MonoBehaviour
{
  private readonly int MAX_MANA_COST_ID = 7;
  private readonly float MANA_COST_TEXT_MAX_LOCAL_Z = 5.167298f;
  protected bool m_shown = true;
  private string m_heroPowerID = string.Empty;
  private List<CollectionDeckInfo.ShowListener> m_showListeners = new List<CollectionDeckInfo.ShowListener>();
  private List<CollectionDeckInfo.HideListener> m_hideListeners = new List<CollectionDeckInfo.HideListener>();
  public GameObject m_root;
  public GameObject m_heroPowerParent;
  public UberText m_heroPowerName;
  public UberText m_heroPowerDescription;
  public UberText m_manaCurveTooltipText;
  public PegUIElement m_offClicker;
  public List<DeckInfoManaBar> m_manaBars;
  private readonly int MIN_MANA_COST_ID;
  private readonly float MANA_COST_TEXT_MIN_LOCAL_Z;
  private Actor m_heroPowerActor;
  private Actor m_goldenHeroPowerActor;
  private bool m_wasTouchModeEnabled;

  private void Awake()
  {
    this.m_manaCurveTooltipText.Text = GameStrings.Get("GLUE_COLLECTION_DECK_INFO_MANA_TOOLTIP");
    using (List<DeckInfoManaBar>.Enumerator enumerator = this.m_manaBars.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckInfoManaBar current = enumerator.Current;
        current.m_costText.Text = this.GetTextForManaCost(current.m_manaCostID);
      }
    }
    AssetLoader.Get().LoadActor("Card_Play_HeroPower", new AssetLoader.GameObjectCallback(this.OnHeroPowerActorLoaded), (object) null, false);
    AssetLoader.Get().LoadActor(ActorNames.GetNameWithPremiumType("Card_Play_HeroPower", TAG_PREMIUM.GOLDEN), new AssetLoader.GameObjectCallback(this.OnGoldenHeroPowerActorLoaded), (object) null, false);
    this.m_wasTouchModeEnabled = true;
  }

  private void Start()
  {
    this.m_offClicker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClosePressed));
    this.m_offClicker.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OverOffClicker));
  }

  private void Update()
  {
    if (this.m_wasTouchModeEnabled == UniversalInputManager.Get().IsTouchMode())
      return;
    this.m_wasTouchModeEnabled = UniversalInputManager.Get().IsTouchMode();
    if (UniversalInputManager.Get().IsTouchMode())
    {
      if ((UnityEngine.Object) this.m_heroPowerActor != (UnityEngine.Object) null)
        this.m_heroPowerActor.TurnOffCollider();
      if ((UnityEngine.Object) this.m_goldenHeroPowerActor != (UnityEngine.Object) null)
        this.m_goldenHeroPowerActor.TurnOffCollider();
      this.m_offClicker.gameObject.SetActive(true);
    }
    else
    {
      if ((UnityEngine.Object) this.m_heroPowerActor != (UnityEngine.Object) null)
        this.m_heroPowerActor.TurnOnCollider();
      if ((UnityEngine.Object) this.m_goldenHeroPowerActor != (UnityEngine.Object) null)
        this.m_goldenHeroPowerActor.TurnOnCollider();
      this.m_offClicker.gameObject.SetActive(true);
    }
  }

  public void Show()
  {
    if (this.m_shown)
      return;
    this.m_root.SetActive(true);
    this.m_shown = true;
    if (UniversalInputManager.Get().IsTouchMode())
      Navigation.Push(new Navigation.NavigateBackHandler(this.GoBackImpl));
    foreach (CollectionDeckInfo.ShowListener showListener in this.m_showListeners.ToArray())
      showListener();
  }

  private bool GoBackImpl()
  {
    this.Hide();
    return true;
  }

  public void Hide()
  {
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.GoBackImpl));
    if (!this.m_shown)
      return;
    this.m_root.SetActive(false);
    this.m_shown = false;
    foreach (CollectionDeckInfo.HideListener hideListener in this.m_hideListeners.ToArray())
      hideListener();
  }

  public void RegisterShowListener(CollectionDeckInfo.ShowListener dlg)
  {
    this.m_showListeners.Add(dlg);
  }

  public void UnregisterShowListener(CollectionDeckInfo.ShowListener dlg)
  {
    this.m_showListeners.Remove(dlg);
  }

  public void RegisterHideListener(CollectionDeckInfo.HideListener dlg)
  {
    this.m_hideListeners.Add(dlg);
  }

  public void UnregisterHideListener(CollectionDeckInfo.HideListener dlg)
  {
    this.m_hideListeners.Remove(dlg);
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  public void UpdateManaCurve()
  {
    this.UpdateManaCurve(CollectionDeckTray.Get().GetCardsContent().GetEditingDeck());
  }

  public void UpdateManaCurve(CollectionDeck deck)
  {
    if (deck == null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckInfo.UpdateManaCurve(): deck is null."));
    }
    else
    {
      DefLoader.Get().LoadCardDef(deck.HeroCardID, new DefLoader.LoadDefCallback<CardDef>(this.OnHeroCardDefLoaded), new object(), new CardPortraitQuality(3, TAG_PREMIUM.NORMAL));
      using (List<DeckInfoManaBar>.Enumerator enumerator = this.m_manaBars.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.m_numCards = 0;
      }
      int num = 0;
      using (List<CollectionDeckSlot>.Enumerator enumerator = deck.GetSlots().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CollectionDeckSlot current = enumerator.Current;
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          CollectionDeckInfo.\u003CUpdateManaCurve\u003Ec__AnonStorey380 curveCAnonStorey380 = new CollectionDeckInfo.\u003CUpdateManaCurve\u003Ec__AnonStorey380();
          EntityDef entityDef = DefLoader.Get().GetEntityDef(current.CardID);
          // ISSUE: reference to a compiler-generated field
          curveCAnonStorey380.manaCost = entityDef.GetCost();
          // ISSUE: reference to a compiler-generated field
          if (curveCAnonStorey380.manaCost > this.MAX_MANA_COST_ID)
          {
            // ISSUE: reference to a compiler-generated field
            curveCAnonStorey380.manaCost = this.MAX_MANA_COST_ID;
          }
          // ISSUE: reference to a compiler-generated method
          DeckInfoManaBar deckInfoManaBar = this.m_manaBars.Find(new Predicate<DeckInfoManaBar>(curveCAnonStorey380.\u003C\u003Em__AF));
          if (deckInfoManaBar == null)
          {
            // ISSUE: reference to a compiler-generated field
            UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckInfo.UpdateManaCurve(): Cannot update curve. Could not find mana bar for {0} (cost {1})", (object) entityDef, (object) curveCAnonStorey380.manaCost));
            return;
          }
          deckInfoManaBar.m_numCards += current.Count;
          if (deckInfoManaBar.m_numCards > num)
            num = deckInfoManaBar.m_numCards;
        }
      }
      using (List<DeckInfoManaBar>.Enumerator enumerator = this.m_manaBars.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          DeckInfoManaBar current = enumerator.Current;
          current.m_numCardsText.Text = Convert.ToString(current.m_numCards);
          float t = num != 0 ? (float) current.m_numCards / (float) num : 0.0f;
          Vector3 localPosition = current.m_numCardsText.transform.localPosition;
          localPosition.z = Mathf.Lerp(this.MANA_COST_TEXT_MIN_LOCAL_Z, this.MANA_COST_TEXT_MAX_LOCAL_Z, t);
          current.m_numCardsText.transform.localPosition = localPosition;
          current.m_barFill.GetComponent<Renderer>().material.SetFloat("_Percent", t);
        }
      }
    }
  }

  public void SetDeck(CollectionDeck deck)
  {
    if (deck == null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckInfo.SetDeckID(): deck is null"));
    }
    else
    {
      this.UpdateManaCurve(deck);
      string powerCardIdFromHero = GameUtils.GetHeroPowerCardIdFromHero(deck.HeroCardID);
      if (string.IsNullOrEmpty(powerCardIdFromHero))
      {
        UnityEngine.Debug.LogWarning((object) "CollectionDeckInfo.UpdateInfo(): invalid hero power ID");
        this.m_heroPowerID = string.Empty;
      }
      else
      {
        if (powerCardIdFromHero.Equals(this.m_heroPowerID))
          return;
        this.m_heroPowerID = powerCardIdFromHero;
        DefLoader.Get().LoadFullDef(this.m_heroPowerID, new DefLoader.LoadDefCallback<FullDef>(this.OnHeroPowerFullDefLoaded), (object) CollectionManager.Get().GetBestCardPremium(CollectionManager.Get().GetVanillaHeroCardIDFromClass(deck.GetClass())));
      }
    }
  }

  private string GetTextForManaCost(int manaCostID)
  {
    if (manaCostID < this.MIN_MANA_COST_ID || manaCostID > this.MAX_MANA_COST_ID)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckInfo.GetTextForManaCost(): don't know how to handle mana cost ID {0}", (object) manaCostID));
      return string.Empty;
    }
    string str = Convert.ToString(manaCostID);
    if (manaCostID == this.MAX_MANA_COST_ID)
      str += GameStrings.Get("GLUE_COLLECTION_PLUS");
    return str;
  }

  private void OnHeroPowerActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckInfo.OnHeroPowerActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      this.m_heroPowerActor = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) this.m_heroPowerActor == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckInfo.OnHeroPowerActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        this.m_heroPowerActor.SetUnlit();
        this.m_heroPowerActor.transform.parent = this.m_heroPowerParent.transform;
        this.m_heroPowerActor.transform.localScale = Vector3.one;
        this.m_heroPowerActor.transform.localPosition = Vector3.zero;
        if (!UniversalInputManager.Get().IsTouchMode())
          return;
        this.m_heroPowerActor.TurnOffCollider();
      }
    }
  }

  private void OnGoldenHeroPowerActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckInfo.OnHeroPowerActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      this.m_goldenHeroPowerActor = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) this.m_goldenHeroPowerActor == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckInfo.OnGoldenHeroPowerActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        this.m_goldenHeroPowerActor.SetUnlit();
        this.m_goldenHeroPowerActor.transform.parent = this.m_heroPowerParent.transform;
        this.m_goldenHeroPowerActor.transform.localScale = Vector3.one;
        this.m_goldenHeroPowerActor.transform.localPosition = Vector3.zero;
        if (!UniversalInputManager.Get().IsTouchMode())
          return;
        this.m_goldenHeroPowerActor.TurnOffCollider();
      }
    }
  }

  private void OnHeroPowerFullDefLoaded(string cardID, FullDef def, object userData)
  {
    if ((UnityEngine.Object) this.m_heroPowerActor != (UnityEngine.Object) null)
      this.SetHeroPowerInfo(cardID, def, (TAG_PREMIUM) userData);
    else
      this.StartCoroutine(this.SetHeroPowerInfoWhenReady(cardID, def, (TAG_PREMIUM) userData));
  }

  [DebuggerHidden]
  private IEnumerator SetHeroPowerInfoWhenReady(string heroPowerCardID, FullDef def, TAG_PREMIUM premium)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionDeckInfo.\u003CSetHeroPowerInfoWhenReady\u003Ec__Iterator31()
    {
      heroPowerCardID = heroPowerCardID,
      def = def,
      premium = premium,
      \u003C\u0024\u003EheroPowerCardID = heroPowerCardID,
      \u003C\u0024\u003Edef = def,
      \u003C\u0024\u003Epremium = premium,
      \u003C\u003Ef__this = this
    };
  }

  private void SetHeroPowerInfo(string heroPowerCardID, FullDef def, TAG_PREMIUM premium)
  {
    if (!heroPowerCardID.Equals(this.m_heroPowerID))
      return;
    EntityDef entityDef = def.GetEntityDef();
    if (premium == TAG_PREMIUM.GOLDEN)
    {
      if ((UnityEngine.Object) this.m_heroPowerActor != (UnityEngine.Object) null)
        this.m_heroPowerActor.Hide();
      this.m_goldenHeroPowerActor.Show();
      this.m_goldenHeroPowerActor.SetEntityDef(def.GetEntityDef());
      this.m_goldenHeroPowerActor.SetCardDef(def.GetCardDef());
      this.m_goldenHeroPowerActor.SetUnlit();
      this.m_goldenHeroPowerActor.SetPremium(premium);
      this.m_goldenHeroPowerActor.UpdateAllComponents();
    }
    else
    {
      if ((UnityEngine.Object) this.m_goldenHeroPowerActor != (UnityEngine.Object) null)
        this.m_goldenHeroPowerActor.Hide();
      this.m_heroPowerActor.Show();
      this.m_heroPowerActor.SetEntityDef(def.GetEntityDef());
      this.m_heroPowerActor.SetCardDef(def.GetCardDef());
      this.m_heroPowerActor.SetUnlit();
      this.m_heroPowerActor.UpdateAllComponents();
    }
    this.m_heroPowerName.Text = entityDef.GetName();
    this.m_heroPowerDescription.Text = entityDef.GetCardTextInHand();
  }

  private void OnHeroCardDefLoaded(string cardId, CardDef def, object userData)
  {
  }

  private void OnClosePressed(UIEvent e)
  {
    this.Hide();
  }

  private void OverOffClicker(UIEvent e)
  {
    this.Hide();
  }

  public delegate void ShowListener();

  public delegate void HideListener();
}
