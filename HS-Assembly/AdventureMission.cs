﻿// Decompiled with JetBrains decompiler
// Type: AdventureMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class AdventureMission
{
  private int m_scenarioID;
  private string m_description;
  private AdventureMission.WingProgress m_requiredProgress;
  private AdventureMission.WingProgress m_grantedProgress;

  public int ScenarioID
  {
    get
    {
      return this.m_scenarioID;
    }
  }

  public string Description
  {
    get
    {
      return this.m_description;
    }
  }

  public AdventureMission.WingProgress RequiredProgress
  {
    get
    {
      return this.m_requiredProgress;
    }
  }

  public AdventureMission.WingProgress GrantedProgress
  {
    get
    {
      return this.m_grantedProgress;
    }
  }

  public AdventureMission(int scenarioID, string description, AdventureMission.WingProgress requiredProgress, AdventureMission.WingProgress grantedProgress)
  {
    this.m_scenarioID = scenarioID;
    this.m_description = description;
    this.m_requiredProgress = !requiredProgress.IsEmpty() ? requiredProgress : (AdventureMission.WingProgress) null;
    this.m_grantedProgress = !grantedProgress.IsEmpty() ? grantedProgress : (AdventureMission.WingProgress) null;
  }

  public bool HasRequiredProgress()
  {
    return this.m_requiredProgress != null;
  }

  public bool HasGrantedProgress()
  {
    return this.m_grantedProgress != null;
  }

  public override string ToString()
  {
    return string.Format("[AdventureMission: ScenarioID={0}, Description={1} RequiredProgress={2} GrantedProgress={3}]", (object) this.ScenarioID, (object) this.Description, (object) this.RequiredProgress, (object) this.GrantedProgress);
  }

  public class WingProgress
  {
    private int m_wing;
    private int m_progress;
    private ulong m_flags;

    public int Wing
    {
      get
      {
        return this.m_wing;
      }
    }

    public int Progress
    {
      get
      {
        return this.m_progress;
      }
    }

    public ulong Flags
    {
      get
      {
        return this.m_flags;
      }
    }

    public WingProgress(int wing, int progress, ulong flags)
    {
      this.m_wing = wing;
      this.m_progress = progress;
      this.m_flags = flags;
    }

    public bool IsEmpty()
    {
      if (this.Wing == 0)
        return true;
      if (this.Progress > 0)
        return false;
      return (long) this.Flags == 0L;
    }

    public bool IsOwned()
    {
      return this.MeetsFlagsRequirement(1UL);
    }

    public bool MeetsProgressRequirement(int requiredProgress)
    {
      return this.Progress >= requiredProgress;
    }

    public bool MeetsFlagsRequirement(ulong requiredFlags)
    {
      return ((long) this.Flags & (long) requiredFlags) == (long) requiredFlags;
    }

    public bool MeetsProgressAndFlagsRequirements(int requiredProgress, ulong requiredFlags)
    {
      if (this.MeetsProgressRequirement(requiredProgress))
        return this.MeetsFlagsRequirement(requiredFlags);
      return false;
    }

    public bool MeetsProgressAndFlagsRequirements(AdventureMission.WingProgress requiredProgress)
    {
      if (requiredProgress == null)
        return true;
      if (requiredProgress.Wing != this.Wing)
        return false;
      return this.MeetsProgressAndFlagsRequirements(requiredProgress.Progress, requiredProgress.Flags);
    }

    public void SetProgress(int progress)
    {
      if (this.m_progress > progress)
        return;
      this.m_progress = progress;
    }

    public void SetFlags(ulong flags)
    {
      this.m_flags = flags;
    }

    public override string ToString()
    {
      return string.Format("[AdventureMission.WingProgress: Wing={0}, Progress={1} Flags={2}]", (object) this.Wing, (object) this.Progress, (object) this.Flags);
    }

    public AdventureMission.WingProgress Clone()
    {
      return new AdventureMission.WingProgress(this.Wing, this.Progress, this.Flags);
    }
  }
}
