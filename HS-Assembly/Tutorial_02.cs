﻿// Decompiled with JetBrains decompiler
// Type: Tutorial_02
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Tutorial_02 : TutorialEntity
{
  private int numManaThisTurn = 1;
  private Notification endTurnNotifier;
  private Notification manaNotifier;
  private Notification manaNotifier2;
  private Notification handBounceArrow;
  private GameObject costLabel;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_02_05");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_01_04");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_04_07");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_05_08");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_07_10");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_11_14");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_13_16");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_15_17");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_06_09");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_03_06");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_17_19");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_08_11");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_09_12");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_10_13");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_16_18");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_20_22_ALT");
    this.PreloadSound("VO_TUTORIAL_02_JAINA_08_22");
    this.PreloadSound("VO_TUTORIAL_02_JAINA_03_18");
    this.PreloadSound("VO_TUTORIAL02_MILLHOUSE_19_21");
  }

  public override void NotifyOfGameOver(TAG_PLAYSTATE gameResult)
  {
    base.NotifyOfGameOver(gameResult);
    if (gameResult == TAG_PLAYSTATE.WON)
    {
      this.SetTutorialProgress(TutorialProgress.MILLHOUSE_COMPLETE);
      this.PlaySound("VO_TUTORIAL02_MILLHOUSE_20_22_ALT", 1f, true, false);
    }
    else
    {
      if (gameResult != TAG_PLAYSTATE.TIED)
        return;
      this.PlaySound("VO_TUTORIAL02_MILLHOUSE_20_22_ALT", 1f, true, false);
    }
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_02.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator20F() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_02.\u003CHandleMissionEventWithTiming\u003Ec__Iterator210() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  public override void NotifyOfCardMousedOver(Entity mousedOverEntity)
  {
    if (mousedOverEntity.GetZone() != TAG_ZONE.HAND || this.GetTag(GAME_TAG.TURN) > 7)
      return;
    AssetLoader.Get().LoadActor("NumberLabel", new AssetLoader.GameObjectCallback(this.ManaLabelLoadedCallback), (object) mousedOverEntity.GetCard(), false);
  }

  public override void NotifyOfCardMousedOff(Entity mousedOffEntity)
  {
    if (!((Object) this.costLabel != (Object) null))
      return;
    Object.Destroy((Object) this.costLabel);
  }

  public override void NotifyOfCoinFlipResult()
  {
    Gameplay.Get().StartCoroutine(this.HandleCoinFlip());
  }

  [DebuggerHidden]
  private IEnumerator HandleCoinFlip()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_02.\u003CHandleCoinFlip\u003Ec__Iterator211() { \u003C\u003Ef__this = this };
  }

  public override bool NotifyOfEndTurnButtonPushed()
  {
    Network.Options optionsPacket = GameState.Get().GetOptionsPacket();
    if (optionsPacket != null && optionsPacket.List != null)
    {
      if (optionsPacket.List.Count == 1)
      {
        NotificationManager.Get().DestroyAllArrows();
        return true;
      }
      if (optionsPacket.List.Count == 2)
      {
        for (int index = 0; index < optionsPacket.List.Count; ++index)
        {
          Network.Options.Option option = optionsPacket.List[index];
          if (option.Type == Network.Options.Option.OptionType.POWER && GameState.Get().GetEntity(option.Main.ID).GetCardId() == "CS2_025")
            return true;
        }
      }
    }
    if ((Object) this.endTurnNotifier != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.endTurnNotifier);
    Vector3 position1 = EndTurnButton.Get().transform.position;
    Vector3 position2 = new Vector3(position1.x - 3f, position1.y, position1.z);
    string key = "TUTORIAL_NO_ENDTURN";
    if (GameState.Get().GetFriendlySidePlayer().HasReadyAttackers())
      key = "TUTORIAL_NO_ENDTURN_ATK";
    this.endTurnNotifier = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, position2, TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get(key), true);
    NotificationManager.Get().DestroyNotification(this.endTurnNotifier, 2.5f);
    return false;
  }

  private void ShowEndTurnBouncingArrow()
  {
    if (EndTurnButton.Get().IsInWaitingState())
      return;
    Vector3 position = EndTurnButton.Get().transform.position;
    NotificationManager.Get().CreateBouncingArrow(UserAttentionBlocker.NONE, new Vector3(position.x - 2f, position.y, position.z), new Vector3(0.0f, -90f, 0.0f));
  }

  public override string[] NotifyOfKeywordHelpPanelDisplay(Entity entity)
  {
    if (entity.GetCardId() == "CS2_122")
      return new string[2]{ GameStrings.Get("TUTORIAL_RAID_LEADER_HEADLINE"), GameStrings.Get("TUTORIAL_RAID_LEADER_DESCRIPTION") };
    if (!(entity.GetCardId() == "CS2_023"))
      return (string[]) null;
    return new string[2]{ GameStrings.Get("TUTORIAL_ARCANE_INTELLECT_HEADLINE"), GameStrings.Get("TUTORIAL_ARCANE_INTELLECT_DESCRIPTION") };
  }

  public override void NotifyOfCardGrabbed(Entity entity)
  {
    if (entity.GetCardId() == "CS2_023" && GameState.Get().GetFriendlySidePlayer().GetNumAvailableResources() >= entity.GetCost())
      BoardTutorial.Get().EnableFullHighlight(true);
    if (!((Object) this.costLabel != (Object) null))
      return;
    Object.Destroy((Object) this.costLabel);
  }

  public override void NotifyOfCardDropped(Entity entity)
  {
    if (!(entity.GetCardId() == "CS2_023"))
      return;
    BoardTutorial.Get().EnableFullHighlight(false);
  }

  public override void NotifyOfManaCrystalSpawned()
  {
    AssetLoader.Get().LoadActor("plus1", new AssetLoader.GameObjectCallback(this.Plus1ActorLoadedCallback), (object) null, false);
    if (this.GetTag(GAME_TAG.TURN) == 3)
      Gameplay.Get().StartCoroutine(this.PlaySoundAndWait("VO_TUTORIAL_02_JAINA_08_22", "TUTORIAL02_JAINA_08", Notification.SpeechBubbleDirection.BottomLeft, GameState.Get().GetFriendlySidePlayer().GetHero().GetCard().GetActor(), 1f, true, false, 3f, 0.0f));
    this.FadeInManaSpotlight();
  }

  private void FadeInManaSpotlight()
  {
    Gameplay.Get().StartCoroutine(this.StartManaSpotFade());
  }

  [DebuggerHidden]
  private IEnumerator StartManaSpotFade()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Tutorial_02.\u003CStartManaSpotFade\u003Ec__Iterator212 fadeCIterator212 = new Tutorial_02.\u003CStartManaSpotFade\u003Ec__Iterator212();
    return (IEnumerator) fadeCIterator212;
  }

  private void Plus1ActorLoadedCallback(string actorName, GameObject actorObject, object callbackData)
  {
    Vector3 crystalSpawnPosition = ManaCrystalMgr.Get().GetManaCrystalSpawnPosition();
    Vector3 vector3 = new Vector3(crystalSpawnPosition.x - 0.02f, crystalSpawnPosition.y + 0.2f, crystalSpawnPosition.z);
    actorObject.transform.position = vector3;
    actorObject.transform.localEulerAngles = new Vector3(0.0f, 180f, 0.0f);
    Vector3 localScale = actorObject.transform.localScale;
    actorObject.transform.localScale = new Vector3(1f, 1f, 1f);
    iTween.MoveTo(actorObject, new Vector3(vector3.x, vector3.y, vector3.z + 2f), 3f);
    float num = 2.5f;
    iTween.ScaleTo(actorObject, new Vector3(localScale.x * num, localScale.y * num, localScale.z * num), 3f);
    iTween.RotateTo(actorObject, new Vector3(0.0f, 170f, 0.0f), 3f);
    iTween.FadeTo(actorObject, 0.0f, 2.75f);
  }

  public override void NotifyOfEnemyManaCrystalSpawned()
  {
    AssetLoader.Get().LoadActor("plus1", new AssetLoader.GameObjectCallback(this.Plus1ActorLoadedCallbackEnemy), (object) null, false);
  }

  private void Plus1ActorLoadedCallbackEnemy(string actorName, GameObject actorObject, object callbackData)
  {
    Vector3 position = SceneUtils.FindChildBySubstring(Board.Get().gameObject, "ManaCounter_Opposing").transform.position;
    Vector3 vector3 = new Vector3(position.x, position.y + 0.2f, position.z);
    actorObject.transform.position = vector3;
    actorObject.transform.localEulerAngles = new Vector3(0.0f, 180f, 0.0f);
    Vector3 localScale = actorObject.transform.localScale;
    actorObject.transform.localScale = new Vector3(1f, 1f, 1f);
    iTween.MoveTo(actorObject, new Vector3(vector3.x, vector3.y, vector3.z - 2f), 3f);
    float num = 2.5f;
    iTween.ScaleTo(actorObject, new Vector3(localScale.x * num, localScale.y * num, localScale.z * num), 3f);
    iTween.RotateTo(actorObject, new Vector3(0.0f, 170f, 0.0f), 3f);
    iTween.FadeTo(actorObject, 0.0f, 2.75f);
  }

  private void ManaLabelLoadedCallback(string actorName, GameObject actorObject, object callbackData)
  {
    GameObject costTextObject = ((Card) callbackData).GetActor().GetCostTextObject();
    if ((Object) costTextObject == (Object) null)
    {
      Object.Destroy((Object) actorObject);
    }
    else
    {
      if ((Object) this.costLabel != (Object) null)
        Object.Destroy((Object) this.costLabel);
      this.costLabel = actorObject;
      actorObject.transform.parent = costTextObject.transform;
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.transform.localPosition = new Vector3(-0.025f, 0.28f, 0.0f);
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.GetComponent<UberText>().Text = GameStrings.Get("GLOBAL_COST");
    }
  }

  public override void NotifyOfTooltipZoneMouseOver(TooltipZone tooltip)
  {
    if (!((Object) tooltip.targetObject.GetComponent<ManaCrystalMgr>() != (Object) null))
      return;
    if ((Object) this.manaNotifier != (Object) null)
      Object.Destroy((Object) this.manaNotifier.gameObject);
    if (!((Object) this.manaNotifier2 != (Object) null))
      return;
    Object.Destroy((Object) this.manaNotifier2.gameObject);
  }

  public override string GetTurnStartReminderText()
  {
    return GameStrings.Format("TUTORIAL02_HELP_04", (object) this.numManaThisTurn);
  }

  public override void NotifyOfDefeatCoinAnimation()
  {
    Gameplay.Get().StartCoroutine(this.PlayGoingOutSound());
  }

  [DebuggerHidden]
  private IEnumerator PlayGoingOutSound()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_02.\u003CPlayGoingOutSound\u003Ec__Iterator213() { \u003C\u003Ef__this = this };
  }

  protected override void NotifyOfManaError()
  {
    NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.manaNotifier);
    NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.manaNotifier2);
  }

  public override List<RewardData> GetCustomRewards()
  {
    List<RewardData> rewardDataList = new List<RewardData>();
    CardRewardData cardRewardData = new CardRewardData("EX1_015", TAG_PREMIUM.NORMAL, 2);
    cardRewardData.MarkAsDummyReward();
    rewardDataList.Add((RewardData) cardRewardData);
    return rewardDataList;
  }
}
