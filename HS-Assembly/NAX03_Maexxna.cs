﻿// Decompiled with JetBrains decompiler
// Type: NAX03_Maexxna
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class NAX03_Maexxna : NAX_MissionEntity
{
  private bool m_heroPowerLinePlayed;
  private bool m_seaGiantLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_NAX3_01_EMOTE_01");
    this.PreloadSound("VO_NAX3_01_EMOTE_02");
    this.PreloadSound("VO_NAX3_01_EMOTE_03");
    this.PreloadSound("VO_NAX3_01_EMOTE_04");
    this.PreloadSound("VO_NAX3_01_EMOTE_05");
    this.PreloadSound("VO_NAX3_01_CARD_01");
    this.PreloadSound("VO_NAX3_01_HP_01");
    this.PreloadSound("VO_KT_MAEXXNA2_47");
    this.PreloadSound("VO_KT_MAEXXNA6_51");
    this.PreloadSound("VO_KT_MAEXXNA3_48");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX3_01_EMOTE_01",
            m_stringTag = "VO_NAX3_01_EMOTE_01"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX3_01_EMOTE_02",
            m_stringTag = "VO_NAX3_01_EMOTE_02"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX3_01_EMOTE_03",
            m_stringTag = "VO_NAX3_01_EMOTE_03"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX3_01_EMOTE_04",
            m_stringTag = "VO_NAX3_01_EMOTE_04"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX3_01_EMOTE_05",
            m_stringTag = "VO_NAX3_01_EMOTE_05"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX03_Maexxna.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1C8() { turn = turn, \u003C\u0024\u003Eturn = turn };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX03_Maexxna.\u003CHandleGameOverWithTiming\u003Ec__Iterator1C9() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX03_Maexxna.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1CA() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }
}
