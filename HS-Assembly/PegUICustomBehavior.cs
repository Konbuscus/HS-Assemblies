﻿// Decompiled with JetBrains decompiler
// Type: PegUICustomBehavior
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class PegUICustomBehavior : MonoBehaviour
{
  protected virtual void Awake()
  {
    PegUI.Get().RegisterCustomBehavior(this);
  }

  protected virtual void OnDestroy()
  {
    if (!((Object) PegUI.Get() != (Object) null))
      return;
    PegUI.Get().UnregisterCustomBehavior(this);
  }

  public abstract bool UpdateUI();
}
