﻿// Decompiled with JetBrains decompiler
// Type: DeckRule_DeckSize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DeckRule_DeckSize : DeckRule
{
  public DeckRule_DeckSize(int size)
  {
    this.m_ruleType = DeckRule.RuleType.DECK_SIZE;
    this.m_minValue = size;
    this.m_maxValue = size;
  }

  public DeckRule_DeckSize(DeckRulesetRuleDbfRecord record)
    : base(DeckRule.RuleType.DECK_SIZE, record)
  {
    if (this.m_ruleIsNot)
      Debug.LogError((object) "DECK_SIZE rules do not support \"is not\".");
    if (this.m_appliesToSubset == null)
      return;
    Debug.LogError((object) "DECK_SIZE rules do not support \"applies to subset\".");
  }

  public override bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason)
  {
    reason = (RuleInvalidReason) null;
    int totalCardCount = deck.GetTotalCardCount();
    int countParam = 0;
    bool isMinimum = false;
    bool val = true;
    if (totalCardCount < this.m_minValue)
    {
      val = false;
      countParam = this.m_minValue - totalCardCount;
      isMinimum = true;
    }
    else if (totalCardCount > this.m_maxValue)
    {
      val = false;
      countParam = totalCardCount - this.m_maxValue;
    }
    bool result = this.GetResult(val);
    if (!result)
    {
      string error;
      if (totalCardCount < this.m_minValue)
        error = GameStrings.Format("GLUE_COLLECTION_DECK_RULE_MISSING_CARDS", (object) countParam);
      else
        error = GameStrings.Format("GLUE_COLLECTION_DECK_RULE_TOO_MANY_CARDS", (object) countParam);
      reason = new RuleInvalidReason(error, countParam, isMinimum);
    }
    return result;
  }

  public int GetDeckSize()
  {
    return this.m_maxValue;
  }
}
