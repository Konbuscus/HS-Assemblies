﻿// Decompiled with JetBrains decompiler
// Type: AdventureDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AdventureDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private int m_SortOrder;
  [SerializeField]
  private DbfLocValue m_StoreBuyButtonLabel;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings1Headline;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings2Headline;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings3Headline;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings4Headline;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings5Headline;
  [SerializeField]
  private DbfLocValue m_StoreOwnedHeadline;
  [SerializeField]
  private DbfLocValue m_StorePreorderHeadline;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings1Desc;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings2Desc;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings3Desc;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings4Desc;
  [SerializeField]
  private DbfLocValue m_StoreBuyWings5Desc;
  [SerializeField]
  private DbfLocValue m_StoreOwnedDesc;
  [SerializeField]
  private DbfLocValue m_StorePreorderWings1Desc;
  [SerializeField]
  private DbfLocValue m_StorePreorderWings2Desc;
  [SerializeField]
  private DbfLocValue m_StorePreorderWings3Desc;
  [SerializeField]
  private DbfLocValue m_StorePreorderWings4Desc;
  [SerializeField]
  private DbfLocValue m_StorePreorderWings5Desc;
  [SerializeField]
  private DbfLocValue m_StorePreorderRadioText;
  [SerializeField]
  private DbfLocValue m_StorePreviewRewardsText;
  [SerializeField]
  private string m_AdventureDefPrefab;
  [SerializeField]
  private string m_StorePrefab;
  [SerializeField]
  private bool m_LeavingSoon;
  [SerializeField]
  private DbfLocValue m_LeavingSoonText;
  [SerializeField]
  private string m_GameModeIcon;
  [SerializeField]
  private string m_ProductStringKey;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("NAME", "")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("SORT_ORDER", "")]
  public int SortOrder
  {
    get
    {
      return this.m_SortOrder;
    }
  }

  [DbfField("STORE_BUY_BUTTON_LABEL", "")]
  public DbfLocValue StoreBuyButtonLabel
  {
    get
    {
      return this.m_StoreBuyButtonLabel;
    }
  }

  [DbfField("STORE_BUY_WINGS_1_HEADLINE", "")]
  public DbfLocValue StoreBuyWings1Headline
  {
    get
    {
      return this.m_StoreBuyWings1Headline;
    }
  }

  [DbfField("STORE_BUY_WINGS_2_HEADLINE", "")]
  public DbfLocValue StoreBuyWings2Headline
  {
    get
    {
      return this.m_StoreBuyWings2Headline;
    }
  }

  [DbfField("STORE_BUY_WINGS_3_HEADLINE", "")]
  public DbfLocValue StoreBuyWings3Headline
  {
    get
    {
      return this.m_StoreBuyWings3Headline;
    }
  }

  [DbfField("STORE_BUY_WINGS_4_HEADLINE", "")]
  public DbfLocValue StoreBuyWings4Headline
  {
    get
    {
      return this.m_StoreBuyWings4Headline;
    }
  }

  [DbfField("STORE_BUY_WINGS_5_HEADLINE", "")]
  public DbfLocValue StoreBuyWings5Headline
  {
    get
    {
      return this.m_StoreBuyWings5Headline;
    }
  }

  [DbfField("STORE_OWNED_HEADLINE", "")]
  public DbfLocValue StoreOwnedHeadline
  {
    get
    {
      return this.m_StoreOwnedHeadline;
    }
  }

  [DbfField("STORE_PREORDER_HEADLINE", "")]
  public DbfLocValue StorePreorderHeadline
  {
    get
    {
      return this.m_StorePreorderHeadline;
    }
  }

  [DbfField("STORE_BUY_WINGS_1_DESC", "")]
  public DbfLocValue StoreBuyWings1Desc
  {
    get
    {
      return this.m_StoreBuyWings1Desc;
    }
  }

  [DbfField("STORE_BUY_WINGS_2_DESC", "")]
  public DbfLocValue StoreBuyWings2Desc
  {
    get
    {
      return this.m_StoreBuyWings2Desc;
    }
  }

  [DbfField("STORE_BUY_WINGS_3_DESC", "")]
  public DbfLocValue StoreBuyWings3Desc
  {
    get
    {
      return this.m_StoreBuyWings3Desc;
    }
  }

  [DbfField("STORE_BUY_WINGS_4_DESC", "")]
  public DbfLocValue StoreBuyWings4Desc
  {
    get
    {
      return this.m_StoreBuyWings4Desc;
    }
  }

  [DbfField("STORE_BUY_WINGS_5_DESC", "")]
  public DbfLocValue StoreBuyWings5Desc
  {
    get
    {
      return this.m_StoreBuyWings5Desc;
    }
  }

  [DbfField("STORE_OWNED_DESC", "")]
  public DbfLocValue StoreOwnedDesc
  {
    get
    {
      return this.m_StoreOwnedDesc;
    }
  }

  [DbfField("STORE_PREORDER_WINGS_1_DESC", "")]
  public DbfLocValue StorePreorderWings1Desc
  {
    get
    {
      return this.m_StorePreorderWings1Desc;
    }
  }

  [DbfField("STORE_PREORDER_WINGS_2_DESC", "")]
  public DbfLocValue StorePreorderWings2Desc
  {
    get
    {
      return this.m_StorePreorderWings2Desc;
    }
  }

  [DbfField("STORE_PREORDER_WINGS_3_DESC", "")]
  public DbfLocValue StorePreorderWings3Desc
  {
    get
    {
      return this.m_StorePreorderWings3Desc;
    }
  }

  [DbfField("STORE_PREORDER_WINGS_4_DESC", "")]
  public DbfLocValue StorePreorderWings4Desc
  {
    get
    {
      return this.m_StorePreorderWings4Desc;
    }
  }

  [DbfField("STORE_PREORDER_WINGS_5_DESC", "")]
  public DbfLocValue StorePreorderWings5Desc
  {
    get
    {
      return this.m_StorePreorderWings5Desc;
    }
  }

  [DbfField("STORE_PREORDER_RADIO_TEXT", "text here overrides GLUE_STORE_DUNGEON_BUTTON_PREORDER_TEXT in GLUE.txt")]
  public DbfLocValue StorePreorderRadioText
  {
    get
    {
      return this.m_StorePreorderRadioText;
    }
  }

  [DbfField("STORE_PREVIEW_REWARDS_TEXT", "")]
  public DbfLocValue StorePreviewRewardsText
  {
    get
    {
      return this.m_StorePreviewRewardsText;
    }
  }

  [DbfField("ADVENTURE_DEF_PREFAB", "")]
  public string AdventureDefPrefab
  {
    get
    {
      return this.m_AdventureDefPrefab;
    }
  }

  [DbfField("STORE_PREFAB", "")]
  public string StorePrefab
  {
    get
    {
      return this.m_StorePrefab;
    }
  }

  [DbfField("LEAVING_SOON", "this adventure will be removed from the in-game store soon")]
  public bool LeavingSoon
  {
    get
    {
      return this.m_LeavingSoon;
    }
  }

  [DbfField("LEAVING_SOON_TEXT", "localized text id explaining why this adventure will be removed from the in-game store soon")]
  public DbfLocValue LeavingSoonText
  {
    get
    {
      return this.m_LeavingSoonText;
    }
  }

  [DbfField("GAME_MODE_ICON", "specifies texture to use for game mode icons")]
  public string GameModeIcon
  {
    get
    {
      return this.m_GameModeIcon;
    }
  }

  [DbfField("PRODUCT_STRING_KEY", "adventure product string key used to target GLUE.txt strings")]
  public string ProductStringKey
  {
    get
    {
      return this.m_ProductStringKey;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    AdventureDbfAsset adventureDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (AdventureDbfAsset)) as AdventureDbfAsset;
    if ((UnityEngine.Object) adventureDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("AdventureDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < adventureDbfAsset.Records.Count; ++index)
      adventureDbfAsset.Records[index].StripUnusedLocales();
    records = (object) adventureDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Name.StripUnusedLocales();
    this.m_StoreBuyButtonLabel.StripUnusedLocales();
    this.m_StoreBuyWings1Headline.StripUnusedLocales();
    this.m_StoreBuyWings2Headline.StripUnusedLocales();
    this.m_StoreBuyWings3Headline.StripUnusedLocales();
    this.m_StoreBuyWings4Headline.StripUnusedLocales();
    this.m_StoreBuyWings5Headline.StripUnusedLocales();
    this.m_StoreOwnedHeadline.StripUnusedLocales();
    this.m_StorePreorderHeadline.StripUnusedLocales();
    this.m_StoreBuyWings1Desc.StripUnusedLocales();
    this.m_StoreBuyWings2Desc.StripUnusedLocales();
    this.m_StoreBuyWings3Desc.StripUnusedLocales();
    this.m_StoreBuyWings4Desc.StripUnusedLocales();
    this.m_StoreBuyWings5Desc.StripUnusedLocales();
    this.m_StoreOwnedDesc.StripUnusedLocales();
    this.m_StorePreorderWings1Desc.StripUnusedLocales();
    this.m_StorePreorderWings2Desc.StripUnusedLocales();
    this.m_StorePreorderWings3Desc.StripUnusedLocales();
    this.m_StorePreorderWings4Desc.StripUnusedLocales();
    this.m_StorePreorderWings5Desc.StripUnusedLocales();
    this.m_StorePreorderRadioText.StripUnusedLocales();
    this.m_StorePreviewRewardsText.StripUnusedLocales();
    this.m_LeavingSoonText.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetSortOrder(int v)
  {
    this.m_SortOrder = v;
  }

  public void SetStoreBuyButtonLabel(DbfLocValue v)
  {
    this.m_StoreBuyButtonLabel = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_BUTTON_LABEL");
  }

  public void SetStoreBuyWings1Headline(DbfLocValue v)
  {
    this.m_StoreBuyWings1Headline = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_1_HEADLINE");
  }

  public void SetStoreBuyWings2Headline(DbfLocValue v)
  {
    this.m_StoreBuyWings2Headline = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_2_HEADLINE");
  }

  public void SetStoreBuyWings3Headline(DbfLocValue v)
  {
    this.m_StoreBuyWings3Headline = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_3_HEADLINE");
  }

  public void SetStoreBuyWings4Headline(DbfLocValue v)
  {
    this.m_StoreBuyWings4Headline = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_4_HEADLINE");
  }

  public void SetStoreBuyWings5Headline(DbfLocValue v)
  {
    this.m_StoreBuyWings5Headline = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_5_HEADLINE");
  }

  public void SetStoreOwnedHeadline(DbfLocValue v)
  {
    this.m_StoreOwnedHeadline = v;
    v.SetDebugInfo(this.ID, "STORE_OWNED_HEADLINE");
  }

  public void SetStorePreorderHeadline(DbfLocValue v)
  {
    this.m_StorePreorderHeadline = v;
    v.SetDebugInfo(this.ID, "STORE_PREORDER_HEADLINE");
  }

  public void SetStoreBuyWings1Desc(DbfLocValue v)
  {
    this.m_StoreBuyWings1Desc = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_1_DESC");
  }

  public void SetStoreBuyWings2Desc(DbfLocValue v)
  {
    this.m_StoreBuyWings2Desc = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_2_DESC");
  }

  public void SetStoreBuyWings3Desc(DbfLocValue v)
  {
    this.m_StoreBuyWings3Desc = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_3_DESC");
  }

  public void SetStoreBuyWings4Desc(DbfLocValue v)
  {
    this.m_StoreBuyWings4Desc = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_4_DESC");
  }

  public void SetStoreBuyWings5Desc(DbfLocValue v)
  {
    this.m_StoreBuyWings5Desc = v;
    v.SetDebugInfo(this.ID, "STORE_BUY_WINGS_5_DESC");
  }

  public void SetStoreOwnedDesc(DbfLocValue v)
  {
    this.m_StoreOwnedDesc = v;
    v.SetDebugInfo(this.ID, "STORE_OWNED_DESC");
  }

  public void SetStorePreorderWings1Desc(DbfLocValue v)
  {
    this.m_StorePreorderWings1Desc = v;
    v.SetDebugInfo(this.ID, "STORE_PREORDER_WINGS_1_DESC");
  }

  public void SetStorePreorderWings2Desc(DbfLocValue v)
  {
    this.m_StorePreorderWings2Desc = v;
    v.SetDebugInfo(this.ID, "STORE_PREORDER_WINGS_2_DESC");
  }

  public void SetStorePreorderWings3Desc(DbfLocValue v)
  {
    this.m_StorePreorderWings3Desc = v;
    v.SetDebugInfo(this.ID, "STORE_PREORDER_WINGS_3_DESC");
  }

  public void SetStorePreorderWings4Desc(DbfLocValue v)
  {
    this.m_StorePreorderWings4Desc = v;
    v.SetDebugInfo(this.ID, "STORE_PREORDER_WINGS_4_DESC");
  }

  public void SetStorePreorderWings5Desc(DbfLocValue v)
  {
    this.m_StorePreorderWings5Desc = v;
    v.SetDebugInfo(this.ID, "STORE_PREORDER_WINGS_5_DESC");
  }

  public void SetStorePreorderRadioText(DbfLocValue v)
  {
    this.m_StorePreorderRadioText = v;
    v.SetDebugInfo(this.ID, "STORE_PREORDER_RADIO_TEXT");
  }

  public void SetStorePreviewRewardsText(DbfLocValue v)
  {
    this.m_StorePreviewRewardsText = v;
    v.SetDebugInfo(this.ID, "STORE_PREVIEW_REWARDS_TEXT");
  }

  public void SetAdventureDefPrefab(string v)
  {
    this.m_AdventureDefPrefab = v;
  }

  public void SetStorePrefab(string v)
  {
    this.m_StorePrefab = v;
  }

  public void SetLeavingSoon(bool v)
  {
    this.m_LeavingSoon = v;
  }

  public void SetLeavingSoonText(DbfLocValue v)
  {
    this.m_LeavingSoonText = v;
    v.SetDebugInfo(this.ID, "LEAVING_SOON_TEXT");
  }

  public void SetGameModeIcon(string v)
  {
    this.m_GameModeIcon = v;
  }

  public void SetProductStringKey(string v)
  {
    this.m_ProductStringKey = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureDbfRecord.\u003C\u003Ef__switch\u0024mapB == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureDbfRecord.\u003C\u003Ef__switch\u0024mapB = new Dictionary<string, int>(31)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "NAME",
            2
          },
          {
            "SORT_ORDER",
            3
          },
          {
            "STORE_BUY_BUTTON_LABEL",
            4
          },
          {
            "STORE_BUY_WINGS_1_HEADLINE",
            5
          },
          {
            "STORE_BUY_WINGS_2_HEADLINE",
            6
          },
          {
            "STORE_BUY_WINGS_3_HEADLINE",
            7
          },
          {
            "STORE_BUY_WINGS_4_HEADLINE",
            8
          },
          {
            "STORE_BUY_WINGS_5_HEADLINE",
            9
          },
          {
            "STORE_OWNED_HEADLINE",
            10
          },
          {
            "STORE_PREORDER_HEADLINE",
            11
          },
          {
            "STORE_BUY_WINGS_1_DESC",
            12
          },
          {
            "STORE_BUY_WINGS_2_DESC",
            13
          },
          {
            "STORE_BUY_WINGS_3_DESC",
            14
          },
          {
            "STORE_BUY_WINGS_4_DESC",
            15
          },
          {
            "STORE_BUY_WINGS_5_DESC",
            16
          },
          {
            "STORE_OWNED_DESC",
            17
          },
          {
            "STORE_PREORDER_WINGS_1_DESC",
            18
          },
          {
            "STORE_PREORDER_WINGS_2_DESC",
            19
          },
          {
            "STORE_PREORDER_WINGS_3_DESC",
            20
          },
          {
            "STORE_PREORDER_WINGS_4_DESC",
            21
          },
          {
            "STORE_PREORDER_WINGS_5_DESC",
            22
          },
          {
            "STORE_PREORDER_RADIO_TEXT",
            23
          },
          {
            "STORE_PREVIEW_REWARDS_TEXT",
            24
          },
          {
            "ADVENTURE_DEF_PREFAB",
            25
          },
          {
            "STORE_PREFAB",
            26
          },
          {
            "LEAVING_SOON",
            27
          },
          {
            "LEAVING_SOON_TEXT",
            28
          },
          {
            "GAME_MODE_ICON",
            29
          },
          {
            "PRODUCT_STRING_KEY",
            30
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureDbfRecord.\u003C\u003Ef__switch\u0024mapB.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.Name;
          case 3:
            return (object) this.SortOrder;
          case 4:
            return (object) this.StoreBuyButtonLabel;
          case 5:
            return (object) this.StoreBuyWings1Headline;
          case 6:
            return (object) this.StoreBuyWings2Headline;
          case 7:
            return (object) this.StoreBuyWings3Headline;
          case 8:
            return (object) this.StoreBuyWings4Headline;
          case 9:
            return (object) this.StoreBuyWings5Headline;
          case 10:
            return (object) this.StoreOwnedHeadline;
          case 11:
            return (object) this.StorePreorderHeadline;
          case 12:
            return (object) this.StoreBuyWings1Desc;
          case 13:
            return (object) this.StoreBuyWings2Desc;
          case 14:
            return (object) this.StoreBuyWings3Desc;
          case 15:
            return (object) this.StoreBuyWings4Desc;
          case 16:
            return (object) this.StoreBuyWings5Desc;
          case 17:
            return (object) this.StoreOwnedDesc;
          case 18:
            return (object) this.StorePreorderWings1Desc;
          case 19:
            return (object) this.StorePreorderWings2Desc;
          case 20:
            return (object) this.StorePreorderWings3Desc;
          case 21:
            return (object) this.StorePreorderWings4Desc;
          case 22:
            return (object) this.StorePreorderWings5Desc;
          case 23:
            return (object) this.StorePreorderRadioText;
          case 24:
            return (object) this.StorePreviewRewardsText;
          case 25:
            return (object) this.AdventureDefPrefab;
          case 26:
            return (object) this.StorePrefab;
          case 27:
            return (object) this.LeavingSoon;
          case 28:
            return (object) this.LeavingSoonText;
          case 29:
            return (object) this.GameModeIcon;
          case 30:
            return (object) this.ProductStringKey;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (AdventureDbfRecord.\u003C\u003Ef__switch\u0024mapC == null)
    {
      // ISSUE: reference to a compiler-generated field
      AdventureDbfRecord.\u003C\u003Ef__switch\u0024mapC = new Dictionary<string, int>(31)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "NAME",
          2
        },
        {
          "SORT_ORDER",
          3
        },
        {
          "STORE_BUY_BUTTON_LABEL",
          4
        },
        {
          "STORE_BUY_WINGS_1_HEADLINE",
          5
        },
        {
          "STORE_BUY_WINGS_2_HEADLINE",
          6
        },
        {
          "STORE_BUY_WINGS_3_HEADLINE",
          7
        },
        {
          "STORE_BUY_WINGS_4_HEADLINE",
          8
        },
        {
          "STORE_BUY_WINGS_5_HEADLINE",
          9
        },
        {
          "STORE_OWNED_HEADLINE",
          10
        },
        {
          "STORE_PREORDER_HEADLINE",
          11
        },
        {
          "STORE_BUY_WINGS_1_DESC",
          12
        },
        {
          "STORE_BUY_WINGS_2_DESC",
          13
        },
        {
          "STORE_BUY_WINGS_3_DESC",
          14
        },
        {
          "STORE_BUY_WINGS_4_DESC",
          15
        },
        {
          "STORE_BUY_WINGS_5_DESC",
          16
        },
        {
          "STORE_OWNED_DESC",
          17
        },
        {
          "STORE_PREORDER_WINGS_1_DESC",
          18
        },
        {
          "STORE_PREORDER_WINGS_2_DESC",
          19
        },
        {
          "STORE_PREORDER_WINGS_3_DESC",
          20
        },
        {
          "STORE_PREORDER_WINGS_4_DESC",
          21
        },
        {
          "STORE_PREORDER_WINGS_5_DESC",
          22
        },
        {
          "STORE_PREORDER_RADIO_TEXT",
          23
        },
        {
          "STORE_PREVIEW_REWARDS_TEXT",
          24
        },
        {
          "ADVENTURE_DEF_PREFAB",
          25
        },
        {
          "STORE_PREFAB",
          26
        },
        {
          "LEAVING_SOON",
          27
        },
        {
          "LEAVING_SOON_TEXT",
          28
        },
        {
          "GAME_MODE_ICON",
          29
        },
        {
          "PRODUCT_STRING_KEY",
          30
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!AdventureDbfRecord.\u003C\u003Ef__switch\u0024mapC.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetName((DbfLocValue) val);
        break;
      case 3:
        this.SetSortOrder((int) val);
        break;
      case 4:
        this.SetStoreBuyButtonLabel((DbfLocValue) val);
        break;
      case 5:
        this.SetStoreBuyWings1Headline((DbfLocValue) val);
        break;
      case 6:
        this.SetStoreBuyWings2Headline((DbfLocValue) val);
        break;
      case 7:
        this.SetStoreBuyWings3Headline((DbfLocValue) val);
        break;
      case 8:
        this.SetStoreBuyWings4Headline((DbfLocValue) val);
        break;
      case 9:
        this.SetStoreBuyWings5Headline((DbfLocValue) val);
        break;
      case 10:
        this.SetStoreOwnedHeadline((DbfLocValue) val);
        break;
      case 11:
        this.SetStorePreorderHeadline((DbfLocValue) val);
        break;
      case 12:
        this.SetStoreBuyWings1Desc((DbfLocValue) val);
        break;
      case 13:
        this.SetStoreBuyWings2Desc((DbfLocValue) val);
        break;
      case 14:
        this.SetStoreBuyWings3Desc((DbfLocValue) val);
        break;
      case 15:
        this.SetStoreBuyWings4Desc((DbfLocValue) val);
        break;
      case 16:
        this.SetStoreBuyWings5Desc((DbfLocValue) val);
        break;
      case 17:
        this.SetStoreOwnedDesc((DbfLocValue) val);
        break;
      case 18:
        this.SetStorePreorderWings1Desc((DbfLocValue) val);
        break;
      case 19:
        this.SetStorePreorderWings2Desc((DbfLocValue) val);
        break;
      case 20:
        this.SetStorePreorderWings3Desc((DbfLocValue) val);
        break;
      case 21:
        this.SetStorePreorderWings4Desc((DbfLocValue) val);
        break;
      case 22:
        this.SetStorePreorderWings5Desc((DbfLocValue) val);
        break;
      case 23:
        this.SetStorePreorderRadioText((DbfLocValue) val);
        break;
      case 24:
        this.SetStorePreviewRewardsText((DbfLocValue) val);
        break;
      case 25:
        this.SetAdventureDefPrefab((string) val);
        break;
      case 26:
        this.SetStorePrefab((string) val);
        break;
      case 27:
        this.SetLeavingSoon((bool) val);
        break;
      case 28:
        this.SetLeavingSoonText((DbfLocValue) val);
        break;
      case 29:
        this.SetGameModeIcon((string) val);
        break;
      case 30:
        this.SetProductStringKey((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AdventureDbfRecord.\u003C\u003Ef__switch\u0024mapD == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdventureDbfRecord.\u003C\u003Ef__switch\u0024mapD = new Dictionary<string, int>(31)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "NAME",
            2
          },
          {
            "SORT_ORDER",
            3
          },
          {
            "STORE_BUY_BUTTON_LABEL",
            4
          },
          {
            "STORE_BUY_WINGS_1_HEADLINE",
            5
          },
          {
            "STORE_BUY_WINGS_2_HEADLINE",
            6
          },
          {
            "STORE_BUY_WINGS_3_HEADLINE",
            7
          },
          {
            "STORE_BUY_WINGS_4_HEADLINE",
            8
          },
          {
            "STORE_BUY_WINGS_5_HEADLINE",
            9
          },
          {
            "STORE_OWNED_HEADLINE",
            10
          },
          {
            "STORE_PREORDER_HEADLINE",
            11
          },
          {
            "STORE_BUY_WINGS_1_DESC",
            12
          },
          {
            "STORE_BUY_WINGS_2_DESC",
            13
          },
          {
            "STORE_BUY_WINGS_3_DESC",
            14
          },
          {
            "STORE_BUY_WINGS_4_DESC",
            15
          },
          {
            "STORE_BUY_WINGS_5_DESC",
            16
          },
          {
            "STORE_OWNED_DESC",
            17
          },
          {
            "STORE_PREORDER_WINGS_1_DESC",
            18
          },
          {
            "STORE_PREORDER_WINGS_2_DESC",
            19
          },
          {
            "STORE_PREORDER_WINGS_3_DESC",
            20
          },
          {
            "STORE_PREORDER_WINGS_4_DESC",
            21
          },
          {
            "STORE_PREORDER_WINGS_5_DESC",
            22
          },
          {
            "STORE_PREORDER_RADIO_TEXT",
            23
          },
          {
            "STORE_PREVIEW_REWARDS_TEXT",
            24
          },
          {
            "ADVENTURE_DEF_PREFAB",
            25
          },
          {
            "STORE_PREFAB",
            26
          },
          {
            "LEAVING_SOON",
            27
          },
          {
            "LEAVING_SOON_TEXT",
            28
          },
          {
            "GAME_MODE_ICON",
            29
          },
          {
            "PRODUCT_STRING_KEY",
            30
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AdventureDbfRecord.\u003C\u003Ef__switch\u0024mapD.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (DbfLocValue);
          case 3:
            return typeof (int);
          case 4:
            return typeof (DbfLocValue);
          case 5:
            return typeof (DbfLocValue);
          case 6:
            return typeof (DbfLocValue);
          case 7:
            return typeof (DbfLocValue);
          case 8:
            return typeof (DbfLocValue);
          case 9:
            return typeof (DbfLocValue);
          case 10:
            return typeof (DbfLocValue);
          case 11:
            return typeof (DbfLocValue);
          case 12:
            return typeof (DbfLocValue);
          case 13:
            return typeof (DbfLocValue);
          case 14:
            return typeof (DbfLocValue);
          case 15:
            return typeof (DbfLocValue);
          case 16:
            return typeof (DbfLocValue);
          case 17:
            return typeof (DbfLocValue);
          case 18:
            return typeof (DbfLocValue);
          case 19:
            return typeof (DbfLocValue);
          case 20:
            return typeof (DbfLocValue);
          case 21:
            return typeof (DbfLocValue);
          case 22:
            return typeof (DbfLocValue);
          case 23:
            return typeof (DbfLocValue);
          case 24:
            return typeof (DbfLocValue);
          case 25:
            return typeof (string);
          case 26:
            return typeof (string);
          case 27:
            return typeof (bool);
          case 28:
            return typeof (DbfLocValue);
          case 29:
            return typeof (string);
          case 30:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
