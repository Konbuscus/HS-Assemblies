﻿// Decompiled with JetBrains decompiler
// Type: Banner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Banner : MonoBehaviour
{
  public UberText m_headline;
  public UberText m_caption;
  public GameObject m_glowObject;

  public void SetText(string headline)
  {
    this.m_headline.Text = headline;
    this.m_caption.gameObject.SetActive(false);
  }

  public void SetText(string headline, string caption)
  {
    this.m_headline.Text = headline;
    this.m_caption.Text = caption;
  }

  public void MoveGlowForBottomPlacement()
  {
    this.m_glowObject.transform.localPosition = new Vector3(this.m_glowObject.transform.localPosition.x, this.m_glowObject.transform.localPosition.y, 0.0f);
  }
}
