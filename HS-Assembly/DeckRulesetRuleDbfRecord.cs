﻿// Decompiled with JetBrains decompiler
// Type: DeckRulesetRuleDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DeckRulesetRuleDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_DeckRulesetId;
  [SerializeField]
  private int m_AppliesToSubsetId;
  [SerializeField]
  private bool m_AppliesToIsNot;
  [SerializeField]
  private string m_RuleType;
  [SerializeField]
  private bool m_RuleIsNot;
  [SerializeField]
  private int m_MinValue;
  [SerializeField]
  private int m_MaxValue;
  [SerializeField]
  private int m_Tag;
  [SerializeField]
  private int m_TagMinValue;
  [SerializeField]
  private int m_TagMaxValue;
  [SerializeField]
  private string m_StringValue;
  [SerializeField]
  private DbfLocValue m_ErrorString;

  [DbfField("DECK_RULESET_ID", "which DECK_RULESET.ID does this rule belong to")]
  public int DeckRulesetId
  {
    get
    {
      return this.m_DeckRulesetId;
    }
  }

  [DbfField("APPLIES_TO_SUBSET_ID", "constrain this rule to apply only to cards that are in this specified SUBSET.ID. when this column value is 0, it means this rule applies to all cards within the deck.")]
  public int AppliesToSubsetId
  {
    get
    {
      return this.m_AppliesToSubsetId;
    }
  }

  [DbfField("APPLIES_TO_IS_NOT", "when 1 (true), means this the rule applies to all cards NOT in the specified APPLIES_TO_SUBSET_ID; otherwise, it applies to cards that ARE in the specified subset.")]
  public bool AppliesToIsNot
  {
    get
    {
      return this.m_AppliesToIsNot;
    }
  }

  [DbfField("RULE_TYPE", "which rule is being applied to this DECK_RULESET - the enum values and string mappings are in server code.")]
  public string RuleType
  {
    get
    {
      return this.m_RuleType;
    }
  }

  [DbfField("RULE_IS_NOT", "whether or not this rule should be validated as successful based on the negation of the parameters")]
  public bool RuleIsNot
  {
    get
    {
      return this.m_RuleIsNot;
    }
  }

  [DbfField("MIN_VALUE", "value range parameter for the rule.")]
  public int MinValue
  {
    get
    {
      return this.m_MinValue;
    }
  }

  [DbfField("MAX_VALUE", "value range parameter for the rule.")]
  public int MaxValue
  {
    get
    {
      return this.m_MaxValue;
    }
  }

  [DbfField("TAG", "for rules that specify tag values, this column specifies which tag.")]
  public int Tag
  {
    get
    {
      return this.m_Tag;
    }
  }

  [DbfField("TAG_MIN_VALUE", "tag value range parameter for the rule.")]
  public int TagMinValue
  {
    get
    {
      return this.m_TagMinValue;
    }
  }

  [DbfField("TAG_MAX_VALUE", "tag value range parameter for the rule.")]
  public int TagMaxValue
  {
    get
    {
      return this.m_TagMaxValue;
    }
  }

  [DbfField("STRING_VALUE", "string parameter for the rule.")]
  public string StringValue
  {
    get
    {
      return this.m_StringValue;
    }
  }

  [DbfField("ERROR_STRING", "when deck validation fails on this rule, this error message is displayed to the player.")]
  public DbfLocValue ErrorString
  {
    get
    {
      return this.m_ErrorString;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    DeckRulesetRuleDbfAsset rulesetRuleDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (DeckRulesetRuleDbfAsset)) as DeckRulesetRuleDbfAsset;
    if ((UnityEngine.Object) rulesetRuleDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("DeckRulesetRuleDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < rulesetRuleDbfAsset.Records.Count; ++index)
      rulesetRuleDbfAsset.Records[index].StripUnusedLocales();
    records = (object) rulesetRuleDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_ErrorString.StripUnusedLocales();
  }

  public void SetDeckRulesetId(int v)
  {
    this.m_DeckRulesetId = v;
  }

  public void SetAppliesToSubsetId(int v)
  {
    this.m_AppliesToSubsetId = v;
  }

  public void SetAppliesToIsNot(bool v)
  {
    this.m_AppliesToIsNot = v;
  }

  public void SetRuleType(string v)
  {
    this.m_RuleType = v;
  }

  public void SetRuleIsNot(bool v)
  {
    this.m_RuleIsNot = v;
  }

  public void SetMinValue(int v)
  {
    this.m_MinValue = v;
  }

  public void SetMaxValue(int v)
  {
    this.m_MaxValue = v;
  }

  public void SetTag(int v)
  {
    this.m_Tag = v;
  }

  public void SetTagMinValue(int v)
  {
    this.m_TagMinValue = v;
  }

  public void SetTagMaxValue(int v)
  {
    this.m_TagMaxValue = v;
  }

  public void SetStringValue(string v)
  {
    this.m_StringValue = v;
  }

  public void SetErrorString(DbfLocValue v)
  {
    this.m_ErrorString = v;
    v.SetDebugInfo(this.ID, "ERROR_STRING");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckRulesetRuleDbfRecord.\u003C\u003Ef__switch\u0024map32 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckRulesetRuleDbfRecord.\u003C\u003Ef__switch\u0024map32 = new Dictionary<string, int>(13)
        {
          {
            "ID",
            0
          },
          {
            "DECK_RULESET_ID",
            1
          },
          {
            "APPLIES_TO_SUBSET_ID",
            2
          },
          {
            "APPLIES_TO_IS_NOT",
            3
          },
          {
            "RULE_TYPE",
            4
          },
          {
            "RULE_IS_NOT",
            5
          },
          {
            "MIN_VALUE",
            6
          },
          {
            "MAX_VALUE",
            7
          },
          {
            "TAG",
            8
          },
          {
            "TAG_MIN_VALUE",
            9
          },
          {
            "TAG_MAX_VALUE",
            10
          },
          {
            "STRING_VALUE",
            11
          },
          {
            "ERROR_STRING",
            12
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckRulesetRuleDbfRecord.\u003C\u003Ef__switch\u0024map32.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.DeckRulesetId;
          case 2:
            return (object) this.AppliesToSubsetId;
          case 3:
            return (object) this.AppliesToIsNot;
          case 4:
            return (object) this.RuleType;
          case 5:
            return (object) this.RuleIsNot;
          case 6:
            return (object) this.MinValue;
          case 7:
            return (object) this.MaxValue;
          case 8:
            return (object) this.Tag;
          case 9:
            return (object) this.TagMinValue;
          case 10:
            return (object) this.TagMaxValue;
          case 11:
            return (object) this.StringValue;
          case 12:
            return (object) this.ErrorString;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (DeckRulesetRuleDbfRecord.\u003C\u003Ef__switch\u0024map33 == null)
    {
      // ISSUE: reference to a compiler-generated field
      DeckRulesetRuleDbfRecord.\u003C\u003Ef__switch\u0024map33 = new Dictionary<string, int>(13)
      {
        {
          "ID",
          0
        },
        {
          "DECK_RULESET_ID",
          1
        },
        {
          "APPLIES_TO_SUBSET_ID",
          2
        },
        {
          "APPLIES_TO_IS_NOT",
          3
        },
        {
          "RULE_TYPE",
          4
        },
        {
          "RULE_IS_NOT",
          5
        },
        {
          "MIN_VALUE",
          6
        },
        {
          "MAX_VALUE",
          7
        },
        {
          "TAG",
          8
        },
        {
          "TAG_MIN_VALUE",
          9
        },
        {
          "TAG_MAX_VALUE",
          10
        },
        {
          "STRING_VALUE",
          11
        },
        {
          "ERROR_STRING",
          12
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!DeckRulesetRuleDbfRecord.\u003C\u003Ef__switch\u0024map33.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetDeckRulesetId((int) val);
        break;
      case 2:
        this.SetAppliesToSubsetId((int) val);
        break;
      case 3:
        this.SetAppliesToIsNot((bool) val);
        break;
      case 4:
        this.SetRuleType((string) val);
        break;
      case 5:
        this.SetRuleIsNot((bool) val);
        break;
      case 6:
        this.SetMinValue((int) val);
        break;
      case 7:
        this.SetMaxValue((int) val);
        break;
      case 8:
        this.SetTag((int) val);
        break;
      case 9:
        this.SetTagMinValue((int) val);
        break;
      case 10:
        this.SetTagMaxValue((int) val);
        break;
      case 11:
        this.SetStringValue((string) val);
        break;
      case 12:
        this.SetErrorString((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckRulesetRuleDbfRecord.\u003C\u003Ef__switch\u0024map34 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckRulesetRuleDbfRecord.\u003C\u003Ef__switch\u0024map34 = new Dictionary<string, int>(13)
        {
          {
            "ID",
            0
          },
          {
            "DECK_RULESET_ID",
            1
          },
          {
            "APPLIES_TO_SUBSET_ID",
            2
          },
          {
            "APPLIES_TO_IS_NOT",
            3
          },
          {
            "RULE_TYPE",
            4
          },
          {
            "RULE_IS_NOT",
            5
          },
          {
            "MIN_VALUE",
            6
          },
          {
            "MAX_VALUE",
            7
          },
          {
            "TAG",
            8
          },
          {
            "TAG_MIN_VALUE",
            9
          },
          {
            "TAG_MAX_VALUE",
            10
          },
          {
            "STRING_VALUE",
            11
          },
          {
            "ERROR_STRING",
            12
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckRulesetRuleDbfRecord.\u003C\u003Ef__switch\u0024map34.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (int);
          case 3:
            return typeof (bool);
          case 4:
            return typeof (string);
          case 5:
            return typeof (bool);
          case 6:
            return typeof (int);
          case 7:
            return typeof (int);
          case 8:
            return typeof (int);
          case 9:
            return typeof (int);
          case 10:
            return typeof (int);
          case 11:
            return typeof (string);
          case 12:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
