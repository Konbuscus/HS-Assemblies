﻿// Decompiled with JetBrains decompiler
// Type: FixedActionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.ComponentModel;

public enum FixedActionType
{
  [Description("tutorial_progress")] TUTORIAL_PROGRESS,
  [Description("wing_progress")] WING_PROGRESS,
  [Description("wing_flags")] WING_FLAGS,
  [Description("hero_level")] HERO_LEVEL,
  [Description("meta_action")] META_ACTION,
  [Description("achieve")] ACHIEVE,
  [Description("account_license_flags")] ACCOUNT_LICENSE_FLAGS,
}
