﻿// Decompiled with JetBrains decompiler
// Type: EndGameTwoScoop
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class EndGameTwoScoop : MonoBehaviour
{
  private static readonly float AFTER_PUNCH_SCALE_VAL = 2.3f;
  protected static readonly float START_SCALE_VAL = 0.01f;
  protected static readonly float END_SCALE_VAL = 2.5f;
  protected static readonly Vector3 START_POSITION = new Vector3(-7.8f, 8.2f, -5f);
  protected static readonly float BAR_ANIMATION_DELAY = 1f;
  public UberText m_bannerLabel;
  public GameObject m_heroBone;
  public Actor m_heroActor;
  public HeroXPBar m_xpBarPrefab;
  public GameObject m_levelUpTier1;
  public GameObject m_levelUpTier2;
  public GameObject m_levelUpTier3;
  protected bool m_heroActorLoaded;
  protected HeroXPBar m_xpBar;
  private bool m_isShown;

  private void Awake()
  {
    this.gameObject.SetActive(false);
    AssetLoader.Get().LoadActor("Card_Play_Hero", new AssetLoader.GameObjectCallback(this.OnHeroActorLoaded), (object) null, false);
  }

  private void Start()
  {
    SceneUtils.SetLayer(this.gameObject, GameLayer.IgnoreFullScreenEffects);
    this.ResetPositions();
  }

  public bool IsShown()
  {
    return this.m_isShown;
  }

  public void Show()
  {
    this.m_isShown = true;
    this.gameObject.SetActive(true);
    this.ShowImpl();
    if (GameMgr.Get().IsTutorial() || GameMgr.Get().IsSpectator())
      return;
    NetCache.HeroLevel heroLevel = GameUtils.GetHeroLevel(GameState.Get().GetFriendlySidePlayer().GetStartingHero().GetClass());
    if (heroLevel == null)
    {
      this.HideXpBar();
    }
    else
    {
      this.m_xpBar = Object.Instantiate<HeroXPBar>(this.m_xpBarPrefab);
      this.m_xpBar.transform.parent = this.m_heroActor.transform;
      this.m_xpBar.transform.localScale = new Vector3(0.88f, 0.88f, 0.88f);
      this.m_xpBar.transform.localPosition = new Vector3(-0.1886583f, 0.2122119f, -0.7446293f);
      this.m_xpBar.m_soloLevelLimit = NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>().XPSoloLimit;
      this.m_xpBar.m_isAnimated = true;
      this.m_xpBar.m_delay = EndGameTwoScoop.BAR_ANIMATION_DELAY;
      this.m_xpBar.m_heroLevel = heroLevel;
      this.m_xpBar.m_levelUpCallback = new HeroXPBar.PlayLevelUpEffectCallback(this.PlayLevelUpEffect);
      this.m_xpBar.UpdateDisplay();
    }
  }

  public void Hide()
  {
    this.HideAll();
  }

  public bool IsLoaded()
  {
    return this.m_heroActorLoaded;
  }

  public void HideXpBar()
  {
    if (!((Object) this.m_xpBar != (Object) null))
      return;
    this.m_xpBar.gameObject.SetActive(false);
  }

  public virtual void StopAnimating()
  {
  }

  protected virtual void ShowImpl()
  {
  }

  protected virtual void ResetPositions()
  {
  }

  protected void SetBannerLabel(string label)
  {
    this.m_bannerLabel.Text = label;
  }

  protected void EnableBannerLabel(bool enable)
  {
    this.m_bannerLabel.gameObject.SetActive(enable);
  }

  protected void PunchEndGameTwoScoop()
  {
    EndGameScreen.Get().SetPlayingBlockingAnim(false);
    iTween.ScaleTo(this.gameObject, new Vector3(EndGameTwoScoop.AFTER_PUNCH_SCALE_VAL, EndGameTwoScoop.AFTER_PUNCH_SCALE_VAL, EndGameTwoScoop.AFTER_PUNCH_SCALE_VAL), 0.15f);
  }

  private void HideAll()
  {
    Hashtable args = iTween.Hash((object) "scale", (object) new Vector3(EndGameTwoScoop.START_SCALE_VAL, EndGameTwoScoop.START_SCALE_VAL, EndGameTwoScoop.START_SCALE_VAL), (object) "time", (object) 0.25f, (object) "oncomplete", (object) "OnAllHidden", (object) "oncompletetarget", (object) this.gameObject);
    iTween.FadeTo(this.gameObject, 0.0f, 0.25f);
    iTween.ScaleTo(this.gameObject, args);
    this.m_isShown = false;
  }

  private void OnAllHidden()
  {
    iTween.FadeTo(this.gameObject, 0.0f, 0.0f);
    this.gameObject.SetActive(false);
    this.ResetPositions();
  }

  private void OnHeroActorLoaded(string name, GameObject go, object callbackData)
  {
    go.transform.parent = this.transform;
    go.transform.localPosition = this.m_heroBone.transform.localPosition;
    go.transform.localScale = this.m_heroBone.transform.localScale;
    this.m_heroActor = go.GetComponent<Actor>();
    this.m_heroActor.TurnOffCollider();
    this.m_heroActor.m_healthObject.SetActive(false);
    this.m_heroActorLoaded = true;
    this.m_heroActor.SetPremium(GameState.Get().GetFriendlySidePlayer().GetHeroCard().GetPremium());
    this.m_heroActor.UpdateAllComponents();
  }

  protected void PlayLevelUpEffect()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this.m_levelUpTier1);
    if (!(bool) ((Object) gameObject))
      return;
    gameObject.transform.parent = this.transform;
    gameObject.GetComponent<PlayMakerFSM>().SendEvent("Birth");
  }
}
