﻿// Decompiled with JetBrains decompiler
// Type: PowerProcessor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusGame;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PowerProcessor
{
  private int m_nextTaskListId = 1;
  private Stack<PowerTaskList> m_previousStack = new Stack<PowerTaskList>();
  private PowerQueue m_powerQueue = new PowerQueue();
  private const string ATTACK_SPELL_CONTROLLER_PREFAB_NAME = "AttackSpellController";
  private const string SECRET_SPELL_CONTROLLER_PREFAB_NAME = "SecretSpellController";
  private const string JOUST_SPELL_CONTROLLER_PREFAB_NAME = "JoustSpellController";
  private const string RITUAL_SPELL_CONTROLLER_PREFAB_NAME = "RitualSpellController";
  private bool m_buildingTaskList;
  private PowerTaskList m_currentTaskList;
  private bool m_historyBlocking;
  private PowerTaskList m_historyBlockingTaskList;
  private PowerTaskList m_busyTaskList;
  private PowerTaskList m_earlyConcedeTaskList;
  private bool m_handledFirstEarlyConcede;
  private PowerTaskList m_gameOverTaskList;

  public void Clear()
  {
    this.m_powerQueue.Clear();
    this.m_currentTaskList = (PowerTaskList) null;
  }

  public bool IsBuildingTaskList()
  {
    return this.m_buildingTaskList;
  }

  public PowerTaskList GetCurrentTaskList()
  {
    return this.m_currentTaskList;
  }

  public PowerQueue GetPowerQueue()
  {
    return this.m_powerQueue;
  }

  public void OnMetaData(Network.HistMetaData metaData)
  {
    if (metaData.MetaType != HistoryMeta.Type.SHOW_BIG_CARD)
      return;
    Entity entity = GameState.Get().GetEntity(metaData.Info[0]);
    if (entity == null)
      return;
    this.m_historyBlocking = true;
    if (this.m_historyBlockingTaskList == null)
      this.m_historyBlockingTaskList = this.m_currentTaskList;
    HistoryManager.Get().CreatePlayedBigCard(entity, new HistoryManager.BigCardFinishedCallback(this.OnBigCardFinished), true, false);
  }

  public bool IsHistoryBlocking()
  {
    return this.m_historyBlocking;
  }

  public PowerTaskList GetHistoryBlockingTaskList()
  {
    return this.m_historyBlockingTaskList;
  }

  public void ForceStopHistoryBlocking()
  {
    this.m_historyBlocking = false;
    this.m_historyBlockingTaskList = (PowerTaskList) null;
  }

  public PowerTaskList GetLatestUnendedTaskList()
  {
    int count = this.m_powerQueue.Count;
    if (count == 0)
      return this.m_currentTaskList;
    return this.m_powerQueue[count - 1];
  }

  public PowerTaskList GetLastTaskList()
  {
    int count = this.m_powerQueue.Count;
    if (count > 0)
      return this.m_powerQueue[count - 1];
    return this.m_currentTaskList;
  }

  public PowerTaskList GetEarlyConcedeTaskList()
  {
    return this.m_earlyConcedeTaskList;
  }

  public bool HasEarlyConcedeTaskList()
  {
    return this.m_earlyConcedeTaskList != null;
  }

  public PowerTaskList GetGameOverTaskList()
  {
    return this.m_gameOverTaskList;
  }

  public bool HasGameOverTaskList()
  {
    return this.m_gameOverTaskList != null;
  }

  public bool CanDoTask(PowerTask task)
  {
    Network.PowerHistory power = task.GetPower();
    return power.Type != Network.PowerType.META_DATA || ((Network.HistMetaData) power).MetaType != HistoryMeta.Type.SHOW_BIG_CARD || !HistoryManager.Get().IsShowingBigCard();
  }

  public void ForEachTaskList(Action<int, PowerTaskList> predicate)
  {
    if (this.m_currentTaskList != null)
      predicate(-1, this.m_currentTaskList);
    for (int index = 0; index < this.m_powerQueue.Count; ++index)
      predicate(index, this.m_powerQueue[index]);
  }

  public bool HasTaskLists()
  {
    return this.m_currentTaskList != null || this.m_powerQueue.Count > 0;
  }

  public bool HasTaskList(PowerTaskList taskList)
  {
    return taskList != null && (this.m_currentTaskList == taskList || this.m_powerQueue.Contains(taskList));
  }

  public void OnPowerHistory(List<Network.PowerHistory> powerList)
  {
    this.m_buildingTaskList = true;
    for (int index = 0; index < powerList.Count; ++index)
    {
      PowerTaskList powerTaskList = new PowerTaskList();
      if (this.m_previousStack.Count > 0)
      {
        powerTaskList.SetPrevious(this.m_previousStack.Pop());
        this.m_previousStack.Push(powerTaskList);
      }
      this.BuildTaskList(powerList, ref index, powerTaskList);
    }
    this.m_buildingTaskList = false;
  }

  public void ProcessPowerQueue()
  {
    while (GameState.Get().CanProcessPowerQueue())
    {
      if (this.m_busyTaskList != null)
      {
        this.m_busyTaskList = (PowerTaskList) null;
      }
      else
      {
        PowerTaskList taskList = this.m_powerQueue.Peek();
        if (this.m_historyBlockingTaskList != null)
        {
          if (!taskList.IsDescendantOfBlock(this.m_historyBlockingTaskList))
            break;
        }
        else if ((UnityEngine.Object) HistoryManager.Get() != (UnityEngine.Object) null && HistoryManager.Get().IsShowingBigCard())
          break;
        this.OnWillProcessTaskList(taskList);
        if (GameState.Get().IsBusy())
        {
          this.m_busyTaskList = taskList;
          break;
        }
      }
      if (this.CanEarlyConcede())
      {
        if (this.m_earlyConcedeTaskList == null && !this.m_handledFirstEarlyConcede)
        {
          this.DoEarlyConcedeVisuals();
          this.m_handledFirstEarlyConcede = true;
        }
        while (this.m_powerQueue.Count > 0)
        {
          this.m_currentTaskList = this.m_powerQueue.Dequeue();
          this.m_currentTaskList.DebugDump();
          this.CancelSpellsForEarlyConcede(this.m_currentTaskList);
          this.m_currentTaskList.DoEarlyConcedeTasks();
          this.m_currentTaskList = (PowerTaskList) null;
        }
        break;
      }
      this.m_currentTaskList = this.m_powerQueue.Dequeue();
      this.m_currentTaskList.DebugDump();
      this.OnProcessTaskList();
      this.StartCurrentTaskList();
    }
  }

  private int GetNextTaskListId()
  {
    int nextTaskListId = this.m_nextTaskListId;
    this.m_nextTaskListId = this.m_nextTaskListId != int.MaxValue ? this.m_nextTaskListId + 1 : 1;
    return nextTaskListId;
  }

  private void BuildTaskList(List<Network.PowerHistory> powerList, ref int index, PowerTaskList taskList)
  {
    while (index < powerList.Count)
    {
      Network.PowerHistory power = powerList[index];
      switch (power.Type)
      {
        case Network.PowerType.BLOCK_START:
          if (!taskList.IsEmpty())
            this.EnqueueTaskList(taskList);
          PowerTaskList powerTaskList = new PowerTaskList();
          powerTaskList.SetBlockStart((Network.HistBlockStart) power);
          PowerTaskList origin = taskList.GetOrigin();
          if (origin.IsStartOfBlock())
            powerTaskList.SetParent(origin);
          this.m_previousStack.Push(powerTaskList);
          index = index + 1;
          this.BuildTaskList(powerList, ref index, powerTaskList);
          return;
        case Network.PowerType.BLOCK_END:
          taskList.SetBlockEnd((Network.HistBlockEnd) power);
          if (this.m_previousStack.Count > 0)
          {
            this.m_previousStack.Pop();
            this.EnqueueTaskList(taskList);
            return;
          }
          goto label_12;
        default:
          taskList.CreateTask(power).DoRealTimeTask(powerList, index);
          index = index + 1;
          continue;
      }
    }
label_12:
    if (taskList.IsEmpty())
      return;
    this.EnqueueTaskList(taskList);
  }

  private void EnqueueTaskList(PowerTaskList taskList)
  {
    taskList.SetId(this.GetNextTaskListId());
    this.m_powerQueue.Enqueue(taskList);
    if (taskList.HasFriendlyConcede())
      this.m_earlyConcedeTaskList = taskList;
    if (!taskList.HasGameOver())
      return;
    this.m_gameOverTaskList = taskList;
  }

  private void OnWillProcessTaskList(PowerTaskList taskList)
  {
    if ((bool) ((UnityEngine.Object) ThinkEmoteManager.Get()))
      ThinkEmoteManager.Get().NotifyOfActivity();
    if (!taskList.IsStartOfBlock())
      return;
    Network.HistBlockStart blockStart = taskList.GetBlockStart();
    if (blockStart.BlockType != HistoryBlock.Type.PLAY)
      return;
    Entity entity = GameState.Get().GetEntity(blockStart.Entity);
    if (!entity.GetController().IsOpposingSide())
      return;
    string cardId = entity.GetCardId();
    if (string.IsNullOrEmpty(cardId))
      cardId = this.FindRevealedCardId(taskList);
    GameState.Get().GetGameEntity().NotifyOfOpponentWillPlayCard(cardId);
  }

  private string FindRevealedCardId(PowerTaskList taskList)
  {
    Network.HistBlockStart blockStart = taskList.GetBlockStart();
    List<PowerTask> taskList1 = taskList.GetTaskList();
    for (int index = 0; index < taskList1.Count; ++index)
    {
      Network.HistShowEntity power = taskList1[index].GetPower() as Network.HistShowEntity;
      if (power != null && power.Entity.ID == blockStart.Entity)
        return power.Entity.CardID;
    }
    return (string) null;
  }

  private void OnProcessTaskList()
  {
    if (this.m_currentTaskList.IsStartOfBlock())
    {
      Network.HistBlockStart blockStart = this.m_currentTaskList.GetBlockStart();
      if (blockStart.BlockType == HistoryBlock.Type.PLAY)
      {
        Entity entity = GameState.Get().GetEntity(blockStart.Entity);
        if (entity.IsControlledByFriendlySidePlayer())
          GameState.Get().GetGameEntity().NotifyOfFriendlyPlayedCard(entity);
        else
          GameState.Get().GetGameEntity().NotifyOfOpponentPlayedCard(entity);
      }
    }
    this.PrepareHistoryForCurrentTaskList();
  }

  private void PrepareHistoryForCurrentTaskList()
  {
    Log.Power.Print("PowerProcessor.PrepareHistoryForCurrentTaskList() - m_currentTaskList={0}", (object) this.m_currentTaskList.GetId());
    Network.HistBlockStart blockStart = this.m_currentTaskList.GetBlockStart();
    if (blockStart == null)
      return;
    switch (blockStart.BlockType)
    {
      case HistoryBlock.Type.ATTACK:
        AttackType attackType = this.m_currentTaskList.GetAttackType();
        Entity attacker = (Entity) null;
        Entity defender = (Entity) null;
        switch (attackType)
        {
          case AttackType.REGULAR:
            attacker = this.m_currentTaskList.GetAttacker();
            defender = this.m_currentTaskList.GetDefender();
            break;
          case AttackType.CANCELED:
            attacker = this.m_currentTaskList.GetAttacker();
            defender = this.m_currentTaskList.GetProposedDefender();
            break;
        }
        if (attacker == null || defender == null)
          break;
        HistoryManager.Get().CreateAttackTile(attacker, defender, this.m_currentTaskList);
        this.m_currentTaskList.SetWillCompleteHistoryEntry(true);
        this.m_currentTaskList.NotifyHistoryOfAdditionalTargets();
        break;
      case HistoryBlock.Type.PLAY:
        Entity entity1 = GameState.Get().GetEntity(blockStart.Entity);
        if (entity1 == null)
          break;
        if (this.m_currentTaskList.IsStartOfBlock())
        {
          if (this.m_currentTaskList.ShouldCreatePlayBlockHistoryTile())
          {
            Entity entity2 = GameState.Get().GetEntity(blockStart.Target);
            HistoryManager.Get().CreatePlayedTile(entity1, entity2);
            this.m_currentTaskList.SetWillCompleteHistoryEntry(true);
          }
          if (this.ShouldShowPlayedBigCard(entity1))
          {
            bool countered = this.m_currentTaskList.WasThePlayedSpellCountered(entity1);
            this.m_historyBlocking = true;
            if (this.m_historyBlockingTaskList == null)
              this.m_historyBlockingTaskList = this.m_currentTaskList;
            HistoryManager.Get().CreatePlayedBigCard(entity1, new HistoryManager.BigCardFinishedCallback(this.OnBigCardFinished), false, countered);
          }
        }
        this.m_currentTaskList.NotifyHistoryOfAdditionalTargets();
        break;
      case HistoryBlock.Type.POWER:
        this.m_currentTaskList.NotifyHistoryOfAdditionalTargets();
        break;
      case HistoryBlock.Type.JOUST:
        this.m_currentTaskList.NotifyHistoryOfAdditionalTargets();
        break;
      case HistoryBlock.Type.TRIGGER:
        Entity entity3 = GameState.Get().GetEntity(blockStart.Entity);
        if (entity3 == null)
          break;
        if (entity3.IsSecret())
        {
          if (this.m_currentTaskList.IsStartOfBlock())
          {
            HistoryManager.Get().CreateTriggerTile(entity3);
            this.m_currentTaskList.SetWillCompleteHistoryEntry(true);
            this.m_historyBlocking = true;
            if (this.m_historyBlockingTaskList == null)
              this.m_historyBlockingTaskList = this.m_currentTaskList;
            HistoryManager.Get().CreateTriggeredBigCard(entity3, new HistoryManager.BigCardFinishedCallback(this.OnBigCardFinished), false, true);
          }
          this.m_currentTaskList.NotifyHistoryOfAdditionalTargets();
          break;
        }
        bool flag = false;
        if (!this.m_currentTaskList.IsStartOfBlock())
        {
          flag = this.GetTriggerTaskListThatShouldCompleteHistoryEntry().WillBlockCompleteHistoryEntry();
        }
        else
        {
          PowerHistoryInfo powerHistoryInfo = (PowerHistoryInfo) null;
          string effectCardId = this.m_currentTaskList.GetEffectCardId();
          if (!string.IsNullOrEmpty(effectCardId))
            powerHistoryInfo = DefLoader.Get().GetEntityDef(effectCardId).GetPowerHistoryInfo(blockStart.EffectIndex);
          if (powerHistoryInfo != null && powerHistoryInfo.ShouldShowInHistory())
          {
            if (entity3.HasTag(GAME_TAG.HISTORY_PROXY))
            {
              Entity entity2 = GameState.Get().GetEntity(entity3.GetTag(GAME_TAG.HISTORY_PROXY));
              HistoryManager.Get().CreatePlayedTile(entity2, (Entity) null);
              if (entity3.GetController() != GameState.Get().GetFriendlySidePlayer() || !entity3.HasTag(GAME_TAG.HISTORY_PROXY_NO_BIG_CARD))
              {
                this.m_historyBlocking = true;
                if (this.m_historyBlockingTaskList == null)
                  this.m_historyBlockingTaskList = this.m_currentTaskList;
                HistoryManager.Get().CreateTriggeredBigCard(entity2, new HistoryManager.BigCardFinishedCallback(this.OnBigCardFinished), false, false);
              }
            }
            else
            {
              if (this.ShouldShowTriggeredBigCard(entity3))
              {
                this.m_historyBlocking = true;
                if (this.m_historyBlockingTaskList == null)
                  this.m_historyBlockingTaskList = this.m_currentTaskList;
                HistoryManager.Get().CreateTriggeredBigCard(entity3, new HistoryManager.BigCardFinishedCallback(this.OnBigCardFinished), false, false);
              }
              HistoryManager.Get().CreateTriggerTile(entity3);
            }
            this.GetTriggerTaskListThatShouldCompleteHistoryEntry().SetWillCompleteHistoryEntry(true);
            flag = true;
          }
        }
        if (!flag)
          break;
        this.m_currentTaskList.NotifyHistoryOfAdditionalTargets();
        break;
      case HistoryBlock.Type.FATIGUE:
        if (this.m_currentTaskList.IsStartOfBlock())
        {
          HistoryManager.Get().CreateFatigueTile();
          this.m_currentTaskList.SetWillCompleteHistoryEntry(true);
        }
        this.m_currentTaskList.NotifyHistoryOfAdditionalTargets();
        break;
    }
  }

  private void OnBigCardFinished()
  {
    this.m_historyBlocking = false;
  }

  private bool ShouldShowPlayedBigCard(Entity sourceEntity)
  {
    return GameMgr.Get().IsSpectator() || sourceEntity.IsControlledByOpposingSidePlayer();
  }

  private bool ShouldShowTriggeredBigCard(Entity sourceEntity)
  {
    return sourceEntity.GetZone() == TAG_ZONE.HAND && sourceEntity.HasTriggerVisual();
  }

  private PowerTaskList GetTriggerTaskListThatShouldCompleteHistoryEntry()
  {
    if (this.m_currentTaskList.GetBlockType() != HistoryBlock.Type.TRIGGER)
      return (PowerTaskList) null;
    PowerTaskList parent = this.m_currentTaskList.GetParent();
    if (parent != null && parent.GetBlockType() == HistoryBlock.Type.RITUAL)
      return parent;
    return this.m_currentTaskList.GetOrigin();
  }

  private bool CanEarlyConcede()
  {
    if (this.m_earlyConcedeTaskList != null)
      return true;
    if (GameState.Get().IsGameOver() || !GameState.Get().WasConcedeRequested())
      return false;
    Network.HistTagChange gameOverTagChange = GameState.Get().GetRealTimeGameOverTagChange();
    return gameOverTagChange != null && gameOverTagChange.Value != 4;
  }

  private void DoEarlyConcedeVisuals()
  {
    Player friendlySidePlayer = GameState.Get().GetFriendlySidePlayer();
    if (friendlySidePlayer == null)
      return;
    friendlySidePlayer.PlayConcedeEmote();
  }

  private void CancelSpellsForEarlyConcede(PowerTaskList taskList)
  {
    Entity sourceEntity = taskList.GetSourceEntity();
    if (sourceEntity == null)
      return;
    Card card = sourceEntity.GetCard();
    if (!(bool) ((UnityEngine.Object) card) || taskList.GetBlockStart().BlockType != HistoryBlock.Type.POWER)
      return;
    Spell playSpell = card.GetPlaySpell(true);
    if (!(bool) ((UnityEngine.Object) playSpell))
      return;
    switch (playSpell.GetActiveState())
    {
      case SpellStateType.NONE:
        break;
      case SpellStateType.CANCEL:
        break;
      default:
        playSpell.ActivateState(SpellStateType.CANCEL);
        break;
    }
  }

  private void StartCurrentTaskList()
  {
    GameState state = GameState.Get();
    Network.HistBlockStart blockStart = this.m_currentTaskList.GetBlockStart();
    if (blockStart == null)
    {
      this.DoCurrentTaskList();
    }
    else
    {
      int entity1 = blockStart.Entity;
      Entity entity2 = state.GetEntity(entity1);
      if (entity2 == null)
      {
        Debug.LogErrorFormat("PowerProcessor.StartCurrentTaskList() - WARNING got a power with a null source entity (ID={0})", (object) entity1);
        this.DoCurrentTaskList();
      }
      else
      {
        if (this.DoTaskListWithSpellController(state, this.m_currentTaskList, entity2))
          return;
        this.DoCurrentTaskList();
      }
    }
  }

  private void DoCurrentTaskList()
  {
    this.m_currentTaskList.DoAllTasks((PowerTaskList.CompleteCallback) ((taskList, startIndex, count, userData) => this.EndCurrentTaskList()));
  }

  private void EndCurrentTaskList()
  {
    Log.Power.Print("PowerProcessor.EndCurrentTaskList() - m_currentTaskList={0}", (object) (this.m_currentTaskList != null ? this.m_currentTaskList.GetId().ToString() : "null"));
    if (this.m_currentTaskList == null)
    {
      GameState.Get().OnTaskListEnded((PowerTaskList) null);
    }
    else
    {
      if (this.m_currentTaskList.GetBlockEnd() != null)
      {
        if (this.m_currentTaskList.IsInBlock(this.m_historyBlockingTaskList))
          this.m_historyBlockingTaskList = (PowerTaskList) null;
        if (this.m_currentTaskList.IsRitualBlock() && HistoryManager.Get().HasHistoryEntry())
          this.AddCthunToHistory();
        if (this.m_currentTaskList.WillBlockCompleteHistoryEntry())
          HistoryManager.Get().MarkCurrentHistoryEntryAsCompleted();
      }
      GameState.Get().OnTaskListEnded(this.m_currentTaskList);
      this.m_currentTaskList = (PowerTaskList) null;
    }
  }

  private void AddCthunToHistory()
  {
    Entity ritualEntityClone = this.m_currentTaskList.GetOrigin().GetRitualEntityClone();
    if (ritualEntityClone == null)
      return;
    HistoryManager.Get().NotifyEntityAffected(ritualEntityClone, true, false);
    Entity entity = GameState.Get().GetEntity(this.m_currentTaskList.GetSourceEntity().GetController().GetTag(GAME_TAG.PROXY_CTHUN));
    if (entity == null || entity.GetTag(GAME_TAG.ATK) == ritualEntityClone.GetTag(GAME_TAG.ATK) && entity.GetTag(GAME_TAG.HEALTH) == ritualEntityClone.GetTag(GAME_TAG.HEALTH) && entity.GetTag(GAME_TAG.TAUNT) == ritualEntityClone.GetTag(GAME_TAG.TAUNT))
      return;
    HistoryManager.Get().NotifyEntityAffected(entity, true, false);
  }

  private bool DoTaskListWithSpellController(GameState state, PowerTaskList taskList, Entity sourceEntity)
  {
    HistoryBlock.Type blockType = taskList.GetBlockType();
    switch (blockType)
    {
      case HistoryBlock.Type.ATTACK:
        AttackSpellController attackSpellController = this.CreateAttackSpellController(taskList);
        if (this.DoTaskListUsingController((SpellController) attackSpellController, taskList))
          return true;
        this.DestroySpellController((SpellController) attackSpellController);
        return false;
      case HistoryBlock.Type.POWER:
        PowerSpellController powerSpellController = this.CreatePowerSpellController(taskList);
        if (this.DoTaskListUsingController((SpellController) powerSpellController, taskList))
          return true;
        this.DestroySpellController((SpellController) powerSpellController);
        return false;
      case HistoryBlock.Type.TRIGGER:
        if (sourceEntity.IsSecret())
        {
          SecretSpellController secretSpellController = this.CreateSecretSpellController(taskList);
          if (!this.DoTaskListUsingController((SpellController) secretSpellController, taskList))
          {
            this.DestroySpellController((SpellController) secretSpellController);
            return false;
          }
        }
        else
        {
          TriggerSpellController triggerSpellController = this.CreateTriggerSpellController(taskList);
          if (TurnStartManager.Get().IsCardDrawHandled(sourceEntity.GetCard()))
          {
            if (!triggerSpellController.AttachPowerTaskList(taskList))
            {
              this.DestroySpellController((SpellController) triggerSpellController);
              return false;
            }
            triggerSpellController.AddFinishedTaskListCallback(new SpellController.FinishedTaskListCallback(this.OnSpellControllerFinishedTaskList));
            triggerSpellController.AddFinishedCallback(new SpellController.FinishedCallback(this.OnSpellControllerFinished));
            TurnStartManager.Get().NotifyOfSpellController((SpellController) triggerSpellController);
          }
          else if (!this.DoTaskListUsingController((SpellController) triggerSpellController, taskList))
          {
            this.DestroySpellController((SpellController) triggerSpellController);
            return false;
          }
        }
        return true;
      case HistoryBlock.Type.DEATHS:
        DeathSpellController deathSpellController = this.CreateDeathSpellController(taskList);
        if (this.DoTaskListUsingController((SpellController) deathSpellController, taskList))
          return true;
        this.DestroySpellController((SpellController) deathSpellController);
        return false;
      case HistoryBlock.Type.FATIGUE:
        FatigueSpellController fatigueSpellController = this.CreateFatigueSpellController(taskList);
        if (!fatigueSpellController.AttachPowerTaskList(taskList))
        {
          this.DestroySpellController((SpellController) fatigueSpellController);
          return false;
        }
        fatigueSpellController.AddFinishedTaskListCallback(new SpellController.FinishedTaskListCallback(this.OnSpellControllerFinishedTaskList));
        fatigueSpellController.AddFinishedCallback(new SpellController.FinishedCallback(this.OnSpellControllerFinished));
        if (state.IsTurnStartManagerActive())
          TurnStartManager.Get().NotifyOfSpellController((SpellController) fatigueSpellController);
        else
          fatigueSpellController.DoPowerTaskList();
        return true;
      case HistoryBlock.Type.JOUST:
        JoustSpellController joustSpellController = this.CreateJoustSpellController(taskList);
        if (this.DoTaskListUsingController((SpellController) joustSpellController, taskList))
          return true;
        this.DestroySpellController((SpellController) joustSpellController);
        return false;
      case HistoryBlock.Type.RITUAL:
        RitualSpellController ritualSpellController = this.CreateRitualSpellController(taskList);
        if (this.DoTaskListUsingController((SpellController) ritualSpellController, taskList))
          return true;
        this.DestroySpellController((SpellController) ritualSpellController);
        return false;
      default:
        Log.Power.Print("PowerProcessor.DoTaskListForCard() - unhandled BlockType {0} for sourceEntity {1}", new object[2]
        {
          (object) blockType,
          (object) sourceEntity
        });
        return false;
    }
  }

  private bool DoTaskListUsingController(SpellController spellController, PowerTaskList taskList)
  {
    if ((UnityEngine.Object) spellController == (UnityEngine.Object) null)
    {
      Log.Power.Print("PowerProcessor.DoTaskListUsingController() - spellController=null");
      return false;
    }
    if (!spellController.AttachPowerTaskList(taskList))
      return false;
    spellController.AddFinishedTaskListCallback(new SpellController.FinishedTaskListCallback(this.OnSpellControllerFinishedTaskList));
    spellController.AddFinishedCallback(new SpellController.FinishedCallback(this.OnSpellControllerFinished));
    spellController.DoPowerTaskList();
    return true;
  }

  private void OnSpellControllerFinishedTaskList(SpellController spellController)
  {
    spellController.DetachPowerTaskList();
    if (this.m_currentTaskList == null)
      return;
    this.DoCurrentTaskList();
  }

  private void OnSpellControllerFinished(SpellController spellController)
  {
    this.DestroySpellController(spellController);
  }

  private AttackSpellController CreateAttackSpellController(PowerTaskList taskList)
  {
    return this.CreateSpellController<AttackSpellController>(taskList, "AttackSpellController");
  }

  private SecretSpellController CreateSecretSpellController(PowerTaskList taskList)
  {
    return this.CreateSpellController<SecretSpellController>(taskList, "SecretSpellController");
  }

  private PowerSpellController CreatePowerSpellController(PowerTaskList taskList)
  {
    return this.CreateSpellController<PowerSpellController>(taskList, (string) null);
  }

  private TriggerSpellController CreateTriggerSpellController(PowerTaskList taskList)
  {
    return this.CreateSpellController<TriggerSpellController>(taskList, (string) null);
  }

  private DeathSpellController CreateDeathSpellController(PowerTaskList taskList)
  {
    return this.CreateSpellController<DeathSpellController>(taskList, (string) null);
  }

  private FatigueSpellController CreateFatigueSpellController(PowerTaskList taskList)
  {
    return this.CreateSpellController<FatigueSpellController>(taskList, (string) null);
  }

  private JoustSpellController CreateJoustSpellController(PowerTaskList taskList)
  {
    return this.CreateSpellController<JoustSpellController>(taskList, "JoustSpellController");
  }

  private RitualSpellController CreateRitualSpellController(PowerTaskList taskList)
  {
    return this.CreateSpellController<RitualSpellController>(taskList, "RitualSpellController");
  }

  private T CreateSpellController<T>(PowerTaskList taskList, string prefabName = null) where T : SpellController
  {
    GameObject gameObject;
    T obj;
    if (prefabName == null)
    {
      gameObject = new GameObject();
      obj = gameObject.AddComponent<T>();
    }
    else
    {
      gameObject = AssetLoader.Get().LoadGameObject(prefabName, true, false);
      obj = gameObject.GetComponent<T>();
    }
    gameObject.name = string.Format("{0} [taskListId={1}]", (object) typeof (T), (object) taskList.GetId());
    return obj;
  }

  private void DestroySpellController(SpellController spellController)
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) spellController.gameObject);
  }
}
