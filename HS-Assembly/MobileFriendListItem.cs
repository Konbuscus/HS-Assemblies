﻿// Decompiled with JetBrains decompiler
// Type: MobileFriendListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class MobileFriendListItem : MonoBehaviour, ITouchListItem, ISelectableTouchListItem
{
  private Bounds m_localBounds;
  private ITouchListItem m_parent;
  private GameObject m_showObject;

  public MobileFriendListItem.TypeFlags Type { get; set; }

  public Bounds LocalBounds
  {
    get
    {
      return this.m_localBounds;
    }
  }

  public bool Selectable
  {
    get
    {
      if (this.Type != MobileFriendListItem.TypeFlags.Friend)
        return this.Type == MobileFriendListItem.TypeFlags.NearbyPlayer;
      return true;
    }
  }

  public bool IsHeader
  {
    get
    {
      return this.m_parent == null;
    }
  }

  public bool Visible
  {
    get
    {
      if (this.m_parent != null)
        return this.m_parent.Visible;
      return false;
    }
    set
    {
      if ((UnityEngine.Object) this.m_showObject == (UnityEngine.Object) null || value == this.m_showObject.activeSelf)
        return;
      this.m_showObject.SetActive(value);
    }
  }

  public void SetParent(ITouchListItem parent)
  {
    this.m_parent = parent;
  }

  public void SetShowObject(GameObject showobj)
  {
    this.m_showObject = showobj;
  }

  private void Awake()
  {
    Transform parent = this.transform.parent;
    TransformProps transformProps = new TransformProps();
    TransformUtil.CopyWorld(transformProps, (Component) this.transform);
    this.transform.parent = (Transform) null;
    TransformUtil.Identity((Component) this.transform);
    this.m_localBounds = this.ComputeWorldBounds();
    this.transform.parent = parent;
    TransformUtil.CopyWorld((Component) this.transform, transformProps);
  }

  public bool IsSelected()
  {
    FriendListUIElement component = this.GetComponent<FriendListUIElement>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      return component.IsSelected();
    return false;
  }

  public void Selected()
  {
    FriendListUIElement component = this.GetComponent<FriendListUIElement>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.SetSelected(true);
  }

  public void Unselected()
  {
    FriendListUIElement component = this.GetComponent<FriendListUIElement>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.SetSelected(false);
  }

  public Bounds ComputeWorldBounds()
  {
    return TransformUtil.ComputeSetPointBounds(this.gameObject);
  }

  public new T GetComponent<T>() where T : Component
  {
    return base.GetComponent<T>();
  }

  GameObject ITouchListItem.get_gameObject()
  {
    return this.gameObject;
  }

  Transform ITouchListItem.get_transform()
  {
    return this.transform;
  }

  [Flags]
  public enum TypeFlags
  {
    Request = 128,
    NearbyPlayer = 64,
    CurrentGame = 32,
    Friend = 16,
    Header = 1,
  }
}
