﻿// Decompiled with JetBrains decompiler
// Type: DeckPickerTrayDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class DeckPickerTrayDisplay : MonoBehaviour
{
  private static readonly Vector3 INNKEEPER_QUOTE_POS = new Vector3(103f, NotificationManager.DEPTH, 42f);
  private static Coroutine s_selectHeroCoroutine = (Coroutine) null;
  public static readonly PlatformDependentValue<bool> HighlightSelectedDeck = new PlatformDependentValue<bool>(PlatformCategory.Screen) { Phone = false, Tablet = true, PC = true };
  private float m_showQuestPause = 1f;
  private float m_playVOPause = 1f;
  private Vector3 m_formatTutorialPopupScale = new Vector3(1.1f, 1.1f, 1.1f);
  private Vector3 m_formatTutorialPopupScalePhone = new Vector3(2.3f, 2.3f, 2.3f);
  private readonly List<TAG_CLASS> HERO_CLASSES = new List<TAG_CLASS>() { TAG_CLASS.WARRIOR, TAG_CLASS.SHAMAN, TAG_CLASS.ROGUE, TAG_CLASS.PALADIN, TAG_CLASS.HUNTER, TAG_CLASS.DRUID, TAG_CLASS.WARLOCK, TAG_CLASS.MAGE, TAG_CLASS.PRIEST };
  private List<HeroPickerButton> m_heroButtons = new List<HeroPickerButton>();
  private Map<string, FullDef> m_heroPowerDefs = new Map<string, FullDef>();
  private int m_numPagesToShow = 1;
  private int m_heroDefsLoading = int.MaxValue;
  private List<DeckPickerTrayDisplay.DeckTrayLoaded> m_DeckTrayLoadedListeners = new List<DeckPickerTrayDisplay.DeckTrayLoaded>();
  private float m_numCardsPerClass = -1f;
  private const int MAX_PRECON_DECKS_TO_DISPLAY = 9;
  private const float TRAY_SLIDE_TIME = 0.25f;
  private const float TRAY_SINK_TIME = 0.0f;
  public GameObject m_randomDeckPickerTray;
  public Transform m_Hero_Bone;
  public Transform m_Hero_BoneDown;
  public Transform m_HeroPower_Bone;
  public Transform m_HeroPower_BoneDown;
  public GameObject m_heroPowerShadowQuad;
  public Transform m_rankedPlayButtonsBone;
  public Texture m_emptyHeroTexture;
  public UberText m_heroName;
  public UberText m_modeName;
  public UIBButton m_backButton;
  public PlayButton m_playButton;
  public NestedPrefab m_leftArrowNestedPrefab;
  public NestedPrefab m_rightArrowNestedPrefab;
  public GameObject m_randomTray;
  public GameObject m_trayFrame;
  public GameObject m_modeLabelBg;
  public GameObject m_randomDecksShownBone;
  public GameObject m_randomDecksHiddenBone;
  public GameObject m_suckedInRandomDecksBone;
  public GameObject m_heroPrefab;
  public Vector3 m_heroPickerButtonStart;
  public Vector3 m_heroPickerButtonScale;
  public float m_heroPickerButtonHorizontalSpacing;
  public float m_heroPickerButtonVerticalSpacing;
  public GameObject m_basicDeckPageContainer;
  public List<NestedPrefab> m_customDeckPageContainers;
  public GameObject m_tooltipPrefab;
  public Transform m_tooltipBone;
  public HeroXPBar m_xpBarPrefab;
  public GameObject m_rankedWinsPlate;
  public UberText m_rankedWins;
  public BoxCollider m_expoClickBlocker;
  public Animator m_premadeDeckGlowAnimator;
  public GameObject m_hierarchyDeckTray;
  public GameObject m_hierarchyDetails;
  public MissingStandardDeckDisplay m_missingStandardDeckDisplay;
  public UIBButton m_collectionButton;
  public HighlightState m_collectionButtonGlow;
  public GameObject m_labelDecoration;
  public PlayMakerFSM m_vineGlowBurst;
  public List<GameObject> m_premadeDeckGlowBurstObjects;
  public NestedPrefab m_switchFormatButtonContainer;
  private SwitchFormatButton m_switchFormatButton;
  public GameObject m_TheClockButtonBone;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_standardTransitionSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_wildTransitionSound;
  [CustomEditField(Sections = "Phone Only")]
  public SlidingTray m_rankedDetailsTray;
  [CustomEditField(Sections = "Phone Only")]
  public GameObject m_detailsTrayFrame;
  [CustomEditField(Sections = "Phone Only")]
  public Transform m_medalBone_phone;
  [CustomEditField(Sections = "Phone Only")]
  public Mesh m_alternateDetailsTrayMesh;
  [CustomEditField(Sections = "Phone Only")]
  public Material m_arrowButtonShadowMaterial;
  private bool m_heroChosen;
  private Actor m_heroActor;
  private UIBButton m_leftArrow;
  private UIBButton m_rightArrow;
  private PegUIElement m_heroPower;
  private Actor m_heroPowerActor;
  private PegUIElement m_goldenHeroPower;
  private Actor m_goldenHeroPowerActor;
  private bool m_isMouseOverHeroPower;
  private FullDef m_selectedHeroPowerFullDef;
  private Actor m_heroPowerBigCard;
  private Actor m_goldenHeroPowerBigCard;
  private HeroXPBar m_xpBar;
  private HeroPickerButton m_selectedHeroButton;
  private CollectionDeckBoxVisual m_selectedCustomDeckBox;
  private DeckPickerMode m_deckPickerMode;
  private bool m_showingSecondPage;
  private static DeckPickerTrayDisplay s_instance;
  private string gameMode;
  private UnityEngine.Vector2 m_keyholeTextureOffset;
  private string m_selectedHeroName;
  private RankedPlayDisplay m_rankedPlayButtons;
  private int m_numDecks;
  private TooltipPanel m_tooltip;
  private CustomDeckPage[] m_customPages;
  private bool m_buttonAchievementsInitialized;
  private bool m_delayButtonAnims;
  private bool m_Loaded;
  private Notification m_expoThankQuote;
  private Notification m_expoIntroQuote;
  private bool m_needUnlockAllHeroesTransition;
  private bool m_isUsingWildVisuals;
  private Notification m_switchFormatPopup;
  private Notification m_innkeeperQuote;
  private GameLayer m_defaultDetailsLayer;
  private SlidingTray m_slidingTray;
  private bool m_innkeeperQuoteFinished;
  [CustomEditField(Sections = "Set Rotation Tutorial")]
  public GameObject m_formatTutorialPopUpPrefab;
  [CustomEditField(Sections = "Set Rotation Tutorial")]
  public Transform m_formatTutorialPopUpBone;
  [CustomEditField(Sections = "Set Rotation Tutorial")]
  public Transform m_Switch_Format_Notification_Bone;
  [CustomEditField(Sections = "Set Rotation Tutorial")]
  public Animator m_dimQuad;
  [CustomEditField(Sections = "Set Rotation Tutorial")]
  public PegUIElement m_clickCatcher;
  [CustomEditField(Sections = "Set Rotation Tutorial", T = EditType.SOUND_PREFAB)]
  public string m_formatPopUpShowSound;
  [CustomEditField(Sections = "Set Rotation Tutorial", T = EditType.SOUND_PREFAB)]
  public string m_formatPopUpHideSound;
  [CustomEditField(Sections = "Set Rotation Tutorial", T = EditType.SOUND_PREFAB)]
  public string m_wildDeckTransitionSound;
  private DeckPickerTrayDisplay.SetRotationTutorialState m_setRotationTutorialState;

  private void Awake()
  {
    this.m_randomDeckPickerTray.transform.localPosition = this.m_randomDecksShownBone.transform.localPosition;
    SoundManager.Get().Load("hero_panel_slide_on");
    SoundManager.Get().Load("hero_panel_slide_off");
    if (SceneMgr.Get().GetPrevMode() == SceneMgr.Mode.GAMEPLAY)
    {
      this.m_delayButtonAnims = true;
      LoadingScreen.Get().RegisterFinishedTransitionListener(new LoadingScreen.FinishedTransitionCallback(this.OnTransitionFromGameplayFinished));
    }
    DeckPickerTray.Get().RegisterHandlers();
    SceneMgr.Get().RegisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
    DeckPickerTrayDisplay.s_instance = this;
    if ((UnityEngine.Object) this.m_heroPowerShadowQuad != (UnityEngine.Object) null)
      this.m_heroPowerShadowQuad.SetActive(false);
    this.LoadHero();
    if (this.ShouldShowHeroPower())
    {
      this.LoadHeroPower();
      this.LoadGoldenHeroPower();
    }
    this.m_heroName.RichText = false;
    if ((UnityEngine.Object) this.m_backButton != (UnityEngine.Object) null)
    {
      this.m_backButton.SetText(GameStrings.Get("GLOBAL_BACK"));
      this.m_backButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.BackButtonPress));
    }
    if ((UnityEngine.Object) this.m_collectionButton != (UnityEngine.Object) null)
    {
      this.EnableCollectionButton(this.ShouldShowCollectionButton());
      if (this.m_collectionButton.IsEnabled())
      {
        this.m_collectionButton.SetText(GameStrings.Get("GLUE_MY_COLLECTION"));
        this.m_collectionButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.CollectionButtonPress));
      }
    }
    this.m_playButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.PlayGameButtonRelease));
    this.EnablePlayButton(false);
    this.m_heroName.Text = string.Empty;
    this.m_xpBar = UnityEngine.Object.Instantiate<HeroXPBar>(this.m_xpBarPrefab);
    this.m_xpBar.m_soloLevelLimit = NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>().XPSoloLimit;
    Navigation.PushIfNotOnTop(new Navigation.NavigateBackHandler(DeckPickerTrayDisplay.OnNavigateBack));
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_slidingTray = this.gameObject.GetComponentInChildren<SlidingTray>();
      this.m_slidingTray.RegisterTrayToggleListener(new SlidingTray.TrayToggledListener(this.OnSlidingTrayToggled));
    }
    this.StartCoroutine(this.InitModeWhenReady());
  }

  private void Start()
  {
    this.m_leftArrow = this.m_leftArrowNestedPrefab.PrefabGameObject(false).GetComponent<UIBButton>();
    this.m_leftArrow.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnShowFirstPage));
    this.m_rightArrow = this.m_rightArrowNestedPrefab.PrefabGameObject(false).GetComponent<UIBButton>();
    this.m_rightArrow.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnShowSecondPage));
    if (this.m_numPagesToShow <= 1)
    {
      this.m_leftArrow.gameObject.SetActive(false);
      this.m_rightArrow.gameObject.SetActive(false);
    }
    if (!((UnityEngine.Object) this.m_switchFormatButtonContainer != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_switchFormatButtonContainer.PrefabGameObject(false) != (UnityEngine.Object) null))
      return;
    this.m_switchFormatButton = this.m_switchFormatButtonContainer.PrefabGameObject(false).GetComponent<SwitchFormatButton>();
    bool flag = (SceneMgr.Get().GetMode() == SceneMgr.Mode.TOURNAMENT || SceneMgr.Get().GetMode() == SceneMgr.Mode.COLLECTIONMANAGER) && CollectionManager.Get().ShouldAccountSeeStandardWild();
    if (flag)
    {
      this.m_switchFormatButton.Uncover();
      this.m_switchFormatButton.SetFormat(Options.Get().GetBool(Option.IN_WILD_MODE), true);
      if (!flag)
        return;
      this.m_switchFormatButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.SwitchFormatButtonPress));
      this.m_switchFormatButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.SwitchFormatButtonRollover));
      this.m_switchFormatButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.SwitchFormatButtonRollout));
    }
    else
    {
      this.m_switchFormatButton.Cover();
      this.m_switchFormatButton.Disable();
    }
  }

  private void OnDestroy()
  {
    this.HideDemoQuotes();
    DeckPickerTray.Get().UnregisterHandlers();
    if ((UnityEngine.Object) TournamentDisplay.Get() != (UnityEngine.Object) null)
      TournamentDisplay.Get().RemoveMedalChangedListener(new TournamentDisplay.DelMedalChanged(this.OnMedalChanged));
    if (FriendChallengeMgr.Get() != null && (UnityEngine.Object) DeckPickerTrayDisplay.Get() != (UnityEngine.Object) null)
      FriendChallengeMgr.Get().RemoveChangedListener(new FriendChallengeMgr.ChangedCallback(DeckPickerTrayDisplay.Get().OnFriendChallengeChanged));
    DeckPickerTrayDisplay.s_instance = (DeckPickerTrayDisplay) null;
  }

  public static DeckPickerTrayDisplay Get()
  {
    return DeckPickerTrayDisplay.s_instance;
  }

  public void Init()
  {
    this.StartCoroutine(this.InitDeckDependentElements());
  }

  public bool IsShowingCustomDecks()
  {
    return this.m_deckPickerMode == DeckPickerMode.CUSTOM;
  }

  public void SuckInFinished()
  {
    this.m_randomDeckPickerTray.SetActive(false);
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
  }

  private void OnShowSecondPage(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("hero_panel_slide_off");
    this.ShowSecondPage();
  }

  public void ResetCurrentMode()
  {
    if (this.IsShowingCustomDecks())
    {
      if ((UnityEngine.Object) this.m_selectedCustomDeckBox != (UnityEngine.Object) null)
      {
        this.EnablePlayButton(true);
        this.RaiseHero();
      }
      else
      {
        if ((UnityEngine.Object) this.m_selectedHeroButton != (UnityEngine.Object) null)
          this.RaiseHero();
        this.EnablePlayButton(false);
      }
    }
    else if ((UnityEngine.Object) this.m_selectedHeroButton != (UnityEngine.Object) null)
    {
      this.EnablePlayButton(true);
      this.RaiseHero();
    }
    else
      this.EnablePlayButton(false);
    this.EnableHeroButtons();
  }

  public int GetSelectedHeroLevel()
  {
    if ((UnityEngine.Object) this.m_selectedHeroButton == (UnityEngine.Object) null)
      return 0;
    return GameUtils.GetHeroLevel(this.m_selectedHeroButton.GetFullDef().GetEntityDef().GetClass()).CurrentLevel.Level;
  }

  public void SetPlayButtonText(string text)
  {
    this.m_playButton.SetText(text);
  }

  public void ToggleRankedDetailsTray(bool shown)
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_rankedDetailsTray.ToggleTraySlider(shown, (Transform) null, true);
  }

  public long GetSelectedDeckID()
  {
    if (this.IsShowingCustomDecks())
    {
      if ((UnityEngine.Object) this.m_selectedCustomDeckBox == (UnityEngine.Object) null)
        return 0;
      return this.m_selectedCustomDeckBox.GetDeckID();
    }
    if ((UnityEngine.Object) this.m_selectedHeroButton == (UnityEngine.Object) null)
      return 0;
    return this.m_selectedHeroButton.GetPreconDeckID();
  }

  public bool GetSelectDeckIsWild()
  {
    if (this.m_deckPickerMode == DeckPickerMode.CUSTOM && !((UnityEngine.Object) this.m_selectedCustomDeckBox == (UnityEngine.Object) null))
      return this.m_selectedCustomDeckBox.IsWild();
    return false;
  }

  public void SetHeaderText(string text)
  {
    this.m_modeName.Text = text;
  }

  public void UpdateCreateDeckText()
  {
    string key = !Options.Get().GetBool(Option.IN_WILD_MODE) ? "GLUE_CREATE_STANDARD_DECK" : "GLUE_CREATE_WILD_DECK";
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
      key = !TavernBrawlManager.Get().IsCurrentSeasonSessionBased ? "GLOBAL_TAVERN_BRAWL" : "GLOBAL_HEROIC_BRAWL";
    this.SetHeaderText(GameStrings.Get(key));
  }

  public bool UpdateRankedClassWinsPlate()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TOURNAMENT && Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE) && this.m_heroActor.GetEntityDef() != null)
    {
      string heroCardID = this.m_heroActor.GetEntityDef().GetCardId();
      if (this.m_heroActor.GetEntityDef().GetCardSet() == TAG_CARD_SET.HERO_SKINS)
        heroCardID = CollectionManager.Get().GetVanillaHeroCardID(this.m_heroActor.GetEntityDef());
      Achievement goldenHeroAchievement = AchieveManager.Get().GetUnlockGoldenHeroAchievement(heroCardID, TAG_PREMIUM.GOLDEN);
      int progress = goldenHeroAchievement.Progress;
      if (progress == 0 || progress >= goldenHeroAchievement.MaxProgress)
      {
        this.m_rankedWinsPlate.SetActive(false);
        return false;
      }
      this.m_rankedWins.Text = GameStrings.Format(!(bool) UniversalInputManager.UsePhoneUI ? "GLOBAL_HERO_WINS" : "GLOBAL_HERO_WINS_PHONE", (object) progress, (object) goldenHeroAchievement.MaxProgress);
      this.m_rankedWinsPlate.SetActive(true);
      return true;
    }
    this.m_rankedWinsPlate.SetActive(false);
    return false;
  }

  public void UpdateMissingStandardDeckTray(bool animateInTray)
  {
    if ((UnityEngine.Object) this.m_missingStandardDeckDisplay == (UnityEngine.Object) null || this.m_setRotationTutorialState != DeckPickerTrayDisplay.SetRotationTutorialState.INACTIVE)
      return;
    if (!CollectionManager.Get().ShouldAccountSeeStandardWild())
    {
      this.m_missingStandardDeckDisplay.Hide();
    }
    else
    {
      bool flag1 = this.GetNumValidStandardDecks() > 0U;
      if (!flag1)
      {
        foreach (GameObject gameObject in this.m_customPages[0].m_customVineGlowToggle)
          gameObject.SetActive(false);
      }
      bool flag2 = SceneMgr.Get().GetMode() == SceneMgr.Mode.TOURNAMENT && !Options.Get().GetBool(Option.IN_WILD_MODE) && !flag1;
      if (flag2)
      {
        if (animateInTray)
          this.m_missingStandardDeckDisplay.Show();
        else
          this.m_missingStandardDeckDisplay.ShowImmediately();
        this.StopCoroutine("ArrowDelayedActivate");
        this.m_rightArrow.gameObject.SetActive(false);
        this.m_leftArrow.gameObject.SetActive(false);
      }
      else
      {
        this.m_missingStandardDeckDisplay.Hide();
        this.m_leftArrow.gameObject.SetActive(this.m_showingSecondPage);
        this.m_rightArrow.gameObject.SetActive(!this.m_showingSecondPage && this.m_customPages.Length > 1);
      }
      this.UpdateCollectionButtonGlow();
      this.m_rightArrow.SetEnabled(!flag2);
      this.m_leftArrow.SetEnabled(!flag2);
      foreach (CustomDeckPage customPage in this.m_customPages)
        customPage.EnableDeckButtons(!flag2);
    }
  }

  public bool IsMissingStandardDeckTrayShown()
  {
    if ((UnityEngine.Object) this.m_missingStandardDeckDisplay != (UnityEngine.Object) null)
      return this.m_missingStandardDeckDisplay.IsShown();
    return false;
  }

  public void Unload()
  {
    DeckPickerTray.Get().UnregisterHandlers();
  }

  public void OnApplicationPause(bool pauseStatus)
  {
    if (!GameMgr.Get().IsFindingGame())
      return;
    GameMgr.Get().CancelFindGame();
  }

  public void OnServerGameStarted()
  {
    FriendChallengeMgr.Get().RemoveChangedListener(new FriendChallengeMgr.ChangedCallback(this.OnFriendChallengeChanged));
  }

  public void OnServerGameCanceled()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.FRIENDLY || TavernBrawlManager.IsInTavernBrawlFriendlyChallenge())
      return;
    this.HandleGameStartupFailure();
  }

  public bool IsLoaded()
  {
    return this.m_Loaded;
  }

  public void AddDeckTrayLoadedListener(DeckPickerTrayDisplay.DeckTrayLoaded dlg)
  {
    this.m_DeckTrayLoadedListeners.Add(dlg);
  }

  public void RemoveDeckTrayLoadedListener(DeckPickerTrayDisplay.DeckTrayLoaded dlg)
  {
    this.m_DeckTrayLoadedListeners.Remove(dlg);
  }

  public void HandleGameStartupFailure()
  {
    this.EnablePlayButton(true);
    this.EnableBackButton(true);
    this.EnableHeroButtons();
    switch (SceneMgr.Get().GetMode())
    {
      case SceneMgr.Mode.TOURNAMENT:
        if (PresenceMgr.Get().CurrentStatus != PresenceStatus.PLAY_QUEUE)
          break;
        PresenceMgr.Get().SetPrevStatus();
        break;
      case SceneMgr.Mode.ADVENTURE:
        if (AdventureConfig.Get().GetCurrentSubScene() != AdventureSubScenes.Practice)
          break;
        PracticePickerTrayDisplay.Get().OnGameDenied();
        break;
    }
  }

  public void SetHeroDetailsTrayToIgnoreFullScreenEffects(bool ignoreEffects)
  {
    if ((UnityEngine.Object) this.m_hierarchyDetails == (UnityEngine.Object) null)
      return;
    if (ignoreEffects)
      SceneUtils.ReplaceLayer(this.m_hierarchyDetails, GameLayer.IgnoreFullScreenEffects, this.m_defaultDetailsLayer);
    else
      SceneUtils.ReplaceLayer(this.m_hierarchyDetails, this.m_defaultDetailsLayer, GameLayer.IgnoreFullScreenEffects);
  }

  public void UpdateRankedPlayDisplay()
  {
    this.m_rankedPlayButtons.UpdateMode();
  }

  public void ShowClickedWildDeckInStandardPopup()
  {
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.TOURNAMENT && SceneMgr.Get().GetMode() != SceneMgr.Mode.FRIENDLY || (!((UnityEngine.Object) this.m_switchFormatPopup == (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_innkeeperQuote == (UnityEngine.Object) null)))
      return;
    if (!this.m_switchFormatButton.IsCovered())
    {
      this.StopCoroutine("ShowSwitchToWildTutorialAfterTransitionsComplete");
      Action action = (Action) (() => this.m_switchFormatPopup = (Notification) null);
      this.m_switchFormatPopup = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.SET_ROTATION_INTRO, this.m_Switch_Format_Notification_Bone.position, this.m_Switch_Format_Notification_Bone.localScale, GameStrings.Get("GLUE_TOURNAMENT_SWITCH_TO_WILD"), true);
      if ((UnityEngine.Object) this.m_switchFormatPopup != (UnityEngine.Object) null)
      {
        this.m_switchFormatPopup.ShowPopUpArrow(!(bool) UniversalInputManager.UsePhoneUI ? Notification.PopUpArrowDirection.Up : Notification.PopUpArrowDirection.RightUp);
        this.m_switchFormatPopup.OnFinishDeathState += action;
      }
    }
    Action finishCallback = (Action) (() =>
    {
      if ((UnityEngine.Object) this.m_switchFormatButton != (UnityEngine.Object) null)
        NotificationManager.Get().DestroyNotification(this.m_switchFormatPopup, 0.0f);
      this.m_innkeeperQuote = (Notification) null;
    });
    this.m_innkeeperQuote = NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.SET_ROTATION_INTRO, DeckPickerTrayDisplay.INNKEEPER_QUOTE_POS, GameStrings.Get("VO_INNKEEPER_WILD_DECK_WARNING"), "VO_INNKEEPER_Male_Dwarf_SetRotation_32", 0.0f, finishCallback, false);
  }

  public void ShowSwitchToWildTutorialIfNecessary()
  {
    if ((UnityEngine.Object) this.m_switchFormatPopup != (UnityEngine.Object) null || !UserAttentionManager.CanShowAttentionGrabber(UserAttentionBlocker.SET_ROTATION_INTRO, "DeckPickerTrayDisplay.ShowSwitchToWildTutorialIfNecessary"))
      return;
    if (Options.Get().GetBool(Option.IN_WILD_MODE))
    {
      Options.Get().SetBool(Option.SHOW_SWITCH_TO_WILD_ON_CREATE_DECK, false);
      Options.Get().SetBool(Option.SHOW_SWITCH_TO_WILD_ON_PLAY_SCREEN, false);
    }
    bool flag = false;
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    if (Options.Get().GetBool(Option.SHOW_SWITCH_TO_WILD_ON_CREATE_DECK) && mode == SceneMgr.Mode.COLLECTIONMANAGER)
    {
      flag = true;
      Options.Get().SetBool(Option.SHOW_SWITCH_TO_WILD_ON_CREATE_DECK, false);
    }
    if (Options.Get().GetBool(Option.SHOW_SWITCH_TO_WILD_ON_PLAY_SCREEN) && mode == SceneMgr.Mode.TOURNAMENT)
    {
      flag = true;
      Options.Get().SetBool(Option.SHOW_SWITCH_TO_WILD_ON_PLAY_SCREEN, false);
    }
    if (!flag)
      return;
    this.StartCoroutine("ShowSwitchToWildTutorialAfterTransitionsComplete");
  }

  [DebuggerHidden]
  private IEnumerator ShowSwitchToWildTutorialAfterTransitionsComplete()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CShowSwitchToWildTutorialAfterTransitionsComplete\u003Ec__Iterator6E() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator InitDeckDependentElements()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CInitDeckDependentElements\u003Ec__Iterator6F() { \u003C\u003Ef__this = this };
  }

  private void InitForMode(SceneMgr.Mode mode)
  {
    bool isWild = Options.Get().GetBool(Option.IN_WILD_MODE);
    switch (mode)
    {
      case SceneMgr.Mode.COLLECTIONMANAGER:
      case SceneMgr.Mode.ADVENTURE:
        this.SetPlayButtonText(GameStrings.Get("GLUE_CHOOSE"));
        break;
      case SceneMgr.Mode.TOURNAMENT:
        AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "RankedPlayButtons" : "RankButtons_phone", new AssetLoader.GameObjectCallback(this.RankedPlayButtonsLoaded), (object) null, false);
        this.UpdateMissingStandardDeckTray(false);
        bool flag1 = CollectionManager.Get().ShouldAccountSeeStandardWild();
        bool flag2 = Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE);
        if (!flag1)
          this.SetPlayButtonText(!flag2 ? GameStrings.Get("GLOBAL_PLAY") : GameStrings.Get("GLOBAL_PLAY_RANKED"));
        else
          this.SetPlayButtonText(!isWild ? GameStrings.Get("GLOBAL_PLAY_STANDARD") : GameStrings.Get("GLOBAL_PLAY_WILD"));
        this.m_playButton.m_newPlayButtonText.TextAlpha = !this.m_playButton.IsEnabled() ? 0.0f : 1f;
        this.UpdateRankedClassWinsPlate();
        if (isWild)
        {
          this.m_switchFormatButton.DoWildFlip();
          break;
        }
        break;
      case SceneMgr.Mode.FRIENDLY:
        if (this.IsChoosingHeroForTavernBrawlChallenge())
        {
          this.SetHeaderForTavernBrawl();
          break;
        }
        this.SetPlayButtonText(GameStrings.Get("GLUE_CHOOSE"));
        break;
      case SceneMgr.Mode.TAVERN_BRAWL:
        this.SetHeaderForTavernBrawl();
        break;
    }
    switch (mode - 5)
    {
      case SceneMgr.Mode.INVALID:
        if ((bool) UniversalInputManager.UsePhoneUI)
        {
          this.m_detailsTrayFrame.GetComponent<MeshFilter>().mesh = this.m_alternateDetailsTrayMesh;
          this.SetCustomTrayTextures(AssetLoader.Get().LoadTexture("HeroPicker_TournamentAndCustom_phone", false), AssetLoader.Get().LoadTexture("HeroPicker_TournamentAndCustom_phone_Wild", false));
          this.SetDetailsTrayTextures(AssetLoader.Get().LoadTexture("DeckBuild_DeckHeroTray_phone", false), AssetLoader.Get().LoadTexture("DeckBuild_DeckHeroTray_phone_Wild", false));
        }
        else
          this.SetTrayTextures(AssetLoader.Get().LoadTexture("HeroPicker_CreateDeck", false), AssetLoader.Get().LoadTexture("HeroPicker_CreateDeck_Wild", false));
        this.UpdateTrayTransitionValues(isWild, false);
        this.m_keyholeTextureOffset = new UnityEngine.Vector2(0.0f, 0.0f);
        break;
      case SceneMgr.Mode.LOGIN:
        if ((bool) UniversalInputManager.UsePhoneUI)
        {
          this.SetCustomTrayTextures(AssetLoader.Get().LoadTexture("HeroPicker_TournamentAndCustom_phone", false), AssetLoader.Get().LoadTexture("HeroPicker_TournamentAndCustom_phone_Wild", false));
          this.SetDetailsTrayTextures(AssetLoader.Get().LoadTexture("HeroPicker_Tournament_HeroTray_phone", false), AssetLoader.Get().LoadTexture("HeroPicker_Tournament_HeroTray_phone_Wild", false));
        }
        else
        {
          if (this.ShouldShowCustomDecks())
            this.SetCustomTrayTextures(AssetLoader.Get().LoadTexture("HeroPicker_Custom_Tournament", false), AssetLoader.Get().LoadTexture("HeroPicker_Custom_Tournament_Wild", false));
          this.SetTrayTextures(AssetLoader.Get().LoadTexture("HeroPicker_Tournament", false), AssetLoader.Get().LoadTexture("HeroPicker_Tournament_Wild", false));
        }
        this.UpdateTrayTransitionValues(isWild, false);
        this.m_keyholeTextureOffset = new UnityEngine.Vector2(0.0f, 0.0f);
        break;
      case SceneMgr.Mode.HUB:
        if (this.IsChoosingHeroForTavernBrawlChallenge())
        {
          this.SetTexturesForTavernBrawl();
          break;
        }
        if ((bool) UniversalInputManager.UsePhoneUI)
        {
          this.m_detailsTrayFrame.GetComponent<MeshFilter>().mesh = this.m_alternateDetailsTrayMesh;
          this.SetCustomTrayTextures(AssetLoader.Get().LoadTexture("HeroPicker_FriendlyAndCustom_phone", false), AssetLoader.Get().LoadTexture("HeroPicker_FriendlyAndCustom_phone_Wild", false));
          this.SetDetailsTrayTextures(AssetLoader.Get().LoadTexture("Friendly_DeckHeroTray_phone", false), AssetLoader.Get().LoadTexture("Friendly_DeckHeroTray_phone_Wild", false));
        }
        else
        {
          if (this.ShouldShowCustomDecks())
            this.SetCustomTrayTextures(AssetLoader.Get().LoadTexture("HeroPicker_Custom_Friendly", false), AssetLoader.Get().LoadTexture("HeroPicker_Custom_Friendly_Wild", false));
          this.SetTrayTextures(AssetLoader.Get().LoadTexture("HeroPicker_Friendly", false), AssetLoader.Get().LoadTexture("HeroPicker_Friendly_Wild", false));
        }
        this.UpdateTrayTransitionValues(isWild, false);
        this.m_keyholeTextureOffset = new UnityEngine.Vector2(0.0f, 0.61f);
        break;
      case SceneMgr.Mode.FRIENDLY:
        if ((bool) UniversalInputManager.UsePhoneUI)
        {
          this.m_detailsTrayFrame.GetComponent<MeshFilter>().mesh = this.m_alternateDetailsTrayMesh;
          Texture texture1 = AssetLoader.Get().LoadTexture("HeroPicker_PracticeAndCustom_phone", false);
          this.SetCustomTrayTextures(texture1, texture1);
          Texture texture2 = AssetLoader.Get().LoadTexture("Practice_DeckHeroTray_phone", false);
          this.SetDetailsTrayTextures(texture2, texture2);
        }
        else
        {
          if (this.ShouldShowCustomDecks())
          {
            Texture texture = AssetLoader.Get().LoadTexture("HeroPicker_Custom_Practice", false);
            this.SetCustomTrayTextures(texture, texture);
          }
          Texture texture1 = AssetLoader.Get().LoadTexture("HeroPicker_Practice", false);
          this.SetTrayTextures(texture1, texture1);
        }
        this.UpdateTrayTransitionValues(false, false);
        this.m_keyholeTextureOffset = new UnityEngine.Vector2(0.5f, 0.0f);
        break;
      case SceneMgr.Mode.FATAL_ERROR:
        this.SetTexturesForTavernBrawl();
        break;
    }
  }

  private void UpdateFormat_Tournament()
  {
    bool isWild = Options.Get().GetBool(Option.IN_WILD_MODE);
    if (CollectionManager.Get().ShouldAccountSeeStandardWild())
    {
      this.SetPlayButtonText(!isWild ? GameStrings.Get("GLOBAL_PLAY_STANDARD") : GameStrings.Get("GLOBAL_PLAY_WILD"));
      this.m_switchFormatButton.Uncover();
      this.m_switchFormatButton.SetFormat(isWild, true);
      if (isWild && SceneMgr.Get().GetMode() == SceneMgr.Mode.TOURNAMENT && (GameUtils.HasSeenStandardModeTutorial() && !Options.Get().GetBool(Option.HAS_SEEN_WILD_MODE_VO)) && UserAttentionManager.CanShowAttentionGrabber("DeckPickerTrayDisplay.UpdateFormat_Tournament:" + (object) Option.HAS_SEEN_WILD_MODE_VO))
      {
        this.HideSetRotationNotifications();
        NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, DeckPickerTrayDisplay.INNKEEPER_QUOTE_POS, GameStrings.Get("VO_INNKEEPER_WILD_GAME"), "VO_INNKEEPER_Male_Dwarf_SetRotation_35", 0.0f, (Action) null, false);
        Options.Get().SetBool(Option.HAS_SEEN_WILD_MODE_VO, true);
      }
      if ((UnityEngine.Object) this.m_selectedCustomDeckBox != (UnityEngine.Object) null && this.m_selectedCustomDeckBox.IsWild() && !Options.Get().GetBool(Option.IN_WILD_MODE))
        this.Deselect();
      this.UpdateMissingStandardDeckTray(true);
      this.UpdateCustomTournamentBackgroundAndDecks();
    }
    this.m_playButton.m_newPlayButtonText.TextAlpha = !this.m_playButton.IsEnabled() ? 0.0f : 1f;
    this.UpdateRankedClassWinsPlate();
  }

  private void UpdateFormat_CollectionManager()
  {
    bool isWild = Options.Get().GetBool(Option.IN_WILD_MODE);
    if (isWild)
      NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, NotificationManager.DEFAULT_CHARACTER_POS, GameStrings.Get("VO_INNKEEPER_PLAY_STANDARD_TO_WILD"), "VO_INNKEEPER_Male_Dwarf_SetRotation_43", 0.0f, (Action) null, false);
    this.m_switchFormatButton.SetFormat(isWild, true);
    this.UpdateTrayTransitionValues(isWild, true);
  }

  private void UpdateCustomTournamentBackgroundAndDecks()
  {
    if (this.m_setRotationTutorialState != DeckPickerTrayDisplay.SetRotationTutorialState.INACTIVE)
      return;
    bool isWild = Options.Get().GetBool(Option.IN_WILD_MODE);
    this.UpdateTrayTransitionValues(isWild, true);
    foreach (CustomDeckPage customPage in this.m_customPages)
      customPage.UpdateDeckVisuals(isWild, true, false);
  }

  [DebuggerHidden]
  private IEnumerator InitButtonAchievements()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CInitButtonAchievements\u003Ec__Iterator70() { \u003C\u003Ef__this = this };
  }

  private void SetHeaderForTavernBrawl()
  {
    if ((UnityEngine.Object) this.m_labelDecoration != (UnityEngine.Object) null)
      this.m_labelDecoration.SetActive(false);
    string key = "GLUE_CHOOSE";
    if (FriendChallengeMgr.Get().IsChallengeTavernBrawl())
      key = "GLUE_BRAWL_FRIEND";
    else if (TavernBrawlManager.Get().SelectHeroBeforeMission())
      key = "GLUE_BRAWL";
    this.SetPlayButtonText(GameStrings.Get(key));
  }

  private void SetTexturesForTavernBrawl()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_detailsTrayFrame.GetComponent<MeshFilter>().mesh = this.m_alternateDetailsTrayMesh;
      Texture texture1 = AssetLoader.Get().LoadTexture("HeroPicker_TavernBrawl_phone", false);
      this.SetCustomTrayTextures(texture1, texture1);
      Texture texture2 = AssetLoader.Get().LoadTexture("TavernBrawl_DeckHeroTray_phone", false);
      this.SetDetailsTrayTextures(texture2, texture2);
    }
    else
    {
      Texture texture = AssetLoader.Get().LoadTexture("HeroPicker_TavernBrawl", false);
      this.SetTrayTextures(texture, texture);
    }
    this.UpdateTrayTransitionValues(false, false);
    this.m_keyholeTextureOffset = new UnityEngine.Vector2(0.5f, 0.61f);
  }

  private void InitHeroPickerButtons()
  {
    int num = 0;
    Vector3 pickerButtonStart = this.m_heroPickerButtonStart;
    float horizontalSpacing = this.m_heroPickerButtonHorizontalSpacing;
    float buttonVerticalSpacing = this.m_heroPickerButtonVerticalSpacing;
    for (; num < 9; ++num)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_heroPrefab);
      GameUtils.SetAutomationName(gameObject, (object) num);
      gameObject.transform.parent = this.m_basicDeckPageContainer.transform;
      gameObject.transform.localScale = this.m_heroPickerButtonScale;
      if (num == 0)
      {
        gameObject.transform.localPosition = pickerButtonStart;
      }
      else
      {
        float x = pickerButtonStart.x - (float) (num % 3) * horizontalSpacing;
        float z = (float) Mathf.CeilToInt((float) (num / 3)) * buttonVerticalSpacing + pickerButtonStart.z;
        gameObject.transform.localPosition = new Vector3(x, pickerButtonStart.y, z);
      }
      HeroPickerButton component = gameObject.transform.FindChild("HeroPremade_Frame").gameObject.GetComponent<HeroPickerButton>();
      int index = !(bool) UniversalInputManager.UsePhoneUI ? 0 : 1;
      component.m_buttonFrame.GetComponent<Renderer>().materials[index].mainTextureOffset = this.m_keyholeTextureOffset;
      this.m_heroButtons.Add(component);
    }
    int index1 = 0;
    this.m_heroDefsLoading = this.m_heroButtons.Count;
    List<CollectionDeck> decks = CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK);
    using (List<HeroPickerButton>.Enumerator enumerator = this.m_heroButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroPickerButton current = enumerator.Current;
        if (index1 >= this.HERO_CLASSES.Count)
        {
          Log.Derek.Print("TournamentDisplay - more buttons than heroes");
          break;
        }
        current.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.HeroPressed));
        current.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.MouseOverHero));
        current.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.MouseOutHero));
        current.SetOriginalLocalPosition();
        current.Lock();
        current.SetProgress(0, 0, 1);
        TAG_CLASS heroClass = this.HERO_CLASSES[index1];
        if (this.m_needUnlockAllHeroesTransition && index1 < decks.Count)
          heroClass = decks[index1].GetClass();
        NetCache.CardDefinition favoriteHero = CollectionManager.Get().GetFavoriteHero(heroClass);
        if (favoriteHero == null)
        {
          UnityEngine.Debug.LogWarning((object) ("Couldn't find Favorite Hero for hero class: " + (object) heroClass + " defaulting to Normal Vanilla Hero!"));
          DefLoader.Get().LoadFullDef(CollectionManager.Get().GetVanillaHeroCardIDFromClass(heroClass), new DefLoader.LoadDefCallback<FullDef>(this.OnHeroFullDefLoaded), (object) new DeckPickerTrayDisplay.HeroFullDefLoadedCallbackData(current, TAG_PREMIUM.NORMAL));
        }
        else
        {
          DeckPickerTrayDisplay.HeroFullDefLoadedCallbackData loadedCallbackData = new DeckPickerTrayDisplay.HeroFullDefLoadedCallbackData(current, favoriteHero.Premium);
          DefLoader.Get().LoadFullDef(favoriteHero.Name, new DefLoader.LoadDefCallback<FullDef>(this.OnHeroFullDefLoaded), (object) loadedCallbackData);
        }
        ++index1;
      }
    }
  }

  private void InitCustomPages()
  {
    this.m_customPages = new CustomDeckPage[this.m_numPagesToShow];
    for (int index = 0; index < this.m_numPagesToShow; ++index)
    {
      GameObject gameObject = this.m_customDeckPageContainers[index].PrefabGameObject(false);
      if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
        UnityEngine.Debug.LogErrorFormat("DeckPickerTrayDisplay - m_customDeckPageContainer[{0}]'s GameObject has not been loaded!", (object) index);
      this.m_customPages[index] = gameObject.GetComponent<CustomDeckPage>();
      this.m_customPages[index].SetDeckButtonCallback(new CustomDeckPage.DeckButtonCallback(this.OnCustomDeckPressed));
    }
    List<CollectionDeck> decks = CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK);
    foreach (CustomDeckPage customPage in this.m_customPages)
    {
      int count = Mathf.Min(decks.Count, 9);
      List<CollectionDeck> range = decks.GetRange(0, count);
      customPage.SetDecks(range);
      using (List<CollectionDeck>.Enumerator enumerator = range.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string powerCardIdFromHero = GameUtils.GetHeroPowerCardIdFromHero(enumerator.Current.HeroCardID);
          this.m_heroPowerDefs[powerCardIdFromHero] = DefLoader.Get().GetFullDef(powerCardIdFromHero, (CardPortraitQuality) null);
        }
      }
      decks.RemoveRange(0, count);
      if (decks.Count <= 0)
        break;
    }
    if (decks.Count <= 0)
      return;
    UnityEngine.Debug.LogWarningFormat("DeckPickerTrayDisplay - {0} more decks than we can display!", (object) decks.Count);
  }

  private void InitMode()
  {
    this.InitRichPresence();
    if (this.IsChoosingHero())
      this.ShowFirstPage();
    else
      this.SetSelectionAndPageFromOptions();
    this.InitExpoDemoMode();
    this.ShowSwitchToWildTutorialIfNecessary();
  }

  private void InitExpoDemoMode()
  {
    if (!DemoMgr.Get().IsExpoDemo())
      return;
    this.m_leftArrow.gameObject.SetActive(false);
    this.m_rightArrow.gameObject.SetActive(false);
    this.EnableBackButton(false);
    this.StartCoroutine("ShowDemoQuotes");
  }

  [DebuggerHidden]
  private IEnumerator ShowDemoQuotes()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CShowDemoQuotes\u003Ec__Iterator71() { \u003C\u003Ef__this = this };
  }

  private void ShowIntroQuote()
  {
    this.HideIntroQuote();
    string text = Vars.Key("Demo.IntroQuote").GetStr(string.Empty).Replace("\\n", "\n");
    if (DemoMgr.Get().GetMode() == DemoMode.BLIZZCON_2015)
      this.m_expoIntroQuote = NotificationManager.Get().CreateCharacterQuote("Reno_Quote", new Vector3(0.0f, NotificationManager.DEPTH, -54.22f), text, string.Empty, true, 0.0f, (Action) null, CanvasAnchor.CENTER);
    else
      this.m_expoIntroQuote = NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, new Vector3(147.6f, NotificationManager.DEPTH, 23.1f), text, string.Empty, 0.0f, (Action) null, false);
  }

  private void EnableExpoClickBlocker(bool enable)
  {
    if ((UnityEngine.Object) this.m_expoClickBlocker == (UnityEngine.Object) null)
      return;
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if (enable)
    {
      fullScreenFxMgr.SetBlurBrightness(1f);
      fullScreenFxMgr.SetBlurDesaturation(0.0f);
      fullScreenFxMgr.Vignette(0.4f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      fullScreenFxMgr.Blur(1f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
    else
    {
      fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
    this.m_expoClickBlocker.gameObject.SetActive(enable);
  }

  private void HideDemoQuotes()
  {
    if (!DemoMgr.Get().IsExpoDemo())
      return;
    this.StopCoroutine("ShowDemoQuotes");
    if ((UnityEngine.Object) this.m_expoThankQuote != (UnityEngine.Object) null)
    {
      NotificationManager.Get().DestroyNotification(this.m_expoThankQuote, 0.0f);
      this.m_expoThankQuote = (Notification) null;
      FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
      fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
    this.HideIntroQuote();
  }

  private void HideIntroQuote()
  {
    if (!((UnityEngine.Object) this.m_expoIntroQuote != (UnityEngine.Object) null))
      return;
    NotificationManager.Get().DestroyNotification(this.m_expoIntroQuote, 0.0f);
    this.m_expoIntroQuote = (Notification) null;
  }

  private void HideSetRotationNotifications()
  {
    if ((UnityEngine.Object) this.m_innkeeperQuote != (UnityEngine.Object) null)
    {
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.m_innkeeperQuote);
      this.m_innkeeperQuote = (Notification) null;
    }
    if (!((UnityEngine.Object) this.m_switchFormatPopup != (UnityEngine.Object) null))
      return;
    NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.m_switchFormatPopup);
    this.m_switchFormatPopup = (Notification) null;
  }

  private void OnTransitionFromGameplayFinished(bool cutoff, object userData)
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.FRIENDLY && !FriendChallengeMgr.Get().HasChallenge())
      this.GoBackUntilOnNavigateBackCalled();
    LoadingScreen.Get().UnregisterFinishedTransitionListener(new LoadingScreen.FinishedTransitionCallback(this.OnTransitionFromGameplayFinished));
    this.m_delayButtonAnims = false;
  }

  private void CollectionButtonPress(UIEvent e)
  {
    if (this.ShouldGlowCollectionButton())
    {
      if (!Options.Get().GetBool(Option.HAS_CLICKED_COLLECTION_BUTTON_FOR_NEW_DECK) && this.HaveDecksThatNeedNames())
        Options.Get().SetBool(Option.HAS_CLICKED_COLLECTION_BUTTON_FOR_NEW_DECK, true);
      else if (!Options.Get().GetBool(Option.HAS_CLICKED_COLLECTION_BUTTON_FOR_NEW_CARD) && this.HaveUnseenBasicCards())
        Options.Get().SetBool(Option.HAS_CLICKED_COLLECTION_BUTTON_FOR_NEW_CARD, true);
      if (Options.Get().GetBool(Option.GLOW_COLLECTION_BUTTON_AFTER_SET_ROTATION) && SceneMgr.Get().GetMode() == SceneMgr.Mode.TOURNAMENT)
        Options.Get().SetBool(Option.GLOW_COLLECTION_BUTTON_AFTER_SET_ROTATION, false);
    }
    if ((UnityEngine.Object) PracticePickerTrayDisplay.Get() != (UnityEngine.Object) null && PracticePickerTrayDisplay.Get().IsShown())
      Navigation.GoBack();
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.COLLECTIONMANAGER);
  }

  private void BackButtonPress(UIEvent e)
  {
    Navigation.GoBack();
  }

  private void SwitchFormatButtonPress(UIEvent e)
  {
    if (this.m_switchFormatButton.IsRotating())
      return;
    Options.Get().SetBool(Option.IN_WILD_MODE, !Options.Get().GetBool(Option.IN_WILD_MODE));
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TOURNAMENT)
    {
      this.UpdateFormat_Tournament();
      TournamentDisplay.Get().UpdateHeaderText();
      this.m_rankedPlayButtons.OnSwitchFormat();
    }
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.COLLECTIONMANAGER)
      return;
    this.UpdateCreateDeckText();
    this.UpdateFormat_CollectionManager();
  }

  private void SwitchFormatButtonRollover(UIEvent e)
  {
    this.m_switchFormatButton.ShowPopUp();
    this.m_switchFormatButton.EnableHighlight(false);
  }

  private void SwitchFormatButtonRollout(UIEvent e)
  {
    this.m_switchFormatButton.HidePopUp();
  }

  public static bool OnNavigateBack()
  {
    if ((UnityEngine.Object) DeckPickerTrayDisplay.Get() != (UnityEngine.Object) null && !DeckPickerTrayDisplay.Get().m_backButton.IsEnabled())
      return false;
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    switch (mode)
    {
      case SceneMgr.Mode.COLLECTIONMANAGER:
      case SceneMgr.Mode.TAVERN_BRAWL:
        if ((UnityEngine.Object) CollectionDeckTray.Get() != (UnityEngine.Object) null)
          CollectionDeckTray.Get().GetDecksContent().CreateNewDeckCancelled();
        if ((UnityEngine.Object) DeckPickerTrayDisplay.Get() != (UnityEngine.Object) null && !DeckPickerTrayDisplay.Get().m_heroChosen && (UnityEngine.Object) CollectionManagerDisplay.Get() != (UnityEngine.Object) null)
          CollectionManagerDisplay.Get().CancelSelectNewDeckHeroMode();
        if ((UnityEngine.Object) HeroPickerDisplay.Get() != (UnityEngine.Object) null)
          HeroPickerDisplay.Get().HideTray(0.0f);
        PresenceMgr.Get().SetPrevStatus();
        if (mode == SceneMgr.Mode.TAVERN_BRAWL)
          TavernBrawlDisplay.Get().EnablePlayButton();
        if ((UnityEngine.Object) CollectionManagerDisplay.Get() != (UnityEngine.Object) null)
        {
          DeckTemplatePicker deckTemplatePicker = !(bool) UniversalInputManager.UsePhoneUI ? CollectionManagerDisplay.Get().m_pageManager.GetDeckTemplatePicker() : CollectionManagerDisplay.Get().GetPhoneDeckTemplateTray();
          if ((UnityEngine.Object) deckTemplatePicker != (UnityEngine.Object) null)
          {
            Navigation.RemoveHandler(new Navigation.NavigateBackHandler(deckTemplatePicker.OnNavigateBack));
            break;
          }
          break;
        }
        break;
      case SceneMgr.Mode.TOURNAMENT:
        DeckPickerTrayDisplay.BackOutToHub();
        GameMgr.Get().CancelFindGame();
        break;
      case SceneMgr.Mode.FRIENDLY:
        DeckPickerTrayDisplay.BackOutToHub();
        FriendChallengeMgr.Get().CancelChallenge();
        break;
      case SceneMgr.Mode.ADVENTURE:
        AdventureConfig.Get().ChangeToLastSubScene(true);
        if (AdventureConfig.Get().GetCurrentSubScene() == AdventureSubScenes.Practice)
          PracticePickerTrayDisplay.Get().gameObject.SetActive(false);
        GameMgr.Get().CancelFindGame();
        break;
    }
    return true;
  }

  private static bool IsBackingOut()
  {
    return SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB);
  }

  private static void BackOutToHub()
  {
    if (DeckPickerTrayDisplay.IsBackingOut())
      return;
    if ((UnityEngine.Object) DeckPickerTrayDisplay.Get() != (UnityEngine.Object) null)
      FriendChallengeMgr.Get().RemoveChangedListener(new FriendChallengeMgr.ChangedCallback(DeckPickerTrayDisplay.Get().OnFriendChallengeChanged));
    if ((UnityEngine.Object) DeckPickerTrayDisplay.Get() != (UnityEngine.Object) null && DeckPickerTrayDisplay.Get().m_showingSecondPage)
      DeckPickerTrayDisplay.Get().SuckInPreconDecks();
    else
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    DeckPickerTrayDisplay.Get().m_slidingTray.ToggleTraySlider(false, (Transform) null, true);
  }

  public void PreUnload()
  {
    if (!this.m_showingSecondPage || !this.m_randomDeckPickerTray.activeSelf)
      return;
    this.m_randomDeckPickerTray.SetActive(false);
  }

  private void ShowSecondPage()
  {
    if (iTween.Count(this.m_randomDeckPickerTray) > 0 || this.m_customPages.Length < 2)
      return;
    this.m_customPages[1].gameObject.SetActive(true);
    this.HideAllPreconHighlights();
    this.LowerHeroButtons();
    if (this.ShouldHandleBoxTransition())
    {
      Box.Get().AddTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
      this.m_randomDeckPickerTray.SetActive(false);
      this.m_randomDeckPickerTray.transform.localPosition = this.m_randomDecksHiddenBone.transform.localPosition;
    }
    else
      iTween.MoveTo(this.m_randomDeckPickerTray, iTween.Hash((object) "time", (object) 0.25f, (object) "position", (object) this.m_randomDecksHiddenBone.transform.localPosition, (object) "isLocal", (object) true, (object) "delay", (object) 0.0f));
    this.m_showingSecondPage = true;
    this.StartCoroutine(this.ArrowDelayedActivate(this.m_leftArrow, 0.25f));
    this.m_rightArrow.gameObject.SetActive(false);
    Options.Get().SetBool(Option.HAS_SEEN_CUSTOM_DECK_PICKER, true);
  }

  [DebuggerHidden]
  private IEnumerator ArrowDelayedActivate(UIBButton arrow, float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CArrowDelayedActivate\u003Ec__Iterator72() { delay = delay, arrow = arrow, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003Earrow = arrow, \u003C\u003Ef__this = this };
  }

  private bool ShouldHandleBoxTransition()
  {
    return SceneMgr.Get().GetPrevMode() != SceneMgr.Mode.GAMEPLAY && (Box.Get().IsBusy() || Box.Get().GetState() == Box.State.LOADING || Box.Get().GetState() == Box.State.LOADING_HUB);
  }

  private void OnBoxTransitionFinished(object userData)
  {
    this.m_randomDeckPickerTray.SetActive(true);
    Box.Get().RemoveTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
  }

  private void OnScenePreUnload(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    this.HideSetRotationNotifications();
    SceneMgr.Get().UnregisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
  }

  private void LowerHeroButtons()
  {
    using (List<HeroPickerButton>.Enumerator enumerator = this.m_heroButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroPickerButton current = enumerator.Current;
        if (current.gameObject.activeSelf)
          current.Lower();
      }
    }
  }

  private void RaiseHeroButtons()
  {
    using (List<HeroPickerButton>.Enumerator enumerator = this.m_heroButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroPickerButton current = enumerator.Current;
        if (current.gameObject.activeSelf)
          current.Raise();
      }
    }
  }

  private void OnShowFirstPage(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("hero_panel_slide_on");
    this.ShowFirstPage();
  }

  private void ShowFirstPage()
  {
    if (iTween.Count(this.m_randomDeckPickerTray) > 0)
      return;
    this.m_showingSecondPage = false;
    if (this.ShouldShowPreconDecks())
      this.ShowPreconHighlights();
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    if (this.IsChoosingHero())
    {
      this.m_leftArrow.gameObject.SetActive(false);
      this.m_rightArrow.gameObject.SetActive(false);
      if ((UnityEngine.Object) this.m_modeLabelBg != (UnityEngine.Object) null)
        this.m_modeLabelBg.transform.localEulerAngles = new Vector3(180f, 0.0f, 0.0f);
      iTween.MoveTo(this.m_randomDeckPickerTray, iTween.Hash((object) "time", (object) 0.25f, (object) "position", (object) this.m_randomDecksShownBone.transform.localPosition, (object) "isLocal", (object) true, (object) "oncomplete", (object) "RaiseHeroButtons", (object) "oncompletetarget", (object) this.gameObject));
    }
    else
    {
      if (mode != SceneMgr.Mode.ADVENTURE && mode != SceneMgr.Mode.TOURNAMENT && mode != SceneMgr.Mode.FRIENDLY)
        return;
      this.m_leftArrow.gameObject.SetActive(false);
      if (this.m_numPagesToShow > 1)
        this.StartCoroutine(this.ArrowDelayedActivate(this.m_rightArrow, 0.25f));
      else
        this.m_rightArrow.gameObject.SetActive(false);
      iTween.MoveTo(this.m_randomDeckPickerTray, iTween.Hash((object) "time", (object) 0.25f, (object) "position", (object) this.m_randomDecksShownBone.transform.localPosition, (object) "isLocal", (object) true, (object) "oncomplete", (object) "RaiseHeroButtons", (object) "oncompletetarget", (object) this.gameObject));
    }
  }

  private void SuckInPreconDecks()
  {
    iTween.MoveTo(this.m_randomDeckPickerTray, iTween.Hash((object) "time", (object) 0.25f, (object) "position", (object) this.m_suckedInRandomDecksBone.transform.localPosition, (object) "isLocal", (object) true, (object) "oncomplete", (object) "SuckInFinished", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void OnCustomDeckPressed(CollectionDeckBoxVisual deckbox, bool showTrayForPhone = true)
  {
    this.SelectCustomDeck(deckbox, showTrayForPhone);
    this.ShowBasicDeckWarning(deckbox);
  }

  private void SelectCustomDeck(CollectionDeckBoxVisual deckbox, bool showTrayForPhone = true)
  {
    this.HideDemoQuotes();
    if (!deckbox.IsValid())
      return;
    Options.Get().SetLong(Option.LAST_CUSTOM_DECK_CHOSEN, deckbox.GetDeckID());
    if ((bool) DeckPickerTrayDisplay.HighlightSelectedDeck)
      deckbox.SetHighlightState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    if (!(bool) UniversalInputManager.UsePhoneUI)
      deckbox.SetEnabled(false);
    if ((UnityEngine.Object) this.m_selectedCustomDeckBox != (UnityEngine.Object) null && (UnityEngine.Object) this.m_selectedCustomDeckBox != (UnityEngine.Object) deckbox)
    {
      this.m_selectedCustomDeckBox.SetHighlightState(ActorStateType.HIGHLIGHT_OFF);
      this.m_selectedCustomDeckBox.SetEnabled(true);
    }
    this.m_selectedCustomDeckBox = deckbox;
    this.UpdateHeroInfo(deckbox);
    this.ShowPreconHero(true);
    this.EnablePlayButton(true);
    this.UpdateHeroLockedTooltip(false);
    if (!(bool) UniversalInputManager.UsePhoneUI || !showTrayForPhone)
      return;
    this.m_slidingTray.ToggleTraySlider(true, (Transform) null, true);
  }

  private void ShowBasicDeckWarning(CollectionDeckBoxVisual deckbox)
  {
    if ((UnityEngine.Object) this.m_selectedCustomDeckBox != (UnityEngine.Object) deckbox || SceneMgr.Get().GetMode() != SceneMgr.Mode.TOURNAMENT || (Options.Get().GetBool(Option.HAS_SEEN_BASIC_DECK_WARNING) || !UserAttentionManager.CanShowAttentionGrabber(UserAttentionBlocker.SET_ROTATION_INTRO, "DeckPickerTrayDisplay.SelectDeck")) || (!CollectionManager.Get().ShouldAccountSeeStandardWild() || deckbox.GetCollectionDeck() == null || !deckbox.GetCollectionDeck().IsBasicDeck()))
      return;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_BASIC_DECK_WARNING_HEADER"),
      m_text = GameStrings.Get("GLUE_BASIC_DECK_WARNING_BODY"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
    Options.Get().SetBool(Option.HAS_SEEN_BASIC_DECK_WARNING, true);
  }

  private void HeroPressed(UIEvent e)
  {
    this.SelectHero((HeroPickerButton) e.GetElement(), true);
    SoundManager.Get().LoadAndPlay("tournament_screen_select_hero");
    this.HideDemoQuotes();
  }

  private void SelectHero(HeroPickerButton button, bool showTrayForPhone = true)
  {
    if ((UnityEngine.Object) button == (UnityEngine.Object) this.m_selectedHeroButton && !(bool) UniversalInputManager.UsePhoneUI || !this.IsChoosingHero() && !button.IsDeckValid())
      return;
    this.DeselectLastSelectedHero();
    if ((bool) DeckPickerTrayDisplay.HighlightSelectedDeck)
      button.SetHighlightState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    else
      button.SetHighlightState(ActorStateType.HIGHLIGHT_OFF);
    this.m_selectedHeroButton = button;
    this.UpdateHeroInfo(button);
    button.SetSelected(true);
    if (!this.IsChoosingHero() && !button.IsLocked())
      Options.Get().SetInt(Option.LAST_PRECON_HERO_CHOSEN, (int) button.GetFullDef().GetEntityDef().GetClass());
    this.ShowPreconHero(true);
    if ((UnityEngine.Object) this.m_tooltip != (UnityEngine.Object) null)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_tooltip.gameObject);
    bool isLocked = button.IsLocked();
    this.EnablePlayButton(!isLocked);
    this.UpdateHeroLockedTooltip(isLocked);
    if (!(bool) UniversalInputManager.UsePhoneUI || !showTrayForPhone)
      return;
    this.m_slidingTray.ToggleTraySlider(true, (Transform) null, true);
  }

  private void UpdateHeroLockedTooltip(bool isLocked)
  {
    if ((UnityEngine.Object) this.m_tooltip != (UnityEngine.Object) null)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_tooltip.gameObject);
    if (!isLocked)
      return;
    GameObject go = UnityEngine.Object.Instantiate<GameObject>(this.m_tooltipPrefab);
    SceneUtils.SetLayer(go, GameLayer.Default);
    this.m_tooltip = go.GetComponent<TooltipPanel>();
    this.m_tooltip.Reset();
    this.m_tooltip.Initialize(GameStrings.Get("GLUE_HERO_LOCKED_NAME"), GameStrings.Get("GLUE_HERO_LOCKED_DESC"));
    GameUtils.SetParent((Component) this.m_tooltip, (Component) this.m_tooltipBone, false);
  }

  private void DeselectLastSelectedHero()
  {
    if ((UnityEngine.Object) this.m_selectedHeroButton == (UnityEngine.Object) null)
      return;
    this.m_selectedHeroButton.SetHighlightState(ActorStateType.HIGHLIGHT_OFF);
    this.m_selectedHeroButton.SetSelected(false);
  }

  private void Deselect()
  {
    if ((UnityEngine.Object) this.m_selectedHeroButton == (UnityEngine.Object) null && (UnityEngine.Object) this.m_selectedCustomDeckBox == (UnityEngine.Object) null)
      return;
    this.EnablePlayButton(false);
    if ((UnityEngine.Object) this.m_selectedCustomDeckBox != (UnityEngine.Object) null)
    {
      this.m_selectedCustomDeckBox.SetHighlightState(ActorStateType.HIGHLIGHT_OFF);
      this.m_selectedCustomDeckBox.SetEnabled(true);
      this.m_selectedCustomDeckBox = (CollectionDeckBoxVisual) null;
    }
    this.m_heroActor.SetEntityDef((EntityDef) null);
    this.m_heroActor.SetCardDef((CardDef) null);
    this.m_heroActor.Hide();
    if ((UnityEngine.Object) this.m_selectedHeroButton != (UnityEngine.Object) null)
    {
      this.m_selectedHeroButton.SetHighlightState(ActorStateType.HIGHLIGHT_OFF);
      this.m_selectedHeroButton.SetSelected(false);
      this.m_selectedHeroButton = (HeroPickerButton) null;
    }
    if (this.ShouldShowHeroPower())
    {
      this.m_heroPowerActor.SetCardDef((CardDef) null);
      this.m_heroPowerActor.SetEntityDef((EntityDef) null);
      this.m_heroPowerActor.Hide();
      this.m_goldenHeroPowerActor.SetCardDef((CardDef) null);
      this.m_goldenHeroPowerActor.SetEntityDef((EntityDef) null);
      this.m_goldenHeroPowerActor.Hide();
      this.m_heroPower.GetComponent<Collider>().enabled = false;
      this.m_goldenHeroPower.GetComponent<Collider>().enabled = false;
      if ((UnityEngine.Object) this.m_heroPowerShadowQuad != (UnityEngine.Object) null)
        this.m_heroPowerShadowQuad.SetActive(false);
    }
    this.m_selectedHeroPowerFullDef = (FullDef) null;
    if ((UnityEngine.Object) this.m_heroPowerBigCard != (UnityEngine.Object) null)
    {
      iTween.Stop(this.m_heroPowerBigCard.gameObject);
      this.m_heroPowerBigCard.Hide();
    }
    if ((UnityEngine.Object) this.m_goldenHeroPowerBigCard != (UnityEngine.Object) null)
    {
      iTween.Stop(this.m_goldenHeroPowerBigCard.gameObject);
      this.m_goldenHeroPowerBigCard.Hide();
    }
    this.m_selectedHeroName = (string) null;
    this.m_heroName.Text = string.Empty;
  }

  private void MouseOverHero(UIEvent e)
  {
    if (e == null)
      return;
    ((HeroPickerButton) e.GetElement()).SetHighlightState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
    SoundManager.Get().LoadAndPlay("collection_manager_hero_mouse_over");
  }

  private void MouseOutHero(UIEvent e)
  {
    HeroPickerButton element = (HeroPickerButton) e.GetElement();
    if ((bool) UniversalInputManager.UsePhoneUI && element.IsSelected())
      return;
    element.SetHighlightState(ActorStateType.HIGHLIGHT_OFF);
  }

  private void UpdateHeroInfo(CollectionDeckBoxVisual deckBox)
  {
    FullDef fullDef = deckBox.GetFullDef();
    string text = deckBox.GetDeckNameText().Text;
    string str = deckBox.GetHeroCardID();
    EntityDef entityDef = DefLoader.Get().GetEntityDef(str);
    if (entityDef.GetCardSet() == TAG_CARD_SET.HERO_SKINS)
      str = CollectionManager.Get().GetVanillaHeroCardID(entityDef);
    this.UpdateHeroInfo(fullDef, text, CollectionManager.Get().GetBestCardPremium(str));
  }

  private void UpdateHeroInfo(HeroPickerButton button)
  {
    FullDef fullDef = button.GetFullDef();
    EntityDef entityDef = fullDef.GetEntityDef();
    string name = entityDef.GetName();
    string cardID = entityDef.GetCardId();
    if (entityDef.GetCardSet() == TAG_CARD_SET.HERO_SKINS)
      cardID = CollectionManager.Get().GetVanillaHeroCardID(entityDef);
    this.UpdateHeroInfo(fullDef, name, CollectionManager.Get().GetBestCardPremium(cardID));
  }

  private void UpdateHeroInfo(FullDef fullDef, string heroName, TAG_PREMIUM premium)
  {
    EntityDef entityDef = fullDef.GetEntityDef();
    CardDef cardDef = fullDef.GetCardDef();
    this.m_heroName.Text = heroName;
    this.m_selectedHeroName = entityDef.GetName();
    this.m_heroActor.SetPremium(premium);
    this.m_heroActor.SetEntityDef(entityDef);
    this.m_heroActor.SetCardDef(cardDef);
    this.m_heroActor.UpdateAllComponents();
    this.m_heroActor.SetUnlit();
    this.m_xpBar.m_heroLevel = GameUtils.GetHeroLevel(entityDef.GetClass());
    this.m_xpBar.UpdateDisplay();
    string powerCardIdFromHero = GameUtils.GetHeroPowerCardIdFromHero(entityDef.GetCardId());
    if (this.ShouldShowHeroPower() && !string.IsNullOrEmpty(powerCardIdFromHero))
      this.UpdateHeroPowerInfo(this.m_heroPowerDefs[powerCardIdFromHero], premium);
    this.UpdateRankedClassWinsPlate();
  }

  private void UpdateHeroPowerInfo(FullDef def, TAG_PREMIUM premium)
  {
    this.m_heroPowerActor.SetCardDef(def.GetCardDef());
    this.m_heroPowerActor.SetEntityDef(def.GetEntityDef());
    this.m_heroPowerActor.UpdateAllComponents();
    this.m_selectedHeroPowerFullDef = def;
    this.m_heroPowerActor.SetUnlit();
    this.m_heroPowerActor.GetCardDef().m_AlwaysRenderPremiumPortrait = false;
    this.m_heroPowerActor.UpdateMaterials();
    this.m_goldenHeroPowerActor.SetCardDef(def.GetCardDef());
    this.m_goldenHeroPowerActor.SetEntityDef(def.GetEntityDef());
    this.m_goldenHeroPowerActor.UpdateAllComponents();
    this.m_selectedHeroPowerFullDef = def;
    this.m_goldenHeroPowerActor.SetUnlit();
    if (premium == TAG_PREMIUM.GOLDEN)
    {
      this.m_heroPowerActor.Hide();
      this.m_goldenHeroPowerActor.Show();
      this.m_goldenHeroPower.GetComponent<Collider>().enabled = true;
    }
    else
    {
      this.m_goldenHeroPowerActor.Hide();
      this.m_heroPowerActor.Show();
      this.m_heroPower.GetComponent<Collider>().enabled = true;
      if (this.m_heroActor.GetEntityDef().GetCardSet() == TAG_CARD_SET.HERO_SKINS)
        this.StartCoroutine(this.UpdateHeroSkinHeroPower());
    }
    if (!((UnityEngine.Object) this.m_heroPowerShadowQuad != (UnityEngine.Object) null))
      return;
    this.m_heroPowerShadowQuad.SetActive(true);
  }

  private void MouseOverHeroPower(UIEvent e)
  {
    this.m_isMouseOverHeroPower = true;
    if (this.m_heroActor.GetPremium() == TAG_PREMIUM.GOLDEN)
    {
      if ((UnityEngine.Object) this.m_goldenHeroPowerBigCard == (UnityEngine.Object) null)
        AssetLoader.Get().LoadActor(ActorNames.GetNameWithPremiumType("History_HeroPower", TAG_PREMIUM.GOLDEN), new AssetLoader.GameObjectCallback(this.LoadGoldenHeroPowerCallback), (object) null, false);
      else
        this.ShowGoldenHeroPowerBigCard();
    }
    else if ((UnityEngine.Object) this.m_heroPowerBigCard == (UnityEngine.Object) null)
      AssetLoader.Get().LoadActor("History_HeroPower", new AssetLoader.GameObjectCallback(this.LoadHeroPowerCallback), (object) null, false);
    else
      this.ShowHeroPowerBigCard();
  }

  private void MouseOutHeroPower(UIEvent e)
  {
    this.m_isMouseOverHeroPower = false;
    if ((UnityEngine.Object) this.m_heroPowerBigCard != (UnityEngine.Object) null)
    {
      iTween.Stop(this.m_heroPowerBigCard.gameObject);
      this.m_heroPowerBigCard.Hide();
    }
    if (!((UnityEngine.Object) this.m_goldenHeroPowerBigCard != (UnityEngine.Object) null))
      return;
    iTween.Stop(this.m_goldenHeroPowerBigCard.gameObject);
    this.m_goldenHeroPowerBigCard.Hide();
  }

  private void LoadHeroPowerCallback(string actorName, GameObject actorObject, object callbackData)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.LoadHeroPowerCallback() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.LoadHeroPowerCallback() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        component.transform.parent = this.m_heroPower.transform;
        component.TurnOffCollider();
        SceneUtils.SetLayer(component.gameObject, this.m_heroPower.gameObject.layer);
        UberText powersText = component.GetPowersText();
        if ((UnityEngine.Object) powersText != (UnityEngine.Object) null)
          TransformUtil.SetLocalPosY(powersText.gameObject, powersText.transform.localPosition.y + 0.1f);
        this.m_heroPowerBigCard = component;
        if (!this.m_isMouseOverHeroPower)
          return;
        this.ShowHeroPowerBigCard();
      }
    }
  }

  private void LoadGoldenHeroPowerCallback(string actorName, GameObject actorObject, object callbackData)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.LoadHeroPowerCallback() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.LoadHeroPowerCallback() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        component.transform.parent = this.m_heroPower.transform;
        component.TurnOffCollider();
        SceneUtils.SetLayer(component.gameObject, this.m_heroPower.gameObject.layer);
        UberText powersText = component.GetPowersText();
        if ((UnityEngine.Object) powersText != (UnityEngine.Object) null)
          TransformUtil.SetLocalPosY(powersText.gameObject, powersText.transform.localPosition.y + 0.1f);
        this.m_goldenHeroPowerBigCard = component;
        if (!this.m_isMouseOverHeroPower)
          return;
        this.ShowGoldenHeroPowerBigCard();
      }
    }
  }

  private void ShowHeroPowerBigCard()
  {
    FullDef heroPowerFullDef = this.m_selectedHeroPowerFullDef;
    if (heroPowerFullDef == null)
      return;
    CardDef cardDef = heroPowerFullDef.GetCardDef();
    if ((UnityEngine.Object) cardDef == (UnityEngine.Object) null)
      return;
    this.m_heroPowerBigCard.SetCardDef(cardDef);
    this.m_heroPowerBigCard.SetEntityDef(heroPowerFullDef.GetEntityDef());
    this.m_heroPowerBigCard.UpdateAllComponents();
    this.m_heroPowerBigCard.Show();
    if ((UnityEngine.Object) this.m_goldenHeroPowerBigCard != (UnityEngine.Object) null)
      this.m_goldenHeroPowerBigCard.Hide();
    this.UpdateCustomHeroPowerBigCard(this.m_heroPowerBigCard.gameObject);
    float num1 = 1f;
    float num2 = 1.5f;
    Vector3 vector3_1 = !UniversalInputManager.Get().IsTouchMode() ? new Vector3(0.019f, 0.54f, -1.12f) : new Vector3(0.019f, 0.54f, 3f);
    GameObject gameObject = this.m_heroPowerBigCard.gameObject;
    Vector3 vector3_2 = !UniversalInputManager.Get().IsTouchMode() || (bool) UniversalInputManager.UsePhoneUI ? new Vector3(0.1f, 0.1f, 0.1f) : new Vector3(0.0f, 0.1f, 0.1f);
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      gameObject.transform.localPosition = new Vector3(-11.4f, 0.6f, -0.14f);
      gameObject.transform.localScale = Vector3.one * 3.2f;
      Vector3 worldScale = TransformUtil.ComputeWorldScale((Component) gameObject.transform.parent);
      Vector3 driftOffset = Vector3.Scale(vector3_2 * 2f, worldScale);
      AnimationUtil.GrowThenDrift(gameObject, this.m_HeroPower_Bone.transform.position, driftOffset);
    }
    else
    {
      gameObject.transform.localPosition = vector3_1;
      gameObject.transform.localScale = Vector3.one * num1;
      iTween.ScaleTo(gameObject, Vector3.one * num2, 0.15f);
      iTween.MoveTo(gameObject, iTween.Hash((object) "position", (object) (vector3_1 + vector3_2), (object) "isLocal", (object) true, (object) "time", (object) 10));
    }
  }

  private void UpdateCustomHeroPowerBigCard(GameObject heroPowerBigCard)
  {
    if ((UnityEngine.Object) this.m_heroActor.GetCardDef() == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "DeckPickerTrayDisplay.UpdateCustomHeroPowerBigCard heroCardDef = null!");
    }
    else
    {
      Actor componentInChildren = heroPowerBigCard.GetComponentInChildren<Actor>();
      componentInChildren.GetCardDef().m_AlwaysRenderPremiumPortrait = this.m_heroActor.GetEntityDef().GetCardSet() == TAG_CARD_SET.HERO_SKINS;
      componentInChildren.UpdateMaterials();
    }
  }

  private void ShowGoldenHeroPowerBigCard()
  {
    FullDef heroPowerFullDef = this.m_selectedHeroPowerFullDef;
    if (heroPowerFullDef == null)
      return;
    CardDef cardDef = heroPowerFullDef.GetCardDef();
    if ((UnityEngine.Object) cardDef == (UnityEngine.Object) null)
      return;
    this.m_goldenHeroPowerBigCard.SetCardDef(cardDef);
    this.m_goldenHeroPowerBigCard.SetEntityDef(heroPowerFullDef.GetEntityDef());
    this.m_goldenHeroPowerBigCard.SetPremium(TAG_PREMIUM.GOLDEN);
    this.m_goldenHeroPowerBigCard.UpdateAllComponents();
    this.m_goldenHeroPowerBigCard.Show();
    if ((UnityEngine.Object) this.m_heroPowerBigCard != (UnityEngine.Object) null)
      this.m_heroPowerBigCard.Hide();
    float num1 = 1f;
    float num2 = 1.5f;
    Vector3 vector3_1 = !UniversalInputManager.Get().IsTouchMode() ? new Vector3(0.019f, 0.54f, -1.12f) : new Vector3(0.019f, 0.54f, 3f);
    GameObject gameObject = this.m_goldenHeroPowerBigCard.gameObject;
    gameObject.transform.localPosition = vector3_1;
    this.m_goldenHeroPowerBigCard.transform.localScale = new Vector3(num1, num1, num1);
    iTween.ScaleTo(gameObject, new Vector3(num2, num2, num2), 0.15f);
    Vector3 vector3_2 = !UniversalInputManager.Get().IsTouchMode() ? new Vector3(0.1f, 0.1f, 0.1f) : new Vector3(0.0f, 0.1f, 0.1f);
    iTween.MoveTo(gameObject, iTween.Hash((object) "position", (object) (vector3_1 + vector3_2), (object) "isLocal", (object) true, (object) "time", (object) 10));
  }

  private void UpdateTrayTransitionValues(bool isWild, bool showVineGlowBurst = true)
  {
    this.m_isUsingWildVisuals = isWild;
    float targetValue = !isWild ? 0.0f : 1f;
    if ((bool) UniversalInputManager.UsePhoneUI && this.m_slidingTray.IsShown())
      this.m_detailsTrayFrame.GetComponentInChildren<Renderer>().material.SetFloat("_Transistion", targetValue);
    if (showVineGlowBurst)
    {
      if (this.m_customPages != null)
      {
        bool hasValidStandardDeck = this.GetNumValidStandardDecks() > 0U;
        if (this.m_customPages.Length > 1 && this.m_showingSecondPage)
          this.m_customPages[1].PlayVineGlowBurst(!isWild, hasValidStandardDeck);
        else if (this.m_customPages.Length > 0)
        {
          if (hasValidStandardDeck)
          {
            foreach (GameObject gameObject in this.m_customPages[0].m_customVineGlowToggle)
              gameObject.SetActive(true);
          }
          this.m_customPages[0].PlayVineGlowBurst(!isWild, hasValidStandardDeck);
        }
      }
      if ((UnityEngine.Object) this.m_vineGlowBurst != (UnityEngine.Object) null)
      {
        bool flag = !this.ShouldShowCustomDecks();
        using (List<GameObject>.Enumerator enumerator = this.m_premadeDeckGlowBurstObjects.GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.SetActive(flag);
        }
        this.m_vineGlowBurst.SendEvent(!isWild ? "GlowVines_Premade" : "GlowVines_PremadeNoFX");
      }
      string path = !isWild ? this.m_standardTransitionSound : this.m_wildTransitionSound;
      if (!string.IsNullOrEmpty(path))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(path));
    }
    this.StopCoroutine("TransitionTrayMaterial");
    this.StartCoroutine(this.TransitionTrayMaterial(targetValue, 2f));
  }

  [DebuggerHidden]
  private IEnumerator TransitionTrayMaterial(float targetValue, float speed)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CTransitionTrayMaterial\u003Ec__Iterator73() { targetValue = targetValue, speed = speed, \u003C\u0024\u003EtargetValue = targetValue, \u003C\u0024\u003Espeed = speed, \u003C\u003Ef__this = this };
  }

  private void SetTrayTextures(Texture standardTexture, Texture wildTexture)
  {
    Material material = this.m_trayFrame.GetComponentInChildren<Renderer>().material;
    material.mainTexture = standardTexture;
    material.SetTexture("_MainTex2", wildTexture);
    Renderer componentInChildren = this.m_randomTray.GetComponentInChildren<Renderer>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    componentInChildren.material.mainTexture = standardTexture;
    componentInChildren.material.SetTexture("_MainTex2", wildTexture);
  }

  private void SetCustomTrayTextures(Texture standardTexture, Texture wildTexture)
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      Material material = this.m_randomTray.GetComponent<Renderer>().material;
      material.mainTexture = standardTexture;
      material.SetTexture("_MainTex2", wildTexture);
    }
    if (this.m_customPages == null)
      return;
    foreach (CustomDeckPage customPage in this.m_customPages)
      customPage.SetTrayTextures(standardTexture, wildTexture);
  }

  private void SetDetailsTrayTextures(Texture standardTexture, Texture wildTexture)
  {
    Material sharedMaterial = this.m_detailsTrayFrame.GetComponent<Renderer>().sharedMaterial;
    sharedMaterial.mainTexture = standardTexture;
    sharedMaterial.SetTexture("_MainTex2", wildTexture);
  }

  private void DisableHeroButtons()
  {
    using (List<HeroPickerButton>.Enumerator enumerator = this.m_heroButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetEnabled(false);
    }
  }

  private void EnableHeroButtons()
  {
    using (List<HeroPickerButton>.Enumerator enumerator = this.m_heroButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetEnabled(true);
    }
  }

  private HeroPickerButton GetPreconButtonForClass(TAG_CLASS classType)
  {
    using (List<HeroPickerButton>.Enumerator enumerator = this.m_heroButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroPickerButton current = enumerator.Current;
        if (current.GetFullDef().GetEntityDef().GetClass() == classType)
          return current;
      }
    }
    return (HeroPickerButton) null;
  }

  private void RankedPlayButtonsLoaded(string name, GameObject go, object callbackData)
  {
    this.m_rankedPlayButtons = go.GetComponent<RankedPlayDisplay>();
    this.m_rankedPlayButtons.transform.parent = this.m_hierarchyDetails.transform;
    this.m_rankedPlayButtons.transform.localScale = this.m_rankedPlayButtonsBone.localScale;
    this.m_rankedPlayButtons.transform.localPosition = this.m_rankedPlayButtonsBone.localPosition;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_rankedPlayButtons.SetRankedMedalTransform(this.m_medalBone_phone);
    this.m_rankedPlayButtons.UpdateMode();
    this.StartCoroutine(this.SetRankedMedalWhenReady());
  }

  [DebuggerHidden]
  private IEnumerator SetRankedMedalWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CSetRankedMedalWhenReady\u003Ec__Iterator74() { \u003C\u003Ef__this = this };
  }

  private void OnMedalChanged(NetCache.NetCacheMedalInfo medalInfo)
  {
    this.m_rankedPlayButtons.SetRankedMedal(medalInfo);
  }

  private void PlayGameButtonRelease(UIEvent e)
  {
    this.HideDemoQuotes();
    this.HideSetRotationNotifications();
    this.m_playButton.SetEnabled(false);
    this.DisableHeroButtons();
    this.m_heroChosen = true;
    this.PlayGame();
  }

  private void EnableBackButton(bool enable)
  {
    if (DemoMgr.Get().IsExpoDemo())
    {
      if (enable)
        return;
      enable = false;
    }
    this.m_backButton.SetEnabled(enable);
    this.m_backButton.Flip(enable);
  }

  private void EnablePlayButton(bool enable)
  {
    if (enable)
    {
      if (SceneMgr.Get().GetMode() == SceneMgr.Mode.FRIENDLY && !FriendChallengeMgr.Get().HasChallenge())
        return;
      this.m_playButton.Enable();
    }
    else
      this.m_playButton.Disable();
  }

  private void EnableCollectionButton(bool enable)
  {
    this.m_collectionButton.SetEnabled(enable);
    this.m_collectionButton.Flip(enable);
    this.UpdateCollectionButtonGlow();
  }

  private void UpdateCollectionButtonGlow()
  {
    if (this.ShouldGlowCollectionButton())
      this.m_collectionButtonGlow.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    else
      this.m_collectionButtonGlow.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }

  private void ShowHero()
  {
    if (this.IsShowingCustomDecks())
      this.UpdateHeroInfo(this.m_selectedCustomDeckBox);
    else
      this.UpdateHeroInfo(this.m_selectedHeroButton);
    this.m_heroActor.Show();
    if (this.ShouldShowHeroPower())
    {
      if (this.m_heroActor.GetPremium() == TAG_PREMIUM.GOLDEN)
      {
        this.m_heroPowerActor.Hide();
        this.m_goldenHeroPowerActor.Show();
        this.m_goldenHeroPower.GetComponent<Collider>().enabled = true;
      }
      else
      {
        this.m_goldenHeroPowerActor.Hide();
        this.m_heroPowerActor.Show();
        this.m_heroPower.GetComponent<Collider>().enabled = true;
      }
    }
    if (this.m_selectedHeroName != null)
      return;
    this.m_heroName.Text = string.Empty;
  }

  private void ShowPreconHero(bool show)
  {
    if (show && SceneMgr.Get().GetMode() == SceneMgr.Mode.ADVENTURE && (AdventureConfig.Get().GetCurrentSubScene() == AdventureSubScenes.Practice && (UnityEngine.Object) PracticePickerTrayDisplay.Get() != (UnityEngine.Object) null) && PracticePickerTrayDisplay.Get().IsShown())
      return;
    if (show)
    {
      this.ShowHero();
    }
    else
    {
      if ((bool) ((UnityEngine.Object) this.m_heroActor))
        this.m_heroActor.Hide();
      if ((bool) ((UnityEngine.Object) this.m_heroPowerActor))
        this.m_heroPowerActor.Hide();
      if ((bool) ((UnityEngine.Object) this.m_goldenHeroPowerActor))
        this.m_goldenHeroPowerActor.Hide();
      if ((bool) ((UnityEngine.Object) this.m_heroPower))
        this.m_heroPower.GetComponent<Collider>().enabled = false;
      if ((bool) ((UnityEngine.Object) this.m_goldenHeroPower))
        this.m_goldenHeroPower.GetComponent<Collider>().enabled = false;
      this.m_heroName.Text = string.Empty;
    }
  }

  private void RaiseHero()
  {
    this.m_xpBar.SetEnabled(true);
    iTween.MoveTo(this.m_heroActor.gameObject, iTween.Hash((object) "position", (object) this.m_Hero_Bone.localPosition, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "islocal", (object) true));
    Hashtable args = iTween.Hash((object) "position", (object) this.m_HeroPower_Bone.localPosition, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "islocal", (object) true);
    if (!this.ShouldShowHeroPower())
      return;
    iTween.MoveTo(this.m_heroPowerActor.gameObject, args);
    this.m_heroPower.GetComponent<Collider>().enabled = true;
    if ((UnityEngine.Object) this.m_goldenHeroPowerActor == (UnityEngine.Object) null)
      return;
    iTween.MoveTo(this.m_goldenHeroPowerActor.gameObject, iTween.Hash((object) "position", (object) this.m_HeroPower_Bone.localPosition, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "islocal", (object) true));
    this.m_goldenHeroPower.GetComponent<Collider>().enabled = true;
  }

  private void LowerHero()
  {
    this.m_xpBar.SetEnabled(false);
    if (!this.m_heroActor.gameObject.activeInHierarchy)
      return;
    iTween.MoveTo(this.m_heroActor.gameObject, iTween.Hash((object) "position", (object) this.m_Hero_BoneDown.localPosition, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "islocal", (object) true));
    Hashtable args = iTween.Hash((object) "position", (object) this.m_HeroPower_BoneDown.localPosition, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "islocal", (object) true);
    if (!this.ShouldShowHeroPower())
      return;
    iTween.MoveTo(this.m_heroPowerActor.gameObject, args);
    this.m_heroPower.GetComponent<Collider>().enabled = false;
    if ((UnityEngine.Object) this.m_goldenHeroPowerActor == (UnityEngine.Object) null)
      return;
    iTween.MoveTo(this.m_goldenHeroPowerActor.gameObject, iTween.Hash((object) "position", (object) this.m_HeroPower_BoneDown.localPosition, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutExpo, (object) "islocal", (object) true));
    this.m_goldenHeroPower.GetComponent<Collider>().enabled = false;
  }

  private void HideAllPreconHighlights()
  {
    using (List<HeroPickerButton>.Enumerator enumerator = this.m_heroButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetHighlightState(ActorStateType.HIGHLIGHT_OFF);
    }
  }

  private void ShowPreconHighlights()
  {
    if (!(bool) DeckPickerTrayDisplay.HighlightSelectedDeck)
      return;
    using (List<HeroPickerButton>.Enumerator enumerator = this.m_heroButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroPickerButton current = enumerator.Current;
        if ((UnityEngine.Object) current == (UnityEngine.Object) this.m_selectedHeroButton)
          current.SetHighlightState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
      }
    }
  }

  private void PlayGame()
  {
    if ((bool) UniversalInputManager.UsePhoneUI && (SceneMgr.Get().GetMode() != SceneMgr.Mode.ADVENTURE || AdventureConfig.Get().GetCurrentSubScene() != AdventureSubScenes.Practice))
      this.m_slidingTray.ToggleTraySlider(false, (Transform) null, true);
    FormatType formatType = FormatType.FT_WILD;
    switch (SceneMgr.Get().GetMode())
    {
      case SceneMgr.Mode.COLLECTIONMANAGER:
        this.SelectHeroForCollectionManager();
        break;
      case SceneMgr.Mode.TOURNAMENT:
        long selectedDeckId1 = this.GetSelectedDeckID();
        if (selectedDeckId1 == 0L)
        {
          UnityEngine.Debug.LogError((object) "Trying to play game with deck ID 0!");
          break;
        }
        this.EnableBackButton(false);
        GameType gameType = !Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE) ? GameType.GT_CASUAL : GameType.GT_RANKED;
        bool useWildRank = Options.Get().GetBool(Option.IN_WILD_MODE);
        if (!useWildRank)
          formatType = FormatType.FT_STANDARD;
        GameMgr.Get().FindGame(gameType, formatType, 2, selectedDeckId1, 0L);
        bool flag = true;
        if (gameType == GameType.GT_RANKED && RankMgr.Get().AmILegendRank(useWildRank))
          flag = false;
        if (!flag)
          break;
        PresenceMgr.Get().SetStatus((Enum) PresenceStatus.PLAY_QUEUE);
        break;
      case SceneMgr.Mode.FRIENDLY:
        long selectedDeckId2 = this.GetSelectedDeckID();
        if (selectedDeckId2 == 0L)
        {
          UnityEngine.Debug.LogError((object) "Trying to play friendly game with deck ID 0!");
          break;
        }
        FriendChallengeMgr.Get().SelectDeck(selectedDeckId2);
        FriendlyChallengeHelper.Get().StartChallengeOrWaitForOpponent("GLOBAL_FRIEND_CHALLENGE_OPPONENT_WAITING_DECK", new AlertPopup.ResponseCallback(this.OnFriendChallengeWaitingForOpponentDialogResponse));
        break;
      case SceneMgr.Mode.ADVENTURE:
        long selectedDeckId3 = this.GetSelectedDeckID();
        AdventureConfig adventureConfig = AdventureConfig.Get();
        if (adventureConfig.GetSelectedAdventure() == AdventureDbId.NAXXRAMAS && !Options.Get().GetBool(Option.HAS_PLAYED_NAXX))
        {
          AdTrackingManager.Get().TrackAdventureProgress(Option.HAS_PLAYED_NAXX.ToString());
          Options.Get().SetBool(Option.HAS_PLAYED_NAXX, true);
        }
        switch (adventureConfig.GetCurrentSubScene())
        {
          case AdventureSubScenes.Practice:
            PracticePickerTrayDisplay.Get().Show();
            this.LowerHero();
            return;
          case AdventureSubScenes.MissionDeckPicker:
            if (DemoMgr.Get().GetMode() != DemoMode.BLIZZCON_2015)
              adventureConfig.ChangeToLastSubScene(false);
            GameMgr.Get().FindGame(GameType.GT_VS_AI, formatType, (int) adventureConfig.GetMission(), selectedDeckId3, 0L);
            return;
          default:
            return;
        }
      case SceneMgr.Mode.TAVERN_BRAWL:
        if (TavernBrawlManager.Get().SelectHeroBeforeMission())
        {
          long selectedDeckId4 = this.GetSelectedDeckID();
          if (selectedDeckId4 == 0L)
          {
            UnityEngine.Debug.LogError((object) "Trying to play Tavern Brawl game with deck ID 0!");
            break;
          }
          if (FriendChallengeMgr.Get().IsChallengeTavernBrawl())
          {
            FriendChallengeMgr.Get().SelectDeck(selectedDeckId4);
            FriendlyChallengeHelper.Get().StartChallengeOrWaitForOpponent("GLOBAL_FRIEND_CHALLENGE_TAVERN_BRAWL_OPPONENT_WAITING_READY", new AlertPopup.ResponseCallback(this.OnFriendChallengeWaitingForOpponentDialogResponse));
            break;
          }
          TavernBrawlManager.Get().StartGame(selectedDeckId4);
          break;
        }
        this.SelectHeroForCollectionManager();
        break;
    }
  }

  private void SelectHeroForCollectionManager()
  {
    this.m_backButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.BackButtonPress));
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(DeckPickerTrayDisplay.OnNavigateBack));
    if (DeckPickerTrayDisplay.s_selectHeroCoroutine != null)
      SceneMgr.Get().StopCoroutine(DeckPickerTrayDisplay.s_selectHeroCoroutine);
    DeckPickerTrayDisplay.s_selectHeroCoroutine = SceneMgr.Get().StartCoroutine(DeckPickerTrayDisplay.SelectHeroForCollectionManagerImpl(this.m_selectedHeroButton.GetFullDef()));
  }

  [DebuggerHidden]
  private static IEnumerator SelectHeroForCollectionManagerImpl(FullDef heroDef)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CSelectHeroForCollectionManagerImpl\u003Ec__Iterator75() { heroDef = heroDef, \u003C\u0024\u003EheroDef = heroDef };
  }

  private void OnSlidingTrayToggled(bool isShowing)
  {
    if (!isShowing && (UnityEngine.Object) PracticePickerTrayDisplay.Get() != (UnityEngine.Object) null && PracticePickerTrayDisplay.Get().IsShown())
    {
      Navigation.GoBack();
    }
    else
    {
      if (!isShowing)
        return;
      this.UpdateTrayTransitionValues(this.m_isUsingWildVisuals, false);
    }
  }

  [DebuggerHidden]
  private IEnumerator InitModeWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CInitModeWhenReady\u003Ec__Iterator76() { \u003C\u003Ef__this = this };
  }

  private bool CustomPagesReady()
  {
    foreach (CustomDeckPage customPage in this.m_customPages)
    {
      if (!customPage.PageReady())
        return false;
    }
    return true;
  }

  private void InitRichPresence()
  {
    PresenceStatus? nullable = new PresenceStatus?();
    switch (SceneMgr.Get().GetMode())
    {
      case SceneMgr.Mode.TOURNAMENT:
        nullable = new PresenceStatus?(PresenceStatus.PLAY_DECKPICKER);
        break;
      case SceneMgr.Mode.FRIENDLY:
        nullable = new PresenceStatus?(PresenceStatus.FRIENDLY_DECKPICKER);
        if (FriendChallengeMgr.Get().IsChallengeTavernBrawl())
        {
          nullable = new PresenceStatus?(PresenceStatus.TAVERN_BRAWL_FRIENDLY_WAITING);
          break;
        }
        break;
      case SceneMgr.Mode.ADVENTURE:
        if (AdventureConfig.Get().GetCurrentSubScene() == AdventureSubScenes.Practice)
        {
          nullable = new PresenceStatus?(PresenceStatus.PRACTICE_DECKPICKER);
          break;
        }
        break;
      case SceneMgr.Mode.TAVERN_BRAWL:
        if (FriendChallengeMgr.Get().IsChallengeTavernBrawl())
        {
          nullable = new PresenceStatus?(PresenceStatus.TAVERN_BRAWL_FRIENDLY_WAITING);
          break;
        }
        break;
    }
    if (!nullable.HasValue)
      return;
    PresenceMgr.Get().SetStatus((Enum) nullable.Value);
  }

  private void SetSelectionAndPageFromOptions()
  {
    bool flag = (bool) UniversalInputManager.UsePhoneUI && SceneMgr.Get().GetPrevMode() != SceneMgr.Mode.GAMEPLAY;
    if (this.ShouldShowCustomDecks())
    {
      int pageNum = 0;
      long id = Options.Get().GetLong(Option.LAST_CUSTOM_DECK_CHOSEN);
      if (Options.Get().GetBool(Option.SHOW_INNKEEPER_DECK_DIALOGUE))
      {
        using (List<CollectionDeck>.Enumerator enumerator = CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK).GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            CollectionDeck current = enumerator.Current;
            if (current.IsInnkeeperDeck())
            {
              id = current.ID;
              break;
            }
          }
        }
      }
      CollectionDeckBoxVisual deckboxWithDeckId = this.GetDeckboxWithDeckID(id, out pageNum);
      if (this.m_needUnlockAllHeroesTransition || pageNum <= 0)
        this.ShowFirstPage();
      else
        this.ShowSecondPage();
      if (!((UnityEngine.Object) deckboxWithDeckId != (UnityEngine.Object) null) || flag)
        return;
      this.SelectCustomDeck(deckboxWithDeckId, true);
    }
    else
    {
      this.ShowFirstPage();
      HeroPickerButton preconButtonForClass = this.GetPreconButtonForClass((TAG_CLASS) Options.Get().GetInt(Option.LAST_PRECON_HERO_CHOSEN));
      if (!((UnityEngine.Object) preconButtonForClass != (UnityEngine.Object) null) || flag)
        return;
      this.SelectHero(preconButtonForClass, true);
    }
  }

  private CollectionDeckBoxVisual GetDeckboxWithDeckID(long deckID, out int pageNum)
  {
    pageNum = 0;
    while (pageNum < this.m_customPages.Length)
    {
      CollectionDeckBoxVisual deckboxWithDeckId = this.m_customPages[pageNum].GetDeckboxWithDeckID(deckID);
      if ((UnityEngine.Object) deckboxWithDeckId != (UnityEngine.Object) null)
        return deckboxWithDeckId;
      pageNum = pageNum + 1;
    }
    pageNum = 0;
    return (CollectionDeckBoxVisual) null;
  }

  private void OnFriendChallengeWaitingForOpponentDialogResponse(AlertPopup.Response response, object userData)
  {
    if (response != AlertPopup.Response.CANCEL || FriendChallengeMgr.Get().AmIInGameState())
      return;
    this.EnableHeroButtons();
    this.Deselect();
    FriendChallengeMgr.Get().DeselectDeck();
    FriendlyChallengeHelper.Get().StopWaitingForFriendChallenge();
  }

  private void OnFriendChallengeChanged(FriendChallengeEvent challengeEvent, BnetPlayer player, object userData)
  {
    if (challengeEvent == FriendChallengeEvent.SELECTED_DECK)
    {
      if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL || player == BnetPresenceMgr.Get().GetMyPlayer() || !FriendChallengeMgr.Get().DidISelectDeck())
        return;
      FriendlyChallengeHelper.Get().HideFriendChallengeWaitingForOpponentDialog();
    }
    else if (challengeEvent == FriendChallengeEvent.OPPONENT_CANCELED_CHALLENGE || challengeEvent == FriendChallengeEvent.OPPONENT_REMOVED_FROM_FRIENDS)
    {
      FriendlyChallengeHelper.Get().StopWaitingForFriendChallenge();
      this.GoBackUntilOnNavigateBackCalled();
    }
    else
    {
      if (challengeEvent != FriendChallengeEvent.DESELECTED_DECK || player == BnetPresenceMgr.Get().GetMyPlayer())
        return;
      if (FriendChallengeMgr.Get().DidISelectDeck())
      {
        FriendlyChallengeHelper.Get().StartChallengeOrWaitForOpponent("GLOBAL_FRIEND_CHALLENGE_OPPONENT_WAITING_DECK", new AlertPopup.ResponseCallback(this.OnFriendChallengeWaitingForOpponentDialogResponse));
      }
      else
      {
        this.EnablePlayButton(true);
        this.EnableBackButton(true);
        this.EnableHeroButtons();
      }
    }
  }

  private void GoBackUntilOnNavigateBackCalled()
  {
    do
      ;
    while (Navigation.BackStackContainsHandler(new Navigation.NavigateBackHandler(DeckPickerTrayDisplay.OnNavigateBack)) && Navigation.GoBack());
  }

  private void OnHeroFullDefLoaded(string cardId, FullDef fullDef, object userData)
  {
    EntityDef entityDef = fullDef.GetEntityDef();
    DeckPickerTrayDisplay.HeroFullDefLoadedCallbackData loadedCallbackData = userData as DeckPickerTrayDisplay.HeroFullDefLoadedCallbackData;
    TAG_PREMIUM premium = fullDef.GetEntityDef().GetCardSet() != TAG_CARD_SET.HERO_SKINS ? CollectionManager.Get().GetBestCardPremium(cardId) : TAG_PREMIUM.GOLDEN;
    loadedCallbackData.HeroPickerButton.UpdateDisplay(fullDef, premium);
    loadedCallbackData.HeroPickerButton.SetOriginalLocalPosition();
    string powerCardIdFromHero = GameUtils.GetHeroPowerCardIdFromHero(entityDef.GetCardId());
    this.m_heroPowerDefs[powerCardIdFromHero] = DefLoader.Get().GetFullDef(powerCardIdFromHero, (CardPortraitQuality) null);
    --this.m_heroDefsLoading;
    if (this.m_heroDefsLoading > 0 || !this.ShouldShowPreconDecks())
      return;
    this.StartCoroutine(this.InitButtonAchievements());
  }

  private void LoadHero()
  {
    AssetLoader.Get().LoadActor("Card_Play_Hero", new AssetLoader.GameObjectCallback(this.OnHeroActorLoaded), (object) null, false);
  }

  private void OnHeroActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.OnHeroActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      this.m_heroActor = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) this.m_heroActor == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.OnHeroActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        actorObject.transform.parent = this.m_hierarchyDetails.transform;
        actorObject.transform.localScale = this.m_Hero_Bone.localScale;
        actorObject.transform.localPosition = this.m_Hero_Bone.localPosition;
        this.m_heroActor.SetUnlit();
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_heroActor.m_healthObject);
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_heroActor.m_attackObject);
        this.m_xpBar.transform.parent = this.m_heroActor.GetRootObject().transform;
        this.m_xpBar.transform.localScale = new Vector3(0.89f, 0.89f, 0.89f);
        this.m_xpBar.transform.localPosition = new Vector3(-0.1776525f, 0.2245596f, -0.7309282f);
        this.m_xpBar.m_isOnDeck = false;
        this.m_heroActor.Hide();
      }
    }
  }

  private void LoadHeroPower()
  {
    AssetLoader.Get().LoadActor("Card_Play_HeroPower", new AssetLoader.GameObjectCallback(this.OnHeroPowerActorLoaded), (object) null, false);
  }

  private void OnHeroPowerActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.OnHeroPowerActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      this.m_heroPowerActor = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) this.m_heroPowerActor == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.OnHeroPowerActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        this.m_heroPower = actorObject.AddComponent<PegUIElement>();
        actorObject.AddComponent<BoxCollider>();
        actorObject.transform.parent = this.m_hierarchyDetails.transform;
        actorObject.transform.localScale = this.m_HeroPower_Bone.localScale;
        actorObject.transform.localPosition = this.m_HeroPower_Bone.localPosition;
        this.m_heroPowerActor.SetUnlit();
        this.m_heroPower.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.MouseOverHeroPower));
        this.m_heroPower.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.MouseOutHeroPower));
        this.m_heroPowerActor.Hide();
        this.m_heroPower.GetComponent<Collider>().enabled = false;
        this.m_heroName.Text = string.Empty;
        this.StartCoroutine(this.UpdateHeroSkinHeroPower());
      }
    }
  }

  private void LoadGoldenHeroPower()
  {
    AssetLoader.Get().LoadActor(ActorNames.GetNameWithPremiumType("Card_Play_HeroPower", TAG_PREMIUM.GOLDEN), new AssetLoader.GameObjectCallback(this.OnGoldenHeroPowerActorLoaded), (object) null, false);
  }

  private void OnGoldenHeroPowerActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.OnHeroPowerActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      this.m_goldenHeroPowerActor = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) this.m_goldenHeroPowerActor == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("DeckPickerTrayDisplay.OnHeroPowerActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        this.m_goldenHeroPower = actorObject.AddComponent<PegUIElement>();
        actorObject.AddComponent<BoxCollider>();
        actorObject.transform.parent = this.m_hierarchyDetails.transform;
        actorObject.transform.localScale = this.m_HeroPower_Bone.localScale;
        actorObject.transform.localPosition = this.m_HeroPower_Bone.localPosition;
        this.m_goldenHeroPowerActor.SetUnlit();
        this.m_goldenHeroPowerActor.SetPremium(TAG_PREMIUM.GOLDEN);
        this.m_goldenHeroPower.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.MouseOverHeroPower));
        this.m_goldenHeroPower.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.MouseOutHeroPower));
        this.m_goldenHeroPowerActor.Hide();
        this.m_goldenHeroPower.GetComponent<Collider>().enabled = false;
        this.m_heroName.Text = string.Empty;
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator UpdateHeroSkinHeroPower()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CUpdateHeroSkinHeroPower\u003Ec__Iterator77() { \u003C\u003Ef__this = this };
  }

  private void FireDeckTrayLoadedEvent()
  {
    foreach (DeckPickerTrayDisplay.DeckTrayLoaded deckTrayLoaded in this.m_DeckTrayLoadedListeners.ToArray())
      deckTrayLoaded();
  }

  private bool ShouldShowHeroPower()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      return this.IsChoosingHero();
    return true;
  }

  private bool IsChoosingHero()
  {
    switch (SceneMgr.Get().GetMode())
    {
      case SceneMgr.Mode.COLLECTIONMANAGER:
      case SceneMgr.Mode.TAVERN_BRAWL:
        return true;
      default:
        return this.IsChoosingHeroForTavernBrawlChallenge();
    }
  }

  private bool ShouldShowCollectionButton()
  {
    if (this.IsChoosingHero() || SceneMgr.Get().GetMode() == SceneMgr.Mode.FRIENDLY)
      return false;
    if (AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
      return true;
    if ((double) this.m_numCardsPerClass < 0.0)
    {
      CollectionManager collectionManager = CollectionManager.Get();
      TAG_CLASS[] tagClassArray = new TAG_CLASS[9]{ TAG_CLASS.MAGE, TAG_CLASS.DRUID, TAG_CLASS.HUNTER, TAG_CLASS.PALADIN, TAG_CLASS.PRIEST, TAG_CLASS.ROGUE, TAG_CLASS.SHAMAN, TAG_CLASS.WARLOCK, TAG_CLASS.WARRIOR };
      // ISSUE: variable of the null type
      __Null local1 = null;
      // ISSUE: variable of the null type
      __Null local2 = null;
      int? manaCost = new int?();
      // ISSUE: variable of the null type
      __Null local3 = null;
      TAG_CLASS[] theseClassTypes = tagClassArray;
      // ISSUE: variable of the null type
      __Null local4 = null;
      TAG_RARITY? rarity = new TAG_RARITY?();
      TAG_RACE? race = new TAG_RACE?();
      bool? isHero = new bool?();
      int? minOwned = new int?(1);
      bool? notSeen = new bool?();
      bool? isCraftable = new bool?();
      // ISSUE: variable of the null type
      __Null local5 = null;
      // ISSUE: variable of the null type
      __Null local6 = null;
      int num = 0;
      this.m_numCardsPerClass = (float) collectionManager.FindCards((string) local1, (List<CollectibleCardFilter.FilterMask>) local2, manaCost, (TAG_CARD_SET[]) local3, theseClassTypes, (TAG_CARDTYPE[]) local4, rarity, race, isHero, minOwned, notSeen, isCraftable, (CollectionManager.CollectibleCardFilterFunc[]) local5, (DeckRuleset) local6, num != 0).Count / (float) AchieveManager.Get().GetAchievesInGroup(Achievement.AchType.UNLOCK_HERO, true).Count;
    }
    return (double) this.m_numCardsPerClass > 6.0;
  }

  private bool IsChoosingHeroForTavernBrawlChallenge()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.FRIENDLY)
      return FriendChallengeMgr.Get().IsChallengeTavernBrawl();
    return false;
  }

  private bool ShouldGlowCollectionButton()
  {
    return this.ShouldShowCollectionButton() && this.m_collectionButton.IsEnabled() && (!Options.Get().GetBool(Option.HAS_CLICKED_COLLECTION_BUTTON_FOR_NEW_DECK) && this.HaveDecksThatNeedNames() || !Options.Get().GetBool(Option.HAS_CLICKED_COLLECTION_BUTTON_FOR_NEW_CARD) && this.HaveUnseenBasicCards() || (Options.Get().GetBool(Option.GLOW_COLLECTION_BUTTON_AFTER_SET_ROTATION) && SceneMgr.Get().GetMode() == SceneMgr.Mode.TOURNAMENT || (UnityEngine.Object) this.m_missingStandardDeckDisplay != (UnityEngine.Object) null && this.m_missingStandardDeckDisplay.IsShown()));
  }

  private bool HaveDecksThatNeedNames()
  {
    using (List<CollectionDeck>.Enumerator enumerator = CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.NeedsName)
          return true;
      }
    }
    return false;
  }

  private uint GetNumValidStandardDecks()
  {
    uint num = 0;
    using (List<CollectionDeck>.Enumerator enumerator = CollectionManager.Get().GetDecks(DeckType.NORMAL_DECK).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeck current = enumerator.Current;
        if (current.IsValidForFormat(false) && current.IsTourneyValid)
          ++num;
      }
    }
    return num;
  }

  private bool HaveUnseenBasicCards()
  {
    return CollectionManager.Get().FindCards((string) null, (List<CollectibleCardFilter.FilterMask>) null, new int?(), new TAG_CARD_SET[1]{ TAG_CARD_SET.CORE }, (TAG_CLASS[]) null, (TAG_CARDTYPE[]) null, new TAG_RARITY?(), new TAG_RACE?(), new bool?(false), new int?(1), new bool?(true), new bool?(), (CollectionManager.CollectibleCardFilterFunc[]) null, (DeckRuleset) null, 1 != 0).Count > 0;
  }

  [DebuggerHidden]
  private IEnumerator ShowUnlockAllHeroesCelebration()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CShowUnlockAllHeroesCelebration\u003Ec__Iterator78() { \u003C\u003Ef__this = this };
  }

  private void InnkeeperCelebrationFinished()
  {
    this.m_innkeeperQuoteFinished = true;
  }

  private void ShowInnkeeperQuoteIfNeeded()
  {
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    switch (mode)
    {
      case SceneMgr.Mode.COLLECTIONMANAGER:
        if (!Options.Get().GetBool(Option.SHOW_WILD_DISCLAIMER_POPUP_ON_CREATE_DECK) || !Options.Get().GetBool(Option.IN_WILD_MODE) || !UserAttentionManager.CanShowAttentionGrabber("DeckPickTrayDisplay.ShowInnkeeperQuoteIfNeeded:" + (object) Option.SHOW_WILD_DISCLAIMER_POPUP_ON_CREATE_DECK))
          break;
        NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, NotificationManager.DEFAULT_CHARACTER_POS, GameStrings.Get("VO_INNKEEPER_PLAY_STANDARD_TO_WILD"), "VO_INNKEEPER_Male_Dwarf_SetRotation_43", 0.0f, (Action) null, false);
        Options.Get().SetBool(Option.SHOW_WILD_DISCLAIMER_POPUP_ON_CREATE_DECK, false);
        break;
      case SceneMgr.Mode.TOURNAMENT:
      case SceneMgr.Mode.FRIENDLY:
        this.ShowInnkeeperDeckDialogIfNeeded(UserAttentionBlocker.NONE, (Action) null);
        break;
      default:
        if (mode != SceneMgr.Mode.ADVENTURE)
          break;
        goto case SceneMgr.Mode.TOURNAMENT;
    }
  }

  private bool ShowInnkeeperDeckDialogIfNeeded(UserAttentionBlocker blocker, Action finishCallback = null)
  {
    if (!this.IsShowingCustomDecks() || !Options.Get().GetBool(Option.SHOW_INNKEEPER_DECK_DIALOGUE) || !UserAttentionManager.CanShowAttentionGrabber(blocker, "UserAttentionManager.CanShowAttentionGrabber:" + (object) Option.SHOW_INNKEEPER_DECK_DIALOGUE))
      return false;
    NotificationManager.Get().CreateInnkeeperQuote(blocker, GameStrings.Get("VO_INNKEEPER_CM_NEW_INNKEEPER_DECK"), "VO_Innkeeper_Male_Dwarf_ReturningPlayers_10", finishCallback, false);
    Options.Get().SetBool(Option.SHOW_INNKEEPER_DECK_DIALOGUE, false);
    return true;
  }

  private bool ShouldShowCustomDecks()
  {
    if (this.m_deckPickerMode == DeckPickerMode.INVALID)
      UnityEngine.Debug.LogWarning((object) "DeckPickerTrayDisplay.ShowCustomDecks() - querying m_deckPickerMode when it hasn't been set yet!");
    if (this.m_deckPickerMode != DeckPickerMode.CUSTOM)
      return this.m_needUnlockAllHeroesTransition;
    return true;
  }

  private bool ShouldShowPreconDecks()
  {
    if (this.m_deckPickerMode == DeckPickerMode.INVALID)
      UnityEngine.Debug.LogWarning((object) "DeckPickerTrayDisplay.ShowPreconDecks() - querying m_deckPickerMode when it hasn't been set yet!");
    if (this.m_deckPickerMode != DeckPickerMode.PRECON)
      return this.m_needUnlockAllHeroesTransition;
    return true;
  }

  public void InitSetRotationTutorial()
  {
    if (this.m_setRotationTutorialState != DeckPickerTrayDisplay.SetRotationTutorialState.INACTIVE)
      UnityEngine.Debug.LogError((object) ("Tried to call DeckPickerTrayDisplay.InitTutorial() when m_setRotationTutorialState was " + this.m_setRotationTutorialState.ToString()));
    else if (!AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
    {
      UnityEngine.Debug.LogError((object) "Tried to call DeckPickerTrayDisplay.InitTutorial() for an account that has not unlocked all 9 heroes.");
    }
    else
    {
      Options.Get().SetBool(Option.IN_WILD_MODE, false);
      this.Deselect();
      this.ShowFirstPage();
      this.m_rankedPlayButtons.StartSetRotationTutorial();
      this.EnablePlayButton(false);
      this.EnableBackButton(false);
      this.EnableCollectionButton(false);
      this.m_rightArrow.gameObject.SetActive(false);
      this.m_leftArrow.gameObject.SetActive(false);
      this.m_rightArrow.SetEnabled(false);
      this.m_leftArrow.SetEnabled(false);
      this.m_switchFormatButton.SetFormat(true, false);
      this.m_switchFormatButton.Disable();
      this.m_switchFormatButton.gameObject.SetActive(false);
      this.SetHeaderText(GameStrings.Get("GLUE_TOURNAMENT"));
      if ((UnityEngine.Object) this.m_heroPower != (UnityEngine.Object) null)
        this.m_heroPower.GetComponent<Collider>().enabled = false;
      if ((UnityEngine.Object) this.m_goldenHeroPower != (UnityEngine.Object) null)
        this.m_goldenHeroPower.GetComponent<Collider>().enabled = false;
      Options.Get().SetBool(Option.IN_WILD_MODE, true);
      this.m_missingStandardDeckDisplay.Hide();
      this.UpdateTrayTransitionValues(false, false);
      foreach (CustomDeckPage customPage in this.m_customPages)
      {
        customPage.UpdateDeckVisuals(false, false, true);
        customPage.EnableDeckButtons(false);
      }
      this.m_setRotationTutorialState = DeckPickerTrayDisplay.SetRotationTutorialState.READY;
    }
  }

  public void StartSetRotationTutorial()
  {
    if (this.m_setRotationTutorialState == DeckPickerTrayDisplay.SetRotationTutorialState.READY)
      this.StartCoroutine(this.ShowFormatTutorialPopUp());
    else
      UnityEngine.Debug.LogError((object) "Tried to start Play Screen Set Rotation Tutorial without calling DeckPickerTrayDisplay.InitTutorial()");
  }

  [DebuggerHidden]
  private IEnumerator ShowFormatTutorialPopUp()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CShowFormatTutorialPopUp\u003Ec__Iterator79() { \u003C\u003Ef__this = this };
  }

  private void StartSwitchToStandard()
  {
    this.m_setRotationTutorialState = DeckPickerTrayDisplay.SetRotationTutorialState.SWITCH_TO_STANDARD;
    this.StartCoroutine(this.TutorialSwitchToStandard());
  }

  [DebuggerHidden]
  private IEnumerator TutorialSwitchToStandard()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CTutorialSwitchToStandard\u003Ec__Iterator7A() { \u003C\u003Ef__this = this };
  }

  private void OnSwitchFormatReleased(UIEvent e)
  {
    if (this.m_setRotationTutorialState == DeckPickerTrayDisplay.SetRotationTutorialState.SWITCH_TO_STANDARD)
    {
      this.m_switchFormatButton.Disable();
      this.m_switchFormatButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnSwitchFormatReleased));
      foreach (CustomDeckPage customPage in this.m_customPages)
        customPage.TransitionWildDecks();
      this.PlayTransitionSounds();
      this.UpdateTrayTransitionValues(false, false);
      this.StartCoroutine(this.ShowQuestLog());
    }
    else
      UnityEngine.Debug.Log((object) "OnSwitchFormatReleased called when not in SWITCH_TO_STANDARD Set Rotation Tutorial state");
  }

  private void PlayTransitionSounds()
  {
    if (!(!this.m_showingSecondPage ? this.m_customPages[0] : this.m_customPages[1]).HasWildDeck() || string.IsNullOrEmpty(this.m_wildDeckTransitionSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_wildDeckTransitionSound));
  }

  [DebuggerHidden]
  private IEnumerator ShowQuestLog()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CShowQuestLog\u003Ec__Iterator7B() { \u003C\u003Ef__this = this };
  }

  private void OnWelcomeQuestDismiss()
  {
    this.StartCoroutine(this.PlayVOAndEndTutorial());
  }

  [DebuggerHidden]
  private IEnumerator PlayVOAndEndTutorial()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckPickerTrayDisplay.\u003CPlayVOAndEndTutorial\u003Ec__Iterator7C() { \u003C\u003Ef__this = this };
  }

  private void OnTutorialVOEnded()
  {
    if ((UnityEngine.Object) this.m_switchFormatButton != (UnityEngine.Object) null)
      this.m_switchFormatButton.Enable();
    ApplicationMgr.Get().ScheduleCallback(5f, false, (ApplicationMgr.ScheduledCallback) (userdata => UserAttentionManager.StopBlocking(UserAttentionBlocker.SET_ROTATION_INTRO)), (object) null);
  }

  private class HeroFullDefLoadedCallbackData
  {
    public HeroPickerButton HeroPickerButton { get; private set; }

    public TAG_PREMIUM Premium { get; private set; }

    public HeroFullDefLoadedCallbackData(HeroPickerButton button, TAG_PREMIUM premium)
    {
      this.HeroPickerButton = button;
      this.Premium = premium;
    }
  }

  private enum SetRotationTutorialState
  {
    INACTIVE,
    READY,
    SHOW_FORMAT_TUTORIAL_POPUP,
    SWITCH_TO_STANDARD,
    SHOW_QUEST_LOG,
  }

  public delegate void DeckTrayLoaded();
}
