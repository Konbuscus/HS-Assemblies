﻿// Decompiled with JetBrains decompiler
// Type: ArmsDealingHintSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArmsDealingHintSpell : Spell
{
  public float m_IdleMaterialIntensity = 1f;
  public float m_MouseOverMaterialIntensity = 0.5f;
  public string m_MaterialIntensityMaterialVar = "_Intensity";
  private int m_materialIntensityPropertyID;
  private Renderer[] m_renderers;

  protected override void Awake()
  {
    base.Awake();
    if (!string.IsNullOrEmpty(this.m_MaterialIntensityMaterialVar))
      this.m_materialIntensityPropertyID = Shader.PropertyToID(this.m_MaterialIntensityMaterialVar);
    this.m_renderers = this.GetComponentsInChildren<Renderer>();
  }

  public void NotifyCardMousedOver()
  {
    this.SetMaterialIntensity(this.m_MouseOverMaterialIntensity);
  }

  public void NotifyCardMousedOut()
  {
    this.SetMaterialIntensity(this.m_IdleMaterialIntensity);
  }

  private void SetMaterialIntensity(float desiredIntensity)
  {
    if (this.m_renderers == null)
      return;
    foreach (Renderer renderer in this.m_renderers)
    {
      foreach (Material material in renderer.materials)
        material.SetFloat(this.m_materialIntensityPropertyID, desiredIntensity);
    }
  }
}
