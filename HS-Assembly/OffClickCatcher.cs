﻿// Decompiled with JetBrains decompiler
// Type: OffClickCatcher
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class OffClickCatcher : PegUIElement
{
  protected override void OnRelease()
  {
    Navigation.GoBack();
  }

  protected override void OnRightClick()
  {
    Navigation.GoBack();
  }
}
