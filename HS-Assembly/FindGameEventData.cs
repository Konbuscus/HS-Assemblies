﻿// Decompiled with JetBrains decompiler
// Type: FindGameEventData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs.types;

public class FindGameEventData
{
  public FindGameState m_state;
  public GameServerInfo m_gameServer;
  public Network.GameCancelInfo m_cancelInfo;
  public int m_queueMinSeconds;
  public int m_queueMaxSeconds;
}
