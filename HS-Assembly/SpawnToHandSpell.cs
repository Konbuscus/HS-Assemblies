﻿// Decompiled with JetBrains decompiler
// Type: SpawnToHandSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class SpawnToHandSpell : SuperSpell
{
  public float m_CardStartScale = 0.1f;
  public float m_CardDelay = 1f;
  public bool m_AccumulateStagger = true;
  public bool m_Shake = true;
  public ShakeMinionIntensity m_ShakeIntensity = ShakeMinionIntensity.MediumShake;
  public float m_CardStaggerMin;
  public float m_CardStaggerMax;
  public float m_ShakeDelay;
  public Spell m_SpellPrefab;
  protected Map<int, Card> m_targetToOriginMap;

  public override bool AddPowerTargets()
  {
    return this.AddPowerTargetsInternal(false);
  }

  public override void RemoveAllTargets()
  {
    base.RemoveAllTargets();
    if (this.m_targetToOriginMap == null)
      return;
    this.m_targetToOriginMap.Clear();
  }

  protected override Card GetTargetCardFromPowerTask(int index, PowerTask task)
  {
    Network.PowerHistory power = task.GetPower();
    if (power.Type != Network.PowerType.FULL_ENTITY)
      return (Card) null;
    Network.Entity entity1 = (power as Network.HistFullEntity).Entity;
    Entity entity2 = GameState.Get().GetEntity(entity1.ID);
    if (entity2 == null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("{0}.GetTargetCardFromPowerTask() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) entity1.ID));
      return (Card) null;
    }
    if (entity2.GetZone() != TAG_ZONE.HAND)
      return (Card) null;
    return entity2.GetCard();
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    this.StartCoroutine(this.DoEffectWithTiming());
  }

  protected virtual Vector3 GetOriginForTarget(int targetIndex = 0)
  {
    if (this.m_targetToOriginMap == null)
      return this.transform.position;
    Card card;
    if (!this.m_targetToOriginMap.TryGetValue(targetIndex, out card))
      return this.transform.position;
    return card.transform.position;
  }

  protected void AddOriginForTarget(int targetIndex, Card card)
  {
    if (this.m_targetToOriginMap == null)
      this.m_targetToOriginMap = new Map<int, Card>();
    this.m_targetToOriginMap[targetIndex] = card;
  }

  protected bool AddUniqueOriginForTarget(int targetIndex, Card card)
  {
    if (this.m_targetToOriginMap != null && this.m_targetToOriginMap.ContainsValue(card))
      return false;
    this.AddOriginForTarget(targetIndex, card);
    return true;
  }

  [DebuggerHidden]
  protected virtual IEnumerator DoEffectWithTiming()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpawnToHandSpell.\u003CDoEffectWithTiming\u003Ec__Iterator2A8() { \u003C\u003Ef__this = this };
  }

  protected string GetCardIdForTarget(int targetIndex)
  {
    return this.m_targets[targetIndex].GetComponent<Card>().GetEntity().GetCardId();
  }

  private void AddTransitionDelays()
  {
    if ((double) this.m_CardStaggerMin <= 0.0 && (double) this.m_CardStaggerMax <= 0.0)
      return;
    if (this.m_AccumulateStagger)
    {
      float delay = 0.0f;
      for (int index = 0; index < this.m_targets.Count; ++index)
      {
        Card component = this.m_targets[index].GetComponent<Card>();
        float num = Random.Range(this.m_CardStaggerMin, this.m_CardStaggerMax);
        delay += num;
        component.SetTransitionDelay(delay);
      }
    }
    else
    {
      for (int index = 0; index < this.m_targets.Count; ++index)
        this.m_targets[index].GetComponent<Card>().SetTransitionDelay(Random.Range(this.m_CardStaggerMin, this.m_CardStaggerMax));
    }
  }
}
