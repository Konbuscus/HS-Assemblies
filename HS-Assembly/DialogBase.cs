﻿// Decompiled with JetBrains decompiler
// Type: DialogBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class DialogBase : MonoBehaviour
{
  protected readonly Vector3 START_SCALE = 0.01f * Vector3.one;
  protected Vector3 PUNCH_SCALE = 1.2f * Vector3.one;
  protected List<DialogBase.HideListener> m_hideListeners = new List<DialogBase.HideListener>();
  protected DialogBase.ShowAnimState m_showAnimState;
  protected bool m_shown;
  protected Vector3 m_originalPosition;
  protected Vector3 m_originalScale;

  protected virtual CanvasScaleMode ScaleMode()
  {
    return CanvasScaleMode.HEIGHT;
  }

  protected virtual void Awake()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.PUNCH_SCALE = 1.08f * Vector3.one;
    OverlayUI overlayUi = OverlayUI.Get();
    CanvasScaleMode canvasScaleMode = this.ScaleMode();
    GameObject gameObject = this.gameObject;
    int num1 = 0;
    int num2 = 0;
    int num3 = (int) canvasScaleMode;
    overlayUi.AddGameObject(gameObject, (CanvasAnchor) num1, num2 != 0, (CanvasScaleMode) num3);
    this.m_originalPosition = this.transform.position;
    this.m_originalScale = this.transform.localScale;
    this.SetHiddenPosition((Camera) null);
  }

  public virtual bool HandleKeyboardInput()
  {
    return false;
  }

  public virtual void GoBack()
  {
  }

  public virtual void Show()
  {
    this.m_shown = true;
    this.SetShownPosition();
  }

  public virtual void Hide()
  {
    this.m_shown = false;
    this.StartCoroutine(this.HideWhenAble());
  }

  public virtual bool IsShown()
  {
    return this.m_shown;
  }

  public bool AddHideListener(DialogBase.HideCallback callback)
  {
    return this.AddHideListener(callback, (object) null);
  }

  public bool AddHideListener(DialogBase.HideCallback callback, object userData)
  {
    DialogBase.HideListener hideListener = new DialogBase.HideListener();
    hideListener.SetCallback(callback);
    hideListener.SetUserData(userData);
    if (this.m_hideListeners.Contains(hideListener))
      return false;
    this.m_hideListeners.Add(hideListener);
    return true;
  }

  public bool RemoveHideListener(DialogBase.HideCallback callback)
  {
    return this.RemoveHideListener(callback, (object) null);
  }

  public bool RemoveHideListener(DialogBase.HideCallback callback, object userData)
  {
    DialogBase.HideListener hideListener = new DialogBase.HideListener();
    hideListener.SetCallback(callback);
    hideListener.SetUserData(userData);
    return this.m_hideListeners.Remove(hideListener);
  }

  protected void SetShownPosition()
  {
    this.transform.position = this.m_originalPosition;
  }

  protected void SetHiddenPosition(Camera referenceCamera = null)
  {
    if ((Object) referenceCamera == (Object) null)
      referenceCamera = PegUI.Get().orthographicUICam;
    this.transform.position = referenceCamera.transform.TransformPoint(0.0f, 0.0f, -1000f);
  }

  protected void DoShowAnimation()
  {
    this.m_showAnimState = DialogBase.ShowAnimState.IN_PROGRESS;
    AnimationUtil.ShowWithPunch(this.gameObject, this.START_SCALE, Vector3.Scale(this.PUNCH_SCALE, this.m_originalScale), this.m_originalScale, "OnShowAnimFinished", false, (GameObject) null, (object) null, (AnimationUtil.DelOnShownWithPunch) null);
  }

  protected void DoHideAnimation()
  {
    AnimationUtil.ScaleFade(this.gameObject, this.START_SCALE, "OnHideAnimFinished");
  }

  protected virtual void OnHideAnimFinished()
  {
    this.SetHiddenPosition((Camera) null);
    UniversalInputManager.Get().SetSystemDialogActive(false);
    this.FireHideListeners();
  }

  protected void FireHideListeners()
  {
    foreach (DialogBase.HideListener hideListener in this.m_hideListeners.ToArray())
      hideListener.Fire(this);
  }

  protected virtual void OnShowAnimFinished()
  {
    this.m_showAnimState = DialogBase.ShowAnimState.FINISHED;
  }

  [DebuggerHidden]
  private IEnumerator HideWhenAble()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DialogBase.\u003CHideWhenAble\u003Ec__IteratorA4() { \u003C\u003Ef__this = this };
  }

  protected class HideListener : EventListener<DialogBase.HideCallback>
  {
    public void Fire(DialogBase dialog)
    {
      this.m_callback(dialog, this.m_userData);
    }
  }

  protected enum ShowAnimState
  {
    NOT_CALLED,
    IN_PROGRESS,
    FINISHED,
  }

  public delegate void HideCallback(DialogBase dialog, object userData);
}
