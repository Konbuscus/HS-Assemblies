﻿// Decompiled with JetBrains decompiler
// Type: PowerTaskList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusGame;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PowerTaskList
{
  private List<PowerTask> m_tasks = new List<PowerTask>();
  private int m_id;
  private Network.HistBlockStart m_blockStart;
  private Network.HistBlockEnd m_blockEnd;
  private ZoneChangeList m_zoneChangeList;
  private PowerTaskList m_previous;
  private PowerTaskList m_next;
  private PowerTaskList m_parent;
  private bool m_attackDataBuilt;
  private AttackInfo m_attackInfo;
  private AttackType m_attackType;
  private Entity m_attacker;
  private Entity m_defender;
  private Entity m_proposedDefender;
  private bool m_willCompleteHistoryEntry;
  private Entity m_ritualEntityClone;

  public int GetId()
  {
    return this.m_id;
  }

  public void SetId(int id)
  {
    this.m_id = id;
  }

  public bool IsEmpty()
  {
    PowerTaskList origin = this.GetOrigin();
    return origin.m_blockStart == null && origin.m_blockEnd == null && origin.m_tasks.Count <= 0;
  }

  public bool IsOrigin()
  {
    return this.m_previous == null;
  }

  public PowerTaskList GetOrigin()
  {
    PowerTaskList powerTaskList = this;
    while (powerTaskList.m_previous != null)
      powerTaskList = powerTaskList.m_previous;
    return powerTaskList;
  }

  public PowerTaskList GetPrevious()
  {
    return this.m_previous;
  }

  public void SetPrevious(PowerTaskList taskList)
  {
    this.m_previous = taskList;
    taskList.m_next = this;
  }

  public PowerTaskList GetNext()
  {
    return this.m_next;
  }

  public PowerTaskList GetLast()
  {
    PowerTaskList powerTaskList = this;
    while (powerTaskList.m_next != null)
      powerTaskList = powerTaskList.m_next;
    return powerTaskList;
  }

  public Network.HistBlockStart GetBlockStart()
  {
    return this.GetOrigin().m_blockStart;
  }

  public void SetBlockStart(Network.HistBlockStart blockStart)
  {
    this.m_blockStart = blockStart;
  }

  public Network.HistBlockEnd GetBlockEnd()
  {
    return this.m_blockEnd;
  }

  public void SetBlockEnd(Network.HistBlockEnd blockEnd)
  {
    this.m_blockEnd = blockEnd;
  }

  public PowerTaskList GetParent()
  {
    return this.GetOrigin().m_parent;
  }

  public void SetParent(PowerTaskList parent)
  {
    this.m_parent = parent;
  }

  public bool IsBlock()
  {
    return this.GetOrigin().m_blockStart != null;
  }

  public bool IsStartOfBlock()
  {
    if (!this.IsBlock())
      return false;
    return this.m_blockStart != null;
  }

  public bool IsEndOfBlock()
  {
    if (!this.IsBlock())
      return false;
    return this.m_blockEnd != null;
  }

  public bool DoesBlockHaveEndAction()
  {
    return this.GetLast().m_blockEnd != null;
  }

  public bool IsBlockUnended()
  {
    return this.IsBlock() && !this.DoesBlockHaveEndAction();
  }

  public bool IsEarlierInBlockThan(PowerTaskList taskList)
  {
    if (taskList == null)
      return false;
    for (PowerTaskList previous = taskList.m_previous; previous != null; previous = previous.m_previous)
    {
      if (this == previous)
        return true;
    }
    return false;
  }

  public bool IsLaterInBlockThan(PowerTaskList taskList)
  {
    if (taskList == null)
      return false;
    for (PowerTaskList next = taskList.m_next; next != null; next = next.m_next)
    {
      if (this == next)
        return true;
    }
    return false;
  }

  public bool IsInBlock(PowerTaskList taskList)
  {
    return this == taskList || this.IsEarlierInBlockThan(taskList) || this.IsLaterInBlockThan(taskList);
  }

  public bool IsDescendantOfBlock(PowerTaskList taskList)
  {
    if (taskList == null)
      return false;
    if (this.IsInBlock(taskList))
      return true;
    PowerTaskList origin = taskList.GetOrigin();
    for (PowerTaskList parent = this.GetParent(); parent != null; parent = parent.m_parent)
    {
      if (parent == origin)
        return true;
    }
    return false;
  }

  public List<PowerTask> GetTaskList()
  {
    return this.m_tasks;
  }

  public bool HasTasks()
  {
    return this.m_tasks.Count > 0;
  }

  public PowerTask CreateTask(Network.PowerHistory netPower)
  {
    PowerTask powerTask = new PowerTask();
    powerTask.SetPower(netPower);
    this.m_tasks.Add(powerTask);
    return powerTask;
  }

  public bool HasTasksOfType(Network.PowerType powType)
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_tasks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.GetPower().Type == powType)
          return true;
      }
    }
    return false;
  }

  public Entity GetSourceEntity()
  {
    Network.HistBlockStart blockStart = this.GetBlockStart();
    if (blockStart == null)
      return (Entity) null;
    int entity1 = blockStart.Entity;
    Entity entity2 = GameState.Get().GetEntity(entity1);
    if (entity2 != null)
      return entity2;
    UnityEngine.Debug.LogWarning((object) string.Format("PowerProcessor.GetSourceEntity() - task list {0} has a source entity with id {1} but there is no entity with that id", (object) this.m_id, (object) entity1));
    return (Entity) null;
  }

  public string GetEffectCardId()
  {
    Network.HistBlockStart blockStart = this.GetBlockStart();
    if (blockStart == null)
      return (string) null;
    string effectCardId = blockStart.EffectCardId;
    if (!string.IsNullOrEmpty(effectCardId))
      return effectCardId;
    Entity sourceEntity = this.GetSourceEntity();
    if (sourceEntity == null)
      return (string) null;
    return sourceEntity.GetCardId();
  }

  public EntityDef GetEffectEntityDef()
  {
    string effectCardId = this.GetEffectCardId();
    if (string.IsNullOrEmpty(effectCardId))
      return (EntityDef) null;
    return DefLoader.Get().GetEntityDef(effectCardId);
  }

  public CardDef GetEffectCardDef()
  {
    string effectCardId = this.GetEffectCardId();
    if (string.IsNullOrEmpty(effectCardId))
      return (CardDef) null;
    return DefLoader.Get().GetCardDef(effectCardId, (CardPortraitQuality) null);
  }

  public Entity GetTargetEntity()
  {
    Network.HistBlockStart blockStart = this.GetBlockStart();
    if (blockStart == null)
      return (Entity) null;
    int target = blockStart.Target;
    Entity entity = GameState.Get().GetEntity(target);
    if (entity != null)
      return entity;
    UnityEngine.Debug.LogWarning((object) string.Format("PowerProcessor.GetTargetEntity() - task list {0} has a target entity with id {1} but there is no entity with that id", (object) this.m_id, (object) target));
    return (Entity) null;
  }

  public bool HasTargetEntity()
  {
    Network.HistBlockStart blockStart = this.GetBlockStart();
    if (blockStart == null)
      return false;
    return GameState.Get().GetEntity(blockStart.Target) != null;
  }

  public bool HasMetaDataTasks()
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_tasks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.GetPower().Type == Network.PowerType.META_DATA)
          return true;
      }
    }
    return false;
  }

  public bool DoesBlockHaveMetaDataTasks()
  {
    for (PowerTaskList powerTaskList = this.GetOrigin(); powerTaskList != null; powerTaskList = powerTaskList.m_next)
    {
      if (powerTaskList.HasMetaDataTasks())
        return true;
    }
    return false;
  }

  public bool HasEffectTimingMetaData()
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_tasks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.HistMetaData power = enumerator.Current.GetPower() as Network.HistMetaData;
        if (power != null && (power.MetaType == HistoryMeta.Type.TARGET || power.MetaType == HistoryMeta.Type.EFFECT_TIMING))
          return true;
      }
    }
    return false;
  }

  public bool DoesBlockHaveEffectTimingMetaData()
  {
    for (PowerTaskList powerTaskList = this.GetOrigin(); powerTaskList != null; powerTaskList = powerTaskList.m_next)
    {
      if (powerTaskList.HasEffectTimingMetaData())
        return true;
    }
    return false;
  }

  public HistoryBlock.Type GetBlockType()
  {
    Network.HistBlockStart blockStart = this.GetBlockStart();
    if (blockStart == null)
      return HistoryBlock.Type.INVALID;
    return blockStart.BlockType;
  }

  public bool IsBlockType(HistoryBlock.Type type)
  {
    Network.HistBlockStart blockStart = this.GetBlockStart();
    if (blockStart == null)
      return false;
    return blockStart.BlockType == type;
  }

  public bool IsPlayBlock()
  {
    return this.IsBlockType(HistoryBlock.Type.PLAY);
  }

  public bool IsPowerBlock()
  {
    return this.IsBlockType(HistoryBlock.Type.POWER);
  }

  public bool IsTriggerBlock()
  {
    return this.IsBlockType(HistoryBlock.Type.TRIGGER);
  }

  public bool IsDeathBlock()
  {
    return this.IsBlockType(HistoryBlock.Type.DEATHS);
  }

  public bool IsRitualBlock()
  {
    return this.IsBlockType(HistoryBlock.Type.RITUAL);
  }

  public void DoTasks(int startIndex, int count)
  {
    this.DoTasks(startIndex, count, (PowerTaskList.CompleteCallback) null, (object) null);
  }

  public void DoTasks(int startIndex, int count, PowerTaskList.CompleteCallback callback)
  {
    this.DoTasks(startIndex, count, callback, (object) null);
  }

  public void DoTasks(int startIndex, int count, PowerTaskList.CompleteCallback callback, object userData)
  {
    bool flag = false;
    int num1 = -1;
    int num2 = Mathf.Min(startIndex + count - 1, this.m_tasks.Count - 1);
    for (int index = startIndex; index <= num2; ++index)
    {
      PowerTask task = this.m_tasks[index];
      if (!task.IsCompleted())
      {
        if (num1 < 0)
          num1 = index;
        if (ZoneMgr.IsHandledPower(task.GetPower()))
        {
          flag = true;
          break;
        }
      }
    }
    if (num1 < 0)
      num1 = startIndex;
    if (flag)
    {
      this.m_zoneChangeList = ZoneMgr.Get().AddServerZoneChanges(this, num1, num2, new ZoneMgr.ChangeCompleteCallback(this.OnZoneChangeComplete), (object) new PowerTaskList.ZoneChangeCallbackData()
      {
        m_startIndex = startIndex,
        m_count = count,
        m_taskListCallback = callback,
        m_taskListUserData = userData
      });
      if (this.m_zoneChangeList != null)
        return;
    }
    if ((Object) Gameplay.Get() != (Object) null)
      Gameplay.Get().StartCoroutine(this.WaitForGameStateAndDoTasks(num1, num2, startIndex, count, callback, userData));
    else
      this.DoTasks(num1, num2, startIndex, count, callback, userData);
  }

  public void DoAllTasks(PowerTaskList.CompleteCallback callback, object userData)
  {
    this.DoTasks(0, this.m_tasks.Count, callback, userData);
  }

  public void DoAllTasks(PowerTaskList.CompleteCallback callback)
  {
    this.DoTasks(0, this.m_tasks.Count, callback, (object) null);
  }

  public void DoAllTasks()
  {
    this.DoTasks(0, this.m_tasks.Count, (PowerTaskList.CompleteCallback) null, (object) null);
  }

  public void DoEarlyConcedeTasks()
  {
    for (int index = 0; index < this.m_tasks.Count; ++index)
      this.m_tasks[index].DoEarlyConcedeTask();
  }

  public bool IsComplete()
  {
    return this.AreTasksComplete() && this.AreZoneChangesComplete();
  }

  public bool AreTasksComplete()
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_tasks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (!enumerator.Current.IsCompleted())
          return false;
      }
    }
    return true;
  }

  public bool IsTaskPartOfMetaData(int taskIndex, HistoryMeta.Type metaType)
  {
    for (int index = taskIndex; index >= 0; --index)
    {
      Network.PowerHistory power = this.m_tasks[index].GetPower();
      if (power.Type == Network.PowerType.META_DATA && ((Network.HistMetaData) power).MetaType == metaType)
        return true;
    }
    return false;
  }

  public int FindEarlierIncompleteTaskIndex(int taskIndex)
  {
    for (int index = taskIndex - 1; index >= 0; --index)
    {
      if (!this.m_tasks[index].IsCompleted())
        return index;
    }
    return -1;
  }

  public bool HasEarlierIncompleteTask(int taskIndex)
  {
    return this.FindEarlierIncompleteTaskIndex(taskIndex) >= 0;
  }

  public bool HasZoneChanges()
  {
    return this.m_zoneChangeList != null;
  }

  public bool AreZoneChangesComplete()
  {
    if (this.m_zoneChangeList == null)
      return true;
    return this.m_zoneChangeList.IsComplete();
  }

  public AttackInfo GetAttackInfo()
  {
    this.BuildAttackData();
    return this.m_attackInfo;
  }

  public AttackType GetAttackType()
  {
    this.BuildAttackData();
    return this.m_attackType;
  }

  public Entity GetAttacker()
  {
    this.BuildAttackData();
    return this.m_attacker;
  }

  public Entity GetDefender()
  {
    this.BuildAttackData();
    return this.m_defender;
  }

  public Entity GetProposedDefender()
  {
    this.BuildAttackData();
    return this.m_proposedDefender;
  }

  public bool HasGameOver()
  {
    for (int index = 0; index < this.m_tasks.Count; ++index)
    {
      Network.PowerHistory power = this.m_tasks[index].GetPower();
      if (power.Type == Network.PowerType.TAG_CHANGE)
      {
        Network.HistTagChange histTagChange = (Network.HistTagChange) power;
        if (GameUtils.IsGameOverTag(histTagChange.Entity, histTagChange.Tag, histTagChange.Value))
          return true;
      }
    }
    return false;
  }

  public bool HasFriendlyConcede()
  {
    for (int index = 0; index < this.m_tasks.Count; ++index)
    {
      Network.PowerHistory power = this.m_tasks[index].GetPower();
      if (power.Type == Network.PowerType.TAG_CHANGE && GameUtils.IsFriendlyConcede((Network.HistTagChange) power))
        return true;
    }
    return false;
  }

  public PowerTaskList.DamageInfo GetDamageInfo(Entity entity)
  {
    if (entity == null)
      return (PowerTaskList.DamageInfo) null;
    int entityId = entity.GetEntityId();
    using (List<PowerTask>.Enumerator enumerator = this.m_tasks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.PowerHistory power = enumerator.Current.GetPower();
        if (power.Type == Network.PowerType.TAG_CHANGE)
        {
          Network.HistTagChange histTagChange = power as Network.HistTagChange;
          if (histTagChange.Tag == 44 && histTagChange.Entity == entityId)
          {
            PowerTaskList.DamageInfo damageInfo = new PowerTaskList.DamageInfo();
            damageInfo.m_entity = GameState.Get().GetEntity(histTagChange.Entity);
            damageInfo.m_damage = histTagChange.Value - damageInfo.m_entity.GetDamage();
            return damageInfo;
          }
        }
      }
    }
    return (PowerTaskList.DamageInfo) null;
  }

  public void SetWillCompleteHistoryEntry(bool set)
  {
    this.m_willCompleteHistoryEntry = set;
  }

  public bool WillCompleteHistoryEntry()
  {
    return this.m_willCompleteHistoryEntry;
  }

  public bool WillBlockCompleteHistoryEntry()
  {
    for (PowerTaskList powerTaskList = this.GetOrigin(); powerTaskList != null; powerTaskList = powerTaskList.m_next)
    {
      if (powerTaskList.WillCompleteHistoryEntry())
        return true;
    }
    return false;
  }

  public Entity GetRitualEntityClone()
  {
    return this.m_ritualEntityClone;
  }

  public void SetRitualEntityClone(Entity ent)
  {
    this.m_ritualEntityClone = ent;
  }

  public bool WasThePlayedSpellCountered(Entity entity)
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_tasks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.PowerHistory power = enumerator.Current.GetPower();
        if (power.Type == Network.PowerType.TAG_CHANGE)
        {
          Network.HistTagChange histTagChange = power as Network.HistTagChange;
          if (histTagChange.Entity == entity.GetEntityId() && histTagChange.Tag == 231 && histTagChange.Value == 1)
            return true;
        }
      }
    }
    using (List<PowerTaskList>.Enumerator enumerator1 = GameState.Get().GetPowerProcessor().GetPowerQueue().GetList().GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        PowerTaskList current = enumerator1.Current;
        using (List<PowerTask>.Enumerator enumerator2 = current.GetTaskList().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Network.PowerHistory power = enumerator2.Current.GetPower();
            if (power.Type == Network.PowerType.TAG_CHANGE)
            {
              Network.HistTagChange histTagChange = power as Network.HistTagChange;
              if (histTagChange.Entity == entity.GetEntityId() && histTagChange.Tag == 231 && histTagChange.Value == 1)
                return true;
            }
          }
        }
        if (current.GetBlockEnd() != null && current.GetBlockStart().BlockType == HistoryBlock.Type.PLAY)
          return false;
      }
    }
    return false;
  }

  public void NotifyHistoryOfAdditionalTargets()
  {
    Network.HistBlockStart blockStart = this.GetBlockStart();
    int num = blockStart != null ? blockStart.Entity : 0;
    List<int> intList = new List<int>();
    bool flag1 = true;
    using (List<PowerTask>.Enumerator enumerator1 = this.GetTaskList().GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Network.PowerHistory power = enumerator1.Current.GetPower();
        if (power.Type == Network.PowerType.META_DATA)
        {
          Network.HistMetaData histMetaData = (Network.HistMetaData) power;
          if (histMetaData.MetaType == HistoryMeta.Type.TARGET)
          {
            for (int index = 0; index < histMetaData.Info.Count; ++index)
              HistoryManager.Get().NotifyEntityAffected(histMetaData.Info[index], false, false);
          }
          else if (histMetaData.MetaType == HistoryMeta.Type.DAMAGE || histMetaData.MetaType == HistoryMeta.Type.HEALING)
            flag1 = false;
          else if (histMetaData.MetaType == HistoryMeta.Type.OVERRIDE_HISTORY)
            HistoryManager.Get().OverrideCurrentHistoryEntryWithMetaData();
          else if (histMetaData.MetaType == HistoryMeta.Type.HISTORY_TARGET)
          {
            Entity entity = GameState.Get().GetEntity(histMetaData.Info[0]);
            if (entity != null)
              HistoryManager.Get().NotifyEntityAffected(entity, false, true);
          }
        }
        else if (power.Type == Network.PowerType.SHOW_ENTITY)
        {
          Network.HistShowEntity histShowEntity = (Network.HistShowEntity) power;
          bool flag2 = false;
          bool flag3 = false;
          bool flag4 = GameState.Get().GetEntity(histShowEntity.Entity.ID).GetZone() == TAG_ZONE.HAND;
          using (List<Network.Entity.Tag>.Enumerator enumerator2 = histShowEntity.Entity.Tags.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Network.Entity.Tag current = enumerator2.Current;
              if (current.Name == 202 && current.Value == 6)
              {
                flag2 = true;
                break;
              }
              if (current.Name == 49 && current.Value == 4)
                flag3 = true;
            }
          }
          if (!flag2)
          {
            if (flag3 && !flag4)
              HistoryManager.Get().NotifyEntityDied(histShowEntity.Entity.ID);
            else
              HistoryManager.Get().NotifyEntityAffected(histShowEntity.Entity.ID, false, false);
          }
        }
        else if (power.Type == Network.PowerType.FULL_ENTITY)
        {
          Network.HistFullEntity histFullEntity = (Network.HistFullEntity) power;
          bool flag2 = false;
          bool flag3 = false;
          bool flag4 = false;
          using (List<Network.Entity.Tag>.Enumerator enumerator2 = histFullEntity.Entity.Tags.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Network.Entity.Tag current = enumerator2.Current;
              if (current.Name == 202 && current.Value == 6)
              {
                flag2 = true;
                break;
              }
              if (current.Name == 49 && (current.Value == 1 || current.Value == 7))
                flag3 = true;
              else if (current.Name == 385 && current.Value == num)
                flag4 = true;
            }
          }
          if (!flag2 && (flag3 || flag4))
            HistoryManager.Get().NotifyEntityAffected(histFullEntity.Entity.ID, false, false);
        }
        else if (power.Type == Network.PowerType.TAG_CHANGE)
        {
          Network.HistTagChange tagChange = (Network.HistTagChange) power;
          Entity entity = GameState.Get().GetEntity(tagChange.Entity);
          if (tagChange.Tag == 44)
          {
            if (!intList.Contains(tagChange.Entity) && !flag1)
            {
              HistoryManager.Get().NotifyDamageChanged(entity, tagChange.Value);
              flag1 = true;
            }
          }
          else if (tagChange.Tag == 292)
          {
            if (!intList.Contains(tagChange.Entity))
              HistoryManager.Get().NotifyArmorChanged(entity, tagChange.Value);
          }
          else if (tagChange.Tag == 318)
            HistoryManager.Get().NotifyEntityAffected(entity, false, false);
          else if (tagChange.Tag == 385 && tagChange.Value == num)
            HistoryManager.Get().NotifyEntityAffected(entity, false, false);
          else if (tagChange.Tag == 262)
            HistoryManager.Get().NotifyEntityAffected(entity, false, false);
          if (GameUtils.IsHistoryDeathTagChange(tagChange))
          {
            HistoryManager.Get().NotifyEntityDied(entity);
            intList.Add(tagChange.Entity);
          }
          if (GameUtils.IsHistoryDiscardTagChange(tagChange))
            HistoryManager.Get().NotifyEntityAffected(entity, false, false);
        }
      }
    }
  }

  public bool ShouldCreatePlayBlockHistoryTile()
  {
    if (!this.IsPlayBlock())
      return false;
    PowerTaskList parent = this.GetParent();
    return parent == null || !parent.GetSourceEntity().HasTag(GAME_TAG.CAST_RANDOM_SPELLS);
  }

  public void DebugDump()
  {
    this.DebugDump(Log.Power);
  }

  public void DebugDump(Logger logger)
  {
    if (!logger.CanPrint())
      return;
    GameState gameState = GameState.Get();
    string indentation = string.Empty;
    int num1 = this.m_parent != null ? this.m_parent.GetId() : 0;
    int num2 = this.m_previous != null ? this.m_previous.GetId() : 0;
    logger.Print("PowerTaskList.DebugDump() - ID={0} ParentID={1} PreviousID={2} TaskCount={3}", (object) this.m_id, (object) num1, (object) num2, (object) this.m_tasks.Count);
    if (this.m_blockStart == null)
    {
      logger.Print("PowerTaskList.DebugDump() - {0}Block Start=(null)", (object) indentation);
      indentation += "    ";
    }
    else
      gameState.DebugPrintPower(logger, "PowerTaskList", (Network.PowerHistory) this.m_blockStart, ref indentation);
    for (int index = 0; index < this.m_tasks.Count; ++index)
    {
      Network.PowerHistory power = this.m_tasks[index].GetPower();
      gameState.DebugPrintPower(logger, "PowerTaskList", power, ref indentation);
    }
    if (this.m_blockEnd == null)
    {
      if (indentation.Length >= "    ".Length)
        indentation = indentation.Remove(indentation.Length - "    ".Length);
      logger.Print("PowerTaskList.DebugDump() - {0}Block End=(null)", (object) indentation);
    }
    else
      gameState.DebugPrintPower(logger, "PowerTaskList", (Network.PowerHistory) this.m_blockEnd, ref indentation);
  }

  public override string ToString()
  {
    return string.Format("id={0} tasks={1} prevId={2} nextId={3} parentId={4}", (object) this.m_id, (object) this.m_tasks.Count, (object) (this.m_previous != null ? this.m_previous.GetId() : 0), (object) (this.m_next != null ? this.m_next.GetId() : 0), (object) (this.m_parent != null ? this.m_parent.GetId() : 0));
  }

  private void OnZoneChangeComplete(ZoneChangeList changeList, object userData)
  {
    PowerTaskList.ZoneChangeCallbackData changeCallbackData = (PowerTaskList.ZoneChangeCallbackData) userData;
    if (changeCallbackData.m_taskListCallback == null)
      return;
    changeCallbackData.m_taskListCallback(this, changeCallbackData.m_startIndex, changeCallbackData.m_count, changeCallbackData.m_taskListUserData);
  }

  [DebuggerHidden]
  private IEnumerator WaitForGameStateAndDoTasks(int incompleteStartIndex, int endIndex, int startIndex, int count, PowerTaskList.CompleteCallback callback, object userData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PowerTaskList.\u003CWaitForGameStateAndDoTasks\u003Ec__IteratorDD() { incompleteStartIndex = incompleteStartIndex, endIndex = endIndex, callback = callback, startIndex = startIndex, count = count, userData = userData, \u003C\u0024\u003EincompleteStartIndex = incompleteStartIndex, \u003C\u0024\u003EendIndex = endIndex, \u003C\u0024\u003Ecallback = callback, \u003C\u0024\u003EstartIndex = startIndex, \u003C\u0024\u003Ecount = count, \u003C\u0024\u003EuserData = userData, \u003C\u003Ef__this = this };
  }

  private void DoTasks(int incompleteStartIndex, int endIndex, int startIndex, int count, PowerTaskList.CompleteCallback callback, object userData)
  {
    for (int index = incompleteStartIndex; index <= endIndex; ++index)
      this.m_tasks[index].DoTask();
    if (callback == null)
      return;
    callback(this, startIndex, count, userData);
  }

  private void BuildAttackData()
  {
    if (this.m_attackDataBuilt)
      return;
    this.m_attackInfo = this.BuildAttackInfo();
    AttackInfo info;
    this.m_attackType = this.DetermineAttackType(out info);
    this.m_attacker = (Entity) null;
    this.m_defender = (Entity) null;
    this.m_proposedDefender = (Entity) null;
    switch (this.m_attackType)
    {
      case AttackType.REGULAR:
        this.m_attacker = info.m_attacker;
        this.m_defender = info.m_defender;
        break;
      case AttackType.PROPOSED:
        this.m_attacker = info.m_proposedAttacker;
        this.m_defender = info.m_proposedDefender;
        this.m_proposedDefender = info.m_proposedDefender;
        break;
      case AttackType.CANCELED:
        this.m_attacker = this.m_previous.GetAttacker();
        this.m_proposedDefender = this.m_previous.GetProposedDefender();
        break;
      case AttackType.ONLY_ATTACKER:
        this.m_attacker = info.m_attacker;
        break;
      case AttackType.ONLY_DEFENDER:
        this.m_defender = info.m_defender;
        break;
      case AttackType.ONLY_PROPOSED_ATTACKER:
        this.m_attacker = info.m_proposedAttacker;
        break;
      case AttackType.ONLY_PROPOSED_DEFENDER:
        this.m_proposedDefender = info.m_proposedDefender;
        this.m_defender = info.m_proposedDefender;
        break;
      case AttackType.WAITING_ON_PROPOSED_ATTACKER:
      case AttackType.WAITING_ON_PROPOSED_DEFENDER:
      case AttackType.WAITING_ON_ATTACKER:
      case AttackType.WAITING_ON_DEFENDER:
        this.m_attacker = this.m_previous.GetAttacker();
        this.m_defender = this.m_previous.GetDefender();
        break;
    }
    this.m_attackDataBuilt = true;
  }

  private AttackInfo BuildAttackInfo()
  {
    GameState gameState = GameState.Get();
    AttackInfo attackInfo = new AttackInfo();
    bool flag = false;
    using (List<PowerTask>.Enumerator enumerator = this.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.PowerHistory power = enumerator.Current.GetPower();
        if (power.Type == Network.PowerType.TAG_CHANGE)
        {
          Network.HistTagChange histTagChange = power as Network.HistTagChange;
          if (histTagChange.Tag == 36)
          {
            attackInfo.m_defenderTagValue = new int?(histTagChange.Value);
            if (histTagChange.Value == 1)
              attackInfo.m_defender = gameState.GetEntity(histTagChange.Entity);
            flag = true;
          }
          else if (histTagChange.Tag == 38)
          {
            attackInfo.m_attackerTagValue = new int?(histTagChange.Value);
            if (histTagChange.Value == 1)
              attackInfo.m_attacker = gameState.GetEntity(histTagChange.Entity);
            flag = true;
          }
          else if (histTagChange.Tag == 39)
          {
            attackInfo.m_proposedAttackerTagValue = new int?(histTagChange.Value);
            if (histTagChange.Value != 0)
              attackInfo.m_proposedAttacker = gameState.GetEntity(histTagChange.Value);
            flag = true;
          }
          else if (histTagChange.Tag == 37)
          {
            attackInfo.m_proposedDefenderTagValue = new int?(histTagChange.Value);
            if (histTagChange.Value != 0)
              attackInfo.m_proposedDefender = gameState.GetEntity(histTagChange.Value);
            flag = true;
          }
        }
      }
    }
    if (flag)
      return attackInfo;
    return (AttackInfo) null;
  }

  private AttackType DetermineAttackType(out AttackInfo info)
  {
    info = this.m_attackInfo;
    GameState gameState = GameState.Get();
    GameEntity gameEntity = gameState.GetGameEntity();
    Entity entity1 = gameState.GetEntity(gameEntity.GetTag(GAME_TAG.PROPOSED_ATTACKER));
    Entity entity2 = gameState.GetEntity(gameEntity.GetTag(GAME_TAG.PROPOSED_DEFENDER));
    AttackType attackType = AttackType.INVALID;
    Entity entity3 = (Entity) null;
    Entity entity4 = (Entity) null;
    if (this.m_previous != null)
    {
      attackType = this.m_previous.GetAttackType();
      entity3 = this.m_previous.GetAttacker();
      entity4 = this.m_previous.GetDefender();
    }
    if (this.m_attackInfo != null)
    {
      if (this.m_attackInfo.m_attacker != null || this.m_attackInfo.m_defender != null)
      {
        if (this.m_attackInfo.m_attacker == null)
        {
          if (attackType != AttackType.ONLY_ATTACKER && attackType != AttackType.WAITING_ON_DEFENDER)
            return AttackType.ONLY_DEFENDER;
          info = new AttackInfo();
          info.m_attacker = entity3;
          info.m_defender = this.m_attackInfo.m_defender;
          return AttackType.REGULAR;
        }
        if (this.m_attackInfo.m_defender != null)
          return AttackType.REGULAR;
        if (attackType != AttackType.ONLY_DEFENDER && attackType != AttackType.WAITING_ON_ATTACKER)
          return AttackType.ONLY_ATTACKER;
        info = new AttackInfo();
        info.m_attacker = this.m_attackInfo.m_attacker;
        info.m_defender = entity4;
        return AttackType.REGULAR;
      }
      if (this.m_attackInfo.m_proposedAttacker != null || this.m_attackInfo.m_proposedDefender != null)
      {
        if (this.m_attackInfo.m_proposedAttacker == null)
        {
          if (entity1 == null)
            return AttackType.ONLY_PROPOSED_DEFENDER;
          info = new AttackInfo();
          info.m_proposedAttacker = entity1;
          info.m_proposedDefender = this.m_attackInfo.m_proposedDefender;
          return AttackType.PROPOSED;
        }
        if (this.m_attackInfo.m_proposedDefender != null)
          return AttackType.PROPOSED;
        if (entity2 == null)
          return AttackType.ONLY_PROPOSED_ATTACKER;
        info = new AttackInfo();
        info.m_proposedAttacker = this.m_attackInfo.m_proposedAttacker;
        info.m_proposedDefender = entity2;
        return AttackType.PROPOSED;
      }
      if (attackType == AttackType.REGULAR || attackType == AttackType.INVALID)
        return AttackType.INVALID;
    }
    if (attackType == AttackType.PROPOSED)
    {
      if (entity1 != null && entity1.GetZone() != TAG_ZONE.PLAY || entity2 != null && entity2.GetZone() != TAG_ZONE.PLAY)
        return AttackType.CANCELED;
      if (entity3 != entity1 || entity4 != entity2)
      {
        info = new AttackInfo();
        info.m_proposedAttacker = entity1;
        info.m_proposedDefender = entity2;
        return AttackType.PROPOSED;
      }
      if (entity1 == null || entity2 == null || this.IsEndOfBlock())
        return AttackType.CANCELED;
      info = new AttackInfo();
      info.m_proposedAttacker = entity1;
      info.m_proposedDefender = entity2;
      return AttackType.PROPOSED;
    }
    if (attackType == AttackType.CANCELED)
      return AttackType.INVALID;
    if (this.IsEndOfBlock())
    {
      if (attackType == AttackType.ONLY_ATTACKER || attackType == AttackType.WAITING_ON_DEFENDER)
        return AttackType.CANCELED;
      UnityEngine.Debug.LogWarningFormat("AttackSpellController.DetermineAttackType() - INVALID ATTACK prevAttackType={0} prevAttacker={1} prevDefender={2}", (object) attackType, (object) entity3, (object) entity4);
      return AttackType.INVALID;
    }
    if (attackType == AttackType.ONLY_PROPOSED_ATTACKER || attackType == AttackType.WAITING_ON_PROPOSED_DEFENDER)
      return AttackType.WAITING_ON_PROPOSED_DEFENDER;
    if (attackType == AttackType.ONLY_PROPOSED_DEFENDER || attackType == AttackType.WAITING_ON_PROPOSED_ATTACKER)
      return AttackType.WAITING_ON_PROPOSED_ATTACKER;
    if (attackType == AttackType.ONLY_ATTACKER || attackType == AttackType.WAITING_ON_DEFENDER)
      return AttackType.WAITING_ON_DEFENDER;
    return attackType == AttackType.ONLY_DEFENDER || attackType == AttackType.WAITING_ON_ATTACKER ? AttackType.WAITING_ON_ATTACKER : AttackType.INVALID;
  }

  public class DamageInfo
  {
    public Entity m_entity;
    public int m_damage;
  }

  private class ZoneChangeCallbackData
  {
    public int m_startIndex;
    public int m_count;
    public PowerTaskList.CompleteCallback m_taskListCallback;
    public object m_taskListUserData;
  }

  public delegate void CompleteCallback(PowerTaskList taskList, int startIndex, int count, object userData);
}
