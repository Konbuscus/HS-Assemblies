﻿// Decompiled with JetBrains decompiler
// Type: WelcomeQuests
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WelcomeQuests : MonoBehaviour
{
  private const float SPECIAL_QUEST_DISMISS_DELAY = 2.5f;
  public QuestTile m_questTilePrefab;
  public Collider m_placementCollider;
  public Banner m_headlineBanner;
  public PegUIElement m_clickCatcher;
  public UberText m_questCaption;
  public UberText m_allCompletedCaption;
  public GameObject m_friendWeekReminderContainer;
  public UberText m_friendWeekReminderCaption;
  public GameObject m_friendWeekReminderGlow;
  public Transform m_phoneNoIksCaptionBone;
  public Animation m_bannerFX;
  public GameObject m_Root;
  public GameObject[] m_normalFXs;
  public GameObject[] m_legendaryFXs;
  private static WelcomeQuests s_instance;
  private WelcomeQuests.ShowRequestData m_showRequestData;
  private List<QuestTile> m_currentQuests;
  private Vector3 m_originalScale;

  public static void Show(UserAttentionBlocker blocker, bool fromLogin, WelcomeQuests.DelOnWelcomeQuestsClosed onCloseCallback = null, bool keepRichPresence = false)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    WelcomeQuests.\u003CShow\u003Ec__AnonStorey3D1 showCAnonStorey3D1 = new WelcomeQuests.\u003CShow\u003Ec__AnonStorey3D1();
    if (!UserAttentionManager.CanShowAttentionGrabber(blocker, "WelcomeQuests.Show:" + (object) fromLogin))
    {
      if (onCloseCallback == null)
        return;
      onCloseCallback();
    }
    else
    {
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.WELCOMEQUESTS);
      // ISSUE: reference to a compiler-generated field
      showCAnonStorey3D1.showRequestData = new WelcomeQuests.ShowRequestData()
      {
        m_fromLogin = fromLogin,
        m_onCloseCallback = onCloseCallback,
        m_keepRichPresence = keepRichPresence,
        m_achievement = (Achievement) null
      };
      if ((UnityEngine.Object) WelcomeQuests.s_instance != (UnityEngine.Object) null)
      {
        Debug.LogWarning((object) "WelcomeQuests.Show(): requested to show welcome quests while it was already active!");
        // ISSUE: reference to a compiler-generated field
        WelcomeQuests.s_instance.InitAndShow(showCAnonStorey3D1.showRequestData);
      }
      else
      {
        // ISSUE: reference to a compiler-generated method
        PopupDisplayManager.Get().ShowAnyOutstandingPopups(new PopupDisplayManager.AllPopupsShownCallback(showCAnonStorey3D1.\u003C\u003Em__183));
      }
    }
  }

  public static void ShowSpecialQuest(UserAttentionBlocker blocker, Achievement achievement, WelcomeQuests.DelOnWelcomeQuestsClosed onCloseCallback = null, bool keepRichPresence = false)
  {
    if (!UserAttentionManager.CanShowAttentionGrabber(blocker, "WelcomeQuests.ShowSpecialQuest:" + (achievement != null ? achievement.ID.ToString() : "null")))
    {
      if (onCloseCallback == null)
        return;
      onCloseCallback();
    }
    else
    {
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.WELCOMEQUESTS);
      WelcomeQuests.ShowRequestData showRequestData = new WelcomeQuests.ShowRequestData() { m_fromLogin = false, m_onCloseCallback = onCloseCallback, m_keepRichPresence = keepRichPresence, m_achievement = achievement };
      if ((UnityEngine.Object) WelcomeQuests.s_instance != (UnityEngine.Object) null)
      {
        Debug.LogWarning((object) "WelcomeQuests.Show(): requested to show welcome quests while it was already active!");
        WelcomeQuests.s_instance.InitAndShow(showRequestData);
      }
      else
        AssetLoader.Get().LoadGameObject("WelcomeQuests", new AssetLoader.GameObjectCallback(WelcomeQuests.OnWelcomeQuestsLoaded), (object) showRequestData, false);
    }
  }

  public static void Hide()
  {
    if ((UnityEngine.Object) WelcomeQuests.s_instance == (UnityEngine.Object) null)
      return;
    WelcomeQuests.s_instance.Close();
  }

  public static WelcomeQuests Get()
  {
    return WelcomeQuests.s_instance;
  }

  public QuestTile GetFirstQuestTile()
  {
    return this.m_currentQuests[0];
  }

  public int CompleteAndReplaceAutoDestroyQuestTile(int achieveId)
  {
    using (List<QuestTile>.Enumerator enumerator = this.m_currentQuests.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        QuestTile current = enumerator.Current;
        if (current.GetQuestID() == achieveId)
        {
          current.CompleteAndAutoDestroyQuest();
          return AchieveManager.Get().GetAchievement(achieveId).LinkToId;
        }
      }
    }
    return 0;
  }

  public void ActivateClickCatcher()
  {
    this.m_clickCatcher.gameObject.SetActive(true);
    this.RegisterClickCatcher();
  }

  private void Awake()
  {
    this.m_originalScale = this.transform.localScale;
    this.m_headlineBanner.gameObject.SetActive(false);
    this.m_friendWeekReminderContainer.SetActive(false);
    this.m_questCaption.gameObject.SetActive(false);
    this.m_clickCatcher.gameObject.SetActive(false);
    this.m_allCompletedCaption.gameObject.SetActive(false);
    SoundManager.Get().Load("new_quest_pop_up");
    SoundManager.Get().Load("existing_quest_pop_up");
    SoundManager.Get().Load("new_quest_click_and_shrink");
  }

  private void OnDestroy()
  {
    if (!((UnityEngine.Object) WelcomeQuests.s_instance != (UnityEngine.Object) null))
      return;
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(WelcomeQuests.OnNavigateBack));
    WelcomeQuests.s_instance = (WelcomeQuests) null;
    this.FadeEffectsOut();
    if ((UnityEngine.Object) DeckPickerTrayDisplay.Get() != (UnityEngine.Object) null && (bool) UniversalInputManager.UsePhoneUI)
      DeckPickerTrayDisplay.Get().SetHeroDetailsTrayToIgnoreFullScreenEffects(true);
    InnKeepersSpecial.Get().Close();
  }

  private static void OnWelcomeQuestsLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) SceneMgr.Get() != (UnityEngine.Object) null && SceneMgr.Get().IsInGame())
    {
      if (!((UnityEngine.Object) WelcomeQuests.s_instance != (UnityEngine.Object) null))
        return;
      WelcomeQuests.s_instance.Close();
    }
    else if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      Debug.LogError((object) string.Format("WelcomeQuests.OnWelcomeQuestsLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      WelcomeQuests.s_instance = go.GetComponent<WelcomeQuests>();
      if ((UnityEngine.Object) WelcomeQuests.s_instance == (UnityEngine.Object) null)
      {
        Debug.LogError((object) string.Format("WelcomeQuests.OnWelcomeQuestsLoaded() - ERROR object \"{0}\" has no WelcomeQuests component", (object) name));
      }
      else
      {
        WelcomeQuests.ShowRequestData showRequestData = callbackData as WelcomeQuests.ShowRequestData;
        WelcomeQuests.s_instance.InitAndShow(showRequestData);
      }
    }
  }

  private List<Achievement> GetQuestsToShow(WelcomeQuests.ShowRequestData showRequestData)
  {
    List<Achievement> achievementList;
    if (showRequestData.m_achievement == null)
    {
      achievementList = AchieveManager.Get().GetActiveQuests(false);
    }
    else
    {
      achievementList = new List<Achievement>();
      achievementList.Add(showRequestData.m_achievement);
    }
    return achievementList;
  }

  private void InitAndShow(WelcomeQuests.ShowRequestData showRequestData)
  {
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    this.m_showRequestData = showRequestData;
    List<Achievement> questsToShow = this.GetQuestsToShow(showRequestData);
    if (questsToShow.Count < 1 && (!InnKeepersSpecial.Get().LoadedSuccessfully() || InnKeepersSpecial.Get().HasAlreadySeenResponse()))
    {
      Log.InnKeepersSpecial.Print("Skipping IKS! hasAlreadySeen={0}, loadedSucsesfully={1}", new object[2]
      {
        (object) InnKeepersSpecial.Get().HasAlreadySeenResponse(),
        (object) InnKeepersSpecial.Get().LoadedSuccessfully()
      });
      this.Close();
    }
    else
    {
      this.m_clickCatcher.gameObject.SetActive(true);
      if (showRequestData.IsSpecialQuestRequest())
        this.Invoke("RegisterClickCatcher", 2.5f);
      else if (!AchieveManager.Get().HasActiveAutoDestroyQuests())
        this.RegisterClickCatcher();
      this.ShowQuests(showRequestData);
      this.FadeEffectsIn();
      if ((UnityEngine.Object) DeckPickerTrayDisplay.Get() != (UnityEngine.Object) null && (bool) UniversalInputManager.UsePhoneUI)
        DeckPickerTrayDisplay.Get().SetHeroDetailsTrayToIgnoreFullScreenEffects(false);
      this.transform.localScale = new Vector3(1f / 1000f, 1f / 1000f, 1f / 1000f);
      iTween.ScaleTo(this.gameObject, this.m_originalScale, 0.5f);
      int val = Options.Get().GetInt(Option.IKS_VIEWS, 0) + 1;
      Options.Get().SetInt(Option.IKS_VIEWS, val);
      bool flag = Options.Get().GetBool(Option.FORCE_SHOW_IKS);
      if (showRequestData.m_fromLogin && InnKeepersSpecial.Get().LoadedSuccessfully() && (!ReturningPlayerMgr.Get().SuppressOldPopups && !AchieveManager.Get().HasActiveDialogQuests()))
      {
        if (val > 3 || flag)
        {
          if ((bool) UniversalInputManager.UsePhoneUI)
          {
            Vector3 localPosition = this.transform.localPosition;
            localPosition.y += 2f;
            this.transform.localPosition = localPosition;
          }
          Log.InnKeepersSpecial.Print("Showing IKS!");
          InnKeepersSpecial.Get().Show();
        }
        else
          Log.InnKeepersSpecial.Print("Skipping IKS! views={0}", (object) val);
      }
      else
        Log.InnKeepersSpecial.Print("Skipping IKS! login={0}, loadedSuccessfully={1}, ReturningPlayerMgr.Get().SuppressOldPopups={2}!", new object[3]
        {
          (object) showRequestData.m_fromLogin,
          (object) InnKeepersSpecial.Get().LoadedSuccessfully(),
          (object) ReturningPlayerMgr.Get().SuppressOldPopups
        });
      if ((bool) UniversalInputManager.UsePhoneUI && !InnKeepersSpecial.Get().IsShown && (UnityEngine.Object) this.m_phoneNoIksCaptionBone != (UnityEngine.Object) null)
      {
        this.m_friendWeekReminderContainer.transform.position = this.m_phoneNoIksCaptionBone.transform.position;
        this.m_questCaption.transform.position = this.m_phoneNoIksCaptionBone.transform.position;
      }
      Navigation.PushUnique(new Navigation.NavigateBackHandler(WelcomeQuests.OnNavigateBack));
      NarrativeManager.Get().OnWelcomeQuestsShown(questsToShow);
    }
  }

  private void RegisterClickCatcher()
  {
    if (!((UnityEngine.Object) WelcomeQuests.s_instance != (UnityEngine.Object) null))
      return;
    this.m_clickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.CloseWelcomeQuests));
  }

  private void ShowQuests(WelcomeQuests.ShowRequestData showRequestData)
  {
    List<Achievement> questsToShow = this.GetQuestsToShow(showRequestData);
    if (questsToShow.Count < 1)
    {
      this.m_allCompletedCaption.gameObject.SetActive(true);
    }
    else
    {
      this.m_headlineBanner.gameObject.SetActive(true);
      if (this.m_showRequestData.IsSpecialQuestRequest())
        this.m_headlineBanner.SetText(GameStrings.Get("GLUE_SPECIAL_QUEST_NOTIFICATION_HEADER"));
      else if (this.m_showRequestData.m_fromLogin)
        this.m_headlineBanner.SetText(GameStrings.Get("GLUE_QUEST_NOTIFICATION_HEADER"));
      else
        this.m_headlineBanner.SetText(GameStrings.Get("GLUE_QUEST_NOTIFICATION_HEADER_NEW_ONLY"));
      bool flag1 = SpecialEventManager.Get().IsEventActive(SpecialEventType.FRIEND_WEEK, false);
      bool flag2 = !flag1;
      if (!AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.DAILY_QUESTS) || showRequestData.IsSpecialQuestRequest() || ReturningPlayerMgr.Get().IsInReturningPlayerMode)
      {
        flag1 = false;
        flag2 = false;
      }
      this.m_friendWeekReminderContainer.SetActive(flag1);
      this.m_questCaption.gameObject.SetActive(flag2);
      if (flag1)
      {
        Vector3 localScale = this.m_friendWeekReminderGlow.transform.localScale;
        localScale.x = this.m_friendWeekReminderCaption.GetTextBounds().extents.x * 2f;
        this.m_friendWeekReminderGlow.transform.localScale = localScale;
      }
      bool flag3 = true;
      using (List<Achievement>.Enumerator enumerator = questsToShow.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (!enumerator.Current.IsLegendary)
          {
            flag3 = false;
            break;
          }
        }
      }
      foreach (GameObject normalFx in this.m_normalFXs)
        normalFx.SetActive(!flag3);
      foreach (GameObject legendaryFx in this.m_legendaryFXs)
        legendaryFx.SetActive(flag3);
      this.m_currentQuests = new List<QuestTile>();
      float num1 = 0.4808684f;
      float num2 = this.m_placementCollider.transform.position.x - this.m_placementCollider.GetComponent<Collider>().bounds.extents.x;
      float num3 = this.m_placementCollider.bounds.size.x / (float) questsToShow.Count;
      float num4 = num3 / 2f;
      bool flag4 = false;
      for (int index = 0; index < questsToShow.Count; ++index)
      {
        Achievement quest = questsToShow[index];
        bool flag5 = quest.IsNewlyActive();
        float y = 180f;
        if (flag5)
        {
          y = 0.0f;
          this.DoInnkeeperLine(quest);
        }
        GameObject go;
        if (quest.AutoDestroy && !string.IsNullOrEmpty(quest.QuestTilePrefabName))
        {
          go = GameUtils.LoadGameObjectWithComponent<QuestTile>(quest.QuestTilePrefabName).gameObject;
          if ((UnityEngine.Object) go == (UnityEngine.Object) null)
            go = UnityEngine.Object.Instantiate<GameObject>(this.m_questTilePrefab.gameObject);
        }
        else
          go = UnityEngine.Object.Instantiate<GameObject>(this.m_questTilePrefab.gameObject);
        SceneUtils.SetLayer(go, GameLayer.UI);
        go.transform.position = new Vector3(num2 + num4, this.m_placementCollider.transform.position.y, this.m_placementCollider.transform.position.z);
        go.transform.parent = this.transform;
        go.transform.localEulerAngles = new Vector3(90f, y, 0.0f);
        go.transform.localScale = new Vector3(num1, num1, num1);
        QuestTile component = go.GetComponent<QuestTile>();
        component.SetupTile(quest);
        this.m_currentQuests.Add(component);
        num4 += num3;
        if (flag5)
        {
          flag4 = true;
          this.FlipQuest(component);
        }
      }
      if (flag4)
        SoundManager.Get().LoadAndPlay("new_quest_pop_up");
      else
        SoundManager.Get().LoadAndPlay("existing_quest_pop_up");
    }
  }

  private void DoInnkeeperLine(Achievement quest)
  {
    if (quest.ID == 11 || quest.ID != 568)
      return;
    NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_RETURNING_PLAYER_QUEST1"), "VO_Innkeeper_Male_Dwarf_ReturningPlayers_06", 0.0f, (Action) null, false);
  }

  private void FlipQuest(QuestTile quest)
  {
    Hashtable args = iTween.Hash((object) "amount", (object) new Vector3(0.0f, 0.0f, 540f), (object) "delay", (object) 1, (object) "time", (object) 2f, (object) "easeType", (object) iTween.EaseType.easeOutElastic, (object) "space", (object) Space.Self);
    iTween.RotateAdd(quest.gameObject, args);
  }

  private void FadeEffectsIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((UnityEngine.Object) fullScreenFxMgr == (UnityEngine.Object) null)
      return;
    fullScreenFxMgr.SetBlurBrightness(1f);
    fullScreenFxMgr.SetBlurDesaturation(0.0f);
    fullScreenFxMgr.Vignette(0.4f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.Blur(1f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void FadeEffectsOut()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((UnityEngine.Object) fullScreenFxMgr == (UnityEngine.Object) null)
      return;
    fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void Close()
  {
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(WelcomeQuests.OnNavigateBack));
    WelcomeQuests.s_instance = (WelcomeQuests) null;
    this.m_clickCatcher.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.CloseWelcomeQuests));
    this.FadeEffectsOut();
    if ((UnityEngine.Object) DeckPickerTrayDisplay.Get() != (UnityEngine.Object) null && (bool) UniversalInputManager.UsePhoneUI)
      DeckPickerTrayDisplay.Get().SetHeroDetailsTrayToIgnoreFullScreenEffects(true);
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) Vector3.zero, (object) "time", (object) 0.5f, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "DestroyWelcomeQuests"));
    SoundManager.Get().LoadAndPlay("new_quest_click_and_shrink");
    this.m_bannerFX.Play("BannerClose");
    GameToastMgr.Get().UpdateQuestProgressToasts();
    GameToastMgr.Get().AddSeasonTimeRemainingToast();
    if (this.m_showRequestData != null)
    {
      if (!this.m_showRequestData.m_keepRichPresence)
        PresenceMgr.Get().SetPrevStatus();
      if (this.m_showRequestData.m_onCloseCallback != null)
        this.m_showRequestData.m_onCloseCallback();
    }
    InnKeepersSpecial.Get().Close();
  }

  public static bool OnNavigateBack()
  {
    if ((UnityEngine.Object) WelcomeQuests.s_instance != (UnityEngine.Object) null)
      WelcomeQuests.s_instance.Close();
    return true;
  }

  private void CloseWelcomeQuests(UIEvent e)
  {
    Navigation.GoBack();
  }

  private void DestroyWelcomeQuests()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  private class ShowRequestData
  {
    public bool m_fromLogin;
    public WelcomeQuests.DelOnWelcomeQuestsClosed m_onCloseCallback;
    public bool m_keepRichPresence;
    public Achievement m_achievement;

    public bool IsSpecialQuestRequest()
    {
      return this.m_achievement != null;
    }
  }

  public delegate void DelOnWelcomeQuestsClosed();
}
