﻿// Decompiled with JetBrains decompiler
// Type: ClientRequestManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using Networking;
using PegasusUtil;
using System;
using System.Collections.Generic;
using System.Diagnostics;

public class ClientRequestManager : IClientRequestManager
{
  private static Map<int, string> s_typeToStringMap = new Map<int, string>();
  private readonly ClientRequestManager.ClientRequestConfig m_defaultConfig = new ClientRequestManager.ClientRequestConfig() { ShouldRetryOnError = true, RequestedSystem = 0 };
  private ClientRequestManager.InternalState m_state = new ClientRequestManager.InternalState();
  private readonly Subscribe m_subscribePacket = new Subscribe();
  public uint m_nextContexId;
  public uint m_nextRequestId;

  public bool SendClientRequest(int type, IProtoBuf body, ClientRequestManager.ClientRequestConfig clientRequestConfig, RequestPhase requestPhase = RequestPhase.RUNNING, int subID = 0)
  {
    return this.SendClientRequestImpl(type, body, clientRequestConfig, requestPhase, subID);
  }

  public void NotifyResponseReceived(PegasusPacket packet)
  {
    this.NotifyResponseReceivedImpl(packet);
  }

  public void NotifyStartupSequenceComplete()
  {
    this.NotifyStartupSequenceCompleteImpl();
  }

  public bool HasPendingDeliveryPackets()
  {
    return this.HasPendingDeliveryPacketsImpl();
  }

  public int PeekNetClientRequestType()
  {
    return this.PeekNetClientRequestTypeImpl();
  }

  public PegasusPacket GetNextClientRequest()
  {
    return this.GetNextClientRequestImpl();
  }

  public void DropNextClientRequest()
  {
    this.DropNextClientRequestImpl();
  }

  public void NotifyLoginSequenceCompleted()
  {
    this.NotifyLoginSequenceCompletedImpl();
  }

  public bool ShouldIgnoreError(BnetErrorInfo errorInfo)
  {
    return this.ShouldIgnoreErrorImpl(errorInfo);
  }

  public void ScheduleResubscribe()
  {
    using (Map<int, ClientRequestManager.SystemChannel>.Enumerator enumerator = this.m_state.m_systems.Systems.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.ScheduleResubscribeWithNewRoute(enumerator.Current.Value);
    }
  }

  public void Terminate()
  {
    this.TerminateImpl();
  }

  public void SetDisconnectedFromBattleNet()
  {
    this.m_state = new ClientRequestManager.InternalState();
  }

  public void Update()
  {
    this.UpdateImpl();
  }

  public bool HasErrors()
  {
    return this.HasErrorsImpl();
  }

  private bool ShouldIgnoreErrorImpl(BnetErrorInfo errorInfo)
  {
    uint context = (uint) errorInfo.GetContext();
    if ((int) context == 0)
      return false;
    ClientRequestManager.ClientRequestType clientRequest = this.GetClientRequest(context, "should_ignore_error", true);
    if (clientRequest == null)
      return this.GetDroppedRequest(context, "should_ignore", true) || this.GetPendingSendRequest(context, "should_ignore", true) != null;
    BattleNetErrors error = errorInfo.GetError();
    if (clientRequest.IsSubsribeRequest)
    {
      if ((ulong) clientRequest.System.SubscribeAttempt >= clientRequest.System.MaxResubscribeAttempts)
        return !clientRequest.ShouldRetryOnError;
      return true;
    }
    if (!clientRequest.ShouldRetryOnError)
      return true;
    if ((long) clientRequest.System.PendingResponseTimeout == 0L)
      return false;
    switch (error)
    {
      case BattleNetErrors.ERROR_INTERNAL:
      case BattleNetErrors.ERROR_RPC_REQUEST_TIMED_OUT:
      case BattleNetErrors.ERROR_GAME_UTILITY_SERVER_NO_SERVER:
        return this.RescheduleSubscriptionAndRetryRequest(clientRequest, "received_error_util_lost");
      default:
        return false;
    }
  }

  private bool RescheduleSubscriptionAndRetryRequest(ClientRequestManager.ClientRequestType clientRequest, string errorReason)
  {
    this.AddRequestToPendingResponse(clientRequest, "resubscribe_and_retry_request");
    this.ScheduleResubscribeWithNewRoute(clientRequest.System);
    return true;
  }

  private void ProcessServiceUnavailable(ClientRequestResponse response, ClientRequestManager.ClientRequestType clientRequest)
  {
    ++clientRequest.RequestNotHandledCount;
    this.RescheduleSubscriptionAndRetryRequest(clientRequest, "received_CRRF_SERVICE_UNAVAILABLE");
  }

  private void ProcessClientRequestResponse(PegasusPacket packet, ClientRequestManager.ClientRequestType clientRequest)
  {
    if (!(packet.Body is ClientRequestResponse))
      return;
    ClientRequestResponse body = (ClientRequestResponse) packet.Body;
    ClientRequestResponse.ClientRequestResponseFlags requestResponseFlags1 = ClientRequestResponse.ClientRequestResponseFlags.CRRF_SERVICE_UNAVAILABLE;
    if ((body.ResponseFlags & requestResponseFlags1) != ClientRequestResponse.ClientRequestResponseFlags.CRRF_SERVICE_NONE)
      this.ProcessServiceUnavailable(body, clientRequest);
    ClientRequestResponse.ClientRequestResponseFlags requestResponseFlags2 = ClientRequestResponse.ClientRequestResponseFlags.CRRF_SERVICE_UNKNOWN_ERROR;
    if ((body.ResponseFlags & requestResponseFlags2) == ClientRequestResponse.ClientRequestResponseFlags.CRRF_SERVICE_NONE)
      return;
    this.m_state.m_receivedErrorSignal = true;
  }

  [Conditional("CLIENTREQUESTMANAGER_LOGGING")]
  private void PopulateStringMap()
  {
    ClientRequestManager.s_typeToStringMap.Add(201, "GetAccountInfo");
    ClientRequestManager.s_typeToStringMap.Add(202, "DeckList");
    ClientRequestManager.s_typeToStringMap.Add(203, "UtilHandshake");
    ClientRequestManager.s_typeToStringMap.Add(204, "UtilAuth");
    ClientRequestManager.s_typeToStringMap.Add(205, "UpdateLogin");
    ClientRequestManager.s_typeToStringMap.Add(206, "DebugAuth");
    ClientRequestManager.s_typeToStringMap.Add(207, "Collection");
    ClientRequestManager.s_typeToStringMap.Add(208, "GamesInfo");
    ClientRequestManager.s_typeToStringMap.Add(209, "CreateDeck");
    ClientRequestManager.s_typeToStringMap.Add(210, "DeleteDeck");
    ClientRequestManager.s_typeToStringMap.Add(211, "RenameDeck");
    ClientRequestManager.s_typeToStringMap.Add(212, "ProfileNotices");
    ClientRequestManager.s_typeToStringMap.Add(213, "AckNotice");
    ClientRequestManager.s_typeToStringMap.Add(214, "GetDeck");
    ClientRequestManager.s_typeToStringMap.Add(215, "DeckContents");
    ClientRequestManager.s_typeToStringMap.Add(216, "DBAction");
    ClientRequestManager.s_typeToStringMap.Add(217, "DeckCreated");
    ClientRequestManager.s_typeToStringMap.Add(218, "DeckDeleted");
    ClientRequestManager.s_typeToStringMap.Add(219, "DeckRenamed");
    ClientRequestManager.s_typeToStringMap.Add(220, "DeckGainedCard");
    ClientRequestManager.s_typeToStringMap.Add(221, "DeckLostCard");
    ClientRequestManager.s_typeToStringMap.Add(222, "DeckSetData");
    ClientRequestManager.s_typeToStringMap.Add(223, "AckCardSeen");
    ClientRequestManager.s_typeToStringMap.Add(224, "BoosterList");
    ClientRequestManager.s_typeToStringMap.Add(225, "OpenBooster");
    ClientRequestManager.s_typeToStringMap.Add(226, "BoosterContent");
    ClientRequestManager.s_typeToStringMap.Add(227, "ProfileLastLogin");
    ClientRequestManager.s_typeToStringMap.Add(228, "ClientTracking");
    ClientRequestManager.s_typeToStringMap.Add(229, "unused");
    ClientRequestManager.s_typeToStringMap.Add(230, "SetProgress");
    ClientRequestManager.s_typeToStringMap.Add(231, "ProfileDeckLimit");
    ClientRequestManager.s_typeToStringMap.Add(232, "MedalInfo");
    ClientRequestManager.s_typeToStringMap.Add(233, "ProfileProgress");
    ClientRequestManager.s_typeToStringMap.Add(234, "MedalHistory");
    ClientRequestManager.s_typeToStringMap.Add(235, "DraftBegin");
    ClientRequestManager.s_typeToStringMap.Add(236, "CardBacks");
    ClientRequestManager.s_typeToStringMap.Add(237, "GetBattlePayConfig");
    ClientRequestManager.s_typeToStringMap.Add(238, "BattlePayConfigResponse");
    ClientRequestManager.s_typeToStringMap.Add(239, "SetOptions");
    ClientRequestManager.s_typeToStringMap.Add(240, "GetOptions");
    ClientRequestManager.s_typeToStringMap.Add(241, "ClientOptions");
    ClientRequestManager.s_typeToStringMap.Add(242, "DraftRetire");
    ClientRequestManager.s_typeToStringMap.Add(243, "AckAchieveProgress");
    ClientRequestManager.s_typeToStringMap.Add(244, "DraftGetPicksAndContents");
    ClientRequestManager.s_typeToStringMap.Add(245, "DraftMakePick");
    ClientRequestManager.s_typeToStringMap.Add(246, "DraftBeginning");
    ClientRequestManager.s_typeToStringMap.Add(247, "DraftRetired");
    ClientRequestManager.s_typeToStringMap.Add(248, "DraftChoicesAndContents");
    ClientRequestManager.s_typeToStringMap.Add(249, "DraftChosen");
    ClientRequestManager.s_typeToStringMap.Add(250, "GetPurchaseMethod");
    ClientRequestManager.s_typeToStringMap.Add(251, "DraftError");
    ClientRequestManager.s_typeToStringMap.Add(252, "Achieves");
    ClientRequestManager.s_typeToStringMap.Add(253, "GetAchieves");
    ClientRequestManager.s_typeToStringMap.Add(254, "NOP");
    ClientRequestManager.s_typeToStringMap.Add((int) byte.MaxValue, "GetBattlePayStatus");
    ClientRequestManager.s_typeToStringMap.Add(256, "PurchaseResponse");
    ClientRequestManager.s_typeToStringMap.Add(257, "BuySellCard");
    ClientRequestManager.s_typeToStringMap.Add(258, "BoughtSoldCard");
    ClientRequestManager.s_typeToStringMap.Add(259, "DevBnetIdentify");
    ClientRequestManager.s_typeToStringMap.Add(260, "CardValues");
    ClientRequestManager.s_typeToStringMap.Add(261, "GuardianTrack");
    ClientRequestManager.s_typeToStringMap.Add(262, "ArcaneDustBalance");
    ClientRequestManager.s_typeToStringMap.Add(263, "CloseCardMarket");
    ClientRequestManager.s_typeToStringMap.Add(264, "GuardianVars");
    ClientRequestManager.s_typeToStringMap.Add(265, "BattlePayStatusResponse");
    ClientRequestManager.s_typeToStringMap.Add(266, "Error37 (deprecated)");
    ClientRequestManager.s_typeToStringMap.Add(267, "CheckAccountLicenses");
    ClientRequestManager.s_typeToStringMap.Add(268, "MassDisenchant");
    ClientRequestManager.s_typeToStringMap.Add(269, "MassDisenchantResponse");
    ClientRequestManager.s_typeToStringMap.Add(270, "PlayerRecords");
    ClientRequestManager.s_typeToStringMap.Add(271, "RewardProgress");
    ClientRequestManager.s_typeToStringMap.Add(272, "PurchaseMethod");
    ClientRequestManager.s_typeToStringMap.Add(273, "DoPurchase");
    ClientRequestManager.s_typeToStringMap.Add(274, "CancelPurchase");
    ClientRequestManager.s_typeToStringMap.Add(275, "CancelPurchaseResponse");
    ClientRequestManager.s_typeToStringMap.Add(276, "CheckGameLicenses");
    ClientRequestManager.s_typeToStringMap.Add(277, "CheckLicensesResponse");
    ClientRequestManager.s_typeToStringMap.Add(278, "GoldBalance");
    ClientRequestManager.s_typeToStringMap.Add(279, "PurchaseWithGold");
    ClientRequestManager.s_typeToStringMap.Add(280, "PurchaseWithGoldResponse");
    ClientRequestManager.s_typeToStringMap.Add(281, "CancelQuest");
    ClientRequestManager.s_typeToStringMap.Add(282, "CancelQuestResponse");
    ClientRequestManager.s_typeToStringMap.Add(283, "HeroXP");
    ClientRequestManager.s_typeToStringMap.Add(284, "ValidateAchieve");
    ClientRequestManager.s_typeToStringMap.Add(285, "ValidateAchieveResponse");
    ClientRequestManager.s_typeToStringMap.Add(286, "PlayQueue");
    ClientRequestManager.s_typeToStringMap.Add(287, "DraftAckRewards");
    ClientRequestManager.s_typeToStringMap.Add(288, "DraftRewardsAcked");
    ClientRequestManager.s_typeToStringMap.Add(289, "Disconnected");
    ClientRequestManager.s_typeToStringMap.Add(290, "Deadend");
    ClientRequestManager.s_typeToStringMap.Add(291, "SetCardBack");
    ClientRequestManager.s_typeToStringMap.Add(292, "SetCardBackResponse");
    ClientRequestManager.s_typeToStringMap.Add(293, "SubmitThirdPartyReceipt");
    ClientRequestManager.s_typeToStringMap.Add(294, "GetThirdPartyPurchaseStatus");
    ClientRequestManager.s_typeToStringMap.Add(295, "ThirdPartyPurchaseStatusResponse");
    ClientRequestManager.s_typeToStringMap.Add(296, "SetProgressResponse");
    ClientRequestManager.s_typeToStringMap.Add(297, "CheckAccountLicenseAchieve");
    ClientRequestManager.s_typeToStringMap.Add(298, "TriggerLaunchDayEvent");
    ClientRequestManager.s_typeToStringMap.Add(299, "EventResponse");
    ClientRequestManager.s_typeToStringMap.Add(300, "MassiveLoginReply");
    ClientRequestManager.s_typeToStringMap.Add(301, "(used in Console.proto)");
    ClientRequestManager.s_typeToStringMap.Add(302, "(used in Console.proto)");
    ClientRequestManager.s_typeToStringMap.Add(303, "GetAssetsVersion");
    ClientRequestManager.s_typeToStringMap.Add(304, "AssetsVersionResponse");
    ClientRequestManager.s_typeToStringMap.Add(305, "GetAdventureProgress");
    ClientRequestManager.s_typeToStringMap.Add(306, "AdventureProgressResponse");
    ClientRequestManager.s_typeToStringMap.Add(307, "UpdateLoginComplete");
    ClientRequestManager.s_typeToStringMap.Add(308, "AckWingProgress");
    ClientRequestManager.s_typeToStringMap.Add(309, "SetPlayerAdventureProgress");
    ClientRequestManager.s_typeToStringMap.Add(310, "SetAdventureOptions");
    ClientRequestManager.s_typeToStringMap.Add(311, "AccountLicenseAchieveResponse");
    ClientRequestManager.s_typeToStringMap.Add(312, "StartThirdPartyPurchase");
    ClientRequestManager.s_typeToStringMap.Add(313, "BoosterTally");
    ClientRequestManager.s_typeToStringMap.Add(314, "Subscribe");
    ClientRequestManager.s_typeToStringMap.Add(315, "SubscribeResponse");
    ClientRequestManager.s_typeToStringMap.Add(316, "TavernBrawlInfo");
    ClientRequestManager.s_typeToStringMap.Add(317, "TavernBrawlPlayerRecordResponse");
    ClientRequestManager.s_typeToStringMap.Add(318, "FavoriteHeroesResponse");
    ClientRequestManager.s_typeToStringMap.Add(319, "SetFavoriteHero");
    ClientRequestManager.s_typeToStringMap.Add(320, "SetFavoriteHeroResponse");
    ClientRequestManager.s_typeToStringMap.Add(321, "GetAssetRequest");
    ClientRequestManager.s_typeToStringMap.Add(322, "GetAssetResponse");
    ClientRequestManager.s_typeToStringMap.Add(323, "DebugCommandRequest");
    ClientRequestManager.s_typeToStringMap.Add(324, "DebugCommandResponse");
    ClientRequestManager.s_typeToStringMap.Add(325, "AccountLicensesInfoResponse");
    ClientRequestManager.s_typeToStringMap.Add(326, "GenericResponse");
    ClientRequestManager.s_typeToStringMap.Add(327, "GenericRequestList");
    ClientRequestManager.s_typeToStringMap.Add(328, "ClientRequestResponse");
  }

  private string GetTypeName(int type)
  {
    string str1 = type.ToString();
    string str2;
    if (ClientRequestManager.s_typeToStringMap.Count > 0 && ClientRequestManager.s_typeToStringMap.TryGetValue(type, out str2))
      return str2 + ":" + str1;
    return str1;
  }

  [Conditional("CLIENTREQUESTMANAGER_LOGGING")]
  private void LOG_DEBUG(string format, params object[] args)
  {
    string str = GeneralUtils.SafeFormat(format, args);
    Log.ClientRequestManager.Print("D " + str);
  }

  [Conditional("CLIENTREQUESTMANAGER_LOGGING")]
  private void LOG_WARN(string format, params object[] args)
  {
    string str = GeneralUtils.SafeFormat(format, args);
    Log.ClientRequestManager.Print("W " + str);
  }

  [Conditional("CLIENTREQUESTMANAGER_LOGGING")]
  private void LOG_ERROR(string format, params object[] args)
  {
    string str = GeneralUtils.SafeFormat(format, args);
    Log.ClientRequestManager.Print("E " + str);
  }

  private bool HasPendingDeliveryPacketsImpl()
  {
    return this.m_state.m_responsesPendingDelivery.Count > 0;
  }

  private int PeekNetClientRequestTypeImpl()
  {
    if (this.m_state.m_responsesPendingDelivery.Count == 0)
      return 0;
    return this.m_state.m_responsesPendingDelivery.Peek().Type;
  }

  private PegasusPacket GetNextClientRequestImpl()
  {
    if (this.m_state.m_responsesPendingDelivery.Count == 0)
      return (PegasusPacket) null;
    return this.m_state.m_responsesPendingDelivery.Peek();
  }

  private void DropNextClientRequestImpl()
  {
    if (this.m_state.m_responsesPendingDelivery.Count == 0)
      return;
    this.m_state.m_responsesPendingDelivery.Dequeue();
  }

  private bool HasErrorsImpl()
  {
    return this.m_state.m_receivedErrorSignal;
  }

  private void UpdateImpl()
  {
    if (!this.m_state.m_loginCompleteNotificationReceived)
      return;
    using (Map<int, ClientRequestManager.SystemChannel>.Enumerator enumerator = this.m_state.m_systems.Systems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, ClientRequestManager.SystemChannel> current = enumerator.Current;
        if (this.UpdateStateSubscribeImpl(current.Value))
          this.ProcessClientRequests(current.Value);
      }
    }
  }

  private bool SendClientRequestImpl(int type, IProtoBuf body, ClientRequestManager.ClientRequestConfig clientRequestConfig, RequestPhase requestPhase, int subID)
  {
    if (type == 0 || requestPhase < RequestPhase.STARTUP || requestPhase > RequestPhase.RUNNING)
      return false;
    ClientRequestManager.ClientRequestConfig clientRequestConfig1 = clientRequestConfig != null ? clientRequestConfig : this.m_defaultConfig;
    ClientRequestManager.SystemChannel system = this.GetOrCreateSystem(clientRequestConfig1.RequestedSystem);
    if (requestPhase < system.CurrentPhase || system.WasEverInRunningPhase && requestPhase < RequestPhase.RUNNING || body == null)
      return false;
    ClientRequestManager.ClientRequestType clientRequestType = new ClientRequestManager.ClientRequestType(system);
    clientRequestType.Type = type;
    clientRequestType.ShouldRetryOnError = clientRequestConfig1.ShouldRetryOnError;
    clientRequestType.SubID = subID;
    clientRequestType.Body = ProtobufUtil.ToByteArray(body);
    clientRequestType.Phase = requestPhase;
    clientRequestType.SendCount = 0U;
    clientRequestType.RequestNotHandledCount = 0U;
    clientRequestType.RequestId = this.GetNextRequestId();
    if (clientRequestType.Phase == RequestPhase.STARTUP)
      system.Phases.StartUp.PendingSend.Enqueue(clientRequestType);
    else
      system.Phases.Running.PendingSend.Enqueue(clientRequestType);
    return true;
  }

  private ClientRequestManager.SystemChannel GetOrCreateSystem(int systemId)
  {
    ClientRequestManager.SystemChannel systemChannel1 = (ClientRequestManager.SystemChannel) null;
    if (this.m_state.m_systems.Systems.TryGetValue(systemId, out systemChannel1))
      return systemChannel1;
    ClientRequestManager.SystemChannel systemChannel2 = new ClientRequestManager.SystemChannel();
    systemChannel2.SystemId = systemId;
    this.m_state.m_systems.Systems[systemId] = systemChannel2;
    return systemChannel2;
  }

  private uint GenerateContextId()
  {
    return ++this.m_nextContexId;
  }

  private void NotifyResponseReceivedImpl(PegasusPacket packet)
  {
    uint context = (uint) packet.Context;
    ClientRequestManager.ClientRequestType clientRequest = this.GetClientRequest(context, "received_response", true);
    if (clientRequest == null)
    {
      if (packet.Context != 0 && this.GetDroppedRequest(context, "received_response", true))
        return;
      this.m_state.m_responsesPendingDelivery.Enqueue(packet);
    }
    else
    {
      switch (packet.Type)
      {
        case 315:
          this.ProcessSubscribeResponse(packet, clientRequest);
          break;
        case 328:
          this.ProcessClientRequestResponse(packet, clientRequest);
          break;
        default:
          this.ProcessResponse(packet, clientRequest);
          break;
      }
    }
  }

  private void NotifyStartupSequenceCompleteImpl()
  {
    this.m_state.m_runningPhaseEnabled = true;
  }

  private void NotifyLoginSequenceCompletedImpl()
  {
    this.m_state.m_loginCompleteNotificationReceived = true;
  }

  private uint SendToUtil(ClientRequestManager.ClientRequestType request)
  {
    uint contextId = this.GenerateContextId();
    byte[] utilPacketBytes = request.GetUtilPacketBytes();
    BattleNet.SendUtilPacket(request.Type, request.System.SystemId, utilPacketBytes, utilPacketBytes.Length, request.SubID, (int) contextId, request.System.Route);
    request.Context = contextId;
    request.SendTime = DateTime.Now;
    ++request.SendCount;
    this.AddRequestToPendingResponse(request, "send_to_util");
    string str = !request.IsSubsribeRequest ? request.Phase.ToString() : "SUBSCRIBE";
    return contextId;
  }

  private void MoveRequestsFromPendingResponseToSend(ClientRequestManager.SystemChannel system, RequestPhase phase, ClientRequestManager.PendingMapType pendingMap)
  {
    List<uint> uintList1 = new List<uint>();
    List<uint> uintList2 = new List<uint>();
    using (Map<uint, ClientRequestManager.ClientRequestType>.Enumerator enumerator = this.m_state.m_activePendingResponseMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<uint, ClientRequestManager.ClientRequestType> current = enumerator.Current;
        uint key = current.Key;
        ClientRequestManager.ClientRequestType clientRequestType = current.Value;
        if (clientRequestType.System.SystemId == system.SystemId && phase == clientRequestType.Phase)
        {
          if (clientRequestType.ShouldRetryOnError)
            uintList1.Add(key);
          else
            uintList2.Add(key);
        }
      }
    }
    string reason = "move_pending_response_to_pending_send";
    using (List<uint>.Enumerator enumerator = uintList1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        uint current = enumerator.Current;
        ClientRequestManager.ClientRequestType clientRequest = this.GetClientRequest(current, reason, true);
        pendingMap.PendingSend.Enqueue(clientRequest);
        this.m_state.m_ignorePendingResponseMap.Add(current);
      }
    }
    using (List<uint>.Enumerator enumerator = uintList2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        uint current = enumerator.Current;
        this.GetClientRequest(current, reason, true);
        this.m_state.m_ignorePendingResponseMap.Add(current);
      }
    }
  }

  private uint GetNextRequestId()
  {
    return ++this.m_nextRequestId;
  }

  private void SendSubscriptionRequest(ClientRequestManager.SystemChannel system)
  {
    int systemId = system.SystemId;
    if ((long) system.Route == 0L)
    {
      this.MoveRequestsFromPendingResponseToSend(system, RequestPhase.STARTUP, system.Phases.StartUp);
      this.MoveRequestsFromPendingResponseToSend(system, RequestPhase.RUNNING, system.Phases.Running);
    }
    ClientRequestManager.ClientRequestType request = new ClientRequestManager.ClientRequestType(system);
    request.Type = 314;
    request.SubID = 0;
    request.Body = ProtobufUtil.ToByteArray((IProtoBuf) this.m_subscribePacket);
    request.RequestId = this.GetNextRequestId();
    request.IsSubsribeRequest = true;
    system.SubscriptionStatus.CurrentState = ClientRequestManager.SubscriptionStatusType.State.PENDING_RESPONSE;
    system.SubscriptionStatus.LastSend = DateTime.Now;
    system.SubscriptionStatus.ContexId = this.SendToUtil(request);
    ++system.SubscribeAttempt;
    ++this.m_state.m_subscribePacketsSent;
  }

  private void ScheduleResubscribeWithNewRoute(ClientRequestManager.SystemChannel system)
  {
    system.Route = 0UL;
    system.SubscriptionStatus.CurrentState = ClientRequestManager.SubscriptionStatusType.State.PENDING_SEND;
  }

  private void ScheduleResubscribeKeepRoute(ClientRequestManager.SystemChannel system)
  {
    system.SubscriptionStatus.CurrentState = ClientRequestManager.SubscriptionStatusType.State.PENDING_SEND;
  }

  private void TerminateImpl()
  {
    Unsubscribe packet = new Unsubscribe();
    using (Map<int, ClientRequestManager.SystemChannel>.Enumerator enumerator = this.m_state.m_systems.Systems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ClientRequestManager.SystemChannel systemChannel = enumerator.Current.Value;
        if (systemChannel.SubscriptionStatus.CurrentState == ClientRequestManager.SubscriptionStatusType.State.SUBSCRIBED && (long) systemChannel.Route != 0L)
          Network.SendUnsubcribeRequest(packet, systemChannel.SystemId);
      }
    }
  }

  private bool UpdateStateSubscribeImpl(ClientRequestManager.SystemChannel system)
  {
    switch (system.SubscriptionStatus.CurrentState)
    {
      case ClientRequestManager.SubscriptionStatusType.State.PENDING_SEND:
        return this.ProcessSubscribeStatePendingSend(system);
      case ClientRequestManager.SubscriptionStatusType.State.PENDING_RESPONSE:
        return this.ProcessSubscribeStatePendingResponse(system);
      case ClientRequestManager.SubscriptionStatusType.State.SUBSCRIBED:
        return this.ProcessSubscribeStateSubscribed(system);
      default:
        return system.SubscriptionStatus.CurrentState == ClientRequestManager.SubscriptionStatusType.State.SUBSCRIBED;
    }
  }

  private bool ProcessSubscribeStatePendingSend(ClientRequestManager.SystemChannel system)
  {
    if ((DateTime.Now - system.SubscriptionStatus.LastSend).TotalSeconds > (double) system.PendingSubscribeTimeout)
      this.SendSubscriptionRequest(system);
    return (long) system.Route != 0L;
  }

  private bool ProcessSubscribeStatePendingResponse(ClientRequestManager.SystemChannel system)
  {
    if ((DateTime.Now - system.SubscriptionStatus.LastSend).TotalSeconds > (double) system.PendingSubscribeTimeout)
      this.ScheduleResubscribeKeepRoute(system);
    return (long) system.Route != 0L;
  }

  private int CountPendingResponsesForSystemId(ClientRequestManager.SystemChannel system)
  {
    int num = 0;
    using (Map<uint, ClientRequestManager.ClientRequestType>.Enumerator enumerator = this.m_state.m_activePendingResponseMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.System.SystemId == system.SystemId)
          ++num;
      }
    }
    return num;
  }

  private bool ProcessSubscribeStateSubscribed(ClientRequestManager.SystemChannel system)
  {
    if ((ulong) (DateTime.Now - system.SubscriptionStatus.SubscribedTime).TotalSeconds < system.KeepAliveSecs || this.CountPendingResponsesForSystemId(system) > 0 || system.KeepAliveSecs <= 0UL)
      return true;
    system.SubscriptionStatus.CurrentState = ClientRequestManager.SubscriptionStatusType.State.PENDING_SEND;
    return true;
  }

  private void ProcessSubscribeResponse(PegasusPacket packet, ClientRequestManager.ClientRequestType request)
  {
    if (!(packet.Body is SubscribeResponse))
      return;
    ClientRequestManager.SystemChannel system = request.System;
    int systemId = system.SystemId;
    SubscribeResponse body = (SubscribeResponse) packet.Body;
    if (body.Result == SubscribeResponse.ResponseResult.FAILED_UNAVAILABLE)
    {
      this.ScheduleResubscribeWithNewRoute(system);
    }
    else
    {
      system.SubscriptionStatus.CurrentState = ClientRequestManager.SubscriptionStatusType.State.SUBSCRIBED;
      system.SubscriptionStatus.SubscribedTime = DateTime.Now;
      system.Route = body.Route;
      system.CurrentPhase = RequestPhase.STARTUP;
      system.SubscribeAttempt = 0U;
      system.KeepAliveSecs = body.KeepAliveSecs;
      system.MaxResubscribeAttempts = body.MaxResubscribeAttempts;
      system.PendingResponseTimeout = body.PendingResponseTimeout;
      system.PendingSubscribeTimeout = body.PendingSubscribeTimeout;
      this.m_state.m_responsesPendingDelivery.Enqueue(packet);
      ++system.m_subscribePacketsReceived;
    }
  }

  private void ProcessClientRequests(ClientRequestManager.SystemChannel system)
  {
    ClientRequestManager.PendingMapType pendingMapType = system.CurrentPhase != RequestPhase.STARTUP ? system.Phases.Running : system.Phases.StartUp;
    using (Map<uint, ClientRequestManager.ClientRequestType>.Enumerator enumerator = this.m_state.m_activePendingResponseMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ClientRequestManager.ClientRequestType clientRequestType = enumerator.Current.Value;
        if (!clientRequestType.IsSubsribeRequest && clientRequestType.System != null && (clientRequestType.System.SystemId == system.SystemId && (long) system.PendingResponseTimeout != 0L) && (DateTime.Now - clientRequestType.SendTime).TotalSeconds >= (double) system.PendingResponseTimeout)
        {
          this.ScheduleResubscribeWithNewRoute(system);
          return;
        }
      }
    }
    bool flag = pendingMapType.PendingSend.Count > 0;
    while (pendingMapType.PendingSend.Count > 0)
    {
      int util = (int) this.SendToUtil(pendingMapType.PendingSend.Dequeue());
    }
    if (flag || system.CurrentPhase != RequestPhase.STARTUP || !this.m_state.m_runningPhaseEnabled)
      return;
    system.CurrentPhase = RequestPhase.RUNNING;
  }

  private void ProcessResponse(PegasusPacket packet, ClientRequestManager.ClientRequestType clientRequest)
  {
    if (packet.Type == 254)
      return;
    this.m_state.m_responsesPendingDelivery.Enqueue(packet);
  }

  private ClientRequestManager.ClientRequestType GetClientRequest(uint contextId, string reason, bool removeIfFound = true)
  {
    if ((int) contextId == 0)
      return (ClientRequestManager.ClientRequestType) null;
    ClientRequestManager.ClientRequestType clientRequestType;
    if (!this.m_state.m_activePendingResponseMap.TryGetValue(contextId, out clientRequestType))
    {
      if (!this.GetDroppedRequest(contextId, "get_client_request", false) || this.GetPendingSendRequest(contextId, "get_client_request", false) != null)
        ;
      return (ClientRequestManager.ClientRequestType) null;
    }
    if (removeIfFound)
      this.m_state.m_activePendingResponseMap.Remove(contextId);
    return clientRequestType;
  }

  private void AddRequestToPendingResponse(ClientRequestManager.ClientRequestType clientRequest, string reason)
  {
    if (this.m_state.m_activePendingResponseMap.ContainsKey(clientRequest.Context))
      return;
    this.m_state.m_activePendingResponseMap.Add(clientRequest.Context, clientRequest);
  }

  private bool GetDroppedRequest(uint contextId, string reason, bool removeIfFound = true)
  {
    if (!this.m_state.m_ignorePendingResponseMap.Contains(contextId) || !removeIfFound)
      return false;
    this.m_state.m_ignorePendingResponseMap.Remove(contextId);
    return true;
  }

  private ClientRequestManager.ClientRequestType GetPendingSendRequestForPhase(uint contextId, bool removeIfFound, ClientRequestManager.PendingMapType pendingMap)
  {
    ClientRequestManager.ClientRequestType clientRequestType = (ClientRequestManager.ClientRequestType) null;
    Queue<ClientRequestManager.ClientRequestType> clientRequestTypeQueue = new Queue<ClientRequestManager.ClientRequestType>();
    using (Queue<ClientRequestManager.ClientRequestType>.Enumerator enumerator = pendingMap.PendingSend.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ClientRequestManager.ClientRequestType current = enumerator.Current;
        if (clientRequestType == null && (int) current.Context == (int) contextId)
        {
          clientRequestType = current;
          if (!removeIfFound)
            clientRequestTypeQueue.Enqueue(current);
        }
        else
          clientRequestTypeQueue.Enqueue(current);
      }
    }
    pendingMap.PendingSend = clientRequestTypeQueue;
    return clientRequestType;
  }

  private ClientRequestManager.ClientRequestType GetPendingSendRequest(uint contextId, string reason, bool removeIfFound = true)
  {
    ClientRequestManager.ClientRequestType clientRequestType = (ClientRequestManager.ClientRequestType) null;
    using (Map<int, ClientRequestManager.SystemChannel>.Enumerator enumerator = this.m_state.m_systems.Systems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ClientRequestManager.SystemChannel systemChannel = enumerator.Current.Value;
        clientRequestType = this.GetPendingSendRequestForPhase(contextId, removeIfFound, systemChannel.Phases.Running);
        if (clientRequestType == null)
          clientRequestType = this.GetPendingSendRequestForPhase(contextId, removeIfFound, systemChannel.Phases.StartUp);
        else
          break;
      }
    }
    return clientRequestType;
  }

  public class ClientRequestConfig
  {
    public bool ShouldRetryOnError { get; set; }

    public int RequestedSystem { get; set; }
  }

  private class ClientRequestType
  {
    public int Type;
    public int SubID;
    public byte[] Body;
    public uint Context;
    public RequestPhase Phase;
    public uint SendCount;
    public uint RequestNotHandledCount;
    public DateTime SendTime;
    public uint RequestId;
    public bool IsSubsribeRequest;
    public ClientRequestManager.SystemChannel System;
    public bool ShouldRetryOnError;

    public ClientRequestType(ClientRequestManager.SystemChannel system)
    {
      this.System = system;
    }

    public byte[] GetUtilPacketBytes()
    {
      RpcHeader rpcHeader = new RpcHeader();
      rpcHeader.Type = (ulong) this.Type;
      if (this.SendCount > 0U)
        rpcHeader.RetryCount = (ulong) this.SendCount;
      if (this.RequestNotHandledCount > 0U)
        rpcHeader.RequestNotHandledCount = (ulong) this.RequestNotHandledCount;
      RpcMessage rpcMessage = new RpcMessage();
      rpcMessage.RpcHeader = rpcHeader;
      if (this.Body != null && this.Body.Length > 0)
        rpcMessage.MessageBody = this.Body;
      return ProtobufUtil.ToByteArray((IProtoBuf) rpcMessage);
    }
  }

  private class SubscriptionStatusType
  {
    public DateTime LastSend = DateTime.MinValue;
    public ClientRequestManager.SubscriptionStatusType.State CurrentState;
    public DateTime SubscribedTime;
    public uint ContexId;

    public enum State
    {
      PENDING_SEND,
      PENDING_RESPONSE,
      SUBSCRIBED,
    }
  }

  private class PendingMapType
  {
    public Queue<ClientRequestManager.ClientRequestType> PendingSend = new Queue<ClientRequestManager.ClientRequestType>();
  }

  private class PhaseMapType
  {
    public ClientRequestManager.PendingMapType StartUp = new ClientRequestManager.PendingMapType();
    public ClientRequestManager.PendingMapType Running = new ClientRequestManager.PendingMapType();
  }

  private class SystemChannel
  {
    public ClientRequestManager.PhaseMapType Phases = new ClientRequestManager.PhaseMapType();
    public ClientRequestManager.SubscriptionStatusType SubscriptionStatus = new ClientRequestManager.SubscriptionStatusType();
    public ulong PendingSubscribeTimeout = 15;
    public ulong Route;
    public RequestPhase CurrentPhase;
    public ulong KeepAliveSecs;
    public ulong MaxResubscribeAttempts;
    public ulong PendingResponseTimeout;
    public uint SubscribeAttempt;
    public bool WasEverInRunningPhase;
    public int SystemId;
    public uint m_subscribePacketsReceived;
  }

  private class SystemMap
  {
    public Map<int, ClientRequestManager.SystemChannel> Systems = new Map<int, ClientRequestManager.SystemChannel>();
  }

  private class InternalState
  {
    public Queue<PegasusPacket> m_responsesPendingDelivery = new Queue<PegasusPacket>();
    public ClientRequestManager.SystemMap m_systems = new ClientRequestManager.SystemMap();
    public Map<uint, ClientRequestManager.ClientRequestType> m_activePendingResponseMap = new Map<uint, ClientRequestManager.ClientRequestType>();
    public HashSet<uint> m_ignorePendingResponseMap = new HashSet<uint>();
    public uint m_subscribePacketsSent;
    public bool m_loginCompleteNotificationReceived;
    public bool m_runningPhaseEnabled;
    public bool m_receivedErrorSignal;
  }
}
