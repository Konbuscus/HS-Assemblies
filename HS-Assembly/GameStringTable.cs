﻿// Decompiled with JetBrains decompiler
// Type: GameStringTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameStringTable
{
  private Map<string, string> m_table = new Map<string, string>();
  public const string KEY_FIELD_NAME = "TAG";
  public const string VALUE_FIELD_NAME = "TEXT";
  private GameStringCategory m_category;

  public bool Load(GameStringCategory cat)
  {
    string pathWithLoadOrder1 = GameStringTable.GetFilePathWithLoadOrder(cat, new GameStringTable.FilePathFromCategoryCallback(GameStringTable.GetFilePathFromCategory));
    string pathWithLoadOrder2 = GameStringTable.GetFilePathWithLoadOrder(cat, new GameStringTable.FilePathFromCategoryCallback(GameStringTable.GetAudioFilePathFromCategory));
    return this.Load(cat, pathWithLoadOrder1, pathWithLoadOrder2);
  }

  public bool Load(GameStringCategory cat, Locale locale)
  {
    string pathFromCategory1 = GameStringTable.GetFilePathFromCategory(cat, locale);
    string pathFromCategory2 = GameStringTable.GetAudioFilePathFromCategory(cat, locale);
    return this.Load(cat, pathFromCategory1, pathFromCategory2);
  }

  public bool Load(GameStringCategory cat, string path, string audioPath)
  {
    this.m_category = GameStringCategory.INVALID;
    this.m_table.Clear();
    List<GameStringTable.Entry> entries1 = (List<GameStringTable.Entry>) null;
    List<GameStringTable.Entry> entries2 = (List<GameStringTable.Entry>) null;
    GameStringTable.Header header1;
    if (File.Exists(path) && !GameStringTable.LoadFile(path, out header1, out entries1))
    {
      Error.AddDevWarning("GameStrings Error", "GameStringTable.Load() - Failed to load {0} for cat {1}.", (object) path, (object) cat);
      return false;
    }
    GameStringTable.Header header2;
    if (File.Exists(audioPath) && !GameStringTable.LoadFile(audioPath, out header2, out entries2))
    {
      Error.AddDevWarning("GameStrings Error", "GameStringTable.Load() - Failed to load {0} for cat {1}.", (object) audioPath, (object) cat);
      return false;
    }
    if (entries1 != null && entries2 != null)
      this.BuildTable(path, entries1, audioPath, entries2);
    else if (entries1 != null)
      this.BuildTable(path, entries1);
    else if (entries2 != null)
    {
      this.BuildTable(audioPath, entries2);
    }
    else
    {
      Error.AddDevWarning("GameStrings Error", "GameStringTable.Load() - There are no entries for cat {0}.", (object) cat);
      return false;
    }
    this.m_category = cat;
    return true;
  }

  public string Get(string key)
  {
    string str;
    this.m_table.TryGetValue(key, out str);
    return str;
  }

  public Map<string, string> GetAll()
  {
    return this.m_table;
  }

  public GameStringCategory GetCategory()
  {
    return this.m_category;
  }

  public static bool LoadFile(string path, out GameStringTable.Header header, out List<GameStringTable.Entry> entries)
  {
    header = (GameStringTable.Header) null;
    entries = (List<GameStringTable.Entry>) null;
    string[] lines;
    try
    {
      lines = File.ReadAllLines(path);
    }
    catch (Exception ex)
    {
      Debug.LogWarning((object) string.Format("GameStringTable.LoadFile() - Failed to read \"{0}\".\n\nException: {1}", (object) path, (object) ex.Message));
      return false;
    }
    header = GameStringTable.LoadFileHeader(lines);
    if (header == null)
    {
      Debug.LogWarning((object) string.Format("GameStringTable.LoadFile() - \"{0}\" had a malformed header.", (object) path));
      return false;
    }
    entries = GameStringTable.LoadFileEntries(path, header, lines);
    return true;
  }

  private static string GetFilePathWithLoadOrder(GameStringCategory cat, GameStringTable.FilePathFromCategoryCallback pathCallback)
  {
    foreach (Locale locale in Localization.GetLoadOrder(false))
    {
      string path = pathCallback(cat, locale);
      if (File.Exists(path))
        return path;
    }
    return (string) null;
  }

  private static string GetFilePathFromCategory(GameStringCategory cat, Locale locale)
  {
    string fileName = string.Format("{0}.txt", (object) cat);
    return GameStrings.GetAssetPath(locale, fileName);
  }

  private static string GetAudioFilePathFromCategory(GameStringCategory cat, Locale locale)
  {
    string fileName = string.Format("{0}_AUDIO.txt", (object) cat);
    return GameStrings.GetAssetPath(locale, fileName);
  }

  private static GameStringTable.Header LoadFileHeader(string[] lines)
  {
    GameStringTable.Header header = new GameStringTable.Header();
    for (int index1 = 0; index1 < lines.Length; ++index1)
    {
      string line = lines[index1];
      if (line.Length != 0 && !line.StartsWith("#"))
      {
        string[] strArray = line.Split('\t');
        for (int index2 = 0; index2 < strArray.Length; ++index2)
        {
          string str = strArray[index2];
          if (str == "TAG")
          {
            header.m_keyIndex = index2;
            if (header.m_valueIndex >= 0)
              break;
          }
          else if (str == "TEXT")
          {
            header.m_valueIndex = index2;
            if (header.m_keyIndex >= 0)
              break;
          }
        }
        if (header.m_keyIndex < 0 && header.m_valueIndex < 0)
          return (GameStringTable.Header) null;
        header.m_entryStartIndex = index1 + 1;
        return header;
      }
    }
    return (GameStringTable.Header) null;
  }

  private static List<GameStringTable.Entry> LoadFileEntries(string path, GameStringTable.Header header, string[] lines)
  {
    List<GameStringTable.Entry> entryList = new List<GameStringTable.Entry>(lines.Length);
    int num = Mathf.Max(header.m_keyIndex, header.m_valueIndex);
    for (int entryStartIndex = header.m_entryStartIndex; entryStartIndex < lines.Length; ++entryStartIndex)
    {
      string line = lines[entryStartIndex];
      if (line.Length != 0 && !line.StartsWith("#"))
      {
        string[] strArray = line.Split('\t');
        if (strArray.Length <= num)
          Error.AddDevWarning("GameStrings Error", "GameStringTable.LoadFileEntries() - line {0} in \"{1}\" is malformed", (object) (entryStartIndex + 1), (object) path);
        else
          entryList.Add(new GameStringTable.Entry()
          {
            m_key = strArray[header.m_keyIndex],
            m_value = TextUtils.DecodeWhitespaces(strArray[header.m_valueIndex])
          });
      }
    }
    return entryList;
  }

  private void BuildTable(string path, List<GameStringTable.Entry> entries)
  {
    int count = entries.Count;
    this.m_table = new Map<string, string>(count);
    if (count == 0)
      return;
    if (ApplicationMgr.IsInternal())
      GameStringTable.CheckConflicts(path, entries);
    using (List<GameStringTable.Entry>.Enumerator enumerator = entries.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameStringTable.Entry current = enumerator.Current;
        this.m_table[current.m_key] = current.m_value;
      }
    }
  }

  private void BuildTable(string path, List<GameStringTable.Entry> entries, string audioPath, List<GameStringTable.Entry> audioEntries)
  {
    int count = entries.Count + audioEntries.Count;
    this.m_table = new Map<string, string>(count);
    if (count == 0)
      return;
    if (ApplicationMgr.IsInternal())
      GameStringTable.CheckConflicts(path, entries);
    using (List<GameStringTable.Entry>.Enumerator enumerator = entries.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameStringTable.Entry current = enumerator.Current;
        this.m_table[current.m_key] = current.m_value;
      }
    }
    using (List<GameStringTable.Entry>.Enumerator enumerator = audioEntries.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameStringTable.Entry current = enumerator.Current;
        this.m_table[current.m_key] = current.m_value;
      }
    }
  }

  private static void CheckConflicts(string path, List<GameStringTable.Entry> entries)
  {
    if (entries.Count == 0)
      return;
    for (int index1 = 0; index1 < entries.Count; ++index1)
    {
      string key1 = entries[index1].m_key;
      for (int index2 = index1 + 1; index2 < entries.Count; ++index2)
      {
        string key2 = entries[index2].m_key;
        if (string.Equals(key1, key2, StringComparison.OrdinalIgnoreCase))
          Error.AddDevWarning("GameStrings Error", string.Format("GameStringTable.CheckConflicts() - Tag {0} appears more than once in {1}. All tags must be unique.", (object) key1, (object) path));
      }
    }
  }

  private static void CheckConflicts(string path1, List<GameStringTable.Entry> entries1, string path2, List<GameStringTable.Entry> entries2)
  {
    if (entries1.Count == 0)
      return;
    GameStringTable.CheckConflicts(path1, entries1);
    if (entries2.Count == 0)
      return;
    GameStringTable.CheckConflicts(path2, entries2);
    for (int index1 = 0; index1 < entries1.Count; ++index1)
    {
      string key1 = entries1[index1].m_key;
      for (int index2 = 0; index2 < entries2.Count; ++index2)
      {
        string key2 = entries2[index2].m_key;
        if (string.Equals(key1, key2, StringComparison.OrdinalIgnoreCase))
          Error.AddDevWarning("GameStrings Error", string.Format("GameStringTable.CheckConflicts() - Tag {0} is used in {1} and {2}. All tags must be unique.", (object) key1, (object) path1, (object) path2));
      }
    }
  }

  public class Entry
  {
    public string m_key;
    public string m_value;
  }

  public class Header
  {
    public int m_entryStartIndex = -1;
    public int m_keyIndex = -1;
    public int m_valueIndex = -1;
  }

  private delegate string FilePathFromCategoryCallback(GameStringCategory cat, Locale locale);
}
