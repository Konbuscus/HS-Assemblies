﻿// Decompiled with JetBrains decompiler
// Type: LOE10_Giantfin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LOE10_Giantfin : LOE_MissionEntity
{
  private int m_turnToPlayFoundLine = -1;
  private bool m_cardLinePlayed1;
  private bool m_cardLinePlayed2;
  private bool m_nyahLinePlayed;

  public override void StartGameplaySoundtracks()
  {
    MusicManager.Get().StartPlaylist(MusicPlaylistType.InGame_LOE_Wing3);
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOEA10_1_MIDDLEFIN");
    this.PreloadSound("VO_LOE10_NYAH_FINLEY");
    this.PreloadSound("VO_LOE_10_NYAH");
    this.PreloadSound("VO_LOE_10_RESPONSE");
    this.PreloadSound("VO_LOE_10_START_2");
    this.PreloadSound("VO_LOE_10_TURN1");
    this.PreloadSound("VO_LOE_10_WIN");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE10_Giantfin.\u003CHandleMissionEventWithTiming\u003Ec__Iterator172() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE10_Giantfin.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator173() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_LOE_10_RESPONSE",
            m_stringTag = "VO_LOE_10_RESPONSE"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE10_Giantfin.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator174() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE10_Giantfin.\u003CHandleGameOverWithTiming\u003Ec__Iterator175() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
