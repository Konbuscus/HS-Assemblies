﻿// Decompiled with JetBrains decompiler
// Type: DeckRuleset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DeckRuleset
{
  private int m_id;
  private List<DeckRule> m_rules;
  private static DeckRuleset s_standardRuleset;
  private static DeckRuleset s_wildRuleset;

  public int Id
  {
    get
    {
      return this.m_id;
    }
  }

  public List<DeckRule> Rules
  {
    get
    {
      return this.m_rules;
    }
  }

  public static DeckRuleset GetDeckRuleset(int id)
  {
    if (id == 1)
      return DeckRuleset.GetWildRuleset();
    if (id == 2)
      return DeckRuleset.GetStandardRuleset();
    return DeckRuleset.GetDeckRulesetFromDBF(id);
  }

  private static DeckRuleset GetDeckRulesetFromDBF(int id)
  {
    if (id <= 0)
      return (DeckRuleset) null;
    if (!GameDbf.DeckRuleset.HasRecord(id))
    {
      Debug.LogErrorFormat("DeckRuleset not found for id {0}", (object) id);
      return (DeckRuleset) null;
    }
    DeckRuleset deckRuleset = new DeckRuleset();
    deckRuleset.m_id = id;
    deckRuleset.m_rules = new List<DeckRule>();
    foreach (DeckRulesetRuleDbfRecord record in GameDbf.GetIndex().GetRulesForDeckRuleset(id))
    {
      DeckRule fromDbf = DeckRule.CreateFromDBF(record);
      deckRuleset.m_rules.Add(fromDbf);
    }
    deckRuleset.m_rules.Sort(new Comparison<DeckRule>(DeckRuleViolation.SortComparison_Rule));
    return deckRuleset;
  }

  public static DeckRuleset GetStandardRuleset()
  {
    if (DeckRuleset.s_standardRuleset == null)
      DeckRuleset.s_standardRuleset = DeckRuleset.BuildStandardRuleset();
    return DeckRuleset.s_standardRuleset;
  }

  private static DeckRuleset BuildStandardRuleset()
  {
    int id = 2;
    if (GameDbf.DeckRuleset.HasRecord(id))
      return DeckRuleset.GetDeckRulesetFromDBF(id);
    DeckRuleset deckRuleset = new DeckRuleset();
    deckRuleset.m_id = id;
    deckRuleset.m_rules = new List<DeckRule>();
    DeckRule_DeckSize deckRuleDeckSize = new DeckRule_DeckSize(30);
    DeckRule_CountCopiesOfEachCard copiesOfEachCard1 = new DeckRule_CountCopiesOfEachCard(0, 0, 2);
    DeckRule_CountCopiesOfEachCard copiesOfEachCard2 = new DeckRule_CountCopiesOfEachCard(7, 0, 1);
    DeckRule_PlayerOwnsEachCopy playerOwnsEachCopy = new DeckRule_PlayerOwnsEachCopy();
    DeckRule_IsNotRotated ruleIsNotRotated = new DeckRule_IsNotRotated();
    deckRuleset.m_rules.Add((DeckRule) playerOwnsEachCopy);
    deckRuleset.m_rules.Add((DeckRule) deckRuleDeckSize);
    deckRuleset.m_rules.Add((DeckRule) copiesOfEachCard1);
    deckRuleset.m_rules.Add((DeckRule) copiesOfEachCard2);
    deckRuleset.m_rules.Add((DeckRule) ruleIsNotRotated);
    deckRuleset.m_rules.Sort(new Comparison<DeckRule>(DeckRuleViolation.SortComparison_Rule));
    return deckRuleset;
  }

  public static DeckRuleset GetWildRuleset()
  {
    if (DeckRuleset.s_wildRuleset == null)
      DeckRuleset.s_wildRuleset = DeckRuleset.BuildWildRuleset();
    return DeckRuleset.s_wildRuleset;
  }

  private static DeckRuleset BuildWildRuleset()
  {
    int id = 1;
    if (GameDbf.DeckRuleset.HasRecord(id))
      return DeckRuleset.GetDeckRulesetFromDBF(id);
    DeckRuleset deckRuleset = new DeckRuleset();
    deckRuleset.m_id = id;
    deckRuleset.m_rules = new List<DeckRule>();
    DeckRule_DeckSize deckRuleDeckSize = new DeckRule_DeckSize(30);
    DeckRule_CountCopiesOfEachCard copiesOfEachCard1 = new DeckRule_CountCopiesOfEachCard(0, 0, 2);
    DeckRule_CountCopiesOfEachCard copiesOfEachCard2 = new DeckRule_CountCopiesOfEachCard(7, 0, 1);
    DeckRule_PlayerOwnsEachCopy playerOwnsEachCopy = new DeckRule_PlayerOwnsEachCopy();
    deckRuleset.m_rules.Add((DeckRule) deckRuleDeckSize);
    deckRuleset.m_rules.Add((DeckRule) copiesOfEachCard1);
    deckRuleset.m_rules.Add((DeckRule) copiesOfEachCard2);
    deckRuleset.m_rules.Add((DeckRule) playerOwnsEachCopy);
    deckRuleset.m_rules.Sort(new Comparison<DeckRule>(DeckRuleViolation.SortComparison_Rule));
    return deckRuleset;
  }

  public bool Filter(EntityDef entity)
  {
    using (List<DeckRule>.Enumerator enumerator = this.m_rules.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (!enumerator.Current.Filter(entity))
          return false;
      }
    }
    return true;
  }

  public bool CanAddToDeck(EntityDef def, TAG_PREMIUM premium, CollectionDeck deck, params DeckRule.RuleType[] ignoreRules)
  {
    RuleInvalidReason reason;
    DeckRule brokenRule;
    return this.CanAddToDeck(def, premium, deck, out reason, out brokenRule, ignoreRules);
  }

  public bool CanAddToDeck(EntityDef def, TAG_PREMIUM premium, CollectionDeck deck, out RuleInvalidReason reason, out DeckRule brokenRule, params DeckRule.RuleType[] ignoreRules)
  {
    reason = (RuleInvalidReason) null;
    brokenRule = (DeckRule) null;
    using (List<DeckRule>.Enumerator enumerator = this.m_rules.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckRule current = enumerator.Current;
        if ((ignoreRules == null || !((IEnumerable<DeckRule.RuleType>) ignoreRules).Contains<DeckRule.RuleType>(current.Type)) && !current.CanAddToDeck(def, premium, deck, out reason))
        {
          brokenRule = current;
          return false;
        }
      }
    }
    return true;
  }

  public bool CanAddToDeck(EntityDef def, TAG_PREMIUM premium, CollectionDeck deck, out List<RuleInvalidReason> reasons, out List<DeckRule> brokenRules, params DeckRule.RuleType[] ignoreRules)
  {
    reasons = new List<RuleInvalidReason>();
    brokenRules = new List<DeckRule>();
    using (List<DeckRule>.Enumerator enumerator = this.m_rules.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckRule current = enumerator.Current;
        RuleInvalidReason reason;
        if ((ignoreRules == null || !((IEnumerable<DeckRule.RuleType>) ignoreRules).Contains<DeckRule.RuleType>(current.Type)) && !current.CanAddToDeck(def, premium, deck, out reason))
        {
          reasons.Add(reason);
          brokenRules.Add(current);
        }
      }
    }
    return brokenRules.Count == 0;
  }

  public bool IsDeckValid(CollectionDeck deck)
  {
    IList<DeckRuleViolation> violations;
    return this.IsDeckValid(deck, out violations);
  }

  public bool IsDeckValid(CollectionDeck deck, out IList<DeckRuleViolation> violations)
  {
    List<DeckRuleViolation> deckRuleViolationList = new List<DeckRuleViolation>();
    violations = (IList<DeckRuleViolation>) deckRuleViolationList;
    List<RuleInvalidReason> reasons = new List<RuleInvalidReason>();
    bool flag1 = true;
    using (List<DeckRule>.Enumerator enumerator = this.m_rules.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckRule current = enumerator.Current;
        RuleInvalidReason reason;
        bool flag2 = current.IsDeckValid(deck, out reason);
        if (!flag2)
        {
          reasons.Add(reason);
          DeckRuleViolation deckRuleViolation = new DeckRuleViolation(current, reason.DisplayError);
          violations.Add(deckRuleViolation);
          flag1 = false;
        }
        Log.DeckRuleset.Print("validating rule={0} deck={1} result={2} reason={3}", (object) current, (object) deck, (object) flag2, (object) reason);
      }
    }
    deckRuleViolationList.Sort(new Comparison<DeckRuleViolation>(DeckRuleViolation.SortComparison_Violation));
    this.CollapseSpecialBrokenRules(violations, reasons);
    return flag1;
  }

  private void CollapseSpecialBrokenRules(IList<DeckRuleViolation> violations, List<RuleInvalidReason> reasons)
  {
    if (reasons.Count <= 1)
      return;
    DeckRule rule1 = (DeckRule) null;
    int countParam = 0;
    List<int> intList = (List<int>) null;
    for (int index = 0; index < violations.Count; ++index)
    {
      DeckRule rule2 = violations[index].Rule;
      if (rule2.Type == DeckRule.RuleType.PLAYER_OWNS_EACH_COPY && !rule2.RuleIsNot)
      {
        if (intList == null)
          intList = new List<int>();
        if (rule1 == null)
          rule1 = rule2;
        intList.Add(index);
        countParam += reasons[index].CountParam;
      }
      else if (rule2.Type == DeckRule.RuleType.DECK_SIZE && reasons[index].IsMinimum)
      {
        if (intList == null)
          intList = new List<int>();
        if (rule1 == null)
          rule1 = rule2;
        intList.Add(index);
        countParam += reasons[index].CountParam;
      }
    }
    if (intList == null || intList.Count <= 1)
      return;
    for (int index1 = intList != null ? intList.Count - 1 : -1; index1 >= 0; --index1)
    {
      int index2 = intList[index1];
      violations.RemoveAt(index2);
      reasons.RemoveAt(index2);
    }
    string str = GameStrings.Format("GLUE_COLLECTION_DECK_RULE_MISSING_CARDS", (object) countParam);
    RuleInvalidReason ruleInvalidReason = new RuleInvalidReason(str, countParam, false);
    reasons.Add(ruleInvalidReason);
    DeckRuleViolation deckRuleViolation = new DeckRuleViolation(rule1, str);
    violations.Add(deckRuleViolation);
  }

  public int GetDeckSize()
  {
    DeckRule deckRule = this.m_rules != null ? this.m_rules.FirstOrDefault<DeckRule>((Func<DeckRule, bool>) (r => r is DeckRule_DeckSize)) : (DeckRule) null;
    if (deckRule != null)
      return ((DeckRule_DeckSize) deckRule).GetDeckSize();
    return 30;
  }

  public bool HasOwnershipOrRotatedRule()
  {
    return (this.m_rules != null ? this.m_rules.FirstOrDefault<DeckRule>((Func<DeckRule, bool>) (r =>
    {
      if (r.Type == DeckRule.RuleType.IS_NOT_ROTATED)
        return true;
      if (r.Type == DeckRule.RuleType.PLAYER_OWNS_EACH_COPY)
        return !r.RuleIsNot;
      return false;
    })) : (DeckRule) null) != null;
  }

  public int GetMaxCopiesOfCardAllowed(EntityDef entity)
  {
    int a = int.MaxValue;
    using (List<DeckRule>.Enumerator enumerator = this.m_rules.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckRule current = enumerator.Current;
        int maxCopies;
        if (current is DeckRule_CountCopiesOfEachCard && ((DeckRule_CountCopiesOfEachCard) current).GetMaxCopies(entity, out maxCopies))
          a = Mathf.Min(a, maxCopies);
      }
    }
    return a;
  }

  public enum DeckRulesetConstant
  {
    UnknownRuleset,
    WildRuleset,
    StandardRuleset,
  }
}
