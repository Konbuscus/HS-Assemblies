﻿// Decompiled with JetBrains decompiler
// Type: LightningCtrl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class LightningCtrl : MonoBehaviour
{
  public float lifetime = 1f;
  public float scale = 0.1f;
  public float speed = 1f;
  public GameObject mylightning;
  private GameObject lightningObj;
  public float position_X;
  public float position_Y;
  public float position_Z;
  public GameObject target;
  public GameObject destination;

  private void Start()
  {
  }

  private void Update()
  {
    if (!UniversalInputManager.Get().GetMouseButtonDown(0))
      return;
    this.Spawn(this.target.transform, this.destination.transform);
  }

  public void Spawn(Transform targetTransform, Transform destinationTransform)
  {
    this.lightningObj = (GameObject) Object.Instantiate((Object) this.mylightning, new Vector3(this.position_X, this.position_Y, this.position_Z), new Quaternion(0.0f, 0.0f, 0.0f, 0.0f));
    this.lightningObj.transform.localScale = new Vector3(this.scale, this.scale, this.scale);
    ElectroScript component = this.lightningObj.GetComponent<ElectroScript>();
    component.timers.timeToPowerUp = this.speed;
    component.prefabs.target.position = targetTransform.position;
    component.prefabs.destination.position = destinationTransform.position;
    this.StartCoroutine(this.DestroyLightning());
  }

  [DebuggerHidden]
  private IEnumerator DestroyLightning()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LightningCtrl.\u003CDestroyLightning\u003Ec__Iterator296() { \u003C\u003Ef__this = this };
  }
}
