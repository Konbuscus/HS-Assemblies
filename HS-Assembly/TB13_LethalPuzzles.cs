﻿// Decompiled with JetBrains decompiler
// Type: TB13_LethalPuzzles
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TB13_LethalPuzzles : MissionEntity
{
  private static readonly Dictionary<int, string> minionMsgs = new Dictionary<int, string>() { { 1, "TB_LETHALPUZZLES_START" }, { 2, "TB_LETHALPUZZLES_SUCCESS" } };
  private float popupDuration = 2.5f;
  private float popupScale = 2.5f;
  private HashSet<int> seen = new HashSet<int>();
  private Notification MyPopup;
  private Vector3 popUpPos;
  private string textID;
  private bool doPopup;
  private bool doLeftArrow;
  private bool doUpArrow;
  private bool doDownArrow;
  private float delayTime;

  public override void PreloadAssets()
  {
    this.PreloadSound("tutorial_mission_hero_coin_mouse_away");
  }

  public override bool ShouldDoAlternateMulliganIntro()
  {
    return true;
  }

  public override bool ShouldHandleCoin()
  {
    return false;
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB13_LethalPuzzles.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1F9() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
