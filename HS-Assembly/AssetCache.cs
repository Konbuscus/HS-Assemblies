﻿// Decompiled with JetBrains decompiler
// Type: AssetCache
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class AssetCache
{
  private static Map<string, int> s_assetLoading = new Map<string, int>();
  private static readonly Map<AssetFamily, AssetCache> s_cacheTable = new Map<AssetFamily, AssetCache>();
  private Map<string, AssetCache.CachedAsset> m_assetMap = new Map<string, AssetCache.CachedAsset>();
  private Map<string, AssetCache.CacheRequest> m_assetRequestMap = new Map<string, AssetCache.CacheRequest>();
  private static long s_cacheClearTime;

  public static void Initialize()
  {
    foreach (int num in Enum.GetValues(typeof (AssetFamily)))
    {
      AssetFamily key = (AssetFamily) num;
      AssetCache assetCache = new AssetCache();
      AssetCache.s_cacheTable.Add(key, assetCache);
    }
  }

  public static AssetCache.CachedAsset Find(Asset asset)
  {
    return AssetCache.s_cacheTable[asset.GetFamily()].GetItem(asset.GetName());
  }

  public static bool HasItem(Asset asset)
  {
    return AssetCache.s_cacheTable[asset.GetFamily()].HasItem(asset.GetName());
  }

  public static void Add(Asset asset, AssetCache.CachedAsset item)
  {
    AssetCache.s_cacheTable[asset.GetFamily()].AddItem(asset.GetName(), item);
  }

  public static long GetCacheClearTime()
  {
    return AssetCache.s_cacheClearTime;
  }

  public static bool IsLoading(string name)
  {
    int num;
    if (AssetCache.s_assetLoading.TryGetValue(name, out num))
      return num > 0;
    return false;
  }

  public static void StartLoading(string name)
  {
    if (!AssetCache.s_assetLoading.ContainsKey(name))
      AssetCache.s_assetLoading.Add(name, 0);
    Map<string, int> assetLoading;
    string index;
    (assetLoading = AssetCache.s_assetLoading)[index = name] = assetLoading[index] + 1;
  }

  public static void StopLoading(string name)
  {
    int num;
    if (!AssetCache.s_assetLoading.TryGetValue(name, out num))
      return;
    if (num < 1)
    {
      AssetCache.s_assetLoading.Remove(name);
    }
    else
    {
      Map<string, int> assetLoading;
      string index;
      (assetLoading = AssetCache.s_assetLoading)[index = name] = assetLoading[index] - 1;
    }
  }

  public static void ClearAllCaches(bool clearPersistent = false, bool clearLoading = true)
  {
    using (Map<AssetFamily, AssetCache>.Enumerator enumerator = AssetCache.s_cacheTable.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.Clear(clearPersistent, clearLoading);
    }
    AssetCache.s_cacheClearTime = TimeUtils.BinaryStamp();
    Log.Asset.Print("AssetCache::ClearAllCaches");
  }

  public static void ClearAllCachesSince(long sinceTimestamp)
  {
    long endTimestamp = TimeUtils.BinaryStamp();
    AssetCache.ClearAllCachesBetween(sinceTimestamp, endTimestamp);
  }

  public static void ClearAllCachesBetween(long startTimestamp, long endTimestamp)
  {
    using (Map<AssetFamily, AssetCache>.Enumerator enumerator = AssetCache.s_cacheTable.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.ClearItemsBetween(startTimestamp, endTimestamp);
    }
  }

  public static void ClearAllCachesFailedRequests()
  {
    using (Map<AssetFamily, AssetCache>.Enumerator enumerator = AssetCache.s_cacheTable.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.ClearAllFailedRequests();
    }
    Log.Asset.Print("AssetCache::ClearAllCachesFailedRequests");
  }

  public static void ForceClearAllCaches()
  {
    using (Map<AssetFamily, AssetCache>.Enumerator enumerator = AssetCache.s_cacheTable.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.ForceClear();
    }
    AssetCache.s_cacheClearTime = TimeUtils.BinaryStamp();
    Log.Asset.Print("AssetCache::ForceClearAllCaches");
  }

  public static void ClearAssetFamilyCache(AssetFamily family, bool clearPersistent = false, bool clearLoading = true)
  {
    AssetCache.s_cacheTable[family].Clear(clearPersistent, clearLoading);
    Log.Asset.Print("AssetCache::ClearCacheFamily family={0} clearPersistent={1} clearLoading={2}", new object[3]
    {
      (object) family,
      (object) clearPersistent,
      (object) clearLoading
    });
  }

  public static void ClearCardPrefabs(IEnumerable<string> names)
  {
    if (names == null)
      return;
    AssetCache.s_cacheTable[AssetFamily.CardPrefab].ClearItems(names);
  }

  public static void ClearCardPrefab(string name)
  {
    AssetCache.s_cacheTable[AssetFamily.CardPrefab].ClearItem(name);
  }

  public static void ClearActors(IEnumerable<string> names)
  {
    if (names == null)
      return;
    AssetCache.s_cacheTable[AssetFamily.Actor].ClearItems(names);
  }

  public static void ClearActor(string name)
  {
    AssetCache.s_cacheTable[AssetFamily.Actor].ClearItem(name);
  }

  public static void ClearTextures(IEnumerable<string> names)
  {
    if (names == null)
      return;
    AssetCache.s_cacheTable[AssetFamily.Texture].ClearItems(names);
  }

  public static void ClearTexture(string name)
  {
    AssetCache.s_cacheTable[AssetFamily.Texture].ClearItem(name);
  }

  public static void ClearSound(string name)
  {
    AssetCache.s_cacheTable[AssetFamily.Sound].ClearItem(name);
  }

  public static void ClearGameObject(string name)
  {
    AssetCache.s_cacheTable[AssetFamily.GameObject].ClearItem(name);
  }

  public static bool ClearItem(Asset asset)
  {
    return AssetCache.s_cacheTable[asset.GetFamily()].ClearItem(asset.GetName());
  }

  public static void ClearLocalizedAssets()
  {
    using (Map<AssetFamily, AssetCache>.Enumerator enumerator = AssetCache.s_cacheTable.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<AssetFamily, AssetCache> current = enumerator.Current;
        AssetFamilyBundleInfo familyBundleInfo = AssetBundleInfo.FamilyInfo[current.Key];
        if (familyBundleInfo.NumberOfLocaleBundles > 0 || familyBundleInfo.NumberOfDownloadableLocaleBundles > 0)
          current.Value.ClearLocalizedItems();
      }
    }
  }

  private AssetCache.CachedAsset GetItem(string key)
  {
    AssetCache.CachedAsset cachedAsset;
    if (this.m_assetMap.TryGetValue(key, out cachedAsset))
      return cachedAsset;
    return (AssetCache.CachedAsset) null;
  }

  private bool HasItem(string key)
  {
    return this.m_assetMap.ContainsKey(key);
  }

  private void AddItem(string name, AssetCache.CachedAsset item)
  {
    if (this.m_assetMap.ContainsKey(name))
      Debug.LogWarning((object) string.Format("AssetCache: Loaded asset {0} twice.  This probably happened because it was loaded asynchronously and synchronously.", (object) name));
    else
      this.m_assetMap.Add(name, item);
  }

  private AssetFamily? GetFamily()
  {
    using (Map<AssetFamily, AssetCache>.Enumerator enumerator = AssetCache.s_cacheTable.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<AssetFamily, AssetCache> current = enumerator.Current;
        if (this == current.Value)
          return new AssetFamily?(current.Key);
      }
    }
    return new AssetFamily?();
  }

  private void Clear(bool clearPersistent = false, bool clearLoading = true)
  {
    List<string> stringList1 = new List<string>();
    using (Map<string, AssetCache.CachedAsset>.Enumerator enumerator = this.m_assetMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, AssetCache.CachedAsset> current = enumerator.Current;
        string key = current.Key;
        if (!current.Value.IsPersistent() || clearPersistent)
        {
          if (AssetCache.IsLoading(key) && !clearLoading)
            Log.Reset.Print(LogLevel.Warning, "Not clearing asset " + key + " because it's still loading", new object[0]);
          else
            stringList1.Add(key);
        }
      }
    }
    using (List<string>.Enumerator enumerator = stringList1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        Log.Asset.Print("AssetCache::Clear removing asset={0}", (object) current);
        this.ClearItem(current);
      }
    }
    List<string> stringList2 = new List<string>();
    using (Map<string, AssetCache.CacheRequest>.Enumerator enumerator = this.m_assetRequestMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, AssetCache.CacheRequest> current = enumerator.Current;
        string key = current.Key;
        if ((!current.Value.IsPersistent() || clearPersistent) && (!AssetCache.IsLoading(key) || clearLoading))
          stringList2.Add(key);
      }
    }
    using (List<string>.Enumerator enumerator = stringList2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        Log.Asset.Print("AssetCache::Clear removing request={0}", (object) current);
        this.ClearItem(current);
      }
    }
  }

  private void ForceClear()
  {
    using (Map<string, AssetCache.CachedAsset>.Enumerator enumerator = this.m_assetMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.UnloadAssetObject();
    }
    using (Map<string, AssetCache.CacheRequest>.Enumerator enumerator = this.m_assetRequestMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.SetSuccess(false);
    }
    this.m_assetMap.Clear();
    this.m_assetRequestMap.Clear();
    Log.Asset.Print("AssetCache::ForceClear");
  }

  private void ClearItemsBetween(long startTimestamp, long endTimestamp)
  {
    if (endTimestamp < startTimestamp)
      return;
    HashSet<string> stringSet = new HashSet<string>();
    using (Map<string, AssetCache.CachedAsset>.Enumerator enumerator = this.m_assetMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, AssetCache.CachedAsset> current = enumerator.Current;
        AssetCache.CachedAsset cachedAsset = current.Value;
        if (!cachedAsset.IsPersistent())
        {
          long requestTimestamp = cachedAsset.GetLastRequestTimestamp();
          if (startTimestamp <= requestTimestamp && requestTimestamp <= endTimestamp)
            stringSet.Add(current.Key);
        }
      }
    }
    using (Map<string, AssetCache.CacheRequest>.Enumerator enumerator = this.m_assetRequestMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, AssetCache.CacheRequest> current = enumerator.Current;
        AssetCache.CacheRequest cacheRequest = current.Value;
        if (!cacheRequest.IsPersistent())
        {
          long requestTimestamp = cacheRequest.GetLastRequestTimestamp();
          if (startTimestamp <= requestTimestamp && requestTimestamp <= endTimestamp)
            stringSet.Add(current.Key);
        }
      }
    }
    using (HashSet<string>.Enumerator enumerator = stringSet.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        Log.Asset.Print("AssetCache::ClearingItemsBetween: removing asset={0}", (object) current);
        this.ClearItem(current);
      }
    }
  }

  private bool ClearItem(string key)
  {
    bool flag = false;
    AssetCache.CachedAsset cachedAsset;
    if (this.m_assetMap.TryGetValue(key, out cachedAsset))
    {
      cachedAsset.UnloadAssetObject();
      this.m_assetMap.Remove(key);
      flag = true;
    }
    AssetCache.CacheRequest cacheRequest;
    if (this.m_assetRequestMap.TryGetValue(key, out cacheRequest))
    {
      cacheRequest.SetSuccess(false);
      this.m_assetRequestMap.Remove(key);
      flag = true;
    }
    if (!flag)
      Log.Asset.PrintWarning(string.Format("AssetCache.ClearItem() - there is no asset and no request for key {0} in {1}", (object) key, (object) this));
    return flag;
  }

  private void ClearItems(IEnumerable<string> itemsToRemove)
  {
    foreach (string key in itemsToRemove)
      this.ClearItem(key);
  }

  private void ClearAllFailedRequests()
  {
    List<string> stringList = new List<string>();
    using (Map<string, AssetCache.CacheRequest>.Enumerator enumerator = this.m_assetRequestMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, AssetCache.CacheRequest> current = enumerator.Current;
        if (current.Value.DidFail())
          stringList.Add(current.Key);
      }
    }
    using (List<string>.Enumerator enumerator = stringList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.m_assetRequestMap.Remove(enumerator.Current);
    }
  }

  private void ClearLocalizedItems()
  {
    HashSet<string> stringSet = new HashSet<string>();
    using (Map<string, AssetCache.CachedAsset>.Enumerator enumerator = this.m_assetMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, AssetCache.CachedAsset> current = enumerator.Current;
        AssetCache.CachedAsset cachedAsset = current.Value;
        if (cachedAsset.GetAsset().GetLocale() != Locale.UNKNOWN && cachedAsset.GetAsset().GetLocale() != Locale.enUS)
          stringSet.Add(current.Key);
      }
    }
    using (HashSet<string>.Enumerator enumerator = stringSet.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.ClearItem(enumerator.Current);
    }
  }

  public static T GetRequest<T>(Asset asset) where T : AssetCache.CacheRequest
  {
    return AssetCache.s_cacheTable[asset.GetFamily()].GetRequest<T>(asset.GetName());
  }

  public T GetRequest<T>(string key) where T : AssetCache.CacheRequest
  {
    AssetCache.CacheRequest cacheRequest;
    if (this.m_assetRequestMap.TryGetValue(key, out cacheRequest))
      return cacheRequest as T;
    return (T) null;
  }

  public static bool HasRequest(Asset asset)
  {
    return AssetCache.s_cacheTable[asset.GetFamily()].HasRequest(asset.GetName());
  }

  public bool HasRequest(string key)
  {
    return this.m_assetRequestMap.ContainsKey(key);
  }

  public static void AddRequest(Asset asset, AssetCache.CacheRequest request)
  {
    AssetCache.s_cacheTable[asset.GetFamily()].AddRequest(asset.GetName(), request);
  }

  public void AddRequest(string key, AssetCache.CacheRequest request)
  {
    this.m_assetRequestMap.Add(key, request);
  }

  public static bool RemoveRequest(Asset asset)
  {
    return AssetCache.s_cacheTable[asset.GetFamily()].RemoveRequest(asset.GetName());
  }

  public bool RemoveRequest(string key)
  {
    return this.m_assetRequestMap.Remove(key);
  }

  public class CachedAsset
  {
    private UnityEngine.Object m_assetObject;
    private Asset m_asset;
    private long m_createdTimestamp;
    private long m_lastRequestTimestamp;
    private bool m_persistent;

    public UnityEngine.Object GetAssetObject()
    {
      return this.m_assetObject;
    }

    public void SetAssetObject(UnityEngine.Object asset)
    {
      this.m_assetObject = asset;
    }

    public Asset GetAsset()
    {
      return this.m_asset;
    }

    public void SetAsset(Asset asset)
    {
      this.m_asset = asset;
    }

    public long GetCreatedTimestamp()
    {
      return this.m_createdTimestamp;
    }

    public void SetCreatedTimestamp(long timestamp)
    {
      this.m_createdTimestamp = timestamp;
    }

    public long GetLastRequestTimestamp()
    {
      return this.m_lastRequestTimestamp;
    }

    public void SetLastRequestTimestamp(long timestamp)
    {
      this.m_lastRequestTimestamp = timestamp;
    }

    public bool IsPersistent()
    {
      return this.m_persistent;
    }

    public void SetPersistent(bool persistent)
    {
      this.m_persistent = persistent;
    }

    public void UnloadAssetObject()
    {
      Log.Asset.Print("CachedAsset.UnloadAssetObject() - unloading name={0} family={1} persistent={2}", new object[3]
      {
        (object) this.m_asset.GetName(),
        (object) this.m_asset.GetFamily(),
        (object) this.IsPersistent()
      });
      this.m_assetObject = (UnityEngine.Object) null;
    }
  }

  public class ObjectRequester
  {
    public AssetLoader.ObjectCallback m_callback;
    public object m_callbackData;
  }

  public class GameObjectRequester
  {
    public AssetLoader.GameObjectCallback m_callback;
    public object m_callbackData;
  }

  public abstract class CacheRequest
  {
    private long m_createdTimestamp;
    private long m_lastRequestTimestamp;
    private bool m_persistent;
    private bool m_complete;
    private bool m_success;

    public long GetCreatedTimestamp()
    {
      return this.m_createdTimestamp;
    }

    public void SetCreatedTimestamp(long timestamp)
    {
      this.m_createdTimestamp = timestamp;
    }

    public long GetLastRequestTimestamp()
    {
      return this.m_lastRequestTimestamp;
    }

    public void SetLastRequestTimestamp(long timestamp)
    {
      this.m_lastRequestTimestamp = timestamp;
    }

    public bool IsPersistent()
    {
      return this.m_persistent;
    }

    public void SetPersistent(bool persistent)
    {
      this.m_persistent = persistent;
    }

    public bool IsComplete()
    {
      return this.m_complete;
    }

    public void SetComplete(bool complete)
    {
      this.m_complete = complete;
    }

    public bool IsSuccess()
    {
      return this.m_success;
    }

    public void SetSuccess(bool success)
    {
      this.m_success = success;
    }

    public abstract int GetRequestCount();

    public void OnLoadSucceeded()
    {
      this.m_complete = true;
      this.m_success = true;
    }

    public virtual void OnLoadFailed(string name)
    {
      this.m_complete = true;
      this.m_success = false;
    }

    public bool DidSucceed()
    {
      if (this.m_complete)
        return this.m_success;
      return false;
    }

    public bool DidFail()
    {
      if (this.m_complete)
        return !this.m_success;
      return false;
    }
  }

  public class ObjectCacheRequest : AssetCache.CacheRequest
  {
    private readonly List<AssetCache.ObjectRequester> m_requesters = new List<AssetCache.ObjectRequester>();

    public List<AssetCache.ObjectRequester> GetRequesters()
    {
      return this.m_requesters;
    }

    public void AddRequester(AssetLoader.ObjectCallback callback, object callbackData)
    {
      this.m_requesters.Add(new AssetCache.ObjectRequester()
      {
        m_callback = callback,
        m_callbackData = callbackData
      });
    }

    public void OnLoadComplete(string name, UnityEngine.Object asset)
    {
      using (List<AssetCache.ObjectRequester>.Enumerator enumerator = this.m_requesters.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AssetCache.ObjectRequester current = enumerator.Current;
          AssetLoader.ObjectCallback callback = current.m_callback;
          if (GeneralUtils.IsCallbackValid((Delegate) callback))
            callback(name, asset, current.m_callbackData);
        }
      }
    }

    public override void OnLoadFailed(string name)
    {
      base.OnLoadFailed(name);
      this.OnLoadComplete(name, (UnityEngine.Object) null);
    }

    public override int GetRequestCount()
    {
      return this.m_requesters.Count;
    }
  }

  public class PrefabCacheRequest : AssetCache.CacheRequest
  {
    private readonly List<AssetCache.GameObjectRequester> m_requesters = new List<AssetCache.GameObjectRequester>();

    public List<AssetCache.GameObjectRequester> GetRequesters()
    {
      return this.m_requesters;
    }

    public void AddRequester(AssetLoader.GameObjectCallback callback, object callbackData)
    {
      this.m_requesters.Add(new AssetCache.GameObjectRequester()
      {
        m_callback = callback,
        m_callbackData = callbackData
      });
    }

    public override void OnLoadFailed(string name)
    {
      base.OnLoadFailed(name);
      using (List<AssetCache.GameObjectRequester>.Enumerator enumerator = this.m_requesters.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AssetCache.GameObjectRequester current = enumerator.Current;
          AssetLoader.GameObjectCallback callback = current.m_callback;
          object callbackData = current.m_callbackData;
          if (GeneralUtils.IsCallbackValid((Delegate) callback))
            callback(name, (GameObject) null, callbackData);
        }
      }
    }

    public override int GetRequestCount()
    {
      return this.m_requesters.Count;
    }
  }
}
