﻿// Decompiled with JetBrains decompiler
// Type: LossMarks
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LossMarks : MonoBehaviour
{
  public void Init(int numMarks)
  {
    for (int index = 0; index < this.transform.childCount; ++index)
    {
      this.transform.GetChild(index).gameObject.SetActive(numMarks > 0);
      --numMarks;
    }
  }

  public void SetNumMarked(int numMarked)
  {
    for (int index = 0; index < this.transform.childCount; ++index)
    {
      this.transform.GetChild(index).GetChild(0).gameObject.SetActive(numMarked > 0);
      --numMarked;
    }
  }
}
