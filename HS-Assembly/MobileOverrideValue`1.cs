﻿// Decompiled with JetBrains decompiler
// Type: MobileOverrideValue`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class MobileOverrideValue<T>
{
  public ScreenCategory[] screens;
  public T[] values;

  public MobileOverrideValue()
  {
    this.screens = new ScreenCategory[1];
    this.screens[0] = ScreenCategory.PC;
    this.values = new T[1];
    this.values[0] = default (T);
  }

  public MobileOverrideValue(T defaultValue)
  {
    this.screens = new ScreenCategory[1]
    {
      ScreenCategory.PC
    };
    this.values = new T[1]{ defaultValue };
  }

  public static implicit operator T(MobileOverrideValue<T> val)
  {
    if (val == null)
      return default (T);
    ScreenCategory[] screens = val.screens;
    T[] values = val.values;
    if (screens.Length < 1)
    {
      Debug.LogError((object) "MobileOverrideValue should always have at least one value!");
      return default (T);
    }
    T obj = values[0];
    ScreenCategory screen = PlatformSettings.Screen;
    for (int index = 1; index < screens.Length; ++index)
    {
      if (screen == screens[index])
        obj = values[index];
    }
    return obj;
  }

  public T[] GetValues()
  {
    return this.values;
  }
}
