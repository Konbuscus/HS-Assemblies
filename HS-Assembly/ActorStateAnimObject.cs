﻿// Decompiled with JetBrains decompiler
// Type: ActorStateAnimObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ActorStateAnimObject
{
  public bool m_Enabled = true;
  public GameObject m_GameObject;
  public AnimationClip m_AnimClip;
  public int m_AnimLayer;
  public float m_CrossFadeSec;
  public bool m_ControlParticles;
  public bool m_EmitParticles;
  public string m_Comment;
  private bool m_prevParticleEmitValue;

  public void Init()
  {
    if ((UnityEngine.Object) this.m_GameObject == (UnityEngine.Object) null || (UnityEngine.Object) this.m_AnimClip == (UnityEngine.Object) null)
      return;
    string name = this.m_AnimClip.name;
    Animation animation = !((UnityEngine.Object) this.m_GameObject.GetComponent<Animation>() == (UnityEngine.Object) null) ? this.m_GameObject.GetComponent<Animation>() : this.m_GameObject.AddComponent<Animation>();
    animation.playAutomatically = false;
    if ((TrackedReference) animation[name] == (TrackedReference) null)
      animation.AddClip(this.m_AnimClip, name);
    animation[name].layer = this.m_AnimLayer;
  }

  public void Play()
  {
    if (!this.m_Enabled || (UnityEngine.Object) this.m_GameObject == (UnityEngine.Object) null)
      return;
    if ((UnityEngine.Object) this.m_AnimClip != (UnityEngine.Object) null)
    {
      string name = this.m_AnimClip.name;
      this.m_GameObject.GetComponent<Animation>()[name].enabled = true;
      if (Mathf.Approximately(this.m_CrossFadeSec, 0.0f))
      {
        if (!this.m_GameObject.GetComponent<Animation>().Play(name))
          Debug.LogWarning((object) string.Format("ActorStateAnimObject.PlayNow() - FAILED to play clip {0} on {1}", (object) name, (object) this.m_GameObject));
      }
      else
        this.m_GameObject.GetComponent<Animation>().CrossFade(name, this.m_CrossFadeSec);
    }
    if (!this.m_ControlParticles || !((UnityEngine.Object) this.m_GameObject.GetComponent<ParticleEmitter>() != (UnityEngine.Object) null))
      return;
    this.m_GameObject.GetComponent<ParticleEmitter>().emit = this.m_EmitParticles;
  }

  public void Stop()
  {
    if (!this.m_Enabled || (UnityEngine.Object) this.m_GameObject == (UnityEngine.Object) null)
      return;
    if ((UnityEngine.Object) this.m_AnimClip != (UnityEngine.Object) null)
    {
      this.m_GameObject.GetComponent<Animation>()[this.m_AnimClip.name].time = 0.0f;
      this.m_GameObject.GetComponent<Animation>().Sample();
      this.m_GameObject.GetComponent<Animation>()[this.m_AnimClip.name].enabled = false;
    }
    if (!this.m_ControlParticles || !((UnityEngine.Object) this.m_GameObject.GetComponent<ParticleEmitter>() != (UnityEngine.Object) null))
      return;
    this.m_GameObject.GetComponent<ParticleEmitter>().emit = this.m_EmitParticles;
  }

  public void Stop(List<ActorState> nextStateList)
  {
    if (!this.m_Enabled || (UnityEngine.Object) this.m_GameObject == (UnityEngine.Object) null)
      return;
    if ((UnityEngine.Object) this.m_AnimClip != (UnityEngine.Object) null)
    {
      bool flag = false;
      for (int index1 = 0; !flag && index1 < nextStateList.Count; ++index1)
      {
        ActorState nextState = nextStateList[index1];
        for (int index2 = 0; index2 < nextState.m_ExternalAnimatedObjects.Count; ++index2)
        {
          ActorStateAnimObject externalAnimatedObject = nextState.m_ExternalAnimatedObjects[index2];
          if ((UnityEngine.Object) this.m_GameObject == (UnityEngine.Object) externalAnimatedObject.m_GameObject && this.m_AnimLayer == externalAnimatedObject.m_AnimLayer)
          {
            flag = true;
            break;
          }
        }
      }
      if (!flag)
        this.m_GameObject.GetComponent<Animation>().Stop(this.m_AnimClip.name);
    }
    if (!this.m_ControlParticles || !((UnityEngine.Object) this.m_GameObject.GetComponent<ParticleEmitter>() != (UnityEngine.Object) null))
      return;
    this.m_GameObject.GetComponent<ParticleEmitter>().emit = this.m_EmitParticles;
  }
}
