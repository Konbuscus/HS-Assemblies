﻿// Decompiled with JetBrains decompiler
// Type: BoxCameraStateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class BoxCameraStateInfo
{
  public float m_ClosedMoveSec = 0.7f;
  public iTween.EaseType m_ClosedMoveEaseType = iTween.EaseType.easeOutCubic;
  public float m_ClosedWithDrawerMoveSec = 0.7f;
  public iTween.EaseType m_ClosedWithDrawerMoveEaseType = iTween.EaseType.easeOutCubic;
  public float m_OpenedMoveSec = 0.7f;
  public iTween.EaseType m_OpenedMoveEaseType = iTween.EaseType.easeOutCubic;
  public GameObject m_ClosedBone;
  public GameObject m_ClosedMinAspectRatioBone;
  public float m_ClosedDelaySec;
  public GameObject m_ClosedWithDrawerBone;
  public GameObject m_ClosedWithDrawerMinAspectRatioBone;
  public float m_ClosedWithDrawerDelaySec;
  public GameObject m_OpenedBone;
  public GameObject m_OpenedMinAspectRatioBone;
  public float m_OpenedDelaySec;
}
