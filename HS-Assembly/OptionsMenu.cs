﻿// Decompiled with JetBrains decompiler
// Type: OptionsMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class OptionsMenu : MonoBehaviour
{
  private List<GraphicsResolution> m_fullScreenResolutions = new List<GraphicsResolution>();
  private readonly PlatformDependentValue<bool> LANGUAGE_SELECTION = new PlatformDependentValue<bool>(PlatformCategory.OS) { iOS = true, Android = true, PC = false, Mac = false };
  [CustomEditField(Sections = "Layout")]
  public MultiSliceElement m_leftPane;
  [CustomEditField(Sections = "Layout")]
  public MultiSliceElement m_rightPane;
  [CustomEditField(Sections = "Layout")]
  public MultiSliceElement m_middlePane;
  [CustomEditField(Sections = "Graphics")]
  public GameObject m_graphicsGroup;
  [CustomEditField(Sections = "Graphics")]
  public DropdownControl m_graphicsRes;
  [CustomEditField(Sections = "Graphics")]
  public DropdownControl m_graphicsQuality;
  [CustomEditField(Sections = "Graphics")]
  public CheckBox m_fullScreenCheckbox;
  [CustomEditField(Sections = "Sound")]
  public GameObject m_soundGroup;
  [CustomEditField(Sections = "Sound")]
  public ScrollbarControl m_masterVolume;
  [CustomEditField(Sections = "Sound")]
  public ScrollbarControl m_musicVolume;
  [CustomEditField(Sections = "Sound")]
  public CheckBox m_backgroundSound;
  [CustomEditField(Sections = "Misc")]
  public GameObject m_miscGroup;
  [CustomEditField(Sections = "Misc")]
  public UIBButton m_creditsButton;
  [CustomEditField(Sections = "Misc")]
  public UIBButton m_cinematicButton;
  [CustomEditField(Sections = "Preferences")]
  public CheckBox m_nearbyPlayers;
  [CustomEditField(Sections = "Preferences")]
  public CheckBox m_spectatorOpenJoinCheckbox;
  [CustomEditField(Sections = "Language")]
  public GameObject m_languageGroup;
  [CustomEditField(Sections = "Language")]
  public DropdownControl m_languageDropdown;
  [CustomEditField(Sections = "Language")]
  public FontDef m_languageDropdownFont;
  [CustomEditField(Sections = "Language")]
  public CheckBox m_languagePackCheckbox;
  [CustomEditField(Sections = "Internal Stuff")]
  public PegUIElement m_inputBlocker;
  [CustomEditField(Sections = "Internal Stuff")]
  public UberText m_versionLabel;
  private static OptionsMenu s_instance;
  private bool m_isShown;
  private OptionsMenu.hideHandler m_hideHandler;
  private Vector3 NORMAL_SCALE;
  private Vector3 HIDDEN_SCALE;

  private void Awake()
  {
    OptionsMenu.s_instance = this;
    this.NORMAL_SCALE = this.transform.localScale;
    this.HIDDEN_SCALE = 0.01f * this.NORMAL_SCALE;
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    if (!(bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_graphicsRes.setItemTextCallback(new DropdownControl.itemTextCallback(this.OnGraphicsResolutionDropdownText));
      this.m_graphicsRes.setItemChosenCallback(new DropdownControl.itemChosenCallback(this.OnNewGraphicsResolution));
      using (List<GraphicsResolution>.Enumerator enumerator = this.GetGoodGraphicsResolution().GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.m_graphicsRes.addItem((object) enumerator.Current);
      }
      this.m_graphicsRes.setSelection((object) this.GetCurrentGraphicsResolution());
      this.m_fullScreenCheckbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnToggleFullScreenCheckbox));
      this.m_fullScreenCheckbox.SetChecked(Options.Get().GetBool(Option.GFX_FULLSCREEN, Screen.fullScreen));
      this.m_graphicsQuality.addItem((object) GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_LOW"));
      this.m_graphicsQuality.addItem((object) GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_MEDIUM"));
      this.m_graphicsQuality.addItem((object) GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_HIGH"));
      this.m_graphicsQuality.setSelection((object) this.GetCurrentGraphicsQuality());
      this.m_graphicsQuality.setItemChosenCallback(new DropdownControl.itemChosenCallback(this.OnNewGraphicsQuality));
    }
    this.m_masterVolume.SetValue(Options.Get().GetFloat(Option.SOUND_VOLUME));
    this.m_masterVolume.SetUpdateHandler(new ScrollbarControl.UpdateHandler(this.OnNewMasterVolume));
    this.m_masterVolume.SetFinishHandler(new ScrollbarControl.FinishHandler(this.OnMasterVolumeRelease));
    this.m_musicVolume.SetValue(Options.Get().GetFloat(Option.MUSIC_VOLUME));
    this.m_musicVolume.SetUpdateHandler(new ScrollbarControl.UpdateHandler(this.OnNewMusicVolume));
    if ((UnityEngine.Object) this.m_backgroundSound != (UnityEngine.Object) null)
    {
      this.m_backgroundSound.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ToggleBackgroundSound));
      this.m_backgroundSound.SetChecked(Options.Get().GetBool(Option.BACKGROUND_SOUND));
    }
    this.UpdateCreditsUI();
    this.m_nearbyPlayers.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ToggleNearbyPlayers));
    this.m_nearbyPlayers.SetChecked(Options.Get().GetBool(Option.NEARBY_PLAYERS));
    this.m_spectatorOpenJoinCheckbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ToggleSpectatorOpenJoin));
    this.m_spectatorOpenJoinCheckbox.SetChecked(Options.Get().GetBool(Option.SPECTATOR_OPEN_JOIN));
    this.m_languageGroup.gameObject.SetActive((bool) this.LANGUAGE_SELECTION);
    if ((bool) this.LANGUAGE_SELECTION)
    {
      this.m_languageDropdown.setFont(this.m_languageDropdownFont.m_Font);
      foreach (int num in Enum.GetValues(typeof (Locale)))
      {
        Locale locale = (Locale) num;
        switch (locale)
        {
          case Locale.UNKNOWN:
          case Locale.enGB:
            continue;
          default:
            this.m_languageDropdown.addItem((object) GameStrings.Get(this.StringNameFromLocale(locale)));
            continue;
        }
      }
      this.m_languageDropdown.setSelection((object) this.GetCurrentLanguage());
      this.m_languageDropdown.setItemChosenCallback(new DropdownControl.itemChosenCallback(this.OnNewLanguage));
    }
    if (AssetLoader.DOWNLOADABLE_LANGUAGE_PACKS && ApplicationMgr.IsInternal())
    {
      this.m_languagePackCheckbox.gameObject.SetActive(true);
      this.m_languagePackCheckbox.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ToggleLanguagePackCheckbox));
      this.m_languagePackCheckbox.SetChecked(Downloader.Get().AllLocalizedAudioBundlesDownloaded());
    }
    this.m_creditsButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCreditsButtonReleased));
    this.m_cinematicButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCinematicButtonReleased));
    this.m_inputBlocker.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.Hide(true)));
    this.ShowOrHide(false);
    this.m_leftPane.UpdateSlices();
    this.m_rightPane.UpdateSlices();
    this.m_middlePane.UpdateSlices();
  }

  public void OnDestroy()
  {
    if (FatalErrorMgr.Get() != null)
      FatalErrorMgr.Get().RemoveErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    OptionsMenu.s_instance = (OptionsMenu) null;
  }

  public static OptionsMenu Get()
  {
    return OptionsMenu.s_instance;
  }

  public OptionsMenu.hideHandler GetHideHandler()
  {
    return this.m_hideHandler;
  }

  public void SetHideHandler(OptionsMenu.hideHandler handler)
  {
    this.m_hideHandler = handler;
  }

  public void RemoveHideHandler(OptionsMenu.hideHandler handler)
  {
    if (!((MulticastDelegate) this.m_hideHandler == (MulticastDelegate) handler))
      return;
    this.m_hideHandler = (OptionsMenu.hideHandler) null;
  }

  public bool IsShown()
  {
    return this.m_isShown;
  }

  public void Show()
  {
    this.UpdateCreditsUI();
    this.ShowOrHide(true);
    AnimationUtil.ShowWithPunch(this.gameObject, this.HIDDEN_SCALE, 1.1f * this.NORMAL_SCALE, this.NORMAL_SCALE, (string) null, true, (GameObject) null, (object) null, (AnimationUtil.DelOnShownWithPunch) null);
  }

  public void Hide(bool callHideHandler = true)
  {
    this.ShowOrHide(false);
    if (this.m_hideHandler == null || !callHideHandler)
      return;
    this.m_hideHandler();
    this.m_hideHandler = (OptionsMenu.hideHandler) null;
  }

  private GraphicsResolution GetCurrentGraphicsResolution()
  {
    return GraphicsResolution.create(Options.Get().GetInt(Option.GFX_WIDTH, Screen.currentResolution.width), Options.Get().GetInt(Option.GFX_HEIGHT, Screen.currentResolution.height));
  }

  private string GetCurrentGraphicsQuality()
  {
    switch (Options.Get().GetInt(Option.GFX_QUALITY))
    {
      case 0:
        return GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_LOW");
      case 1:
        return GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_MEDIUM");
      case 2:
        return GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_HIGH");
      default:
        return GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_LOW");
    }
  }

  private List<GraphicsResolution> GetGoodGraphicsResolution()
  {
    if (this.m_fullScreenResolutions.Count == 0)
    {
      using (List<GraphicsResolution>.Enumerator enumerator = GraphicsResolution.list.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GraphicsResolution current = enumerator.Current;
          if (current.x >= 1024 && current.y >= 728 && ((double) current.aspectRatio - 0.01 <= 16.0 / 9.0 && (double) current.aspectRatio + 0.01 >= 4.0 / 3.0))
            this.m_fullScreenResolutions.Add(current);
        }
      }
    }
    return this.m_fullScreenResolutions;
  }

  private string GetCurrentLanguage()
  {
    return GameStrings.Get(this.StringNameFromLocale(Localization.GetLocale()));
  }

  private void ShowOrHide(bool showOrHide)
  {
    this.m_isShown = showOrHide;
    this.gameObject.SetActive(showOrHide);
    this.m_leftPane.UpdateSlices();
    this.m_rightPane.UpdateSlices();
    this.m_middlePane.UpdateSlices();
  }

  private string StringNameFromLocale(Locale locale)
  {
    return "GLOBAL_LANGUAGE_NATIVE_" + locale.ToString().ToUpper();
  }

  private void UpdateCreditsUI()
  {
    this.m_miscGroup.SetActive(this.CanShowCredits());
  }

  private void OnFatalError(FatalErrorMessage message, object userData)
  {
    this.Hide(true);
  }

  private void OnToggleFullScreenCheckbox(UIEvent e)
  {
    GraphicsResolution selection = this.m_graphicsRes.getSelection() as GraphicsResolution;
    if (selection == null)
    {
      this.m_graphicsRes.setSelectionToLastItem();
      selection = this.m_graphicsRes.getSelection() as GraphicsResolution;
    }
    if (selection == null)
      return;
    GraphicsManager.Get().SetScreenResolution(selection.x, selection.y, this.m_fullScreenCheckbox.IsChecked());
    Options.Get().SetBool(Option.GFX_FULLSCREEN, this.m_fullScreenCheckbox.IsChecked());
  }

  private void OnNewGraphicsQuality(object selection, object prevSelection)
  {
    GraphicsQuality graphicsQuality = GraphicsQuality.Low;
    string str = (string) selection;
    if (str == GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_LOW"))
      graphicsQuality = GraphicsQuality.Low;
    else if (str == GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_MEDIUM"))
      graphicsQuality = GraphicsQuality.Medium;
    else if (str == GameStrings.Get("GLOBAL_OPTIONS_GRAPHICS_QUALITY_HIGH"))
      graphicsQuality = GraphicsQuality.High;
    Log.Kyle.Print("Graphics Quality: " + graphicsQuality.ToString());
    GraphicsManager.Get().RenderQualityLevel = graphicsQuality;
  }

  private void OnNewGraphicsResolution(object selection, object prevSelection)
  {
    GraphicsResolution graphicsResolution = (GraphicsResolution) selection;
    GraphicsManager.Get().SetScreenResolution(graphicsResolution.x, graphicsResolution.y, this.m_fullScreenCheckbox.IsChecked());
    Options.Get().SetInt(Option.GFX_WIDTH, graphicsResolution.x);
    Options.Get().SetInt(Option.GFX_HEIGHT, graphicsResolution.y);
  }

  private void OnNewLanguage(object selection, object prevSelection)
  {
    if (selection == prevSelection)
      return;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_LANGUAGE_CHANGE_CONFIRM_TITLE"),
      m_text = GameStrings.Get("GLOBAL_LANGUAGE_CHANGE_CONFIRM_MESSAGE"),
      m_showAlertIcon = false,
      m_layerToUse = new GameLayer?(GameLayer.HighPriorityUI),
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_responseCallback = new AlertPopup.ResponseCallback(this.OnChangeLanguageConfirmationResponse),
      m_responseUserData = selection
    });
  }

  private void OnChangeLanguageConfirmationResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
    {
      this.m_languageDropdown.setSelection((object) this.GetCurrentLanguage());
    }
    else
    {
      string str1 = (string) null;
      string str2 = (string) userData;
      foreach (int num in Enum.GetValues(typeof (Locale)))
      {
        Locale locale = (Locale) num;
        if (str2 == GameStrings.Get(this.StringNameFromLocale(locale)))
        {
          str1 = locale.ToString();
          break;
        }
      }
      if (str1 == null)
      {
        Debug.LogError((object) string.Format("OptionsMenu.OnChangeLanguageConfirmationResponse() - locale not found"));
      }
      else
      {
        Localization.SetLocaleName(str1);
        Options.Get().SetString(Option.LOCALE, str1);
        this.Hide(false);
        ApplicationMgr.Get().Reset();
      }
    }
  }

  private void ToggleLanguagePackCheckbox(UIEvent e)
  {
    if (this.m_languagePackCheckbox.IsChecked())
      Downloader.Get().DownloadLocalizedBundles();
    else
      Downloader.Get().DeleteLocalizedBundles();
  }

  private string OnGraphicsResolutionDropdownText(object val)
  {
    GraphicsResolution graphicsResolution = (GraphicsResolution) val;
    return string.Format("{0} x {1}", (object) graphicsResolution.x, (object) graphicsResolution.y);
  }

  private void OnNewMasterVolume(float newVolume)
  {
    Options.Get().SetFloat(Option.SOUND_VOLUME, newVolume);
  }

  private void OnMasterVolumeRelease()
  {
    SoundManager.Get().LoadAndPlay("UI_MouseClick_01", this.gameObject, 1f, (SoundManager.LoadedCallback) ((source, userData) => SoundManager.Get().Set3d(source, false)));
  }

  private void OnNewMusicVolume(float newVolume)
  {
    Options.Get().SetFloat(Option.MUSIC_VOLUME, newVolume);
  }

  private void ToggleBackgroundSound(UIEvent e)
  {
    Options.Get().SetBool(Option.BACKGROUND_SOUND, this.m_backgroundSound.IsChecked());
  }

  private void OnCreditsButtonReleased(UIEvent e)
  {
    this.Hide(false);
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.LOGIN)
      return;
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.CREDITS);
  }

  private void OnCinematicButtonReleased(UIEvent e)
  {
    Cinematic componentInChildren = SceneMgr.Get().GetComponentInChildren<Cinematic>();
    if ((bool) ((UnityEngine.Object) componentInChildren))
    {
      this.Hide(false);
      componentInChildren.Play(new Cinematic.MovieCallback(GameMenu.Get().ShowOptionsMenu));
    }
    else
      Debug.LogWarning((object) "Failed to locate Cinematic component on SceneMgr!");
  }

  private void ToggleNearbyPlayers(UIEvent e)
  {
    Options.Get().SetBool(Option.NEARBY_PLAYERS, this.m_nearbyPlayers.IsChecked());
  }

  private void ToggleSpectatorOpenJoin(UIEvent e)
  {
    Options.Get().SetBool(Option.SPECTATOR_OPEN_JOIN, this.m_spectatorOpenJoinCheckbox.IsChecked());
  }

  private bool CanShowCredits()
  {
    SceneMgr.Mode mode = SceneMgr.Get().GetMode();
    switch (mode)
    {
      case SceneMgr.Mode.GAMEPLAY:
      case SceneMgr.Mode.PACKOPENING:
label_2:
        return false;
      default:
        switch (mode - 11)
        {
          case SceneMgr.Mode.INVALID:
          case SceneMgr.Mode.LOGIN:
            goto label_2;
          default:
            return (!((UnityEngine.Object) GeneralStore.Get() != (UnityEngine.Object) null) || !GeneralStore.Get().IsShown()) && (!Network.Get().IsFindingGame() && GameUtils.AreAllTutorialsComplete()) && (!((UnityEngine.Object) WelcomeQuests.Get() != (UnityEngine.Object) null) && (!((UnityEngine.Object) ArenaStore.Get() != (UnityEngine.Object) null) || !ArenaStore.Get().IsShown())) && ((!((UnityEngine.Object) TavernBrawlStore.Get() != (UnityEngine.Object) null) || !TavernBrawlStore.Get().IsShown()) && (!((UnityEngine.Object) DraftDisplay.Get() != (UnityEngine.Object) null) || DraftDisplay.Get().GetDraftMode() != DraftDisplay.DraftMode.IN_REWARDS));
        }
    }
  }

  public delegate void hideHandler();
}
