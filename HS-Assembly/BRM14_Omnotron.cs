﻿// Decompiled with JetBrains decompiler
// Type: BRM14_Omnotron
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class BRM14_Omnotron : BRM_MissionEntity
{
  private bool m_heroPower1LinePlayed;
  private bool m_heroPower2LinePlayed;
  private bool m_heroPower3LinePlayed;
  private bool m_heroPower4LinePlayed;
  private bool m_heroPower5LinePlayed;
  private bool m_cardLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BRMA14_1_RESPONSE1_10");
    this.PreloadSound("VO_BRMA14_1_RESPONSE2_11");
    this.PreloadSound("VO_BRMA14_1_RESPONSE3_12");
    this.PreloadSound("VO_BRMA14_1_RESPONSE4_13");
    this.PreloadSound("VO_BRMA14_1_RESPONSE5_14");
    this.PreloadSound("VO_BRMA14_1_HP1_03");
    this.PreloadSound("VO_BRMA14_1_HP2_04");
    this.PreloadSound("VO_BRMA14_1_HP3_05");
    this.PreloadSound("VO_BRMA14_1_HP4_06");
    this.PreloadSound("VO_BRMA14_1_HP5_07");
    this.PreloadSound("VO_BRMA14_1_CARD_09");
    this.PreloadSound("VO_BRMA14_1_TURN1_02");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA14_1_RESPONSE1_10",
            m_stringTag = "VO_BRMA14_1_RESPONSE1_10"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA14_1_RESPONSE2_11",
            m_stringTag = "VO_BRMA14_1_RESPONSE2_11"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA14_1_RESPONSE3_12",
            m_stringTag = "VO_BRMA14_1_RESPONSE3_12"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA14_1_RESPONSE4_13",
            m_stringTag = "VO_BRMA14_1_RESPONSE4_13"
          },
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA14_1_RESPONSE5_14",
            m_stringTag = "VO_BRMA14_1_RESPONSE5_14"
          }
        }
      }
    };
  }

  protected override void CycleNextResponseGroupIndex(MissionEntity.EmoteResponseGroup responseGroup)
  {
    if (responseGroup.m_responseIndex == responseGroup.m_responses.Count - 1)
      return;
    ++responseGroup.m_responseIndex;
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM14_Omnotron.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator14F() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM14_Omnotron.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator150() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM14_Omnotron.\u003CHandleGameOverWithTiming\u003Ec__Iterator151() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }
}
