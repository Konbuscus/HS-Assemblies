﻿// Decompiled with JetBrains decompiler
// Type: AnimateVars
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AnimateVars : MonoBehaviour
{
  public List<GameObject> m_objects;
  public float amount;
  public string varName;
  private List<Renderer> m_renderers;

  public void AnimateValue()
  {
    using (List<Renderer>.Enumerator enumerator = this.m_renderers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Renderer current = enumerator.Current;
        if ((Object) current != (Object) null)
          current.material.SetFloat(this.varName, this.amount);
      }
    }
  }

  private void Start()
  {
    this.m_renderers = new List<Renderer>();
    using (List<GameObject>.Enumerator enumerator = this.m_objects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          this.m_renderers.Add(current.GetComponent<Renderer>());
      }
    }
  }

  private void Update()
  {
    this.AnimateValue();
  }
}
