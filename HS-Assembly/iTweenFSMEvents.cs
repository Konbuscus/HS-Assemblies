﻿// Decompiled with JetBrains decompiler
// Type: iTweenFSMEvents
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using HutongGames.PlayMaker.Actions;
using UnityEngine;

public class iTweenFSMEvents : MonoBehaviour
{
  public static int itweenIDCount;
  public int itweenID;
  public iTweenFsmAction itweenFSMAction;
  public bool donotfinish;
  public bool islooping;

  private void iTweenOnStart(int aniTweenID)
  {
    if (this.itweenID != aniTweenID)
      return;
    this.itweenFSMAction.Fsm.Event(this.itweenFSMAction.startEvent);
  }

  private void iTweenOnComplete(int aniTweenID)
  {
    if (this.itweenID != aniTweenID)
      return;
    if (this.islooping)
    {
      if (this.donotfinish)
        return;
      this.itweenFSMAction.Fsm.Event(this.itweenFSMAction.finishEvent);
      this.itweenFSMAction.Finish();
    }
    else
    {
      this.itweenFSMAction.Fsm.Event(this.itweenFSMAction.finishEvent);
      this.itweenFSMAction.Finish();
    }
  }
}
