﻿// Decompiled with JetBrains decompiler
// Type: MultiClassGroupDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MultiClassGroupDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private string m_IconAssetPath;

  [DbfField("NOTE_DESC", "name of group")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("ICON_ASSET_PATH", "")]
  public string IconAssetPath
  {
    get
    {
      return this.m_IconAssetPath;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    MultiClassGroupDbfAsset classGroupDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (MultiClassGroupDbfAsset)) as MultiClassGroupDbfAsset;
    if ((UnityEngine.Object) classGroupDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("MultiClassGroupDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < classGroupDbfAsset.Records.Count; ++index)
      classGroupDbfAsset.Records[index].StripUnusedLocales();
    records = (object) classGroupDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetIconAssetPath(string v)
  {
    this.m_IconAssetPath = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (MultiClassGroupDbfRecord.\u003C\u003Ef__switch\u0024map4A == null)
      {
        // ISSUE: reference to a compiler-generated field
        MultiClassGroupDbfRecord.\u003C\u003Ef__switch\u0024map4A = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "ICON_ASSET_PATH",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (MultiClassGroupDbfRecord.\u003C\u003Ef__switch\u0024map4A.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.IconAssetPath;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (MultiClassGroupDbfRecord.\u003C\u003Ef__switch\u0024map4B == null)
    {
      // ISSUE: reference to a compiler-generated field
      MultiClassGroupDbfRecord.\u003C\u003Ef__switch\u0024map4B = new Dictionary<string, int>(3)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "ICON_ASSET_PATH",
          2
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!MultiClassGroupDbfRecord.\u003C\u003Ef__switch\u0024map4B.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetIconAssetPath((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (MultiClassGroupDbfRecord.\u003C\u003Ef__switch\u0024map4C == null)
      {
        // ISSUE: reference to a compiler-generated field
        MultiClassGroupDbfRecord.\u003C\u003Ef__switch\u0024map4C = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "ICON_ASSET_PATH",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (MultiClassGroupDbfRecord.\u003C\u003Ef__switch\u0024map4C.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
