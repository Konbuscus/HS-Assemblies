﻿// Decompiled with JetBrains decompiler
// Type: WingDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WingDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private int m_AdventureId;
  [SerializeField]
  private int m_SortOrder;
  [SerializeField]
  private int m_UnlockOrder;
  [SerializeField]
  private string m_Release;
  [SerializeField]
  private string m_RequiredEvent;
  [SerializeField]
  private int m_OwnershipPrereqWingId;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private DbfLocValue m_NameShort;
  [SerializeField]
  private DbfLocValue m_ClassChallengeRewardSource;
  [SerializeField]
  private string m_AdventureWingDefPrefab;
  [SerializeField]
  private DbfLocValue m_ComingSoonLabel;
  [SerializeField]
  private DbfLocValue m_RequiresLabel;
  [SerializeField]
  private int m_OpenPrereqWingId;
  [SerializeField]
  private DbfLocValue m_OpenDiscouragedLabel;
  [SerializeField]
  private DbfLocValue m_OpenDiscouragedWarning;

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("ADVENTURE_ID", "ASSET.ADVENTURE.ID")]
  public int AdventureId
  {
    get
    {
      return this.m_AdventureId;
    }
  }

  [DbfField("SORT_ORDER", "sort order of this wing on its adventure screen in the client")]
  public int SortOrder
  {
    get
    {
      return this.m_SortOrder;
    }
  }

  [DbfField("UNLOCK_ORDER", "order in which wing is unlocked for play")]
  public int UnlockOrder
  {
    get
    {
      return this.m_UnlockOrder;
    }
  }

  [DbfField("RELEASE", "first RELEASE.ID in which this asset appeared")]
  public string Release
  {
    get
    {
      return this.m_Release;
    }
  }

  [DbfField("REQUIRED_EVENT", "EVENT_TMING.EVENT that must have started before this wing can be played")]
  public string RequiredEvent
  {
    get
    {
      return this.m_RequiredEvent;
    }
  }

  [DbfField("OWNERSHIP_PREREQ_WING_ID", "WING.ID of ANOTHER wing that needs to be owned before THIS wing can be bought. Only the client cares about this.")]
  public int OwnershipPrereqWingId
  {
    get
    {
      return this.m_OwnershipPrereqWingId;
    }
  }

  [DbfField("NAME", "")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("NAME_SHORT", "shortened version of wing name. removes a The prefix from a wing, if it includes it, for use in AdventureStore")]
  public DbfLocValue NameShort
  {
    get
    {
      return this.m_NameShort;
    }
  }

  [DbfField("CLASS_CHALLENGE_REWARD_SOURCE", "")]
  public DbfLocValue ClassChallengeRewardSource
  {
    get
    {
      return this.m_ClassChallengeRewardSource;
    }
  }

  [DbfField("ADVENTURE_WING_DEF_PREFAB", "")]
  public string AdventureWingDefPrefab
  {
    get
    {
      return this.m_AdventureWingDefPrefab;
    }
  }

  [DbfField("COMING_SOON_LABEL", "")]
  public DbfLocValue ComingSoonLabel
  {
    get
    {
      return this.m_ComingSoonLabel;
    }
  }

  [DbfField("REQUIRES_LABEL", "")]
  public DbfLocValue RequiresLabel
  {
    get
    {
      return this.m_RequiresLabel;
    }
  }

  [DbfField("OPEN_PREREQ_WING_ID", "WING.ID of ANOTHER wing that needs to be opened before THIS wing can be opened. Only the client cares about this.")]
  public int OpenPrereqWingId
  {
    get
    {
      return this.m_OpenPrereqWingId;
    }
  }

  [DbfField("OPEN_DISCOURAGED_LABEL", "Label displayed on plate when opening this wing is not recommended (because the OWNERSHIP_PREREQ_WING_ID wing is not complete).")]
  public DbfLocValue OpenDiscouragedLabel
  {
    get
    {
      return this.m_OpenDiscouragedLabel;
    }
  }

  [DbfField("OPEN_DISCOURAGED_WARNING", "Warning text when attempting to open this wing and opening is not recommended (because the OWNERSHIP_PREREQ_WING_ID wing is not complete).")]
  public DbfLocValue OpenDiscouragedWarning
  {
    get
    {
      return this.m_OpenDiscouragedWarning;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    WingDbfAsset wingDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (WingDbfAsset)) as WingDbfAsset;
    if ((UnityEngine.Object) wingDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("WingDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < wingDbfAsset.Records.Count; ++index)
      wingDbfAsset.Records[index].StripUnusedLocales();
    records = (object) wingDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Name.StripUnusedLocales();
    this.m_NameShort.StripUnusedLocales();
    this.m_ClassChallengeRewardSource.StripUnusedLocales();
    this.m_ComingSoonLabel.StripUnusedLocales();
    this.m_RequiresLabel.StripUnusedLocales();
    this.m_OpenDiscouragedLabel.StripUnusedLocales();
    this.m_OpenDiscouragedWarning.StripUnusedLocales();
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetAdventureId(int v)
  {
    this.m_AdventureId = v;
  }

  public void SetSortOrder(int v)
  {
    this.m_SortOrder = v;
  }

  public void SetUnlockOrder(int v)
  {
    this.m_UnlockOrder = v;
  }

  public void SetRelease(string v)
  {
    this.m_Release = v;
  }

  public void SetRequiredEvent(string v)
  {
    this.m_RequiredEvent = v;
  }

  public void SetOwnershipPrereqWingId(int v)
  {
    this.m_OwnershipPrereqWingId = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetNameShort(DbfLocValue v)
  {
    this.m_NameShort = v;
    v.SetDebugInfo(this.ID, "NAME_SHORT");
  }

  public void SetClassChallengeRewardSource(DbfLocValue v)
  {
    this.m_ClassChallengeRewardSource = v;
    v.SetDebugInfo(this.ID, "CLASS_CHALLENGE_REWARD_SOURCE");
  }

  public void SetAdventureWingDefPrefab(string v)
  {
    this.m_AdventureWingDefPrefab = v;
  }

  public void SetComingSoonLabel(DbfLocValue v)
  {
    this.m_ComingSoonLabel = v;
    v.SetDebugInfo(this.ID, "COMING_SOON_LABEL");
  }

  public void SetRequiresLabel(DbfLocValue v)
  {
    this.m_RequiresLabel = v;
    v.SetDebugInfo(this.ID, "REQUIRES_LABEL");
  }

  public void SetOpenPrereqWingId(int v)
  {
    this.m_OpenPrereqWingId = v;
  }

  public void SetOpenDiscouragedLabel(DbfLocValue v)
  {
    this.m_OpenDiscouragedLabel = v;
    v.SetDebugInfo(this.ID, "OPEN_DISCOURAGED_LABEL");
  }

  public void SetOpenDiscouragedWarning(DbfLocValue v)
  {
    this.m_OpenDiscouragedWarning = v;
    v.SetDebugInfo(this.ID, "OPEN_DISCOURAGED_WARNING");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (WingDbfRecord.\u003C\u003Ef__switch\u0024map71 == null)
      {
        // ISSUE: reference to a compiler-generated field
        WingDbfRecord.\u003C\u003Ef__switch\u0024map71 = new Dictionary<string, int>(17)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "ADVENTURE_ID",
            2
          },
          {
            "SORT_ORDER",
            3
          },
          {
            "UNLOCK_ORDER",
            4
          },
          {
            "RELEASE",
            5
          },
          {
            "REQUIRED_EVENT",
            6
          },
          {
            "OWNERSHIP_PREREQ_WING_ID",
            7
          },
          {
            "NAME",
            8
          },
          {
            "NAME_SHORT",
            9
          },
          {
            "CLASS_CHALLENGE_REWARD_SOURCE",
            10
          },
          {
            "ADVENTURE_WING_DEF_PREFAB",
            11
          },
          {
            "COMING_SOON_LABEL",
            12
          },
          {
            "REQUIRES_LABEL",
            13
          },
          {
            "OPEN_PREREQ_WING_ID",
            14
          },
          {
            "OPEN_DISCOURAGED_LABEL",
            15
          },
          {
            "OPEN_DISCOURAGED_WARNING",
            16
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (WingDbfRecord.\u003C\u003Ef__switch\u0024map71.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteDesc;
          case 2:
            return (object) this.AdventureId;
          case 3:
            return (object) this.SortOrder;
          case 4:
            return (object) this.UnlockOrder;
          case 5:
            return (object) this.Release;
          case 6:
            return (object) this.RequiredEvent;
          case 7:
            return (object) this.OwnershipPrereqWingId;
          case 8:
            return (object) this.Name;
          case 9:
            return (object) this.NameShort;
          case 10:
            return (object) this.ClassChallengeRewardSource;
          case 11:
            return (object) this.AdventureWingDefPrefab;
          case 12:
            return (object) this.ComingSoonLabel;
          case 13:
            return (object) this.RequiresLabel;
          case 14:
            return (object) this.OpenPrereqWingId;
          case 15:
            return (object) this.OpenDiscouragedLabel;
          case 16:
            return (object) this.OpenDiscouragedWarning;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (WingDbfRecord.\u003C\u003Ef__switch\u0024map72 == null)
    {
      // ISSUE: reference to a compiler-generated field
      WingDbfRecord.\u003C\u003Ef__switch\u0024map72 = new Dictionary<string, int>(17)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_DESC",
          1
        },
        {
          "ADVENTURE_ID",
          2
        },
        {
          "SORT_ORDER",
          3
        },
        {
          "UNLOCK_ORDER",
          4
        },
        {
          "RELEASE",
          5
        },
        {
          "REQUIRED_EVENT",
          6
        },
        {
          "OWNERSHIP_PREREQ_WING_ID",
          7
        },
        {
          "NAME",
          8
        },
        {
          "NAME_SHORT",
          9
        },
        {
          "CLASS_CHALLENGE_REWARD_SOURCE",
          10
        },
        {
          "ADVENTURE_WING_DEF_PREFAB",
          11
        },
        {
          "COMING_SOON_LABEL",
          12
        },
        {
          "REQUIRES_LABEL",
          13
        },
        {
          "OPEN_PREREQ_WING_ID",
          14
        },
        {
          "OPEN_DISCOURAGED_LABEL",
          15
        },
        {
          "OPEN_DISCOURAGED_WARNING",
          16
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!WingDbfRecord.\u003C\u003Ef__switch\u0024map72.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteDesc((string) val);
        break;
      case 2:
        this.SetAdventureId((int) val);
        break;
      case 3:
        this.SetSortOrder((int) val);
        break;
      case 4:
        this.SetUnlockOrder((int) val);
        break;
      case 5:
        this.SetRelease((string) val);
        break;
      case 6:
        this.SetRequiredEvent((string) val);
        break;
      case 7:
        this.SetOwnershipPrereqWingId((int) val);
        break;
      case 8:
        this.SetName((DbfLocValue) val);
        break;
      case 9:
        this.SetNameShort((DbfLocValue) val);
        break;
      case 10:
        this.SetClassChallengeRewardSource((DbfLocValue) val);
        break;
      case 11:
        this.SetAdventureWingDefPrefab((string) val);
        break;
      case 12:
        this.SetComingSoonLabel((DbfLocValue) val);
        break;
      case 13:
        this.SetRequiresLabel((DbfLocValue) val);
        break;
      case 14:
        this.SetOpenPrereqWingId((int) val);
        break;
      case 15:
        this.SetOpenDiscouragedLabel((DbfLocValue) val);
        break;
      case 16:
        this.SetOpenDiscouragedWarning((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (WingDbfRecord.\u003C\u003Ef__switch\u0024map73 == null)
      {
        // ISSUE: reference to a compiler-generated field
        WingDbfRecord.\u003C\u003Ef__switch\u0024map73 = new Dictionary<string, int>(17)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_DESC",
            1
          },
          {
            "ADVENTURE_ID",
            2
          },
          {
            "SORT_ORDER",
            3
          },
          {
            "UNLOCK_ORDER",
            4
          },
          {
            "RELEASE",
            5
          },
          {
            "REQUIRED_EVENT",
            6
          },
          {
            "OWNERSHIP_PREREQ_WING_ID",
            7
          },
          {
            "NAME",
            8
          },
          {
            "NAME_SHORT",
            9
          },
          {
            "CLASS_CHALLENGE_REWARD_SOURCE",
            10
          },
          {
            "ADVENTURE_WING_DEF_PREFAB",
            11
          },
          {
            "COMING_SOON_LABEL",
            12
          },
          {
            "REQUIRES_LABEL",
            13
          },
          {
            "OPEN_PREREQ_WING_ID",
            14
          },
          {
            "OPEN_DISCOURAGED_LABEL",
            15
          },
          {
            "OPEN_DISCOURAGED_WARNING",
            16
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (WingDbfRecord.\u003C\u003Ef__switch\u0024map73.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (int);
          case 3:
            return typeof (int);
          case 4:
            return typeof (int);
          case 5:
            return typeof (string);
          case 6:
            return typeof (string);
          case 7:
            return typeof (int);
          case 8:
            return typeof (DbfLocValue);
          case 9:
            return typeof (DbfLocValue);
          case 10:
            return typeof (DbfLocValue);
          case 11:
            return typeof (string);
          case 12:
            return typeof (DbfLocValue);
          case 13:
            return typeof (DbfLocValue);
          case 14:
            return typeof (int);
          case 15:
            return typeof (DbfLocValue);
          case 16:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
