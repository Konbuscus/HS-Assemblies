﻿// Decompiled with JetBrains decompiler
// Type: WebAuth
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WebAuth
{
  private string m_url;
  private Rect m_window;
  private bool m_show;
  private string m_callbackGameObject;
  private static bool m_isNewCreatedAccount;

  public WebAuth(string url, float x, float y, float width, float height, string gameObjectName)
  {
    Debug.Log((object) "WebAuth");
    this.m_url = url;
    this.m_window = new Rect(x, y, width, height);
    this.m_callbackGameObject = gameObjectName;
    WebAuth.m_isNewCreatedAccount = false;
  }

  public void Load()
  {
    Debug.Log((object) "Load");
    WebAuth.LoadWebView(this.m_url, this.m_window.x, this.m_window.y, this.m_window.width, this.m_window.height, SystemInfo.deviceUniqueIdentifier, this.m_callbackGameObject);
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
  }

  public void Close()
  {
    Debug.Log((object) "Close");
    SplashScreen splashScreen = SplashScreen.Get();
    if ((Object) splashScreen != (Object) null)
      splashScreen.HideWebLoginCanvas();
    WebAuth.CloseWebView();
    FatalErrorMgr.Get().RemoveErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
  }

  public void SetCountryCodeCookie(string countryCode, string domain)
  {
    WebAuth.SetWebViewCountryCodeCookie(countryCode, domain);
  }

  public WebAuth.Status GetStatus()
  {
    int webViewStatus = WebAuth.GetWebViewStatus();
    if (webViewStatus < 0 || webViewStatus >= 6)
      return WebAuth.Status.Error;
    return (WebAuth.Status) webViewStatus;
  }

  public WebAuth.Error GetError()
  {
    return WebAuth.Error.InternalError;
  }

  public string GetLoginCode()
  {
    Debug.Log((object) "GetLoginCode");
    string str = WebAuth.GetWebViewLoginCode();
    int length = str.IndexOf("#");
    if (length > 0)
    {
      Debug.Log((object) "Trimming invalid token characters");
      str = str.Substring(0, length);
    }
    return str;
  }

  public void Show()
  {
    if (this.m_show)
      return;
    this.m_show = true;
    WebAuth.ShowWebView(true);
  }

  public bool IsShown()
  {
    return this.m_show;
  }

  public void Hide()
  {
    if (!this.m_show)
      return;
    this.m_show = false;
    WebAuth.ShowWebView(false);
  }

  public static void ClearLoginData()
  {
    WebAuth.DeleteStoredToken();
    WebAuth.DeleteCookies();
    WebAuth.ClearBrowserCache();
  }

  public static void DeleteCookies()
  {
    WebAuth.ClearWebViewCookies();
  }

  public static void ClearBrowserCache()
  {
    WebAuth.ClearURLCache();
  }

  public static string GetStoredToken()
  {
    return WebAuth.GetStoredLoginToken();
  }

  public static bool SetStoredToken(string str)
  {
    return WebAuth.SetStoredLoginToken(str);
  }

  public static void DeleteStoredToken()
  {
    WebAuth.DeleteStoredLoginToken();
  }

  public static void UpdateBreakingNews(string title, string body, string gameObjectName)
  {
    WebAuth.SetBreakingNews(title, body, gameObjectName);
  }

  public static void UpdateRegionSelectVisualState(bool isVisible)
  {
    WebAuth.SetRegionSelectVisualState(isVisible);
  }

  public static void GoBackWebPage()
  {
    WebAuth.GoBack();
  }

  public static bool GetIsNewCreatedAccount()
  {
    return WebAuth.m_isNewCreatedAccount;
  }

  public static void SetIsNewCreatedAccount(bool isNewCreatedAccount)
  {
    WebAuth.m_isNewCreatedAccount = isNewCreatedAccount;
  }

  private void OnFatalError(FatalErrorMessage message, object userData)
  {
    this.Close();
  }

  private static void LoadWebView(string str, float x, float y, float width, float height, string deviceUniqueIdentifier, string gameObjectName)
  {
  }

  private static void CloseWebView()
  {
  }

  private static int GetWebViewStatus()
  {
    return 0;
  }

  private static void ShowWebView(bool show)
  {
  }

  private static void ClearWebViewCookies()
  {
  }

  private static void ClearURLCache()
  {
  }

  private static string GetWebViewLoginCode()
  {
    return string.Empty;
  }

  private static string GetStoredLoginToken()
  {
    return string.Empty;
  }

  private static bool SetStoredLoginToken(string str)
  {
    return true;
  }

  private static void DeleteStoredLoginToken()
  {
  }

  private static void SetBreakingNews(string localized_title, string body, string gameObjectName)
  {
  }

  private static void SetRegionSelectVisualState(bool isVisible)
  {
  }

  private static void GoBack()
  {
  }

  private static void SetWebViewCountryCodeCookie(string countryCode, string domain)
  {
  }

  public enum Status
  {
    Closed,
    Loading,
    ReadyToDisplay,
    Processing,
    Success,
    Error,
    MAX,
  }

  public enum Error
  {
    InternalError,
  }
}
