﻿// Decompiled with JetBrains decompiler
// Type: GoldenHeroEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GoldenHeroEvent : MonoBehaviour
{
  private List<GoldenHeroEvent.AnimationDoneListener> m_animationDoneListeners = new List<GoldenHeroEvent.AnimationDoneListener>();
  public PlayMakerFSM m_playmaker;
  public Transform m_heroBone;
  public GameObject m_burningHero;
  private VictoryTwoScoop m_victoryTwoScoop;
  private CardDef m_VanillaHeroCardDef;

  private void Awake()
  {
    this.LoadVanillaHeroCardDef();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.m_playmaker.SendEvent("Action");
    this.m_victoryTwoScoop.HideXpBar();
    this.m_victoryTwoScoop.m_bannerLabel.Text = string.Empty;
  }

  public void Hide()
  {
    this.m_playmaker.SendEvent("Done");
    SoundManager.Get().LoadAndPlay("rank_window_shrink");
  }

  public void SetHeroBurnAwayTexture(Texture heroTexture)
  {
    this.m_burningHero.GetComponent<Renderer>().material.mainTexture = heroTexture;
  }

  public void HideTwoScoop()
  {
    this.m_victoryTwoScoop.Hide();
  }

  public void SetVictoryTwoScoop(VictoryTwoScoop twoScoop)
  {
    this.m_victoryTwoScoop = twoScoop;
  }

  public void SwapHeroToVanilla()
  {
    if ((Object) this.m_VanillaHeroCardDef == (Object) null)
      return;
    this.m_victoryTwoScoop.m_heroActor.SetCardDef(this.m_VanillaHeroCardDef);
    this.m_victoryTwoScoop.m_heroActor.UpdateAllComponents();
  }

  public void SwapMaterialToPremium()
  {
    this.m_victoryTwoScoop.m_heroActor.SetPremium(TAG_PREMIUM.GOLDEN);
    this.m_victoryTwoScoop.m_heroActor.UpdateAllComponents();
  }

  public void AnimationDone()
  {
    this.FireAnimationDoneEvent();
  }

  private void FireAnimationDoneEvent()
  {
    foreach (GoldenHeroEvent.AnimationDoneListener animationDoneListener in this.m_animationDoneListeners.ToArray())
      animationDoneListener();
  }

  public void RegisterAnimationDoneListener(GoldenHeroEvent.AnimationDoneListener listener)
  {
    if (this.m_animationDoneListeners.Contains(listener))
      return;
    this.m_animationDoneListeners.Add(listener);
  }

  public void RemoveAnimationDoneListener(GoldenHeroEvent.AnimationDoneListener listener)
  {
    this.m_animationDoneListeners.Remove(listener);
  }

  private void LoadVanillaHeroCardDef()
  {
    Player player = (Player) null;
    using (Map<int, Player>.ValueCollection.Enumerator enumerator = GameState.Get().GetPlayerMap().Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player current = enumerator.Current;
        if (current.GetSide() == Player.Side.FRIENDLY)
        {
          player = current;
          break;
        }
      }
    }
    if (player == null)
    {
      Debug.LogWarning((object) "GoldenHeroEvent.LoadVanillaHeroCardDef() - currentPlayer == null");
    }
    else
    {
      EntityDef heroEntityDef = player.GetHeroEntityDef();
      if (heroEntityDef.GetCardSet() != TAG_CARD_SET.HERO_SKINS)
        return;
      DefLoader.Get().LoadCardDef(CollectionManager.Get().GetVanillaHeroCardID(heroEntityDef), new DefLoader.LoadDefCallback<CardDef>(this.OnVanillaHeroCardDefLoaded), new object(), new CardPortraitQuality(3, TAG_PREMIUM.NORMAL));
    }
  }

  private void OnVanillaHeroCardDefLoaded(string cardId, CardDef def, object userData)
  {
    if ((Object) def == (Object) null)
      Debug.LogError((object) "GoldenHeroEvent.LoadDefaultHeroTexture() faild to load CardDef!");
    else
      this.m_VanillaHeroCardDef = def;
  }

  public delegate void AnimationDoneListener();
}
