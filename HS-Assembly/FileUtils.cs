﻿// Decompiled with JetBrains decompiler
// Type: FileUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class FileUtils
{
  public static readonly char[] FOLDER_SEPARATOR_CHARS = new char[2]{ '/', '\\' };

  public static string BasePersistentDataPath
  {
    get
    {
      return string.Format("{0}/Blizzard/Hearthstone", (object) Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData).Replace('\\', '/'));
    }
  }

  public static string PublicPersistentDataPath
  {
    get
    {
      return FileUtils.BasePersistentDataPath;
    }
  }

  public static string InternalPersistentDataPath
  {
    get
    {
      return string.Format("{0}/Dev", (object) FileUtils.BasePersistentDataPath);
    }
  }

  public static string PersistentDataPath
  {
    get
    {
      string path = !ApplicationMgr.IsInternal() ? FileUtils.PublicPersistentDataPath : FileUtils.InternalPersistentDataPath;
      if (!Directory.Exists(path))
      {
        try
        {
          Directory.CreateDirectory(path);
        }
        catch (Exception ex)
        {
          Debug.LogError((object) string.Format("FileUtils.PersistentDataPath - Error creating {0}. Exception={1}", (object) path, (object) ex.Message));
          Error.AddFatalLoc("GLOBAL_ERROR_ASSET_CREATE_PERSISTENT_DATA_PATH");
        }
      }
      return path;
    }
  }

  public static string CachePath
  {
    get
    {
      string path = string.Format("{0}/Cache", (object) FileUtils.PersistentDataPath);
      if (!Directory.Exists(path))
      {
        try
        {
          Directory.CreateDirectory(path);
        }
        catch (Exception ex)
        {
          Debug.LogError((object) string.Format("FileUtils.CachePath - Error creating {0}. Exception={1}", (object) path, (object) ex.Message));
        }
      }
      return path;
    }
  }

  public static string MakeSourceAssetPath(DirectoryInfo folder)
  {
    return FileUtils.MakeSourceAssetPath(folder.FullName);
  }

  public static string MakeSourceAssetPath(FileInfo fileInfo)
  {
    return FileUtils.MakeSourceAssetPath(fileInfo.FullName);
  }

  public static string MakeSourceAssetPath(string path)
  {
    string str = path.Replace("\\", "/");
    int num = str.IndexOf("/Assets", StringComparison.OrdinalIgnoreCase);
    return str.Remove(0, num + 1);
  }

  public static string MakeMetaPathFromSourcePath(string path)
  {
    return string.Format("{0}.meta", (object) path);
  }

  public static string MakeSourceAssetMetaPath(string path)
  {
    return FileUtils.MakeMetaPathFromSourcePath(FileUtils.MakeSourceAssetPath(path));
  }

  public static string MakeLocalizedPathFromSourcePath(Locale locale, string enUsPath)
  {
    string str = Path.GetDirectoryName(enUsPath);
    string fileName = Path.GetFileName(enUsPath);
    int startIndex = str.LastIndexOf("/");
    if (startIndex >= 0 && str.Substring(startIndex + 1).Equals(Localization.DEFAULT_LOCALE_NAME))
      str = str.Remove(startIndex);
    if (string.IsNullOrEmpty(str))
      return string.Format("{0}/{1}", (object) locale, (object) fileName);
    return string.Format("{0}/{1}/{2}", (object) str, (object) locale, (object) fileName);
  }

  public static Locale? GetLocaleFromSourcePath(string path)
  {
    string directoryName = Path.GetDirectoryName(path);
    int num = directoryName.LastIndexOf("/");
    if (num < 0)
      return new Locale?();
    string str = directoryName.Substring(num + 1);
    Locale locale;
    try
    {
      locale = EnumUtils.Parse<Locale>(str);
    }
    catch (Exception ex)
    {
      return new Locale?();
    }
    return new Locale?(locale);
  }

  public static Locale? GetForeignLocaleFromSourcePath(string path)
  {
    Locale? localeFromSourcePath = FileUtils.GetLocaleFromSourcePath(path);
    if (!localeFromSourcePath.HasValue)
      return new Locale?();
    if (localeFromSourcePath.Value == Locale.enUS)
      return new Locale?();
    return localeFromSourcePath;
  }

  public static bool IsForeignLocaleSourcePath(string path)
  {
    return FileUtils.GetForeignLocaleFromSourcePath(path).HasValue;
  }

  public static string StripLocaleFromPath(string path)
  {
    string directoryName = Path.GetDirectoryName(path);
    string fileName = Path.GetFileName(path);
    if (Localization.IsValidLocaleName(Path.GetFileName(directoryName)))
      return string.Format("{0}/{1}", (object) Path.GetDirectoryName(directoryName), (object) fileName);
    return path;
  }

  public static string GameToSourceAssetPath(string path, string dotExtension = ".prefab")
  {
    return string.Format("{0}{1}", (object) path, (object) dotExtension);
  }

  public static string GameToSourceAssetName(string folder, string name, string dotExtension = ".prefab")
  {
    return string.Format("{0}/{1}{2}", (object) folder, (object) name, (object) dotExtension);
  }

  public static string SourceToGameAssetPath(string path)
  {
    int length = path.LastIndexOf('.');
    if (length < 0)
      return path;
    return path.Substring(0, length);
  }

  public static string SourceToGameAssetName(string path)
  {
    int num = path.LastIndexOf('/');
    if (num < 0)
      return path;
    int length = path.LastIndexOf('.');
    if (length < 0)
      return path;
    return path.Substring(num + 1, length);
  }

  public static string GameAssetPathToName(string path)
  {
    if (string.IsNullOrEmpty(path))
      return path;
    int num = path.LastIndexOf('/');
    if (num < 0)
      return path;
    return path.Substring(num + 1);
  }

  public static string GetAssetPath(string fileName)
  {
    return fileName;
  }

  public static string GetPluginPath(string fileName)
  {
    string empty = string.Empty;
    return string.Format("Hearthstone_Data/Plugins/{0}", (object) fileName);
  }

  public static IntPtr LoadPlugin(string fileName, bool handleError = true)
  {
    try
    {
      string pluginPath = FileUtils.GetPluginPath(fileName);
      IntPtr num = DLLUtils.LoadLibrary(pluginPath);
      if (num == IntPtr.Zero && handleError)
      {
        Error.AddDevFatal("Failed to load plugin from '{0}'", (object) string.Format("{0}/{1}", (object) Directory.GetCurrentDirectory().Replace("\\", "/"), (object) pluginPath));
        Error.AddFatalLoc("GLOBAL_ERROR_ASSET_LOAD_FAILED", (object) fileName);
      }
      return num;
    }
    catch (Exception ex)
    {
      Error.AddDevFatal("FileUtils.LoadPlugin() - Exception occurred. message={0} stackTrace={1}", (object) ex.Message, (object) ex.StackTrace);
      return IntPtr.Zero;
    }
  }

  public static string GetOnDiskCapitalizationForFile(string filePath)
  {
    return FileUtils.GetOnDiskCapitalizationForFile(new FileInfo(filePath));
  }

  public static string GetOnDiskCapitalizationForDir(string dirPath)
  {
    return FileUtils.GetOnDiskCapitalizationForDir(new DirectoryInfo(dirPath));
  }

  public static string GetOnDiskCapitalizationForFile(FileInfo fileInfo)
  {
    DirectoryInfo directory = fileInfo.Directory;
    string name = directory.GetFiles(fileInfo.Name)[0].Name;
    return Path.Combine(FileUtils.GetOnDiskCapitalizationForDir(directory), name);
  }

  public static string GetOnDiskCapitalizationForDir(DirectoryInfo dirInfo)
  {
    DirectoryInfo parent = dirInfo.Parent;
    if (parent == null)
      return dirInfo.Name;
    string name = parent.GetDirectories(dirInfo.Name)[0].Name;
    return Path.Combine(FileUtils.GetOnDiskCapitalizationForDir(parent), name);
  }

  public static bool GetLastFolderAndFileFromPath(string path, out string folderName, out string fileName)
  {
    folderName = (string) null;
    fileName = (string) null;
    if (string.IsNullOrEmpty(path))
      return false;
    int num1 = path.LastIndexOfAny(FileUtils.FOLDER_SEPARATOR_CHARS);
    if (num1 > 0)
    {
      int num2 = path.LastIndexOfAny(FileUtils.FOLDER_SEPARATOR_CHARS, num1 - 1);
      int startIndex = num2 >= 0 ? num2 + 1 : 0;
      int length = num1 - startIndex;
      folderName = path.Substring(startIndex, length);
    }
    if (num1 < 0)
      fileName = path;
    else if (num1 < path.Length - 1)
      fileName = path.Substring(num1 + 1);
    if (folderName == null)
      return fileName != null;
    return true;
  }

  public static bool SetFolderWritableFlag(string dirPath, bool writable)
  {
    foreach (string file in Directory.GetFiles(dirPath))
      FileUtils.SetFileWritableFlag(file, writable);
    foreach (string directory in Directory.GetDirectories(dirPath))
      FileUtils.SetFolderWritableFlag(directory, writable);
    return true;
  }

  public static bool SetFileWritableFlag(string path, bool setWritable)
  {
    if (!File.Exists(path))
      return false;
    try
    {
      FileAttributes attributes = File.GetAttributes(path);
      FileAttributes fileAttributes = !setWritable ? attributes | FileAttributes.ReadOnly : attributes & ~FileAttributes.ReadOnly;
      if (setWritable && Environment.OSVersion.Platform == PlatformID.MacOSX)
        fileAttributes |= FileAttributes.Normal;
      if (fileAttributes == attributes)
        return true;
      File.SetAttributes(path, fileAttributes);
      return File.GetAttributes(path) == fileAttributes;
    }
    catch (DirectoryNotFoundException ex)
    {
    }
    catch (FileNotFoundException ex)
    {
    }
    catch (Exception ex)
    {
    }
    return false;
  }

  public static string GetMD5(string fileName)
  {
    if (!File.Exists(fileName))
      return string.Empty;
    using (FileStream fileStream = File.OpenRead(fileName))
      return BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash((Stream) fileStream)).Replace("-", string.Empty);
  }

  public static string GetMD5FromFoldersAndFiles(IEnumerable<string> folders, IEnumerable<string> files)
  {
    List<string> stringList = new List<string>();
    foreach (string folder in folders)
    {
      if (!Directory.Exists(folder))
        return string.Empty;
      stringList.AddRange((IEnumerable<string>) Directory.GetFiles(folder));
    }
    foreach (string file in files)
    {
      if (!File.Exists(file))
        stringList.Add(file);
    }
    return FileUtils.GetMD5FromFiles((IEnumerable<string>) stringList);
  }

  public static string GetMD5FromFiles(IEnumerable<string> files)
  {
    float realtimeSinceStartup1 = Time.realtimeSinceStartup;
    List<byte> byteList = new List<byte>();
    MD5CryptoServiceProvider cryptoServiceProvider = new MD5CryptoServiceProvider();
    int num = 0;
    foreach (string file in files)
    {
      using (FileStream fileStream = File.OpenRead(file))
      {
        byteList.AddRange((IEnumerable<byte>) cryptoServiceProvider.ComputeHash((Stream) fileStream));
        ++num;
      }
    }
    string str = BitConverter.ToString(cryptoServiceProvider.ComputeHash(byteList.ToArray())).Replace("-", string.Empty);
    float realtimeSinceStartup2 = Time.realtimeSinceStartup;
    Debug.LogFormat("Hashing {0} files took {1}s, HASH={1}", (object) num, (object) (float) ((double) realtimeSinceStartup2 - (double) realtimeSinceStartup1), (object) str);
    return str;
  }

  public static string GetMD5FromString(string buf)
  {
    return BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(buf))).Replace("-", string.Empty);
  }

  public static string CombinePath(params object[] args)
  {
    string path1 = string.Empty;
    foreach (object obj in args)
    {
      if (obj != null)
      {
        string path2 = obj.ToString();
        if (!string.IsNullOrEmpty(path2))
          path1 = Path.Combine(path1, path2);
      }
    }
    return path1;
  }
}
