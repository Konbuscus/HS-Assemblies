﻿// Decompiled with JetBrains decompiler
// Type: ButtonListMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class ButtonListMenu : MonoBehaviour
{
  protected static readonly Vector3 HIDDEN_SCALE = 0.01f * Vector3.one;
  private List<UIBButton> m_allButtons = new List<UIBButton>();
  private List<GameObject> m_horizontalDividers = new List<GameObject>();
  protected float PUNCH_SCALE = 1.08f;
  protected Vector3 NORMAL_SCALE = Vector3.one;
  protected string m_menuDefPrefab = "ButtonListMenuDef";
  protected ButtonListMenuDef m_menu;
  private bool m_isShown;
  protected PegUIElement m_blocker;
  protected Transform m_menuParent;

  protected virtual void Awake()
  {
    GameObject gameObject = (GameObject) GameUtils.InstantiateGameObject(this.m_menuDefPrefab, (GameObject) null, false);
    this.m_menu = gameObject.GetComponent<ButtonListMenuDef>();
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    this.SetTransform();
    this.m_blocker = CameraUtils.CreateInputBlocker(CameraUtils.FindFirstByLayer(gameObject.layer), "GameMenuInputBlocker", (Component) this, (Component) gameObject.transform, 10f).AddComponent<PegUIElement>();
    FatalErrorMgr.Get().AddErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
    this.m_blocker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBlockerRelease));
  }

  protected virtual void OnDestroy()
  {
    FatalErrorMgr.Get().RemoveErrorListener(new FatalErrorMgr.ErrorCallback(this.OnFatalError));
  }

  public virtual void Show()
  {
    UniversalInputManager.Get().CancelTextInput(this.gameObject, true);
    this.SetTransform();
    SoundManager.Get().LoadAndPlay("Small_Click");
    this.gameObject.SetActive(true);
    UniversalInputManager.Get().SetGameDialogActive(true);
    this.HideAllButtons();
    this.LayoutMenu();
    this.m_isShown = true;
    TransformUtil.SetLocalScaleToWorldDimension(this.m_menu.m_headerMiddle, new WorldDimensionIndex(this.m_menu.m_headerText.GetTextBounds().size.x, 0));
    this.m_menu.m_header.UpdateSlices();
    AnimationUtil.ShowWithPunch(this.m_menu.gameObject, ButtonListMenu.HIDDEN_SCALE, this.PUNCH_SCALE * this.NORMAL_SCALE, this.NORMAL_SCALE, (string) null, true, (GameObject) null, (object) null, (AnimationUtil.DelOnShownWithPunch) null);
  }

  public virtual void Hide()
  {
    if ((Object) this.gameObject != (Object) null)
      this.gameObject.SetActive(false);
    UniversalInputManager.Get().SetGameDialogActive(false);
    this.m_isShown = false;
  }

  public bool IsShown()
  {
    return this.m_isShown;
  }

  public UIBButton CreateMenuButton(string name, string buttonTextString, UIEvent.Handler releaseHandler)
  {
    UIBButton uibButton = (UIBButton) GameUtils.Instantiate((Component) this.m_menu.m_templateButton, this.m_menu.m_buttonContainer.gameObject, false);
    uibButton.SetText(GameStrings.Get(buttonTextString));
    if (name != null)
      uibButton.gameObject.name = name;
    uibButton.AddEventListener(UIEventType.RELEASE, releaseHandler);
    uibButton.transform.localRotation = this.m_menu.m_templateButton.transform.localRotation;
    this.m_allButtons.Add(uibButton);
    return uibButton;
  }

  protected abstract List<UIBButton> GetButtons();

  protected void SetTransform()
  {
    if ((Object) this.m_menuParent == (Object) null)
      this.m_menuParent = this.transform;
    TransformUtil.AttachAndPreserveLocalTransform(this.m_menu.transform, this.m_menuParent);
    if ((Object) this.m_blocker != (Object) null)
    {
      this.m_blocker.transform.localPosition = new Vector3(0.0f, -5f, 0.0f);
      this.m_blocker.transform.eulerAngles = new Vector3(90f, 0.0f, 0.0f);
    }
    SceneUtils.SetLayer((Component) this, GameLayer.UI);
    this.m_menu.gameObject.transform.localScale = this.NORMAL_SCALE;
  }

  protected virtual void LayoutMenu()
  {
    this.LayoutMenuButtons();
    this.m_menu.m_buttonContainer.UpdateSlices();
    this.LayoutMenuBackground();
  }

  protected void LayoutMenuButtons()
  {
    List<UIBButton> buttons = this.GetButtons();
    this.m_menu.m_buttonContainer.ClearSlices();
    int index1 = 0;
    int index2 = 0;
    for (; index1 < buttons.Count; ++index1)
    {
      UIBButton uibButton = buttons[index1];
      Vector3 minLocalPadding = Vector3.zero;
      bool reverse = false;
      GameObject gameObject1;
      if ((Object) uibButton == (Object) null)
      {
        GameObject gameObject2;
        if (index2 >= this.m_horizontalDividers.Count)
        {
          gameObject2 = (GameObject) GameUtils.Instantiate(this.m_menu.m_templateHorizontalDivider, this.m_menu.m_buttonContainer.gameObject, false);
          gameObject2.transform.localRotation = this.m_menu.m_templateHorizontalDivider.transform.localRotation;
          this.m_horizontalDividers.Add(gameObject2);
        }
        else
          gameObject2 = this.m_horizontalDividers[index2];
        ++index2;
        gameObject1 = gameObject2;
        minLocalPadding = this.m_menu.m_horizontalDividerMinPadding;
        reverse = true;
      }
      else
        gameObject1 = uibButton.gameObject;
      this.m_menu.m_buttonContainer.AddSlice(gameObject1, minLocalPadding, Vector3.zero, reverse);
      gameObject1.SetActive(true);
    }
  }

  protected void LayoutMenuBackground()
  {
    OrientedBounds orientedWorldBounds = TransformUtil.ComputeOrientedWorldBounds(this.m_menu.m_buttonContainer.gameObject, true);
    float width = orientedWorldBounds.Extents[0].magnitude * 2f;
    float height = orientedWorldBounds.Extents[2].magnitude * 2f;
    this.m_menu.m_background.SetSize(width, height);
    this.m_menu.m_border.SetSize(width, height);
  }

  private void OnFatalError(FatalErrorMessage message, object userData)
  {
    this.Hide();
  }

  private void HideAllButtons()
  {
    for (int index = 0; index < this.m_allButtons.Count; ++index)
      this.m_allButtons[index].gameObject.SetActive(false);
    for (int index = 0; index < this.m_horizontalDividers.Count; ++index)
      this.m_horizontalDividers[index].SetActive(false);
  }

  private void OnBlockerRelease(UIEvent e)
  {
    SoundManager.Get().LoadAndPlay("Small_Click");
    this.Hide();
  }
}
