﻿// Decompiled with JetBrains decompiler
// Type: SpellTableEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class SpellTableEntry
{
  [CustomEditField(T = EditType.SPELL)]
  public string m_SpellPrefabName = string.Empty;
  public SpellType m_Type;
  [CustomEditField(Hide = true)]
  public Spell m_Spell;
}
