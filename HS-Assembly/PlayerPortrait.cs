﻿// Decompiled with JetBrains decompiler
// Type: PlayerPortrait
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using UnityEngine;

public class PlayerPortrait : MonoBehaviour
{
  private BnetProgramId m_programId;
  private string m_currentTextureName;
  private string m_loadingTextureName;

  public BnetProgramId GetProgramId()
  {
    return this.m_programId;
  }

  public bool SetProgramId(BnetProgramId programId)
  {
    if ((bgs.FourCC) this.m_programId == (bgs.FourCC) programId)
      return false;
    this.m_programId = programId;
    this.UpdateIcon();
    return true;
  }

  public bool IsIconReady()
  {
    if (this.m_loadingTextureName == null)
      return this.m_currentTextureName != null;
    return false;
  }

  public bool IsIconLoading()
  {
    return this.m_loadingTextureName != null;
  }

  private void UpdateIcon()
  {
    if ((bgs.FourCC) this.m_programId == (bgs.FourCC) null)
    {
      this.m_currentTextureName = (string) null;
      this.m_loadingTextureName = (string) null;
      this.GetComponent<Renderer>().material.mainTexture = (Texture) null;
    }
    else
    {
      string textureName = BnetProgramId.GetTextureName(this.m_programId);
      if (this.m_currentTextureName == textureName || this.m_loadingTextureName == textureName)
        return;
      this.m_loadingTextureName = textureName;
      AssetLoader.Get().LoadTexture(this.m_loadingTextureName, new AssetLoader.ObjectCallback(this.OnTextureLoaded), (object) null, false);
    }
  }

  private void OnTextureLoaded(string name, Object obj, object callbackData)
  {
    if (name != this.m_loadingTextureName)
      return;
    Texture texture = obj as Texture;
    if ((Object) texture == (Object) null)
    {
      Error.AddDevFatal("PlayerPortrait.OnTextureLoaded() - Failed to load {0}. ProgramId={1}", (object) name, (object) this.m_programId);
      this.m_currentTextureName = (string) null;
      this.m_loadingTextureName = (string) null;
    }
    else
    {
      this.m_currentTextureName = this.m_loadingTextureName;
      this.m_loadingTextureName = (string) null;
      this.GetComponent<Renderer>().material.mainTexture = texture;
    }
  }
}
