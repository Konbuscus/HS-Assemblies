﻿// Decompiled with JetBrains decompiler
// Type: AdventureStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class AdventureStore : Store
{
  private static readonly int NUM_BUNDLE_ITEMS_REQUIRED = 1;
  [CustomEditField(Sections = "UI")]
  public UIBButton m_BuyDungeonButton;
  [CustomEditField(Sections = "UI")]
  public UberText m_Headline;
  [CustomEditField(Sections = "UI")]
  public UberText m_DetailsText1;
  [CustomEditField(Sections = "UI")]
  public UberText m_DetailsText2;
  [CustomEditField(Sections = "UI")]
  public GameObject m_BuyWithMoneyButtonOpaqueCover;
  [CustomEditField(Sections = "UI")]
  public GameObject m_BuyWithGoldButtonOpaqueCover;
  [CustomEditField(Sections = "UI")]
  public GameObject m_BuyDungeonButtonOpaqueCover;
  private bool m_animating;
  private Network.Bundle m_bundle;
  private Network.Bundle m_fullAdventureBundle;
  private bool m_fullAdventureBundleProductExists;

  protected override void Start()
  {
    base.Start();
    this.m_BuyDungeonButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnBuyDungeonPressed));
    this.m_offClicker.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.Close()));
  }

  public void SetAdventureProduct(ProductType product, int productData)
  {
    string productName = StoreManager.Get().GetProductName(new List<Network.BundleItem>() { new Network.BundleItem() { Product = product, ProductData = productData, Quantity = 1 } });
    this.m_Headline.Text = productName;
    string str1 = string.Empty;
    switch (product)
    {
      case ProductType.PRODUCT_TYPE_NAXX:
        str1 = "NAXX";
        break;
      case ProductType.PRODUCT_TYPE_BRM:
        str1 = "BRM";
        break;
      case ProductType.PRODUCT_TYPE_LOE:
        str1 = "LOE";
        break;
      case ProductType.PRODUCT_TYPE_WING:
        str1 = GameUtils.GetAdventureProductStringKey(productData);
        break;
    }
    string key1 = string.Format("GLUE_STORE_PRODUCT_DETAILS_{0}_PART_1", (object) str1);
    string key2 = string.Format("GLUE_STORE_PRODUCT_DETAILS_{0}_PART_2", (object) str1);
    string nameShort = (string) GameDbf.Wing.GetRecord(productData).NameShort;
    string str2 = string.IsNullOrEmpty(nameShort) ? productName : nameShort;
    this.m_DetailsText1.Text = GameStrings.Format(key1, (object) str2);
    this.m_DetailsText2.Text = GameStrings.Format(key2);
    bool productTypeExists = false;
    List<Network.Bundle> bundlesForProduct = StoreManager.Get().GetAvailableBundlesForProduct(product, false, false, out productTypeExists, productData, AdventureStore.NUM_BUNDLE_ITEMS_REQUIRED);
    if (bundlesForProduct.Count == 1)
    {
      this.m_bundle = bundlesForProduct[0];
    }
    else
    {
      Debug.LogWarning((object) string.Format("AdventureStore.SetAdventureProduct(): expected to find 1 available bundle for product {0} productData {1}, found {2}", (object) product, (object) productData, (object) bundlesForProduct.Count));
      this.m_bundle = (Network.Bundle) null;
    }
    StoreManager.Get().GetAvailableAdventureBundle(GameUtils.GetAdventureIdByWingId(productData), GeneralStoreAdventureContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, out this.m_fullAdventureBundle, out this.m_fullAdventureBundleProductExists);
  }

  public override void Hide()
  {
    this.m_shown = false;
    StoreManager.Get().RemoveAuthorizationExitListener(new StoreManager.AuthorizationExitCallback(this.OnAuthExit));
    this.EnableFullScreenEffects(false);
    this.DoHideAnimation((UIBPopup.OnAnimationComplete) null);
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    BnetBar.Get().SetCurrencyType(new CurrencyFrame.CurrencyType?());
    BnetBar.Get().UpdateForPhone();
  }

  public override void OnMoneySpent()
  {
    this.UpdateMoneyButtonState();
  }

  public override void OnGoldBalanceChanged(NetCache.NetCacheGoldBalance balance)
  {
    this.UpdateGoldButtonState(balance);
  }

  public override void Close()
  {
    Navigation.GoBack();
  }

  protected override void ShowImpl(Store.DelOnStoreShown onStoreShownCB, bool isTotallyFake)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureStore.\u003CShowImpl\u003Ec__AnonStorey3DD implCAnonStorey3Dd = new AdventureStore.\u003CShowImpl\u003Ec__AnonStorey3DD();
    // ISSUE: reference to a compiler-generated field
    implCAnonStorey3Dd.onStoreShownCB = onStoreShownCB;
    // ISSUE: reference to a compiler-generated field
    implCAnonStorey3Dd.\u003C\u003Ef__this = this;
    if (this.m_shown)
      return;
    this.m_shown = true;
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    StoreManager.Get().RegisterAuthorizationExitListener(new StoreManager.AuthorizationExitCallback(this.OnAuthExit));
    this.EnableFullScreenEffects(true);
    this.SetUpBuyButtons();
    this.m_animating = true;
    // ISSUE: reference to a compiler-generated method
    this.DoShowAnimation(new UIBPopup.OnAnimationComplete(implCAnonStorey3Dd.\u003C\u003Em__19E));
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    BnetBar.Get().SetCurrencyType(new CurrencyFrame.CurrencyType?(CurrencyFrame.CurrencyType.GOLD));
    BnetBar.Get().UpdateForPhone();
  }

  protected override void BuyWithGold(UIEvent e)
  {
    if (this.m_bundle == null || this.m_animating)
      return;
    this.FireBuyWithGoldEventGTAPP(this.m_bundle.ProductID, 1);
  }

  protected override void BuyWithMoney(UIEvent e)
  {
    if (this.m_bundle == null || this.m_animating)
      return;
    this.FireBuyWithMoneyEvent(this.m_bundle.ProductID, 1);
  }

  private void OnAuthExit(object userData)
  {
    this.ActivateCover(false);
    SceneUtils.SetLayer(this.gameObject, GameLayer.Default);
    this.EnableFullScreenEffects(false);
    StoreManager.Get().RemoveAuthorizationExitListener(new StoreManager.AuthorizationExitCallback(this.OnAuthExit));
    this.FireExitEvent(true);
  }

  private void UpdateMoneyButtonState()
  {
    Store.BuyButtonState state = Store.BuyButtonState.ENABLED;
    bool flag = false;
    if (this.m_bundle == null)
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsOpen())
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsBattlePayFeatureEnabled())
      state = Store.BuyButtonState.DISABLED_FEATURE;
    else if (!StoreManager.Get().IsBundleRealMoneyOptionAvailableNow(this.m_bundle))
    {
      state = Store.BuyButtonState.DISABLED_NO_TOOLTIP;
      flag = true;
    }
    this.m_BuyWithMoneyButtonOpaqueCover.SetActive(flag);
    this.SetMoneyButtonState(state);
  }

  private void UpdateGoldButtonState(NetCache.NetCacheGoldBalance balance)
  {
    Store.BuyButtonState state = Store.BuyButtonState.ENABLED;
    bool flag = false;
    if (this.m_bundle == null)
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsOpen())
      state = Store.BuyButtonState.DISABLED;
    else if (!StoreManager.Get().IsBuyWithGoldFeatureEnabled())
      state = Store.BuyButtonState.DISABLED_FEATURE;
    else if (!StoreManager.Get().IsBundleGoldOptionAvailableNow(this.m_bundle))
    {
      state = Store.BuyButtonState.DISABLED_NO_TOOLTIP;
      flag = true;
    }
    else if (balance == null)
      state = Store.BuyButtonState.DISABLED;
    else if (balance.GetTotal() < this.m_bundle.GoldCost.Value)
      state = Store.BuyButtonState.DISABLED_NOT_ENOUGH_GOLD;
    this.m_BuyWithGoldButtonOpaqueCover.SetActive(flag);
    this.SetGoldButtonState(state);
  }

  private void SetUpBuyButtons()
  {
    this.SetUpBuyWithGoldButton();
    this.SetUpBuyWithMoneyButton();
    this.SetUpBuyFullAdventureButton();
  }

  private void SetUpBuyWithGoldButton()
  {
    string empty = string.Empty;
    string text;
    if (this.m_bundle != null)
    {
      text = this.m_bundle.GoldCost.ToString();
      this.UpdateGoldButtonState(NetCache.Get().GetNetObject<NetCache.NetCacheGoldBalance>());
    }
    else
    {
      Debug.LogWarning((object) "AdventureStore.SetUpBuyWithGoldButton(): m_bundle is null");
      text = GameStrings.Get("GLUE_STORE_PRODUCT_PRICE_NA");
      this.SetGoldButtonState(Store.BuyButtonState.DISABLED);
    }
    this.m_buyWithGoldButton.SetText(text);
  }

  private void SetUpBuyWithMoneyButton()
  {
    string empty = string.Empty;
    string text;
    if (this.m_bundle != null)
    {
      text = StoreManager.Get().FormatCostBundle(this.m_bundle);
      this.UpdateMoneyButtonState();
    }
    else
    {
      Debug.LogWarning((object) "AdventureStore.SetUpBuyWithMoneyButton(): m_bundle is null");
      text = GameStrings.Get("GLUE_STORE_PRODUCT_PRICE_NA");
      this.SetMoneyButtonState(Store.BuyButtonState.DISABLED);
    }
    this.m_buyWithMoneyButton.SetText(text);
  }

  private void SetUpBuyFullAdventureButton()
  {
    string empty = string.Empty;
    bool flag = false;
    string text;
    if (this.m_fullAdventureBundle != null)
      text = GameStrings.Format("GLUE_STORE_DUNGEON_BUTTON_TEXT", (object) this.m_fullAdventureBundle.Items.Count, (object) StoreManager.Get().FormatCostBundle(this.m_fullAdventureBundle));
    else if (this.m_fullAdventureBundleProductExists)
    {
      flag = false;
      text = GameStrings.Get("GLUE_STORE_PRODUCT_PRICE_NA");
    }
    else
    {
      flag = true;
      text = string.Empty;
    }
    this.m_BuyDungeonButton.SetText(text);
    this.m_BuyDungeonButtonOpaqueCover.SetActive(flag);
    this.m_BuyDungeonButton.SetEnabled(!flag);
  }

  private void OnBuyDungeonPressed(UIEvent e)
  {
    this.Close();
    Options.Get().SetInt(Option.LAST_SELECTED_STORE_ADVENTURE_ID, (int) AdventureConfig.Get().GetSelectedAdventure());
    StoreManager.Get().StartGeneralTransaction(GeneralStoreMode.ADVENTURE);
  }

  private bool OnNavigateBack()
  {
    this.Hide();
    this.FireExitEvent(false);
    return true;
  }
}
