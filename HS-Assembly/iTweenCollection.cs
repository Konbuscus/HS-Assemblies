﻿// Decompiled with JetBrains decompiler
// Type: iTweenCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

public class iTweenCollection
{
  public iTween[] Tweens = new iTween[256];
  public int LastIndex;
  public int Count;
  public int DeletedCount;

  public void Add(iTween tween)
  {
    if (tween == null)
      return;
    if (this.LastIndex >= this.Tweens.Length)
      Array.Resize<iTween>(ref this.Tweens, this.Tweens.Length * 2);
    this.Tweens[this.LastIndex] = tween;
    ++this.LastIndex;
    ++this.Count;
  }

  public void Remove(iTween tween)
  {
    if (tween == null || tween.destroyed)
      return;
    for (int index = 0; index < this.LastIndex; ++index)
    {
      if (this.Tweens[index] == tween)
      {
        this.Tweens[index].destroyed = true;
        this.Tweens[index] = (iTween) null;
        --this.Count;
        ++this.DeletedCount;
        break;
      }
    }
  }

  public iTweenIterator GetIterator()
  {
    return new iTweenIterator(this);
  }

  public void CleanUp()
  {
    if (this.DeletedCount == 0)
      return;
    int index1 = 0;
    for (int index2 = 0; index2 < this.LastIndex; ++index2)
    {
      if (this.Tweens[index2] != null)
      {
        this.Tweens[index1] = this.Tweens[index2];
        ++index1;
      }
    }
    this.LastIndex -= this.DeletedCount;
    this.DeletedCount = 0;
  }
}
