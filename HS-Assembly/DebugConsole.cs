﻿// Decompiled with JetBrains decompiler
// Type: DebugConsole
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using BobNetProto;
using System.Collections.Generic;
using System.ComponentModel;

public class DebugConsole
{
  private static DebugConsole s_instance;
  private bool m_initialized;
  private static Map<string, DebugConsole.ConsoleCallbackInfo> s_serverConsoleCallbackMap;
  private static Map<string, DebugConsole.ConsoleCallbackInfo> s_clientConsoleCallbackMap;

  private static List<DebugConsole.CommandParamDecl> CreateParamDeclList(params DebugConsole.CommandParamDecl[] paramDecls)
  {
    List<DebugConsole.CommandParamDecl> commandParamDeclList = new List<DebugConsole.CommandParamDecl>();
    foreach (DebugConsole.CommandParamDecl paramDecl in paramDecls)
      commandParamDeclList.Add(paramDecl);
    return commandParamDeclList;
  }

  private void InitConsoleCallbackMaps()
  {
    this.InitClientConsoleCallbackMap();
    this.InitServerConsoleCallbackMap();
  }

  private void InitServerConsoleCallbackMap()
  {
    if (DebugConsole.s_serverConsoleCallbackMap != null)
      return;
    DebugConsole.s_serverConsoleCallbackMap = new Map<string, DebugConsole.ConsoleCallbackInfo>();
    DebugConsole.s_serverConsoleCallbackMap.Add("spawncard", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.STR, "cardGUID"), new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "playerID"), new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.STR, "zoneName"), new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "premium"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("drawcard", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "playerID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("shuffle", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "playerID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("cyclehand", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "playerID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("nuke", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "playerID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("damage", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "entityID"), new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "damage"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("addmana", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "playerID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("readymana", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "playerID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("maxmana", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "playerID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("nocosts", new DebugConsole.ConsoleCallbackInfo(true, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList()));
    DebugConsole.s_serverConsoleCallbackMap.Add("healhero", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "playerID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("healentity", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "entityID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("ready", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "entityID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("exhaust", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "entityID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("freeze", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "entityID"))));
    DebugConsole.s_serverConsoleCallbackMap.Add("move", new DebugConsole.ConsoleCallbackInfo(1 != 0, (DebugConsole.ConsoleCallback) null, DebugConsole.CreateParamDeclList(new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "entityID"), new DebugConsole.CommandParamDecl(DebugConsole.CommandParamDecl.ParamType.I32, "zoneID"))));
  }

  private void InitClientConsoleCallbackMap()
  {
    if (DebugConsole.s_clientConsoleCallbackMap != null)
      return;
    DebugConsole.s_clientConsoleCallbackMap = new Map<string, DebugConsole.ConsoleCallbackInfo>();
  }

  private void SendDebugConsoleResponse(DebugConsole.DebugConsoleResponseType type, string message)
  {
    Network.SendDebugConsoleResponse((int) type, message);
  }

  private void SendConsoleCmdToServer(string commandName, List<string> commandParams)
  {
    if (!DebugConsole.s_serverConsoleCallbackMap.ContainsKey(commandName))
      return;
    string command = commandName;
    using (List<string>.Enumerator enumerator = commandParams.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        command = command + " " + current;
      }
    }
    if (Network.SendDebugConsoleCommand(command))
      return;
    this.SendDebugConsoleResponse(DebugConsole.DebugConsoleResponseType.CONSOLE_OUTPUT, string.Format("Cannot send command '{0}'; not currently connected to a game server.", (object) commandName));
  }

  private void OnCommandReceived()
  {
    string[] strArray = Network.GetDebugConsoleCommand().Split(' ');
    if (strArray.Length == 0)
    {
      Log.Rachelle.Print("Received empty command from debug console!");
    }
    else
    {
      string index1 = strArray[0];
      List<string> commandParams = new List<string>();
      for (int index2 = 1; index2 < strArray.Length; ++index2)
        commandParams.Add(strArray[index2]);
      if (DebugConsole.s_serverConsoleCallbackMap.ContainsKey(index1))
        this.SendConsoleCmdToServer(index1, commandParams);
      else if (!DebugConsole.s_clientConsoleCallbackMap.ContainsKey(index1))
      {
        this.SendDebugConsoleResponse(DebugConsole.DebugConsoleResponseType.CONSOLE_OUTPUT, string.Format("Unknown command '{0}'.", (object) index1));
      }
      else
      {
        DebugConsole.ConsoleCallbackInfo clientConsoleCallback = DebugConsole.s_clientConsoleCallbackMap[index1];
        if (clientConsoleCallback.GetNumParams() != commandParams.Count)
        {
          this.SendDebugConsoleResponse(DebugConsole.DebugConsoleResponseType.CONSOLE_OUTPUT, string.Format("Invalid params for command '{0}'.", (object) index1));
        }
        else
        {
          Log.Rachelle.Print(string.Format("Processing command '{0}' from debug console.", (object) index1));
          clientConsoleCallback.Callback(commandParams);
        }
      }
    }
  }

  private void OnCommandResponseReceived()
  {
    Network.DebugConsoleResponse debugConsoleResponse = Network.GetDebugConsoleResponse();
    if (debugConsoleResponse == null)
      return;
    this.SendDebugConsoleResponse((DebugConsole.DebugConsoleResponseType) debugConsoleResponse.Type, debugConsoleResponse.Response);
  }

  public static DebugConsole Get()
  {
    if (DebugConsole.s_instance == null)
      DebugConsole.s_instance = new DebugConsole();
    return DebugConsole.s_instance;
  }

  public void Init()
  {
    if (this.m_initialized)
      return;
    this.InitConsoleCallbackMaps();
    Network network = Network.Get();
    network.RegisterNetHandler((object) DebugConsoleCommand.PacketID.ID, new Network.NetHandler(this.OnCommandReceived), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) BobNetProto.DebugConsoleResponse.PacketID.ID, new Network.NetHandler(this.OnCommandResponseReceived), (Network.TimeoutHandler) null);
    this.m_initialized = true;
  }

  private class CommandParamDecl
  {
    public string Name;
    public DebugConsole.CommandParamDecl.ParamType Type;

    public CommandParamDecl(DebugConsole.CommandParamDecl.ParamType type, string name)
    {
      this.Type = type;
      this.Name = name;
    }

    public enum ParamType
    {
      [Description("string")] STR,
      [Description("int32")] I32,
      [Description("float32")] F32,
      [Description("bool")] BOOL,
    }
  }

  private class ConsoleCallbackInfo
  {
    public bool DisplayInCommandList;
    public List<DebugConsole.CommandParamDecl> ParamList;
    public DebugConsole.ConsoleCallback Callback;

    public ConsoleCallbackInfo(bool displayInCmdList, DebugConsole.ConsoleCallback callback, DebugConsole.CommandParamDecl[] commandParams)
    {
      this.DisplayInCommandList = displayInCmdList;
      this.ParamList = new List<DebugConsole.CommandParamDecl>((IEnumerable<DebugConsole.CommandParamDecl>) commandParams);
      this.Callback = callback;
    }

    public ConsoleCallbackInfo(bool displayInCmdList, DebugConsole.ConsoleCallback callback, List<DebugConsole.CommandParamDecl> commandParams)
      : this(displayInCmdList, callback, commandParams.ToArray())
    {
    }

    public int GetNumParams()
    {
      return this.ParamList.Count;
    }
  }

  private enum DebugConsoleResponseType
  {
    CONSOLE_OUTPUT,
    LOG_MESSAGE,
  }

  private delegate void ConsoleCallback(List<string> commandParams);
}
