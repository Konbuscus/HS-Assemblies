﻿// Decompiled with JetBrains decompiler
// Type: KAR06_Crone
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR06_Crone : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Crone_Female_Troll_CroneEmoteResponse_02");
    this.PreloadSound("VO_Crone_Female_Troll_CroneFlyingMonkeys_01");
    this.PreloadSound("VO_Crone_Female_Troll_CroneHeroPower_02");
    this.PreloadSound("VO_KARA_04_01_Female_Human_CroneLionTigerBear_02");
    this.PreloadSound("VO_KARA_04_01_Female_Human_CroneHuffer_01");
    this.PreloadSound("VO_KARA_04_01_Female_Human_CroneTurn1_01");
    this.PreloadSound("VO_KARA_04_01_Female_Human_CroneTurn3_01");
    this.PreloadSound("VO_Moroes_Male_Human_CroneTurn5_02");
    this.PreloadSound("VO_Moroes_Male_Human_CroneWin_01");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Crone_Female_Troll_CroneHeroPower_02",
            m_stringTag = "VO_Crone_Female_Troll_CroneHeroPower_02"
          }
        }
      }
    };
  }

  private Actor GetDorothee()
  {
    Player friendlySidePlayer = GameState.Get().GetFriendlySidePlayer();
    using (List<Card>.Enumerator enumerator = friendlySidePlayer.GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity = enumerator.Current.GetEntity();
        if (entity.GetControllerId() == friendlySidePlayer.GetPlayerId() && entity.GetCardId() == "KARA_04_01")
          return entity.GetCard().GetActor();
      }
    }
    return (Actor) null;
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR06_Crone.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1A1() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR06_Crone.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1A2() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR06_Crone.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1A3() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR06_Crone.\u003CHandleGameOverWithTiming\u003Ec__Iterator1A4() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
