﻿// Decompiled with JetBrains decompiler
// Type: DeckTrayContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DeckTrayContent : MonoBehaviour
{
  private bool m_isModeActive;
  private bool m_isModeTrying;

  public virtual void Show(bool showAll = false)
  {
  }

  public virtual void Hide(bool hideAll = false)
  {
  }

  public virtual void OnContentLoaded()
  {
  }

  public virtual bool IsContentLoaded()
  {
    return true;
  }

  public virtual bool PreAnimateContentEntrance()
  {
    return true;
  }

  public virtual bool PostAnimateContentEntrance()
  {
    return true;
  }

  public virtual bool AnimateContentEntranceStart()
  {
    return true;
  }

  public virtual bool AnimateContentEntranceEnd()
  {
    return true;
  }

  public virtual bool AnimateContentExitStart()
  {
    return true;
  }

  public virtual bool AnimateContentExitEnd()
  {
    return true;
  }

  public virtual bool PreAnimateContentExit()
  {
    return true;
  }

  public virtual bool PostAnimateContentExit()
  {
    return true;
  }

  public virtual void OnTaggedDeckChanged(CollectionManager.DeckTag tag, CollectionDeck newDeck, CollectionDeck oldDeck, bool isNewDeck)
  {
  }

  public bool IsModeActive()
  {
    return this.m_isModeActive;
  }

  public bool IsModeTryingOrActive()
  {
    if (!this.m_isModeTrying)
      return this.m_isModeActive;
    return true;
  }

  public void SetModeActive(bool active)
  {
    this.m_isModeActive = active;
  }

  public void SetModeTrying(bool trying)
  {
    this.m_isModeTrying = trying;
  }
}
