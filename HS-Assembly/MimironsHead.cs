﻿// Decompiled with JetBrains decompiler
// Type: MimironsHead
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class MimironsHead : SuperSpell
{
  private List<Card> m_mechMinions = new List<Card>();
  private Color m_clear = new Color(1f, 1f, 1f, 0.0f);
  private Map<GameObject, List<GameObject>> m_cleanup = new Map<GameObject, List<GameObject>>();
  private float m_flashDelay = 0.15f;
  private float m_mimironHighTime = 1.5f;
  private float m_minionHighTime = 2f;
  private float m_sparkDelay = 0.3f;
  private float m_absorbTime = 1f;
  private float m_glowTime = 0.5f;
  public GameObject m_root;
  public GameObject m_highPosBone;
  public GameObject m_minionPosBone;
  public GameObject m_background;
  public GameObject m_minionElectricity;
  public GameObject m_minionGlow;
  public GameObject m_mimironNegative;
  public GameObject m_mimironFlare;
  public GameObject m_mimironGlow;
  public GameObject m_mimironElectricity;
  public Spell m_voltSpawnOverrideSpell;
  public string m_perMinionSound;
  public string[] m_startSounds;
  private Card m_volt;
  private Card m_mimiron;
  private Transform m_voltParent;
  private bool m_isNegFlash;
  private PowerTaskList m_waitForTaskList;

  public override bool AddPowerTargets()
  {
    if (!this.CanAddPowerTargets())
      return false;
    Card card1 = this.m_taskList.GetSourceEntity().GetCard();
    if (this.m_taskList.IsOrigin())
    {
      List<PowerTaskList> powerTaskListList = new List<PowerTaskList>();
      for (PowerTaskList powerTaskList = this.m_taskList; powerTaskList != null; powerTaskList = powerTaskList.GetNext())
        powerTaskListList.Add(powerTaskList);
      using (List<PowerTaskList>.Enumerator enumerator1 = powerTaskListList.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          PowerTaskList current = enumerator1.Current;
          using (List<PowerTask>.Enumerator enumerator2 = current.GetTaskList().GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Network.PowerHistory power = enumerator2.Current.GetPower();
              if (power.Type == Network.PowerType.TAG_CHANGE)
              {
                Network.HistTagChange histTagChange = power as Network.HistTagChange;
                if (histTagChange.Tag == 360 && histTagChange.Value == 1)
                {
                  Entity entity = GameState.Get().GetEntity(histTagChange.Entity);
                  if (entity == null)
                  {
                    UnityEngine.Debug.LogWarning((object) string.Format("{0}.AddPowerTargets() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) histTagChange.Entity));
                    continue;
                  }
                  Card card2 = entity.GetCard();
                  if ((UnityEngine.Object) card2 != (UnityEngine.Object) card1)
                    this.m_mechMinions.Add(card2);
                  else
                    this.m_mimiron = card2;
                  this.m_waitForTaskList = current;
                }
              }
              if (power.Type == Network.PowerType.FULL_ENTITY)
              {
                Network.Entity entity1 = (power as Network.HistFullEntity).Entity;
                Entity entity2 = GameState.Get().GetEntity(entity1.ID);
                if (entity2 == null)
                {
                  UnityEngine.Debug.LogWarning((object) string.Format("{0}.AddPowerTargets() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) entity1.ID));
                }
                else
                {
                  Card card2 = entity2.GetCard();
                  Log.Becca.Print("Found V-07-TR-0N: {0}", (object) entity2.GetName());
                  this.m_volt = card2;
                  this.m_waitForTaskList = current;
                }
              }
            }
          }
        }
      }
      if ((UnityEngine.Object) this.m_volt != (UnityEngine.Object) null && (UnityEngine.Object) this.m_mimiron != (UnityEngine.Object) null && this.m_mechMinions.Count > 0)
      {
        this.m_mimiron.IgnoreDeath(true);
        using (List<Card>.Enumerator enumerator = this.m_mechMinions.GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.IgnoreDeath(true);
        }
        using (List<Card>.Enumerator enumerator = card1.GetController().GetBattlefieldZone().GetCards().GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.SetDoNotSort(true);
        }
      }
      else
      {
        this.m_volt = (Card) null;
        this.m_mimiron = (Card) null;
        this.m_mechMinions.Clear();
      }
    }
    if ((UnityEngine.Object) this.m_volt == (UnityEngine.Object) null || (UnityEngine.Object) this.m_mimiron == (UnityEngine.Object) null || (this.m_mechMinions.Count == 0 || this.m_taskList != this.m_waitForTaskList))
      return false;
    using (List<Card>.Enumerator enumerator = card1.GetController().GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetDoNotSort(true);
    }
    return true;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    if ((bool) ((UnityEngine.Object) this.m_voltSpawnOverrideSpell))
      this.m_volt.OverrideCustomSpawnSpell(UnityEngine.Object.Instantiate<Spell>(this.m_voltSpawnOverrideSpell));
    Log.Becca.Print("V-07-TR-0N: {0}", (object) this.m_volt.GetEntity().GetName());
    this.StartCoroutine(this.TransformEffect());
  }

  [DebuggerHidden]
  private IEnumerator TransformEffect()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MimironsHead.\u003CTransformEffect\u003Ec__Iterator2BE() { \u003C\u003Ef__this = this };
  }

  private void TransformMinions()
  {
    float num1 = 1f;
    Vector3 vector3_1 = new Vector3(0.0f, 0.0f, 2.3f);
    List<int> intList1 = new List<int>();
    for (int index = 0; index < this.m_mechMinions.Count; ++index)
      intList1.Add(index);
    List<int> intList2 = new List<int>();
    for (int index1 = 0; index1 < this.m_mechMinions.Count; ++index1)
    {
      int index2 = UnityEngine.Random.Range(0, intList1.Count);
      intList2.Add(intList1[index2]);
      intList1.RemoveAt(index2);
    }
    for (int index = 0; index < this.m_mechMinions.Count; ++index)
    {
      this.m_minionPosBone.transform.localPosition = this.m_highPosBone.transform.localPosition + Quaternion.Euler(0.0f, (float) (360 / this.m_mechMinions.Count * intList2[index] + 15), 0.0f) * vector3_1;
      GameObject gameObject = this.m_mechMinions[index].GetActor().gameObject;
      float num2 = num1 / (float) this.m_mechMinions.Count * (float) index;
      this.StartCoroutine(this.MinionPlayFX(gameObject, this.m_minionElectricity, num2 / 2f));
      List<Vector3> vector3List = new List<Vector3>();
      Vector3 vector3_2 = new Vector3(UnityEngine.Random.Range(-2f, 2f), 0.0f, UnityEngine.Random.Range(-2f, 2f));
      vector3List.Add(gameObject.transform.position + (this.m_minionPosBone.transform.localPosition - gameObject.transform.position) / 4f + vector3_2);
      vector3List.Add(this.m_minionPosBone.transform.localPosition);
      if (index < this.m_mechMinions.Count - 1)
        iTween.MoveTo(gameObject, iTween.Hash((object) "path", (object) vector3List.ToArray(), (object) "easetype", (object) iTween.EaseType.easeInOutSine, (object) "delay", (object) num2, (object) "time", (object) (float) ((double) this.m_minionHighTime / (double) this.m_mechMinions.Count)));
      else
        iTween.MoveTo(gameObject, iTween.Hash((object) "path", (object) vector3List.ToArray(), (object) "easetype", (object) iTween.EaseType.easeInOutSine, (object) "delay", (object) num2, (object) "time", (object) (float) ((double) this.m_minionHighTime / (double) this.m_mechMinions.Count), (object) "oncomplete", (object) (Action<object>) (newVal => this.FadeInBackground())));
    }
  }

  [DebuggerHidden]
  private IEnumerator MinionPlayFX(GameObject minion, GameObject FX, float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MimironsHead.\u003CMinionPlayFX\u003Ec__Iterator2BF() { FX = FX, minion = minion, delay = delay, \u003C\u0024\u003EFX = FX, \u003C\u0024\u003Eminion = minion, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator MimironNegativeFX()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MimironsHead.\u003CMimironNegativeFX\u003Ec__Iterator2C0() { \u003C\u003Ef__this = this };
  }

  private void MinionCleanup(GameObject minion)
  {
    if (!this.m_cleanup.ContainsKey(minion))
      return;
    using (List<GameObject>.Enumerator enumerator = this.m_cleanup[minion].GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
  }

  private void FadeInBackground()
  {
    this.m_background.SetActive(true);
    this.m_background.GetComponent<Renderer>().material.SetColor("_Color", this.m_clear);
    HighlightState componentInChildren = this.m_volt.GetActor().gameObject.GetComponentInChildren<HighlightState>();
    if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
      componentInChildren.Hide();
    iTween.ColorTo(this.m_background, iTween.Hash((object) "r", (object) 1f, (object) "g", (object) 1f, (object) "b", (object) 1f, (object) "a", (object) 1f, (object) "time", (object) 0.5f, (object) "oncomplete", (object) (Action<object>) (newVal => this.MimironPowerUp())));
  }

  private void SetGlow(Material glowMat, float newVal, string colorVal = "_TintColor")
  {
    glowMat.SetColor(colorVal, Color.Lerp(this.m_clear, Color.white, newVal));
  }

  private void MimironPowerUp()
  {
    this.m_mimironElectricity.GetComponent<ParticleSystem>().Play();
    for (int index = 0; index < this.m_mechMinions.Count; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      MimironsHead.\u003CMimironPowerUp\u003Ec__AnonStorey407 upCAnonStorey407 = new MimironsHead.\u003CMimironPowerUp\u003Ec__AnonStorey407();
      // ISSUE: reference to a compiler-generated field
      upCAnonStorey407.\u003C\u003Ef__this = this;
      GameObject gameObject = this.m_mechMinions[index].GetActor().gameObject;
      // ISSUE: reference to a compiler-generated field
      upCAnonStorey407.minionGlow = UnityEngine.Object.Instantiate<GameObject>(this.m_minionGlow);
      if (!this.m_cleanup.ContainsKey(gameObject))
        this.m_cleanup.Add(gameObject, new List<GameObject>());
      // ISSUE: reference to a compiler-generated field
      this.m_cleanup[gameObject].Add(upCAnonStorey407.minionGlow);
      // ISSUE: reference to a compiler-generated field
      upCAnonStorey407.minionGlow.transform.parent = gameObject.transform;
      // ISSUE: reference to a compiler-generated field
      upCAnonStorey407.minionGlow.transform.localPosition = new Vector3(0.0f, 0.5f, 0.0f);
      float num = this.m_absorbTime / (float) this.m_mechMinions.Count * (float) index;
      // ISSUE: reference to a compiler-generated field
      upCAnonStorey407.minionGlow.GetComponent<Renderer>().material.SetColor("_TintColor", this.m_clear);
      // ISSUE: reference to a compiler-generated field
      SceneUtils.EnableRenderers(upCAnonStorey407.minionGlow, true);
      if (index < this.m_mechMinions.Count - 1)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        iTween.ValueTo(upCAnonStorey407.minionGlow, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) this.m_glowTime, (object) "delay", (object) (float) (0.100000001490116 + (double) num + (double) this.m_sparkDelay), (object) "onstart", (object) new Action<object>(upCAnonStorey407.\u003C\u003Em__218), (object) "onupdate", (object) new Action<object>(upCAnonStorey407.\u003C\u003Em__219)));
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        iTween.ValueTo(upCAnonStorey407.minionGlow, iTween.Hash((object) "from", (object) 1f, (object) "to", (object) 0.0f, (object) "time", (object) this.m_glowTime, (object) "delay", (object) (float) (0.100000001490116 + (double) num + (double) this.m_sparkDelay + (double) this.m_glowTime), (object) "onupdate", (object) new Action<object>(upCAnonStorey407.\u003C\u003Em__21A)));
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        iTween.ValueTo(upCAnonStorey407.minionGlow, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) this.m_glowTime, (object) "delay", (object) (float) (0.100000001490116 + (double) num + (double) this.m_sparkDelay), (object) "onstart", (object) new Action<object>(upCAnonStorey407.\u003C\u003Em__21B), (object) "onupdate", (object) new Action<object>(upCAnonStorey407.\u003C\u003Em__21C), (object) "oncomplete", (object) new Action<object>(upCAnonStorey407.\u003C\u003Em__21D)));
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        iTween.ValueTo(upCAnonStorey407.minionGlow, iTween.Hash((object) "from", (object) 1f, (object) "to", (object) 0.0f, (object) "time", (object) this.m_glowTime, (object) "delay", (object) (float) (0.100000001490116 + (double) num + (double) this.m_sparkDelay + (double) this.m_glowTime), (object) "onupdate", (object) new Action<object>(upCAnonStorey407.\u003C\u003Em__21E)));
      }
    }
  }

  private void AbsorbMinions()
  {
    Vector3 vector3 = new Vector3(0.0f, -1f, 0.0f);
    for (int index = 0; index < this.m_mechMinions.Count; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      MimironsHead.\u003CAbsorbMinions\u003Ec__AnonStorey408 minionsCAnonStorey408 = new MimironsHead.\u003CAbsorbMinions\u003Ec__AnonStorey408();
      // ISSUE: reference to a compiler-generated field
      minionsCAnonStorey408.\u003C\u003Ef__this = this;
      float num = this.m_absorbTime / (float) this.m_mechMinions.Count * (float) index;
      // ISSUE: reference to a compiler-generated field
      minionsCAnonStorey408.minion = this.m_mechMinions[index].GetActor().gameObject;
      if (index < this.m_mechMinions.Count - 1)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        iTween.MoveTo(minionsCAnonStorey408.minion, iTween.Hash((object) "position", (object) (this.m_highPosBone.transform.localPosition + vector3), (object) "easetype", (object) iTween.EaseType.easeInOutSine, (object) "delay", (object) (float) ((double) this.m_glowTime + (double) num + (double) this.m_sparkDelay), (object) "time", (object) 0.5f, (object) "oncomplete", (object) new Action<object>(minionsCAnonStorey408.\u003C\u003Em__21F)));
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        iTween.MoveTo(minionsCAnonStorey408.minion, iTween.Hash((object) "position", (object) (this.m_highPosBone.transform.localPosition + vector3), (object) "easetype", (object) iTween.EaseType.easeInOutSine, (object) "delay", (object) (float) ((double) this.m_glowTime + (double) num + (double) this.m_sparkDelay), (object) "time", (object) 0.5f, (object) "oncomplete", (object) new Action<object>(minionsCAnonStorey408.\u003C\u003Em__220)));
      }
    }
    this.m_isNegFlash = true;
    this.StartCoroutine(this.MimironNegativeFX());
  }

  private void FlareMimiron()
  {
    this.m_mimironGlow.GetComponent<Renderer>().material.SetColor("_TintColor", this.m_clear);
    this.m_mimironFlare.GetComponent<Renderer>().material.SetColor("_TintColor", this.m_clear);
    this.m_mimironGlow.SetActive(true);
    this.m_mimironFlare.SetActive(true);
    iTween.ValueTo(this.m_mimironGlow, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 0.7f, (object) "time", (object) 0.3, (object) "onupdate", (object) (Action<object>) (newVal => this.SetGlow(this.m_mimironGlow.GetComponent<Renderer>().material, (float) newVal, "_TintColor"))));
    iTween.ValueTo(this.m_mimironFlare, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 2.5f, (object) "time", (object) 0.3f, (object) "onupdate", (object) (Action<object>) (newVal => this.SetGlow(this.m_mimironFlare.GetComponent<Renderer>().material, (float) newVal, "_Intensity")), (object) "oncomplete", (object) (Action<object>) (newVal => this.UnflareMimiron())));
  }

  private void UnflareMimiron()
  {
    this.m_volt.SetDoNotSort(false);
    ZonePlay battlefieldZone = this.m_volt.GetController().GetBattlefieldZone();
    using (List<Card>.Enumerator enumerator = battlefieldZone.GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetDoNotSort(false);
    }
    battlefieldZone.UpdateLayout();
    this.DestroyMinions();
    this.m_volt.GetActor().Show();
    this.m_mimironGlow.GetComponent<Renderer>().material.SetColor("_TintColor", this.m_clear);
    this.m_mimironFlare.GetComponent<Renderer>().material.SetColor("_TintColor", this.m_clear);
    this.m_mimironGlow.SetActive(true);
    this.m_mimironFlare.SetActive(true);
    iTween.ValueTo(this.m_mimironGlow, iTween.Hash((object) "from", (object) 0.7f, (object) "to", (object) 0.0f, (object) "time", (object) 0.3, (object) "onupdate", (object) (Action<object>) (newVal => this.SetGlow(this.m_mimironGlow.GetComponent<Renderer>().material, (float) newVal, "_TintColor"))));
    iTween.ValueTo(this.m_mimironFlare, iTween.Hash((object) "from", (object) 2.5f, (object) "to", (object) 0.0f, (object) "time", (object) 0.3f, (object) "onupdate", (object) (Action<object>) (newVal => this.SetGlow(this.m_mimironFlare.GetComponent<Renderer>().material, (float) newVal, "_Intensity")), (object) "oncomplete", (object) (Action<object>) (newVal => this.FadeOutBackground())));
    this.m_isNegFlash = false;
    this.OnSpellFinished();
  }

  private void FadeOutBackground()
  {
    this.m_mimironGlow.SetActive(false);
    this.m_mimironFlare.SetActive(false);
    iTween.ColorTo(this.m_background, iTween.Hash((object) "r", (object) 1f, (object) "g", (object) 1f, (object) "b", (object) 1f, (object) "a", (object) 0.0f, (object) "time", (object) 0.5f, (object) "oncomplete", (object) (Action<object>) (newVal => this.RaiseVolt())));
  }

  private void DestroyMinions()
  {
    using (List<Card>.Enumerator enumerator = this.m_mechMinions.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        current.IgnoreDeath(false);
        current.SetDoNotSort(false);
        current.GetActor().Destroy();
      }
    }
    this.m_mimiron.IgnoreDeath(false);
    this.m_mimiron.SetDoNotSort(false);
    this.m_mimiron.GetActor().Destroy();
  }

  private void RaiseVolt()
  {
    this.m_mimironElectricity.GetComponent<ParticleSystem>().Stop();
    this.m_background.GetComponent<Renderer>().material.SetColor("_Color", this.m_clear);
    this.m_background.SetActive(false);
    GameObject gameObject = this.m_volt.GetActor().gameObject;
    gameObject.transform.parent = this.m_voltParent;
    iTween.MoveTo(gameObject, iTween.Hash((object) "position", (object) (gameObject.transform.localPosition + new Vector3(0.0f, 3f, 0.0f)), (object) "time", (object) 0.2f, (object) "islocal", (object) true, (object) "oncomplete", (object) (Action<object>) (newVal => this.DropV07tron())));
  }

  private void DropV07tron()
  {
    iTween.MoveTo(this.m_volt.GetActor().gameObject, iTween.Hash((object) "position", (object) Vector3.zero, (object) "time", (object) 0.3f, (object) "islocal", (object) true));
    this.Finish();
  }

  private void Finish()
  {
    this.m_volt = (Card) null;
    this.m_mimiron = (Card) null;
    this.m_mechMinions.Clear();
    --this.m_effectsPendingFinish;
    this.FinishIfPossible();
  }
}
