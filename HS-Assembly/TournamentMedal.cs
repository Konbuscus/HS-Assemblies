﻿// Decompiled with JetBrains decompiler
// Type: TournamentMedal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class TournamentMedal : PegUIElement
{
  private List<GameObject> m_stars = new List<GameObject>();
  public GameObject m_banner;
  public UberText m_rankNumber;
  public UberText m_legendIndex;
  public GameObject m_rankMedal;
  public GameObject m_starPrefab;
  public GameObject m_wildFlair;
  public Material m_starFilledMaterial;
  public Material_MobileOverride m_starEmptyMaterial;
  public GameObject m_glowPlane;
  public List<Transform> m_evenStarBones;
  public List<Transform> m_oddStarBones;
  private MedalInfoTranslator m_medal;
  private bool m_isWild;
  private Texture m_standardMedalTexture;
  private Texture m_wildMedalTexture;
  private bool m_showStars;

  protected override void Awake()
  {
    base.Awake();
    if (!((Object) this.gameObject.GetComponent<TooltipZone>() != (Object) null))
      return;
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.MedalOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.MedalOut));
  }

  private void OnDestroy()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_stars.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current);
    }
  }

  public void SetMedal(NetCache.NetCacheMedalInfo medal, bool showStars)
  {
    this.SetMedal(new MedalInfoTranslator(medal), showStars);
  }

  public void SetMedal(NetCache.NetCacheMedalInfo medal)
  {
    this.SetMedal(new MedalInfoTranslator(medal), true);
  }

  public void SetMedal(MedalInfoTranslator medal, bool showStars)
  {
    this.m_banner.SetActive(false);
    this.m_medal = medal;
    this.m_showStars = showStars;
    this.m_rankMedal.GetComponent<Renderer>().enabled = false;
    this.m_standardMedalTexture = AssetLoader.Get().LoadTexture(this.m_medal.GetCurrentMedal(false).textureName, false);
    this.m_wildMedalTexture = AssetLoader.Get().LoadTexture(this.m_medal.GetCurrentMedal(true).textureName, false);
    this.UpdateMedal();
  }

  public bool IsBestCurrentRankWild()
  {
    if (this.m_medal == null)
      return false;
    return this.m_medal.IsBestCurrentRankWild();
  }

  public TranslatedMedalInfo GetMedal()
  {
    return this.m_medal.GetCurrentMedal(this.m_isWild);
  }

  private void UpdateStars(int numEarned, int numTotal)
  {
    using (List<GameObject>.Enumerator enumerator = this.m_stars.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current);
    }
    int num = 0;
    List<Transform> transformList;
    if (numTotal % 2 == 0)
    {
      transformList = this.m_evenStarBones;
      if (numTotal == 2)
        num = 1;
    }
    else
    {
      transformList = this.m_oddStarBones;
      if (numTotal == 3)
        num = 1;
      else if (numTotal == 1)
        num = 2;
    }
    for (int index = 0; index < numTotal; ++index)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.m_starPrefab);
      gameObject.transform.parent = this.transform;
      gameObject.layer = this.gameObject.layer;
      this.m_stars.Add(gameObject);
      gameObject.transform.localScale = new Vector3(0.6f, 0.6f, 1f);
      gameObject.transform.position = transformList[index + num].position;
      if (index < numEarned)
        gameObject.GetComponent<Renderer>().material = this.m_starFilledMaterial;
      else
        gameObject.GetComponent<Renderer>().material = (Material) ((MobileOverrideValue<Material>) this.m_starEmptyMaterial);
    }
  }

  public void MedalOver(UIEvent e)
  {
    string bodytext = string.Empty;
    bool flag = SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB);
    TranslatedMedalInfo currentMedal = this.m_medal.GetCurrentMedal(this.m_isWild);
    if (Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE) || flag)
    {
      string rankPercentile = SeasonEndDialog.GetRankPercentile(currentMedal.rank);
      string str1;
      if (this.m_medal.GetCurrentMedal(this.m_isWild).rank == 0)
        str1 = GameStrings.Format("GLOBAL_MEDAL_TOOLTIP_BODY_LEGEND");
      else if (rankPercentile.Length > 0)
        str1 = GameStrings.Format("GLOBAL_MEDAL_TOURNAMENT_TOOLTIP_BODY", (object) rankPercentile);
      else
        str1 = GameStrings.Format("GLOBAL_MEDAL_TOOLTIP_BODY");
      string str2 = !this.IsBestCurrentRankWild() ? GameStrings.Get("GLOBAL_MEDAL_TOOLTIP_BEST_RANK_STANDARD") : GameStrings.Get("GLOBAL_MEDAL_TOOLTIP_BEST_RANK_WILD");
      bodytext = str1 + "\n\n" + str2;
    }
    this.gameObject.GetComponent<TooltipZone>().ShowLayerTooltip(currentMedal.name, bodytext);
  }

  public void SetFormat(bool isWild)
  {
    this.m_isWild = isWild;
    if (this.m_medal == null)
      return;
    this.UpdateMedal();
  }

  private void MedalOut(UIEvent e)
  {
    this.gameObject.GetComponent<TooltipZone>().HideTooltip();
  }

  private void UpdateMedal()
  {
    this.m_rankMedal.GetComponent<Renderer>().enabled = true;
    this.m_rankMedal.GetComponent<Renderer>().material.mainTexture = !this.m_isWild ? this.m_standardMedalTexture : this.m_wildMedalTexture;
    TranslatedMedalInfo currentMedal = this.m_medal.GetCurrentMedal(this.m_isWild);
    int rank = currentMedal.rank;
    if (rank == 0)
    {
      this.m_banner.SetActive(false);
      this.m_legendIndex.gameObject.SetActive(true);
      this.m_legendIndex.Text = currentMedal.legendIndex != 0 ? (currentMedal.legendIndex != -1 ? currentMedal.legendIndex.ToString() : string.Empty) : string.Empty;
    }
    else
    {
      this.m_banner.SetActive(true);
      this.m_rankNumber.Text = rank.ToString();
      this.m_legendIndex.gameObject.SetActive(false);
      this.m_legendIndex.Text = string.Empty;
    }
    if ((Object) this.m_wildFlair != (Object) null)
      this.m_wildFlair.SetActive(this.m_isWild);
    if (!this.m_showStars)
      return;
    this.UpdateStars(currentMedal.earnedStars, currentMedal.totalStars);
  }
}
