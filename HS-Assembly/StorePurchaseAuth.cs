﻿// Decompiled with JetBrains decompiler
// Type: StorePurchaseAuth
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using PegasusUtil;
using System;
using System.Collections.Generic;
using System.Linq;

[CustomEditClass]
public class StorePurchaseAuth : UIBPopup
{
  private List<StorePurchaseAuth.AckPurchaseResultListener> m_ackPurchaseResultListeners = new List<StorePurchaseAuth.AckPurchaseResultListener>();
  private List<StorePurchaseAuth.ExitListener> m_exitListeners = new List<StorePurchaseAuth.ExitListener>();
  private const string s_OkButtonText = "GLOBAL_OKAY";
  private const string s_BackButtonText = "GLOBAL_BACK";
  [CustomEditField(Sections = "Base UI")]
  public MultiSliceElement m_root;
  [CustomEditField(Sections = "Swirly Animation")]
  public Spell m_spell;
  [CustomEditField(Sections = "Base UI")]
  public UIBButton m_okButton;
  [CustomEditField(Sections = "Text")]
  public UberText m_waitingForAuthText;
  [CustomEditField(Sections = "Text")]
  public UberText m_successHeadlineText;
  [CustomEditField(Sections = "Text")]
  public UberText m_failHeadlineText;
  [CustomEditField(Sections = "Text")]
  public UberText m_failDetailsText;
  [CustomEditField(Sections = "Base UI")]
  public StoreMiniSummary m_miniSummary;
  private bool m_isBackButton;
  private bool m_showingSuccess;
  private MoneyOrGTAPPTransaction m_moneyOrGTAPPTransaction;

  private void Awake()
  {
    this.m_miniSummary.gameObject.SetActive(false);
    this.m_okButton.gameObject.SetActive(false);
    this.m_okButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnOkayButtonPressed));
  }

  public void Show(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, bool enableBackButton, bool isZeroCostLicense)
  {
    if (this.m_shown)
      return;
    if (isZeroCostLicense)
    {
      this.m_waitingForAuthText.Text = GameStrings.Get("GLUE_STORE_AUTH_ZERO_COST_WAITING");
      this.m_successHeadlineText.Text = GameStrings.Get("GLUE_STORE_AUTH_ZERO_COST_SUCCESS_HEADLINE");
      this.m_failHeadlineText.Text = GameStrings.Get("GLUE_STORE_AUTH_ZERO_COST_FAIL_HEADLINE");
    }
    else
    {
      this.m_waitingForAuthText.Text = GameStrings.Get("GLUE_STORE_AUTH_WAITING");
      this.m_successHeadlineText.Text = GameStrings.Get("GLUE_STORE_AUTH_SUCCESS_HEADLINE");
      this.m_failHeadlineText.Text = GameStrings.Get("GLUE_STORE_AUTH_FAIL_HEADLINE");
    }
    this.m_shown = true;
    this.m_showingSuccess = false;
    this.m_moneyOrGTAPPTransaction = moneyOrGTAPPTransaction;
    this.m_isBackButton = enableBackButton;
    this.m_okButton.gameObject.SetActive(enableBackButton);
    this.m_okButton.SetText(!enableBackButton ? "GLOBAL_OKAY" : "GLOBAL_BACK");
    this.m_waitingForAuthText.gameObject.SetActive(true);
    this.m_successHeadlineText.gameObject.SetActive(false);
    this.m_failHeadlineText.gameObject.SetActive(false);
    this.m_failDetailsText.gameObject.SetActive(false);
    this.m_spell.ActivateState(SpellStateType.BIRTH);
    if (this.m_moneyOrGTAPPTransaction != null && this.m_moneyOrGTAPPTransaction.ShouldShowMiniSummary())
      this.ShowMiniSummary();
    else
      this.m_root.UpdateSlices();
    this.DoShowAnimation((UIBPopup.OnAnimationComplete) null);
  }

  public void ShowPurchaseLocked(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, bool enableBackButton, bool isZeroCostLicense, StorePurchaseAuth.PurchaseLockedDialogCallback purchaseLockedCallback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    StorePurchaseAuth.\u003CShowPurchaseLocked\u003Ec__AnonStorey3FC lockedCAnonStorey3Fc = new StorePurchaseAuth.\u003CShowPurchaseLocked\u003Ec__AnonStorey3FC();
    // ISSUE: reference to a compiler-generated field
    lockedCAnonStorey3Fc.purchaseLockedCallback = purchaseLockedCallback;
    this.Show(moneyOrGTAPPTransaction, enableBackButton, isZeroCostLicense);
    string empty = string.Empty;
    if (moneyOrGTAPPTransaction.Provider.HasValue)
    {
      switch (moneyOrGTAPPTransaction.Provider.Value)
      {
        case BattlePayProvider.BP_PROVIDER_APPLE:
          empty = GameStrings.Get("GLOBAL_STORE_MOBILE_NAME_APPLE");
          break;
        case BattlePayProvider.BP_PROVIDER_GOOGLE_PLAY:
          empty = GameStrings.Get("GLOBAL_STORE_MOBILE_NAME_GOOGLE");
          break;
        case BattlePayProvider.BP_PROVIDER_AMAZON:
          empty = GameStrings.Get("GLOBAL_STORE_MOBILE_NAME_AMAZON");
          break;
        default:
          empty = GameStrings.Get("GLOBAL_STORE_MOBILE_NAME_DEFAULT");
          break;
      }
    }
    string str = GameStrings.Format("GLUE_STORE_PURCHASE_LOCK_DESCRIPTION", (object) empty);
    // ISSUE: reference to a compiler-generated method
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_STORE_PURCHASE_LOCK_HEADER"),
      m_confirmText = GameStrings.Get("GLOBAL_CANCEL"),
      m_cancelText = GameStrings.Get("GLOBAL_HELP"),
      m_text = str,
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_iconSet = AlertPopup.PopupInfo.IconSet.Alternate,
      m_responseCallback = new AlertPopup.ResponseCallback(lockedCAnonStorey3Fc.\u003C\u003Em__1F2)
    });
  }

  public override void Hide()
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    this.DoHideAnimation((UIBPopup.OnAnimationComplete) (() =>
    {
      this.m_okButton.gameObject.SetActive(false);
      this.m_miniSummary.gameObject.SetActive(false);
      this.m_spell.ActivateState(SpellStateType.NONE);
    }));
  }

  public bool CompletePurchaseSuccess(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction)
  {
    if (!this.gameObject.activeInHierarchy)
      return false;
    bool showMiniSummary = false;
    if (moneyOrGTAPPTransaction != null)
      showMiniSummary = moneyOrGTAPPTransaction.ShouldShowMiniSummary();
    this.ShowPurchaseSuccess(moneyOrGTAPPTransaction, showMiniSummary);
    return true;
  }

  public bool CompletePurchaseFailure(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, string failDetails, Network.PurchaseErrorInfo.ErrorType error)
  {
    if (!this.gameObject.activeInHierarchy)
      return false;
    bool showMiniSummary = false;
    if (moneyOrGTAPPTransaction != null)
      showMiniSummary = moneyOrGTAPPTransaction.ShouldShowMiniSummary();
    this.ShowPurchaseFailure(moneyOrGTAPPTransaction, failDetails, showMiniSummary, error);
    return true;
  }

  public void ShowPreviousPurchaseSuccess(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, bool enableBackButton)
  {
    this.Show(moneyOrGTAPPTransaction, enableBackButton, false);
    this.ShowPurchaseSuccess(moneyOrGTAPPTransaction, true);
  }

  public void ShowPreviousPurchaseFailure(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, string failDetails, bool enableBackButton, Network.PurchaseErrorInfo.ErrorType error)
  {
    this.Show(moneyOrGTAPPTransaction, enableBackButton, false);
    this.ShowPurchaseFailure(moneyOrGTAPPTransaction, failDetails, true, error);
  }

  public void ShowPurchaseMethodFailure(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, string failDetails, bool enableBackButton, Network.PurchaseErrorInfo.ErrorType error)
  {
    this.Show(moneyOrGTAPPTransaction, enableBackButton, false);
    this.ShowPurchaseFailure(moneyOrGTAPPTransaction, failDetails, false, error);
  }

  public void RegisterAckPurchaseResultListener(StorePurchaseAuth.AckPurchaseResultListener listener)
  {
    if (this.m_ackPurchaseResultListeners.Contains(listener))
      return;
    this.m_ackPurchaseResultListeners.Add(listener);
  }

  public void RemoveAckPurchaseResultListener(StorePurchaseAuth.AckPurchaseResultListener listener)
  {
    this.m_ackPurchaseResultListeners.Remove(listener);
  }

  public void RegisterExitListener(StorePurchaseAuth.ExitListener listener)
  {
    if (this.m_exitListeners.Contains(listener))
      return;
    this.m_exitListeners.Add(listener);
  }

  public void RemoveExitListener(StorePurchaseAuth.ExitListener listener)
  {
    this.m_exitListeners.Remove(listener);
  }

  private void OnOkayButtonPressed(UIEvent e)
  {
    if (this.m_showingSuccess)
    {
      string str = (string) null;
      Network.Bundle bundle = this.m_moneyOrGTAPPTransaction != null ? StoreManager.Get().GetBundle(this.m_moneyOrGTAPPTransaction.ProductID) : (Network.Bundle) null;
      if (bundle != null && bundle.Items != null)
      {
        Network.BundleItem bundleItem = bundle.Items.FirstOrDefault<Network.BundleItem>((Func<Network.BundleItem, bool>) (i => i.Product == ProductType.PRODUCT_TYPE_HERO));
        if (bundleItem != null)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: reference to a compiler-generated method
          CardHeroDbfRecord record = GameDbf.CardHero.GetRecord(new Predicate<CardHeroDbfRecord>(new StorePurchaseAuth.\u003COnOkayButtonPressed\u003Ec__AnonStorey3FD() { boughtHeroCardId = GameUtils.TranslateDbIdToCardId(bundleItem.ProductData) }.\u003C\u003Em__1F5));
          if (record != null)
            str = (string) record.PurchaseCompleteMsg;
        }
      }
      if (!string.IsNullOrEmpty(str))
      {
        this.Hide();
        DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
        {
          m_headerText = GameStrings.Get("GLUE_STORE_AUTH_SUCCESS_HEADLINE"),
          m_text = str,
          m_showAlertIcon = false,
          m_responseDisplay = AlertPopup.ResponseDisplay.OK,
          m_responseCallback = (AlertPopup.ResponseCallback) ((response, data) => this.OnOkayButtonPressed_Finish())
        });
        return;
      }
    }
    this.OnOkayButtonPressed_Finish();
  }

  private void OnOkayButtonPressed_Finish()
  {
    if (this.m_isBackButton)
    {
      foreach (StorePurchaseAuth.ExitListener exitListener in this.m_exitListeners.ToArray())
        exitListener();
    }
    else
    {
      this.Hide();
      foreach (StorePurchaseAuth.AckPurchaseResultListener purchaseResultListener in this.m_ackPurchaseResultListeners.ToArray())
        purchaseResultListener(this.m_showingSuccess, this.m_moneyOrGTAPPTransaction);
    }
    FixedRewardsMgr.Get().ShowFixedRewards(UserAttentionBlocker.NONE, new HashSet<RewardVisualTiming>()
    {
      RewardVisualTiming.IMMEDIATE
    }, (FixedRewardsMgr.DelOnAllFixedRewardsShown) null, (FixedRewardsMgr.DelPositionNonToastReward) (reward => reward.transform.localPosition = PopupDisplayManager.Get().GetRewardLocalPos()), PopupDisplayManager.Get().GetRewardPunchScale(), PopupDisplayManager.Get().GetRewardScale());
  }

  private void ShowPurchaseSuccess(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, bool showMiniSummary)
  {
    this.m_showingSuccess = true;
    this.m_moneyOrGTAPPTransaction = moneyOrGTAPPTransaction;
    this.m_isBackButton = false;
    this.m_okButton.gameObject.SetActive(true);
    this.m_okButton.SetText("GLOBAL_OKAY");
    if (showMiniSummary)
      this.ShowMiniSummary();
    this.m_waitingForAuthText.gameObject.SetActive(false);
    this.m_successHeadlineText.gameObject.SetActive(true);
    this.m_failHeadlineText.gameObject.SetActive(false);
    this.m_failDetailsText.gameObject.SetActive(false);
    this.m_spell.ActivateState(SpellStateType.ACTION);
  }

  private void ShowPurchaseFailure(MoneyOrGTAPPTransaction moneyOrGTAPPTransaction, string failDetails, bool showMiniSummary, Network.PurchaseErrorInfo.ErrorType error)
  {
    this.m_showingSuccess = false;
    this.m_moneyOrGTAPPTransaction = moneyOrGTAPPTransaction;
    this.m_isBackButton = false;
    if (error == Network.PurchaseErrorInfo.ErrorType.PRODUCT_EVENT_HAS_ENDED && SceneMgr.Get().IsModeRequested(SceneMgr.Mode.TAVERN_BRAWL))
      this.m_isBackButton = true;
    this.m_okButton.gameObject.SetActive(true);
    this.m_okButton.SetText("GLOBAL_OKAY");
    if (showMiniSummary)
      this.ShowMiniSummary();
    this.m_failDetailsText.Text = failDetails;
    this.m_waitingForAuthText.gameObject.SetActive(false);
    this.m_successHeadlineText.gameObject.SetActive(false);
    this.m_failHeadlineText.gameObject.SetActive(true);
    this.m_failDetailsText.gameObject.SetActive(true);
    this.m_spell.ActivateState(SpellStateType.DEATH);
  }

  private void ShowMiniSummary()
  {
    if (this.m_moneyOrGTAPPTransaction == null)
      return;
    this.m_miniSummary.SetDetails(this.m_moneyOrGTAPPTransaction.ProductID, 1);
    this.m_miniSummary.gameObject.SetActive(true);
    this.m_root.UpdateSlices();
  }

  public delegate void AckPurchaseResultListener(bool success, MoneyOrGTAPPTransaction moneyOrGTAPPTransaction);

  public delegate void ExitListener();

  public delegate void PurchaseLockedDialogCallback(bool showHelp);
}
