﻿// Decompiled with JetBrains decompiler
// Type: PlayMakerAnimatorIKProxy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[RequireComponent(typeof (PlayMakerFSM))]
[RequireComponent(typeof (Animator))]
public class PlayMakerAnimatorIKProxy : MonoBehaviour
{
  private Animator _animator;

  public event Action<int> OnAnimatorIKEvent;

  private void OnAnimatorIK(int layerIndex)
  {
    if (this.OnAnimatorIKEvent == null)
      return;
    this.OnAnimatorIKEvent(layerIndex);
  }
}
