﻿// Decompiled with JetBrains decompiler
// Type: ThinkEmoteManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ThinkEmoteManager : MonoBehaviour
{
  public const float SECONDS_BEFORE_EMOTE = 20f;
  private float m_secondsSinceAction;
  private static ThinkEmoteManager s_instance;

  private void Awake()
  {
    ThinkEmoteManager.s_instance = this;
  }

  private void OnDestroy()
  {
    ThinkEmoteManager.s_instance = (ThinkEmoteManager) null;
  }

  public static ThinkEmoteManager Get()
  {
    return ThinkEmoteManager.s_instance;
  }

  private void Update()
  {
    GameState gameState = GameState.Get();
    if (gameState == null || !gameState.IsMainPhase())
      return;
    this.m_secondsSinceAction += Time.deltaTime;
    if ((double) this.m_secondsSinceAction <= 20.0)
      return;
    this.PlayThinkEmote();
  }

  private void PlayThinkEmote()
  {
    this.m_secondsSinceAction = 0.0f;
    GameState.Get().GetGameEntity().OnPlayThinkEmote();
  }

  public void NotifyOfActivity()
  {
    this.m_secondsSinceAction = 0.0f;
  }
}
