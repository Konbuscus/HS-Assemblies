﻿// Decompiled with JetBrains decompiler
// Type: GeneralStorePacksPane
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class GeneralStorePacksPane : GeneralStorePane
{
  private List<GeneralStorePackSelectorButton> m_packButtons = new List<GeneralStorePackSelectorButton>();
  [SerializeField]
  private Vector3 m_packButtonSpacing;
  [SerializeField]
  private int m_maxRibbons;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_boosterSelectionSound;
  [CustomEditField(Sections = "Purchase Flow")]
  public GameObject m_purchaseAnimationBlocker;
  private GeneralStorePacksContent m_packsContent;
  private bool m_paneInitialized;
  private bool m_inRemovingBundleFlow;
  private string m_randomRewardCardId;

  [CustomEditField(Sections = "Layout")]
  public Vector3 PackButtonSpacing
  {
    get
    {
      return this.m_packButtonSpacing;
    }
    set
    {
      this.m_packButtonSpacing = value;
      this.UpdatePackButtonPositions();
    }
  }

  [CustomEditField(Sections = "Content")]
  public int MaxRibbons
  {
    get
    {
      return this.m_maxRibbons;
    }
    set
    {
      this.m_maxRibbons = value;
      this.UpdatePackButtonRecommendedIndicators();
    }
  }

  private void Awake()
  {
    this.m_packsContent = this.m_parentContent as GeneralStorePacksContent;
    this.m_purchaseAnimationBlocker.SetActive(false);
    if ((UnityEngine.Object) this.m_packsContent == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "m_packsContent is not the correct type: GeneralStorePacksContent");
    }
    else
    {
      NetCache.Get().RegisterNewNoticesListener(new NetCache.DelNewNoticesListener(this.OnNewNotices));
      StoreManager.Get().RegisterSuccessfulPurchaseAckListener(new StoreManager.SuccessfulPurchaseAckCallback(this.OnItemPurchased));
    }
  }

  private void OnDestroy()
  {
    NetCache.Get().RemoveNewNoticesListener(new NetCache.DelNewNoticesListener(this.OnNewNotices));
    StoreManager.Get().RemoveSuccessfulPurchaseAckListener(new StoreManager.SuccessfulPurchaseAckCallback(this.OnItemPurchased));
  }

  public override void StoreShown(bool isCurrent)
  {
    if (!this.m_paneInitialized)
    {
      this.m_paneInitialized = true;
      this.SetupPackButtons();
      this.SetupInitialSelectedPack();
    }
    this.UpdatePackButtonPositions();
    this.UpdatePackButtonRecommendedIndicators();
  }

  public override void PrePaneSwappedIn()
  {
    if (!(bool) UniversalInputManager.UsePhoneUI || !this.m_inRemovingBundleFlow)
      return;
    this.OnPackSelectorButtonClicked(this.m_packButtons[0], (int) this.m_packButtons[0].GetBoosterId());
    this.m_inRemovingBundleFlow = false;
  }

  public void RemoveFirstPurchaseBundle(float glowOutLength)
  {
    if (!StoreManager.IsFirstPurchaseBundleOwned())
      return;
    this.StartCoroutine(this.AnimateRemoveFirstPurchaseBundle(glowOutLength));
  }

  private void OnItemPurchased(Network.Bundle bundle, PaymentMethod purchaseMethod, object userData)
  {
    if (bundle == null || bundle.Items == null)
      return;
    using (List<Network.BundleItem>.Enumerator enumerator = bundle.Items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.BundleItem current = enumerator.Current;
        if (current != null && current.Product == ProductType.PRODUCT_TYPE_RANDOM_CARD && this.m_packsContent.GetBoosterId() == 17)
        {
          this.OnRandomCardPurchased(this.m_randomRewardCardId);
          break;
        }
      }
    }
  }

  private void OnRandomCardPurchased(string cardId)
  {
    if ((UnityEngine.Object) this.m_packsContent == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarningFormat("OnRandomCardPurchased() m_packsContent == null for cardID {0}", (object) cardId);
    else
      this.m_packsContent.FirstPurchaseBundlePurchased(cardId);
  }

  private void OnPackSelectorButtonClicked(GeneralStorePackSelectorButton btn, int boosterId)
  {
    if (!this.m_parentContent.IsContentActive())
      return;
    this.m_packsContent.SetBoosterId(boosterId, false, false);
    using (List<GeneralStorePackSelectorButton>.Enumerator enumerator = this.m_packButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Unselect();
    }
    btn.Select();
    Options.Get().SetInt(Option.LAST_SELECTED_STORE_BOOSTER_ID, (int) btn.GetBoosterId());
    if (string.IsNullOrEmpty(this.m_boosterSelectionSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_boosterSelectionSound));
  }

  private void SetupPackButtons()
  {
    Map<int, StorePackDef> storePackDefs = this.m_packsContent.GetStorePackDefs();
    int boosterId = this.m_packsContent.GetBoosterId();
    using (Map<int, StorePackDef>.Enumerator enumerator = storePackDefs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, StorePackDef> current = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GeneralStorePacksPane.\u003CSetupPackButtons\u003Ec__AnonStorey3F9 buttonsCAnonStorey3F9 = new GeneralStorePacksPane.\u003CSetupPackButtons\u003Ec__AnonStorey3F9();
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey3F9.\u003C\u003Ef__this = this;
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey3F9.id = current.Key;
        // ISSUE: reference to a compiler-generated field
        if (StoreManager.Get().GetAllBundlesForProduct(ProductType.PRODUCT_TYPE_BOOSTER, GeneralStorePacksContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, buttonsCAnonStorey3F9.id, 0).Count != 0 || !StoreManager.IsFirstPurchaseBundleOwned() && StoreManager.Get().GetAllBundlesForProduct(ProductType.PRODUCT_TYPE_HIDDEN_LICENSE, GeneralStorePacksContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, 1, 0).Count != 0)
        {
          GameObject gameObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(current.Value.m_buttonPrefab), false, false);
          GameUtils.SetParent(gameObject, this.m_paneContainer, true);
          SceneUtils.SetLayer(gameObject, this.m_paneContainer.layer);
          // ISSUE: reference to a compiler-generated field
          buttonsCAnonStorey3F9.newPackButton = gameObject.GetComponent<GeneralStorePackSelectorButton>();
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          buttonsCAnonStorey3F9.newPackButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(buttonsCAnonStorey3F9.\u003C\u003Em__1DF));
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          buttonsCAnonStorey3F9.newPackButton.SetBoosterId((BoosterDbId) buttonsCAnonStorey3F9.id);
          // ISSUE: reference to a compiler-generated field
          if (buttonsCAnonStorey3F9.id == boosterId)
          {
            // ISSUE: reference to a compiler-generated field
            buttonsCAnonStorey3F9.newPackButton.Select();
          }
          // ISSUE: reference to a compiler-generated field
          this.m_packButtons.Add(buttonsCAnonStorey3F9.newPackButton);
        }
      }
    }
    this.UpdatePackButtonPositions();
  }

  private void SortPackButtons()
  {
    this.m_packButtons.Sort((Comparison<GeneralStorePackSelectorButton>) ((lhs, rhs) =>
    {
      bool flag1 = StoreManager.IsFirstPurchaseBundleBooster((int) lhs.GetBoosterId());
      bool flag2 = StoreManager.IsFirstPurchaseBundleBooster((int) rhs.GetBoosterId());
      if (flag1 != flag2)
        return flag1 ? -1 : 1;
      bool flag3 = lhs.IsRecommendedForNewPlayer();
      bool flag4 = rhs.IsRecommendedForNewPlayer();
      if (flag3 != flag4)
        return flag3 ? -1 : 1;
      bool flag5 = lhs.IsPreorder();
      bool flag6 = rhs.IsPreorder();
      if (flag5 != flag6)
        return flag5 ? -1 : 1;
      bool flag7 = lhs.IsLatestExpansion();
      bool flag8 = rhs.IsLatestExpansion();
      if (flag7 != flag8)
        return flag7 ? -1 : 1;
      BoosterDbfRecord booster1 = lhs.GetBooster();
      BoosterDbfRecord booster2 = rhs.GetBooster();
      bool flag9 = booster1 != null && booster1.ID == 1;
      bool flag10 = booster2 != null && booster2.ID == 1;
      if (flag9 != flag10)
        return flag9 ? -1 : 1;
      int num = booster1 != null ? booster1.SortOrder : 0;
      return Mathf.Clamp((booster2 != null ? booster2.SortOrder : 0) - num, -1, 1);
    }));
  }

  private void UpdatePackButtonPositions()
  {
    this.SortPackButtons();
    GeneralStorePackSelectorButton[] array = this.m_packButtons.ToArray();
    int index = 0;
    int num = 0;
    for (; index < array.Length; ++index)
    {
      GeneralStorePackSelectorButton packSelectorButton = array[index];
      bool flag = packSelectorButton.IsPurchasable();
      packSelectorButton.gameObject.SetActive(flag);
      if (flag)
        packSelectorButton.transform.localPosition = this.m_packButtonSpacing * (float) num++;
    }
  }

  private void UpdatePackButtonRecommendedIndicators()
  {
    int num = 0;
    GeneralStorePackSelectorButton[] array = this.m_packButtons.ToArray();
    for (int index = 0; index < array.Length; ++index)
    {
      if (++num <= this.m_maxRibbons)
        array[index].UpdateRibbonIndicator(false);
      else
        array[index].UpdateRibbonIndicator(true);
    }
  }

  private bool ShouldResetPackSelection()
  {
    List<Network.Bundle> bundlesForProduct = StoreManager.Get().GetAllBundlesForProduct(ProductType.PRODUCT_TYPE_BOOSTER, GeneralStorePacksContent.REQUIRE_REAL_MONEY_BUNDLE_OPTION, 0, 0);
    List<string> stringList = new List<string>((IEnumerable<string>) Options.Get().GetString(Option.SEEN_PACK_PRODUCT_LIST, string.Empty).Split(':'));
    bool flag = false;
    using (List<Network.Bundle>.Enumerator enumerator = bundlesForProduct.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Bundle current = enumerator.Current;
        if (!stringList.Contains(current.ProductID))
        {
          stringList.Add(current.ProductID);
          flag = true;
        }
      }
    }
    Options.Get().SetString(Option.SEEN_PACK_PRODUCT_LIST, string.Join(":", stringList.ToArray()));
    return flag;
  }

  private void SetupInitialSelectedPack()
  {
    BoosterDbId boosterDbId = BoosterDbId.INVALID;
    if (this.ShouldResetPackSelection())
      Options.Get().SetInt(Option.LAST_SELECTED_STORE_BOOSTER_ID, 0);
    else
      boosterDbId = (BoosterDbId) Options.Get().GetInt(Option.LAST_SELECTED_STORE_BOOSTER_ID, 0);
    using (List<GeneralStorePackSelectorButton>.Enumerator enumerator = this.m_packButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GeneralStorePackSelectorButton current = enumerator.Current;
        if (current.GetBoosterId() == boosterDbId)
        {
          this.m_packsContent.SetBoosterId((int) boosterDbId, true, true);
          current.Select();
          break;
        }
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator AnimateRemoveFirstPurchaseBundle(float glowOutLength)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStorePacksPane.\u003CAnimateRemoveFirstPurchaseBundle\u003Ec__Iterator26F() { glowOutLength = glowOutLength, \u003C\u0024\u003EglowOutLength = glowOutLength, \u003C\u003Ef__this = this };
  }

  private void OnNewNotices(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList)
  {
    List<NetCache.ProfileNotice> all = newNotices.FindAll((Predicate<NetCache.ProfileNotice>) (obj =>
    {
      if (obj.Origin == NetCache.ProfileNotice.NoticeOrigin.FROM_PURCHASE)
        return obj.Type == NetCache.ProfileNotice.NoticeType.REWARD_CARD;
      return false;
    }));
    if (all.Count <= 0)
      return;
    NetCache.ProfileNotice profileNotice = (NetCache.ProfileNotice) null;
    using (List<NetCache.ProfileNotice>.Enumerator enumerator = all.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.ProfileNotice current = enumerator.Current;
        if (StoreManager.Get().IsIdActiveTransaction(current.OriginData))
        {
          profileNotice = current;
          break;
        }
      }
    }
    if (profileNotice == null)
      return;
    this.m_randomRewardCardId = (profileNotice as NetCache.ProfileNoticeRewardCard).CardID;
  }
}
