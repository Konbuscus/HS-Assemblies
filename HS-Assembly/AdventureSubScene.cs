﻿// Decompiled with JetBrains decompiler
// Type: AdventureSubScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class AdventureSubScene : MonoBehaviour
{
  [CustomEditField(Sections = "Animation Settings")]
  public float m_TransitionAnimationTime = 1f;
  private List<AdventureSubScene.SubSceneTransitionFinished> m_SubSceneTransitionListeners = new List<AdventureSubScene.SubSceneTransitionFinished>();
  [CustomEditField(Sections = "Bounds Settings")]
  public Vector3_MobileOverride m_SubSceneBounds;
  private bool m_IsLoaded;

  public void SetIsLoaded(bool loaded)
  {
    this.m_IsLoaded = loaded;
  }

  public bool IsLoaded()
  {
    return this.m_IsLoaded;
  }

  public void AddSubSceneTransitionFinishedListener(AdventureSubScene.SubSceneTransitionFinished dlg)
  {
    this.m_SubSceneTransitionListeners.Add(dlg);
  }

  public void RemoveSubSceneTransitionFinishedListener(AdventureSubScene.SubSceneTransitionFinished dlg)
  {
    this.m_SubSceneTransitionListeners.Remove(dlg);
  }

  public void NotifyTransitionComplete()
  {
    this.FireSubSceneTransitionFinishedEvent();
  }

  private void FireSubSceneTransitionFinishedEvent()
  {
    foreach (AdventureSubScene.SubSceneTransitionFinished transitionFinished in this.m_SubSceneTransitionListeners.ToArray())
      transitionFinished();
  }

  public delegate void SubSceneTransitionFinished();
}
