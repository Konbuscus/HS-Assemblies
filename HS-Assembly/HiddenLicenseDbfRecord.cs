﻿// Decompiled with JetBrains decompiler
// Type: HiddenLicenseDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HiddenLicenseDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_AccountLicenseId;
  [SerializeField]
  private string m_NoteDesc;

  [DbfField("ACCOUNT_LICENSE_ID", "ID of corresponding license in ACCOUNT_LICENSE table")]
  public int AccountLicenseId
  {
    get
    {
      return this.m_AccountLicenseId;
    }
  }

  [DbfField("NOTE_DESC", "designer name of booster contents")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    HiddenLicenseDbfAsset hiddenLicenseDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (HiddenLicenseDbfAsset)) as HiddenLicenseDbfAsset;
    if ((UnityEngine.Object) hiddenLicenseDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("HiddenLicenseDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < hiddenLicenseDbfAsset.Records.Count; ++index)
      hiddenLicenseDbfAsset.Records[index].StripUnusedLocales();
    records = (object) hiddenLicenseDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetAccountLicenseId(int v)
  {
    this.m_AccountLicenseId = v;
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (HiddenLicenseDbfRecord.\u003C\u003Ef__switch\u0024map44 == null)
      {
        // ISSUE: reference to a compiler-generated field
        HiddenLicenseDbfRecord.\u003C\u003Ef__switch\u0024map44 = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "ACCOUNT_LICENSE_ID",
            1
          },
          {
            "NOTE_DESC",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (HiddenLicenseDbfRecord.\u003C\u003Ef__switch\u0024map44.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.AccountLicenseId;
          case 2:
            return (object) this.NoteDesc;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (HiddenLicenseDbfRecord.\u003C\u003Ef__switch\u0024map45 == null)
    {
      // ISSUE: reference to a compiler-generated field
      HiddenLicenseDbfRecord.\u003C\u003Ef__switch\u0024map45 = new Dictionary<string, int>(3)
      {
        {
          "ID",
          0
        },
        {
          "ACCOUNT_LICENSE_ID",
          1
        },
        {
          "NOTE_DESC",
          2
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!HiddenLicenseDbfRecord.\u003C\u003Ef__switch\u0024map45.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetAccountLicenseId((int) val);
        break;
      case 2:
        this.SetNoteDesc((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (HiddenLicenseDbfRecord.\u003C\u003Ef__switch\u0024map46 == null)
      {
        // ISSUE: reference to a compiler-generated field
        HiddenLicenseDbfRecord.\u003C\u003Ef__switch\u0024map46 = new Dictionary<string, int>(3)
        {
          {
            "ID",
            0
          },
          {
            "ACCOUNT_LICENSE_ID",
            1
          },
          {
            "NOTE_DESC",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (HiddenLicenseDbfRecord.\u003C\u003Ef__switch\u0024map46.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
