﻿// Decompiled with JetBrains decompiler
// Type: SoundDucker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class SoundDucker : MonoBehaviour
{
  public bool m_DuckAllCategories = true;
  public SoundDuckedCategoryDef m_GlobalDuckDef;
  public List<SoundDuckedCategoryDef> m_DuckedCategoryDefs;
  private bool m_ducking;

  private void Awake()
  {
    this.InitDuckedCategoryDefs();
  }

  private void OnDestroy()
  {
    this.StopDucking();
  }

  public override string ToString()
  {
    return string.Format("[SoundDucker: {0}]", (object) this.name);
  }

  public List<SoundDuckedCategoryDef> GetDuckedCategoryDefs()
  {
    return this.m_DuckedCategoryDefs;
  }

  public bool IsDucking()
  {
    return this.m_ducking;
  }

  public void StartDucking()
  {
    if ((UnityEngine.Object) SoundManager.Get() == (UnityEngine.Object) null || this.m_ducking)
      return;
    this.InitDuckedCategoryDefs();
    this.m_ducking = SoundManager.Get().StartDucking(this);
  }

  public void StopDucking()
  {
    if ((UnityEngine.Object) SoundManager.Get() == (UnityEngine.Object) null || !this.m_ducking)
      return;
    this.m_ducking = false;
    SoundManager.Get().StopDucking(this);
  }

  private void InitDuckedCategoryDefs()
  {
    if (!this.m_DuckAllCategories || this.m_GlobalDuckDef == null)
      return;
    this.m_DuckedCategoryDefs = new List<SoundDuckedCategoryDef>();
    foreach (int num in Enum.GetValues(typeof (SoundCategory)))
    {
      SoundCategory soundCategory = (SoundCategory) num;
      if (soundCategory != SoundCategory.NONE)
      {
        SoundDuckedCategoryDef dst = new SoundDuckedCategoryDef();
        SoundUtils.CopyDuckedCategoryDef(this.m_GlobalDuckDef, dst);
        dst.m_Category = soundCategory;
        this.m_DuckedCategoryDefs.Add(dst);
      }
    }
  }
}
