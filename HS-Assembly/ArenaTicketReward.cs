﻿// Decompiled with JetBrains decompiler
// Type: ArenaTicketReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArenaTicketReward : Reward
{
  public GameObject m_ticketVisual;
  public UberText m_countLabel;
  public UberText m_playerNameLabel;

  protected override void InitData()
  {
    this.SetData((RewardData) new ForgeTicketRewardData(), false);
  }

  protected override void ShowReward(bool updateCacheValues)
  {
    string empty1 = string.Empty;
    string empty2 = string.Empty;
    string source = string.Empty;
    string headline;
    if (this.Data.Origin == NetCache.ProfileNotice.NoticeOrigin.OUT_OF_BAND_LICENSE)
    {
      ForgeTicketRewardData data = this.Data as ForgeTicketRewardData;
      headline = GameStrings.Get("GLOBAL_REWARD_FORGE_HEADLINE");
      source = GameStrings.Format("GLOBAL_REWARD_BOOSTER_DETAILS_OUT_OF_BAND", (object) data.Quantity);
    }
    else if (this.Data.Origin == NetCache.ProfileNotice.NoticeOrigin.ACHIEVEMENT)
    {
      headline = GameStrings.Get("GLOBAL_REWARD_ARENA_TICKET_HEADLINE");
    }
    else
    {
      headline = GameStrings.Get("GLOBAL_REWARD_FORGE_UNLOCKED_HEADLINE");
      source = GameStrings.Get("GLOBAL_REWARD_FORGE_UNLOCKED_SOURCE");
    }
    this.SetRewardText(headline, empty2, source);
    if ((Object) this.m_countLabel != (Object) null)
      this.m_countLabel.Text = (this.Data as ForgeTicketRewardData).Quantity.ToString();
    if ((Object) this.m_playerNameLabel != (Object) null)
    {
      BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
      if (myPlayer != null)
        this.m_playerNameLabel.Text = myPlayer.GetBattleTag().GetName();
    }
    this.m_root.SetActive(true);
    this.m_ticketVisual.transform.localEulerAngles = new Vector3(this.m_ticketVisual.transform.localEulerAngles.x, this.m_ticketVisual.transform.localEulerAngles.y, 180f);
    iTween.RotateAdd(this.m_ticketVisual, iTween.Hash((object) "amount", (object) new Vector3(0.0f, 0.0f, 540f), (object) "time", (object) 1.5f, (object) "easeType", (object) iTween.EaseType.easeOutElastic, (object) "space", (object) Space.Self));
  }

  protected override void HideReward()
  {
    base.HideReward();
    this.m_root.SetActive(false);
  }
}
