﻿// Decompiled with JetBrains decompiler
// Type: DeckTemplatePickerButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class DeckTemplatePickerButton : PegUIElement
{
  public static readonly int s_MinimumRecommendedSize = 25;
  public List<UberText> m_cardCountTexts = new List<UberText>();
  public MeshRenderer m_deckTexture;
  public MeshRenderer m_packRibbon;
  public GameObject m_selectGlow;
  public UberText m_title;
  public GameObject m_incompleteTextRibbon;
  public GameObject m_completeTextRibbon;
  private bool m_isStarterDeck;
  private int m_ownedCardCount;

  public void SetIsStarterDeck(bool starter)
  {
    this.m_isStarterDeck = starter;
  }

  public bool IsStarterDeck()
  {
    return this.m_isStarterDeck;
  }

  public void SetSelected(bool selected)
  {
    if (!((Object) this.m_selectGlow != (Object) null))
      return;
    this.m_selectGlow.SetActive(selected);
  }

  public void SetTitleText(string titleText)
  {
    if (!((Object) this.m_title != (Object) null))
      return;
    this.m_title.Text = titleText;
  }

  public void SetCardCountText(int count)
  {
    this.m_ownedCardCount = count;
    int deckSize = CollectionManager.Get().GetDeckSize();
    using (List<UberText>.Enumerator enumerator = this.m_cardCountTexts.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Text = string.Format("{0}/{1}", (object) count, (object) deckSize);
    }
    bool flag = count < DeckTemplatePickerButton.s_MinimumRecommendedSize && !this.m_isStarterDeck;
    if ((Object) this.m_incompleteTextRibbon != (Object) null)
      this.m_incompleteTextRibbon.SetActive(flag);
    if (!((Object) this.m_completeTextRibbon != (Object) null))
      return;
    this.m_completeTextRibbon.SetActive(!flag);
  }

  public int GetOwnedCardCount()
  {
    return this.m_ownedCardCount;
  }

  public void SetDeckTexture(string texturePath)
  {
    if ((Object) this.m_deckTexture == (Object) null)
      return;
    Texture texture = string.IsNullOrEmpty(texturePath) ? (Texture) null : AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(texturePath), false);
    if (!((Object) texture != (Object) null))
      return;
    this.m_deckTexture.material.mainTexture = texture;
  }
}
