﻿// Decompiled with JetBrains decompiler
// Type: BoxStartButtonStateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class BoxStartButtonStateInfo
{
  public float m_ShownAlpha = 1f;
  public float m_ShownFadeSec = 0.3f;
  public iTween.EaseType m_ShownFadeEaseType = iTween.EaseType.linear;
  public float m_HiddenFadeSec = 0.3f;
  public iTween.EaseType m_HiddenFadeEaseType = iTween.EaseType.linear;
  public float m_ShownDelaySec;
  public float m_HiddenAlpha;
  public float m_HiddenDelaySec;
}
