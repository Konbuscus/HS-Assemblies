﻿// Decompiled with JetBrains decompiler
// Type: ManaFilterTabManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ManaFilterTabManager : MonoBehaviour
{
  private List<ManaFilterTab> m_tabs = new List<ManaFilterTab>();
  private int m_currentFilterValue = ManaFilterTab.ALL_TAB_IDX;
  public ManaFilterTab m_singleManaFilterPrefab;
  public ManaFilterTab m_dynamicManaFilterPrefab;
  public MultiSliceElement m_manaCrystalContainer;
  private bool m_tabsActive;

  private void Awake()
  {
  }

  public void ClearFilter()
  {
    this.UpdateCurrentFilterValue(ManaFilterTab.ALL_TAB_IDX);
  }

  public void SetUpTabs()
  {
    for (int index = 0; index <= ManaFilterTab.SEVEN_PLUS_TAB_IDX - 1; ++index)
      this.CreateNewTab(this.m_singleManaFilterPrefab, index);
    this.CreateNewTab(this.m_dynamicManaFilterPrefab, ManaFilterTab.SEVEN_PLUS_TAB_IDX);
    this.m_manaCrystalContainer.UpdateSlices();
  }

  public void ActivateTabs(bool active)
  {
    this.m_tabsActive = active;
    this.UpdateFilterStates();
    if (!active)
      return;
    this.m_manaCrystalContainer.UpdateSlices();
  }

  public void Enable(bool enabled)
  {
    using (List<ManaFilterTab>.Enumerator enumerator = this.m_tabs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ManaFilterTab current = enumerator.Current;
        current.SetEnabled(enabled);
        ManaFilterTab.FilterState state = ManaFilterTab.FilterState.DISABLED;
        if (enabled && this.m_tabsActive)
          state = current.GetManaID() != this.m_currentFilterValue ? ManaFilterTab.FilterState.OFF : ManaFilterTab.FilterState.ON;
        current.SetFilterState(state);
        if ((Object) current.m_costText != (Object) null)
          current.m_costText.gameObject.SetActive(enabled);
      }
    }
  }

  private void CreateNewTab(ManaFilterTab tabPrefab, int index)
  {
    ManaFilterTab manaFilterTab = (ManaFilterTab) GameUtils.Instantiate((Component) tabPrefab, this.m_manaCrystalContainer.gameObject, false);
    manaFilterTab.SetManaID(index);
    manaFilterTab.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnTabPressed));
    manaFilterTab.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnTabMousedOver));
    manaFilterTab.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnTabMousedOut));
    manaFilterTab.SetFilterState(ManaFilterTab.FilterState.DISABLED);
    if (UniversalInputManager.Get().IsTouchMode())
      manaFilterTab.SetReceiveReleaseWithoutMouseDown(true);
    this.m_tabs.Add(manaFilterTab);
    this.m_manaCrystalContainer.AddSlice(manaFilterTab.gameObject);
  }

  private void OnTabPressed(UIEvent e)
  {
    if (!this.m_tabsActive)
      return;
    ManaFilterTab element = (ManaFilterTab) e.GetElement();
    if (!(bool) UniversalInputManager.UsePhoneUI && !Options.Get().GetBool(Option.HAS_CLICKED_MANA_TAB, false) && UserAttentionManager.CanShowAttentionGrabber("ManaFilterTabManager.OnTabPressed:" + (object) Option.HAS_CLICKED_MANA_TAB))
    {
      Options.Get().SetBool(Option.HAS_CLICKED_MANA_TAB, true);
      this.ShowManaTabHint(element);
    }
    if (element.GetManaID() == this.m_currentFilterValue)
      this.UpdateCurrentFilterValue(ManaFilterTab.ALL_TAB_IDX);
    else
      this.UpdateCurrentFilterValue(element.GetManaID());
  }

  private void OnTabMousedOver(UIEvent e)
  {
    if (!this.m_tabsActive)
      return;
    ((ManaFilterTab) e.GetElement()).NotifyMousedOver();
  }

  private void OnTabMousedOut(UIEvent e)
  {
    if (!this.m_tabsActive)
      return;
    ((ManaFilterTab) e.GetElement()).NotifyMousedOut();
  }

  private void UpdateCurrentFilterValue(int filterValue)
  {
    if (filterValue != this.m_currentFilterValue)
    {
      SoundManager.Get().LoadAndPlay("mana_crystal_refresh");
      CollectionManagerDisplay.Get().FilterByManaCost(filterValue);
    }
    this.m_currentFilterValue = filterValue;
    this.UpdateFilterStates();
  }

  private void UpdateFilterStates()
  {
    using (List<ManaFilterTab>.Enumerator enumerator = this.m_tabs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ManaFilterTab current = enumerator.Current;
        ManaFilterTab.FilterState state = ManaFilterTab.FilterState.DISABLED;
        if (this.m_tabsActive)
          state = current.GetManaID() != this.m_currentFilterValue ? ManaFilterTab.FilterState.OFF : ManaFilterTab.FilterState.ON;
        current.SetFilterState(state);
      }
    }
  }

  private void ShowManaTabHint(ManaFilterTab tabButton)
  {
    Notification popupText = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, tabButton.transform.position + new Vector3(0.0f, 0.0f, 7f), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("GLUE_COLLECTION_MANAGER_MANA_TAB_FIRST_CLICK"), true);
    if ((Object) popupText == (Object) null)
      return;
    popupText.ShowPopUpArrow(Notification.PopUpArrowDirection.Down);
    NotificationManager.Get().DestroyNotification(popupText, 3f);
  }
}
