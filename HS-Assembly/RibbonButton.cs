﻿// Decompiled with JetBrains decompiler
// Type: RibbonButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RibbonButton : PegUIElement
{
  public GameObject m_highlight;

  public void Start()
  {
    this.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnButtonOver));
    this.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnButtonOut));
  }

  public void OnButtonOver(UIEvent e)
  {
    if (!((Object) this.m_highlight != (Object) null))
      return;
    this.m_highlight.SetActive(true);
  }

  public void OnButtonOut(UIEvent e)
  {
    if (!((Object) this.m_highlight != (Object) null))
      return;
    this.m_highlight.SetActive(false);
  }
}
