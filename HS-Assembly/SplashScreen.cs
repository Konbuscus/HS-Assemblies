﻿// Decompiled with JetBrains decompiler
// Type: SplashScreen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using WTCG.BI;

[CustomEditClass]
public class SplashScreen : MonoBehaviour
{
  private List<SplashScreen.FinishedHandler> m_finishedListeners = new List<SplashScreen.FinishedHandler>();
  private const float RATINGS_SCREEN_DISPLAY_TIME = 5f;
  private const float GLOW_FADE_TIME = 1f;
  private const float MinPatchingBarDisplayTime = 3f;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_logoPrefab;
  public GameObject m_logoContainer;
  public GameObject m_queueSign;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public String_MobileOverride m_webLoginCanvasPrefab;
  public GameObject m_quitButtonParent;
  public UberText m_queueTitle;
  public UberText m_queueText;
  public UberText m_queueTime;
  public StandardPegButtonNew m_quitButton;
  public Glow m_glow1;
  public Glow m_glow2;
  public GameObject m_blizzardLogo;
  public GameObject m_patchingFrame;
  public ProgressBar m_patchingBar;
  public GameObject m_demoDisclaimer;
  public UberText m_logoCopyright;
  public StandardPegButtonNew m_devClearLoginButton;
  private static SplashScreen s_instance;
  private Network.QueueInfo m_queueInfo;
  private bool m_queueFinished;
  private bool m_queueShown;
  private bool m_fadingStarted;
  private bool m_RatingsFinished;
  private bool m_LogoFinished;
  private bool m_loginFinished;
  private GameObject m_logo;
  private GameObject m_webLoginCanvas;
  private bool m_patching;
  private float m_patchingBarShownTime;
  private string m_webAuthUrl;
  private string m_webToken;
  private bool m_triedToken;
  private WebAuth m_webAuth;
  private bool m_webAuthHidden;
  private bool m_inputCameraSet;
  private bool m_hasProvidedWebAuthToken;

  private void Awake()
  {
    SplashScreen.s_instance = this;
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    string name = FileUtils.GameAssetPathToName(this.m_logoPrefab);
    AssetCache.ClearGameObject(name);
    this.m_logo = AssetLoader.Get().LoadGameObject(name, true, false);
    this.m_logo.SetActive(true);
    GameUtils.SetParent(this.m_logo, this.m_logoContainer, true);
    this.m_logoContainer.SetActive(false);
    if (Localization.GetLocale() == Locale.zhCN)
      this.m_logoCopyright.gameObject.SetActive(true);
    this.m_webLoginCanvas = (GameObject) null;
    this.Show();
    this.UpdateLayout();
    if (Vars.Key("Aurora.ClientCheck").GetBool(true) && BattleNetClient.needsToRun)
    {
      BattleNetClient.quitHearthstoneAndRun();
    }
    else
    {
      Network.Get().RegisterQueueInfoHandler(new Network.QueueInfoHandler(this.QueueInfoHandler));
      if (DemoMgr.Get().GetMode() == DemoMode.APPLE_STORE)
        this.m_demoDisclaimer.SetActive(true);
      if (!ApplicationMgr.IsInternal() || !(bool) ApplicationMgr.AllowResetFromFatalError)
        return;
      this.m_devClearLoginButton.gameObject.SetActive(true);
      this.m_devClearLoginButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ClearLogin));
    }
  }

  private void OnDestroy()
  {
    if (this.m_webAuth != null)
      this.m_webAuth.Close();
    SplashScreen.s_instance = (SplashScreen) null;
  }

  private void Update()
  {
    Network.Get().ProcessNetwork();
    if (!this.m_queueFinished && !Network.ShouldBeConnectedToAurora())
      this.m_queueFinished = true;
    if (!(bool) Network.LAUNCHES_WITH_BNET_APP && !this.m_loginFinished && (!Network.ShouldBeConnectedToAurora() || Network.IsLoggedIn()))
    {
      this.FinishSplashScreen();
      Log.Cameron.Print("finishing splash screen..");
    }
    else
    {
      if (Network.ShouldBeConnectedToAurora())
        this.UpdateWebAuth();
      if (!this.m_inputCameraSet && (Object) PegUI.Get() != (Object) null)
      {
        this.m_inputCameraSet = true;
        PegUI.Get().SetInputCamera(OverlayUI.Get().m_UICamera);
      }
      if (!PlayErrors.IsInitialized())
        PlayErrors.Init();
      this.UpdatePatching();
      this.HandleKeyboardInput();
    }
  }

  public void UpdatePatching()
  {
    if (this.m_patching)
    {
      UpdateManager.UpdateProgress progressForCurrentFile = UpdateManager.Get().GetProgressForCurrentFile();
      if (!this.m_patchingFrame.activeSelf && progressForCurrentFile.showProgressBar && progressForCurrentFile.numFilesToDownload > 0)
      {
        this.m_patchingFrame.SetActive(true);
        this.m_patchingBarShownTime = Time.realtimeSinceStartup;
      }
      if (this.m_patchingFrame.activeSelf && progressForCurrentFile.numFilesToDownload > progressForCurrentFile.numFilesDownloaded)
      {
        this.m_patchingBar.SetProgressBar((float) ((double) progressForCurrentFile.numFilesDownloaded / (double) progressForCurrentFile.numFilesToDownload + (double) progressForCurrentFile.downloadPercentage / (double) progressForCurrentFile.numFilesToDownload));
        this.m_patchingBar.SetLabel(GameStrings.Format("GLUE_PATCHING_LABEL", (object) (progressForCurrentFile.numFilesDownloaded + 1), (object) progressForCurrentFile.numFilesToDownload));
        if (!UpdateManager.Get().UpdateIsRequired() && (double) Time.realtimeSinceStartup >= (double) this.m_patchingBarShownTime + (double) progressForCurrentFile.maxPatchingBarDisplayTime)
        {
          Log.UpdateManager.Print("Optional update is taking too long, no longer blocking.");
          UpdateManager.Get().StopWaitingForUpdate();
        }
      }
    }
    if (!((Object) this.m_patchingFrame != (Object) null) || !this.m_patchingFrame.activeSelf || (this.m_patching || (double) Time.realtimeSinceStartup < (double) this.m_patchingBarShownTime + 3.0))
      return;
    this.m_patchingFrame.SetActive(false);
  }

  public void UpdateWebAuth()
  {
    if (this.m_webAuth != null)
    {
      switch (this.m_webAuth.GetStatus())
      {
        case WebAuth.Status.ReadyToDisplay:
          if (!this.m_webLoginCanvas.activeSelf)
            this.m_webLoginCanvas.SetActive(true);
          if (this.m_webAuthHidden || this.m_webAuth.IsShown())
            break;
          this.m_webAuth.Show();
          break;
        case WebAuth.Status.Success:
          this.m_webToken = this.m_webAuth.GetLoginCode();
          Log.BattleNet.Print("Web token retrieved from web pane: {0}", (object) this.m_webToken);
          WebAuth.SetStoredToken(this.m_webToken);
          BattleNet.ProvideWebAuthToken(this.m_webToken);
          this.HideWebLoginCanvas();
          this.m_webAuth.Close();
          break;
        case WebAuth.Status.Error:
          BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_WEB_LOGIN_ERROR);
          Network.Get().ShowConnectionFailureError("GLOBAL_ERROR_NETWORK_LOGIN_FAILURE");
          break;
      }
    }
    else
    {
      if (!BattleNet.CheckWebAuth(out this.m_webAuthUrl))
        return;
      if (ApplicationMgr.IsInternal() && this.m_hasProvidedWebAuthToken)
        Log.Yim.Print("Fetching new webToken since last one failed...");
      string str = Vars.Key("Aurora.VerifyWebCredentials").GetStr((string) null);
      if (str != null)
      {
        BattleNet.ProvideWebAuthToken(str);
        this.m_hasProvidedWebAuthToken = true;
        UnityEngine.Debug.Log((object) string.Format("Calling ProvideWebAuthToken with={0}. If this repeats, then web token is invalid!", (object) str));
      }
      else
      {
        this.m_webToken = BattleNet.GetLaunchOption("WEB_TOKEN", true);
        if (GameUtils.AreAllTutorialsComplete(Options.Get().GetEnum<TutorialProgress>(Option.LOCAL_TUTORIAL_PROGRESS)))
          this.m_webAuthUrl = NydusLink.GetAccountCreationLink();
        UnityEngine.Debug.Log((object) ("Web URL for auth: " + this.m_webAuthUrl));
        MobileCallbackManager.Get().m_wasBreakingNewsShown = false;
        if (!string.IsNullOrEmpty(this.m_webToken))
        {
          BattleNet.ProvideWebAuthToken(this.m_webToken);
          this.m_triedToken = true;
          this.m_webToken = (string) null;
          this.RequestBreakingNews();
        }
        else
        {
          this.m_webLoginCanvas = (GameObject) GameUtils.InstantiateGameObject((string) ((MobileOverrideValue<string>) this.m_webLoginCanvasPrefab), (GameObject) null, false);
          WebLoginCanvas component = this.m_webLoginCanvas.GetComponent<WebLoginCanvas>();
          this.m_webLoginCanvas.SetActive(false);
          Camera uiCamera = OverlayUI.Get().m_UICamera;
          Vector3 screenPoint1 = uiCamera.WorldToScreenPoint(component.m_topLeftBone.transform.position);
          Vector3 screenPoint2 = uiCamera.WorldToScreenPoint(component.m_bottomRightBone.transform.position);
          this.m_webAuth = new WebAuth(this.m_webAuthUrl, screenPoint1.x, (float) uiCamera.pixelHeight - screenPoint1.y, screenPoint2.x - screenPoint1.x, screenPoint1.y - screenPoint2.y, this.m_webLoginCanvas.gameObject.name);
          this.m_webAuth.SetCountryCodeCookie(MobileDeviceLocale.GetCountryCode(), ApplicationMgr.GetMobileEnvironment() != MobileEnv.DEVELOPMENT ? ".battle.net" : ".blizzard.net");
          this.m_webAuth.Load();
          this.RequestBreakingNews();
          MobileCallbackManager.Get().m_wasBreakingNewsShown = true;
          if (!this.m_triedToken)
            return;
          Log.BattleNet.Print("Submitted login token {0} but it was rejected, so deleting stored token.", (object) this.m_webToken);
          WebAuth.ClearLoginData();
        }
      }
    }
  }

  public void HideWebLoginCanvas()
  {
    if (!((Object) this.m_webLoginCanvas != (Object) null))
      return;
    Object.Destroy((Object) this.m_webLoginCanvas);
    this.m_webLoginCanvas = (GameObject) null;
  }

  public static SplashScreen Get()
  {
    return SplashScreen.s_instance;
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    iTween.FadeTo(this.gameObject, iTween.Hash((object) "amount", (object) 1f, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutCubic));
    if (this.m_fadingStarted)
      return;
    this.FadeGlowsIn();
  }

  public void Hide()
  {
    if (this.m_webAuth != null)
      this.m_webAuth.Close();
    this.HideWebLoginCanvas();
    this.StartCoroutine(this.HideCoroutine());
  }

  public void HideLogo()
  {
    this.m_logoContainer.SetActive(false);
  }

  public void HideWebAuth()
  {
    UnityEngine.Debug.Log((object) "HideWebAuth");
    if (this.m_webAuth == null)
      return;
    this.m_webAuth.Hide();
    this.m_webAuthHidden = true;
  }

  public void UnHideWebAuth()
  {
    UnityEngine.Debug.Log((object) "ShowWebAuth");
    if (this.m_webAuth == null || this.m_webAuth.GetStatus() < WebAuth.Status.ReadyToDisplay)
      return;
    this.m_webAuth.Show();
    this.m_webAuthHidden = false;
  }

  public void StartPatching()
  {
    this.m_patching = true;
  }

  public void StopPatching()
  {
    this.m_patching = false;
    if (!((Object) this.m_patchingBar != (Object) null) || !this.m_patchingBar.gameObject.activeInHierarchy)
      return;
    this.m_patchingBar.SetProgressBar(1f);
  }

  public void ShowRatings()
  {
    this.StartCoroutine(this.RatingsScreen());
  }

  public bool IsRatingsScreenFinished()
  {
    return this.m_RatingsFinished;
  }

  public bool IsFinished()
  {
    return this.m_loginFinished;
  }

  public bool AddFinishedListener(SplashScreen.FinishedHandler handler)
  {
    if (this.m_finishedListeners.Contains(handler))
      return false;
    this.m_finishedListeners.Add(handler);
    return true;
  }

  public virtual bool RemoveFinishedListener(SplashScreen.FinishedHandler handler)
  {
    return this.m_finishedListeners.Remove(handler);
  }

  public void UpdateLayout()
  {
    if (PlatformSettings.IsMobile())
      this.m_blizzardLogo.SetActive(false);
    else
      TransformUtil.SetPosX(this.m_blizzardLogo, CameraUtils.GetNearClipBounds(OverlayUI.Get().m_UICamera).max.x - this.m_blizzardLogo.GetComponent<Renderer>().bounds.size.x / 1.5f);
  }

  public bool IsWebLoginCanvasActive()
  {
    return (Object) this.m_webLoginCanvas != (Object) null && this.m_webLoginCanvas.activeSelf;
  }

  [DebuggerHidden]
  private IEnumerator HideCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SplashScreen.\u003CHideCoroutine\u003Ec__Iterator25E() { \u003C\u003Ef__this = this };
  }

  private void DestroySplashScreen()
  {
    Object.Destroy((Object) this.gameObject);
  }

  public void FinishSplashScreen()
  {
    this.m_loginFinished = true;
    this.StartCoroutine(this.FadeInLogo());
    this.StartCoroutine(this.FireFinishedEvent());
  }

  private void QueueInfoHandler(Network.QueueInfo queueInfo)
  {
    this.m_queueInfo = queueInfo;
    if (queueInfo.position == 0)
    {
      Network.Get().RemoveQueueInfoHandler(new Network.QueueInfoHandler(this.QueueInfoHandler));
      this.m_queueFinished = true;
      this.m_queueShown = false;
      this.m_queueSign.SetActive(false);
      if (!(bool) Network.LAUNCHES_WITH_BNET_APP)
        return;
      this.FinishSplashScreen();
    }
    else
      this.ShowQueueInfo();
  }

  private void ShowQueueInfo()
  {
    if (!this.m_queueShown)
    {
      this.m_queueShown = true;
      this.m_queueTitle.Text = GameStrings.Get("GLUE_SPLASH_QUEUE_TITLE");
      this.m_queueText.Text = GameStrings.Get("GLUE_SPLASH_QUEUE_TEXT");
      if (PlatformSettings.IsMobile())
      {
        this.m_quitButtonParent.SetActive(false);
      }
      else
      {
        this.m_quitButton.SetOriginalLocalPosition();
        this.m_quitButton.SetText(GameStrings.Get("GLOBAL_QUIT"));
        this.m_quitButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.QuitGame));
      }
      RenderUtils.SetAlpha(this.m_queueSign, 0.0f);
      this.m_queueSign.SetActive(true);
      iTween.FadeTo(this.m_queueSign, iTween.Hash((object) "amount", (object) 1f, (object) "time", (object) 0.5f, (object) "easeType", (object) iTween.EaseType.easeOutCubic));
      iTween.FadeTo(this.m_logoContainer, iTween.Hash((object) "amount", (object) 0.0f, (object) "time", (object) 0.5f, (object) "includechildren", (object) true, (object) "easeType", (object) iTween.EaseType.easeOutCubic));
    }
    this.m_queueTime.Text = TimeUtils.GetElapsedTimeString((int) this.m_queueInfo.end, TimeUtils.SPLASHSCREEN_DATETIME_STRINGSET);
  }

  private void QuitGame(UIEvent e)
  {
    ApplicationMgr.Get().Exit();
  }

  private void ClearLogin(UIEvent e)
  {
    UnityEngine.Debug.LogWarning((object) "Clear Login Button pressed from the Splash Screen!");
    WebAuth.ClearLoginData();
  }

  [DebuggerHidden]
  private IEnumerator FadeGlowInOut(Glow glow, float timeDelay, bool shouldStartOver)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SplashScreen.\u003CFadeGlowInOut\u003Ec__Iterator25F() { timeDelay = timeDelay, glow = glow, shouldStartOver = shouldStartOver, \u003C\u0024\u003EtimeDelay = timeDelay, \u003C\u0024\u003Eglow = glow, \u003C\u0024\u003EshouldStartOver = shouldStartOver, \u003C\u003Ef__this = this };
  }

  private void FadeGlowsIn()
  {
    this.m_fadingStarted = true;
    this.StartCoroutine(this.FadeGlowInOut(this.m_glow1, 0.0f, false));
    this.StartCoroutine(this.FadeGlowInOut(this.m_glow2, 1f, true));
  }

  [DebuggerHidden]
  private IEnumerator FireFinishedEvent()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SplashScreen.\u003CFireFinishedEvent\u003Ec__Iterator260() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator FadeInLogo()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SplashScreen.\u003CFadeInLogo\u003Ec__Iterator261() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator RatingsScreen()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SplashScreen.\u003CRatingsScreen\u003Ec__Iterator262() { \u003C\u003Ef__this = this };
  }

  private void ShowRatingsScreen(SplashScreen.RatingsScreenRegion region)
  {
    if (region == SplashScreen.RatingsScreenRegion.NONE)
      return;
    AssetLoader.Get().LoadGameObject(region != SplashScreen.RatingsScreenRegion.CHINA ? "Korean_Ratings_SplashScreen" : "China_Ratings_SplashScreen", new AssetLoader.GameObjectCallback(this.OnRatingsScreenLoaded), (object) null, false);
  }

  private void OnRatingsScreenLoaded(string name, GameObject go, object callbackData)
  {
    OverlayUI.Get().AddGameObject(go, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    go.SetActive(false);
    this.m_queueSign.SetActive(false);
    this.m_queueTitle.gameObject.SetActive(false);
    this.m_queueText.gameObject.SetActive(false);
    this.m_queueTime.gameObject.SetActive(false);
    this.StartCoroutine(this.RatingsScreenWait(go));
  }

  [DebuggerHidden]
  private IEnumerator RatingsScreenWait(GameObject ratingsObject)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SplashScreen.\u003CRatingsScreenWait\u003Ec__Iterator263() { ratingsObject = ratingsObject, \u003C\u0024\u003EratingsObject = ratingsObject, \u003C\u003Ef__this = this };
  }

  private void RequestBreakingNews()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    BreakingNews.FetchBreakingNews(NydusLink.GetBreakingNewsLink(), new BreakingNews.BreakingNewsRecievedDelegate(new SplashScreen.\u003CRequestBreakingNews\u003Ec__AnonStorey3DB()
    {
      breakingNewsLocalized = GameStrings.Get("GLUE_MOBILE_SPLASH_SCREEN_BREAKING_NEWS")
    }.\u003C\u003Em__19A));
  }

  public bool HandleKeyboardInput()
  {
    return false;
  }

  private enum RatingsScreenRegion
  {
    NONE,
    KOREA,
    CHINA,
  }

  public delegate void FinishedHandler();
}
