﻿// Decompiled with JetBrains decompiler
// Type: TextureOffsetStates
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TextureOffsetStates : MonoBehaviour
{
  public TextureOffsetState[] m_states;
  private string m_currentState;
  private Material m_originalMaterial;

  public string CurrentState
  {
    get
    {
      return this.m_currentState;
    }
    set
    {
      TextureOffsetStates.\u003C\u003Ec__AnonStorey465 cAnonStorey465 = new TextureOffsetStates.\u003C\u003Ec__AnonStorey465();
      cAnonStorey465.value = value;
      TextureOffsetState textureOffsetState = ((IEnumerable<TextureOffsetState>) this.m_states).FirstOrDefault<TextureOffsetState>(new Func<TextureOffsetState, bool>(cAnonStorey465.\u003C\u003Em__345));
      if (textureOffsetState == null)
        return;
      this.m_currentState = cAnonStorey465.value;
      if ((UnityEngine.Object) textureOffsetState.Material == (UnityEngine.Object) null)
        this.GetComponent<Renderer>().material = this.m_originalMaterial;
      else
        this.GetComponent<Renderer>().material = textureOffsetState.Material;
      this.GetComponent<Renderer>().material.mainTextureOffset = textureOffsetState.Offset;
    }
  }

  private void Awake()
  {
    this.m_originalMaterial = this.GetComponent<Renderer>().sharedMaterial;
  }
}
