﻿// Decompiled with JetBrains decompiler
// Type: CollectionManagerScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class CollectionManagerScene : Scene
{
  private bool m_unloading;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public String_MobileOverride m_CollectionManagerPrefab;

  protected override void Awake()
  {
    base.Awake();
    AssetLoader.Get().LoadUIScreen(FileUtils.GameAssetPathToName((string) ((MobileOverrideValue<string>) this.m_CollectionManagerPrefab)), new AssetLoader.GameObjectCallback(this.OnUIScreenLoaded), (object) null, false);
  }

  private void Update()
  {
    Network.Get().ProcessNetwork();
  }

  public override bool IsUnloading()
  {
    return this.m_unloading;
  }

  public override void Unload()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
      BnetBar.Get().ToggleActive(true);
    this.m_unloading = true;
    CollectionManagerDisplay.Get().Unload();
    Network.SendAckCardsSeen();
    this.m_unloading = false;
  }

  private void OnUIScreenLoaded(string name, GameObject screen, object callbackData)
  {
    if ((Object) screen == (Object) null)
      UnityEngine.Debug.LogError((object) string.Format("CollectionManagerScene.OnUIScreenLoaded() - failed to load screen {0}", (object) name));
    else
      this.StartCoroutine(this.NotifySceneLoadedWhenReady());
  }

  [DebuggerHidden]
  private IEnumerator NotifySceneLoadedWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManagerScene.\u003CNotifySceneLoadedWhenReady\u003Ec__Iterator42 readyCIterator42 = new CollectionManagerScene.\u003CNotifySceneLoadedWhenReady\u003Ec__Iterator42();
    return (IEnumerator) readyCIterator42;
  }
}
