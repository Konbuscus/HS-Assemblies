﻿// Decompiled with JetBrains decompiler
// Type: ArenaTrayDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class ArenaTrayDisplay : MonoBehaviour
{
  public float m_TheKeyTransitionDelay = 0.5f;
  public float m_TheKeyTransitionFadeInTime = 1.5f;
  public float m_TheKeyTransitionFadeOutTime = 2f;
  public string m_TheKeyTransitionSound = "arena_key_transition";
  public int m_Rank;
  public PlayMakerFSM m_RewardPlaymaker;
  [CustomEditField(Sections = "Keys")]
  public GameObject m_TheKeyMesh;
  public GameObject m_TheKeyGlowPlane;
  public GameObject m_TheKeyGlowHoleMesh;
  public GameObject m_TheKeySelectionGlow;
  public GameObject m_TheKeyOldSelectionGlow;
  public ParticleSystem m_TheKeyTransitionParticles;
  [CustomEditField(Sections = "Reward Panel")]
  public UberText m_WinCountUberText;
  public UberText m_WinsUberText;
  public UberText m_LossesUberText;
  public GameObject m_XmarksRoot;
  public List<GameObject> m_XmarkBox;
  public GameObject m_Xmark1;
  public GameObject m_Xmark2;
  public GameObject m_Xmark3;
  public GameObject m_RewardDoorPlates;
  public GameObject m_BehindTheDoors;
  public GameObject m_Paper;
  public GameObject m_PaperMain;
  public Material m_PlainPaperMaterial;
  public Material m_RewardPaperMaterial;
  public GameObject m_RewardBoxesBone;
  public GameObject m_InstructionText;
  public List<ArenaTrayDisplay.ArenaKeyVisualData> m_ArenaKeyVisualData;
  private RewardBoxesDisplay m_RewardBoxes;
  private GameObject m_TheKeyParticleSystems;
  private GameObject m_TheKeyIdleEffects;
  private bool m_isTheKeyIdleEffectsLoading;
  private static ArenaTrayDisplay s_Instance;

  private void Awake()
  {
    ArenaTrayDisplay.s_Instance = this;
  }

  private void Start()
  {
    if ((UnityEngine.Object) this.m_WinsUberText == (UnityEngine.Object) null || (UnityEngine.Object) this.m_LossesUberText == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: m_WinsUberText or m_LossesUberText is null!");
    }
    else
    {
      this.m_WinsUberText.Text = GameStrings.Get("GLUE_DRAFT_WINS_LABEL");
      this.m_LossesUberText.Text = GameStrings.Get("GLUE_DRAFT_LOSSES_LABEL");
      if ((UnityEngine.Object) this.m_BehindTheDoors == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: m_BehindTheDoors is null!");
      }
      else
      {
        this.m_BehindTheDoors.SetActive(false);
        if ((UnityEngine.Object) this.m_RewardDoorPlates == (UnityEngine.Object) null)
        {
          UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: m_RewardDoorPlates is null!");
        }
        else
        {
          this.m_RewardDoorPlates.SetActive(false);
          SceneUtils.EnableColliders(this.m_TheKeyMesh, false);
        }
      }
    }
  }

  private void OnDisable()
  {
  }

  private void OnDestroy()
  {
  }

  private void OnEnable()
  {
  }

  public static ArenaTrayDisplay Get()
  {
    return ArenaTrayDisplay.s_Instance;
  }

  public void UpdateTray()
  {
    this.UpdateTray(true);
  }

  public void UpdateTray(bool showNewKey)
  {
    this.ShowPlainPaper();
    if ((UnityEngine.Object) this.m_InstructionText != (UnityEngine.Object) null)
      this.m_InstructionText.SetActive(false);
    if ((UnityEngine.Object) this.m_RewardDoorPlates != (UnityEngine.Object) null && !this.m_RewardDoorPlates.activeSelf)
      this.m_RewardDoorPlates.SetActive(true);
    bool flag = false;
    DraftManager draftManager = DraftManager.Get();
    if (draftManager == null)
    {
      UnityEngine.Debug.LogError((object) "ArenaTrayDisplay: DraftManager.Get() == null!");
    }
    else
    {
      int wins = draftManager.GetWins();
      int losses = draftManager.GetLosses();
      if (SceneMgr.Get().GetPrevMode() == SceneMgr.Mode.GAMEPLAY && GameMgr.Get().WasArena() && draftManager.GetIsNewKey())
        flag = true;
      this.m_WinCountUberText.Text = wins.ToString();
      if (losses > 0)
        this.m_Xmark1.GetComponent<Renderer>().enabled = true;
      else
        this.m_Xmark1.GetComponent<Renderer>().enabled = false;
      if (losses > 1)
        this.m_Xmark2.GetComponent<Renderer>().enabled = true;
      else
        this.m_Xmark2.GetComponent<Renderer>().enabled = false;
      if (losses > 2)
        this.m_Xmark3.GetComponent<Renderer>().enabled = true;
      else
        this.m_Xmark3.GetComponent<Renderer>().enabled = false;
      this.UpdateXBoxes();
      if (flag && wins > 0 && showNewKey)
      {
        this.UpdateKeyArt(wins - 1);
        this.StartCoroutine(this.AnimateKeyTransition(wins));
      }
      else
        this.UpdateKeyArt(wins);
    }
  }

  public void ShowPlainPaperBackground()
  {
    this.ShowPlainPaper();
    if ((UnityEngine.Object) this.m_InstructionText != (UnityEngine.Object) null)
      this.m_InstructionText.SetActive(true);
    if (!((UnityEngine.Object) this.m_RewardDoorPlates != (UnityEngine.Object) null) || !this.m_RewardDoorPlates.activeSelf)
      return;
    this.m_RewardDoorPlates.SetActive(false);
  }

  public void ActivateKey()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ArenaTrayDisplay.\u003CActivateKey\u003Ec__AnonStorey3AF keyCAnonStorey3Af = new ArenaTrayDisplay.\u003CActivateKey\u003Ec__AnonStorey3AF();
    SceneUtils.EnableColliders(this.m_TheKeyMesh, true);
    this.m_TheKeySelectionGlow.GetComponent<Renderer>().enabled = true;
    Color color = this.m_TheKeySelectionGlow.GetComponent<Renderer>().sharedMaterial.color;
    color.a = 0.0f;
    this.m_TheKeySelectionGlow.GetComponent<Renderer>().sharedMaterial.color = color;
    this.m_TheKeySelectionGlow.GetComponent<Renderer>().sharedMaterial.SetFloat("_FxIntensity", 1f);
    iTween.FadeTo(this.m_TheKeySelectionGlow, iTween.Hash((object) "alpha", (object) 0.8f, (object) "time", (object) 2f, (object) "easetype", (object) iTween.EaseType.easeInOutBack));
    // ISSUE: reference to a compiler-generated field
    keyCAnonStorey3Af.KeyGlowMat = this.m_TheKeySelectionGlow.GetComponent<Renderer>().material;
    // ISSUE: reference to a compiler-generated field
    keyCAnonStorey3Af.KeyGlowMat.SetFloat("_FxIntensity", 0.0f);
    // ISSUE: reference to a compiler-generated method
    iTween.ValueTo(this.m_TheKeySelectionGlow, iTween.Hash((object) "time", (object) 2f, (object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "easetype", (object) iTween.EaseType.easeInOutBack, (object) "onupdate", (object) new Action<object>(keyCAnonStorey3Af.\u003C\u003Em__121), (object) "onupdatetarget", (object) this.m_TheKeySelectionGlow));
    PegUIElement component = this.m_TheKeyMesh.GetComponent<PegUIElement>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: PegUIElement missing on the Key!");
    }
    else
    {
      component.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.OpenRewardBox));
      Navigation.Push(new Navigation.NavigateBackHandler(Navigation.BlockBackingOut));
    }
  }

  public void ShowRewardsOpenAtStart()
  {
    if ((UnityEngine.Object) this.m_RewardPlaymaker == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: Missing Playmaker FSM!");
    }
    else
    {
      this.HidePaper();
      if ((UnityEngine.Object) this.m_InstructionText != (UnityEngine.Object) null)
        this.m_InstructionText.SetActive(false);
      if ((UnityEngine.Object) this.m_WinCountUberText != (UnityEngine.Object) null)
        this.m_WinCountUberText.gameObject.SetActive(false);
      if ((UnityEngine.Object) this.m_WinsUberText != (UnityEngine.Object) null)
        this.m_WinsUberText.gameObject.SetActive(false);
      if ((UnityEngine.Object) this.m_LossesUberText != (UnityEngine.Object) null)
        this.m_LossesUberText.gameObject.SetActive(false);
      if ((UnityEngine.Object) this.m_XmarksRoot != (UnityEngine.Object) null)
        this.m_XmarksRoot.SetActive(false);
      if ((UnityEngine.Object) this.m_TheKeySelectionGlow != (UnityEngine.Object) null)
        this.m_TheKeySelectionGlow.SetActive(false);
      this.m_WinsUberText.gameObject.SetActive(false);
      this.m_LossesUberText.gameObject.SetActive(false);
      this.m_TheKeyMesh.gameObject.SetActive(false);
      if ((UnityEngine.Object) this.m_BehindTheDoors == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: m_BehindTheDoors is null!");
      }
      else
      {
        this.m_BehindTheDoors.SetActive(true);
        if (DraftManager.Get() == null)
        {
          UnityEngine.Debug.LogError((object) "ArenaTrayDisplay: DraftManager.Get() == null!");
        }
        else
        {
          AssetLoader.Get().LoadGameObject("RewardBoxes", (AssetLoader.GameObjectCallback) ((name, go, callbackData) =>
          {
            this.m_RewardBoxes = go.GetComponent<RewardBoxesDisplay>();
            this.m_RewardBoxes.SetRewards(DraftManager.Get().GetRewards());
            this.m_RewardBoxes.RegisterDoneCallback(new Action(this.OnRewardBoxesDone));
            TransformUtil.AttachAndPreserveLocalTransform(this.m_RewardBoxes.transform, this.m_RewardBoxesBone.transform);
            this.m_RewardBoxes.DebugLogRewards();
            this.m_RewardBoxes.ShowAlreadyOpenedRewards();
          }), (object) null, false);
          this.m_RewardPlaymaker.gameObject.SetActive(true);
          this.m_RewardPlaymaker.SendEvent("Death");
          if (!((UnityEngine.Object) this.m_TheKeyMesh.GetComponent<PegUIElement>() == (UnityEngine.Object) null))
            return;
          UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: PegUIElement missing on the Key!");
        }
      }
    }
  }

  public void ShowOpenedRewards()
  {
  }

  public void AnimateRewards()
  {
    AssetLoader.Get().LoadGameObject("RewardBoxes", (AssetLoader.GameObjectCallback) ((name, go, callbackData) =>
    {
      this.m_RewardBoxes = go.GetComponent<RewardBoxesDisplay>();
      this.m_RewardBoxes.SetRewards(DraftManager.Get().GetRewards());
      this.m_RewardBoxes.RegisterDoneCallback(new Action(this.OnRewardBoxesDone));
      TransformUtil.AttachAndPreserveLocalTransform(this.m_RewardBoxes.transform, this.m_RewardBoxesBone.transform);
      this.m_RewardBoxes.AnimateRewards();
    }), (object) null, false);
  }

  public void KeyFXCancel()
  {
    if (!(bool) ((UnityEngine.Object) this.m_TheKeyIdleEffects))
      return;
    PlayMakerFSM componentInChildren = this.m_TheKeyIdleEffects.GetComponentInChildren<PlayMakerFSM>();
    if (!(bool) ((UnityEngine.Object) componentInChildren))
      return;
    componentInChildren.SendEvent("Cancel");
  }

  private void UpdateKeyArt(int rank)
  {
    if ((UnityEngine.Object) this.m_TheKeyMesh == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: key mesh missing!");
    }
    else
    {
      this.ShowRewardPaper();
      ArenaTrayDisplay.ArenaKeyVisualData arenaKeyVisualData = this.m_ArenaKeyVisualData[rank];
      if ((UnityEngine.Object) arenaKeyVisualData.m_Mesh != (UnityEngine.Object) null)
      {
        MeshFilter component = this.m_TheKeyMesh.GetComponent<MeshFilter>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          component.mesh = UnityEngine.Object.Instantiate<Mesh>(arenaKeyVisualData.m_Mesh);
      }
      if ((UnityEngine.Object) arenaKeyVisualData.m_Material != (UnityEngine.Object) null)
        this.m_TheKeyMesh.GetComponent<Renderer>().sharedMaterial = UnityEngine.Object.Instantiate<Material>(arenaKeyVisualData.m_Material);
      if (arenaKeyVisualData.m_IdleEffectsPrefabName != string.Empty)
      {
        this.m_isTheKeyIdleEffectsLoading = true;
        AssetLoader.Get().LoadGameObject(arenaKeyVisualData.m_IdleEffectsPrefabName, new AssetLoader.GameObjectCallback(this.OnIdleEffectsLoaded), (object) null, false);
      }
      if ((UnityEngine.Object) arenaKeyVisualData.m_ParticlePrefab != (UnityEngine.Object) null)
      {
        GameObject gameObject1 = UnityEngine.Object.Instantiate<GameObject>(arenaKeyVisualData.m_ParticlePrefab);
        Transform child1 = gameObject1.transform.FindChild("FX_Motes");
        if ((UnityEngine.Object) child1 != (UnityEngine.Object) null)
        {
          GameObject gameObject2 = child1.gameObject;
          gameObject2.transform.parent = this.m_TheKeyMesh.transform;
          gameObject2.transform.localPosition = Vector3.zero;
          gameObject2.transform.localRotation = Quaternion.identity;
          this.m_RewardPlaymaker.FsmVariables.GetFsmGameObject("FX_Motes").Value = gameObject2;
        }
        Transform child2 = gameObject1.transform.FindChild("FX_Motes_glow");
        if ((UnityEngine.Object) child2 != (UnityEngine.Object) null)
        {
          GameObject gameObject2 = child2.gameObject;
          gameObject2.transform.parent = this.m_TheKeyMesh.transform;
          gameObject2.transform.localPosition = Vector3.zero;
          gameObject2.transform.localRotation = Quaternion.identity;
          this.m_RewardPlaymaker.FsmVariables.GetFsmGameObject("FX_Motes_glow").Value = gameObject2;
        }
        Transform child3 = gameObject1.transform.FindChild("FX_Motes_trail");
        if ((UnityEngine.Object) child3 != (UnityEngine.Object) null)
        {
          GameObject gameObject2 = child3.gameObject;
          gameObject2.transform.parent = this.m_TheKeyMesh.transform;
          gameObject2.transform.localPosition = Vector3.zero;
          gameObject2.transform.localRotation = Quaternion.identity;
          this.m_RewardPlaymaker.FsmVariables.GetFsmGameObject("FX_Motes_trail").Value = gameObject2;
        }
      }
      if ((UnityEngine.Object) this.m_TheKeyGlowPlane != (UnityEngine.Object) null && (UnityEngine.Object) arenaKeyVisualData.m_EffectGlowTexture != (UnityEngine.Object) null)
        this.m_TheKeyGlowPlane.GetComponent<Renderer>().material.mainTexture = arenaKeyVisualData.m_EffectGlowTexture;
      if ((UnityEngine.Object) arenaKeyVisualData.m_KeyHoleGlowMesh != (UnityEngine.Object) null)
      {
        MeshFilter component = this.m_TheKeyGlowHoleMesh.GetComponent<MeshFilter>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          component.mesh = UnityEngine.Object.Instantiate<Mesh>(arenaKeyVisualData.m_KeyHoleGlowMesh);
      }
      if ((UnityEngine.Object) this.m_TheKeySelectionGlow != (UnityEngine.Object) null && (UnityEngine.Object) arenaKeyVisualData.m_SelectionGlowTexture != (UnityEngine.Object) null)
        this.m_TheKeySelectionGlow.GetComponent<Renderer>().material.mainTexture = arenaKeyVisualData.m_SelectionGlowTexture;
      SceneUtils.SetLayer(this.m_TheKeyMesh.transform.parent.gameObject, GameLayer.Default);
    }
  }

  private void OnIdleEffectsLoaded(string name, GameObject go, object callbackData)
  {
    this.m_isTheKeyIdleEffectsLoading = false;
    if ((bool) ((UnityEngine.Object) this.m_TheKeyIdleEffects))
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_TheKeyIdleEffects);
    this.m_TheKeyIdleEffects = go;
    go.SetActive(true);
    go.transform.parent = this.m_TheKeyMesh.transform;
    go.transform.localPosition = Vector3.zero;
  }

  [DebuggerHidden]
  private IEnumerator AnimateKeyTransition(int rank)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ArenaTrayDisplay.\u003CAnimateKeyTransition\u003Ec__Iterator80()
    {
      rank = rank,
      \u003C\u0024\u003Erank = rank,
      \u003C\u003Ef__this = this
    };
  }

  private void UpdateXBoxes()
  {
    if (!DemoMgr.Get().ArenaIs1WinMode())
      return;
    this.m_XmarkBox[0].SetActive(true);
    this.m_XmarkBox[1].SetActive(false);
    this.m_XmarkBox[2].SetActive(false);
  }

  private void OpenRewardBox(UIEvent e)
  {
    this.OpenRewardBox();
  }

  private void OpenRewardBox()
  {
    if ((UnityEngine.Object) this.m_RewardPlaymaker == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: Missing Playmaker FSM!");
    }
    else
    {
      if ((UnityEngine.Object) this.m_XmarksRoot != (UnityEngine.Object) null)
        this.m_XmarksRoot.SetActive(false);
      if ((UnityEngine.Object) this.m_TheKeySelectionGlow != (UnityEngine.Object) null)
        this.m_TheKeySelectionGlow.SetActive(false);
      this.m_WinsUberText.gameObject.SetActive(false);
      this.m_LossesUberText.gameObject.SetActive(false);
      SceneUtils.EnableColliders(this.m_TheKeyMesh, false);
      SceneUtils.SetLayer(this.m_TheKeyMesh.transform.parent.gameObject, GameLayer.Default);
      if ((bool) ((UnityEngine.Object) this.m_TheKeyIdleEffects))
      {
        PlayMakerFSM componentInChildren = this.m_TheKeyIdleEffects.GetComponentInChildren<PlayMakerFSM>();
        if ((bool) ((UnityEngine.Object) componentInChildren))
          componentInChildren.SendEvent("Death");
      }
      if ((UnityEngine.Object) this.m_BehindTheDoors == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) "ArenaTrayDisplay: m_BehindTheDoors is null!");
      }
      else
      {
        this.m_BehindTheDoors.SetActive(true);
        this.m_RewardPlaymaker.SendEvent("Birth");
      }
    }
  }

  private void OnRewardBoxesDone()
  {
    if ((UnityEngine.Object) this == (UnityEngine.Object) null || (UnityEngine.Object) this.gameObject == (UnityEngine.Object) null)
      return;
    DraftManager draftManager = DraftManager.Get();
    if (draftManager.GetDraftDeck() == null)
      Log.Rachelle.Print("bug 8052, null exception");
    else
      Network.AckDraftRewards(draftManager.GetDraftDeck().ID, draftManager.GetSlot());
    DraftDisplay.Get().OnOpenRewardsComplete();
  }

  private void ShowPlainPaper()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_Paper.SetActive(true);
      this.m_Paper.GetComponent<Renderer>().sharedMaterial = this.m_PlainPaperMaterial;
    }
    else
    {
      this.m_Paper.SetActive(false);
      this.m_PaperMain.SetActive(true);
    }
    this.m_XmarksRoot.SetActive(false);
    this.m_WinsUberText.Hide();
    this.m_LossesUberText.Hide();
  }

  private void ShowRewardPaper()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_Paper.SetActive(true);
      this.m_Paper.GetComponent<Renderer>().sharedMaterial = this.m_RewardPaperMaterial;
    }
    else
    {
      this.m_Paper.SetActive(true);
      this.m_PaperMain.SetActive(false);
    }
    this.m_XmarksRoot.SetActive(true);
    this.m_WinsUberText.Show();
    this.m_LossesUberText.Show();
  }

  private void HidePaper()
  {
    this.m_Paper.SetActive(false);
  }

  [Serializable]
  public class ArenaKeyVisualData
  {
    public Mesh m_Mesh;
    public Material m_Material;
    public Mesh m_KeyHoleGlowMesh;
    public Texture m_EffectGlowTexture;
    public Texture m_SelectionGlowTexture;
    public GameObject m_ParticlePrefab;
    public string m_IdleEffectsPrefabName;
  }
}
