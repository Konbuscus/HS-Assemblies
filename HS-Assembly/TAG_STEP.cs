﻿// Decompiled with JetBrains decompiler
// Type: TAG_STEP
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum TAG_STEP
{
  INVALID,
  BEGIN_FIRST,
  BEGIN_SHUFFLE,
  BEGIN_DRAW,
  BEGIN_MULLIGAN,
  MAIN_BEGIN,
  MAIN_READY,
  MAIN_RESOURCE,
  MAIN_DRAW,
  MAIN_START,
  MAIN_ACTION,
  MAIN_COMBAT,
  MAIN_END,
  MAIN_NEXT,
  FINAL_WRAPUP,
  FINAL_GAMEOVER,
  MAIN_CLEANUP,
  MAIN_START_TRIGGERS,
}
