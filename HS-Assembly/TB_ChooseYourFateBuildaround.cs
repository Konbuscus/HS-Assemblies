﻿// Decompiled with JetBrains decompiler
// Type: TB_ChooseYourFateBuildaround
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TB_ChooseYourFateBuildaround : MissionEntity
{
  private string friendlyFate = "TB_PICKYOURFATE_BUILDAROUND_NEWFATE";
  private string opposingFate = "TB_PICKYOURFATE_BUILDAROUND_OPPONENTFATE";
  private HashSet<int> seen = new HashSet<int>();
  private Notification ChooseYourFatePopup;
  private Vector3 popUpPos;
  private string textID;

  public override void PreloadAssets()
  {
    this.PreloadSound("tutorial_mission_hero_coin_mouse_away");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB_ChooseYourFateBuildaround.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1EF() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
