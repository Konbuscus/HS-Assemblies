﻿// Decompiled with JetBrains decompiler
// Type: GameEntity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class GameEntity : Entity
{
  private Map<string, AudioSource> m_preloadedSounds = new Map<string, AudioSource>();
  private int m_preloadsNeeded;

  public GameEntity()
  {
    this.PreloadAssets();
  }

  public void FadeOutHeroActor(Actor actorToFade)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameEntity.\u003CFadeOutHeroActor\u003Ec__AnonStorey3B6 actorCAnonStorey3B6 = new GameEntity.\u003CFadeOutHeroActor\u003Ec__AnonStorey3B6();
    this.ToggleSpotLight(actorToFade.GetHeroSpotlight(), false);
    // ISSUE: reference to a compiler-generated field
    actorCAnonStorey3B6.heroMat = actorToFade.m_portraitMesh.GetComponent<Renderer>().materials[actorToFade.m_portraitMatIdx];
    // ISSUE: reference to a compiler-generated field
    actorCAnonStorey3B6.heroFrameMat = actorToFade.m_portraitMesh.GetComponent<Renderer>().materials[actorToFade.m_portraitFrameMatIdx];
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    Hashtable args = iTween.Hash((object) "time", (object) 0.25f, (object) "from", (object) actorCAnonStorey3B6.heroMat.GetFloat("_LightingBlend"), (object) "to", (object) 1f, (object) "onupdate", (object) new Action<object>(actorCAnonStorey3B6.\u003C\u003Em__13C), (object) "onupdatetarget", (object) actorToFade.gameObject);
    iTween.ValueTo(actorToFade.gameObject, args);
  }

  public void FadeOutActor(Actor actorToFade)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameEntity.\u003CFadeOutActor\u003Ec__AnonStorey3B7 actorCAnonStorey3B7 = new GameEntity.\u003CFadeOutActor\u003Ec__AnonStorey3B7();
    // ISSUE: reference to a compiler-generated field
    actorCAnonStorey3B7.mat = actorToFade.m_portraitMesh.GetComponent<Renderer>().materials[actorToFade.m_portraitMatIdx];
    // ISSUE: reference to a compiler-generated field
    actorCAnonStorey3B7.frameMat = actorToFade.m_portraitMesh.GetComponent<Renderer>().materials[actorToFade.m_portraitFrameMatIdx];
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    Hashtable args = iTween.Hash((object) "time", (object) 0.25f, (object) "from", (object) actorCAnonStorey3B7.mat.GetFloat("_LightingBlend"), (object) "to", (object) 1f, (object) "onupdate", (object) new Action<object>(actorCAnonStorey3B7.\u003C\u003Em__13D), (object) "onupdatetarget", (object) actorToFade.gameObject);
    iTween.ValueTo(actorToFade.gameObject, args);
  }

  private void ToggleSpotLight(Light light, bool bOn)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameEntity.\u003CToggleSpotLight\u003Ec__AnonStorey3B8 lightCAnonStorey3B8 = new GameEntity.\u003CToggleSpotLight\u003Ec__AnonStorey3B8();
    // ISSUE: reference to a compiler-generated field
    lightCAnonStorey3B8.light = light;
    float num1 = 0.1f;
    float num2 = 1.3f;
    // ISSUE: reference to a compiler-generated method
    Action<object> action1 = new Action<object>(lightCAnonStorey3B8.\u003C\u003Em__13E);
    // ISSUE: reference to a compiler-generated method
    Action<object> action2 = new Action<object>(lightCAnonStorey3B8.\u003C\u003Em__13F);
    if (bOn)
    {
      // ISSUE: reference to a compiler-generated field
      lightCAnonStorey3B8.light.enabled = true;
      // ISSUE: reference to a compiler-generated field
      lightCAnonStorey3B8.light.intensity = 0.0f;
      // ISSUE: reference to a compiler-generated field
      Hashtable args = iTween.Hash((object) "time", (object) num1, (object) "from", (object) 0.0f, (object) "to", (object) num2, (object) "onupdate", (object) action1, (object) "onupdatetarget", (object) lightCAnonStorey3B8.light.gameObject);
      // ISSUE: reference to a compiler-generated field
      iTween.ValueTo(lightCAnonStorey3B8.light.gameObject, args);
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      Hashtable args = iTween.Hash((object) "time", (object) num1, (object) "from", (object) lightCAnonStorey3B8.light.intensity, (object) "to", (object) 0.0f, (object) "onupdate", (object) action1, (object) "onupdatetarget", (object) lightCAnonStorey3B8.light.gameObject, (object) "oncomplete", (object) action2);
      // ISSUE: reference to a compiler-generated field
      iTween.ValueTo(lightCAnonStorey3B8.light.gameObject, args);
    }
  }

  public void FadeInHeroActor(Actor actorToFade)
  {
    this.FadeInHeroActor(actorToFade, 0.0f);
  }

  public void FadeInHeroActor(Actor actorToFade, float lightBlendAmount)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameEntity.\u003CFadeInHeroActor\u003Ec__AnonStorey3B9 actorCAnonStorey3B9 = new GameEntity.\u003CFadeInHeroActor\u003Ec__AnonStorey3B9();
    this.ToggleSpotLight(actorToFade.GetHeroSpotlight(), true);
    // ISSUE: reference to a compiler-generated field
    actorCAnonStorey3B9.heroMat = actorToFade.m_portraitMesh.GetComponent<Renderer>().materials[actorToFade.m_portraitMatIdx];
    // ISSUE: reference to a compiler-generated field
    actorCAnonStorey3B9.heroFrameMat = actorToFade.m_portraitMesh.GetComponent<Renderer>().materials[actorToFade.m_portraitFrameMatIdx];
    // ISSUE: reference to a compiler-generated field
    float num = actorCAnonStorey3B9.heroMat.GetFloat("_LightingBlend");
    // ISSUE: reference to a compiler-generated method
    Action<object> action = new Action<object>(actorCAnonStorey3B9.\u003C\u003Em__140);
    Hashtable args = iTween.Hash((object) "time", (object) 0.25f, (object) "from", (object) num, (object) "to", (object) lightBlendAmount, (object) "onupdate", (object) action, (object) "onupdatetarget", (object) actorToFade.gameObject);
    iTween.ValueTo(actorToFade.gameObject, args);
  }

  public void FadeInActor(Actor actorToFade)
  {
    this.FadeInActor(actorToFade, 0.0f);
  }

  public void FadeInActor(Actor actorToFade, float lightBlendAmount)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameEntity.\u003CFadeInActor\u003Ec__AnonStorey3BA actorCAnonStorey3Ba = new GameEntity.\u003CFadeInActor\u003Ec__AnonStorey3BA();
    // ISSUE: reference to a compiler-generated field
    actorCAnonStorey3Ba.mat = actorToFade.m_portraitMesh.GetComponent<Renderer>().materials[actorToFade.m_portraitMatIdx];
    // ISSUE: reference to a compiler-generated field
    actorCAnonStorey3Ba.frameMat = actorToFade.m_portraitMesh.GetComponent<Renderer>().materials[actorToFade.m_portraitFrameMatIdx];
    // ISSUE: reference to a compiler-generated field
    float num = actorCAnonStorey3Ba.mat.GetFloat("_LightingBlend");
    // ISSUE: reference to a compiler-generated method
    Action<object> action = new Action<object>(actorCAnonStorey3Ba.\u003C\u003Em__141);
    Hashtable args = iTween.Hash((object) "time", (object) 0.25f, (object) "from", (object) num, (object) "to", (object) lightBlendAmount, (object) "onupdate", (object) action, (object) "onupdatetarget", (object) actorToFade.gameObject);
    iTween.ValueTo(actorToFade.gameObject, args);
  }

  public void PreloadSound(string soundName)
  {
    ++this.m_preloadsNeeded;
    AssetLoader.Get().LoadSound(soundName, new AssetLoader.GameObjectCallback(this.OnSoundLoaded), (object) null, false, SoundManager.Get().GetPlaceholderSound());
  }

  private void OnSoundLoaded(string name, GameObject go, object callbackData)
  {
    --this.m_preloadsNeeded;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("GameEntity.OnSoundLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      AudioSource component = go.GetComponent<AudioSource>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        UnityEngine.Debug.LogWarning((object) string.Format("GameEntity.OnSoundLoaded() - ERROR \"{0}\" has no Spell component", (object) name));
      else
        this.m_preloadedSounds.Add(name, component);
    }
  }

  public void RemovePreloadedSound(string name)
  {
    this.m_preloadedSounds.Remove(name);
  }

  public bool CheckPreloadedSound(string name)
  {
    AudioSource audioSource;
    return this.m_preloadedSounds.TryGetValue(name, out audioSource);
  }

  public AudioSource GetPreloadedSound(string name)
  {
    AudioSource audioSource;
    if (this.m_preloadedSounds.TryGetValue(name, out audioSource))
      return audioSource;
    UnityEngine.Debug.LogError((object) string.Format("GameEntity.GetPreloadedSound() - \"{0}\" was not preloaded", (object) name));
    return (AudioSource) null;
  }

  public bool IsPreloadingAssets()
  {
    return this.m_preloadsNeeded > 0;
  }

  public override string GetName()
  {
    return "GameEntity";
  }

  public override string GetDebugName()
  {
    return "GameEntity";
  }

  public override void OnTagsChanged(TagDeltaList changeList)
  {
    for (int index = 0; index < changeList.Count; ++index)
      this.OnTagChanged(changeList[index]);
  }

  public override void OnRealTimeTagChanged(Network.HistTagChange change)
  {
    if (change.Tag != 6)
      return;
    this.HandleRealTimeMissionEvent(change.Value);
  }

  public virtual void PreloadAssets()
  {
  }

  public virtual void NotifyOfStartOfTurnEventsFinished()
  {
  }

  public virtual bool NotifyOfEndTurnButtonPushed()
  {
    return true;
  }

  public virtual bool NotifyOfBattlefieldCardClicked(Entity clickedEntity, bool wasInTargetMode)
  {
    return true;
  }

  public virtual void NotifyOfCardMousedOver(Entity mousedOverEntity)
  {
  }

  public virtual void NotifyOfCardMousedOff(Entity mousedOffEntity)
  {
  }

  public virtual bool NotifyOfCardTooltipDisplayShow(Card card)
  {
    return true;
  }

  public virtual void NotifyOfCardTooltipDisplayHide(Card card)
  {
  }

  public virtual void NotifyOfCoinFlipResult()
  {
  }

  public virtual bool NotifyOfPlayError(PlayErrors.ErrorType error, Entity errorSource)
  {
    return false;
  }

  public virtual string[] NotifyOfKeywordHelpPanelDisplay(Entity entity)
  {
    return (string[]) null;
  }

  public virtual void NotifyOfCardGrabbed(Entity entity)
  {
  }

  public virtual void NotifyOfCardDropped(Entity entity)
  {
  }

  public virtual void NotifyOfTargetModeCancelled()
  {
  }

  public virtual void NotifyOfHelpPanelDisplay(int numPanels)
  {
  }

  public virtual void NotifyOfDebugCommand(int command)
  {
  }

  public virtual void NotifyOfManaCrystalSpawned()
  {
  }

  public virtual void NotifyOfEnemyManaCrystalSpawned()
  {
  }

  public virtual void NotifyOfTooltipZoneMouseOver(TooltipZone tooltip)
  {
  }

  public virtual void NotifyOfHistoryTokenMousedOver(GameObject mousedOverTile)
  {
  }

  public virtual void NotifyOfHistoryTokenMousedOut()
  {
  }

  public virtual void NotifyOfCustomIntroFinished()
  {
  }

  public virtual void NotifyOfGameOver(TAG_PLAYSTATE playState)
  {
    PegCursor.Get().SetMode(PegCursor.Mode.STOPWAITING);
    MusicManager.Get().StartPlaylist(MusicPlaylistType.UI_EndGameScreen);
    Card heroCard1 = GameState.Get().GetOpposingSidePlayer().GetHeroCard();
    Card heroCard2 = GameState.Get().GetFriendlySidePlayer().GetHeroCard();
    Gameplay.Get().SaveOriginalTimeScale();
    Spell enemyBlowUpSpell = (Spell) null;
    Spell friendlyBlowUpSpell = (Spell) null;
    if (this.ShouldPlayHeroBlowUpSpells(playState))
    {
      switch (playState)
      {
        case TAG_PLAYSTATE.WON:
          SoundManager.Get().LoadAndPlay("victory_jingle");
          enemyBlowUpSpell = this.BlowUpHero(heroCard1, SpellType.ENDGAME_WIN);
          break;
        case TAG_PLAYSTATE.LOST:
          SoundManager.Get().LoadAndPlay("defeat_jingle");
          friendlyBlowUpSpell = this.BlowUpHero(heroCard2, SpellType.ENDGAME_LOSE);
          break;
        case TAG_PLAYSTATE.TIED:
          SoundManager.Get().LoadAndPlay("defeat_jingle");
          enemyBlowUpSpell = this.BlowUpHero(heroCard1, SpellType.ENDGAME_DRAW);
          friendlyBlowUpSpell = this.BlowUpHero(heroCard2, SpellType.ENDGAME_LOSE);
          break;
      }
    }
    this.ShowEndGameScreen(playState, enemyBlowUpSpell, friendlyBlowUpSpell);
  }

  public virtual void NotifyOfRealTimeTagChange(Entity entity, Network.HistTagChange tagChange)
  {
  }

  public virtual bool ShouldPlayHeroBlowUpSpells(TAG_PLAYSTATE playState)
  {
    return true;
  }

  public virtual string GetVictoryScreenBannerText()
  {
    return GameStrings.Get("GAMEPLAY_END_OF_GAME_VICTORY");
  }

  public virtual void NotifyOfHeroesFinishedAnimatingInMulligan()
  {
  }

  public virtual bool NotifyOfTooltipDisplay(TooltipZone tooltip)
  {
    return false;
  }

  public virtual void NotifyOfMulliganInitialized()
  {
    if (GameMgr.Get().IsTutorial())
      return;
    AssetLoader.Get().LoadActor("EmoteHandler", new AssetLoader.GameObjectCallback(this.EmoteHandlerDoneLoadingCallback), (object) null, false);
    if (GameMgr.Get().IsAI())
      return;
    AssetLoader.Get().LoadActor("EnemyEmoteHandler", new AssetLoader.GameObjectCallback(this.EnemyEmoteHandlerDoneLoadingCallback), (object) null, false);
  }

  public virtual AudioSource GetAnnouncerLine(Card heroCard, Card.AnnouncerLineType type)
  {
    return heroCard.GetAnnouncerLine(type);
  }

  public virtual void NotifyOfMulliganEnded()
  {
  }

  private void EmoteHandlerDoneLoadingCallback(string actorName, GameObject actorObject, object callbackData)
  {
    actorObject.transform.position = ZoneMgr.Get().FindZoneOfType<ZoneHero>(Player.Side.FRIENDLY).transform.position;
  }

  private void EnemyEmoteHandlerDoneLoadingCallback(string actorName, GameObject actorObject, object callbackData)
  {
    actorObject.transform.position = ZoneMgr.Get().FindZoneOfType<ZoneHero>(Player.Side.OPPOSING).transform.position;
  }

  public virtual void NotifyOfGamePackOpened()
  {
  }

  public virtual void NotifyOfDefeatCoinAnimation()
  {
  }

  public virtual void SendCustomEvent(int eventID)
  {
  }

  public virtual string GetTurnStartReminderText()
  {
    return string.Empty;
  }

  public virtual bool ShouldDoAlternateMulliganIntro()
  {
    return false;
  }

  public virtual bool DoAlternateMulliganIntro()
  {
    return false;
  }

  public virtual float GetAdditionalTimeToWaitForSpells()
  {
    return 0.0f;
  }

  public virtual bool IsKeywordHelpDelayOverridden()
  {
    return false;
  }

  public virtual bool IsMouseOverDelayOverriden()
  {
    return false;
  }

  public virtual bool AreTooltipsDisabled()
  {
    return false;
  }

  public virtual bool ShouldShowBigCard()
  {
    return true;
  }

  public virtual bool ShouldShowCrazyKeywordTooltip()
  {
    return false;
  }

  public virtual bool ShouldShowHeroTooltips()
  {
    return false;
  }

  public virtual bool ShouldDoOpeningTaunts()
  {
    return true;
  }

  public virtual bool ShouldHandleCoin()
  {
    return true;
  }

  public virtual List<RewardData> GetCustomRewards()
  {
    return (List<RewardData>) null;
  }

  public virtual void HandleRealTimeMissionEvent(int missionEvent)
  {
  }

  public virtual void OnPlayThinkEmote()
  {
    if (GameMgr.Get().IsAI())
      return;
    EmoteType emoteType = EmoteType.THINK1;
    switch (UnityEngine.Random.Range(1, 4))
    {
      case 1:
        emoteType = EmoteType.THINK1;
        break;
      case 2:
        emoteType = EmoteType.THINK2;
        break;
      case 3:
        emoteType = EmoteType.THINK3;
        break;
    }
    GameState.Get().GetCurrentPlayer().GetHeroCard().PlayEmote(emoteType);
  }

  public virtual void OnEmotePlayed(Card card, EmoteType emoteType, CardSoundSpell emoteSpell)
  {
  }

  public virtual void NotifyOfOpponentWillPlayCard(string cardId)
  {
  }

  public virtual void NotifyOfOpponentPlayedCard(Entity entity)
  {
  }

  public virtual void NotifyOfFriendlyPlayedCard(Entity entity)
  {
  }

  public virtual string UpdateCardText(Card card, Actor bigCardActor, string text)
  {
    return text;
  }

  public virtual bool ShouldUseSecretClassNames()
  {
    return false;
  }

  public virtual void StartGameplaySoundtracks()
  {
    Board board = Board.Get();
    MusicManager.Get().StartPlaylist(!((UnityEngine.Object) board == (UnityEngine.Object) null) ? board.m_BoardMusic : MusicPlaylistType.InGame_Default);
  }

  public virtual string GetAlternatePlayerName()
  {
    return string.Empty;
  }

  [DebuggerHidden]
  public virtual IEnumerator PlayMissionIntroLineAndWait()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameEntity.\u003CPlayMissionIntroLineAndWait\u003Ec__IteratorAE andWaitCIteratorAe = new GameEntity.\u003CPlayMissionIntroLineAndWait\u003Ec__IteratorAE();
    return (IEnumerator) andWaitCIteratorAe;
  }

  [DebuggerHidden]
  public virtual IEnumerator DoActionsAfterIntroBeforeMulligan()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameEntity.\u003CDoActionsAfterIntroBeforeMulligan\u003Ec__IteratorAF mulliganCIteratorAf = new GameEntity.\u003CDoActionsAfterIntroBeforeMulligan\u003Ec__IteratorAF();
    return (IEnumerator) mulliganCIteratorAf;
  }

  [DebuggerHidden]
  public virtual IEnumerator DoCustomIntro(Card friendlyHero, Card enemyHero, HeroLabel friendlyHeroLabel, HeroLabel enemyHeroLabel, GameStartVsLetters versusText)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameEntity.\u003CDoCustomIntro\u003Ec__IteratorB0 introCIteratorB0 = new GameEntity.\u003CDoCustomIntro\u003Ec__IteratorB0();
    return (IEnumerator) introCIteratorB0;
  }

  public virtual void OnCustomIntroCancelled(Card friendlyHero, Card enemyHero, HeroLabel friendlyHeroLabel, HeroLabel enemyHeroLabel, GameStartVsLetters versusText)
  {
  }

  public virtual bool IsEnemySpeaking()
  {
    return false;
  }

  public virtual bool ShouldDelayCardSoundSpells()
  {
    return false;
  }

  public virtual bool ShouldAllowCardGrab(Entity entity)
  {
    return true;
  }

  public virtual string CustomChoiceBannerText()
  {
    return (string) null;
  }

  protected virtual Spell BlowUpHero(Card card, SpellType spellType)
  {
    Spell spell = card.ActivateActorSpell(spellType);
    Gameplay.Get().StartCoroutine(this.HideOtherElements(card));
    return spell;
  }

  [DebuggerHidden]
  protected IEnumerator HideOtherElements(Card card)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GameEntity.\u003CHideOtherElements\u003Ec__IteratorB1() { card = card, \u003C\u0024\u003Ecard = card };
  }

  protected void ShowEndGameScreen(TAG_PLAYSTATE playState, Spell enemyBlowUpSpell, Spell friendlyBlowUpSpell)
  {
    string name = (string) null;
    switch (playState)
    {
      case TAG_PLAYSTATE.WON:
        name = "VictoryTwoScoop";
        break;
      case TAG_PLAYSTATE.LOST:
      case TAG_PLAYSTATE.TIED:
        name = "DefeatTwoScoop";
        break;
    }
    GameObject gameObject = AssetLoader.Get().LoadActor(name, false, false);
    if (!(bool) ((UnityEngine.Object) gameObject))
    {
      UnityEngine.Debug.LogErrorFormat("GameEntity.ShowEndGameScreen() - FAILED to load \"{0}\"", (object) name);
    }
    else
    {
      EndGameScreen component = gameObject.GetComponent<EndGameScreen>();
      if (!(bool) ((UnityEngine.Object) component))
      {
        UnityEngine.Debug.LogErrorFormat("GameEntity.ShowEndGameScreen() - \"{0}\" does not have an EndGameScreen component", (object) name);
      }
      else
      {
        GameEntity.EndGameScreenContext context = new GameEntity.EndGameScreenContext();
        context.m_screen = component;
        context.m_enemyBlowUpSpell = enemyBlowUpSpell;
        context.m_friendlyBlowUpSpell = friendlyBlowUpSpell;
        if ((bool) ((UnityEngine.Object) enemyBlowUpSpell) && !enemyBlowUpSpell.IsFinished())
          enemyBlowUpSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnBlowUpSpellFinished), (object) context);
        if ((bool) ((UnityEngine.Object) friendlyBlowUpSpell) && !friendlyBlowUpSpell.IsFinished())
          friendlyBlowUpSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnBlowUpSpellFinished), (object) context);
        this.ShowEndGameScreenAfterEffects(context);
      }
    }
  }

  private void OnBlowUpSpellFinished(Spell spell, object userData)
  {
    this.ShowEndGameScreenAfterEffects((GameEntity.EndGameScreenContext) userData);
  }

  private void ShowEndGameScreenAfterEffects(GameEntity.EndGameScreenContext context)
  {
    if (!this.AreBlowUpSpellsFinished(context))
      return;
    Gameplay.Get().RestoreOriginalTimeScale();
    context.m_screen.Show();
  }

  private bool AreBlowUpSpellsFinished(GameEntity.EndGameScreenContext context)
  {
    return (!((UnityEngine.Object) context.m_enemyBlowUpSpell != (UnityEngine.Object) null) || context.m_enemyBlowUpSpell.IsFinished()) && (!((UnityEngine.Object) context.m_friendlyBlowUpSpell != (UnityEngine.Object) null) || context.m_friendlyBlowUpSpell.IsFinished());
  }

  protected class EndGameScreenContext
  {
    public EndGameScreen m_screen;
    public Spell m_enemyBlowUpSpell;
    public Spell m_friendlyBlowUpSpell;
  }
}
