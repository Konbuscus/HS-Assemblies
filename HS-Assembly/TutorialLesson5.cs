﻿// Decompiled with JetBrains decompiler
// Type: TutorialLesson5
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TutorialLesson5 : MonoBehaviour
{
  public UberText m_heroPower;
  public UberText m_used;
  public UberText m_yourTurn;

  private void Awake()
  {
    this.m_heroPower.SetGameStringText("GLOBAL_TUTORIAL_HERO_POWER");
    this.m_used.SetGameStringText("GLOBAL_TUTORIAL_USED");
    this.m_yourTurn.SetGameStringText("GLOBAL_TUTORIAL_YOUR_TURN");
  }
}
