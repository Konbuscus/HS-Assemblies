﻿// Decompiled with JetBrains decompiler
// Type: CardSpecificMultiVoSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class CardSpecificMultiVoSpell : CardSoundSpell
{
  public CardSpecificMultiVoData m_CardSpecificVoData = new CardSpecificMultiVoData();
  private int m_ActiveAudioIndex;
  private bool m_SpecificCardFound;

  protected override void Play()
  {
    if (!this.m_forceDefaultAudioSource)
      this.m_SpecificCardFound = this.SearchForCard();
    if (this.m_SpecificCardFound)
    {
      this.Stop();
      this.m_ActiveAudioIndex = 0;
      this.m_activeAudioSource = !this.m_forceDefaultAudioSource ? this.DetermineBestAudioSource() : this.m_CardSoundData.m_AudioSource;
      if ((Object) this.m_activeAudioSource == (Object) null)
        this.OnStateFinished();
      else
        this.StartCoroutine("DelayedPlayMulti");
    }
    else
      base.Play();
  }

  protected virtual void PlayNowMulti()
  {
    SoundManager.Get().Play(this.m_activeAudioSource, true);
    this.StartCoroutine("WaitForSourceThenContinue");
  }

  protected override void Stop()
  {
    this.StopCoroutine("WaitForSourceThenContinue");
    base.Stop();
  }

  public override AudioSource DetermineBestAudioSource()
  {
    if (!this.m_SpecificCardFound)
      return base.DetermineBestAudioSource();
    if (this.m_ActiveAudioIndex < this.m_CardSpecificVoData.m_Lines.Length)
      return this.m_CardSpecificVoData.m_Lines[this.m_ActiveAudioIndex].m_AudioSource;
    return (AudioSource) null;
  }

  private bool SearchForCard()
  {
    if (string.IsNullOrEmpty(this.m_CardSpecificVoData.m_CardId))
      return false;
    using (List<SpellZoneTag>.Enumerator enumerator = this.m_CardSpecificVoData.m_ZonesToSearch.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (this.IsCardInZones(SpellUtils.FindZonesFromTag((Spell) this, enumerator.Current, this.m_CardSpecificVoData.m_SideToSearch)))
          return true;
      }
    }
    return false;
  }

  private bool IsCardInZones(List<Zone> zones)
  {
    if (zones == null)
      return false;
    using (List<Zone>.Enumerator enumerator1 = zones.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Card>.Enumerator enumerator2 = enumerator1.Current.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            if (enumerator2.Current.GetEntity().GetCardId() == this.m_CardSpecificVoData.m_CardId)
              return true;
          }
        }
      }
    }
    return false;
  }

  [DebuggerHidden]
  protected IEnumerator DelayedPlayMulti()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardSpecificMultiVoSpell.\u003CDelayedPlayMulti\u003Ec__Iterator29B() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator WaitForSourceThenContinue()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardSpecificMultiVoSpell.\u003CWaitForSourceThenContinue\u003Ec__Iterator29C() { \u003C\u003Ef__this = this };
  }
}
