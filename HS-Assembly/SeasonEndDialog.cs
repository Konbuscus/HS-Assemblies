﻿// Decompiled with JetBrains decompiler
// Type: SeasonEndDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class SeasonEndDialog : DialogBase
{
  private static string[] s_percentiles = new string[18]{ "0.25", "0.33", "0.5", "1", "2", "3", "4", "5", "7", "9", "12", "15", "20", "25", "30", "40", "45", "50" };
  public UIBButton m_okayButton;
  public Vector3 m_loadPosition;
  public Vector3 m_showPosition;
  public GameObject m_boostedMedalBone;
  public GameObject m_boostedMedalLeftFiligreeBone;
  public GameObject m_boostedMedalRightFiligreeBone;
  public GameObject m_rewardChestPage;
  public PegUIElement m_rewardChest;
  public UberText m_rewardChestHeader;
  public UberText m_rewardChestInstructions;
  public GameObject m_rewardChestLeftFiligreeBone;
  public GameObject m_rewardChestRightFiligreeBone;
  public GameObject m_rewardBoxesBone;
  public NestedPrefab m_medalContainer;
  public UberText m_header;
  public UberText m_rankAchieved;
  public UberText m_rankName;
  public UberText m_rankPercentile;
  public GameObject m_ribbon;
  public GameObject m_nameFlourish;
  public GameObject m_welcomeItems;
  public GameObject m_leftFiligree;
  public GameObject m_rightFiligree;
  public UberText m_welcomeDetails;
  public UberText m_welcomeTitle;
  public GameObject m_shieldIcon;
  public GameObject m_bonusStarItems;
  public UberText m_bonusStarTitle;
  public UberText m_bonusStarLabel;
  public GameObject m_bonusStar;
  public UberText m_bonusStarText;
  public GameObject m_bonusStarFlourish;
  public Material m_transparentMaterial;
  public PlayMakerFSM m_medalPlayMaker;
  public PlayMakerFSM m_starPlayMaker;
  public GameObject m_legendaryGem;
  public List<PegUIElement> m_rewardChests;
  private SeasonEndDialog.SeasonEndInfo m_seasonEndInfo;
  private bool m_earnedRewardChest;
  private SeasonEndDialog.MODE m_currentMode;
  private bool TESTING;
  private int m_bonusStars;
  private bool m_chestOpened;
  private TournamentMedal m_medal;

  protected override void Awake()
  {
    base.Awake();
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    if (UniversalInputManager.Get().IsTouchMode())
      this.m_rewardChestInstructions.Text = GameStrings.Format("GLOBAL_SEASON_END_CHEST_INSTRUCTIONS_TOUCH");
    if (this.TESTING)
      return;
    this.m_okayButton.SetText(GameStrings.Get("GLOBAL_BUTTON_NEXT"));
    this.m_okayButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OkayButtonReleased));
  }

  private void Start()
  {
    this.m_medal = this.m_medalContainer.PrefabGameObject(false).GetComponent<TournamentMedal>();
    SceneUtils.SetLayer((Component) this.m_medal, GameLayer.PerspectiveUI);
    this.m_medal.SetEnabled(false);
  }

  public void Init(SeasonEndDialog.SeasonEndInfo info)
  {
    this.m_seasonEndInfo = info;
    this.m_header.Text = this.GetSeasonName(info.m_seasonID);
    this.m_earnedRewardChest = info.m_rankedRewards != null && info.m_rankedRewards.Count > 0;
    this.m_medal.SetMedal(new MedalInfoTranslator(info.m_rank, info.m_legendIndex, info.m_rank, info.m_legendIndex), false);
    this.m_medal.SetFormat(info.m_isWild);
    this.m_rankName.Text = this.m_medal.GetMedal().name;
    this.m_bonusStars = info.m_bonusStars;
    string rankPercentile = SeasonEndDialog.GetRankPercentile(this.m_seasonEndInfo.m_rank);
    if (rankPercentile.Length > 0)
    {
      this.m_rankPercentile.gameObject.SetActive(true);
      this.m_rankPercentile.Text = GameStrings.Format("GLOBAL_SEASON_END_PERCENTILE_LABEL", (object) rankPercentile);
    }
    else
      this.m_rankPercentile.gameObject.SetActive(false);
    using (List<PegUIElement>.Enumerator enumerator = this.m_rewardChests.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.gameObject.SetActive(false);
    }
    int chestIndexFromRank = RankedRewardChest.GetChestIndexFromRank(this.m_seasonEndInfo.m_chestRank);
    if (chestIndexFromRank >= 0)
    {
      this.m_rewardChest = this.m_rewardChests[chestIndexFromRank];
      this.m_rewardChest.gameObject.SetActive(true);
      this.m_medalPlayMaker.FsmVariables.GetFsmGameObject("RankChest").Value = this.m_rewardChest.gameObject;
      UberText[] componentsInChildren = this.m_rewardChest.GetComponentsInChildren<UberText>(true);
      if (componentsInChildren.Length > 0)
        componentsInChildren[0].Text = info.m_chestRank.ToString();
      this.m_rewardChestHeader.Text = RankedRewardChest.GetChestEarnedFromRank(this.m_seasonEndInfo.m_chestRank);
    }
    this.m_rewardChest.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ChestButtonReleased));
  }

  public void OnDestroy()
  {
    if (!(bool) ((UnityEngine.Object) SceneMgr.Get()))
      return;
    SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
  }

  public void ShowMedal()
  {
    this.m_medal.gameObject.SetActive(true);
  }

  public void HideMedal()
  {
    this.m_medal.gameObject.SetActive(false);
  }

  public void ShowRewardChest()
  {
    if (this.m_seasonEndInfo.m_rank == 0)
      this.m_legendaryGem.SetActive(true);
    this.m_rewardChestPage.SetActive(true);
    this.m_leftFiligree.transform.position = this.m_rewardChestLeftFiligreeBone.transform.position;
    this.m_rightFiligree.transform.position = this.m_rewardChestRightFiligreeBone.transform.position;
    iTween.FadeTo(this.m_leftFiligree.gameObject, 1f, 0.5f);
    iTween.FadeTo(this.m_rightFiligree.gameObject, 1f, 0.5f);
  }

  public void HideRewardChest()
  {
    this.m_rewardChestPage.SetActive(false);
  }

  public void HideBonusStarText()
  {
    this.m_bonusStarText.gameObject.SetActive(false);
  }

  public void MedalAnimationFinished()
  {
    if (this.m_earnedRewardChest)
    {
      this.m_currentMode = SeasonEndDialog.MODE.CHEST_EARNED;
      this.m_medalPlayMaker.SendEvent("RevealRewardChest");
      iTween.FadeTo(this.m_rankAchieved.gameObject, 0.0f, 0.5f);
    }
    else
    {
      this.GotoBonusStarsOrWelcome();
      this.m_okayButton.SetEnabled(true);
    }
  }

  public void GotoBonusStarsOrWelcome()
  {
    if (this.m_bonusStars > 0)
      this.GotoBonusStars();
    else
      this.GotoSeasonWelcome();
  }

  public void GotoBonusStars()
  {
    this.m_currentMode = SeasonEndDialog.MODE.BONUS_STARS;
    this.m_medalPlayMaker.SendEvent("PageTear");
    this.m_rewardChestPage.SetActive(false);
    this.m_welcomeItems.SetActive(false);
    this.m_bonusStarItems.SetActive(true);
    this.m_bonusStarText.Text = this.m_bonusStars.ToString();
    this.m_bonusStarLabel.Text = GameStrings.FormatPlurals("GLOBAL_SEASON_END_BONUS_STARS_LABEL", new GameStrings.PluralNumber[1]
    {
      new GameStrings.PluralNumber()
      {
        m_index = 0,
        m_number = this.m_bonusStars
      }
    });
    this.m_bonusStarTitle.Text = GameStrings.Get("GLOBAL_SEASON_END_BONUS_STAR_TITLE");
  }

  public void GotoBoostedMedal()
  {
    this.m_currentMode = SeasonEndDialog.MODE.BOOSTED_WELCOME;
    this.m_starPlayMaker.SendEvent("Burst Big");
    this.m_medal.transform.position = this.m_boostedMedalBone.transform.position;
    this.m_medal.SetMedal(new MedalInfoTranslator(this.m_seasonEndInfo.m_boostedRank, 0, this.m_seasonEndInfo.m_boostedRank, 0), false);
    this.m_leftFiligree.transform.position = this.m_boostedMedalLeftFiligreeBone.transform.position;
    this.m_rightFiligree.transform.position = this.m_boostedMedalRightFiligreeBone.transform.position;
  }

  public void StarBurstFinished()
  {
    if (this.m_medal.GetMedal().rank == 0)
      this.m_medalPlayMaker.SendEvent("JustMedalIn");
    else
      this.m_medalPlayMaker.SendEvent("MedalBannerIn");
    this.m_bonusStarText.gameObject.SetActive(false);
    this.m_bonusStarLabel.Text = this.m_medal.GetMedal().name;
    this.m_bonusStarTitle.Text = GameStrings.Format("GLOBAL_SEASON_END_NEW_SEASON", (object) this.GetInlineSeasonName(NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>().Season));
  }

  public void GotoSeasonWelcome()
  {
    this.m_currentMode = SeasonEndDialog.MODE.SEASON_WELCOME;
    this.m_medalPlayMaker.SendEvent("PageTear");
    this.m_welcomeItems.SetActive(true);
    this.m_bonusStarItems.SetActive(false);
    string seasonName = this.GetSeasonName(NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>().Season);
    this.m_header.Text = seasonName;
    this.m_welcomeDetails.Text = GameStrings.Format("GLOBAL_SEASON_END_NEW_SEASON", (object) seasonName);
  }

  public void PageTearFinished()
  {
    if (this.m_currentMode == SeasonEndDialog.MODE.SEASON_WELCOME)
      this.m_okayButton.SetText("GLOBAL_DONE");
    this.m_okayButton.SetEnabled(true);
  }

  public void MedalInFinished()
  {
    this.m_okayButton.SetText("GLOBAL_DONE");
    this.m_okayButton.SetEnabled(true);
  }

  public override void Show()
  {
    this.FadeEffectsIn();
    base.Show();
    this.DoShowAnimation();
    UniversalInputManager.Get().SetGameDialogActive(true);
    SoundManager.Get().LoadAndPlay("rank_window_expand");
  }

  public override void Hide()
  {
    base.Hide();
    this.FadeEffectsOut();
    SoundManager.Get().LoadAndPlay("rank_window_shrink");
  }

  protected override void OnHideAnimFinished()
  {
    UniversalInputManager.Get().SetGameDialogActive(false);
    base.OnHideAnimFinished();
  }

  private void OkayButtonReleased(UIEvent e)
  {
    if (this.m_currentMode == SeasonEndDialog.MODE.SEASON_WELCOME || this.m_currentMode == SeasonEndDialog.MODE.BOOSTED_WELCOME)
    {
      this.Hide();
      using (List<long>.Enumerator enumerator = this.m_seasonEndInfo.m_noticesToAck.GetEnumerator())
      {
        while (enumerator.MoveNext())
          Network.AckNotice(enumerator.Current);
      }
      this.m_okayButton.SetEnabled(false);
    }
    else if (this.m_currentMode == SeasonEndDialog.MODE.RANK_EARNED)
    {
      this.m_ribbon.GetComponent<Renderer>().material = this.m_transparentMaterial;
      this.m_nameFlourish.GetComponent<Renderer>().material = this.m_transparentMaterial;
      iTween.FadeTo(this.m_nameFlourish.gameObject, 0.0f, 0.5f);
      iTween.FadeTo(this.m_rankName.gameObject, 0.0f, 0.5f);
      iTween.FadeTo(this.m_rankAchieved.gameObject, 0.0f, 0.5f);
      iTween.FadeTo(this.m_leftFiligree.gameObject, 0.0f, 0.5f);
      iTween.FadeTo(this.m_rightFiligree.gameObject, 0.0f, 0.5f);
      if (this.m_medal.GetMedal().rank == 0)
        this.m_medalPlayMaker.SendEvent("JustMedal");
      else
        this.m_medalPlayMaker.SendEvent("MedalBanner");
      this.m_okayButton.SetEnabled(false);
      this.m_rankPercentile.gameObject.SetActive(false);
    }
    else
    {
      if (this.m_currentMode != SeasonEndDialog.MODE.BONUS_STARS)
        return;
      this.GotoBoostedMedal();
    }
  }

  private void ChestButtonReleased(UIEvent e)
  {
    if (this.m_chestOpened)
      return;
    this.m_chestOpened = true;
    this.m_rewardChest.GetComponent<PlayMakerFSM>().SendEvent("StartAnim");
  }

  private void OpenRewards()
  {
    AssetLoader.Get().LoadGameObject("RewardBoxes", (AssetLoader.GameObjectCallback) ((name, go, callbackData) =>
    {
      if ((UnityEngine.Object) SoundManager.Get() != (UnityEngine.Object) null)
        SoundManager.Get().LoadAndPlay("card_turn_over_legendary");
      RewardBoxesDisplay component = go.GetComponent<RewardBoxesDisplay>();
      component.SetRewards(this.m_seasonEndInfo.m_rankedRewards);
      component.m_playBoxFlyoutSound = false;
      component.SetLayer(GameLayer.PerspectiveUI);
      component.UseDarkeningClickCatcher(true);
      component.RegisterDoneCallback((Action) (() => this.m_rewardChest.GetComponent<PlayMakerFSM>().SendEvent("SummonOut")));
      component.transform.localPosition = this.m_rewardBoxesBone.transform.localPosition;
      component.transform.localRotation = this.m_rewardBoxesBone.transform.localRotation;
      component.transform.localScale = this.m_rewardBoxesBone.transform.localScale;
      component.AnimateRewards();
    }), (object) null, false);
    iTween.FadeTo(this.m_rewardChestInstructions.gameObject, 0.0f, 0.5f);
  }

  private void FadeEffectsIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.SetBlurBrightness(1f);
    fullScreenFxMgr.SetBlurDesaturation(0.0f);
    fullScreenFxMgr.Vignette(0.4f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.Blur(1f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void FadeEffectsOut()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private string GetSeasonName(int seasonID)
  {
    SeasonDbfRecord record = GameDbf.Season.GetRecord(seasonID);
    if (record != null)
      return (string) record.Name;
    Debug.LogError((object) string.Format("SeasonEndDialog.GetSeasonName() - There is no Season DBF record for ID {0}", (object) seasonID));
    return "NO RECORD FOUND";
  }

  private string GetInlineSeasonName(int seasonID)
  {
    SeasonDbfRecord record = GameDbf.Season.GetRecord(seasonID);
    if (record != null)
      return (string) record.SeasonStartName;
    Debug.LogError((object) string.Format("SeasonEndDialog.GetInlineSeasonName() - There is no Season DBF record for ID {0}", (object) seasonID));
    return "NO RECORD FOUND";
  }

  public static string GetRankPercentile(int rank)
  {
    int index = rank - 1;
    if (index < 0)
      index = 0;
    if (index >= SeasonEndDialog.s_percentiles.Length)
      return string.Empty;
    if (Localization.DoesLocaleUseDecimalPoint(Localization.GetLocale()))
      return SeasonEndDialog.s_percentiles[index];
    return SeasonEndDialog.s_percentiles[index].Replace(".", ",");
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (mode == SceneMgr.Mode.HUB)
      return;
    this.Hide();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  public class SeasonEndInfo
  {
    public List<long> m_noticesToAck = new List<long>();
    public int m_seasonID;
    public int m_rank;
    public int m_chestRank;
    public int m_legendIndex;
    public int m_bonusStars;
    public int m_boostedRank;
    public List<RewardData> m_rankedRewards;
    public bool m_isWild;
    public bool m_isFake;
  }

  private enum MODE
  {
    RANK_EARNED,
    CHEST_EARNED,
    SEASON_WELCOME,
    BONUS_STARS,
    BOOSTED_WELCOME,
  }
}
