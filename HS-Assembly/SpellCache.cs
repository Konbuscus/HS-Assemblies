﻿// Decompiled with JetBrains decompiler
// Type: SpellCache
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpellCache : MonoBehaviour
{
  private Map<string, SpellTable> m_spellTableCache = new Map<string, SpellTable>();
  private static SpellCache s_instance;

  private void Awake()
  {
    SpellCache.s_instance = this;
  }

  private void Start()
  {
    if (!(bool) ((Object) SceneMgr.Get()))
      return;
    SceneMgr.Get().RegisterScenePreLoadEvent(new SceneMgr.ScenePreLoadCallback(this.OnScenePreLoad));
  }

  private void OnDestroy()
  {
    SpellCache.s_instance = (SpellCache) null;
  }

  public static SpellCache Get()
  {
    if (!((Object) SpellCache.s_instance == (Object) null) || Application.isEditor)
      return SpellCache.s_instance;
    Debug.LogError((object) "Attempting to access null SpellCache");
    return (SpellCache) null;
  }

  public SpellTable GetSpellTable(string tablePath)
  {
    string name = FileUtils.GameAssetPathToName(tablePath);
    SpellTable spellTable;
    if (!this.m_spellTableCache.TryGetValue(name, out spellTable))
      spellTable = this.LoadSpellTable(name);
    return spellTable;
  }

  public void Clear()
  {
    using (Map<string, SpellTable>.Enumerator enumerator = this.m_spellTableCache.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.ReleaseAllSpells();
    }
  }

  private SpellTable LoadSpellTable(string tableName)
  {
    GameObject gameObject = AssetLoader.Get().LoadActor(tableName, false, false);
    if ((Object) gameObject == (Object) null)
    {
      Error.AddDevFatal("SpellCache.LoadSpellTable() - {0} failed to load", (object) this.name);
      return (SpellTable) null;
    }
    SpellTable component = gameObject.GetComponent<SpellTable>();
    if ((Object) component == (Object) null)
    {
      Error.AddDevFatal("SpellCache.LoadSpellTable() - {0} has no SpellTable component", (object) this.name);
      return (SpellTable) null;
    }
    component.transform.parent = this.transform;
    this.m_spellTableCache.Add(tableName, component);
    return component;
  }

  private void OnScenePreLoad(SceneMgr.Mode prevMode, SceneMgr.Mode mode, object userData)
  {
    switch (mode)
    {
      case SceneMgr.Mode.GAMEPLAY:
        this.PreloadSpell("Card_Hand_Ability_SpellTable", SpellType.SPELL_POWER_HINT_IDLE);
        this.PreloadSpell("Card_Hand_Ability_SpellTable", SpellType.SPELL_POWER_HINT_BURST);
        this.PreloadSpell("Card_Hand_Ability_SpellTable", SpellType.POWER_UP);
        this.PreloadSpell("Card_Hand_Ally_SpellTable", SpellType.SUMMON_OUT_MEDIUM);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.OPPONENT_ATTACK);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.STEALTH);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.DAMAGE);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.DEATH);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.SUMMON_OUT);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.FROZEN);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.FRIENDLY_ATTACK);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.SUMMON_IN_MEDIUM);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.SUMMON_IN);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.SUMMON_IN_OPPONENT);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.BATTLECRY);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.ENCHANT_POSITIVE);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.ENCHANT_NEGATIVE);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.ENCHANT_NEUTRAL);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.TAUNT_STEALTH);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.TRIGGER);
        this.PreloadSpell("Card_Play_Ally_SpellTable", SpellType.Zzz);
        this.PreloadSpell("Card_Hidden_SpellTable", SpellType.SUMMON_OUT);
        this.PreloadSpell("Card_Hidden_SpellTable", SpellType.SUMMON_IN);
        this.PreloadSpell("Card_Hidden_SpellTable", SpellType.SUMMON_OUT_WEAPON);
        this.PreloadSpell("Card_Play_Hero_SpellTable", SpellType.ENDGAME_WIN);
        this.PreloadSpell("Card_Play_Hero_SpellTable", SpellType.OPPONENT_ATTACK);
        this.PreloadSpell("Card_Play_Hero_SpellTable", SpellType.FRIENDLY_ATTACK);
        this.PreloadSpell("Card_Play_Hero_SpellTable", SpellType.FROZEN);
        this.PreloadSpell("Card_Play_Hero_SpellTable", SpellType.DAMAGE);
        this.PreloadSpell("Card_Play_Weapon_SpellTable", SpellType.ENCHANT_POSITIVE);
        this.PreloadSpell("Card_Play_Weapon_SpellTable", SpellType.ENCHANT_NEUTRAL);
        this.PreloadSpell("Card_Play_Weapon_SpellTable", SpellType.ENCHANT_NEGATIVE);
        this.PreloadSpell("Card_Play_Weapon_SpellTable", SpellType.DAMAGE);
        this.PreloadSpell("Card_Play_Weapon_SpellTable", SpellType.DEATH);
        this.PreloadSpell("Card_Play_Weapon_SpellTable", SpellType.SHEATHE);
        this.PreloadSpell("Card_Play_Weapon_SpellTable", SpellType.UNSHEATHE);
        this.PreloadSpell("Card_Play_Weapon_SpellTable", SpellType.SUMMON_IN_OPPONENT);
        this.PreloadSpell("Card_Play_Weapon_SpellTable", SpellType.SUMMON_IN_FRIENDLY);
        this.PreloadSpell("Card_Play_Hero_SpellTable", SpellType.FRIENDLY_ATTACK);
        break;
      case SceneMgr.Mode.COLLECTIONMANAGER:
      case SceneMgr.Mode.TAVERN_BRAWL:
        this.PreloadSpell("Card_Hand_Ally_SpellTable", SpellType.DEATHREVERSE);
        this.PreloadSpell("Card_Hand_Ability_SpellTable", SpellType.DEATHREVERSE);
        this.PreloadSpell("Card_Hand_Weapon_SpellTable", SpellType.DEATHREVERSE);
        this.PreloadSpell("Card_Hand_Ally_SpellTable", SpellType.GHOSTCARD);
        this.PreloadSpell("Card_Hand_Ability_SpellTable", SpellType.GHOSTCARD);
        this.PreloadSpell("Card_Hand_Weapon_SpellTable", SpellType.GHOSTCARD);
        break;
    }
  }

  private void PreloadSpell(string tableName, SpellType type)
  {
    SpellTable spellTable = this.GetSpellTable(tableName);
    if ((Object) spellTable == (Object) null)
    {
      Error.AddDevFatal("SpellCache.PreloadSpell() - Preloaded nonexistent SpellTable {0}", (object) tableName);
    }
    else
    {
      SpellTableEntry entry = spellTable.FindEntry(type);
      if (entry == null)
      {
        Error.AddDevFatal("SpellCache.PreloadSpell() - SpellTable {0} has no spell of type {1}", (object) tableName, (object) type);
      }
      else
      {
        if ((Object) entry.m_Spell != (Object) null)
          return;
        string name = FileUtils.GameAssetPathToName(entry.m_SpellPrefabName);
        GameObject gameObject = AssetLoader.Get().LoadActor(name, true, true);
        if ((Object) gameObject == (Object) null)
        {
          Error.AddDevFatal("SpellCache.PreloadSpell() - Failed to load {0}", (object) name);
        }
        else
        {
          Spell component = gameObject.GetComponent<Spell>();
          if ((Object) component == (Object) null)
            Error.AddDevFatal("SpellCache.PreloadSpell() - {0} does not have a Spell component", (object) name);
          else
            spellTable.SetSpell(type, component);
        }
      }
    }
  }
}
