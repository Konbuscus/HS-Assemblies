﻿// Decompiled with JetBrains decompiler
// Type: CardEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class CardEffect
{
  private Spell m_spell;
  private List<CardSoundSpell> m_soundSpells;
  private string m_spellPath;
  private List<string> m_soundSpellPaths;
  private Card m_owner;

  public CardEffect(CardEffectDef def, Card owner)
  {
    this.m_spellPath = def.m_SpellPath;
    this.m_soundSpellPaths = def.m_SoundSpellPaths;
    this.m_owner = owner;
    if (this.m_soundSpellPaths == null)
      return;
    this.m_soundSpells = new List<CardSoundSpell>(this.m_soundSpellPaths.Count);
    for (int index = 0; index < this.m_soundSpellPaths.Count; ++index)
      this.m_soundSpells.Add((CardSoundSpell) null);
  }

  public CardEffect(string spellPath, Card owner)
  {
    this.m_spellPath = spellPath;
    this.m_owner = owner;
  }

  public Spell GetSpell(bool loadIfNeeded = true)
  {
    if ((Object) this.m_spell == (Object) null && !string.IsNullOrEmpty(this.m_spellPath) && loadIfNeeded)
      this.LoadSpell();
    return this.m_spell;
  }

  public void LoadSoundSpell(int index)
  {
    if (index < 0 || this.m_soundSpellPaths == null || (index >= this.m_soundSpellPaths.Count || string.IsNullOrEmpty(this.m_soundSpellPaths[index])) || !((Object) this.m_soundSpells[index] == (Object) null))
      return;
    string soundSpellPath = this.m_soundSpellPaths[index];
    GameObject gameObject = AssetLoader.Get().LoadSpell(soundSpellPath, true, false);
    if ((Object) gameObject == (Object) null)
    {
      if (AssetLoader.DOWNLOADABLE_LANGUAGE_PACKS)
        return;
      Error.AddDevFatal("CardEffect.LoadSoundSpell() - FAILED TO LOAD \"{0}\" (PATH: \"{1}\") (index {2})", (object) this.m_spellPath, (object) soundSpellPath, (object) index);
    }
    else
    {
      CardSoundSpell component = gameObject.GetComponent<CardSoundSpell>();
      this.m_soundSpells[index] = component;
      if ((Object) component == (Object) null)
      {
        if (AssetLoader.DOWNLOADABLE_LANGUAGE_PACKS)
          return;
        Error.AddDevFatal("CardEffect.LoadSoundSpell() - FAILED TO LOAD \"{0}\" (PATH: \"{1}\") (index {2})", (object) this.m_spellPath, (object) soundSpellPath, (object) index);
      }
      else
      {
        if (!((Object) this.m_owner != (Object) null))
          return;
        SpellUtils.SetupSoundSpell(component, (Component) this.m_owner);
      }
    }
  }

  public List<CardSoundSpell> GetSoundSpells(bool loadIfNeeded = true)
  {
    if (this.m_soundSpells == null)
      return (List<CardSoundSpell>) null;
    if (loadIfNeeded)
    {
      for (int index = 0; index < this.m_soundSpells.Count; ++index)
        this.LoadSoundSpell(index);
    }
    return this.m_soundSpells;
  }

  public void Clear()
  {
    if ((Object) this.m_spell != (Object) null)
      Object.Destroy((Object) this.m_spell.gameObject);
    if (this.m_soundSpells == null)
      return;
    for (int index = 0; index < this.m_soundSpells.Count; ++index)
    {
      Spell soundSpell = (Spell) this.m_soundSpells[index];
      if ((Object) soundSpell != (Object) null)
        Object.Destroy((Object) soundSpell.gameObject);
    }
  }

  public void LoadAll()
  {
    this.GetSpell(true);
    if (this.m_soundSpellPaths == null)
      return;
    for (int index = 0; index < this.m_soundSpellPaths.Count; ++index)
      this.LoadSoundSpell(index);
  }

  public void PurgeSpells()
  {
    SpellUtils.PurgeSpell(this.m_spell);
    SpellUtils.PurgeSpells<CardSoundSpell>(this.m_soundSpells);
  }

  private void LoadSpell()
  {
    GameObject gameObject = AssetLoader.Get().LoadSpell(this.m_spellPath, true, false);
    if ((Object) gameObject == (Object) null)
    {
      Error.AddDevFatalUnlessWorkarounds("CardEffect.LoadSpell() - Failed to load \"{0}\"", (object) this.m_spellPath);
    }
    else
    {
      this.m_spell = gameObject.GetComponent<Spell>();
      if ((Object) this.m_spell == (Object) null)
      {
        Object.Destroy((Object) gameObject);
        Error.AddDevFatalUnlessWorkarounds("CardEffect.LoadSpell() - \"{0}\" does not have a Spell component.", (object) this.m_spellPath);
      }
      else
      {
        if (!((Object) this.m_owner != (Object) null))
          return;
        SpellUtils.SetupSpell(this.m_spell, (Component) this.m_owner);
      }
    }
  }

  private void DestroySpell(Spell spell)
  {
    if ((Object) spell == (Object) null)
      return;
    Object.Destroy((Object) spell.gameObject);
  }
}
