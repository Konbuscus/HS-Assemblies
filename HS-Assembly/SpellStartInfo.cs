﻿// Decompiled with JetBrains decompiler
// Type: SpellStartInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class SpellStartInfo
{
  public bool m_Enabled = true;
  public bool m_UseSuperSpellLocation = true;
  public bool m_DeathAfterAllMissilesFire = true;
  public Spell m_Prefab;
}
