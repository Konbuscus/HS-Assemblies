﻿// Decompiled with JetBrains decompiler
// Type: TB05_GiftExchange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TB05_GiftExchange : MissionEntity
{
  private string[] GiftVOList = new string[4]{ "VO_TB_1503_FATHER_WINTER_GIFT1", "VO_TB_1503_FATHER_WINTER_GIFT2", "VO_TB_1503_FATHER_WINTER_GIFT3", "VO_TB_1503_FATHER_WINTER_GIFT4" };
  private string[] PissedVOList = new string[5]{ "VO_TB_1503_FATHER_WINTER_LONG2", "VO_TB_1503_FATHER_WINTER_LONG3", "VO_TB_1503_FATHER_WINTER_LONG4", "VO_TB_1503_FATHER_WINTER_LONG5", "VO_TB_1503_FATHER_WINTER_LONG6" };
  private string FirstGiftVO = "VO_TB_1503_FATHER_WINTER_GIFT1";
  private string StartVO = "VO_TB_1503_FATHER_WINTER_LONG6";
  private string FirstStolenVO = "VO_TB_1503_FATHER_WINTER_START";
  private string NextStolenVO = "VO_TB_1503_FATHER_WINTER_LONG1";
  private string VOChoice;
  private float delayTime;
  private Notification GiftStolenPopup;
  private Notification GiftSpawnedPopup;
  private Notification GameStartPopup;
  private string textID;
  private Vector3 popUpPos;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_GIFT1");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_GIFT2");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_GIFT3");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_GIFT4");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_LONG1");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_LONG2");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_LONG3");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_LONG4");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_LONG5");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_LONG6");
    this.PreloadSound("VO_TB_1503_FATHER_WINTER_START");
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB05_GiftExchange.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1F2() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
