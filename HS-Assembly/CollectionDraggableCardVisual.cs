﻿// Decompiled with JetBrains decompiler
// Type: CollectionDraggableCardVisual
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CollectionDraggableCardVisual : MonoBehaviour
{
  private static readonly Vector3 DECK_TILE_LOCAL_SCALE = (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen)
  {
    PC = new Vector3(0.6f, 0.6f, 0.6f),
    Phone = new Vector3(0.9f, 0.9f, 0.9f)
  };
  private static readonly Vector3 CARD_ACTOR_LOCAL_SCALE = (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen)
  {
    PC = new Vector3(6f, 6f, 6f),
    Phone = new Vector3(6.9f, 6.9f, 6.9f)
  };
  private static readonly Vector3 HERO_SKIN_ACTOR_LOCAL_SCALE = (Vector3) new PlatformDependentValue<Vector3>(PlatformCategory.Screen)
  {
    PC = new Vector3(7.5f, 7.5f, 7.5f),
    Phone = new Vector3(8.2f, 8.2f, 8.2f)
  };
  public DragRotatorInfo m_CardDragRotatorInfo = new DragRotatorInfo()
  {
    m_PitchInfo = new DragRotatorAxisInfo()
    {
      m_ForceMultiplier = 3f,
      m_MinDegrees = -55f,
      m_MaxDegrees = 55f,
      m_RestSeconds = 2f
    },
    m_RollInfo = new DragRotatorAxisInfo()
    {
      m_ForceMultiplier = 4.5f,
      m_MinDegrees = -60f,
      m_MaxDegrees = 60f,
      m_RestSeconds = 2f
    }
  };
  private HandActorCache m_actorCache = new HandActorCache();
  private CollectionDeckSlot m_slot;
  private DeckTrayDeckTileVisual m_deckTileToRemove;
  private Actor m_cardBackActor;
  private CardBack m_currentCardBack;
  private EntityDef m_entityDef;
  private TAG_PREMIUM m_premium;
  private CardDef m_cardDef;
  private Actor m_activeActor;
  private CollectionDeckTileActor m_deckTile;
  private Actor m_cardActor;
  private string m_cardActorName;
  private bool m_actorCacheInit;
  private CollectionManagerDisplay.ViewMode m_visualType;
  private int m_cardBackId;

  private void Awake()
  {
    this.gameObject.SetActive(false);
    this.LoadDeckTile();
    this.LoadCardBack();
    if (!((Object) this.gameObject.GetComponent<AudioSource>() == (Object) null))
      return;
    this.gameObject.AddComponent<AudioSource>();
  }

  private void Update()
  {
    if ((Object) this.m_deckTileToRemove != (Object) null)
      this.m_deckTileToRemove.SetHighlight(false);
    this.m_deckTileToRemove = (DeckTrayDeckTileVisual) null;
    if ((Object) this.m_activeActor != (Object) this.m_deckTile)
      return;
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    RaycastHit hitInfo;
    if (taggedDeck == null || !UniversalInputManager.Get().GetInputHitInfo((LayerMask) DeckTrayDeckTileVisual.LAYER.LayerBit(), out hitInfo))
      return;
    DeckTrayDeckTileVisual component = hitInfo.collider.gameObject.GetComponent<DeckTrayDeckTileVisual>();
    if ((Object) component == (Object) null || (Object) component == (Object) this.m_deckTileToRemove)
      return;
    if ((Object) this.m_deckTile != (Object) null && (Object) this.m_activeActor != (Object) null)
    {
      bool flag = taggedDeck.GetTotalValidCardCount() == CollectionManager.Get().GetDeckSize();
      bool highlight = !taggedDeck.IsValidSlot(component.GetSlot()) || flag;
      if (highlight)
      {
        NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL_TEMPLATE_REPLACE_1"), 0.0f);
        NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL_TEMPLATE_REPLACE_2"), 0.0f);
        NotificationManager.Get().DestroyNotificationWithText(GameStrings.Get("GLUE_COLLECTION_TUTORIAL_REPLACE_WILD_CARDS"), 0.0f);
      }
      component.SetHighlight(highlight);
    }
    this.m_deckTileToRemove = component;
  }

  public void SetCardBackId(int cardBackId)
  {
    this.m_cardBackId = cardBackId;
  }

  public int GetCardBackId()
  {
    return this.m_cardBackId;
  }

  public string GetCardID()
  {
    return this.m_entityDef.GetCardId();
  }

  public TAG_PREMIUM GetPremium()
  {
    return this.m_premium;
  }

  public EntityDef GetEntityDef()
  {
    return this.m_entityDef;
  }

  public CollectionDeckSlot GetSlot()
  {
    return this.m_slot;
  }

  public void SetSlot(CollectionDeckSlot slot)
  {
    this.m_slot = slot;
  }

  public CollectionManagerDisplay.ViewMode GetVisualType()
  {
    return this.m_visualType;
  }

  public bool ChangeActor(Actor actor, CollectionManagerDisplay.ViewMode vtype)
  {
    if (!this.m_actorCacheInit)
    {
      this.m_actorCacheInit = true;
      this.m_actorCache.AddActorLoadedListener(new HandActorCache.ActorLoadedCallback(this.OnCardActorLoaded));
      this.m_actorCache.Initialize();
    }
    if (this.m_actorCache.IsInitializing())
      return false;
    this.m_visualType = vtype;
    if (this.m_visualType != CollectionManagerDisplay.ViewMode.CARD_BACKS)
    {
      EntityDef entityDef = actor.GetEntityDef();
      TAG_PREMIUM premium = actor.GetPremium();
      bool flag1 = entityDef != this.m_entityDef;
      bool flag2 = premium != this.m_premium;
      if (!flag1 && !flag2)
        return true;
      this.m_entityDef = entityDef;
      this.m_premium = premium;
      this.m_cardActor = this.m_actorCache.GetActor(entityDef, premium);
      if ((Object) this.m_cardActor == (Object) null)
        return false;
      if (flag1)
      {
        DefLoader.Get().LoadCardDef(this.m_entityDef.GetCardId(), new DefLoader.LoadDefCallback<CardDef>(this.OnCardDefLoaded), (object) new CardPortraitQuality(1, this.m_premium), (CardPortraitQuality) null);
      }
      else
      {
        this.InitDeckTileActor();
        this.InitCardActor();
      }
      return true;
    }
    if (!((Object) actor != (Object) null))
      return false;
    this.m_entityDef = (EntityDef) null;
    this.m_premium = TAG_PREMIUM.NORMAL;
    this.m_currentCardBack = actor.GetComponentInChildren<CardBack>();
    this.m_cardActor = this.m_cardBackActor;
    this.m_cardBackActor.SetCardbackUpdateIgnore(true);
    return true;
  }

  public void UpdateVisual(bool isOverDeck)
  {
    Actor activeActor = this.m_activeActor;
    SpellType spellType;
    if (this.m_visualType == CollectionManagerDisplay.ViewMode.CARDS)
    {
      if (isOverDeck && this.m_entityDef != null && !this.m_entityDef.IsHero())
      {
        this.m_activeActor = (Actor) this.m_deckTile;
        spellType = SpellType.SUMMON_IN;
      }
      else
      {
        this.m_activeActor = this.m_cardActor;
        spellType = SpellType.DEATHREVERSE;
      }
    }
    else
    {
      this.m_activeActor = this.m_cardActor;
      spellType = SpellType.DEATHREVERSE;
      if ((Object) this.m_deckTileToRemove != (Object) null)
        this.m_deckTileToRemove.SetHighlight(false);
    }
    if ((Object) activeActor == (Object) this.m_activeActor)
      return;
    if ((Object) activeActor != (Object) null)
    {
      activeActor.Hide();
      activeActor.gameObject.SetActive(false);
    }
    if ((Object) this.m_activeActor == (Object) null)
      return;
    this.m_activeActor.gameObject.SetActive(true);
    this.m_activeActor.Show();
    if (this.m_visualType == CollectionManagerDisplay.ViewMode.CARD_BACKS && (Object) this.m_currentCardBack != (Object) null)
      CardBackManager.Get().UpdateCardBack(this.m_activeActor, this.m_currentCardBack);
    Spell spell = this.m_activeActor.GetSpell(spellType);
    if ((Object) spell != (Object) null)
      spell.ActivateState(SpellStateType.BIRTH);
    if (this.m_entityDef == null || !this.m_entityDef.IsHero())
      return;
    CollectionHeroSkin component = this.m_activeActor.gameObject.GetComponent<CollectionHeroSkin>();
    component.SetClass(this.m_entityDef.GetClass());
    component.ShowSocketFX();
  }

  public bool IsShown()
  {
    return this.gameObject.activeSelf;
  }

  public void Show(bool isOverDeck)
  {
    this.gameObject.SetActive(true);
    this.UpdateVisual(isOverDeck);
    if (!((Object) this.m_deckTile != (Object) null) || this.m_entityDef == null)
      return;
    this.m_deckTile.UpdateDeckCardProperties(this.m_entityDef.IsElite(), 1, false);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    if ((Object) this.m_activeActor == (Object) null)
      return;
    this.m_activeActor.Hide();
    this.m_activeActor.gameObject.SetActive(false);
    this.m_activeActor = (Actor) null;
  }

  public DeckTrayDeckTileVisual GetDeckTileToRemove()
  {
    return this.m_deckTileToRemove;
  }

  private void LoadDeckTile()
  {
    GameObject gameObject = AssetLoader.Get().LoadActor("DeckCardBar", false, false);
    if ((Object) gameObject == (Object) null)
    {
      Debug.LogWarning((object) string.Format("CollectionDraggableCardVisual.OnDeckTileActorLoaded() - FAILED to load actor \"{0}\"", (object) "DeckCardBar"));
    }
    else
    {
      this.m_deckTile = gameObject.GetComponent<CollectionDeckTileActor>();
      if ((Object) this.m_deckTile == (Object) null)
      {
        Debug.LogWarning((object) string.Format("CollectionDraggableCardVisual.OnDeckTileActorLoaded() - ERROR game object \"{0}\" has no CollectionDeckTileActor component", (object) "DeckCardBar"));
      }
      else
      {
        this.m_deckTile.Hide();
        this.m_deckTile.transform.parent = this.transform;
        this.m_deckTile.transform.localPosition = new Vector3(2.194931f, 0.0f, 0.0f);
        this.m_deckTile.transform.localScale = CollectionDraggableCardVisual.DECK_TILE_LOCAL_SCALE;
        this.m_deckTile.transform.localEulerAngles = new Vector3(0.0f, 180f, 0.0f);
      }
    }
  }

  private void LoadCardBack()
  {
    GameObject child = AssetLoader.Get().LoadActor("Card_Hidden", false, false);
    GameUtils.SetParent(child, (Component) this, false);
    this.m_cardBackActor = child.GetComponent<Actor>();
    this.m_cardBackActor.transform.localScale = CollectionDraggableCardVisual.CARD_ACTOR_LOCAL_SCALE;
    this.m_cardBackActor.TurnOffCollider();
    this.m_cardBackActor.Hide();
    child.AddComponent<DragRotator>().SetInfo(this.m_CardDragRotatorInfo);
  }

  private void OnCardActorLoaded(string name, Actor actor, object callbackData)
  {
    if ((Object) actor == (Object) null)
    {
      Debug.LogWarning((object) string.Format("CollectionDraggableCardVisual.OnCardActorLoaded() - FAILED to load {0}", (object) name));
    }
    else
    {
      ((object) actor).GetType();
      actor.TurnOffCollider();
      actor.Hide();
      if (name == "Card_Hero_Skin")
        actor.transform.localScale = CollectionDraggableCardVisual.HERO_SKIN_ACTOR_LOCAL_SCALE;
      else
        actor.transform.localScale = CollectionDraggableCardVisual.CARD_ACTOR_LOCAL_SCALE;
      actor.transform.parent = this.transform;
      actor.transform.localPosition = Vector3.zero;
      actor.gameObject.AddComponent<DragRotator>().SetInfo(this.m_CardDragRotatorInfo);
    }
  }

  private void OnCardDefLoaded(string cardID, CardDef cardDef, object callbackData)
  {
    if (this.m_entityDef == null || this.m_entityDef.GetCardId() != cardID)
      return;
    this.m_cardDef = cardDef;
    this.InitDeckTileActor();
    this.InitCardActor();
  }

  private void InitDeckTileActor()
  {
    this.InitActor((Actor) this.m_deckTile);
    this.m_deckTile.SetCardDef(this.m_cardDef);
    this.m_deckTile.SetDisablePremiumPortrait(true);
    this.m_deckTile.UpdateAllComponents();
    this.m_deckTile.UpdateMaterial(this.m_cardDef.GetDeckCardBarPortrait());
    this.m_deckTile.UpdateDeckCardProperties(this.m_entityDef.IsElite(), 1, false);
  }

  private void InitCardActor()
  {
    this.InitActor(this.m_cardActor);
    this.m_cardActor.transform.localRotation = Quaternion.identity;
  }

  private void InitActor(Actor actor)
  {
    actor.SetEntityDef(this.m_entityDef);
    actor.SetCardDef(this.m_cardDef);
    actor.SetPremium(this.m_premium);
    actor.UpdateAllComponents();
  }
}
