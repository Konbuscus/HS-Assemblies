﻿// Decompiled with JetBrains decompiler
// Type: HighlightRender
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class HighlightRender : MonoBehaviour
{
  private static float s_offset = -2000f;
  private readonly string MULTISAMPLE_SHADER_NAME = "Custom/Selection/HighlightMultiSample";
  private readonly string MULTISAMPLE_BLEND_SHADER_NAME = "Custom/Selection/HighlightMultiSampleBlend";
  private readonly string BLEND_SHADER_NAME = "Custom/Selection/HighlightMaskBlend";
  private readonly string HIGHLIGHT_SHADER_NAME = "Custom/Selection/Highlight";
  private readonly string UNLIT_COLOR_SHADER_NAME = "Custom/UnlitColor";
  private readonly string UNLIT_GREY_SHADER_NAME = "Custom/Unlit/Color/Grey";
  private readonly string UNLIT_LIGHTGREY_SHADER_NAME = "Custom/Unlit/Color/LightGrey";
  private readonly string UNLIT_DARKGREY_SHADER_NAME = "Custom/Unlit/Color/DarkGrey";
  private readonly string UNLIT_BLACK_SHADER_NAME = "Custom/Unlit/Color/BlackOverlay";
  private readonly string UNLIT_WHITE_SHADER_NAME = "Custom/Unlit/Color/White";
  public float m_SilouetteRenderSize = 1f;
  public float m_SilouetteClipSize = 1f;
  private float m_RenderScale = 1f;
  private int m_RenderSizeX = 200;
  private int m_RenderSizeY = 200;
  private const float RENDER_SIZE1 = 0.3f;
  private const float RENDER_SIZE2 = 0.3f;
  private const float RENDER_SIZE3 = 0.5f;
  private const float RENDER_SIZE4 = 0.92f;
  private const float ORTHO_SIZE1 = 0.2f;
  private const float ORTHO_SIZE2 = 0.25f;
  private const float ORTHO_SIZE3 = 0.01f;
  private const float ORTHO_SIZE4 = -0.05f;
  private const float BLUR_BLEND1 = 1.25f;
  private const float BLUR_BLEND2 = 1.25f;
  private const float BLUR_BLEND3 = 1f;
  private const float BLUR_BLEND4 = 1.5f;
  private const int SILHOUETTE_RENDER_SIZE = 200;
  private const int SILHOUETTE_RENDER_DEPTH = 24;
  private const int MAX_HIGHLIGHT_EXCLUDE_PARENT_SEARCH = 25;
  public Transform m_RootTransform;
  private GameObject m_RenderPlane;
  private Vector3 m_OrgPosition;
  private Quaternion m_OrgRotation;
  private Vector3 m_OrgScale;
  private Shader m_MultiSampleShader;
  private Shader m_MultiSampleBlendShader;
  private Shader m_BlendShader;
  private Shader m_HighlightShader;
  private Shader m_UnlitColorShader;
  private Shader m_UnlitGreyShader;
  private Shader m_UnlitLightGreyShader;
  private Shader m_UnlitDarkGreyShader;
  private Shader m_UnlitBlackShader;
  private Shader m_UnlitWhiteShader;
  private RenderTexture m_CameraTexture;
  private Camera m_Camera;
  private float m_CameraOrthoSize;
  private Map<Renderer, bool> m_VisibilityStates;
  private Map<Transform, Vector3> m_ObjectsOrginalPosition;
  private bool m_Initialized;
  private Material m_Material;
  private Material m_MultiSampleMaterial;
  private Material m_MultiSampleBlendMaterial;
  private Material m_BlendMaterial;

  protected Material HighlightMaterial
  {
    get
    {
      if ((Object) this.m_Material == (Object) null)
      {
        this.m_Material = new Material(this.m_HighlightShader);
        SceneUtils.SetHideFlags((Object) this.m_Material, HideFlags.DontSave);
      }
      return this.m_Material;
    }
  }

  protected Material MultiSampleMaterial
  {
    get
    {
      if ((Object) this.m_MultiSampleMaterial == (Object) null)
      {
        this.m_MultiSampleMaterial = new Material(this.m_MultiSampleShader);
        SceneUtils.SetHideFlags((Object) this.m_MultiSampleMaterial, HideFlags.DontSave);
      }
      return this.m_MultiSampleMaterial;
    }
  }

  protected Material MultiSampleBlendMaterial
  {
    get
    {
      if ((Object) this.m_MultiSampleBlendMaterial == (Object) null)
      {
        this.m_MultiSampleBlendMaterial = new Material(this.m_MultiSampleBlendShader);
        SceneUtils.SetHideFlags((Object) this.m_MultiSampleBlendMaterial, HideFlags.DontSave);
      }
      return this.m_MultiSampleBlendMaterial;
    }
  }

  protected Material BlendMaterial
  {
    get
    {
      if ((Object) this.m_BlendMaterial == (Object) null)
      {
        this.m_BlendMaterial = new Material(this.m_BlendShader);
        SceneUtils.SetHideFlags((Object) this.m_BlendMaterial, HideFlags.DontSave);
      }
      return this.m_BlendMaterial;
    }
  }

  public RenderTexture SilhouetteTexture
  {
    get
    {
      return this.m_CameraTexture;
    }
  }

  protected void OnApplicationFocus(bool state)
  {
  }

  protected void OnDisable()
  {
    if ((bool) ((Object) this.m_Material))
      Object.Destroy((Object) this.m_Material);
    if ((bool) ((Object) this.m_MultiSampleMaterial))
      Object.Destroy((Object) this.m_MultiSampleMaterial);
    if ((bool) ((Object) this.m_BlendMaterial))
      Object.Destroy((Object) this.m_BlendMaterial);
    if ((bool) ((Object) this.m_MultiSampleBlendMaterial))
      Object.Destroy((Object) this.m_MultiSampleBlendMaterial);
    if (this.m_VisibilityStates != null)
      this.m_VisibilityStates.Clear();
    if ((Object) this.m_CameraTexture != (Object) null)
    {
      if ((Object) RenderTexture.active == (Object) this.m_CameraTexture)
        RenderTexture.active = (RenderTexture) null;
      this.m_CameraTexture.Release();
      this.m_CameraTexture = (RenderTexture) null;
    }
    this.m_Initialized = false;
  }

  protected void Initialize()
  {
    if (this.m_Initialized)
      return;
    this.m_Initialized = true;
    if ((Object) this.m_HighlightShader == (Object) null)
      this.m_HighlightShader = ShaderUtils.FindShader(this.HIGHLIGHT_SHADER_NAME);
    if (!(bool) ((Object) this.m_HighlightShader))
    {
      Debug.LogError((object) ("Failed to load Highlight Shader: " + this.HIGHLIGHT_SHADER_NAME));
      this.enabled = false;
    }
    this.GetComponent<Renderer>().material.shader = this.m_HighlightShader;
    if ((Object) this.m_MultiSampleShader == (Object) null)
      this.m_MultiSampleShader = ShaderUtils.FindShader(this.MULTISAMPLE_SHADER_NAME);
    if (!(bool) ((Object) this.m_MultiSampleShader))
    {
      Debug.LogError((object) ("Failed to load Highlight Shader: " + this.MULTISAMPLE_SHADER_NAME));
      this.enabled = false;
    }
    if ((Object) this.m_MultiSampleBlendShader == (Object) null)
      this.m_MultiSampleBlendShader = ShaderUtils.FindShader(this.MULTISAMPLE_BLEND_SHADER_NAME);
    if (!(bool) ((Object) this.m_MultiSampleBlendShader))
    {
      Debug.LogError((object) ("Failed to load Highlight Shader: " + this.MULTISAMPLE_BLEND_SHADER_NAME));
      this.enabled = false;
    }
    if ((Object) this.m_BlendShader == (Object) null)
      this.m_BlendShader = ShaderUtils.FindShader(this.BLEND_SHADER_NAME);
    if (!(bool) ((Object) this.m_BlendShader))
    {
      Debug.LogError((object) ("Failed to load Highlight Shader: " + this.BLEND_SHADER_NAME));
      this.enabled = false;
    }
    if ((Object) this.m_RootTransform == (Object) null)
    {
      Transform parent = this.transform.parent.parent;
      this.m_RootTransform = !(bool) ((Object) parent.GetComponent<ActorStateMgr>()) ? parent : parent.parent;
      if ((Object) this.m_RootTransform == (Object) null)
      {
        Debug.LogError((object) "m_RootTransform is null. Highlighting disabled!");
        this.enabled = false;
      }
    }
    this.m_VisibilityStates = new Map<Renderer, bool>();
    HighlightSilhouetteInclude[] componentsInChildren = this.m_RootTransform.GetComponentsInChildren<HighlightSilhouetteInclude>();
    if (componentsInChildren != null)
    {
      foreach (Component component1 in componentsInChildren)
      {
        Renderer component2 = component1.gameObject.GetComponent<Renderer>();
        if (!((Object) component2 == (Object) null))
          this.m_VisibilityStates.Add(component2, false);
      }
    }
    this.m_UnlitColorShader = ShaderUtils.FindShader(this.UNLIT_COLOR_SHADER_NAME);
    if (!(bool) ((Object) this.m_UnlitColorShader))
      Debug.LogError((object) ("Failed to load Highlight Rendering Shader: " + this.UNLIT_COLOR_SHADER_NAME));
    this.m_UnlitGreyShader = ShaderUtils.FindShader(this.UNLIT_GREY_SHADER_NAME);
    if (!(bool) ((Object) this.m_UnlitGreyShader))
      Debug.LogError((object) ("Failed to load Highlight Rendering Shader: " + this.UNLIT_GREY_SHADER_NAME));
    this.m_UnlitLightGreyShader = ShaderUtils.FindShader(this.UNLIT_LIGHTGREY_SHADER_NAME);
    if (!(bool) ((Object) this.m_UnlitLightGreyShader))
      Debug.LogError((object) ("Failed to load Highlight Rendering Shader: " + this.UNLIT_LIGHTGREY_SHADER_NAME));
    this.m_UnlitDarkGreyShader = ShaderUtils.FindShader(this.UNLIT_DARKGREY_SHADER_NAME);
    if (!(bool) ((Object) this.m_UnlitDarkGreyShader))
      Debug.LogError((object) ("Failed to load Highlight Rendering Shader: " + this.UNLIT_DARKGREY_SHADER_NAME));
    this.m_UnlitBlackShader = ShaderUtils.FindShader(this.UNLIT_BLACK_SHADER_NAME);
    if (!(bool) ((Object) this.m_UnlitBlackShader))
      Debug.LogError((object) ("Failed to load Highlight Rendering Shader: " + this.UNLIT_BLACK_SHADER_NAME));
    this.m_UnlitWhiteShader = ShaderUtils.FindShader(this.UNLIT_WHITE_SHADER_NAME);
    if ((bool) ((Object) this.m_UnlitWhiteShader))
      return;
    Debug.LogError((object) ("Failed to load Highlight Rendering Shader: " + this.UNLIT_WHITE_SHADER_NAME));
  }

  protected void Update()
  {
    if (!(bool) ((Object) this.m_CameraTexture) || !this.m_Initialized || this.m_CameraTexture.IsCreated())
      return;
    this.CreateSilhouetteTexture();
  }

  [ContextMenu("Export Silhouette Texture")]
  public void ExportSilhouetteTexture()
  {
    RenderTexture.active = this.m_CameraTexture;
    Texture2D texture2D = new Texture2D(this.m_RenderSizeX, this.m_RenderSizeY, TextureFormat.RGB24, false);
    texture2D.ReadPixels(new Rect(0.0f, 0.0f, (float) this.m_RenderSizeX, (float) this.m_RenderSizeY), 0, 0, false);
    texture2D.Apply();
    string path = Application.dataPath + "/SilhouetteTexture.png";
    File.WriteAllBytes(path, texture2D.EncodeToPNG());
    RenderTexture.active = (RenderTexture) null;
    Debug.Log((object) string.Format("Silhouette Texture Created: {0}", (object) path));
  }

  public void CreateSilhouetteTexture()
  {
    this.CreateSilhouetteTexture(false);
  }

  public void CreateSilhouetteTexture(bool force)
  {
    this.Initialize();
    if (!this.VisibilityStatesChanged() && !force)
      return;
    this.SetupRenderObjects();
    if ((Object) this.m_RenderPlane == (Object) null || this.m_RenderSizeX < 1 || this.m_RenderSizeY < 1)
      return;
    bool enabled = this.GetComponent<Renderer>().enabled;
    this.GetComponent<Renderer>().enabled = false;
    RenderTexture temporary1 = RenderTexture.GetTemporary((int) ((double) this.m_RenderSizeX * 0.300000011920929), (int) ((double) this.m_RenderSizeY * 0.300000011920929), 24);
    RenderTexture temporary2 = RenderTexture.GetTemporary((int) ((double) this.m_RenderSizeX * 0.300000011920929), (int) ((double) this.m_RenderSizeY * 0.300000011920929), 24);
    RenderTexture temporary3 = RenderTexture.GetTemporary((int) ((double) this.m_RenderSizeX * 0.300000011920929), (int) ((double) this.m_RenderSizeY * 0.300000011920929), 24);
    RenderTexture temporary4 = RenderTexture.GetTemporary((int) ((double) this.m_RenderSizeX * 0.5), (int) ((double) this.m_RenderSizeY * 0.5), 24);
    RenderTexture temporary5 = RenderTexture.GetTemporary((int) ((double) this.m_RenderSizeX * 0.5), (int) ((double) this.m_RenderSizeY * 0.5), 24);
    RenderTexture temporary6 = RenderTexture.GetTemporary(this.m_RenderSizeX, this.m_RenderSizeY, 24);
    RenderTexture temporary7 = RenderTexture.GetTemporary(this.m_RenderSizeX, this.m_RenderSizeY, 24);
    RenderTexture temporary8 = RenderTexture.GetTemporary(this.m_RenderSizeX, this.m_RenderSizeY, 24);
    RenderTexture temporary9 = RenderTexture.GetTemporary((int) ((double) this.m_RenderSizeX * 0.920000016689301), (int) ((double) this.m_RenderSizeY * 0.920000016689301), 24);
    this.m_CameraTexture.DiscardContents();
    this.m_Camera.clearFlags = CameraClearFlags.Color;
    this.m_Camera.depth = Camera.main.depth - 1f;
    this.m_Camera.clearFlags = CameraClearFlags.Color;
    this.m_Camera.orthographicSize = this.m_CameraOrthoSize + 0.1f * this.m_SilouetteClipSize;
    this.m_Camera.targetTexture = temporary9;
    this.m_Camera.RenderWithShader(this.m_UnlitWhiteShader, "Highlight");
    this.m_Camera.depth = Camera.main.depth - 5f;
    this.m_Camera.orthographicSize = this.m_CameraOrthoSize - 0.2f * this.m_SilouetteRenderSize;
    this.m_Camera.targetTexture = temporary1;
    this.m_Camera.RenderWithShader(this.m_UnlitDarkGreyShader, "Highlight");
    this.m_Camera.depth = Camera.main.depth - 4f;
    this.m_Camera.orthographicSize = this.m_CameraOrthoSize - 0.25f * this.m_SilouetteRenderSize;
    this.m_Camera.targetTexture = temporary2;
    this.m_Camera.RenderWithShader(this.m_UnlitGreyShader, "Highlight");
    this.SampleBlend(temporary1, temporary2, temporary3, 1.25f);
    this.m_Camera.depth = Camera.main.depth - 3f;
    this.m_Camera.orthographicSize = this.m_CameraOrthoSize - 0.01f * this.m_SilouetteRenderSize;
    this.m_Camera.targetTexture = temporary4;
    this.m_Camera.RenderWithShader(this.m_UnlitLightGreyShader, "Highlight");
    this.SampleBlend(temporary3, temporary4, temporary5, 1.25f);
    this.m_Camera.depth = Camera.main.depth - 2f;
    this.m_Camera.orthographicSize = this.m_CameraOrthoSize - -0.05f * this.m_SilouetteRenderSize;
    this.m_Camera.targetTexture = temporary6;
    this.m_Camera.RenderWithShader(this.m_UnlitWhiteShader, "Highlight");
    this.SampleBlend(temporary5, temporary6, temporary7, 1f);
    this.Sample(temporary7, temporary8, 1.5f);
    this.BlendMaterial.SetTexture("_BlendTex", (Texture) temporary9);
    float num = 0.8f;
    Graphics.BlitMultiTap((Texture) temporary8, this.m_CameraTexture, this.BlendMaterial, new Vector2(-num, -num), new Vector2(-num, num), new Vector2(num, num), new Vector2(num, -num));
    RenderTexture.ReleaseTemporary(temporary1);
    RenderTexture.ReleaseTemporary(temporary2);
    RenderTexture.ReleaseTemporary(temporary3);
    RenderTexture.ReleaseTemporary(temporary4);
    RenderTexture.ReleaseTemporary(temporary5);
    RenderTexture.ReleaseTemporary(temporary6);
    RenderTexture.ReleaseTemporary(temporary7);
    RenderTexture.ReleaseTemporary(temporary8);
    RenderTexture.ReleaseTemporary(temporary9);
    this.m_Camera.orthographicSize = this.m_CameraOrthoSize;
    this.GetComponent<Renderer>().enabled = enabled;
    this.RestoreRenderObjects();
  }

  public bool isTextureCreated()
  {
    if ((bool) ((Object) this.m_CameraTexture))
      return this.m_CameraTexture.IsCreated();
    return false;
  }

  private void SetupRenderObjects()
  {
    if ((Object) this.m_RootTransform == (Object) null)
    {
      this.m_RenderPlane = (GameObject) null;
    }
    else
    {
      HighlightRender.s_offset -= 10f;
      if ((double) HighlightRender.s_offset < -9000.0)
        HighlightRender.s_offset = -2000f;
      this.m_OrgPosition = this.m_RootTransform.position;
      this.m_OrgRotation = this.m_RootTransform.rotation;
      this.m_OrgScale = this.m_RootTransform.localScale;
      this.m_RootTransform.position = Vector3.left * HighlightRender.s_offset;
      this.SetWorldScale(this.m_RootTransform, Vector3.one);
      this.m_RootTransform.rotation = Quaternion.identity;
      Bounds bounds = this.GetComponent<Renderer>().bounds;
      float x = bounds.size.x;
      float num = bounds.size.z;
      if ((double) num < (double) bounds.size.y)
        num = bounds.size.y;
      if ((double) x > (double) num)
      {
        this.m_RenderSizeX = 200;
        this.m_RenderSizeY = (int) (200.0 * ((double) num / (double) x));
      }
      else
      {
        this.m_RenderSizeX = (int) (200.0 * ((double) x / (double) num));
        this.m_RenderSizeY = 200;
      }
      this.m_CameraOrthoSize = num * 0.5f;
      if ((Object) this.m_CameraTexture == (Object) null)
      {
        if (this.m_RenderSizeX < 1 || this.m_RenderSizeY < 1)
        {
          this.m_RenderSizeX = 200;
          this.m_RenderSizeY = 200;
        }
        this.m_CameraTexture = new RenderTexture(this.m_RenderSizeX, this.m_RenderSizeY, 24);
        this.m_CameraTexture.format = RenderTextureFormat.ARGB32;
      }
      HighlightState componentInChildren1 = this.m_RootTransform.GetComponentInChildren<HighlightState>();
      if ((Object) componentInChildren1 == (Object) null)
      {
        Debug.LogError((object) "Can not find Highlight(HighlightState component) object for selection highlighting.");
        this.m_RenderPlane = (GameObject) null;
      }
      else
      {
        componentInChildren1.transform.localPosition = Vector3.zero;
        HighlightRender componentInChildren2 = this.m_RootTransform.GetComponentInChildren<HighlightRender>();
        if ((Object) componentInChildren2 == (Object) null)
        {
          Debug.LogError((object) "Can not find render plane object(HighlightRender component) for selection highlighting.");
          this.m_RenderPlane = (GameObject) null;
        }
        else
        {
          this.m_RenderPlane = componentInChildren2.gameObject;
          this.m_RenderScale = HighlightRender.GetWorldScale(this.m_RenderPlane.transform).x;
          this.CreateCamera(componentInChildren2.transform);
        }
      }
    }
  }

  private void RestoreRenderObjects()
  {
    this.m_RootTransform.position = this.m_OrgPosition;
    this.m_RootTransform.rotation = this.m_OrgRotation;
    this.m_RootTransform.localScale = this.m_OrgScale;
    this.m_RenderPlane = (GameObject) null;
  }

  private bool VisibilityStatesChanged()
  {
    bool flag1 = false;
    HighlightSilhouetteInclude[] componentsInChildren = this.m_RootTransform.GetComponentsInChildren<HighlightSilhouetteInclude>();
    List<Renderer> rendererList = new List<Renderer>();
    foreach (Component component1 in componentsInChildren)
    {
      Renderer component2 = component1.gameObject.GetComponent<Renderer>();
      if ((Object) component2 != (Object) null)
        rendererList.Add(component2);
    }
    Map<Renderer, bool> map = new Map<Renderer, bool>();
    using (List<Renderer>.Enumerator enumerator = rendererList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Renderer current = enumerator.Current;
        bool flag2 = current.enabled && current.gameObject.activeInHierarchy;
        if (!this.m_VisibilityStates.ContainsKey(current))
        {
          map.Add(current, flag2);
          if (flag2)
            flag1 = true;
        }
        else
        {
          if (this.m_VisibilityStates[current] != flag2)
            flag1 = true;
          map.Add(current, flag2);
        }
      }
    }
    return flag1;
  }

  private List<GameObject> GetExcludedObjects()
  {
    List<GameObject> gameObjectList = new List<GameObject>();
    HighlightExclude[] componentsInChildren1 = this.m_RootTransform.GetComponentsInChildren<HighlightExclude>();
    if (componentsInChildren1 == null)
      return (List<GameObject>) null;
    foreach (Component component in componentsInChildren1)
    {
      Transform[] componentsInChildren2 = component.GetComponentsInChildren<Transform>();
      if (componentsInChildren2 != null)
      {
        foreach (Transform transform in componentsInChildren2)
          gameObjectList.Add(transform.gameObject);
      }
    }
    gameObjectList.Add(this.gameObject);
    gameObjectList.Add(this.transform.parent.gameObject);
    return gameObjectList;
  }

  private bool isHighlighExclude(Transform objXform)
  {
    if ((Object) objXform == (Object) null)
      return true;
    HighlightExclude component1 = objXform.GetComponent<HighlightExclude>();
    if ((bool) ((Object) component1) && component1.enabled)
      return true;
    Transform parent = objXform.transform.parent;
    if ((Object) parent != (Object) null)
    {
      for (int index = 0; ((Object) parent != (Object) this.m_RootTransform || (Object) parent != (Object) null) && (!((Object) parent == (Object) null) && index <= 25); ++index)
      {
        HighlightExclude component2 = parent.GetComponent<HighlightExclude>();
        if ((Object) component2 != (Object) null && component2.ExcludeChildren)
          return true;
        parent = parent.parent;
      }
    }
    return false;
  }

  private void CreateCamera(Transform renderPlane)
  {
    if ((Object) this.m_Camera != (Object) null)
      return;
    GameObject gameObject = new GameObject();
    this.m_Camera = gameObject.AddComponent<Camera>();
    gameObject.name = renderPlane.name + "_SilhouetteCamera";
    SceneUtils.SetHideFlags((Object) gameObject, HideFlags.HideAndDontSave);
    this.m_Camera.orthographic = true;
    this.m_Camera.orthographicSize = this.m_CameraOrthoSize;
    this.m_Camera.transform.position = renderPlane.position;
    this.m_Camera.transform.rotation = renderPlane.rotation;
    this.m_Camera.transform.Rotate(90f, 180f, 0.0f);
    this.m_Camera.transform.parent = renderPlane;
    this.m_Camera.nearClipPlane = (float) (-(double) this.m_RenderScale + 1.0);
    this.m_Camera.farClipPlane = this.m_RenderScale + 1f;
    this.m_Camera.depth = Camera.main.depth - 5f;
    this.m_Camera.backgroundColor = Color.black;
    this.m_Camera.clearFlags = CameraClearFlags.Color;
    this.m_Camera.depthTextureMode = DepthTextureMode.None;
    this.m_Camera.renderingPath = RenderingPath.VertexLit;
    this.m_Camera.SetReplacementShader(this.m_UnlitColorShader, string.Empty);
    this.m_Camera.targetTexture = (RenderTexture) null;
    this.m_Camera.enabled = false;
  }

  private void Sample(RenderTexture source, RenderTexture dest, float off)
  {
    if ((Object) source == (Object) null || (Object) dest == (Object) null)
      return;
    dest.DiscardContents();
    Graphics.BlitMultiTap((Texture) source, dest, this.MultiSampleMaterial, new Vector2(-off, -off), new Vector2(-off, off), new Vector2(off, off), new Vector2(off, -off));
  }

  private void SampleBlend(RenderTexture source, RenderTexture blend, RenderTexture dest, float off)
  {
    if ((Object) source == (Object) null || (Object) dest == (Object) null || (Object) blend == (Object) null)
      return;
    this.MultiSampleBlendMaterial.SetTexture("_BlendTex", (Texture) blend);
    dest.DiscardContents();
    Graphics.BlitMultiTap((Texture) source, dest, this.MultiSampleBlendMaterial, new Vector2(-off, -off), new Vector2(-off, off), new Vector2(off, off), new Vector2(off, -off));
  }

  public static Vector3 GetWorldScale(Transform transform)
  {
    Vector3 a = transform.localScale;
    for (Transform parent = transform.parent; (Object) parent != (Object) null; parent = parent.parent)
      a = Vector3.Scale(a, parent.localScale);
    return a;
  }

  public void SetWorldScale(Transform xform, Vector3 scale)
  {
    GameObject gameObject = new GameObject();
    Transform transform = gameObject.transform;
    transform.parent = (Transform) null;
    transform.localRotation = Quaternion.identity;
    transform.localScale = Vector3.one;
    Transform parent = xform.parent;
    xform.parent = transform;
    xform.localScale = scale;
    xform.parent = parent;
    Object.Destroy((Object) gameObject);
  }
}
