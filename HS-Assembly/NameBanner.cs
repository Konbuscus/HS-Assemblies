﻿// Decompiled with JetBrains decompiler
// Type: NameBanner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class NameBanner : MonoBehaviour
{
  public float FUDGE_FACTOR = 0.1915f;
  private bool m_shouldCenterName = true;
  private const float SKINNED_BANNER_MIN_SIZE = 12f;
  private const float SKINNED_MEDAL_BANNER_MIN_SIZE = 17f;
  private const float SKINNED_GAME_ICON_BANNER_MIN_SIZE = 17f;
  private const float UNKNOWN_NAME_WAIT = 5f;
  public GameObject m_alphaBannerSkinned;
  public GameObject m_alphaBannerBone;
  public GameObject m_medalBannerSkinned;
  public GameObject m_medalBannerBone;
  public GameObject m_modeIconBannerSkinned;
  public GameObject m_modeIconBannerBone;
  public GameObject m_alphaBanner;
  public GameObject m_alphaBannerLeft;
  public GameObject m_alphaBannerMiddle;
  public GameObject m_alphaBannerRight;
  public GameObject m_medalAlphaBanner;
  public GameObject m_medalAlphaBannerLeft;
  public GameObject m_medalAlphaBannerMiddle;
  public GameObject m_medalAlphaBannerRight;
  public bool m_canShowModeIcons;
  public bool m_isGameplayNameBanner;
  public GameObject m_casualGameModeIcon;
  public GameObject m_casualWildGameModeIcon;
  public GameObject m_arenaGameModeIcon;
  public UberText m_arenaWinsText;
  public GameObject m_Xmark1;
  public GameObject m_Xmark2;
  public GameObject m_Xmark3;
  public GameObject m_adventureGameModeIcon;
  public GameObject m_adventureIcon;
  public GameObject m_adventureShadow;
  public GameObject m_friendlyGameModeIcon;
  public GameObject m_friendlyWildGameModeIcon;
  public GameObject m_friendlyBrawlGameModeIcon;
  public TavernBrawlGameModeIcon m_tavernBrawlGameMode;
  public GameObject m_tavernBrawlGameModeIcon;
  public GameObject m_friendlyBrawlBanner;
  public GameObject m_HeroicTavernBrawlIcon;
  public UberText m_HeroicBrawlWinsText;
  public GameObject m_HeroicBrawlXmark1;
  public GameObject m_HeroicBrawlXmark2;
  public GameObject m_HeroicBrawlXmark3;
  public GameObject m_nameText;
  public GameObject m_longNameText;
  public Transform m_nameBone;
  public Transform m_classBone;
  public Transform m_longNameBone;
  public Transform m_longClassBone;
  public Transform m_medalNameBone;
  public Transform m_medalClassBone;
  public Transform m_longMedalNameBone;
  public Transform m_longMedalClassBone;
  public TournamentMedal m_medal;
  public UberText m_playerName;
  public UberText m_subtext;
  public UberText m_longPlayerName;
  public UberText m_longSubtext;
  private int m_playerId;
  private Player.Side m_playerSide;
  private Transform m_nameBoneToUse;
  private Transform m_classBoneToUse;
  private UberText m_currentPlayerName;
  private UberText m_currentSubtext;
  private int m_missionId;
  private bool m_useLongName;

  private void Awake()
  {
    this.m_currentPlayerName = this.m_playerName;
    this.m_currentSubtext = this.m_subtext;
    this.m_nameText.SetActive(true);
    this.m_useLongName = false;
    this.m_playerName.Text = string.Empty;
    this.m_nameBoneToUse = this.m_nameBone;
    if ((bool) ((Object) this.m_longNameText))
      this.m_longNameText.SetActive(false);
    this.m_missionId = GameMgr.Get().GetMissionId();
  }

  private void Update()
  {
    this.UpdateAnchor();
  }

  public void SetName(string name)
  {
    this.m_currentPlayerName.Text = name;
    if ((Object) this.m_alphaBannerSkinned != (Object) null)
      this.AdjustSkinnedBanner();
    else
      this.AdjustBanner();
  }

  private void AdjustBanner()
  {
    Vector3 worldScale = TransformUtil.ComputeWorldScale(this.m_currentPlayerName.gameObject);
    float num1 = this.FUDGE_FACTOR * worldScale.x * this.m_currentPlayerName.GetTextWorldSpaceBounds().size.x;
    float num2 = this.m_currentPlayerName.GetTextWorldSpaceBounds().size.x * worldScale.x + num1;
    float x1 = this.m_alphaBannerMiddle.GetComponent<Renderer>().bounds.size.x;
    float x2 = this.m_currentPlayerName.GetTextBounds().size.x;
    if ((Object) this.m_medalAlphaBannerMiddle != (Object) null)
    {
      MeshRenderer componentsInChild = this.m_medalAlphaBannerMiddle.GetComponentsInChildren<MeshRenderer>(true)[0];
      float x3 = componentsInChild.bounds.size.x;
      if ((double) num2 <= (double) x1)
        return;
      if (GameUtils.ShouldShowRankedMedals())
      {
        TransformUtil.SetLocalScaleX(this.m_medalAlphaBannerMiddle, x2 / x3);
        TransformUtil.SetPoint(this.m_medalAlphaBannerRight, Anchor.LEFT, componentsInChild.gameObject, Anchor.RIGHT, new Vector3(0.0f, 0.0f, 0.0f));
      }
      else
      {
        TransformUtil.SetLocalScaleX(this.m_alphaBannerMiddle, num2 / x1);
        TransformUtil.SetPoint(this.m_alphaBannerRight, Anchor.LEFT, this.m_alphaBannerMiddle, Anchor.RIGHT, new Vector3(-num1, 0.0f, 0.0f));
      }
    }
    else
    {
      if ((double) num2 <= (double) x1)
        return;
      TransformUtil.SetLocalScaleX(this.m_alphaBanner, num2 / x1);
    }
  }

  private void AdjustSkinnedBanner()
  {
    bool flag1 = false;
    bool flag2 = false;
    if (GameUtils.ShouldShowRankedMedals())
      flag1 = true;
    else if (this.ShouldShowGameIconBanner())
      flag2 = true;
    if (flag1)
    {
      float x = (float) (-(double) this.m_currentPlayerName.GetTextBounds().size.x - 10.0);
      if ((double) x > -17.0)
        x = -17f;
      Vector3 localPosition = this.m_medalBannerBone.transform.localPosition;
      this.m_medalBannerBone.transform.localPosition = new Vector3(x, localPosition.y, localPosition.z);
    }
    else if (flag2)
    {
      float x = (float) (-(double) this.m_currentPlayerName.GetTextBounds().size.x - 10.0);
      if ((double) x > -17.0)
        x = -17f;
      Vector3 localPosition = this.m_modeIconBannerBone.transform.localPosition;
      this.m_modeIconBannerBone.transform.localPosition = new Vector3(x, localPosition.y, localPosition.z);
    }
    else
    {
      float x = (float) (-(double) this.m_currentPlayerName.GetTextBounds().size.x - 1.0);
      if ((double) x > -12.0)
        x = -12f;
      Vector3 localPosition = this.m_alphaBannerBone.transform.localPosition;
      this.m_alphaBannerBone.transform.localPosition = new Vector3(x, localPosition.y, localPosition.z);
    }
  }

  public void SetSubtext(string subtext)
  {
    if ((Object) this.m_currentSubtext != (Object) null)
    {
      this.m_currentSubtext.gameObject.SetActive(true);
      this.m_currentSubtext.Text = subtext;
    }
    if (!((Object) this.m_currentPlayerName != (Object) null))
      return;
    this.m_currentPlayerName.transform.localPosition = !((Object) this.m_classBoneToUse == (Object) null) ? this.m_classBoneToUse.localPosition : this.m_nameBoneToUse.localPosition;
  }

  public void PositionNameText(bool shouldTween)
  {
    if (!this.m_shouldCenterName || (Object) this.m_currentPlayerName == (Object) null)
      return;
    if ((bool) UniversalInputManager.UsePhoneUI || !shouldTween)
      this.m_currentPlayerName.transform.position = this.m_nameBoneToUse.position;
    else
      iTween.MoveTo(this.m_currentPlayerName.gameObject, iTween.Hash((object) "position", (object) this.m_nameBoneToUse.localPosition, (object) "isLocal", (object) true, (object) "time", (object) 1f));
  }

  public void FadeOutSubtext()
  {
    if ((Object) this.m_currentSubtext == (Object) null)
      return;
    if (this.m_playerSide == Player.Side.OPPOSING && (GameUtils.IsExpansionAdventure(GameUtils.GetAdventureId(this.m_missionId)) && !GameUtils.IsClassChallengeMission(this.m_missionId)))
      this.m_shouldCenterName = false;
    else if (this.m_playerSide == Player.Side.FRIENDLY && !string.IsNullOrEmpty(GameState.Get().GetGameEntity().GetAlternatePlayerName()))
    {
      if ((Object) this.m_adventureGameModeIcon != (Object) null)
        this.m_adventureGameModeIcon.SetActive(false);
      iTween.FadeTo(this.gameObject, 0.0f, 1f);
    }
    else
      iTween.FadeTo(this.m_currentSubtext.gameObject, 0.0f, 1f);
  }

  public void FadeIn()
  {
    if ((Object) this.m_alphaBannerSkinned != (Object) null)
      iTween.FadeTo(this.m_alphaBannerSkinned.gameObject, 1f, 1f);
    else
      iTween.FadeTo(this.m_alphaBanner.gameObject, 1f, 1f);
    iTween.FadeTo(this.m_currentPlayerName.gameObject, 1f, 1f);
  }

  public void SetPlayerSide(Player.Side side)
  {
    this.m_playerSide = side;
    this.UpdateAnchor();
    this.StartCoroutine(this.UpdateName());
  }

  public Player.Side GetPlayerSide()
  {
    return this.m_playerSide;
  }

  public void Unload()
  {
    Object.DestroyImmediate((Object) this.gameObject);
  }

  public void UseLongName()
  {
    this.m_currentPlayerName = this.m_longPlayerName;
    this.m_currentSubtext = this.m_longSubtext;
    this.m_longNameText.SetActive(true);
    this.m_nameText.SetActive(false);
    this.m_useLongName = true;
  }

  public void UpdateHeroNameBanner()
  {
    this.SetName(GameState.Get().GetPlayer(this.m_playerId).GetHero().GetName());
  }

  private void UpdateAnchor()
  {
    if (!this.m_canShowModeIcons)
      OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
    else if ((bool) UniversalInputManager.UsePhoneUI)
    {
      if (this.m_playerSide == Player.Side.FRIENDLY)
        OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.BOTTOM_RIGHT, false, CanvasScaleMode.HEIGHT);
      else
        OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.BOTTOM_LEFT, false, CanvasScaleMode.HEIGHT);
    }
    else
    {
      Vector3 vector3;
      if (this.m_playerSide == Player.Side.FRIENDLY)
      {
        OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.BOTTOM_LEFT, false, CanvasScaleMode.HEIGHT);
        vector3 = new Vector3(0.0f, 5f, 22f);
      }
      else
      {
        OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.TOP_LEFT, false, CanvasScaleMode.HEIGHT);
        vector3 = new Vector3(0.0f, 5f, -10f);
      }
      this.transform.localPosition = vector3;
    }
  }

  private bool IsReady(Player p)
  {
    return p != null && p.GetName() != null && p.GetRank() != null;
  }

  [DebuggerHidden]
  private IEnumerator UpdateName()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NameBanner.\u003CUpdateName\u003Ec__IteratorDC() { \u003C\u003Ef__this = this };
  }

  public void UpdateMedalChange(MedalInfoTranslator medalInfo)
  {
    if (medalInfo == null || !medalInfo.IsDisplayable(Options.Get().GetBool(Option.IN_WILD_MODE)))
      return;
    this.m_medal.SetMedal(medalInfo, false);
  }

  private bool ShouldShowGameIconBanner()
  {
    if (this.m_playerSide == Player.Side.FRIENDLY && !(bool) UniversalInputManager.UsePhoneUI && (!GameUtils.IsPracticeMission(this.m_missionId) && !GameUtils.IsTutorialMission(this.m_missionId)))
      return !GameUtils.IsReturningPlayerMission(this.m_missionId);
    return false;
  }
}
