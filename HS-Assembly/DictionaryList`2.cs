﻿// Decompiled with JetBrains decompiler
// Type: DictionaryList`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[Serializable]
public class DictionaryList<T, U> : List<DictionaryListItem<T, U>>
{
  public U this[T key]
  {
    get
    {
      U u;
      if (!this.TryGetValue(key, out u))
        throw new KeyNotFoundException(string.Format("{0} key does not exist in ListDict.", (object) key));
      return u;
    }
    set
    {
      using (List<DictionaryListItem<T, U>>.Enumerator enumerator = this.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          DictionaryListItem<T, U> current = enumerator.Current;
          if (current.m_key.Equals((object) key))
          {
            current.m_value = value;
            return;
          }
        }
      }
      this.Add(new DictionaryListItem<T, U>()
      {
        m_key = key,
        m_value = value
      });
    }
  }

  public bool TryGetValue(T key, out U value)
  {
    using (List<DictionaryListItem<T, U>>.Enumerator enumerator = this.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DictionaryListItem<T, U> current = enumerator.Current;
        if (current.m_key.Equals((object) key))
        {
          value = current.m_value;
          return true;
        }
      }
    }
    value = default (U);
    return false;
  }
}
