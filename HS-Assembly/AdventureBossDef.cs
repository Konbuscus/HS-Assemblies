﻿// Decompiled with JetBrains decompiler
// Type: AdventureBossDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class AdventureBossDef : MonoBehaviour
{
  public Material m_CoinPortraitMaterial;
  public AdventureBossDef.IntroLinePlayTime m_IntroLinePlayTime;
  public string m_IntroLine;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_quotePrefabOverride;
  public MusicPlaylistType m_MissionMusic;

  public enum IntroLinePlayTime
  {
    MissionSelect,
    MissionStart,
  }
}
