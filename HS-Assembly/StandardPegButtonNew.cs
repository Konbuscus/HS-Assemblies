﻿// Decompiled with JetBrains decompiler
// Type: StandardPegButtonNew
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

[ExecuteInEditMode]
public class StandardPegButtonNew : PegUIElement
{
  private const float HIGHLIGHT_SCALE = 1.2f;
  private const float GRAY_FRAME_SCALE = 0.88f;
  public UberText m_buttonText;
  public ThreeSliceElement m_button;
  public ThreeSliceElement m_border;
  public ThreeSliceElement m_highlight;
  public GameObject m_upBone;
  public GameObject m_downBone;
  public float m_buttonWidth;
  public bool m_ExecuteInEditMode;
  private bool m_highlightLocked;

  public void SetText(string t)
  {
    this.m_buttonText.Text = t;
  }

  public void SetWidth(float globalWidth)
  {
    this.m_button.SetWidth(globalWidth * 0.88f);
    if ((Object) this.m_border != (Object) null)
      this.m_border.SetWidth(globalWidth);
    Quaternion rotation = this.transform.rotation;
    this.transform.rotation = Quaternion.Euler(Vector3.zero);
    Vector3 size = this.m_button.GetSize();
    Vector3 worldScale = TransformUtil.ComputeWorldScale((Component) this.transform);
    this.GetComponent<BoxCollider>().size = new Vector3(size.x / worldScale.x, size.z / worldScale.z, size.y / worldScale.y);
    this.transform.rotation = rotation;
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void Disable()
  {
    this.m_button.transform.localRotation = Quaternion.Euler(new Vector3(180f, 180f, 0.0f));
    this.SetEnabled(false);
  }

  public void Enable()
  {
    this.m_button.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 180f, 0.0f));
    this.SetEnabled(true);
  }

  public void Reset()
  {
    iTween.StopByName(this.m_button.gameObject, "rotation");
    this.m_button.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 180f, 0.0f));
  }

  public void LockHighlight()
  {
    this.m_highlight.gameObject.SetActive(true);
    this.m_highlightLocked = true;
  }

  public void UnlockHighlight()
  {
    this.m_highlight.gameObject.SetActive(false);
    this.m_highlightLocked = false;
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    if (this.m_highlightLocked)
      return;
    Hashtable args = iTween.Hash((object) "amount", (object) new Vector3(90f, 0.0f, 0.0f), (object) "time", (object) 0.5f, (object) "name", (object) "rotation");
    iTween.StopByName(this.m_button.gameObject, "rotation");
    this.m_button.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 180f, 0.0f));
    iTween.PunchRotation(this.m_button.gameObject, args);
    this.m_highlight.gameObject.SetActive(true);
    if (!((Object) SoundManager.Get() != (Object) null) || !((Object) SoundManager.Get().GetConfig() != (Object) null))
      return;
    SoundManager.Get().LoadAndPlay("Small_Mouseover");
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_button.transform.localPosition = this.m_upBone.transform.localPosition;
    if (this.m_highlightLocked)
      return;
    this.m_highlight.gameObject.SetActive(false);
  }

  protected override void OnPress()
  {
    this.m_button.transform.localPosition = this.m_downBone.transform.localPosition;
    if (!((Object) SoundManager.Get() != (Object) null) || !((Object) SoundManager.Get().GetConfig() != (Object) null))
      return;
    SoundManager.Get().LoadAndPlay("Back_Click");
  }

  protected override void OnRelease()
  {
    this.m_button.transform.localPosition = this.m_upBone.transform.localPosition;
  }
}
