﻿// Decompiled with JetBrains decompiler
// Type: Crypto
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text;

public static class Crypto
{
  public static class SHA1
  {
    public const int Length = 40;
    public const string Null = "0000000000000000000000000000000000000000";

    public static string Calc(byte[] bytes, int start, int count)
    {
      byte[] hash = System.Security.Cryptography.SHA1.Create().ComputeHash(bytes, start, count);
      StringBuilder stringBuilder = new StringBuilder();
      foreach (byte num in hash)
        stringBuilder.Append(num.ToString("x2"));
      return stringBuilder.ToString();
    }

    public static string Calc(byte[] bytes)
    {
      return Crypto.SHA1.Calc(bytes, 0, bytes.Length);
    }

    [DebuggerHidden]
    public static IEnumerator Calc(byte[] bytes, int inputCount, Action<string> hash)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new Crypto.SHA1.\u003CCalc\u003Ec__Iterator322() { bytes = bytes, inputCount = inputCount, hash = hash, \u003C\u0024\u003Ebytes = bytes, \u003C\u0024\u003EinputCount = inputCount, \u003C\u0024\u003Ehash = hash };
    }

    public static string Calc(string message)
    {
      byte[] bytes = new byte[message.Length * 2];
      Buffer.BlockCopy((Array) message.ToCharArray(), 0, (Array) bytes, 0, bytes.Length);
      return Crypto.SHA1.Calc(bytes);
    }

    public static string Calc(FileInfo path)
    {
      return Crypto.SHA1.Calc(File.ReadAllBytes(path.FullName));
    }
  }
}
