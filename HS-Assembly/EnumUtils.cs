﻿// Decompiled with JetBrains decompiler
// Type: EnumUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.ComponentModel;

public class EnumUtils
{
  private static Map<System.Type, Map<string, object>> s_enumCache = new Map<System.Type, Map<string, object>>();

  public static string GetString<T>(T enumVal)
  {
    string name = enumVal.ToString();
    DescriptionAttribute[] customAttributes = (DescriptionAttribute[]) enumVal.GetType().GetField(name).GetCustomAttributes(typeof (DescriptionAttribute), false);
    if (customAttributes.Length > 0)
      return customAttributes[0].Description;
    return name;
  }

  public static bool TryGetEnum<T>(string str, StringComparison comparisonType, out T result)
  {
    System.Type type = typeof (T);
    Map<string, object> map;
    EnumUtils.s_enumCache.TryGetValue(type, out map);
    object obj1;
    if (map != null && map.TryGetValue(str, out obj1))
    {
      result = (T) obj1;
      return true;
    }
    foreach (string name in Enum.GetNames(type))
    {
      T obj2 = (T) Enum.Parse(type, name);
      bool flag = false;
      if (name.Equals(str, comparisonType))
      {
        flag = true;
        result = obj2;
      }
      else
      {
        foreach (DescriptionAttribute customAttribute in (DescriptionAttribute[]) obj2.GetType().GetField(name).GetCustomAttributes(typeof (DescriptionAttribute), false))
        {
          if (customAttribute.Description.Equals(str, comparisonType))
          {
            flag = true;
            break;
          }
        }
      }
      if (flag)
      {
        if (map == null)
        {
          map = new Map<string, object>();
          EnumUtils.s_enumCache.Add(type, map);
        }
        if (!map.ContainsKey(str))
          map.Add(str, (object) obj2);
        result = obj2;
        return true;
      }
    }
    result = default (T);
    return false;
  }

  public static T GetEnum<T>(string str)
  {
    return EnumUtils.GetEnum<T>(str, StringComparison.Ordinal);
  }

  public static T GetEnum<T>(string str, StringComparison comparisonType)
  {
    T result;
    if (EnumUtils.TryGetEnum<T>(str, comparisonType, out result))
      return result;
    throw new ArgumentException(string.Format("EnumUtils.GetEnum() - \"{0}\" has no matching value in enum {1}", (object) str, (object) typeof (T)));
  }

  public static bool TryGetEnum<T>(string str, out T outVal)
  {
    return EnumUtils.TryGetEnum<T>(str, StringComparison.Ordinal, out outVal);
  }

  public static T Parse<T>(string str)
  {
    return (T) Enum.Parse(typeof (T), str);
  }

  public static T SafeParse<T>(string str)
  {
    try
    {
      return (T) Enum.Parse(typeof (T), str);
    }
    catch (Exception ex)
    {
      return default (T);
    }
  }

  public static bool TryCast<T>(object inVal, out T outVal)
  {
    outVal = default (T);
    try
    {
      outVal = (T) inVal;
      return true;
    }
    catch (Exception ex)
    {
      return false;
    }
  }

  public static int Length<T>()
  {
    return Enum.GetValues(typeof (T)).Length;
  }
}
