﻿// Decompiled with JetBrains decompiler
// Type: AdventurePurchaseScreen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[CustomEditClass]
public class AdventurePurchaseScreen : Store
{
  private List<AdventurePurchaseScreen.PurchaseListener> m_PurchaseListeners = new List<AdventurePurchaseScreen.PurchaseListener>();
  [CustomEditField(Sections = "UI")]
  public PegUIElement m_BuyDungeonButton;

  protected override void Awake()
  {
    base.Awake();
    this.m_buyWithMoneyButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.BuyWithMoney()));
    this.m_buyWithGoldButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.BuyWithGold()));
    this.m_BuyDungeonButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.SendToStore()));
  }

  public void AddPurchaseListener(AdventurePurchaseScreen.Purchase dlg, object userdata)
  {
    AdventurePurchaseScreen.PurchaseListener purchaseListener = new AdventurePurchaseScreen.PurchaseListener();
    purchaseListener.SetCallback(dlg);
    purchaseListener.SetUserData(userdata);
    this.m_PurchaseListeners.Add(purchaseListener);
  }

  public void RemovePurchaseListener(AdventurePurchaseScreen.Purchase dlg)
  {
    using (List<AdventurePurchaseScreen.PurchaseListener>.Enumerator enumerator = this.m_PurchaseListeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventurePurchaseScreen.PurchaseListener current = enumerator.Current;
        if ((MulticastDelegate) current.GetCallback() == (MulticastDelegate) dlg)
        {
          this.m_PurchaseListeners.Remove(current);
          break;
        }
      }
    }
  }

  private void BuyWithMoney()
  {
    this.FirePurchaseEvent(true);
  }

  private void BuyWithGold()
  {
    this.FirePurchaseEvent(true);
  }

  private void SendToStore()
  {
    this.FirePurchaseEvent(false);
  }

  private void FirePurchaseEvent(bool success)
  {
    foreach (AdventurePurchaseScreen.PurchaseListener purchaseListener in this.m_PurchaseListeners.ToArray())
      purchaseListener.Fire(success);
  }

  public class PurchaseListener : EventListener<AdventurePurchaseScreen.Purchase>
  {
    public void Fire(bool success)
    {
      this.m_callback(success, this.m_userData);
    }
  }

  public delegate void Purchase(bool success, object userdata);
}
