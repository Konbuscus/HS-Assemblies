﻿// Decompiled with JetBrains decompiler
// Type: Actor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class Actor : MonoBehaviour
{
  protected readonly Vector2 GEM_TEXTURE_OFFSET_RARE = new Vector2(0.5f, 0.0f);
  protected readonly Vector2 GEM_TEXTURE_OFFSET_EPIC = new Vector2(0.0f, 0.5f);
  protected readonly Vector2 GEM_TEXTURE_OFFSET_LEGENDARY = new Vector2(0.5f, 0.5f);
  protected readonly Vector2 GEM_TEXTURE_OFFSET_COMMON = new Vector2(0.0f, 0.0f);
  protected readonly Color GEM_COLOR_RARE = new Color(0.1529f, 0.498f, 1f);
  protected readonly Color GEM_COLOR_EPIC = new Color(0.596f, 0.1568f, 0.7333f);
  protected readonly Color GEM_COLOR_LEGENDARY = new Color(1f, 0.5333f, 0.0f);
  protected readonly Color GEM_COLOR_COMMON = new Color(0.549f, 0.549f, 0.549f);
  protected readonly Color CLASS_COLOR_GENERIC = new Color(0.7f, 0.7f, 0.7f);
  protected readonly Color CLASS_COLOR_WARLOCK = new Color(0.33f, 0.2f, 0.4f);
  protected readonly Color CLASS_COLOR_ROGUE = new Color(0.23f, 0.23f, 0.23f);
  protected readonly Color CLASS_COLOR_DRUID = new Color(0.42f, 0.29f, 0.14f);
  protected readonly Color CLASS_COLOR_SHAMAN = new Color(0.0f, 0.32f, 0.71f);
  protected readonly Color CLASS_COLOR_HUNTER = new Color(0.26f, 0.54f, 0.18f);
  protected readonly Color CLASS_COLOR_MAGE = new Color(0.44f, 0.48f, 0.69f);
  protected readonly Color CLASS_COLOR_PALADIN = new Color(0.71f, 0.49f, 0.2f);
  protected readonly Color CLASS_COLOR_PRIEST = new Color(1f, 1f, 1f);
  protected readonly Color CLASS_COLOR_WARRIOR = new Color(0.43f, 0.14f, 0.14f);
  public int m_cardFrontMatIdx = -1;
  public int m_cardBackMatIdx = -1;
  public int m_premiumRibbon = -1;
  public int m_portraitFrameMatIdx = -1;
  public int m_portraitMatIdx = -1;
  protected bool m_shown = true;
  protected ActorStateType m_actorState = ActorStateType.CARD_IDLE;
  protected int m_legacyPortraitMaterialIndex = -1;
  protected int m_legacyCardColorMaterialIndex = -1;
  protected const string WATERMARK_EXPERT1 = "Set1_Icon";
  protected const string WATERMARK_FP1 = "NaxxIcon";
  protected const string WATERMARK_GVG = "GvGIcon";
  protected const string WATERMARK_BRM = "BRMIcon";
  protected const string WATERMARK_TGT = "TGTIcon";
  protected const string WATERMARK_LOE = "LOEIcon";
  protected const string WATERMARK_OG = "OGIcon";
  protected const string WATERMARK_KARA = "KaraIcon";
  protected const string WATERMARK_GANGS = "GangsIcon";
  public GameObject m_cardMesh;
  public GameObject m_portraitMesh;
  public GameObject m_nameBannerMesh;
  public GameObject m_descriptionMesh;
  public GameObject m_descriptionTrimMesh;
  public GameObject m_rarityFrameMesh;
  public GameObject m_rarityGemMesh;
  public GameObject m_racePlateMesh;
  public GameObject m_attackObject;
  public GameObject m_healthObject;
  public GameObject m_manaObject;
  public GameObject m_racePlateObject;
  public GameObject m_cardTypeAnchorObject;
  public GameObject m_eliteObject;
  public GameObject m_classIconObject;
  public GameObject m_heroSpotLight;
  public GameObject m_glints;
  public GameObject m_armorSpellBone;
  public NestedPrefab m_multiClassBannerContainer;
  public UberText m_costTextMesh;
  public UberText m_attackTextMesh;
  public UberText m_healthTextMesh;
  public UberText m_nameTextMesh;
  public UberText m_powersTextMesh;
  public UberText m_raceTextMesh;
  public UberText m_secretText;
  public GameObject m_missingCardEffect;
  public GameObject m_ghostCardGameObject;
  [CustomEditField(T = EditType.ACTOR)]
  public string m_spellTablePrefab;
  protected Card m_card;
  protected Entity m_entity;
  protected CardDef m_cardDef;
  protected EntityDef m_entityDef;
  protected TAG_PREMIUM m_premiumType;
  protected ProjectedShadow m_projectedShadow;
  protected ActorStateMgr m_actorStateMgr;
  protected bool forceIdleState;
  protected GameObject m_rootObject;
  protected GameObject m_bones;
  protected MeshRenderer m_meshRenderer;
  protected Material m_initialPortraitMaterial;
  protected List<Material> m_lightBlendMaterials;
  protected List<UberText> m_lightBlendUberText;
  protected SpellTable m_sharedSpellTable;
  protected bool m_useSharedSpellTable;
  protected Map<SpellType, Spell> m_localSpellTable;
  protected SpellTable m_spellTable;
  protected ArmorSpell m_armorSpell;
  protected GameObject m_hiddenCardStandIn;
  protected bool m_shadowform;
  protected GhostCard.Type m_ghostCard;
  protected bool m_missingcard;
  protected bool m_armorSpellLoading;
  protected bool m_materialEffectsSeeded;
  protected Player.Side? m_cardBackSideOverride;
  protected bool m_ignoreUpdateCardback;
  protected bool isPortraitMaterialDirty;
  protected bool m_DisablePremiumPortrait;
  protected Texture m_portraitTextureOverride;
  protected MultiClassBannerTransition m_multiClassBanner;
  private ActorStateType m_actualState;
  private bool m_hideActorState;

  public virtual void Awake()
  {
    this.AssignRootObject();
    this.AssignBones();
    this.AssignMeshRenderers();
    this.AssignSpells();
  }

  private void OnEnable()
  {
    if ((UnityEngine.Object) GraphicsManager.Get() != (UnityEngine.Object) null)
      this.m_DisablePremiumPortrait = GraphicsManager.Get().isVeryLowQualityDevice();
    if (!this.isPortraitMaterialDirty)
      return;
    this.UpdateAllComponents();
  }

  private void Start()
  {
    this.Init();
  }

  public void Init()
  {
    if ((UnityEngine.Object) this.m_portraitMesh != (UnityEngine.Object) null)
      this.m_initialPortraitMaterial = RenderUtils.GetMaterial(this.m_portraitMesh, this.m_portraitMatIdx);
    else if (this.m_legacyPortraitMaterialIndex >= 0)
      this.m_initialPortraitMaterial = RenderUtils.GetMaterial((Renderer) this.m_meshRenderer, this.m_legacyPortraitMaterialIndex);
    if ((UnityEngine.Object) this.m_rootObject != (UnityEngine.Object) null)
      TransformUtil.Identity((Component) this.m_rootObject.transform);
    if ((UnityEngine.Object) this.m_actorStateMgr != (UnityEngine.Object) null)
      this.m_actorStateMgr.ChangeState(this.m_actorState);
    this.m_projectedShadow = this.GetComponent<ProjectedShadow>();
    if (this.m_shown)
      this.ShowImpl(false);
    else
      this.HideImpl(false);
  }

  public void Destroy()
  {
    if (this.m_localSpellTable != null)
    {
      using (Map<SpellType, Spell>.ValueCollection.Enumerator enumerator = this.m_localSpellTable.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Deactivate();
      }
    }
    if ((UnityEngine.Object) this.m_spellTable != (UnityEngine.Object) null)
    {
      using (List<SpellTableEntry>.Enumerator enumerator = this.m_spellTable.m_Table.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          SpellTableEntry current = enumerator.Current;
          if (!((UnityEngine.Object) current.m_Spell == (UnityEngine.Object) null))
            current.m_Spell.Deactivate();
        }
      }
    }
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  public virtual Actor Clone()
  {
    GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate((UnityEngine.Object) this.gameObject, this.transform.position, this.transform.rotation);
    Actor component = gameObject.GetComponent<Actor>();
    component.SetEntity(this.m_entity);
    component.SetEntityDef(this.m_entityDef);
    component.SetCard(this.m_card);
    component.SetPremium(this.m_premiumType);
    gameObject.transform.localScale = this.gameObject.transform.localScale;
    gameObject.transform.position = this.gameObject.transform.position;
    component.SetActorState(this.m_actorState);
    if (this.m_shown)
      component.ShowImpl(false);
    else
      component.HideImpl(false);
    return component;
  }

  public Card GetCard()
  {
    return this.m_card;
  }

  public void SetCard(Card card)
  {
    if ((UnityEngine.Object) this.m_card == (UnityEngine.Object) card)
      return;
    if ((UnityEngine.Object) card == (UnityEngine.Object) null)
    {
      this.m_card = (Card) null;
      this.transform.parent = (Transform) null;
    }
    else
    {
      this.m_card = card;
      this.transform.parent = card.transform;
      TransformUtil.Identity((Component) this.transform);
      if (!((UnityEngine.Object) this.m_rootObject != (UnityEngine.Object) null))
        return;
      TransformUtil.Identity((Component) this.m_rootObject.transform);
    }
  }

  public CardDef GetCardDef()
  {
    return this.m_cardDef;
  }

  public void SetCardDef(CardDef cardDef)
  {
    if ((UnityEngine.Object) this.m_cardDef == (UnityEngine.Object) cardDef)
      return;
    this.m_cardDef = cardDef;
    this.LoadArmorSpell();
  }

  public Entity GetEntity()
  {
    return this.m_entity;
  }

  public void SetEntity(Entity entity)
  {
    this.m_entity = entity;
    if (this.m_entity == null)
      return;
    this.SetPremium(this.m_entity.GetPremiumType());
  }

  public EntityDef GetEntityDef()
  {
    return this.m_entityDef;
  }

  public void SetEntityDef(EntityDef entityDef)
  {
    this.m_entityDef = entityDef;
  }

  public virtual void SetPremium(TAG_PREMIUM premium)
  {
    this.m_premiumType = premium;
  }

  public TAG_PREMIUM GetPremium()
  {
    return this.m_premiumType;
  }

  public TAG_CARD_SET GetCardSet()
  {
    if (this.m_entityDef == null && this.m_entity == null)
      return TAG_CARD_SET.NONE;
    return this.m_entityDef == null ? this.m_entity.GetCardSet() : this.m_entityDef.GetCardSet();
  }

  public ActorStateType GetActorStateType()
  {
    if ((UnityEngine.Object) this.m_actorStateMgr == (UnityEngine.Object) null)
      return ActorStateType.NONE;
    return this.m_actorStateMgr.GetActiveStateType();
  }

  public void ShowActorState()
  {
  }

  public void HideActorState()
  {
  }

  public void SetActorState(ActorStateType stateType)
  {
    this.m_actorState = stateType;
    if ((UnityEngine.Object) this.m_actorStateMgr == (UnityEngine.Object) null)
      return;
    if (this.forceIdleState)
      this.m_actorState = ActorStateType.CARD_IDLE;
    this.m_actorStateMgr.ChangeState(this.m_actorState);
  }

  public void ToggleForceIdle(bool bOn)
  {
    this.forceIdleState = bOn;
  }

  public void TurnOffCollider()
  {
    this.ToggleCollider(false);
  }

  public void TurnOnCollider()
  {
    this.ToggleCollider(true);
  }

  public void ToggleCollider(bool enabled)
  {
    MeshRenderer meshRenderer = this.GetMeshRenderer();
    if ((UnityEngine.Object) meshRenderer == (UnityEngine.Object) null || (UnityEngine.Object) meshRenderer.gameObject.GetComponent<Collider>() == (UnityEngine.Object) null)
      return;
    meshRenderer.gameObject.GetComponent<Collider>().enabled = enabled;
  }

  public TAG_RARITY GetRarity()
  {
    if (this.m_entityDef != null)
      return this.m_entityDef.GetRarity();
    if (this.m_entity != null)
      return this.m_entity.GetRarity();
    return TAG_RARITY.FREE;
  }

  public bool IsElite()
  {
    if (this.m_entityDef != null)
      return this.m_entityDef.IsElite();
    if (this.m_entity != null)
      return this.m_entity.IsElite();
    return false;
  }

  public bool IsMultiClass()
  {
    if (this.m_entityDef != null)
      return this.m_entityDef.IsMultiClass();
    if (this.m_entity != null)
      return this.m_entity.IsMultiClass();
    return false;
  }

  public void SetHiddenStandIn(GameObject standIn)
  {
    this.m_hiddenCardStandIn = standIn;
  }

  public GameObject GetHiddenStandIn()
  {
    return this.m_hiddenCardStandIn;
  }

  public void SetShadowform(bool shadowform)
  {
    this.m_shadowform = shadowform;
  }

  public void SetDisablePremiumPortrait(bool disable)
  {
    this.m_DisablePremiumPortrait = disable;
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  public void Show()
  {
    if (this.m_shown)
      return;
    this.m_shown = true;
    this.ShowImpl(false);
  }

  public void Show(bool ignoreSpells)
  {
    if (this.m_shown)
      return;
    this.m_shown = true;
    this.ShowImpl(ignoreSpells);
  }

  public void ShowSpellTable()
  {
    if (this.m_localSpellTable != null)
    {
      using (Map<SpellType, Spell>.ValueCollection.Enumerator enumerator = this.m_localSpellTable.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Show();
      }
    }
    if (!((UnityEngine.Object) this.m_spellTable != (UnityEngine.Object) null))
      return;
    this.m_spellTable.Show();
  }

  public void Hide()
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    this.HideImpl(false);
  }

  public void Hide(bool ignoreSpells)
  {
    if (!this.m_shown)
      return;
    this.m_shown = false;
    this.HideImpl(ignoreSpells);
  }

  public void HideSpellTable()
  {
    if (this.m_localSpellTable != null)
    {
      using (Map<SpellType, Spell>.ValueCollection.Enumerator enumerator = this.m_localSpellTable.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Spell current = enumerator.Current;
          if (current.GetSpellType() != SpellType.NONE)
            current.Hide();
        }
      }
    }
    if (!((UnityEngine.Object) this.m_spellTable != (UnityEngine.Object) null))
      return;
    this.m_spellTable.Hide();
  }

  protected virtual void ShowImpl(bool ignoreSpells)
  {
    if ((UnityEngine.Object) this.m_rootObject != (UnityEngine.Object) null)
      this.m_rootObject.SetActive(true);
    this.ShowAllText();
    this.UpdateAllComponents();
    if ((bool) ((UnityEngine.Object) this.m_projectedShadow))
      this.m_projectedShadow.enabled = true;
    if ((UnityEngine.Object) this.m_actorStateMgr != (UnityEngine.Object) null)
      this.m_actorStateMgr.ShowStateMgr();
    if (!ignoreSpells)
      this.ShowSpellTable();
    if ((UnityEngine.Object) this.m_ghostCardGameObject != (UnityEngine.Object) null)
      this.m_ghostCardGameObject.SetActive(true);
    HighlightState componentInChildren = this.GetComponentInChildren<HighlightState>();
    if (!(bool) ((UnityEngine.Object) componentInChildren))
      return;
    componentInChildren.Show();
  }

  protected virtual void HideImpl(bool ignoreSpells)
  {
    if ((UnityEngine.Object) this.m_rootObject != (UnityEngine.Object) null)
      this.m_rootObject.SetActive(false);
    if ((UnityEngine.Object) this.m_armorSpell != (UnityEngine.Object) null)
      this.m_armorSpell.Hide();
    if ((UnityEngine.Object) this.m_actorStateMgr != (UnityEngine.Object) null)
      this.m_actorStateMgr.HideStateMgr();
    if ((bool) ((UnityEngine.Object) this.m_projectedShadow))
      this.m_projectedShadow.enabled = false;
    if ((UnityEngine.Object) this.m_ghostCardGameObject != (UnityEngine.Object) null)
      this.m_ghostCardGameObject.SetActive(false);
    if (!ignoreSpells)
      this.HideSpellTable();
    if ((UnityEngine.Object) this.m_missingCardEffect != (UnityEngine.Object) null)
      this.UpdateMissingCardArt();
    HighlightState componentInChildren = this.GetComponentInChildren<HighlightState>();
    if (!(bool) ((UnityEngine.Object) componentInChildren))
      return;
    componentInChildren.Hide();
  }

  public ActorStateMgr GetActorStateMgr()
  {
    return this.m_actorStateMgr;
  }

  public Collider GetCollider()
  {
    if ((UnityEngine.Object) this.GetMeshRenderer() == (UnityEngine.Object) null)
      return (Collider) null;
    return this.GetMeshRenderer().gameObject.GetComponent<Collider>();
  }

  public GameObject GetRootObject()
  {
    return this.m_rootObject;
  }

  public MeshRenderer GetMeshRenderer()
  {
    return this.m_meshRenderer;
  }

  public GameObject GetBones()
  {
    return this.m_bones;
  }

  public UberText GetPowersText()
  {
    return this.m_powersTextMesh;
  }

  public UberText GetRaceText()
  {
    return this.m_raceTextMesh;
  }

  public UberText GetNameText()
  {
    return this.m_nameTextMesh;
  }

  public Light GetHeroSpotlight()
  {
    if ((UnityEngine.Object) this.m_heroSpotLight == (UnityEngine.Object) null)
      return (Light) null;
    return this.m_heroSpotLight.GetComponent<Light>();
  }

  public GameObject FindBone(string boneName)
  {
    if ((UnityEngine.Object) this.m_bones == (UnityEngine.Object) null)
      return (GameObject) null;
    return SceneUtils.FindChildBySubstring(this.m_bones, boneName);
  }

  public GameObject GetCardTypeBannerAnchor()
  {
    if ((UnityEngine.Object) this.m_cardTypeAnchorObject == (UnityEngine.Object) null)
      return this.gameObject;
    return this.m_cardTypeAnchorObject;
  }

  public UberText GetAttackText()
  {
    return this.m_attackTextMesh;
  }

  public GameObject GetAttackTextObject()
  {
    if ((UnityEngine.Object) this.m_attackTextMesh == (UnityEngine.Object) null)
      return (GameObject) null;
    return this.m_attackTextMesh.gameObject;
  }

  public GemObject GetAttackObject()
  {
    if ((UnityEngine.Object) this.m_attackObject == (UnityEngine.Object) null)
      return (GemObject) null;
    return this.m_attackObject.GetComponent<GemObject>();
  }

  public GemObject GetHealthObject()
  {
    if ((UnityEngine.Object) this.m_healthObject == (UnityEngine.Object) null)
      return (GemObject) null;
    return this.m_healthObject.GetComponent<GemObject>();
  }

  public UberText GetHealthText()
  {
    return this.m_healthTextMesh;
  }

  public GameObject GetHealthTextObject()
  {
    if ((UnityEngine.Object) this.m_healthTextMesh == (UnityEngine.Object) null)
      return (GameObject) null;
    return this.m_healthTextMesh.gameObject;
  }

  public UberText GetCostText()
  {
    if ((UnityEngine.Object) this.m_costTextMesh == (UnityEngine.Object) null)
      return (UberText) null;
    return this.m_costTextMesh;
  }

  public GameObject GetCostTextObject()
  {
    if ((UnityEngine.Object) this.m_costTextMesh == (UnityEngine.Object) null)
      return (GameObject) null;
    return this.m_costTextMesh.gameObject;
  }

  public UberText GetSecretText()
  {
    return this.m_secretText;
  }

  public void UpdateAllComponents()
  {
    this.UpdateTextComponents();
    this.UpdateMaterials();
    this.UpdateTextures();
    this.UpdateCardBack();
    this.UpdateMeshComponents();
    this.UpdateRootObjectSpellComponents();
    this.UpdateMissingCardArt();
    this.UpdateGhostCardEffect();
  }

  public bool MissingCardEffect()
  {
    if (!(bool) ((UnityEngine.Object) this.m_missingCardEffect) || !(bool) ((UnityEngine.Object) this.m_missingCardEffect.GetComponent<RenderToTexture>()))
      return false;
    this.m_missingcard = true;
    this.UpdateAllComponents();
    return true;
  }

  public void DisableMissingCardEffect()
  {
    this.m_missingcard = false;
    if (!(bool) ((UnityEngine.Object) this.m_missingCardEffect))
      return;
    RenderToTexture component = this.m_missingCardEffect.GetComponent<RenderToTexture>();
    if ((bool) ((UnityEngine.Object) component))
      component.enabled = false;
    this.MaterialShaderAnimation(true);
  }

  public void UpdateMissingCardArt()
  {
    if (!this.m_missingcard || (UnityEngine.Object) this.m_missingCardEffect == (UnityEngine.Object) null)
      return;
    RenderToTexture component = this.m_missingCardEffect.GetComponent<RenderToTexture>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    if (this.m_rootObject.activeSelf)
    {
      this.MaterialShaderAnimation(false);
      if (this.GetPremium() == TAG_PREMIUM.GOLDEN)
        component.m_Material.color = !CollectionManager.Get().IsShowingWildTheming((CollectionDeck) null) ? new Color(0.867f, 0.675f, 0.22f, 0.53f) : new Color(0.518f, 0.361f, 0.0f, 0.68f);
      component.enabled = true;
      component.Show(true);
    }
    else
      component.Hide();
  }

  public void SetMissingCardMaterial(Material missingCardMat)
  {
    if ((UnityEngine.Object) this.m_missingCardEffect == (UnityEngine.Object) null || (UnityEngine.Object) missingCardMat == (UnityEngine.Object) null)
      return;
    RenderToTexture component = this.m_missingCardEffect.GetComponent<RenderToTexture>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || !this.m_rootObject.activeSelf)
      return;
    component.m_Material = missingCardMat;
    this.MaterialShaderAnimation(false);
    if (!component.enabled)
      return;
    component.Render();
  }

  public bool isMissingCard()
  {
    if ((UnityEngine.Object) this.m_missingCardEffect == (UnityEngine.Object) null)
      return false;
    RenderToTexture component = this.m_missingCardEffect.GetComponent<RenderToTexture>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return false;
    return component.enabled;
  }

  public void GhostCardEffect(GhostCard.Type ghostType)
  {
    if (this.m_ghostCard == ghostType)
      return;
    this.m_ghostCard = ghostType;
    this.UpdateAllComponents();
  }

  private void UpdateGhostCardEffect()
  {
    if ((UnityEngine.Object) this.m_ghostCardGameObject == (UnityEngine.Object) null)
      return;
    GhostCard component = this.m_ghostCardGameObject.GetComponent<GhostCard>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    if (this.m_ghostCard != GhostCard.Type.NONE)
    {
      component.SetGhostType(this.m_ghostCard);
      component.RenderGhostCard();
    }
    else
      component.DisableGhost();
  }

  public bool isGhostCard()
  {
    if (this.m_ghostCard != GhostCard.Type.NONE)
      return (bool) ((UnityEngine.Object) this.m_ghostCardGameObject);
    return false;
  }

  public void UpdateMaterials()
  {
    this.m_lightBlendMaterials = (List<Material>) null;
    this.m_lightBlendUberText = (List<UberText>) null;
    if (this.gameObject.activeInHierarchy)
      this.StartCoroutine(this.UpdatePortraitMaterials());
    else
      this.isPortraitMaterialDirty = true;
  }

  public void OverrideAllMeshMaterials(Material material)
  {
    if ((UnityEngine.Object) this.m_rootObject == (UnityEngine.Object) null)
      return;
    this.RecursivelyReplaceMaterialsList(this.m_rootObject.transform, material);
  }

  public void SetUnlit()
  {
    this.SetLightingBlend(0.0f);
  }

  public void SetLit()
  {
    this.SetLightingBlend(1f);
  }

  private void SetLightingBlend(float value)
  {
    foreach (Renderer componentsInChild in this.GetComponentsInChildren<Renderer>(true))
    {
      foreach (Material material in componentsInChild.materials)
      {
        if (!((UnityEngine.Object) material == (UnityEngine.Object) null) && material.HasProperty("_LightingBlend"))
          material.SetFloat("_LightingBlend", value);
      }
    }
    foreach (UberText componentsInChild in this.GetComponentsInChildren<UberText>(true))
      componentsInChild.AmbientLightBlend = value;
  }

  public void SetLightBlend(float blendValue)
  {
    if (this.m_lightBlendMaterials == null)
    {
      this.m_lightBlendMaterials = new List<Material>();
      foreach (Renderer componentsInChild in this.GetComponentsInChildren<Renderer>())
      {
        foreach (Material material in componentsInChild.materials)
        {
          if (!((UnityEngine.Object) material == (UnityEngine.Object) null) && material.HasProperty("_LightingBlend"))
            this.m_lightBlendMaterials.Add(material);
        }
      }
      this.m_lightBlendUberText = new List<UberText>();
      foreach (UberText componentsInChild in this.GetComponentsInChildren<UberText>())
        this.m_lightBlendUberText.Add(componentsInChild);
    }
    using (List<Material>.Enumerator enumerator = this.m_lightBlendMaterials.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Material current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null && current.HasProperty("_LightingBlend"))
          current.SetFloat("_LightingBlend", blendValue);
      }
    }
    using (List<UberText>.Enumerator enumerator = this.m_lightBlendUberText.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UberText current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null)
          current.AmbientLightBlend = blendValue;
      }
    }
  }

  public void ReleasePortrait()
  {
    this.SetPortraitMaterial((Material) null);
    this.SetPortraitTexture((Texture) null);
    this.SetPortraitTextureOverride((Texture) null);
    this.m_lightBlendMaterials = (List<Material>) null;
    this.m_lightBlendUberText = (List<UberText>) null;
  }

  private void RecursivelyReplaceMaterialsList(Transform transformToRecurse, Material newMaterialPrefab)
  {
    bool flag = true;
    if ((UnityEngine.Object) transformToRecurse.GetComponent<MaterialReplacementExclude>() != (UnityEngine.Object) null)
      flag = false;
    else if ((UnityEngine.Object) transformToRecurse.GetComponent<UberText>() != (UnityEngine.Object) null)
      flag = false;
    else if ((UnityEngine.Object) transformToRecurse.GetComponent<Renderer>() == (UnityEngine.Object) null)
      flag = false;
    if (flag)
      this.ReplaceMaterialsList(transformToRecurse.GetComponent<Renderer>(), newMaterialPrefab);
    IEnumerator enumerator = transformToRecurse.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
        this.RecursivelyReplaceMaterialsList((Transform) enumerator.Current, newMaterialPrefab);
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    this.m_lightBlendMaterials = (List<Material>) null;
    this.m_lightBlendUberText = (List<UberText>) null;
  }

  private void ReplaceMaterialsList(Renderer renderer, Material newMaterialPrefab)
  {
    Material[] materialArray = new Material[renderer.materials.Length];
    for (int index = 0; index < renderer.materials.Length; ++index)
    {
      Material material = renderer.materials[index];
      materialArray[index] = this.CreateReplacementMaterial(material, newMaterialPrefab);
    }
    renderer.materials = materialArray;
    this.m_lightBlendMaterials = (List<Material>) null;
    this.m_lightBlendUberText = (List<UberText>) null;
    if ((UnityEngine.Object) renderer != (UnityEngine.Object) this.m_meshRenderer)
      return;
    this.UpdatePortraitTexture();
  }

  private Material CreateReplacementMaterial(Material oldMaterial, Material newMaterialPrefab)
  {
    Material material = UnityEngine.Object.Instantiate<Material>(newMaterialPrefab);
    material.mainTexture = oldMaterial.mainTexture;
    return material;
  }

  public void SeedMaterialEffects()
  {
    if (this.m_materialEffectsSeeded)
      return;
    this.m_materialEffectsSeeded = true;
    Renderer[] componentsInChildren = this.GetComponentsInChildren<Renderer>();
    float num = UnityEngine.Random.Range(0.0f, 2f);
    foreach (Renderer renderer in componentsInChildren)
    {
      if (renderer.sharedMaterials.Length == 1)
      {
        if (renderer.material.HasProperty("_Seed") && (double) renderer.material.GetFloat("_Seed") == 0.0)
          renderer.material.SetFloat("_Seed", num);
      }
      else
      {
        Material[] materials = renderer.materials;
        if (materials != null && materials.Length != 0)
        {
          foreach (Material material in materials)
          {
            if (!((UnityEngine.Object) material == (UnityEngine.Object) null) && material.HasProperty("_Seed") && (double) material.GetFloat("_Seed") == 0.0)
              material.SetFloat("_Seed", num);
          }
        }
      }
    }
  }

  public void MaterialShaderAnimation(bool animationEnabled)
  {
    float num = 0.0f;
    if (animationEnabled)
      num = 1f;
    foreach (Renderer componentsInChild in this.GetComponentsInChildren<Renderer>(true))
    {
      foreach (Material sharedMaterial in componentsInChild.sharedMaterials)
      {
        if (!((UnityEngine.Object) sharedMaterial == (UnityEngine.Object) null) && sharedMaterial.HasProperty("_TimeScale"))
          sharedMaterial.SetFloat("_TimeScale", num);
      }
    }
  }

  public Player.Side GetCardBackSide()
  {
    if (this.m_cardBackSideOverride.HasValue)
      return this.m_cardBackSideOverride.Value;
    if (this.m_entity == null)
      return Player.Side.FRIENDLY;
    Player controller = this.m_entity.GetController();
    if (controller == null)
      return Player.Side.FRIENDLY;
    return controller.GetSide();
  }

  public Player.Side? GetCardBackSideOverride()
  {
    return this.m_cardBackSideOverride;
  }

  public void SetCardBackSideOverride(Player.Side? sideOverride)
  {
    this.m_cardBackSideOverride = sideOverride;
  }

  public bool GetCardbackUpdateIgnore()
  {
    return this.m_ignoreUpdateCardback;
  }

  public void SetCardbackUpdateIgnore(bool ignoreUpdate)
  {
    this.m_ignoreUpdateCardback = ignoreUpdate;
  }

  public void UpdateCardBack()
  {
    if (this.m_ignoreUpdateCardback)
      return;
    CardBackManager cardBackManager = CardBackManager.Get();
    if ((UnityEngine.Object) cardBackManager == (UnityEngine.Object) null)
      return;
    bool friendlySide = this.GetCardBackSide() == Player.Side.FRIENDLY;
    this.UpdateCardBackDisplay(friendlySide);
    this.UpdateCardBackDragEffect();
    if ((UnityEngine.Object) this.m_cardMesh == (UnityEngine.Object) null || this.m_cardBackMatIdx < 0)
      return;
    cardBackManager.SetCardBackTexture(this.m_cardMesh.GetComponent<Renderer>(), this.m_cardBackMatIdx, friendlySide);
  }

  private void UpdateCardBackDragEffect()
  {
    if ((UnityEngine.Object) SceneMgr.Get() == (UnityEngine.Object) null || SceneMgr.Get().GetMode() != SceneMgr.Mode.GAMEPLAY)
      return;
    CardBackDragEffect componentInChildren = this.GetComponentInChildren<CardBackDragEffect>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
      return;
    componentInChildren.SetEffect();
  }

  private void UpdateCardBackDisplay(bool friendlySide)
  {
    CardBackDisplay componentInChildren = this.GetComponentInChildren<CardBackDisplay>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
      return;
    componentInChildren.SetCardBack(friendlySide);
  }

  private void UpdateTextures()
  {
    this.UpdatePortraitTexture();
  }

  public void UpdatePortraitTexture()
  {
    if ((UnityEngine.Object) this.m_portraitTextureOverride != (UnityEngine.Object) null)
    {
      this.SetPortraitTexture(this.m_portraitTextureOverride);
    }
    else
    {
      if (!((UnityEngine.Object) this.m_cardDef != (UnityEngine.Object) null))
        return;
      this.SetPortraitTexture(this.m_cardDef.GetPortraitTexture());
    }
  }

  public void SetPortraitTexture(Texture texture)
  {
    if ((UnityEngine.Object) this.m_cardDef != (UnityEngine.Object) null && !this.m_DisablePremiumPortrait && (this.m_premiumType == TAG_PREMIUM.GOLDEN || this.m_cardDef.m_AlwaysRenderPremiumPortrait) && (UnityEngine.Object) this.m_cardDef.GetPremiumPortraitMaterial() != (UnityEngine.Object) null)
      return;
    Material portraitMaterial = this.GetPortraitMaterial();
    if ((UnityEngine.Object) portraitMaterial == (UnityEngine.Object) null)
      return;
    portraitMaterial.mainTexture = texture;
  }

  public void SetPortraitTextureOverride(Texture portrait)
  {
    this.m_portraitTextureOverride = portrait;
    this.UpdatePortraitTexture();
  }

  public Texture GetPortraitTexture()
  {
    Material portraitMaterial = this.GetPortraitMaterial();
    if ((UnityEngine.Object) portraitMaterial == (UnityEngine.Object) null)
      return (Texture) null;
    return portraitMaterial.mainTexture;
  }

  [DebuggerHidden]
  private IEnumerator UpdatePortraitMaterials()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Actor.\u003CUpdatePortraitMaterials\u003Ec__Iterator32() { \u003C\u003Ef__this = this };
  }

  public void SetPortraitMaterial(Material material)
  {
    if ((UnityEngine.Object) material == (UnityEngine.Object) null)
      return;
    if ((UnityEngine.Object) this.m_portraitMesh != (UnityEngine.Object) null && this.m_portraitMatIdx > -1)
    {
      Material material1 = RenderUtils.GetMaterial(this.m_portraitMesh, this.m_portraitMatIdx);
      if ((UnityEngine.Object) material1.mainTexture == (UnityEngine.Object) material.mainTexture && (UnityEngine.Object) material1.shader == (UnityEngine.Object) material.shader)
        return;
      if ((UnityEngine.Object) material == (UnityEngine.Object) null)
        RenderUtils.SetMaterial(this.m_portraitMesh, this.m_portraitMatIdx, this.m_initialPortraitMaterial);
      else
        RenderUtils.SetMaterial(this.m_portraitMesh, this.m_portraitMatIdx, material);
      if (this.m_entity == null)
        return;
      float num = 0.0f;
      if (this.m_entity.GetZone() == TAG_ZONE.PLAY)
        num = 1f;
      foreach (Material material2 in this.m_portraitMesh.GetComponent<Renderer>().materials)
      {
        if (material2.HasProperty("_LightingBlend"))
          material2.SetFloat("_LightingBlend", num);
        if (material2.HasProperty("_Seed") && (double) material2.GetFloat("_Seed") == 0.0)
          material2.SetFloat("_Seed", UnityEngine.Random.Range(0.0f, 2f));
      }
    }
    else
    {
      if (this.m_legacyPortraitMaterialIndex < 0 || (UnityEngine.Object) RenderUtils.GetMaterial((Renderer) this.m_meshRenderer, this.m_legacyPortraitMaterialIndex) == (UnityEngine.Object) material)
        return;
      RenderUtils.SetMaterial((Renderer) this.m_meshRenderer, this.m_legacyPortraitMaterialIndex, material);
    }
  }

  public GameObject GetPortraitMesh()
  {
    return this.m_portraitMesh;
  }

  protected virtual Material GetPortraitMaterial()
  {
    if ((UnityEngine.Object) this.m_portraitMesh != (UnityEngine.Object) null && 0 <= this.m_portraitMatIdx && this.m_portraitMatIdx < this.m_portraitMesh.GetComponent<Renderer>().materials.Length)
      return this.m_portraitMesh.GetComponent<Renderer>().materials[this.m_portraitMatIdx];
    if (this.m_legacyPortraitMaterialIndex >= 0)
      return this.m_meshRenderer.materials[this.m_legacyPortraitMaterialIndex];
    return (Material) null;
  }

  public virtual void UpdateTextComponents()
  {
    if (this.m_entityDef != null)
      this.UpdateTextComponentsDef(this.m_entityDef);
    else
      this.UpdateTextComponents(this.m_entity);
  }

  public virtual void UpdateTextComponentsDef(EntityDef entityDef)
  {
    if (entityDef == null)
      return;
    if ((UnityEngine.Object) this.m_costTextMesh != (UnityEngine.Object) null)
      this.m_costTextMesh.Text = !entityDef.HasTag(GAME_TAG.HIDE_STATS) ? (!entityDef.HasTriggerVisual() || !entityDef.IsHeroPower() ? Convert.ToString(entityDef.GetTag(GAME_TAG.COST)) : string.Empty) : string.Empty;
    int tag = entityDef.GetTag(GAME_TAG.ATK);
    if ((UnityEngine.Object) this.m_attackTextMesh != (UnityEngine.Object) null && entityDef.HasTag(GAME_TAG.HIDE_STATS))
    {
      this.m_attackTextMesh.Text = string.Empty;
      this.m_attackTextMesh.gameObject.SetActive(false);
      GemObject componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<GemObject>(this.m_attackTextMesh.gameObject);
      if ((UnityEngine.Object) componentInThisOrParents != (UnityEngine.Object) null)
      {
        componentInThisOrParents.Hide();
        componentInThisOrParents.SetHideNumberFlag(true);
      }
    }
    else if (entityDef.IsHero())
    {
      if (tag == 0)
      {
        if ((UnityEngine.Object) this.m_attackObject != (UnityEngine.Object) null && this.m_attackObject.activeSelf)
          this.m_attackObject.SetActive(false);
        if ((UnityEngine.Object) this.m_attackTextMesh != (UnityEngine.Object) null)
          this.m_attackTextMesh.Text = string.Empty;
      }
      else
      {
        if ((UnityEngine.Object) this.m_attackObject != (UnityEngine.Object) null && !this.m_attackObject.activeSelf)
          this.m_attackObject.SetActive(true);
        if ((UnityEngine.Object) this.m_attackTextMesh != (UnityEngine.Object) null)
          this.m_attackTextMesh.Text = Convert.ToString(tag);
      }
    }
    else if ((UnityEngine.Object) this.m_attackTextMesh != (UnityEngine.Object) null)
      this.m_attackTextMesh.Text = Convert.ToString(tag);
    if ((UnityEngine.Object) this.m_healthTextMesh != (UnityEngine.Object) null)
    {
      if (entityDef.HasTag(GAME_TAG.HIDE_STATS))
      {
        this.m_healthTextMesh.Text = string.Empty;
        this.m_healthTextMesh.gameObject.SetActive(false);
        GemObject componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<GemObject>(this.m_healthTextMesh.gameObject);
        if ((UnityEngine.Object) componentInThisOrParents != (UnityEngine.Object) null)
        {
          componentInThisOrParents.Hide();
          componentInThisOrParents.SetHideNumberFlag(true);
        }
      }
      else
        this.m_healthTextMesh.Text = !entityDef.IsWeapon() ? Convert.ToString(entityDef.GetTag(GAME_TAG.HEALTH)) : Convert.ToString(entityDef.GetTag(GAME_TAG.DURABILITY));
    }
    this.UpdateNameText();
    this.UpdatePowersText();
    this.UpdateRace(entityDef.GetRaceText());
    this.UpdateSecretText();
  }

  public void UpdateMinionStatsImmediately()
  {
    if (this.m_entity == null || !this.m_entity.IsMinion() || this.m_entity.HasTag(GAME_TAG.HIDE_STATS))
      return;
    if ((UnityEngine.Object) this.m_attackTextMesh != (UnityEngine.Object) null)
    {
      this.UpdateTextColorToGreenOrWhite(this.m_attackTextMesh, this.m_entity.GetDefATK(), this.m_entity.GetATK());
      this.m_attackTextMesh.Text = Convert.ToString(this.m_entity.GetATK());
    }
    if (!((UnityEngine.Object) this.m_healthTextMesh != (UnityEngine.Object) null))
      return;
    int health = this.m_entity.GetHealth();
    int defHealth = this.m_entity.GetDefHealth();
    int num = health - this.m_entity.GetDamage();
    if (this.m_entity.GetDamage() > 0)
      this.UpdateTextColor(this.m_healthTextMesh, health, num);
    else if (health > defHealth)
      this.UpdateTextColor(this.m_healthTextMesh, defHealth, num);
    else
      this.UpdateTextColor(this.m_healthTextMesh, num, num);
    this.m_healthTextMesh.Text = Convert.ToString(num);
  }

  public virtual void UpdateTextComponents(Entity entity)
  {
    if (entity == null)
      return;
    if ((UnityEngine.Object) this.m_costTextMesh != (UnityEngine.Object) null)
    {
      if (this.m_entity.HasTag(GAME_TAG.HIDE_STATS))
      {
        this.UpdateNumberText(this.m_costTextMesh, string.Empty, false);
      }
      else
      {
        if (this.m_entity.IsSecret() && this.m_entity.IsHidden() && this.m_entity.IsControlledByConcealedPlayer())
          this.m_costTextMesh.TextColor = Color.white;
        else
          this.UpdateTextColor(this.m_costTextMesh, entity.GetDefCost(), entity.GetCost(), true);
        if (this.m_entity.HasTriggerVisual() && this.m_entity.IsHeroPower())
          this.UpdateNumberText(this.m_costTextMesh, string.Empty, true);
        else
          this.UpdateNumberText(this.m_costTextMesh, Convert.ToString(entity.GetCost()));
      }
    }
    if ((UnityEngine.Object) this.m_attackTextMesh != (UnityEngine.Object) null)
    {
      if (entity.HasTag(GAME_TAG.HIDE_STATS))
        this.UpdateNumberText(this.m_attackTextMesh, string.Empty, true);
      else if (entity.IsHero())
      {
        int atk = entity.GetATK();
        if (atk == 0)
          this.UpdateNumberText(this.m_attackTextMesh, string.Empty, true);
        else
          this.UpdateNumberText(this.m_attackTextMesh, Convert.ToString(atk));
      }
      else
      {
        this.UpdateTextColorToGreenOrWhite(this.m_attackTextMesh, entity.GetDefATK(), entity.GetATK());
        this.UpdateNumberText(this.m_attackTextMesh, Convert.ToString(entity.GetATK()));
      }
    }
    if ((UnityEngine.Object) this.m_healthTextMesh != (UnityEngine.Object) null && (!entity.IsHero() || entity.GetZone() != TAG_ZONE.GRAVEYARD))
    {
      if (entity.HasTag(GAME_TAG.HIDE_STATS))
      {
        this.UpdateNumberText(this.m_healthTextMesh, string.Empty, true);
      }
      else
      {
        int defNumber1;
        int defNumber2;
        if (entity.IsWeapon())
        {
          defNumber1 = entity.GetDurability();
          defNumber2 = entity.GetDefDurability();
        }
        else
        {
          defNumber1 = entity.GetHealth();
          defNumber2 = entity.GetDefHealth();
        }
        int num = defNumber1 - entity.GetDamage();
        if (entity.GetDamage() > 0)
          this.UpdateTextColor(this.m_healthTextMesh, defNumber1, num);
        else if (defNumber1 > defNumber2)
          this.UpdateTextColor(this.m_healthTextMesh, defNumber2, num);
        else
          this.UpdateTextColor(this.m_healthTextMesh, num, num);
        this.UpdateNumberText(this.m_healthTextMesh, Convert.ToString(num));
      }
    }
    this.UpdateNameText();
    this.UpdatePowersText();
    this.UpdateRace(entity.GetRaceText());
    this.UpdateSecretText();
  }

  public void UpdatePowersText()
  {
    if ((UnityEngine.Object) this.m_powersTextMesh == (UnityEngine.Object) null)
      return;
    string text;
    if (this.ShouldUseEntityDefForPowersText())
    {
      text = this.m_entityDef.GetCardTextInHand();
    }
    else
    {
      text = !this.m_entity.IsSecret() || !this.m_entity.IsHidden() || !this.m_entity.IsControlledByConcealedPlayer() ? (!this.m_entity.IsHistoryDupe() ? this.m_entity.GetCardTextInHand() : this.m_entity.GetCardTextInHistory()) : GameStrings.Get("GAMEPLAY_SECRET_DESC");
      if (GameState.Get() != null && GameState.Get().GetGameEntity() != null)
        text = GameState.Get().GetGameEntity().UpdateCardText(this.m_card, this, text);
    }
    this.UpdateText(this.m_powersTextMesh, text);
  }

  private bool ShouldUseEntityDefForPowersText()
  {
    return this.m_entityDef != null && (this.m_entity == null || !((UnityEngine.Object) this.m_entity.GetCardDef() != (UnityEngine.Object) null) || !this.m_entity.GetCardDef().GetCardTextBuilder().ShouldUseEntityForTextInPlay());
  }

  private void UpdateNumberText(UberText textMesh, string newText)
  {
    this.UpdateNumberText(textMesh, newText, false);
  }

  private void UpdateNumberText(UberText textMesh, string newText, bool shouldHide)
  {
    GemObject componentInThisOrParents = SceneUtils.FindComponentInThisOrParents<GemObject>(textMesh.gameObject);
    if ((UnityEngine.Object) componentInThisOrParents != (UnityEngine.Object) null)
    {
      if (!componentInThisOrParents.IsNumberHidden())
      {
        if (shouldHide)
        {
          textMesh.gameObject.SetActive(false);
          if ((UnityEngine.Object) this.GetHistoryCard() != (UnityEngine.Object) null || (UnityEngine.Object) this.GetHistoryChildCard() != (UnityEngine.Object) null)
            componentInThisOrParents.Hide();
          else
            componentInThisOrParents.ScaleToZero();
        }
        else if (textMesh.Text != newText)
          componentInThisOrParents.Jiggle();
      }
      else if (!shouldHide)
      {
        textMesh.gameObject.SetActive(true);
        componentInThisOrParents.SetToZeroThenEnlarge();
      }
      componentInThisOrParents.Initialize();
      componentInThisOrParents.SetHideNumberFlag(shouldHide);
    }
    textMesh.Text = newText;
  }

  private void UpdateNameText()
  {
    if ((UnityEngine.Object) this.m_nameTextMesh == (UnityEngine.Object) null)
      return;
    bool flag = false;
    string name;
    if (this.m_entityDef != null)
    {
      name = this.m_entityDef.GetName();
    }
    else
    {
      flag = this.m_entity.IsSecret() && this.m_entity.IsHidden() && this.m_entity.IsControlledByConcealedPlayer();
      name = this.m_entity.GetName();
    }
    if (flag)
    {
      if (GameState.Get().GetGameEntity().ShouldUseSecretClassNames())
      {
        switch (this.m_entity.GetClass())
        {
          case TAG_CLASS.HUNTER:
            name = GameStrings.Get("GAMEPLAY_SECRET_NAME_HUNTER");
            break;
          case TAG_CLASS.MAGE:
            name = GameStrings.Get("GAMEPLAY_SECRET_NAME_MAGE");
            break;
          case TAG_CLASS.PALADIN:
            name = GameStrings.Get("GAMEPLAY_SECRET_NAME_PALADIN");
            break;
          default:
            name = GameStrings.Get("GAMEPLAY_SECRET_NAME");
            break;
        }
      }
      else
        name = GameStrings.Get("GAMEPLAY_SECRET_NAME");
    }
    this.UpdateText(this.m_nameTextMesh, name);
  }

  private void UpdateSecretText()
  {
    if (!(bool) ((UnityEngine.Object) this.m_secretText))
      return;
    string text = "?";
    if ((bool) UniversalInputManager.UsePhoneUI && this.m_entity != null)
    {
      TransformUtil.SetLocalPosZ((Component) this.m_secretText, -0.01f);
      Player controller = this.m_entity.GetController();
      if (controller != null)
      {
        ZoneSecret secretZone = controller.GetSecretZone();
        if ((bool) ((UnityEngine.Object) secretZone))
        {
          int cardCount = secretZone.GetCardCount();
          if (cardCount > 1)
          {
            text = string.Format("{0}", (object) cardCount);
            TransformUtil.SetLocalPosZ((Component) this.m_secretText, -0.03f);
          }
        }
      }
      Transform child = this.m_secretText.transform.parent.FindChild("Secret_mesh");
      if ((UnityEngine.Object) child != (UnityEngine.Object) null && (UnityEngine.Object) child.gameObject != (UnityEngine.Object) null)
      {
        SphereCollider component = child.gameObject.GetComponent<SphereCollider>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          component.radius = 0.5f;
      }
    }
    this.UpdateText(this.m_secretText, text);
  }

  private void UpdateText(UberText uberTextMesh, string text)
  {
    if ((UnityEngine.Object) uberTextMesh == (UnityEngine.Object) null)
      return;
    uberTextMesh.Text = text;
  }

  private void UpdateTextColor(UberText originalMesh, int defNumber, int currentNumber)
  {
    this.UpdateTextColor(originalMesh, defNumber, currentNumber, false);
  }

  private void UpdateTextColor(UberText uberTextMesh, int defNumber, int currentNumber, bool higherIsBetter)
  {
    if (defNumber > currentNumber && higherIsBetter || defNumber < currentNumber && !higherIsBetter)
      uberTextMesh.TextColor = Color.green;
    else if (defNumber < currentNumber && higherIsBetter || defNumber > currentNumber && !higherIsBetter)
    {
      if ((bool) UniversalInputManager.UsePhoneUI)
        uberTextMesh.TextColor = new Color(1f, 0.1960784f, 0.1960784f);
      else
        uberTextMesh.TextColor = Color.red;
    }
    else
    {
      if (defNumber != currentNumber)
        return;
      uberTextMesh.TextColor = Color.white;
    }
  }

  private void UpdateTextColorToGreenOrWhite(UberText uberTextMesh, int defNumber, int currentNumber)
  {
    if (defNumber < currentNumber)
      uberTextMesh.TextColor = Color.green;
    else
      uberTextMesh.TextColor = Color.white;
  }

  private void DisableTextMesh(UberText mesh)
  {
    if ((UnityEngine.Object) mesh == (UnityEngine.Object) null)
      return;
    mesh.gameObject.SetActive(false);
  }

  public void OverrideNameText(UberText newText)
  {
    if ((UnityEngine.Object) this.m_nameTextMesh != (UnityEngine.Object) null)
      this.m_nameTextMesh.gameObject.SetActive(false);
    this.m_nameTextMesh = newText;
    this.UpdateNameText();
    if (!this.m_shown || !((UnityEngine.Object) newText != (UnityEngine.Object) null))
      return;
    newText.gameObject.SetActive(true);
  }

  public void HideAllText()
  {
    this.ToggleTextVisibility(false);
  }

  public void ShowAllText()
  {
    this.ToggleTextVisibility(true);
  }

  private void ToggleTextVisibility(bool bOn)
  {
    if ((UnityEngine.Object) this.m_healthTextMesh != (UnityEngine.Object) null)
      this.m_healthTextMesh.gameObject.SetActive(bOn);
    if ((UnityEngine.Object) this.m_attackTextMesh != (UnityEngine.Object) null)
      this.m_attackTextMesh.gameObject.SetActive(bOn);
    if ((UnityEngine.Object) this.m_nameTextMesh != (UnityEngine.Object) null)
    {
      this.m_nameTextMesh.gameObject.SetActive(bOn);
      if ((bool) ((UnityEngine.Object) this.m_nameTextMesh.RenderOnObject))
        this.m_nameTextMesh.RenderOnObject.GetComponent<Renderer>().enabled = bOn;
    }
    if ((UnityEngine.Object) this.m_powersTextMesh != (UnityEngine.Object) null)
      this.m_powersTextMesh.gameObject.SetActive(bOn);
    if ((UnityEngine.Object) this.m_costTextMesh != (UnityEngine.Object) null)
      this.m_costTextMesh.gameObject.SetActive(bOn);
    if ((UnityEngine.Object) this.m_raceTextMesh != (UnityEngine.Object) null)
      this.m_raceTextMesh.gameObject.SetActive(bOn);
    if (!(bool) ((UnityEngine.Object) this.m_secretText))
      return;
    this.m_secretText.gameObject.SetActive(bOn);
  }

  public void ContactShadow(bool visible)
  {
    string tag1 = "FakeShadow";
    string tag2 = "FakeShadowUnique";
    GameObject childByTag1 = SceneUtils.FindChildByTag(this.gameObject, tag1);
    GameObject childByTag2 = SceneUtils.FindChildByTag(this.gameObject, tag2);
    if (visible)
    {
      if (this.IsElite())
      {
        if ((UnityEngine.Object) childByTag1 != (UnityEngine.Object) null)
          childByTag1.GetComponent<Renderer>().enabled = false;
        if (!((UnityEngine.Object) childByTag2 != (UnityEngine.Object) null))
          return;
        childByTag2.GetComponent<Renderer>().enabled = true;
      }
      else
      {
        if ((UnityEngine.Object) childByTag1 != (UnityEngine.Object) null)
          childByTag1.GetComponent<Renderer>().enabled = true;
        if (!((UnityEngine.Object) childByTag2 != (UnityEngine.Object) null))
          return;
        childByTag2.GetComponent<Renderer>().enabled = false;
      }
    }
    else
    {
      if ((UnityEngine.Object) childByTag1 != (UnityEngine.Object) null)
        childByTag1.GetComponent<Renderer>().enabled = false;
      if (!((UnityEngine.Object) childByTag2 != (UnityEngine.Object) null))
        return;
      childByTag2.GetComponent<Renderer>().enabled = false;
    }
  }

  public void UpdateMeshComponents()
  {
    this.UpdateRarityComponent();
    this.UpdateWatermark();
    this.UpdateEliteComponent();
    this.UpdatePremiumComponents();
    this.UpdateCardColor();
  }

  private void UpdateRarityComponent()
  {
    if (!(bool) ((UnityEngine.Object) this.m_rarityGemMesh))
      return;
    Vector2 offset;
    Color tint;
    bool rarityTextureOffset = this.GetRarityTextureOffset(out offset, out tint);
    SceneUtils.EnableRenderers(this.m_rarityGemMesh, rarityTextureOffset, true);
    if ((bool) ((UnityEngine.Object) this.m_rarityFrameMesh))
      SceneUtils.EnableRenderers(this.m_rarityFrameMesh, rarityTextureOffset, true);
    if (!rarityTextureOffset)
      return;
    this.m_rarityGemMesh.GetComponent<Renderer>().material.mainTextureOffset = offset;
    this.m_rarityGemMesh.GetComponent<Renderer>().material.SetColor("_tint", tint);
  }

  private bool GetRarityTextureOffset(out Vector2 offset, out Color tint)
  {
    offset = this.GEM_TEXTURE_OFFSET_COMMON;
    tint = this.GEM_COLOR_COMMON;
    if (this.m_entityDef == null && this.m_entity == null)
      return false;
    TAG_CARD_SET tagCardSet = this.m_entityDef == null ? this.m_entity.GetCardSet() : this.m_entityDef.GetCardSet();
    if (tagCardSet == TAG_CARD_SET.CORE || tagCardSet == TAG_CARD_SET.MISSIONS)
      return false;
    switch (this.GetRarity())
    {
      case TAG_RARITY.COMMON:
        offset = this.GEM_TEXTURE_OFFSET_COMMON;
        tint = this.GEM_COLOR_COMMON;
        break;
      case TAG_RARITY.RARE:
        offset = this.GEM_TEXTURE_OFFSET_RARE;
        tint = this.GEM_COLOR_RARE;
        break;
      case TAG_RARITY.EPIC:
        offset = this.GEM_TEXTURE_OFFSET_EPIC;
        tint = this.GEM_COLOR_EPIC;
        break;
      case TAG_RARITY.LEGENDARY:
        offset = this.GEM_TEXTURE_OFFSET_LEGENDARY;
        tint = this.GEM_COLOR_LEGENDARY;
        break;
      default:
        return false;
    }
    return true;
  }

  private void UpdateWatermark()
  {
    if (this.m_entityDef == null && this.m_entity == null)
      return;
    TAG_CARD_SET cardSet = this.GetCardSet();
    if (!(bool) ((UnityEngine.Object) this.m_descriptionMesh) || !this.m_descriptionMesh.GetComponent<Renderer>().material.HasProperty("_SecondTint"))
      return;
    string name = "Set1_Icon";
    TAG_CARD_SET tagCardSet = cardSet;
    float num;
    switch (tagCardSet)
    {
      case TAG_CARD_SET.FP1:
        name = "NaxxIcon";
        num = 99f / 128f;
        break;
      case TAG_CARD_SET.PE1:
        name = "GvGIcon";
        num = 99f / 128f;
        break;
      case TAG_CARD_SET.BRM:
        name = "BRMIcon";
        num = 99f / 128f;
        break;
      case TAG_CARD_SET.TGT:
        name = "TGTIcon";
        num = 99f / 128f;
        break;
      case TAG_CARD_SET.LOE:
        name = "LOEIcon";
        num = 99f / 128f;
        break;
      case TAG_CARD_SET.OG:
        name = "OGIcon";
        num = 99f / 128f;
        break;
      case TAG_CARD_SET.KARA:
        name = "KaraIcon";
        num = 99f / 128f;
        break;
      case TAG_CARD_SET.GANGS:
        name = "GangsIcon";
        num = 99f / 128f;
        break;
      default:
        num = tagCardSet == TAG_CARD_SET.EXPERT1 ? 99f / 128f : 0.0f;
        break;
    }
    this.m_descriptionMesh.GetComponent<Renderer>().material.SetTexture("_SecondTex", AssetLoader.Get().LoadTexture(name, false));
    Color color = this.m_descriptionMesh.GetComponent<Renderer>().material.GetColor("_SecondTint");
    color.a = num;
    this.m_descriptionMesh.GetComponent<Renderer>().material.SetColor("_SecondTint", color);
  }

  private void UpdateEliteComponent()
  {
    if ((UnityEngine.Object) this.m_eliteObject == (UnityEngine.Object) null)
      return;
    SceneUtils.EnableRenderers(this.m_eliteObject, this.IsElite(), true);
  }

  private void UpdatePremiumComponents()
  {
    if (this.m_premiumType == TAG_PREMIUM.NORMAL || (UnityEngine.Object) this.m_glints == (UnityEngine.Object) null)
      return;
    this.m_glints.SetActive(true);
    foreach (Renderer componentsInChild in this.m_glints.GetComponentsInChildren<Renderer>())
      componentsInChild.enabled = true;
  }

  private void UpdateRace(string raceText)
  {
    if (!(bool) ((UnityEngine.Object) this.m_racePlateObject))
      return;
    bool flag = !string.IsNullOrEmpty(raceText);
    foreach (Renderer component in this.m_racePlateObject.GetComponents<MeshRenderer>())
      component.enabled = flag;
    if (flag)
    {
      if ((bool) ((UnityEngine.Object) this.m_descriptionMesh))
        this.m_descriptionMesh.GetComponent<Renderer>().material.SetTextureOffset("_SecondTex", new Vector2(-0.04f, 0.0f));
    }
    else if ((bool) ((UnityEngine.Object) this.m_descriptionMesh))
      this.m_descriptionMesh.GetComponent<Renderer>().material.SetTextureOffset("_SecondTex", new Vector2(-0.04f, 0.07f));
    if (!(bool) ((UnityEngine.Object) this.m_raceTextMesh))
      return;
    this.m_raceTextMesh.Text = raceText;
  }

  public MultiClassBannerTransition GetMultiClassBanner()
  {
    return this.m_multiClassBanner;
  }

  public void UpdateCardColor()
  {
    if (this.m_legacyPortraitMaterialIndex < 0 && (UnityEngine.Object) this.m_cardMesh == (UnityEngine.Object) null || this.GetEntityDef() == null && this.GetEntity() == null)
      return;
    TAG_CARDTYPE tagCardtype;
    TAG_CLASS tagClass1;
    bool flag;
    int groupID;
    if (this.m_entityDef != null)
    {
      tagCardtype = this.m_entityDef.GetCardType();
      tagClass1 = this.m_entityDef.GetClass();
      flag = this.m_entityDef.IsMultiClass();
      groupID = this.m_entityDef.GetTag(GAME_TAG.MULTI_CLASS_GROUP);
    }
    else if (this.m_entity != null)
    {
      tagCardtype = this.m_entity.GetCardType();
      tagClass1 = this.m_entity.GetClass();
      flag = this.m_entity.IsMultiClass();
      groupID = this.m_entity.GetTag(GAME_TAG.MULTI_CLASS_GROUP);
    }
    else
    {
      tagCardtype = TAG_CARDTYPE.INVALID;
      tagClass1 = TAG_CLASS.INVALID;
      flag = false;
      groupID = 0;
    }
    Color magenta = Color.magenta;
    CardColorSwitcher.CardColorType colorType;
    Color color;
    switch (tagClass1)
    {
      case TAG_CLASS.DRUID:
        colorType = CardColorSwitcher.CardColorType.TYPE_DRUID;
        color = this.CLASS_COLOR_DRUID;
        break;
      case TAG_CLASS.HUNTER:
        colorType = CardColorSwitcher.CardColorType.TYPE_HUNTER;
        color = this.CLASS_COLOR_HUNTER;
        break;
      case TAG_CLASS.MAGE:
        colorType = CardColorSwitcher.CardColorType.TYPE_MAGE;
        color = this.CLASS_COLOR_MAGE;
        break;
      case TAG_CLASS.PALADIN:
        colorType = CardColorSwitcher.CardColorType.TYPE_PALADIN;
        color = this.CLASS_COLOR_PALADIN;
        break;
      case TAG_CLASS.PRIEST:
        colorType = CardColorSwitcher.CardColorType.TYPE_PRIEST;
        color = this.CLASS_COLOR_PRIEST;
        break;
      case TAG_CLASS.ROGUE:
        colorType = CardColorSwitcher.CardColorType.TYPE_ROGUE;
        color = this.CLASS_COLOR_ROGUE;
        break;
      case TAG_CLASS.SHAMAN:
        colorType = CardColorSwitcher.CardColorType.TYPE_SHAMAN;
        color = this.CLASS_COLOR_SHAMAN;
        break;
      case TAG_CLASS.WARLOCK:
        colorType = CardColorSwitcher.CardColorType.TYPE_WARLOCK;
        color = this.CLASS_COLOR_WARLOCK;
        break;
      case TAG_CLASS.WARRIOR:
        colorType = CardColorSwitcher.CardColorType.TYPE_WARRIOR;
        color = this.CLASS_COLOR_WARRIOR;
        break;
      case TAG_CLASS.DREAM:
        colorType = CardColorSwitcher.CardColorType.TYPE_HUNTER;
        color = this.CLASS_COLOR_HUNTER;
        break;
      default:
        colorType = CardColorSwitcher.CardColorType.TYPE_GENERIC;
        color = this.CLASS_COLOR_GENERIC;
        break;
    }
    if (flag)
    {
      colorType = CardColorSwitcher.CardColorType.TYPE_GENERIC;
      if ((UnityEngine.Object) this.m_multiClassBannerContainer != (UnityEngine.Object) null)
      {
        this.m_multiClassBannerContainer.gameObject.SetActive(true);
        this.m_multiClassBanner = this.m_multiClassBannerContainer.PrefabGameObject(true).GetComponent<MultiClassBannerTransition>();
        if ((UnityEngine.Object) this.m_multiClassBanner != (UnityEngine.Object) null)
        {
          IEnumerable<TAG_CLASS> classes;
          if (this.m_entityDef != null)
            classes = this.m_entityDef.GetClasses(new Comparison<TAG_CLASS>(MultiClassBannerTransition.CompareClasses));
          else if (this.m_entity != null)
          {
            classes = this.m_entity.GetClasses(new Comparison<TAG_CLASS>(MultiClassBannerTransition.CompareClasses));
            if (this.m_entity.GetZone() == TAG_ZONE.HAND && !this.m_entity.IsHistoryDupe())
            {
              foreach (TAG_CLASS tagClass2 in classes)
              {
                if (tagClass2 == this.m_entity.GetHero().GetClass())
                {
                  classes = (IEnumerable<TAG_CLASS>) new TAG_CLASS[1]
                  {
                    tagClass2
                  };
                  break;
                }
              }
            }
          }
          else
            classes = (IEnumerable<TAG_CLASS>) new List<TAG_CLASS>();
          this.m_multiClassBanner.SetClasses(classes);
          this.m_multiClassBanner.SetMultiClassGroup(groupID);
          if (this.m_premiumType == TAG_PREMIUM.GOLDEN)
            this.m_multiClassBanner.SetGoldenCardMesh(this.m_cardMesh, this.m_premiumRibbon);
          Vector3 localPosition1 = this.m_manaObject.transform.localPosition;
          Vector3 localPosition2 = this.m_costTextMesh.transform.localPosition;
          localPosition1.y = 0.027f;
          localPosition2.y = 0.088f;
          this.m_manaObject.transform.localPosition = localPosition1;
          this.m_costTextMesh.transform.localPosition = localPosition2;
        }
      }
    }
    else if ((UnityEngine.Object) this.m_multiClassBannerContainer != (UnityEngine.Object) null)
      this.m_multiClassBannerContainer.gameObject.SetActive(false);
    switch (tagCardtype)
    {
      case TAG_CARDTYPE.HERO:
        if (this.m_entity == null || this.m_entity.IsControlledByFriendlySidePlayer() || !this.m_entity.IsHistoryDupe())
          break;
        Transform child = this.GetRootObject().transform.FindChild("History_Hero_Banner");
        if ((UnityEngine.Object) child == (UnityEngine.Object) null)
          break;
        child.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.005f, -0.505f);
        break;
      case TAG_CARDTYPE.MINION:
        switch (this.m_premiumType)
        {
          case TAG_PREMIUM.NORMAL:
            if ((UnityEngine.Object) CardColorSwitcher.Get() == (UnityEngine.Object) null)
              return;
            Texture texture1 = (Texture) null;
            if ((bool) ((UnityEngine.Object) CardColorSwitcher.Get()))
              texture1 = CardColorSwitcher.Get().GetMinionTexture(colorType);
            if ((bool) ((UnityEngine.Object) this.m_cardMesh))
            {
              if (this.m_cardFrontMatIdx <= -1)
                return;
              this.m_cardMesh.GetComponent<Renderer>().materials[this.m_cardFrontMatIdx].mainTexture = texture1;
              return;
            }
            if (this.m_legacyCardColorMaterialIndex < 0)
              return;
            this.m_meshRenderer.GetComponent<Renderer>().materials[this.m_legacyCardColorMaterialIndex].mainTexture = texture1;
            return;
          case TAG_PREMIUM.GOLDEN:
            if (this.m_premiumRibbon < 0 || !(bool) ((UnityEngine.Object) this.m_cardMesh))
              return;
            this.m_cardMesh.GetComponent<Renderer>().materials[this.m_premiumRibbon].color = color;
            return;
          default:
            UnityEngine.Debug.LogWarning((object) string.Format("Actor.UpdateCardColor(): unexpected premium type {0}", (object) this.m_premiumType));
            return;
        }
      case TAG_CARDTYPE.SPELL:
        switch (this.m_premiumType)
        {
          case TAG_PREMIUM.NORMAL:
            if ((UnityEngine.Object) CardColorSwitcher.Get() == (UnityEngine.Object) null)
              return;
            Texture texture2 = (Texture) null;
            if ((bool) ((UnityEngine.Object) CardColorSwitcher.Get()))
              texture2 = CardColorSwitcher.Get().GetSpellTexture(colorType);
            if ((bool) ((UnityEngine.Object) this.m_cardMesh))
            {
              if (this.m_cardFrontMatIdx > -1)
                this.m_cardMesh.GetComponent<Renderer>().materials[this.m_cardFrontMatIdx].mainTexture = texture2;
              if (!(bool) ((UnityEngine.Object) this.m_portraitMesh) || this.m_portraitFrameMatIdx <= -1)
                return;
              this.m_portraitMesh.GetComponent<Renderer>().materials[this.m_portraitFrameMatIdx].mainTexture = texture2;
              return;
            }
            if (this.m_legacyCardColorMaterialIndex < 0 || !((UnityEngine.Object) this.m_meshRenderer != (UnityEngine.Object) null))
              return;
            this.m_meshRenderer.materials[this.m_legacyCardColorMaterialIndex].mainTexture = texture2;
            return;
          case TAG_PREMIUM.GOLDEN:
            if (this.m_premiumRibbon < 0 || !(bool) ((UnityEngine.Object) this.m_cardMesh))
              return;
            this.m_cardMesh.GetComponent<Renderer>().materials[this.m_premiumRibbon].color = color;
            return;
          default:
            UnityEngine.Debug.LogWarning((object) string.Format("Actor.UpdateCardColor(): unexpected premium type {0}", (object) this.m_premiumType));
            return;
        }
      case TAG_CARDTYPE.WEAPON:
        switch (this.m_premiumType)
        {
          case TAG_PREMIUM.NORMAL:
            if (!(bool) ((UnityEngine.Object) this.m_descriptionTrimMesh))
              return;
            this.m_descriptionTrimMesh.GetComponent<Renderer>().material.SetColor("_Color", color);
            return;
          case TAG_PREMIUM.GOLDEN:
            if (this.m_premiumRibbon < 0 || !(bool) ((UnityEngine.Object) this.m_cardMesh))
              return;
            this.m_cardMesh.GetComponent<Renderer>().materials[this.m_premiumRibbon].color = color;
            return;
          default:
            return;
        }
    }
  }

  public HistoryCard GetHistoryCard()
  {
    if ((UnityEngine.Object) this.transform.parent == (UnityEngine.Object) null)
      return (HistoryCard) null;
    return this.transform.parent.gameObject.GetComponent<HistoryCard>();
  }

  public HistoryChildCard GetHistoryChildCard()
  {
    if ((UnityEngine.Object) this.transform.parent == (UnityEngine.Object) null)
      return (HistoryChildCard) null;
    return this.transform.parent.gameObject.GetComponent<HistoryChildCard>();
  }

  public void SetHistoryItem(HistoryItem card)
  {
    if ((UnityEngine.Object) card == (UnityEngine.Object) null)
    {
      this.transform.parent = (Transform) null;
    }
    else
    {
      this.transform.parent = card.transform;
      TransformUtil.Identity((Component) this.transform);
      if ((UnityEngine.Object) this.m_rootObject != (UnityEngine.Object) null)
        TransformUtil.Identity((Component) this.m_rootObject.transform);
      this.m_entity = card.GetEntity();
      this.UpdateTextComponents(this.m_entity);
      this.UpdateMeshComponents();
      if (this.m_premiumType == TAG_PREMIUM.GOLDEN && (UnityEngine.Object) card.GetPortraitGoldenMaterial() != (UnityEngine.Object) null && !this.m_DisablePremiumPortrait)
        this.SetPortraitMaterial(card.GetPortraitGoldenMaterial());
      else
        this.SetPortraitTextureOverride(card.GetPortraitTexture());
      if (!((UnityEngine.Object) this.m_spellTable != (UnityEngine.Object) null))
        return;
      using (List<SpellTableEntry>.Enumerator enumerator = this.m_spellTable.m_Table.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Spell spell = enumerator.Current.m_Spell;
          if (!((UnityEngine.Object) spell == (UnityEngine.Object) null))
            spell.m_BlockServerEvents = false;
        }
      }
    }
  }

  public SpellTable GetSpellTable()
  {
    return this.m_spellTable;
  }

  public Spell LoadSpell(SpellType spellType)
  {
    if ((UnityEngine.Object) this.m_sharedSpellTable == (UnityEngine.Object) null)
      return (Spell) null;
    Spell spell = this.m_sharedSpellTable.GetSpell(spellType);
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
      return (Spell) null;
    this.m_localSpellTable.Add(spellType, spell);
    Transform transform1 = spell.gameObject.transform;
    Transform transform2 = this.gameObject.transform;
    TransformUtil.AttachAndPreserveLocalTransform(transform1, transform2);
    transform1.localScale.Scale(this.m_sharedSpellTable.gameObject.transform.localScale);
    SceneUtils.SetLayer(spell.gameObject, (GameLayer) this.gameObject.layer);
    spell.OnLoad();
    if ((UnityEngine.Object) this.m_actorStateMgr != (UnityEngine.Object) null)
      spell.AddStateStartedCallback(new Spell.StateStartedCallback(this.OnSpellStateStarted));
    return spell;
  }

  public Spell GetLoadedSpell(SpellType spellType)
  {
    Spell spell = (Spell) null;
    if (this.m_localSpellTable != null)
      this.m_localSpellTable.TryGetValue(spellType, out spell);
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
      spell = this.LoadSpell(spellType);
    return spell;
  }

  public Spell ActivateTaunt()
  {
    this.DeactivateTaunt();
    if (this.GetEntity().IsStealthed() && !Options.Get().GetBool(Option.HAS_SEEN_STEALTH_TAUNTER, false))
    {
      NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_STEALTH_TAUNT3_22"), "VO_INNKEEPER_STEALTH_TAUNT3_22", 0.0f, (Action) null, false);
      Options.Get().SetBool(Option.HAS_SEEN_STEALTH_TAUNTER, true);
    }
    if (this.m_premiumType == TAG_PREMIUM.GOLDEN)
    {
      if (this.GetEntity().IsStealthed())
        return this.ActivateSpellBirthState(SpellType.TAUNT_PREMIUM_STEALTH);
      return this.ActivateSpellBirthState(SpellType.TAUNT_PREMIUM);
    }
    if (this.GetEntity().IsStealthed())
      return this.ActivateSpellBirthState(SpellType.TAUNT_STEALTH);
    return this.ActivateSpellBirthState(SpellType.TAUNT);
  }

  public void DeactivateTaunt()
  {
    if (this.IsSpellActive(SpellType.TAUNT))
      this.ActivateSpellDeathState(SpellType.TAUNT);
    if (this.IsSpellActive(SpellType.TAUNT_PREMIUM))
      this.ActivateSpellDeathState(SpellType.TAUNT_PREMIUM);
    if (this.IsSpellActive(SpellType.TAUNT_PREMIUM_STEALTH))
      this.ActivateSpellDeathState(SpellType.TAUNT_PREMIUM_STEALTH);
    if (!this.IsSpellActive(SpellType.TAUNT_STEALTH))
      return;
    this.ActivateSpellDeathState(SpellType.TAUNT_STEALTH);
  }

  public Spell GetSpell(SpellType spellType)
  {
    Spell spell = (Spell) null;
    if (this.m_useSharedSpellTable)
      spell = this.GetLoadedSpell(spellType);
    else if ((UnityEngine.Object) this.m_spellTable != (UnityEngine.Object) null)
      this.m_spellTable.FindSpell(spellType, out spell);
    return spell;
  }

  public Spell GetSpellIfLoaded(SpellType spellType)
  {
    Spell spell = (Spell) null;
    if (this.m_useSharedSpellTable)
      this.GetSpellIfLoaded(spellType, out spell);
    else if ((UnityEngine.Object) this.m_spellTable != (UnityEngine.Object) null)
      this.m_spellTable.FindSpell(spellType, out spell);
    return spell;
  }

  public bool GetSpellIfLoaded(SpellType spellType, out Spell result)
  {
    if (this.m_localSpellTable == null || !this.m_localSpellTable.ContainsKey(spellType))
    {
      result = (Spell) null;
      return false;
    }
    result = this.m_localSpellTable[spellType];
    return (UnityEngine.Object) result != (UnityEngine.Object) null;
  }

  public Spell ActivateSpellBirthState(SpellType spellType)
  {
    Spell spell = this.GetSpell(spellType);
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
      return (Spell) null;
    spell.ActivateState(SpellStateType.BIRTH);
    return spell;
  }

  public bool IsSpellActive(SpellType spellType)
  {
    Spell spellIfLoaded = this.GetSpellIfLoaded(spellType);
    if ((UnityEngine.Object) spellIfLoaded == (UnityEngine.Object) null)
      return false;
    return spellIfLoaded.IsActive();
  }

  public void ActivateSpellDeathState(SpellType spellType)
  {
    Spell spellIfLoaded = this.GetSpellIfLoaded(spellType);
    if ((UnityEngine.Object) spellIfLoaded == (UnityEngine.Object) null)
      return;
    spellIfLoaded.ActivateState(SpellStateType.DEATH);
  }

  public void ActivateAllSpellsDeathStates()
  {
    if (this.m_useSharedSpellTable)
    {
      using (Map<SpellType, Spell>.ValueCollection.Enumerator enumerator = this.m_localSpellTable.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Spell current = enumerator.Current;
          if (current.IsActive())
            current.ActivateState(SpellStateType.DEATH);
        }
      }
    }
    else
    {
      if (!((UnityEngine.Object) this.m_spellTable != (UnityEngine.Object) null))
        return;
      using (List<SpellTableEntry>.Enumerator enumerator = this.m_spellTable.m_Table.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Spell spell = enumerator.Current.m_Spell;
          if (!((UnityEngine.Object) spell == (UnityEngine.Object) null) && spell.IsActive())
            spell.ActivateState(SpellStateType.DEATH);
        }
      }
    }
  }

  public void DoCardDeathVisuals()
  {
    foreach (int num in Enum.GetValues(typeof (SpellType)))
    {
      SpellType spellType = (SpellType) num;
      if (this.IsSpellActive(spellType) && spellType != SpellType.DEATH && (spellType != SpellType.DEATHRATTLE_DEATH && spellType != SpellType.DAMAGE))
        this.ActivateSpellDeathState(spellType);
    }
  }

  public void DeactivateAllSpells()
  {
    if (this.m_useSharedSpellTable)
    {
      using (Map<SpellType, Spell>.ValueCollection.Enumerator enumerator = this.m_localSpellTable.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Deactivate();
      }
    }
    else
    {
      if (!((UnityEngine.Object) this.m_spellTable != (UnityEngine.Object) null))
        return;
      using (List<SpellTableEntry>.Enumerator enumerator = this.m_spellTable.m_Table.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Spell spell = enumerator.Current.m_Spell;
          if (!((UnityEngine.Object) spell == (UnityEngine.Object) null))
            spell.Deactivate();
        }
      }
    }
  }

  public void DestroySpell(SpellType spellType)
  {
    if (this.m_useSharedSpellTable)
    {
      Spell spell;
      if (!this.m_localSpellTable.TryGetValue(spellType, out spell))
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
      this.m_localSpellTable.Remove(spellType);
    }
    else
      UnityEngine.Debug.LogError((object) string.Format("Actor.DestroySpell() - FAILED to destroy {0} because the Actor is not using a shared spell table.", (object) spellType));
  }

  public void HideArmorSpell()
  {
    this.m_armorSpell.gameObject.SetActive(false);
  }

  private void UpdateRootObjectSpellComponents()
  {
    if (this.m_entity == null)
      return;
    if (this.m_armorSpellLoading)
      this.StartCoroutine(this.UpdateArmorSpellWhenLoaded());
    if (!((UnityEngine.Object) this.m_armorSpell != (UnityEngine.Object) null))
      return;
    this.UpdateArmorSpell();
  }

  [DebuggerHidden]
  private IEnumerator UpdateArmorSpellWhenLoaded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Actor.\u003CUpdateArmorSpellWhenLoaded\u003Ec__Iterator33() { \u003C\u003Ef__this = this };
  }

  private void UpdateArmorSpell()
  {
    if (!this.m_armorSpell.gameObject.activeInHierarchy)
      return;
    int armor1 = this.m_entity.GetArmor();
    int armor2 = this.m_armorSpell.GetArmor();
    this.m_armorSpell.SetArmor(armor1);
    if (armor1 > 0)
    {
      bool flag = this.m_armorSpell.IsShown();
      if (!flag)
        this.m_armorSpell.Show();
      if (armor2 <= 0)
        this.StartCoroutine(this.ActivateArmorSpell(SpellStateType.BIRTH, true));
      else if (armor2 > armor1)
        this.StartCoroutine(this.ActivateArmorSpell(SpellStateType.ACTION, true));
      else if (armor2 < armor1)
      {
        this.StartCoroutine(this.ActivateArmorSpell(SpellStateType.CANCEL, true));
      }
      else
      {
        if (flag)
          return;
        this.StartCoroutine(this.ActivateArmorSpell(SpellStateType.IDLE, true));
      }
    }
    else
    {
      if (armor2 <= 0)
        return;
      this.StartCoroutine(this.ActivateArmorSpell(SpellStateType.DEATH, false));
    }
  }

  [DebuggerHidden]
  private IEnumerator ActivateArmorSpell(SpellStateType stateType, bool armorShouldBeOn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Actor.\u003CActivateArmorSpell\u003Ec__Iterator34() { stateType = stateType, armorShouldBeOn = armorShouldBeOn, \u003C\u0024\u003EstateType = stateType, \u003C\u0024\u003EarmorShouldBeOn = armorShouldBeOn, \u003C\u003Ef__this = this };
  }

  private void OnSpellStateStarted(Spell spell, SpellStateType prevStateType, object userData)
  {
    spell.AddStateStartedCallback(new Spell.StateStartedCallback(this.OnSpellStateStarted));
    this.m_actorStateMgr.RefreshStateMgr();
    if (!(bool) ((UnityEngine.Object) this.m_projectedShadow))
      return;
    this.m_projectedShadow.UpdateContactShadow();
  }

  private void AssignRootObject()
  {
    this.m_rootObject = SceneUtils.FindChildBySubstring(this.gameObject, "RootObject");
  }

  private void AssignBones()
  {
    this.m_bones = SceneUtils.FindChildBySubstring(this.gameObject, "Bones");
  }

  private void AssignMeshRenderers()
  {
    foreach (MeshRenderer componentsInChild1 in this.gameObject.GetComponentsInChildren<MeshRenderer>(true))
    {
      if (componentsInChild1.gameObject.name.Equals("Mesh", StringComparison.OrdinalIgnoreCase))
      {
        this.m_meshRenderer = componentsInChild1;
        foreach (MeshRenderer componentsInChild2 in componentsInChild1.gameObject.GetComponentsInChildren<MeshRenderer>(true))
          this.AssignMaterials(componentsInChild2);
        break;
      }
    }
  }

  private void AssignMaterials(MeshRenderer meshRenderer)
  {
    for (int materialIndex = 0; materialIndex < meshRenderer.sharedMaterials.Length; ++materialIndex)
    {
      Material sharedMaterial = RenderUtils.GetSharedMaterial((Renderer) meshRenderer, materialIndex);
      if (!((UnityEngine.Object) sharedMaterial == (UnityEngine.Object) null))
      {
        if (sharedMaterial.name.LastIndexOf("Portrait", StringComparison.OrdinalIgnoreCase) >= 0)
          this.m_legacyPortraitMaterialIndex = materialIndex;
        else if (sharedMaterial.name.IndexOf("Card_Inhand_Ability_Warlock", StringComparison.OrdinalIgnoreCase) >= 0)
          this.m_legacyCardColorMaterialIndex = materialIndex;
        else if (sharedMaterial.name.IndexOf("Card_Inhand_Warlock", StringComparison.OrdinalIgnoreCase) >= 0)
          this.m_legacyCardColorMaterialIndex = materialIndex;
        else if (sharedMaterial.name.IndexOf("Card_Inhand_Weapon_Warlock", StringComparison.OrdinalIgnoreCase) >= 0)
          this.m_legacyCardColorMaterialIndex = materialIndex;
      }
    }
  }

  private void AssignSpells()
  {
    this.m_spellTable = this.gameObject.GetComponentInChildren<SpellTable>();
    this.m_actorStateMgr = this.gameObject.GetComponentInChildren<ActorStateMgr>();
    if ((UnityEngine.Object) this.m_spellTable == (UnityEngine.Object) null)
    {
      if (string.IsNullOrEmpty(this.m_spellTablePrefab))
        return;
      SpellCache spellCache = SpellCache.Get();
      if ((UnityEngine.Object) spellCache != (UnityEngine.Object) null)
      {
        SpellTable spellTable = spellCache.GetSpellTable(this.m_spellTablePrefab);
        if ((UnityEngine.Object) spellTable != (UnityEngine.Object) null)
        {
          this.m_useSharedSpellTable = true;
          this.m_sharedSpellTable = spellTable;
          this.m_localSpellTable = new Map<SpellType, Spell>();
        }
        else
          UnityEngine.Debug.LogWarning((object) ("failed to load spell table: " + this.m_spellTablePrefab));
      }
      else
        UnityEngine.Debug.LogWarning((object) ("Null spell cache: " + this.m_spellTablePrefab));
    }
    else
    {
      if (!((UnityEngine.Object) this.m_actorStateMgr != (UnityEngine.Object) null))
        return;
      using (List<SpellTableEntry>.Enumerator enumerator = this.m_spellTable.m_Table.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          SpellTableEntry current = enumerator.Current;
          if (!((UnityEngine.Object) current.m_Spell == (UnityEngine.Object) null))
            current.m_Spell.AddStateStartedCallback(new Spell.StateStartedCallback(this.OnSpellStateStarted));
        }
      }
    }
  }

  private void LoadArmorSpell()
  {
    if ((UnityEngine.Object) this.m_armorSpellBone == (UnityEngine.Object) null)
      return;
    this.m_armorSpellLoading = true;
    string name = "Assets/Game/Spells/Armor/Hero_Armor";
    if ((UnityEngine.Object) this.GetCardDef() != (UnityEngine.Object) null && this.GetCardDef().m_CustomHeroArmorSpell != string.Empty)
      name = this.GetCardDef().m_CustomHeroArmorSpell;
    AssetLoader.Get().LoadSpell(name, new AssetLoader.GameObjectCallback(this.OnArmorSpellLoaded), (object) null, false);
  }

  private void OnArmorSpellLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("{0} - Actor.OnArmorSpellLoaded() - failed to load Hero_Armor spell! m_armorSpell GameObject = null!", (object) name));
    }
    else
    {
      this.m_armorSpellLoading = false;
      this.m_armorSpell = go.GetComponent<ArmorSpell>();
      if ((UnityEngine.Object) this.m_armorSpell == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("{0} - Actor.OnArmorSpellLoaded() - failed to load Hero_Armor spell! m_armorSpell Spell = null!", (object) name));
      }
      else
      {
        go.transform.parent = this.m_armorSpellBone.transform;
        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.identity;
        go.transform.localScale = Vector3.one;
      }
    }
  }
}
