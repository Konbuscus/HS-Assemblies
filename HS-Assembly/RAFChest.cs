﻿// Decompiled with JetBrains decompiler
// Type: RAFChest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RAFChest : PegUIElement
{
  public Renderer m_chestQuad;
  public GameObject m_tooltipBone;
  private bool m_isChestOpen;

  public void SetOpen(bool isChestOpen)
  {
    if (this.m_isChestOpen == isChestOpen)
      return;
    this.m_isChestOpen = isChestOpen;
    this.m_chestQuad.material.SetTextureOffset("_MainTex", new Vector2(!this.m_isChestOpen ? 0.0f : 0.5f, 0.5f));
    this.gameObject.GetComponent<UIBHighlight>().EnableResponse = !this.m_isChestOpen;
  }

  public bool IsOpen()
  {
    return this.m_isChestOpen;
  }
}
