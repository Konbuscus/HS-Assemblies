﻿// Decompiled with JetBrains decompiler
// Type: DeckCardBarSummonIn
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DeckCardBarSummonIn : SpellImpl
{
  public GameObject m_echoQuad;
  public GameObject m_fxEvaporate;

  private void OnDisable()
  {
    if ((Object) this.m_echoQuad != (Object) null)
      this.m_echoQuad.GetComponent<Renderer>().material.color = Color.clear;
    if (!((Object) this.m_fxEvaporate != (Object) null))
      return;
    this.m_fxEvaporate.GetComponent<ParticleSystem>().Clear();
  }

  protected override void OnBirth(SpellStateType prevStateType)
  {
    this.StartCoroutine(this.BirthState());
  }

  [DebuggerHidden]
  private IEnumerator BirthState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckCardBarSummonIn.\u003CBirthState\u003Ec__Iterator2B2() { \u003C\u003Ef__this = this };
  }
}
