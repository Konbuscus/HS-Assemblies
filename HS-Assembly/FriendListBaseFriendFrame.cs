﻿// Decompiled with JetBrains decompiler
// Type: FriendListBaseFriendFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using UnityEngine;

public class FriendListBaseFriendFrame : MonoBehaviour
{
  private static readonly Color TEXT_COLOR_NORMAL = Color.white;
  private static readonly Color TEXT_COLOR_AWAY = Color.yellow;
  private static readonly Color TEXT_COLOR_BUSY = Color.red;
  private static readonly Color TEXT_COLOR_OFFLINE = Color.grey;
  public PlayerIcon m_PlayerIcon;
  public UberText m_PlayerNameText;
  public UberText m_StatusText;
  public FriendListFrameBasePrefabs m_Prefabs;
  public FriendListChatIcon m_ChatIcon;
  public TournamentMedal m_rankMedalPrefab;
  public Spawner m_rankMedalSpawner;
  public TournamentMedal m_rankMedal;
  protected BnetPlayer m_player;
  protected MedalInfoTranslator m_medal;

  protected virtual void Awake()
  {
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    BnetWhisperMgr.Get().AddWhisperListener(new BnetWhisperMgr.WhisperCallback(this.OnWhisper));
    ChatMgr.Get().AddPlayerChatInfoChangedListener(new ChatMgr.PlayerChatInfoChangedCallback(this.OnPlayerChatInfoChanged));
    if ((Object) this.m_rankMedalSpawner == (Object) null)
    {
      this.m_rankMedal = Object.Instantiate<TournamentMedal>(this.m_rankMedalPrefab);
      this.m_rankMedal.transform.parent = this.transform;
      this.m_rankMedal.transform.localScale = new Vector3(20f, 1f, 20f);
      this.m_rankMedal.transform.localRotation = Quaternion.Euler(new Vector3(-90f, 0.0f, 0.0f));
    }
    else
      this.m_rankMedal = this.m_rankMedalSpawner.Spawn<TournamentMedal>();
    this.m_rankMedal.RemoveEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.m_rankMedal.MedalOver));
    this.m_rankMedal.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.RankMedalOver));
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_rankMedal.GetComponent<Collider>().enabled = false;
    this.m_rankMedal.gameObject.SetActive(false);
    SceneUtils.SetLayer((Component) this.m_rankMedal, GameLayer.BattleNetFriendList);
  }

  protected virtual void OnDestroy()
  {
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    BnetWhisperMgr.Get().RemoveWhisperListener(new BnetWhisperMgr.WhisperCallback(this.OnWhisper));
    if (!((Object) ChatMgr.Get() != (Object) null))
      return;
    ChatMgr.Get().RemovePlayerChatInfoChangedListener(new ChatMgr.PlayerChatInfoChangedCallback(this.OnPlayerChatInfoChanged));
  }

  public BnetPlayer GetFriend()
  {
    return this.m_player;
  }

  public virtual bool SetFriend(BnetPlayer player)
  {
    if (this.m_player == player)
      return false;
    this.m_player = player;
    this.m_PlayerIcon.SetPlayer(player);
    this.m_ChatIcon.SetPlayer(player);
    this.UpdateFriend();
    return true;
  }

  public virtual void UpdateFriend()
  {
    this.m_ChatIcon.UpdateIcon();
    if (this.m_player == null)
      return;
    this.m_StatusText.TextColor = !this.m_player.IsOnline() ? FriendListBaseFriendFrame.TEXT_COLOR_OFFLINE : (!this.m_player.IsAway() ? (!this.m_player.IsBusy() ? FriendListBaseFriendFrame.TEXT_COLOR_NORMAL : FriendListBaseFriendFrame.TEXT_COLOR_BUSY) : FriendListBaseFriendFrame.TEXT_COLOR_AWAY);
    BnetGameAccount hearthstoneGameAccount = this.m_player.GetHearthstoneGameAccount();
    this.m_medal = !(hearthstoneGameAccount == (BnetGameAccount) null) ? RankMgr.Get().GetRankPresenceField(hearthstoneGameAccount) : (MedalInfoTranslator) null;
    if (this.m_medal == null || this.m_medal.GetCurrentMedal(this.m_medal.IsBestCurrentRankWild()).rank == 25)
    {
      this.m_rankMedal.gameObject.SetActive(false);
      this.m_PlayerIcon.Show();
    }
    else
    {
      this.m_PlayerIcon.Hide();
      this.m_rankMedal.gameObject.SetActive(true);
      this.m_rankMedal.SetMedal(this.m_medal, false);
      this.m_rankMedal.SetFormat(this.m_rankMedal.IsBestCurrentRankWild());
    }
  }

  private void RankMedalOver(UIEvent e)
  {
    TooltipPanel tooltipPanel = this.m_rankMedal.GetComponent<TooltipZone>().ShowTooltip(this.m_medal.GetCurrentMedal(this.m_medal.IsBestCurrentRankWild()).name, string.Empty, 0.7f, true);
    SceneUtils.SetLayer(tooltipPanel.gameObject, GameLayer.BattleNetFriendList);
    tooltipPanel.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
    tooltipPanel.transform.localScale = new Vector3(3f, 3f, 3f);
    TransformUtil.SetPoint((Component) tooltipPanel, Anchor.LEFT, this.m_rankMedal.gameObject, Anchor.RIGHT, new Vector3(1f, 0.0f, 0.0f));
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    if (!changelist.HasChange(this.m_player))
      return;
    this.UpdateFriend();
  }

  private void OnWhisper(BnetWhisper whisper, object userData)
  {
    if (this.m_player == null || !WhisperUtil.IsSpeakerOrReceiver(this.m_player, whisper))
      return;
    this.UpdateFriend();
  }

  private void OnPlayerChatInfoChanged(PlayerChatInfo chatInfo, object userData)
  {
    if (this.m_player != chatInfo.GetPlayer())
      return;
    this.UpdateFriend();
  }

  protected void UpdateOnlineStatus()
  {
    if (this.m_player.IsAway())
      this.m_StatusText.Text = FriendUtils.GetAwayTimeString(this.m_player.GetBestAwayTimeMicrosec());
    else if (this.m_player.IsBusy())
    {
      this.m_StatusText.Text = GameStrings.Get("GLOBAL_FRIENDLIST_BUSYSTATUS");
    }
    else
    {
      string statusText = PresenceMgr.Get().GetStatusText(this.m_player);
      if (statusText != null)
      {
        this.m_StatusText.Text = statusText;
      }
      else
      {
        BnetProgramId bestProgramId = this.m_player.GetBestProgramId();
        if ((bgs.FourCC) bestProgramId != (bgs.FourCC) null)
          this.m_StatusText.Text = BnetUtils.GetNameForProgramId(bestProgramId);
        else
          this.m_StatusText.Text = string.Empty;
      }
    }
  }

  protected void UpdateOfflineStatus()
  {
    this.m_StatusText.Text = FriendUtils.GetLastOnlineElapsedTimeString(this.m_player.GetBestLastOnlineMicrosec());
  }
}
