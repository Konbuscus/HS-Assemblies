﻿// Decompiled with JetBrains decompiler
// Type: GameMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class GameMenu : ButtonListMenu
{
  [CustomEditField(Sections = "Template Items")]
  public Vector3 m_ratingsObjectMinPadding = new Vector3(0.0f, 0.0f, -0.06f);
  private PlatformDependentValue<string> OPTIONS_MENU_NAME = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "OptionsMenu", Phone = "OptionsMenu_phone" };
  private readonly Vector3 BUTTON_SCALE = 15f * Vector3.one;
  private readonly Vector3 BUTTON_SCALE_PHONE = 25f * Vector3.one;
  public Transform m_menuBone;
  private static GameMenu s_instance;
  private OptionsMenu m_optionsMenu;
  private UIBButton m_concedeButton;
  private UIBButton m_leaveButton;
  private UIBButton m_restartButton;
  private UIBButton m_quitButton;
  private UIBButton m_optionsButton;
  private Notification m_loginButtonPopup;
  private bool m_hasSeenLoginTooltip;
  private constants.BnetRegion m_AccountRegion;
  private GameObject m_ratingsObject;

  protected override void Awake()
  {
    this.m_menuParent = this.m_menuBone;
    base.Awake();
    GameMenu.s_instance = this;
    this.LoadRatings();
    this.m_concedeButton = this.CreateMenuButton("ConcedeButton", "GLOBAL_CONCEDE", new UIEvent.Handler(this.ConcedeButtonPressed));
    this.m_leaveButton = this.CreateMenuButton("LeaveButton", "GLOBAL_LEAVE_SPECTATOR_MODE", new UIEvent.Handler(this.LeaveButtonPressed));
    this.m_restartButton = this.CreateMenuButton("RestartButton", "GLOBAL_RESTART", new UIEvent.Handler(this.RestartButtonPressed));
    this.m_quitButton = !(bool) ApplicationMgr.CanQuitGame ? this.CreateMenuButton("LogoutButton", !Network.ShouldBeConnectedToAurora() ? "GLOBAL_LOGIN" : "GLOBAL_LOGOUT", new UIEvent.Handler(this.LogoutButtonPressed)) : this.CreateMenuButton("QuitButton", "GLOBAL_QUIT", new UIEvent.Handler(this.QuitButtonPressed));
    this.m_optionsButton = this.CreateMenuButton("OptionsButton", "GLOBAL_OPTIONS", new UIEvent.Handler(this.OptionsButtonPressed));
    this.m_menu.m_headerText.Text = GameStrings.Get("GLOBAL_GAME_MENU");
  }

  protected override void OnDestroy()
  {
    base.OnDestroy();
    if ((Object) this.m_optionsMenu != (Object) null)
      this.m_optionsMenu.RemoveHideHandler(new OptionsMenu.hideHandler(this.OnOptionsMenuHidden));
    GameMenu.s_instance = (GameMenu) null;
  }

  private void Start()
  {
    this.gameObject.SetActive(false);
  }

  public static GameMenu Get()
  {
    return GameMenu.s_instance;
  }

  public override void Show()
  {
    if ((Object) OptionsMenu.Get() != (Object) null && OptionsMenu.Get().IsShown())
    {
      UniversalInputManager.Get().CancelTextInput(this.gameObject, true);
      OptionsMenu.Get().Hide(true);
    }
    else
    {
      base.Show();
      if ((bool) UniversalInputManager.UsePhoneUI && (Object) this.m_ratingsObject != (Object) null)
      {
        this.m_ratingsObject.SetActive(this.UseKoreanRating());
        this.m_menu.m_buttonContainer.UpdateSlices();
        this.LayoutMenuBackground();
      }
      this.ShowLoginTooltipIfNeeded();
      BnetBar.Get().m_menuButton.SetSelected(true);
    }
  }

  public override void Hide()
  {
    base.Hide();
    this.HideLoginTooltip();
    BnetBar.Get().m_menuButton.SetSelected(false);
  }

  public void ShowLoginTooltipIfNeeded()
  {
    if (Network.ShouldBeConnectedToAurora() || this.m_hasSeenLoginTooltip || (Object) this.m_quitButton == (Object) null)
      return;
    this.m_loginButtonPopup = !(bool) UniversalInputManager.UsePhoneUI ? NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(-46.9f, 34.2f, 9.4f), this.BUTTON_SCALE, GameStrings.Get("GLOBAL_MOBILE_LOG_IN_TOOLTIP"), false) : NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(-82.9f, 42.1f, 17.2f), this.BUTTON_SCALE_PHONE, GameStrings.Get("GLOBAL_MOBILE_LOG_IN_TOOLTIP"), false);
    if (!((Object) this.m_loginButtonPopup != (Object) null))
      return;
    this.m_loginButtonPopup.ShowPopUpArrow(Notification.PopUpArrowDirection.Right);
    this.m_hasSeenLoginTooltip = true;
  }

  public void HideLoginTooltip()
  {
    if ((Object) this.m_loginButtonPopup != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.m_loginButtonPopup);
    this.m_loginButtonPopup = (Notification) null;
  }

  private bool IsInGameMenu()
  {
    return SceneMgr.Get().IsInGame() && SceneMgr.Get().IsSceneLoaded() && (!LoadingScreen.Get().IsTransitioning() && GameState.Get() != null) && (!GameState.Get().IsGameOver() && (!((Object) TutorialProgressScreen.Get() != (Object) null) || !TutorialProgressScreen.Get().gameObject.activeInHierarchy));
  }

  public void ShowOptionsMenu()
  {
    this.Hide();
    if ((Object) this.m_optionsMenu == (Object) null)
    {
      this.m_optionsMenu = AssetLoader.Get().LoadGameObject((string) this.OPTIONS_MENU_NAME, true, false).GetComponent<OptionsMenu>();
      if (!((Object) this.m_optionsMenu != (Object) null))
        return;
      this.SwitchToOptionsMenu();
    }
    else
      this.SwitchToOptionsMenu();
  }

  protected override List<UIBButton> GetButtons()
  {
    List<UIBButton> uibButtonList = new List<UIBButton>();
    if (this.IsInGameMenu())
    {
      if (GameUtils.CanConcedeCurrentMission())
      {
        uibButtonList.Add(this.m_concedeButton);
        uibButtonList.Add((UIBButton) null);
      }
      if (GameMgr.Get().IsSpectator())
      {
        uibButtonList.Add(this.m_leaveButton);
        uibButtonList.Add((UIBButton) null);
      }
      if (GameUtils.CanRestartCurrentMission(true))
      {
        uibButtonList.Add(this.m_restartButton);
        uibButtonList.Add((UIBButton) null);
      }
    }
    if (!DemoMgr.Get().IsExpoDemo())
    {
      uibButtonList.Add(this.m_optionsButton);
      uibButtonList.Add(this.m_quitButton);
    }
    return uibButtonList;
  }

  protected override void LayoutMenu()
  {
    this.LayoutMenuButtons();
    if ((Object) this.m_ratingsObject != (Object) null)
      this.m_menu.m_buttonContainer.AddSlice(this.m_ratingsObject, Vector3.zero, this.m_ratingsObjectMinPadding, false);
    this.m_menu.m_buttonContainer.UpdateSlices();
    this.LayoutMenuBackground();
  }

  private void QuitButtonPressed(UIEvent e)
  {
    Network.AutoConcede();
    ApplicationMgr.Get().Exit();
  }

  private void LogoutButtonPressed(UIEvent e)
  {
    this.HideLoginTooltip();
    GameUtils.LogoutConfirmation();
    this.Hide();
  }

  private void ConcedeButtonPressed(UIEvent e)
  {
    if (GameState.Get() != null)
      GameState.Get().Concede();
    this.Hide();
  }

  private void LeaveButtonPressed(UIEvent e)
  {
    if (GameMgr.Get().IsSpectator())
      SpectatorManager.Get().LeaveSpectatorMode();
    this.Hide();
  }

  private void RestartButtonPressed(UIEvent e)
  {
    if (GameState.Get() != null)
      GameState.Get().Restart();
    this.Hide();
  }

  private void OptionsButtonPressed(UIEvent e)
  {
    this.ShowOptionsMenu();
  }

  private void SwitchToOptionsMenu()
  {
    this.m_optionsMenu.SetHideHandler(new OptionsMenu.hideHandler(this.OnOptionsMenuHidden));
    this.m_optionsMenu.Show();
  }

  private void OnOptionsMenuHidden()
  {
    Object.Destroy((Object) this.m_optionsMenu.gameObject);
    this.m_optionsMenu = (OptionsMenu) null;
    AssetCache.ClearGameObject((string) this.OPTIONS_MENU_NAME);
    if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.FATAL_ERROR) || ApplicationMgr.Get().IsResetting() || !BnetBar.Get().IsEnabled())
      return;
    this.Show();
  }

  private bool UseKoreanRating()
  {
    if (SceneMgr.Get().IsInGame())
      return false;
    bool flag = BattleNet.GetAccountCountry() == "KOR";
    if (PlatformSettings.IsMobile() && !flag)
      flag = MobileDeviceLocale.GetCountryCode() == "KR";
    return flag;
  }

  private void LoadRatings()
  {
    if (!this.UseKoreanRating())
      return;
    AssetLoader.Get().LoadGameObject("Korean_Ratings_OptionsScreen", (AssetLoader.GameObjectCallback) ((name, go, data) =>
    {
      if ((Object) go == (Object) null)
        return;
      Quaternion localRotation = go.transform.localRotation;
      GameUtils.SetParent(go, (Component) this.m_menu.m_buttonContainer, false);
      go.transform.localScale = Vector3.one;
      go.transform.localRotation = localRotation;
      this.m_ratingsObject = go;
      this.LayoutMenu();
    }), (object) null, false);
  }
}
