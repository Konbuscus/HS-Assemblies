﻿// Decompiled with JetBrains decompiler
// Type: CardSoundSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CardSoundSpell : Spell
{
  public CardSoundData m_CardSoundData = new CardSoundData();
  protected AudioSource m_activeAudioSource;
  protected bool m_forceDefaultAudioSource;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    base.OnBirth(prevStateType);
    this.Play();
  }

  protected override void OnNone(SpellStateType prevStateType)
  {
    base.OnNone(prevStateType);
    this.Stop();
  }

  public AudioSource GetActiveAudioSource()
  {
    return this.m_activeAudioSource;
  }

  public void ForceDefaultAudioSource()
  {
    this.m_forceDefaultAudioSource = true;
  }

  public bool HasActiveAudioSource()
  {
    return (Object) this.m_activeAudioSource != (Object) null;
  }

  public virtual AudioSource DetermineBestAudioSource()
  {
    return this.m_CardSoundData.m_AudioSource;
  }

  protected virtual void Play()
  {
    this.Stop();
    this.m_activeAudioSource = !this.m_forceDefaultAudioSource ? this.DetermineBestAudioSource() : this.m_CardSoundData.m_AudioSource;
    if ((Object) this.m_activeAudioSource == (Object) null)
      this.OnStateFinished();
    else
      this.StartCoroutine("DelayedPlay");
  }

  protected virtual void PlayNow()
  {
    SoundManager.Get().Play(this.m_activeAudioSource, true);
    this.StartCoroutine("WaitForSourceThenFinishState");
  }

  protected virtual void Stop()
  {
    this.StopCoroutine("DelayedPlay");
    this.StopCoroutine("WaitForSourceThenFinishState");
    SoundManager.Get().Stop(this.m_activeAudioSource);
  }

  [DebuggerHidden]
  protected IEnumerator DelayedPlay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardSoundSpell.\u003CDelayedPlay\u003Ec__Iterator299() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator WaitForSourceThenFinishState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardSoundSpell.\u003CWaitForSourceThenFinishState\u003Ec__Iterator29A() { \u003C\u003Ef__this = this };
  }
}
