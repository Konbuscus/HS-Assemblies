﻿// Decompiled with JetBrains decompiler
// Type: TouchListScrollbar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TouchListScrollbar : PegUIElement
{
  public TouchList list;
  public PegUIElement thumb;
  public Transform thumbMin;
  public Transform thumbMax;
  public GameObject cover;
  public PegUIElement track;
  private bool isActive;

  protected override void Awake()
  {
    if (this.list.orientation == TouchList.Orientation.Horizontal)
    {
      UnityEngine.Debug.LogError((object) "Horizontal TouchListScrollbar not implemented");
      UnityEngine.Object.Destroy((UnityEngine.Object) this);
    }
    else
    {
      base.Awake();
      this.ShowThumb(this.isActive);
      this.list.ClipSizeChanged += new Action(this.UpdateLayout);
      this.list.ScrollingEnabledChanged += new TouchList.ScrollingEnabledChangedEvent(this.UpdateActive);
      this.list.Scrolled += new Action(this.UpdateThumb);
      this.thumb.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.ThumbPressed));
      this.track.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.TrackPressed));
      this.UpdateLayout();
    }
  }

  private void UpdateActive(bool canScroll)
  {
    if (this.isActive == canScroll)
      return;
    this.isActive = canScroll;
    this.thumb.GetComponent<Collider>().enabled = this.isActive;
    if (this.isActive)
      this.UpdateThumb();
    this.ShowThumb(this.isActive);
  }

  private void UpdateLayout()
  {
    TransformUtil.SetPosX((Component) this.thumb, this.thumbMin.position.x);
    this.UpdateThumb();
  }

  private void ShowThumb(bool show)
  {
    Transform child = this.thumb.transform.FindChild("Mesh");
    if ((UnityEngine.Object) child != (UnityEngine.Object) null)
      child.gameObject.SetActive(show);
    if (!((UnityEngine.Object) this.cover != (UnityEngine.Object) null))
      return;
    this.cover.SetActive(!show);
  }

  private void UpdateThumb()
  {
    if (!this.isActive)
      return;
    TransformUtil.SetPosZ((Component) this.thumb, this.GetComponent<Collider>().bounds.min.z);
    float scrollValue = this.list.ScrollValue;
    TransformUtil.SetPosY((Component) this.thumb, this.thumbMin.position.y + (this.thumbMax.position.y - this.thumbMin.position.y) * Mathf.Clamp01(scrollValue));
    this.thumb.transform.localScale = Vector3.one;
    if ((double) scrollValue >= 0.0 && (double) scrollValue <= 1.0)
      return;
    float num = (float) (1.0 / ((double) scrollValue >= 0.0 ? (double) scrollValue : -(double) scrollValue + 1.0));
    TransformUtil.SetPosY((Component) this.thumb, this.thumb.transform.position.y + (float) (((double) this.thumb.transform.position.y - (double) ((double) scrollValue >= 0.0 ? this.thumb.GetComponent<Collider>().bounds.min : this.thumb.GetComponent<Collider>().bounds.max).y) * ((double) num - 1.0)));
  }

  private void ThumbPressed(UIEvent e)
  {
    this.StartCoroutine(this.UpdateThumbDrag());
  }

  private void TrackPressed(UIEvent e)
  {
    Camera firstByLayer = CameraUtils.FindFirstByLayer(this.gameObject.layer);
    this.list.ScrollValue = (float) (((double) Mathf.Clamp(this.GetTouchPoint(new Plane(-firstByLayer.transform.forward, this.track.transform.position), firstByLayer).y, this.thumbMax.position.y, this.thumbMin.position.y) - (double) this.thumbMin.position.y) / ((double) this.thumbMax.position.y - (double) this.thumbMin.position.y));
  }

  [DebuggerHidden]
  private IEnumerator UpdateThumbDrag()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TouchListScrollbar.\u003CUpdateThumbDrag\u003Ec__Iterator349() { \u003C\u003Ef__this = this };
  }

  private Vector3 GetTouchPoint(Plane dragPlane, Camera camera)
  {
    Ray ray = camera.ScreenPointToRay(UniversalInputManager.Get().GetMousePosition());
    float enter;
    dragPlane.Raycast(ray, out enter);
    return ray.GetPoint(enter);
  }
}
