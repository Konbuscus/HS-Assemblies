﻿// Decompiled with JetBrains decompiler
// Type: BoosterPackReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class BoosterPackReward : Reward
{
  public bool m_RotateIn = true;
  public GameLayer m_Layer = GameLayer.IgnoreFullScreenEffects;
  public GameObject m_BoosterPackBone;
  public Material m_PackGlowMaterial;
  public AnimationCurve m_RotationCurve;
  private UnopenedPack m_unopenedPack;

  protected override void InitData()
  {
    this.SetData((RewardData) new BoosterPackRewardData(), false);
  }

  protected override void ShowReward(bool updateCacheValues)
  {
    this.m_root.SetActive(true);
    SceneUtils.SetLayer(this.m_root, this.m_Layer);
    Vector3 localScale = this.m_unopenedPack.transform.localScale;
    this.m_unopenedPack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    iTween.ScaleTo(this.m_unopenedPack.gameObject, iTween.Hash((object) "scale", (object) localScale, (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.easeOutElastic));
    if (!this.m_RotateIn)
      return;
    this.PlayRotateInAnimation();
  }

  protected override void HideReward()
  {
    base.HideReward();
    this.m_root.SetActive(false);
  }

  protected override void OnDataSet(bool updateVisuals)
  {
    if (!updateVisuals)
      return;
    this.m_BoosterPackBone.gameObject.SetActive(false);
    BoosterPackRewardData data = this.Data as BoosterPackRewardData;
    string empty1 = string.Empty;
    string empty2 = string.Empty;
    string source = string.Empty;
    string headline;
    if (this.Data.Origin == NetCache.ProfileNotice.NoticeOrigin.OUT_OF_BAND_LICENSE)
    {
      BoosterDbfRecord record = GameDbf.Booster.GetRecord(data.Id);
      if (record == null)
        return;
      headline = data.Count > 1 ? GameStrings.Get("GLOBAL_REWARD_BOOSTER_HEADLINE_OUT_OF_BAND_MULTI") : GameStrings.Get("GLOBAL_REWARD_BOOSTER_HEADLINE_OUT_OF_BAND");
      SpecialEventManager specialEventManager = SpecialEventManager.Get();
      SpecialEventType eventType = SpecialEventManager.GetEventType(record.BuyWithGoldEvent, SpecialEventType.UNKNOWN);
      if (!specialEventManager.IsEventActive(eventType, false) && (specialEventManager.GetEventLocalStartTime(eventType).HasValue && !specialEventManager.HasEventStarted(eventType)))
        source = GameStrings.Format("GLOBAL_REWARD_BOOSTER_DETAILS_PRESALE_OUT_OF_BAND", (object) data.Count);
      else
        source = GameStrings.Format("GLOBAL_REWARD_BOOSTER_DETAILS_OUT_OF_BAND", (object) data.Count);
    }
    else if (data.Count <= 1)
      headline = GameStrings.Get("GLOBAL_REWARD_BOOSTER_HEADLINE_GENERIC");
    else
      headline = GameStrings.Format("GLOBAL_REWARD_BOOSTER_HEADLINE_MULTIPLE", (object) data.Count);
    this.SetRewardText(headline, empty2, source);
    BoosterDbfRecord record1 = GameDbf.Booster.GetRecord(data.Id);
    if (record1 == null)
      return;
    this.SetReady(false);
    GameObject gameObject = AssetLoader.Get().LoadActor(FileUtils.GameAssetPathToName(record1.PackOpeningPrefab), false, false);
    gameObject.transform.parent = this.m_BoosterPackBone.transform.parent;
    gameObject.transform.position = this.m_BoosterPackBone.transform.position;
    gameObject.transform.rotation = this.m_BoosterPackBone.transform.rotation;
    gameObject.transform.localScale = this.m_BoosterPackBone.transform.localScale;
    this.m_unopenedPack = gameObject.GetComponent<UnopenedPack>();
    this.m_MeshRoot = gameObject;
    if ((Object) this.m_unopenedPack.m_SingleStack.m_MeshRenderer != (Object) null)
    {
      Texture mainTexture = this.m_unopenedPack.m_SingleStack.m_MeshRenderer.sharedMaterial.mainTexture;
      this.m_unopenedPack.m_SingleStack.m_MeshRenderer.material = this.m_PackGlowMaterial;
      this.m_unopenedPack.m_SingleStack.m_MeshRenderer.material.mainTexture = mainTexture;
      if ((Object) this.m_unopenedPack.m_SingleStack.m_Shadow != (Object) null)
        this.m_unopenedPack.m_SingleStack.m_Shadow.SetActive(false);
    }
    if ((Object) this.m_unopenedPack.m_MultipleStack.m_MeshRenderer != (Object) null)
    {
      Texture mainTexture = this.m_unopenedPack.m_MultipleStack.m_MeshRenderer.sharedMaterial.mainTexture;
      this.m_unopenedPack.m_MultipleStack.m_MeshRenderer.material = this.m_PackGlowMaterial;
      this.m_unopenedPack.m_MultipleStack.m_MeshRenderer.material.mainTexture = mainTexture;
      if ((Object) this.m_unopenedPack.m_MultipleStack.m_Shadow != (Object) null)
        this.m_unopenedPack.m_MultipleStack.m_Shadow.SetActive(false);
    }
    this.UpdatePackStacks();
    this.SetReady(true);
  }

  [ContextMenu("Play Rotate In Animation")]
  public void PlayRotateInAnimation()
  {
    this.StartCoroutine(this.RotateAnimation());
  }

  private void UpdatePackStacks()
  {
    BoosterPackRewardData data = this.Data as BoosterPackRewardData;
    if (data == null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("BoosterPackReward.UpdatePackStacks() - Data {0} is not CardRewardData", (object) this.Data));
    }
    else
    {
      this.m_unopenedPack.SetBoosterStack(new NetCache.BoosterStack()
      {
        Id = data.Id,
        Count = data.Count
      });
      bool flag = data.Count > 1;
      this.m_unopenedPack.m_SingleStack.m_RootObject.SetActive(!flag);
      this.m_unopenedPack.m_MultipleStack.m_RootObject.SetActive(flag);
      this.m_unopenedPack.m_MultipleStack.m_AmountText.enabled = flag;
      if (!flag)
        return;
      this.m_unopenedPack.m_MultipleStack.m_AmountText.Text = data.Count.ToString();
    }
  }

  [ContextMenu("Play Rotate In Animation")]
  [DebuggerHidden]
  private IEnumerator RotateAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BoosterPackReward.\u003CRotateAnimation\u003Ec__Iterator23F() { \u003C\u003Ef__this = this };
  }
}
