﻿// Decompiled with JetBrains decompiler
// Type: TraySection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TraySection : MonoBehaviour
{
  private static readonly Vector3 DECKBOX_LOCAL_EULER_ANGLES = new Vector3(90f, 180f, 0.0f);
  private readonly string DOOR_OPEN_ANIM_NAME = "Deck_DoorOpen";
  private readonly string DOOR_CLOSE_ANIM_NAME = "Deck_DoorClose";
  private bool m_showDoor = true;
  private const float DOOR_ANIM_SPEED = 6f;
  public GameObject m_door;
  public CollectionDeckBoxVisual m_deckBox;
  public Animator m_deckFX;
  private bool m_isOpen;
  private bool m_wasTouchModeEnabled;
  private bool m_deckBoxShown;
  private Transform m_parent;

  public void ShowDoor(bool show)
  {
    if (!this.m_showDoor)
      show = false;
    this.m_door.gameObject.SetActive(show);
  }

  public bool IsOpen()
  {
    return this.m_isOpen;
  }

  public Bounds GetDoorBounds()
  {
    return this.m_door.GetComponent<Renderer>().bounds;
  }

  public void OpenDoor()
  {
    this.OpenDoor((TraySection.DelOnDoorStateChangedCallback) null);
  }

  public void OpenDoor(TraySection.DelOnDoorStateChangedCallback callback)
  {
    this.OpenDoor(callback, (object) null);
  }

  public void OpenDoor(TraySection.DelOnDoorStateChangedCallback callback, object callbackData)
  {
    this.OpenDoor(false, callback, callbackData);
  }

  public void OpenDoorImmediately()
  {
    this.OpenDoorImmediately((TraySection.DelOnDoorStateChangedCallback) null);
  }

  public void OpenDoorImmediately(TraySection.DelOnDoorStateChangedCallback callback)
  {
    this.OpenDoorImmediately(callback, (object) null);
  }

  public void OpenDoorImmediately(TraySection.DelOnDoorStateChangedCallback callback, object callbackData)
  {
    this.OpenDoor(true, callback, callbackData);
  }

  public void CloseDoor()
  {
    this.CloseDoor((TraySection.DelOnDoorStateChangedCallback) null);
  }

  public void CloseDoor(TraySection.DelOnDoorStateChangedCallback callback)
  {
    this.CloseDoor(callback, (object) null);
  }

  public void CloseDoor(TraySection.DelOnDoorStateChangedCallback callback, object callbackData)
  {
    this.CloseDoor(false, callback, callbackData);
  }

  public void CloseDoorImmediately()
  {
    this.CloseDoorImmediately((TraySection.DelOnDoorStateChangedCallback) null);
  }

  public void CloseDoorImmediately(TraySection.DelOnDoorStateChangedCallback callback)
  {
    this.CloseDoorImmediately(callback, (object) null);
  }

  public void CloseDoorImmediately(TraySection.DelOnDoorStateChangedCallback callback, object callbackData)
  {
    this.CloseDoor(true, callback, callbackData);
  }

  public bool IsDeckBoxShown()
  {
    return this.m_deckBoxShown;
  }

  public void EnableDoors(bool show)
  {
    this.m_showDoor = show;
  }

  public void ShowDeckBox(bool immediate = false, TraySection.DelOnDoorStateChangedCallback callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TraySection.\u003CShowDeckBox\u003Ec__AnonStorey3AA boxCAnonStorey3Aa = new TraySection.\u003CShowDeckBox\u003Ec__AnonStorey3AA();
    // ISSUE: reference to a compiler-generated field
    boxCAnonStorey3Aa.callback = callback;
    // ISSUE: reference to a compiler-generated field
    boxCAnonStorey3Aa.\u003C\u003Ef__this = this;
    this.gameObject.SetActive(true);
    this.m_deckBoxShown = true;
    if (this.m_showDoor)
      this.m_door.gameObject.SetActive(true);
    // ISSUE: reference to a compiler-generated method
    this.OpenDoor(immediate, new TraySection.DelOnDoorStateChangedCallback(boxCAnonStorey3Aa.\u003C\u003Em__10F), (object) null);
  }

  public void ShowDeckBoxNoAnim()
  {
    this.gameObject.SetActive(true);
    this.m_deckBoxShown = true;
    this.m_deckBox.Show();
  }

  public void HideDeckBox(bool immediate = false, TraySection.DelOnDoorStateChangedCallback callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TraySection.\u003CHideDeckBox\u003Ec__AnonStorey3AB boxCAnonStorey3Ab = new TraySection.\u003CHideDeckBox\u003Ec__AnonStorey3AB();
    // ISSUE: reference to a compiler-generated field
    boxCAnonStorey3Ab.callback = callback;
    // ISSUE: reference to a compiler-generated field
    boxCAnonStorey3Ab.\u003C\u003Ef__this = this;
    this.m_deckBoxShown = false;
    // ISSUE: reference to a compiler-generated method
    this.CloseDoor(immediate, new TraySection.DelOnDoorStateChangedCallback(boxCAnonStorey3Ab.\u003C\u003Em__110), (object) null);
  }

  public void MoveDeckBoxToEditPosition(Vector3 position, float time, TraySection.DelOnDoorStateChangedCallback callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TraySection.\u003CMoveDeckBoxToEditPosition\u003Ec__AnonStorey3AC positionCAnonStorey3Ac = new TraySection.\u003CMoveDeckBoxToEditPosition\u003Ec__AnonStorey3AC();
    // ISSUE: reference to a compiler-generated field
    positionCAnonStorey3Ac.position = position;
    // ISSUE: reference to a compiler-generated field
    positionCAnonStorey3Ac.time = time;
    // ISSUE: reference to a compiler-generated field
    positionCAnonStorey3Ac.callback = callback;
    // ISSUE: reference to a compiler-generated field
    positionCAnonStorey3Ac.\u003C\u003Ef__this = this;
    if ((UnityEngine.Object) this.m_deckBox == (UnityEngine.Object) null)
      return;
    this.m_deckBox.DisableButtonAnimation();
    this.m_door.gameObject.SetActive(true);
    this.CloseDoor();
    // ISSUE: reference to a compiler-generated method
    this.m_deckBox.PlayScaleUpAnimation(new CollectionDeckBoxVisual.DelOnAnimationFinished(positionCAnonStorey3Ac.\u003C\u003Em__111));
  }

  public void MoveDeckBoxBackToOriginalPosition(float time, TraySection.DelOnDoorStateChangedCallback callback = null)
  {
    if ((UnityEngine.Object) this.m_deckBox == (UnityEngine.Object) null)
      return;
    this.OpenDoor((TraySection.DelOnDoorStateChangedCallback) (_1 => this.m_door.gameObject.SetActive(false)));
    this.StartCoroutine(this.MoveToOriginalPosition(time, callback));
  }

  [DebuggerHidden]
  private IEnumerator MoveToOriginalPosition(float time, TraySection.DelOnDoorStateChangedCallback callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TraySection.\u003CMoveToOriginalPosition\u003Ec__Iterator67()
    {
      time = time,
      callback = callback,
      \u003C\u0024\u003Etime = time,
      \u003C\u0024\u003Ecallback = callback,
      \u003C\u003Ef__this = this
    };
  }

  public void FlipDeckBoxHalfOverToShow(float animTime, TraySection.DelOnDoorStateChangedCallback callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TraySection.\u003CFlipDeckBoxHalfOverToShow\u003Ec__AnonStorey3AD showCAnonStorey3Ad = new TraySection.\u003CFlipDeckBoxHalfOverToShow\u003Ec__AnonStorey3AD();
    // ISSUE: reference to a compiler-generated field
    showCAnonStorey3Ad.callback = callback;
    // ISSUE: reference to a compiler-generated field
    showCAnonStorey3Ad.\u003C\u003Ef__this = this;
    this.m_deckBox.gameObject.SetActive(true);
    this.m_deckBox.transform.localEulerAngles = new Vector3(0.0f, 180f, 0.0f);
    SoundManager.Get().LoadAndPlay("collection_manager_new_deck_edge_flips", this.gameObject);
    iTween.StopByName(this.m_deckBox.gameObject, "rotation");
    // ISSUE: reference to a compiler-generated method
    iTween.RotateTo(this.m_deckBox.gameObject, iTween.Hash((object) "rotation", (object) TraySection.DECKBOX_LOCAL_EULER_ANGLES, (object) "isLocal", (object) true, (object) "time", (object) animTime, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) new Action<object>(showCAnonStorey3Ad.\u003C\u003Em__113), (object) "name", (object) "rotation"));
  }

  public void ClearDeckInfo()
  {
    if ((UnityEngine.Object) this.m_deckBox == (UnityEngine.Object) null)
      return;
    this.m_deckBox.SetDeckName(string.Empty);
    this.m_deckBox.SetDeckID(-1L);
  }

  public bool HideIfNotInBounds(Bounds bounds)
  {
    UIBScrollableItem component = this.GetComponent<UIBScrollableItem>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "UIBScrollableItem not found on a TraySection! This section may not be hidden properly while entering or exiting Collection Manager!");
      return false;
    }
    Bounds bounds1 = new Bounds();
    Vector3 min;
    Vector3 max;
    component.GetWorldBounds(out min, out max);
    bounds1.SetMinMax(min, max);
    if (bounds.Intersects(bounds1))
      return false;
    this.gameObject.SetActive(false);
    return true;
  }

  private void Awake()
  {
    if ((UnityEngine.Object) this.m_deckBox != (UnityEngine.Object) null)
    {
      this.m_deckBox.transform.localPosition = CollectionDeckBoxVisual.POPPED_DOWN_LOCAL_POS;
      this.m_deckBox.transform.localScale = new Vector3(0.95f, 0.95f, 0.95f);
      this.m_deckBox.transform.localEulerAngles = new Vector3(90f, 180f, 0.0f);
    }
    this.m_wasTouchModeEnabled = UniversalInputManager.Get().IsTouchMode();
    UIBScrollableItem component = this.GetComponent<UIBScrollableItem>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.SetCustomActiveState(new UIBScrollableItem.ActiveStateCallback(this.IsDeckBoxShown));
    this.m_parent = this.m_deckBox.transform.parent;
  }

  private void Update()
  {
    if (this.m_wasTouchModeEnabled == UniversalInputManager.Get().IsTouchMode())
      return;
    this.m_wasTouchModeEnabled = UniversalInputManager.Get().IsTouchMode();
  }

  private void OpenDoor(bool isImmediate, TraySection.DelOnDoorStateChangedCallback callback, object callbackData)
  {
    if (this.m_isOpen)
    {
      if (callback == null)
        return;
      callback(callbackData);
    }
    else
    {
      this.m_isOpen = true;
      this.m_door.GetComponent<Animation>()[this.DOOR_OPEN_ANIM_NAME].time = !isImmediate ? 0.0f : this.m_door.GetComponent<Animation>()[this.DOOR_OPEN_ANIM_NAME].length;
      this.m_door.GetComponent<Animation>()[this.DOOR_OPEN_ANIM_NAME].speed = 6f;
      this.PlayDoorAnimation(this.DOOR_OPEN_ANIM_NAME, callback, callbackData);
    }
  }

  private void CloseDoor(bool isImmediate, TraySection.DelOnDoorStateChangedCallback callback, object callbackData)
  {
    if (!this.m_isOpen)
    {
      if (callback == null)
        return;
      callback(callbackData);
    }
    else
    {
      this.m_isOpen = false;
      this.m_door.GetComponent<Animation>()[this.DOOR_CLOSE_ANIM_NAME].time = !isImmediate ? 0.0f : this.m_door.GetComponent<Animation>()[this.DOOR_CLOSE_ANIM_NAME].length;
      this.m_door.GetComponent<Animation>()[this.DOOR_CLOSE_ANIM_NAME].speed = 6f;
      this.PlayDoorAnimation(this.DOOR_CLOSE_ANIM_NAME, callback, callbackData);
    }
  }

  private void PlayDoorAnimation(string animationName, TraySection.DelOnDoorStateChangedCallback callback, object callbackData)
  {
    this.m_door.GetComponent<Animation>().Play(animationName);
    TraySection.OnDoorStateChangedCallbackData changedCallbackData = new TraySection.OnDoorStateChangedCallbackData()
    {
      m_callback = callback,
      m_callbackData = callbackData,
      m_animationName = animationName
    };
    this.StopCoroutine("WaitThenCallDoorAnimationCallback");
    this.StartCoroutine("WaitThenCallDoorAnimationCallback", (object) changedCallbackData);
  }

  [DebuggerHidden]
  private IEnumerator WaitThenCallDoorAnimationCallback(TraySection.OnDoorStateChangedCallbackData callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TraySection.\u003CWaitThenCallDoorAnimationCallback\u003Ec__Iterator68()
    {
      callbackData = callbackData,
      \u003C\u0024\u003EcallbackData = callbackData,
      \u003C\u003Ef__this = this
    };
  }

  private class OnDoorStateChangedCallbackData
  {
    public TraySection.DelOnDoorStateChangedCallback m_callback;
    public object m_callbackData;
    public string m_animationName;
  }

  public delegate void DelOnDoorStateChangedCallback(object callbackData);
}
