﻿// Decompiled with JetBrains decompiler
// Type: TavernBrawlDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class TavernBrawlDisplay : MonoBehaviour
{
  private static readonly PlatformDependentValue<string> DEFAULT_CHALKBOARD_TEXTURE_NAME_NO_DECK = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "TavernBrawl_Chalkboard_Default_NoBorders", Phone = "TavernBrawl_Chalkboard_Default_phone" };
  private static readonly PlatformDependentValue<string> DEFAULT_CHALKBOARD_TEXTURE_NAME_WITH_DECK = new PlatformDependentValue<string>(PlatformCategory.Screen) { PC = "TavernBrawl_Chalkboard_Default_Borders", Phone = "TavernBrawl_Chalkboard_Default_phone" };
  private static readonly PlatformDependentValue<UnityEngine.Vector2> DEFAULT_CHALKBOARD_TEXTURE_OFFSET_NO_DECK = new PlatformDependentValue<UnityEngine.Vector2>(PlatformCategory.Screen) { PC = UnityEngine.Vector2.zero, Phone = UnityEngine.Vector2.zero };
  private static readonly PlatformDependentValue<UnityEngine.Vector2> DEFAULT_CHALKBOARD_TEXTURE_OFFSET_WITH_DECK = new PlatformDependentValue<UnityEngine.Vector2>(PlatformCategory.Screen) { PC = UnityEngine.Vector2.zero, Phone = new UnityEngine.Vector2(0.0f, -0.389f) };
  public Color m_disabledTextColor = new Color(0.5f, 0.5f, 0.5f);
  private readonly string CARD_COUNT_PANEL_OPEN_ANIM = "TavernBrawl_DecksNumberCoverUp_Open";
  private readonly string CARD_COUNT_PANEL_CLOSE_ANIM = "TavernBrawl_DecksNumberCoverUp_Close";
  [CustomEditField(Sections = "Buttons")]
  public UIBButton m_createDeckButton;
  [CustomEditField(Sections = "Buttons")]
  public UIBButton m_editDeckButton;
  [CustomEditField(Sections = "Buttons")]
  public PlayButton m_playButton;
  [CustomEditField(Sections = "Buttons")]
  public UIBButton m_backButton;
  [CustomEditField(Sections = "Buttons")]
  public PegUIElement m_rewardChest;
  [CustomEditField(Sections = "Buttons")]
  public UIBButton m_viewDeckButton;
  [CustomEditField(Sections = "Strings")]
  public UberText m_chalkboardHeader;
  [CustomEditField(Sections = "Strings")]
  public UberText m_chalkboardInfo;
  [CustomEditField(Sections = "Strings")]
  public UberText m_chalkboardEndInfo;
  [CustomEditField(Sections = "Strings")]
  public UberText m_numWins;
  [CustomEditField(Sections = "Strings")]
  public UberText m_TavernBrawlHeadline;
  [CustomEditField(Sections = "Animating Elements")]
  public SlidingTray m_tavernBrawlTray;
  [CustomEditField(Sections = "Animating Elements")]
  public SlidingTray m_cardListPanel;
  [CustomEditField(Sections = "Animating Elements")]
  public Animation m_cardCountPanelAnim;
  [CustomEditField(Sections = "Animating Elements")]
  public GameObject m_rewardsPreview;
  [CustomEditField(Sections = "Animating Elements")]
  public GameObject m_rewardContainer;
  [CustomEditField(Sections = "Animating Elements")]
  public UberText m_rewardsText;
  [CustomEditField(Sections = "Animating Elements")]
  public Animator m_LockedDeckTray;
  [CustomEditField(Sections = "Animating Elements")]
  public TavernBrawlPhoneDeckTray m_PhoneDeckTrayView;
  [CustomEditField(Sections = "Animating Elements")]
  public DraftManaCurve m_ManaCurvePhone;
  [CustomEditField(Sections = "Highlights")]
  public HighlightState m_createDeckHighlight;
  [CustomEditField(Sections = "Highlights")]
  public HighlightState m_rewardHighlight;
  [CustomEditField(Sections = "Highlights")]
  public HighlightState m_editDeckHighlight;
  public GameObject m_winsBanner;
  public GameObject m_panelWithCreateDeck;
  public GameObject m_fullPanel;
  public GameObject m_chalkboard;
  public Material m_chestOpenMaterial;
  public float m_wipeAnimStartDelay;
  public PegUIElement m_rewardOffClickCatcher;
  public GameObject m_editIcon;
  public GameObject m_deleteIcon;
  public UberText m_editText;
  public GameObject m_lossesRoot;
  public LossMarks m_lossMarks;
  public GameObject m_rewardBoxesBone;
  public GameObject m_normalWinLocationBone;
  public GameObject m_sessionWinLocationBone;
  public PegUIElement m_LockedDeckTooltipTrigger;
  public TooltipZone m_LockedDeckTooltipZone;
  public Transform m_SocketHeroBone;
  public BoxCollider m_clickBlocker;
  public GameObject m_chalkboardFX;
  private static TavernBrawlDisplay s_instance;
  private bool m_doWipeAnimation;
  private long m_deckBeingEdited;
  private GameObject m_rewardObject;
  private Vector3 m_rewardsScale;
  private bool m_cardCountPanelAnimOpen;
  private Color? m_originalEditTextColor;
  private Color? m_originalEditIconColor;
  private TavernBrawlMission m_currentMission;
  private TavernBrawlStatus m_currentlyShowingMode;
  private bool m_firstTimeHeroicIntroShowing;
  private BannerPopup m_firstTimeHeroicIntroBanner;
  private Actor m_chosenHero;
  private Notification m_expoThankQuote;
  private bool m_tavernBrawlHasEndedDialogActive;

  private void Awake()
  {
    TavernBrawlDisplay.s_instance = this;
    this.transform.position = Vector3.zero;
    this.transform.localScale = Vector3.one;
    this.m_currentMission = TavernBrawlManager.Get().CurrentMission();
    this.Awake_InitializeRewardDisplay();
    this.SetupUniversalButtons();
    this.RegisterListeners();
    this.SetUIForFriendlyChallenge(FriendChallengeMgr.Get().IsChallengeTavernBrawl());
  }

  private void Start()
  {
    this.m_tavernBrawlTray.ToggleTraySlider(true, (Transform) null, false);
    if (PresenceMgr.Get().CurrentStatus != PresenceStatus.TAVERN_BRAWL_FRIENDLY_WAITING)
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TAVERN_BRAWL_SCREEN);
    this.RefreshStateBasedUI(false);
    this.RefreshDataBasedUI(this.m_wipeAnimStartDelay);
    if (this.m_currentMission == null)
      return;
    MusicManager.Get().StartPlaylist(!this.m_currentMission.IsSessionBased ? MusicPlaylistType.UI_TavernBrawl : MusicPlaylistType.UI_HeroicBrawl);
    this.InitExpoDemoMode();
    this.StartCoroutine(this.Start_WaitForDependencies());
  }

  private void OnDestroy()
  {
    this.HideDemoQuotes();
    TavernBrawlDisplay.s_instance = (TavernBrawlDisplay) null;
  }

  public static TavernBrawlDisplay Get()
  {
    return TavernBrawlDisplay.s_instance;
  }

  public void Unload()
  {
    SceneMgr.Get().UnregisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
    CollectionManager.Get().RemoveDeckCreatedListener(new CollectionManager.DelOnDeckCreated(this.OnDeckCreated));
    CollectionManager.Get().RemoveDeckDeletedListener(new CollectionManager.DelOnDeckDeleted(this.OnDeckDeleted));
    CollectionManager.Get().RemoveDeckContentsListener(new CollectionManager.DelOnDeckContents(this.OnDeckContents));
    FriendChallengeMgr.Get().RemoveChangedListener(new FriendChallengeMgr.ChangedCallback(this.OnFriendChallengeChanged));
    GameMgr.Get().UnregisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    TavernBrawlManager.Get().OnTavernBrawlUpdated -= new Action(this.OnTavernBrawlUpdated);
    if (FriendChallengeMgr.Get().IsChallengeTavernBrawl() && !SceneMgr.Get().IsInGame() && !SceneMgr.Get().IsModeRequested(SceneMgr.Mode.FRIENDLY))
      FriendChallengeMgr.Get().CancelChallenge();
    if (!this.IsInDeckEditMode())
      return;
    Navigation.Pop();
  }

  public void RefreshDataBasedUI(float animDelay = 0.0f)
  {
    if (this.m_currentlyShowingMode == TavernBrawlStatus.TB_STATUS_IN_REWARDS || this.m_tavernBrawlHasEndedDialogActive)
      return;
    this.RefreshTavernBrawlInfo(animDelay);
    if (this.m_currentMission == null)
      return;
    this.UpdateRecordUI();
    TavernBrawlManager.Get().SetFirstTimeSeeingCurrentSeason(false);
    if (this.m_currentMission.IsSessionBased && !this.m_firstTimeHeroicIntroShowing && (!Options.Get().GetBool(Option.HAS_SEEN_HEROIC_BRAWL, false) && UserAttentionManager.CanShowAttentionGrabber("TavernBrawlDisplay.RefreshDataBasedUI:" + (object) Option.HAS_SEEN_HEROIC_BRAWL)))
    {
      this.m_firstTimeHeroicIntroShowing = true;
      this.StartCoroutine(this.DoFirstTimeHeroicIntro());
    }
    else
    {
      if (this.m_firstTimeHeroicIntroShowing)
        return;
      TavernBrawlStatus playerStatus = TavernBrawlManager.Get().PlayerStatus;
      if (playerStatus != TavernBrawlStatus.TB_STATUS_TICKET_REQUIRED)
        StoreManager.Get().HideStore(StoreType.TAVERN_BRAWL_STORE);
      switch (playerStatus - 1)
      {
        case TavernBrawlStatus.TB_STATUS_INVALID:
          this.StartCoroutine(this.ShowPurchaseScreen());
          break;
        case TavernBrawlStatus.TB_STATUS_ACTIVE:
          this.StartCoroutine(this.ShowHeroicRewardsScreen());
          break;
        default:
          if (playerStatus != TavernBrawlStatus.TB_STATUS_ACTIVE && this.m_currentMission != null && this.m_currentMission.IsSessionBased)
          {
            UnityEngine.Debug.LogErrorFormat("TavernBrawlDisplay.UpdateDisplayState(): don't know how to handle currentStatus={0}. Kicking to HUB", (object) playerStatus);
            if (!SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
              SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
            DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
            {
              m_headerText = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_ERROR_TITLE"),
              m_text = GameStrings.Get("GLUE_HEROIC_BRAWL_SESSION_ERROR"),
              m_responseCallback = (AlertPopup.ResponseCallback) ((response, userData) => TavernBrawlManager.Get().RefreshServerData()),
              m_responseDisplay = AlertPopup.ResponseDisplay.OK,
              m_alertTextAlignment = UberText.AlignmentOptions.Center
            });
            break;
          }
          this.ShowActiveScreen(animDelay);
          break;
      }
    }
  }

  public bool IsInDeckEditMode()
  {
    return this.m_deckBeingEdited > 0L;
  }

  public bool IsInRewards()
  {
    return this.m_currentlyShowingMode == TavernBrawlStatus.TB_STATUS_IN_REWARDS;
  }

  public bool BackFromDeckEdit(bool animate)
  {
    if (!this.IsInDeckEditMode())
      return false;
    if (animate)
      PresenceMgr.Get().SetPrevStatus();
    if (CollectionManagerDisplay.Get().GetViewMode() != CollectionManagerDisplay.ViewMode.CARDS)
    {
      if (TavernBrawlManager.Get().CurrentDeck() == null)
        CollectionManagerDisplay.Get().SetViewMode(CollectionManagerDisplay.ViewMode.CARDS, (CollectionManagerDisplay.ViewModeData) null);
      else
        CollectionManagerDisplay.Get().m_pageManager.JumpToCollectionClassPage(TavernBrawlManager.Get().CurrentDeck().GetClass());
    }
    this.m_tavernBrawlTray.ToggleTraySlider(true, (Transform) null, animate);
    this.RefreshStateBasedUI(animate);
    this.m_deckBeingEdited = 0L;
    BnetBar.Get().m_currencyFrame.RefreshContents();
    FriendChallengeMgr.Get().UpdateMyAvailability();
    this.UpdateEditOrCreate();
    if (!(bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_editDeckButton.SetText(GameStrings.Get("GLUE_EDIT"));
      if ((UnityEngine.Object) this.m_editIcon != (UnityEngine.Object) null)
        this.m_editIcon.SetActive(true);
      if ((UnityEngine.Object) this.m_deleteIcon != (UnityEngine.Object) null)
        this.m_deleteIcon.SetActive(false);
    }
    CollectionDeckTray.Get().ExitEditDeckModeForTavernBrawl();
    return true;
  }

  public static bool IsTavernBrawlOpen()
  {
    return SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL && !((UnityEngine.Object) TavernBrawlDisplay.s_instance == (UnityEngine.Object) null);
  }

  public static bool IsTavernBrawlEditing()
  {
    if (TavernBrawlDisplay.IsTavernBrawlOpen())
      return TavernBrawlDisplay.s_instance.IsInDeckEditMode();
    return false;
  }

  public static bool IsTavernBrawlViewing()
  {
    if (TavernBrawlDisplay.IsTavernBrawlOpen())
      return !TavernBrawlDisplay.s_instance.IsInDeckEditMode();
    return false;
  }

  public void ValidateDeck()
  {
    if (this.m_currentMission == null)
    {
      this.m_playButton.Disable();
    }
    else
    {
      if (!this.m_currentMission.canCreateDeck)
        return;
      if (TavernBrawlManager.Get().HasValidDeck())
      {
        if (TavernBrawlManager.Get().PlayerStatus == TavernBrawlStatus.TB_STATUS_ACTIVE)
          this.m_playButton.Enable();
        this.m_editDeckHighlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
      }
      else
      {
        this.m_playButton.Disable();
        this.m_editDeckHighlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
      }
    }
  }

  public void EnablePlayButton()
  {
    if (this.m_currentMission == null || this.m_currentMission.canCreateDeck)
      this.ValidateDeck();
    else
      this.m_playButton.Enable();
  }

  private void RefreshTavernBrawlInfo(float animDelay)
  {
    this.UpdateEditOrCreate();
    this.m_currentMission = TavernBrawlManager.Get().CurrentMission();
    if (this.m_currentMission == null || this.m_currentMission.missionId < 0)
    {
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_TAVERN_BRAWL_HAS_ENDED_HEADER"),
        m_text = GameStrings.Get("GLUE_TAVERN_BRAWL_HAS_ENDED_TEXT"),
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_responseCallback = new AlertPopup.ResponseCallback(this.RefreshTavernBrawlInfo_ConfirmEnded),
        m_offset = new Vector3(0.0f, 104f, 0.0f),
        m_alertTextAlignment = UberText.AlignmentOptions.Center
      });
      this.m_tavernBrawlHasEndedDialogActive = true;
    }
    else
    {
      if (this.m_currentMission.rewardType == RewardType.REWARD_CHEST)
        this.m_rewardChest.gameObject.SetActive(false);
      if (this.m_currentMission.IsSessionBased)
      {
        if ((UnityEngine.Object) this.m_sessionWinLocationBone != (UnityEngine.Object) null)
          this.m_winsBanner.transform.position = this.m_sessionWinLocationBone.transform.position;
        if ((UnityEngine.Object) this.m_lossMarks != (UnityEngine.Object) null)
        {
          this.m_lossesRoot.SetActive(true);
          this.m_lossMarks.Init(this.m_currentMission.maxLosses);
        }
        this.m_TavernBrawlHeadline.Text = GameStrings.Get("GLOBAL_HEROIC_BRAWL");
      }
      else
      {
        if ((UnityEngine.Object) this.m_normalWinLocationBone != (UnityEngine.Object) null)
          this.m_winsBanner.transform.position = this.m_normalWinLocationBone.transform.position;
        if ((UnityEngine.Object) this.m_lossMarks != (UnityEngine.Object) null)
          this.m_lossesRoot.SetActive(false);
        this.m_TavernBrawlHeadline.Text = GameStrings.Get("GLOBAL_TAVERN_BRAWL");
      }
      if (DemoMgr.Get().IsExpoDemo())
      {
        string str = Vars.Key("Demo.Header").GetStr(string.Empty);
        if (!string.IsNullOrEmpty(str))
          this.m_TavernBrawlHeadline.Text = str;
      }
      ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(this.m_currentMission.missionId);
      this.m_chalkboardHeader.Text = (string) record.Name;
      this.m_chalkboardInfo.Text = (string) record.Description;
      this.CancelInvoke("UpdateTimeText");
      this.InvokeRepeating("UpdateTimeText", 0.1f, 0.1f);
      this.UpdateTimeText();
    }
  }

  private void RefreshTavernBrawlInfo_ConfirmEnded(AlertPopup.Response response, object userData)
  {
    if ((UnityEngine.Object) TavernBrawlDisplay.s_instance == (UnityEngine.Object) null)
      return;
    Navigation.Clear();
    this.OnNavigateBack();
  }

  private void SetUIForFriendlyChallenge(bool isTavernBrawlChallenge)
  {
    string key = "GLUE_BRAWL";
    if (TavernBrawlManager.Get().SelectHeroBeforeMission())
      key = "GLUE_CHOOSE";
    else if (isTavernBrawlChallenge && !DemoMgr.Get().IsExpoDemo())
      key = "GLUE_BRAWL_FRIEND";
    this.m_playButton.SetText(GameStrings.Get(key));
    this.m_rewardChest.gameObject.SetActive(!isTavernBrawlChallenge);
    this.m_winsBanner.SetActive(!isTavernBrawlChallenge);
    if ((UnityEngine.Object) this.m_lossMarks != (UnityEngine.Object) null)
      this.m_lossMarks.gameObject.SetActive(!isTavernBrawlChallenge);
    if (!((UnityEngine.Object) this.m_editDeckButton != (UnityEngine.Object) null))
      return;
    if (!this.m_originalEditTextColor.HasValue)
      this.m_originalEditTextColor = new Color?(this.m_editText.TextColor);
    if (isTavernBrawlChallenge)
    {
      this.m_editText.TextColor = this.m_disabledTextColor;
      this.m_editDeckButton.SetEnabled(false);
    }
    else
    {
      this.m_editText.TextColor = this.m_originalEditTextColor.Value;
      this.m_editDeckButton.SetEnabled(true);
    }
    if (!((UnityEngine.Object) this.m_editIcon != (UnityEngine.Object) null))
      return;
    if (!this.m_originalEditIconColor.HasValue)
      this.m_originalEditIconColor = new Color?(this.m_editIcon.GetComponent<Renderer>().material.color);
    if (isTavernBrawlChallenge)
      this.m_editIcon.GetComponent<Renderer>().material.color = this.m_disabledTextColor;
    else
      this.m_editIcon.GetComponent<Renderer>().material.color = this.m_originalEditIconColor.Value;
  }

  private void UpdateChalkboardVisual(float animDelay)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(this.m_currentMission.missionId);
    if (!((UnityEngine.Object) this.m_chalkboard != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_chalkboard.GetComponent<MeshRenderer>() != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_chalkboard.GetComponent<MeshRenderer>().material != (UnityEngine.Object) null))
      return;
    Material material = this.m_chalkboard.GetComponent<MeshRenderer>().material;
    string path = record.TbTexture;
    UnityEngine.Vector2 offset = UnityEngine.Vector2.zero;
    if (PlatformSettings.Screen == ScreenCategory.Phone)
    {
      path = record.TbTexturePhone;
      offset.y = record.TbTexturePhoneOffsetY;
    }
    Texture texture = !string.IsNullOrEmpty(path) ? AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(path), false) : (Texture) null;
    if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
    {
      bool canCreateDeck = this.m_currentMission.canCreateDeck;
      string name = (string) (!canCreateDeck ? TavernBrawlDisplay.DEFAULT_CHALKBOARD_TEXTURE_NAME_NO_DECK : TavernBrawlDisplay.DEFAULT_CHALKBOARD_TEXTURE_NAME_WITH_DECK);
      offset = (UnityEngine.Vector2) (!canCreateDeck ? TavernBrawlDisplay.DEFAULT_CHALKBOARD_TEXTURE_OFFSET_NO_DECK : TavernBrawlDisplay.DEFAULT_CHALKBOARD_TEXTURE_OFFSET_WITH_DECK);
      texture = AssetLoader.Get().LoadTexture(name, false);
    }
    if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
    {
      material.SetTexture("_TopTex", texture);
      material.SetTextureOffset("_MainTex", offset);
    }
    this.StartCoroutine(this.WaitThenPlayWipeAnim(!this.m_doWipeAnimation ? 0.0f : animDelay));
  }

  private void UpdateTimeText()
  {
    if (DemoMgr.Get().IsExpoDemo())
      return;
    string endingTimeText = TavernBrawlManager.Get().EndingTimeText;
    if (endingTimeText == null)
      this.CancelInvoke("UpdateTimeText");
    else
      this.m_chalkboardEndInfo.Text = endingTimeText;
  }

  private void UpdateRecordUI()
  {
    this.m_numWins.Text = TavernBrawlManager.Get().GamesWon.ToString();
    if (this.m_currentMission.IsSessionBased)
    {
      this.m_lossMarks.SetNumMarked(TavernBrawlManager.Get().GamesLost);
    }
    else
    {
      if (TavernBrawlManager.Get().RewardProgress <= 0)
        return;
      this.m_rewardChest.GetComponent<Renderer>().material = this.m_chestOpenMaterial;
      this.m_rewardHighlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
      this.m_rewardChest.SetEnabled(false);
    }
  }

  [DebuggerHidden]
  private IEnumerator DoFirstTimeHeroicIntro()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlDisplay.\u003CDoFirstTimeHeroicIntro\u003Ec__Iterator281() { \u003C\u003Ef__this = this };
  }

  private static void OnFirstTimeHeroicIntroCreated(BannerPopup popup)
  {
    TavernBrawlDisplay tavernBrawlDisplay = TavernBrawlDisplay.Get();
    if ((UnityEngine.Object) tavernBrawlDisplay == (UnityEngine.Object) null)
      return;
    tavernBrawlDisplay.m_firstTimeHeroicIntroBanner = popup;
  }

  private static void OnFirstTimeHeroicIntroClosed()
  {
    TavernBrawlDisplay tavernBrawlDisplay = TavernBrawlDisplay.Get();
    if ((UnityEngine.Object) tavernBrawlDisplay == (UnityEngine.Object) null)
      return;
    tavernBrawlDisplay.m_firstTimeHeroicIntroBanner = (BannerPopup) null;
    tavernBrawlDisplay.m_firstTimeHeroicIntroShowing = false;
    Options.Get().SetBool(Option.HAS_SEEN_HEROIC_BRAWL, true);
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL)
      return;
    tavernBrawlDisplay.RefreshDataBasedUI(0.0f);
  }

  private void RegisterListeners()
  {
    SceneMgr.Get().RegisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.OnScenePreUnload));
    CollectionManager.Get().RegisterDeckCreatedListener(new CollectionManager.DelOnDeckCreated(this.OnDeckCreated));
    CollectionManager.Get().RegisterDeckDeletedListener(new CollectionManager.DelOnDeckDeleted(this.OnDeckDeleted));
    CollectionManager.Get().RegisterDeckContentsListener(new CollectionManager.DelOnDeckContents(this.OnDeckContents));
    FriendChallengeMgr.Get().AddChangedListener(new FriendChallengeMgr.ChangedCallback(this.OnFriendChallengeChanged));
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    TavernBrawlManager.Get().OnTavernBrawlUpdated += new Action(this.OnTavernBrawlUpdated);
    if (this.m_currentMission != null && this.m_currentMission.canEditDeck)
      return;
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  private void Start_ShowAttentionGrabbers()
  {
    if (this.m_currentMission == null)
      return;
    bool flag = UserAttentionManager.CanShowAttentionGrabber("TavernBrawlDisplay.Show");
    int num1 = Options.Get().GetInt(Option.LATEST_SEEN_TAVERNBRAWL_SEASON_CHALKBOARD);
    if (num1 == 0)
    {
      this.m_doWipeAnimation = true;
      if (flag && !NotificationManager.Get().HasSoundPlayedThisSession("VO_INNKEEPER_TAVERNBRAWL_WELCOME1_27"))
      {
        NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_TAVERNBRAWL_WELCOME1_27"), "VO_INNKEEPER_TAVERNBRAWL_WELCOME1_27", 0.0f, (Action) null, false);
        NotificationManager.Get().ForceAddSoundToPlayedList("VO_INNKEEPER_TAVERNBRAWL_WELCOME1_27");
      }
    }
    else if (num1 < this.m_currentMission.seasonId)
    {
      this.m_doWipeAnimation = true;
      int num2 = Options.Get().GetInt(Option.TIMES_SEEN_TAVERNBRAWL_CRAZY_RULES_QUOTE);
      if (flag && !NotificationManager.Get().HasSoundPlayedThisSession("VO_INNKEEPER_TAVERNBRAWL_DESC2_30") && num2 < 3)
      {
        NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_TAVERNBRAWL_DESC2_30"), "VO_INNKEEPER_TAVERNBRAWL_DESC2_30", 0.0f, (Action) null, false);
        NotificationManager.Get().ForceAddSoundToPlayedList("VO_INNKEEPER_TAVERNBRAWL_DESC2_30");
        Options.Get().SetInt(Option.TIMES_SEEN_TAVERNBRAWL_CRAZY_RULES_QUOTE, num2 + 1);
      }
    }
    if (!flag || num1 == this.m_currentMission.seasonId)
      return;
    Options.Get().SetInt(Option.LATEST_SEEN_TAVERNBRAWL_SEASON_CHALKBOARD, this.m_currentMission.seasonId);
  }

  [DebuggerHidden]
  private IEnumerator Start_WaitForDependencies()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlDisplay.\u003CStart_WaitForDependencies\u003Ec__Iterator282() { \u003C\u003Ef__this = this };
  }

  private void Start_ShowQuestUpdates()
  {
    bool flag = true;
    if ((AchieveManager.Get().HasActiveQuests(true) || AchieveManager.Get().HasActiveAutoDestroyQuests()) && TavernBrawlManager.Get().PlayerStatus != TavernBrawlStatus.TB_STATUS_IN_REWARDS)
    {
      WelcomeQuests.Show(UserAttentionBlocker.NONE, false, (WelcomeQuests.DelOnWelcomeQuestsClosed) null, false);
      flag = false;
    }
    if (!flag)
      return;
    GameToastMgr.Get().UpdateQuestProgressToasts();
  }

  private void OnTavernBrawlUpdated()
  {
    this.m_currentMission = TavernBrawlManager.Get().CurrentMission();
    this.RefreshDataBasedUI(0.0f);
  }

  [DebuggerHidden]
  private IEnumerator ShowPurchaseScreen()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlDisplay.\u003CShowPurchaseScreen\u003Ec__Iterator283() { \u003C\u003Ef__this = this };
  }

  private void ShowActiveScreen(float animDelay)
  {
    if (this.m_currentlyShowingMode == TavernBrawlStatus.TB_STATUS_ACTIVE)
      return;
    this.m_currentlyShowingMode = TavernBrawlStatus.TB_STATUS_ACTIVE;
    this.Start_ShowAttentionGrabbers();
    this.UpdateChalkboardVisual(animDelay);
    this.UpdateDeckUI(false);
  }

  [DebuggerHidden]
  private IEnumerator ShowHeroicRewardsScreen()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlDisplay.\u003CShowHeroicRewardsScreen\u003Ec__Iterator284() { \u003C\u003Ef__this = this };
  }

  private void OnRewardsDone()
  {
    if ((UnityEngine.Object) this == (UnityEngine.Object) null || (UnityEngine.Object) this.gameObject == (UnityEngine.Object) null)
      return;
    Network.AckTavernBrawlSessionRewards();
    this.OnOpenRewardsComplete();
  }

  public void OnOpenRewardsComplete()
  {
    this.ExitScene();
  }

  private void ExitScene()
  {
    GameMgr.Get().CancelFindGame();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      this.m_playButton.Disable();
    StoreManager.Get().HideStore(StoreType.TAVERN_BRAWL_STORE);
    SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
  }

  private void UpdateDeckUI(bool animate)
  {
    this.UpdateDeckPanels(animate);
    this.ValidateDeck();
  }

  private bool OnNavigateBack()
  {
    this.m_tavernBrawlTray.m_animateBounce = false;
    this.m_tavernBrawlTray.ShowTray();
    this.ExitScene();
    return true;
  }

  private void OnBackButton()
  {
    Navigation.GoBack();
  }

  private void OnStoreBackButtonPressed(bool authorizationBackButtonPressed, object userData)
  {
    this.ExitScene();
  }

  private void RefreshStateBasedUI(bool animate)
  {
    this.UpdateDeckUI(animate);
  }

  private void UpdateEditOrCreate()
  {
    bool flag1 = this.m_currentMission != null && this.m_currentMission.canCreateDeck;
    bool flag2 = this.m_currentMission != null && this.m_currentMission.canEditDeck && !TavernBrawlManager.Get().IsDeckLocked;
    bool flag3 = TavernBrawlManager.Get().HasCreatedDeck();
    bool isDeckLocked = TavernBrawlManager.Get().IsDeckLocked;
    bool flag4 = (bool) UniversalInputManager.UsePhoneUI && isDeckLocked;
    bool flag5 = flag1 && !flag3;
    bool flag6 = flag4;
    bool flag7 = flag2 && flag1 && flag3 && !flag6;
    if ((UnityEngine.Object) this.m_viewDeckButton != (UnityEngine.Object) null)
      this.m_viewDeckButton.gameObject.SetActive(flag6);
    if ((UnityEngine.Object) this.m_editDeckButton != (UnityEngine.Object) null)
    {
      this.m_editDeckButton.gameObject.SetActive(flag7);
      if (TavernBrawlManager.Get().IsDeckLocked)
      {
        if ((bool) UniversalInputManager.UsePhoneUI)
        {
          this.m_PhoneDeckTrayView.Initialize();
          this.InitializeDeckTrayManaCurve();
          this.LoadAndPositionPhoneDeckTrayHeroCard();
        }
        else
        {
          CollectionDeckTray.Get().m_cardsContent.UpdateDeckCompleteHighlight();
          if (SceneMgr.Get().GetPrevMode() == SceneMgr.Mode.GAMEPLAY && GameMgr.Get().WasTavernBrawl() && TavernBrawlManager.Get().GamesWon + TavernBrawlManager.Get().GamesLost == 1)
            this.StartCoroutine(this.DoDeckTrayLockedAnimation());
          else
            this.ShowDeckTrayLocked();
        }
      }
      if ((UnityEngine.Object) this.m_editIcon != (UnityEngine.Object) null)
        this.m_editIcon.SetActive(true);
    }
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      if ((UnityEngine.Object) this.m_createDeckButton != (UnityEngine.Object) null)
        this.m_createDeckButton.gameObject.SetActive(flag5);
    }
    else
    {
      if ((UnityEngine.Object) this.m_panelWithCreateDeck != (UnityEngine.Object) null)
        this.m_panelWithCreateDeck.SetActive(flag5);
      if ((UnityEngine.Object) this.m_fullPanel != (UnityEngine.Object) null)
        this.m_fullPanel.SetActive(!flag5);
    }
    if (!((UnityEngine.Object) this.m_createDeckHighlight != (UnityEngine.Object) null))
      return;
    if (!this.m_createDeckHighlight.gameObject.activeInHierarchy && flag5)
      UnityEngine.Debug.LogWarning((object) "Attempting to activate m_createDeckHighlight, but it is inactive! This will not behave correctly!");
    this.m_createDeckHighlight.ChangeState(!flag5 ? ActorStateType.HIGHLIGHT_OFF : ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }

  private void LoadAndPositionPhoneDeckTrayHeroCard()
  {
    if ((UnityEngine.Object) this.m_chosenHero != (UnityEngine.Object) null)
      return;
    CollectionDeck collectionDeck = TavernBrawlManager.Get().CurrentDeck();
    if (collectionDeck == null)
      Log.TavernBrawl.PrintError("TavernBrawlManager.LoadAndPositionPhoneDeckTrayHeroCard: No deck found but trying to load the deck tray!");
    else
      GameUtils.LoadAndPositionHeroCard(collectionDeck.HeroCardID, collectionDeck.HeroPremium, new GameUtils.LoadHeroActorCallback(this.OnHeroActorLoaded));
  }

  private void OnHeroActorLoaded(Actor actor)
  {
    this.m_chosenHero = actor;
    this.m_chosenHero.transform.parent = this.m_SocketHeroBone.transform;
    this.m_chosenHero.transform.localPosition = Vector3.zero;
    this.m_chosenHero.transform.localScale = Vector3.one;
  }

  [DebuggerHidden]
  private IEnumerator DoDeckTrayLockedAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlDisplay.\u003CDoDeckTrayLockedAnimation\u003Ec__Iterator285() { \u003C\u003Ef__this = this };
  }

  private void ShowDeckTrayLocked()
  {
    this.m_LockedDeckTray.enabled = true;
    this.m_LockedDeckTooltipZone.GetComponent<BoxCollider>().enabled = true;
  }

  private void InitializeDeckTrayManaCurve()
  {
    CollectionDeck collectionDeck = TavernBrawlManager.Get().CurrentDeck();
    if (collectionDeck == null)
      return;
    using (List<CollectionDeckSlot>.Enumerator enumerator = collectionDeck.GetSlots().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        EntityDef entityDef = DefLoader.Get().GetEntityDef(current.CardID);
        for (int index = 0; index < current.Count; ++index)
          this.AddCardToManaCurve(entityDef);
      }
    }
  }

  public void AddCardToManaCurve(EntityDef entityDef)
  {
    if ((UnityEngine.Object) this.m_ManaCurvePhone == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) string.Format("TavernBrawlDisplay.AddCardToManaCurve({0}) - m_manaCurve is null", (object) entityDef));
    else
      this.m_ManaCurvePhone.AddCardToManaCurve(entityDef);
  }

  private void UpdateDeckPanels(bool animate = true)
  {
    this.UpdateDeckPanels(this.m_currentMission != null && this.m_currentMission.canCreateDeck && TavernBrawlManager.Get().HasCreatedDeck(), animate);
  }

  private void UpdateDeckPanels(bool hasDeck, bool animate)
  {
    if ((UnityEngine.Object) this.m_cardListPanel != (UnityEngine.Object) null)
    {
      bool show = !hasDeck;
      if (animate && !show)
      {
        this.m_createDeckButton.gameObject.SetActive(false);
        this.m_createDeckHighlight.gameObject.SetActive(false);
      }
      else if (show)
      {
        this.m_createDeckButton.gameObject.SetActive(true);
        this.m_createDeckHighlight.gameObject.SetActive(true);
      }
      this.m_cardListPanel.ToggleTraySlider(show, (Transform) null, animate);
    }
    if (!((UnityEngine.Object) this.m_cardCountPanelAnim != (UnityEngine.Object) null) || this.m_cardCountPanelAnimOpen == hasDeck)
      return;
    this.m_cardCountPanelAnim.Play(!hasDeck ? this.CARD_COUNT_PANEL_CLOSE_ANIM : this.CARD_COUNT_PANEL_OPEN_ANIM);
    this.m_cardCountPanelAnimOpen = hasDeck;
  }

  private void CreateDeck()
  {
    PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TAVERN_BRAWL_DECKEDITOR);
    CollectionManagerDisplay.Get().EnterSelectNewDeckHeroMode();
    this.HideChalkboardFX();
  }

  private void EditDeckButton_OnRelease(UIEvent e)
  {
    if (this.IsInDeckEditMode())
    {
      this.OnDeleteButtonPressed();
    }
    else
    {
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TAVERN_BRAWL_DECKEDITOR);
      if (!this.SwitchToEditDeckMode(TavernBrawlManager.Get().CurrentDeck()))
        ;
    }
  }

  private void ViewDeckButton_OnRelease(UIEvent e)
  {
    this.m_PhoneDeckTrayView.gameObject.GetComponent<SlidingTray>().ShowTray();
  }

  private bool SwitchToEditDeckMode(CollectionDeck deck)
  {
    if ((UnityEngine.Object) CollectionManagerDisplay.Get() == (UnityEngine.Object) null || deck == null)
      return false;
    this.m_tavernBrawlTray.HideTray();
    this.UpdateDeckPanels(true, true);
    if (!(bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_editDeckButton.gameObject.SetActive(this.m_currentMission.canEditDeck);
      this.m_editDeckButton.SetText(GameStrings.Get("GLUE_COLLECTION_DECK_DELETE"));
      if ((UnityEngine.Object) this.m_editIcon != (UnityEngine.Object) null)
        this.m_editIcon.SetActive(false);
      if ((UnityEngine.Object) this.m_deleteIcon != (UnityEngine.Object) null)
        this.m_deleteIcon.SetActive(true);
      this.m_editDeckHighlight.ChangeState(ActorStateType.HIGHLIGHT_OFF);
    }
    this.m_deckBeingEdited = deck.ID;
    BnetBar.Get().m_currencyFrame.RefreshContents();
    CollectionDeckTray.Get().EnterEditDeckModeForTavernBrawl();
    FriendChallengeMgr.Get().UpdateMyAvailability();
    return true;
  }

  private void ShowNonSessionRewardPreview(UIEvent e)
  {
    if (this.m_currentMission == null)
      return;
    switch (this.m_currentMission.rewardType)
    {
      case RewardType.REWARD_BOOSTER_PACKS:
        if ((UnityEngine.Object) this.m_rewardObject == (UnityEngine.Object) null)
        {
          int rewardData1 = (int) this.m_currentMission.RewardData1;
          BoosterDbfRecord record = GameDbf.Booster.GetRecord(rewardData1);
          if (record == null)
          {
            UnityEngine.Debug.LogErrorFormat("TavernBrawlDisplay.ShowReward() - no record found for booster {0}!", (object) rewardData1);
            return;
          }
          string packOpeningPrefab = record.PackOpeningPrefab;
          if (string.IsNullOrEmpty(packOpeningPrefab))
          {
            UnityEngine.Debug.LogErrorFormat("TavernBrawlDisplay.ShowReward() - no prefab found for booster {0}!", (object) rewardData1);
            return;
          }
          GameObject gameObject = AssetLoader.Get().LoadActor(FileUtils.GameAssetPathToName(packOpeningPrefab), false, false);
          if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
          {
            UnityEngine.Debug.LogError((object) string.Format("TavernBrawlDisplay.ShowReward() - failed to load prefab {0} for booster {1}!", (object) packOpeningPrefab, (object) rewardData1));
            return;
          }
          this.m_rewardObject = gameObject;
          UnopenedPack component = gameObject.GetComponent<UnopenedPack>();
          if ((UnityEngine.Object) component == (UnityEngine.Object) null)
          {
            UnityEngine.Debug.LogError((object) string.Format("TavernBrawlDisplay.ShowReward() - No UnopenedPack script found on prefab {0} for booster {1}!", (object) packOpeningPrefab, (object) rewardData1));
            return;
          }
          GameUtils.SetParent(this.m_rewardObject, this.m_rewardContainer, false);
          component.AddBooster();
          break;
        }
        break;
      case RewardType.REWARD_CARD_BACK:
        if ((UnityEngine.Object) this.m_rewardObject == (UnityEngine.Object) null)
        {
          int rewardData1 = (int) this.m_currentMission.RewardData1;
          CardBackManager.LoadCardBackData loadCardBackData = CardBackManager.Get().LoadCardBackByIndex(rewardData1, false, "Card_Hidden");
          if (loadCardBackData == null)
          {
            UnityEngine.Debug.LogErrorFormat("TavernBrawlDisplay.ShowReward() - Could not load cardback ID {0}!", (object) rewardData1);
            return;
          }
          this.m_rewardObject = loadCardBackData.m_GameObject;
          GameUtils.SetParent(this.m_rewardObject, this.m_rewardContainer, false);
          this.m_rewardObject.transform.localScale = Vector3.one * 5.92f;
          break;
        }
        break;
      default:
        UnityEngine.Debug.LogErrorFormat("Tavern Brawl reward type currently not supported! Add type {0} to TaverBrawlDisplay.ShowReward().", (object) this.m_currentMission.rewardType);
        return;
    }
    this.m_rewardsPreview.SetActive(true);
    iTween.Stop(this.m_rewardsPreview);
    iTween.ScaleTo(this.m_rewardsPreview, iTween.Hash((object) "scale", (object) this.m_rewardsScale, (object) "time", (object) 0.15f));
  }

  private void HideNonSessionRewardPreview(UIEvent e)
  {
    iTween.Stop(this.m_rewardsPreview);
    iTween.ScaleTo(this.m_rewardsPreview, iTween.Hash((object) "scale", (object) (Vector3.one * 0.01f), (object) "time", (object) 0.15f, (object) "oncomplete", (object) (Action<object>) (o => this.m_rewardsPreview.SetActive(false))));
  }

  private void PlayButton_OnRelease(UIEvent e)
  {
    if (this.m_currentMission == null)
      this.RefreshDataBasedUI(0.0f);
    else if (this.m_currentMission.IsSessionBased && this.m_currentMission.canEditDeck && !TavernBrawlManager.Get().IsDeckLocked)
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_HEROIC_BRAWL_PLAY_CONFIRMATION_TITLE"),
        m_text = GameStrings.Get("GLUE_HEROIC_BRAWL_PLAY_CONFIRMATION"),
        m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
        m_confirmText = GameStrings.Get("GLUE_HEROIC_BRAWL_PLAY_CONFIRMATION_OK"),
        m_cancelText = GameStrings.Get("GLUE_HEROIC_BRAWL_PLAY_CONFIRMATION_CANCEL"),
        m_responseCallback = new AlertPopup.ResponseCallback(this.OnPlayButtonConfirmationResponse),
        m_alertTextAlignment = UberText.AlignmentOptions.Center
      });
    else
      this.OnPlayButtonExecute();
  }

  private void OnPlayButtonConfirmationResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
      return;
    this.OnPlayButtonExecute();
  }

  private void OnPlayButtonExecute()
  {
    if (TavernBrawlManager.Get().SelectHeroBeforeMission())
    {
      if ((UnityEngine.Object) HeroPickerDisplay.Get() == (UnityEngine.Object) null)
      {
        AssetLoader.Get().LoadActor("HeroPicker", false, false);
        this.HideChalkboardFX();
      }
      else
      {
        Log.JMac.PrintWarning("Attempting to load HeroPickerDisplay a second time!");
        return;
      }
    }
    else if (this.m_currentMission.canCreateDeck)
    {
      if (TavernBrawlManager.Get().HasValidDeck())
      {
        CollectionDeck collectionDeck = TavernBrawlManager.Get().CurrentDeck();
        if (FriendChallengeMgr.Get().IsChallengeTavernBrawl())
        {
          FriendChallengeMgr.Get().SelectDeck(collectionDeck.ID);
          FriendlyChallengeHelper.Get().StartChallengeOrWaitForOpponent("GLOBAL_FRIEND_CHALLENGE_TAVERN_BRAWL_OPPONENT_WAITING_READY", new AlertPopup.ResponseCallback(this.OnFriendChallengeWaitingForOpponentDialogResponse));
        }
        else
          TavernBrawlManager.Get().StartGame(collectionDeck.ID);
      }
      else
      {
        UnityEngine.Debug.LogError((object) "Attempting to start a Tavern Brawl game without having a valid deck!");
        return;
      }
    }
    else if (FriendChallengeMgr.Get().IsChallengeTavernBrawl())
    {
      FriendChallengeMgr.Get().SkipDeckSelection();
      FriendlyChallengeHelper.Get().StartChallengeOrWaitForOpponent("GLOBAL_FRIEND_CHALLENGE_TAVERN_BRAWL_OPPONENT_WAITING_READY", new AlertPopup.ResponseCallback(this.OnFriendChallengeWaitingForOpponentDialogResponse));
    }
    else
      TavernBrawlManager.Get().StartGame(0L);
    this.m_playButton.Disable();
  }

  private void OnScenePreUnload(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    if (prevMode != SceneMgr.Mode.TAVERN_BRAWL || !((UnityEngine.Object) this.m_firstTimeHeroicIntroBanner != (UnityEngine.Object) null))
      return;
    this.m_firstTimeHeroicIntroBanner.Close();
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    switch (eventData.m_state)
    {
      case FindGameState.CLIENT_CANCELED:
      case FindGameState.CLIENT_ERROR:
      case FindGameState.BNET_QUEUE_CANCELED:
      case FindGameState.BNET_ERROR:
      case FindGameState.SERVER_GAME_CANCELED:
        this.HandleGameStartupFailure();
        break;
      case FindGameState.SERVER_GAME_STARTED:
        FriendChallengeMgr.Get().RemoveChangedListener(new FriendChallengeMgr.ChangedCallback(this.OnFriendChallengeChanged));
        break;
    }
    return false;
  }

  private void HandleGameStartupFailure()
  {
    if (TavernBrawlManager.Get().SelectHeroBeforeMission())
      return;
    this.EnablePlayButton();
  }

  private void OnDeleteButtonPressed()
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_COLLECTION_DELETE_CONFIRM_HEADER"),
      m_text = GameStrings.Get("GLUE_COLLECTION_DELETE_CONFIRM_DESC"),
      m_alertTextAlignment = UberText.AlignmentOptions.Center,
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_responseCallback = new AlertPopup.ResponseCallback(this.OnDeleteButtonConfirmationResponse),
      m_alertTextAlignment = UberText.AlignmentOptions.Center
    });
  }

  private void OnDeleteButtonConfirmationResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
      return;
    CollectionDeckTray.Get().DeleteEditingDeck(true);
    if (!(bool) ((UnityEngine.Object) CollectionManagerDisplay.Get()))
      return;
    CollectionManagerDisplay.Get().OnDoneEditingDeck();
  }

  private void OnDeckCreated(long deckID)
  {
    CollectionDeck deck = TavernBrawlManager.Get().CurrentDeck();
    if (deck == null || deckID != deck.ID)
      return;
    this.SwitchToEditDeckMode(deck);
  }

  private void OnDeckDeleted(long deckID)
  {
    if (deckID != this.m_deckBeingEdited || !TavernBrawlDisplay.IsTavernBrawlOpen())
      return;
    this.StartCoroutine(this.WaitThenCreateDeck());
  }

  [DebuggerHidden]
  private IEnumerator WaitThenPlayWipeAnim(float waitTime)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlDisplay.\u003CWaitThenPlayWipeAnim\u003Ec__Iterator286() { waitTime = waitTime, \u003C\u0024\u003EwaitTime = waitTime, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitThenCreateDeck()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlDisplay.\u003CWaitThenCreateDeck\u003Ec__Iterator287() { \u003C\u003Ef__this = this };
  }

  private void OnDeckContents(long deckID)
  {
    CollectionDeck collectionDeck = TavernBrawlManager.Get().CurrentDeck();
    if (collectionDeck == null || deckID != collectionDeck.ID || !TavernBrawlDisplay.IsTavernBrawlOpen())
      return;
    this.ValidateDeck();
  }

  private void Awake_InitializeRewardDisplay()
  {
    RewardType rewardType = this.m_currentMission != null ? this.m_currentMission.rewardType : RewardType.REWARD_UNKNOWN;
    RewardTrigger rewardTrigger = this.m_currentMission != null ? this.m_currentMission.rewardTrigger : RewardTrigger.REWARD_TRIGGER_UNKNOWN;
    switch (rewardType)
    {
      case RewardType.REWARD_BOOSTER_PACKS:
        switch (rewardTrigger)
        {
          case RewardTrigger.REWARD_TRIGGER_FINISH_GAME:
            this.m_rewardsText.Text = GameStrings.Get("GLUE_TAVERN_BRAWL_REWARD_DESC_FINISH");
            break;
          default:
            this.m_rewardsText.Text = GameStrings.Get("GLUE_TAVERN_BRAWL_REWARD_DESC");
            break;
        }
      case RewardType.REWARD_CARD_BACK:
        switch (rewardTrigger)
        {
          case RewardTrigger.REWARD_TRIGGER_FINISH_GAME:
            this.m_rewardsText.Text = GameStrings.Get("GLUE_TAVERN_BRAWL_REWARD_DESC_FINISH_CARDBACK");
            break;
          default:
            this.m_rewardsText.Text = GameStrings.Get("GLUE_TAVERN_BRAWL_REWARD_DESC_CARDBACK");
            break;
        }
    }
    if ((UnityEngine.Object) this.m_rewardOffClickCatcher != (UnityEngine.Object) null)
    {
      this.m_rewardChest.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.ShowNonSessionRewardPreview));
      this.m_rewardOffClickCatcher.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.HideNonSessionRewardPreview));
    }
    else
    {
      this.m_rewardChest.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.ShowNonSessionRewardPreview));
      this.m_rewardChest.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.HideNonSessionRewardPreview));
    }
    this.m_rewardsScale = this.m_rewardsPreview.transform.localScale;
    this.m_rewardsPreview.transform.localScale = Vector3.one * 0.01f;
    if (TavernBrawlManager.Get().RewardProgress != 0)
      return;
    this.m_rewardHighlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }

  private void SetupUniversalButtons()
  {
    if ((UnityEngine.Object) this.m_editDeckButton != (UnityEngine.Object) null)
      this.m_editDeckButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.EditDeckButton_OnRelease));
    if ((UnityEngine.Object) this.m_createDeckButton != (UnityEngine.Object) null)
      this.m_createDeckButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.CreateDeck()));
    if ((UnityEngine.Object) this.m_backButton != (UnityEngine.Object) null)
      this.m_backButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.OnBackButton()));
    if ((UnityEngine.Object) this.m_LockedDeckTooltipTrigger != (UnityEngine.Object) null && (UnityEngine.Object) this.m_LockedDeckTooltipZone != (UnityEngine.Object) null)
    {
      this.m_LockedDeckTooltipTrigger.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnLockedTooltipRollover));
      this.m_LockedDeckTooltipTrigger.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnLockedTooltipRollout));
    }
    if ((UnityEngine.Object) this.m_viewDeckButton != (UnityEngine.Object) null)
      this.m_viewDeckButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ViewDeckButton_OnRelease));
    this.m_playButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.PlayButton_OnRelease));
  }

  private void OnLockedTooltipRollover(UIEvent e)
  {
    if (!TavernBrawlManager.Get().IsDeckLocked)
      return;
    this.m_LockedDeckTooltipZone.ShowLayerTooltip(GameStrings.Get("GLUE_LOCKED_DECK_TOOLTIP_TITLE"), GameStrings.Get("GLUE_LOCKED_DECK_TOOLTIP"));
  }

  private void OnLockedTooltipRollout(UIEvent e)
  {
    this.m_LockedDeckTooltipZone.HideTooltip();
  }

  private void OnFriendChallengeWaitingForOpponentDialogResponse(AlertPopup.Response response, object userData)
  {
    if (response != AlertPopup.Response.CANCEL || FriendChallengeMgr.Get().AmIInGameState())
      return;
    FriendChallengeMgr.Get().DeselectDeck();
    FriendlyChallengeHelper.Get().StopWaitingForFriendChallenge();
    if (TavernBrawlManager.Get().SelectHeroBeforeMission())
      return;
    this.EnablePlayButton();
  }

  private void OnFriendChallengeChanged(FriendChallengeEvent challengeEvent, BnetPlayer player, object userData)
  {
    if (challengeEvent == FriendChallengeEvent.OPPONENT_ACCEPTED_CHALLENGE || challengeEvent == FriendChallengeEvent.I_ACCEPTED_CHALLENGE)
      this.SetUIForFriendlyChallenge(true);
    else if (challengeEvent == FriendChallengeEvent.SELECTED_DECK)
    {
      if (player == BnetPresenceMgr.Get().GetMyPlayer() || !FriendChallengeMgr.Get().DidISelectDeck())
        return;
      FriendlyChallengeHelper.Get().HideFriendChallengeWaitingForOpponentDialog();
    }
    else if (challengeEvent == FriendChallengeEvent.I_RESCINDED_CHALLENGE || challengeEvent == FriendChallengeEvent.OPPONENT_DECLINED_CHALLENGE || challengeEvent == FriendChallengeEvent.OPPONENT_RESCINDED_CHALLENGE)
      this.SetUIForFriendlyChallenge(false);
    else if (challengeEvent == FriendChallengeEvent.OPPONENT_CANCELED_CHALLENGE || challengeEvent == FriendChallengeEvent.OPPONENT_REMOVED_FROM_FRIENDS)
    {
      this.SetUIForFriendlyChallenge(false);
      FriendlyChallengeHelper.Get().StopWaitingForFriendChallenge();
    }
    else
    {
      if (challengeEvent != FriendChallengeEvent.DESELECTED_DECK || player == BnetPresenceMgr.Get().GetMyPlayer())
        return;
      if (!TavernBrawlManager.Get().SelectHeroBeforeMission())
      {
        this.EnablePlayButton();
      }
      else
      {
        if (!FriendChallengeMgr.Get().DidISelectDeck())
          return;
        FriendlyChallengeHelper.Get().StartChallengeOrWaitForOpponent("GLOBAL_FRIEND_CHALLENGE_OPPONENT_WAITING_DECK", new AlertPopup.ResponseCallback(this.OnFriendChallengeWaitingForOpponentDialogResponse));
      }
    }
  }

  private void InitExpoDemoMode()
  {
    if (!DemoMgr.Get().IsExpoDemo())
      return;
    this.m_backButton.Flip(false);
    this.m_backButton.SetEnabled(false);
    this.m_chalkboardEndInfo.gameObject.SetActive(false);
    this.StartCoroutine("ShowDemoQuotes");
  }

  [DebuggerHidden]
  private IEnumerator ShowDemoQuotes()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TavernBrawlDisplay.\u003CShowDemoQuotes\u003Ec__Iterator288() { \u003C\u003Ef__this = this };
  }

  private void EnableClickBlocker(bool enable)
  {
    if ((UnityEngine.Object) this.m_clickBlocker == (UnityEngine.Object) null)
      return;
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if (enable)
    {
      fullScreenFxMgr.SetBlurBrightness(1f);
      fullScreenFxMgr.SetBlurDesaturation(0.0f);
      fullScreenFxMgr.Vignette(0.4f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      fullScreenFxMgr.Blur(1f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
    else
    {
      fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
      fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    }
    this.m_clickBlocker.gameObject.SetActive(enable);
  }

  private void HideDemoQuotes()
  {
    if (!DemoMgr.Get().IsExpoDemo())
      return;
    this.StopCoroutine("ShowDemoQuotes");
    if (!((UnityEngine.Object) this.m_expoThankQuote != (UnityEngine.Object) null))
      return;
    NotificationManager.Get().DestroyNotification(this.m_expoThankQuote, 0.0f);
    this.m_expoThankQuote = (Notification) null;
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void HideChalkboardFX()
  {
    this.m_chalkboardFX.SetActive(false);
  }
}
