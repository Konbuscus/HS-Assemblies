﻿// Decompiled with JetBrains decompiler
// Type: DeckTrayHeroSkinContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class DeckTrayHeroSkinContent : DeckTrayContent
{
  [CustomEditField(Sections = "Animation & Sounds")]
  public iTween.EaseType m_traySlideSlideInAnimation = iTween.EaseType.easeOutBounce;
  [CustomEditField(Sections = "Animation & Sounds")]
  public float m_traySlideAnimationTime = 0.25f;
  private List<DeckTrayHeroSkinContent.HeroAssigned> m_heroAssignedListeners = new List<DeckTrayHeroSkinContent.HeroAssigned>();
  private const string ADD_CARD_TO_DECK_SOUND = "collection_manager_card_add_to_deck_instant";
  [CustomEditField(Sections = "Positioning")]
  public GameObject m_root;
  [CustomEditField(Sections = "Positioning")]
  public Vector3 m_trayHiddenOffset;
  [CustomEditField(Sections = "Positioning")]
  public GameObject m_heroSkinContainer;
  [CustomEditField(Sections = "Animation & Sounds")]
  public iTween.EaseType m_traySlideSlideOutAnimation;
  [CustomEditField(Sections = "Animation & Sounds", T = EditType.SOUND_PREFAB)]
  public string m_socketSound;
  public UberText m_currentHeroSkinName;
  [CustomEditField(Sections = "Card Effects")]
  public Material m_sepiaCardMaterial;
  private string m_currentHeroCardId;
  private Actor m_heroSkinObject;
  private Vector3 m_originalLocalPosition;
  private bool m_animating;
  private bool m_waitingToLoadHeroDef;
  private DeckTrayHeroSkinContent.AnimatedHeroSkin m_animData;

  private void Awake()
  {
    this.m_originalLocalPosition = this.transform.localPosition;
    this.transform.localPosition = this.m_originalLocalPosition + this.m_trayHiddenOffset;
    this.m_root.SetActive(false);
    this.LoadHeroSkinActor();
  }

  public void UpdateHeroSkin(EntityDef entityDef, TAG_PREMIUM premium, bool assigning)
  {
    this.UpdateHeroSkin(entityDef.GetCardId(), premium, assigning, (Actor) null);
  }

  public void UpdateHeroSkin(string cardId, TAG_PREMIUM premium, bool assigning, Actor baseActor = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTrayHeroSkinContent.\u003CUpdateHeroSkin\u003Ec__AnonStorey436 skinCAnonStorey436 = new DeckTrayHeroSkinContent.\u003CUpdateHeroSkin\u003Ec__AnonStorey436();
    // ISSUE: reference to a compiler-generated field
    skinCAnonStorey436.premium = premium;
    // ISSUE: reference to a compiler-generated field
    skinCAnonStorey436.assigning = assigning;
    // ISSUE: reference to a compiler-generated field
    skinCAnonStorey436.\u003C\u003Ef__this = this;
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    // ISSUE: reference to a compiler-generated field
    if (skinCAnonStorey436.assigning)
    {
      if (!string.IsNullOrEmpty(this.m_socketSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_socketSound));
      taggedDeck.HeroOverridden = true;
    }
    this.UpdateMissingEffect(taggedDeck.HeroOverridden);
    if (this.m_currentHeroCardId == cardId)
    {
      this.ShowSocketFX();
    }
    else
    {
      this.m_currentHeroCardId = cardId;
      taggedDeck.HeroCardID = cardId;
      // ISSUE: reference to a compiler-generated field
      taggedDeck.HeroPremium = skinCAnonStorey436.premium;
      if ((UnityEngine.Object) baseActor != (UnityEngine.Object) null)
      {
        // ISSUE: reference to a compiler-generated field
        this.UpdateHeroSkinVisual(baseActor.GetEntityDef(), baseActor.GetCardDef(), baseActor.GetPremium(), skinCAnonStorey436.assigning);
      }
      else
      {
        this.m_waitingToLoadHeroDef = true;
        // ISSUE: reference to a compiler-generated method
        DefLoader.Get().LoadFullDef(cardId, new DefLoader.LoadDefCallback<FullDef>(skinCAnonStorey436.\u003C\u003Em__2D7));
      }
    }
  }

  public void AnimateInNewHeroSkin(Actor actor)
  {
    GameObject gameObject = actor.gameObject;
    DeckTrayHeroSkinContent.AnimatedHeroSkin animatedHeroSkin = new DeckTrayHeroSkinContent.AnimatedHeroSkin();
    animatedHeroSkin.Actor = actor;
    animatedHeroSkin.GameObject = gameObject;
    animatedHeroSkin.OriginalScale = gameObject.transform.localScale;
    animatedHeroSkin.OriginalPosition = gameObject.transform.position;
    this.m_animData = animatedHeroSkin;
    gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 0.5f, gameObject.transform.position.z);
    gameObject.transform.localScale = this.m_heroSkinContainer.transform.lossyScale;
    Hashtable args = iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) 0.6f, (object) "easetype", (object) iTween.EaseType.easeOutCubic, (object) "onupdate", (object) "AnimateNewHeroSkinUpdate", (object) "onupdatetarget", (object) this.gameObject, (object) "oncomplete", (object) "AnimateNewHeroSkinFinished", (object) "oncompleteparams", (object) animatedHeroSkin, (object) "oncompletetarget", (object) this.gameObject);
    iTween.ValueTo(gameObject, args);
    CollectionHeroSkin component = actor.gameObject.GetComponent<CollectionHeroSkin>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.ShowSocketFX();
    SoundManager.Get().LoadAndPlay("collection_manager_card_add_to_deck_instant", this.gameObject);
  }

  private void AnimateNewHeroSkinFinished()
  {
    this.m_heroSkinObject.gameObject.SetActive(true);
    Actor actor = this.m_animData.Actor;
    this.UpdateHeroSkin(actor.GetEntityDef().GetCardId(), actor.GetPremium(), true, actor);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_animData.GameObject);
    this.m_animData = (DeckTrayHeroSkinContent.AnimatedHeroSkin) null;
  }

  private void AnimateNewHeroSkinUpdate(float val)
  {
    GameObject gameObject = this.m_animData.GameObject;
    Vector3 originalPosition = this.m_animData.OriginalPosition;
    Vector3 position = this.m_heroSkinContainer.transform.position;
    if ((double) val <= 0.850000023841858)
    {
      val /= 0.85f;
      gameObject.transform.position = new Vector3(Mathf.Lerp(originalPosition.x, position.x, val), (float) ((double) Mathf.Lerp(originalPosition.y, position.y, val) + (double) Mathf.Sin(val * 3.141593f) * 15.0 + (double) val * 4.0), Mathf.Lerp(originalPosition.z, position.z, val));
    }
    else
    {
      this.m_heroSkinObject.gameObject.SetActive(false);
      val = (float) (((double) val - 0.850000023841858) / 0.149999976158142);
      gameObject.transform.position = new Vector3(position.x, position.y + Mathf.Lerp(4f, 0.0f, val), position.z);
    }
  }

  public void SetNewHeroSkin(Actor actor)
  {
    if (this.m_animData != null)
      return;
    Actor actor1 = actor.Clone();
    actor1.SetCardDef(actor.GetCardDef());
    this.AnimateInNewHeroSkin(actor1);
  }

  public override bool PreAnimateContentEntrance()
  {
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    this.UpdateHeroSkin(taggedDeck.HeroCardID, taggedDeck.HeroPremium, false, (Actor) null);
    return true;
  }

  public override bool AnimateContentEntranceStart()
  {
    if (this.m_waitingToLoadHeroDef)
      return false;
    this.m_root.SetActive(true);
    this.transform.localPosition = this.m_originalLocalPosition;
    this.m_animating = true;
    iTween.MoveFrom(this.gameObject, iTween.Hash((object) "position", (object) (this.m_originalLocalPosition + this.m_trayHiddenOffset), (object) "islocal", (object) true, (object) "time", (object) this.m_traySlideAnimationTime, (object) "easetype", (object) this.m_traySlideSlideInAnimation, (object) "oncomplete", (object) (Action<object>) (o => this.m_animating = false)));
    return true;
  }

  public override bool AnimateContentEntranceEnd()
  {
    return !this.m_animating;
  }

  public override bool AnimateContentExitStart()
  {
    this.transform.localPosition = this.m_originalLocalPosition;
    this.m_animating = true;
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) (this.m_originalLocalPosition + this.m_trayHiddenOffset), (object) "islocal", (object) true, (object) "time", (object) this.m_traySlideAnimationTime, (object) "easetype", (object) this.m_traySlideSlideOutAnimation, (object) "oncomplete", (object) (Action<object>) (o =>
    {
      this.m_animating = false;
      this.m_root.SetActive(false);
    })));
    return true;
  }

  public override bool AnimateContentExitEnd()
  {
    return !this.m_animating;
  }

  public void RegisterHeroAssignedListener(DeckTrayHeroSkinContent.HeroAssigned dlg)
  {
    this.m_heroAssignedListeners.Add(dlg);
  }

  public void UnregisterHeroAssignedListener(DeckTrayHeroSkinContent.HeroAssigned dlg)
  {
    this.m_heroAssignedListeners.Remove(dlg);
  }

  private void LoadHeroSkinActor()
  {
    AssetLoader.Get().LoadActor(ActorNames.GetHeroSkinOrHandActor(TAG_CARDTYPE.HERO, TAG_PREMIUM.NORMAL), (AssetLoader.GameObjectCallback) ((name, go, callbackData) =>
    {
      if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      {
        Debug.LogWarning((object) string.Format("DeckTrayHeroSkinContent.LoadHeroSkinActor - FAILED to load \"{0}\"", (object) name));
      }
      else
      {
        Actor component = go.GetComponent<Actor>();
        if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        {
          Debug.LogWarning((object) string.Format("HandActorCache.OnActorLoaded() - ERROR \"{0}\" has no Actor component", (object) name));
        }
        else
        {
          GameUtils.SetParent((Component) component, this.m_heroSkinContainer, false);
          this.m_heroSkinObject = component;
        }
      }
    }), (object) null, false);
  }

  private void UpdateHeroSkinVisual(EntityDef entityDef, CardDef cardDef, TAG_PREMIUM premium, bool assigning)
  {
    if ((UnityEngine.Object) this.m_heroSkinObject == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "Hero skin object not loaded yet! Cannot set portrait!");
    }
    else
    {
      this.m_heroSkinObject.SetEntityDef(entityDef);
      this.m_heroSkinObject.SetCardDef(cardDef);
      this.m_heroSkinObject.SetPremium(premium);
      this.m_heroSkinObject.UpdateAllComponents();
      CollectionHeroSkin component = this.m_heroSkinObject.GetComponent<CollectionHeroSkin>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.SetClass(entityDef.GetClass());
      foreach (DeckTrayHeroSkinContent.HeroAssigned heroAssigned in this.m_heroAssignedListeners.ToArray())
        heroAssigned(entityDef.GetCardId());
      if (assigning)
        GameUtils.LoadCardDefEmoteSound(cardDef, EmoteType.PICKED, new GameUtils.EmoteSoundLoaded(this.OnPickEmoteLoaded));
      if ((UnityEngine.Object) this.m_currentHeroSkinName != (UnityEngine.Object) null)
        this.m_currentHeroSkinName.Text = entityDef.GetName();
      this.ShowSocketFX();
    }
  }

  private void OnPickEmoteLoaded(CardSoundSpell spell)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null)
      return;
    spell.AddFinishedCallback(new Spell.FinishedCallback(this.OnPickEmoteFinished));
    spell.Reactivate();
  }

  private void OnPickEmoteFinished(Spell spell, object userData)
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
  }

  private void ShowSocketFX()
  {
    CollectionHeroSkin component = this.m_heroSkinObject.GetComponent<CollectionHeroSkin>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.ShowSocketFX();
  }

  private void UpdateMissingEffect(bool overriden)
  {
    if (overriden)
    {
      this.m_heroSkinObject.DisableMissingCardEffect();
    }
    else
    {
      this.m_heroSkinObject.SetMissingCardMaterial(this.m_sepiaCardMaterial);
      this.m_heroSkinObject.MissingCardEffect();
    }
    this.m_heroSkinObject.UpdateAllComponents();
  }

  private class AnimatedHeroSkin
  {
    public Actor Actor;
    public GameObject GameObject;
    public Vector3 OriginalScale;
    public Vector3 OriginalPosition;
  }

  public delegate void HeroAssigned(string cardId);
}
