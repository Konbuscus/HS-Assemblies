﻿// Decompiled with JetBrains decompiler
// Type: ClassChallengeUnlock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class ClassChallengeUnlock : Reward
{
  private List<GameObject> m_classFrames = new List<GameObject>();
  [CustomEditField(Sections = "Container")]
  public UIBObjectSpacing m_classFrameContainer;
  [CustomEditField(Sections = "Text Settings")]
  public UberText m_headerText;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_appearSound;

  protected override void Awake()
  {
    base.Awake();
    if ((bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_rewardBanner.transform.localScale = this.m_rewardBanner.transform.localScale * 8f;
  }

  public static List<AdventureMissionDbfRecord> AdventureMissionsUnlockedByWingId(int wingId)
  {
    List<AdventureMissionDbfRecord> missionDbfRecordList = new List<AdventureMissionDbfRecord>();
    using (List<AdventureMissionDbfRecord>.Enumerator enumerator = GameDbf.AdventureMission.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureMissionDbfRecord current = enumerator.Current;
        if (current.ReqWingId == wingId)
        {
          int scenarioId = current.ScenarioId;
          ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(scenarioId);
          if (record == null)
            Debug.LogError((object) string.Format("Unable to find Scenario record with ID: {0}", (object) scenarioId));
          else if (record.ModeId == 4)
            missionDbfRecordList.Add(current);
        }
      }
    }
    return missionDbfRecordList;
  }

  protected override void InitData()
  {
    this.SetData((RewardData) new ClassChallengeUnlockData(), false);
  }

  protected override void PlayShowSounds()
  {
  }

  protected override void ShowReward(bool updateCacheValues)
  {
    this.m_root.SetActive(true);
    this.m_classFrameContainer.UpdatePositions();
    using (List<GameObject>.Enumerator enumerator = this.m_classFrames.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 180f);
        Hashtable args = iTween.Hash((object) "amount", (object) new Vector3(0.0f, 0.0f, 540f), (object) "time", (object) 1.5f, (object) "easeType", (object) iTween.EaseType.easeOutElastic, (object) "space", (object) Space.Self);
        iTween.RotateAdd(current, args);
      }
    }
    FullScreenFXMgr.Get().StartStandardBlurVignette(1f);
  }

  protected override void HideReward()
  {
    base.HideReward();
    FullScreenFXMgr.Get().EndStandardBlurVignette(1f, new FullScreenFXMgr.EffectListener(this.DestroyClassChallengeUnlock));
    this.m_root.SetActive(false);
  }

  protected override void OnDataSet(bool updateVisuals)
  {
    if (!updateVisuals)
      return;
    ClassChallengeUnlockData data = this.Data as ClassChallengeUnlockData;
    if (data == null)
    {
      Debug.LogWarning((object) string.Format("ClassChallengeUnlock.OnDataSet() - Data {0} is not ClassChallengeUnlockData", (object) this.Data));
    }
    else
    {
      List<string> stringList1 = new List<string>();
      List<string> stringList2 = new List<string>();
      using (List<AdventureMissionDbfRecord>.Enumerator enumerator = ClassChallengeUnlock.AdventureMissionsUnlockedByWingId(data.WingID).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AdventureMissionDbfRecord current = enumerator.Current;
          int scenarioId = current.ScenarioId;
          ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(scenarioId);
          if (record == null)
            Debug.LogError((object) string.Format("Unable to find Scenario record with ID: {0}", (object) scenarioId));
          else if (!string.IsNullOrEmpty(current.ClassChallengePrefabPopup))
          {
            DbfLocValue shortName = record.ShortName;
            stringList1.Add(current.ClassChallengePrefabPopup);
            stringList2.Add((string) shortName);
          }
          else
            Debug.LogWarning((object) string.Format("CLASS_CHALLENGE_PREFAB_POPUP not define for AdventureMission SCENARIO_ID: {0}", (object) scenarioId));
        }
      }
      if (stringList1.Count == 0)
      {
        Debug.LogError((object) string.Format("Unable to find AdventureMission record with REQ_WING_ID: {0}.", (object) data.WingID));
      }
      else
      {
        this.m_headerText.Text = GameStrings.FormatPlurals("GLOBAL_REWARD_CLASS_CHALLENGE_HEADLINE", new GameStrings.PluralNumber[1]
        {
          new GameStrings.PluralNumber()
          {
            m_index = 0,
            m_number = stringList1.Count
          }
        });
        string headline = stringList1.Count <= 0 ? string.Empty : string.Join(", ", stringList2.ToArray());
        string challengeRewardSource = (string) GameDbf.Wing.GetRecord(data.WingID).ClassChallengeRewardSource;
        this.SetRewardText(headline, string.Empty, challengeRewardSource);
        using (List<string>.Enumerator enumerator = stringList1.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            GameObject child = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(enumerator.Current), true, false);
            if (!((Object) child == (Object) null))
            {
              GameUtils.SetParent(child, (Component) this.m_classFrameContainer, false);
              child.transform.localRotation = Quaternion.identity;
              this.m_classFrameContainer.AddObject(child, true);
              this.m_classFrames.Add(child);
            }
          }
        }
        this.m_classFrameContainer.UpdatePositions();
        this.SetReady(true);
        this.EnableClickCatcher(true);
        this.RegisterClickListener(new Reward.OnClickedCallback(this.OnClicked));
      }
    }
  }

  private void OnClicked(Reward reward, object userData)
  {
    this.HideReward();
  }

  private void DestroyClassChallengeUnlock()
  {
    Object.DestroyImmediate((Object) this.gameObject);
  }
}
