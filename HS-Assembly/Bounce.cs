﻿// Decompiled with JetBrains decompiler
// Type: Bounce
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class Bounce : MonoBehaviour
{
  public float m_BounceSpeed = 3.5f;
  public float m_BounceAmount = 3f;
  public int m_BounceCount = 3;
  public float m_Bounceness = 0.2f;
  public float m_Delay;
  public bool m_PlayOnStart;
  private Vector3 m_StartingPosition;
  private float m_BounceAmountOverTime;

  private void Start()
  {
    if (!this.m_PlayOnStart)
      return;
    this.StartAnimation();
  }

  private void Update()
  {
  }

  public void StartAnimation()
  {
    this.m_BounceAmountOverTime = this.m_BounceAmount;
    this.m_StartingPosition = this.transform.position;
    this.StartCoroutine("BounceAnimation");
  }

  [DebuggerHidden]
  private IEnumerator BounceAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Bounce.\u003CBounceAnimation\u003Ec__Iterator324() { \u003C\u003Ef__this = this };
  }
}
