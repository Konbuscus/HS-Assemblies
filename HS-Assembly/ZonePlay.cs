﻿// Decompiled with JetBrains decompiler
// Type: ZonePlay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class ZonePlay : Zone
{
  public int m_MaxSlots = 7;
  private float[] PHONE_WIDTH_MODIFIERS = new float[8]{ 0.25f, 0.25f, 0.25f, 0.25f, 0.22f, 0.19f, 0.15f, 0.1f };
  private int m_slotMousedOver = -1;
  private float m_transitionTime = 1f;
  private const float DEFAULT_TRANSITION_TIME = 1f;
  private const float PHONE_CARD_SCALE = 1.15f;
  private float m_slotWidth;

  private void Awake()
  {
    this.m_slotWidth = this.GetComponent<Collider>().bounds.size.x / (float) this.m_MaxSlots;
  }

  public float GetTransitionTime()
  {
    return this.m_transitionTime;
  }

  public void SetTransitionTime(float transitionTime)
  {
    this.m_transitionTime = transitionTime;
  }

  public void ResetTransitionTime()
  {
    this.m_transitionTime = 1f;
  }

  public void SortWithSpotForHeldCard(int slot)
  {
    this.m_slotMousedOver = slot;
    this.UpdateLayout();
  }

  public int GetSlotMousedOver()
  {
    return this.m_slotMousedOver;
  }

  public float GetSlotWidth()
  {
    this.m_slotWidth = this.GetComponent<Collider>().bounds.size.x / (float) this.m_MaxSlots;
    int count = this.m_cards.Count;
    if (this.m_slotMousedOver >= 0)
      ++count;
    int index = Mathf.Clamp(count, 0, this.m_MaxSlots);
    float num = 1f;
    if ((bool) UniversalInputManager.UsePhoneUI)
      num += this.PHONE_WIDTH_MODIFIERS[index];
    return this.m_slotWidth * num;
  }

  public Vector3 GetCardPosition(Card card)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.GetCardPosition(this.m_cards.FindIndex(new Predicate<Card>(new ZonePlay.\u003CGetCardPosition\u003Ec__AnonStorey3C4() { card = card }.\u003C\u003Em__15F)));
  }

  public Vector3 GetCardPosition(int index)
  {
    if (index < 0 || index >= this.m_cards.Count)
      return this.transform.position;
    int count = this.m_cards.Count;
    if (this.m_slotMousedOver >= 0)
      ++count;
    Vector3 center = this.GetComponent<Collider>().bounds.center;
    float num1 = 0.5f * this.GetSlotWidth();
    float num2 = (float) count * num1;
    float num3 = center.x - num2 + num1;
    int num4 = this.m_slotMousedOver < 0 || index < this.m_slotMousedOver ? 0 : 1;
    for (int index1 = 0; index1 < index; ++index1)
    {
      if (this.CanAnimateCard(this.m_cards[index1]))
        ++num4;
    }
    return new Vector3(num3 + (float) num4 * this.GetSlotWidth(), center.y, center.z);
  }

  public override bool CanAcceptTags(int controllerId, TAG_ZONE zoneTag, TAG_CARDTYPE cardType)
  {
    return base.CanAcceptTags(controllerId, zoneTag, cardType) && cardType == TAG_CARDTYPE.MINION;
  }

  public override void UpdateLayout()
  {
    this.m_updatingLayout = true;
    if (this.IsBlockingLayout())
    {
      this.UpdateLayoutFinished();
    }
    else
    {
      if ((UnityEngine.Object) InputManager.Get() != (UnityEngine.Object) null && (UnityEngine.Object) InputManager.Get().GetHeldCard() == (UnityEngine.Object) null)
        this.m_slotMousedOver = -1;
      int num1 = 0;
      this.m_cards.Sort(new Comparison<Card>(Zone.CardSortComparison));
      float num2 = 0.0f;
      for (int index = 0; index < this.m_cards.Count; ++index)
      {
        Card card = this.m_cards[index];
        if (this.CanAnimateCard(card))
        {
          string tweenName = ZoneMgr.Get().GetTweenName<ZonePlay>();
          if (this.m_Side == Player.Side.OPPOSING)
            iTween.StopOthersByName(card.gameObject, tweenName, false);
          Vector3 localScale = this.transform.localScale;
          if ((bool) UniversalInputManager.UsePhoneUI)
            localScale *= 1.15f;
          Vector3 cardPosition = this.GetCardPosition(index);
          float transitionDelay = card.GetTransitionDelay();
          card.SetTransitionDelay(0.0f);
          ZoneTransitionStyle transitionStyle = card.GetTransitionStyle();
          card.SetTransitionStyle(ZoneTransitionStyle.NORMAL);
          if (transitionStyle == ZoneTransitionStyle.INSTANT)
          {
            card.EnableTransitioningZones(false);
            card.transform.position = cardPosition;
            card.transform.rotation = this.transform.rotation;
            card.transform.localScale = localScale;
          }
          else
          {
            card.EnableTransitioningZones(true);
            ++num1;
            Hashtable args1 = iTween.Hash((object) "scale", (object) localScale, (object) "delay", (object) transitionDelay, (object) "time", (object) this.m_transitionTime, (object) "name", (object) tweenName);
            iTween.ScaleTo(card.gameObject, args1);
            Hashtable args2 = iTween.Hash((object) "rotation", (object) this.transform.eulerAngles, (object) "delay", (object) transitionDelay, (object) "time", (object) this.m_transitionTime, (object) "name", (object) tweenName);
            iTween.RotateTo(card.gameObject, args2);
            Hashtable args3 = iTween.Hash((object) "position", (object) cardPosition, (object) "delay", (object) transitionDelay, (object) "time", (object) this.m_transitionTime, (object) "name", (object) tweenName);
            iTween.MoveTo(card.gameObject, args3);
            num2 = Mathf.Max(num2, transitionDelay + this.m_transitionTime);
          }
        }
      }
      if (num1 > 0)
        this.StartFinishLayoutTimer(num2);
      else
        this.UpdateLayoutFinished();
    }
  }

  protected bool CanAnimateCard(Card card)
  {
    return !card.IsDoNotSort();
  }
}
