﻿// Decompiled with JetBrains decompiler
// Type: TournamentDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TournamentDisplay : MonoBehaviour
{
  public Vector3 m_SetRotationOnscreenPosition = new Vector3(27.051f, 1.7f, -22.4f);
  public Vector3 m_SetRotationOffscreenPosition = new Vector3(-60f, 1.7f, -22.4f);
  public Vector3 m_SetRotationOffscreenDuringTransition = new Vector3(-260f, 1.7f, -22.4f);
  public float m_SetRotationSideInTime = 1f;
  private List<TournamentDisplay.DelMedalChanged> m_medalChangedListeners = new List<TournamentDisplay.DelMedalChanged>();
  public TextMesh m_modeName;
  public Vector3_MobileOverride m_deckPickerPosition;
  private static TournamentDisplay s_instance;
  private bool m_allInitialized;
  private bool m_netCacheReturned;
  private bool m_deckPickerTrayLoaded;
  private DeckPickerTrayDisplay m_deckPickerTray;
  private GameObject m_deckPickerTrayGO;
  private NetCache.NetCacheMedalInfo m_currentMedalInfo;

  private void Awake()
  {
    AssetLoader.Get().LoadActor(!(bool) UniversalInputManager.UsePhoneUI ? "DeckPickerTray" : "DeckPickerTray_phone", new AssetLoader.GameObjectCallback(this.DeckPickerTrayLoaded), (object) null, false);
    TournamentDisplay.s_instance = this;
  }

  private void OnDestroy()
  {
    TournamentDisplay.s_instance = (TournamentDisplay) null;
    UserAttentionManager.StopBlocking(UserAttentionBlocker.SET_ROTATION_INTRO);
  }

  private void Start()
  {
    MusicManager.Get().StartPlaylist(MusicPlaylistType.UI_Tournament);
    NetCache.Get().RegisterScreenTourneys(new NetCache.NetCacheCallback(this.UpdateTourneyPage), new NetCache.ErrorCallback(NetCache.DefaultErrorHandler));
  }

  private void Update()
  {
    if (this.m_allInitialized || !this.m_netCacheReturned || !this.m_deckPickerTrayLoaded)
      return;
    this.StartCoroutine(this.UpdateTourneyPageWhenReady());
    this.m_deckPickerTray.Init();
    this.m_allInitialized = true;
  }

  public void UpdateHeaderText()
  {
    string key = !Options.Get().GetBool(Option.IN_WILD_MODE) ? "GLUE_PLAY_STANDARD" : "GLUE_PLAY_WILD";
    if (!((Object) this.m_deckPickerTray != (Object) null))
      return;
    this.m_deckPickerTray.SetHeaderText(GameStrings.Get(key));
  }

  private void DeckPickerTrayLoaded(string name, GameObject go, object callbackData)
  {
    this.m_deckPickerTrayGO = go;
    this.m_deckPickerTray = go.GetComponent<DeckPickerTrayDisplay>();
    this.m_deckPickerTray.transform.parent = this.transform;
    this.m_deckPickerTray.transform.localPosition = (Vector3) ((MobileOverrideValue<Vector3>) this.m_deckPickerPosition);
    this.m_deckPickerTrayLoaded = true;
    this.UpdateHeaderText();
    if (!GameUtils.ShouldShowSetRotationIntro())
      return;
    this.m_deckPickerTrayGO.transform.localPosition = this.m_SetRotationOffscreenDuringTransition;
    this.SetupSetRotation();
  }

  public void SetRotationSlideIn()
  {
    this.m_deckPickerTrayGO.transform.localPosition = this.m_SetRotationOffscreenPosition;
    iTween.MoveTo(this.m_deckPickerTrayGO, iTween.Hash((object) "position", (object) this.m_SetRotationOnscreenPosition, (object) "delay", (object) 0.0f, (object) "time", (object) this.m_SetRotationSideInTime, (object) "islocal", (object) true, (object) "easetype", (object) iTween.EaseType.easeOutBounce));
  }

  private void UpdateTourneyPage()
  {
    if (!NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>().Games.Tournament)
    {
      if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
        return;
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
      Error.AddWarningLoc("GLOBAL_FEATURE_DISABLED_TITLE", "GLOBAL_FEATURE_DISABLED_MESSAGE_PLAY");
    }
    else
    {
      NetCache.NetCacheMedalInfo netObject = NetCache.Get().GetNetObject<NetCache.NetCacheMedalInfo>();
      bool flag = this.m_currentMedalInfo != null && (netObject.Standard.StarLevel != this.m_currentMedalInfo.Standard.StarLevel || netObject.Standard.Stars != this.m_currentMedalInfo.Standard.Stars || netObject.Wild.StarLevel != this.m_currentMedalInfo.Wild.StarLevel || netObject.Wild.Stars != this.m_currentMedalInfo.Wild.Stars);
      this.m_currentMedalInfo = netObject;
      if (flag)
      {
        foreach (TournamentDisplay.DelMedalChanged delMedalChanged in this.m_medalChangedListeners.ToArray())
          delMedalChanged(this.m_currentMedalInfo);
      }
      this.m_netCacheReturned = true;
    }
  }

  [DebuggerHidden]
  private IEnumerator UpdateTourneyPageWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TournamentDisplay.\u003CUpdateTourneyPageWhenReady\u003Ec__Iterator28C() { \u003C\u003Ef__this = this };
  }

  public static TournamentDisplay Get()
  {
    return TournamentDisplay.s_instance;
  }

  public void Unload()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.UpdateTourneyPage));
  }

  public NetCache.NetCacheMedalInfo GetCurrentMedalInfo()
  {
    return this.m_currentMedalInfo;
  }

  public void RegisterMedalChangedListener(TournamentDisplay.DelMedalChanged listener)
  {
    if (this.m_medalChangedListeners.Contains(listener))
      return;
    this.m_medalChangedListeners.Add(listener);
  }

  public void RemoveMedalChangedListener(TournamentDisplay.DelMedalChanged listener)
  {
    this.m_medalChangedListeners.Remove(listener);
  }

  public int GetRankedWinsForClass(TAG_CLASS heroClass)
  {
    int num = 0;
    using (List<NetCache.PlayerRecord>.Enumerator enumerator = NetCache.Get().GetNetObject<NetCache.NetCachePlayerRecords>().Records.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.PlayerRecord current = enumerator.Current;
        if (current.Data != 0)
        {
          EntityDef entityDef = DefLoader.Get().GetEntityDef(current.Data);
          if (entityDef != null && entityDef.GetClass() == heroClass && current.RecordType == GameType.GT_RANKED)
            num += current.Wins;
        }
      }
    }
    return num;
  }

  private void SetupSetRotation()
  {
    AssetLoader.Get().LoadGameObject("TheBox_TheClock", true, false);
  }

  public delegate void DelMedalChanged(NetCache.NetCacheMedalInfo medalInfo);
}
