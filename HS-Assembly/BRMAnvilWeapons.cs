﻿// Decompiled with JetBrains decompiler
// Type: BRMAnvilWeapons
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class BRMAnvilWeapons : MonoBehaviour
{
  public List<BRMAnvilWeapons.AnvilWeapon> m_Weapons;
  private int m_LastWeaponIndex;

  public void RandomWeaponEvent()
  {
    List<int> intList = new List<int>();
    for (int index = 0; index < this.m_Weapons.Count; ++index)
    {
      if (index != this.m_LastWeaponIndex)
        intList.Add(index);
    }
    if (this.m_Weapons.Count <= 0 || intList.Count <= 0)
      return;
    int index1 = UnityEngine.Random.Range(0, intList.Count);
    BRMAnvilWeapons.AnvilWeapon weapon = this.m_Weapons[intList[index1]];
    this.m_LastWeaponIndex = intList[index1];
    weapon.m_FSM.SendEvent(weapon.m_Events[this.RandomSubWeapon(weapon)]);
  }

  public int RandomSubWeapon(BRMAnvilWeapons.AnvilWeapon weapon)
  {
    List<int> intList = new List<int>();
    for (int index = 0; index < weapon.m_Events.Count; ++index)
    {
      if (index != weapon.m_CurrentWeaponIndex)
        intList.Add(index);
    }
    int index1 = UnityEngine.Random.Range(0, intList.Count);
    weapon.m_CurrentWeaponIndex = intList[index1];
    return intList[index1];
  }

  [Serializable]
  public class AnvilWeapon
  {
    public PlayMakerFSM m_FSM;
    public List<string> m_Events;
    [HideInInspector]
    public int m_CurrentWeaponIndex;
  }
}
