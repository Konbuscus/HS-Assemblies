﻿// Decompiled with JetBrains decompiler
// Type: TwistingNetherSqueezeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class TwistingNetherSqueezeInfo
{
  public float m_DurationMin = 1f;
  public float m_DurationMax = 1.5f;
  public iTween.EaseType m_EaseType = iTween.EaseType.easeInCubic;
  public float m_DelayMin;
  public float m_DelayMax;
}
