﻿// Decompiled with JetBrains decompiler
// Type: PremiumMaterialSwitcher
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PremiumMaterialSwitcher : MonoBehaviour
{
  public Material[] m_PremiumMaterials;
  private Material[] OrgMaterials;

  private void Start()
  {
  }

  public void SetToPremium(int premium)
  {
    if (premium < 1)
    {
      if (this.GetComponent<Renderer>().materials == null || this.OrgMaterials == null)
        return;
      Material[] materials = this.GetComponent<Renderer>().materials;
      for (int index = 0; index < this.m_PremiumMaterials.Length && index < materials.Length; ++index)
      {
        if (!((Object) this.m_PremiumMaterials[index] == (Object) null))
          materials[index] = this.OrgMaterials[index];
      }
      this.GetComponent<Renderer>().materials = materials;
      this.OrgMaterials = (Material[]) null;
    }
    else
    {
      if (this.m_PremiumMaterials.Length < 1)
        return;
      if (this.OrgMaterials == null)
        this.OrgMaterials = this.GetComponent<Renderer>().materials;
      Material[] materials = this.GetComponent<Renderer>().materials;
      for (int index = 0; index < this.m_PremiumMaterials.Length && index < materials.Length; ++index)
      {
        if (!((Object) this.m_PremiumMaterials[index] == (Object) null))
          materials[index] = this.m_PremiumMaterials[index];
      }
      this.GetComponent<Renderer>().materials = materials;
    }
  }
}
