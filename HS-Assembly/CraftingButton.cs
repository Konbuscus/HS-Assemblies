﻿// Decompiled with JetBrains decompiler
// Type: CraftingButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CraftingButton : PegUIElement
{
  public Material undoMaterial;
  public Material disabledMaterial;
  public Material enabledMaterial;
  public UberText labelText;
  public MeshRenderer buttonRenderer;
  public GameObject m_costObject;
  public Transform m_disabledCostBone;
  public Transform m_enabledCostBone;
  private bool isEnabled;

  public virtual void DisableButton()
  {
    this.OnEnabled(false);
    this.buttonRenderer.material = this.disabledMaterial;
    this.labelText.Text = string.Empty;
  }

  public virtual void EnterUndoMode()
  {
    this.OnEnabled(true);
    this.buttonRenderer.material = this.undoMaterial;
    this.labelText.Text = GameStrings.Get("GLUE_CRAFTING_UNDO");
  }

  public virtual void EnableButton()
  {
    this.OnEnabled(true);
    this.buttonRenderer.material = this.enabledMaterial;
  }

  public bool IsButtonEnabled()
  {
    return this.isEnabled;
  }

  private void OnEnabled(bool enable)
  {
    this.isEnabled = enable;
    this.GetComponent<Collider>().enabled = enable;
    if (!((Object) this.m_costObject != (Object) null))
      return;
    if ((Object) this.m_enabledCostBone != (Object) null && (Object) this.m_disabledCostBone != (Object) null)
      this.m_costObject.transform.position = !enable ? this.m_disabledCostBone.position : this.m_enabledCostBone.position;
    else
      this.m_costObject.SetActive(enable);
  }
}
