﻿// Decompiled with JetBrains decompiler
// Type: NAX15_KelThuzad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class NAX15_KelThuzad : NAX_MissionEntity
{
  private bool m_frostHeroPowerLinePlayed;
  private bool m_bigglesLinePlayed;
  private bool m_hurryLinePlayed;
  private int m_numTimesMindControlPlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_NAX15_01_SUMMON_ADDS_12");
    this.PreloadSound("VO_NAX15_01_PHASE2_10");
    this.PreloadSound("VO_NAX15_01_HP_07");
    this.PreloadSound("VO_NAX15_01_HP2_05");
    this.PreloadSound("VO_NAX15_01_HP3_06");
    this.PreloadSound("VO_NAX15_01_PHASE2_ALT_11");
    this.PreloadSound("VO_NAX15_01_EMOTE_HELLO_26");
    this.PreloadSound("VO_NAX15_01_EMOTE_WP_25");
    this.PreloadSound("VO_NAX15_01_EMOTE_OOPS_29");
    this.PreloadSound("VO_NAX15_01_EMOTE_SORRY_28");
    this.PreloadSound("VO_NAX15_01_EMOTE_THANKS_27");
    this.PreloadSound("VO_NAX15_01_EMOTE_THREATEN_30");
    this.PreloadSound("VO_KT_HEIGAN2_55");
    this.PreloadSound("VO_NAX15_01_RESPOND_GARROSH_15");
    this.PreloadSound("VO_NAX15_01_RESPOND_THRALL_17");
    this.PreloadSound("VO_NAX15_01_RESPOND_VALEERA_18");
    this.PreloadSound("VO_NAX15_01_RESPOND_UTHER_14");
    this.PreloadSound("VO_NAX15_01_RESPOND_REXXAR_19");
    this.PreloadSound("VO_NAX15_01_RESPOND_MALFURION_ALT_21");
    this.PreloadSound("VO_NAX15_01_RESPOND_GULDAN_22");
    this.PreloadSound("VO_NAX15_01_RESPOND_JAINA_23");
    this.PreloadSound("VO_NAX15_01_RESPOND_ANDUIN_24");
    this.PreloadSound("VO_NAX15_01_BIGGLES_32");
    this.PreloadSound("VO_NAX15_01_HURRY_31");
  }

  public override void OnPlayThinkEmote()
  {
    if (this.m_hurryLinePlayed || this.m_enemySpeaking)
      return;
    Player currentPlayer = GameState.Get().GetCurrentPlayer();
    if (!currentPlayer.IsLocalUser() || currentPlayer.GetHeroCard().HasActiveEmoteSound())
      return;
    this.m_hurryLinePlayed = true;
    Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_HURRY_31", "VO_NAX15_01_HURRY_31", Notification.SpeechBubbleDirection.TopRight, GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor(), 1f, true, false));
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX15_KelThuzad.\u003CHandleGameOverWithTiming\u003Ec__Iterator1E9() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX15_KelThuzad.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1EA() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  public override void HandleRealTimeMissionEvent(int missionEvent)
  {
    if (missionEvent != 1)
      return;
    AssetLoader.Get().LoadGameObject("KelThuzad_StealTurn", new AssetLoader.GameObjectCallback(this.OnStealTurnSpellLoaded), (object) null, false);
  }

  private void OnStealTurnSpellLoaded(string name, GameObject go, object callbackData)
  {
    if ((Object) go == (Object) null)
    {
      if ((Object) TurnTimer.Get() != (Object) null)
        TurnTimer.Get().OnEndTurnRequested();
      EndTurnButton.Get().OnEndTurnRequested();
    }
    else
    {
      go.transform.position = EndTurnButton.Get().transform.position;
      Spell component = go.GetComponent<Spell>();
      if ((Object) component == (Object) null)
      {
        if ((Object) TurnTimer.Get() != (Object) null)
          TurnTimer.Get().OnEndTurnRequested();
        EndTurnButton.Get().OnEndTurnRequested();
      }
      else
      {
        Actor actor = GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor();
        component.ActivateState(SpellStateType.ACTION);
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_PHASE2_ALT_11", "VO_NAX15_01_PHASE2_ALT_11", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
      }
    }
  }

  protected override void PlayEmoteResponse(EmoteType emoteType, CardSoundSpell emoteSpell)
  {
    Actor actor = GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor();
    EmoteType emoteType1 = emoteType;
    switch (emoteType1)
    {
      case EmoteType.GREETINGS:
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_EMOTE_HELLO_26", "VO_NAX15_01_EMOTE_HELLO_26", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
        break;
      case EmoteType.WELL_PLAYED:
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_EMOTE_WP_25", "VO_NAX15_01_EMOTE_WP_25", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
        break;
      case EmoteType.OOPS:
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_EMOTE_OOPS_29", "VO_NAX15_01_EMOTE_OOPS_29", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
        break;
      case EmoteType.THREATEN:
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_EMOTE_THREATEN_30", "VO_NAX15_01_EMOTE_THREATEN_30", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
        break;
      case EmoteType.THANKS:
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_EMOTE_THANKS_27", "VO_NAX15_01_EMOTE_THANKS_27", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
        break;
      case EmoteType.SORRY:
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_EMOTE_SORRY_28", "VO_NAX15_01_EMOTE_SORRY_28", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
        break;
      case EmoteType.START:
        string cardId = GameState.Get().GetFriendlySidePlayer().GetHero().GetCardId();
        if (cardId == null)
          break;
        // ISSUE: reference to a compiler-generated field
        if (NAX15_KelThuzad.\u003C\u003Ef__switch\u0024mapAF == null)
        {
          // ISSUE: reference to a compiler-generated field
          NAX15_KelThuzad.\u003C\u003Ef__switch\u0024mapAF = new Dictionary<string, int>(9)
          {
            {
              "HERO_01",
              0
            },
            {
              "HERO_02",
              1
            },
            {
              "HERO_03",
              2
            },
            {
              "HERO_04",
              3
            },
            {
              "HERO_05",
              4
            },
            {
              "HERO_06",
              5
            },
            {
              "HERO_07",
              6
            },
            {
              "HERO_08",
              7
            },
            {
              "HERO_09",
              8
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (!NAX15_KelThuzad.\u003C\u003Ef__switch\u0024mapAF.TryGetValue(cardId, out num))
          break;
        switch (num)
        {
          case 0:
            Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_RESPOND_GARROSH_15", "VO_NAX15_01_RESPOND_GARROSH_15", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
            return;
          case 1:
            Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_RESPOND_THRALL_17", "VO_NAX15_01_RESPOND_THRALL_17", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
            return;
          case 2:
            Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_RESPOND_VALEERA_18", "VO_NAX15_01_RESPOND_VALEERA_18", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
            return;
          case 3:
            Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_RESPOND_UTHER_14", "VO_NAX15_01_RESPOND_UTHER_14", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
            return;
          case 4:
            Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_RESPOND_REXXAR_19", "VO_NAX15_01_RESPOND_REXXAR_19", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
            return;
          case 5:
            Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_RESPOND_MALFURION_ALT_21", "VO_NAX15_01_RESPOND_MALFURION_ALT_21", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
            return;
          case 6:
            Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_RESPOND_GULDAN_22", "VO_NAX15_01_RESPOND_GULDAN_22", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
            return;
          case 7:
            Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_RESPOND_JAINA_23", "VO_NAX15_01_RESPOND_JAINA_23", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
            return;
          case 8:
            Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_NAX15_01_RESPOND_ANDUIN_24", "VO_NAX15_01_RESPOND_ANDUIN_24", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
            return;
          default:
            return;
        }
      default:
        if (emoteType1 != EmoteType.WOW)
          break;
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_KT_HEIGAN2_55", "VO_KT_HEIGAN2_55", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
        break;
    }
  }
}
