﻿// Decompiled with JetBrains decompiler
// Type: FontTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class FontTable : MonoBehaviour
{
  private List<FontTable.InitializedListener> m_initializedListeners = new List<FontTable.InitializedListener>();
  private Map<string, FontDef> m_defs = new Map<string, FontDef>();
  public List<FontTableEntry> m_Entries;
  private static FontTable s_instance;
  private bool m_initialized;
  private int m_initialDefsLoading;

  private void Awake()
  {
    FontTable.s_instance = this;
    if (!((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null))
      return;
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
      ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
    FontTable.s_instance = (FontTable) null;
  }

  public static FontTable Get()
  {
    return FontTable.s_instance;
  }

  public FontDef GetFontDef(Font enUSFont)
  {
    return this.GetFontDef(this.GetFontDefName(enUSFont));
  }

  public FontDef GetFontDef(string name)
  {
    FontDef fontDef = (FontDef) null;
    this.m_defs.TryGetValue(name, out fontDef);
    return fontDef;
  }

  public void Reset()
  {
    using (Map<string, FontDef>.Enumerator enumerator = this.m_defs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, FontDef> current = enumerator.Current;
        if (!((UnityEngine.Object) current.Value == (UnityEngine.Object) null))
          UnityEngine.Object.Destroy((UnityEngine.Object) current.Value.gameObject);
      }
    }
    this.m_defs.Clear();
    foreach (Transform componentsInChild in this.GetComponentsInChildren<Transform>())
    {
      if (!((UnityEngine.Object) componentsInChild == (UnityEngine.Object) this.transform))
        UnityEngine.Object.Destroy((UnityEngine.Object) componentsInChild.gameObject);
    }
    this.Initialize();
  }

  public bool IsInitialized()
  {
    return this.m_initialized;
  }

  public void Initialize()
  {
    this.m_initialized = false;
    this.m_initialDefsLoading = this.m_Entries.Count;
    if (this.m_initialDefsLoading == 0)
    {
      this.FinishInitialization();
    }
    else
    {
      using (List<FontTableEntry>.Enumerator enumerator = this.m_Entries.GetEnumerator())
      {
        while (enumerator.MoveNext())
          AssetLoader.Get().LoadFontDef(enumerator.Current.m_FontDefName, new AssetLoader.GameObjectCallback(this.OnFontDefLoaded), (object) null, false);
      }
    }
  }

  public void AddInitializedCallback(FontTable.InitializedCallback callback)
  {
    this.AddInitializedCallback(callback, (object) null);
  }

  public void AddInitializedCallback(FontTable.InitializedCallback callback, object userData)
  {
    FontTable.InitializedListener initializedListener = new FontTable.InitializedListener();
    initializedListener.SetCallback(callback);
    initializedListener.SetUserData(userData);
    if (this.m_initializedListeners.Contains(initializedListener))
      return;
    this.m_initializedListeners.Add(initializedListener);
  }

  public bool RemoveInitializedCallback(FontTable.InitializedCallback callback)
  {
    return this.RemoveInitializedCallback(callback, (object) null);
  }

  public bool RemoveInitializedCallback(FontTable.InitializedCallback callback, object userData)
  {
    FontTable.InitializedListener initializedListener = new FontTable.InitializedListener();
    initializedListener.SetCallback(callback);
    initializedListener.SetUserData(userData);
    return this.m_initializedListeners.Remove(initializedListener);
  }

  private void OnFontDefLoaded(string name, GameObject go, object userData)
  {
    Log.Kyle.Print("OnFontDefLoaded {0}", (object) name);
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      this.OnInitialDefLoaded();
    }
    else
    {
      FontDef component = go.GetComponent<FontDef>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        string message = GameStrings.Format("GLOBAL_ERROR_ASSET_INCORRECT_DATA", (object) name);
        Error.AddFatal(message);
        Debug.LogError((object) string.Format("FontTable.OnFontDefLoaded() - name={0} message={1}", (object) name, (object) message));
        this.OnInitialDefLoaded();
      }
      else
      {
        component.transform.parent = this.transform;
        this.m_defs.Add(name, component);
        this.OnInitialDefLoaded();
      }
    }
  }

  private void OnInitialDefLoaded()
  {
    --this.m_initialDefsLoading;
    if (this.m_initialDefsLoading > 0)
      return;
    this.FinishInitialization();
  }

  private void FinishInitialization()
  {
    this.m_initialized = true;
    this.FireInitializedCallbacks();
  }

  private void FireInitializedCallbacks()
  {
    FontTable.InitializedListener[] array = this.m_initializedListeners.ToArray();
    this.m_initializedListeners.Clear();
    foreach (FontTable.InitializedListener initializedListener in array)
      initializedListener.Fire();
  }

  private string GetFontDefName(Font font)
  {
    if ((UnityEngine.Object) font == (UnityEngine.Object) null)
      return (string) null;
    return this.GetFontDefName(font.name);
  }

  private string GetFontDefName(string fontName)
  {
    using (List<FontTableEntry>.Enumerator enumerator = this.m_Entries.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FontTableEntry current = enumerator.Current;
        if (current.m_FontName == fontName)
          return current.m_FontDefName;
      }
    }
    return (string) null;
  }

  private void WillReset()
  {
    AssetCache.ClearAssetFamilyCache(AssetFamily.FontDef, true, true);
    this.m_defs.Clear();
  }

  private class InitializedListener : EventListener<FontTable.InitializedCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  public delegate void InitializedCallback(object userData);
}
