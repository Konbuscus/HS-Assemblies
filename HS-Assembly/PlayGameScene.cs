﻿// Decompiled with JetBrains decompiler
// Type: PlayGameScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public abstract class PlayGameScene : Scene
{
  private bool m_deckPickerIsLoaded;

  protected void Start()
  {
    AssetLoader.Get().LoadUIScreen(this.GetScreenName(), new AssetLoader.GameObjectCallback(this.OnUIScreenLoaded), (object) null, false);
  }

  protected void Update()
  {
    Network.Get().ProcessNetwork();
  }

  public void OnDeckPickerLoaded()
  {
    this.m_deckPickerIsLoaded = true;
  }

  public abstract string GetScreenName();

  public override void PreUnload()
  {
    if (!((Object) DeckPickerTrayDisplay.Get() != (Object) null))
      return;
    DeckPickerTrayDisplay.Get().PreUnload();
  }

  public override void Unload()
  {
    DeckPickerTray.Get().Unload();
    this.m_deckPickerIsLoaded = false;
  }

  private void OnUIScreenLoaded(string name, GameObject screen, object callbackData)
  {
    if ((Object) screen == (Object) null)
      UnityEngine.Debug.LogError((object) string.Format("PlayGameScene.OnUIScreenLoaded() - failed to load screen {0}", (object) name));
    else
      this.StartCoroutine(this.WaitForAllToBeLoaded());
  }

  [DebuggerHidden]
  private IEnumerator WaitForAllToBeLoaded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PlayGameScene.\u003CWaitForAllToBeLoaded\u003Ec__IteratorA3() { \u003C\u003Ef__this = this };
  }

  protected virtual bool IsLoaded()
  {
    return this.m_deckPickerIsLoaded;
  }
}
