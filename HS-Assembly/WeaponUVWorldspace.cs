﻿// Decompiled with JetBrains decompiler
// Type: WeaponUVWorldspace
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WeaponUVWorldspace : MonoBehaviour
{
  public float xOffset;
  public float yOffset;

  private void Start()
  {
  }

  private void Update()
  {
    Vector3 vector3 = this.transform.position * 0.7f;
    Material material = this.gameObject.GetComponent<Renderer>().material;
    material.SetFloat("_OffsetX", -vector3.z - this.xOffset);
    material.SetFloat("_OffsetY", -vector3.x - this.yOffset);
  }
}
