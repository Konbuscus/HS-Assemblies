﻿// Decompiled with JetBrains decompiler
// Type: BRM17_ZombieNef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;

public class BRM17_ZombieNef : BRM_MissionEntity
{
  private bool m_heroPowerLinePlayed;
  private bool m_cardLinePlayed;
  private bool m_inOnyxiaState;
  private Actor m_nefActor;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BRMA17_1_DEATHWING_88");
    this.PreloadSound("VO_BRMA17_1_HERO_POWER_87");
    this.PreloadSound("VO_BRMA17_1_CARD_86");
    this.PreloadSound("VO_BRMA17_1_RESPONSE_85");
    this.PreloadSound("VO_BRMA17_1_TURN1_79");
    this.PreloadSound("VO_BRMA17_1_RESURRECT1_82");
    this.PreloadSound("VO_BRMA17_1_RESURRECT3_84");
    this.PreloadSound("VO_BRMA17_1_NEF_AIR1_89");
    this.PreloadSound("VO_BRMA17_1_NEF_AIR2_90");
    this.PreloadSound("VO_BRMA17_1_NEF_AIR3_91");
    this.PreloadSound("VO_BRMA17_1_NEF_AIR4_92");
    this.PreloadSound("VO_BRMA17_1_NEF_AIR5_93");
    this.PreloadSound("VO_BRMA17_1_NEF_AIR6_94");
    this.PreloadSound("VO_BRMA17_1_NEF_AIR7_95");
    this.PreloadSound("VO_BRMA17_1_NEF_AIR8_96");
    this.PreloadSound("VO_BRMA17_1_TRANSFORM1_80");
    this.PreloadSound("VO_BRMA17_1_TRANSFORM2_81");
    this.PreloadSound("OnyxiaBoss_Start_1");
    this.PreloadSound("OnyxiaBoss_Death_1");
    this.PreloadSound("OnyxiaBoss_EmoteResponse_1");
  }

  protected override void PlayEmoteResponse(EmoteType emoteType, CardSoundSpell emoteSpell)
  {
    Actor actor = GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor();
    switch (emoteType)
    {
      case EmoteType.GREETINGS:
      case EmoteType.WELL_PLAYED:
      case EmoteType.OOPS:
      case EmoteType.THREATEN:
      case EmoteType.THANKS:
      case EmoteType.SORRY:
        string cardId = GameState.Get().GetOpposingSidePlayer().GetHero().GetCardId();
        if (cardId == "BRMA17_2" || cardId == "BRMA17_2H")
        {
          Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_BRMA17_1_RESPONSE_85", "VO_BRMA17_1_RESPONSE_85", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
          break;
        }
        if (!(cardId == "BRMA17_3") && !(cardId == "BRMA17_3H"))
          break;
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("OnyxiaBoss_EmoteResponse_1", "OnyxiaBoss_EmoteResponse_1", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
        break;
    }
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM17_ZombieNef.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator159() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM17_ZombieNef.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator15A() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM17_ZombieNef.\u003CHandleMissionEventWithTiming\u003Ec__Iterator15B() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
