﻿// Decompiled with JetBrains decompiler
// Type: DoomedMinionSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DoomedMinionSpell : SuperSpell
{
  public SpellType m_SpellType;

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    using (List<GameObject>.Enumerator enumerator = this.GetVisualTargets().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          current.GetComponent<Card>().ActivateActorSpell(this.m_SpellType);
      }
    }
    --this.m_effectsPendingFinish;
    this.FinishIfPossible();
  }
}
