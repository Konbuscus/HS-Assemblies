﻿// Decompiled with JetBrains decompiler
// Type: FatalErrorScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FatalErrorScene : Scene
{
  protected override void Awake()
  {
    AssetLoader.Get().LoadGameObject("FatalErrorScreen", new AssetLoader.GameObjectCallback(this.OnFatalErrorScreenLoaded), (object) null, false);
    base.Awake();
    Navigation.Clear();
    Network.AppAbort();
    UserAttentionManager.StartBlocking(UserAttentionBlocker.FATAL_ERROR_SCENE);
    if ((Object) DialogManager.Get() != (Object) null)
    {
      Log.Mike.Print("FatalErrorScene.Awake() - calling DialogManager.Get().ClearAllImmediately()");
      DialogManager.Get().ClearAllImmediately();
    }
    foreach (Component allCamera in Camera.allCameras)
    {
      FullScreenEffects component = allCamera.GetComponent<FullScreenEffects>();
      if (!((Object) component == (Object) null))
        component.Disable();
    }
  }

  private void Start()
  {
    SceneMgr.Get().NotifySceneLoaded();
  }

  public override void Unload()
  {
    UserAttentionManager.StopBlocking(UserAttentionBlocker.FATAL_ERROR_SCENE);
  }

  private void OnFatalErrorScreenLoaded(string name, GameObject go, object callbackData)
  {
    if (!((Object) go == (Object) null))
      return;
    this.gameObject.AddComponent<FatalErrorDialog>();
  }
}
