﻿// Decompiled with JetBrains decompiler
// Type: FriendListButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FriendListButton : FriendListUIElement
{
  public GameObject m_Background;
  public UberText m_Text;
  public GameObject m_ActiveGlow;

  public string GetText()
  {
    return this.m_Text.Text;
  }

  public void SetText(string text)
  {
    this.m_Text.Text = text;
    this.UpdateAll();
  }

  public void ShowActiveGlow(bool show)
  {
    if (!((Object) this.m_ActiveGlow != (Object) null))
      return;
    HighlightState componentInChildren = this.m_ActiveGlow.GetComponentInChildren<HighlightState>();
    if (!((Object) componentInChildren != (Object) null))
      return;
    if (show)
      componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    else
      componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_OFF);
  }

  private void UpdateAll()
  {
    this.UpdateHighlight();
  }
}
