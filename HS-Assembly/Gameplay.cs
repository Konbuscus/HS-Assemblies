﻿// Decompiled with JetBrains decompiler
// Type: Gameplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using PegasusGame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Gameplay : Scene
{
  private List<NameBanner> m_nameBanners = new List<NameBanner>();
  private Queue<List<Network.PowerHistory>> m_queuedPowerHistory = new Queue<List<Network.PowerHistory>>();
  private static Gameplay s_instance;
  private bool m_unloading;
  private BnetErrorInfo m_lastFatalBnetErrorInfo;
  private bool m_handleLastFatalBnetErrorNow;
  private float m_boardProgress;
  private NameBanner m_nameBannerGamePlayPhone;
  private Actor m_cardDrawStandIn;
  private bool m_criticalAssetsLoaded;
  private float? m_originalTimeScale;

  protected override void Awake()
  {
    Log.LoadingScreen.Print("Gameplay.Awake()");
    base.Awake();
    Gameplay.s_instance = this;
    GameState gameState = GameState.Initialize();
    if (this.ShouldHandleDisconnect())
    {
      Log.LoadingScreen.Print(LogLevel.Warning, "Gameplay.Awake() - DISCONNECTED", new object[0]);
      this.HandleDisconnect();
    }
    else
    {
      Network.Get().SetGameServerDisconnectEventListener(new Network.GameServerDisconnectEvent(this.OnDisconnect));
      CheatMgr.Get().RegisterCheatHandler("saveme", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_saveme), (string) null, (string) null, (string) null);
      gameState.RegisterCreateGameListener(new GameState.CreateGameCallback(this.OnCreateGame));
      AssetLoader.Get().LoadGameObject("InputManager", new AssetLoader.GameObjectCallback(this.OnInputManagerLoaded), (object) null, false);
      AssetLoader.Get().LoadGameObject("MulliganManager", new AssetLoader.GameObjectCallback(this.OnMulliganManagerLoaded), (object) null, false);
      AssetLoader.Get().LoadGameObject("ThinkEmoteController", true, false);
      AssetLoader.Get().LoadActor("Card_Hidden", new AssetLoader.GameObjectCallback(this.OnCardDrawStandinLoaded), (object) null, false);
      AssetLoader.Get().LoadGameObject("TurnStartManager", new AssetLoader.GameObjectCallback(this.OnTurnStartManagerLoaded), (object) null, false);
      AssetLoader.Get().LoadGameObject("TargetReticleManager", new AssetLoader.GameObjectCallback(this.OnTargetReticleManagerLoaded), (object) null, false);
      AssetLoader.Get().LoadGameObject("GameplayErrorManager", new AssetLoader.GameObjectCallback(this.OnGameplayErrorManagerLoaded), (object) null, false);
      AssetLoader.Get().LoadGameObject("RemoteActionHandler", new AssetLoader.GameObjectCallback(this.OnRemoteActionHandlerLoaded), (object) null, false);
      AssetLoader.Get().LoadGameObject("ChoiceCardMgr", new AssetLoader.GameObjectCallback(this.OnChoiceCardMgrLoaded), (object) null, false);
      LoadingScreen.Get().RegisterFinishedTransitionListener(new LoadingScreen.FinishedTransitionCallback(this.OnTransitionFinished));
      this.m_boardProgress = -1f;
      this.ProcessGameSetupPacket();
    }
  }

  private void OnDestroy()
  {
    Log.LoadingScreen.Print("Gameplay.OnDestroy()");
    this.RestoreOriginalTimeScale();
    Gameplay.s_instance = (Gameplay) null;
  }

  private void Start()
  {
    Log.LoadingScreen.Print("Gameplay.Start()");
    this.CheckBattleNetConnection();
    Network network = Network.Get();
    network.AddBnetErrorListener(new Network.BnetErrorCallback(this.OnBnetError));
    network.RegisterNetHandler((object) PegasusGame.PowerHistory.PacketID.ID, new Network.NetHandler(this.OnPowerHistory), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) AllOptions.PacketID.ID, new Network.NetHandler(this.OnAllOptions), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusGame.EntityChoices.PacketID.ID, new Network.NetHandler(this.OnEntityChoices), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusGame.EntitiesChosen.PacketID.ID, new Network.NetHandler(this.OnEntitiesChosen), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusGame.UserUI.PacketID.ID, new Network.NetHandler(this.OnUserUI), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) NAckOption.PacketID.ID, new Network.NetHandler(this.OnOptionRejected), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) PegasusGame.TurnTimer.PacketID.ID, new Network.NetHandler(this.OnTurnTimerUpdate), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) SpectatorNotify.PacketID.ID, new Network.NetHandler(this.OnSpectatorNotify), (Network.TimeoutHandler) null);
    network.GetGameState();
  }

  private void CheckBattleNetConnection()
  {
    if (Network.IsLoggedIn() || !Network.ShouldBeConnectedToAurora())
      return;
    this.OnBnetError(new BnetErrorInfo(BnetFeature.Bnet, BnetFeatureEvent.Bnet_OnDisconnected, BattleNetErrors.ERROR_RPC_DISCONNECT), (object) null);
  }

  private void Update()
  {
    this.CheckCriticalAssetLoads();
    Network.Get().ProcessNetwork();
    if (this.IsDoneUpdatingGame())
    {
      this.HandleLastFatalBnetError();
    }
    else
    {
      if (GameMgr.Get().IsFindingGame() || this.m_unloading || (SceneMgr.Get().WillTransition() || !this.AreCriticalAssetsLoaded()))
        return;
      GameState.Get().Update();
    }
  }

  private void OnGUI()
  {
    this.LayoutProgressGUI();
  }

  private void LayoutProgressGUI()
  {
    if ((double) this.m_boardProgress < 0.0)
      return;
    Vector2 vector2_1 = new Vector2(150f, 30f);
    Vector2 vector2_2 = new Vector2((float) ((double) Screen.width * 0.5 - (double) vector2_1.x * 0.5), (float) ((double) Screen.height * 0.5 - (double) vector2_1.y * 0.5));
    GUI.Box(new Rect(vector2_2.x, vector2_2.y, vector2_1.x, vector2_1.y), string.Empty);
    GUI.Box(new Rect(vector2_2.x, vector2_2.y, this.m_boardProgress * vector2_1.x, vector2_1.y), string.Empty);
    GUI.TextField(new Rect(vector2_2.x, vector2_2.y, vector2_1.x, vector2_1.y), string.Format("{0:0}%", (object) (float) ((double) this.m_boardProgress * 100.0)));
  }

  public static Gameplay Get()
  {
    return Gameplay.s_instance;
  }

  public override void PreUnload()
  {
    this.m_unloading = true;
    if (!((UnityEngine.Object) Board.Get() != (UnityEngine.Object) null) || !((UnityEngine.Object) BoardCameras.Get() != (UnityEngine.Object) null))
      return;
    LoadingScreen.Get().SetFreezeFrameCamera(Camera.main);
    LoadingScreen.Get().SetTransitionAudioListener(BoardCameras.Get().GetAudioListener());
  }

  public override bool IsUnloading()
  {
    return this.m_unloading;
  }

  public override void Unload()
  {
    Log.LoadingScreen.Print("Gameplay.Unload()");
    bool flag = this.IsLeavingGameUnfinished();
    GameState.Shutdown();
    Network network = Network.Get();
    network.RemoveGameServerDisconnectEventListener(new Network.GameServerDisconnectEvent(this.OnDisconnect));
    network.RemoveBnetErrorListener(new Network.BnetErrorCallback(this.OnBnetError));
    network.RemoveNetHandler((object) PegasusGame.PowerHistory.PacketID.ID, new Network.NetHandler(this.OnPowerHistory));
    network.RemoveNetHandler((object) AllOptions.PacketID.ID, new Network.NetHandler(this.OnAllOptions));
    network.RemoveNetHandler((object) PegasusGame.EntityChoices.PacketID.ID, new Network.NetHandler(this.OnEntityChoices));
    network.RemoveNetHandler((object) PegasusGame.EntitiesChosen.PacketID.ID, new Network.NetHandler(this.OnEntitiesChosen));
    network.RemoveNetHandler((object) PegasusGame.UserUI.PacketID.ID, new Network.NetHandler(this.OnUserUI));
    network.RemoveNetHandler((object) NAckOption.PacketID.ID, new Network.NetHandler(this.OnOptionRejected));
    network.RemoveNetHandler((object) PegasusGame.TurnTimer.PacketID.ID, new Network.NetHandler(this.OnTurnTimerUpdate));
    network.RemoveNetHandler((object) SpectatorNotify.PacketID.ID, new Network.NetHandler(this.OnSpectatorNotify));
    CheatMgr.Get().UnregisterCheatHandler("saveme", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_saveme));
    if (flag)
    {
      if (GameMgr.Get().IsPendingAutoConcede())
      {
        Network.AutoConcede();
        GameMgr.Get().SetPendingAutoConcede(false);
      }
      Network.DisconnectFromGameServer();
    }
    using (List<NameBanner>.Enumerator enumerator = this.m_nameBanners.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Unload();
    }
    if ((UnityEngine.Object) this.m_nameBannerGamePlayPhone != (UnityEngine.Object) null)
      this.m_nameBannerGamePlayPhone.Unload();
    this.m_unloading = false;
  }

  public void RemoveClassNames()
  {
    using (List<NameBanner>.Enumerator enumerator = this.m_nameBanners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NameBanner current = enumerator.Current;
        current.FadeOutSubtext();
        current.PositionNameText(true);
      }
    }
  }

  public void RemoveNameBanners()
  {
    using (List<NameBanner>.Enumerator enumerator = this.m_nameBanners.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.m_nameBanners.Clear();
  }

  public void AddGamePlayNameBannerPhone()
  {
    if (!((UnityEngine.Object) this.m_nameBannerGamePlayPhone == (UnityEngine.Object) null))
      return;
    AssetLoader.Get().LoadGameObject("NameBannerGamePlay_phone", new AssetLoader.GameObjectCallback(this.OnPlayerBannerLoaded), (object) Player.Side.OPPOSING, false);
  }

  public void RemoveGamePlayNameBannerPhone()
  {
    if (!((UnityEngine.Object) this.m_nameBannerGamePlayPhone != (UnityEngine.Object) null))
      return;
    this.m_nameBannerGamePlayPhone.Unload();
  }

  public void UpdateFriendlySideMedalChange(MedalInfoTranslator medalInfo)
  {
    using (List<NameBanner>.Enumerator enumerator = this.m_nameBanners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NameBanner current = enumerator.Current;
        if (current.GetPlayerSide() == Player.Side.FRIENDLY)
          current.UpdateMedalChange(medalInfo);
      }
    }
  }

  public void UpdateEnemySideNameBannerName(string newName)
  {
    using (List<NameBanner>.Enumerator enumerator = this.m_nameBanners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NameBanner current = enumerator.Current;
        if (current.GetPlayerSide() == Player.Side.OPPOSING)
          current.SetName(newName);
      }
    }
  }

  public Actor GetCardDrawStandIn()
  {
    return this.m_cardDrawStandIn;
  }

  public NameBanner GetNameBannerForSide(Player.Side wantedSide)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Gameplay.\u003CGetNameBannerForSide\u003Ec__AnonStorey3BC sideCAnonStorey3Bc = new Gameplay.\u003CGetNameBannerForSide\u003Ec__AnonStorey3BC();
    // ISSUE: reference to a compiler-generated field
    sideCAnonStorey3Bc.wantedSide = wantedSide;
    // ISSUE: reference to a compiler-generated field
    if ((UnityEngine.Object) this.m_nameBannerGamePlayPhone != (UnityEngine.Object) null && this.m_nameBannerGamePlayPhone.GetPlayerSide() == sideCAnonStorey3Bc.wantedSide)
      return this.m_nameBannerGamePlayPhone;
    // ISSUE: reference to a compiler-generated method
    return this.m_nameBanners.Find(new Predicate<NameBanner>(sideCAnonStorey3Bc.\u003C\u003Em__143));
  }

  public void SetGameStateBusy(bool busy, float delay)
  {
    if ((double) delay <= (double) Mathf.Epsilon)
      GameState.Get().SetBusy(busy);
    else
      this.StartCoroutine(this.SetGameStateBusyWithDelay(busy, delay));
  }

  public void SwapCardBacks()
  {
    int cardBackId1 = GameState.Get().GetOpposingSidePlayer().GetCardBackId();
    int cardBackId2 = GameState.Get().GetFriendlySidePlayer().GetCardBackId();
    GameState.Get().GetOpposingSidePlayer().SetCardBackId(cardBackId2);
    GameState.Get().GetFriendlySidePlayer().SetCardBackId(cardBackId1);
    CardBackManager.Get().SetGameCardBackIDs(cardBackId1, cardBackId2);
  }

  private void ProcessGameSetupPacket()
  {
    Network.GameSetup gameSetup = GameMgr.Get().GetGameSetup();
    this.LoadBoard(gameSetup);
    GameState.Get().OnGameSetup(gameSetup);
  }

  private bool IsHandlingNetworkProblem()
  {
    return this.ShouldHandleDisconnect() || this.m_handleLastFatalBnetErrorNow;
  }

  private bool ShouldHandleDisconnect()
  {
    return !Network.IsConnectedToGameServer() && !Network.WasGameConceded() && (Network.WasDisconnectRequested() && GameMgr.Get() != null && (GameMgr.Get().IsSpectator() && !GameState.Get().IsGameOverNowOrPending()) || (GameState.Get() == null || !GameState.Get().IsGameOverNowOrPending()));
  }

  private void OnDisconnect(BattleNetErrors error)
  {
    if (!this.ShouldHandleDisconnect() || error != BattleNetErrors.ERROR_RPC_PEER_DISCONNECTED)
      return;
    Network.Get().RemoveGameServerDisconnectEventListener(new Network.GameServerDisconnectEvent(this.OnDisconnect));
    this.HandleDisconnect();
  }

  private void HandleDisconnect()
  {
    Log.GameMgr.PrintWarning("Gameplay is handling a game disconnect.");
    if (ReconnectMgr.Get().ReconnectFromGameplay() || SpectatorManager.Get().HandleDisconnectFromGameplay())
      return;
    DisconnectMgr.Get().DisconnectFromGameplay();
  }

  private bool IsDoneUpdatingGame()
  {
    return this.m_handleLastFatalBnetErrorNow || !Network.IsConnectedToGameServer() && !GameState.Get().HasPowersToProcess() && GameState.Get().IsGameOver();
  }

  private bool OnBnetError(BnetErrorInfo info, object userData)
  {
    if (Network.Get().OnIgnorableBnetError(info) || this.m_handleLastFatalBnetErrorNow)
      return true;
    this.m_lastFatalBnetErrorInfo = info;
    switch (info.GetError())
    {
      case BattleNetErrors.ERROR_PARENTAL_CONTROL_RESTRICTION:
      case BattleNetErrors.ERROR_SESSION_DUPLICATE:
        this.m_handleLastFatalBnetErrorNow = true;
        break;
    }
    return true;
  }

  private void OnBnetErrorResponse(AlertPopup.Response response, object userData)
  {
    if ((bool) ApplicationMgr.AllowResetFromFatalError)
      ApplicationMgr.Get().Reset();
    else
      ApplicationMgr.Get().Exit();
  }

  private void HandleLastFatalBnetError()
  {
    if (this.m_lastFatalBnetErrorInfo == null)
      return;
    if (this.m_handleLastFatalBnetErrorNow)
    {
      Network.Get().OnFatalBnetError(this.m_lastFatalBnetErrorInfo);
      this.m_handleLastFatalBnetErrorNow = false;
    }
    else
    {
      string key = !(bool) ApplicationMgr.AllowResetFromFatalError ? "GAMEPLAY_DISCONNECT_BODY" : "GAMEPLAY_DISCONNECT_BODY_RESET";
      if (GameMgr.Get().IsSpectator())
        key = !(bool) ApplicationMgr.AllowResetFromFatalError ? "GAMEPLAY_SPECTATOR_DISCONNECT_BODY" : "GAMEPLAY_SPECTATOR_DISCONNECT_BODY_RESET";
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GAMEPLAY_DISCONNECT_HEADER"),
        m_text = GameStrings.Get(key),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.OK,
        m_responseCallback = new AlertPopup.ResponseCallback(this.OnBnetErrorResponse)
      });
    }
    this.m_lastFatalBnetErrorInfo = (BnetErrorInfo) null;
  }

  private void OnPowerHistory()
  {
    List<Network.PowerHistory> powerHistory = Network.GetPowerHistory();
    Log.LoadingScreen.Print("Gameplay.OnPowerHistory() - powerList={0}", (object) powerHistory.Count);
    if (this.AreCriticalAssetsLoaded())
      GameState.Get().OnPowerHistory(powerHistory);
    else
      this.m_queuedPowerHistory.Enqueue(powerHistory);
  }

  private void OnAllOptions()
  {
    Network.Options options = Network.GetOptions();
    Log.LoadingScreen.Print("Gameplay.OnAllOptions() - id={0}", (object) options.ID);
    GameState.Get().OnAllOptions(options);
  }

  private void OnEntityChoices()
  {
    Network.EntityChoices entityChoices = Network.GetEntityChoices();
    Log.LoadingScreen.Print("Gameplay.OnEntityChoices() - id={0}", (object) entityChoices.ID);
    GameState.Get().OnEntityChoices(entityChoices);
  }

  private void OnEntitiesChosen()
  {
    GameState.Get().OnEntitiesChosen(Network.GetEntitiesChosen());
  }

  private void OnUserUI()
  {
    if (!(bool) ((UnityEngine.Object) RemoteActionHandler.Get()))
      return;
    RemoteActionHandler.Get().HandleAction(Network.GetUserUI());
  }

  private void OnOptionRejected()
  {
    GameState.Get().OnOptionRejected(Network.GetNAckOption());
  }

  private void OnTurnTimerUpdate()
  {
    GameState.Get().OnTurnTimerUpdate(Network.GetTurnTimerInfo());
  }

  private void OnSpectatorNotify()
  {
    GameState.Get().OnSpectatorNotifyEvent(Network.GetSpectatorNotify());
  }

  private bool AreCriticalAssetsLoaded()
  {
    return this.m_criticalAssetsLoaded;
  }

  private bool CheckCriticalAssetLoads()
  {
    if (this.m_criticalAssetsLoaded)
      return true;
    if ((UnityEngine.Object) Board.Get() == (UnityEngine.Object) null || (UnityEngine.Object) BoardCameras.Get() == (UnityEngine.Object) null || (UnityEngine.Object) BoardStandardGame.Get() == (UnityEngine.Object) null || (GameMgr.Get().IsTutorial() && (UnityEngine.Object) BoardTutorial.Get() == (UnityEngine.Object) null || ((UnityEngine.Object) MulliganManager.Get() == (UnityEngine.Object) null || (UnityEngine.Object) TurnStartManager.Get() == (UnityEngine.Object) null)) || ((UnityEngine.Object) TargetReticleManager.Get() == (UnityEngine.Object) null || (UnityEngine.Object) GameplayErrorManager.Get() == (UnityEngine.Object) null || ((UnityEngine.Object) EndTurnButton.Get() == (UnityEngine.Object) null || (UnityEngine.Object) BigCard.Get() == (UnityEngine.Object) null) || ((UnityEngine.Object) CardTypeBanner.Get() == (UnityEngine.Object) null || (UnityEngine.Object) TurnTimer.Get() == (UnityEngine.Object) null || ((UnityEngine.Object) CardColorSwitcher.Get() == (UnityEngine.Object) null || (UnityEngine.Object) RemoteActionHandler.Get() == (UnityEngine.Object) null))) || ((UnityEngine.Object) ChoiceCardMgr.Get() == (UnityEngine.Object) null || (UnityEngine.Object) InputManager.Get() == (UnityEngine.Object) null))
      return false;
    this.m_criticalAssetsLoaded = true;
    this.ProcessQueuedPowerHistory();
    return true;
  }

  private void InitCardBacks()
  {
    CardBackManager.Get().SetGameCardBackIDs(GameState.Get().GetFriendlySidePlayer().GetCardBackId(), GameState.Get().GetOpposingSidePlayer().GetCardBackId());
  }

  private void LoadBoard(Network.GameSetup setup)
  {
    string board = Cheats.Get().GetBoard();
    string boardName = !string.IsNullOrEmpty(board) ? board : FileUtils.GameAssetPathToName(GameDbf.Board.GetRecord(setup.Board).Prefab);
    if ((bool) UniversalInputManager.UsePhoneUI)
      boardName = string.Format("{0}_phone", (object) boardName);
    AssetLoader.Get().LoadBoard(boardName, new AssetLoader.GameObjectCallback(this.OnBoardLoaded), (object) null, false);
  }

  [DebuggerHidden]
  private IEnumerator NotifyPlayersOfBoardLoad()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Gameplay.\u003CNotifyPlayersOfBoardLoad\u003Ec__IteratorB2 boardLoadCIteratorB2 = new Gameplay.\u003CNotifyPlayersOfBoardLoad\u003Ec__IteratorB2();
    return (IEnumerator) boardLoadCIteratorB2;
  }

  private void OnBoardLoaded(string name, GameObject go, object callbackData)
  {
    this.m_boardProgress = -1f;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnBoardLoaded() - FAILED to load board \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    }
    else
    {
      go.GetComponent<Board>().SetBoardDbId(GameMgr.Get().GetGameSetup().Board);
      AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "BoardCameras" : "BoardCameras_phone", new AssetLoader.GameObjectCallback(this.OnBoardCamerasLoaded), (object) null, false);
      AssetLoader.Get().LoadGameObject("BoardStandardGame", new AssetLoader.GameObjectCallback(this.OnBoardStandardGameLoaded), (object) null, false);
      if (!GameMgr.Get().IsTutorial())
        return;
      AssetLoader.Get().LoadGameObject("BoardTutorial", new AssetLoader.GameObjectCallback(this.OnBoardTutorialLoaded), (object) null, false);
    }
  }

  private void OnBoardProgressUpdate(string name, float progress, object callbackData)
  {
    this.m_boardProgress = progress;
  }

  private void OnBoardCamerasLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnBoardCamerasLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    }
    else
    {
      go.transform.parent = Board.Get().transform;
      PegUI.Get().SetInputCamera(Camera.main);
      AssetLoader.Get().LoadActor("CardTypeBanner", false, false);
      AssetLoader.Get().LoadActor("BigCard", false, false);
    }
  }

  private void OnBoardStandardGameLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnBoardStandardGameLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    }
    else
    {
      go.transform.parent = Board.Get().transform;
      AssetLoader.Get().LoadActor("EndTurnButton", new AssetLoader.GameObjectCallback(this.OnEndTurnButtonLoaded), (object) null, false);
      AssetLoader.Get().LoadActor("TurnTimer", new AssetLoader.GameObjectCallback(this.OnTurnTimerLoaded), (object) null, false);
    }
  }

  private void OnBoardTutorialLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnBoardTutorialLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else
      go.transform.parent = Board.Get().transform;
  }

  private void OnEndTurnButtonLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnEndTurnButtonLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    }
    else
    {
      EndTurnButton component = go.GetComponent<EndTurnButton>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnEndTurnButtonLoaded() - ERROR \"{0}\" has no {1} component", (object) name, (object) typeof (EndTurnButton)));
      }
      else
      {
        component.transform.position = Board.Get().FindBone("EndTurnButton").position;
        foreach (Renderer componentsInChild in go.GetComponentsInChildren<Renderer>())
        {
          if (!(bool) ((UnityEngine.Object) componentsInChild.gameObject.GetComponent<TextMesh>()))
            componentsInChild.material.color = Board.Get().m_EndTurnButtonColor;
        }
      }
    }
  }

  private void OnTurnTimerLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnTurnTimerLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    }
    else
    {
      TurnTimer component = go.GetComponent<TurnTimer>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnTurnTimerLoaded() - ERROR \"{0}\" has no {1} component", (object) name, (object) typeof (TurnTimer)));
      else
        component.transform.position = Board.Get().FindBone("TurnTimerBone").position;
    }
  }

  private void OnRemoteActionHandlerLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnRemoteActionHandlerLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else
      go.transform.parent = this.transform;
  }

  private void OnChoiceCardMgrLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnChoiceCardMgrLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else
      go.transform.parent = this.transform;
  }

  private void OnInputManagerLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnInputManagerLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else
      go.transform.parent = this.transform;
  }

  private void OnMulliganManagerLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnMulliganManagerLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else
      go.transform.parent = this.transform;
  }

  private void OnTurnStartManagerLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnTurnStartManagerLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else
      go.transform.parent = this.transform;
  }

  private void OnTargetReticleManagerLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnTargetReticleManagerLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    }
    else
    {
      go.transform.parent = this.transform;
      TargetReticleManager.Get().PreloadTargetArrows();
    }
  }

  private void OnGameplayErrorManagerLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.GameplayErrorManagerLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else
      go.transform.parent = this.transform;
  }

  private void OnPlayerBannerLoaded(string name, GameObject go, object callbackData)
  {
    Player.Side side = (Player.Side) callbackData;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnPlayerBannerLoaded() - FAILED to load \"{0}\" side={1}", (object) name, (object) side.ToString()));
    else if (this.IsHandlingNetworkProblem())
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    else if ((bool) UniversalInputManager.UsePhoneUI)
    {
      NameBanner component = go.GetComponent<NameBanner>();
      if (name == "NameBannerGamePlay_phone")
      {
        this.m_nameBannerGamePlayPhone = component;
        this.m_nameBannerGamePlayPhone.SetPlayerSide(side);
      }
      else
      {
        component.SetPlayerSide(side);
        this.m_nameBanners.Add(component);
      }
    }
    else
    {
      NameBanner component = go.GetComponent<NameBanner>();
      if (!string.IsNullOrEmpty(GameState.Get().GetGameEntity().GetAlternatePlayerName()) && side == Player.Side.FRIENDLY)
        component.UseLongName();
      component.SetPlayerSide(side);
      this.m_nameBanners.Add(component);
    }
  }

  private void OnCardDrawStandinLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) string.Format("Gameplay.OnCardDrawStandinLoaded() - FAILED to load \"{0}\"", (object) name));
    else if (this.IsHandlingNetworkProblem())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    }
    else
    {
      this.m_cardDrawStandIn = go.GetComponent<Actor>();
      go.GetComponentInChildren<CardBackDisplay>().SetCardBack(true);
      this.m_cardDrawStandIn.Hide();
    }
  }

  private void OnTransitionFinished(bool cutoff, object userData)
  {
    LoadingScreen.Get().UnregisterFinishedTransitionListener(new LoadingScreen.FinishedTransitionCallback(this.OnTransitionFinished));
    if (cutoff || this.IsHandlingNetworkProblem())
      return;
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      if (GameState.Get().IsMulliganPhase())
      {
        AssetLoader.Get().LoadGameObject("NameBannerRight_phone", new AssetLoader.GameObjectCallback(this.OnPlayerBannerLoaded), (object) Player.Side.FRIENDLY, false);
        AssetLoader.Get().LoadGameObject("NameBanner_phone", new AssetLoader.GameObjectCallback(this.OnPlayerBannerLoaded), (object) Player.Side.OPPOSING, false);
      }
      else
      {
        if (GameMgr.Get().IsTutorial())
          return;
        this.AddGamePlayNameBannerPhone();
      }
    }
    else
    {
      AssetLoader.Get().LoadGameObject("NameBanner", new AssetLoader.GameObjectCallback(this.OnPlayerBannerLoaded), (object) Player.Side.FRIENDLY, false);
      AssetLoader.Get().LoadGameObject("NameBanner", new AssetLoader.GameObjectCallback(this.OnPlayerBannerLoaded), (object) Player.Side.OPPOSING, false);
    }
  }

  private void ProcessQueuedPowerHistory()
  {
    while (this.m_queuedPowerHistory.Count > 0)
      GameState.Get().OnPowerHistory(this.m_queuedPowerHistory.Dequeue());
  }

  private bool IsLeavingGameUnfinished()
  {
    return (GameState.Get() == null || !GameState.Get().IsGameOver()) && (!GameMgr.Get().IsReconnect() && !SceneMgr.Get().IsModeRequested(SceneMgr.Mode.FATAL_ERROR));
  }

  private void OnCreateGame(GameState.CreateGamePhase phase, object userData)
  {
    if (phase == GameState.CreateGamePhase.CREATING)
    {
      this.InitCardBacks();
      this.StartCoroutine(this.NotifyPlayersOfBoardLoad());
    }
    else
    {
      if (phase != GameState.CreateGamePhase.CREATED)
        return;
      CardBackManager.Get().UpdateAllCardBacks();
    }
  }

  [DebuggerHidden]
  private IEnumerator SetGameStateBusyWithDelay(bool busy, float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Gameplay.\u003CSetGameStateBusyWithDelay\u003Ec__IteratorB3() { delay = delay, busy = busy, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003Ebusy = busy };
  }

  public void SaveOriginalTimeScale()
  {
    this.m_originalTimeScale = new float?(Time.timeScale);
  }

  public void RestoreOriginalTimeScale()
  {
    if (!this.m_originalTimeScale.HasValue)
      return;
    Time.timeScale = this.m_originalTimeScale.Value;
    this.m_originalTimeScale = new float?();
  }

  private bool OnProcessCheat_saveme(string func, string[] args, string rawArgs)
  {
    GameState.Get().DebugNukeServerBlocks();
    return true;
  }
}
