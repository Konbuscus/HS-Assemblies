﻿// Decompiled with JetBrains decompiler
// Type: StoreDisclaimer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StoreDisclaimer : MonoBehaviour
{
  public UberText m_headlineText;
  public UberText m_warningText;
  public UberText m_detailsText;
  public GameObject m_root;

  private void Awake()
  {
    this.m_headlineText.Text = GameStrings.Get("GLUE_STORE_DISCLAIMER_HEADLINE");
    this.m_warningText.Text = GameStrings.Get("GLUE_STORE_DISCLAIMER_WARNING");
    this.m_detailsText.Text = string.Empty;
  }

  public void UpdateTextSize()
  {
    this.m_headlineText.UpdateNow();
    this.m_warningText.UpdateNow();
    this.m_detailsText.UpdateNow();
  }

  public void SetDetailsText(string detailsText)
  {
    this.m_detailsText.Text = detailsText;
  }
}
