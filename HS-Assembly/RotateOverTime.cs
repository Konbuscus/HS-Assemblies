﻿// Decompiled with JetBrains decompiler
// Type: RotateOverTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RotateOverTime : MonoBehaviour
{
  public float RotateSpeedX;
  public float RotateSpeedY;
  public float RotateSpeedZ;
  public bool RandomStartX;
  public bool RandomStartY;
  public bool RandomStartZ;

  private void Start()
  {
    if (this.RandomStartX)
      this.transform.Rotate(Vector3.left, (float) Random.Range(0, 360));
    if (this.RandomStartY)
      this.transform.Rotate(Vector3.up, (float) Random.Range(0, 360));
    if (!this.RandomStartZ)
      return;
    this.transform.Rotate(Vector3.forward, (float) Random.Range(0, 360));
  }

  private void Update()
  {
    this.transform.Rotate(Vector3.left, Time.deltaTime * this.RotateSpeedX, Space.Self);
    this.transform.Rotate(Vector3.up, Time.deltaTime * this.RotateSpeedY, Space.Self);
    this.transform.Rotate(Vector3.forward, Time.deltaTime * this.RotateSpeedZ, Space.Self);
  }
}
