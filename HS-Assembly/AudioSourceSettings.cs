﻿// Decompiled with JetBrains decompiler
// Type: AudioSourceSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AudioSourceSettings
{
  public const bool DEFAULT_BYPASS_EFFECTS = false;
  public const bool DEFAULT_LOOP = false;
  public const float MIN_VOLUME = 0.0f;
  public const float MAX_VOLUME = 1f;
  public const float DEFAULT_VOLUME = 1f;
  public const int MIN_PRIORITY = 0;
  public const int MAX_PRIORITY = 256;
  public const int DEFAULT_PRIORITY = 128;
  public const float MIN_PITCH = -3f;
  public const float MAX_PITCH = 3f;
  public const float DEFAULT_PITCH = 1f;
  public const float MIN_STEREO_PAN = -1f;
  public const float MAX_STEREO_PAN = 1f;
  public const float DEFAULT_STEREO_PAN = 0.0f;
  public const float MIN_SPATIAL_BLEND = 0.0f;
  public const float MAX_SPATIAL_BLEND = 1f;
  public const float DEFAULT_SPATIAL_BLEND = 1f;
  public const float MIN_REVERB_ZONE_MIX = 0.0f;
  public const float MAX_REVERB_ZONE_MIX = 1.1f;
  public const float DEFAULT_REVERB_ZONE_MIX = 1f;
  public const AudioRolloffMode DEFAULT_ROLLOFF_MODE = AudioRolloffMode.Linear;
  public const float MIN_DOPPLER_LEVEL = 0.0f;
  public const float MAX_DOPPLER_LEVEL = 5f;
  public const float DEFAULT_DOPPLER_LEVEL = 1f;
  public const float DEFAULT_MIN_DISTANCE = 100f;
  public const float DEFAULT_MAX_DISTANCE = 500f;
  public const float MIN_SPREAD = 0.0f;
  public const float MAX_SPREAD = 360f;
  public const float DEFAULT_SPREAD = 0.0f;
  public bool m_bypassEffects;
  public bool m_loop;
  public int m_priority;
  public float m_volume;
  public float m_pitch;
  public float m_stereoPan;
  public float m_spatialBlend;
  public float m_reverbZoneMix;
  public AudioRolloffMode m_rolloffMode;
  public float m_dopplerLevel;
  public float m_minDistance;
  public float m_maxDistance;
  public float m_spread;

  public AudioSourceSettings()
  {
    this.LoadDefaults();
  }

  public void LoadDefaults()
  {
    this.m_bypassEffects = false;
    this.m_loop = false;
    this.m_priority = 128;
    this.m_volume = 1f;
    this.m_pitch = 1f;
    this.m_stereoPan = 0.0f;
    this.m_spatialBlend = 1f;
    this.m_reverbZoneMix = 1f;
    this.m_rolloffMode = AudioRolloffMode.Linear;
    this.m_dopplerLevel = 1f;
    this.m_minDistance = 100f;
    this.m_maxDistance = 500f;
    this.m_spread = 0.0f;
  }
}
