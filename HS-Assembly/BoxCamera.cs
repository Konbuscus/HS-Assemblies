﻿// Decompiled with JetBrains decompiler
// Type: BoxCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class BoxCamera : MonoBehaviour
{
  private bool m_disableAccelerometer = true;
  private float MAX_GYRO_RANGE = 2.1f;
  private float ROTATION_SCALE = 0.085f;
  public BoxCameraEventTable m_EventTable;
  public GameObject m_IgnoreFullscreenEffectsCamera;
  public GameObject m_TooltipCamera;
  private Box m_parent;
  private BoxCameraStateInfo m_info;
  private BoxCamera.State m_state;
  private bool m_applyAccelerometer;
  private Vector2 m_currentAngle;
  private Vector3 m_basePosition;
  private Vector2 m_gyroRotation;
  private float m_offset;
  private Vector3 m_lookAtPoint;

  public void SetParent(Box parent)
  {
    this.m_parent = parent;
  }

  public Box GetParent()
  {
    return this.m_parent;
  }

  public BoxCameraStateInfo GetInfo()
  {
    return this.m_info;
  }

  public void SetInfo(BoxCameraStateInfo info)
  {
    this.m_info = info;
  }

  public BoxCameraEventTable GetEventTable()
  {
    return this.m_EventTable;
  }

  public Vector3 GetCameraPosition(BoxCamera.State state)
  {
    Transform transform1;
    Transform transform2;
    if (state == BoxCamera.State.CLOSED)
    {
      transform1 = this.m_info.m_ClosedMinAspectRatioBone.transform;
      transform2 = this.m_info.m_ClosedBone.transform;
    }
    else if (state == BoxCamera.State.CLOSED_WITH_DRAWER)
    {
      transform1 = this.m_info.m_ClosedWithDrawerMinAspectRatioBone.transform;
      transform2 = this.m_info.m_ClosedWithDrawerBone.transform;
    }
    else
    {
      transform1 = this.m_info.m_OpenedMinAspectRatioBone.transform;
      transform2 = this.m_info.m_OpenedBone.transform;
    }
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return transform2.position;
    return TransformUtil.GetAspectRatioDependentPosition(transform1.position, transform2.position);
  }

  public BoxCamera.State GetState()
  {
    return this.m_state;
  }

  public bool ChangeState(BoxCamera.State state)
  {
    if (this.m_state == state)
      return false;
    this.m_state = state;
    Vector3 cameraPosition = this.GetCameraPosition(state);
    this.m_parent.OnAnimStarted();
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_applyAccelerometer = false;
      this.m_basePosition = this.transform.parent.InverseTransformPoint(cameraPosition);
      this.m_lookAtPoint = this.transform.parent.InverseTransformPoint(new Vector3(cameraPosition.x, 1.5f, cameraPosition.z));
      if (cameraPosition == this.gameObject.transform.position)
      {
        this.OnAnimFinished();
        return true;
      }
    }
    Hashtable args = (Hashtable) null;
    if (state == BoxCamera.State.CLOSED)
      args = iTween.Hash((object) "position", (object) cameraPosition, (object) "delay", (object) this.m_info.m_ClosedDelaySec, (object) "time", (object) this.m_info.m_ClosedMoveSec, (object) "easeType", (object) this.m_info.m_ClosedMoveEaseType, (object) "oncomplete", (object) "OnAnimFinished", (object) "oncompletetarget", (object) this.gameObject);
    else if (state == BoxCamera.State.CLOSED_WITH_DRAWER)
      args = iTween.Hash((object) "position", (object) cameraPosition, (object) "delay", (object) this.m_info.m_ClosedWithDrawerDelaySec, (object) "time", (object) this.m_info.m_ClosedWithDrawerMoveSec, (object) "easeType", (object) this.m_info.m_ClosedWithDrawerMoveEaseType, (object) "oncomplete", (object) "OnAnimFinished", (object) "oncompletetarget", (object) this.gameObject);
    else if (state == BoxCamera.State.OPENED)
      args = iTween.Hash((object) "position", (object) cameraPosition, (object) "delay", (object) this.m_info.m_OpenedDelaySec, (object) "time", (object) this.m_info.m_OpenedMoveSec, (object) "easeType", (object) this.m_info.m_OpenedMoveEaseType, (object) "oncomplete", (object) "OnAnimFinished", (object) "oncompletetarget", (object) this.gameObject);
    else if (state == BoxCamera.State.SET_ROTATION_OPENED)
      args = iTween.Hash((object) "position", (object) cameraPosition, (object) "delay", (object) this.m_info.m_OpenedDelaySec, (object) "time", (object) 1.5f, (object) "easeType", (object) this.m_info.m_OpenedMoveEaseType, (object) "oncomplete", (object) "OnAnimFinished", (object) "oncompletetarget", (object) this.gameObject);
    iTween.MoveTo(this.gameObject, args);
    return true;
  }

  public void EnableAccelerometer()
  {
    if (!MobileCallbackManager.AreMotionEffectsEnabled())
      return;
    this.m_disableAccelerometer = false;
  }

  public void Update()
  {
    if (this.m_disableAccelerometer || (Object) this.transform.parent.gameObject.GetComponent<LoadingScreen>() != (Object) null || !(bool) UniversalInputManager.UsePhoneUI)
      return;
    if (this.m_applyAccelerometer)
    {
      this.m_gyroRotation.x = Input.gyro.rotationRateUnbiased.x;
      this.m_gyroRotation.y = -Input.gyro.rotationRateUnbiased.y;
      this.m_currentAngle.x += this.m_gyroRotation.y * this.ROTATION_SCALE;
      this.m_currentAngle.y += this.m_gyroRotation.x * this.ROTATION_SCALE;
      this.m_currentAngle.x = Mathf.Clamp(this.m_currentAngle.x, -this.MAX_GYRO_RANGE, this.MAX_GYRO_RANGE);
      this.m_currentAngle.y = Mathf.Clamp(this.m_currentAngle.y, -this.MAX_GYRO_RANGE, this.MAX_GYRO_RANGE);
      this.gameObject.transform.localPosition = new Vector3(this.m_basePosition.x, this.m_basePosition.y, this.m_basePosition.z + this.m_currentAngle.y);
    }
    Vector3 worldUp = new Vector3(0.0f, 0.0f, 1f);
    Vector3 worldPosition = this.gameObject.transform.parent.TransformPoint(this.m_lookAtPoint);
    this.gameObject.transform.LookAt(worldPosition, worldUp);
    if (this.m_applyAccelerometer)
    {
      this.m_IgnoreFullscreenEffectsCamera.transform.position = this.gameObject.transform.parent.TransformPoint(this.m_basePosition);
      this.m_IgnoreFullscreenEffectsCamera.transform.LookAt(worldPosition, worldUp);
      this.m_TooltipCamera.transform.position = this.gameObject.transform.parent.TransformPoint(this.m_basePosition);
      this.m_TooltipCamera.transform.LookAt(worldPosition, worldUp);
    }
    else
    {
      TransformUtil.Identity(this.m_TooltipCamera);
      TransformUtil.Identity(this.m_IgnoreFullscreenEffectsCamera);
    }
  }

  public void OnAnimFinished()
  {
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_applyAccelerometer = this.m_state != BoxCamera.State.OPENED;
      this.m_currentAngle = new Vector2(0.0f, 0.0f);
    }
    this.m_parent.OnAnimFinished();
  }

  public void UpdateState(BoxCamera.State state)
  {
    this.m_state = state;
    this.transform.position = this.GetCameraPosition(state);
  }

  public enum State
  {
    CLOSED,
    CLOSED_WITH_DRAWER,
    OPENED,
    SET_ROTATION_OPENED,
  }
}
