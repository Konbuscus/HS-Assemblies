﻿// Decompiled with JetBrains decompiler
// Type: ArcaneDustReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArcaneDustReward : Reward
{
  public GameObject m_dustJar;
  public UberText m_dustCount;

  protected override void InitData()
  {
    this.SetData((RewardData) new ArcaneDustRewardData(), false);
  }

  protected override void ShowReward(bool updateCacheValues)
  {
    ArcaneDustRewardData data = this.Data as ArcaneDustRewardData;
    if (data == null)
    {
      Debug.LogWarning((object) string.Format("ArcaneDustReward.ShowReward() - Data {0} is not ArcaneDustRewardData", (object) this.Data));
    }
    else
    {
      if (!data.IsDummyReward && updateCacheValues)
      {
        NetCache.Get().OnArcaneDustBalanceChanged((long) data.Amount);
        if ((Object) CraftingManager.Get() != (Object) null)
        {
          CraftingManager.Get().AdjustLocalArcaneDustBalance(data.Amount);
          CraftingManager.Get().UpdateBankText();
        }
      }
      this.m_root.SetActive(true);
      this.m_dustCount.Text = data.Amount.ToString();
      Vector3 localScale = this.m_dustJar.transform.localScale;
      this.m_dustJar.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
      iTween.ScaleTo(this.m_dustJar.gameObject, iTween.Hash((object) "scale", (object) localScale, (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.easeOutElastic));
    }
  }

  protected override void HideReward()
  {
    base.HideReward();
    this.m_root.SetActive(false);
  }

  protected override void OnDataSet(bool updateVisuals)
  {
    if (!updateVisuals)
      return;
    this.SetRewardText(GameStrings.Get("GLOBAL_REWARD_ARCANE_DUST_HEADLINE"), string.Empty, string.Empty);
  }
}
