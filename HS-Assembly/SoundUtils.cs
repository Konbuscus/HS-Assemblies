﻿// Decompiled with JetBrains decompiler
// Type: SoundUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SoundUtils
{
  public static PlatformDependentValue<bool> PlATFORM_CAN_DETECT_VOLUME = new PlatformDependentValue<bool>(PlatformCategory.OS) { PC = true, Mac = true, iOS = false, Android = false };

  private static bool IsBackgroundMusicPlaying()
  {
    return false;
  }

  public static Option GetCategoryEnabledOption(SoundCategory cat)
  {
    Option option = Option.INVALID;
    SoundDataTables.s_categoryEnabledOptionMap.TryGetValue(cat, out option);
    return option;
  }

  public static Option GetCategoryVolumeOption(SoundCategory cat)
  {
    Option option = Option.INVALID;
    SoundDataTables.s_categoryVolumeOptionMap.TryGetValue(cat, out option);
    return option;
  }

  public static float GetCategoryVolume(SoundCategory cat)
  {
    float num1 = Options.Get().GetFloat(Option.SOUND_VOLUME);
    Option categoryVolumeOption = SoundUtils.GetCategoryVolumeOption(cat);
    if (categoryVolumeOption == Option.INVALID)
      return num1;
    float num2 = Options.Get().GetFloat(categoryVolumeOption);
    return num1 * num2;
  }

  public static bool IsCategoryEnabled(SoundCategory cat)
  {
    if (SoundUtils.IsMusicCategory(cat) && SoundUtils.IsBackgroundMusicPlaying() || !Options.Get().GetBool(Option.SOUND))
      return false;
    Option categoryEnabledOption = SoundUtils.GetCategoryEnabledOption(cat);
    if (categoryEnabledOption == Option.INVALID)
      return true;
    return Options.Get().GetBool(categoryEnabledOption);
  }

  public static bool IsFxEnabled()
  {
    return SoundUtils.IsCategoryEnabled(SoundCategory.FX);
  }

  public static bool IsMusicEnabled()
  {
    return SoundUtils.IsCategoryEnabled(SoundCategory.MUSIC);
  }

  public static bool IsVoiceEnabled()
  {
    return SoundUtils.IsCategoryEnabled(SoundCategory.VO);
  }

  public static bool IsCategoryAudible(SoundCategory cat)
  {
    if ((double) SoundUtils.GetCategoryVolume(cat) <= (double) Mathf.Epsilon)
      return false;
    return SoundUtils.IsCategoryEnabled(cat);
  }

  public static bool IsFxAudible()
  {
    return SoundUtils.IsCategoryAudible(SoundCategory.FX);
  }

  public static bool IsMusicAudible()
  {
    return SoundUtils.IsCategoryAudible(SoundCategory.MUSIC);
  }

  public static bool IsVoiceAudible()
  {
    return SoundUtils.IsCategoryAudible(SoundCategory.VO);
  }

  public static bool IsMusicCategory(SoundCategory cat)
  {
    return cat == SoundCategory.MUSIC || cat == SoundCategory.SPECIAL_MUSIC;
  }

  public static bool IsVoiceCategory(SoundCategory cat)
  {
    return cat == SoundCategory.VO || cat == SoundCategory.SPECIAL_VO;
  }

  public static SoundCategory GetCategoryFromSource(AudioSource source)
  {
    SoundDef component = source.GetComponent<SoundDef>();
    if ((Object) component == (Object) null)
      return SoundCategory.NONE;
    return component.m_Category;
  }

  public static bool CanDetectVolume()
  {
    return (bool) SoundUtils.PlATFORM_CAN_DETECT_VOLUME;
  }

  public static void SetVolumes(Component c, float volume, bool includeInactive = false)
  {
    if (!(bool) ((Object) c))
      return;
    SoundUtils.SetVolumes(c.gameObject, volume, false);
  }

  public static void SetVolumes(GameObject go, float volume, bool includeInactive = false)
  {
    if (!(bool) ((Object) go))
      return;
    foreach (AudioSource componentsInChild in go.GetComponentsInChildren<AudioSource>(includeInactive))
      SoundManager.Get().SetVolume(componentsInChild, volume);
  }

  public static void SetSourceVolumes(Component c, float volume, bool includeInactive = false)
  {
    if (!(bool) ((Object) c))
      return;
    SoundUtils.SetSourceVolumes(c.gameObject, volume, false);
  }

  public static void SetSourceVolumes(GameObject go, float volume, bool includeInactive = false)
  {
    if (!(bool) ((Object) go))
      return;
    foreach (AudioSource componentsInChild in go.GetComponentsInChildren<AudioSource>(includeInactive))
      componentsInChild.volume = volume;
  }

  public static AudioClip GetRandomClipFromDef(SoundDef def)
  {
    if ((Object) def == (Object) null)
      return (AudioClip) null;
    if (def.m_RandomClips == null)
      return (AudioClip) null;
    if (def.m_RandomClips.Count == 0)
      return (AudioClip) null;
    float max = 0.0f;
    using (List<RandomAudioClip>.Enumerator enumerator = def.m_RandomClips.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RandomAudioClip current = enumerator.Current;
        max += current.m_Weight;
      }
    }
    float num1 = Random.Range(0.0f, max);
    float num2 = 0.0f;
    int index1 = def.m_RandomClips.Count - 1;
    for (int index2 = 0; index2 < index1; ++index2)
    {
      RandomAudioClip randomClip = def.m_RandomClips[index2];
      num2 += randomClip.m_Weight;
      if ((double) num1 <= (double) num2)
        return randomClip.m_Clip;
    }
    return def.m_RandomClips[index1].m_Clip;
  }

  public static float GetRandomVolumeFromDef(SoundDef def)
  {
    if ((Object) def == (Object) null)
      return 1f;
    return Random.Range(def.m_RandomVolumeMin, def.m_RandomVolumeMax);
  }

  public static float GetRandomPitchFromDef(SoundDef def)
  {
    if ((Object) def == (Object) null)
      return 1f;
    return Random.Range(def.m_RandomPitchMin, def.m_RandomPitchMax);
  }

  public static void CopyDuckedCategoryDef(SoundDuckedCategoryDef src, SoundDuckedCategoryDef dst)
  {
    dst.m_Category = src.m_Category;
    dst.m_Volume = src.m_Volume;
    dst.m_BeginSec = src.m_BeginSec;
    dst.m_BeginEaseType = src.m_BeginEaseType;
    dst.m_RestoreSec = src.m_RestoreSec;
    dst.m_RestoreEaseType = src.m_RestoreEaseType;
  }

  public static void CopyAudioSource(AudioSource src, AudioSource dst)
  {
    dst.clip = src.clip;
    dst.bypassEffects = src.bypassEffects;
    dst.loop = src.loop;
    dst.priority = src.priority;
    dst.volume = src.volume;
    dst.pitch = src.pitch;
    dst.panStereo = src.panStereo;
    dst.spatialBlend = src.spatialBlend;
    dst.reverbZoneMix = src.reverbZoneMix;
    dst.rolloffMode = src.rolloffMode;
    dst.dopplerLevel = src.dopplerLevel;
    dst.minDistance = src.minDistance;
    dst.maxDistance = src.maxDistance;
    dst.spread = src.spread;
    SoundDef component1 = src.GetComponent<SoundDef>();
    if ((Object) component1 == (Object) null)
    {
      SoundDef component2 = dst.GetComponent<SoundDef>();
      if (!((Object) component2 != (Object) null))
        return;
      Object.DestroyImmediate((Object) component2);
    }
    else
    {
      SoundDef dst1 = dst.GetComponent<SoundDef>();
      if ((Object) dst1 == (Object) null)
        dst1 = dst.gameObject.AddComponent<SoundDef>();
      SoundUtils.CopySoundDef(component1, dst1);
    }
  }

  public static void CopySoundDef(SoundDef src, SoundDef dst)
  {
    dst.m_Category = src.m_Category;
    dst.m_RandomClips = (List<RandomAudioClip>) null;
    if (src.m_RandomClips != null)
    {
      for (int index = 0; index < src.m_RandomClips.Count; ++index)
        dst.m_RandomClips.Add(src.m_RandomClips[index]);
    }
    dst.m_RandomPitchMin = src.m_RandomPitchMin;
    dst.m_RandomPitchMax = src.m_RandomPitchMax;
    dst.m_RandomVolumeMin = src.m_RandomVolumeMin;
    dst.m_RandomVolumeMax = src.m_RandomVolumeMax;
    dst.m_IgnoreDucking = src.m_IgnoreDucking;
  }

  public static bool ChangeAudioSourceSettings(AudioSource source, AudioSourceSettings settings)
  {
    bool flag = false;
    if (source.bypassEffects != settings.m_bypassEffects)
    {
      source.bypassEffects = settings.m_bypassEffects;
      flag = true;
    }
    if (source.loop != settings.m_loop)
    {
      source.loop = settings.m_loop;
      flag = true;
    }
    if (source.priority != settings.m_priority)
    {
      source.priority = settings.m_priority;
      flag = true;
    }
    if (!Mathf.Approximately(source.volume, settings.m_volume))
    {
      source.volume = settings.m_volume;
      flag = true;
    }
    if (!Mathf.Approximately(source.pitch, settings.m_pitch))
    {
      source.pitch = settings.m_pitch;
      flag = true;
    }
    if (!Mathf.Approximately(source.panStereo, settings.m_stereoPan))
    {
      source.panStereo = settings.m_stereoPan;
      flag = true;
    }
    if (!Mathf.Approximately(source.spatialBlend, settings.m_spatialBlend))
    {
      source.spatialBlend = settings.m_spatialBlend;
      flag = true;
    }
    if (!Mathf.Approximately(source.reverbZoneMix, settings.m_reverbZoneMix))
    {
      source.reverbZoneMix = settings.m_reverbZoneMix;
      flag = true;
    }
    if (source.rolloffMode != settings.m_rolloffMode)
    {
      source.rolloffMode = settings.m_rolloffMode;
      flag = true;
    }
    if (!Mathf.Approximately(source.dopplerLevel, settings.m_dopplerLevel))
    {
      source.dopplerLevel = settings.m_dopplerLevel;
      flag = true;
    }
    if (!Mathf.Approximately(source.minDistance, settings.m_minDistance))
    {
      source.minDistance = settings.m_minDistance;
      flag = true;
    }
    if (!Mathf.Approximately(source.maxDistance, settings.m_maxDistance))
    {
      source.maxDistance = settings.m_maxDistance;
      flag = true;
    }
    if (!Mathf.Approximately(source.spread, settings.m_spread))
    {
      source.spread = settings.m_spread;
      flag = true;
    }
    return flag;
  }

  public static bool AddAudioSourceComponents(GameObject go, AudioClip clip = null)
  {
    bool flag = false;
    AudioSource source = go.GetComponent<AudioSource>();
    if ((Object) source == (Object) null)
    {
      source = go.AddComponent<AudioSource>();
      SoundUtils.ChangeAudioSourceSettings(source, new AudioSourceSettings());
      flag = true;
    }
    if ((Object) clip != (Object) null && (Object) clip != (Object) source.clip)
    {
      source.clip = clip;
      flag = true;
    }
    if (source.playOnAwake)
    {
      source.playOnAwake = false;
      flag = true;
    }
    if ((Object) go.GetComponent<SoundDef>() == (Object) null)
    {
      go.AddComponent<SoundDef>();
      flag = true;
    }
    return flag;
  }

  public static bool IsVOFilePath(string path)
  {
    return Path.GetFileName(path).StartsWith("VO_");
  }

  public static bool IsVOFileName(string name)
  {
    return name.StartsWith("VO_");
  }

  public static bool IsVOClip(AudioClip clip)
  {
    if ((Object) clip == (Object) null)
      return false;
    return SoundUtils.IsVOFileName(clip.name);
  }

  public static bool IsVOClip(RandomAudioClip randomClip)
  {
    if (randomClip == null)
      return false;
    return SoundUtils.IsVOClip(randomClip.m_Clip);
  }
}
