﻿// Decompiled with JetBrains decompiler
// Type: MusicStingerSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class MusicStingerSpell : CardSoundSpell
{
  public MusicStingerData m_MusicStingerData = new MusicStingerData();

  private bool CanPlay()
  {
    if (GameState.Get() == null)
      return true;
    Player controller = this.GetSourceCard().GetController();
    if (controller == null)
      return true;
    return controller.IsLocalUser();
  }
}
