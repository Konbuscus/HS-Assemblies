﻿// Decompiled with JetBrains decompiler
// Type: DeckRule_DefaultType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class DeckRule_DefaultType : DeckRule
{
  public DeckRule_DefaultType(string ruleType, DeckRulesetRuleDbfRecord record)
    : base(DeckRule.RuleType.UNKNOWN, record)
  {
    Log.DeckRuleset.Print("DeckRule_DefaultType created for ruleType={0} ruleId={1} deckRulesetId={2}", new object[3]
    {
      (object) ruleType,
      (object) record.ID,
      (object) record.DeckRulesetId
    });
  }

  public override bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason)
  {
    return this.DefaultYes(out reason);
  }
}
