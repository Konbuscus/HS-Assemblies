﻿// Decompiled with JetBrains decompiler
// Type: DbfXml
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Xml;
using UnityEngine;

public class DbfXml
{
  public static bool Load(string xmlFile, IDbf dbf)
  {
    if (!File.Exists(xmlFile))
      return false;
    using (XmlReader xmlReader = XmlReader.Create(xmlFile))
    {
      while (xmlReader.Read())
      {
        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Record")
          DbfXml.LoadRecord(xmlReader.ReadSubtree(), dbf, false);
      }
      return true;
    }
  }

  public static void LoadRecord(XmlReader reader, IDbf dbf, bool hideDbfLocDebugInfo = false)
  {
    DbfRecord newRecord = dbf.CreateNewRecord();
    while (reader.Read())
    {
      if (reader.NodeType == XmlNodeType.Element && !(reader.Name != "Field") && !reader.IsEmptyElement)
      {
        string varName = reader["column"];
        System.Type varType = newRecord.GetVarType(varName);
        if (varType != null)
        {
          if (varType == typeof (DbfLocValue))
            newRecord.SetVar(varName, (object) DbfXml.LoadLocalizedString(reader["loc_ID"], reader.ReadSubtree(), hideDbfLocDebugInfo));
          else if (varType == typeof (bool))
          {
            string strVal = reader.ReadElementContentAsString();
            newRecord.SetVar(varName, (object) GeneralUtils.ForceBool(strVal));
          }
          else if (varType == typeof (ulong))
            newRecord.SetVar(varName, (object) ulong.Parse(reader.ReadElementContentAsString()));
          else
            newRecord.SetVar(varName, reader.ReadElementContentAs(varType, (IXmlNamespaceResolver) null));
        }
        else
          Debug.LogErrorFormat("Type is not defined for column {0}, dbf={1}. Try \"Build->Generate DBFs and Code\"", new object[2]
          {
            (object) varName,
            (object) newRecord.GetType().Name
          });
      }
    }
    dbf.AddRecord(newRecord);
  }

  public static DbfLocValue LoadLocalizedString(string locIdStr, XmlReader reader, bool hideDebugInfo = false)
  {
    reader.Read();
    DbfLocValue dbfLocValue = new DbfLocValue(hideDebugInfo);
    if (!string.IsNullOrEmpty(locIdStr))
    {
      int result = 0;
      if (int.TryParse(locIdStr, out result))
        dbfLocValue.SetLocId(result);
    }
    while (reader.Read())
    {
      if (reader.NodeType == XmlNodeType.Element)
      {
        string name = reader.Name;
        string text = reader.ReadElementContentAsString();
        Locale loc;
        try
        {
          loc = EnumUtils.GetEnum<Locale>(name);
        }
        catch (ArgumentException ex)
        {
          continue;
        }
        dbfLocValue.SetString(loc, TextUtils.DecodeWhitespaces(text));
      }
    }
    return dbfLocValue;
  }
}
