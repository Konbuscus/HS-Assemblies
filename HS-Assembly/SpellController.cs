﻿// Decompiled with JetBrains decompiler
// Type: SpellController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class SpellController : MonoBehaviour
{
  private List<SpellController.FinishedTaskListCallback> m_finishedTaskListListeners = new List<SpellController.FinishedTaskListCallback>();
  private List<SpellController.FinishedCallback> m_finishedListeners = new List<SpellController.FinishedCallback>();
  protected List<Card> m_targets = new List<Card>();
  public const float FINISH_FUDGE_SEC = 10f;
  protected Card m_source;
  protected PowerTaskList m_taskList;
  protected int m_taskListId;
  protected bool m_processingTaskList;
  protected bool m_pendingFinish;

  public Card GetSource()
  {
    return this.m_source;
  }

  public void SetSource(Card card)
  {
    this.m_source = card;
  }

  public bool IsSource(Card card)
  {
    return (Object) this.m_source == (Object) card;
  }

  public void RemoveSource()
  {
    this.m_source = (Card) null;
  }

  public List<Card> GetTargets()
  {
    return this.m_targets;
  }

  public Card GetTarget()
  {
    if (this.m_targets.Count == 0)
      return (Card) null;
    return this.m_targets[0];
  }

  public void AddTarget(Card card)
  {
    this.m_targets.Add(card);
  }

  public void RemoveTarget(Card card)
  {
    this.m_targets.Remove(card);
  }

  public void RemoveAllTargets()
  {
    this.m_targets.Clear();
  }

  public bool IsTarget(Card card)
  {
    using (List<Card>.Enumerator enumerator = this.m_targets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if ((Object) enumerator.Current == (Object) card)
          return true;
      }
    }
    return false;
  }

  public void AddFinishedTaskListCallback(SpellController.FinishedTaskListCallback callback)
  {
    if (this.m_finishedTaskListListeners.Contains(callback))
      return;
    this.m_finishedTaskListListeners.Add(callback);
  }

  public void AddFinishedCallback(SpellController.FinishedCallback callback)
  {
    if (this.m_finishedListeners.Contains(callback))
      return;
    this.m_finishedListeners.Add(callback);
  }

  public bool IsProcessingTaskList()
  {
    return this.m_processingTaskList;
  }

  public PowerTaskList GetPowerTaskList()
  {
    return this.m_taskList;
  }

  public bool AttachPowerTaskList(PowerTaskList taskList)
  {
    if (this.m_taskList != taskList)
    {
      this.DetachPowerTaskList();
      this.m_taskList = taskList;
    }
    this.m_taskListId = this.m_taskList.GetId();
    return this.AddPowerSourceAndTargets(taskList);
  }

  public void SetPowerTaskList(PowerTaskList taskList)
  {
    if (this.m_taskList == taskList)
      return;
    this.DetachPowerTaskList();
    this.m_taskList = taskList;
  }

  public PowerTaskList DetachPowerTaskList()
  {
    PowerTaskList taskList = this.m_taskList;
    this.RemoveSource();
    this.RemoveAllTargets();
    this.m_taskList = (PowerTaskList) null;
    return taskList;
  }

  public void DoPowerTaskList()
  {
    this.m_processingTaskList = true;
    this.gameObject.SetActive(true);
    GameState.Get().AddServerBlockingSpellController(this);
    this.StartCoroutine(this.WaitForCardsThenDoTaskList());
  }

  public void ForceKill()
  {
    this.OnFinishedTaskList();
  }

  protected virtual void OnProcessTaskList()
  {
    this.OnFinishedTaskList();
    this.OnFinished();
  }

  protected virtual void OnFinishedTaskList()
  {
    GameState.Get().RemoveServerBlockingSpellController(this);
    this.m_processingTaskList = false;
    this.FireFinishedTaskListCallbacks();
    if (!this.m_pendingFinish)
      return;
    this.m_pendingFinish = false;
    this.OnFinished();
  }

  protected virtual void OnFinished()
  {
    if (this.m_processingTaskList)
    {
      this.m_pendingFinish = true;
    }
    else
    {
      this.gameObject.SetActive(false);
      this.FireFinishedCallbacks();
    }
  }

  protected virtual bool AddPowerSourceAndTargets(PowerTaskList taskList)
  {
    if (!this.HasSourceCard(taskList) || !SpellUtils.CanAddPowerTargets(taskList))
      return false;
    Card card = taskList.GetSourceEntity().GetCard();
    this.SetSource(card);
    List<PowerTask> taskList1 = this.m_taskList.GetTaskList();
    for (int index = 0; index < taskList1.Count; ++index)
    {
      Card cardFromPowerTask = this.GetTargetCardFromPowerTask(taskList1[index]);
      if (!((Object) cardFromPowerTask == (Object) null) && !((Object) card == (Object) cardFromPowerTask) && !this.IsTarget(cardFromPowerTask))
        this.AddTarget(cardFromPowerTask);
    }
    return (Object) card != (Object) null || this.m_targets.Count > 0;
  }

  protected virtual bool HasSourceCard(PowerTaskList taskList)
  {
    Entity sourceEntity = taskList.GetSourceEntity();
    return sourceEntity != null && !((Object) sourceEntity.GetCard() == (Object) null);
  }

  [DebuggerHidden]
  private IEnumerator WaitForCardsThenDoTaskList()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpellController.\u003CWaitForCardsThenDoTaskList\u003Ec__Iterator247() { \u003C\u003Ef__this = this };
  }

  protected bool IsCardBusy(Card card)
  {
    Entity entity = card.GetEntity();
    return !this.WillEntityLoadCard(entity) && (entity.IsLoadingAssets() || (!(bool) ((Object) TurnStartManager.Get()) || !TurnStartManager.Get().IsCardDrawHandled(card)) && !card.IsActorReady());
  }

  private bool WillEntityLoadCard(Entity entity)
  {
    int entityId = entity.GetEntityId();
    using (List<PowerTask>.Enumerator enumerator = this.m_taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.PowerHistory power = enumerator.Current.GetPower();
        switch (power.Type)
        {
          case Network.PowerType.FULL_ENTITY:
            Network.HistFullEntity histFullEntity = power as Network.HistFullEntity;
            if (entityId == histFullEntity.Entity.ID)
              return true;
            continue;
          case Network.PowerType.SHOW_ENTITY:
            Network.HistShowEntity histShowEntity = power as Network.HistShowEntity;
            if (entityId == histShowEntity.Entity.ID)
              return true;
            continue;
          default:
            continue;
        }
      }
    }
    return false;
  }

  private void FireFinishedTaskListCallbacks()
  {
    SpellController.FinishedTaskListCallback[] array = this.m_finishedTaskListListeners.ToArray();
    this.m_finishedTaskListListeners.Clear();
    for (int index = 0; index < array.Length; ++index)
      array[index](this);
  }

  private void FireFinishedCallbacks()
  {
    SpellController.FinishedCallback[] array = this.m_finishedListeners.ToArray();
    this.m_finishedListeners.Clear();
    for (int index = 0; index < array.Length; ++index)
      array[index](this);
  }

  protected Card GetTargetCardFromPowerTask(PowerTask task)
  {
    Network.PowerHistory power = task.GetPower();
    if (power.Type != Network.PowerType.TAG_CHANGE)
      return (Card) null;
    Network.HistTagChange histTagChange = power as Network.HistTagChange;
    Entity entity = GameState.Get().GetEntity(histTagChange.Entity);
    if (entity != null)
      return entity.GetCard();
    UnityEngine.Debug.LogWarning((object) string.Format("{0}.GetTargetCardFromPowerTask() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) histTagChange.Entity));
    return (Card) null;
  }

  public delegate void FinishedTaskListCallback(SpellController spellController);

  public delegate void FinishedCallback(SpellController spellController);
}
