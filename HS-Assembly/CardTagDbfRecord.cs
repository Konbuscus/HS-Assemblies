﻿// Decompiled with JetBrains decompiler
// Type: CardTagDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CardTagDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_CardId;
  [SerializeField]
  private int m_TagId;
  [SerializeField]
  private int m_TagValue;
  [SerializeField]
  private bool m_IsReferenceTag;
  [SerializeField]
  private bool m_IsPowerKeywordTag;

  [DbfField("CARD_ID", "which CARD does this tag belong to")]
  public int CardId
  {
    get
    {
      return this.m_CardId;
    }
  }

  [DbfField("TAG_ID", "this tag's id from TAG.dbi")]
  public int TagId
  {
    get
    {
      return this.m_TagId;
    }
  }

  [DbfField("TAG_VALUE", "numeric value for this tag")]
  public int TagValue
  {
    get
    {
      return this.m_TagValue;
    }
  }

  [DbfField("IS_REFERENCE_TAG", "Is this a reference tag?")]
  public bool IsReferenceTag
  {
    get
    {
      return this.m_IsReferenceTag;
    }
  }

  [DbfField("IS_POWER_KEYWORD_TAG", "Is this A keyword tag from card's powers?")]
  public bool IsPowerKeywordTag
  {
    get
    {
      return this.m_IsPowerKeywordTag;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    CardTagDbfAsset cardTagDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (CardTagDbfAsset)) as CardTagDbfAsset;
    if ((UnityEngine.Object) cardTagDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("CardTagDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < cardTagDbfAsset.Records.Count; ++index)
      cardTagDbfAsset.Records[index].StripUnusedLocales();
    records = (object) cardTagDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetCardId(int v)
  {
    this.m_CardId = v;
  }

  public void SetTagId(int v)
  {
    this.m_TagId = v;
  }

  public void SetTagValue(int v)
  {
    this.m_TagValue = v;
  }

  public void SetIsReferenceTag(bool v)
  {
    this.m_IsReferenceTag = v;
  }

  public void SetIsPowerKeywordTag(bool v)
  {
    this.m_IsPowerKeywordTag = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CardTagDbfRecord.\u003C\u003Ef__switch\u0024map26 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CardTagDbfRecord.\u003C\u003Ef__switch\u0024map26 = new Dictionary<string, int>(5)
        {
          {
            "CARD_ID",
            0
          },
          {
            "TAG_ID",
            1
          },
          {
            "TAG_VALUE",
            2
          },
          {
            "IS_REFERENCE_TAG",
            3
          },
          {
            "IS_POWER_KEYWORD_TAG",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CardTagDbfRecord.\u003C\u003Ef__switch\u0024map26.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.CardId;
          case 1:
            return (object) this.TagId;
          case 2:
            return (object) this.TagValue;
          case 3:
            return (object) this.IsReferenceTag;
          case 4:
            return (object) this.IsPowerKeywordTag;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (CardTagDbfRecord.\u003C\u003Ef__switch\u0024map27 == null)
    {
      // ISSUE: reference to a compiler-generated field
      CardTagDbfRecord.\u003C\u003Ef__switch\u0024map27 = new Dictionary<string, int>(5)
      {
        {
          "CARD_ID",
          0
        },
        {
          "TAG_ID",
          1
        },
        {
          "TAG_VALUE",
          2
        },
        {
          "IS_REFERENCE_TAG",
          3
        },
        {
          "IS_POWER_KEYWORD_TAG",
          4
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!CardTagDbfRecord.\u003C\u003Ef__switch\u0024map27.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetCardId((int) val);
        break;
      case 1:
        this.SetTagId((int) val);
        break;
      case 2:
        this.SetTagValue((int) val);
        break;
      case 3:
        this.SetIsReferenceTag((bool) val);
        break;
      case 4:
        this.SetIsPowerKeywordTag((bool) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CardTagDbfRecord.\u003C\u003Ef__switch\u0024map28 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CardTagDbfRecord.\u003C\u003Ef__switch\u0024map28 = new Dictionary<string, int>(5)
        {
          {
            "CARD_ID",
            0
          },
          {
            "TAG_ID",
            1
          },
          {
            "TAG_VALUE",
            2
          },
          {
            "IS_REFERENCE_TAG",
            3
          },
          {
            "IS_POWER_KEYWORD_TAG",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CardTagDbfRecord.\u003C\u003Ef__switch\u0024map28.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (int);
          case 3:
            return typeof (bool);
          case 4:
            return typeof (bool);
        }
      }
    }
    return (System.Type) null;
  }
}
