﻿// Decompiled with JetBrains decompiler
// Type: HistoryInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class HistoryInfo
{
  public HistoryInfoType m_infoType;
  public int m_damageChangeAmount;
  public int m_armorChangeAmount;
  private Entity m_originalEntity;
  private Entity m_duplicatedEntity;
  private bool m_died;
  private bool m_cachedBonuses;

  public int GetSplatAmount()
  {
    return this.m_damageChangeAmount + this.m_armorChangeAmount;
  }

  public Entity GetDuplicatedEntity()
  {
    return this.m_duplicatedEntity;
  }

  public Entity GetOriginalEntity()
  {
    return this.m_originalEntity;
  }

  public void SetOriginalEntity(Entity entity)
  {
    this.m_originalEntity = entity;
    this.DuplicateEntity(false);
  }

  public bool HasDied()
  {
    return this.m_died;
  }

  public void SetDied(bool set)
  {
    this.m_died = set;
  }

  public bool CanDuplicateEntity(bool duplicateHiddenNonSecret)
  {
    return this.m_originalEntity != null && this.m_originalEntity.GetLoadState() == Entity.LoadState.DONE && (!this.m_originalEntity.IsHidden() || GameUtils.IsEntityHiddenAfterCurrentTasklist(this.m_originalEntity) && (this.m_originalEntity.IsSecret() || duplicateHiddenNonSecret));
  }

  public void DuplicateEntity(bool duplicateHiddenNonSecret)
  {
    if (this.m_duplicatedEntity != null || !this.CanDuplicateEntity(duplicateHiddenNonSecret))
      return;
    this.m_duplicatedEntity = this.m_originalEntity.CloneForHistory(this);
    if (this.m_infoType != HistoryInfoType.CARD_PLAYED && this.m_infoType != HistoryInfoType.WEAPON_PLAYED)
      return;
    this.m_duplicatedEntity.SetTag(GAME_TAG.COST, this.m_originalEntity.GetTag(GAME_TAG.TAG_LAST_KNOWN_COST_IN_HAND));
  }
}
