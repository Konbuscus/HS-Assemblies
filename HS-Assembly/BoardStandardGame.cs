﻿// Decompiled with JetBrains decompiler
// Type: BoardStandardGame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoardStandardGame : MonoBehaviour
{
  public Transform m_BoneParent;
  public Transform m_ColliderParent;
  public GameObject[] m_DeckGameObjects;
  private static BoardStandardGame s_instance;

  private void Awake()
  {
    BoardStandardGame.s_instance = this;
    if (!((Object) LoadingScreen.Get() != (Object) null))
      return;
    LoadingScreen.Get().NotifyMainSceneObjectAwoke(this.gameObject);
  }

  private void Start()
  {
    this.DeckColors();
  }

  private void OnDestroy()
  {
    BoardStandardGame.s_instance = (BoardStandardGame) null;
  }

  public static BoardStandardGame Get()
  {
    return BoardStandardGame.s_instance;
  }

  public Transform FindBone(string name)
  {
    return this.m_BoneParent.Find(name);
  }

  public Collider FindCollider(string name)
  {
    Transform transform = this.m_ColliderParent.Find(name);
    if ((Object) transform == (Object) null)
      return (Collider) null;
    return transform.GetComponent<Collider>();
  }

  public void DeckColors()
  {
    foreach (GameObject deckGameObject in this.m_DeckGameObjects)
      deckGameObject.GetComponent<Renderer>().material.color = Board.Get().m_DeckColor;
  }
}
