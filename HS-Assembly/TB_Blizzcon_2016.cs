﻿// Decompiled with JetBrains decompiler
// Type: TB_Blizzcon_2016
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TB_Blizzcon_2016 : MissionEntity
{
  private int emoteTurnsLimit = 7;
  private TB_Blizzcon_2016.MATCHUP currentMatchup = TB_Blizzcon_2016.MATCHUP.ERROR;
  private TAG_CLASS lotusHero = TAG_CLASS.DRUID;
  private TAG_CLASS kabalHero = TAG_CLASS.PRIEST;
  private TAG_CLASS goonsHero = TAG_CLASS.PALADIN;
  private string grimyGoonsName = GameStrings.Get("GLOBAL_KEYWORD_GRIMY_GOONS");
  private string jadeLotusName = GameStrings.Get("GLOBAL_KEYWORD_JADE_LOTUS");
  private string kabalName = GameStrings.Get("GLOBAL_KEYWORD_KABAL");
  private Card m_bossCard;
  private Card m_mediva;
  private bool[] hasUsedLine;
  private int currentTurnsWOEmote;
  private bool emoteThisTurn;
  private List<int> priorityLines;
  private bool hasPlayedMatchupTriggerGoons;
  private bool hasPlayedMatchupTriggerLotus;
  private bool hasPlayedMatchupTriggerKabal;
  private TB_Blizzcon_2016.VICTOR matchResult;
  private TAG_CLASS firstPlayerHero;
  private TAG_CLASS secondPlayerHero;

  public override void NotifyOfGameOver(TAG_PLAYSTATE gameResult)
  {
    if (this.currentMatchup == TB_Blizzcon_2016.MATCHUP.ERROR)
      this.currentMatchup = this.GetBrawlHeroes();
    switch (gameResult)
    {
      case TAG_PLAYSTATE.WON:
        if (this.firstPlayerHero == this.goonsHero && this.secondPlayerHero == this.lotusHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.GOONSBEATLOTUS;
          break;
        }
        if (this.firstPlayerHero == this.goonsHero && this.secondPlayerHero == this.kabalHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.GOONSBEATKABAL;
          break;
        }
        if (this.firstPlayerHero == this.lotusHero && this.secondPlayerHero == this.kabalHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.LOTUSBEATKABAL;
          break;
        }
        if (this.firstPlayerHero == this.lotusHero && this.secondPlayerHero == this.goonsHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.LOTUSBEATGOONS;
          break;
        }
        if (this.firstPlayerHero == this.kabalHero && this.secondPlayerHero == this.lotusHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.KABALBEATLOTUS;
          break;
        }
        if (this.firstPlayerHero == this.kabalHero && this.secondPlayerHero == this.goonsHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.KABALBEATGOONS;
          break;
        }
        break;
      case TAG_PLAYSTATE.LOST:
        if (this.firstPlayerHero == this.goonsHero && this.secondPlayerHero == this.lotusHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.LOTUSBEATGOONS;
          break;
        }
        if (this.firstPlayerHero == this.goonsHero && this.secondPlayerHero == this.kabalHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.KABALBEATGOONS;
          break;
        }
        if (this.firstPlayerHero == this.lotusHero && this.secondPlayerHero == this.kabalHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.KABALBEATLOTUS;
          break;
        }
        if (this.firstPlayerHero == this.lotusHero && this.secondPlayerHero == this.goonsHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.GOONSBEATLOTUS;
          break;
        }
        if (this.firstPlayerHero == this.kabalHero && this.secondPlayerHero == this.lotusHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.LOTUSBEATKABAL;
          break;
        }
        if (this.firstPlayerHero == this.kabalHero && this.secondPlayerHero == this.goonsHero)
        {
          this.matchResult = TB_Blizzcon_2016.VICTOR.GOONSBEATKABAL;
          break;
        }
        break;
      case TAG_PLAYSTATE.TIED:
        this.matchResult = TB_Blizzcon_2016.VICTOR.ERROR;
        break;
    }
    base.NotifyOfGameOver(gameResult);
  }

  private TB_Blizzcon_2016.MATCHUP GetBrawlHeroes()
  {
    this.firstPlayerHero = GameState.Get().GetFriendlySidePlayer().GetHero().GetClass();
    this.secondPlayerHero = GameState.Get().GetOpposingSidePlayer().GetHero().GetClass();
    if (this.firstPlayerHero == this.goonsHero && this.secondPlayerHero == this.lotusHero || this.firstPlayerHero == this.lotusHero && this.secondPlayerHero == this.goonsHero)
      return TB_Blizzcon_2016.MATCHUP.GOONSVLOTUS;
    if (this.firstPlayerHero == this.goonsHero && this.secondPlayerHero == this.kabalHero || this.firstPlayerHero == this.kabalHero && this.secondPlayerHero == this.goonsHero)
      return TB_Blizzcon_2016.MATCHUP.KABALVGOONS;
    if (this.firstPlayerHero == this.kabalHero && this.secondPlayerHero == this.lotusHero || this.firstPlayerHero == this.lotusHero && this.secondPlayerHero == this.kabalHero)
      return TB_Blizzcon_2016.MATCHUP.KABALVLOTUS;
    UnityEngine.Debug.LogError((object) "Matchup is not as predicted. Should be only one of each hero as defined in TB_Blizzcon_2016.cs");
    return TB_Blizzcon_2016.MATCHUP.ERROR;
  }

  public override AudioSource GetAnnouncerLine(Card heroCard, Card.AnnouncerLineType type)
  {
    int num = Random.Range(0, 3);
    if (heroCard.GetEntity().GetClass() == this.lotusHero)
    {
      switch (num)
      {
        case 0:
          return this.GetPreloadedSound("VO_INKEEPER_Male_Dwarf_JadeLotus_Intro_01");
        case 1:
          return this.GetPreloadedSound("VO_INKEEPER_Male_Dwarf_JadeLotus_Intro_02");
        case 2:
          return this.GetPreloadedSound("VO_INKEEPER_Male_Dwarf_JadeLotus_Intro_03");
      }
    }
    else if (heroCard.GetEntity().GetClass() == this.goonsHero)
    {
      switch (num)
      {
        case 0:
          return this.GetPreloadedSound("VO_INKEEPER_Male_Dwarf_GrimyGoons_Intro_01");
        case 1:
          return this.GetPreloadedSound("VO_INKEEPER_Male_Dwarf_GrimyGoons_Intro_02");
        case 2:
          return this.GetPreloadedSound("VO_INKEEPER_Male_Dwarf_GrimyGoons_Intro_03");
      }
    }
    else if (heroCard.GetEntity().GetClass() == this.kabalHero)
    {
      switch (num)
      {
        case 0:
          return this.GetPreloadedSound("VO_INKEEPER_Male_Dwarf_TheKabal_Intro_01");
        case 1:
          return this.GetPreloadedSound("VO_INKEEPER_Male_Dwarf_TheKabal_Intro_02");
        case 2:
          return this.GetPreloadedSound("VO_INKEEPER_Male_Dwarf_TheKabal_Intro_03");
      }
    }
    return base.GetAnnouncerLine(heroCard, type);
  }

  public override void PreloadAssets()
  {
    this.hasUsedLine = new bool[1000];
    this.SetupPriorityLines();
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_1_1st_Turn_Start_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_1_2nd_Turn_Start_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_1_Fill1_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_1_Goons_Win_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_1_Goons_Winning_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_1_Kabal_Win_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_1_Kabal_Winning_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_1st_Turn_Start_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Fill1_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Fill2_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Kabal_Winning_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Kabal_Winning_02");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Lotus_Winning_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Lotus_Wins_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Kabal_Wins_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Potion1_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_0_Potion_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_0_Potion_02");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_0_Potion_04");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_0_Potion_05");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Jade_Golem3_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_3_Jade_Golem2_01");
    this.PreloadSound("VO_BOSS_KAZAKUS_Male_Troll_BC_1_Arms_Dealing1_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_2_1st_Turn_Start_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_2_2nd_Turn_Start_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_2_Fill1_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_2_Goons_Win_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_2_Goons_Winning_02");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_2_Jade_Win_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_2_Lotus_Winning_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_1st_Turn_Start_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_1st_Turn_Start_02");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_2nd_Turn_Start_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_Fill1_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_Fill2_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_Kabal_Winning_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_Kabal_Wins_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_Lotus_Winning_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_Lotus_Wins_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_Potion1_02");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_0_Jade_Golem_02");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_0_Jade_Golem_03");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_0_Jade_Golem_04");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_3_Jade_Golem3_01");
    this.PreloadSound("VO_BOSS_AYA_Female_Pandaren_BC_2_Arms_Dealing1_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_1_1st_Turn_Start_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_1_2nd_Turn_Start_02");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_1_Fill1_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_1_Fill2_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_1_Goons_Win_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_1_Goons_Winning_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_1_Kabal_Win_02");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_1_Kabal_Winning_02");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_2_1st_Turn_Start_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_2_1st_Turn_Start_04");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_2_Fill1_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_2_Goons_Win_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_2_Goons_Winning_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_2_Lotus_Win_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_2_Lotus_Winning_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_1_Potion2_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_2_Jade_Golem2_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_0_Arms_Dealing_01");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_0_Arms_Dealing_02");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_0_Arms_Dealing_03");
    this.PreloadSound("VO_BOSS_HAN_Male_Ogre_BC_0_Arms_Dealing_04");
    this.PreloadSound("VO_BOSS_CHO_Male_Ogre_BC_1_1st_Turn_Start_01");
    this.PreloadSound("VO_BOSS_CHO_Male_Ogre_BC_1_Fill2_01");
    this.PreloadSound("VO_BOSS_CHO_Male_Ogre_BC_2_Fill1_01");
    this.PreloadSound("VO_INKEEPER_Male_Dwarf_GrimyGoons_Intro_01");
    this.PreloadSound("VO_INKEEPER_Male_Dwarf_GrimyGoons_Intro_02");
    this.PreloadSound("VO_INKEEPER_Male_Dwarf_GrimyGoons_Intro_03");
    this.PreloadSound("VO_INKEEPER_Male_Dwarf_JadeLotus_Intro_01");
    this.PreloadSound("VO_INKEEPER_Male_Dwarf_JadeLotus_Intro_02");
    this.PreloadSound("VO_INKEEPER_Male_Dwarf_JadeLotus_Intro_03");
    this.PreloadSound("VO_INKEEPER_Male_Dwarf_TheKabal_Intro_01");
    this.PreloadSound("VO_INKEEPER_Male_Dwarf_TheKabal_Intro_02");
    this.PreloadSound("VO_INKEEPER_Male_Dwarf_TheKabal_Intro_03");
  }

  private void SetupPriorityLines()
  {
    this.priorityLines = new List<int>();
    this.priorityLines.Add(0);
    this.priorityLines.Add(102);
    this.priorityLines.Add(103);
    this.priorityLines.Add(202);
    this.priorityLines.Add(203);
    this.priorityLines.Add(302);
    this.priorityLines.Add(303);
  }

  private Vector3 GetPositionForBoss(TB_Blizzcon_2016.BOSS boss)
  {
    this.firstPlayerHero = GameState.Get().GetFriendlySidePlayer().GetHero().GetClass();
    switch (boss)
    {
      case TB_Blizzcon_2016.BOSS.HAN:
      case TB_Blizzcon_2016.BOSS.CHO:
        if (this.firstPlayerHero == this.goonsHero)
          return NotificationManager.LEFT_OF_FRIENDLY_HERO;
        return NotificationManager.RIGHT_OF_ENEMY_HERO;
      case TB_Blizzcon_2016.BOSS.AYA:
        if (this.firstPlayerHero == this.lotusHero)
          return NotificationManager.LEFT_OF_FRIENDLY_HERO;
        return NotificationManager.RIGHT_OF_ENEMY_HERO;
      case TB_Blizzcon_2016.BOSS.KAZAKUS:
        if (this.firstPlayerHero == this.kabalHero)
          return NotificationManager.LEFT_OF_FRIENDLY_HERO;
        return NotificationManager.RIGHT_OF_ENEMY_HERO;
      default:
        return NotificationManager.DEFAULT_CHARACTER_POS;
    }
  }

  private Notification.SpeechBubbleDirection GetBubbleDirectionForBoss(TB_Blizzcon_2016.BOSS boss)
  {
    this.firstPlayerHero = GameState.Get().GetFriendlySidePlayer().GetHero().GetClass();
    switch (boss)
    {
      case TB_Blizzcon_2016.BOSS.HAN:
        return this.firstPlayerHero == this.goonsHero ? Notification.SpeechBubbleDirection.BottomRight : Notification.SpeechBubbleDirection.TopRight;
      case TB_Blizzcon_2016.BOSS.CHO:
        return this.firstPlayerHero == this.goonsHero ? Notification.SpeechBubbleDirection.BottomLeft : Notification.SpeechBubbleDirection.TopLeft;
      case TB_Blizzcon_2016.BOSS.AYA:
        return this.firstPlayerHero == this.lotusHero ? Notification.SpeechBubbleDirection.BottomLeft : Notification.SpeechBubbleDirection.TopLeft;
      case TB_Blizzcon_2016.BOSS.KAZAKUS:
        return this.firstPlayerHero == this.kabalHero ? Notification.SpeechBubbleDirection.BottomLeft : Notification.SpeechBubbleDirection.TopLeft;
      default:
        return Notification.SpeechBubbleDirection.BottomLeft;
    }
  }

  [DebuggerHidden]
  private IEnumerator PlayBossLine(TB_Blizzcon_2016.BOSS boss, string line, bool persistCharacter = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB_Blizzcon_2016.\u003CPlayBossLine\u003Ec__Iterator1FC() { boss = boss, line = line, persistCharacter = persistCharacter, \u003C\u0024\u003Eboss = boss, \u003C\u0024\u003Eline = line, \u003C\u0024\u003EpersistCharacter = persistCharacter, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB_Blizzcon_2016.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1FD() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  private void SetNamePlate()
  {
    GameState.Get().GetFriendlySidePlayer();
    GameState.Get().GetOpposingSidePlayer();
    TAG_CLASS tagClass1 = GameState.Get().GetFriendlySidePlayer().GetHero().GetClass();
    TAG_CLASS tagClass2 = GameState.Get().GetOpposingSidePlayer().GetHero().GetClass();
    if (tagClass1 == TAG_CLASS.PALADIN)
      Gameplay.Get().GetNameBannerForSide(Player.Side.FRIENDLY).SetName(this.grimyGoonsName);
    else if (tagClass1 == TAG_CLASS.PRIEST)
      Gameplay.Get().GetNameBannerForSide(Player.Side.FRIENDLY).SetName(this.kabalName);
    else if (tagClass1 == TAG_CLASS.DRUID)
      Gameplay.Get().GetNameBannerForSide(Player.Side.FRIENDLY).SetName(this.jadeLotusName);
    else
      UnityEngine.Debug.Log((object) "Incorrect class found in SetNamePlate()");
    if (tagClass2 == TAG_CLASS.PALADIN)
      Gameplay.Get().GetNameBannerForSide(Player.Side.OPPOSING).SetName(this.grimyGoonsName);
    else if (tagClass2 == TAG_CLASS.PRIEST)
      Gameplay.Get().GetNameBannerForSide(Player.Side.OPPOSING).SetName(this.kabalName);
    else if (tagClass2 == TAG_CLASS.DRUID)
      Gameplay.Get().GetNameBannerForSide(Player.Side.OPPOSING).SetName(this.jadeLotusName);
    else
      UnityEngine.Debug.Log((object) "Incorrect class found in SetNamePlate()");
  }

  [DebuggerHidden]
  private IEnumerator PlayFillLine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB_Blizzcon_2016.\u003CPlayFillLine\u003Ec__Iterator1FE() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB_Blizzcon_2016.\u003CHandleGameOverWithTiming\u003Ec__Iterator1FF() { \u003C\u003Ef__this = this };
  }

  private enum MATCHUP
  {
    KABALVLOTUS,
    KABALVGOONS,
    GOONSVLOTUS,
    ERROR,
  }

  private enum BOSS
  {
    HAN,
    CHO,
    AYA,
    KAZAKUS,
  }

  private enum VICTOR
  {
    GOONSBEATKABAL,
    GOONSBEATLOTUS,
    LOTUSBEATKABAL,
    LOTUSBEATGOONS,
    KABALBEATGOONS,
    KABALBEATLOTUS,
    ERROR,
  }
}
