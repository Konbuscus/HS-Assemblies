﻿// Decompiled with JetBrains decompiler
// Type: DisableMesh_ColorAlpha
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DisableMesh_ColorAlpha : MonoBehaviour
{
  private Material m_material;

  private void Start()
  {
    this.m_material = this.GetComponent<Renderer>().material;
    if ((Object) this.m_material == (Object) null)
      this.enabled = false;
    if (this.m_material.HasProperty("_Color"))
      return;
    this.enabled = false;
  }

  private void Update()
  {
    if ((double) this.m_material.color.a == 0.0)
      this.GetComponent<Renderer>().enabled = false;
    else
      this.GetComponent<Renderer>().enabled = true;
  }
}
