﻿// Decompiled with JetBrains decompiler
// Type: ActivateZoneDeck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ActivateZoneDeck : MonoBehaviour
{
  private bool onoff = true;
  public bool m_friendlyDeck;

  public void ToggleActive()
  {
    if (GameState.Get() == null || GameState.Get().GetFriendlySidePlayer() == null || GameState.Get().GetOpposingSidePlayer() == null)
    {
      Debug.LogError((object) "ActivateZoneDeck - Game State not yet initialized.");
    }
    else
    {
      ZoneDeck zoneDeck = !this.m_friendlyDeck ? GameState.Get().GetOpposingSidePlayer().GetDeckZone() : GameState.Get().GetFriendlySidePlayer().GetDeckZone();
      if ((Object) zoneDeck == (Object) null)
        Debug.LogError((object) "ActivateZoneDeck - zoneDeck is null!");
      else
        zoneDeck.SetVisibility(this.onoff);
    }
  }
}
