﻿// Decompiled with JetBrains decompiler
// Type: SuperSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using HutongGames.PlayMaker;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class SuperSpell : Spell
{
  public bool m_MakeClones = true;
  public SpellTargetInfo m_TargetInfo = new SpellTargetInfo();
  protected List<GameObject> m_visualTargets = new List<GameObject>();
  protected Map<int, int> m_visualToTargetIndexMap = new Map<int, int>();
  protected Map<int, int> m_targetToMetaDataMap = new Map<int, int>();
  public SpellStartInfo m_StartInfo;
  public SpellActionInfo m_ActionInfo;
  public SpellMissileInfo m_MissileInfo;
  public SpellImpactInfo m_ImpactInfo;
  public SpellAreaEffectInfo m_FriendlyAreaEffectInfo;
  public SpellAreaEffectInfo m_OpponentAreaEffectInfo;
  public SpellChainInfo m_ChainInfo;
  protected Spell m_startSpell;
  protected int m_currentTargetIndex;
  protected int m_effectsPendingFinish;
  protected bool m_pendingNoneStateChange;
  protected bool m_pendingSpellFinish;
  protected bool m_settingUpAction;

  public override List<GameObject> GetVisualTargets()
  {
    return this.m_visualTargets;
  }

  public override GameObject GetVisualTarget()
  {
    if (this.m_visualTargets.Count == 0)
      return (GameObject) null;
    return this.m_visualTargets[0];
  }

  public override void AddVisualTarget(GameObject go)
  {
    this.m_visualTargets.Add(go);
  }

  public override void AddVisualTargets(List<GameObject> targets)
  {
    this.m_visualTargets.AddRange((IEnumerable<GameObject>) targets);
  }

  public override bool RemoveVisualTarget(GameObject go)
  {
    return this.m_visualTargets.Remove(go);
  }

  public override void RemoveAllVisualTargets()
  {
    this.m_visualTargets.Clear();
  }

  public override bool IsVisualTarget(GameObject go)
  {
    return this.m_visualTargets.Contains(go);
  }

  public override Card GetVisualTargetCard()
  {
    GameObject visualTarget = this.GetVisualTarget();
    if ((UnityEngine.Object) visualTarget == (UnityEngine.Object) null)
      return (Card) null;
    return visualTarget.GetComponent<Card>();
  }

  protected bool AddPowerTargetsInternal(bool fallbackToStartBlockTarget)
  {
    this.m_visualToTargetIndexMap.Clear();
    this.m_targetToMetaDataMap.Clear();
    if (!this.CanAddPowerTargets() || this.HasChain() && !this.AddPrimaryChainTarget() || !this.AddMultiplePowerTargets())
      return false;
    if (this.m_targets.Count > 0 || !fallbackToStartBlockTarget)
      return true;
    Network.HistBlockStart blockStart = this.m_taskList.GetBlockStart();
    if (blockStart == null || blockStart.Target == 0)
      return true;
    return this.AddSinglePowerTarget_FromBlockStart(blockStart);
  }

  public override bool AddPowerTargets()
  {
    return this.AddPowerTargetsInternal(true);
  }

  protected override void AddTargetFromMetaData(int metaDataIndex, Card targetCard)
  {
    this.m_targetToMetaDataMap[this.m_targets.Count] = metaDataIndex;
    this.AddTarget(targetCard.gameObject);
  }

  protected override void OnBirth(SpellStateType prevStateType)
  {
    this.UpdatePosition();
    this.UpdateOrientation();
    this.m_currentTargetIndex = 0;
    if (this.HasStart())
    {
      this.SpawnStart();
      this.m_startSpell.SafeActivateState(SpellStateType.BIRTH);
      if (this.m_startSpell.GetActiveState() == SpellStateType.NONE)
        this.m_startSpell = (Spell) null;
    }
    base.OnBirth(prevStateType);
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    this.m_settingUpAction = true;
    this.UpdateTargets();
    this.UpdatePosition();
    this.UpdateOrientation();
    this.m_currentTargetIndex = this.GetPrimaryTargetIndex();
    this.UpdatePendingStateChangeFlags(SpellStateType.ACTION);
    this.DoAction();
    base.OnAction(prevStateType);
    this.m_settingUpAction = false;
    this.FinishIfPossible();
  }

  protected override void OnCancel(SpellStateType prevStateType)
  {
    this.UpdatePendingStateChangeFlags(SpellStateType.CANCEL);
    if ((UnityEngine.Object) this.m_startSpell != (UnityEngine.Object) null)
    {
      this.m_startSpell.SafeActivateState(SpellStateType.CANCEL);
      this.m_startSpell = (Spell) null;
    }
    base.OnCancel(prevStateType);
    this.FinishIfPossible();
  }

  public override void OnStateFinished()
  {
    if (this.GuessNextStateType() == SpellStateType.NONE && this.AreEffectsActive())
      this.m_pendingNoneStateChange = true;
    else
      base.OnStateFinished();
  }

  public override void OnSpellFinished()
  {
    if (this.AreEffectsActive())
      this.m_pendingSpellFinish = true;
    else
      base.OnSpellFinished();
  }

  public override void OnFsmStateStarted(FsmState state, SpellStateType stateType)
  {
    if (this.m_activeStateChange == stateType)
      return;
    if (stateType == SpellStateType.NONE && this.AreEffectsActive())
    {
      this.m_pendingSpellFinish = true;
      this.m_pendingNoneStateChange = true;
    }
    else
      base.OnFsmStateStarted(state, stateType);
  }

  private void DoAction()
  {
    if (this.CheckAndWaitForGameEventsThenDoAction() || this.CheckAndWaitForStartDelayThenDoAction() || this.CheckAndWaitForStartPrefabThenDoAction())
      return;
    this.DoActionNow();
  }

  private bool CheckAndWaitForGameEventsThenDoAction()
  {
    if (this.m_taskList == null)
      return false;
    if (this.m_ActionInfo.m_ShowSpellVisuals == SpellVisualShowTime.DURING_GAME_EVENTS)
      return this.DoActionDuringGameEvents();
    if (this.m_ActionInfo.m_ShowSpellVisuals != SpellVisualShowTime.AFTER_GAME_EVENTS)
      return false;
    this.DoActionAfterGameEvents();
    return true;
  }

  private bool DoActionDuringGameEvents()
  {
    this.m_taskList.DoAllTasks();
    if (this.m_taskList.IsComplete())
      return false;
    QueueList<PowerTask> tasksToWaitFor = this.DetermineTasksToWaitFor(0, this.m_taskList.GetTaskList().Count);
    if (tasksToWaitFor.Count == 0)
      return false;
    this.StartCoroutine(this.DoDelayedActionDuringGameEvents(tasksToWaitFor));
    return true;
  }

  [DebuggerHidden]
  private IEnumerator DoDelayedActionDuringGameEvents(QueueList<PowerTask> tasksToWaitFor)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CDoDelayedActionDuringGameEvents\u003Ec__Iterator29D() { tasksToWaitFor = tasksToWaitFor, \u003C\u0024\u003EtasksToWaitFor = tasksToWaitFor, \u003C\u003Ef__this = this };
  }

  private Entity GetEntityFromZoneChangePowerTask(PowerTask task)
  {
    Entity entity;
    int zoneTag;
    this.GetZoneChangeFromPowerTask(task, out entity, out zoneTag);
    return entity;
  }

  private bool GetZoneChangeFromPowerTask(PowerTask task, out Entity entity, out int zoneTag)
  {
    entity = (Entity) null;
    zoneTag = 0;
    Network.PowerHistory power = task.GetPower();
    switch (power.Type)
    {
      case Network.PowerType.FULL_ENTITY:
        Network.HistFullEntity histFullEntity = (Network.HistFullEntity) power;
        Entity entity1 = GameState.Get().GetEntity(histFullEntity.Entity.ID);
        if ((UnityEngine.Object) entity1.GetCard() == (UnityEngine.Object) null)
          return false;
        using (List<Network.Entity.Tag>.Enumerator enumerator = histFullEntity.Entity.Tags.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Network.Entity.Tag current = enumerator.Current;
            if (current.Name == 49)
            {
              entity = entity1;
              zoneTag = current.Value;
              return true;
            }
          }
          break;
        }
      case Network.PowerType.SHOW_ENTITY:
        Network.HistShowEntity histShowEntity = (Network.HistShowEntity) power;
        Entity entity2 = GameState.Get().GetEntity(histShowEntity.Entity.ID);
        if ((UnityEngine.Object) entity2.GetCard() == (UnityEngine.Object) null)
          return false;
        using (List<Network.Entity.Tag>.Enumerator enumerator = histShowEntity.Entity.Tags.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Network.Entity.Tag current = enumerator.Current;
            if (current.Name == 49)
            {
              entity = entity2;
              zoneTag = current.Value;
              return true;
            }
          }
          break;
        }
      case Network.PowerType.TAG_CHANGE:
        Network.HistTagChange histTagChange = (Network.HistTagChange) power;
        Entity entity3 = GameState.Get().GetEntity(histTagChange.Entity);
        if ((UnityEngine.Object) entity3.GetCard() == (UnityEngine.Object) null || histTagChange.Tag != 49)
          return false;
        entity = entity3;
        zoneTag = histTagChange.Value;
        return true;
    }
    return false;
  }

  private void DoActionAfterGameEvents()
  {
    ++this.m_effectsPendingFinish;
    this.m_taskList.DoAllTasks((PowerTaskList.CompleteCallback) ((taskList, startIndex, count, userData) =>
    {
      --this.m_effectsPendingFinish;
      if (this.CheckAndWaitForStartDelayThenDoAction() || this.CheckAndWaitForStartPrefabThenDoAction())
        return;
      this.DoActionNow();
    }));
  }

  private bool CheckAndWaitForStartDelayThenDoAction()
  {
    if ((double) Mathf.Min(this.m_ActionInfo.m_StartDelayMax, this.m_ActionInfo.m_StartDelayMin) <= (double) Mathf.Epsilon)
      return false;
    ++this.m_effectsPendingFinish;
    this.StartCoroutine(this.WaitForStartDelayThenDoAction());
    return true;
  }

  [DebuggerHidden]
  private IEnumerator WaitForStartDelayThenDoAction()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CWaitForStartDelayThenDoAction\u003Ec__Iterator29E() { \u003C\u003Ef__this = this };
  }

  private bool CheckAndWaitForStartPrefabThenDoAction()
  {
    if (!this.HasStart() || (UnityEngine.Object) this.m_startSpell != (UnityEngine.Object) null && this.m_startSpell.GetActiveState() == SpellStateType.IDLE)
      return false;
    if ((UnityEngine.Object) this.m_startSpell == (UnityEngine.Object) null)
      this.SpawnStart();
    this.m_startSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnStartSpellBirthStateFinished));
    if (this.m_startSpell.GetActiveState() != SpellStateType.BIRTH)
    {
      this.m_startSpell.SafeActivateState(SpellStateType.BIRTH);
      if (this.m_startSpell.GetActiveState() == SpellStateType.NONE)
      {
        this.m_startSpell = (Spell) null;
        return false;
      }
    }
    return true;
  }

  private void OnStartSpellBirthStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (prevStateType != SpellStateType.BIRTH)
      return;
    spell.RemoveStateFinishedCallback(new Spell.StateFinishedCallback(this.OnStartSpellBirthStateFinished), userData);
    this.DoActionNow();
  }

  protected virtual void DoActionNow()
  {
    SpellAreaEffectInfo areaEffectInfo = this.DetermineAreaEffectInfo();
    if (areaEffectInfo != null)
      this.SpawnAreaEffect(areaEffectInfo);
    bool flag1 = this.HasMissile();
    bool flag2 = this.HasImpact();
    bool flag3 = this.HasChain();
    if (this.GetVisualTargetCount() > 0 && (flag1 || flag2 || flag3))
    {
      if (flag1)
      {
        if (flag3)
          this.SpawnChainMissile();
        else if (this.m_MissileInfo.m_SpawnInSequence)
          this.SpawnMissileInSequence();
        else
          this.SpawnAllMissiles();
      }
      else
      {
        if (flag2)
        {
          if (flag3)
            this.SpawnImpact(this.m_currentTargetIndex);
          else
            this.SpawnAllImpacts();
        }
        if (flag3)
          this.SpawnChain();
        this.DoStartSpellAction();
      }
    }
    else
      this.DoStartSpellAction();
    this.FinishIfPossible();
  }

  private bool HasStart()
  {
    if (this.m_StartInfo != null && this.m_StartInfo.m_Enabled)
      return (UnityEngine.Object) this.m_StartInfo.m_Prefab != (UnityEngine.Object) null;
    return false;
  }

  private void SpawnStart()
  {
    ++this.m_effectsPendingFinish;
    this.m_startSpell = this.CloneSpell(this.m_StartInfo.m_Prefab);
    this.m_startSpell.SetSource(this.GetSource());
    this.m_startSpell.AddTargets(this.GetTargets());
    if (!this.m_StartInfo.m_UseSuperSpellLocation)
      return;
    this.m_startSpell.SetPosition(this.transform.position);
  }

  private void DoStartSpellAction()
  {
    if ((UnityEngine.Object) this.m_startSpell == (UnityEngine.Object) null)
      return;
    if (!this.m_startSpell.HasUsableState(SpellStateType.ACTION))
    {
      this.m_startSpell.UpdateTransform();
      this.m_startSpell.SafeActivateState(SpellStateType.DEATH);
    }
    else
    {
      this.m_startSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnStartSpellActionFinished));
      this.m_startSpell.ActivateState(SpellStateType.ACTION);
    }
    this.m_startSpell = (Spell) null;
  }

  private void OnStartSpellActionFinished(Spell spell, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.ACTION)
      return;
    spell.SafeActivateState(SpellStateType.DEATH);
  }

  private bool HasMissile()
  {
    if (this.m_MissileInfo == null || !this.m_MissileInfo.m_Enabled)
      return false;
    if (!((UnityEngine.Object) this.m_MissileInfo.m_Prefab != (UnityEngine.Object) null))
      return (UnityEngine.Object) this.m_MissileInfo.m_ReversePrefab != (UnityEngine.Object) null;
    return true;
  }

  private void SpawnChainMissile()
  {
    this.SpawnMissile(this.GetPrimaryTargetIndex());
    this.DoStartSpellAction();
  }

  private void SpawnMissileInSequence()
  {
    if (this.m_currentTargetIndex >= this.GetVisualTargetCount())
      return;
    this.SpawnMissile(this.m_currentTargetIndex);
    ++this.m_currentTargetIndex;
    if ((UnityEngine.Object) this.m_startSpell == (UnityEngine.Object) null)
      return;
    if (this.m_StartInfo.m_DeathAfterAllMissilesFire)
    {
      if (this.m_currentTargetIndex < this.GetVisualTargetCount())
      {
        if (!this.m_startSpell.HasUsableState(SpellStateType.ACTION))
          return;
        this.m_startSpell.ActivateState(SpellStateType.ACTION);
      }
      else
        this.DoStartSpellAction();
    }
    else
      this.DoStartSpellAction();
  }

  private void SpawnAllMissiles()
  {
    for (int targetIndex = 0; targetIndex < this.GetVisualTargetCount(); ++targetIndex)
      this.SpawnMissile(targetIndex);
    this.DoStartSpellAction();
  }

  private void SpawnMissile(int targetIndex)
  {
    ++this.m_effectsPendingFinish;
    this.StartCoroutine(this.WaitAndSpawnMissile(targetIndex));
  }

  [DebuggerHidden]
  private IEnumerator WaitAndSpawnMissile(int targetIndex)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CWaitAndSpawnMissile\u003Ec__Iterator29F() { targetIndex = targetIndex, \u003C\u0024\u003EtargetIndex = targetIndex, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator SpawnReverseMissile(Spell cloneSpell, GameObject sourceObject, GameObject targetObject, float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CSpawnReverseMissile\u003Ec__Iterator2A0() { delay = delay, cloneSpell = cloneSpell, sourceObject = sourceObject, targetObject = targetObject, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003EcloneSpell = cloneSpell, \u003C\u0024\u003EsourceObject = sourceObject, \u003C\u0024\u003EtargetObject = targetObject, \u003C\u003Ef__this = this };
  }

  private void OnMissileSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (prevStateType != SpellStateType.BIRTH)
      return;
    spell.RemoveStateFinishedCallback(new Spell.StateFinishedCallback(this.OnMissileSpellStateFinished), userData);
    int targetIndex = (int) userData;
    bool reverse = targetIndex < 0;
    this.FireMissileOnPath(spell, targetIndex, reverse);
  }

  private void FireMissileOnPath(Spell missile, int targetIndex, bool reverse)
  {
    Vector3[] missilePath = this.GenerateMissilePath(missile);
    float num = UnityEngine.Random.Range(this.m_MissileInfo.m_PathDurationMin, this.m_MissileInfo.m_PathDurationMax);
    Hashtable args = iTween.Hash((object) "path", (object) missilePath, (object) "time", (object) num, (object) "easetype", (object) this.m_MissileInfo.m_PathEaseType, (object) "oncompletetarget", (object) this.gameObject);
    if (reverse)
    {
      args.Add((object) "oncomplete", (object) "OnReverseMissileTargetReached");
      args.Add((object) "oncompleteparams", (object) missile);
    }
    else
    {
      Hashtable hashtable = iTween.Hash((object) "missile", (object) missile, (object) "targetIndex", (object) targetIndex);
      args.Add((object) "oncomplete", (object) "OnMissileTargetReached");
      args.Add((object) "oncompleteparams", (object) hashtable);
    }
    if (!object.Equals((object) missilePath[0], (object) missilePath[2]))
      args.Add((object) "orienttopath", (object) this.m_MissileInfo.m_OrientToPath);
    if (this.m_MissileInfo.m_TargetJoint.Length > 0)
    {
      GameObject childBySubstring = SceneUtils.FindChildBySubstring(missile.gameObject, this.m_MissileInfo.m_TargetJoint);
      if ((UnityEngine.Object) childBySubstring != (UnityEngine.Object) null)
      {
        missile.transform.LookAt(missile.GetTarget().transform, this.m_MissileInfo.m_JointUpVector);
        missilePath[2].y += this.m_MissileInfo.m_TargetHeightOffset;
        iTween.MoveTo(childBySubstring, args);
        return;
      }
    }
    iTween.MoveTo(missile.gameObject, args);
  }

  private Vector3[] GenerateMissilePath(Spell missile)
  {
    Vector3[] path = new Vector3[3];
    path[0] = missile.transform.position;
    Card targetCard = missile.GetTargetCard();
    if ((UnityEngine.Object) targetCard != (UnityEngine.Object) null && targetCard.GetZone() is ZoneHand)
    {
      ZoneHand zone = targetCard.GetZone() as ZoneHand;
      path[2] = zone.GetCardPosition(zone.GetCardSlot(targetCard), -1);
    }
    else
      path[2] = missile.GetTarget().transform.position;
    path[1] = this.GenerateMissilePathCenterPoint(path);
    return path;
  }

  private Vector3 GenerateMissilePathCenterPoint(Vector3[] path)
  {
    Vector3 vector3_1 = path[0];
    Vector3 vector3_2 = path[2];
    Vector3 vector3_3 = vector3_2 - vector3_1;
    float magnitude = vector3_3.magnitude;
    Vector3 vector3_4 = vector3_1;
    bool flag1 = (double) magnitude <= (double) Mathf.Epsilon;
    if (!flag1)
      vector3_4 = vector3_1 + vector3_3 * (this.m_MissileInfo.m_CenterOffsetPercent * 0.01f);
    float num1 = magnitude / this.m_MissileInfo.m_DistanceScaleFactor;
    if (flag1)
    {
      if ((double) this.m_MissileInfo.m_CenterPointHeightMin <= (double) Mathf.Epsilon && (double) this.m_MissileInfo.m_CenterPointHeightMax <= (double) Mathf.Epsilon)
        vector3_4.y += 2f;
      else
        vector3_4.y += UnityEngine.Random.Range(this.m_MissileInfo.m_CenterPointHeightMin, this.m_MissileInfo.m_CenterPointHeightMax);
    }
    else
      vector3_4.y += num1 * UnityEngine.Random.Range(this.m_MissileInfo.m_CenterPointHeightMin, this.m_MissileInfo.m_CenterPointHeightMax);
    float num2 = 1f;
    if ((double) vector3_1.z > (double) vector3_2.z)
      num2 = -1f;
    bool flag2 = GeneralUtils.RandomBool();
    if ((double) this.m_MissileInfo.m_RightMin == 0.0 && (double) this.m_MissileInfo.m_RightMax == 0.0)
      flag2 = false;
    if ((double) this.m_MissileInfo.m_LeftMin == 0.0 && (double) this.m_MissileInfo.m_LeftMax == 0.0)
      flag2 = true;
    if (flag2)
    {
      if ((double) this.m_MissileInfo.m_RightMin == (double) this.m_MissileInfo.m_RightMax || this.m_MissileInfo.m_DebugForceMax)
        vector3_4.x += this.m_MissileInfo.m_RightMax * num1 * num2;
      else
        vector3_4.x += UnityEngine.Random.Range(this.m_MissileInfo.m_RightMin * num1, this.m_MissileInfo.m_RightMax * num1) * num2;
    }
    else if ((double) this.m_MissileInfo.m_LeftMin == (double) this.m_MissileInfo.m_LeftMax || this.m_MissileInfo.m_DebugForceMax)
      vector3_4.x -= this.m_MissileInfo.m_LeftMax * num1 * num2;
    else
      vector3_4.x -= UnityEngine.Random.Range(this.m_MissileInfo.m_LeftMin * num1, this.m_MissileInfo.m_LeftMax * num1) * num2;
    return vector3_4;
  }

  private void OnMissileTargetReached(Hashtable args)
  {
    Spell spell = (Spell) args[(object) "missile"];
    int targetIndex = (int) args[(object) "targetIndex"];
    if (this.HasImpact())
      this.SpawnImpact(targetIndex);
    if (this.HasChain())
      this.SpawnChain();
    else if (this.m_MissileInfo.m_SpawnInSequence)
      this.SpawnMissileInSequence();
    spell.ActivateState(SpellStateType.DEATH);
  }

  private void OnReverseMissileTargetReached(Spell missile)
  {
    missile.ActivateState(SpellStateType.DEATH);
  }

  private bool HasImpact()
  {
    if (this.m_ImpactInfo == null || !this.m_ImpactInfo.m_Enabled)
      return false;
    if (!((UnityEngine.Object) this.m_ImpactInfo.m_Prefab != (UnityEngine.Object) null))
      return this.m_ImpactInfo.m_damageAmountImpactSpells.Length > 0;
    return true;
  }

  private void SpawnAllImpacts()
  {
    for (int targetIndex = 0; targetIndex < this.GetVisualTargetCount(); ++targetIndex)
      this.SpawnImpact(targetIndex);
  }

  private void SpawnImpact(int targetIndex)
  {
    ++this.m_effectsPendingFinish;
    this.StartCoroutine(this.WaitAndSpawnImpact(targetIndex));
  }

  [DebuggerHidden]
  private IEnumerator WaitAndSpawnImpact(int targetIndex)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CWaitAndSpawnImpact\u003Ec__Iterator2A1() { targetIndex = targetIndex, \u003C\u0024\u003EtargetIndex = targetIndex, \u003C\u003Ef__this = this };
  }

  private Spell DetermineImpactPrefab(GameObject targetObject)
  {
    if (this.m_ImpactInfo.m_damageAmountImpactSpells.Length == 0)
      return this.m_ImpactInfo.m_Prefab;
    Spell spell = this.m_ImpactInfo.m_Prefab;
    if (this.m_taskList == null)
      return spell;
    Card component = targetObject.GetComponent<Card>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return spell;
    PowerTaskList.DamageInfo damageInfo = this.m_taskList.GetDamageInfo(component.GetEntity());
    if (damageInfo == null)
      return spell;
    SpellValueRange accordingToRanges = SpellUtils.GetAppropriateElementAccordingToRanges<SpellValueRange>(this.m_ImpactInfo.m_damageAmountImpactSpells, (Func<SpellValueRange, ValueRange>) (x => x.m_range), damageInfo.m_damage);
    if (accordingToRanges != null && (UnityEngine.Object) accordingToRanges.m_spellPrefab != (UnityEngine.Object) null)
      spell = accordingToRanges.m_spellPrefab;
    return spell;
  }

  private bool HasChain()
  {
    if (this.m_ChainInfo != null && this.m_ChainInfo.m_Enabled)
      return (UnityEngine.Object) this.m_ChainInfo.m_Prefab != (UnityEngine.Object) null;
    return false;
  }

  private void SpawnChain()
  {
    if (this.GetVisualTargetCount() <= 1)
      return;
    ++this.m_effectsPendingFinish;
    this.StartCoroutine(this.WaitAndSpawnChain());
  }

  [DebuggerHidden]
  private IEnumerator WaitAndSpawnChain()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CWaitAndSpawnChain\u003Ec__Iterator2A2() { \u003C\u003Ef__this = this };
  }

  private SpellAreaEffectInfo DetermineAreaEffectInfo()
  {
    Card sourceCard = this.GetSourceCard();
    if ((UnityEngine.Object) sourceCard != (UnityEngine.Object) null)
    {
      Player controller = sourceCard.GetController();
      if (controller != null)
      {
        if (controller.IsFriendlySide() && this.HasFriendlyAreaEffect())
          return this.m_FriendlyAreaEffectInfo;
        if (!controller.IsFriendlySide() && this.HasOpponentAreaEffect())
          return this.m_OpponentAreaEffectInfo;
      }
    }
    if (this.HasFriendlyAreaEffect())
      return this.m_FriendlyAreaEffectInfo;
    if (this.HasOpponentAreaEffect())
      return this.m_OpponentAreaEffectInfo;
    return (SpellAreaEffectInfo) null;
  }

  private bool HasAreaEffect()
  {
    if (!this.HasFriendlyAreaEffect())
      return this.HasOpponentAreaEffect();
    return true;
  }

  private bool HasFriendlyAreaEffect()
  {
    if (this.m_FriendlyAreaEffectInfo != null && this.m_FriendlyAreaEffectInfo.m_Enabled)
      return (UnityEngine.Object) this.m_FriendlyAreaEffectInfo.m_Prefab != (UnityEngine.Object) null;
    return false;
  }

  private bool HasOpponentAreaEffect()
  {
    if (this.m_OpponentAreaEffectInfo != null && this.m_OpponentAreaEffectInfo.m_Enabled)
      return (UnityEngine.Object) this.m_OpponentAreaEffectInfo.m_Prefab != (UnityEngine.Object) null;
    return false;
  }

  private void SpawnAreaEffect(SpellAreaEffectInfo info)
  {
    ++this.m_effectsPendingFinish;
    this.StartCoroutine(this.WaitAndSpawnAreaEffect(info));
  }

  [DebuggerHidden]
  private IEnumerator WaitAndSpawnAreaEffect(SpellAreaEffectInfo info)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CWaitAndSpawnAreaEffect\u003Ec__Iterator2A3() { info = info, \u003C\u0024\u003Einfo = info, \u003C\u003Ef__this = this };
  }

  private bool AddPrimaryChainTarget()
  {
    Network.HistBlockStart blockStart = this.m_taskList.GetBlockStart();
    if (blockStart == null)
      return false;
    return this.AddSinglePowerTarget_FromBlockStart(blockStart);
  }

  private int GetPrimaryTargetIndex()
  {
    return 0;
  }

  private GameObject GetPrimaryTarget()
  {
    return this.m_visualTargets[this.GetPrimaryTargetIndex()];
  }

  protected virtual void UpdateTargets()
  {
    this.UpdateVisualTargets();
    this.SuppressPlaySoundsOnVisualTargets();
  }

  private int GetVisualTargetCount()
  {
    if (this.IsMakingClones())
      return this.m_visualTargets.Count;
    return Mathf.Min(1, this.m_visualTargets.Count);
  }

  protected virtual void UpdateVisualTargets()
  {
    switch (this.m_TargetInfo.m_Behavior)
    {
      case SpellTargetBehavior.FRIENDLY_PLAY_ZONE_CENTER:
        this.AddVisualTarget(SpellUtils.FindFriendlyPlayZone((Spell) this).gameObject);
        break;
      case SpellTargetBehavior.FRIENDLY_PLAY_ZONE_RANDOM:
        this.GenerateRandomPlayZoneVisualTargets(SpellUtils.FindFriendlyPlayZone((Spell) this));
        break;
      case SpellTargetBehavior.OPPONENT_PLAY_ZONE_CENTER:
        this.AddVisualTarget(SpellUtils.FindOpponentPlayZone((Spell) this).gameObject);
        break;
      case SpellTargetBehavior.OPPONENT_PLAY_ZONE_RANDOM:
        this.GenerateRandomPlayZoneVisualTargets(SpellUtils.FindOpponentPlayZone((Spell) this));
        break;
      case SpellTargetBehavior.BOARD_CENTER:
        this.AddVisualTarget(Board.Get().FindBone("CenterPointBone").gameObject);
        break;
      case SpellTargetBehavior.UNTARGETED:
        this.AddVisualTarget(this.GetSource());
        break;
      case SpellTargetBehavior.CHOSEN_TARGET_ONLY:
        this.AddChosenTargetAsVisualTarget();
        break;
      case SpellTargetBehavior.BOARD_RANDOM:
        this.GenerateRandomBoardVisualTargets();
        break;
      case SpellTargetBehavior.TARGET_ZONE_CENTER:
        this.AddVisualTarget(SpellUtils.FindTargetZone((Spell) this).gameObject);
        break;
      case SpellTargetBehavior.NEW_CREATED_CARDS:
        this.GenerateCreatedCardsTargets();
        break;
      default:
        this.AddAllTargetsAsVisualTargets();
        break;
    }
  }

  protected void GenerateRandomBoardVisualTargets()
  {
    ZonePlay friendlyPlayZone = SpellUtils.FindFriendlyPlayZone((Spell) this);
    ZonePlay opponentPlayZone = SpellUtils.FindOpponentPlayZone((Spell) this);
    Bounds bounds1 = friendlyPlayZone.GetComponent<Collider>().bounds;
    Bounds bounds2 = opponentPlayZone.GetComponent<Collider>().bounds;
    Vector3 vector3_1 = Vector3.Min(bounds1.min, bounds2.min);
    Vector3 vector3_2 = Vector3.Max(bounds1.max, bounds2.max);
    Vector3 center = 0.5f * (vector3_2 + vector3_1);
    Vector3 vector3_3 = vector3_2 - vector3_1;
    Vector3 size = new Vector3(Mathf.Abs(vector3_3.x), Mathf.Abs(vector3_3.y), Mathf.Abs(vector3_3.z));
    this.GenerateRandomVisualTargets(new Bounds(center, size));
  }

  protected void GenerateRandomPlayZoneVisualTargets(ZonePlay zonePlay)
  {
    this.GenerateRandomVisualTargets(zonePlay.GetComponent<Collider>().bounds);
  }

  private void GenerateRandomVisualTargets(Bounds bounds)
  {
    int length = UnityEngine.Random.Range(this.m_TargetInfo.m_RandomTargetCountMin, this.m_TargetInfo.m_RandomTargetCountMax + 1);
    if (length == 0)
      return;
    float x = bounds.min.x;
    float z1 = bounds.max.z;
    float z2 = bounds.min.z;
    float num1 = bounds.size.x / (float) length;
    int[] boxUsageCounts = new int[length];
    int[] numArray = new int[length];
    for (int index = 0; index < length; ++index)
    {
      boxUsageCounts[index] = 0;
      numArray[index] = -1;
    }
    for (int index1 = 0; index1 < length; ++index1)
    {
      float num2 = UnityEngine.Random.Range(0.0f, 1f);
      int max1 = 0;
      for (int index2 = 0; index2 < length; ++index2)
      {
        if ((double) this.ComputeBoxPickChance(boxUsageCounts, index2) >= (double) num2)
          numArray[max1++] = index2;
      }
      int boxIndex = numArray[UnityEngine.Random.Range(0, max1)];
      ++boxUsageCounts[boxIndex];
      float min = x + (float) boxIndex * num1;
      float max2 = min + num1;
      this.GenerateVisualTarget(new Vector3()
      {
        x = UnityEngine.Random.Range(min, max2),
        y = bounds.center.y,
        z = UnityEngine.Random.Range(z2, z1)
      }, index1, boxIndex);
    }
  }

  private void GenerateVisualTarget(Vector3 position, int index, int boxIndex)
  {
    GameObject go = new GameObject();
    go.name = string.Format("{0} Target {1} (box {2})", (object) this, (object) index, (object) boxIndex);
    go.transform.position = position;
    go.AddComponent<SpellGeneratedTarget>();
    this.AddVisualTarget(go);
  }

  private float ComputeBoxPickChance(int[] boxUsageCounts, int index)
  {
    return 1f - (float) boxUsageCounts[index] / ((float) boxUsageCounts.Length * 0.25f);
  }

  private void GenerateCreatedCardsTargets()
  {
    using (List<PowerTask>.Enumerator enumerator = this.m_taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.PowerHistory power = enumerator.Current.GetPower();
        if (power.Type == Network.PowerType.FULL_ENTITY)
        {
          int id = (power as Network.HistFullEntity).Entity.ID;
          Entity entity = GameState.Get().GetEntity(id);
          if (entity.GetTag(GAME_TAG.ZONE) != 6)
          {
            if (entity == null)
            {
              UnityEngine.Debug.LogWarning((object) string.Format("{0}.GenerateCreatedCardsTargets() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) id));
            }
            else
            {
              Card card = entity.GetCard();
              if ((UnityEngine.Object) card == (UnityEngine.Object) null)
                UnityEngine.Debug.LogWarning((object) string.Format("{0}.GenerateCreatedCardsTargets() - WARNING trying to target entity.GetCard() with id {1} but there is no card with that id", (object) this, (object) id));
              else
                this.m_visualTargets.Add(card.gameObject);
            }
          }
        }
      }
    }
  }

  private void AddChosenTargetAsVisualTarget()
  {
    Card powerTargetCard = this.GetPowerTargetCard();
    if ((UnityEngine.Object) powerTargetCard == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) string.Format("{0}.AddChosenTargetAsVisualTarget() - there is no chosen target", (object) this));
    else
      this.AddVisualTarget(powerTargetCard.gameObject);
  }

  private void AddAllTargetsAsVisualTargets()
  {
    for (int index = 0; index < this.m_targets.Count; ++index)
    {
      this.m_visualToTargetIndexMap[this.m_visualTargets.Count] = index;
      this.AddVisualTarget(this.m_targets[index]);
    }
  }

  private void SuppressPlaySoundsOnVisualTargets()
  {
    if (!this.m_TargetInfo.m_SuppressPlaySounds)
      return;
    for (int index = 0; index < this.m_visualTargets.Count; ++index)
    {
      Card component = this.m_visualTargets[index].GetComponent<Card>();
      if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        component.SuppressPlaySounds(true);
    }
  }

  protected virtual void CleanUp()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_visualTargets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((UnityEngine.Object) current.GetComponent<SpellGeneratedTarget>() == (UnityEngine.Object) null))
          UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.m_visualTargets.Clear();
  }

  protected bool HasMetaDataTargets()
  {
    return this.m_targetToMetaDataMap.Count > 0;
  }

  protected int GetMetaDataIndexForTarget(int visualTargetIndex)
  {
    int key;
    int num;
    if (!this.m_visualToTargetIndexMap.TryGetValue(visualTargetIndex, out key) || !this.m_targetToMetaDataMap.TryGetValue(key, out num))
      return -1;
    return num;
  }

  protected bool ShouldCompleteTasksUntilMetaData(int metaDataIndex)
  {
    return this.m_taskList.HasEarlierIncompleteTask(metaDataIndex);
  }

  [DebuggerHidden]
  protected IEnumerator CompleteTasksUntilMetaData(int metaDataIndex)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CCompleteTasksUntilMetaData\u003Ec__Iterator2A4() { metaDataIndex = metaDataIndex, \u003C\u0024\u003EmetaDataIndex = metaDataIndex, \u003C\u003Ef__this = this };
  }

  protected QueueList<PowerTask> DetermineTasksToWaitFor(int startIndex, int count)
  {
    if (count == 0)
      return (QueueList<PowerTask>) null;
    int num = startIndex + count;
    QueueList<PowerTask> queueList = new QueueList<PowerTask>();
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    for (int index = startIndex; index < num; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      SuperSpell.\u003CDetermineTasksToWaitFor\u003Ec__AnonStorey405 forCAnonStorey405 = new SuperSpell.\u003CDetermineTasksToWaitFor\u003Ec__AnonStorey405();
      PowerTask task = taskList[index];
      // ISSUE: reference to a compiler-generated field
      forCAnonStorey405.entity = this.GetEntityFromZoneChangePowerTask(task);
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      if (forCAnonStorey405.entity != null && !((UnityEngine.Object) this.m_visualTargets.Find(new Predicate<GameObject>(forCAnonStorey405.\u003C\u003Em__20C)) == (UnityEngine.Object) null))
      {
        for (int position = 0; position < queueList.Count; ++position)
        {
          Entity zoneChangePowerTask = this.GetEntityFromZoneChangePowerTask(queueList[position]);
          // ISSUE: reference to a compiler-generated field
          if (forCAnonStorey405.entity == zoneChangePowerTask)
          {
            queueList.RemoveAt(position);
            break;
          }
        }
        queueList.Enqueue(task);
      }
    }
    return queueList;
  }

  [DebuggerHidden]
  protected IEnumerator WaitForTasks(QueueList<PowerTask> tasksToWaitFor)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CWaitForTasks\u003Ec__Iterator2A5() { tasksToWaitFor = tasksToWaitFor, \u003C\u0024\u003EtasksToWaitFor = tasksToWaitFor, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected IEnumerator CompleteTasksFromMetaData(int metaDataIndex, float delaySec)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperSpell.\u003CCompleteTasksFromMetaData\u003Ec__Iterator2A6() { delaySec = delaySec, metaDataIndex = metaDataIndex, \u003C\u0024\u003EdelaySec = delaySec, \u003C\u0024\u003EmetaDataIndex = metaDataIndex, \u003C\u003Ef__this = this };
  }

  protected void OnMetaDataTasksComplete(PowerTaskList taskList, int startIndex, int count, object userData)
  {
    --this.m_effectsPendingFinish;
    this.FinishIfPossible();
  }

  protected bool IsMakingClones()
  {
    return true;
  }

  protected bool AreEffectsActive()
  {
    return this.m_effectsPendingFinish > 0;
  }

  protected Spell CloneSpell(Spell prefab)
  {
    Spell spell;
    if (this.IsMakingClones())
    {
      spell = UnityEngine.Object.Instantiate<Spell>(prefab);
      spell.AddStateStartedCallback(new Spell.StateStartedCallback(this.OnCloneSpellStateStarted));
      spell.transform.parent = this.transform;
    }
    else
    {
      spell = prefab;
      spell.RemoveAllTargets();
    }
    spell.AddFinishedCallback(new Spell.FinishedCallback(this.OnCloneSpellFinished));
    return spell;
  }

  private void OnCloneSpellFinished(Spell spell, object userData)
  {
    --this.m_effectsPendingFinish;
    this.FinishIfPossible();
  }

  private void OnCloneSpellStateStarted(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) spell.gameObject);
  }

  private void UpdatePendingStateChangeFlags(SpellStateType stateType)
  {
    if (!this.HasStateContent(stateType))
    {
      this.m_pendingNoneStateChange = true;
      this.m_pendingSpellFinish = true;
    }
    else
    {
      this.m_pendingNoneStateChange = false;
      this.m_pendingSpellFinish = false;
    }
  }

  protected void FinishIfPossible()
  {
    if (this.m_settingUpAction || this.AreEffectsActive())
      return;
    if (this.m_pendingSpellFinish)
    {
      this.OnSpellFinished();
      this.m_pendingSpellFinish = false;
    }
    if (this.m_pendingNoneStateChange)
    {
      this.OnStateFinished();
      this.m_pendingNoneStateChange = false;
    }
    this.CleanUp();
  }
}
