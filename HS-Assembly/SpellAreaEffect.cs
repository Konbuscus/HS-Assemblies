﻿// Decompiled with JetBrains decompiler
// Type: SpellAreaEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpellAreaEffect : Spell
{
  public Spell m_ImpactSpellPrefab;

  public override bool AddPowerTargets()
  {
    if (!this.CanAddPowerTargets() || !this.AddMultiplePowerTargets())
      return false;
    return this.GetTargets().Count > 0;
  }

  protected override void OnDeath(SpellStateType prevStateType)
  {
    base.OnDeath(prevStateType);
    if ((Object) this.m_ImpactSpellPrefab == (Object) null)
      return;
    for (int index = 0; index < this.m_targets.Count; ++index)
      this.SpawnImpactSpell(this.m_targets[index]);
  }

  private void SpawnImpactSpell(GameObject targetObject)
  {
    Spell component = ((GameObject) Object.Instantiate((Object) this.m_ImpactSpellPrefab.gameObject, targetObject.transform.position, Quaternion.identity)).GetComponent<Spell>();
    component.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnImpactSpellStateFinished));
    component.Activate();
  }

  private void OnImpactSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    Object.Destroy((Object) spell.gameObject);
  }
}
