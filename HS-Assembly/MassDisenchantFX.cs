﻿// Decompiled with JetBrains decompiler
// Type: MassDisenchantFX
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class MassDisenchantFX
{
  public HighlightState m_highlight;
  public GameObject m_blockInteraction;
  public GameObject m_glowBall;
  public GameObject m_glowTotal;
  public GameObject m_gemBoxLeft1;
  public GameObject m_gemBoxLeft2;
  public GameObject m_gemBoxRight1;
  public GameObject m_gemBoxRight2;
  public GameObject m_burstFX_Common;
  public GameObject m_burstFX_Rare;
  public GameObject m_burstFX_Epic;
  public GameObject m_burstFX_Legendary;
  public Material m_glowBallMat_Common;
  public Material m_glowBallMat_Rare;
  public Material m_glowBallMat_Epic;
  public Material m_glowBallMat_Legendary;
  public Material m_glowTrailMat_Common;
  public Material m_glowTrailMat_Rare;
  public Material m_glowTrailMat_Epic;
  public Material m_glowTrailMat_Legendary;
}
