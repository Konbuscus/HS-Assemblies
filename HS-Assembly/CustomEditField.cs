﻿// Decompiled with JetBrains decompiler
// Type: CustomEditField
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
public class CustomEditField : Attribute
{
  public int ListSortPriority = -1;
  public bool AllowSceneObject = true;
  public bool Hide;
  public string Sections;
  public string Parent;
  public string Label;
  public string Range;
  public bool ListTable;
  public bool ListSortable;
  public EditType T;

  public override string ToString()
  {
    if (this.Sections == null)
      return this.T.ToString();
    return string.Format("Sections={0} T={1}", (object) this.Sections, (object) this.T);
  }
}
