﻿// Decompiled with JetBrains decompiler
// Type: CollectionCardVisual
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.Rendering;

[CustomEditClass]
public class CollectionCardVisual : PegUIElement
{
  public Vector3 m_boxColliderCenter = new Vector3(0.0f, 0.14f, 0.0f);
  public Vector3 m_boxColliderSize = new Vector3(2f, 0.21f, 2.7f);
  public Vector3 m_heroSkinBoxColliderCenter = new Vector3(0.0f, 0.14f, -0.58f);
  public Vector3 m_heroSkinBoxColliderSize = new Vector3(2f, 0.21f, 2f);
  private const string ADD_CARD_TO_DECK_SOUND = "collection_manager_card_add_to_deck_instant";
  private const string CARD_LIMIT_UNLOCK_SOUND = "card_limit_unlock";
  private const string CARD_LIMIT_LOCK_SOUND = "card_limit_lock";
  private const string CARD_MOUSE_OVER_SOUND = "collection_manager_card_mouse_over";
  private const string CARD_MOVE_INVALID_OR_CLICK_SOUND = "collection_manager_card_move_invalid_or_click";
  public CollectionCardCount m_cardCount;
  public CollectionCardLock m_cardLock;
  public GameObject m_newCardCallout;
  private Vector3 m_originalPosition;
  private Actor m_actor;
  private CollectionCardVisual.LockType m_lockType;
  private bool m_shown;
  private CollectionManagerDisplay.ViewMode m_visualType;
  private int m_cmRow;

  protected override void Awake()
  {
    base.Awake();
    if ((Object) this.gameObject.GetComponent<AudioSource>() == (Object) null)
      this.gameObject.AddComponent<AudioSource>();
    this.SetDragTolerance(5f);
    SoundManager.Get().Load("collection_manager_card_add_to_deck_instant");
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  public void ShowLock(CollectionCardVisual.LockType type)
  {
    this.ShowLock(type, (string) null, false);
  }

  public void ShowLock(CollectionCardVisual.LockType lockType, string reason, bool playSound)
  {
    CollectionCardVisual.LockType lockType1 = this.m_lockType;
    this.m_lockType = lockType;
    this.UpdateCardCountVisibility();
    if ((Object) this.m_cardLock != (Object) null)
    {
      if ((Object) this.m_actor != (Object) null)
        this.m_cardLock.UpdateLockVisual(this.m_actor.GetEntityDef(), lockType, reason);
      else
        this.m_cardLock.UpdateLockVisual((EntityDef) null, CollectionCardVisual.LockType.NONE, (string) null);
    }
    if (!playSound)
      return;
    if (this.m_lockType == CollectionCardVisual.LockType.NONE && lockType1 != CollectionCardVisual.LockType.NONE)
      SoundManager.Get().LoadAndPlay("card_limit_unlock");
    if (this.m_lockType == CollectionCardVisual.LockType.NONE || lockType1 != CollectionCardVisual.LockType.NONE)
      return;
    SoundManager.Get().LoadAndPlay("card_limit_lock");
  }

  public void OnDoneCrafting()
  {
    this.UpdateCardCount();
  }

  public void SetActor(Actor actor, CollectionManagerDisplay.ViewMode type = CollectionManagerDisplay.ViewMode.CARDS)
  {
    if ((Object) this.m_actor != (Object) null && (Object) this.m_actor.transform.parent == (Object) this.transform)
    {
      EntityDef entityDef = this.m_actor.GetEntityDef();
      if (entityDef != null)
        DefLoader.Get().ClearCardDef(entityDef.GetCardId());
      this.m_actor.Hide();
    }
    this.m_visualType = type;
    this.m_actor = actor;
    this.UpdateCardCount();
    if ((Object) this.m_actor == (Object) null)
      return;
    GameUtils.SetParent((Component) actor, (Component) this, false);
    this.ShowNewCardCallout(this.m_actor.GetActorStateMgr().GetActiveStateType() == ActorStateType.CARD_RECENTLY_ACQUIRED);
  }

  public Actor GetActor()
  {
    if ((Object) this.m_actor == (Object) null)
      return (Actor) null;
    if ((Object) this.m_actor.GetCardDef() == (Object) null && this.m_actor.GetEntityDef() != null)
      this.m_actor.SetCardDef(DefLoader.Get().GetCardDef(this.m_actor.GetEntityDef().GetCardId(), new CardPortraitQuality(3, this.m_actor.GetPremium())));
    return this.m_actor;
  }

  public CollectionManagerDisplay.ViewMode GetVisualType()
  {
    return this.m_visualType;
  }

  public void SetCMRow(int rowNum)
  {
    this.m_cmRow = rowNum;
  }

  public int GetCMRow()
  {
    return this.m_cmRow;
  }

  public static void ShowActorShadow(Actor actor, bool show)
  {
    string tag1 = "FakeShadow";
    string tag2 = "FakeShadowUnique";
    GameObject childByTag1 = SceneUtils.FindChildByTag(actor.gameObject, tag1);
    GameObject childByTag2 = SceneUtils.FindChildByTag(actor.gameObject, tag2);
    EntityDef entityDef = actor.GetEntityDef();
    if (entityDef != null && show)
    {
      if (entityDef.IsElite())
      {
        if ((Object) childByTag1 != (Object) null)
          childByTag1.GetComponent<Renderer>().enabled = false;
        if (!((Object) childByTag2 != (Object) null))
          return;
        childByTag2.GetComponent<Renderer>().enabled = true;
      }
      else
      {
        if ((Object) childByTag1 != (Object) null)
          childByTag1.GetComponent<Renderer>().enabled = true;
        if (!((Object) childByTag2 != (Object) null))
          return;
        childByTag2.GetComponent<Renderer>().enabled = false;
      }
    }
    else
    {
      if ((Object) childByTag1 != (Object) null)
        childByTag1.GetComponent<Renderer>().enabled = false;
      if (!((Object) childByTag2 != (Object) null))
        return;
      childByTag2.GetComponent<Renderer>().enabled = false;
    }
  }

  public void Show()
  {
    this.m_shown = true;
    this.SetEnabled(true);
    this.GetComponent<Collider>().enabled = true;
    if ((Object) this.m_actor == (Object) null || this.m_actor.GetEntityDef() == null)
      return;
    bool show = false;
    if (this.m_visualType == CollectionManagerDisplay.ViewMode.CARDS)
    {
      string cardId = this.m_actor.GetEntityDef().GetCardId();
      TAG_PREMIUM premium = this.m_actor.GetPremium();
      show = CollectionManagerDisplay.Get().ShouldShowNewCardGlow(cardId, premium);
      if (!show)
        CollectionManager.Get().MarkAllInstancesAsSeen(cardId, premium);
    }
    this.ShowNewCardCallout(show);
    this.m_actor.Show();
    this.m_actor.SetActorState(!show ? ActorStateType.CARD_IDLE : ActorStateType.CARD_RECENTLY_ACQUIRED);
    Renderer[] componentsInChildren = this.m_actor.gameObject.GetComponentsInChildren<Renderer>();
    if (componentsInChildren != null)
    {
      foreach (Renderer renderer in componentsInChildren)
        renderer.shadowCastingMode = ShadowCastingMode.Off;
    }
    CollectionCardVisual.ShowActorShadow(this.m_actor, CollectionManager.Get().IsCardInCollection(this.m_actor.GetEntityDef().GetCardId(), this.m_actor.GetPremium()));
  }

  public void Hide()
  {
    this.m_shown = false;
    this.SetEnabled(false);
    this.GetComponent<Collider>().enabled = false;
    this.ShowLock(CollectionCardVisual.LockType.NONE);
    this.ShowNewCardCallout(false);
    if ((Object) this.m_cardCount != (Object) null)
      this.m_cardCount.Hide();
    if ((Object) this.m_actor == (Object) null)
      return;
    this.m_actor.Hide();
    PegUI.Get().RemoveAsMouseDownElement((PegUIElement) this);
  }

  public CollectionCardCount GetCardCountObject()
  {
    return this.m_cardCount;
  }

  public void SetHeroSkinBoxCollider()
  {
    BoxCollider component = this.GetComponent<BoxCollider>();
    component.center = this.m_heroSkinBoxColliderCenter;
    component.size = this.m_heroSkinBoxColliderSize;
  }

  public void SetDefaultBoxCollider()
  {
    BoxCollider component = this.GetComponent<BoxCollider>();
    component.center = this.m_boxColliderCenter;
    component.size = this.m_boxColliderSize;
  }

  private bool CheckCardSeen()
  {
    bool flag = this.m_actor.GetActorStateMgr().GetActiveStateType() == ActorStateType.CARD_RECENTLY_ACQUIRED;
    if (flag)
    {
      EntityDef entityDef = this.m_actor.GetEntityDef();
      if (entityDef != null)
        CollectionManager.Get().MarkAllInstancesAsSeen(entityDef.GetCardId(), this.m_actor.GetPremium());
    }
    return flag;
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    if (this.ShouldIgnoreAllInput())
      return;
    EntityDef entityDef = this.m_actor.GetEntityDef();
    if (entityDef != null)
    {
      TooltipPanelManager.Orientation orientation = this.m_cmRow <= 0 ? TooltipPanelManager.Orientation.RightTop : TooltipPanelManager.Orientation.RightBottom;
      TooltipPanelManager.Get().UpdateKeywordHelpForCollectionManager(entityDef, this.m_actor, orientation);
    }
    SoundManager.Get().LoadAndPlay("collection_manager_card_mouse_over", this.gameObject);
    if (!this.IsInCollection())
      return;
    ActorStateType stateType = ActorStateType.CARD_MOUSE_OVER;
    if (this.CheckCardSeen())
      stateType = ActorStateType.CARD_RECENTLY_ACQUIRED_MOUSE_OVER;
    this.m_actor.SetActorState(stateType);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    TooltipPanelManager.Get().HideKeywordHelp();
    if (this.ShouldIgnoreAllInput() || !this.IsInCollection())
      return;
    this.CheckCardSeen();
    this.m_actor.SetActorState(ActorStateType.CARD_IDLE);
    this.ShowNewCardCallout(false);
  }

  protected override void OnHold()
  {
    if (!this.CanPickUpCard())
      return;
    CollectionInputMgr.Get().GrabCard(this);
  }

  protected override void OnRelease()
  {
    if (this.IsTransactionPendingOnThisCard())
      return;
    if (UniversalInputManager.Get().IsTouchMode() || (Object) CraftingTray.Get() != (Object) null && CraftingTray.Get().IsShown())
    {
      this.CheckCardSeen();
      this.ShowNewCardCallout(false);
      this.m_actor.SetActorState(ActorStateType.CARD_IDLE);
      this.EnterCraftingMode();
    }
    else
    {
      Spell spell = this.m_actor.GetSpell(SpellType.DEATHREVERSE);
      if ((Object) spell != (Object) null)
        spell.gameObject.GetComponentInChildren<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.Local;
      if (!this.CanPickUpCard())
      {
        SoundManager.Get().LoadAndPlay("collection_manager_card_move_invalid_or_click");
        if ((Object) spell != (Object) null)
          spell.ActivateState(SpellStateType.BIRTH);
        EntityDef entityDef = this.m_actor.GetEntityDef();
        CollectionManagerDisplay.Get().ShowInnkeeeprLClickHelp(entityDef != null && entityDef.IsHero());
      }
      else if (this.m_visualType == CollectionManagerDisplay.ViewMode.CARDS)
      {
        EntityDef entityDef = this.m_actor.GetEntityDef();
        if (entityDef == null)
          return;
        if ((Object) spell != (Object) null)
          spell.ActivateState(SpellStateType.BIRTH);
        CollectionDeckTray.Get().AddCard(entityDef, this.m_actor.GetPremium(), (DeckTrayDeckTileVisual) null, false, this.m_actor);
      }
      else if (this.m_visualType == CollectionManagerDisplay.ViewMode.CARD_BACKS)
      {
        CollectionDeckTray.Get().SetCardBack(this.m_actor);
      }
      else
      {
        if (this.m_visualType != CollectionManagerDisplay.ViewMode.HERO_SKINS)
          return;
        CollectionDeckTray.Get().SetHeroSkin(this.m_actor);
      }
    }
  }

  protected override void OnRightClick()
  {
    if (this.IsTransactionPendingOnThisCard())
      return;
    if (!Options.Get().GetBool(Option.SHOW_ADVANCED_COLLECTIONMANAGER, false))
      Options.Get().SetBool(Option.SHOW_ADVANCED_COLLECTIONMANAGER, true);
    this.ShowNewCardCallout(false);
    this.m_actor.SetActorState(ActorStateType.CARD_IDLE);
    this.EnterCraftingMode();
  }

  private void EnterCraftingMode()
  {
    CollectionManagerDisplay.ViewMode viewMode = CollectionManagerDisplay.Get().GetViewMode();
    if (this.m_visualType != viewMode)
      return;
    switch (viewMode)
    {
      case CollectionManagerDisplay.ViewMode.CARDS:
        if ((Object) CraftingManager.Get() != (Object) null)
        {
          CraftingManager.Get().EnterCraftMode(this.GetActor());
          break;
        }
        break;
      case CollectionManagerDisplay.ViewMode.HERO_SKINS:
        HeroSkinInfoManager heroSkinInfoManager = HeroSkinInfoManager.Get();
        if ((Object) heroSkinInfoManager != (Object) null)
        {
          heroSkinInfoManager.EnterPreview(this);
          break;
        }
        break;
      case CollectionManagerDisplay.ViewMode.CARD_BACKS:
        CardBackInfoManager cardBackInfoManager = CardBackInfoManager.Get();
        if ((Object) cardBackInfoManager != (Object) null)
        {
          cardBackInfoManager.EnterPreview(this);
          break;
        }
        break;
    }
    CollectionDeckTray.Get().CancelRenamingDeck();
  }

  private bool IsTransactionPendingOnThisCard()
  {
    CraftingManager craftingManager = CraftingManager.Get();
    if ((Object) craftingManager == (Object) null)
      return false;
    PendingTransaction serverTransaction = craftingManager.GetPendingServerTransaction();
    if (serverTransaction == null)
      return false;
    EntityDef entityDef = this.m_actor.GetEntityDef();
    return entityDef != null && !(serverTransaction.CardID != entityDef.GetCardId()) && serverTransaction.Premium == this.m_actor.GetPremium();
  }

  private bool ShouldIgnoreAllInput()
  {
    return !this.m_shown || (Object) CollectionInputMgr.Get() != (Object) null && CollectionInputMgr.Get().IsDraggingScrollbar() || ((Object) CraftingManager.Get() != (Object) null && CraftingManager.Get().IsCardShowing() || CollectionManagerDisplay.Get().m_pageManager.ArePagesTurning());
  }

  private bool IsInCollection()
  {
    if ((Object) this.m_cardCount == (Object) null)
      return false;
    return this.m_cardCount.GetCount() > 0;
  }

  private bool IsUnlocked()
  {
    return this.m_lockType == CollectionCardVisual.LockType.NONE;
  }

  private bool CanPickUpCard()
  {
    return !this.ShouldIgnoreAllInput() && CollectionManagerDisplay.Get().GetViewMode() == this.m_visualType && CollectionDeckTray.Get().CanPickupCard() && (this.m_visualType != CollectionManagerDisplay.ViewMode.CARDS || this.IsInCollection() && this.IsUnlocked());
  }

  public void ShowNewCardCallout(bool show)
  {
    if ((Object) this.m_newCardCallout == (Object) null)
      return;
    this.m_newCardCallout.SetActive(show);
  }

  private void UpdateCardCount()
  {
    if ((Object) this.m_cardCount == (Object) null)
      return;
    int cardCount = 0;
    if ((Object) this.m_actor != (Object) null)
    {
      EntityDef entityDef = this.m_actor.GetEntityDef();
      if (entityDef != null)
        cardCount = CollectionManager.Get().GetCard(entityDef.GetCardId(), this.m_actor.GetPremium()).OwnedCount;
    }
    this.m_cardCount.SetCount(cardCount);
    this.UpdateCardCountVisibility();
  }

  private void UpdateCardCountVisibility()
  {
    if ((Object) this.m_cardCount == (Object) null)
      return;
    if (this.m_lockType == CollectionCardVisual.LockType.NONE && this.m_visualType == CollectionManagerDisplay.ViewMode.CARDS)
      this.m_cardCount.Show();
    else
      this.m_cardCount.Hide();
  }

  public enum LockType
  {
    NONE,
    MAX_COPIES_IN_DECK,
    NO_MORE_INSTANCES,
  }
}
