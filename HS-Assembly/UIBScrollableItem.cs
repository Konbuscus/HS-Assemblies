﻿// Decompiled with JetBrains decompiler
// Type: UIBScrollableItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIBScrollableItem : MonoBehaviour
{
  public Vector3 m_offset = Vector3.zero;
  public Vector3 m_size = Vector3.one;
  public UIBScrollableItem.ActiveState m_active;
  private UIBScrollableItem.ActiveStateCallback m_activeStateCallback;

  public bool IsActive()
  {
    if (this.m_activeStateCallback != null)
      return this.m_activeStateCallback();
    if (this.m_active == UIBScrollableItem.ActiveState.Active)
      return true;
    if (this.m_active == UIBScrollableItem.ActiveState.UseHierarchy)
      return this.gameObject.activeInHierarchy;
    return false;
  }

  public void SetCustomActiveState(UIBScrollableItem.ActiveStateCallback callback)
  {
    this.m_activeStateCallback = callback;
  }

  public OrientedBounds GetOrientedBounds()
  {
    Matrix4x4 localToWorldMatrix = this.transform.localToWorldMatrix;
    return new OrientedBounds() { Origin = this.transform.position + (Vector3) (localToWorldMatrix * (Vector4) this.m_offset), Extents = new Vector3[3]{ (Vector3) (localToWorldMatrix * (Vector4) new Vector3(this.m_size.x * 0.5f, 0.0f, 0.0f)), (Vector3) (localToWorldMatrix * (Vector4) new Vector3(0.0f, this.m_size.y * 0.5f, 0.0f)), (Vector3) (localToWorldMatrix * (Vector4) new Vector3(0.0f, 0.0f, this.m_size.z * 0.5f)) } };
  }

  public void GetWorldBounds(out Vector3 min, out Vector3 max)
  {
    min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
    max = new Vector3(float.MinValue, float.MinValue, float.MinValue);
    Vector3[] boundsPoints = this.GetBoundsPoints();
    for (int index = 0; index < 8; ++index)
    {
      min.x = Mathf.Min(boundsPoints[index].x, min.x);
      min.y = Mathf.Min(boundsPoints[index].y, min.y);
      min.z = Mathf.Min(boundsPoints[index].z, min.z);
      max.x = Mathf.Max(boundsPoints[index].x, max.x);
      max.y = Mathf.Max(boundsPoints[index].y, max.y);
      max.z = Mathf.Max(boundsPoints[index].z, max.z);
    }
  }

  private Vector3[] GetBoundsPoints()
  {
    Matrix4x4 localToWorldMatrix = this.transform.localToWorldMatrix;
    Vector3[] vector3Array = new Vector3[3]{ (Vector3) (localToWorldMatrix * (Vector4) new Vector3(this.m_size.x * 0.5f, 0.0f, 0.0f)), (Vector3) (localToWorldMatrix * (Vector4) new Vector3(0.0f, this.m_size.y * 0.5f, 0.0f)), (Vector3) (localToWorldMatrix * (Vector4) new Vector3(0.0f, 0.0f, this.m_size.z * 0.5f)) };
    Vector3 vector3 = this.transform.position + (Vector3) (localToWorldMatrix * (Vector4) this.m_offset);
    return new Vector3[8]{ vector3 + vector3Array[0] + vector3Array[1] + vector3Array[2], vector3 + vector3Array[0] + vector3Array[1] - vector3Array[2], vector3 + vector3Array[0] - vector3Array[1] + vector3Array[2], vector3 + vector3Array[0] - vector3Array[1] - vector3Array[2], vector3 - vector3Array[0] + vector3Array[1] + vector3Array[2], vector3 - vector3Array[0] + vector3Array[1] - vector3Array[2], vector3 - vector3Array[0] - vector3Array[1] + vector3Array[2], vector3 - vector3Array[0] - vector3Array[1] - vector3Array[2] };
  }

  public enum ActiveState
  {
    Active,
    Inactive,
    UseHierarchy,
  }

  public delegate bool ActiveStateCallback();
}
