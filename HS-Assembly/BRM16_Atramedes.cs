﻿// Decompiled with JetBrains decompiler
// Type: BRM16_Atramedes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class BRM16_Atramedes : BRM_MissionEntity
{
  private bool m_heroPowerLinePlayed;
  private bool m_cardLinePlayed;
  private int m_gongLinePlayed;
  private int m_weaponLinePlayed;

  public override string GetAlternatePlayerName()
  {
    return GameStrings.Get("MISSION_NEFARIAN_TITLE");
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BRMA16_1_RESPONSE_03");
    this.PreloadSound("VO_BRMA16_1_HERO_POWER_05");
    this.PreloadSound("VO_BRMA16_1_CARD_04");
    this.PreloadSound("VO_BRMA16_1_GONG1_10");
    this.PreloadSound("VO_BRMA16_1_GONG2_11");
    this.PreloadSound("VO_BRMA16_1_GONG3_12");
    this.PreloadSound("VO_BRMA16_1_TRIGGER1_07");
    this.PreloadSound("VO_BRMA16_1_TRIGGER2_08");
    this.PreloadSound("VO_BRMA16_1_TRIGGER3_09");
    this.PreloadSound("VO_BRMA16_1_TURN1_02");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_BRMA16_1_RESPONSE_03",
            m_stringTag = "VO_BRMA16_1_RESPONSE_03"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM16_Atramedes.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator155() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM16_Atramedes.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator156() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM16_Atramedes.\u003CHandleMissionEventWithTiming\u003Ec__Iterator157() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM16_Atramedes.\u003CHandleGameOverWithTiming\u003Ec__Iterator158() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }
}
