﻿// Decompiled with JetBrains decompiler
// Type: NarrativeManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class NarrativeManager : MonoBehaviour
{
  private Map<string, AudioSource> m_preloadedSounds = new Map<string, AudioSource>();
  private Queue<int> m_onQuestCompleteDialogToShow = new Queue<int>();
  private const float MINIMUM_DISPLAY_TIME_FOR_BIG_QUOTE = 3f;
  private const float DELAY_TIME_FOR_QUEST_PROGRESS = 1.5f;
  private const float DELAY_TIME_FOR_QUEST_COMPLETE = 1.5f;
  private const float DELAY_TIME_FOR_AUTO_DESTROY_QUEST_RECEIVED = 3.8f;
  private const float DELAY_TIME_BEFORE_QUEST_DESTROY = 0.8f;
  private const float DELAY_TIME_FOR_AUTO_DESTROY_POST_DESTROY = 1.3f;
  private const float DELAY_TIME_BEFORE_SHOW_BANNER = 1f;
  private const float DEFAULT_QUOTE_CLIP_LENGTH = 3.5f;
  private static NarrativeManager s_instance;
  private int m_preloadsNeeded;
  private bool m_isBannerShowing;

  public void Awake()
  {
    NarrativeManager.s_instance = this;
    PopupDisplayManager.Get().RegisterCompletedQuestShownListener(new PopupDisplayManager.CompletedQuestShownCallback(NarrativeManager.s_instance.OnQuestCompleteShown));
  }

  public void Start()
  {
    this.StartCoroutine(this.WaitForAchievesThenInit());
  }

  public void OnDestroy()
  {
    if (!((Object) NarrativeManager.s_instance != (Object) null))
      return;
    AchieveManager.Get().RemoveAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(NarrativeManager.s_instance.OnAchievesUpdated));
    PopupDisplayManager.Get().RemoveCompletedQuestShownListener(new PopupDisplayManager.CompletedQuestShownCallback(NarrativeManager.s_instance.OnQuestCompleteShown));
    NarrativeManager.s_instance = (NarrativeManager) null;
  }

  public static NarrativeManager Get()
  {
    return NarrativeManager.s_instance;
  }

  public void OnQuestCompleteShown(int achieveId)
  {
    this.m_onQuestCompleteDialogToShow.Enqueue(achieveId);
  }

  public void OnWelcomeQuestsShown(List<Achievement> questsShown)
  {
    using (List<Achievement>.Enumerator enumerator = questsShown.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement current = enumerator.Current;
        if (current.AutoDestroy)
          this.StartCoroutine(this.DestroyAndReplaceQuest(current));
      }
    }
  }

  public bool HasQuestCompleteDialogToShow()
  {
    return this.m_onQuestCompleteDialogToShow.Count > 0;
  }

  [DebuggerHidden]
  public IEnumerator ShowOutstandingQuestCompleteDialog()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NarrativeManager.\u003CShowOutstandingQuestCompleteDialog\u003Ec__Iterator224() { \u003C\u003Ef__this = this };
  }

  private void OnQuestDialogCompleteBannerClosed()
  {
    this.m_isBannerShowing = false;
  }

  [DebuggerHidden]
  private IEnumerator WaitForAchievesThenInit()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NarrativeManager.\u003CWaitForAchievesThenInit\u003Ec__Iterator225() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator DestroyAndReplaceQuest(Achievement quest)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NarrativeManager.\u003CDestroyAndReplaceQuest\u003Ec__Iterator226() { quest = quest, \u003C\u0024\u003Equest = quest, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator PlayCharacterQuoteAndWait(string prefabName, string audioName, bool useAltSpeechBubble = false, NarrativeManager.CharacterQuotePlayedCallback callback = null, float waitTimeScale = 1f, bool persistCharacter = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NarrativeManager.\u003CPlayCharacterQuoteAndWait\u003Ec__Iterator227() { audioName = audioName, waitTimeScale = waitTimeScale, persistCharacter = persistCharacter, prefabName = prefabName, useAltSpeechBubble = useAltSpeechBubble, callback = callback, \u003C\u0024\u003EaudioName = audioName, \u003C\u0024\u003EwaitTimeScale = waitTimeScale, \u003C\u0024\u003EpersistCharacter = persistCharacter, \u003C\u0024\u003EprefabName = prefabName, \u003C\u0024\u003EuseAltSpeechBubble = useAltSpeechBubble, \u003C\u0024\u003Ecallback = callback, \u003C\u003Ef__this = this };
  }

  private void OnAchievesUpdated(List<Achievement> updatedAchieves, List<Achievement> completedAchieves, object userData)
  {
    this.PreloadQuestDialog(AchieveManager.Get().GetActiveQuests(false));
  }

  private void OnQuestProgressToastShown(int achieveId)
  {
    this.StartCoroutine(this.HandleOnQuestProgressToastShown(achieveId));
  }

  [DebuggerHidden]
  private IEnumerator HandleOnQuestProgressToastShown(int achieveId)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NarrativeManager.\u003CHandleOnQuestProgressToastShown\u003Ec__Iterator228() { achieveId = achieveId, \u003C\u0024\u003EachieveId = achieveId, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator PlayOnQuestCompleteShownDialog(int achieveId)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NarrativeManager.\u003CPlayOnQuestCompleteShownDialog\u003Ec__Iterator229() { achieveId = achieveId, \u003C\u0024\u003EachieveId = achieveId, \u003C\u003Ef__this = this };
  }

  private void OnWelcomeQuestNarrativeFinished()
  {
    WelcomeQuests.Get().ActivateClickCatcher();
  }

  private void PreloadActiveQuestDialog()
  {
    this.PreloadQuestDialog(AchieveManager.Get().GetActiveQuests(false));
  }

  private void PreloadQuestDialog(Achievement achievement)
  {
    if (achievement.QuestDialogId == 0)
      return;
    this.PreloadQuestDialog(achievement.OnReceivedDialog);
    this.PreloadQuestDialog(achievement.OnCompleteDialog);
    this.PreloadQuestDialog(achievement.OnProgress1Dialog);
    this.PreloadQuestDialog(achievement.OnProgress2Dialog);
  }

  private void PreloadQuestDialog(List<Achievement> activeAchievements)
  {
    using (List<Achievement>.Enumerator enumerator = activeAchievements.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.PreloadQuestDialog(enumerator.Current);
    }
  }

  private void PreloadQuestDialog(List<Achievement.QuestDialog> questDialog)
  {
    using (List<Achievement.QuestDialog>.Enumerator enumerator = questDialog.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Achievement.QuestDialog current = enumerator.Current;
        if (!string.IsNullOrEmpty(current.audioName))
          this.PreloadSound(current.audioName);
      }
    }
  }

  private void PreloadSound(string soundName)
  {
    if (this.CheckPreloadedSound(soundName))
      return;
    ++this.m_preloadsNeeded;
    AssetLoader.Get().LoadSound(soundName, new AssetLoader.GameObjectCallback(this.OnSoundLoaded), (object) null, false, SoundManager.Get().GetPlaceholderSound());
  }

  private void OnSoundLoaded(string name, GameObject go, object callbackData)
  {
    --this.m_preloadsNeeded;
    if ((Object) go == (Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("NarrativeManager.OnSoundLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      AudioSource component = go.GetComponent<AudioSource>();
      if ((Object) component == (Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("NarrativeManager.OnSoundLoaded() - ERROR \"{0}\" has no Spell component", (object) name));
      }
      else
      {
        if (this.CheckPreloadedSound(name))
          return;
        this.m_preloadedSounds.Add(name, component);
      }
    }
  }

  private void RemovePreloadedSound(string name)
  {
    this.m_preloadedSounds.Remove(name);
  }

  private bool CheckPreloadedSound(string name)
  {
    AudioSource audioSource;
    return this.m_preloadedSounds.TryGetValue(name, out audioSource);
  }

  private AudioSource GetPreloadedSound(string name)
  {
    AudioSource audioSource;
    if (this.m_preloadedSounds.TryGetValue(name, out audioSource))
      return audioSource;
    UnityEngine.Debug.LogError((object) string.Format("NarrativeManager.GetPreloadedSound() - \"{0}\" was not preloaded", (object) name));
    return (AudioSource) null;
  }

  private bool IsPreloadingAssets()
  {
    return this.m_preloadsNeeded > 0;
  }

  public delegate void CharacterQuotePlayedCallback();
}
