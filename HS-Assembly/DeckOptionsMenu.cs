﻿// Decompiled with JetBrains decompiler
// Type: DeckOptionsMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DeckOptionsMenu : MonoBehaviour
{
  public GameObject m_root;
  public UberText m_convertText;
  public PegUIElement m_renameButton;
  public PegUIElement m_deleteButton;
  public PegUIElement m_switchFormatButton;
  public PegUIElement m_retireButton;
  public GameObject m_top;
  public GameObject m_bottom;
  public HighlightState m_highlight;
  public Transform m_showBone;
  public Transform m_hideBone;
  public Transform[] m_buttonPositions;
  public Transform[] m_bottomPositions;
  public float[] m_topScales;
  private int m_buttonCount;
  private bool m_shown;
  private CollectionDeck m_deck;
  private CollectionDeckInfo m_deckInfo;

  public void Awake()
  {
    this.m_root.SetActive(false);
    if ((Object) this.m_renameButton != (Object) null)
      this.m_renameButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRenameButtonPressed));
    if ((Object) this.m_deleteButton != (Object) null)
      this.m_deleteButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDeleteButtonPressed));
    if ((Object) this.m_switchFormatButton != (Object) null)
      this.m_switchFormatButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnSwitchFormatButtonPressed));
    if (!((Object) this.m_retireButton != (Object) null))
      return;
    this.m_retireButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnRetireButtonPressed));
  }

  public void Show()
  {
    if (this.m_shown)
      return;
    iTween.Stop(this.gameObject);
    this.m_root.SetActive(true);
    this.SetSwitchFormatText(this.m_deck.IsWild);
    this.UpdateLayout();
    if (this.m_buttonCount == 0)
    {
      this.m_root.SetActive(false);
    }
    else
    {
      iTween.MoveTo(this.m_root, iTween.Hash((object) "position", (object) this.m_showBone.transform.position, (object) "time", (object) 0.35f, (object) "easeType", (object) iTween.EaseType.easeOutCubic, (object) "oncomplete", (object) "FinishShow", (object) "oncompletetarget", (object) this.gameObject));
      this.m_shown = true;
    }
  }

  private void FinishShow()
  {
  }

  public void Hide(bool animate = true)
  {
    if (!this.m_shown)
      return;
    iTween.Stop(this.gameObject);
    if (!animate)
    {
      this.m_root.SetActive(false);
    }
    else
    {
      this.m_root.SetActive(true);
      iTween.MoveTo(this.m_root, iTween.Hash((object) "position", (object) this.m_hideBone.transform.position, (object) "time", (object) 0.35f, (object) "easeType", (object) iTween.EaseType.easeOutCubic, (object) "oncomplete", (object) "FinishHide", (object) "oncompletetarget", (object) this.gameObject));
      this.m_shown = false;
    }
  }

  private void FinishHide()
  {
    if (this.m_shown)
      return;
    this.m_root.SetActive(false);
  }

  public void SetDeck(CollectionDeck deck)
  {
    this.m_deck = deck;
  }

  public void SetDeckInfo(CollectionDeckInfo deckInfo)
  {
    this.m_deckInfo = deckInfo;
  }

  private void OnRenameButtonPressed(UIEvent e)
  {
    this.m_deckInfo.Hide();
    CollectionDeckTray.Get().GetDecksContent().RenameCurrentlyEditingDeck();
  }

  private void OnDeleteButtonPressed(UIEvent e)
  {
    AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
    info.m_headerText = GameStrings.Get("GLUE_COLLECTION_DELETE_CONFIRM_HEADER");
    info.m_showAlertIcon = false;
    if (AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
    {
      info.m_text = GameStrings.Get("GLUE_COLLECTION_DELETE_CONFIRM_DESC");
      info.m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL;
      info.m_responseCallback = new AlertPopup.ResponseCallback(this.OnDeleteButtonConfirmationResponse);
    }
    else
    {
      info.m_text = GameStrings.Get("GLUE_COLLECTION_DELETE_UNAVAILABLE_DESC");
      info.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
      info.m_responseCallback = (AlertPopup.ResponseCallback) null;
    }
    this.m_deckInfo.Hide();
    DialogManager.Get().ShowPopup(info);
  }

  private void OnDeleteButtonConfirmationResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
      return;
    CollectionDeckTray.Get().DeleteEditingDeck(true);
    if (!(bool) ((Object) CollectionManagerDisplay.Get()))
      return;
    CollectionManagerDisplay.Get().OnDoneEditingDeck();
  }

  private void OnRetireButtonPressed(UIEvent e)
  {
    AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
    info.m_headerText = GameStrings.Get("GLUE_TAVERN_BRAWL_RETIRE_CONFIRM_HEADER");
    info.m_showAlertIcon = false;
    info.m_text = GameStrings.Get("GLUE_TAVERN_BRAWL_RETIRE_CONFIRM_DESC");
    info.m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL;
    info.m_responseCallback = new AlertPopup.ResponseCallback(this.OnRetireButtonConfirmationResponse);
    this.m_deckInfo.Hide();
    DialogManager.Get().ShowPopup(info);
  }

  private void OnRetireButtonConfirmationResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
      return;
    Network.TavernBrawlRetire();
  }

  private void OnClosePressed(UIEvent e)
  {
    Navigation.GoBack();
  }

  private void OverOffClicker(UIEvent e)
  {
    UnityEngine.Debug.Log((object) "OverOffClicker");
    this.Hide(true);
  }

  private void OnSwitchFormatButtonPressed(UIEvent e)
  {
    this.StartCoroutine(this.SwitchFormat());
  }

  [DebuggerHidden]
  private IEnumerator SwitchFormat()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckOptionsMenu.\u003CSwitchFormat\u003Ec__Iterator57()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void SetDeckFormat(bool isWild)
  {
    this.m_deck.IsWild = isWild;
    CollectionDeckTray.Get().GetEditingDeckBox().SetFormat(isWild);
    CollectionManager.Get().SetDeckRuleset(!isWild ? DeckRuleset.GetStandardRuleset() : DeckRuleset.GetWildRuleset());
    CollectionManagerDisplay.Get().m_pageManager.RefreshCurrentPageContents(CollectionPageManager.PageTransitionType.SINGLE_PAGE_RIGHT);
    CollectionManagerDisplay.Get().UpdateSetFilters(isWild, true, false);
    CollectionDeckTray.Get().GetCardsContent().UpdateCardList(true, (Actor) null);
    CollectionDeckTray.Get().GetCardsContent().UpdateTileVisuals();
    if (isWild)
      return;
    if (CollectionManager.Get().ShouldShowWildToStandardTutorial(true))
      CollectionManagerDisplay.Get().ShowStandardInfoTutorial(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS);
    this.StartCoroutine(CollectionManagerDisplay.Get().ShowDeckTemplateTipsIfNeeded());
  }

  private void SetSwitchFormatText(bool isWild)
  {
    this.m_convertText.Text = GameStrings.Get(!isWild ? "GLUE_COLLECTION_TO_WILD" : "GLUE_COLLECTION_TO_STANDARD");
  }

  private void UpdateLayout()
  {
    int buttonCount = this.GetButtonCount();
    if (buttonCount != this.m_buttonCount)
    {
      this.m_buttonCount = buttonCount;
      this.UpdateBackground();
    }
    this.UpdateButtons();
  }

  private void UpdateBackground()
  {
    if (this.m_buttonCount == 0)
      return;
    this.m_top.transform.transform.localScale = new Vector3(1f, 1f, this.m_topScales[this.m_buttonCount - 1]);
    this.m_bottom.transform.transform.position = this.m_bottomPositions[this.m_buttonCount - 1].position;
  }

  private void UpdateButtons()
  {
    int index = 0;
    bool flag1 = this.ShowConvertButton();
    bool flag2 = this.ShowRenameButton();
    bool flag3 = this.ShowDeleteButton();
    bool flag4 = this.ShowRetireButton();
    this.m_switchFormatButton.gameObject.SetActive(flag1);
    if (flag1)
    {
      if (this.m_deck.IsWild && (Object) this.m_highlight != (Object) null && CollectionManager.Get().ShouldShowWildToStandardTutorial(true))
        this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
      this.m_switchFormatButton.transform.position = this.m_buttonPositions[index].position;
      ++index;
    }
    this.m_renameButton.gameObject.SetActive(flag2);
    if (flag2)
    {
      this.m_renameButton.transform.position = this.m_buttonPositions[index].position;
      ++index;
    }
    this.m_deleteButton.gameObject.SetActive(flag3);
    if (flag3)
    {
      this.m_deleteButton.transform.position = this.m_buttonPositions[index].position;
      ++index;
    }
    this.m_retireButton.gameObject.SetActive(flag4);
    if (!flag4)
      return;
    this.m_retireButton.transform.position = this.m_buttonPositions[index].position;
    int num = index + 1;
  }

  private int GetButtonCount()
  {
    return 0 + (!this.ShowRenameButton() ? 0 : 1) + (!this.ShowDeleteButton() ? 0 : 1) + (!this.ShowConvertButton() ? 0 : 1) + (!this.ShowRetireButton() ? 0 : 1);
  }

  private bool ShowRenameButton()
  {
    CollectionDeck editedDeck = CollectionManager.Get().GetEditedDeck();
    if (editedDeck != null && editedDeck.Locked || SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
      return false;
    return UniversalInputManager.Get().IsTouchMode();
  }

  private bool ShowDeleteButton()
  {
    CollectionDeck editedDeck = CollectionManager.Get().GetEditedDeck();
    if (editedDeck != null && editedDeck.Locked)
      return false;
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
      return (bool) UniversalInputManager.UsePhoneUI;
    return UniversalInputManager.Get().IsTouchMode();
  }

  private bool ShowRetireButton()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL && TavernBrawlManager.Get().IsCurrentSeasonSessionBased)
    {
      TavernBrawlDisplay tavernBrawlDisplay = TavernBrawlDisplay.Get();
      if ((Object) tavernBrawlDisplay != (Object) null && !tavernBrawlDisplay.IsInDeckEditMode())
        return true;
    }
    return false;
  }

  private bool ShowConvertButton()
  {
    return SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL && CollectionManager.Get().ShouldAccountSeeStandardWild() && CollectionManagerDisplay.Get().GetViewMode() != CollectionManagerDisplay.ViewMode.DECK_TEMPLATE;
  }
}
