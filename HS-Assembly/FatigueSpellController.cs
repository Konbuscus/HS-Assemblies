﻿// Decompiled with JetBrains decompiler
// Type: FatigueSpellController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class FatigueSpellController : SpellController
{
  private static readonly Vector3 FATIGUE_ACTOR_START_SCALE = new Vector3(0.88f, 0.88f, 0.88f);
  private static readonly Vector3 FATIGUE_ACTOR_FINAL_SCALE = Vector3.one;
  private static readonly Vector3 FATIGUE_ACTOR_INITIAL_LOCAL_ROTATION = new Vector3(270f, 270f, 0.0f);
  private static readonly Vector3 FATIGUE_ACTOR_FINAL_LOCAL_ROTATION = Vector3.zero;
  private const float FATIGUE_DRAW_ANIM_TIME = 1.2f;
  private const float FATIGUE_DRAW_SCALE_TIME = 1f;
  private const float FATIGUE_HOLD_TIME = 0.8f;
  private Network.HistTagChange m_fatigueTagChange;
  private Actor m_fatigueActor;

  protected override bool AddPowerSourceAndTargets(PowerTaskList taskList)
  {
    if (!this.HasSourceCard(taskList))
      return false;
    this.m_fatigueTagChange = (Network.HistTagChange) null;
    List<PowerTask> taskList1 = this.m_taskList.GetTaskList();
    for (int index = 0; index < taskList1.Count; ++index)
    {
      Network.PowerHistory power = taskList1[index].GetPower();
      if (power.Type == Network.PowerType.TAG_CHANGE)
      {
        Network.HistTagChange histTagChange = (Network.HistTagChange) power;
        if (histTagChange.Tag == 22)
          this.m_fatigueTagChange = histTagChange;
      }
    }
    if (this.m_fatigueTagChange == null)
      return false;
    this.SetSource(taskList.GetSourceEntity().GetCard());
    return true;
  }

  protected override void OnProcessTaskList()
  {
    AssetLoader.Get().LoadActor("Card_Hand_Fatigue", new AssetLoader.GameObjectCallback(this.OnFatigueActorLoaded), (object) null, false);
  }

  private void OnFatigueActorLoaded(string actorName, GameObject actorObject, object callbackData)
  {
    if ((Object) actorObject == (Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("FatigueSpellController.OnFatigueActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
      this.DoFinishFatigue();
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((Object) component == (Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("FatigueSpellController.OnFatigueActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
        this.DoFinishFatigue();
      }
      else
      {
        Player.Side controllerSide = this.GetSource().GetControllerSide();
        bool flag = controllerSide == Player.Side.FRIENDLY;
        this.m_fatigueActor = component;
        UberText nameText = this.m_fatigueActor.GetNameText();
        if ((Object) nameText != (Object) null)
          nameText.Text = GameStrings.Get("GAMEPLAY_FATIGUE_TITLE");
        UberText powersText = this.m_fatigueActor.GetPowersText();
        if ((Object) powersText != (Object) null)
          powersText.Text = GameStrings.Format("GAMEPLAY_FATIGUE_TEXT", (object) this.m_fatigueTagChange.Value);
        component.SetCardBackSideOverride(new Player.Side?(controllerSide));
        component.UpdateCardBack();
        ZoneDeck zoneDeck = !flag ? GameState.Get().GetOpposingSidePlayer().GetDeckZone() : GameState.Get().GetFriendlySidePlayer().GetDeckZone();
        zoneDeck.DoFatigueGlow();
        this.m_fatigueActor.transform.localEulerAngles = FatigueSpellController.FATIGUE_ACTOR_INITIAL_LOCAL_ROTATION;
        this.m_fatigueActor.transform.localScale = FatigueSpellController.FATIGUE_ACTOR_START_SCALE;
        this.m_fatigueActor.transform.position = zoneDeck.transform.position;
        iTween.MoveTo(this.m_fatigueActor.gameObject, iTween.Hash((object) "path", (object) new Vector3[3]
        {
          this.m_fatigueActor.transform.position,
          new Vector3(this.m_fatigueActor.transform.position.x, this.m_fatigueActor.transform.position.y + 3.6f, this.m_fatigueActor.transform.position.z),
          Board.Get().FindBone("FatigueCardBone").position
        }, (object) "time", (object) 1.2f, (object) "easetype", (object) iTween.EaseType.easeInSineOutExpo));
        iTween.RotateTo(this.m_fatigueActor.gameObject, iTween.Hash((object) "rotation", (object) FatigueSpellController.FATIGUE_ACTOR_FINAL_LOCAL_ROTATION, (object) "time", (object) 1.2f, (object) "delay", (object) 0.15f));
        iTween.ScaleTo(this.m_fatigueActor.gameObject, FatigueSpellController.FATIGUE_ACTOR_FINAL_SCALE, 1f);
        this.StartCoroutine(this.WaitThenFinishFatigue(0.8f));
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitThenFinishFatigue(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FatigueSpellController.\u003CWaitThenFinishFatigue\u003Ec__Iterator249() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds, \u003C\u003Ef__this = this };
  }

  private void DoFinishFatigue()
  {
    Spell spell = this.GetSource().GetActor().GetSpell(SpellType.FATIGUE_DEATH);
    spell.AddFinishedCallback(new Spell.FinishedCallback(this.OnFatigueDamageFinished));
    spell.ActivateState(SpellStateType.BIRTH);
  }

  private void OnFatigueDamageFinished(Spell spell, object userData)
  {
    spell.RemoveFinishedCallback(new Spell.FinishedCallback(this.OnFatigueDamageFinished));
    if ((Object) this.m_fatigueActor == (Object) null)
    {
      this.OnFinishedTaskList();
    }
    else
    {
      Spell spell1 = this.m_fatigueActor.GetSpell(SpellType.DEATH);
      if ((Object) spell1 == (Object) null)
      {
        this.OnFinishedTaskList();
      }
      else
      {
        Actor fatigueActor = this.m_fatigueActor;
        this.m_fatigueActor = (Actor) null;
        spell1.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnFatigueDeathSpellFinished), (object) fatigueActor);
        spell1.Activate();
        this.OnFinishedTaskList();
      }
    }
  }

  private void OnFatigueDeathSpellFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    Actor actor = (Actor) userData;
    if ((Object) actor != (Object) null)
      actor.Destroy();
    this.OnFinished();
  }
}
