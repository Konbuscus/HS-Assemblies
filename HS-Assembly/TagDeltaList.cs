﻿// Decompiled with JetBrains decompiler
// Type: TagDeltaList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class TagDeltaList
{
  private List<TagDelta> m_deltas = new List<TagDelta>();

  public int Count
  {
    get
    {
      return this.m_deltas.Count;
    }
  }

  public TagDelta this[int index]
  {
    get
    {
      return this.m_deltas[index];
    }
  }

  public void Add(int tag, int prev, int curr)
  {
    this.m_deltas.Add(new TagDelta()
    {
      tag = tag,
      oldValue = prev,
      newValue = curr
    });
  }
}
