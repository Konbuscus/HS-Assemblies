﻿// Decompiled with JetBrains decompiler
// Type: ChangedCardMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ChangedCardMgr
{
  private static readonly ChangedCardMgr s_instance = new ChangedCardMgr();
  private List<ChangedCardMgr.TrackedCard> m_cards = new List<ChangedCardMgr.TrackedCard>();
  public const int MaxViewCount = 1;
  private bool m_isInitialized;

  private ChangedCardMgr.TrackedCard FindCard(int dbId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.m_cards.Find(new Predicate<ChangedCardMgr.TrackedCard>(new ChangedCardMgr.\u003CFindCard\u003Ec__AnonStorey45C() { dbId = dbId }.\u003C\u003Em__32C));
  }

  private void AddCard(ChangedCardMgr.TrackedCard card)
  {
    ChangedCardMgr.TrackedCard card1 = this.FindCard(card.DbId);
    if (card1 != null)
    {
      if (card1.Index >= card.Index)
        return;
      card1.Index = card.Index;
      card1.Count = card.Count;
    }
    else
      this.m_cards.Add(card);
  }

  private void Load()
  {
    this.m_isInitialized = true;
    string str1 = Options.Get().GetString(Option.CHANGED_CARDS_DATA);
    if (string.IsNullOrEmpty(str1))
      return;
    string str2 = str1;
    char[] chArray = new char[1]{ '-' };
    foreach (string str3 in str2.Split(chArray))
    {
      if (!string.IsNullOrEmpty(str3))
      {
        string[] strArray = str3.Split(',');
        if (strArray.Length == 3)
        {
          for (int index = 0; index < 3; ++index)
          {
            if (!string.IsNullOrEmpty(strArray[index]))
              ;
          }
          int val1;
          int val2;
          int val3;
          if (GeneralUtils.TryParseInt(strArray[0], out val1) && GeneralUtils.TryParseInt(strArray[1], out val2) && GeneralUtils.TryParseInt(strArray[2], out val3))
            this.AddCard(new ChangedCardMgr.TrackedCard()
            {
              Index = val1,
              DbId = val2,
              Count = val3
            });
        }
      }
    }
  }

  private void Save()
  {
    StringBuilder stringBuilder = new StringBuilder();
    using (List<ChangedCardMgr.TrackedCard>.Enumerator enumerator = this.m_cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ChangedCardMgr.TrackedCard current = enumerator.Current;
        stringBuilder.Append(current.ToString());
      }
    }
    Options.Get().SetString(Option.CHANGED_CARDS_DATA, stringBuilder.ToString());
    Log.ChangedCards.Print("Saved CHANGED_CARDS_DATA " + stringBuilder.ToString());
  }

  public void Initialize()
  {
    if (this.m_isInitialized)
      return;
    this.Load();
  }

  public bool AllowCard(int index, int dbId)
  {
    if (!this.m_isInitialized)
    {
      Debug.LogWarning((object) "ChangedCardMgr.AllowCard called before Initialize!");
      return true;
    }
    ChangedCardMgr.TrackedCard card = this.FindCard(dbId);
    if (card == null)
    {
      card = new ChangedCardMgr.TrackedCard();
      card.Index = index;
      card.DbId = dbId;
      card.Count = 0;
      this.AddCard(card);
    }
    if (card.Index < index)
    {
      Log.ChangedCards.PrintWarning("Updating to a newer change version for card " + (object) card);
      card.Index = index;
      card.Count = 0;
    }
    if (index != card.Index || card.Count >= 1)
      return false;
    ++card.Count;
    this.Save();
    return true;
  }

  public static ChangedCardMgr Get()
  {
    return ChangedCardMgr.s_instance;
  }

  private class TrackedCard
  {
    public int Index { get; set; }

    public int DbId { get; set; }

    public int Count { get; set; }

    public override string ToString()
    {
      return string.Format("{0},{1},{2}-", (object) this.Index, (object) this.DbId, (object) this.Count);
    }
  }
}
