﻿// Decompiled with JetBrains decompiler
// Type: GameDbf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GameDbf
{
  public static Dbf<AchieveDbfRecord> Achieve;
  public static Dbf<AccountLicenseDbfRecord> AccountLicense;
  public static Dbf<AdventureDbfRecord> Adventure;
  public static Dbf<AdventureDataDbfRecord> AdventureData;
  public static Dbf<AdventureMissionDbfRecord> AdventureMission;
  public static Dbf<AdventureModeDbfRecord> AdventureMode;
  public static Dbf<BannerDbfRecord> Banner;
  public static Dbf<BoardDbfRecord> Board;
  public static Dbf<BoosterDbfRecord> Booster;
  public static Dbf<CardTagDbfRecord> CardTag;
  public static Dbf<CardDbfRecord> Card;
  public static Dbf<CardBackDbfRecord> CardBack;
  public static Dbf<CardHeroDbfRecord> CardHero;
  public static Dbf<DeckDbfRecord> Deck;
  public static Dbf<DeckRulesetDbfRecord> DeckRuleset;
  public static Dbf<DeckRulesetRuleDbfRecord> DeckRulesetRule;
  public static Dbf<DeckRulesetRuleSubsetDbfRecord> DeckRulesetRuleSubset;
  public static Dbf<DeckCardDbfRecord> DeckCard;
  public static Dbf<DeckTemplateDbfRecord> DeckTemplate;
  public static Dbf<FixedRewardDbfRecord> FixedReward;
  public static Dbf<FixedRewardActionDbfRecord> FixedRewardAction;
  public static Dbf<FixedRewardMapDbfRecord> FixedRewardMap;
  public static Dbf<HiddenLicenseDbfRecord> HiddenLicense;
  public static Dbf<QuestDialogDbfRecord> QuestDialog;
  public static Dbf<QuestDialogOnCompleteDbfRecord> QuestDialogOnComplete;
  public static Dbf<QuestDialogOnProgress1DbfRecord> QuestDialogOnProgress1;
  public static Dbf<QuestDialogOnProgress2DbfRecord> QuestDialogOnProgress2;
  public static Dbf<QuestDialogOnReceivedDbfRecord> QuestDialogOnReceived;
  public static Dbf<RotatedItemDbfRecord> RotatedItem;
  public static Dbf<ScenarioDbfRecord> Scenario;
  public static Dbf<ScoreLabelDbfRecord> ScoreLabel;
  public static Dbf<SeasonDbfRecord> Season;
  public static Dbf<SubsetDbfRecord> Subset;
  public static Dbf<SubsetCardDbfRecord> SubsetCard;
  public static Dbf<TavernBrawlTicketDbfRecord> TavernBrawlTicket;
  public static Dbf<WingDbfRecord> Wing;
  public static Dbf<MultiClassGroupDbfRecord> MultiClassGroup;
  public static Dbf<KeywordTextDbfRecord> KeywordText;
  private static GameDbfIndex s_index;

  public static GameDbfIndex GetIndex()
  {
    if (GameDbf.s_index == null)
      GameDbf.s_index = new GameDbfIndex();
    return GameDbf.s_index;
  }

  public static void LoadXml()
  {
    GameDbf.Load(true);
  }

  public static void Load(bool useXmlLoading = false)
  {
    if (GameDbf.s_index == null)
      GameDbf.s_index = new GameDbfIndex();
    else
      GameDbf.s_index.Initialize();
    if (ApplicationMgr.UsingStandaloneLocalData())
      useXmlLoading = true;
    DbfFormat format = !useXmlLoading ? DbfFormat.ASSET : DbfFormat.XML;
    DbfShared.Reset();
    Log.Dbf.Print("Loading DBFS with format={0}", (object) format);
    GameDbf.Achieve = Dbf<AchieveDbfRecord>.Load("ACHIEVE", format);
    GameDbf.AccountLicense = Dbf<AccountLicenseDbfRecord>.Load("ACCOUNT_LICENSE", format);
    GameDbf.Adventure = Dbf<AdventureDbfRecord>.Load("ADVENTURE", format);
    GameDbf.AdventureData = Dbf<AdventureDataDbfRecord>.Load("ADVENTURE_DATA", format);
    GameDbf.AdventureMission = Dbf<AdventureMissionDbfRecord>.Load("ADVENTURE_MISSION", format);
    GameDbf.AdventureMode = Dbf<AdventureModeDbfRecord>.Load("ADVENTURE_MODE", format);
    GameDbf.Banner = Dbf<BannerDbfRecord>.Load("BANNER", format);
    GameDbf.Booster = Dbf<BoosterDbfRecord>.Load("BOOSTER", format);
    GameDbf.Board = Dbf<BoardDbfRecord>.Load("BOARD", format);
    GameDbf.CardTag = Dbf<CardTagDbfRecord>.Load("CARD_TAG", new Dbf<CardTagDbfRecord>.RecordAddedListener(GameDbf.s_index.OnCardTagAdded), new Dbf<CardTagDbfRecord>.RecordsRemovedListener(GameDbf.s_index.OnCardTagRemoved), format);
    GameDbf.Card = Dbf<CardDbfRecord>.Load("CARD", new Dbf<CardDbfRecord>.RecordAddedListener(GameDbf.s_index.OnCardAdded), new Dbf<CardDbfRecord>.RecordsRemovedListener(GameDbf.s_index.OnCardRemoved), format);
    GameDbf.CardBack = Dbf<CardBackDbfRecord>.Load("CARD_BACK", format);
    GameDbf.CardHero = Dbf<CardHeroDbfRecord>.Load("CARD_HERO", format);
    GameDbf.Deck = Dbf<DeckDbfRecord>.Load("DECK", format);
    GameDbf.DeckRuleset = Dbf<DeckRulesetDbfRecord>.Load("DECK_RULESET", format);
    GameDbf.DeckRulesetRule = Dbf<DeckRulesetRuleDbfRecord>.Load("DECK_RULESET_RULE", new Dbf<DeckRulesetRuleDbfRecord>.RecordAddedListener(GameDbf.s_index.OnDeckRulesetRuleAdded), new Dbf<DeckRulesetRuleDbfRecord>.RecordsRemovedListener(GameDbf.s_index.OnDeckRulesetRuleRemoved), format);
    GameDbf.DeckRulesetRuleSubset = Dbf<DeckRulesetRuleSubsetDbfRecord>.Load("DECK_RULESET_RULE_SUBSET", new Dbf<DeckRulesetRuleSubsetDbfRecord>.RecordAddedListener(GameDbf.s_index.OnDeckRulesetRuleSubsetAdded), new Dbf<DeckRulesetRuleSubsetDbfRecord>.RecordsRemovedListener(GameDbf.s_index.OnDeckRulesetRuleSubsetRemoved), format);
    GameDbf.DeckCard = Dbf<DeckCardDbfRecord>.Load("DECK_CARD", format);
    GameDbf.DeckTemplate = Dbf<DeckTemplateDbfRecord>.Load("DECK_TEMPLATE", format);
    GameDbf.FixedReward = Dbf<FixedRewardDbfRecord>.Load("FIXED_REWARD", format);
    GameDbf.FixedRewardAction = Dbf<FixedRewardActionDbfRecord>.Load("FIXED_REWARD_ACTION", new Dbf<FixedRewardActionDbfRecord>.RecordAddedListener(GameDbf.s_index.OnFixedRewardActionAdded), new Dbf<FixedRewardActionDbfRecord>.RecordsRemovedListener(GameDbf.s_index.OnFixedRewardActionRemoved), format);
    GameDbf.FixedRewardMap = Dbf<FixedRewardMapDbfRecord>.Load("FIXED_REWARD_MAP", new Dbf<FixedRewardMapDbfRecord>.RecordAddedListener(GameDbf.s_index.OnFixedRewardMapAdded), new Dbf<FixedRewardMapDbfRecord>.RecordsRemovedListener(GameDbf.s_index.OnFixedRewardMapRemoved), format);
    GameDbf.HiddenLicense = Dbf<HiddenLicenseDbfRecord>.Load("HIDDEN_LICENSE", format);
    GameDbf.QuestDialog = Dbf<QuestDialogDbfRecord>.Load("QUEST_DIALOG", format);
    GameDbf.QuestDialogOnComplete = Dbf<QuestDialogOnCompleteDbfRecord>.Load("QUEST_DIALOG_ON_COMPLETE", format);
    GameDbf.QuestDialogOnProgress1 = Dbf<QuestDialogOnProgress1DbfRecord>.Load("QUEST_DIALOG_ON_PROGRESS1", format);
    GameDbf.QuestDialogOnProgress2 = Dbf<QuestDialogOnProgress2DbfRecord>.Load("QUEST_DIALOG_ON_PROGRESS2", format);
    GameDbf.QuestDialogOnReceived = Dbf<QuestDialogOnReceivedDbfRecord>.Load("QUEST_DIALOG_ON_RECEIVED", format);
    GameDbf.RotatedItem = Dbf<RotatedItemDbfRecord>.Load("ROTATED_ITEM", format);
    GameDbf.Scenario = Dbf<ScenarioDbfRecord>.Load("SCENARIO", format);
    GameDbf.ScoreLabel = Dbf<ScoreLabelDbfRecord>.Load("SCORE_LABEL", format);
    GameDbf.Season = Dbf<SeasonDbfRecord>.Load("SEASON", format);
    GameDbf.Subset = Dbf<SubsetDbfRecord>.Load("SUBSET", format);
    GameDbf.SubsetCard = Dbf<SubsetCardDbfRecord>.Load("SUBSET_CARD", new Dbf<SubsetCardDbfRecord>.RecordAddedListener(GameDbf.s_index.OnSubsetCardAdded), new Dbf<SubsetCardDbfRecord>.RecordsRemovedListener(GameDbf.s_index.OnSubsetCardRemoved), format);
    GameDbf.TavernBrawlTicket = Dbf<TavernBrawlTicketDbfRecord>.Load("TAVERN_BRAWL_TICKET", format);
    GameDbf.Wing = Dbf<WingDbfRecord>.Load("WING", format);
    GameDbf.MultiClassGroup = Dbf<MultiClassGroupDbfRecord>.Load("MULTI_CLASS_GROUP", format);
    GameDbf.KeywordText = Dbf<KeywordTextDbfRecord>.Load("KEYWORD_TEXT", format);
  }

  public static void Reload(string name, string xml)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (GameDbf.\u003C\u003Ef__switch\u0024mapC7 == null)
      {
        // ISSUE: reference to a compiler-generated field
        GameDbf.\u003C\u003Ef__switch\u0024mapC7 = new Dictionary<string, int>(2)
        {
          {
            "ACHIEVE",
            0
          },
          {
            "CARD_BACK",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (GameDbf.\u003C\u003Ef__switch\u0024mapC7.TryGetValue(key, out num))
      {
        if (num != 0)
        {
          if (num == 1)
          {
            GameDbf.CardBack = Dbf<CardBackDbfRecord>.Load(name, DbfFormat.XML);
            if (!((Object) CardBackManager.Get() != (Object) null))
              return;
            CardBackManager.Get().InitCardBackData();
            return;
          }
        }
        else
        {
          GameDbf.Achieve = Dbf<AchieveDbfRecord>.Load(name, DbfFormat.XML);
          if (AchieveManager.Get() == null)
            return;
          AchieveManager.Get().InitAchieveManager();
          return;
        }
      }
    }
    Error.AddDevFatal("Reloading {0} is unsupported", (object) name);
  }
}
