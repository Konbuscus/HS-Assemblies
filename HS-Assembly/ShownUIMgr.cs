﻿// Decompiled with JetBrains decompiler
// Type: ShownUIMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ShownUIMgr : MonoBehaviour
{
  private ShownUIMgr.UI_WINDOW m_shownUI;
  private static ShownUIMgr s_instance;

  private void Awake()
  {
    ShownUIMgr.s_instance = this;
  }

  private void OnDestroy()
  {
    ShownUIMgr.s_instance = (ShownUIMgr) null;
  }

  public static ShownUIMgr Get()
  {
    return ShownUIMgr.s_instance;
  }

  public void SetShownUI(ShownUIMgr.UI_WINDOW uiWindow)
  {
    this.m_shownUI = uiWindow;
  }

  public ShownUIMgr.UI_WINDOW GetShownUI()
  {
    return this.m_shownUI;
  }

  public bool HasShownUI()
  {
    return this.m_shownUI != ShownUIMgr.UI_WINDOW.NONE;
  }

  public void ClearShownUI()
  {
    this.m_shownUI = ShownUIMgr.UI_WINDOW.NONE;
  }

  public enum UI_WINDOW
  {
    NONE,
    GENERAL_STORE,
    ARENA_STORE,
    TAVERN_BRAWL_STORE,
    QUEST_LOG,
  }
}
