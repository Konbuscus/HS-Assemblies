﻿// Decompiled with JetBrains decompiler
// Type: DbfUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System.Collections.Generic;

public static class DbfUtils
{
  public static ScenarioDbfRecord ConvertFromProtobuf(ScenarioDbRecord protoScenario)
  {
    if (protoScenario == null)
      return (ScenarioDbfRecord) null;
    ScenarioDbfRecord scenarioDbfRecord = new ScenarioDbfRecord();
    scenarioDbfRecord.SetID(protoScenario.Id);
    scenarioDbfRecord.SetNoteDesc(protoScenario.NoteDesc);
    scenarioDbfRecord.SetPlayers(protoScenario.NumPlayers);
    scenarioDbfRecord.SetPlayer1HeroCardId((int) protoScenario.Player1HeroCardId);
    scenarioDbfRecord.SetPlayer2HeroCardId((int) protoScenario.Player2HeroCardId);
    scenarioDbfRecord.SetIsExpert(protoScenario.IsExpert);
    scenarioDbfRecord.SetIsCoop(protoScenario.HasIsCoop && protoScenario.IsCoop);
    scenarioDbfRecord.SetAdventureId(protoScenario.AdventureId);
    if (protoScenario.HasAdventureModeId)
      scenarioDbfRecord.SetModeId(protoScenario.AdventureModeId);
    scenarioDbfRecord.SetWingId(protoScenario.WingId);
    scenarioDbfRecord.SetSortOrder(protoScenario.SortOrder);
    if (protoScenario.HasClientPlayer2HeroCardId)
      scenarioDbfRecord.SetClientPlayer2HeroCardId((int) protoScenario.ClientPlayer2HeroCardId);
    scenarioDbfRecord.SetTbTexture(protoScenario.TavernBrawlTexture);
    scenarioDbfRecord.SetTbTexturePhone(protoScenario.TavernBrawlTexturePhone);
    if (protoScenario.HasTavernBrawlTexturePhoneOffset)
      scenarioDbfRecord.SetTbTexturePhoneOffsetY(protoScenario.TavernBrawlTexturePhoneOffset.Y);
    DbfUtils.AddLocStrings((DbfRecord) scenarioDbfRecord, protoScenario.Strings);
    if (protoScenario.HasDeckRulesetId)
      scenarioDbfRecord.SetDeckRulesetId(protoScenario.DeckRulesetId);
    return scenarioDbfRecord;
  }

  public static DeckRulesetDbfRecord ConvertFromProtobuf(DeckRulesetDbRecord proto)
  {
    if (proto == null)
      return (DeckRulesetDbfRecord) null;
    DeckRulesetDbfRecord rulesetDbfRecord = new DeckRulesetDbfRecord();
    rulesetDbfRecord.SetID(proto.Id);
    return rulesetDbfRecord;
  }

  public static DeckRulesetRuleDbfRecord ConvertFromProtobuf(DeckRulesetRuleDbRecord proto, out List<int> outTargetSubsetIds)
  {
    outTargetSubsetIds = (List<int>) null;
    if (proto == null)
      return (DeckRulesetRuleDbfRecord) null;
    DeckRulesetRuleDbfRecord rulesetRuleDbfRecord = new DeckRulesetRuleDbfRecord();
    rulesetRuleDbfRecord.SetID(proto.Id);
    rulesetRuleDbfRecord.SetDeckRulesetId(proto.DeckRulesetId);
    if (proto.HasAppliesToSubsetId)
      rulesetRuleDbfRecord.SetAppliesToSubsetId(proto.AppliesToSubsetId);
    if (proto.HasAppliesToIsNot)
      rulesetRuleDbfRecord.SetAppliesToIsNot(proto.AppliesToIsNot);
    rulesetRuleDbfRecord.SetRuleType(proto.RuleType);
    rulesetRuleDbfRecord.SetRuleIsNot(proto.RuleIsNot);
    if (proto.HasMinValue)
      rulesetRuleDbfRecord.SetMinValue(proto.MinValue);
    if (proto.HasMaxValue)
      rulesetRuleDbfRecord.SetMaxValue(proto.MaxValue);
    if (proto.HasTag)
      rulesetRuleDbfRecord.SetTag(proto.Tag);
    if (proto.HasTagMinValue)
      rulesetRuleDbfRecord.SetTagMinValue(proto.TagMinValue);
    if (proto.HasTagMaxValue)
      rulesetRuleDbfRecord.SetTagMaxValue(proto.TagMaxValue);
    if (proto.HasStringValue)
      rulesetRuleDbfRecord.SetStringValue(proto.StringValue);
    outTargetSubsetIds = proto.TargetSubsetIds;
    DbfUtils.AddLocStrings((DbfRecord) rulesetRuleDbfRecord, proto.Strings);
    return rulesetRuleDbfRecord;
  }

  private static void AddLocStrings(DbfRecord record, List<LocalizedString> protoStrings)
  {
    using (List<LocalizedString>.Enumerator enumerator = protoStrings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LocalizedString current = enumerator.Current;
        DbfLocValue dbfLocValue = new DbfLocValue();
        string key = current.Key;
        for (int index = 0; index < current.Values.Count; ++index)
        {
          LocalizedStringValue localizedStringValue = current.Values[index];
          Locale locale = (Locale) localizedStringValue.Locale;
          dbfLocValue.SetString(locale, TextUtils.DecodeWhitespaces(localizedStringValue.Value));
        }
        record.SetVar(key, (object) dbfLocValue);
      }
    }
  }
}
