﻿// Decompiled with JetBrains decompiler
// Type: PlatformDependentValue`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class PlatformDependentValue<T>
{
  [SerializeField]
  private T[] settings = new T[14];
  [SerializeField]
  private bool[] isSet = new bool[14];
  private bool resolved;
  private T result;
  [SerializeField]
  private PlatformCategory type;
  private T defaultValue;

  public T PC
  {
    get
    {
      return this.GetValue(PlatformSettingType.PC);
    }
    set
    {
      this.SetValue(PlatformSettingType.PC, value);
    }
  }

  public T Mac
  {
    get
    {
      return this.GetValue(PlatformSettingType.Mac);
    }
    set
    {
      this.SetValue(PlatformSettingType.Mac, value);
    }
  }

  public T iOS
  {
    get
    {
      return this.GetValue(PlatformSettingType.iOS);
    }
    set
    {
      this.SetValue(PlatformSettingType.iOS, value);
    }
  }

  public T Android
  {
    get
    {
      return this.GetValue(PlatformSettingType.Android);
    }
    set
    {
      this.SetValue(PlatformSettingType.Android, value);
    }
  }

  public T Tablet
  {
    get
    {
      return this.GetValue(PlatformSettingType.Tablet);
    }
    set
    {
      this.SetValue(PlatformSettingType.Tablet, value);
    }
  }

  public T MiniTablet
  {
    get
    {
      return this.GetValue(PlatformSettingType.MiniTablet);
    }
    set
    {
      this.SetValue(PlatformSettingType.MiniTablet, value);
    }
  }

  public T Phone
  {
    get
    {
      return this.GetValue(PlatformSettingType.Phone);
    }
    set
    {
      this.SetValue(PlatformSettingType.Phone, value);
    }
  }

  public T Mouse
  {
    get
    {
      return this.GetValue(PlatformSettingType.Mouse);
    }
    set
    {
      this.SetValue(PlatformSettingType.Mouse, value);
    }
  }

  public T Touch
  {
    get
    {
      return this.GetValue(PlatformSettingType.Touch);
    }
    set
    {
      this.SetValue(PlatformSettingType.Touch, value);
    }
  }

  public T LowMemory
  {
    get
    {
      return this.GetValue(PlatformSettingType.LowMemory);
    }
    set
    {
      this.SetValue(PlatformSettingType.LowMemory, value);
    }
  }

  public T MediumMemory
  {
    get
    {
      return this.GetValue(PlatformSettingType.MediumMemory);
    }
    set
    {
      this.SetValue(PlatformSettingType.MediumMemory, value);
    }
  }

  public T HighMemory
  {
    get
    {
      return this.GetValue(PlatformSettingType.HighMemory);
    }
    set
    {
      this.SetValue(PlatformSettingType.HighMemory, value);
    }
  }

  public T NormalScreenDensity
  {
    get
    {
      return this.GetValue(PlatformSettingType.NormalScreenDensity);
    }
    set
    {
      this.SetValue(PlatformSettingType.NormalScreenDensity, value);
    }
  }

  public T HighScreenDensity
  {
    get
    {
      return this.GetValue(PlatformSettingType.HighScreenDensity);
    }
    set
    {
      this.SetValue(PlatformSettingType.HighScreenDensity, value);
    }
  }

  private T Value
  {
    get
    {
      if (this.resolved)
        return this.result;
      switch (this.type)
      {
        case PlatformCategory.OS:
          this.result = this.GetOSSetting(PlatformSettings.OS);
          break;
        case PlatformCategory.Screen:
          this.result = this.GetScreenSetting(PlatformSettings.Screen);
          break;
        case PlatformCategory.Memory:
          this.result = this.GetMemorySetting(PlatformSettings.Memory);
          break;
        case PlatformCategory.Input:
          this.result = this.GetInputSetting(PlatformSettings.Input);
          break;
      }
      this.resolved = true;
      return this.result;
    }
  }

  public PlatformDependentValue(PlatformCategory t)
  {
    this.type = t;
    this.InitSettingsMap();
  }

  public static implicit operator T(PlatformDependentValue<T> val)
  {
    return val.Value;
  }

  public void Reset()
  {
    this.resolved = false;
  }

  private void InitSettingsMap()
  {
    for (int index = 0; index < 14; ++index)
    {
      this.settings[index] = default (T);
      this.isSet[index] = false;
    }
  }

  private void SetValue(PlatformSettingType type, T value)
  {
    this.settings[(int) type] = value;
    this.isSet[(int) type] = true;
  }

  public T GetValue(PlatformSettingType type)
  {
    return this.settings[(int) type];
  }

  public bool IsSet(PlatformSettingType type)
  {
    return this.isSet[(int) type];
  }

  private T GetOSSetting(OSCategory os)
  {
    switch (os)
    {
      case OSCategory.PC:
        if (this.IsSet(PlatformSettingType.PC))
          return this.GetValue(PlatformSettingType.PC);
        break;
      case OSCategory.Mac:
        if (this.IsSet(PlatformSettingType.Mac))
          return this.GetValue(PlatformSettingType.Mac);
        return this.GetOSSetting(OSCategory.PC);
      case OSCategory.iOS:
        if (this.IsSet(PlatformSettingType.iOS))
          return this.GetValue(PlatformSettingType.iOS);
        return this.GetOSSetting(OSCategory.PC);
      case OSCategory.Android:
        if (this.IsSet(PlatformSettingType.Android))
          return this.GetValue(PlatformSettingType.Android);
        return this.GetOSSetting(OSCategory.PC);
    }
    Debug.LogError((object) "Could not find OS dependent value");
    return default (T);
  }

  private T GetScreenSetting(ScreenCategory screen)
  {
    switch (screen)
    {
      case ScreenCategory.Phone:
        if (this.IsSet(PlatformSettingType.Phone))
          return this.GetValue(PlatformSettingType.Phone);
        return this.GetScreenSetting(ScreenCategory.Tablet);
      case ScreenCategory.MiniTablet:
        if (this.IsSet(PlatformSettingType.MiniTablet))
          return this.GetValue(PlatformSettingType.MiniTablet);
        return this.GetScreenSetting(ScreenCategory.Tablet);
      case ScreenCategory.Tablet:
        if (this.IsSet(PlatformSettingType.Tablet))
          return this.GetValue(PlatformSettingType.Tablet);
        return this.GetScreenSetting(ScreenCategory.PC);
      case ScreenCategory.PC:
        if (this.IsSet(PlatformSettingType.PC))
          return this.GetValue(PlatformSettingType.PC);
        break;
    }
    Debug.LogError((object) "Could not find screen dependent value");
    return default (T);
  }

  private T GetMemorySetting(MemoryCategory memory)
  {
    switch (memory)
    {
      case MemoryCategory.Low:
        if (this.IsSet(PlatformSettingType.LowMemory))
          return this.GetValue(PlatformSettingType.LowMemory);
        break;
      case MemoryCategory.Medium:
        if (this.IsSet(PlatformSettingType.MediumMemory))
          return this.GetValue(PlatformSettingType.MediumMemory);
        return this.GetMemorySetting(MemoryCategory.Low);
      case MemoryCategory.High:
        if (this.IsSet(PlatformSettingType.HighMemory))
          return this.GetValue(PlatformSettingType.HighMemory);
        return this.GetMemorySetting(MemoryCategory.Medium);
    }
    Debug.LogError((object) "Could not find memory dependent value");
    return default (T);
  }

  private T GetInputSetting(InputCategory input)
  {
    switch (input)
    {
      case InputCategory.Mouse:
        if (this.IsSet(PlatformSettingType.Mouse))
          return this.GetValue(PlatformSettingType.Mouse);
        break;
      case InputCategory.Touch:
        if (this.IsSet(PlatformSettingType.Touch))
          return this.GetValue(PlatformSettingType.Touch);
        return this.GetInputSetting(InputCategory.Mouse);
    }
    Debug.LogError((object) "Could not find input dependent value");
    return default (T);
  }

  private T GetScreenDensitySetting(ScreenDensityCategory input)
  {
    switch (input)
    {
      case ScreenDensityCategory.Normal:
        if (this.IsSet(PlatformSettingType.NormalScreenDensity))
          return this.GetValue(PlatformSettingType.NormalScreenDensity);
        break;
      case ScreenDensityCategory.High:
        if (this.IsSet(PlatformSettingType.HighScreenDensity))
          return this.GetValue(PlatformSettingType.HighScreenDensity);
        return this.GetScreenDensitySetting(ScreenDensityCategory.Normal);
    }
    Debug.LogError((object) "Could not find screen density dependent value");
    return default (T);
  }
}
