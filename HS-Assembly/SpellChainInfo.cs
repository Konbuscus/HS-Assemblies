﻿// Decompiled with JetBrains decompiler
// Type: SpellChainInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class SpellChainInfo
{
  public bool m_Enabled = true;
  public Spell m_Prefab;
  public float m_SpawnDelayMin;
  public float m_SpawnDelayMax;
}
