﻿// Decompiled with JetBrains decompiler
// Type: Disable_LowQuality
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Disable_LowQuality : MonoBehaviour
{
  private void Awake()
  {
    GraphicsManager.Get().RegisterLowQualityDisableObject(this.gameObject);
    if (GraphicsManager.Get().RenderQualityLevel != GraphicsQuality.Low)
      return;
    this.gameObject.SetActive(false);
  }

  private void OnDestroy()
  {
    GraphicsManager graphicsManager = GraphicsManager.Get();
    if (!((Object) graphicsManager != (Object) null))
      return;
    graphicsManager.DeregisterLowQualityDisableObject(this.gameObject);
  }
}
