﻿// Decompiled with JetBrains decompiler
// Type: GameState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using PegasusGame;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityEngine;

public class GameState
{
  private Map<int, Entity> m_entityMap = new Map<int, Entity>();
  private Map<int, Player> m_playerMap = new Map<int, Player>();
  private Map<int, Network.EntityChoices> m_choicesMap = new Map<int, Network.EntityChoices>();
  private Queue<GameState.QueuedChoice> m_queuedChoices = new Queue<GameState.QueuedChoice>();
  private List<Entity> m_chosenEntities = new List<Entity>();
  private GameState.SelectedOption m_selectedOption = new GameState.SelectedOption();
  private List<GameState.CreateGameListener> m_createGameListeners = new List<GameState.CreateGameListener>();
  private List<GameState.OptionsReceivedListener> m_optionsReceivedListeners = new List<GameState.OptionsReceivedListener>();
  private List<GameState.OptionsSentListener> m_optionsSentListeners = new List<GameState.OptionsSentListener>();
  private List<GameState.OptionRejectedListener> m_optionRejectedListeners = new List<GameState.OptionRejectedListener>();
  private List<GameState.EntityChoicesReceivedListener> m_entityChoicesReceivedListeners = new List<GameState.EntityChoicesReceivedListener>();
  private List<GameState.EntitiesChosenReceivedListener> m_entitiesChosenReceivedListeners = new List<GameState.EntitiesChosenReceivedListener>();
  private List<GameState.CurrentPlayerChangedListener> m_currentPlayerChangedListeners = new List<GameState.CurrentPlayerChangedListener>();
  private List<GameState.FriendlyTurnStartedListener> m_friendlyTurnStartedListeners = new List<GameState.FriendlyTurnStartedListener>();
  private List<GameState.TurnChangedListener> m_turnChangedListeners = new List<GameState.TurnChangedListener>();
  private List<GameState.SpectatorNotifyListener> m_spectatorNotifyListeners = new List<GameState.SpectatorNotifyListener>();
  private List<GameState.GameOverListener> m_gameOverListeners = new List<GameState.GameOverListener>();
  private List<GameState.HeroChangedListener> m_heroChangedListeners = new List<GameState.HeroChangedListener>();
  private PowerProcessor m_powerProcessor = new PowerProcessor();
  private List<Spell> m_serverBlockingSpells = new List<Spell>();
  private List<SpellController> m_serverBlockingSpellControllers = new List<SpellController>();
  private List<GameState.TurnTimerUpdateListener> m_turnTimerUpdateListeners = new List<GameState.TurnTimerUpdateListener>();
  private List<GameState.TurnTimerUpdateListener> m_mulliganTimerUpdateListeners = new List<GameState.TurnTimerUpdateListener>();
  private Map<int, TurnTimerUpdate> m_turnTimerUpdates = new Map<int, TurnTimerUpdate>();
  public const int DEFAULT_SUBOPTION = -1;
  private const string INDENT = "    ";
  private const float BLOCK_REPORT_START_SEC = 10f;
  private const float BLOCK_REPORT_INTERVAL_SEC = 3f;
  private static GameState s_instance;
  private static List<GameState.GameStateInitializedListener> s_gameStateInitializedListeners;
  private GameEntity m_gameEntity;
  private GameState.CreateGamePhase m_createGamePhase;
  private Network.HistTagChange m_realTimeGameOverTagChange;
  private bool m_gameOver;
  private bool m_concedeRequested;
  private bool m_restartRequested;
  private int m_maxSecretsPerPlayer;
  private int m_maxFriendlyMinionsPerPlayer;
  private GameState.ResponseMode m_responseMode;
  private Network.Options m_options;
  private Network.Options m_lastOptions;
  private GameState.SelectedOption m_lastSelectedOption;
  private bool m_coinHasSpawned;
  private Card m_friendlyCardBeingDrawn;
  private Card m_opponentCardBeingDrawn;
  private int m_lastTurnRemindedOfFullHand;
  private bool m_usingFastActorTriggers;
  private float m_blockedSec;
  private float m_lastBlockedReportTimestamp;
  private bool m_busy;
  private bool m_mulliganBusy;

  public static GameState Get()
  {
    return GameState.s_instance;
  }

  public static GameState Initialize()
  {
    if (GameState.s_instance == null)
    {
      GameState.s_instance = new GameState();
      GameState.FireGameStateInitializedEvent();
    }
    return GameState.s_instance;
  }

  public static void Shutdown()
  {
    if (GameState.s_instance == null)
      return;
    GameState.s_instance = (GameState) null;
  }

  public void Update()
  {
    if (this.CheckBlockingPowerProcessor())
      return;
    this.m_powerProcessor.ProcessPowerQueue();
  }

  public PowerProcessor GetPowerProcessor()
  {
    return this.m_powerProcessor;
  }

  public bool HasPowersToProcess()
  {
    return this.m_powerProcessor.GetCurrentTaskList() != null || this.m_powerProcessor.GetPowerQueue().Count > 0;
  }

  public Entity GetEntity(int id)
  {
    Entity entity;
    this.m_entityMap.TryGetValue(id, out entity);
    return entity;
  }

  public Player GetPlayer(int id)
  {
    Player player;
    this.m_playerMap.TryGetValue(id, out player);
    return player;
  }

  public GameEntity GetGameEntity()
  {
    return this.m_gameEntity;
  }

  [Conditional("UNITY_EDITOR")]
  public void DebugSetGameEntity(GameEntity gameEntity)
  {
    this.m_gameEntity = gameEntity;
  }

  public bool WasGameCreated()
  {
    return this.m_gameEntity != null;
  }

  public Player GetFriendlySidePlayer()
  {
    using (Map<int, Player>.ValueCollection.Enumerator enumerator = this.m_playerMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player current = enumerator.Current;
        if (current.IsFriendlySide())
          return current;
      }
    }
    return (Player) null;
  }

  public Player GetOpposingSidePlayer()
  {
    using (Map<int, Player>.ValueCollection.Enumerator enumerator = this.m_playerMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player current = enumerator.Current;
        if (current.IsOpposingSide())
          return current;
      }
    }
    return (Player) null;
  }

  public int GetFriendlyPlayerId()
  {
    Player friendlySidePlayer = this.GetFriendlySidePlayer();
    if (friendlySidePlayer == null)
      return 0;
    return friendlySidePlayer.GetPlayerId();
  }

  public int GetOpposingPlayerId()
  {
    Player opposingSidePlayer = this.GetOpposingSidePlayer();
    if (opposingSidePlayer == null)
      return 0;
    return opposingSidePlayer.GetPlayerId();
  }

  public bool IsFriendlySidePlayerTurn()
  {
    Player friendlySidePlayer = this.GetFriendlySidePlayer();
    if (friendlySidePlayer == null)
      return false;
    return friendlySidePlayer.IsCurrentPlayer();
  }

  public Player GetCurrentPlayer()
  {
    using (Map<int, Player>.ValueCollection.Enumerator enumerator = this.m_playerMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player current = enumerator.Current;
        if (current.IsCurrentPlayer())
          return current;
      }
    }
    return (Player) null;
  }

  public Player GetFirstOpponentPlayer(Player player)
  {
    using (Map<int, Player>.ValueCollection.Enumerator enumerator = this.m_playerMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player current = enumerator.Current;
        if (current.GetSide() != player.GetSide())
          return current;
      }
    }
    return (Player) null;
  }

  public int GetNumFriendlyMinionsInPlay(bool includeUntouchables)
  {
    return this.GetNumMinionsInPlay(this.GetFriendlySidePlayer(), includeUntouchables);
  }

  public int GetNumEnemyMinionsInPlay(bool includeUntouchables)
  {
    return this.GetNumMinionsInPlay(this.GetOpposingSidePlayer(), includeUntouchables);
  }

  private int GetNumMinionsInPlay(Player player, bool includeUntouchables)
  {
    if (player == null)
      return 0;
    int num = 0;
    using (List<Card>.Enumerator enumerator = player.GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity = enumerator.Current.GetEntity();
        if (entity.GetController() == player && entity.IsMinion() && (includeUntouchables || !entity.HasTag(GAME_TAG.UNTOUCHABLE)))
          ++num;
      }
    }
    return num;
  }

  public int GetTurn()
  {
    return this.m_gameEntity != null ? this.m_gameEntity.GetTag(GAME_TAG.TURN) : 0;
  }

  public bool IsResponsePacketBlocked()
  {
    if (this.IsMulliganManagerActive())
      return false;
    if (!this.IsFriendlySidePlayerTurn() || this.IsTurnStartManagerBlockingInput() || EndTurnButton.Get().IsInWaitingState())
      return true;
    switch (this.m_responseMode)
    {
      case GameState.ResponseMode.NONE:
        return true;
      case GameState.ResponseMode.OPTION:
      case GameState.ResponseMode.SUB_OPTION:
      case GameState.ResponseMode.OPTION_TARGET:
        if (this.m_options == null)
          return true;
        break;
      case GameState.ResponseMode.CHOICE:
        if (this.GetFriendlyEntityChoices() == null)
          return true;
        break;
      default:
        UnityEngine.Debug.LogWarning((object) string.Format("GameState.IsResponsePacketBlocked() - unhandled response mode {0}", (object) this.m_responseMode));
        break;
    }
    return false;
  }

  public Map<int, Entity> GetEntityMap()
  {
    return this.m_entityMap;
  }

  public Map<int, Player> GetPlayerMap()
  {
    return this.m_playerMap;
  }

  public void AddPlayer(Player player)
  {
    this.m_playerMap.Add(player.GetPlayerId(), player);
    this.m_entityMap.Add(player.GetEntityId(), (Entity) player);
  }

  public void RemovePlayer(Player player)
  {
    player.Destroy();
    this.m_playerMap.Remove(player.GetPlayerId());
    this.m_entityMap.Remove(player.GetEntityId());
  }

  public void AddEntity(Entity entity)
  {
    this.m_entityMap.Add(entity.GetEntityId(), entity);
  }

  public void RemoveEntity(Entity entity)
  {
    if (entity.IsPlayer())
      this.RemovePlayer(entity as Player);
    else if (entity.IsGame())
    {
      this.m_gameEntity = (GameEntity) null;
    }
    else
    {
      if (entity.IsAttached())
      {
        Entity entity1 = this.GetEntity(entity.GetAttached());
        if (entity1 != null)
          entity1.RemoveAttachment(entity);
      }
      if (entity.IsHero())
      {
        Player player = this.GetPlayer(entity.GetControllerId());
        if (player != null)
          player.SetHero((Entity) null);
      }
      else if (entity.IsHeroPower())
      {
        Player player = this.GetPlayer(entity.GetControllerId());
        if (player != null)
          player.SetHeroPower((Entity) null);
      }
      entity.Destroy();
    }
  }

  public int GetMaxSecretsPerPlayer()
  {
    return this.m_maxSecretsPerPlayer;
  }

  public int GetMaxFriendlyMinionsPerPlayer()
  {
    return this.m_maxFriendlyMinionsPerPlayer;
  }

  public bool IsBusy()
  {
    return this.m_busy;
  }

  public void SetBusy(bool busy)
  {
    this.m_busy = busy;
  }

  public bool IsMulliganBusy()
  {
    return this.m_mulliganBusy;
  }

  public void SetMulliganBusy(bool busy)
  {
    this.m_mulliganBusy = busy;
  }

  public bool IsMulliganManagerActive()
  {
    if ((UnityEngine.Object) MulliganManager.Get() == (UnityEngine.Object) null)
      return false;
    return MulliganManager.Get().IsMulliganActive();
  }

  public bool IsTurnStartManagerActive()
  {
    if ((UnityEngine.Object) TurnStartManager.Get() == (UnityEngine.Object) null)
      return false;
    return TurnStartManager.Get().IsListeningForTurnEvents();
  }

  public bool IsTurnStartManagerBlockingInput()
  {
    if ((UnityEngine.Object) TurnStartManager.Get() == (UnityEngine.Object) null)
      return false;
    return TurnStartManager.Get().IsBlockingInput();
  }

  public bool HasTheCoinBeenSpawned()
  {
    return this.m_coinHasSpawned;
  }

  public void NotifyOfCoinSpawn()
  {
    this.m_coinHasSpawned = true;
  }

  public bool IsBeginPhase()
  {
    if (this.m_gameEntity == null)
      return false;
    return GameUtils.IsBeginPhase(this.m_gameEntity.GetTag<TAG_STEP>(GAME_TAG.STEP));
  }

  public bool IsPastBeginPhase()
  {
    if (this.m_gameEntity == null)
      return false;
    return GameUtils.IsPastBeginPhase(this.m_gameEntity.GetTag<TAG_STEP>(GAME_TAG.STEP));
  }

  public bool IsMainPhase()
  {
    if (this.m_gameEntity == null)
      return false;
    return GameUtils.IsMainPhase(this.m_gameEntity.GetTag<TAG_STEP>(GAME_TAG.STEP));
  }

  public bool IsMulliganPhase()
  {
    if (this.m_gameEntity == null)
      return false;
    return this.m_gameEntity.GetTag<TAG_STEP>(GAME_TAG.STEP) == TAG_STEP.BEGIN_MULLIGAN;
  }

  public bool IsMulliganPhasePending()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameState.\u003CIsMulliganPhasePending\u003Ec__AnonStorey3BB pendingCAnonStorey3Bb = new GameState.\u003CIsMulliganPhasePending\u003Ec__AnonStorey3BB();
    if (this.m_gameEntity == null)
      return false;
    if (this.m_gameEntity.GetTag<TAG_STEP>(GAME_TAG.NEXT_STEP) == TAG_STEP.BEGIN_MULLIGAN)
      return true;
    // ISSUE: reference to a compiler-generated field
    pendingCAnonStorey3Bb.foundMulliganStep = false;
    // ISSUE: reference to a compiler-generated field
    pendingCAnonStorey3Bb.gameEntityId = this.m_gameEntity.GetEntityId();
    // ISSUE: reference to a compiler-generated method
    this.m_powerProcessor.ForEachTaskList(new Action<int, PowerTaskList>(pendingCAnonStorey3Bb.\u003C\u003Em__142));
    // ISSUE: reference to a compiler-generated field
    return pendingCAnonStorey3Bb.foundMulliganStep;
  }

  public bool IsMulliganPhaseNowOrPending()
  {
    return this.IsMulliganPhase() || this.IsMulliganPhasePending();
  }

  public GameState.CreateGamePhase GetCreateGamePhase()
  {
    return this.m_createGamePhase;
  }

  public bool IsGameCreating()
  {
    return this.m_createGamePhase == GameState.CreateGamePhase.CREATING;
  }

  public bool IsGameCreated()
  {
    return this.m_createGamePhase == GameState.CreateGamePhase.CREATED;
  }

  public bool IsGameCreatedOrCreating()
  {
    if (!this.IsGameCreated())
      return this.IsGameCreating();
    return true;
  }

  public bool WasConcedeRequested()
  {
    return this.m_concedeRequested;
  }

  public void Concede()
  {
    if (this.m_concedeRequested)
      return;
    this.m_concedeRequested = true;
    Network.Concede();
  }

  public bool WasRestartRequested()
  {
    return this.m_restartRequested;
  }

  public void Restart()
  {
    if (this.m_restartRequested)
      return;
    this.m_restartRequested = true;
    if (this.IsGameOverNowOrPending())
      this.CheckRestartOnRealTimeGameOver();
    else
      this.Concede();
  }

  private void CheckRestartOnRealTimeGameOver()
  {
    if (!this.WasRestartRequested())
      return;
    this.m_gameOver = true;
    this.m_realTimeGameOverTagChange = (Network.HistTagChange) null;
    Network.DisconnectFromGameServer();
    NotificationManager.Get().DestroyAllNotificationsNowWithNoAnim();
    GameMgr.Get().RestartGame();
  }

  public bool IsGameOver()
  {
    return this.m_gameOver;
  }

  public bool IsGameOverPending()
  {
    return this.m_realTimeGameOverTagChange != null;
  }

  public bool IsGameOverNowOrPending()
  {
    return this.IsGameOver() || this.IsGameOverPending();
  }

  public Network.HistTagChange GetRealTimeGameOverTagChange()
  {
    return this.m_realTimeGameOverTagChange;
  }

  public void ShowEnemyTauntCharacters()
  {
    List<Zone> zones = ZoneMgr.Get().GetZones();
    for (int index1 = 0; index1 < zones.Count; ++index1)
    {
      Zone zone = zones[index1];
      if (zone.m_ServerTag == TAG_ZONE.PLAY && zone.m_Side == Player.Side.OPPOSING)
      {
        List<Card> cards = zone.GetCards();
        for (int index2 = 0; index2 < cards.Count; ++index2)
        {
          Card card = cards[index2];
          Entity entity = card.GetEntity();
          if (entity.HasTaunt() && !entity.IsStealthed())
            card.DoTauntNotification();
        }
      }
    }
  }

  public void GetTauntCounts(Player player, out int minionCount, out int heroCount)
  {
    minionCount = 0;
    heroCount = 0;
    List<Zone> zones = ZoneMgr.Get().GetZones();
    for (int index1 = 0; index1 < zones.Count; ++index1)
    {
      Zone zone = zones[index1];
      if (zone.m_ServerTag == TAG_ZONE.PLAY && player == zone.GetController())
      {
        List<Card> cards = zone.GetCards();
        for (int index2 = 0; index2 < cards.Count; ++index2)
        {
          Entity entity = cards[index2].GetEntity();
          if (entity.HasTaunt() && !entity.IsStealthed())
          {
            switch (entity.GetCardType())
            {
              case TAG_CARDTYPE.HERO:
                heroCount = heroCount + 1;
                continue;
              case TAG_CARDTYPE.MINION:
                minionCount = minionCount + 1;
                continue;
              default:
                continue;
            }
          }
        }
      }
    }
  }

  public Card GetFriendlyCardBeingDrawn()
  {
    return this.m_friendlyCardBeingDrawn;
  }

  public void SetFriendlyCardBeingDrawn(Card card)
  {
    this.m_friendlyCardBeingDrawn = card;
  }

  public Card GetOpponentCardBeingDrawn()
  {
    return this.m_opponentCardBeingDrawn;
  }

  public void SetOpponentCardBeingDrawn(Card card)
  {
    this.m_opponentCardBeingDrawn = card;
  }

  public bool IsBeingDrawn(Card card)
  {
    return (UnityEngine.Object) card == (UnityEngine.Object) this.m_friendlyCardBeingDrawn || (UnityEngine.Object) card == (UnityEngine.Object) this.m_opponentCardBeingDrawn;
  }

  public bool ClearCardBeingDrawn(Card card)
  {
    if ((UnityEngine.Object) card == (UnityEngine.Object) this.m_friendlyCardBeingDrawn)
    {
      this.m_friendlyCardBeingDrawn = (Card) null;
      return true;
    }
    if (!((UnityEngine.Object) card == (UnityEngine.Object) this.m_opponentCardBeingDrawn))
      return false;
    this.m_opponentCardBeingDrawn = (Card) null;
    return true;
  }

  public int GetLastTurnRemindedOfFullHand()
  {
    return this.m_lastTurnRemindedOfFullHand;
  }

  public void SetLastTurnRemindedOfFullHand(int turn)
  {
    this.m_lastTurnRemindedOfFullHand = turn;
  }

  public bool IsUsingFastActorTriggers()
  {
    return this.m_usingFastActorTriggers;
  }

  public void SetUsingFastActorTriggers(bool enable)
  {
    this.m_usingFastActorTriggers = enable;
  }

  public bool HasHandPlays()
  {
    if (this.m_options == null)
      return false;
    using (List<Network.Options.Option>.Enumerator enumerator = this.m_options.List.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Options.Option current = enumerator.Current;
        if (current.Type == Network.Options.Option.OptionType.POWER)
        {
          Entity entity = this.GetEntity(current.Main.ID);
          if (entity != null)
          {
            Card card = entity.GetCard();
            if (!((UnityEngine.Object) card == (UnityEngine.Object) null) && !((UnityEngine.Object) (card.GetZone() as ZoneHand) == (UnityEngine.Object) null))
              return true;
          }
        }
      }
    }
    return false;
  }

  public bool CanShowScoreScreen()
  {
    return this.m_gameEntity.HasTag(GAME_TAG.SCORE_LABELID_1) || this.m_gameEntity.HasTag(GAME_TAG.SCORE_LABELID_2) || this.m_gameEntity.HasTag(GAME_TAG.SCORE_LABELID_3);
  }

  private void PreprocessRealTimeTagChange(Entity entity, Network.HistTagChange change)
  {
    if (change.Tag != 17 || !GameUtils.IsGameOverTag(change.Entity, change.Tag, change.Value))
      return;
    this.OnRealTimeGameOver(change);
  }

  private void PreprocessTagChange(Entity entity, TagDelta change)
  {
    GAME_TAG tag = (GAME_TAG) change.tag;
    switch (tag)
    {
      case GAME_TAG.PLAYSTATE:
        if (!GameUtils.IsGameOverTag((Player) entity, change.tag, change.newValue))
          break;
        this.OnGameOver((TAG_PLAYSTATE) change.newValue);
        break;
      case GAME_TAG.TURN:
        this.OnTurnChanged(change.oldValue, change.newValue);
        break;
      default:
        if (tag != GAME_TAG.CURRENT_PLAYER || change.newValue != 1)
          break;
        this.OnCurrentPlayerChanged((Player) entity);
        break;
    }
  }

  private void PreprocessEarlyConcedeTagChange(Entity entity, TagDelta change)
  {
    if (change.tag != 17 || !GameUtils.IsGameOverTag((Player) entity, change.tag, change.newValue))
      return;
    this.OnGameOver((TAG_PLAYSTATE) change.newValue);
  }

  private void ProcessEarlyConcedeTagChange(Entity entity, TagDelta change)
  {
    if (change.tag != 17)
      return;
    entity.OnTagChanged(change);
  }

  private void OnRealTimeGameOver(Network.HistTagChange change)
  {
    this.m_realTimeGameOverTagChange = change;
    if (Network.ShouldBeConnectedToAurora())
      BnetPresenceMgr.Get().SetGameFieldBlob(21U, (IProtoBuf) null);
    SpectatorManager.Get().OnRealTimeGameOver();
    this.CheckRestartOnRealTimeGameOver();
  }

  private void OnGameOver(TAG_PLAYSTATE playState)
  {
    this.m_gameOver = true;
    this.m_realTimeGameOverTagChange = (Network.HistTagChange) null;
    this.m_gameEntity.NotifyOfGameOver(playState);
    this.FireGameOverEvent(playState);
  }

  private void OnCurrentPlayerChanged(Player player)
  {
    this.FireCurrentPlayerChangedEvent(player);
  }

  private void OnTurnChanged(int oldTurn, int newTurn)
  {
    this.OnTurnChanged_TurnTimer(oldTurn, newTurn);
    this.FireTurnChangedEvent(oldTurn, newTurn);
  }

  public void AddServerBlockingSpell(Spell spell)
  {
    if ((UnityEngine.Object) spell == (UnityEngine.Object) null || this.m_serverBlockingSpells.Contains(spell))
      return;
    this.m_serverBlockingSpells.Add(spell);
  }

  public bool RemoveServerBlockingSpell(Spell spell)
  {
    return this.m_serverBlockingSpells.Remove(spell);
  }

  public void AddServerBlockingSpellController(SpellController spellController)
  {
    if ((UnityEngine.Object) spellController == (UnityEngine.Object) null || this.m_serverBlockingSpellControllers.Contains(spellController))
      return;
    this.m_serverBlockingSpellControllers.Add(spellController);
  }

  public bool RemoveServerBlockingSpellController(SpellController spellController)
  {
    return this.m_serverBlockingSpellControllers.Remove(spellController);
  }

  public void DebugNukeServerBlocks()
  {
    while (this.m_serverBlockingSpells.Count > 0)
      this.m_serverBlockingSpells[0].OnSpellFinished();
    while (this.m_serverBlockingSpellControllers.Count > 0)
      this.m_serverBlockingSpellControllers[0].ForceKill();
    this.m_powerProcessor.ForceStopHistoryBlocking();
    this.m_busy = false;
  }

  public bool IsBlockingPowerProcessor()
  {
    return this.m_serverBlockingSpells.Count > 0 || this.m_serverBlockingSpellControllers.Count > 0 || this.m_powerProcessor.IsHistoryBlocking();
  }

  public bool CanProcessPowerQueue()
  {
    return !this.IsBlockingPowerProcessor() && !this.IsBusy() && (this.m_powerProcessor.GetCurrentTaskList() == null && this.m_powerProcessor.GetPowerQueue().Count != 0) && !this.WasRestartRequested();
  }

  private bool CheckBlockingPowerProcessor()
  {
    if (!this.IsBlockingPowerProcessor())
    {
      this.m_blockedSec = 0.0f;
      return false;
    }
    this.m_blockedSec += Time.deltaTime;
    if (this.ReconnectIfStuck())
      return true;
    this.ReportStuck();
    return true;
  }

  private bool ReconnectIfStuck()
  {
    if (!ReconnectMgr.Get().IsReconnectEnabled())
      return false;
    Network.GameSetup gameSetup = GameMgr.Get().GetGameSetup();
    if (gameSetup.DisconnectWhenStuckSeconds > 0 && (double) this.m_blockedSec < (double) gameSetup.DisconnectWhenStuckSeconds)
      return false;
    string elapsedTimeString = TimeUtils.GetDevElapsedTimeString(this.m_blockedSec);
    string str1 = this.BuildServerBlockingCausesString();
    BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
    BnetPlayer myPlayer = BnetPresenceMgr.Get().GetMyPlayer();
    string str2 = string.Format("Player {0} (Game Account: {1}) reconnected because they got stuck on client effects for more than {2}.\nCause: {3}", (object) (myPlayer == null || !(myPlayer.GetBattleTag() != (BnetBattleTag) null) ? "UNKNOWN" : myPlayer.GetBattleTag().ToString()), (object) myGameAccountId, (object) elapsedTimeString, (object) str1);
    Log.Power.PrintWarning("GameState.ReconnectIfStuck() - Blocked more than {0}. Telemetry Message:\n{1}", new object[2]
    {
      (object) elapsedTimeString,
      (object) str2
    });
    BIReport biReport = BIReport.Get();
    string str3 = str2;
    int num1 = 100;
    int errorCode = 0;
    string message = str3;
    int num2 = -1;
    biReport.TelemetryWarn((BIReport.TelemetryEvent) num1, errorCode, message, (constants.BnetRegion) num2);
    Network.DisconnectFromGameServer();
    return true;
  }

  private void ReportStuck()
  {
    if ((double) this.m_blockedSec < 10.0)
      return;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if ((double) (realtimeSinceStartup - this.m_lastBlockedReportTimestamp) < 3.0)
      return;
    this.m_lastBlockedReportTimestamp = realtimeSinceStartup;
    string elapsedTimeString = TimeUtils.GetDevElapsedTimeString(this.m_blockedSec);
    string str = this.BuildServerBlockingCausesString();
    Log.Power.PrintWarning("GameState.ReportStuck() - Stuck for {0}. {1}", new object[2]
    {
      (object) elapsedTimeString,
      (object) str
    });
  }

  private string BuildServerBlockingCausesString()
  {
    StringBuilder builder = new StringBuilder();
    int sectionCount = 0;
    this.AppendServerBlockingSection<Spell>(builder, "Spells:", this.m_serverBlockingSpells, new GameState.AppendBlockingServerItemCallback<Spell>(this.AppendServerBlockingSpell), ref sectionCount);
    this.AppendServerBlockingSection<SpellController>(builder, "SpellControllers:", this.m_serverBlockingSpellControllers, new GameState.AppendBlockingServerItemCallback<SpellController>(this.AppendServerBlockingSpellController), ref sectionCount);
    this.AppendServerBlockingHistory(builder, ref sectionCount);
    if (this.m_busy)
    {
      if (sectionCount > 0)
        builder.Append(' ');
      builder.Append("Busy=true");
      int num = sectionCount + 1;
    }
    return builder.ToString();
  }

  private void AppendServerBlockingSection<T>(StringBuilder builder, string sectionPrefix, List<T> items, GameState.AppendBlockingServerItemCallback<T> itemCallback, ref int sectionCount) where T : Component
  {
    if (items.Count == 0)
      return;
    if (sectionCount > 0)
      builder.Append(' ');
    builder.Append('{');
    builder.Append(sectionPrefix);
    for (int index = 0; index < items.Count; ++index)
    {
      builder.Append(' ');
      if (itemCallback == null)
        builder.Append(items[index].name);
      else
        itemCallback(builder, items[index]);
    }
    builder.Append('}');
    sectionCount = sectionCount + 1;
  }

  private void AppendServerBlockingSpell(StringBuilder builder, Spell spell)
  {
    builder.Append('[');
    builder.Append(spell.name);
    builder.Append(' ');
    builder.AppendFormat("Source: {0}", (object) spell.GetSource());
    builder.Append(' ');
    builder.Append("Targets:");
    List<GameObject> targets = spell.GetTargets();
    if (targets.Count == 0)
    {
      builder.Append(' ');
      builder.Append("none");
    }
    else
    {
      for (int index = 0; index < targets.Count; ++index)
      {
        builder.Append(' ');
        GameObject gameObject = targets[index];
        builder.Append(gameObject.ToString());
      }
    }
    builder.Append(']');
  }

  private void AppendServerBlockingSpellController(StringBuilder builder, SpellController spellController)
  {
    builder.Append('[');
    builder.Append(spellController.name);
    builder.Append(' ');
    builder.AppendFormat("Source: {0}", (object) spellController.GetSource());
    builder.Append(' ');
    builder.Append("Targets:");
    List<Card> targets = spellController.GetTargets();
    if (targets.Count == 0)
    {
      builder.Append(' ');
      builder.Append("none");
    }
    else
    {
      for (int index = 0; index < targets.Count; ++index)
      {
        builder.Append(' ');
        Card card = targets[index];
        builder.Append(card.ToString());
      }
    }
    builder.Append(']');
  }

  private void AppendServerBlockingHistory(StringBuilder builder, ref int sectionCount)
  {
    if (!this.m_powerProcessor.IsHistoryBlocking())
      return;
    Entity pendingBigCardEntity = HistoryManager.Get().GetPendingBigCardEntity();
    PowerTaskList blockingTaskList = this.m_powerProcessor.GetHistoryBlockingTaskList();
    PowerTaskList currentTaskList = this.m_powerProcessor.GetCurrentTaskList();
    if (sectionCount > 0)
      builder.Append(' ');
    builder.Append("History: ");
    builder.Append('{');
    builder.AppendFormat("PendingBigCard: {0}", (object) pendingBigCardEntity);
    builder.Append(' ');
    builder.AppendFormat("BlockingTaskList: ");
    this.PrintBlockingTaskList(builder, blockingTaskList);
    builder.Append(' ');
    builder.AppendFormat("CurrentTaskList: ");
    this.PrintBlockingTaskList(builder, currentTaskList);
    builder.Append('}');
    sectionCount = sectionCount + 1;
  }

  public static bool RegisterGameStateInitializedListener(GameState.GameStateInitializedCallback callback, object userData = null)
  {
    if (callback == null)
      return false;
    GameState.GameStateInitializedListener initializedListener = new GameState.GameStateInitializedListener();
    initializedListener.SetCallback(callback);
    initializedListener.SetUserData(userData);
    if (GameState.s_gameStateInitializedListeners == null)
      GameState.s_gameStateInitializedListeners = new List<GameState.GameStateInitializedListener>();
    else if (GameState.s_gameStateInitializedListeners.Contains(initializedListener))
      return false;
    GameState.s_gameStateInitializedListeners.Add(initializedListener);
    return true;
  }

  public static bool UnregisterGameStateInitializedListener(GameState.GameStateInitializedCallback callback, object userData = null)
  {
    if (callback == null || GameState.s_gameStateInitializedListeners == null)
      return false;
    GameState.GameStateInitializedListener initializedListener = new GameState.GameStateInitializedListener();
    initializedListener.SetCallback(callback);
    initializedListener.SetUserData(userData);
    return GameState.s_gameStateInitializedListeners.Remove(initializedListener);
  }

  public bool RegisterCreateGameListener(GameState.CreateGameCallback callback)
  {
    return this.RegisterCreateGameListener(callback, (object) null);
  }

  public bool RegisterCreateGameListener(GameState.CreateGameCallback callback, object userData)
  {
    GameState.CreateGameListener createGameListener = new GameState.CreateGameListener();
    createGameListener.SetCallback(callback);
    createGameListener.SetUserData(userData);
    if (this.m_createGameListeners.Contains(createGameListener))
      return false;
    this.m_createGameListeners.Add(createGameListener);
    return true;
  }

  public bool UnregisterCreateGameListener(GameState.CreateGameCallback callback)
  {
    return this.UnregisterCreateGameListener(callback, (object) null);
  }

  public bool UnregisterCreateGameListener(GameState.CreateGameCallback callback, object userData)
  {
    GameState.CreateGameListener createGameListener = new GameState.CreateGameListener();
    createGameListener.SetCallback(callback);
    createGameListener.SetUserData(userData);
    return this.m_createGameListeners.Remove(createGameListener);
  }

  public bool RegisterOptionsReceivedListener(GameState.OptionsReceivedCallback callback)
  {
    return this.RegisterOptionsReceivedListener(callback, (object) null);
  }

  public bool RegisterOptionsReceivedListener(GameState.OptionsReceivedCallback callback, object userData)
  {
    GameState.OptionsReceivedListener receivedListener = new GameState.OptionsReceivedListener();
    receivedListener.SetCallback(callback);
    receivedListener.SetUserData(userData);
    if (this.m_optionsReceivedListeners.Contains(receivedListener))
      return false;
    this.m_optionsReceivedListeners.Add(receivedListener);
    return true;
  }

  public bool UnregisterOptionsReceivedListener(GameState.OptionsReceivedCallback callback)
  {
    return this.UnregisterOptionsReceivedListener(callback, (object) null);
  }

  public bool UnregisterOptionsReceivedListener(GameState.OptionsReceivedCallback callback, object userData)
  {
    GameState.OptionsReceivedListener receivedListener = new GameState.OptionsReceivedListener();
    receivedListener.SetCallback(callback);
    receivedListener.SetUserData(userData);
    return this.m_optionsReceivedListeners.Remove(receivedListener);
  }

  public bool RegisterOptionsSentListener(GameState.OptionsSentCallback callback, object userData = null)
  {
    GameState.OptionsSentListener optionsSentListener = new GameState.OptionsSentListener();
    optionsSentListener.SetCallback(callback);
    optionsSentListener.SetUserData(userData);
    if (this.m_optionsSentListeners.Contains(optionsSentListener))
      return false;
    this.m_optionsSentListeners.Add(optionsSentListener);
    return true;
  }

  public bool UnregisterOptionsReceivedListener(GameState.OptionsSentCallback callback, object userData = null)
  {
    GameState.OptionsSentListener optionsSentListener = new GameState.OptionsSentListener();
    optionsSentListener.SetCallback(callback);
    optionsSentListener.SetUserData(userData);
    return this.m_optionsSentListeners.Remove(optionsSentListener);
  }

  public bool RegisterOptionRejectedListener(GameState.OptionRejectedCallback callback, object userData = null)
  {
    GameState.OptionRejectedListener rejectedListener = new GameState.OptionRejectedListener();
    rejectedListener.SetCallback(callback);
    rejectedListener.SetUserData(userData);
    if (this.m_optionRejectedListeners.Contains(rejectedListener))
      return false;
    this.m_optionRejectedListeners.Add(rejectedListener);
    return true;
  }

  public bool UnregisterOptionRejectedListener(GameState.OptionRejectedCallback callback, object userData = null)
  {
    GameState.OptionRejectedListener rejectedListener = new GameState.OptionRejectedListener();
    rejectedListener.SetCallback(callback);
    rejectedListener.SetUserData(userData);
    return this.m_optionRejectedListeners.Remove(rejectedListener);
  }

  public bool RegisterEntityChoicesReceivedListener(GameState.EntityChoicesReceivedCallback callback)
  {
    return this.RegisterEntityChoicesReceivedListener(callback, (object) null);
  }

  public bool RegisterEntityChoicesReceivedListener(GameState.EntityChoicesReceivedCallback callback, object userData)
  {
    GameState.EntityChoicesReceivedListener receivedListener = new GameState.EntityChoicesReceivedListener();
    receivedListener.SetCallback(callback);
    receivedListener.SetUserData(userData);
    if (this.m_entityChoicesReceivedListeners.Contains(receivedListener))
      return false;
    this.m_entityChoicesReceivedListeners.Add(receivedListener);
    return true;
  }

  public bool UnregisterEntityChoicesReceivedListener(GameState.EntityChoicesReceivedCallback callback)
  {
    return this.UnregisterEntityChoicesReceivedListener(callback, (object) null);
  }

  public bool UnregisterEntityChoicesReceivedListener(GameState.EntityChoicesReceivedCallback callback, object userData)
  {
    GameState.EntityChoicesReceivedListener receivedListener = new GameState.EntityChoicesReceivedListener();
    receivedListener.SetCallback(callback);
    receivedListener.SetUserData(userData);
    return this.m_entityChoicesReceivedListeners.Remove(receivedListener);
  }

  public bool RegisterEntitiesChosenReceivedListener(GameState.EntitiesChosenReceivedCallback callback)
  {
    return this.RegisterEntitiesChosenReceivedListener(callback, (object) null);
  }

  public bool RegisterEntitiesChosenReceivedListener(GameState.EntitiesChosenReceivedCallback callback, object userData)
  {
    GameState.EntitiesChosenReceivedListener receivedListener = new GameState.EntitiesChosenReceivedListener();
    receivedListener.SetCallback(callback);
    receivedListener.SetUserData(userData);
    if (this.m_entitiesChosenReceivedListeners.Contains(receivedListener))
      return false;
    this.m_entitiesChosenReceivedListeners.Add(receivedListener);
    return true;
  }

  public bool UnregisterEntitiesChosenReceivedListener(GameState.EntitiesChosenReceivedCallback callback)
  {
    return this.UnregisterEntitiesChosenReceivedListener(callback, (object) null);
  }

  public bool UnregisterEntitiesChosenReceivedListener(GameState.EntitiesChosenReceivedCallback callback, object userData)
  {
    GameState.EntitiesChosenReceivedListener receivedListener = new GameState.EntitiesChosenReceivedListener();
    receivedListener.SetCallback(callback);
    receivedListener.SetUserData(userData);
    return this.m_entitiesChosenReceivedListeners.Remove(receivedListener);
  }

  public bool RegisterCurrentPlayerChangedListener(GameState.CurrentPlayerChangedCallback callback)
  {
    return this.RegisterCurrentPlayerChangedListener(callback, (object) null);
  }

  public bool RegisterCurrentPlayerChangedListener(GameState.CurrentPlayerChangedCallback callback, object userData)
  {
    GameState.CurrentPlayerChangedListener playerChangedListener = new GameState.CurrentPlayerChangedListener();
    playerChangedListener.SetCallback(callback);
    playerChangedListener.SetUserData(userData);
    if (this.m_currentPlayerChangedListeners.Contains(playerChangedListener))
      return false;
    this.m_currentPlayerChangedListeners.Add(playerChangedListener);
    return true;
  }

  public bool UnregisterCurrentPlayerChangedListener(GameState.CurrentPlayerChangedCallback callback)
  {
    return this.UnregisterCurrentPlayerChangedListener(callback, (object) null);
  }

  public bool UnregisterCurrentPlayerChangedListener(GameState.CurrentPlayerChangedCallback callback, object userData)
  {
    GameState.CurrentPlayerChangedListener playerChangedListener = new GameState.CurrentPlayerChangedListener();
    playerChangedListener.SetCallback(callback);
    playerChangedListener.SetUserData(userData);
    return this.m_currentPlayerChangedListeners.Remove(playerChangedListener);
  }

  public bool RegisterTurnChangedListener(GameState.TurnChangedCallback callback)
  {
    return this.RegisterTurnChangedListener(callback, (object) null);
  }

  public bool RegisterTurnChangedListener(GameState.TurnChangedCallback callback, object userData)
  {
    GameState.TurnChangedListener turnChangedListener = new GameState.TurnChangedListener();
    turnChangedListener.SetCallback(callback);
    turnChangedListener.SetUserData(userData);
    if (this.m_turnChangedListeners.Contains(turnChangedListener))
      return false;
    this.m_turnChangedListeners.Add(turnChangedListener);
    return true;
  }

  public bool UnregisterTurnChangedListener(GameState.TurnChangedCallback callback)
  {
    return this.UnregisterTurnChangedListener(callback, (object) null);
  }

  public bool UnregisterTurnChangedListener(GameState.TurnChangedCallback callback, object userData)
  {
    GameState.TurnChangedListener turnChangedListener = new GameState.TurnChangedListener();
    turnChangedListener.SetCallback(callback);
    turnChangedListener.SetUserData(userData);
    return this.m_turnChangedListeners.Remove(turnChangedListener);
  }

  public bool RegisterFriendlyTurnStartedListener(GameState.FriendlyTurnStartedCallback callback, object userData = null)
  {
    GameState.FriendlyTurnStartedListener turnStartedListener = new GameState.FriendlyTurnStartedListener();
    turnStartedListener.SetCallback(callback);
    turnStartedListener.SetUserData(userData);
    if (this.m_friendlyTurnStartedListeners.Contains(turnStartedListener))
      return false;
    this.m_friendlyTurnStartedListeners.Add(turnStartedListener);
    return true;
  }

  public bool UnregisterFriendlyTurnStartedListener(GameState.FriendlyTurnStartedCallback callback, object userData = null)
  {
    GameState.FriendlyTurnStartedListener turnStartedListener = new GameState.FriendlyTurnStartedListener();
    turnStartedListener.SetCallback(callback);
    turnStartedListener.SetUserData(userData);
    return this.m_friendlyTurnStartedListeners.Remove(turnStartedListener);
  }

  public bool RegisterTurnTimerUpdateListener(GameState.TurnTimerUpdateCallback callback)
  {
    return this.RegisterTurnTimerUpdateListener(callback, (object) null);
  }

  public bool RegisterTurnTimerUpdateListener(GameState.TurnTimerUpdateCallback callback, object userData)
  {
    GameState.TurnTimerUpdateListener timerUpdateListener = new GameState.TurnTimerUpdateListener();
    timerUpdateListener.SetCallback(callback);
    timerUpdateListener.SetUserData(userData);
    if (this.m_turnTimerUpdateListeners.Contains(timerUpdateListener))
      return false;
    this.m_turnTimerUpdateListeners.Add(timerUpdateListener);
    return true;
  }

  public bool UnregisterTurnTimerUpdateListener(GameState.TurnTimerUpdateCallback callback)
  {
    return this.UnregisterTurnTimerUpdateListener(callback, (object) null);
  }

  public bool UnregisterTurnTimerUpdateListener(GameState.TurnTimerUpdateCallback callback, object userData)
  {
    GameState.TurnTimerUpdateListener timerUpdateListener = new GameState.TurnTimerUpdateListener();
    timerUpdateListener.SetCallback(callback);
    timerUpdateListener.SetUserData(userData);
    return this.m_turnTimerUpdateListeners.Remove(timerUpdateListener);
  }

  public bool RegisterMulliganTimerUpdateListener(GameState.TurnTimerUpdateCallback callback)
  {
    return this.RegisterMulliganTimerUpdateListener(callback, (object) null);
  }

  public bool RegisterMulliganTimerUpdateListener(GameState.TurnTimerUpdateCallback callback, object userData)
  {
    GameState.TurnTimerUpdateListener timerUpdateListener = new GameState.TurnTimerUpdateListener();
    timerUpdateListener.SetCallback(callback);
    timerUpdateListener.SetUserData(userData);
    if (this.m_mulliganTimerUpdateListeners.Contains(timerUpdateListener))
      return false;
    this.m_mulliganTimerUpdateListeners.Add(timerUpdateListener);
    return true;
  }

  public bool UnregisterMulliganTimerUpdateListener(GameState.TurnTimerUpdateCallback callback)
  {
    return this.UnregisterMulliganTimerUpdateListener(callback, (object) null);
  }

  public bool UnregisterMulliganTimerUpdateListener(GameState.TurnTimerUpdateCallback callback, object userData)
  {
    GameState.TurnTimerUpdateListener timerUpdateListener = new GameState.TurnTimerUpdateListener();
    timerUpdateListener.SetCallback(callback);
    timerUpdateListener.SetUserData(userData);
    return this.m_mulliganTimerUpdateListeners.Remove(timerUpdateListener);
  }

  public bool RegisterSpectatorNotifyListener(GameState.SpectatorNotifyEventCallback callback, object userData = null)
  {
    GameState.SpectatorNotifyListener spectatorNotifyListener = new GameState.SpectatorNotifyListener();
    spectatorNotifyListener.SetCallback(callback);
    spectatorNotifyListener.SetUserData(userData);
    if (this.m_spectatorNotifyListeners.Contains(spectatorNotifyListener))
      return false;
    this.m_spectatorNotifyListeners.Add(spectatorNotifyListener);
    return true;
  }

  public bool UnregisterSpectatorNotifyListener(GameState.SpectatorNotifyEventCallback callback, object userData = null)
  {
    GameState.SpectatorNotifyListener spectatorNotifyListener = new GameState.SpectatorNotifyListener();
    spectatorNotifyListener.SetCallback(callback);
    spectatorNotifyListener.SetUserData(userData);
    return this.m_spectatorNotifyListeners.Remove(spectatorNotifyListener);
  }

  public bool RegisterGameOverListener(GameState.GameOverCallback callback, object userData = null)
  {
    GameState.GameOverListener gameOverListener = new GameState.GameOverListener();
    gameOverListener.SetCallback(callback);
    gameOverListener.SetUserData(userData);
    if (this.m_gameOverListeners.Contains(gameOverListener))
      return false;
    this.m_gameOverListeners.Add(gameOverListener);
    return true;
  }

  public bool UnregisterGameOverListener(GameState.GameOverCallback callback, object userData = null)
  {
    GameState.GameOverListener gameOverListener = new GameState.GameOverListener();
    gameOverListener.SetCallback(callback);
    gameOverListener.SetUserData(userData);
    return this.m_gameOverListeners.Remove(gameOverListener);
  }

  public bool RegisterHeroChangedListener(GameState.HeroChangedCallback callback, object userData = null)
  {
    GameState.HeroChangedListener heroChangedListener = new GameState.HeroChangedListener();
    heroChangedListener.SetCallback(callback);
    heroChangedListener.SetUserData(userData);
    if (this.m_heroChangedListeners.Contains(heroChangedListener))
      return false;
    this.m_heroChangedListeners.Add(heroChangedListener);
    return true;
  }

  public bool UnregisterHeroChangedListener(GameState.HeroChangedCallback callback, object userData = null)
  {
    GameState.HeroChangedListener heroChangedListener = new GameState.HeroChangedListener();
    heroChangedListener.SetCallback(callback);
    heroChangedListener.SetUserData(userData);
    return this.m_heroChangedListeners.Remove(heroChangedListener);
  }

  private static void FireGameStateInitializedEvent()
  {
    if (GameState.s_gameStateInitializedListeners == null)
      return;
    foreach (GameState.GameStateInitializedListener initializedListener in GameState.s_gameStateInitializedListeners.ToArray())
      initializedListener.Fire(GameState.s_instance);
  }

  private void FireCreateGameEvent()
  {
    foreach (GameState.CreateGameListener createGameListener in this.m_createGameListeners.ToArray())
      createGameListener.Fire(this.m_createGamePhase);
  }

  private void FireOptionsReceivedEvent()
  {
    foreach (GameState.OptionsReceivedListener receivedListener in this.m_optionsReceivedListeners.ToArray())
      receivedListener.Fire();
  }

  private void FireOptionsSentEvent(Network.Options.Option option)
  {
    foreach (GameState.OptionsSentListener optionsSentListener in this.m_optionsSentListeners.ToArray())
      optionsSentListener.Fire(option);
  }

  private void FireOptionRejectedEvent(Network.Options.Option option)
  {
    foreach (GameState.OptionRejectedListener rejectedListener in this.m_optionRejectedListeners.ToArray())
      rejectedListener.Fire(option);
  }

  private void FireEntityChoicesReceivedEvent(Network.EntityChoices choices, PowerTaskList preChoiceTaskList)
  {
    foreach (GameState.EntityChoicesReceivedListener receivedListener in this.m_entityChoicesReceivedListeners.ToArray())
      receivedListener.Fire(choices, preChoiceTaskList);
  }

  private bool FireEntitiesChosenReceivedEvent(Network.EntitiesChosen chosen)
  {
    GameState.EntitiesChosenReceivedListener[] array = this.m_entitiesChosenReceivedListeners.ToArray();
    bool flag = false;
    foreach (GameState.EntitiesChosenReceivedListener receivedListener in array)
      flag = receivedListener.Fire(chosen) || flag;
    return flag;
  }

  private void FireTurnChangedEvent(int oldTurn, int newTurn)
  {
    foreach (GameState.TurnChangedListener turnChangedListener in this.m_turnChangedListeners.ToArray())
      turnChangedListener.Fire(oldTurn, newTurn);
  }

  public void FireFriendlyTurnStartedEvent()
  {
    this.m_gameEntity.NotifyOfStartOfTurnEventsFinished();
    foreach (GameState.FriendlyTurnStartedListener turnStartedListener in this.m_friendlyTurnStartedListeners.ToArray())
      turnStartedListener.Fire();
  }

  private void FireTurnTimerUpdateEvent(TurnTimerUpdate update)
  {
    foreach (GameState.TurnTimerUpdateListener timerUpdateListener in !this.IsMulliganManagerActive() ? this.m_turnTimerUpdateListeners.ToArray() : this.m_mulliganTimerUpdateListeners.ToArray())
      timerUpdateListener.Fire(update);
  }

  private void FireCurrentPlayerChangedEvent(Player player)
  {
    foreach (GameState.CurrentPlayerChangedListener playerChangedListener in this.m_currentPlayerChangedListeners.ToArray())
      playerChangedListener.Fire(player);
  }

  private void FireSpectatorNotifyEvent(SpectatorNotify notify)
  {
    foreach (GameState.SpectatorNotifyListener spectatorNotifyListener in this.m_spectatorNotifyListeners.ToArray())
      spectatorNotifyListener.Fire(notify);
  }

  private void FireGameOverEvent(TAG_PLAYSTATE playState)
  {
    foreach (GameState.GameOverListener gameOverListener in this.m_gameOverListeners.ToArray())
      gameOverListener.Fire(playState);
  }

  public void FireHeroChangedEvent(Player player)
  {
    foreach (GameState.HeroChangedListener heroChangedListener in this.m_heroChangedListeners.ToArray())
      heroChangedListener.Fire(player);
  }

  public GameState.ResponseMode GetResponseMode()
  {
    return this.m_responseMode;
  }

  public Network.EntityChoices GetFriendlyEntityChoices()
  {
    return this.GetEntityChoices(this.GetFriendlyPlayerId());
  }

  public Network.EntityChoices GetOpponentEntityChoices()
  {
    return this.GetEntityChoices(this.GetOpposingPlayerId());
  }

  public Network.EntityChoices GetEntityChoices(int playerId)
  {
    Network.EntityChoices entityChoices;
    this.m_choicesMap.TryGetValue(playerId, out entityChoices);
    return entityChoices;
  }

  public Map<int, Network.EntityChoices> GetEntityChoicesMap()
  {
    return this.m_choicesMap;
  }

  public bool IsChoosableEntity(Entity entity)
  {
    Network.EntityChoices friendlyEntityChoices = this.GetFriendlyEntityChoices();
    if (friendlyEntityChoices == null)
      return false;
    return friendlyEntityChoices.Entities.Contains(entity.GetEntityId());
  }

  public bool IsChosenEntity(Entity entity)
  {
    if (this.GetFriendlyEntityChoices() == null)
      return false;
    return this.m_chosenEntities.Contains(entity);
  }

  public bool IsValidOptionTarget(Entity entity)
  {
    Network.Options.Option.SubOption networkSubOption = this.GetSelectedNetworkSubOption();
    if (networkSubOption == null || networkSubOption.Targets == null)
      return false;
    return networkSubOption.Targets.Contains(entity.GetEntityId());
  }

  public bool AddChosenEntity(Entity entity)
  {
    if (this.m_chosenEntities.Contains(entity))
      return false;
    this.m_chosenEntities.Add(entity);
    Card card = entity.GetCard();
    if ((UnityEngine.Object) card != (UnityEngine.Object) null)
      card.UpdateActorState();
    return true;
  }

  public bool RemoveChosenEntity(Entity entity)
  {
    if (!this.m_chosenEntities.Remove(entity))
      return false;
    Card card = entity.GetCard();
    if ((UnityEngine.Object) card != (UnityEngine.Object) null)
      card.UpdateActorState();
    return true;
  }

  public List<Entity> GetChosenEntities()
  {
    return this.m_chosenEntities;
  }

  public Network.Options GetOptionsPacket()
  {
    return this.m_options;
  }

  public void EnterChoiceMode()
  {
    this.m_responseMode = GameState.ResponseMode.CHOICE;
    this.UpdateOptionHighlights();
    this.UpdateChoiceHighlights();
  }

  public void EnterMainOptionMode()
  {
    GameState.ResponseMode responseMode = this.m_responseMode;
    this.m_responseMode = GameState.ResponseMode.OPTION;
    if (responseMode == GameState.ResponseMode.SUB_OPTION)
      this.UpdateSubOptionHighlights(this.m_options.List[this.m_selectedOption.m_main]);
    else if (responseMode == GameState.ResponseMode.OPTION_TARGET)
    {
      Network.Options.Option option = this.m_options.List[this.m_selectedOption.m_main];
      this.UpdateTargetHighlights(option.Main);
      if (this.m_selectedOption.m_sub != -1)
        this.UpdateTargetHighlights(option.Subs[this.m_selectedOption.m_sub]);
    }
    this.UpdateOptionHighlights(this.m_lastOptions);
    this.UpdateOptionHighlights();
    this.m_selectedOption.Clear();
  }

  public void EnterSubOptionMode()
  {
    Network.Options.Option option = this.m_options.List[this.m_selectedOption.m_main];
    if (this.m_responseMode == GameState.ResponseMode.OPTION)
    {
      this.m_responseMode = GameState.ResponseMode.SUB_OPTION;
      this.UpdateOptionHighlights();
    }
    else if (this.m_responseMode == GameState.ResponseMode.OPTION_TARGET)
    {
      this.m_responseMode = GameState.ResponseMode.SUB_OPTION;
      this.UpdateTargetHighlights(option.Subs[this.m_selectedOption.m_sub]);
    }
    this.UpdateSubOptionHighlights(option);
  }

  public void EnterOptionTargetMode()
  {
    if (this.m_responseMode == GameState.ResponseMode.OPTION)
    {
      this.m_responseMode = GameState.ResponseMode.OPTION_TARGET;
      this.UpdateOptionHighlights();
      this.UpdateTargetHighlights(this.m_options.List[this.m_selectedOption.m_main].Main);
    }
    else
    {
      if (this.m_responseMode != GameState.ResponseMode.SUB_OPTION)
        return;
      this.m_responseMode = GameState.ResponseMode.OPTION_TARGET;
      Network.Options.Option option = this.m_options.List[this.m_selectedOption.m_main];
      this.UpdateSubOptionHighlights(option);
      this.UpdateTargetHighlights(option.Subs[this.m_selectedOption.m_sub]);
    }
  }

  public void CancelCurrentOptionMode()
  {
    if (this.IsInTargetMode())
      this.GetGameEntity().NotifyOfTargetModeCancelled();
    this.CancelSelectedOptionProposedMana();
    this.EnterMainOptionMode();
  }

  public bool IsInMainOptionMode()
  {
    return this.m_responseMode == GameState.ResponseMode.OPTION;
  }

  public bool IsInSubOptionMode()
  {
    return this.m_responseMode == GameState.ResponseMode.SUB_OPTION;
  }

  public bool IsInTargetMode()
  {
    return this.m_responseMode == GameState.ResponseMode.OPTION_TARGET;
  }

  public bool IsInChoiceMode()
  {
    return this.m_responseMode == GameState.ResponseMode.CHOICE;
  }

  public void SetSelectedOption(ChooseOption packet)
  {
    this.m_selectedOption.m_main = packet.Index;
    this.m_selectedOption.m_sub = packet.SubOption;
    this.m_selectedOption.m_target = packet.Target;
    this.m_selectedOption.m_position = packet.Position;
  }

  public void SetChosenEntities(ChooseEntities packet)
  {
    this.m_chosenEntities.Clear();
    using (List<int>.Enumerator enumerator = packet.Entities.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity = this.GetEntity(enumerator.Current);
        if (entity != null)
          this.m_chosenEntities.Add(entity);
      }
    }
  }

  public void SetSelectedOption(int index)
  {
    this.m_selectedOption.m_main = index;
  }

  public int GetSelectedOption()
  {
    return this.m_selectedOption.m_main;
  }

  public void SetSelectedSubOption(int index)
  {
    this.m_selectedOption.m_sub = index;
  }

  public int GetSelectedSubOption()
  {
    return this.m_selectedOption.m_sub;
  }

  public void SetSelectedOptionTarget(int target)
  {
    this.m_selectedOption.m_target = target;
  }

  public int GetSelectedOptionTarget()
  {
    return this.m_selectedOption.m_target;
  }

  public bool IsSelectedOptionFriendlyHero()
  {
    Entity hero = this.GetFriendlySidePlayer().GetHero();
    Network.Options.Option selectedNetworkOption = this.GetSelectedNetworkOption();
    if (selectedNetworkOption != null)
      return selectedNetworkOption.Main.ID == hero.GetEntityId();
    return false;
  }

  public void SetSelectedOptionPosition(int position)
  {
    this.m_selectedOption.m_position = position;
  }

  public int GetSelectedOptionPosition()
  {
    return this.m_selectedOption.m_position;
  }

  public Network.Options.Option GetSelectedNetworkOption()
  {
    if (this.m_selectedOption.m_main < 0)
      return (Network.Options.Option) null;
    return this.m_options.List[this.m_selectedOption.m_main];
  }

  public Network.Options.Option.SubOption GetSelectedNetworkSubOption()
  {
    if (this.m_selectedOption.m_main < 0)
      return (Network.Options.Option.SubOption) null;
    Network.Options.Option option = this.m_options.List[this.m_selectedOption.m_main];
    if (this.m_selectedOption.m_sub == -1)
      return option.Main;
    return option.Subs[this.m_selectedOption.m_sub];
  }

  public bool EntityHasSubOptions(Entity entity)
  {
    int entityId = entity.GetEntityId();
    Network.Options optionsPacket = this.GetOptionsPacket();
    if (optionsPacket == null)
      return false;
    for (int index = 0; index < optionsPacket.List.Count; ++index)
    {
      Network.Options.Option option = optionsPacket.List[index];
      if (option.Type == Network.Options.Option.OptionType.POWER && option.Main.ID == entityId)
      {
        if (option.Subs != null)
          return option.Subs.Count > 0;
        return false;
      }
    }
    return false;
  }

  public bool EntityHasTargets(Entity entity)
  {
    return this.EntityHasTargets(entity, false);
  }

  public bool SubEntityHasTargets(Entity subEntity)
  {
    return this.EntityHasTargets(subEntity, true);
  }

  public bool HasSubOptions(Entity entity)
  {
    if (!this.IsEntityInputEnabled(entity))
      return false;
    int entityId = entity.GetEntityId();
    Network.Options optionsPacket = this.GetOptionsPacket();
    for (int index = 0; index < optionsPacket.List.Count; ++index)
    {
      Network.Options.Option option = optionsPacket.List[index];
      if (option.Type == Network.Options.Option.OptionType.POWER && option.Main.ID == entityId)
        return option.Subs.Count > 0;
    }
    return false;
  }

  public bool HasResponse(Entity entity)
  {
    switch (this.GetResponseMode())
    {
      case GameState.ResponseMode.OPTION:
        return this.IsOption(entity);
      case GameState.ResponseMode.SUB_OPTION:
        return this.IsSubOption(entity);
      case GameState.ResponseMode.OPTION_TARGET:
        return this.IsOptionTarget(entity);
      case GameState.ResponseMode.CHOICE:
        return this.IsChoice(entity);
      default:
        return false;
    }
  }

  public bool IsChoice(Entity entity)
  {
    return this.IsEntityInputEnabled(entity) && this.IsChoosableEntity(entity) && !this.IsChosenEntity(entity);
  }

  public bool IsOption(Entity entity)
  {
    if (!this.IsEntityInputEnabled(entity))
      return false;
    int entityId = entity.GetEntityId();
    Network.Options optionsPacket = this.GetOptionsPacket();
    if (optionsPacket == null)
      return false;
    for (int index = 0; index < optionsPacket.List.Count; ++index)
    {
      Network.Options.Option option = optionsPacket.List[index];
      if (option.Type == Network.Options.Option.OptionType.POWER && option.Main.ID == entityId)
        return true;
    }
    return false;
  }

  public bool IsSubOption(Entity entity)
  {
    if (!this.IsEntityInputEnabled(entity))
      return false;
    int entityId = entity.GetEntityId();
    Network.Options.Option selectedNetworkOption = this.GetSelectedNetworkOption();
    for (int index = 0; index < selectedNetworkOption.Subs.Count; ++index)
    {
      if (selectedNetworkOption.Subs[index].ID == entityId)
        return true;
    }
    return false;
  }

  public bool IsOptionTarget(Entity entity)
  {
    if (!this.IsEntityInputEnabled(entity))
      return false;
    int entityId = entity.GetEntityId();
    Network.Options.Option.SubOption networkSubOption = this.GetSelectedNetworkSubOption();
    if (networkSubOption.Targets == null)
      return false;
    for (int index = 0; index < networkSubOption.Targets.Count; ++index)
    {
      if (networkSubOption.Targets[index] == entityId)
        return true;
    }
    return false;
  }

  public bool IsEntityInputEnabled(Entity entity)
  {
    if (this.IsResponsePacketBlocked() || entity.IsBusy())
      return false;
    Card card = entity.GetCard();
    if ((UnityEngine.Object) card != (UnityEngine.Object) null)
    {
      if (!card.IsInputEnabled())
        return false;
      Zone zone = card.GetZone();
      if ((UnityEngine.Object) zone != (UnityEngine.Object) null && !zone.IsInputEnabled())
        return false;
    }
    return true;
  }

  private bool EntityHasTargets(Entity entity, bool isSubEntity)
  {
    int entityId = entity.GetEntityId();
    Network.Options optionsPacket = this.GetOptionsPacket();
    if (optionsPacket == null)
      return false;
    for (int index1 = 0; index1 < optionsPacket.List.Count; ++index1)
    {
      Network.Options.Option option = optionsPacket.List[index1];
      if (option.Type == Network.Options.Option.OptionType.POWER)
      {
        if (isSubEntity)
        {
          if (option.Subs != null)
          {
            for (int index2 = 0; index2 < option.Subs.Count; ++index2)
            {
              Network.Options.Option.SubOption sub = option.Subs[index2];
              if (sub.ID == entityId)
              {
                if (sub.Targets != null)
                  return sub.Targets.Count > 0;
                return false;
              }
            }
          }
        }
        else if (option.Main.ID == entityId)
        {
          if (option.Main.Targets != null)
            return option.Main.Targets.Count > 0;
          return false;
        }
      }
    }
    return false;
  }

  private void CancelSelectedOptionProposedMana()
  {
    Network.Options.Option selectedNetworkOption = this.GetSelectedNetworkOption();
    if (selectedNetworkOption == null)
      return;
    this.GetFriendlySidePlayer().CancelAllProposedMana(this.GetEntity(selectedNetworkOption.Main.ID));
  }

  public void ClearResponseMode()
  {
    Log.Hand.Print("ClearResponseMode");
    this.m_responseMode = GameState.ResponseMode.NONE;
    if (this.m_options != null)
    {
      for (int index = 0; index < this.m_options.List.Count; ++index)
      {
        Network.Options.Option option = this.m_options.List[index];
        if (option.Type == Network.Options.Option.OptionType.POWER)
        {
          Entity entity = this.GetEntity(option.Main.ID);
          if (entity != null)
            entity.ClearBattlecryFlag();
        }
      }
      this.UpdateHighlightsBasedOnSelection();
      this.UpdateOptionHighlights(this.m_options);
    }
    else
    {
      if (this.GetFriendlyEntityChoices() == null)
        return;
      this.UpdateChoiceHighlights();
    }
  }

  public void UpdateChoiceHighlights()
  {
    using (Map<int, Network.EntityChoices>.ValueCollection.Enumerator enumerator1 = this.m_choicesMap.Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Network.EntityChoices current = enumerator1.Current;
        Entity entity1 = this.GetEntity(current.Source);
        if (entity1 != null)
        {
          Card card = entity1.GetCard();
          if ((UnityEngine.Object) card != (UnityEngine.Object) null)
            card.UpdateActorState();
        }
        using (List<int>.Enumerator enumerator2 = current.Entities.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            Entity entity2 = this.GetEntity(enumerator2.Current);
            if (entity2 != null)
            {
              Card card = entity2.GetCard();
              if (!((UnityEngine.Object) card == (UnityEngine.Object) null))
                card.UpdateActorState();
            }
          }
        }
      }
    }
    using (List<Entity>.Enumerator enumerator = this.m_chosenEntities.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card card = enumerator.Current.GetCard();
        if (!((UnityEngine.Object) card == (UnityEngine.Object) null))
          card.UpdateActorState();
      }
    }
  }

  private void UpdateHighlightsBasedOnSelection()
  {
    if (this.m_selectedOption.m_target != 0)
    {
      this.UpdateTargetHighlights(this.GetSelectedNetworkSubOption());
    }
    else
    {
      if (this.m_selectedOption.m_sub < 0)
        return;
      this.UpdateSubOptionHighlights(this.GetSelectedNetworkOption());
    }
  }

  public void UpdateOptionHighlights()
  {
    this.UpdateOptionHighlights(this.m_options);
  }

  public void UpdateOptionHighlights(Network.Options options)
  {
    if (options == null || options.List == null)
      return;
    for (int index = 0; index < options.List.Count; ++index)
    {
      Network.Options.Option option = options.List[index];
      if (option.Type == Network.Options.Option.OptionType.POWER)
      {
        Entity entity = this.GetEntity(option.Main.ID);
        if (entity != null)
        {
          Card card = entity.GetCard();
          if (!((UnityEngine.Object) card == (UnityEngine.Object) null))
            card.UpdateActorState();
        }
      }
    }
  }

  private void UpdateSubOptionHighlights(Network.Options.Option option)
  {
    Entity entity1 = this.GetEntity(option.Main.ID);
    if (entity1 != null)
    {
      Card card = entity1.GetCard();
      if ((UnityEngine.Object) card != (UnityEngine.Object) null)
        card.UpdateActorState();
    }
    using (List<Network.Options.Option.SubOption>.Enumerator enumerator = option.Subs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity2 = this.GetEntity(enumerator.Current.ID);
        if (entity2 != null)
        {
          Card card = entity2.GetCard();
          if (!((UnityEngine.Object) card == (UnityEngine.Object) null))
            card.UpdateActorState();
        }
      }
    }
  }

  private void UpdateTargetHighlights(Network.Options.Option.SubOption subOption)
  {
    Entity entity1 = this.GetEntity(subOption.ID);
    if (entity1 != null)
    {
      Card card = entity1.GetCard();
      if ((UnityEngine.Object) card != (UnityEngine.Object) null)
        card.UpdateActorState();
    }
    using (List<int>.Enumerator enumerator = subOption.Targets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity entity2 = this.GetEntity(enumerator.Current);
        if (entity2 != null)
        {
          Card card = entity2.GetCard();
          if (!((UnityEngine.Object) card == (UnityEngine.Object) null))
            card.UpdateActorState();
        }
      }
    }
  }

  public Network.Options GetLastOptions()
  {
    return this.m_lastOptions;
  }

  public bool FriendlyHeroIsTargetable()
  {
    if (this.m_responseMode == GameState.ResponseMode.OPTION_TARGET)
    {
      Network.Options.Option option = this.m_options.List[this.m_selectedOption.m_main];
      using (List<int>.Enumerator enumerator = (this.m_selectedOption.m_sub == -1 ? option.Main : option.Subs[this.m_selectedOption.m_sub]).Targets.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Entity entity = this.GetEntity(enumerator.Current);
          if (entity != null && !((UnityEngine.Object) entity.GetCard() == (UnityEngine.Object) null) && (entity.IsHero() && entity.IsControlledByFriendlySidePlayer()))
            return true;
        }
      }
    }
    return false;
  }

  private void ClearLastOptions()
  {
    this.m_lastOptions = (Network.Options) null;
    this.m_lastSelectedOption = (GameState.SelectedOption) null;
  }

  private void ClearOptions()
  {
    this.m_options = (Network.Options) null;
    this.m_selectedOption.Clear();
  }

  private void ClearFriendlyChoices()
  {
    this.m_chosenEntities.Clear();
    this.m_choicesMap.Remove(this.GetFriendlyPlayerId());
  }

  private void OnSelectedOptionsSent()
  {
    this.ClearResponseMode();
    this.m_lastOptions = new Network.Options();
    this.m_lastOptions.CopyFrom(this.m_options);
    this.m_lastSelectedOption = new GameState.SelectedOption();
    this.m_lastSelectedOption.CopyFrom(this.m_selectedOption);
    this.ClearOptions();
  }

  private void OnTimeout()
  {
    if (this.m_responseMode == GameState.ResponseMode.NONE)
      return;
    this.ClearResponseMode();
    this.ClearLastOptions();
    this.ClearOptions();
  }

  public void OnRealTimeCreateGame(List<Network.PowerHistory> powerList, int index, Network.HistCreateGame createGame)
  {
    this.m_gameEntity = GameMgr.Get().CreateGameEntity();
    this.m_gameEntity.SetTags(createGame.Game.Tags);
    this.AddEntity((Entity) this.m_gameEntity);
    using (List<Network.HistCreateGame.PlayerData>.Enumerator enumerator = createGame.Players.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.HistCreateGame.PlayerData current = enumerator.Current;
        Player player = new Player();
        player.InitPlayer(current);
        this.AddPlayer(player);
      }
    }
    this.m_createGamePhase = GameState.CreateGamePhase.CREATING;
    this.FireCreateGameEvent();
  }

  public bool OnRealTimeFullEntity(Network.HistFullEntity fullEntity)
  {
    Entity entity = new Entity();
    entity.OnRealTimeFullEntity(fullEntity);
    this.AddEntity(entity);
    return true;
  }

  public bool OnFullEntity(Network.HistFullEntity fullEntity)
  {
    Network.Entity entity1 = fullEntity.Entity;
    Entity entity2 = this.GetEntity(entity1.ID);
    if (entity2 == null)
    {
      Log.Power.PrintWarning("GameState.OnFullEntity() - WARNING entity {0} DOES NOT EXIST!", (object) entity1.ID);
      return false;
    }
    entity2.OnFullEntity(fullEntity);
    return true;
  }

  public bool OnRealTimeShowEntity(Network.HistShowEntity showEntity)
  {
    Network.Entity entity1 = showEntity.Entity;
    Entity entity2 = this.GetEntity(entity1.ID);
    if (entity2 == null)
    {
      Log.Power.PrintWarning("GameState.OnRealTimeShowEntity() - WARNING entity {0} DOES NOT EXIST!", (object) entity1.ID);
      return false;
    }
    entity2.OnRealTimeShowEntity(showEntity);
    return true;
  }

  public bool OnShowEntity(Network.HistShowEntity showEntity)
  {
    Network.Entity entity1 = showEntity.Entity;
    Entity entity2 = this.GetEntity(entity1.ID);
    if (entity2 == null)
    {
      Log.Power.PrintWarning("GameState.OnShowEntity() - WARNING entity {0} DOES NOT EXIST!", (object) entity1.ID);
      return false;
    }
    entity2.OnShowEntity(showEntity);
    return true;
  }

  public bool OnEarlyConcedeShowEntity(Network.HistShowEntity showEntity)
  {
    Network.Entity entity1 = showEntity.Entity;
    Entity entity2 = this.GetEntity(entity1.ID);
    if (entity2 == null)
    {
      Log.Power.PrintWarning("GameState.OnEarlyConcedeShowEntity() - WARNING entity {0} DOES NOT EXIST!", (object) entity1.ID);
      return false;
    }
    entity2.SetTags(entity1.Tags);
    return true;
  }

  public bool OnHideEntity(Network.HistHideEntity hideEntity)
  {
    Entity entity = this.GetEntity(hideEntity.Entity);
    if (entity == null)
    {
      Log.Power.PrintWarning("GameState.OnHideEntity() - WARNING entity {0} DOES NOT EXIST! zone={1}", new object[2]
      {
        (object) hideEntity.Entity,
        (object) hideEntity.Zone
      });
      return false;
    }
    entity.SetTagAndHandleChange<int>(GAME_TAG.ZONE, hideEntity.Zone);
    EntityDef entityDef = entity.GetEntityDef();
    entity.SetTag(GAME_TAG.ATK, entityDef.GetATK());
    entity.SetTag(GAME_TAG.HEALTH, entityDef.GetHealth());
    entity.SetTag(GAME_TAG.DAMAGE, 0);
    return true;
  }

  public bool OnEarlyConcedeHideEntity(Network.HistHideEntity hideEntity)
  {
    Entity entity = this.GetEntity(hideEntity.Entity);
    if (entity == null)
    {
      Log.Power.PrintWarning("GameState.OnEarlyConcedeHideEntity() - WARNING entity {0} DOES NOT EXIST! zone={1}", new object[2]
      {
        (object) hideEntity.Entity,
        (object) hideEntity.Zone
      });
      return false;
    }
    entity.SetTag(GAME_TAG.ZONE, hideEntity.Zone);
    return true;
  }

  public bool OnRealTimeChangeEntity(Network.HistChangeEntity changeEntity)
  {
    Network.Entity entity1 = changeEntity.Entity;
    Entity entity2 = this.GetEntity(entity1.ID);
    if (entity2 == null)
    {
      Log.Power.PrintWarning("GameState.OnRealTimeChangeEntity() - WARNING entity {0} DOES NOT EXIST!", (object) entity1.ID);
      return false;
    }
    entity2.OnRealTimeChangeEntity(changeEntity);
    return true;
  }

  public bool OnChangeEntity(Network.HistChangeEntity changeEntity)
  {
    Network.Entity entity1 = changeEntity.Entity;
    Entity entity2 = this.GetEntity(entity1.ID);
    if (entity2 == null)
    {
      Log.Power.PrintWarning("GameState.OnChangeEntity() - WARNING entity {0} DOES NOT EXIST!", (object) entity1.ID);
      return false;
    }
    entity2.OnChangeEntity(changeEntity);
    return true;
  }

  public bool OnEarlyConcedeChangeEntity(Network.HistChangeEntity changeEntity)
  {
    Network.Entity entity1 = changeEntity.Entity;
    Entity entity2 = this.GetEntity(entity1.ID);
    if (entity2 == null)
    {
      Log.Power.PrintWarning("GameState.OnEarlyConcedeChangeEntity() - WARNING entity {0} DOES NOT EXIST!", (object) entity1.ID);
      return false;
    }
    entity2.SetTags(entity1.Tags);
    return true;
  }

  public bool OnRealTimeTagChange(Network.HistTagChange change)
  {
    Entity entity = GameState.Get().GetEntity(change.Entity);
    if (entity == null)
    {
      Log.Power.PrintWarning("GameState.OnRealTimeTagChange() - WARNING Entity {0} does not exist", (object) change.Entity);
      return false;
    }
    this.PreprocessRealTimeTagChange(entity, change);
    this.m_gameEntity.NotifyOfRealTimeTagChange(entity, change);
    entity.OnRealTimeTagChanged(change);
    return true;
  }

  public bool OnTagChange(Network.HistTagChange netChange)
  {
    Entity entity = this.GetEntity(netChange.Entity);
    if (entity == null)
    {
      UnityEngine.Debug.LogWarningFormat("GameState.OnTagChange() - WARNING Entity {0} does not exist", (object) netChange.Entity);
      return false;
    }
    TagDelta change = new TagDelta();
    change.tag = netChange.Tag;
    change.oldValue = entity.GetTag(netChange.Tag);
    change.newValue = netChange.Value;
    entity.SetTag(change.tag, change.newValue);
    this.PreprocessTagChange(entity, change);
    entity.OnTagChanged(change);
    return true;
  }

  public bool OnEarlyConcedeTagChange(Network.HistTagChange netChange)
  {
    Entity entity = this.GetEntity(netChange.Entity);
    if (entity == null)
    {
      UnityEngine.Debug.LogWarningFormat("GameState.OnEarlyConcedeTagChange() - WARNING Entity {0} does not exist", (object) netChange.Entity);
      return false;
    }
    TagDelta change = new TagDelta();
    change.tag = netChange.Tag;
    change.oldValue = entity.GetTag(netChange.Tag);
    change.newValue = netChange.Value;
    entity.SetTag(change.tag, change.newValue);
    this.PreprocessEarlyConcedeTagChange(entity, change);
    this.ProcessEarlyConcedeTagChange(entity, change);
    return true;
  }

  public bool OnMetaData(Network.HistMetaData metaData)
  {
    this.m_powerProcessor.OnMetaData(metaData);
    using (List<int>.Enumerator enumerator = metaData.Info.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        Entity entity = this.GetEntity(current);
        if (entity == null)
        {
          UnityEngine.Debug.LogWarning((object) string.Format("GameState.OnMetaData() - WARNING Entity {0} does not exist", (object) current));
          return false;
        }
        entity.OnMetaData(metaData);
      }
    }
    return true;
  }

  public void OnTaskListEnded(PowerTaskList taskList)
  {
    if (taskList == null)
      return;
    using (List<PowerTask>.Enumerator enumerator = taskList.GetTaskList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.GetPower().Type == Network.PowerType.CREATE_GAME)
        {
          this.m_createGamePhase = GameState.CreateGamePhase.CREATED;
          this.FireCreateGameEvent();
          this.m_createGameListeners.Clear();
        }
      }
    }
  }

  public void OnPowerHistory(List<Network.PowerHistory> powerList)
  {
    this.DebugPrintPowerList(powerList);
    bool flag1 = this.m_powerProcessor.HasEarlyConcedeTaskList();
    this.m_powerProcessor.OnPowerHistory(powerList);
    this.ProcessAllQueuedChoices();
    bool flag2 = this.m_powerProcessor.HasEarlyConcedeTaskList();
    if (flag1 || !flag2)
      return;
    this.OnReceivedEarlyConcede();
  }

  private void OnReceivedEarlyConcede()
  {
    this.ClearResponseMode();
    this.ClearLastOptions();
    this.ClearOptions();
  }

  public void OnAllOptions(Network.Options options)
  {
    this.m_responseMode = GameState.ResponseMode.OPTION;
    this.m_chosenEntities.Clear();
    if (this.m_options != null && (this.m_lastOptions == null || this.m_lastOptions.ID < this.m_options.ID))
    {
      this.m_lastOptions = new Network.Options();
      this.m_lastOptions.CopyFrom(this.m_options);
    }
    this.m_options = options;
    using (List<Network.Options.Option>.Enumerator enumerator = this.m_options.List.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Options.Option current = enumerator.Current;
        if (current.Type == Network.Options.Option.OptionType.POWER)
        {
          Entity entity = this.GetEntity(current.Main.ID);
          if (entity != null && current.Main.Targets != null && current.Main.Targets.Count > 0)
            entity.UpdateUseBattlecryFlag(true);
        }
      }
    }
    this.DebugPrintOptions();
    this.EnterMainOptionMode();
    this.FireOptionsReceivedEvent();
  }

  public void OnEntityChoices(Network.EntityChoices choices)
  {
    PowerTaskList lastTaskList = this.m_powerProcessor.GetLastTaskList();
    if (!this.CanProcessEntityChoices(choices))
    {
      Log.Power.Print("GameState.OnEntityChoices() - id={0} playerId={1} queued", new object[2]
      {
        (object) choices.ID,
        (object) choices.PlayerId
      });
      this.m_queuedChoices.Enqueue(new GameState.QueuedChoice()
      {
        m_type = GameState.QueuedChoice.PacketType.ENTITY_CHOICES,
        m_packet = (object) choices,
        m_eventData = (object) lastTaskList
      });
    }
    else
      this.ProcessEntityChoices(choices, lastTaskList);
  }

  public void OnEntitiesChosen(Network.EntitiesChosen chosen)
  {
    if (!this.CanProcessEntitiesChosen(chosen))
    {
      Log.Power.Print("GameState.OnEntitiesChosen() - id={0} playerId={1} queued", new object[2]
      {
        (object) chosen.ID,
        (object) chosen.PlayerId
      });
      this.m_queuedChoices.Enqueue(new GameState.QueuedChoice()
      {
        m_type = GameState.QueuedChoice.PacketType.ENTITIES_CHOSEN,
        m_packet = (object) chosen
      });
    }
    else
      this.ProcessEntitiesChosen(chosen);
  }

  private bool CanProcessEntityChoices(Network.EntityChoices choices)
  {
    int playerId = choices.PlayerId;
    if (!this.m_playerMap.ContainsKey(playerId))
      return false;
    using (List<int>.Enumerator enumerator = choices.Entities.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (!this.m_entityMap.ContainsKey(enumerator.Current))
          return false;
      }
    }
    return !this.m_choicesMap.ContainsKey(playerId);
  }

  private bool CanProcessEntitiesChosen(Network.EntitiesChosen chosen)
  {
    int playerId = chosen.PlayerId;
    if (!this.m_playerMap.ContainsKey(playerId))
      return false;
    using (List<int>.Enumerator enumerator = chosen.Entities.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (!this.m_entityMap.ContainsKey(enumerator.Current))
          return false;
      }
    }
    Network.EntityChoices entityChoices;
    return !this.m_choicesMap.TryGetValue(playerId, out entityChoices) || entityChoices.ID == chosen.ID;
  }

  private void ProcessAllQueuedChoices()
  {
    while (this.m_queuedChoices.Count > 0)
    {
      GameState.QueuedChoice queuedChoice = this.m_queuedChoices.Peek();
      switch (queuedChoice.m_type)
      {
        case GameState.QueuedChoice.PacketType.ENTITY_CHOICES:
          Network.EntityChoices packet1 = (Network.EntityChoices) queuedChoice.m_packet;
          if (!this.CanProcessEntityChoices(packet1))
            return;
          this.m_queuedChoices.Dequeue();
          PowerTaskList eventData = (PowerTaskList) queuedChoice.m_eventData;
          this.ProcessEntityChoices(packet1, eventData);
          continue;
        case GameState.QueuedChoice.PacketType.ENTITIES_CHOSEN:
          Network.EntitiesChosen packet2 = (Network.EntitiesChosen) queuedChoice.m_packet;
          if (!this.CanProcessEntitiesChosen(packet2))
            return;
          this.m_queuedChoices.Dequeue();
          this.ProcessEntitiesChosen(packet2);
          continue;
        default:
          continue;
      }
    }
  }

  private void ProcessEntityChoices(Network.EntityChoices choices, PowerTaskList preChoiceTaskList)
  {
    this.DebugPrintEntityChoices(choices, preChoiceTaskList);
    if (this.m_powerProcessor.HasEarlyConcedeTaskList())
      return;
    int playerId = choices.PlayerId;
    this.m_choicesMap[playerId] = choices;
    int friendlyPlayerId = this.GetFriendlyPlayerId();
    if (playerId == friendlyPlayerId)
    {
      this.m_responseMode = GameState.ResponseMode.CHOICE;
      this.m_chosenEntities.Clear();
      this.EnterChoiceMode();
    }
    this.FireEntityChoicesReceivedEvent(choices, preChoiceTaskList);
  }

  private void ProcessEntitiesChosen(Network.EntitiesChosen chosen)
  {
    this.DebugPrintEntitiesChosen(chosen);
    if (this.m_powerProcessor.HasEarlyConcedeTaskList() || this.FireEntitiesChosenReceivedEvent(chosen))
      return;
    this.OnEntitiesChosenProcessed(chosen);
  }

  public void OnGameSetup(Network.GameSetup setup)
  {
    this.m_maxSecretsPerPlayer = setup.MaxSecretsPerPlayer;
    this.m_maxFriendlyMinionsPerPlayer = setup.MaxFriendlyMinionsPerPlayer;
  }

  public void OnOptionRejected(int optionId)
  {
    if (this.m_lastSelectedOption == null)
      UnityEngine.Debug.LogError((object) "GameState.OnOptionRejected() - got an option rejection without a last selected option");
    else if (this.m_lastOptions.ID != optionId)
    {
      UnityEngine.Debug.LogErrorFormat("GameState.OnOptionRejected() - rejected option id ({0}) does not match last option id ({1})", new object[2]
      {
        (object) optionId,
        (object) this.m_lastOptions.ID
      });
    }
    else
    {
      this.FireOptionRejectedEvent(this.m_lastOptions.List[this.m_lastSelectedOption.m_main]);
      this.ClearLastOptions();
    }
  }

  public void OnTurnTimerUpdate(Network.TurnTimerInfo info)
  {
    TurnTimerUpdate update = new TurnTimerUpdate();
    update.SetSecondsRemaining(info.Seconds);
    update.SetEndTimestamp(Time.realtimeSinceStartup + info.Seconds);
    update.SetShow(info.Show);
    int num = this.m_gameEntity != null ? this.m_gameEntity.GetTag(GAME_TAG.TURN) : 0;
    if (info.Turn > num)
      this.m_turnTimerUpdates[info.Turn] = update;
    else
      this.TriggerTurnTimerUpdate(update);
  }

  public void OnSpectatorNotifyEvent(SpectatorNotify notify)
  {
    this.FireSpectatorNotifyEvent(notify);
  }

  public void SendChoices()
  {
    if (this.m_responseMode != GameState.ResponseMode.CHOICE)
      return;
    Network.EntityChoices friendlyEntityChoices = this.GetFriendlyEntityChoices();
    if (friendlyEntityChoices == null)
      return;
    Log.Power.Print("GameState.SendChoices() - id={0} ChoiceType={1}", new object[2]
    {
      (object) friendlyEntityChoices.ID,
      (object) friendlyEntityChoices.ChoiceType
    });
    List<int> picks = new List<int>();
    for (int index = 0; index < this.m_chosenEntities.Count; ++index)
    {
      Entity chosenEntity = this.m_chosenEntities[index];
      int entityId = chosenEntity.GetEntityId();
      Log.Power.Print("GameState.SendChoices() -   m_chosenEntities[{0}]={1}", new object[2]
      {
        (object) index,
        (object) chosenEntity
      });
      picks.Add(entityId);
    }
    if (!GameMgr.Get().IsSpectator())
      Network.Get().SendChoices(friendlyEntityChoices.ID, picks);
    this.ClearResponseMode();
    this.ClearFriendlyChoices();
    this.ProcessAllQueuedChoices();
  }

  public void OnEntitiesChosenProcessed(Network.EntitiesChosen chosen)
  {
    int playerId = chosen.PlayerId;
    int friendlyPlayerId = this.GetFriendlyPlayerId();
    if (playerId == friendlyPlayerId)
    {
      this.ClearResponseMode();
      this.ClearFriendlyChoices();
    }
    else
      this.m_choicesMap.Remove(playerId);
    this.ProcessAllQueuedChoices();
  }

  public void SendOption()
  {
    if (!GameMgr.Get().IsSpectator())
    {
      Network.Get().SendOption(this.m_options.ID, this.m_selectedOption.m_main, this.m_selectedOption.m_target, this.m_selectedOption.m_sub, this.m_selectedOption.m_position);
      Log.Power.Print("GameState.SendOption() - selectedOption={0} selectedSubOption={1} selectedTarget={2} selectedPosition={3}", (object) this.m_selectedOption.m_main, (object) this.m_selectedOption.m_sub, (object) this.m_selectedOption.m_target, (object) this.m_selectedOption.m_position);
    }
    this.OnSelectedOptionsSent();
    this.FireOptionsSentEvent(this.m_lastOptions.List[this.m_lastSelectedOption.m_main]);
  }

  public PlayErrors.GameStateInfo ConvertToGameStateInfo()
  {
    PlayErrors.GameStateInfo gameStateInfo = new PlayErrors.GameStateInfo();
    GameState gameState = GameState.Get();
    gameStateInfo.currentStep = (TAG_STEP) gameState.GetGameEntity().GetTag(GAME_TAG.STEP);
    return gameStateInfo;
  }

  private void OnTurnChanged_TurnTimer(int oldTurn, int newTurn)
  {
    TurnTimerUpdate update;
    if (this.m_turnTimerUpdates.Count == 0 || !this.m_turnTimerUpdates.TryGetValue(newTurn, out update))
      return;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    float sec = Mathf.Max(0.0f, update.GetEndTimestamp() - realtimeSinceStartup);
    update.SetSecondsRemaining(sec);
    this.TriggerTurnTimerUpdate(update);
    this.m_turnTimerUpdates.Remove(newTurn);
  }

  private void TriggerTurnTimerUpdate(TurnTimerUpdate update)
  {
    this.FireTurnTimerUpdateEvent(update);
    if ((double) update.GetSecondsRemaining() > (double) Mathf.Epsilon)
      return;
    this.OnTimeout();
  }

  private void DebugPrintPowerList(List<Network.PowerHistory> powerList)
  {
    if (!Log.Power.CanPrint())
      return;
    string empty = string.Empty;
    Log.Power.Print(string.Format("GameState.DebugPrintPowerList() - Count={0}", (object) powerList.Count));
    for (int index = 0; index < powerList.Count; ++index)
    {
      Network.PowerHistory power = powerList[index];
      this.DebugPrintPower(Log.Power, "GameState", power, ref empty);
    }
  }

  public void DebugPrintPower(Logger logger, string callerName, Network.PowerHistory power)
  {
    string empty = string.Empty;
    this.DebugPrintPower(logger, callerName, power, ref empty);
  }

  public void DebugPrintPower(Logger logger, string callerName, Network.PowerHistory power, ref string indentation)
  {
    if (!Log.Power.CanPrint())
      return;
    switch (power.Type)
    {
      case Network.PowerType.FULL_ENTITY:
        Network.Entity entity1 = ((Network.HistFullEntity) power).Entity;
        Entity entity2 = this.GetEntity(entity1.ID);
        if (entity2 == null)
          logger.Print("{0}.DebugPrintPower() - {1}FULL_ENTITY - Creating ID={2} CardID={3}", (object) callerName, (object) indentation, (object) entity1.ID, (object) entity1.CardID);
        else
          logger.Print("{0}.DebugPrintPower() - {1}FULL_ENTITY - Updating {2} CardID={3}", (object) callerName, (object) indentation, (object) entity2, (object) entity1.CardID);
        this.DebugPrintTags(logger, callerName, indentation, entity1);
        break;
      case Network.PowerType.SHOW_ENTITY:
        Network.Entity entity3 = ((Network.HistShowEntity) power).Entity;
        object printableEntity1 = this.GetPrintableEntity(entity3.ID);
        logger.Print("{0}.DebugPrintPower() - {1}SHOW_ENTITY - Updating Entity={2} CardID={3}", (object) callerName, (object) indentation, printableEntity1, (object) entity3.CardID);
        this.DebugPrintTags(logger, callerName, indentation, entity3);
        break;
      case Network.PowerType.HIDE_ENTITY:
        Network.HistHideEntity histHideEntity = (Network.HistHideEntity) power;
        object printableEntity2 = this.GetPrintableEntity(histHideEntity.Entity);
        logger.Print("{0}.DebugPrintPower() - {1}HIDE_ENTITY - Entity={2} {3}", (object) callerName, (object) indentation, printableEntity2, (object) Tags.DebugTag(49, histHideEntity.Zone));
        break;
      case Network.PowerType.TAG_CHANGE:
        Network.HistTagChange histTagChange = (Network.HistTagChange) power;
        object printableEntity3 = this.GetPrintableEntity(histTagChange.Entity);
        logger.Print("{0}.DebugPrintPower() - {1}TAG_CHANGE Entity={2} {3}", (object) callerName, (object) indentation, printableEntity3, (object) Tags.DebugTag(histTagChange.Tag, histTagChange.Value));
        break;
      case Network.PowerType.BLOCK_START:
        Network.HistBlockStart histBlockStart = (Network.HistBlockStart) power;
        object printableEntity4 = this.GetPrintableEntity(histBlockStart.Entity);
        object printableEntity5 = this.GetPrintableEntity(histBlockStart.Target);
        logger.Print("{0}.DebugPrintPower() - {1}BLOCK_START BlockType={2} Entity={3} EffectCardId={4} EffectIndex={5} Target={6}", (object) callerName, (object) indentation, (object) histBlockStart.BlockType, printableEntity4, (object) histBlockStart.EffectCardId, (object) histBlockStart.EffectIndex, printableEntity5);
        indentation = indentation + "    ";
        break;
      case Network.PowerType.BLOCK_END:
        if (indentation.Length >= "    ".Length)
          indentation = indentation.Remove(indentation.Length - "    ".Length);
        logger.Print("{0}.DebugPrintPower() - {1}BLOCK_END", new object[2]
        {
          (object) callerName,
          (object) indentation
        });
        break;
      case Network.PowerType.CREATE_GAME:
        Network.HistCreateGame histCreateGame = (Network.HistCreateGame) power;
        logger.Print("{0}.DebugPrintPower() - {1}CREATE_GAME", new object[2]
        {
          (object) callerName,
          (object) indentation
        });
        indentation = indentation + "    ";
        logger.Print("{0}.DebugPrintPower() - {1}GameEntity EntityID={2}", new object[3]
        {
          (object) callerName,
          (object) indentation,
          (object) histCreateGame.Game.ID
        });
        this.DebugPrintTags(logger, callerName, indentation, histCreateGame.Game);
        using (List<Network.HistCreateGame.PlayerData>.Enumerator enumerator = histCreateGame.Players.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Network.HistCreateGame.PlayerData current = enumerator.Current;
            logger.Print("{0}.DebugPrintPower() - {1}Player EntityID={2} PlayerID={3} GameAccountId={4}", (object) callerName, (object) indentation, (object) current.Player.ID, (object) current.ID, (object) current.GameAccountId);
            this.DebugPrintTags(logger, callerName, indentation, current.Player);
          }
        }
        indentation = indentation.Remove(indentation.Length - "    ".Length);
        break;
      case Network.PowerType.META_DATA:
        Network.HistMetaData histMetaData = (Network.HistMetaData) power;
        object obj = (object) histMetaData.Data;
        if (histMetaData.MetaType == HistoryMeta.Type.JOUST)
          obj = this.GetPrintableEntity(histMetaData.Data);
        logger.Print("{0}.DebugPrintPower() - {1}META_DATA - Meta={2} Data={3} Info={4}", (object) callerName, (object) indentation, (object) histMetaData.MetaType, obj, (object) histMetaData.Info.Count);
        if (histMetaData.Info.Count <= 0 || !logger.IsVerbose())
          break;
        indentation = indentation + "    ";
        for (int index = 0; index < histMetaData.Info.Count; ++index)
        {
          object printableEntity6 = this.GetPrintableEntity(histMetaData.Info[index]);
          logger.Print(1 != 0, "{0}.DebugPrintPower() - {1}Info[{2}] = {3}", (object) callerName, (object) indentation, (object) index, printableEntity6);
        }
        indentation = indentation.Remove(indentation.Length - "    ".Length);
        break;
      case Network.PowerType.CHANGE_ENTITY:
        Network.Entity entity4 = ((Network.HistChangeEntity) power).Entity;
        object printableEntity7 = this.GetPrintableEntity(entity4.ID);
        logger.Print("{0}.DebugPrintPower() - {1}CHANGE_ENTITY - Updating Entity={2} CardID={3}", (object) callerName, (object) indentation, printableEntity7, (object) entity4.CardID);
        this.DebugPrintTags(logger, callerName, indentation, entity4);
        break;
      default:
        logger.Print("{0}.DebugPrintPower() - ERROR: unhandled PowType {1}", new object[2]
        {
          (object) callerName,
          (object) power.Type
        });
        break;
    }
  }

  private void DebugPrintTags(Logger logger, string callerName, string indentation, Network.Entity netEntity)
  {
    if (!Log.Power.CanPrint())
      return;
    if (indentation != null)
      indentation += "    ";
    for (int index = 0; index < netEntity.Tags.Count; ++index)
    {
      Network.Entity.Tag tag = netEntity.Tags[index];
      logger.Print("{0}.DebugPrintPower() - {1}{2}", new object[3]
      {
        (object) callerName,
        (object) indentation,
        (object) Tags.DebugTag(tag.Name, tag.Value)
      });
    }
  }

  private void DebugPrintOptions()
  {
    if (!Log.Power.CanPrint())
      return;
    Log.Power.Print("GameState.DebugPrintOptions() - id={0}", (object) this.m_options.ID);
    for (int index1 = 0; index1 < this.m_options.List.Count; ++index1)
    {
      Network.Options.Option option = this.m_options.List[index1];
      Entity entity1 = this.GetEntity(option.Main.ID);
      Log.Power.Print("GameState.DebugPrintOptions() -   option {0} type={1} mainEntity={2}", new object[3]
      {
        (object) index1,
        (object) option.Type,
        (object) entity1
      });
      if (option.Main.Targets != null)
      {
        for (int index2 = 0; index2 < option.Main.Targets.Count; ++index2)
        {
          Entity entity2 = this.GetEntity(option.Main.Targets[index2]);
          Log.Power.Print("GameState.DebugPrintOptions() -     target {0} entity={1}", new object[2]
          {
            (object) index2,
            (object) entity2
          });
        }
      }
      for (int index2 = 0; index2 < option.Subs.Count; ++index2)
      {
        Network.Options.Option.SubOption sub = option.Subs[index2];
        Entity entity2 = this.GetEntity(sub.ID);
        Log.Power.Print("GameState.DebugPrintOptions() -     subOption {0} entity={1}", new object[2]
        {
          (object) index2,
          (object) entity2
        });
        if (sub.Targets != null)
        {
          for (int index3 = 0; index3 < sub.Targets.Count; ++index3)
          {
            Entity entity3 = this.GetEntity(sub.Targets[index3]);
            Log.Power.Print("GameState.DebugPrintOptions() -       target {0} entity={1}", new object[2]
            {
              (object) index3,
              (object) entity3
            });
          }
        }
      }
    }
  }

  private void DebugPrintEntityChoices(Network.EntityChoices choices, PowerTaskList preChoiceTaskList)
  {
    if (!Log.Power.CanPrint())
      return;
    object printableEntity1 = this.GetPrintableEntity(this.GetPlayer(choices.PlayerId).GetEntityId());
    object obj = (object) null;
    if (preChoiceTaskList != null)
      obj = (object) preChoiceTaskList.GetId();
    Log.Power.Print("GameState.DebugPrintEntityChoices() - id={0} Player={1} TaskList={2} ChoiceType={3} CountMin={4} CountMax={5}", (object) choices.ID, printableEntity1, obj, (object) choices.ChoiceType, (object) choices.CountMin, (object) choices.CountMax);
    object printableEntity2 = this.GetPrintableEntity(choices.Source);
    Log.Power.Print("GameState.DebugPrintEntityChoices() -   Source={0}", printableEntity2);
    for (int index = 0; index < choices.Entities.Count; ++index)
    {
      object printableEntity3 = this.GetPrintableEntity(choices.Entities[index]);
      Log.Power.Print("GameState.DebugPrintEntityChoices() -   Entities[{0}]={1}", new object[2]
      {
        (object) index,
        printableEntity3
      });
    }
  }

  private void DebugPrintEntitiesChosen(Network.EntitiesChosen chosen)
  {
    if (!Log.Power.CanPrint())
      return;
    object printableEntity1 = this.GetPrintableEntity(this.GetPlayer(chosen.PlayerId).GetEntityId());
    Log.Power.Print("GameState.DebugPrintEntitiesChosen() - id={0} Player={1} EntitiesCount={2}", new object[3]
    {
      (object) chosen.ID,
      printableEntity1,
      (object) chosen.Entities.Count
    });
    for (int index = 0; index < chosen.Entities.Count; ++index)
    {
      object printableEntity2 = this.GetPrintableEntity(chosen.Entities[index]);
      Log.Power.Print("GameState.DebugPrintEntitiesChosen() -   Entities[{0}]={1}", new object[2]
      {
        (object) index,
        printableEntity2
      });
    }
  }

  private object GetPrintableEntity(int id)
  {
    return (object) this.GetEntity(id) ?? (object) id;
  }

  private void PrintBlockingTaskList(StringBuilder builder, PowerTaskList taskList)
  {
    if (taskList == null)
    {
      builder.Append("null");
    }
    else
    {
      builder.AppendFormat("ID={0} ", (object) taskList.GetId());
      builder.Append("Source=[");
      Network.HistBlockStart blockStart = taskList.GetBlockStart();
      if (blockStart == null)
      {
        builder.Append("null");
      }
      else
      {
        builder.AppendFormat("BlockType={0}", (object) blockStart.BlockType);
        builder.Append(' ');
        object printableEntity1 = this.GetPrintableEntity(blockStart.Entity);
        builder.AppendFormat("Entity={0}", printableEntity1);
        builder.Append(' ');
        object printableEntity2 = this.GetPrintableEntity(blockStart.Target);
        builder.AppendFormat("Target={0}", printableEntity2);
      }
      builder.Append(']');
      builder.AppendFormat(" Tasks={0}", (object) taskList.GetTaskList().Count);
    }
  }

  private void QuickGameFlipHeroesCheat(List<Network.PowerHistory> powerList)
  {
  }

  public enum ResponseMode
  {
    NONE,
    OPTION,
    SUB_OPTION,
    OPTION_TARGET,
    CHOICE,
  }

  public enum CreateGamePhase
  {
    INVALID,
    CREATING,
    CREATED,
  }

  private class SelectedOption
  {
    public int m_main = -1;
    public int m_sub = -1;
    public int m_target;
    public int m_position;

    public void Clear()
    {
      this.m_main = -1;
      this.m_sub = -1;
      this.m_target = 0;
      this.m_position = 0;
    }

    public void CopyFrom(GameState.SelectedOption original)
    {
      this.m_main = original.m_main;
      this.m_sub = original.m_sub;
      this.m_target = original.m_target;
      this.m_position = original.m_position;
    }
  }

  private class QueuedChoice
  {
    public GameState.QueuedChoice.PacketType m_type;
    public object m_packet;
    public object m_eventData;

    public enum PacketType
    {
      ENTITY_CHOICES,
      ENTITIES_CHOSEN,
    }
  }

  private class GameStateInitializedListener : EventListener<GameState.GameStateInitializedCallback>
  {
    public void Fire(GameState instance)
    {
      this.m_callback(instance, this.m_userData);
    }
  }

  private class CreateGameListener : EventListener<GameState.CreateGameCallback>
  {
    public void Fire(GameState.CreateGamePhase phase)
    {
      this.m_callback(phase, this.m_userData);
    }
  }

  private class OptionsReceivedListener : EventListener<GameState.OptionsReceivedCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  private class OptionsSentListener : EventListener<GameState.OptionsSentCallback>
  {
    public void Fire(Network.Options.Option option)
    {
      this.m_callback(option, this.m_userData);
    }
  }

  private class OptionRejectedListener : EventListener<GameState.OptionRejectedCallback>
  {
    public void Fire(Network.Options.Option option)
    {
      this.m_callback(option, this.m_userData);
    }
  }

  private class EntityChoicesReceivedListener : EventListener<GameState.EntityChoicesReceivedCallback>
  {
    public void Fire(Network.EntityChoices choices, PowerTaskList preChoiceTaskList)
    {
      this.m_callback(choices, preChoiceTaskList, this.m_userData);
    }
  }

  private class EntitiesChosenReceivedListener : EventListener<GameState.EntitiesChosenReceivedCallback>
  {
    public bool Fire(Network.EntitiesChosen chosen)
    {
      return this.m_callback(chosen, this.m_userData);
    }
  }

  private class CurrentPlayerChangedListener : EventListener<GameState.CurrentPlayerChangedCallback>
  {
    public void Fire(Player player)
    {
      this.m_callback(player, this.m_userData);
    }
  }

  private class TurnChangedListener : EventListener<GameState.TurnChangedCallback>
  {
    public void Fire(int oldTurn, int newTurn)
    {
      this.m_callback(oldTurn, newTurn, this.m_userData);
    }
  }

  private class FriendlyTurnStartedListener : EventListener<GameState.FriendlyTurnStartedCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  private class TurnTimerUpdateListener : EventListener<GameState.TurnTimerUpdateCallback>
  {
    public void Fire(TurnTimerUpdate update)
    {
      this.m_callback(update, this.m_userData);
    }
  }

  private class SpectatorNotifyListener : EventListener<GameState.SpectatorNotifyEventCallback>
  {
    public void Fire(SpectatorNotify notify)
    {
      this.m_callback(notify, this.m_userData);
    }
  }

  private class GameOverListener : EventListener<GameState.GameOverCallback>
  {
    public void Fire(TAG_PLAYSTATE playState)
    {
      this.m_callback(playState, this.m_userData);
    }
  }

  private class HeroChangedListener : EventListener<GameState.HeroChangedCallback>
  {
    public void Fire(Player player)
    {
      this.m_callback(player, this.m_userData);
    }
  }

  public delegate void GameStateInitializedCallback(GameState instance, object userData);

  public delegate void CreateGameCallback(GameState.CreateGamePhase phase, object userData);

  public delegate void OptionsReceivedCallback(object userData);

  public delegate void OptionsSentCallback(Network.Options.Option option, object userData);

  public delegate void OptionRejectedCallback(Network.Options.Option option, object userData);

  public delegate void EntityChoicesReceivedCallback(Network.EntityChoices choices, PowerTaskList preChoiceTaskList, object userData);

  public delegate bool EntitiesChosenReceivedCallback(Network.EntitiesChosen chosen, object userData);

  public delegate void CurrentPlayerChangedCallback(Player player, object userData);

  public delegate void TurnChangedCallback(int oldTurn, int newTurn, object userData);

  public delegate void FriendlyTurnStartedCallback(object userData);

  public delegate void TurnTimerUpdateCallback(TurnTimerUpdate update, object userData);

  public delegate void SpectatorNotifyEventCallback(SpectatorNotify notify, object userData);

  public delegate void GameOverCallback(TAG_PLAYSTATE playState, object userData);

  public delegate void HeroChangedCallback(Player player, object userData);

  private delegate void AppendBlockingServerItemCallback<T>(StringBuilder builder, T item);
}
