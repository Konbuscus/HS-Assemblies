﻿// Decompiled with JetBrains decompiler
// Type: FriendListCurrentGameFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using UnityEngine;

public class FriendListCurrentGameFrame : FriendListBaseFriendFrame
{
  public GameObject m_Background;
  public FriendListButton m_PlayButton;

  protected override void Awake()
  {
    base.Awake();
    this.m_PlayButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnPlayButtonPressed));
  }

  private void OnEnable()
  {
    this.UpdateFriend();
  }

  public override void UpdateFriend()
  {
    if (!this.gameObject.activeSelf || this.m_player == null)
      return;
    base.UpdateFriend();
    this.m_PlayerIcon.m_OnlinePortrait.SetProgramId(BnetProgramId.HEARTHSTONE);
    this.m_PlayerNameText.Text = FriendUtils.GetFriendListName(this.m_player, true);
    this.UpdateOnlineStatus();
  }

  private void OnPlayButtonPressed(UIEvent e)
  {
  }
}
