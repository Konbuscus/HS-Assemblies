﻿// Decompiled with JetBrains decompiler
// Type: HistoryTileInitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HistoryTileInitInfo : HistoryItemInitInfo
{
  public HistoryInfoType m_type;
  public List<HistoryInfo> m_childInfos;
  public Texture m_fatigueTexture;
  public Material m_fullTileMaterial;
  public Material m_halfTileMaterial;
  public bool m_dead;
  public int m_splatAmount;
}
