﻿// Decompiled with JetBrains decompiler
// Type: BoxLightMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxLightMgr : MonoBehaviour
{
  private BoxLightStateType m_activeStateType = BoxLightStateType.DEFAULT;
  public List<BoxLightState> m_States;

  private void Start()
  {
    this.UpdateState();
  }

  public BoxLightStateType GetActiveState()
  {
    return this.m_activeStateType;
  }

  public void ChangeState(BoxLightStateType stateType)
  {
    if (stateType == BoxLightStateType.INVALID || this.m_activeStateType == stateType)
      return;
    this.ChangeStateImpl(stateType);
  }

  public void SetState(BoxLightStateType stateType)
  {
    if (this.m_activeStateType == stateType)
      return;
    this.m_activeStateType = stateType;
    this.UpdateState();
  }

  public void UpdateState()
  {
    BoxLightState state = this.FindState(this.m_activeStateType);
    if (state == null)
      return;
    state.m_Spell.ActivateState(SpellStateType.ACTION);
    iTween.Stop(this.gameObject);
    RenderSettings.ambientLight = state.m_AmbientColor;
    if (state.m_LightInfos == null)
      return;
    using (List<BoxLightInfo>.Enumerator enumerator = state.m_LightInfos.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoxLightInfo current = enumerator.Current;
        iTween.Stop(current.m_Light.gameObject);
        current.m_Light.color = current.m_Color;
        current.m_Light.intensity = current.m_Intensity;
        LightType type = current.m_Light.type;
        switch (type)
        {
          case LightType.Point:
          case LightType.Spot:
            current.m_Light.range = current.m_Range;
            if (type == LightType.Spot)
            {
              current.m_Light.spotAngle = current.m_SpotAngle;
              continue;
            }
            continue;
          default:
            continue;
        }
      }
    }
  }

  private BoxLightState FindState(BoxLightStateType stateType)
  {
    using (List<BoxLightState>.Enumerator enumerator = this.m_States.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoxLightState current = enumerator.Current;
        if (current.m_Type == stateType)
          return current;
      }
    }
    return (BoxLightState) null;
  }

  private void ChangeStateImpl(BoxLightStateType stateType)
  {
    this.m_activeStateType = stateType;
    BoxLightState state = this.FindState(stateType);
    if (state == null)
      return;
    iTween.Stop(this.gameObject);
    state.m_Spell.ActivateState(SpellStateType.BIRTH);
    this.ChangeAmbient(state);
    if (state.m_LightInfos == null)
      return;
    using (List<BoxLightInfo>.Enumerator enumerator = state.m_LightInfos.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoxLightInfo current = enumerator.Current;
        this.ChangeLight(state, current);
      }
    }
  }

  private void ChangeAmbient(BoxLightState state)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BoxLightMgr.\u003CChangeAmbient\u003Ec__AnonStorey374 ambientCAnonStorey374 = new BoxLightMgr.\u003CChangeAmbient\u003Ec__AnonStorey374();
    // ISSUE: reference to a compiler-generated field
    ambientCAnonStorey374.state = state;
    // ISSUE: reference to a compiler-generated field
    ambientCAnonStorey374.prevAmbientColor = RenderSettings.ambientLight;
    // ISSUE: reference to a compiler-generated method
    Action<object> action = new Action<object>(ambientCAnonStorey374.\u003C\u003Em__86);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "delay", (object) ambientCAnonStorey374.state.m_DelaySec, (object) "time", (object) ambientCAnonStorey374.state.m_TransitionSec, (object) "easetype", (object) ambientCAnonStorey374.state.m_TransitionEaseType, (object) "onupdate", (object) action));
  }

  private void ChangeLight(BoxLightState state, BoxLightInfo lightInfo)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BoxLightMgr.\u003CChangeLight\u003Ec__AnonStorey375 lightCAnonStorey375 = new BoxLightMgr.\u003CChangeLight\u003Ec__AnonStorey375();
    // ISSUE: reference to a compiler-generated field
    lightCAnonStorey375.lightInfo = lightInfo;
    // ISSUE: reference to a compiler-generated field
    iTween.Stop(lightCAnonStorey375.lightInfo.m_Light.gameObject);
    // ISSUE: reference to a compiler-generated field
    Hashtable args1 = iTween.Hash((object) "color", (object) lightCAnonStorey375.lightInfo.m_Color, (object) "delay", (object) state.m_DelaySec, (object) "time", (object) state.m_TransitionSec, (object) "easetype", (object) state.m_TransitionEaseType);
    // ISSUE: reference to a compiler-generated field
    iTween.ColorTo(lightCAnonStorey375.lightInfo.m_Light.gameObject, args1);
    // ISSUE: reference to a compiler-generated field
    float intensity = lightCAnonStorey375.lightInfo.m_Light.intensity;
    // ISSUE: reference to a compiler-generated method
    Action<object> action1 = new Action<object>(lightCAnonStorey375.\u003C\u003Em__87);
    // ISSUE: reference to a compiler-generated field
    Hashtable args2 = iTween.Hash((object) "from", (object) intensity, (object) "to", (object) lightCAnonStorey375.lightInfo.m_Intensity, (object) "delay", (object) state.m_DelaySec, (object) "time", (object) state.m_TransitionSec, (object) "easetype", (object) state.m_TransitionEaseType, (object) "onupdate", (object) action1);
    // ISSUE: reference to a compiler-generated field
    iTween.ValueTo(lightCAnonStorey375.lightInfo.m_Light.gameObject, args2);
    // ISSUE: reference to a compiler-generated field
    LightType type = lightCAnonStorey375.lightInfo.m_Light.type;
    switch (type)
    {
      case LightType.Point:
      case LightType.Spot:
        // ISSUE: reference to a compiler-generated field
        float range = lightCAnonStorey375.lightInfo.m_Light.range;
        // ISSUE: reference to a compiler-generated method
        Action<object> action2 = new Action<object>(lightCAnonStorey375.\u003C\u003Em__88);
        // ISSUE: reference to a compiler-generated field
        Hashtable args3 = iTween.Hash((object) "from", (object) range, (object) "to", (object) lightCAnonStorey375.lightInfo.m_Range, (object) "delay", (object) state.m_DelaySec, (object) "time", (object) state.m_TransitionSec, (object) "easetype", (object) state.m_TransitionEaseType, (object) "onupdate", (object) action2);
        // ISSUE: reference to a compiler-generated field
        iTween.ValueTo(lightCAnonStorey375.lightInfo.m_Light.gameObject, args3);
        if (type != LightType.Spot)
          break;
        // ISSUE: reference to a compiler-generated field
        float spotAngle = lightCAnonStorey375.lightInfo.m_Light.spotAngle;
        // ISSUE: reference to a compiler-generated method
        Action<object> action3 = new Action<object>(lightCAnonStorey375.\u003C\u003Em__89);
        // ISSUE: reference to a compiler-generated field
        Hashtable args4 = iTween.Hash((object) "from", (object) spotAngle, (object) "to", (object) lightCAnonStorey375.lightInfo.m_SpotAngle, (object) "delay", (object) state.m_DelaySec, (object) "time", (object) state.m_TransitionSec, (object) "easetype", (object) state.m_TransitionEaseType, (object) "onupdate", (object) action3);
        // ISSUE: reference to a compiler-generated field
        iTween.ValueTo(lightCAnonStorey375.lightInfo.m_Light.gameObject, args4);
        break;
    }
  }
}
