﻿// Decompiled with JetBrains decompiler
// Type: Notification
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class Notification : MonoBehaviour
{
  private Map<Notification.SpeechBubbleDirection, Vector3> m_speechBubbleScales = new Map<Notification.SpeechBubbleDirection, Vector3>();
  private const float BOUNCE_SPEED = 0.75f;
  private const float FADE_SPEED = 0.5f;
  private const float FADE_PAUSE = 0.85f;
  private const int MAX_CHARACTERS = 20;
  private const int MAX_CHARACTERS_IN_DIALOG = 28;
  public const float DEATH_ANIMATION_DURATION = 0.5f;
  public UberText speechUberText;
  public UberText headlineUberText;
  public GameObject upperLeftBubble;
  public GameObject bottomLeftBubble;
  public GameObject upperRightBubble;
  public GameObject bottomRightBubble;
  public GameObject bounceObject;
  public GameObject fadeArrowObject;
  public GameObject leftPopupArrow;
  public GameObject rightPopupArrow;
  public GameObject bottomPopupArrow;
  public GameObject topPopupArrow;
  public GameObject bottomLeftPopupArrow;
  public GameObject bottomRightPopupArrow;
  public GameObject topRightPopupArrow;
  public Spell showEvent;
  public Spell destroyEvent;
  public PegUIElement clickOff;
  public bool ignoreAudioOnDestroy;
  public MeshRenderer artOverlay;
  public Material swapMaterial;
  public Action OnFinishDeathState;
  private bool isDying;
  private AudioSource m_accompaniedAudio;
  private Notification.SpeechBubbleDirection m_bubbleDirection;
  private Vector3 m_initialScale;
  private GameObject m_parentOffsetObject;

  private void Start()
  {
    foreach (int num in Enum.GetValues(typeof (Notification.SpeechBubbleDirection)))
    {
      Notification.SpeechBubbleDirection speechBubbleDirection = (Notification.SpeechBubbleDirection) num;
      GameObject speechBubble = this.GetSpeechBubble(speechBubbleDirection);
      if ((UnityEngine.Object) speechBubble != (UnityEngine.Object) null)
        this.m_speechBubbleScales.Add(speechBubbleDirection, speechBubble.transform.localScale);
    }
  }

  private void LateUpdate()
  {
    if (!((UnityEngine.Object) this.upperLeftBubble != (UnityEngine.Object) null) || !((UnityEngine.Object) this.upperRightBubble != (UnityEngine.Object) null) || (!((UnityEngine.Object) this.bottomLeftBubble != (UnityEngine.Object) null) || !((UnityEngine.Object) this.bottomRightBubble != (UnityEngine.Object) null)))
      return;
    this.gameObject.transform.rotation = Quaternion.identity;
  }

  private void OnDestroy()
  {
    if ((bool) ((UnityEngine.Object) this.m_accompaniedAudio) && !this.ignoreAudioOnDestroy && (UnityEngine.Object) SoundManager.Get() != (UnityEngine.Object) null)
      SoundManager.Get().Destroy(this.m_accompaniedAudio);
    if (!((UnityEngine.Object) this.m_parentOffsetObject != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_parentOffsetObject);
  }

  public void ChangeText(string newText)
  {
    this.speechUberText.Text = newText;
  }

  public void ChangeDialogText(string headlineString, string bodyString, string yesOrOKstring, string noString)
  {
    this.speechUberText.Text = bodyString;
    this.headlineUberText.Text = headlineString;
  }

  public void RepositionSpeechBubbleAroundBigQuote(Notification.SpeechBubbleDirection direction, bool animateSpeechBubble)
  {
    GameObject gameObject = this.FaceDirection(direction);
    if (animateSpeechBubble)
      Notification.PlayBirthAnim(gameObject, gameObject.transform.localScale * 0.75f, gameObject.transform.localScale);
    TransformUtil.AttachAndPreserveLocalTransform(this.speechUberText.transform, gameObject.transform);
  }

  public GameObject FaceDirection(Notification.SpeechBubbleDirection direction)
  {
    this.m_bubbleDirection = direction;
    foreach (int num in Enum.GetValues(typeof (Notification.SpeechBubbleDirection)))
    {
      GameObject speechBubble = this.GetSpeechBubble((Notification.SpeechBubbleDirection) num);
      if ((UnityEngine.Object) speechBubble != (UnityEngine.Object) null)
      {
        iTween.Stop(speechBubble);
        speechBubble.GetComponent<Renderer>().enabled = false;
      }
    }
    GameObject speechBubble1 = this.GetSpeechBubble(direction);
    if ((UnityEngine.Object) speechBubble1 != (UnityEngine.Object) null)
    {
      if (this.m_speechBubbleScales.ContainsKey(direction))
        speechBubble1.transform.localScale = this.m_speechBubbleScales[direction];
      speechBubble1.GetComponent<Renderer>().enabled = true;
    }
    return speechBubble1;
  }

  private GameObject GetSpeechBubble(Notification.SpeechBubbleDirection direction)
  {
    switch (direction)
    {
      case Notification.SpeechBubbleDirection.TopLeft:
        return this.upperLeftBubble;
      case Notification.SpeechBubbleDirection.TopRight:
        return this.upperRightBubble;
      case Notification.SpeechBubbleDirection.BottomLeft:
        return this.bottomLeftBubble;
      case Notification.SpeechBubbleDirection.BottomRight:
        return this.bottomRightBubble;
      default:
        return (GameObject) null;
    }
  }

  public void PlaySpeechBubbleDeath()
  {
    Notification.SpeechBubbleDirection bubbleDirection = this.m_bubbleDirection;
    GameObject speechBubble = this.GetSpeechBubble(bubbleDirection);
    if (!((UnityEngine.Object) speechBubble != (UnityEngine.Object) null))
      return;
    iTween.ScaleTo(speechBubble, iTween.Hash((object) "scale", (object) Vector3.zero, (object) "easetype", (object) iTween.EaseType.easeInExpo, (object) "time", (object) 0.5f, (object) "oncomplete", (object) "OnBubbleDeathComplete", (object) "oncompletetarget", (object) this.gameObject, (object) "oncompleteparams", (object) bubbleDirection));
  }

  private void OnBubbleDeathComplete(Notification.SpeechBubbleDirection direction)
  {
    Log.JMac.Print("Bubble death is complete for direction {0}!", (object) direction);
    GameObject speechBubble = this.GetSpeechBubble(direction);
    if (!((UnityEngine.Object) speechBubble != (UnityEngine.Object) null))
      return;
    speechBubble.GetComponent<Renderer>().enabled = false;
  }

  public Notification.SpeechBubbleDirection GetSpeechBubbleDirection()
  {
    return this.m_bubbleDirection;
  }

  public void ShowPopUpArrow(Notification.PopUpArrowDirection direction)
  {
    switch (direction)
    {
      case Notification.PopUpArrowDirection.Left:
        this.leftPopupArrow.GetComponent<Renderer>().enabled = true;
        break;
      case Notification.PopUpArrowDirection.Right:
        this.rightPopupArrow.GetComponent<Renderer>().enabled = true;
        break;
      case Notification.PopUpArrowDirection.Down:
        this.bottomPopupArrow.GetComponent<Renderer>().enabled = true;
        break;
      case Notification.PopUpArrowDirection.Up:
        this.topPopupArrow.GetComponent<Renderer>().enabled = true;
        break;
      case Notification.PopUpArrowDirection.LeftDown:
        this.bottomLeftPopupArrow.GetComponent<Renderer>().enabled = true;
        break;
      case Notification.PopUpArrowDirection.RightDown:
        this.bottomRightPopupArrow.GetComponent<Renderer>().enabled = true;
        break;
      case Notification.PopUpArrowDirection.RightUp:
        this.topRightPopupArrow.GetComponent<Renderer>().enabled = true;
        break;
    }
  }

  public void SetPosition(Actor actor, Notification.SpeechBubbleDirection direction)
  {
    if ((UnityEngine.Object) actor.GetBones() == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Notification Error - Tried to set the position of a Speech Bubble, but the target actor has no bones!");
    }
    else
    {
      GameObject childBySubstring = SceneUtils.FindChildBySubstring(actor.GetBones(), "SpeechBubbleBones");
      if ((UnityEngine.Object) childBySubstring == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "Notification Error - Tried to set the position of a Speech Bubble, but the target actor has no SpeechBubbleBones!");
      }
      else
      {
        Vector3 vector3 = Vector3.zero;
        switch (direction)
        {
          case Notification.SpeechBubbleDirection.TopLeft:
            vector3 = SceneUtils.FindChildBySubstring(childBySubstring, "BottomRight").transform.position;
            break;
          case Notification.SpeechBubbleDirection.TopRight:
            vector3 = SceneUtils.FindChildBySubstring(childBySubstring, "BottomLeft").transform.position;
            break;
          case Notification.SpeechBubbleDirection.BottomLeft:
            vector3 = SceneUtils.FindChildBySubstring(childBySubstring, "TopRight").transform.position;
            break;
          case Notification.SpeechBubbleDirection.BottomRight:
            vector3 = SceneUtils.FindChildBySubstring(childBySubstring, "TopLeft").transform.position;
            break;
        }
        this.transform.position = vector3;
      }
    }
  }

  public void SetPositionForSmallBubble(Actor actor)
  {
    if ((UnityEngine.Object) actor.GetBones() == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Notification Error - Tried to set the position of a Speech Bubble, but the target actor has no bones!");
    }
    else
    {
      GameObject childBySubstring = SceneUtils.FindChildBySubstring(actor.GetBones(), "SpeechBubbleBones");
      if ((UnityEngine.Object) childBySubstring == (UnityEngine.Object) null)
        UnityEngine.Debug.LogError((object) "Notification Error - Tried to set the position of a Speech Bubble, but the target actor has no SpeechBubbleBones!");
      else
        this.transform.position = SceneUtils.FindChildBySubstring(childBySubstring, "SmallBubble").transform.position;
    }
  }

  private void FinishDeath()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    if (this.OnFinishDeathState == null)
      return;
    this.OnFinishDeathState();
  }

  public void PlayDeath()
  {
    if ((UnityEngine.Object) this.destroyEvent != (UnityEngine.Object) null)
      this.destroyEvent.Activate();
    if ((UnityEngine.Object) this.bounceObject != (UnityEngine.Object) null || (UnityEngine.Object) this.fadeArrowObject != (UnityEngine.Object) null)
    {
      this.FinishDeath();
    }
    else
    {
      this.isDying = true;
      iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) Vector3.zero, (object) "easetype", (object) iTween.EaseType.easeInExpo, (object) "time", (object) 0.5f, (object) "oncomplete", (object) "FinishDeath", (object) "oncompletetarget", (object) this.gameObject));
    }
  }

  public void Shrink(float duration = -1f)
  {
    if ((double) duration < 0.0)
      duration = 0.5f;
    iTween.Stop(this.gameObject);
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) Vector3.zero, (object) "easetype", (object) iTween.EaseType.easeInExpo, (object) "time", (object) duration));
  }

  public void Unshrink(float duration = -1f)
  {
    if (this.isDying)
      return;
    if ((double) duration < 0.0)
      duration = 0.5f;
    iTween.Stop(this.gameObject);
    iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) this.m_initialScale, (object) "easetype", (object) iTween.EaseType.easeInExpo, (object) "time", (object) duration));
  }

  public bool IsDying()
  {
    return this.isDying;
  }

  public void PlayBirth()
  {
    if ((UnityEngine.Object) this.showEvent != (UnityEngine.Object) null)
      this.showEvent.Activate();
    if ((UnityEngine.Object) this.bounceObject == (UnityEngine.Object) null && (UnityEngine.Object) this.fadeArrowObject == (UnityEngine.Object) null)
    {
      Vector3 localScale = this.transform.localScale;
      Notification.PlayBirthAnim(this.gameObject, new Vector3(0.01f, 0.01f, 0.01f), localScale);
      this.m_initialScale = localScale;
    }
    else if ((UnityEngine.Object) this.bounceObject != (UnityEngine.Object) null)
    {
      this.BounceDown();
    }
    else
    {
      if (!((UnityEngine.Object) this.fadeArrowObject != (UnityEngine.Object) null))
        return;
      this.FadeOut();
    }
  }

  public void PlayBirthWithForcedScale(Vector3 targetScale)
  {
    Notification.PlayBirthAnim(this.gameObject, this.gameObject.transform.localScale, targetScale);
    this.m_initialScale = this.transform.localScale;
  }

  public void PlaySmallBirthForFakeBubble()
  {
    if ((UnityEngine.Object) this.showEvent != (UnityEngine.Object) null)
      this.showEvent.Activate();
    if ((UnityEngine.Object) this.bounceObject == (UnityEngine.Object) null && (UnityEngine.Object) this.fadeArrowObject == (UnityEngine.Object) null)
    {
      float num = 0.25f;
      Notification.PlayBirthAnim(this.gameObject, new Vector3(0.01f, 0.01f, 0.01f), new Vector3(num * this.transform.localScale.x, num * this.transform.localScale.y, num * this.transform.localScale.z));
    }
    else
      this.BounceDown();
  }

  public static void PlayBirthAnim(GameObject gameObject, Vector3 startingScale, Vector3 targetScale)
  {
    gameObject.transform.localScale = startingScale;
    iTween.ScaleTo(gameObject, iTween.Hash((object) "scale", (object) targetScale, (object) "easetype", (object) iTween.EaseType.easeOutElastic, (object) "time", (object) 1f));
  }

  public void PulseReminderEveryXSeconds(float seconds)
  {
    this.StartCoroutine(this.PulseReminder(seconds));
  }

  [DebuggerHidden]
  private IEnumerator PulseReminder(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Notification.\u003CPulseReminder\u003Ec__Iterator107() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds, \u003C\u003Ef__this = this };
  }

  private void BounceUp()
  {
    iTween.MoveTo(this.bounceObject, iTween.Hash((object) "islocal", (object) true, (object) "z", (object) (float) ((double) this.bounceObject.transform.localPosition.z - 0.5), (object) "time", (object) 0.75f, (object) "easetype", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) "BounceDown", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void BounceDown()
  {
    iTween.MoveTo(this.bounceObject, iTween.Hash((object) "islocal", (object) true, (object) "z", (object) (float) ((double) this.bounceObject.transform.localPosition.z + 0.5), (object) "time", (object) 0.75f, (object) "easetype", (object) iTween.EaseType.easeOutCubic, (object) "oncomplete", (object) "BounceUp", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void FadeOut()
  {
    iTween.MoveTo(this.fadeArrowObject, iTween.Hash((object) "islocal", (object) true, (object) "z", (object) (float) ((double) this.fadeArrowObject.transform.localPosition.z - 0.5), (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "FadeComplete", (object) "oncompletetarget", (object) this.gameObject));
    AnimationUtil.FadeTexture(this.fadeArrowObject.GetComponentInChildren<MeshRenderer>(), 1f, 0.0f, 0.5f, 0.15f, (AnimationUtil.DelOnFade) null);
  }

  private void FadeComplete()
  {
    iTween.MoveTo(this.fadeArrowObject, iTween.Hash((object) "islocal", (object) true, (object) "z", (object) (float) ((double) this.fadeArrowObject.transform.localPosition.z + 0.5), (object) "time", (object) 0.0f, (object) "delay", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.linear, (object) "oncomplete", (object) "FadeOut", (object) "oncompletetarget", (object) this.gameObject));
    AnimationUtil.FadeTexture(this.fadeArrowObject.GetComponentInChildren<MeshRenderer>(), 0.0f, 1f, 0.0f, 0.85f, (AnimationUtil.DelOnFade) null);
  }

  public void AssignAudio(AudioSource source)
  {
    this.m_accompaniedAudio = source;
  }

  public AudioSource GetAudio()
  {
    return this.m_accompaniedAudio;
  }

  public GameObject GetParentOffsetObject()
  {
    return this.m_parentOffsetObject;
  }

  public void SetParentOffsetObject(GameObject parentOffset)
  {
    if ((UnityEngine.Object) this.m_parentOffsetObject != (UnityEngine.Object) null)
    {
      this.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_parentOffsetObject);
    }
    this.m_parentOffsetObject = parentOffset;
    this.transform.SetParent(parentOffset.transform);
  }

  public enum SpeechBubbleDirection
  {
    None,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
  }

  public enum PopUpArrowDirection
  {
    Left,
    Right,
    Down,
    Up,
    LeftDown,
    RightDown,
    RightUp,
  }
}
