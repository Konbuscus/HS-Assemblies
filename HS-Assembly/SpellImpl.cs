﻿// Decompiled with JetBrains decompiler
// Type: SpellImpl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpellImpl : Spell
{
  protected Actor m_actor;
  protected GameObject m_rootObject;
  protected MeshRenderer m_rootObjectRenderer;

  protected void InitActorVariables()
  {
    this.m_actor = SpellUtils.GetParentActor((Spell) this);
    this.m_rootObject = SpellUtils.GetParentRootObject((Spell) this);
    this.m_rootObjectRenderer = SpellUtils.GetParentRootObjectMesh((Spell) this);
  }

  protected void SetActorVisibility(bool visible, bool ignoreSpells)
  {
    if (!((Object) this.m_actor != (Object) null))
      return;
    if (visible)
      this.m_actor.Show(ignoreSpells);
    else
      this.m_actor.Hide(ignoreSpells);
  }

  protected void SetVisibility(GameObject go, bool visible)
  {
    go.GetComponent<Renderer>().enabled = visible;
  }

  protected void SetVisibilityRecursive(GameObject go, bool visible)
  {
    if ((Object) go == (Object) null)
      return;
    Renderer[] componentsInChildren = go.GetComponentsInChildren<Renderer>();
    if (componentsInChildren == null)
      return;
    foreach (Renderer renderer in componentsInChildren)
      renderer.enabled = visible;
  }

  protected void SetAnimationSpeed(GameObject go, string animName, float speed)
  {
    if ((Object) go == (Object) null)
      return;
    go.GetComponent<Animation>()[animName].speed = speed;
  }

  protected void SetAnimationTime(GameObject go, string animName, float time)
  {
    if ((Object) go == (Object) null)
      return;
    go.GetComponent<Animation>()[animName].time = time;
  }

  protected void PlayAnimation(GameObject go, string animName, PlayMode playMode, float crossFade = 0.0f)
  {
    if ((Object) go == (Object) null)
      return;
    if ((double) crossFade <= (double) Mathf.Epsilon)
      go.GetComponent<Animation>().Play(animName, playMode);
    else
      go.GetComponent<Animation>().CrossFade(animName, crossFade, playMode);
  }

  protected void PlayParticles(GameObject go, bool includeChildren)
  {
    if ((Object) go == (Object) null)
      return;
    go.GetComponent<ParticleSystem>().Play(includeChildren);
  }

  protected GameObject GetActorObject(string name)
  {
    if ((Object) this.m_actor == (Object) null)
      return (GameObject) null;
    return SceneUtils.FindChildBySubstring(this.m_actor.gameObject, name);
  }

  protected void SetMaterialColor(GameObject go, Material material, string colorName, Color color, int materialIndex = 0)
  {
    if (colorName == string.Empty)
      colorName = "_Color";
    if ((Object) material != (Object) null)
    {
      material.SetColor(colorName, color);
    }
    else
    {
      if ((Object) go == (Object) null || (Object) go.GetComponent<Renderer>() == (Object) null || (Object) go.GetComponent<Renderer>().material == (Object) null)
        return;
      if (materialIndex == 0)
      {
        go.GetComponent<Renderer>().material.SetColor(colorName, color);
      }
      else
      {
        if (go.GetComponent<Renderer>().materials.Length <= materialIndex)
          return;
        Material[] materials = go.GetComponent<Renderer>().materials;
        materials[materialIndex].SetColor(colorName, color);
        go.GetComponent<Renderer>().materials = materials;
      }
    }
  }

  protected Material GetMaterial(GameObject go, Material material, bool getSharedMaterial = false, int materialIndex = 0)
  {
    if ((Object) go == (Object) null || (Object) go.GetComponent<Renderer>() == (Object) null)
      return (Material) null;
    if (materialIndex == 0 && !getSharedMaterial)
      return go.GetComponent<Renderer>().material;
    if (materialIndex == 0 && getSharedMaterial)
      return go.GetComponent<Renderer>().sharedMaterial;
    if (go.GetComponent<Renderer>().materials.Length > materialIndex && !getSharedMaterial)
    {
      Material[] materials = go.GetComponent<Renderer>().materials;
      Material material1 = materials[materialIndex];
      go.GetComponent<Renderer>().materials = materials;
      return material1;
    }
    if (go.GetComponent<Renderer>().materials.Length <= materialIndex || !getSharedMaterial)
      return (Material) null;
    Material[] sharedMaterials = go.GetComponent<Renderer>().sharedMaterials;
    Material material2 = sharedMaterials[materialIndex];
    go.GetComponent<Renderer>().sharedMaterials = sharedMaterials;
    return material2;
  }
}
