﻿// Decompiled with JetBrains decompiler
// Type: DeckCardBarFlareUp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DeckCardBarFlareUp : SpellImpl
{
  public GameObject m_fuseQuad;
  public GameObject m_fxSparks;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    if (!this.gameObject.activeSelf)
      return;
    this.StartCoroutine(this.BirthState());
  }

  [DebuggerHidden]
  private IEnumerator BirthState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckCardBarFlareUp.\u003CBirthState\u003Ec__Iterator2B1() { \u003C\u003Ef__this = this };
  }
}
