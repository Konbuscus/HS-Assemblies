﻿// Decompiled with JetBrains decompiler
// Type: HeroicBrawlRewardDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class HeroicBrawlRewardDisplay : MonoBehaviour
{
  public float m_FirewarksRewardDelayMin = 0.45f;
  public float m_FirewarksRewardDelayMax = 0.75f;
  public float m_FirewarksRewardHold = 0.7f;
  public float m_FireworksRewardRanRot = 30f;
  public float m_CardRewardDelay = 0.5f;
  public float m_CardRewardBurstDelay = 0.2f;
  public float m_EndScaleAwayTime = 0.3f;
  public float m_CardAnimationTime = 0.5f;
  public HeroicBrawlRewardDisplay.RewardVisuals[] m_RewardVisuals = new HeroicBrawlRewardDisplay.RewardVisuals[12];
  public HeroicBrawlRewardDisplay.CardVisuals[] m_CardVisuals = new HeroicBrawlRewardDisplay.CardVisuals[3];
  private List<RewardData> m_Rewards = new List<RewardData>();
  private List<Reward> m_finalRewards = new List<Reward>();
  private int m_lastZone = 1;
  private long m_noticeID = -1;
  public const string DEFAULT_PREFAB = "HeroicBrawlReward";
  public GameObject m_Root;
  public PlayMakerFSM m_PackFireworkFSM;
  public PlayMakerFSM m_GoldFireworkFSM;
  public PlayMakerFSM m_DustFireworkFSM;
  public PlayMakerFSM m_CardFireworkFSM;
  public NormalButton m_DoneButton;
  public PlayMakerFSM m_FSM;
  public GameObject m_RewardFireworksRoot;
  public HeroicBrawlRewardDisplay.FireworkRewardZone[] m_RewardZones;
  public GameObject m_FinalRewardsRoot;
  public UberText m_BannerUberText;
  public GameObject m_CardsRoot;
  public int m_DebugWins;
  public PegUIElement m_HeroicRewardChest;
  public GameObject m_DescText;
  private HeroicBrawlRewardDisplay.RewardsReceivedData m_RewardsReceived;
  private int m_finalRewardsLoadedCount;
  private List<Action> m_doneCallbacks;
  private int m_wins;
  private bool m_fromNotice;
  private static HeroicBrawlRewardDisplay s_instance;

  private void Awake()
  {
    HeroicBrawlRewardDisplay.s_instance = this;
    this.m_doneCallbacks = new List<Action>();
  }

  private void Start()
  {
    this.Init();
  }

  private void OnDisable()
  {
  }

  private void OnDestroy()
  {
    HeroicBrawlRewardDisplay.s_instance = (HeroicBrawlRewardDisplay) null;
  }

  private void OnEnable()
  {
  }

  public static HeroicBrawlRewardDisplay Get()
  {
    return HeroicBrawlRewardDisplay.s_instance;
  }

  public void ShowRewards(int wins, List<RewardData> rewards, bool fromNotice = false, long noticeID = -1)
  {
    if ((UnityEngine.Object) this.m_FSM == (UnityEngine.Object) null)
      UnityEngine.Debug.LogErrorFormat("FSM is null!");
    else if (rewards == null && rewards.Count < 1)
    {
      UnityEngine.Debug.LogErrorFormat("rewards is null!");
    }
    else
    {
      this.m_Rewards = rewards;
      this.m_wins = wins;
      this.m_fromNotice = fromNotice;
      this.m_noticeID = noticeID;
      this.m_DescText.SetActive(fromNotice);
      this.ShowRewardChest();
    }
  }

  [ContextMenu("Debug Show Rewards")]
  public void DebugShowRewards()
  {
    this.ShowRewards(this.m_DebugWins, this.DebugRewards(this.m_DebugWins), false, -1L);
  }

  public void RegisterDoneCallback(Action action)
  {
    this.m_doneCallbacks.Add(action);
  }

  private void Init()
  {
    for (int index = 0; index < this.m_RewardZones.Length; ++index)
    {
      this.m_RewardZones[index].goldReward = this.m_RewardZones[index].GoldGameObject.GetComponentInChildren<GoldReward>();
      this.m_RewardZones[index].dustReward = this.m_RewardZones[index].DustGameObject.GetComponentInChildren<ArcaneDustReward>();
    }
    this.m_FinalRewardsRoot.SetActive(false);
    this.m_RewardFireworksRoot.SetActive(false);
    this.m_PackFireworkFSM.gameObject.SetActive(false);
    this.m_GoldFireworkFSM.gameObject.SetActive(false);
    this.m_DustFireworkFSM.gameObject.SetActive(false);
    this.m_CardFireworkFSM.gameObject.SetActive(false);
  }

  private void ShowRewardChest()
  {
    if ((UnityEngine.Object) FullScreenFXMgr.Get() != (UnityEngine.Object) null)
      FullScreenFXMgr.Get().StartStandardBlurVignette(0.5f);
    this.LoadFinalRewards();
    this.m_HeroicRewardChest.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ShowRewardsCeremony));
  }

  private void ShowRewardsCeremony(UIEvent e)
  {
    this.StartCoroutine(this.AnimateRewardsCeremony());
  }

  [DebuggerHidden]
  private IEnumerator AnimateRewardsCeremony()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroicBrawlRewardDisplay.\u003CAnimateRewardsCeremony\u003Ec__Iterator278() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator ShowRewardsSimple(int wins)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroicBrawlRewardDisplay.\u003CShowRewardsSimple\u003Ec__Iterator279() { wins = wins, \u003C\u0024\u003Ewins = wins, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator ShowRewardsFireworks(int wins)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroicBrawlRewardDisplay.\u003CShowRewardsFireworks\u003Ec__Iterator27A() { wins = wins, \u003C\u0024\u003Ewins = wins, \u003C\u003Ef__this = this };
  }

  private float GetFireworkRewardDelay()
  {
    return UnityEngine.Random.Range(this.m_FirewarksRewardDelayMin, this.m_FirewarksRewardDelayMax);
  }

  private float DisplayFirework(PlayMakerFSM fsm, Vector3 targetPosition)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(fsm.gameObject);
    gameObject.transform.parent = this.transform;
    gameObject.transform.position = fsm.transform.position;
    gameObject.SetActive(true);
    gameObject.gameObject.layer = fsm.gameObject.layer;
    PlayMakerFSM component = gameObject.GetComponent<PlayMakerFSM>();
    component.FsmVariables.FindFsmVector3("TargetPosition").Value = targetPosition;
    component.SendEvent("Firework");
    this.m_FSM.SendEvent("BounceBox");
    return component.FsmVariables.GetFsmFloat("FireworkTime").Value;
  }

  [DebuggerHidden]
  private IEnumerator DisplayFireworkPack(int zone, Vector3 localPosition)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroicBrawlRewardDisplay.\u003CDisplayFireworkPack\u003Ec__Iterator27B() { zone = zone, localPosition = localPosition, \u003C\u0024\u003Ezone = zone, \u003C\u0024\u003ElocalPosition = localPosition, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator DisplayFireworkGold(int zone, Vector3 localPosition, int amount)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroicBrawlRewardDisplay.\u003CDisplayFireworkGold\u003Ec__Iterator27C() { zone = zone, localPosition = localPosition, amount = amount, \u003C\u0024\u003Ezone = zone, \u003C\u0024\u003ElocalPosition = localPosition, \u003C\u0024\u003Eamount = amount, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator DisplayFireworkDust(int zone, Vector3 localPosition, int amount)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroicBrawlRewardDisplay.\u003CDisplayFireworkDust\u003Ec__Iterator27D() { zone = zone, localPosition = localPosition, amount = amount, \u003C\u0024\u003Ezone = zone, \u003C\u0024\u003ElocalPosition = localPosition, \u003C\u0024\u003Eamount = amount, \u003C\u003Ef__this = this };
  }

  private Vector3 ZoneRandomLocalPosition(int zone)
  {
    Bounds bounds = this.m_RewardZones[zone].Collider.bounds;
    Vector3 vector3 = new Vector3(UnityEngine.Random.Range(-bounds.extents.x, bounds.extents.x), 0.0f, UnityEngine.Random.Range(-bounds.extents.z, bounds.extents.z));
    Log.Kyle.Print("Zone: {0}, Pos: {1}", new object[2]
    {
      (object) zone,
      (object) vector3
    });
    return vector3;
  }

  [DebuggerHidden]
  private IEnumerator ShowCards(int wins)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroicBrawlRewardDisplay.\u003CShowCards\u003Ec__Iterator27E() { wins = wins, \u003C\u0024\u003Ewins = wins, \u003C\u003Ef__this = this };
  }

  private void InitRewardsReceived()
  {
    this.m_RewardsReceived.PackID = 1;
    this.m_RewardsReceived.PackCount = 0;
    this.m_RewardsReceived.DustCount = 0;
    this.m_RewardsReceived.GoldCount = 0;
    this.m_RewardsReceived.CardsCount = 0;
    this.m_RewardsReceived.Cards = new List<HeroicBrawlRewardDisplay.RewardCardReceived>();
    for (int index = 0; index < this.m_finalRewards.Count; ++index)
    {
      Reward finalReward = this.m_finalRewards[index];
      switch (finalReward.RewardType)
      {
        case Reward.Type.ARCANE_DUST:
          this.m_RewardsReceived.DustCount = ((ArcaneDustRewardData) finalReward.Data).Amount;
          break;
        case Reward.Type.BOOSTER_PACK:
          BoosterPackRewardData data1 = (BoosterPackRewardData) finalReward.Data;
          this.m_RewardsReceived.PackID = data1.Id;
          this.m_RewardsReceived.PackCount = data1.Count;
          break;
        case Reward.Type.CARD:
          CardRewardData data2 = (CardRewardData) finalReward.Data;
          HeroicBrawlRewardDisplay.RewardCardReceived rewardCardReceived = new HeroicBrawlRewardDisplay.RewardCardReceived();
          rewardCardReceived.CardID = data2.CardID;
          rewardCardReceived.Premium = data2.Premium;
          EntityDef entityDef = DefLoader.Get().GetEntityDef(rewardCardReceived.CardID);
          if (entityDef == null)
          {
            UnityEngine.Debug.LogWarningFormat("InitRewardsReceived() - entityDef for Card ID {0} is null", (object) rewardCardReceived.CardID);
            return;
          }
          rewardCardReceived.CardEntityDef = entityDef;
          this.m_RewardsReceived.Cards.Add(rewardCardReceived);
          ++this.m_RewardsReceived.CardsCount;
          break;
        case Reward.Type.GOLD:
          this.m_RewardsReceived.GoldCount = (int) ((GoldRewardData) finalReward.Data).Amount;
          break;
      }
    }
  }

  private int NextRewardZone()
  {
    int num = UnityEngine.Random.Range(0, this.m_RewardZones.Length);
    if (num == this.m_lastZone)
    {
      num = UnityEngine.Random.Range(0, this.m_RewardZones.Length);
      if (num == this.m_lastZone)
      {
        ++num;
        if (num >= this.m_RewardZones.Length)
          num = 0;
      }
    }
    this.m_lastZone = num;
    return num;
  }

  [DebuggerHidden]
  private IEnumerator ShowFinalRewards(int wins, bool simpleRewards = false)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroicBrawlRewardDisplay.\u003CShowFinalRewards\u003Ec__Iterator27F() { wins = wins, simpleRewards = simpleRewards, \u003C\u0024\u003Ewins = wins, \u003C\u0024\u003EsimpleRewards = simpleRewards, \u003C\u003Ef__this = this };
  }

  private void LoadFinalRewards()
  {
    this.m_finalRewardsLoadedCount = 0;
    for (int index = 0; index < this.m_Rewards.Count; ++index)
      this.m_Rewards[index].LoadRewardObject(new Reward.DelOnRewardLoaded(this.FinalRewardLoaded));
  }

  private bool IsFinalRewardsLoaded()
  {
    return this.m_finalRewardsLoadedCount >= this.m_Rewards.Count;
  }

  private void FinalRewardLoaded(Reward reward, object callbackData)
  {
    ++this.m_finalRewardsLoadedCount;
    if ((UnityEngine.Object) reward == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarningFormat("HeroicBrawlRewardDisplay.FinalRewardLoaded() - FAILED to load reward");
    else if ((UnityEngine.Object) reward.gameObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarningFormat("HeroicBrawlRewardDisplay.FinalRewardLoaded() - reward GameObject is null");
    }
    else
    {
      reward.gameObject.layer = this.gameObject.layer;
      this.m_finalRewards.Add(reward);
    }
  }

  private void AllDone()
  {
    this.m_DoneButton.gameObject.SetActive(true);
    Spell component = this.m_DoneButton.m_button.GetComponent<Spell>();
    component.AddFinishedCallback(new Spell.FinishedCallback(this.OnDoneButtonShown));
    component.ActivateState(SpellStateType.BIRTH);
  }

  private void OnDoneButtonShown(Spell spell, object userData)
  {
    this.m_DoneButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDoneButtonPressed));
  }

  private void OnDoneButtonPressed(UIEvent e)
  {
    this.m_DoneButton.m_button.GetComponent<Spell>().ActivateState(SpellStateType.DEATH);
    iTween.ScaleTo(this.m_Root, Vector3.zero, this.m_EndScaleAwayTime);
    FullScreenFXMgr.Get().DisableBlur();
    if (this.m_fromNotice)
      Network.AckNotice(this.m_noticeID);
    using (List<Action>.Enumerator enumerator = this.m_doneCallbacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Action current = enumerator.Current;
        if (current != null)
          current();
      }
    }
    this.StartCoroutine(this.OnDone());
  }

  [DebuggerHidden]
  private IEnumerator OnDone()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroicBrawlRewardDisplay.\u003COnDone\u003Ec__Iterator280() { \u003C\u003Ef__this = this };
  }

  private void LoadRewardCards()
  {
    for (int index = 0; index < this.m_RewardsReceived.Cards.Count; ++index)
    {
      HeroicBrawlRewardDisplay.RewardCardReceived card = this.m_RewardsReceived.Cards[index];
      GameObject gameObject = AssetLoader.Get().LoadActor(ActorNames.GetHandActor(this.m_RewardsReceived.Cards[index].CardEntityDef.GetCardType()), false, false);
      this.m_RewardsReceived.Cards[index] = new HeroicBrawlRewardDisplay.RewardCardReceived()
      {
        CardGameObject = gameObject,
        CardID = card.CardID,
        CardEntityDef = card.CardEntityDef,
        Premium = card.Premium
      };
    }
  }

  private void LoadBoosterReward()
  {
    string name = "BoosterPackReward";
    for (int index = 0; index < this.m_RewardZones.Length; ++index)
    {
      GameObject gameObject = AssetLoader.Get().LoadGameObject(name, false, false);
      gameObject.transform.parent = this.m_RewardZones[index].PackGameObject.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localRotation = Quaternion.identity;
      gameObject.transform.localScale = Vector3.one;
      this.m_RewardZones[index].packReward = this.m_RewardZones[index].PackGameObject.GetComponentInChildren<BoosterPackReward>();
    }
  }

  private List<RewardData> DebugRewards(int wins)
  {
    int num1 = 0;
    int num2 = 0;
    int num3 = 0;
    int num4 = 0;
    switch (wins)
    {
      case 0:
        num1 = 1;
        break;
      case 1:
        num1 = 2;
        break;
      case 2:
        num1 = 3;
        break;
      case 3:
        num1 = 4;
        num2 = 200;
        num3 = 200;
        break;
      case 4:
        num1 = 5;
        num2 = 350;
        num3 = 350;
        break;
      case 5:
        num1 = 6;
        num2 = 400;
        num3 = 400;
        break;
      case 6:
        num1 = 7;
        num2 = 450;
        num3 = 450;
        break;
      case 7:
        num1 = 8;
        num2 = 500;
        num3 = 500;
        break;
      case 8:
        num1 = 9;
        num2 = 550;
        num3 = 550;
        break;
      case 9:
        num1 = 14;
        num2 = 800;
        num3 = 800;
        break;
      case 10:
        num1 = 15;
        num2 = 950;
        num3 = 950;
        num4 = 1;
        break;
      case 11:
        num1 = 20;
        num2 = 1000;
        num3 = 1000;
        num4 = 2;
        break;
      case 12:
        num1 = 50;
        num2 = 2300;
        num3 = 2300;
        num4 = 3;
        break;
    }
    List<RewardData> rewardDataList = new List<RewardData>();
    if (num1 > 0)
    {
      BoosterPackRewardData boosterPackRewardData = new BoosterPackRewardData();
      boosterPackRewardData.Count = num1;
      boosterPackRewardData.Id = 11;
      boosterPackRewardData.MarkAsDummyReward();
      rewardDataList.Add((RewardData) boosterPackRewardData);
    }
    if (num3 > 0)
    {
      GoldRewardData goldRewardData = new GoldRewardData();
      goldRewardData.Amount = (long) num3;
      goldRewardData.MarkAsDummyReward();
      rewardDataList.Add((RewardData) goldRewardData);
    }
    if (num2 > 0)
    {
      ArcaneDustRewardData arcaneDustRewardData = new ArcaneDustRewardData();
      arcaneDustRewardData.Amount = num2;
      arcaneDustRewardData.MarkAsDummyReward();
      rewardDataList.Add((RewardData) arcaneDustRewardData);
    }
    if (num4 > 0)
    {
      string[] strArray = new string[3]{ "NEW1_030", "NEW1_030", "NEW1_030" };
      for (int index = 0; index < num4; ++index)
      {
        CardRewardData cardRewardData = new CardRewardData();
        cardRewardData.CardID = strArray[index];
        cardRewardData.Premium = TAG_PREMIUM.GOLDEN;
        cardRewardData.MarkAsDummyReward();
        rewardDataList.Add((RewardData) cardRewardData);
      }
    }
    return rewardDataList;
  }

  [Serializable]
  public class RewardVisuals
  {
    public int DustPerBottleMin = 50;
    public int DustPerBottleMax = 100;
    public int GoldPerBagMin = 50;
    public int GoldPerBagMax = 100;
    public bool DropBox;
    public bool ShatterDialog;
    public Transform m_FinalPacksBone;
    public Transform m_FinalGoldBone;
    public Transform m_FinalDustBone;
  }

  [Serializable]
  public struct FireworkRewardZone
  {
    public GameObject ZoneRoot;
    public BoxCollider Collider;
    public GameObject PackGameObject;
    public GameObject GoldGameObject;
    public GameObject DustGameObject;
    public BoosterPackReward packReward;
    public ArcaneDustReward dustReward;
    public GoldReward goldReward;
  }

  [Serializable]
  public struct CardVisuals
  {
    public GameObject[] m_Cards;
    public GameObject[] m_CardTargets;
  }

  public struct RewardCardReceived
  {
    public string CardID;
    public TAG_PREMIUM Premium;
    public EntityDef CardEntityDef;
    public GameObject CardGameObject;
  }

  public struct RewardsReceivedData
  {
    public int PackID;
    public int PackCount;
    public int GoldCount;
    public int DustCount;
    public int CardsCount;
    public List<HeroicBrawlRewardDisplay.RewardCardReceived> Cards;
  }
}
