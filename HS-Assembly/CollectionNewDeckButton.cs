﻿// Decompiled with JetBrains decompiler
// Type: CollectionNewDeckButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CollectionNewDeckButton : PegUIElement
{
  private readonly string DECKBOX_POPUP_ANIM_NAME = "NewDeck_PopUp";
  private readonly string DECKBOX_POPDOWN_ANIM_NAME = "NewDeck_PopDown";
  private const float BUTTON_POP_SPEED = 2.5f;
  public HighlightState m_highlightState;
  public UberText m_buttonText;
  private bool m_isPoppedUp;
  private bool m_isUsable;

  protected override void Awake()
  {
    base.Awake();
    this.SetEnabled(false);
    this.m_buttonText.Text = SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL ? GameStrings.Get("GLUE_COLLECTION_NEW_DECK") : string.Empty;
    UIBScrollableItem component = this.GetComponent<UIBScrollableItem>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.SetCustomActiveState(new UIBScrollableItem.ActiveStateCallback(this.IsUsable));
  }

  public void SetIsUsable(bool isUsable)
  {
    this.m_isUsable = isUsable;
  }

  public bool IsUsable()
  {
    return this.m_isUsable;
  }

  public void PlayPopUpAnimation()
  {
    this.PlayPopUpAnimation((CollectionNewDeckButton.DelOnAnimationFinished) null);
  }

  public void PlayPopUpAnimation(CollectionNewDeckButton.DelOnAnimationFinished callback)
  {
    this.PlayPopUpAnimation(callback, (object) null, new float?());
  }

  public void PlayPopUpAnimation(CollectionNewDeckButton.DelOnAnimationFinished callback, object callbackData, float? speed = null)
  {
    this.gameObject.SetActive(true);
    if (this.m_isPoppedUp)
    {
      if (callback == null)
        return;
      callback(callbackData);
    }
    else
    {
      this.m_isPoppedUp = true;
      this.GetComponent<Animation>()[this.DECKBOX_POPUP_ANIM_NAME].time = 0.0f;
      this.GetComponent<Animation>()[this.DECKBOX_POPUP_ANIM_NAME].speed = !speed.HasValue ? 2.5f : speed.Value;
      this.PlayAnimation(this.DECKBOX_POPUP_ANIM_NAME, callback, callbackData);
    }
  }

  public void PlayPopDownAnimation()
  {
    this.PlayPopDownAnimation((CollectionNewDeckButton.DelOnAnimationFinished) null);
  }

  public void PlayPopDownAnimation(CollectionNewDeckButton.DelOnAnimationFinished callback)
  {
    this.PlayPopDownAnimation(callback, (object) null, new float?());
  }

  public void PlayPopDownAnimation(CollectionNewDeckButton.DelOnAnimationFinished callback, object callbackData, float? speed = null)
  {
    this.gameObject.SetActive(true);
    if (!this.m_isPoppedUp)
    {
      if (callback == null)
        return;
      callback(callbackData);
    }
    else
    {
      this.m_isPoppedUp = false;
      this.GetComponent<Animation>()[this.DECKBOX_POPDOWN_ANIM_NAME].time = 0.0f;
      this.GetComponent<Animation>()[this.DECKBOX_POPDOWN_ANIM_NAME].speed = !speed.HasValue ? 2.5f : speed.Value;
      this.PlayAnimation(this.DECKBOX_POPDOWN_ANIM_NAME, callback, callbackData);
    }
  }

  public void FlipHalfOverAndHide(float animTime, CollectionNewDeckButton.DelOnAnimationFinished finished = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionNewDeckButton.\u003CFlipHalfOverAndHide\u003Ec__AnonStorey396 hideCAnonStorey396 = new CollectionNewDeckButton.\u003CFlipHalfOverAndHide\u003Ec__AnonStorey396();
    // ISSUE: reference to a compiler-generated field
    hideCAnonStorey396.finished = finished;
    // ISSUE: reference to a compiler-generated field
    hideCAnonStorey396.\u003C\u003Ef__this = this;
    if (!this.m_isPoppedUp)
    {
      UnityEngine.Debug.LogWarning((object) "Can't flip over and hide button. It is currently not popped up.");
    }
    else
    {
      iTween.StopByName(this.gameObject, "rotation");
      // ISSUE: reference to a compiler-generated method
      iTween.RotateTo(this.gameObject, iTween.Hash((object) "rotation", (object) new Vector3(270f, 0.0f, 0.0f), (object) "isLocal", (object) true, (object) "time", (object) animTime, (object) "easeType", (object) iTween.EaseType.easeInCubic, (object) "oncomplete", (object) new Action<object>(hideCAnonStorey396.\u003C\u003Em__E3), (object) "name", (object) "rotation"));
      this.m_isPoppedUp = false;
    }
  }

  public bool IsPoppedUp()
  {
    return this.m_isPoppedUp;
  }

  private void PlayAnimation(string animationName)
  {
    this.PlayAnimation(animationName, (CollectionNewDeckButton.DelOnAnimationFinished) null, (object) null);
  }

  private void PlayAnimation(string animationName, CollectionNewDeckButton.DelOnAnimationFinished callback, object callbackData)
  {
    this.GetComponent<Animation>().Play(animationName);
    CollectionNewDeckButton.OnPopAnimationFinishedCallbackData finishedCallbackData = new CollectionNewDeckButton.OnPopAnimationFinishedCallbackData()
    {
      m_callback = callback,
      m_callbackData = callbackData,
      m_animationName = animationName
    };
    this.StopCoroutine("WaitThenCallAnimationCallback");
    this.StartCoroutine("WaitThenCallAnimationCallback", (object) finishedCallbackData);
  }

  [DebuggerHidden]
  private IEnumerator WaitThenCallAnimationCallback(CollectionNewDeckButton.OnPopAnimationFinishedCallbackData callbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionNewDeckButton.\u003CWaitThenCallAnimationCallback\u003Ec__Iterator43()
    {
      callbackData = callbackData,
      \u003C\u0024\u003EcallbackData = callbackData,
      \u003C\u003Ef__this = this
    };
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    SoundManager.Get().LoadAndPlay("Hub_Mouseover");
    this.m_highlightState.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.m_highlightState.ChangeState(ActorStateType.NONE);
  }

  private class OnPopAnimationFinishedCallbackData
  {
    public string m_animationName;
    public CollectionNewDeckButton.DelOnAnimationFinished m_callback;
    public object m_callbackData;
  }

  public delegate void DelOnAnimationFinished(object callbackData);
}
