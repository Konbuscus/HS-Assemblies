﻿// Decompiled with JetBrains decompiler
// Type: TutorialLesson4
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TutorialLesson4 : MonoBehaviour
{
  public UberText m_tauntDescriptionTitle;
  public UberText m_tauntDescription;
  public UberText m_taunt;

  private void Awake()
  {
    this.m_tauntDescriptionTitle.SetGameStringText("GLOBAL_TUTORIAL_TAUNT");
    this.m_tauntDescription.SetGameStringText("GLOBAL_TUTORIAL_TAUNT_DESCRIPTION");
    this.m_taunt.SetGameStringText("GLOBAL_TUTORIAL_TAUNT");
  }
}
