﻿// Decompiled with JetBrains decompiler
// Type: MobileChatLogFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MobileChatLogFrame : MonoBehaviour
{
  public Spawner playerIconRef;
  public TouchList messageFrames;
  public MobileChatLogFrame.InputInfo inputInfo;
  public TextField inputTextField;
  public MobileChatLogFrame.MessageInfo messageInfo;
  public NineSliceElement window;
  public UberText nameText;
  public UIBButton closeButton;
  public MobileChatNotification notifications;
  public GameObject medalPatch;
  public TournamentMedal medal;
  public ChatLog chatLog;
  public MobileChatLogFrame.Followers followers;
  private PlayerIcon playerIcon;
  private BnetPlayer receiver;

  public bool HasFocus
  {
    get
    {
      return this.inputTextField.Active;
    }
  }

  public BnetPlayer Receiver
  {
    get
    {
      return this.receiver;
    }
    set
    {
      if (this.receiver == value)
        return;
      this.receiver = value;
      this.Focus(this.receiver != null);
      if (this.receiver == null)
        return;
      this.playerIcon.SetPlayer(this.receiver);
      this.UpdateReceiver();
      this.chatLog.Receiver = this.receiver;
    }
  }

  public event Action InputCanceled;

  public event Action CloseButtonReleased;

  private void Awake()
  {
    this.playerIcon = this.playerIconRef.Spawn<PlayerIcon>();
    this.UpdateBackgroundCollider();
    this.inputTextField.maxCharacters = 512;
    this.inputTextField.Submitted += new Action<string>(this.OnInputComplete);
    this.inputTextField.Canceled += new Action(this.OnInputCanceled);
    this.closeButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCloseButtonReleased));
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
  }

  private void Start()
  {
    if (this.receiver != null)
      return;
    this.gameObject.SetActive(false);
  }

  private void OnDestroy()
  {
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
  }

  private void Update()
  {
  }

  public void Focus(bool focus)
  {
    if (focus && !this.inputTextField.Active)
    {
      this.inputTextField.Activate();
    }
    else
    {
      if (focus || !this.inputTextField.Active)
        return;
      this.inputTextField.Deactivate();
    }
  }

  public void SetWorldRect(float x, float y, float width, float height)
  {
    bool activeSelf = this.gameObject.activeSelf;
    this.gameObject.SetActive(true);
    float viewWindowMaxValue = this.messageFrames.ViewWindowMaxValue;
    this.window.SetEntireSize(width, height);
    Vector3 worldPoint = TransformUtil.ComputeWorldPoint(TransformUtil.ComputeSetPointBounds((Component) this.window), new Vector3(0.0f, 1f, 0.0f));
    this.transform.Translate(new Vector3(x, y, worldPoint.z) - worldPoint);
    this.messageFrames.transform.position = (this.messageInfo.messagesTopLeft.position + this.messageInfo.messagesBottomRight.position) / 2f;
    Vector3 vector3 = this.messageInfo.messagesBottomRight.position - this.messageInfo.messagesTopLeft.position;
    this.messageFrames.ClipSize = new Vector2(vector3.x, Math.Abs(vector3.y));
    this.messageFrames.ViewWindowMaxValue = viewWindowMaxValue;
    this.messageFrames.ScrollValue = Mathf.Clamp01(this.messageFrames.ScrollValue);
    this.chatLog.OnResize();
    this.UpdateBackgroundCollider();
    this.UpdateFollowers();
    this.gameObject.SetActive(activeSelf);
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    if (changelist.FindChange(this.receiver) == null)
      return;
    this.UpdateReceiver();
  }

  private void OnCloseButtonReleased(UIEvent e)
  {
    if (this.CloseButtonReleased == null)
      return;
    this.CloseButtonReleased();
  }

  private bool IsFullScreenKeyboard()
  {
    return (double) ChatMgr.Get().KeyboardRect.height == (double) Screen.height;
  }

  private void OnInputComplete(string input)
  {
    if (string.IsNullOrEmpty(input))
      return;
    if (!BnetWhisperMgr.Get().SendWhisper(this.receiver, input))
      this.chatLog.OnWhisperFailed();
    ChatMgr.Get().AddRecentWhisperPlayerToTop(this.receiver);
  }

  private void OnInputCanceled()
  {
    if (this.InputCanceled == null)
      return;
    this.InputCanceled();
  }

  private void UpdateReceiver()
  {
    this.playerIcon.UpdateIcon();
    this.nameText.Text = string.Format("<color=#{0}>{1}</color>", !this.receiver.IsOnline() ? (object) "999999ff" : (object) "5ecaf0ff", (object) this.receiver.GetBestName());
    if (this.receiver != null && this.receiver.IsDisplayable() && this.receiver.IsOnline())
    {
      MedalInfoTranslator rankPresenceField = RankMgr.Get().GetRankPresenceField(this.receiver.GetBestGameAccount());
      if (rankPresenceField == null || rankPresenceField.GetCurrentMedal(rankPresenceField.IsBestCurrentRankWild()).rank == 25)
      {
        this.medalPatch.SetActive(false);
        this.playerIcon.Show();
      }
      else
      {
        this.playerIcon.Hide();
        this.medal.SetEnabled(false);
        this.medal.SetMedal(rankPresenceField, false);
        this.medal.SetFormat(this.medal.IsBestCurrentRankWild());
        this.medalPatch.SetActive(true);
      }
    }
    else
    {
      if (this.receiver.IsOnline())
        return;
      this.medalPatch.SetActive(false);
      this.playerIcon.Show();
    }
  }

  private void UpdateBackgroundCollider()
  {
    Bounds bounds = ((IEnumerable<Renderer>) this.window.GetComponentsInChildren<Renderer>()).Aggregate<Renderer, Bounds>(new Bounds(this.transform.position, Vector3.zero), (Func<Bounds, Renderer, Bounds>) ((aggregate, renderer) =>
    {
      if ((double) renderer.bounds.size.x != 0.0 && (double) renderer.bounds.size.y != 0.0 && (double) renderer.bounds.size.z != 0.0)
        aggregate.Encapsulate(renderer.bounds);
      return aggregate;
    }));
    Vector3 vector3_1 = this.transform.InverseTransformPoint(bounds.min);
    Vector3 vector3_2 = this.transform.InverseTransformPoint(bounds.max);
    BoxCollider boxCollider = this.GetComponent<BoxCollider>();
    if ((UnityEngine.Object) boxCollider == (UnityEngine.Object) null)
      boxCollider = this.gameObject.AddComponent<BoxCollider>();
    boxCollider.center = (vector3_1 + vector3_2) / 2f + Vector3.forward;
    boxCollider.size = vector3_2 - vector3_1;
    boxCollider.size = new Vector3(boxCollider.size.x, boxCollider.size.y, 0.0f);
  }

  private void UpdateFollowers()
  {
    this.followers.UpdateFollowPosition();
  }

  [Serializable]
  public class MessageInfo
  {
    public Transform messagesTopLeft;
    public Transform messagesBottomRight;
  }

  [Serializable]
  public class InputInfo
  {
    public Transform inputTopLeft;
    public Transform inputBottomRight;
  }

  [Serializable]
  public class Followers
  {
    public UIBFollowObject playerInfoFollower;
    public UIBFollowObject closeButtonFollower;
    public UIBFollowObject bubbleFollower;

    public void UpdateFollowPosition()
    {
      this.playerInfoFollower.UpdateFollowPosition();
      this.closeButtonFollower.UpdateFollowPosition();
      this.bubbleFollower.UpdateFollowPosition();
    }
  }
}
