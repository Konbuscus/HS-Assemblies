﻿// Decompiled with JetBrains decompiler
// Type: AnimateTransitions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AnimateTransitions : MonoBehaviour
{
  public List<GameObject> m_TargetList;
  public float amount;
  private List<Renderer> rend;

  public void StartTransitions()
  {
    using (List<Renderer>.Enumerator enumerator = this.rend.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.material.SetFloat("_Transistion", this.amount);
    }
  }

  private void Start()
  {
    this.rend = new List<Renderer>();
    using (List<GameObject>.Enumerator enumerator = this.m_TargetList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((Object) current == (Object) null))
          this.rend.Add(current.GetComponent<Renderer>());
      }
    }
  }

  private void Update()
  {
    this.StartTransitions();
  }
}
