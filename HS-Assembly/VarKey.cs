﻿// Decompiled with JetBrains decompiler
// Type: VarKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class VarKey
{
  private string m_key;

  public VarKey(string key)
  {
    this.m_key = key;
  }

  public VarKey(string key, string subKey)
  {
    this.m_key = key + "." + subKey;
  }

  public VarKey Key(string subKey)
  {
    return new VarKey(this.m_key, subKey);
  }

  public string GetStr(string def)
  {
    if (VarsInternal.Get().Contains(this.m_key))
      return VarsInternal.Get().Value(this.m_key);
    return def;
  }

  public int GetInt(int def)
  {
    if (VarsInternal.Get().Contains(this.m_key))
      return GeneralUtils.ForceInt(VarsInternal.Get().Value(this.m_key));
    return def;
  }

  public float GetFloat(float def)
  {
    if (VarsInternal.Get().Contains(this.m_key))
      return GeneralUtils.ForceFloat(VarsInternal.Get().Value(this.m_key));
    return def;
  }

  public bool GetBool(bool def)
  {
    if (VarsInternal.Get().Contains(this.m_key))
      return GeneralUtils.ForceBool(VarsInternal.Get().Value(this.m_key));
    return def;
  }
}
