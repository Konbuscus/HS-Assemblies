﻿// Decompiled with JetBrains decompiler
// Type: RitualSpellController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RitualSpellController : SpellController
{
  public float m_noSpellDisplayTime = 3f;
  public string m_friendlyRitualBoneName = "FriendlyRitual";
  public string m_opponentRitualBoneName = "OpponentRitual";
  public bool m_hideRitualActor = true;
  public float m_prebuffDisplayTime = 1f;
  public Spell m_ritualSpell;
  public Spell m_tauntInstantSpell;
  public Spell m_tauntInstantPremiumSpell;
  public bool m_skipIfCthunInPlay;
  public bool m_showBonusAnims;
  private Entity m_ritualEntity;
  private Entity m_ritualEntityClone;
  private bool m_finished;
  private Actor m_ritualActor;
  private Spell m_tauntSpellInstance;

  protected override bool AddPowerSourceAndTargets(PowerTaskList taskList)
  {
    Entity sourceEntity = taskList.GetSourceEntity();
    Player controller = sourceEntity.GetController();
    if (taskList.IsOrigin())
      return true;
    if (!controller.HasTag(GAME_TAG.SEEN_CTHUN) || !taskList.IsEndOfBlock())
      return false;
    int tag = controller.GetTag(GAME_TAG.PROXY_CTHUN);
    if (tag == 0 || this.m_skipIfCthunInPlay && this.IsCthunInPlay(controller))
      return false;
    this.m_ritualEntity = GameState.Get().GetEntity(tag);
    if (this.m_ritualEntity == null)
      return false;
    this.m_ritualEntityClone = taskList.GetOrigin().GetRitualEntityClone();
    if (this.m_ritualEntityClone == null)
      return false;
    this.SetSource(sourceEntity.GetCard());
    this.AddTarget(this.m_ritualEntity.GetCard());
    return true;
  }

  protected override void OnProcessTaskList()
  {
    this.StartCoroutine(this.DoRitualEffect());
  }

  [DebuggerHidden]
  private IEnumerator DoRitualEffect()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RitualSpellController.\u003CDoRitualEffect\u003Ec__Iterator251() { \u003C\u003Ef__this = this };
  }

  private int FindLatestProxyCthunCreationTask()
  {
    int num = -1;
    List<PowerTask> taskList = this.m_taskList.GetTaskList();
    for (int index = 0; index < taskList.Count; ++index)
    {
      Network.PowerHistory power = taskList[index].GetPower();
      switch (power.Type)
      {
        case Network.PowerType.TAG_CHANGE:
          Network.HistTagChange histTagChange = (Network.HistTagChange) power;
          if (histTagChange.Tag == 434 && histTagChange.Value > 0 && index > num)
          {
            num = index;
            break;
          }
          break;
        case Network.PowerType.FULL_ENTITY:
          if (index > num)
          {
            num = index;
            break;
          }
          break;
      }
    }
    return num;
  }

  public void OnSpellFinished(Spell spell, object userData)
  {
    this.OnFinishedTaskList();
  }

  public void OnSpellEvent(string eventName, object eventData, object userData)
  {
    if (eventName != "showCthun")
      UnityEngine.Debug.LogError((object) ("RitualSpellController received unexpected Spell Event " + eventName));
    if (!this.m_hideRitualActor)
      return;
    this.m_ritualActor.Show();
    if (!((Object) this.m_tauntSpellInstance != (Object) null))
      return;
    this.m_tauntSpellInstance.Activate();
  }

  private void OnRitualSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    ((Actor) userData).Destroy();
    this.FinishRitual();
  }

  private void FinishRitual()
  {
    this.m_finished = true;
    if (this.m_processingTaskList)
      this.OnFinishedTaskList();
    this.OnFinished();
  }

  private Actor LoadRitualActor(Entity entity)
  {
    GameObject gameObject = AssetLoader.Get().LoadActor(ActorNames.GetZoneActor(entity, TAG_ZONE.PLAY), false, false);
    if ((Object) gameObject == (Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "RitualSpellController unable to load Ritual Actor GameObject.");
      return (Actor) null;
    }
    Actor component = gameObject.GetComponent<Actor>();
    if ((Object) component == (Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "RitualSpellController Ritual Actor GameObject contains no Actor component.");
      Object.Destroy((Object) gameObject);
      return (Actor) null;
    }
    component.SetEntity(entity);
    component.SetCardDef(entity.GetCardDef());
    return component;
  }

  private void UpdateAndPositionRitualActor()
  {
    if (this.m_ritualActor.GetEntity().HasTag(GAME_TAG.TAUNT))
    {
      Spell original = this.m_ritualEntity.GetPremiumType() != TAG_PREMIUM.NORMAL ? this.m_tauntInstantPremiumSpell : this.m_tauntInstantSpell;
      if ((Object) original != (Object) null)
      {
        this.m_tauntSpellInstance = Object.Instantiate<Spell>(original);
        TransformUtil.AttachAndPreserveLocalTransform(this.m_tauntSpellInstance.transform, this.m_ritualActor.transform);
        if (!this.m_hideRitualActor)
          this.m_tauntSpellInstance.Activate();
      }
      else
        UnityEngine.Debug.LogWarning((object) "RitualSpellController does not have a instant taunt spell hooked up.");
    }
    this.m_ritualActor.UpdateMinionStatsImmediately();
    if (this.m_hideRitualActor)
      this.m_ritualActor.Hide();
    this.m_ritualActor.transform.parent = Board.Get().FindBone(this.m_ritualActor.GetEntity().GetControllerSide() != Player.Side.FRIENDLY ? this.m_opponentRitualBoneName : this.m_friendlyRitualBoneName);
    this.m_ritualActor.transform.localPosition = Vector3.zero;
  }

  private bool IsCthunInPlay(Player player)
  {
    using (List<Card>.Enumerator enumerator = player.GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (current.GetController() == player && current.GetEntity().GetCardId() == "OG_280")
          return true;
      }
    }
    return false;
  }
}
