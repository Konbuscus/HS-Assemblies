﻿// Decompiled with JetBrains decompiler
// Type: PresenceMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class PresenceMgr
{
  private static readonly Map<Enum, Enum> s_richPresenceMap = new Map<Enum, Enum>()
  {
    {
      (Enum) PresenceStatus.LOGIN,
      (Enum) null
    },
    {
      (Enum) PresenceStatus.WELCOMEQUESTS,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.STORE,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.QUESTLOG,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.PACKOPENING,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.COLLECTION,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.DECKEDITOR,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.CRAFTING,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.PLAY_DECKPICKER,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.PLAY_QUEUE,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.PRACTICE_DECKPICKER,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.ARENA_PURCHASE,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.ARENA_FORGE,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.ARENA_IDLE,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.ARENA_QUEUE,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.ARENA_REWARD,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.FRIENDLY_DECKPICKER,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.ADVENTURE_CHOOSING_MODE,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.ADVENTURE_SCENARIO_SELECT,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_SCREEN,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_DECKEDITOR,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_QUEUE,
      (Enum) PresenceStatus.HUB
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_FRIENDLY_WAITING,
      (Enum) PresenceStatus.HUB
    }
  };
  private static readonly Map<Enum, string> s_stringKeyMap = new Map<Enum, string>()
  {
    {
      (Enum) PresenceStatus.LOGIN,
      "PRESENCE_STATUS_LOGIN"
    },
    {
      (Enum) PresenceStatus.TUTORIAL_PREGAME,
      "PRESENCE_STATUS_TUTORIAL_PREGAME"
    },
    {
      (Enum) PresenceStatus.TUTORIAL_GAME,
      "PRESENCE_STATUS_TUTORIAL_GAME"
    },
    {
      (Enum) PresenceStatus.WELCOMEQUESTS,
      "PRESENCE_STATUS_WELCOMEQUESTS"
    },
    {
      (Enum) PresenceStatus.HUB,
      "PRESENCE_STATUS_HUB"
    },
    {
      (Enum) PresenceStatus.STORE,
      "PRESENCE_STATUS_STORE"
    },
    {
      (Enum) PresenceStatus.QUESTLOG,
      "PRESENCE_STATUS_QUESTLOG"
    },
    {
      (Enum) PresenceStatus.PACKOPENING,
      "PRESENCE_STATUS_PACKOPENING"
    },
    {
      (Enum) PresenceStatus.COLLECTION,
      "PRESENCE_STATUS_COLLECTION"
    },
    {
      (Enum) PresenceStatus.DECKEDITOR,
      "PRESENCE_STATUS_DECKEDITOR"
    },
    {
      (Enum) PresenceStatus.CRAFTING,
      "PRESENCE_STATUS_CRAFTING"
    },
    {
      (Enum) PresenceStatus.PLAY_DECKPICKER,
      "PRESENCE_STATUS_PLAY_DECKPICKER"
    },
    {
      (Enum) PresenceStatus.PLAY_QUEUE,
      "PRESENCE_STATUS_PLAY_QUEUE"
    },
    {
      (Enum) PresenceStatus.PLAY_GAME,
      "PRESENCE_STATUS_PLAY_GAME"
    },
    {
      (Enum) PresenceStatus.PRACTICE_DECKPICKER,
      "PRESENCE_STATUS_PRACTICE_DECKPICKER"
    },
    {
      (Enum) PresenceStatus.PRACTICE_GAME,
      "PRESENCE_STATUS_PRACTICE_GAME"
    },
    {
      (Enum) PresenceStatus.ARENA_PURCHASE,
      "PRESENCE_STATUS_ARENA_PURCHASE"
    },
    {
      (Enum) PresenceStatus.ARENA_FORGE,
      "PRESENCE_STATUS_ARENA_FORGE"
    },
    {
      (Enum) PresenceStatus.ARENA_IDLE,
      "PRESENCE_STATUS_ARENA_IDLE"
    },
    {
      (Enum) PresenceStatus.ARENA_QUEUE,
      "PRESENCE_STATUS_ARENA_QUEUE"
    },
    {
      (Enum) PresenceStatus.ARENA_GAME,
      "PRESENCE_STATUS_ARENA_GAME"
    },
    {
      (Enum) PresenceStatus.ARENA_REWARD,
      "PRESENCE_STATUS_ARENA_REWARD"
    },
    {
      (Enum) PresenceStatus.FRIENDLY_DECKPICKER,
      "PRESENCE_STATUS_FRIENDLY_DECKPICKER"
    },
    {
      (Enum) PresenceStatus.FRIENDLY_GAME,
      "PRESENCE_STATUS_FRIENDLY_GAME"
    },
    {
      (Enum) PresenceStatus.ADVENTURE_CHOOSING_MODE,
      "PRESENCE_STATUS_ADVENTURE_CHOOSING_MODE"
    },
    {
      (Enum) PresenceStatus.ADVENTURE_SCENARIO_SELECT,
      "PRESENCE_STATUS_ADVENTURE_SCENARIO_SELECT"
    },
    {
      (Enum) PresenceStatus.ADVENTURE_SCENARIO_PLAYING_GAME,
      "PRESENCE_STATUS_ADVENTURE_SCENARIO_PLAYING_GAME"
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_SCREEN,
      "PRESENCE_STATUS_TAVERN_BRAWL_SCREEN"
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_DECKEDITOR,
      "PRESENCE_STATUS_TAVERN_BRAWL_DECKEDITOR"
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_QUEUE,
      "PRESENCE_STATUS_TAVERN_BRAWL_QUEUE"
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_GAME,
      "PRESENCE_STATUS_TAVERN_BRAWL_GAME"
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_FRIENDLY_WAITING,
      "PRESENCE_STATUS_TAVERN_BRAWL_FRIENDLY_WAITING"
    },
    {
      (Enum) PresenceStatus.TAVERN_BRAWL_FRIENDLY_GAME,
      "PRESENCE_STATUS_TAVERN_BRAWL_FRIENDLY_GAME"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_TUTORIAL,
      "PRESENCE_STATUS_SPECTATING_GAME_TUTORIAL"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_PRACTICE,
      "PRESENCE_STATUS_SPECTATING_GAME_PRACTICE"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_PLAY,
      "PRESENCE_STATUS_SPECTATING_GAME_PLAY"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ARENA,
      "PRESENCE_STATUS_SPECTATING_GAME_ARENA"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_FRIENDLY,
      "PRESENCE_STATUS_SPECTATING_GAME_FRIENDLY"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_NAXX_NORMAL,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_NAXX_NORMAL"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_NAXX_HEROIC,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_NAXX_HEROIC"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_NAXX_CLASS_CHALLENGE,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_NAXX_CLASS_CHALLENGE"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_BRM_NORMAL,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_BRM_NORMAL"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_BRM_HEROIC,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_BRM_HEROIC"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_BRM_CLASS_CHALLENGE,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_BRM_CLASS_CHALLENGE"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_LOE_NORMAL,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_LOE_NORMAL"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_LOE_HEROIC,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_LOE_HEROIC"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_LOE_CLASS_CHALLENGE,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_LOE_CLASS_CHALLENGE"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_KAR_NORMAL,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_KAR_NORMAL"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_KAR_HEROIC,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_KAR_HEROIC"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_ADVENTURE_KAR_CLASS_CHALLENGE,
      "PRESENCE_STATUS_SPECTATING_GAME_ADVENTURE_KAR_CLASS_CHALLENGE"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_TAVERN_BRAWL,
      "PRESENCE_STATUS_SPECTATING_GAME_TAVERN_BRAWL"
    },
    {
      (Enum) PresenceStatus.SPECTATING_GAME_RETURNING_PLAYER_CHALLENGE,
      "PRESENCE_STATUS_SPECTATING_GAME_RETURNING_PLAYER_CHALLENGE"
    },
    {
      (Enum) PresenceTutorial.HOGGER,
      "PRESENCE_TUTORIAL_HOGGER"
    },
    {
      (Enum) PresenceTutorial.MILLHOUSE,
      "PRESENCE_TUTORIAL_MILLHOUSE"
    },
    {
      (Enum) PresenceTutorial.MUKLA,
      "PRESENCE_TUTORIAL_MUKLA"
    },
    {
      (Enum) PresenceTutorial.HEMET,
      "PRESENCE_TUTORIAL_HEMET"
    },
    {
      (Enum) PresenceTutorial.ILLIDAN,
      "PRESENCE_TUTORIAL_ILLIDAN"
    },
    {
      (Enum) PresenceTutorial.CHO,
      "PRESENCE_TUTORIAL_CHO"
    },
    {
      (Enum) PresenceAdventureMode.NAXX_NORMAL,
      "PRESENCE_ADVENTURE_MODE_NAXX_NORMAL"
    },
    {
      (Enum) PresenceAdventureMode.NAXX_HEROIC,
      "PRESENCE_ADVENTURE_MODE_NAXX_HEROIC"
    },
    {
      (Enum) PresenceAdventureMode.NAXX_CLASS_CHALLENGE,
      "PRESENCE_ADVENTURE_MODE_NAXX_CLASS_CHALLENGE"
    },
    {
      (Enum) PresenceAdventureMode.BRM_NORMAL,
      "PRESENCE_ADVENTURE_MODE_BRM_NORMAL"
    },
    {
      (Enum) PresenceAdventureMode.BRM_HEROIC,
      "PRESENCE_ADVENTURE_MODE_BRM_HEROIC"
    },
    {
      (Enum) PresenceAdventureMode.BRM_CLASS_CHALLENGE,
      "PRESENCE_ADVENTURE_MODE_BRM_CLASS_CHALLENGE"
    },
    {
      (Enum) PresenceAdventureMode.LOE_NORMAL,
      "PRESENCE_ADVENTURE_MODE_LOE_NORMAL"
    },
    {
      (Enum) PresenceAdventureMode.LOE_HEROIC,
      "PRESENCE_ADVENTURE_MODE_LOE_HEROIC"
    },
    {
      (Enum) PresenceAdventureMode.LOE_CLASS_CHALLENGE,
      "PRESENCE_ADVENTURE_MODE_LOE_CLASS_CHALLENGE"
    },
    {
      (Enum) PresenceAdventureMode.KAR_NORMAL,
      "PRESENCE_ADVENTURE_MODE_KAR_NORMAL"
    },
    {
      (Enum) PresenceAdventureMode.KAR_HEROIC,
      "PRESENCE_ADVENTURE_MODE_KAR_HEROIC"
    },
    {
      (Enum) PresenceAdventureMode.KAR_CLASS_CHALLENGE,
      "PRESENCE_ADVENTURE_MODE_KAR_CLASS_CHALLENGE"
    },
    {
      (Enum) PresenceAdventureMode.RETURNING_PLAYER_CHALLENGE,
      "PRESENCE_ADVENTURE_MODE_RETURNING_PLAYER_CHALLENGE"
    },
    {
      (Enum) ScenarioDbId.NAXX_ANUBREKHAN,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_ANUBREKHAN"
    },
    {
      (Enum) ScenarioDbId.NAXX_FAERLINA,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_FAERLINA"
    },
    {
      (Enum) ScenarioDbId.NAXX_MAEXXNA,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_MAEXXNA"
    },
    {
      (Enum) ScenarioDbId.NAXX_NOTH,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_NOTH"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEIGAN,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_HEIGAN"
    },
    {
      (Enum) ScenarioDbId.NAXX_LOATHEB,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_LOATHEB"
    },
    {
      (Enum) ScenarioDbId.NAXX_RAZUVIOUS,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_RAZUVIOUS"
    },
    {
      (Enum) ScenarioDbId.NAXX_GOTHIK,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_GOTHIK"
    },
    {
      (Enum) ScenarioDbId.NAXX_HORSEMEN,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_HORSEMEN"
    },
    {
      (Enum) ScenarioDbId.NAXX_PATCHWERK,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_PATCHWERK"
    },
    {
      (Enum) ScenarioDbId.NAXX_GROBBULUS,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_GROBBULUS"
    },
    {
      (Enum) ScenarioDbId.NAXX_GLUTH,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_GLUTH"
    },
    {
      (Enum) ScenarioDbId.NAXX_THADDIUS,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_THADDIUS"
    },
    {
      (Enum) ScenarioDbId.NAXX_SAPPHIRON,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_SAPPHIRON"
    },
    {
      (Enum) ScenarioDbId.NAXX_KELTHUZAD,
      "PRESENCE_SCENARIO_NAXX_NORMAL_SCENARIO_KELTHUZAD"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_ANUBREKHAN,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_ANUBREKHAN"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_FAERLINA,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_FAERLINA"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_MAEXXNA,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_MAEXXNA"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_NOTH,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_NOTH"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_HEIGAN,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_HEIGAN"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_LOATHEB,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_LOATHEB"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_RAZUVIOUS,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_RAZUVIOUS"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_GOTHIK,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_GOTHIK"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_HORSEMEN,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_HORSEMEN"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_PATCHWERK,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_PATCHWERK"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_GROBBULUS,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_GROBBULUS"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_GLUTH,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_GLUTH"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_THADDIUS,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_THADDIUS"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_SAPPHIRON,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_SAPPHIRON"
    },
    {
      (Enum) ScenarioDbId.NAXX_HEROIC_KELTHUZAD,
      "PRESENCE_SCENARIO_NAXX_HEROIC_SCENARIO_KELTHUZAD"
    },
    {
      (Enum) ScenarioDbId.NAXX_CHALLENGE_HUNTER_V_LOATHEB,
      "PRESENCE_SCENARIO_NAXX_CLASS_CHALLENGE_HUNTER"
    },
    {
      (Enum) ScenarioDbId.NAXX_CHALLENGE_WARRIOR_V_GROBBULUS,
      "PRESENCE_SCENARIO_NAXX_CLASS_CHALLENGE_WARRIOR"
    },
    {
      (Enum) ScenarioDbId.NAXX_CHALLENGE_ROGUE_V_MAEXXNA,
      "PRESENCE_SCENARIO_NAXX_CLASS_CHALLENGE_ROGUE"
    },
    {
      (Enum) ScenarioDbId.NAXX_CHALLENGE_DRUID_V_FAERLINA,
      "PRESENCE_SCENARIO_NAXX_CLASS_CHALLENGE_DRUID"
    },
    {
      (Enum) ScenarioDbId.NAXX_CHALLENGE_PRIEST_V_THADDIUS,
      "PRESENCE_SCENARIO_NAXX_CLASS_CHALLENGE_PRIEST"
    },
    {
      (Enum) ScenarioDbId.NAXX_CHALLENGE_SHAMAN_V_GOTHIK,
      "PRESENCE_SCENARIO_NAXX_CLASS_CHALLENGE_SHAMAN"
    },
    {
      (Enum) ScenarioDbId.NAXX_CHALLENGE_MAGE_V_HEIGAN,
      "PRESENCE_SCENARIO_NAXX_CLASS_CHALLENGE_MAGE"
    },
    {
      (Enum) ScenarioDbId.NAXX_CHALLENGE_PALADIN_V_KELTHUZAD,
      "PRESENCE_SCENARIO_NAXX_CLASS_CHALLENGE_PALADIN"
    },
    {
      (Enum) ScenarioDbId.NAXX_CHALLENGE_WARLOCK_V_HORSEMEN,
      "PRESENCE_SCENARIO_NAXX_CLASS_CHALLENGE_WARLOCK"
    },
    {
      (Enum) ScenarioDbId.BRM_GRIM_GUZZLER,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_GRIM_GUZZLER"
    },
    {
      (Enum) ScenarioDbId.BRM_DARK_IRON_ARENA,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_DARK_IRON_ARENA"
    },
    {
      (Enum) ScenarioDbId.BRM_THAURISSAN,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_THAURISSAN"
    },
    {
      (Enum) ScenarioDbId.BRM_GARR,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_GARR"
    },
    {
      (Enum) ScenarioDbId.BRM_MAJORDOMO,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_MAJORDOMO"
    },
    {
      (Enum) ScenarioDbId.BRM_BARON_GEDDON,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_BARON_GEDDON"
    },
    {
      (Enum) ScenarioDbId.BRM_OMOKK,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_OMOKK"
    },
    {
      (Enum) ScenarioDbId.BRM_DRAKKISATH,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_DRAKKISATH"
    },
    {
      (Enum) ScenarioDbId.BRM_REND_BLACKHAND,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_REND_BLACKHAND"
    },
    {
      (Enum) ScenarioDbId.BRM_RAZORGORE,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_RAZORGORE"
    },
    {
      (Enum) ScenarioDbId.BRM_VAELASTRASZ,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_VAELASTRASZ"
    },
    {
      (Enum) ScenarioDbId.BRM_CHROMAGGUS,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_CHROMAGGUS"
    },
    {
      (Enum) ScenarioDbId.BRM_NEFARIAN,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_NEFARIAN"
    },
    {
      (Enum) ScenarioDbId.BRM_OMNOTRON,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_OMNOTRON"
    },
    {
      (Enum) ScenarioDbId.BRM_MALORIAK,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_MALORIAK"
    },
    {
      (Enum) ScenarioDbId.BRM_ATRAMEDES,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_ATRAMEDES"
    },
    {
      (Enum) ScenarioDbId.BRM_ZOMBIE_NEF,
      "PRESENCE_SCENARIO_BRM_NORMAL_SCENARIO_ZOMBIE_NEF"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_GRIM_GUZZLER,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_GRIM_GUZZLER"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_DARK_IRON_ARENA,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_DARK_IRON_ARENA"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_THAURISSAN,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_THAURISSAN"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_GARR,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_GARR"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_MAJORDOMO,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_MAJORDOMO"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_BARON_GEDDON,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_BARON_GEDDON"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_OMOKK,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_OMOKK"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_DRAKKISATH,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_DRAKKISATH"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_REND_BLACKHAND,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_REND_BLACKHAND"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_RAZORGORE,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_RAZORGORE"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_VAELASTRASZ,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_VAELASTRASZ"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_CHROMAGGUS,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_CHROMAGGUS"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_NEFARIAN,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_NEFARIAN"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_OMNOTRON,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_OMNOTRON"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_MALORIAK,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_MALORIAK"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_ATRAMEDES,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_ATRAMEDES"
    },
    {
      (Enum) ScenarioDbId.BRM_HEROIC_ZOMBIE_NEF,
      "PRESENCE_SCENARIO_BRM_HEROIC_SCENARIO_ZOMBIE_NEF"
    },
    {
      (Enum) ScenarioDbId.BRM_CHALLENGE_HUNTER_V_GUZZLER,
      "PRESENCE_SCENARIO_BRM_CLASS_CHALLENGE_HUNTER"
    },
    {
      (Enum) ScenarioDbId.BRM_CHALLENGE_WARRIOR_V_GARR,
      "PRESENCE_SCENARIO_BRM_CLASS_CHALLENGE_WARRIOR"
    },
    {
      (Enum) ScenarioDbId.BRM_CHALLENGE_ROGUE_V_VAELASTRASZ,
      "PRESENCE_SCENARIO_BRM_CLASS_CHALLENGE_ROGUE"
    },
    {
      (Enum) ScenarioDbId.BRM_CHALLENGE_DRUID_V_BLACKHAND,
      "PRESENCE_SCENARIO_BRM_CLASS_CHALLENGE_DRUID"
    },
    {
      (Enum) ScenarioDbId.BRM_CHALLENGE_PRIEST_V_DRAKKISATH,
      "PRESENCE_SCENARIO_BRM_CLASS_CHALLENGE_PRIEST"
    },
    {
      (Enum) ScenarioDbId.BRM_CHALLENGE_SHAMAN_V_GEDDON,
      "PRESENCE_SCENARIO_BRM_CLASS_CHALLENGE_SHAMAN"
    },
    {
      (Enum) ScenarioDbId.BRM_CHALLENGE_MAGE_V_DARK_IRON_ARENA,
      "PRESENCE_SCENARIO_BRM_CLASS_CHALLENGE_MAGE"
    },
    {
      (Enum) ScenarioDbId.BRM_CHALLENGE_PALADIN_V_OMNOTRON,
      "PRESENCE_SCENARIO_BRM_CLASS_CHALLENGE_PALADIN"
    },
    {
      (Enum) ScenarioDbId.BRM_CHALLENGE_WARLOCK_V_RAZORGORE,
      "PRESENCE_SCENARIO_BRM_CLASS_CHALLENGE_WARLOCK"
    },
    {
      (Enum) ScenarioDbId.LOE_ZINAAR,
      "PRESENCE_SCENARIO_LOE_NORMAL_ZINAAR"
    },
    {
      (Enum) ScenarioDbId.LOE_SUN_RAIDER_PHAERIX,
      "PRESENCE_SCENARIO_LOE_NORMAL_SUN_RAIDER_PHAERIX"
    },
    {
      (Enum) ScenarioDbId.LOE_TEMPLE_ESCAPE,
      "PRESENCE_SCENARIO_LOE_NORMAL_TEMPLE_ESCAPE"
    },
    {
      (Enum) ScenarioDbId.LOE_SCARVASH,
      "PRESENCE_SCENARIO_LOE_NORMAL_SCARVASH"
    },
    {
      (Enum) ScenarioDbId.LOE_MINE_CART,
      "PRESENCE_SCENARIO_LOE_NORMAL_MINE_CART"
    },
    {
      (Enum) ScenarioDbId.LOE_ARCHAEDAS,
      "PRESENCE_SCENARIO_LOE_NORMAL_ARCHAEDAS"
    },
    {
      (Enum) ScenarioDbId.LOE_SLITHERSPEAR,
      "PRESENCE_SCENARIO_LOE_NORMAL_SLITHERSPEAR"
    },
    {
      (Enum) ScenarioDbId.LOE_GIANTFIN,
      "PRESENCE_SCENARIO_LOE_NORMAL_GIANTFIN"
    },
    {
      (Enum) ScenarioDbId.LOE_LADY_NAZJAR,
      "PRESENCE_SCENARIO_LOE_NORMAL_LADY_NAZJAR"
    },
    {
      (Enum) ScenarioDbId.LOE_SKELESAURUS,
      "PRESENCE_SCENARIO_LOE_NORMAL_SKELESAURUS"
    },
    {
      (Enum) ScenarioDbId.LOE_STEEL_SENTINEL,
      "PRESENCE_SCENARIO_LOE_NORMAL_STEEL_SENTINEL"
    },
    {
      (Enum) ScenarioDbId.LOE_RAFAAM_1,
      "PRESENCE_SCENARIO_LOE_NORMAL_RAFAAM_1"
    },
    {
      (Enum) ScenarioDbId.LOE_RAFAAM_2,
      "PRESENCE_SCENARIO_LOE_NORMAL_RAFAAM_2"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_ZINAAR,
      "PRESENCE_SCENARIO_LOE_HEROIC_ZINAAR"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_SUN_RAIDER_PHAERIX,
      "PRESENCE_SCENARIO_LOE_HEROIC_SUN_RAIDER_PHAERIX"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_TEMPLE_ESCAPE,
      "PRESENCE_SCENARIO_LOE_HEROIC_TEMPLE_ESCAPE"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_SCARVASH,
      "PRESENCE_SCENARIO_LOE_HEROIC_SCARVASH"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_MINE_CART,
      "PRESENCE_SCENARIO_LOE_HEROIC_MINE_CART"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_ARCHAEDAS,
      "PRESENCE_SCENARIO_LOE_HEROIC_ARCHAEDAS"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_SLITHERSPEAR,
      "PRESENCE_SCENARIO_LOE_HEROIC_SLITHERSPEAR"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_GIANTFIN,
      "PRESENCE_SCENARIO_LOE_HEROIC_GIANTFIN"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_LADY_NAZJAR,
      "PRESENCE_SCENARIO_LOE_HEROIC_LADY_NAZJAR"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_SKELESAURUS,
      "PRESENCE_SCENARIO_LOE_HEROIC_SKELESAURUS"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_STEEL_SENTINEL,
      "PRESENCE_SCENARIO_LOE_HEROIC_STEEL_SENTINEL"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_RAFAAM_1,
      "PRESENCE_SCENARIO_LOE_HEROIC_RAFAAM_1"
    },
    {
      (Enum) ScenarioDbId.LOE_HEROIC_RAFAAM_2,
      "PRESENCE_SCENARIO_LOE_HEROIC_RAFAAM_2"
    },
    {
      (Enum) ScenarioDbId.LOE_CHALLENGE_WARRIOR_V_ZINAAR,
      "PRESENCE_SCENARIO_LOE_CLASS_CHALLENGE_WARRIOR"
    },
    {
      (Enum) ScenarioDbId.LOE_CHALLENGE_WARLOCK_V_SUN_RAIDER,
      "PRESENCE_SCENARIO_LOE_CLASS_CHALLENGE_WARLOCK"
    },
    {
      (Enum) ScenarioDbId.LOE_CHALLENGE_DRUID_V_SCARVASH,
      "PRESENCE_SCENARIO_LOE_CLASS_CHALLENGE_DRUID"
    },
    {
      (Enum) ScenarioDbId.LOE_CHALLENGE_PALADIN_V_ARCHAEDUS,
      "PRESENCE_SCENARIO_LOE_CLASS_CHALLENGE_PALADIN"
    },
    {
      (Enum) ScenarioDbId.LOE_CHALLENGE_HUNTER_V_SLITHERSPEAR,
      "PRESENCE_SCENARIO_LOE_CLASS_CHALLENGE_HUNTER"
    },
    {
      (Enum) ScenarioDbId.LOE_CHALLENGE_SHAMAN_V_GIANTFIN,
      "PRESENCE_SCENARIO_LOE_CLASS_CHALLENGE_SHAMAN"
    },
    {
      (Enum) ScenarioDbId.LOE_CHALLENGE_PRIEST_V_NAZJAR,
      "PRESENCE_SCENARIO_LOE_CLASS_CHALLENGE_PRIEST"
    },
    {
      (Enum) ScenarioDbId.LOE_CHALLENGE_ROGUE_V_SKELESAURUS,
      "PRESENCE_SCENARIO_LOE_CLASS_CHALLENGE_ROGUE"
    },
    {
      (Enum) ScenarioDbId.LOE_CHALLENGE_MAGE_V_SENTINEL,
      "PRESENCE_SCENARIO_LOE_CLASS_CHALLENGE_MAGE"
    },
    {
      (Enum) ScenarioDbId.KAR_PROLOGUE,
      "PRESENCE_SCENARIO_KAR_NORMAL_PROLOGUE"
    },
    {
      (Enum) ScenarioDbId.KAR_PANTRY,
      "PRESENCE_SCENARIO_KAR_NORMAL_PANTRY"
    },
    {
      (Enum) ScenarioDbId.KAR_MIRROR,
      "PRESENCE_SCENARIO_KAR_NORMAL_MIRROR"
    },
    {
      (Enum) ScenarioDbId.KAR_CHESS,
      "PRESENCE_SCENARIO_KAR_NORMAL_CHESS"
    },
    {
      (Enum) ScenarioDbId.KAR_JULIANNE,
      "PRESENCE_SCENARIO_KAR_NORMAL_JULIANNE"
    },
    {
      (Enum) ScenarioDbId.KAR_WOLF,
      "PRESENCE_SCENARIO_KAR_NORMAL_WOLF"
    },
    {
      (Enum) ScenarioDbId.KAR_CRONE,
      "PRESENCE_SCENARIO_KAR_NORMAL_CRONE"
    },
    {
      (Enum) ScenarioDbId.KAR_CURATOR,
      "PRESENCE_SCENARIO_KAR_NORMAL_CURATOR"
    },
    {
      (Enum) ScenarioDbId.KAR_NIGHTBANE,
      "PRESENCE_SCENARIO_KAR_NORMAL_NIGHTBANE"
    },
    {
      (Enum) ScenarioDbId.KAR_ILLHOOF,
      "PRESENCE_SCENARIO_KAR_NORMAL_ILLHOOF"
    },
    {
      (Enum) ScenarioDbId.KAR_ARAN,
      "PRESENCE_SCENARIO_KAR_NORMAL_ARAN"
    },
    {
      (Enum) ScenarioDbId.KAR_NETHERSPITE,
      "PRESENCE_SCENARIO_KAR_NORMAL_NETHERSPITE"
    },
    {
      (Enum) ScenarioDbId.KAR_PORTALS,
      "PRESENCE_SCENARIO_KAR_NORMAL_PORTALS"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_PROLOGUE,
      "PRESENCE_SCENARIO_KAR_HEROIC_PROLOGUE"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_PANTRY,
      "PRESENCE_SCENARIO_KAR_HEROIC_PANTRY"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_MIRROR,
      "PRESENCE_SCENARIO_KAR_HEROIC_MIRROR"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_CHESS,
      "PRESENCE_SCENARIO_KAR_HEROIC_CHESS"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_JULIANNE,
      "PRESENCE_SCENARIO_KAR_HEROIC_JULIANNE"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_WOLF,
      "PRESENCE_SCENARIO_KAR_HEROIC_WOLF"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_CRONE,
      "PRESENCE_SCENARIO_KAR_HEROIC_CRONE"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_CURATOR,
      "PRESENCE_SCENARIO_KAR_HEROIC_CURATOR"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_NIGHTBANE,
      "PRESENCE_SCENARIO_KAR_HEROIC_NIGHTBANE"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_ILLHOOF,
      "PRESENCE_SCENARIO_KAR_HEROIC_ILLHOOF"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_ARAN,
      "PRESENCE_SCENARIO_KAR_HEROIC_ARAN"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_NETHERSPITE,
      "PRESENCE_SCENARIO_KAR_HEROIC_NETHERSPITE"
    },
    {
      (Enum) ScenarioDbId.KAR_HEROIC_PORTALS,
      "PRESENCE_SCENARIO_KAR_HEROIC_PORTALS"
    },
    {
      (Enum) ScenarioDbId.KAR_CHALLENGE_SHAMAN_V_MIRROR,
      "PRESENCE_SCENARIO_KAR_CLASS_CHALLENGE_SHAMAN"
    },
    {
      (Enum) ScenarioDbId.KAR_CHALLENGE_PRIEST_V_PANTRY,
      "PRESENCE_SCENARIO_KAR_CLASS_CHALLENGE_PRIEST"
    },
    {
      (Enum) ScenarioDbId.KAR_CHALLENGE_PALADIN_V_WOLF,
      "PRESENCE_SCENARIO_KAR_CLASS_CHALLENGE_PALADIN"
    },
    {
      (Enum) ScenarioDbId.KAR_CHALLENGE_WARLOCK_V_JULIANNE,
      "PRESENCE_SCENARIO_KAR_CLASS_CHALLENGE_WARLOCK"
    },
    {
      (Enum) ScenarioDbId.KAR_CHALLENGE_HUNTER_V_CURATOR,
      "PRESENCE_SCENARIO_KAR_CLASS_CHALLENGE_HUNTER"
    },
    {
      (Enum) ScenarioDbId.KAR_CHALLENGE_WARRIOR_V_ILLHOOF,
      "PRESENCE_SCENARIO_KAR_CLASS_CHALLENGE_WARRIOR"
    },
    {
      (Enum) ScenarioDbId.KAR_CHALLENGE_MAGE_V_NIGHTBANE,
      "PRESENCE_SCENARIO_KAR_CLASS_CHALLENGE_MAGE"
    },
    {
      (Enum) ScenarioDbId.KAR_CHALLENGE_ROGUE_V_ARAN,
      "PRESENCE_SCENARIO_KAR_CLASS_CHALLENGE_ROGUE"
    },
    {
      (Enum) ScenarioDbId.KAR_CHALLENGE_DRUID_V_NETHERSPITE,
      "PRESENCE_SCENARIO_KAR_CLASS_CHALLENGE_DRUID"
    },
    {
      (Enum) ScenarioDbId.RETURNING_PLAYER_CHALLENGE_1,
      "PRESENCE_SCENARIO_RETURNING_PLAYER_CHALLENGE_1"
    },
    {
      (Enum) ScenarioDbId.RETURNING_PLAYER_CHALLENGE_2,
      "PRESENCE_SCENARIO_RETURNING_PLAYER_CHALLENGE_2"
    },
    {
      (Enum) ScenarioDbId.RETURNING_PLAYER_CHALLENGE_3,
      "PRESENCE_SCENARIO_RETURNING_PLAYER_CHALLENGE_3"
    }
  };
  private static readonly Map<KeyValuePair<AdventureDbId, AdventureModeDbId>, PresenceMgr.PresenceTargets> s_adventurePresenceMap = new Map<KeyValuePair<AdventureDbId, AdventureModeDbId>, PresenceMgr.PresenceTargets>()
  {
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.NAXXRAMAS, AdventureModeDbId.NORMAL),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.NAXX_NORMAL, PresenceStatus.SPECTATING_GAME_ADVENTURE_NAXX_NORMAL)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.NAXXRAMAS, AdventureModeDbId.HEROIC),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.NAXX_HEROIC, PresenceStatus.SPECTATING_GAME_ADVENTURE_NAXX_HEROIC)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.NAXXRAMAS, AdventureModeDbId.CLASS_CHALLENGE),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.NAXX_CLASS_CHALLENGE, PresenceStatus.SPECTATING_GAME_ADVENTURE_NAXX_CLASS_CHALLENGE)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.BRM, AdventureModeDbId.NORMAL),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.BRM_NORMAL, PresenceStatus.SPECTATING_GAME_ADVENTURE_BRM_NORMAL)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.BRM, AdventureModeDbId.HEROIC),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.BRM_HEROIC, PresenceStatus.SPECTATING_GAME_ADVENTURE_BRM_HEROIC)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.BRM, AdventureModeDbId.CLASS_CHALLENGE),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.BRM_CLASS_CHALLENGE, PresenceStatus.SPECTATING_GAME_ADVENTURE_BRM_CLASS_CHALLENGE)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.LOE, AdventureModeDbId.NORMAL),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.LOE_NORMAL, PresenceStatus.SPECTATING_GAME_ADVENTURE_LOE_NORMAL)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.LOE, AdventureModeDbId.HEROIC),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.LOE_HEROIC, PresenceStatus.SPECTATING_GAME_ADVENTURE_LOE_HEROIC)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.LOE, AdventureModeDbId.CLASS_CHALLENGE),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.LOE_CLASS_CHALLENGE, PresenceStatus.SPECTATING_GAME_ADVENTURE_LOE_CLASS_CHALLENGE)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.KARA, AdventureModeDbId.NORMAL),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.KAR_NORMAL, PresenceStatus.SPECTATING_GAME_ADVENTURE_KAR_NORMAL)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.KARA, AdventureModeDbId.HEROIC),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.KAR_HEROIC, PresenceStatus.SPECTATING_GAME_ADVENTURE_KAR_HEROIC)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.KARA, AdventureModeDbId.CLASS_CHALLENGE),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.KAR_CLASS_CHALLENGE, PresenceStatus.SPECTATING_GAME_ADVENTURE_KAR_CLASS_CHALLENGE)
    },
    {
      new KeyValuePair<AdventureDbId, AdventureModeDbId>(AdventureDbId.RETURNING_PLAYER, AdventureModeDbId.CLASS_CHALLENGE),
      new PresenceMgr.PresenceTargets(PresenceAdventureMode.RETURNING_PLAYER_CHALLENGE, PresenceStatus.SPECTATING_GAME_RETURNING_PLAYER_CHALLENGE)
    }
  };
  private static readonly System.Type[] s_enumIdList = new System.Type[4]
  {
    typeof (PresenceStatus),
    typeof (PresenceTutorial),
    typeof (PresenceAdventureMode),
    typeof (ScenarioDbId)
  };
  private Map<System.Type, byte> m_enumToIdMap = new Map<System.Type, byte>();
  private Map<byte, System.Type> m_idToEnumMap = new Map<byte, System.Type>();
  private static PresenceMgr s_instance;
  private Enum[] m_prevStatus;
  private Enum[] m_status;
  private Enum[] m_richPresence;

  public PresenceStatus CurrentStatus
  {
    get
    {
      if (this.m_status == null || this.m_status.Length == 0)
        return PresenceStatus.UNKNOWN;
      return (PresenceStatus) this.m_status[0];
    }
  }

  public static PresenceMgr Get()
  {
    if (PresenceMgr.s_instance == null)
    {
      PresenceMgr.s_instance = new PresenceMgr();
      PresenceMgr.s_instance.Initialize();
    }
    return PresenceMgr.s_instance;
  }

  public bool SetStatus(params Enum[] args)
  {
    return this.SetStatusImpl(args);
  }

  public bool SetStatus_EnteringAdventure(AdventureDbId adventureId, AdventureModeDbId adventureModeId)
  {
    KeyValuePair<AdventureDbId, AdventureModeDbId> key = new KeyValuePair<AdventureDbId, AdventureModeDbId>(adventureId, adventureModeId);
    PresenceMgr.PresenceTargets presenceTargets;
    if (!PresenceMgr.s_adventurePresenceMap.TryGetValue(key, out presenceTargets))
      return false;
    this.SetStatus((Enum) PresenceStatus.ADVENTURE_SCENARIO_SELECT, (Enum) presenceTargets.EnteringAdventureValue);
    return true;
  }

  public bool SetStatus_PlayingMission(ScenarioDbId missionId)
  {
    if (!PresenceMgr.s_stringKeyMap.ContainsKey((Enum) missionId))
      return false;
    return this.SetStatus((Enum) PresenceStatus.ADVENTURE_SCENARIO_PLAYING_GAME, (Enum) missionId);
  }

  public bool SetStatus_SpectatingMission(ScenarioDbId missionId)
  {
    KeyValuePair<AdventureDbId, AdventureModeDbId> key = new KeyValuePair<AdventureDbId, AdventureModeDbId>(GameUtils.GetAdventureId((int) missionId), GameUtils.GetAdventureModeId((int) missionId));
    PresenceMgr.PresenceTargets presenceTargets;
    if (!PresenceMgr.s_adventurePresenceMap.TryGetValue(key, out presenceTargets))
      return false;
    return this.SetStatus((Enum) presenceTargets.SpectatingValue);
  }

  public Enum[] GetStatus()
  {
    return this.m_status;
  }

  public Enum[] GetPrevStatus()
  {
    return this.m_prevStatus;
  }

  public bool SetPrevStatus()
  {
    return this.SetStatusImpl(this.m_prevStatus);
  }

  public bool IsRichPresence(params Enum[] status)
  {
    if (status == null || status.Length == 0)
      return false;
    for (int index = 0; index < status.Length; ++index)
    {
      Enum statu = status[index];
      if (statu == null || PresenceMgr.s_richPresenceMap.ContainsKey(statu))
        return false;
    }
    return true;
  }

  public string GetStatusText(BnetPlayer player)
  {
    List<string> stringArgs = new List<string>();
    string statusKey = (string) null;
    if (this.GetStatus_Internal(player, ref statusKey, stringArgs, (List<Enum>) null) == PresenceStatus.UNKNOWN)
    {
      BnetGameAccount bestGameAccount = player.GetBestGameAccount();
      if (bestGameAccount == (BnetGameAccount) null)
        return (string) null;
      return bestGameAccount.GetRichPresence();
    }
    string[] array = stringArgs.ToArray();
    return GameStrings.Format(statusKey, (object[]) array);
  }

  public PresenceStatus GetStatus(BnetPlayer player)
  {
    string statusKey = (string) null;
    return this.GetStatus_Internal(player, ref statusKey, (List<string>) null, (List<Enum>) null);
  }

  public Enum[] GetStatusEnums(BnetPlayer player)
  {
    string statusKey = (string) null;
    List<Enum> enumVals = new List<Enum>();
    int statusInternal = (int) this.GetStatus_Internal(player, ref statusKey, (List<string>) null, enumVals);
    return enumVals.ToArray();
  }

  private PresenceStatus GetStatus_Internal(BnetPlayer player, ref string statusKey, List<string> stringArgs = null, List<Enum> enumVals = null)
  {
    PresenceStatus presenceStatus = PresenceStatus.UNKNOWN;
    if (player == null || player.GetBestGameAccount() == (BnetGameAccount) null)
      return presenceStatus;
    BnetGameAccount hearthstoneGameAccount = player.GetHearthstoneGameAccount();
    byte[] val;
    if (hearthstoneGameAccount == (BnetGameAccount) null || !hearthstoneGameAccount.TryGetGameFieldBytes(17U, out val))
      return presenceStatus;
    Enum enumVal = (Enum) null;
    using (MemoryStream memoryStream = new MemoryStream(val))
    {
      using (BinaryReader reader = new BinaryReader((Stream) memoryStream))
      {
        if (!this.DecodeStatusVal(reader, ref enumVal, ref statusKey))
          return presenceStatus;
        presenceStatus = (PresenceStatus) enumVal;
        if (enumVals != null)
          enumVals.Add((Enum) presenceStatus);
        if (stringArgs == null)
        {
          if (enumVals == null)
            goto label_25;
        }
        while (memoryStream.Position < (long) val.Length)
        {
          string key = (string) null;
          if (!this.DecodeStatusVal(reader, ref enumVal, ref key))
            return presenceStatus;
          if (enumVals != null)
            enumVals.Add(enumVal);
          if (stringArgs != null)
          {
            string str = GameStrings.Get(key);
            stringArgs.Add(str);
          }
        }
      }
    }
label_25:
    return presenceStatus;
  }

  public static Map<Enum, string> GetEnumStringMap()
  {
    return PresenceMgr.s_stringKeyMap;
  }

  private void Initialize()
  {
    for (int index = 0; index < PresenceMgr.s_enumIdList.Length; ++index)
    {
      System.Type enumId = PresenceMgr.s_enumIdList[index];
      if (Enum.GetUnderlyingType(enumId) != typeof (int))
        throw new Exception(string.Format("Underlying type of enum {0} (underlying={1}) must {2} be to used by Presence system.", (object) enumId.FullName, (object) Enum.GetUnderlyingType(enumId).FullName, (object) typeof (int).Name));
      byte key = (byte) (index + 1);
      this.m_enumToIdMap.Add(enumId, key);
      this.m_idToEnumMap.Add(key, enumId);
    }
  }

  private bool SetStatusImpl(Enum[] status)
  {
    if (!Network.ShouldBeConnectedToAurora())
      return false;
    if (!Network.IsLoggedIn())
      return true;
    if (status == null || status.Length == 0)
    {
      Error.AddDevFatal("PresenceMgr.SetStatusImpl() - Received status of length 0. Setting empty status is not supported.");
      return false;
    }
    if (GeneralUtils.AreArraysEqual<Enum>(this.m_status, status))
      return true;
    if (!this.SetRichPresence(status) || !this.SetGamePresence(status))
      return false;
    BIReport.Get().Report_PresenceChange((PresenceStatus) status[0], status.Length != 1 ? ((IEnumerable<Enum>) status).Skip<Enum>(1).Cast<int>() : (IEnumerable<int>) null);
    this.m_prevStatus = this.m_status;
    bool flag = this.m_prevStatus == null || this.m_prevStatus.Length == 0;
    this.m_status = new Enum[status.Length];
    Array.Copy((Array) status, (Array) this.m_status, status.Length);
    if (flag || !PresenceMgr.IsStatusPlayingGame((PresenceStatus) status[0]))
      SpectatorManager.Get().UpdateMySpectatorInfo();
    return true;
  }

  private bool SetRichPresence(Enum[] status)
  {
    Enum[] arr2 = new Enum[status.Length];
    for (int index = 0; index < status.Length; ++index)
    {
      Enum statu = status[index];
      Enum @enum;
      if (PresenceMgr.s_richPresenceMap.TryGetValue(statu, out @enum))
      {
        if (@enum == null)
          return false;
      }
      else
        @enum = statu;
      arr2[index] = @enum;
    }
    if (((IEnumerable<Enum>) arr2).Any<Enum>((Func<Enum, bool>) (e => !RichPresence.s_streamIds.ContainsKey(e.GetType()))))
      arr2 = new Enum[1]{ arr2[0] };
    if (GeneralUtils.AreArraysEqual<Enum>(this.m_richPresence, arr2))
      return true;
    this.m_richPresence = arr2;
    if (!Network.ShouldBeConnectedToAurora())
    {
      Error.AddDevFatal(string.Format("Caller should check for Battle.net connection before calling SetRichPresence {0}", arr2 != null ? (object) string.Join(", ", ((IEnumerable<Enum>) arr2).Select<Enum, string>((Func<Enum, string>) (x => x.ToString())).ToArray<string>()) : (object) string.Empty));
      return false;
    }
    if (arr2 == null || arr2.Length == 0)
      return false;
    RichPresenceUpdate[] updates = new RichPresenceUpdate[arr2.Length];
    for (int index = 0; index < arr2.Length; ++index)
    {
      Enum @enum = arr2[index];
      System.Type type = @enum.GetType();
      FourCC streamId = RichPresence.s_streamIds[type];
      updates[index] = new RichPresenceUpdate()
      {
        presenceFieldIndex = index != 0 ? (ulong) (uint) (458752 + index) : 0UL,
        programId = BnetProgramId.HEARTHSTONE.GetValue(),
        streamId = streamId.GetValue(),
        index = Convert.ToUInt32((object) @enum)
      };
    }
    BattleNet.SetRichPresence(updates);
    return true;
  }

  private bool SetGamePresence(Enum[] status)
  {
    using (MemoryStream memoryStream = new MemoryStream())
    {
      using (BinaryWriter binaryWriter = new BinaryWriter((Stream) memoryStream))
      {
        for (int index = 0; index < status.Length; ++index)
        {
          byte id;
          int intVal;
          if (!this.EncodeStatusVal(status, index, out id, out intVal))
            return false;
          binaryWriter.Write(id);
          binaryWriter.Write(intVal);
        }
        byte[] buffer = memoryStream.GetBuffer();
        byte[] val = new byte[memoryStream.Position];
        Array.Copy((Array) buffer, (Array) val, val.Length);
        return BnetPresenceMgr.Get().SetGameField(17U, val);
      }
    }
  }

  private bool EncodeStatusVal(Enum[] status, int index, out byte id, out int intVal)
  {
    Enum statu = status[index];
    System.Type type = statu.GetType();
    intVal = Convert.ToInt32((object) statu);
    if (this.m_enumToIdMap.TryGetValue(type, out id))
      return true;
    Error.AddDevFatal("PresenceMgr.EncodeStatusVal() - {0} at index {1} belongs to type {2}, which has no id", (object) statu, (object) index, (object) type);
    return false;
  }

  private bool DecodeStatusVal(BinaryReader reader, ref Enum enumVal, ref string key)
  {
    key = (string) null;
    byte key1 = 0;
    int position1 = (int) reader.BaseStream.Position;
    int num1 = position1 + 1;
    int position2;
    try
    {
      key1 = reader.ReadByte();
      position2 = (int) reader.BaseStream.Position;
    }
    catch (Exception ex)
    {
      Log.Henry.Print("PresenceMgr.DecodeStatusVal - unable to decode enum id {0} at index {1} : {2} {3}", (object) key1, (object) position1, (object) ex.GetType().FullName, (object) ex.Message);
      return false;
    }
    System.Type type;
    if (!this.m_idToEnumMap.TryGetValue(key1, out type))
    {
      Log.Henry.Print("PresenceMgr.DecodeStatusVal - id {0} at index {1}, has no enum type", new object[2]
      {
        (object) key1,
        (object) position1
      });
      return false;
    }
    int num2;
    try
    {
      num2 = reader.ReadInt32();
    }
    catch (Exception ex)
    {
      Log.Henry.Print("PresenceMgr.DecodeStatusVal - unable to decode enum value {0} at index {1} : {2} {3}", (object) key1, (object) position2, (object) ex.GetType().FullName, (object) ex.Message);
      return false;
    }
    if (type == typeof (PresenceStatus))
    {
      PresenceStatus presenceStatus = (PresenceStatus) num2;
      enumVal = (Enum) presenceStatus;
      if (!PresenceMgr.s_stringKeyMap.TryGetValue((Enum) presenceStatus, out key))
      {
        Log.Henry.Print("PresenceMgr.DecodeStatusVal - value {0}.{1} at index {2}, has no string", new object[3]
        {
          (object) type,
          (object) presenceStatus,
          (object) position2
        });
        return false;
      }
    }
    else if (type == typeof (PresenceTutorial))
    {
      PresenceTutorial presenceTutorial = (PresenceTutorial) num2;
      enumVal = (Enum) presenceTutorial;
      if (!PresenceMgr.s_stringKeyMap.TryGetValue((Enum) presenceTutorial, out key))
      {
        Log.Henry.Print("PresenceMgr.DecodeStatusVal - value {0}.{1} at index {2}, has no string", new object[3]
        {
          (object) type,
          (object) presenceTutorial,
          (object) position2
        });
        return false;
      }
    }
    else if (type == typeof (PresenceAdventureMode))
    {
      PresenceAdventureMode presenceAdventureMode = (PresenceAdventureMode) num2;
      enumVal = (Enum) presenceAdventureMode;
      if (!PresenceMgr.s_stringKeyMap.TryGetValue((Enum) presenceAdventureMode, out key))
      {
        Log.Henry.Print("PresenceMgr.DecodeStatusVal - value {0}.{1} at index {2}, has no string", new object[3]
        {
          (object) type,
          (object) presenceAdventureMode,
          (object) position2
        });
        return false;
      }
    }
    else if (type == typeof (ScenarioDbId))
    {
      ScenarioDbId scenarioDbId = (ScenarioDbId) num2;
      enumVal = (Enum) scenarioDbId;
      if (!PresenceMgr.s_stringKeyMap.TryGetValue((Enum) scenarioDbId, out key))
      {
        Log.Henry.Print("PresenceMgr.DecodeStatusVal - value {0}.{1} at index {2}, has no string", new object[3]
        {
          (object) type,
          (object) scenarioDbId,
          (object) position2
        });
        return false;
      }
    }
    return true;
  }

  public static bool IsStatusPlayingGame(PresenceStatus status)
  {
    PresenceStatus presenceStatus = status;
    switch (presenceStatus)
    {
      case PresenceStatus.ARENA_GAME:
      case PresenceStatus.FRIENDLY_GAME:
label_4:
        return true;
      default:
        switch (presenceStatus - 13)
        {
          case PresenceStatus.LOGIN:
          case PresenceStatus.TUTORIAL_GAME:
            goto label_4;
          default:
            switch (presenceStatus - 41)
            {
              case PresenceStatus.LOGIN:
              case PresenceStatus.TUTORIAL_GAME:
                goto label_4;
              default:
                if (presenceStatus != PresenceStatus.TUTORIAL_GAME && presenceStatus != PresenceStatus.ADVENTURE_SCENARIO_PLAYING_GAME)
                  return false;
                goto label_4;
            }
        }
    }
  }

  private struct PresenceTargets
  {
    public PresenceAdventureMode EnteringAdventureValue;
    public PresenceStatus SpectatingValue;

    public PresenceTargets(PresenceAdventureMode enteringAdventureValue, PresenceStatus spectatingValue)
    {
      this.EnteringAdventureValue = enteringAdventureValue;
      this.SpectatingValue = spectatingValue;
    }
  }
}
