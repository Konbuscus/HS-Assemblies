﻿// Decompiled with JetBrains decompiler
// Type: TB07_FactionWars
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TB07_FactionWars : MissionEntity
{
  private Notification GameOverPopup;
  private string textID;
  private Vector3 popUpPos;

  public override void PreloadAssets()
  {
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TB07_FactionWars.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1F3() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
