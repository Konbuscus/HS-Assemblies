﻿// Decompiled with JetBrains decompiler
// Type: Player
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using PegasusClient;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity
{
  private BnetGameAccountId m_gameAccountId;
  private bool m_waitingForHeroEntity;
  private string m_name;
  private bool m_local;
  private Player.Side m_side;
  private int m_cardBackId;
  private ManaCounter m_manaCounter;
  private Entity m_startingHero;
  private Entity m_hero;
  private Entity m_heroPower;
  private int m_queuedSpentMana;
  private int m_usedTempMana;
  private int m_realtimeTempMana;
  private bool m_realTimeComboActive;
  private MedalInfoTranslator m_medalInfo;
  private uint m_arenaWins;
  private uint m_arenaLoss;
  private uint m_tavernBrawlWins;
  private uint m_tavernBrawlLoss;
  private bool m_concedeEmotePlayed;
  private TAG_PLAYSTATE m_preGameOverPlayState;
  private bool m_hasSeenMalchezaarSpell;

  public void InitPlayer(Network.HistCreateGame.PlayerData netPlayer)
  {
    this.SetPlayerId(netPlayer.ID);
    this.SetGameAccountId(netPlayer.GameAccountId);
    this.SetCardBackId(netPlayer.CardBackID);
    this.SetTags(netPlayer.Player.Tags);
    GameState.Get().RegisterTurnChangedListener(new GameState.TurnChangedCallback(this.OnTurnChanged));
    if (!this.IsFriendlySide())
      return;
    GameState.Get().RegisterOptionsReceivedListener(new GameState.OptionsReceivedCallback(this.OnFriendlyOptionsReceived));
    GameState.Get().RegisterOptionsSentListener(new GameState.OptionsSentCallback(this.OnFriendlyOptionsSent), (object) null);
    GameState.Get().RegisterFriendlyTurnStartedListener(new GameState.FriendlyTurnStartedCallback(this.OnFriendlyTurnStarted), (object) null);
  }

  public override string GetName()
  {
    return this.m_name;
  }

  public MedalInfoTranslator GetRank()
  {
    return this.m_medalInfo;
  }

  public override string GetDebugName()
  {
    if (this.m_name != null)
      return this.m_name;
    if (this.IsAI())
      return GameStrings.Get("GAMEPLAY_AI_OPPONENT_NAME");
    return "UNKNOWN HUMAN PLAYER";
  }

  public void SetName(string name)
  {
    this.m_name = name;
  }

  public void SetGameAccountId(BnetGameAccountId id)
  {
    this.m_gameAccountId = id;
    this.UpdateLocal();
    this.UpdateSide();
    if (this.IsDisplayable())
    {
      this.UpdateDisplayInfo();
    }
    else
    {
      this.UpdateRank();
      this.UpdateSessionRecord();
      if (!this.IsBnetPlayer())
        return;
      BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnBnetPlayersChanged));
      if (BnetFriendMgr.Get().IsFriend(this.m_gameAccountId) || !GameMgr.Get().IsSpectator() && GameMgr.Get().GetReconnectType() != ReconnectType.LOGIN)
        return;
      this.RequestPlayerPresence();
    }
  }

  private void RequestPlayerPresence()
  {
    EntityId entityId = new EntityId();
    entityId.hi = this.m_gameAccountId.GetHi();
    entityId.lo = this.m_gameAccountId.GetLo();
    List<PresenceFieldKey> presenceFieldKeyList = new List<PresenceFieldKey>();
    PresenceFieldKey presenceFieldKey = new PresenceFieldKey();
    presenceFieldKey.programId = BnetProgramId.BNET.GetValue();
    presenceFieldKey.groupId = 2U;
    presenceFieldKey.fieldId = 7U;
    presenceFieldKey.index = 0UL;
    presenceFieldKeyList.Add(presenceFieldKey);
    presenceFieldKey.programId = BnetProgramId.BNET.GetValue();
    presenceFieldKey.groupId = 2U;
    presenceFieldKey.fieldId = 3U;
    presenceFieldKey.index = 0UL;
    presenceFieldKeyList.Add(presenceFieldKey);
    presenceFieldKey.programId = BnetProgramId.BNET.GetValue();
    presenceFieldKey.groupId = 2U;
    presenceFieldKey.fieldId = 5U;
    presenceFieldKey.index = 0UL;
    presenceFieldKeyList.Add(presenceFieldKey);
    if (GameUtils.ShouldShowRankedMedals())
      presenceFieldKeyList.Add(new PresenceFieldKey()
      {
        programId = BnetProgramId.HEARTHSTONE.GetValue(),
        groupId = 2U,
        fieldId = 18U,
        index = 0UL
      });
    PresenceFieldKey[] array = presenceFieldKeyList.ToArray();
    BattleNet.RequestPresenceFields(true, entityId, array);
  }

  public bool IsLocalUser()
  {
    return this.m_local;
  }

  public void SetLocalUser(bool local)
  {
    this.m_local = local;
    this.UpdateSide();
  }

  public bool IsAI()
  {
    if ((BnetEntityId) this.m_gameAccountId == (BnetEntityId) null)
      return false;
    return !this.m_gameAccountId.IsValid();
  }

  public bool IsHuman()
  {
    if ((BnetEntityId) this.m_gameAccountId == (BnetEntityId) null)
      return false;
    return this.m_gameAccountId.IsValid();
  }

  public bool IsBnetPlayer()
  {
    if (!this.IsHuman())
      return false;
    return Network.ShouldBeConnectedToAurora();
  }

  public bool IsGuestPlayer()
  {
    if (!this.IsHuman())
      return false;
    return !Network.ShouldBeConnectedToAurora();
  }

  public Player.Side GetSide()
  {
    return this.m_side;
  }

  public bool IsFriendlySide()
  {
    return this.m_side == Player.Side.FRIENDLY;
  }

  public bool IsOpposingSide()
  {
    return this.m_side == Player.Side.OPPOSING;
  }

  public bool IsRevealed()
  {
    return this.IsFriendlySide() || SpectatorManager.Get().IsSpectatingPlayer(this.m_gameAccountId);
  }

  public void SetSide(Player.Side side)
  {
    this.m_side = side;
  }

  public int GetCardBackId()
  {
    return this.m_cardBackId;
  }

  public void SetCardBackId(int id)
  {
    this.m_cardBackId = id;
  }

  public int GetPlayerId()
  {
    return this.GetTag(GAME_TAG.PLAYER_ID);
  }

  public void SetPlayerId(int playerId)
  {
    this.SetTag(GAME_TAG.PLAYER_ID, playerId);
  }

  public List<string> GetSecretDefinitions()
  {
    List<string> stringList = new List<string>();
    using (List<Zone>.Enumerator enumerator1 = ZoneMgr.Get().GetZones().GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        Zone current1 = enumerator1.Current;
        if (current1 is ZoneSecret && current1.m_Side == Player.Side.FRIENDLY)
        {
          using (List<Card>.Enumerator enumerator2 = current1.GetCards().GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Card current2 = enumerator2.Current;
              stringList.Add(current2.GetEntity().GetCardId());
            }
          }
        }
      }
    }
    return stringList;
  }

  public bool IsCurrentPlayer()
  {
    return this.HasTag(GAME_TAG.CURRENT_PLAYER);
  }

  public bool IsComboActive()
  {
    return this.HasTag(GAME_TAG.COMBO_ACTIVE);
  }

  public bool IsRealTimeComboActive()
  {
    return this.m_realTimeComboActive;
  }

  public void SetRealTimeComboActive(int tagValue)
  {
    this.SetRealTimeComboActive(tagValue == 1);
  }

  public void SetRealTimeComboActive(bool active)
  {
    this.m_realTimeComboActive = active;
  }

  public int GetNumAvailableResources()
  {
    int tag1 = this.GetTag(GAME_TAG.TEMP_RESOURCES);
    int tag2 = this.GetTag(GAME_TAG.RESOURCES);
    int tag3 = this.GetTag(GAME_TAG.RESOURCES_USED);
    int num = tag2 + tag1 - tag3 - this.m_queuedSpentMana - this.m_usedTempMana;
    if (num < 0)
      return 0;
    return num;
  }

  public bool HasWeapon()
  {
    using (List<Zone>.Enumerator enumerator = ZoneMgr.Get().GetZones().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current is ZoneWeapon && current.m_Side == this.m_side)
          return current.GetCards().Count > 0;
      }
    }
    return false;
  }

  public void SetHero(Entity hero)
  {
    this.m_hero = hero;
    if (this.m_startingHero == null && hero != null)
      this.m_startingHero = hero;
    if (this.ShouldUseHeroName())
      this.UpdateDisplayInfo();
    using (List<Card>.Enumerator enumerator = this.GetHandZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (current.GetEntity().IsMultiClass())
          current.UpdateActorComponents();
      }
    }
    if (!this.IsFriendlySide())
      return;
    GameState.Get().FireHeroChangedEvent(this);
  }

  public Entity GetStartingHero()
  {
    return this.m_startingHero;
  }

  public override Entity GetHero()
  {
    return this.m_hero;
  }

  public EntityDef GetHeroEntityDef()
  {
    if (this.m_hero == null)
      return (EntityDef) null;
    return this.m_hero.GetEntityDef() ?? (EntityDef) null;
  }

  public override Card GetHeroCard()
  {
    if (this.m_hero == null)
      return (Card) null;
    return this.m_hero.GetCard();
  }

  public void SetHeroPower(Entity heroPower)
  {
    this.m_heroPower = heroPower;
  }

  public override Entity GetHeroPower()
  {
    return this.m_heroPower;
  }

  public override Card GetHeroPowerCard()
  {
    if (this.m_heroPower == null)
      return (Card) null;
    return this.m_heroPower.GetCard();
  }

  public bool IsHeroPowerAffectedByBonusDamage()
  {
    Card heroPowerCard = this.GetHeroPowerCard();
    if ((UnityEngine.Object) heroPowerCard == (UnityEngine.Object) null)
      return false;
    Entity entity = heroPowerCard.GetEntity();
    if (!entity.IsHeroPower())
      return false;
    return TextUtils.HasBonusDamage(entity.GetCardTextInHand());
  }

  public override Card GetWeaponCard()
  {
    return ZoneMgr.Get().FindZoneOfType<ZoneWeapon>(this.GetSide()).GetFirstCard();
  }

  public ZoneHand GetHandZone()
  {
    return ZoneMgr.Get().FindZoneOfType<ZoneHand>(this.GetSide());
  }

  public ZonePlay GetBattlefieldZone()
  {
    return ZoneMgr.Get().FindZoneOfType<ZonePlay>(this.GetSide());
  }

  public ZoneDeck GetDeckZone()
  {
    return ZoneMgr.Get().FindZoneOfType<ZoneDeck>(this.GetSide());
  }

  public ZoneGraveyard GetGraveyardZone()
  {
    return ZoneMgr.Get().FindZoneOfType<ZoneGraveyard>(this.GetSide());
  }

  public ZoneSecret GetSecretZone()
  {
    return ZoneMgr.Get().FindZoneOfType<ZoneSecret>(this.GetSide());
  }

  public int GetNumDragonsInHand()
  {
    int num = 0;
    using (List<Card>.Enumerator enumerator = this.GetHandZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.GetEntity().GetRace() == TAG_RACE.DRAGON)
          ++num;
      }
    }
    return num;
  }

  public bool HasReadyAttackers()
  {
    List<Card> cards = this.GetBattlefieldZone().GetCards();
    for (int index = 0; index < cards.Count; ++index)
    {
      if (GameState.Get().HasResponse(cards[index].GetEntity()))
        return true;
    }
    return false;
  }

  public bool HasATauntMinion()
  {
    List<Card> cards = this.GetBattlefieldZone().GetCards();
    for (int index = 0; index < cards.Count; ++index)
    {
      if (cards[index].GetEntity().HasTaunt())
        return true;
    }
    return false;
  }

  public uint GetArenaWins()
  {
    return this.m_arenaWins;
  }

  public uint GetArenaLosses()
  {
    return this.m_arenaLoss;
  }

  public uint GetTavernBrawlWins()
  {
    return this.m_tavernBrawlWins;
  }

  public uint GetTavernBrawlLosses()
  {
    return this.m_tavernBrawlLoss;
  }

  public void PlayConcedeEmote()
  {
    if (this.m_concedeEmotePlayed)
      return;
    Card heroCard = this.GetHeroCard();
    if ((UnityEngine.Object) heroCard == (UnityEngine.Object) null)
      return;
    heroCard.PlayEmote(EmoteType.CONCEDE);
    this.m_concedeEmotePlayed = true;
  }

  public BnetGameAccountId GetGameAccountId()
  {
    return this.m_gameAccountId;
  }

  public BnetPlayer GetBnetPlayer()
  {
    return BnetPresenceMgr.Get().GetPlayer(this.m_gameAccountId);
  }

  public bool IsDisplayable()
  {
    if ((BnetEntityId) this.m_gameAccountId == (BnetEntityId) null)
      return false;
    if (!this.IsBnetPlayer())
      return !this.ShouldUseHeroName() || this.GetHeroEntityDef() != null;
    BnetPlayer player = BnetPresenceMgr.Get().GetPlayer(this.m_gameAccountId);
    if (player == null || !player.IsDisplayable())
      return false;
    if (GameUtils.ShouldShowRankedMedals())
    {
      BnetGameAccount hearthstoneGameAccount = player.GetHearthstoneGameAccount();
      if (hearthstoneGameAccount == (BnetGameAccount) null || !hearthstoneGameAccount.HasGameField(18U))
        return false;
    }
    return true;
  }

  public void WipeZzzs()
  {
    using (List<Card>.Enumerator enumerator = this.GetBattlefieldZone().GetCards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Spell actorSpell = enumerator.Current.GetActorSpell(SpellType.Zzz, true);
        if (!((UnityEngine.Object) actorSpell == (UnityEngine.Object) null))
          actorSpell.ActivateState(SpellStateType.DEATH);
      }
    }
  }

  public TAG_PLAYSTATE GetPreGameOverPlayState()
  {
    return this.m_preGameOverPlayState;
  }

  public bool HasSeenMalchezaarSpell()
  {
    return this.m_hasSeenMalchezaarSpell;
  }

  public void SetHasSeenMalchezaarSpell(bool hasSeen)
  {
    this.m_hasSeenMalchezaarSpell = hasSeen;
  }

  public void AddManaCrystal(int numCrystals, bool isTurnStart)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().AddManaCrystals(numCrystals, isTurnStart);
  }

  public void AddManaCrystal(int numCrystals)
  {
    this.AddManaCrystal(numCrystals, false);
  }

  public void DestroyManaCrystal(int numCrystals)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().DestroyManaCrystals(numCrystals);
  }

  public void AddTempManaCrystal(int numCrystals)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().AddTempManaCrystals(numCrystals);
  }

  public void DestroyTempManaCrystal(int numCrystals)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().DestroyTempManaCrystals(numCrystals);
  }

  public void SpendManaCrystal(int numCrystals)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().SpendManaCrystals(numCrystals);
  }

  public void ReadyManaCrystal(int numCrystals)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().ReadyManaCrystals(numCrystals);
  }

  public void HandleSameTurnOverloadChanged(int crystalsChanged)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().HandleSameTurnOverloadChanged(crystalsChanged);
  }

  public void UnlockCrystals(int numCrystals)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().UnlockCrystals(numCrystals);
  }

  public void CancelAllProposedMana(Entity entity)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().CancelAllProposedMana(entity);
  }

  public void ProposeManaCrystalUsage(Entity entity)
  {
    if (!this.IsFriendlySide())
      return;
    ManaCrystalMgr.Get().ProposeManaCrystalUsage(entity);
  }

  public void UpdateManaCounter()
  {
    if ((UnityEngine.Object) this.m_manaCounter == (UnityEngine.Object) null)
      return;
    this.m_manaCounter.UpdateText();
  }

  public void NotifyOfSpentMana(int spentMana)
  {
    this.m_queuedSpentMana += spentMana;
  }

  public void NotifyOfUsedTempMana(int usedMana)
  {
    this.m_usedTempMana += usedMana;
  }

  public int GetQueuedUsedTempMana()
  {
    return this.m_usedTempMana;
  }

  public int GetQueuedSpentMana()
  {
    return this.m_queuedSpentMana;
  }

  public void SetRealTimeTempMana(int tempMana)
  {
    this.m_realtimeTempMana = tempMana;
  }

  public int GetRealTimeTempMana()
  {
    return this.m_realtimeTempMana;
  }

  public void OnBoardLoaded()
  {
    this.AssignPlayerBoardObjects();
  }

  public override void OnRealTimeTagChanged(Network.HistTagChange change)
  {
    switch ((GAME_TAG) change.Tag)
    {
      case GAME_TAG.PLAYSTATE:
        TAG_PLAYSTATE playState = (TAG_PLAYSTATE) change.Value;
        if (!GameUtils.IsPreGameOverPlayState(playState))
          break;
        this.m_preGameOverPlayState = playState;
        break;
      case GAME_TAG.COMBO_ACTIVE:
        this.SetRealTimeComboActive(change.Value);
        break;
      case GAME_TAG.TEMP_RESOURCES:
        this.SetRealTimeTempMana(change.Value);
        break;
    }
  }

  public override void OnTagsChanged(TagDeltaList changeList)
  {
    for (int index = 0; index < changeList.Count; ++index)
      this.OnTagChanged(changeList[index]);
  }

  public override void OnTagChanged(TagDelta change)
  {
    if (this.IsFriendlySide())
      this.OnFriendlyPlayerTagChanged(change);
    else
      this.OnOpposingPlayerTagChanged(change);
    GAME_TAG tag = (GAME_TAG) change.tag;
    switch (tag)
    {
      case GAME_TAG.LOCK_AND_LOAD:
        this.ToggleActorSpellOnCard(this.GetHeroCard(), change, SpellType.LOCK_AND_LOAD);
        break;
      case GAME_TAG.SHADOWFORM:
        this.ToggleActorSpellOnCard(this.GetHeroCard(), change, SpellType.SHADOWFORM);
        break;
      case GAME_TAG.CHOOSE_BOTH:
        this.UpdateChooseBoth();
        break;
      default:
        if (tag != GAME_TAG.RESOURCES_USED && tag != GAME_TAG.RESOURCES)
        {
          if (tag != GAME_TAG.PLAYSTATE)
          {
            if (tag != GAME_TAG.COMBO_ACTIVE)
            {
              if (tag != GAME_TAG.TEMP_RESOURCES)
              {
                if (tag != GAME_TAG.MULLIGAN_STATE)
                {
                  if (tag != GAME_TAG.STEADY_SHOT_CAN_TARGET)
                  {
                    if (tag != GAME_TAG.CURRENT_HEROPOWER_DAMAGE_BONUS)
                    {
                      if (tag != GAME_TAG.SPELLS_COST_HEALTH)
                      {
                        if (tag != GAME_TAG.EMBRACE_THE_SHADOW)
                          break;
                        this.ToggleActorSpellOnCard(this.GetHeroCard(), change, SpellType.EMBRACE_THE_SHADOW);
                        break;
                      }
                      this.UpdateSpellsCostHealth(change);
                      break;
                    }
                    if (!this.IsHeroPowerAffectedByBonusDamage())
                      break;
                    this.ToggleActorSpellOnCard(this.GetHeroPowerCard(), change, SpellType.CURRENT_HEROPOWER_DAMAGE_BONUS);
                    break;
                  }
                  this.ToggleActorSpellOnCard(this.GetHeroPowerCard(), change, SpellType.STEADY_SHOT_CAN_TARGET);
                  break;
                }
                if (change.newValue != 4 || !((UnityEngine.Object) MulliganManager.Get() != (UnityEngine.Object) null))
                  break;
                MulliganManager.Get().ServerHasDealtReplacementCards(this.IsFriendlySide());
                break;
              }
            }
            else
            {
              using (List<Card>.Enumerator enumerator = this.GetHandZone().GetCards().GetEnumerator())
              {
                while (enumerator.MoveNext())
                  enumerator.Current.UpdateActorState();
                break;
              }
            }
          }
          else
          {
            if (change.newValue != 8)
              break;
            this.PlayConcedeEmote();
            break;
          }
        }
        if (GameState.Get().IsTurnStartManagerActive() && this.IsFriendlySide())
          break;
        this.UpdateManaCounter();
        break;
    }
  }

  private void OnFriendlyPlayerTagChanged(TagDelta change)
  {
    GAME_TAG tag = (GAME_TAG) change.tag;
    switch (tag)
    {
      case GAME_TAG.CURRENT_SPELLPOWER:
label_39:
        this.UpdateHandCardPowersText(true);
        break;
      case GAME_TAG.TEMP_RESOURCES:
        int num1 = change.oldValue - this.m_usedTempMana;
        int num2 = change.newValue - change.oldValue;
        if (num2 < 0)
          this.m_usedTempMana += num2;
        if (this.m_usedTempMana < 0)
          this.m_usedTempMana = 0;
        if (num1 < 0)
          num1 = 0;
        int numCrystals = change.newValue - num1 - this.m_usedTempMana;
        if (numCrystals > 0)
        {
          this.AddTempManaCrystal(numCrystals);
          break;
        }
        this.DestroyTempManaCrystal(-numCrystals);
        break;
      case GAME_TAG.OVERLOAD_OWED:
        this.HandleSameTurnOverloadChanged(change.newValue - change.oldValue);
        break;
      default:
        switch (tag - 23)
        {
          case (GAME_TAG) 0:
            if (change.newValue != 1)
              return;
            ManaCrystalMgr.Get().OnCurrentPlayerChanged();
            this.m_queuedSpentMana = 0;
            if (!GameState.Get().IsMainPhase())
              return;
            TurnStartManager.Get().BeginListeningForTurnEvents();
            return;
          case GAME_TAG.TAG_SCRIPT_DATA_NUM_1:
            int num3 = change.oldValue + this.m_queuedSpentMana;
            int num4 = change.newValue - change.oldValue;
            if (num4 > 0)
              this.m_queuedSpentMana -= num4;
            if (this.m_queuedSpentMana < 0)
              this.m_queuedSpentMana = 0;
            ManaCrystalMgr.Get().UpdateSpentMana(change.newValue - num3 + this.m_queuedSpentMana);
            return;
          case GAME_TAG.TAG_SCRIPT_DATA_NUM_2:
            if (change.newValue > change.oldValue)
            {
              if (GameState.Get().IsTurnStartManagerActive() && this.IsFriendlySide())
              {
                TurnStartManager.Get().NotifyOfManaCrystalGained(change.newValue - change.oldValue);
                return;
              }
              this.AddManaCrystal(change.newValue - change.oldValue);
              return;
            }
            this.DestroyManaCrystal(change.oldValue - change.newValue);
            return;
          default:
            if (tag != GAME_TAG.SPELLPOWER_DOUBLE && tag != GAME_TAG.HEALING_DOUBLE)
            {
              if (tag != GAME_TAG.MULLIGAN_STATE)
              {
                if (tag != GAME_TAG.OVERLOAD_LOCKED)
                {
                  if (tag != GAME_TAG.JADE_GOLEM)
                  {
                    if (tag != GAME_TAG.RED_MANA_CRYSTALS)
                      return;
                    ManaCrystalMgr.Get().TurnCrystalsRed(change.oldValue, change.newValue);
                    return;
                  }
                  this.UpdateHandCardPowersText(false);
                  return;
                }
                if (change.newValue >= change.oldValue || GameState.Get().IsTurnStartManagerActive())
                  return;
                this.UnlockCrystals(change.oldValue - change.newValue);
                return;
              }
              if (change.newValue != 4 || !((UnityEngine.Object) MulliganManager.Get() == (UnityEngine.Object) null))
                return;
              using (List<Card>.Enumerator enumerator = this.GetHandZone().GetCards().GetEnumerator())
              {
                while (enumerator.MoveNext())
                  enumerator.Current.GetActor().TurnOnCollider();
                return;
              }
            }
            else
              goto label_39;
        }
    }
  }

  private void OnOpposingPlayerTagChanged(TagDelta change)
  {
    switch ((GAME_TAG) change.tag)
    {
      case GAME_TAG.PLAYSTATE:
        if (change.newValue != 7)
          break;
        NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_ANNOUNCER_DISCONNECT_45"), "VO_ANNOUNCER_DISCONNECT_45", 0.0f, (System.Action) null, false);
        break;
      case GAME_TAG.RESOURCES:
        if (change.newValue <= change.oldValue)
          break;
        GameState.Get().GetGameEntity().NotifyOfEnemyManaCrystalSpawned();
        break;
    }
  }

  private void UpdateName()
  {
    if (this.ShouldUseHeroName())
      this.UpdateNameWithHeroName();
    else if (this.IsAI())
      this.m_name = GameStrings.Get("GAMEPLAY_AI_OPPONENT_NAME");
    else if (this.IsBnetPlayer())
    {
      this.m_name = BnetPresenceMgr.Get().GetPlayer(this.m_gameAccountId).GetBestName();
      if (string.IsNullOrEmpty(this.m_name))
        return;
      GameMgr.Get().SetLastDisplayedPlayerName(this.GetPlayerId(), this.m_name);
    }
    else
      Debug.LogError((object) string.Format("Player.UpdateName() - unable to determine player name"));
  }

  private bool ShouldUseHeroName()
  {
    return !this.IsBnetPlayer() && (!this.IsAI() || !GameMgr.Get().IsPractice());
  }

  private void UpdateNameWithHeroName()
  {
    if (this.m_hero == null)
      return;
    EntityDef entityDef = this.m_hero.GetEntityDef();
    if (entityDef == null)
      return;
    this.m_name = entityDef.GetName();
  }

  private bool ShouldUseBogusRank()
  {
    return !this.IsBnetPlayer();
  }

  private void UpdateRank()
  {
    MedalInfoTranslator medalInfoTranslator = (MedalInfoTranslator) null;
    if (this.ShouldUseBogusRank())
      medalInfoTranslator = new MedalInfoTranslator();
    else if ((BnetEntityId) this.m_gameAccountId == (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId())
    {
      NetCache.NetCacheMedalInfo netObject = NetCache.Get().GetNetObject<NetCache.NetCacheMedalInfo>();
      if (netObject != null)
        medalInfoTranslator = new MedalInfoTranslator(netObject);
    }
    if (medalInfoTranslator == null)
      medalInfoTranslator = RankMgr.Get().GetRankPresenceField(BnetPresenceMgr.Get().GetPlayer(this.m_gameAccountId));
    this.m_medalInfo = medalInfoTranslator;
  }

  private void UpdateDisplayInfo()
  {
    this.UpdateName();
    this.UpdateRank();
    this.UpdateSessionRecord();
    if (!this.IsBnetPlayer() || this.IsLocalUser())
      return;
    BnetPlayer player = BnetPresenceMgr.Get().GetPlayer(this.m_gameAccountId);
    if (!BnetFriendMgr.Get().IsFriend(player))
      return;
    ChatMgr.Get().AddRecentWhisperPlayerToBottom(player);
  }

  private void UpdateSessionRecord()
  {
    BnetPlayer player = BnetPresenceMgr.Get().GetPlayer(this.m_gameAccountId);
    if (player == null)
      return;
    BnetGameAccount hearthstoneGameAccount = player.GetHearthstoneGameAccount();
    if (hearthstoneGameAccount == (BnetGameAccount) null)
      return;
    SessionRecord sessionRecord = hearthstoneGameAccount.GetSessionRecord();
    if (sessionRecord == null)
      return;
    if (sessionRecord.SessionRecordType == SessionRecordType.ARENA)
    {
      this.m_arenaWins = sessionRecord.Wins;
      this.m_arenaLoss = sessionRecord.Losses;
    }
    else
    {
      if (sessionRecord.SessionRecordType != SessionRecordType.TAVERN_BRAWL)
        return;
      this.m_tavernBrawlWins = sessionRecord.Wins;
      this.m_tavernBrawlLoss = sessionRecord.Losses;
    }
  }

  private void OnBnetPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    if (changelist.FindChange(this.m_gameAccountId) == null || !this.IsDisplayable())
      return;
    BnetPresenceMgr.Get().RemovePlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnBnetPlayersChanged));
    this.UpdateDisplayInfo();
  }

  private void UpdateLocal()
  {
    if (this.IsBnetPlayer())
      this.m_local = (BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId() == (BnetEntityId) this.m_gameAccountId;
    else
      this.m_local = (long) this.m_gameAccountId.GetLo() == 1L;
  }

  private void UpdateSide()
  {
    if (GameMgr.Get().IsSpectator())
    {
      if ((BnetEntityId) this.m_gameAccountId == (BnetEntityId) SpectatorManager.Get().GetSpectateeFriendlySide())
        this.m_side = Player.Side.FRIENDLY;
      else
        this.m_side = Player.Side.OPPOSING;
    }
    else if (this.IsLocalUser())
      this.m_side = Player.Side.FRIENDLY;
    else
      this.m_side = Player.Side.OPPOSING;
  }

  private void AssignPlayerBoardObjects()
  {
    foreach (ManaCounter componentsInChild in BoardStandardGame.Get().gameObject.GetComponentsInChildren<ManaCounter>(true))
    {
      if (componentsInChild.m_Side == this.m_side)
      {
        this.m_manaCounter = componentsInChild;
        this.m_manaCounter.SetPlayer(this);
        this.m_manaCounter.UpdateText();
        break;
      }
    }
    this.InitManaCrystalMgr();
    using (List<Zone>.Enumerator enumerator = ZoneMgr.Get().GetZones().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Zone current = enumerator.Current;
        if (current.m_Side == this.m_side)
          current.SetController(this);
      }
    }
  }

  private void InitManaCrystalMgr()
  {
    if (!this.IsFriendlySide())
      return;
    int tag1 = this.GetTag(GAME_TAG.TEMP_RESOURCES);
    int tag2 = this.GetTag(GAME_TAG.RESOURCES);
    int tag3 = this.GetTag(GAME_TAG.RESOURCES_USED);
    int tag4 = this.GetTag(GAME_TAG.OVERLOAD_OWED);
    ManaCrystalMgr.Get().AddManaCrystals(tag2, false);
    ManaCrystalMgr.Get().AddTempManaCrystals(tag1);
    ManaCrystalMgr.Get().UpdateSpentMana(tag3);
    ManaCrystalMgr.Get().MarkCrystalsOwedForOverload(tag4);
  }

  private void OnTurnChanged(int oldTurn, int newTurn, object userData)
  {
    this.WipeZzzs();
  }

  private void OnFriendlyOptionsReceived(object userData)
  {
    this.UpdateChooseBoth();
  }

  private void OnFriendlyOptionsSent(Network.Options.Option option, object userData)
  {
    this.UpdateChooseBoth();
    this.CancelAllProposedMana(GameState.Get().GetEntity(option.Main.ID));
  }

  private void OnFriendlyTurnStarted(object userData)
  {
    this.UpdateChooseBoth();
  }

  private void ToggleActorSpellOnCard(Card card, TagDelta change, SpellType spellType)
  {
    if ((UnityEngine.Object) card == (UnityEngine.Object) null || !card.CanShowActorVisuals())
      return;
    Actor actor = card.GetActor();
    if (change.newValue > 0)
      actor.ActivateSpellBirthState(spellType);
    else
      actor.ActivateSpellDeathState(spellType);
  }

  private void UpdateHandCardPowersText(bool onlySpells)
  {
    List<Card> cards = this.GetHandZone().GetCards();
    for (int index = 0; index < cards.Count; ++index)
    {
      Card card = cards[index];
      if (!onlySpells || card.GetEntity().IsSpell())
        card.GetActor().UpdatePowersText();
    }
  }

  private void UpdateSpellsCostHealth(TagDelta change)
  {
    if (this.IsFriendlySide())
    {
      Card mousedOverCard = InputManager.Get().GetMousedOverCard();
      if ((UnityEngine.Object) mousedOverCard != (UnityEngine.Object) null)
      {
        Entity entity = mousedOverCard.GetEntity();
        if (entity.IsSpell())
        {
          if (change.newValue > 0)
            ManaCrystalMgr.Get().CancelAllProposedMana(entity);
          else
            ManaCrystalMgr.Get().ProposeManaCrystalUsage(entity);
        }
      }
    }
    List<Card> cards = this.GetHandZone().GetCards();
    for (int index = 0; index < cards.Count; ++index)
    {
      Card card = cards[index];
      if (card.CanShowActorVisuals() && card.GetEntity().IsSpell())
      {
        Actor actor = card.GetActor();
        if (change.newValue > 0)
          actor.ActivateSpellBirthState(SpellType.SPELLS_COST_HEALTH);
        else
          actor.ActivateSpellDeathState(SpellType.SPELLS_COST_HEALTH);
      }
    }
  }

  private void UpdateChooseBoth()
  {
    List<Card> cards = this.GetHandZone().GetCards();
    for (int index = 0; index < cards.Count; ++index)
    {
      Card card = cards[index];
      if (card.CanShowActorVisuals())
      {
        Entity entity = card.GetEntity();
        if (entity.HasTag(GAME_TAG.CHOOSE_ONE))
        {
          Actor actor = card.GetActor();
          SpellType spellType = SpellType.CHOOSE_BOTH;
          if (this.HasTag(GAME_TAG.CHOOSE_BOTH) && GameState.Get().IsOption(entity))
            SpellUtils.ActivateBirthIfNecessary(actor.GetSpell(spellType));
          else
            SpellUtils.ActivateDeathIfNecessary(actor.GetSpellIfLoaded(spellType));
        }
      }
    }
  }

  public PlayErrors.PlayerInfo ConvertToPlayerInfo()
  {
    return new PlayErrors.PlayerInfo() { id = this.GetPlayerId(), numResources = this.GetNumAvailableResources(), numPermanentResources = this.GetTag(GAME_TAG.RESOURCES), numMaxPermanentResources = this.GetTag(GAME_TAG.MAXRESOURCES), numTouchableFriendlyMinionsInPlay = GameState.Get().GetNumFriendlyMinionsInPlay(false), numTouchableEnemyMinionsInPlay = GameState.Get().GetNumEnemyMinionsInPlay(false), numTotalFriendlyMinionsInPlay = GameState.Get().GetNumFriendlyMinionsInPlay(true), numMinionSlotsPerPlayer = GameState.Get().GetMaxFriendlyMinionsPerPlayer(), numSecretSlotsPerPlayer = GameState.Get().GetMaxSecretsPerPlayer(), numDragonsInHand = this.GetNumDragonsInHand(), numFriendlyMinionsThatDiedThisTurn = this.GetTag(GAME_TAG.NUM_FRIENDLY_MINIONS_THAT_DIED_THIS_TURN), numFriendlyMinionsThatDiedThisGame = this.GetTag(GAME_TAG.NUM_FRIENDLY_MINIONS_THAT_DIED_THIS_GAME), currentDefense = this.GetHero().GetCurrentDefense(), numFriendlySecretsInPlay = this.GetSecretDefinitions().Count, isCurrentPlayer = this.IsCurrentPlayer(), weaponEquipped = this.HasWeapon(), enemyWeaponEquipped = GameState.Get().GetOpposingSidePlayer().HasWeapon(), comboActive = this.IsComboActive(), steadyShotRequiresTarget = this.HasTag(GAME_TAG.STEADY_SHOT_CAN_TARGET), spellsCostHealth = this.HasTag(GAME_TAG.SPELLS_COST_HEALTH) };
  }

  public enum Side
  {
    NEUTRAL,
    FRIENDLY,
    OPPOSING,
  }
}
