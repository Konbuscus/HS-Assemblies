﻿// Decompiled with JetBrains decompiler
// Type: NefarianTB
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class NefarianTB : Spell
{
  protected override void OnAction(SpellStateType prevStateType)
  {
    this.BlockZoneLayout();
    base.OnAction(prevStateType);
  }

  private void BlockZoneLayout()
  {
    Card sourceCard = this.GetSourceCard();
    if ((Object) sourceCard == (Object) null)
      return;
    Player controller = sourceCard.GetController();
    if (controller == null)
      return;
    ZonePlay battlefieldZone = controller.GetBattlefieldZone();
    if ((Object) battlefieldZone == (Object) null)
      return;
    battlefieldZone.AddLayoutBlocker();
  }
}
