﻿// Decompiled with JetBrains decompiler
// Type: CardDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CardDbfRecord : DbfRecord
{
  [SerializeField]
  private string m_NoteMiniGuid;
  [SerializeField]
  private string m_LongGuid;
  [SerializeField]
  private DbfLocValue m_TextInHand;
  [SerializeField]
  private string m_CraftingEvent;
  [SerializeField]
  private int m_SuggestionWeight;
  [SerializeField]
  private bool m_ChangedManaCost;
  [SerializeField]
  private bool m_ChangedHealth;
  [SerializeField]
  private bool m_ChangedAttack;
  [SerializeField]
  private bool m_ChangedCardTextInHand;
  [SerializeField]
  private int m_ChangeVersion;
  [SerializeField]
  private DbfLocValue m_Name;
  [SerializeField]
  private DbfLocValue m_FlavorText;
  [SerializeField]
  private DbfLocValue m_HowToGetCard;
  [SerializeField]
  private DbfLocValue m_HowToGetGoldCard;
  [SerializeField]
  private DbfLocValue m_TargetArrowText;
  [SerializeField]
  private string m_ArtistName;

  [DbfField("NOTE_MINI_GUID", "client-side card name")]
  public string NoteMiniGuid
  {
    get
    {
      return this.m_NoteMiniGuid;
    }
  }

  [DbfField("LONG_GUID", "game server file name")]
  public string LongGuid
  {
    get
    {
      return this.m_LongGuid;
    }
  }

  [DbfField("TEXT_IN_HAND", "FK to LOC_STR.ID for the user-facing card text in hand")]
  public DbfLocValue TextInHand
  {
    get
    {
      return this.m_TextInHand;
    }
  }

  [DbfField("CRAFTING_EVENT", "If value is not 'always', this event controls when this card can be crafted or disenchanted (assuming the card has an entry in CARD_VALUE).")]
  public string CraftingEvent
  {
    get
    {
      return this.m_CraftingEvent;
    }
  }

  [DbfField("SUGGESTION_WEIGHT", "Collection manager card suggestion weight. Typically ranges from 1-10.  Higher is better.")]
  public int SuggestionWeight
  {
    get
    {
      return this.m_SuggestionWeight;
    }
  }

  [DbfField("CHANGED_MANA_COST", "Should an indication be shown to the player that the mana cost of this card has changed?")]
  public bool ChangedManaCost
  {
    get
    {
      return this.m_ChangedManaCost;
    }
  }

  [DbfField("CHANGED_HEALTH", "Should an indication be shown to the player that the health of this card has changed?")]
  public bool ChangedHealth
  {
    get
    {
      return this.m_ChangedHealth;
    }
  }

  [DbfField("CHANGED_ATTACK", "Should an indication be shown to the player that the attack of this card has changed?")]
  public bool ChangedAttack
  {
    get
    {
      return this.m_ChangedAttack;
    }
  }

  [DbfField("CHANGED_CARD_TEXT_IN_HAND", "Should an indication be shown to the player that the card text has changed?")]
  public bool ChangedCardTextInHand
  {
    get
    {
      return this.m_ChangedCardTextInHand;
    }
  }

  [DbfField("CHANGE_VERSION", "Whenever any of the 'changed_' values are modified, this is incremented.")]
  public int ChangeVersion
  {
    get
    {
      return this.m_ChangeVersion;
    }
  }

  [DbfField("NAME", "FK to LOC_STR.ID for the user-facing name of this card")]
  public DbfLocValue Name
  {
    get
    {
      return this.m_Name;
    }
  }

  [DbfField("FLAVOR_TEXT", "FK to LOC_STR.ID for the user-facing flavor text")]
  public DbfLocValue FlavorText
  {
    get
    {
      return this.m_FlavorText;
    }
  }

  [DbfField("HOW_TO_GET_CARD", "FK to LOC_STR.ID for the user-facing text on how to get this card")]
  public DbfLocValue HowToGetCard
  {
    get
    {
      return this.m_HowToGetCard;
    }
  }

  [DbfField("HOW_TO_GET_GOLD_CARD", "FK to LOC_STR.ID for the user-facing text on how to get this gold card")]
  public DbfLocValue HowToGetGoldCard
  {
    get
    {
      return this.m_HowToGetGoldCard;
    }
  }

  [DbfField("TARGET_ARROW_TEXT", "FK to LOC_STR.ID for the user-facing target arrow text")]
  public DbfLocValue TargetArrowText
  {
    get
    {
      return this.m_TargetArrowText;
    }
  }

  [DbfField("ARTIST_NAME", "artist name")]
  public string ArtistName
  {
    get
    {
      return this.m_ArtistName;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    CardDbfAsset cardDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (CardDbfAsset)) as CardDbfAsset;
    if ((UnityEngine.Object) cardDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("CardDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < cardDbfAsset.Records.Count; ++index)
      cardDbfAsset.Records[index].StripUnusedLocales();
    records = (object) cardDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_TextInHand.StripUnusedLocales();
    this.m_Name.StripUnusedLocales();
    this.m_FlavorText.StripUnusedLocales();
    this.m_HowToGetCard.StripUnusedLocales();
    this.m_HowToGetGoldCard.StripUnusedLocales();
    this.m_TargetArrowText.StripUnusedLocales();
  }

  public void SetNoteMiniGuid(string v)
  {
    this.m_NoteMiniGuid = v;
  }

  public void SetLongGuid(string v)
  {
    this.m_LongGuid = v;
  }

  public void SetTextInHand(DbfLocValue v)
  {
    this.m_TextInHand = v;
    v.SetDebugInfo(this.ID, "TEXT_IN_HAND");
  }

  public void SetCraftingEvent(string v)
  {
    this.m_CraftingEvent = v;
  }

  public void SetSuggestionWeight(int v)
  {
    this.m_SuggestionWeight = v;
  }

  public void SetChangedManaCost(bool v)
  {
    this.m_ChangedManaCost = v;
  }

  public void SetChangedHealth(bool v)
  {
    this.m_ChangedHealth = v;
  }

  public void SetChangedAttack(bool v)
  {
    this.m_ChangedAttack = v;
  }

  public void SetChangedCardTextInHand(bool v)
  {
    this.m_ChangedCardTextInHand = v;
  }

  public void SetChangeVersion(int v)
  {
    this.m_ChangeVersion = v;
  }

  public void SetName(DbfLocValue v)
  {
    this.m_Name = v;
    v.SetDebugInfo(this.ID, "NAME");
  }

  public void SetFlavorText(DbfLocValue v)
  {
    this.m_FlavorText = v;
    v.SetDebugInfo(this.ID, "FLAVOR_TEXT");
  }

  public void SetHowToGetCard(DbfLocValue v)
  {
    this.m_HowToGetCard = v;
    v.SetDebugInfo(this.ID, "HOW_TO_GET_CARD");
  }

  public void SetHowToGetGoldCard(DbfLocValue v)
  {
    this.m_HowToGetGoldCard = v;
    v.SetDebugInfo(this.ID, "HOW_TO_GET_GOLD_CARD");
  }

  public void SetTargetArrowText(DbfLocValue v)
  {
    this.m_TargetArrowText = v;
    v.SetDebugInfo(this.ID, "TARGET_ARROW_TEXT");
  }

  public void SetArtistName(string v)
  {
    this.m_ArtistName = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CardDbfRecord.\u003C\u003Ef__switch\u0024map20 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CardDbfRecord.\u003C\u003Ef__switch\u0024map20 = new Dictionary<string, int>(17)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_MINI_GUID",
            1
          },
          {
            "LONG_GUID",
            2
          },
          {
            "TEXT_IN_HAND",
            3
          },
          {
            "CRAFTING_EVENT",
            4
          },
          {
            "SUGGESTION_WEIGHT",
            5
          },
          {
            "CHANGED_MANA_COST",
            6
          },
          {
            "CHANGED_HEALTH",
            7
          },
          {
            "CHANGED_ATTACK",
            8
          },
          {
            "CHANGED_CARD_TEXT_IN_HAND",
            9
          },
          {
            "CHANGE_VERSION",
            10
          },
          {
            "NAME",
            11
          },
          {
            "FLAVOR_TEXT",
            12
          },
          {
            "HOW_TO_GET_CARD",
            13
          },
          {
            "HOW_TO_GET_GOLD_CARD",
            14
          },
          {
            "TARGET_ARROW_TEXT",
            15
          },
          {
            "ARTIST_NAME",
            16
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CardDbfRecord.\u003C\u003Ef__switch\u0024map20.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NoteMiniGuid;
          case 2:
            return (object) this.LongGuid;
          case 3:
            return (object) this.TextInHand;
          case 4:
            return (object) this.CraftingEvent;
          case 5:
            return (object) this.SuggestionWeight;
          case 6:
            return (object) this.ChangedManaCost;
          case 7:
            return (object) this.ChangedHealth;
          case 8:
            return (object) this.ChangedAttack;
          case 9:
            return (object) this.ChangedCardTextInHand;
          case 10:
            return (object) this.ChangeVersion;
          case 11:
            return (object) this.Name;
          case 12:
            return (object) this.FlavorText;
          case 13:
            return (object) this.HowToGetCard;
          case 14:
            return (object) this.HowToGetGoldCard;
          case 15:
            return (object) this.TargetArrowText;
          case 16:
            return (object) this.ArtistName;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (CardDbfRecord.\u003C\u003Ef__switch\u0024map21 == null)
    {
      // ISSUE: reference to a compiler-generated field
      CardDbfRecord.\u003C\u003Ef__switch\u0024map21 = new Dictionary<string, int>(17)
      {
        {
          "ID",
          0
        },
        {
          "NOTE_MINI_GUID",
          1
        },
        {
          "LONG_GUID",
          2
        },
        {
          "TEXT_IN_HAND",
          3
        },
        {
          "CRAFTING_EVENT",
          4
        },
        {
          "SUGGESTION_WEIGHT",
          5
        },
        {
          "CHANGED_MANA_COST",
          6
        },
        {
          "CHANGED_HEALTH",
          7
        },
        {
          "CHANGED_ATTACK",
          8
        },
        {
          "CHANGED_CARD_TEXT_IN_HAND",
          9
        },
        {
          "CHANGE_VERSION",
          10
        },
        {
          "NAME",
          11
        },
        {
          "FLAVOR_TEXT",
          12
        },
        {
          "HOW_TO_GET_CARD",
          13
        },
        {
          "HOW_TO_GET_GOLD_CARD",
          14
        },
        {
          "TARGET_ARROW_TEXT",
          15
        },
        {
          "ARTIST_NAME",
          16
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!CardDbfRecord.\u003C\u003Ef__switch\u0024map21.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNoteMiniGuid((string) val);
        break;
      case 2:
        this.SetLongGuid((string) val);
        break;
      case 3:
        this.SetTextInHand((DbfLocValue) val);
        break;
      case 4:
        this.SetCraftingEvent((string) val);
        break;
      case 5:
        this.SetSuggestionWeight((int) val);
        break;
      case 6:
        this.SetChangedManaCost((bool) val);
        break;
      case 7:
        this.SetChangedHealth((bool) val);
        break;
      case 8:
        this.SetChangedAttack((bool) val);
        break;
      case 9:
        this.SetChangedCardTextInHand((bool) val);
        break;
      case 10:
        this.SetChangeVersion((int) val);
        break;
      case 11:
        this.SetName((DbfLocValue) val);
        break;
      case 12:
        this.SetFlavorText((DbfLocValue) val);
        break;
      case 13:
        this.SetHowToGetCard((DbfLocValue) val);
        break;
      case 14:
        this.SetHowToGetGoldCard((DbfLocValue) val);
        break;
      case 15:
        this.SetTargetArrowText((DbfLocValue) val);
        break;
      case 16:
        this.SetArtistName((string) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CardDbfRecord.\u003C\u003Ef__switch\u0024map22 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CardDbfRecord.\u003C\u003Ef__switch\u0024map22 = new Dictionary<string, int>(17)
        {
          {
            "ID",
            0
          },
          {
            "NOTE_MINI_GUID",
            1
          },
          {
            "LONG_GUID",
            2
          },
          {
            "TEXT_IN_HAND",
            3
          },
          {
            "CRAFTING_EVENT",
            4
          },
          {
            "SUGGESTION_WEIGHT",
            5
          },
          {
            "CHANGED_MANA_COST",
            6
          },
          {
            "CHANGED_HEALTH",
            7
          },
          {
            "CHANGED_ATTACK",
            8
          },
          {
            "CHANGED_CARD_TEXT_IN_HAND",
            9
          },
          {
            "CHANGE_VERSION",
            10
          },
          {
            "NAME",
            11
          },
          {
            "FLAVOR_TEXT",
            12
          },
          {
            "HOW_TO_GET_CARD",
            13
          },
          {
            "HOW_TO_GET_GOLD_CARD",
            14
          },
          {
            "TARGET_ARROW_TEXT",
            15
          },
          {
            "ARTIST_NAME",
            16
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CardDbfRecord.\u003C\u003Ef__switch\u0024map22.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (string);
          case 2:
            return typeof (string);
          case 3:
            return typeof (DbfLocValue);
          case 4:
            return typeof (string);
          case 5:
            return typeof (int);
          case 6:
            return typeof (bool);
          case 7:
            return typeof (bool);
          case 8:
            return typeof (bool);
          case 9:
            return typeof (bool);
          case 10:
            return typeof (int);
          case 11:
            return typeof (DbfLocValue);
          case 12:
            return typeof (DbfLocValue);
          case 13:
            return typeof (DbfLocValue);
          case 14:
            return typeof (DbfLocValue);
          case 15:
            return typeof (DbfLocValue);
          case 16:
            return typeof (string);
        }
      }
    }
    return (System.Type) null;
  }
}
