﻿// Decompiled with JetBrains decompiler
// Type: Tooltip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Tooltip : MonoBehaviour
{
  public TextMesh headlineText;
  public TextMesh descriptionText;

  public void UpdateText(string headline, string description)
  {
    this.headlineText.text = headline;
    this.descriptionText.text = description;
  }
}
