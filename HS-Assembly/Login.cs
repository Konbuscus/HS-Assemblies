﻿// Decompiled with JetBrains decompiler
// Type: Login
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using PegasusShared;
using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class Login : Scene
{
  private bool m_waitingForBattleNet = true;
  private bool m_waitingForUpdateLoginComplete = true;
  private int m_nextMissionId;
  private TutorialProgress m_skipToTutorialProgress;
  private bool m_waitingForSetProgress;
  private ExistingAccountPopup m_existingAccountPopup;
  private static Login s_instance;

  protected override void Awake()
  {
    Login.s_instance = this;
    base.Awake();
  }

  private void Start()
  {
    Network network = Network.Get();
    network.RegisterNetHandler((object) SetProgressResponse.PacketID.ID, new Network.NetHandler(this.OnSetProgressResponse), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) AssetsVersionResponse.PacketID.ID, new Network.NetHandler(this.OnAssetsVersion), (Network.TimeoutHandler) null);
    network.RegisterNetHandler((object) UpdateLoginComplete.PacketID.ID, new Network.NetHandler(this.OnUpdateLoginComplete), (Network.TimeoutHandler) null);
    SceneMgr.Get().NotifySceneLoaded();
    Network.Get().OnLoginStarted();
  }

  private void OnDestroy()
  {
    if (!((UnityEngine.Object) Login.s_instance == (UnityEngine.Object) this))
      return;
    Login.s_instance = (Login) null;
  }

  private void Update()
  {
    Network.Get().ProcessNetwork();
    if (!this.m_waitingForBattleNet)
      return;
    Network.BnetLoginState bnetLoginState = Network.BattleNetStatus();
    if (bnetLoginState == Network.BnetLoginState.BATTLE_NET_LOGGED_IN && BattleNet.GetAccountCountry() != null && BattleNet.GetAccountRegion() != constants.BnetRegion.REGION_UNINITIALIZED || !Network.ShouldBeConnectedToAurora())
    {
      this.m_waitingForBattleNet = false;
      this.LoginOk();
    }
    else
    {
      if (bnetLoginState != Network.BnetLoginState.BATTLE_NET_LOGIN_FAILED && bnetLoginState != Network.BnetLoginState.BATTLE_NET_TIMEOUT)
        return;
      this.m_waitingForBattleNet = false;
      Network.Get().ShowConnectionFailureError("GLOBAL_ERROR_NETWORK_LOGIN_FAILURE");
    }
  }

  public static Login Get()
  {
    return Login.s_instance;
  }

  public static bool IsLoginSceneActive()
  {
    return (UnityEngine.Object) Login.s_instance != (UnityEngine.Object) null;
  }

  public bool isWaitingForBattleNet()
  {
    return this.m_waitingForBattleNet;
  }

  public override void Unload()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    Network network = Network.Get();
    network.RemoveNetHandler((object) SetProgressResponse.PacketID.ID, new Network.NetHandler(this.OnSetProgressResponse));
    network.RemoveNetHandler((object) AssetsVersionResponse.PacketID.ID, new Network.NetHandler(this.OnAssetsVersion));
    network.RemoveNetHandler((object) UpdateLoginComplete.PacketID.ID, new Network.NetHandler(this.OnUpdateLoginComplete));
    GameMgr.Get().UnregisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
  }

  private void AssetsVersionCheckCompleted()
  {
    if (!string.IsNullOrEmpty(UpdateManager.Get().GetError()) && UpdateManager.Get().UpdateIsRequired())
    {
      Error.AddFatalLoc("GLUE_PATCHING_ERROR");
    }
    else
    {
      if (Network.ShouldBeConnectedToAurora())
      {
        BnetPresenceMgr.Get().Initialize();
        BnetFriendMgr.Get().Initialize();
        BnetWhisperMgr.Get().Initialize();
        BnetNearbyPlayerMgr.Get().Initialize();
        FriendChallengeMgr.Get().OnLoggedIn();
        SpectatorManager.Get().InitializeConnectedToBnet();
        RAFManager.Get().Initialize();
        PopupDisplayManager.Get().Initialize();
        if (!Options.Get().GetBool(Option.CONNECT_TO_AURORA))
          Options.Get().SetBool(Option.CONNECT_TO_AURORA, true);
        TutorialProgress tutorialProgress = Options.Get().GetEnum<TutorialProgress>(Option.LOCAL_TUTORIAL_PROGRESS);
        if (tutorialProgress > TutorialProgress.NOTHING_COMPLETE)
        {
          this.m_waitingForSetProgress = true;
          Network.SetProgress((long) tutorialProgress);
        }
        if (WebAuth.GetIsNewCreatedAccount())
        {
          AdTrackingManager.Get().TrackAccountCreated();
          WebAuth.SetIsNewCreatedAccount(false);
        }
      }
      DefLoader.Get().Initialize();
      CollectionManager.Init();
      Box.Get().OnLoggedIn();
      BaseUI.Get().OnLoggedIn();
      InactivePlayerKicker.Get().OnLoggedIn();
      HealthyGamingMgr.Get().OnLoggedIn();
      if (Network.ShouldBeConnectedToAurora())
        RAFManager.Get().InitializeRequests();
      AccountLicenseMgr.Get().InitRequests();
      AdventureProgressMgr.InitRequests();
      Tournament.Init();
      GameMgr.Get().OnLoggedIn();
      if (Network.ShouldBeConnectedToAurora())
        StoreManager.Get().Init();
      DraftManager.Get().OnLoggedIn();
      Network.ResetConnectionFailureCount();
      if (Network.ShouldBeConnectedToAurora())
        Network.DoLoginUpdate();
      else
        this.m_waitingForUpdateLoginComplete = false;
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.LOGIN);
      if ((UnityEngine.Object) SplashScreen.Get() != (UnityEngine.Object) null)
      {
        SplashScreen.Get().StopPatching();
        SplashScreen.Get().ShowRatings();
      }
      this.PreloadActors();
      this.StartCoroutine(this.RegisterScreenWhenReady());
      SceneMgr.Get().LoadShaderPreCompiler();
    }
  }

  private void LoginOk()
  {
    if (Network.ShouldBeConnectedToAurora())
    {
      Network.LoginOk();
      Network.RequestAssetsVersion();
    }
    else
      this.AssetsVersionCheckCompleted();
  }

  private void PreloadActors()
  {
  }

  private void OnNetCacheReady()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    this.StartCoroutine(this.WaitForAchievesThenInit());
  }

  [DebuggerHidden]
  private IEnumerator WaitForAchievesThenInit()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Login.\u003CWaitForAchievesThenInit\u003Ec__IteratorF4() { \u003C\u003Ef__this = this };
  }

  private void OnCinematicFinished()
  {
    this.ReconnectOrChangeMode();
  }

  private void ReconnectOrChangeMode()
  {
    if (!Cheats.Get().IsLaunchingQuickGame() && ReconnectMgr.Get().ReconnectFromLogin())
      ReconnectMgr.Get().AddTimeoutListener(new ReconnectMgr.TimeoutCallback(this.OnReconnectTimeout));
    else
      this.ChangeMode();
  }

  private void ChangeMode()
  {
    this.m_nextMissionId = this.m_skipToTutorialProgress == TutorialProgress.NOTHING_COMPLETE ? GameUtils.GetNextTutorial() : GameUtils.GetNextTutorial(this.m_skipToTutorialProgress);
    MusicManager.Get().StartPlaylist(MusicPlaylistType.UI_MainTitle);
    if (GameUtils.ShouldShowSetRotationIntro() && this.m_nextMissionId == 0)
      this.ChangeMode_SetRotation();
    else if (this.m_nextMissionId == 0)
      this.ChangeMode_Hub();
    else
      this.ChangeMode_Tutorial();
  }

  private bool OnReconnectTimeout(object userData)
  {
    this.ChangeMode();
    return true;
  }

  private void ChangeMode_Hub()
  {
    if (Options.Get().GetBool(Option.HAS_SEEN_HUB, false))
      this.PlayInnkeeperIntroVO();
    if (GameUtils.ShouldDoBoxIntro())
    {
      Spell eventSpell = Box.Get().GetEventSpell(BoxEventType.STARTUP_HUB);
      eventSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnStartupHubSpellFinished));
      eventSpell.Activate();
    }
    else
    {
      this.DoSkippedBoxIntro();
      this.OnStartupHubSpellFinished((Spell) null, (object) null);
    }
  }

  private void PlayInnkeeperIntroVO()
  {
    if (ReturningPlayerMgr.Get().PlayReturningPlayerInnkeeperGreetingIfNecessary())
      return;
    SoundManager.Get().LoadAndPlay("VO_INNKEEPER_INTRO_01");
  }

  private void DoSkippedBoxIntro()
  {
    Spell fadeFromBlackSpell = Box.Get().GetBoxCamera().GetEventTable().m_FadeFromBlackSpell;
    Spell.StateFinishedCallback callback = (Spell.StateFinishedCallback) ((thisSpell, prevStateType, userData) => Time.timeScale = (float) userData);
    fadeFromBlackSpell.AddStateFinishedCallback(callback, (object) Time.timeScale);
    Time.timeScale = SceneDebugger.Get().m_MaxTimeScale;
    fadeFromBlackSpell.Activate();
  }

  private void OnStartupHubSpellFinished(Spell spell, object userData)
  {
    this.StartCoroutine(this.ShowIntroPopups());
  }

  [DebuggerHidden]
  private IEnumerator ShowIntroPopups()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Login.\u003CShowIntroPopups\u003Ec__IteratorF5() { \u003C\u003Ef__this = this };
  }

  private void ShowWelcomeQuests()
  {
    if (!DemoMgr.Get().ShouldShowWelcomeQuests())
      this.GoToNextMode();
    else if (GameUtils.ShouldDoBoxIntro())
      WelcomeQuests.Show(UserAttentionBlocker.NONE, true, new WelcomeQuests.DelOnWelcomeQuestsClosed(this.OnWelcomeQuestsCallback), true);
    else
      this.OnWelcomeQuestsCallback();
  }

  private void OnWelcomeQuestsCallback()
  {
    this.ShowAlertDialogs();
    this.GoToNextMode();
  }

  private void GoToNextMode()
  {
    if (ReturningPlayerMgr.Get().IsInReturningPlayerModeAndOnRails)
    {
      AdventureConfig.Get().SetSelectedAdventureMode(AdventureDbId.RETURNING_PLAYER, AdventureModeDbId.CLASS_CHALLENGE);
      AdventureConfig.Get().ResetSubScene(AdventureSubScenes.ClassChallenge);
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.ADVENTURE);
      Box.Get().m_OuterFrame.SetActive(false);
    }
    else
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
  }

  private void ShowAlertDialogs()
  {
    Login.ShowNerfedCards((DialogBase.HideCallback) null);
    this.ShowGoldCapAlert();
  }

  public static bool ShowNerfedCards(DialogBase.HideCallback callbackOnHide = null)
  {
    if (!UserAttentionManager.CanShowAttentionGrabber(UserAttentionBlocker.SET_ROTATION_INTRO, "ShowNerfedCards"))
      return false;
    bool flag = false;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    List<CollectibleCard> all = CollectionManager.Get().GetAllCards().FindAll((Predicate<CollectibleCard>) (card =>
    {
      if (card.PremiumType == TAG_PREMIUM.NORMAL)
        return card.IsCardChanged();
      return false;
    }));
    List<CollectibleCard> source = new List<CollectibleCard>();
    using (List<CollectibleCard>.Enumerator enumerator = all.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectibleCard current = enumerator.Current;
        if (ChangedCardMgr.Get().AllowCard(current.ChangeVersion, current.CardDbId))
        {
          source.Add(current);
          if (!flag && (CollectionManager.Get().IsCardInCollection(current.CardId, TAG_PREMIUM.NORMAL) || CollectionManager.Get().IsCardInCollection(current.CardId, TAG_PREMIUM.GOLDEN)))
            flag = true;
        }
      }
    }
    Log.ChangedCards.Print("Time to find all nerfed cards: " + (object) (float) ((double) Time.realtimeSinceStartup - (double) realtimeSinceStartup));
    if (!flag)
      return false;
    if (ReturningPlayerMgr.Get().SuppressOldPopups)
    {
      Log.ReturningPlayer.Print("Suppressing card nerf dialog due to being a Returning Player.");
      return false;
    }
    List<CollectibleCard> list = source.OrderBy<CollectibleCard, TAG_RARITY>((Func<CollectibleCard, TAG_RARITY>) (c => c.Rarity)).ThenBy<CollectibleCard, string>((Func<CollectibleCard, string>) (c => c.Name)).ToList<CollectibleCard>();
    for (int index = 0; index < list.Count; ++index)
    {
      if (list[index].CardId == "NEW1_019")
      {
        CollectibleCard collectibleCard = list[0];
        list[0] = list[index];
        list[index] = collectibleCard;
      }
    }
    using (List<CollectibleCard>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectibleCard current = enumerator.Current;
        Log.ChangedCards.Print("Displaying nerfed card " + current.Name);
      }
    }
    DialogManager.Get().ShowCardListPopup(UserAttentionBlocker.SET_ROTATION_INTRO, new CardListPopup.Info()
    {
      m_description = GameStrings.Get("GLUE_CARDS_UPDATED"),
      m_cards = list,
      m_callbackOnHide = callbackOnHide
    });
    return true;
  }

  private void ShowGoldCapAlert()
  {
    if (!UserAttentionManager.CanShowAttentionGrabber("Login.ShowGoldCapAlert"))
      return;
    NetCache.NetCacheGoldBalance netObject = NetCache.Get().GetNetObject<NetCache.NetCacheGoldBalance>();
    long cap = netObject.Cap;
    if (netObject.GetTotal() < cap)
      return;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_attentionCategory = UserAttentionBlocker.NONE,
      m_headerText = GameStrings.Format("GLUE_GOLD_CAP_HEADER", (object) cap.ToString()),
      m_text = GameStrings.Format("GLUE_GOLD_CAP_BODY", (object) cap.ToString()),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.OK
    });
  }

  private void ShowTextureCompressionWarning()
  {
    if (!ApplicationMgr.IsInternal() || AndroidDeviceSettings.IsCurrentTextureFormatSupported())
      return;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_TEXTURE_COMPRESSION_WARNING_TITLE"),
      m_text = GameStrings.Get("GLUE_TEXTURE_COMPRESSION_WARNING"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_iconSet = AlertPopup.PopupInfo.IconSet.None,
      m_cancelText = GameStrings.Get("GLOBAL_OKAY"),
      m_confirmText = GameStrings.Get("GLOBAL_CANCEL"),
      m_responseCallback = (AlertPopup.ResponseCallback) ((response, data) =>
      {
        if (response != AlertPopup.Response.CANCEL)
          return;
        Application.OpenURL("http://www.hearthstone.com.cn/download");
      })
    });
  }

  private void ShowGraphicsDeviceWarning()
  {
    if (Options.Get().GetBool(Option.SHOWN_GFX_DEVICE_WARNING, false))
      return;
    Options.Get().SetBool(Option.SHOWN_GFX_DEVICE_WARNING, true);
    string lower = SystemInfo.graphicsDeviceName.ToLower();
    if (!lower.Contains("powervr") || !lower.Contains("540") && !lower.Contains("544"))
      return;
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLUE_UNRELIABLE_GPU_WARNING_TITLE"),
      m_text = GameStrings.Get("GLUE_UNRELIABLE_GPU_WARNING"),
      m_showAlertIcon = true,
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_iconSet = AlertPopup.PopupInfo.IconSet.None,
      m_cancelText = GameStrings.Get("GLOBAL_SUPPORT"),
      m_confirmText = GameStrings.Get("GLOBAL_OKAY"),
      m_responseCallback = (AlertPopup.ResponseCallback) ((response, data) =>
      {
        if (response != AlertPopup.Response.CANCEL)
          return;
        Application.OpenURL(NydusLink.GetSupportLink("system-requirements", false));
      })
    });
  }

  private void ChangeMode_Tutorial()
  {
    PresenceMgr.Get().SetStatus((Enum) PresenceStatus.TUTORIAL_PREGAME);
    Box.Get().SetLightState(BoxLightStateType.TUTORIAL);
    Spell eventSpell = Box.Get().GetEventSpell(BoxEventType.STARTUP_TUTORIAL);
    eventSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnStartupTutorialSpellFinished));
    eventSpell.Activate();
  }

  private void OnStartupTutorialSpellFinished(Spell spell, object userData)
  {
    Box.Get().AddButtonPressListener(new Box.ButtonPressCallback(this.OnStartButtonPressed));
    Box.Get().ChangeState(Box.State.PRESS_START);
  }

  private void OnStartButtonPressed(Box.ButtonType buttonType, object userData)
  {
    if (buttonType != Box.ButtonType.START)
      return;
    if (this.m_nextMissionId == 3)
      AdTrackingManager.Get().TrackTutorialProgress(TutorialProgress.NOTHING_COMPLETE.ToString());
    Box.Get().RemoveButtonPressListener(new Box.ButtonPressCallback(this.OnStartButtonPressed));
    if (this.m_nextMissionId == 3)
    {
      if (DemoMgr.Get().GetMode() == DemoMode.APPLE_STORE || Network.ShouldBeConnectedToAurora())
      {
        this.StartTutorial();
      }
      else
      {
        Box.Get().m_StartButton.ChangeState(BoxStartButton.State.HIDDEN);
        DialogManager.Get().ShowExistingAccountPopup(new ExistingAccountPopup.ResponseCallback(this.OnExistingAccountPopupResponse), new DialogManager.DialogProcessCallback(this.OnExistingAccountLoadedCallback));
      }
    }
    else
      this.ShowTutorialProgressScreen();
  }

  private void ShowTutorialProgressScreen()
  {
    Box.Get().m_StartButton.ChangeState(BoxStartButton.State.HIDDEN);
    AssetLoader.Get().LoadActor("TutorialProgressScreen", new AssetLoader.GameObjectCallback(this.OnTutorialProgressScreenCallback), (object) null, false);
  }

  private void OnTutorialProgressScreenCallback(string name, GameObject go, object callbackData)
  {
    TutorialProgressScreen component = go.GetComponent<TutorialProgressScreen>();
    component.SetCoinPressCallback(new HeroCoin.CoinPressCallback(this.StartTutorial));
    component.StartTutorialProgress();
  }

  private void OnExistingAccountPopupResponse(bool hasAccount)
  {
    this.m_existingAccountPopup.gameObject.SetActive(false);
    if (hasAccount)
      ApplicationMgr.Get().ResetAndForceLogin();
    else
      this.StartTutorial();
  }

  private void StartTutorial()
  {
    MusicManager.Get().StopPlaylist();
    Box.Get().ChangeState(Box.State.CLOSED);
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    GameMgr.Get().FindGame(GameType.GT_TUTORIAL, FormatType.FT_WILD, this.m_nextMissionId, 0L, 0L);
  }

  private bool OnExistingAccountLoadedCallback(DialogBase dialog, object userData)
  {
    this.m_existingAccountPopup = (ExistingAccountPopup) dialog;
    this.m_existingAccountPopup.gameObject.SetActive(true);
    return true;
  }

  private void ChangeMode_SetRotation()
  {
    UserAttentionManager.StartBlocking(UserAttentionBlocker.SET_ROTATION_INTRO);
    Spell eventSpell = Box.Get().GetEventSpell(BoxEventType.STARTUP_SET_ROTATION);
    Box.Get().m_StoreButton.gameObject.SetActive(false);
    Box.Get().m_QuestLogButton.gameObject.SetActive(false);
    eventSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnSetRotationSpellFinished));
    eventSpell.Activate();
  }

  private void OnSetRotationSpellFinished(Spell spell, object userData)
  {
    this.GoToNextMode();
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    if (eventData.m_state != FindGameState.SERVER_GAME_STARTED || GameMgr.Get().IsNextReconnect())
      return false;
    Spell eventSpell = Box.Get().GetEventSpell(BoxEventType.TUTORIAL_PLAY);
    eventSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnTutorialPlaySpellStateFinished));
    eventSpell.ActivateState(SpellStateType.BIRTH);
    return true;
  }

  private void OnUpdateLoginComplete()
  {
    Network.GetUpdateLoginComplete();
    this.m_waitingForUpdateLoginComplete = false;
  }

  private void OnAssetsVersion()
  {
    AssetsVersionResponse assetsVersion = Network.GetAssetsVersion();
    if (assetsVersion != null && assetsVersion.HasReturningPlayerInfo)
      ReturningPlayerMgr.Get().SetReturningPlayerInfo(assetsVersion.ReturningPlayerInfo);
    int def = assetsVersion != null ? assetsVersion.Version : 0;
    int version = !ApplicationMgr.IsPublic() ? Vars.Key("Application.AssetsVersion").GetInt(def) : def;
    Log.UpdateManager.Print("UpdataManager: assetsVersion = {0}, gameVersion = {1}", new object[2]
    {
      (object) version,
      (object) 15590
    });
    if (version != 0 && version > 15590)
    {
      if ((UnityEngine.Object) SplashScreen.Get() != (UnityEngine.Object) null)
        SplashScreen.Get().StartPatching();
      UpdateManager.Get().StartInitialize(version, new UpdateManager.InitCallback(this.AssetsVersionCheckCompleted));
    }
    else
      this.AssetsVersionCheckCompleted();
  }

  private void OnSetProgressResponse()
  {
    SetProgressResponse progressResponse = Network.GetSetProgressResponse();
    switch (progressResponse.Result_)
    {
      case SetProgressResponse.Result.SUCCESS:
      case SetProgressResponse.Result.ALREADY_DONE:
        if (progressResponse.HasProgress)
          this.m_skipToTutorialProgress = (TutorialProgress) progressResponse.Progress;
        Options.Get().DeleteOption(Option.LOCAL_TUTORIAL_PROGRESS);
        break;
      default:
        UnityEngine.Debug.LogWarning((object) string.Format("Login.OnSetProgressResponse(): received unexpected result {0}", (object) progressResponse.Result_));
        break;
    }
    this.m_waitingForSetProgress = false;
  }

  [DebuggerHidden]
  private IEnumerator RegisterScreenWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Login.\u003CRegisterScreenWhenReady\u003Ec__IteratorF6() { \u003C\u003Ef__this = this };
  }

  private void OnTutorialPlaySpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    SpellStateType activeState = spell.GetActiveState();
    if (prevStateType == SpellStateType.BIRTH)
    {
      LoadingScreen.Get().SetFadeColor(Color.white);
      LoadingScreen.Get().EnableFadeOut(false);
      LoadingScreen.Get().AddTransitionObject(Box.Get().gameObject);
      LoadingScreen.Get().AddTransitionBlocker();
      SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnMissionSceneLoaded));
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.GAMEPLAY);
    }
    else
    {
      if (activeState != SpellStateType.NONE)
        return;
      LoadingScreen.Get().NotifyTransitionBlockerComplete();
    }
  }

  private void OnMissionSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnMissionSceneLoaded));
    Box.Get().GetEventSpell(BoxEventType.TUTORIAL_PLAY).ActivateState(SpellStateType.ACTION);
  }

  private void ChangeMode_Resume(SceneMgr.Mode mode)
  {
    if (mode == SceneMgr.Mode.HUB && Options.Get().GetBool(Option.HAS_SEEN_HUB, false))
      this.PlayInnkeeperIntroVO();
    if (mode == SceneMgr.Mode.COLLECTIONMANAGER)
      CollectionManager.Get().NotifyOfBoxTransitionStart();
    SceneMgr.Get().SetNextMode(mode);
    Box.Get().m_Camera.m_EventTable.m_FadeFromBlackSpell.Activate();
  }
}
