﻿// Decompiled with JetBrains decompiler
// Type: DesktopChatLogUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DesktopChatLogUI : IChatLogUI
{
  private QuickChatFrame m_quickChatFrame;

  public bool IsShowing
  {
    get
    {
      return (Object) this.m_quickChatFrame != (Object) null;
    }
  }

  public GameObject GameObject
  {
    get
    {
      if ((Object) this.m_quickChatFrame == (Object) null)
        return (GameObject) null;
      return this.m_quickChatFrame.gameObject;
    }
  }

  public BnetPlayer Receiver
  {
    get
    {
      if ((Object) this.m_quickChatFrame == (Object) null)
        return (BnetPlayer) null;
      return this.m_quickChatFrame.GetReceiver();
    }
  }

  public void ShowForPlayer(BnetPlayer player)
  {
    if ((Object) this.m_quickChatFrame != (Object) null)
      return;
    GameObject gameObject = AssetLoader.Get().LoadGameObject("QuickChatFrame", true, false);
    if (!((Object) gameObject != (Object) null))
      return;
    this.m_quickChatFrame = gameObject.GetComponent<QuickChatFrame>();
    this.m_quickChatFrame.SetReceiver(player);
  }

  public void Hide()
  {
    if ((Object) this.m_quickChatFrame == (Object) null)
      return;
    Object.Destroy((Object) this.m_quickChatFrame.gameObject);
    this.m_quickChatFrame = (QuickChatFrame) null;
  }

  public void GoBack()
  {
  }
}
