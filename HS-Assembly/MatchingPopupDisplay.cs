﻿// Decompiled with JetBrains decompiler
// Type: MatchingPopupDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using HutongGames.PlayMaker;
using PegasusShared;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class MatchingPopupDisplay : TransitionPopup
{
  private List<GameObject> m_spinnerTexts = new List<GameObject>();
  private const int NUM_SPINNER_ENTRIES = 10;
  public UberText m_tipOfTheDay;
  public GameObject m_nameContainer;
  public GameObject m_wildVines;
  private SceneMgr.Mode m_gameMode;

  protected override void Awake()
  {
    base.Awake();
    this.SetupSpinnerText();
    this.UpdateTipOfTheDay();
    this.GenerateRandomSpinnerTexts();
    this.m_title.Text = GameStrings.Get("GLUE_MATCHMAKER_FINDING_OPPONENT");
    this.m_nameContainer.SetActive(false);
    this.m_title.gameObject.SetActive(false);
    this.m_tipOfTheDay.gameObject.SetActive(false);
    this.m_wildVines.SetActive(false);
    SoundManager.Get().Load("FindOpponent_mechanism_start");
  }

  protected override void OnGameConnecting(FindGameEventData eventData)
  {
    base.OnGameConnecting(eventData);
    this.IncreaseTooltipProgress();
  }

  protected override void OnGameEntered(FindGameEventData eventData)
  {
    this.EnableCancelButtonIfPossible();
  }

  protected override void OnGameDelayed(FindGameEventData eventData)
  {
    this.EnableCancelButtonIfPossible();
  }

  protected override void OnAnimateShowFinished()
  {
    base.OnAnimateShowFinished();
    this.EnableCancelButtonIfPossible();
  }

  private void SetupSpinnerText()
  {
    for (int index = 1; index <= 10; ++index)
      this.m_spinnerTexts.Add(SceneUtils.FindChild(this.gameObject, "NAME_" + (object) index).gameObject);
  }

  private void GenerateRandomSpinnerTexts()
  {
    int num = 1;
    List<string> stringList = new List<string>();
    while (true)
    {
      string str = GameStrings.Get("GLUE_SPINNER_" + (object) num);
      if (!(str == "GLUE_SPINNER_" + (object) num))
      {
        stringList.Add(str);
        ++num;
      }
      else
        break;
    }
    SceneUtils.FindChild(this.gameObject, "NAME_PerfectOpponent").gameObject.GetComponent<UberText>().Text = GameStrings.Get("GLUE_MATCHMAKER_PERFECT_OPPONENT");
    for (int index1 = 0; index1 < 10; ++index1)
    {
      int index2 = Mathf.FloorToInt(Random.value * (float) stringList.Count);
      this.m_spinnerTexts[index1].GetComponent<UberText>().Text = stringList[index2];
      stringList.RemoveAt(index2);
    }
  }

  [DebuggerHidden]
  private IEnumerator StopSpinnerDelay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MatchingPopupDisplay.\u003CStopSpinnerDelay\u003Ec__IteratorF7() { \u003C\u003Ef__this = this };
  }

  private bool OnNavigateBack()
  {
    if (!this.m_cancelButton.gameObject.activeSelf)
      return false;
    this.GetComponent<PlayMakerFSM>().SendEvent("Cancel");
    this.FireMatchCanceledEvent();
    return true;
  }

  protected override void OnCancelButtonReleased(UIEvent e)
  {
    base.OnCancelButtonReleased(e);
    Navigation.GoBack();
  }

  private void UpdateTipOfTheDay()
  {
    this.m_gameMode = SceneMgr.Get().GetMode();
    if (this.m_gameMode == SceneMgr.Mode.TOURNAMENT)
      this.m_tipOfTheDay.Text = GameStrings.GetTip(TipCategory.PLAY, Options.Get().GetInt(Option.TIP_PLAY_PROGRESS, 0), TipCategory.DEFAULT);
    else if (this.m_gameMode == SceneMgr.Mode.DRAFT)
      this.m_tipOfTheDay.Text = GameStrings.GetTip(TipCategory.FORGE, Options.Get().GetInt(Option.TIP_FORGE_PROGRESS, 0), TipCategory.DEFAULT);
    else if (this.m_gameMode == SceneMgr.Mode.TAVERN_BRAWL)
    {
      if (TavernBrawlManager.Get().IsCurrentSeasonSessionBased)
        this.m_tipOfTheDay.Text = GameStrings.GetRandomTip(TipCategory.HEROICBRAWL);
      else
        this.m_tipOfTheDay.Text = GameStrings.GetRandomTip(TipCategory.TAVERNBRAWL);
    }
    else
      this.m_tipOfTheDay.Text = GameStrings.GetRandomTip(TipCategory.DEFAULT);
  }

  private void IncreaseTooltipProgress()
  {
    if (this.m_gameMode == SceneMgr.Mode.TOURNAMENT)
    {
      Options.Get().SetInt(Option.TIP_PLAY_PROGRESS, Options.Get().GetInt(Option.TIP_PLAY_PROGRESS, 0) + 1);
    }
    else
    {
      if (this.m_gameMode != SceneMgr.Mode.DRAFT)
        return;
      Options.Get().SetInt(Option.TIP_FORGE_PROGRESS, Options.Get().GetInt(Option.TIP_FORGE_PROGRESS, 0) + 1);
    }
  }

  protected override void ShowPopup()
  {
    SoundManager.Get().LoadAndPlay("FindOpponent_mechanism_start");
    base.ShowPopup();
    PlayMakerFSM component = this.GetComponent<PlayMakerFSM>();
    FsmBool fsmBool = component.FsmVariables.FindFsmBool("PlaySpinningMusic");
    if (fsmBool != null)
      fsmBool.Value = this.m_gameMode != SceneMgr.Mode.TAVERN_BRAWL;
    component.SendEvent("Birth");
    SceneUtils.EnableRenderers(this.m_nameContainer, false);
    this.m_title.gameObject.SetActive(true);
    this.m_tipOfTheDay.gameObject.SetActive(true);
    bool flag = this.m_formatType == FormatType.FT_WILD;
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.TOURNAMENT)
      flag = false;
    this.m_wildVines.SetActive(flag);
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  protected override void OnGameplaySceneLoaded()
  {
    this.m_nameContainer.SetActive(true);
    this.GetComponent<PlayMakerFSM>().SendEvent("Death");
    this.StartCoroutine(this.StopSpinnerDelay());
    Navigation.Clear();
  }

  protected override void OnGameError(FindGameEventData eventData)
  {
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  protected override void OnGameCanceled(FindGameEventData eventData)
  {
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }
}
