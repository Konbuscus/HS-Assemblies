﻿// Decompiled with JetBrains decompiler
// Type: DialogManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

[CustomEditClass]
public class DialogManager : MonoBehaviour
{
  private Queue<DialogManager.DialogRequest> m_dialogRequests = new Queue<DialogManager.DialogRequest>();
  private List<long> m_handledMedalNoticeIDs = new List<long>();
  public List<DialogManager.DialogTypeMapping> m_typeMapping = new List<DialogManager.DialogTypeMapping>();
  private static DialogManager s_instance;
  private DialogBase m_currentDialog;
  private bool m_loadingDialog;
  private bool m_isReadyForSeasonEndPopup;
  private bool m_waitingToShowSeasonEndDialog;
  private NetCache.ProfileNoticeMedal m_medalNotice;
  private NetCache.ProfileNoticeBonusStars m_bonusStarsNotice;

  public event Action OnDialogShown;

  public event Action OnDialogHidden;

  private void Awake()
  {
    DialogManager.s_instance = this;
    NetCache.NetCacheProfileNotices netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileNotices>();
    if (netObject != null)
      this.MaybeShowSeasonEndDialog(netObject.Notices, false);
    NetCache.Get().RegisterNewNoticesListener(new NetCache.DelNewNoticesListener(this.OnNewNotices));
  }

  private void OnDestroy()
  {
    NetCache.Get().RemoveNewNoticesListener(new NetCache.DelNewNoticesListener(this.OnNewNotices));
    DialogManager.s_instance = (DialogManager) null;
  }

  public static DialogManager Get()
  {
    return DialogManager.s_instance;
  }

  public void GoBack()
  {
    if (!(bool) ((UnityEngine.Object) this.m_currentDialog))
      return;
    this.m_currentDialog.GoBack();
  }

  public void ReadyForSeasonEndPopup(bool ready)
  {
    this.m_isReadyForSeasonEndPopup = ready;
  }

  public bool HandleKeyboardInput()
  {
    if (Input.GetKeyUp(KeyCode.Escape) && (bool) ((UnityEngine.Object) this.m_currentDialog))
      return this.m_currentDialog.HandleKeyboardInput();
    return false;
  }

  public void AddToQueue(DialogManager.DialogRequest request)
  {
    if (UserAttentionManager.IsBlockedBy(UserAttentionBlocker.FATAL_ERROR_SCENE) || !UserAttentionManager.CanShowAttentionGrabber(request.m_attentionCategory, "DialogManager.AddToQueue:" + (request != null ? request.m_type.ToString() : "null")))
      return;
    this.m_dialogRequests.Enqueue(request);
    this.UpdateQueue();
  }

  private void UpdateQueue()
  {
    if (UserAttentionManager.IsBlockedBy(UserAttentionBlocker.FATAL_ERROR_SCENE) || (UnityEngine.Object) this.m_currentDialog != (UnityEngine.Object) null || this.m_loadingDialog)
      return;
    if (this.m_dialogRequests.Count == 0)
    {
      this.DestroyPopupAssetsIfPossible();
    }
    else
    {
      DialogManager.DialogRequest request = this.m_dialogRequests.Peek();
      if (!UserAttentionManager.CanShowAttentionGrabber(request.m_attentionCategory, "DialogManager.UpdateQueue:" + (object) request.m_attentionCategory))
        ApplicationMgr.Get().ScheduleCallback(0.5f, false, (ApplicationMgr.ScheduledCallback) (userData => this.UpdateQueue()), (object) null);
      else
        this.LoadPopup(request);
    }
  }

  public void ShowPopup(AlertPopup.PopupInfo info, DialogManager.DialogProcessCallback callback, object userData)
  {
    if (UserAttentionManager.IsBlockedBy(UserAttentionBlocker.FATAL_ERROR_SCENE) || !UserAttentionManager.CanShowAttentionGrabber(info.m_attentionCategory, "DialogManager.ShowPopup:" + (info != null ? info.m_id + ":" + info.m_attentionCategory.ToString() : "null")))
      return;
    this.AddToQueue(new DialogManager.DialogRequest()
    {
      m_type = DialogManager.DialogType.ALERT,
      m_attentionCategory = info.m_attentionCategory,
      m_info = (object) info,
      m_callback = callback,
      m_userData = userData
    });
  }

  public void ShowPopup(AlertPopup.PopupInfo info, DialogManager.DialogProcessCallback callback)
  {
    this.ShowPopup(info, callback, (object) null);
  }

  public void ShowPopup(AlertPopup.PopupInfo info)
  {
    this.ShowPopup(info, (DialogManager.DialogProcessCallback) null, (object) null);
  }

  public bool ShowUniquePopup(AlertPopup.PopupInfo info, DialogManager.DialogProcessCallback callback, object userData)
  {
    if (UserAttentionManager.IsBlockedBy(UserAttentionBlocker.FATAL_ERROR_SCENE) || !UserAttentionManager.CanShowAttentionGrabber(info.m_attentionCategory, "DialogManager.ShowUniquePopup:" + (info != null ? info.m_id + ":" + info.m_attentionCategory.ToString() : "null")))
      return false;
    if (!string.IsNullOrEmpty(info.m_id))
    {
      using (Queue<DialogManager.DialogRequest>.Enumerator enumerator = this.m_dialogRequests.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          DialogManager.DialogRequest current = enumerator.Current;
          if (current.m_type == DialogManager.DialogType.ALERT && ((AlertPopup.PopupInfo) current.m_info).m_id == info.m_id)
            return false;
        }
      }
    }
    this.ShowPopup(info, callback, userData);
    return true;
  }

  public bool ShowUniquePopup(AlertPopup.PopupInfo info, DialogManager.DialogProcessCallback callback)
  {
    return this.ShowUniquePopup(info, callback, (object) null);
  }

  public bool ShowUniquePopup(AlertPopup.PopupInfo info)
  {
    return this.ShowUniquePopup(info, (DialogManager.DialogProcessCallback) null, (object) null);
  }

  public void ShowMessageOfTheDay(string message)
  {
    this.ShowPopup(new AlertPopup.PopupInfo()
    {
      m_text = message
    });
  }

  public void RemoveUniquePopupRequestFromQueue(string id)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DialogManager.\u003CRemoveUniquePopupRequestFromQueue\u003Ec__AnonStorey437 queueCAnonStorey437 = new DialogManager.\u003CRemoveUniquePopupRequestFromQueue\u003Ec__AnonStorey437();
    // ISSUE: reference to a compiler-generated field
    queueCAnonStorey437.id = id;
    // ISSUE: reference to a compiler-generated field
    if (string.IsNullOrEmpty(queueCAnonStorey437.id))
      return;
    using (Queue<DialogManager.DialogRequest>.Enumerator enumerator = this.m_dialogRequests.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DialogManager.DialogRequest current = enumerator.Current;
        // ISSUE: reference to a compiler-generated field
        if (current.m_type == DialogManager.DialogType.ALERT && ((AlertPopup.PopupInfo) current.m_info).m_id == queueCAnonStorey437.id)
        {
          // ISSUE: reference to a compiler-generated method
          this.m_dialogRequests = new Queue<DialogManager.DialogRequest>(this.m_dialogRequests.Where<DialogManager.DialogRequest>(new Func<DialogManager.DialogRequest, bool>(queueCAnonStorey437.\u003C\u003Em__2DD)));
          break;
        }
      }
    }
  }

  public bool WaitingToShowSeasonEndDialog()
  {
    if (this.m_waitingToShowSeasonEndDialog || (UnityEngine.Object) this.m_currentDialog != (UnityEngine.Object) null && this.m_currentDialog is SeasonEndDialog)
      return true;
    return this.m_dialogRequests.FirstOrDefault<DialogManager.DialogRequest>((Func<DialogManager.DialogRequest, bool>) (obj => obj.m_type == DialogManager.DialogType.SEASON_END)) != null;
  }

  public void ShowFriendlyChallenge(FormatType formatType, BnetPlayer challenger, bool challengeIsTavernBrawl, FriendlyChallengeDialog.ResponseCallback responseCallback, DialogManager.DialogProcessCallback callback)
  {
    this.AddToQueue(new DialogManager.DialogRequest()
    {
      m_type = !challengeIsTavernBrawl ? DialogManager.DialogType.FRIENDLY_CHALLENGE : DialogManager.DialogType.TAVERN_BRAWL_CHALLENGE,
      m_info = (object) new FriendlyChallengeDialog.Info()
      {
        m_formatType = formatType,
        m_challenger = challenger,
        m_callback = responseCallback
      },
      m_callback = callback
    });
  }

  public void ShowExistingAccountPopup(ExistingAccountPopup.ResponseCallback responseCallback, DialogManager.DialogProcessCallback callback)
  {
    this.AddToQueue(new DialogManager.DialogRequest()
    {
      m_type = DialogManager.DialogType.EXISTING_ACCOUNT,
      m_info = (object) new ExistingAccountPopup.Info()
      {
        m_callback = responseCallback
      },
      m_callback = callback
    });
  }

  public void ShowCardListPopup(UserAttentionBlocker attentionCategory, CardListPopup.Info info)
  {
    this.AddToQueue(new DialogManager.DialogRequest()
    {
      m_type = DialogManager.DialogType.CARD_LIST,
      m_attentionCategory = attentionCategory,
      m_info = (object) info
    });
  }

  public void ClearAllImmediately()
  {
    if ((UnityEngine.Object) this.m_currentDialog != (UnityEngine.Object) null)
    {
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_currentDialog.gameObject);
      this.m_currentDialog = (DialogBase) null;
    }
    this.m_dialogRequests.Clear();
    this.DestroyPopupAssetsIfPossible();
  }

  public bool ShowingDialog()
  {
    if (!((UnityEngine.Object) this.m_currentDialog != (UnityEngine.Object) null))
      return this.m_dialogRequests.Count > 0;
    return true;
  }

  private void OnNewNotices(List<NetCache.ProfileNotice> newNotices, bool isInitialNoticeList)
  {
    this.MaybeShowSeasonEndDialog(newNotices, !isInitialNoticeList);
  }

  private void MaybeShowSeasonEndDialog(List<NetCache.ProfileNotice> newNotices, bool fromOutOfBandNotice)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DialogManager.\u003CMaybeShowSeasonEndDialog\u003Ec__AnonStorey438 dialogCAnonStorey438 = new DialogManager.\u003CMaybeShowSeasonEndDialog\u003Ec__AnonStorey438();
    // ISSUE: reference to a compiler-generated field
    dialogCAnonStorey438.newNotices = newNotices;
    // ISSUE: reference to a compiler-generated field
    dialogCAnonStorey438.newNotices.Sort((Comparison<NetCache.ProfileNotice>) ((a, b) =>
    {
      if (a.Type != b.Type)
        return a.Type - b.Type;
      if (a.Origin != b.Origin)
        return a.Origin - b.Origin;
      if (a.OriginData != b.OriginData)
        return (int) (a.OriginData - b.OriginData);
      return (int) (a.NoticeID - b.NoticeID);
    }));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    dialogCAnonStorey438.medalNotice = dialogCAnonStorey438.newNotices.Where<NetCache.ProfileNotice>(new Func<NetCache.ProfileNotice, bool>(dialogCAnonStorey438.\u003C\u003Em__2E0)).FirstOrDefault<NetCache.ProfileNotice>();
    // ISSUE: reference to a compiler-generated field
    if (dialogCAnonStorey438.medalNotice != null)
    {
      // ISSUE: reference to a compiler-generated field
      this.m_medalNotice = (NetCache.ProfileNoticeMedal) dialogCAnonStorey438.medalNotice;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      this.m_bonusStarsNotice = (NetCache.ProfileNoticeBonusStars) dialogCAnonStorey438.newNotices.FirstOrDefault<NetCache.ProfileNotice>(new Func<NetCache.ProfileNotice, bool>(dialogCAnonStorey438.\u003C\u003Em__2E1));
    }
    if (this.m_medalNotice == null || this.m_bonusStarsNotice == null || (this.m_medalNotice.OriginData != this.m_bonusStarsNotice.OriginData || this.m_handledMedalNoticeIDs.Contains(this.m_medalNotice.NoticeID)) || (UserAttentionManager.IsBlockedBy(UserAttentionBlocker.FATAL_ERROR_SCENE) || !UserAttentionManager.CanShowAttentionGrabber("DialogManager.MaybeShowSeasonEndDialog")))
      return;
    this.m_handledMedalNoticeIDs.Add(this.m_medalNotice.NoticeID);
    if (ReturningPlayerMgr.Get().SuppressOldPopups)
    {
      Log.ReturningPlayer.Print("Suppressing popup for Season End Dialogue {0} due to being a Returning Player!");
      Network.AckNotice(this.m_medalNotice.NoticeID);
      Network.AckNotice(this.m_bonusStarsNotice.NoticeID);
    }
    else
    {
      if (fromOutOfBandNotice)
      {
        NetCache.Get().RefreshNetObject<NetCache.NetCacheMedalInfo>();
        NetCache.Get().ReloadNetObject<NetCache.NetCacheRewardProgress>();
      }
      this.StartCoroutine(this.ShowSeasonEndDialogWhenReady(new DialogManager.DialogRequest()
      {
        m_type = DialogManager.DialogType.SEASON_END,
        m_info = (object) new DialogManager.SeasonEndDialogRequestInfo()
        {
          m_noticeMedal = this.m_medalNotice,
          m_noticeBonusStars = this.m_bonusStarsNotice
        }
      }));
    }
  }

  private void LoadPopup(DialogManager.DialogRequest request)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DialogManager.\u003CLoadPopup\u003Ec__AnonStorey43A popupCAnonStorey43A = new DialogManager.\u003CLoadPopup\u003Ec__AnonStorey43A();
    // ISSUE: reference to a compiler-generated field
    popupCAnonStorey43A.request = request;
    // ISSUE: reference to a compiler-generated method
    DialogManager.DialogTypeMapping dialogTypeMapping = this.m_typeMapping.Find(new Predicate<DialogManager.DialogTypeMapping>(popupCAnonStorey43A.\u003C\u003Em__2E2));
    if (dialogTypeMapping == null || dialogTypeMapping.m_prefabName == null)
    {
      // ISSUE: reference to a compiler-generated field
      Error.AddDevFatal("DialogManager.LoadPopup() - unhandled dialog type {0}", (object) popupCAnonStorey43A.request.m_type);
    }
    else
    {
      this.m_loadingDialog = true;
      AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(dialogTypeMapping.m_prefabName), new AssetLoader.GameObjectCallback(this.OnPopupLoaded), (object) null, true);
    }
  }

  private void OnPopupLoaded(string name, GameObject go, object callbackData)
  {
    this.m_loadingDialog = false;
    DialogManager.DialogRequest dialogRequest = this.m_dialogRequests.Peek();
    if (this.m_dialogRequests.Count == 0 || UserAttentionManager.IsBlockedBy(UserAttentionBlocker.FATAL_ERROR_SCENE) || !UserAttentionManager.CanShowAttentionGrabber(dialogRequest != null ? dialogRequest.m_attentionCategory : UserAttentionBlocker.NONE, "DialogManager.OnPopupLoaded:" + (dialogRequest != null ? dialogRequest.m_type.ToString() : "null")))
    {
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
      this.DestroyPopupAssetsIfPossible();
    }
    else
    {
      DialogManager.DialogRequest request = this.m_dialogRequests.Dequeue();
      DialogBase component = go.GetComponent<DialogBase>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("DialogManager.OnPopupLoaded() - game object {0} has no {1} component", (object) go, (object) request.m_type));
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
        this.UpdateQueue();
      }
      else
        this.ProcessRequest(request, component);
    }
  }

  private void DestroyPopupAssetsIfPossible()
  {
    if (this.m_loadingDialog)
      return;
    using (List<DialogManager.DialogTypeMapping>.Enumerator enumerator = this.m_typeMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
        AssetCache.ClearGameObject(FileUtils.GameAssetPathToName(enumerator.Current.m_prefabName));
    }
  }

  private void ProcessRequest(DialogManager.DialogRequest request, DialogBase dialog)
  {
    if (request.m_callback != null && !request.m_callback(dialog, request.m_userData))
    {
      this.UpdateQueue();
    }
    else
    {
      this.m_currentDialog = dialog;
      this.m_currentDialog.AddHideListener(new DialogBase.HideCallback(this.OnCurrentDialogHidden));
      if (request.m_type == DialogManager.DialogType.ALERT)
        this.ProcessAlertRequest(request, (AlertPopup) dialog);
      else if (request.m_type == DialogManager.DialogType.SEASON_END)
        this.ProcessMedalRequest(request, (SeasonEndDialog) dialog);
      else if (request.m_type == DialogManager.DialogType.FRIENDLY_CHALLENGE || request.m_type == DialogManager.DialogType.TAVERN_BRAWL_CHALLENGE)
        this.ProcessFriendlyChallengeRequest(request, (FriendlyChallengeDialog) dialog);
      else if (request.m_type == DialogManager.DialogType.EXISTING_ACCOUNT)
        this.ProcessExistingAccountRequest(request, (ExistingAccountPopup) dialog);
      else if (request.m_type == DialogManager.DialogType.CARD_LIST)
        this.ProcessCardListRequest(request, (CardListPopup) dialog);
      if (this.OnDialogShown == null)
        return;
      this.OnDialogShown();
    }
  }

  private void ProcessExistingAccountRequest(DialogManager.DialogRequest request, ExistingAccountPopup exAcctPopup)
  {
    exAcctPopup.SetInfo((ExistingAccountPopup.Info) request.m_info);
    exAcctPopup.Show();
  }

  private void ProcessAlertRequest(DialogManager.DialogRequest request, AlertPopup alertPopup)
  {
    AlertPopup.PopupInfo info = (AlertPopup.PopupInfo) request.m_info;
    alertPopup.SetInfo(info);
    alertPopup.Show();
  }

  private void ProcessMedalRequest(DialogManager.DialogRequest request, SeasonEndDialog seasonEndDialog)
  {
    SeasonEndDialog.SeasonEndInfo info1;
    if (request.m_isFake)
    {
      info1 = request.m_info as SeasonEndDialog.SeasonEndInfo;
      if (info1 == null)
        return;
    }
    else
    {
      DialogManager.SeasonEndDialogRequestInfo info2 = request.m_info as DialogManager.SeasonEndDialogRequestInfo;
      info1 = new SeasonEndDialog.SeasonEndInfo();
      info1.m_noticesToAck.Add(info2.m_noticeMedal.NoticeID);
      info1.m_seasonID = (int) info2.m_noticeMedal.OriginData;
      info1.m_rank = 26 - info2.m_noticeMedal.StarLevel;
      info1.m_chestRank = 26 - info2.m_noticeMedal.BestStarLevel;
      info1.m_legendIndex = info2.m_noticeMedal.LegendRank;
      info1.m_rankedRewards = info2.m_noticeMedal.Chest.Rewards;
      info1.m_isWild = info2.m_noticeMedal.IsWild;
      if (info2.m_noticeBonusStars != null)
      {
        info1.m_boostedRank = 26 - info2.m_noticeBonusStars.StarLevel;
        info1.m_bonusStars = info2.m_noticeBonusStars.Stars;
        info1.m_noticesToAck.Add(info2.m_noticeBonusStars.NoticeID);
      }
    }
    seasonEndDialog.Init(info1);
    seasonEndDialog.Show();
  }

  private void ProcessFriendlyChallengeRequest(DialogManager.DialogRequest request, FriendlyChallengeDialog friendlyChallengeDialog)
  {
    friendlyChallengeDialog.SetInfo((FriendlyChallengeDialog.Info) request.m_info);
    friendlyChallengeDialog.Show();
  }

  private void ProcessCardListRequest(DialogManager.DialogRequest request, CardListPopup cardListPopup)
  {
    CardListPopup.Info info = (CardListPopup.Info) request.m_info;
    cardListPopup.SetInfo(info);
    cardListPopup.Show();
  }

  private void OnCurrentDialogHidden(DialogBase dialog, object userData)
  {
    if ((UnityEngine.Object) dialog != (UnityEngine.Object) this.m_currentDialog)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_currentDialog.gameObject);
    this.m_currentDialog = (DialogBase) null;
    this.UpdateQueue();
    if (this.OnDialogHidden == null)
      return;
    this.OnDialogHidden();
  }

  [DebuggerHidden]
  private IEnumerator ShowSeasonEndDialogWhenReady(DialogManager.DialogRequest request)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DialogManager.\u003CShowSeasonEndDialogWhenReady\u003Ec__Iterator307() { request = request, \u003C\u0024\u003Erequest = request, \u003C\u003Ef__this = this };
  }

  public enum DialogType
  {
    ALERT,
    SEASON_END,
    FRIENDLY_CHALLENGE,
    TAVERN_BRAWL_CHALLENGE,
    EXISTING_ACCOUNT,
    CARD_LIST,
  }

  public class DialogRequest
  {
    public DialogManager.DialogType m_type;
    public UserAttentionBlocker m_attentionCategory;
    public object m_info;
    public DialogManager.DialogProcessCallback m_callback;
    public object m_userData;
    public bool m_isFake;
  }

  [Serializable]
  public class DialogTypeMapping
  {
    public DialogManager.DialogType m_type;
    [CustomEditField(T = EditType.GAME_OBJECT)]
    public string m_prefabName;
  }

  private class SeasonEndDialogRequestInfo
  {
    public NetCache.ProfileNoticeMedal m_noticeMedal;
    public NetCache.ProfileNoticeBonusStars m_noticeBonusStars;
  }

  public delegate bool DialogProcessCallback(DialogBase dialog, object userData);
}
