﻿// Decompiled with JetBrains decompiler
// Type: RankedPlayToggleButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class RankedPlayToggleButton : PegUIElement
{
  public GameObject m_button;
  public Transform m_upBone;
  public Transform m_downBone;
  public HighlightState m_highlight;
  public Material m_buttonUpMaterial;
  public Material m_buttonDownMaterial;
  public Material m_wildButtonUpMaterial;
  public Material m_wildButtonDownMaterial;
  public Material m_buttonDisabledMaterial;
  public MeshRenderer m_glowQuad;
  public RankedPlayToggleButton.EnabledStateMaterialToggler[] m_materialTogglers;
  private bool m_isDown;
  private bool m_isWild;
  private Material m_currentDownMaterial;
  private Material m_currentUpMaterial;

  public void Up()
  {
    this.m_isDown = false;
    this.m_glowQuad.enabled = false;
    iTween.MoveTo(this.m_button, iTween.Hash((object) "position", (object) this.m_upBone.localPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.1, (object) "easeType", (object) iTween.EaseType.linear));
    this.m_button.GetComponent<Renderer>().material = this.GetCorrectMaterial(this.m_isWild);
    this.m_highlight.ChangeState(ActorStateType.NONE);
    this.UpdateMaterialTogglers();
  }

  public void Down()
  {
    this.m_isDown = true;
    iTween.MoveTo(this.m_button, iTween.Hash((object) "position", (object) this.m_downBone.localPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.1, (object) "easeType", (object) iTween.EaseType.linear));
    this.m_button.GetComponent<Renderer>().material = this.GetCorrectMaterial(this.m_isWild);
    if (DeckPickerTrayDisplay.Get().IsMissingStandardDeckTrayShown())
    {
      this.m_glowQuad.enabled = false;
      this.m_highlight.ChangeState(ActorStateType.NONE);
    }
    else
    {
      this.m_glowQuad.enabled = true;
      this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    }
    this.UpdateMaterialTogglers();
  }

  public void SetFormat(bool isWild)
  {
    this.m_isWild = isWild;
    this.m_button.GetComponent<Renderer>().material = this.GetCorrectMaterial(this.m_isWild);
    this.UpdateMaterialTogglers();
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    SoundManager.Get().LoadAndPlay("collection_manager_hero_mouse_over");
    if (this.m_glowQuad.enabled)
      return;
    this.m_highlight.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    if (this.m_glowQuad.enabled)
      return;
    this.m_highlight.ChangeState(ActorStateType.NONE);
  }

  private void UpdateMaterialTogglers()
  {
    foreach (RankedPlayToggleButton.EnabledStateMaterialToggler materialToggler in this.m_materialTogglers)
    {
      if (this.IsEnabled())
        materialToggler.m_targetMesh.GetComponent<Renderer>().material = !this.m_isWild ? materialToggler.m_enabledMaterial : materialToggler.m_wildEnabledMaterial;
      else
        materialToggler.m_targetMesh.GetComponent<Renderer>().material = materialToggler.m_disabledMaterial;
    }
  }

  private Material GetCorrectMaterial(bool isWild)
  {
    if (!this.IsEnabled() && (UnityEngine.Object) this.m_buttonDisabledMaterial != (UnityEngine.Object) null)
      return this.m_buttonDisabledMaterial;
    if (this.m_isDown)
    {
      if (isWild && (UnityEngine.Object) this.m_wildButtonDownMaterial != (UnityEngine.Object) null)
        return this.m_wildButtonDownMaterial;
      return this.m_buttonDownMaterial;
    }
    if (isWild && (UnityEngine.Object) this.m_wildButtonUpMaterial != (UnityEngine.Object) null)
      return this.m_wildButtonUpMaterial;
    return this.m_buttonUpMaterial;
  }

  [Serializable]
  public struct EnabledStateMaterialToggler
  {
    public MeshRenderer m_targetMesh;
    public Material m_enabledMaterial;
    public Material m_wildEnabledMaterial;
    public Material m_disabledMaterial;
  }
}
