﻿// Decompiled with JetBrains decompiler
// Type: AchievementDbId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum AchievementDbId
{
  INVALID = 0,
  FIRST_BLOOD = 11,
  ENTER_THE_ARENA = 56,
  OG_LAUNCH_PERIOD_FREE_PACKS = 339,
  SET_ROTATION_2016_QUEST1 = 340,
  RETURNING_PLAYER_QUEST1 = 568,
  RETURNING_PLAYER_QUEST2 = 569,
  RETURNING_PLAYER_QUEST3 = 571,
}
