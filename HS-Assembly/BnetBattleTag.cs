﻿// Decompiled with JetBrains decompiler
// Type: BnetBattleTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BnetBattleTag
{
  private string m_name;
  private int m_number;

  public static bool operator ==(BnetBattleTag a, BnetBattleTag b)
  {
    if (object.ReferenceEquals((object) a, (object) b))
      return true;
    if ((object) a == null || (object) b == null || !(a.m_name == b.m_name))
      return false;
    return a.m_number == b.m_number;
  }

  public static bool operator !=(BnetBattleTag a, BnetBattleTag b)
  {
    return !(a == b);
  }

  public static BnetBattleTag CreateFromString(string src)
  {
    BnetBattleTag bnetBattleTag = new BnetBattleTag();
    if (!bnetBattleTag.SetString(src))
      return (BnetBattleTag) null;
    return bnetBattleTag;
  }

  public BnetBattleTag Clone()
  {
    return (BnetBattleTag) this.MemberwiseClone();
  }

  public string GetName()
  {
    return this.m_name;
  }

  public void SetName(string name)
  {
    this.m_name = name;
  }

  public int GetNumber()
  {
    return this.m_number;
  }

  public void SetNumber(int number)
  {
    this.m_number = number;
  }

  public string GetString()
  {
    return string.Format("{0}#{1}", (object) this.m_name, (object) this.m_number);
  }

  public bool SetString(string composite)
  {
    if (composite == null)
    {
      Error.AddDevFatal("BnetBattleTag.SetString() - Given null string.");
      return false;
    }
    string[] strArray = composite.Split('#');
    if (strArray.Length < 2)
    {
      Debug.LogWarningFormat("BnetBattleTag.SetString() - Failed to split BattleTag \"{0}\" into 2 parts - this will prevent this player from showing up in Friends list and other places.", (object) composite);
      return false;
    }
    if (!int.TryParse(strArray[1], out this.m_number))
    {
      Error.AddDevFatal("BnetBattleTag.SetString() - Failed to parse \"{0}\" into a number. Original string: \"{1}\"", (object) strArray[1], (object) composite);
      return false;
    }
    this.m_name = strArray[0];
    return true;
  }

  public override bool Equals(object obj)
  {
    if (obj == null)
      return false;
    BnetBattleTag bnetBattleTag = obj as BnetBattleTag;
    if ((object) bnetBattleTag == null || !(this.m_name == bnetBattleTag.m_name))
      return false;
    return this.m_number == bnetBattleTag.m_number;
  }

  public bool Equals(BnetBattleTag other)
  {
    if ((object) other == null || !(this.m_name == other.m_name))
      return false;
    return this.m_number == other.m_number;
  }

  public override int GetHashCode()
  {
    return (17 * 11 + this.m_name.GetHashCode()) * 11 + this.m_number.GetHashCode();
  }

  public override string ToString()
  {
    return string.Format("{0}#{1}", (object) this.m_name, (object) this.m_number);
  }
}
