﻿// Decompiled with JetBrains decompiler
// Type: BaseUIBones
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class BaseUIBones
{
  public Transform m_AddFriend;
  public Transform m_AddFriendVirtualKeyboard;
  public Transform m_AddFriendAndroidKeyboard;
  public Transform m_AddFriendPhoneKeyboard;
  public Transform m_RecruitAFriend;
  public Transform m_RecruitAFriendVirtualKeyboard;
  public Transform m_ChatBubble;
  public Transform m_InGameMenu;
  public Transform m_BoxMenu;
  public Transform m_BoxMenuWithRatings;
  public Transform m_OptionsMenu;
  public Transform m_QuickChat;
  public Transform m_QuickChatVirtualKeyboard;
}
