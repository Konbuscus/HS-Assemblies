﻿// Decompiled with JetBrains decompiler
// Type: QuestTile
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class QuestTile : MonoBehaviour
{
  private const float NAME_LINE_PADDING = 0.22f;
  public UberText m_goldAmount;
  public GameObject m_defaultBone;
  public UberText m_requirement;
  public UberText m_questName;
  public GameObject m_nameLine;
  public GameObject m_progress;
  public UberText m_progressText;
  public GameObject m_rewardIcon;
  public NormalButton m_cancelButton;
  public GameObject m_cancelButtonRoot;
  public PlayMakerFSM m_BurnPlaymaker;
  public GameObject m_legendaryFX;
  public MeshRenderer m_tileRenderer;
  public Material m_tileLegendaryMaterial;
  private Achievement m_quest;
  private bool m_canShowCancelButton;

  private void Awake()
  {
    this.SetCanShowCancelButton(false);
    this.m_cancelButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCancelButtonReleased));
  }

  public void SetupTile(Achievement quest)
  {
    quest.AckCurrentProgressAndRewardNotices();
    this.m_goldAmount.gameObject.SetActive(false);
    this.m_quest = quest;
    if (this.m_quest.MaxProgress > 1)
    {
      this.m_progressText.Text = this.m_quest.Progress.ToString() + "/" + (object) this.m_quest.MaxProgress;
      this.m_progress.SetActive(true);
    }
    else
    {
      this.m_progressText.Text = string.Empty;
      this.m_progress.SetActive(false);
    }
    if (quest.IsLegendary)
    {
      this.m_tileRenderer.material = this.m_tileLegendaryMaterial;
      this.m_legendaryFX.SetActive(true);
    }
    this.m_questName.Text = quest.Name;
    RewardUtils.SetQuestTileNameLinePosition(this.m_nameLine, this.m_questName, 0.22f);
    this.m_requirement.Text = quest.Description;
    this.LoadCenterImage();
  }

  public void SetCanShowCancelButton(bool canShowCancel)
  {
    this.m_canShowCancelButton = canShowCancel;
    this.UpdateCancelButtonVisibility();
  }

  public void UpdateCancelButtonVisibility()
  {
    bool flag = false;
    if (this.m_canShowCancelButton && this.m_quest != null)
      flag = AchieveManager.Get().CanCancelQuest(this.m_quest.ID);
    this.m_cancelButtonRoot.gameObject.SetActive(flag);
  }

  public int GetQuestID()
  {
    if (this.m_quest == null)
      return 0;
    return this.m_quest.ID;
  }

  public void PlayBirth()
  {
    this.m_BurnPlaymaker.SendEvent("Birth");
  }

  public void CompleteAndAutoDestroyQuest()
  {
    if (this.m_quest == null || !this.m_quest.AutoDestroy)
      return;
    this.m_BurnPlaymaker.SendEvent("Death");
    AchieveManager.Get().CompleteAutoDestroyAchieve(this.m_quest.ID);
  }

  private void ReplaceQuest()
  {
    int linkToId = this.m_quest.LinkToId;
    if (linkToId == 0)
      return;
    this.SetupTile(AchieveManager.Get().GetAchievement(linkToId));
    this.m_BurnPlaymaker.SendEvent("Birth");
  }

  private void LoadCenterImage()
  {
    if (this.m_quest.Rewards == null || this.m_quest.Rewards.Count == 0 || this.m_quest.Rewards[0] == null)
    {
      Debug.LogError((object) "QuestTile.LoadCenterImage() - This quest doesn't grant a reward!!!");
    }
    else
    {
      this.m_rewardIcon.transform.localPosition = this.m_defaultBone.transform.localPosition;
      float amountToScaleReward;
      RewardUtils.SetRewardMaterialOffset(this.m_quest.Rewards, this.m_rewardIcon.GetComponent<Renderer>().material, this.m_goldAmount, out amountToScaleReward);
      this.m_rewardIcon.transform.localScale *= amountToScaleReward;
    }
  }

  private void OnCancelButtonReleased(UIEvent e)
  {
    if (this.m_quest == null)
      return;
    AchieveManager.Get().CancelQuest(this.m_quest.ID);
    this.m_BurnPlaymaker.SendEvent("Death");
  }
}
