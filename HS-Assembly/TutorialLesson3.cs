﻿// Decompiled with JetBrains decompiler
// Type: TutorialLesson3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TutorialLesson3 : MonoBehaviour
{
  public UberText m_attacker;
  public UberText m_defender;

  private void Awake()
  {
    this.m_attacker.SetGameStringText("GLOBAL_TUTORIAL_ATTACKER");
    this.m_defender.SetGameStringText("GLOBAL_TUTORIAL_DEFENDER");
  }
}
