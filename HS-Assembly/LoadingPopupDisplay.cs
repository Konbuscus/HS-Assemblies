﻿// Decompiled with JetBrains decompiler
// Type: LoadingPopupDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class LoadingPopupDisplay : TransitionPopup
{
  public static readonly Vector3 START_POS = new Vector3(-0.0152f, -0.0894f, -0.0837f);
  public static readonly Vector3 MID_POS = new Vector3(-0.0152f, -0.0894f, 0.0226f);
  public static readonly Vector3 END_POS = new Vector3(-0.0152f, 0.0368f, 0.0226f);
  public static readonly Vector3 OFFSCREEN_POS = new Vector3(-0.0152f, -0.0894f, 0.13f);
  public List<LoadingPopupDisplay.LoadingbarTexture> m_barTextures = new List<LoadingPopupDisplay.LoadingbarTexture>();
  private Map<AdventureDbId, List<string>> m_taskNameMap = new Map<AdventureDbId, List<string>>();
  private List<string> m_spectatorTaskNameMap = new List<string>();
  private const int TASK_DURATION_VARIATION = 2;
  private const float ROTATION_DURATION = 0.5f;
  private const float ROTATION_DELAY = 0.5f;
  private const float SLIDE_IN_TIME = 0.5f;
  private const float SLIDE_OUT_TIME = 0.25f;
  private const float RAISE_TIME = 0.5f;
  private const float LOWER_TIME = 0.25f;
  private const string SHOW_CANCEL_BUTTON_TWEEN_NAME = "ShowCancelButton";
  private const float SHOW_CANCEL_BUTTON_THRESHOLD = 30f;
  public UberText m_tipOfTheDay;
  public ProgressBar m_progressBar;
  public GameObject m_loadingTile;
  public GameObject m_cancelButtonParent;
  private bool m_stopAnimating;
  private bool m_animationStopped;
  private AudioSource m_loopSound;
  private bool m_barAnimating;

  protected override void Awake()
  {
    base.Awake();
    this.GenerateTaskNameMap();
    this.m_title.Text = GameStrings.Get("GLUE_STARTING_GAME");
    this.gameObject.transform.localPosition = new Vector3(-0.05f, 9f, 3.908f);
    SoundManager.Get().Load("StartGame_window_expand_up");
    SoundManager.Get().Load("StartGame_window_shrink_down");
    SoundManager.Get().Load("StartGame_window_loading_bar_move_down_and_forward");
    SoundManager.Get().Load("StartGame_window_loading_bar_flip");
    SoundManager.Get().Load("StartGame_window_bar_filling_loop");
    SoundManager.Get().Load("StartGame_window_loading_bar_drop");
    this.DisableCancelButton();
  }

  protected override void OnDestroy()
  {
    base.OnDestroy();
    if (!((UnityEngine.Object) SoundManager.Get() != (UnityEngine.Object) null))
      return;
    this.StopLoopingSound();
  }

  protected override bool EnableCancelButtonIfPossible()
  {
    if (!base.EnableCancelButtonIfPossible())
      return false;
    TransformUtil.SetLocalPosX((Component) this.m_queueTab, -0.3234057f);
    return true;
  }

  protected override void EnableCancelButton()
  {
    this.m_cancelButtonParent.SetActive(true);
    base.EnableCancelButton();
  }

  protected override void DisableCancelButton()
  {
    base.DisableCancelButton();
    this.m_cancelButtonParent.SetActive(false);
  }

  protected override void AnimateShow()
  {
    iTween.Timer(this.gameObject, iTween.Hash((object) "name", (object) "ShowCancelButton", (object) "time", (object) 30f, (object) "ignoretimescale", (object) true, (object) "oncomplete", (object) new Action<object>(this.OnCancelButtonShowTimerCompleted), (object) "oncompletetarget", (object) this.gameObject));
    this.SetTipOfTheDay();
    this.SetLoadingBarTexture();
    SoundManager.Get().LoadAndPlay("StartGame_window_expand_up");
    base.AnimateShow();
    this.m_stopAnimating = false;
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  protected void OnCancelButtonShowTimerCompleted(object userData)
  {
    this.EnableCancelButtonIfPossible();
  }

  protected override void OnGameError(FindGameEventData eventData)
  {
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  protected override void OnGameCanceled(FindGameEventData eventData)
  {
    Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.OnNavigateBack));
  }

  protected override void AnimateHide()
  {
    SoundManager.Get().LoadAndPlay("StartGame_window_shrink_down");
    iTween.StopByName(this.gameObject, "ShowCancelButton");
    if (this.m_barAnimating)
    {
      this.StopCoroutine("AnimateBar");
      this.m_barAnimating = false;
      this.StopLoopingSound();
    }
    base.AnimateHide();
  }

  protected override void OnAnimateShowFinished()
  {
    base.OnAnimateShowFinished();
    this.AnimateInLoadingTile();
  }

  private void AnimateInLoadingTile()
  {
    if (this.m_stopAnimating)
    {
      this.m_animationStopped = true;
    }
    else
    {
      this.m_loadingTile.transform.localEulerAngles = new Vector3(180f, 0.0f, 0.0f);
      this.m_loadingTile.transform.localPosition = LoadingPopupDisplay.START_POS;
      this.m_progressBar.SetProgressBar(0.0f);
      iTween.MoveTo(this.m_loadingTile, iTween.Hash((object) "position", (object) LoadingPopupDisplay.MID_POS, (object) "isLocal", (object) true, (object) "time", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.easeOutBounce));
      SoundManager.Get().LoadAndPlay("StartGame_window_loading_bar_move_down_and_forward");
      iTween.MoveTo(this.m_loadingTile, iTween.Hash((object) "position", (object) LoadingPopupDisplay.END_POS, (object) "isLocal", (object) true, (object) "time", (object) 0.5f, (object) "delay", (object) 0.5f, (object) "easetype", (object) iTween.EaseType.easeOutCubic));
      iTween.RotateAdd(this.m_loadingTile, iTween.Hash((object) "amount", (object) new Vector3(180f, 0.0f, 0.0f), (object) "time", (object) 0.5f, (object) "delay", (object) 0.8f, (object) "easeType", (object) iTween.EaseType.easeOutElastic, (object) "space", (object) Space.Self, (object) "name", (object) "flip"));
      this.m_progressBar.SetLabel(this.GetRandomTaskName());
      this.StartCoroutine("AnimateBar");
    }
  }

  private void AnimateOutLoadingTile()
  {
    iTween.MoveTo(this.m_loadingTile, iTween.Hash((object) "position", (object) LoadingPopupDisplay.MID_POS, (object) "isLocal", (object) true, (object) "time", (object) 0.25f, (object) "easetype", (object) iTween.EaseType.easeOutBounce));
    SoundManager.Get().LoadAndPlay("StartGame_window_loading_bar_drop");
    iTween.MoveTo(this.m_loadingTile, iTween.Hash((object) "position", (object) LoadingPopupDisplay.OFFSCREEN_POS, (object) "isLocal", (object) true, (object) "time", (object) 0.25f, (object) "delay", (object) 0.25f, (object) "easetype", (object) iTween.EaseType.easeOutCubic, (object) "oncomplete", (object) "AnimateInLoadingTile", (object) "oncompletetarget", (object) this.gameObject));
  }

  private float GetRandomTaskDuration()
  {
    return (float) (1.0 + (double) UnityEngine.Random.value * 2.0);
  }

  private string GetRandomTaskName()
  {
    List<string> stringList;
    if (GameMgr.Get().IsSpectator())
      stringList = this.m_spectatorTaskNameMap;
    else if (!this.m_taskNameMap.TryGetValue(this.m_adventureId, out stringList))
      stringList = this.m_taskNameMap[AdventureDbId.INVALID];
    if (stringList.Count == 0)
      return "ERROR - OUT OF TASK NAMES!!!";
    int index = UnityEngine.Random.Range(0, stringList.Count);
    return stringList[index];
  }

  [DebuggerHidden]
  private IEnumerator AnimateBar()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LoadingPopupDisplay.\u003CAnimateBar\u003Ec__IteratorF2() { \u003C\u003Ef__this = this };
  }

  private void LoopingSoundLoadedCallback(AudioSource source, object userData)
  {
    this.StopLoopingSound();
    if (this.m_barAnimating)
      this.m_loopSound = source;
    else
      SoundManager.Get().Stop(source);
  }

  protected override void OnGameplaySceneLoaded()
  {
    this.StartCoroutine(this.StopLoading());
    Navigation.Clear();
  }

  [DebuggerHidden]
  private IEnumerator StopLoading()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LoadingPopupDisplay.\u003CStopLoading\u003Ec__IteratorF3() { \u003C\u003Ef__this = this };
  }

  private void StopLoopingSound()
  {
    SoundManager.Get().Stop(this.m_loopSound);
    this.m_loopSound = (AudioSource) null;
  }

  private bool OnNavigateBack()
  {
    if (!this.m_cancelButtonParent.gameObject.activeSelf)
      return false;
    this.StartCoroutine(this.StopLoading());
    this.FireMatchCanceledEvent();
    return true;
  }

  protected override void OnCancelButtonReleased(UIEvent e)
  {
    base.OnCancelButtonReleased(e);
    Navigation.GoBack();
  }

  private void GenerateTaskNameMap()
  {
    this.GenerateTaskNamesForAdventure(AdventureDbId.INVALID, "GLUE_LOADING_BAR_TASK_");
    this.GenerateTaskNamesForAdventure(AdventureDbId.NAXXRAMAS, "GLUE_NAXX_LOADING_BAR_TASK_");
    this.GenerateTaskNamesForAdventure(AdventureDbId.BRM, "GLUE_BRM_LOADING_BAR_TASK_");
    this.GenerateTaskNamesForAdventure(AdventureDbId.LOE, "GLUE_LOE_LOADING_BAR_TASK_");
    this.GenerateTaskNamesForAdventure(AdventureDbId.KARA, "GLUE_KARA_LOADING_BAR_TASK_");
    this.GenerateTaskNamesForPrefix(this.m_spectatorTaskNameMap, "GLUE_SPECTATOR_LOADING_BAR_TASK_");
  }

  private void GenerateTaskNamesForAdventure(AdventureDbId adventureId, string prefix)
  {
    List<string> taskNames = new List<string>();
    this.GenerateTaskNamesForPrefix(taskNames, prefix);
    this.m_taskNameMap[adventureId] = taskNames;
  }

  private void GenerateTaskNamesForPrefix(List<string> taskNames, string prefix)
  {
    taskNames.Clear();
    for (int index = 1; index < 100; ++index)
    {
      string key = prefix + (object) index;
      string str = GameStrings.Get(key);
      if (str == key)
        break;
      taskNames.Add(str);
    }
  }

  private void SetTipOfTheDay()
  {
    if (this.m_adventureId == AdventureDbId.PRACTICE)
      this.m_tipOfTheDay.Text = GameStrings.GetTip(TipCategory.PRACTICE, Options.Get().GetInt(Option.TIP_PRACTICE_PROGRESS, 0), TipCategory.DEFAULT);
    else if (GameUtils.IsExpansionAdventure(this.m_adventureId))
      this.m_tipOfTheDay.Text = GameStrings.GetRandomTip(TipCategory.ADVENTURE);
    else
      this.m_tipOfTheDay.Text = GameStrings.GetRandomTip(TipCategory.DEFAULT);
  }

  private void SetLoadingBarTexture()
  {
    Texture texture = this.m_barTextures[0].texture;
    using (List<LoadingPopupDisplay.LoadingbarTexture>.Enumerator enumerator = this.m_barTextures.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LoadingPopupDisplay.LoadingbarTexture current = enumerator.Current;
        if (current.adventureID == this.m_adventureId)
        {
          texture = current.texture;
          this.m_progressBar.m_barIntensity = current.m_barIntensity;
          this.m_progressBar.m_barIntensityIncreaseMax = current.m_barIntensityIncreaseMax;
        }
      }
    }
    this.m_progressBar.SetBarTexture(texture);
  }

  [Serializable]
  public class LoadingbarTexture
  {
    public float m_barIntensity = 1.2f;
    public float m_barIntensityIncreaseMax = 3f;
    public AdventureDbId adventureID;
    public Texture texture;
  }
}
