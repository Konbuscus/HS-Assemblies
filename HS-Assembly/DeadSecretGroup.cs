﻿// Decompiled with JetBrains decompiler
// Type: DeadSecretGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DeadSecretGroup
{
  private List<Card> m_cards = new List<Card>();
  private Card m_mainCard;

  public Card GetMainCard()
  {
    return this.m_mainCard;
  }

  public void SetMainCard(Card card)
  {
    this.m_mainCard = card;
  }

  public List<Card> GetCards()
  {
    return this.m_cards;
  }

  public void AddCard(Card card)
  {
    this.m_cards.Add(card);
  }
}
