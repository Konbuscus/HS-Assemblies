﻿// Decompiled with JetBrains decompiler
// Type: FourCC
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

[Serializable]
public class FourCC
{
  protected uint m_value;

  public FourCC()
  {
  }

  public FourCC(uint value)
  {
    this.m_value = value;
  }

  public FourCC(string stringVal)
  {
    this.SetString(stringVal);
  }

  public static implicit operator FourCC(uint val)
  {
    return new FourCC(val);
  }

  public static bool operator ==(uint val, FourCC fourCC)
  {
    if (fourCC == (FourCC) null)
      return false;
    return (int) val == (int) fourCC.m_value;
  }

  public static bool operator ==(FourCC fourCC, uint val)
  {
    if (fourCC == (FourCC) null)
      return false;
    return (int) fourCC.m_value == (int) val;
  }

  public static bool operator !=(uint val, FourCC fourCC)
  {
    return !(val == fourCC);
  }

  public static bool operator !=(FourCC fourCC, uint val)
  {
    return !(fourCC == val);
  }

  public static bool operator ==(FourCC a, FourCC b)
  {
    if (object.ReferenceEquals((object) a, (object) b))
      return true;
    if ((object) a == null || (object) b == null)
      return false;
    return (int) a.m_value == (int) b.m_value;
  }

  public static bool operator !=(FourCC a, FourCC b)
  {
    return !(a == b);
  }

  public FourCC Clone()
  {
    FourCC fourCc = new FourCC();
    fourCc.CopyFrom(this);
    return fourCc;
  }

  public uint GetValue()
  {
    return this.m_value;
  }

  public void SetValue(uint val)
  {
    this.m_value = val;
  }

  public string GetString()
  {
    StringBuilder stringBuilder = new StringBuilder(4);
    int num = 24;
    while (num >= 0)
    {
      char ch = (char) (this.m_value >> num & (uint) byte.MaxValue);
      if ((int) ch != 0)
        stringBuilder.Append(ch);
      num -= 8;
    }
    return stringBuilder.ToString();
  }

  public void SetString(string str)
  {
    this.m_value = 0U;
    for (int index = 0; index < str.Length && index < 4; ++index)
      this.m_value = this.m_value << 8 | (uint) (byte) str[index];
  }

  public void CopyFrom(FourCC other)
  {
    this.m_value = other.m_value;
  }

  public override bool Equals(object obj)
  {
    if (obj == null)
      return false;
    FourCC fourCc = obj as FourCC;
    if ((object) fourCc == null)
      return false;
    return (int) this.m_value == (int) fourCc.m_value;
  }

  public bool Equals(FourCC other)
  {
    if ((object) other == null)
      return false;
    return (int) this.m_value == (int) other.m_value;
  }

  public override int GetHashCode()
  {
    return this.m_value.GetHashCode();
  }

  public override string ToString()
  {
    return this.GetString();
  }
}
