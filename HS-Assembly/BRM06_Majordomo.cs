﻿// Decompiled with JetBrains decompiler
// Type: BRM06_Majordomo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;

public class BRM06_Majordomo : BRM_MissionEntity
{
  public override void PreloadAssets()
  {
    this.PreloadSound("VO_BRMA06_1_RESPONSE_03");
    this.PreloadSound("VO_BRMA06_3_RESPONSE_03");
    this.PreloadSound("VO_BRMA06_1_DEATH_04");
    this.PreloadSound("VO_BRMA06_1_TURN1_02_ALT");
    this.PreloadSound("VO_BRMA06_1_SUMMON_RAG_05");
    this.PreloadSound("VO_BRMA06_3_INTRO_01");
    this.PreloadSound("VO_BRMA06_3_TURN1_02");
    this.PreloadSound("VO_NEFARIAN_MAJORDOMO_41");
  }

  protected override void PlayEmoteResponse(EmoteType emoteType, CardSoundSpell emoteSpell)
  {
    Actor actor = GameState.Get().GetOpposingSidePlayer().GetHeroCard().GetActor();
    switch (emoteType)
    {
      case EmoteType.GREETINGS:
      case EmoteType.WELL_PLAYED:
      case EmoteType.OOPS:
      case EmoteType.THREATEN:
      case EmoteType.THANKS:
      case EmoteType.SORRY:
        Entity hero = GameState.Get().GetOpposingSidePlayer().GetHero();
        if (hero.GetCardId() == "BRMA06_1" || hero.GetCardId() == "BRMA06_1H")
        {
          Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_BRMA06_1_RESPONSE_03", "VO_BRMA06_1_RESPONSE_03", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
          break;
        }
        if (!(hero.GetCardId() == "BRMA06_3") && !(hero.GetCardId() == "BRMA06_3H"))
          break;
        Gameplay.Get().StartCoroutine(this.PlaySoundAndBlockSpeech("VO_BRMA06_3_RESPONSE_03", "VO_BRMA06_3_RESPONSE_03", Notification.SpeechBubbleDirection.TopRight, actor, 1f, true, false));
        break;
    }
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM06_Majordomo.\u003CHandleMissionEventWithTiming\u003Ec__Iterator134() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM06_Majordomo.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator135() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM06_Majordomo.\u003CHandleGameOverWithTiming\u003Ec__Iterator136() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }

  [DebuggerHidden]
  public override IEnumerator PlayMissionIntroLineAndWait()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BRM06_Majordomo.\u003CPlayMissionIntroLineAndWait\u003Ec__Iterator137() { \u003C\u003Ef__this = this };
  }
}
