﻿// Decompiled with JetBrains decompiler
// Type: GraphicsManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using UnityEngine;

public class GraphicsManager : MonoBehaviour
{
  private const int ANDROID_MIN_DPI_HIGH_RES_TEXTURES = 180;
  private const int REDUCE_MAX_WINDOW_SIZE_X = 20;
  private const int REDUCE_MAX_WINDOW_SIZE_Y = 60;
  private const int MIN_WINDOW_WIDTH = 200;
  private const int MIN_WINDOW_HEIGHT = 200;
  private GraphicsQuality m_GraphicsQuality;
  private bool m_RealtimeShadows;
  private List<GameObject> m_DisableLowQualityObjects;
  private bool m_VeryLowQuality;
  private static GraphicsManager s_instance;
  private int m_lastWidth;
  private int m_lastHeight;
  private int m_winPosX;
  private int m_winPosY;

  public GraphicsQuality RenderQualityLevel
  {
    get
    {
      return this.m_GraphicsQuality;
    }
    set
    {
      this.m_GraphicsQuality = value;
      Options.Get().SetInt(Option.GFX_QUALITY, (int) this.m_GraphicsQuality);
      this.UpdateQualitySettings();
    }
  }

  public bool RealtimeShadows
  {
    get
    {
      return this.m_RealtimeShadows;
    }
  }

  private void Awake()
  {
    GraphicsManager.s_instance = this;
    this.m_DisableLowQualityObjects = new List<GameObject>();
    if (!Options.Get().HasOption(Option.GFX_QUALITY))
    {
      string intelDeviceName = W8Touch.Get().GetIntelDeviceName();
      Log.Yim.Print("Intel Device Name = {0}", (object) intelDeviceName);
      if (intelDeviceName != null && intelDeviceName.Contains("Haswell") && intelDeviceName.Contains("U28W"))
      {
        if (Screen.currentResolution.height > 1080)
          Options.Get().SetInt(Option.GFX_QUALITY, 0);
      }
      else if (intelDeviceName != null && intelDeviceName.Contains("Crystal-Well"))
        Options.Get().SetInt(Option.GFX_QUALITY, 2);
      else if (intelDeviceName != null && intelDeviceName.Contains("BayTrail"))
        Options.Get().SetInt(Option.GFX_QUALITY, 0);
    }
    this.m_GraphicsQuality = (GraphicsQuality) Options.Get().GetInt(Option.GFX_QUALITY);
    this.InitializeScreen();
    this.UpdateQualitySettings();
    this.m_lastWidth = Screen.width;
    this.m_lastHeight = Screen.height;
  }

  private void OnDestroy()
  {
    if (!Screen.fullScreen)
    {
      Options.Get().SetInt(Option.GFX_WIDTH, Screen.width);
      Options.Get().SetInt(Option.GFX_HEIGHT, Screen.height);
      int[] windowPosition = GraphicsManager.GetWindowPosition();
      Options.Get().SetInt(Option.GFX_WIN_POSX, windowPosition[0]);
      Options.Get().SetInt(Option.GFX_WIN_POSY, windowPosition[1]);
    }
    GraphicsManager.s_instance = (GraphicsManager) null;
  }

  private void Start()
  {
    Application.targetFrameRate = !Options.Get().HasOption(Option.GFX_TARGET_FRAME_RATE) ? 30 : Options.Get().GetInt(Option.GFX_TARGET_FRAME_RATE);
    this.LogSystemInfo();
  }

  private void Update()
  {
    if (Screen.fullScreen || this.m_lastWidth == Screen.width && this.m_lastHeight == Screen.height || (double) Screen.width / (double) Screen.height < 1.77777695655823 && (double) Screen.width / (double) Screen.height > 1.33333301544189)
      return;
    if (Screen.width < 200)
      ;
    int height = Screen.height;
    if (height < 200)
      height = 200;
    int width = (double) Screen.width / (double) Screen.height <= 1.77777695655823 ? (int) ((double) Screen.height * 1.33333301544189) : (int) ((double) Screen.height * 1.77777695655823);
    int[] windowPosition = GraphicsManager.GetWindowPosition();
    int x = windowPosition[0];
    int y = windowPosition[1];
    Screen.SetResolution(width, height, Screen.fullScreen);
    this.m_lastWidth = width;
    this.m_lastHeight = height;
    this.StartCoroutine(this.SetPos(x, y, 0.0f));
  }

  public static GraphicsManager Get()
  {
    return GraphicsManager.s_instance;
  }

  public void SetScreenResolution(int width, int height, bool fullscreen)
  {
    if (height > Screen.currentResolution.height - 60 && !fullscreen)
      height = Screen.currentResolution.height - 60;
    if (width > Screen.currentResolution.width - 20 && !fullscreen)
      width = Screen.currentResolution.width - 20;
    this.StartCoroutine(this.SetRes(width, height, fullscreen));
  }

  public void RegisterLowQualityDisableObject(GameObject lowQualityObject)
  {
    if (this.m_DisableLowQualityObjects.Contains(lowQualityObject))
      return;
    this.m_DisableLowQualityObjects.Add(lowQualityObject);
  }

  public void DeregisterLowQualityDisableObject(GameObject lowQualityObject)
  {
    if (!this.m_DisableLowQualityObjects.Contains(lowQualityObject))
      return;
    this.m_DisableLowQualityObjects.Remove(lowQualityObject);
  }

  public bool isVeryLowQualityDevice()
  {
    return this.m_VeryLowQuality;
  }

  public void SetVeryLowQuality(bool value)
  {
    this.m_VeryLowQuality = value;
  }

  [ContextMenu("Set Very Low Quality Toggle")]
  public void SetVeryLowQualityToggle()
  {
    this.SetVeryLowQuality(!this.m_VeryLowQuality);
  }

  private static void _GetLimits(ref GraphicsManager.GPULimits limits)
  {
    limits.highPrecisionBits = 16;
    limits.mediumPrecisionBits = 16;
    limits.lowPrecisionBits = 23;
    limits.maxFragmentTextureUnits = 16;
    limits.maxVertexTextureUnits = 16;
    limits.maxCombinedTextureUnits = 32;
    limits.maxTextureSize = 8192;
    limits.maxCubeMapSize = 8192;
    limits.maxRenderBufferSize = 8192;
    limits.maxFragmentUniforms = 256;
    limits.maxVertexUniforms = 256;
    limits.maxVaryings = 32;
    limits.maxVertexAttribs = 32;
  }

  public GraphicsManager.GPULimits GetGPULimits()
  {
    GraphicsManager.GPULimits limits = new GraphicsManager.GPULimits();
    GraphicsManager._GetLimits(ref limits);
    return limits;
  }

  private void InitializeScreen()
  {
    bool fullscreen = Options.Get().GetBool(Option.GFX_FULLSCREEN, Screen.fullScreen);
    int width;
    int height;
    if (fullscreen)
    {
      width = Options.Get().GetInt(Option.GFX_WIDTH, Screen.currentResolution.width);
      height = Options.Get().GetInt(Option.GFX_HEIGHT, Screen.currentResolution.height);
      if (!Options.Get().HasOption(Option.GFX_WIDTH) || !Options.Get().HasOption(Option.GFX_HEIGHT))
      {
        string intelDeviceName = W8Touch.Get().GetIntelDeviceName();
        if (intelDeviceName != null && (intelDeviceName.Contains("Haswell") && intelDeviceName.Contains("Y6W") || intelDeviceName.Contains("Haswell") && intelDeviceName.Contains("U15W")) && Screen.currentResolution.height >= 1080)
        {
          width = 1920;
          height = 1080;
        }
      }
      if (width == Screen.currentResolution.width && (height == Screen.currentResolution.height && fullscreen == Screen.fullScreen))
        return;
    }
    else
    {
      if (!Options.Get().HasOption(Option.GFX_WIDTH) || !Options.Get().HasOption(Option.GFX_HEIGHT))
        return;
      width = Options.Get().GetInt(Option.GFX_WIDTH);
      height = Options.Get().GetInt(Option.GFX_HEIGHT);
    }
    this.SetScreenResolution(width, height, fullscreen);
    if (fullscreen || !Options.Get().HasOption(Option.GFX_WIN_POSX) || !Options.Get().HasOption(Option.GFX_WIN_POSY))
      return;
    int x = Options.Get().GetInt(Option.GFX_WIN_POSX);
    int y = Options.Get().GetInt(Option.GFX_WIN_POSY);
    if (x < 0)
      x = 0;
    if (y < 0)
      y = 0;
    this.StartCoroutine(this.SetPos(x, y, 0.6f));
  }

  private void UpdateQualitySettings()
  {
    Log.Kyle.Print("GraphicsManager Update, Graphics Quality: " + this.m_GraphicsQuality.ToString());
    this.UpdateRenderQualitySettings();
    this.UpdateAntiAliasing();
  }

  [DllImport("user32.dll")]
  private static extern bool SetWindowPos(IntPtr hwnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

  [DllImport("user32.dll")]
  private static extern IntPtr FindWindow(string className, string windowName);

  private static bool SetWindowPosition(int x, int y, int resX = 0, int resY = 0)
  {
    IntPtr activeWindow = GraphicsManager.GetActiveWindow();
    IntPtr window = GraphicsManager.FindWindow((string) null, "Hearthstone");
    if (!(activeWindow == window))
      return false;
    GraphicsManager.SetWindowPos(activeWindow, 0, x, y, resX, resY, resX * resY != 0 ? 0 : 1);
    return true;
  }

  [DllImport("user32.dll")]
  private static extern IntPtr GetForegroundWindow();

  private static IntPtr GetActiveWindow()
  {
    return GraphicsManager.GetForegroundWindow();
  }

  [DllImport("user32.dll")]
  [return: MarshalAs(UnmanagedType.Bool)]
  private static extern bool GetWindowRect(IntPtr hWnd, out GraphicsManager.RECT lpRect);

  private static int[] GetWindowPosition()
  {
    int[] numArray = new int[2];
    GraphicsManager.RECT lpRect = new GraphicsManager.RECT();
    GraphicsManager.GetWindowRect(GraphicsManager.GetActiveWindow(), out lpRect);
    numArray[0] = lpRect.Left;
    numArray[1] = lpRect.Top;
    return numArray;
  }

  [DebuggerHidden]
  private IEnumerator SetRes(int width, int height, bool fullscreen)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GraphicsManager.\u003CSetRes\u003Ec__Iterator332() { width = width, height = height, fullscreen = fullscreen, \u003C\u0024\u003Ewidth = width, \u003C\u0024\u003Eheight = height, \u003C\u0024\u003Efullscreen = fullscreen, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator SetPos(int x, int y, float delay = 0.0f)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GraphicsManager.\u003CSetPos\u003Ec__Iterator333() { delay = delay, x = x, y = y, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003Ex = x, \u003C\u0024\u003Ey = y, \u003C\u003Ef__this = this };
  }

  private void UpdateAntiAliasing()
  {
    bool flag = false;
    int num = 0;
    if (this.m_GraphicsQuality == GraphicsQuality.Low)
    {
      num = 0;
      flag = false;
    }
    if (this.m_GraphicsQuality == GraphicsQuality.Medium)
    {
      num = 2;
      flag = false;
      if ((UnityEngine.Object) W8Touch.Get() != (UnityEngine.Object) null)
      {
        string intelDeviceName = W8Touch.Get().GetIntelDeviceName();
        if (intelDeviceName != null && (intelDeviceName.Equals("BayTrail") || intelDeviceName.Equals("Poulsbo") || intelDeviceName.Equals("CloverTrail") || intelDeviceName.Contains("Haswell") && intelDeviceName.Contains("Y6W")))
          num = 0;
      }
    }
    if (this.m_GraphicsQuality == GraphicsQuality.High)
    {
      switch (Localization.GetLocale())
      {
        case Locale.koKR:
        case Locale.ruRU:
        case Locale.zhTW:
        case Locale.zhCN:
        case Locale.plPL:
        case Locale.jaJP:
        case Locale.thTH:
          num = 2;
          flag = false;
          break;
        default:
          num = 0;
          flag = true;
          break;
      }
    }
    if (Options.Get().HasOption(Option.GFX_MSAA))
      num = Options.Get().GetInt(Option.GFX_MSAA);
    if (Options.Get().HasOption(Option.GFX_FXAA))
      flag = Options.Get().GetBool(Option.GFX_FXAA);
    if (flag)
      num = 0;
    if (num > 0)
      flag = false;
    QualitySettings.antiAliasing = num;
    foreach (Behaviour behaviour in UnityEngine.Object.FindObjectsOfType(typeof (FullScreenAntialiasing)) as FullScreenAntialiasing[])
      behaviour.enabled = flag;
  }

  private void UpdateRenderQualitySettings()
  {
    int num1 = 30;
    int num2 = 0;
    int num3 = 101;
    if (this.m_GraphicsQuality == GraphicsQuality.Low)
    {
      num1 = 30;
      num2 = 0;
      this.m_RealtimeShadows = false;
      this.SetQualityByName("Low");
      num3 = 101;
    }
    if (this.m_GraphicsQuality == GraphicsQuality.Medium)
    {
      num1 = 30;
      num2 = 0;
      this.m_RealtimeShadows = false;
      this.SetQualityByName("Medium");
      num3 = 201;
    }
    if (this.m_GraphicsQuality == GraphicsQuality.High)
    {
      num1 = 60;
      num2 = 1;
      this.m_RealtimeShadows = true;
      this.SetQualityByName("High");
      num3 = 301;
    }
    Shader.DisableKeyword("LOW_QUALITY");
    if (Options.Get().HasOption(Option.GFX_TARGET_FRAME_RATE))
    {
      Application.targetFrameRate = Options.Get().GetInt(Option.GFX_TARGET_FRAME_RATE);
    }
    else
    {
      if ((UnityEngine.Object) W8Touch.Get() != (UnityEngine.Object) null && W8Touch.Get().GetBatteryMode() == W8Touch.PowerSource.BatteryPower && num1 > 30)
      {
        Log.Yim.Print("Battery Mode Detected - Clamping Target Frame Rate from {0} to 30", (object) num1);
        num1 = 30;
        num2 = 0;
      }
      Application.targetFrameRate = num1;
    }
    QualitySettings.vSyncCount = !Options.Get().HasOption(Option.GFX_VSYNC) ? num2 : Options.Get().GetInt(Option.GFX_VSYNC);
    Log.Kyle.Print(string.Format("Target frame rate: {0}", (object) Application.targetFrameRate));
    foreach (Behaviour behaviour in UnityEngine.Object.FindObjectsOfType(typeof (ProjectedShadow)) as ProjectedShadow[])
      behaviour.enabled = !this.m_RealtimeShadows;
    foreach (RenderToTexture renderToTexture in UnityEngine.Object.FindObjectsOfType(typeof (RenderToTexture)) as RenderToTexture[])
      renderToTexture.ForceTextureRebuild();
    foreach (Shader shader in UnityEngine.Object.FindObjectsOfType(typeof (Shader)) as Shader[])
      shader.maximumLOD = num3;
    using (List<GameObject>.Enumerator enumerator = this.m_DisableLowQualityObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (!((UnityEngine.Object) current == (UnityEngine.Object) null))
        {
          if (this.m_GraphicsQuality == GraphicsQuality.Low)
          {
            Log.Kyle.Print(string.Format("Low Quality Disable: {0}", (object) current.name));
            current.SetActive(false);
          }
          else
          {
            Log.Kyle.Print(string.Format("Low Quality Enable: {0}", (object) current.name));
            current.SetActive(true);
          }
        }
      }
    }
    Shader.globalMaximumLOD = num3;
    this.SetScreenEffects();
  }

  private void SetScreenEffects()
  {
    if (!((UnityEngine.Object) ScreenEffectsMgr.Get() != (UnityEngine.Object) null))
      return;
    if (this.m_GraphicsQuality == GraphicsQuality.Low)
      ScreenEffectsMgr.Get().gameObject.SetActive(false);
    else
      ScreenEffectsMgr.Get().gameObject.SetActive(true);
  }

  private void SetQualityByName(string qualityName)
  {
    string[] names = QualitySettings.names;
    int index1 = -1;
    int index2;
    for (index2 = 0; index2 < names.Length; ++index2)
    {
      if (names[index2] == qualityName)
        index1 = index2;
    }
    if (index2 < 0)
      UnityEngine.Debug.LogError((object) string.Format("GraphicsManager: Quality Level not found: {0}", (object) qualityName));
    else
      QualitySettings.SetQualityLevel(index1, true);
  }

  private void LogSystemInfo()
  {
    UnityEngine.Debug.Log((object) "System Info:");
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Device Name: {0}", (object) SystemInfo.deviceName));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Device Model: {0}", (object) SystemInfo.deviceModel));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - OS: {0}", (object) SystemInfo.operatingSystem));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - CPU Type: {0}", (object) SystemInfo.processorType));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - CPU Cores: {0}", (object) SystemInfo.processorCount));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - System Memory: {0}", (object) SystemInfo.systemMemorySize));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Screen Resolution: {0}x{1}", (object) Screen.currentResolution.width, (object) Screen.currentResolution.height));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Screen DPI: {0}", (object) Screen.dpi));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - GPU ID: {0}", (object) SystemInfo.graphicsDeviceID));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - GPU Name: {0}", (object) SystemInfo.graphicsDeviceName));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - GPU Vendor: {0}", (object) SystemInfo.graphicsDeviceVendor));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - GPU Memory: {0}", (object) SystemInfo.graphicsMemorySize));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - GPU Shader Level: {0}", (object) SystemInfo.graphicsShaderLevel));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - GPU NPOT Support: {0}", (object) SystemInfo.npotSupport));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics API (version): {0}", (object) SystemInfo.graphicsDeviceVersion));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics API (type): {0}", (object) SystemInfo.graphicsDeviceType));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Supported Render Target Count: {0}", (object) SystemInfo.supportedRenderTargetCount));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Supports 3D Textures: {0}", (object) SystemInfo.supports3DTextures));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Supports Compute Shaders: {0}", (object) SystemInfo.supportsComputeShaders));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Supports Image Effects: {0}", (object) SystemInfo.supportsImageEffects));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Supports Render Textures: {0}", (object) SystemInfo.supportsRenderTextures));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Supports Render To Cubemap: {0}", (object) SystemInfo.supportsRenderToCubemap));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Supports Shadows: {0}", (object) SystemInfo.supportsShadows));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Supports Sparse Textures: {0}", (object) SystemInfo.supportsSparseTextures));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Supports Stencil: {0}", (object) SystemInfo.supportsStencil));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics RenderTextureFormat.ARGBHalf: {0}", (object) SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf)));
    UnityEngine.Debug.Log((object) string.Format("SystemInfo - Graphics Metal Support: {0}", (object) SystemInfo.graphicsDeviceVersion.StartsWith("Metal")));
  }

  private void LogGPULimits()
  {
    GraphicsManager.GPULimits gpuLimits = this.GetGPULimits();
    UnityEngine.Debug.Log((object) "GPU Limits:");
    UnityEngine.Debug.Log((object) string.Format("GPU - Fragment High Precision: {0}", (object) gpuLimits.highPrecisionBits));
    UnityEngine.Debug.Log((object) string.Format("GPU - Fragment Medium Precision: {0}", (object) gpuLimits.mediumPrecisionBits));
    UnityEngine.Debug.Log((object) string.Format("GPU - Fragment Low Precision: {0}", (object) gpuLimits.lowPrecisionBits));
    UnityEngine.Debug.Log((object) string.Format("GPU - Fragment Max Texture Units: {0}", (object) gpuLimits.maxFragmentTextureUnits));
    UnityEngine.Debug.Log((object) string.Format("GPU - Vertex Max Texture Units: {0}", (object) gpuLimits.maxVertexTextureUnits));
    UnityEngine.Debug.Log((object) string.Format("GPU - Combined Max Texture Units: {0}", (object) gpuLimits.maxCombinedTextureUnits));
    UnityEngine.Debug.Log((object) string.Format("GPU - Max Texture Size: {0}", (object) gpuLimits.maxTextureSize));
    UnityEngine.Debug.Log((object) string.Format("GPU - Max Cube-Map Texture Size: {0}", (object) gpuLimits.maxCubeMapSize));
    UnityEngine.Debug.Log((object) string.Format("GPU - Max Renderbuffer Size: {0}", (object) gpuLimits.maxRenderBufferSize));
    UnityEngine.Debug.Log((object) string.Format("GPU - Fragment Max Uniform Vectors: {0}", (object) gpuLimits.maxFragmentUniforms));
    UnityEngine.Debug.Log((object) string.Format("GPU - Vertex Max Uniform Vectors: {0}", (object) gpuLimits.maxVertexUniforms));
    UnityEngine.Debug.Log((object) string.Format("GPU - Max Varying Vectors: {0}", (object) gpuLimits.maxVaryings));
    UnityEngine.Debug.Log((object) string.Format("GPU - Vertex Max Attribs: {0}", (object) gpuLimits.maxVertexAttribs));
  }

  public struct GPULimits
  {
    public int highPrecisionBits;
    public int mediumPrecisionBits;
    public int lowPrecisionBits;
    public int maxFragmentTextureUnits;
    public int maxVertexTextureUnits;
    public int maxCombinedTextureUnits;
    public int maxTextureSize;
    public int maxCubeMapSize;
    public int maxRenderBufferSize;
    public int maxFragmentUniforms;
    public int maxVertexUniforms;
    public int maxVaryings;
    public int maxVertexAttribs;
  }

  private struct RECT
  {
    public int Left;
    public int Top;
    public int Right;
    public int Bottom;
  }
}
