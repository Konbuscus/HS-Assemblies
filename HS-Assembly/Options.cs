﻿// Decompiled with JetBrains decompiler
// Type: Options
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class Options
{
  private static readonly ServerOption[] s_serverFlagContainers = new ServerOption[10]{ ServerOption.FLAGS1, ServerOption.FLAGS2, ServerOption.FLAGS3, ServerOption.FLAGS4, ServerOption.FLAGS5, ServerOption.FLAGS6, ServerOption.FLAGS7, ServerOption.FLAGS8, ServerOption.FLAGS9, ServerOption.FLAGS10 };
  private Map<Option, List<Options.ChangedListener>> m_changedListeners = new Map<Option, List<Options.ChangedListener>>();
  private List<Options.ChangedListener> m_globalChangedListeners = new List<Options.ChangedListener>();
  private const string DEPRECATED_NAME_PREFIX = "DEPRECATED";
  private const string FLAG_NAME_PREFIX = "FLAGS";
  private const int FLAG_BIT_COUNT = 64;
  private const float RCP_FLAG_BIT_COUNT = 0.015625f;
  private static Options s_instance;
  private Map<Option, string> m_clientOptionMap;
  private Map<Option, ServerOption> m_serverOptionMap;
  private Map<Option, ServerOptionFlag> m_serverOptionFlagMap;

  public static Options Get()
  {
    if (Options.s_instance == null)
    {
      Options.s_instance = new Options();
      Options.s_instance.Initialize();
    }
    return Options.s_instance;
  }

  public bool IsClientOption(Option option)
  {
    return this.m_clientOptionMap.ContainsKey(option);
  }

  public bool IsServerOption(Option option)
  {
    return this.m_serverOptionMap.ContainsKey(option);
  }

  public bool IsServerOptionFlag(Option option)
  {
    return this.m_serverOptionFlagMap.ContainsKey(option);
  }

  public Map<Option, string> GetClientOptions()
  {
    return this.m_clientOptionMap;
  }

  public Map<Option, ServerOption> GetServerOptions()
  {
    return this.m_serverOptionMap;
  }

  public System.Type GetOptionType(Option option)
  {
    System.Type type;
    if (OptionDataTables.s_typeMap.TryGetValue(option, out type))
      return type;
    if (this.m_serverOptionFlagMap.ContainsKey(option))
      return typeof (bool);
    return (System.Type) null;
  }

  public System.Type GetServerOptionType(ServerOption serverOption)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Options.\u003CGetServerOptionType\u003Ec__AnonStorey44C typeCAnonStorey44C = new Options.\u003CGetServerOptionType\u003Ec__AnonStorey44C();
    // ISSUE: reference to a compiler-generated field
    typeCAnonStorey44C.serverOption = serverOption;
    // ISSUE: reference to a compiler-generated method
    if (Array.Exists<ServerOption>(Options.s_serverFlagContainers, new Predicate<ServerOption>(typeCAnonStorey44C.\u003C\u003Em__302)))
      return typeof (ulong);
    using (Map<Option, ServerOption>.Enumerator enumerator = this.m_serverOptionMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<Option, ServerOption> current = enumerator.Current;
        // ISSUE: reference to a compiler-generated field
        if (current.Value == typeCAnonStorey44C.serverOption)
        {
          Option key = current.Key;
          System.Type type;
          if (OptionDataTables.s_typeMap.TryGetValue(key, out type))
            return type;
          break;
        }
      }
    }
    return (System.Type) null;
  }

  public bool RegisterChangedListener(Option option, Options.ChangedCallback callback)
  {
    return this.RegisterChangedListener(option, callback, (object) null);
  }

  public bool RegisterChangedListener(Option option, Options.ChangedCallback callback, object userData)
  {
    Options.ChangedListener changedListener = new Options.ChangedListener();
    changedListener.SetCallback(callback);
    changedListener.SetUserData(userData);
    List<Options.ChangedListener> changedListenerList;
    if (!this.m_changedListeners.TryGetValue(option, out changedListenerList))
    {
      changedListenerList = new List<Options.ChangedListener>();
      this.m_changedListeners.Add(option, changedListenerList);
    }
    else if (changedListenerList.Contains(changedListener))
      return false;
    changedListenerList.Add(changedListener);
    return true;
  }

  public bool UnregisterChangedListener(Option option, Options.ChangedCallback callback)
  {
    return this.UnregisterChangedListener(option, callback, (object) null);
  }

  public bool UnregisterChangedListener(Option option, Options.ChangedCallback callback, object userData)
  {
    Options.ChangedListener changedListener = new Options.ChangedListener();
    changedListener.SetCallback(callback);
    changedListener.SetUserData(userData);
    List<Options.ChangedListener> changedListenerList;
    if (!this.m_changedListeners.TryGetValue(option, out changedListenerList) || !changedListenerList.Remove(changedListener))
      return false;
    if (changedListenerList.Count == 0)
      this.m_changedListeners.Remove(option);
    return true;
  }

  public bool RegisterGlobalChangedListener(Options.ChangedCallback callback)
  {
    return this.RegisterGlobalChangedListener(callback, (object) null);
  }

  public bool RegisterGlobalChangedListener(Options.ChangedCallback callback, object userData)
  {
    Options.ChangedListener changedListener = new Options.ChangedListener();
    changedListener.SetCallback(callback);
    changedListener.SetUserData(userData);
    if (this.m_globalChangedListeners.Contains(changedListener))
      return false;
    this.m_globalChangedListeners.Add(changedListener);
    return true;
  }

  public bool UnregisterGlobalChangedListener(Options.ChangedCallback callback)
  {
    return this.UnregisterGlobalChangedListener(callback, (object) null);
  }

  public bool UnregisterGlobalChangedListener(Options.ChangedCallback callback, object userData)
  {
    Options.ChangedListener changedListener = new Options.ChangedListener();
    changedListener.SetCallback(callback);
    changedListener.SetUserData(userData);
    return this.m_globalChangedListeners.Remove(changedListener);
  }

  public bool HasOption(Option option)
  {
    string key;
    if (this.m_clientOptionMap.TryGetValue(option, out key))
      return LocalOptions.Get().Has(key);
    ServerOption type;
    if (this.m_serverOptionMap.TryGetValue(option, out type))
      return NetCache.Get().ClientOptionExists(type);
    ServerOptionFlag serverOptionFlag;
    if (this.m_serverOptionFlagMap.TryGetValue(option, out serverOptionFlag))
      return this.HasServerOptionFlag(serverOptionFlag);
    return false;
  }

  public void DeleteOption(Option option)
  {
    string optionName;
    if (this.m_clientOptionMap.TryGetValue(option, out optionName))
    {
      this.DeleteClientOption(option, optionName);
    }
    else
    {
      ServerOption serverOption;
      if (this.m_serverOptionMap.TryGetValue(option, out serverOption))
      {
        this.DeleteServerOption(option, serverOption);
      }
      else
      {
        ServerOptionFlag serverOptionFlag;
        if (!this.m_serverOptionFlagMap.TryGetValue(option, out serverOptionFlag))
          return;
        this.DeleteServerOptionFlag(option, serverOptionFlag);
      }
    }
  }

  public object GetOption(Option option)
  {
    object val;
    if (this.GetOptionImpl(option, out val))
      return val;
    object obj;
    if (OptionDataTables.s_defaultsMap.TryGetValue(option, out obj))
      return obj;
    return (object) null;
  }

  public object GetOption(Option option, object defaultVal)
  {
    object val;
    if (this.GetOptionImpl(option, out val))
      return val;
    return defaultVal;
  }

  public bool GetBool(Option option)
  {
    bool val;
    if (this.GetBoolImpl(option, out val))
      return val;
    object obj;
    if (OptionDataTables.s_defaultsMap.TryGetValue(option, out obj))
      return (bool) obj;
    return false;
  }

  public bool GetBool(Option option, bool defaultVal)
  {
    bool val;
    if (this.GetBoolImpl(option, out val))
      return val;
    return defaultVal;
  }

  public int GetInt(Option option)
  {
    int val;
    if (this.GetIntImpl(option, out val))
      return val;
    object obj;
    if (OptionDataTables.s_defaultsMap.TryGetValue(option, out obj))
      return (int) obj;
    return 0;
  }

  public int GetInt(Option option, int defaultVal)
  {
    int val;
    if (this.GetIntImpl(option, out val))
      return val;
    return defaultVal;
  }

  public long GetLong(Option option)
  {
    long val;
    if (this.GetLongImpl(option, out val))
      return val;
    object obj;
    if (OptionDataTables.s_defaultsMap.TryGetValue(option, out obj))
      return (long) obj;
    return 0;
  }

  public long GetLong(Option option, long defaultVal)
  {
    long val;
    if (this.GetLongImpl(option, out val))
      return val;
    return defaultVal;
  }

  public float GetFloat(Option option)
  {
    float val;
    if (this.GetFloatImpl(option, out val))
      return val;
    object obj;
    if (OptionDataTables.s_defaultsMap.TryGetValue(option, out obj))
      return (float) obj;
    return 0.0f;
  }

  public float GetFloat(Option option, float defaultVal)
  {
    float val;
    if (this.GetFloatImpl(option, out val))
      return val;
    return defaultVal;
  }

  public ulong GetULong(Option option)
  {
    ulong val;
    if (this.GetULongImpl(option, out val))
      return val;
    object obj;
    if (OptionDataTables.s_defaultsMap.TryGetValue(option, out obj))
      return (ulong) obj;
    return 0;
  }

  public ulong GetULong(Option option, ulong defaultVal)
  {
    ulong val;
    if (this.GetULongImpl(option, out val))
      return val;
    return defaultVal;
  }

  public string GetString(Option option)
  {
    string val;
    if (this.GetStringImpl(option, out val))
      return val;
    object obj;
    if (OptionDataTables.s_defaultsMap.TryGetValue(option, out obj))
      return (string) obj;
    return string.Empty;
  }

  public string GetString(Option option, string defaultVal)
  {
    string val;
    if (this.GetStringImpl(option, out val))
      return val;
    return defaultVal;
  }

  public T GetEnum<T>(Option option)
  {
    T val;
    object genericVal;
    if (this.GetEnumImpl<T>(option, out val) || OptionDataTables.s_defaultsMap.TryGetValue(option, out genericVal) && this.TranslateEnumVal<T>(option, genericVal, out val))
      return val;
    return default (T);
  }

  public T GetEnum<T>(Option option, T defaultVal)
  {
    T val;
    if (this.GetEnumImpl<T>(option, out val))
      return val;
    return defaultVal;
  }

  public void SetOption(Option option, object val)
  {
    System.Type optionType = this.GetOptionType(option);
    if (optionType == typeof (bool))
      this.SetBool(option, (bool) val);
    else if (optionType == typeof (int))
      this.SetInt(option, (int) val);
    else if (optionType == typeof (long))
      this.SetLong(option, (long) val);
    else if (optionType == typeof (float))
      this.SetFloat(option, (float) val);
    else if (optionType == typeof (string))
      this.SetString(option, (string) val);
    else if (optionType == typeof (ulong))
      this.SetULong(option, (ulong) val);
    else
      Error.AddDevFatal("Options.SetOption() - option {0} has unsupported underlying type {1}", (object) option, (object) optionType);
  }

  public void SetBool(Option option, bool val)
  {
    string key;
    if (this.m_clientOptionMap.TryGetValue(option, out key))
    {
      bool existed = LocalOptions.Get().Has(key);
      bool flag = LocalOptions.Get().GetBool(key);
      if (existed && flag == val)
        return;
      LocalOptions.Get().Set(key, (object) val);
      this.FireChangedEvent(option, (object) flag, existed);
    }
    else
    {
      ServerOptionFlag flag1;
      if (!this.m_serverOptionFlagMap.TryGetValue(option, out flag1))
        return;
      ServerOption container;
      ulong flagBit;
      ulong existenceBit;
      this.GetServerOptionFlagInfo(flag1, out container, out flagBit, out existenceBit);
      ulong ulongOption = NetCache.Get().GetULongOption(container);
      bool flag2 = ((long) ulongOption & (long) flagBit) != 0L;
      bool existed = ((long) ulongOption & (long) existenceBit) != 0L;
      if (existed && flag2 == val)
        return;
      ulong val1 = (!val ? ulongOption & ~flagBit : ulongOption | flagBit) | existenceBit;
      NetCache.Get().SetULongOption(container, val1);
      this.FireChangedEvent(option, (object) flag2, existed);
    }
  }

  public void SetInt(Option option, int val)
  {
    string key;
    if (this.m_clientOptionMap.TryGetValue(option, out key))
    {
      bool existed = LocalOptions.Get().Has(key);
      int num = LocalOptions.Get().GetInt(key);
      if (existed && num == val)
        return;
      LocalOptions.Get().Set(key, (object) val);
      this.FireChangedEvent(option, (object) num, existed);
    }
    else
    {
      ServerOption type;
      if (!this.m_serverOptionMap.TryGetValue(option, out type))
        return;
      int ret;
      bool intOption = NetCache.Get().GetIntOption(type, out ret);
      if (intOption && ret == val)
        return;
      NetCache.Get().SetIntOption(type, val);
      this.FireChangedEvent(option, (object) ret, intOption);
    }
  }

  public void SetLong(Option option, long val)
  {
    string key;
    if (this.m_clientOptionMap.TryGetValue(option, out key))
    {
      bool existed = LocalOptions.Get().Has(key);
      long num = LocalOptions.Get().GetLong(key);
      if (existed && num == val)
        return;
      LocalOptions.Get().Set(key, (object) val);
      this.FireChangedEvent(option, (object) num, existed);
    }
    else
    {
      ServerOption type;
      if (!this.m_serverOptionMap.TryGetValue(option, out type))
        return;
      long ret;
      bool longOption = NetCache.Get().GetLongOption(type, out ret);
      if (longOption && ret == val)
        return;
      NetCache.Get().SetLongOption(type, val);
      this.FireChangedEvent(option, (object) ret, longOption);
    }
  }

  public void SetFloat(Option option, float val)
  {
    string key;
    if (this.m_clientOptionMap.TryGetValue(option, out key))
    {
      bool existed = LocalOptions.Get().Has(key);
      float num = LocalOptions.Get().GetFloat(key);
      if (existed && (double) num == (double) val)
        return;
      LocalOptions.Get().Set(key, (object) val);
      this.FireChangedEvent(option, (object) num, existed);
    }
    else
    {
      ServerOption type;
      if (!this.m_serverOptionMap.TryGetValue(option, out type))
        return;
      float ret;
      bool floatOption = NetCache.Get().GetFloatOption(type, out ret);
      if (floatOption && (double) ret == (double) val)
        return;
      NetCache.Get().SetFloatOption(type, val);
      this.FireChangedEvent(option, (object) ret, floatOption);
    }
  }

  public void SetULong(Option option, ulong val)
  {
    string key;
    if (this.m_clientOptionMap.TryGetValue(option, out key))
    {
      bool existed = LocalOptions.Get().Has(key);
      ulong num = LocalOptions.Get().GetULong(key);
      if (existed && (long) num == (long) val)
        return;
      LocalOptions.Get().Set(key, (object) val);
      this.FireChangedEvent(option, (object) num, existed);
    }
    else
    {
      ServerOption type;
      if (!this.m_serverOptionMap.TryGetValue(option, out type))
        return;
      ulong ret;
      bool ulongOption = NetCache.Get().GetULongOption(type, out ret);
      if (ulongOption && (long) ret == (long) val)
        return;
      NetCache.Get().SetULongOption(type, val);
      this.FireChangedEvent(option, (object) ret, ulongOption);
    }
  }

  public void SetString(Option option, string val)
  {
    string key;
    if (!this.m_clientOptionMap.TryGetValue(option, out key))
      return;
    bool existed = LocalOptions.Get().Has(key);
    string str = LocalOptions.Get().GetString(key);
    if (existed && !(str != val))
      return;
    LocalOptions.Get().Set(key, (object) val);
    this.FireChangedEvent(option, (object) str, existed);
  }

  public void SetEnum<T>(Option option, T val)
  {
    if (!Enum.IsDefined(typeof (T), (object) val))
    {
      Error.AddDevFatal("Options.SetEnum() - {0} is not convertible to enum type {1} for option {2}", (object) val, (object) typeof (T), (object) option);
    }
    else
    {
      System.Type optionType = this.GetOptionType(option);
      if (optionType == typeof (int))
        this.SetInt(option, Convert.ToInt32((object) val));
      else if (optionType == typeof (long))
        this.SetLong(option, Convert.ToInt64((object) val));
      else
        Error.AddDevFatal("Options.SetEnum() - option {0} has unsupported underlying type {1}", (object) option, (object) optionType);
    }
  }

  private void Initialize()
  {
    Array values = Enum.GetValues(typeof (Option));
    Map<string, Option> options = new Map<string, Option>();
    foreach (int num in values)
    {
      Option option = (Option) num;
      if (option != Option.INVALID)
      {
        string key = option.ToString();
        options.Add(key, option);
      }
    }
    this.BuildClientOptionMap(options);
    this.BuildServerOptionMap(options);
    this.BuildServerOptionFlagMap(options);
  }

  private void BuildClientOptionMap(Map<string, Option> options)
  {
    this.m_clientOptionMap = new Map<Option, string>();
    foreach (int num in Enum.GetValues(typeof (ClientOption)))
    {
      ClientOption clientOption = (ClientOption) num;
      if (clientOption != ClientOption.INVALID)
      {
        string key = clientOption.ToString();
        Option option;
        if (!options.TryGetValue(key, out option))
        {
          Debug.LogError((object) string.Format("Options.BuildClientOptionMap() - ClientOption {0} is not mirrored in the Option enum", (object) clientOption));
        }
        else
        {
          System.Type type;
          if (!OptionDataTables.s_typeMap.TryGetValue(option, out type))
          {
            Debug.LogError((object) string.Format("Options.BuildClientOptionMap() - ClientOption {0} has no type. Please add its type to the type map.", (object) clientOption));
          }
          else
          {
            string str = EnumUtils.GetString<Option>(option);
            this.m_clientOptionMap.Add(option, str);
          }
        }
      }
    }
  }

  private void BuildServerOptionMap(Map<string, Option> options)
  {
    this.m_serverOptionMap = new Map<Option, ServerOption>();
    foreach (int num in Enum.GetValues(typeof (ServerOption)))
    {
      ServerOption serverOption = (ServerOption) num;
      switch (serverOption)
      {
        case ServerOption.INVALID:
        case ServerOption.LIMIT:
          continue;
        default:
          string key1 = serverOption.ToString();
          if (!key1.StartsWith("FLAGS") && !key1.StartsWith("DEPRECATED"))
          {
            Option key2;
            if (!options.TryGetValue(key1, out key2))
            {
              Debug.LogError((object) string.Format("Options.BuildServerOptionMap() - ServerOption {0} is not mirrored in the Option enum", (object) serverOption));
              continue;
            }
            System.Type type;
            if (!OptionDataTables.s_typeMap.TryGetValue(key2, out type))
            {
              Debug.LogError((object) string.Format("Options.BuildServerOptionMap() - ServerOption {0} has no type. Please add its type to the type map.", (object) serverOption));
              continue;
            }
            if (type == typeof (bool))
            {
              Debug.LogError((object) string.Format("Options.BuildServerOptionMap() - ServerOption {0} is a bool. You should convert it to a ServerOptionFlag.", (object) serverOption));
              continue;
            }
            this.m_serverOptionMap.Add(key2, serverOption);
            continue;
          }
          continue;
      }
    }
  }

  private void BuildServerOptionFlagMap(Map<string, Option> options)
  {
    this.m_serverOptionFlagMap = new Map<Option, ServerOptionFlag>();
    foreach (int num in Enum.GetValues(typeof (ServerOptionFlag)))
    {
      ServerOptionFlag serverOptionFlag = (ServerOptionFlag) num;
      if (serverOptionFlag != ServerOptionFlag.LIMIT)
      {
        string key1 = serverOptionFlag.ToString();
        if (!key1.StartsWith("DEPRECATED"))
        {
          Option key2;
          if (!options.TryGetValue(key1, out key2))
            Debug.LogError((object) string.Format("Options.BuildServerOptionFlagMap() - ServerOptionFlag {0} is not mirrored in the Option enum", (object) serverOptionFlag));
          else
            this.m_serverOptionFlagMap.Add(key2, serverOptionFlag);
        }
      }
    }
  }

  private void GetServerOptionFlagInfo(ServerOptionFlag flag, out ServerOption container, out ulong flagBit, out ulong existenceBit)
  {
    int num1 = 2 * (int) flag;
    int index = Mathf.FloorToInt((float) num1 * (1f / 64f));
    int num2 = num1 % 64;
    int num3 = 1 + num2;
    container = Options.s_serverFlagContainers[index];
    flagBit = (ulong) (1L << num2);
    existenceBit = (ulong) (1L << num3);
  }

  private bool HasServerOptionFlag(ServerOptionFlag serverOptionFlag)
  {
    ServerOption container;
    ulong flagBit;
    ulong existenceBit;
    this.GetServerOptionFlagInfo(serverOptionFlag, out container, out flagBit, out existenceBit);
    return ((long) NetCache.Get().GetULongOption(container) & (long) existenceBit) != 0L;
  }

  private void DeleteClientOption(Option option, string optionName)
  {
    if (!LocalOptions.Get().Has(optionName))
      return;
    object clientOption = this.GetClientOption(option, optionName);
    LocalOptions.Get().Delete(optionName);
    this.RemoveListeners(option, clientOption);
  }

  private void DeleteServerOption(Option option, ServerOption serverOption)
  {
    if (!NetCache.Get().ClientOptionExists(serverOption))
      return;
    object serverOption1 = this.GetServerOption(option, serverOption);
    NetCache.Get().DeleteClientOption(serverOption);
    this.RemoveListeners(option, serverOption1);
  }

  private void DeleteServerOptionFlag(Option option, ServerOptionFlag serverOptionFlag)
  {
    ServerOption container;
    ulong flagBit;
    ulong existenceBit;
    this.GetServerOptionFlagInfo(serverOptionFlag, out container, out flagBit, out existenceBit);
    ulong ulongOption = NetCache.Get().GetULongOption(container);
    if (((long) ulongOption & (long) existenceBit) == 0L)
      return;
    bool flag = ((long) ulongOption & (long) flagBit) != 0L;
    ulong val = ulongOption & ~existenceBit;
    NetCache.Get().SetULongOption(container, val);
    this.RemoveListeners(option, (object) flag);
  }

  private object GetClientOption(Option option, string optionName)
  {
    System.Type optionType = this.GetOptionType(option);
    if (optionType == typeof (bool))
      return (object) LocalOptions.Get().GetBool(optionName);
    if (optionType == typeof (int))
      return (object) LocalOptions.Get().GetInt(optionName);
    if (optionType == typeof (long))
      return (object) LocalOptions.Get().GetLong(optionName);
    if (optionType == typeof (ulong))
      return (object) LocalOptions.Get().GetULong(optionName);
    if (optionType == typeof (float))
      return (object) LocalOptions.Get().GetFloat(optionName);
    if (optionType == typeof (string))
      return (object) LocalOptions.Get().GetString(optionName);
    Error.AddDevFatal("Options.GetClientOption() - option {0} has unsupported underlying type {1}", (object) option, (object) optionType);
    return (object) null;
  }

  private object GetServerOption(Option option, ServerOption serverOption)
  {
    System.Type optionType = this.GetOptionType(option);
    if (optionType == typeof (int))
      return (object) NetCache.Get().GetIntOption(serverOption);
    if (optionType == typeof (long))
      return (object) NetCache.Get().GetLongOption(serverOption);
    if (optionType == typeof (float))
      return (object) NetCache.Get().GetFloatOption(serverOption);
    if (optionType == typeof (ulong))
      return (object) NetCache.Get().GetULongOption(serverOption);
    Error.AddDevFatal("Options.GetServerOption() - option {0} has unsupported underlying type {1}", (object) option, (object) optionType);
    return (object) null;
  }

  private bool GetOptionImpl(Option option, out object val)
  {
    val = (object) null;
    string str;
    if (this.m_clientOptionMap.TryGetValue(option, out str))
    {
      if (LocalOptions.Get().Has(str))
        val = this.GetClientOption(option, str);
    }
    else
    {
      ServerOption container;
      if (this.m_serverOptionMap.TryGetValue(option, out container))
      {
        if (NetCache.Get().ClientOptionExists(container))
          val = this.GetServerOption(option, container);
      }
      else
      {
        ServerOptionFlag flag;
        if (this.m_serverOptionFlagMap.TryGetValue(option, out flag))
        {
          ulong flagBit;
          ulong existenceBit;
          this.GetServerOptionFlagInfo(flag, out container, out flagBit, out existenceBit);
          ulong ulongOption = NetCache.Get().GetULongOption(container);
          if (((long) ulongOption & (long) existenceBit) != 0L)
            val = (object) (((long) ulongOption & (long) flagBit) != 0L);
        }
      }
    }
    return val != null;
  }

  private bool GetBoolImpl(Option option, out bool val)
  {
    val = false;
    object val1;
    if (!this.GetOptionImpl(option, out val1))
      return false;
    val = (bool) val1;
    return true;
  }

  private bool GetIntImpl(Option option, out int val)
  {
    val = 0;
    object val1;
    if (!this.GetOptionImpl(option, out val1))
      return false;
    val = (int) val1;
    return true;
  }

  private bool GetLongImpl(Option option, out long val)
  {
    val = 0L;
    object val1;
    if (!this.GetOptionImpl(option, out val1))
      return false;
    val = (long) val1;
    return true;
  }

  private bool GetFloatImpl(Option option, out float val)
  {
    val = 0.0f;
    object val1;
    if (!this.GetOptionImpl(option, out val1))
      return false;
    val = (float) val1;
    return true;
  }

  private bool GetULongImpl(Option option, out ulong val)
  {
    val = 0UL;
    object val1;
    if (!this.GetOptionImpl(option, out val1))
      return false;
    val = (ulong) val1;
    return true;
  }

  private bool GetStringImpl(Option option, out string val)
  {
    val = string.Empty;
    object val1;
    if (!this.GetOptionImpl(option, out val1))
      return false;
    val = (string) val1;
    return true;
  }

  private bool GetEnumImpl<T>(Option option, out T val)
  {
    val = default (T);
    object val1;
    if (this.GetOptionImpl(option, out val1))
      return this.TranslateEnumVal<T>(option, val1, out val);
    return false;
  }

  private bool TranslateEnumVal<T>(Option option, object genericVal, out T val)
  {
    val = default (T);
    System.Type type = typeof (T);
    try
    {
      val = (T) genericVal;
      return true;
    }
    catch (Exception ex)
    {
      Debug.LogError((object) string.Format("Options.TranslateEnumVal() - option {0} has value {1}, which cannot be converted to type {2}", (object) option, genericVal, (object) type));
      return false;
    }
  }

  private void RemoveListeners(Option option, object prevVal)
  {
    this.FireChangedEvent(option, prevVal, true);
    this.m_changedListeners.Remove(option);
  }

  private void FireChangedEvent(Option option, object prevVal, bool existed)
  {
    List<Options.ChangedListener> changedListenerList;
    if (this.m_changedListeners.TryGetValue(option, out changedListenerList))
    {
      foreach (Options.ChangedListener changedListener in changedListenerList.ToArray())
        changedListener.Fire(option, prevVal, existed);
    }
    foreach (Options.ChangedListener changedListener in this.m_globalChangedListeners.ToArray())
      changedListener.Fire(option, prevVal, existed);
  }

  private class ChangedListener : EventListener<Options.ChangedCallback>
  {
    public void Fire(Option option, object prevValue, bool didExist)
    {
      this.m_callback(option, prevValue, didExist, this.m_userData);
    }
  }

  public delegate void ChangedCallback(Option option, object prevValue, bool existed, object userData);
}
