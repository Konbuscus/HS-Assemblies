﻿// Decompiled with JetBrains decompiler
// Type: LOE01_Zinaar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LOE01_Zinaar : LOE_MissionEntity
{
  private bool m_wishMoreWishesLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOE_02_RESPONSE");
    this.PreloadSound("VO_LOE_02_WISH");
    this.PreloadSound("VO_LOE_02_START2");
    this.PreloadSound("VO_LOE_02_START3");
    this.PreloadSound("VO_LOE_02_TURN_6");
    this.PreloadSound("VO_LOE_ZINAAR_TURN_6_CARTOGRAPHER_2");
    this.PreloadSound("VO_LOE_02_TURN_6_2");
    this.PreloadSound("VO_LOE_ZINAAR_TURN_6_CARTOGRAPHER_2_ALT");
    this.PreloadSound("VO_LOE_02_TURN_10");
    this.PreloadSound("VO_LOE_ZINAAR_TURN_10_CARTOGRAPHER_2");
    this.PreloadSound("VO_LOE_02_WIN");
    this.PreloadSound("VO_LOE_02_MORE_WISHES");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_LOE_02_RESPONSE",
            m_stringTag = "VO_LOE_02_RESPONSE"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE01_Zinaar.\u003CHandleMissionEventWithTiming\u003Ec__Iterator15C() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE01_Zinaar.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator15D() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE01_Zinaar.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator15E() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE01_Zinaar.\u003CHandleGameOverWithTiming\u003Ec__Iterator15F() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
