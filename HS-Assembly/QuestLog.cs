﻿// Decompiled with JetBrains decompiler
// Type: QuestLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class QuestLog : UIBPopup
{
  [CustomEditField(Sections = "Aspect Ratio Positioning")]
  public float m_yPosRoot16to9 = 0.475f;
  public const int QUEST_LOG_MAX_COUNT = 3;
  public GameObject m_root;
  public UberText m_winsCountText;
  public UberText m_forgeRecordCountText;
  public UberText m_totalLevelsText;
  public Transform m_medalBone;
  public TournamentMedal m_medalPrefab;
  public Transform m_arenaMedalBone;
  public ArenaMedal m_arenaMedalPrefab;
  public PegUIElement m_offClickCatcher;
  public List<ClassProgressBar> m_classProgressBars;
  public List<ClassProgressInfo> m_classProgressInfos;
  public ClassProgressBar m_classProgressPrefab;
  public RankedRewardChest2D m_rewardChest;
  public GameObject m_questTilePrefab;
  public List<Transform> m_questBones;
  public UberText m_noQuestText;
  public UIBButton m_closeButton;
  [CustomEditField(Sections = "Aspect Ratio Positioning")]
  public float m_yPosRoot3to2;
  private List<QuestTile> m_currentQuests;
  private static QuestLog s_instance;
  private int m_justCanceledQuestID;
  private TournamentMedal m_currentMedal;
  private ArenaMedal m_arenaMedal;
  private Enum[] m_presencePrevStatus;

  private void Awake()
  {
    QuestLog.s_instance = this;
    this.m_presencePrevStatus = PresenceMgr.Get().GetStatus();
    PresenceMgr.Get().SetStatus((Enum) PresenceStatus.QUESTLOG);
    AchieveManager.Get().RegisterAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(this.OnAchievesUpdated), (object) null);
    if (!((UnityEngine.Object) this.m_closeButton != (UnityEngine.Object) null))
      return;
    this.m_closeButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCloseButtonReleased));
  }

  private void Start()
  {
    if ((UnityEngine.Object) this.m_classProgressPrefab != (UnityEngine.Object) null)
    {
      for (int index = 0; index < this.m_classProgressInfos.Count; ++index)
      {
        ClassProgressInfo classProgressInfo = this.m_classProgressInfos[index];
        TAG_CLASS tagClass = classProgressInfo.m_class;
        ClassProgressBar classProgressBar = (ClassProgressBar) GameUtils.Instantiate((Component) this.m_classProgressPrefab, classProgressInfo.m_bone, true);
        SceneUtils.SetLayer((Component) classProgressBar, classProgressInfo.m_bone.layer);
        TransformUtil.Identity((Component) classProgressBar.transform);
        classProgressBar.m_class = tagClass;
        classProgressBar.m_classIcon.GetComponent<Renderer>().material = classProgressInfo.m_iconMaterial;
        classProgressBar.Init();
        this.m_classProgressBars.Add(classProgressBar);
      }
    }
    this.m_offClickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnQuestLogCloseEvent));
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) ShownUIMgr.Get() != (UnityEngine.Object) null)
      ShownUIMgr.Get().ClearShownUI();
    if (AchieveManager.Get() != null)
    {
      AchieveManager.Get().RemoveAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(this.OnAchievesUpdated));
      AchieveManager.Get().RemoveQuestCanceledListener(new AchieveManager.AchieveCanceledCallback(this.OnQuestCanceled));
    }
    if (Network.IsRunning())
      PresenceMgr.Get().SetPrevStatus();
    QuestLog.s_instance = (QuestLog) null;
  }

  public static QuestLog Get()
  {
    return QuestLog.s_instance;
  }

  public override void Show()
  {
    AchieveManager.Get().RegisterQuestCanceledListener(new AchieveManager.AchieveCanceledCallback(this.OnQuestCanceled));
    this.UpdateData();
    FullScreenFXMgr.Get().StartStandardBlurVignette(0.1f);
    Navigation.Push(new Navigation.NavigateBackHandler(this.OnNavigateBack));
    base.Show();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.WIDTH);
  }

  protected override void Hide(bool animate)
  {
    if (this.m_presencePrevStatus == null)
      this.m_presencePrevStatus = new Enum[1]
      {
        (Enum) PresenceStatus.HUB
      };
    PresenceMgr.Get().SetStatus(this.m_presencePrevStatus);
    if ((UnityEngine.Object) ShownUIMgr.Get() != (UnityEngine.Object) null)
      ShownUIMgr.Get().ClearShownUI();
    this.DoHideAnimation(!animate, (UIBPopup.OnAnimationComplete) (() =>
    {
      AchieveManager.Get().RemoveQuestCanceledListener(new AchieveManager.AchieveCanceledCallback(this.OnQuestCanceled));
      this.DeleteQuests();
      FullScreenFXMgr.Get().EndStandardBlurVignette(0.1f, (FullScreenFXMgr.EffectListener) null);
      this.m_shown = false;
    }));
  }

  private void DeleteQuests()
  {
    if (this.m_currentQuests == null || this.m_currentQuests.Count == 0)
      return;
    for (int index = 0; index < this.m_currentQuests.Count; ++index)
    {
      if (!((UnityEngine.Object) this.m_currentQuests[index] == (UnityEngine.Object) null))
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_currentQuests[index].gameObject);
    }
  }

  private void OnQuestLogCloseEvent(UIEvent e)
  {
    Navigation.GoBack();
  }

  private bool OnNavigateBack()
  {
    this.Hide(true);
    return true;
  }

  private void UpdateData()
  {
    this.UpdateClassProgress();
    this.UpdateActiveQuests();
    this.UpdateCurrentMedal();
    this.UpdateBestArenaMedal();
    this.UpdateTotalWins();
  }

  private void UpdateTotalWins()
  {
    int num1 = 0;
    int num2 = 0;
    using (List<NetCache.PlayerRecord>.Enumerator enumerator = NetCache.Get().GetNetObject<NetCache.NetCachePlayerRecords>().Records.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NetCache.PlayerRecord current = enumerator.Current;
        if (current.Data == 0)
        {
          GameType recordType = current.RecordType;
          switch (recordType)
          {
            case GameType.GT_ARENA:
              num2 += current.Wins;
              continue;
            case GameType.GT_RANKED:
            case GameType.GT_CASUAL:
              num1 += current.Wins;
              continue;
            default:
              if (recordType == GameType.GT_TAVERNBRAWL)
                goto case GameType.GT_RANKED;
              else
                continue;
          }
        }
      }
    }
    this.m_winsCountText.Text = num1.ToString();
    this.m_forgeRecordCountText.Text = num2.ToString();
  }

  private void UpdateBestArenaMedal()
  {
    NetCache.NetCacheProfileProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>();
    if ((UnityEngine.Object) this.m_arenaMedal == (UnityEngine.Object) null)
    {
      this.m_arenaMedal = (ArenaMedal) GameUtils.Instantiate((Component) this.m_arenaMedalPrefab, this.m_arenaMedalBone.gameObject, true);
      SceneUtils.SetLayer((Component) this.m_arenaMedal, this.m_arenaMedalBone.gameObject.layer);
      this.m_arenaMedal.transform.localScale = Vector3.one;
    }
    if (netObject.LastForgeDate != 0L)
    {
      this.m_arenaMedal.gameObject.SetActive(true);
      this.m_arenaMedal.SetMedal(netObject.BestForgeWins);
    }
    else
      this.m_arenaMedal.gameObject.SetActive(false);
  }

  private void UpdateCurrentMedal()
  {
    NetCache.NetCacheMedalInfo netObject = NetCache.Get().GetNetObject<NetCache.NetCacheMedalInfo>();
    if ((UnityEngine.Object) this.m_currentMedal == (UnityEngine.Object) null)
    {
      this.m_currentMedal = (TournamentMedal) GameUtils.Instantiate((Component) this.m_medalPrefab, this.m_medalBone.gameObject, true);
      SceneUtils.SetLayer((Component) this.m_currentMedal, this.gameObject.layer);
      this.m_currentMedal.transform.localScale = Vector3.one;
    }
    this.m_currentMedal.SetMedal(netObject, false);
    this.m_currentMedal.SetFormat(this.m_currentMedal.IsBestCurrentRankWild());
    this.m_rewardChest.SetRank(26 - Math.Max(netObject.Standard.BestStarLevel, netObject.Wild.BestStarLevel));
  }

  private void UpdateClassProgress()
  {
    if (this.m_classProgressBars.Count == 0)
      return;
    int num = 0;
    List<Achievement> achievesInGroup = AchieveManager.Get().GetAchievesInGroup(Achievement.AchType.UNLOCK_HERO, true);
    NetCache.NetCacheHeroLevels netObject = NetCache.Get().GetNetObject<NetCache.NetCacheHeroLevels>();
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    QuestLog.\u003CUpdateClassProgress\u003Ec__AnonStorey3CF progressCAnonStorey3Cf = new QuestLog.\u003CUpdateClassProgress\u003Ec__AnonStorey3CF();
    using (List<ClassProgressBar>.Enumerator enumerator = this.m_classProgressBars.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        progressCAnonStorey3Cf.classProgress = enumerator.Current;
        // ISSUE: reference to a compiler-generated field
        if (!((UnityEngine.Object) progressCAnonStorey3Cf.classProgress.m_classLockedGO == (UnityEngine.Object) null))
        {
          // ISSUE: reference to a compiler-generated method
          if (achievesInGroup.Find(new Predicate<Achievement>(progressCAnonStorey3Cf.\u003C\u003Em__180)) != null)
          {
            // ISSUE: reference to a compiler-generated field
            progressCAnonStorey3Cf.classProgress.m_classLockedGO.SetActive(false);
            // ISSUE: reference to a compiler-generated method
            NetCache.HeroLevel heroLevel = netObject.Levels.Find(new Predicate<NetCache.HeroLevel>(progressCAnonStorey3Cf.\u003C\u003Em__181));
            // ISSUE: reference to a compiler-generated field
            progressCAnonStorey3Cf.classProgress.m_levelText.Text = heroLevel.CurrentLevel.Level.ToString();
            if (heroLevel.CurrentLevel.IsMaxLevel())
            {
              // ISSUE: reference to a compiler-generated field
              progressCAnonStorey3Cf.classProgress.m_progressBar.SetProgressBar(1f);
            }
            else
            {
              // ISSUE: reference to a compiler-generated field
              progressCAnonStorey3Cf.classProgress.m_progressBar.SetProgressBar((float) heroLevel.CurrentLevel.XP / (float) heroLevel.CurrentLevel.MaxXP);
            }
            // ISSUE: reference to a compiler-generated field
            progressCAnonStorey3Cf.classProgress.SetNextReward(heroLevel.NextReward);
            num += heroLevel.CurrentLevel.Level;
          }
          else
          {
            // ISSUE: reference to a compiler-generated field
            progressCAnonStorey3Cf.classProgress.m_levelText.Text = string.Empty;
            // ISSUE: reference to a compiler-generated field
            progressCAnonStorey3Cf.classProgress.m_classLockedGO.SetActive(true);
          }
        }
      }
    }
    if (!((UnityEngine.Object) this.m_totalLevelsText != (UnityEngine.Object) null))
      return;
    this.m_totalLevelsText.Text = string.Format(GameStrings.Get("GLUE_QUEST_LOG_TOTAL_LEVELS"), (object) num);
  }

  private void UpdateActiveQuests()
  {
    List<Achievement> activeQuests = AchieveManager.Get().GetActiveQuests(false);
    Log.Ben.Print(string.Format("Found {0} activeQuests! I should do something awesome with them here.", (object) activeQuests.Count));
    this.m_currentQuests = new List<QuestTile>();
    for (int slot = 0; slot < activeQuests.Count; ++slot)
    {
      if (slot < 3)
        this.AddCurrentQuestTile(activeQuests[slot], slot);
    }
    if (this.m_currentQuests.Count == 0)
    {
      this.m_noQuestText.gameObject.SetActive(true);
      if (AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.DAILY_QUESTS))
      {
        this.m_noQuestText.Text = GameStrings.Get("GLUE_QUEST_LOG_NO_QUESTS_DAILIES_UNLOCKED");
        if (Options.Get().GetBool(Option.HAS_RUN_OUT_OF_QUESTS, false) || !UserAttentionManager.CanShowAttentionGrabber("QuestLog.UpdateActiveQuests:" + (object) Option.HAS_RUN_OUT_OF_QUESTS))
          return;
        NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, new Vector3(155.3f, NotificationManager.DEPTH, 34.5f), GameStrings.Get("VO_INNKEEPER_OUT_OF_QUESTS"), "VO_INNKEEPER_OUT_OF_QUESTS", 0.0f, (Action) null, false);
        Options.Get().SetBool(Option.HAS_RUN_OUT_OF_QUESTS, true);
      }
      else
        this.m_noQuestText.Text = GameStrings.Get("GLUE_QUEST_LOG_NO_QUESTS");
    }
    else
      this.m_noQuestText.gameObject.SetActive(false);
  }

  private void AddCurrentQuestTile(Achievement achieveQuest, int slot)
  {
    GameObject go = (GameObject) GameUtils.Instantiate(this.m_questTilePrefab, this.m_questBones[slot].gameObject, true);
    SceneUtils.SetLayer(go, this.m_questBones[slot].gameObject.layer);
    go.transform.localScale = Vector3.one;
    QuestTile component = go.GetComponent<QuestTile>();
    component.SetupTile(achieveQuest);
    component.SetCanShowCancelButton(true);
    this.m_currentQuests.Add(component);
  }

  private void OnQuestCanceled(int achieveID, bool canceled, object userData)
  {
    if (!canceled)
      return;
    this.m_justCanceledQuestID = achieveID;
  }

  private void OnAchievesUpdated(List<Achievement> updatedAchieves, List<Achievement> completedAchieves, object userData)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    QuestLog.\u003COnAchievesUpdated\u003Ec__AnonStorey3D0 updatedCAnonStorey3D0 = new QuestLog.\u003COnAchievesUpdated\u003Ec__AnonStorey3D0();
    if (this.m_justCanceledQuestID == 0)
      return;
    List<Achievement> activeQuests = AchieveManager.Get().GetActiveQuests(true);
    if (activeQuests.Count <= 0)
      return;
    if (activeQuests.Count > 1 && !Vars.Key("Quests.CanCancelManyTimes").GetBool(false) && !Vars.Key("Quests.CancelGivesManyNewQuests").GetBool(false))
    {
      Debug.LogError((object) string.Format("QuestLog.OnActiveAchievesUpdated(): expecting ONE new active quest after a quest cancel but received {0}", (object) activeQuests.Count));
      this.Hide();
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      updatedCAnonStorey3D0.justCanceledQuest = this.m_justCanceledQuestID;
      this.m_justCanceledQuestID = 0;
      // ISSUE: reference to a compiler-generated method
      QuestTile questTile = this.m_currentQuests.Find(new Predicate<QuestTile>(updatedCAnonStorey3D0.\u003C\u003Em__182));
      if ((UnityEngine.Object) questTile == (UnityEngine.Object) null)
      {
        // ISSUE: reference to a compiler-generated field
        Debug.LogError((object) string.Format("QuestLog.OnActiveAchievesUpdated(): could not find tile for just canceled quest (quest ID {0})", (object) updatedCAnonStorey3D0.justCanceledQuest));
        this.Hide();
      }
      else
      {
        Log.Achievements.Print("Adding QuestLog tile for: {0}", (object) activeQuests[0]);
        questTile.SetupTile(activeQuests[0]);
        questTile.PlayBirth();
        for (int index = 1; index < activeQuests.Count; ++index)
        {
          int count = this.m_currentQuests.Count;
          if (count < this.m_questBones.Count)
            this.AddCurrentQuestTile(activeQuests[index], count);
          else
            break;
        }
        using (List<QuestTile>.Enumerator enumerator = this.m_currentQuests.GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.UpdateCancelButtonVisibility();
        }
      }
    }
  }

  private void OnCloseButtonReleased(UIEvent e)
  {
    this.OnNavigateBack();
  }
}
