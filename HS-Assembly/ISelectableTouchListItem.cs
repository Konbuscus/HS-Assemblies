﻿// Decompiled with JetBrains decompiler
// Type: ISelectableTouchListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public interface ISelectableTouchListItem : ITouchListItem
{
  bool Selectable { get; }

  bool IsSelected();

  void Selected();

  void Unselected();
}
