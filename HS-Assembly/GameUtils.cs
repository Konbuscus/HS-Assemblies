﻿// Decompiled with JetBrains decompiler
// Type: GameUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using PegasusGame;
using PegasusShared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class GameUtils
{
  private static KeyValuePair<float, HashSet<TAG_CARD_SET>> s_cachedRotatedCardSets = new KeyValuePair<float, HashSet<TAG_CARD_SET>>(0.0f, (HashSet<TAG_CARD_SET>) null);
  private static KeyValuePair<float, HashSet<int>> s_cachedRotatedIndividualCards = new KeyValuePair<float, HashSet<int>>(0.0f, (HashSet<int>) null);
  private static KeyValuePair<float, HashSet<AdventureDbId>> s_cachedRotatedAdventures = new KeyValuePair<float, HashSet<AdventureDbId>>(0.0f, (HashSet<AdventureDbId>) null);
  private static KeyValuePair<float, HashSet<BoosterDbId>> s_cachedRotatedBoosters = new KeyValuePair<float, HashSet<BoosterDbId>>(0.0f, (HashSet<BoosterDbId>) null);

  public static string TranslateDbIdToCardId(int dbId)
  {
    CardDbfRecord record = GameDbf.Card.GetRecord(dbId);
    if (record == null)
    {
      GameUtils.ShowDetailedCardError("GameUtils.TranslateDbIdToCardId() - Failed to find card with database id {0} in the Card DBF.", (object) dbId);
      return (string) null;
    }
    string noteMiniGuid = record.NoteMiniGuid;
    if (noteMiniGuid != null)
      return noteMiniGuid;
    GameUtils.ShowDetailedCardError("GameUtils.TranslateDbIdToCardId() - Card with database id {0} has no NOTE_MINI_GUID field in the Card DBF.", (object) dbId);
    return (string) null;
  }

  public static int TranslateCardIdToDbId(string cardId)
  {
    CardDbfRecord cardRecord = GameUtils.GetCardRecord(cardId);
    if (cardRecord != null)
      return cardRecord.ID;
    GameUtils.ShowDetailedCardError("GameUtils.TranslateCardIdToDbId() - There is no card with NOTE_MINI_GUID {0} in the Card DBF.", (object) cardId);
    return 0;
  }

  public static long CardUID(string cardStringId, TAG_PREMIUM premium)
  {
    return GameUtils.CardUID(GameUtils.TranslateCardIdToDbId(cardStringId), premium);
  }

  public static long CardUID(int cardDbId, TAG_PREMIUM premium)
  {
    return (premium != TAG_PREMIUM.GOLDEN ? 0L : 4294967296L) | (long) cardDbId;
  }

  public static long ClientCardUID(string cardStringId, TAG_PREMIUM premium, bool owned)
  {
    return GameUtils.ClientCardUID(GameUtils.TranslateCardIdToDbId(cardStringId), premium, owned);
  }

  public static long ClientCardUID(int cardDbId, TAG_PREMIUM premium, bool owned)
  {
    return GameUtils.CardUID(cardDbId, premium) | (!owned ? 0L : 8589934592L);
  }

  public static bool IsCardCollectible(string cardId)
  {
    return GameUtils.GetCardTagValue(cardId, GAME_TAG.COLLECTIBLE) == 1;
  }

  public static bool IsAnythingRotated()
  {
    return GameUtils.GetRotatedAdventures().Count > 0 || GameUtils.GetRotatedBoosters().Count > 0 || (GameUtils.GetRotatedSets().Count > 0 || GameUtils.GetRotatedIndividualCards().Count > 0);
  }

  public static HashSet<AdventureDbId> GetRotatedAdventures()
  {
    if (GameUtils.s_cachedRotatedAdventures.Value == null || (double) Time.realtimeSinceStartup >= (double) GameUtils.s_cachedRotatedAdventures.Key + 1.0)
    {
      HashSet<AdventureDbId> adventureDbIdSet = new HashSet<AdventureDbId>();
      SpecialEventManager specialEventManager = SpecialEventManager.Get();
      using (List<RotatedItemDbfRecord>.Enumerator enumerator = GameDbf.RotatedItem.GetRecords().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          RotatedItemDbfRecord current = enumerator.Current;
          AdventureDbId outVal;
          if (current.ItemType == 2 && EnumUtils.TryCast<AdventureDbId>((object) current.ItemId, out outVal) && specialEventManager.IsEventActive(current.RotationEvent, false))
            adventureDbIdSet.Add(outVal);
        }
      }
      GameUtils.s_cachedRotatedAdventures = new KeyValuePair<float, HashSet<AdventureDbId>>(Time.realtimeSinceStartup, adventureDbIdSet);
    }
    return GameUtils.s_cachedRotatedAdventures.Value;
  }

  public static bool IsAdventureRotated(AdventureDbId adventureID)
  {
    return GameUtils.GetRotatedAdventures().Contains(adventureID);
  }

  public static HashSet<BoosterDbId> GetRotatedBoosters()
  {
    if (GameUtils.s_cachedRotatedBoosters.Value == null || (double) Time.realtimeSinceStartup >= (double) GameUtils.s_cachedRotatedBoosters.Key + 1.0)
    {
      HashSet<BoosterDbId> boosterDbIdSet = new HashSet<BoosterDbId>();
      SpecialEventManager specialEventManager = SpecialEventManager.Get();
      using (List<RotatedItemDbfRecord>.Enumerator enumerator = GameDbf.RotatedItem.GetRecords().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          RotatedItemDbfRecord current = enumerator.Current;
          BoosterDbId outVal;
          if (current.ItemType == 1 && EnumUtils.TryCast<BoosterDbId>((object) current.ItemId, out outVal) && specialEventManager.IsEventActive(current.RotationEvent, false))
            boosterDbIdSet.Add(outVal);
        }
      }
      GameUtils.s_cachedRotatedBoosters = new KeyValuePair<float, HashSet<BoosterDbId>>(Time.realtimeSinceStartup, boosterDbIdSet);
    }
    return GameUtils.s_cachedRotatedBoosters.Value;
  }

  public static bool IsBoosterRotated(BoosterDbId boosterID)
  {
    return GameUtils.GetRotatedBoosters().Contains(boosterID);
  }

  public static HashSet<TAG_CARD_SET> GetRotatedSets()
  {
    if (GameUtils.s_cachedRotatedCardSets.Value == null || (double) Time.realtimeSinceStartup >= (double) GameUtils.s_cachedRotatedCardSets.Key + 1.0)
    {
      HashSet<TAG_CARD_SET> tagCardSetSet = new HashSet<TAG_CARD_SET>();
      SpecialEventManager specialEventManager = SpecialEventManager.Get();
      using (List<RotatedItemDbfRecord>.Enumerator enumerator = GameDbf.RotatedItem.GetRecords().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          RotatedItemDbfRecord current = enumerator.Current;
          switch (current.ItemType)
          {
            case 1:
            case 2:
            case 3:
              if (current.CardSetId != 0 && specialEventManager.IsEventActive(current.RotationEvent, false))
              {
                tagCardSetSet.Add((TAG_CARD_SET) current.CardSetId);
                continue;
              }
              continue;
            default:
              continue;
          }
        }
      }
      GameUtils.s_cachedRotatedCardSets = new KeyValuePair<float, HashSet<TAG_CARD_SET>>(Time.realtimeSinceStartup, tagCardSetSet);
    }
    return GameUtils.s_cachedRotatedCardSets.Value;
  }

  public static bool IsSetRotated(TAG_CARD_SET set)
  {
    return GameUtils.GetRotatedSets().Contains(set);
  }

  private static HashSet<int> GetRotatedIndividualCards()
  {
    if (GameUtils.s_cachedRotatedIndividualCards.Value == null || (double) Time.realtimeSinceStartup >= (double) GameUtils.s_cachedRotatedCardSets.Key + 1.0)
    {
      HashSet<int> intSet = new HashSet<int>();
      SpecialEventManager specialEventManager = SpecialEventManager.Get();
      using (List<RotatedItemDbfRecord>.Enumerator enumerator = GameDbf.RotatedItem.GetRecords().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          RotatedItemDbfRecord current = enumerator.Current;
          if (current.ItemType == 4 && current.CardId != 0 && specialEventManager.IsEventActive(current.RotationEvent, false))
            intSet.Add(current.CardId);
        }
      }
      GameUtils.s_cachedRotatedIndividualCards = new KeyValuePair<float, HashSet<int>>(Time.realtimeSinceStartup, intSet);
    }
    return GameUtils.s_cachedRotatedIndividualCards.Value;
  }

  public static bool IsSetAnAdventure(TAG_CARD_SET cardSet)
  {
    if (cardSet != TAG_CARD_SET.BRM && cardSet != TAG_CARD_SET.FP1)
      return cardSet == TAG_CARD_SET.LOE;
    return true;
  }

  public static bool IsCardRotated(string cardId)
  {
    return GameUtils.IsCardRotated(DefLoader.Get().GetEntityDef(cardId));
  }

  public static bool IsCardRotated(EntityDef def)
  {
    return GameUtils.IsSetRotated(def.GetTag<TAG_CARD_SET>(GAME_TAG.CARD_SET)) || GameUtils.GetRotatedIndividualCards().Contains(GameUtils.TranslateCardIdToDbId(def.GetCardId()));
  }

  public static TAG_CARD_SET[] GetStandardSets()
  {
    List<TAG_CARD_SET> tagCardSetList = new List<TAG_CARD_SET>();
    using (List<TAG_CARD_SET>.Enumerator enumerator = CollectionManager.Get().GetDisplayableCardSets().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TAG_CARD_SET current = enumerator.Current;
        if (!GameUtils.IsSetRotated(current))
          tagCardSetList.Add(current);
      }
    }
    return tagCardSetList.ToArray();
  }

  public static int CountAllCollectibleCards()
  {
    return GameDbf.GetIndex().GetCollectibleCardCount();
  }

  public static List<string> GetAllCardIds()
  {
    return GameDbf.GetIndex().GetAllCardIds();
  }

  public static List<string> GetAllCollectibleCardIds()
  {
    return GameDbf.GetIndex().GetCollectibleCardIds();
  }

  public static List<int> GetAllCollectibleCardDbIds()
  {
    return GameDbf.GetIndex().GetCollectibleCardDbIds();
  }

  public static int CountNonHeroCollectibleCards()
  {
    int num = 0;
    using (List<string>.Enumerator enumerator = GameUtils.GetAllCollectibleCardIds().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EntityDef entityDef = DefLoader.Get().GetEntityDef(enumerator.Current);
        if (entityDef != null && entityDef.GetCardType() != TAG_CARDTYPE.HERO)
          ++num;
      }
    }
    return num;
  }

  public static List<string> GetNonHeroCollectibleCardIds()
  {
    List<string> stringList = new List<string>();
    using (List<string>.Enumerator enumerator = GameUtils.GetAllCollectibleCardIds().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        EntityDef entityDef = DefLoader.Get().GetEntityDef(current);
        if (entityDef != null && entityDef.GetCardType() != TAG_CARDTYPE.HERO)
          stringList.Add(current);
      }
    }
    return stringList;
  }

  public static List<string> GetNonHeroAllCardIds()
  {
    List<string> stringList = new List<string>();
    using (List<string>.Enumerator enumerator = GameUtils.GetAllCardIds().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        EntityDef entityDef = DefLoader.Get().GetEntityDef(current);
        if (entityDef != null && entityDef.GetCardType() != TAG_CARDTYPE.HERO && (entityDef.GetCardType() != TAG_CARDTYPE.HERO_POWER && entityDef.GetCardType() != TAG_CARDTYPE.ENCHANTMENT))
          stringList.Add(current);
      }
    }
    return stringList;
  }

  public static CardDbfRecord GetCardRecord(string cardId)
  {
    if (cardId == null)
      return (CardDbfRecord) null;
    return GameDbf.GetIndex().GetCardRecord(cardId);
  }

  public static int GetCardTagValue(string cardId, GAME_TAG tagId)
  {
    return GameDbf.GetIndex().GetCardTagValue(GameUtils.TranslateCardIdToDbId(cardId), tagId);
  }

  public static int GetCardTagValue(int cardDbId, GAME_TAG tagId)
  {
    return GameDbf.GetIndex().GetCardTagValue(cardDbId, tagId);
  }

  public static IEnumerable<CardTagDbfRecord> GetCardTagRecords(string cardId)
  {
    return GameDbf.GetIndex().GetCardTagRecords(GameUtils.TranslateCardIdToDbId(cardId));
  }

  public static string GetHeroPowerCardIdFromHero(string heroCardId)
  {
    int cardTagValue = GameUtils.GetCardTagValue(heroCardId, GAME_TAG.HERO_POWER);
    if (cardTagValue == 0)
      return string.Empty;
    return GameUtils.TranslateDbIdToCardId(cardTagValue);
  }

  public static string GetHeroPowerCardIdFromHero(int heroDbId)
  {
    if (GameDbf.Card.GetRecord(heroDbId) != null)
      return GameUtils.TranslateDbIdToCardId(GameUtils.GetCardTagValue(heroDbId, GAME_TAG.HERO_POWER));
    UnityEngine.Debug.LogError((object) string.Format("GameUtils.GetHeroPowerCardIdFromHero() - failed to find record for heroDbId {0}", (object) heroDbId));
    return string.Empty;
  }

  public static TAG_CARD_SET GetCardSetFromCardID(string cardID)
  {
    EntityDef entityDef = DefLoader.Get().GetEntityDef(cardID);
    if (entityDef != null)
      return entityDef.GetCardSet();
    UnityEngine.Debug.LogError((object) string.Format("Null EntityDef in GetCardSetFromCardID() for {0}", (object) cardID));
    return TAG_CARD_SET.INVALID;
  }

  public static string GetBasicHeroCardIdFromClass(TAG_CLASS classTag)
  {
    switch (classTag)
    {
      case TAG_CLASS.DRUID:
        return "HERO_06";
      case TAG_CLASS.HUNTER:
        return "HERO_05";
      case TAG_CLASS.MAGE:
        return "HERO_08";
      case TAG_CLASS.PALADIN:
        return "HERO_04";
      case TAG_CLASS.PRIEST:
        return "HERO_09";
      case TAG_CLASS.ROGUE:
        return "HERO_03";
      case TAG_CLASS.SHAMAN:
        return "HERO_02";
      case TAG_CLASS.WARLOCK:
        return "HERO_07";
      case TAG_CLASS.WARRIOR:
        return "HERO_01";
      default:
        UnityEngine.Debug.LogError((object) string.Format("GameUtils.GetBasicHeroCardIdFromClass() - unsupported class tag {0}", (object) classTag));
        return string.Empty;
    }
  }

  public static NetCache.HeroLevel GetHeroLevel(TAG_CLASS heroClass)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameUtils.\u003CGetHeroLevel\u003Ec__AnonStorey45E levelCAnonStorey45E = new GameUtils.\u003CGetHeroLevel\u003Ec__AnonStorey45E();
    // ISSUE: reference to a compiler-generated field
    levelCAnonStorey45E.heroClass = heroClass;
    NetCache.NetCacheHeroLevels netObject = NetCache.Get().GetNetObject<NetCache.NetCacheHeroLevels>();
    if (netObject == null)
      UnityEngine.Debug.LogError((object) "GameUtils.GetHeroLevel() - NetCache.NetCacheHeroLevels is null");
    // ISSUE: reference to a compiler-generated method
    return netObject.Levels.Find(new Predicate<NetCache.HeroLevel>(levelCAnonStorey45E.\u003C\u003Em__32E));
  }

  public static int CardPremiumSortComparisonAsc(TAG_PREMIUM premium1, TAG_PREMIUM premium2)
  {
    return premium1 - premium2;
  }

  public static int CardPremiumSortComparisonDesc(TAG_PREMIUM premium1, TAG_PREMIUM premium2)
  {
    return premium2 - premium1;
  }

  private static void ShowDetailedCardError(string format, params object[] formatArgs)
  {
    string str = string.Format(format, formatArgs);
    MethodBase method = new StackTrace().GetFrame(2).GetMethod();
    Error.AddDevWarning("Error", "{0}\nCalled by {1}.{2}", (object) str, (object) method.ReflectedType, (object) method.Name);
  }

  public static bool CanConcedeCurrentMission()
  {
    return GameState.Get() != null && !GameMgr.Get().IsTutorial() && !GameMgr.Get().IsSpectator();
  }

  public static bool CanRestartCurrentMission(bool checkTutorial = true)
  {
    return GameState.Get() != null && (!checkTutorial || !GameMgr.Get().IsTutorial()) && (!GameMgr.Get().IsSpectator() && GameMgr.Get().IsAI() && (GameMgr.Get().HasLastPlayedDeckId() && BattleNet.IsConnected()));
  }

  public static Card GetJoustWinner(Network.HistMetaData metaData)
  {
    if (metaData == null)
      return (Card) null;
    if (metaData.MetaType != HistoryMeta.Type.JOUST)
      return (Card) null;
    Entity entity = GameState.Get().GetEntity(metaData.Data);
    if (entity == null)
      return (Card) null;
    return entity.GetCard();
  }

  public static bool IsHistoryDeathTagChange(Network.HistTagChange tagChange)
  {
    if (tagChange.Tag != 360 || tagChange.Value != 1)
      return false;
    Entity entity = GameState.Get().GetEntity(tagChange.Entity);
    return entity != null && !entity.IsEnchantment() && entity.GetCardType() != TAG_CARDTYPE.INVALID;
  }

  public static bool IsHistoryDiscardTagChange(Network.HistTagChange tagChange)
  {
    return tagChange.Tag == 49 && GameState.Get().GetEntity(tagChange.Entity).GetZone() == TAG_ZONE.HAND && tagChange.Value == 4;
  }

  public static bool IsCharacterDeathTagChange(Network.HistTagChange tagChange)
  {
    if (tagChange.Tag != 49 || tagChange.Value != 4)
      return false;
    Entity entity = GameState.Get().GetEntity(tagChange.Entity);
    return entity != null && entity.IsCharacter();
  }

  public static bool IsPreGameOverPlayState(TAG_PLAYSTATE playState)
  {
    switch (playState)
    {
      case TAG_PLAYSTATE.WINNING:
      case TAG_PLAYSTATE.LOSING:
      case TAG_PLAYSTATE.DISCONNECTED:
      case TAG_PLAYSTATE.CONCEDED:
        return true;
      default:
        return false;
    }
  }

  public static bool IsGameOverTag(int entityId, int tag, int val)
  {
    return GameUtils.IsGameOverTag(GameState.Get().GetEntity(entityId) as Player, tag, val);
  }

  public static bool IsGameOverTag(Player player, int tag, int val)
  {
    if (player == null || tag != 17 || !player.IsFriendlySide())
      return false;
    switch (val)
    {
      case 4:
      case 5:
      case 6:
        return true;
      default:
        return false;
    }
  }

  public static bool IsFriendlyConcede(Network.HistTagChange tagChange)
  {
    if (tagChange.Tag != 17)
      return false;
    Player entity = GameState.Get().GetEntity(tagChange.Entity) as Player;
    if (entity == null || !entity.IsFriendlySide())
      return false;
    return tagChange.Value == 8;
  }

  public static bool IsBeginPhase(TAG_STEP step)
  {
    switch (step)
    {
      case TAG_STEP.INVALID:
      case TAG_STEP.BEGIN_FIRST:
      case TAG_STEP.BEGIN_SHUFFLE:
      case TAG_STEP.BEGIN_DRAW:
      case TAG_STEP.BEGIN_MULLIGAN:
        return true;
      default:
        return false;
    }
  }

  public static bool IsPastBeginPhase(TAG_STEP step)
  {
    return !GameUtils.IsBeginPhase(step);
  }

  public static bool IsMainPhase(TAG_STEP step)
  {
    switch (step)
    {
      case TAG_STEP.MAIN_BEGIN:
      case TAG_STEP.MAIN_READY:
      case TAG_STEP.MAIN_RESOURCE:
      case TAG_STEP.MAIN_DRAW:
      case TAG_STEP.MAIN_START:
      case TAG_STEP.MAIN_ACTION:
      case TAG_STEP.MAIN_COMBAT:
      case TAG_STEP.MAIN_END:
      case TAG_STEP.MAIN_NEXT:
      case TAG_STEP.MAIN_CLEANUP:
      case TAG_STEP.MAIN_START_TRIGGERS:
        return true;
      default:
        return false;
    }
  }

  public static List<Entity> GetEntitiesKilledBySourceAmongstTargets(int damageSourceID, List<Entity> targetEntities)
  {
    List<Entity> entityList1 = new List<Entity>();
    using (List<Entity>.Enumerator enumerator = targetEntities.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Entity current = enumerator.Current;
        if (current != null)
          entityList1.Add(current.CloneForZoneMgr());
      }
    }
    List<Entity> entityList2 = new List<Entity>();
    PowerProcessor powerProcessor = GameState.Get().GetPowerProcessor();
    List<PowerTaskList> powerTaskListList = new List<PowerTaskList>();
    if (powerProcessor.GetCurrentTaskList() != null)
      powerTaskListList.Add(powerProcessor.GetCurrentTaskList());
    powerTaskListList.AddRange((IEnumerable<PowerTaskList>) powerProcessor.GetPowerQueue().GetList());
    for (int index1 = 0; index1 < powerTaskListList.Count; ++index1)
    {
      List<PowerTask> taskList = powerTaskListList[index1].GetTaskList();
      for (int index2 = 0; index2 < taskList.Count; ++index2)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GameUtils.\u003CGetEntitiesKilledBySourceAmongstTargets\u003Ec__AnonStorey45F targetsCAnonStorey45F = new GameUtils.\u003CGetEntitiesKilledBySourceAmongstTargets\u003Ec__AnonStorey45F();
        PowerTask powerTask = taskList[index2];
        // ISSUE: reference to a compiler-generated field
        targetsCAnonStorey45F.tagChange = powerTask.GetPower() as Network.HistTagChange;
        // ISSUE: reference to a compiler-generated field
        if (targetsCAnonStorey45F.tagChange != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (targetsCAnonStorey45F.tagChange.Tag == 18)
          {
            // ISSUE: reference to a compiler-generated method
            Entity entity = entityList1.Find(new Predicate<Entity>(targetsCAnonStorey45F.\u003C\u003Em__32F));
            if (entity != null)
            {
              // ISSUE: reference to a compiler-generated field
              entity.SetTag(18, targetsCAnonStorey45F.tagChange.Value);
            }
          }
          else
          {
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated field
            if (targetsCAnonStorey45F.tagChange.Tag == 49 && targetsCAnonStorey45F.tagChange.Value == 4)
            {
              // ISSUE: reference to a compiler-generated method
              Entity entity = entityList1.Find(new Predicate<Entity>(targetsCAnonStorey45F.\u003C\u003Em__330));
              if (entity != null && entity.GetTag(GAME_TAG.LAST_AFFECTED_BY) == damageSourceID)
                entityList2.Add(entity);
            }
          }
        }
      }
    }
    return entityList2;
  }

  public static void ApplyPower(Entity entity, Network.PowerHistory power)
  {
    switch (power.Type)
    {
      case Network.PowerType.SHOW_ENTITY:
        GameUtils.ApplyShowEntity(entity, (Network.HistShowEntity) power);
        break;
      case Network.PowerType.HIDE_ENTITY:
        GameUtils.ApplyHideEntity(entity, (Network.HistHideEntity) power);
        break;
      case Network.PowerType.TAG_CHANGE:
        GameUtils.ApplyTagChange(entity, (Network.HistTagChange) power);
        break;
    }
  }

  public static void ApplyShowEntity(Entity entity, Network.HistShowEntity showEntity)
  {
    using (List<Network.Entity.Tag>.Enumerator enumerator = showEntity.Entity.Tags.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.Entity.Tag current = enumerator.Current;
        entity.SetTag(current.Name, current.Value);
      }
    }
  }

  public static void ApplyHideEntity(Entity entity, Network.HistHideEntity hideEntity)
  {
    entity.SetTag(GAME_TAG.ZONE, hideEntity.Zone);
  }

  public static void ApplyTagChange(Entity entity, Network.HistTagChange tagChange)
  {
    entity.SetTag(tagChange.Tag, tagChange.Value);
  }

  public static TAG_ZONE GetFinalZoneForEntity(Entity entity)
  {
    PowerProcessor powerProcessor = GameState.Get().GetPowerProcessor();
    List<PowerTaskList> powerTaskListList = new List<PowerTaskList>();
    if (powerProcessor.GetCurrentTaskList() != null)
      powerTaskListList.Add(powerProcessor.GetCurrentTaskList());
    powerTaskListList.AddRange((IEnumerable<PowerTaskList>) powerProcessor.GetPowerQueue().GetList());
    for (int index1 = powerTaskListList.Count - 1; index1 >= 0; --index1)
    {
      List<PowerTask> taskList = powerTaskListList[index1].GetTaskList();
      for (int index2 = taskList.Count - 1; index2 >= 0; --index2)
      {
        Network.HistTagChange power = taskList[index2].GetPower() as Network.HistTagChange;
        if (power != null && power.Entity == entity.GetEntityId() && power.Tag == 49)
          return (TAG_ZONE) power.Value;
      }
    }
    return entity.GetZone();
  }

  public static bool IsEntityHiddenAfterCurrentTasklist(Entity entity)
  {
    if (!entity.IsHidden())
      return false;
    PowerProcessor powerProcessor = GameState.Get().GetPowerProcessor();
    if (powerProcessor.GetCurrentTaskList() != null)
    {
      using (List<PowerTask>.Enumerator enumerator = powerProcessor.GetCurrentTaskList().GetTaskList().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Network.HistShowEntity power = enumerator.Current.GetPower() as Network.HistShowEntity;
          if (power != null && power.Entity.ID == entity.GetEntityId() && !string.IsNullOrEmpty(power.Entity.CardID))
            return false;
        }
      }
    }
    return true;
  }

  public static void DoDamageTasks(PowerTaskList powerTaskList, Card sourceCard, Card targetCard)
  {
    List<PowerTask> taskList = powerTaskList.GetTaskList();
    if (taskList == null || taskList.Count == 0)
      return;
    int entityId1 = sourceCard.GetEntity().GetEntityId();
    int entityId2 = targetCard.GetEntity().GetEntityId();
    using (List<PowerTask>.Enumerator enumerator1 = taskList.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        PowerTask current1 = enumerator1.Current;
        Network.PowerHistory power = current1.GetPower();
        if (power.Type == Network.PowerType.META_DATA)
        {
          Network.HistMetaData histMetaData = (Network.HistMetaData) power;
          if (histMetaData.MetaType == HistoryMeta.Type.DAMAGE || histMetaData.MetaType == HistoryMeta.Type.HEALING)
          {
            using (List<int>.Enumerator enumerator2 = histMetaData.Info.GetEnumerator())
            {
              while (enumerator2.MoveNext())
              {
                int current2 = enumerator2.Current;
                if (current2 == entityId1 || current2 == entityId2)
                  current1.DoTask();
              }
            }
          }
        }
        else if (power.Type == Network.PowerType.TAG_CHANGE)
        {
          Network.HistTagChange histTagChange = (Network.HistTagChange) power;
          if (histTagChange.Entity == entityId1 || histTagChange.Entity == entityId2)
          {
            switch ((GAME_TAG) histTagChange.Tag)
            {
              case GAME_TAG.DAMAGE:
              case GAME_TAG.EXHAUSTED:
                current1.DoTask();
                continue;
              default:
                continue;
            }
          }
        }
      }
    }
  }

  public static AdventureDbfRecord GetAdventureRecord(int missionId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record == null)
      return (AdventureDbfRecord) null;
    int adventureId = record.AdventureId;
    return GameDbf.Adventure.GetRecord(adventureId);
  }

  public static WingDbfRecord GetWingRecord(int missionId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record == null)
      return (WingDbfRecord) null;
    return GameDbf.Wing.GetRecord(record.WingId);
  }

  public static int GetFinalAdventureWing(int adventureId)
  {
    int num1 = -1;
    int num2 = 0;
    using (List<WingDbfRecord>.Enumerator enumerator = GameDbf.Wing.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WingDbfRecord current = enumerator.Current;
        if (current.AdventureId == adventureId && current.UnlockOrder > num1)
        {
          num1 = current.UnlockOrder;
          num2 = current.ID;
        }
      }
    }
    return num2;
  }

  public static AdventureDataDbfRecord GetAdventureDataRecord(int adventureId, int modeId)
  {
    using (List<AdventureDataDbfRecord>.Enumerator enumerator = GameDbf.AdventureData.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureDataDbfRecord current = enumerator.Current;
        if (current.AdventureId == adventureId && current.ModeId == modeId)
          return current;
      }
    }
    return (AdventureDataDbfRecord) null;
  }

  public static List<ScenarioDbfRecord> GetClassChallengeRecords(int adventureId, int wingId)
  {
    List<ScenarioDbfRecord> scenarioDbfRecordList = new List<ScenarioDbfRecord>();
    using (List<ScenarioDbfRecord>.Enumerator enumerator = GameDbf.Scenario.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ScenarioDbfRecord current = enumerator.Current;
        if (current.ModeId == 4 && current.AdventureId == adventureId && current.WingId == wingId)
          scenarioDbfRecordList.Add(current);
      }
    }
    return scenarioDbfRecordList;
  }

  public static TAG_CLASS GetClassChallengeHeroClass(ScenarioDbfRecord rec)
  {
    if (rec.ModeId != 4)
      return TAG_CLASS.INVALID;
    EntityDef entityDef = DefLoader.Get().GetEntityDef(rec.Player1HeroCardId);
    if (entityDef == null)
      return TAG_CLASS.INVALID;
    return entityDef.GetClass();
  }

  public static List<TAG_CLASS> GetClassChallengeHeroClasses(int adventureId, int wingId)
  {
    List<ScenarioDbfRecord> challengeRecords = GameUtils.GetClassChallengeRecords(adventureId, wingId);
    List<TAG_CLASS> tagClassList = new List<TAG_CLASS>();
    using (List<ScenarioDbfRecord>.Enumerator enumerator = challengeRecords.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ScenarioDbfRecord current = enumerator.Current;
        tagClassList.Add(GameUtils.GetClassChallengeHeroClass(current));
      }
    }
    return tagClassList;
  }

  public static bool IsAIMission(int missionId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    return record != null && record.Players == 1;
  }

  public static bool IsCoopMission(int missionId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record == null)
      return false;
    return record.IsCoop;
  }

  public static string GetMissionHeroCardId(int missionId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record == null)
      return (string) null;
    int player2HeroCardId = record.ClientPlayer2HeroCardId;
    if (player2HeroCardId == 0)
      player2HeroCardId = record.Player2HeroCardId;
    return GameUtils.TranslateDbIdToCardId(player2HeroCardId);
  }

  public static string GetMissionHeroName(int missionId)
  {
    string missionHeroCardId = GameUtils.GetMissionHeroCardId(missionId);
    if (missionHeroCardId == null)
      return (string) null;
    EntityDef entityDef = DefLoader.Get().GetEntityDef(missionHeroCardId);
    if (entityDef != null)
      return entityDef.GetName();
    UnityEngine.Debug.LogError((object) string.Format("GameUtils.GetMissionHeroName() - hero {0} for mission {1} has no EntityDef", (object) missionHeroCardId, (object) missionId));
    return (string) null;
  }

  public static string GetMissionHeroPowerCardId(int missionId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record == null)
      return (string) null;
    int player2HeroCardId = record.ClientPlayer2HeroCardId;
    if (player2HeroCardId == 0)
      player2HeroCardId = record.Player2HeroCardId;
    return GameUtils.TranslateDbIdToCardId(GameUtils.GetCardTagValue(player2HeroCardId, GAME_TAG.HERO_POWER));
  }

  public static bool IsMissionForAdventure(int missionId, int adventureId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record == null)
      return false;
    return adventureId == record.AdventureId;
  }

  public static bool IsTutorialMission(int missionId)
  {
    return GameUtils.IsMissionForAdventure(missionId, 1);
  }

  public static bool IsPracticeMission(int missionId)
  {
    return GameUtils.IsMissionForAdventure(missionId, 2);
  }

  public static bool IsReturningPlayerMission(int missionId)
  {
    return GameUtils.IsMissionForAdventure(missionId, 245);
  }

  public static bool IsBoosterLatestActiveExpansion(int boosterId, bool isPreorder)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameUtils.\u003CIsBoosterLatestActiveExpansion\u003Ec__AnonStorey460 expansionCAnonStorey460 = new GameUtils.\u003CIsBoosterLatestActiveExpansion\u003Ec__AnonStorey460();
    BoosterDbfRecord record = GameDbf.Booster.GetRecord(boosterId);
    if (record == null)
      return false;
    // ISSUE: reference to a compiler-generated field
    expansionCAnonStorey460.events = SpecialEventManager.Get();
    // ISSUE: reference to a compiler-generated method
    return record.SortOrder == GameDbf.Booster.GetRecords(new Predicate<BoosterDbfRecord>(expansionCAnonStorey460.\u003C\u003Em__331)).Max<BoosterDbfRecord>((Func<BoosterDbfRecord, int>) (r => r.SortOrder));
  }

  public static bool IsExpansionMission(int missionId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record == null)
      return false;
    int adventureId = record.AdventureId;
    if (adventureId == 0)
      return false;
    return GameUtils.IsExpansionAdventure((AdventureDbId) adventureId);
  }

  public static bool IsExpansionAdventure(AdventureDbId adventureId)
  {
    AdventureDbId adventureDbId = adventureId;
    switch (adventureDbId)
    {
      case AdventureDbId.INVALID:
      case AdventureDbId.TUTORIAL:
      case AdventureDbId.PRACTICE:
      case AdventureDbId.TAVERN_BRAWL:
        return false;
      default:
        if (adventureDbId != AdventureDbId.RETURNING_PLAYER)
          return true;
        goto case AdventureDbId.INVALID;
    }
  }

  public static string GetAdventureProductStringKey(int wingID)
  {
    AdventureDbId adventureIdByWingId = GameUtils.GetAdventureIdByWingId(wingID);
    if (adventureIdByWingId != AdventureDbId.INVALID)
      return GameDbf.Adventure.GetRecord((int) adventureIdByWingId).ProductStringKey;
    return string.Empty;
  }

  public static AdventureDbId GetAdventureId(int missionId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record == null)
      return AdventureDbId.INVALID;
    return (AdventureDbId) record.AdventureId;
  }

  public static AdventureDbId GetAdventureIdByWingId(int wingID)
  {
    WingDbfRecord record = GameDbf.Wing.GetRecord(wingID);
    if (record == null)
      return AdventureDbId.INVALID;
    AdventureDbId adventureId = (AdventureDbId) record.AdventureId;
    if (!GameUtils.IsExpansionAdventure(adventureId))
      return AdventureDbId.INVALID;
    return adventureId;
  }

  public static AdventureModeDbId GetAdventureModeId(int missionId)
  {
    ScenarioDbfRecord record = GameDbf.Scenario.GetRecord(missionId);
    if (record == null)
      return AdventureModeDbId.INVALID;
    return (AdventureModeDbId) record.ModeId;
  }

  public static bool IsHeroicAdventureMission(int missionId)
  {
    return GameUtils.GetAdventureModeId(missionId) == AdventureModeDbId.HEROIC;
  }

  public static bool IsClassChallengeMission(int missionId)
  {
    return GameUtils.GetAdventureModeId(missionId) == AdventureModeDbId.CLASS_CHALLENGE;
  }

  public static bool ShouldDoBoxIntro()
  {
    if (ApplicationMgr.IsPublic() || AchieveManager.Get().HasActiveAutoDestroyQuests())
      return true;
    return Options.Get().GetBool(Option.INTRO);
  }

  public static bool AreAllTutorialsComplete(TutorialProgress progress)
  {
    if (DemoMgr.Get().GetMode() == DemoMode.APPLE_STORE)
      return false;
    return progress == TutorialProgress.ILLIDAN_COMPLETE;
  }

  public static bool AreAllTutorialsComplete(long campaignProgress)
  {
    return GameUtils.AreAllTutorialsComplete((TutorialProgress) campaignProgress);
  }

  public static bool AreAllTutorialsComplete()
  {
    NetCache.NetCacheProfileProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>();
    return netObject != null && GameUtils.AreAllTutorialsComplete(netObject.CampaignProgress);
  }

  public static int GetNextTutorial(TutorialProgress progress)
  {
    if (progress == TutorialProgress.NOTHING_COMPLETE)
      return 3;
    if (progress == TutorialProgress.HOGGER_COMPLETE)
      return 4;
    if (progress == TutorialProgress.MILLHOUSE_COMPLETE)
      return 249;
    if (progress == TutorialProgress.CHO_COMPLETE)
      return 181;
    if (progress == TutorialProgress.MUKLA_COMPLETE)
      return 201;
    return progress == TutorialProgress.NESINGWARY_COMPLETE ? 248 : 0;
  }

  public static int GetNextTutorial()
  {
    NetCache.NetCacheProfileProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>();
    if (netObject == null)
      return 0;
    return GameUtils.GetNextTutorial(netObject.CampaignProgress);
  }

  public static string GetTutorialCardRewardDetails(int missionId)
  {
    switch ((ScenarioDbId) missionId)
    {
      case ScenarioDbId.TUTORIAL_HOGGER:
        return GameStrings.Get("GLOBAL_REWARD_CARD_DETAILS_TUTORIAL01");
      case ScenarioDbId.TUTORIAL_MILLHOUSE:
        return GameStrings.Get("GLOBAL_REWARD_CARD_DETAILS_TUTORIAL02");
      case ScenarioDbId.TUTORIAL_ILLIDAN:
        return GameStrings.Get("GLOBAL_REWARD_CARD_DETAILS_TUTORIAL05");
      case ScenarioDbId.TUTORIAL_CHO:
        return GameStrings.Get("GLOBAL_REWARD_CARD_DETAILS_TUTORIAL06");
      case ScenarioDbId.TUTORIAL_MUKLA:
        return GameStrings.Get("GLOBAL_REWARD_CARD_DETAILS_TUTORIAL03");
      case ScenarioDbId.TUTORIAL_NESINGWARY:
        return GameStrings.Get("GLOBAL_REWARD_CARD_DETAILS_TUTORIAL04");
      default:
        UnityEngine.Debug.LogWarning((object) string.Format("GameUtils.GetTutorialCardRewardDetails(): no card reward details for mission {0}", (object) missionId));
        return string.Empty;
    }
  }

  public static string GetCurrentTutorialCardRewardDetails()
  {
    return GameUtils.GetTutorialCardRewardDetails(GameMgr.Get().GetMissionId());
  }

  public static int MissionSortComparison(ScenarioDbfRecord rec1, ScenarioDbfRecord rec2)
  {
    return rec1.SortOrder - rec2.SortOrder;
  }

  public static List<FixedRewardActionDbfRecord> GetFixedActionRecords(FixedActionType actionType)
  {
    return GameDbf.GetIndex().GetFixedActionRecordsForType(actionType);
  }

  public static FixedRewardDbfRecord GetFixedRewardForCard(string cardID, TAG_PREMIUM premium)
  {
    int dbId = GameUtils.TranslateCardIdToDbId(cardID);
    using (List<FixedRewardDbfRecord>.Enumerator enumerator = GameDbf.FixedReward.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        FixedRewardDbfRecord current = enumerator.Current;
        int cardId = current.CardId;
        if (dbId == cardId)
        {
          int cardPremium = current.CardPremium;
          if (premium == (TAG_PREMIUM) cardPremium)
            return current;
        }
      }
    }
    return (FixedRewardDbfRecord) null;
  }

  public static List<FixedRewardMapDbfRecord> GetFixedRewardMapRecordsForAction(int actionID)
  {
    return GameDbf.GetIndex().GetFixedRewardMapRecordsForAction(actionID);
  }

  public static bool IsMatchmadeGameType(GameType gameType)
  {
    GameType gameType1 = gameType;
    switch (gameType1)
    {
      case GameType.GT_ARENA:
      case GameType.GT_RANKED:
      case GameType.GT_CASUAL:
        return true;
      default:
        if (gameType1 != GameType.GT_TAVERNBRAWL)
          return false;
        TavernBrawlMission tavernBrawlMission = TavernBrawlManager.Get().CurrentMission();
        return tavernBrawlMission == null || !GameUtils.IsAIMission(tavernBrawlMission.missionId);
    }
  }

  public static bool ShouldShowArenaModeIcon()
  {
    return GameMgr.Get().GetGameType() == GameType.GT_ARENA;
  }

  public static bool ShouldShowCasualModeIcon()
  {
    return GameMgr.Get().GetGameType() == GameType.GT_CASUAL;
  }

  public static bool ShouldShowFriendlyChallengeIcon()
  {
    return GameMgr.Get().GetGameType() == GameType.GT_VS_FRIEND;
  }

  public static bool ShouldShowTavernBrawlModeIcon()
  {
    switch (GameMgr.Get().GetGameType())
    {
      case GameType.GT_TAVERNBRAWL:
      case GameType.GT_TB_1P_VS_AI:
      case GameType.GT_TB_2P_COOP:
        return true;
      default:
        return false;
    }
  }

  public static bool ShouldShowAdventureModeIcon()
  {
    int missionId = GameMgr.Get().GetMissionId();
    AdventureDbId adventureId = GameUtils.GetAdventureId(missionId);
    if (GameUtils.IsExpansionMission(missionId))
      return adventureId != AdventureDbId.TAVERN_BRAWL;
    return false;
  }

  public static bool ShouldShowRankedMedals()
  {
    return GameUtils.ShouldShowRankedMedals(GameMgr.Get().GetGameType());
  }

  public static bool ShouldShowRankedMedals(GameType gameType)
  {
    if (DemoMgr.Get().IsExpoDemo())
      return false;
    return gameType == GameType.GT_RANKED;
  }

  public static bool IsAnyTransitionActive()
  {
    SceneMgr sceneMgr = SceneMgr.Get();
    if ((UnityEngine.Object) sceneMgr != (UnityEngine.Object) null && sceneMgr.IsTransitionNowOrPending())
      return true;
    Box box = Box.Get();
    if ((UnityEngine.Object) box != (UnityEngine.Object) null && box.IsTransitioningToSceneMode())
      return true;
    LoadingScreen loadingScreen = LoadingScreen.Get();
    return (UnityEngine.Object) loadingScreen != (UnityEngine.Object) null && loadingScreen.IsTransitioning();
  }

  public static void LogoutConfirmation()
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get(!Network.ShouldBeConnectedToAurora() ? "GLOBAL_LOGIN_CONFIRM_TITLE" : "GLOBAL_LOGOUT_CONFIRM_TITLE"),
      m_text = GameStrings.Get(!Network.ShouldBeConnectedToAurora() ? "GLOBAL_LOGIN_CONFIRM_MESSAGE" : "GLOBAL_LOGOUT_CONFIRM_MESSAGE"),
      m_alertTextAlignment = UberText.AlignmentOptions.Center,
      m_showAlertIcon = false,
      m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
      m_responseCallback = new AlertPopup.ResponseCallback(GameUtils.OnLogoutConfirmationResponse)
    });
  }

  private static void OnLogoutConfirmationResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
      return;
    GameMgr.Get().SetPendingAutoConcede(true);
    if (Network.ShouldBeConnectedToAurora())
      WebAuth.ClearLoginData();
    ApplicationMgr.Get().ResetAndForceLogin();
  }

  public static int GetBoosterCount()
  {
    NetCache.NetCacheBoosters netObject = NetCache.Get().GetNetObject<NetCache.NetCacheBoosters>();
    if (netObject == null)
      return 0;
    return netObject.GetTotalNumBoosters();
  }

  public static int GetBoosterCount(int boosterStackId)
  {
    NetCache.NetCacheBoosters netObject = NetCache.Get().GetNetObject<NetCache.NetCacheBoosters>();
    if (netObject == null)
      return 0;
    NetCache.BoosterStack boosterStack = netObject.GetBoosterStack(boosterStackId);
    if (boosterStack == null)
      return 0;
    return boosterStack.Count;
  }

  public static bool HaveBoosters()
  {
    return GameUtils.GetBoosterCount() > 0;
  }

  public static PackOpeningRarity GetPackOpeningRarity(TAG_RARITY tag)
  {
    switch (tag)
    {
      case TAG_RARITY.COMMON:
        return PackOpeningRarity.COMMON;
      case TAG_RARITY.FREE:
        return PackOpeningRarity.COMMON;
      case TAG_RARITY.RARE:
        return PackOpeningRarity.RARE;
      case TAG_RARITY.EPIC:
        return PackOpeningRarity.EPIC;
      case TAG_RARITY.LEGENDARY:
        return PackOpeningRarity.LEGENDARY;
      default:
        return PackOpeningRarity.NONE;
    }
  }

  public static List<BoosterDbfRecord> GetPackRecordsWithStorePrefab()
  {
    return GameDbf.Booster.GetRecords((Predicate<BoosterDbfRecord>) (r => !string.IsNullOrEmpty(r.StorePrefab)));
  }

  public static List<AdventureDbfRecord> GetSortedAdventureRecordsWithStorePrefab()
  {
    List<AdventureDbfRecord> records = GameDbf.Adventure.GetRecords((Predicate<AdventureDbfRecord>) (r => !string.IsNullOrEmpty(r.StorePrefab)));
    records.Sort((Comparison<AdventureDbfRecord>) ((l, r) => r.SortOrder - l.SortOrder));
    return records;
  }

  public static List<AdventureDbfRecord> GetAdventureRecordsWithDefPrefab()
  {
    return GameDbf.Adventure.GetRecords((Predicate<AdventureDbfRecord>) (r => !string.IsNullOrEmpty(r.AdventureDefPrefab)));
  }

  public static List<AdventureDataDbfRecord> GetAdventureDataRecordsWithSubDefPrefab()
  {
    return GameDbf.AdventureData.GetRecords((Predicate<AdventureDataDbfRecord>) (r => !string.IsNullOrEmpty(r.AdventureSubDefPrefab)));
  }

  public static IEnumerable<int> GetSortedPackIds(bool ascending = true)
  {
    return (!ascending ? (IEnumerable<BoosterDbfRecord>) GameDbf.Booster.GetRecords().OrderByDescending<BoosterDbfRecord, int>((Func<BoosterDbfRecord, int>) (b => b.SortOrder)) : (IEnumerable<BoosterDbfRecord>) GameDbf.Booster.GetRecords().OrderBy<BoosterDbfRecord, int>((Func<BoosterDbfRecord, int>) (b => b.SortOrder))).Select<BoosterDbfRecord, int>((Func<BoosterDbfRecord, int>) (b => b.ID));
  }

  public static bool IsFakePackOpeningEnabled()
  {
    if (!ApplicationMgr.IsInternal())
      return false;
    return Options.Get().GetBool(Option.FAKE_PACK_OPENING);
  }

  public static int GetFakePackCount()
  {
    if (!ApplicationMgr.IsInternal())
      return 0;
    return Options.Get().GetInt(Option.FAKE_PACK_COUNT);
  }

  public static int GetBoardIdFromAssetName(string name)
  {
    using (List<BoardDbfRecord>.Enumerator enumerator = GameDbf.Board.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoardDbfRecord current = enumerator.Current;
        string name1 = FileUtils.GameAssetPathToName(current.Prefab);
        if (!(name != name1))
          return current.ID;
      }
    }
    return 0;
  }

  public static UnityEngine.Object Instantiate(GameObject original, GameObject parent, bool withRotation = false)
  {
    if ((UnityEngine.Object) original == (UnityEngine.Object) null)
      return (UnityEngine.Object) null;
    GameObject child = UnityEngine.Object.Instantiate<GameObject>(original);
    GameUtils.SetParent(child, parent, withRotation);
    return (UnityEngine.Object) child;
  }

  public static UnityEngine.Object Instantiate(Component original, GameObject parent, bool withRotation = false)
  {
    if ((UnityEngine.Object) original == (UnityEngine.Object) null)
      return (UnityEngine.Object) null;
    Component child = UnityEngine.Object.Instantiate<Component>(original);
    GameUtils.SetParent(child, parent, withRotation);
    return (UnityEngine.Object) child;
  }

  public static UnityEngine.Object Instantiate(UnityEngine.Object original)
  {
    if (original == (UnityEngine.Object) null)
      return (UnityEngine.Object) null;
    return UnityEngine.Object.Instantiate(original);
  }

  public static UnityEngine.Object InstantiateGameObject(string name, GameObject parent = null, bool withRotation = false)
  {
    if (name == null)
      return (UnityEngine.Object) null;
    GameObject child = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(name), true, false);
    if ((UnityEngine.Object) parent != (UnityEngine.Object) null)
      GameUtils.SetParent(child, parent, withRotation);
    return (UnityEngine.Object) child;
  }

  public static void SetParent(Component child, Component parent, bool withRotation = false)
  {
    GameUtils.SetParent(child.transform, parent.transform, withRotation);
  }

  public static void SetParent(GameObject child, Component parent, bool withRotation = false)
  {
    GameUtils.SetParent(child.transform, parent.transform, withRotation);
  }

  public static void SetParent(Component child, GameObject parent, bool withRotation = false)
  {
    GameUtils.SetParent(child.transform, parent.transform, withRotation);
  }

  public static void SetParent(GameObject child, GameObject parent, bool withRotation = false)
  {
    GameUtils.SetParent(child.transform, parent.transform, withRotation);
  }

  private static void SetParent(Transform child, Transform parent, bool withRotation)
  {
    Vector3 localScale = child.localScale;
    Quaternion localRotation = child.localRotation;
    child.parent = parent;
    child.localPosition = Vector3.zero;
    child.localScale = localScale;
    if (!withRotation)
      return;
    child.localRotation = localRotation;
  }

  public static void ResetTransform(GameObject obj)
  {
    obj.transform.localPosition = Vector3.zero;
    obj.transform.localScale = Vector3.one;
    obj.transform.localRotation = Quaternion.identity;
  }

  public static void ResetTransform(Component comp)
  {
    GameUtils.ResetTransform(comp.gameObject);
  }

  public static T LoadGameObjectWithComponent<T>(string assetPath) where T : Component
  {
    GameObject gameObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(assetPath), true, false);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      return (T) null;
    T component = gameObject.GetComponent<T>();
    if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
      return component;
    UnityEngine.Debug.LogError((object) string.Format("{0} object does not contain {1} component.", (object) assetPath, (object) typeof (T)));
    UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
    return (T) null;
  }

  public static void PlayCardEffectDefSounds(CardEffectDef cardEffectDef)
  {
    if (cardEffectDef == null)
      return;
    using (List<string>.Enumerator enumerator = cardEffectDef.m_SoundSpellPaths.GetEnumerator())
    {
      while (enumerator.MoveNext())
        AssetLoader.Get().LoadSpell(enumerator.Current, (AssetLoader.GameObjectCallback) ((name, go, data) =>
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          GameUtils.\u003CPlayCardEffectDefSounds\u003Ec__AnonStorey461 soundsCAnonStorey461 = new GameUtils.\u003CPlayCardEffectDefSounds\u003Ec__AnonStorey461();
          if ((UnityEngine.Object) go == (UnityEngine.Object) null)
          {
            UnityEngine.Debug.LogError((object) string.Format("Unable to load spell object: {0}", (object) name));
          }
          else
          {
            // ISSUE: reference to a compiler-generated field
            soundsCAnonStorey461.destroyObj = go;
            CardSoundSpell component = go.GetComponent<CardSoundSpell>();
            if ((UnityEngine.Object) component == (UnityEngine.Object) null)
            {
              UnityEngine.Debug.LogError((object) string.Format("Card sound spell component not found: {0}", (object) name));
              // ISSUE: reference to a compiler-generated field
              UnityEngine.Object.Destroy((UnityEngine.Object) soundsCAnonStorey461.destroyObj);
            }
            else
            {
              // ISSUE: reference to a compiler-generated method
              component.AddStateFinishedCallback(new Spell.StateFinishedCallback(soundsCAnonStorey461.\u003C\u003Em__340));
              component.ForceDefaultAudioSource();
              component.Activate();
            }
          }
        }), (object) null, false);
    }
  }

  public static bool LoadCardDefEmoteSound(CardDef cardDef, EmoteType type, GameUtils.EmoteSoundLoaded callback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameUtils.\u003CLoadCardDefEmoteSound\u003Ec__AnonStorey462 soundCAnonStorey462 = new GameUtils.\u003CLoadCardDefEmoteSound\u003Ec__AnonStorey462();
    // ISSUE: reference to a compiler-generated field
    soundCAnonStorey462.type = type;
    // ISSUE: reference to a compiler-generated field
    soundCAnonStorey462.callback = callback;
    // ISSUE: reference to a compiler-generated field
    if (soundCAnonStorey462.callback == null)
    {
      UnityEngine.Debug.LogError((object) "No callback provided for LoadEmote!");
      return false;
    }
    if ((UnityEngine.Object) cardDef == (UnityEngine.Object) null || cardDef.m_EmoteDefs == null)
      return false;
    // ISSUE: reference to a compiler-generated method
    EmoteEntryDef emoteEntryDef = cardDef.m_EmoteDefs.Find(new Predicate<EmoteEntryDef>(soundCAnonStorey462.\u003C\u003Em__33C));
    if (emoteEntryDef == null)
      return false;
    // ISSUE: reference to a compiler-generated method
    AssetLoader.Get().LoadSpell(emoteEntryDef.m_emoteSoundSpellPath, new AssetLoader.GameObjectCallback(soundCAnonStorey462.\u003C\u003Em__33D), (object) null, false);
    return true;
  }

  public static void SetAutomationName(GameObject obj, params object[] formatArgs)
  {
    string str = obj.name;
    for (int index = 0; index < formatArgs.Length; ++index)
      str = str + "_" + formatArgs[index].ToString();
    obj.name = str;
  }

  public static bool HasSeenStandardModeTutorial()
  {
    return Options.Get().GetBool(Option.HAS_SEEN_STANDARD_MODE_TUTORIAL, false);
  }

  public static bool ShouldShowSetRotationIntro()
  {
    bool flag = GameUtils.ShouldShowSetRotationIntro_Impl();
    if (flag && !GameUtils.ShouldDoBoxIntro() && GameUtils.Cheat_AutoCompleteSetRotationIntro())
      flag = false;
    return flag;
  }

  private static bool ShouldShowSetRotationIntro_Impl()
  {
    Options options = Options.Get();
    if (options == null)
    {
      UnityEngine.Debug.LogError((object) "ShouldShowSetRotationIntro: Options is NULL!");
      return false;
    }
    if (options.HasOption(Option.SHOW_SET_ROTATION_INTRO_VISUALS) && options.GetBool(Option.SHOW_SET_ROTATION_INTRO_VISUALS))
      return true;
    if (ReturningPlayerMgr.Get().IsInReturningPlayerModeAndOnRails)
      return false;
    SpecialEventManager specialEventManager = SpecialEventManager.Get();
    if (specialEventManager == null)
    {
      UnityEngine.Debug.LogError((object) "ShouldShowSetRotationIntro: SpecialEventManager is NULL!");
      return false;
    }
    if (!specialEventManager.IsEventActive(SpecialEventType.SPECIAL_EVENT_SET_ROTATION_2016, false) && !specialEventManager.IsEventActive(SpecialEventType.SPECIAL_EVENT_SET_ROTATION_2017, false))
      return false;
    CollectionManager collectionManager = CollectionManager.Get();
    if (collectionManager == null)
    {
      UnityEngine.Debug.LogError((object) "ShouldShowSetRotationIntro: CollectionManager is NULL!");
      return false;
    }
    return (options.GetInt(Option.SET_ROTATION_INTRO_PROGRESS, 0) == 0 || !GameUtils.HasSeenStandardModeTutorial()) && collectionManager.ShouldAccountSeeStandardWild();
  }

  public static bool Cheat_AutoCompleteSetRotationIntro()
  {
    Options options = Options.Get();
    if (options == null)
    {
      Log.All.PrintError("Cheat_SkipSetRotationIntro: Options is NULL!");
      return false;
    }
    if (options.HasOption(Option.SHOW_SET_ROTATION_INTRO_VISUALS))
      options.SetBool(Option.SHOW_SET_ROTATION_INTRO_VISUALS, false);
    Options.Get().SetInt(Option.SET_ROTATION_INTRO_PROGRESS, 1);
    Options.Get().SetBool(Option.HAS_SEEN_STANDARD_MODE_TUTORIAL, true);
    string str = "Set Rotation intro was auto-completed due to 'intro' option being false!";
    UIStatus.Get().AddInfo(str, 10f);
    Log.All.Print(str);
    return true;
  }

  public static void LoadAndPositionHeroCard(string heroCardID, TAG_PREMIUM premium, GameUtils.LoadHeroActorCallback callback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameUtils.\u003CLoadAndPositionHeroCard\u003Ec__AnonStorey463 cardCAnonStorey463 = new GameUtils.\u003CLoadAndPositionHeroCard\u003Ec__AnonStorey463();
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey463.callback = callback;
    if (string.IsNullOrEmpty(heroCardID))
      return;
    // ISSUE: reference to a compiler-generated method
    DefLoader.Get().LoadFullDef(heroCardID, new DefLoader.LoadDefCallback<FullDef>(cardCAnonStorey463.\u003C\u003Em__33E), (object) premium);
  }

  private static void LoadAndPositionHeroCard_OnFullHeroDefLoaded(string cardID, FullDef def, object userData, GameUtils.LoadHeroActorCallback callback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameUtils.\u003CLoadAndPositionHeroCard_OnFullHeroDefLoaded\u003Ec__AnonStorey464 loadedCAnonStorey464 = new GameUtils.\u003CLoadAndPositionHeroCard_OnFullHeroDefLoaded\u003Ec__AnonStorey464();
    // ISSUE: reference to a compiler-generated field
    loadedCAnonStorey464.callback = callback;
    TAG_PREMIUM tagPremium = (TAG_PREMIUM) userData;
    GameUtils.LoadHeroActorCallbackInfo actorCallbackInfo = new GameUtils.LoadHeroActorCallbackInfo() { heroFullDef = def, premium = tagPremium };
    // ISSUE: reference to a compiler-generated method
    AssetLoader.Get().LoadActor("Card_Play_Hero", new AssetLoader.GameObjectCallback(loadedCAnonStorey464.\u003C\u003Em__33F), (object) actorCallbackInfo, false);
  }

  private static void LoadAndPositionHeroCard_OnHeroActorLoaded(string actorName, GameObject actorObject, object callbackData, GameUtils.LoadHeroActorCallback callback)
  {
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("GameUtils.OnHeroActorLoaded() - FAILED to load actor \"{0}\"", (object) actorName));
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("GameUtils.OnActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) actorName));
      }
      else
      {
        GameUtils.LoadHeroActorCallbackInfo actorCallbackInfo = callbackData as GameUtils.LoadHeroActorCallbackInfo;
        FullDef heroFullDef = actorCallbackInfo.heroFullDef;
        EntityDef entityDef = heroFullDef.GetEntityDef();
        CardDef cardDef = heroFullDef.GetCardDef();
        component.SetPremium(actorCallbackInfo.premium);
        component.SetEntityDef(entityDef);
        component.SetCardDef(cardDef);
        component.UpdateAllComponents();
        component.gameObject.name = cardDef.name + "_actor";
        if ((bool) UniversalInputManager.UsePhoneUI)
          SceneUtils.SetLayer(component.gameObject, GameLayer.IgnoreFullScreenEffects);
        component.GetHealthObject().Hide();
        if (callback == null)
          return;
        callback(component);
      }
    }
  }

  private class LoadHeroActorCallbackInfo
  {
    public FullDef heroFullDef;
    public TAG_PREMIUM premium;
  }

  public delegate void EmoteSoundLoaded(CardSoundSpell emoteObj);

  public delegate void LoadHeroActorCallback(Actor heroActor);
}
