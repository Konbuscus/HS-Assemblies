﻿// Decompiled with JetBrains decompiler
// Type: CardTextBuilderFactory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class CardTextBuilderFactory
{
  public static CardTextBuilder Create(CardTextBuilderType type)
  {
    switch (type)
    {
      case CardTextBuilderType.JADE_GOLEM:
        return (CardTextBuilder) new JadeGolemCardTextBuilder();
      case CardTextBuilderType.JADE_GOLEM_TRIGGER:
        return (CardTextBuilder) new JadeGolemTriggerCardTextBuilder();
      case CardTextBuilderType.KAZAKUS_POTION:
        return (CardTextBuilder) new KazakusPotionCardTextBuilder();
      case CardTextBuilderType.KAZAKUS_POTION_EFFECT:
        return (CardTextBuilder) new KazakusPotionEffectCardTextBuilder();
      default:
        return new CardTextBuilder();
    }
  }
}
