﻿// Decompiled with JetBrains decompiler
// Type: floatyObj
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class floatyObj : MonoBehaviour
{
  public float frequencyMin = 0.0001f;
  public float frequencyMax = 1f / 1000f;
  public float magnitude = 0.0001f;
  private float m_interval;

  private void Start()
  {
    this.m_interval = Random.Range(this.frequencyMin, this.frequencyMax);
  }

  private void Update()
  {
    float num = Mathf.Sin(Time.time * this.m_interval) * this.magnitude;
    Vector3 vector3 = new Vector3(num, num, num);
    this.transform.position += vector3;
    this.transform.eulerAngles += vector3;
  }
}
