﻿// Decompiled with JetBrains decompiler
// Type: WingDbId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum WingDbId
{
  INVALID,
  NAXX_ARACHNID,
  NAXX_PLAGUE,
  NAXX_MILITARY,
  NAXX_CONSTRUCT,
  NAXX_FROSTWYRM,
  BRM_BLACKROCK_DEPTHS,
  BRM_MOLTEN_CORE,
  BRM_BLACKROCK_SPIRE,
  BRM_BLACKWING_LAIR,
  BRM_HIDDEN_LAB,
  LOE_TEMPLE_OF_ORSIS,
  LOE_ULDAMAN,
  LOE_RUINED_CITY,
  LOE_HALL_OF_EXPLORERS,
  KARA_PROLOGUE,
  KARA_PARLOR,
  KARA_OPERA,
  KARA_MENAGERIE,
  KARA_TOWER,
}
