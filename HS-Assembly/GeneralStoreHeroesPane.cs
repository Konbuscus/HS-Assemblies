﻿// Decompiled with JetBrains decompiler
// Type: GeneralStoreHeroesPane
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class GeneralStoreHeroesPane : GeneralStorePane
{
  [SerializeField]
  private Vector3 m_unpurchasedHeroButtonSpacing = new Vector3(0.0f, 0.0f, 0.285f);
  [SerializeField]
  private Vector3 m_purchasedHeroButtonSpacing = new Vector3(0.0f, 0.0f, 0.092f);
  [CustomEditField(Sections = "Layout")]
  public float m_unpurchasedHeroButtonHeight = 0.0275f;
  [CustomEditField(Sections = "Layout")]
  public float m_purchasedHeroButtonHeightPadding = 0.01f;
  [CustomEditField(Sections = "Layout/Purchased Section")]
  public Vector3 m_purchasedSectionOffset = new Vector3(0.0f, 0.0f, 0.145f);
  [CustomEditField(Sections = "Animations")]
  public Vector3 m_purchaseAnimationMidPointWorldOffset = new Vector3(0.0f, 0.0f, -7.5f);
  [CustomEditField(Sections = "Animations")]
  public string m_purchaseAnimationName = "HeroSkin_HeroHolderPopOut";
  private List<GeneralStoreHeroesSelectorButton> m_unpurchasedHeroesButtons = new List<GeneralStoreHeroesSelectorButton>();
  private List<GeneralStoreHeroesSelectorButton> m_purchasedHeroesButtons = new List<GeneralStoreHeroesSelectorButton>();
  private List<GameObject> m_purchasedSectionMidMeshes = new List<GameObject>();
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public string m_heroUnpurchasedFrame;
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public string m_heroPurchasedFrame;
  [CustomEditField(Sections = "Prefabs", T = EditType.GAME_OBJECT)]
  public string m_heroAnimationFrame;
  [CustomEditField(Sections = "Layout")]
  public float m_purchasedHeroButtonHeight;
  [CustomEditField(Sections = "Layout")]
  public float m_maxPurchasedHeightAdd;
  [CustomEditField(Sections = "Layout/Purchased Section")]
  public GameObject m_purchasedSectionTop;
  [CustomEditField(Sections = "Layout/Purchased Section")]
  public GameObject m_purchasedSectionBottom;
  [CustomEditField(Sections = "Layout/Purchased Section")]
  public GameObject m_purchasedSectionMidTemplate;
  [CustomEditField(Sections = "Layout/Purchased Section")]
  public MultiSliceElement m_purchasedSection;
  [CustomEditField(Sections = "Layout/Purchased Section")]
  public GameObject m_purchasedButtonContainer;
  [CustomEditField(Sections = "Scroll")]
  public UIBScrollable m_scrollUpdate;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_heroSelectionSound;
  [CustomEditField(Sections = "Sounds", T = EditType.SOUND_PREFAB)]
  public string m_buttonsSlideUpSound;
  [CustomEditField(Sections = "Purchase Flow")]
  public GameObject m_purchaseAnimationBlocker;
  [CustomEditField(Sections = "Animations")]
  public GameObject m_purchaseAnimationEndBone;
  private GeneralStoreHeroesContent m_heroesContent;
  private bool m_initializeFirstHero;
  private int m_currentPurchaseRemovalIdx;

  [CustomEditField(Sections = "Layout")]
  public Vector3 UnpurchasedHeroButtonSpacing
  {
    get
    {
      return this.m_unpurchasedHeroButtonSpacing;
    }
    set
    {
      this.m_unpurchasedHeroButtonSpacing = value;
      this.PositionAllHeroButtons();
    }
  }

  [CustomEditField(Sections = "Layout")]
  public Vector3 PurchasedHeroButtonSpacing
  {
    get
    {
      return this.m_purchasedHeroButtonSpacing;
    }
    set
    {
      this.m_purchasedHeroButtonSpacing = value;
      this.PositionAllHeroButtons();
    }
  }

  private void Awake()
  {
    this.m_heroesContent = this.m_parentContent as GeneralStoreHeroesContent;
    this.PopulateHeroes();
    this.m_purchaseAnimationBlocker.SetActive(false);
    StoreManager.Get().RegisterSuccessfulPurchaseAckListener(new StoreManager.SuccessfulPurchaseAckCallback(this.OnItemPurchased));
    CheatMgr.Get().RegisterCheatHandler("herobuy", new CheatMgr.ProcessCheatCallback(this.OnHeroPurchased_cheat), (string) null, (string) null, (string) null);
  }

  private void OnDestroy()
  {
    CheatMgr.Get().UnregisterCheatHandler("herobuy", new CheatMgr.ProcessCheatCallback(this.OnHeroPurchased_cheat));
    StoreManager.Get().RemoveSuccessfulPurchaseAckListener(new StoreManager.SuccessfulPurchaseAckCallback(this.OnItemPurchased));
  }

  public override void PrePaneSwappedIn()
  {
    this.SetupInitialSelectedHero();
  }

  public void RefreshHeroAvailability()
  {
  }

  private void PopulateHeroes()
  {
    SpecialEventManager specialEventManager = SpecialEventManager.Get();
    using (List<CardHeroDbfRecord>.Enumerator enumerator = GameDbf.CardHero.GetRecords().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardHeroDbfRecord current = enumerator.Current;
        Network.Bundle heroBundle = (Network.Bundle) null;
        if (StoreManager.Get().GetHeroBundleByCardDbId(current.CardId, out heroBundle) && specialEventManager.IsEventActive(heroBundle.ProductEvent, false))
          this.CreateNewHeroButton(current, heroBundle).SetSortOrder(current.StoreSortOrder);
      }
    }
    this.PositionAllHeroButtons();
  }

  private GeneralStoreHeroesSelectorButton CreateNewHeroButton(CardHeroDbfRecord cardHero, Network.Bundle heroBundle)
  {
    if (!StoreManager.Get().CanBuyBundle(heroBundle))
      return this.CreatePurchasedHeroButton(cardHero, heroBundle);
    return this.CreateUnpurchasedHeroButton(cardHero, heroBundle);
  }

  private GeneralStoreHeroesSelectorButton CreateUnpurchasedHeroButton(CardHeroDbfRecord cardHero, Network.Bundle heroBundle)
  {
    GameObject gameObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(this.m_heroUnpurchasedFrame), true, false);
    GeneralStoreHeroesSelectorButton component = gameObject.GetComponent<GeneralStoreHeroesSelectorButton>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Prefab does not contain GeneralStoreHeroesSelectorButton component.");
      UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
      return (GeneralStoreHeroesSelectorButton) null;
    }
    GameUtils.SetParent((Component) component, this.m_paneContainer, true);
    SceneUtils.SetLayer((Component) component, this.m_paneContainer.layer);
    this.m_unpurchasedHeroesButtons.Add(component);
    this.SetupHeroButton(cardHero, component);
    return component;
  }

  public GeneralStoreHeroesSelectorButton CreatePurchasedHeroButton(CardHeroDbfRecord cardHero, Network.Bundle heroBundle)
  {
    GameObject gameObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(this.m_heroPurchasedFrame), true, false);
    GeneralStoreHeroesSelectorButton component = gameObject.GetComponent<GeneralStoreHeroesSelectorButton>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Prefab does not contain GeneralStoreHeroesSelectorButton component.");
      UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
      return (GeneralStoreHeroesSelectorButton) null;
    }
    GameUtils.SetParent((Component) component, this.m_purchasedButtonContainer, true);
    SceneUtils.SetLayer((Component) component, this.m_purchasedButtonContainer.layer);
    this.m_purchasedHeroesButtons.Add(component);
    this.SetupHeroButton(cardHero, component);
    return component;
  }

  private void SetupHeroButton(CardHeroDbfRecord cardHero, GeneralStoreHeroesSelectorButton heroButton)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStoreHeroesPane.\u003CSetupHeroButton\u003Ec__AnonStorey3F0 buttonCAnonStorey3F0 = new GeneralStoreHeroesPane.\u003CSetupHeroButton\u003Ec__AnonStorey3F0();
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey3F0.heroButton = heroButton;
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey3F0.\u003C\u003Ef__this = this;
    string cardId = GameUtils.TranslateDbIdToCardId(cardHero.CardId);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey3F0.heroButton.SetCardHeroDbfRecord(cardHero);
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey3F0.heroButton.SetPurchased(false);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    buttonCAnonStorey3F0.heroButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(buttonCAnonStorey3F0.\u003C\u003Em__1BF));
    // ISSUE: reference to a compiler-generated method
    DefLoader.Get().LoadFullDef(cardId, new DefLoader.LoadDefCallback<FullDef>(buttonCAnonStorey3F0.\u003C\u003Em__1C0));
  }

  private void UpdatePurchasedSectionLayout()
  {
    if (this.m_purchasedHeroesButtons.Count == 0)
    {
      this.m_purchasedButtonContainer.SetActive(false);
      this.m_purchasedSection.gameObject.SetActive(false);
    }
    else
    {
      this.m_purchasedButtonContainer.SetActive(true);
      this.m_purchasedSection.gameObject.SetActive(true);
      if (this.m_purchasedSectionMidMeshes.Count < this.m_purchasedHeroesButtons.Count)
      {
        int num = this.m_purchasedHeroesButtons.Count - this.m_purchasedSectionMidMeshes.Count;
        for (int index = 0; index < num; ++index)
        {
          GameObject gameObject = (GameObject) GameUtils.Instantiate(this.m_purchasedSectionMidTemplate, this.m_purchasedSection.gameObject, true);
          gameObject.SetActive(true);
          this.m_purchasedSectionMidMeshes.Add(gameObject);
        }
      }
      this.m_purchasedSection.ClearSlices();
      this.m_purchasedSection.AddSlice(this.m_purchasedSectionTop);
      using (List<GameObject>.Enumerator enumerator = this.m_purchasedSectionMidMeshes.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.m_purchasedSection.AddSlice(enumerator.Current);
      }
      this.m_purchasedSection.AddSlice(this.m_purchasedSectionBottom);
      this.m_purchasedSection.UpdateSlices();
    }
  }

  private void SelectHero(GeneralStoreHeroesSelectorButton button)
  {
    using (List<GeneralStoreHeroesSelectorButton>.Enumerator enumerator = this.m_unpurchasedHeroesButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Unselect();
    }
    using (List<GeneralStoreHeroesSelectorButton>.Enumerator enumerator = this.m_purchasedHeroesButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Unselect();
    }
    button.Select();
    Options.Get().SetInt(Option.LAST_SELECTED_STORE_HERO_ID, button.GetHeroDbId());
    this.m_heroesContent.SelectHero(button.GetCardHeroDbfRecord(), true);
    if (string.IsNullOrEmpty(this.m_heroSelectionSound))
      return;
    SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_heroSelectionSound));
  }

  private void SetupInitialSelectedHero()
  {
    if (this.m_initializeFirstHero)
      return;
    this.m_initializeFirstHero = true;
    int num = Options.Get().GetInt(Option.LAST_SELECTED_STORE_HERO_ID, -1);
    if (num == -1)
      return;
    List<GeneralStoreHeroesSelectorButton> heroesSelectorButtonList = new List<GeneralStoreHeroesSelectorButton>();
    heroesSelectorButtonList.AddRange((IEnumerable<GeneralStoreHeroesSelectorButton>) this.m_unpurchasedHeroesButtons);
    heroesSelectorButtonList.AddRange((IEnumerable<GeneralStoreHeroesSelectorButton>) this.m_purchasedHeroesButtons);
    using (List<GeneralStoreHeroesSelectorButton>.Enumerator enumerator = heroesSelectorButtonList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GeneralStoreHeroesSelectorButton current = enumerator.Current;
        if (current.GetHeroDbId() == num)
        {
          this.m_heroesContent.SelectHero(current.GetCardHeroDbfRecord(), false);
          current.Select();
          break;
        }
      }
    }
  }

  private void PositionAllHeroButtons()
  {
    this.PositionUnpurchasedHeroButtons();
    this.PositionPurchasedHeroButtons(true);
  }

  private void PositionUnpurchasedHeroButtons()
  {
    this.m_unpurchasedHeroesButtons.Sort((Comparison<GeneralStoreHeroesSelectorButton>) ((lhs, rhs) =>
    {
      int sortOrder1 = lhs.GetSortOrder();
      int sortOrder2 = rhs.GetSortOrder();
      if (sortOrder1 < sortOrder2)
        return -1;
      return sortOrder1 > sortOrder2 ? 1 : 0;
    }));
    for (int index = 0; index < this.m_unpurchasedHeroesButtons.Count; ++index)
      this.m_unpurchasedHeroesButtons[index].transform.localPosition = this.m_unpurchasedHeroButtonSpacing * (float) index;
  }

  private void PositionPurchasedHeroButtons(bool sortAndSetSectionPos = true)
  {
    if (sortAndSetSectionPos)
    {
      this.m_purchasedHeroesButtons.Sort((Comparison<GeneralStoreHeroesSelectorButton>) ((lhs, rhs) =>
      {
        int sortOrder1 = lhs.GetSortOrder();
        int sortOrder2 = rhs.GetSortOrder();
        if (sortOrder1 < sortOrder2)
          return -1;
        return sortOrder1 > sortOrder2 ? 1 : 0;
      }));
      this.m_purchasedSection.transform.localPosition = this.m_unpurchasedHeroButtonSpacing * (float) (this.m_unpurchasedHeroesButtons.Count - 1) + this.m_purchasedSectionOffset;
    }
    for (int index = 0; index < this.m_purchasedHeroesButtons.Count; ++index)
      this.m_purchasedHeroesButtons[index].transform.localPosition = this.m_purchasedHeroButtonSpacing * (float) index;
    this.UpdatePurchasedSectionLayout();
  }

  [DebuggerHidden]
  private IEnumerator AnimateShowPurchase(int btnIndex)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralStoreHeroesPane.\u003CAnimateShowPurchase\u003Ec__Iterator269() { btnIndex = btnIndex, \u003C\u0024\u003EbtnIndex = btnIndex, \u003C\u003Ef__this = this };
  }

  private void OnItemPurchased(Network.Bundle bundle, PaymentMethod purchaseMethod, object userData)
  {
    if (bundle == null || bundle.Items == null)
      return;
    using (List<Network.BundleItem>.Enumerator enumerator = bundle.Items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.BundleItem current = enumerator.Current;
        if (current != null && current.Product == ProductType.PRODUCT_TYPE_HERO)
        {
          this.OnHeroPurchased(current.ProductData);
          break;
        }
      }
    }
  }

  private void OnHeroPurchased(int heroCardDbId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStoreHeroesPane.\u003COnHeroPurchased\u003Ec__AnonStorey3F1 purchasedCAnonStorey3F1 = new GeneralStoreHeroesPane.\u003COnHeroPurchased\u003Ec__AnonStorey3F1();
    // ISSUE: reference to a compiler-generated field
    purchasedCAnonStorey3F1.heroCardDbId = heroCardDbId;
    // ISSUE: reference to a compiler-generated method
    int index = this.m_unpurchasedHeroesButtons.FindIndex(new Predicate<GeneralStoreHeroesSelectorButton>(purchasedCAnonStorey3F1.\u003C\u003Em__1C3));
    if (index == -1)
    {
      // ISSUE: reference to a compiler-generated field
      UnityEngine.Debug.LogError((object) string.Format("Hero Card DB ID {0} does not exist in button list.", (object) purchasedCAnonStorey3F1.heroCardDbId));
    }
    else
      this.RunHeroPurchaseAnimation(index);
  }

  private void RunHeroPurchaseAnimation(int btnIndex)
  {
    this.m_currentPurchaseRemovalIdx = btnIndex;
    this.StartCoroutine(this.AnimateShowPurchase(btnIndex));
  }

  private bool OnHeroPurchased_cheat(string func, string[] args, string rawArgs)
  {
    if (args.Length == 0)
      return true;
    int result = -1;
    if (int.TryParse(args[0], out result) && result >= 0 && result < this.m_unpurchasedHeroesButtons.Count)
      this.RunHeroPurchaseAnimation(result);
    return true;
  }
}
