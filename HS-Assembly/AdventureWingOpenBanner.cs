﻿// Decompiled with JetBrains decompiler
// Type: AdventureWingOpenBanner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[CustomEditClass]
public class AdventureWingOpenBanner : MonoBehaviour
{
  public iTween.EaseType m_showEase = iTween.EaseType.easeOutElastic;
  public float m_showTime = 0.5f;
  public float m_hideTime = 0.5f;
  public Vector3 m_VOQuotePosition = new Vector3(0.0f, 0.0f, -55f);
  public PegUIElement m_clickCatcher;
  public GameObject m_root;
  public string m_VOQuotePrefab;
  public string m_VOQuoteLine;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_showSound;
  [CustomEditField(T = EditType.SOUND_PREFAB)]
  public string m_hideSound;
  private Vector3 m_originalScale;
  private AdventureWingOpenBanner.OnBannerHidden m_bannerHiddenCallback;

  private void Awake()
  {
    if ((UnityEngine.Object) this.m_clickCatcher != (UnityEngine.Object) null)
      this.m_clickCatcher.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.HideBanner()));
    if ((UnityEngine.Object) this.m_root != (UnityEngine.Object) null)
      this.m_root.SetActive(false);
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, true, CanvasScaleMode.HEIGHT);
  }

  public void ShowBanner(AdventureWingOpenBanner.OnBannerHidden onBannerHiddenCallback = null)
  {
    if ((UnityEngine.Object) this.m_root == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "m_root not defined in banner!");
    }
    else
    {
      this.m_bannerHiddenCallback = onBannerHiddenCallback;
      this.m_originalScale = this.m_root.transform.localScale;
      this.m_root.SetActive(true);
      iTween.ScaleFrom(this.m_root, iTween.Hash((object) "scale", (object) new Vector3(0.01f, 0.01f, 0.01f), (object) "time", (object) this.m_showTime, (object) "easetype", (object) this.m_showEase));
      if (!string.IsNullOrEmpty(this.m_showSound))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_showSound));
      if (!string.IsNullOrEmpty(this.m_VOQuotePrefab) && !string.IsNullOrEmpty(this.m_VOQuoteLine))
        NotificationManager.Get().CreateCharacterQuote(FileUtils.GameAssetPathToName(this.m_VOQuotePrefab), this.m_VOQuotePosition, GameStrings.Get(this.m_VOQuoteLine), this.m_VOQuoteLine, true, 0.0f, (Action) null, CanvasAnchor.CENTER);
      FullScreenFXMgr.Get().StartStandardBlurVignette(this.m_showTime);
    }
  }

  public void HideBanner()
  {
    if ((UnityEngine.Object) this.m_root == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "m_root not defined in banner!");
    }
    else
    {
      FullScreenFXMgr.Get().EndStandardBlurVignette(this.m_hideTime, (FullScreenFXMgr.EffectListener) null);
      this.m_root.transform.localScale = this.m_originalScale;
      iTween.ScaleTo(this.m_root, iTween.Hash((object) "scale", (object) new Vector3(0.01f, 0.01f, 0.01f), (object) "oncomplete", (object) (Action<object>) (o =>
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
        if (this.m_bannerHiddenCallback == null)
          return;
        this.m_bannerHiddenCallback();
      }), (object) "time", (object) this.m_hideTime));
      if (string.IsNullOrEmpty(this.m_hideSound))
        return;
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_hideSound));
    }
  }

  public delegate void OnBannerHidden();
}
