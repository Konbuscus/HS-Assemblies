﻿// Decompiled with JetBrains decompiler
// Type: ClassChallengeUnlockData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class ClassChallengeUnlockData : RewardData
{
  public int WingID { get; set; }

  public ClassChallengeUnlockData()
    : this(0)
  {
  }

  public ClassChallengeUnlockData(int wingID)
    : base(Reward.Type.CLASS_CHALLENGE)
  {
    this.WingID = wingID;
  }

  public override string ToString()
  {
    return string.Format("[ClassChallengeUnlockData: WingID={0} Origin={1} OriginData={2}]", (object) this.WingID, (object) this.Origin, (object) this.OriginData);
  }

  protected override string GetGameObjectName()
  {
    return "ClassChallengeUnlocked";
  }
}
