﻿// Decompiled with JetBrains decompiler
// Type: Network
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using bnet.protocol;
using bnet.protocol.attribute;
using bnet.protocol.game_master;
using BobNetProto;
using Networking;
using PegasusGame;
using PegasusShared;
using PegasusUtil;
using SpectatorProto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using WTCG.BI;

public class Network
{
  public static string TutorialServer = "02";
  private static readonly TimeSpan LOGIN_TIMEOUT = new TimeSpan(0, 0, 5);
  private static readonly TimeSpan PROCESS_WARNING = new TimeSpan(0, 0, 15);
  private static readonly TimeSpan PROCESS_WARNING_REPORT_GAP = new TimeSpan(0, 0, 1);
  private static TimeSpan MAX_DEFERRED_WAIT = new TimeSpan(0, 0, 120);
  private static Network s_instance = new Network();
  private static UnityUrlDownloader s_urlDownloader = new UnityUrlDownloader();
  private static int s_numConnectionFailures = 0;
  private static bool s_gameConceded = false;
  private static bool s_disconnectRequested = false;
  private static AckCardSeen s_ackCardSeenPacket = new AckCardSeen();
  private static readonly List<Network.ConnectErrorParams> s_errorList = new List<Network.ConnectErrorParams>();
  private static List<Network.ThrottledPacketListener> s_throttledPacketListeners = new List<Network.ThrottledPacketListener>();
  private static List<Network.RequestContext> m_inTransitRequests = new List<Network.RequestContext>();
  private static SortedDictionary<int, int> m_deferredMessageResponseMap = new SortedDictionary<int, int>() { { 305, 306 }, { 303, 304 }, { 205, 307 }, { 253, 252 }, { 314, 315 }, { 333, 333 } };
  private static SortedDictionary<int, int> m_deferredGetAccountInfoMessageResponseMap = new SortedDictionary<int, int>() { { 11, 233 }, { 6, 224 }, { 14, 241 }, { 18, 264 }, { 4, 232 }, { 12, 212 }, { 2, 202 }, { 3, 207 }, { 10, 231 }, { 15, 260 }, { 17, 262 }, { 23, 300 }, { 19, 271 }, { 8, 270 }, { 20, 278 }, { 21, 283 }, { 7, 236 }, { 27, 318 }, { 28, 325 } };
  public static readonly PlatformDependentValue<bool> LAUNCHES_WITH_BNET_APP = new PlatformDependentValue<bool>(PlatformCategory.OS) { PC = true, Mac = true, iOS = false, Android = false };
  public static readonly PlatformDependentValue<bool> TUTORIALS_WITHOUT_ACCOUNT = new PlatformDependentValue<bool>(PlatformCategory.OS) { PC = false, Mac = false, iOS = true, Android = true };
  private static bool s_shouldBeConnectedToAurora = true;
  private BattleNetLogSource m_logSource = new BattleNetLogSource("Network");
  private Map<int, List<Network.NetHandler>> m_netHandlers = new Map<int, List<Network.NetHandler>>();
  private bool m_loginWaiting = true;
  private DateTime lastCall = DateTime.Now;
  private DateTime lastCallReport = DateTime.Now;
  private Map<int, Network.TimeoutHandler> m_netTimeoutHandlers = new Map<int, Network.TimeoutHandler>();
  private Network.SpectatorInviteReceivedHandler m_spectatorInviteReceivedHandler = (Network.SpectatorInviteReceivedHandler) (param0 => {});
  private Map<BnetFeature, List<Network.BnetErrorListener>> m_featureBnetErrorListeners = new Map<BnetFeature, List<Network.BnetErrorListener>>();
  private List<Network.BnetErrorListener> m_globalBnetErrorListeners = new List<Network.BnetErrorListener>();
  public const int NoSubOption = -1;
  public const int NoPosition = 0;
  public const string CosmeticVersion = "7.0";
  public const string VersionPostfix = "";
  public const string DEFAULT_INTERNAL_ENVIRONMENT = "bn12-01.battle.net";
  public const string DEFAULT_PUBLIC_ENVIRONMENT = "us.actual.battle.net";
  private const int MIN_DEFERRED_WAIT = 30;
  public const int SEND_DECK_DATA_NO_HERO_ASSET_CHANGE = -1;
  public const int SEND_DECK_DATA_NO_CARD_BACK_CHANGE = -1;
  private const bool ReconnectAfterFailedPings = true;
  private const float ERROR_HANDLING_DELAY = 0.4f;
  private static IDispatcher s_dispatcherImpl;
  private static bool s_running;
  private DateTime m_loginStarted;
  private BnetGameType m_findingBnetGameType;
  private Network.QueueInfoHandler m_queueInfoHandler;
  private Network.GameQueueHandler m_gameQueueHandler;
  private bgs.types.GameServerInfo m_lastGameServerInfo;
  private string m_delayedError;
  private float m_timeBeforeAllowReset;
  private static ConnectAPI s_connectApi;
  private static int s_gameServerKeepAliveFrequencySeconds;
  private Network.BnetEventHandler m_bnetEventHandler;
  private Network.FriendsHandler m_friendsHandler;
  private Network.WhisperHandler m_whisperHandler;
  private Network.PresenceHandler m_presenceHandler;
  private Network.ShutdownHandler m_shutdownHandler;
  private Network.ChallengeHandler m_challengeHandler;
  private Network.GameServerDisconnectEvent m_gameServerDisconnectEventListener;

  public static List<BattleNetErrors> GameServerDisconnectEvents { get; private set; }

  public static int ProductVersion()
  {
    return 458752;
  }

  public static TimeSpan GetMaxDeferredWait()
  {
    return Network.MAX_DEFERRED_WAIT;
  }

  private static void ProcessRequestTimeouts()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Network.\u003CProcessRequestTimeouts\u003Ec__AnonStorey3CB timeoutsCAnonStorey3Cb = new Network.\u003CProcessRequestTimeouts\u003Ec__AnonStorey3CB();
    // ISSUE: reference to a compiler-generated field
    timeoutsCAnonStorey3Cb.now = DateTime.Now;
    // ISSUE: reference to a compiler-generated method
    Network.m_inTransitRequests.ForEach(new System.Action<Network.RequestContext>(timeoutsCAnonStorey3Cb.\u003C\u003Em__170));
    // ISSUE: reference to a compiler-generated method
    Network.m_inTransitRequests.RemoveAll(new Predicate<Network.RequestContext>(timeoutsCAnonStorey3Cb.\u003C\u003Em__171));
  }

  public static void AddPendingRequestTimeout(int requestId, int requestSubId)
  {
    if (!Network.ShouldBeConnectedToAurora())
      return;
    int num = 0;
    if ((requestId != 201 || !Network.m_deferredGetAccountInfoMessageResponseMap.TryGetValue(requestSubId, out num)) && !Network.m_deferredMessageResponseMap.TryGetValue(requestId, out num))
      return;
    Network.TimeoutHandler timeoutHandler = (Network.TimeoutHandler) null;
    if (Network.s_instance.m_netTimeoutHandlers.TryGetValue(num, out timeoutHandler))
      Network.m_inTransitRequests.Add(new Network.RequestContext(num, requestId, requestSubId, timeoutHandler));
    else
      Network.m_inTransitRequests.Add(new Network.RequestContext(num, requestId, requestSubId, new Network.TimeoutHandler(Network.OnRequestTimeout)));
  }

  private static void RemovePendingRequestTimeout(int pendingResponseId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    Network.m_inTransitRequests.RemoveAll(new Predicate<Network.RequestContext>(new Network.\u003CRemovePendingRequestTimeout\u003Ec__AnonStorey3CC()
    {
      pendingResponseId = pendingResponseId
    }.\u003C\u003Em__172));
  }

  private static void OnRequestTimeout(int pendingResponseId, int requestId, int requestSubId)
  {
    if (Network.m_deferredMessageResponseMap.ContainsValue(pendingResponseId) || Network.m_deferredGetAccountInfoMessageResponseMap.ContainsValue(pendingResponseId))
    {
      Debug.LogError((object) string.Format("OnRequestTimeout pending ID {0} {1} {2}", (object) pendingResponseId, (object) requestId, (object) requestSubId));
      FatalErrorMgr.Get().SetErrorCode("HS", "NT" + pendingResponseId.ToString(), requestId.ToString(), requestSubId.ToString());
      BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_ERROR_NETWORK_UNAVAILABLE, 2, FatalErrorMgr.Get().GetFormattedErrorCode());
      Network.Get().ShowBreakingNewsOrError("GLOBAL_ERROR_NETWORK_UNAVAILABLE_UNKNOWN", 0.0f);
    }
    else
    {
      Debug.LogError((object) string.Format("Unhandled OnRequestTimeout pending ID {0} {1} {2}", (object) pendingResponseId, (object) requestId, (object) requestSubId));
      FatalErrorMgr.Get().SetErrorCode("HS", "NU" + pendingResponseId.ToString(), requestId.ToString(), requestSubId.ToString());
      BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_ERROR_NETWORK_UNAVAILABLE, 3, FatalErrorMgr.Get().GetFormattedErrorCode());
      Network.Get().ShowBreakingNewsOrError("GLOBAL_ERROR_NETWORK_UNAVAILABLE_UNKNOWN", 0.0f);
    }
  }

  private static void OnGenericResponse()
  {
    Network.GenericResponse genericResponse = Network.GetGenericResponse();
    if (genericResponse == null)
    {
      Debug.LogError((object) string.Format("Login - GenericResponse parse error"));
    }
    else
    {
      if ((genericResponse.RequestId != 201 || !Network.m_deferredGetAccountInfoMessageResponseMap.ContainsKey(genericResponse.RequestSubId)) && !Network.m_deferredMessageResponseMap.ContainsKey(genericResponse.RequestId) || genericResponse.ResultCode == Network.GenericResponse.Result.REQUEST_IN_PROCESS)
        return;
      Debug.LogError((object) string.Format("Unhandled resultCode {0} for requestId {1}:{2}", (object) genericResponse.ResultCode, (object) genericResponse.RequestId, (object) genericResponse.RequestSubId));
      FatalErrorMgr.Get().SetErrorCode("HS", "NG" + genericResponse.ResultCode.ToString(), genericResponse.RequestId.ToString(), genericResponse.RequestSubId.ToString());
      BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_ERROR_NETWORK_UNAVAILABLE, 4, FatalErrorMgr.Get().GetFormattedErrorCode());
      Network.Get().ShowBreakingNewsOrError("GLOBAL_ERROR_NETWORK_UNAVAILABLE_UNKNOWN", 0.0f);
    }
  }

  public static bool IsRunning()
  {
    return Network.s_running;
  }

  public static double TimeSinceLastPong()
  {
    if (!Network.IsConnectedToGameServer() || Network.s_gameServerKeepAliveFrequencySeconds <= 0 || (Network.s_connectApi.GetTimeLastPingSent() <= 0.0 || Network.s_connectApi.GetTimeLastPingSent() <= Network.s_connectApi.GetTimeLastPingReceieved()))
      return 0.0;
    return (double) Time.realtimeSinceStartup - Network.s_connectApi.GetTimeLastPingReceieved();
  }

  private static void OnSubscribeResponse()
  {
    PegasusUtil.SubscribeResponse subscribeResponse = Network.s_connectApi.GetSubscribeResponse();
    if (subscribeResponse == null || !subscribeResponse.HasRequestMaxWaitSecs || subscribeResponse.RequestMaxWaitSecs < 30UL)
      return;
    Network.MAX_DEFERRED_WAIT = new TimeSpan(0, 0, (int) subscribeResponse.RequestMaxWaitSecs);
  }

  private static void OnAchievementNotifications()
  {
    AchieveManager.Get().OnAchievementNotifications(Network.s_connectApi.GetAchievementNotifications().AchievementNotifications_);
  }

  public static void Initialize(IDispatcher dispatcher)
  {
    Network.s_running = true;
    Network.s_dispatcherImpl = dispatcher;
    NetCache.Get().InitNetCache();
    Network.InitBattleNet(Network.s_dispatcherImpl);
    Network.s_instance.RegisterNetHandler((object) PegasusUtil.SubscribeResponse.PacketID.ID, new Network.NetHandler(Network.OnSubscribeResponse), (Network.TimeoutHandler) null);
    Network.s_instance.RegisterNetHandler((object) AchievementNotifications.PacketID.ID, new Network.NetHandler(Network.OnAchievementNotifications), (Network.TimeoutHandler) null);
    Network.s_instance.RegisterNetHandler((object) PegasusUtil.GenericResponse.PacketID.ID, new Network.NetHandler(Network.OnGenericResponse), (Network.TimeoutHandler) null);
  }

  public static void Reset()
  {
    NetCache.Get().Clear();
    Network.s_instance.m_delayedError = (string) null;
    Network.s_instance.m_timeBeforeAllowReset = 0.0f;
    Network instance = Network.s_instance;
    Network.s_instance = new Network();
    Network.s_instance.m_netHandlers = instance.m_netHandlers;
    Network.s_instance.m_gameQueueHandler = instance.m_gameQueueHandler;
    Network.s_instance.m_spectatorInviteReceivedHandler = instance.m_spectatorInviteReceivedHandler;
    Network.s_running = true;
    Network.CloseAll();
    string username = Network.GetUsername();
    string targetServer = Network.GetTargetServer();
    int port = Network.GetPort();
    ClientInterface clientInterface = (ClientInterface) new Network.HSClientInterface();
    SslParameters sslParams = Network.GetSSLParams();
    ICompressionProvider compressionProvider = (ICompressionProvider) new SharpZipCompressionProvider();
    IFileUtil fileUtil = (IFileUtil) new FileUtil(compressionProvider);
    IJsonSerializer jsonSerializer = (IJsonSerializer) new UnityJsonSerializer();
    LoggerInterface loggerInterface = (LoggerInterface) new BattleNetLogger();
    IRpcConnectionFactory rpcConnectionFactory = (IRpcConnectionFactory) new RpcConnectionFactory();
    if (!BattleNet.Reset((IBattleNet) new BattleNetCSharp(clientInterface, rpcConnectionFactory, compressionProvider, fileUtil, jsonSerializer, loggerInterface), ApplicationMgr.IsInternal(), username, targetServer, port, sslParams) && Network.ShouldBeConnectedToAurora())
      return;
    BnetParty.SetDisconnectedFromBattleNet();
    Network.s_connectApi.SetDisconnectedFromBattleNet();
    Network.InitializeConnectApi(Network.s_dispatcherImpl);
  }

  private static void InitializeConnectApi(IDispatcher dispatcher)
  {
    Network.s_errorList.Clear();
    if (Network.s_connectApi == null)
    {
      Network.GameServerDisconnectEvents = new List<BattleNetErrors>();
      Network.s_connectApi = new ConnectAPI(dispatcher);
    }
    Network.s_connectApi.SetGameStartState(GameStartState.Invalid);
  }

  public static void ApplicationPaused()
  {
    NetCache.NetCacheClientOptions netObject = NetCache.Get().GetNetObject<NetCache.NetCacheClientOptions>();
    if (netObject != null)
      netObject.DispatchClientOptionsToServer();
    BattleNet.ApplicationWasPaused();
  }

  public static void CloseAll()
  {
    if (Network.s_ackCardSeenPacket.CardDefs.Count != 0)
      Network.SendAckCardsSeen();
    Network.s_connectApi.Close();
  }

  public static void ApplicationUnpaused()
  {
    BattleNet.ApplicationWasUnpaused();
  }

  public static void Heartbeat()
  {
    if (!Network.s_running)
      return;
    Network.ProcessRequestTimeouts();
    NetCache.Get().Heartbeat();
    Network.ProcessConnectApiHeartbeat();
    StoreManager.Get().Heartbeat();
    if (AchieveManager.Get() != null)
      AchieveManager.Get().Heartbeat();
    NetCache.Get().CheckSeasonForRoll();
    TimeSpan span = DateTime.Now - Network.s_instance.lastCall;
    if (span < Network.PROCESS_WARNING || DateTime.Now - Network.s_instance.lastCallReport < Network.PROCESS_WARNING_REPORT_GAP)
      return;
    Network.s_instance.lastCallReport = DateTime.Now;
    Debug.LogWarning((object) string.Format("Network.ProcessNetwork not called for {0}", (object) TimeUtils.GetDevElapsedTimeString(span)));
  }

  private static void ProcessConnectApiHeartbeat()
  {
    Network.GetBattleNetPackets();
    int count = Network.s_errorList.Count;
    for (int index = 0; index < count; ++index)
    {
      Network.ConnectErrorParams error = Network.s_errorList[index];
      if (error == null)
        Debug.LogError((object) ("null error! " + (object) Network.s_errorList.Count));
      else if ((double) Time.realtimeSinceStartup >= (double) error.m_creationTime + 0.400000005960464)
      {
        Network.s_errorList.RemoveAt(index);
        --index;
        count = Network.s_errorList.Count;
        Error.AddFatal((ErrorParams) error);
      }
    }
    if (Network.s_connectApi.HasGameServerConnection())
    {
      Network.s_connectApi.UpdateGameServerConnection();
      Network.UpdatePingPong();
    }
    Network.s_connectApi.ProcessUtilPackets();
    if (!Network.s_connectApi.TryConnectDebugConsole())
      return;
    Network.s_connectApi.UpdateDebugConsole();
  }

  public static void AddErrorToList(Network.ConnectErrorParams errorParams)
  {
    Network.s_errorList.Add(errorParams);
  }

  private static void UpdatePingPong()
  {
    if (Network.s_gameServerKeepAliveFrequencySeconds <= 0)
      return;
    double totalSeconds = TimeUtils.GetElapsedTimeSinceEpoch(new DateTime?()).TotalSeconds;
    if (!Network.s_connectApi.IsConnectedToGameServer() || totalSeconds - Network.s_connectApi.GetTimeLastPingSent() <= (double) Network.s_gameServerKeepAliveFrequencySeconds)
      return;
    int pingsSinceLastPong = Network.s_connectApi.GetPingsSinceLastPong();
    if (Network.s_connectApi.GetTimeLastPingSent() <= Network.s_connectApi.GetTimeLastPingReceieved())
      Network.s_connectApi.SetTimeLastPingReceived(totalSeconds - 0.001);
    Network.s_connectApi.SetTimeLastPingSent(totalSeconds);
    Network.s_connectApi.SendPing();
    if (pingsSinceLastPong >= 3 && totalSeconds - Network.s_connectApi.GetTimeLastPingReceieved() > 10.0)
      Network.DisconnectFromGameServer();
    Network.s_connectApi.SetPingsSinceLastPong(pingsSinceLastPong + 1);
  }

  private static void GetBattleNetPackets()
  {
    GamesAPI.UtilResponse utilResponse;
    while ((utilResponse = BattleNet.NextUtilPacket()) != null)
    {
      bnet.protocol.attribute.Attribute attribute1 = utilResponse.m_response.AttributeList[0];
      bnet.protocol.attribute.Attribute attribute2 = utilResponse.m_response.AttributeList[1];
      int intValue = (int) attribute1.Value.IntValue;
      byte[] blobValue = attribute2.Value.BlobValue;
      Network.s_connectApi.DecodeAndProcessPacket(new PegasusPacket(intValue, blobValue.Length, (object) blobValue)
      {
        Context = utilResponse.m_context
      });
    }
  }

  public static void AppQuit()
  {
    if (!Network.s_running)
      return;
    NetCache.NetCacheClientOptions netObject = NetCache.Get().GetNetObject<NetCache.NetCacheClientOptions>();
    if (netObject != null)
      netObject.DispatchClientOptionsToServer();
    BIReport.Get().Report_AppQuit();
    Network.ConcedeIfReconnectDisabled();
    Network.s_instance.CancelFindGame();
    Network.CloseAll();
    BattleNet.AppQuit();
    PlayErrors.AppQuit();
    MemModule.AppQuit();
    BnetNearbyPlayerMgr.Get().Shutdown();
    Network.s_running = false;
  }

  public static void AppAbort()
  {
    if (!Network.s_running)
      return;
    NetCache.NetCacheClientOptions netObject = NetCache.Get().GetNetObject<NetCache.NetCacheClientOptions>();
    if (netObject != null)
      netObject.DispatchClientOptionsToServer();
    BIReport.Get().Report_AppQuit();
    Network.ConcedeIfReconnectDisabled();
    Network.s_instance.CancelFindGame();
    Network.CloseAll();
    BattleNet.AppQuit();
    PlayErrors.AppQuit();
    BnetNearbyPlayerMgr.Get().Shutdown();
    Network.s_running = false;
  }

  public static void ResetConnectionFailureCount()
  {
    Network.s_numConnectionFailures = 0;
  }

  private static void ConcedeIfReconnectDisabled()
  {
    if (ReconnectMgr.Get().IsReconnectEnabled())
      return;
    Network.AutoConcede();
  }

  public bool RegisterNetHandler(object enumId, Network.NetHandler handler, Network.TimeoutHandler timeoutHandler = null)
  {
    int key = (int) enumId;
    if (timeoutHandler != null)
    {
      if (this.m_netTimeoutHandlers.ContainsKey(key))
        return false;
      this.m_netTimeoutHandlers.Add(key, timeoutHandler);
    }
    List<Network.NetHandler> netHandlerList;
    if (this.m_netHandlers.TryGetValue(key, out netHandlerList))
    {
      if (netHandlerList.Contains(handler))
        return false;
    }
    else
    {
      netHandlerList = new List<Network.NetHandler>();
      this.m_netHandlers.Add(key, netHandlerList);
    }
    netHandlerList.Add(handler);
    return true;
  }

  public bool RemoveNetHandler(object enumId, Network.NetHandler handler)
  {
    List<Network.NetHandler> netHandlerList;
    return this.m_netHandlers.TryGetValue((int) enumId, out netHandlerList) && netHandlerList.Remove(handler);
  }

  public static void RegisterThrottledPacketListener(Network.ThrottledPacketListener listener)
  {
    if (Network.s_throttledPacketListeners.Contains(listener))
      return;
    Network.s_throttledPacketListeners.Add(listener);
  }

  public static void RemoveThrottledPacketListener(Network.ThrottledPacketListener listener)
  {
    Network.s_throttledPacketListeners.Remove(listener);
  }

  public void RegisterGameQueueHandler(Network.GameQueueHandler handler)
  {
    if (this.m_gameQueueHandler != null)
      Log.Net.Print("handler {0} would bash game queue handler {1}", new object[2]
      {
        (object) handler,
        (object) this.m_gameQueueHandler
      });
    else
      this.m_gameQueueHandler = handler;
  }

  public void RemoveGameQueueHandler(Network.GameQueueHandler handler)
  {
    if ((MulticastDelegate) this.m_gameQueueHandler != (MulticastDelegate) handler)
      Log.Net.Print("Removing game queue handler that is not active {0}", (object) handler);
    else
      this.m_gameQueueHandler = (Network.GameQueueHandler) null;
  }

  public void RegisterQueueInfoHandler(Network.QueueInfoHandler handler)
  {
    if (this.m_queueInfoHandler != null)
      Log.Net.Print("handler {0} would bash queue info handler {1}", new object[2]
      {
        (object) handler,
        (object) this.m_queueInfoHandler
      });
    else
      this.m_queueInfoHandler = handler;
  }

  public void RemoveQueueInfoHandler(Network.QueueInfoHandler handler)
  {
    if ((MulticastDelegate) this.m_queueInfoHandler != (MulticastDelegate) handler)
      Log.Net.Print("Removing queue info handler that is not active {0}", (object) handler);
    else
      this.m_queueInfoHandler = (Network.QueueInfoHandler) null;
  }

  public bool FakeHandleType(Enum enumId)
  {
    return this.FakeHandleType(Convert.ToInt32((object) enumId));
  }

  public bool FakeHandleType(int id)
  {
    if (Network.ShouldBeConnectedToAurora())
      return false;
    this.HandleType(id);
    return true;
  }

  private bool HandleType(int id)
  {
    Network.RemovePendingRequestTimeout(id);
    List<Network.NetHandler> netHandlerList;
    if (!this.m_netHandlers.TryGetValue(id, out netHandlerList) || netHandlerList.Count == 0)
    {
      if (!this.CanIgnoreUnhandledPacket(id))
        Debug.LogError((object) string.Format("Network.HandleType() - Received packet {0}, but there are no handlers for it.", (object) id));
      return false;
    }
    foreach (Network.NetHandler netHandler in netHandlerList.ToArray())
      netHandler();
    return true;
  }

  private bool CanIgnoreUnhandledPacket(int id)
  {
    switch (id)
    {
      case 15:
      case 116:
      case 254:
        return true;
      default:
        return false;
    }
  }

  private bool ProcessGameQueue()
  {
    QueueEvent queueEvent = BattleNet.GetQueueEvent();
    if (queueEvent == null)
      return false;
    switch (queueEvent.EventType)
    {
      case QueueEvent.Type.QUEUE_LEAVE:
      case QueueEvent.Type.QUEUE_DELAY_ERROR:
      case QueueEvent.Type.QUEUE_AMM_ERROR:
      case QueueEvent.Type.QUEUE_CANCEL:
      case QueueEvent.Type.QUEUE_GAME_STARTED:
      case QueueEvent.Type.ABORT_CLIENT_DROPPED:
        this.m_findingBnetGameType = BnetGameType.BGT_UNKNOWN;
        break;
    }
    if (this.m_gameQueueHandler == null)
      Debug.LogWarningFormat("m_gameQueueHandler is null in Network.ProcessGameQueue! event={0} server={1}:{2} gameHandle={3} clientHandle={4}", (object) queueEvent.EventType, (object) (queueEvent.GameServer != null ? queueEvent.GameServer.Address : "null"), (object) (queueEvent.GameServer != null ? queueEvent.GameServer.Port : 0), (object) (queueEvent.GameServer != null ? queueEvent.GameServer.GameHandle : 0), (object) (queueEvent.GameServer != null ? queueEvent.GameServer.ClientHandle : 0L));
    else
      this.m_gameQueueHandler(queueEvent);
    return true;
  }

  private bool ProcessGameServer()
  {
    bool flag = this.HandleType(Network.s_connectApi.NextGamePacketType());
    Network.s_connectApi.DropGamePacket();
    return flag;
  }

  private bool ProcessUtilServer()
  {
    bool flag = this.HandleType(Network.s_connectApi.NextUtilPacketType());
    Network.s_connectApi.DropUtilPacket();
    return flag;
  }

  private bool ProcessConsole()
  {
    bool flag = this.HandleType(Network.s_connectApi.NextDebugPacketType());
    Network.s_connectApi.DropDebugPacket();
    return flag;
  }

  public static Network.UnavailableReason GetHearthstoneUnavailable(bool gamePacket)
  {
    Network.UnavailableReason unavailableReason = new Network.UnavailableReason();
    if (gamePacket)
    {
      Deadend deadendGame = Network.s_connectApi.GetDeadendGame();
      unavailableReason.mainReason = deadendGame.Reply1;
      unavailableReason.subReason = deadendGame.Reply2;
      unavailableReason.extraData = deadendGame.Reply3;
    }
    else
    {
      DeadendUtil deadendUtil = Network.s_connectApi.GetDeadendUtil();
      unavailableReason.mainReason = deadendUtil.Reply1;
      unavailableReason.subReason = deadendUtil.Reply2;
      unavailableReason.extraData = deadendUtil.Reply3;
    }
    return unavailableReason;
  }

  public static void BuyCard(int assetId, TAG_PREMIUM premium, int count, int unitBuyPrice)
  {
    PegasusShared.CardDef cardDef = new PegasusShared.CardDef() { Asset = assetId };
    if (premium != TAG_PREMIUM.NORMAL)
      cardDef.Premium = (int) premium;
    Network.s_connectApi.BuyCard(cardDef, count, unitBuyPrice);
  }

  public static void SellCard(int assetId, TAG_PREMIUM premium, int count, int unitSellPrice, int currentCollectionCount)
  {
    PegasusShared.CardDef cardDef = new PegasusShared.CardDef() { Asset = assetId };
    if (premium != TAG_PREMIUM.NORMAL)
      cardDef.Premium = (int) premium;
    Network.s_connectApi.SellCard(cardDef, count, unitSellPrice, currentCollectionCount);
  }

  public static void CloseCardMarket()
  {
  }

  public static void GetAllClientOptions()
  {
    Network.s_connectApi.GetAllClientOptions();
  }

  public static void SetClientOptions(SetOptions packet)
  {
    Network.s_connectApi.SetClientOptions(packet);
  }

  public static Network.BnetLoginState BattleNetStatus()
  {
    return (Network.BnetLoginState) BattleNet.BattleNetStatus();
  }

  public static bool IsLoggedIn()
  {
    if (!BattleNet.IsInitialized())
      return false;
    return BattleNet.BattleNetStatus() == 4;
  }

  public bool HaveUnhandledPackets()
  {
    return Network.s_connectApi.HasUtilPackets() || Network.s_connectApi.HasGamePackets() || (Network.s_connectApi.HasDebugPackets() || BattleNet.GetNotificationCount() > 0);
  }

  public void ProcessNetwork()
  {
    if (!Network.s_running)
      return;
    this.lastCall = DateTime.Now;
    if (this.m_loginWaiting && this.lastCall - this.m_loginStarted > Network.LOGIN_TIMEOUT)
      this.m_loginWaiting = false;
    if (!Network.InitBattleNet(Network.s_dispatcherImpl) || MemModule.Process())
      return;
    Network.s_urlDownloader.Process();
    if (Network.ShouldBeConnectedToAurora())
      this.ProcessAurora();
    else
      this.ProcessDelayedError();
    if (this.ProcessGameQueue())
      return;
    if (Network.s_connectApi.HasGamePackets())
    {
      this.ProcessGameServer();
    }
    else
    {
      if (Network.GameServerDisconnectEvents != null && Network.GameServerDisconnectEvents.Count > 0)
      {
        if (this.m_gameServerDisconnectEventListener != null)
        {
          foreach (BattleNetErrors errorCode in Network.GameServerDisconnectEvents.ToArray())
            this.m_gameServerDisconnectEventListener(errorCode);
        }
        Network.GameServerDisconnectEvents.Clear();
      }
      if (Network.s_connectApi.HasUtilPackets())
        this.ProcessUtilServer();
      else if (Network.s_connectApi.HasDebugPackets())
        this.ProcessConsole();
      else
        this.ProcessQueuePosition();
    }
  }

  private static bool InitBattleNet(IDispatcher dispatcher)
  {
    bool flag = BattleNet.IsInitialized();
    if (!flag)
    {
      ClientInterface clientInterface = (ClientInterface) new Network.HSClientInterface();
      ICompressionProvider compressionProvider = (ICompressionProvider) new SharpZipCompressionProvider();
      IFileUtil fileUtil = (IFileUtil) new FileUtil(compressionProvider);
      IJsonSerializer jsonSerializer = (IJsonSerializer) new UnityJsonSerializer();
      LoggerInterface loggerInterface = (LoggerInterface) new BattleNetLogger();
      IRpcConnectionFactory rpcConnectionFactory = (IRpcConnectionFactory) new RpcConnectionFactory();
      IBattleNet battleNet = (IBattleNet) new BattleNetCSharp(clientInterface, rpcConnectionFactory, compressionProvider, fileUtil, jsonSerializer, loggerInterface);
      BattleNet.SetImpl(battleNet);
      string username = Network.GetUsername();
      string targetServer = Network.GetTargetServer();
      int port = Network.GetPort();
      SslParameters sslParams = Network.GetSSLParams();
      flag = BattleNet.Init(battleNet, ApplicationMgr.IsInternal(), username, targetServer, port, sslParams);
      if (flag || !Network.ShouldBeConnectedToAurora())
      {
        Network.Get().AddBnetErrorListener(BnetFeature.Auth, new Network.BnetErrorCallback(Network.Get().OnBnetAuthError));
        DebugConsole.Get().Init();
        Network.InitializeConnectApi(dispatcher);
      }
    }
    return flag;
  }

  public static bool ShouldBeConnectedToAurora()
  {
    if ((bool) Network.TUTORIALS_WITHOUT_ACCOUNT)
      return Network.s_shouldBeConnectedToAurora;
    return true;
  }

  public static void SetShouldBeConnectedToAurora(bool shouldBeConnected)
  {
    Network.s_shouldBeConnectedToAurora = shouldBeConnected;
  }

  public void ProcessQueuePosition()
  {
    bgs.types.QueueInfo queueInfo = new bgs.types.QueueInfo();
    BattleNet.GetQueueInfo(ref queueInfo);
    if (!queueInfo.changed || this.m_queueInfoHandler == null)
      return;
    this.m_queueInfoHandler(new Network.QueueInfo()
    {
      position = queueInfo.position,
      end = queueInfo.end,
      stdev = queueInfo.stdev
    });
  }

  public Network.BnetEventHandler GetBnetEventHandler()
  {
    return this.m_bnetEventHandler;
  }

  public void SetBnetStateHandler(Network.BnetEventHandler handler)
  {
    this.m_bnetEventHandler = handler;
  }

  public Network.FriendsHandler GetFriendsHandler()
  {
    return this.m_friendsHandler;
  }

  public void SetFriendsHandler(Network.FriendsHandler handler)
  {
    this.m_friendsHandler = handler;
  }

  public Network.WhisperHandler GetWhisperHandler()
  {
    return this.m_whisperHandler;
  }

  public void SetWhisperHandler(Network.WhisperHandler handler)
  {
    this.m_whisperHandler = handler;
  }

  public Network.PresenceHandler GetPresenceHandler()
  {
    return this.m_presenceHandler;
  }

  public void SetPresenceHandler(Network.PresenceHandler handler)
  {
    this.m_presenceHandler = handler;
  }

  public Network.ShutdownHandler GetShutdownHandler()
  {
    return this.m_shutdownHandler;
  }

  public void SetShutdownHandler(Network.ShutdownHandler handler)
  {
    this.m_shutdownHandler = handler;
  }

  public Network.ChallengeHandler GetChallengeHandler()
  {
    return this.m_challengeHandler;
  }

  public void SetChallengeHandler(Network.ChallengeHandler handler)
  {
    this.m_challengeHandler = handler;
  }

  public void SetSpectatorInviteReceivedHandler(Network.SpectatorInviteReceivedHandler handler)
  {
    this.m_spectatorInviteReceivedHandler = handler;
  }

  public void SetGameServerDisconnectEventListener(Network.GameServerDisconnectEvent handler)
  {
    this.m_gameServerDisconnectEventListener = handler;
  }

  public void RemoveGameServerDisconnectEventListener(Network.GameServerDisconnectEvent handler)
  {
    if (!((MulticastDelegate) this.m_gameServerDisconnectEventListener == (MulticastDelegate) handler))
      return;
    this.m_gameServerDisconnectEventListener = (Network.GameServerDisconnectEvent) null;
  }

  public void AddBnetErrorListener(BnetFeature feature, Network.BnetErrorCallback callback)
  {
    this.AddBnetErrorListener(feature, callback, (object) null);
  }

  public void AddBnetErrorListener(BnetFeature feature, Network.BnetErrorCallback callback, object userData)
  {
    Network.BnetErrorListener bnetErrorListener = new Network.BnetErrorListener();
    bnetErrorListener.SetCallback(callback);
    bnetErrorListener.SetUserData(userData);
    List<Network.BnetErrorListener> bnetErrorListenerList;
    if (!this.m_featureBnetErrorListeners.TryGetValue(feature, out bnetErrorListenerList))
    {
      bnetErrorListenerList = new List<Network.BnetErrorListener>();
      this.m_featureBnetErrorListeners.Add(feature, bnetErrorListenerList);
    }
    else if (bnetErrorListenerList.Contains(bnetErrorListener))
      return;
    bnetErrorListenerList.Add(bnetErrorListener);
  }

  public void AddBnetErrorListener(Network.BnetErrorCallback callback)
  {
    this.AddBnetErrorListener(callback, (object) null);
  }

  public void AddBnetErrorListener(Network.BnetErrorCallback callback, object userData)
  {
    Network.BnetErrorListener bnetErrorListener = new Network.BnetErrorListener();
    bnetErrorListener.SetCallback(callback);
    bnetErrorListener.SetUserData(userData);
    if (this.m_globalBnetErrorListeners.Contains(bnetErrorListener))
      return;
    this.m_globalBnetErrorListeners.Add(bnetErrorListener);
  }

  public bool RemoveBnetErrorListener(BnetFeature feature, Network.BnetErrorCallback callback)
  {
    return this.RemoveBnetErrorListener(feature, callback, (object) null);
  }

  public bool RemoveBnetErrorListener(BnetFeature feature, Network.BnetErrorCallback callback, object userData)
  {
    List<Network.BnetErrorListener> bnetErrorListenerList;
    if (!this.m_featureBnetErrorListeners.TryGetValue(feature, out bnetErrorListenerList))
      return false;
    Network.BnetErrorListener bnetErrorListener = new Network.BnetErrorListener();
    bnetErrorListener.SetCallback(callback);
    bnetErrorListener.SetUserData(userData);
    return bnetErrorListenerList.Remove(bnetErrorListener);
  }

  public bool RemoveBnetErrorListener(Network.BnetErrorCallback callback)
  {
    return this.RemoveBnetErrorListener(callback, (object) null);
  }

  public bool RemoveBnetErrorListener(Network.BnetErrorCallback callback, object userData)
  {
    Network.BnetErrorListener bnetErrorListener = new Network.BnetErrorListener();
    bnetErrorListener.SetCallback(callback);
    bnetErrorListener.SetUserData(userData);
    return this.m_globalBnetErrorListeners.Remove(bnetErrorListener);
  }

  public static void SendUnsubcribeRequest(Unsubscribe packet, int systemChannel)
  {
    Network.s_connectApi.SendUnsubscribeRequest(packet, systemChannel);
  }

  public void ProcessAurora()
  {
    BattleNet.ProcessAurora();
    this.ProcessBnetEvents();
    if (Network.IsLoggedIn())
    {
      this.ProcessPresence();
      this.ProcessFriends();
      this.ProcessWhispers();
      this.ProcessChallenges();
      this.ProcessParties();
      this.ProcessBroadcasts();
      this.ProcessNotifications();
      BnetNearbyPlayerMgr.Get().Update();
    }
    this.ProcessErrors();
  }

  private void ProcessBnetEvents()
  {
    int bnetEventsSize = BattleNet.GetBnetEventsSize();
    if (bnetEventsSize <= 0 || this.m_bnetEventHandler == null)
      return;
    BattleNet.BnetEvent[] bnetEventArray = new BattleNet.BnetEvent[bnetEventsSize];
    BattleNet.GetBnetEvents(bnetEventArray);
    this.m_bnetEventHandler(bnetEventArray);
    BattleNet.ClearBnetEvents();
  }

  private void ProcessWhispers()
  {
    WhisperInfo info = new WhisperInfo();
    BattleNet.GetWhisperInfo(ref info);
    if (info.whisperSize <= 0 || this.m_whisperHandler == null)
      return;
    BnetWhisper[] whispers = new BnetWhisper[info.whisperSize];
    BattleNet.GetWhispers(whispers);
    this.m_whisperHandler(whispers);
    BattleNet.ClearWhispers();
  }

  private void ProcessParties()
  {
    BnetParty.Process();
  }

  private void ProcessBroadcasts()
  {
    int shutdownMinutes = BattleNet.GetShutdownMinutes();
    if (shutdownMinutes <= 0 || this.m_shutdownHandler == null)
      return;
    this.m_shutdownHandler(shutdownMinutes);
  }

  private void ProcessNotifications()
  {
    int notificationCount = BattleNet.GetNotificationCount();
    if (notificationCount <= 0)
      return;
    BnetNotification[] notifications = new BnetNotification[notificationCount];
    BattleNet.GetNotifications(notifications);
    BattleNet.ClearNotifications();
    foreach (BnetNotification bnetNotification in notifications)
    {
      string notificationType = bnetNotification.NotificationType;
      if (notificationType != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (Network.\u003C\u003Ef__switch\u0024mapB1 == null)
        {
          // ISSUE: reference to a compiler-generated field
          Network.\u003C\u003Ef__switch\u0024mapB1 = new Dictionary<string, int>(1)
          {
            {
              "WTCG.UtilNotificationMessage",
              0
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (Network.\u003C\u003Ef__switch\u0024mapB1.TryGetValue(notificationType, out num) && num == 0)
        {
          PegasusPacket packet = new PegasusPacket(bnetNotification.MessageType, 0, bnetNotification.MessageSize, (object) bnetNotification.BlobMessage);
          Network.s_connectApi.DecodeAndProcessPacket(packet);
        }
      }
    }
  }

  private void ProcessFriends()
  {
    FriendsInfo info = new FriendsInfo();
    BattleNet.GetFriendsInfo(ref info);
    if (info.updateSize == 0 || this.m_friendsHandler == null)
      return;
    FriendsUpdate[] updates = new FriendsUpdate[info.updateSize];
    BattleNet.GetFriendsUpdates(updates);
    this.m_friendsHandler(updates);
    BattleNet.ClearFriendsUpdates();
  }

  private void ProcessPresence()
  {
    int length = BattleNet.PresenceSize();
    if (length == 0 || this.m_presenceHandler == null)
      return;
    PresenceUpdate[] updates = new PresenceUpdate[length];
    BattleNet.GetPresence(updates);
    this.m_presenceHandler(updates);
    BattleNet.ClearPresence();
  }

  private void ProcessChallenges()
  {
    int length = BattleNet.NumChallenges();
    if (length == 0 || this.m_challengeHandler == null)
      return;
    ChallengeInfo[] challenges = new ChallengeInfo[length];
    BattleNet.GetChallenges(challenges);
    this.m_challengeHandler(challenges);
    BattleNet.ClearChallenges();
  }

  private void ProcessErrors()
  {
    this.ProcessDelayedError();
    BnetErrorInfo[] errors;
    if (Network.s_connectApi.HasErrors())
    {
      errors = new BnetErrorInfo[1]
      {
        new BnetErrorInfo(BnetFeature.Games, BnetFeatureEvent.Games_OnClientRequest, BattleNetErrors.ERROR_GAME_UTILITY_SERVER_NO_SERVER)
      };
    }
    else
    {
      int errorsCount = BattleNet.GetErrorsCount();
      if (errorsCount == 0)
        return;
      errors = new BnetErrorInfo[errorsCount];
      BattleNet.GetErrors(errors);
    }
    for (int index = 0; index < errors.Length; ++index)
    {
      BnetErrorInfo bnetErrorInfo = errors[index];
      BattleNetErrors error = bnetErrorInfo.GetError();
      string str = !ApplicationMgr.IsPublic() ? error.ToString() : string.Empty;
      if (!Network.s_connectApi.HasErrors() && Network.s_connectApi.ShouldIgnoreError(bnetErrorInfo))
      {
        if (!ApplicationMgr.IsPublic())
          Log.BattleNet.PrintDebug("BattleNet/ConnectDLL generated error={0} {1} (can ignore)", new object[2]
          {
            (object) (int) error,
            (object) str
          });
      }
      else if (!this.FireErrorListeners(bnetErrorInfo) && (Network.s_connectApi.HasErrors() || !this.OnIgnorableBnetError(bnetErrorInfo)))
        this.OnFatalBnetError(bnetErrorInfo);
    }
    BattleNet.ClearErrors();
  }

  private bool FireErrorListeners(BnetErrorInfo info)
  {
    bool flag = false;
    List<Network.BnetErrorListener> bnetErrorListenerList;
    if (this.m_featureBnetErrorListeners.TryGetValue(info.GetFeature(), out bnetErrorListenerList) && bnetErrorListenerList.Count > 0)
    {
      foreach (Network.BnetErrorListener bnetErrorListener in bnetErrorListenerList.ToArray())
        flag = bnetErrorListener.Fire(info) || flag;
    }
    foreach (Network.BnetErrorListener bnetErrorListener in this.m_globalBnetErrorListeners.ToArray())
      flag = bnetErrorListener.Fire(info) || flag;
    return flag;
  }

  public void ShowConnectionFailureError(string error)
  {
    this.ShowBreakingNewsOrError(error, this.DelayForConnectionFailures(Network.s_numConnectionFailures++));
  }

  public void ShowBreakingNewsOrError(string error, float timeBeforeAllowReset = 0)
  {
    this.m_delayedError = error;
    this.m_timeBeforeAllowReset = timeBeforeAllowReset;
    Debug.LogError((object) string.Format("Setting delayed error for Error Message: {0} and prevent reset for {1} seconds", (object) error, (object) timeBeforeAllowReset));
    this.ProcessDelayedError();
  }

  private bool ProcessDelayedError()
  {
    if (this.m_delayedError == null)
      return false;
    bool flag = false;
    if (BreakingNews.Get().GetStatus() != BreakingNews.Status.Fetching)
    {
      ErrorParams parms = new ErrorParams();
      parms.m_delayBeforeNextReset = this.m_timeBeforeAllowReset;
      string text = BreakingNews.Get().GetText();
      if (string.IsNullOrEmpty(text))
        parms.m_message = BreakingNews.Get().GetError() == null || !(this.m_delayedError == "GLOBAL_ERROR_NETWORK_NO_GAME_SERVER") ? (!ApplicationMgr.IsInternal() || !(this.m_delayedError == "GLOBAL_ERROR_UNKNOWN_ERROR") ? GameStrings.Format(this.m_delayedError) : "Dev Message: Could not connect to Battle.net, and there was no breaking news to display. Maybe Battle.net is down?") : GameStrings.Format("GLOBAL_ERROR_NETWORK_NO_CONNECTION");
      else
        parms.m_message = GameStrings.Format("GLOBAL_MOBILE_ERROR_BREAKING_NEWS", (object) text);
      Error.AddFatal(parms);
      this.m_delayedError = (string) null;
      this.m_timeBeforeAllowReset = 0.0f;
      flag = true;
    }
    return flag;
  }

  public bool OnIgnorableBnetError(BnetErrorInfo info)
  {
    BattleNetErrors error = info.GetError();
    BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_IGNORABLE_BNET_ERROR, (int) error, (string) null);
    switch (error)
    {
      case BattleNetErrors.ERROR_OK:
        return true;
      case BattleNetErrors.ERROR_WAITING_FOR_DEPENDENCY:
        return true;
      case BattleNetErrors.ERROR_INVALID_TARGET_ID:
        if (info.GetFeature() == BnetFeature.Friends)
          return info.GetFeatureEvent() == BnetFeatureEvent.Friends_OnSendInvitation;
        return false;
      case BattleNetErrors.ERROR_API_NOT_READY:
        return info.GetFeature() == BnetFeature.Presence;
      case BattleNetErrors.ERROR_INCOMPLETE_PROFANITY_FILTERS:
        Locale locale = Localization.GetLocale();
        if (locale == Locale.zhCN)
          this.m_logSource.LogError("Network.IgnoreBnetError() - error={0} locale={1}", (object) info, (object) locale);
        return true;
      case BattleNetErrors.ERROR_TARGET_OFFLINE:
        return true;
      case BattleNetErrors.ERROR_PRESENCE_TEMPORARY_OUTAGE:
        return true;
      case BattleNetErrors.ERROR_GAME_UTILITY_SERVER_NO_SERVER:
        this.m_logSource.LogError("Network.IgnoreBnetError() - error={0}", (object) info);
        return true;
      default:
        return false;
    }
  }

  public void OnFatalBnetError(BnetErrorInfo info)
  {
    BattleNetErrors error1 = info.GetError();
    this.m_logSource.LogError("Network.OnFatalBnetError() - error={0}", (object) info);
    BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_FATAL_BNET_ERROR, (int) error1, (string) null);
    BattleNetErrors battleNetErrors = error1;
    switch (battleNetErrors)
    {
      case BattleNetErrors.ERROR_RPC_PEER_UNKNOWN:
        string error2;
        if (ApplicationMgr.IsInternal())
        {
          error2 = string.Format("Unhandled Bnet Error: {0}", (object) info);
        }
        else
        {
          Debug.LogError((object) string.Format("Unhandled Bnet Error: {0}", (object) info));
          error2 = GameStrings.Format("GLOBAL_ERROR_UNKNOWN_ERROR");
        }
        BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_ERROR_UNKNOWN_ERROR, 2, info.ToString());
        this.ShowConnectionFailureError(error2);
        break;
      case BattleNetErrors.ERROR_RPC_PEER_UNAVAILABLE:
        BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_ERROR_UNKNOWN_ERROR, 1, info.ToString());
        this.ShowConnectionFailureError("GLOBAL_ERROR_UNKNOWN_ERROR");
        Log.JMac.Print(LogLevel.Warning, string.Format("ERROR_RPC_PEER_UNAVAILABLE - {0} connection failures.", (object) Network.s_numConnectionFailures), new object[0]);
        break;
      case BattleNetErrors.ERROR_RPC_PEER_DISCONNECTED:
        this.ShowConnectionFailureError("GLOBAL_ERROR_NETWORK_DISCONNECT");
        break;
      case BattleNetErrors.ERROR_RPC_REQUEST_TIMED_OUT:
        this.ShowConnectionFailureError("GLOBAL_ERROR_NETWORK_UTIL_TIMEOUT");
        break;
      case BattleNetErrors.ERROR_RPC_CONNECTION_TIMED_OUT:
        this.ShowConnectionFailureError("GLOBAL_ERROR_NETWORK_CONNECTION_TIMEOUT");
        break;
      case BattleNetErrors.ERROR_RPC_QUOTA_EXCEEDED:
        Error.AddFatalLoc("GLOBAL_ERROR_NETWORK_SPAM");
        break;
      default:
        if (battleNetErrors != BattleNetErrors.ERROR_GAME_ACCOUNT_BANNED)
        {
          if (battleNetErrors != BattleNetErrors.ERROR_GAME_ACCOUNT_SUSPENDED)
          {
            if (battleNetErrors != BattleNetErrors.ERROR_SESSION_DUPLICATE)
            {
              if (battleNetErrors != BattleNetErrors.ERROR_SESSION_DISCONNECTED)
              {
                if (battleNetErrors != BattleNetErrors.ERROR_DENIED)
                {
                  if (battleNetErrors != BattleNetErrors.ERROR_PARENTAL_CONTROL_RESTRICTION)
                  {
                    if (battleNetErrors != BattleNetErrors.ERROR_SERVER_IS_PRIVATE)
                    {
                      if (battleNetErrors != BattleNetErrors.ERROR_PHONE_LOCK)
                      {
                        if (battleNetErrors != BattleNetErrors.ERROR_LOGON_WEB_VERIFY_TIMEOUT)
                        {
                          if (battleNetErrors == BattleNetErrors.ERROR_RISK_ACCOUNT_LOCKED)
                          {
                            WebAuth.DeleteStoredToken();
                            WebAuth.DeleteCookies();
                            Error.AddFatalLoc("GLOBAL_ERROR_NETWORK_RISK_ACCOUNT_LOCKED");
                            break;
                          }
                          goto case BattleNetErrors.ERROR_RPC_PEER_UNKNOWN;
                        }
                        else
                        {
                          this.ShowConnectionFailureError("GLOBAL_MOBILE_ERROR_LOGON_WEB_TIMEOUT");
                          break;
                        }
                      }
                      else
                      {
                        Error.AddFatalLoc("GLOBAL_ERROR_NETWORK_PHONE_LOCK");
                        break;
                      }
                    }
                    else
                    {
                      BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_ERROR, BIReport.TelemetryEvent.EVENT_ERROR_UNKNOWN_ERROR, 0, info.ToString());
                      this.ShowConnectionFailureError("GLOBAL_ERROR_UNKNOWN_ERROR");
                      Log.JMac.Print(LogLevel.Warning, string.Format("ERROR_SERVER_IS_PRIVATE - {0} connection failures.", (object) Network.s_numConnectionFailures), new object[0]);
                      break;
                    }
                  }
                  else
                  {
                    WebAuth.DeleteStoredToken();
                    WebAuth.DeleteCookies();
                    Error.AddFatalLoc("GLOBAL_ERROR_NETWORK_PARENTAL_CONTROLS");
                    break;
                  }
                }
                else
                {
                  this.ShowConnectionFailureError("GLOBAL_ERROR_NETWORK_LOGIN_FAILURE");
                  break;
                }
              }
              else
              {
                Error.AddFatalLoc("GLOBAL_ERROR_NETWORK_DISCONNECT");
                break;
              }
            }
            else
            {
              Error.AddFatalLoc("GLOBAL_ERROR_NETWORK_DUPLICATE_LOGIN");
              break;
            }
          }
          else
          {
            WebAuth.DeleteStoredToken();
            WebAuth.DeleteCookies();
            Error.AddFatalLoc("GLOBAL_ERROR_NETWORK_ACCOUNT_SUSPENDED");
            break;
          }
        }
        else
        {
          WebAuth.DeleteStoredToken();
          WebAuth.DeleteCookies();
          Error.AddFatalLoc("GLOBAL_ERROR_NETWORK_ACCOUNT_BANNED");
          break;
        }
    }
  }

  private float DelayForConnectionFailures(int numFailures)
  {
    float num = (float) (new System.Random().NextDouble() * 3.0) + 3.5f;
    return (float) Math.Min(numFailures, 3) * num;
  }

  public void AnswerChallenge(ulong challengeID, string answer)
  {
    BattleNet.AnswerChallenge(challengeID, answer);
  }

  public void CancelChallenge(ulong challengeID)
  {
    BattleNet.CancelChallenge(challengeID);
  }

  private bool OnBnetAuthError(BnetErrorInfo info, object userData)
  {
    return false;
  }

  public static void AcceptFriendInvite(BnetInvitationId inviteid)
  {
    BattleNet.ManageFriendInvite(1, inviteid.GetVal());
  }

  public static void RevokeFriendInvite(BnetInvitationId inviteid)
  {
    BattleNet.ManageFriendInvite(2, inviteid.GetVal());
  }

  public static void DeclineFriendInvite(BnetInvitationId inviteid)
  {
    BattleNet.ManageFriendInvite(3, inviteid.GetVal());
  }

  public static void IgnoreFriendInvite(BnetInvitationId inviteid)
  {
    BattleNet.ManageFriendInvite(4, inviteid.GetVal());
  }

  private static void SendFriendInvite(string sender, string target, bool byEmail)
  {
    BattleNet.SendFriendInvite(sender, target, byEmail);
  }

  public static void SendFriendInviteByEmail(string sender, string target)
  {
    Network.SendFriendInvite(sender, target, true);
  }

  public static void SendFriendInviteByBattleTag(string sender, string target)
  {
    Network.SendFriendInvite(sender, target, false);
  }

  public static void RemoveFriend(BnetAccountId id)
  {
    BattleNet.RemoveFriend(id);
  }

  public static void SendWhisper(BnetGameAccountId gameAccount, string message)
  {
    BattleNet.SendWhisper(gameAccount, message);
  }

  public void GotoGameServer(bgs.types.GameServerInfo info, bool reconnecting)
  {
    this.m_lastGameServerInfo = info;
    if (Network.s_connectApi.GetGameStartState() != GameStartState.Invalid)
    {
      Error.AddDevFatal("GotoGameServer() was called when we're already waiting for a game to start.");
    }
    else
    {
      string address = info.Address;
      int port = Vars.Key("Application.GameServerPortOverride").GetInt(info.Port);
      Debug.LogFormat("Network.GotoGameServer -- address= " + address + ":" + (object) port + ", game=" + (object) info.GameHandle + ", client=" + (object) info.ClientHandle + ", spectateKey=" + info.SpectatorPassword + " reconnecting=" + (object) reconnecting);
      if (address == null)
        return;
      if (string.IsNullOrEmpty(address) || port == 0 || info.GameHandle == 0 && Network.ShouldBeConnectedToAurora())
        Debug.LogWarning((object) ("Network.GotoGameServer: ERROR in ServerInfo address= " + address + ":" + (object) port + ",    game=" + (object) info.GameHandle + ", client=" + (object) info.ClientHandle + " reconnecting=" + (object) reconnecting));
      Network.s_gameServerKeepAliveFrequencySeconds = 0;
      Network.s_gameConceded = false;
      Network.s_disconnectRequested = false;
      Network.s_gameServerKeepAliveFrequencySeconds = 0;
      Network.s_connectApi.SetTimeLastPingSent(0.0);
      Network.s_connectApi.SetTimeLastPingReceived(0.0);
      Network.s_connectApi.SetPingsSinceLastPong(0);
      if (Network.GameServerDisconnectEvents != null)
        Network.GameServerDisconnectEvents.Clear();
      if (!Network.s_connectApi.GotoGameServer(address, port))
        return;
      Network.SendGameServerHandshake(info);
      Network.s_connectApi.SetGameStartState(!reconnecting ? GameStartState.InitialStart : GameStartState.Reconnecting);
    }
  }

  public void SpectateSecondPlayer(bgs.types.GameServerInfo info)
  {
    info.SpectatorMode = true;
    if (!Network.IsConnectedToGameServer())
      this.GotoGameServer(info, false);
    else
      Network.SendGameServerHandshake(info);
  }

  public bool RetryGotoGameServer()
  {
    if (Network.s_connectApi.GetGameStartState() == GameStartState.Invalid)
      return false;
    Network.SendGameServerHandshake(this.m_lastGameServerInfo);
    return true;
  }

  public bgs.types.GameServerInfo GetLastGameServerJoined()
  {
    return this.m_lastGameServerInfo;
  }

  public void ClearLastGameServerJoined()
  {
    this.m_lastGameServerInfo = (bgs.types.GameServerInfo) null;
  }

  public static Network Get()
  {
    return Network.s_instance;
  }

  public static string GetUsername()
  {
    string str = (string) null;
    try
    {
      str = Network.GetStoredUserName();
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("Exception while loading settings: " + ex.Message));
    }
    if (str == null)
      str = Vars.Key("Aurora.Username").GetStr("NOT_PROVIDED_PLEASE_PROVIDE_VIA_CONFIG");
    if (str != null && str.IndexOf("@") == -1)
      str += "@blizzard.com";
    return str;
  }

  public static string GetTargetServer()
  {
    bool flag = Vars.Key("Aurora.Env.Override").GetInt(0) != 0;
    string def = "default";
    string str = (string) null;
    if (flag)
    {
      str = Vars.Key("Aurora.Env").GetStr(def);
      if (string.IsNullOrEmpty(str))
        str = (string) null;
    }
    if (str == null)
      str = BattleNet.GetConnectionString();
    if (str == null)
    {
      string launchOption = BattleNet.GetLaunchOption("REGION", false);
      if (!string.IsNullOrEmpty(launchOption))
      {
        string key = launchOption;
        if (key != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (Network.\u003C\u003Ef__switch\u0024mapB2 == null)
          {
            // ISSUE: reference to a compiler-generated field
            Network.\u003C\u003Ef__switch\u0024mapB2 = new Dictionary<string, int>(5)
            {
              {
                "US",
                0
              },
              {
                "XX",
                1
              },
              {
                "EU",
                2
              },
              {
                "CN",
                3
              },
              {
                "KR",
                4
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (Network.\u003C\u003Ef__switch\u0024mapB2.TryGetValue(key, out num))
          {
            switch (num)
            {
              case 0:
                str = "us.actual.battle.net";
                goto label_18;
              case 1:
                str = "beta.actual.battle.net";
                goto label_18;
              case 2:
                str = "eu.actual.battle.net";
                goto label_18;
              case 3:
                str = "cn.actual.battle.net";
                goto label_18;
              case 4:
                str = "kr.actual.battle.net";
                goto label_18;
            }
          }
        }
        str = def;
      }
    }
label_18:
    if (str.ToLower() == def)
      str = "bn11-01.battle.net";
    return str;
  }

  public static int GetPort()
  {
    int num = 0;
    if (Vars.Key("Aurora.Env.Override").GetInt(0) != 0)
      num = Vars.Key("Aurora.Port").GetInt(0);
    if (num == 0)
      num = 1119;
    return num;
  }

  private static SslParameters GetSSLParams()
  {
    SslParameters sslParameters = new SslParameters();
    TextAsset textAsset = (TextAsset) Resources.Load("SSLCert/ssl_cert_bundle");
    if ((UnityEngine.Object) textAsset != (UnityEngine.Object) null)
      sslParameters.bundleSettings.bundle = new SslCertBundle(textAsset.bytes);
    sslParameters.bundleSettings.bundleDownloadConfig.numRetries = 3;
    sslParameters.bundleSettings.bundleDownloadConfig.timeoutMs = -1;
    return sslParameters;
  }

  public static bool IsVersionInt()
  {
    return Network.GetIsVersionIntFromConfig();
  }

  public static bool GetIsVersionIntFromConfig()
  {
    string str = Vars.Key("Aurora.Version.Source").GetStr("undefined");
    if (str == "undefined")
      str = "product";
    return str == "product" || str == "int" || !(str == "string");
  }

  public static string GetVersion()
  {
    return Network.GetVersionFromConfig();
  }

  private static string GetVersionFromConfig()
  {
    string str1 = Vars.Key("Aurora.Version.Source").GetStr("undefined");
    if (str1 == "undefined")
      str1 = "product";
    string str2;
    if (str1 == "product")
      str2 = Network.ProductVersion().ToString();
    else if (str1 == "int")
    {
      int maxValue = int.MaxValue;
      int num = Vars.Key("Aurora.Version.Int").GetInt(maxValue);
      if (num == maxValue)
        Debug.LogError((object) "Aurora.Version.Int undefined");
      str2 = num.ToString();
    }
    else if (str1 == "string")
    {
      string def = "undefined";
      str2 = Vars.Key("Aurora.Version.String").GetStr(def);
      if (str2 == def)
        Debug.LogError((object) "Aurora.Version.String undefined");
    }
    else
    {
      Debug.LogError((object) ("unknown version source: " + str1));
      str2 = "0";
    }
    return str2;
  }

  public void OnLoginStarted()
  {
    Network.s_connectApi.OnLoginStarted();
    this.m_loginStarted = DateTime.Now;
  }

  public static void DoLoginUpdate()
  {
    string referralSource = Vars.Key("Application.Referral").GetStr("none");
    if (referralSource.Equals("none"))
    {
      if (PlatformSettings.OS == OSCategory.PC || PlatformSettings.OS == OSCategory.Mac)
        referralSource = "Battle.net";
      else if (PlatformSettings.OS == OSCategory.iOS)
        referralSource = "AppleAppStore";
      else if (PlatformSettings.OS == OSCategory.Android)
      {
        switch (ApplicationMgr.GetAndroidStore())
        {
          case AndroidStore.GOOGLE:
            referralSource = "GooglePlay";
            break;
          case AndroidStore.AMAZON:
            referralSource = "AmazonAppStore";
            break;
        }
      }
    }
    Network.s_connectApi.DoLoginUpdate(referralSource);
  }

  public static void OnStartupPacketSequenceComplete()
  {
    Network.s_connectApi.OnStartupPacketSequenceComplete();
  }

  public bool IsFindingGame()
  {
    return this.m_findingBnetGameType != BnetGameType.BGT_UNKNOWN;
  }

  public BnetGameType GetFindingBnetGameType()
  {
    return this.m_findingBnetGameType;
  }

  public static BnetGameType TranslateGameTypeToBnet(GameType gameType, FormatType formatType, int missionId)
  {
    GameType gameType1 = gameType;
    switch (gameType1)
    {
      case GameType.GT_VS_AI:
        return BnetGameType.BGT_VS_AI;
      case GameType.GT_VS_FRIEND:
        return BnetGameType.BGT_FRIENDS;
      case GameType.GT_TUTORIAL:
        return BnetGameType.BGT_TUTORIAL;
      case GameType.GT_ARENA:
        return BnetGameType.BGT_ARENA;
      case GameType.GT_RANKED:
        return formatType != FormatType.FT_STANDARD ? BnetGameType.BGT_RANKED_WILD : BnetGameType.BGT_RANKED_STANDARD;
      case GameType.GT_CASUAL:
        if (formatType != FormatType.FT_STANDARD)
          return BnetGameType.BGT_CASUAL_WILD;
        return NetCache.Get().GetNetObject<NetCache.NetCachePlayQueue>().GameType == BnetGameType.BGT_NEWBIE ? BnetGameType.BGT_NEWBIE : BnetGameType.BGT_CASUAL_STANDARD;
      default:
        if (gameType1 == GameType.GT_TAVERNBRAWL)
        {
          if (GameUtils.IsAIMission(missionId))
            return BnetGameType.BGT_TAVERNBRAWL_1P_VERSUS_AI;
          return GameUtils.IsCoopMission(missionId) ? BnetGameType.BGT_TAVERNBRAWL_2P_COOP : BnetGameType.BGT_TAVERNBRAWL_PVP;
        }
        Error.AddDevFatal("Network.TranslateGameTypeToBnet() - do not know how to translate {0}", (object) gameType);
        return BnetGameType.BGT_UNKNOWN;
    }
  }

  public void FindGame(GameType gameType, FormatType formatType, int scenarioId, long deckId, long aiDeckId)
  {
    if (gameType == GameType.GT_VS_FRIEND)
    {
      Error.AddDevFatal("Network.FindGame() - friend games must go through the Party API");
    }
    else
    {
      BnetGameType bnet = Network.TranslateGameTypeToBnet(gameType, formatType, scenarioId);
      if (bnet == BnetGameType.BGT_UNKNOWN)
        return;
      this.m_findingBnetGameType = bnet;
      bool flag = Network.RequiresScenarioIdAttribute(bnet);
      byte[] byteArray = Guid.NewGuid().ToByteArray();
      Log.BattleNet.PrintInfo("FindGame type={0} scenario={1} deck={2} aideck={3} setScenId={4} request_guid={5}", (object) bnet, (object) scenarioId, (object) deckId, (object) aiDeckId, (object) (!flag ? 0 : 1), (object) (byteArray != null ? byteArray.ToHexString() : "null"));
      bnet.protocol.game_master.Player player = new bnet.protocol.game_master.Player();
      Identity val1 = new Identity();
      val1.SetGameAccountId(BnetEntityId.CreateForProtocol((BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId()));
      player.SetIdentity(val1);
      player.AddAttribute(ProtocolHelper.CreateAttribute("type", (long) bnet));
      player.AddAttribute(ProtocolHelper.CreateAttribute("scenario", (long) scenarioId));
      player.AddAttribute(ProtocolHelper.CreateAttribute("deck", deckId));
      player.AddAttribute(ProtocolHelper.CreateAttribute("aideck", aiDeckId));
      player.AddAttribute(ProtocolHelper.CreateAttribute("request_guid", byteArray));
      GameProperties gameProperties = new GameProperties();
      AttributeFilter val2 = new AttributeFilter();
      val2.SetOp(AttributeFilter.Types.Operation.MATCH_ALL);
      val2.AddAttribute(ProtocolHelper.CreateAttribute("GameType", (long) bnet));
      if (flag)
        val2.AddAttribute(ProtocolHelper.CreateAttribute("ScenarioId", (long) scenarioId));
      gameProperties.SetFilter(val2);
      gameProperties.AddCreationAttributes(ProtocolHelper.CreateAttribute("type", (long) bnet));
      gameProperties.AddCreationAttributes(ProtocolHelper.CreateAttribute("scenario", (long) scenarioId));
      BattleNet.FindGame(gameProperties, player);
    }
  }

  public void EnterFriendlyChallengeGame(FormatType formatType, int scenarioId, long player1DeckId, long player2DeckId, BnetGameAccountId player2GameAccountId)
  {
    Identity val1 = new Identity();
    val1.SetGameAccountId(BnetEntityId.CreateForProtocol((BnetEntityId) BnetPresenceMgr.Get().GetMyGameAccountId()));
    GameProperties gameProperties = new GameProperties();
    AttributeFilter val2 = new AttributeFilter();
    val2.SetOp(AttributeFilter.Types.Operation.MATCH_ALL);
    val2.AddAttribute(ProtocolHelper.CreateAttribute("GameType", 1L));
    val2.AddAttribute(ProtocolHelper.CreateAttribute("ScenarioId", (long) scenarioId));
    gameProperties.SetFilter(val2);
    gameProperties.AddCreationAttributes(ProtocolHelper.CreateAttribute("type", 1L));
    gameProperties.AddCreationAttributes(ProtocolHelper.CreateAttribute("scenario", (long) scenarioId));
    gameProperties.AddCreationAttributes(ProtocolHelper.CreateAttribute("format", (long) formatType));
    bnet.protocol.game_master.Player player1 = new bnet.protocol.game_master.Player();
    player1.SetIdentity(val1);
    player1.AddAttribute(ProtocolHelper.CreateAttribute("type", 1L));
    player1.AddAttribute(ProtocolHelper.CreateAttribute("scenario", (long) scenarioId));
    player1.AddAttribute(ProtocolHelper.CreateAttribute("deck", player1DeckId));
    player1.AddAttribute(ProtocolHelper.CreateAttribute("player_type", 1L));
    Identity val3 = new Identity();
    bnet.protocol.game_master.Player player2 = new bnet.protocol.game_master.Player();
    val3.SetGameAccountId(BnetEntityId.CreateForProtocol((BnetEntityId) player2GameAccountId));
    player2.SetIdentity(val3);
    player2.AddAttribute(ProtocolHelper.CreateAttribute("type", 1L));
    player2.AddAttribute(ProtocolHelper.CreateAttribute("scenario", (long) scenarioId));
    player2.AddAttribute(ProtocolHelper.CreateAttribute("deck", player2DeckId));
    player2.AddAttribute(ProtocolHelper.CreateAttribute("player_type", 2L));
    BattleNet.FindGame(gameProperties, player1, player2);
  }

  private static bool RequiresScenarioIdAttribute(BnetGameType bnetGameType)
  {
    switch (bnetGameType)
    {
      case BnetGameType.BGT_TAVERNBRAWL_PVP:
      case BnetGameType.BGT_TAVERNBRAWL_1P_VERSUS_AI:
      case BnetGameType.BGT_TAVERNBRAWL_2P_COOP:
      case BnetGameType.BGT_FRIENDS:
        return true;
      default:
        return false;
    }
  }

  public void CancelFindGame()
  {
    if (this.m_findingBnetGameType == BnetGameType.BGT_UNKNOWN)
      return;
    if (!this.IsNoAccountTutorialGame(this.GetFindingBnetGameType()))
      BattleNet.CancelFindGame();
    this.m_findingBnetGameType = BnetGameType.BGT_UNKNOWN;
  }

  private bool IsNoAccountTutorialGame(BnetGameType gameType)
  {
    return !Network.ShouldBeConnectedToAurora() && gameType == BnetGameType.BGT_TUTORIAL;
  }

  private static void SendGameServerHandshake(bgs.types.GameServerInfo gameInfo)
  {
    NetCache.NetCacheClientOptions netObject = NetCache.Get().GetNetObject<NetCache.NetCacheClientOptions>();
    if (netObject != null)
      netObject.DispatchClientOptionsToServer();
    if (gameInfo.SpectatorMode)
    {
      BnetGameAccountId myGameAccountId = BnetPresenceMgr.Get().GetMyGameAccountId();
      Network.s_connectApi.SendSpectatorGameHandshake(BattleNet.GetVersion(), Network.GetPlatformBuilder(), gameInfo, new PegasusShared.BnetId()
      {
        Hi = myGameAccountId.GetHi(),
        Lo = myGameAccountId.GetLo()
      });
    }
    else
      Network.s_connectApi.SendGameHandshake(gameInfo, Network.GetPlatformBuilder());
  }

  private void ResolveAddressAndGotoGameServer(bgs.types.GameServerInfo gameServer)
  {
    IPAddress address;
    if (IPAddress.TryParse(gameServer.Address, out address))
    {
      gameServer.Address = address.ToString();
      Network.Get().GotoGameServer(gameServer, false);
    }
    else
    {
      try
      {
        IPAddress[] addressList = Dns.GetHostEntry(gameServer.Address).AddressList;
        int index = 0;
        if (index < addressList.Length)
        {
          IPAddress ipAddress = addressList[index];
          gameServer.Address = ipAddress.ToString();
          Network.Get().GotoGameServer(gameServer, false);
          return;
        }
      }
      catch (Exception ex)
      {
        this.m_logSource.LogError("Exception within ResolveAddressAndGotoGameServer: " + ex.Message);
      }
      this.ThrowDnsResolveError(gameServer.Address);
    }
  }

  private void ThrowDnsResolveError(string environment)
  {
    if (ApplicationMgr.IsInternal())
      Error.AddDevFatal("Environment " + environment + " could not be resolved! Please check your environment and Internet connection!");
    else
      Error.AddFatalLoc("GLOBAL_ERROR_NETWORK_NO_CONNECTION");
  }

  public static Network.GameCancelInfo GetGameCancelInfo()
  {
    GameCanceled gameCancelInfo = Network.s_connectApi.GetGameCancelInfo();
    if (gameCancelInfo == null)
      return (Network.GameCancelInfo) null;
    return new Network.GameCancelInfo() { CancelReason = (Network.GameCancelInfo.Reason) gameCancelInfo.Reason_ };
  }

  public void GetGameState()
  {
    Network.s_connectApi.GetGameState();
  }

  public static Network.TurnTimerInfo GetTurnTimerInfo()
  {
    PegasusGame.TurnTimer turnTimerInfo = Network.s_connectApi.GetTurnTimerInfo();
    if (turnTimerInfo == null)
      return (Network.TurnTimerInfo) null;
    return new Network.TurnTimerInfo() { Seconds = (float) turnTimerInfo.Seconds, Turn = turnTimerInfo.Turn, Show = turnTimerInfo.Show };
  }

  public static int GetNAckOption()
  {
    NAckOption nackOption = Network.s_connectApi.GetNAckOption();
    if (nackOption == null)
      return 0;
    return nackOption.Id;
  }

  public static SpectatorNotify GetSpectatorNotify()
  {
    return Network.s_connectApi.GetSpectatorNotify();
  }

  public static void DisconnectFromGameServer()
  {
    if (!Network.IsConnectedToGameServer())
      return;
    Network.s_disconnectRequested = true;
    Network.s_connectApi.DisconnectFromGameServer();
  }

  public static bool WasDisconnectRequested()
  {
    return Network.s_disconnectRequested;
  }

  public static bool IsConnectedToGameServer()
  {
    return Network.s_connectApi.IsConnectedToGameServer();
  }

  public static bool GameServerHasEvents()
  {
    return Network.s_connectApi.GameServerHasEvents();
  }

  public static bool WasGameConceded()
  {
    return Network.s_gameConceded;
  }

  public static void Concede()
  {
    Network.s_gameConceded = true;
    Network.s_connectApi.Concede();
  }

  public static void AutoConcede()
  {
    if (!Network.IsConnectedToGameServer() || Network.WasGameConceded())
      return;
    Network.Concede();
  }

  public static Network.EntityChoices GetEntityChoices()
  {
    PegasusGame.EntityChoices entityChoices = Network.s_connectApi.GetEntityChoices();
    if (entityChoices == null)
      return (Network.EntityChoices) null;
    return new Network.EntityChoices() { ID = entityChoices.Id, ChoiceType = (CHOICE_TYPE) entityChoices.ChoiceType, CountMax = entityChoices.CountMax, CountMin = entityChoices.CountMin, Entities = Network.CopyIntList((IList<int>) entityChoices.Entities), Source = entityChoices.Source, PlayerId = entityChoices.PlayerId, HideChosen = entityChoices.HideChosen };
  }

  public static Network.EntitiesChosen GetEntitiesChosen()
  {
    PegasusGame.EntitiesChosen entitiesChosen = Network.s_connectApi.GetEntitiesChosen();
    if (entitiesChosen == null)
      return (Network.EntitiesChosen) null;
    return new Network.EntitiesChosen() { ID = entitiesChosen.ChooseEntities.Id, Entities = Network.CopyIntList((IList<int>) entitiesChosen.ChooseEntities.Entities), PlayerId = entitiesChosen.PlayerId, ChoiceType = (CHOICE_TYPE) entitiesChosen.ChoiceType };
  }

  public void SendChoices(int id, List<int> picks)
  {
    Network.s_connectApi.SendChoices(id, picks);
  }

  public void SendOption(int id, int index, int target, int sub, int pos)
  {
    Network.s_connectApi.SendOption(id, index, target, sub, pos);
  }

  public static Network.Options GetOptions()
  {
    AllOptions allOptions = Network.s_connectApi.GetAllOptions();
    Network.Options options = new Network.Options() { ID = allOptions.Id };
    using (List<PegasusGame.Option>.Enumerator enumerator1 = allOptions.Options.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        PegasusGame.Option current1 = enumerator1.Current;
        Network.Options.Option option = new Network.Options.Option();
        option.Type = (Network.Options.Option.OptionType) current1.Type_;
        if (current1.HasMainOption)
        {
          option.Main.ID = current1.MainOption.Id;
          option.Main.Targets = Network.CopyIntList((IList<int>) current1.MainOption.Targets);
        }
        using (List<PegasusGame.SubOption>.Enumerator enumerator2 = current1.SubOptions.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            PegasusGame.SubOption current2 = enumerator2.Current;
            option.Subs.Add(new Network.Options.Option.SubOption()
            {
              ID = current2.Id,
              Targets = Network.CopyIntList((IList<int>) current2.Targets)
            });
          }
        }
        options.List.Add(option);
      }
    }
    return options;
  }

  private static List<int> CopyIntList(IList<int> intList)
  {
    int[] array = new int[intList.Count];
    intList.CopyTo(array, 0);
    return new List<int>((IEnumerable<int>) array);
  }

  public void SendUserUI(int overCard, int heldCard, int arrowOrigin, int x, int y)
  {
    if (NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>().Games.ShowUserUI == 0)
      return;
    Network.s_connectApi.SendUserUi(overCard, heldCard, arrowOrigin, x, y);
  }

  public void SendEmote(EmoteType emote)
  {
    Network.s_connectApi.SendEmote((int) emote);
  }

  public void SendSpectatorInvite(BnetAccountId bnetAccountId, BnetGameAccountId bnetGameAccountId)
  {
    PegasusShared.BnetId targetBnetId = new PegasusShared.BnetId() { Hi = bnetAccountId.GetHi(), Lo = bnetAccountId.GetLo() };
    PegasusShared.BnetId targetGameAccountId = new PegasusShared.BnetId() { Hi = bnetGameAccountId.GetHi(), Lo = bnetGameAccountId.GetLo() };
    Network.s_connectApi.SendSpectatorInvite(targetBnetId, targetGameAccountId);
  }

  public void SendRemoveSpectators(bool regenerateSpectatorPassword, params BnetGameAccountId[] bnetGameAccountIds)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Network.\u003CSendRemoveSpectators\u003Ec__AnonStorey3CD spectatorsCAnonStorey3Cd = new Network.\u003CSendRemoveSpectators\u003Ec__AnonStorey3CD();
    // ISSUE: reference to a compiler-generated field
    spectatorsCAnonStorey3Cd.bnetIds = new List<PegasusShared.BnetId>();
    // ISSUE: reference to a compiler-generated method
    ((IEnumerable<BnetGameAccountId>) bnetGameAccountIds).ForEach<BnetGameAccountId>(new System.Action<BnetGameAccountId>(spectatorsCAnonStorey3Cd.\u003C\u003Em__173));
    // ISSUE: reference to a compiler-generated field
    Network.s_connectApi.SendRemoveSpectators(regenerateSpectatorPassword, spectatorsCAnonStorey3Cd.bnetIds);
  }

  public void SendRemoveAllSpectators(bool regenerateSpectatorPassword)
  {
    Network.s_connectApi.SendRemoveAllSpectators(regenerateSpectatorPassword);
  }

  public static Network.UserUI GetUserUI()
  {
    PegasusGame.UserUI userUi1 = Network.s_connectApi.GetUserUi();
    if (userUi1 == null)
      return (Network.UserUI) null;
    Network.UserUI userUi2 = new Network.UserUI();
    if (userUi1.HasPlayerId)
      userUi2.playerId = new int?(userUi1.PlayerId);
    if (userUi1.HasMouseInfo)
    {
      PegasusGame.MouseInfo mouseInfo = userUi1.MouseInfo;
      userUi2.mouseInfo = new Network.UserUI.MouseInfo();
      userUi2.mouseInfo.ArrowOriginID = mouseInfo.ArrowOrigin;
      userUi2.mouseInfo.HeldCardID = mouseInfo.HeldCard;
      userUi2.mouseInfo.OverCardID = mouseInfo.OverCard;
      userUi2.mouseInfo.X = mouseInfo.X;
      userUi2.mouseInfo.Y = mouseInfo.Y;
    }
    else if (userUi1.HasEmote)
    {
      userUi2.emoteInfo = new Network.UserUI.EmoteInfo();
      userUi2.emoteInfo.Emote = userUi1.Emote;
    }
    return userUi2;
  }

  public static Network.GameSetup GetGameSetupInfo()
  {
    PegasusGame.GameSetup gameSetup1 = Network.s_connectApi.GetGameSetup();
    if (gameSetup1 == null)
      return (Network.GameSetup) null;
    Network.GameSetup gameSetup2 = new Network.GameSetup();
    gameSetup2.Board = gameSetup1.Board;
    gameSetup2.MaxSecretsPerPlayer = gameSetup1.MaxSecretsPerPlayer;
    gameSetup2.MaxFriendlyMinionsPerPlayer = gameSetup1.MaxFriendlyMinionsPerPlayer;
    Network.s_gameServerKeepAliveFrequencySeconds = !gameSetup1.HasKeepAliveFrequencySeconds ? 0 : gameSetup1.KeepAliveFrequencySeconds;
    if (gameSetup1.HasDisconnectWhenStuckSeconds)
      gameSetup2.DisconnectWhenStuckSeconds = gameSetup1.DisconnectWhenStuckSeconds;
    return gameSetup2;
  }

  public static NetCache.NetCacheDisconnectedGame GetDisconnectedGameInfo()
  {
    NetCache.NetCacheDisconnectedGame disconnectedGame = new NetCache.NetCacheDisconnectedGame();
    if (!Network.ShouldBeConnectedToAurora())
      return disconnectedGame;
    Disconnected disconnectedGameInfo = Network.s_connectApi.GetDisconnectedGameInfo();
    if (disconnectedGameInfo != null && disconnectedGameInfo.HasAddress)
    {
      disconnectedGame.ServerInfo = new bgs.types.GameServerInfo();
      disconnectedGame.ServerInfo.Address = disconnectedGameInfo.Address;
      disconnectedGame.ServerInfo.GameHandle = disconnectedGameInfo.GameHandle;
      disconnectedGame.ServerInfo.ClientHandle = disconnectedGameInfo.ClientHandle;
      disconnectedGame.ServerInfo.Port = disconnectedGameInfo.Port;
      disconnectedGame.ServerInfo.AuroraPassword = disconnectedGameInfo.AuroraPassword;
      disconnectedGame.ServerInfo.Mission = disconnectedGameInfo.Scenario;
      disconnectedGame.ServerInfo.Version = BattleNet.GetVersion();
      disconnectedGame.ServerInfo.Resumable = true;
    }
    return disconnectedGame;
  }

  public static List<Network.PowerHistory> GetPowerHistory()
  {
    PegasusGame.PowerHistory powerHistory1 = Network.s_connectApi.GetPowerHistory();
    if (powerHistory1 == null)
      return (List<Network.PowerHistory>) null;
    List<Network.PowerHistory> powerHistoryList = new List<Network.PowerHistory>();
    using (List<PowerHistoryData>.Enumerator enumerator = powerHistory1.List.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PowerHistoryData current = enumerator.Current;
        Network.PowerHistory powerHistory2 = (Network.PowerHistory) null;
        if (current.HasFullEntity)
          powerHistory2 = (Network.PowerHistory) Network.GetFullEntity(current.FullEntity);
        else if (current.HasShowEntity)
          powerHistory2 = (Network.PowerHistory) Network.GetShowEntity(current.ShowEntity);
        else if (current.HasHideEntity)
          powerHistory2 = (Network.PowerHistory) Network.GetHideEntity(current.HideEntity);
        else if (current.HasChangeEntity)
          powerHistory2 = (Network.PowerHistory) Network.GetChangeEntity(current.ChangeEntity);
        else if (current.HasTagChange)
          powerHistory2 = (Network.PowerHistory) Network.GetTagChange(current.TagChange);
        else if (current.HasPowerStart)
          powerHistory2 = (Network.PowerHistory) Network.GetBlockStart(current.PowerStart);
        else if (current.HasPowerEnd)
          powerHistory2 = (Network.PowerHistory) Network.GetBlockEnd(current.PowerEnd);
        else if (current.HasCreateGame)
          powerHistory2 = (Network.PowerHistory) Network.GetCreateGame(current.CreateGame);
        else if (current.HasMetaData)
          powerHistory2 = (Network.PowerHistory) Network.GetMetaData(current.MetaData);
        else
          Debug.LogError((object) "Network.GetPowerHistory() - received invalid PowerHistoryData packet");
        if (powerHistory2 != null)
          powerHistoryList.Add(powerHistory2);
      }
    }
    return powerHistoryList;
  }

  private static Network.HistFullEntity GetFullEntity(PowerHistoryEntity entity)
  {
    return new Network.HistFullEntity() { Entity = Network.Entity.CreateFromProto(entity) };
  }

  private static Network.HistShowEntity GetShowEntity(PowerHistoryEntity entity)
  {
    return new Network.HistShowEntity() { Entity = Network.Entity.CreateFromProto(entity) };
  }

  private static Network.HistHideEntity GetHideEntity(PowerHistoryHide hide)
  {
    return new Network.HistHideEntity() { Entity = hide.Entity, Zone = hide.Zone };
  }

  private static Network.HistChangeEntity GetChangeEntity(PowerHistoryEntity entity)
  {
    return new Network.HistChangeEntity() { Entity = Network.Entity.CreateFromProto(entity) };
  }

  private static Network.HistTagChange GetTagChange(PowerHistoryTagChange tagChange)
  {
    return new Network.HistTagChange() { Entity = tagChange.Entity, Tag = tagChange.Tag, Value = tagChange.Value };
  }

  private static Network.HistBlockStart GetBlockStart(PowerHistoryStart start)
  {
    return new Network.HistBlockStart(start.Type) { Entity = start.Source, Target = start.Target, EffectCardId = start.EffectCardId, EffectIndex = start.Index };
  }

  private static Network.HistBlockEnd GetBlockEnd(PowerHistoryEnd end)
  {
    return new Network.HistBlockEnd();
  }

  private static Network.HistCreateGame GetCreateGame(PowerHistoryCreateGame createGame)
  {
    return Network.HistCreateGame.CreateFromProto(createGame);
  }

  private static Network.HistMetaData GetMetaData(PowerHistoryMetaData metaData)
  {
    Network.HistMetaData histMetaData = new Network.HistMetaData();
    histMetaData.MetaType = !metaData.HasType ? HistoryMeta.Type.TARGET : metaData.Type;
    histMetaData.Data = !metaData.HasData ? 0 : metaData.Data;
    using (List<int>.Enumerator enumerator = metaData.Info.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        histMetaData.Info.Add(current);
      }
    }
    return histMetaData;
  }

  private static List<int> MakeChoicesList(int choice1, int choice2, int choice3)
  {
    List<int> intList = new List<int>();
    if (choice1 == 0)
      return (List<int>) null;
    intList.Add(choice1);
    if (choice2 == 0)
      return intList;
    intList.Add(choice2);
    if (choice3 == 0)
      return intList;
    intList.Add(choice3);
    return intList;
  }

  public static void RequestAchieves()
  {
    Log.Achievements.Print("Requesting Achieves");
    string deviceModel = (string) null;
    Network.s_connectApi.RequestAchieves(deviceModel);
  }

  public static Achieves Achieves()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new Achieves();
    return Network.s_connectApi.GetAchieves();
  }

  public static void ValidateAchieve(int achieveID)
  {
    Log.Achievements.Print("Validiting achieve: " + (object) achieveID);
    Network.s_connectApi.ValidateAchieve(achieveID);
  }

  public static ValidateAchieveResponse GetValidatedAchieve()
  {
    return Network.s_connectApi.GetValidateAchieveResponse();
  }

  public static void RequestCancelQuest(int achieveID)
  {
    Network.s_connectApi.RequestCancelQuest(achieveID);
  }

  public static Network.CanceledQuest GetCanceledQuest()
  {
    CancelQuestResponse canceledQuestResponse = Network.s_connectApi.GetCanceledQuestResponse();
    if (canceledQuestResponse == null)
      return (Network.CanceledQuest) null;
    return new Network.CanceledQuest() { AchieveID = canceledQuestResponse.QuestId, Canceled = canceledQuestResponse.Success, NextQuestCancelDate = !canceledQuestResponse.HasNextQuestCancel ? 0L : TimeUtils.PegDateToFileTimeUtc(canceledQuestResponse.NextQuestCancel) };
  }

  public static Network.TriggeredEvent GetTriggerEventResponse()
  {
    TriggerEventResponse triggerEventResponse = Network.s_connectApi.GetTriggerEventResponse();
    if (triggerEventResponse == null)
      return (Network.TriggeredEvent) null;
    return new Network.TriggeredEvent() { EventID = triggerEventResponse.EventId, Success = triggerEventResponse.Success };
  }

  public static void RequestAdventureProgress()
  {
    Network.s_connectApi.RequestAdventureProgress();
  }

  public static List<Network.AdventureProgress> GetAdventureProgressResponse()
  {
    AdventureProgressResponse progressResponse = Network.s_connectApi.GetAdventureProgressResponse();
    if (progressResponse == null)
      return (List<Network.AdventureProgress>) null;
    List<Network.AdventureProgress> adventureProgressList = new List<Network.AdventureProgress>();
    using (List<PegasusShared.AdventureProgress>.Enumerator enumerator = progressResponse.List.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PegasusShared.AdventureProgress current = enumerator.Current;
        adventureProgressList.Add(new Network.AdventureProgress()
        {
          Wing = current.WingId,
          Progress = current.Progress,
          Ack = current.Ack,
          Flags = current.Flags_
        });
      }
    }
    return adventureProgressList;
  }

  public static Network.BeginDraft GetNewDraftDeckID()
  {
    DraftBeginning beginning = Network.s_connectApi.DraftGetBeginning();
    if (beginning == null)
      return (Network.BeginDraft) null;
    Network.BeginDraft beginDraft = new Network.BeginDraft();
    beginDraft.DeckID = beginning.DeckId;
    using (List<PegasusShared.CardDef>.Enumerator enumerator = beginning.ChoiceList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PegasusShared.CardDef current = enumerator.Current;
        NetCache.CardDefinition cardDefinition = new NetCache.CardDefinition() { Name = GameUtils.TranslateDbIdToCardId(current.Asset), Premium = (TAG_PREMIUM) current.Premium };
        beginDraft.Heroes.Add(cardDefinition);
      }
    }
    return beginDraft;
  }

  public static Network.DraftError GetDraftError()
  {
    PegasusUtil.DraftError error = Network.s_connectApi.DraftGetError();
    if (error != null)
      return (Network.DraftError) error.ErrorCode_;
    return Network.DraftError.DE_UNKNOWN;
  }

  public static Network.DraftChoicesAndContents GetDraftChoicesAndContents()
  {
    PegasusUtil.DraftChoicesAndContents choicesAndContents1 = Network.s_connectApi.DraftGetChoicesAndContents();
    if (choicesAndContents1 == null)
      return (Network.DraftChoicesAndContents) null;
    Network.DraftChoicesAndContents choicesAndContents2 = new Network.DraftChoicesAndContents();
    choicesAndContents2.DeckInfo.Deck = choicesAndContents1.DeckId;
    choicesAndContents2.Slot = choicesAndContents1.Slot;
    choicesAndContents2.Hero.Name = choicesAndContents1.HeroDef.Asset != 0 ? GameUtils.TranslateDbIdToCardId(choicesAndContents1.HeroDef.Asset) : string.Empty;
    choicesAndContents2.Hero.Premium = (TAG_PREMIUM) choicesAndContents1.HeroDef.Premium;
    choicesAndContents2.Wins = choicesAndContents1.Wins;
    choicesAndContents2.Losses = choicesAndContents1.Losses;
    choicesAndContents2.MaxWins = !choicesAndContents1.HasMaxWins ? int.MaxValue : choicesAndContents1.MaxWins;
    using (List<PegasusShared.CardDef>.Enumerator enumerator = choicesAndContents1.ChoiceList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PegasusShared.CardDef current = enumerator.Current;
        if (current.Asset != 0)
        {
          NetCache.CardDefinition cardDefinition = new NetCache.CardDefinition() { Name = GameUtils.TranslateDbIdToCardId(current.Asset), Premium = (TAG_PREMIUM) current.Premium };
          choicesAndContents2.Choices.Add(cardDefinition);
        }
      }
    }
    using (List<DeckCardData>.Enumerator enumerator = choicesAndContents1.Cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckCardData current = enumerator.Current;
        choicesAndContents2.DeckInfo.Cards.Add(new Network.CardUserData()
        {
          DbId = current.Def.Asset,
          Count = !current.HasQty ? 1 : current.Qty,
          Premium = !current.Def.HasPremium ? TAG_PREMIUM.NORMAL : (TAG_PREMIUM) current.Def.Premium
        });
      }
    }
    choicesAndContents2.Chest = !choicesAndContents1.HasChest ? (Network.RewardChest) null : Network.ConvertRewardChest(choicesAndContents1.Chest);
    return choicesAndContents2;
  }

  public static Network.DraftChosen GetChosenAndNext()
  {
    PegasusUtil.DraftChosen draftChosen = Network.s_connectApi.DraftCardChosen();
    if (draftChosen == null)
      return (Network.DraftChosen) null;
    NetCache.CardDefinition cardDefinition1 = new NetCache.CardDefinition() { Name = GameUtils.TranslateDbIdToCardId(draftChosen.Chosen.Asset), Premium = (TAG_PREMIUM) draftChosen.Chosen.Premium };
    List<NetCache.CardDefinition> cardDefinitionList = new List<NetCache.CardDefinition>();
    using (List<PegasusShared.CardDef>.Enumerator enumerator = draftChosen.NextChoiceList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PegasusShared.CardDef current = enumerator.Current;
        NetCache.CardDefinition cardDefinition2 = new NetCache.CardDefinition() { Name = GameUtils.TranslateDbIdToCardId(current.Asset), Premium = (TAG_PREMIUM) current.Premium };
        cardDefinitionList.Add(cardDefinition2);
      }
    }
    return new Network.DraftChosen() { ChosenCard = cardDefinition1, NextChoices = cardDefinitionList };
  }

  public static void MakeDraftChoice(long deckID, int slot, int index)
  {
    Network.s_connectApi.DraftMakePick(deckID, slot, index);
  }

  public static void FindOutCurrentDraftState()
  {
    Network.s_connectApi.DraftGetPicksAndContents();
  }

  public static void SendArenaSessionRequest()
  {
    Network.s_connectApi.SendArenaSessionRequest();
  }

  public static ArenaSessionResponse GetArenaSessionResponse()
  {
    return Network.s_connectApi.GetArenaSessionResponse();
  }

  public static void StartANewDraft()
  {
    Network.s_connectApi.DraftBegin();
  }

  public static void RetireDraftDeck(long deckID, int slot)
  {
    Network.s_connectApi.DraftRetire(deckID, slot);
  }

  public static Network.DraftRetired GetRetiredDraft()
  {
    PegasusUtil.DraftRetired draftRetired = Network.s_connectApi.GetDraftRetired();
    if (draftRetired == null)
      return (Network.DraftRetired) null;
    return new Network.DraftRetired() { Deck = draftRetired.DeckId, Chest = Network.ConvertRewardChest(draftRetired.Chest) };
  }

  public static void AckDraftRewards(long deckID, int slot)
  {
    Network.s_connectApi.DraftAckRewards(deckID, slot);
  }

  public static long GetRewardsAckDraftID()
  {
    DraftRewardsAcked draftRewardsAcked = Network.s_connectApi.DraftRewardsAcked();
    if (draftRewardsAcked == null)
      return 0;
    return draftRewardsAcked.DeckId;
  }

  public static Network.RewardChest ConvertRewardChest(PegasusShared.RewardChest chest)
  {
    Network.RewardChest rewardChest = new Network.RewardChest();
    for (int index = 0; index < chest.Bag.Count; ++index)
      rewardChest.Rewards.Add(Network.ConvertRewardBag(chest.Bag[index]));
    if (rewardChest.Rewards.Count > 0)
      return rewardChest;
    if (chest.HasBag1)
      rewardChest.Rewards.Add(Network.ConvertRewardBag(chest.Bag1));
    if (chest.HasBag2)
      rewardChest.Rewards.Add(Network.ConvertRewardBag(chest.Bag2));
    if (chest.HasBag3)
      rewardChest.Rewards.Add(Network.ConvertRewardBag(chest.Bag3));
    if (chest.HasBag4)
      rewardChest.Rewards.Add(Network.ConvertRewardBag(chest.Bag4));
    if (chest.HasBag5)
      rewardChest.Rewards.Add(Network.ConvertRewardBag(chest.Bag5));
    return rewardChest;
  }

  private static RewardData ConvertRewardBag(RewardBag bag)
  {
    if (bag.HasRewardBooster)
      return (RewardData) new BoosterPackRewardData(bag.RewardBooster.BoosterType, bag.RewardBooster.BoosterCount);
    if (bag.HasRewardCard)
      return (RewardData) new CardRewardData(GameUtils.TranslateDbIdToCardId(bag.RewardCard.Card.Asset), (TAG_PREMIUM) bag.RewardCard.Card.Premium, 1);
    if (bag.HasRewardDust)
      return (RewardData) new ArcaneDustRewardData(bag.RewardDust.Amount);
    if (bag.HasRewardGold)
      return (RewardData) new GoldRewardData((long) bag.RewardGold.Amount);
    if (bag.HasRewardCardBack)
      return (RewardData) new CardBackRewardData(bag.RewardCardBack.CardBack);
    Debug.LogError((object) "Unrecognized draft bag reward");
    return (RewardData) null;
  }

  public static void MassDisenchant()
  {
    Network.s_connectApi.MassDisenchant();
  }

  public static Network.MassDisenchantResponse GetMassDisenchantResponse()
  {
    PegasusUtil.MassDisenchantResponse disenchantResponse = Network.s_connectApi.GetMassDisenchantResponse();
    if (disenchantResponse == null)
      return (Network.MassDisenchantResponse) null;
    return new Network.MassDisenchantResponse() { Amount = disenchantResponse.Amount };
  }

  public static void SetFavoriteHero(TAG_CLASS heroClass, NetCache.CardDefinition hero)
  {
    Network.s_connectApi.SetFavoriteHero((int) heroClass, new PegasusShared.CardDef()
    {
      Asset = GameUtils.TranslateCardIdToDbId(hero.Name),
      Premium = (int) hero.Premium
    });
  }

  public static Network.SetFavoriteHeroResponse GetSetFavoriteHeroResponse()
  {
    PegasusUtil.SetFavoriteHeroResponse favoriteHeroResponse1 = Network.s_connectApi.GetSetFavoriteHeroResponse();
    if (favoriteHeroResponse1 == null)
      return (Network.SetFavoriteHeroResponse) null;
    Network.SetFavoriteHeroResponse favoriteHeroResponse2 = new Network.SetFavoriteHeroResponse();
    favoriteHeroResponse2.Success = favoriteHeroResponse1.Success;
    if (favoriteHeroResponse1.HasFavoriteHero)
    {
      if (!EnumUtils.TryCast<TAG_CLASS>((object) favoriteHeroResponse1.FavoriteHero.ClassId, out favoriteHeroResponse2.HeroClass))
        Debug.LogWarning((object) string.Format("Network.GetSetFavoriteHeroResponse() invalid class {0}", (object) favoriteHeroResponse1.FavoriteHero.ClassId));
      TAG_PREMIUM outVal;
      if (!EnumUtils.TryCast<TAG_PREMIUM>((object) favoriteHeroResponse1.FavoriteHero.Hero.Premium, out outVal))
        Debug.LogWarning((object) string.Format("Network.GetSetFavoriteHeroResponse() invalid heroPremium {0}", (object) favoriteHeroResponse1.FavoriteHero.Hero.Premium));
      favoriteHeroResponse2.Hero = new NetCache.CardDefinition()
      {
        Name = GameUtils.TranslateDbIdToCardId(favoriteHeroResponse1.FavoriteHero.Hero.Asset),
        Premium = outVal
      };
    }
    return favoriteHeroResponse2;
  }

  public static void RequestRecruitAFriendUrl()
  {
    Network.s_connectApi.RequestRecruitAFriendUrl(Network.GetPlatformBuilder());
  }

  public static RecruitAFriendURLResponse GetRecruitAFriendUrlResponse()
  {
    return Network.s_connectApi.GetRecruitAFriendUrlResponse();
  }

  public static void RequestRecruitAFriendData()
  {
    Network.s_connectApi.RequestRecruitAFriendData();
  }

  public static void RequestBattlePayStatus()
  {
    Network.s_connectApi.RequestBattlePayStatus();
  }

  public static RecruitAFriendDataResponse GetRecruitAFriendDataResponse()
  {
    return Network.s_connectApi.GetRecruitAFriendDataResponse();
  }

  public static void RequestProcessRecruitAFriend()
  {
    Network.s_connectApi.RequestProcessRecruitAFriend();
  }

  public static ProcessRecruitAFriendResponse GetProcessRecruitAFriendResponse()
  {
    return Network.s_connectApi.GetProcessRecruitAFriendResponse();
  }

  public static Network.PurchaseCanceledResponse GetPurchaseCanceledResponse()
  {
    CancelPurchaseResponse purchaseResponse = Network.s_connectApi.GetCancelPurchaseResponse();
    if (purchaseResponse == null)
      return (Network.PurchaseCanceledResponse) null;
    Network.PurchaseCanceledResponse canceledResponse = new Network.PurchaseCanceledResponse() { TransactionID = !purchaseResponse.HasTransactionId ? 0L : purchaseResponse.TransactionId, ProductID = !purchaseResponse.HasProductId ? string.Empty : purchaseResponse.ProductId, CurrencyType = !purchaseResponse.HasCurrency ? Currency.UNKNOWN : (Currency) purchaseResponse.Currency };
    switch (purchaseResponse.Result)
    {
      case CancelPurchaseResponse.CancelResult.CR_SUCCESS:
        canceledResponse.Result = Network.PurchaseCanceledResponse.CancelResult.SUCCESS;
        break;
      case CancelPurchaseResponse.CancelResult.CR_NOT_ALLOWED:
        canceledResponse.Result = Network.PurchaseCanceledResponse.CancelResult.NOT_ALLOWED;
        break;
      case CancelPurchaseResponse.CancelResult.CR_NOTHING_TO_CANCEL:
        canceledResponse.Result = Network.PurchaseCanceledResponse.CancelResult.NOTHING_TO_CANCEL;
        break;
    }
    return canceledResponse;
  }

  public static Network.BattlePayStatus GetBattlePayStatusResponse()
  {
    BattlePayStatusResponse payStatusResponse = Network.s_connectApi.GetBattlePayStatusResponse();
    if (payStatusResponse == null)
      return (Network.BattlePayStatus) null;
    Network.BattlePayStatus battlePayStatus = new Network.BattlePayStatus() { State = (Network.BattlePayStatus.PurchaseState) payStatusResponse.Status, BattlePayAvailable = payStatusResponse.BattlePayAvailable, CurrencyType = !payStatusResponse.HasCurrency ? Currency.UNKNOWN : (Currency) payStatusResponse.Currency };
    if (payStatusResponse.HasTransactionId)
      battlePayStatus.TransactionID = payStatusResponse.TransactionId;
    if (payStatusResponse.HasProductId)
      battlePayStatus.ProductID = payStatusResponse.ProductId;
    if (payStatusResponse.HasPurchaseError)
      battlePayStatus.PurchaseError = Network.ConvertPurchaseError(payStatusResponse.PurchaseError);
    if (payStatusResponse.HasThirdPartyId)
      battlePayStatus.ThirdPartyID = payStatusResponse.ThirdPartyId;
    if (payStatusResponse.HasProvider)
      battlePayStatus.Provider = new BattlePayProvider?(payStatusResponse.Provider);
    return battlePayStatus;
  }

  private static Network.PurchaseErrorInfo ConvertPurchaseError(PurchaseError purchaseError)
  {
    Network.PurchaseErrorInfo purchaseErrorInfo = new Network.PurchaseErrorInfo() { Error = (Network.PurchaseErrorInfo.ErrorType) purchaseError.Error_ };
    if (purchaseError.HasPurchaseInProgress)
      purchaseErrorInfo.PurchaseInProgressProductID = purchaseError.PurchaseInProgress;
    if (purchaseError.HasErrorCode)
      purchaseErrorInfo.ErrorCode = purchaseError.ErrorCode;
    return purchaseErrorInfo;
  }

  public static void RequestBattlePayConfig()
  {
    Network.s_connectApi.RequestBattlePayConfig();
  }

  public static Network.BattlePayConfig GetBattlePayConfigResponse()
  {
    BattlePayConfigResponse payConfigResponse = Network.s_connectApi.GetBattlePayConfigResponse();
    if (payConfigResponse == null)
      return (Network.BattlePayConfig) null;
    Network.BattlePayConfig battlePayConfig = new Network.BattlePayConfig() { Available = !payConfigResponse.HasUnavailable || !payConfigResponse.Unavailable, Currency = !payConfigResponse.HasCurrency ? Currency.UNKNOWN : (Currency) payConfigResponse.Currency, SecondsBeforeAutoCancel = !payConfigResponse.HasSecsBeforeAutoCancel ? StoreManager.DEFAULT_SECONDS_BEFORE_AUTO_CANCEL : payConfigResponse.SecsBeforeAutoCancel };
    using (List<PegasusUtil.Bundle>.Enumerator enumerator1 = payConfigResponse.Bundles.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        PegasusUtil.Bundle current1 = enumerator1.Current;
        Network.Bundle bundle = new Network.Bundle() { ProductID = current1.Id, Cost = new double?(), GoldCost = new long?(), AppleID = !current1.HasAppleId ? string.Empty : current1.AppleId, GooglePlayID = !current1.HasGooglePlayId ? string.Empty : current1.GooglePlayId, AmazonID = !current1.HasAmazonId ? string.Empty : current1.AmazonId, ExclusiveProviders = current1.ExclusiveProviders };
        if (current1.HasCost && current1.Cost > 0.0)
          bundle.Cost = new double?(current1.Cost);
        if (current1.HasGoldCost && current1.GoldCost > 0L)
          bundle.GoldCost = new long?(current1.GoldCost);
        if (current1.HasProductEventName)
          bundle.ProductEvent = SpecialEventManager.GetEventType(current1.ProductEventName, SpecialEventType.UNKNOWN);
        if (current1.HasRealMoneyProductEventName)
          bundle.RealMoneyProductEvent = SpecialEventManager.GetEventType(current1.RealMoneyProductEventName, SpecialEventType.UNKNOWN);
        using (List<PegasusUtil.BundleItem>.Enumerator enumerator2 = current1.Items.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            PegasusUtil.BundleItem current2 = enumerator2.Current;
            Network.BundleItem bundleItem = new Network.BundleItem() { Product = current2.ProductType, ProductData = current2.Data, Quantity = current2.Quantity };
            bundle.Items.Add(bundleItem);
          }
        }
        battlePayConfig.Bundles.Add(bundle);
      }
    }
    using (List<PegasusUtil.GoldCostBooster>.Enumerator enumerator = payConfigResponse.GoldCostBoosters.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PegasusUtil.GoldCostBooster current = enumerator.Current;
        Network.GoldCostBooster goldCostBooster = new Network.GoldCostBooster() { ID = current.PackType };
        goldCostBooster.Cost = current.Cost <= 0L ? new long?() : new long?(current.Cost);
        if (current.HasBuyWithGoldEventName)
          goldCostBooster.BuyWithGoldEvent = SpecialEventManager.GetEventType(current.BuyWithGoldEventName, SpecialEventType.UNKNOWN);
        battlePayConfig.GoldCostBoosters.Add(goldCostBooster);
      }
    }
    battlePayConfig.GoldCostArena = !payConfigResponse.HasGoldCostArena || payConfigResponse.GoldCostArena <= 0L ? new long?() : new long?(payConfigResponse.GoldCostArena);
    return battlePayConfig;
  }

  public static void PurchaseViaGold(int quantity, ProductType product, int data)
  {
    Network.s_connectApi.PurchaseViaGold(quantity, product, data);
  }

  public static void GetPurchaseMethod(string productID, int quantity, Currency currency)
  {
    Network.s_connectApi.RequestPurchaseMethod(productID, quantity, (int) currency, SystemInfo.deviceUniqueIdentifier, Network.GetPlatformBuilder());
  }

  public static void ConfirmPurchase()
  {
    Network.s_connectApi.ConfirmPurchase();
  }

  public static void BeginThirdPartyPurchase(BattlePayProvider provider, string productId, int quantity)
  {
    Network.s_connectApi.BeginThirdPartyPurchase(SystemInfo.deviceUniqueIdentifier, provider, productId, quantity);
  }

  public static void BeginThirdPartyPurchaseWithReceipt(BattlePayProvider provider, string productId, int quantity, string thirdPartyId, string base64receipt, string thirdPartyUserId = "")
  {
    BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_THIRD_PARTY_PURCHASE_RECEIPT_SUBMITTED_DANGLING, 0, provider.ToString() + "|" + (string.IsNullOrEmpty(productId) ? string.Empty : productId) + "|" + (string.IsNullOrEmpty(thirdPartyId) ? string.Empty : thirdPartyId) + "|" + (string.IsNullOrEmpty(thirdPartyUserId) ? string.Empty : thirdPartyUserId));
    Network.s_connectApi.BeginThirdPartyPurchaseWithReceipt(SystemInfo.deviceUniqueIdentifier, provider, productId, quantity, thirdPartyId, base64receipt, !string.IsNullOrEmpty(thirdPartyUserId) ? thirdPartyUserId : (string) null);
  }

  public static void SubmitThirdPartyReceipt(long bpayId, string thirdPartyId, string base64receipt, string thirdPartyUserId = "")
  {
    BIReport.Get().Report_Telemetry(Telemetry.Level.LEVEL_INFO, BIReport.TelemetryEvent.EVENT_THIRD_PARTY_PURCHASE_RECEIPT_SUBMITTED, 0, bpayId.ToString() + "|" + (string.IsNullOrEmpty(thirdPartyId) ? string.Empty : thirdPartyId) + "|" + (string.IsNullOrEmpty(thirdPartyUserId) ? string.Empty : thirdPartyUserId));
    Network.s_connectApi.SubmitThirdPartyPurchaseReceipt(bpayId, thirdPartyId, base64receipt, thirdPartyUserId);
  }

  public static void GetThirdPartyPurchaseStatus(string transactionId)
  {
    Network.s_connectApi.GetThirdPartyPurchaseStatus(transactionId);
  }

  public static void CancelBlizzardPurchase(bool isAutoCanceled, CancelPurchase.CancelReason? reason, string error)
  {
    Network.s_connectApi.AbortBlizzardPurchase(SystemInfo.deviceUniqueIdentifier, isAutoCanceled, reason, error);
  }

  public static void CancelThirdPartyPurchase(CancelPurchase.CancelReason reason, string error)
  {
    Network.s_connectApi.AbortThirdPartyPurchase(SystemInfo.deviceUniqueIdentifier, reason, error);
  }

  public static Network.PurchaseMethod GetPurchaseMethodResponse()
  {
    PegasusUtil.PurchaseMethod purchaseMethodResponse = Network.s_connectApi.GetPurchaseMethodResponse();
    if (purchaseMethodResponse == null)
      return (Network.PurchaseMethod) null;
    Network.PurchaseMethod purchaseMethod = new Network.PurchaseMethod();
    if (purchaseMethodResponse.HasTransactionId)
      purchaseMethod.TransactionID = purchaseMethodResponse.TransactionId;
    if (purchaseMethodResponse.HasProductId)
      purchaseMethod.ProductID = purchaseMethodResponse.ProductId;
    if (purchaseMethodResponse.HasQuantity)
      purchaseMethod.Quantity = purchaseMethodResponse.Quantity;
    if (purchaseMethodResponse.HasCurrency)
      purchaseMethod.Currency = (Currency) purchaseMethodResponse.Currency;
    if (purchaseMethodResponse.HasWalletName)
      purchaseMethod.WalletName = purchaseMethodResponse.WalletName;
    if (purchaseMethodResponse.HasUseEbalance)
      purchaseMethod.UseEBalance = purchaseMethodResponse.UseEbalance;
    purchaseMethod.IsZeroCostLicense = purchaseMethodResponse.HasIsZeroCostLicense && purchaseMethodResponse.IsZeroCostLicense;
    if (purchaseMethodResponse.HasChallengeId)
      purchaseMethod.ChallengeID = purchaseMethodResponse.ChallengeId;
    if (purchaseMethodResponse.HasChallengeUrl)
      purchaseMethod.ChallengeURL = purchaseMethodResponse.ChallengeUrl;
    if (purchaseMethodResponse.HasError)
      purchaseMethod.PurchaseError = Network.ConvertPurchaseError(purchaseMethodResponse.Error);
    return purchaseMethod;
  }

  public static Network.PurchaseResponse GetPurchaseResponse()
  {
    PegasusUtil.PurchaseResponse purchaseResponse = Network.s_connectApi.GetPurchaseResponse();
    if (purchaseResponse == null)
      return (Network.PurchaseResponse) null;
    return new Network.PurchaseResponse() { PurchaseError = Network.ConvertPurchaseError(purchaseResponse.Error), TransactionID = !purchaseResponse.HasTransactionId ? 0L : purchaseResponse.TransactionId, ProductID = !purchaseResponse.HasProductId ? string.Empty : purchaseResponse.ProductId, ThirdPartyID = !purchaseResponse.HasThirdPartyId ? string.Empty : purchaseResponse.ThirdPartyId, CurrencyType = !purchaseResponse.HasCurrency ? Currency.UNKNOWN : (Currency) purchaseResponse.Currency };
  }

  public static Network.PurchaseViaGoldResponse GetPurchaseWithGoldResponse()
  {
    PurchaseWithGoldResponse withGoldResponse = Network.s_connectApi.GetPurchaseWithGoldResponse();
    if (withGoldResponse == null)
      return (Network.PurchaseViaGoldResponse) null;
    Network.PurchaseViaGoldResponse purchaseViaGoldResponse = new Network.PurchaseViaGoldResponse() { Error = (Network.PurchaseViaGoldResponse.ErrorType) withGoldResponse.Result };
    if (withGoldResponse.HasGoldUsed)
      purchaseViaGoldResponse.GoldUsed = withGoldResponse.GoldUsed;
    return purchaseViaGoldResponse;
  }

  public static Network.ThirdPartyPurchaseStatusResponse GetThirdPartyPurchaseStatusResponse()
  {
    PegasusUtil.ThirdPartyPurchaseStatusResponse purchaseStatusResponse = Network.s_connectApi.GetThirdPartyPurchaseStatusResponse();
    if (purchaseStatusResponse == null)
      return (Network.ThirdPartyPurchaseStatusResponse) null;
    return new Network.ThirdPartyPurchaseStatusResponse() { ThirdPartyID = purchaseStatusResponse.ThirdPartyId, Status = (Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus) purchaseStatusResponse.Status_ };
  }

  public static Network.CardBackResponse GetCardBackResponse()
  {
    SetCardBackResponse cardBackResponse = Network.s_connectApi.GetCardBackResponse();
    if (cardBackResponse == null)
      return (Network.CardBackResponse) null;
    return new Network.CardBackResponse() { Success = cardBackResponse.Success, CardBack = cardBackResponse.CardBack };
  }

  public static void SetDefaultCardBack(int cardBack)
  {
    Network.s_connectApi.SetDefaultCardBack(cardBack);
  }

  public static NetCache.NetCacheCardBacks GetCardBacks()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheCardBacks();
    CardBacks cardBacks = Network.s_connectApi.GetCardBacks();
    if (cardBacks == null)
      return (NetCache.NetCacheCardBacks) null;
    NetCache.NetCacheCardBacks netCacheCardBacks = new NetCache.NetCacheCardBacks();
    netCacheCardBacks.DefaultCardBack = cardBacks.DefaultCardBack;
    using (List<int>.Enumerator enumerator = cardBacks.CardBacks_.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        netCacheCardBacks.CardBacks.Add(current);
      }
    }
    return netCacheCardBacks;
  }

  public static CardValues GetCardValues()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return (CardValues) null;
    return Network.s_connectApi.GetCardValues();
  }

  public static NetCache.NetCacheCollection GetCollectionCardStacks()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheCollection();
    Collection collectionCardStacks = Network.s_connectApi.GetCollectionCardStacks();
    if (collectionCardStacks == null)
      return (NetCache.NetCacheCollection) null;
    NetCache.NetCacheCollection netCacheCollection = new NetCache.NetCacheCollection();
    using (List<PegasusShared.CardStack>.Enumerator enumerator = collectionCardStacks.Stacks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PegasusShared.CardStack current = enumerator.Current;
        NetCache.CardStack cardStack = new NetCache.CardStack();
        cardStack.Def.Name = GameUtils.TranslateDbIdToCardId(current.CardDef.Asset);
        if (string.IsNullOrEmpty(cardStack.Def.Name))
        {
          Error.AddDevFatal("Network.GetCollectionCardStacks() - failed to find a card with databaseId: {0}", (object) current.CardDef.Asset);
        }
        else
        {
          cardStack.Def.Premium = (TAG_PREMIUM) current.CardDef.Premium;
          cardStack.Date = TimeUtils.PegDateToFileTimeUtc(current.LatestInsertDate);
          cardStack.Count = current.Count;
          cardStack.NumSeen = current.NumSeen;
          netCacheCollection.Stacks.Add(cardStack);
          netCacheCollection.TotalCardsOwned += cardStack.Count;
          if (cardStack.Def.Premium == TAG_PREMIUM.NORMAL && cardStack.Count > 0)
          {
            EntityDef entityDef = DefLoader.Get().GetEntityDef(cardStack.Def.Name);
            if (entityDef.IsBasicCardUnlock())
            {
              int val1 = !entityDef.IsElite() ? 2 : 1;
              Map<TAG_CLASS, int> unlockedPerClass;
              TAG_CLASS index;
              (unlockedPerClass = netCacheCollection.BasicCardsUnlockedPerClass)[index = entityDef.GetClass()] = unlockedPerClass[index] + Math.Min(val1, cardStack.Count);
            }
          }
        }
      }
    }
    return netCacheCollection;
  }

  public static void OpenBooster(int id)
  {
    Log.Bob.Print("Network.OpenBooster");
    Network.s_connectApi.OpenBooster(id);
  }

  public void CreateDeck(DeckType deckType, string name, int heroDatabaseAssetID, TAG_PREMIUM heroPremium, bool isWild, long sortOrder, DeckSourceType sourceType)
  {
    Log.Rachelle.Print(string.Format("Network.CreateDeck hero={0},premium={1}", (object) heroDatabaseAssetID, (object) heroPremium));
    Network.s_connectApi.CreateDeck(deckType, name, heroDatabaseAssetID, heroPremium, isWild, sortOrder, sourceType);
  }

  public void RenameDeck(long deck, string name)
  {
    Log.Rachelle.Print(string.Format("Network.RenameDeck {0}", (object) deck));
    Network.s_connectApi.RenameDeck(deck, name);
  }

  public static void SendDeckData(long deck, List<Network.CardUserData> cards, int newHeroAssetID, TAG_PREMIUM newHeroCardPremium, int newCardBackID, bool isWild, long sortOrder)
  {
    DeckSetData packet = new DeckSetData() { Deck = deck, TaggedStandard = !isWild, SortOrder = sortOrder };
    using (List<Network.CardUserData>.Enumerator enumerator = cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Network.CardUserData current = enumerator.Current;
        DeckCardData deckCardData = new DeckCardData();
        PegasusShared.CardDef cardDef = new PegasusShared.CardDef();
        cardDef.Asset = current.DbId;
        if (current.Premium != TAG_PREMIUM.NORMAL)
          cardDef.Premium = (int) current.Premium;
        deckCardData.Def = cardDef;
        deckCardData.Qty = current.Count;
        packet.Cards.Add(deckCardData);
      }
    }
    if (newHeroAssetID != -1)
      packet.Hero = new PegasusShared.CardDef()
      {
        Asset = newHeroAssetID,
        Premium = (int) newHeroCardPremium
      };
    if (newCardBackID != -1)
      packet.CardBack = newCardBackID;
    Network.s_connectApi.SendDeckData(packet);
  }

  public void DeleteDeck(long deck)
  {
    Log.Rachelle.Print(string.Format("Network.DeleteDeck {0}", (object) deck));
    Network.s_connectApi.DeleteDeck(deck);
  }

  public void RequestDeckContents(params long[] deckIds)
  {
    Log.Bob.Print("Network.GetDeckContents {0}", (object) string.Join(", ", ((IEnumerable<long>) deckIds).Select<long, string>((Func<long, string>) (id => id.ToString())).ToArray<string>()));
    Network.s_connectApi.RequestDeckContents(deckIds);
  }

  public void SetDeckTemplateSource(long deck, int templateID)
  {
    Log.Bob.Print(string.Format("Network.SendDeckTemplateSource {0}, {1}", (object) deck, (object) templateID));
    Network.s_connectApi.SendDeckTemplateSource(deck, templateID);
  }

  public static GetDeckContentsResponse GetDeckContentsResponse()
  {
    return Network.s_connectApi.GetDeckContentsResponse();
  }

  public static List<NetCache.BoosterCard> OpenedBooster()
  {
    BoosterContent openedBooster = Network.s_connectApi.GetOpenedBooster();
    if (openedBooster == null)
      return (List<NetCache.BoosterCard>) null;
    List<NetCache.BoosterCard> boosterCardList = new List<NetCache.BoosterCard>();
    using (List<PegasusUtil.BoosterCard>.Enumerator enumerator = openedBooster.List.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PegasusUtil.BoosterCard current = enumerator.Current;
        boosterCardList.Add(new NetCache.BoosterCard()
        {
          Def = {
            Name = GameUtils.TranslateDbIdToCardId(current.CardDef.Asset),
            Premium = (TAG_PREMIUM) current.CardDef.Premium
          },
          Date = TimeUtils.PegDateToFileTimeUtc(current.InsertDate)
        });
      }
    }
    return boosterCardList;
  }

  public static Network.DBAction GetDeckResponse()
  {
    return Network.GetDbAction();
  }

  public static Network.DBAction GetDbAction()
  {
    PegasusUtil.DBAction dbAction = Network.s_connectApi.GetDbAction();
    if (dbAction == null)
      return (Network.DBAction) null;
    return new Network.DBAction() { Action = (Network.DBAction.ActionType) dbAction.Action, Result = (Network.DBAction.ResultType) dbAction.Result, MetaData = dbAction.MetaData };
  }

  public static NetCache.NetCacheDecks GetDeckHeaders()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheDecks();
    DeckList deckHeaders = Network.s_connectApi.GetDeckHeaders();
    if (deckHeaders == null)
      return (NetCache.NetCacheDecks) null;
    NetCache.NetCacheDecks netCacheDecks = new NetCache.NetCacheDecks();
    using (List<DeckInfo>.Enumerator enumerator = deckHeaders.Decks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckInfo current = enumerator.Current;
        NetCache.DeckHeader deckHeader = new NetCache.DeckHeader() { ID = current.Id, Name = current.Name, Hero = GameUtils.TranslateDbIdToCardId(current.Hero), HeroPremium = (TAG_PREMIUM) current.HeroPremium, HeroPower = GameUtils.GetHeroPowerCardIdFromHero(current.Hero), Type = current.DeckType, CardBack = current.CardBack, CardBackOverridden = current.CardBackOverride, HeroOverridden = current.HeroOverride, SeasonId = current.SeasonId, NeedsName = Network.DeckNeedsName(current.Validity), SortOrder = !current.HasSortOrder ? 0L : current.SortOrder, IsWild = Network.AreDeckFlagsWild(current.Validity), Locked = Network.AreDeckFlagsLocked(current.Validity), SourceType = !current.HasSourceType ? DeckSourceType.DECK_SOURCE_TYPE_UNKNOWN : current.SourceType };
        deckHeader.CreateDate = !current.HasCreateDate ? new DateTime?() : new DateTime?(TimeUtils.UnixTimeStampToDateTime((ulong) current.CreateDate));
        deckHeader.LastModified = !current.HasLastModified ? new DateTime?() : new DateTime?(TimeUtils.UnixTimeStampToDateTime((ulong) current.LastModified));
        netCacheDecks.Decks.Add(deckHeader);
      }
    }
    return netCacheDecks;
  }

  private static bool DeckNeedsName(ulong deckValidityFlags)
  {
    return ((long) deckValidityFlags & 512L) != 0L;
  }

  private static bool AreDeckFlagsWild(ulong deckValidityFlags)
  {
    return ((long) deckValidityFlags & 128L) == 0L;
  }

  private static bool AreDeckFlagsLocked(ulong deckValidityFlags)
  {
    return ((long) deckValidityFlags & 1024L) != 0L;
  }

  public static NetCache.DeckHeader GetCreatedDeck()
  {
    DeckCreated deckCreated = Network.s_connectApi.DeckCreated();
    if (deckCreated == null)
      return (NetCache.DeckHeader) null;
    DeckInfo info = deckCreated.Info;
    NetCache.DeckHeader deckHeader = new NetCache.DeckHeader() { ID = info.Id, Name = info.Name, Hero = GameUtils.TranslateDbIdToCardId(info.Hero), HeroPremium = (TAG_PREMIUM) info.HeroPremium, HeroPower = GameUtils.GetHeroPowerCardIdFromHero(info.Hero), Type = info.DeckType, CardBack = info.CardBack, CardBackOverridden = info.CardBackOverride, HeroOverridden = info.HeroOverride, SeasonId = info.SeasonId, NeedsName = Network.DeckNeedsName(info.Validity), SortOrder = !info.HasSortOrder ? info.Id : info.SortOrder, IsWild = Network.AreDeckFlagsWild(info.Validity), Locked = Network.AreDeckFlagsLocked(info.Validity), SourceType = !info.HasSourceType ? DeckSourceType.DECK_SOURCE_TYPE_UNKNOWN : info.SourceType };
    deckHeader.CreateDate = !info.HasCreateDate ? new DateTime?() : new DateTime?(TimeUtils.UnixTimeStampToDateTime((ulong) info.CreateDate));
    return deckHeader;
  }

  public static int GetDeckLimit()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return 0;
    ProfileDeckLimit deckLimit = Network.s_connectApi.GetDeckLimit();
    if (deckLimit == null)
      return 0;
    return deckLimit.DeckLimit;
  }

  public static long GetDeletedDeckID()
  {
    DeckDeleted deckDeleted = Network.s_connectApi.DeckDeleted();
    if (deckDeleted == null)
      return 0;
    return deckDeleted.Deck;
  }

  public static Network.DeckName GetRenamedDeck()
  {
    DeckRenamed deckRenamed = Network.s_connectApi.DeckRenamed();
    if (deckRenamed == null)
      return (Network.DeckName) null;
    return new Network.DeckName() { Deck = deckRenamed.Deck, Name = deckRenamed.Name };
  }

  public static Network.GenericResponse GetGenericResponse()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new Network.GenericResponse() { RequestId = 0, RequestSubId = 1, ResultCode = Network.GenericResponse.Result.OK };
    PegasusUtil.GenericResponse genericResponse = Network.s_connectApi.GetGenericResponse();
    if (genericResponse == null)
      return (Network.GenericResponse) null;
    return new Network.GenericResponse() { ResultCode = (Network.GenericResponse.Result) genericResponse.ResultCode, RequestId = genericResponse.RequestId, RequestSubId = !genericResponse.HasRequestSubId ? 0 : genericResponse.RequestSubId, GenericData = (object) genericResponse.GenericData };
  }

  public static long GetArcaneDustBalance()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return 0;
    ArcaneDustBalance arcaneDustBalance = Network.s_connectApi.GetArcaneDustBalance();
    if (arcaneDustBalance == null)
      return 0;
    return arcaneDustBalance.Balance;
  }

  public static NetCache.NetCacheGoldBalance GetGoldBalance()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheGoldBalance();
    GoldBalance goldBalance = Network.s_connectApi.GetGoldBalance();
    if (goldBalance == null)
      return (NetCache.NetCacheGoldBalance) null;
    return new NetCache.NetCacheGoldBalance() { CappedBalance = goldBalance.CappedBalance, BonusBalance = goldBalance.BonusBalance, Cap = goldBalance.Cap, CapWarning = goldBalance.CapWarning };
  }

  public static void RequestNetCacheObject(GetAccountInfo.Request request)
  {
    Network.s_connectApi.RequestAccountInfoNetCacheObject(request);
  }

  public static void RequestNetCacheObjectList(List<GetAccountInfo.Request> requestList, List<GenericRequest> genericRequests)
  {
    Network.s_connectApi.RequestNetCacheObjectList(requestList, genericRequests);
  }

  public static NetCache.NetCacheProfileProgress GetProfileProgress()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheProfileProgress() { CampaignProgress = global::Options.Get().GetEnum<TutorialProgress>(Option.LOCAL_TUTORIAL_PROGRESS) };
    ProfileProgress profileProgress = Network.s_connectApi.GetProfileProgress();
    if (profileProgress == null)
      return (NetCache.NetCacheProfileProgress) null;
    return new NetCache.NetCacheProfileProgress() { CampaignProgress = (TutorialProgress) profileProgress.Progress, BestForgeWins = profileProgress.BestForge, LastForgeDate = !profileProgress.HasLastForge ? 0L : TimeUtils.PegDateToFileTimeUtc(profileProgress.LastForge), DisplayBanner = profileProgress.DisplayBanner };
  }

  public static void SetProgress(long value)
  {
    Network.s_connectApi.SetProgress(value);
  }

  public static SetProgressResponse GetSetProgressResponse()
  {
    return Network.s_connectApi.GetSetProgressResponse();
  }

  public static List<NetCache.ProfileNotice> GetProfileNotices()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new List<NetCache.ProfileNotice>();
    List<NetCache.ProfileNotice> result = new List<NetCache.ProfileNotice>();
    Network.HandleProfileNotices(Network.s_connectApi.GetProfileNotices().List, ref result);
    return result;
  }

  public static List<NetCache.ProfileNotice> GetProfileNoticeNotifications()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new List<NetCache.ProfileNotice>();
    List<PegasusUtil.ProfileNotice> notices = new List<PegasusUtil.ProfileNotice>();
    List<NetCache.ProfileNotice> result = new List<NetCache.ProfileNotice>();
    using (List<NoticeNotification>.Enumerator enumerator = Network.s_connectApi.GetProfileNoticeNotifications().NoticeNotifications_.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NoticeNotification current = enumerator.Current;
        notices.Add(current.Notice);
      }
    }
    Network.HandleProfileNotices(notices, ref result);
    return result;
  }

  private static void HandleProfileNotices(List<PegasusUtil.ProfileNotice> notices, ref List<NetCache.ProfileNotice> result)
  {
    using (List<PegasusUtil.ProfileNotice>.Enumerator enumerator = notices.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PegasusUtil.ProfileNotice current = enumerator.Current;
        NetCache.ProfileNotice profileNotice = (NetCache.ProfileNotice) null;
        if (current.HasMedal)
        {
          NetCache.ProfileNoticeMedal profileNoticeMedal = new NetCache.ProfileNoticeMedal() { StarLevel = current.Medal.StarLevel, LegendRank = !current.Medal.HasLegendRank ? 0 : current.Medal.LegendRank, BestStarLevel = !current.Medal.HasBestStarLevel ? 0 : current.Medal.BestStarLevel, IsWild = current.Medal.HasMedalType_ && current.Medal.MedalType_ == PegasusShared.ProfileNoticeMedal.MedalType.WILD_MEDAL };
          if (current.Medal.HasChest)
            profileNoticeMedal.Chest = Network.ConvertRewardChest(current.Medal.Chest);
          profileNotice = (NetCache.ProfileNotice) profileNoticeMedal;
        }
        else if (current.HasRewardBooster)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardBooster()
          {
            Id = current.RewardBooster.BoosterType,
            Count = current.RewardBooster.BoosterCount
          };
        else if (current.HasRewardCard)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardCard()
          {
            CardID = GameUtils.TranslateDbIdToCardId(current.RewardCard.Card.Asset),
            Premium = (!current.RewardCard.Card.HasPremium ? TAG_PREMIUM.NORMAL : (TAG_PREMIUM) current.RewardCard.Card.Premium),
            Quantity = (!current.RewardCard.HasQuantity ? 1 : current.RewardCard.Quantity)
          };
        else if (current.HasPreconDeck)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticePreconDeck()
          {
            DeckID = current.PreconDeck.Deck,
            HeroAsset = current.PreconDeck.Hero
          };
        else if (current.HasRewardDust)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardDust()
          {
            Amount = current.RewardDust.Amount
          };
        else if (current.HasRewardMount)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardMount()
          {
            MountID = current.RewardMount.MountId
          };
        else if (current.HasRewardForge)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardForge()
          {
            Quantity = current.RewardForge.Quantity
          };
        else if (current.HasRewardGold)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardGold()
          {
            Amount = current.RewardGold.Amount
          };
        else if (current.HasPurchase)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticePurchase()
          {
            ProductID = current.Purchase.ProductId,
            Data = (!current.Purchase.HasData ? 0L : current.Purchase.Data),
            CurrencyType = (!current.Purchase.HasCurrency ? Currency.UNKNOWN : (Currency) current.Purchase.Currency)
          };
        else if (current.HasRewardCardBack)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeRewardCardBack()
          {
            CardBackID = current.RewardCardBack.CardBack
          };
        else if (current.HasBonusStars)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeBonusStars()
          {
            StarLevel = current.BonusStars.StarLevel,
            Stars = current.BonusStars.Stars
          };
        else if (current.HasDcGameResult)
        {
          if (!current.DcGameResult.HasGameType)
          {
            Debug.LogError((object) "Network.GetProfileNotices(): Missing GameType");
            continue;
          }
          if (!current.DcGameResult.HasMissionId)
          {
            Debug.LogError((object) "Network.GetProfileNotices(): Missing GameType");
            continue;
          }
          if (!current.DcGameResult.HasGameResult_)
          {
            Debug.LogError((object) "Network.GetProfileNotices(): Missing GameResult");
            continue;
          }
          NetCache.ProfileNoticeDisconnectedGame disconnectedGame = new NetCache.ProfileNoticeDisconnectedGame() { GameType = current.DcGameResult.GameType, FormatType = current.DcGameResult.FormatType, MissionId = current.DcGameResult.MissionId, GameResult = current.DcGameResult.GameResult_ };
          if (disconnectedGame.GameResult == ProfileNoticeDisconnectedGameResult.GameResult.GR_WINNER)
          {
            if (!current.DcGameResult.HasYourResult || !current.DcGameResult.HasOpponentResult)
            {
              Debug.LogError((object) "Network.GetProfileNotices(): Missing PlayerResult");
              continue;
            }
            disconnectedGame.YourResult = current.DcGameResult.YourResult;
            disconnectedGame.OpponentResult = current.DcGameResult.OpponentResult;
          }
          profileNotice = (NetCache.ProfileNotice) disconnectedGame;
        }
        else if (current.HasAdventureProgress)
        {
          NetCache.ProfileNoticeAdventureProgress adventureProgress = new NetCache.ProfileNoticeAdventureProgress() { Wing = current.AdventureProgress.WingId };
          switch ((NetCache.ProfileNotice.NoticeOrigin) current.Origin)
          {
            case NetCache.ProfileNotice.NoticeOrigin.ADVENTURE_PROGRESS:
              adventureProgress.Progress = new int?(!current.HasOriginData ? 0 : (int) current.OriginData);
              break;
            case NetCache.ProfileNotice.NoticeOrigin.ADVENTURE_FLAGS:
              adventureProgress.Flags = new ulong?(!current.HasOriginData ? 0UL : (ulong) current.OriginData);
              break;
          }
          profileNotice = (NetCache.ProfileNotice) adventureProgress;
        }
        else if (current.HasLevelUp)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeLevelUp()
          {
            HeroClass = current.LevelUp.HeroClass,
            NewLevel = current.LevelUp.NewLevel
          };
        else if (current.HasAccountLicense)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeAcccountLicense()
          {
            License = current.AccountLicense.License,
            CasID = current.AccountLicense.CasId
          };
        else if (current.HasTavernBrawlRewards)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeTavernBrawlRewards()
          {
            Chest = current.TavernBrawlRewards.RewardChest,
            Wins = current.TavernBrawlRewards.NumWins
          };
        else if (current.HasTavernBrawlTicket)
          profileNotice = (NetCache.ProfileNotice) new NetCache.ProfileNoticeTavernBrawlTicket()
          {
            TicketType = current.TavernBrawlTicket.TicketType,
            Quantity = current.TavernBrawlTicket.Quantity
          };
        else
          Debug.LogError((object) "Network.GetProfileNotices(): Unrecognized profile notice");
        if (profileNotice == null)
        {
          Debug.LogError((object) "Network.GetProfileNotices(): Unhandled notice type! This notice will be lost!");
        }
        else
        {
          profileNotice.NoticeID = current.Entry;
          profileNotice.Origin = (NetCache.ProfileNotice.NoticeOrigin) current.Origin;
          profileNotice.OriginData = !current.HasOriginData ? 0L : current.OriginData;
          profileNotice.Date = TimeUtils.PegDateToFileTimeUtc(current.When);
          result.Add(profileNotice);
        }
      }
    }
  }

  public static NetCache.NetCachePlayQueue GetPlayQueue()
  {
    PlayQueue playQueue = Network.s_connectApi.GetPlayQueue();
    if (playQueue == null)
      return (NetCache.NetCachePlayQueue) null;
    return new NetCache.NetCachePlayQueue() { GameType = playQueue.Queue.GameType };
  }

  public static NetCache.NetCacheBoosters GetBoosters()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheBoosters();
    BoosterList boosters = Network.s_connectApi.GetBoosters();
    if (boosters == null)
      return (NetCache.NetCacheBoosters) null;
    NetCache.NetCacheBoosters netCacheBoosters = new NetCache.NetCacheBoosters();
    using (List<BoosterInfo>.Enumerator enumerator = boosters.List.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoosterInfo current = enumerator.Current;
        NetCache.BoosterStack boosterStack = new NetCache.BoosterStack() { Id = current.Type, Count = current.Count };
        netCacheBoosters.BoosterStacks.Add(boosterStack);
      }
    }
    return netCacheBoosters;
  }

  public static NetCache.NetCacheMedalInfo GetMedalInfo()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheMedalInfo() { Standard = new MedalInfoData(), Wild = new MedalInfoData() };
    MedalInfo medalInfo = Network.s_connectApi.GetMedalInfo();
    if (medalInfo == null)
      return (NetCache.NetCacheMedalInfo) null;
    return new NetCache.NetCacheMedalInfo() { Standard = NetCache.NetCacheMedalInfo.CloneMedalInfoData(medalInfo.Standard), Wild = NetCache.NetCacheMedalInfo.CloneMedalInfoData(medalInfo.Wild) };
  }

  public static NetCache.NetCacheFeatures GetFeatures()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheFeatures();
    GuardianVars guardianVars = Network.s_connectApi.GetGuardianVars();
    if (guardianVars == null)
      return (NetCache.NetCacheFeatures) null;
    return new NetCache.NetCacheFeatures() { Games = { Tournament = !guardianVars.HasTourney || guardianVars.Tourney, Practice = !guardianVars.HasPractice || guardianVars.Practice, Casual = !guardianVars.HasCasual || guardianVars.Casual, Forge = !guardianVars.HasForge || guardianVars.Forge, Friendly = !guardianVars.HasFriendly || guardianVars.Friendly, TavernBrawl = !guardianVars.HasTavernBrawl || guardianVars.TavernBrawl, ShowUserUI = !guardianVars.HasShowUserUI ? 0 : guardianVars.ShowUserUI }, Collection = { Manager = !guardianVars.HasManager || guardianVars.Manager, Crafting = !guardianVars.HasCrafting || guardianVars.Crafting }, Store = { Store = !guardianVars.HasStore || guardianVars.Store, BattlePay = !guardianVars.HasBattlePay || guardianVars.BattlePay, BuyWithGold = !guardianVars.HasBuyWithGold || guardianVars.BuyWithGold }, Heroes = { Hunter = !guardianVars.HasHunter || guardianVars.Hunter, Mage = !guardianVars.HasMage || guardianVars.Mage, Paladin = !guardianVars.HasPaladin || guardianVars.Paladin, Priest = !guardianVars.HasPriest || guardianVars.Priest, Rogue = !guardianVars.HasRogue || guardianVars.Rogue, Shaman = !guardianVars.HasShaman || guardianVars.Shaman, Warlock = !guardianVars.HasWarlock || guardianVars.Warlock, Warrior = !guardianVars.HasWarrior || guardianVars.Warrior }, Misc = { ClientOptionsUpdateIntervalSeconds = !guardianVars.HasClientOptionsUpdateIntervalSeconds ? 0 : guardianVars.ClientOptionsUpdateIntervalSeconds }, CaisEnabledNonMobile = !guardianVars.HasCaisEnabledNonMobile || guardianVars.CaisEnabledNonMobile, CaisEnabledMobileChina = guardianVars.HasCaisEnabledMobileChina && guardianVars.CaisEnabledMobileChina, CaisEnabledMobileSouthKorea = guardianVars.HasCaisEnabledMobileSouthKorea && guardianVars.CaisEnabledMobileSouthKorea, SendTelemetryPresence = guardianVars.HasSendTelemetryPresence && guardianVars.SendTelemetryPresence };
  }

  public static void ReadClientOptions(Map<ServerOption, NetCache.ClientOptionBase> state)
  {
    if (!Network.ShouldBeConnectedToAurora())
      return;
    ClientOptions clientOptions = Network.s_connectApi.GetClientOptions();
    if (clientOptions == null)
      return;
    if (clientOptions.HasFailed && clientOptions.Failed)
    {
      Debug.LogError((object) "ReadClientOptions: packet.Failed=true. Unable to retrieve client options from UtilServer.");
      Network.Get().ShowConnectionFailureError("GLOBAL_ERROR_NETWORK_GENERIC");
    }
    else
    {
      using (List<PegasusUtil.ClientOption>.Enumerator enumerator = clientOptions.Options.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          PegasusUtil.ClientOption current = enumerator.Current;
          ServerOption index = (ServerOption) current.Index;
          if (current.HasAsInt32)
            state[index] = (NetCache.ClientOptionBase) new NetCache.ClientOptionInt(current.AsInt32);
          else if (current.HasAsInt64)
            state[index] = (NetCache.ClientOptionBase) new NetCache.ClientOptionLong(current.AsInt64);
          else if (current.HasAsFloat)
            state[index] = (NetCache.ClientOptionBase) new NetCache.ClientOptionFloat(current.AsFloat);
          else if (current.HasAsUint64)
            state[index] = (NetCache.ClientOptionBase) new NetCache.ClientOptionULong(current.AsUint64);
        }
      }
    }
  }

  public static NetCache.NetCachePlayerRecords GetPlayerRecords()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCachePlayerRecords();
    PlayerRecords playerRecords = Network.s_connectApi.GetPlayerRecords();
    if (playerRecords == null)
      return (NetCache.NetCachePlayerRecords) null;
    NetCache.NetCachePlayerRecords cachePlayerRecords = new NetCache.NetCachePlayerRecords();
    using (List<PegasusUtil.PlayerRecord>.Enumerator enumerator = playerRecords.Records.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PegasusUtil.PlayerRecord current = enumerator.Current;
        cachePlayerRecords.Records.Add(new NetCache.PlayerRecord()
        {
          RecordType = current.Type,
          Data = !current.HasData ? 0 : current.Data,
          Wins = current.Wins,
          Losses = current.Losses,
          Ties = current.Ties
        });
      }
    }
    return cachePlayerRecords;
  }

  public static NetCache.NetCacheRewardProgress GetRewardProgress()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheRewardProgress();
    RewardProgress rewardProgress = Network.s_connectApi.GetRewardProgress();
    if (rewardProgress == null)
      return (NetCache.NetCacheRewardProgress) null;
    return new NetCache.NetCacheRewardProgress() { Season = rewardProgress.SeasonNumber, SeasonEndDate = TimeUtils.PegDateToFileTimeUtc(rewardProgress.SeasonEnd), WinsPerGold = rewardProgress.WinsPerGold, GoldPerReward = rewardProgress.GoldPerReward, MaxGoldPerDay = rewardProgress.MaxGoldPerDay, PackRewardId = !rewardProgress.HasPackId ? 1 : rewardProgress.PackId, XPSoloLimit = rewardProgress.XpSoloLimit, MaxHeroLevel = rewardProgress.MaxHeroLevel, NextQuestCancelDate = TimeUtils.PegDateToFileTimeUtc(rewardProgress.NextQuestCancel), SpecialEventTimingMod = rewardProgress.EventTimingMod, FriendWeekConcederMaxDefense = rewardProgress.FriendWeekConcederMaxDefense };
  }

  public static NetCache.NetCacheGamesPlayed GetGamesInfo()
  {
    GamesInfo gamesInfo = Network.s_connectApi.GetGamesInfo();
    if (gamesInfo == null)
      return (NetCache.NetCacheGamesPlayed) null;
    return new NetCache.NetCacheGamesPlayed() { GamesStarted = gamesInfo.GamesStarted, GamesWon = gamesInfo.GamesWon, GamesLost = gamesInfo.GamesLost, FreeRewardProgress = gamesInfo.FreeRewardProgress };
  }

  public static NotSoMassiveLoginReply GetNotSoMassiveLoginReply()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NotSoMassiveLoginReply();
    return Network.s_connectApi.GetNotSoMassiveLoginReply();
  }

  public static ClientStaticAssetsResponse GetClientStaticAssetsResponse()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new ClientStaticAssetsResponse();
    return Network.s_connectApi.GetClientStaticAssetsResponse();
  }

  public static TavernBrawlInfo GetTavernBrawlInfo()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new TavernBrawlInfo();
    return Network.s_connectApi.GetTavernBrawlInfo();
  }

  public static TavernBrawlRequestSessionBeginResponse GetTavernBrawlSessionBegin()
  {
    return Network.s_connectApi.GetTavernBrawlSessionBeginResponse();
  }

  public static void TavernBrawlRetire()
  {
    Network.s_connectApi.TavernBrawlRetire();
  }

  public static TavernBrawlRequestSessionRetireResponse GetTavernBrawlSessionRetired()
  {
    return Network.s_connectApi.GetTavernBrawlSessionRetired();
  }

  public static void RequestTavernBrawlSessionBegin()
  {
    Network.s_connectApi.RequestTavernBrawlSessionBegin();
  }

  public static void AckTavernBrawlSessionRewards()
  {
    Network.s_connectApi.AckTavernBrawlSessionRewards();
  }

  public static TavernBrawlPlayerRecord GetTavernBrawlRecord()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new TavernBrawlPlayerRecord();
    TavernBrawlPlayerRecordResponse playerRecordResponse = Network.s_connectApi.GeTavernBrawlPlayerRecordResponse();
    if (playerRecordResponse == null)
      return (TavernBrawlPlayerRecord) null;
    return playerRecordResponse.Record;
  }

  public static FavoriteHeroesResponse GetFavoriteHeroesResponse()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new FavoriteHeroesResponse();
    return Network.s_connectApi.GetFavoriteHeroesResponse();
  }

  public static AccountLicensesInfoResponse GetAccountLicensesInfoResponse()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new AccountLicensesInfoResponse();
    return Network.s_connectApi.GetAccountLicensesInfoResponse();
  }

  public static void RequestAccountLicensesUpdate()
  {
    Network.s_connectApi.RequestAccountLicensesUpdate();
  }

  public static UpdateAccountLicensesResponse GetUpdateAccountLicensesResponse()
  {
    return Network.s_connectApi.GetUpdateAccountLicensesResponse();
  }

  public static UpdateLoginComplete GetUpdateLoginComplete()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new UpdateLoginComplete();
    return Network.s_connectApi.GetUpdateLoginComplete();
  }

  public static NetCache.NetCacheHeroLevels GetAllHeroXP()
  {
    if (!Network.ShouldBeConnectedToAurora())
      return new NetCache.NetCacheHeroLevels();
    HeroXP allHeroXp = Network.s_connectApi.GetAllHeroXp();
    if (allHeroXp == null)
      return (NetCache.NetCacheHeroLevels) null;
    NetCache.NetCacheHeroLevels netCacheHeroLevels = new NetCache.NetCacheHeroLevels();
    using (List<HeroXPInfo>.Enumerator enumerator = allHeroXp.XpInfos.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroXPInfo current = enumerator.Current;
        NetCache.HeroLevel heroLevel = new NetCache.HeroLevel();
        heroLevel.Class = (TAG_CLASS) current.ClassId;
        heroLevel.CurrentLevel.Level = current.Level;
        heroLevel.CurrentLevel.XP = current.CurrXp;
        heroLevel.CurrentLevel.MaxXP = current.MaxXp;
        netCacheHeroLevels.Levels.Add(heroLevel);
        if (current.HasNextReward)
        {
          heroLevel.NextReward = new NetCache.HeroLevel.NextLevelReward();
          heroLevel.NextReward.Level = current.NextReward.Level;
          if (current.NextReward.HasRewardBooster)
            heroLevel.NextReward.Reward = (RewardData) new BoosterPackRewardData(current.NextReward.RewardBooster.BoosterType, current.NextReward.RewardBooster.BoosterCount);
          else if (current.NextReward.HasRewardCard)
          {
            string cardId = GameUtils.TranslateDbIdToCardId(current.NextReward.RewardCard.Card.Asset);
            TAG_PREMIUM premium = !current.NextReward.RewardCard.Card.HasPremium ? TAG_PREMIUM.NORMAL : (TAG_PREMIUM) current.NextReward.RewardCard.Card.Premium;
            EntityDef entityDef = DefLoader.Get().GetEntityDef(cardId);
            int count = !entityDef.IsHero() ? (premium != TAG_PREMIUM.GOLDEN ? (!entityDef.IsElite() ? 2 : 1) : 1) : 1;
            heroLevel.NextReward.Reward = (RewardData) new CardRewardData(cardId, premium, count);
          }
          else if (current.NextReward.HasRewardDust)
            heroLevel.NextReward.Reward = (RewardData) new ArcaneDustRewardData(current.NextReward.RewardDust.Amount);
          else if (current.NextReward.HasRewardGold)
            heroLevel.NextReward.Reward = (RewardData) new GoldRewardData((long) current.NextReward.RewardGold.Amount);
          else if (current.NextReward.HasRewardMount)
            heroLevel.NextReward.Reward = (RewardData) new MountRewardData((MountRewardData.MountType) current.NextReward.RewardMount.MountId);
          else if (current.NextReward.HasRewardForge)
            heroLevel.NextReward.Reward = (RewardData) new ForgeTicketRewardData(current.NextReward.RewardForge.Quantity);
          else
            Debug.LogWarning((object) string.Format("Network.GetAllHeroXP(): next reward for hero {0} is at level {1} but has no recognized reward type in packet", (object) heroLevel.Class, (object) heroLevel.NextReward.Level));
        }
      }
    }
    return netCacheHeroLevels;
  }

  public static void AckNotice(long id)
  {
    if (!NetCache.Get().RemoveNotice(id))
      return;
    Log.Achievements.Print("acking notice: {0}", (object) id);
    Network.s_connectApi.AckNotice(id);
  }

  public static void AckAchieveProgress(int id, int ackProgress)
  {
    Log.Achievements.Print("AckAchieveProgress: Achieve={0} Progress={1}", new object[2]
    {
      (object) id,
      (object) ackProgress
    });
    Network.s_connectApi.AckAchieveProgress(id, ackProgress);
  }

  public static void CheckAccountLicenseAchieve(int achieveID)
  {
    Network.s_connectApi.CheckAccountLicenseAchieve(achieveID);
  }

  public static Network.AccountLicenseAchieveResponse GetAccountLicenseAchieveResponse()
  {
    PegasusUtil.AccountLicenseAchieveResponse licenseAchieveResponse = Network.s_connectApi.GetAccountLicenseAchieveResponse();
    if (licenseAchieveResponse == null)
      return (Network.AccountLicenseAchieveResponse) null;
    return new Network.AccountLicenseAchieveResponse() { Achieve = licenseAchieveResponse.Achieve, Result = (Network.AccountLicenseAchieveResponse.AchieveResult) licenseAchieveResponse.Result_ };
  }

  public static void AckCardSeenBefore(int assetId, TAG_PREMIUM premium)
  {
    PegasusShared.CardDef cardDef = new PegasusShared.CardDef() { Asset = assetId };
    if (premium != TAG_PREMIUM.NORMAL)
      cardDef.Premium = (int) premium;
    Network.s_ackCardSeenPacket.CardDefs.Add(cardDef);
    if (Network.s_ackCardSeenPacket.CardDefs.Count <= 10)
      return;
    Network.SendAckCardsSeen();
  }

  public static void AckWingProgress(int wingId, int ackId)
  {
    Network.s_connectApi.AckWingProgress(wingId, ackId);
  }

  public static void AcknowledgeBanner(int banner)
  {
    Network.s_connectApi.AcknowledgeBanner(banner);
  }

  public static void SendAckCardsSeen()
  {
    Network.s_connectApi.AckCardSeen(Network.s_ackCardSeenPacket);
  }

  public static Network.CardSaleResult GetCardSaleResult()
  {
    BoughtSoldCard cardSaleResult1 = Network.s_connectApi.GetCardSaleResult();
    if (cardSaleResult1 == null)
      return (Network.CardSaleResult) null;
    Network.CardSaleResult cardSaleResult2 = new Network.CardSaleResult() { AssetID = cardSaleResult1.Def.Asset, AssetName = GameUtils.TranslateDbIdToCardId(cardSaleResult1.Def.Asset), Premium = !cardSaleResult1.Def.HasPremium ? TAG_PREMIUM.NORMAL : (TAG_PREMIUM) cardSaleResult1.Def.Premium, Action = (Network.CardSaleResult.SaleResult) cardSaleResult1.Result_, Amount = cardSaleResult1.Amount, Count = !cardSaleResult1.HasCount ? 1 : cardSaleResult1.Count, Nerfed = cardSaleResult1.HasNerfed && cardSaleResult1.Nerfed, UnitSellPrice = !cardSaleResult1.HasUnitSellPrice ? 0 : cardSaleResult1.UnitSellPrice, UnitBuyPrice = !cardSaleResult1.HasUnitBuyPrice ? 0 : cardSaleResult1.UnitBuyPrice };
    cardSaleResult2.CurrentCollectionCount = !cardSaleResult1.HasCurrentCollectionCount ? new int?() : new int?(cardSaleResult1.CurrentCollectionCount);
    return cardSaleResult2;
  }

  public static void TriggerLaunchEvent(BnetGameAccountId lastOpponentHSGameAccountID, ulong lastOpponentSessionStartTime, BnetGameAccountId otherPlayerHSGameAccountID, ulong otherPlayerSessionStartTime)
  {
    Network.s_connectApi.TriggerLaunchEvent(lastOpponentHSGameAccountID.GetHi(), lastOpponentHSGameAccountID.GetLo(), lastOpponentSessionStartTime, otherPlayerHSGameAccountID.GetHi(), otherPlayerHSGameAccountID.GetLo(), otherPlayerSessionStartTime);
  }

  public static void RequestAssetsVersion()
  {
    Network.s_connectApi.RequestAssetsVersion(Network.GetPlatformBuilder());
  }

  public static void LoginOk()
  {
    Network.s_connectApi.OnLoginComplete();
  }

  public static AssetsVersionResponse GetAssetsVersion()
  {
    return Network.s_connectApi.GetAssetsVersionResponse();
  }

  public static GetAssetResponse GetAssetResponse()
  {
    return Network.s_connectApi.GetAssetResponse();
  }

  public static void SendAssetRequest(int clientToken, List<AssetKey> requestKeys)
  {
    if (requestKeys == null || requestKeys.Count == 0)
      return;
    Network.s_connectApi.SendAssetRequest(clientToken, requestKeys);
  }

  public static ServerResult GetServerResult()
  {
    return Network.s_connectApi.GetServerResult();
  }

  private static PegasusShared.Platform GetPlatformBuilder()
  {
    PegasusShared.Platform platform = new PegasusShared.Platform() { Os = (int) PlatformSettings.OS, Screen = (int) PlatformSettings.Screen, Name = PlatformSettings.DeviceName, UniqueDeviceIdentifier = SystemInfo.deviceUniqueIdentifier };
    AndroidStore androidStore = ApplicationMgr.GetAndroidStore();
    if (androidStore != AndroidStore.NONE)
      platform.Store = (int) androidStore;
    return platform;
  }

  public static bool SendDebugConsoleCommand(string command)
  {
    if (!Network.IsConnectedToGameServer())
    {
      Log.Rachelle.Print(string.Format("Cannot send command '{0}' to server; no game server is active.", (object) command));
      return false;
    }
    if (Network.s_connectApi.AllowDebugConnections() && command != null)
      Network.s_connectApi.SendDebugConsoleCommand(command);
    return true;
  }

  public static void SendDebugConsoleResponse(int responseType, string message)
  {
    Network.s_connectApi.SendDebugConsoleResponse(responseType, message);
  }

  public static string GetDebugConsoleCommand()
  {
    DebugConsoleCommand debugConsoleCommand = Network.s_connectApi.GetDebugConsoleCommand();
    if (debugConsoleCommand == null)
      return string.Empty;
    return debugConsoleCommand.Command;
  }

  public static Network.DebugConsoleResponse GetDebugConsoleResponse()
  {
    BobNetProto.DebugConsoleResponse debugConsoleResponse = Network.s_connectApi.GetDebugConsoleResponse();
    if (debugConsoleResponse == null)
      return (Network.DebugConsoleResponse) null;
    return new Network.DebugConsoleResponse() { Type = (int) debugConsoleResponse.ResponseType_, Response = debugConsoleResponse.Response };
  }

  public static void SendDebugCommandRequest(DebugCommandRequest packet)
  {
    Network.s_connectApi.SendDebugCommandRequest(packet);
  }

  public static DebugCommandResponse GetDebugCommandResponse()
  {
    return Network.s_connectApi.GetDebugCommandResponse();
  }

  public static void SimulateUncleanDisconnectFromGameServer()
  {
    if (!Network.s_connectApi.HasGameServerConnection())
      return;
    Network.s_connectApi.DisconnectFromGameServer();
  }

  private static string GetStoredUserName()
  {
    return (string) null;
  }

  private static string GetStoredBNetIP()
  {
    return (string) null;
  }

  private static string GetStoredVersion()
  {
    return (string) null;
  }

  public enum DraftError
  {
    DE_UNKNOWN,
    DE_NO_LICENSE,
    DE_RETIRE_FIRST,
    DE_NOT_IN_DRAFT,
    DE_BAD_DECK,
    DE_BAD_SLOT,
    DE_BAD_INDEX,
    DE_NOT_IN_DRAFT_BUT_COULD_BE,
    DE_FEATURE_DISABLED,
  }

  public enum BnetLoginState
  {
    BATTLE_NET_UNKNOWN,
    BATTLE_NET_LOGGING_IN,
    BATTLE_NET_TIMEOUT,
    BATTLE_NET_LOGIN_FAILED,
    BATTLE_NET_LOGGED_IN,
  }

  public enum BoosterSource
  {
    UNKNOWN = 0,
    ARENA_REWARD = 3,
    BOUGHT = 4,
    LICENSED = 6,
    CS_GIFT = 8,
    QUEST_REWARD = 10,
    BOUGHT_GOLD = 11,
  }

  public enum Version
  {
    Minor = 0,
    Patch = 0,
    Sku = 0,
    Major = 7,
  }

  public class ConnectErrorParams : ErrorParams
  {
    public float m_creationTime;

    public ConnectErrorParams()
    {
      this.m_creationTime = Time.realtimeSinceStartup;
    }
  }

  private class RequestContext
  {
    public DateTime m_waitUntil;
    public int m_pendingResponseId;
    public int m_requestId;
    public int m_requestSubId;
    public Network.TimeoutHandler m_timeoutHandler;

    public RequestContext(int pendingResponseId, int requestId, int requestSubId, Network.TimeoutHandler timeoutHandler)
    {
      this.m_waitUntil = DateTime.Now + Network.GetMaxDeferredWait();
      this.m_pendingResponseId = pendingResponseId;
      this.m_requestId = requestId;
      this.m_requestSubId = requestSubId;
      this.m_timeoutHandler = timeoutHandler;
    }
  }

  public class UnavailableReason
  {
    public string mainReason;
    public string subReason;
    public string extraData;
  }

  private class BnetErrorListener : EventListener<Network.BnetErrorCallback>
  {
    public bool Fire(BnetErrorInfo info)
    {
      return this.m_callback(info, this.m_userData);
    }
  }

  public enum AuthResult
  {
    UNKNOWN,
    ALLOWED,
    INVALID,
    SECOND,
    OFFLINE,
  }

  public class QueueInfo
  {
    public int position;
    public long end;
    public long stdev;
  }

  public class CanceledQuest
  {
    public int AchieveID { get; set; }

    public bool Canceled { get; set; }

    public long NextQuestCancelDate { get; set; }

    public CanceledQuest()
    {
      this.AchieveID = 0;
      this.Canceled = false;
      this.NextQuestCancelDate = 0L;
    }

    public override string ToString()
    {
      return string.Format("[CanceledQuest AchieveID={0} Canceled={1} NextQuestCancelDate={2}]", (object) this.AchieveID, (object) this.Canceled, (object) this.NextQuestCancelDate);
    }
  }

  public class TriggeredEvent
  {
    public int EventID { get; set; }

    public bool Success { get; set; }

    public TriggeredEvent()
    {
      this.EventID = 0;
      this.Success = false;
    }
  }

  public class AdventureProgress
  {
    public int Wing { get; set; }

    public int Progress { get; set; }

    public int Ack { get; set; }

    public ulong Flags { get; set; }

    public AdventureProgress()
    {
      this.Wing = 0;
      this.Progress = 0;
      this.Ack = 0;
      this.Flags = 0UL;
    }
  }

  public class CardSaleResult
  {
    public Network.CardSaleResult.SaleResult Action { get; set; }

    public int AssetID { get; set; }

    public string AssetName { get; set; }

    public TAG_PREMIUM Premium { get; set; }

    public int Amount { get; set; }

    public int Count { get; set; }

    public bool Nerfed { get; set; }

    public int UnitSellPrice { get; set; }

    public int UnitBuyPrice { get; set; }

    public int? CurrentCollectionCount { get; set; }

    public override string ToString()
    {
      return string.Format("[CardSaleResult Action={0} assetName={1} premium={2} amount={3} count={4}]", (object) this.Action, (object) this.AssetName, (object) this.Premium, (object) this.Amount, (object) this.Count);
    }

    public enum SaleResult
    {
      GENERIC_FAILURE = 1,
      CARD_WAS_SOLD = 2,
      CARD_WAS_BOUGHT = 3,
      SOULBOUND = 4,
      FAILED_WRONG_SELL_PRICE = 5,
      FAILED_WRONG_BUY_PRICE = 6,
      FAILED_NO_PERMISSION = 7,
      FAILED_EVENT_NOT_ACTIVE = 8,
      COUNT_MISMATCH = 9,
    }
  }

  public class BeginDraft
  {
    public long DeckID { get; set; }

    public List<NetCache.CardDefinition> Heroes { get; set; }

    public BeginDraft()
    {
      this.Heroes = new List<NetCache.CardDefinition>();
    }
  }

  public class DraftChoicesAndContents
  {
    public int Slot { get; set; }

    public List<NetCache.CardDefinition> Choices { get; set; }

    public NetCache.CardDefinition Hero { get; set; }

    public Network.DeckContents DeckInfo { get; set; }

    public int Wins { get; set; }

    public int Losses { get; set; }

    public Network.RewardChest Chest { get; set; }

    public int MaxWins { get; set; }

    public DraftChoicesAndContents()
    {
      this.Choices = new List<NetCache.CardDefinition>();
      this.Hero = new NetCache.CardDefinition();
      this.DeckInfo = new Network.DeckContents();
      this.Chest = (Network.RewardChest) null;
    }
  }

  public class DraftChosen
  {
    public NetCache.CardDefinition ChosenCard { get; set; }

    public List<NetCache.CardDefinition> NextChoices { get; set; }

    public DraftChosen()
    {
      this.ChosenCard = new NetCache.CardDefinition();
      this.NextChoices = new List<NetCache.CardDefinition>();
    }
  }

  public class RewardChest
  {
    public List<RewardData> Rewards { get; set; }

    public RewardChest()
    {
      this.Rewards = new List<RewardData>();
    }
  }

  public class DraftRetired
  {
    public long Deck { get; set; }

    public Network.RewardChest Chest { get; set; }

    public DraftRetired()
    {
      this.Deck = 0L;
      this.Chest = new Network.RewardChest();
    }
  }

  public class MassDisenchantResponse
  {
    public int Amount { get; set; }

    public MassDisenchantResponse()
    {
      this.Amount = 0;
    }
  }

  public class SetFavoriteHeroResponse
  {
    public bool Success;
    public TAG_CLASS HeroClass;
    public NetCache.CardDefinition Hero;

    public SetFavoriteHeroResponse()
    {
      this.Success = false;
      this.HeroClass = TAG_CLASS.INVALID;
      this.Hero = (NetCache.CardDefinition) null;
    }
  }

  public class PurchaseErrorInfo
  {
    public Network.PurchaseErrorInfo.ErrorType Error { get; set; }

    public string PurchaseInProgressProductID { get; set; }

    public string ErrorCode { get; set; }

    public PurchaseErrorInfo()
    {
      this.Error = Network.PurchaseErrorInfo.ErrorType.UNKNOWN;
      this.PurchaseInProgressProductID = string.Empty;
      this.ErrorCode = string.Empty;
    }

    public enum ErrorType
    {
      UNKNOWN = -1,
      SUCCESS = 0,
      STILL_IN_PROGRESS = 1,
      INVALID_BNET = 2,
      SERVICE_NA = 3,
      PURCHASE_IN_PROGRESS = 4,
      DATABASE = 5,
      INVALID_QUANTITY = 6,
      DUPLICATE_LICENSE = 7,
      REQUEST_NOT_SENT = 8,
      NO_ACTIVE_BPAY = 9,
      FAILED_RISK = 10,
      CANCELED = 11,
      WAIT_MOP = 12,
      WAIT_CONFIRM = 13,
      WAIT_RISK = 14,
      PRODUCT_NA = 15,
      RISK_TIMEOUT = 16,
      PRODUCT_ALREADY_OWNED = 17,
      WAIT_THIRD_PARTY_RECEIPT = 18,
      PRODUCT_EVENT_HAS_ENDED = 19,
      BP_GENERIC_FAIL = 100,
      BP_INVALID_CC_EXPIRY = 101,
      BP_RISK_ERROR = 102,
      BP_NO_VALID_PAYMENT = 103,
      BP_PAYMENT_AUTH = 104,
      BP_PROVIDER_DENIED = 105,
      BP_PURCHASE_BAN = 106,
      BP_SPENDING_LIMIT = 107,
      BP_PARENTAL_CONTROL = 108,
      BP_THROTTLED = 109,
      BP_THIRD_PARTY_BAD_RECEIPT = 110,
      BP_THIRD_PARTY_RECEIPT_USED = 111,
      BP_PRODUCT_UNIQUENESS_VIOLATED = 112,
      BP_REGION_IS_DOWN = 113,
      E_BP_GENERIC_FAIL_RETRY_CONTACT_CS_IF_PERSISTS = 115,
      E_BP_CHALLENGE_ID_FAILED_VERIFICATION = 116,
    }
  }

  public class PurchaseCanceledResponse
  {
    public Network.PurchaseCanceledResponse.CancelResult Result { get; set; }

    public long TransactionID { get; set; }

    public string ProductID { get; set; }

    public Currency CurrencyType { get; set; }

    public enum CancelResult
    {
      SUCCESS,
      NOT_ALLOWED,
      NOTHING_TO_CANCEL,
    }
  }

  public class BattlePayStatus
  {
    public Network.BattlePayStatus.PurchaseState State { get; set; }

    public long TransactionID { get; set; }

    public string ThirdPartyID { get; set; }

    public string ProductID { get; set; }

    public Network.PurchaseErrorInfo PurchaseError { get; set; }

    public bool BattlePayAvailable { get; set; }

    public Currency CurrencyType { get; set; }

    public BattlePayProvider? Provider { get; set; }

    public BattlePayStatus()
    {
      this.State = Network.BattlePayStatus.PurchaseState.UNKNOWN;
      this.TransactionID = 0L;
      this.ThirdPartyID = string.Empty;
      this.ProductID = string.Empty;
      this.PurchaseError = new Network.PurchaseErrorInfo();
      this.BattlePayAvailable = false;
      this.CurrencyType = Currency.UNKNOWN;
      this.Provider = MoneyOrGTAPPTransaction.UNKNOWN_PROVIDER;
    }

    public enum PurchaseState
    {
      UNKNOWN = -1,
      READY = 0,
      CHECK_RESULTS = 1,
      ERROR = 2,
    }
  }

  public class BundleItem
  {
    public ProductType Product { get; set; }

    public int ProductData { get; set; }

    public int Quantity { get; set; }

    public BundleItem()
    {
      this.Product = ProductType.PRODUCT_TYPE_UNKNOWN;
      this.ProductData = 0;
      this.Quantity = 0;
    }

    public override bool Equals(object obj)
    {
      Network.BundleItem bundleItem = obj as Network.BundleItem;
      if (bundleItem == null || bundleItem.Product != this.Product || bundleItem.ProductData != this.ProductData)
        return false;
      return bundleItem.Quantity == this.Quantity;
    }

    public override int GetHashCode()
    {
      return this.Product.GetHashCode() * this.ProductData.GetHashCode() * this.Quantity.GetHashCode();
    }
  }

  public class Bundle
  {
    public string ProductID { get; set; }

    public double? Cost { get; set; }

    public long? GoldCost { get; set; }

    public string AppleID { get; set; }

    public string GooglePlayID { get; set; }

    public string AmazonID { get; set; }

    public List<Network.BundleItem> Items { get; set; }

    public SpecialEventType ProductEvent { get; set; }

    public SpecialEventType RealMoneyProductEvent { get; set; }

    public List<BattlePayProvider> ExclusiveProviders { get; set; }

    public Bundle()
    {
      this.ProductID = string.Empty;
      this.Cost = new double?();
      this.GoldCost = new long?();
      this.AppleID = string.Empty;
      this.GooglePlayID = string.Empty;
      this.AmazonID = string.Empty;
      this.Items = new List<Network.BundleItem>();
      this.ProductEvent = SpecialEventType.UNKNOWN;
      this.RealMoneyProductEvent = SpecialEventType.UNKNOWN;
    }
  }

  public class GoldCostBooster
  {
    public long? Cost { get; set; }

    public int ID { get; set; }

    public SpecialEventType BuyWithGoldEvent { get; set; }

    public GoldCostBooster()
    {
      this.Cost = new long?();
      this.ID = 0;
      this.BuyWithGoldEvent = SpecialEventType.UNKNOWN;
    }
  }

  public class BattlePayConfig
  {
    public bool Available { get; set; }

    public Currency Currency { get; set; }

    public List<Network.Bundle> Bundles { get; set; }

    public List<Network.GoldCostBooster> GoldCostBoosters { get; set; }

    public long? GoldCostArena { get; set; }

    public int SecondsBeforeAutoCancel { get; set; }

    public BattlePayConfig()
    {
      this.Available = false;
      this.Currency = Currency.UNKNOWN;
      this.Bundles = new List<Network.Bundle>();
      this.GoldCostBoosters = new List<Network.GoldCostBooster>();
      this.GoldCostArena = new long?();
      this.SecondsBeforeAutoCancel = StoreManager.DEFAULT_SECONDS_BEFORE_AUTO_CANCEL;
    }
  }

  public class PurchaseViaGoldResponse
  {
    public Network.PurchaseViaGoldResponse.ErrorType Error { get; set; }

    public long GoldUsed { get; set; }

    public PurchaseViaGoldResponse()
    {
      this.Error = Network.PurchaseViaGoldResponse.ErrorType.UNKNOWN;
      this.GoldUsed = 0L;
    }

    public enum ErrorType
    {
      UNKNOWN = -1,
      SUCCESS = 1,
      INSUFFICIENT_GOLD = 2,
      PRODUCT_NA = 3,
      FEATURE_NA = 4,
      INVALID_QUANTITY = 5,
    }
  }

  public class PurchaseMethod
  {
    public long TransactionID { get; set; }

    public string ProductID { get; set; }

    public int Quantity { get; set; }

    public Currency Currency { get; set; }

    public string WalletName { get; set; }

    public bool UseEBalance { get; set; }

    public bool IsZeroCostLicense { get; set; }

    public string ChallengeID { get; set; }

    public string ChallengeURL { get; set; }

    public Network.PurchaseErrorInfo PurchaseError { get; set; }

    public PurchaseMethod()
    {
      this.TransactionID = 0L;
      this.ProductID = string.Empty;
      this.Quantity = 0;
      this.Currency = Currency.UNKNOWN;
      this.WalletName = string.Empty;
      this.UseEBalance = false;
      this.IsZeroCostLicense = false;
      this.ChallengeID = string.Empty;
      this.ChallengeURL = string.Empty;
      this.PurchaseError = (Network.PurchaseErrorInfo) null;
    }
  }

  public class PurchaseResponse
  {
    public Network.PurchaseErrorInfo PurchaseError { get; set; }

    public long TransactionID { get; set; }

    public string ProductID { get; set; }

    public string ThirdPartyID { get; set; }

    public Currency CurrencyType { get; set; }

    public PurchaseResponse()
    {
      this.PurchaseError = new Network.PurchaseErrorInfo();
      this.TransactionID = 0L;
      this.ProductID = string.Empty;
      this.ThirdPartyID = string.Empty;
      this.CurrencyType = Currency.UNKNOWN;
    }
  }

  public class ThirdPartyPurchaseStatusResponse
  {
    public string ThirdPartyID { get; set; }

    public Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus Status { get; set; }

    public ThirdPartyPurchaseStatusResponse()
    {
      this.ThirdPartyID = string.Empty;
      this.Status = Network.ThirdPartyPurchaseStatusResponse.PurchaseStatus.UNKNOWN;
    }

    public enum PurchaseStatus
    {
      UNKNOWN = -1,
      NOT_FOUND = 1,
      SUCCEEDED = 2,
      FAILED = 3,
      IN_PROGRESS = 4,
    }
  }

  public class CardBackResponse
  {
    public bool Success { get; set; }

    public int CardBack { get; set; }

    public CardBackResponse()
    {
      this.Success = false;
      this.CardBack = 0;
    }
  }

  public class GameCancelInfo
  {
    public Network.GameCancelInfo.Reason CancelReason { get; set; }

    public enum Reason
    {
      OPPONENT_TIMEOUT = 1,
    }
  }

  public class Entity
  {
    public int ID { get; set; }

    public List<Network.Entity.Tag> Tags { get; set; }

    public string CardID { get; set; }

    public Entity()
    {
      this.Tags = new List<Network.Entity.Tag>();
    }

    public static Network.Entity CreateFromProto(PegasusGame.Entity src)
    {
      return new Network.Entity() { ID = src.Id, CardID = string.Empty, Tags = Network.Entity.CreateTagsFromProto((IList<PegasusGame.Tag>) src.Tags) };
    }

    public static Network.Entity CreateFromProto(PowerHistoryEntity src)
    {
      return new Network.Entity() { ID = src.Entity, CardID = src.Name, Tags = Network.Entity.CreateTagsFromProto((IList<PegasusGame.Tag>) src.Tags) };
    }

    public static List<Network.Entity.Tag> CreateTagsFromProto(IList<PegasusGame.Tag> tagList)
    {
      List<Network.Entity.Tag> tagList1 = new List<Network.Entity.Tag>();
      for (int index = 0; index < tagList.Count; ++index)
      {
        PegasusGame.Tag tag = tagList[index];
        tagList1.Add(new Network.Entity.Tag()
        {
          Name = tag.Name,
          Value = tag.Value
        });
      }
      return tagList1;
    }

    public override string ToString()
    {
      return string.Format("id={0} cardId={1} tags={2}", (object) this.ID, (object) this.CardID, (object) this.Tags.Count);
    }

    public class Tag
    {
      public int Name { get; set; }

      public int Value { get; set; }
    }
  }

  public class Options
  {
    public int ID { get; set; }

    public List<Network.Options.Option> List { get; set; }

    public Options()
    {
      this.List = new List<Network.Options.Option>();
    }

    public void CopyFrom(Network.Options options)
    {
      this.ID = options.ID;
      if (options.List == null)
      {
        this.List = (List<Network.Options.Option>) null;
      }
      else
      {
        if (this.List != null)
          this.List.Clear();
        else
          this.List = new List<Network.Options.Option>();
        for (int index = 0; index < options.List.Count; ++index)
        {
          Network.Options.Option option = new Network.Options.Option();
          option.CopyFrom(options.List[index]);
          this.List.Add(option);
        }
      }
    }

    public class Option
    {
      public Network.Options.Option.OptionType Type { get; set; }

      public Network.Options.Option.SubOption Main { get; set; }

      public List<Network.Options.Option.SubOption> Subs { get; set; }

      public Option()
      {
        this.Main = new Network.Options.Option.SubOption();
        this.Subs = new List<Network.Options.Option.SubOption>();
      }

      public void CopyFrom(Network.Options.Option option)
      {
        this.Type = option.Type;
        if (this.Main == null)
          this.Main = new Network.Options.Option.SubOption();
        this.Main.CopyFrom(option.Main);
        if (option.Subs == null)
        {
          this.Subs = (List<Network.Options.Option.SubOption>) null;
        }
        else
        {
          if (this.Subs == null)
            this.Subs = new List<Network.Options.Option.SubOption>();
          else
            this.Subs.Clear();
          for (int index = 0; index < option.Subs.Count; ++index)
          {
            Network.Options.Option.SubOption subOption = new Network.Options.Option.SubOption();
            subOption.CopyFrom(option.Subs[index]);
            this.Subs.Add(subOption);
          }
        }
      }

      public enum OptionType
      {
        PASS = 1,
        END_TURN = 2,
        POWER = 3,
      }

      public class SubOption
      {
        public int ID { get; set; }

        public List<int> Targets { get; set; }

        public void CopyFrom(Network.Options.Option.SubOption subOption)
        {
          this.ID = subOption.ID;
          if (subOption.Targets == null)
          {
            this.Targets = (List<int>) null;
          }
          else
          {
            if (this.Targets == null)
              this.Targets = new List<int>();
            else
              this.Targets.Clear();
            for (int index = 0; index < subOption.Targets.Count; ++index)
              this.Targets.Add(subOption.Targets[index]);
          }
        }
      }
    }
  }

  public class EntityChoices
  {
    public int ID { get; set; }

    public CHOICE_TYPE ChoiceType { get; set; }

    public int CountMin { get; set; }

    public int CountMax { get; set; }

    public List<int> Entities { get; set; }

    public int Source { get; set; }

    public int PlayerId { get; set; }

    public bool HideChosen { get; set; }
  }

  public class EntitiesChosen
  {
    public int ID { get; set; }

    public List<int> Entities { get; set; }

    public int PlayerId { get; set; }

    public CHOICE_TYPE ChoiceType { get; set; }
  }

  public class Notification
  {
    public Network.Notification.Type NotificationType { get; set; }

    public enum Type
    {
      IN_HAND_CARD_CAP = 1,
      MANA_CAP = 2,
    }
  }

  public class GameSetup
  {
    public int Board { get; set; }

    public int MaxSecretsPerPlayer { get; set; }

    public int MaxFriendlyMinionsPerPlayer { get; set; }

    public int DisconnectWhenStuckSeconds { get; set; }
  }

  public class UserUI
  {
    public Network.UserUI.MouseInfo mouseInfo;
    public Network.UserUI.EmoteInfo emoteInfo;
    public int? playerId;

    public class MouseInfo
    {
      public int OverCardID { get; set; }

      public int HeldCardID { get; set; }

      public int ArrowOriginID { get; set; }

      public int X { get; set; }

      public int Y { get; set; }
    }

    public class EmoteInfo
    {
      public int Emote { get; set; }
    }
  }

  public enum PowerType
  {
    FULL_ENTITY = 1,
    SHOW_ENTITY = 2,
    HIDE_ENTITY = 3,
    TAG_CHANGE = 4,
    BLOCK_START = 5,
    BLOCK_END = 6,
    CREATE_GAME = 7,
    META_DATA = 8,
    CHANGE_ENTITY = 9,
  }

  public class PowerHistory
  {
    public Network.PowerType Type { get; set; }

    public PowerHistory(Network.PowerType init)
    {
      this.Type = init;
    }

    public override string ToString()
    {
      return string.Format("type={0}", (object) this.Type);
    }
  }

  public class HistBlockStart : Network.PowerHistory
  {
    public HistoryBlock.Type BlockType { get; set; }

    public int Entity { get; set; }

    public int Target { get; set; }

    public string EffectCardId { get; set; }

    public int EffectIndex { get; set; }

    public HistBlockStart(HistoryBlock.Type type)
      : base(Network.PowerType.BLOCK_START)
    {
      this.BlockType = type;
    }

    public override string ToString()
    {
      return string.Format("type={0} blockType={1} entity={2} target={3}", (object) this.Type, (object) this.BlockType, (object) this.Entity, (object) this.Target);
    }
  }

  public class HistBlockEnd : Network.PowerHistory
  {
    public HistBlockEnd()
      : base(Network.PowerType.BLOCK_END)
    {
    }
  }

  public class HistCreateGame : Network.PowerHistory
  {
    public Network.Entity Game { get; set; }

    public List<Network.HistCreateGame.PlayerData> Players { get; set; }

    public HistCreateGame()
      : base(Network.PowerType.CREATE_GAME)
    {
    }

    public static Network.HistCreateGame CreateFromProto(PowerHistoryCreateGame src)
    {
      Network.HistCreateGame histCreateGame = new Network.HistCreateGame();
      histCreateGame.Game = Network.Entity.CreateFromProto(src.GameEntity);
      if (src.Players != null)
      {
        histCreateGame.Players = new List<Network.HistCreateGame.PlayerData>();
        for (int index = 0; index < src.Players.Count; ++index)
        {
          Network.HistCreateGame.PlayerData fromProto = Network.HistCreateGame.PlayerData.CreateFromProto(src.Players[index]);
          histCreateGame.Players.Add(fromProto);
        }
      }
      return histCreateGame;
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendFormat("game={0}", (object) this.Game);
      if (this.Players == null)
        stringBuilder.Append(" players=(null)");
      else if (this.Players.Count == 0)
      {
        stringBuilder.Append(" players=0");
      }
      else
      {
        for (int index = 0; index < this.Players.Count; ++index)
          stringBuilder.AppendFormat(" players[{0}]=[{1}]", (object) index, (object) this.Players[index]);
      }
      return stringBuilder.ToString();
    }

    public class PlayerData
    {
      public int ID { get; set; }

      public BnetGameAccountId GameAccountId { get; set; }

      public Network.Entity Player { get; set; }

      public int CardBackID { get; set; }

      public static Network.HistCreateGame.PlayerData CreateFromProto(PegasusGame.Player src)
      {
        return new Network.HistCreateGame.PlayerData() { ID = src.Id, GameAccountId = BnetUtils.CreateGameAccountId(src.GameAccountId), Player = Network.Entity.CreateFromProto(src.Entity), CardBackID = src.CardBack };
      }

      public override string ToString()
      {
        return string.Format("ID={0} GameAccountId={1} Player={2} CardBackID={3}", (object) this.ID, (object) this.GameAccountId, (object) this.Player, (object) this.CardBackID);
      }
    }
  }

  public class HistFullEntity : Network.PowerHistory
  {
    public Network.Entity Entity { get; set; }

    public HistFullEntity()
      : base(Network.PowerType.FULL_ENTITY)
    {
    }

    public override string ToString()
    {
      return string.Format("type={0} entity=[{1}]", (object) this.Type, (object) this.Entity);
    }
  }

  public class HistShowEntity : Network.PowerHistory
  {
    public Network.Entity Entity { get; set; }

    public HistShowEntity()
      : base(Network.PowerType.SHOW_ENTITY)
    {
    }

    public override string ToString()
    {
      return string.Format("type={0} entity=[{1}]", (object) this.Type, (object) this.Entity);
    }
  }

  public class HistHideEntity : Network.PowerHistory
  {
    public int Entity { get; set; }

    public int Zone { get; set; }

    public HistHideEntity()
      : base(Network.PowerType.HIDE_ENTITY)
    {
    }

    public override string ToString()
    {
      return string.Format("type={0} entity={1} zone={2}", (object) this.Type, (object) this.Entity, (object) this.Zone);
    }
  }

  public class HistChangeEntity : Network.PowerHistory
  {
    public Network.Entity Entity { get; set; }

    public HistChangeEntity()
      : base(Network.PowerType.CHANGE_ENTITY)
    {
    }

    public override string ToString()
    {
      return string.Format("type={0} entity=[{1}]", (object) this.Type, (object) this.Entity);
    }
  }

  public class HistTagChange : Network.PowerHistory
  {
    public int Entity { get; set; }

    public int Tag { get; set; }

    public int Value { get; set; }

    public HistTagChange()
      : base(Network.PowerType.TAG_CHANGE)
    {
    }

    public override string ToString()
    {
      return string.Format("type={0} entity={1} tag={2} value={3}", (object) this.Type, (object) this.Entity, (object) this.Tag, (object) this.Value);
    }
  }

  public class HistMetaData : Network.PowerHistory
  {
    public HistoryMeta.Type MetaType { get; set; }

    public List<int> Info { get; set; }

    public int Data { get; set; }

    public HistMetaData()
      : base(Network.PowerType.META_DATA)
    {
      this.Info = new List<int>();
    }

    public override string ToString()
    {
      return string.Format("type={0} metaType={1} infoCount={2} data={3}", (object) this.Type, (object) this.MetaType, (object) this.Info.Count, (object) this.Data);
    }
  }

  public class CardUserData
  {
    public int DbId { get; set; }

    public int Count { get; set; }

    public TAG_PREMIUM Premium { get; set; }
  }

  public class DeckContents
  {
    public long Deck { get; set; }

    public List<Network.CardUserData> Cards { get; set; }

    public DeckContents()
    {
      this.Cards = new List<Network.CardUserData>();
    }

    public static Network.DeckContents FromPacket(PegasusUtil.DeckContents packet)
    {
      Network.DeckContents deckContents = new Network.DeckContents();
      deckContents.Deck = packet.DeckId;
      using (List<DeckCardData>.Enumerator enumerator = packet.Cards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          DeckCardData current = enumerator.Current;
          deckContents.Cards.Add(new Network.CardUserData()
          {
            DbId = current.Def.Asset,
            Count = !current.HasQty ? 1 : current.Qty,
            Premium = !current.Def.HasPremium ? TAG_PREMIUM.NORMAL : (TAG_PREMIUM) current.Def.Premium
          });
        }
      }
      return deckContents;
    }
  }

  public class DeckName
  {
    public long Deck { get; set; }

    public string Name { get; set; }
  }

  public class DeckCard
  {
    public long Deck { get; set; }

    public long Card { get; set; }
  }

  public class GenericResponse
  {
    public int RequestId { get; set; }

    public int RequestSubId { get; set; }

    public Network.GenericResponse.Result ResultCode { get; set; }

    public object GenericData { get; set; }

    public enum Result
    {
      OK = 0,
      REQUEST_IN_PROCESS = 1,
      REQUEST_COMPLETE = 2,
      FIRST_ERROR = 100,
      INTERNAL_ERROR = 101,
      DB_ERROR = 102,
      INVALID_REQUEST = 103,
      LOGIN_LOAD = 104,
      RESULT_DATA_MIGRATION_OR_PLAYER_ID_ERROR = 105,
    }
  }

  public class DBAction
  {
    public Network.DBAction.ActionType Action { get; set; }

    public Network.DBAction.ResultType Result { get; set; }

    public long MetaData { get; set; }

    public enum ActionType
    {
      UNKNOWN,
      GET_DECK,
      CREATE_DECK,
      RENAME_DECK,
      DELETE_DECK,
      SET_DECK,
      OPEN_BOOSTER,
      GAMES_INFO,
    }

    public enum ResultType
    {
      UNKNOWN,
      SUCCESS,
      NOT_OWNED,
      CONSTRAINT,
    }
  }

  public class TurnTimerInfo
  {
    public float Seconds { get; set; }

    public int Turn { get; set; }

    public bool Show { get; set; }
  }

  public class CardQuote
  {
    public int AssetID { get; set; }

    public int BuyPrice { get; set; }

    public int SaleValue { get; set; }

    public Network.CardQuote.QuoteState Status { get; set; }

    public enum QuoteState
    {
      SUCCESS,
      UNKNOWN_ERROR,
    }
  }

  public class GameEnd
  {
    public List<NetCache.ProfileNotice> Notices { get; set; }

    public GameEnd()
    {
      this.Notices = new List<NetCache.ProfileNotice>();
    }
  }

  public class ProfileNotices
  {
    public List<NetCache.ProfileNotice> Notices { get; set; }

    public ProfileNotices()
    {
      this.Notices = new List<NetCache.ProfileNotice>();
    }
  }

  public class AccountLicenseAchieveResponse
  {
    public int Achieve { get; set; }

    public Network.AccountLicenseAchieveResponse.AchieveResult Result { get; set; }

    public enum AchieveResult
    {
      INVALID_ACHIEVE = 1,
      NOT_ACTIVE = 2,
      IN_PROGRESS = 3,
      COMPLETE = 4,
      STATUS_UNKNOWN = 5,
    }
  }

  public class DebugConsoleResponse
  {
    public int Type { get; set; }

    public string Response { get; set; }

    public DebugConsoleResponse()
    {
      this.Response = string.Empty;
    }
  }

  public class RecruitInfo
  {
    public ulong ID { get; set; }

    public BnetAccountId RecruitID { get; set; }

    public string Nickname { get; set; }

    public int Status { get; set; }

    public int Level { get; set; }

    public ulong CreationTimeMicrosec { get; set; }

    public RecruitInfo()
    {
      this.ID = 0UL;
      this.RecruitID = new BnetAccountId();
      this.Nickname = string.Empty;
      this.Status = 0;
      this.Level = 0;
    }

    public override string ToString()
    {
      return string.Format("[RecruitInfo: ID={0}, RecruitID={1}, Nickname={2}, Status={3}, Level={4}]", (object) this.ID, (object) this.RecruitID, (object) this.Nickname, (object) this.Status, (object) this.Level);
    }
  }

  private class HSClientInterface : ClientInterface
  {
    private string s_tempCachePath = Application.temporaryCachePath;

    public string GetVersion()
    {
      return Network.GetVersion();
    }

    public bool IsVersionInt()
    {
      return Network.IsVersionInt();
    }

    public string GetUserAgent()
    {
      string str1 = "Hearthstone/" + "7.0." + (object) 15590 + " (";
      string str2 = (PlatformSettings.OS != OSCategory.iOS ? (PlatformSettings.OS != OSCategory.Android ? (PlatformSettings.OS != OSCategory.PC ? (PlatformSettings.OS != OSCategory.Mac ? str1 + "UNKNOWN;" : str1 + "Mac;") : str1 + "PC;") : str1 + "Android;") : str1 + "iOS;") + this.CleanUserAgentString(SystemInfo.deviceModel) + ";" + (object) SystemInfo.deviceType + ";" + this.CleanUserAgentString(SystemInfo.deviceUniqueIdentifier) + ";" + (object) SystemInfo.graphicsDeviceID + ";" + this.CleanUserAgentString(SystemInfo.graphicsDeviceName) + ";" + this.CleanUserAgentString(SystemInfo.graphicsDeviceVendor) + ";" + (object) SystemInfo.graphicsDeviceVendorID + ";" + this.CleanUserAgentString(SystemInfo.graphicsDeviceVersion) + ";" + (object) SystemInfo.graphicsMemorySize + ";" + (object) SystemInfo.graphicsShaderLevel + ";" + (object) SystemInfo.npotSupport + ";" + this.CleanUserAgentString(SystemInfo.operatingSystem) + ";" + (object) SystemInfo.processorCount + ";" + this.CleanUserAgentString(SystemInfo.processorType) + ";" + (object) SystemInfo.supportedRenderTargetCount + ";" + (object) SystemInfo.supports3DTextures + ";" + (object) SystemInfo.supportsAccelerometer + ";" + (object) SystemInfo.supportsComputeShaders + ";" + (object) SystemInfo.supportsGyroscope + ";" + (object) SystemInfo.supportsImageEffects + ";" + (object) SystemInfo.supportsInstancing + ";" + (object) SystemInfo.supportsLocationService + ";" + (object) SystemInfo.supportsRenderTextures + ";" + (object) SystemInfo.supportsRenderToCubemap + ";" + (object) SystemInfo.supportsShadows + ";" + (object) SystemInfo.supportsSparseTextures + ";" + (object) SystemInfo.supportsStencil + ";" + (object) SystemInfo.supportsVibration + ";" + (object) SystemInfo.systemMemorySize + ";" + (object) SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf) + ";" + (object) SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGB4444) + ";" + (object) SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.Depth) + ";" + (object) SystemInfo.graphicsDeviceVersion.StartsWith("Metal") + ";" + (object) Screen.currentResolution.width + ";" + (object) Screen.currentResolution.height + ";" + (object) Screen.dpi + ";";
      string format = (!PlatformSettings.IsMobile() ? (object) (str2 + "Desktop;") : (!(bool) UniversalInputManager.UsePhoneUI ? (object) (str2 + "Tablet;") : (object) (str2 + "Phone;"))).ToString() + (object) Application.genuine + ") Battle.net/CSharp";
      Log.Kyle.Print(format);
      return format;
    }

    public int GetApplicationVersion()
    {
      return 15590;
    }

    private string CleanUserAgentString(string data)
    {
      return Regex.Replace(data, "[^a-zA-Z0-9_.]+", "_");
    }

    public string GetBasePersistentDataPath()
    {
      return FileUtils.PersistentDataPath;
    }

    public string GetTemporaryCachePath()
    {
      return this.s_tempCachePath;
    }

    public bool GetDisableConnectionMetering()
    {
      return Vars.Key("Aurora.DisableConnectionMetering").GetBool(false);
    }

    public constants.MobileEnv GetMobileEnvironment()
    {
      return ApplicationMgr.GetMobileEnvironment() == MobileEnv.PRODUCTION ? constants.MobileEnv.PRODUCTION : constants.MobileEnv.DEVELOPMENT;
    }

    public string GetAuroraVersionName()
    {
      return 15590.ToString();
    }

    public string GetLocaleName()
    {
      return Localization.GetLocaleName();
    }

    public string GetPlatformName()
    {
      return "Win";
    }

    public constants.RuntimeEnvironment GetRuntimeEnvironment()
    {
      return constants.RuntimeEnvironment.Mono;
    }

    public IUrlDownloader GetUrlDownloader()
    {
      return (IUrlDownloader) Network.s_urlDownloader;
    }
  }

  public delegate bool BnetErrorCallback(BnetErrorInfo info, object userData);

  public delegate void NetHandler();

  public delegate void QueueInfoHandler(Network.QueueInfo queueInfo);

  public delegate void GameQueueHandler(QueueEvent queueEvent);

  public delegate void ThrottledPacketListener(int packetID, long retryMillis);

  public delegate void TimeoutHandler(int pendingResponseId, int requestId, int requestSubId);

  public delegate void BnetEventHandler(BattleNet.BnetEvent[] updates);

  public delegate void FriendsHandler(FriendsUpdate[] updates);

  public delegate void WhisperHandler(BnetWhisper[] whispers);

  public delegate void PartyHandler(PartyEvent[] updates);

  public delegate void PresenceHandler(PresenceUpdate[] updates);

  public delegate void ShutdownHandler(int minutes);

  public delegate void ChallengeHandler(ChallengeInfo[] challenges);

  public delegate void SpectatorInviteReceivedHandler(Invite invite);

  public delegate void GameServerDisconnectEvent(BattleNetErrors errorCode);
}
