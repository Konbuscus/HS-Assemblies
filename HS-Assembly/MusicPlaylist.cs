﻿// Decompiled with JetBrains decompiler
// Type: MusicPlaylist
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[Serializable]
public class MusicPlaylist
{
  [CustomEditField(ListTable = true)]
  public List<MusicTrack> m_tracks = new List<MusicTrack>();
  [CustomEditField(ListSortable = true)]
  public MusicPlaylistType m_type;

  public List<MusicTrack> GetMusicTracks()
  {
    return this.GetRandomizedTracks(this.m_tracks, MusicTrackType.Music);
  }

  public List<MusicTrack> GetAmbienceTracks()
  {
    return this.GetRandomizedTracks(this.m_tracks, MusicTrackType.Ambience);
  }

  private List<MusicTrack> GetRandomizedTracks(List<MusicTrack> trackList, MusicTrackType type)
  {
    List<MusicTrack> musicTrackList1 = new List<MusicTrack>();
    List<MusicTrack> musicTrackList2 = new List<MusicTrack>();
    using (List<MusicTrack>.Enumerator enumerator = trackList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MusicTrack current = enumerator.Current;
        if (type == current.m_trackType && !string.IsNullOrEmpty(current.m_name))
        {
          if (current.m_shuffle)
            musicTrackList2.Add(current.Clone());
          else
            musicTrackList1.Add(current.Clone());
        }
      }
    }
    Random random = new Random();
    while (musicTrackList2.Count > 0)
    {
      int index = random.Next(0, musicTrackList2.Count);
      musicTrackList1.Add(musicTrackList2[index]);
      musicTrackList2.RemoveAt(index);
    }
    return musicTrackList1;
  }
}
