﻿// Decompiled with JetBrains decompiler
// Type: LocalizedTexture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LocalizedTexture : MonoBehaviour
{
  public string m_textureName;

  private void Awake()
  {
    AssetLoader.Get().LoadTexture(this.m_textureName, new AssetLoader.ObjectCallback(this.OnTextureLoaded), (object) null, false);
  }

  private void OnTextureLoaded(string name, Object obj, object callbackData)
  {
    Texture texture = obj as Texture;
    if ((Object) texture == (Object) null)
      Debug.LogError((object) "Failed to load LocalizedTexture m_textureName!");
    else
      this.GetComponent<Renderer>().material.mainTexture = texture;
  }
}
