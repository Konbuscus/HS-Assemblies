﻿// Decompiled with JetBrains decompiler
// Type: Bolvar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

public class Bolvar : SuperSpell
{
  public SpellValueRange[] m_atkPrefabs;

  protected override void OnAction(SpellStateType prevStateType)
  {
    ++this.m_effectsPendingFinish;
    base.OnAction(prevStateType);
    Card sourceCard = this.GetSourceCard();
    Spell spell = this.CloneSpell(this.DetermineRangePrefab(sourceCard.GetEntity().GetATK()));
    spell.SetSource(sourceCard.gameObject);
    spell.Activate();
    --this.m_effectsPendingFinish;
    this.FinishIfPossible();
  }

  private Spell DetermineRangePrefab(int atk)
  {
    SpellValueRange accordingToRanges = SpellUtils.GetAppropriateElementAccordingToRanges<SpellValueRange>(this.m_atkPrefabs, (Func<SpellValueRange, ValueRange>) (x => x.m_range), atk);
    if (accordingToRanges != null)
      return accordingToRanges.m_spellPrefab;
    return (Spell) null;
  }
}
