﻿// Decompiled with JetBrains decompiler
// Type: PolarityShift
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PolarityShift : SuperSpell
{
  public float m_ParticleHeightOffset = 0.1f;
  public float m_CleanupTime = 2f;
  public float m_SpellFinishTime = 2f;
  public AnimationCurve m_HeightCurve;
  public float m_RotationDriftAmount;
  public AnimationCurve m_RotationDriftCurve;
  public ParticleSystem m_GlowParticle;
  public ParticleSystem m_LightningParticle;
  public ParticleSystem m_ImpactParticle;
  public ParticleEffects m_ParticleEffects;
  private float m_HeightCurveLength;
  private float m_AnimTime;
  private AudioSource m_Sound;

  protected override void Awake()
  {
    this.m_Sound = this.GetComponent<AudioSource>();
    base.Awake();
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    if (this.m_HeightCurve.length == 0)
    {
      UnityEngine.Debug.LogWarning((object) "PolarityShift Spell height animation curve in not defined");
      base.OnAction(prevStateType);
    }
    else if (this.m_RotationDriftCurve.length == 0)
    {
      UnityEngine.Debug.LogWarning((object) "PolarityShift Spell rotation drift animation curve in not defined");
      base.OnAction(prevStateType);
    }
    else
    {
      ++this.m_effectsPendingFinish;
      base.OnAction(prevStateType);
      this.m_HeightCurveLength = this.m_HeightCurve.keys[this.m_HeightCurve.length - 1].time;
      this.m_ParticleEffects.m_ParticleSystems.Clear();
      List<PolarityShift.MinionData> minions = new List<PolarityShift.MinionData>();
      using (List<GameObject>.Enumerator enumerator = this.GetTargets().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GameObject current = enumerator.Current;
          PolarityShift.MinionData minionData = new PolarityShift.MinionData();
          minionData.gameObject = current;
          minionData.orgLocPos = current.transform.localPosition;
          minionData.orgLocRot = current.transform.localRotation;
          float x = Mathf.Lerp(-this.m_RotationDriftAmount, this.m_RotationDriftAmount, Random.value);
          float y = Mathf.Lerp(-this.m_RotationDriftAmount, this.m_RotationDriftAmount, Random.value) * 0.1f;
          float z = Mathf.Lerp(-this.m_RotationDriftAmount, this.m_RotationDriftAmount, Random.value);
          minionData.rotationDrift = new Vector3(x, y, z);
          minionData.glowParticle = Object.Instantiate<ParticleSystem>(this.m_GlowParticle);
          minionData.glowParticle.transform.position = current.transform.position;
          minionData.glowParticle.transform.Translate(0.0f, this.m_ParticleHeightOffset, 0.0f, Space.World);
          minionData.lightningParticle = Object.Instantiate<ParticleSystem>(this.m_LightningParticle);
          minionData.lightningParticle.transform.position = current.transform.position;
          minionData.lightningParticle.transform.Translate(0.0f, this.m_ParticleHeightOffset, 0.0f, Space.World);
          minionData.impactParticle = Object.Instantiate<ParticleSystem>(this.m_ImpactParticle);
          minionData.impactParticle.transform.position = current.transform.position;
          minionData.impactParticle.transform.Translate(0.0f, this.m_ParticleHeightOffset, 0.0f, Space.World);
          this.m_ParticleEffects.m_ParticleSystems.Add(minionData.lightningParticle);
          if ((Object) this.m_Sound != (Object) null)
            SoundManager.Get().Play(this.m_Sound, true);
          minions.Add(minionData);
        }
      }
      this.StartCoroutine(this.DoSpellFinished());
      this.StartCoroutine(this.MinionAnimation(minions));
    }
  }

  [DebuggerHidden]
  private IEnumerator DoSpellFinished()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PolarityShift.\u003CDoSpellFinished\u003Ec__Iterator2C9() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator MinionAnimation(List<PolarityShift.MinionData> minions)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PolarityShift.\u003CMinionAnimation\u003Ec__Iterator2CA() { minions = minions, \u003C\u0024\u003Eminions = minions, \u003C\u003Ef__this = this };
  }

  private void ShakeCamera()
  {
    CameraShakeMgr.Shake(Camera.main, new Vector3(0.1f, 0.1f, 0.1f), 0.75f);
  }

  public class MinionData
  {
    public GameObject gameObject;
    public Vector3 orgLocPos;
    public Quaternion orgLocRot;
    public Vector3 rotationDrift;
    public ParticleSystem glowParticle;
    public ParticleSystem lightningParticle;
    public ParticleSystem impactParticle;
  }
}
