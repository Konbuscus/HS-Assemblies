﻿// Decompiled with JetBrains decompiler
// Type: CollectionPageManagerTouchBehavior
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CollectionPageManagerTouchBehavior : PegUICustomBehavior
{
  private float TurnDist = 0.07f;
  private PegUIElement pageLeftRegion;
  private PegUIElement pageRightRegion;
  private PegUIElement pageDragRegion;
  private CollectionPageManagerTouchBehavior.SwipeState swipeState;
  private Vector2 m_swipeStartPosition;

  protected override void Awake()
  {
    base.Awake();
    CollectionPageManager component = this.GetComponent<CollectionPageManager>();
    this.pageLeftRegion = component.m_pageLeftClickableRegion;
    this.pageRightRegion = component.m_pageRightClickableRegion;
    this.pageDragRegion = component.m_pageDraggableRegion;
    this.pageDragRegion.gameObject.SetActive(true);
    this.pageDragRegion.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(this.OnPageDraggableRegionDown));
  }

  protected override void OnDestroy()
  {
    this.pageDragRegion.gameObject.SetActive(false);
    this.pageDragRegion.RemoveEventListener(UIEventType.PRESS, new UIEvent.Handler(this.OnPageDraggableRegionDown));
    base.OnDestroy();
  }

  public override bool UpdateUI()
  {
    if (CollectionInputMgr.Get().HasHeldCard() || (Object) CraftingManager.Get() != (Object) null && CraftingManager.Get().IsCardShowing())
      return false;
    bool flag = false;
    if (UniversalInputManager.Get().GetMouseButtonUp(0))
    {
      flag = this.swipeState == CollectionPageManagerTouchBehavior.SwipeState.Success;
      this.swipeState = CollectionPageManagerTouchBehavior.SwipeState.None;
    }
    if (this.swipeState != CollectionPageManagerTouchBehavior.SwipeState.None)
      return true;
    return flag;
  }

  private void OnPageDraggableRegionDown(UIEvent e)
  {
    if ((Object) this.gameObject == (Object) null)
      return;
    this.TryStartPageTurnGesture();
  }

  private void TryStartPageTurnGesture()
  {
    if (this.swipeState == CollectionPageManagerTouchBehavior.SwipeState.Update)
      return;
    this.StartCoroutine(this.HandlePageTurnGesture());
  }

  private Vector2 GetTouchPosition()
  {
    Vector3 touchPosition = W8Touch.Get().GetTouchPosition();
    return new Vector2(touchPosition.x, touchPosition.y);
  }

  [DebuggerHidden]
  private IEnumerator HandlePageTurnGesture()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionPageManagerTouchBehavior.\u003CHandlePageTurnGesture\u003Ec__Iterator49()
    {
      \u003C\u003Ef__this = this
    };
  }

  private PegUIElement HitTestPageTurnRegions()
  {
    PegUIElement pegUiElement = (PegUIElement) null;
    this.pageDragRegion.GetComponent<Collider>().enabled = false;
    RaycastHit hitInfo;
    if (UniversalInputManager.Get().GetInputHitInfo(out hitInfo))
    {
      pegUiElement = hitInfo.collider.GetComponent<PegUIElement>();
      if ((Object) pegUiElement != (Object) this.pageLeftRegion && (Object) pegUiElement != (Object) this.pageRightRegion)
        pegUiElement = (PegUIElement) null;
    }
    this.pageDragRegion.GetComponent<Collider>().enabled = true;
    return pegUiElement;
  }

  private enum SwipeState
  {
    None,
    Update,
    Success,
  }
}
