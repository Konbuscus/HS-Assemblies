﻿// Decompiled with JetBrains decompiler
// Type: UberText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
[CustomEditClass]
public class UberText : MonoBehaviour
{
  private static bool s_InlineImageTextureLoaded = false;
  private static float s_offset = -3000f;
  private static RenderTextureFormat s_TextureFormat = RenderTextureFormat.DefaultHDR;
  private static bool s_disableCache = false;
  private static Map<int, UberText.CachedTextValues> s_CachedText = new Map<int, UberText.CachedTextValues>();
  private static Map<Font, int> s_TexelUpdateFrame = new Map<Font, int>();
  private static Map<Font, Vector2> s_TexelUpdateData = new Map<Font, Vector2>();
  private static int RENDER_LAYER = 28;
  private static int RENDER_LAYER_BIT = GameLayer.InvisibleRender.LayerBit();
  private static readonly char[] STRIP_CHARS_INDEX_OF_ANY = new char[7]{ '<', '[', '\\', ' ', '\t', '\r', '\n' };
  private readonly string TEXT_SHADER_NAME = "Hero/Text_Unlit";
  private readonly string PLANE_SHADER_NAME = "Hidden/TextPlane";
  private readonly string BOLD_SHADER_NAME = "Hidden/Text_Bold";
  private readonly string BOLD_OUTLINE_TEXT_SHADER_NAME = "Hidden/TextBoldOutline_Unlit";
  private readonly string OUTLINE_TEXT_SHADER_NAME = "Hidden/TextOutline_Unlit";
  private readonly string OUTLINE_TEXT_2PASS_SHADER_NAME = "Hidden/TextOutline_Unlit_2pass";
  private readonly string OUTLINE_NO_VERT_COLOR_TEXT_SHADER_NAME = "Hidden/TextOutline_Unlit_NoVertColor";
  private readonly string OUTLINE_NO_VERT_COLOR_TEXT_2PASS_SHADER_NAME = "Hidden/TextOutline_Unlit_NoVertColor_2pass";
  private readonly string TEXT_ANTIALAISING_SHADER_NAME = "Hidden/TextAntialiasing";
  private readonly string INLINE_IMAGE_SHADER_NAME = "Hero/Unlit_Transparent";
  private readonly string SHADOW_SHADER_NAME = "Hidden/TextShadow";
  [SerializeField]
  private string m_Text = "Uber Text";
  [SerializeField]
  private float m_Width = 1f;
  [SerializeField]
  private float m_Height = 1f;
  [SerializeField]
  private float m_LineSpacing = 1f;
  [SerializeField]
  private int m_FontSize = 35;
  [SerializeField]
  private int m_MinFontSize = 10;
  [SerializeField]
  private float m_CharacterSize = 5f;
  [SerializeField]
  private float m_MinCharacterSize = 1f;
  [SerializeField]
  private bool m_RichText = true;
  [SerializeField]
  private Color m_TextColor = Color.white;
  [SerializeField]
  private float m_UnderwearWidth = 0.2f;
  [SerializeField]
  private float m_UnderwearHeight = 0.2f;
  [SerializeField]
  private UberText.AlignmentOptions m_Alignment = UberText.AlignmentOptions.Center;
  [SerializeField]
  private UberText.AnchorOptions m_Anchor = UberText.AnchorOptions.Middle;
  [SerializeField]
  private int m_Resolution = 256;
  [SerializeField]
  private float m_OutlineSize = 1f;
  [SerializeField]
  private Color m_OutlineColor = Color.black;
  [SerializeField]
  private float m_AntiAliasAmount = 0.5f;
  [SerializeField]
  private float m_AntiAliasEdge = 0.5f;
  [SerializeField]
  private float m_ShadowOffset = 1f;
  [SerializeField]
  private Color m_ShadowColor = new Color(0.1f, 0.1f, 0.1f, 0.333f);
  [SerializeField]
  private float m_ShadowBlur = 1.5f;
  [SerializeField]
  private int m_ShadowRenderQueueOffset = -1;
  [SerializeField]
  private Color m_GradientUpperColor = Color.white;
  [SerializeField]
  private Color m_GradientLowerColor = Color.white;
  [SerializeField]
  private bool m_Cache = true;
  private float m_LineSpaceModifier = 1f;
  private float m_FontSizeModifier = 1f;
  private float m_CharacterSizeModifier = 1f;
  private float m_UnboundCharacterSizeModifier = 1f;
  private float m_OutlineModifier = 1f;
  private string m_PreviousText = string.Empty;
  private Map<UberText.TextRenderMaterial, int> m_TextMaterialIndices = new Map<UberText.TextRenderMaterial, int>();
  private int m_PreviousResolution = 256;
  private int m_OrgRenderQueue = -9999;
  private readonly Vector2[] PLANE_UVS = new Vector2[4]{ new Vector2(0.0f, 0.0f), new Vector2(1f, 0.0f), new Vector2(0.0f, 1f), new Vector2(1f, 1f) };
  private readonly Vector3[] PLANE_NORMALS = new Vector3[4]{ Vector3.up, Vector3.up, Vector3.up, Vector3.up };
  private readonly int[] PLANE_TRIANGLES = new int[6]{ 3, 1, 2, 2, 1, 0 };
  private const int CACHE_FILE_VERSION_TEMP = 2;
  private const int CACHE_FILE_MAX_SIZE = 50000;
  private const string FONT_NAME_BLIZZARD_GLOBAL = "BlizzardGlobal";
  private const string FONT_NAME_BELWE_OUTLINE = "Belwe_Outline";
  private const string FONT_NAME_BELWE = "Belwe";
  private const string FONT_NAME_FRANKLIN_GOTHIC = "FranklinGothic";
  private const float CHARACTER_SIZE_SCALE = 0.01f;
  private const float BOLD_MAX_SIZE = 10f;
  private const int MAX_REDUCE_TEXT_COUNT = 40;
  [SerializeField]
  private bool m_GameStringLookup;
  [SerializeField]
  private bool m_UseEditorText;
  [SerializeField]
  private Font m_Font;
  [SerializeField]
  private float m_BoldSize;
  [SerializeField]
  private bool m_WordWrap;
  [SerializeField]
  private bool m_ForceWrapLargeWords;
  [SerializeField]
  private bool m_ResizeToFit;
  [SerializeField]
  private bool m_Underwear;
  [SerializeField]
  private bool m_UnderwearFlip;
  [SerializeField]
  private bool m_RenderToTexture;
  [SerializeField]
  private GameObject m_RenderOnObject;
  [SerializeField]
  private bool m_Outline;
  [SerializeField]
  private bool m_AntiAlias;
  [SerializeField]
  private bool m_Shadow;
  [SerializeField]
  private float m_ShadowDepthOffset;
  [SerializeField]
  private int m_RenderQueue;
  [SerializeField]
  private float m_AmbientLightBlend;
  [SerializeField]
  private UberText.LocalizationSettings m_LocalizedSettings;
  private bool m_isFontDefLoaded;
  private Font m_LocalizedFont;
  private Texture m_FontTexture;
  private float m_SingleLineAdjustment;
  private float m_WorldWidth;
  private float m_WorldHeight;
  private bool m_updated;
  private string[] m_Words;
  private TextMesh m_TextMesh;
  private GameObject m_TextMeshGameObject;
  private Material TextMeshBaseMaterial;
  private RenderTexture m_TextTexture;
  private Camera m_Camera;
  private GameObject m_CameraGO;
  private Mesh m_PlaneMesh;
  private GameObject m_PlaneGameObject;
  private float m_PreviousPlaneWidth;
  private float m_PreviousPlaneHeight;
  private int m_LineCount;
  private GameObject m_ShadowPlaneGameObject;
  private Vector2 m_PreviousTexelSize;
  private bool m_Ellipsized;
  private int m_CacheHash;
  private bool m_Hidden;
  private bool m_TextSet;
  private Bounds m_UnderwearLeftBounds;
  private Bounds m_UnderwearRightBounds;
  private static Texture2D s_InlineImageTexture;
  private float m_Offset;
  private Shader m_TextShader;
  private Material m_TextMaterial;
  private Shader m_PlaneShader;
  private Material m_PlaneMaterial;
  private Shader m_BoldShader;
  private Shader m_BoldOutlineShader;
  private Material m_BoldMaterial;
  private Shader m_OutlineTextShader;
  private Material m_OutlineTextMaterial;
  private Shader m_AntialiasingTextShader;
  private Material m_TextAntialiasingMaterial;
  private Shader m_ShadowTextShader;
  private Material m_ShadowMaterial;
  private Shader m_InlineImageShader;
  private Material m_InlineImageMaterial;

  protected float Offset
  {
    get
    {
      if ((double) this.m_Offset == 0.0)
      {
        UberText.s_offset -= 100f;
        this.m_Offset = UberText.s_offset;
      }
      return this.m_Offset;
    }
  }

  protected Material TextMaterial
  {
    get
    {
      if ((UnityEngine.Object) this.m_TextMaterial == (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.m_TextShader == (UnityEngine.Object) null)
        {
          this.m_TextShader = ShaderUtils.FindShader(this.TEXT_SHADER_NAME);
          if (!(bool) ((UnityEngine.Object) this.m_TextShader))
            Debug.LogError((object) ("UberText Failed to load Shader: " + this.TEXT_SHADER_NAME));
        }
        this.m_TextMaterial = new Material(this.m_TextShader);
        SceneUtils.SetHideFlags((UnityEngine.Object) this.m_TextMaterial, HideFlags.DontSave);
      }
      return this.m_TextMaterial;
    }
  }

  protected Material PlaneMaterial
  {
    get
    {
      if ((UnityEngine.Object) this.m_PlaneMaterial == (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.m_PlaneShader == (UnityEngine.Object) null)
        {
          this.m_PlaneShader = ShaderUtils.FindShader(this.PLANE_SHADER_NAME);
          if (!(bool) ((UnityEngine.Object) this.m_PlaneShader))
            Debug.LogError((object) ("UberText Failed to load Shader: " + this.PLANE_SHADER_NAME));
        }
        this.m_PlaneMaterial = new Material(this.m_PlaneShader);
        SceneUtils.SetHideFlags((UnityEngine.Object) this.m_PlaneMaterial, HideFlags.DontSave);
      }
      return this.m_PlaneMaterial;
    }
  }

  protected Material BoldMaterial
  {
    get
    {
      if ((UnityEngine.Object) this.m_BoldMaterial == (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.m_BoldShader == (UnityEngine.Object) null)
        {
          this.m_BoldShader = ShaderUtils.FindShader(this.BOLD_SHADER_NAME);
          if (!(bool) ((UnityEngine.Object) this.m_BoldShader))
            Debug.LogError((object) ("UberText Failed to load Shader: " + this.BOLD_SHADER_NAME));
        }
        this.m_BoldMaterial = new Material(this.m_BoldShader);
        SceneUtils.SetHideFlags((UnityEngine.Object) this.m_BoldMaterial, HideFlags.DontSave);
      }
      return this.m_BoldMaterial;
    }
  }

  protected Material OutlineTextMaterial
  {
    get
    {
      string name = this.OUTLINE_TEXT_SHADER_NAME;
      if (Localization.GetLocale() == Locale.thTH)
        name = this.OUTLINE_TEXT_2PASS_SHADER_NAME;
      if (!this.m_RichText)
        name = Localization.GetLocale() != Locale.thTH ? this.OUTLINE_NO_VERT_COLOR_TEXT_SHADER_NAME : this.OUTLINE_NO_VERT_COLOR_TEXT_2PASS_SHADER_NAME;
      if ((UnityEngine.Object) this.m_OutlineTextMaterial != (UnityEngine.Object) null && name != this.m_OutlineTextMaterial.shader.name)
      {
        this.m_OutlineTextShader = (Shader) null;
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_OutlineTextMaterial);
        this.m_OutlineTextMaterial = (Material) null;
      }
      if ((UnityEngine.Object) this.m_OutlineTextMaterial == (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.m_OutlineTextShader == (UnityEngine.Object) null)
        {
          this.m_OutlineTextShader = ShaderUtils.FindShader(name);
          if (!(bool) ((UnityEngine.Object) this.m_OutlineTextShader))
            Debug.LogError((object) ("UberText Failed to load Shader: " + name));
        }
        this.m_OutlineTextMaterial = new Material(this.m_OutlineTextShader);
        SceneUtils.SetHideFlags((UnityEngine.Object) this.m_OutlineTextMaterial, HideFlags.DontSave);
      }
      return this.m_OutlineTextMaterial;
    }
  }

  protected Material TextAntialiasingMaterial
  {
    get
    {
      if ((UnityEngine.Object) this.m_TextAntialiasingMaterial == (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.m_AntialiasingTextShader == (UnityEngine.Object) null)
        {
          this.m_AntialiasingTextShader = ShaderUtils.FindShader(this.TEXT_ANTIALAISING_SHADER_NAME);
          if (!(bool) ((UnityEngine.Object) this.m_AntialiasingTextShader))
            Debug.LogError((object) ("UberText Failed to load Shader: " + this.TEXT_ANTIALAISING_SHADER_NAME));
        }
        this.m_TextAntialiasingMaterial = new Material(this.m_AntialiasingTextShader);
        SceneUtils.SetHideFlags((UnityEngine.Object) this.m_TextAntialiasingMaterial, HideFlags.DontSave);
      }
      return this.m_TextAntialiasingMaterial;
    }
  }

  protected Material ShadowMaterial
  {
    get
    {
      if ((UnityEngine.Object) this.m_ShadowMaterial == (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.m_ShadowTextShader == (UnityEngine.Object) null)
        {
          this.m_ShadowTextShader = ShaderUtils.FindShader(this.SHADOW_SHADER_NAME);
          if (!(bool) ((UnityEngine.Object) this.m_ShadowTextShader))
            Debug.LogError((object) ("UberText Failed to load Shader: " + this.SHADOW_SHADER_NAME));
        }
        this.m_ShadowMaterial = new Material(this.m_ShadowTextShader);
        SceneUtils.SetHideFlags((UnityEngine.Object) this.m_ShadowMaterial, HideFlags.DontSave);
      }
      return this.m_ShadowMaterial;
    }
  }

  protected Material InlineImageMaterial
  {
    get
    {
      if ((UnityEngine.Object) this.m_InlineImageMaterial == (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.m_InlineImageShader == (UnityEngine.Object) null)
        {
          this.m_InlineImageShader = ShaderUtils.FindShader(this.INLINE_IMAGE_SHADER_NAME);
          if (!(bool) ((UnityEngine.Object) this.m_InlineImageShader))
            Debug.LogError((object) ("UberText Failed to load Shader: " + this.INLINE_IMAGE_SHADER_NAME));
        }
        this.m_InlineImageMaterial = new Material(this.m_InlineImageShader);
      }
      return this.m_InlineImageMaterial;
    }
  }

  [CustomEditField(Sections = "Text", T = EditType.TEXT_AREA)]
  public string Text
  {
    get
    {
      return this.m_Text;
    }
    set
    {
      this.m_TextSet = true;
      this.m_TextSet = true;
      if (value == this.m_Text)
        return;
      this.m_Text = value ?? string.Empty;
      if (this.m_Text.Any<char>((Func<char, bool>) (c => char.IsSurrogate(c))))
        this.m_Text = new string(this.m_Text.Where<char>((Func<char, bool>) (c => !char.IsLowSurrogate(c))).Select<char, char>((Func<char, char>) (c =>
        {
          if (char.IsHighSurrogate(c))
            return '�';
          return c;
        })).ToArray<char>());
      if (this.m_Text == this.m_PreviousText)
        return;
      this.UpdateNow();
    }
  }

  [CustomEditField(Sections = "Text")]
  public bool GameStringLookup
  {
    get
    {
      return this.m_GameStringLookup;
    }
    set
    {
      if (value == this.m_GameStringLookup)
        return;
      this.m_GameStringLookup = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Text")]
  public bool UseEditorText
  {
    get
    {
      return this.m_UseEditorText;
    }
    set
    {
      if (value == this.m_UseEditorText)
        return;
      this.m_UseEditorText = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Text")]
  public bool Cache
  {
    get
    {
      return this.m_Cache;
    }
    set
    {
      this.m_Cache = value;
    }
  }

  [CustomEditField(Sections = "Size")]
  public float Width
  {
    get
    {
      return this.m_Width;
    }
    set
    {
      if ((double) value == (double) this.m_Width)
        return;
      this.m_Width = value;
      if ((double) this.m_Width < 0.00999999977648258)
        this.m_Width = 0.01f;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Size")]
  public float Height
  {
    get
    {
      return this.m_Height;
    }
    set
    {
      if ((double) value == (double) this.m_Height)
        return;
      this.m_Height = value;
      if ((double) this.m_Height < 0.00999999977648258)
        this.m_Height = 0.01f;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Size")]
  public float LineSpacing
  {
    get
    {
      return this.m_LineSpacing;
    }
    set
    {
      if ((double) value == (double) this.m_LineSpacing)
        return;
      this.m_LineSpacing = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Style")]
  public Font TrueTypeFont
  {
    get
    {
      return this.m_Font;
    }
    set
    {
      if ((UnityEngine.Object) value == (UnityEngine.Object) this.m_Font)
        return;
      this.m_Font = value;
      this.SetFont(this.m_Font);
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Style")]
  public int FontSize
  {
    get
    {
      return this.m_FontSize;
    }
    set
    {
      if (value == this.m_FontSize)
        return;
      this.m_FontSize = value;
      if (this.m_FontSize < 1)
        this.m_FontSize = 1;
      if (this.m_FontSize > 120)
        this.m_FontSize = 120;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Style")]
  public int MinFontSize
  {
    get
    {
      return this.m_MinFontSize;
    }
    set
    {
      if (value == this.m_MinFontSize)
        return;
      this.m_MinFontSize = value;
      if (this.m_MinFontSize < 1)
        this.m_MinFontSize = 1;
      if (this.m_MinFontSize > this.m_FontSize)
        this.m_MinFontSize = this.m_FontSize;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Style")]
  public float CharacterSize
  {
    get
    {
      return this.m_CharacterSize;
    }
    set
    {
      if ((double) value == (double) this.m_CharacterSize)
        return;
      this.m_CharacterSize = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Style")]
  public float MinCharacterSize
  {
    get
    {
      return this.m_MinCharacterSize;
    }
    set
    {
      if ((double) value == (double) this.m_MinCharacterSize)
        return;
      this.m_MinCharacterSize = value;
      if ((double) this.m_MinCharacterSize < 1.0 / 1000.0)
        this.m_MinCharacterSize = 1f / 1000f;
      if ((double) this.m_MinCharacterSize > (double) this.m_CharacterSize)
        this.m_MinCharacterSize = this.m_CharacterSize;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Style")]
  public bool RichText
  {
    get
    {
      return this.m_RichText;
    }
    set
    {
      if (value == this.m_RichText)
        return;
      this.m_RichText = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Style")]
  public Color TextColor
  {
    get
    {
      return this.m_TextColor;
    }
    set
    {
      if (value == this.m_TextColor)
        return;
      this.m_TextColor = value;
      this.UpdateColor();
    }
  }

  [CustomEditField(Hide = true)]
  public float TextAlpha
  {
    get
    {
      return this.m_TextColor.a;
    }
    set
    {
      if ((double) value == (double) this.m_TextColor.a)
        return;
      this.m_TextColor.a = value;
      this.UpdateColor();
    }
  }

  [CustomEditField(Sections = "Style")]
  public float BoldSize
  {
    get
    {
      return this.m_BoldSize;
    }
    set
    {
      if ((double) value == (double) this.m_BoldSize)
        return;
      this.m_BoldSize = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Paragraph")]
  public bool WordWrap
  {
    get
    {
      return this.m_WordWrap;
    }
    set
    {
      if (value == this.m_WordWrap)
        return;
      this.m_WordWrap = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Paragraph")]
  public bool ForceWrapLargeWords
  {
    get
    {
      return this.m_ForceWrapLargeWords;
    }
    set
    {
      if (value == this.m_ForceWrapLargeWords)
        return;
      this.m_ForceWrapLargeWords = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Paragraph")]
  public bool ResizeToFit
  {
    get
    {
      return this.m_ResizeToFit;
    }
    set
    {
      if (value == this.m_ResizeToFit)
        return;
      this.m_ResizeToFit = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Label = "Enable", Sections = "Underwear")]
  public bool Underwear
  {
    get
    {
      return this.m_Underwear;
    }
    set
    {
      if (value == this.m_Underwear)
        return;
      this.m_Underwear = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Label = "Flip", Parent = "Underwear")]
  public bool UnderwearFlip
  {
    get
    {
      return this.m_UnderwearFlip;
    }
    set
    {
      if (value == this.m_UnderwearFlip)
        return;
      this.m_UnderwearFlip = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Label = "Width", Parent = "Underwear")]
  public float UnderwearWidth
  {
    get
    {
      return this.m_UnderwearWidth;
    }
    set
    {
      if ((double) value == (double) this.m_UnderwearWidth)
        return;
      this.m_UnderwearWidth = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Label = "Height", Parent = "Underwear")]
  public float UnderwearHeight
  {
    get
    {
      return this.m_UnderwearHeight;
    }
    set
    {
      if ((double) value == (double) this.m_UnderwearHeight)
        return;
      this.m_UnderwearHeight = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Label = "Enable", Sections = "Alignment")]
  public UberText.AlignmentOptions Alignment
  {
    get
    {
      return this.m_Alignment;
    }
    set
    {
      if (value == this.m_Alignment)
        return;
      this.m_Alignment = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Parent = "Alignment")]
  public UberText.AnchorOptions Anchor
  {
    get
    {
      return this.m_Anchor;
    }
    set
    {
      if (value == this.m_Anchor)
        return;
      this.m_Anchor = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Render/Bake")]
  public bool RenderToTexture
  {
    get
    {
      return this.m_RenderToTexture;
    }
    set
    {
      if (value == this.m_RenderToTexture)
        return;
      this.m_RenderToTexture = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Render/Bake")]
  public GameObject RenderOnObject
  {
    get
    {
      return this.m_RenderOnObject;
    }
    set
    {
      if ((UnityEngine.Object) value == (UnityEngine.Object) this.m_RenderOnObject)
        return;
      this.m_RenderOnObject = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Parent = "RenderToTexture")]
  public int TextureResolution
  {
    get
    {
      return this.m_Resolution;
    }
    set
    {
      if (value == this.m_Resolution)
        return;
      this.m_Resolution = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Label = "Enable", Sections = "Outline")]
  public bool Outline
  {
    get
    {
      return this.m_Outline;
    }
    set
    {
      if (value == this.m_Outline)
        return;
      this.m_Outline = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Label = "Size", Parent = "Outline")]
  public float OutlineSize
  {
    get
    {
      return this.m_OutlineSize;
    }
    set
    {
      if ((double) value == (double) this.m_OutlineSize)
        return;
      this.m_OutlineSize = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Label = "Color", Parent = "Outline")]
  public Color OutlineColor
  {
    get
    {
      return this.m_OutlineColor;
    }
    set
    {
      if (value == this.m_OutlineColor)
        return;
      this.m_OutlineColor = value;
      this.UpdateColor();
    }
  }

  [CustomEditField(Hide = true)]
  public float OutlineAlpha
  {
    get
    {
      return this.m_OutlineColor.a;
    }
    set
    {
      if ((double) value == (double) this.m_OutlineColor.a)
        return;
      this.m_OutlineColor.a = value;
      this.UpdateColor();
    }
  }

  [CustomEditField(Parent = "RenderToTexture")]
  public bool AntiAlias
  {
    get
    {
      return this.m_AntiAlias;
    }
    set
    {
      if (value == this.m_AntiAlias)
        return;
      this.m_AntiAlias = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Parent = "AntiAlias")]
  public float AntiAliasAmount
  {
    get
    {
      return this.m_AntiAliasAmount;
    }
    set
    {
      if ((double) value == (double) this.m_AntiAliasAmount)
        return;
      this.m_AntiAliasAmount = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Localization")]
  public UberText.LocalizationSettings LocalizeSettings
  {
    get
    {
      return this.m_LocalizedSettings;
    }
    set
    {
      this.m_LocalizedSettings = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Parent = "AntiAlias")]
  public float AntiAliasEdge
  {
    get
    {
      return this.m_AntiAliasEdge;
    }
    set
    {
      if ((double) value == (double) this.m_AntiAliasEdge)
        return;
      this.m_AntiAliasEdge = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Label = "Enable", Sections = "Shadow")]
  public bool Shadow
  {
    get
    {
      return this.m_Shadow;
    }
    set
    {
      if (value == this.m_Shadow)
        return;
      this.m_Shadow = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Parent = "Shadow")]
  public float ShadowOffset
  {
    get
    {
      return this.m_ShadowOffset;
    }
    set
    {
      if ((double) value == (double) this.m_ShadowOffset)
        return;
      this.m_ShadowOffset = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Parent = "Shadow")]
  public float ShadowDepthOffset
  {
    get
    {
      return this.m_ShadowDepthOffset;
    }
    set
    {
      if ((double) value == (double) this.m_ShadowDepthOffset)
        return;
      this.m_ShadowDepthOffset = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Parent = "Shadow")]
  public float ShadowBlur
  {
    get
    {
      return this.m_ShadowBlur;
    }
    set
    {
      if ((double) value == (double) this.m_ShadowBlur)
        return;
      this.m_ShadowBlur = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Parent = "Shadow")]
  public Color ShadowColor
  {
    get
    {
      return this.m_ShadowColor;
    }
    set
    {
      if (value == this.m_ShadowColor)
        return;
      this.m_ShadowColor = value;
      this.UpdateColor();
    }
  }

  [CustomEditField(Parent = "Shadow")]
  public float ShadowAlpha
  {
    get
    {
      return this.m_ShadowColor.a;
    }
    set
    {
      if ((double) value == (double) this.m_ShadowColor.a)
        return;
      this.m_ShadowColor.a = value;
      this.UpdateColor();
    }
  }

  [CustomEditField(Parent = "Shadow")]
  public int ShadowRenderQueueOffset
  {
    get
    {
      return this.m_ShadowRenderQueueOffset;
    }
    set
    {
      if (value == this.m_ShadowRenderQueueOffset)
        return;
      this.m_ShadowRenderQueueOffset = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Render")]
  public int RenderQueue
  {
    get
    {
      return this.m_RenderQueue;
    }
    set
    {
      if (value == this.m_RenderQueue)
        return;
      this.m_RenderQueue = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Sections = "Render")]
  public float AmbientLightBlend
  {
    get
    {
      return this.m_AmbientLightBlend;
    }
    set
    {
      if ((double) value == (double) this.m_AmbientLightBlend)
        return;
      this.m_AmbientLightBlend = value;
      this.UpdateText();
    }
  }

  [CustomEditField(Parent = "RenderToTexture")]
  public Color GradientUpperColor
  {
    get
    {
      return this.m_GradientUpperColor;
    }
    set
    {
      if (value == this.m_GradientUpperColor)
        return;
      this.m_GradientUpperColor = value;
      this.UpdateColor();
    }
  }

  [CustomEditField(Hide = true)]
  public float GradientUpperAlpha
  {
    get
    {
      return this.m_GradientUpperColor.a;
    }
    set
    {
      if ((double) value == (double) this.m_GradientUpperColor.a)
        return;
      this.m_GradientUpperColor.a = value;
      this.UpdateColor();
    }
  }

  [CustomEditField(Parent = "RenderToTexture")]
  public Color GradientLowerColor
  {
    get
    {
      return this.m_GradientLowerColor;
    }
    set
    {
      if (value == this.m_GradientLowerColor)
        return;
      this.m_GradientLowerColor = value;
      this.UpdateColor();
    }
  }

  [CustomEditField(Hide = true)]
  public float GradientLowerAlpha
  {
    get
    {
      return this.m_GradientLowerColor.a;
    }
    set
    {
      if ((double) value == (double) this.m_GradientLowerColor.a)
        return;
      this.m_GradientLowerColor.a = value;
      this.UpdateColor();
    }
  }

  public static UberText[] EnableAllTextInObject(GameObject obj, bool enable)
  {
    UberText[] componentsInChildren = obj.GetComponentsInChildren<UberText>();
    UberText.EnableAllTextObjects(componentsInChildren, enable);
    return componentsInChildren;
  }

  public static void EnableAllTextObjects(UberText[] objs, bool enable)
  {
    foreach (Component component in objs)
      component.gameObject.SetActive(enable);
  }

  private void Awake()
  {
    if (!this.m_GameStringLookup && !this.m_TextSet && !this.m_UseEditorText)
      this.m_Text = string.Empty;
    this.FindSupportedTextureFormat();
    if (!(bool) ((UnityEngine.Object) UberText.s_InlineImageTexture))
    {
      UberText.s_InlineImageTexture = new Texture2D(2, 2, TextureFormat.ARGB32, false);
      UberText.s_InlineImageTexture.SetPixel(0, 0, Color.clear);
      UberText.s_InlineImageTexture.SetPixel(1, 0, Color.clear);
      UberText.s_InlineImageTexture.SetPixel(0, 1, Color.clear);
      UberText.s_InlineImageTexture.SetPixel(1, 1, Color.clear);
      UberText.s_InlineImageTexture.Apply();
    }
    this.DestroyChildren();
  }

  private void Start()
  {
    this.m_updated = false;
  }

  private void Update()
  {
    if (this.m_RenderToTexture && (bool) ((UnityEngine.Object) this.m_TextTexture) && !this.m_TextTexture.IsCreated())
    {
      Log.Kyle.Print("UberText Texture lost 1. UpdateText Called");
      this.m_updated = false;
      this.RenderText();
    }
    else
    {
      this.RenderText();
      this.UpdateTexelSize();
    }
  }

  private void OnDisable()
  {
    if (!(bool) ((UnityEngine.Object) this.m_RenderOnObject))
      return;
    this.m_RenderOnObject.GetComponent<Renderer>().enabled = false;
  }

  private void OnDestroy()
  {
    this.CleanUp();
  }

  private void OnEnable()
  {
    this.m_updated = false;
    this.SetFont(this.m_Font);
    this.UpdateNow();
  }

  private void OnDrawGizmos()
  {
    float width = this.GetWidth();
    float height = this.GetHeight();
    Gizmos.matrix = this.transform.localToWorldMatrix;
    Gizmos.color = new Color(0.3f, 0.3f, 0.35f, 0.2f);
    Gizmos.DrawCube(Vector3.zero, new Vector3(width + width * 0.02f, height + height * 0.02f, 0.0f));
    Gizmos.color = Color.black;
    Gizmos.DrawWireCube(Vector3.zero, new Vector3(width, height, 0.0f));
    if (this.m_Underwear)
    {
      float underwearWidth = this.m_UnderwearWidth;
      float underwearHeight = this.m_UnderwearHeight;
      if (this.m_LocalizedSettings != null)
      {
        UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
        if (locale != null)
        {
          if ((double) locale.m_UnderwearWidth > 0.0)
            underwearWidth = locale.m_UnderwearWidth;
          if ((double) locale.m_UnderwearHeight > 0.0)
            underwearHeight = locale.m_UnderwearHeight;
        }
      }
      float x = (float) ((double) width * (double) underwearWidth * 0.5);
      float y = height * underwearHeight;
      if (this.m_UnderwearFlip)
      {
        Gizmos.DrawWireCube(new Vector3((float) -((double) width * 0.5 - (double) x * 0.5), (float) ((double) height * 0.5 - (double) y * 0.5), 0.0f), new Vector3(x, y, 0.0f));
        Gizmos.DrawWireCube(new Vector3((float) ((double) width * 0.5 - (double) x * 0.5), (float) ((double) height * 0.5 - (double) y * 0.5), 0.0f), new Vector3(x, y, 0.0f));
      }
      else
      {
        Gizmos.DrawWireCube(new Vector3((float) -((double) width * 0.5 - (double) x * 0.5), (float) -((double) height * 0.5 - (double) y * 0.5), 0.0f), new Vector3(x, y, 0.0f));
        Gizmos.DrawWireCube(new Vector3((float) ((double) width * 0.5 - (double) x * 0.5), (float) -((double) height * 0.5 - (double) y * 0.5), 0.0f), new Vector3(x, y, 0.0f));
      }
    }
    Gizmos.matrix = Matrix4x4.identity;
  }

  private void OnDrawGizmosSelected()
  {
    float width = this.GetWidth();
    float height = this.GetHeight();
    Gizmos.matrix = this.transform.localToWorldMatrix;
    Gizmos.color = Color.green;
    Gizmos.DrawWireCube(Vector3.zero, new Vector3(width + width * 0.04f, height + height * 0.04f, 0.0f));
    Gizmos.matrix = Matrix4x4.identity;
  }

  public void Show()
  {
    this.m_Hidden = false;
    this.UpdateText();
  }

  public void Hide()
  {
    this.m_Hidden = true;
    this.UpdateText();
  }

  public bool isHidden()
  {
    return this.m_Hidden;
  }

  public void EditorAwake()
  {
    this.DestroyChildren();
    this.UpdateText();
  }

  public bool IsDone()
  {
    return this.m_updated;
  }

  public void UpdateText()
  {
    if (!this.gameObject.activeInHierarchy)
      return;
    this.m_updated = false;
  }

  public void UpdateNow()
  {
    if (!this.gameObject.activeInHierarchy)
      return;
    this.m_updated = false;
    this.RenderText();
  }

  public Bounds GetBounds()
  {
    Matrix4x4 localToWorldMatrix = this.transform.localToWorldMatrix;
    Vector3 vector3_1 = localToWorldMatrix.MultiplyVector(Vector3.up) * (this.GetHeight() * 0.5f);
    Vector3 vector3_2 = localToWorldMatrix.MultiplyVector(Vector3.right) * (this.GetWidth() * 0.5f);
    return new Bounds() { min = this.transform.position - vector3_2 + vector3_1, max = this.transform.position + vector3_2 - vector3_1 };
  }

  public Bounds GetTextBounds()
  {
    if (!this.m_updated)
      this.UpdateNow();
    Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
    if ((bool) ((UnityEngine.Object) this.m_TextMesh))
    {
      Quaternion rotation = this.transform.rotation;
      this.transform.rotation = Quaternion.identity;
      bounds = this.m_TextMesh.GetComponent<Renderer>().bounds;
      this.transform.rotation = rotation;
    }
    return bounds;
  }

  public Bounds GetTextWorldSpaceBounds()
  {
    if (!this.m_updated)
      this.UpdateNow();
    Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
    if ((bool) ((UnityEngine.Object) this.m_TextMesh))
      bounds = this.m_TextMesh.GetComponent<Renderer>().bounds;
    return bounds;
  }

  public Vector3 GetLocalizationPositionOffset()
  {
    Vector3 vector3 = Vector3.zero;
    if (this.m_LocalizedSettings != null)
    {
      UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
      if (locale != null)
        vector3 = locale.m_PositionOffset;
    }
    return vector3;
  }

  public int GetLineCount()
  {
    return this.m_LineCount;
  }

  public float GetActualCharacterSize()
  {
    return (float) ((double) this.m_TextMesh.characterSize / (double) this.m_CharacterSizeModifier / 0.00999999977648258);
  }

  public bool IsMultiLine()
  {
    return this.m_LineCount > 1;
  }

  public bool IsEllipsized()
  {
    return this.m_Ellipsized;
  }

  public void SetGameStringText(string gameStringTag)
  {
    this.Text = GameStrings.Get(gameStringTag);
  }

  public Font GetLocalizedFont()
  {
    if ((bool) ((UnityEngine.Object) this.m_LocalizedFont))
      return this.m_LocalizedFont;
    return this.m_Font;
  }

  public UberText.LocalizationSettings.LocaleAdjustment AddLocaleAdjustment(Locale locale)
  {
    return this.m_LocalizedSettings.AddLocale(locale);
  }

  public UberText.LocalizationSettings.LocaleAdjustment GetLocaleAdjustment(Locale locale)
  {
    return this.m_LocalizedSettings.GetLocale(locale);
  }

  public void RemoveLocaleAdjustment(Locale locale)
  {
    this.m_LocalizedSettings.RemoveLocale(locale);
  }

  public UberText.LocalizationSettings GetAllLocalizationSettings()
  {
    return this.m_LocalizedSettings;
  }

  public void SetFontWithoutLocalization(FontDef fontDef)
  {
    Font font = fontDef.m_Font;
    if ((UnityEngine.Object) font == (UnityEngine.Object) null || (UnityEngine.Object) this.m_TextMesh != (UnityEngine.Object) null && (UnityEngine.Object) this.m_TextMesh.font == (UnityEngine.Object) font)
      return;
    this.m_Font = font;
    this.m_LocalizedFont = this.m_Font;
    this.m_LineSpaceModifier = fontDef.m_LineSpaceModifier;
    this.m_FontSizeModifier = fontDef.m_FontSizeModifier;
    this.m_SingleLineAdjustment = fontDef.m_SingleLineAdjustment;
    this.m_CharacterSizeModifier = fontDef.m_CharacterSizeModifier;
    this.m_UnboundCharacterSizeModifier = fontDef.m_UnboundCharacterSizeModifier;
    this.m_OutlineModifier = fontDef.m_OutlineModifier;
    this.m_isFontDefLoaded = true;
    this.m_FontTexture = font.material.mainTexture;
    this.UpdateFontTextures();
    if ((UnityEngine.Object) this.m_TextMesh != (UnityEngine.Object) null)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_TextMesh);
    this.UpdateNow();
  }

  public string GetProcessedText()
  {
    string str = string.Empty;
    if ((UnityEngine.Object) this.m_TextMesh != (UnityEngine.Object) null)
      str = this.m_TextMesh.text;
    return str.Replace("<material=1></material>", string.Empty).Replace("<material=1>", "<b>").Replace("</material>", "</b>");
  }

  private void RenderText()
  {
    if (this.m_updated && (!(bool) ((UnityEngine.Object) this.m_TextTexture) || this.m_TextTexture.IsCreated()))
      return;
    if ((UnityEngine.Object) this.m_Font == (UnityEngine.Object) null)
      Debug.LogWarning((object) string.Format("UberText error: Font is null for {0}", (object) this.gameObject.name));
    else if (this.m_Text == null || this.m_Text == string.Empty)
    {
      if ((bool) ((UnityEngine.Object) this.m_TextMesh))
        this.m_TextMesh.GetComponent<Renderer>().enabled = false;
      if ((bool) ((UnityEngine.Object) this.m_PlaneGameObject))
        this.m_PlaneGameObject.GetComponent<Renderer>().enabled = false;
      if ((bool) ((UnityEngine.Object) this.m_RenderOnObject))
        this.m_RenderOnObject.GetComponent<Renderer>().enabled = false;
      if (!(bool) ((UnityEngine.Object) this.m_ShadowPlaneGameObject))
        return;
      this.m_ShadowPlaneGameObject.GetComponent<Renderer>().enabled = false;
    }
    else
    {
      if ((bool) ((UnityEngine.Object) this.m_TextMesh))
        this.m_TextMesh.GetComponent<Renderer>().enabled = true;
      if ((bool) ((UnityEngine.Object) this.m_PlaneGameObject))
        this.m_PlaneGameObject.GetComponent<Renderer>().enabled = true;
      if ((bool) ((UnityEngine.Object) this.m_RenderOnObject))
        this.m_RenderOnObject.GetComponent<Renderer>().enabled = true;
      if ((bool) ((UnityEngine.Object) this.m_ShadowPlaneGameObject))
        this.m_ShadowPlaneGameObject.GetComponent<Renderer>().enabled = true;
      if (this.m_Hidden)
      {
        if ((bool) ((UnityEngine.Object) this.m_TextMesh))
          this.m_TextMesh.GetComponent<Renderer>().enabled = false;
        if ((bool) ((UnityEngine.Object) this.m_PlaneGameObject))
          this.m_PlaneGameObject.GetComponent<Renderer>().enabled = false;
        if ((bool) ((UnityEngine.Object) this.m_RenderOnObject))
          this.m_RenderOnObject.GetComponent<Renderer>().enabled = false;
        if (!(bool) ((UnityEngine.Object) this.m_ShadowPlaneGameObject))
          return;
        this.m_ShadowPlaneGameObject.GetComponent<Renderer>().enabled = false;
      }
      else
      {
        if ((bool) ((UnityEngine.Object) this.m_TextMesh))
          this.m_TextMesh.GetComponent<Renderer>().enabled = true;
        if ((bool) ((UnityEngine.Object) this.m_PlaneGameObject))
          this.m_PlaneGameObject.GetComponent<Renderer>().enabled = true;
        if ((bool) ((UnityEngine.Object) this.m_RenderOnObject))
          this.m_RenderOnObject.GetComponent<Renderer>().enabled = true;
        if ((bool) ((UnityEngine.Object) this.m_ShadowPlaneGameObject))
          this.m_ShadowPlaneGameObject.GetComponent<Renderer>().enabled = true;
        Vector2 worldWidthAndHight = this.GetWorldWidthAndHight();
        this.m_WorldWidth = worldWidthAndHight.x;
        this.m_WorldHeight = worldWidthAndHight.y;
        this.CreateTextMesh();
        if ((UnityEngine.Object) this.m_TextMesh == (UnityEngine.Object) null)
          return;
        this.UpdateTextMesh();
        if (this.m_Outline)
          this.OutlineRender();
        if (this.m_RenderToTexture)
        {
          this.CreateCamera();
          this.CreateTexture();
          if ((bool) ((UnityEngine.Object) this.m_RenderOnObject))
            this.SetupRenderOnObject();
          else
            this.CreateRenderPlane();
          this.SetupForRender();
          if (!(bool) ((UnityEngine.Object) this.m_RenderOnObject))
            this.ShadowRender();
          this.UpdateTexelSize();
          if ((UnityEngine.Object) this.m_TextTexture == (UnityEngine.Object) null)
          {
            Debug.LogWarning((object) "UberText Render to Texture m_TextTexture is null!");
            this.m_updated = false;
            return;
          }
          if ((UnityEngine.Object) this.m_Camera.targetTexture != (UnityEngine.Object) this.m_TextTexture)
            this.m_Camera.targetTexture = this.m_TextTexture;
          this.m_Camera.Render();
          if (!this.m_TextTexture.IsCreated())
          {
            Debug.LogWarning((object) "UberText Render to Texture m_TextTexture.IsCreated() == false after render!");
            this.m_updated = false;
            return;
          }
          this.AntiAliasRender();
        }
        this.UpdateLayers();
        this.UpdateRenderQueue();
        this.UpdateColor();
        if ((bool) ((UnityEngine.Object) this.m_RenderOnObject))
          this.m_RenderOnObject.GetComponent<Renderer>().enabled = true;
        this.m_PreviousText = this.m_Text;
        this.m_updated = true;
      }
    }
  }

  private void UpdateTextMesh()
  {
    string str = string.Empty;
    bool flag1 = false;
    bool flag2 = false;
    if (!flag1)
    {
      this.m_CacheHash = new UberText.CachedTextKeyData()
      {
        m_Text = this.m_Text,
        m_CharSize = this.m_CharacterSize,
        m_Font = this.m_Font,
        m_FontSize = this.m_FontSize,
        m_Height = this.GetHeight(),
        m_Width = this.GetWidth(),
        m_LineSpacing = this.m_LineSpacing
      }.GetHashCode();
      if (this.m_Cache && (this.m_WordWrap || this.m_ResizeToFit) && UberText.s_CachedText.ContainsKey(this.m_CacheHash))
      {
        UberText.CachedTextValues cachedTextValues = UberText.s_CachedText[this.m_CacheHash];
        if (cachedTextValues.m_OriginalTextHash == this.m_Text.GetHashCode())
        {
          str = cachedTextValues.m_Text;
          this.SetText(str);
          this.SetActualCharacterSize(cachedTextValues.m_CharSize);
          flag2 = true;
        }
      }
    }
    Quaternion rotation = this.transform.rotation;
    this.transform.rotation = Quaternion.identity;
    if (!flag2)
    {
      string text = this.m_Text;
      string empty = string.Empty;
      if (this.m_GameStringLookup)
        text = GameStrings.Get(text.Trim());
      if (Localization.GetLocale() != Locale.enUS)
        text = this.LocalizationFixes(text);
      str = this.ProcessText(text);
      this.m_Words = this.BreakStringIntoWords(str);
      this.m_LineCount = UberText.LineCount(str);
      this.m_Ellipsized = false;
      if (this.m_WordWrap && !this.m_ResizeToFit)
        this.SetText(this.WordWrapString(str, this.m_WorldWidth));
      else
        this.SetText(str);
      this.SetActualCharacterSize((float) ((double) this.m_CharacterSize * (double) this.m_CharacterSizeModifier * 0.00999999977648258));
    }
    this.m_TextMesh.GetComponent<Renderer>().enabled = true;
    this.SetFont(this.m_Font);
    this.SetFontSize(this.m_FontSize);
    this.SetLineSpacing(this.m_LineSpacing);
    float width = this.GetWidth();
    float height = this.GetHeight();
    switch (this.m_Alignment)
    {
      case UberText.AlignmentOptions.Left:
        this.m_TextMesh.alignment = TextAlignment.Left;
        switch (this.m_Anchor)
        {
          case UberText.AnchorOptions.Upper:
            this.m_TextMesh.transform.localPosition = new Vector3((float) (-(double) width * 0.5), height * 0.5f, 0.0f);
            this.m_TextMesh.anchor = TextAnchor.UpperLeft;
            break;
          case UberText.AnchorOptions.Middle:
            this.m_TextMesh.transform.localPosition = new Vector3((float) (-(double) width * 0.5), 0.0f, 0.0f);
            this.m_TextMesh.anchor = TextAnchor.MiddleLeft;
            break;
          case UberText.AnchorOptions.Lower:
            this.m_TextMesh.transform.localPosition = new Vector3((float) (-(double) width * 0.5), (float) (-(double) height * 0.5), 0.0f);
            this.m_TextMesh.anchor = TextAnchor.LowerLeft;
            break;
        }
      case UberText.AlignmentOptions.Center:
        this.m_TextMesh.alignment = TextAlignment.Center;
        switch (this.m_Anchor)
        {
          case UberText.AnchorOptions.Upper:
            this.m_TextMesh.transform.localPosition = new Vector3(0.0f, height * 0.5f, 0.0f);
            this.m_TextMesh.anchor = TextAnchor.UpperCenter;
            break;
          case UberText.AnchorOptions.Middle:
            this.m_TextMesh.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            this.m_TextMesh.anchor = TextAnchor.MiddleCenter;
            break;
          case UberText.AnchorOptions.Lower:
            this.m_TextMesh.transform.localPosition = new Vector3(0.0f, (float) (-(double) height * 0.5), 0.0f);
            this.m_TextMesh.anchor = TextAnchor.LowerCenter;
            break;
        }
      case UberText.AlignmentOptions.Right:
        this.m_TextMesh.alignment = TextAlignment.Right;
        switch (this.m_Anchor)
        {
          case UberText.AnchorOptions.Upper:
            this.m_TextMesh.transform.localPosition = new Vector3(width * 0.5f, height * 0.5f, 0.0f);
            this.m_TextMesh.anchor = TextAnchor.UpperRight;
            break;
          case UberText.AnchorOptions.Middle:
            this.m_TextMesh.transform.localPosition = new Vector3(width * 0.5f, 0.0f, 0.0f);
            this.m_TextMesh.anchor = TextAnchor.MiddleRight;
            break;
          case UberText.AnchorOptions.Lower:
            this.m_TextMesh.transform.localPosition = new Vector3(width * 0.5f, (float) (-(double) height * 0.5), 0.0f);
            this.m_TextMesh.anchor = TextAnchor.LowerRight;
            break;
        }
    }
    if (this.m_ResizeToFit && !flag2)
      this.ResizeTextToFit(str);
    float num = 1f;
    if (this.m_LocalizedSettings != null)
    {
      UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
      if (locale != null)
        num = locale.m_UnboundCharacterSizeModifier;
    }
    this.m_TextMesh.characterSize = this.m_TextMesh.characterSize;
    this.transform.rotation = rotation;
    if (!flag1 && this.m_Cache && !flag2 && (this.m_WordWrap || this.m_ResizeToFit))
    {
      double result = 0.0;
      if (!double.TryParse(this.m_Text, out result) && this.m_Text.Length > 3)
      {
        UberText.s_CachedText[this.m_CacheHash] = new UberText.CachedTextValues();
        UberText.s_CachedText[this.m_CacheHash].m_Text = this.m_TextMesh.text;
        UberText.s_CachedText[this.m_CacheHash].m_CharSize = this.m_TextMesh.characterSize;
        UberText.s_CachedText[this.m_CacheHash].m_OriginalTextHash = this.m_Text.GetHashCode();
      }
    }
    this.m_TextMesh.characterSize *= this.m_UnboundCharacterSizeModifier * num;
    if (this.m_LocalizedSettings == null)
      return;
    UberText.LocalizationSettings.LocaleAdjustment locale1 = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
    if (locale1 == null)
      return;
    this.m_TextMesh.transform.localPosition += locale1.m_PositionOffset;
  }

  private void UpdateColor()
  {
    if (this.m_Outline)
    {
      if ((bool) ((UnityEngine.Object) this.m_OutlineTextMaterial))
      {
        this.m_OutlineTextMaterial.SetColor("_Color", this.m_TextColor);
        this.m_OutlineTextMaterial.SetColor("_OutlineColor", this.m_OutlineColor);
        this.m_OutlineTextMaterial.SetFloat("_LightingBlend", this.m_AmbientLightBlend);
      }
      if ((bool) ((UnityEngine.Object) this.m_BoldMaterial))
      {
        this.m_BoldMaterial.SetColor("_Color", this.m_TextColor);
        this.m_BoldMaterial.SetColor("_OutlineColor", this.m_OutlineColor);
        this.m_BoldMaterial.SetFloat("_LightingBlend", this.m_AmbientLightBlend);
      }
    }
    else
    {
      if ((bool) ((UnityEngine.Object) this.m_TextMaterial))
      {
        this.m_TextMaterial.SetColor("_Color", this.m_TextColor);
        this.m_TextMaterial.SetFloat("_LightingBlend", this.m_AmbientLightBlend);
      }
      if ((bool) ((UnityEngine.Object) this.m_BoldMaterial))
      {
        this.m_BoldMaterial.SetColor("_Color", this.m_TextColor);
        this.m_BoldMaterial.SetFloat("_LightingBlend", this.m_AmbientLightBlend);
      }
    }
    if (this.m_Shadow && (bool) ((UnityEngine.Object) this.m_ShadowMaterial))
      this.m_ShadowMaterial.SetColor("_Color", this.m_ShadowColor);
    if (!(bool) ((UnityEngine.Object) this.m_PlaneMesh))
      return;
    this.m_PlaneMesh.colors = new Color[4]
    {
      this.m_GradientLowerColor,
      this.m_GradientLowerColor,
      this.m_GradientUpperColor,
      this.m_GradientUpperColor
    };
  }

  private void UpdateFontTextures()
  {
    if ((bool) ((UnityEngine.Object) this.m_TextMaterial))
      this.m_TextMaterial.mainTexture = this.m_FontTexture;
    if ((bool) ((UnityEngine.Object) this.m_OutlineTextMaterial))
      this.m_OutlineTextMaterial.mainTexture = this.m_FontTexture;
    if (!(bool) ((UnityEngine.Object) this.m_BoldMaterial))
      return;
    this.m_BoldMaterial.mainTexture = this.m_FontTexture;
  }

  private void UpdateLayers()
  {
    if (this.m_RenderToTexture)
    {
      this.m_TextMeshGameObject.layer = 0;
      if ((bool) ((UnityEngine.Object) this.m_PlaneGameObject))
        this.m_PlaneGameObject.layer = this.gameObject.layer;
    }
    else if ((bool) ((UnityEngine.Object) this.m_TextMeshGameObject))
      this.m_TextMeshGameObject.layer = this.gameObject.layer;
    if (!this.m_Shadow || !(bool) ((UnityEngine.Object) this.m_ShadowPlaneGameObject))
      return;
    this.m_ShadowPlaneGameObject.layer = this.gameObject.layer;
  }

  private void UpdateRenderQueue()
  {
    GameObject gameObject = !this.m_RenderToTexture ? this.m_TextMeshGameObject : (!(bool) ((UnityEngine.Object) this.m_RenderOnObject) ? this.m_PlaneGameObject : this.m_RenderOnObject);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      return;
    if (this.m_OrgRenderQueue == -9999)
      this.m_OrgRenderQueue = gameObject.GetComponent<Renderer>().sharedMaterial.renderQueue;
    foreach (Material sharedMaterial in gameObject.GetComponent<Renderer>().sharedMaterials)
      sharedMaterial.renderQueue = this.m_OrgRenderQueue + this.m_RenderQueue;
    if (!this.m_Shadow || !(bool) ((UnityEngine.Object) this.m_ShadowPlaneGameObject))
      return;
    this.m_ShadowPlaneGameObject.GetComponent<Renderer>().sharedMaterial.renderQueue = gameObject.GetComponent<Renderer>().sharedMaterial.renderQueue + this.m_ShadowRenderQueueOffset;
  }

  private void UpdateTexelSize()
  {
    float num = this.m_OutlineSize * this.m_OutlineModifier;
    if (this.m_LocalizedSettings != null)
    {
      UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
      if (locale != null)
        num = this.m_OutlineSize * this.m_OutlineModifier * locale.m_OutlineModifier;
    }
    if ((UnityEngine.Object) this.m_FontTexture == (UnityEngine.Object) null)
      this.m_FontTexture = this.GetFontTexture();
    if ((UnityEngine.Object) this.m_FontTexture == (UnityEngine.Object) null)
    {
      if (!Application.isPlaying)
        return;
      Debug.LogError((object) string.Format("UberText.UpdateTexelSize() - m_FontTexture == null!  text={0}", (object) this.m_Text));
    }
    else
    {
      Vector2 vector2 = this.TexelSize(this.m_FontTexture);
      if (vector2 == this.m_PreviousTexelSize)
        return;
      if ((UnityEngine.Object) this.m_BoldMaterial != (UnityEngine.Object) null)
      {
        this.m_BoldMaterial.SetFloat("_BoldOffsetX", this.m_BoldSize * vector2.x);
        this.m_BoldMaterial.SetFloat("_BoldOffsetY", this.m_BoldSize * vector2.y);
      }
      if (this.m_Outline && !this.m_RenderToTexture)
      {
        if ((UnityEngine.Object) this.m_OutlineTextMaterial != (UnityEngine.Object) null)
        {
          this.m_OutlineTextMaterial.SetFloat("_OutlineOffsetX", vector2.x * num);
          this.m_OutlineTextMaterial.SetFloat("_OutlineOffsetY", vector2.y * num);
          this.m_OutlineTextMaterial.SetFloat("_TexelSizeX", vector2.x);
          this.m_OutlineTextMaterial.SetFloat("_TexelSizeY", vector2.y);
        }
        if ((UnityEngine.Object) this.m_BoldMaterial != (UnityEngine.Object) null)
        {
          this.m_BoldMaterial.SetFloat("_BoldOffsetX", this.m_BoldSize * vector2.x);
          this.m_BoldMaterial.SetFloat("_BoldOffsetY", this.m_BoldSize * vector2.y);
          this.m_BoldMaterial.SetFloat("_OutlineOffsetX", vector2.x * num);
          this.m_BoldMaterial.SetFloat("_OutlineOffsetY", vector2.y * num);
        }
      }
      if (this.m_Shadow && this.m_RenderToTexture && (UnityEngine.Object) this.m_ShadowMaterial != (UnityEngine.Object) null)
      {
        this.m_ShadowMaterial.SetFloat("_OffsetX", vector2.x * this.m_ShadowBlur);
        this.m_ShadowMaterial.SetFloat("_OffsetY", vector2.y * this.m_ShadowBlur);
      }
      if (this.m_AntiAlias && this.m_RenderToTexture && (UnityEngine.Object) this.m_TextAntialiasingMaterial != (UnityEngine.Object) null)
      {
        this.m_TextAntialiasingMaterial.SetFloat("_OffsetX", vector2.x * this.m_AntiAliasAmount);
        this.m_TextAntialiasingMaterial.SetFloat("_OffsetY", vector2.y * this.m_AntiAliasAmount);
      }
      this.m_PreviousTexelSize = vector2;
    }
  }

  private void CreateTextMesh()
  {
    if (!(bool) ((UnityEngine.Object) this.m_TextMeshGameObject))
    {
      this.m_TextMeshGameObject = new GameObject();
      this.m_TextMeshGameObject.name = "UberText_RenderObject_" + this.name;
      SceneUtils.SetHideFlags((UnityEngine.Object) this.m_TextMeshGameObject, HideFlags.HideAndDontSave);
    }
    else if ((bool) ((UnityEngine.Object) this.m_TextMeshGameObject.GetComponent<TextMesh>()))
      this.SetText(string.Empty);
    if (this.m_RenderToTexture)
    {
      Vector3 vector3 = new Vector3(-3000f, 3000f, this.Offset);
      this.m_TextMeshGameObject.transform.parent = (Transform) null;
      this.m_TextMeshGameObject.transform.position = vector3;
      this.m_TextMeshGameObject.transform.rotation = Quaternion.identity;
    }
    else
    {
      this.m_TextMeshGameObject.transform.parent = this.transform;
      this.m_TextMeshGameObject.transform.localPosition = Vector3.zero;
      this.m_TextMeshGameObject.transform.localRotation = Quaternion.identity;
      this.m_TextMeshGameObject.transform.localScale = Vector3.one;
    }
    if (!(bool) ((UnityEngine.Object) this.m_TextMesh))
    {
      this.m_TextMaterialIndices.Clear();
      if ((UnityEngine.Object) this.m_TextMeshGameObject == (UnityEngine.Object) null)
        return;
      if ((UnityEngine.Object) this.m_TextMeshGameObject.GetComponent<MeshRenderer>() == (UnityEngine.Object) null)
        this.m_TextMeshGameObject.AddComponent<MeshRenderer>();
      TextMesh component = this.m_TextMeshGameObject.GetComponent<TextMesh>();
      this.m_TextMesh = !(bool) ((UnityEngine.Object) component) ? this.m_TextMeshGameObject.AddComponent<TextMesh>() : component;
      if ((UnityEngine.Object) this.m_TextMesh == (UnityEngine.Object) null)
      {
        Debug.LogError((object) "UberText: Faild to create TextMesh");
        return;
      }
      this.SetRichText(this.m_RichText);
      Texture fontTexture = this.GetFontTexture();
      this.m_TextMesh.GetComponent<Renderer>().sharedMaterial = this.TextMaterial;
      this.m_TextMesh.GetComponent<Renderer>().sharedMaterial.mainTexture = fontTexture;
      this.m_TextMesh.GetComponent<Renderer>().sharedMaterial.color = this.m_TextColor;
      if (this.m_RichText)
      {
        Material[] materialArray = new Material[2]{ this.m_TextMesh.GetComponent<Renderer>().sharedMaterial, null };
        this.m_TextMaterialIndices.Add(UberText.TextRenderMaterial.Text, 0);
        materialArray[1] = this.BoldMaterial;
        materialArray[1].mainTexture = fontTexture;
        this.m_TextMaterialIndices.Add(UberText.TextRenderMaterial.Bold, 1);
        this.m_TextMesh.GetComponent<Renderer>().sharedMaterials = materialArray;
      }
      else
      {
        Material[] materialArray = new Material[1]{ this.m_TextMesh.GetComponent<Renderer>().sharedMaterial };
        this.m_TextMaterialIndices.Add(UberText.TextRenderMaterial.Text, 0);
        this.m_TextMesh.GetComponent<Renderer>().sharedMaterials = materialArray;
      }
    }
    if (!this.m_Outline && (UnityEngine.Object) this.m_TextMesh.GetComponent<Renderer>().sharedMaterial == (UnityEngine.Object) this.m_OutlineTextMaterial)
    {
      Texture mainTexture = this.m_TextMesh.GetComponent<Renderer>().sharedMaterial.mainTexture;
      this.m_TextMesh.GetComponent<Renderer>().sharedMaterial = this.TextMaterial;
      this.m_TextMesh.GetComponent<Renderer>().sharedMaterial.mainTexture = mainTexture;
    }
    this.SetFont(this.m_Font);
    this.SetFontSize(this.m_FontSize);
    this.SetLineSpacing(this.m_LineSpacing);
    this.SetActualCharacterSize((float) ((double) this.m_CharacterSize * (double) this.m_CharacterSizeModifier * 0.00999999977648258));
    if (this.m_Text == null)
      this.SetText(string.Empty);
    else
      this.SetText(this.m_Text);
  }

  private void SetFont(Font font)
  {
    if ((UnityEngine.Object) font == (UnityEngine.Object) null)
      return;
    if (!this.m_isFontDefLoaded)
    {
      FontTable fontTable = FontTable.Get();
      if ((UnityEngine.Object) fontTable != (UnityEngine.Object) null)
      {
        FontDef fontDef = fontTable.GetFontDef(font);
        if ((UnityEngine.Object) fontDef != (UnityEngine.Object) null)
        {
          this.m_LocalizedFont = fontDef.m_Font;
          this.m_LineSpaceModifier = fontDef.m_LineSpaceModifier;
          this.m_FontSizeModifier = fontDef.m_FontSizeModifier;
          this.m_SingleLineAdjustment = fontDef.m_SingleLineAdjustment;
          this.m_CharacterSizeModifier = fontDef.m_CharacterSizeModifier;
          this.m_UnboundCharacterSizeModifier = fontDef.m_UnboundCharacterSizeModifier;
          this.m_OutlineModifier = fontDef.m_OutlineModifier;
          this.m_isFontDefLoaded = true;
        }
        else
          Debug.LogErrorFormat("Error loading fontDef for UberText component={0} font={1}", new object[2]
          {
            (object) this.name,
            (object) font
          });
      }
    }
    if ((UnityEngine.Object) this.m_TextMesh == (UnityEngine.Object) null || (UnityEngine.Object) this.m_TextMesh.font == (UnityEngine.Object) font)
      return;
    this.m_TextMesh.font = !((UnityEngine.Object) this.m_LocalizedFont == (UnityEngine.Object) null) ? this.m_LocalizedFont : this.m_Font;
    this.m_FontTexture = this.m_TextMesh.font.material.mainTexture;
    this.UpdateFontTextures();
  }

  private void SetFontSize(int fontSize)
  {
    if (this.m_LocalizedSettings != null)
    {
      UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
      if (locale != null)
        fontSize = (int) ((double) locale.m_FontSizeModifier * (double) fontSize);
    }
    fontSize = (int) ((double) this.m_FontSizeModifier * (double) fontSize);
    if (this.m_TextMesh.fontSize == fontSize)
      return;
    this.m_TextMesh.fontSize = fontSize;
  }

  private float GetWidth()
  {
    float width = this.m_Width;
    if (this.m_LocalizedSettings != null)
    {
      UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
      if (locale != null && (double) locale.m_Width > 0.0)
        width = locale.m_Width;
    }
    return width;
  }

  private float GetHeight()
  {
    float height = this.m_Height;
    if (this.m_LocalizedSettings != null)
    {
      UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
      if (locale != null && (double) locale.m_Height > 0.0)
        height = locale.m_Height;
    }
    return height;
  }

  private void SetLineSpacing(float lineSpacing)
  {
    int num = UberText.LineCount(this.m_TextMesh.text);
    if (this.m_LocalizedSettings != null)
    {
      UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
      if (locale != null)
      {
        if (num == 1)
          lineSpacing += locale.m_SingleLineAdjustment;
        else
          lineSpacing *= locale.m_LineSpaceModifier;
      }
    }
    if (num == 1)
      lineSpacing += this.m_SingleLineAdjustment;
    else
      lineSpacing *= this.m_LineSpaceModifier;
    if ((double) this.m_TextMesh.lineSpacing == (double) lineSpacing)
      return;
    this.m_TextMesh.lineSpacing = lineSpacing;
  }

  private void SetActualCharacterSize(float characterSize)
  {
    if ((double) this.m_TextMesh.characterSize == (double) characterSize)
      return;
    this.m_TextMesh.characterSize = characterSize;
  }

  private void SetText(string text)
  {
    if (!(this.m_TextMesh.text != text))
      return;
    this.m_TextMesh.text = text;
  }

  private void SetRichText(bool richText)
  {
    if (this.m_TextMesh.richText == richText)
      return;
    this.m_TextMesh.richText = richText;
  }

  private Texture GetFontTexture()
  {
    if (!((UnityEngine.Object) this.m_LocalizedFont == (UnityEngine.Object) null))
      return this.m_LocalizedFont.material.mainTexture;
    if ((UnityEngine.Object) this.m_Font == (UnityEngine.Object) null)
      return (Texture) null;
    return this.m_Font.material.mainTexture;
  }

  private void DestroyTextMesh()
  {
    if (!(bool) ((UnityEngine.Object) this.m_TextMeshGameObject))
      return;
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_TextMeshGameObject);
  }

  private void CreateEditorRoot()
  {
  }

  private void CreateCamera()
  {
    if ((UnityEngine.Object) this.m_Camera != (UnityEngine.Object) null)
      return;
    this.m_CameraGO = new GameObject();
    this.m_Camera = this.m_CameraGO.AddComponent<Camera>();
    this.m_CameraGO.name = "UberText_RenderCamera_" + this.name;
    SceneUtils.SetHideFlags((UnityEngine.Object) this.m_CameraGO, HideFlags.HideAndDontSave);
    this.m_Camera.orthographic = true;
    this.m_CameraGO.transform.parent = this.m_TextMeshGameObject.transform;
    this.m_CameraGO.transform.rotation = Quaternion.identity;
    this.m_CameraGO.transform.position = this.m_TextMeshGameObject.transform.position;
    this.m_Camera.nearClipPlane = -0.1f;
    this.m_Camera.farClipPlane = 0.1f;
    if ((bool) ((UnityEngine.Object) Camera.main))
      this.m_Camera.depth = Camera.main.depth - 50f;
    Color color = this.m_TextColor;
    if (this.m_Outline)
      color = this.m_OutlineColor;
    this.m_Camera.backgroundColor = new Color(color.r, color.g, color.b, 0.0f);
    this.m_Camera.clearFlags = CameraClearFlags.Color;
    this.m_Camera.depthTextureMode = DepthTextureMode.None;
    this.m_Camera.renderingPath = RenderingPath.Forward;
    this.m_Camera.cullingMask = UberText.RENDER_LAYER_BIT;
    this.m_Camera.enabled = false;
  }

  private void DestroyCamera()
  {
    if (!(bool) ((UnityEngine.Object) this.m_CameraGO))
      return;
    this.m_Camera.targetTexture = (RenderTexture) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_CameraGO);
  }

  private void CreateTexture()
  {
    Vector2 vector2 = this.CalcTextureSize();
    if ((UnityEngine.Object) this.m_TextTexture != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) this.m_Camera.targetTexture == (UnityEngine.Object) null)
        this.m_Camera.targetTexture = this.m_TextTexture;
      if (this.m_TextTexture.width == (int) vector2.x && this.m_TextTexture.height == (int) vector2.y)
        return;
    }
    this.DestroyTexture();
    this.m_TextTexture = new RenderTexture((int) vector2.x, (int) vector2.y, 0, UberText.s_TextureFormat);
    SceneUtils.SetHideFlags((UnityEngine.Object) this.m_TextTexture, HideFlags.HideAndDontSave);
    if ((bool) ((UnityEngine.Object) this.m_Camera))
      this.m_Camera.targetTexture = this.m_TextTexture;
    if ((bool) ((UnityEngine.Object) this.m_PlaneGameObject) && (bool) ((UnityEngine.Object) this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial))
      this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial.mainTexture = (Texture) this.m_TextTexture;
    this.m_PreviousResolution = this.m_Resolution;
  }

  private void DestroyTexture()
  {
    if (!((UnityEngine.Object) this.m_TextTexture != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_TextTexture);
  }

  private void CreateRenderPlane()
  {
    if ((UnityEngine.Object) this.m_PlaneMesh != (UnityEngine.Object) null && (double) this.GetWidth() == (double) this.m_PreviousPlaneWidth && ((double) this.GetHeight() == (double) this.m_PreviousPlaneHeight && this.m_PreviousResolution == this.m_Resolution))
      return;
    if ((UnityEngine.Object) this.m_PlaneGameObject != (UnityEngine.Object) null)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_PlaneGameObject);
    this.m_PlaneGameObject = new GameObject();
    this.m_PlaneGameObject.name = "UberText_RenderPlane_" + this.name;
    this.m_PlaneGameObject.AddComponent<MeshFilter>();
    this.m_PlaneGameObject.AddComponent<MeshRenderer>();
    Mesh mesh1 = new Mesh();
    SceneUtils.SetHideFlags((UnityEngine.Object) this.m_PlaneGameObject, HideFlags.DontSave);
    this.m_PlaneGameObject.transform.parent = this.transform;
    this.m_PlaneGameObject.transform.position = this.transform.position;
    this.m_PlaneGameObject.transform.rotation = this.transform.rotation;
    this.m_PlaneGameObject.transform.Rotate(-90f, 0.0f, 0.0f);
    this.m_PlaneGameObject.transform.localScale = Vector3.one;
    float x = this.GetWidth() * 0.5f;
    float z = this.GetHeight() * 0.5f;
    mesh1.vertices = new Vector3[4]
    {
      new Vector3(-x, 0.0f, -z),
      new Vector3(x, 0.0f, -z),
      new Vector3(-x, 0.0f, z),
      new Vector3(x, 0.0f, z)
    };
    mesh1.colors = new Color[4]
    {
      this.m_GradientLowerColor,
      this.m_GradientLowerColor,
      this.m_GradientUpperColor,
      this.m_GradientUpperColor
    };
    mesh1.uv = this.PLANE_UVS;
    mesh1.normals = this.PLANE_NORMALS;
    mesh1.triangles = this.PLANE_TRIANGLES;
    Mesh mesh2 = mesh1;
    this.m_PlaneGameObject.GetComponent<MeshFilter>().mesh = mesh2;
    this.m_PlaneMesh = mesh2;
    this.m_PlaneMesh.RecalculateBounds();
    this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial = this.PlaneMaterial;
    this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial.mainTexture = (Texture) this.m_TextTexture;
    this.m_PreviousPlaneWidth = this.GetWidth();
    this.m_PreviousPlaneHeight = this.GetHeight();
  }

  private void DestroyRenderPlane()
  {
    if (!((UnityEngine.Object) this.m_PlaneGameObject != (UnityEngine.Object) null))
      return;
    MeshFilter component = this.m_PlaneGameObject.GetComponent<MeshFilter>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
    {
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) component.sharedMesh);
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) component);
    }
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_PlaneGameObject);
  }

  private void SetupRenderOnObject()
  {
    if (!(bool) ((UnityEngine.Object) this.m_RenderOnObject))
      return;
    this.m_RenderOnObject.GetComponent<Renderer>().sharedMaterial = this.PlaneMaterial;
    this.m_RenderOnObject.GetComponent<Renderer>().sharedMaterial.mainTexture = (Texture) this.m_TextTexture;
  }

  private void ResizeTextToFit(string text)
  {
    if (text == null || text == string.Empty)
      return;
    Transform parent = this.m_TextMeshGameObject.transform.parent;
    Quaternion rotation = this.m_TextMeshGameObject.transform.rotation;
    Vector3 localScale = this.m_TextMeshGameObject.transform.localScale;
    this.m_TextMeshGameObject.transform.parent = (Transform) null;
    this.m_TextMeshGameObject.transform.localScale = Vector3.one;
    this.m_TextMeshGameObject.transform.rotation = Quaternion.identity;
    float width = this.GetWidth();
    this.SetText(this.RemoveTagsFromWord(text) ?? string.Empty);
    if (this.m_WordWrap)
      this.SetText(this.WordWrapString(text, width));
    this.ReduceText_CharSize(text);
    this.m_TextMeshGameObject.transform.parent = parent;
    this.m_TextMeshGameObject.transform.localScale = localScale;
    this.m_TextMeshGameObject.transform.rotation = rotation;
    if (this.m_WordWrap)
      return;
    this.SetText(text);
  }

  private void ReduceText(string text, int step, int newSize)
  {
    if (this.m_FontSize == 1)
      return;
    this.SetFontSize(newSize);
    float num1 = this.GetHeight();
    float width = this.GetWidth();
    if (!this.m_RenderToTexture)
    {
      num1 = this.m_WorldHeight;
      width = this.m_WorldWidth;
    }
    if (!this.IsMultiLine())
      this.SetLineSpacing(0.0f);
    float y = this.m_TextMesh.GetComponent<Renderer>().bounds.size.y;
    float x = this.m_TextMesh.GetComponent<Renderer>().bounds.size.x;
    int num2 = 0;
    for (; (double) y > (double) num1 || (double) x > (double) width; x = this.m_TextMesh.GetComponent<Renderer>().bounds.size.x)
    {
      ++num2;
      if (num2 <= 40)
      {
        newSize -= step;
        if (newSize < this.m_MinFontSize)
        {
          newSize = this.m_MinFontSize;
          break;
        }
        this.SetFontSize(newSize);
        if (this.m_WordWrap)
          this.SetText(this.WordWrapString(text, width));
        y = this.m_TextMesh.GetComponent<Renderer>().bounds.size.y;
      }
      else
        break;
    }
    if (!this.IsMultiLine())
      this.SetLineSpacing(this.m_LineSpacing);
    this.m_FontSize = newSize;
  }

  private void ReduceText_CharSize(string text)
  {
    float height = this.GetHeight();
    float width = this.GetWidth();
    float characterSize = this.m_TextMesh.characterSize;
    if (!this.IsMultiLine())
      this.SetLineSpacing(0.0f);
    else
      this.SetLineSpacing(this.m_LineSpacing);
    float x = this.m_TextMesh.GetComponent<Renderer>().bounds.size.x;
    float y = this.m_TextMesh.GetComponent<Renderer>().bounds.size.y;
    int num1 = 0;
    float num2 = 1f;
    if (this.m_LocalizedSettings != null)
    {
      UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
      if (locale != null && (double) locale.m_ResizeToFitWidthModifier > 0.0)
        num2 = locale.m_ResizeToFitWidthModifier;
    }
    for (; (double) y > (double) height || (double) x > (double) width * (double) num2; y = this.m_TextMesh.GetComponent<Renderer>().bounds.size.y)
    {
      ++num1;
      if (num1 <= 40)
      {
        characterSize *= 0.95f;
        if ((double) characterSize <= (double) this.m_MinCharacterSize * 0.00999999977648258)
        {
          this.SetActualCharacterSize(this.m_MinCharacterSize * 0.01f);
          if (this.m_WordWrap)
          {
            this.SetText(this.WordWrapString(text, width, true));
            break;
          }
          break;
        }
        this.SetActualCharacterSize(characterSize);
        if (this.m_WordWrap)
          this.SetText(this.WordWrapString(text, width, false));
        if (UberText.LineCount(this.m_TextMesh.text) > 1)
          this.SetLineSpacing(this.m_LineSpacing);
        else
          this.SetLineSpacing(0.0f);
        x = this.m_TextMesh.GetComponent<Renderer>().bounds.size.x;
      }
      else
        break;
    }
    this.SetLineSpacing(this.m_LineSpacing);
  }

  private void SetupForRender()
  {
    if (this.m_RenderToTexture)
    {
      Vector3 vector3_1 = new Vector3(-3000f, 3000f, this.Offset);
      this.m_TextMeshGameObject.transform.parent = (Transform) null;
      this.m_TextMeshGameObject.transform.position = vector3_1;
      this.m_TextMeshGameObject.transform.rotation = Quaternion.identity;
      this.m_TextMeshGameObject.transform.localScale = Vector3.one;
      this.m_TextMeshGameObject.layer = UberText.RENDER_LAYER;
      float x = -3000f;
      if (this.Alignment == UberText.AlignmentOptions.Left)
        x += this.m_Width * 0.5f;
      if (this.Alignment == UberText.AlignmentOptions.Right)
        x -= this.m_Width * 0.5f;
      float num = 0.0f;
      if (this.m_Anchor == UberText.AnchorOptions.Upper)
        num += this.m_Height * 0.5f;
      if (this.m_Anchor == UberText.AnchorOptions.Lower)
        num -= this.m_Height * 0.5f;
      Vector3 vector3_2 = new Vector3(x, 3000f - num, this.Offset);
      this.m_CameraGO.transform.parent = this.m_TextMeshGameObject.transform;
      this.m_CameraGO.transform.position = vector3_2;
      Color color = this.m_TextColor;
      if (this.m_Outline)
        color = this.m_OutlineColor;
      this.m_Camera.backgroundColor = new Color(color.r, color.g, color.b, 0.0f);
      this.m_Camera.orthographicSize = this.m_Height * 0.5f;
      if ((bool) ((UnityEngine.Object) this.RenderOnObject) || !((UnityEngine.Object) this.m_PlaneGameObject != (UnityEngine.Object) null))
        return;
      this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial.mainTexture = (Texture) this.m_TextTexture;
    }
    else
    {
      this.m_TextMeshGameObject.transform.parent = this.transform;
      this.m_TextMeshGameObject.transform.localPosition = Vector3.zero;
      this.m_CameraGO.transform.position = this.transform.position;
    }
  }

  private string WordWrapString(string text, float width)
  {
    return this.WordWrapString(text, width, false);
  }

  private string WordWrapString(string text, float width, bool ellipsis)
  {
    if (text == null || text == string.Empty)
      return string.Empty;
    float width1 = this.GetWidth();
    float height = this.GetHeight();
    float num1 = width;
    float num2 = 0.0f;
    float num3 = 0.0f;
    Bounds bounds1 = new Bounds();
    Bounds bounds2 = new Bounds();
    Quaternion rotation = this.m_TextMeshGameObject.transform.rotation;
    Vector3 position = this.m_TextMeshGameObject.transform.position;
    Vector3 localScale = this.m_TextMeshGameObject.transform.localScale;
    this.m_TextMeshGameObject.transform.rotation = Quaternion.identity;
    this.m_TextMeshGameObject.transform.position = new Vector3(0.0f, height * 0.25f, 0.0f);
    this.m_TextMeshGameObject.transform.localScale = Vector3.one;
    if (this.m_Underwear)
    {
      float underwearHeight = this.m_UnderwearHeight;
      float underwearWidth = this.m_UnderwearWidth;
      if (this.m_LocalizedSettings != null)
      {
        UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
        if (locale != null)
        {
          if ((double) locale.m_UnderwearWidth > 0.0)
            underwearWidth = locale.m_UnderwearWidth;
          if ((double) locale.m_UnderwearHeight > 0.0)
            underwearHeight = locale.m_UnderwearHeight;
        }
      }
      num2 = !this.m_UnderwearFlip ? height * (1f - underwearHeight) : height * underwearHeight;
      num3 = width * (1f - underwearWidth);
      Vector3 vector3 = new Vector3((float) ((double) width1 * (double) this.m_UnderwearWidth * 0.5), (float) ((double) height * (double) this.m_UnderwearHeight * 0.5), 1f);
      float num4 = 0.0f;
      float num5 = !this.m_UnderwearFlip ? (float) ((double) num4 - (double) height * 0.5 + (double) height * (double) this.m_UnderwearHeight * 0.5) : (float) ((double) num4 + (double) height * 0.5 - (double) height * (double) this.m_UnderwearHeight * 0.5);
      Vector3 zero1 = Vector3.zero;
      zero1.x = (float) ((double) zero1.x + (double) width1 * 0.5 - (double) width1 * 0.5 * (double) this.m_UnderwearWidth * 0.5);
      zero1.y = num5;
      bounds1.center = zero1;
      bounds1.size = vector3;
      Vector3 zero2 = Vector3.zero;
      zero2.x = (float) ((double) zero2.x - (double) width1 * 0.5 + (double) width1 * 0.5 * (double) this.m_UnderwearWidth * 0.5);
      zero2.y = num5;
      bounds2.center = zero2;
      bounds2.size = vector3;
    }
    TextAnchor anchor = this.m_TextMesh.anchor;
    if (this.m_Underwear)
      this.m_TextMesh.anchor = TextAnchor.UpperCenter;
    StringBuilder stringBuilder1 = new StringBuilder();
    StringBuilder stringBuilder2 = new StringBuilder();
    string[] strArray = this.m_Words;
    if (strArray == null)
      return text;
    if ((!this.m_ResizeToFit || ellipsis) && this.m_ForceWrapLargeWords)
    {
      List<string> stringList = new List<string>();
      foreach (string str in strArray)
      {
        this.SetText(str);
        float x = this.m_TextMesh.GetComponent<Renderer>().bounds.size.x;
        if ((double) x < (double) width)
        {
          stringList.Add(str);
        }
        else
        {
          int num4 = Mathf.CeilToInt(x / width);
          int start = 0;
          int end = 1;
          for (int index = 0; index < num4; ++index)
          {
            this.SetText(str.Slice(start, end));
            while ((double) this.m_TextMesh.GetComponent<Renderer>().bounds.size.x < (double) width && end < str.Length)
            {
              ++end;
              this.SetText(str.Slice(start, end));
            }
            stringList.Add(str.Slice(start, end - 1));
            start = end - 1;
          }
          stringList.Add(str.Slice(start, str.Length));
        }
      }
      strArray = stringList.ToArray();
    }
    int num6 = 0;
    if (text.Contains("\n"))
    {
      foreach (byte num4 in text)
      {
        if ((int) num4 == 10)
          ++num6;
      }
    }
    bool flag = false;
    if (this.m_Underwear && !this.m_UnderwearFlip)
      flag = true;
    if (this.m_Underwear && this.m_UnderwearFlip)
    {
      StringBuilder stringBuilder3 = new StringBuilder();
      StringBuilder stringBuilder4 = new StringBuilder();
      foreach (string word in strArray)
      {
        string str = this.RemoveTagsFromWord(word);
        stringBuilder4.Append(str);
        string empty = stringBuilder4.ToString();
        if (empty == null)
        {
          Debug.LogWarning((object) "UberText: actualLine is null in WordWrapString!");
          empty = string.Empty;
        }
        this.SetText(empty);
        if ((double) this.m_TextMesh.GetComponent<Renderer>().bounds.size.x < (double) width)
        {
          stringBuilder3.Append(word);
        }
        else
        {
          flag = true;
          break;
        }
      }
      if (stringBuilder3.ToString().Contains("\n"))
        flag = true;
    }
    foreach (string word in strArray)
    {
      string str = this.RemoveTagsFromWord(word);
      stringBuilder2.Append(str);
      string empty = stringBuilder2.ToString();
      if (empty == null)
      {
        Debug.LogWarning((object) "UberText: actualLine is null in WordWrapString!");
        empty = string.Empty;
      }
      this.SetText(empty);
      float x = this.m_TextMesh.GetComponent<Renderer>().bounds.size.x;
      if (this.m_Underwear && flag)
      {
        this.SetText(stringBuilder1.ToString());
        float y = this.m_TextMesh.GetComponent<Renderer>().bounds.size.y;
        if (this.m_UnderwearFlip)
          width = (double) y - ((double) height - (double) y) * 0.200000002980232 >= (double) num2 ? num1 : num3;
        else if (this.m_TextMesh.GetComponent<Renderer>().bounds.Intersects(bounds1) || this.m_TextMesh.GetComponent<Renderer>().bounds.Intersects(bounds2))
          width = num3;
      }
      if ((double) x < (double) width)
      {
        stringBuilder1.Append(word);
      }
      else
      {
        if (ellipsis)
        {
          this.SetText(stringBuilder1.ToString() + (object) '\n');
          if ((double) this.m_TextMesh.GetComponent<Renderer>().bounds.size.y > (double) height)
          {
            this.m_Ellipsized = true;
            stringBuilder1.Append(" ...");
            break;
          }
        }
        if (stringBuilder1.Length > 2 && (int) stringBuilder1.ToString()[stringBuilder1.Length - 1] == 93 && ((int) stringBuilder1.ToString()[stringBuilder1.Length - 2] == 100 && (int) stringBuilder1.ToString()[stringBuilder1.Length - 3] == 91))
          stringBuilder1.Append('-');
        if (stringBuilder1.Length > 0)
          stringBuilder1.Append('\n');
        ++num6;
        stringBuilder1.Append(word.TrimStart(' '));
        stringBuilder2 = new StringBuilder();
        for (int index = 0; index < this.m_LineCount; ++index)
          stringBuilder2.Append("\n");
        stringBuilder2.Append(str);
      }
    }
    this.m_TextMeshGameObject.transform.rotation = rotation;
    this.m_TextMeshGameObject.transform.position = position;
    this.m_TextMeshGameObject.transform.localScale = localScale;
    this.m_TextMesh.anchor = anchor;
    string s = UberText.RemoveLineBreakTagsHardSpace(stringBuilder1.ToString());
    if (s == null)
    {
      Debug.LogWarning((object) "UberText: Word Wrap returned a null string!");
      s = string.Empty;
    }
    this.m_LineCount = UberText.LineCount(s);
    return s;
  }

  private string ProcessText(string text)
  {
    if (!this.m_RichText)
      return text;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.Append("<material=1></material>");
    stringBuilder.Append(text);
    for (int startIndex = 0; startIndex < text.Length; ++startIndex)
    {
      if ((int) text[startIndex] == 60 && startIndex <= text.Length - 2)
      {
        if ((int) text[startIndex + 1] == 98)
        {
          if (startIndex + 3 >= text.Length || (int) text[startIndex + 3] != 60)
          {
            this.Bold();
            if (this.m_TextMesh.GetComponent<Renderer>().sharedMaterials.Length < 1)
            {
              Debug.LogWarning((object) "UberText: Tried to set Bold material, but material missing!");
            }
            else
            {
              stringBuilder.Replace("<b>", "<material=1>");
              stringBuilder.Replace("</b>", "</material>");
              ++startIndex;
            }
          }
        }
        else if ((int) text[startIndex + 1] == 109 && startIndex <= text.Length - 3 && (int) text[startIndex + 2] != 97)
        {
          int num = text.Substring(startIndex).IndexOf('>');
          if (num < 1)
          {
            ++startIndex;
          }
          else
          {
            string str = text.Substring(startIndex, num + 1);
            stringBuilder.Replace(str, this.InlineImage(str));
          }
        }
      }
    }
    string empty = stringBuilder.ToString();
    if (empty == null)
    {
      Debug.LogWarning((object) "UberText: ProcessText returned a null string!");
      empty = string.Empty;
    }
    return empty;
  }

  private string LocalizationFixes(string text)
  {
    if (Localization.GetLocale() == Locale.thTH)
      return this.FixThai(text);
    return text;
  }

  private void ShadowRender()
  {
    if (!this.m_Shadow)
    {
      this.DestroyShadow();
    }
    else
    {
      if ((UnityEngine.Object) this.m_PlaneGameObject == (UnityEngine.Object) null)
        return;
      if ((UnityEngine.Object) this.m_ShadowPlaneGameObject != (UnityEngine.Object) null)
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_ShadowPlaneGameObject);
      this.m_ShadowPlaneGameObject = new GameObject();
      this.m_ShadowPlaneGameObject.name = "UberText_ShadowPlane_" + this.name;
      this.m_ShadowPlaneGameObject.AddComponent<MeshFilter>();
      this.m_ShadowPlaneGameObject.AddComponent<MeshRenderer>();
      Mesh mesh1 = new Mesh();
      SceneUtils.SetHideFlags((UnityEngine.Object) this.m_ShadowPlaneGameObject, HideFlags.DontSave);
      this.m_ShadowPlaneGameObject.transform.parent = this.m_PlaneGameObject.transform;
      this.m_ShadowPlaneGameObject.transform.localRotation = Quaternion.identity;
      this.m_ShadowPlaneGameObject.transform.localScale = Vector3.one;
      float num = (float) (-(double) this.m_ShadowOffset * 0.00999999977648258);
      float y = this.m_ShadowDepthOffset * 0.01f;
      this.m_ShadowPlaneGameObject.transform.localPosition = new Vector3(num, y, num);
      float x = this.GetWidth() * 0.5f;
      float z = this.GetHeight() * 0.5f;
      mesh1.vertices = new Vector3[4]
      {
        new Vector3(-x, 0.0f, -z),
        new Vector3(x, 0.0f, -z),
        new Vector3(-x, 0.0f, z),
        new Vector3(x, 0.0f, z)
      };
      mesh1.uv = this.PLANE_UVS;
      mesh1.normals = this.PLANE_NORMALS;
      mesh1.triangles = this.PLANE_TRIANGLES;
      Mesh mesh2 = mesh1;
      this.m_ShadowPlaneGameObject.GetComponent<MeshFilter>().mesh = mesh2;
      mesh2.RecalculateBounds();
      this.m_ShadowPlaneGameObject.GetComponent<Renderer>().sharedMaterial = this.ShadowMaterial;
      this.m_ShadowMaterial.mainTexture = (Texture) this.m_TextTexture;
      Vector2 vector2 = this.TexelSize((Texture) this.m_TextTexture);
      this.m_ShadowMaterial.SetColor("_Color", this.m_ShadowColor);
      this.m_ShadowMaterial.SetFloat("_OffsetX", vector2.x * this.m_ShadowBlur);
      this.m_ShadowMaterial.SetFloat("_OffsetY", vector2.y * this.m_ShadowBlur);
    }
  }

  private void DestroyShadow()
  {
    if (!((UnityEngine.Object) this.m_ShadowPlaneGameObject != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_ShadowPlaneGameObject);
  }

  private void OutlineRender()
  {
    if ((UnityEngine.Object) this.m_TextMesh == (UnityEngine.Object) null)
    {
      Debug.LogError((object) string.Format("UberText OutlineRender ({0}, {1}): m_TextMesh == null", (object) this.gameObject.name, (object) this.m_Text));
    }
    else
    {
      Material sharedMaterial = this.m_TextMesh.GetComponent<Renderer>().sharedMaterial;
      if ((UnityEngine.Object) sharedMaterial == (UnityEngine.Object) null)
      {
        Debug.LogError((object) string.Format("UberText OutlineRender ({0}, {1}): m_TextMesh.renderer.sharedMaterial == null", (object) this.gameObject.name, (object) this.m_Text));
      }
      else
      {
        Texture mainTexture = sharedMaterial.mainTexture;
        if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
          Debug.LogError((object) string.Format("UberText OutlineRender ({0}, {1}): textMat.mainTexture == null", (object) this.gameObject.name, (object) this.m_Text));
        else if ((UnityEngine.Object) this.OutlineTextMaterial == (UnityEngine.Object) null)
        {
          Debug.LogError((object) string.Format("UberText OutlineRender ({0}, {1}): OutlineTextMaterial == null", (object) this.gameObject.name, (object) this.m_Text));
        }
        else
        {
          this.m_TextMesh.GetComponent<Renderer>().sharedMaterial = this.OutlineTextMaterial;
          if ((UnityEngine.Object) this.m_OutlineTextMaterial == (UnityEngine.Object) null)
          {
            Debug.LogError((object) string.Format("UberText OutlineRender ({0}, {1}): m_OutlineTextMaterial == null", (object) this.gameObject.name, (object) this.m_Text));
          }
          else
          {
            this.m_OutlineTextMaterial.mainTexture = mainTexture;
            Vector2 vector2 = this.TexelSize(this.m_OutlineTextMaterial.mainTexture);
            float num = this.m_OutlineSize * this.m_OutlineModifier;
            if (this.m_LocalizedSettings != null)
            {
              UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
              if (locale != null)
                num = this.m_OutlineSize * this.m_OutlineModifier * locale.m_OutlineModifier;
            }
            this.m_OutlineTextMaterial.SetFloat("_OutlineOffsetX", vector2.x * num);
            this.m_OutlineTextMaterial.SetFloat("_OutlineOffsetY", vector2.y * num);
            this.m_OutlineTextMaterial.SetColor("_Color", this.m_TextColor);
            this.m_OutlineTextMaterial.SetColor("_OutlineColor", this.m_OutlineColor);
            this.m_OutlineTextMaterial.SetFloat("_TexelSizeX", vector2.x);
            this.m_OutlineTextMaterial.SetFloat("_TexelSizeY", vector2.y);
          }
        }
      }
    }
  }

  private void AntiAliasRender()
  {
    if ((UnityEngine.Object) this.m_PlaneGameObject == (UnityEngine.Object) null && (UnityEngine.Object) this.m_RenderOnObject == (UnityEngine.Object) null)
      return;
    if (this.m_AntiAlias)
    {
      Texture texture;
      if ((bool) ((UnityEngine.Object) this.m_RenderOnObject))
      {
        texture = this.m_RenderOnObject.GetComponent<Renderer>().sharedMaterial.GetTexture("_MainTex");
        this.m_RenderOnObject.GetComponent<Renderer>().sharedMaterial = this.TextAntialiasingMaterial;
        this.m_RenderOnObject.GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex", texture);
      }
      else
      {
        texture = this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial.GetTexture("_MainTex");
        this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial = this.TextAntialiasingMaterial;
        this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex", texture);
      }
      Vector2 vector2 = this.TexelSize(texture);
      this.m_TextAntialiasingMaterial.SetFloat("_OffsetX", vector2.x * this.m_AntiAliasAmount);
      this.m_TextAntialiasingMaterial.SetFloat("_OffsetY", vector2.y * this.m_AntiAliasAmount);
      this.m_TextAntialiasingMaterial.SetFloat("_Edge", this.m_AntiAliasEdge);
    }
    else if ((bool) ((UnityEngine.Object) this.m_RenderOnObject))
      this.m_RenderOnObject.GetComponent<Renderer>().sharedMaterial = this.PlaneMaterial;
    else
      this.m_PlaneGameObject.GetComponent<Renderer>().sharedMaterial = this.PlaneMaterial;
  }

  private void Bold()
  {
    if ((double) this.m_BoldSize > 10.0)
      this.m_BoldSize = 10f;
    if (this.m_Outline)
    {
      if ((UnityEngine.Object) this.m_BoldOutlineShader == (UnityEngine.Object) null)
      {
        this.m_BoldOutlineShader = ShaderUtils.FindShader(this.BOLD_OUTLINE_TEXT_SHADER_NAME);
        if (!(bool) ((UnityEngine.Object) this.m_BoldOutlineShader))
          Debug.LogError((object) ("UberText Failed to load Shader: " + this.BOLD_OUTLINE_TEXT_SHADER_NAME));
      }
      float num = this.m_OutlineSize * this.m_OutlineModifier;
      if (this.m_LocalizedSettings != null)
      {
        UberText.LocalizationSettings.LocaleAdjustment locale = this.m_LocalizedSettings.GetLocale(Localization.GetLocale());
        if (locale != null)
          num = this.m_OutlineSize * this.m_OutlineModifier * locale.m_OutlineModifier;
      }
      this.m_BoldMaterial.shader = this.m_BoldOutlineShader;
      Vector2 vector2 = this.TexelSize(this.m_BoldMaterial.mainTexture);
      this.m_BoldMaterial.SetColor("_OutlineColor", this.m_OutlineColor);
      this.m_BoldMaterial.SetFloat("_BoldOffsetX", this.m_BoldSize * vector2.x);
      this.m_BoldMaterial.SetFloat("_BoldOffsetY", this.m_BoldSize * vector2.y);
      this.m_BoldMaterial.SetFloat("_OutlineOffsetX", vector2.x * (num + this.m_BoldSize * 0.75f));
      this.m_BoldMaterial.SetFloat("_OutlineOffsetY", vector2.y * (num + this.m_BoldSize * 0.75f));
    }
    else
    {
      this.m_BoldMaterial.shader = this.m_BoldShader;
      Vector2 vector2 = this.TexelSize(this.m_BoldMaterial.mainTexture);
      this.m_BoldMaterial.SetFloat("_BoldOffsetX", this.m_BoldSize * vector2.x);
      this.m_BoldMaterial.SetFloat("_BoldOffsetY", this.m_BoldSize * vector2.y);
      this.m_BoldMaterial.SetColor("_Color", this.m_TextColor);
    }
  }

  private string InlineImage(string tag)
  {
    if (tag == string.Empty)
      return string.Empty;
    if (!UberText.s_InlineImageTextureLoaded)
      AssetLoader.Get().LoadTexture("mana_in_line", new AssetLoader.ObjectCallback(this.SetManaTexture), (object) null, false);
    int index = this.TextEffectsMaterial(UberText.TextRenderMaterial.InlineImages, this.InlineImageMaterial);
    string key = tag;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (UberText.\u003C\u003Ef__switch\u0024mapCF == null)
      {
        // ISSUE: reference to a compiler-generated field
        UberText.\u003C\u003Ef__switch\u0024mapCF = new Dictionary<string, int>(13)
        {
          {
            "<m>",
            0
          },
          {
            "<me>",
            1
          },
          {
            "<m0>",
            2
          },
          {
            "<m1>",
            3
          },
          {
            "<m2>",
            4
          },
          {
            "<m3>",
            5
          },
          {
            "<m4>",
            6
          },
          {
            "<m5>",
            7
          },
          {
            "<m6>",
            8
          },
          {
            "<m7>",
            9
          },
          {
            "<m8>",
            10
          },
          {
            "<m9>",
            11
          },
          {
            "<m10>",
            12
          }
        };
      }
      int num1;
      // ISSUE: reference to a compiler-generated field
      if (UberText.\u003C\u003Ef__switch\u0024mapCF.TryGetValue(key, out num1))
      {
        float num2;
        float num3;
        switch (num1)
        {
          case 0:
            num2 = 0.0f;
            num3 = 0.0f;
            break;
          case 1:
            num2 = 0.75f;
            num3 = 0.25f;
            break;
          case 2:
            num2 = 0.0f;
            num3 = 0.75f;
            break;
          case 3:
            num2 = 0.25f;
            num3 = 0.75f;
            break;
          case 4:
            num2 = 0.5f;
            num3 = 0.75f;
            break;
          case 5:
            num2 = 0.75f;
            num3 = 0.75f;
            break;
          case 6:
            num2 = 0.0f;
            num3 = 0.5f;
            break;
          case 7:
            num2 = 0.25f;
            num3 = 0.5f;
            break;
          case 8:
            num2 = 0.5f;
            num3 = 0.5f;
            break;
          case 9:
            num2 = 0.75f;
            num3 = 0.5f;
            break;
          case 10:
            num2 = 0.0f;
            num3 = 0.25f;
            break;
          case 11:
            num2 = 0.25f;
            num3 = 0.25f;
            break;
          case 12:
            num2 = 0.5f;
            num3 = 0.25f;
            break;
          default:
            goto label_22;
        }
        string empty = string.Empty;
        this.m_TextMesh.GetComponent<Renderer>().sharedMaterials[index].mainTexture = (Texture) UberText.s_InlineImageTexture;
        return string.Format("<quad material={0} size={1} x={2} y={3} width=0.25 height=0.25 />", (object) index, (object) this.m_FontSize, (object) num2, (object) num3);
      }
    }
label_22:
    return tag;
  }

  private int TextEffectsMaterial(UberText.TextRenderMaterial materialKey, Material material)
  {
    if (this.m_TextMaterialIndices.ContainsKey(materialKey))
      return this.m_TextMaterialIndices[materialKey];
    Material[] materialArray = new Material[this.m_TextMesh.GetComponent<Renderer>().sharedMaterials.Length + 1];
    int index = materialArray.Length - 1;
    this.m_TextMesh.GetComponent<Renderer>().sharedMaterials.CopyTo((Array) materialArray, 0);
    materialArray[index] = material;
    this.m_TextMesh.GetComponent<Renderer>().sharedMaterials = materialArray;
    this.m_TextMaterialIndices.Add(materialKey, index);
    return index;
  }

  private void SetManaTexture(string name, UnityEngine.Object obj, object callbackData)
  {
    UberText.s_InlineImageTexture = obj as Texture2D;
    UberText.s_InlineImageTextureLoaded = true;
    this.m_TextMesh.GetComponent<Renderer>().sharedMaterials[this.m_TextMaterialIndices[UberText.TextRenderMaterial.InlineImages]].mainTexture = (Texture) UberText.s_InlineImageTexture;
  }

  private void UpdateEditorText()
  {
  }

  public static void DisableCache()
  {
    UberText.s_disableCache = true;
    UberText.s_CachedText.Clear();
  }

  private Vector2 GetWorldWidthAndHight()
  {
    float width = this.GetWidth();
    float height = this.GetHeight();
    Quaternion rotation = this.transform.rotation;
    this.transform.rotation = Quaternion.identity;
    Vector3 lossyScale = this.transform.lossyScale;
    float x = width;
    if ((double) lossyScale.x > 0.0)
      x = width * lossyScale.x;
    float y = height;
    if ((double) lossyScale.y > 0.0)
      y = height * lossyScale.y;
    this.transform.rotation = rotation;
    return new Vector2(x, y);
  }

  public static Vector3 GetWorldScale(Transform xform)
  {
    Vector3 localScale = xform.localScale;
    if ((UnityEngine.Object) xform.parent != (UnityEngine.Object) null)
    {
      for (Transform parent = xform.parent; (UnityEngine.Object) parent != (UnityEngine.Object) null; parent = parent.parent)
        localScale.Scale(parent.localScale);
    }
    return localScale;
  }

  private Vector3 GetLossyWorldScale(Transform xform)
  {
    Quaternion rotation = xform.rotation;
    xform.rotation = Quaternion.identity;
    Vector3 lossyScale = this.transform.lossyScale;
    xform.rotation = rotation;
    return lossyScale;
  }

  private void FindSupportedTextureFormat()
  {
    if (UberText.s_TextureFormat != RenderTextureFormat.DefaultHDR)
      return;
    if (SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGB4444))
      UberText.s_TextureFormat = RenderTextureFormat.ARGB4444;
    else
      UberText.s_TextureFormat = RenderTextureFormat.Default;
  }

  private Vector2 CalcTextureSize()
  {
    float width = this.GetWidth();
    float height = this.GetHeight();
    Vector2 vector2 = new Vector2((float) this.m_Resolution, (float) this.m_Resolution);
    if ((double) width > (double) height)
    {
      vector2.x = (float) this.m_Resolution;
      vector2.y = (float) this.m_Resolution * (height / width);
    }
    else
    {
      vector2.x = (float) this.m_Resolution * (width / height);
      vector2.y = (float) this.m_Resolution;
    }
    if ((UnityEngine.Object) GraphicsManager.Get() != (UnityEngine.Object) null && GraphicsManager.Get().RenderQualityLevel == GraphicsQuality.Low)
    {
      vector2.x *= 0.75f;
      vector2.y *= 0.75f;
    }
    return vector2;
  }

  private string RemoveTagsFromWord(string word)
  {
    if (!this.m_RichText || !word.Contains("<") && !word.Contains("["))
      return word;
    StringBuilder stringBuilder = new StringBuilder();
    bool flag = false;
    for (int startIndex = 0; startIndex < word.Length; ++startIndex)
    {
      if ((int) word[startIndex] == 60)
      {
        if (startIndex < word.Length - 1)
        {
          if ((int) word[startIndex + 1] == 113)
          {
            if (!word.Substring(startIndex).Contains(">"))
              return stringBuilder.ToString();
            int index = startIndex + 1;
            while ((int) word[index] != 62)
              ++index;
            stringBuilder.Append("W");
            startIndex = index;
          }
          else
            flag = true;
        }
      }
      else if ((int) word[startIndex] == 62)
        flag = false;
      else if ((int) word[startIndex] == 91 && startIndex + 2 < word.Length && (UberText.IsValidSquareBracketTag(word[startIndex + 1]) && (int) word[startIndex + 2] == 93))
      {
        flag = true;
      }
      else
      {
        if ((int) word[startIndex] == 93)
        {
          if (startIndex - 2 >= 0 && UberText.IsValidSquareBracketTag(word[startIndex - 1]) && (int) word[startIndex - 2] == 91)
          {
            flag = false;
            continue;
          }
          flag = false;
        }
        if (!flag)
          stringBuilder.Append(word[startIndex]);
      }
    }
    return stringBuilder.ToString();
  }

  private static string RemoveLineBreakTagsHardSpace(string text)
  {
    StringBuilder stringBuilder = new StringBuilder();
    bool flag = false;
    for (int index = 0; index < text.Length; ++index)
    {
      if ((int) text[index] == 91 && index + 2 < text.Length && (UberText.IsValidSquareBracketTag(text[index + 1]) && (int) text[index + 2] == 93))
      {
        flag = true;
      }
      else
      {
        if ((int) text[index] == 93)
        {
          if (index - 2 >= 0 && UberText.IsValidSquareBracketTag(text[index - 1]) && (int) text[index - 2] == 91)
          {
            flag = false;
            continue;
          }
          flag = false;
        }
        if (!flag)
        {
          if ((int) text[index] == 95)
            stringBuilder.Append(' ');
          else
            stringBuilder.Append(text[index]);
        }
      }
    }
    return stringBuilder.ToString();
  }

  private static bool IsValidSquareBracketTag(char ch)
  {
    char ch1 = ch;
    switch (ch1)
    {
      case 'b':
      case 'd':
        return true;
      default:
        if ((int) ch1 != 120)
          return false;
        goto case 'b';
    }
  }

  private static bool IsWhitespace(char ch)
  {
    char ch1 = ch;
    switch (ch1)
    {
      case '\t':
      case '\n':
      case '\r':
        return true;
      default:
        if ((int) ch1 != 32)
          return false;
        goto case '\t';
    }
  }

  public static string RemoveMarkupAndCollapseWhitespaces(string text)
  {
    if (text == null)
      return string.Empty;
    StringBuilder stringBuilder = (StringBuilder) null;
    int startIndex1 = 0;
    int startIndex2 = 0;
    int startIndex3;
    for (int index = text.IndexOfAny(UberText.STRIP_CHARS_INDEX_OF_ANY, startIndex2); index >= 0 && index < text.Length; index = text.IndexOfAny(UberText.STRIP_CHARS_INDEX_OF_ANY, startIndex3))
    {
      startIndex3 = index + 1;
      if (((int) text[index] != 32 || index + 1 < text.Length && UberText.IsWhitespace(text[index + 1])) && ((int) text[index] != 91 || index + 2 < text.Length && UberText.IsValidSquareBracketTag(text[index + 1]) && (int) text[index + 2] == 93))
      {
        if (index > startIndex1)
        {
          if (stringBuilder == null)
            stringBuilder = new StringBuilder();
          string str = text.Substring(startIndex1, index - startIndex1);
          stringBuilder.Append(str);
        }
        if (UberText.IsWhitespace(text[index]))
        {
          while (startIndex3 < text.Length && UberText.IsWhitespace(text[startIndex3]))
            ++startIndex3;
          if (stringBuilder == null)
            stringBuilder = new StringBuilder();
          stringBuilder.Append(' ');
          startIndex1 = startIndex3;
        }
        else
        {
          char ch = (int) text[index] != 60 ? ']' : '>';
          int num = text.IndexOf(ch, startIndex3);
          if (num < 0)
          {
            startIndex1 = text.Length;
            break;
          }
          startIndex3 = num + 1;
          startIndex1 = startIndex3;
        }
      }
    }
    if (startIndex1 < text.Length && startIndex1 > 0)
      stringBuilder.Append(text.Substring(startIndex1));
    if (stringBuilder != null)
      return stringBuilder.ToString();
    if (startIndex1 == 0)
      return text;
    return string.Empty;
  }

  private void DestroyChildren()
  {
    GameObject gameObject1 = new GameObject("UberTextDestroyDummy");
    foreach (Transform componentsInChild in this.GetComponentsInChildren<Transform>())
    {
      if (!((UnityEngine.Object) this.transform == (UnityEngine.Object) componentsInChild) && !((UnityEngine.Object) componentsInChild == (UnityEngine.Object) null))
      {
        GameObject gameObject2 = componentsInChild.gameObject;
        if (!((UnityEngine.Object) gameObject2 == (UnityEngine.Object) null) && gameObject2.name.StartsWith("UberText_"))
          gameObject2.transform.parent = gameObject1.transform;
      }
    }
    if (Application.isPlaying)
      UnityEngine.Object.Destroy((UnityEngine.Object) gameObject1);
    else
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) gameObject1);
  }

  private void CleanUp()
  {
    this.m_Offset = 0.0f;
    this.DestroyRenderPlane();
    this.DestroyCamera();
    this.DestroyTexture();
    this.DestroyShadow();
    this.DestroyTextMesh();
    this.m_updated = false;
    if ((bool) ((UnityEngine.Object) this.m_BoldMaterial))
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_BoldMaterial);
    if ((bool) ((UnityEngine.Object) this.m_TextMaterial))
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_TextMaterial);
    if ((bool) ((UnityEngine.Object) this.m_OutlineTextMaterial))
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_OutlineTextMaterial);
    if ((bool) ((UnityEngine.Object) this.m_TextAntialiasingMaterial))
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_TextAntialiasingMaterial);
    if ((bool) ((UnityEngine.Object) this.m_ShadowMaterial))
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_ShadowMaterial);
    if ((bool) ((UnityEngine.Object) this.m_PlaneMaterial))
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_PlaneMaterial);
    if (!(bool) ((UnityEngine.Object) this.m_InlineImageMaterial))
      return;
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_InlineImageMaterial);
  }

  private static int LineCount(string s)
  {
    int num = 1;
    for (int index = 0; index < s.Length; ++index)
    {
      if ((int) s[index] == 10)
        ++num;
    }
    return num;
  }

  private string[] BreakStringIntoWords(string text)
  {
    if (text == null || text == string.Empty)
      return (string[]) null;
    List<string> stringList1 = new List<string>();
    bool flag1 = false;
    TextElementEnumerator elementEnumerator = StringInfo.GetTextElementEnumerator(text);
    List<string> stringList2 = new List<string>(StringInfo.ParseCombiningCharacters(text).Length);
    while (elementEnumerator.MoveNext())
      stringList2.Add(elementEnumerator.GetTextElement());
    bool flag2 = false;
    StringBuilder stringBuilder = new StringBuilder(stringList2[0]);
    if (stringList2[0] == "<" && this.m_RichText)
      flag1 = true;
    for (int index = 1; index < stringList2.Count; ++index)
    {
      if (stringList2[index] == "]" && index - 2 > 0)
      {
        if ((stringList2[index - 1] == "b" || stringList2[index - 1] == "d") && stringList2[index - 2] == "[")
        {
          stringBuilder.Append(stringList2[index]);
          stringList1.Add(stringBuilder.ToString());
          stringBuilder.Length = 0;
          continue;
        }
        if (stringList2[index - 1] == "x" && stringList2[index - 2] == "[")
          flag2 = true;
      }
      if (stringList2[index] == "<" && this.m_RichText)
      {
        flag1 = true;
        stringBuilder.Append(stringList2[index]);
      }
      else if (stringList2[index] == ">")
      {
        flag1 = false;
        stringBuilder.Append(stringList2[index]);
      }
      else if (flag1)
      {
        stringBuilder.Append(stringList2[index]);
      }
      else
      {
        string s1 = stringList2[index - 1];
        string s2 = stringList2[index];
        string empty = string.Empty;
        int utf32_1 = char.ConvertToUtf32(s1, 0);
        int utf32_2 = char.ConvertToUtf32(s2, 0);
        int nextChar = 0;
        if (index < stringList2.Count - 1 && !string.IsNullOrEmpty(empty))
          nextChar = char.ConvertToUtf32(empty, 0);
        if (!flag2 && this.CanWrapBetween(utf32_1, utf32_2, nextChar))
        {
          stringList1.Add(stringBuilder.ToString());
          stringBuilder.Length = 0;
        }
        stringBuilder.Append(stringList2[index]);
      }
    }
    stringList1.Add(stringBuilder.ToString());
    return stringList1.ToArray();
  }

  public Vector2 TexelSize(Texture texture)
  {
    int frameCount = Time.frameCount;
    Font key = this.m_Font;
    if ((UnityEngine.Object) this.m_LocalizedFont != (UnityEngine.Object) null)
      key = this.m_LocalizedFont;
    if (UberText.s_TexelUpdateFrame.ContainsKey(key) && UberText.s_TexelUpdateFrame[key] == frameCount)
      return UberText.s_TexelUpdateData[key];
    Vector2 vector2 = new Vector2();
    vector2.x = 1f / (float) texture.width;
    vector2.y = 1f / (float) texture.height;
    UberText.s_TexelUpdateFrame[key] = frameCount;
    UberText.s_TexelUpdateData[key] = vector2;
    return vector2;
  }

  private static void DeleteOldCacheFiles()
  {
    foreach (int num in Enum.GetValues(typeof (Locale)))
    {
      string path = string.Format("{0}/text_{1}.cache", (object) FileUtils.PersistentDataPath, (object) (Locale) num);
      if (File.Exists(path))
      {
        try
        {
          File.Delete(path);
        }
        catch (Exception ex)
        {
          Debug.LogError((object) string.Format("UberText.DeleteOldCacheFiles() - Failed to delete {0}. Reason={1}", (object) path, (object) ex.Message));
        }
      }
    }
  }

  private static string GetCacheFolderPath()
  {
    return string.Format("{0}/UberText", (object) FileUtils.CachePath);
  }

  private static string GetCacheFilePath()
  {
    return string.Format("{0}/text_{1}.cache", (object) UberText.GetCacheFolderPath(), (object) Localization.GetLocale());
  }

  private static void CreateCacheFolder()
  {
    string cacheFolderPath = UberText.GetCacheFolderPath();
    if (Directory.Exists(cacheFolderPath))
      return;
    try
    {
      Directory.CreateDirectory(cacheFolderPath);
    }
    catch (Exception ex)
    {
      Debug.LogError((object) string.Format("UberText.CreateCacheFolder() - Failed to create {0}. Reason={1}", (object) cacheFolderPath, (object) ex.Message));
    }
  }

  public static void StoreCachedData()
  {
    if (UberText.s_disableCache)
      return;
    UberText.CreateCacheFolder();
    string cacheFilePath = UberText.GetCacheFilePath();
    using (BinaryWriter binaryWriter = new BinaryWriter((Stream) File.Open(cacheFilePath, FileMode.Create)))
    {
      int num = 15590;
      binaryWriter.Write(num);
      using (Map<int, UberText.CachedTextValues>.Enumerator enumerator = UberText.s_CachedText.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, UberText.CachedTextValues> current = enumerator.Current;
          binaryWriter.Write(current.Key);
          binaryWriter.Write(current.Value.m_Text);
          binaryWriter.Write(current.Value.m_CharSize);
          binaryWriter.Write(current.Value.m_OriginalTextHash);
        }
      }
    }
    Log.Kyle.Print("UberText Cache Stored: " + cacheFilePath);
  }

  public static void LoadCachedData()
  {
    if (UberText.s_disableCache)
      return;
    UberText.s_CachedText.Clear();
    UberText.DeleteOldCacheFiles();
    UberText.CreateCacheFolder();
    string cacheFilePath = UberText.GetCacheFilePath();
    if (!File.Exists(cacheFilePath))
      return;
    int num = 15590;
    using (BinaryReader binaryReader = new BinaryReader((Stream) File.Open(cacheFilePath, FileMode.Open)))
    {
      if (binaryReader.BaseStream.Length == 0L || binaryReader.ReadInt32() != num)
        return;
      if (binaryReader.PeekChar() == -1)
        return;
      try
      {
        while (binaryReader.PeekChar() != -1)
        {
          int key = binaryReader.ReadInt32();
          UberText.s_CachedText.Add(key, new UberText.CachedTextValues()
          {
            m_Text = binaryReader.ReadString(),
            m_CharSize = binaryReader.ReadSingle(),
            m_OriginalTextHash = binaryReader.ReadInt32()
          });
        }
      }
      catch (Exception ex)
      {
        Debug.LogWarning((object) string.Format("UberText LoadCachedData() failed: {0}", (object) ex.Message));
        UberText.s_CachedText.Clear();
      }
    }
    if (UberText.s_CachedText.Count > 50000)
      UberText.s_CachedText.Clear();
    Log.Kyle.Print("UberText Cache Loaded: " + UberText.s_CachedText.Count.ToString());
  }

  private bool CanWrapBetween(int lastChar, int wideChar, int nextChar)
  {
    if (Localization.GetLocale() == Locale.frFR || Localization.GetLocale() == Locale.deDE)
    {
      if (char.IsWhiteSpace(char.ConvertFromUtf32(wideChar), 0))
      {
        int num = nextChar;
        switch (num)
        {
          case 58:
          case 59:
          case 63:
            return false;
          default:
            if (num == 33 || num == 46 || (num == 171 || num == 187))
              goto case 58;
            else
              break;
        }
      }
      if (char.IsWhiteSpace(char.ConvertFromUtf32(wideChar), 0) && lastChar == 171)
        return false;
    }
    if (lastChar == 45)
      return wideChar < 48 || wideChar > 57;
    if (lastChar == 59 || wideChar == 124)
      return true;
    if (char.IsWhiteSpace(char.ConvertFromUtf32(lastChar), 0))
      return false;
    if (char.IsWhiteSpace(char.ConvertFromUtf32(wideChar), 0))
      return true;
    int num1 = lastChar;
    switch (num1)
    {
      case 12296:
      case 12298:
      case 12300:
      case 12302:
      case 12304:
label_21:
        return false;
      default:
        switch (num1 - 65505)
        {
          case 0:
          case 4:
          case 5:
            goto label_21;
          default:
            switch (num1 - 65113)
            {
              case 0:
              case 2:
              case 4:
                goto label_21;
              default:
                if (num1 != 91 && num1 != 92 && (num1 != 36 && num1 != 40) && (num1 != 123 && num1 != 8216 && (num1 != 8220 && num1 != 8245)) && (num1 != 12308 && num1 != 12317 && (num1 != 65284 && num1 != 65288) && (num1 != 65339 && num1 != 65371)))
                {
                  int num2 = wideChar;
                  switch (num2)
                  {
                    case 65104:
                    case 65105:
                    case 65106:
                    case 65108:
                    case 65109:
                    case 65110:
                    case 65111:
                    case 65114:
                    case 65116:
                    case 65118:
label_30:
                      return false;
                    default:
                      switch (num2 - 12297)
                      {
                        case 0:
                        case 2:
                        case 4:
                        case 6:
                        case 8:
                          goto label_30;
                        default:
                          switch (num2 - 41)
                          {
                            case 0:
                            case 3:
                            case 5:
                              goto label_30;
                            default:
                              switch (num2 - 58)
                              {
                                case 0:
                                case 1:
                                case 5:
                                  goto label_30;
                                default:
                                  switch (num2 - 8226)
                                  {
                                    case 0:
                                    case 4:
                                    case 5:
                                      goto label_30;
                                    default:
                                      switch (num2 - 65289)
                                      {
                                        case 0:
                                        case 3:
                                        case 5:
                                          goto label_30;
                                        default:
                                          switch (num2 - 65306)
                                          {
                                            case 0:
                                            case 1:
                                            case 5:
                                              goto label_30;
                                            default:
                                              if (num2 != 8211 && num2 != 8212 && (num2 != 8242 && num2 != 8243) && (num2 != 12289 && num2 != 12290 && (num2 != 65438 && num2 != 65439)) && (num2 != 33 && num2 != 37 && (num2 != 93 && num2 != 125) && (num2 != 176 && num2 != 183 && (num2 != 8217 && num2 != 8221))) && (num2 != 8451 && num2 != 12309 && (num2 != 12318 && num2 != 12540) && (num2 != 65072 && num2 != 65281 && (num2 != 65285 && num2 != 65341)) && (num2 != 65373 && num2 != 65392 && num2 != 65504)))
                                              {
                                                switch (lastChar)
                                                {
                                                  case 12290:
                                                  case 65292:
                                                    return true;
                                                  default:
                                                    return (Localization.GetLocale() != Locale.koKR || this.m_Alignment != UberText.AlignmentOptions.Center) && (wideChar >= 4352 && wideChar <= 4607 || wideChar >= 12288 && wideChar <= 55215 || (wideChar >= 63744 && wideChar <= 64255 || wideChar >= 65280 && wideChar <= 65439) || wideChar >= 65440 && wideChar <= 65500);
                                                }
                                              }
                                              else
                                                goto label_30;
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
                }
                else
                  goto label_21;
            }
        }
    }
  }

  private string FixThai(string text)
  {
    if (string.IsNullOrEmpty(text))
      return (string) null;
    char[] charArray = text.ToCharArray();
    UberText.ThaiGlyphType[] thaiGlyphTypeArray = new UberText.ThaiGlyphType[((IEnumerable<char>) charArray).Count<char>()];
    StringBuilder stringBuilder = new StringBuilder(text);
    for (int index = 0; index < ((IEnumerable<char>) charArray).Count<char>(); ++index)
    {
      char ch = charArray[index];
      if ((int) ch >= 3585 && (int) ch <= 3631 || ((int) ch == 3632 || (int) ch == 3648) || (int) ch == 3649)
        thaiGlyphTypeArray[index] = (int) ch == 3613 || (int) ch == 3615 || ((int) ch == 3611 || (int) ch == 3628) ? UberText.ThaiGlyphType.BASE_ASCENDER : ((int) ch == 3599 || (int) ch == 3598 ? UberText.ThaiGlyphType.BASE_DESCENDER : UberText.ThaiGlyphType.BASE);
      else if ((int) ch >= 3656 && (int) ch <= 3660)
        thaiGlyphTypeArray[index] = UberText.ThaiGlyphType.TONE_MARK;
      else if ((int) ch == 3633 || (int) ch == 3636 || ((int) ch == 3637 || (int) ch == 3638) || ((int) ch == 3639 || (int) ch == 3655 || (int) ch == 3661))
        thaiGlyphTypeArray[index] = UberText.ThaiGlyphType.UPPER;
      else if ((int) ch == 3640 || (int) ch == 3641 || (int) ch == 3642)
        thaiGlyphTypeArray[index] = UberText.ThaiGlyphType.LOWER;
    }
    for (int index = 0; index < ((IEnumerable<char>) charArray).Count<char>(); ++index)
    {
      char ch1 = charArray[index];
      UberText.ThaiGlyphType thaiGlyphType1 = thaiGlyphTypeArray[index];
      stringBuilder[index] = ch1;
      if (index >= 1)
      {
        UberText.ThaiGlyphType thaiGlyphType2 = thaiGlyphTypeArray[index - 1];
        char ch2 = charArray[index - 1];
        if (thaiGlyphType1 == UberText.ThaiGlyphType.UPPER && thaiGlyphType2 == UberText.ThaiGlyphType.BASE_ASCENDER)
        {
          char ch3 = ch1;
          switch (ch3)
          {
            case 'ั':
              stringBuilder[index] = '\xF710';
              continue;
            case 'ิ':
              stringBuilder[index] = '\xF701';
              continue;
            case 'ี':
              stringBuilder[index] = '\xF702';
              continue;
            case 'ึ':
              stringBuilder[index] = '\xF703';
              continue;
            case 'ื':
              stringBuilder[index] = '\xF704';
              continue;
            default:
              if ((int) ch3 != 3655)
              {
                if ((int) ch3 == 3661)
                {
                  stringBuilder[index] = '\xF711';
                  continue;
                }
                continue;
              }
              stringBuilder[index] = '\xF712';
              continue;
          }
        }
        else if (thaiGlyphType1 == UberText.ThaiGlyphType.LOWER && thaiGlyphType2 == UberText.ThaiGlyphType.BASE_DESCENDER)
        {
          stringBuilder[index] = (char) ((uint) ch1 + 59616U);
        }
        else
        {
          if (thaiGlyphType1 == UberText.ThaiGlyphType.LOWER)
          {
            if ((int) ch2 == 3597)
            {
              stringBuilder[index - 1] = '\xF70F';
              continue;
            }
            if ((int) ch2 == 3600)
            {
              stringBuilder[index - 1] = '\xF700';
              continue;
            }
          }
          if (thaiGlyphType1 == UberText.ThaiGlyphType.TONE_MARK)
          {
            if (index - 2 >= 0)
            {
              if (thaiGlyphType2 == UberText.ThaiGlyphType.UPPER && thaiGlyphTypeArray[index - 2] == UberText.ThaiGlyphType.BASE_ASCENDER)
                stringBuilder[index] = (char) ((uint) ch1 + 59595U);
              if (thaiGlyphType2 == UberText.ThaiGlyphType.LOWER && index > 1)
              {
                thaiGlyphType2 = thaiGlyphTypeArray[index - 2];
                char ch3 = charArray[index - 2];
              }
            }
            if (index < ((IEnumerable<char>) charArray).Count<char>() - 1 && ((int) charArray[index + 1] == 3635 || (int) charArray[index + 1] == 3661))
            {
              if (thaiGlyphType2 == UberText.ThaiGlyphType.BASE_ASCENDER)
              {
                stringBuilder[index] = (char) ((uint) ch1 + 59595U);
                stringBuilder.Insert(index + 1, '\xF711');
                stringBuilder.Insert(index + 2, ch1);
                if ((int) charArray[index + 1] == 3635)
                  stringBuilder[index + 1] = 'ำ';
                ++index;
                continue;
              }
            }
            else if (thaiGlyphType2 == UberText.ThaiGlyphType.BASE || thaiGlyphType2 == UberText.ThaiGlyphType.BASE_DESCENDER)
            {
              stringBuilder[index] = (char) ((uint) ch1 + 59586U);
              continue;
            }
            if (thaiGlyphType2 == UberText.ThaiGlyphType.BASE_ASCENDER)
              stringBuilder[index] = (char) ((uint) ch1 + 59581U);
          }
        }
      }
    }
    return stringBuilder.ToString();
  }

  [Serializable]
  public class LocalizationSettings
  {
    public List<UberText.LocalizationSettings.LocaleAdjustment> m_LocaleAdjustments;

    public LocalizationSettings()
    {
      this.m_LocaleAdjustments = new List<UberText.LocalizationSettings.LocaleAdjustment>();
    }

    public bool HasLocale(Locale locale)
    {
      return this.GetLocale(locale) != null;
    }

    public UberText.LocalizationSettings.LocaleAdjustment GetLocale(Locale locale)
    {
      using (List<UberText.LocalizationSettings.LocaleAdjustment>.Enumerator enumerator = this.m_LocaleAdjustments.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          UberText.LocalizationSettings.LocaleAdjustment current = enumerator.Current;
          if (current.m_Locale == locale)
            return current;
        }
      }
      return (UberText.LocalizationSettings.LocaleAdjustment) null;
    }

    public UberText.LocalizationSettings.LocaleAdjustment AddLocale(Locale locale)
    {
      UberText.LocalizationSettings.LocaleAdjustment locale1 = this.GetLocale(locale);
      if (locale1 != null)
        return locale1;
      UberText.LocalizationSettings.LocaleAdjustment localeAdjustment = new UberText.LocalizationSettings.LocaleAdjustment(locale);
      this.m_LocaleAdjustments.Add(localeAdjustment);
      return localeAdjustment;
    }

    public void RemoveLocale(Locale locale)
    {
      for (int index = 0; index < this.m_LocaleAdjustments.Count; ++index)
      {
        if (this.m_LocaleAdjustments[index].m_Locale == locale)
        {
          this.m_LocaleAdjustments.RemoveAt(index);
          break;
        }
      }
    }

    [Serializable]
    public class LocaleAdjustment
    {
      public float m_LineSpaceModifier = 1f;
      public float m_FontSizeModifier = 1f;
      public float m_OutlineModifier = 1f;
      public float m_UnboundCharacterSizeModifier = 1f;
      public float m_ResizeToFitWidthModifier = 1f;
      public Vector3 m_PositionOffset = Vector3.zero;
      public Locale m_Locale;
      public float m_SingleLineAdjustment;
      public float m_Width;
      public float m_Height;
      public float m_UnderwearWidth;
      public float m_UnderwearHeight;

      public LocaleAdjustment()
      {
        this.m_Locale = Locale.enUS;
      }

      public LocaleAdjustment(Locale locale)
      {
        this.m_Locale = locale;
      }
    }
  }

  private class CachedTextKeyData
  {
    public string m_Text;
    public int m_FontSize;
    public float m_CharSize;
    public float m_Width;
    public float m_Height;
    public Font m_Font;
    public float m_LineSpacing;

    public override int GetHashCode()
    {
      return this.m_Text.Length + this.m_FontSize + this.m_Text.GetHashCode() + this.m_FontSize.GetHashCode() - this.m_CharSize.GetHashCode() + this.m_Width.GetHashCode() - this.m_Height.GetHashCode() + this.m_Font.GetHashCode() - this.m_LineSpacing.GetHashCode();
    }
  }

  [Serializable]
  private class CachedTextValues
  {
    public string m_Text;
    public float m_CharSize;
    public int m_OriginalTextHash;
  }

  public enum AlignmentOptions
  {
    Left,
    Center,
    Right,
  }

  public enum AnchorOptions
  {
    Upper,
    Middle,
    Lower,
  }

  private enum TextRenderMaterial
  {
    Text,
    Bold,
    Outline,
    InlineImages,
  }

  private enum Fonts
  {
    BlizzardGlobal,
    Belwe,
    BelweOutline,
    FranklinGothic,
  }

  private enum ThaiGlyphType
  {
    BASE,
    BASE_ASCENDER,
    BASE_DESCENDER,
    TONE_MARK,
    UPPER,
    LOWER,
  }
}
