﻿// Decompiled with JetBrains decompiler
// Type: BnetPresenceMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using bgs.types;
using System;
using System.Collections.Generic;
using System.Linq;

public class BnetPresenceMgr
{
  private Map<BnetAccountId, BnetAccount> m_accounts = new Map<BnetAccountId, BnetAccount>();
  private Map<BnetGameAccountId, BnetGameAccount> m_gameAccounts = new Map<BnetGameAccountId, BnetGameAccount>();
  private Map<BnetAccountId, BnetPlayer> m_players = new Map<BnetAccountId, BnetPlayer>();
  private List<BnetPresenceMgr.PlayersChangedListener> m_playersChangedListeners = new List<BnetPresenceMgr.PlayersChangedListener>();
  private static BnetPresenceMgr s_instance;
  private BnetGameAccountId m_myGameAccountId;
  private BnetPlayer m_myPlayer;

  public event System.Action<PresenceUpdate[]> OnGameAccountPresenceChange;

  public static BnetPresenceMgr Get()
  {
    if (BnetPresenceMgr.s_instance == null)
    {
      BnetPresenceMgr.s_instance = new BnetPresenceMgr();
      if ((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null)
        ApplicationMgr.Get().WillReset += (System.Action) (() =>
        {
          BnetPresenceMgr instance = BnetPresenceMgr.s_instance;
          BnetPresenceMgr.s_instance = new BnetPresenceMgr();
          BnetPresenceMgr.s_instance.m_playersChangedListeners = instance.m_playersChangedListeners;
          BnetPresenceMgr.s_instance.OnGameAccountPresenceChange = instance.OnGameAccountPresenceChange;
        });
      else
        Log.BattleNet.PrintWarning("BnetPresenceMgr.Get(): ApplicationMgr.Get() returned null. Unable to subscribe to ApplicationMgr.WillReset.");
    }
    return BnetPresenceMgr.s_instance;
  }

  public void Initialize()
  {
    Network.Get().SetPresenceHandler(new Network.PresenceHandler(this.OnPresenceUpdate));
    BnetEventMgr.Get().AddChangeListener(new BnetEventMgr.ChangeCallback(this.OnBnetEventOccurred));
    this.m_myGameAccountId = BnetGameAccountId.CreateFromEntityId(BattleNet.GetMyGameAccountId());
  }

  public void Shutdown()
  {
    Network.Get().SetPresenceHandler((Network.PresenceHandler) null);
  }

  public BnetGameAccountId GetMyGameAccountId()
  {
    return this.m_myGameAccountId;
  }

  public BnetPlayer GetMyPlayer()
  {
    return this.m_myPlayer;
  }

  public BnetAccount GetAccount(BnetAccountId id)
  {
    if ((BnetEntityId) id == (BnetEntityId) null)
      return (BnetAccount) null;
    BnetAccount bnetAccount = (BnetAccount) null;
    this.m_accounts.TryGetValue(id, out bnetAccount);
    return bnetAccount;
  }

  public BnetGameAccount GetGameAccount(BnetGameAccountId id)
  {
    if ((BnetEntityId) id == (BnetEntityId) null)
      return (BnetGameAccount) null;
    BnetGameAccount bnetGameAccount = (BnetGameAccount) null;
    this.m_gameAccounts.TryGetValue(id, out bnetGameAccount);
    return bnetGameAccount;
  }

  public BnetPlayer GetPlayer(BnetAccountId id)
  {
    if ((BnetEntityId) id == (BnetEntityId) null)
      return (BnetPlayer) null;
    BnetPlayer bnetPlayer = (BnetPlayer) null;
    this.m_players.TryGetValue(id, out bnetPlayer);
    return bnetPlayer;
  }

  public BnetPlayer GetPlayer(BnetGameAccountId id)
  {
    BnetGameAccount gameAccount = this.GetGameAccount(id);
    if (gameAccount == (BnetGameAccount) null)
      return (BnetPlayer) null;
    return this.GetPlayer(gameAccount.GetOwnerId());
  }

  public BnetPlayer RegisterPlayer(BnetAccountId id)
  {
    BnetPlayer player1 = this.GetPlayer(id);
    if (player1 != null)
      return player1;
    BnetPlayer player2 = new BnetPlayer();
    player2.SetAccountId(id);
    this.m_players[id] = player2;
    BnetPlayerChange change = new BnetPlayerChange();
    change.SetNewPlayer(player2);
    BnetPlayerChangelist changelist = new BnetPlayerChangelist();
    changelist.AddChange(change);
    this.FirePlayersChangedEvent(changelist);
    return player2;
  }

  public bool SetGameField(uint fieldId, bool val)
  {
    if (!Network.ShouldBeConnectedToAurora())
    {
      Error.AddDevFatal("Caller should check for Battle.net connection before calling SetGameField {0}={1}", (object) fieldId, (object) val);
      return false;
    }
    BnetGameAccount hsGameAccount;
    if (!this.ShouldUpdateGameField(fieldId, (object) val, out hsGameAccount))
      return false;
    if ((int) fieldId == 2)
    {
      hsGameAccount.SetBusy(val);
      int num = !val ? 0 : 1;
      BattleNet.SetPresenceInt(fieldId, (long) num);
    }
    else
      BattleNet.SetPresenceBool(fieldId, val);
    BnetPlayerChangelist changelist = this.ChangeGameField(hsGameAccount, fieldId, (object) val);
    switch (fieldId)
    {
      case 2:
        if (val)
        {
          hsGameAccount.SetAway(false);
          break;
        }
        break;
      case 10:
        if (val)
        {
          hsGameAccount.SetBusy(false);
          break;
        }
        break;
    }
    this.FirePlayersChangedEvent(changelist);
    return true;
  }

  public bool SetGameField(uint fieldId, int val)
  {
    if (!Network.ShouldBeConnectedToAurora())
    {
      Error.AddDevFatal("Caller should check for Battle.net connection before calling SetGameField {0}={1}", (object) fieldId, (object) val);
      return false;
    }
    BnetGameAccount hsGameAccount;
    if (!this.ShouldUpdateGameField(fieldId, (object) val, out hsGameAccount))
      return false;
    BattleNet.SetPresenceInt(fieldId, (long) val);
    this.FirePlayersChangedEvent(this.ChangeGameField(hsGameAccount, fieldId, (object) val));
    return true;
  }

  public bool SetGameField(uint fieldId, string val)
  {
    if (!Network.ShouldBeConnectedToAurora())
    {
      Error.AddDevFatal("Caller should check for Battle.net connection before calling SetGameField {0}={1}", (object) fieldId, (object) val);
      return false;
    }
    BnetGameAccount hsGameAccount;
    if (!this.ShouldUpdateGameField(fieldId, (object) val, out hsGameAccount))
      return false;
    BattleNet.SetPresenceString(fieldId, val);
    this.FirePlayersChangedEvent(this.ChangeGameField(hsGameAccount, fieldId, (object) val));
    return true;
  }

  public bool SetGameField(uint fieldId, byte[] val)
  {
    if (!Network.ShouldBeConnectedToAurora())
    {
      Error.AddDevFatal("Caller should check for Battle.net connection before calling SetGameField {0}=[{1}]", (object) fieldId, (object) (val != null ? val.Length.ToString() : string.Empty));
      return false;
    }
    BnetGameAccount hsGameAccount;
    if (!this.ShouldUpdateGameFieldBlob(fieldId, val, out hsGameAccount))
      return false;
    BattleNet.SetPresenceBlob(fieldId, val);
    this.FirePlayersChangedEvent(this.ChangeGameField(hsGameAccount, fieldId, (object) val));
    return true;
  }

  public bool SetGameFieldBlob(uint fieldId, IProtoBuf protoMessage)
  {
    byte[] val = protoMessage != null ? ProtobufUtil.ToByteArray(protoMessage) : (byte[]) null;
    return this.SetGameField(fieldId, val);
  }

  public bool SetRichPresence(Enum[] richPresence)
  {
    if (!Network.ShouldBeConnectedToAurora())
    {
      Error.AddDevFatal("Caller should check for Battle.net connection before calling SetRichPresence {0}", (object) (richPresence != null ? string.Join(", ", ((IEnumerable<Enum>) richPresence).Select<Enum, string>((Func<Enum, string>) (x => x.ToString())).ToArray<string>()) : string.Empty));
      return false;
    }
    if (richPresence == null || richPresence.Length == 0)
      return false;
    RichPresenceUpdate[] updates = new RichPresenceUpdate[richPresence.Length];
    for (int index = 0; index < richPresence.Length; ++index)
    {
      Enum @enum = richPresence[index];
      System.Type type = @enum.GetType();
      FourCC streamId = RichPresence.s_streamIds[type];
      updates[index] = new RichPresenceUpdate()
      {
        presenceFieldIndex = index != 0 ? (ulong) (uint) (458752 + index) : 0UL,
        programId = BnetProgramId.HEARTHSTONE.GetValue(),
        streamId = streamId.GetValue(),
        index = Convert.ToUInt32((object) @enum)
      };
    }
    BattleNet.SetRichPresence(updates);
    return true;
  }

  public bool AddPlayersChangedListener(BnetPresenceMgr.PlayersChangedCallback callback)
  {
    return this.AddPlayersChangedListener(callback, (object) null);
  }

  public bool AddPlayersChangedListener(BnetPresenceMgr.PlayersChangedCallback callback, object userData)
  {
    BnetPresenceMgr.PlayersChangedListener playersChangedListener = new BnetPresenceMgr.PlayersChangedListener();
    playersChangedListener.SetCallback(callback);
    playersChangedListener.SetUserData(userData);
    if (this.m_playersChangedListeners.Contains(playersChangedListener))
      return false;
    this.m_playersChangedListeners.Add(playersChangedListener);
    return true;
  }

  public bool RemovePlayersChangedListener(BnetPresenceMgr.PlayersChangedCallback callback)
  {
    return this.RemovePlayersChangedListener(callback, (object) null);
  }

  public bool RemovePlayersChangedListener(BnetPresenceMgr.PlayersChangedCallback callback, object userData)
  {
    BnetPresenceMgr.PlayersChangedListener playersChangedListener = new BnetPresenceMgr.PlayersChangedListener();
    playersChangedListener.SetCallback(callback);
    playersChangedListener.SetUserData(userData);
    return this.m_playersChangedListeners.Remove(playersChangedListener);
  }

  private void OnPresenceUpdate(PresenceUpdate[] updates)
  {
    BnetPlayerChangelist changelist1 = new BnetPlayerChangelist();
    foreach (PresenceUpdate update1 in ((IEnumerable<PresenceUpdate>) updates).Where<PresenceUpdate>((Func<PresenceUpdate, bool>) (u =>
    {
      if (u.programId == (bgs.FourCC) BnetProgramId.BNET && (int) u.groupId == 2)
        return (int) u.fieldId == 7;
      return false;
    })))
    {
      BnetGameAccountId fromEntityId1 = BnetGameAccountId.CreateFromEntityId(update1.entityId);
      BnetAccountId fromEntityId2 = BnetAccountId.CreateFromEntityId(update1.entityIdVal);
      if (!fromEntityId2.IsEmpty())
      {
        if (this.GetAccount(fromEntityId2) == (BnetAccount) null)
        {
          PresenceUpdate update2 = new PresenceUpdate();
          BnetPlayerChangelist changelist2 = new BnetPlayerChangelist();
          this.CreateAccount(fromEntityId2, update2, changelist2);
        }
        if (!fromEntityId1.IsEmpty() && this.GetGameAccount(fromEntityId1) == (BnetGameAccount) null)
          this.CreateGameAccount(fromEntityId1, update1, changelist1);
      }
    }
    List<PresenceUpdate> presenceUpdateList = (List<PresenceUpdate>) null;
    foreach (PresenceUpdate update in updates)
    {
      if (update.programId == (bgs.FourCC) BnetProgramId.BNET)
      {
        if ((int) update.groupId == 1)
          this.OnAccountUpdate(update, changelist1);
        else if ((int) update.groupId == 2)
          this.OnGameAccountUpdate(update, changelist1);
      }
      else if (update.programId == (bgs.FourCC) BnetProgramId.HEARTHSTONE)
        this.OnGameUpdate(update, changelist1);
      if ((update.programId == (bgs.FourCC) BnetProgramId.HEARTHSTONE || update.programId == (bgs.FourCC) BnetProgramId.BNET && (int) update.groupId == 2) && this.OnGameAccountPresenceChange != null)
      {
        if (presenceUpdateList == null)
          presenceUpdateList = new List<PresenceUpdate>();
        presenceUpdateList.Add(update);
      }
    }
    if (presenceUpdateList != null)
      this.OnGameAccountPresenceChange(presenceUpdateList.ToArray());
    this.FirePlayersChangedEvent(changelist1);
  }

  private void OnBnetEventOccurred(BattleNet.BnetEvent bnetEvent, object userData)
  {
    if (bnetEvent != BattleNet.BnetEvent.Disconnected)
      return;
    using (Map<BnetAccountId, BnetAccount>.ValueCollection.Enumerator enumerator = this.m_accounts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        GeneralUtils.DeepReset<BnetAccount>(enumerator.Current);
    }
    using (Map<BnetGameAccountId, BnetGameAccount>.ValueCollection.Enumerator enumerator = this.m_gameAccounts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        GeneralUtils.DeepReset<BnetGameAccount>(enumerator.Current);
    }
  }

  private void OnAccountUpdate(PresenceUpdate update, BnetPlayerChangelist changelist)
  {
    BnetAccountId fromEntityId = BnetAccountId.CreateFromEntityId(update.entityId);
    BnetAccount account = (BnetAccount) null;
    if (!this.m_accounts.TryGetValue(fromEntityId, out account))
      this.CreateAccount(fromEntityId, update, changelist);
    else
      this.UpdateAccount(account, update, changelist);
  }

  private void CreateAccount(BnetAccountId id, PresenceUpdate update, BnetPlayerChangelist changelist)
  {
    BnetAccount account = new BnetAccount();
    this.m_accounts.Add(id, account);
    account.SetId(id);
    BnetPlayer player = (BnetPlayer) null;
    if (!this.m_players.TryGetValue(id, out player))
    {
      player = new BnetPlayer();
      this.m_players.Add(id, player);
      BnetPlayerChange change = new BnetPlayerChange();
      change.SetNewPlayer(player);
      changelist.AddChange(change);
    }
    player.SetAccount(account);
    this.UpdateAccount(account, update, changelist);
  }

  private void UpdateAccount(BnetAccount account, PresenceUpdate update, BnetPlayerChangelist changelist)
  {
    BnetPlayer player = this.m_players[account.GetId()];
    if ((int) update.fieldId == 7)
    {
      bool boolVal = update.boolVal;
      if (boolVal == account.IsAway())
        return;
      this.AddChangedPlayer(player, changelist);
      account.SetAway(boolVal);
      if (!boolVal)
        return;
      account.SetBusy(false);
    }
    else if ((int) update.fieldId == 8)
    {
      ulong intVal = (ulong) update.intVal;
      if ((long) intVal == (long) account.GetAwayTimeMicrosec())
        return;
      this.AddChangedPlayer(player, changelist);
      account.SetAwayTimeMicrosec(intVal);
    }
    else if ((int) update.fieldId == 11)
    {
      bool boolVal = update.boolVal;
      if (boolVal == account.IsBusy())
        return;
      this.AddChangedPlayer(player, changelist);
      account.SetBusy(boolVal);
      if (!boolVal)
        return;
      account.SetAway(false);
    }
    else if ((int) update.fieldId == 4)
    {
      BnetBattleTag fromString = BnetBattleTag.CreateFromString(update.stringVal);
      if (fromString == account.GetBattleTag())
        return;
      this.AddChangedPlayer(player, changelist);
      account.SetBattleTag(fromString);
    }
    else if ((int) update.fieldId == 1)
    {
      string stringVal = update.stringVal;
      if (stringVal == null)
      {
        Error.AddDevFatal("BnetPresenceMgr.UpdateAccount() - Failed to convert full name to native string for {0}.", (object) account);
      }
      else
      {
        if (stringVal == account.GetFullName())
          return;
        this.AddChangedPlayer(player, changelist);
        account.SetFullName(stringVal);
      }
    }
    else if ((int) update.fieldId == 6)
    {
      ulong intVal = (ulong) update.intVal;
      if ((long) intVal == (long) account.GetLastOnlineMicrosec())
        return;
      this.AddChangedPlayer(player, changelist);
      account.SetLastOnlineMicrosec(intVal);
    }
    else if ((int) update.fieldId == 3)
      ;
  }

  private void OnGameAccountUpdate(PresenceUpdate update, BnetPlayerChangelist changelist)
  {
    BnetGameAccountId fromEntityId = BnetGameAccountId.CreateFromEntityId(update.entityId);
    BnetGameAccount gameAccount = (BnetGameAccount) null;
    if (!this.m_gameAccounts.TryGetValue(fromEntityId, out gameAccount))
      this.CreateGameAccount(fromEntityId, update, changelist);
    else
      this.UpdateGameAccount(gameAccount, update, changelist);
  }

  private void CreateGameAccount(BnetGameAccountId id, PresenceUpdate update, BnetPlayerChangelist changelist)
  {
    BnetGameAccount gameAccount = new BnetGameAccount();
    this.m_gameAccounts.Add(id, gameAccount);
    gameAccount.SetId(id);
    this.UpdateGameAccount(gameAccount, update, changelist);
  }

  private void UpdateGameAccount(BnetGameAccount gameAccount, PresenceUpdate update, BnetPlayerChangelist changelist)
  {
    BnetPlayer player = (BnetPlayer) null;
    BnetAccountId ownerId = gameAccount.GetOwnerId();
    if ((BnetEntityId) ownerId != (BnetEntityId) null)
      this.m_players.TryGetValue(ownerId, out player);
    if ((int) update.fieldId == 2)
    {
      int num = !gameAccount.IsBusy() ? 0 : 1;
      int intVal = (int) update.intVal;
      if (intVal == num)
        return;
      this.AddChangedPlayer(player, changelist);
      bool busy = intVal == 1;
      gameAccount.SetBusy(busy);
      if (busy)
        gameAccount.SetAway(false);
      this.HandleGameAccountChange(player, update);
    }
    else if ((int) update.fieldId == 10)
    {
      bool boolVal = update.boolVal;
      if (boolVal == gameAccount.IsAway())
        return;
      this.AddChangedPlayer(player, changelist);
      gameAccount.SetAway(boolVal);
      if (boolVal)
        gameAccount.SetBusy(false);
      this.HandleGameAccountChange(player, update);
    }
    else if ((int) update.fieldId == 11)
    {
      ulong intVal = (ulong) update.intVal;
      if ((long) intVal == (long) gameAccount.GetAwayTimeMicrosec())
        return;
      this.AddChangedPlayer(player, changelist);
      gameAccount.SetAwayTimeMicrosec(intVal);
      this.HandleGameAccountChange(player, update);
    }
    else if ((int) update.fieldId == 5)
    {
      BnetBattleTag fromString = BnetBattleTag.CreateFromString(update.stringVal);
      if (fromString == gameAccount.GetBattleTag())
        return;
      this.AddChangedPlayer(player, changelist);
      gameAccount.SetBattleTag(fromString);
      this.HandleGameAccountChange(player, update);
    }
    else if ((int) update.fieldId == 1)
    {
      bool boolVal = update.boolVal;
      if (boolVal == gameAccount.IsOnline())
        return;
      this.AddChangedPlayer(player, changelist);
      gameAccount.SetOnline(boolVal);
      this.HandleGameAccountChange(player, update);
    }
    else if ((int) update.fieldId == 3)
    {
      BnetProgramId programId = new BnetProgramId(update.stringVal);
      if ((bgs.FourCC) programId == (bgs.FourCC) gameAccount.GetProgramId())
        return;
      this.AddChangedPlayer(player, changelist);
      gameAccount.SetProgramId(programId);
      this.HandleGameAccountChange(player, update);
    }
    else if ((int) update.fieldId == 4)
    {
      ulong intVal = (ulong) update.intVal;
      if ((long) intVal == (long) gameAccount.GetLastOnlineMicrosec())
        return;
      this.AddChangedPlayer(player, changelist);
      gameAccount.SetLastOnlineMicrosec(intVal);
      this.HandleGameAccountChange(player, update);
    }
    else if ((int) update.fieldId == 7)
    {
      BnetAccountId fromEntityId = BnetAccountId.CreateFromEntityId(update.entityIdVal);
      if ((BnetEntityId) fromEntityId == (BnetEntityId) gameAccount.GetOwnerId())
        return;
      this.UpdateGameAccountOwner(fromEntityId, gameAccount, changelist);
    }
    else if ((int) update.fieldId == 9)
    {
      if (!update.valCleared || gameAccount.GetRichPresence() == null)
        return;
      this.AddChangedPlayer(player, changelist);
      gameAccount.SetRichPresence((string) null);
      this.HandleGameAccountChange(player, update);
    }
    else
    {
      if ((int) update.fieldId != 1000)
        return;
      string richPresence = update.stringVal ?? string.Empty;
      if (richPresence == gameAccount.GetRichPresence())
        return;
      this.AddChangedPlayer(player, changelist);
      gameAccount.SetRichPresence(richPresence);
      this.HandleGameAccountChange(player, update);
    }
  }

  private void UpdateGameAccountOwner(BnetAccountId ownerId, BnetGameAccount gameAccount, BnetPlayerChangelist changelist)
  {
    BnetPlayer player1 = (BnetPlayer) null;
    BnetAccountId ownerId1 = gameAccount.GetOwnerId();
    if ((BnetEntityId) ownerId1 != (BnetEntityId) null && this.m_players.TryGetValue(ownerId1, out player1))
    {
      player1.RemoveGameAccount(gameAccount.GetId());
      this.AddChangedPlayer(player1, changelist);
    }
    BnetPlayer player2 = (BnetPlayer) null;
    if (this.m_players.TryGetValue(ownerId, out player2))
    {
      this.AddChangedPlayer(player2, changelist);
    }
    else
    {
      player2 = new BnetPlayer();
      this.m_players.Add(ownerId, player2);
      BnetPlayerChange change = new BnetPlayerChange();
      change.SetNewPlayer(player2);
      changelist.AddChange(change);
    }
    gameAccount.SetOwnerId(ownerId);
    player2.AddGameAccount(gameAccount);
    this.CacheMyself(gameAccount, player2);
  }

  private void HandleGameAccountChange(BnetPlayer player, PresenceUpdate update)
  {
    if (player == null)
      return;
    player.OnGameAccountChanged(update.fieldId);
  }

  private void OnGameUpdate(PresenceUpdate update, BnetPlayerChangelist changelist)
  {
    BnetGameAccountId fromEntityId = BnetGameAccountId.CreateFromEntityId(update.entityId);
    BnetGameAccount gameAccount = (BnetGameAccount) null;
    if (!this.m_gameAccounts.TryGetValue(fromEntityId, out gameAccount))
      this.CreateGameInfo(fromEntityId, update, changelist);
    else
      this.UpdateGameInfo(gameAccount, update, changelist);
  }

  private void CreateGameInfo(BnetGameAccountId id, PresenceUpdate update, BnetPlayerChangelist changelist)
  {
    BnetGameAccount gameAccount = new BnetGameAccount();
    this.m_gameAccounts.Add(id, gameAccount);
    gameAccount.SetId(id);
    this.UpdateGameInfo(gameAccount, update, changelist);
  }

  private void UpdateGameInfo(BnetGameAccount gameAccount, PresenceUpdate update, BnetPlayerChangelist changelist)
  {
    BnetPlayer player = (BnetPlayer) null;
    BnetAccountId ownerId = gameAccount.GetOwnerId();
    if ((BnetEntityId) ownerId != (BnetEntityId) null)
      this.m_players.TryGetValue(ownerId, out player);
    if (update.valCleared)
    {
      if (!gameAccount.HasGameField(update.fieldId))
        return;
      this.AddChangedPlayer(player, changelist);
      gameAccount.RemoveGameField(update.fieldId);
      this.HandleGameAccountChange(player, update);
    }
    else
    {
      switch (update.fieldId)
      {
        case 1:
          if (update.boolVal == gameAccount.GetGameFieldBool(update.fieldId))
            break;
          this.AddChangedPlayer(player, changelist);
          gameAccount.SetGameField(update.fieldId, (object) update.boolVal);
          this.HandleGameAccountChange(player, update);
          break;
        case 2:
        case 4:
        case 19:
        case 20:
          if (update.stringVal == gameAccount.GetGameFieldString(update.fieldId))
            break;
          this.AddChangedPlayer(player, changelist);
          gameAccount.SetGameField(update.fieldId, (object) update.stringVal);
          this.HandleGameAccountChange(player, update);
          break;
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
          if ((int) update.intVal == gameAccount.GetGameFieldInt(update.fieldId))
            break;
          this.AddChangedPlayer(player, changelist);
          gameAccount.SetGameField(update.fieldId, (object) (int) update.intVal);
          this.HandleGameAccountChange(player, update);
          break;
        case 17:
        case 18:
        case 21:
        case 22:
          if (GeneralUtils.AreBytesEqual(update.blobVal, gameAccount.GetGameFieldBytes(update.fieldId)))
            break;
          this.AddChangedPlayer(player, changelist);
          gameAccount.SetGameField(update.fieldId, (object) update.blobVal);
          this.HandleGameAccountChange(player, update);
          break;
      }
    }
  }

  private void CacheMyself(BnetGameAccount gameAccount, BnetPlayer player)
  {
    if (player == this.m_myPlayer || (BnetEntityId) gameAccount.GetId() != (BnetEntityId) this.m_myGameAccountId)
      return;
    this.m_myPlayer = player;
  }

  private void AddChangedPlayer(BnetPlayer player, BnetPlayerChangelist changelist)
  {
    if (player == null || changelist.HasChange(player))
      return;
    BnetPlayerChange change = new BnetPlayerChange();
    change.SetOldPlayer(player.Clone());
    change.SetNewPlayer(player);
    changelist.AddChange(change);
  }

  private void FirePlayersChangedEvent(BnetPlayerChangelist changelist)
  {
    if (changelist == null || changelist.GetChanges().Count == 0)
      return;
    foreach (BnetPresenceMgr.PlayersChangedListener playersChangedListener in this.m_playersChangedListeners.ToArray())
      playersChangedListener.Fire(changelist);
  }

  private bool ShouldUpdateGameField(uint fieldId, object val, out BnetGameAccount hsGameAccount)
  {
    hsGameAccount = (BnetGameAccount) null;
    if (this.m_myPlayer == null)
      return true;
    hsGameAccount = this.m_myPlayer.GetHearthstoneGameAccount();
    if (hsGameAccount == (BnetGameAccount) null)
      return true;
    if (hsGameAccount.HasGameField(fieldId))
    {
      object gameField = hsGameAccount.GetGameField(fieldId);
      if (val == null)
      {
        if (gameField == null)
          return false;
      }
      else if (val.Equals(gameField))
        return false;
    }
    else if (val == null)
      return false;
    return true;
  }

  private bool ShouldUpdateGameFieldBlob(uint fieldId, byte[] val, out BnetGameAccount hsGameAccount)
  {
    hsGameAccount = (BnetGameAccount) null;
    if (this.m_myPlayer == null)
      return true;
    hsGameAccount = this.m_myPlayer.GetHearthstoneGameAccount();
    if (hsGameAccount == (BnetGameAccount) null)
      return true;
    if (hsGameAccount.HasGameField(fieldId))
    {
      byte[] gameFieldBytes = hsGameAccount.GetGameFieldBytes(fieldId);
      if (GeneralUtils.AreArraysEqual<byte>(val, gameFieldBytes))
        return false;
    }
    else if (val == null)
      return false;
    return true;
  }

  private BnetPlayerChangelist ChangeGameField(BnetGameAccount hsGameAccount, uint fieldId, object val)
  {
    if (hsGameAccount == (BnetGameAccount) null)
      return (BnetPlayerChangelist) null;
    BnetPlayerChange change = new BnetPlayerChange();
    change.SetOldPlayer(this.m_myPlayer.Clone());
    change.SetNewPlayer(this.m_myPlayer);
    hsGameAccount.SetGameField(fieldId, val);
    BnetPlayerChangelist playerChangelist = new BnetPlayerChangelist();
    playerChangelist.AddChange(change);
    return playerChangelist;
  }

  private class PlayersChangedListener : EventListener<BnetPresenceMgr.PlayersChangedCallback>
  {
    public void Fire(BnetPlayerChangelist changelist)
    {
      this.m_callback(changelist, this.m_userData);
    }
  }

  public delegate void PlayersChangedCallback(BnetPlayerChangelist changelist, object userData);
}
