﻿// Decompiled with JetBrains decompiler
// Type: ZoneWeapon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class ZoneWeapon : Zone
{
  private List<Card> m_destroyedWeapons = new List<Card>();
  private const float INTERMEDIATE_Y_OFFSET = 1.5f;
  private const float INTERMEDIATE_TRANSITION_SEC = 0.9f;
  private const float DESTROYED_WEAPON_WAIT_SEC = 1.75f;
  private const float FINAL_TRANSITION_SEC = 0.1f;

  public override string ToString()
  {
    return string.Format("{0} (Weapon)", (object) base.ToString());
  }

  public override bool CanAcceptTags(int controllerId, TAG_ZONE zoneTag, TAG_CARDTYPE cardType)
  {
    return base.CanAcceptTags(controllerId, zoneTag, cardType) && cardType == TAG_CARDTYPE.WEAPON;
  }

  public override int RemoveCard(Card card)
  {
    int num = base.RemoveCard(card);
    if (num >= 0 && !this.m_destroyedWeapons.Contains(card))
      this.m_destroyedWeapons.Add(card);
    return num;
  }

  public override void UpdateLayout()
  {
    if (GameState.Get().IsMulliganManagerActive())
    {
      this.UpdateLayoutFinished();
    }
    else
    {
      this.m_updatingLayout = true;
      if (this.IsBlockingLayout())
        this.UpdateLayoutFinished();
      else if (this.m_cards.Count == 0)
      {
        this.m_destroyedWeapons.Clear();
        this.UpdateLayoutFinished();
      }
      else
        this.StartCoroutine(this.UpdateLayoutImpl());
    }
  }

  [DebuggerHidden]
  private IEnumerator UpdateLayoutImpl()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ZoneWeapon.\u003CUpdateLayoutImpl\u003Ec__IteratorEE() { \u003C\u003Ef__this = this };
  }
}
