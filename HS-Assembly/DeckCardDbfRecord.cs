﻿// Decompiled with JetBrains decompiler
// Type: DeckCardDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DeckCardDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_NextCard;
  [SerializeField]
  private int m_CardId;
  [SerializeField]
  private int m_DeckId;
  [SerializeField]
  private DbfLocValue m_Description;

  [DbfField("NEXT_CARD", "DECK_CARD.ID of next card in deck, or NULL to terminate")]
  public int NextCard
  {
    get
    {
      return this.m_NextCard;
    }
  }

  [DbfField("CARD_ID", "ASSET.CARD.ID")]
  public int CardId
  {
    get
    {
      return this.m_CardId;
    }
  }

  [DbfField("DECK_ID", "DECK.ID of deck")]
  public int DeckId
  {
    get
    {
      return this.m_DeckId;
    }
  }

  [DbfField("DESCRIPTION", "")]
  public DbfLocValue Description
  {
    get
    {
      return this.m_Description;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    DeckCardDbfAsset deckCardDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (DeckCardDbfAsset)) as DeckCardDbfAsset;
    if ((UnityEngine.Object) deckCardDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("DeckCardDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < deckCardDbfAsset.Records.Count; ++index)
      deckCardDbfAsset.Records[index].StripUnusedLocales();
    records = (object) deckCardDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_Description.StripUnusedLocales();
  }

  public void SetNextCard(int v)
  {
    this.m_NextCard = v;
  }

  public void SetCardId(int v)
  {
    this.m_CardId = v;
  }

  public void SetDeckId(int v)
  {
    this.m_DeckId = v;
  }

  public void SetDescription(DbfLocValue v)
  {
    this.m_Description = v;
    v.SetDebugInfo(this.ID, "DESCRIPTION");
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckCardDbfRecord.\u003C\u003Ef__switch\u0024map29 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckCardDbfRecord.\u003C\u003Ef__switch\u0024map29 = new Dictionary<string, int>(5)
        {
          {
            "ID",
            0
          },
          {
            "NEXT_CARD",
            1
          },
          {
            "CARD_ID",
            2
          },
          {
            "DECK_ID",
            3
          },
          {
            "DESCRIPTION",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckCardDbfRecord.\u003C\u003Ef__switch\u0024map29.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.NextCard;
          case 2:
            return (object) this.CardId;
          case 3:
            return (object) this.DeckId;
          case 4:
            return (object) this.Description;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (DeckCardDbfRecord.\u003C\u003Ef__switch\u0024map2A == null)
    {
      // ISSUE: reference to a compiler-generated field
      DeckCardDbfRecord.\u003C\u003Ef__switch\u0024map2A = new Dictionary<string, int>(5)
      {
        {
          "ID",
          0
        },
        {
          "NEXT_CARD",
          1
        },
        {
          "CARD_ID",
          2
        },
        {
          "DECK_ID",
          3
        },
        {
          "DESCRIPTION",
          4
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!DeckCardDbfRecord.\u003C\u003Ef__switch\u0024map2A.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetNextCard((int) val);
        break;
      case 2:
        this.SetCardId((int) val);
        break;
      case 3:
        this.SetDeckId((int) val);
        break;
      case 4:
        this.SetDescription((DbfLocValue) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DeckCardDbfRecord.\u003C\u003Ef__switch\u0024map2B == null)
      {
        // ISSUE: reference to a compiler-generated field
        DeckCardDbfRecord.\u003C\u003Ef__switch\u0024map2B = new Dictionary<string, int>(5)
        {
          {
            "ID",
            0
          },
          {
            "NEXT_CARD",
            1
          },
          {
            "CARD_ID",
            2
          },
          {
            "DECK_ID",
            3
          },
          {
            "DESCRIPTION",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DeckCardDbfRecord.\u003C\u003Ef__switch\u0024map2B.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (int);
          case 3:
            return typeof (int);
          case 4:
            return typeof (DbfLocValue);
        }
      }
    }
    return (System.Type) null;
  }
}
