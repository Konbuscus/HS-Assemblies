﻿// Decompiled with JetBrains decompiler
// Type: AdventureWingKarazhanHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (AdventureWing))]
[CustomEditClass]
public class AdventureWingKarazhanHelper : MonoBehaviour
{
  public List<AdventureWingKarazhanHelper.WingSpecificObject> m_WingSpecificObjects = new List<AdventureWingKarazhanHelper.WingSpecificObject>();
  public List<MeshRenderer> m_backgroundRenderers = new List<MeshRenderer>();
  public List<Animator> m_adventureCompleteAnimators = new List<Animator>();
  public PlayMakerFSM m_doorOpenPlayMakerFSM;
  private AdventureWing m_adventureWing;
  private GameObject m_objectForThisWing;
  private float m_backgroundOffsetForThisWing;

  public void Initialize()
  {
    this.m_adventureWing = this.GetComponent<AdventureWing>();
    if ((UnityEngine.Object) this.m_adventureWing == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "AdventureWingKarazhanHelper could not find an AdventureWing component on the same GameObject!");
    }
    else
    {
      WingDbId wingId = this.m_adventureWing.GetWingId();
      for (int index = 0; index < this.m_WingSpecificObjects.Count; ++index)
      {
        AdventureWingKarazhanHelper.WingSpecificObject wingSpecificObject = this.m_WingSpecificObjects[index];
        wingSpecificObject.m_ObjectSpecificToWing.SetActive(false);
        if (wingSpecificObject.m_wingDbId == wingId)
        {
          this.m_objectForThisWing = wingSpecificObject.m_ObjectSpecificToWing;
          using (List<MeshRenderer>.Enumerator enumerator = this.m_backgroundRenderers.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              Material material = enumerator.Current.material;
              Vector2 textureOffset = material.GetTextureOffset("_MainTex");
              textureOffset.y = wingSpecificObject.m_backgroundOffset;
              material.SetTextureOffset("_MainTex", textureOffset);
            }
          }
        }
      }
      if ((UnityEngine.Object) this.m_objectForThisWing == (UnityEngine.Object) null)
      {
        Debug.LogError((object) "AdventureWingKarazhanHelper could not find an object for m_objectForThisWing!");
      }
      else
      {
        this.m_objectForThisWing.SetActive(true);
        PegUIElement componentInChildren = this.m_objectForThisWing.GetComponentInChildren<PegUIElement>();
        if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
        {
          Debug.LogError((object) "AdventureWingKarazhanHelper could not find the unlock button!");
        }
        else
        {
          this.m_adventureWing.m_UnlockButton = componentInChildren;
          foreach (PlayMakerFSM componentsInChild in this.m_adventureWing.m_WingEventTable.GetComponentsInChildren<PlayMakerFSM>())
            componentsInChild.FsmVariables.GetFsmGameObject("KnockerRootVar").Value = componentInChildren.gameObject;
          this.m_doorOpenPlayMakerFSM.FsmVariables.GetFsmGameObject("KnockerHeadVar").Value = this.m_objectForThisWing;
          AdventureConfig adventureConfig = AdventureConfig.Get();
          if (!AdventureProgressMgr.Get().IsAdventureModeComplete(adventureConfig.GetSelectedAdventure(), adventureConfig.GetSelectedMode()))
            return;
          using (List<Animator>.Enumerator enumerator = this.m_adventureCompleteAnimators.GetEnumerator())
          {
            while (enumerator.MoveNext())
              enumerator.Current.enabled = true;
          }
        }
      }
    }
  }

  [Serializable]
  public class WingSpecificObject
  {
    public WingDbId m_wingDbId;
    public GameObject m_ObjectSpecificToWing;
    public float m_backgroundOffset;
  }
}
