﻿// Decompiled with JetBrains decompiler
// Type: DisenchantButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DisenchantButton : CraftingButton
{
  private string m_lastwarnedCard;

  public override void EnableButton()
  {
    if (CraftingManager.Get().GetNumClientTransactions() > 0)
    {
      this.EnterUndoMode();
    }
    else
    {
      this.labelText.Text = GameStrings.Get("GLUE_CRAFTING_DISENCHANT");
      base.EnableButton();
    }
  }

  protected override void OnRelease()
  {
    if (CraftingManager.Get().GetPendingServerTransaction() != null)
      return;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.GetComponent<Animation>().Play("CardExchange_ButtonPress1_phone");
    else
      this.GetComponent<Animation>().Play("CardExchange_ButtonPress1");
    if (CraftingManager.Get().GetNumClientTransactions() > 0)
      this.DoDisenchant();
    else
      CollectionManager.Get().RequestDeckContentsForDecksWithoutContentsLoaded(new CollectionManager.DelOnAllDeckContents(this.OnReadyToStartDisenchant));
  }

  private void OnReadyToStartDisenchant()
  {
    if (!CraftingManager.Get().IsCardShowing())
      return;
    List<string> invalidDeckNames = this.GetPostDisenchantInvalidDeckNames();
    if (invalidDeckNames.Count == 0)
    {
      EntityDef entityDef = CraftingManager.Get().GetShownActor().GetEntityDef();
      string cardId = entityDef.GetCardId();
      int num = CraftingManager.Get().GetNumOwnedIncludePending(TAG_PREMIUM.GOLDEN) + CraftingManager.Get().GetNumOwnedIncludePending(TAG_PREMIUM.NORMAL);
      if (CraftingManager.Get().GetNumClientTransactions() <= 0 && this.m_lastwarnedCard != cardId && (!entityDef.IsElite() && num <= 2 || entityDef.IsElite() && num <= 1))
      {
        AlertPopup.PopupInfo info = new AlertPopup.PopupInfo();
        info.m_headerText = GameStrings.Get("GLUE_CRAFTING_DISENCHANT_CONFIRM_HEADER");
        info.m_text = GameStrings.Get("GLUE_CRAFTING_DISENCHANT_CONFIRM2_DESC");
        info.m_showAlertIcon = true;
        info.m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL;
        info.m_responseCallback = new AlertPopup.ResponseCallback(this.OnConfirmDisenchantResponse);
        this.m_lastwarnedCard = cardId;
        DialogManager.Get().ShowPopup(info);
      }
      else
        this.DoDisenchant();
    }
    else
    {
      string str = GameStrings.Get("GLUE_CRAFTING_DISENCHANT_CONFIRM_DESC");
      using (List<string>.Enumerator enumerator = invalidDeckNames.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          str = str + "\n" + current;
        }
      }
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_headerText = GameStrings.Get("GLUE_CRAFTING_DISENCHANT_CONFIRM_HEADER"),
        m_text = str,
        m_showAlertIcon = false,
        m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
        m_responseCallback = new AlertPopup.ResponseCallback(this.OnConfirmDisenchantResponse)
      });
    }
  }

  private void OnConfirmDisenchantResponse(AlertPopup.Response response, object userData)
  {
    if (response == AlertPopup.Response.CANCEL)
      return;
    this.DoDisenchant();
  }

  private void DoDisenchant()
  {
    CraftingManager.Get().DisenchantButtonPressed();
  }

  private List<string> GetPostDisenchantInvalidDeckNames()
  {
    Actor shownActor = CraftingManager.Get().GetShownActor();
    string cardId = shownActor.GetEntityDef().GetCardId();
    TAG_PREMIUM premium = shownActor.GetPremium();
    int num = Mathf.Max(0, CraftingManager.Get().GetNumOwnedIncludePending() - 1);
    SortedDictionary<long, CollectionDeck> decks = CollectionManager.Get().GetDecks();
    List<string> stringList = new List<string>();
    using (SortedDictionary<long, CollectionDeck>.ValueCollection.Enumerator enumerator = decks.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeck current = enumerator.Current;
        if (current.GetCardCountFirstMatchingSlot(cardId, premium) > num && !current.Locked)
        {
          stringList.Add(current.Name);
          Log.Rachelle.Print(string.Format("Disenchanting will invalidate deck '{0}'", (object) current.Name));
        }
      }
    }
    return stringList;
  }
}
