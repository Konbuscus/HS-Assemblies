﻿// Decompiled with JetBrains decompiler
// Type: CameraMask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class CameraMask : MonoBehaviour
{
  [CustomEditField(Sections = "Mask Settings")]
  public float m_Width = 1f;
  [CustomEditField(Sections = "Mask Settings")]
  public float m_Height = 1f;
  [CustomEditField(Sections = "Render Camera")]
  public List<GameLayer> m_CullingMasks = new List<GameLayer>() { GameLayer.Default, GameLayer.IgnoreFullScreenEffects };
  [CustomEditField(Sections = "Mask Settings")]
  public GameObject m_ClipObjects;
  [CustomEditField(Sections = "Mask Settings")]
  public CameraMask.CAMERA_MASK_UP_VECTOR m_UpVector;
  [CustomEditField(Sections = "Mask Settings")]
  public bool m_RealtimeUpdate;
  [CustomEditField(Sections = "Render Camera")]
  public bool m_UseCameraFromLayer;
  [CustomEditField(Parent = "m_UseCameraFromLayer", Sections = "Render Camera")]
  public GameLayer m_CameraFromLayer;
  private Camera m_RenderCamera;
  private Camera m_MaskCamera;
  private GameObject m_MaskCameraGameObject;

  private void Update()
  {
    if (!this.m_RealtimeUpdate)
      return;
    this.UpdateCameraClipping();
  }

  private void OnDisable()
  {
    if ((Object) this.m_MaskCamera != (Object) null && (Object) UniversalInputManager.Get() != (Object) null)
      UniversalInputManager.Get().RemoveCameraMaskCamera(this.m_MaskCamera);
    if ((Object) this.m_MaskCameraGameObject != (Object) null)
      Object.Destroy((Object) this.m_MaskCameraGameObject);
    this.m_MaskCamera = (Camera) null;
  }

  private void OnEnable()
  {
    this.Init();
  }

  private void OnDrawGizmos()
  {
    Matrix4x4 matrix4x4 = new Matrix4x4();
    if (this.m_UpVector == CameraMask.CAMERA_MASK_UP_VECTOR.Z)
      matrix4x4.SetTRS(this.transform.position, Quaternion.identity, this.transform.lossyScale);
    else
      matrix4x4.SetTRS(this.transform.position, Quaternion.Euler(90f, 0.0f, 0.0f), this.transform.lossyScale);
    Gizmos.matrix = matrix4x4;
    Gizmos.color = Color.magenta;
    Gizmos.DrawWireCube(Vector3.zero, new Vector3(this.m_Width, this.m_Height, 0.0f));
    Gizmos.matrix = Matrix4x4.identity;
  }

  [ContextMenu("UpdateMask")]
  public void UpdateMask()
  {
    this.UpdateCameraClipping();
  }

  private bool Init()
  {
    if ((Object) this.m_MaskCamera != (Object) null)
      return false;
    if ((Object) this.m_MaskCameraGameObject != (Object) null)
      Object.Destroy((Object) this.m_MaskCameraGameObject);
    this.m_RenderCamera = !this.m_UseCameraFromLayer ? Camera.main : CameraUtils.FindFirstByLayer(this.m_CameraFromLayer);
    if ((Object) this.m_RenderCamera == (Object) null)
      return false;
    this.m_MaskCameraGameObject = new GameObject("MaskCamera");
    SceneUtils.SetLayer(this.m_MaskCameraGameObject, GameLayer.CameraMask);
    this.m_MaskCameraGameObject.transform.parent = this.m_RenderCamera.gameObject.transform;
    this.m_MaskCameraGameObject.transform.localPosition = Vector3.zero;
    this.m_MaskCameraGameObject.transform.localRotation = Quaternion.identity;
    this.m_MaskCameraGameObject.transform.localScale = Vector3.one;
    int num = GameLayer.CameraMask.LayerBit();
    using (List<GameLayer>.Enumerator enumerator = this.m_CullingMasks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameLayer current = enumerator.Current;
        num |= current.LayerBit();
      }
    }
    this.m_MaskCamera = this.m_MaskCameraGameObject.AddComponent<Camera>();
    this.m_MaskCamera.CopyFrom(this.m_RenderCamera);
    this.m_MaskCamera.clearFlags = CameraClearFlags.Nothing;
    this.m_MaskCamera.cullingMask = num;
    this.m_MaskCamera.depth = this.m_RenderCamera.depth + 1f;
    if ((Object) this.m_ClipObjects == (Object) null)
      this.m_ClipObjects = this.gameObject;
    foreach (Component componentsInChild in this.m_ClipObjects.GetComponentsInChildren<Transform>())
    {
      GameObject gameObject = componentsInChild.gameObject;
      if (!((Object) gameObject == (Object) null))
        SceneUtils.SetLayer(gameObject, GameLayer.CameraMask);
    }
    this.UpdateCameraClipping();
    UniversalInputManager.Get().AddCameraMaskCamera(this.m_MaskCamera);
    return true;
  }

  private void UpdateCameraClipping()
  {
    if ((Object) this.m_RenderCamera == (Object) null && !this.Init())
      return;
    Vector3 position1 = Vector3.zero;
    Vector3 position2 = Vector3.zero;
    if (this.m_UpVector == CameraMask.CAMERA_MASK_UP_VECTOR.Y)
    {
      position1 = new Vector3(this.transform.position.x - this.m_Width * 0.5f * this.transform.lossyScale.x, this.transform.position.y, this.transform.position.z - this.m_Height * 0.5f * this.transform.lossyScale.z);
      position2 = new Vector3(this.transform.position.x + this.m_Width * 0.5f * this.transform.lossyScale.x, this.transform.position.y, this.transform.position.z + this.m_Height * 0.5f * this.transform.lossyScale.z);
    }
    else
    {
      position1 = new Vector3(this.transform.position.x - this.m_Width * 0.5f * this.transform.lossyScale.x, this.transform.position.y - this.m_Height * 0.5f * this.transform.lossyScale.y, this.transform.position.z);
      position2 = new Vector3(this.transform.position.x + this.m_Width * 0.5f * this.transform.lossyScale.x, this.transform.position.y + this.m_Height * 0.5f * this.transform.lossyScale.y, this.transform.position.z);
    }
    Vector3 viewportPoint1 = this.m_RenderCamera.WorldToViewportPoint(position1);
    Vector3 viewportPoint2 = this.m_RenderCamera.WorldToViewportPoint(position2);
    if ((double) viewportPoint1.x < 0.0 && (double) viewportPoint2.x < 0.0)
    {
      if (!this.m_MaskCamera.enabled)
        return;
      this.m_MaskCamera.enabled = false;
    }
    else if ((double) viewportPoint1.x > 1.0 && (double) viewportPoint2.x > 1.0)
    {
      if (!this.m_MaskCamera.enabled)
        return;
      this.m_MaskCamera.enabled = false;
    }
    else if ((double) viewportPoint1.y < 0.0 && (double) viewportPoint2.y < 0.0)
    {
      if (!this.m_MaskCamera.enabled)
        return;
      this.m_MaskCamera.enabled = false;
    }
    else if ((double) viewportPoint1.y > 1.0 && (double) viewportPoint2.y > 1.0)
    {
      if (!this.m_MaskCamera.enabled)
        return;
      this.m_MaskCamera.enabled = false;
    }
    else
    {
      if (!this.m_MaskCamera.enabled)
        this.m_MaskCamera.enabled = true;
      Rect rect = new Rect(viewportPoint1.x, viewportPoint1.y, viewportPoint2.x - viewportPoint1.x, viewportPoint2.y - viewportPoint1.y);
      if ((double) rect.x < 0.0)
      {
        rect.width += rect.x;
        rect.x = 0.0f;
      }
      if ((double) rect.y < 0.0)
      {
        rect.height += rect.y;
        rect.y = 0.0f;
      }
      if ((double) rect.x > 1.0)
      {
        rect.width -= rect.x;
        rect.x = 1f;
      }
      if ((double) rect.y > 1.0)
      {
        rect.height -= rect.y;
        rect.y = 1f;
      }
      rect.width = Mathf.Min(1f - rect.x, rect.width);
      rect.height = Mathf.Min(1f - rect.y, rect.height);
      this.m_MaskCamera.rect = new Rect(0.0f, 0.0f, 1f, 1f);
      this.m_MaskCamera.ResetProjectionMatrix();
      Matrix4x4 projectionMatrix = this.m_MaskCamera.projectionMatrix;
      this.m_MaskCamera.rect = rect;
      this.m_MaskCamera.projectionMatrix = Matrix4x4.TRS(new Vector3((float) (-(double) rect.x * 2.0) / rect.width, (float) (-(double) rect.y * 2.0) / rect.height, 0.0f), Quaternion.identity, Vector3.one) * Matrix4x4.TRS(new Vector3((float) (1.0 / (double) rect.width - 1.0), (float) (1.0 / (double) rect.height - 1.0), 0.0f), Quaternion.identity, new Vector3(1f / rect.width, 1f / rect.height, 1f)) * projectionMatrix;
    }
  }

  public enum CAMERA_MASK_UP_VECTOR
  {
    Y,
    Z,
  }
}
