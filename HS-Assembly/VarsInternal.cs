﻿// Decompiled with JetBrains decompiler
// Type: VarsInternal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

internal class VarsInternal
{
  private static VarsInternal s_instance = new VarsInternal();
  private Map<string, string> m_vars = new Map<string, string>();

  private VarsInternal()
  {
    if (!this.LoadConfig(Vars.GetClientConfigPath()))
      ;
  }

  public static VarsInternal Get()
  {
    return VarsInternal.s_instance;
  }

  public static void RefreshVars()
  {
    VarsInternal.s_instance = new VarsInternal();
  }

  public bool Contains(string key)
  {
    return this.m_vars.ContainsKey(key);
  }

  public string Value(string key)
  {
    return this.m_vars[key];
  }

  public void Set(string key, string value)
  {
    this.m_vars[key] = value;
  }

  private bool LoadConfig(string path)
  {
    ConfigFile configFile = new ConfigFile();
    if (!configFile.LightLoad(path))
      return false;
    using (List<ConfigFile.Line>.Enumerator enumerator = configFile.GetLines().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConfigFile.Line current = enumerator.Current;
        this.m_vars[current.m_fullKey] = current.m_value;
      }
    }
    return true;
  }
}
