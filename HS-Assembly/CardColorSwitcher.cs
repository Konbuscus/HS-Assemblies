﻿// Decompiled with JetBrains decompiler
// Type: CardColorSwitcher
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardColorSwitcher : MonoBehaviour
{
  private static CardColorSwitcher s_instance;
  public Texture[] spellCardTextures;
  public Texture[] minionCardTextures;

  private void Awake()
  {
    CardColorSwitcher.s_instance = this;
  }

  private void OnDestroy()
  {
    CardColorSwitcher.s_instance = (CardColorSwitcher) null;
  }

  public static CardColorSwitcher Get()
  {
    return CardColorSwitcher.s_instance;
  }

  public Texture GetMinionTexture(CardColorSwitcher.CardColorType colorType)
  {
    int index = (int) colorType;
    if (this.minionCardTextures.Length <= index)
      return (Texture) null;
    if ((Object) this.minionCardTextures[index] == (Object) null)
      return (Texture) null;
    return this.minionCardTextures[index];
  }

  public Texture GetSpellTexture(CardColorSwitcher.CardColorType colorType)
  {
    int index = (int) colorType;
    if (this.spellCardTextures.Length <= index)
      return (Texture) null;
    if ((Object) this.spellCardTextures[index] == (Object) null)
      return (Texture) null;
    return this.spellCardTextures[index];
  }

  public enum CardColorType
  {
    TYPE_GENERIC,
    TYPE_WARLOCK,
    TYPE_ROGUE,
    TYPE_DRUID,
    TYPE_SHAMAN,
    TYPE_HUNTER,
    TYPE_MAGE,
    TYPE_PALADIN,
    TYPE_PRIEST,
    TYPE_WARRIOR,
  }
}
