﻿// Decompiled with JetBrains decompiler
// Type: Crush
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Crush : Spell
{
  public MinionPieces m_minionPieces;
  public Material m_premiumTauntMaterial;
  public Material m_premiumEliteMaterial;
  public UberText m_attack;
  public UberText m_health;

  protected override void OnAction(SpellStateType prevStateType)
  {
    base.OnAction(prevStateType);
    Entity entity = this.GetSourceCard().GetEntity();
    Actor componentInParents = SceneUtils.FindComponentInParents<Actor>((Component) this);
    GameObject go = this.m_minionPieces.m_main;
    bool flag = entity.HasTag(GAME_TAG.PREMIUM);
    if (flag)
    {
      go = this.m_minionPieces.m_premium;
      SceneUtils.EnableRenderers(this.m_minionPieces.m_main, false);
    }
    GameObject portraitMesh = componentInParents.GetPortraitMesh();
    go.GetComponent<Renderer>().material = portraitMesh.GetComponent<Renderer>().sharedMaterial;
    go.SetActive(true);
    SceneUtils.EnableRenderers(go, true);
    if (entity.HasTaunt())
    {
      if (flag)
        this.m_minionPieces.m_taunt.GetComponent<Renderer>().material = this.m_premiumTauntMaterial;
      this.m_minionPieces.m_taunt.SetActive(true);
      SceneUtils.EnableRenderers(this.m_minionPieces.m_taunt, true);
    }
    if (entity.IsElite())
    {
      if (flag)
        this.m_minionPieces.m_legendary.GetComponent<Renderer>().material = this.m_premiumEliteMaterial;
      this.m_minionPieces.m_legendary.SetActive(true);
      SceneUtils.EnableRenderers(this.m_minionPieces.m_legendary, true);
    }
    this.m_attack.SetGameStringText(entity.GetATK().ToString());
    this.m_health.SetGameStringText(entity.GetHealth().ToString());
  }
}
