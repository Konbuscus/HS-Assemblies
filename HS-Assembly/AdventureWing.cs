﻿// Decompiled with JetBrains decompiler
// Type: AdventureWing
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusUtil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class AdventureWing : MonoBehaviour
{
  private static List<int> s_LastRandomNumbers = new List<int>();
  [CustomEditField(Sections = "UI")]
  public List<UberText> m_WingTitles = new List<UberText>();
  [CustomEditField(ListTable = true, Sections = "Random Background Properties")]
  public List<AdventureWing.BackgroundRandomization> m_BackgroundRenderers = new List<AdventureWing.BackgroundRandomization>();
  [CustomEditField(Sections = "Random Background Properties")]
  public List<float> m_BackgroundOffsets = new List<float>();
  [CustomEditField(Sections = "Special UI/LOE")]
  public float m_UnlockButtonHighlightIntensityOut = 1.52f;
  [CustomEditField(Sections = "Special UI/LOE")]
  public float m_UnlockButtonHighlightIntensityOver = 2f;
  [SerializeField]
  private float m_CoinSpacing = 25f;
  [SerializeField]
  private Vector3 m_CoinsOffset = Vector3.zero;
  [SerializeField]
  private Vector3 m_CoinsChestOffset = Vector3.zero;
  private List<AdventureWing.Boss> m_BossCoins = new List<AdventureWing.Boss>();
  private List<AdventureWing.BossSelected> m_BossSelectedListeners = new List<AdventureWing.BossSelected>();
  private List<AdventureWing.OpenPlateStart> m_OpenPlateStartListeners = new List<AdventureWing.OpenPlateStart>();
  private List<AdventureWing.OpenPlateEnd> m_OpenPlateEndListeners = new List<AdventureWing.OpenPlateEnd>();
  private List<AdventureWing.ShowCardRewards> m_ShowCardRewardsListeners = new List<AdventureWing.ShowCardRewards>();
  private List<AdventureWing.HideCardRewards> m_HideCardRewardsListeners = new List<AdventureWing.HideCardRewards>();
  private List<AdventureWing.ShowRewardsPreview> m_ShowRewardsPreviewListeners = new List<AdventureWing.ShowRewardsPreview>();
  private List<AdventureWing.TryPurchaseWing> m_TryPurchaseWingListeners = new List<AdventureWing.TryPurchaseWing>();
  [CustomEditField(Sections = "Wing Event Table")]
  public AdventureWingEventTable m_WingEventTable;
  [CustomEditField(Sections = "Containers & Bones")]
  public GameObject m_ContentsContainer;
  [CustomEditField(Sections = "Containers & Bones")]
  public GameObject m_CoinContainer;
  [CustomEditField(Sections = "Containers & Bones")]
  public GameObject m_WallAccentContainer;
  [CustomEditField(Sections = "Containers & Bones")]
  public GameObject m_PlateAccentContainer;
  [CustomEditField(Sections = "Lock Plate")]
  public GameObject m_LockPlate;
  [CustomEditField(Sections = "Lock Plate")]
  public GameObject m_LockPlateFXContainer;
  [CustomEditField(Sections = "UI")]
  public PegUIElement m_UnlockButton;
  [CustomEditField(Sections = "UI")]
  public PegUIElement m_BuyButton;
  [CustomEditField(Sections = "UI")]
  public MeshRenderer m_BuyButtonMesh;
  [CustomEditField(Sections = "UI")]
  public UberText m_BuyButtonText;
  [CustomEditField(Sections = "UI")]
  public UberText m_ReleaseLabelText;
  [CustomEditField(Sections = "UI")]
  public PegUIElement m_RewardsPreviewButton;
  [CustomEditField(Sections = "UI")]
  public GameObject m_PurchasedBanner;
  [CustomEditField(Sections = "Wing Rewards")]
  public PegUIElement m_BigChest;
  [CustomEditField(Sections = "Special UI")]
  public bool m_BuyButtonOnOppositeSideOfKey;
  [CustomEditField(Sections = "Special UI/LOE")]
  public MeshRenderer m_UnlockButtonHighlightMesh_LOE;
  [CustomEditField(Sections = "Special UI/KARA")]
  public PlayMakerFSM m_prologueLoadingPlayMakerFSM_KARA;
  private AdventureWingDef m_WingDef;
  private AdventureWingDef m_DependsOnWingDef;
  private Spell m_UnlockSpell;
  private GameObject m_WallAccentObject;
  private GameObject m_PlateAccentObject;
  private AdventureWing.BringToFocusCallback m_BringToFocusCallback;
  private bool m_Owned;
  private bool m_Playable;
  private bool m_Locked;
  private bool m_EventStartDetected;

  public float CoinSpacing
  {
    get
    {
      return this.m_CoinSpacing;
    }
    set
    {
      this.m_CoinSpacing = value;
      this.UpdateCoinPositions();
    }
  }

  public Vector3 CoinsOffset
  {
    get
    {
      return this.m_CoinsOffset;
    }
    set
    {
      this.m_CoinsOffset = value;
      this.UpdateCoinPositions();
    }
  }

  public Vector3 CoinsChestOffset
  {
    get
    {
      return this.m_CoinsChestOffset;
    }
    set
    {
      this.m_CoinsChestOffset = value;
      this.UpdateCoinPositions();
    }
  }

  private void Awake()
  {
    if (!((UnityEngine.Object) this.m_BigChest != (UnityEngine.Object) null))
      return;
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_BigChest.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ShowBigChestRewards));
    }
    else
    {
      this.m_BigChest.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.ShowBigChestRewards));
      this.m_BigChest.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.HideBigChestRewards));
    }
  }

  private void OnDestroy()
  {
    if (StoreManager.Get() == null)
      return;
    StoreManager.Get().RemoveStatusChangedListener(new StoreManager.StatusChangedCallback(this.UpdateBuyButton));
  }

  private void Update()
  {
    if (!this.m_ContentsContainer.activeSelf && !AdventureScene.Get().IsDevMode)
    {
      if (!this.m_EventStartDetected && AdventureProgressMgr.Get().IsWingOpen((int) this.m_WingDef.GetWingId()))
        this.UpdatePlateState();
      if (!this.m_Owned && AdventureProgressMgr.Get().OwnsWing((int) this.m_WingDef.GetWingId()))
        this.UpdatePlateState();
    }
    if (!AdventureScene.Get().IsDevMode)
      return;
    if (Input.GetKeyDown(KeyCode.Alpha1))
    {
      this.m_ContentsContainer.SetActive(false);
      this.m_LockPlate.SetActive(true);
      this.SetPlateKeyEvent(true);
    }
    if (Input.GetKeyDown(KeyCode.Alpha2))
    {
      this.m_ContentsContainer.SetActive(false);
      this.m_LockPlate.SetActive(true);
      this.m_WingEventTable.PlateBuy(true);
    }
    if (Input.GetKeyDown(KeyCode.Alpha3))
    {
      this.m_ContentsContainer.SetActive(false);
      this.m_LockPlate.SetActive(true);
      this.m_WingEventTable.PlateInitialText();
    }
    if (Input.GetKeyDown(KeyCode.Alpha4))
      this.m_WingEventTable.PlateDeactivate();
    if (Input.GetKeyDown(KeyCode.Alpha5))
    {
      this.m_ContentsContainer.SetActive(false);
      this.m_LockPlate.SetActive(true);
      if ((UnityEngine.Object) this.m_UnlockSpell == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.Log((object) "Cheat case is invalid - no unlock spell is defined for this AdventureWing!");
      }
      else
      {
        AdventureWingUnlockSpell component = this.m_UnlockSpell.GetComponent<AdventureWingUnlockSpell>();
        this.m_WingEventTable.PlateOpen(!((UnityEngine.Object) component != (UnityEngine.Object) null) ? 0.0f : component.m_UnlockDelay);
      }
    }
    if (!Input.GetKeyDown(KeyCode.Alpha6))
      return;
    using (List<AdventureWing.Boss>.Enumerator enumerator = this.m_BossCoins.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.m_Chest.SlamInCheckmark();
    }
  }

  public void Initialize(AdventureWingDef wingDef, AdventureWingDef dependsOnWingDef)
  {
    this.m_WingDef = wingDef;
    this.m_DependsOnWingDef = dependsOnWingDef;
    GameUtils.SetAutomationName(this.gameObject, (object) wingDef.GetWingId());
    using (List<UberText>.Enumerator enumerator = this.m_WingTitles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UberText current = enumerator.Current;
        if ((UnityEngine.Object) current != (UnityEngine.Object) null)
          current.Text = this.m_WingDef.GetWingName();
      }
    }
    if (!string.IsNullOrEmpty(wingDef.m_UnlockSpellPrefab) && (UnityEngine.Object) this.m_LockPlateFXContainer != (UnityEngine.Object) null)
    {
      this.m_UnlockSpell = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(wingDef.m_UnlockSpellPrefab), true, false).GetComponent<Spell>();
      GameUtils.SetParent((Component) this.m_UnlockSpell, this.m_LockPlateFXContainer, false);
      this.m_UnlockSpell.gameObject.SetActive(false);
    }
    this.SetAccent(wingDef.m_AccentPrefab);
    this.m_Owned = AdventureProgressMgr.Get().OwnsWing((int) wingDef.GetWingId());
    this.m_EventStartDetected = AdventureProgressMgr.Get().IsWingOpen((int) wingDef.GetWingId());
    this.m_Playable = this.m_Owned && this.m_EventStartDetected;
    this.m_Locked = AdventureProgressMgr.Get().IsWingLocked(wingDef);
    this.UpdatePurchasedBanner();
    bool flag1 = AdventureConfig.Get().GetSelectedMode() == AdventureModeDbId.HEROIC;
    int ack;
    AdventureProgressMgr.Get().GetWingAck((int) this.m_WingDef.GetWingId(), out ack);
    bool flag2 = ack > 0;
    AdventureWingKarazhanHelper component = this.GetComponent<AdventureWingKarazhanHelper>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.Initialize();
    if (AdventureScene.Get().IsDevMode)
    {
      int devModeSetting = AdventureScene.Get().DevModeSetting;
      this.m_WingEventTable.PlateActivate();
      if (devModeSetting == 1)
        this.SetPlateKeyEvent(true);
      else if (devModeSetting == 2)
        this.m_WingEventTable.PlateInitialText();
      if ((UnityEngine.Object) this.m_ReleaseLabelText != (UnityEngine.Object) null)
        this.m_ReleaseLabelText.Text = this.m_WingDef.GetComingSoonLabel();
    }
    else if (this.m_Playable && (flag2 || flag1))
    {
      this.m_WingEventTable.PlateDeactivate();
    }
    else
    {
      this.UpdateBuyButton(StoreManager.Get().IsOpen(), (object) null);
      StoreManager.Get().RegisterStatusChangedListener(new StoreManager.StatusChangedCallback(this.UpdateBuyButton));
      this.m_WingEventTable.PlateActivate();
      if (this.m_Locked && this.m_EventStartDetected)
      {
        this.m_WingEventTable.PlateInitialText();
        if ((UnityEngine.Object) this.m_ReleaseLabelText != (UnityEngine.Object) null)
          this.m_ReleaseLabelText.Text = !this.m_Owned ? GameStrings.Get(this.m_WingDef.m_LockedPurchaseLocString) : GameStrings.Get(this.m_WingDef.m_LockedLocString);
      }
      else
      {
        bool flag3 = (UnityEngine.Object) dependsOnWingDef == (UnityEngine.Object) null || AdventureProgressMgr.Get().OwnsWing((int) dependsOnWingDef.GetWingId());
        if (!this.m_EventStartDetected)
        {
          this.m_WingEventTable.PlateInitialText();
          if ((UnityEngine.Object) this.m_ReleaseLabelText != (UnityEngine.Object) null)
            this.m_ReleaseLabelText.Text = this.m_WingDef.GetComingSoonLabel();
        }
        else if (!this.m_Owned && flag3)
        {
          if ((UnityEngine.Object) this.m_prologueLoadingPlayMakerFSM_KARA != (UnityEngine.Object) null)
            this.m_prologueLoadingPlayMakerFSM_KARA.SendEvent("on");
          this.m_WingEventTable.PlateBuy(true);
        }
        else if (this.m_Owned && ack == 0)
        {
          this.SetPlateKeyEvent(true);
        }
        else
        {
          this.m_WingEventTable.PlateInitialText();
          if ((UnityEngine.Object) this.m_ReleaseLabelText != (UnityEngine.Object) null)
            this.m_ReleaseLabelText.Text = this.m_WingDef.GetRequiresLabel();
        }
      }
    }
    if ((UnityEngine.Object) this.m_UnlockButton == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) "Adventure Wing's Unlock Button is not hooked up!");
    }
    else
    {
      this.m_UnlockButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.UnlockButtonPressed));
      this.m_UnlockButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnUnlockButtonOut));
      this.m_UnlockButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnUnlockButtonOver));
    }
    if ((UnityEngine.Object) this.m_BuyButton != (UnityEngine.Object) null)
      this.m_BuyButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e =>
      {
        if (AdventureScene.Get().IsDevMode)
          this.SetPlateKeyEvent(false);
        else
          this.FireTryPurchaseWingEvent();
      }));
    if (!((UnityEngine.Object) this.m_RewardsPreviewButton != (UnityEngine.Object) null))
      return;
    this.m_RewardsPreviewButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.FireShowRewardsPreviewEvent()));
  }

  public AdventureWingDef GetWingDef()
  {
    return this.m_WingDef;
  }

  public WingDbId GetWingId()
  {
    return this.m_WingDef.GetWingId();
  }

  public List<AdventureRewardsChest> GetChests()
  {
    List<AdventureRewardsChest> adventureRewardsChestList = new List<AdventureRewardsChest>();
    using (List<AdventureWing.Boss>.Enumerator enumerator = this.m_BossCoins.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureWing.Boss current = enumerator.Current;
        adventureRewardsChestList.Add(current.m_Chest);
      }
    }
    return adventureRewardsChestList;
  }

  public AdventureDbId GetAdventureId()
  {
    return this.m_WingDef.GetAdventureId();
  }

  public ProductType GetProductType()
  {
    return StoreManager.GetAdventureProductType(this.GetAdventureId());
  }

  public int GetProductData()
  {
    return (int) this.GetWingId();
  }

  public string GetWingName()
  {
    return this.m_WingDef.GetWingName();
  }

  public void AddBossSelectedListener(AdventureWing.BossSelected dlg)
  {
    this.m_BossSelectedListeners.Add(dlg);
  }

  public void AddOpenPlateStartListener(AdventureWing.OpenPlateStart dlg)
  {
    this.m_OpenPlateStartListeners.Add(dlg);
  }

  public void AddOpenPlateEndListener(AdventureWing.OpenPlateEnd dlg)
  {
    this.m_OpenPlateEndListeners.Add(dlg);
  }

  public void AddShowCardRewardsListener(AdventureWing.ShowCardRewards dlg)
  {
    this.m_ShowCardRewardsListeners.Add(dlg);
  }

  public void AddHideCardRewardsListener(AdventureWing.HideCardRewards dlg)
  {
    this.m_HideCardRewardsListeners.Add(dlg);
  }

  public void AddShowRewardsPreviewListeners(AdventureWing.ShowRewardsPreview dlg)
  {
    this.m_ShowRewardsPreviewListeners.Add(dlg);
  }

  public void AddTryPurchaseWingListener(AdventureWing.TryPurchaseWing dlg)
  {
    this.m_TryPurchaseWingListeners.Add(dlg);
  }

  public AdventureBossCoin CreateBoss(string coinPrefab, string rewardsPrefab, ScenarioDbId mission, bool enabled)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AdventureWing.\u003CCreateBoss\u003Ec__AnonStorey35E bossCAnonStorey35E = new AdventureWing.\u003CCreateBoss\u003Ec__AnonStorey35E();
    // ISSUE: reference to a compiler-generated field
    bossCAnonStorey35E.mission = mission;
    // ISSUE: reference to a compiler-generated field
    bossCAnonStorey35E.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    bossCAnonStorey35E.newcoin = GameUtils.LoadGameObjectWithComponent<AdventureBossCoin>(coinPrefab);
    // ISSUE: reference to a compiler-generated field
    bossCAnonStorey35E.newchest = GameUtils.LoadGameObjectWithComponent<AdventureRewardsChest>(rewardsPrefab);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    GameUtils.SetAutomationName(bossCAnonStorey35E.newcoin.gameObject, (object) bossCAnonStorey35E.mission);
    // ISSUE: reference to a compiler-generated field
    if ((UnityEngine.Object) bossCAnonStorey35E.newchest != (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      GameUtils.SetAutomationName(bossCAnonStorey35E.newchest.gameObject, (object) bossCAnonStorey35E.mission);
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      this.UpdateBossChest(bossCAnonStorey35E.newchest, bossCAnonStorey35E.mission);
    }
    if ((UnityEngine.Object) this.m_CoinContainer != (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated field
      GameUtils.SetParent((Component) bossCAnonStorey35E.newcoin, this.m_CoinContainer, false);
      // ISSUE: reference to a compiler-generated field
      if ((UnityEngine.Object) bossCAnonStorey35E.newchest != (UnityEngine.Object) null)
      {
        // ISSUE: reference to a compiler-generated field
        GameUtils.SetParent((Component) bossCAnonStorey35E.newchest, this.m_CoinContainer, false);
        // ISSUE: reference to a compiler-generated field
        TransformUtil.SetLocalPosY((Component) bossCAnonStorey35E.newchest.transform, 0.01f);
      }
    }
    // ISSUE: reference to a compiler-generated field
    bossCAnonStorey35E.newcoin.Enable(enabled, false);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    bossCAnonStorey35E.newcoin.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(bossCAnonStorey35E.\u003C\u003Em__39));
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      // ISSUE: reference to a compiler-generated field
      bossCAnonStorey35E.newchest.Enable(false);
      // ISSUE: reference to a compiler-generated field
      if ((UnityEngine.Object) bossCAnonStorey35E.newcoin.m_DisabledCollider != (UnityEngine.Object) null)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        bossCAnonStorey35E.newcoin.m_DisabledCollider.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(bossCAnonStorey35E.\u003C\u003Em__3A));
      }
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      bossCAnonStorey35E.newchest.AddChestEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(bossCAnonStorey35E.\u003C\u003Em__3B));
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      bossCAnonStorey35E.newchest.AddChestEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(bossCAnonStorey35E.\u003C\u003Em__3C));
    }
    if (this.m_BossCoins.Count == 0)
    {
      // ISSUE: reference to a compiler-generated field
      bossCAnonStorey35E.newcoin.ShowConnector(false);
    }
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    this.m_BossCoins.Add(new AdventureWing.Boss()
    {
      m_MissionId = bossCAnonStorey35E.mission,
      m_Coin = bossCAnonStorey35E.newcoin,
      m_Chest = bossCAnonStorey35E.newchest
    });
    this.UpdateCoinPositions();
    // ISSUE: reference to a compiler-generated field
    return bossCAnonStorey35E.newcoin;
  }

  public void SetAccent(string accentPrefab)
  {
    if ((UnityEngine.Object) this.m_WallAccentObject != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_WallAccentObject);
    if ((UnityEngine.Object) this.m_PlateAccentObject != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_PlateAccentObject);
    if (string.IsNullOrEmpty(accentPrefab))
      return;
    if ((UnityEngine.Object) this.m_WallAccentContainer != (UnityEngine.Object) null)
    {
      this.m_WallAccentObject = AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(accentPrefab), true, false);
      GameUtils.SetParent(this.m_WallAccentObject, this.m_WallAccentContainer, false);
    }
    if (!((UnityEngine.Object) this.m_PlateAccentContainer != (UnityEngine.Object) null))
      return;
    this.m_PlateAccentObject = UnityEngine.Object.Instantiate<GameObject>(this.m_WallAccentObject);
    GameUtils.SetParent(this.m_PlateAccentObject, this.m_PlateAccentContainer, false);
  }

  public void SetBringToFocusCallback(AdventureWing.BringToFocusCallback dlg)
  {
    this.m_BringToFocusCallback = dlg;
  }

  public void OpenBigChest()
  {
    this.m_WingEventTable.BigChestOpen();
    if (!((UnityEngine.Object) this.m_BigChest != (UnityEngine.Object) null))
      return;
    this.m_BigChest.RemoveEventListener(UIEventType.PRESS, new UIEvent.Handler(this.ShowBigChestRewards));
    this.m_BigChest.RemoveEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.ShowBigChestRewards));
    this.m_BigChest.RemoveEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.HideBigChestRewards));
  }

  public void HideBigChest()
  {
    this.m_WingEventTable.BigChestCover();
  }

  public void BigChestStayOpen()
  {
    this.m_WingEventTable.BigChestStayOpen();
  }

  public void SetBigChestRewards(WingDbId wingId)
  {
    if (AdventureConfig.Get().GetSelectedMode() != AdventureModeDbId.NORMAL)
      return;
    HashSet<RewardVisualTiming> rewardTimings = new HashSet<RewardVisualTiming>()
    {
      RewardVisualTiming.ADVENTURE_CHEST
    };
    List<CardRewardData> cardRewardsForWing = AdventureProgressMgr.Get().GetCardRewardsForWing((int) wingId, rewardTimings);
    if (!((UnityEngine.Object) this.m_BigChest != (UnityEngine.Object) null))
      return;
    this.m_BigChest.SetData((object) cardRewardsForWing);
  }

  public List<CardRewardData> GetBigChestRewards()
  {
    if ((UnityEngine.Object) this.m_BigChest != (UnityEngine.Object) null)
      return (List<CardRewardData>) this.m_BigChest.GetData();
    return (List<CardRewardData>) null;
  }

  public bool HasBigChestRewards()
  {
    if ((UnityEngine.Object) this.m_BigChest != (UnityEngine.Object) null)
      return this.m_BigChest.GetData() != null;
    return false;
  }

  public bool UpdateAndAnimateCoinsAndChests(float startDelay, bool forceCoinAnimation, AdventureWing.DelOnCoinAnimateCallback dlg)
  {
    if (this.m_WingEventTable.IsPlateInOrGoingToAnActiveState())
      return false;
    List<AdventureWing.Boss> thingsToFlip = new List<AdventureWing.Boss>();
    AdventureConfig adventureConfig = AdventureConfig.Get();
    List<KeyValuePair<int, int>> keyValuePairList = new List<KeyValuePair<int, int>>();
    using (List<AdventureWing.Boss>.Enumerator enumerator = this.m_BossCoins.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureWing.Boss current = enumerator.Current;
        int wingId = 0;
        int missionReqProgress = 0;
        bool reqs = adventureConfig.IsMissionNewlyAvailableAndGetReqs((int) current.m_MissionId, ref wingId, ref missionReqProgress);
        if ((forceCoinAnimation || reqs) && (!forceCoinAnimation || AdventureProgressMgr.Get().CanPlayScenario((int) current.m_MissionId)))
        {
          keyValuePairList.Add(new KeyValuePair<int, int>(wingId, missionReqProgress));
          AdventureWing.Boss boss = new AdventureWing.Boss();
          boss.m_MissionId = current.m_MissionId;
          boss.m_Coin = current.m_Coin;
          if (AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario((int) current.m_MissionId).Count > 0)
            boss.m_Chest = current.m_Chest;
          thingsToFlip.Add(boss);
        }
      }
    }
    using (List<KeyValuePair<int, int>>.Enumerator enumerator = keyValuePairList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, int> current = enumerator.Current;
        adventureConfig.SetWingAckIfGreater(current.Key, current.Value);
      }
    }
    if (thingsToFlip.Count <= 0)
      return false;
    this.StartCoroutine(this.AnimateCoinsAndChests(thingsToFlip, startDelay, dlg));
    return true;
  }

  public void UpdatePlateState()
  {
    this.UpdatePurchasedBanner();
    bool flag1 = AdventureProgressMgr.Get().IsWingLocked(this.m_WingDef);
    bool flag2 = AdventureProgressMgr.Get().OwnsWing((int) this.m_WingDef.GetWingId());
    bool flag3 = AdventureProgressMgr.Get().IsWingOpen((int) this.m_WingDef.GetWingId());
    bool flag4 = flag2 && flag3;
    if (flag4 && this.m_Playable && (!flag1 && !this.m_Locked))
      return;
    if (flag4 && !flag1 && !this.m_WingEventTable.IsPlateKey())
    {
      if (this.m_BuyButtonOnOppositeSideOfKey && !this.m_WingEventTable.IsPlateBuy())
        this.m_WingEventTable.PlateBuy(false);
      if ((UnityEngine.Object) this.m_prologueLoadingPlayMakerFSM_KARA != (UnityEngine.Object) null)
        this.m_prologueLoadingPlayMakerFSM_KARA.SendEvent("off");
      this.SetPlateKeyEvent(false);
    }
    else if (!this.m_WingEventTable.IsPlateBuy())
    {
      if (flag1 && flag3)
      {
        this.m_WingEventTable.PlateInitialText();
        if ((UnityEngine.Object) this.m_ReleaseLabelText != (UnityEngine.Object) null)
          this.m_ReleaseLabelText.Text = !flag2 ? GameStrings.Get(this.m_WingDef.m_LockedPurchaseLocString) : GameStrings.Get(this.m_WingDef.m_LockedLocString);
      }
      else
      {
        bool flag5 = (UnityEngine.Object) this.m_DependsOnWingDef == (UnityEngine.Object) null || AdventureProgressMgr.Get().OwnsWing((int) this.m_DependsOnWingDef.GetWingId());
        if (!flag3)
        {
          this.m_WingEventTable.PlateInitialText();
          if ((UnityEngine.Object) this.m_ReleaseLabelText != (UnityEngine.Object) null)
            this.m_ReleaseLabelText.Text = this.m_WingDef.GetComingSoonLabel();
        }
        else if (!flag2 && flag5)
        {
          this.m_WingEventTable.PlateBuy(false);
        }
        else
        {
          this.m_WingEventTable.PlateInitialText();
          if ((UnityEngine.Object) this.m_ReleaseLabelText != (UnityEngine.Object) null)
            this.m_ReleaseLabelText.Text = this.m_WingDef.GetRequiresLabel();
        }
      }
    }
    this.m_EventStartDetected = flag3;
    this.m_Playable = flag4;
    this.m_Owned = flag2;
    this.m_Locked = flag1;
  }

  public void UpdateRewardsPreviewCover()
  {
    if (this.HasRewards())
      return;
    this.m_WingEventTable.PlateCoverPreviewChest();
  }

  public bool HasRewards()
  {
    List<CardRewardData> bigChestRewards = this.GetBigChestRewards();
    if (bigChestRewards != null && bigChestRewards.Count > 0)
      return true;
    using (List<AdventureWing.Boss>.Enumerator enumerator = this.m_BossCoins.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario((int) enumerator.Current.m_MissionId).Count > 0)
          return true;
      }
    }
    return false;
  }

  public void RandomizeBackground()
  {
    if (this.m_BackgroundOffsets.Count == 0)
      return;
    int index;
    do
    {
      index = UnityEngine.Random.Range(0, this.m_BackgroundOffsets.Count);
    }
    while (AdventureWing.s_LastRandomNumbers.Contains(index));
    AdventureWing.s_LastRandomNumbers.Add(index);
    if (AdventureWing.s_LastRandomNumbers.Count >= this.m_BackgroundOffsets.Count)
      AdventureWing.s_LastRandomNumbers.RemoveAt(0);
    using (List<AdventureWing.BackgroundRandomization>.Enumerator enumerator = this.m_BackgroundRenderers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureWing.BackgroundRandomization current = enumerator.Current;
        if (!((UnityEngine.Object) current.m_backgroundRenderer == (UnityEngine.Object) null) && !string.IsNullOrEmpty(current.m_materialTextureName))
        {
          Material material = current.m_backgroundRenderer.materials[0];
          Vector2 textureOffset = material.GetTextureOffset(current.m_materialTextureName);
          textureOffset.y = this.m_BackgroundOffsets[index];
          material.SetTextureOffset(current.m_materialTextureName, textureOffset);
        }
      }
    }
  }

  public void BringToFocus()
  {
    if (this.m_BringToFocusCallback == null)
      return;
    this.m_BringToFocusCallback();
  }

  public void HideBossChests()
  {
    using (List<AdventureWing.Boss>.Enumerator enumerator = this.m_BossCoins.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureWing.Boss current = enumerator.Current;
        if ((UnityEngine.Object) current.m_Chest != (UnityEngine.Object) null)
          current.m_Chest.FadeOutChestImmediate();
      }
    }
  }

  public void NavigateBackCleanup()
  {
    if (!((UnityEngine.Object) this.m_prologueLoadingPlayMakerFSM_KARA != (UnityEngine.Object) null))
      return;
    this.m_prologueLoadingPlayMakerFSM_KARA.SendEvent("cancel");
  }

  [DebuggerHidden]
  private IEnumerator AnimateCoinsAndChests(List<AdventureWing.Boss> thingsToFlip, float delaySeconds, AdventureWing.DelOnCoinAnimateCallback dlg)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureWing.\u003CAnimateCoinsAndChests\u003Ec__IteratorD()
    {
      delaySeconds = delaySeconds,
      dlg = dlg,
      thingsToFlip = thingsToFlip,
      \u003C\u0024\u003EdelaySeconds = delaySeconds,
      \u003C\u0024\u003Edlg = dlg,
      \u003C\u0024\u003EthingsToFlip = thingsToFlip,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator AnimateOneCoinAndChest(AdventureWing.Boss boss)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureWing.\u003CAnimateOneCoinAndChest\u003Ec__IteratorE()
    {
      boss = boss,
      \u003C\u0024\u003Eboss = boss
    };
  }

  private void UpdateCoinPositions()
  {
    int num = 0;
    using (List<AdventureWing.Boss>.Enumerator enumerator = this.m_BossCoins.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AdventureWing.Boss current = enumerator.Current;
        current.m_Coin.transform.localPosition = this.m_CoinsOffset;
        TransformUtil.SetLocalPosX((Component) current.m_Coin, this.m_CoinsOffset.x + (float) num * this.m_CoinSpacing);
        if ((UnityEngine.Object) current.m_Chest != (UnityEngine.Object) null)
        {
          current.m_Chest.transform.localPosition = this.m_CoinsOffset;
          TransformUtil.SetLocalPosX((Component) current.m_Chest, this.m_CoinsOffset.x + (float) num * this.m_CoinSpacing);
          current.m_Chest.transform.localPosition += this.m_CoinsChestOffset;
        }
        ++num;
      }
    }
  }

  private void FireBossSelectedEvent(AdventureBossCoin coin, ScenarioDbId mission)
  {
    foreach (AdventureWing.BossSelected bossSelected in this.m_BossSelectedListeners.ToArray())
      bossSelected(coin, mission);
  }

  private void FireOpenPlateStartEvent()
  {
    foreach (AdventureWing.OpenPlateStart openPlateStart in this.m_OpenPlateStartListeners.ToArray())
      openPlateStart(this);
  }

  private void FireOpenPlateEndEvent(Spell s)
  {
    if ((UnityEngine.Object) this.m_UnlockSpell != (UnityEngine.Object) null)
      this.m_UnlockSpell.gameObject.SetActive(false);
    foreach (AdventureWing.OpenPlateEnd openPlateEnd in this.m_OpenPlateEndListeners.ToArray())
      openPlateEnd(this);
  }

  private void OnUnlockButtonOut(UIEvent e)
  {
    if ((UnityEngine.Object) this.m_UnlockButtonHighlightMesh_LOE == (UnityEngine.Object) null)
      return;
    this.m_UnlockButtonHighlightMesh_LOE.material.SetFloat("_Intensity", this.m_UnlockButtonHighlightIntensityOut);
  }

  private void OnUnlockButtonOver(UIEvent e)
  {
    if ((UnityEngine.Object) this.m_UnlockButtonHighlightMesh_LOE == (UnityEngine.Object) null)
      return;
    this.m_UnlockButtonHighlightMesh_LOE.material.SetFloat("_Intensity", this.m_UnlockButtonHighlightIntensityOver);
  }

  private void UnlockButtonPressed(UIEvent e)
  {
    if (!string.IsNullOrEmpty(this.m_WingDef.GetOpeningNotRecommendedWarning()) && !this.IsWingRecommendedToOpen())
      DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
      {
        m_text = this.m_WingDef.GetOpeningNotRecommendedWarning(),
        m_showAlertIcon = true,
        m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
        m_responseCallback = new AlertPopup.ResponseCallback(this.OnConfirmWingUnlockResponse)
      });
    else
      this.UnlockPlate();
  }

  private void OnConfirmWingUnlockResponse(AlertPopup.Response response, object userData)
  {
    if (response != AlertPopup.Response.CONFIRM)
      return;
    this.UnlockPlate();
  }

  private void UnlockPlate()
  {
    this.m_UnlockButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.UnlockButtonPressed));
    float startDelay = 0.0f;
    if (this.m_BringToFocusCallback != null)
    {
      startDelay = 0.5f;
      this.m_BringToFocusCallback();
    }
    this.StartCoroutine(this.DoUnlockPlate(startDelay));
  }

  [DebuggerHidden]
  private IEnumerator DoUnlockPlate(float startDelay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureWing.\u003CDoUnlockPlate\u003Ec__IteratorF()
    {
      startDelay = startDelay,
      \u003C\u0024\u003EstartDelay = startDelay,
      \u003C\u003Ef__this = this
    };
  }

  private void OnUnlockSpellFinished(Spell spell, object userData)
  {
    if (!((UnityEngine.Object) this.m_WingDef != (UnityEngine.Object) null) || string.IsNullOrEmpty(this.m_WingDef.m_OpenQuoteVOLine))
      return;
    this.StartCoroutine(this.PlayOpenQuoteAfterDelay());
  }

  [DebuggerHidden]
  private IEnumerator PlayOpenQuoteAfterDelay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureWing.\u003CPlayOpenQuoteAfterDelay\u003Ec__Iterator10()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ShowBigChestRewards(UIEvent e)
  {
    List<CardRewardData> bigChestRewards = this.GetBigChestRewards();
    if (bigChestRewards == null)
      return;
    this.FireShowCardRewardsEvent(bigChestRewards, this.m_BigChest.transform.position);
  }

  private void HideBigChestRewards(UIEvent e)
  {
    List<CardRewardData> bigChestRewards = this.GetBigChestRewards();
    if (bigChestRewards == null)
      return;
    this.FireHideCardRewardsEvent(bigChestRewards);
  }

  private void ShowBossRewards(ScenarioDbId mission, Vector3 origin)
  {
    this.FireShowCardRewardsEvent(AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario((int) mission), origin);
  }

  private void HideBossRewards(ScenarioDbId mission)
  {
    this.FireHideCardRewardsEvent(AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario((int) mission));
  }

  private void FireShowCardRewardsEvent(List<CardRewardData> rewards, Vector3 origin)
  {
    foreach (AdventureWing.ShowCardRewards showCardRewards in this.m_ShowCardRewardsListeners.ToArray())
      showCardRewards(rewards, origin);
  }

  private void FireHideCardRewardsEvent(List<CardRewardData> rewards)
  {
    foreach (AdventureWing.HideCardRewards hideCardRewards in this.m_HideCardRewardsListeners.ToArray())
      hideCardRewards(rewards);
  }

  private void FireShowRewardsPreviewEvent()
  {
    foreach (AdventureWing.ShowRewardsPreview showRewardsPreview in this.m_ShowRewardsPreviewListeners.ToArray())
      showRewardsPreview();
  }

  private void FireTryPurchaseWingEvent()
  {
    foreach (AdventureWing.TryPurchaseWing tryPurchaseWing in this.m_TryPurchaseWingListeners.ToArray())
      tryPurchaseWing();
  }

  private void UpdateBossChest(AdventureRewardsChest chest, ScenarioDbId mission)
  {
    AdventureConfig adventureConfig = AdventureConfig.Get();
    if (adventureConfig.IsScenarioDefeatedAndInitCache(mission))
    {
      if (adventureConfig.IsScenarioJustDefeated(mission))
        chest.SlamInCheckmark();
      else
        chest.ShowCheckmark();
    }
    else
    {
      if (AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario((int) mission).Count != 0)
        return;
      chest.HideAll();
    }
  }

  private void UpdatePurchasedBanner()
  {
    if (!((UnityEngine.Object) this.m_PurchasedBanner != (UnityEngine.Object) null))
      return;
    this.m_PurchasedBanner.SetActive(AdventureProgressMgr.Get().OwnsWing((int) this.m_WingDef.GetWingId()) && !AdventureProgressMgr.Get().IsWingOpen((int) this.m_WingDef.GetWingId()));
  }

  private void UpdateBuyButton(bool isOpen, object userData)
  {
    if ((UnityEngine.Object) this.m_BuyButton == (UnityEngine.Object) null)
      return;
    float num = 0.0f;
    bool flag = true;
    string gameStringTag = "GLUE_STORE_MONEY_BUTTON_TOOLTIP_HEADLINE";
    if (!isOpen)
    {
      num = 1f;
      flag = false;
      gameStringTag = "GLUE_ADVENTURE_LABEL_SHOP_CLOSED";
    }
    this.m_BuyButtonMesh.materials[0].SetFloat("_Desaturate", num);
    this.m_BuyButton.GetComponent<Collider>().enabled = flag;
    this.m_BuyButtonText.SetGameStringText(gameStringTag);
  }

  private bool IsWingRecommendedToOpen()
  {
    if ((UnityEngine.Object) this.m_DependsOnWingDef == (UnityEngine.Object) null)
      return true;
    return AdventureProgressMgr.Get().IsWingComplete(this.m_DependsOnWingDef.GetAdventureId(), AdventureConfig.Get().GetSelectedMode(), this.m_DependsOnWingDef.GetWingId());
  }

  private void SetPlateKeyEvent(bool initial)
  {
    bool open = this.IsWingRecommendedToOpen();
    this.m_WingEventTable.PlateKey(open, initial);
    if (!((UnityEngine.Object) this.m_ReleaseLabelText != (UnityEngine.Object) null))
      return;
    if (!open && !string.IsNullOrEmpty(this.m_WingDef.GetOpeningNotRecommendedLabel()))
      this.m_ReleaseLabelText.Text = this.m_WingDef.GetOpeningNotRecommendedLabel();
    else
      this.m_ReleaseLabelText.Text = GameStrings.Get("GLUE_ADVENTURE_READY_TO_OPEN");
  }

  [Serializable]
  public class BackgroundRandomization
  {
    public string m_materialTextureName = "_MainTex";
    public MeshRenderer m_backgroundRenderer;
  }

  protected class Boss
  {
    public ScenarioDbId m_MissionId;
    public AdventureBossCoin m_Coin;
    public AdventureRewardsChest m_Chest;
  }

  public delegate void BossSelected(AdventureBossCoin coin, ScenarioDbId mission);

  public delegate void OpenPlateStart(AdventureWing wing);

  public delegate void OpenPlateEnd(AdventureWing wing);

  public delegate void ShowCardRewards(List<CardRewardData> rewards, Vector3 origin);

  public delegate void HideCardRewards(List<CardRewardData> rewards);

  public delegate void ShowRewardsPreview();

  public delegate void TryPurchaseWing();

  public delegate void DelOnCoinAnimateCallback(Vector3 coinPosition);

  public delegate void BringToFocusCallback();
}
