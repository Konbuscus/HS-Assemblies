﻿// Decompiled with JetBrains decompiler
// Type: MusicManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
  private static MusicManager s_instance;
  private MusicPlaylistType m_currentPlaylist;

  private void Awake()
  {
    MusicManager.s_instance = this;
  }

  private void Start()
  {
    if (!((UnityEngine.Object) ApplicationMgr.Get() != (UnityEngine.Object) null))
      return;
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
  }

  private void OnDestroy()
  {
    MusicManager.s_instance = (MusicManager) null;
  }

  public static MusicManager Get()
  {
    return MusicManager.s_instance;
  }

  public bool StartPlaylist(MusicPlaylistType type)
  {
    if (this.m_currentPlaylist == type)
      return true;
    SoundManager soundManager = SoundManager.Get();
    if ((UnityEngine.Object) soundManager == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "MusicManager.StartPlaylist() - SoundManager does not exist.");
      return false;
    }
    MusicPlaylist playlist = this.FindPlaylist(type);
    if (playlist == null)
    {
      Debug.LogWarning((object) string.Format("MusicManager.StartPlaylist() - failed to find playlist for type {0}", (object) type));
      return false;
    }
    List<MusicTrack> musicTracks = playlist.GetMusicTracks();
    List<MusicTrack> currentMusicTracks = soundManager.GetCurrentMusicTracks();
    if (!this.AreTracksEqual(musicTracks, currentMusicTracks))
    {
      soundManager.NukeMusicAndStopPlayingCurrentTrack();
      if (musicTracks != null && musicTracks.Count > 0)
        soundManager.AddMusicTracks(musicTracks);
    }
    List<MusicTrack> ambienceTracks = playlist.GetAmbienceTracks();
    List<MusicTrack> currentAmbienceTracks = soundManager.GetCurrentAmbienceTracks();
    if (!this.AreTracksEqual(ambienceTracks, currentAmbienceTracks))
    {
      soundManager.NukeAmbienceAndStopPlayingCurrentTrack();
      if (ambienceTracks != null && ambienceTracks.Count > 0)
        soundManager.AddAmbienceTracks(ambienceTracks);
    }
    this.m_currentPlaylist = playlist.m_type;
    return true;
  }

  public bool StopPlaylist()
  {
    SoundManager soundManager = SoundManager.Get();
    if ((UnityEngine.Object) soundManager == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "MusicManager.StopPlaylist() - SoundManager does not exist.");
      return false;
    }
    if (this.m_currentPlaylist == MusicPlaylistType.Invalid)
      return false;
    this.m_currentPlaylist = MusicPlaylistType.Invalid;
    soundManager.NukePlaylistsAndStopPlayingCurrentTracks();
    return true;
  }

  public MusicPlaylistType GetCurrentPlaylist()
  {
    return this.m_currentPlaylist;
  }

  private void WillReset()
  {
    SoundManager soundManager = SoundManager.Get();
    if ((UnityEngine.Object) soundManager == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "MusicManager.WillReset() - SoundManager does not exist.");
    }
    else
    {
      this.m_currentPlaylist = MusicPlaylistType.Invalid;
      soundManager.ImmediatelyKillMusicAndAmbience();
    }
  }

  private MusicPlaylist FindPlaylist(MusicPlaylistType type)
  {
    MusicConfig musicConfig = MusicConfig.Get();
    if ((UnityEngine.Object) musicConfig == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "MusicManager.FindPlaylist() - MusicConfig does not exist.");
      return (MusicPlaylist) null;
    }
    MusicPlaylist playlist = musicConfig.FindPlaylist(type);
    if (playlist != null)
      return playlist;
    Debug.LogWarning((object) string.Format("MusicManager.FindPlaylist() - {0} playlist is not defined.", (object) type));
    return (MusicPlaylist) null;
  }

  private bool AreTracksEqual(List<MusicTrack> lhsTracks, List<MusicTrack> rhsTracks)
  {
    if (lhsTracks.Count != rhsTracks.Count)
      return false;
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MusicManager.\u003CAreTracksEqual\u003Ec__AnonStorey454 equalCAnonStorey454 = new MusicManager.\u003CAreTracksEqual\u003Ec__AnonStorey454();
    using (List<MusicTrack>.Enumerator enumerator = lhsTracks.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        equalCAnonStorey454.lhs = enumerator.Current;
        // ISSUE: reference to a compiler-generated method
        if (rhsTracks.Find(new Predicate<MusicTrack>(equalCAnonStorey454.\u003C\u003Em__31D)) == null)
          return false;
      }
    }
    return true;
  }
}
