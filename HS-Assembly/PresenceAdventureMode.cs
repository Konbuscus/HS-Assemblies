﻿// Decompiled with JetBrains decompiler
// Type: PresenceAdventureMode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum PresenceAdventureMode
{
  NAXX_NORMAL,
  NAXX_HEROIC,
  NAXX_CLASS_CHALLENGE,
  BRM_NORMAL,
  BRM_HEROIC,
  BRM_CLASS_CHALLENGE,
  LOE_NORMAL,
  LOE_HEROIC,
  LOE_CLASS_CHALLENGE,
  KAR_NORMAL,
  KAR_HEROIC,
  KAR_CLASS_CHALLENGE,
  RETURNING_PLAYER_CHALLENGE,
}
