﻿// Decompiled with JetBrains decompiler
// Type: DeathrattleSummonSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DeathrattleSummonSpell : Spell
{
  protected override Card GetTargetCardFromPowerTask(int index, PowerTask task)
  {
    Network.PowerHistory power = task.GetPower();
    if (power.Type != Network.PowerType.FULL_ENTITY)
      return (Card) null;
    Network.Entity entity1 = ((Network.HistFullEntity) power).Entity;
    Entity entity2 = GameState.Get().GetEntity(entity1.ID);
    if (entity2 != null)
      return entity2.GetCard();
    Debug.LogWarning((object) string.Format("{0}.GetTargetCardFromPowerTask() - WARNING trying to target entity with id {1} but there is no entity with that id", (object) this, (object) entity1.ID));
    return (Card) null;
  }

  protected override void OnAction(SpellStateType prevStateType)
  {
    Card sourceCard = this.GetSourceCard();
    using (List<GameObject>.Enumerator enumerator = this.m_targets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card component = enumerator.Current.GetComponent<Card>();
        component.transform.position = sourceCard.transform.position;
        float num = 0.2f;
        component.transform.localScale = new Vector3(num, num, num);
        component.SetTransitionStyle(ZoneTransitionStyle.VERY_SLOW);
        component.SetDoNotWarpToNewZone(true);
      }
    }
    this.OnBirth(prevStateType);
    this.OnSpellFinished();
  }
}
