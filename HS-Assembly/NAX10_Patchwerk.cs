﻿// Decompiled with JetBrains decompiler
// Type: NAX10_Patchwerk
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class NAX10_Patchwerk : NAX_MissionEntity
{
  private bool m_heroPowerLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_NAX10_01_HP_02");
    this.PreloadSound("VO_NAX10_01_EMOTE2_05");
    this.PreloadSound("VO_NAX10_01_EMOTE1_04");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>()
        {
          EmoteType.GREETINGS,
          EmoteType.OOPS,
          EmoteType.SORRY,
          EmoteType.THANKS,
          EmoteType.THREATEN
        },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX10_01_EMOTE1_04",
            m_stringTag = "VO_NAX10_01_EMOTE1_04"
          }
        }
      },
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>()
        {
          EmoteType.WELL_PLAYED
        },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX10_01_EMOTE2_05",
            m_stringTag = "VO_NAX10_01_EMOTE2_05"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX10_Patchwerk.\u003CHandleGameOverWithTiming\u003Ec__Iterator1DB() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX10_Patchwerk.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1DC() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX10_Patchwerk.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator1DD() { turn = turn, \u003C\u0024\u003Eturn = turn };
  }
}
