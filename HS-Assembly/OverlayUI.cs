﻿// Decompiled with JetBrains decompiler
// Type: OverlayUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OverlayUI : MonoBehaviour
{
  private HashSet<GameObject> m_destroyOnSceneLoad = new HashSet<GameObject>();
  public CanvasAnchors m_heightScale;
  public CanvasAnchors m_widthScale;
  public Transform m_BoneParent;
  public GameObject m_clickBlocker;
  public Camera m_UICamera;
  public Camera m_PerspectiveUICamera;
  private static OverlayUI s_instance;

  private void Awake()
  {
    OverlayUI.s_instance = this;
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneChange));
    ApplicationMgr.Get().WillReset += new Action(this.WillReset);
  }

  private void Start()
  {
    Log.Cameron.Print("loading overlay ui");
    CanvasScaler componentInChildren = this.gameObject.GetComponentInChildren<CanvasScaler>();
    Log.Cameron.Print("canvas scaler component " + (object) componentInChildren);
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    Log.Cameron.Print("canvas scaler values " + (object) componentInChildren.referenceResolution);
    Log.Cameron.Print("object scale values " + (object) componentInChildren.gameObject.transform.localScale);
  }

  private void OnDestroy()
  {
    ApplicationMgr.Get().WillReset -= new Action(this.WillReset);
    OverlayUI.s_instance = (OverlayUI) null;
  }

  public static OverlayUI Get()
  {
    return OverlayUI.s_instance;
  }

  public void AddGameObject(GameObject go, CanvasAnchor anchor = CanvasAnchor.CENTER, bool destroyOnSceneLoad = false, CanvasScaleMode scaleMode = CanvasScaleMode.HEIGHT)
  {
    CanvasAnchors canvasAnchors = scaleMode != CanvasScaleMode.HEIGHT ? this.m_widthScale : this.m_heightScale;
    TransformUtil.AttachAndPreserveLocalTransform(go.transform, canvasAnchors.GetAnchor(anchor));
    if (!destroyOnSceneLoad)
      return;
    this.DestroyOnSceneLoad(go);
  }

  public Vector3 GetRelativePosition(Vector3 worldPosition, Camera camera = null, Transform bone = null, float depth = 0.0f)
  {
    if ((UnityEngine.Object) camera == (UnityEngine.Object) null)
      camera = SceneMgr.Get().GetMode() != SceneMgr.Mode.GAMEPLAY ? Box.Get().GetBoxCamera().GetComponent<Camera>() : BoardCameras.Get().GetComponentInChildren<Camera>();
    if ((UnityEngine.Object) bone == (UnityEngine.Object) null)
      bone = this.m_heightScale.m_Center;
    Vector3 worldPoint = this.m_UICamera.ScreenToWorldPoint(camera.WorldToScreenPoint(worldPosition));
    worldPoint.y = depth;
    return bone.InverseTransformPoint(worldPoint);
  }

  public void DestroyOnSceneLoad(GameObject go)
  {
    if (this.m_destroyOnSceneLoad.Contains(go))
      return;
    this.m_destroyOnSceneLoad.Add(go);
  }

  public void DontDestroyOnSceneLoad(GameObject go)
  {
    if (!this.m_destroyOnSceneLoad.Contains(go))
      return;
    this.m_destroyOnSceneLoad.Remove(go);
  }

  public Transform FindBone(string name)
  {
    if ((UnityEngine.Object) this.m_BoneParent != (UnityEngine.Object) null)
    {
      Transform transform = this.m_BoneParent.Find(name);
      if ((UnityEngine.Object) transform != (UnityEngine.Object) null)
        return transform;
    }
    return this.transform;
  }

  public void ActivateClickBlocker()
  {
    if ((UnityEngine.Object) this.m_clickBlocker == (UnityEngine.Object) null)
      return;
    this.m_clickBlocker.SetActive(true);
  }

  public void DeactivateClickBlocker()
  {
    if ((UnityEngine.Object) this.m_clickBlocker == (UnityEngine.Object) null)
      return;
    this.m_clickBlocker.SetActive(false);
  }

  private void OnSceneChange(SceneMgr.Mode mode, Scene scene, object userData)
  {
    this.m_destroyOnSceneLoad.RemoveWhere((Predicate<GameObject>) (go =>
    {
      if (!((UnityEngine.Object) go != (UnityEngine.Object) null))
        return false;
      Log.Cameron.Print("destroying go " + go.name);
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
      return true;
    }));
  }

  private void WillReset()
  {
    this.m_widthScale.WillReset();
    this.m_heightScale.WillReset();
  }
}
