﻿// Decompiled with JetBrains decompiler
// Type: SubsetCardDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SubsetCardDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_SubsetId;
  [SerializeField]
  private int m_CardId;

  [DbfField("SUBSET_ID", "the SUBSET.ID this card is for")]
  public int SubsetId
  {
    get
    {
      return this.m_SubsetId;
    }
  }

  [DbfField("CARD_ID", "a CARD.ID that validates against this subset's rules")]
  public int CardId
  {
    get
    {
      return this.m_CardId;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    SubsetCardDbfAsset subsetCardDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (SubsetCardDbfAsset)) as SubsetCardDbfAsset;
    if ((UnityEngine.Object) subsetCardDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("SubsetCardDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < subsetCardDbfAsset.Records.Count; ++index)
      subsetCardDbfAsset.Records[index].StripUnusedLocales();
    records = (object) subsetCardDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
  }

  public void SetSubsetId(int v)
  {
    this.m_SubsetId = v;
  }

  public void SetCardId(int v)
  {
    this.m_CardId = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (SubsetCardDbfRecord.\u003C\u003Ef__switch\u0024map68 == null)
      {
        // ISSUE: reference to a compiler-generated field
        SubsetCardDbfRecord.\u003C\u003Ef__switch\u0024map68 = new Dictionary<string, int>(2)
        {
          {
            "SUBSET_ID",
            0
          },
          {
            "CARD_ID",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (SubsetCardDbfRecord.\u003C\u003Ef__switch\u0024map68.TryGetValue(key, out num))
      {
        if (num == 0)
          return (object) this.SubsetId;
        if (num == 1)
          return (object) this.CardId;
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (SubsetCardDbfRecord.\u003C\u003Ef__switch\u0024map69 == null)
    {
      // ISSUE: reference to a compiler-generated field
      SubsetCardDbfRecord.\u003C\u003Ef__switch\u0024map69 = new Dictionary<string, int>(2)
      {
        {
          "SUBSET_ID",
          0
        },
        {
          "CARD_ID",
          1
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!SubsetCardDbfRecord.\u003C\u003Ef__switch\u0024map69.TryGetValue(key, out num))
      return;
    if (num != 0)
    {
      if (num != 1)
        return;
      this.SetCardId((int) val);
    }
    else
      this.SetSubsetId((int) val);
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (SubsetCardDbfRecord.\u003C\u003Ef__switch\u0024map6A == null)
      {
        // ISSUE: reference to a compiler-generated field
        SubsetCardDbfRecord.\u003C\u003Ef__switch\u0024map6A = new Dictionary<string, int>(2)
        {
          {
            "SUBSET_ID",
            0
          },
          {
            "CARD_ID",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (SubsetCardDbfRecord.\u003C\u003Ef__switch\u0024map6A.TryGetValue(key, out num))
      {
        if (num == 0)
          return typeof (int);
        if (num == 1)
          return typeof (int);
      }
    }
    return (System.Type) null;
  }
}
