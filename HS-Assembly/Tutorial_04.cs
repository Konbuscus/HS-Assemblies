﻿// Decompiled with JetBrains decompiler
// Type: Tutorial_04
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Tutorial_04 : TutorialEntity
{
  private Notification endTurnNotifier;
  private Notification thatsABadPlayPopup;
  private Notification handBounceArrow;
  private Notification sheepTheBog;
  private Notification noSheepPopup;
  private int numBeastsPlayed;
  private GameObject m_heroPowerCostLabel;
  private Notification heroPowerHelp;
  private bool victory;
  private bool m_hemetSpeaking;
  private int numComplaintsMade;
  private bool m_shouldSignalPolymorph;
  private bool m_isPolymorphGrabbed;
  private bool m_isBogSheeped;
  private bool m_playOneHealthCommentNextTurn;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_TUTORIAL04_HEMET_23_21");
    this.PreloadSound("VO_TUTORIAL04_HEMET_15_13");
    this.PreloadSound("VO_TUTORIAL04_HEMET_20_18");
    this.PreloadSound("VO_TUTORIAL04_HEMET_16_14");
    this.PreloadSound("VO_TUTORIAL04_HEMET_13_12");
    this.PreloadSound("VO_TUTORIAL04_HEMET_19_17");
    this.PreloadSound("VO_TUTORIAL_04_JAINA_09_43");
    this.PreloadSound("VO_TUTORIAL_04_JAINA_10_44");
    this.PreloadSound("VO_TUTORIAL04_HEMET_06_05");
    this.PreloadSound("VO_TUTORIAL04_HEMET_07_06_ALT");
    this.PreloadSound("VO_TUTORIAL_04_JAINA_04_40");
    this.PreloadSound("VO_TUTORIAL04_HEMET_08_07");
    this.PreloadSound("VO_TUTORIAL04_HEMET_09_08");
    this.PreloadSound("VO_TUTORIAL04_HEMET_10_09");
    this.PreloadSound("VO_TUTORIAL04_HEMET_11_10");
    this.PreloadSound("VO_TUTORIAL04_HEMET_12_11");
    this.PreloadSound("VO_TUTORIAL04_HEMET_12_11_ALT");
    this.PreloadSound("VO_TUTORIAL_04_JAINA_08_42");
    this.PreloadSound("VO_TUTORIAL04_HEMET_01_01");
    this.PreloadSound("VO_TUTORIAL_04_JAINA_01_37");
    this.PreloadSound("VO_TUTORIAL04_HEMET_02_02");
    this.PreloadSound("VO_TUTORIAL04_HEMET_03_03");
    this.PreloadSound("VO_TUTORIAL_04_JAINA_02_38");
    this.PreloadSound("VO_TUTORIAL04_HEMET_04_04_ALT");
  }

  public override void NotifyOfGameOver(TAG_PLAYSTATE gameResult)
  {
    if (gameResult == TAG_PLAYSTATE.WON)
      this.victory = true;
    base.NotifyOfGameOver(gameResult);
    if ((Object) this.m_heroPowerCostLabel != (Object) null)
      Object.Destroy((Object) this.m_heroPowerCostLabel);
    if (gameResult == TAG_PLAYSTATE.WON)
    {
      this.SetTutorialProgress(TutorialProgress.NESINGWARY_COMPLETE);
      this.PlaySound("VO_TUTORIAL04_HEMET_23_21", 1f, true, false);
    }
    else if (gameResult == TAG_PLAYSTATE.TIED)
    {
      this.PlaySound("VO_TUTORIAL04_HEMET_23_21", 1f, true, false);
    }
    else
    {
      if (gameResult != TAG_PLAYSTATE.LOST)
        return;
      this.SetTutorialLostProgress(TutorialProgress.NESINGWARY_COMPLETE);
    }
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_04.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator219() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_04.\u003CHandleMissionEventWithTiming\u003Ec__Iterator21A() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  public override bool IsKeywordHelpDelayOverridden()
  {
    return true;
  }

  public override void NotifyOfCoinFlipResult()
  {
    Gameplay.Get().StartCoroutine(this.HandleCoinFlip());
  }

  [DebuggerHidden]
  private IEnumerator HandleCoinFlip()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_04.\u003CHandleCoinFlip\u003Ec__Iterator21B() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator Wait(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_04.\u003CWait\u003Ec__Iterator21C() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds };
  }

  private void ShowEndTurnBouncingArrow()
  {
    if (EndTurnButton.Get().IsInWaitingState())
      return;
    Vector3 position = EndTurnButton.Get().transform.position;
    NotificationManager.Get().CreateBouncingArrow(UserAttentionBlocker.NONE, new Vector3(position.x - 2f, position.y, position.z), new Vector3(0.0f, -90f, 0.0f));
  }

  private bool AllowEndTurn()
  {
    return !this.m_shouldSignalPolymorph || this.m_shouldSignalPolymorph && this.m_isBogSheeped;
  }

  public override bool NotifyOfEndTurnButtonPushed()
  {
    if (this.GetTag(GAME_TAG.TURN) != 4 && this.AllowEndTurn())
    {
      NotificationManager.Get().DestroyAllArrows();
      return true;
    }
    Network.Options optionsPacket = GameState.Get().GetOptionsPacket();
    if (optionsPacket != null && optionsPacket.List != null && optionsPacket.List.Count == 1)
    {
      NotificationManager.Get().DestroyAllArrows();
      return true;
    }
    if ((Object) this.endTurnNotifier != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.endTurnNotifier);
    Vector3 position1 = EndTurnButton.Get().transform.position;
    Vector3 position2 = new Vector3(position1.x - 3f, position1.y, position1.z);
    string key = "TUTORIAL_NO_ENDTURN_HP";
    if (GameState.Get().GetFriendlySidePlayer().HasReadyAttackers())
      key = "TUTORIAL_NO_ENDTURN_ATK";
    if (this.m_shouldSignalPolymorph && !this.m_isBogSheeped)
      key = "TUTORIAL_NO_ENDTURN";
    this.endTurnNotifier = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, position2, TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get(key), true);
    NotificationManager.Get().DestroyNotification(this.endTurnNotifier, 2.5f);
    return false;
  }

  public override void NotifyOfTargetModeCancelled()
  {
    if ((Object) this.sheepTheBog == (Object) null)
      return;
    NotificationManager.Get().DestroyAllPopUps();
  }

  public override void NotifyOfCardGrabbed(Entity entity)
  {
    if (!this.m_shouldSignalPolymorph)
      return;
    if (entity.GetCardId() == "CS2_022")
    {
      this.m_isPolymorphGrabbed = true;
      if ((Object) this.sheepTheBog != (Object) null)
        NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.sheepTheBog);
      if ((Object) this.handBounceArrow != (Object) null)
        NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.handBounceArrow);
      NotificationManager.Get().DestroyAllPopUps();
      Vector3 position = GameState.Get().GetOpposingSidePlayer().GetBattlefieldZone().GetFirstCard().transform.position;
      this.sheepTheBog = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(position.x - 3f, position.y, position.z), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("TUTORIAL04_HELP_02"), true);
      this.sheepTheBog.ShowPopUpArrow(Notification.PopUpArrowDirection.Right);
    }
    else
    {
      if ((Object) this.sheepTheBog != (Object) null)
        NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.sheepTheBog);
      NotificationManager.Get().DestroyAllPopUps();
      if ((bool) UniversalInputManager.UsePhoneUI)
        InputManager.Get().ReturnHeldCardToHand();
      else
        InputManager.Get().DropHeldCard();
    }
  }

  public override void NotifyOfCardDropped(Entity entity)
  {
    this.m_isPolymorphGrabbed = false;
    if (!this.m_shouldSignalPolymorph)
      return;
    if ((Object) this.sheepTheBog != (Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.sheepTheBog);
    NotificationManager.Get().DestroyAllPopUps();
    if (!this.ShouldShowArrowOnCardInHand(entity))
      return;
    Gameplay.Get().StartCoroutine(this.ShowArrowInSeconds(0.5f));
  }

  public override bool NotifyOfBattlefieldCardClicked(Entity clickedEntity, bool wasInTargetMode)
  {
    if (this.m_shouldSignalPolymorph)
    {
      if (clickedEntity.GetCardId() == "CS1_069" && wasInTargetMode)
      {
        if ((Object) this.sheepTheBog != (Object) null)
          NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.sheepTheBog);
        NotificationManager.Get().DestroyAllPopUps();
        this.m_shouldSignalPolymorph = false;
        this.m_isBogSheeped = true;
      }
      else
      {
        if (!this.m_isPolymorphGrabbed || !wasInTargetMode)
          return false;
        if ((Object) this.noSheepPopup != (Object) null)
          NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.noSheepPopup);
        Vector3 position = clickedEntity.GetCard().transform.position;
        this.noSheepPopup = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, new Vector3(position.x + 2.5f, position.y, position.z), TutorialEntity.HELP_POPUP_SCALE, GameStrings.Get("TUTORIAL04_HELP_03"), true);
        NotificationManager.Get().DestroyNotification(this.noSheepPopup, 3f);
        return false;
      }
    }
    return true;
  }

  public override bool ShouldAllowCardGrab(Entity entity)
  {
    return !this.m_shouldSignalPolymorph || !(entity.GetCardId() != "CS2_022");
  }

  private void ManaLabelLoadedCallback(string actorName, GameObject actorObject, object callbackData)
  {
    GameObject costTextObject = ((Card) callbackData).GetActor().GetCostTextObject();
    if ((Object) costTextObject == (Object) null)
    {
      Object.Destroy((Object) actorObject);
    }
    else
    {
      this.m_heroPowerCostLabel = actorObject;
      SceneUtils.SetLayer(actorObject, GameLayer.Default);
      actorObject.transform.parent = costTextObject.transform;
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.transform.localPosition = new Vector3(-0.02f, 0.38f, 0.0f);
      actorObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
      actorObject.transform.localScale = new Vector3(actorObject.transform.localScale.x, actorObject.transform.localScale.x, actorObject.transform.localScale.x);
      actorObject.GetComponent<UberText>().Text = GameStrings.Get("GLOBAL_COST");
    }
  }

  public override void NotifyOfCardMousedOver(Entity mousedOverEntity)
  {
    if (!this.ShouldShowArrowOnCardInHand(mousedOverEntity))
      return;
    NotificationManager.Get().DestroyAllArrows();
  }

  public override void NotifyOfCardMousedOff(Entity mousedOffEntity)
  {
    if (!this.ShouldShowArrowOnCardInHand(mousedOffEntity))
      return;
    Gameplay.Get().StartCoroutine(this.ShowArrowInSeconds(0.5f));
  }

  private bool ShouldShowArrowOnCardInHand(Entity entity)
  {
    return entity.GetZone() == TAG_ZONE.HAND && this.m_shouldSignalPolymorph && entity.GetCardId() == "CS2_022";
  }

  [DebuggerHidden]
  private IEnumerator ShowArrowInSeconds(float seconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Tutorial_04.\u003CShowArrowInSeconds\u003Ec__Iterator21D() { seconds = seconds, \u003C\u0024\u003Eseconds = seconds, \u003C\u003Ef__this = this };
  }

  private void ShowHandBouncingArrow()
  {
    if ((Object) this.handBounceArrow != (Object) null)
      return;
    List<Card> cards = GameState.Get().GetFriendlySidePlayer().GetHandZone().GetCards();
    if (cards.Count == 0)
      return;
    Card card = (Card) null;
    using (List<Card>.Enumerator enumerator = cards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Card current = enumerator.Current;
        if (current.GetEntity().GetCardId() == "CS2_022")
          card = current;
      }
    }
    if ((Object) card == (Object) null || this.m_isPolymorphGrabbed)
      return;
    Vector3 position = card.transform.position;
    this.handBounceArrow = NotificationManager.Get().CreateBouncingArrow(UserAttentionBlocker.NONE, new Vector3(position.x, position.y, position.z + 2f), new Vector3(0.0f, 0.0f, 0.0f));
    this.handBounceArrow.transform.parent = card.transform;
  }

  public override void NotifyOfDefeatCoinAnimation()
  {
    if (!this.victory)
      return;
    this.PlaySound("VO_TUTORIAL_04_JAINA_10_44", 1f, true, false);
  }

  public override List<RewardData> GetCustomRewards()
  {
    if (!this.victory)
      return (List<RewardData>) null;
    List<RewardData> rewardDataList = new List<RewardData>();
    CardRewardData cardRewardData = new CardRewardData("CS2_213", TAG_PREMIUM.NORMAL, 2);
    cardRewardData.MarkAsDummyReward();
    rewardDataList.Add((RewardData) cardRewardData);
    return rewardDataList;
  }
}
