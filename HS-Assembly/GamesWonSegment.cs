﻿// Decompiled with JetBrains decompiler
// Type: GamesWonSegment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class GamesWonSegment
{
  public GameObject m_root;
  public GamesWonCrown m_crown;

  public virtual void Init(Reward.Type rewardType, int rewardAmount, bool hideCrown)
  {
    if (hideCrown)
      this.m_crown.Hide();
    else
      this.m_crown.Show();
    this.m_root.SetActive(true);
  }

  public virtual void AnimateReward()
  {
    this.m_crown.Animate();
  }

  public virtual float GetWidth()
  {
    return this.m_root.GetComponent<Renderer>().bounds.size.x;
  }

  public virtual void Hide()
  {
    this.m_root.SetActive(false);
  }
}
