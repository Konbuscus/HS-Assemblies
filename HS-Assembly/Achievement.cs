﻿// Decompiled with JetBrains decompiler
// Type: Achievement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using PegasusUtil;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Achievement
{
  public static readonly int NEW_ACHIEVE_ACK_PROGRESS = -1;
  private string m_name = string.Empty;
  private string m_description = string.Empty;
  private Achievement.GameMode m_gameMode = Achievement.GameMode.ANY;
  private List<RewardData> m_rewards = new List<RewardData>();
  private RewardVisualTiming m_rewardTiming = RewardVisualTiming.IMMEDIATE;
  private List<Achievement.QuestDialog> m_onReceivedDialog = new List<Achievement.QuestDialog>();
  private List<Achievement.QuestDialog> m_onCompleteDialog = new List<Achievement.QuestDialog>();
  private List<Achievement.QuestDialog> m_onProgress1Dialog = new List<Achievement.QuestDialog>();
  private List<Achievement.QuestDialog> m_onProgress2Dialog = new List<Achievement.QuestDialog>();
  private List<long> m_rewardNoticeIDs = new List<long>();
  private int m_id;
  private bool m_enabled;
  private Achievement.Predicate m_altTextPredicate;
  private string m_altName;
  private string m_altDescription;
  private Achievement.AchType m_group;
  private int m_maxProgress;
  private TAG_RACE? m_raceReq;
  private TAG_CLASS? m_classReq;
  private TAG_CARD_SET? m_cardSetReq;
  private Achievement.ClickTriggerType? m_clickType;
  private SpecialEventType m_eventTrigger;
  private int m_linkToId;
  private Achievement.Trigger m_trigger;
  private Achievement.UnlockableFeature? m_unlockedFeature;
  private int m_scenarioID;
  private int m_wingID;
  private int m_boosterReq;
  private Achievement.ClientFlags m_clientFlags;
  private bool m_useGenericRewardVisual;
  private bool m_showToReturningPlayer;
  private int m_questDialogId;
  private bool m_autoDestroy;
  private string m_questTilePrefabName;
  private int m_onCompleteQuestDialogBannerId;
  private int m_progress;
  private int m_ackProgress;
  private int m_completionCount;
  private bool m_active;
  private long m_dateGiven;
  private long m_dateCompleted;
  private bool m_canAck;
  private int m_intervalRewardCount;
  private long m_intervalRewardStartDate;

  public int ID
  {
    get
    {
      return this.m_id;
    }
  }

  public bool Enabled
  {
    get
    {
      return this.m_enabled;
    }
  }

  public Achievement.AchType AchieveType
  {
    get
    {
      return this.m_group;
    }
  }

  public int MaxProgress
  {
    get
    {
      return this.m_maxProgress;
    }
  }

  public TAG_RACE? RaceRequirement
  {
    get
    {
      return this.m_raceReq;
    }
  }

  public TAG_CLASS? ClassRequirement
  {
    get
    {
      return this.m_classReq;
    }
  }

  public TAG_CARD_SET? CardSetRequirement
  {
    get
    {
      return this.m_cardSetReq;
    }
  }

  public Achievement.ClickTriggerType? ClickType
  {
    get
    {
      return this.m_clickType;
    }
  }

  public SpecialEventType EventTrigger
  {
    get
    {
      return this.m_eventTrigger;
    }
  }

  public int LinkToId
  {
    get
    {
      return this.m_linkToId;
    }
  }

  public Achievement.Trigger AchieveTrigger
  {
    get
    {
      return this.m_trigger;
    }
  }

  public Achievement.GameMode Mode
  {
    get
    {
      return this.m_gameMode;
    }
  }

  public Achievement.UnlockableFeature? UnlockedFeature
  {
    get
    {
      return this.m_unlockedFeature;
    }
  }

  public List<RewardData> Rewards
  {
    get
    {
      return this.m_rewards;
    }
  }

  public int ScenarioID
  {
    get
    {
      return this.m_scenarioID;
    }
  }

  public int WingID
  {
    get
    {
      return this.m_wingID;
    }
  }

  public RewardVisualTiming RewardTiming
  {
    get
    {
      return this.m_rewardTiming;
    }
  }

  public int BoosterRequirement
  {
    get
    {
      return this.m_boosterReq;
    }
  }

  public bool UseGenericRewardVisual
  {
    get
    {
      return this.m_useGenericRewardVisual;
    }
  }

  public bool ShowToReturningPlayer
  {
    get
    {
      return this.m_showToReturningPlayer;
    }
  }

  public int QuestDialogId
  {
    get
    {
      return this.m_questDialogId;
    }
  }

  public bool AutoDestroy
  {
    get
    {
      return this.m_autoDestroy;
    }
  }

  public string QuestTilePrefabName
  {
    get
    {
      return this.m_questTilePrefabName;
    }
  }

  public int OnCompleteQuestDialogBannerId
  {
    get
    {
      return this.m_onCompleteQuestDialogBannerId;
    }
  }

  public List<Achievement.QuestDialog> OnReceivedDialog
  {
    get
    {
      return this.m_onReceivedDialog;
    }
  }

  public List<Achievement.QuestDialog> OnCompleteDialog
  {
    get
    {
      return this.m_onCompleteDialog;
    }
  }

  public List<Achievement.QuestDialog> OnProgress1Dialog
  {
    get
    {
      return this.m_onProgress1Dialog;
    }
  }

  public List<Achievement.QuestDialog> OnProgress2Dialog
  {
    get
    {
      return this.m_onProgress2Dialog;
    }
  }

  public AchieveDbfRecord DbfRecord { get; private set; }

  public int Progress
  {
    get
    {
      return this.m_progress;
    }
  }

  public int AcknowledgedProgress
  {
    get
    {
      return this.m_ackProgress;
    }
  }

  public bool CanBeAcknowledged
  {
    get
    {
      return this.m_canAck;
    }
  }

  public int CompletionCount
  {
    get
    {
      return this.m_completionCount;
    }
  }

  public bool Active
  {
    get
    {
      return this.m_active;
    }
  }

  public long DateGiven
  {
    get
    {
      return this.m_dateGiven;
    }
  }

  public long DateCompleted
  {
    get
    {
      return this.m_dateCompleted;
    }
  }

  public int IntervalRewardCount
  {
    get
    {
      return this.m_intervalRewardCount;
    }
  }

  public long IntervalRewardStartDate
  {
    get
    {
      return this.m_intervalRewardStartDate;
    }
  }

  public bool IsLegendary
  {
    get
    {
      return (this.m_clientFlags & Achievement.ClientFlags.IS_LEGENDARY) != Achievement.ClientFlags.NONE;
    }
  }

  public bool CanShowInQuestLog
  {
    get
    {
      if ((this.m_clientFlags & Achievement.ClientFlags.SHOW_IN_QUEST_LOG) != Achievement.ClientFlags.NONE)
        return true;
      switch (this.AchieveType)
      {
        case Achievement.AchType.STARTER:
        case Achievement.AchType.DAILY_QUEST:
        case Achievement.AchType.NORMAL_QUEST:
          return true;
        case Achievement.AchType.UNLOCK_HERO:
        case Achievement.AchType.UNLOCK_GOLDEN_HERO:
        case Achievement.AchType.DAILY_REPEATABLE:
        case Achievement.AchType.HIDDEN:
        case Achievement.AchType.INTERNAL_ACTIVE:
        case Achievement.AchType.INTERNAL_INACTIVE:
          return false;
        default:
          return false;
      }
    }
  }

  public bool IsAffectedByFriendWeek
  {
    get
    {
      return (this.m_clientFlags & Achievement.ClientFlags.IS_AFFECTED_BY_FRIEND_WEEK) != Achievement.ClientFlags.NONE;
    }
  }

  public bool IsFriendlyChallengeQuest
  {
    get
    {
      return this.m_gameMode == Achievement.GameMode.FRIENDLY_CHALLENGE;
    }
  }

  public PlayerType PlayerType
  {
    get
    {
      string playerType = this.DbfRecord.PlayerType;
      if (playerType != null)
      {
        if (Achievement.\u003C\u003Ef__switch\u0024mapB8 == null)
          Achievement.\u003C\u003Ef__switch\u0024mapB8 = new Dictionary<string, int>(2)
          {
            {
              "challenger",
              0
            },
            {
              "challengee",
              1
            }
          };
        int num;
        if (Achievement.\u003C\u003Ef__switch\u0024mapB8.TryGetValue(playerType, out num))
        {
          if (num == 0)
            return PlayerType.PT_FRIENDLY_CHALLENGER;
          if (num == 1)
            return PlayerType.PT_FRIENDLY_CHALLENGEE;
        }
      }
      return PlayerType.PT_ANY;
    }
  }

  public string Name
  {
    get
    {
      if (!string.IsNullOrEmpty(this.m_altName) && AchieveManager.IsPredicateTrue(this.m_altTextPredicate))
        return this.m_altName;
      return this.m_name;
    }
  }

  public string Description
  {
    get
    {
      if (!string.IsNullOrEmpty(this.m_altDescription) && AchieveManager.IsPredicateTrue(this.m_altTextPredicate))
        return this.m_altDescription;
      return this.m_description;
    }
  }

  public Achievement()
  {
  }

  public Achievement(AchieveDbfRecord dbfRecord, int id, bool enabled, Achievement.AchType achieveGroup, int maxProgress, int linkToId, Achievement.Trigger trigger, Achievement.GameMode gameMode, TAG_RACE? raceReq, TAG_CLASS? classReq, TAG_CARD_SET? cardSetReq, Achievement.ClickTriggerType? clickType, SpecialEventType eventTrigger, Achievement.UnlockableFeature? unlockedFeature, List<RewardData> rewards, int scenarioID, int wingID, RewardVisualTiming rewardTiming, int boosterReq, bool useGenericRewardVisual, bool showToReturningPlayer, int questDialogId, bool autoDestroy, string questTilePrefabName, int onCompleteQuestDialogBannerId, List<Achievement.QuestDialog> onReceivedDialog, List<Achievement.QuestDialog> onCompleteDialog, List<Achievement.QuestDialog> onProgress1Dialog, List<Achievement.QuestDialog> onProgress2Dialog)
  {
    this.DbfRecord = dbfRecord != null ? dbfRecord : new AchieveDbfRecord();
    this.m_id = id;
    this.m_enabled = enabled;
    this.m_group = achieveGroup;
    this.m_maxProgress = maxProgress;
    this.m_linkToId = linkToId;
    this.m_trigger = trigger;
    this.m_gameMode = gameMode;
    this.m_raceReq = raceReq;
    this.m_classReq = classReq;
    this.m_cardSetReq = cardSetReq;
    this.m_clickType = clickType;
    this.m_eventTrigger = eventTrigger;
    this.SetRewards(rewards);
    this.m_unlockedFeature = unlockedFeature;
    this.m_scenarioID = scenarioID;
    this.m_wingID = wingID;
    this.m_rewardTiming = rewardTiming;
    this.m_boosterReq = boosterReq;
    this.m_useGenericRewardVisual = useGenericRewardVisual;
    this.m_showToReturningPlayer = showToReturningPlayer;
    this.m_questDialogId = questDialogId;
    this.m_autoDestroy = autoDestroy;
    this.m_questTilePrefabName = questTilePrefabName;
    this.m_onCompleteQuestDialogBannerId = onCompleteQuestDialogBannerId;
    this.m_onReceivedDialog = onReceivedDialog;
    this.m_onCompleteDialog = onCompleteDialog;
    this.m_onProgress1Dialog = onProgress1Dialog;
    this.m_onProgress2Dialog = onProgress2Dialog;
    this.m_progress = 0;
    this.m_ackProgress = Achievement.NEW_ACHIEVE_ACK_PROGRESS;
    this.m_completionCount = 0;
    this.m_active = false;
    this.m_dateGiven = 0L;
    this.m_dateCompleted = 0L;
  }

  public void SetClientFlags(Achievement.ClientFlags clientFlags)
  {
    this.m_clientFlags = clientFlags;
  }

  public void SetAltTextPredicate(string altTextPredicateName)
  {
    if (string.IsNullOrEmpty(altTextPredicateName))
    {
      this.m_altTextPredicate = Achievement.Predicate.NONE;
    }
    else
    {
      if (EnumUtils.TryGetEnum<Achievement.Predicate>(altTextPredicateName, StringComparison.CurrentCultureIgnoreCase, out this.m_altTextPredicate))
        return;
      this.m_altTextPredicate = Achievement.Predicate.NONE;
      Error.AddDevFatal("Achievement id={0} name=\"{1}\" has unknown ALT_TEXT_PREDICATE: \"{2}\"", (object) this.ID, (object) this.Name, (object) altTextPredicateName);
    }
  }

  public void SetName(string name, string altName)
  {
    this.m_name = name;
    this.m_altName = altName;
  }

  public void SetDescription(string description, string altDescription)
  {
    this.m_description = description;
    this.m_altDescription = altDescription;
  }

  public void AddChildRewards(List<RewardData> childRewards)
  {
    List<RewardData> rewardDataList = new List<RewardData>((IEnumerable<RewardData>) childRewards);
    this.FixUpRewardOrigins(rewardDataList);
    using (List<RewardData>.Enumerator enumerator = rewardDataList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        RewardUtils.AddRewardDataToList(enumerator.Current, this.m_rewards);
    }
  }

  public void OnAchieveData(Achieve achieveData)
  {
    this.SetProgress(achieveData.Progress);
    this.SetAcknowledgedProgress(achieveData.AckProgress);
    this.m_completionCount = !achieveData.HasCompletionCount ? 0 : achieveData.CompletionCount;
    this.m_active = achieveData.HasActive && achieveData.Active;
    this.m_dateGiven = !achieveData.HasDateGiven ? 0L : TimeUtils.PegDateToFileTimeUtc(achieveData.DateGiven);
    this.m_dateCompleted = !achieveData.HasDateCompleted ? 0L : TimeUtils.PegDateToFileTimeUtc(achieveData.DateCompleted);
    this.m_canAck = !achieveData.HasDoNotAck || !achieveData.DoNotAck;
    this.m_intervalRewardCount = !achieveData.HasIntervalRewardCount ? 0 : achieveData.IntervalRewardCount;
    this.m_intervalRewardStartDate = !achieveData.HasIntervalRewardStart ? 0L : TimeUtils.PegDateToFileTimeUtc(achieveData.IntervalRewardStart);
    this.AutoAckIfNeeded();
  }

  public void OnAchieveNotification(AchievementNotification notification)
  {
    Achieve achieveData = new Achieve();
    achieveData.Id = (int) notification.AchievementId;
    achieveData.CompletionCount = this.CompletionCount;
    achieveData.Progress = this.Progress;
    achieveData.Active = this.Active;
    achieveData.DoNotAck = !this.CanBeAcknowledged;
    achieveData.DateCompleted = TimeUtils.FileTimeUtcToPegDate(this.DateCompleted);
    achieveData.DateGiven = TimeUtils.FileTimeUtcToPegDate(this.DateGiven);
    achieveData.AckProgress = this.AcknowledgedProgress;
    Log.Achievements.Print("OnAchieveNotification PlayerID={0} ID={1} Complete={2} New={3} Remove={4} Amount={5}", (object) notification.PlayerId, (object) notification.AchievementId, (object) notification.Complete, (object) notification.NewAchievement, (object) notification.RemoveAchievement, (object) notification.Amount);
    if (notification.NewAchievement)
    {
      achieveData.DateGiven = TimeUtils.FileTimeUtcToPegDate(DateTime.UtcNow.ToFileTimeUtc());
      achieveData.Active = true;
      achieveData.AckProgress = Achievement.NEW_ACHIEVE_ACK_PROGRESS;
      achieveData.Progress = 0;
    }
    achieveData.Progress += notification.Amount;
    if (notification.Complete)
    {
      achieveData.Progress = this.MaxProgress;
      ++achieveData.CompletionCount;
      achieveData.DateCompleted = TimeUtils.FileTimeUtcToPegDate(DateTime.UtcNow.ToFileTimeUtc());
      achieveData.Active = false;
      achieveData.DoNotAck = false;
    }
    if (notification.RemoveAchievement)
      achieveData.Active = false;
    if (!achieveData.Active)
      this.OnAchieveData(achieveData);
    else
      this.UpdateActiveAchieve(achieveData);
  }

  public void UpdateActiveAchieve(Achieve achieveData)
  {
    this.SetProgress(achieveData.Progress);
    this.SetAcknowledgedProgress(achieveData.AckProgress);
    this.m_active = true;
    this.m_dateGiven = !achieveData.HasDateGiven ? 0L : TimeUtils.PegDateToFileTimeUtc(achieveData.DateGiven);
    if (achieveData.HasIntervalRewardCount)
      this.m_intervalRewardCount = achieveData.IntervalRewardCount;
    if (achieveData.HasIntervalRewardStart)
      this.m_intervalRewardStartDate = TimeUtils.PegDateToFileTimeUtc(achieveData.IntervalRewardStart);
    this.AutoAckIfNeeded();
  }

  public void AddRewardNoticeID(long noticeID)
  {
    if (this.m_rewardNoticeIDs.Contains(noticeID))
      return;
    if (this.IsCompleted() && !this.NeedToAcknowledgeProgress(false))
      Network.AckNotice(noticeID);
    this.m_rewardNoticeIDs.Add(noticeID);
  }

  public void OnCancelSuccess()
  {
    this.m_active = false;
  }

  public bool IsInternal()
  {
    if (this.AchieveType != Achievement.AchType.INTERNAL_ACTIVE)
      return Achievement.AchType.INTERNAL_INACTIVE == this.AchieveType;
    return true;
  }

  public bool IsNewlyActive()
  {
    return this.m_ackProgress == Achievement.NEW_ACHIEVE_ACK_PROGRESS;
  }

  public bool IsCompleted()
  {
    return this.Progress >= this.MaxProgress;
  }

  public bool IsActiveLicenseAddedAchieve()
  {
    if (this.AchieveTrigger != Achievement.Trigger.ACCOUNT_LICENSE_ADDED)
      return false;
    return this.Active;
  }

  public void AckCurrentProgressAndRewardNotices()
  {
    this.AckCurrentProgressAndRewardNotices(false);
  }

  public void AckCurrentProgressAndRewardNotices(bool ackIntermediateProgress)
  {
    long[] array = this.m_rewardNoticeIDs.ToArray();
    this.m_rewardNoticeIDs.Clear();
    foreach (long id in array)
      Network.AckNotice(id);
    if (!this.NeedToAcknowledgeProgress(ackIntermediateProgress))
      return;
    this.m_ackProgress = this.Progress;
    if (!this.m_canAck)
      return;
    Network.AckAchieveProgress(this.ID, this.AcknowledgedProgress);
  }

  public void IncrementIntervalRewardCount()
  {
    if (this.m_intervalRewardCount < 0)
      this.m_intervalRewardCount = 0;
    ++this.m_intervalRewardCount;
    if (this.m_intervalRewardStartDate != 0L)
      return;
    this.m_intervalRewardStartDate = DateTime.UtcNow.ToFileTimeUtc();
  }

  public bool IsValidFriendlyPlayerChallengeType(PlayerType playerType)
  {
    if (this.PlayerType != PlayerType.PT_ANY)
      return playerType == this.PlayerType;
    return true;
  }

  public override string ToString()
  {
    return string.Format("[Achievement: ID={0} AchieveGroup={1} Name='{2}' MaxProgress={3} Progress={4} AckProgress={5} IsActive={6} DateGiven={7} DateCompleted={8} Description='{9}' Trigger={10} CanAck={11}]", (object) this.ID, (object) this.AchieveType, (object) this.m_name, (object) this.MaxProgress, (object) this.Progress, (object) this.AcknowledgedProgress, (object) this.Active, (object) this.DateGiven, (object) this.DateCompleted, (object) this.m_description, (object) this.AchieveTrigger, (object) this.m_canAck);
  }

  private bool NeedToAcknowledgeProgress(bool ackIntermediateProgress)
  {
    return this.AcknowledgedProgress < this.MaxProgress && this.AcknowledgedProgress != this.Progress && (ackIntermediateProgress || this.Progress <= 0 || this.Progress == this.MaxProgress);
  }

  private void SetProgress(int progress)
  {
    this.m_progress = Mathf.Clamp(progress, 0, this.MaxProgress);
  }

  private void SetAcknowledgedProgress(int acknowledgedProgress)
  {
    this.m_ackProgress = Mathf.Clamp(acknowledgedProgress, Achievement.NEW_ACHIEVE_ACK_PROGRESS, this.Progress);
  }

  private void AutoAckIfNeeded()
  {
    if (!this.IsInternal() && Achievement.AchType.DAILY_REPEATABLE != this.AchieveType)
      return;
    this.AckCurrentProgressAndRewardNotices();
  }

  private void SetRewards(List<RewardData> rewardDataList)
  {
    this.m_rewards = new List<RewardData>((IEnumerable<RewardData>) rewardDataList);
    this.FixUpRewardOrigins(this.m_rewards);
  }

  private void FixUpRewardOrigins(List<RewardData> rewardDataList)
  {
    using (List<RewardData>.Enumerator enumerator = rewardDataList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetOrigin(NetCache.ProfileNotice.NoticeOrigin.ACHIEVEMENT, (long) this.ID);
    }
  }

  public enum AchType
  {
    OTHER,
    [System.ComponentModel.Description("starter")] STARTER,
    [System.ComponentModel.Description("hero")] UNLOCK_HERO,
    [System.ComponentModel.Description("goldhero")] UNLOCK_GOLDEN_HERO,
    [System.ComponentModel.Description("daily")] DAILY_QUEST,
    [System.ComponentModel.Description("daily_repeatable")] DAILY_REPEATABLE,
    [System.ComponentModel.Description("hidden")] HIDDEN,
    [System.ComponentModel.Description("internal_active")] INTERNAL_ACTIVE,
    [System.ComponentModel.Description("internal_inactive")] INTERNAL_INACTIVE,
    [System.ComponentModel.Description("login_activated")] LOGIN_ACTIVATED,
    [System.ComponentModel.Description("normal_quest")] NORMAL_QUEST,
  }

  public enum UnlockableFeature
  {
    [System.ComponentModel.Description("daily")] DAILY_QUESTS,
    [System.ComponentModel.Description("forge")] FORGE,
    [System.ComponentModel.Description("naxx1_owned")] NAXX_WING_1_OWNED,
    [System.ComponentModel.Description("naxx2_owned")] NAXX_WING_2_OWNED,
    [System.ComponentModel.Description("naxx3_owned")] NAXX_WING_3_OWNED,
    [System.ComponentModel.Description("naxx4_owned")] NAXX_WING_4_OWNED,
    [System.ComponentModel.Description("naxx5_owned")] NAXX_WING_5_OWNED,
    [System.ComponentModel.Description("naxx1_playable")] NAXX_WING_1_PLAYABLE,
    [System.ComponentModel.Description("naxx2_playable")] NAXX_WING_2_PLAYABLE,
    [System.ComponentModel.Description("naxx3_playable")] NAXX_WING_3_PLAYABLE,
    [System.ComponentModel.Description("naxx4_playable")] NAXX_WING_4_PLAYABLE,
    [System.ComponentModel.Description("naxx5_playable")] NAXX_WING_5_PLAYABLE,
    [System.ComponentModel.Description("vanilla heroes")] VANILLA_HEROES,
  }

  public enum Trigger
  {
    [System.ComponentModel.Description("none")] IGNORE,
    [System.ComponentModel.Description("licenseadded")] ACCOUNT_LICENSE_ADDED,
    [System.ComponentModel.Description("adventure_progress")] ADVENTURE_PROGRESS,
    [System.ComponentModel.Description("click")] CLICK,
    [System.ComponentModel.Description("disenchant")] DISENCHANT,
    [System.ComponentModel.Description("cardset")] COMPLETE_CARD_SET,
    [System.ComponentModel.Description("event")] EVENT,
    [System.ComponentModel.Description("event_timing_only")] EVENT_TIMING_ONLY,
    [System.ComponentModel.Description("race")] GAIN_CARD,
    [System.ComponentModel.Description("goldrace")] GAIN_GOLDEN_CARD,
    [System.ComponentModel.Description("purchase")] PURCHASE,
    [System.ComponentModel.Description("win")] WIN_GAME,
    [System.ComponentModel.Description("pack_ready_to_open")] PACK_READY_TO_OPEN,
    [System.ComponentModel.Description("finish")] FINISH_GAME,
    [System.ComponentModel.Description("destroyed")] DESTROYED,
  }

  public enum GameMode
  {
    UNKNOWN,
    [System.ComponentModel.Description("any")] ANY,
    [System.ComponentModel.Description("any ai")] VS_ANY_AI,
    [System.ComponentModel.Description("any practice")] VS_ANY_PRACTICE_AI,
    [System.ComponentModel.Description("basic ai")] VS_BASIC_AI,
    [System.ComponentModel.Description("expert ai")] VS_EXPERT_AI,
    [System.ComponentModel.Description("adventure")] VS_ADVENTURE_AI,
    [System.ComponentModel.Description("tutorial")] TUTORIAL,
    [System.ComponentModel.Description("matchmaker")] MATCHMAKER,
    [System.ComponentModel.Description("play_mode")] PLAY_MODE,
    [System.ComponentModel.Description("play_mode_tb")] PLAY_MODE_TAVERN_BRAWL,
    [System.ComponentModel.Description("play_mode_standard")] PLAY_MODE_STARDARD_FORMAT,
    [System.ComponentModel.Description("play_mode_wild")] PLAY_MODE_WILD_FORMAT,
    [System.ComponentModel.Description("ranked")] RANKED,
    [System.ComponentModel.Description("casual")] CASUAL,
    [System.ComponentModel.Description("arena")] ARENA,
    [System.ComponentModel.Description("friendly")] FRIENDLY_CHALLENGE,
    [System.ComponentModel.Description("tavernbrawl")] TAVERN_BRAWL,
  }

  public struct QuestDialog
  {
    public int playOrder;
    public string prefabName;
    public string audioName;
    public bool useAltSpeechBubble;
    public float waitBefore;
    public float waitAfter;
    public bool persistPrefab;
  }

  [Flags]
  public enum ClientFlags
  {
    [System.ComponentModel.Description("none")] NONE = 0,
    [System.ComponentModel.Description("is_legendary")] IS_LEGENDARY = 1,
    [System.ComponentModel.Description("show_in_quest_log")] SHOW_IN_QUEST_LOG = 2,
    [System.ComponentModel.Description("affected_by_friend_week")] IS_AFFECTED_BY_FRIEND_WEEK = 4,
  }

  public enum Predicate
  {
    [System.ComponentModel.Description("none")] NONE,
    [System.ComponentModel.Description("can_see_wild")] CAN_SEE_WILD,
  }

  public enum ClickTriggerType
  {
    BUTTON_PLAY = 1,
    BUTTON_ARENA = 2,
    BUTTON_ADVENTURE = 3,
  }
}
