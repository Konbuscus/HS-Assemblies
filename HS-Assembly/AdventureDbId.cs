﻿// Decompiled with JetBrains decompiler
// Type: AdventureDbId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum AdventureDbId
{
  INVALID = 0,
  TUTORIAL = 1,
  PRACTICE = 2,
  NAXXRAMAS = 3,
  BRM = 4,
  TAVERN_BRAWL = 7,
  LOE = 8,
  KARA = 10,
  RETURNING_PLAYER = 245,
}
