﻿// Decompiled with JetBrains decompiler
// Type: SocialToast
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SocialToast : MonoBehaviour
{
  private const float FUDGE_FACTOR = 0.95f;
  public UberText m_text;

  public void SetText(string text)
  {
    this.m_text.Text = text;
    this.GetComponent<ThreeSliceElement>().SetMiddleWidth(this.m_text.GetTextWorldSpaceBounds().size.x * 0.95f);
  }
}
