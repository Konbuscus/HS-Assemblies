﻿// Decompiled with JetBrains decompiler
// Type: SeasonTimeRemainingToast
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SeasonTimeRemainingToast : GameToast
{
  public UberText m_seasonTitle;
  public UberText m_timeRemaining;
  public GameObject m_background;

  private void Awake()
  {
    this.m_intensityMaterials.Add(this.m_background.GetComponent<Renderer>().material);
  }

  public void UpdateDisplay(string title, string timeRemaining)
  {
    this.m_seasonTitle.Text = title;
    this.m_timeRemaining.Text = timeRemaining;
  }
}
