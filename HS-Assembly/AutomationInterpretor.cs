﻿// Decompiled with JetBrains decompiler
// Type: AutomationInterpretor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AutomationInterpretor
{
  public KeyCode m_ExportMouseKey = KeyCode.F9;
  public const string s_newPacketTag = "<!--NEWPACKET-->";
  public const float s_connectionTimeoutLength = 60f;
  private const string CUSTOM_ARG_PREFIX = "+";
  public bool m_isClosing;
  public bool m_isClosed;
  public bool m_initLoadComplete;
  public bool m_wasPaused;
  private static bool m_isDebugBuild;
  private static bool m_DebugLog;
  private static AutomationInterpretor s_instance;

  public static AutomationInterpretor Get()
  {
    if (AutomationInterpretor.s_instance == null)
      AutomationInterpretor.s_instance = new AutomationInterpretor();
    return AutomationInterpretor.s_instance;
  }

  public void Start()
  {
  }

  public void Update()
  {
  }
}
