﻿// Decompiled with JetBrains decompiler
// Type: PackOpeningDirector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class PackOpeningDirector : MonoBehaviour
{
  private readonly Vector3 PACK_OPENING_FX_POSITION = Vector3.zero;
  private List<PackOpeningCard> m_hiddenCards = new List<PackOpeningCard>();
  private Map<int, Spell> m_packFxSpells = new Map<int, Spell>();
  private List<PackOpeningDirector.FinishedListener> m_finishedListeners = new List<PackOpeningDirector.FinishedListener>();
  public PackOpeningCard m_HiddenCard;
  public GameObject m_CardsInsidePack;
  public GameObject m_ClassName;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_DoneButtonPrefab;
  public Carousel m_Carousel;
  private NormalButton m_doneButton;
  private bool m_loadingDoneButton;
  private bool m_playing;
  private Spell m_activePackFxSpell;
  private int m_cardsPendingReveal;
  private int m_effectsPendingFinish;
  private int m_effectsPendingDestroy;
  private int m_centerCardIndex;
  private bool m_doneButtonShown;
  private PackOpeningCard m_clickedCard;
  private int m_clickedPosition;
  private PackOpeningCard m_glowingCard;

  private void Awake()
  {
    this.InitializeCards();
    this.InitializeUI();
  }

  private void Update()
  {
    if (!(bool) ((Object) this.m_Carousel))
      return;
    this.m_Carousel.UpdateUI(UniversalInputManager.Get().GetMouseButtonDown(0));
  }

  public void Play(int boosterId)
  {
    if (this.m_playing)
      return;
    this.m_playing = true;
    this.EnableCardInput(false);
    this.StartCoroutine(this.PlayWhenReady(boosterId));
  }

  public bool IsPlaying()
  {
    return this.m_playing;
  }

  public void OnBoosterOpened(List<NetCache.BoosterCard> cards)
  {
    if (cards.Count > this.m_hiddenCards.Count)
    {
      UnityEngine.Debug.LogError((object) string.Format("PackOpeningDirector.OnBoosterOpened() - Not enough PackOpeningCards! Received {0} cards. There are only {1} hidden cards.", (object) cards.Count, (object) this.m_hiddenCards.Count));
    }
    else
    {
      this.m_cardsPendingReveal = Mathf.Min(cards.Count, this.m_hiddenCards.Count);
      this.StartCoroutine(this.AttachBoosterCards(cards));
      CollectionManager.Get().OnBoosterOpened(cards);
    }
  }

  public void AddFinishedListener(PackOpeningDirector.FinishedCallback callback)
  {
    this.AddFinishedListener(callback, (object) null);
  }

  public void AddFinishedListener(PackOpeningDirector.FinishedCallback callback, object userData)
  {
    PackOpeningDirector.FinishedListener finishedListener = new PackOpeningDirector.FinishedListener();
    finishedListener.SetCallback(callback);
    finishedListener.SetUserData(userData);
    this.m_finishedListeners.Add(finishedListener);
  }

  public void RemoveFinishedListener(PackOpeningDirector.FinishedCallback callback)
  {
    this.RemoveFinishedListener(callback, (object) null);
  }

  public void RemoveFinishedListener(PackOpeningDirector.FinishedCallback callback, object userData)
  {
    PackOpeningDirector.FinishedListener finishedListener = new PackOpeningDirector.FinishedListener();
    finishedListener.SetCallback(callback);
    finishedListener.SetUserData(userData);
    this.m_finishedListeners.Remove(finishedListener);
  }

  public bool IsDoneButtonShown()
  {
    return this.m_doneButtonShown;
  }

  public List<PackOpeningCard> GetHiddenCards()
  {
    return this.m_hiddenCards;
  }

  public bool FinishPackOpen()
  {
    if (!this.m_doneButtonShown)
      return false;
    this.m_activePackFxSpell.ActivateState(SpellStateType.DEATH);
    Box.Get().GetBoxCamera().GetEventTable().m_BlurSpell.ActivateState(SpellStateType.DEATH);
    this.m_effectsPendingFinish = 1 + 2 * this.m_hiddenCards.Count;
    this.m_effectsPendingDestroy = this.m_effectsPendingFinish;
    this.HideDoneButton();
    using (List<PackOpeningCard>.Enumerator enumerator = this.m_hiddenCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PackOpeningCard current = enumerator.Current;
        CardBackDisplay componentInChildren = current.GetComponentInChildren<CardBackDisplay>();
        if ((Object) componentInChildren != (Object) null)
          componentInChildren.gameObject.GetComponent<Renderer>().enabled = false;
        Spell classNameSpell = current.m_ClassNameSpell;
        classNameSpell.AddFinishedCallback(new Spell.FinishedCallback(this.OnHiddenCardSpellFinished));
        classNameSpell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnHiddenCardSpellStateFinished));
        classNameSpell.ActivateState(SpellStateType.DEATH);
        Spell isNewSpell = current.m_IsNewSpell;
        if ((Object) isNewSpell != (Object) null)
          isNewSpell.ActivateState(SpellStateType.DEATH);
        Spell spell = current.GetActor().GetSpell(SpellType.DEATH);
        spell.AddFinishedCallback(new Spell.FinishedCallback(this.OnHiddenCardSpellFinished));
        spell.AddStateFinishedCallback(new Spell.StateFinishedCallback(this.OnHiddenCardSpellStateFinished));
        spell.Activate();
      }
    }
    this.HideKeywordTooltips();
    return true;
  }

  [DebuggerHidden]
  private IEnumerator PlayWhenReady(int boosterId)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PackOpeningDirector.\u003CPlayWhenReady\u003Ec__Iterator22F() { boosterId = boosterId, \u003C\u0024\u003EboosterId = boosterId, \u003C\u003Ef__this = this };
  }

  private void OnSpellFinished(Spell spell, object userData)
  {
    using (List<PackOpeningCard>.Enumerator enumerator = this.m_hiddenCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PackOpeningCard current = enumerator.Current;
        current.EnableInput(true);
        current.EnableReveal(true);
      }
    }
    this.AttachCardsToCarousel();
  }

  private void CameraBlurOn()
  {
    Box.Get().GetBoxCamera().GetEventTable().m_BlurSpell.ActivateState(SpellStateType.BIRTH);
  }

  private void AttachCardsToCarousel()
  {
    if (!(bool) ((Object) this.m_Carousel))
      return;
    List<PackOpeningCardCarouselItem> cardCarouselItemList = new List<PackOpeningCardCarouselItem>();
    for (int index = 0; index < this.m_hiddenCards.Count; ++index)
    {
      PackOpeningCard hiddenCard = this.m_hiddenCards[index];
      hiddenCard.GetComponent<Collider>().enabled = true;
      PackOpeningCardCarouselItem cardCarouselItem = new PackOpeningCardCarouselItem(hiddenCard);
      cardCarouselItemList.Add(cardCarouselItem);
    }
    this.m_Carousel.Initialize((CarouselItem[]) cardCarouselItemList.ToArray(), 0);
    this.m_Carousel.SetListeners((Carousel.ItemPulled) null, new Carousel.ItemClicked(this.CarouselItemClicked), new Carousel.ItemReleased(this.CarouselItemReleased), new Carousel.CarouselSettled(this.CarouselSettled), new Carousel.CarouselStartedScrolling(this.CarouselStartedScrolling));
    this.CarouselSettled();
  }

  private void CarouselItemClicked(CarouselItem item, int index)
  {
    this.m_clickedCard = item.GetGameObject().GetComponent<PackOpeningCard>();
    this.m_clickedPosition = index;
  }

  private void CarouselItemReleased()
  {
    if (this.m_Carousel.IsScrolling())
      return;
    if (this.m_clickedPosition == this.m_Carousel.GetCurrentIndex())
    {
      if (this.m_clickedCard.IsRevealed())
      {
        if (this.m_clickedPosition >= 4)
          return;
        this.m_Carousel.SetPosition(this.m_clickedPosition + 1, true);
      }
      else
        this.m_clickedCard.ForceReveal();
    }
    else
      this.m_Carousel.SetPosition(this.m_clickedPosition, true);
  }

  private void CarouselSettled()
  {
    PackOpeningCard component = ((PackOpeningCardCarouselItem) this.m_Carousel.GetCurrentItem()).GetGameObject().GetComponent<PackOpeningCard>();
    this.m_glowingCard = component;
    component.ShowRarityGlow();
  }

  private void CarouselStartedScrolling()
  {
    if (!((Object) this.m_glowingCard != (Object) null) || this.m_glowingCard.GetEntityDef().GetRarity() == TAG_RARITY.COMMON)
      return;
    this.m_glowingCard.HideRarityGlow();
  }

  private void InitializeUI()
  {
    this.m_loadingDoneButton = true;
    AssetLoader.Get().LoadActor(FileUtils.GameAssetPathToName(this.m_DoneButtonPrefab), new AssetLoader.GameObjectCallback(this.OnDoneButtonLoaded), (object) null, false);
  }

  private void OnDoneButtonLoaded(string name, GameObject actorObject, object userData)
  {
    this.m_loadingDoneButton = false;
    if ((Object) actorObject == (Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("PackOpeningDirector.OnDoneButtonLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      this.m_doneButton = actorObject.GetComponent<NormalButton>();
      if ((Object) this.m_doneButton == (Object) null)
      {
        UnityEngine.Debug.LogError((object) string.Format("PackOpeningDirector.OnDoneButtonLoaded() - ERROR \"{0}\" has no {1} component", (object) name, (object) typeof (NormalButton)));
      }
      else
      {
        SceneUtils.SetLayer(this.m_doneButton.gameObject, GameLayer.IgnoreFullScreenEffects);
        this.m_doneButton.transform.parent = this.transform;
        TransformUtil.CopyWorld((Component) this.m_doneButton, (Component) PackOpening.Get().m_Bones.m_DoneButton);
        SceneUtils.EnableRenderersAndColliders(this.m_doneButton.gameObject, false);
      }
    }
  }

  private void ShowDoneButton()
  {
    this.m_doneButtonShown = true;
    SceneUtils.EnableRenderersAndColliders(this.m_doneButton.gameObject, true);
    Spell component = this.m_doneButton.m_button.GetComponent<Spell>();
    component.AddFinishedCallback(new Spell.FinishedCallback(this.OnDoneButtonShown));
    component.ActivateState(SpellStateType.BIRTH);
  }

  private void OnDoneButtonShown(Spell spell, object userData)
  {
    this.m_doneButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDoneButtonPressed));
  }

  private void HideDoneButton()
  {
    this.m_doneButtonShown = false;
    SceneUtils.EnableColliders(this.m_doneButton.gameObject, false);
    this.m_doneButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDoneButtonPressed));
    Spell component = this.m_doneButton.m_button.GetComponent<Spell>();
    component.AddFinishedCallback(new Spell.FinishedCallback(this.OnDoneButtonHidden));
    component.ActivateState(SpellStateType.DEATH);
  }

  private void OnDoneButtonHidden(Spell spell, object userData)
  {
    this.OnEffectFinished();
    this.OnEffectDone();
  }

  private void OnDoneButtonPressed(UIEvent e)
  {
    this.HideKeywordTooltips();
    this.FinishPackOpen();
  }

  private void HideKeywordTooltips()
  {
    using (List<PackOpeningCard>.Enumerator enumerator = this.m_hiddenCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.RemoveOnOverWhileFlippedListeners();
    }
    TooltipPanelManager.Get().HideKeywordHelp();
  }

  private void InitializeCards()
  {
    this.m_hiddenCards.Add(this.m_HiddenCard);
    for (int index = 1; index < 5; ++index)
    {
      PackOpeningCard component = Object.Instantiate<GameObject>(this.m_HiddenCard.gameObject).GetComponent<PackOpeningCard>();
      component.transform.parent = this.m_HiddenCard.transform.parent;
      TransformUtil.CopyLocal((Component) component, (Component) this.m_HiddenCard);
      this.m_hiddenCards.Add(component);
    }
    for (int index = 0; index < this.m_hiddenCards.Count; ++index)
    {
      PackOpeningCard hiddenCard = this.m_hiddenCards[index];
      hiddenCard.name = string.Format("Card_Hidden{0}", (object) (index + 1));
      hiddenCard.EnableInput(false);
      hiddenCard.AddRevealedListener(new PackOpeningCard.RevealedCallback(this.OnCardRevealed), (object) hiddenCard);
    }
  }

  private void EnableCardInput(bool enable)
  {
    using (List<PackOpeningCard>.Enumerator enumerator = this.m_hiddenCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.EnableInput(enable);
    }
  }

  private void OnCardRevealed(object userData)
  {
    PackOpeningCard packOpeningCard = (PackOpeningCard) userData;
    if (packOpeningCard.GetEntityDef().GetRarity() == TAG_RARITY.LEGENDARY)
    {
      if (packOpeningCard.GetActor().GetPremium() == TAG_PREMIUM.GOLDEN)
        BnetPresenceMgr.Get().SetGameField(4U, packOpeningCard.GetCardId() + ",1");
      else
        BnetPresenceMgr.Get().SetGameField(4U, packOpeningCard.GetCardId() + ",0");
    }
    --this.m_cardsPendingReveal;
    if (this.m_cardsPendingReveal > 0)
      return;
    this.ShowDoneButton();
  }

  private void OnHiddenCardSpellFinished(Spell spell, object userData)
  {
    this.OnEffectFinished();
  }

  private void OnHiddenCardSpellStateFinished(Spell spell, SpellStateType prevStateType, object userData)
  {
    if (spell.GetActiveState() != SpellStateType.NONE)
      return;
    this.OnEffectDone();
  }

  [DebuggerHidden]
  private IEnumerator AttachBoosterCards(List<NetCache.BoosterCard> cards)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PackOpeningDirector.\u003CAttachBoosterCards\u003Ec__Iterator230() { cards = cards, \u003C\u0024\u003Ecards = cards, \u003C\u003Ef__this = this };
  }

  private void OnEffectFinished()
  {
    --this.m_effectsPendingFinish;
    if (this.m_effectsPendingFinish > 0)
      return;
    this.FireFinishedEvent();
  }

  private void OnEffectDone()
  {
    --this.m_effectsPendingDestroy;
    if (this.m_effectsPendingDestroy > 0)
      return;
    Object.Destroy((Object) this.gameObject);
  }

  private void FireFinishedEvent()
  {
    foreach (PackOpeningDirector.FinishedListener finishedListener in this.m_finishedListeners.ToArray())
      finishedListener.Fire();
  }

  private class FinishedListener : EventListener<PackOpeningDirector.FinishedCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  public delegate void FinishedCallback(object userData);
}
