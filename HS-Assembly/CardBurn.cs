﻿// Decompiled with JetBrains decompiler
// Type: CardBurn
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CardBurn : Spell
{
  public string m_BurnCardAnim = "CardBurnUpFire";
  public GameObject m_BurnCardQuad;
  public ParticleSystem m_EdgeEmbers;

  protected override void OnBirth(SpellStateType prevStateType)
  {
    this.StartCoroutine(this.BirthAction());
  }

  [DebuggerHidden]
  private IEnumerator BirthAction()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardBurn.\u003CBirthAction\u003Ec__Iterator2AA() { \u003C\u003Ef__this = this };
  }
}
