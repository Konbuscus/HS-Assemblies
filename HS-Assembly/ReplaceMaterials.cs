﻿// Decompiled with JetBrains decompiler
// Type: ReplaceMaterials
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[CustomEditClass]
public class ReplaceMaterials : MonoBehaviour
{
  public List<ReplaceMaterials.MaterialData> m_Materials;

  private void Start()
  {
    using (List<ReplaceMaterials.MaterialData>.Enumerator enumerator = this.m_Materials.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ReplaceMaterials.MaterialData current = enumerator.Current;
        GameObject gameObject = this.FindGameObject(current.GameObjectName);
        if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null && !current.ReplaceChildMaterials)
          Log.Kyle.Print("ReplaceMaterials failed to locate object: {0}", (object) current.GameObjectName);
        else if (current.ReplaceChildMaterials)
        {
          foreach (Renderer componentsInChild in gameObject.GetComponentsInChildren<Renderer>())
          {
            if (!((UnityEngine.Object) componentsInChild == (UnityEngine.Object) null))
              RenderUtils.SetMaterial(componentsInChild, current.MaterialIndex, current.NewMaterial);
          }
        }
        else
        {
          Renderer component = gameObject.GetComponent<Renderer>();
          if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
            Log.Kyle.Print("ReplaceMaterials failed to get Renderer: {0}", (object) current.GameObjectName);
          else
            RenderUtils.SetMaterial(component, current.MaterialIndex, current.NewMaterial);
        }
      }
    }
  }

  private GameObject FindGameObject(string gameObjName)
  {
    if ((int) gameObjName[0] != 47)
      return GameObject.Find(gameObjName);
    string[] strArray = gameObjName.Split('/');
    return GameObject.Find(strArray[strArray.Length - 1]);
  }

  [Serializable]
  public class MaterialData
  {
    [CustomEditField(T = EditType.SCENE_OBJECT)]
    public string GameObjectName;
    public int MaterialIndex;
    public Material NewMaterial;
    public bool ReplaceChildMaterials;
    public GameObject DisplayGameObject;
  }
}
