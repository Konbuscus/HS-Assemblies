﻿// Decompiled with JetBrains decompiler
// Type: FontDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FontDef : MonoBehaviour
{
  public float m_LineSpaceModifier = 1f;
  public float m_FontSizeModifier = 1f;
  public float m_CharacterSizeModifier = 1f;
  public float m_OutlineModifier = 1f;
  public float m_UnboundCharacterSizeModifier = 1f;
  public Font m_Font;
  public float m_SingleLineAdjustment;
}
