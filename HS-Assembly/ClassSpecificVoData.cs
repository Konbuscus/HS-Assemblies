﻿// Decompiled with JetBrains decompiler
// Type: ClassSpecificVoData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[Serializable]
public class ClassSpecificVoData
{
  public List<ClassSpecificVoLine> m_Lines = new List<ClassSpecificVoLine>();
  public SpellPlayerSide m_SideToSearch = SpellPlayerSide.TARGET;
  public List<SpellZoneTag> m_ZonesToSearch = new List<SpellZoneTag>() { SpellZoneTag.HERO };
}
