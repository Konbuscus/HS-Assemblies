﻿// Decompiled with JetBrains decompiler
// Type: TurnTimer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TurnTimer : MonoBehaviour
{
  public float m_DebugTimeout = 30f;
  public float m_DebugTimeoutStart = 20f;
  public string m_FuseMatValName = "_Xamount";
  public float m_FuseMatValStart = 0.42f;
  public float m_FuseMatValFinish = -1.5f;
  public float m_FuseXamountAnimation = -1.5f;
  private const float BIRTH_ANIMATION_TIME = 1f;
  public GameObject m_SparksObject;
  public Transform m_SparksStartBone;
  public Transform m_SparksFinishBone;
  public GameObject m_FuseWickObject;
  public GameObject m_FuseShadowObject;
  private static TurnTimer s_instance;
  private Spell m_spell;
  private TurnTimerState m_state;
  private float m_countdownTimeoutSec;
  private float m_countdownEndTimestamp;
  private uint m_currentMoveAnimId;
  private uint m_currentMatValAnimId;
  private bool m_currentTimerBelongsToFriendlySidePlayer;
  private bool m_waitingForTurnStartManagerFinish;

  private void Awake()
  {
    TurnTimer.s_instance = this;
    this.m_spell = this.GetComponent<Spell>();
    this.m_spell.AddStateStartedCallback(new Spell.StateStartedCallback(this.OnSpellStateStarted));
    if (GameState.Get() == null)
      return;
    GameState.Get().RegisterCurrentPlayerChangedListener(new GameState.CurrentPlayerChangedCallback(this.OnCurrentPlayerChanged));
    GameState.Get().RegisterFriendlyTurnStartedListener(new GameState.FriendlyTurnStartedCallback(this.OnFriendlyTurnStarted), (object) null);
    GameState.Get().RegisterTurnTimerUpdateListener(new GameState.TurnTimerUpdateCallback(this.OnTurnTimerUpdate));
    GameState.Get().RegisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
  }

  private void OnDestroy()
  {
    TurnTimer.s_instance = (TurnTimer) null;
  }

  public static TurnTimer Get()
  {
    return TurnTimer.s_instance;
  }

  public bool HasCountdownTimeout()
  {
    return (double) this.m_countdownTimeoutSec > (double) Mathf.Epsilon;
  }

  public void OnEndTurnRequested()
  {
    if (!this.HasCountdownTimeout())
      return;
    this.ChangeState(TurnTimerState.KILL);
  }

  private void ChangeState(TurnTimerState state)
  {
    this.ChangeSpellState(state);
  }

  private void ChangeStateImpl(TurnTimerState state)
  {
    if (state == TurnTimerState.START)
      this.ChangeState_Start();
    else if (state == TurnTimerState.COUNTDOWN)
      this.ChangeState_Countdown();
    else if (state == TurnTimerState.TIMEOUT)
    {
      this.ChangeState_Timeout();
    }
    else
    {
      if (state != TurnTimerState.KILL)
        return;
      this.ChangeState_Kill();
    }
  }

  private void ChangeState_Start()
  {
    this.m_state = TurnTimerState.START;
    if (GameState.Get() == null)
      return;
    Card heroCard = GameState.Get().GetCurrentPlayer().GetHeroCard();
    if ((Object) heroCard != (Object) null)
      heroCard.PlayEmote(EmoteType.TIMER);
    this.m_currentTimerBelongsToFriendlySidePlayer = GameState.Get().IsFriendlySidePlayerTurn();
  }

  private void ChangeState_Countdown()
  {
    this.m_state = TurnTimerState.COUNTDOWN;
    this.m_countdownTimeoutSec = this.ComputeCountdownRemainingSec();
    this.RestartCountdownAnims(this.m_countdownTimeoutSec);
  }

  private void ChangeState_Timeout()
  {
    this.m_state = TurnTimerState.TIMEOUT;
    this.m_countdownEndTimestamp = 0.0f;
    if ((Object) EndTurnButton.Get() != (Object) null)
      EndTurnButton.Get().OnTurnTimerEnded(this.m_currentTimerBelongsToFriendlySidePlayer);
    this.StopCountdownAnims();
    double num = (double) this.UpdateCountdownAnims(0.0f);
  }

  private void ChangeState_Kill()
  {
    this.m_state = TurnTimerState.KILL;
    this.m_countdownEndTimestamp = 0.0f;
    this.StopCountdownAnims();
    double num = (double) this.UpdateCountdownAnims(0.0f);
  }

  private void ChangeSpellState(TurnTimerState timerState)
  {
    this.m_spell.ActivateState(this.TranslateTimerStateToSpellState(timerState));
    if (timerState != TurnTimerState.START)
      return;
    this.StartCoroutine(this.TimerBirthAnimateMaterialValues());
  }

  [DebuggerHidden]
  private IEnumerator TimerBirthAnimateMaterialValues()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TurnTimer.\u003CTimerBirthAnimateMaterialValues\u003Ec__IteratorE8() { \u003C\u003Ef__this = this };
  }

  private void OnSpellStateStarted(Spell spell, SpellStateType prevStateType, object userData)
  {
    this.ChangeStateImpl(this.TranslateSpellStateToTimerState(spell.GetActiveState()));
  }

  private SpellStateType TranslateTimerStateToSpellState(TurnTimerState timerState)
  {
    if (timerState == TurnTimerState.START)
      return SpellStateType.BIRTH;
    if (timerState == TurnTimerState.COUNTDOWN)
      return SpellStateType.IDLE;
    if (timerState == TurnTimerState.TIMEOUT)
      return SpellStateType.DEATH;
    return timerState == TurnTimerState.KILL ? SpellStateType.CANCEL : SpellStateType.NONE;
  }

  private TurnTimerState TranslateSpellStateToTimerState(SpellStateType spellState)
  {
    if (spellState == SpellStateType.BIRTH)
      return TurnTimerState.START;
    if (spellState == SpellStateType.IDLE)
      return TurnTimerState.COUNTDOWN;
    if (spellState == SpellStateType.DEATH)
      return TurnTimerState.TIMEOUT;
    return spellState == SpellStateType.CANCEL ? TurnTimerState.KILL : TurnTimerState.NONE;
  }

  private bool ShouldUpdateCountdownRemaining()
  {
    return this.m_state == TurnTimerState.COUNTDOWN;
  }

  private void StopCountdownAnims()
  {
    iTween.StopByName(this.m_SparksObject, this.GenerateMoveAnimName());
    iTween.StopByName(this.m_FuseWickObject, this.GenerateMatValAnimName());
  }

  private float UpdateCountdownAnims(float countdownRemainingSec)
  {
    float countdownProgress = this.ComputeCountdownProgress(countdownRemainingSec);
    this.m_SparksObject.transform.position = Vector3.Lerp(this.m_SparksFinishBone.position, this.m_SparksStartBone.position, countdownProgress);
    float num = Mathf.Lerp(this.m_FuseMatValFinish, this.m_FuseMatValStart, countdownProgress);
    this.m_FuseWickObject.GetComponent<Renderer>().material.SetFloat(this.m_FuseMatValName, num);
    this.m_FuseShadowObject.GetComponent<Renderer>().material.SetFloat(this.m_FuseMatValName, num);
    return num;
  }

  private void StartCountdownAnims(float startingMatVal, float countdownRemainingSec)
  {
    ++this.m_currentMoveAnimId;
    ++this.m_currentMatValAnimId;
    iTween.MoveTo(this.m_SparksObject, iTween.Hash((object) "name", (object) this.GenerateMoveAnimName(), (object) "time", (object) countdownRemainingSec, (object) "position", (object) this.m_SparksFinishBone.position, (object) "ignoretimescale", (object) true, (object) "easetype", (object) iTween.EaseType.linear));
    iTween.ValueTo(this.m_FuseWickObject, iTween.Hash((object) "name", (object) this.GenerateMatValAnimName(), (object) "time", (object) countdownRemainingSec, (object) "from", (object) startingMatVal, (object) "to", (object) this.m_FuseMatValFinish, (object) "ignoretimescale", (object) true, (object) "easetype", (object) iTween.EaseType.linear, (object) "onupdate", (object) "OnUpdateFuseMatVal", (object) "onupdatetarget", (object) this.gameObject));
  }

  private string GenerateMoveAnimName()
  {
    return string.Format("SparksMove{0}", (object) this.m_currentMoveAnimId);
  }

  private string GenerateMatValAnimName()
  {
    return string.Format("FuseMatVal{0}", (object) this.m_currentMatValAnimId);
  }

  private void OnUpdateFuseMatVal(float val)
  {
    this.m_FuseWickObject.GetComponent<Renderer>().material.SetFloat(this.m_FuseMatValName, val);
    this.m_FuseShadowObject.GetComponent<Renderer>().material.SetFloat(this.m_FuseMatValName, val);
  }

  private void RestartCountdownAnims(float countdownRemainingSec)
  {
    this.StopCountdownAnims();
    this.StartCountdownAnims(this.UpdateCountdownAnims(countdownRemainingSec), countdownRemainingSec);
  }

  private void UpdateCountdownTimeout()
  {
    this.m_countdownTimeoutSec = 0.0f;
    if (GameState.Get() == null)
      return;
    Player currentPlayer = GameState.Get().GetCurrentPlayer();
    if (currentPlayer == null || !currentPlayer.HasTag(GAME_TAG.TIMEOUT))
      return;
    this.m_countdownTimeoutSec = (float) currentPlayer.GetTag(GAME_TAG.TIMEOUT);
  }

  private float ComputeCountdownRemainingSec()
  {
    float num = this.m_countdownEndTimestamp - Time.realtimeSinceStartup;
    if ((double) num < 0.0)
      return 0.0f;
    return num;
  }

  private float ComputeCountdownProgress(float countdownRemainingSec)
  {
    if ((double) countdownRemainingSec <= (double) Mathf.Epsilon)
      return 0.0f;
    return countdownRemainingSec / this.m_countdownTimeoutSec;
  }

  private void OnCurrentPlayerChanged(Player player, object userData)
  {
    if (this.m_state == TurnTimerState.COUNTDOWN || this.m_state == TurnTimerState.START)
      this.ChangeState(TurnTimerState.KILL);
    this.UpdateCountdownTimeout();
  }

  private void OnFriendlyTurnStarted(object userData)
  {
    if (!this.HasCountdownTimeout())
      return;
    if (this.m_waitingForTurnStartManagerFinish)
      this.ChangeState(TurnTimerState.START);
    this.m_waitingForTurnStartManagerFinish = false;
  }

  private void OnTurnTimerUpdate(TurnTimerUpdate update, object userData)
  {
    this.m_countdownEndTimestamp = update.GetEndTimestamp();
    if (!update.ShouldShow())
    {
      if (this.m_state != TurnTimerState.COUNTDOWN && this.m_state != TurnTimerState.START)
        return;
      this.ChangeState(TurnTimerState.KILL);
    }
    else
    {
      float secondsRemaining = update.GetSecondsRemaining();
      if ((double) secondsRemaining <= (double) Mathf.Epsilon)
        this.OnTurnTimedOut();
      else if (this.m_state == TurnTimerState.COUNTDOWN)
        this.RestartCountdownAnims(secondsRemaining);
      else if (GameState.Get().IsTurnStartManagerActive())
        this.m_waitingForTurnStartManagerFinish = true;
      else
        this.ChangeState(TurnTimerState.START);
    }
  }

  private void OnTurnTimedOut()
  {
    if (!this.HasCountdownTimeout())
      return;
    this.ChangeState(TurnTimerState.TIMEOUT);
  }

  private void OnGameOver(TAG_PLAYSTATE playState, object userData)
  {
    if (this.m_state != TurnTimerState.COUNTDOWN && this.m_state != TurnTimerState.START)
      return;
    this.ChangeState(TurnTimerState.KILL);
  }
}
