﻿// Decompiled with JetBrains decompiler
// Type: CardBack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CardBack : MonoBehaviour
{
  public float m_EffectMinVelocity = 2f;
  public float m_EffectMaxVelocity = 40f;
  public Mesh m_CardBackMesh;
  public Material m_CardBackMaterial;
  public Material m_CardBackMaterial1;
  public Texture2D m_CardBackTexture;
  public Texture2D m_HiddenCardEchoTexture;
  public GameObject m_DragEffect;
}
