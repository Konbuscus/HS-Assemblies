﻿// Decompiled with JetBrains decompiler
// Type: AdventureClassChallenge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class AdventureClassChallenge : MonoBehaviour
{
  private readonly float[] EMPTY_SLOT_UV_OFFSET = new float[6]
  {
    0.0f,
    0.223f,
    0.377f,
    0.535f,
    0.69f,
    0.85f
  };
  private List<AdventureClassChallenge.ClassChallengeData> m_ClassChallenges = new List<AdventureClassChallenge.ClassChallengeData>();
  private Map<int, int> m_ScenarioChallengeLookup = new Map<int, int>();
  private const float CHALLENGE_BUTTON_OFFSET = 4.3f;
  private const int VISIBLE_SLOT_COUNT = 10;
  [CustomEditField(Sections = "DBF Stuff")]
  public UberText m_ModeName;
  [CustomEditField(Sections = "Class Challenge Buttons")]
  public GameObject m_ClassChallengeButtonPrefab;
  [CustomEditField(Sections = "Class Challenge Buttons")]
  public Vector3 m_ClassChallengeButtonSpacing;
  [CustomEditField(Sections = "Class Challenge Buttons")]
  public GameObject m_ChallengeButtonContainer;
  [CustomEditField(Sections = "Class Challenge Buttons")]
  public GameObject m_EmptyChallengeButtonSlot;
  [CustomEditField(Sections = "Class Challenge Buttons")]
  public float m_ChallengeButtonHeight;
  [CustomEditField(Sections = "Class Challenge Buttons")]
  public UIBScrollable m_ChallengeButtonScroller;
  [CustomEditField(Sections = "Hero Portraits")]
  public GameObject m_LeftHeroContainer;
  [CustomEditField(Sections = "Hero Portraits")]
  public GameObject m_RightHeroContainer;
  [CustomEditField(Sections = "Hero Portraits")]
  public UberText m_LeftHeroName;
  [CustomEditField(Sections = "Hero Portraits")]
  public UberText m_RightHeroName;
  [CustomEditField(Sections = "Versus Text", T = EditType.GAME_OBJECT)]
  public string m_VersusTextPrefab;
  [CustomEditField(Sections = "Versus Text")]
  public GameObject m_VersusTextContainer;
  [CustomEditField(Sections = "Versus Text")]
  public Color m_VersusTextColor;
  [CustomEditField(Sections = "Text")]
  public UberText m_ChallengeTitle;
  [CustomEditField(Sections = "Text")]
  public UberText m_ChallengeDescription;
  [CustomEditField(Sections = "Basic UI")]
  public PlayButton m_PlayButton;
  [CustomEditField(Sections = "Basic UI")]
  public UIBButton m_BackButton;
  [CustomEditField(Sections = "Reward UI")]
  public AdventureClassChallengeChestButton m_ChestButton;
  [CustomEditField(Sections = "Reward UI")]
  public GameObject m_ChestButtonCover;
  [CustomEditField(Sections = "Reward UI")]
  public Transform m_RewardBone;
  private int m_UVoffset;
  private AdventureClassChallengeButton m_SelectedButton;
  private GameObject m_LeftHero;
  private GameObject m_RightHero;
  private int m_SelectedScenario;
  private bool m_gameDenied;

  private void Awake()
  {
    this.transform.position = new Vector3(-500f, 0.0f, 0.0f);
    this.m_BackButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.BackButton()));
    this.m_PlayButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.Play()));
    this.m_EmptyChallengeButtonSlot.SetActive(false);
    AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(this.m_VersusTextPrefab), new AssetLoader.GameObjectCallback(this.OnVersusLettersLoaded), (object) null, false);
  }

  private void Start()
  {
    GameMgr.Get().RegisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
    this.InitModeName();
    this.InitAdventureChallenges();
    Box.Get().AddTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    if (AdventureConfig.Get().GetSelectedAdventure() == AdventureDbId.RETURNING_PLAYER)
    {
      this.m_BackButton.SetEnabled(false);
      if ((bool) UniversalInputManager.UsePhoneUI)
        this.m_BackButton.gameObject.SetActive(false);
      else
        this.m_BackButton.Flip(false);
    }
    else
      Navigation.PushUnique(new Navigation.NavigateBackHandler(AdventureClassChallenge.OnNavigateBack));
    this.StartCoroutine(this.CreateChallengeButtons());
  }

  private void OnDestroy()
  {
    GameMgr.Get().UnregisterFindGameEvent(new GameMgr.FindGameCallback(this.OnFindGameEvent));
  }

  private void InitModeName()
  {
    AdventureDataDbfRecord adventureDataRecord = GameUtils.GetAdventureDataRecord((int) AdventureConfig.Get().GetSelectedAdventure(), 4);
    this.m_ModeName.Text = (string) (!(bool) UniversalInputManager.UsePhoneUI ? adventureDataRecord.Name : adventureDataRecord.ShortName);
  }

  private void InitAdventureChallenges()
  {
    List<ScenarioDbfRecord> records = GameDbf.Scenario.GetRecords();
    records.Sort((Comparison<ScenarioDbfRecord>) ((a, b) => a.SortOrder - b.SortOrder));
    using (List<ScenarioDbfRecord>.Enumerator enumerator = records.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ScenarioDbfRecord current = enumerator.Current;
        if ((AdventureDbId) current.AdventureId == AdventureConfig.Get().GetSelectedAdventure() && current.ModeId == 4)
        {
          int player1HeroCardId = current.Player1HeroCardId;
          int player2HeroCardId = current.ClientPlayer2HeroCardId;
          if (player2HeroCardId == 0)
            player2HeroCardId = current.Player2HeroCardId;
          AdventureClassChallenge.ClassChallengeData classChallengeData = new AdventureClassChallenge.ClassChallengeData();
          classChallengeData.scenarioRecord = current;
          classChallengeData.heroID0 = GameUtils.TranslateDbIdToCardId(player1HeroCardId);
          classChallengeData.heroID1 = GameUtils.TranslateDbIdToCardId(player2HeroCardId);
          classChallengeData.unlocked = AdventureProgressMgr.Get().CanPlayScenario(current.ID);
          classChallengeData.defeated = AdventureProgressMgr.Get().HasDefeatedScenario(current.ID);
          classChallengeData.name = (string) current.ShortName;
          classChallengeData.title = (string) current.Name;
          classChallengeData.description = (string) current.Description;
          classChallengeData.completedDescription = (string) current.CompletedDescription;
          classChallengeData.opponentName = (string) current.OpponentName;
          this.m_ScenarioChallengeLookup.Add(current.ID, this.m_ClassChallenges.Count);
          this.m_ClassChallenges.Add(classChallengeData);
        }
      }
    }
  }

  private int BossCreateParamsSortComparison(AdventureClassChallenge.ClassChallengeData data1, AdventureClassChallenge.ClassChallengeData data2)
  {
    return GameUtils.MissionSortComparison(data1.scenarioRecord, data2.scenarioRecord);
  }

  [DebuggerHidden]
  private IEnumerator CreateChallengeButtons()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdventureClassChallenge.\u003CCreateChallengeButtons\u003Ec__Iterator1()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ButtonPressed(UIEvent e)
  {
    if ((UnityEngine.Object) this.m_ChallengeButtonScroller != (UnityEngine.Object) null && this.m_ChallengeButtonScroller.IsTouchDragging())
      return;
    AdventureClassChallengeButton element = (AdventureClassChallengeButton) e.GetElement();
    this.m_SelectedButton.Deselect();
    this.SetSelectedButton(element);
    element.Select(true);
    this.m_SelectedScenario = element.m_ScenarioID;
    this.m_SelectedButton = element;
    this.GetRewardCardForSelectedScenario();
  }

  private void SetSelectedButton(AdventureClassChallengeButton button)
  {
    int scenarioId = button.m_ScenarioID;
    AdventureConfig.Get().SetMission((ScenarioDbId) scenarioId, true);
    this.SetScenario(scenarioId);
  }

  private void LoadButtonPortrait(AdventureClassChallengeButton button, string heroID)
  {
    DefLoader.Get().LoadFullDef(heroID, new DefLoader.LoadDefCallback<FullDef>(this.OnButtonFullDefLoaded), (object) button);
  }

  private void OnButtonFullDefLoaded(string cardId, FullDef fullDef, object userData)
  {
    AdventureClassChallengeButton classChallengeButton = (AdventureClassChallengeButton) userData;
    CardDef cardDef = fullDef.GetCardDef();
    Material practiceAiPortrait = cardDef.GetPracticeAIPortrait();
    if (!((UnityEngine.Object) practiceAiPortrait != (UnityEngine.Object) null))
      return;
    practiceAiPortrait.mainTexture = cardDef.GetPortraitTexture();
    classChallengeButton.SetPortraitMaterial(practiceAiPortrait);
  }

  private void SetScenario(int scenarioID)
  {
    AdventureClassChallenge.ClassChallengeData classChallenge = this.m_ClassChallenges[this.m_ScenarioChallengeLookup[scenarioID]];
    this.LoadHero(0, classChallenge.heroID0);
    this.LoadHero(1, classChallenge.heroID1);
    this.m_RightHeroName.Text = classChallenge.opponentName;
    this.m_ChallengeTitle.Text = classChallenge.title;
    this.m_ChallengeDescription.Text = !classChallenge.defeated ? classChallenge.description : classChallenge.completedDescription;
    if ((bool) UniversalInputManager.UsePhoneUI)
      return;
    List<CardRewardData> defeatingScenario = AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario(scenarioID);
    bool flag = defeatingScenario != null && defeatingScenario.Count > 0;
    if (this.m_ClassChallenges[this.m_ScenarioChallengeLookup[scenarioID]].defeated || !flag)
    {
      this.m_ChestButton.gameObject.SetActive(false);
      this.m_ChestButtonCover.SetActive(true);
    }
    else
    {
      this.m_ChestButton.gameObject.SetActive(true);
      this.m_ChestButtonCover.SetActive(false);
    }
  }

  private void LoadHero(int heroNum, string heroID)
  {
    DefLoader.Get().LoadFullDef(heroID, new DefLoader.LoadDefCallback<FullDef>(this.OnHeroFullDefLoaded), (object) new AdventureClassChallenge.HeroLoadData()
    {
      heroNum = heroNum,
      heroID = heroID
    });
  }

  private void OnHeroFullDefLoaded(string cardId, FullDef fullDef, object userData)
  {
    if (fullDef == null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("AdventureClassChallenge.OnHeroFullDefLoaded() - FAILED to load \"{0}\"", (object) cardId));
    }
    else
    {
      AdventureClassChallenge.HeroLoadData heroLoadData = (AdventureClassChallenge.HeroLoadData) userData;
      heroLoadData.fulldef = fullDef;
      AssetLoader.Get().LoadActor("Card_Play_Hero", new AssetLoader.GameObjectCallback(this.OnActorLoaded), (object) heroLoadData, false);
    }
  }

  private void OnActorLoaded(string name, GameObject actorObject, object userData)
  {
    AdventureClassChallenge.HeroLoadData heroLoadData = (AdventureClassChallenge.HeroLoadData) userData;
    if ((UnityEngine.Object) actorObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("AdventureClassChallenge.OnActorLoaded() - FAILED to load actor \"{0}\"", (object) name));
    }
    else
    {
      Actor component = actorObject.GetComponent<Actor>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("AdventureClassChallenge.OnActorLoaded() - ERROR actor \"{0}\" has no Actor component", (object) name));
      }
      else
      {
        component.TurnOffCollider();
        component.SetUnlit();
        UnityEngine.Object.Destroy((UnityEngine.Object) component.m_healthObject);
        UnityEngine.Object.Destroy((UnityEngine.Object) component.m_attackObject);
        component.SetEntityDef(heroLoadData.fulldef.GetEntityDef());
        component.SetCardDef(heroLoadData.fulldef.GetCardDef());
        component.SetPremium(TAG_PREMIUM.NORMAL);
        component.UpdateAllComponents();
        GameObject parent = this.m_LeftHeroContainer;
        if (heroLoadData.heroNum == 0)
        {
          UnityEngine.Object.Destroy((UnityEngine.Object) this.m_LeftHero);
          this.m_LeftHero = actorObject;
          this.m_LeftHeroName.Text = heroLoadData.fulldef.GetEntityDef().GetName();
        }
        else
        {
          UnityEngine.Object.Destroy((UnityEngine.Object) this.m_RightHero);
          this.m_RightHero = actorObject;
          parent = this.m_RightHeroContainer;
        }
        GameUtils.SetParent((Component) component, parent, false);
        component.transform.localRotation = Quaternion.identity;
        component.transform.localScale = Vector3.one;
        component.GetAttackObject().Hide();
        component.Show();
      }
    }
  }

  private void OnVersusLettersLoaded(string name, GameObject go, object userData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) string.Format("AdventureClassChallenge.OnVersusLettersLoaded() - FAILED to load \"{0}\"", (object) name));
    }
    else
    {
      GameUtils.SetParent(go, this.m_VersusTextContainer, false);
      go.GetComponentInChildren<VS>().ActivateShadow(true);
      go.transform.localRotation = Quaternion.identity;
      go.transform.Rotate(new Vector3(0.0f, 180f, 0.0f));
      go.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
      Component[] componentsInChildren = go.GetComponentsInChildren(typeof (Renderer));
      for (int index = 0; index < componentsInChildren.Length - 1; ++index)
        ((Renderer) componentsInChildren[index]).material.SetColor("_Color", this.m_VersusTextColor);
    }
  }

  private static bool OnNavigateBack()
  {
    AdventureConfig.Get().ChangeToLastSubScene(true);
    return true;
  }

  private void BackButton()
  {
    Navigation.GoBack();
  }

  private void Play()
  {
    this.m_PlayButton.Disable();
    GameMgr.Get().FindGame(GameType.GT_VS_AI, FormatType.FT_WILD, this.m_SelectedScenario, 0L, 0L);
  }

  private bool OnFindGameEvent(FindGameEventData eventData, object userData)
  {
    if (eventData.m_state == FindGameState.INVALID)
      this.m_PlayButton.Enable();
    return false;
  }

  private void GetRewardCardForSelectedScenario()
  {
    if ((UnityEngine.Object) this.m_RewardBone == (UnityEngine.Object) null)
      return;
    this.m_ChestButton.m_IsRewardLoading = true;
    List<CardRewardData> defeatingScenario = AdventureProgressMgr.Get().GetImmediateCardRewardsForDefeatingScenario(this.m_SelectedScenario);
    if (defeatingScenario == null || defeatingScenario.Count <= 0)
      return;
    defeatingScenario[0].LoadRewardObject(new Reward.DelOnRewardLoaded(this.RewardCardLoaded));
  }

  private void RewardCardLoaded(Reward reward, object callbackData)
  {
    if ((UnityEngine.Object) reward == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) string.Format("AdventureClassChallenge.RewardCardLoaded() - FAILED to load reward \"{0}\"", (object) this.name));
    else if ((UnityEngine.Object) reward.gameObject == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("AdventureClassChallenge.RewardCardLoaded() - Reward GameObject is null \"{0}\"", (object) this.name));
    }
    else
    {
      reward.gameObject.transform.parent = this.m_ChestButton.transform;
      CardReward component = reward.GetComponent<CardReward>();
      if ((UnityEngine.Object) this.m_ChestButton.m_RewardCard != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.m_ChestButton.m_RewardCard);
      this.m_ChestButton.m_RewardCard = component.m_nonHeroCardsRoot;
      GameUtils.SetParent(component.m_nonHeroCardsRoot, (Component) this.m_RewardBone, false);
      component.m_nonHeroCardsRoot.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
      this.m_ChestButton.m_IsRewardLoading = false;
    }
  }

  private void OnBoxTransitionFinished(object userData)
  {
    Box.Get().RemoveTransitionFinishedListener(new Box.TransitionFinishedCallback(this.OnBoxTransitionFinished));
    if (AdventureConfig.Get().GetSelectedAdventure() != AdventureDbId.RETURNING_PLAYER)
      return;
    ReturningPlayerMgr.Get().PlayReturningPlayerInnkeeperChallengeIntroIfNecessary();
  }

  private class ClassChallengeData
  {
    public ScenarioDbfRecord scenarioRecord;
    public bool unlocked;
    public bool defeated;
    public string heroID0;
    public string heroID1;
    public string name;
    public string title;
    public string description;
    public string completedDescription;
    public string opponentName;
  }

  private class HeroLoadData
  {
    public int heroNum;
    public string heroID;
    public FullDef fulldef;
  }
}
