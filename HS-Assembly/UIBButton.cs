﻿// Decompiled with JetBrains decompiler
// Type: UIBButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

[CustomEditClass]
[RequireComponent(typeof (BoxCollider))]
public class UIBButton : PegUIElement
{
  [CustomEditField(Sections = "Click Depress Behavior")]
  public Vector3 m_ClickDownOffset = new Vector3(0.0f, -0.05f, 0.0f);
  [CustomEditField(Sections = "Click Depress Behavior")]
  public float m_RaiseTime = 0.1f;
  [CustomEditField(Sections = "Click Depress Behavior")]
  public float m_DepressTime = 0.1f;
  [CustomEditField(Sections = "Wiggle Behavior")]
  public Vector3 m_WiggleAmount = new Vector3(90f, 0.0f, 0.0f);
  [CustomEditField(Sections = "Wiggle Behavior")]
  public float m_WiggleTime = 0.5f;
  [CustomEditField(Sections = "Flip Enable Behavior")]
  public Vector3 m_DisabledRotation = Vector3.zero;
  [CustomEditField(Sections = "Flip Enable Behavior")]
  public float m_AnimateFlipTime = 0.25f;
  [CustomEditField(Sections = "Button Objects")]
  public GameObject m_RootObject;
  [CustomEditField(Sections = "Text Object")]
  public UberText m_ButtonText;
  [CustomEditField(Sections = "Roll Over Depress Behavior")]
  public bool m_DepressOnOver;
  [CustomEditField(Sections = "Flip Enable Behavior")]
  public bool m_AnimateFlip;
  private Vector3? m_RootObjectOriginalPosition;
  private Vector3? m_RootObjectOriginalRotation;
  private bool m_Depressed;

  protected override void OnPress()
  {
    if (this.m_DepressOnOver)
      return;
    this.Depress();
  }

  protected override void OnRelease()
  {
    if (this.m_DepressOnOver)
      return;
    this.Raise();
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    if (!this.m_Depressed)
      return;
    this.Raise();
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    if (this.m_DepressOnOver)
      this.Depress();
    this.Wiggle();
  }

  public void Select()
  {
    this.Depress();
  }

  public void Deselect()
  {
    this.Raise();
  }

  public void Flip(bool faceUp)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    UIBButton.\u003CFlip\u003Ec__AnonStorey39B flipCAnonStorey39B = new UIBButton.\u003CFlip\u003Ec__AnonStorey39B();
    // ISSUE: reference to a compiler-generated field
    flipCAnonStorey39B.\u003C\u003Ef__this = this;
    if ((UnityEngine.Object) this.m_RootObject == (UnityEngine.Object) null)
      return;
    this.InitOriginalRotation();
    // ISSUE: reference to a compiler-generated field
    flipCAnonStorey39B.targetLocalEulers = !faceUp ? this.m_RootObjectOriginalRotation.Value + this.m_DisabledRotation : this.m_RootObjectOriginalRotation.Value;
    iTween.StopByName(this.m_RootObject, "flip");
    if (this.m_AnimateFlip)
    {
      // ISSUE: reference to a compiler-generated method
      iTween.RotateAdd(this.m_RootObject, iTween.Hash((object) "amount", (object) (!faceUp ? this.m_DisabledRotation : -this.m_DisabledRotation), (object) "time", (object) this.m_AnimateFlipTime, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true, (object) "name", (object) "flip", (object) "oncomplete", (object) new Action<object>(flipCAnonStorey39B.\u003C\u003Em__EA)));
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      this.m_RootObject.transform.localEulerAngles = flipCAnonStorey39B.targetLocalEulers;
    }
  }

  public void SetText(string text)
  {
    if (!((UnityEngine.Object) this.m_ButtonText != (UnityEngine.Object) null))
      return;
    this.m_ButtonText.Text = text;
  }

  public string GetText()
  {
    if (this.m_ButtonText.GameStringLookup)
      return GameStrings.Get(this.m_ButtonText.Text);
    return this.m_ButtonText.Text;
  }

  private void Raise()
  {
    if ((UnityEngine.Object) this.m_RootObject == (UnityEngine.Object) null || !this.m_Depressed)
      return;
    this.m_Depressed = false;
    iTween.StopByName(this.m_RootObject, "depress");
    if ((double) this.m_RaiseTime > 0.0)
      iTween.MoveTo(this.m_RootObject, iTween.Hash((object) "position", (object) this.m_RootObjectOriginalPosition, (object) "time", (object) this.m_RaiseTime, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true, (object) "name", (object) "depress"));
    else
      this.m_RootObject.transform.localPosition = this.m_RootObjectOriginalPosition.Value;
  }

  private void Depress()
  {
    if ((UnityEngine.Object) this.m_RootObject == (UnityEngine.Object) null || this.m_Depressed || (bool) UniversalInputManager.UsePhoneUI)
      return;
    this.InitOriginalPosition();
    this.m_Depressed = true;
    iTween.StopByName(this.m_RootObject, "depress");
    Vector3 vector3 = this.m_RootObjectOriginalPosition.Value + this.m_ClickDownOffset;
    if ((double) this.m_DepressTime > 0.0)
      iTween.MoveTo(this.m_RootObject, iTween.Hash((object) "position", (object) vector3, (object) "time", (object) this.m_DepressTime, (object) "easeType", (object) iTween.EaseType.linear, (object) "isLocal", (object) true, (object) "name", (object) "depress"));
    else
      this.m_RootObject.transform.localPosition = vector3;
  }

  private void Wiggle()
  {
    if ((UnityEngine.Object) this.m_RootObject == (UnityEngine.Object) null || (double) this.m_WiggleAmount.sqrMagnitude == 0.0 || ((double) this.m_WiggleTime <= 0.0 || (bool) UniversalInputManager.UsePhoneUI))
      return;
    this.InitOriginalRotation();
    Hashtable args = iTween.Hash((object) "amount", (object) this.m_WiggleAmount, (object) "time", (object) this.m_WiggleTime, (object) "name", (object) "wiggle");
    iTween.StopByName(this.m_RootObject, "wiggle");
    this.m_RootObject.transform.localEulerAngles = this.m_RootObjectOriginalRotation.Value;
    iTween.PunchRotation(this.m_RootObject, args);
  }

  private void InitOriginalRotation()
  {
    if ((UnityEngine.Object) this.m_RootObject == (UnityEngine.Object) null || this.m_RootObjectOriginalRotation.HasValue)
      return;
    this.m_RootObjectOriginalRotation = new Vector3?(this.m_RootObject.transform.localEulerAngles);
  }

  private void InitOriginalPosition()
  {
    if ((UnityEngine.Object) this.m_RootObject == (UnityEngine.Object) null || this.m_RootObjectOriginalPosition.HasValue)
      return;
    this.m_RootObjectOriginalPosition = new Vector3?(this.m_RootObject.transform.localPosition);
  }
}
