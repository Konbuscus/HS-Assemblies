﻿// Decompiled with JetBrains decompiler
// Type: SetFilterTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SetFilterTray : MonoBehaviour
{
  private bool m_showWild = true;
  private List<SetFilterItem> m_items = new List<SetFilterItem>();
  private HashSet<TAG_CARD_SET> m_setsWithOwnedCards = new HashSet<TAG_CARD_SET>();
  public UIBScrollable m_scroller;
  public GameObject m_contents;
  public CollectionSetFilterDropdownToggle m_toggleButton;
  public PegUIElement m_hideArea;
  public GameObject m_trayObject;
  public GameObject m_contentsBone;
  public GameObject m_headerPrefab;
  public GameObject m_itemPrefab;
  public GameObject m_showBone;
  public GameObject m_hideBone;
  private bool m_shown;
  private bool m_editingDeck;
  private bool m_showUnownedSets;
  private bool m_isAnimating;
  private float m_lastCollectionQueryTime;
  private SetFilterItem m_selected;
  private SetFilterItem m_lastSelected;

  private void Awake()
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_toggleButton.AddEventListener(UIEventType.PRESS, (UIEvent.Handler) (e => this.Show(true)));
      this.m_hideArea.AddEventListener(UIEventType.PRESS, (UIEvent.Handler) (e => this.Show(false)));
      this.m_trayObject.SetActive(false);
    }
    else
      this.m_hideArea.gameObject.SetActive(false);
    this.m_toggleButton.gameObject.SetActive(false);
  }

  public void SetButtonShown(bool isShown)
  {
    this.m_toggleButton.gameObject.SetActive(isShown);
  }

  public void SetButtonEnabled(bool isEnabled)
  {
    this.m_toggleButton.SetEnabled(isEnabled);
    this.m_toggleButton.SetEnabledVisual(isEnabled);
  }

  public void AddHeader(string headerName, bool isWild)
  {
    GameObject child = Object.Instantiate<GameObject>(this.m_headerPrefab);
    GameUtils.SetParent(child, this.m_contents, false);
    child.SetActive(false);
    SetFilterItem component1 = child.GetComponent<SetFilterItem>();
    UIBScrollableItem component2 = child.GetComponent<UIBScrollableItem>();
    component1.IsHeader = true;
    component1.Text = headerName;
    component1.Height = component2.m_size.z;
    component1.IsWild = isWild;
    this.m_items.Add(component1);
  }

  public void AddItem(string itemName, Vector2? iconOffset, SetFilterItem.ItemSelectedCallback callback, List<TAG_CARD_SET> data, bool isWild, bool isAllStandard = false)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SetFilterTray.\u003CAddItem\u003Ec__AnonStorey3A9 itemCAnonStorey3A9 = new SetFilterTray.\u003CAddItem\u003Ec__AnonStorey3A9();
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.\u003C\u003Ef__this = this;
    GameObject child = Object.Instantiate<GameObject>(this.m_itemPrefab);
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.item = child.GetComponent<SetFilterItem>();
    GameUtils.SetParent(child, this.m_contents, false);
    child.SetActive(false);
    SetFilterItem component1 = child.GetComponent<SetFilterItem>();
    UIBScrollableItem component2 = child.GetComponent<UIBScrollableItem>();
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.item.IsHeader = false;
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.item.Text = itemName;
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.item.Height = component2.m_size.z;
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.item.IsWild = isWild;
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.item.IsAllStandard = isAllStandard;
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.item.CardSets = data;
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.item.Callback = callback;
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey3A9.item.IconOffset = iconOffset;
    // ISSUE: reference to a compiler-generated field
    this.m_items.Add(itemCAnonStorey3A9.item);
    // ISSUE: reference to a compiler-generated method
    component1.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(itemCAnonStorey3A9.\u003C\u003Em__10E));
  }

  public void SelectFirstItem()
  {
    using (List<SetFilterItem>.Enumerator enumerator = this.m_items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SetFilterItem current = enumerator.Current;
        if (!current.IsHeader)
        {
          UIBScrollableItem component = current.GetComponent<UIBScrollableItem>();
          if ((Object) component != (Object) null && component.m_active == UIBScrollableItem.ActiveState.Active)
          {
            this.Select(current, true);
            break;
          }
        }
      }
    }
  }

  public bool HasActiveFilter()
  {
    using (List<SetFilterItem>.Enumerator enumerator = this.m_items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SetFilterItem current = enumerator.Current;
        if (!current.IsHeader && current.isActiveAndEnabled)
          return !((Object) current == (Object) this.m_selected);
      }
    }
    return false;
  }

  public void Select(SetFilterItem item, bool callCallback = true)
  {
    if ((Object) item == (Object) this.m_selected)
      return;
    if ((Object) this.m_selected != (Object) null)
    {
      this.m_selected.SetSelected(false);
      this.m_lastSelected = this.m_selected;
    }
    this.m_selected = item;
    item.SetSelected(true);
    if (callCallback)
      item.Callback((object) item.CardSets, item.IsWild);
    this.m_toggleButton.SetToggleIconOffset(new Vector2?(item.IconOffset.Value));
  }

  public void SelectPreviouslySelectedItem()
  {
    this.Select(this.m_lastSelected, false);
  }

  public void UpdateSetFilters(bool showWild, bool editingDeck, bool showUnownedSets)
  {
    if (this.m_showWild == showWild && this.m_editingDeck == editingDeck && this.m_showUnownedSets == showUnownedSets)
      return;
    this.m_showWild = showWild;
    this.m_editingDeck = editingDeck;
    this.m_showUnownedSets = showUnownedSets;
    this.Arrange();
  }

  public void ClearFilter()
  {
    this.SelectFirstItem();
    if (!(bool) UniversalInputManager.UsePhoneUI)
      return;
    this.SetButtonShown(false);
  }

  public void Show(bool show)
  {
    if (!(bool) UniversalInputManager.UsePhoneUI)
    {
      if (this.m_isAnimating)
        return;
      this.m_shown = show;
      this.m_trayObject.SetActive(true);
      this.m_hideArea.gameObject.SetActive(true);
      UIBHighlight component = this.m_toggleButton.GetComponent<UIBHighlight>();
      if ((Object) component != (Object) null)
        component.AlwaysOver = show;
      this.m_isAnimating = true;
      if (show)
      {
        this.Arrange();
        this.m_trayObject.transform.localPosition = this.m_hideBone.transform.localPosition;
        iTween.MoveTo(this.m_trayObject, iTween.Hash((object) "position", (object) this.m_showBone.transform.localPosition, (object) "time", (object) 0.35f, (object) "easeType", (object) iTween.EaseType.easeOutCubic, (object) "isLocal", (object) true, (object) "oncomplete", (object) "FinishFilterShown", (object) "oncompletetarget", (object) this.gameObject));
        SoundManager.Get().LoadAndPlay("choose_opponent_panel_slide_on", this.gameObject);
      }
      else
      {
        this.m_trayObject.transform.localPosition = this.m_showBone.transform.localPosition;
        iTween.MoveTo(this.m_trayObject, iTween.Hash((object) "position", (object) this.m_hideBone.transform.localPosition, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutCubic, (object) "isLocal", (object) true, (object) "oncomplete", (object) "FinishFilterHidden", (object) "oncompletetarget", (object) this.gameObject));
        SoundManager.Get().LoadAndPlay("choose_opponent_panel_slide_off", this.gameObject);
      }
      this.m_hideArea.gameObject.SetActive(this.m_shown);
    }
    else
    {
      this.m_shown = show;
      if (show)
        this.Arrange();
    }
    CollectionManagerDisplay.Get().HideSetFilterTutorial();
  }

  public bool IsShown()
  {
    return this.m_shown;
  }

  private void FinishFilterShown()
  {
    this.m_isAnimating = false;
  }

  private void FinishFilterHidden()
  {
    this.m_isAnimating = false;
    this.m_trayObject.SetActive(false);
    this.m_hideArea.gameObject.SetActive(false);
  }

  private void Arrange()
  {
    this.m_scroller.ClearVisibleAffectObjects();
    if (!this.m_showUnownedSets)
      this.EvaluateOwnership();
    Vector3 position = this.m_contentsBone.transform.position;
    bool flag = false;
    using (List<SetFilterItem>.Enumerator enumerator = this.m_items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SetFilterItem current = enumerator.Current;
        UIBScrollableItem component = current.GetComponent<UIBScrollableItem>();
        if ((Object) component == (Object) null)
          Debug.LogWarning((object) "SetFilterItem has no UIBScrollableItem component!");
        else if (current.IsWild && !this.m_showWild || this.m_showWild && this.m_editingDeck && current.IsAllStandard || !this.m_showUnownedSets && !this.OwnCardInSetsForItem(current))
        {
          if ((Object) current == (Object) this.m_selected)
            flag = true;
          current.gameObject.SetActive(false);
          component.m_active = UIBScrollableItem.ActiveState.Inactive;
        }
        else
        {
          current.gameObject.SetActive(true);
          component.m_active = UIBScrollableItem.ActiveState.Active;
          current.gameObject.transform.position = position;
          position.z -= current.Height;
          this.m_scroller.AddVisibleAffectedObject(current.gameObject, new Vector3(current.Height, current.Height, current.Height), true, (UIBScrollable.VisibleAffected) null);
        }
      }
    }
    if (flag)
      this.SelectFirstItem();
    this.m_scroller.UpdateAndFireVisibleAffectedObjects();
  }

  private void EvaluateOwnership()
  {
    if ((double) this.m_lastCollectionQueryTime > (double) CollectionManager.Get().CollectionLastModifiedTime())
      return;
    this.m_setsWithOwnedCards.Clear();
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    List<CollectibleCard> allCards = CollectionManager.Get().GetAllCards();
    for (int index = 0; index < allCards.Count; ++index)
    {
      CollectibleCard collectibleCard = allCards[index];
      if (collectibleCard.OwnedCount > 0)
        this.m_setsWithOwnedCards.Add(collectibleCard.Set);
    }
    Log.JMac.Print("SetFilterTray - Evaluating Ownership took {0} seconds.", (object) (float) ((double) Time.realtimeSinceStartup - (double) realtimeSinceStartup));
    this.m_lastCollectionQueryTime = Time.realtimeSinceStartup;
  }

  private bool OwnCardInSetsForItem(SetFilterItem item)
  {
    if (item.CardSets == null)
      return true;
    for (int index = 0; index < item.CardSets.Count; ++index)
    {
      if (this.m_setsWithOwnedCards.Contains(item.CardSets[index]))
        return true;
    }
    return false;
  }
}
