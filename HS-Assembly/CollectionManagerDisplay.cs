﻿// Decompiled with JetBrains decompiler
// Type: CollectionManagerDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class CollectionManagerDisplay : MonoBehaviour
{
  [CustomEditField(Sections = "Controls")]
  public CollectionPageLayoutSettings m_pageLayoutSettings = new CollectionPageLayoutSettings();
  [CustomEditField(ListTable = true, Sections = "Materials")]
  public List<CollectionManagerDisplay.CardSetIconMatOffset> m_manaFilterCardSetIcons = new List<CollectionManagerDisplay.CardSetIconMatOffset>();
  [CustomEditField(Sections = "Tavern Brawl Changes")]
  public List<GameObject> m_tavernBrawlObjectsToSwap = new List<GameObject>();
  private Map<TAG_CLASS, Texture> m_loadedClassTextures = new Map<TAG_CLASS, Texture>();
  private Map<TAG_CLASS, CollectionManagerDisplay.TextureRequests> m_requestedClassTextures = new Map<TAG_CLASS, CollectionManagerDisplay.TextureRequests>();
  private List<Actor> m_cardBackActors = new List<Actor>();
  private List<Actor> m_cardActors = new List<Actor>();
  private List<Actor> m_previousCardActors = new List<Actor>();
  private List<CollectionManagerDisplay.OnSwitchViewMode> m_switchViewModeListeners = new List<CollectionManagerDisplay.OnSwitchViewMode>();
  private List<CollectionManagerDisplay.FilterStateListener> m_searchFilterListeners = new List<CollectionManagerDisplay.FilterStateListener>();
  private List<CollectionManagerDisplay.FilterStateListener> m_setFilterListeners = new List<CollectionManagerDisplay.FilterStateListener>();
  private List<CollectionManagerDisplay.FilterStateListener> m_manaFilterListeners = new List<CollectionManagerDisplay.FilterStateListener>();
  private PlatformDependentValue<int> m_onscreenDecks = new PlatformDependentValue<int>(PlatformCategory.Screen)
  {
    PC = 8,
    Phone = 4
  };
  private const float CRAFTING_TRAY_SLIDE_IN_TIME = 0.25f;
  [CustomEditField(Sections = "Prefabs")]
  public CollectionCardVisual m_cardVisualPrefab;
  [CustomEditField(Sections = "Bones")]
  public GameObject m_activeSearchBone;
  [CustomEditField(Sections = "Bones")]
  public GameObject m_activeSearchBone_Win8;
  [CustomEditField(Sections = "Bones")]
  public GameObject m_craftingTrayHiddenBone;
  [CustomEditField(Sections = "Bones")]
  public GameObject m_craftingTrayShownBone;
  [CustomEditField(Sections = "Bones")]
  public Transform m_deckTemplateHiddenBone;
  [CustomEditField(Sections = "Bones")]
  public Transform m_deckTemplateShownBone;
  [CustomEditField(Sections = "Bones")]
  public Transform m_deckTemplateTutorialWelcomeBone;
  [CustomEditField(Sections = "Bones")]
  public Transform m_deckTemplateTutorialReminderBone;
  [CustomEditField(Sections = "Bones")]
  public Transform m_editDeckTutorialBone;
  [CustomEditField(Sections = "Bones")]
  public Transform m_convertDeckTutorialBone;
  [CustomEditField(Sections = "Bones")]
  public Transform m_setFilterTutorialBone;
  [CustomEditField(Sections = "Objects")]
  public ManaFilterTabManager m_manaTabManager;
  [CustomEditField(Sections = "Objects")]
  public CollectionPageManager m_pageManager;
  [CustomEditField(Sections = "Objects")]
  public CollectionCoverDisplay m_cover;
  [CustomEditField(Sections = "Objects")]
  public CollectionSearch m_search;
  [CustomEditField(Sections = "Objects")]
  public ActiveFilterButton m_filterButton;
  [CustomEditField(Sections = "Objects")]
  public CraftingModeButton m_craftingModeButton;
  [CustomEditField(Sections = "Objects")]
  public Notification m_deckTemplateCardReplacePopup;
  [CustomEditField(Sections = "Objects")]
  public PegUIElement m_inputBlocker;
  [CustomEditField(Sections = "Objects")]
  public NestedPrefab m_setFilterTrayContainer;
  [CustomEditField(Sections = "Controls")]
  public Vector2 m_allSetsIconOffset;
  [CustomEditField(Sections = "Controls")]
  public Vector2 m_wildSetsIconOffset;
  [CustomEditField(Sections = "Materials")]
  public Material m_goldenCardNotOwnedMeshMaterial;
  [CustomEditField(Sections = "Materials")]
  public Material m_cardNotOwnedMeshMaterial;
  [CustomEditField(Sections = "Tavern Brawl Changes")]
  public GameObject m_bookBack;
  [CustomEditField(Sections = "Tavern Brawl Changes", T = EditType.TEXTURE)]
  public string m_corkBackTexture;
  [CustomEditField(Sections = "Tavern Brawl Changes")]
  public Mesh m_tavernBrawlBookBackMesh;
  [CustomEditField(Sections = "Tavern Brawl Changes")]
  public Material m_tavernBrawlElements;
  private static CollectionManagerDisplay s_instance;
  private bool m_netCacheReady;
  private bool m_isReady;
  private bool m_unloading;
  private List<Actor> m_previousCardBackActors;
  private int m_displayRequestID;
  private bool m_selectingNewDeckHero;
  private long m_showDeckContentsRequest;
  private Notification m_deckHelpPopup;
  private Notification m_innkeeperLClickReminder;
  private bool m_setFilterTrayInitialized;
  private bool m_isCoverLoading;
  private CraftingTray m_craftingTray;
  private SetFilterTray m_setFilterTray;
  private CollectionManagerDisplay.ViewMode m_currentViewMode;
  private DeckTemplatePicker m_deckTemplatePickerPhone;
  private HeroPickerDisplay m_heroPickerDisplay;
  private int m_inputBlockers;
  private Notification m_createDeckNotification;
  private Notification m_convertTutorialPopup;
  private IEnumerator m_showConvertTutorialCoroutine;
  private Notification m_setFilterTutorialPopup;
  private IEnumerator m_showSetFilterTutorialCoroutine;
  private bool m_showingDeckTemplateTips;
  private bool m_showingDeckTemplateTipsPending;
  private bool m_searchTriggeredCraftingTray;

  private void Start()
  {
    NetCache.Get().RegisterScreenCollectionManager(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    CollectionManager.Get().RegisterCollectionNetHandlers();
    CollectionManager.Get().RegisterCollectionLoadedListener(new CollectionManager.DelOnCollectionLoaded(this.OnCollectionLoaded));
    CollectionManager.Get().RegisterCollectionChangedListener(new CollectionManager.DelOnCollectionChanged(this.OnCollectionChanged));
    CollectionManager.Get().RegisterDeckCreatedListener(new CollectionManager.DelOnDeckCreated(this.OnDeckCreated));
    CollectionManager.Get().RegisterDeckContentsListener(new CollectionManager.DelOnDeckContents(this.OnDeckContents));
    CollectionManager.Get().RegisterNewCardSeenListener(new CollectionManager.DelOnNewCardSeen(this.OnNewCardSeen));
    CollectionManager.Get().RegisterCardRewardInsertedListener(new CollectionManager.DelOnCardRewardInserted(this.OnCardRewardInserted));
    CardBackManager.Get().SetSearchText((string) null);
    this.m_inputBlocker.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnInputBlockerRelease));
    this.m_search.RegisterActivatedListener(new CollectionSearch.ActivatedListener(this.OnSearchActivated));
    this.m_search.RegisterDeactivatedListener(new CollectionSearch.DeactivatedListener(this.OnSearchDeactivated));
    this.m_search.RegisterClearedListener(new CollectionSearch.ClearedListener(this.OnSearchCleared));
    this.m_pageManager.LoadPagingArrows();
    if ((UnityEngine.Object) this.m_setFilterTrayContainer != (UnityEngine.Object) null)
      this.m_setFilterTray = this.m_setFilterTrayContainer.PrefabGameObject(true).GetComponentsInChildren<SetFilterTray>(true)[0];
    bool show = Options.Get().GetBool(Option.SHOW_ADVANCED_COLLECTIONMANAGER, false);
    this.ShowAdvancedCollectionManager(show);
    if (!show)
      Options.Get().RegisterChangedListener(Option.SHOW_ADVANCED_COLLECTIONMANAGER, new Options.ChangedCallback(this.OnShowAdvancedCMChanged));
    this.DoEnterCollectionManagerEvents();
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL)
      MusicManager.Get().StartPlaylist(MusicPlaylistType.UI_CollectionManager);
    if (CollectionManager.Get().ShouldShowWildToStandardTutorial(true))
      UserAttentionManager.StartBlocking(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS);
    this.StartCoroutine(this.WaitUntilReady());
  }

  private void Awake()
  {
    CollectionManagerDisplay.s_instance = this;
    if (GraphicsManager.Get().RenderQualityLevel != GraphicsQuality.Low && (UnityEngine.Object) this.m_cover == (UnityEngine.Object) null)
    {
      this.m_isCoverLoading = true;
      AssetLoader.Get().LoadGameObject("CollectionBookCover", new AssetLoader.GameObjectCallback(this.OnCoverLoaded), (object) null, false);
    }
    if ((bool) UniversalInputManager.UsePhoneUI)
    {
      this.m_deckTemplatePickerPhone = AssetLoader.Get().LoadGameObject("DeckTemplate_phone", true, false).GetComponent<DeckTemplatePicker>();
      SlidingTray component = this.m_deckTemplatePickerPhone.GetComponent<SlidingTray>();
      component.m_trayHiddenBone = this.m_deckTemplateHiddenBone.transform;
      component.m_trayShownBone = this.m_deckTemplateShownBone.transform;
    }
    this.LoadAllClassTextures();
    this.EnableInput(true);
    this.StartCoroutine(this.InitCollectionWhenReady());
    this.SetTavernBrawlTexturesIfNecessary();
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) this.m_deckTemplatePickerPhone != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_deckTemplatePickerPhone.gameObject);
      this.m_deckTemplatePickerPhone = (DeckTemplatePicker) null;
    }
    CollectionManagerDisplay.s_instance = (CollectionManagerDisplay) null;
    UserAttentionManager.StopBlocking(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS);
  }

  private void Update()
  {
    if (!ApplicationMgr.IsInternal())
      return;
    if (Input.GetKeyDown(KeyCode.Alpha1))
      this.SetViewMode(CollectionManagerDisplay.ViewMode.HERO_SKINS, (CollectionManagerDisplay.ViewModeData) null);
    else if (Input.GetKeyDown(KeyCode.Alpha2))
      this.SetViewMode(CollectionManagerDisplay.ViewMode.CARDS, (CollectionManagerDisplay.ViewModeData) null);
    else if (Input.GetKeyDown(KeyCode.Alpha3))
      this.SetViewMode(CollectionManagerDisplay.ViewMode.CARD_BACKS, (CollectionManagerDisplay.ViewModeData) null);
    else if (Input.GetKeyDown(KeyCode.Alpha4))
    {
      this.SetViewMode(CollectionManagerDisplay.ViewMode.DECK_TEMPLATE, (CollectionManagerDisplay.ViewModeData) null);
    }
    else
    {
      if (!Input.GetKeyDown(KeyCode.Alpha4))
        return;
      this.OnCraftingModeButtonReleased((UIEvent) null);
    }
  }

  public static CollectionManagerDisplay Get()
  {
    return CollectionManagerDisplay.s_instance;
  }

  public Material GetGoldenCardNotOwnedMeshMaterial()
  {
    return this.m_goldenCardNotOwnedMeshMaterial;
  }

  public Material GetCardNotOwnedMeshMaterial()
  {
    return this.m_cardNotOwnedMeshMaterial;
  }

  public CollectionCardVisual GetCardVisualPrefab()
  {
    return this.m_cardVisualPrefab;
  }

  public bool IsReady()
  {
    return this.m_isReady;
  }

  public void Unload()
  {
    this.m_unloading = true;
    NotificationManager.Get().DestroyAllPopUps();
    this.UnloadAllClassTextures();
    CollectionDeckTray.Get().GetCardsContent().UnregisterCardTileRightClickedListener(new DeckTrayCardListContent.CardTileRightClicked(this.OnCardTileRightClicked));
    CollectionDeckTray.Get().Unload();
    CollectionInputMgr.Get().Unload();
    CollectionManager.Get().RemoveCollectionLoadedListener(new CollectionManager.DelOnCollectionLoaded(this.OnCollectionLoaded));
    CollectionManager.Get().RemoveCollectionChangedListener(new CollectionManager.DelOnCollectionChanged(this.OnCollectionChanged));
    CollectionManager.Get().RemoveDeckCreatedListener(new CollectionManager.DelOnDeckCreated(this.OnDeckCreated));
    CollectionManager.Get().RemoveDeckContentsListener(new CollectionManager.DelOnDeckContents(this.OnDeckContents));
    CollectionManager.Get().RemoveNewCardSeenListener(new CollectionManager.DelOnNewCardSeen(this.OnNewCardSeen));
    CollectionManager.Get().RemoveCardRewardInsertedListener(new CollectionManager.DelOnCardRewardInserted(this.OnCardRewardInserted));
    CollectionManager.Get().RemoveCollectionNetHandlers();
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    Options.Get().UnregisterChangedListener(Option.SHOW_ADVANCED_COLLECTIONMANAGER, new Options.ChangedCallback(this.OnShowAdvancedCMChanged));
    this.m_unloading = false;
  }

  public void Exit()
  {
    this.EnableInput(false);
    NotificationManager.Get().DestroyAllPopUps();
    CollectionDeckTray.Get().Exit();
    SceneMgr.Mode mode = SceneMgr.Get().GetPrevMode();
    if (mode == SceneMgr.Mode.GAMEPLAY)
      mode = SceneMgr.Mode.HUB;
    SceneMgr.Get().SetNextMode(mode);
  }

  public void GetClassTexture(TAG_CLASS classTag, CollectionManagerDisplay.DelTextureLoaded callback, object callbackData)
  {
    if (this.m_loadedClassTextures.ContainsKey(classTag))
    {
      callback(classTag, this.m_loadedClassTextures[classTag], callbackData);
    }
    else
    {
      CollectionManagerDisplay.TextureRequests textureRequests;
      if (this.m_requestedClassTextures.ContainsKey(classTag))
      {
        textureRequests = this.m_requestedClassTextures[classTag];
      }
      else
      {
        textureRequests = new CollectionManagerDisplay.TextureRequests();
        this.m_requestedClassTextures[classTag] = textureRequests;
      }
      textureRequests.m_requests.Add(new CollectionManagerDisplay.TextureRequests.Request()
      {
        m_callback = callback,
        m_callbackData = callbackData
      });
    }
  }

  public void LoadCard(string cardID, DefLoader.LoadDefCallback<CardDef> callback, object callbackData, CardPortraitQuality quality = null)
  {
    DefLoader.Get().LoadCardDef(cardID, callback, callbackData, quality);
  }

  public void CollectionPageContentsChanged(List<CollectibleCard> cardsToDisplay, CollectionManagerDisplay.CollectionActorsReadyCallback callback, object callbackData)
  {
    if (this.m_displayRequestID == int.MaxValue)
      this.m_displayRequestID = 0;
    else
      ++this.m_displayRequestID;
    bool flag = false;
    if (cardsToDisplay == null)
    {
      Log.Rachelle.Print("artStacksToDisplay is null!");
      flag = true;
    }
    else if (cardsToDisplay.Count == 0)
    {
      Log.Rachelle.Print("artStacksToDisplay has a count of 0!");
      flag = true;
    }
    if (flag)
    {
      List<Actor> actors = new List<Actor>();
      callback(actors, callbackData);
    }
    else
    {
      if (this.m_unloading)
        return;
      using (List<Actor>.Enumerator enumerator = this.m_previousCardActors.GetEnumerator())
      {
        while (enumerator.MoveNext())
          UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
      }
      this.m_previousCardActors.Clear();
      this.m_previousCardActors = this.m_cardActors;
      this.m_cardActors = new List<Actor>();
      long balance = NetCache.Get().GetNetObject<NetCache.NetCacheArcaneDustBalance>().Balance;
      using (List<CollectibleCard>.Enumerator enumerator = cardsToDisplay.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CollectibleCard current = enumerator.Current;
          EntityDef entityDef = DefLoader.Get().GetEntityDef(current.CardId);
          CardDef cardDef = DefLoader.Get().GetCardDef(current.CardId, (CardPortraitQuality) null);
          GameObject gameObject = AssetLoader.Get().LoadActor(ActorNames.GetHeroSkinOrHandActor(entityDef.GetCardType(), current.PremiumType), false, false);
          if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
          {
            UnityEngine.Debug.LogError((object) "Unable to load card actor.");
          }
          else
          {
            Actor component = gameObject.GetComponent<Actor>();
            if ((UnityEngine.Object) component == (UnityEngine.Object) null)
            {
              UnityEngine.Debug.LogError((object) "Actor object does not contain Actor component.");
            }
            else
            {
              component.SetEntityDef(entityDef);
              component.SetCardDef(cardDef);
              component.SetPremium(current.PremiumType);
              if (current.OwnedCount == 0)
              {
                if (current.IsCraftable && balance >= (long) current.CraftBuyCost)
                  component.GhostCardEffect(GhostCard.Type.MISSING);
                else
                  component.MissingCardEffect();
              }
              component.UpdateAllComponents();
              this.m_cardActors.Add(component);
            }
          }
        }
      }
      if (callback == null)
        return;
      callback(this.m_cardActors, callbackData);
    }
  }

  public void CollectionPageContentsChangedToCardBacks(int pageNumber, int numCardBacksPerPage, CollectionManagerDisplay.CollectionActorsReadyCallback callback, object callbackData, bool showAll)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManagerDisplay.\u003CCollectionPageContentsChangedToCardBacks\u003Ec__AnonStorey392 backsCAnonStorey392 = new CollectionManagerDisplay.\u003CCollectionPageContentsChangedToCardBacks\u003Ec__AnonStorey392();
    // ISSUE: reference to a compiler-generated field
    backsCAnonStorey392.callback = callback;
    // ISSUE: reference to a compiler-generated field
    backsCAnonStorey392.callbackData = callbackData;
    // ISSUE: reference to a compiler-generated field
    backsCAnonStorey392.\u003C\u003Ef__this = this;
    CardBackManager cardBackManager = CardBackManager.Get();
    // ISSUE: reference to a compiler-generated field
    backsCAnonStorey392.result = new List<Actor>();
    List<CardBackManager.OwnedCardBack> enabledCardBacks = cardBackManager.GetOrderedEnabledCardBacks(!showAll);
    if (enabledCardBacks.Count == 0)
    {
      // ISSUE: reference to a compiler-generated field
      if (backsCAnonStorey392.callback == null)
        return;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      backsCAnonStorey392.callback(backsCAnonStorey392.result, backsCAnonStorey392.callbackData);
    }
    else
    {
      int index1 = (pageNumber - 1) * numCardBacksPerPage;
      int count = Mathf.Min(enabledCardBacks.Count - index1, numCardBacksPerPage);
      List<CardBackManager.OwnedCardBack> range = enabledCardBacks.GetRange(index1, count);
      // ISSUE: reference to a compiler-generated field
      backsCAnonStorey392.numCardBacksToLoad = range.Count;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      backsCAnonStorey392.cbLoadedCallback = new Action<int, CardBackManager.OwnedCardBack, Actor>(backsCAnonStorey392.\u003C\u003Em__DD);
      if (this.m_previousCardBackActors != null)
      {
        using (List<Actor>.Enumerator enumerator = this.m_previousCardBackActors.GetEnumerator())
        {
          while (enumerator.MoveNext())
            UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
        }
        this.m_previousCardBackActors.Clear();
      }
      this.m_previousCardBackActors = this.m_cardBackActors;
      this.m_cardBackActors = new List<Actor>();
      for (int index2 = 0; index2 < range.Count; ++index2)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        CollectionManagerDisplay.\u003CCollectionPageContentsChangedToCardBacks\u003Ec__AnonStorey393 backsCAnonStorey393 = new CollectionManagerDisplay.\u003CCollectionPageContentsChangedToCardBacks\u003Ec__AnonStorey393();
        // ISSUE: reference to a compiler-generated field
        backsCAnonStorey393.\u003C\u003Ef__ref\u0024914 = backsCAnonStorey392;
        // ISSUE: reference to a compiler-generated field
        backsCAnonStorey393.\u003C\u003Ef__this = this;
        // ISSUE: reference to a compiler-generated field
        backsCAnonStorey393.currIndex = index2;
        // ISSUE: reference to a compiler-generated field
        backsCAnonStorey393.cardBackLoad = range[index2];
        // ISSUE: reference to a compiler-generated field
        int cardBackId = backsCAnonStorey393.cardBackLoad.m_cardBackId;
        // ISSUE: reference to a compiler-generated field
        backsCAnonStorey392.result.Add((Actor) null);
        // ISSUE: reference to a compiler-generated method
        if (!cardBackManager.LoadCardBackByIndex(cardBackId, new CardBackManager.LoadCardBackData.LoadCardBackCallback(backsCAnonStorey393.\u003C\u003Em__DE), "Collection_Card_Back"))
        {
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          backsCAnonStorey392.cbLoadedCallback(backsCAnonStorey393.currIndex, backsCAnonStorey393.cardBackLoad, (Actor) null);
        }
      }
    }
  }

  public void RequestContentsToShowDeck(long deckID)
  {
    this.m_showDeckContentsRequest = deckID;
    CollectionManager.Get().RequestDeckContents(this.m_showDeckContentsRequest);
  }

  public CollectionPageLayoutSettings.Variables GetCurrentPageLayoutSettings()
  {
    return this.GetPageLayoutSettings(this.m_currentViewMode);
  }

  public CollectionPageLayoutSettings.Variables GetPageLayoutSettings(CollectionManagerDisplay.ViewMode viewMode)
  {
    return this.m_pageLayoutSettings.GetVariables(viewMode);
  }

  public void ShowPhoneDeckTemplateTray()
  {
    this.m_pageManager.UpdateDeckTemplate(this.m_deckTemplatePickerPhone);
    SlidingTray component = this.m_deckTemplatePickerPhone.GetComponent<SlidingTray>();
    component.RegisterTrayToggleListener(new SlidingTray.TrayToggledListener(this.m_deckTemplatePickerPhone.OnTrayToggled));
    component.ShowTray();
  }

  public DeckTemplatePicker GetPhoneDeckTemplateTray()
  {
    return this.m_deckTemplatePickerPhone;
  }

  public void SetViewMode(CollectionManagerDisplay.ViewMode mode, bool triggerResponse, CollectionManagerDisplay.ViewModeData userdata = null)
  {
    if (this.m_currentViewMode == mode || (mode == CollectionManagerDisplay.ViewMode.HERO_SKINS || mode == CollectionManagerDisplay.ViewMode.CARD_BACKS) && CollectionDeckTray.Get().IsUpdatingTrayMode())
      return;
    if (mode == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
    {
      if (!CollectionManager.Get().IsInEditMode() || SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
        return;
      if ((bool) UniversalInputManager.UsePhoneUI)
        this.ShowPhoneDeckTemplateTray();
    }
    CollectionManagerDisplay.ViewMode currentViewMode = this.m_currentViewMode;
    this.m_currentViewMode = mode;
    this.OnSwitchViewModeResponse(triggerResponse, currentViewMode, mode, userdata);
  }

  public void SetViewMode(CollectionManagerDisplay.ViewMode mode, CollectionManagerDisplay.ViewModeData userdata = null)
  {
    this.SetViewMode(mode, true, userdata);
  }

  public CollectionManagerDisplay.ViewMode GetViewMode()
  {
    return this.m_currentViewMode;
  }

  public bool SetFilterTrayInitialized()
  {
    return this.m_setFilterTrayInitialized;
  }

  public void OnStartEditingDeck(bool isWild)
  {
    this.UpdateSetFilters(isWild, true, false);
  }

  public void OnDoneEditingDeck()
  {
    this.UpdateSetFilters(CollectionManager.Get().ShouldAccountSeeStandardWild(), false, (UnityEngine.Object) this.m_craftingTray != (UnityEngine.Object) null && this.m_craftingTray.IsShown());
    if (this.m_currentViewMode == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
      this.SetViewMode(CollectionManagerDisplay.ViewMode.CARDS, false, (CollectionManagerDisplay.ViewModeData) null);
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL)
      this.m_pageManager.SetDeckRuleset((DeckRuleset) null, false);
    this.m_pageManager.OnDoneEditingDeck();
  }

  public void FilterByManaCost(int cost)
  {
    this.NotifyFilterUpdate(this.m_manaFilterListeners, cost != ManaFilterTab.ALL_TAB_IDX, cost >= 7 ? (object) (cost.ToString() + "+") : (object) cost.ToString());
    this.m_pageManager.FilterByManaCost(cost);
  }

  public void ShowOnlyCardsIOwn()
  {
    this.ShowOnlyCardsIOwn((object) null);
  }

  public void ShowOnlyCardsIOwn(object obj)
  {
    this.m_pageManager.ShowOnlyCardsIOwn();
  }

  public void HideAllTips()
  {
    if ((UnityEngine.Object) this.m_innkeeperLClickReminder != (UnityEngine.Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.m_innkeeperLClickReminder);
    if ((UnityEngine.Object) this.m_deckHelpPopup != (UnityEngine.Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.m_deckHelpPopup);
    if ((UnityEngine.Object) this.m_convertTutorialPopup != (UnityEngine.Object) null)
      NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.m_convertTutorialPopup);
    if (!((UnityEngine.Object) this.m_createDeckNotification != (UnityEngine.Object) null))
      return;
    NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.m_createDeckNotification);
  }

  public void HideDeckHelpPopup()
  {
    if (!((UnityEngine.Object) this.m_deckHelpPopup != (UnityEngine.Object) null))
      return;
    NotificationManager.Get().DestroyNotificationNowWithNoAnim(this.m_deckHelpPopup);
  }

  public void ShowInnkeeeprLClickHelp(bool isHero)
  {
    if (CollectionDeckTray.Get().IsShowingDeckContents())
      return;
    if (isHero)
      this.m_innkeeperLClickReminder = NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_CM_LCLICK_HERO"), string.Empty, 3f, (Action) null, false);
    else
      this.m_innkeeperLClickReminder = NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_CM_LCLICK"), string.Empty, 3f, (Action) null, false);
  }

  public void ShowPremiumCardsNotOwned(bool show)
  {
    this.m_pageManager.ShowCardsNotOwned(show);
  }

  public void ShowPremiumCardsOnly()
  {
    this.m_pageManager.ShowPremiumCardsOnly();
  }

  public void SetFilterCallback(object data, bool isWild)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManagerDisplay.\u003CSetFilterCallback\u003Ec__AnonStorey394 callbackCAnonStorey394 = new CollectionManagerDisplay.\u003CSetFilterCallback\u003Ec__AnonStorey394();
    // ISSUE: reference to a compiler-generated field
    callbackCAnonStorey394.data = data;
    // ISSUE: reference to a compiler-generated field
    callbackCAnonStorey394.\u003C\u003Ef__this = this;
    if (isWild)
    {
      if (!AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES))
      {
        UnityEngine.Debug.LogError((object) "User selected a Wild set filter, without having unlocked all 9 heroes!");
        return;
      }
      if (!CollectionManager.Get().AccountEverHadWildCards())
      {
        // ISSUE: reference to a compiler-generated method
        DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
        {
          m_headerText = GameStrings.Get("GLUE_COLLECTION_SET_FILTER_WILD_SET_HEADER"),
          m_text = GameStrings.Get("GLUE_COLLECTION_SET_FILTER_WILD_SET_BODY"),
          m_cancelText = GameStrings.Get("GLOBAL_CANCEL"),
          m_confirmText = GameStrings.Get("GLOBAL_BUTTON_YES"),
          m_showAlertIcon = true,
          m_responseDisplay = AlertPopup.ResponseDisplay.CONFIRM_CANCEL,
          m_responseCallback = new AlertPopup.ResponseCallback(callbackCAnonStorey394.\u003C\u003Em__DF)
        });
        return;
      }
    }
    // ISSUE: reference to a compiler-generated field
    this.ShowSet(callbackCAnonStorey394.data);
  }

  public void ShowSet(object data)
  {
    this.m_pageManager.FilterByCardSets((List<TAG_CARD_SET>) data);
    this.NotifyFilterUpdate(this.m_setFilterListeners, data != null, (object) null);
  }

  public void GoToPageWithCard(string cardID, TAG_PREMIUM premium)
  {
    if (this.m_currentViewMode == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
      this.SetViewMode(CollectionManagerDisplay.ViewMode.CARDS, new CollectionManagerDisplay.ViewModeData()
      {
        m_setPageByCard = cardID,
        m_setPageByPremium = premium
      });
    else
      this.m_pageManager.JumpToPageWithCard(cardID, premium);
  }

  public void UpdateCurrentPageCardLocks(bool playSound = false)
  {
    this.m_pageManager.UpdateCurrentPageCardLocks(playSound);
  }

  public HeroPickerDisplay GetHeroPickerDisplay()
  {
    return this.m_heroPickerDisplay;
  }

  public void EnterSelectNewDeckHeroMode()
  {
    if (this.m_selectingNewDeckHero)
      return;
    this.EnableInput(false);
    this.m_selectingNewDeckHero = true;
    this.m_heroPickerDisplay = AssetLoader.Get().LoadActor("HeroPicker", false, false).GetComponent<HeroPickerDisplay>();
    NotificationManager.Get().DestroyAllPopUps();
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.COLLECTIONMANAGER)
      return;
    this.m_pageManager.HideNonDeckTemplateTabs(true, false);
  }

  public void ExitSelectNewDeckHeroMode()
  {
    this.m_selectingNewDeckHero = false;
  }

  public void CancelSelectNewDeckHeroMode()
  {
    this.EnableInput(true);
    this.m_pageManager.HideNonDeckTemplateTabs(false, true);
    this.ExitSelectNewDeckHeroMode();
  }

  public bool ShouldShowNewCardGlow(string cardID, TAG_PREMIUM premium)
  {
    CollectibleCard card = CollectionManager.Get().GetCard(cardID, premium);
    if (card != null)
      return card.IsNewCard;
    return false;
  }

  public bool CanViewHeroSkins()
  {
    CollectionManager collectionManager = CollectionManager.Get();
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    if (taggedDeck == null)
      return true;
    return collectionManager.GetBestHeroesIOwn(taggedDeck.GetClass()).Count > 1;
  }

  public bool CanViewCardBacks()
  {
    return CardBackManager.Get().GetCardBacksOwned().Count > 1;
  }

  public void RegisterSwitchViewModeListener(CollectionManagerDisplay.OnSwitchViewMode listener)
  {
    this.m_switchViewModeListeners.Add(listener);
  }

  public void RemoveSwitchViewModeListener(CollectionManagerDisplay.OnSwitchViewMode listener)
  {
    this.m_switchViewModeListeners.Remove(listener);
  }

  public void RegisterManaFilterListener(CollectionManagerDisplay.FilterStateListener listener)
  {
    this.m_manaFilterListeners.Add(listener);
  }

  public void UnregisterManaFilterListener(CollectionManagerDisplay.FilterStateListener listener)
  {
    this.m_manaFilterListeners.Remove(listener);
  }

  public void RegisterSearchFilterListener(CollectionManagerDisplay.FilterStateListener listener)
  {
    this.m_searchFilterListeners.Add(listener);
  }

  public void UnregisterSearchFilterListener(CollectionManagerDisplay.FilterStateListener listener)
  {
    this.m_searchFilterListeners.Remove(listener);
  }

  public void RegisterSetFilterListener(CollectionManagerDisplay.FilterStateListener listener)
  {
    this.m_setFilterListeners.Add(listener);
  }

  public void UnregisterSetFilterListener(CollectionManagerDisplay.FilterStateListener listener)
  {
    this.m_setFilterListeners.Remove(listener);
  }

  public void ResetFilters(bool updateVisuals = true)
  {
    this.m_search.ClearFilter(updateVisuals);
    this.m_manaTabManager.ClearFilter();
    if (!((UnityEngine.Object) this.m_setFilterTray != (UnityEngine.Object) null))
      return;
    this.m_setFilterTray.ClearFilter();
  }

  public void ShowAppropriateSetFilters()
  {
    bool editingDeck = CollectionManager.Get().IsInEditMode();
    bool showWild;
    if (editingDeck)
    {
      CollectionDeck editedDeck = CollectionManager.Get().GetEditedDeck();
      showWild = editedDeck != null && editedDeck.IsWild;
    }
    else
      showWild = CollectionManager.Get().ShouldAccountSeeStandardWild();
    this.UpdateSetFilters(showWild, editingDeck, (UnityEngine.Object) this.m_craftingTray != (UnityEngine.Object) null && this.m_craftingTray.IsShown());
  }

  public void UpdateSetFilters(bool showWild, bool editingDeck, bool showUnownedSets = false)
  {
    this.m_setFilterTray.UpdateSetFilters(showWild, editingDeck, showUnownedSets);
  }

  public bool SetFilterIsDefaultSelection()
  {
    if ((UnityEngine.Object) this.m_setFilterTray == (UnityEngine.Object) null)
      return true;
    return !this.m_setFilterTray.HasActiveFilter();
  }

  public bool IsShowingSetFilterTray()
  {
    if ((UnityEngine.Object) this.m_setFilterTray == (UnityEngine.Object) null)
      return false;
    return this.m_setFilterTray.IsShown();
  }

  private void OnCollectionLoaded()
  {
    this.m_pageManager.OnCollectionLoaded();
  }

  private void OnDeckContents(long deckID)
  {
    if (deckID != this.m_showDeckContentsRequest)
      return;
    this.m_showDeckContentsRequest = 0L;
    this.ShowDeck(deckID, false);
  }

  private void OnDeckCreated(long deckID)
  {
    this.ShowDeck(deckID, true);
  }

  private void OnNewCardSeen(string cardID, TAG_PREMIUM premium)
  {
    this.m_pageManager.UpdateClassTabNewCardCounts();
  }

  private void OnCardRewardInserted(string cardID, TAG_PREMIUM premium)
  {
    this.m_pageManager.RefreshCurrentPageContents();
  }

  private void OnCollectionChanged()
  {
    if (this.m_pageManager.IsShowingMassDisenchant())
      return;
    this.m_pageManager.NotifyOfCollectionChanged();
  }

  private void NotifyFilterUpdate(List<CollectionManagerDisplay.FilterStateListener> listeners, bool active, object value)
  {
    using (List<CollectionManagerDisplay.FilterStateListener>.Enumerator enumerator = listeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current(active, value);
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitUntilReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionManagerDisplay.\u003CWaitUntilReady\u003Ec__Iterator39()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator InitCollectionWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionManagerDisplay.\u003CInitCollectionWhenReady\u003Ec__Iterator3A()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnNetCacheReady()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    if (!NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>().Collection.Manager)
    {
      if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
        return;
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
      Error.AddWarningLoc("GLOBAL_FEATURE_DISABLED_TITLE", "GLOBAL_FEATURE_DISABLED_MESSAGE_COLLECTION");
    }
    else
      this.m_netCacheReady = true;
  }

  private void OnShowAdvancedCMChanged(Option option, object prevValue, bool existed, object userData)
  {
    bool show = Options.Get().GetBool(Option.SHOW_ADVANCED_COLLECTIONMANAGER, false);
    if (show)
      Options.Get().UnregisterChangedListener(Option.SHOW_ADVANCED_COLLECTIONMANAGER, new Options.ChangedCallback(this.OnShowAdvancedCMChanged));
    this.ShowAdvancedCollectionManager(show);
    this.m_manaTabManager.ActivateTabs(true);
  }

  private void OnCardTileRightClicked(DeckTrayDeckTileVisual cardTile)
  {
    if (this.GetViewMode() == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
      return;
    if (!cardTile.GetSlot().Owned)
      CraftingManager.Get().EnterCraftMode((Actor) cardTile.GetActor());
    this.GoToPageWithCard(cardTile.GetCardID(), cardTile.GetPremium());
  }

  private void LoadAllClassTextures()
  {
    foreach (int num in Enum.GetValues(typeof (TAG_CLASS)))
    {
      TAG_CLASS classTag = (TAG_CLASS) num;
      string classTextureName = CollectionManagerDisplay.GetClassTextureName(classTag);
      if (!string.IsNullOrEmpty(classTextureName))
        AssetLoader.Get().LoadTexture(classTextureName, new AssetLoader.ObjectCallback(this.OnClassTextureLoaded), (object) classTag, false);
    }
  }

  private void UnloadAllClassTextures()
  {
    if (this.m_loadedClassTextures.Count == 0)
      return;
    List<string> stringList = new List<string>();
    using (Map<TAG_CLASS, Texture>.KeyCollection.Enumerator enumerator = this.m_loadedClassTextures.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string classTextureName = CollectionManagerDisplay.GetClassTextureName(enumerator.Current);
        if (!string.IsNullOrEmpty(classTextureName))
          stringList.Add(classTextureName);
      }
    }
    AssetCache.ClearTextures((IEnumerable<string>) stringList);
    this.m_loadedClassTextures.Clear();
  }

  public static string GetClassTextureName(TAG_CLASS classTag)
  {
    switch (classTag)
    {
      case TAG_CLASS.DRUID:
        return "Druid";
      case TAG_CLASS.HUNTER:
        return "Hunter";
      case TAG_CLASS.MAGE:
        return "Mage";
      case TAG_CLASS.PALADIN:
        return "Paladin";
      case TAG_CLASS.PRIEST:
        return "Priest";
      case TAG_CLASS.ROGUE:
        return "Rogue";
      case TAG_CLASS.SHAMAN:
        return "Shaman";
      case TAG_CLASS.WARLOCK:
        return "Warlock";
      case TAG_CLASS.WARRIOR:
        return "Warrior";
      default:
        return string.Empty;
    }
  }

  private void SetTavernBrawlTexturesIfNecessary()
  {
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.TAVERN_BRAWL)
      return;
    if ((UnityEngine.Object) this.m_bookBack != (UnityEngine.Object) null && !string.IsNullOrEmpty(this.m_corkBackTexture) && (UnityEngine.Object) this.m_tavernBrawlBookBackMesh != (UnityEngine.Object) null)
    {
      this.m_bookBack.GetComponent<MeshFilter>().mesh = this.m_tavernBrawlBookBackMesh;
      this.m_bookBack.GetComponent<MeshRenderer>().material.SetTexture(0, AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(this.m_corkBackTexture), false));
    }
    if ((bool) UniversalInputManager.UsePhoneUI)
      return;
    using (List<GameObject>.Enumerator enumerator = this.m_tavernBrawlObjectsToSwap.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.GetComponent<Renderer>().material = this.m_tavernBrawlElements;
    }
  }

  private void OnClassTextureLoaded(string assetName, UnityEngine.Object asset, object callbackData)
  {
    if (asset == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CollectionManagerDisplay.OnClassTextureLoaded(): asset for {0} is null!", (object) assetName));
    }
    else
    {
      TAG_CLASS index = (TAG_CLASS) callbackData;
      Texture classTexture = asset as Texture;
      if ((UnityEngine.Object) classTexture == (UnityEngine.Object) null)
        UnityEngine.Debug.LogWarning((object) string.Format("CollectionManagerDisplay.OnClassTextureLoaded(): classTexture for {0} is null (asset is not a texture)!", (object) assetName));
      else if (this.m_loadedClassTextures.ContainsKey(index))
      {
        UnityEngine.Debug.LogWarning((object) string.Format("CollectionManagerDisplay.OnClassTextureLoaded(): classTexture for {0} ({1}) has already been loaded!", (object) index, (object) assetName));
      }
      else
      {
        this.m_loadedClassTextures[index] = classTexture;
        if (!this.m_requestedClassTextures.ContainsKey(index))
          return;
        CollectionManagerDisplay.TextureRequests requestedClassTexture = this.m_requestedClassTextures[index];
        this.m_requestedClassTextures.Remove(index);
        using (List<CollectionManagerDisplay.TextureRequests.Request>.Enumerator enumerator = requestedClassTexture.m_requests.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            CollectionManagerDisplay.TextureRequests.Request current = enumerator.Current;
            current.m_callback(index, classTexture, current.m_callbackData);
          }
        }
      }
    }
  }

  public void EnableInput(bool enable)
  {
    if (!enable)
      ++this.m_inputBlockers;
    else if (this.m_inputBlockers > 0)
      --this.m_inputBlockers;
    this.m_inputBlocker.gameObject.SetActive(this.m_inputBlockers > 0);
  }

  private void ShowDeck(long deckID, bool isNewDeck)
  {
    if (CollectionManager.Get().GetDeck(deckID) == null)
      return;
    bool flag = isNewDeck && SceneMgr.Get().GetMode() == SceneMgr.Mode.COLLECTIONMANAGER;
    if (!flag)
      this.m_pageManager.HideNonDeckTemplateTabs(false, false);
    CollectionManagerDisplay.ViewMode? nullable = new CollectionManagerDisplay.ViewMode?();
    if (flag)
      nullable = new CollectionManagerDisplay.ViewMode?(CollectionManagerDisplay.ViewMode.DECK_TEMPLATE);
    else if (this.m_currentViewMode == CollectionManagerDisplay.ViewMode.HERO_SKINS && !this.CanViewHeroSkins() || this.m_currentViewMode == CollectionManagerDisplay.ViewMode.CARD_BACKS && !this.CanViewCardBacks())
      nullable = new CollectionManagerDisplay.ViewMode?(CollectionManagerDisplay.ViewMode.CARDS);
    CollectionDeckTray.Get().ShowDeck(nullable.HasValue ? nullable.Value : this.GetViewMode(), deckID, isNewDeck);
    this.m_pageManager.SetClassFilter(this.GetDeckHeroClass(deckID), isNewDeck, !nullable.HasValue, (CollectionPageManager.DelOnPageTransitionComplete) null, (object) null);
    this.m_pageManager.UpdateCraftingModeButtonDustBottleVisibility();
    if (nullable.HasValue)
      this.SetViewMode(nullable.Value, (CollectionManagerDisplay.ViewModeData) null);
    NotificationManager.Get().DestroyNotification(this.m_createDeckNotification, 0.25f);
    this.StartCoroutine(this.ShowDeckTemplateTipsIfNeeded());
  }

  private TAG_CLASS GetDeckHeroClass(long deckID)
  {
    CollectionDeck deck = CollectionManager.Get().GetDeck(deckID);
    if (deck == null)
    {
      Log.Derek.Print(string.Format("CollectionManagerDisplay no deck with ID {0}!", (object) deckID));
      return TAG_CLASS.INVALID;
    }
    EntityDef entityDef = DefLoader.Get().GetEntityDef(deck.HeroCardID);
    if (entityDef != null)
      return entityDef.GetClass();
    Log.Derek.Print(string.Format("CollectionManagerDisplay: CollectionManager doesn't have an entity def for {0}!", (object) deck.HeroCardID));
    return TAG_CLASS.INVALID;
  }

  [DebuggerHidden]
  private IEnumerator DoBookOpeningAnimations()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionManagerDisplay.\u003CDoBookOpeningAnimations\u003Ec__Iterator3B()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator SetBookToOpen()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionManagerDisplay.\u003CSetBookToOpen\u003Ec__Iterator3C()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void DoBookClosingAnimations()
  {
    if ((UnityEngine.Object) this.m_cover != (UnityEngine.Object) null)
      this.m_cover.Close();
    this.m_manaTabManager.ActivateTabs(false);
  }

  private void ShowAdvancedCollectionManager(bool show)
  {
    show |= (bool) UniversalInputManager.UsePhoneUI;
    this.m_search.gameObject.SetActive(show);
    this.m_manaTabManager.gameObject.SetActive(show);
    if ((UnityEngine.Object) this.m_setFilterTray != (UnityEngine.Object) null)
      this.m_setFilterTray.SetButtonShown(show && !(bool) UniversalInputManager.UsePhoneUI);
    if ((UnityEngine.Object) this.m_craftingTray == (UnityEngine.Object) null)
      AssetLoader.Get().LoadGameObject(!(bool) UniversalInputManager.UsePhoneUI ? "CraftingTray" : "CraftingTray_phone", new AssetLoader.GameObjectCallback(this.OnCraftingTrayLoaded), (object) null, false);
    this.m_craftingModeButton.gameObject.SetActive(true);
    this.m_craftingModeButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnCraftingModeButtonReleased));
    if ((UnityEngine.Object) this.m_setFilterTray != (UnityEngine.Object) null && show && !this.m_setFilterTrayInitialized)
    {
      this.m_setFilterTray.AddItem(GameStrings.Get("GLUE_COLLECTION_ALL_STANDARD_CARDS"), new Vector2?(this.m_allSetsIconOffset), new SetFilterItem.ItemSelectedCallback(this.SetFilterCallback), new List<TAG_CARD_SET>((IEnumerable<TAG_CARD_SET>) GameUtils.GetStandardSets()), false, true);
      this.m_setFilterTray.AddItem(GameStrings.Get("GLUE_COLLECTION_ALL_CARDS"), new Vector2?(this.m_wildSetsIconOffset), new SetFilterItem.ItemSelectedCallback(this.SetFilterCallback), (List<TAG_CARD_SET>) null, true, false);
      this.m_setFilterTray.AddHeader(GameStrings.Get("GLUE_COLLECTION_STANDARD_SETS"), false);
      this.AddSetFilters(false);
      this.m_setFilterTray.AddHeader(GameStrings.Get("GLUE_COLLECTION_WILD_SETS"), true);
      this.AddSetFilters(true);
      this.AddSetFilter(TAG_CARD_SET.REWARD);
      if (CollectionManager.Get().GetDisplayableCardSets().Contains(TAG_CARD_SET.SLUSH))
        this.AddSetFilter(TAG_CARD_SET.SLUSH);
      this.m_setFilterTray.SelectFirstItem();
      this.m_setFilterTrayInitialized = true;
    }
    this.ShowAppropriateSetFilters();
    if (!show)
      return;
    this.m_manaTabManager.SetUpTabs();
  }

  private void AddSetFilters(bool isWild)
  {
    List<TAG_CARD_SET> displayableCardSets = CollectionManager.Get().GetDisplayableCardSets();
    for (int index = displayableCardSets.Count - 1; index >= 0; --index)
    {
      TAG_CARD_SET tagCardSet = displayableCardSets[index];
      if (GameUtils.IsSetRotated(tagCardSet) == isWild && tagCardSet != TAG_CARD_SET.REWARD && tagCardSet != TAG_CARD_SET.SLUSH)
        this.AddSetFilter(tagCardSet);
    }
  }

  private void AddSetFilter(TAG_CARD_SET cardSet)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CollectionManagerDisplay.\u003CAddSetFilter\u003Ec__AnonStorey395 filterCAnonStorey395 = new CollectionManagerDisplay.\u003CAddSetFilter\u003Ec__AnonStorey395();
    // ISSUE: reference to a compiler-generated field
    filterCAnonStorey395.cardSet = cardSet;
    List<TAG_CARD_SET> data = new List<TAG_CARD_SET>();
    // ISSUE: reference to a compiler-generated method
    CollectionManagerDisplay.CardSetIconMatOffset setIconMatOffset = this.m_manaFilterCardSetIcons.Find(new Predicate<CollectionManagerDisplay.CardSetIconMatOffset>(filterCAnonStorey395.\u003C\u003Em__E0));
    Vector2? iconOffset = new Vector2?();
    if (setIconMatOffset != null)
      iconOffset = new Vector2?(setIconMatOffset.m_offset);
    // ISSUE: reference to a compiler-generated field
    if (filterCAnonStorey395.cardSet == TAG_CARD_SET.PROMO)
      return;
    // ISSUE: reference to a compiler-generated field
    data.Add(filterCAnonStorey395.cardSet);
    // ISSUE: reference to a compiler-generated field
    if (filterCAnonStorey395.cardSet == TAG_CARD_SET.REWARD)
      data.Add(TAG_CARD_SET.PROMO);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    this.m_setFilterTray.AddItem(GameStrings.GetCardSetNameShortened(filterCAnonStorey395.cardSet), iconOffset, new SetFilterItem.ItemSelectedCallback(this.SetFilterCallback), data, GameUtils.IsSetRotated(filterCAnonStorey395.cardSet), false);
  }

  private void OnCoverLoaded(string name, GameObject go, object userData)
  {
    this.m_isCoverLoading = false;
    this.m_cover = go.GetComponent<CollectionCoverDisplay>();
  }

  private void OnInputBlockerRelease(UIEvent e)
  {
    this.m_search.Deactivate();
  }

  private void OnSearchActivated()
  {
    this.EnableInput(false);
  }

  private void OnSearchDeactivated(string oldSearchText, string newSearchText)
  {
    this.EnableInput(true);
    if (oldSearchText == newSearchText)
      return;
    if (!this.m_craftingTray.IsShown() && newSearchText == GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_MISSING"))
    {
      this.m_searchTriggeredCraftingTray = true;
      this.ShowCraftingTray(new bool?(), new bool?(), true);
    }
    else if (this.m_craftingTray.IsShown() && newSearchText != GameStrings.Get("GLUE_COLLECTION_MANAGER_SEARCH_MISSING") && (this.m_searchTriggeredCraftingTray && (UnityEngine.Object) this.m_craftingTray != (UnityEngine.Object) null))
    {
      this.m_craftingTray.Hide();
      this.m_searchTriggeredCraftingTray = false;
    }
    this.NotifyFilterUpdate(this.m_searchFilterListeners, !string.IsNullOrEmpty(newSearchText), (object) newSearchText);
    this.m_pageManager.ChangeSearchTextFilter(newSearchText, true);
  }

  private void OnSearchCleared(bool updateVisuals)
  {
    if (this.m_searchTriggeredCraftingTray && (UnityEngine.Object) this.m_craftingTray != (UnityEngine.Object) null)
    {
      this.m_craftingTray.Hide();
      this.m_searchTriggeredCraftingTray = false;
    }
    this.NotifyFilterUpdate(this.m_searchFilterListeners, false, (object) string.Empty);
    this.m_pageManager.ChangeSearchTextFilter(string.Empty, updateVisuals);
  }

  public void ShowTavernBrawlDeck(long deckID)
  {
    CollectionDeckTray.Get().GetDecksContent().SetEditingTraySection(0);
    CollectionDeckTray.Get().SetTrayMode(CollectionDeckTray.DeckContentTypes.Decks);
    this.RequestContentsToShowDeck(deckID);
  }

  private void DoEnterCollectionManagerEvents()
  {
    if (CollectionManager.Get().HasVisitedCollection() || SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
    {
      this.EnableInput(true);
      this.OpenBookImmediately();
    }
    else
    {
      CollectionManager.Get().SetHasVisitedCollection(true);
      this.EnableInput(false);
      this.StartCoroutine(this.OpenBookWhenReady());
    }
  }

  private void OpenBookImmediately()
  {
    if (SceneMgr.Get().GetMode() == SceneMgr.Mode.COLLECTIONMANAGER)
      PresenceMgr.Get().SetStatus((Enum) PresenceStatus.COLLECTION);
    this.StartCoroutine(this.SetBookToOpen());
    if (SceneMgr.Get().GetMode() != SceneMgr.Mode.COLLECTIONMANAGER)
      return;
    this.StartCoroutine(this.ShowCollectionTipsIfNeeded());
  }

  [DebuggerHidden]
  private IEnumerator OpenBookWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionManagerDisplay.\u003COpenBookWhenReady\u003Ec__Iterator3D()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ShowCraftingTipIfNeeded()
  {
    if (Options.Get().GetBool(Option.TIP_CRAFTING_UNLOCKED, false) || !UserAttentionManager.CanShowAttentionGrabber("CollectionManagerDisplay.ShowCraftingTipIfNeeded"))
      return;
    NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_DISENCHANT_31"), "VO_INNKEEPER_DISENCHANT_31", 0.0f, (Action) null, false);
    Options.Get().SetBool(Option.TIP_CRAFTING_UNLOCKED, true);
  }

  private Vector3 GetNewDeckPosition()
  {
    Vector3 vector3 = !(bool) UniversalInputManager.UsePhoneUI ? new Vector3(17.5f, 0.0f, 0.0f) : new Vector3(25.7f, 2.6f, 0.0f);
    CollectionDeckTray collectionDeckTray = CollectionDeckTray.Get();
    if ((UnityEngine.Object) collectionDeckTray != (UnityEngine.Object) null)
      return collectionDeckTray.GetDecksContent().GetNewDeckButtonPosition() - vector3;
    return new Vector3(0.0f, 0.0f, 0.0f);
  }

  private Vector3 GetLastDeckPosition()
  {
    Vector3 vector3 = !(bool) UniversalInputManager.UsePhoneUI ? new Vector3(9.6f, 0.0f, 3f) : new Vector3(15.8f, 0.0f, 6f);
    CollectionDeckTray collectionDeckTray = CollectionDeckTray.Get();
    if ((UnityEngine.Object) collectionDeckTray != (UnityEngine.Object) null)
      return collectionDeckTray.GetDecksContent().GetLastUsedTraySection().transform.position - vector3;
    return new Vector3(0.0f, 0.0f, 0.0f);
  }

  private Vector3 GetMiddleDeckPosition()
  {
    int index = 4;
    Vector3 vector3 = !(bool) UniversalInputManager.UsePhoneUI ? new Vector3(9.6f, 0.0f, 3f) : new Vector3(15.8f, 0.0f, 6f);
    CollectionDeckTray collectionDeckTray = CollectionDeckTray.Get();
    if ((UnityEngine.Object) collectionDeckTray != (UnityEngine.Object) null)
      return collectionDeckTray.GetDecksContent().GetTraySection(index).transform.position - vector3;
    return new Vector3(0.0f, 0.0f, 0.0f);
  }

  private void ShowSetRotationNewDeckIndicator(float f)
  {
    string empty = string.Empty;
    string text;
    Vector3 position;
    if (CollectionManager.Get().GetNumberOfWildDecks() >= 18)
    {
      text = GameStrings.Get("GLUE_COLLECTION_TUTORIAL15");
      position = this.GetMiddleDeckPosition();
    }
    else
    {
      if (CollectionManager.Get().GetNumberOfWildDecks() <= 0)
        return;
      if (CollectionManager.Get().GetNumberOfStandardDecks() > 0)
      {
        text = GameStrings.Get("GLUE_COLLECTION_TUTORIAL14");
        position = this.GetLastDeckPosition();
      }
      else
      {
        text = GameStrings.Get("GLUE_COLLECTION_TUTORIAL10");
        CollectionDeckTray.Get().GetDecksContent().m_newDeckButton.m_highlightState.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
        position = this.GetNewDeckPosition();
      }
    }
    this.m_createDeckNotification = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.SET_ROTATION_CM_TUTORIALS, position, this.m_editDeckTutorialBone.localScale, text, true);
    if (!((UnityEngine.Object) this.m_createDeckNotification != (UnityEngine.Object) null))
      return;
    this.m_createDeckNotification.ShowPopUpArrow(Notification.PopUpArrowDirection.Right);
    this.m_createDeckNotification.PulseReminderEveryXSeconds(3f);
  }

  [DebuggerHidden]
  public IEnumerator ShowCollectionTipsIfNeeded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionManagerDisplay.\u003CShowCollectionTipsIfNeeded\u003Ec__Iterator3E()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  public IEnumerator ShowDeckTemplateTipsIfNeeded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionManagerDisplay.\u003CShowDeckTemplateTipsIfNeeded\u003Ec__Iterator3F()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnCoverOpened()
  {
    this.EnableInput(true);
  }

  private void OnSwitchViewModeResponse(bool triggerResponse, CollectionManagerDisplay.ViewMode prevMode, CollectionManagerDisplay.ViewMode newMode, CollectionManagerDisplay.ViewModeData userdata)
  {
    foreach (CollectionManagerDisplay.OnSwitchViewMode onSwitchViewMode in this.m_switchViewModeListeners.ToArray())
      onSwitchViewMode(prevMode, newMode, userdata, triggerResponse);
    this.StartCoroutine(this.ShowDeckTemplateTipsIfNeeded());
    this.EnableSearchUI(newMode);
  }

  private void EnableSearchUI(CollectionManagerDisplay.ViewMode viewMode)
  {
    bool flag = viewMode == CollectionManagerDisplay.ViewMode.CARDS;
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_craftingModeButton.Enable(flag);
    this.m_manaTabManager.Enable(flag);
    if ((UnityEngine.Object) this.m_setFilterTray != (UnityEngine.Object) null)
      this.m_setFilterTray.SetButtonEnabled(flag);
    if ((UnityEngine.Object) this.m_filterButton != (UnityEngine.Object) null)
      this.m_filterButton.SetEnabled(flag);
    this.m_search.SetEnabled(true);
  }

  private void OnCraftingTrayLoaded(string name, GameObject go, object userData)
  {
    go.SetActive(false);
    this.m_craftingTray = go.GetComponent<CraftingTray>();
    go.transform.parent = this.m_craftingTrayShownBone.transform.parent;
    go.transform.localPosition = this.m_craftingTrayHiddenBone.transform.localPosition;
    go.transform.localScale = this.m_craftingTrayHiddenBone.transform.localScale;
    this.m_pageManager.UpdateMassDisenchant();
  }

  private void OnCraftingModeButtonReleased(UIEvent e)
  {
    if (this.m_craftingTray.IsShown())
      this.m_craftingTray.Hide();
    else
      this.ShowCraftingTray(new bool?(), new bool?(), true);
    this.StartCoroutine(this.ShowDeckTemplateTipsIfNeeded());
  }

  public void LoadCraftingManager(AssetLoader.GameObjectCallback callback)
  {
    callback(string.Empty, (GameObject) null, (object) null);
  }

  public void ShowCraftingTray(bool? includeUncraftable = null, bool? showOnlyGolden = null, bool updatePage = true)
  {
    bool editingDeck = CollectionManager.Get().IsInEditMode();
    bool showWild = AchieveManager.Get().HasUnlockedFeature(Achievement.UnlockableFeature.VANILLA_HEROES);
    if (editingDeck)
    {
      CollectionDeck editedDeck = CollectionManager.Get().GetEditedDeck();
      showWild = editedDeck != null && editedDeck.IsWild;
    }
    this.UpdateSetFilters(showWild, editingDeck, true);
    CollectionDeckTray collectionDeckTray = CollectionDeckTray.Get();
    if ((UnityEngine.Object) collectionDeckTray != (UnityEngine.Object) null)
    {
      DeckTrayDeckListContent decksContent = collectionDeckTray.GetDecksContent();
      if ((UnityEngine.Object) decksContent != (UnityEngine.Object) null)
        decksContent.CancelRenameEditingDeck();
    }
    this.HideDeckHelpPopup();
    this.m_craftingTray.gameObject.SetActive(true);
    this.m_craftingTray.Show(includeUncraftable, showOnlyGolden, updatePage);
    Hashtable args = iTween.Hash((object) "position", (object) this.m_craftingTrayShownBone.transform.localPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutBounce);
    iTween.Stop(this.m_craftingTray.gameObject);
    iTween.MoveTo(this.m_craftingTray.gameObject, args);
    this.m_craftingModeButton.ShowActiveGlow(true);
  }

  public void HideCraftingTray()
  {
    this.ShowAppropriateSetFilters();
    this.m_craftingTray.gameObject.SetActive(true);
    Hashtable args = iTween.Hash((object) "position", (object) this.m_craftingTrayHiddenBone.transform.localPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutBounce, (object) "oncomplete", (object) (Action<object>) (o => this.m_craftingTray.gameObject.SetActive(false)));
    iTween.Stop(this.m_craftingTray.gameObject);
    iTween.MoveTo(this.m_craftingTray.gameObject, args);
    this.m_craftingModeButton.ShowActiveGlow(false);
  }

  public void ShowConvertTutorial(UserAttentionBlocker blocker)
  {
    if (!UserAttentionManager.CanShowAttentionGrabber(blocker, "CollectionManagerDisplay.ShowConvertTutorial"))
      return;
    this.m_showConvertTutorialCoroutine = this.ShowConvertTutorialCoroutine(blocker);
    this.StartCoroutine(this.m_showConvertTutorialCoroutine);
  }

  [DebuggerHidden]
  private IEnumerator ShowConvertTutorialCoroutine(UserAttentionBlocker blocker)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionManagerDisplay.\u003CShowConvertTutorialCoroutine\u003Ec__Iterator40()
    {
      blocker = blocker,
      \u003C\u0024\u003Eblocker = blocker,
      \u003C\u003Ef__this = this
    };
  }

  public void HideConvertTutorial()
  {
    if (this.m_showConvertTutorialCoroutine != null)
    {
      this.StopCoroutine(this.m_showConvertTutorialCoroutine);
      this.m_showConvertTutorialCoroutine = (IEnumerator) null;
    }
    if (!((UnityEngine.Object) this.m_convertTutorialPopup != (UnityEngine.Object) null))
      return;
    NotificationManager.Get().DestroyNotification(this.m_convertTutorialPopup, 0.25f);
  }

  public void ShowSetFilterTutorial(UserAttentionBlocker blocker)
  {
    if (!UserAttentionManager.CanShowAttentionGrabber(blocker, "CollectionManagerDisplay.ShowSetFilterTutorial"))
      return;
    this.m_showSetFilterTutorialCoroutine = this.ShowSetFilterTutorialCoroutine(blocker);
    this.StartCoroutine(this.m_showSetFilterTutorialCoroutine);
  }

  [DebuggerHidden]
  private IEnumerator ShowSetFilterTutorialCoroutine(UserAttentionBlocker blocker)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CollectionManagerDisplay.\u003CShowSetFilterTutorialCoroutine\u003Ec__Iterator41()
    {
      blocker = blocker,
      \u003C\u0024\u003Eblocker = blocker,
      \u003C\u003Ef__this = this
    };
  }

  public void HideSetFilterTutorial()
  {
    if (this.m_showSetFilterTutorialCoroutine != null)
    {
      this.StopCoroutine(this.m_showSetFilterTutorialCoroutine);
      this.m_showSetFilterTutorialCoroutine = (IEnumerator) null;
    }
    if (!((UnityEngine.Object) this.m_setFilterTutorialPopup != (UnityEngine.Object) null))
      return;
    NotificationManager.Get().DestroyNotification(this.m_setFilterTutorialPopup, 0.25f);
  }

  public void ShowStandardInfoTutorial(UserAttentionBlocker blocker)
  {
    NotificationManager.Get().CreateInnkeeperQuote(blocker, GameStrings.Get("GLUE_COLLECTION_TUTORIAL13"), "VO_INNKEEPER_Male_Dwarf_STANDARD_WELCOME3_14", 0.0f, (Action) null, false);
  }

  public enum ViewMode
  {
    CARDS,
    HERO_SKINS,
    CARD_BACKS,
    DECK_TEMPLATE,
    COUNT,
  }

  public class ViewModeData
  {
    public TAG_CLASS? m_setPageByClass;
    public string m_setPageByCard;
    public TAG_PREMIUM m_setPageByPremium;
    public CollectionPageManager.DelOnPageTransitionComplete m_pageTransitionCompleteCallback;
    public object m_pageTransitionCompleteData;

    public delegate bool WaitToTurnPageDelegate();
  }

  [Serializable]
  public class CardSetIconMatOffset
  {
    public TAG_CARD_SET m_cardSet;
    public Vector2 m_offset;
  }

  private class TextureRequests
  {
    public List<CollectionManagerDisplay.TextureRequests.Request> m_requests = new List<CollectionManagerDisplay.TextureRequests.Request>();

    public class Request
    {
      public CollectionManagerDisplay.DelTextureLoaded m_callback;
      public object m_callbackData;
    }
  }

  public delegate void DelTextureLoaded(TAG_CLASS classTag, Texture classTexture, object callbackData);

  public delegate void CollectionActorsReadyCallback(List<Actor> actors, object callbackData);

  public delegate void OnSwitchViewMode(CollectionManagerDisplay.ViewMode prevMode, CollectionManagerDisplay.ViewMode mode, CollectionManagerDisplay.ViewModeData userdata, bool triggerResponse);

  public delegate void FilterStateListener(bool filterActive, object value);
}
