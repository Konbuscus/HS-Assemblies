﻿// Decompiled with JetBrains decompiler
// Type: DeckRule_IsClassCardOrNeutral
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class DeckRule_IsClassCardOrNeutral : DeckRule
{
  public DeckRule_IsClassCardOrNeutral(DeckRulesetRuleDbfRecord record)
    : base(DeckRule.RuleType.IS_CLASS_CARD_OR_NEUTRAL, record)
  {
    if (!this.m_ruleIsNot)
      return;
    Debug.LogError((object) "IS_CLASS_CARD_OR_NEUTRAL rules do not support \"is not\".");
  }

  public override bool Filter(EntityDef def)
  {
    if (!this.AppliesTo(def.GetCardId()))
      return true;
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(CollectionManager.DeckTag.Editing);
    if (taggedDeck == null)
      return true;
    TAG_CLASS tagClass1 = taggedDeck.GetClass();
    IEnumerable<TAG_CLASS> classes = def.GetClasses((Comparison<TAG_CLASS>) null);
    bool val = false;
    foreach (TAG_CLASS tagClass2 in classes)
    {
      if (tagClass2 == TAG_CLASS.NEUTRAL || tagClass2 == tagClass1)
      {
        val = true;
        break;
      }
    }
    return this.GetResult(val);
  }

  public override bool IsDeckValid(CollectionDeck deck, out RuleInvalidReason reason)
  {
    return this.DefaultYes(out reason);
  }
}
