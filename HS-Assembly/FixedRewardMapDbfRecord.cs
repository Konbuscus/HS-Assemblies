﻿// Decompiled with JetBrains decompiler
// Type: FixedRewardMapDbfRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FixedRewardMapDbfRecord : DbfRecord
{
  [SerializeField]
  private int m_ActionId;
  [SerializeField]
  private int m_RewardId;
  [SerializeField]
  private int m_RewardCount;
  [SerializeField]
  private string m_NoteDesc;
  [SerializeField]
  private bool m_UseQuestToast;
  [SerializeField]
  private string m_RewardTiming;
  [SerializeField]
  private DbfLocValue m_ToastName;
  [SerializeField]
  private DbfLocValue m_ToastDescription;
  [SerializeField]
  private int m_SortOrder;

  [DbfField("ACTION_ID", "specifies which FIXED_REWARD_ACTION.ID (conditions) triggers/grants what Reward.")]
  public int ActionId
  {
    get
    {
      return this.m_ActionId;
    }
  }

  [DbfField("REWARD_ID", "specifies what FIXED_REWARD.ID is granted by this action.")]
  public int RewardId
  {
    get
    {
      return this.m_RewardId;
    }
  }

  [DbfField("REWARD_COUNT", "how many copies of this reward are granted by this action")]
  public int RewardCount
  {
    get
    {
      return this.m_RewardCount;
    }
  }

  [DbfField("NOTE_DESC", "designer note")]
  public string NoteDesc
  {
    get
    {
      return this.m_NoteDesc;
    }
  }

  [DbfField("USE_QUEST_TOAST", "used for client visuals")]
  public bool UseQuestToast
  {
    get
    {
      return this.m_UseQuestToast;
    }
  }

  [DbfField("REWARD_TIMING", "when should the client show the visual associated with this fixed reward?")]
  public string RewardTiming
  {
    get
    {
      return this.m_RewardTiming;
    }
  }

  [DbfField("TOAST_NAME", "")]
  public DbfLocValue ToastName
  {
    get
    {
      return this.m_ToastName;
    }
  }

  [DbfField("TOAST_DESCRIPTION", "")]
  public DbfLocValue ToastDescription
  {
    get
    {
      return this.m_ToastDescription;
    }
  }

  [DbfField("SORT_ORDER", "the order rewards will show up to players when client displays more than 1 reward at a time. this has a UNIQUE key because it is theoretically possible to get many rewards show up at once and a definitive order needs to be in place.")]
  public int SortOrder
  {
    get
    {
      return this.m_SortOrder;
    }
  }

  public override bool LoadRecordsFromAsset<T>(string resourcePath, out List<T> records)
  {
    FixedRewardMapDbfAsset rewardMapDbfAsset = DbfShared.GetAssetBundle().LoadAsset(resourcePath, typeof (FixedRewardMapDbfAsset)) as FixedRewardMapDbfAsset;
    if ((UnityEngine.Object) rewardMapDbfAsset == (UnityEngine.Object) null)
    {
      records = new List<T>();
      Debug.LogError((object) string.Format("FixedRewardMapDbfAsset.LoadRecordsFromAsset() - failed to load records from assetbundle: {0}", (object) resourcePath));
      return false;
    }
    for (int index = 0; index < rewardMapDbfAsset.Records.Count; ++index)
      rewardMapDbfAsset.Records[index].StripUnusedLocales();
    records = (object) rewardMapDbfAsset.Records as List<T>;
    return true;
  }

  public override bool SaveRecordsToAsset<T>(string assetPath, List<T> records)
  {
    return false;
  }

  public override void StripUnusedLocales()
  {
    this.m_ToastName.StripUnusedLocales();
    this.m_ToastDescription.StripUnusedLocales();
  }

  public void SetActionId(int v)
  {
    this.m_ActionId = v;
  }

  public void SetRewardId(int v)
  {
    this.m_RewardId = v;
  }

  public void SetRewardCount(int v)
  {
    this.m_RewardCount = v;
  }

  public void SetNoteDesc(string v)
  {
    this.m_NoteDesc = v;
  }

  public void SetUseQuestToast(bool v)
  {
    this.m_UseQuestToast = v;
  }

  public void SetRewardTiming(string v)
  {
    this.m_RewardTiming = v;
  }

  public void SetToastName(DbfLocValue v)
  {
    this.m_ToastName = v;
    v.SetDebugInfo(this.ID, "TOAST_NAME");
  }

  public void SetToastDescription(DbfLocValue v)
  {
    this.m_ToastDescription = v;
    v.SetDebugInfo(this.ID, "TOAST_DESCRIPTION");
  }

  public void SetSortOrder(int v)
  {
    this.m_SortOrder = v;
  }

  public override object GetVar(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardMapDbfRecord.\u003C\u003Ef__switch\u0024map41 == null)
      {
        // ISSUE: reference to a compiler-generated field
        FixedRewardMapDbfRecord.\u003C\u003Ef__switch\u0024map41 = new Dictionary<string, int>(10)
        {
          {
            "ID",
            0
          },
          {
            "ACTION_ID",
            1
          },
          {
            "REWARD_ID",
            2
          },
          {
            "REWARD_COUNT",
            3
          },
          {
            "NOTE_DESC",
            4
          },
          {
            "USE_QUEST_TOAST",
            5
          },
          {
            "REWARD_TIMING",
            6
          },
          {
            "TOAST_NAME",
            7
          },
          {
            "TOAST_DESCRIPTION",
            8
          },
          {
            "SORT_ORDER",
            9
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardMapDbfRecord.\u003C\u003Ef__switch\u0024map41.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return (object) this.ID;
          case 1:
            return (object) this.ActionId;
          case 2:
            return (object) this.RewardId;
          case 3:
            return (object) this.RewardCount;
          case 4:
            return (object) this.NoteDesc;
          case 5:
            return (object) this.UseQuestToast;
          case 6:
            return (object) this.RewardTiming;
          case 7:
            return (object) this.ToastName;
          case 8:
            return (object) this.ToastDescription;
          case 9:
            return (object) this.SortOrder;
        }
      }
    }
    return (object) null;
  }

  public override void SetVar(string name, object val)
  {
    string key = name;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (FixedRewardMapDbfRecord.\u003C\u003Ef__switch\u0024map42 == null)
    {
      // ISSUE: reference to a compiler-generated field
      FixedRewardMapDbfRecord.\u003C\u003Ef__switch\u0024map42 = new Dictionary<string, int>(10)
      {
        {
          "ID",
          0
        },
        {
          "ACTION_ID",
          1
        },
        {
          "REWARD_ID",
          2
        },
        {
          "REWARD_COUNT",
          3
        },
        {
          "NOTE_DESC",
          4
        },
        {
          "USE_QUEST_TOAST",
          5
        },
        {
          "REWARD_TIMING",
          6
        },
        {
          "TOAST_NAME",
          7
        },
        {
          "TOAST_DESCRIPTION",
          8
        },
        {
          "SORT_ORDER",
          9
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!FixedRewardMapDbfRecord.\u003C\u003Ef__switch\u0024map42.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.SetID((int) val);
        break;
      case 1:
        this.SetActionId((int) val);
        break;
      case 2:
        this.SetRewardId((int) val);
        break;
      case 3:
        this.SetRewardCount((int) val);
        break;
      case 4:
        this.SetNoteDesc((string) val);
        break;
      case 5:
        this.SetUseQuestToast((bool) val);
        break;
      case 6:
        this.SetRewardTiming((string) val);
        break;
      case 7:
        this.SetToastName((DbfLocValue) val);
        break;
      case 8:
        this.SetToastDescription((DbfLocValue) val);
        break;
      case 9:
        this.SetSortOrder((int) val);
        break;
    }
  }

  public override System.Type GetVarType(string name)
  {
    string key = name;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardMapDbfRecord.\u003C\u003Ef__switch\u0024map43 == null)
      {
        // ISSUE: reference to a compiler-generated field
        FixedRewardMapDbfRecord.\u003C\u003Ef__switch\u0024map43 = new Dictionary<string, int>(10)
        {
          {
            "ID",
            0
          },
          {
            "ACTION_ID",
            1
          },
          {
            "REWARD_ID",
            2
          },
          {
            "REWARD_COUNT",
            3
          },
          {
            "NOTE_DESC",
            4
          },
          {
            "USE_QUEST_TOAST",
            5
          },
          {
            "REWARD_TIMING",
            6
          },
          {
            "TOAST_NAME",
            7
          },
          {
            "TOAST_DESCRIPTION",
            8
          },
          {
            "SORT_ORDER",
            9
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (FixedRewardMapDbfRecord.\u003C\u003Ef__switch\u0024map43.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return typeof (int);
          case 1:
            return typeof (int);
          case 2:
            return typeof (int);
          case 3:
            return typeof (int);
          case 4:
            return typeof (string);
          case 5:
            return typeof (bool);
          case 6:
            return typeof (string);
          case 7:
            return typeof (DbfLocValue);
          case 8:
            return typeof (DbfLocValue);
          case 9:
            return typeof (int);
        }
      }
    }
    return (System.Type) null;
  }
}
