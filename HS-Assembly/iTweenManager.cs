﻿// Decompiled with JetBrains decompiler
// Type: iTweenManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iTweenManager : MonoBehaviour
{
  private iTweenCollection m_tweenCollection = new iTweenCollection();
  private static iTweenManager s_instance;
  private static bool s_quitting;

  public static iTweenManager Get()
  {
    if (iTweenManager.s_quitting)
      return (iTweenManager) null;
    if ((UnityEngine.Object) iTweenManager.s_instance == (UnityEngine.Object) null)
    {
      GameObject gameObject = new GameObject();
      gameObject.name = "iTweenManager";
      iTweenManager.s_instance = gameObject.AddComponent<iTweenManager>();
    }
    return iTweenManager.s_instance;
  }

  public static void Add(iTween tween)
  {
    iTweenManager iTweenManager = iTweenManager.Get();
    if (!((UnityEngine.Object) iTweenManager != (UnityEngine.Object) null))
      return;
    iTweenManager.AddImpl(tween);
  }

  private void AddImpl(iTween tween)
  {
    this.m_tweenCollection.Add(tween);
    tween.Awake();
  }

  public static void Remove(iTween tween)
  {
    iTweenManager iTweenManager = iTweenManager.Get();
    if (!((UnityEngine.Object) iTweenManager != (UnityEngine.Object) null))
      return;
    iTweenManager.RemoveImpl(tween);
  }

  private void RemoveImpl(iTween tween)
  {
    this.m_tweenCollection.Remove(tween);
    tween.destroyed = true;
  }

  public void OnApplicationQuit()
  {
    iTweenManager.s_instance = (iTweenManager) null;
    iTweenManager.s_quitting = true;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  public void OnDestroy()
  {
    if (!((UnityEngine.Object) iTweenManager.s_instance == (UnityEngine.Object) this))
      return;
    iTweenManager.s_instance = (iTweenManager) null;
  }

  public void Update()
  {
    iTweenIterator iterator = this.m_tweenCollection.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      next.Upkeep();
      next.Update();
    }
    this.m_tweenCollection.CleanUp();
  }

  public void LateUpdate()
  {
    iTweenIterator iterator = this.m_tweenCollection.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      next.Upkeep();
      next.LateUpdate();
    }
    this.m_tweenCollection.CleanUp();
  }

  public void FixedUpdate()
  {
    iTweenIterator iterator = this.m_tweenCollection.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      next.Upkeep();
      next.FixedUpdate();
    }
    this.m_tweenCollection.CleanUp();
  }

  public iTween GetTweenForObject(GameObject obj)
  {
    iTweenIterator iterator = this.m_tweenCollection.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      if ((UnityEngine.Object) next.gameObject == (UnityEngine.Object) obj)
        return next;
    }
    return (iTween) null;
  }

  public static iTween[] GetTweensForObject(GameObject obj)
  {
    iTweenManager iTweenManager = iTweenManager.Get();
    if ((UnityEngine.Object) iTweenManager != (UnityEngine.Object) null)
      return iTweenManager.GetTweensForObjectImpl(obj);
    return new iTween[0];
  }

  private iTween[] GetTweensForObjectImpl(GameObject obj)
  {
    List<iTween> iTweenList = new List<iTween>();
    iTweenIterator iterator = this.m_tweenCollection.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      if ((UnityEngine.Object) next.gameObject == (UnityEngine.Object) obj)
        iTweenList.Add(next);
    }
    return iTweenList.ToArray();
  }

  public static iTweenIterator GetIterator()
  {
    iTweenManager iTweenManager = iTweenManager.Get();
    if ((UnityEngine.Object) iTweenManager == (UnityEngine.Object) null)
      return new iTweenIterator((iTweenCollection) null);
    return iTweenManager.GetIteratorImpl();
  }

  private iTweenIterator GetIteratorImpl()
  {
    return this.m_tweenCollection.GetIterator();
  }

  public static int GetTweenCount()
  {
    iTweenManager iTweenManager = iTweenManager.Get();
    if ((UnityEngine.Object) iTweenManager == (UnityEngine.Object) null)
      return 0;
    return iTweenManager.GetTweenCountImpl();
  }

  private int GetTweenCountImpl()
  {
    return this.m_tweenCollection.Count;
  }

  public static void ForEach(iTweenManager.TweenOperation op, GameObject go = null, string name = null, string type = null, bool includeChildren = false)
  {
    iTweenManager iTweenManager = iTweenManager.Get();
    if (!((UnityEngine.Object) iTweenManager != (UnityEngine.Object) null))
      return;
    iTweenManager.ForEachImpl(op, go, name, type, includeChildren);
  }

  public static void ForEachByGameObject(iTweenManager.TweenOperation op, GameObject go)
  {
    iTweenManager.ForEach(op, go, (string) null, (string) null, false);
  }

  public static void ForEachByType(iTweenManager.TweenOperation op, string type)
  {
    iTweenManager.ForEach(op, (GameObject) null, (string) null, type, false);
  }

  public static void ForEachByName(iTweenManager.TweenOperation op, string name)
  {
    iTweenManager.ForEach(op, (GameObject) null, name, (string) null, false);
  }

  private void ForEachImpl(iTweenManager.TweenOperation op, GameObject go = null, string name = null, string type = null, bool includeChildren = false)
  {
    iTweenIterator iterator = this.m_tweenCollection.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      if ((!((UnityEngine.Object) go != (UnityEngine.Object) null) || !((UnityEngine.Object) next.gameObject != (UnityEngine.Object) go)) && (name == null || name.Equals(next._name)) && (type == null || (next.type + next.method).Substring(0, type.Length).ToLower().Equals(type.ToLower())))
      {
        op(next);
        if ((UnityEngine.Object) go != (UnityEngine.Object) null && includeChildren)
        {
          IEnumerator enumerator = go.transform.GetEnumerator();
          try
          {
            while (enumerator.MoveNext())
            {
              Transform current = (Transform) enumerator.Current;
              iTweenManager.ForEach(op, current.gameObject, name, type, true);
            }
          }
          finally
          {
            IDisposable disposable = enumerator as IDisposable;
            if (disposable != null)
              disposable.Dispose();
          }
        }
      }
    }
  }

  public static void ForEachInverted(iTweenManager.TweenOperation op, GameObject go, string name, string type, bool includeChildren = false)
  {
    iTweenManager iTweenManager = iTweenManager.Get();
    if (!((UnityEngine.Object) iTweenManager != (UnityEngine.Object) null))
      return;
    iTweenManager.ForEachInvertedImpl(op, go, name, type, includeChildren);
  }

  private void ForEachInvertedImpl(iTweenManager.TweenOperation op, GameObject go, string name, string type, bool includeChildren = false)
  {
    iTweenIterator iterator = this.m_tweenCollection.GetIterator();
    iTween next;
    while ((next = iterator.GetNext()) != null)
    {
      if ((!((UnityEngine.Object) go != (UnityEngine.Object) null) || !((UnityEngine.Object) next.gameObject != (UnityEngine.Object) go)) && (name == null || !name.Equals(next._name)) && (type == null || !(next.type + next.method).Substring(0, type.Length).ToLower().Equals(type.ToLower())))
      {
        op(next);
        if ((UnityEngine.Object) go != (UnityEngine.Object) null && includeChildren)
        {
          IEnumerator enumerator = go.transform.GetEnumerator();
          try
          {
            while (enumerator.MoveNext())
            {
              Transform current = (Transform) enumerator.Current;
              iTweenManager.ForEachInverted(op, current.gameObject, name, type, true);
            }
          }
          finally
          {
            IDisposable disposable = enumerator as IDisposable;
            if (disposable != null)
              disposable.Dispose();
          }
        }
      }
    }
  }

  private class iTweenEntry
  {
    private GameObject gameObject;
    private iTween iTween;
    private Hashtable args;
  }

  public delegate void TweenOperation(iTween tween);
}
