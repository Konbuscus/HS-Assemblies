﻿// Decompiled with JetBrains decompiler
// Type: RewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class RewardData
{
  private NetCache.ProfileNotice.NoticeOrigin m_origin = NetCache.ProfileNotice.NoticeOrigin.UNKNOWN;
  protected List<long> m_noticeIDs = new List<long>();
  private Reward.Type m_type;
  private long m_originData;
  private bool m_isDummyReward;

  public Reward.Type RewardType
  {
    get
    {
      return this.m_type;
    }
  }

  public NetCache.ProfileNotice.NoticeOrigin Origin
  {
    get
    {
      return this.m_origin;
    }
  }

  public long OriginData
  {
    get
    {
      return this.m_originData;
    }
  }

  public bool IsDummyReward
  {
    get
    {
      return this.m_isDummyReward;
    }
  }

  protected RewardData(Reward.Type type)
  {
    this.m_type = type;
  }

  public void LoadRewardObject(Reward.DelOnRewardLoaded callback)
  {
    this.LoadRewardObject(callback, (object) null);
  }

  public void LoadRewardObject(Reward.DelOnRewardLoaded callback, object callbackData)
  {
    string gameObjectName = this.GetGameObjectName();
    if (string.IsNullOrEmpty(gameObjectName))
    {
      Debug.LogError((object) string.Format("Reward.LoadRewardObject(): Do not know how to load reward object for {0}.", (object) this));
    }
    else
    {
      Reward.LoadRewardCallbackData rewardCallbackData = new Reward.LoadRewardCallbackData() { m_callback = callback, m_callbackData = callbackData };
      AssetLoader.Get().LoadGameObject(gameObjectName, new AssetLoader.GameObjectCallback(this.OnRewardObjectLoaded), (object) rewardCallbackData, false);
    }
  }

  public void SetOrigin(NetCache.ProfileNotice.NoticeOrigin origin, long originData)
  {
    this.m_origin = origin;
    this.m_originData = originData;
  }

  public void AddNoticeID(long noticeID)
  {
    if (this.m_noticeIDs.Contains(noticeID))
      return;
    this.m_noticeIDs.Add(noticeID);
  }

  public List<long> GetNoticeIDs()
  {
    return this.m_noticeIDs;
  }

  public void AcknowledgeNotices()
  {
    long[] array = this.m_noticeIDs.ToArray();
    this.m_noticeIDs.Clear();
    foreach (long id in array)
      Network.AckNotice(id);
  }

  public void MarkAsDummyReward()
  {
    this.m_isDummyReward = true;
  }

  protected abstract string GetGameObjectName();

  private void OnRewardObjectLoaded(string name, GameObject go, object callbackData)
  {
    if ((Object) go == (Object) null)
    {
      Debug.LogWarning((object) string.Format("Reward.OnRewardObjectLoaded() - game object is null name={0}", (object) name));
    }
    else
    {
      Reward component = go.GetComponent<Reward>();
      if ((Object) component == (Object) null)
      {
        Debug.LogErrorFormat("Reward.OnRewardObjectLoaded() - loaded game object has no reward component name={0}", (object) name);
      }
      else
      {
        go.transform.parent = SceneMgr.Get().transform;
        component.SetData(this, true);
        Reward.LoadRewardCallbackData loadRewardCallbackData = callbackData as Reward.LoadRewardCallbackData;
        component.NotifyLoadedWhenReady(loadRewardCallbackData);
      }
    }
  }
}
