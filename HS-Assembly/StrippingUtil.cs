﻿// Decompiled with JetBrains decompiler
// Type: StrippingUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StrippingUtil : MonoBehaviour
{
  public void SeedMonotouchAheadOfTimeCompile()
  {
    ParticleSystem particleSystem = (ParticleSystem) null;
    ++particleSystem.playbackSpeed;
    ++particleSystem.startLifetime;
    ++particleSystem.startSpeed;
    ++particleSystem.startDelay;
    ++particleSystem.startRotation;
    particleSystem.emission.enabled = true;
    RenderToTexture renderToTexture1 = (RenderToTexture) null;
    renderToTexture1.m_ObjectToRender = (GameObject) null;
    RenderToTexture renderToTexture2 = renderToTexture1;
    int num1 = (renderToTexture2.m_RealtimeRender ? 1 : 0) & 1;
    renderToTexture2.m_RealtimeRender = num1 != 0;
    RenderToTexture renderToTexture3 = renderToTexture1;
    int num2 = (renderToTexture3.enabled ? 1 : 0) & 1;
    renderToTexture3.enabled = num2 != 0;
    FullScreenEffects fullScreenEffects1 = (FullScreenEffects) null;
    ++fullScreenEffects1.BlendToColorAmount;
    FullScreenEffects fullScreenEffects2 = fullScreenEffects1;
    int num3 = (fullScreenEffects2.VignettingEnable ? 1 : 0) & 1;
    fullScreenEffects2.VignettingEnable = num3 != 0;
    ++fullScreenEffects1.VignettingIntensity;
    ((MeshFilter) null).mesh = (Mesh) null;
    RotateOverTime rotateOverTime = (RotateOverTime) null;
    ++rotateOverTime.RotateSpeedX;
    ++rotateOverTime.RotateSpeedY;
    ++rotateOverTime.RotateSpeedZ;
  }
}
