﻿// Decompiled with JetBrains decompiler
// Type: TimeDelete
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TimeDelete : MonoBehaviour
{
  public float m_SecondsToDelete = 10f;
  private float m_StartTime;

  private void Start()
  {
    this.m_StartTime = Time.time;
  }

  private void Update()
  {
    if ((double) Time.time <= (double) this.m_StartTime + (double) this.m_SecondsToDelete)
      return;
    Object.Destroy((Object) this.gameObject);
  }
}
