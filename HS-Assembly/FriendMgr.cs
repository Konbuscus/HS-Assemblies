﻿// Decompiled with JetBrains decompiler
// Type: FriendMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections.Generic;

public class FriendMgr
{
  private List<FriendMgr.RecentOpponentListener> m_recentOpponentListeners = new List<FriendMgr.RecentOpponentListener>();
  private static FriendMgr s_instance;
  private BnetPlayer m_selectedFriend;
  private BnetPlayer m_recentOpponent;
  private bool m_friendListScrollEnabled;
  private float m_friendListScrollCamPosY;

  public static FriendMgr Get()
  {
    if (FriendMgr.s_instance == null)
    {
      FriendMgr.s_instance = new FriendMgr();
      FriendMgr.s_instance.Initialize();
    }
    return FriendMgr.s_instance;
  }

  public BnetPlayer GetSelectedFriend()
  {
    return this.m_selectedFriend;
  }

  public void SetSelectedFriend(BnetPlayer friend)
  {
    this.m_selectedFriend = friend;
  }

  public bool IsFriendListScrollEnabled()
  {
    return this.m_friendListScrollEnabled;
  }

  public void SetFriendListScrollEnabled(bool enabled)
  {
    this.m_friendListScrollEnabled = enabled;
  }

  public float GetFriendListScrollCamPosY()
  {
    return this.m_friendListScrollCamPosY;
  }

  public void SetFriendListScrollCamPosY(float y)
  {
    this.m_friendListScrollCamPosY = y;
  }

  public BnetPlayer GetRecentOpponent()
  {
    return this.m_recentOpponent;
  }

  private void UpdateRecentOpponent()
  {
    if (SpectatorManager.Get().IsInSpectatorMode() || GameState.Get() == null)
      return;
    Player opposingSidePlayer = GameState.Get().GetOpposingSidePlayer();
    if (opposingSidePlayer == null)
      return;
    BnetPlayer player = BnetPresenceMgr.Get().GetPlayer(opposingSidePlayer.GetGameAccountId());
    if (player == null || this.m_recentOpponent == player)
      return;
    this.m_recentOpponent = player;
    this.FireRecentOpponentEvent(this.m_recentOpponent);
  }

  public void AddRecentOpponentListener(FriendMgr.RecentOpponentCallback callback)
  {
    FriendMgr.RecentOpponentListener opponentListener = new FriendMgr.RecentOpponentListener();
    opponentListener.SetCallback(callback);
    opponentListener.SetUserData((object) null);
    if (this.m_recentOpponentListeners.Contains(opponentListener))
      return;
    this.m_recentOpponentListeners.Add(opponentListener);
  }

  public bool RemoveRecentOpponentListener(FriendMgr.RecentOpponentCallback callback)
  {
    FriendMgr.RecentOpponentListener opponentListener = new FriendMgr.RecentOpponentListener();
    opponentListener.SetCallback(callback);
    opponentListener.SetUserData((object) null);
    return this.m_recentOpponentListeners.Remove(opponentListener);
  }

  public void FireRecentOpponentEvent(BnetPlayer recentOpponent)
  {
    foreach (FriendMgr.RecentOpponentListener opponentListener in this.m_recentOpponentListeners.ToArray())
      opponentListener.Fire(recentOpponent);
  }

  private void Initialize()
  {
    BnetFriendMgr.Get().AddChangeListener(new BnetFriendMgr.ChangeCallback(this.OnFriendsChanged));
    Network.Get().AddBnetErrorListener(BnetFeature.Friends, new Network.BnetErrorCallback(this.OnBnetError));
    BnetPresenceMgr.Get().AddPlayersChangedListener(new BnetPresenceMgr.PlayersChangedCallback(this.OnPlayersChanged));
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
  }

  private void OnFriendsChanged(BnetFriendChangelist changelist, object userData)
  {
    List<BnetPlayer> removedFriends = changelist.GetRemovedFriends();
    if (removedFriends == null || !removedFriends.Contains(this.m_selectedFriend))
      return;
    this.m_selectedFriend = (BnetPlayer) null;
  }

  private bool OnBnetError(BnetErrorInfo info, object userData)
  {
    if (info.GetFeature() == BnetFeature.Friends && info.GetFeatureEvent() == BnetFeatureEvent.Friends_OnSendInvitation)
    {
      switch (info.GetError())
      {
        case BattleNetErrors.ERROR_OK:
          UIStatus.Get().AddInfo(GameStrings.Get("GLOBAL_ADDFRIEND_SENT_CONFIRMATION"));
          return true;
        case BattleNetErrors.ERROR_FRIENDS_FRIENDSHIP_ALREADY_EXISTS:
          UIStatus.Get().AddError(GameStrings.Get("GLOBAL_ADDFRIEND_ERROR_ALREADY_FRIEND"), -1f);
          return true;
      }
    }
    return false;
  }

  private void OnPlayersChanged(BnetPlayerChangelist changelist, object userData)
  {
    BnetPlayerChange change = changelist.FindChange(this.m_selectedFriend);
    if (change == null)
      return;
    BnetPlayer oldPlayer = change.GetOldPlayer();
    BnetPlayer newPlayer = change.GetNewPlayer();
    if (oldPlayer != null && oldPlayer.IsOnline() == newPlayer.IsOnline())
      return;
    this.m_selectedFriend = (BnetPlayer) null;
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    if (mode != SceneMgr.Mode.GAMEPLAY)
      return;
    GameState.Get().RegisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
  }

  private void OnGameOver(TAG_PLAYSTATE playState, object userData)
  {
    GameState.Get().UnregisterGameOverListener(new GameState.GameOverCallback(this.OnGameOver), (object) null);
    this.UpdateRecentOpponent();
  }

  private class RecentOpponentListener : EventListener<FriendMgr.RecentOpponentCallback>
  {
    public void Fire(BnetPlayer recentOpponent)
    {
      this.m_callback(recentOpponent, this.m_userData);
    }
  }

  public delegate void RecentOpponentCallback(BnetPlayer recentOpponent, object userData);
}
