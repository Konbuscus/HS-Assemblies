﻿// Decompiled with JetBrains decompiler
// Type: CardNerfGlows
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class CardNerfGlows : MonoBehaviour
{
  public GameObject m_attack;
  public GameObject m_health;
  public GameObject m_manaCost;
  public GameObject m_rarityGem;
  public GameObject m_art;
  public GameObject m_cardText;
  public GameObject m_cardName;
  public GameObject m_race;

  private void Awake()
  {
    this.HideAll();
  }

  public void SetGlowsForCard(CollectibleCard card)
  {
    this.HideAll();
    if (card.IsAttackChanged)
      this.m_attack.SetActive(true);
    if (card.IsCardInHandTextChanged)
      this.m_cardText.SetActive(true);
    if (card.IsHealthChanged)
      this.m_health.SetActive(true);
    if (!card.IsManaCostChanged)
      return;
    this.m_manaCost.SetActive(true);
  }

  private void HideAll()
  {
    IEnumerator enumerator = this.transform.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
        ((Component) enumerator.Current).gameObject.SetActive(false);
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
  }
}
