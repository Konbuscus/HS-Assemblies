﻿// Decompiled with JetBrains decompiler
// Type: GameLayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum GameLayer
{
  Default = 0,
  TransparentFX = 1,
  IgnoreRaycast = 2,
  Water = 4,
  UI = 5,
  CardRaycast = 8,
  DragPlane = 9,
  PlayAreaCollision = 10,
  InvisibleHitBox1 = 11,
  InvisibleHitBox2 = 12,
  BackgroundUI = 13,
  Tooltip = 14,
  ScrollRaycast = 15,
  Unused16 = 16,
  PerspectiveUI = 17,
  BattleNet = 18,
  IgnoreFullScreenEffects = 19,
  NoLight = 20,
  Effects = 21,
  FXCollide = 22,
  ScreenEffects = 23,
  BattleNetFriendList = 24,
  BattleNetChat = 25,
  BattleNetDialog = 26,
  HighPriorityUI = 27,
  InvisibleRender = 28,
  Unused29 = 29,
  CameraFade = 30,
  CameraMask = 31,
}
