﻿// Decompiled with JetBrains decompiler
// Type: CollectionSetFilterDropdownToggle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CollectionSetFilterDropdownToggle : PegUIElement
{
  public MeshRenderer m_currentIconQuad;
  public MeshRenderer m_buttonMesh;

  public void SetToggleIconOffset(Vector2? materialOffset)
  {
    if (!materialOffset.HasValue)
      return;
    this.m_currentIconQuad.material.SetTextureOffset("_MainTex", materialOffset.Value);
  }

  public void SetEnabledVisual(bool enabled)
  {
    if ((Object) this.m_buttonMesh == (Object) null)
      return;
    this.m_buttonMesh.material.SetFloat("_Desaturate", !enabled ? 1f : 0.0f);
  }
}
