﻿// Decompiled with JetBrains decompiler
// Type: QuickChatFrameBones
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class QuickChatFrameBones
{
  public Transform m_LastMessage;
  public Transform m_ChatLog;
  public Transform m_InputTopLeft;
  public Transform m_InputBottomRight;
  public Transform m_RecentPlayerDropdown;
  public Transform m_InputBlocker;
}
