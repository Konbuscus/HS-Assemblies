﻿// Decompiled with JetBrains decompiler
// Type: CameraShakeMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CameraShakeMgr : MonoBehaviour
{
  private Vector3 m_amount;
  private AnimationCurve m_intensityCurve;
  private float? m_holdAtSec;
  private bool m_started;
  private Vector3 m_initialPos;
  private float m_progressSec;
  private float m_durationSec;

  private void Update()
  {
    if ((double) this.m_progressSec >= (double) this.m_durationSec && !this.IsHolding())
      this.DestroyShake();
    else
      this.UpdateShake();
  }

  public static void Shake(Camera camera, Vector3 amount, AnimationCurve intensityCurve, float? holdAtTime = null)
  {
    if (!(bool) ((Object) camera))
      return;
    CameraShakeMgr cameraShakeMgr = camera.GetComponent<CameraShakeMgr>();
    if ((bool) ((Object) cameraShakeMgr))
    {
      if (CameraShakeMgr.DoesCurveHaveZeroTime(intensityCurve))
      {
        cameraShakeMgr.DestroyShake();
        return;
      }
    }
    else
    {
      if (CameraShakeMgr.DoesCurveHaveZeroTime(intensityCurve))
        return;
      cameraShakeMgr = camera.gameObject.AddComponent<CameraShakeMgr>();
    }
    cameraShakeMgr.StartShake(amount, intensityCurve, holdAtTime);
  }

  public static void Shake(Camera camera, Vector3 amount, float time)
  {
    AnimationCurve intensityCurve = AnimationCurve.Linear(0.0f, 1f, time, 0.0f);
    CameraShakeMgr.Shake(camera, amount, intensityCurve, new float?());
  }

  public static void Stop(Camera camera, float time = 0.0f)
  {
    if (!(bool) ((Object) camera))
      return;
    CameraShakeMgr component = camera.GetComponent<CameraShakeMgr>();
    if (!(bool) ((Object) component))
      return;
    if ((double) time <= 0.0)
    {
      component.DestroyShake();
    }
    else
    {
      AnimationCurve intensityCurve = AnimationCurve.Linear(0.0f, component.ComputeIntensity(), time, 0.0f);
      component.StartShake(component.m_amount, intensityCurve, new float?());
    }
  }

  public static bool IsShaking(Camera camera)
  {
    return (bool) ((Object) camera) && (bool) ((Object) camera.GetComponent<CameraShakeMgr>());
  }

  private static bool DoesCurveHaveZeroTime(AnimationCurve intensityCurve)
  {
    return intensityCurve == null || intensityCurve.length == 0 || (double) intensityCurve.keys[intensityCurve.length - 1].time <= 0.0;
  }

  private void StartShake(Vector3 amount, AnimationCurve intensityCurve, float? holdAtSec = null)
  {
    this.m_amount = amount;
    this.m_intensityCurve = intensityCurve;
    this.m_holdAtSec = holdAtSec;
    if (!this.m_started)
    {
      this.m_started = true;
      this.m_initialPos = this.transform.position;
    }
    this.m_progressSec = 0.0f;
    this.m_durationSec = intensityCurve.keys[intensityCurve.length - 1].time;
  }

  private void DestroyShake()
  {
    this.transform.position = this.m_initialPos;
    Object.Destroy((Object) this);
  }

  private void UpdateShake()
  {
    float intensity = this.ComputeIntensity();
    this.transform.position = this.m_initialPos + new Vector3()
    {
      x = Random.Range(-this.m_amount.x * intensity, this.m_amount.x * intensity),
      y = Random.Range(-this.m_amount.y * intensity, this.m_amount.y * intensity),
      z = Random.Range(-this.m_amount.z * intensity, this.m_amount.z * intensity)
    };
    if (this.IsHolding())
      return;
    this.m_progressSec = Mathf.Min(this.m_progressSec + Time.deltaTime, this.m_durationSec);
  }

  private float ComputeIntensity()
  {
    return this.m_intensityCurve.Evaluate(this.m_progressSec);
  }

  private bool IsHolding()
  {
    if (!this.m_holdAtSec.HasValue)
      return false;
    float? holdAtSec = this.m_holdAtSec;
    if (holdAtSec.HasValue)
      return (double) this.m_progressSec >= (double) holdAtSec.Value;
    return false;
  }
}
