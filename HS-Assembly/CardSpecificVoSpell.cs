﻿// Decompiled with JetBrains decompiler
// Type: CardSpecificVoSpell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class CardSpecificVoSpell : CardSoundSpell
{
  public List<CardSpecificVoData> m_CardSpecificVoDataList = new List<CardSpecificVoData>();

  public override AudioSource DetermineBestAudioSource()
  {
    CardSpecificVoData bestVoiceData = this.GetBestVoiceData();
    if (bestVoiceData != null)
      return bestVoiceData.m_AudioSource;
    return base.DetermineBestAudioSource();
  }

  public CardSpecificVoData GetBestVoiceData()
  {
    using (List<CardSpecificVoData>.Enumerator enumerator = this.m_CardSpecificVoDataList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CardSpecificVoData current = enumerator.Current;
        if (this.SearchForCard(current))
          return current;
      }
    }
    return (CardSpecificVoData) null;
  }

  private bool SearchForCard(CardSpecificVoData cardVOData)
  {
    if (string.IsNullOrEmpty(cardVOData.m_CardId))
      return false;
    using (List<SpellZoneTag>.Enumerator enumerator = cardVOData.m_ZonesToSearch.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        List<Zone> zonesFromTag = SpellUtils.FindZonesFromTag((Spell) this, enumerator.Current, cardVOData.m_SideToSearch);
        if (this.IsCardInZones(cardVOData.m_CardId, zonesFromTag))
          return true;
      }
    }
    return false;
  }

  private bool IsCardInZones(string cardId, List<Zone> zones)
  {
    if (zones == null)
      return false;
    using (List<Zone>.Enumerator enumerator1 = zones.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<Card>.Enumerator enumerator2 = enumerator1.Current.GetCards().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            if (enumerator2.Current.GetEntity().GetCardId() == cardId)
              return true;
          }
        }
      }
    }
    return false;
  }
}
