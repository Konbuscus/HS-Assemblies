﻿// Decompiled with JetBrains decompiler
// Type: Reward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public abstract class Reward : MonoBehaviour
{
  public bool m_showBanner = true;
  public bool m_playSounds = true;
  public float m_EchoHideMeshDelay = 0.65f;
  private bool m_ready = true;
  private List<Reward.OnClickedListener> m_clickListeners = new List<Reward.OnClickedListener>();
  private List<Reward.OnHideListener> m_hideListeners = new List<Reward.OnHideListener>();
  public GameObject m_root;
  public RewardBanner m_rewardBannerPrefab;
  public GameObject m_rewardBannerBone;
  public PegUIElement m_clickCatcher;
  public GameObject m_MeshRoot;
  public Animator m_EchoAnimator;
  protected RewardBanner m_rewardBanner;
  private RewardData m_data;
  private Reward.Type m_type;
  private bool m_shown;

  public Reward.Type RewardType
  {
    get
    {
      return this.Data.RewardType;
    }
  }

  public RewardData Data
  {
    get
    {
      return this.m_data;
    }
  }

  public bool IsShown
  {
    get
    {
      return this.m_shown;
    }
  }

  protected Reward()
  {
    this.InitData();
  }

  protected virtual void Awake()
  {
    if (this.m_showBanner && (Object) this.m_rewardBannerPrefab != (Object) null)
    {
      if (!(bool) UniversalInputManager.UsePhoneUI)
      {
        this.m_rewardBanner = Object.Instantiate<RewardBanner>(this.m_rewardBannerPrefab);
        this.m_rewardBanner.gameObject.SetActive(false);
        this.m_rewardBanner.transform.parent = this.m_rewardBannerBone.transform;
        this.m_rewardBanner.transform.localPosition = Vector3.zero;
      }
      else
        this.m_rewardBanner = (RewardBanner) GameUtils.Instantiate((Component) this.m_rewardBannerPrefab, this.m_rewardBannerBone, false);
    }
    this.EnableClickCatcher(false);
    SoundManager.Get().Load("game_end_reward");
  }

  private void Start()
  {
    if ((Object) this.m_clickCatcher != (Object) null)
      this.m_clickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClickReleased));
    this.Hide(false);
  }

  public void Show(bool updateCacheValues)
  {
    this.Data.AcknowledgeNotices();
    if ((Object) this.m_MeshRoot != (Object) null)
      this.m_MeshRoot.SetActive(true);
    if (this.m_showBanner && (Object) this.m_rewardBanner != (Object) null)
    {
      this.m_rewardBanner.gameObject.SetActive(true);
    }
    else
    {
      if ((Object) this.m_rewardBannerBone != (Object) null)
        this.m_rewardBannerBone.SetActive(false);
      if ((Object) this.m_rewardBanner != (Object) null)
        this.m_rewardBanner.gameObject.SetActive(false);
    }
    if (this.m_playSounds)
      this.PlayShowSounds();
    this.ShowReward(updateCacheValues);
    this.m_shown = true;
  }

  protected virtual void PlayShowSounds()
  {
    SoundManager.Get().LoadAndPlay("Quest_Complete_Jingle");
    SoundManager.Get().LoadAndPlay("quest_complete_pop_up");
    SoundManager.Get().LoadAndPlay("tavern_crowd_play_reaction_positive_random");
  }

  public void HideWithFX()
  {
    this.StartCoroutine(this.HideFXAnimation());
  }

  [DebuggerHidden]
  private IEnumerator HideFXAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Reward.\u003CHideFXAnimation\u003Ec__Iterator23D() { \u003C\u003Ef__this = this };
  }

  public void Hide(bool animate = false)
  {
    if (!animate)
    {
      this.OnHideAnimateComplete();
    }
    else
    {
      iTween.FadeTo(this.gameObject, 0.0f, RewardUtils.REWARD_HIDE_TIME);
      iTween.ScaleTo(this.gameObject, iTween.Hash((object) "scale", (object) RewardUtils.REWARD_HIDDEN_SCALE, (object) "time", (object) RewardUtils.REWARD_HIDE_TIME, (object) "oncomplete", (object) "OnHideAnimateComplete", (object) "oncompletetarget", (object) this.gameObject));
    }
  }

  private void OnHideAnimateComplete()
  {
    this.HideReward();
    this.m_shown = false;
  }

  public void SetData(RewardData data, bool updateVisuals)
  {
    this.m_data = data;
    this.OnDataSet(updateVisuals);
  }

  public void NotifyLoadedWhenReady(Reward.LoadRewardCallbackData loadRewardCallbackData)
  {
    this.StartCoroutine(this.WaitThenNotifyLoaded(loadRewardCallbackData));
  }

  public void EnableClickCatcher(bool enabled)
  {
    if (!((Object) this.m_clickCatcher != (Object) null))
      return;
    this.m_clickCatcher.gameObject.SetActive(enabled);
  }

  public bool RegisterClickListener(Reward.OnClickedCallback callback)
  {
    return this.RegisterClickListener(callback, (object) null);
  }

  public bool RegisterClickListener(Reward.OnClickedCallback callback, object userData)
  {
    Reward.OnClickedListener onClickedListener = new Reward.OnClickedListener();
    onClickedListener.SetCallback(callback);
    onClickedListener.SetUserData(userData);
    if (this.m_clickListeners.Contains(onClickedListener))
      return false;
    this.m_clickListeners.Add(onClickedListener);
    return true;
  }

  public bool RemoveClickListener(Reward.OnClickedCallback callback)
  {
    return this.RemoveClickListener(callback, (object) null);
  }

  public bool RemoveClickListener(Reward.OnClickedCallback callback, object userData)
  {
    Reward.OnClickedListener onClickedListener = new Reward.OnClickedListener();
    onClickedListener.SetCallback(callback);
    onClickedListener.SetUserData(userData);
    return this.m_clickListeners.Remove(onClickedListener);
  }

  public bool RegisterHideListener(Reward.OnHideCallback callback)
  {
    return this.RegisterHideListener(callback, (object) null);
  }

  public bool RegisterHideListener(Reward.OnHideCallback callback, object userData)
  {
    Reward.OnHideListener onHideListener = new Reward.OnHideListener();
    onHideListener.SetCallback(callback);
    onHideListener.SetUserData(userData);
    if (this.m_hideListeners.Contains(onHideListener))
      return false;
    this.m_hideListeners.Add(onHideListener);
    return true;
  }

  public void RemoveHideListener(Reward.OnHideCallback callback, object userData)
  {
    Reward.OnHideListener onHideListener = new Reward.OnHideListener();
    onHideListener.SetCallback(callback);
    onHideListener.SetUserData(userData);
    this.m_hideListeners.Remove(onHideListener);
  }

  protected abstract void InitData();

  protected virtual void ShowReward(bool updateCacheValues)
  {
  }

  protected virtual void OnDataSet(bool updateVisuals)
  {
  }

  protected virtual void HideReward()
  {
    this.OnHide();
  }

  protected void SetReady(bool ready)
  {
    this.m_ready = ready;
  }

  protected void SetRewardText(string headline, string details, string source)
  {
    if ((bool) UniversalInputManager.UsePhoneUI && this.RewardType != Reward.Type.GOLD)
      details = string.Empty;
    if (!((Object) this.m_rewardBanner != (Object) null))
      return;
    this.m_rewardBanner.SetText(headline, details, source);
  }

  [DebuggerHidden]
  private IEnumerator WaitThenNotifyLoaded(Reward.LoadRewardCallbackData loadRewardCallbackData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Reward.\u003CWaitThenNotifyLoaded\u003Ec__Iterator23E() { loadRewardCallbackData = loadRewardCallbackData, \u003C\u0024\u003EloadRewardCallbackData = loadRewardCallbackData, \u003C\u003Ef__this = this };
  }

  private void OnClickReleased(UIEvent e)
  {
    foreach (Reward.OnClickedListener onClickedListener in this.m_clickListeners.ToArray())
      onClickedListener.Fire(this);
  }

  private void OnHide()
  {
    foreach (Reward.OnHideListener onHideListener in this.m_hideListeners.ToArray())
      onHideListener.Fire();
  }

  public enum Type
  {
    ARCANE_DUST,
    BOOSTER_PACK,
    CARD,
    CARD_BACK,
    CRAFTABLE_CARD,
    FORGE_TICKET,
    GOLD,
    MOUNT,
    CLASS_CHALLENGE,
  }

  public class LoadRewardCallbackData
  {
    public Reward.DelOnRewardLoaded m_callback;
    public object m_callbackData;
  }

  private class OnClickedListener : EventListener<Reward.OnClickedCallback>
  {
    public void Fire(Reward reward)
    {
      this.m_callback(reward, this.m_userData);
    }
  }

  private class OnHideListener : EventListener<Reward.OnHideCallback>
  {
    public void Fire()
    {
      this.m_callback(this.m_userData);
    }
  }

  public delegate void OnClickedCallback(Reward reward, object userData);

  public delegate void OnHideCallback(object userData);

  public delegate void DelOnRewardLoaded(Reward reward, object callbackData);
}
