﻿// Decompiled with JetBrains decompiler
// Type: SlidingTray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[CustomEditClass]
public class SlidingTray : MonoBehaviour
{
  [CustomEditField(Sections = "Parameters")]
  public bool m_inactivateOnHide = true;
  [CustomEditField(Sections = "Parameters")]
  public bool m_playAudioOnSlide = true;
  [CustomEditField(Sections = "Parameters")]
  public float m_traySlideDuration = 0.5f;
  private GameLayer m_shownLayer = GameLayer.IgnoreFullScreenEffects;
  [CustomEditField(Sections = "Bones")]
  public Transform m_trayHiddenBone;
  [CustomEditField(Sections = "Bones")]
  public Transform m_trayShownBone;
  [CustomEditField(Sections = "Parameters")]
  public bool m_useNavigationBack;
  [CustomEditField(Sections = "Parameters")]
  public bool m_animateBounce;
  [CustomEditField(Sections = "Optional Features")]
  public PegUIElement m_offClickCatcher;
  [CustomEditField(Sections = "Optional Features")]
  public PegUIElement m_traySliderButton;
  private bool m_trayShown;
  private bool m_traySliderAnimating;
  private SlidingTray.TrayToggledListener m_trayToggledListener;
  private bool m_startingPositionSet;
  private GameLayer m_hiddenLayer;

  private void Awake()
  {
    if ((bool) UniversalInputManager.UsePhoneUI || 1 != 0)
    {
      if (!this.m_startingPositionSet)
      {
        this.transform.localPosition = this.m_trayHiddenBone.localPosition;
        this.m_trayShown = false;
        if (this.m_inactivateOnHide)
          this.gameObject.SetActive(false);
        this.m_startingPositionSet = true;
      }
      if ((UnityEngine.Object) this.m_traySliderButton != (UnityEngine.Object) null)
        this.m_traySliderButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnTraySliderPressed));
      if (!((UnityEngine.Object) this.m_offClickCatcher != (UnityEngine.Object) null))
        return;
      this.m_offClickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClickCatcherPressed));
    }
    else
    {
      if (!((UnityEngine.Object) this.m_traySliderButton != (UnityEngine.Object) null) || !this.m_inactivateOnHide)
        return;
      this.m_traySliderButton.gameObject.SetActive(false);
    }
  }

  private void Start()
  {
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) this.m_offClickCatcher != (UnityEngine.Object) null)
      this.m_offClickCatcher.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClickCatcherPressed));
    if (!((UnityEngine.Object) this.m_traySliderButton != (UnityEngine.Object) null))
      return;
    this.m_traySliderButton.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnTraySliderPressed));
  }

  [ContextMenu("Show")]
  public void ShowTray()
  {
    this.ToggleTraySlider(true, (Transform) null, true);
  }

  [ContextMenu("Hide")]
  public void HideTray()
  {
    this.ToggleTraySlider(false, (Transform) null, true);
  }

  public void ToggleTraySlider(bool show, Transform target = null, bool animate = true)
  {
    if (this.m_trayShown == show)
      return;
    if (show && (UnityEngine.Object) target != (UnityEngine.Object) null)
      this.m_trayShownBone = target;
    if (show)
    {
      if (this.m_useNavigationBack)
        Navigation.Push(new Navigation.NavigateBackHandler(this.BackPressed));
      this.gameObject.SetActive(true);
      if (this.gameObject.activeInHierarchy && animate)
      {
        iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) this.m_trayShownBone.localPosition, (object) "isLocal", (object) true, (object) "time", (object) this.m_traySlideDuration, (object) "oncomplete", (object) "OnTraySliderAnimFinished", (object) "oncompletetarget", (object) this.gameObject, (object) "easetype", (object) (iTween.EaseType) (!this.m_animateBounce ? (int) iTween.Defaults.easeType : 24)));
        this.m_traySliderAnimating = true;
        if ((UnityEngine.Object) this.m_offClickCatcher != (UnityEngine.Object) null)
        {
          this.FadeEffectsIn(0.4f);
          this.m_offClickCatcher.gameObject.SetActive(true);
        }
        if (this.m_playAudioOnSlide)
          SoundManager.Get().LoadAndPlay("choose_opponent_panel_slide_on", this.gameObject);
      }
      else
        this.gameObject.transform.localPosition = this.m_trayShownBone.localPosition;
    }
    else
    {
      if (this.m_useNavigationBack)
        Navigation.RemoveHandler(new Navigation.NavigateBackHandler(this.BackPressed));
      if (this.gameObject.activeInHierarchy && animate)
      {
        iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) this.m_trayHiddenBone.localPosition, (object) "isLocal", (object) true, (object) "oncomplete", (object) "OnTraySliderAnimFinished", (object) "oncompletetarget", (object) this.gameObject, (object) "time", (object) (float) (!this.m_animateBounce ? (double) this.m_traySlideDuration / 2.0 : (double) this.m_traySlideDuration), (object) "easetype", (object) (iTween.EaseType) (!this.m_animateBounce ? 21 : 24)));
        this.m_traySliderAnimating = true;
        if ((UnityEngine.Object) this.m_offClickCatcher != (UnityEngine.Object) null)
        {
          this.FadeEffectsOut(0.2f);
          this.m_offClickCatcher.gameObject.SetActive(false);
        }
        if (this.m_playAudioOnSlide)
          SoundManager.Get().LoadAndPlay("choose_opponent_panel_slide_off", this.gameObject);
      }
      else
      {
        this.gameObject.transform.localPosition = this.m_trayHiddenBone.localPosition;
        if (this.m_inactivateOnHide)
          this.gameObject.SetActive(false);
      }
    }
    this.m_trayShown = show;
    this.m_startingPositionSet = true;
    if (this.m_trayToggledListener == null)
      return;
    this.m_trayToggledListener(show);
  }

  public bool TraySliderIsAnimating()
  {
    return this.m_traySliderAnimating;
  }

  public bool IsTrayInShownPosition()
  {
    return this.gameObject.transform.localPosition == this.m_trayShownBone.localPosition;
  }

  public bool IsShown()
  {
    return this.m_trayShown;
  }

  public void RegisterTrayToggleListener(SlidingTray.TrayToggledListener listener)
  {
    this.m_trayToggledListener = listener;
  }

  public void UnregisterTrayToggleListener(SlidingTray.TrayToggledListener listener)
  {
    if ((MulticastDelegate) this.m_trayToggledListener == (MulticastDelegate) listener)
      this.m_trayToggledListener = (SlidingTray.TrayToggledListener) null;
    else
      Log.JMac.Print("Attempting to unregister a TrayToggleListener that has not been registered!");
  }

  public void SetLayers(GameLayer visible, GameLayer hidden)
  {
    this.m_shownLayer = visible;
    this.m_hiddenLayer = hidden;
  }

  private bool BackPressed()
  {
    this.ToggleTraySlider(false, (Transform) null, true);
    return true;
  }

  private void OnTraySliderAnimFinished()
  {
    this.m_traySliderAnimating = false;
    if (this.m_trayShown || !this.m_inactivateOnHide)
      return;
    this.gameObject.SetActive(false);
  }

  private void OnTraySliderPressed(UIEvent e)
  {
    if (this.m_useNavigationBack && this.m_trayShown)
      return;
    this.ToggleTraySlider(!this.m_trayShown, (Transform) null, true);
  }

  private void OnClickCatcherPressed(UIEvent e)
  {
    this.ToggleTraySlider(false, (Transform) null, true);
  }

  private void FadeEffectsIn(float time = 0.4f)
  {
    SceneUtils.SetLayer(this.gameObject, this.m_shownLayer);
    if (this.m_shownLayer == GameLayer.IgnoreFullScreenEffects)
      SceneUtils.SetLayer(Box.Get().m_letterboxingContainer, this.m_shownLayer);
    FullScreenFXMgr.Get().StartStandardBlurVignette(time);
  }

  private void FadeEffectsOut(float time = 0.2f)
  {
    FullScreenFXMgr.Get().EndStandardBlurVignette(time, new FullScreenFXMgr.EffectListener(this.OnFadeFinished));
  }

  private void OnFadeFinished()
  {
    if ((UnityEngine.Object) this.gameObject == (UnityEngine.Object) null)
      return;
    SceneUtils.SetLayer(this.gameObject, this.m_shownLayer);
    if (this.m_hiddenLayer != GameLayer.Default)
      return;
    SceneUtils.SetLayer(Box.Get().m_letterboxingContainer, this.m_hiddenLayer);
  }

  public delegate void TrayToggledListener(bool shown);
}
