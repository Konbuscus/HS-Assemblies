﻿// Decompiled with JetBrains decompiler
// Type: ZoneDeck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ZoneDeck : Zone
{
  private const int MAX_THICKNESS_CARD_COUNT = 26;
  public GameObject m_ThicknessFull;
  public GameObject m_Thickness75;
  public GameObject m_Thickness50;
  public GameObject m_Thickness25;
  public GameObject m_Thickness1;
  public Spell m_DeckFatigueGlow;
  private bool m_suppressEmotes;
  private bool m_warnedAboutLastCard;
  private bool m_warnedAboutNoCards;
  private bool m_wasFatigued;

  public override void UpdateLayout()
  {
    this.m_updatingLayout = true;
    if (this.IsBlockingLayout())
    {
      this.UpdateLayoutFinished();
    }
    else
    {
      this.UpdateThickness();
      this.UpdateDeckStateEmotes();
      for (int index = 0; index < this.m_cards.Count; ++index)
      {
        Card card = this.m_cards[index];
        if (!card.IsDoNotSort())
        {
          card.HideCard();
          this.SetCardToInDeckState(card);
        }
      }
      this.UpdateLayoutFinished();
    }
  }

  public void SetVisibility(bool visible)
  {
    this.gameObject.SetActive(visible);
  }

  public void SetCardToInDeckState(Card card)
  {
    card.transform.localEulerAngles = new Vector3(270f, 270f, 0.0f);
    card.transform.position = this.transform.position;
    card.transform.localScale = new Vector3(0.88f, 0.88f, 0.88f);
    card.EnableTransitioningZones(false);
  }

  public void DoFatigueGlow()
  {
    if ((Object) this.m_DeckFatigueGlow == (Object) null)
      return;
    this.m_DeckFatigueGlow.ActivateState(SpellStateType.ACTION);
  }

  public bool IsFatigued()
  {
    return this.m_cards.Count == 0;
  }

  public GameObject GetActiveThickness()
  {
    if (this.m_ThicknessFull.GetComponent<Renderer>().enabled)
      return this.m_ThicknessFull;
    if (this.m_Thickness75.GetComponent<Renderer>().enabled)
      return this.m_Thickness75;
    if (this.m_Thickness50.GetComponent<Renderer>().enabled)
      return this.m_Thickness50;
    if (this.m_Thickness25.GetComponent<Renderer>().enabled)
      return this.m_Thickness25;
    if (this.m_Thickness1.GetComponent<Renderer>().enabled)
      return this.m_Thickness1;
    return (GameObject) null;
  }

  public GameObject GetThicknessForLayout()
  {
    GameObject activeThickness = this.GetActiveThickness();
    if ((Object) activeThickness != (Object) null)
      return activeThickness;
    return this.m_Thickness1;
  }

  public bool AreEmotesSuppressed()
  {
    return this.m_suppressEmotes;
  }

  public void SetSuppressEmotes(bool suppress)
  {
    this.m_suppressEmotes = suppress;
  }

  private void UpdateThickness()
  {
    this.m_ThicknessFull.GetComponent<Renderer>().enabled = false;
    this.m_Thickness75.GetComponent<Renderer>().enabled = false;
    this.m_Thickness50.GetComponent<Renderer>().enabled = false;
    this.m_Thickness25.GetComponent<Renderer>().enabled = false;
    this.m_Thickness1.GetComponent<Renderer>().enabled = false;
    int count = this.m_cards.Count;
    if (count == 0)
    {
      this.m_DeckFatigueGlow.ActivateState(SpellStateType.BIRTH);
      this.m_wasFatigued = true;
    }
    else
    {
      if (this.m_wasFatigued)
      {
        this.m_DeckFatigueGlow.ActivateState(SpellStateType.DEATH);
        this.m_wasFatigued = false;
      }
      if (count == 1)
      {
        this.m_Thickness1.GetComponent<Renderer>().enabled = true;
      }
      else
      {
        float num = (float) count / 26f;
        if ((double) num > 0.75)
          this.m_ThicknessFull.GetComponent<Renderer>().enabled = true;
        else if ((double) num > 0.5)
          this.m_Thickness75.GetComponent<Renderer>().enabled = true;
        else if ((double) num > 0.25)
        {
          this.m_Thickness50.GetComponent<Renderer>().enabled = true;
        }
        else
        {
          if ((double) num <= 0.0)
            return;
          this.m_Thickness25.GetComponent<Renderer>().enabled = true;
        }
      }
    }
  }

  private void UpdateDeckStateEmotes()
  {
    if (!GameState.Get().IsPastBeginPhase() || this.m_suppressEmotes)
      return;
    int count = this.m_cards.Count;
    if (count <= 0 && !this.m_warnedAboutNoCards)
    {
      this.m_warnedAboutNoCards = true;
      this.m_warnedAboutLastCard = true;
      this.m_controller.GetHeroCard().PlayEmote(EmoteType.NO_CARDS);
    }
    else if (count == 1 && !this.m_warnedAboutLastCard)
    {
      this.m_warnedAboutLastCard = true;
      this.m_controller.GetHeroCard().PlayEmote(EmoteType.LOW_CARDS);
    }
    else
    {
      if (this.m_warnedAboutLastCard && count > 1)
        this.m_warnedAboutLastCard = false;
      if (!this.m_warnedAboutNoCards || count <= 0)
        return;
      this.m_warnedAboutNoCards = false;
    }
  }
}
