﻿// Decompiled with JetBrains decompiler
// Type: LOE09_LordSlitherspear
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LOE09_LordSlitherspear : LOE_MissionEntity
{
  private bool m_finley_death_line;
  private bool m_finley_saved;
  private Card m_cauldronCard;

  public override void StartGameplaySoundtracks()
  {
    MusicManager.Get().StartPlaylist(MusicPlaylistType.InGame_LOE_Wing3);
  }

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_LOEA09_1_RESPONSE");
    this.PreloadSound("VO_LOEA09_UNSTABLE");
    this.PreloadSound("VO_LOEA09_HERO_POWER");
    this.PreloadSound("FX_MinionSummon_Cast");
    this.PreloadSound("VO_LOEA09_QUOTE1");
    this.PreloadSound("VO_LOEA09_FINLEY_DEATH");
    this.PreloadSound("VO_LOEA09_HERO_POWER1");
    this.PreloadSound("VO_LOEA09_HERO_POWER2");
    this.PreloadSound("VO_LOEA09_HERO_POWER3");
    this.PreloadSound("VO_LOEA09_HERO_POWER4");
    this.PreloadSound("VO_LOEA09_HERO_POWER5");
    this.PreloadSound("VO_LOEA09_HERO_POWER6");
    this.PreloadSound("VO_LOEA09_WIN");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_LOEA09_1_RESPONSE",
            m_stringTag = "VO_LOEA09_1_RESPONSE"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE09_LordSlitherspear.\u003CHandleMissionEventWithTiming\u003Ec__Iterator16F() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE09_LordSlitherspear.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator170() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LOE09_LordSlitherspear.\u003CHandleGameOverWithTiming\u003Ec__Iterator171() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
