﻿// Decompiled with JetBrains decompiler
// Type: BoxLightState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BoxLightState
{
  public float m_TransitionSec = 0.5f;
  public iTween.EaseType m_TransitionEaseType = iTween.EaseType.linear;
  public Color m_AmbientColor = new Color(0.5058824f, 0.4745098f, 0.4745098f, 1f);
  public BoxLightStateType m_Type;
  public float m_DelaySec;
  public Spell m_Spell;
  public List<BoxLightInfo> m_LightInfos;
}
