﻿// Decompiled with JetBrains decompiler
// Type: RankedWinsPlate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class RankedWinsPlate : PegUIElement
{
  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    this.GetComponent<TooltipZone>().ShowTooltip(GameStrings.Get("GLUE_TOOLTIP_GOLDEN_WINS_HEADER"), GameStrings.Get("GLUE_TOOLTIP_GOLDEN_WINS_DESC"), 5f, true);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    this.GetComponent<TooltipZone>().HideTooltip();
  }
}
