﻿// Decompiled with JetBrains decompiler
// Type: SoundCategory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public enum SoundCategory
{
  NONE,
  FX,
  MUSIC,
  VO,
  SPECIAL_VO,
  SPECIAL_CARD,
  AMBIENCE,
  SPECIAL_MUSIC,
  TRIGGER_VO,
  HERO_MUSIC,
  BOSS_VO,
}
