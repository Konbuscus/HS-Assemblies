﻿// Decompiled with JetBrains decompiler
// Type: KAR02_Mirror
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class KAR02_Mirror : KAR_MissionEntity
{
  private HashSet<string> m_playedLines = new HashSet<string>();

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_Mirror_Male_Mirror_MirrorEmoteResponse_01");
    this.PreloadSound("VO_Mirror_Male_Mirror_MirrorWellPlayedResponse_01");
    this.PreloadSound("VO_Mirror_Male_Mirror_MirrorFirstCard_01");
    this.PreloadSound("VO_Mirror_Male_Mirror_MirrorTurn1_01");
    this.PreloadSound("VO_Mirror_Male_Mirror_MirrorTurn3_01");
    this.PreloadSound("VO_Mirror_Male_Mirror_MirrorTurn3_03");
    this.PreloadSound("VO_Mirror_Male_Mirror_MirrorTurn5_02");
    this.PreloadSound("VO_Mirror_Male_Mirror_MirrorMirrorImage_01");
    this.PreloadSound("VO_Mirror_Male_Mirror_MirrorMedivhSkin_01");
    this.PreloadSound("VO_Moroes_Male_Human_MirrorTurn5_01");
    this.PreloadSound("VO_Moroes_Male_Human_MirrorWin_02");
    this.PreloadSound("VO_Moroes_Male_Human_MirrorTurn3_01");
  }

  protected override void InitEmoteResponses()
  {
    List<EmoteType> emoteTypeList = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS);
    emoteTypeList.Remove(EmoteType.WELL_PLAYED);
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = emoteTypeList,
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Mirror_Male_Mirror_MirrorEmoteResponse_01",
            m_stringTag = "VO_Mirror_Male_Mirror_MirrorEmoteResponse_01"
          }
        }
      },
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>()
        {
          EmoteType.WELL_PLAYED
        },
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_Mirror_Male_Mirror_MirrorWellPlayedResponse_01",
            m_stringTag = "VO_Mirror_Male_Mirror_MirrorWellPlayedResponse_01"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR02_Mirror.\u003CHandleMissionEventWithTiming\u003Ec__Iterator191() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleStartOfTurnWithTiming(int turn)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR02_Mirror.\u003CHandleStartOfTurnWithTiming\u003Ec__Iterator192() { turn = turn, \u003C\u0024\u003Eturn = turn, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToFriendlyPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR02_Mirror.\u003CRespondToFriendlyPlayedCardWithTiming\u003Ec__Iterator193() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KAR02_Mirror.\u003CHandleGameOverWithTiming\u003Ec__Iterator194() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult, \u003C\u003Ef__this = this };
  }
}
