﻿// Decompiled with JetBrains decompiler
// Type: BannerManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class BannerManager
{
  private List<int> m_seenBanners = new List<int>();
  private static BannerManager s_instance;
  private bool m_bannerWasAcknowledged;

  private BannerManager()
  {
  }

  public static BannerManager Get()
  {
    if (BannerManager.s_instance == null)
      BannerManager.s_instance = new BannerManager();
    return BannerManager.s_instance;
  }

  private int GetOutstandingDisplayBannerId()
  {
    int num = Vars.Key("Events.BannerIdOverride").GetInt(0);
    if (num != 0)
      return num;
    NetCache.NetCacheProfileProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>();
    if (netObject == null)
      return 0;
    return netObject.DisplayBanner;
  }

  private bool AcknowledgeBanner(int banner)
  {
    this.m_seenBanners.Add(banner);
    if (banner != this.GetOutstandingDisplayBannerId() || this.m_bannerWasAcknowledged)
      return false;
    this.m_bannerWasAcknowledged = true;
    NetCache.NetCacheProfileProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheProfileProgress>();
    if (netObject != null)
    {
      netObject.DisplayBanner = banner;
      NetCache.Get().NetCacheChanged<NetCache.NetCacheProfileProgress>();
    }
    Network.AcknowledgeBanner(banner);
    return true;
  }

  public bool ShowOutstandingBannerEvent(BannerManager.DelOnCloseBanner callback = null)
  {
    int outstandingDisplayBannerId = this.GetOutstandingDisplayBannerId();
    if (!Options.Get().GetBool(Option.HAS_SEEN_HUB, false) || this.m_seenBanners.Contains(outstandingDisplayBannerId) || !this.ShowBanner(outstandingDisplayBannerId, callback))
      return false;
    this.AcknowledgeBanner(outstandingDisplayBannerId);
    return true;
  }

  public bool ShowBanner(string assetName, string text, BannerManager.DelOnCloseBanner callback = null, Action<BannerPopup> onCreateCallback = null)
  {
    BannerPopup bannerPopup = GameUtils.LoadGameObjectWithComponent<BannerPopup>(assetName);
    if ((UnityEngine.Object) bannerPopup == (UnityEngine.Object) null)
      return false;
    if (onCreateCallback != null)
      onCreateCallback(bannerPopup);
    bannerPopup.Show(text, callback);
    return true;
  }

  public bool ShowBanner(int bannerID, BannerManager.DelOnCloseBanner callback = null)
  {
    if (bannerID == 0)
      return false;
    BannerDbfRecord record = GameDbf.Banner.GetRecord(bannerID);
    string assetName = record != null ? FileUtils.GameAssetPathToName(record.Prefab) : (string) null;
    if (record != null && assetName != null)
      return this.ShowBanner(assetName, (string) record.Text, callback, (Action<BannerPopup>) null);
    Debug.LogWarning((object) string.Format("No banner defined for bannerID={0}", (object) bannerID));
    return false;
  }

  public void Cheat_ClearSeenBannersNewerThan(int bannerId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.m_seenBanners.RemoveAll(new Predicate<int>(new BannerManager.\u003CCheat_ClearSeenBannersNewerThan\u003Ec__AnonStorey362()
    {
      bannerId = bannerId
    }.\u003C\u003Em__46));
  }

  public void Cheat_ClearSeenBanners()
  {
    this.m_seenBanners.Clear();
  }

  public bool ShowCustomBanner(string bannerAsset, string bannerText, BannerManager.DelOnCloseBanner callback = null)
  {
    BannerPopup bannerPopup = GameUtils.LoadGameObjectWithComponent<BannerPopup>(bannerAsset);
    if ((UnityEngine.Object) bannerPopup == (UnityEngine.Object) null)
      return false;
    bannerPopup.Show(bannerText, callback);
    return true;
  }

  public delegate void DelOnCloseBanner();
}
