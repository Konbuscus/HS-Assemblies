﻿// Decompiled with JetBrains decompiler
// Type: CardListPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class CardListPopup : DialogBase
{
  [SerializeField]
  private float m_CardSpacing = 5f;
  private int m_numPages = 1;
  private List<Actor> m_cardActors = new List<Actor>();
  private const int MAX_CARDS_PER_PAGE = 3;
  [CustomEditField(Sections = "Object Links")]
  public GameObject m_CardsContainer;
  public UIBButton m_okayButton;
  public UberText m_description;
  public NestedPrefab m_leftArrowNested;
  public NestedPrefab m_rightArrowNested;
  private CardListPopup.Info m_info;
  private UIBButton m_leftArrow;
  private UIBButton m_rightArrow;
  private int m_pageNum;

  [CustomEditField(Sections = "Variables")]
  public float CardSpacing
  {
    get
    {
      return this.m_CardSpacing;
    }
    set
    {
      this.m_CardSpacing = value;
      this.UpdateCardPositions();
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.m_okayButton.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.PressOkay()));
  }

  private void Start()
  {
  }

  private void OnDestroy()
  {
    if (!((Object) UniversalInputManager.Get() != (Object) null))
      return;
    UniversalInputManager.Get().SetSystemDialogActive(false);
  }

  public void SetInfo(CardListPopup.Info info)
  {
    this.m_info = info;
    if (this.m_info.m_callbackOnHide == null)
      return;
    this.AddHideListener(this.m_info.m_callbackOnHide);
  }

  public override void Show()
  {
    base.Show();
    this.FadeEffectsIn();
    this.InitInfo();
    this.UpdateAll(this.m_info);
    UniversalInputManager.Get().SetSystemDialogActive(true);
  }

  public override void Hide()
  {
    base.Hide();
    this.FadeEffectsOut();
  }

  private void InitInfo()
  {
    if (this.m_info != null)
      return;
    this.m_info = new CardListPopup.Info();
  }

  private void UpdateAll(CardListPopup.Info info)
  {
    this.m_description.Text = info.m_description;
    if (this.m_info.m_cards.Count > 3)
      this.SetupPagingArrows();
    this.m_numPages = (this.m_info.m_cards.Count + 3 - 1) / 3;
    this.ShowPage(0);
  }

  private void ShowPage(int pageNum)
  {
    if (pageNum < 0 || pageNum >= this.m_numPages)
    {
      Log.JMac.PrintWarning("Attempting to show invalid page number " + (object) pageNum);
    }
    else
    {
      this.m_pageNum = pageNum;
      this.StopCoroutine("TransitionPage");
      this.StartCoroutine("TransitionPage");
    }
  }

  [DebuggerHidden]
  private IEnumerator TransitionPage()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CardListPopup.\u003CTransitionPage\u003Ec__Iterator348() { \u003C\u003Ef__this = this };
  }

  private void UpdateCardPositions()
  {
    int count = this.m_cardActors.Count;
    for (int index = 0; index < count; ++index)
    {
      Actor cardActor = this.m_cardActors[index];
      Vector3 zero = Vector3.zero;
      float num = ((float) index - (float) (count - 1) / 2f) * this.m_CardSpacing;
      zero.x += num;
      cardActor.transform.localPosition = zero;
    }
  }

  private void SetupPagingArrows()
  {
    this.m_leftArrowNested.gameObject.SetActive(true);
    this.m_rightArrowNested.gameObject.SetActive(true);
    GameObject go1 = this.m_leftArrowNested.PrefabGameObject(false);
    SceneUtils.SetLayer(go1, this.m_leftArrowNested.gameObject.layer);
    this.m_leftArrow = go1.GetComponent<UIBButton>();
    this.m_leftArrow.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.TurnPage(false)));
    GameObject go2 = this.m_rightArrowNested.PrefabGameObject(false);
    SceneUtils.SetLayer(go2, this.m_rightArrowNested.gameObject.layer);
    this.m_rightArrow = go2.GetComponent<UIBButton>();
    this.m_rightArrow.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.TurnPage(true)));
    HighlightState componentInChildren = this.m_rightArrow.GetComponentInChildren<HighlightState>();
    if (!(bool) ((Object) componentInChildren))
      return;
    componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }

  private void TurnPage(bool right)
  {
    Log.JMac.Print("Turn page!");
    HighlightState componentInChildren = this.m_rightArrow.GetComponentInChildren<HighlightState>();
    if ((bool) ((Object) componentInChildren))
      componentInChildren.ChangeState(ActorStateType.NONE);
    this.ShowPage(this.m_pageNum + (!right ? -1 : 1));
  }

  private void PressOkay()
  {
    this.Hide();
  }

  private void FadeEffectsIn()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((Object) fullScreenFxMgr == (Object) null)
      return;
    fullScreenFxMgr.SetBlurBrightness(1f);
    fullScreenFxMgr.SetBlurDesaturation(0.0f);
    fullScreenFxMgr.Vignette(0.4f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.Blur(1f, 0.4f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  private void FadeEffectsOut()
  {
    FullScreenFXMgr fullScreenFxMgr = FullScreenFXMgr.Get();
    if ((Object) fullScreenFxMgr == (Object) null)
      return;
    fullScreenFxMgr.StopVignette(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
    fullScreenFxMgr.StopBlur(0.2f, iTween.EaseType.easeOutCirc, (FullScreenFXMgr.EffectListener) null);
  }

  protected override CanvasScaleMode ScaleMode()
  {
    return (bool) UniversalInputManager.UsePhoneUI ? CanvasScaleMode.WIDTH : CanvasScaleMode.HEIGHT;
  }

  public class Info
  {
    public string m_description;
    public List<CollectibleCard> m_cards;
    public DialogBase.HideCallback m_callbackOnHide;
  }
}
