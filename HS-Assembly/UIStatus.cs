﻿// Decompiled with JetBrains decompiler
// Type: UIStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIStatus : MonoBehaviour
{
  public float m_FadeDelaySec = 2f;
  public float m_FadeSec = 0.5f;
  public iTween.EaseType m_FadeEaseType = iTween.EaseType.linear;
  public UberText m_Text;
  public Color m_InfoColor;
  public Color m_ErrorColor;
  private static UIStatus s_instance;
  private bool m_isScreenshot;

  private void Awake()
  {
    UIStatus.s_instance = this;
    this.m_Text.gameObject.SetActive(false);
    if (!(bool) ((Object) OverlayUI.Get()))
      throw new UnityException("Trying to create UIStatus before OverlayUI!");
    OverlayUI.Get().AddGameObject(this.gameObject, CanvasAnchor.CENTER, false, CanvasScaleMode.HEIGHT);
  }

  private void OnDestroy()
  {
    UIStatus.s_instance = (UIStatus) null;
  }

  public static UIStatus Get()
  {
    if ((Object) UIStatus.s_instance == (Object) null)
      UIStatus.s_instance = AssetLoader.Get().LoadUIScreen("UIStatus", true, false).GetComponent<UIStatus>();
    return UIStatus.s_instance;
  }

  public void AddInfo(string message)
  {
    this.AddInfo(message, false);
  }

  public void AddInfo(string message, float delay)
  {
    this.AddInfo(message, false, delay);
  }

  public void AddInfo(string message, bool isScreenshot)
  {
    this.AddInfo(message, isScreenshot, -1f);
  }

  public void AddInfo(string message, bool isScreenshot, float delay)
  {
    this.m_isScreenshot = isScreenshot;
    this.m_Text.TextColor = this.m_InfoColor;
    this.ShowMessage(message, delay);
  }

  public void AddError(string message, float delay = -1f)
  {
    this.m_Text.TextColor = this.m_ErrorColor;
    this.ShowMessage(message, delay);
  }

  public void HideIfScreenshotMessage()
  {
    if (!this.m_isScreenshot)
      return;
    iTween.Stop(this.m_Text.gameObject);
    this.OnFadeComplete();
  }

  private void ShowMessage(string message)
  {
    this.ShowMessage(message, -1f);
  }

  private void ShowMessage(string message, float delay)
  {
    this.m_Text.Text = string.Empty;
    if (message.Contains("\n"))
    {
      this.m_Text.ResizeToFit = false;
      this.m_Text.WordWrap = true;
      this.m_Text.ForceWrapLargeWords = true;
    }
    else
    {
      this.m_Text.ResizeToFit = true;
      this.m_Text.WordWrap = false;
      this.m_Text.ForceWrapLargeWords = false;
    }
    this.m_Text.Text = message;
    this.m_Text.gameObject.SetActive(true);
    this.m_Text.TextAlpha = 1f;
    iTween.Stop(this.m_Text.gameObject, true);
    if ((double) delay < 0.0)
      delay = this.m_FadeDelaySec;
    iTween.FadeTo(this.m_Text.gameObject, iTween.Hash((object) "amount", (object) 0.0f, (object) "delay", (object) delay, (object) "time", (object) this.m_FadeSec, (object) "easeType", (object) this.m_FadeEaseType, (object) "oncomplete", (object) "OnFadeComplete", (object) "oncompletetarget", (object) this.gameObject));
  }

  private void OnFadeComplete()
  {
    this.m_isScreenshot = false;
    this.m_Text.gameObject.SetActive(false);
  }
}
