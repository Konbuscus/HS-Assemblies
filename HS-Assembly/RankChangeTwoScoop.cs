﻿// Decompiled with JetBrains decompiler
// Type: RankChangeTwoScoop
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RankChangeTwoScoop : MonoBehaviour
{
  private List<RankChangeStar> m_stars = new List<RankChangeStar>();
  private List<RankChangeStar> m_chestStars = new List<RankChangeStar>();
  private const float STAR_ACTION_DELAY = 0.2f;
  public GameObject m_medalContainer;
  public GameObject m_bannerTop;
  public UberText m_rankNumberTop;
  public GameObject m_bannerBottom;
  public UberText m_rankNumberBottom;
  public UberText m_legendIndex;
  public GameObject m_rankMedalTop;
  public GameObject m_rankMedalBottom;
  public GameObject m_wildFlair;
  public UberText m_name;
  public Material m_legendaryMaterial;
  public RankChangeStar m_starPrefab;
  public PlayMakerFSM m_mainFSM;
  public List<Transform> m_evenStarBones;
  public List<Transform> m_oddStarBones;
  public GameObject m_winStreakParent;
  public GameObject m_scrubRankDesc;
  public PegUIElement m_debugClickCatcher;
  public RankedRewardChest m_rewardChest;
  public MeshRenderer m_rewardChestGlow;
  public UberText m_rewardChestInstructions;
  private MedalInfoTranslator m_medalInfoTranslator;
  private TranslatedMedalInfo m_currMedalInfo;
  private TranslatedMedalInfo m_prevMedalInfo;
  private bool m_validPrevMedal;
  private int m_numTexturesLoading;
  private int m_chestRank;
  private RankChangeTwoScoop.RankChangeClosed m_closedCallback;
  protected Vector3 START_SCALE;
  protected Vector3 END_SCALE;
  protected Vector3 AFTER_PUNCH_SCALE;

  private void Awake()
  {
    PlatformDependentValue<float> platformDependentValue = new PlatformDependentValue<float>(PlatformCategory.Screen) { PC = 1.2f, Phone = 0.8f };
    float num = 1.25f * (float) platformDependentValue;
    this.START_SCALE = new Vector3(0.01f, 0.01f, 0.01f);
    this.END_SCALE = new Vector3(num, num, num);
    this.AFTER_PUNCH_SCALE = new Vector3((float) platformDependentValue, (float) platformDependentValue, (float) platformDependentValue);
    for (int index = 0; index < 5; ++index)
    {
      this.m_stars.Add((RankChangeStar) GameUtils.Instantiate((Component) this.m_starPrefab, this.gameObject, false));
      this.m_chestStars.Add((RankChangeStar) GameUtils.Instantiate((Component) this.m_starPrefab, this.gameObject, false));
    }
    for (int index = 0; index < this.m_chestStars.Count; ++index)
      this.m_chestStars[index].gameObject.SetActive(false);
    if ((bool) UniversalInputManager.UsePhoneUI)
      this.m_name.Width *= 1.2f;
    this.m_winStreakParent.SetActive(false);
    this.m_medalContainer.SetActive(false);
    this.m_debugClickCatcher.gameObject.SetActive(false);
  }

  private void OnDestroy()
  {
    using (List<RankChangeStar>.Enumerator enumerator = this.m_stars.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RankChangeStar current = enumerator.Current;
        if ((Object) current.gameObject != (Object) null)
          Object.Destroy((Object) current.gameObject);
      }
    }
    if (!((Object) EndGameScreen.Get() != (Object) null))
      return;
    EndGameScreen.Get().m_hitbox.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.Hide));
  }

  public void CheatRankUp(string[] args)
  {
    bool flag1 = false;
    bool flag2 = false;
    for (int index = 0; index < args.Length; ++index)
    {
      if (args[index].ToLower() == "winstreak")
        flag1 = true;
      if (args[index].ToLower() == "chest")
        flag2 = true;
    }
    this.m_debugClickCatcher.gameObject.SetActive(true);
    this.m_debugClickCatcher.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.Hide));
    this.m_medalInfoTranslator = new MedalInfoTranslator();
    TranslatedMedalInfo prevMedal = new TranslatedMedalInfo();
    TranslatedMedalInfo currMedal = new TranslatedMedalInfo();
    int result1 = 14;
    int result2 = 15;
    int result3 = 3;
    int result4 = 1;
    if (args.Length >= 2)
    {
      int.TryParse(args[0], out result1);
      int.TryParse(args[1], out result2);
    }
    if (args.Length >= 4)
    {
      int.TryParse(args[2], out result3);
      int.TryParse(args[3], out result4);
    }
    prevMedal.earnedStars = result3;
    prevMedal.totalStars = 3;
    if (result1 <= 15)
      prevMedal.totalStars = 4;
    if (result1 <= 10)
      prevMedal.totalStars = 5;
    prevMedal.canLoseStars = result1 <= 20;
    prevMedal.canLoseLevel = result1 < 20;
    prevMedal.name = string.Format("Rank {0}", (object) result1);
    prevMedal.nextMedalName = string.Format("Rank {0}", (object) result2);
    prevMedal.rank = result1;
    prevMedal.textureName = string.Format("Medal_Ranked_{0}", (object) result1);
    currMedal.earnedStars = result4;
    currMedal.totalStars = 3;
    if (result2 <= 15)
      currMedal.totalStars = 4;
    if (result2 <= 10)
      currMedal.totalStars = 5;
    if (result2 == 0)
      currMedal.legendIndex = 1337;
    currMedal.canLoseStars = result2 <= 20;
    currMedal.canLoseLevel = result2 < 20;
    currMedal.name = string.Format("Rank {0}", (object) result2);
    currMedal.nextMedalName = string.Format("Rank {0}", (object) result2);
    currMedal.rank = result2;
    currMedal.textureName = string.Format("Medal_Ranked_{0}", (object) result2);
    if (flag1)
      currMedal.winStreak = 3;
    if (flag2)
    {
      prevMedal.bestRank = prevMedal.rank;
      currMedal.bestRank = currMedal.rank;
    }
    this.m_medalInfoTranslator.TestSetMedalInfo(currMedal, prevMedal);
  }

  public void Initialize(MedalInfoTranslator medalInfoTranslator, RankChangeTwoScoop.RankChangeClosed callback)
  {
    if (medalInfoTranslator != null)
      this.m_medalInfoTranslator = medalInfoTranslator;
    bool useWildMedal = Options.Get().GetBool(Option.IN_WILD_MODE);
    this.m_currMedalInfo = this.m_medalInfoTranslator.GetCurrentMedal(useWildMedal);
    this.m_prevMedalInfo = this.m_medalInfoTranslator.GetPreviousMedal(useWildMedal);
    this.m_validPrevMedal = this.m_medalInfoTranslator.IsPreviousMedalValid();
    this.m_closedCallback = callback;
    this.InitMedalsAndStars();
  }

  private void Show()
  {
    if (this.m_numTexturesLoading > 0)
      return;
    this.m_medalContainer.SetActive(true);
    AnimationUtil.ShowWithPunch(this.gameObject, this.START_SCALE, this.END_SCALE, this.AFTER_PUNCH_SCALE, "AnimateRankChange", true, (GameObject) null, (object) null, (AnimationUtil.DelOnShownWithPunch) null);
    SoundManager.Get().LoadAndPlay("rank_window_expand");
  }

  public void AnimateRankChange()
  {
    float delay = 0.0f;
    bool useWildMedal = Options.Get().GetBool(Option.IN_WILD_MODE);
    switch (this.m_medalInfoTranslator.GetChangeType(useWildMedal))
    {
      case RankChangeType.RANK_UP:
        delay = this.IncreaseStars(this.m_prevMedalInfo.earnedStars, this.m_prevMedalInfo.totalStars);
        if (this.m_medalInfoTranslator.ShowRewardChest(useWildMedal))
        {
          this.StartCoroutine(this.PlayChestLevelUp(delay + 0.6f));
          break;
        }
        this.StartCoroutine(this.PlayLevelUp(delay + 0.6f));
        break;
      case RankChangeType.RANK_DOWN:
        delay = this.DecreaseStars(0, this.m_prevMedalInfo.earnedStars);
        this.StartCoroutine(this.PlayLevelDown(delay + 0.2f));
        break;
      case RankChangeType.RANK_SAME:
        if (!this.m_currMedalInfo.IsLegendRank())
        {
          delay = this.m_currMedalInfo.earnedStars <= this.m_prevMedalInfo.earnedStars ? this.DecreaseStars(this.m_currMedalInfo.earnedStars, this.m_prevMedalInfo.earnedStars) : this.IncreaseStars(this.m_prevMedalInfo.earnedStars, this.m_currMedalInfo.earnedStars);
          break;
        }
        if (this.m_currMedalInfo.legendIndex == 0)
          this.m_legendIndex.gameObject.SetActive(false);
        else
          this.m_legendIndex.gameObject.SetActive(true);
        this.UpdateLegendText(this.m_currMedalInfo.legendIndex);
        break;
    }
    this.StartCoroutine(this.EnableHitboxOnAnimFinished(delay));
  }

  private void UpdateLegendText(int legendIndex)
  {
    if (legendIndex == -1)
      this.m_legendIndex.Text = string.Empty;
    else if (legendIndex == 0)
      this.m_legendIndex.Text = string.Empty;
    else
      this.m_legendIndex.Text = legendIndex.ToString();
  }

  [DebuggerHidden]
  private IEnumerator EnableHitboxOnAnimFinished(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeTwoScoop.\u003CEnableHitboxOnAnimFinished\u003Ec__Iterator97() { delay = delay, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }

  private float IncreaseStars(int blinkToThisIndex, int burstToThisIndex)
  {
    if (!this.m_currMedalInfo.IsLegendRank())
    {
      if (this.m_currMedalInfo.winStreak >= 3 && (this.m_prevMedalInfo.rank == this.m_currMedalInfo.rank ? this.m_currMedalInfo.earnedStars - this.m_prevMedalInfo.earnedStars : this.m_prevMedalInfo.totalStars - this.m_prevMedalInfo.earnedStars + this.m_currMedalInfo.earnedStars) > 1)
        this.m_winStreakParent.SetActive(true);
    }
    float delay = 0.0f;
    for (int index = 0; index < this.m_stars.Count; ++index)
    {
      delay = (float) (index + 1) * 0.2f;
      RankChangeStar star = this.m_stars[index];
      if (index < blinkToThisIndex)
        star.Blink(delay);
      else if (index < burstToThisIndex)
        star.Burst(delay);
    }
    return delay;
  }

  private float DecreaseStars(int lastWipeIndex, int firstWipeIndex)
  {
    bool flag1 = EndGameScreen.Get() is DefeatScreen;
    bool flag2 = false;
    if (flag1 && this.m_validPrevMedal)
    {
      if (!this.m_currMedalInfo.canLoseStars)
        flag2 = true;
      else if (this.m_currMedalInfo.IsHighestRankThatCannotBeLost() && this.m_currMedalInfo.earnedStars == 0 && (this.m_prevMedalInfo.rank == this.m_currMedalInfo.rank && this.m_prevMedalInfo.earnedStars == 0))
        flag2 = true;
    }
    this.m_scrubRankDesc.SetActive(flag2);
    float delay = 0.0f;
    for (int index = this.m_stars.Count - 1; index >= 0; --index)
    {
      delay = (float) (this.m_stars.Count - index) * 0.2f;
      RankChangeStar star = this.m_stars[index];
      if (star.gameObject.activeInHierarchy && index < firstWipeIndex && index >= lastWipeIndex)
        star.Wipe(delay);
    }
    return delay;
  }

  private void Hide(UIEvent e)
  {
    if ((Object) EndGameScreen.Get() != (Object) null)
      EndGameScreen.Get().m_hitbox.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.Hide));
    if ((Object) this.gameObject != (Object) null)
      AnimationUtil.ScaleFade(this.gameObject, new Vector3(0.1f, 0.1f, 0.1f), "DestroyRankChange");
    if (!((Object) SoundManager.Get() != (Object) null))
      return;
    SoundManager.Get().LoadAndPlay("rank_window_shrink");
  }

  private void DestroyRankChange()
  {
    if (this.m_closedCallback != null)
      this.m_closedCallback();
    Object.Destroy((Object) this.gameObject);
  }

  private void InitStars(int numEarned, int numTotal, bool fadeIn = false)
  {
    int num = 0;
    List<Transform> transformList;
    if (numTotal % 2 == 0)
    {
      transformList = this.m_evenStarBones;
      if (numTotal == 2)
        num = 1;
    }
    else
    {
      transformList = this.m_oddStarBones;
      if (numTotal == 3)
        num = 1;
      else if (numTotal == 1)
        num = 2;
    }
    for (int index = 0; index < numTotal; ++index)
    {
      RankChangeStar star = this.m_stars[index];
      star.gameObject.SetActive(true);
      star.transform.localScale = new Vector3(0.22f, 0.1f, 0.22f);
      star.transform.position = transformList[index + num].position;
      star.Reset();
      if (index >= numEarned)
        star.BlackOut();
      else
        star.UnBlackOut();
      if (fadeIn)
        star.FadeIn();
    }
    for (int index = numTotal; index < this.m_stars.Count; ++index)
      this.m_stars[index].gameObject.SetActive(false);
  }

  private void OnTopTextureLoaded(string assetName, Object asset, object callbackData)
  {
    if (asset == (Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("RankChangeTwoScoop.OnTopTextureLoaded(): asset for {0} is null!", (object) assetName));
    }
    else
    {
      Texture texture = asset as Texture;
      if ((Object) texture == (Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("RankChangeTwoScoop.OnTopTextureLoaded(): medalTexture for {0} is null (asset is not a texture)!", (object) assetName));
      }
      else
      {
        this.m_rankMedalTop.GetComponent<Renderer>().material.mainTexture = texture;
        --this.m_numTexturesLoading;
        this.Show();
      }
    }
  }

  private void OnBottomTextureLoaded(string assetName, Object asset, object callbackData)
  {
    if (asset == (Object) null)
    {
      UnityEngine.Debug.LogWarning((object) string.Format("RankChangeTwoScoop.OnBottomTextureLoaded(): asset for {0} is null!", (object) assetName));
    }
    else
    {
      Texture texture = asset as Texture;
      if ((Object) texture == (Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("RankChangeTwoScoop.OnBottomTextureLoaded(): medalTexture for {0} is null (asset is not a texture)!", (object) assetName));
      }
      else
      {
        this.m_rankMedalBottom.GetComponent<Renderer>().material.mainTexture = texture;
        --this.m_numTexturesLoading;
        this.Show();
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator PlayLevelUp(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeTwoScoop.\u003CPlayLevelUp\u003Ec__Iterator98() { delay = delay, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator PlayChestLevelUp(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeTwoScoop.\u003CPlayChestLevelUp\u003Ec__Iterator99() { delay = delay, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }

  private void DespawnOldStars()
  {
    this.m_winStreakParent.SetActive(false);
    for (int index = 0; index < this.m_stars.Count; ++index)
    {
      if (this.m_stars[index].gameObject.activeSelf)
        this.m_stars[index].GetComponent<PlayMakerFSM>().SendEvent("DeSpawn");
    }
  }

  private void StartChestGlow()
  {
    if (!((Object) SoundManager.Get() != (Object) null))
      return;
    SoundManager.Get().LoadAndPlay("tutorial_intro_box_opens");
  }

  [DebuggerHidden]
  private IEnumerator UpdateToCurrentChestCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeTwoScoop.\u003CUpdateToCurrentChestCoroutine\u003Ec__Iterator9A() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitAndPlayRankedStarImpact(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeTwoScoop.\u003CWaitAndPlayRankedStarImpact\u003Ec__Iterator9B() { delay = delay, \u003C\u0024\u003Edelay = delay };
  }

  private void UpdateToCurrentChest()
  {
    this.StartCoroutine(this.UpdateToCurrentChestCoroutine());
  }

  private void UpdateChestAfterLevelUp()
  {
    if (this.m_rewardChest.DoesChestVisualChange(this.m_chestRank, this.m_currMedalInfo.bestRank))
      return;
    this.UpdateToFinalChest();
  }

  private void UpdateToFinalChest()
  {
    if ((Object) SoundManager.Get() != (Object) null && this.m_prevMedalInfo.CanGetRankedRewardChest())
      SoundManager.Get().LoadAndPlay("level_up");
    Log.EndOfGame.Print("Updating to final chest..");
    this.m_rewardChest.SetRank(this.m_currMedalInfo.bestRank);
    this.m_rewardChestInstructions.gameObject.SetActive(true);
    this.m_rewardChestInstructions.TextAlpha = 0.0f;
    iTween.FadeTo(this.m_rewardChestInstructions.gameObject, 1f, 1f);
  }

  private void PlayChestChange()
  {
    if (!this.m_prevMedalInfo.CanGetRankedRewardChest() || !this.m_rewardChest.DoesChestVisualChange(this.m_chestRank, this.m_currMedalInfo.bestRank))
      return;
    this.m_rewardChest.GetChestVisualFromRank(this.m_currMedalInfo.bestRank).m_glowMesh.gameObject.SetActive(true);
    Animator component = this.GetComponent<Animator>();
    string chestChangeAnimation = this.m_rewardChest.GetChestVisualFromRank(this.m_currMedalInfo.bestRank).chestChangeAnimation;
    Log.EndOfGame.Print("playing chest change animation " + chestChangeAnimation);
    component.Play(chestChangeAnimation);
  }

  [DebuggerHidden]
  private IEnumerator PlayLevelDown(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeTwoScoop.\u003CPlayLevelDown\u003Ec__Iterator9C() { delay = delay, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }

  private void LevelUpChanges()
  {
    this.m_rankMedalTop.GetComponent<Renderer>().material.mainTexture = this.m_rankMedalBottom.GetComponent<Renderer>().material.mainTexture;
    this.m_rankNumberTop.Text = this.m_currMedalInfo.rank.ToString();
    this.m_name.Text = this.m_currMedalInfo.name;
    if (!((Object) Gameplay.Get() != (Object) null))
      return;
    Gameplay.Get().UpdateFriendlySideMedalChange(this.m_medalInfoTranslator);
  }

  [DebuggerHidden]
  private IEnumerator LegendaryChanges()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RankChangeTwoScoop.\u003CLegendaryChanges\u003Ec__Iterator9D() { \u003C\u003Ef__this = this };
  }

  private void InitMedalsAndStars()
  {
    bool useWildMedal = Options.Get().GetBool(Option.IN_WILD_MODE);
    this.m_wildFlair.SetActive(useWildMedal);
    switch (this.m_medalInfoTranslator.GetChangeType(useWildMedal))
    {
      case RankChangeType.RANK_UP:
        this.m_rankNumberTop.Text = this.m_prevMedalInfo.rank.ToString();
        this.m_name.Text = this.m_prevMedalInfo.name;
        this.m_numTexturesLoading = 2;
        AssetLoader.Get().LoadTexture(this.m_prevMedalInfo.textureName, new AssetLoader.ObjectCallback(this.OnTopTextureLoaded), (object) null, false);
        AssetLoader.Get().LoadTexture(this.m_currMedalInfo.textureName, new AssetLoader.ObjectCallback(this.OnBottomTextureLoaded), (object) null, false);
        this.InitStars(this.m_prevMedalInfo.earnedStars, this.m_prevMedalInfo.totalStars, false);
        break;
      case RankChangeType.RANK_DOWN:
        this.m_rankNumberBottom.Text = this.m_currMedalInfo.rank.ToString();
        this.m_rankNumberTop.Text = this.m_prevMedalInfo.rank.ToString();
        this.m_name.Text = this.m_prevMedalInfo.name;
        this.m_numTexturesLoading = 2;
        AssetLoader.Get().LoadTexture(this.m_prevMedalInfo.textureName, new AssetLoader.ObjectCallback(this.OnTopTextureLoaded), (object) null, false);
        AssetLoader.Get().LoadTexture(this.m_currMedalInfo.textureName, new AssetLoader.ObjectCallback(this.OnBottomTextureLoaded), (object) null, false);
        this.InitStars(this.m_prevMedalInfo.earnedStars, this.m_prevMedalInfo.totalStars, false);
        break;
      case RankChangeType.RANK_SAME:
        if (this.m_currMedalInfo.rank == 0)
        {
          this.m_bannerTop.SetActive(false);
          this.m_bannerBottom.SetActive(false);
          this.m_rankMedalBottom.SetActive(false);
        }
        else
        {
          this.m_rankNumberTop.Text = this.m_currMedalInfo.rank.ToString();
          this.m_bannerBottom.SetActive(false);
          this.m_rankNumberBottom.gameObject.SetActive(false);
        }
        this.m_name.Text = this.m_currMedalInfo.name;
        this.m_numTexturesLoading = 1;
        AssetLoader.Get().LoadTexture(this.m_currMedalInfo.textureName, new AssetLoader.ObjectCallback(this.OnTopTextureLoaded), (object) null, false);
        this.InitStars(this.m_prevMedalInfo.earnedStars, this.m_prevMedalInfo.totalStars, false);
        break;
    }
  }

  public delegate void RankChangeClosed();
}
