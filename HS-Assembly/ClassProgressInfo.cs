﻿// Decompiled with JetBrains decompiler
// Type: ClassProgressInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class ClassProgressInfo
{
  public GameObject m_bone;
  public TAG_CLASS m_class;
  public Material m_iconMaterial;
}
