﻿// Decompiled with JetBrains decompiler
// Type: GeneralStoreHeroesContentDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class GeneralStoreHeroesContentDisplay : MonoBehaviour
{
  private bool m_keyArtShowing = true;
  public UberText m_heroName;
  public UberText m_className;
  public GameObject m_renderArtQuad;
  public UIBButton m_previewToggle;
  public Animator m_keyArtAnimation;
  public MeshRenderer m_classIcon;
  public MeshRenderer m_fauxPlateTexture;
  public MeshRenderer m_backgroundFrame;
  public int m_backgroundMaterialIndex;
  private Texture m_defaultBackgroundTexture;
  public GameObject m_heroContainer;
  public GameObject m_heroPowerContainer;
  public GameObject m_cardBackContainer;
  public GameObject m_previewButtonFX;
  public GameObject m_purchasedCheckMark;
  public GeneralStoreHeroesContentLite m_parentLite;
  private GeneralStoreHeroesContent m_parent;
  private CollectionHeroDef m_currentHeroAsset;
  private GameObject m_cardBack;
  private Actor m_heroActor;
  private Actor m_heroPowerActor;
  private CardSoundSpell m_previewEmote;
  private CardSoundSpell m_purchaseEmote;
  private MeshRenderer m_keyArt;

  private void Awake()
  {
    if ((Object) this.m_defaultBackgroundTexture == (Object) null && (Object) this.m_backgroundFrame != (Object) null && (this.m_backgroundMaterialIndex >= 0 && this.m_backgroundMaterialIndex < this.m_backgroundFrame.materials.Length))
      this.m_defaultBackgroundTexture = this.m_backgroundFrame.materials[this.m_backgroundMaterialIndex].GetTexture("_MainTex");
    this.m_previewToggle.AddEventListener(UIEventType.RELEASE, (UIEvent.Handler) (e => this.TogglePreview()));
  }

  public void SetKeyArtRenderer(MeshRenderer keyArtRenderer)
  {
    this.m_keyArt = keyArtRenderer;
  }

  public void PlayPreviewEmote()
  {
    if ((Object) this.m_previewEmote == (Object) null || (Object) Box.Get() == (Object) null || (Object) Box.Get().GetCamera() == (Object) null)
      return;
    this.m_previewEmote.SetPosition(Box.Get().GetCamera().transform.position);
    this.m_previewEmote.Reactivate();
  }

  public void PlayPurchaseEmote()
  {
    if ((Object) this.m_purchaseEmote == (Object) null)
      return;
    this.m_purchaseEmote.SetPosition(Box.Get().GetCamera().transform.position);
    this.m_purchaseEmote.Reactivate();
  }

  public void SetParent(GeneralStoreHeroesContent parent)
  {
    this.m_parent = parent;
  }

  public void Init()
  {
    if ((Object) this.m_heroActor == (Object) null)
    {
      this.m_heroActor = AssetLoader.Get().LoadActor("Card_Play_Hero", false, false).GetComponent<Actor>();
      this.m_heroActor.SetUnlit();
      this.m_heroActor.Show();
      this.m_heroActor.GetHealthObject().Hide();
      this.m_heroActor.GetAttackObject().Hide();
      GameUtils.SetParent((Component) this.m_heroActor, this.m_heroContainer, true);
      SceneUtils.SetLayer((Component) this.m_heroActor, this.m_heroContainer.layer);
    }
    if (!((Object) this.m_heroPowerActor == (Object) null))
      return;
    this.m_heroPowerActor = AssetLoader.Get().LoadActor("Card_Play_HeroPower", false, false).GetComponent<Actor>();
    this.m_heroPowerActor.SetUnlit();
    this.m_heroPowerActor.Show();
    GameUtils.SetParent((Component) this.m_heroPowerActor, this.m_heroPowerContainer, true);
    SceneUtils.SetLayer((Component) this.m_heroPowerActor, this.m_heroPowerContainer.layer);
  }

  public void ShowPurchasedCheckmark(bool show)
  {
    if (!((Object) this.m_purchasedCheckMark != (Object) null))
      return;
    this.m_purchasedCheckMark.SetActive(show);
  }

  public void UpdateFrame(CardHeroDbfRecord cardHeroDbfRecord, int cardBackIdx, CollectionHeroDef heroDef)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GeneralStoreHeroesContentDisplay.\u003CUpdateFrame\u003Ec__AnonStorey3EF frameCAnonStorey3Ef = new GeneralStoreHeroesContentDisplay.\u003CUpdateFrame\u003Ec__AnonStorey3EF();
    // ISSUE: reference to a compiler-generated field
    frameCAnonStorey3Ef.heroDef = heroDef;
    // ISSUE: reference to a compiler-generated field
    frameCAnonStorey3Ef.cardBackIdx = cardBackIdx;
    // ISSUE: reference to a compiler-generated field
    frameCAnonStorey3Ef.\u003C\u003Ef__this = this;
    this.Init();
    // ISSUE: reference to a compiler-generated field
    this.m_keyArt.material = frameCAnonStorey3Ef.heroDef.m_previewTexture;
    // ISSUE: reference to a compiler-generated field
    if ((Object) frameCAnonStorey3Ef.heroDef.m_fauxPlateTexture != (Object) null)
    {
      // ISSUE: reference to a compiler-generated field
      this.m_fauxPlateTexture.material.SetTexture("_MainTex", frameCAnonStorey3Ef.heroDef.m_fauxPlateTexture);
    }
    // ISSUE: reference to a compiler-generated method
    DefLoader.Get().LoadFullDef(GameUtils.TranslateDbIdToCardId(cardHeroDbfRecord.CardId), new DefLoader.LoadDefCallback<FullDef>(frameCAnonStorey3Ef.\u003C\u003Em__1BA));
    if ((Object) this.m_cardBack != (Object) null)
    {
      Object.Destroy((Object) this.m_cardBack);
      this.m_cardBack = (GameObject) null;
    }
    // ISSUE: reference to a compiler-generated field
    if (frameCAnonStorey3Ef.cardBackIdx != 0)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      CardBackManager.Get().LoadCardBackByIndex(frameCAnonStorey3Ef.cardBackIdx, new CardBackManager.LoadCardBackData.LoadCardBackCallback(frameCAnonStorey3Ef.\u003C\u003Em__1BB), "Card_Hidden");
    }
    if (!((Object) this.m_backgroundFrame != (Object) null) || this.m_backgroundMaterialIndex < 0 || this.m_backgroundMaterialIndex > this.m_backgroundFrame.materials.Length)
      return;
    Texture texture1 = this.m_defaultBackgroundTexture;
    if (!string.IsNullOrEmpty(cardHeroDbfRecord.StoreBackgroundTexture))
    {
      Texture texture2 = AssetLoader.Get().LoadTexture(FileUtils.GameAssetPathToName(cardHeroDbfRecord.StoreBackgroundTexture), false);
      if ((Object) texture2 != (Object) null)
        texture1 = texture2;
    }
    if (!((Object) texture1 != (Object) null))
      return;
    this.m_backgroundFrame.materials[this.m_backgroundMaterialIndex].SetTexture("_MainTex", texture1);
  }

  public void TogglePreview()
  {
    if ((Object) this.m_parentLite != (Object) null)
    {
      if (!string.IsNullOrEmpty(this.m_parentLite.m_previewButtonClick))
        SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_parentLite.m_previewButtonClick));
    }
    else if (!string.IsNullOrEmpty(this.m_parent.m_previewButtonClick))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(this.m_parent.m_previewButtonClick));
    this.PlayKeyArtAnimation(this.m_keyArtShowing);
    this.m_keyArtShowing = !this.m_keyArtShowing;
    if (!this.m_keyArtShowing)
    {
      this.m_heroActor.Show();
      this.m_heroPowerActor.Show();
      this.PlayPreviewEmote();
    }
    else
    {
      this.m_heroActor.Hide();
      this.m_heroPowerActor.Hide();
    }
  }

  public void ResetPreview()
  {
    this.m_keyArtShowing = true;
    this.m_keyArtAnimation.enabled = true;
    this.m_keyArtAnimation.StopPlayback();
    if ((Object) this.m_parentLite != (Object) null)
      this.m_keyArtAnimation.Play(this.m_parentLite.m_keyArtAppearAnim, -1, 1f);
    else
      this.m_keyArtAnimation.Play(this.m_parent.m_keyArtAppearAnim, -1, 1f);
    this.m_previewButtonFX.SetActive(false);
  }

  private void PlayKeyArtAnimation(bool showPreview)
  {
    string stateName;
    string path;
    if (showPreview)
    {
      if ((Object) this.m_parentLite != (Object) null)
      {
        stateName = this.m_parentLite.m_keyArtFadeAnim;
        path = this.m_parentLite.m_keyArtFadeSound;
      }
      else
      {
        stateName = this.m_parent.m_keyArtFadeAnim;
        path = this.m_parent.m_keyArtFadeSound;
      }
    }
    else if ((Object) this.m_parentLite != (Object) null)
    {
      stateName = this.m_parentLite.m_keyArtAppearAnim;
      path = this.m_parentLite.m_keyArtAppearSound;
    }
    else
    {
      stateName = this.m_parent.m_keyArtAppearAnim;
      path = this.m_parent.m_keyArtAppearSound;
    }
    this.m_previewButtonFX.SetActive(showPreview);
    if (!string.IsNullOrEmpty(path))
      SoundManager.Get().LoadAndPlay(FileUtils.GameAssetPathToName(path));
    this.m_keyArtAnimation.enabled = true;
    this.m_keyArtAnimation.StopPlayback();
    this.m_keyArtAnimation.Play(stateName, -1, 0.0f);
  }

  private void ClearEmotes()
  {
    if ((Object) this.m_previewEmote != (Object) null)
      Object.Destroy((Object) this.m_previewEmote.gameObject);
    if (!((Object) this.m_purchaseEmote != (Object) null))
      return;
    Object.Destroy((Object) this.m_purchaseEmote.gameObject);
  }
}
