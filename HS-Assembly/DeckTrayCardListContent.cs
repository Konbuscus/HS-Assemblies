﻿// Decompiled with JetBrains decompiler
// Type: DeckTrayCardListContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class DeckTrayCardListContent : DeckTrayContent
{
  [CustomEditField(Sections = "Card Tile Settings")]
  public float m_cardTileHeight = 2.45f;
  [CustomEditField(Sections = "Card Tile Settings")]
  public float m_cardHelpButtonHeight = 3f;
  [CustomEditField(Sections = "Card Tile Settings")]
  public float m_deckCardBarFlareUpInterval = 0.075f;
  [CustomEditField(Sections = "Card Tile Settings")]
  public Vector3 m_cardTileOffset = Vector3.zero;
  [CustomEditField(Sections = "Card Tile Settings")]
  public Vector3 m_cardTileSlotLocalScaleVec3 = new Vector3(0.01f, 0.02f, 0.01f);
  private List<DeckTrayDeckTileVisual> m_cardTiles = new List<DeckTrayDeckTileVisual>();
  private List<DeckTrayCardListContent.CardTileHeld> m_cardTileHeldListeners = new List<DeckTrayCardListContent.CardTileHeld>();
  private List<DeckTrayCardListContent.CardTilePress> m_cardTilePressListeners = new List<DeckTrayCardListContent.CardTilePress>();
  private List<DeckTrayCardListContent.CardTileTap> m_cardTileTapListeners = new List<DeckTrayCardListContent.CardTileTap>();
  private List<DeckTrayCardListContent.CardTileOver> m_cardTileOverListeners = new List<DeckTrayCardListContent.CardTileOver>();
  private List<DeckTrayCardListContent.CardTileOut> m_cardTileOutListeners = new List<DeckTrayCardListContent.CardTileOut>();
  private List<DeckTrayCardListContent.CardTileRelease> m_cardTileReleaseListeners = new List<DeckTrayCardListContent.CardTileRelease>();
  private List<DeckTrayCardListContent.CardTileRightClicked> m_cardTileRightClickedListeners = new List<DeckTrayCardListContent.CardTileRightClicked>();
  private List<DeckTrayCardListContent.CardCountChanged> m_cardCountChangedListeners = new List<DeckTrayCardListContent.CardCountChanged>();
  private CollectionDeck m_templateFakeDeck = new CollectionDeck();
  private bool m_hasFinishedExiting = true;
  private const string ADD_CARD_TO_DECK_SOUND = "collection_manager_card_add_to_deck_instant";
  private const float CARD_MOVEMENT_TIME = 0.3f;
  private const float DECK_HELP_BUTTON_EMPTY_DECK_Y_LOCAL_POS = -0.01194457f;
  private const float DECK_HELP_BUTTON_Y_TILE_OFFSET = -0.04915909f;
  [CustomEditField(Sections = "Card Tile Settings")]
  public GameObject m_phoneDeckTileBone;
  [CustomEditField(Sections = "Card Tile Settings")]
  public float m_cardTileSlotLocalHeight;
  [CustomEditField(Sections = "Deck Help")]
  public UIBButton m_deckHelpButton;
  [CustomEditField(Sections = "Deck Help")]
  public UIBButton m_deckTemplateHelpButton;
  [CustomEditField(Sections = "Other Objects")]
  public GameObject m_deckCompleteHighlight;
  [CustomEditField(Sections = "Scroll Settings")]
  public UIBScrollable m_scrollbar;
  [CustomEditField(Sections = "Scroll Settings")]
  public BoxCollider m_LockedScrollBounds;
  [CustomEditField(Sections = "Deck Type Settings")]
  public CollectionManager.DeckTag m_deckType;
  private Vector3 m_originalLocalPosition;
  private bool m_animating;
  private bool m_loading;
  private bool m_inArena;
  private bool m_isShowingFakeDeck;
  private bool m_hasFinishedEntering;
  private Notification m_deckHelpPopup;

  private void Awake()
  {
    if ((UnityEngine.Object) this.m_deckHelpButton != (UnityEngine.Object) null)
    {
      this.m_deckHelpButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDeckHelpButtonPress));
      this.m_deckHelpButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnDeckHelpButtonOver));
      this.m_deckHelpButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnDeckHelpButtonOut));
    }
    if ((UnityEngine.Object) this.m_deckTemplateHelpButton != (UnityEngine.Object) null)
    {
      this.m_deckTemplateHelpButton.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnDeckTemplateHelpButtonPress));
      this.m_deckTemplateHelpButton.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(this.OnDeckTemplateHelpButtonOver));
      this.m_deckTemplateHelpButton.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(this.OnDeckTemplateHelpButtonOut));
    }
    this.m_originalLocalPosition = this.transform.localPosition;
    this.m_hasFinishedEntering = false;
  }

  public override bool AnimateContentEntranceStart()
  {
    if (this.m_loading)
      return false;
    this.m_animating = true;
    this.m_hasFinishedEntering = false;
    Action<object> action = (Action<object>) (_1 =>
    {
      this.UpdateDeckCompleteHighlight();
      this.ShowDeckEditingTipsIfNeeded();
      this.m_animating = false;
    });
    CollectionDeck editingDeck = this.GetEditingDeck();
    if (editingDeck != null)
    {
      this.transform.localPosition = this.GetOffscreenLocalPosition();
      iTween.StopByName(this.gameObject, "position");
      iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) this.m_originalLocalPosition, (object) "isLocal", (object) true, (object) "time", (object) 0.3f, (object) "easeType", (object) iTween.EaseType.easeOutQuad, (object) "oncomplete", (object) action, (object) "name", (object) "position"));
      if (editingDeck.GetTotalCardCount() > 0)
        SoundManager.Get().LoadAndPlay("collection_manager_new_deck_moves_up_tray", this.gameObject);
      this.UpdateCardList(false, (Actor) null);
    }
    else
      action((object) null);
    return true;
  }

  public override bool AnimateContentEntranceEnd()
  {
    if (this.m_animating)
      return false;
    this.m_hasFinishedEntering = true;
    this.FireCardCountChangedEvent();
    return true;
  }

  public override bool AnimateContentExitStart()
  {
    if (this.m_animating)
      return false;
    this.m_animating = true;
    this.m_hasFinishedExiting = false;
    if ((UnityEngine.Object) this.m_deckCompleteHighlight != (UnityEngine.Object) null)
      this.m_deckCompleteHighlight.SetActive(false);
    iTween.StopByName(this.gameObject, "position");
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) this.GetOffscreenLocalPosition(), (object) "isLocal", (object) true, (object) "time", (object) 0.3f, (object) "easeType", (object) iTween.EaseType.easeInQuad, (object) "name", (object) "position"));
    if ((UnityEngine.Object) HeroPickerDisplay.Get() == (UnityEngine.Object) null || !HeroPickerDisplay.Get().IsShown())
      SoundManager.Get().LoadAndPlay("panel_slide_off_deck_creation_screen", this.gameObject);
    ApplicationMgr.Get().ScheduleCallback(0.5f, false, (ApplicationMgr.ScheduledCallback) (o => this.m_animating = false), (object) null);
    return true;
  }

  public override bool AnimateContentExitEnd()
  {
    this.m_hasFinishedExiting = true;
    return !this.m_animating;
  }

  public bool HasFinishedEntering()
  {
    return this.m_hasFinishedEntering;
  }

  public bool HasFinishedExiting()
  {
    return this.m_hasFinishedExiting;
  }

  public override void OnTaggedDeckChanged(CollectionManager.DeckTag tag, CollectionDeck newDeck, CollectionDeck oldDeck, bool isNewDeck)
  {
    if (tag != CollectionManager.DeckTag.Editing || newDeck == null)
      return;
    this.LoadCardPrefabs(newDeck.GetSlots());
    if (!this.IsModeActive())
      return;
    this.ShowDeckHelpButtonIfNeeded();
  }

  public void ShowDeckHelper(DeckTrayDeckTileVisual tileToRemove, bool continueAfterReplace, bool replacingCard = false)
  {
    if (!CollectionManager.Get().IsInEditMode() || !(bool) ((UnityEngine.Object) DeckHelper.Get()))
      return;
    DeckHelper.Get().Show(tileToRemove, continueAfterReplace, replacingCard);
  }

  public Vector3 GetCardVisualExtents()
  {
    return new Vector3(this.m_cardTileHeight, this.m_cardTileHeight, this.m_cardTileHeight);
  }

  public List<DeckTrayDeckTileVisual> GetCardTiles()
  {
    return this.m_cardTiles;
  }

  public DeckTrayDeckTileVisual GetCardTileVisual(string cardID)
  {
    using (List<DeckTrayDeckTileVisual>.Enumerator enumerator = this.m_cardTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckTrayDeckTileVisual current = enumerator.Current;
        if (!((UnityEngine.Object) current == (UnityEngine.Object) null) && !((UnityEngine.Object) current.GetActor() == (UnityEngine.Object) null) && (current.GetActor().GetEntityDef() != null && current.GetActor().GetEntityDef().GetCardId() == cardID))
          return current;
      }
    }
    return (DeckTrayDeckTileVisual) null;
  }

  public DeckTrayDeckTileVisual GetCardTileVisual(string cardID, TAG_PREMIUM premType)
  {
    using (List<DeckTrayDeckTileVisual>.Enumerator enumerator = this.m_cardTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckTrayDeckTileVisual current = enumerator.Current;
        if (!((UnityEngine.Object) current == (UnityEngine.Object) null) && !((UnityEngine.Object) current.GetActor() == (UnityEngine.Object) null) && (current.GetActor().GetEntityDef() != null && current.GetActor().GetEntityDef().GetCardId() == cardID) && current.GetActor().GetPremium() == premType)
          return current;
      }
    }
    return (DeckTrayDeckTileVisual) null;
  }

  public DeckTrayDeckTileVisual GetCardTileVisual(int index)
  {
    if (index < this.m_cardTiles.Count)
      return this.m_cardTiles[index];
    return (DeckTrayDeckTileVisual) null;
  }

  public DeckTrayDeckTileVisual GetCardTileVisualOrLastVisible(string cardID)
  {
    int num = 0;
    using (List<DeckTrayDeckTileVisual>.Enumerator enumerator = this.m_cardTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckTrayDeckTileVisual current = enumerator.Current;
        ++num;
        if (!((UnityEngine.Object) current == (UnityEngine.Object) null) && !((UnityEngine.Object) current.GetActor() == (UnityEngine.Object) null) && current.GetActor().GetEntityDef() != null && (num > 20 || current.GetActor().GetEntityDef().GetCardId() == cardID))
          return current;
      }
    }
    return (DeckTrayDeckTileVisual) null;
  }

  public DeckTrayDeckTileVisual GetOrAddCardTileVisual(int index)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTrayCardListContent.\u003CGetOrAddCardTileVisual\u003Ec__AnonStorey42F visualCAnonStorey42F = new DeckTrayCardListContent.\u003CGetOrAddCardTileVisual\u003Ec__AnonStorey42F();
    // ISSUE: reference to a compiler-generated field
    visualCAnonStorey42F.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    visualCAnonStorey42F.newTileVisual = this.GetCardTileVisual(index);
    // ISSUE: reference to a compiler-generated field
    if ((UnityEngine.Object) visualCAnonStorey42F.newTileVisual != (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated field
      return visualCAnonStorey42F.newTileVisual;
    }
    GameObject child = new GameObject("DeckTileVisual" + (object) index);
    GameUtils.SetParent(child, (Component) this, false);
    child.transform.localScale = this.m_cardTileSlotLocalScaleVec3;
    // ISSUE: reference to a compiler-generated field
    visualCAnonStorey42F.newTileVisual = child.AddComponent<DeckTrayDeckTileVisual>();
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    visualCAnonStorey42F.newTileVisual.AddEventListener(UIEventType.HOLD, new UIEvent.Handler(visualCAnonStorey42F.\u003C\u003Em__2B6));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    visualCAnonStorey42F.newTileVisual.AddEventListener(UIEventType.PRESS, new UIEvent.Handler(visualCAnonStorey42F.\u003C\u003Em__2B7));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    visualCAnonStorey42F.newTileVisual.AddEventListener(UIEventType.TAP, new UIEvent.Handler(visualCAnonStorey42F.\u003C\u003Em__2B8));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    visualCAnonStorey42F.newTileVisual.AddEventListener(UIEventType.ROLLOVER, new UIEvent.Handler(visualCAnonStorey42F.\u003C\u003Em__2B9));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    visualCAnonStorey42F.newTileVisual.AddEventListener(UIEventType.ROLLOUT, new UIEvent.Handler(visualCAnonStorey42F.\u003C\u003Em__2BA));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    visualCAnonStorey42F.newTileVisual.AddEventListener(UIEventType.RELEASE, new UIEvent.Handler(visualCAnonStorey42F.\u003C\u003Em__2BB));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    visualCAnonStorey42F.newTileVisual.AddEventListener(UIEventType.RIGHTCLICK, new UIEvent.Handler(visualCAnonStorey42F.\u003C\u003Em__2BC));
    // ISSUE: reference to a compiler-generated field
    this.m_cardTiles.Insert(index, visualCAnonStorey42F.newTileVisual);
    Vector3 extents = new Vector3(this.m_cardTileHeight, this.m_cardTileHeight, this.m_cardTileHeight);
    if ((UnityEngine.Object) this.m_scrollbar != (UnityEngine.Object) null)
      this.m_scrollbar.AddVisibleAffectedObject(child, extents, true, new UIBScrollable.VisibleAffected(this.IsCardTileVisible));
    // ISSUE: reference to a compiler-generated field
    return visualCAnonStorey42F.newTileVisual;
  }

  public void UpdateTileVisuals()
  {
    using (List<DeckTrayDeckTileVisual>.Enumerator enumerator = this.m_cardTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateGhostedState();
    }
  }

  public override void Show(bool showAll = false)
  {
    using (List<DeckTrayDeckTileVisual>.Enumerator enumerator = this.m_cardTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckTrayDeckTileVisual current = enumerator.Current;
        if (showAll || current.IsInUse())
          current.Show();
      }
    }
  }

  public override void Hide(bool hideAll = false)
  {
    using (List<DeckTrayDeckTileVisual>.Enumerator enumerator = this.m_cardTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DeckTrayDeckTileVisual current = enumerator.Current;
        if (hideAll || !current.IsInUse())
          current.Hide();
      }
    }
  }

  public void CommitFakeDeckChanges()
  {
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(this.m_deckType);
    taggedDeck.CopyContents(this.m_templateFakeDeck);
    taggedDeck.Name = this.m_templateFakeDeck.Name;
  }

  public CollectionDeck GetEditingDeck()
  {
    if (this.m_isShowingFakeDeck)
      return this.m_templateFakeDeck;
    return CollectionManager.Get().GetTaggedDeck(this.m_deckType);
  }

  public void ShowFakeDeck(bool show)
  {
    if (this.m_isShowingFakeDeck == show)
      return;
    this.m_isShowingFakeDeck = show;
    this.UpdateCardList(true, (Actor) null);
  }

  public void ResetFakeDeck()
  {
    if (this.m_templateFakeDeck == null)
      return;
    CollectionDeck taggedDeck = CollectionManager.Get().GetTaggedDeck(this.m_deckType);
    if (taggedDeck == null)
      return;
    this.m_templateFakeDeck.CopyContents(taggedDeck);
    this.m_templateFakeDeck.Name = taggedDeck.Name;
  }

  public void ShowDeckCompleteEffects()
  {
    this.StartCoroutine(this.ShowDeckCompleteEffectsWithInterval(this.m_deckCardBarFlareUpInterval));
  }

  public void SetInArena(bool inArena)
  {
    this.m_inArena = inArena;
  }

  public bool AddCard(EntityDef cardEntityDef, TAG_PREMIUM premium, DeckTrayDeckTileVisual deckTileToRemove, bool playSound, Actor animateFromActor = null)
  {
    if (!this.IsModeActive())
      return false;
    if (cardEntityDef == null)
    {
      UnityEngine.Debug.LogError((object) "Trying to add card EntityDef that is null.");
      return false;
    }
    string cardId1 = cardEntityDef.GetCardId();
    CollectionDeck editingDeck = this.GetEditingDeck();
    if (editingDeck == null || !editingDeck.CanAddOwnedCard(cardId1, premium))
      return false;
    if (playSound)
      SoundManager.Get().LoadAndPlay("collection_manager_place_card_in_deck", this.gameObject);
    bool flag = editingDeck.GetTotalCardCount() == CollectionManager.Get().GetDeckSize();
    DeckTrayDeckTileVisual firstInvalidCard = this.GetFirstInvalidCard();
    if (flag && ((UnityEngine.Object) deckTileToRemove == (UnityEngine.Object) null || editingDeck.IsValidSlot(deckTileToRemove.GetSlot())) && (UnityEngine.Object) firstInvalidCard != (UnityEngine.Object) null)
      deckTileToRemove = firstInvalidCard;
    if (flag || (UnityEngine.Object) deckTileToRemove != (UnityEngine.Object) null && !editingDeck.IsValidSlot(deckTileToRemove.GetSlot()))
    {
      if ((UnityEngine.Object) deckTileToRemove == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckTray.AddCard(): Cannot add card {0} (premium {1}) without removing one first.", (object) cardEntityDef.GetCardId(), (object) premium));
        return false;
      }
      deckTileToRemove.SetHighlight(false);
      string cardId2 = deckTileToRemove.GetCardID();
      TAG_PREMIUM premium1 = deckTileToRemove.GetPremium();
      if (!editingDeck.RemoveCard(cardId2, premium1, editingDeck.IsValidSlot(deckTileToRemove.GetSlot()), false))
      {
        UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckTray.AddCard({0},{1}): Tried to remove card {2} with premium {3}, but it failed!", (object) cardId1, (object) premium, (object) cardId2, (object) premium1));
        return false;
      }
    }
    if (!editingDeck.AddCard(cardEntityDef, premium, false))
    {
      UnityEngine.Debug.LogWarning((object) string.Format("CollectionDeckTray.AddCard({0},{1}): deck.AddCard failed!", (object) cardId1, (object) premium));
      return false;
    }
    if (editingDeck.GetTotalValidCardCount() == CollectionManager.Get().GetDeckSize())
      DeckHelper.Get().Hide(true);
    this.UpdateCardList(cardEntityDef, true, animateFromActor);
    CollectionManagerDisplay.Get().UpdateCurrentPageCardLocks(true);
    if (!Options.Get().GetBool(Option.HAS_ADDED_CARDS_TO_DECK, false) && editingDeck.GetTotalCardCount() >= 2 && (!DeckHelper.Get().IsActive() && editingDeck.GetTotalCardCount() < 15) && UserAttentionManager.CanShowAttentionGrabber("DeckTrayCardListContent.AddCard:" + (object) Option.HAS_ADDED_CARDS_TO_DECK))
    {
      NotificationManager.Get().CreateInnkeeperQuote(UserAttentionBlocker.NONE, GameStrings.Get("VO_INNKEEPER_CM_PAGEFLIP_28"), "VO_INNKEEPER_CM_PAGEFLIP_28", 0.0f, (Action) null, false);
      Options.Get().SetBool(Option.HAS_ADDED_CARDS_TO_DECK, true);
    }
    return true;
  }

  public int RemoveClosestInvalidCard(EntityDef entityDef, int sameRemoveCount)
  {
    CollectionDeck editingDeck = this.GetEditingDeck();
    int cost = entityDef.GetCost();
    int num1 = int.MaxValue;
    string cardID = string.Empty;
    TAG_PREMIUM premium = TAG_PREMIUM.NORMAL;
    using (List<CollectionDeckSlot>.Enumerator enumerator = editingDeck.GetSlots().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CollectionDeckSlot current = enumerator.Current;
        if (!editingDeck.IsValidSlot(current))
        {
          EntityDef entityDef1 = DefLoader.Get().GetEntityDef(current.CardID);
          if (entityDef1 == entityDef)
          {
            cardID = entityDef.GetCardId();
            premium = current.Premium;
            break;
          }
          int num2 = Mathf.Abs(cost - entityDef1.GetCost());
          if (num2 < num1)
          {
            num1 = num2;
            cardID = current.CardID;
          }
        }
      }
    }
    int num3 = 0;
    if (!string.IsNullOrEmpty(cardID))
    {
      for (int index = 0; index < sameRemoveCount; ++index)
      {
        if (editingDeck.RemoveCard(cardID, premium, false, false))
          ++num3;
      }
    }
    this.UpdateCardList(true, (Actor) null);
    return num3;
  }

  [ContextMenu("Update Card List")]
  public void UpdateCardList(bool updateHighlight = true, Actor animateFromActor = null)
  {
    this.UpdateCardList(string.Empty, updateHighlight, animateFromActor);
  }

  public void UpdateCardList(EntityDef justChangedCardEntityDef, bool updateHighlight = true, Actor animateFromActor = null)
  {
    this.UpdateCardList(justChangedCardEntityDef == null ? string.Empty : justChangedCardEntityDef.GetCardId(), updateHighlight, animateFromActor);
  }

  public void UpdateCardList(string justChangedCardID, bool updateHighlight = true, Actor animateFromActor = null)
  {
    CollectionDeck editingDeck = this.GetEditingDeck();
    if (editingDeck == null)
      return;
    using (List<DeckTrayDeckTileVisual>.Enumerator enumerator = this.m_cardTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.MarkAsUnused();
    }
    List<CollectionDeckSlot> slots = editingDeck.GetSlots();
    int num = 0;
    Vector3 cardTileOffset = this.GetCardTileOffset(editingDeck);
    for (int index = 0; index < slots.Count; ++index)
    {
      CollectionDeckSlot s = slots[index];
      if (s.Count == 0)
      {
        Log.Rachelle.Print(string.Format("CollectionDeckTray.UpdateCardList(): Slot {0} of deck is empty! Skipping...", (object) index));
      }
      else
      {
        num += s.Count;
        DeckTrayDeckTileVisual addCardTileVisual = this.GetOrAddCardTileVisual(index);
        addCardTileVisual.SetInArena(this.m_inArena);
        addCardTileVisual.gameObject.transform.localPosition = cardTileOffset + Vector3.down * (this.m_cardTileSlotLocalHeight * (float) index);
        addCardTileVisual.MarkAsUsed();
        addCardTileVisual.Show();
        addCardTileVisual.SetSlot(editingDeck, s, justChangedCardID.Equals(s.CardID));
      }
    }
    this.Hide(false);
    this.ShowDeckHelpButtonIfNeeded();
    this.FireCardCountChangedEvent();
    this.m_scrollbar.UpdateScroll();
    if (updateHighlight)
      this.UpdateDeckCompleteHighlight();
    if (!((UnityEngine.Object) animateFromActor != (UnityEngine.Object) null))
      return;
    this.StartCoroutine(this.ShowAddCardAnimationAfterTrayLoads(animateFromActor));
  }

  private Vector3 GetCardTileOffset(CollectionDeck currentDeck)
  {
    Vector3 vector3 = Vector3.zero;
    if (!this.m_isShowingFakeDeck && currentDeck != null && (UnityEngine.Object) this.m_deckTemplateHelpButton != (UnityEngine.Object) null)
    {
      bool flag = true;
      if (SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL)
        flag = TavernBrawlDisplay.IsTavernBrawlEditing();
      if (flag && currentDeck.GetTotalInvalidCardCount() > 0)
        vector3 = Vector3.down * this.m_deckTemplateHelpButton.GetComponent<UIBScrollableItem>().m_size.y * this.m_cardTileSlotLocalScaleVec3.y;
    }
    return vector3 + this.m_cardTileOffset;
  }

  public void TriggerCardCountUpdate()
  {
    this.FireCardCountChangedEvent();
  }

  public void HideDeckHelpPopup()
  {
    if (!((UnityEngine.Object) this.m_deckHelpPopup != (UnityEngine.Object) null))
      return;
    NotificationManager.Get().DestroyNotification(this.m_deckHelpPopup, 0.0f);
  }

  public DeckTrayDeckTileVisual GetFirstInvalidCard()
  {
    CollectionDeck editingDeck = this.GetEditingDeck();
    if (editingDeck != null)
    {
      using (List<CollectionDeckSlot>.Enumerator enumerator = editingDeck.GetSlots().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CollectionDeckSlot current = enumerator.Current;
          if (!editingDeck.IsValidSlot(current))
            return this.GetCardTileVisual(current.Index);
        }
      }
    }
    return (DeckTrayDeckTileVisual) null;
  }

  public void UpdateDeckCompleteHighlight()
  {
    CollectionDeck editingDeck = this.GetEditingDeck();
    bool flag1 = editingDeck != null && editingDeck.GetTotalValidCardCount() == CollectionManager.Get().GetDeckSize();
    bool flag2 = editingDeck != null && editingDeck.Locked;
    if ((UnityEngine.Object) this.m_LockedScrollBounds != (UnityEngine.Object) null && flag2)
    {
      this.m_scrollbar.m_ScrollBounds.center = this.m_LockedScrollBounds.center;
      this.m_scrollbar.m_ScrollBounds.size = this.m_LockedScrollBounds.size;
    }
    if ((UnityEngine.Object) this.m_deckCompleteHighlight != (UnityEngine.Object) null)
    {
      if (flag2)
        this.m_deckCompleteHighlight.SetActive(false);
      else
        this.m_deckCompleteHighlight.SetActive(flag1);
    }
    if (!flag1 || Options.Get().GetBool(Option.HAS_FINISHED_A_DECK, false))
      return;
    Options.Get().SetBool(Option.HAS_FINISHED_A_DECK, true);
  }

  [DebuggerHidden]
  private IEnumerator ShowAddCardAnimationAfterTrayLoads(Actor cardToAnimate)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTrayCardListContent.\u003CShowAddCardAnimationAfterTrayLoads\u003Ec__Iterator300() { cardToAnimate = cardToAnimate, \u003C\u0024\u003EcardToAnimate = cardToAnimate, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator FinishPhoneMovingCardTile(GameObject obj, Actor movingCardTile, float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTrayCardListContent.\u003CFinishPhoneMovingCardTile\u003Ec__Iterator301() { delay = delay, movingCardTile = movingCardTile, obj = obj, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003EmovingCardTile = movingCardTile, \u003C\u0024\u003Eobj = obj };
  }

  [DebuggerHidden]
  private IEnumerator ShowDeckCompleteEffectsWithInterval(float interval)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DeckTrayCardListContent.\u003CShowDeckCompleteEffectsWithInterval\u003Ec__Iterator302() { interval = interval, \u003C\u0024\u003Einterval = interval, \u003C\u003Ef__this = this };
  }

  private void IsCardTileVisible(GameObject obj, bool visible)
  {
    if (obj.activeSelf == visible)
      return;
    obj.SetActive(visible && obj.GetComponent<DeckTrayDeckTileVisual>().IsInUse());
  }

  private void ShowDeckEditingTipsIfNeeded()
  {
    if (Options.Get().GetBool(Option.HAS_REMOVED_CARD_FROM_DECK, false) || CollectionManagerDisplay.Get().GetViewMode() != CollectionManagerDisplay.ViewMode.CARDS || (this.m_cardTiles.Count <= 0 || SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL))
      return;
    Transform cardTutorialBone = CollectionDeckTray.Get().m_removeCardTutorialBone;
    if (!((UnityEngine.Object) this.m_deckHelpPopup == (UnityEngine.Object) null))
      return;
    this.m_deckHelpPopup = NotificationManager.Get().CreatePopupText(UserAttentionBlocker.NONE, cardTutorialBone.position, cardTutorialBone.localScale, GameStrings.Get("GLUE_COLLECTION_TUTORIAL08"), true);
    if (!((UnityEngine.Object) this.m_deckHelpPopup != (UnityEngine.Object) null))
      return;
    this.m_deckHelpPopup.PulseReminderEveryXSeconds(3f);
  }

  private void ShowDeckHelpButtonIfNeeded()
  {
    bool flag1 = false;
    if ((UnityEngine.Object) CollectionManagerDisplay.Get() == (UnityEngine.Object) null)
      return;
    CollectionDeck editingDeck = this.GetEditingDeck();
    if (editingDeck != null && (UnityEngine.Object) DeckHelper.Get() != (UnityEngine.Object) null && editingDeck.GetTotalValidCardCount() < CollectionManager.Get().GetDeckSize())
      flag1 = true;
    bool flag2;
    if (editingDeck.GetTotalInvalidCardCount() > 0)
    {
      flag1 = false;
      flag2 = true;
    }
    else
      flag2 = false;
    if (CollectionManagerDisplay.Get().GetViewMode() == CollectionManagerDisplay.ViewMode.DECK_TEMPLATE)
    {
      flag1 = false;
      flag2 = false;
    }
    if (TavernBrawlDisplay.IsTavernBrawlViewing())
    {
      flag1 = false;
      flag2 = false;
    }
    if (flag1)
    {
      Vector3 cardTileOffset = this.GetCardTileOffset(editingDeck);
      cardTileOffset.y -= this.m_cardTileSlotLocalHeight * (float) editingDeck.GetSlots().Count;
      this.m_deckHelpButton.transform.localPosition = cardTileOffset;
    }
    if ((UnityEngine.Object) this.m_deckHelpButton != (UnityEngine.Object) null)
      this.m_deckHelpButton.gameObject.SetActive(flag1);
    if ((UnityEngine.Object) this.m_deckTemplateHelpButton != (UnityEngine.Object) null)
      this.m_deckTemplateHelpButton.gameObject.SetActive(flag2);
    if (Options.Get().GetBool(Option.HAS_FINISHED_A_DECK, false))
      return;
    HighlightState componentInChildren1 = this.m_deckHelpButton.GetComponentInChildren<HighlightState>();
    if ((UnityEngine.Object) componentInChildren1 != (UnityEngine.Object) null)
      componentInChildren1.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    if (!((UnityEngine.Object) this.m_deckTemplateHelpButton != (UnityEngine.Object) null))
      return;
    HighlightState componentInChildren2 = this.m_deckTemplateHelpButton.GetComponentInChildren<HighlightState>();
    if (!((UnityEngine.Object) componentInChildren2 != (UnityEngine.Object) null))
      return;
    componentInChildren2.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
  }

  private void OnDeckHelpButtonPress(UIEvent e)
  {
    DeckTrayDeckTileVisual firstInvalidCard = this.GetFirstInvalidCard();
    this.ShowDeckHelper(firstInvalidCard, true, (UnityEngine.Object) firstInvalidCard != (UnityEngine.Object) null);
  }

  private void OnDeckTemplateHelpButtonPress(UIEvent e)
  {
    Options.Get().SetBool(Option.HAS_CLICKED_DECK_TEMPLATE_REPLACE, true);
    this.OnDeckHelpButtonPress(e);
  }

  private void OnDeckHelpButtonOver(UIEvent e)
  {
    HighlightState componentInChildren = this.m_deckHelpButton.GetComponentInChildren<HighlightState>();
    if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
    {
      if (!Options.Get().GetBool(Option.HAS_FINISHED_A_DECK, false))
        componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
      else
        componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
    }
    SoundManager.Get().LoadAndPlay("Small_Mouseover", this.gameObject);
  }

  private void OnDeckHelpButtonOut(UIEvent e)
  {
    HighlightState componentInChildren = this.m_deckHelpButton.GetComponentInChildren<HighlightState>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    if (!Options.Get().GetBool(Option.HAS_FINISHED_A_DECK, false))
      componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    else
      componentInChildren.ChangeState(ActorStateType.NONE);
  }

  private void OnDeckTemplateHelpButtonOver(UIEvent e)
  {
    HighlightState componentInChildren = this.m_deckTemplateHelpButton.GetComponentInChildren<HighlightState>();
    if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
    {
      if (!Options.Get().GetBool(Option.HAS_FINISHED_A_DECK, false))
        componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
      else
        componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_MOUSE_OVER);
    }
    SoundManager.Get().LoadAndPlay("Small_Mouseover", this.gameObject);
  }

  private void OnDeckTemplateHelpButtonOut(UIEvent e)
  {
    HighlightState componentInChildren = this.m_deckTemplateHelpButton.GetComponentInChildren<HighlightState>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    if (!Options.Get().GetBool(Option.HAS_CLICKED_DECK_TEMPLATE_REPLACE, false))
      componentInChildren.ChangeState(ActorStateType.HIGHLIGHT_PRIMARY_ACTIVE);
    else
      componentInChildren.ChangeState(ActorStateType.NONE);
  }

  private void LoadCardPrefabs(List<CollectionDeckSlot> deckSlots)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DeckTrayCardListContent.\u003CLoadCardPrefabs\u003Ec__AnonStorey430 prefabsCAnonStorey430 = new DeckTrayCardListContent.\u003CLoadCardPrefabs\u003Ec__AnonStorey430();
    // ISSUE: reference to a compiler-generated field
    prefabsCAnonStorey430.\u003C\u003Ef__this = this;
    if (deckSlots.Count == 0)
      return;
    // ISSUE: reference to a compiler-generated field
    prefabsCAnonStorey430.prefabsToLoad = deckSlots.Count;
    this.m_loading = true;
    for (int index = 0; index < deckSlots.Count; ++index)
    {
      CollectionDeckSlot deckSlot = deckSlots[index];
      if (deckSlot.Count == 0)
      {
        Log.Rachelle.Print(string.Format("CollectionDeckTray.LoadCardPrefabs(): Slot {0} of deck is empty! Skipping...", (object) index));
      }
      else
      {
        // ISSUE: reference to a compiler-generated method
        DefLoader.Get().LoadCardDef(deckSlot.CardID, new DefLoader.LoadDefCallback<CardDef>(prefabsCAnonStorey430.\u003C\u003Em__2BD), (object) null, new CardPortraitQuality(1, false));
      }
    }
  }

  private Vector3 GetOffscreenLocalPosition()
  {
    Vector3 originalLocalPosition = this.m_originalLocalPosition;
    CollectionDeck editingDeck = this.GetEditingDeck();
    int num = editingDeck == null ? 0 : editingDeck.GetSlotCount() + 2;
    originalLocalPosition.z -= (float) ((double) this.m_cardTileHeight * (double) num - (double) this.GetCardTileOffset(editingDeck).y / (double) this.m_cardTileSlotLocalScaleVec3.y);
    return originalLocalPosition;
  }

  public void RegisterCardTileHeldListener(DeckTrayCardListContent.CardTileHeld dlg)
  {
    this.m_cardTileHeldListeners.Add(dlg);
  }

  public void RegisterCardTilePressListener(DeckTrayCardListContent.CardTilePress dlg)
  {
    this.m_cardTilePressListeners.Add(dlg);
  }

  public void RegisterCardTileTapListener(DeckTrayCardListContent.CardTileTap dlg)
  {
    this.m_cardTileTapListeners.Add(dlg);
  }

  public void RegisterCardTileOverListener(DeckTrayCardListContent.CardTileOver dlg)
  {
    this.m_cardTileOverListeners.Add(dlg);
  }

  public void RegisterCardTileOutListener(DeckTrayCardListContent.CardTileOut dlg)
  {
    this.m_cardTileOutListeners.Add(dlg);
  }

  public void RegisterCardTileReleaseListener(DeckTrayCardListContent.CardTileRelease dlg)
  {
    this.m_cardTileReleaseListeners.Add(dlg);
  }

  public void RegisterCardTileRightClickedListener(DeckTrayCardListContent.CardTileRightClicked dlg)
  {
    this.m_cardTileRightClickedListeners.Add(dlg);
  }

  public void RegisterCardCountUpdated(DeckTrayCardListContent.CardCountChanged dlg)
  {
    this.m_cardCountChangedListeners.Add(dlg);
  }

  public void UnregisterCardTileHeldListener(DeckTrayCardListContent.CardTileHeld dlg)
  {
    this.m_cardTileHeldListeners.Remove(dlg);
  }

  public void UnregisterCardTileTapListener(DeckTrayCardListContent.CardTileTap dlg)
  {
    this.m_cardTileTapListeners.Remove(dlg);
  }

  public void UnregisterCardTilePressListener(DeckTrayCardListContent.CardTilePress dlg)
  {
    this.m_cardTilePressListeners.Remove(dlg);
  }

  public void UnregisterCardTileOverListener(DeckTrayCardListContent.CardTileOver dlg)
  {
    this.m_cardTileOverListeners.Remove(dlg);
  }

  public void UnregisterCardTileOutListener(DeckTrayCardListContent.CardTileOut dlg)
  {
    this.m_cardTileOutListeners.Remove(dlg);
  }

  public void UnregisterCardTileReleaseListener(DeckTrayCardListContent.CardTileRelease dlg)
  {
    this.m_cardTileReleaseListeners.Remove(dlg);
  }

  public void UnregisterCardTileRightClickedListener(DeckTrayCardListContent.CardTileRightClicked dlg)
  {
    this.m_cardTileRightClickedListeners.Remove(dlg);
  }

  public void UnregisterCardCountUpdated(DeckTrayCardListContent.CardCountChanged dlg)
  {
    this.m_cardCountChangedListeners.Remove(dlg);
  }

  private void FireCardTileHeldEvent(DeckTrayDeckTileVisual cardTile)
  {
    foreach (DeckTrayCardListContent.CardTileHeld cardTileHeld in this.m_cardTileHeldListeners.ToArray())
      cardTileHeld(cardTile);
  }

  private void FireCardTilePressEvent(DeckTrayDeckTileVisual cardTile)
  {
    foreach (DeckTrayCardListContent.CardTilePress cardTilePress in this.m_cardTilePressListeners.ToArray())
      cardTilePress(cardTile);
  }

  private void FireCardTileTapEvent(DeckTrayDeckTileVisual cardTile)
  {
    foreach (DeckTrayCardListContent.CardTileTap cardTileTap in this.m_cardTileTapListeners.ToArray())
      cardTileTap(cardTile);
  }

  private void FireCardTileOverEvent(DeckTrayDeckTileVisual cardTile)
  {
    foreach (DeckTrayCardListContent.CardTileOver cardTileOver in this.m_cardTileOverListeners.ToArray())
      cardTileOver(cardTile);
  }

  private void FireCardTileOutEvent(DeckTrayDeckTileVisual cardTile)
  {
    foreach (DeckTrayCardListContent.CardTileOut cardTileOut in this.m_cardTileOutListeners.ToArray())
      cardTileOut(cardTile);
  }

  private void FireCardTileReleaseEvent(DeckTrayDeckTileVisual cardTile)
  {
    foreach (DeckTrayCardListContent.CardTileRelease cardTileRelease in this.m_cardTileReleaseListeners.ToArray())
      cardTileRelease(cardTile);
  }

  private void FireCardTileRightClickedEvent(DeckTrayDeckTileVisual cardTile)
  {
    foreach (DeckTrayCardListContent.CardTileRightClicked tileRightClicked in this.m_cardTileRightClickedListeners.ToArray())
      tileRightClicked(cardTile);
  }

  private void FireCardCountChangedEvent()
  {
    DeckTrayCardListContent.CardCountChanged[] array = this.m_cardCountChangedListeners.ToArray();
    CollectionDeck editingDeck = this.GetEditingDeck();
    int cardCount = 0;
    if (editingDeck != null)
      cardCount = !editingDeck.ShouldSplitSlotsByValidity() ? editingDeck.GetTotalCardCount() : editingDeck.GetTotalValidCardCount();
    foreach (DeckTrayCardListContent.CardCountChanged cardCountChanged in array)
      cardCountChanged(cardCount);
  }

  public delegate void CardTileHeld(DeckTrayDeckTileVisual cardTile);

  public delegate void CardTilePress(DeckTrayDeckTileVisual cardTile);

  public delegate void CardTileTap(DeckTrayDeckTileVisual cardTile);

  public delegate void CardTileOver(DeckTrayDeckTileVisual cardTile);

  public delegate void CardTileOut(DeckTrayDeckTileVisual cardTile);

  public delegate void CardTileRelease(DeckTrayDeckTileVisual cardTile);

  public delegate void CardTileRightClicked(DeckTrayDeckTileVisual cardTile);

  public delegate void CardCountChanged(int cardCount);
}
