﻿// Decompiled with JetBrains decompiler
// Type: CardDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[CustomEditClass]
public class CardDef : MonoBehaviour
{
  [CustomEditField(Sections = "Hero")]
  public bool m_SocketInParentEffectToHero = true;
  private CardPortraitQuality m_portraitQuality = CardPortraitQuality.GetUnloaded();
  protected const int LARGE_MINION_COST = 7;
  protected const int MEDIUM_MINION_COST = 4;
  [CustomEditField(Sections = "Portrait", T = EditType.CARD_TEXTURE)]
  public string m_PortraitTexturePath;
  [CustomEditField(Sections = "Portrait", T = EditType.MATERIAL)]
  public string m_PremiumPortraitMaterialPath;
  [CustomEditField(Sections = "Portrait", T = EditType.CARD_TEXTURE)]
  public string m_PremiumPortraitTexturePath;
  [CustomEditField(Sections = "Portrait")]
  public Material m_DeckCardBarPortrait;
  [CustomEditField(Sections = "Portrait")]
  public Material m_EnchantmentPortrait;
  [CustomEditField(Sections = "Portrait")]
  public Material m_HistoryTileHalfPortrait;
  [CustomEditField(Sections = "Portrait")]
  public Material m_HistoryTileFullPortrait;
  [CustomEditField(Sections = "Portrait")]
  public Material_MobileOverride m_CustomDeckPortrait;
  [CustomEditField(Sections = "Portrait")]
  public Material_MobileOverride m_DeckPickerPortrait;
  [CustomEditField(Sections = "Portrait")]
  public Material m_PracticeAIPortrait;
  [CustomEditField(Sections = "Portrait")]
  public Material m_DeckBoxPortrait;
  [CustomEditField(Sections = "Portrait")]
  public bool m_AlwaysRenderPremiumPortrait;
  [CustomEditField(Sections = "Play")]
  public CardEffectDef m_PlayEffectDef;
  [CustomEditField(Sections = "Attack")]
  public CardEffectDef m_AttackEffectDef;
  [CustomEditField(Sections = "Death")]
  public CardEffectDef m_DeathEffectDef;
  [CustomEditField(Sections = "Lifetime")]
  public CardEffectDef m_LifetimeEffectDef;
  [CustomEditField(Sections = "Trigger")]
  public List<CardEffectDef> m_TriggerEffectDefs;
  [CustomEditField(Sections = "SubOption")]
  public List<CardEffectDef> m_SubOptionEffectDefs;
  [CustomEditField(Sections = "Custom", T = EditType.SPELL)]
  public string m_CustomSummonSpellPath;
  [CustomEditField(Sections = "Custom", T = EditType.SPELL)]
  public string m_GoldenCustomSummonSpellPath;
  [CustomEditField(Sections = "Custom", T = EditType.SPELL)]
  public string m_CustomSpawnSpellPath;
  [CustomEditField(Sections = "Custom", T = EditType.SPELL)]
  public string m_GoldenCustomSpawnSpellPath;
  [CustomEditField(Sections = "Custom", T = EditType.SPELL)]
  public string m_CustomDeathSpellPath;
  [CustomEditField(Sections = "Custom", T = EditType.SPELL)]
  public string m_GoldenCustomDeathSpellPath;
  [CustomEditField(Sections = "Custom", T = EditType.SPELL)]
  public string m_CustomKeywordSpellPath;
  [CustomEditField(Sections = "Hero", T = EditType.GAME_OBJECT)]
  public string m_CollectionHeroDefPath;
  [CustomEditField(Sections = "Hero", T = EditType.SPELL)]
  public string m_CustomHeroArmorSpell;
  [CustomEditField(Sections = "Hero", T = EditType.SPELL)]
  public string m_SocketInEffectFriendly;
  [CustomEditField(Sections = "Hero", T = EditType.SPELL)]
  public string m_SocketInEffectOpponent;
  [CustomEditField(Sections = "Hero", T = EditType.SPELL)]
  public string m_SocketInEffectFriendlyPhone;
  [CustomEditField(Sections = "Hero", T = EditType.SPELL)]
  public string m_SocketInEffectOpponentPhone;
  [CustomEditField(Sections = "Hero")]
  public bool m_SocketInOverrideHeroAnimation;
  [CustomEditField(Sections = "Hero", T = EditType.TEXTURE)]
  public string m_CustomHeroTray;
  [CustomEditField(Sections = "Hero")]
  public List<Board.CustomTraySettings> m_CustomHeroTraySettings;
  [CustomEditField(Sections = "Hero", T = EditType.TEXTURE)]
  public string m_CustomHeroPhoneTray;
  [CustomEditField(Sections = "Hero", T = EditType.TEXTURE)]
  public string m_CustomHeroPhoneManaGem;
  [CustomEditField(Sections = "Hero", T = EditType.SOUND_PREFAB)]
  public string m_AnnouncerLinePath;
  [CustomEditField(Sections = "Hero", T = EditType.SOUND_PREFAB)]
  public string m_AnnouncerLineBeforeVersusPath;
  [CustomEditField(Sections = "Hero", T = EditType.SOUND_PREFAB)]
  public string m_AnnouncerLineAfterVersusPath;
  [CustomEditField(Sections = "Hero")]
  public List<EmoteEntryDef> m_EmoteDefs;
  [CustomEditField(Sections = "Misc")]
  public bool m_SuppressDeathrattleDeath;
  [CustomEditField(Sections = "Misc")]
  public bool m_SuppressPlaySoundsOnSummon;
  [CustomEditField(Sections = "Misc")]
  public CardTextBuilderType m_CardTextBuilderType;
  private Material m_LoadedPremiumPortraitMaterial;
  private Material m_LoadedDeckCardBarPortrait;
  private Material m_LoadedEnchantmentPortrait;
  private Material m_LoadedHistoryTileFullPortrait;
  private Material m_LoadedHistoryTileHalfPortrait;
  private Material m_LoadedCustomDeckPortrait;
  private Material m_LoadedDeckPickerPortrait;
  private Material m_LoadedPracticeAIPortrait;
  private Material m_LoadedDeckBoxPortrait;
  private Texture m_LoadedPortraitTexture;
  private Texture m_LoadedPremiumPortraitTexture;
  private CardTextBuilder m_cardTextBuilder;

  public void Awake()
  {
    if (string.IsNullOrEmpty(this.m_PortraitTexturePath))
    {
      this.m_portraitQuality.TextureQuality = 3;
      this.m_portraitQuality.LoadPremium = true;
    }
    else
    {
      if (!string.IsNullOrEmpty(this.m_PremiumPortraitMaterialPath))
        return;
      this.m_portraitQuality.LoadPremium = true;
    }
  }

  public virtual string DetermineActorNameForZone(Entity entity, TAG_ZONE zoneTag)
  {
    return ActorNames.GetZoneActor(entity, zoneTag);
  }

  public virtual SpellType DetermineSummonInSpell_HandToPlay(Card card)
  {
    Entity entity = card.GetEntity();
    int cost = entity.GetEntityDef().GetCost();
    TAG_PREMIUM premiumType = entity.GetPremiumType();
    bool flag = entity.GetController().IsFriendlySide();
    if (cost >= 7)
    {
      switch (premiumType)
      {
        case TAG_PREMIUM.NORMAL:
          return flag ? SpellType.SUMMON_IN_LARGE : SpellType.SUMMON_IN_OPPONENT_LARGE;
        case TAG_PREMIUM.GOLDEN:
          return flag ? SpellType.SUMMON_IN_LARGE_PREMIUM : SpellType.SUMMON_IN_OPPONENT_LARGE_PREMIUM;
        default:
          Debug.LogWarning((object) string.Format("CardDef.DetermineSummonInSpell_HandToPlay() - unexpected premium type {0}", (object) premiumType));
          goto case TAG_PREMIUM.NORMAL;
      }
    }
    else if (cost >= 4)
    {
      switch (premiumType)
      {
        case TAG_PREMIUM.NORMAL:
          return flag ? SpellType.SUMMON_IN_MEDIUM : SpellType.SUMMON_IN_OPPONENT_MEDIUM;
        case TAG_PREMIUM.GOLDEN:
          return flag ? SpellType.SUMMON_IN_MEDIUM_PREMIUM : SpellType.SUMMON_IN_OPPONENT_MEDIUM_PREMIUM;
        default:
          Debug.LogWarning((object) string.Format("CardDef.DetermineSummonInSpell_HandToPlay() - unexpected premium type {0}", (object) premiumType));
          goto case TAG_PREMIUM.NORMAL;
      }
    }
    else
    {
      switch (premiumType)
      {
        case TAG_PREMIUM.NORMAL:
          return flag ? SpellType.SUMMON_IN : SpellType.SUMMON_IN_OPPONENT;
        case TAG_PREMIUM.GOLDEN:
          return flag ? SpellType.SUMMON_IN_PREMIUM : SpellType.SUMMON_IN_OPPONENT_PREMIUM;
        default:
          Debug.LogWarning((object) string.Format("CardDef.DetermineSummonInSpell_HandToPlay() - unexpected premium type {0}", (object) premiumType));
          goto case TAG_PREMIUM.NORMAL;
      }
    }
  }

  public virtual SpellType DetermineSummonOutSpell_HandToPlay(Card card)
  {
    Entity entity = card.GetEntity();
    if (!entity.GetController().IsFriendlySide())
      return SpellType.SUMMON_OUT;
    int cost = entity.GetEntityDef().GetCost();
    TAG_PREMIUM premiumType = entity.GetPremiumType();
    if (cost >= 7)
    {
      switch (premiumType)
      {
        case TAG_PREMIUM.NORMAL:
          return SpellType.SUMMON_OUT_LARGE;
        case TAG_PREMIUM.GOLDEN:
          return SpellType.SUMMON_OUT_PREMIUM;
        default:
          Debug.LogWarning((object) string.Format("CardDef.DetermineSummonOutSpell_HandToPlay(): unexpected premium type {0}", (object) premiumType));
          goto case TAG_PREMIUM.NORMAL;
      }
    }
    else if (cost >= 4)
    {
      switch (premiumType)
      {
        case TAG_PREMIUM.NORMAL:
          return SpellType.SUMMON_OUT_MEDIUM;
        case TAG_PREMIUM.GOLDEN:
          return SpellType.SUMMON_OUT_PREMIUM;
        default:
          Debug.LogWarning((object) string.Format("CardDef.DetermineSummonOutSpell_HandToPlay(): unexpected premium type {0}", (object) premiumType));
          goto case TAG_PREMIUM.NORMAL;
      }
    }
    else
    {
      switch (premiumType)
      {
        case TAG_PREMIUM.NORMAL:
          return SpellType.SUMMON_OUT;
        case TAG_PREMIUM.GOLDEN:
          return SpellType.SUMMON_OUT_PREMIUM;
        default:
          Debug.LogWarning((object) string.Format("CardDef.DetermineSummonOutSpell_HandToPlay(): unexpected premium type {0}", (object) premiumType));
          goto case TAG_PREMIUM.NORMAL;
      }
    }
  }

  private static void SetTextureIfNotNull(Material baseMat, ref Material targetMat, Texture tex)
  {
    if ((Object) baseMat == (Object) null)
      return;
    if ((Object) targetMat == (Object) null)
      targetMat = Object.Instantiate<Material>(baseMat);
    targetMat.mainTexture = tex;
  }

  public void OnPortraitLoaded(Texture portrait, int quality)
  {
    if (quality <= this.m_portraitQuality.TextureQuality)
    {
      Debug.LogWarning((object) string.Format("Loaded texture of quality lower or equal to what was was already available ({0} <= {1}), texture={2}", (object) quality, (object) this.m_portraitQuality, (object) portrait));
    }
    else
    {
      this.m_portraitQuality.TextureQuality = quality;
      this.m_LoadedPortraitTexture = portrait;
      if ((Object) this.m_LoadedPremiumPortraitMaterial != (Object) null && string.IsNullOrEmpty(this.m_PremiumPortraitTexturePath))
      {
        this.m_LoadedPremiumPortraitMaterial.mainTexture = portrait;
        this.m_portraitQuality.LoadPremium = true;
      }
      CardDef.SetTextureIfNotNull(this.m_DeckCardBarPortrait, ref this.m_LoadedDeckCardBarPortrait, portrait);
      CardDef.SetTextureIfNotNull(this.m_EnchantmentPortrait, ref this.m_LoadedEnchantmentPortrait, portrait);
      CardDef.SetTextureIfNotNull(this.m_HistoryTileFullPortrait, ref this.m_LoadedHistoryTileFullPortrait, portrait);
      CardDef.SetTextureIfNotNull(this.m_HistoryTileHalfPortrait, ref this.m_LoadedHistoryTileHalfPortrait, portrait);
      CardDef.SetTextureIfNotNull((Material) ((MobileOverrideValue<Material>) this.m_CustomDeckPortrait), ref this.m_LoadedCustomDeckPortrait, portrait);
      CardDef.SetTextureIfNotNull((Material) ((MobileOverrideValue<Material>) this.m_DeckPickerPortrait), ref this.m_LoadedDeckPickerPortrait, portrait);
      CardDef.SetTextureIfNotNull(this.m_PracticeAIPortrait, ref this.m_LoadedPracticeAIPortrait, portrait);
      CardDef.SetTextureIfNotNull(this.m_DeckBoxPortrait, ref this.m_LoadedDeckBoxPortrait, portrait);
    }
  }

  public void OnPremiumMaterialLoaded(Material material, Texture portrait)
  {
    if ((Object) this.m_LoadedPremiumPortraitMaterial != (Object) null)
    {
      Debug.LogWarning((object) string.Format("Loaded premium material twice: {0}", (object) material));
    }
    else
    {
      if ((Object) material != (Object) null)
        this.m_LoadedPremiumPortraitMaterial = Object.Instantiate<Material>(material);
      this.m_LoadedPremiumPortraitTexture = portrait;
      if (string.IsNullOrEmpty(this.m_PremiumPortraitTexturePath))
      {
        if (!((Object) this.m_LoadedPortraitTexture != (Object) null))
          return;
        if ((Object) this.m_LoadedPremiumPortraitMaterial != (Object) null)
          this.m_LoadedPremiumPortraitMaterial.mainTexture = this.m_LoadedPortraitTexture;
        this.m_portraitQuality.LoadPremium = true;
      }
      else
      {
        if (!((Object) this.m_LoadedPremiumPortraitTexture != (Object) null))
          return;
        if ((Object) this.m_LoadedPremiumPortraitMaterial != (Object) null)
          this.m_LoadedPremiumPortraitMaterial.mainTexture = this.m_LoadedPremiumPortraitTexture;
        this.m_portraitQuality.LoadPremium = true;
      }
    }
  }

  public CardPortraitQuality GetPortraitQuality()
  {
    return this.m_portraitQuality;
  }

  public Texture GetPortraitTexture()
  {
    return this.m_LoadedPortraitTexture;
  }

  public bool IsPremiumLoaded()
  {
    return this.m_portraitQuality.LoadPremium;
  }

  public CardTextBuilder GetCardTextBuilder()
  {
    if (this.m_cardTextBuilder == null)
      this.m_cardTextBuilder = CardTextBuilderFactory.Create(this.m_CardTextBuilderType);
    return this.m_cardTextBuilder;
  }

  public Material GetPremiumPortraitMaterial()
  {
    return this.m_LoadedPremiumPortraitMaterial;
  }

  public Material GetDeckCardBarPortrait()
  {
    return this.m_LoadedDeckCardBarPortrait;
  }

  public Material GetEnchantmentPortrait()
  {
    return this.m_LoadedEnchantmentPortrait;
  }

  public Material GetHistoryTileFullPortrait()
  {
    return this.m_LoadedHistoryTileFullPortrait;
  }

  public Material GetHistoryTileHalfPortrait()
  {
    return this.m_LoadedHistoryTileHalfPortrait;
  }

  public Material GetCustomDeckPortrait()
  {
    return this.m_LoadedCustomDeckPortrait;
  }

  public Material GetDeckPickerPortrait()
  {
    return this.m_LoadedDeckPickerPortrait;
  }

  public Material GetPracticeAIPortrait()
  {
    return this.m_LoadedPracticeAIPortrait;
  }

  public Material GetDeckBoxPortrait()
  {
    return this.m_LoadedDeckBoxPortrait;
  }
}
