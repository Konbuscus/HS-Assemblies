﻿// Decompiled with JetBrains decompiler
// Type: PracticeDisplay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class PracticeDisplay : MonoBehaviour
{
  public GameObject m_deckPickerTrayContainer;
  public GameObject m_practicePickerTrayContainer;
  public GameObject_MobileOverride m_practicePickerTrayPrefab;
  public Vector3_MobileOverride m_practicePickerTrayHideOffset;
  private static PracticeDisplay s_instance;
  private PracticePickerTrayDisplay m_practicePickerTray;
  private Vector3 m_practicePickerTrayShowPos;
  private DeckPickerTrayDisplay m_deckPickerTray;

  private void Awake()
  {
    PracticeDisplay.s_instance = this;
    this.m_practicePickerTray = ((GameObject) GameUtils.Instantiate((GameObject) ((MobileOverrideValue<GameObject>) this.m_practicePickerTrayPrefab), this.m_practicePickerTrayContainer, false)).GetComponent<PracticePickerTrayDisplay>();
    if ((bool) UniversalInputManager.UsePhoneUI)
      SceneUtils.SetLayer((Component) this.m_practicePickerTray, GameLayer.IgnoreFullScreenEffects);
    AssetLoader.Get().LoadActor(!(bool) UniversalInputManager.UsePhoneUI ? "DeckPickerTray" : "DeckPickerTray_phone", (AssetLoader.GameObjectCallback) ((name, go, data) =>
    {
      if ((Object) go == (Object) null)
      {
        UnityEngine.Debug.LogError((object) "Unable to load DeckPickerTray.");
      }
      else
      {
        this.m_deckPickerTray = go.GetComponent<DeckPickerTrayDisplay>();
        if ((Object) this.m_deckPickerTray == (Object) null)
        {
          UnityEngine.Debug.LogError((object) "DeckPickerTrayDisplay component not found in DeckPickerTray object.");
        }
        else
        {
          if ((Object) this.m_deckPickerTrayContainer != (Object) null)
            GameUtils.SetParent((Component) this.m_deckPickerTray, this.m_deckPickerTrayContainer, false);
          AdventureSubScene component = this.GetComponent<AdventureSubScene>();
          if ((Object) component != (Object) null)
          {
            this.m_practicePickerTray.AddTrayLoadedListener((PracticePickerTrayDisplay.TrayLoaded) (() =>
            {
              this.OnTrayPartLoaded();
              this.m_practicePickerTray.gameObject.SetActive(false);
            }));
            this.m_deckPickerTray.AddDeckTrayLoadedListener(new DeckPickerTrayDisplay.DeckTrayLoaded(this.OnTrayPartLoaded));
            if (this.m_practicePickerTray.IsLoaded() && this.m_deckPickerTray.IsLoaded())
              component.SetIsLoaded(true);
          }
          this.InitializeTrays();
          CheatMgr.Get().RegisterCheatHandler("replaymissions", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_replaymissions), (string) null, (string) null, (string) null);
          CheatMgr.Get().RegisterCheatHandler("replaymission", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_replaymissions), (string) null, (string) null, (string) null);
          NetCache.Get().RegisterScreenPractice(new NetCache.NetCacheCallback(this.OnNetCacheReady));
        }
      }
    }), (object) null, false);
  }

  private void OnTrayPartLoaded()
  {
    AdventureSubScene component = this.GetComponent<AdventureSubScene>();
    if (!((Object) component != (Object) null))
      return;
    component.SetIsLoaded(this.IsLoaded());
  }

  private void OnDestroy()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    if ((Object) CheatMgr.Get() != (Object) null)
    {
      CheatMgr.Get().UnregisterCheatHandler("replaymissions", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_replaymissions));
      CheatMgr.Get().UnregisterCheatHandler("replaymission", new CheatMgr.ProcessCheatCallback(this.OnProcessCheat_replaymissions));
    }
    PracticeDisplay.s_instance = (PracticeDisplay) null;
  }

  public static PracticeDisplay Get()
  {
    return PracticeDisplay.s_instance;
  }

  public bool IsLoaded()
  {
    if (this.m_practicePickerTray.IsLoaded())
      return this.m_deckPickerTray.IsLoaded();
    return false;
  }

  private bool OnProcessCheat_replaymissions(string func, string[] args, string rawArgs)
  {
    AssetLoader.Get().LoadGameObject("ReplayTutorialDebug", true, false);
    return true;
  }

  public Vector3 GetPracticePickerShowPosition()
  {
    return this.m_practicePickerTrayShowPos;
  }

  public Vector3 GetPracticePickerHidePosition()
  {
    return this.m_practicePickerTrayShowPos + (Vector3) ((MobileOverrideValue<Vector3>) this.m_practicePickerTrayHideOffset);
  }

  private void OnNetCacheReady()
  {
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    if (!NetCache.Get().GetNetObject<NetCache.NetCacheFeatures>().Games.Practice)
    {
      if (SceneMgr.Get().IsModeRequested(SceneMgr.Mode.HUB))
        return;
      SceneMgr.Get().SetNextMode(SceneMgr.Mode.HUB);
      Error.AddWarningLoc("GLOBAL_FEATURE_DISABLED_TITLE", "GLOBAL_FEATURE_DISABLED_MESSAGE_PRACTICE");
    }
    else
      this.StartCoroutine(this.ShowQuestPopups());
  }

  [DebuggerHidden]
  private IEnumerator ShowQuestPopups()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PracticeDisplay.\u003CShowQuestPopups\u003Ec__Iterator232() { \u003C\u003Ef__this = this };
  }

  private void InitializeTrays()
  {
    this.m_deckPickerTray.SetHeaderText((string) GameUtils.GetAdventureDataRecord((int) AdventureConfig.Get().GetSelectedAdventure(), (int) AdventureConfig.Get().GetSelectedMode()).Name);
    this.m_deckPickerTray.Init();
    this.m_practicePickerTray.Init();
    this.m_practicePickerTrayShowPos = this.m_practicePickerTray.transform.localPosition;
    this.m_practicePickerTray.transform.localPosition = this.GetPracticePickerHidePosition();
  }
}
