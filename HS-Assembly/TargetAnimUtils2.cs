﻿// Decompiled with JetBrains decompiler
// Type: TargetAnimUtils2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TargetAnimUtils2 : MonoBehaviour
{
  public GameObject m_Target;

  public void PrintLog2(string message)
  {
    Debug.Log((object) message);
  }

  public void PrintLogWarning2(string message)
  {
    Debug.LogWarning((object) message);
  }

  public void PrintLogError2(string message)
  {
    Debug.LogError((object) message);
  }

  public void PlayNewParticles2()
  {
    this.m_Target.GetComponent<ParticleSystem>().Play();
  }

  public void StopNewParticles2()
  {
    this.m_Target.GetComponent<ParticleSystem>().Stop();
  }

  public void PlayParticles2()
  {
    if (!((Object) this.m_Target.GetComponent<ParticleEmitter>() != (Object) null))
      return;
    this.m_Target.GetComponent<ParticleEmitter>().emit = true;
  }

  public void StopParticles2()
  {
    if (!((Object) this.m_Target.GetComponent<ParticleEmitter>() != (Object) null))
      return;
    this.m_Target.GetComponent<ParticleEmitter>().emit = false;
  }

  public void PlayParticlesInChildren2()
  {
    foreach (ParticleEmitter componentsInChild in this.m_Target.GetComponentsInChildren<ParticleEmitter>())
      componentsInChild.emit = true;
  }

  public void StopParticlesInChildren2()
  {
    foreach (ParticleEmitter componentsInChild in this.m_Target.GetComponentsInChildren<ParticleEmitter>())
      componentsInChild.emit = false;
  }

  public void KillParticlesInChildren2()
  {
    Particle[] particleArray = new Particle[0];
    foreach (ParticleEmitter componentsInChild in this.m_Target.GetComponentsInChildren<ParticleEmitter>())
    {
      componentsInChild.emit = false;
      componentsInChild.particles = particleArray;
    }
  }

  public void PlayAnimation2()
  {
    if (!((Object) this.m_Target.GetComponent<Animation>() != (Object) null))
      return;
    this.m_Target.GetComponent<Animation>().Play();
  }

  public void StopAnimation2()
  {
    if (!((Object) this.m_Target.GetComponent<Animation>() != (Object) null))
      return;
    this.m_Target.GetComponent<Animation>().Stop();
  }

  public void PlayAnimationsInChildren2()
  {
    foreach (Animation componentsInChild in this.m_Target.GetComponentsInChildren<Animation>())
      componentsInChild.Play();
  }

  public void StopAnimationsInChildren2()
  {
    foreach (Animation componentsInChild in this.m_Target.GetComponentsInChildren<Animation>())
      componentsInChild.Stop();
  }

  public void ActivateHierarchy2()
  {
    this.m_Target.SetActive(true);
  }

  public void DeactivateHierarchy2()
  {
    this.m_Target.SetActive(false);
  }

  public void DestroyHierarchy2()
  {
    Object.Destroy((Object) this.m_Target);
  }

  public void FadeIn2(float FadeSec)
  {
    iTween.FadeTo(this.m_Target, 1f, FadeSec);
  }

  public void FadeOut2(float FadeSec)
  {
    iTween.FadeTo(this.m_Target, 0.0f, FadeSec);
  }

  public void SetAlphaHierarchy2(float alpha)
  {
    foreach (Renderer componentsInChild in this.m_Target.GetComponentsInChildren<Renderer>())
    {
      if (componentsInChild.material.HasProperty("_Color"))
      {
        Color color = componentsInChild.material.color;
        color.a = alpha;
        componentsInChild.material.color = color;
      }
    }
  }

  public void PlayDefaultSound2()
  {
    if ((Object) this.m_Target.GetComponent<AudioSource>() == (Object) null)
      Debug.LogError((object) string.Format("TargetAnimUtils2.PlayDefaultSound() - Tried to play the AudioSource on {0} but it has no AudioSource. You need an AudioSource to use this function.", (object) this.m_Target));
    else if ((Object) SoundManager.Get() == (Object) null)
      this.m_Target.GetComponent<AudioSource>().Play();
    else
      SoundManager.Get().Play(this.m_Target.GetComponent<AudioSource>(), true);
  }

  public void PlaySound2(AudioClip clip)
  {
    if ((Object) clip == (Object) null)
      Debug.LogError((object) string.Format("TargetAnimUtils2.PlayDefaultSound() - No clip was given when trying to play the AudioSource on {0}. You need a clip to use this function.", (object) this.m_Target));
    else if ((Object) this.m_Target.GetComponent<AudioSource>() == (Object) null)
      Debug.LogError((object) string.Format("TargetAnimUtils2.PlayDefaultSound() - Tried to play clip {0} on {1} but it has no AudioSource. You need an AudioSource to use this function.", (object) clip, (object) this.m_Target));
    else if ((Object) SoundManager.Get() == (Object) null)
      this.m_Target.GetComponent<AudioSource>().PlayOneShot(clip);
    else
      SoundManager.Get().PlayOneShot(this.m_Target.GetComponent<AudioSource>(), clip, 1f, true);
  }
}
