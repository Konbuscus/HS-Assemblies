﻿// Decompiled with JetBrains decompiler
// Type: DisconnectMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DisconnectMgr : MonoBehaviour
{
  private static DisconnectMgr s_instance;
  private AlertPopup m_dialog;

  private void Awake()
  {
    DisconnectMgr.s_instance = this;
  }

  private void OnDestroy()
  {
    if ((Object) SceneMgr.Get() != (Object) null)
      SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    DisconnectMgr.s_instance = (DisconnectMgr) null;
  }

  public static DisconnectMgr Get()
  {
    return DisconnectMgr.s_instance;
  }

  public void DisconnectFromGameplay()
  {
    SceneMgr.Mode disconnectSceneMode = GameMgr.Get().GetPostDisconnectSceneMode();
    GameMgr.Get().PreparePostGameSceneMode(disconnectSceneMode);
    if (disconnectSceneMode == SceneMgr.Mode.INVALID)
      Network.Get().ShowBreakingNewsOrError("GLOBAL_ERROR_NETWORK_LOST_GAME_CONNECTION", 0.0f);
    else if (Network.WasDisconnectRequested())
      SceneMgr.Get().SetNextMode(disconnectSceneMode);
    else
      this.ShowGameplayDialog(disconnectSceneMode);
  }

  private void ShowGameplayDialog(SceneMgr.Mode nextMode)
  {
    DialogManager.Get().ShowPopup(new AlertPopup.PopupInfo()
    {
      m_headerText = GameStrings.Get("GLOBAL_ERROR_NETWORK_TITLE"),
      m_text = GameStrings.Get("GLOBAL_ERROR_NETWORK_LOST_GAME_CONNECTION"),
      m_responseDisplay = AlertPopup.ResponseDisplay.NONE
    }, new DialogManager.DialogProcessCallback(this.OnGameplayDialogProcessed), (object) nextMode);
  }

  private bool OnGameplayDialogProcessed(DialogBase dialog, object userData)
  {
    this.m_dialog = (AlertPopup) dialog;
    SceneMgr.Get().SetNextMode((SceneMgr.Mode) userData);
    SceneMgr.Get().RegisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded));
    return true;
  }

  private void OnSceneLoaded(SceneMgr.Mode mode, Scene scene, object userData)
  {
    SceneMgr.Get().UnregisterSceneLoadedEvent(new SceneMgr.SceneLoadedCallback(this.OnSceneLoaded), userData);
    this.UpdateGameplayDialog();
  }

  private void UpdateGameplayDialog()
  {
    AlertPopup.PopupInfo info = this.m_dialog.GetInfo();
    info.m_responseDisplay = AlertPopup.ResponseDisplay.OK;
    info.m_responseCallback = new AlertPopup.ResponseCallback(this.OnGameplayDialogResponse);
    this.m_dialog.UpdateInfo(info);
  }

  private void OnGameplayDialogResponse(AlertPopup.Response response, object userData)
  {
    this.m_dialog = (AlertPopup) null;
  }
}
