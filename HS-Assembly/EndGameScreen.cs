﻿// Decompiled with JetBrains decompiler
// Type: EndGameScreen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using PegasusShared;
using SpectatorProto;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

[CustomEditClass]
public class EndGameScreen : MonoBehaviour
{
  protected List<Achievement> m_completedQuests = new List<Achievement>();
  private List<Reward> m_rewards = new List<Reward>();
  public EndGameTwoScoop m_twoScoop;
  public PegUIElement m_hitbox;
  public UberText m_noGoldRewardText;
  public UberText m_continueText;
  [CustomEditField(T = EditType.GAME_OBJECT)]
  public string m_ScoreScreenPrefab;
  public static EndGameScreen.OnTwoScoopsShownHandler OnTwoScoopsShown;
  private static EndGameScreen s_instance;
  protected bool m_shown;
  private ScoreScreen m_scoreScreen;
  protected bool m_netCacheReady;
  protected bool m_achievesReady;
  protected bool m_goldenHeroEventReady;
  private int m_numRewardsToLoad;
  private bool m_rewardsLoaded;
  private Reward m_currentlyShowingReward;
  private bool m_haveShownTwoScoop;
  private bool m_hasAlreadySetMode;
  protected bool m_playingBlockingAnim;
  protected bool m_doneDisplayingRewards;

  protected virtual void Awake()
  {
    EndGameScreen.s_instance = this;
    this.StartCoroutine(this.WaitForAchieveManager());
    this.ProcessPreviousAchievements();
    AchieveManager.Get().RegisterAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(this.OnAchievesUpdated), (object) null);
    AchieveManager.Get().TriggerLaunchDayEvent();
    this.m_hitbox.gameObject.SetActive(false);
    string key = "GLOBAL_CLICK_TO_CONTINUE";
    if (UniversalInputManager.Get().IsTouchMode())
      key = "GLOBAL_CLICK_TO_CONTINUE_TOUCH";
    this.m_continueText.Text = GameStrings.Get(key);
    this.m_continueText.gameObject.SetActive(false);
    this.m_noGoldRewardText.gameObject.SetActive(false);
    PegUI.Get().SetInputCamera(CameraUtils.FindFirstByLayer(GameLayer.IgnoreFullScreenEffects));
    SceneUtils.SetLayer(this.m_hitbox.gameObject, GameLayer.IgnoreFullScreenEffects);
    SceneUtils.SetLayer(this.m_continueText.gameObject, GameLayer.IgnoreFullScreenEffects);
    if (Network.ShouldBeConnectedToAurora())
      return;
    this.UpdateRewards();
  }

  private void OnDestroy()
  {
    if (EndGameScreen.OnTwoScoopsShown != null)
      EndGameScreen.OnTwoScoopsShown(false, this.m_twoScoop);
    AchieveManager.Get().RemoveAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(this.OnAchievesUpdated));
    EndGameScreen.s_instance = (EndGameScreen) null;
  }

  public static EndGameScreen Get()
  {
    return EndGameScreen.s_instance;
  }

  public virtual void Show()
  {
    this.m_shown = true;
    Network.DisconnectFromGameServer();
    InputManager.Get().DisableInput();
    this.m_hitbox.gameObject.SetActive(true);
    FullScreenFXMgr.Get().SetBlurDesaturation(0.5f);
    FullScreenFXMgr.Get().Blur(1f, 0.5f, iTween.EaseType.easeInCirc, (FullScreenFXMgr.EffectListener) null);
    if (GameState.Get() != null && GameState.Get().GetFriendlySidePlayer() != null)
      GameState.Get().GetFriendlySidePlayer().GetHandZone().UpdateLayout(-1);
    this.ShowScoreScreen();
    this.InitIfReady();
  }

  public void SetPlayingBlockingAnim(bool set)
  {
    this.m_playingBlockingAnim = set;
  }

  public bool IsPlayingBlockingAnim()
  {
    return this.m_playingBlockingAnim;
  }

  private void ShowTutorialProgress()
  {
    this.HideTwoScoop();
    this.StartCoroutine(this.LoadTutorialProgress());
  }

  [DebuggerHidden]
  private IEnumerator LoadTutorialProgress()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EndGameScreen.\u003CLoadTutorialProgress\u003Ec__Iterator8D()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnTutorialProgressScreenCallback(string name, GameObject go, object callbackData)
  {
    go.transform.parent = this.transform;
    go.GetComponent<TutorialProgressScreen>().StartTutorialProgress();
  }

  protected void ContinueButtonPress_Common()
  {
    LoadingScreen.Get().AddTransitionObject((Component) this);
  }

  protected void ContinueButtonPress_PrevMode(UIEvent e)
  {
    this.ContinueEvents();
  }

  public bool ContinueEvents()
  {
    if (this.ContinueDefaultEvents())
      return true;
    PlayMakerFSM component = this.m_twoScoop.GetComponent<PlayMakerFSM>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.SendEvent("Death");
    this.ContinueButtonPress_Common();
    this.m_hitbox.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ContinueButtonPress_PrevMode));
    this.ReturnToPreviousMode();
    return false;
  }

  protected void ContinueButtonPress_TutorialProgress(UIEvent e)
  {
    this.ContinueTutorialEvents();
  }

  public void ContinueTutorialEvents()
  {
    if (this.ContinueDefaultEvents())
      return;
    this.ContinueButtonPress_Common();
    this.m_hitbox.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.ContinueButtonPress_TutorialProgress));
    this.m_continueText.gameObject.SetActive(false);
    this.ShowTutorialProgress();
  }

  private bool ContinueDefaultEvents()
  {
    if (this.IsPlayingBlockingAnim())
      return true;
    this.HideScoreScreen();
    if (!this.m_haveShownTwoScoop)
      return true;
    this.HideTwoScoop();
    if (this.ShowGoldenHeroEvent() && this.m_goldenHeroEventReady || (this.ShowFixedRewards() || this.ShowNextCompletedQuest()) || this.ShowNextReward())
      return true;
    this.m_doneDisplayingRewards = true;
    return false;
  }

  protected virtual void InitGoldRewardUI()
  {
  }

  protected virtual void OnTwoScoopShown()
  {
  }

  protected virtual void OnTwoScoopHidden()
  {
  }

  private static string GetFriendlyChallengeRewardMessage(Achievement achieve)
  {
    string str = (string) null;
    if (achieve.DbfRecord.MaxDefense > 0)
    {
      str = EndGameScreen.GetFriendlyChallengeConcederDefenseMessage(achieve.DbfRecord.MaxDefense);
      if (!string.IsNullOrEmpty(str))
        return str;
    }
    if (achieve.DbfRecord.RewardableLimit > 0 && achieve.IntervalRewardStartDate > 0L && ((DateTime.UtcNow - DateTime.FromFileTimeUtc(achieve.IntervalRewardStartDate)).TotalDays < (double) achieve.DbfRecord.RewardableInterval && achieve.IntervalRewardCount >= achieve.DbfRecord.RewardableLimit))
      str = GameStrings.Get("GLOBAL_FRIENDLYCHALLENGE_QUEST_REWARD_AT_LIMIT");
    if (string.IsNullOrEmpty(str) && achieve.DbfRecord.RewardableLimit > 0 && FriendChallengeMgr.Get().DidReceiveChallenge())
      achieve.IncrementIntervalRewardCount();
    return str;
  }

  protected static string GetFriendlyChallengeRewardText()
  {
    if (!FriendChallengeMgr.Get().HasChallenge())
      return (string) null;
    string str = (string) null;
    AchieveManager achieveManager = AchieveManager.Get();
    PartyQuestInfo partyQuestInfo = FriendChallengeMgr.Get().GetPartyQuestInfo();
    if (partyQuestInfo != null)
    {
      bool flag = FriendChallengeMgr.Get().DidSendChallenge();
      bool challenge = FriendChallengeMgr.Get().DidReceiveChallenge();
      PlayerType playerType = PlayerType.PT_ANY;
      if (flag)
        playerType = PlayerType.PT_FRIENDLY_CHALLENGER;
      if (challenge)
        playerType = PlayerType.PT_FRIENDLY_CHALLENGEE;
      for (int index = 0; index < partyQuestInfo.QuestIds.Count; ++index)
      {
        Achievement achievement1 = achieveManager.GetAchievement(partyQuestInfo.QuestIds[index]);
        if (achievement1 != null && achievement1.IsValidFriendlyPlayerChallengeType(playerType))
          str = EndGameScreen.GetFriendlyChallengeRewardMessage(achievement1);
        if (string.IsNullOrEmpty(str))
        {
          Achievement achievement2 = achieveManager.GetAchievement(achievement1.DbfRecord.SharedAchieveId);
          if (achievement2 != null && achievement1.IsValidFriendlyPlayerChallengeType(playerType))
            str = EndGameScreen.GetFriendlyChallengeRewardMessage(achievement2);
        }
      }
    }
    if (string.IsNullOrEmpty(str) && SpecialEventManager.Get().IsEventActive(SpecialEventType.FRIEND_WEEK, false))
    {
      if (!achieveManager.GetActiveQuests(false).Where<Achievement>((Func<Achievement, bool>) (a =>
      {
        if (!a.IsAffectedByFriendWeek)
          return false;
        if (a.AchieveTrigger != Achievement.Trigger.WIN_GAME)
          return a.AchieveTrigger == Achievement.Trigger.FINISH_GAME;
        return true;
      })).Any<Achievement>())
        return (string) null;
      int concederMaxDefense = 0;
      NetCache.NetCacheRewardProgress netObject = NetCache.Get().GetNetObject<NetCache.NetCacheRewardProgress>();
      if (netObject != null)
        concederMaxDefense = netObject.FriendWeekConcederMaxDefense;
      if (concederMaxDefense > 0)
        str = EndGameScreen.GetFriendlyChallengeConcederDefenseMessage(concederMaxDefense);
    }
    return str;
  }

  private static string GetFriendlyChallengeConcederDefenseMessage(int concederMaxDefense)
  {
    string key = (string) null;
    int num = 0;
    using (Map<int, Player>.Enumerator enumerator = GameState.Get().GetPlayerMap().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player player = enumerator.Current.Value;
        switch (player.GetPreGameOverPlayState())
        {
          case TAG_PLAYSTATE.CONCEDED:
          case TAG_PLAYSTATE.DISCONNECTED:
            Entity hero = player.GetHero();
            if (hero != null)
            {
              num = hero.GetCurrentDefense();
              key = player.GetSide() != Player.Side.FRIENDLY ? "GLOBAL_FRIENDLYCHALLENGE_REWARD_CONCEDED_YOUR_OPPONENT" : "GLOBAL_FRIENDLYCHALLENGE_REWARD_CONCEDED_YOURSELF";
              goto label_7;
            }
            else
              continue;
          default:
            continue;
        }
      }
    }
label_7:
    if (num > concederMaxDefense)
      return GameStrings.Get(key);
    return (string) null;
  }

  protected void BackToMode(SceneMgr.Mode mode)
  {
    AchieveManager.Get().RemoveAchievesUpdatedListener(new AchieveManager.AchievesUpdatedCallback(this.OnAchievesUpdated));
    this.HideTwoScoop();
    if (this.m_hasAlreadySetMode)
      return;
    this.m_hasAlreadySetMode = true;
    this.StartCoroutine(this.ToMode(mode));
    Navigation.Clear();
  }

  [DebuggerHidden]
  private IEnumerator ToMode(SceneMgr.Mode mode)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EndGameScreen.\u003CToMode\u003Ec__Iterator8E()
    {
      mode = mode,
      \u003C\u0024\u003Emode = mode
    };
  }

  private void ReturnToPreviousMode()
  {
    SceneMgr.Mode postGameSceneMode = GameMgr.Get().GetPostGameSceneMode();
    GameMgr.Get().PreparePostGameSceneMode(postGameSceneMode);
    this.BackToMode(postGameSceneMode);
  }

  private void ShowScoreScreen()
  {
    if (!GameState.Get().CanShowScoreScreen())
      return;
    this.m_scoreScreen = GameUtils.LoadGameObjectWithComponent<ScoreScreen>(this.m_ScoreScreenPrefab);
    if (!(bool) ((UnityEngine.Object) this.m_scoreScreen))
      return;
    TransformUtil.AttachAndPreserveLocalTransform(this.m_scoreScreen.transform, this.transform);
    SceneUtils.SetLayer((Component) this.m_scoreScreen, GameLayer.IgnoreFullScreenEffects);
    this.m_scoreScreen.Show();
    this.SetPlayingBlockingAnim(true);
    this.StartCoroutine(this.WaitThenSetPlayingBlockingAnim(0.65f, false));
  }

  private void HideScoreScreen()
  {
    if (!(bool) ((UnityEngine.Object) this.m_scoreScreen))
      return;
    this.m_scoreScreen.Hide();
    this.SetPlayingBlockingAnim(true);
    this.StartCoroutine(this.WaitThenSetPlayingBlockingAnim(0.25f, false));
  }

  protected void HideTwoScoop()
  {
    if (!this.m_twoScoop.IsShown())
      return;
    this.m_twoScoop.Hide();
    this.m_noGoldRewardText.gameObject.SetActive(false);
    this.OnTwoScoopHidden();
    if (EndGameScreen.OnTwoScoopsShown != null)
      EndGameScreen.OnTwoScoopsShown(false, this.m_twoScoop);
    if (!((UnityEngine.Object) InputManager.Get() != (UnityEngine.Object) null))
      return;
    InputManager.Get().EnableInput();
  }

  private void ShowTwoScoop()
  {
    this.StartCoroutine(this.ShowTwoScoopWhenReady());
  }

  [DebuggerHidden]
  private IEnumerator ShowTwoScoopWhenReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EndGameScreen.\u003CShowTwoScoopWhenReady\u003Ec__Iterator8F()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  protected IEnumerator WaitThenSetPlayingBlockingAnim(float sec, bool set)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EndGameScreen.\u003CWaitThenSetPlayingBlockingAnim\u003Ec__Iterator90()
    {
      sec = sec,
      set = set,
      \u003C\u0024\u003Esec = sec,
      \u003C\u0024\u003Eset = set,
      \u003C\u003Ef__this = this
    };
  }

  protected bool ShouldMakeUtilRequests()
  {
    return Network.ShouldBeConnectedToAurora();
  }

  protected bool IsReady()
  {
    if (this.m_shown && this.m_netCacheReady && this.m_achievesReady)
      return this.m_rewardsLoaded;
    return false;
  }

  public bool IsDoneDisplayingRewards()
  {
    return this.m_doneDisplayingRewards;
  }

  protected bool InitIfReady()
  {
    if (!this.IsReady() && (this.ShouldMakeUtilRequests() || !this.m_shown))
      return false;
    if (!GameMgr.Get().IsSpectator() && GameMgr.Get().IsPlay() && Options.Get().GetBool(Option.IN_RANKED_PLAY_MODE))
      AssetLoader.Get().LoadGameObject("RankChangeTwoScoop", new AssetLoader.GameObjectCallback(this.OnRankChangeLoaded), (object) null, false);
    else
      this.ShowStandardFlow();
    return true;
  }

  protected virtual void ShowStandardFlow()
  {
    this.ShowTwoScoop();
    if ((bool) UniversalInputManager.UsePhoneUI)
      return;
    this.m_continueText.gameObject.SetActive(true);
  }

  protected virtual void OnNetCacheReady()
  {
    this.m_netCacheReady = true;
    NetCache.Get().UnregisterNetCacheHandler(new NetCache.NetCacheCallback(this.OnNetCacheReady));
    this.MaybeUpdateRewards();
  }

  private void OnRankChangeLoaded(string name, GameObject go, object callbackData)
  {
    NetCache.NetCacheMedalInfo netObject = NetCache.Get().GetNetObject<NetCache.NetCacheMedalInfo>();
    go.GetComponent<RankChangeTwoScoop>().Initialize(new MedalInfoTranslator(netObject, netObject.PreviousMedalInfo), new RankChangeTwoScoop.RankChangeClosed(this.ShowStandardFlow));
  }

  [DebuggerHidden]
  private IEnumerator WaitForAchieveManager()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EndGameScreen.\u003CWaitForAchieveManager\u003Ec__Iterator91()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ProcessPreviousAchievements()
  {
    this.OnAchievesUpdated(new List<Achievement>(), new List<Achievement>(), (object) null);
  }

  private void OnAchievesUpdated(List<Achievement> updatedAchieves, List<Achievement> completedAchieves, object userData)
  {
    if (!GameUtils.AreAllTutorialsComplete())
      return;
    List<Achievement> completedAchieves1 = AchieveManager.Get().GetNewCompletedAchieves();
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    EndGameScreen.\u003COnAchievesUpdated\u003Ec__AnonStorey3B1 updatedCAnonStorey3B1 = new EndGameScreen.\u003COnAchievesUpdated\u003Ec__AnonStorey3B1();
    using (List<Achievement>.Enumerator enumerator = completedAchieves1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        // ISSUE: reference to a compiler-generated field
        updatedCAnonStorey3B1.achieve = enumerator.Current;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        if (updatedCAnonStorey3B1.achieve.RewardTiming == RewardVisualTiming.IMMEDIATE && this.m_completedQuests.Find(new Predicate<Achievement>(updatedCAnonStorey3B1.\u003C\u003Em__128)) == null)
        {
          // ISSUE: reference to a compiler-generated field
          this.m_completedQuests.Add(updatedCAnonStorey3B1.achieve);
        }
      }
    }
  }

  protected bool HasShownScoops()
  {
    return this.m_haveShownTwoScoop;
  }

  protected void SetGoldenHeroEventReady(bool isReady)
  {
    this.m_goldenHeroEventReady = isReady;
  }

  private void MaybeUpdateRewards()
  {
    if (!this.m_achievesReady || !this.m_netCacheReady)
      return;
    this.UpdateRewards();
    this.InitIfReady();
  }

  private void UpdateRewards()
  {
    bool flag = true;
    if (GameMgr.Get().IsTutorial())
      flag = GameUtils.AreAllTutorialsComplete();
    List<RewardData> rewardsToShow = (List<RewardData>) null;
    if (flag)
      RewardUtils.GetViewableRewards(RewardUtils.GetRewards(NetCache.Get().GetNetObject<NetCache.NetCacheProfileNotices>().Notices), new HashSet<RewardVisualTiming>()
      {
        RewardVisualTiming.IMMEDIATE
      }, ref rewardsToShow, ref this.m_completedQuests);
    else
      rewardsToShow = new List<RewardData>();
    if (this.JustEarnedGoldenHero())
      this.LoadGoldenHeroEvent();
    if (!GameMgr.Get().IsSpectator())
    {
      List<RewardData> customRewards = GameState.Get().GetGameEntity().GetCustomRewards();
      if (customRewards != null)
        rewardsToShow.AddRange((IEnumerable<RewardData>) customRewards);
    }
    this.m_numRewardsToLoad = rewardsToShow.Count;
    if (this.m_numRewardsToLoad == 0)
    {
      this.m_rewardsLoaded = true;
    }
    else
    {
      using (List<RewardData>.Enumerator enumerator = rewardsToShow.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.LoadRewardObject(new Reward.DelOnRewardLoaded(this.RewardObjectLoaded));
      }
    }
  }

  private void PositionReward(Reward reward)
  {
    reward.transform.parent = this.transform;
    reward.transform.localRotation = Quaternion.identity;
    reward.transform.localPosition = PopupDisplayManager.Get().GetRewardLocalPos();
  }

  private void RewardObjectLoaded(Reward reward, object callbackData)
  {
    reward.Hide(false);
    this.PositionReward(reward);
    this.m_rewards.Add(reward);
    --this.m_numRewardsToLoad;
    if (this.m_numRewardsToLoad > 0)
      return;
    RewardUtils.SortRewards(ref this.m_rewards);
    this.m_rewardsLoaded = true;
    this.InitIfReady();
  }

  private void DisplayLoadedRewardObject(Reward reward, object callbackData)
  {
    if ((UnityEngine.Object) this.m_currentlyShowingReward != (UnityEngine.Object) null)
    {
      this.m_currentlyShowingReward.Hide(true);
      this.m_currentlyShowingReward = (Reward) null;
    }
    reward.Hide(false);
    this.PositionReward(reward);
    this.m_currentlyShowingReward = reward;
    this.SetPlayingBlockingAnim(true);
    SceneUtils.SetLayer(this.m_currentlyShowingReward.gameObject, GameLayer.IgnoreFullScreenEffects);
    this.ShowReward(this.m_currentlyShowingReward);
  }

  private void ShowReward(Reward reward)
  {
    RewardUtils.ShowReward(UserAttentionBlocker.NONE, reward, true, PopupDisplayManager.Get().GetRewardPunchScale(), PopupDisplayManager.Get().GetRewardScale(), string.Empty, (object) null, (GameObject) null);
    this.StartCoroutine(this.WaitThenSetPlayingBlockingAnim(0.35f, false));
  }

  private void OnRewardHidden(Reward reward)
  {
    reward.Hide(false);
  }

  protected virtual void LoadGoldenHeroEvent()
  {
  }

  protected virtual bool ShowGoldenHeroEvent()
  {
    return false;
  }

  protected bool ShowFixedRewards()
  {
    return FixedRewardsMgr.Get().ShowFixedRewards(UserAttentionBlocker.NONE, new HashSet<RewardVisualTiming>()
    {
      RewardVisualTiming.IMMEDIATE
    }, (FixedRewardsMgr.DelOnAllFixedRewardsShown) (userData => this.ContinueEvents()), new FixedRewardsMgr.DelPositionNonToastReward(this.PositionReward), PopupDisplayManager.Get().GetRewardPunchScale(), PopupDisplayManager.Get().GetRewardScale());
  }

  protected bool ShowNextCompletedQuest()
  {
    if (QuestToast.IsQuestActive())
      QuestToast.GetCurrentToast().CloseQuestToast();
    if (this.m_completedQuests.Count == 0)
      return false;
    Achievement completedQuest = this.m_completedQuests[0];
    this.m_completedQuests.RemoveAt(0);
    if (!completedQuest.UseGenericRewardVisual)
    {
      QuestToast.ShowQuestToast(UserAttentionBlocker.NONE, new QuestToast.DelOnCloseQuestToast(this.ShowQuestToastCallback), true, completedQuest);
      NarrativeManager.Get().OnQuestCompleteShown(completedQuest.ID);
    }
    else
    {
      completedQuest.AckCurrentProgressAndRewardNotices();
      completedQuest.Rewards[0].LoadRewardObject(new Reward.DelOnRewardLoaded(this.DisplayLoadedRewardObject));
    }
    return true;
  }

  protected void ShowQuestToastCallback(object userData)
  {
    this.ContinueEvents();
  }

  protected bool ShowNextReward()
  {
    if ((UnityEngine.Object) this.m_currentlyShowingReward != (UnityEngine.Object) null)
    {
      this.m_currentlyShowingReward.Hide(true);
      this.m_currentlyShowingReward = (Reward) null;
    }
    if (this.m_rewards.Count == 0)
      return false;
    this.SetPlayingBlockingAnim(true);
    this.m_currentlyShowingReward = this.m_rewards[0];
    this.m_rewards.RemoveAt(0);
    this.ShowReward(this.m_currentlyShowingReward);
    return true;
  }

  protected virtual bool JustEarnedGoldenHero()
  {
    return false;
  }

  public delegate void OnTwoScoopsShownHandler(bool shown, EndGameTwoScoop twoScoops);
}
