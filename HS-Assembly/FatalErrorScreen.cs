﻿// Decompiled with JetBrains decompiler
// Type: FatalErrorScreen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using bgs;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class FatalErrorScreen : MonoBehaviour
{
  public UberText m_closedSignText;
  public UberText m_closedSignTitle;
  public UberText m_reconnectTip;
  public UberText m_errorCodeText;
  private Camera m_camera;
  private PegUIElement m_inputBlocker;
  private bool m_allowClick;
  private bool m_redirectToStore;
  public float m_delayBeforeNextReset;

  private void Awake()
  {
    SplashScreen splashScreen = SplashScreen.Get();
    if ((Object) splashScreen != (Object) null)
      splashScreen.HideLogo();
    this.m_closedSignTitle.Text = GameStrings.Get("GLOBAL_SPLASH_CLOSED_SIGN_TITLE");
    List<FatalErrorMessage> messages = FatalErrorMgr.Get().GetMessages();
    Log.JMac.Print(LogLevel.Warning, string.Format("Showing Fatal Error Screen with {0} messages", (object) messages.Count), new object[0]);
    if (messages.Count > 0)
    {
      this.m_closedSignText.Text = messages[0].m_text;
      this.m_allowClick = messages[0].m_allowClick;
      this.m_redirectToStore = messages[0].m_redirectToStore;
      this.m_delayBeforeNextReset = messages[0].m_delayBeforeNextReset;
    }
    else
      this.m_closedSignText.Text = "Please make it sure FatalError scene is NOT in your Hierarchy window.";
  }

  private void Start()
  {
    this.StartCoroutine(this.WaitForUIThenFinishSetup());
  }

  private void Update()
  {
    if (!(bool) ApplicationMgr.AllowResetFromFatalError || !this.m_allowClick)
      return;
    if (this.m_redirectToStore)
      this.m_reconnectTip.SetGameStringText("GLOBAL_MOBILE_TAP_TO_UPDATE");
    else
      this.m_reconnectTip.SetGameStringText("GLOBAL_MOBILE_TAP_TO_RECONNECT");
    this.m_reconnectTip.gameObject.SetActive(true);
    this.m_reconnectTip.TextAlpha = (float) (((double) Mathf.Sin(Time.time * 3.141593f / 1f) + 1.0) / 2.0);
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    iTween.FadeTo(this.gameObject, iTween.Hash((object) "amount", (object) 1f, (object) "time", (object) 0.25f, (object) "easeType", (object) iTween.EaseType.easeOutCubic));
  }

  [DebuggerHidden]
  private IEnumerator WaitForUIThenFinishSetup()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FatalErrorScreen.\u003CWaitForUIThenFinishSetup\u003Ec__IteratorA1() { \u003C\u003Ef__this = this };
  }

  private void OnClick(UIEvent e)
  {
    if ((bool) ApplicationMgr.AllowResetFromFatalError)
    {
      if (this.m_redirectToStore)
      {
        PlatformDependentValue<string> platformDependentValue1 = new PlatformDependentValue<string>(PlatformCategory.OS) { iOS = "https://itunes.apple.com/app/hearthstone-heroes-warcraft/id625257520?ls=1&mt=8", Android = "https://play.google.com/store/apps/details?id=com.blizzard.wtcg.hearthstone" };
        PlatformDependentValue<string> platformDependentValue2 = new PlatformDependentValue<string>(PlatformCategory.OS) { iOS = "https://itunes.apple.com/cn/app/lu-shi-chuan-shuo-mo-shou/id841140063?ls=1&mt=8", Android = "https://www.battlenet.com.cn/account/download/hearthstone/android?style=hearthstone" };
        if (ApplicationMgr.GetAndroidStore() == AndroidStore.AMAZON)
          platformDependentValue1.Android = "http://www.amazon.com/gp/mas/dl/android?p=com.blizzard.wtcg.hearthstone";
        if (MobileDeviceLocale.GetCurrentRegionId() == constants.BnetRegion.REGION_CN)
          platformDependentValue1 = platformDependentValue2;
        Application.OpenURL((string) platformDependentValue1);
      }
      else
      {
        float waitDuration = ApplicationMgr.Get().LastResetTime() + this.m_delayBeforeNextReset - Time.realtimeSinceStartup;
        Log.JMac.Print("Remaining time to wait before allowing a reconnect attempt: " + (object) waitDuration);
        if ((double) waitDuration > 0.0)
        {
          this.m_inputBlocker.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClick));
          this.m_closedSignText.Text = GameStrings.Get("GLOBAL_SPLASH_CLOSED_RECONNECTING");
          this.m_allowClick = false;
          this.m_reconnectTip.gameObject.SetActive(false);
          this.StartCoroutine(this.WaitBeforeReconnecting(waitDuration));
        }
        else
        {
          UnityEngine.Debug.Log((object) "resetting!");
          this.m_inputBlocker.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClick));
          ApplicationMgr.Get().Reset();
        }
      }
    }
    else
    {
      this.m_inputBlocker.RemoveEventListener(UIEventType.RELEASE, new UIEvent.Handler(this.OnClick));
      ApplicationMgr.Get().Exit();
    }
  }

  [DebuggerHidden]
  private IEnumerator WaitBeforeReconnecting(float waitDuration)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FatalErrorScreen.\u003CWaitBeforeReconnecting\u003Ec__IteratorA2() { waitDuration = waitDuration, \u003C\u0024\u003EwaitDuration = waitDuration };
  }
}
