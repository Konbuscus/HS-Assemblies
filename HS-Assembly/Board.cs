﻿// Decompiled with JetBrains decompiler
// Type: Board
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[CustomEditClass]
public class Board : MonoBehaviour
{
  private readonly Color MULLIGAN_AMBIENT_LIGHT_COLOR = new Color(0.1607843f, 0.1921569f, 0.282353f, 1f);
  public Color m_AmbientColor = Color.white;
  public float m_DirectionalLightIntensity = 0.275f;
  public Color m_ShadowColor = new Color(0.098f, 0.098f, 0.235f, 0.45f);
  public Color m_DeckColor = Color.white;
  public Color m_EndTurnButtonColor = Color.white;
  public Color m_HistoryTileColor = Color.white;
  public Color m_GoldenHeroTrayColor = Color.white;
  public MusicPlaylistType m_BoardMusic = MusicPlaylistType.InGame_Default;
  private Color m_TrayTint = Color.white;
  private const string GOLDEN_HERO_TRAY_FRIENDLY = "HeroTray_Golden_Friendly";
  private const string GOLDEN_HERO_TRAY_OPPONENT = "HeroTray_Golden_Opponent";
  private const float MULLIGAN_LIGHT_INTENSITY = 0.0f;
  public Light m_DirectionalLight;
  public GameObject m_FriendlyHeroTray;
  public GameObject m_OpponentHeroTray;
  public GameObject m_FriendlyHeroPhoneTray;
  public GameObject m_OpponentHeroPhoneTray;
  public Transform m_BoneParent;
  public GameObject m_SplitPlaySurface;
  public GameObject m_CombinedPlaySurface;
  public Transform m_ColliderParent;
  public GameObject m_MouseClickDustEffect;
  public List<Board.BoardSpecialEvents> m_SpecialEvents;
  public Texture m_GemManaPhoneTexture;
  private static Board s_instance;
  private bool m_raisedLights;
  private Spell m_FriendlyTraySpellEffect;
  private Spell m_OpponentTraySpellEffect;
  private int m_boardDbId;

  private void Awake()
  {
    Board.s_instance = this;
    if ((UnityEngine.Object) LoadingScreen.Get() != (UnityEngine.Object) null)
      LoadingScreen.Get().NotifyMainSceneObjectAwoke(this.gameObject);
    if ((UnityEngine.Object) this.m_FriendlyHeroTray == (UnityEngine.Object) null)
      UnityEngine.Debug.LogError((object) "Friendly Hero Tray is not assigned!");
    if (!((UnityEngine.Object) this.m_OpponentHeroTray == (UnityEngine.Object) null))
      return;
    UnityEngine.Debug.LogError((object) "Opponent Hero Tray is not assigned!");
  }

  private void OnDestroy()
  {
    Board.s_instance = (Board) null;
  }

  private void Start()
  {
    ProjectedShadow.SetShadowColor(this.m_ShadowColor);
    if ((UnityEngine.Object) this.GetComponent<Animation>() != (UnityEngine.Object) null)
    {
      this.GetComponent<Animation>()[this.GetComponent<Animation>().clip.name].normalizedTime = 0.25f;
      this.GetComponent<Animation>()[this.GetComponent<Animation>().clip.name].speed = -3f;
      this.GetComponent<Animation>().Play(this.GetComponent<Animation>().clip.name);
    }
    this.StartCoroutine(this.GoldenHeroes());
    using (List<Board.BoardSpecialEvents>.Enumerator enumerator = this.m_SpecialEvents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Board.BoardSpecialEvents current = enumerator.Current;
        if (SpecialEventManager.Get().IsEventActive(current.EventType, false))
        {
          Log.Kyle.Print("Board Special Event: ", (object) current.EventType);
          this.LoadBoardSpecialEvent(current);
        }
      }
    }
  }

  public static Board Get()
  {
    return Board.s_instance;
  }

  public void SetBoardDbId(int id)
  {
    this.m_boardDbId = id;
    Log.Kyle.Print("Board DB ID: {0}", (object) id);
  }

  public void ResetAmbientColor()
  {
    RenderSettings.ambientLight = this.m_AmbientColor;
  }

  [ContextMenu("RaiseTheLights")]
  public void RaiseTheLights()
  {
    this.RaiseTheLights(1f);
  }

  public void RaiseTheLightsQuickly()
  {
    this.RaiseTheLights(5f);
  }

  public void RaiseTheLights(float speed)
  {
    if (this.m_raisedLights)
      return;
    float num = 3f / speed;
    Action<object> action1 = (Action<object>) (amount => RenderSettings.ambientLight = (Color) amount);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) RenderSettings.ambientLight, (object) "to", (object) this.m_AmbientColor, (object) "time", (object) num, (object) "easeType", (object) iTween.EaseType.easeInOutQuad, (object) "onupdate", (object) action1, (object) "onupdatetarget", (object) this.gameObject));
    Action<object> action2 = (Action<object>) (amount => this.m_DirectionalLight.intensity = (float) amount);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) this.m_DirectionalLight.intensity, (object) "to", (object) this.m_DirectionalLightIntensity, (object) "time", (object) num, (object) "easeType", (object) iTween.EaseType.easeInOutQuad, (object) "onupdate", (object) action2, (object) "onupdatetarget", (object) this.gameObject));
    this.m_raisedLights = true;
  }

  public void SetMulliganLighting()
  {
    RenderSettings.ambientLight = this.MULLIGAN_AMBIENT_LIGHT_COLOR;
    this.m_DirectionalLight.intensity = 0.0f;
  }

  public void DimTheLights()
  {
    this.DimTheLights(5f);
  }

  public void DimTheLights(float speed)
  {
    if (!this.m_raisedLights)
      return;
    float num = 3f / speed;
    Action<object> action1 = (Action<object>) (amount => RenderSettings.ambientLight = (Color) amount);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) RenderSettings.ambientLight, (object) "to", (object) this.MULLIGAN_AMBIENT_LIGHT_COLOR, (object) "time", (object) num, (object) "easeType", (object) iTween.EaseType.easeInOutQuad, (object) "onupdate", (object) action1, (object) "onupdatetarget", (object) this.gameObject));
    Action<object> action2 = (Action<object>) (amount => this.m_DirectionalLight.intensity = (float) amount);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) this.m_DirectionalLight.intensity, (object) "to", (object) 0.0f, (object) "time", (object) num, (object) "easeType", (object) iTween.EaseType.easeInOutQuad, (object) "onupdate", (object) action2, (object) "onupdatetarget", (object) this.gameObject));
    this.m_raisedLights = false;
  }

  public Transform FindBone(string name)
  {
    if ((UnityEngine.Object) this.m_BoneParent != (UnityEngine.Object) null)
    {
      Transform transform = this.m_BoneParent.Find(name);
      if ((UnityEngine.Object) transform != (UnityEngine.Object) null)
        return transform;
    }
    return BoardStandardGame.Get().FindBone(name);
  }

  public Collider FindCollider(string name)
  {
    if ((UnityEngine.Object) this.m_ColliderParent != (UnityEngine.Object) null)
    {
      Transform transform = this.m_ColliderParent.Find(name);
      if ((UnityEngine.Object) transform != (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) transform == (UnityEngine.Object) null)
          return (Collider) null;
        return transform.GetComponent<Collider>();
      }
    }
    return BoardStandardGame.Get().FindCollider(name);
  }

  public GameObject GetMouseClickDustEffectPrefab()
  {
    return this.m_MouseClickDustEffect;
  }

  public void CombinedSurface()
  {
    if (!((UnityEngine.Object) this.m_CombinedPlaySurface != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_SplitPlaySurface != (UnityEngine.Object) null))
      return;
    this.m_CombinedPlaySurface.SetActive(true);
    this.m_SplitPlaySurface.SetActive(false);
  }

  public void SplitSurface()
  {
    if (!((UnityEngine.Object) this.m_CombinedPlaySurface != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_SplitPlaySurface != (UnityEngine.Object) null))
      return;
    this.m_CombinedPlaySurface.SetActive(false);
    this.m_SplitPlaySurface.SetActive(true);
  }

  public Spell GetFriendlyTraySpell()
  {
    return this.m_FriendlyTraySpellEffect;
  }

  public Spell GetOpponentTraySpell()
  {
    return this.m_OpponentTraySpellEffect;
  }

  [DebuggerHidden]
  private IEnumerator GoldenHeroes()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Board.\u003CGoldenHeroes\u003Ec__Iterator2E2() { \u003C\u003Ef__this = this };
  }

  private void ShowFriendlyHeroTray(string name, GameObject go, object callbackData)
  {
    go.transform.position = ZoneMgr.Get().FindZoneOfType<ZoneHero>(Player.Side.FRIENDLY).transform.position;
    go.SetActive(true);
    foreach (Renderer componentsInChild in go.GetComponentsInChildren<Renderer>())
      componentsInChild.material.color = this.m_GoldenHeroTrayColor;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_FriendlyHeroTray);
    this.m_FriendlyHeroTray = go;
    this.StartCoroutine(this.UpdateHeroTray(Player.Side.FRIENDLY, true));
  }

  private void ShowOpponentHeroTray(string name, GameObject go, object callbackData)
  {
    go.transform.position = ZoneMgr.Get().FindZoneOfType<ZoneHero>(Player.Side.OPPOSING).transform.position;
    go.SetActive(true);
    foreach (Renderer componentsInChild in go.GetComponentsInChildren<Renderer>())
      componentsInChild.material.color = this.m_GoldenHeroTrayColor;
    this.m_OpponentHeroTray.SetActive(false);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_OpponentHeroTray);
    this.m_OpponentHeroTray = go;
    this.StartCoroutine(this.UpdateHeroTray(Player.Side.OPPOSING, true));
  }

  [DebuggerHidden]
  private IEnumerator UpdateHeroTray(Player.Side side, bool isGolden)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Board.\u003CUpdateHeroTray\u003Ec__Iterator2E3() { side = side, \u003C\u0024\u003Eside = side, \u003C\u003Ef__this = this };
  }

  private void OnHeroSkinManaGemTextureLoaded(string path, UnityEngine.Object obj, object callbackData)
  {
    if (obj == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "OnHeroSkinManaGemTextureLoaded() loaded texture is null!");
    }
    else
    {
      ManaCrystalMgr.Get().SetFriendlyManaGemTexture((Texture) obj);
      ManaCrystalMgr.Get().SetFriendlyManaGemTint(this.m_TrayTint);
    }
  }

  private void OnHeroTrayTextureLoaded(string path, UnityEngine.Object obj, object callbackData)
  {
    if (obj == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Board.OnHeroTrayTextureLoaded() loaded texture is null!");
    }
    else
    {
      Texture texture = (Texture) obj;
      if ((int) callbackData == 1)
      {
        Material material = this.m_FriendlyHeroTray.GetComponentInChildren<MeshRenderer>().material;
        material.mainTexture = texture;
        material.color = this.m_TrayTint;
      }
      else
      {
        Material material = this.m_OpponentHeroTray.GetComponentInChildren<MeshRenderer>().material;
        material.mainTexture = texture;
        material.color = this.m_TrayTint;
      }
    }
  }

  private void OnHeroPhoneTrayTextureLoaded(string path, UnityEngine.Object obj, object callbackData)
  {
    if (obj == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Board.OnHeroTrayTextureLoaded() loaded texture is null!");
    }
    else
    {
      Texture texture = (Texture) obj;
      if ((int) callbackData == 1)
      {
        if ((UnityEngine.Object) this.m_FriendlyHeroPhoneTray == (UnityEngine.Object) null)
        {
          UnityEngine.Debug.LogWarning((object) "Friendly Hero Phone Tray Object on Board is null!");
        }
        else
        {
          Material material = this.m_FriendlyHeroPhoneTray.GetComponentInChildren<MeshRenderer>().material;
          material.mainTexture = texture;
          material.color = this.m_TrayTint;
        }
      }
      else if ((UnityEngine.Object) this.m_OpponentHeroPhoneTray == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogWarning((object) "Opponent Hero Phone Tray Object on Board is null!");
      }
      else
      {
        Material material = this.m_OpponentHeroPhoneTray.GetComponentInChildren<MeshRenderer>().material;
        material.mainTexture = texture;
        material.color = this.m_TrayTint;
      }
    }
  }

  private void OnHeroTrayEffectLoaded(string name, GameObject go, object callbackData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "Board.OnHeroTrayEffectLoaded() Hero tray effect is null!");
    }
    else
    {
      Spell component = go.GetComponent<Spell>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        UnityEngine.Debug.LogError((object) "Board.OnHeroTrayEffectLoaded() Hero tray effect: could not find spell component!");
      else if ((int) callbackData == 1)
      {
        go.transform.parent = this.transform;
        go.transform.position = this.FindBone("CustomSocketIn_Friendly").position;
        this.m_FriendlyTraySpellEffect = component;
      }
      else
      {
        go.transform.parent = this.transform;
        go.transform.position = this.FindBone("CustomSocketIn_Opposing").position;
        this.m_OpponentTraySpellEffect = component;
      }
    }
  }

  private void LoadBoardSpecialEvent(Board.BoardSpecialEvents boardSpecialEvent)
  {
    Log.Kyle.Print("Loading Board Special Event Prefab: {0}", (object) boardSpecialEvent.Prefab);
    if ((UnityEngine.Object) AssetLoader.Get().LoadGameObject(FileUtils.GameAssetPathToName(boardSpecialEvent.Prefab), true, false) == (UnityEngine.Object) null)
      UnityEngine.Debug.LogWarning((object) string.Format("Failed to load special board event: {0}", (object) boardSpecialEvent.Prefab));
    this.m_AmbientColor = boardSpecialEvent.AmbientColorOverride;
  }

  [Serializable]
  public class CustomTraySettings
  {
    public Color m_Tint = Color.white;
    public BoardDdId m_Board;
  }

  [Serializable]
  public class BoardSpecialEvents
  {
    public Color AmbientColorOverride = Color.white;
    public SpecialEventType EventType;
    [CustomEditField(T = EditType.GAME_OBJECT)]
    public string Prefab;
  }
}
