﻿// Decompiled with JetBrains decompiler
// Type: BoxMenuButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoxMenuButton : PegUIElement
{
  public UberText m_TextMesh;
  public Spell m_Spell;
  public HighlightState m_HighlightState;

  public string GetText()
  {
    return this.m_TextMesh.Text;
  }

  public void SetText(string text)
  {
    this.m_TextMesh.Text = text;
  }

  protected override void OnOver(PegUIElement.InteractionState oldState)
  {
    if ((Object) this.m_Spell == (Object) null)
      return;
    this.m_Spell.ActivateState(SpellStateType.BIRTH);
  }

  protected override void OnOut(PegUIElement.InteractionState oldState)
  {
    if ((Object) this.m_Spell == (Object) null)
      return;
    this.m_Spell.ActivateState(SpellStateType.DEATH);
  }

  protected override void OnPress()
  {
    if ((Object) this.m_Spell == (Object) null)
      return;
    this.m_Spell.ActivateState(SpellStateType.IDLE);
  }

  protected override void OnRelease()
  {
    if ((Object) this.m_Spell == (Object) null)
      return;
    this.m_Spell.ActivateState(SpellStateType.ACTION);
  }
}
