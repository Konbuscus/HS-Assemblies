﻿// Decompiled with JetBrains decompiler
// Type: NAX07_Razuvious
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class NAX07_Razuvious : NAX_MissionEntity
{
  private bool m_heroPowerLinePlayed;

  public override void PreloadAssets()
  {
    this.PreloadSound("VO_NAX7_01_HP_02");
    this.PreloadSound("VO_NAX7_01_START_01");
    this.PreloadSound("VO_NAX7_01_EMOTE_05");
  }

  protected override void InitEmoteResponses()
  {
    this.m_emoteResponseGroups = new List<MissionEntity.EmoteResponseGroup>()
    {
      new MissionEntity.EmoteResponseGroup()
      {
        m_triggers = new List<EmoteType>((IEnumerable<EmoteType>) MissionEntity.STANDARD_EMOTE_RESPONSE_TRIGGERS),
        m_responses = new List<MissionEntity.EmoteResponse>()
        {
          new MissionEntity.EmoteResponse()
          {
            m_soundName = "VO_NAX7_01_EMOTE_05",
            m_stringTag = "VO_NAX7_01_EMOTE_05"
          }
        }
      }
    };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleGameOverWithTiming(TAG_PLAYSTATE gameResult)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX07_Razuvious.\u003CHandleGameOverWithTiming\u003Ec__Iterator1D2() { gameResult = gameResult, \u003C\u0024\u003EgameResult = gameResult };
  }

  [DebuggerHidden]
  protected override IEnumerator RespondToPlayedCardWithTiming(Entity entity)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX07_Razuvious.\u003CRespondToPlayedCardWithTiming\u003Ec__Iterator1D3() { entity = entity, \u003C\u0024\u003Eentity = entity, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  protected override IEnumerator HandleMissionEventWithTiming(int missionEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NAX07_Razuvious.\u003CHandleMissionEventWithTiming\u003Ec__Iterator1D4() { missionEvent = missionEvent, \u003C\u0024\u003EmissionEvent = missionEvent, \u003C\u003Ef__this = this };
  }
}
