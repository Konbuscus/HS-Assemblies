﻿// Decompiled with JetBrains decompiler
// Type: EchoOfMedivh
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

public class EchoOfMedivh : SpawnToHandSpell
{
  protected override void OnAction(SpellStateType prevStateType)
  {
    Player controller = this.GetSourceCard().GetEntity().GetController();
    ZonePlay battlefieldZone = controller.GetBattlefieldZone();
    if (controller.IsRevealed())
    {
      for (int targetIndex = 0; targetIndex < this.m_targets.Count; ++targetIndex)
      {
        string cardIdForTarget = this.GetCardIdForTarget(targetIndex);
        for (int index = 0; index < battlefieldZone.GetCardCount(); ++index)
        {
          Card cardAtIndex = battlefieldZone.GetCardAtIndex(index);
          if (cardAtIndex.GetPredictedZonePosition() == 0)
          {
            string cardId = cardAtIndex.GetEntity().GetCardId();
            if (!(cardIdForTarget != cardId) && this.AddUniqueOriginForTarget(targetIndex, cardAtIndex))
              break;
          }
        }
      }
    }
    else
    {
      int index = 0;
      for (int targetIndex = 0; targetIndex < this.m_targets.Count; ++targetIndex)
      {
        Card cardAtIndex = battlefieldZone.GetCardAtIndex(index);
        this.AddOriginForTarget(targetIndex, cardAtIndex);
        ++index;
      }
    }
    base.OnAction(prevStateType);
  }
}
