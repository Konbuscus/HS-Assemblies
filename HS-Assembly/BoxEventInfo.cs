﻿// Decompiled with JetBrains decompiler
// Type: BoxEventInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class BoxEventInfo
{
  public Spell m_StartupHub;
  public Spell m_StartupTutorial;
  public Spell m_TutorialPlay;
  public Spell m_DiskLoading;
  public Spell m_DiskMainMenu;
  public Spell m_DoorsClose;
  public Spell m_DoorsOpen;
  public Spell m_DrawerClose;
  public Spell m_DrawerOpen;
  public Spell m_ShadowFadeIn;
  public Spell m_ShadowFadeOut;
  public Spell m_StartupSetRotation;
}
