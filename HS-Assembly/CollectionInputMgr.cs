﻿// Decompiled with JetBrains decompiler
// Type: CollectionInputMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CollectionInputMgr : MonoBehaviour
{
  public CollectionDraggableCardVisual m_heldCardVisual;
  public Collider TooltipPlane;
  private static CollectionInputMgr s_instance;
  private bool m_showingDeckTile;
  private Vector3 m_heldCardScreenSpace;
  private bool m_mouseIsOverDeck;
  private UIBScrollable m_scrollBar;
  private bool m_cardsDraggable;

  private void Awake()
  {
    CollectionInputMgr.s_instance = this;
    UniversalInputManager.Get().RegisterMouseOnOrOffScreenListener(new UniversalInputManager.MouseOnOrOffScreenCallback(this.OnMouseOnOrOffScreen));
  }

  private void OnDestroy()
  {
    CollectionInputMgr.s_instance = (CollectionInputMgr) null;
  }

  private void Start()
  {
  }

  private void Update()
  {
    this.UpdateHeldCard();
  }

  public static CollectionInputMgr Get()
  {
    return CollectionInputMgr.s_instance;
  }

  public void Unload()
  {
    UniversalInputManager.Get().UnregisterMouseOnOrOffScreenListener(new UniversalInputManager.MouseOnOrOffScreenCallback(this.OnMouseOnOrOffScreen));
  }

  public bool HandleKeyboardInput()
  {
    if (Input.GetKeyUp(KeyCode.Escape))
    {
      if ((Object) CardBackInfoManager.Get() != (Object) null && CardBackInfoManager.Get().IsPreviewing)
      {
        CardBackInfoManager.Get().CancelPreview();
        return true;
      }
      if ((Object) CraftingManager.Get() != (Object) null && CraftingManager.Get().IsCardShowing())
      {
        Navigation.GoBack();
        return true;
      }
    }
    return false;
  }

  public bool GrabCard(CollectionCardVisual cardVisual)
  {
    Actor actor = cardVisual.GetActor();
    if (!this.CanGrabItem(actor) || !this.m_heldCardVisual.ChangeActor(actor, cardVisual.GetVisualType()))
      return false;
    this.m_scrollBar.Pause(true);
    PegCursor.Get().SetMode(PegCursor.Mode.DRAG);
    CollectionCardBack component = actor.GetComponent<CollectionCardBack>();
    this.m_heldCardVisual.SetSlot((CollectionDeckSlot) null);
    if ((Object) component != (Object) null)
      this.m_heldCardVisual.SetCardBackId(component.GetCardBackId());
    this.m_heldCardVisual.transform.position = actor.transform.position;
    this.m_heldCardVisual.Show(this.m_mouseIsOverDeck);
    SoundManager.Get().LoadAndPlay("collection_manager_pick_up_card", this.m_heldCardVisual.gameObject);
    return true;
  }

  public bool GrabCard(DeckTrayDeckTileVisual deckTileVisual)
  {
    Actor actor = (Actor) deckTileVisual.GetActor();
    if (!this.CanGrabItem(actor) || !this.m_heldCardVisual.ChangeActor(actor, CollectionManagerDisplay.ViewMode.CARDS))
      return false;
    CollectionDeck editingDeck = CollectionDeckTray.Get().GetCardsContent().GetEditingDeck();
    this.m_scrollBar.Pause(true);
    PegCursor.Get().SetMode(PegCursor.Mode.DRAG);
    this.m_heldCardVisual.SetSlot(deckTileVisual.GetSlot());
    this.m_heldCardVisual.transform.position = actor.transform.position;
    this.m_heldCardVisual.Show(this.m_mouseIsOverDeck);
    SoundManager.Get().LoadAndPlay("collection_manager_pick_up_card", this.m_heldCardVisual.gameObject);
    CollectionDeckTray.Get().RemoveCard(this.m_heldCardVisual.GetCardID(), this.m_heldCardVisual.GetPremium(), editingDeck.IsValidSlot(deckTileVisual.GetSlot()));
    if (!Options.Get().GetBool(Option.HAS_REMOVED_CARD_FROM_DECK, false))
    {
      CollectionDeckTray.Get().GetCardsContent().HideDeckHelpPopup();
      Options.Get().SetBool(Option.HAS_REMOVED_CARD_FROM_DECK, true);
    }
    return true;
  }

  public void DropCard(DeckTrayDeckTileVisual deckTileToRemove)
  {
    this.DropCard(false, deckTileToRemove);
  }

  public void SetScrollbar(UIBScrollable scrollbar)
  {
    this.m_scrollBar = scrollbar;
  }

  public bool IsDraggingScrollbar()
  {
    if ((Object) this.m_scrollBar != (Object) null)
      return this.m_scrollBar.IsDragging();
    return false;
  }

  public bool HasHeldCard()
  {
    return this.m_heldCardVisual.IsShown();
  }

  private bool CanGrabItem(Actor actor)
  {
    return !this.IsDraggingScrollbar() && !this.m_heldCardVisual.IsShown() && !((Object) actor == (Object) null);
  }

  private void UpdateHeldCard()
  {
    RaycastHit hitInfo;
    if (!this.m_heldCardVisual.IsShown() || !UniversalInputManager.Get().GetInputHitInfo((LayerMask) GameLayer.DragPlane.LayerBit(), out hitInfo))
      return;
    this.m_heldCardVisual.transform.position = hitInfo.point;
    this.m_mouseIsOverDeck = CollectionDeckTray.Get().MouseIsOver();
    this.m_heldCardVisual.UpdateVisual(this.m_mouseIsOverDeck);
    if (!Input.GetMouseButtonUp(0))
      return;
    this.DropCard(this.m_heldCardVisual.GetDeckTileToRemove());
  }

  private void DropCard(bool dragCanceled, DeckTrayDeckTileVisual deckTileToRemove)
  {
    PegCursor.Get().SetMode(PegCursor.Mode.STOPDRAG);
    if (!dragCanceled)
    {
      if (this.m_mouseIsOverDeck)
      {
        switch (this.m_heldCardVisual.GetVisualType())
        {
          case CollectionManagerDisplay.ViewMode.CARDS:
            CollectionDeckTray.Get().AddCard(this.m_heldCardVisual.GetEntityDef(), this.m_heldCardVisual.GetPremium(), deckTileToRemove, true, (Actor) null);
            break;
          case CollectionManagerDisplay.ViewMode.HERO_SKINS:
            CollectionDeckTray.Get().GetHeroSkinContent().UpdateHeroSkin(this.m_heldCardVisual.GetEntityDef(), this.m_heldCardVisual.GetPremium(), true);
            break;
          case CollectionManagerDisplay.ViewMode.CARD_BACKS:
            object cardBackId = (object) this.m_heldCardVisual.GetCardBackId();
            if (cardBackId == null)
            {
              Debug.LogWarning((object) "Cardback ID not set for dragging card back.");
              break;
            }
            CollectionDeckTray.Get().GetCardBackContent().UpdateCardBack((int) cardBackId, true, (GameObject) null);
            break;
        }
      }
      else
        SoundManager.Get().LoadAndPlay("collection_manager_drop_card", this.m_heldCardVisual.gameObject);
    }
    this.m_heldCardVisual.Hide();
    this.m_scrollBar.Pause(false);
  }

  private void OnMouseOnOrOffScreen(bool onScreen)
  {
    if (onScreen || !this.m_heldCardVisual.IsShown())
      return;
    this.DropCard(true, (DeckTrayDeckTileVisual) null);
    CollectionDeckTray.Get().GetDeckBigCard().ForceHide();
  }
}
