﻿// Decompiled with JetBrains decompiler
// Type: ShaderPreCompiler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class ShaderPreCompiler : MonoBehaviour
{
  protected static Map<string, Shader> s_shaderCache = new Map<string, Shader>();
  private readonly string[] GOLDEN_UBER_KEYWORDS1 = new string[2]{ "FX3_ADDBLEND", "FX3_ALPHABLEND" };
  private readonly string[] GOLDEN_UBER_KEYWORDS2 = new string[3]{ "LAYER3", "FX3_FLOWMAP", "LAYER4" };
  private readonly Vector3[] MESH_VERTS = new Vector3[3]{ Vector3.zero, Vector3.zero, Vector3.zero };
  private readonly Vector2[] MESH_UVS = new Vector2[3]{ new Vector2(0.0f, 0.0f), new Vector2(1f, 0.0f), new Vector2(0.0f, 1f) };
  private readonly Vector3[] MESH_NORMALS = new Vector3[3]{ Vector3.up, Vector3.up, Vector3.up };
  private readonly Vector4[] MESH_TANGENTS = new Vector4[3]{ new Vector4(1f, 0.0f, 0.0f, 0.0f), new Vector4(1f, 0.0f, 0.0f, 0.0f), new Vector4(1f, 0.0f, 0.0f, 0.0f) };
  private readonly int[] MESH_TRIANGLES = new int[3]{ 2, 1, 0 };
  public Shader m_GoldenUberShader;
  public Shader[] m_StartupCompileShaders;
  public Shader[] m_SceneChangeCompileShaders;
  private bool SceneChangeShadersCompiled;
  private bool PremiumShadersCompiled;

  private void Start()
  {
    if (GraphicsManager.Get().isVeryLowQualityDevice())
    {
      UnityEngine.Debug.Log((object) "ShaderPreCompiler: Disabled, very low quality mode");
    }
    else
    {
      if (GraphicsManager.Get().RenderQualityLevel != GraphicsQuality.Low)
        this.StartCoroutine(this.WarmupShaders(this.m_StartupCompileShaders));
      SceneMgr.Get().RegisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.WarmupSceneChangeShader));
      this.AddShader(this.m_GoldenUberShader.name, this.m_GoldenUberShader);
      foreach (Shader startupCompileShader in this.m_StartupCompileShaders)
      {
        if (!((Object) startupCompileShader == (Object) null))
          this.AddShader(startupCompileShader.name, startupCompileShader);
      }
      foreach (Shader changeCompileShader in this.m_SceneChangeCompileShaders)
      {
        if (!((Object) changeCompileShader == (Object) null))
          this.AddShader(changeCompileShader.name, changeCompileShader);
      }
    }
  }

  public static Shader GetShader(string shaderName)
  {
    Shader shader1;
    if (ShaderPreCompiler.s_shaderCache.TryGetValue(shaderName, out shader1))
      return shader1;
    Shader shader2 = Shader.Find(shaderName);
    if ((Object) shader2 != (Object) null)
      ShaderPreCompiler.s_shaderCache.Add(shaderName, shader2);
    return shader2;
  }

  private void AddShader(string shaderName, Shader shader)
  {
    if (ShaderPreCompiler.s_shaderCache.ContainsKey(shaderName))
      return;
    ShaderPreCompiler.s_shaderCache.Add(shaderName, shader);
  }

  private void WarmupSceneChangeShader(SceneMgr.Mode prevMode, Scene prevScene, object userData)
  {
    if ((SceneMgr.Get().GetMode() == SceneMgr.Mode.GAMEPLAY || SceneMgr.Get().GetMode() == SceneMgr.Mode.COLLECTIONMANAGER || SceneMgr.Get().GetMode() == SceneMgr.Mode.TAVERN_BRAWL) && Network.ShouldBeConnectedToAurora())
    {
      this.StartCoroutine(this.WarmupGoldenUberShader());
      this.PremiumShadersCompiled = true;
    }
    if (prevMode != SceneMgr.Mode.HUB || this.SceneChangeShadersCompiled)
      return;
    this.SceneChangeShadersCompiled = true;
    if (GraphicsManager.Get().RenderQualityLevel != GraphicsQuality.Low)
      this.StartCoroutine(this.WarmupShaders(this.m_SceneChangeCompileShaders));
    if (!this.SceneChangeShadersCompiled || !this.PremiumShadersCompiled)
      return;
    SceneMgr.Get().UnregisterScenePreUnloadEvent(new SceneMgr.ScenePreUnloadCallback(this.WarmupSceneChangeShader));
  }

  [DebuggerHidden]
  private IEnumerator WarmupGoldenUberShader()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ShaderPreCompiler.\u003CWarmupGoldenUberShader\u003Ec__Iterator33F() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WarmupShaders(Shader[] shaders)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ShaderPreCompiler.\u003CWarmupShaders\u003Ec__Iterator340() { shaders = shaders, \u003C\u0024\u003Eshaders = shaders };
  }

  private GameObject CreateMesh(string name)
  {
    GameObject gameObject = new GameObject();
    gameObject.name = name;
    gameObject.transform.parent = this.gameObject.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    gameObject.AddComponent<MeshFilter>();
    gameObject.AddComponent<MeshRenderer>();
    gameObject.GetComponent<MeshFilter>().mesh = new Mesh()
    {
      vertices = this.MESH_VERTS,
      uv = this.MESH_UVS,
      normals = this.MESH_NORMALS,
      tangents = this.MESH_TANGENTS,
      triangles = this.MESH_TRIANGLES
    };
    return gameObject;
  }

  private Material CreateMaterial(string name, Shader shader)
  {
    Material material = new Material(shader);
    material.name = name;
    return material;
  }
}
