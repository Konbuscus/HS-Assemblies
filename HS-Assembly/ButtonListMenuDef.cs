﻿// Decompiled with JetBrains decompiler
// Type: ButtonListMenuDef
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7920E40E-3106-4716-8009-D7ECCAF25E2A
// Assembly location: D:\Hearthstone\Hearthstone_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[CustomEditClass]
public class ButtonListMenuDef : MonoBehaviour
{
  [CustomEditField(Sections = "Template Items")]
  public Vector3 m_horizontalDividerMinPadding = new Vector3(0.0f, 0.0f, 0.18f);
  [CustomEditField(Sections = "Header")]
  public UberText m_headerText;
  [CustomEditField(Sections = "Header")]
  public MultiSliceElement m_header;
  [CustomEditField(Sections = "Header")]
  public GameObject m_headerMiddle;
  [CustomEditField(Sections = "Layout")]
  public NineSliceElement m_background;
  [CustomEditField(Sections = "Layout")]
  public NineSliceElement m_border;
  [CustomEditField(Sections = "Layout")]
  public MultiSliceElement m_buttonContainer;
  [CustomEditField(Sections = "Template Items")]
  public UIBButton m_templateButton;
  [CustomEditField(Sections = "Template Items")]
  public GameObject m_templateHorizontalDivider;
}
